// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _22sort(int _x_4971, int _order_4972)
{
    int _gap_4973 = NOVALUE;
    int _j_4974 = NOVALUE;
    int _first_4975 = NOVALUE;
    int _last_4976 = NOVALUE;
    int _tempi_4977 = NOVALUE;
    int _tempj_4978 = NOVALUE;
    int _2509 = NOVALUE;
    int _2505 = NOVALUE;
    int _2502 = NOVALUE;
    int _2498 = NOVALUE;
    int _2495 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if order >= 0 then*/

    /** 		order = -1*/
    _order_4972 = -1;
    goto L1; // [16] 25

    /** 		order = 1*/
    _order_4972 = 1;
L1: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_4971)){
            _last_4976 = SEQ_PTR(_x_4971)->length;
    }
    else {
        _last_4976 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_4976 >= 0) {
        _2495 = _last_4976 / 10;
    }
    else {
        temp_dbl = floor((double)_last_4976 / (double)10);
        _2495 = (long)temp_dbl;
    }
    _gap_4973 = _2495 + 1;
    _2495 = NOVALUE;

    /** 	while 1 do*/
L2: 

    /** 		first = gap + 1*/
    _first_4975 = _gap_4973 + 1;

    /** 		for i = first to last do*/
    _2498 = _last_4976;
    {
        int _i_4988;
        _i_4988 = _first_4975;
L3: 
        if (_i_4988 > _2498){
            goto L4; // [56] 152
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_4977);
        _2 = (int)SEQ_PTR(_x_4971);
        _tempi_4977 = (int)*(((s1_ptr)_2)->base + _i_4988);
        Ref(_tempi_4977);

        /** 			j = i - gap*/
        _j_4974 = _i_4988 - _gap_4973;

        /** 			while 1 do*/
L5: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_4978);
        _2 = (int)SEQ_PTR(_x_4971);
        _tempj_4978 = (int)*(((s1_ptr)_2)->base + _j_4974);
        Ref(_tempj_4978);

        /** 				if eu:compare(tempi, tempj) != order then*/
        if (IS_ATOM_INT(_tempi_4977) && IS_ATOM_INT(_tempj_4978)){
            _2502 = (_tempi_4977 < _tempj_4978) ? -1 : (_tempi_4977 > _tempj_4978);
        }
        else{
            _2502 = compare(_tempi_4977, _tempj_4978);
        }
        if (_2502 == _order_4972)
        goto L6; // [92] 107

        /** 					j += gap*/
        _j_4974 = _j_4974 + _gap_4973;

        /** 					exit*/
        goto L7; // [104] 139
L6: 

        /** 				x[j+gap] = tempj*/
        _2505 = _j_4974 + _gap_4973;
        Ref(_tempj_4978);
        _2 = (int)SEQ_PTR(_x_4971);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_4971 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _2505);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_4978;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_4974 > _gap_4973)
        goto L8; // [119] 128

        /** 					exit*/
        goto L7; // [125] 139
L8: 

        /** 				j -= gap*/
        _j_4974 = _j_4974 - _gap_4973;

        /** 			end while*/
        goto L5; // [136] 80
L7: 

        /** 			x[j] = tempi*/
        Ref(_tempi_4977);
        _2 = (int)SEQ_PTR(_x_4971);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_4971 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_4974);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_4977;
        DeRef(_1);

        /** 		end for*/
        _i_4988 = _i_4988 + 1;
        goto L3; // [147] 63
L4: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_4973 != 1)
    goto L9; // [154] 167

    /** 			return x*/
    DeRef(_tempi_4977);
    DeRef(_tempj_4978);
    DeRef(_2505);
    _2505 = NOVALUE;
    return _x_4971;
    goto L2; // [164] 45
L9: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_4973 >= 0) {
        _2509 = _gap_4973 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_4973 / (double)7);
        _2509 = (long)temp_dbl;
    }
    _gap_4973 = _2509 + 1;
    _2509 = NOVALUE;

    /** 	end while*/
    goto L2; // [180] 45
    ;
}


int _22custom_sort(int _custom_compare_5009, int _x_5010, int _data_5011, int _order_5012)
{
    int _gap_5013 = NOVALUE;
    int _j_5014 = NOVALUE;
    int _first_5015 = NOVALUE;
    int _last_5016 = NOVALUE;
    int _tempi_5017 = NOVALUE;
    int _tempj_5018 = NOVALUE;
    int _result_5019 = NOVALUE;
    int _args_5020 = NOVALUE;
    int _2537 = NOVALUE;
    int _2533 = NOVALUE;
    int _2530 = NOVALUE;
    int _2528 = NOVALUE;
    int _2527 = NOVALUE;
    int _2522 = NOVALUE;
    int _2519 = NOVALUE;
    int _2515 = NOVALUE;
    int _2513 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence args = {0, 0}*/
    DeRef(_args_5020);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _args_5020 = MAKE_SEQ(_1);

    /** 	if order >= 0 then*/

    /** 		order = -1*/
    _order_5012 = -1;
    goto L1; // [24] 33

    /** 		order = 1*/
    _order_5012 = 1;
L1: 

    /** 	if atom(data) then*/
    _2513 = IS_ATOM(_data_5011);
    if (_2513 == 0)
    {
        _2513 = NOVALUE;
        goto L2; // [38] 50
    }
    else{
        _2513 = NOVALUE;
    }

    /** 		args &= data*/
    if (IS_SEQUENCE(_args_5020) && IS_ATOM(_data_5011)) {
        Ref(_data_5011);
        Append(&_args_5020, _args_5020, _data_5011);
    }
    else if (IS_ATOM(_args_5020) && IS_SEQUENCE(_data_5011)) {
    }
    else {
        Concat((object_ptr)&_args_5020, _args_5020, _data_5011);
    }
    goto L3; // [47] 70
L2: 

    /** 	elsif length(data) then*/
    _2515 = 0;
L3: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_5010)){
            _last_5016 = SEQ_PTR(_x_5010)->length;
    }
    else {
        _last_5016 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_5016 >= 0) {
        _2519 = _last_5016 / 10;
    }
    else {
        temp_dbl = floor((double)_last_5016 / (double)10);
        _2519 = (long)temp_dbl;
    }
    _gap_5013 = _2519 + 1;
    _2519 = NOVALUE;

    /** 	while 1 do*/
L4: 

    /** 		first = gap + 1*/
    _first_5015 = _gap_5013 + 1;

    /** 		for i = first to last do*/
    _2522 = _last_5016;
    {
        int _i_5038;
        _i_5038 = _first_5015;
L5: 
        if (_i_5038 > _2522){
            goto L6; // [101] 240
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_5017);
        _2 = (int)SEQ_PTR(_x_5010);
        _tempi_5017 = (int)*(((s1_ptr)_2)->base + _i_5038);
        Ref(_tempi_5017);

        /** 			args[1] = tempi*/
        Ref(_tempi_5017);
        _2 = (int)SEQ_PTR(_args_5020);
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_5017;
        DeRef(_1);

        /** 			j = i - gap*/
        _j_5014 = _i_5038 - _gap_5013;

        /** 			while 1 do*/
L7: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_5018);
        _2 = (int)SEQ_PTR(_x_5010);
        _tempj_5018 = (int)*(((s1_ptr)_2)->base + _j_5014);
        Ref(_tempj_5018);

        /** 				args[2] = tempj*/
        Ref(_tempj_5018);
        _2 = (int)SEQ_PTR(_args_5020);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_5018;
        DeRef(_1);

        /** 				result = call_func(custom_compare, args)*/
        _1 = (int)SEQ_PTR(_args_5020);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_custom_compare_5009].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
            case 7:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28)
                                     );
                break;
            case 8:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32)
                                     );
                break;
            case 9:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36)
                                     );
                break;
            case 10:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40)
                                     );
                break;
            case 11:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44)
                                     );
                break;
            case 12:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48)
                                     );
                break;
            case 13:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52)
                                     );
                break;
            case 14:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56)
                                     );
                break;
            case 15:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60)
                                     );
                break;
            case 16:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64)
                                     );
                break;
            case 17:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68)
                                     );
                break;
            case 18:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72)
                                     );
                break;
            case 19:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                Ref(*(int *)(_2+76));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72), 
                                    *(int *)(_2+76)
                                     );
                break;
            case 20:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                Ref(*(int *)(_2+28));
                Ref(*(int *)(_2+32));
                Ref(*(int *)(_2+36));
                Ref(*(int *)(_2+40));
                Ref(*(int *)(_2+44));
                Ref(*(int *)(_2+48));
                Ref(*(int *)(_2+52));
                Ref(*(int *)(_2+56));
                Ref(*(int *)(_2+60));
                Ref(*(int *)(_2+64));
                Ref(*(int *)(_2+68));
                Ref(*(int *)(_2+72));
                Ref(*(int *)(_2+76));
                Ref(*(int *)(_2+80));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24), 
                                    *(int *)(_2+28), 
                                    *(int *)(_2+32), 
                                    *(int *)(_2+36), 
                                    *(int *)(_2+40), 
                                    *(int *)(_2+44), 
                                    *(int *)(_2+48), 
                                    *(int *)(_2+52), 
                                    *(int *)(_2+56), 
                                    *(int *)(_2+60), 
                                    *(int *)(_2+64), 
                                    *(int *)(_2+68), 
                                    *(int *)(_2+72), 
                                    *(int *)(_2+76), 
                                    *(int *)(_2+80)
                                     );
                break;
        }
        DeRef(_result_5019);
        _result_5019 = _1;

        /** 				if sequence(result) then*/
        _2527 = IS_SEQUENCE(_result_5019);
        if (_2527 == 0)
        {
            _2527 = NOVALUE;
            goto L8; // [154] 174
        }
        else{
            _2527 = NOVALUE;
        }

        /** 					args[3] = result[2]*/
        _2 = (int)SEQ_PTR(_result_5019);
        _2528 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_2528);
        _2 = (int)SEQ_PTR(_args_5020);
        _2 = (int)(((s1_ptr)_2)->base + 3);
        _1 = *(int *)_2;
        *(int *)_2 = _2528;
        if( _1 != _2528 ){
            DeRef(_1);
        }
        _2528 = NOVALUE;

        /** 					result = result[1]*/
        _0 = _result_5019;
        _2 = (int)SEQ_PTR(_result_5019);
        _result_5019 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_result_5019);
        DeRef(_0);
L8: 

        /** 				if eu:compare(result, 0) != order then*/
        if (IS_ATOM_INT(_result_5019) && IS_ATOM_INT(0)){
            _2530 = (_result_5019 < 0) ? -1 : (_result_5019 > 0);
        }
        else{
            _2530 = compare(_result_5019, 0);
        }
        if (_2530 == _order_5012)
        goto L9; // [180] 195

        /** 					j += gap*/
        _j_5014 = _j_5014 + _gap_5013;

        /** 					exit*/
        goto LA; // [192] 227
L9: 

        /** 				x[j+gap] = tempj*/
        _2533 = _j_5014 + _gap_5013;
        Ref(_tempj_5018);
        _2 = (int)SEQ_PTR(_x_5010);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_5010 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _2533);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_5018;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_5014 > _gap_5013)
        goto LB; // [207] 216

        /** 					exit*/
        goto LA; // [213] 227
LB: 

        /** 				j -= gap*/
        _j_5014 = _j_5014 - _gap_5013;

        /** 			end while*/
        goto L7; // [224] 131
LA: 

        /** 			x[j] = tempi*/
        Ref(_tempi_5017);
        _2 = (int)SEQ_PTR(_x_5010);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_5010 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_5014);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_5017;
        DeRef(_1);

        /** 		end for*/
        _i_5038 = _i_5038 + 1;
        goto L5; // [235] 108
L6: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_5013 != 1)
    goto LC; // [242] 255

    /** 			return x*/
    DeRef(_data_5011);
    DeRef(_tempi_5017);
    DeRef(_tempj_5018);
    DeRef(_result_5019);
    DeRef(_args_5020);
    DeRef(_2533);
    _2533 = NOVALUE;
    return _x_5010;
    goto L4; // [252] 90
LC: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_5013 >= 0) {
        _2537 = _gap_5013 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_5013 / (double)7);
        _2537 = (long)temp_dbl;
    }
    _gap_5013 = _2537 + 1;
    _2537 = NOVALUE;

    /** 	end while*/
    goto L4; // [268] 90
    ;
}


int _22column_compare(int _a_5064, int _b_5065, int _cols_5066)
{
    int _sign_5067 = NOVALUE;
    int _column_5068 = NOVALUE;
    int _2560 = NOVALUE;
    int _2558 = NOVALUE;
    int _2557 = NOVALUE;
    int _2556 = NOVALUE;
    int _2555 = NOVALUE;
    int _2554 = NOVALUE;
    int _2553 = NOVALUE;
    int _2551 = NOVALUE;
    int _2550 = NOVALUE;
    int _2549 = NOVALUE;
    int _2547 = NOVALUE;
    int _2545 = NOVALUE;
    int _2542 = NOVALUE;
    int _2540 = NOVALUE;
    int _2539 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(cols) do*/
    if (IS_SEQUENCE(_cols_5066)){
            _2539 = SEQ_PTR(_cols_5066)->length;
    }
    else {
        _2539 = 1;
    }
    {
        int _i_5070;
        _i_5070 = 1;
L1: 
        if (_i_5070 > _2539){
            goto L2; // [6] 176
        }

        /** 		if cols[i] < 0 then*/
        _2 = (int)SEQ_PTR(_cols_5066);
        _2540 = (int)*(((s1_ptr)_2)->base + _i_5070);
        if (binary_op_a(GREATEREQ, _2540, 0)){
            _2540 = NOVALUE;
            goto L3; // [19] 42
        }
        _2540 = NOVALUE;

        /** 			sign = -1*/
        _sign_5067 = -1;

        /** 			column = -cols[i]*/
        _2 = (int)SEQ_PTR(_cols_5066);
        _2542 = (int)*(((s1_ptr)_2)->base + _i_5070);
        if (IS_ATOM_INT(_2542)) {
            if ((unsigned long)_2542 == 0xC0000000)
            _column_5068 = (int)NewDouble((double)-0xC0000000);
            else
            _column_5068 = - _2542;
        }
        else {
            _column_5068 = unary_op(UMINUS, _2542);
        }
        _2542 = NOVALUE;
        if (!IS_ATOM_INT(_column_5068)) {
            _1 = (long)(DBL_PTR(_column_5068)->dbl);
            if (UNIQUE(DBL_PTR(_column_5068)) && (DBL_PTR(_column_5068)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_column_5068);
            _column_5068 = _1;
        }
        goto L4; // [39] 56
L3: 

        /** 			sign = 1*/
        _sign_5067 = 1;

        /** 			column = cols[i]*/
        _2 = (int)SEQ_PTR(_cols_5066);
        _column_5068 = (int)*(((s1_ptr)_2)->base + _i_5070);
        if (!IS_ATOM_INT(_column_5068)){
            _column_5068 = (long)DBL_PTR(_column_5068)->dbl;
        }
L4: 

        /** 		if column <= length(a) then*/
        if (IS_SEQUENCE(_a_5064)){
                _2545 = SEQ_PTR(_a_5064)->length;
        }
        else {
            _2545 = 1;
        }
        if (_column_5068 > _2545)
        goto L5; // [63] 137

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_5065)){
                _2547 = SEQ_PTR(_b_5065)->length;
        }
        else {
            _2547 = 1;
        }
        if (_column_5068 > _2547)
        goto L6; // [72] 121

        /** 				if not equal(a[column], b[column]) then*/
        _2 = (int)SEQ_PTR(_a_5064);
        _2549 = (int)*(((s1_ptr)_2)->base + _column_5068);
        _2 = (int)SEQ_PTR(_b_5065);
        _2550 = (int)*(((s1_ptr)_2)->base + _column_5068);
        if (_2549 == _2550)
        _2551 = 1;
        else if (IS_ATOM_INT(_2549) && IS_ATOM_INT(_2550))
        _2551 = 0;
        else
        _2551 = (compare(_2549, _2550) == 0);
        _2549 = NOVALUE;
        _2550 = NOVALUE;
        if (_2551 != 0)
        goto L7; // [90] 169
        _2551 = NOVALUE;

        /** 					return sign * eu:compare(a[column], b[column])*/
        _2 = (int)SEQ_PTR(_a_5064);
        _2553 = (int)*(((s1_ptr)_2)->base + _column_5068);
        _2 = (int)SEQ_PTR(_b_5065);
        _2554 = (int)*(((s1_ptr)_2)->base + _column_5068);
        if (IS_ATOM_INT(_2553) && IS_ATOM_INT(_2554)){
            _2555 = (_2553 < _2554) ? -1 : (_2553 > _2554);
        }
        else{
            _2555 = compare(_2553, _2554);
        }
        _2553 = NOVALUE;
        _2554 = NOVALUE;
        if (_sign_5067 == (short)_sign_5067)
        _2556 = _sign_5067 * _2555;
        else
        _2556 = NewDouble(_sign_5067 * (double)_2555);
        _2555 = NOVALUE;
        DeRef(_a_5064);
        DeRef(_b_5065);
        DeRef(_cols_5066);
        return _2556;
        goto L7; // [118] 169
L6: 

        /** 				return sign * -1*/
        if (_sign_5067 == (short)_sign_5067)
        _2557 = _sign_5067 * -1;
        else
        _2557 = NewDouble(_sign_5067 * (double)-1);
        DeRef(_a_5064);
        DeRef(_b_5065);
        DeRef(_cols_5066);
        DeRef(_2556);
        _2556 = NOVALUE;
        return _2557;
        goto L7; // [134] 169
L5: 

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_5065)){
                _2558 = SEQ_PTR(_b_5065)->length;
        }
        else {
            _2558 = 1;
        }
        if (_column_5068 > _2558)
        goto L8; // [142] 161

        /** 				return sign * 1*/
        _2560 = _sign_5067 * 1;
        DeRef(_a_5064);
        DeRef(_b_5065);
        DeRef(_cols_5066);
        DeRef(_2556);
        _2556 = NOVALUE;
        DeRef(_2557);
        _2557 = NOVALUE;
        return _2560;
        goto L9; // [158] 168
L8: 

        /** 				return 0*/
        DeRef(_a_5064);
        DeRef(_b_5065);
        DeRef(_cols_5066);
        DeRef(_2556);
        _2556 = NOVALUE;
        DeRef(_2557);
        _2557 = NOVALUE;
        DeRef(_2560);
        _2560 = NOVALUE;
        return 0;
L9: 
L7: 

        /** 	end for*/
        _i_5070 = _i_5070 + 1;
        goto L1; // [171] 13
L2: 
        ;
    }

    /** 	return 0*/
    DeRef(_a_5064);
    DeRef(_b_5065);
    DeRef(_cols_5066);
    DeRef(_2556);
    _2556 = NOVALUE;
    DeRef(_2557);
    _2557 = NOVALUE;
    DeRef(_2560);
    _2560 = NOVALUE;
    return 0;
    ;
}



// 0xFCE7BE16
