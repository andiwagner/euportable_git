// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _53regex(int _o_22233)
{
    int _13013 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sequence(o)*/
    _13013 = IS_SEQUENCE(_o_22233);
    DeRef(_o_22233);
    return _13013;
    ;
}


int _53new(int _pattern_22273, int _options_22274)
{
    int _13033 = NOVALUE;
    int _13032 = NOVALUE;
    int _13030 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _13030 = 0;
    if (_13030 == 0)
    {
        _13030 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _13030 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22274 = _18or_all(_options_22274);
L1: 

    /** 	return machine_func(M_PCRE_COMPILE, { pattern, options })*/
    Ref(_options_22274);
    Ref(_pattern_22273);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pattern_22273;
    ((int *)_2)[2] = _options_22274;
    _13032 = MAKE_SEQ(_1);
    _13033 = machine(68, _13032);
    DeRefDS(_13032);
    _13032 = NOVALUE;
    DeRef(_pattern_22273);
    DeRef(_options_22274);
    return _13033;
    ;
}


int _53get_ovector_size(int _ex_22293, int _maxsize_22294)
{
    int _m_22295 = NOVALUE;
    int _13041 = NOVALUE;
    int _13038 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer m = machine_func(M_PCRE_GET_OVECTOR_SIZE, {ex})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_22293);
    *((int *)(_2+4)) = _ex_22293;
    _13038 = MAKE_SEQ(_1);
    _m_22295 = machine(97, _13038);
    DeRefDS(_13038);
    _13038 = NOVALUE;
    if (!IS_ATOM_INT(_m_22295)) {
        _1 = (long)(DBL_PTR(_m_22295)->dbl);
        if (UNIQUE(DBL_PTR(_m_22295)) && (DBL_PTR(_m_22295)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_m_22295);
        _m_22295 = _1;
    }

    /** 	if (m > maxsize) then*/
    if (_m_22295 <= 30)
    goto L1; // [19] 30

    /** 		return maxsize*/
    DeRef(_ex_22293);
    return 30;
L1: 

    /** 	return m+1*/
    _13041 = _m_22295 + 1;
    if (_13041 > MAXINT){
        _13041 = NewDouble((double)_13041);
    }
    DeRef(_ex_22293);
    return _13041;
    ;
}


int _53find(int _re_22303, int _haystack_22305, int _from_22306, int _options_22307, int _size_22308)
{
    int _13048 = NOVALUE;
    int _13047 = NOVALUE;
    int _13046 = NOVALUE;
    int _13043 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_22308)) {
        _1 = (long)(DBL_PTR(_size_22308)->dbl);
        if (UNIQUE(DBL_PTR(_size_22308)) && (DBL_PTR(_size_22308)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_22308);
        _size_22308 = _1;
    }

    /** 	if sequence(options) then */
    _13043 = IS_SEQUENCE(_options_22307);
    if (_13043 == 0)
    {
        _13043 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _13043 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_22307);
    _0 = _options_22307;
    _options_22307 = _18or_all(_options_22307);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_22308 >= 0)
    goto L2; // [22] 32

    /** 		size = 0*/
    _size_22308 = 0;
L2: 

    /** 	return machine_func(M_PCRE_EXEC, { re, haystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_22305)){
            _13046 = SEQ_PTR(_haystack_22305)->length;
    }
    else {
        _13046 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_22303);
    *((int *)(_2+4)) = _re_22303;
    Ref(_haystack_22305);
    *((int *)(_2+8)) = _haystack_22305;
    *((int *)(_2+12)) = _13046;
    Ref(_options_22307);
    *((int *)(_2+16)) = _options_22307;
    *((int *)(_2+20)) = _from_22306;
    *((int *)(_2+24)) = _size_22308;
    _13047 = MAKE_SEQ(_1);
    _13046 = NOVALUE;
    _13048 = machine(70, _13047);
    DeRefDS(_13047);
    _13047 = NOVALUE;
    DeRef(_re_22303);
    DeRef(_haystack_22305);
    DeRef(_options_22307);
    return _13048;
    ;
}


int _53find_all(int _re_22320, int _haystack_22322, int _from_22323, int _options_22324, int _size_22325)
{
    int _result_22332 = NOVALUE;
    int _results_22333 = NOVALUE;
    int _pHaystack_22334 = NOVALUE;
    int _13061 = NOVALUE;
    int _13060 = NOVALUE;
    int _13058 = NOVALUE;
    int _13056 = NOVALUE;
    int _13054 = NOVALUE;
    int _13050 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_22325)) {
        _1 = (long)(DBL_PTR(_size_22325)->dbl);
        if (UNIQUE(DBL_PTR(_size_22325)) && (DBL_PTR(_size_22325)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_22325);
        _size_22325 = _1;
    }

    /** 	if sequence(options) then */
    _13050 = IS_SEQUENCE(_options_22324);
    if (_13050 == 0)
    {
        _13050 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _13050 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_22324);
    _0 = _options_22324;
    _options_22324 = _18or_all(_options_22324);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_22325 >= 0)
    goto L2; // [22] 32

    /** 		size = 0*/
    _size_22325 = 0;
L2: 

    /** 	object result*/

    /** 	sequence results = {}*/
    RefDS(_5);
    DeRef(_results_22333);
    _results_22333 = _5;

    /** 	atom pHaystack = machine:allocate_string(haystack)*/
    Ref(_haystack_22322);
    _0 = _pHaystack_22334;
    _pHaystack_22334 = _4allocate_string(_haystack_22322, 0);
    DeRef(_0);

    /** 	while sequence(result) with entry do*/
    goto L3; // [50] 94
L4: 
    _13054 = IS_SEQUENCE(_result_22332);
    if (_13054 == 0)
    {
        _13054 = NOVALUE;
        goto L5; // [58] 119
    }
    else{
        _13054 = NOVALUE;
    }

    /** 		results = append(results, result)*/
    Ref(_result_22332);
    Append(&_results_22333, _results_22333, _result_22332);

    /** 		from = math:max(result) + 1*/
    Ref(_result_22332);
    _13056 = _18max(_result_22332);
    if (IS_ATOM_INT(_13056)) {
        _from_22323 = _13056 + 1;
    }
    else
    { // coercing _from_22323 to an integer 1
        _from_22323 = 1+(long)(DBL_PTR(_13056)->dbl);
        if( !IS_ATOM_INT(_from_22323) ){
            _from_22323 = (object)DBL_PTR(_from_22323)->dbl;
        }
    }
    DeRef(_13056);
    _13056 = NOVALUE;

    /** 		if from > length(haystack) then*/
    if (IS_SEQUENCE(_haystack_22322)){
            _13058 = SEQ_PTR(_haystack_22322)->length;
    }
    else {
        _13058 = 1;
    }
    if (_from_22323 <= _13058)
    goto L6; // [82] 91

    /** 			exit*/
    goto L5; // [88] 119
L6: 

    /** 	entry*/
L3: 

    /** 		result = machine_func(M_PCRE_EXEC, { re, pHaystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_22322)){
            _13060 = SEQ_PTR(_haystack_22322)->length;
    }
    else {
        _13060 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_22320);
    *((int *)(_2+4)) = _re_22320;
    Ref(_pHaystack_22334);
    *((int *)(_2+8)) = _pHaystack_22334;
    *((int *)(_2+12)) = _13060;
    Ref(_options_22324);
    *((int *)(_2+16)) = _options_22324;
    *((int *)(_2+20)) = _from_22323;
    *((int *)(_2+24)) = _size_22325;
    _13061 = MAKE_SEQ(_1);
    _13060 = NOVALUE;
    DeRef(_result_22332);
    _result_22332 = machine(70, _13061);
    DeRefDS(_13061);
    _13061 = NOVALUE;

    /** 	end while*/
    goto L4; // [116] 53
L5: 

    /** 	machine:free(pHaystack)*/
    Ref(_pHaystack_22334);
    _4free(_pHaystack_22334);

    /** 	return results*/
    DeRef(_re_22320);
    DeRef(_haystack_22322);
    DeRef(_options_22324);
    DeRef(_result_22332);
    DeRef(_pHaystack_22334);
    return _results_22333;
    ;
}


int _53has_match(int _re_22349, int _haystack_22351, int _from_22352, int _options_22353)
{
    int _13065 = NOVALUE;
    int _13064 = NOVALUE;
    int _13063 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_re_22349);
    _13063 = _53get_ovector_size(_re_22349, 30);
    Ref(_re_22349);
    Ref(_haystack_22351);
    _13064 = _53find(_re_22349, _haystack_22351, 1, 0, _13063);
    _13063 = NOVALUE;
    _13065 = IS_SEQUENCE(_13064);
    DeRef(_13064);
    _13064 = NOVALUE;
    DeRef(_re_22349);
    DeRef(_haystack_22351);
    return _13065;
    ;
}


int _53matches(int _re_22383, int _haystack_22385, int _from_22386, int _options_22387)
{
    int _str_offsets_22391 = NOVALUE;
    int _match_data_22393 = NOVALUE;
    int _tmp_22403 = NOVALUE;
    int _13102 = NOVALUE;
    int _13101 = NOVALUE;
    int _13100 = NOVALUE;
    int _13099 = NOVALUE;
    int _13098 = NOVALUE;
    int _13096 = NOVALUE;
    int _13095 = NOVALUE;
    int _13094 = NOVALUE;
    int _13093 = NOVALUE;
    int _13091 = NOVALUE;
    int _13090 = NOVALUE;
    int _13089 = NOVALUE;
    int _13088 = NOVALUE;
    int _13086 = NOVALUE;
    int _13085 = NOVALUE;
    int _13084 = NOVALUE;
    int _13081 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _13081 = 0;
    if (_13081 == 0)
    {
        _13081 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _13081 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22387 = _18or_all(0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_22387)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_22387;
             _str_offsets_22391 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_22391 = binary_op(AND_BITS, 201326592, _options_22387);
    }
    if (!IS_ATOM_INT(_str_offsets_22391)) {
        _1 = (long)(DBL_PTR(_str_offsets_22391)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_22391)) && (DBL_PTR(_str_offsets_22391)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_22391);
        _str_offsets_22391 = _1;
    }

    /** 	object match_data = find(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _13084 = not_bits(201326592);
    if (IS_ATOM_INT(_options_22387) && IS_ATOM_INT(_13084)) {
        {unsigned long tu;
             tu = (unsigned long)_options_22387 & (unsigned long)_13084;
             _13085 = MAKE_UINT(tu);
        }
    }
    else {
        _13085 = binary_op(AND_BITS, _options_22387, _13084);
    }
    DeRef(_13084);
    _13084 = NOVALUE;
    Ref(_re_22383);
    _13086 = _53get_ovector_size(_re_22383, 30);
    Ref(_re_22383);
    Ref(_haystack_22385);
    _0 = _match_data_22393;
    _match_data_22393 = _53find(_re_22383, _haystack_22385, _from_22386, _13085, _13086);
    DeRef(_0);
    _13085 = NOVALUE;
    _13086 = NOVALUE;

    /** 	if atom(match_data) then */
    _13088 = IS_ATOM(_match_data_22393);
    if (_13088 == 0)
    {
        _13088 = NOVALUE;
        goto L2; // [53] 63
    }
    else{
        _13088 = NOVALUE;
    }

    /** 		return ERROR_NOMATCH */
    DeRef(_re_22383);
    DeRef(_haystack_22385);
    DeRef(_options_22387);
    DeRef(_match_data_22393);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_22393)){
            _13089 = SEQ_PTR(_match_data_22393)->length;
    }
    else {
        _13089 = 1;
    }
    {
        int _i_22401;
        _i_22401 = 1;
L3: 
        if (_i_22401 > _13089){
            goto L4; // [68] 181
        }

        /** 		sequence tmp*/

        /** 		if match_data[i][1] = 0 then*/
        _2 = (int)SEQ_PTR(_match_data_22393);
        _13090 = (int)*(((s1_ptr)_2)->base + _i_22401);
        _2 = (int)SEQ_PTR(_13090);
        _13091 = (int)*(((s1_ptr)_2)->base + 1);
        _13090 = NOVALUE;
        if (binary_op_a(NOTEQ, _13091, 0)){
            _13091 = NOVALUE;
            goto L5; // [87] 101
        }
        _13091 = NOVALUE;

        /** 			tmp = ""*/
        RefDS(_5);
        DeRef(_tmp_22403);
        _tmp_22403 = _5;
        goto L6; // [98] 125
L5: 

        /** 			tmp = haystack[match_data[i][1]..match_data[i][2]]*/
        _2 = (int)SEQ_PTR(_match_data_22393);
        _13093 = (int)*(((s1_ptr)_2)->base + _i_22401);
        _2 = (int)SEQ_PTR(_13093);
        _13094 = (int)*(((s1_ptr)_2)->base + 1);
        _13093 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_22393);
        _13095 = (int)*(((s1_ptr)_2)->base + _i_22401);
        _2 = (int)SEQ_PTR(_13095);
        _13096 = (int)*(((s1_ptr)_2)->base + 2);
        _13095 = NOVALUE;
        rhs_slice_target = (object_ptr)&_tmp_22403;
        RHS_Slice(_haystack_22385, _13094, _13096);
L6: 

        /** 		if str_offsets then*/
        if (_str_offsets_22391 == 0)
        {
            goto L7; // [127] 163
        }
        else{
        }

        /** 			match_data[i] = { tmp, match_data[i][1], match_data[i][2] }*/
        _2 = (int)SEQ_PTR(_match_data_22393);
        _13098 = (int)*(((s1_ptr)_2)->base + _i_22401);
        _2 = (int)SEQ_PTR(_13098);
        _13099 = (int)*(((s1_ptr)_2)->base + 1);
        _13098 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_22393);
        _13100 = (int)*(((s1_ptr)_2)->base + _i_22401);
        _2 = (int)SEQ_PTR(_13100);
        _13101 = (int)*(((s1_ptr)_2)->base + 2);
        _13100 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_tmp_22403);
        *((int *)(_2+4)) = _tmp_22403;
        Ref(_13099);
        *((int *)(_2+8)) = _13099;
        Ref(_13101);
        *((int *)(_2+12)) = _13101;
        _13102 = MAKE_SEQ(_1);
        _13101 = NOVALUE;
        _13099 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_22393);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_22393 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22401);
        _1 = *(int *)_2;
        *(int *)_2 = _13102;
        if( _1 != _13102 ){
            DeRef(_1);
        }
        _13102 = NOVALUE;
        goto L8; // [160] 172
L7: 

        /** 			match_data[i] = tmp*/
        RefDS(_tmp_22403);
        _2 = (int)SEQ_PTR(_match_data_22393);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_22393 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22401);
        _1 = *(int *)_2;
        *(int *)_2 = _tmp_22403;
        DeRef(_1);
L8: 
        DeRef(_tmp_22403);
        _tmp_22403 = NOVALUE;

        /** 	end for*/
        _i_22401 = _i_22401 + 1;
        goto L3; // [176] 75
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_22383);
    DeRef(_haystack_22385);
    DeRef(_options_22387);
    _13094 = NOVALUE;
    _13096 = NOVALUE;
    return _match_data_22393;
    ;
}


int _53split(int _re_22470, int _text_22472, int _from_22473, int _options_22474)
{
    int _13128 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return split_limit(re, text, 0, from, options)*/
    Ref(_re_22470);
    RefDS(_text_22472);
    _13128 = _53split_limit(_re_22470, _text_22472, 0, 1, 0);
    DeRef(_re_22470);
    DeRefDS(_text_22472);
    return _13128;
    ;
}


int _53split_limit(int _re_22479, int _text_22481, int _limit_22482, int _from_22483, int _options_22484)
{
    int _match_data_22488 = NOVALUE;
    int _result_22491 = NOVALUE;
    int _last_22492 = NOVALUE;
    int _a_22503 = NOVALUE;
    int _13154 = NOVALUE;
    int _13153 = NOVALUE;
    int _13152 = NOVALUE;
    int _13150 = NOVALUE;
    int _13148 = NOVALUE;
    int _13147 = NOVALUE;
    int _13146 = NOVALUE;
    int _13145 = NOVALUE;
    int _13144 = NOVALUE;
    int _13141 = NOVALUE;
    int _13140 = NOVALUE;
    int _13139 = NOVALUE;
    int _13136 = NOVALUE;
    int _13135 = NOVALUE;
    int _13133 = NOVALUE;
    int _13131 = NOVALUE;
    int _13129 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _13129 = 0;
    if (_13129 == 0)
    {
        _13129 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _13129 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22484 = _18or_all(0);
L1: 

    /** 	sequence match_data = find_all(re, text, from, options), result*/
    Ref(_re_22479);
    _13131 = _53get_ovector_size(_re_22479, 30);
    Ref(_re_22479);
    Ref(_text_22481);
    Ref(_options_22484);
    _0 = _match_data_22488;
    _match_data_22488 = _53find_all(_re_22479, _text_22481, _from_22483, _options_22484, _13131);
    DeRef(_0);
    _13131 = NOVALUE;

    /** 	integer last = 1*/
    _last_22492 = 1;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _13133 = (_limit_22482 == 0);
    if (_13133 != 0) {
        goto L2; // [48] 64
    }
    if (IS_SEQUENCE(_match_data_22488)){
            _13135 = SEQ_PTR(_match_data_22488)->length;
    }
    else {
        _13135 = 1;
    }
    _13136 = (_limit_22482 > _13135);
    _13135 = NOVALUE;
    if (_13136 == 0)
    {
        DeRef(_13136);
        _13136 = NOVALUE;
        goto L3; // [60] 70
    }
    else{
        DeRef(_13136);
        _13136 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_22488)){
            _limit_22482 = SEQ_PTR(_match_data_22488)->length;
    }
    else {
        _limit_22482 = 1;
    }
L3: 

    /** 	result = repeat(0, limit)*/
    DeRef(_result_22491);
    _result_22491 = Repeat(0, _limit_22482);

    /** 	for i = 1 to limit do*/
    _13139 = _limit_22482;
    {
        int _i_22501;
        _i_22501 = 1;
L4: 
        if (_i_22501 > _13139){
            goto L5; // [81] 164
        }

        /** 		integer a*/

        /** 		a = match_data[i][1][1]*/
        _2 = (int)SEQ_PTR(_match_data_22488);
        _13140 = (int)*(((s1_ptr)_2)->base + _i_22501);
        _2 = (int)SEQ_PTR(_13140);
        _13141 = (int)*(((s1_ptr)_2)->base + 1);
        _13140 = NOVALUE;
        _2 = (int)SEQ_PTR(_13141);
        _a_22503 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_a_22503)){
            _a_22503 = (long)DBL_PTR(_a_22503)->dbl;
        }
        _13141 = NOVALUE;

        /** 		if a = 0 then*/
        if (_a_22503 != 0)
        goto L6; // [108] 121

        /** 			result[i] = ""*/
        RefDS(_5);
        _2 = (int)SEQ_PTR(_result_22491);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_22491 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22501);
        _1 = *(int *)_2;
        *(int *)_2 = _5;
        DeRef(_1);
        goto L7; // [118] 155
L6: 

        /** 			result[i] = text[last..a - 1]*/
        _13144 = _a_22503 - 1;
        rhs_slice_target = (object_ptr)&_13145;
        RHS_Slice(_text_22481, _last_22492, _13144);
        _2 = (int)SEQ_PTR(_result_22491);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_22491 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_22501);
        _1 = *(int *)_2;
        *(int *)_2 = _13145;
        if( _1 != _13145 ){
            DeRef(_1);
        }
        _13145 = NOVALUE;

        /** 			last = match_data[i][1][2] + 1*/
        _2 = (int)SEQ_PTR(_match_data_22488);
        _13146 = (int)*(((s1_ptr)_2)->base + _i_22501);
        _2 = (int)SEQ_PTR(_13146);
        _13147 = (int)*(((s1_ptr)_2)->base + 1);
        _13146 = NOVALUE;
        _2 = (int)SEQ_PTR(_13147);
        _13148 = (int)*(((s1_ptr)_2)->base + 2);
        _13147 = NOVALUE;
        if (IS_ATOM_INT(_13148)) {
            _last_22492 = _13148 + 1;
        }
        else
        { // coercing _last_22492 to an integer 1
            _last_22492 = 1+(long)(DBL_PTR(_13148)->dbl);
            if( !IS_ATOM_INT(_last_22492) ){
                _last_22492 = (object)DBL_PTR(_last_22492)->dbl;
            }
        }
        _13148 = NOVALUE;
L7: 

        /** 	end for*/
        _i_22501 = _i_22501 + 1;
        goto L4; // [159] 88
L5: 
        ;
    }

    /** 	if last < length(text) then*/
    if (IS_SEQUENCE(_text_22481)){
            _13150 = SEQ_PTR(_text_22481)->length;
    }
    else {
        _13150 = 1;
    }
    if (_last_22492 >= _13150)
    goto L8; // [169] 192

    /** 		result &= { text[last..$] }*/
    if (IS_SEQUENCE(_text_22481)){
            _13152 = SEQ_PTR(_text_22481)->length;
    }
    else {
        _13152 = 1;
    }
    rhs_slice_target = (object_ptr)&_13153;
    RHS_Slice(_text_22481, _last_22492, _13152);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13153;
    _13154 = MAKE_SEQ(_1);
    _13153 = NOVALUE;
    Concat((object_ptr)&_result_22491, _result_22491, _13154);
    DeRefDS(_13154);
    _13154 = NOVALUE;
L8: 

    /** 	return result*/
    DeRef(_re_22479);
    DeRef(_text_22481);
    DeRef(_options_22484);
    DeRef(_match_data_22488);
    DeRef(_13133);
    _13133 = NOVALUE;
    DeRef(_13144);
    _13144 = NOVALUE;
    return _result_22491;
    ;
}


int _53find_replace(int _ex_22525, int _text_22527, int _replacement_22528, int _from_22529, int _options_22530)
{
    int _13156 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return find_replace_limit(ex, text, replacement, -1, from, options)*/
    Ref(_ex_22525);
    RefDS(_text_22527);
    RefDS(_replacement_22528);
    _13156 = _53find_replace_limit(_ex_22525, _text_22527, _replacement_22528, -1, 1, 0);
    DeRef(_ex_22525);
    DeRefDS(_text_22527);
    DeRefDS(_replacement_22528);
    return _13156;
    ;
}


int _53find_replace_limit(int _ex_22535, int _text_22537, int _replacement_22538, int _limit_22539, int _from_22540, int _options_22541)
{
    int _13160 = NOVALUE;
    int _13159 = NOVALUE;
    int _13157 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _13157 = 0;
    if (_13157 == 0)
    {
        _13157 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _13157 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    _options_22541 = _18or_all(0);
L1: 

    /**     return machine_func(M_PCRE_REPLACE, { ex, text, replacement, options, */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_22535);
    *((int *)(_2+4)) = _ex_22535;
    Ref(_text_22537);
    *((int *)(_2+8)) = _text_22537;
    RefDS(_replacement_22538);
    *((int *)(_2+12)) = _replacement_22538;
    Ref(_options_22541);
    *((int *)(_2+16)) = _options_22541;
    *((int *)(_2+20)) = _from_22540;
    *((int *)(_2+24)) = _limit_22539;
    _13159 = MAKE_SEQ(_1);
    _13160 = machine(71, _13159);
    DeRefDS(_13159);
    _13159 = NOVALUE;
    DeRef(_ex_22535);
    DeRef(_text_22537);
    DeRefDS(_replacement_22538);
    DeRef(_options_22541);
    return _13160;
    ;
}



// 0x32C9E93A
