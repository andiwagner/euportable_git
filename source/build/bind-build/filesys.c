// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _13find_first_wildcard(int _name_7038, int _from_7039)
{
    int _asterisk_at_7040 = NOVALUE;
    int _question_at_7042 = NOVALUE;
    int _first_wildcard_at_7044 = NOVALUE;
    int _3723 = NOVALUE;
    int _3722 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer asterisk_at = eu:find('*', name, from)*/
    _asterisk_at_7040 = find_from(42, _name_7038, 1);

    /** 	integer question_at = eu:find('?', name, from)*/
    _question_at_7042 = find_from(63, _name_7038, 1);

    /** 	integer first_wildcard_at = asterisk_at*/
    _first_wildcard_at_7044 = _asterisk_at_7040;

    /** 	if asterisk_at or question_at then*/
    if (_asterisk_at_7040 != 0) {
        goto L1; // [26] 35
    }
    if (_question_at_7042 == 0)
    {
        goto L2; // [31] 56
    }
    else{
    }
L1: 

    /** 		if question_at and question_at < asterisk_at then*/
    if (_question_at_7042 == 0) {
        goto L3; // [37] 55
    }
    _3723 = (_question_at_7042 < _asterisk_at_7040);
    if (_3723 == 0)
    {
        DeRef(_3723);
        _3723 = NOVALUE;
        goto L3; // [46] 55
    }
    else{
        DeRef(_3723);
        _3723 = NOVALUE;
    }

    /** 			first_wildcard_at = question_at*/
    _first_wildcard_at_7044 = _question_at_7042;
L3: 
L2: 

    /** 	return first_wildcard_at*/
    DeRefDS(_name_7038);
    return _first_wildcard_at_7044;
    ;
}


int _13dir(int _name_7052)
{
    int _3724 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    _3724 = machine(22, _name_7052);
    DeRefDS(_name_7052);
    return _3724;
    ;
}


int _13current_dir()
{
    int _3726 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    _3726 = machine(23, 0);
    return _3726;
    ;
}


int _13chdir(int _newdir_7060)
{
    int _3727 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CHDIR, newdir)*/
    _3727 = machine(63, _newdir_7060);
    DeRefDS(_newdir_7060);
    return _3727;
    ;
}


int _13delete_file(int _name_7194)
{
    int _pfilename_7195 = NOVALUE;
    int _success_7197 = NOVALUE;
    int _3802 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom pfilename = machine:allocate_string(name)*/
    RefDS(_name_7194);
    _0 = _pfilename_7195;
    _pfilename_7195 = _4allocate_string(_name_7194, 0);
    DeRef(_0);

    /** 	integer success = c_func(xDeleteFile, {pfilename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pfilename_7195);
    *((int *)(_2+4)) = _pfilename_7195;
    _3802 = MAKE_SEQ(_1);
    _success_7197 = call_c(1, _13xDeleteFile_6977, _3802);
    DeRefDS(_3802);
    _3802 = NOVALUE;
    if (!IS_ATOM_INT(_success_7197)) {
        _1 = (long)(DBL_PTR(_success_7197)->dbl);
        if (UNIQUE(DBL_PTR(_success_7197)) && (DBL_PTR(_success_7197)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_7197);
        _success_7197 = _1;
    }

    /** 	ifdef UNIX then*/

    /** 	machine:free(pfilename)*/
    Ref(_pfilename_7195);
    _4free(_pfilename_7195);

    /** 	return success*/
    DeRefDS(_name_7194);
    DeRef(_pfilename_7195);
    return _success_7197;
    ;
}


int _13curdir(int _drive_id_7202)
{
    int _lCurDir_7203 = NOVALUE;
    int _lOrigDir_7204 = NOVALUE;
    int _lDrive_7205 = NOVALUE;
    int _current_dir_inlined_current_dir_at_25_7210 = NOVALUE;
    int _chdir_inlined_chdir_at_55_7213 = NOVALUE;
    int _current_dir_inlined_current_dir_at_77_7216 = NOVALUE;
    int _chdir_inlined_chdir_at_109_7223 = NOVALUE;
    int _newdir_inlined_chdir_at_106_7222 = NOVALUE;
    int _3810 = NOVALUE;
    int _3809 = NOVALUE;
    int _3808 = NOVALUE;
    int _3806 = NOVALUE;
    int _3804 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_drive_id_7202)) {
        _1 = (long)(DBL_PTR(_drive_id_7202)->dbl);
        if (UNIQUE(DBL_PTR(_drive_id_7202)) && (DBL_PTR(_drive_id_7202)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_drive_id_7202);
        _drive_id_7202 = _1;
    }

    /** 	ifdef not LINUX then*/

    /** 	    sequence lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_7204);
    _lOrigDir_7204 = _5;

    /** 	    sequence lDrive*/

    /** 	    if t_alpha(drive_id) then*/
    _3804 = _9t_alpha(_drive_id_7202);
    if (_3804 == 0) {
        DeRef(_3804);
        _3804 = NOVALUE;
        goto L1; // [20] 75
    }
    else {
        if (!IS_ATOM_INT(_3804) && DBL_PTR(_3804)->dbl == 0.0){
            DeRef(_3804);
            _3804 = NOVALUE;
            goto L1; // [20] 75
        }
        DeRef(_3804);
        _3804 = NOVALUE;
    }
    DeRef(_3804);
    _3804 = NOVALUE;

    /** 		    lOrigDir =  current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefDSi(_lOrigDir_7204);
    _lOrigDir_7204 = machine(23, 0);

    /** 		    lDrive = "  "*/
    RefDS(_157);
    DeRefi(_lDrive_7205);
    _lDrive_7205 = _157;

    /** 		    lDrive[1] = drive_id*/
    _2 = (int)SEQ_PTR(_lDrive_7205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_7205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _drive_id_7202;

    /** 		    lDrive[2] = ':'*/
    _2 = (int)SEQ_PTR(_lDrive_7205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_7205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    *(int *)_2 = 58;

    /** 		    if chdir(lDrive) = 0 then*/

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_55_7213 = machine(63, _lDrive_7205);
    if (_chdir_inlined_chdir_at_55_7213 != 0)
    goto L2; // [62] 74

    /** 		    	lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_7204);
    _lOrigDir_7204 = _5;
L2: 
L1: 

    /**     lCurDir = current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_lCurDir_7203);
    _lCurDir_7203 = machine(23, 0);

    /** 	ifdef not LINUX then*/

    /** 		if length(lOrigDir) > 0 then*/
    if (IS_SEQUENCE(_lOrigDir_7204)){
            _3806 = SEQ_PTR(_lOrigDir_7204)->length;
    }
    else {
        _3806 = 1;
    }
    if (_3806 <= 0)
    goto L3; // [95] 121

    /** 	    	chdir(lOrigDir[1..2])*/
    rhs_slice_target = (object_ptr)&_3808;
    RHS_Slice(_lOrigDir_7204, 1, 2);
    DeRefi(_newdir_inlined_chdir_at_106_7222);
    _newdir_inlined_chdir_at_106_7222 = _3808;
    _3808 = NOVALUE;

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_109_7223 = machine(63, _newdir_inlined_chdir_at_106_7222);
    DeRefi(_newdir_inlined_chdir_at_106_7222);
    _newdir_inlined_chdir_at_106_7222 = NOVALUE;
L3: 

    /** 	if (lCurDir[$] != SLASH) then*/
    if (IS_SEQUENCE(_lCurDir_7203)){
            _3809 = SEQ_PTR(_lCurDir_7203)->length;
    }
    else {
        _3809 = 1;
    }
    _2 = (int)SEQ_PTR(_lCurDir_7203);
    _3810 = (int)*(((s1_ptr)_2)->base + _3809);
    if (_3810 == 92)
    goto L4; // [130] 141

    /** 		lCurDir &= SLASH*/
    Append(&_lCurDir_7203, _lCurDir_7203, 92);
L4: 

    /** 	return lCurDir*/
    DeRefi(_lOrigDir_7204);
    DeRefi(_lDrive_7205);
    _3810 = NOVALUE;
    return _lCurDir_7203;
    ;
}


int _13pathinfo(int _path_7386, int _std_slash_7387)
{
    int _slash_7388 = NOVALUE;
    int _period_7389 = NOVALUE;
    int _ch_7390 = NOVALUE;
    int _dir_name_7391 = NOVALUE;
    int _file_name_7392 = NOVALUE;
    int _file_ext_7393 = NOVALUE;
    int _file_full_7394 = NOVALUE;
    int _drive_id_7395 = NOVALUE;
    int _from_slash_7435 = NOVALUE;
    int _3939 = NOVALUE;
    int _3932 = NOVALUE;
    int _3931 = NOVALUE;
    int _3928 = NOVALUE;
    int _3927 = NOVALUE;
    int _3925 = NOVALUE;
    int _3924 = NOVALUE;
    int _3921 = NOVALUE;
    int _3920 = NOVALUE;
    int _3918 = NOVALUE;
    int _3914 = NOVALUE;
    int _3912 = NOVALUE;
    int _3911 = NOVALUE;
    int _3910 = NOVALUE;
    int _3909 = NOVALUE;
    int _3907 = NOVALUE;
    int _0, _1, _2;
    

    /** 	dir_name  = ""*/
    RefDS(_5);
    DeRef(_dir_name_7391);
    _dir_name_7391 = _5;

    /** 	file_name = ""*/
    RefDS(_5);
    DeRef(_file_name_7392);
    _file_name_7392 = _5;

    /** 	file_ext  = ""*/
    RefDS(_5);
    DeRef(_file_ext_7393);
    _file_ext_7393 = _5;

    /** 	file_full = ""*/
    RefDS(_5);
    DeRef(_file_full_7394);
    _file_full_7394 = _5;

    /** 	drive_id  = ""*/
    RefDS(_5);
    DeRef(_drive_id_7395);
    _drive_id_7395 = _5;

    /** 	slash = 0*/
    _slash_7388 = 0;

    /** 	period = 0*/
    _period_7389 = 0;

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_7386)){
            _3907 = SEQ_PTR(_path_7386)->length;
    }
    else {
        _3907 = 1;
    }
    {
        int _i_7397;
        _i_7397 = _3907;
L1: 
        if (_i_7397 < 1){
            goto L2; // [55] 122
        }

        /** 		ch = path[i]*/
        _2 = (int)SEQ_PTR(_path_7386);
        _ch_7390 = (int)*(((s1_ptr)_2)->base + _i_7397);
        if (!IS_ATOM_INT(_ch_7390))
        _ch_7390 = (long)DBL_PTR(_ch_7390)->dbl;

        /** 		if period = 0 and ch = '.' then*/
        _3909 = (_period_7389 == 0);
        if (_3909 == 0) {
            goto L3; // [74] 94
        }
        _3911 = (_ch_7390 == 46);
        if (_3911 == 0)
        {
            DeRef(_3911);
            _3911 = NOVALUE;
            goto L3; // [83] 94
        }
        else{
            DeRef(_3911);
            _3911 = NOVALUE;
        }

        /** 			period = i*/
        _period_7389 = _i_7397;
        goto L4; // [91] 115
L3: 

        /** 		elsif eu:find(ch, SLASHES) then*/
        _3912 = find_from(_ch_7390, _13SLASHES_7005, 1);
        if (_3912 == 0)
        {
            _3912 = NOVALUE;
            goto L5; // [101] 114
        }
        else{
            _3912 = NOVALUE;
        }

        /** 			slash = i*/
        _slash_7388 = _i_7397;

        /** 			exit*/
        goto L2; // [111] 122
L5: 
L4: 

        /** 	end for*/
        _i_7397 = _i_7397 + -1;
        goto L1; // [117] 62
L2: 
        ;
    }

    /** 	if slash > 0 then*/
    if (_slash_7388 <= 0)
    goto L6; // [124] 181

    /** 		dir_name = path[1..slash-1]*/
    _3914 = _slash_7388 - 1;
    rhs_slice_target = (object_ptr)&_dir_name_7391;
    RHS_Slice(_path_7386, 1, _3914);

    /** 		ifdef not UNIX then*/

    /** 			ch = eu:find(':', dir_name)*/
    _ch_7390 = find_from(58, _dir_name_7391, 1);

    /** 			if ch != 0 then*/
    if (_ch_7390 == 0)
    goto L7; // [150] 180

    /** 				drive_id = dir_name[1..ch-1]*/
    _3918 = _ch_7390 - 1;
    rhs_slice_target = (object_ptr)&_drive_id_7395;
    RHS_Slice(_dir_name_7391, 1, _3918);

    /** 				dir_name = dir_name[ch+1..$]*/
    _3920 = _ch_7390 + 1;
    if (IS_SEQUENCE(_dir_name_7391)){
            _3921 = SEQ_PTR(_dir_name_7391)->length;
    }
    else {
        _3921 = 1;
    }
    rhs_slice_target = (object_ptr)&_dir_name_7391;
    RHS_Slice(_dir_name_7391, _3920, _3921);
L7: 
L6: 

    /** 	if period > 0 then*/
    if (_period_7389 <= 0)
    goto L8; // [183] 227

    /** 		file_name = path[slash+1..period-1]*/
    _3924 = _slash_7388 + 1;
    if (_3924 > MAXINT){
        _3924 = NewDouble((double)_3924);
    }
    _3925 = _period_7389 - 1;
    rhs_slice_target = (object_ptr)&_file_name_7392;
    RHS_Slice(_path_7386, _3924, _3925);

    /** 		file_ext = path[period+1..$]*/
    _3927 = _period_7389 + 1;
    if (_3927 > MAXINT){
        _3927 = NewDouble((double)_3927);
    }
    if (IS_SEQUENCE(_path_7386)){
            _3928 = SEQ_PTR(_path_7386)->length;
    }
    else {
        _3928 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_ext_7393;
    RHS_Slice(_path_7386, _3927, _3928);

    /** 		file_full = file_name & '.' & file_ext*/
    {
        int concat_list[3];

        concat_list[0] = _file_ext_7393;
        concat_list[1] = 46;
        concat_list[2] = _file_name_7392;
        Concat_N((object_ptr)&_file_full_7394, concat_list, 3);
    }
    goto L9; // [224] 249
L8: 

    /** 		file_name = path[slash+1..$]*/
    _3931 = _slash_7388 + 1;
    if (_3931 > MAXINT){
        _3931 = NewDouble((double)_3931);
    }
    if (IS_SEQUENCE(_path_7386)){
            _3932 = SEQ_PTR(_path_7386)->length;
    }
    else {
        _3932 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_name_7392;
    RHS_Slice(_path_7386, _3931, _3932);

    /** 		file_full = file_name*/
    RefDS(_file_name_7392);
    DeRef(_file_full_7394);
    _file_full_7394 = _file_name_7392;
L9: 

    /** 	if std_slash != 0 then*/
    if (_std_slash_7387 == 0)
    goto LA; // [251] 317

    /** 		if std_slash < 0 then*/
    if (_std_slash_7387 >= 0)
    goto LB; // [257] 293

    /** 			std_slash = SLASH*/
    _std_slash_7387 = 92;

    /** 			ifdef UNIX then*/

    /** 			sequence from_slash = "/"*/
    RefDS(_3702);
    DeRefi(_from_slash_7435);
    _from_slash_7435 = _3702;

    /** 			dir_name = search:match_replace(from_slash, dir_name, std_slash)*/
    RefDS(_from_slash_7435);
    RefDS(_dir_name_7391);
    _0 = _dir_name_7391;
    _dir_name_7391 = _12match_replace(_from_slash_7435, _dir_name_7391, 92, 0);
    DeRefDS(_0);
    DeRefDSi(_from_slash_7435);
    _from_slash_7435 = NOVALUE;
    goto LC; // [290] 316
LB: 

    /** 			dir_name = search:match_replace("\\", dir_name, std_slash)*/
    RefDS(_958);
    RefDS(_dir_name_7391);
    _0 = _dir_name_7391;
    _dir_name_7391 = _12match_replace(_958, _dir_name_7391, _std_slash_7387, 0);
    DeRefDS(_0);

    /** 			dir_name = search:match_replace("/", dir_name, std_slash)*/
    RefDS(_3702);
    RefDS(_dir_name_7391);
    _0 = _dir_name_7391;
    _dir_name_7391 = _12match_replace(_3702, _dir_name_7391, _std_slash_7387, 0);
    DeRefDS(_0);
LC: 
LA: 

    /** 	return {dir_name, file_full, file_name, file_ext, drive_id}*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_dir_name_7391);
    *((int *)(_2+4)) = _dir_name_7391;
    RefDS(_file_full_7394);
    *((int *)(_2+8)) = _file_full_7394;
    RefDS(_file_name_7392);
    *((int *)(_2+12)) = _file_name_7392;
    RefDS(_file_ext_7393);
    *((int *)(_2+16)) = _file_ext_7393;
    RefDS(_drive_id_7395);
    *((int *)(_2+20)) = _drive_id_7395;
    _3939 = MAKE_SEQ(_1);
    DeRefDS(_path_7386);
    DeRefDS(_dir_name_7391);
    DeRefDS(_file_name_7392);
    DeRefDS(_file_ext_7393);
    DeRefDS(_file_full_7394);
    DeRefDS(_drive_id_7395);
    DeRef(_3909);
    _3909 = NOVALUE;
    DeRef(_3914);
    _3914 = NOVALUE;
    DeRef(_3918);
    _3918 = NOVALUE;
    DeRef(_3920);
    _3920 = NOVALUE;
    DeRef(_3924);
    _3924 = NOVALUE;
    DeRef(_3925);
    _3925 = NOVALUE;
    DeRef(_3927);
    _3927 = NOVALUE;
    DeRef(_3931);
    _3931 = NOVALUE;
    return _3939;
    ;
}


int _13dirname(int _path_7443, int _pcd_7444)
{
    int _data_7445 = NOVALUE;
    int _3944 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7443);
    _0 = _data_7445;
    _data_7445 = _13pathinfo(_path_7443, 0);
    DeRef(_0);

    /** 	if pcd then*/

    /** 	return data[1]*/
    _2 = (int)SEQ_PTR(_data_7445);
    _3944 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3944);
    DeRefDS(_path_7443);
    DeRefDS(_data_7445);
    return _3944;
    ;
}


int _13pathname(int _path_7455)
{
    int _data_7456 = NOVALUE;
    int _stop_7457 = NOVALUE;
    int _3949 = NOVALUE;
    int _3948 = NOVALUE;
    int _3946 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = canonical_path(path)*/
    RefDS(_path_7455);
    _0 = _data_7456;
    _data_7456 = _13canonical_path(_path_7455, 0, 0);
    DeRef(_0);

    /** 	stop = search:rfind(SLASH, data)*/
    if (IS_SEQUENCE(_data_7456)){
            _3946 = SEQ_PTR(_data_7456)->length;
    }
    else {
        _3946 = 1;
    }
    RefDS(_data_7456);
    _stop_7457 = _12rfind(92, _data_7456, _3946);
    _3946 = NOVALUE;
    if (!IS_ATOM_INT(_stop_7457)) {
        _1 = (long)(DBL_PTR(_stop_7457)->dbl);
        if (UNIQUE(DBL_PTR(_stop_7457)) && (DBL_PTR(_stop_7457)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stop_7457);
        _stop_7457 = _1;
    }

    /** 	return data[1 .. stop - 1]*/
    _3948 = _stop_7457 - 1;
    rhs_slice_target = (object_ptr)&_3949;
    RHS_Slice(_data_7456, 1, _3948);
    DeRefDS(_path_7455);
    DeRefDS(_data_7456);
    _3948 = NOVALUE;
    return _3949;
    ;
}


int _13filebase(int _path_7472)
{
    int _data_7473 = NOVALUE;
    int _3953 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7472);
    _0 = _data_7473;
    _data_7473 = _13pathinfo(_path_7472, 0);
    DeRef(_0);

    /** 	return data[3]*/
    _2 = (int)SEQ_PTR(_data_7473);
    _3953 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_3953);
    DeRefDS(_path_7472);
    DeRefDS(_data_7473);
    return _3953;
    ;
}


int _13fileext(int _path_7478)
{
    int _data_7479 = NOVALUE;
    int _3955 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7478);
    _0 = _data_7479;
    _data_7479 = _13pathinfo(_path_7478, 0);
    DeRef(_0);

    /** 	return data[4]*/
    _2 = (int)SEQ_PTR(_data_7479);
    _3955 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_3955);
    DeRefDS(_path_7478);
    DeRefDS(_data_7479);
    return _3955;
    ;
}


int _13defaultext(int _path_7490, int _defext_7491)
{
    int _3970 = NOVALUE;
    int _3967 = NOVALUE;
    int _3965 = NOVALUE;
    int _3964 = NOVALUE;
    int _3963 = NOVALUE;
    int _3961 = NOVALUE;
    int _3960 = NOVALUE;
    int _3958 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(defext) = 0 then*/
    _3958 = 3;

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_7490)){
            _3960 = SEQ_PTR(_path_7490)->length;
    }
    else {
        _3960 = 1;
    }
    {
        int _i_7496;
        _i_7496 = _3960;
L1: 
        if (_i_7496 < 1){
            goto L2; // [26] 95
        }

        /** 		if path[i] = '.' then*/
        _2 = (int)SEQ_PTR(_path_7490);
        _3961 = (int)*(((s1_ptr)_2)->base + _i_7496);
        if (binary_op_a(NOTEQ, _3961, 46)){
            _3961 = NOVALUE;
            goto L3; // [39] 50
        }
        _3961 = NOVALUE;

        /** 			return path*/
        DeRefDSi(_defext_7491);
        return _path_7490;
L3: 

        /** 		if find(path[i], SLASHES) then*/
        _2 = (int)SEQ_PTR(_path_7490);
        _3963 = (int)*(((s1_ptr)_2)->base + _i_7496);
        _3964 = find_from(_3963, _13SLASHES_7005, 1);
        _3963 = NOVALUE;
        if (_3964 == 0)
        {
            _3964 = NOVALUE;
            goto L4; // [61] 88
        }
        else{
            _3964 = NOVALUE;
        }

        /** 			if i = length(path) then*/
        if (IS_SEQUENCE(_path_7490)){
                _3965 = SEQ_PTR(_path_7490)->length;
        }
        else {
            _3965 = 1;
        }
        if (_i_7496 != _3965)
        goto L2; // [69] 95

        /** 				return path*/
        DeRefDSi(_defext_7491);
        return _path_7490;
        goto L5; // [79] 87

        /** 				exit*/
        goto L2; // [84] 95
L5: 
L4: 

        /** 	end for*/
        _i_7496 = _i_7496 + -1;
        goto L1; // [90] 33
L2: 
        ;
    }

    /** 	if defext[1] != '.' then*/
    _2 = (int)SEQ_PTR(_defext_7491);
    _3967 = (int)*(((s1_ptr)_2)->base + 1);
    if (_3967 == 46)
    goto L6; // [101] 112

    /** 		path &= '.'*/
    Append(&_path_7490, _path_7490, 46);
L6: 

    /** 	return path & defext*/
    Concat((object_ptr)&_3970, _path_7490, _defext_7491);
    DeRefDS(_path_7490);
    DeRefDSi(_defext_7491);
    _3967 = NOVALUE;
    return _3970;
    ;
}


int _13absolute_path(int _filename_7515)
{
    int _3982 = NOVALUE;
    int _3981 = NOVALUE;
    int _3979 = NOVALUE;
    int _3977 = NOVALUE;
    int _3975 = NOVALUE;
    int _3974 = NOVALUE;
    int _3973 = NOVALUE;
    int _3971 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(filename) = 0 then*/
    if (IS_SEQUENCE(_filename_7515)){
            _3971 = SEQ_PTR(_filename_7515)->length;
    }
    else {
        _3971 = 1;
    }
    if (_3971 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRefDS(_filename_7515);
    return 0;
L1: 

    /** 	if eu:find(filename[1], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_7515);
    _3973 = (int)*(((s1_ptr)_2)->base + 1);
    _3974 = find_from(_3973, _13SLASHES_7005, 1);
    _3973 = NOVALUE;
    if (_3974 == 0)
    {
        _3974 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _3974 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_filename_7515);
    return 1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 1 then*/
    if (IS_SEQUENCE(_filename_7515)){
            _3975 = SEQ_PTR(_filename_7515)->length;
    }
    else {
        _3975 = 1;
    }
    if (_3975 != 1)
    goto L3; // [47] 58

    /** 			return 0*/
    DeRefDS(_filename_7515);
    return 0;
L3: 

    /** 		if filename[2] != ':' then*/
    _2 = (int)SEQ_PTR(_filename_7515);
    _3977 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(EQUALS, _3977, 58)){
        _3977 = NOVALUE;
        goto L4; // [64] 75
    }
    _3977 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_filename_7515);
    return 0;
L4: 

    /** 		if length(filename) < 3 then*/
    if (IS_SEQUENCE(_filename_7515)){
            _3979 = SEQ_PTR(_filename_7515)->length;
    }
    else {
        _3979 = 1;
    }
    if (_3979 >= 3)
    goto L5; // [80] 91

    /** 			return 0*/
    DeRefDS(_filename_7515);
    return 0;
L5: 

    /** 		if eu:find(filename[3], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_7515);
    _3981 = (int)*(((s1_ptr)_2)->base + 3);
    _3982 = find_from(_3981, _13SLASHES_7005, 1);
    _3981 = NOVALUE;
    if (_3982 == 0)
    {
        _3982 = NOVALUE;
        goto L6; // [102] 112
    }
    else{
        _3982 = NOVALUE;
    }

    /** 			return 1*/
    DeRefDS(_filename_7515);
    return 1;
L6: 

    /** 	return 0*/
    DeRefDS(_filename_7515);
    return 0;
    ;
}


int _13canonical_path(int _path_in_7554, int _directory_given_7555, int _case_flags_7556)
{
    int _lPath_7557 = NOVALUE;
    int _lPosA_7558 = NOVALUE;
    int _lPosB_7559 = NOVALUE;
    int _lLevel_7560 = NOVALUE;
    int _lHome_7561 = NOVALUE;
    int _lDrive_7562 = NOVALUE;
    int _current_dir_inlined_current_dir_at_300_7622 = NOVALUE;
    int _driveid_inlined_driveid_at_307_7625 = NOVALUE;
    int _data_inlined_driveid_at_307_7624 = NOVALUE;
    int _wildcard_suffix_7627 = NOVALUE;
    int _first_wildcard_at_7628 = NOVALUE;
    int _last_slash_7631 = NOVALUE;
    int _sl_7703 = NOVALUE;
    int _short_name_7706 = NOVALUE;
    int _correct_name_7709 = NOVALUE;
    int _lower_name_7712 = NOVALUE;
    int _part_7728 = NOVALUE;
    int _list_7733 = NOVALUE;
    int _dir_inlined_dir_at_899_7737 = NOVALUE;
    int _name_inlined_dir_at_896_7736 = NOVALUE;
    int _supplied_name_7738 = NOVALUE;
    int _read_name_7757 = NOVALUE;
    int _read_name_7782 = NOVALUE;
    int _4204 = NOVALUE;
    int _4201 = NOVALUE;
    int _4197 = NOVALUE;
    int _4196 = NOVALUE;
    int _4194 = NOVALUE;
    int _4193 = NOVALUE;
    int _4192 = NOVALUE;
    int _4191 = NOVALUE;
    int _4190 = NOVALUE;
    int _4188 = NOVALUE;
    int _4187 = NOVALUE;
    int _4186 = NOVALUE;
    int _4185 = NOVALUE;
    int _4184 = NOVALUE;
    int _4183 = NOVALUE;
    int _4182 = NOVALUE;
    int _4181 = NOVALUE;
    int _4179 = NOVALUE;
    int _4178 = NOVALUE;
    int _4177 = NOVALUE;
    int _4176 = NOVALUE;
    int _4175 = NOVALUE;
    int _4174 = NOVALUE;
    int _4173 = NOVALUE;
    int _4172 = NOVALUE;
    int _4171 = NOVALUE;
    int _4169 = NOVALUE;
    int _4168 = NOVALUE;
    int _4167 = NOVALUE;
    int _4166 = NOVALUE;
    int _4165 = NOVALUE;
    int _4164 = NOVALUE;
    int _4163 = NOVALUE;
    int _4162 = NOVALUE;
    int _4161 = NOVALUE;
    int _4160 = NOVALUE;
    int _4159 = NOVALUE;
    int _4158 = NOVALUE;
    int _4157 = NOVALUE;
    int _4156 = NOVALUE;
    int _4155 = NOVALUE;
    int _4153 = NOVALUE;
    int _4152 = NOVALUE;
    int _4151 = NOVALUE;
    int _4150 = NOVALUE;
    int _4149 = NOVALUE;
    int _4147 = NOVALUE;
    int _4146 = NOVALUE;
    int _4145 = NOVALUE;
    int _4144 = NOVALUE;
    int _4143 = NOVALUE;
    int _4142 = NOVALUE;
    int _4141 = NOVALUE;
    int _4140 = NOVALUE;
    int _4139 = NOVALUE;
    int _4138 = NOVALUE;
    int _4137 = NOVALUE;
    int _4136 = NOVALUE;
    int _4135 = NOVALUE;
    int _4133 = NOVALUE;
    int _4132 = NOVALUE;
    int _4130 = NOVALUE;
    int _4129 = NOVALUE;
    int _4128 = NOVALUE;
    int _4127 = NOVALUE;
    int _4126 = NOVALUE;
    int _4124 = NOVALUE;
    int _4123 = NOVALUE;
    int _4122 = NOVALUE;
    int _4121 = NOVALUE;
    int _4120 = NOVALUE;
    int _4119 = NOVALUE;
    int _4117 = NOVALUE;
    int _4116 = NOVALUE;
    int _4115 = NOVALUE;
    int _4113 = NOVALUE;
    int _4112 = NOVALUE;
    int _4110 = NOVALUE;
    int _4109 = NOVALUE;
    int _4108 = NOVALUE;
    int _4106 = NOVALUE;
    int _4105 = NOVALUE;
    int _4103 = NOVALUE;
    int _4101 = NOVALUE;
    int _4099 = NOVALUE;
    int _4092 = NOVALUE;
    int _4089 = NOVALUE;
    int _4088 = NOVALUE;
    int _4087 = NOVALUE;
    int _4086 = NOVALUE;
    int _4080 = NOVALUE;
    int _4076 = NOVALUE;
    int _4075 = NOVALUE;
    int _4074 = NOVALUE;
    int _4073 = NOVALUE;
    int _4072 = NOVALUE;
    int _4070 = NOVALUE;
    int _4067 = NOVALUE;
    int _4066 = NOVALUE;
    int _4065 = NOVALUE;
    int _4064 = NOVALUE;
    int _4063 = NOVALUE;
    int _4062 = NOVALUE;
    int _4060 = NOVALUE;
    int _4059 = NOVALUE;
    int _4057 = NOVALUE;
    int _4055 = NOVALUE;
    int _4054 = NOVALUE;
    int _4053 = NOVALUE;
    int _4051 = NOVALUE;
    int _4050 = NOVALUE;
    int _4049 = NOVALUE;
    int _4048 = NOVALUE;
    int _4046 = NOVALUE;
    int _4044 = NOVALUE;
    int _4039 = NOVALUE;
    int _4037 = NOVALUE;
    int _4036 = NOVALUE;
    int _4035 = NOVALUE;
    int _4034 = NOVALUE;
    int _4033 = NOVALUE;
    int _4031 = NOVALUE;
    int _4030 = NOVALUE;
    int _4028 = NOVALUE;
    int _4027 = NOVALUE;
    int _4026 = NOVALUE;
    int _4025 = NOVALUE;
    int _4024 = NOVALUE;
    int _4023 = NOVALUE;
    int _4022 = NOVALUE;
    int _4019 = NOVALUE;
    int _4018 = NOVALUE;
    int _4016 = NOVALUE;
    int _4014 = NOVALUE;
    int _4012 = NOVALUE;
    int _4009 = NOVALUE;
    int _4008 = NOVALUE;
    int _4007 = NOVALUE;
    int _4006 = NOVALUE;
    int _4005 = NOVALUE;
    int _4003 = NOVALUE;
    int _4002 = NOVALUE;
    int _4001 = NOVALUE;
    int _4000 = NOVALUE;
    int _3999 = NOVALUE;
    int _3998 = NOVALUE;
    int _3997 = NOVALUE;
    int _3996 = NOVALUE;
    int _3995 = NOVALUE;
    int _3994 = NOVALUE;
    int _3993 = NOVALUE;
    int _0, _1, _2;
    

    /**     sequence lPath = ""*/
    RefDS(_5);
    DeRef(_lPath_7557);
    _lPath_7557 = _5;

    /**     integer lPosA = -1*/
    _lPosA_7558 = -1;

    /**     integer lPosB = -1*/
    _lPosB_7559 = -1;

    /**     sequence lLevel = ""*/
    RefDS(_5);
    DeRefi(_lLevel_7560);
    _lLevel_7560 = _5;

    /**     path_in = path_in*/
    RefDS(_path_in_7554);
    DeRefDS(_path_in_7554);
    _path_in_7554 = _path_in_7554;

    /** 	ifdef UNIX then*/

    /** 	    sequence lDrive*/

    /** 	    lPath = match_replace("/", path_in, SLASH)*/
    RefDS(_3702);
    RefDS(_path_in_7554);
    _0 = _lPath_7557;
    _lPath_7557 = _12match_replace(_3702, _path_in_7554, 92, 0);
    DeRefDS(_0);

    /**     if (length(lPath) > 2 and lPath[1] = '"' and lPath[$] = '"') then*/
    if (IS_SEQUENCE(_lPath_7557)){
            _3993 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _3993 = 1;
    }
    _3994 = (_3993 > 2);
    _3993 = NOVALUE;
    if (_3994 == 0) {
        _3995 = 0;
        goto L1; // [62] 78
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _3996 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3996)) {
        _3997 = (_3996 == 34);
    }
    else {
        _3997 = binary_op(EQUALS, _3996, 34);
    }
    _3996 = NOVALUE;
    if (IS_ATOM_INT(_3997))
    _3995 = (_3997 != 0);
    else
    _3995 = DBL_PTR(_3997)->dbl != 0.0;
L1: 
    if (_3995 == 0) {
        DeRef(_3998);
        _3998 = 0;
        goto L2; // [78] 97
    }
    if (IS_SEQUENCE(_lPath_7557)){
            _3999 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _3999 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4000 = (int)*(((s1_ptr)_2)->base + _3999);
    if (IS_ATOM_INT(_4000)) {
        _4001 = (_4000 == 34);
    }
    else {
        _4001 = binary_op(EQUALS, _4000, 34);
    }
    _4000 = NOVALUE;
    if (IS_ATOM_INT(_4001))
    _3998 = (_4001 != 0);
    else
    _3998 = DBL_PTR(_4001)->dbl != 0.0;
L2: 
    if (_3998 == 0)
    {
        _3998 = NOVALUE;
        goto L3; // [97] 115
    }
    else{
        _3998 = NOVALUE;
    }

    /**         lPath = lPath[2..$-1]*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4002 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4002 = 1;
    }
    _4003 = _4002 - 1;
    _4002 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lPath_7557;
    RHS_Slice(_lPath_7557, 2, _4003);
L3: 

    /**     if (length(lPath) > 0 and lPath[1] = '~') then*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4005 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4005 = 1;
    }
    _4006 = (_4005 > 0);
    _4005 = NOVALUE;
    if (_4006 == 0) {
        DeRef(_4007);
        _4007 = 0;
        goto L4; // [124] 140
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4008 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4008)) {
        _4009 = (_4008 == 126);
    }
    else {
        _4009 = binary_op(EQUALS, _4008, 126);
    }
    _4008 = NOVALUE;
    if (IS_ATOM_INT(_4009))
    _4007 = (_4009 != 0);
    else
    _4007 = DBL_PTR(_4009)->dbl != 0.0;
L4: 
    if (_4007 == 0)
    {
        _4007 = NOVALUE;
        goto L5; // [140] 249
    }
    else{
        _4007 = NOVALUE;
    }

    /** 		lHome = getenv("HOME")*/
    DeRef(_lHome_7561);
    _lHome_7561 = EGetEnv(_4010);

    /** 		ifdef WINDOWS then*/

    /** 			if atom(lHome) then*/
    _4012 = IS_ATOM(_lHome_7561);
    if (_4012 == 0)
    {
        _4012 = NOVALUE;
        goto L6; // [155] 171
    }
    else{
        _4012 = NOVALUE;
    }

    /** 				lHome = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _4014 = EGetEnv(_4013);
    _4016 = EGetEnv(_4015);
    if (IS_SEQUENCE(_4014) && IS_ATOM(_4016)) {
        Ref(_4016);
        Append(&_lHome_7561, _4014, _4016);
    }
    else if (IS_ATOM(_4014) && IS_SEQUENCE(_4016)) {
        Ref(_4014);
        Prepend(&_lHome_7561, _4016, _4014);
    }
    else {
        Concat((object_ptr)&_lHome_7561, _4014, _4016);
        DeRef(_4014);
        _4014 = NOVALUE;
    }
    DeRef(_4014);
    _4014 = NOVALUE;
    DeRef(_4016);
    _4016 = NOVALUE;
L6: 

    /** 		if lHome[$] != SLASH then*/
    if (IS_SEQUENCE(_lHome_7561)){
            _4018 = SEQ_PTR(_lHome_7561)->length;
    }
    else {
        _4018 = 1;
    }
    _2 = (int)SEQ_PTR(_lHome_7561);
    _4019 = (int)*(((s1_ptr)_2)->base + _4018);
    if (binary_op_a(EQUALS, _4019, 92)){
        _4019 = NOVALUE;
        goto L7; // [180] 191
    }
    _4019 = NOVALUE;

    /** 			lHome &= SLASH*/
    if (IS_SEQUENCE(_lHome_7561) && IS_ATOM(92)) {
        Append(&_lHome_7561, _lHome_7561, 92);
    }
    else if (IS_ATOM(_lHome_7561) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_lHome_7561, _lHome_7561, 92);
    }
L7: 

    /** 		if length(lPath) > 1 and lPath[2] = SLASH then*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4022 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4022 = 1;
    }
    _4023 = (_4022 > 1);
    _4022 = NOVALUE;
    if (_4023 == 0) {
        goto L8; // [200] 233
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4025 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4025)) {
        _4026 = (_4025 == 92);
    }
    else {
        _4026 = binary_op(EQUALS, _4025, 92);
    }
    _4025 = NOVALUE;
    if (_4026 == 0) {
        DeRef(_4026);
        _4026 = NOVALUE;
        goto L8; // [213] 233
    }
    else {
        if (!IS_ATOM_INT(_4026) && DBL_PTR(_4026)->dbl == 0.0){
            DeRef(_4026);
            _4026 = NOVALUE;
            goto L8; // [213] 233
        }
        DeRef(_4026);
        _4026 = NOVALUE;
    }
    DeRef(_4026);
    _4026 = NOVALUE;

    /** 			lPath = lHome & lPath[3 .. $]*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4027 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4027 = 1;
    }
    rhs_slice_target = (object_ptr)&_4028;
    RHS_Slice(_lPath_7557, 3, _4027);
    if (IS_SEQUENCE(_lHome_7561) && IS_ATOM(_4028)) {
    }
    else if (IS_ATOM(_lHome_7561) && IS_SEQUENCE(_4028)) {
        Ref(_lHome_7561);
        Prepend(&_lPath_7557, _4028, _lHome_7561);
    }
    else {
        Concat((object_ptr)&_lPath_7557, _lHome_7561, _4028);
    }
    DeRefDS(_4028);
    _4028 = NOVALUE;
    goto L9; // [230] 248
L8: 

    /** 			lPath = lHome & lPath[2 .. $]*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4030 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4030 = 1;
    }
    rhs_slice_target = (object_ptr)&_4031;
    RHS_Slice(_lPath_7557, 2, _4030);
    if (IS_SEQUENCE(_lHome_7561) && IS_ATOM(_4031)) {
    }
    else if (IS_ATOM(_lHome_7561) && IS_SEQUENCE(_4031)) {
        Ref(_lHome_7561);
        Prepend(&_lPath_7557, _4031, _lHome_7561);
    }
    else {
        Concat((object_ptr)&_lPath_7557, _lHome_7561, _4031);
    }
    DeRefDS(_4031);
    _4031 = NOVALUE;
L9: 
L5: 

    /** 	ifdef WINDOWS then*/

    /** 	    if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4033 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4033 = 1;
    }
    _4034 = (_4033 > 1);
    _4033 = NOVALUE;
    if (_4034 == 0) {
        DeRef(_4035);
        _4035 = 0;
        goto LA; // [260] 276
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4036 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4036)) {
        _4037 = (_4036 == 58);
    }
    else {
        _4037 = binary_op(EQUALS, _4036, 58);
    }
    _4036 = NOVALUE;
    if (IS_ATOM_INT(_4037))
    _4035 = (_4037 != 0);
    else
    _4035 = DBL_PTR(_4037)->dbl != 0.0;
LA: 
    if (_4035 == 0)
    {
        _4035 = NOVALUE;
        goto LB; // [276] 299
    }
    else{
        _4035 = NOVALUE;
    }

    /** 			lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_7562;
    RHS_Slice(_lPath_7557, 1, 2);

    /** 			lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4039 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4039 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_7557;
    RHS_Slice(_lPath_7557, 3, _4039);
    goto LC; // [296] 333
LB: 

    /** 			lDrive = driveid(current_dir()) & ':'*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_current_dir_inlined_current_dir_at_300_7622);
    _current_dir_inlined_current_dir_at_300_7622 = machine(23, 0);

    /** 	data = pathinfo(path)*/
    RefDS(_current_dir_inlined_current_dir_at_300_7622);
    _0 = _data_inlined_driveid_at_307_7624;
    _data_inlined_driveid_at_307_7624 = _13pathinfo(_current_dir_inlined_current_dir_at_300_7622, 0);
    DeRef(_0);

    /** 	return data[5]*/
    DeRef(_driveid_inlined_driveid_at_307_7625);
    _2 = (int)SEQ_PTR(_data_inlined_driveid_at_307_7624);
    _driveid_inlined_driveid_at_307_7625 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_driveid_inlined_driveid_at_307_7625);
    DeRef(_data_inlined_driveid_at_307_7624);
    _data_inlined_driveid_at_307_7624 = NOVALUE;
    if (IS_SEQUENCE(_driveid_inlined_driveid_at_307_7625) && IS_ATOM(58)) {
        Append(&_lDrive_7562, _driveid_inlined_driveid_at_307_7625, 58);
    }
    else if (IS_ATOM(_driveid_inlined_driveid_at_307_7625) && IS_SEQUENCE(58)) {
    }
    else {
        Concat((object_ptr)&_lDrive_7562, _driveid_inlined_driveid_at_307_7625, 58);
    }
LC: 

    /** 	sequence wildcard_suffix*/

    /** 	integer first_wildcard_at = find_first_wildcard( lPath )*/
    RefDS(_lPath_7557);
    _first_wildcard_at_7628 = _13find_first_wildcard(_lPath_7557, 1);
    if (!IS_ATOM_INT(_first_wildcard_at_7628)) {
        _1 = (long)(DBL_PTR(_first_wildcard_at_7628)->dbl);
        if (UNIQUE(DBL_PTR(_first_wildcard_at_7628)) && (DBL_PTR(_first_wildcard_at_7628)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_first_wildcard_at_7628);
        _first_wildcard_at_7628 = _1;
    }

    /** 	if first_wildcard_at then*/
    if (_first_wildcard_at_7628 == 0)
    {
        goto LD; // [346] 407
    }
    else{
    }

    /** 		integer last_slash = search:rfind( SLASH, lPath, first_wildcard_at )*/
    RefDS(_lPath_7557);
    _last_slash_7631 = _12rfind(92, _lPath_7557, _first_wildcard_at_7628);
    if (!IS_ATOM_INT(_last_slash_7631)) {
        _1 = (long)(DBL_PTR(_last_slash_7631)->dbl);
        if (UNIQUE(DBL_PTR(_last_slash_7631)) && (DBL_PTR(_last_slash_7631)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_slash_7631);
        _last_slash_7631 = _1;
    }

    /** 		if last_slash then*/
    if (_last_slash_7631 == 0)
    {
        goto LE; // [361] 387
    }
    else{
    }

    /** 			wildcard_suffix = lPath[last_slash..$]*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4044 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4044 = 1;
    }
    rhs_slice_target = (object_ptr)&_wildcard_suffix_7627;
    RHS_Slice(_lPath_7557, _last_slash_7631, _4044);

    /** 			lPath = remove( lPath, last_slash, length( lPath ) )*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4046 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4046 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7557);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_last_slash_7631)) ? _last_slash_7631 : (long)(DBL_PTR(_last_slash_7631)->dbl);
        int stop = (IS_ATOM_INT(_4046)) ? _4046 : (long)(DBL_PTR(_4046)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7557), start, &_lPath_7557 );
            }
            else Tail(SEQ_PTR(_lPath_7557), stop+1, &_lPath_7557);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7557), start, &_lPath_7557);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7557 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7557)->ref == 1));
        }
    }
    _4046 = NOVALUE;
    goto LF; // [384] 402
LE: 

    /** 			wildcard_suffix = lPath*/
    RefDS(_lPath_7557);
    DeRef(_wildcard_suffix_7627);
    _wildcard_suffix_7627 = _lPath_7557;

    /** 			lPath = ""*/
    RefDS(_5);
    DeRefDS(_lPath_7557);
    _lPath_7557 = _5;
LF: 
    goto L10; // [404] 415
LD: 

    /** 		wildcard_suffix = ""*/
    RefDS(_5);
    DeRef(_wildcard_suffix_7627);
    _wildcard_suffix_7627 = _5;
L10: 

    /** 	if ((length(lPath) = 0) or not find(lPath[1], "/\\")) then*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4048 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4048 = 1;
    }
    _4049 = (_4048 == 0);
    _4048 = NOVALUE;
    if (_4049 != 0) {
        DeRef(_4050);
        _4050 = 1;
        goto L11; // [424] 444
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4051 = (int)*(((s1_ptr)_2)->base + 1);
    _4053 = find_from(_4051, _4052, 1);
    _4051 = NOVALUE;
    _4054 = (_4053 == 0);
    _4053 = NOVALUE;
    _4050 = (_4054 != 0);
L11: 
    if (_4050 == 0)
    {
        _4050 = NOVALUE;
        goto L12; // [444] 545
    }
    else{
        _4050 = NOVALUE;
    }

    /** 		ifdef UNIX then*/

    /** 			if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_7562)){
            _4055 = SEQ_PTR(_lDrive_7562)->length;
    }
    else {
        _4055 = 1;
    }
    if (_4055 != 0)
    goto L13; // [456] 473

    /** 				lPath = curdir() & lPath*/
    _4057 = _13curdir(0);
    if (IS_SEQUENCE(_4057) && IS_ATOM(_lPath_7557)) {
    }
    else if (IS_ATOM(_4057) && IS_SEQUENCE(_lPath_7557)) {
        Ref(_4057);
        Prepend(&_lPath_7557, _lPath_7557, _4057);
    }
    else {
        Concat((object_ptr)&_lPath_7557, _4057, _lPath_7557);
        DeRef(_4057);
        _4057 = NOVALUE;
    }
    DeRef(_4057);
    _4057 = NOVALUE;
    goto L14; // [470] 488
L13: 

    /** 				lPath = curdir(lDrive[1]) & lPath*/
    _2 = (int)SEQ_PTR(_lDrive_7562);
    _4059 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_4059);
    _4060 = _13curdir(_4059);
    _4059 = NOVALUE;
    if (IS_SEQUENCE(_4060) && IS_ATOM(_lPath_7557)) {
    }
    else if (IS_ATOM(_4060) && IS_SEQUENCE(_lPath_7557)) {
        Ref(_4060);
        Prepend(&_lPath_7557, _lPath_7557, _4060);
    }
    else {
        Concat((object_ptr)&_lPath_7557, _4060, _lPath_7557);
        DeRef(_4060);
        _4060 = NOVALUE;
    }
    DeRef(_4060);
    _4060 = NOVALUE;
L14: 

    /** 			if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4062 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4062 = 1;
    }
    _4063 = (_4062 > 1);
    _4062 = NOVALUE;
    if (_4063 == 0) {
        DeRef(_4064);
        _4064 = 0;
        goto L15; // [497] 513
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4065 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4065)) {
        _4066 = (_4065 == 58);
    }
    else {
        _4066 = binary_op(EQUALS, _4065, 58);
    }
    _4065 = NOVALUE;
    if (IS_ATOM_INT(_4066))
    _4064 = (_4066 != 0);
    else
    _4064 = DBL_PTR(_4066)->dbl != 0.0;
L15: 
    if (_4064 == 0)
    {
        _4064 = NOVALUE;
        goto L16; // [513] 544
    }
    else{
        _4064 = NOVALUE;
    }

    /** 				if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_7562)){
            _4067 = SEQ_PTR(_lDrive_7562)->length;
    }
    else {
        _4067 = 1;
    }
    if (_4067 != 0)
    goto L17; // [521] 533

    /** 					lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_7562;
    RHS_Slice(_lPath_7557, 1, 2);
L17: 

    /** 				lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4070 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4070 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_7557;
    RHS_Slice(_lPath_7557, 3, _4070);
L16: 
L12: 

    /** 	if ((directory_given != 0) and (lPath[$] != SLASH) ) then*/
    _4072 = (_directory_given_7555 != 0);
    if (_4072 == 0) {
        DeRef(_4073);
        _4073 = 0;
        goto L18; // [551] 570
    }
    if (IS_SEQUENCE(_lPath_7557)){
            _4074 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4074 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4075 = (int)*(((s1_ptr)_2)->base + _4074);
    if (IS_ATOM_INT(_4075)) {
        _4076 = (_4075 != 92);
    }
    else {
        _4076 = binary_op(NOTEQ, _4075, 92);
    }
    _4075 = NOVALUE;
    if (IS_ATOM_INT(_4076))
    _4073 = (_4076 != 0);
    else
    _4073 = DBL_PTR(_4076)->dbl != 0.0;
L18: 
    if (_4073 == 0)
    {
        _4073 = NOVALUE;
        goto L19; // [570] 580
    }
    else{
        _4073 = NOVALUE;
    }

    /** 		lPath &= SLASH*/
    Append(&_lPath_7557, _lPath_7557, 92);
L19: 

    /** 	lLevel = SLASH & '.' & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = 46;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_7560, concat_list, 3);
    }

    /** 	lPosA = 1*/
    _lPosA_7558 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1A; // [595] 616
L1B: 
    if (_lPosA_7558 == 0)
    goto L1C; // [598] 628

    /** 		lPath = eu:remove(lPath, lPosA, lPosA + 1)*/
    _4080 = _lPosA_7558 + 1;
    if (_4080 > MAXINT){
        _4080 = NewDouble((double)_4080);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7557);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosA_7558)) ? _lPosA_7558 : (long)(DBL_PTR(_lPosA_7558)->dbl);
        int stop = (IS_ATOM_INT(_4080)) ? _4080 : (long)(DBL_PTR(_4080)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7557), start, &_lPath_7557 );
            }
            else Tail(SEQ_PTR(_lPath_7557), stop+1, &_lPath_7557);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7557), start, &_lPath_7557);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7557 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7557)->ref == 1));
        }
    }
    DeRef(_4080);
    _4080 = NOVALUE;

    /** 	  entry*/
L1A: 

    /** 		lPosA = match(lLevel, lPath, lPosA )*/
    _lPosA_7558 = e_match_from(_lLevel_7560, _lPath_7557, _lPosA_7558);

    /** 	end while*/
    goto L1B; // [625] 598
L1C: 

    /** 	lLevel = SLASH & ".." & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = _3756;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_7560, concat_list, 3);
    }

    /** 	lPosB = 1*/
    _lPosB_7559 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1D; // [643] 721
L1E: 
    if (_lPosA_7558 == 0)
    goto L1F; // [646] 733

    /** 		lPosB = lPosA-1*/
    _lPosB_7559 = _lPosA_7558 - 1;

    /** 		while((lPosB > 0) and (lPath[lPosB] != SLASH)) do*/
L20: 
    _4086 = (_lPosB_7559 > 0);
    if (_4086 == 0) {
        DeRef(_4087);
        _4087 = 0;
        goto L21; // [665] 681
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4088 = (int)*(((s1_ptr)_2)->base + _lPosB_7559);
    if (IS_ATOM_INT(_4088)) {
        _4089 = (_4088 != 92);
    }
    else {
        _4089 = binary_op(NOTEQ, _4088, 92);
    }
    _4088 = NOVALUE;
    if (IS_ATOM_INT(_4089))
    _4087 = (_4089 != 0);
    else
    _4087 = DBL_PTR(_4089)->dbl != 0.0;
L21: 
    if (_4087 == 0)
    {
        _4087 = NOVALUE;
        goto L22; // [681] 695
    }
    else{
        _4087 = NOVALUE;
    }

    /** 			lPosB -= 1*/
    _lPosB_7559 = _lPosB_7559 - 1;

    /** 		end while*/
    goto L20; // [692] 661
L22: 

    /** 		if (lPosB <= 0) then*/
    if (_lPosB_7559 > 0)
    goto L23; // [697] 707

    /** 			lPosB = 1*/
    _lPosB_7559 = 1;
L23: 

    /** 		lPath = eu:remove(lPath, lPosB, lPosA + 2)*/
    _4092 = _lPosA_7558 + 2;
    if ((long)((unsigned long)_4092 + (unsigned long)HIGH_BITS) >= 0) 
    _4092 = NewDouble((double)_4092);
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7557);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosB_7559)) ? _lPosB_7559 : (long)(DBL_PTR(_lPosB_7559)->dbl);
        int stop = (IS_ATOM_INT(_4092)) ? _4092 : (long)(DBL_PTR(_4092)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7557), start, &_lPath_7557 );
            }
            else Tail(SEQ_PTR(_lPath_7557), stop+1, &_lPath_7557);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7557), start, &_lPath_7557);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7557 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7557)->ref == 1));
        }
    }
    DeRef(_4092);
    _4092 = NOVALUE;

    /** 	  entry*/
L1D: 

    /** 		lPosA = match(lLevel, lPath, lPosB )*/
    _lPosA_7558 = e_match_from(_lLevel_7560, _lPath_7557, _lPosB_7559);

    /** 	end while*/
    goto L1E; // [730] 646
L1F: 

    /** 	if case_flags = TO_LOWER then*/
    if (_case_flags_7556 != 1)
    goto L24; // [737] 752

    /** 		lPath = lower( lPath )*/
    RefDS(_lPath_7557);
    _0 = _lPath_7557;
    _lPath_7557 = _10lower(_lPath_7557);
    DeRefDS(_0);
    goto L25; // [749] 1407
L24: 

    /** 	elsif case_flags != AS_IS then*/
    if (_case_flags_7556 == 0)
    goto L26; // [756] 1404

    /** 		sequence sl = find_all(SLASH,lPath) -- split apart lPath*/
    RefDS(_lPath_7557);
    _0 = _sl_7703;
    _sl_7703 = _12find_all(92, _lPath_7557, 1);
    DeRef(_0);

    /** 		integer short_name = and_bits(TO_SHORT,case_flags)=TO_SHORT*/
    {unsigned long tu;
         tu = (unsigned long)4 & (unsigned long)_case_flags_7556;
         _4099 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4099)) {
        _short_name_7706 = (_4099 == 4);
    }
    else {
        _short_name_7706 = (DBL_PTR(_4099)->dbl == (double)4);
    }
    DeRef(_4099);
    _4099 = NOVALUE;

    /** 		integer correct_name = and_bits(case_flags,CORRECT)=CORRECT*/
    {unsigned long tu;
         tu = (unsigned long)_case_flags_7556 & (unsigned long)2;
         _4101 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4101)) {
        _correct_name_7709 = (_4101 == 2);
    }
    else {
        _correct_name_7709 = (DBL_PTR(_4101)->dbl == (double)2);
    }
    DeRef(_4101);
    _4101 = NOVALUE;

    /** 		integer lower_name = and_bits(TO_LOWER,case_flags)=TO_LOWER*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_7556;
         _4103 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4103)) {
        _lower_name_7712 = (_4103 == 1);
    }
    else {
        _lower_name_7712 = (DBL_PTR(_4103)->dbl == (double)1);
    }
    DeRef(_4103);
    _4103 = NOVALUE;

    /** 		if lPath[$] != SLASH then*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4105 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4105 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7557);
    _4106 = (int)*(((s1_ptr)_2)->base + _4105);
    if (binary_op_a(EQUALS, _4106, 92)){
        _4106 = NOVALUE;
        goto L27; // [827] 849
    }
    _4106 = NOVALUE;

    /** 			sl = sl & {length(lPath)+1}*/
    if (IS_SEQUENCE(_lPath_7557)){
            _4108 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4108 = 1;
    }
    _4109 = _4108 + 1;
    _4108 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4109;
    _4110 = MAKE_SEQ(_1);
    _4109 = NOVALUE;
    Concat((object_ptr)&_sl_7703, _sl_7703, _4110);
    DeRefDS(_4110);
    _4110 = NOVALUE;
L27: 

    /** 		for i = length(sl)-1 to 1 by -1 label "partloop" do*/
    if (IS_SEQUENCE(_sl_7703)){
            _4112 = SEQ_PTR(_sl_7703)->length;
    }
    else {
        _4112 = 1;
    }
    _4113 = _4112 - 1;
    _4112 = NOVALUE;
    {
        int _i_7724;
        _i_7724 = _4113;
L28: 
        if (_i_7724 < 1){
            goto L29; // [858] 1363
        }

        /** 			ifdef WINDOWS then*/

        /** 				sequence part = lDrive & lPath[1..sl[i]-1]*/
        _2 = (int)SEQ_PTR(_sl_7703);
        _4115 = (int)*(((s1_ptr)_2)->base + _i_7724);
        if (IS_ATOM_INT(_4115)) {
            _4116 = _4115 - 1;
        }
        else {
            _4116 = binary_op(MINUS, _4115, 1);
        }
        _4115 = NOVALUE;
        rhs_slice_target = (object_ptr)&_4117;
        RHS_Slice(_lPath_7557, 1, _4116);
        Concat((object_ptr)&_part_7728, _lDrive_7562, _4117);
        DeRefDS(_4117);
        _4117 = NOVALUE;

        /** 			object list = dir( part & SLASH )*/
        Append(&_4119, _part_7728, 92);
        DeRef(_name_inlined_dir_at_896_7736);
        _name_inlined_dir_at_896_7736 = _4119;
        _4119 = NOVALUE;

        /** 	ifdef WINDOWS then*/

        /** 		return machine_func(M_DIR, name)*/
        DeRef(_list_7733);
        _list_7733 = machine(22, _name_inlined_dir_at_896_7736);
        DeRef(_name_inlined_dir_at_896_7736);
        _name_inlined_dir_at_896_7736 = NOVALUE;

        /** 			sequence supplied_name = lPath[sl[i]+1..sl[i+1]-1]*/
        _2 = (int)SEQ_PTR(_sl_7703);
        _4120 = (int)*(((s1_ptr)_2)->base + _i_7724);
        if (IS_ATOM_INT(_4120)) {
            _4121 = _4120 + 1;
            if (_4121 > MAXINT){
                _4121 = NewDouble((double)_4121);
            }
        }
        else
        _4121 = binary_op(PLUS, 1, _4120);
        _4120 = NOVALUE;
        _4122 = _i_7724 + 1;
        _2 = (int)SEQ_PTR(_sl_7703);
        _4123 = (int)*(((s1_ptr)_2)->base + _4122);
        if (IS_ATOM_INT(_4123)) {
            _4124 = _4123 - 1;
        }
        else {
            _4124 = binary_op(MINUS, _4123, 1);
        }
        _4123 = NOVALUE;
        rhs_slice_target = (object_ptr)&_supplied_name_7738;
        RHS_Slice(_lPath_7557, _4121, _4124);

        /** 			if atom(list) then*/
        _4126 = IS_ATOM(_list_7733);
        if (_4126 == 0)
        {
            _4126 = NOVALUE;
            goto L2A; // [944] 982
        }
        else{
            _4126 = NOVALUE;
        }

        /** 				if lower_name then*/
        if (_lower_name_7712 == 0)
        {
            goto L2B; // [949] 975
        }
        else{
        }

        /** 					lPath = part & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_7703);
        _4127 = (int)*(((s1_ptr)_2)->base + _i_7724);
        if (IS_SEQUENCE(_lPath_7557)){
                _4128 = SEQ_PTR(_lPath_7557)->length;
        }
        else {
            _4128 = 1;
        }
        rhs_slice_target = (object_ptr)&_4129;
        RHS_Slice(_lPath_7557, _4127, _4128);
        _4130 = _10lower(_4129);
        _4129 = NOVALUE;
        if (IS_SEQUENCE(_part_7728) && IS_ATOM(_4130)) {
            Ref(_4130);
            Append(&_lPath_7557, _part_7728, _4130);
        }
        else if (IS_ATOM(_part_7728) && IS_SEQUENCE(_4130)) {
        }
        else {
            Concat((object_ptr)&_lPath_7557, _part_7728, _4130);
        }
        DeRef(_4130);
        _4130 = NOVALUE;
L2B: 

        /** 				continue*/
        DeRef(_part_7728);
        _part_7728 = NOVALUE;
        DeRef(_list_7733);
        _list_7733 = NOVALUE;
        DeRef(_supplied_name_7738);
        _supplied_name_7738 = NOVALUE;
        goto L2C; // [979] 1358
L2A: 

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_7733)){
                _4132 = SEQ_PTR(_list_7733)->length;
        }
        else {
            _4132 = 1;
        }
        {
            int _j_7755;
            _j_7755 = 1;
L2D: 
            if (_j_7755 > _4132){
                goto L2E; // [987] 1118
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_7733);
            _4133 = (int)*(((s1_ptr)_2)->base + _j_7755);
            DeRef(_read_name_7757);
            _2 = (int)SEQ_PTR(_4133);
            _read_name_7757 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_7757);
            _4133 = NOVALUE;

            /** 				if equal(read_name, supplied_name) then*/
            if (_read_name_7757 == _supplied_name_7738)
            _4135 = 1;
            else if (IS_ATOM_INT(_read_name_7757) && IS_ATOM_INT(_supplied_name_7738))
            _4135 = 0;
            else
            _4135 = (compare(_read_name_7757, _supplied_name_7738) == 0);
            if (_4135 == 0)
            {
                _4135 = NOVALUE;
                goto L2F; // [1014] 1109
            }
            else{
                _4135 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_7706 == 0) {
                goto L30; // [1019] 1100
            }
            _2 = (int)SEQ_PTR(_list_7733);
            _4137 = (int)*(((s1_ptr)_2)->base + _j_7755);
            _2 = (int)SEQ_PTR(_4137);
            _4138 = (int)*(((s1_ptr)_2)->base + 11);
            _4137 = NOVALUE;
            _4139 = IS_SEQUENCE(_4138);
            _4138 = NOVALUE;
            if (_4139 == 0)
            {
                _4139 = NOVALUE;
                goto L30; // [1037] 1100
            }
            else{
                _4139 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7703);
            _4140 = (int)*(((s1_ptr)_2)->base + _i_7724);
            rhs_slice_target = (object_ptr)&_4141;
            RHS_Slice(_lPath_7557, 1, _4140);
            _2 = (int)SEQ_PTR(_list_7733);
            _4142 = (int)*(((s1_ptr)_2)->base + _j_7755);
            _2 = (int)SEQ_PTR(_4142);
            _4143 = (int)*(((s1_ptr)_2)->base + 11);
            _4142 = NOVALUE;
            _4144 = _i_7724 + 1;
            _2 = (int)SEQ_PTR(_sl_7703);
            _4145 = (int)*(((s1_ptr)_2)->base + _4144);
            if (IS_SEQUENCE(_lPath_7557)){
                    _4146 = SEQ_PTR(_lPath_7557)->length;
            }
            else {
                _4146 = 1;
            }
            rhs_slice_target = (object_ptr)&_4147;
            RHS_Slice(_lPath_7557, _4145, _4146);
            {
                int concat_list[3];

                concat_list[0] = _4147;
                concat_list[1] = _4143;
                concat_list[2] = _4141;
                Concat_N((object_ptr)&_lPath_7557, concat_list, 3);
            }
            DeRefDS(_4147);
            _4147 = NOVALUE;
            _4143 = NOVALUE;
            DeRefDS(_4141);
            _4141 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_7703)){
                    _4149 = SEQ_PTR(_sl_7703)->length;
            }
            else {
                _4149 = 1;
            }
            if (IS_SEQUENCE(_lPath_7557)){
                    _4150 = SEQ_PTR(_lPath_7557)->length;
            }
            else {
                _4150 = 1;
            }
            _4151 = _4150 + 1;
            _4150 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_7703);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_7703 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _4149);
            _1 = *(int *)_2;
            *(int *)_2 = _4151;
            if( _1 != _4151 ){
                DeRef(_1);
            }
            _4151 = NOVALUE;
L30: 

            /** 					continue "partloop"*/
            DeRef(_read_name_7757);
            _read_name_7757 = NOVALUE;
            DeRef(_part_7728);
            _part_7728 = NOVALUE;
            DeRef(_list_7733);
            _list_7733 = NOVALUE;
            DeRef(_supplied_name_7738);
            _supplied_name_7738 = NOVALUE;
            goto L2C; // [1106] 1358
L2F: 
            DeRef(_read_name_7757);
            _read_name_7757 = NOVALUE;

            /** 			end for*/
            _j_7755 = _j_7755 + 1;
            goto L2D; // [1113] 994
L2E: 
            ;
        }

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_7733)){
                _4152 = SEQ_PTR(_list_7733)->length;
        }
        else {
            _4152 = 1;
        }
        {
            int _j_7780;
            _j_7780 = 1;
L31: 
            if (_j_7780 > _4152){
                goto L32; // [1123] 1301
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_7733);
            _4153 = (int)*(((s1_ptr)_2)->base + _j_7780);
            DeRef(_read_name_7782);
            _2 = (int)SEQ_PTR(_4153);
            _read_name_7782 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_7782);
            _4153 = NOVALUE;

            /** 				if equal(lower(read_name), lower(supplied_name)) then*/
            RefDS(_read_name_7782);
            _4155 = _10lower(_read_name_7782);
            RefDS(_supplied_name_7738);
            _4156 = _10lower(_supplied_name_7738);
            if (_4155 == _4156)
            _4157 = 1;
            else if (IS_ATOM_INT(_4155) && IS_ATOM_INT(_4156))
            _4157 = 0;
            else
            _4157 = (compare(_4155, _4156) == 0);
            DeRef(_4155);
            _4155 = NOVALUE;
            DeRef(_4156);
            _4156 = NOVALUE;
            if (_4157 == 0)
            {
                _4157 = NOVALUE;
                goto L33; // [1158] 1292
            }
            else{
                _4157 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_7706 == 0) {
                goto L34; // [1163] 1244
            }
            _2 = (int)SEQ_PTR(_list_7733);
            _4159 = (int)*(((s1_ptr)_2)->base + _j_7780);
            _2 = (int)SEQ_PTR(_4159);
            _4160 = (int)*(((s1_ptr)_2)->base + 11);
            _4159 = NOVALUE;
            _4161 = IS_SEQUENCE(_4160);
            _4160 = NOVALUE;
            if (_4161 == 0)
            {
                _4161 = NOVALUE;
                goto L34; // [1181] 1244
            }
            else{
                _4161 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7703);
            _4162 = (int)*(((s1_ptr)_2)->base + _i_7724);
            rhs_slice_target = (object_ptr)&_4163;
            RHS_Slice(_lPath_7557, 1, _4162);
            _2 = (int)SEQ_PTR(_list_7733);
            _4164 = (int)*(((s1_ptr)_2)->base + _j_7780);
            _2 = (int)SEQ_PTR(_4164);
            _4165 = (int)*(((s1_ptr)_2)->base + 11);
            _4164 = NOVALUE;
            _4166 = _i_7724 + 1;
            _2 = (int)SEQ_PTR(_sl_7703);
            _4167 = (int)*(((s1_ptr)_2)->base + _4166);
            if (IS_SEQUENCE(_lPath_7557)){
                    _4168 = SEQ_PTR(_lPath_7557)->length;
            }
            else {
                _4168 = 1;
            }
            rhs_slice_target = (object_ptr)&_4169;
            RHS_Slice(_lPath_7557, _4167, _4168);
            {
                int concat_list[3];

                concat_list[0] = _4169;
                concat_list[1] = _4165;
                concat_list[2] = _4163;
                Concat_N((object_ptr)&_lPath_7557, concat_list, 3);
            }
            DeRefDS(_4169);
            _4169 = NOVALUE;
            _4165 = NOVALUE;
            DeRefDS(_4163);
            _4163 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_7703)){
                    _4171 = SEQ_PTR(_sl_7703)->length;
            }
            else {
                _4171 = 1;
            }
            if (IS_SEQUENCE(_lPath_7557)){
                    _4172 = SEQ_PTR(_lPath_7557)->length;
            }
            else {
                _4172 = 1;
            }
            _4173 = _4172 + 1;
            _4172 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_7703);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_7703 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _4171);
            _1 = *(int *)_2;
            *(int *)_2 = _4173;
            if( _1 != _4173 ){
                DeRef(_1);
            }
            _4173 = NOVALUE;
L34: 

            /** 					if correct_name then*/
            if (_correct_name_7709 == 0)
            {
                goto L35; // [1246] 1283
            }
            else{
            }

            /** 						lPath = lPath[1..sl[i]] & read_name & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7703);
            _4174 = (int)*(((s1_ptr)_2)->base + _i_7724);
            rhs_slice_target = (object_ptr)&_4175;
            RHS_Slice(_lPath_7557, 1, _4174);
            _4176 = _i_7724 + 1;
            _2 = (int)SEQ_PTR(_sl_7703);
            _4177 = (int)*(((s1_ptr)_2)->base + _4176);
            if (IS_SEQUENCE(_lPath_7557)){
                    _4178 = SEQ_PTR(_lPath_7557)->length;
            }
            else {
                _4178 = 1;
            }
            rhs_slice_target = (object_ptr)&_4179;
            RHS_Slice(_lPath_7557, _4177, _4178);
            {
                int concat_list[3];

                concat_list[0] = _4179;
                concat_list[1] = _read_name_7782;
                concat_list[2] = _4175;
                Concat_N((object_ptr)&_lPath_7557, concat_list, 3);
            }
            DeRefDS(_4179);
            _4179 = NOVALUE;
            DeRefDS(_4175);
            _4175 = NOVALUE;
L35: 

            /** 					continue "partloop"*/
            DeRef(_read_name_7782);
            _read_name_7782 = NOVALUE;
            DeRef(_part_7728);
            _part_7728 = NOVALUE;
            DeRef(_list_7733);
            _list_7733 = NOVALUE;
            DeRef(_supplied_name_7738);
            _supplied_name_7738 = NOVALUE;
            goto L2C; // [1289] 1358
L33: 
            DeRef(_read_name_7782);
            _read_name_7782 = NOVALUE;

            /** 			end for*/
            _j_7780 = _j_7780 + 1;
            goto L31; // [1296] 1130
L32: 
            ;
        }

        /** 			if and_bits(TO_LOWER,case_flags) then*/
        {unsigned long tu;
             tu = (unsigned long)1 & (unsigned long)_case_flags_7556;
             _4181 = MAKE_UINT(tu);
        }
        if (_4181 == 0) {
            DeRef(_4181);
            _4181 = NOVALUE;
            goto L36; // [1309] 1348
        }
        else {
            if (!IS_ATOM_INT(_4181) && DBL_PTR(_4181)->dbl == 0.0){
                DeRef(_4181);
                _4181 = NOVALUE;
                goto L36; // [1309] 1348
            }
            DeRef(_4181);
            _4181 = NOVALUE;
        }
        DeRef(_4181);
        _4181 = NOVALUE;

        /** 				lPath = lPath[1..sl[i]-1] & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_7703);
        _4182 = (int)*(((s1_ptr)_2)->base + _i_7724);
        if (IS_ATOM_INT(_4182)) {
            _4183 = _4182 - 1;
        }
        else {
            _4183 = binary_op(MINUS, _4182, 1);
        }
        _4182 = NOVALUE;
        rhs_slice_target = (object_ptr)&_4184;
        RHS_Slice(_lPath_7557, 1, _4183);
        _2 = (int)SEQ_PTR(_sl_7703);
        _4185 = (int)*(((s1_ptr)_2)->base + _i_7724);
        if (IS_SEQUENCE(_lPath_7557)){
                _4186 = SEQ_PTR(_lPath_7557)->length;
        }
        else {
            _4186 = 1;
        }
        rhs_slice_target = (object_ptr)&_4187;
        RHS_Slice(_lPath_7557, _4185, _4186);
        _4188 = _10lower(_4187);
        _4187 = NOVALUE;
        if (IS_SEQUENCE(_4184) && IS_ATOM(_4188)) {
            Ref(_4188);
            Append(&_lPath_7557, _4184, _4188);
        }
        else if (IS_ATOM(_4184) && IS_SEQUENCE(_4188)) {
        }
        else {
            Concat((object_ptr)&_lPath_7557, _4184, _4188);
            DeRefDS(_4184);
            _4184 = NOVALUE;
        }
        DeRef(_4184);
        _4184 = NOVALUE;
        DeRef(_4188);
        _4188 = NOVALUE;
L36: 

        /** 			exit*/
        DeRef(_part_7728);
        _part_7728 = NOVALUE;
        DeRef(_list_7733);
        _list_7733 = NOVALUE;
        DeRef(_supplied_name_7738);
        _supplied_name_7738 = NOVALUE;
        goto L29; // [1352] 1363

        /** 		end for*/
L2C: 
        _i_7724 = _i_7724 + -1;
        goto L28; // [1358] 865
L29: 
        ;
    }

    /** 		if and_bits(case_flags,or_bits(CORRECT,TO_LOWER))=TO_LOWER and length(lPath) then*/
    {unsigned long tu;
         tu = (unsigned long)2 | (unsigned long)1;
         _4190 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_4190)) {
        {unsigned long tu;
             tu = (unsigned long)_case_flags_7556 & (unsigned long)_4190;
             _4191 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_case_flags_7556;
        _4191 = Dand_bits(&temp_d, DBL_PTR(_4190));
    }
    DeRef(_4190);
    _4190 = NOVALUE;
    if (IS_ATOM_INT(_4191)) {
        _4192 = (_4191 == 1);
    }
    else {
        _4192 = (DBL_PTR(_4191)->dbl == (double)1);
    }
    DeRef(_4191);
    _4191 = NOVALUE;
    if (_4192 == 0) {
        goto L37; // [1383] 1403
    }
    if (IS_SEQUENCE(_lPath_7557)){
            _4194 = SEQ_PTR(_lPath_7557)->length;
    }
    else {
        _4194 = 1;
    }
    if (_4194 == 0)
    {
        _4194 = NOVALUE;
        goto L37; // [1391] 1403
    }
    else{
        _4194 = NOVALUE;
    }

    /** 			lPath = lower(lPath)*/
    RefDS(_lPath_7557);
    _0 = _lPath_7557;
    _lPath_7557 = _10lower(_lPath_7557);
    DeRefDS(_0);
L37: 
L26: 
    DeRef(_sl_7703);
    _sl_7703 = NOVALUE;
L25: 

    /** 	ifdef WINDOWS then*/

    /** 		if and_bits(CORRECT,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)2 & (unsigned long)_case_flags_7556;
         _4196 = MAKE_UINT(tu);
    }
    if (_4196 == 0) {
        DeRef(_4196);
        _4196 = NOVALUE;
        goto L38; // [1417] 1459
    }
    else {
        if (!IS_ATOM_INT(_4196) && DBL_PTR(_4196)->dbl == 0.0){
            DeRef(_4196);
            _4196 = NOVALUE;
            goto L38; // [1417] 1459
        }
        DeRef(_4196);
        _4196 = NOVALUE;
    }
    DeRef(_4196);
    _4196 = NOVALUE;

    /** 			if or_bits(system_drive_case,'A') = 'a' then*/
    if (IS_ATOM_INT(_13system_drive_case_7540)) {
        {unsigned long tu;
             tu = (unsigned long)_13system_drive_case_7540 | (unsigned long)65;
             _4197 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)65;
        _4197 = Dor_bits(DBL_PTR(_13system_drive_case_7540), &temp_d);
    }
    if (binary_op_a(NOTEQ, _4197, 97)){
        DeRef(_4197);
        _4197 = NOVALUE;
        goto L39; // [1428] 1445
    }
    DeRef(_4197);
    _4197 = NOVALUE;

    /** 				lDrive = lower(lDrive)*/
    RefDS(_lDrive_7562);
    _0 = _lDrive_7562;
    _lDrive_7562 = _10lower(_lDrive_7562);
    DeRefDS(_0);
    goto L3A; // [1442] 1482
L39: 

    /** 				lDrive = upper(lDrive)*/
    RefDS(_lDrive_7562);
    _0 = _lDrive_7562;
    _lDrive_7562 = _10upper(_lDrive_7562);
    DeRefDS(_0);
    goto L3A; // [1456] 1482
L38: 

    /** 		elsif and_bits(TO_LOWER,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_7556;
         _4201 = MAKE_UINT(tu);
    }
    if (_4201 == 0) {
        DeRef(_4201);
        _4201 = NOVALUE;
        goto L3B; // [1467] 1481
    }
    else {
        if (!IS_ATOM_INT(_4201) && DBL_PTR(_4201)->dbl == 0.0){
            DeRef(_4201);
            _4201 = NOVALUE;
            goto L3B; // [1467] 1481
        }
        DeRef(_4201);
        _4201 = NOVALUE;
    }
    DeRef(_4201);
    _4201 = NOVALUE;

    /** 			lDrive = lower(lDrive)*/
    RefDS(_lDrive_7562);
    _0 = _lDrive_7562;
    _lDrive_7562 = _10lower(_lDrive_7562);
    DeRefDS(_0);
L3B: 
L3A: 

    /** 		lPath = lDrive & lPath*/
    Concat((object_ptr)&_lPath_7557, _lDrive_7562, _lPath_7557);

    /** 	return lPath & wildcard_suffix*/
    Concat((object_ptr)&_4204, _lPath_7557, _wildcard_suffix_7627);
    DeRefDS(_path_in_7554);
    DeRefDS(_lPath_7557);
    DeRefi(_lLevel_7560);
    DeRef(_lHome_7561);
    DeRefDS(_lDrive_7562);
    DeRefDS(_wildcard_suffix_7627);
    DeRef(_3994);
    _3994 = NOVALUE;
    DeRef(_4089);
    _4089 = NOVALUE;
    DeRef(_3997);
    _3997 = NOVALUE;
    DeRef(_4072);
    _4072 = NOVALUE;
    _4140 = NOVALUE;
    DeRef(_4176);
    _4176 = NOVALUE;
    DeRef(_4076);
    _4076 = NOVALUE;
    DeRef(_4121);
    _4121 = NOVALUE;
    DeRef(_4037);
    _4037 = NOVALUE;
    DeRef(_4183);
    _4183 = NOVALUE;
    DeRef(_4009);
    _4009 = NOVALUE;
    DeRef(_4124);
    _4124 = NOVALUE;
    _4162 = NOVALUE;
    DeRef(_4192);
    _4192 = NOVALUE;
    _4145 = NOVALUE;
    _4167 = NOVALUE;
    DeRef(_4034);
    _4034 = NOVALUE;
    DeRef(_4144);
    _4144 = NOVALUE;
    DeRef(_4023);
    _4023 = NOVALUE;
    DeRef(_4113);
    _4113 = NOVALUE;
    DeRef(_4001);
    _4001 = NOVALUE;
    DeRef(_4054);
    _4054 = NOVALUE;
    _4177 = NOVALUE;
    _4174 = NOVALUE;
    DeRef(_4049);
    _4049 = NOVALUE;
    DeRef(_4122);
    _4122 = NOVALUE;
    DeRef(_4066);
    _4066 = NOVALUE;
    DeRef(_4086);
    _4086 = NOVALUE;
    DeRef(_4116);
    _4116 = NOVALUE;
    _4127 = NOVALUE;
    DeRef(_4006);
    _4006 = NOVALUE;
    DeRef(_4063);
    _4063 = NOVALUE;
    _4185 = NOVALUE;
    DeRef(_4003);
    _4003 = NOVALUE;
    DeRef(_4166);
    _4166 = NOVALUE;
    return _4204;
    ;
}


int _13fs_case(int _s_7853)
{
    int _4205 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return lower(s)*/
    RefDS(_s_7853);
    _4205 = _10lower(_s_7853);
    DeRefDS(_s_7853);
    return _4205;
    ;
}


int _13abbreviate_path(int _orig_path_7858, int _base_paths_7859)
{
    int _expanded_path_7860 = NOVALUE;
    int _lowered_expanded_path_7870 = NOVALUE;
    int _4250 = NOVALUE;
    int _4249 = NOVALUE;
    int _4246 = NOVALUE;
    int _4245 = NOVALUE;
    int _4244 = NOVALUE;
    int _4243 = NOVALUE;
    int _4242 = NOVALUE;
    int _4240 = NOVALUE;
    int _4239 = NOVALUE;
    int _4238 = NOVALUE;
    int _4237 = NOVALUE;
    int _4236 = NOVALUE;
    int _4235 = NOVALUE;
    int _4234 = NOVALUE;
    int _4233 = NOVALUE;
    int _4232 = NOVALUE;
    int _4229 = NOVALUE;
    int _4228 = NOVALUE;
    int _4226 = NOVALUE;
    int _4225 = NOVALUE;
    int _4224 = NOVALUE;
    int _4223 = NOVALUE;
    int _4222 = NOVALUE;
    int _4221 = NOVALUE;
    int _4220 = NOVALUE;
    int _4219 = NOVALUE;
    int _4218 = NOVALUE;
    int _4217 = NOVALUE;
    int _4216 = NOVALUE;
    int _4215 = NOVALUE;
    int _4214 = NOVALUE;
    int _4211 = NOVALUE;
    int _4210 = NOVALUE;
    int _4209 = NOVALUE;
    int _4207 = NOVALUE;
    int _0, _1, _2;
    

    /** 	expanded_path = canonical_path(orig_path)*/
    RefDS(_orig_path_7858);
    _0 = _expanded_path_7860;
    _expanded_path_7860 = _13canonical_path(_orig_path_7858, 0, 0);
    DeRef(_0);

    /** 	base_paths = append(base_paths, curdir())*/
    _4207 = _13curdir(0);
    Ref(_4207);
    Append(&_base_paths_7859, _base_paths_7859, _4207);
    DeRef(_4207);
    _4207 = NOVALUE;

    /** 	for i = 1 to length(base_paths) do*/
    _4209 = 1;
    {
        int _i_7865;
        _i_7865 = 1;
L1: 
        if (_i_7865 > 1){
            goto L2; // [32] 64
        }

        /** 		base_paths[i] = canonical_path(base_paths[i], 1) -- assume each base path is meant to be a directory.*/
        _2 = (int)SEQ_PTR(_base_paths_7859);
        _4210 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_4210);
        _4211 = _13canonical_path(_4210, 1, 0);
        _4210 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_7859);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _base_paths_7859 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _4211;
        if( _1 != _4211 ){
            DeRef(_1);
        }
        _4211 = NOVALUE;

        /** 	end for*/
        _i_7865 = 1 + 1;
        goto L1; // [59] 39
L2: 
        ;
    }

    /** 	base_paths = fs_case(base_paths)*/
    RefDS(_base_paths_7859);
    _0 = _base_paths_7859;
    _base_paths_7859 = _13fs_case(_base_paths_7859);
    DeRefDS(_0);

    /** 	sequence lowered_expanded_path = fs_case(expanded_path)*/
    RefDS(_expanded_path_7860);
    _0 = _lowered_expanded_path_7870;
    _lowered_expanded_path_7870 = _13fs_case(_expanded_path_7860);
    DeRef(_0);

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_7859)){
            _4214 = SEQ_PTR(_base_paths_7859)->length;
    }
    else {
        _4214 = 1;
    }
    {
        int _i_7873;
        _i_7873 = 1;
L3: 
        if (_i_7873 > _4214){
            goto L4; // [85] 139
        }

        /** 		if search:begins(base_paths[i], lowered_expanded_path) then*/
        _2 = (int)SEQ_PTR(_base_paths_7859);
        _4215 = (int)*(((s1_ptr)_2)->base + _i_7873);
        Ref(_4215);
        RefDS(_lowered_expanded_path_7870);
        _4216 = _12begins(_4215, _lowered_expanded_path_7870);
        _4215 = NOVALUE;
        if (_4216 == 0) {
            DeRef(_4216);
            _4216 = NOVALUE;
            goto L5; // [103] 132
        }
        else {
            if (!IS_ATOM_INT(_4216) && DBL_PTR(_4216)->dbl == 0.0){
                DeRef(_4216);
                _4216 = NOVALUE;
                goto L5; // [103] 132
            }
            DeRef(_4216);
            _4216 = NOVALUE;
        }
        DeRef(_4216);
        _4216 = NOVALUE;

        /** 			return expanded_path[length(base_paths[i]) + 1 .. $]*/
        _2 = (int)SEQ_PTR(_base_paths_7859);
        _4217 = (int)*(((s1_ptr)_2)->base + _i_7873);
        if (IS_SEQUENCE(_4217)){
                _4218 = SEQ_PTR(_4217)->length;
        }
        else {
            _4218 = 1;
        }
        _4217 = NOVALUE;
        _4219 = _4218 + 1;
        _4218 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_7860)){
                _4220 = SEQ_PTR(_expanded_path_7860)->length;
        }
        else {
            _4220 = 1;
        }
        rhs_slice_target = (object_ptr)&_4221;
        RHS_Slice(_expanded_path_7860, _4219, _4220);
        DeRefDS(_orig_path_7858);
        DeRefDS(_base_paths_7859);
        DeRefDS(_expanded_path_7860);
        DeRefDS(_lowered_expanded_path_7870);
        _4217 = NOVALUE;
        _4219 = NOVALUE;
        return _4221;
L5: 

        /** 	end for*/
        _i_7873 = _i_7873 + 1;
        goto L3; // [134] 92
L4: 
        ;
    }

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(base_paths[$][1], lowered_expanded_path[1]) then*/
    if (IS_SEQUENCE(_base_paths_7859)){
            _4222 = SEQ_PTR(_base_paths_7859)->length;
    }
    else {
        _4222 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_7859);
    _4223 = (int)*(((s1_ptr)_2)->base + _4222);
    _2 = (int)SEQ_PTR(_4223);
    _4224 = (int)*(((s1_ptr)_2)->base + 1);
    _4223 = NOVALUE;
    _2 = (int)SEQ_PTR(_lowered_expanded_path_7870);
    _4225 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4224 == _4225)
    _4226 = 1;
    else if (IS_ATOM_INT(_4224) && IS_ATOM_INT(_4225))
    _4226 = 0;
    else
    _4226 = (compare(_4224, _4225) == 0);
    _4224 = NOVALUE;
    _4225 = NOVALUE;
    if (_4226 != 0)
    goto L6; // [162] 172
    _4226 = NOVALUE;

    /** 			return orig_path*/
    DeRefDS(_base_paths_7859);
    DeRef(_expanded_path_7860);
    DeRefDS(_lowered_expanded_path_7870);
    _4217 = NOVALUE;
    DeRef(_4219);
    _4219 = NOVALUE;
    DeRef(_4221);
    _4221 = NOVALUE;
    return _orig_path_7858;
L6: 

    /** 	base_paths = stdseq:split(base_paths[$], SLASH)*/
    if (IS_SEQUENCE(_base_paths_7859)){
            _4228 = SEQ_PTR(_base_paths_7859)->length;
    }
    else {
        _4228 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_7859);
    _4229 = (int)*(((s1_ptr)_2)->base + _4228);
    Ref(_4229);
    _0 = _base_paths_7859;
    _base_paths_7859 = _21split(_4229, 92, 0, 0);
    DeRefDS(_0);
    _4229 = NOVALUE;

    /** 	expanded_path = stdseq:split(expanded_path, SLASH)*/
    RefDS(_expanded_path_7860);
    _0 = _expanded_path_7860;
    _expanded_path_7860 = _21split(_expanded_path_7860, 92, 0, 0);
    DeRefDS(_0);

    /** 	lowered_expanded_path = ""*/
    RefDS(_5);
    DeRef(_lowered_expanded_path_7870);
    _lowered_expanded_path_7870 = _5;

    /** 	for i = 1 to math:min({length(expanded_path), length(base_paths) - 1}) do*/
    if (IS_SEQUENCE(_expanded_path_7860)){
            _4232 = SEQ_PTR(_expanded_path_7860)->length;
    }
    else {
        _4232 = 1;
    }
    if (IS_SEQUENCE(_base_paths_7859)){
            _4233 = SEQ_PTR(_base_paths_7859)->length;
    }
    else {
        _4233 = 1;
    }
    _4234 = _4233 - 1;
    _4233 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4232;
    ((int *)_2)[2] = _4234;
    _4235 = MAKE_SEQ(_1);
    _4234 = NOVALUE;
    _4232 = NOVALUE;
    _4236 = _18min(_4235);
    _4235 = NOVALUE;
    {
        int _i_7895;
        _i_7895 = 1;
L7: 
        if (binary_op_a(GREATER, _i_7895, _4236)){
            goto L8; // [228] 321
        }

        /** 		if not equal(fs_case(expanded_path[i]), base_paths[i]) then*/
        _2 = (int)SEQ_PTR(_expanded_path_7860);
        if (!IS_ATOM_INT(_i_7895)){
            _4237 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7895)->dbl));
        }
        else{
            _4237 = (int)*(((s1_ptr)_2)->base + _i_7895);
        }
        Ref(_4237);
        _4238 = _13fs_case(_4237);
        _4237 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_7859);
        if (!IS_ATOM_INT(_i_7895)){
            _4239 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7895)->dbl));
        }
        else{
            _4239 = (int)*(((s1_ptr)_2)->base + _i_7895);
        }
        if (_4238 == _4239)
        _4240 = 1;
        else if (IS_ATOM_INT(_4238) && IS_ATOM_INT(_4239))
        _4240 = 0;
        else
        _4240 = (compare(_4238, _4239) == 0);
        DeRef(_4238);
        _4238 = NOVALUE;
        _4239 = NOVALUE;
        if (_4240 != 0)
        goto L9; // [253] 314
        _4240 = NOVALUE;

        /** 			expanded_path = repeat("..", length(base_paths) - i) & expanded_path[i .. $]*/
        if (IS_SEQUENCE(_base_paths_7859)){
                _4242 = SEQ_PTR(_base_paths_7859)->length;
        }
        else {
            _4242 = 1;
        }
        if (IS_ATOM_INT(_i_7895)) {
            _4243 = _4242 - _i_7895;
        }
        else {
            _4243 = NewDouble((double)_4242 - DBL_PTR(_i_7895)->dbl);
        }
        _4242 = NOVALUE;
        _4244 = Repeat(_3756, _4243);
        DeRef(_4243);
        _4243 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_7860)){
                _4245 = SEQ_PTR(_expanded_path_7860)->length;
        }
        else {
            _4245 = 1;
        }
        rhs_slice_target = (object_ptr)&_4246;
        RHS_Slice(_expanded_path_7860, _i_7895, _4245);
        Concat((object_ptr)&_expanded_path_7860, _4244, _4246);
        DeRefDS(_4244);
        _4244 = NOVALUE;
        DeRef(_4244);
        _4244 = NOVALUE;
        DeRefDS(_4246);
        _4246 = NOVALUE;

        /** 			expanded_path = stdseq:join(expanded_path, SLASH)*/
        RefDS(_expanded_path_7860);
        _0 = _expanded_path_7860;
        _expanded_path_7860 = _21join(_expanded_path_7860, 92);
        DeRefDS(_0);

        /** 			if length(expanded_path) < length(orig_path) then*/
        if (IS_SEQUENCE(_expanded_path_7860)){
                _4249 = SEQ_PTR(_expanded_path_7860)->length;
        }
        else {
            _4249 = 1;
        }
        if (IS_SEQUENCE(_orig_path_7858)){
                _4250 = SEQ_PTR(_orig_path_7858)->length;
        }
        else {
            _4250 = 1;
        }
        if (_4249 >= _4250)
        goto L8; // [298] 321

        /** 		  		return expanded_path*/
        DeRef(_i_7895);
        DeRefDS(_orig_path_7858);
        DeRefDS(_base_paths_7859);
        DeRef(_lowered_expanded_path_7870);
        _4217 = NOVALUE;
        DeRef(_4219);
        _4219 = NOVALUE;
        DeRef(_4221);
        _4221 = NOVALUE;
        DeRef(_4236);
        _4236 = NOVALUE;
        return _expanded_path_7860;

        /** 			exit*/
        goto L8; // [311] 321
L9: 

        /** 	end for*/
        _0 = _i_7895;
        if (IS_ATOM_INT(_i_7895)) {
            _i_7895 = _i_7895 + 1;
            if ((long)((unsigned long)_i_7895 +(unsigned long) HIGH_BITS) >= 0){
                _i_7895 = NewDouble((double)_i_7895);
            }
        }
        else {
            _i_7895 = binary_op_a(PLUS, _i_7895, 1);
        }
        DeRef(_0);
        goto L7; // [316] 235
L8: 
        ;
        DeRef(_i_7895);
    }

    /** 	return orig_path*/
    DeRefDS(_base_paths_7859);
    DeRef(_expanded_path_7860);
    DeRef(_lowered_expanded_path_7870);
    _4217 = NOVALUE;
    DeRef(_4219);
    _4219 = NOVALUE;
    DeRef(_4221);
    _4221 = NOVALUE;
    DeRef(_4236);
    _4236 = NOVALUE;
    return _orig_path_7858;
    ;
}


int _13split_path(int _fname_7920)
{
    int _4252 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return stdseq:split(fname, SLASH, 1)*/
    RefDS(_fname_7920);
    _4252 = _21split(_fname_7920, 92, 1, 0);
    DeRefDS(_fname_7920);
    return _4252;
    ;
}


int _13join_path(int _path_elements_7924)
{
    int _fname_7925 = NOVALUE;
    int _elem_7929 = NOVALUE;
    int _4266 = NOVALUE;
    int _4265 = NOVALUE;
    int _4264 = NOVALUE;
    int _4263 = NOVALUE;
    int _4262 = NOVALUE;
    int _4261 = NOVALUE;
    int _4259 = NOVALUE;
    int _4258 = NOVALUE;
    int _4256 = NOVALUE;
    int _4255 = NOVALUE;
    int _4253 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fname = ""*/
    RefDS(_5);
    DeRef(_fname_7925);
    _fname_7925 = _5;

    /** 	for i = 1 to length(path_elements) do*/
    if (IS_SEQUENCE(_path_elements_7924)){
            _4253 = SEQ_PTR(_path_elements_7924)->length;
    }
    else {
        _4253 = 1;
    }
    {
        int _i_7927;
        _i_7927 = 1;
L1: 
        if (_i_7927 > _4253){
            goto L2; // [15] 117
        }

        /** 		sequence elem = path_elements[i]*/
        DeRef(_elem_7929);
        _2 = (int)SEQ_PTR(_path_elements_7924);
        _elem_7929 = (int)*(((s1_ptr)_2)->base + _i_7927);
        Ref(_elem_7929);

        /** 		if elem[$] = SLASH then*/
        if (IS_SEQUENCE(_elem_7929)){
                _4255 = SEQ_PTR(_elem_7929)->length;
        }
        else {
            _4255 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_7929);
        _4256 = (int)*(((s1_ptr)_2)->base + _4255);
        if (binary_op_a(NOTEQ, _4256, 92)){
            _4256 = NOVALUE;
            goto L3; // [39] 58
        }
        _4256 = NOVALUE;

        /** 			elem = elem[1..$ - 1]*/
        if (IS_SEQUENCE(_elem_7929)){
                _4258 = SEQ_PTR(_elem_7929)->length;
        }
        else {
            _4258 = 1;
        }
        _4259 = _4258 - 1;
        _4258 = NOVALUE;
        rhs_slice_target = (object_ptr)&_elem_7929;
        RHS_Slice(_elem_7929, 1, _4259);
L3: 

        /** 		if length(elem) and elem[1] != SLASH then*/
        if (IS_SEQUENCE(_elem_7929)){
                _4261 = SEQ_PTR(_elem_7929)->length;
        }
        else {
            _4261 = 1;
        }
        if (_4261 == 0) {
            goto L4; // [63] 102
        }
        _2 = (int)SEQ_PTR(_elem_7929);
        _4263 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_4263)) {
            _4264 = (_4263 != 92);
        }
        else {
            _4264 = binary_op(NOTEQ, _4263, 92);
        }
        _4263 = NOVALUE;
        if (_4264 == 0) {
            DeRef(_4264);
            _4264 = NOVALUE;
            goto L4; // [76] 102
        }
        else {
            if (!IS_ATOM_INT(_4264) && DBL_PTR(_4264)->dbl == 0.0){
                DeRef(_4264);
                _4264 = NOVALUE;
                goto L4; // [76] 102
            }
            DeRef(_4264);
            _4264 = NOVALUE;
        }
        DeRef(_4264);
        _4264 = NOVALUE;

        /** 			ifdef WINDOWS then*/

        /** 				if elem[$] != ':' then*/
        if (IS_SEQUENCE(_elem_7929)){
                _4265 = SEQ_PTR(_elem_7929)->length;
        }
        else {
            _4265 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_7929);
        _4266 = (int)*(((s1_ptr)_2)->base + _4265);
        if (binary_op_a(EQUALS, _4266, 58)){
            _4266 = NOVALUE;
            goto L5; // [90] 101
        }
        _4266 = NOVALUE;

        /** 					elem = SLASH & elem*/
        Prepend(&_elem_7929, _elem_7929, 92);
L5: 
L4: 

        /** 		fname &= elem*/
        Concat((object_ptr)&_fname_7925, _fname_7925, _elem_7929);
        DeRefDS(_elem_7929);
        _elem_7929 = NOVALUE;

        /** 	end for*/
        _i_7927 = _i_7927 + 1;
        goto L1; // [112] 22
L2: 
        ;
    }

    /** 	return fname*/
    DeRefDS(_path_elements_7924);
    DeRef(_4259);
    _4259 = NOVALUE;
    return _fname_7925;
    ;
}


int _13file_type(int _filename_7958)
{
    int _dirfil_7959 = NOVALUE;
    int _dir_inlined_dir_at_66_7972 = NOVALUE;
    int _4294 = NOVALUE;
    int _4293 = NOVALUE;
    int _4292 = NOVALUE;
    int _4291 = NOVALUE;
    int _4290 = NOVALUE;
    int _4288 = NOVALUE;
    int _4287 = NOVALUE;
    int _4286 = NOVALUE;
    int _4285 = NOVALUE;
    int _4284 = NOVALUE;
    int _4283 = NOVALUE;
    int _4282 = NOVALUE;
    int _4280 = NOVALUE;
    int _4279 = NOVALUE;
    int _4278 = NOVALUE;
    int _4277 = NOVALUE;
    int _4276 = NOVALUE;
    int _4275 = NOVALUE;
    int _4273 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if eu:find('*', filename) or eu:find('?', filename) then return FILETYPE_UNDEFINED end if*/
    _4273 = find_from(42, _filename_7958, 1);
    if (_4273 != 0) {
        goto L1; // [10] 24
    }
    _4275 = find_from(63, _filename_7958, 1);
    if (_4275 == 0)
    {
        _4275 = NOVALUE;
        goto L2; // [20] 31
    }
    else{
        _4275 = NOVALUE;
    }
L1: 
    DeRefDS(_filename_7958);
    DeRef(_dirfil_7959);
    return -1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 2 and filename[2] = ':' then*/
    if (IS_SEQUENCE(_filename_7958)){
            _4276 = SEQ_PTR(_filename_7958)->length;
    }
    else {
        _4276 = 1;
    }
    _4277 = (_4276 == 2);
    _4276 = NOVALUE;
    if (_4277 == 0) {
        goto L3; // [42] 65
    }
    _2 = (int)SEQ_PTR(_filename_7958);
    _4279 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4279)) {
        _4280 = (_4279 == 58);
    }
    else {
        _4280 = binary_op(EQUALS, _4279, 58);
    }
    _4279 = NOVALUE;
    if (_4280 == 0) {
        DeRef(_4280);
        _4280 = NOVALUE;
        goto L3; // [55] 65
    }
    else {
        if (!IS_ATOM_INT(_4280) && DBL_PTR(_4280)->dbl == 0.0){
            DeRef(_4280);
            _4280 = NOVALUE;
            goto L3; // [55] 65
        }
        DeRef(_4280);
        _4280 = NOVALUE;
    }
    DeRef(_4280);
    _4280 = NOVALUE;

    /** 			filename &= "\\"*/
    Concat((object_ptr)&_filename_7958, _filename_7958, _958);
L3: 

    /** 	dirfil = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_dirfil_7959);
    _dirfil_7959 = machine(22, _filename_7958);

    /** 	if sequence(dirfil) then*/
    _4282 = IS_SEQUENCE(_dirfil_7959);
    if (_4282 == 0)
    {
        _4282 = NOVALUE;
        goto L4; // [81] 169
    }
    else{
        _4282 = NOVALUE;
    }

    /** 		if length( dirfil ) > 1 or eu:find('d', dirfil[1][2]) or (length(filename)=3 and filename[2]=':') then*/
    if (IS_SEQUENCE(_dirfil_7959)){
            _4283 = SEQ_PTR(_dirfil_7959)->length;
    }
    else {
        _4283 = 1;
    }
    _4284 = (_4283 > 1);
    _4283 = NOVALUE;
    if (_4284 != 0) {
        _4285 = 1;
        goto L5; // [93] 114
    }
    _2 = (int)SEQ_PTR(_dirfil_7959);
    _4286 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4286);
    _4287 = (int)*(((s1_ptr)_2)->base + 2);
    _4286 = NOVALUE;
    _4288 = find_from(100, _4287, 1);
    _4287 = NOVALUE;
    _4285 = (_4288 != 0);
L5: 
    if (_4285 != 0) {
        goto L6; // [114] 146
    }
    if (IS_SEQUENCE(_filename_7958)){
            _4290 = SEQ_PTR(_filename_7958)->length;
    }
    else {
        _4290 = 1;
    }
    _4291 = (_4290 == 3);
    _4290 = NOVALUE;
    if (_4291 == 0) {
        DeRef(_4292);
        _4292 = 0;
        goto L7; // [125] 141
    }
    _2 = (int)SEQ_PTR(_filename_7958);
    _4293 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4293)) {
        _4294 = (_4293 == 58);
    }
    else {
        _4294 = binary_op(EQUALS, _4293, 58);
    }
    _4293 = NOVALUE;
    if (IS_ATOM_INT(_4294))
    _4292 = (_4294 != 0);
    else
    _4292 = DBL_PTR(_4294)->dbl != 0.0;
L7: 
    if (_4292 == 0)
    {
        _4292 = NOVALUE;
        goto L8; // [142] 157
    }
    else{
        _4292 = NOVALUE;
    }
L6: 

    /** 			return FILETYPE_DIRECTORY*/
    DeRefDS(_filename_7958);
    DeRef(_dirfil_7959);
    DeRef(_4277);
    _4277 = NOVALUE;
    DeRef(_4284);
    _4284 = NOVALUE;
    DeRef(_4291);
    _4291 = NOVALUE;
    DeRef(_4294);
    _4294 = NOVALUE;
    return 2;
    goto L9; // [154] 178
L8: 

    /** 			return FILETYPE_FILE*/
    DeRefDS(_filename_7958);
    DeRef(_dirfil_7959);
    DeRef(_4277);
    _4277 = NOVALUE;
    DeRef(_4284);
    _4284 = NOVALUE;
    DeRef(_4291);
    _4291 = NOVALUE;
    DeRef(_4294);
    _4294 = NOVALUE;
    return 1;
    goto L9; // [166] 178
L4: 

    /** 		return FILETYPE_NOT_FOUND*/
    DeRefDS(_filename_7958);
    DeRef(_dirfil_7959);
    DeRef(_4277);
    _4277 = NOVALUE;
    DeRef(_4284);
    _4284 = NOVALUE;
    DeRef(_4291);
    _4291 = NOVALUE;
    DeRef(_4294);
    _4294 = NOVALUE;
    return 0;
L9: 
    ;
}


int _13file_exists(int _name_8016)
{
    int _pName_8019 = NOVALUE;
    int _r_8022 = NOVALUE;
    int _4309 = NOVALUE;
    int _4307 = NOVALUE;
    int _4305 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(name) then*/
    _4305 = IS_ATOM(_name_8016);
    if (_4305 == 0)
    {
        _4305 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _4305 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_name_8016);
    DeRef(_pName_8019);
    DeRef(_r_8022);
    return 0;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom pName = allocate_string(name)*/
    Ref(_name_8016);
    _0 = _pName_8019;
    _pName_8019 = _4allocate_string(_name_8016, 0);
    DeRef(_0);

    /** 		atom r = c_func(xGetFileAttributes, {pName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pName_8019);
    *((int *)(_2+4)) = _pName_8019;
    _4307 = MAKE_SEQ(_1);
    DeRef(_r_8022);
    _r_8022 = call_c(1, _13xGetFileAttributes_6992, _4307);
    DeRefDS(_4307);
    _4307 = NOVALUE;

    /** 		free(pName)*/
    Ref(_pName_8019);
    _4free(_pName_8019);

    /** 		return r > 0*/
    if (IS_ATOM_INT(_r_8022)) {
        _4309 = (_r_8022 > 0);
    }
    else {
        _4309 = (DBL_PTR(_r_8022)->dbl > (double)0);
    }
    DeRef(_name_8016);
    DeRef(_pName_8019);
    DeRef(_r_8022);
    return _4309;
    ;
}


int _13file_timestamp(int _fname_8029)
{
    int _d_8030 = NOVALUE;
    int _dir_inlined_dir_at_4_8032 = NOVALUE;
    int _4323 = NOVALUE;
    int _4322 = NOVALUE;
    int _4321 = NOVALUE;
    int _4320 = NOVALUE;
    int _4319 = NOVALUE;
    int _4318 = NOVALUE;
    int _4317 = NOVALUE;
    int _4316 = NOVALUE;
    int _4315 = NOVALUE;
    int _4314 = NOVALUE;
    int _4313 = NOVALUE;
    int _4312 = NOVALUE;
    int _4311 = NOVALUE;
    int _4310 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d = dir(fname)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_d_8030);
    _d_8030 = machine(22, _fname_8029);

    /** 	if atom(d) then return -1 end if*/
    _4310 = IS_ATOM(_d_8030);
    if (_4310 == 0)
    {
        _4310 = NOVALUE;
        goto L1; // [19] 27
    }
    else{
        _4310 = NOVALUE;
    }
    DeRefDS(_fname_8029);
    DeRef(_d_8030);
    return -1;
L1: 

    /** 	return datetime:new(d[1][D_YEAR], d[1][D_MONTH], d[1][D_DAY],*/
    _2 = (int)SEQ_PTR(_d_8030);
    _4311 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4311);
    _4312 = (int)*(((s1_ptr)_2)->base + 4);
    _4311 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_8030);
    _4313 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4313);
    _4314 = (int)*(((s1_ptr)_2)->base + 5);
    _4313 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_8030);
    _4315 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4315);
    _4316 = (int)*(((s1_ptr)_2)->base + 6);
    _4315 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_8030);
    _4317 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4317);
    _4318 = (int)*(((s1_ptr)_2)->base + 7);
    _4317 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_8030);
    _4319 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4319);
    _4320 = (int)*(((s1_ptr)_2)->base + 8);
    _4319 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_8030);
    _4321 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4321);
    _4322 = (int)*(((s1_ptr)_2)->base + 9);
    _4321 = NOVALUE;
    Ref(_4312);
    Ref(_4314);
    Ref(_4316);
    Ref(_4318);
    Ref(_4320);
    Ref(_4322);
    _4323 = _14new(_4312, _4314, _4316, _4318, _4320, _4322);
    _4312 = NOVALUE;
    _4314 = NOVALUE;
    _4316 = NOVALUE;
    _4318 = NOVALUE;
    _4320 = NOVALUE;
    _4322 = NOVALUE;
    DeRefDS(_fname_8029);
    DeRef(_d_8030);
    return _4323;
    ;
}


int _13locate_file(int _filename_8163, int _search_list_8164, int _subdir_8165)
{
    int _extra_paths_8166 = NOVALUE;
    int _this_path_8167 = NOVALUE;
    int _4463 = NOVALUE;
    int _4462 = NOVALUE;
    int _4460 = NOVALUE;
    int _4458 = NOVALUE;
    int _4456 = NOVALUE;
    int _4455 = NOVALUE;
    int _4454 = NOVALUE;
    int _4452 = NOVALUE;
    int _4451 = NOVALUE;
    int _4450 = NOVALUE;
    int _4448 = NOVALUE;
    int _4447 = NOVALUE;
    int _4446 = NOVALUE;
    int _4443 = NOVALUE;
    int _4442 = NOVALUE;
    int _4440 = NOVALUE;
    int _4438 = NOVALUE;
    int _4437 = NOVALUE;
    int _4434 = NOVALUE;
    int _4429 = NOVALUE;
    int _4425 = NOVALUE;
    int _4416 = NOVALUE;
    int _4413 = NOVALUE;
    int _4410 = NOVALUE;
    int _4409 = NOVALUE;
    int _4405 = NOVALUE;
    int _4402 = NOVALUE;
    int _4400 = NOVALUE;
    int _4396 = NOVALUE;
    int _4394 = NOVALUE;
    int _4393 = NOVALUE;
    int _4391 = NOVALUE;
    int _4390 = NOVALUE;
    int _4387 = NOVALUE;
    int _4386 = NOVALUE;
    int _4383 = NOVALUE;
    int _4381 = NOVALUE;
    int _4380 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path(filename) then*/
    RefDS(_filename_8163);
    _4380 = _13absolute_path(_filename_8163);
    if (_4380 == 0) {
        DeRef(_4380);
        _4380 = NOVALUE;
        goto L1; // [13] 23
    }
    else {
        if (!IS_ATOM_INT(_4380) && DBL_PTR(_4380)->dbl == 0.0){
            DeRef(_4380);
            _4380 = NOVALUE;
            goto L1; // [13] 23
        }
        DeRef(_4380);
        _4380 = NOVALUE;
    }
    DeRef(_4380);
    _4380 = NOVALUE;

    /** 		return filename*/
    DeRefDS(_search_list_8164);
    DeRefDS(_subdir_8165);
    DeRef(_extra_paths_8166);
    DeRef(_this_path_8167);
    return _filename_8163;
L1: 

    /** 	if length(search_list) = 0 then*/
    if (IS_SEQUENCE(_search_list_8164)){
            _4381 = SEQ_PTR(_search_list_8164)->length;
    }
    else {
        _4381 = 1;
    }
    if (_4381 != 0)
    goto L2; // [28] 281

    /** 		search_list = append(search_list, "." & SLASH)*/
    Append(&_4383, _3725, 92);
    RefDS(_4383);
    Append(&_search_list_8164, _search_list_8164, _4383);
    DeRefDS(_4383);
    _4383 = NOVALUE;

    /** 		extra_paths = command_line()*/
    DeRef(_extra_paths_8166);
    _extra_paths_8166 = Command_Line();

    /** 		extra_paths = canonical_path(dirname(extra_paths[2]), 1)*/
    _2 = (int)SEQ_PTR(_extra_paths_8166);
    _4386 = (int)*(((s1_ptr)_2)->base + 2);
    RefDS(_4386);
    _4387 = _13dirname(_4386, 0);
    _4386 = NOVALUE;
    _0 = _extra_paths_8166;
    _extra_paths_8166 = _13canonical_path(_4387, 1, 0);
    DeRefDS(_0);
    _4387 = NOVALUE;

    /** 		search_list = append(search_list, extra_paths)*/
    Ref(_extra_paths_8166);
    Append(&_search_list_8164, _search_list_8164, _extra_paths_8166);

    /** 		ifdef UNIX then*/

    /** 			extra_paths = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _4390 = EGetEnv(_4013);
    _4391 = EGetEnv(_4015);
    if (IS_SEQUENCE(_4390) && IS_ATOM(_4391)) {
        Ref(_4391);
        Append(&_extra_paths_8166, _4390, _4391);
    }
    else if (IS_ATOM(_4390) && IS_SEQUENCE(_4391)) {
        Ref(_4390);
        Prepend(&_extra_paths_8166, _4391, _4390);
    }
    else {
        Concat((object_ptr)&_extra_paths_8166, _4390, _4391);
        DeRef(_4390);
        _4390 = NOVALUE;
    }
    DeRef(_4390);
    _4390 = NOVALUE;
    DeRef(_4391);
    _4391 = NOVALUE;

    /** 		if sequence(extra_paths) then*/
    _4393 = 1;
    if (_4393 == 0)
    {
        _4393 = NOVALUE;
        goto L3; // [90] 104
    }
    else{
        _4393 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    Append(&_4394, _extra_paths_8166, 92);
    RefDS(_4394);
    Append(&_search_list_8164, _search_list_8164, _4394);
    DeRefDS(_4394);
    _4394 = NOVALUE;
L3: 

    /** 		search_list = append(search_list, ".." & SLASH)*/
    Append(&_4396, _3756, 92);
    RefDS(_4396);
    Append(&_search_list_8164, _search_list_8164, _4396);
    DeRefDS(_4396);
    _4396 = NOVALUE;

    /** 		extra_paths = getenv("EUDIR")*/
    DeRef(_extra_paths_8166);
    _extra_paths_8166 = EGetEnv(_4398);

    /** 		if sequence(extra_paths) then*/
    _4400 = IS_SEQUENCE(_extra_paths_8166);
    if (_4400 == 0)
    {
        _4400 = NOVALUE;
        goto L4; // [124] 154
    }
    else{
        _4400 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH & "bin" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4401;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8166;
        Concat_N((object_ptr)&_4402, concat_list, 4);
    }
    RefDS(_4402);
    Append(&_search_list_8164, _search_list_8164, _4402);
    DeRefDS(_4402);
    _4402 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "docs" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4404;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8166;
        Concat_N((object_ptr)&_4405, concat_list, 4);
    }
    RefDS(_4405);
    Append(&_search_list_8164, _search_list_8164, _4405);
    DeRefDS(_4405);
    _4405 = NOVALUE;
L4: 

    /** 		extra_paths = getenv("EUDIST")*/
    DeRef(_extra_paths_8166);
    _extra_paths_8166 = EGetEnv(_4407);

    /** 		if sequence(extra_paths) then*/
    _4409 = IS_SEQUENCE(_extra_paths_8166);
    if (_4409 == 0)
    {
        _4409 = NOVALUE;
        goto L5; // [164] 204
    }
    else{
        _4409 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    if (IS_SEQUENCE(_extra_paths_8166) && IS_ATOM(92)) {
        Append(&_4410, _extra_paths_8166, 92);
    }
    else if (IS_ATOM(_extra_paths_8166) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_4410, _extra_paths_8166, 92);
    }
    RefDS(_4410);
    Append(&_search_list_8164, _search_list_8164, _4410);
    DeRefDS(_4410);
    _4410 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "etc" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4412;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8166;
        Concat_N((object_ptr)&_4413, concat_list, 4);
    }
    RefDS(_4413);
    Append(&_search_list_8164, _search_list_8164, _4413);
    DeRefDS(_4413);
    _4413 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "data" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4415;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_8166;
        Concat_N((object_ptr)&_4416, concat_list, 4);
    }
    RefDS(_4416);
    Append(&_search_list_8164, _search_list_8164, _4416);
    DeRefDS(_4416);
    _4416 = NOVALUE;
L5: 

    /** 		ifdef UNIX then*/

    /** 		search_list &= include_paths(1)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_4424);
    *((int *)(_2+4)) = _4424;
    RefDS(_4423);
    *((int *)(_2+8)) = _4423;
    RefDS(_4422);
    *((int *)(_2+12)) = _4422;
    RefDS(_4421);
    *((int *)(_2+16)) = _4421;
    RefDS(_4420);
    *((int *)(_2+20)) = _4420;
    _4425 = MAKE_SEQ(_1);
    Concat((object_ptr)&_search_list_8164, _search_list_8164, _4425);
    DeRefDS(_4425);
    _4425 = NOVALUE;

    /** 		extra_paths = getenv("USERPATH")*/
    DeRef(_extra_paths_8166);
    _extra_paths_8166 = EGetEnv(_4427);

    /** 		if sequence(extra_paths) then*/
    _4429 = IS_SEQUENCE(_extra_paths_8166);
    if (_4429 == 0)
    {
        _4429 = NOVALUE;
        goto L6; // [230] 249
    }
    else{
        _4429 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_8166);
    _0 = _extra_paths_8166;
    _extra_paths_8166 = _21split(_extra_paths_8166, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_8164) && IS_ATOM(_extra_paths_8166)) {
        Ref(_extra_paths_8166);
        Append(&_search_list_8164, _search_list_8164, _extra_paths_8166);
    }
    else if (IS_ATOM(_search_list_8164) && IS_SEQUENCE(_extra_paths_8166)) {
    }
    else {
        Concat((object_ptr)&_search_list_8164, _search_list_8164, _extra_paths_8166);
    }
L6: 

    /** 		extra_paths = getenv("PATH")*/
    DeRef(_extra_paths_8166);
    _extra_paths_8166 = EGetEnv(_4432);

    /** 		if sequence(extra_paths) then*/
    _4434 = IS_SEQUENCE(_extra_paths_8166);
    if (_4434 == 0)
    {
        _4434 = NOVALUE;
        goto L7; // [259] 306
    }
    else{
        _4434 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_8166);
    _0 = _extra_paths_8166;
    _extra_paths_8166 = _21split(_extra_paths_8166, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_8164) && IS_ATOM(_extra_paths_8166)) {
        Ref(_extra_paths_8166);
        Append(&_search_list_8164, _search_list_8164, _extra_paths_8166);
    }
    else if (IS_ATOM(_search_list_8164) && IS_SEQUENCE(_extra_paths_8166)) {
    }
    else {
        Concat((object_ptr)&_search_list_8164, _search_list_8164, _extra_paths_8166);
    }
    goto L7; // [278] 306
L2: 

    /** 		if integer(search_list[1]) then*/
    _2 = (int)SEQ_PTR(_search_list_8164);
    _4437 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4437))
    _4438 = 1;
    else if (IS_ATOM_DBL(_4437))
    _4438 = IS_ATOM_INT(DoubleToInt(_4437));
    else
    _4438 = 0;
    _4437 = NOVALUE;
    if (_4438 == 0)
    {
        _4438 = NOVALUE;
        goto L8; // [290] 305
    }
    else{
        _4438 = NOVALUE;
    }

    /** 			search_list = stdseq:split(search_list, PATHSEP)*/
    RefDS(_search_list_8164);
    _0 = _search_list_8164;
    _search_list_8164 = _21split(_search_list_8164, 59, 0, 0);
    DeRefDS(_0);
L8: 
L7: 

    /** 	if length(subdir) > 0 then*/
    if (IS_SEQUENCE(_subdir_8165)){
            _4440 = SEQ_PTR(_subdir_8165)->length;
    }
    else {
        _4440 = 1;
    }
    if (_4440 <= 0)
    goto L9; // [311] 336

    /** 		if subdir[$] != SLASH then*/
    if (IS_SEQUENCE(_subdir_8165)){
            _4442 = SEQ_PTR(_subdir_8165)->length;
    }
    else {
        _4442 = 1;
    }
    _2 = (int)SEQ_PTR(_subdir_8165);
    _4443 = (int)*(((s1_ptr)_2)->base + _4442);
    if (binary_op_a(EQUALS, _4443, 92)){
        _4443 = NOVALUE;
        goto LA; // [324] 335
    }
    _4443 = NOVALUE;

    /** 			subdir &= SLASH*/
    Append(&_subdir_8165, _subdir_8165, 92);
LA: 
L9: 

    /** 	for i = 1 to length(search_list) do*/
    if (IS_SEQUENCE(_search_list_8164)){
            _4446 = SEQ_PTR(_search_list_8164)->length;
    }
    else {
        _4446 = 1;
    }
    {
        int _i_8246;
        _i_8246 = 1;
LB: 
        if (_i_8246 > _4446){
            goto LC; // [341] 466
        }

        /** 		if length(search_list[i]) = 0 then*/
        _2 = (int)SEQ_PTR(_search_list_8164);
        _4447 = (int)*(((s1_ptr)_2)->base + _i_8246);
        if (IS_SEQUENCE(_4447)){
                _4448 = SEQ_PTR(_4447)->length;
        }
        else {
            _4448 = 1;
        }
        _4447 = NOVALUE;
        if (_4448 != 0)
        goto LD; // [357] 366

        /** 			continue*/
        goto LE; // [363] 461
LD: 

        /** 		if search_list[i][$] != SLASH then*/
        _2 = (int)SEQ_PTR(_search_list_8164);
        _4450 = (int)*(((s1_ptr)_2)->base + _i_8246);
        if (IS_SEQUENCE(_4450)){
                _4451 = SEQ_PTR(_4450)->length;
        }
        else {
            _4451 = 1;
        }
        _2 = (int)SEQ_PTR(_4450);
        _4452 = (int)*(((s1_ptr)_2)->base + _4451);
        _4450 = NOVALUE;
        if (binary_op_a(EQUALS, _4452, 92)){
            _4452 = NOVALUE;
            goto LF; // [379] 398
        }
        _4452 = NOVALUE;

        /** 			search_list[i] &= SLASH*/
        _2 = (int)SEQ_PTR(_search_list_8164);
        _4454 = (int)*(((s1_ptr)_2)->base + _i_8246);
        if (IS_SEQUENCE(_4454) && IS_ATOM(92)) {
            Append(&_4455, _4454, 92);
        }
        else if (IS_ATOM(_4454) && IS_SEQUENCE(92)) {
        }
        else {
            Concat((object_ptr)&_4455, _4454, 92);
            _4454 = NOVALUE;
        }
        _4454 = NOVALUE;
        _2 = (int)SEQ_PTR(_search_list_8164);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _search_list_8164 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8246);
        _1 = *(int *)_2;
        *(int *)_2 = _4455;
        if( _1 != _4455 ){
            DeRef(_1);
        }
        _4455 = NOVALUE;
LF: 

        /** 		if length(subdir) > 0 then*/
        if (IS_SEQUENCE(_subdir_8165)){
                _4456 = SEQ_PTR(_subdir_8165)->length;
        }
        else {
            _4456 = 1;
        }
        if (_4456 <= 0)
        goto L10; // [403] 422

        /** 			this_path = search_list[i] & subdir & filename*/
        _2 = (int)SEQ_PTR(_search_list_8164);
        _4458 = (int)*(((s1_ptr)_2)->base + _i_8246);
        {
            int concat_list[3];

            concat_list[0] = _filename_8163;
            concat_list[1] = _subdir_8165;
            concat_list[2] = _4458;
            Concat_N((object_ptr)&_this_path_8167, concat_list, 3);
        }
        _4458 = NOVALUE;
        goto L11; // [419] 433
L10: 

        /** 			this_path = search_list[i] & filename*/
        _2 = (int)SEQ_PTR(_search_list_8164);
        _4460 = (int)*(((s1_ptr)_2)->base + _i_8246);
        if (IS_SEQUENCE(_4460) && IS_ATOM(_filename_8163)) {
        }
        else if (IS_ATOM(_4460) && IS_SEQUENCE(_filename_8163)) {
            Ref(_4460);
            Prepend(&_this_path_8167, _filename_8163, _4460);
        }
        else {
            Concat((object_ptr)&_this_path_8167, _4460, _filename_8163);
            _4460 = NOVALUE;
        }
        _4460 = NOVALUE;
L11: 

        /** 		if file_exists(this_path) then*/
        RefDS(_this_path_8167);
        _4462 = _13file_exists(_this_path_8167);
        if (_4462 == 0) {
            DeRef(_4462);
            _4462 = NOVALUE;
            goto L12; // [441] 459
        }
        else {
            if (!IS_ATOM_INT(_4462) && DBL_PTR(_4462)->dbl == 0.0){
                DeRef(_4462);
                _4462 = NOVALUE;
                goto L12; // [441] 459
            }
            DeRef(_4462);
            _4462 = NOVALUE;
        }
        DeRef(_4462);
        _4462 = NOVALUE;

        /** 			return canonical_path(this_path)*/
        RefDS(_this_path_8167);
        _4463 = _13canonical_path(_this_path_8167, 0, 0);
        DeRefDS(_filename_8163);
        DeRefDS(_search_list_8164);
        DeRefDS(_subdir_8165);
        DeRef(_extra_paths_8166);
        DeRefDS(_this_path_8167);
        _4447 = NOVALUE;
        return _4463;
L12: 

        /** 	end for*/
LE: 
        _i_8246 = _i_8246 + 1;
        goto LB; // [461] 348
LC: 
        ;
    }

    /** 	return filename*/
    DeRefDS(_search_list_8164);
    DeRefDS(_subdir_8165);
    DeRef(_extra_paths_8166);
    DeRef(_this_path_8167);
    _4447 = NOVALUE;
    DeRef(_4463);
    _4463 = NOVALUE;
    return _filename_8163;
    ;
}


int _13count_files(int _orig_path_8323, int _dir_info_8324, int _inst_8325)
{
    int _pos_8326 = NOVALUE;
    int _ext_8327 = NOVALUE;
    int _fileext_inlined_fileext_at_243_8370 = NOVALUE;
    int _data_inlined_fileext_at_243_8369 = NOVALUE;
    int _path_inlined_fileext_at_240_8368 = NOVALUE;
    int _4559 = NOVALUE;
    int _4558 = NOVALUE;
    int _4557 = NOVALUE;
    int _4555 = NOVALUE;
    int _4554 = NOVALUE;
    int _4553 = NOVALUE;
    int _4552 = NOVALUE;
    int _4550 = NOVALUE;
    int _4549 = NOVALUE;
    int _4547 = NOVALUE;
    int _4546 = NOVALUE;
    int _4545 = NOVALUE;
    int _4544 = NOVALUE;
    int _4543 = NOVALUE;
    int _4542 = NOVALUE;
    int _4541 = NOVALUE;
    int _4539 = NOVALUE;
    int _4538 = NOVALUE;
    int _4536 = NOVALUE;
    int _4535 = NOVALUE;
    int _4534 = NOVALUE;
    int _4533 = NOVALUE;
    int _4532 = NOVALUE;
    int _4531 = NOVALUE;
    int _4530 = NOVALUE;
    int _4529 = NOVALUE;
    int _4528 = NOVALUE;
    int _4527 = NOVALUE;
    int _4526 = NOVALUE;
    int _4525 = NOVALUE;
    int _4524 = NOVALUE;
    int _4523 = NOVALUE;
    int _4521 = NOVALUE;
    int _4520 = NOVALUE;
    int _4519 = NOVALUE;
    int _4518 = NOVALUE;
    int _4516 = NOVALUE;
    int _4515 = NOVALUE;
    int _4514 = NOVALUE;
    int _4513 = NOVALUE;
    int _4512 = NOVALUE;
    int _4511 = NOVALUE;
    int _4510 = NOVALUE;
    int _4508 = NOVALUE;
    int _4507 = NOVALUE;
    int _4506 = NOVALUE;
    int _4505 = NOVALUE;
    int _4504 = NOVALUE;
    int _4503 = NOVALUE;
    int _4500 = NOVALUE;
    int _4499 = NOVALUE;
    int _4498 = NOVALUE;
    int _4497 = NOVALUE;
    int _4496 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer pos = 0*/
    _pos_8326 = 0;

    /** 	orig_path = orig_path*/
    RefDS(_orig_path_8323);
    DeRefDS(_orig_path_8323);
    _orig_path_8323 = _orig_path_8323;

    /** 	if equal(dir_info[D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4496 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4496 == _3725)
    _4497 = 1;
    else if (IS_ATOM_INT(_4496) && IS_ATOM_INT(_3725))
    _4497 = 0;
    else
    _4497 = (compare(_4496, _3725) == 0);
    _4496 = NOVALUE;
    if (_4497 == 0)
    {
        _4497 = NOVALUE;
        goto L1; // [31] 41
    }
    else{
        _4497 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_8323);
    DeRefDS(_dir_info_8324);
    DeRefDS(_inst_8325);
    DeRef(_ext_8327);
    return 0;
L1: 

    /** 	if equal(dir_info[D_NAME], "..") then*/
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4498 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4498 == _3756)
    _4499 = 1;
    else if (IS_ATOM_INT(_4498) && IS_ATOM_INT(_3756))
    _4499 = 0;
    else
    _4499 = (compare(_4498, _3756) == 0);
    _4498 = NOVALUE;
    if (_4499 == 0)
    {
        _4499 = NOVALUE;
        goto L2; // [53] 63
    }
    else{
        _4499 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_8323);
    DeRefDS(_dir_info_8324);
    DeRefDS(_inst_8325);
    DeRef(_ext_8327);
    return 0;
L2: 

    /** 	if inst[1] = 0 then -- count all is false*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4500 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _4500, 0)){
        _4500 = NOVALUE;
        goto L3; // [69] 120
    }
    _4500 = NOVALUE;

    /** 		if find('h', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4503 = (int)*(((s1_ptr)_2)->base + 2);
    _4504 = find_from(104, _4503, 1);
    _4503 = NOVALUE;
    if (_4504 == 0)
    {
        _4504 = NOVALUE;
        goto L4; // [86] 96
    }
    else{
        _4504 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_8323);
    DeRefDS(_dir_info_8324);
    DeRefDS(_inst_8325);
    DeRef(_ext_8327);
    return 0;
L4: 

    /** 		if find('s', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4505 = (int)*(((s1_ptr)_2)->base + 2);
    _4506 = find_from(115, _4505, 1);
    _4505 = NOVALUE;
    if (_4506 == 0)
    {
        _4506 = NOVALUE;
        goto L5; // [109] 119
    }
    else{
        _4506 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_8323);
    DeRefDS(_dir_info_8324);
    DeRefDS(_inst_8325);
    DeRef(_ext_8327);
    return 0;
L5: 
L3: 

    /** 	file_counters[inst[2]][COUNT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4507 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _13file_counters_8320 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4507))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4507)->dbl));
    else
    _3 = (int)(_4507 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4510 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4511 = (int)*(((s1_ptr)_2)->base + 3);
    _4508 = NOVALUE;
    if (IS_ATOM_INT(_4511) && IS_ATOM_INT(_4510)) {
        _4512 = _4511 + _4510;
        if ((long)((unsigned long)_4512 + (unsigned long)HIGH_BITS) >= 0) 
        _4512 = NewDouble((double)_4512);
    }
    else {
        _4512 = binary_op(PLUS, _4511, _4510);
    }
    _4511 = NOVALUE;
    _4510 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4512;
    if( _1 != _4512 ){
        DeRef(_1);
    }
    _4512 = NOVALUE;
    _4508 = NOVALUE;

    /** 	if find('d', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4513 = (int)*(((s1_ptr)_2)->base + 2);
    _4514 = find_from(100, _4513, 1);
    _4513 = NOVALUE;
    if (_4514 == 0)
    {
        _4514 = NOVALUE;
        goto L6; // [166] 199
    }
    else{
        _4514 = NOVALUE;
    }

    /** 		file_counters[inst[2]][COUNT_DIRS] += 1*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4515 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _13file_counters_8320 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4515))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4515)->dbl));
    else
    _3 = (int)(_4515 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4518 = (int)*(((s1_ptr)_2)->base + 1);
    _4516 = NOVALUE;
    if (IS_ATOM_INT(_4518)) {
        _4519 = _4518 + 1;
        if (_4519 > MAXINT){
            _4519 = NewDouble((double)_4519);
        }
    }
    else
    _4519 = binary_op(PLUS, 1, _4518);
    _4518 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _4519;
    if( _1 != _4519 ){
        DeRef(_1);
    }
    _4519 = NOVALUE;
    _4516 = NOVALUE;
    goto L7; // [196] 504
L6: 

    /** 		file_counters[inst[2]][COUNT_FILES] += 1*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4520 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _13file_counters_8320 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4520))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4520)->dbl));
    else
    _3 = (int)(_4520 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4523 = (int)*(((s1_ptr)_2)->base + 2);
    _4521 = NOVALUE;
    if (IS_ATOM_INT(_4523)) {
        _4524 = _4523 + 1;
        if (_4524 > MAXINT){
            _4524 = NewDouble((double)_4524);
        }
    }
    else
    _4524 = binary_op(PLUS, 1, _4523);
    _4523 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4524;
    if( _1 != _4524 ){
        DeRef(_1);
    }
    _4524 = NOVALUE;
    _4521 = NOVALUE;

    /** 		ifdef not UNIX then*/

    /** 			ext = fileext(lower(dir_info[D_NAME]))*/
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4525 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_4525);
    _4526 = _10lower(_4525);
    _4525 = NOVALUE;
    DeRef(_path_inlined_fileext_at_240_8368);
    _path_inlined_fileext_at_240_8368 = _4526;
    _4526 = NOVALUE;

    /** 	data = pathinfo(path)*/
    Ref(_path_inlined_fileext_at_240_8368);
    _0 = _data_inlined_fileext_at_243_8369;
    _data_inlined_fileext_at_243_8369 = _13pathinfo(_path_inlined_fileext_at_240_8368, 0);
    DeRef(_0);

    /** 	return data[4]*/
    DeRef(_ext_8327);
    _2 = (int)SEQ_PTR(_data_inlined_fileext_at_243_8369);
    _ext_8327 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_ext_8327);
    DeRef(_path_inlined_fileext_at_240_8368);
    _path_inlined_fileext_at_240_8368 = NOVALUE;
    DeRef(_data_inlined_fileext_at_243_8369);
    _data_inlined_fileext_at_243_8369 = NOVALUE;

    /** 		pos = 0*/
    _pos_8326 = 0;

    /** 		for i = 1 to length(file_counters[inst[2]][COUNT_TYPES]) do*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4527 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!IS_ATOM_INT(_4527)){
        _4528 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4527)->dbl));
    }
    else{
        _4528 = (int)*(((s1_ptr)_2)->base + _4527);
    }
    _2 = (int)SEQ_PTR(_4528);
    _4529 = (int)*(((s1_ptr)_2)->base + 4);
    _4528 = NOVALUE;
    if (IS_SEQUENCE(_4529)){
            _4530 = SEQ_PTR(_4529)->length;
    }
    else {
        _4530 = 1;
    }
    _4529 = NOVALUE;
    {
        int _i_8372;
        _i_8372 = 1;
L8: 
        if (_i_8372 > _4530){
            goto L9; // [291] 352
        }

        /** 			if equal(file_counters[inst[2]][COUNT_TYPES][i][EXT_NAME], ext) then*/
        _2 = (int)SEQ_PTR(_inst_8325);
        _4531 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_13file_counters_8320);
        if (!IS_ATOM_INT(_4531)){
            _4532 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4531)->dbl));
        }
        else{
            _4532 = (int)*(((s1_ptr)_2)->base + _4531);
        }
        _2 = (int)SEQ_PTR(_4532);
        _4533 = (int)*(((s1_ptr)_2)->base + 4);
        _4532 = NOVALUE;
        _2 = (int)SEQ_PTR(_4533);
        _4534 = (int)*(((s1_ptr)_2)->base + _i_8372);
        _4533 = NOVALUE;
        _2 = (int)SEQ_PTR(_4534);
        _4535 = (int)*(((s1_ptr)_2)->base + 1);
        _4534 = NOVALUE;
        if (_4535 == _ext_8327)
        _4536 = 1;
        else if (IS_ATOM_INT(_4535) && IS_ATOM_INT(_ext_8327))
        _4536 = 0;
        else
        _4536 = (compare(_4535, _ext_8327) == 0);
        _4535 = NOVALUE;
        if (_4536 == 0)
        {
            _4536 = NOVALUE;
            goto LA; // [332] 345
        }
        else{
            _4536 = NOVALUE;
        }

        /** 				pos = i*/
        _pos_8326 = _i_8372;

        /** 				exit*/
        goto L9; // [342] 352
LA: 

        /** 		end for*/
        _i_8372 = _i_8372 + 1;
        goto L8; // [347] 298
L9: 
        ;
    }

    /** 		if pos = 0 then*/
    if (_pos_8326 != 0)
    goto LB; // [354] 419

    /** 			file_counters[inst[2]][COUNT_TYPES] &= {{ext, 0, 0}}*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4538 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _13file_counters_8320 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4538))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4538)->dbl));
    else
    _3 = (int)(_4538 + ((s1_ptr)_2)->base);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ext_8327);
    *((int *)(_2+4)) = _ext_8327;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    _4541 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4541;
    _4542 = MAKE_SEQ(_1);
    _4541 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4543 = (int)*(((s1_ptr)_2)->base + 4);
    _4539 = NOVALUE;
    if (IS_SEQUENCE(_4543) && IS_ATOM(_4542)) {
    }
    else if (IS_ATOM(_4543) && IS_SEQUENCE(_4542)) {
        Ref(_4543);
        Prepend(&_4544, _4542, _4543);
    }
    else {
        Concat((object_ptr)&_4544, _4543, _4542);
        _4543 = NOVALUE;
    }
    _4543 = NOVALUE;
    DeRefDS(_4542);
    _4542 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _4544;
    if( _1 != _4544 ){
        DeRef(_1);
    }
    _4544 = NOVALUE;
    _4539 = NOVALUE;

    /** 			pos = length(file_counters[inst[2]][COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4545 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!IS_ATOM_INT(_4545)){
        _4546 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4545)->dbl));
    }
    else{
        _4546 = (int)*(((s1_ptr)_2)->base + _4545);
    }
    _2 = (int)SEQ_PTR(_4546);
    _4547 = (int)*(((s1_ptr)_2)->base + 4);
    _4546 = NOVALUE;
    if (IS_SEQUENCE(_4547)){
            _pos_8326 = SEQ_PTR(_4547)->length;
    }
    else {
        _pos_8326 = 1;
    }
    _4547 = NOVALUE;
LB: 

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4549 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _13file_counters_8320 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4549))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4549)->dbl));
    else
    _3 = (int)(_4549 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4550 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_8326 + ((s1_ptr)_2)->base);
    _4550 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4552 = (int)*(((s1_ptr)_2)->base + 2);
    _4550 = NOVALUE;
    if (IS_ATOM_INT(_4552)) {
        _4553 = _4552 + 1;
        if (_4553 > MAXINT){
            _4553 = NewDouble((double)_4553);
        }
    }
    else
    _4553 = binary_op(PLUS, 1, _4552);
    _4552 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4553;
    if( _1 != _4553 ){
        DeRef(_1);
    }
    _4553 = NOVALUE;
    _4550 = NOVALUE;

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_8325);
    _4554 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_13file_counters_8320);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _13file_counters_8320 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4554))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4554)->dbl));
    else
    _3 = (int)(_4554 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4555 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_8326 + ((s1_ptr)_2)->base);
    _4555 = NOVALUE;
    _2 = (int)SEQ_PTR(_dir_info_8324);
    _4557 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4558 = (int)*(((s1_ptr)_2)->base + 3);
    _4555 = NOVALUE;
    if (IS_ATOM_INT(_4558) && IS_ATOM_INT(_4557)) {
        _4559 = _4558 + _4557;
        if ((long)((unsigned long)_4559 + (unsigned long)HIGH_BITS) >= 0) 
        _4559 = NewDouble((double)_4559);
    }
    else {
        _4559 = binary_op(PLUS, _4558, _4557);
    }
    _4558 = NOVALUE;
    _4557 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4559;
    if( _1 != _4559 ){
        DeRef(_1);
    }
    _4559 = NOVALUE;
    _4555 = NOVALUE;
L7: 

    /** 	return 0*/
    DeRefDS(_orig_path_8323);
    DeRefDS(_dir_info_8324);
    DeRefDS(_inst_8325);
    DeRef(_ext_8327);
    _4507 = NOVALUE;
    _4515 = NOVALUE;
    _4520 = NOVALUE;
    _4527 = NOVALUE;
    _4529 = NOVALUE;
    _4531 = NOVALUE;
    _4538 = NOVALUE;
    _4545 = NOVALUE;
    _4547 = NOVALUE;
    _4549 = NOVALUE;
    _4554 = NOVALUE;
    return 0;
    ;
}



// 0xA77B4B8F
