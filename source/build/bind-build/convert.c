// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _11int_to_bytes(int _x_1834)
{
    int _a_1835 = NOVALUE;
    int _b_1836 = NOVALUE;
    int _c_1837 = NOVALUE;
    int _d_1838 = NOVALUE;
    int _783 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1834)) {
        _a_1835 = (_x_1834 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _a_1835 = Dremainder(DBL_PTR(_x_1834), &temp_d);
    }
    if (!IS_ATOM_INT(_a_1835)) {
        _1 = (long)(DBL_PTR(_a_1835)->dbl);
        if (UNIQUE(DBL_PTR(_a_1835)) && (DBL_PTR(_a_1835)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_1835);
        _a_1835 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1834;
    if (IS_ATOM_INT(_x_1834)) {
        if (256 > 0 && _x_1834 >= 0) {
            _x_1834 = _x_1834 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1834 / (double)256);
            if (_x_1834 != MININT)
            _x_1834 = (long)temp_dbl;
            else
            _x_1834 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1834, 256);
        _x_1834 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	b = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1834)) {
        _b_1836 = (_x_1834 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _b_1836 = Dremainder(DBL_PTR(_x_1834), &temp_d);
    }
    if (!IS_ATOM_INT(_b_1836)) {
        _1 = (long)(DBL_PTR(_b_1836)->dbl);
        if (UNIQUE(DBL_PTR(_b_1836)) && (DBL_PTR(_b_1836)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_1836);
        _b_1836 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1834;
    if (IS_ATOM_INT(_x_1834)) {
        if (256 > 0 && _x_1834 >= 0) {
            _x_1834 = _x_1834 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1834 / (double)256);
            if (_x_1834 != MININT)
            _x_1834 = (long)temp_dbl;
            else
            _x_1834 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1834, 256);
        _x_1834 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	c = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1834)) {
        _c_1837 = (_x_1834 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _c_1837 = Dremainder(DBL_PTR(_x_1834), &temp_d);
    }
    if (!IS_ATOM_INT(_c_1837)) {
        _1 = (long)(DBL_PTR(_c_1837)->dbl);
        if (UNIQUE(DBL_PTR(_c_1837)) && (DBL_PTR(_c_1837)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_1837);
        _c_1837 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1834;
    if (IS_ATOM_INT(_x_1834)) {
        if (256 > 0 && _x_1834 >= 0) {
            _x_1834 = _x_1834 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1834 / (double)256);
            if (_x_1834 != MININT)
            _x_1834 = (long)temp_dbl;
            else
            _x_1834 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1834, 256);
        _x_1834 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	d = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1834)) {
        _d_1838 = (_x_1834 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _d_1838 = Dremainder(DBL_PTR(_x_1834), &temp_d);
    }
    if (!IS_ATOM_INT(_d_1838)) {
        _1 = (long)(DBL_PTR(_d_1838)->dbl);
        if (UNIQUE(DBL_PTR(_d_1838)) && (DBL_PTR(_d_1838)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_d_1838);
        _d_1838 = _1;
    }

    /** 	return {a,b,c,d}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _a_1835;
    *((int *)(_2+8)) = _b_1836;
    *((int *)(_2+12)) = _c_1837;
    *((int *)(_2+16)) = _d_1838;
    _783 = MAKE_SEQ(_1);
    DeRef(_x_1834);
    return _783;
    ;
}


int _11int_to_bits(int _x_1876, int _nbits_1877)
{
    int _bits_1878 = NOVALUE;
    int _mask_1879 = NOVALUE;
    int _809 = NOVALUE;
    int _808 = NOVALUE;
    int _806 = NOVALUE;
    int _803 = NOVALUE;
    int _802 = NOVALUE;
    int _801 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if nbits < 1 then*/

    /** 	bits = repeat(0, nbits)*/
    DeRef(_bits_1878);
    _bits_1878 = Repeat(0, _nbits_1877);

    /** 	if nbits <= 32 then*/

    /** 		mask = 1*/
    DeRef(_mask_1879);
    _mask_1879 = 1;

    /** 		for i = 1 to nbits do*/
    _801 = _nbits_1877;
    {
        int _i_1886;
        _i_1886 = 1;
L1: 
        if (_i_1886 > _801){
            goto L2; // [38] 72
        }

        /** 			bits[i] = and_bits(x, mask) and 1*/
        if (IS_ATOM_INT(_x_1876) && IS_ATOM_INT(_mask_1879)) {
            {unsigned long tu;
                 tu = (unsigned long)_x_1876 & (unsigned long)_mask_1879;
                 _802 = MAKE_UINT(tu);
            }
        }
        else {
            if (IS_ATOM_INT(_x_1876)) {
                temp_d.dbl = (double)_x_1876;
                _802 = Dand_bits(&temp_d, DBL_PTR(_mask_1879));
            }
            else {
                if (IS_ATOM_INT(_mask_1879)) {
                    temp_d.dbl = (double)_mask_1879;
                    _802 = Dand_bits(DBL_PTR(_x_1876), &temp_d);
                }
                else
                _802 = Dand_bits(DBL_PTR(_x_1876), DBL_PTR(_mask_1879));
            }
        }
        if (IS_ATOM_INT(_802)) {
            _803 = (_802 != 0 && 1 != 0);
        }
        else {
            temp_d.dbl = (double)1;
            _803 = Dand(DBL_PTR(_802), &temp_d);
        }
        DeRef(_802);
        _802 = NOVALUE;
        _2 = (int)SEQ_PTR(_bits_1878);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_1878 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1886);
        _1 = *(int *)_2;
        *(int *)_2 = _803;
        if( _1 != _803 ){
            DeRef(_1);
        }
        _803 = NOVALUE;

        /** 			mask *= 2*/
        _0 = _mask_1879;
        if (IS_ATOM_INT(_mask_1879) && IS_ATOM_INT(_mask_1879)) {
            _mask_1879 = _mask_1879 + _mask_1879;
            if ((long)((unsigned long)_mask_1879 + (unsigned long)HIGH_BITS) >= 0) 
            _mask_1879 = NewDouble((double)_mask_1879);
        }
        else {
            if (IS_ATOM_INT(_mask_1879)) {
                _mask_1879 = NewDouble((double)_mask_1879 + DBL_PTR(_mask_1879)->dbl);
            }
            else {
                if (IS_ATOM_INT(_mask_1879)) {
                    _mask_1879 = NewDouble(DBL_PTR(_mask_1879)->dbl + (double)_mask_1879);
                }
                else
                _mask_1879 = NewDouble(DBL_PTR(_mask_1879)->dbl + DBL_PTR(_mask_1879)->dbl);
            }
        }
        DeRef(_0);

        /** 		end for*/
        _i_1886 = _i_1886 + 1;
        goto L1; // [67] 45
L2: 
        ;
    }
    goto L3; // [72] 128

    /** 		if x < 0 then*/
    if (binary_op_a(GREATEREQ, _x_1876, 0)){
        goto L4; // [77] 92
    }

    /** 			x += power(2, nbits) -- for 2's complement bit pattern*/
    _806 = power(2, _nbits_1877);
    _0 = _x_1876;
    if (IS_ATOM_INT(_x_1876) && IS_ATOM_INT(_806)) {
        _x_1876 = _x_1876 + _806;
        if ((long)((unsigned long)_x_1876 + (unsigned long)HIGH_BITS) >= 0) 
        _x_1876 = NewDouble((double)_x_1876);
    }
    else {
        if (IS_ATOM_INT(_x_1876)) {
            _x_1876 = NewDouble((double)_x_1876 + DBL_PTR(_806)->dbl);
        }
        else {
            if (IS_ATOM_INT(_806)) {
                _x_1876 = NewDouble(DBL_PTR(_x_1876)->dbl + (double)_806);
            }
            else
            _x_1876 = NewDouble(DBL_PTR(_x_1876)->dbl + DBL_PTR(_806)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_806);
    _806 = NOVALUE;
L4: 

    /** 		for i = 1 to nbits do*/
    _808 = _nbits_1877;
    {
        int _i_1897;
        _i_1897 = 1;
L5: 
        if (_i_1897 > _808){
            goto L6; // [97] 127
        }

        /** 			bits[i] = remainder(x, 2)*/
        if (IS_ATOM_INT(_x_1876)) {
            _809 = (_x_1876 % 2);
        }
        else {
            temp_d.dbl = (double)2;
            _809 = Dremainder(DBL_PTR(_x_1876), &temp_d);
        }
        _2 = (int)SEQ_PTR(_bits_1878);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_1878 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1897);
        _1 = *(int *)_2;
        *(int *)_2 = _809;
        if( _1 != _809 ){
            DeRef(_1);
        }
        _809 = NOVALUE;

        /** 			x = floor(x / 2)*/
        _0 = _x_1876;
        if (IS_ATOM_INT(_x_1876)) {
            _x_1876 = _x_1876 >> 1;
        }
        else {
            _1 = binary_op(DIVIDE, _x_1876, 2);
            _x_1876 = unary_op(FLOOR, _1);
            DeRef(_1);
        }
        DeRef(_0);

        /** 		end for*/
        _i_1897 = _i_1897 + 1;
        goto L5; // [122] 104
L6: 
        ;
    }
L3: 

    /** 	return bits*/
    DeRef(_x_1876);
    DeRef(_mask_1879);
    return _bits_1878;
    ;
}


int _11bits_to_int(int _bits_1903)
{
    int _value_1904 = NOVALUE;
    int _p_1905 = NOVALUE;
    int _812 = NOVALUE;
    int _811 = NOVALUE;
    int _0, _1, _2;
    

    /** 	value = 0*/
    DeRef(_value_1904);
    _value_1904 = 0;

    /** 	p = 1*/
    DeRef(_p_1905);
    _p_1905 = 1;

    /** 	for i = 1 to length(bits) do*/
    if (IS_SEQUENCE(_bits_1903)){
            _811 = SEQ_PTR(_bits_1903)->length;
    }
    else {
        _811 = 1;
    }
    {
        int _i_1907;
        _i_1907 = 1;
L1: 
        if (_i_1907 > _811){
            goto L2; // [18] 54
        }

        /** 		if bits[i] then*/
        _2 = (int)SEQ_PTR(_bits_1903);
        _812 = (int)*(((s1_ptr)_2)->base + _i_1907);
        if (_812 == 0) {
            _812 = NOVALUE;
            goto L3; // [31] 41
        }
        else {
            if (!IS_ATOM_INT(_812) && DBL_PTR(_812)->dbl == 0.0){
                _812 = NOVALUE;
                goto L3; // [31] 41
            }
            _812 = NOVALUE;
        }
        _812 = NOVALUE;

        /** 			value += p*/
        _0 = _value_1904;
        if (IS_ATOM_INT(_value_1904) && IS_ATOM_INT(_p_1905)) {
            _value_1904 = _value_1904 + _p_1905;
            if ((long)((unsigned long)_value_1904 + (unsigned long)HIGH_BITS) >= 0) 
            _value_1904 = NewDouble((double)_value_1904);
        }
        else {
            if (IS_ATOM_INT(_value_1904)) {
                _value_1904 = NewDouble((double)_value_1904 + DBL_PTR(_p_1905)->dbl);
            }
            else {
                if (IS_ATOM_INT(_p_1905)) {
                    _value_1904 = NewDouble(DBL_PTR(_value_1904)->dbl + (double)_p_1905);
                }
                else
                _value_1904 = NewDouble(DBL_PTR(_value_1904)->dbl + DBL_PTR(_p_1905)->dbl);
            }
        }
        DeRef(_0);
L3: 

        /** 		p += p*/
        _0 = _p_1905;
        if (IS_ATOM_INT(_p_1905) && IS_ATOM_INT(_p_1905)) {
            _p_1905 = _p_1905 + _p_1905;
            if ((long)((unsigned long)_p_1905 + (unsigned long)HIGH_BITS) >= 0) 
            _p_1905 = NewDouble((double)_p_1905);
        }
        else {
            if (IS_ATOM_INT(_p_1905)) {
                _p_1905 = NewDouble((double)_p_1905 + DBL_PTR(_p_1905)->dbl);
            }
            else {
                if (IS_ATOM_INT(_p_1905)) {
                    _p_1905 = NewDouble(DBL_PTR(_p_1905)->dbl + (double)_p_1905);
                }
                else
                _p_1905 = NewDouble(DBL_PTR(_p_1905)->dbl + DBL_PTR(_p_1905)->dbl);
            }
        }
        DeRef(_0);

        /** 	end for*/
        _i_1907 = _i_1907 + 1;
        goto L1; // [49] 25
L2: 
        ;
    }

    /** 	return value*/
    DeRefDS(_bits_1903);
    DeRef(_p_1905);
    return _value_1904;
    ;
}


int _11atom_to_float64(int _a_1915)
{
    int _815 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_A_TO_F64, a)*/
    _815 = machine(46, _a_1915);
    DeRef(_a_1915);
    return _815;
    ;
}


int _11atom_to_float32(int _a_1919)
{
    int _816 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_A_TO_F32, a)*/
    _816 = machine(48, _a_1919);
    DeRef(_a_1919);
    return _816;
    ;
}


int _11float64_to_atom(int _ieee64_1923)
{
    int _817 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F64_TO_A, ieee64)*/
    _817 = machine(47, _ieee64_1923);
    DeRefDS(_ieee64_1923);
    return _817;
    ;
}


int _11float32_to_atom(int _ieee32_1927)
{
    int _818 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    _818 = machine(49, _ieee32_1927);
    DeRefDS(_ieee32_1927);
    return _818;
    ;
}


int _11to_number(int _text_in_2007, int _return_bad_pos_2008)
{
    int _lDotFound_2009 = NOVALUE;
    int _lSignFound_2010 = NOVALUE;
    int _lCharValue_2011 = NOVALUE;
    int _lBadPos_2012 = NOVALUE;
    int _lLeftSize_2013 = NOVALUE;
    int _lRightSize_2014 = NOVALUE;
    int _lLeftValue_2015 = NOVALUE;
    int _lRightValue_2016 = NOVALUE;
    int _lBase_2017 = NOVALUE;
    int _lPercent_2019 = NOVALUE;
    int _lResult_2020 = NOVALUE;
    int _lDigitCount_2021 = NOVALUE;
    int _lCurrencyFound_2022 = NOVALUE;
    int _lLastDigit_2023 = NOVALUE;
    int _lChar_2024 = NOVALUE;
    int _944 = NOVALUE;
    int _943 = NOVALUE;
    int _936 = NOVALUE;
    int _934 = NOVALUE;
    int _933 = NOVALUE;
    int _928 = NOVALUE;
    int _927 = NOVALUE;
    int _926 = NOVALUE;
    int _925 = NOVALUE;
    int _924 = NOVALUE;
    int _923 = NOVALUE;
    int _919 = NOVALUE;
    int _915 = NOVALUE;
    int _906 = NOVALUE;
    int _894 = NOVALUE;
    int _893 = NOVALUE;
    int _887 = NOVALUE;
    int _885 = NOVALUE;
    int _879 = NOVALUE;
    int _878 = NOVALUE;
    int _877 = NOVALUE;
    int _876 = NOVALUE;
    int _875 = NOVALUE;
    int _874 = NOVALUE;
    int _873 = NOVALUE;
    int _872 = NOVALUE;
    int _871 = NOVALUE;
    int _863 = NOVALUE;
    int _862 = NOVALUE;
    int _861 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lDotFound = 0*/
    _lDotFound_2009 = 0;

    /** 	integer lSignFound = 2*/
    _lSignFound_2010 = 2;

    /** 	integer lBadPos = 0*/
    _lBadPos_2012 = 0;

    /** 	atom    lLeftSize = 0*/
    DeRef(_lLeftSize_2013);
    _lLeftSize_2013 = 0;

    /** 	atom    lRightSize = 1*/
    DeRef(_lRightSize_2014);
    _lRightSize_2014 = 1;

    /** 	atom    lLeftValue = 0*/
    DeRef(_lLeftValue_2015);
    _lLeftValue_2015 = 0;

    /** 	atom    lRightValue = 0*/
    DeRef(_lRightValue_2016);
    _lRightValue_2016 = 0;

    /** 	integer lBase = 10*/
    _lBase_2017 = 10;

    /** 	integer lPercent = 1*/
    _lPercent_2019 = 1;

    /** 	integer lDigitCount = 0*/
    _lDigitCount_2021 = 0;

    /** 	integer lCurrencyFound = 0*/
    _lCurrencyFound_2022 = 0;

    /** 	integer lLastDigit = 0*/
    _lLastDigit_2023 = 0;

    /** 	for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_2007)){
            _861 = SEQ_PTR(_text_in_2007)->length;
    }
    else {
        _861 = 1;
    }
    {
        int _i_2026;
        _i_2026 = 1;
L1: 
        if (_i_2026 > _861){
            goto L2; // [70] 672
        }

        /** 		if not integer(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_2007);
        _862 = (int)*(((s1_ptr)_2)->base + _i_2026);
        if (IS_ATOM_INT(_862))
        _863 = 1;
        else if (IS_ATOM_DBL(_862))
        _863 = IS_ATOM_INT(DoubleToInt(_862));
        else
        _863 = 0;
        _862 = NOVALUE;
        if (_863 != 0)
        goto L3; // [86] 94
        _863 = NOVALUE;

        /** 			exit*/
        goto L2; // [91] 672
L3: 

        /** 		lChar = text_in[i]*/
        _2 = (int)SEQ_PTR(_text_in_2007);
        _lChar_2024 = (int)*(((s1_ptr)_2)->base + _i_2026);
        if (!IS_ATOM_INT(_lChar_2024))
        _lChar_2024 = (long)DBL_PTR(_lChar_2024)->dbl;

        /** 		switch lChar do*/
        _0 = _lChar_2024;
        switch ( _0 ){ 

            /** 			case '-' then*/
            case 45:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_2010 != 2)
            goto L4; // [113] 130

            /** 					lSignFound = -1*/
            _lSignFound_2010 = -1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_2023 = _lDigitCount_2021;
            goto L5; // [127] 654
L4: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [136] 654

            /** 			case '+' then*/
            case 43:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_2010 != 2)
            goto L6; // [144] 161

            /** 					lSignFound = 1*/
            _lSignFound_2010 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_2023 = _lDigitCount_2021;
            goto L5; // [158] 654
L6: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [167] 654

            /** 			case '#' then*/
            case 35:

            /** 				if lDigitCount = 0 and lBase = 10 then*/
            _871 = (_lDigitCount_2021 == 0);
            if (_871 == 0) {
                goto L7; // [179] 199
            }
            _873 = (_lBase_2017 == 10);
            if (_873 == 0)
            {
                DeRef(_873);
                _873 = NOVALUE;
                goto L7; // [188] 199
            }
            else{
                DeRef(_873);
                _873 = NOVALUE;
            }

            /** 					lBase = 16*/
            _lBase_2017 = 16;
            goto L5; // [196] 654
L7: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [205] 654

            /** 			case '@' then*/
            case 64:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _874 = (_lDigitCount_2021 == 0);
            if (_874 == 0) {
                goto L8; // [217] 237
            }
            _876 = (_lBase_2017 == 10);
            if (_876 == 0)
            {
                DeRef(_876);
                _876 = NOVALUE;
                goto L8; // [226] 237
            }
            else{
                DeRef(_876);
                _876 = NOVALUE;
            }

            /** 					lBase = 8*/
            _lBase_2017 = 8;
            goto L5; // [234] 654
L8: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [243] 654

            /** 			case '!' then*/
            case 33:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _877 = (_lDigitCount_2021 == 0);
            if (_877 == 0) {
                goto L9; // [255] 275
            }
            _879 = (_lBase_2017 == 10);
            if (_879 == 0)
            {
                DeRef(_879);
                _879 = NOVALUE;
                goto L9; // [264] 275
            }
            else{
                DeRef(_879);
                _879 = NOVALUE;
            }

            /** 					lBase = 2*/
            _lBase_2017 = 2;
            goto L5; // [272] 654
L9: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [281] 654

            /** 			case '$', '�', '�', '�', '�' then*/
            case 36:
            case 163:
            case 164:
            case 165:
            case 128:

            /** 				if lCurrencyFound = 0 then*/
            if (_lCurrencyFound_2022 != 0)
            goto LA; // [297] 314

            /** 					lCurrencyFound = 1*/
            _lCurrencyFound_2022 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_2023 = _lDigitCount_2021;
            goto L5; // [311] 654
LA: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [320] 654

            /** 			case '_' then -- grouping character*/
            case 95:

            /** 				if lDigitCount = 0 or lLastDigit != 0 then*/
            _885 = (_lDigitCount_2021 == 0);
            if (_885 != 0) {
                goto LB; // [332] 345
            }
            _887 = (_lLastDigit_2023 != 0);
            if (_887 == 0)
            {
                DeRef(_887);
                _887 = NOVALUE;
                goto L5; // [341] 654
            }
            else{
                DeRef(_887);
                _887 = NOVALUE;
            }
LB: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [351] 654

            /** 			case '.', ',' then*/
            case 46:
            case 44:

            /** 				if lLastDigit = 0 then*/
            if (_lLastDigit_2023 != 0)
            goto LC; // [361] 400

            /** 					if decimal_mark = lChar then*/
            if (46 != _lChar_2024)
            goto L5; // [369] 654

            /** 						if lDotFound = 0 then*/
            if (_lDotFound_2009 != 0)
            goto LD; // [375] 387

            /** 							lDotFound = 1*/
            _lDotFound_2009 = 1;
            goto L5; // [384] 654
LD: 

            /** 							lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [393] 654
            goto L5; // [397] 654
LC: 

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [406] 654

            /** 			case '%' then*/
            case 37:

            /** 				lLastDigit = lDigitCount*/
            _lLastDigit_2023 = _lDigitCount_2021;

            /** 				if lPercent = 1 then*/
            if (_lPercent_2019 != 1)
            goto LE; // [419] 431

            /** 					lPercent = 100*/
            _lPercent_2019 = 100;
            goto L5; // [428] 654
LE: 

            /** 					if text_in[i-1] = '%' then*/
            _893 = _i_2026 - 1;
            _2 = (int)SEQ_PTR(_text_in_2007);
            _894 = (int)*(((s1_ptr)_2)->base + _893);
            if (binary_op_a(NOTEQ, _894, 37)){
                _894 = NOVALUE;
                goto LF; // [441] 456
            }
            _894 = NOVALUE;

            /** 						lPercent *= 10 -- Yes ten not one hundred.*/
            _lPercent_2019 = _lPercent_2019 * 10;
            goto L5; // [453] 654
LF: 

            /** 						lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [463] 654

            /** 			case '\t', ' ', #A0 then*/
            case 9:
            case 32:
            case 160:

            /** 				if lDigitCount = 0 then*/
            if (_lDigitCount_2021 != 0)
            goto L10; // [475] 482
            goto L5; // [479] 654
L10: 

            /** 					lLastDigit = i*/
            _lLastDigit_2023 = _i_2026;
            goto L5; // [488] 654

            /** 			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',*/
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:

            /** 	            lCharValue = find(lChar, vDigits) - 1*/
            _906 = find_from(_lChar_2024, _11vDigits_1993, 1);
            _lCharValue_2011 = _906 - 1;
            _906 = NOVALUE;

            /** 	            if lCharValue > 15 then*/
            if (_lCharValue_2011 <= 15)
            goto L11; // [549] 560

            /** 	            	lCharValue -= 6*/
            _lCharValue_2011 = _lCharValue_2011 - 6;
L11: 

            /** 	            if lCharValue >= lBase then*/
            if (_lCharValue_2011 < _lBase_2017)
            goto L12; // [562] 574

            /** 	                lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [571] 654
L12: 

            /** 	            elsif lLastDigit != 0 then  -- shouldn't be any more digits*/
            if (_lLastDigit_2023 == 0)
            goto L13; // [576] 588

            /** 					lBadPos = i*/
            _lBadPos_2012 = _i_2026;
            goto L5; // [585] 654
L13: 

            /** 				elsif lDotFound = 1 then*/
            if (_lDotFound_2009 != 1)
            goto L14; // [590] 619

            /** 					lRightSize *= lBase*/
            _0 = _lRightSize_2014;
            if (IS_ATOM_INT(_lRightSize_2014)) {
                if (_lRightSize_2014 == (short)_lRightSize_2014 && _lBase_2017 <= INT15 && _lBase_2017 >= -INT15)
                _lRightSize_2014 = _lRightSize_2014 * _lBase_2017;
                else
                _lRightSize_2014 = NewDouble(_lRightSize_2014 * (double)_lBase_2017);
            }
            else {
                _lRightSize_2014 = NewDouble(DBL_PTR(_lRightSize_2014)->dbl * (double)_lBase_2017);
            }
            DeRef(_0);

            /** 					lRightValue = (lRightValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lRightValue_2016)) {
                if (_lRightValue_2016 == (short)_lRightValue_2016 && _lBase_2017 <= INT15 && _lBase_2017 >= -INT15)
                _915 = _lRightValue_2016 * _lBase_2017;
                else
                _915 = NewDouble(_lRightValue_2016 * (double)_lBase_2017);
            }
            else {
                _915 = NewDouble(DBL_PTR(_lRightValue_2016)->dbl * (double)_lBase_2017);
            }
            DeRef(_lRightValue_2016);
            if (IS_ATOM_INT(_915)) {
                _lRightValue_2016 = _915 + _lCharValue_2011;
                if ((long)((unsigned long)_lRightValue_2016 + (unsigned long)HIGH_BITS) >= 0) 
                _lRightValue_2016 = NewDouble((double)_lRightValue_2016);
            }
            else {
                _lRightValue_2016 = NewDouble(DBL_PTR(_915)->dbl + (double)_lCharValue_2011);
            }
            DeRef(_915);
            _915 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_2021 = _lDigitCount_2021 + 1;
            goto L5; // [616] 654
L14: 

            /** 					lLeftSize += 1*/
            _0 = _lLeftSize_2013;
            if (IS_ATOM_INT(_lLeftSize_2013)) {
                _lLeftSize_2013 = _lLeftSize_2013 + 1;
                if (_lLeftSize_2013 > MAXINT){
                    _lLeftSize_2013 = NewDouble((double)_lLeftSize_2013);
                }
            }
            else
            _lLeftSize_2013 = binary_op(PLUS, 1, _lLeftSize_2013);
            DeRef(_0);

            /** 					lLeftValue = (lLeftValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lLeftValue_2015)) {
                if (_lLeftValue_2015 == (short)_lLeftValue_2015 && _lBase_2017 <= INT15 && _lBase_2017 >= -INT15)
                _919 = _lLeftValue_2015 * _lBase_2017;
                else
                _919 = NewDouble(_lLeftValue_2015 * (double)_lBase_2017);
            }
            else {
                _919 = NewDouble(DBL_PTR(_lLeftValue_2015)->dbl * (double)_lBase_2017);
            }
            DeRef(_lLeftValue_2015);
            if (IS_ATOM_INT(_919)) {
                _lLeftValue_2015 = _919 + _lCharValue_2011;
                if ((long)((unsigned long)_lLeftValue_2015 + (unsigned long)HIGH_BITS) >= 0) 
                _lLeftValue_2015 = NewDouble((double)_lLeftValue_2015);
            }
            else {
                _lLeftValue_2015 = NewDouble(DBL_PTR(_919)->dbl + (double)_lCharValue_2011);
            }
            DeRef(_919);
            _919 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_2021 = _lDigitCount_2021 + 1;
            goto L5; // [642] 654

            /** 			case else*/
            default:

            /** 				lBadPos = i*/
            _lBadPos_2012 = _i_2026;
        ;}L5: 

        /** 		if lBadPos != 0 then*/
        if (_lBadPos_2012 == 0)
        goto L15; // [656] 665

        /** 			exit*/
        goto L2; // [662] 672
L15: 

        /** 	end for*/
        _i_2026 = _i_2026 + 1;
        goto L1; // [667] 77
L2: 
        ;
    }

    /** 	if lBadPos = 0 and lDigitCount = 0 then*/
    _923 = (_lBadPos_2012 == 0);
    if (_923 == 0) {
        goto L16; // [678] 696
    }
    _925 = (_lDigitCount_2021 == 0);
    if (_925 == 0)
    {
        DeRef(_925);
        _925 = NOVALUE;
        goto L16; // [687] 696
    }
    else{
        DeRef(_925);
        _925 = NOVALUE;
    }

    /** 		lBadPos = 1*/
    _lBadPos_2012 = 1;
L16: 

    /** 	if return_bad_pos = 0 and lBadPos != 0 then*/
    _926 = (_return_bad_pos_2008 == 0);
    if (_926 == 0) {
        goto L17; // [702] 721
    }
    _928 = (_lBadPos_2012 != 0);
    if (_928 == 0)
    {
        DeRef(_928);
        _928 = NOVALUE;
        goto L17; // [711] 721
    }
    else{
        DeRef(_928);
        _928 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_text_in_2007);
    DeRef(_lLeftSize_2013);
    DeRef(_lRightSize_2014);
    DeRef(_lLeftValue_2015);
    DeRef(_lRightValue_2016);
    DeRef(_lResult_2020);
    DeRef(_871);
    _871 = NOVALUE;
    DeRef(_874);
    _874 = NOVALUE;
    DeRef(_877);
    _877 = NOVALUE;
    DeRef(_885);
    _885 = NOVALUE;
    DeRef(_893);
    _893 = NOVALUE;
    DeRef(_923);
    _923 = NOVALUE;
    DeRef(_926);
    _926 = NOVALUE;
    return 0;
L17: 

    /** 	if lRightValue = 0 then*/
    if (binary_op_a(NOTEQ, _lRightValue_2016, 0)){
        goto L18; // [723] 751
    }

    /** 	    if lPercent != 1 then*/
    if (_lPercent_2019 == 1)
    goto L19; // [729] 742

    /** 			lResult = (lLeftValue / lPercent)*/
    DeRef(_lResult_2020);
    if (IS_ATOM_INT(_lLeftValue_2015)) {
        _lResult_2020 = (_lLeftValue_2015 % _lPercent_2019) ? NewDouble((double)_lLeftValue_2015 / _lPercent_2019) : (_lLeftValue_2015 / _lPercent_2019);
    }
    else {
        _lResult_2020 = NewDouble(DBL_PTR(_lLeftValue_2015)->dbl / (double)_lPercent_2019);
    }
    goto L1A; // [739] 786
L19: 

    /** 	        lResult = lLeftValue*/
    Ref(_lLeftValue_2015);
    DeRef(_lResult_2020);
    _lResult_2020 = _lLeftValue_2015;
    goto L1A; // [748] 786
L18: 

    /** 	    if lPercent != 1 then*/
    if (_lPercent_2019 == 1)
    goto L1B; // [753] 774

    /** 	        lResult = (lLeftValue  + (lRightValue / (lRightSize))) / lPercent*/
    if (IS_ATOM_INT(_lRightValue_2016) && IS_ATOM_INT(_lRightSize_2014)) {
        _933 = (_lRightValue_2016 % _lRightSize_2014) ? NewDouble((double)_lRightValue_2016 / _lRightSize_2014) : (_lRightValue_2016 / _lRightSize_2014);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_2016)) {
            _933 = NewDouble((double)_lRightValue_2016 / DBL_PTR(_lRightSize_2014)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_2014)) {
                _933 = NewDouble(DBL_PTR(_lRightValue_2016)->dbl / (double)_lRightSize_2014);
            }
            else
            _933 = NewDouble(DBL_PTR(_lRightValue_2016)->dbl / DBL_PTR(_lRightSize_2014)->dbl);
        }
    }
    if (IS_ATOM_INT(_lLeftValue_2015) && IS_ATOM_INT(_933)) {
        _934 = _lLeftValue_2015 + _933;
        if ((long)((unsigned long)_934 + (unsigned long)HIGH_BITS) >= 0) 
        _934 = NewDouble((double)_934);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_2015)) {
            _934 = NewDouble((double)_lLeftValue_2015 + DBL_PTR(_933)->dbl);
        }
        else {
            if (IS_ATOM_INT(_933)) {
                _934 = NewDouble(DBL_PTR(_lLeftValue_2015)->dbl + (double)_933);
            }
            else
            _934 = NewDouble(DBL_PTR(_lLeftValue_2015)->dbl + DBL_PTR(_933)->dbl);
        }
    }
    DeRef(_933);
    _933 = NOVALUE;
    DeRef(_lResult_2020);
    if (IS_ATOM_INT(_934)) {
        _lResult_2020 = (_934 % _lPercent_2019) ? NewDouble((double)_934 / _lPercent_2019) : (_934 / _lPercent_2019);
    }
    else {
        _lResult_2020 = NewDouble(DBL_PTR(_934)->dbl / (double)_lPercent_2019);
    }
    DeRef(_934);
    _934 = NOVALUE;
    goto L1C; // [771] 785
L1B: 

    /** 	        lResult = lLeftValue + (lRightValue / lRightSize)*/
    if (IS_ATOM_INT(_lRightValue_2016) && IS_ATOM_INT(_lRightSize_2014)) {
        _936 = (_lRightValue_2016 % _lRightSize_2014) ? NewDouble((double)_lRightValue_2016 / _lRightSize_2014) : (_lRightValue_2016 / _lRightSize_2014);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_2016)) {
            _936 = NewDouble((double)_lRightValue_2016 / DBL_PTR(_lRightSize_2014)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_2014)) {
                _936 = NewDouble(DBL_PTR(_lRightValue_2016)->dbl / (double)_lRightSize_2014);
            }
            else
            _936 = NewDouble(DBL_PTR(_lRightValue_2016)->dbl / DBL_PTR(_lRightSize_2014)->dbl);
        }
    }
    DeRef(_lResult_2020);
    if (IS_ATOM_INT(_lLeftValue_2015) && IS_ATOM_INT(_936)) {
        _lResult_2020 = _lLeftValue_2015 + _936;
        if ((long)((unsigned long)_lResult_2020 + (unsigned long)HIGH_BITS) >= 0) 
        _lResult_2020 = NewDouble((double)_lResult_2020);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_2015)) {
            _lResult_2020 = NewDouble((double)_lLeftValue_2015 + DBL_PTR(_936)->dbl);
        }
        else {
            if (IS_ATOM_INT(_936)) {
                _lResult_2020 = NewDouble(DBL_PTR(_lLeftValue_2015)->dbl + (double)_936);
            }
            else
            _lResult_2020 = NewDouble(DBL_PTR(_lLeftValue_2015)->dbl + DBL_PTR(_936)->dbl);
        }
    }
    DeRef(_936);
    _936 = NOVALUE;
L1C: 
L1A: 

    /** 	if lSignFound < 0 then*/
    if (_lSignFound_2010 >= 0)
    goto L1D; // [788] 800

    /** 		lResult = -lResult*/
    _0 = _lResult_2020;
    if (IS_ATOM_INT(_lResult_2020)) {
        if ((unsigned long)_lResult_2020 == 0xC0000000)
        _lResult_2020 = (int)NewDouble((double)-0xC0000000);
        else
        _lResult_2020 = - _lResult_2020;
    }
    else {
        _lResult_2020 = unary_op(UMINUS, _lResult_2020);
    }
    DeRef(_0);
L1D: 

    /** 	if return_bad_pos = 0 then*/
    if (_return_bad_pos_2008 != 0)
    goto L1E; // [802] 815

    /** 		return lResult*/
    DeRefDS(_text_in_2007);
    DeRef(_lLeftSize_2013);
    DeRef(_lRightSize_2014);
    DeRef(_lLeftValue_2015);
    DeRef(_lRightValue_2016);
    DeRef(_871);
    _871 = NOVALUE;
    DeRef(_874);
    _874 = NOVALUE;
    DeRef(_877);
    _877 = NOVALUE;
    DeRef(_885);
    _885 = NOVALUE;
    DeRef(_893);
    _893 = NOVALUE;
    DeRef(_923);
    _923 = NOVALUE;
    DeRef(_926);
    _926 = NOVALUE;
    return _lResult_2020;
L1E: 

    /** 	if return_bad_pos = -1 then*/
    if (_return_bad_pos_2008 != -1)
    goto L1F; // [817] 850

    /** 		if lBadPos = 0 then*/
    if (_lBadPos_2012 != 0)
    goto L20; // [823] 838

    /** 			return lResult*/
    DeRefDS(_text_in_2007);
    DeRef(_lLeftSize_2013);
    DeRef(_lRightSize_2014);
    DeRef(_lLeftValue_2015);
    DeRef(_lRightValue_2016);
    DeRef(_871);
    _871 = NOVALUE;
    DeRef(_874);
    _874 = NOVALUE;
    DeRef(_877);
    _877 = NOVALUE;
    DeRef(_885);
    _885 = NOVALUE;
    DeRef(_893);
    _893 = NOVALUE;
    DeRef(_923);
    _923 = NOVALUE;
    DeRef(_926);
    _926 = NOVALUE;
    return _lResult_2020;
    goto L21; // [835] 849
L20: 

    /** 			return {lBadPos}	*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _lBadPos_2012;
    _943 = MAKE_SEQ(_1);
    DeRefDS(_text_in_2007);
    DeRef(_lLeftSize_2013);
    DeRef(_lRightSize_2014);
    DeRef(_lLeftValue_2015);
    DeRef(_lRightValue_2016);
    DeRef(_lResult_2020);
    DeRef(_871);
    _871 = NOVALUE;
    DeRef(_874);
    _874 = NOVALUE;
    DeRef(_877);
    _877 = NOVALUE;
    DeRef(_885);
    _885 = NOVALUE;
    DeRef(_893);
    _893 = NOVALUE;
    DeRef(_923);
    _923 = NOVALUE;
    DeRef(_926);
    _926 = NOVALUE;
    return _943;
L21: 
L1F: 

    /** 	return {lResult, lBadPos}*/
    Ref(_lResult_2020);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_2020;
    ((int *)_2)[2] = _lBadPos_2012;
    _944 = MAKE_SEQ(_1);
    DeRefDS(_text_in_2007);
    DeRef(_lLeftSize_2013);
    DeRef(_lRightSize_2014);
    DeRef(_lLeftValue_2015);
    DeRef(_lRightValue_2016);
    DeRef(_lResult_2020);
    DeRef(_871);
    _871 = NOVALUE;
    DeRef(_874);
    _874 = NOVALUE;
    DeRef(_877);
    _877 = NOVALUE;
    DeRef(_885);
    _885 = NOVALUE;
    DeRef(_893);
    _893 = NOVALUE;
    DeRef(_923);
    _923 = NOVALUE;
    DeRef(_926);
    _926 = NOVALUE;
    DeRef(_943);
    _943 = NOVALUE;
    return _944;
    ;
}



// 0x54CED109
