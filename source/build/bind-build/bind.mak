CC     = gcc
CFLAGS =  -DEWINDOWS -fomit-frame-pointer -c -w -fsigned-char -O2 -m32 -Ic:/develop/offEu/euphoria -ffast-math
LINKER = gcc
LFLAGS = c:/develop/offEu/euphoria/source/build/eu.a -m32 
BIND_SOURCES = init-.c bind.c main-.c mode.c il.c machine.c memconst.c memory.c dll.c error.c types.c text.c convert.c search.c filesys.c datetime.c get.c io.c math.c sequence.c sort.c pretty.c info.c cmdline.c console.c map.c eumem.c primes.c common.c compress.c backend.c global.c fwdref.c parser.c platform.c emit.c pathopen.c cominit.c 0rror.c msgtext.c locale.c eds.c coverage.c regex.c symtab.c c_out.c buildsys.c utils.c c_decl.c compile.c scanner.c scinot.c fenv.c keylist.c preproc.c shift.c block.c inline.c intinit.c main.c init-0.c
BIND_OBJECTS = init-.o bind.o main-.o mode.o il.o machine.o memconst.o memory.o dll.o error.o types.o text.o convert.o search.o filesys.o datetime.o get.o io.o math.o sequence.o sort.o pretty.o info.o cmdline.o console.o map.o eumem.o primes.o common.o compress.o backend.o global.o fwdref.o parser.o platform.o emit.o pathopen.o cominit.o 0rror.o msgtext.o locale.o eds.o coverage.o regex.o symtab.o c_out.o buildsys.o utils.o c_decl.o compile.o scanner.o scinot.o fenv.o keylist.o preproc.o shift.o block.o inline.o intinit.o main.o init-0.o
BIND_GENERATED_FILES =  init-.c init-.o main-.h bind.c bind.o main-.c main-.o mode.c mode.o il.c il.o machine.c machine.o memconst.c memconst.o memory.c memory.o dll.c dll.o error.c error.o types.c types.o text.c text.o convert.c convert.o search.c search.o filesys.c filesys.o datetime.c datetime.o get.c get.o io.c io.o math.c math.o sequence.c sequence.o sort.c sort.o pretty.c pretty.o info.c info.o cmdline.c cmdline.o console.c console.o map.c map.o eumem.c eumem.o primes.c primes.o common.c common.o compress.c compress.o backend.c backend.o global.c global.o fwdref.c fwdref.o parser.c parser.o platform.c platform.o emit.c emit.o pathopen.c pathopen.o cominit.c cominit.o 0rror.c 0rror.o msgtext.c msgtext.o locale.c locale.o eds.c eds.o coverage.c coverage.o regex.c regex.o symtab.c symtab.o c_out.c c_out.o buildsys.c buildsys.o utils.c utils.o c_decl.c c_decl.o compile.c compile.o scanner.c scanner.o scinot.c scinot.o fenv.c fenv.o keylist.c keylist.o preproc.c preproc.o shift.c shift.o block.c block.o inline.c inline.o intinit.c intinit.o main.c main.o init-0.c init-0.o

c:/develop/offEu/euphoria/source/build/eubind.exe: $(BIND_OBJECTS) c:/develop/offEu/euphoria/source/build/eu.a 
	$(LINKER) -o c:/develop/offEu/euphoria/source/build/eubind.exe $(BIND_OBJECTS)  $(LFLAGS)

.PHONY: bind-clean bind-clean-all

bind-clean:
	rm -rf $(BIND_OBJECTS) 

bind-clean-all: bind-clean
	rm -rf $(BIND_SOURCES)  c:/develop/offEu/euphoria/source/build/eubind.exe

%.o: %.c
	$(CC) $(CFLAGS) $*.c -o $*.o

