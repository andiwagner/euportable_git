// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _30malloc(int _mem_struct_p_11053, int _cleanup_p_11054)
{
    int _temp__11055 = NOVALUE;
    int _6229 = NOVALUE;
    int _6227 = NOVALUE;
    int _6226 = NOVALUE;
    int _6225 = NOVALUE;
    int _6221 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_cleanup_p_11054)) {
        _1 = (long)(DBL_PTR(_cleanup_p_11054)->dbl);
        if (UNIQUE(DBL_PTR(_cleanup_p_11054)) && (DBL_PTR(_cleanup_p_11054)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_cleanup_p_11054);
        _cleanup_p_11054 = _1;
    }

    /** 	if atom(mem_struct_p) then*/
    _6221 = IS_ATOM(_mem_struct_p_11053);
    if (_6221 == 0)
    {
        _6221 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _6221 = NOVALUE;
    }

    /** 		mem_struct_p = repeat(0, mem_struct_p)*/
    _0 = _mem_struct_p_11053;
    _mem_struct_p_11053 = Repeat(0, _mem_struct_p_11053);
    DeRef(_0);
L1: 

    /** 	if ram_free_list = 0 then*/
    if (_30ram_free_list_11049 != 0)
    goto L2; // [22] 72

    /** 		ram_space = append(ram_space, mem_struct_p)*/
    Ref(_mem_struct_p_11053);
    Append(&_30ram_space_11048, _30ram_space_11048, _mem_struct_p_11053);

    /** 		if cleanup_p then*/
    if (_cleanup_p_11054 == 0)
    {
        goto L3; // [36] 59
    }
    else{
    }

    /** 			return delete_routine( length(ram_space), free_rid )*/
    if (IS_SEQUENCE(_30ram_space_11048)){
            _6225 = SEQ_PTR(_30ram_space_11048)->length;
    }
    else {
        _6225 = 1;
    }
    _6226 = NewDouble( (double) _6225 );
    _1 = (int) _00[_30free_rid_11050].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_30free_rid_11050].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _30free_rid_11050;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6226)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6226)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6226)) ){
        DeRefDS(_6226);
        _6226 = NewDouble( DBL_PTR(_6226)->dbl );
    }
    DBL_PTR(_6226)->cleanup = (cleanup_ptr)_1;
    _6225 = NOVALUE;
    DeRef(_mem_struct_p_11053);
    return _6226;
    goto L4; // [56] 71
L3: 

    /** 			return length(ram_space)*/
    if (IS_SEQUENCE(_30ram_space_11048)){
            _6227 = SEQ_PTR(_30ram_space_11048)->length;
    }
    else {
        _6227 = 1;
    }
    DeRef(_mem_struct_p_11053);
    DeRef(_6226);
    _6226 = NOVALUE;
    return _6227;
L4: 
L2: 

    /** 	temp_ = ram_free_list*/
    _temp__11055 = _30ram_free_list_11049;

    /** 	ram_free_list = ram_space[temp_]*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _30ram_free_list_11049 = (int)*(((s1_ptr)_2)->base + _temp__11055);
    if (!IS_ATOM_INT(_30ram_free_list_11049))
    _30ram_free_list_11049 = (long)DBL_PTR(_30ram_free_list_11049)->dbl;

    /** 	ram_space[temp_] = mem_struct_p*/
    Ref(_mem_struct_p_11053);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp__11055);
    _1 = *(int *)_2;
    *(int *)_2 = _mem_struct_p_11053;
    DeRef(_1);

    /** 	if cleanup_p then*/
    if (_cleanup_p_11054 == 0)
    {
        goto L5; // [97] 115
    }
    else{
    }

    /** 		return delete_routine( temp_, free_rid )*/
    _6229 = NewDouble( (double) _temp__11055 );
    _1 = (int) _00[_30free_rid_11050].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_30free_rid_11050].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _30free_rid_11050;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6229)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6229)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6229)) ){
        DeRefDS(_6229);
        _6229 = NewDouble( DBL_PTR(_6229)->dbl );
    }
    DBL_PTR(_6229)->cleanup = (cleanup_ptr)_1;
    DeRef(_mem_struct_p_11053);
    DeRef(_6226);
    _6226 = NOVALUE;
    return _6229;
    goto L6; // [112] 122
L5: 

    /** 		return temp_*/
    DeRef(_mem_struct_p_11053);
    DeRef(_6226);
    _6226 = NOVALUE;
    DeRef(_6229);
    _6229 = NOVALUE;
    return _temp__11055;
L6: 
    ;
}


void _30free(int _mem_p_11073)
{
    int _6231 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if mem_p < 1 then return end if*/
    if (binary_op_a(GREATEREQ, _mem_p_11073, 1)){
        goto L1; // [3] 11
    }
    DeRef(_mem_p_11073);
    return;
L1: 

    /** 	if mem_p > length(ram_space) then return end if*/
    if (IS_SEQUENCE(_30ram_space_11048)){
            _6231 = SEQ_PTR(_30ram_space_11048)->length;
    }
    else {
        _6231 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_11073, _6231)){
        _6231 = NOVALUE;
        goto L2; // [18] 26
    }
    _6231 = NOVALUE;
    DeRef(_mem_p_11073);
    return;
L2: 

    /** 	ram_space[mem_p] = ram_free_list*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_mem_p_11073))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_11073)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _mem_p_11073);
    _1 = *(int *)_2;
    *(int *)_2 = _30ram_free_list_11049;
    DeRef(_1);

    /** 	ram_free_list = floor(mem_p)*/
    if (IS_ATOM_INT(_mem_p_11073))
    _30ram_free_list_11049 = e_floor(_mem_p_11073);
    else
    _30ram_free_list_11049 = unary_op(FLOOR, _mem_p_11073);
    if (!IS_ATOM_INT(_30ram_free_list_11049)) {
        _1 = (long)(DBL_PTR(_30ram_free_list_11049)->dbl);
        if (UNIQUE(DBL_PTR(_30ram_free_list_11049)) && (DBL_PTR(_30ram_free_list_11049)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_30ram_free_list_11049);
        _30ram_free_list_11049 = _1;
    }

    /** end procedure*/
    DeRef(_mem_p_11073);
    return;
    ;
}


int _30valid(int _mem_p_11083, int _mem_struct_p_11084)
{
    int _6245 = NOVALUE;
    int _6244 = NOVALUE;
    int _6242 = NOVALUE;
    int _6241 = NOVALUE;
    int _6240 = NOVALUE;
    int _6238 = NOVALUE;
    int _6235 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(mem_p) then return 0 end if*/
    if (IS_ATOM_INT(_mem_p_11083))
    _6235 = 1;
    else if (IS_ATOM_DBL(_mem_p_11083))
    _6235 = IS_ATOM_INT(DoubleToInt(_mem_p_11083));
    else
    _6235 = 0;
    if (_6235 != 0)
    goto L1; // [6] 14
    _6235 = NOVALUE;
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    return 0;
L1: 

    /** 	if mem_p < 1 then return 0 end if*/
    if (binary_op_a(GREATEREQ, _mem_p_11083, 1)){
        goto L2; // [16] 25
    }
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    return 0;
L2: 

    /** 	if mem_p > length(ram_space) then return 0 end if*/
    if (IS_SEQUENCE(_30ram_space_11048)){
            _6238 = SEQ_PTR(_30ram_space_11048)->length;
    }
    else {
        _6238 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_11083, _6238)){
        _6238 = NOVALUE;
        goto L3; // [32] 41
    }
    _6238 = NOVALUE;
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    return 0;
L3: 

    /** 	if sequence(mem_struct_p) then return 1 end if*/
    _6240 = IS_SEQUENCE(_mem_struct_p_11084);
    if (_6240 == 0)
    {
        _6240 = NOVALUE;
        goto L4; // [46] 54
    }
    else{
        _6240 = NOVALUE;
    }
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    return 1;
L4: 

    /** 	if atom(ram_space[mem_p]) then */
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_mem_p_11083)){
        _6241 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_11083)->dbl));
    }
    else{
        _6241 = (int)*(((s1_ptr)_2)->base + _mem_p_11083);
    }
    _6242 = IS_ATOM(_6241);
    _6241 = NOVALUE;
    if (_6242 == 0)
    {
        _6242 = NOVALUE;
        goto L5; // [65] 88
    }
    else{
        _6242 = NOVALUE;
    }

    /** 		if mem_struct_p >= 0 then*/
    if (binary_op_a(LESS, _mem_struct_p_11084, 0)){
        goto L6; // [70] 81
    }

    /** 			return 0*/
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    return 0;
L6: 

    /** 		return 1*/
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    return 1;
L5: 

    /** 	if length(ram_space[mem_p]) != mem_struct_p then*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_mem_p_11083)){
        _6244 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_11083)->dbl));
    }
    else{
        _6244 = (int)*(((s1_ptr)_2)->base + _mem_p_11083);
    }
    if (IS_SEQUENCE(_6244)){
            _6245 = SEQ_PTR(_6244)->length;
    }
    else {
        _6245 = 1;
    }
    _6244 = NOVALUE;
    if (binary_op_a(EQUALS, _6245, _mem_struct_p_11084)){
        _6245 = NOVALUE;
        goto L7; // [99] 110
    }
    _6245 = NOVALUE;

    /** 		return 0*/
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    _6244 = NOVALUE;
    return 0;
L7: 

    /** 	return 1*/
    DeRef(_mem_p_11083);
    DeRef(_mem_struct_p_11084);
    _6244 = NOVALUE;
    return 1;
    ;
}



// 0x62998BAA
