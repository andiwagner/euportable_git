// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _10trim(int _source_9352, int _what_9353, int _ret_index_9354)
{
    int _rpos_9355 = NOVALUE;
    int _lpos_9356 = NOVALUE;
    int _5145 = NOVALUE;
    int _5143 = NOVALUE;
    int _5141 = NOVALUE;
    int _5139 = NOVALUE;
    int _5136 = NOVALUE;
    int _5135 = NOVALUE;
    int _5130 = NOVALUE;
    int _5129 = NOVALUE;
    int _5127 = NOVALUE;
    int _5125 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(what) then*/
    _5125 = 0;
    if (_5125 == 0)
    {
        _5125 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _5125 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_9353;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_what_9353);
    *((int *)(_2+4)) = _what_9353;
    _what_9353 = MAKE_SEQ(_1);
    DeRefDSi(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_9356 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_9352)){
            _5127 = SEQ_PTR(_source_9352)->length;
    }
    else {
        _5127 = 1;
    }
    if (_lpos_9356 > _5127)
    goto L3; // [33] 67

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_9352);
    _5129 = (int)*(((s1_ptr)_2)->base + _lpos_9356);
    _5130 = find_from(_5129, _what_9353, 1);
    _5129 = NOVALUE;
    if (_5130 != 0)
    goto L4; // [48] 56
    _5130 = NOVALUE;

    /** 			exit*/
    goto L3; // [53] 67
L4: 

    /** 		lpos += 1*/
    _lpos_9356 = _lpos_9356 + 1;

    /** 	end while*/
    goto L2; // [64] 30
L3: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_9352)){
            _rpos_9355 = SEQ_PTR(_source_9352)->length;
    }
    else {
        _rpos_9355 = 1;
    }

    /** 	while rpos > lpos do*/
L5: 
    if (_rpos_9355 <= _lpos_9356)
    goto L6; // [77] 111

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_9352);
    _5135 = (int)*(((s1_ptr)_2)->base + _rpos_9355);
    _5136 = find_from(_5135, _what_9353, 1);
    _5135 = NOVALUE;
    if (_5136 != 0)
    goto L7; // [92] 100
    _5136 = NOVALUE;

    /** 			exit*/
    goto L6; // [97] 111
L7: 

    /** 		rpos -= 1*/
    _rpos_9355 = _rpos_9355 - 1;

    /** 	end while*/
    goto L5; // [108] 77
L6: 

    /** 	if ret_index then*/
    if (_ret_index_9354 == 0)
    {
        goto L8; // [113] 129
    }
    else{
    }

    /** 		return {lpos, rpos}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lpos_9356;
    ((int *)_2)[2] = _rpos_9355;
    _5139 = MAKE_SEQ(_1);
    DeRefDS(_source_9352);
    DeRef(_what_9353);
    return _5139;
    goto L9; // [126] 180
L8: 

    /** 		if lpos = 1 then*/
    if (_lpos_9356 != 1)
    goto LA; // [131] 152

    /** 			if rpos = length(source) then*/
    if (IS_SEQUENCE(_source_9352)){
            _5141 = SEQ_PTR(_source_9352)->length;
    }
    else {
        _5141 = 1;
    }
    if (_rpos_9355 != _5141)
    goto LB; // [140] 151

    /** 				return source*/
    DeRef(_what_9353);
    DeRef(_5139);
    _5139 = NOVALUE;
    return _source_9352;
LB: 
LA: 

    /** 		if lpos > length(source) then*/
    if (IS_SEQUENCE(_source_9352)){
            _5143 = SEQ_PTR(_source_9352)->length;
    }
    else {
        _5143 = 1;
    }
    if (_lpos_9356 <= _5143)
    goto LC; // [157] 168

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_source_9352);
    DeRef(_what_9353);
    DeRef(_5139);
    _5139 = NOVALUE;
    return _5;
LC: 

    /** 		return source[lpos..rpos]*/
    rhs_slice_target = (object_ptr)&_5145;
    RHS_Slice(_source_9352, _lpos_9356, _rpos_9355);
    DeRefDS(_source_9352);
    DeRef(_what_9353);
    DeRef(_5139);
    _5139 = NOVALUE;
    return _5145;
L9: 
    ;
}


int _10change_case(int _x_9567, int _api_9568)
{
    int _changed_text_9569 = NOVALUE;
    int _single_char_9570 = NOVALUE;
    int _len_9571 = NOVALUE;
    int _5276 = NOVALUE;
    int _5274 = NOVALUE;
    int _5272 = NOVALUE;
    int _5271 = NOVALUE;
    int _5268 = NOVALUE;
    int _5266 = NOVALUE;
    int _5264 = NOVALUE;
    int _5263 = NOVALUE;
    int _5262 = NOVALUE;
    int _5261 = NOVALUE;
    int _5260 = NOVALUE;
    int _5257 = NOVALUE;
    int _5255 = NOVALUE;
    int _0, _1, _2;
    

    /** 		integer single_char = 0*/
    _single_char_9570 = 0;

    /** 		if not string(x) then*/
    Ref(_x_9567);
    _5255 = _9string(_x_9567);
    if (IS_ATOM_INT(_5255)) {
        if (_5255 != 0){
            DeRef(_5255);
            _5255 = NOVALUE;
            goto L1; // [12] 95
        }
    }
    else {
        if (DBL_PTR(_5255)->dbl != 0.0){
            DeRef(_5255);
            _5255 = NOVALUE;
            goto L1; // [12] 95
        }
    }
    DeRef(_5255);
    _5255 = NOVALUE;

    /** 			if atom(x) then*/
    _5257 = IS_ATOM(_x_9567);
    if (_5257 == 0)
    {
        _5257 = NOVALUE;
        goto L2; // [20] 50
    }
    else{
        _5257 = NOVALUE;
    }

    /** 				if x = 0 then*/
    if (binary_op_a(NOTEQ, _x_9567, 0)){
        goto L3; // [25] 36
    }

    /** 					return 0*/
    DeRef(_x_9567);
    DeRef(_api_9568);
    DeRefi(_changed_text_9569);
    return 0;
L3: 

    /** 				x = {x}*/
    _0 = _x_9567;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_x_9567);
    *((int *)(_2+4)) = _x_9567;
    _x_9567 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 				single_char = 1*/
    _single_char_9570 = 1;
    goto L4; // [47] 94
L2: 

    /** 				for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_9567)){
            _5260 = SEQ_PTR(_x_9567)->length;
    }
    else {
        _5260 = 1;
    }
    {
        int _i_9583;
        _i_9583 = 1;
L5: 
        if (_i_9583 > _5260){
            goto L6; // [55] 87
        }

        /** 					x[i] = change_case(x[i], api)*/
        _2 = (int)SEQ_PTR(_x_9567);
        _5261 = (int)*(((s1_ptr)_2)->base + _i_9583);
        Ref(_api_9568);
        DeRef(_5262);
        _5262 = _api_9568;
        Ref(_5261);
        _5263 = _10change_case(_5261, _5262);
        _5261 = NOVALUE;
        _5262 = NOVALUE;
        _2 = (int)SEQ_PTR(_x_9567);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_9567 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9583);
        _1 = *(int *)_2;
        *(int *)_2 = _5263;
        if( _1 != _5263 ){
            DeRef(_1);
        }
        _5263 = NOVALUE;

        /** 				end for*/
        _i_9583 = _i_9583 + 1;
        goto L5; // [82] 62
L6: 
        ;
    }

    /** 				return x*/
    DeRef(_api_9568);
    DeRefi(_changed_text_9569);
    return _x_9567;
L4: 
L1: 

    /** 		if length(x) = 0 then*/
    if (IS_SEQUENCE(_x_9567)){
            _5264 = SEQ_PTR(_x_9567)->length;
    }
    else {
        _5264 = 1;
    }
    if (_5264 != 0)
    goto L7; // [100] 111

    /** 			return x*/
    DeRef(_api_9568);
    DeRefi(_changed_text_9569);
    return _x_9567;
L7: 

    /** 		if length(x) >= tm_size then*/
    if (IS_SEQUENCE(_x_9567)){
            _5266 = SEQ_PTR(_x_9567)->length;
    }
    else {
        _5266 = 1;
    }
    if (_5266 < _10tm_size_9561)
    goto L8; // [118] 148

    /** 			tm_size = length(x) + 1*/
    if (IS_SEQUENCE(_x_9567)){
            _5268 = SEQ_PTR(_x_9567)->length;
    }
    else {
        _5268 = 1;
    }
    _10tm_size_9561 = _5268 + 1;
    _5268 = NOVALUE;

    /** 			free(temp_mem)*/
    Ref(_10temp_mem_9562);
    _4free(_10temp_mem_9562);

    /** 			temp_mem = allocate(tm_size)*/
    _0 = _4allocate(_10tm_size_9561, 0);
    DeRef(_10temp_mem_9562);
    _10temp_mem_9562 = _0;
L8: 

    /** 		poke(temp_mem, x)*/
    if (IS_ATOM_INT(_10temp_mem_9562)){
        poke_addr = (unsigned char *)_10temp_mem_9562;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_10temp_mem_9562)->dbl);
    }
    if (IS_ATOM_INT(_x_9567)) {
        *poke_addr = (unsigned char)_x_9567;
    }
    else if (IS_ATOM(_x_9567)) {
        _1 = (signed char)DBL_PTR(_x_9567)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_x_9567);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }

    /** 		len = c_func(api, {temp_mem, length(x)} )*/
    if (IS_SEQUENCE(_x_9567)){
            _5271 = SEQ_PTR(_x_9567)->length;
    }
    else {
        _5271 = 1;
    }
    Ref(_10temp_mem_9562);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _10temp_mem_9562;
    ((int *)_2)[2] = _5271;
    _5272 = MAKE_SEQ(_1);
    _5271 = NOVALUE;
    _len_9571 = call_c(1, _api_9568, _5272);
    DeRefDS(_5272);
    _5272 = NOVALUE;
    if (!IS_ATOM_INT(_len_9571)) {
        _1 = (long)(DBL_PTR(_len_9571)->dbl);
        if (UNIQUE(DBL_PTR(_len_9571)) && (DBL_PTR(_len_9571)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_9571);
        _len_9571 = _1;
    }

    /** 		changed_text = peek({temp_mem, len})*/
    Ref(_10temp_mem_9562);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _10temp_mem_9562;
    ((int *)_2)[2] = _len_9571;
    _5274 = MAKE_SEQ(_1);
    DeRefi(_changed_text_9569);
    _1 = (int)SEQ_PTR(_5274);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _changed_text_9569 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_5274);
    _5274 = NOVALUE;

    /** 		if single_char then*/
    if (_single_char_9570 == 0)
    {
        goto L9; // [188] 204
    }
    else{
    }

    /** 			return changed_text[1]*/
    _2 = (int)SEQ_PTR(_changed_text_9569);
    _5276 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_x_9567);
    DeRef(_api_9568);
    DeRefDSi(_changed_text_9569);
    return _5276;
    goto LA; // [201] 211
L9: 

    /** 			return changed_text*/
    DeRef(_x_9567);
    DeRef(_api_9568);
    _5276 = NOVALUE;
    return _changed_text_9569;
LA: 
    ;
}


int _10lower(int _x_9609)
{
    int _5280 = NOVALUE;
    int _5277 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(lower_case_SET) != 0 then*/
    _5277 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharLowerBuff)*/
    Ref(_x_9609);
    Ref(_10api_CharLowerBuff_9545);
    _5280 = _10change_case(_x_9609, _10api_CharLowerBuff_9545);
    DeRef(_x_9609);
    return _5280;
    ;
}


int _10upper(int _x_9617)
{
    int _5284 = NOVALUE;
    int _5281 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(upper_case_SET) != 0 then*/
    _5281 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharUpperBuff)*/
    Ref(_x_9617);
    Ref(_10api_CharUpperBuff_9553);
    _5284 = _10change_case(_x_9617, _10api_CharUpperBuff_9553);
    DeRef(_x_9617);
    return _5284;
    ;
}


int _10proper(int _x_9625)
{
    int _pos_9626 = NOVALUE;
    int _inword_9627 = NOVALUE;
    int _convert_9628 = NOVALUE;
    int _res_9629 = NOVALUE;
    int _5313 = NOVALUE;
    int _5312 = NOVALUE;
    int _5311 = NOVALUE;
    int _5310 = NOVALUE;
    int _5309 = NOVALUE;
    int _5308 = NOVALUE;
    int _5307 = NOVALUE;
    int _5306 = NOVALUE;
    int _5305 = NOVALUE;
    int _5304 = NOVALUE;
    int _5302 = NOVALUE;
    int _5301 = NOVALUE;
    int _5297 = NOVALUE;
    int _5294 = NOVALUE;
    int _5291 = NOVALUE;
    int _5288 = NOVALUE;
    int _5287 = NOVALUE;
    int _5286 = NOVALUE;
    int _5285 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inword = 0	-- Initially not in a word*/
    _inword_9627 = 0;

    /** 	convert = 1	-- Initially convert text*/
    _convert_9628 = 1;

    /** 	res = x		-- Work on a copy of the original, in case we need to restore.*/
    RefDS(_x_9625);
    DeRef(_res_9629);
    _res_9629 = _x_9625;

    /** 	for i = 1 to length(res) do*/
    if (IS_SEQUENCE(_res_9629)){
            _5285 = SEQ_PTR(_res_9629)->length;
    }
    else {
        _5285 = 1;
    }
    {
        int _i_9631;
        _i_9631 = 1;
L1: 
        if (_i_9631 > _5285){
            goto L2; // [25] 298
        }

        /** 		if integer(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5286 = (int)*(((s1_ptr)_2)->base + _i_9631);
        if (IS_ATOM_INT(_5286))
        _5287 = 1;
        else if (IS_ATOM_DBL(_5286))
        _5287 = IS_ATOM_INT(DoubleToInt(_5286));
        else
        _5287 = 0;
        _5286 = NOVALUE;
        if (_5287 == 0)
        {
            _5287 = NOVALUE;
            goto L3; // [41] 209
        }
        else{
            _5287 = NOVALUE;
        }

        /** 			if convert then*/
        if (_convert_9628 == 0)
        {
            goto L4; // [46] 291
        }
        else{
        }

        /** 				pos = types:t_upper(res[i])*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5288 = (int)*(((s1_ptr)_2)->base + _i_9631);
        Ref(_5288);
        _pos_9626 = _9t_upper(_5288);
        _5288 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9626)) {
            _1 = (long)(DBL_PTR(_pos_9626)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9626)) && (DBL_PTR(_pos_9626)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9626);
            _pos_9626 = _1;
        }

        /** 				if pos = 0 then*/
        if (_pos_9626 != 0)
        goto L5; // [63] 175

        /** 					pos = types:t_lower(res[i])*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5291 = (int)*(((s1_ptr)_2)->base + _i_9631);
        Ref(_5291);
        _pos_9626 = _9t_lower(_5291);
        _5291 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9626)) {
            _1 = (long)(DBL_PTR(_pos_9626)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9626)) && (DBL_PTR(_pos_9626)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9626);
            _pos_9626 = _1;
        }

        /** 					if pos = 0 then*/
        if (_pos_9626 != 0)
        goto L6; // [81] 138

        /** 						pos = t_digit(res[i])*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5294 = (int)*(((s1_ptr)_2)->base + _i_9631);
        Ref(_5294);
        _pos_9626 = _9t_digit(_5294);
        _5294 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9626)) {
            _1 = (long)(DBL_PTR(_pos_9626)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9626)) && (DBL_PTR(_pos_9626)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9626);
            _pos_9626 = _1;
        }

        /** 						if pos = 0 then*/
        if (_pos_9626 != 0)
        goto L4; // [99] 291

        /** 							pos = t_specword(res[i])*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5297 = (int)*(((s1_ptr)_2)->base + _i_9631);
        Ref(_5297);
        _pos_9626 = _9t_specword(_5297);
        _5297 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9626)) {
            _1 = (long)(DBL_PTR(_pos_9626)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9626)) && (DBL_PTR(_pos_9626)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9626);
            _pos_9626 = _1;
        }

        /** 							if pos then*/
        if (_pos_9626 == 0)
        {
            goto L7; // [117] 128
        }
        else{
        }

        /** 								inword = 1*/
        _inword_9627 = 1;
        goto L4; // [125] 291
L7: 

        /** 								inword = 0*/
        _inword_9627 = 0;
        goto L4; // [135] 291
L6: 

        /** 						if inword = 0 then*/
        if (_inword_9627 != 0)
        goto L4; // [140] 291

        /** 							if pos <= 26 then*/
        if (_pos_9626 > 26)
        goto L8; // [146] 165

        /** 								res[i] = upper(res[i]) -- Convert to uppercase*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5301 = (int)*(((s1_ptr)_2)->base + _i_9631);
        Ref(_5301);
        _5302 = _10upper(_5301);
        _5301 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9629);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9629 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9631);
        _1 = *(int *)_2;
        *(int *)_2 = _5302;
        if( _1 != _5302 ){
            DeRef(_1);
        }
        _5302 = NOVALUE;
L8: 

        /** 							inword = 1	-- now we are in a word*/
        _inword_9627 = 1;
        goto L4; // [172] 291
L5: 

        /** 					if inword = 1 then*/
        if (_inword_9627 != 1)
        goto L9; // [177] 198

        /** 						res[i] = lower(res[i]) -- Convert to lowercase*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5304 = (int)*(((s1_ptr)_2)->base + _i_9631);
        Ref(_5304);
        _5305 = _10lower(_5304);
        _5304 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9629);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9629 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9631);
        _1 = *(int *)_2;
        *(int *)_2 = _5305;
        if( _1 != _5305 ){
            DeRef(_1);
        }
        _5305 = NOVALUE;
        goto L4; // [195] 291
L9: 

        /** 						inword = 1	-- now we are in a word*/
        _inword_9627 = 1;
        goto L4; // [206] 291
L3: 

        /** 			if convert then*/
        if (_convert_9628 == 0)
        {
            goto LA; // [211] 263
        }
        else{
        }

        /** 				for j = 1 to i-1 do*/
        _5306 = _i_9631 - 1;
        {
            int _j_9671;
            _j_9671 = 1;
LB: 
            if (_j_9671 > _5306){
                goto LC; // [220] 257
            }

            /** 					if atom(x[j]) then*/
            _2 = (int)SEQ_PTR(_x_9625);
            _5307 = (int)*(((s1_ptr)_2)->base + _j_9671);
            _5308 = IS_ATOM(_5307);
            _5307 = NOVALUE;
            if (_5308 == 0)
            {
                _5308 = NOVALUE;
                goto LD; // [236] 250
            }
            else{
                _5308 = NOVALUE;
            }

            /** 						res[j] = x[j]*/
            _2 = (int)SEQ_PTR(_x_9625);
            _5309 = (int)*(((s1_ptr)_2)->base + _j_9671);
            Ref(_5309);
            _2 = (int)SEQ_PTR(_res_9629);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _res_9629 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_9671);
            _1 = *(int *)_2;
            *(int *)_2 = _5309;
            if( _1 != _5309 ){
                DeRef(_1);
            }
            _5309 = NOVALUE;
LD: 

            /** 				end for*/
            _j_9671 = _j_9671 + 1;
            goto LB; // [252] 227
LC: 
            ;
        }

        /** 				convert = 0*/
        _convert_9628 = 0;
LA: 

        /** 			if sequence(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5310 = (int)*(((s1_ptr)_2)->base + _i_9631);
        _5311 = IS_SEQUENCE(_5310);
        _5310 = NOVALUE;
        if (_5311 == 0)
        {
            _5311 = NOVALUE;
            goto LE; // [272] 290
        }
        else{
            _5311 = NOVALUE;
        }

        /** 				res[i] = proper(res[i])	-- recursive conversion*/
        _2 = (int)SEQ_PTR(_res_9629);
        _5312 = (int)*(((s1_ptr)_2)->base + _i_9631);
        Ref(_5312);
        _5313 = _10proper(_5312);
        _5312 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9629);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9629 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9631);
        _1 = *(int *)_2;
        *(int *)_2 = _5313;
        if( _1 != _5313 ){
            DeRef(_1);
        }
        _5313 = NOVALUE;
LE: 
L4: 

        /** 	end for*/
        _i_9631 = _i_9631 + 1;
        goto L1; // [293] 32
L2: 
        ;
    }

    /** 	return res*/
    DeRefDS(_x_9625);
    DeRef(_5306);
    _5306 = NOVALUE;
    return _res_9629;
    ;
}


int _10quote(int _text_in_9947, int _quote_pair_9948, int _esc_9950, int _sp_9952)
{
    int _5584 = NOVALUE;
    int _5583 = NOVALUE;
    int _5582 = NOVALUE;
    int _5580 = NOVALUE;
    int _5579 = NOVALUE;
    int _5578 = NOVALUE;
    int _5576 = NOVALUE;
    int _5575 = NOVALUE;
    int _5574 = NOVALUE;
    int _5573 = NOVALUE;
    int _5572 = NOVALUE;
    int _5571 = NOVALUE;
    int _5570 = NOVALUE;
    int _5569 = NOVALUE;
    int _5568 = NOVALUE;
    int _5566 = NOVALUE;
    int _5565 = NOVALUE;
    int _5564 = NOVALUE;
    int _5562 = NOVALUE;
    int _5561 = NOVALUE;
    int _5560 = NOVALUE;
    int _5559 = NOVALUE;
    int _5558 = NOVALUE;
    int _5557 = NOVALUE;
    int _5556 = NOVALUE;
    int _5555 = NOVALUE;
    int _5554 = NOVALUE;
    int _5552 = NOVALUE;
    int _5551 = NOVALUE;
    int _5549 = NOVALUE;
    int _5548 = NOVALUE;
    int _5547 = NOVALUE;
    int _5545 = NOVALUE;
    int _5544 = NOVALUE;
    int _5543 = NOVALUE;
    int _5542 = NOVALUE;
    int _5541 = NOVALUE;
    int _5540 = NOVALUE;
    int _5539 = NOVALUE;
    int _5538 = NOVALUE;
    int _5537 = NOVALUE;
    int _5536 = NOVALUE;
    int _5535 = NOVALUE;
    int _5534 = NOVALUE;
    int _5533 = NOVALUE;
    int _5532 = NOVALUE;
    int _5531 = NOVALUE;
    int _5530 = NOVALUE;
    int _5529 = NOVALUE;
    int _5526 = NOVALUE;
    int _5525 = NOVALUE;
    int _5524 = NOVALUE;
    int _5523 = NOVALUE;
    int _5522 = NOVALUE;
    int _5521 = NOVALUE;
    int _5520 = NOVALUE;
    int _5519 = NOVALUE;
    int _5518 = NOVALUE;
    int _5517 = NOVALUE;
    int _5516 = NOVALUE;
    int _5515 = NOVALUE;
    int _5514 = NOVALUE;
    int _5513 = NOVALUE;
    int _5510 = NOVALUE;
    int _5508 = NOVALUE;
    int _5507 = NOVALUE;
    int _5505 = NOVALUE;
    int _5503 = NOVALUE;
    int _5502 = NOVALUE;
    int _5501 = NOVALUE;
    int _5499 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_9947)){
            _5499 = SEQ_PTR(_text_in_9947)->length;
    }
    else {
        _5499 = 1;
    }
    if (_5499 != 0)
    goto L1; // [10] 21

    /** 		return text_in*/
    DeRef(_quote_pair_9948);
    DeRef(_sp_9952);
    return _text_in_9947;
L1: 

    /** 	if atom(quote_pair) then*/
    _5501 = IS_ATOM(_quote_pair_9948);
    if (_5501 == 0)
    {
        _5501 = NOVALUE;
        goto L2; // [26] 46
    }
    else{
        _5501 = NOVALUE;
    }

    /** 		quote_pair = {{quote_pair}, {quote_pair}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_9948);
    *((int *)(_2+4)) = _quote_pair_9948;
    _5502 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_9948);
    *((int *)(_2+4)) = _quote_pair_9948;
    _5503 = MAKE_SEQ(_1);
    DeRef(_quote_pair_9948);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5502;
    ((int *)_2)[2] = _5503;
    _quote_pair_9948 = MAKE_SEQ(_1);
    _5503 = NOVALUE;
    _5502 = NOVALUE;
    goto L3; // [43] 89
L2: 

    /** 	elsif length(quote_pair) = 1 then*/
    if (IS_SEQUENCE(_quote_pair_9948)){
            _5505 = SEQ_PTR(_quote_pair_9948)->length;
    }
    else {
        _5505 = 1;
    }
    if (_5505 != 1)
    goto L4; // [51] 72

    /** 		quote_pair = {quote_pair[1], quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5507 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5508 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5508);
    Ref(_5507);
    DeRef(_quote_pair_9948);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5507;
    ((int *)_2)[2] = _5508;
    _quote_pair_9948 = MAKE_SEQ(_1);
    _5508 = NOVALUE;
    _5507 = NOVALUE;
    goto L3; // [69] 89
L4: 

    /** 	elsif length(quote_pair) = 0 then*/
    if (IS_SEQUENCE(_quote_pair_9948)){
            _5510 = SEQ_PTR(_quote_pair_9948)->length;
    }
    else {
        _5510 = 1;
    }
    if (_5510 != 0)
    goto L5; // [77] 88

    /** 		quote_pair = {"\"", "\""}*/
    RefDS(_5491);
    RefDS(_5491);
    DeRef(_quote_pair_9948);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5491;
    ((int *)_2)[2] = _5491;
    _quote_pair_9948 = MAKE_SEQ(_1);
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_9947);
    _5513 = (int)*(((s1_ptr)_2)->base + 1);
    _5514 = IS_SEQUENCE(_5513);
    _5513 = NOVALUE;
    if (_5514 == 0)
    {
        _5514 = NOVALUE;
        goto L6; // [98] 166
    }
    else{
        _5514 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_9947)){
            _5515 = SEQ_PTR(_text_in_9947)->length;
    }
    else {
        _5515 = 1;
    }
    {
        int _i_9975;
        _i_9975 = 1;
L7: 
        if (_i_9975 > _5515){
            goto L8; // [106] 159
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_9947);
        _5516 = (int)*(((s1_ptr)_2)->base + _i_9975);
        _5517 = IS_SEQUENCE(_5516);
        _5516 = NOVALUE;
        if (_5517 == 0)
        {
            _5517 = NOVALUE;
            goto L9; // [122] 152
        }
        else{
            _5517 = NOVALUE;
        }

        /** 				text_in[i] = quote(text_in[i], quote_pair, esc, sp)*/
        _2 = (int)SEQ_PTR(_text_in_9947);
        _5518 = (int)*(((s1_ptr)_2)->base + _i_9975);
        Ref(_quote_pair_9948);
        DeRef(_5519);
        _5519 = _quote_pair_9948;
        DeRef(_5520);
        _5520 = _esc_9950;
        Ref(_sp_9952);
        DeRef(_5521);
        _5521 = _sp_9952;
        Ref(_5518);
        _5522 = _10quote(_5518, _5519, _5520, _5521);
        _5518 = NOVALUE;
        _5519 = NOVALUE;
        _5520 = NOVALUE;
        _5521 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_9947);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_9947 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9975);
        _1 = *(int *)_2;
        *(int *)_2 = _5522;
        if( _1 != _5522 ){
            DeRef(_1);
        }
        _5522 = NOVALUE;
L9: 

        /** 		end for*/
        _i_9975 = _i_9975 + 1;
        goto L7; // [154] 113
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pair_9948);
    DeRef(_sp_9952);
    return _text_in_9947;
L6: 

    /** 	for i = 1 to length(sp) do*/
    if (IS_SEQUENCE(_sp_9952)){
            _5523 = SEQ_PTR(_sp_9952)->length;
    }
    else {
        _5523 = 1;
    }
    {
        int _i_9986;
        _i_9986 = 1;
LA: 
        if (_i_9986 > _5523){
            goto LB; // [171] 220
        }

        /** 		if find(sp[i], text_in) then*/
        _2 = (int)SEQ_PTR(_sp_9952);
        _5524 = (int)*(((s1_ptr)_2)->base + _i_9986);
        _5525 = find_from(_5524, _text_in_9947, 1);
        _5524 = NOVALUE;
        if (_5525 == 0)
        {
            _5525 = NOVALUE;
            goto LC; // [189] 197
        }
        else{
            _5525 = NOVALUE;
        }

        /** 			exit*/
        goto LB; // [194] 220
LC: 

        /** 		if i = length(sp) then*/
        if (IS_SEQUENCE(_sp_9952)){
                _5526 = SEQ_PTR(_sp_9952)->length;
        }
        else {
            _5526 = 1;
        }
        if (_i_9986 != _5526)
        goto LD; // [202] 213

        /** 			return text_in*/
        DeRef(_quote_pair_9948);
        DeRef(_sp_9952);
        return _text_in_9947;
LD: 

        /** 	end for*/
        _i_9986 = _i_9986 + 1;
        goto LA; // [215] 178
LB: 
        ;
    }

    /** 	if esc >= 0  then*/
    if (_esc_9950 < 0)
    goto LE; // [222] 561

    /** 		if atom(quote_pair[1]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5529 = (int)*(((s1_ptr)_2)->base + 1);
    _5530 = IS_ATOM(_5529);
    _5529 = NOVALUE;
    if (_5530 == 0)
    {
        _5530 = NOVALUE;
        goto LF; // [235] 253
    }
    else{
        _5530 = NOVALUE;
    }

    /** 			quote_pair[1] = {quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5531 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5531);
    *((int *)(_2+4)) = _5531;
    _5532 = MAKE_SEQ(_1);
    _5531 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_9948 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _5532;
    if( _1 != _5532 ){
        DeRef(_1);
    }
    _5532 = NOVALUE;
LF: 

    /** 		if atom(quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5533 = (int)*(((s1_ptr)_2)->base + 2);
    _5534 = IS_ATOM(_5533);
    _5533 = NOVALUE;
    if (_5534 == 0)
    {
        _5534 = NOVALUE;
        goto L10; // [262] 280
    }
    else{
        _5534 = NOVALUE;
    }

    /** 			quote_pair[2] = {quote_pair[2]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5535 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5535);
    *((int *)(_2+4)) = _5535;
    _5536 = MAKE_SEQ(_1);
    _5535 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_9948 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _5536;
    if( _1 != _5536 ){
        DeRef(_1);
    }
    _5536 = NOVALUE;
L10: 

    /** 		if equal(quote_pair[1], quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5537 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5538 = (int)*(((s1_ptr)_2)->base + 2);
    if (_5537 == _5538)
    _5539 = 1;
    else if (IS_ATOM_INT(_5537) && IS_ATOM_INT(_5538))
    _5539 = 0;
    else
    _5539 = (compare(_5537, _5538) == 0);
    _5537 = NOVALUE;
    _5538 = NOVALUE;
    if (_5539 == 0)
    {
        _5539 = NOVALUE;
        goto L11; // [294] 372
    }
    else{
        _5539 = NOVALUE;
    }

    /** 			if match(quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5540 = (int)*(((s1_ptr)_2)->base + 1);
    _5541 = e_match_from(_5540, _text_in_9947, 1);
    _5540 = NOVALUE;
    if (_5541 == 0)
    {
        _5541 = NOVALUE;
        goto L12; // [308] 560
    }
    else{
        _5541 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5542 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5542)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5542)) {
        Prepend(&_5543, _5542, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5543, _esc_9950, _5542);
    }
    _5542 = NOVALUE;
    _5544 = e_match_from(_5543, _text_in_9947, 1);
    DeRefDS(_5543);
    _5543 = NOVALUE;
    if (_5544 == 0)
    {
        _5544 = NOVALUE;
        goto L13; // [326] 345
    }
    else{
        _5544 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc, text_in, esc & esc)*/
    Concat((object_ptr)&_5545, _esc_9950, _esc_9950);
    RefDS(_text_in_9947);
    _0 = _text_in_9947;
    _text_in_9947 = _12match_replace(_esc_9950, _text_in_9947, _5545, 0);
    DeRefDS(_0);
    _5545 = NOVALUE;
L13: 

    /** 				text_in = search:match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5547 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5548 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5548)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5548)) {
        Prepend(&_5549, _5548, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5549, _esc_9950, _5548);
    }
    _5548 = NOVALUE;
    Ref(_5547);
    RefDS(_text_in_9947);
    _0 = _text_in_9947;
    _text_in_9947 = _12match_replace(_5547, _text_in_9947, _5549, 0);
    DeRefDS(_0);
    _5547 = NOVALUE;
    _5549 = NOVALUE;
    goto L12; // [369] 560
L11: 

    /** 			if match(quote_pair[1], text_in) or*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5551 = (int)*(((s1_ptr)_2)->base + 1);
    _5552 = e_match_from(_5551, _text_in_9947, 1);
    _5551 = NOVALUE;
    if (_5552 != 0) {
        goto L14; // [383] 401
    }
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5554 = (int)*(((s1_ptr)_2)->base + 2);
    _5555 = e_match_from(_5554, _text_in_9947, 1);
    _5554 = NOVALUE;
    if (_5555 == 0)
    {
        _5555 = NOVALUE;
        goto L15; // [397] 473
    }
    else{
        _5555 = NOVALUE;
    }
L14: 

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5556 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5556)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5556)) {
        Prepend(&_5557, _5556, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5557, _esc_9950, _5556);
    }
    _5556 = NOVALUE;
    _5558 = e_match_from(_5557, _text_in_9947, 1);
    DeRefDS(_5557);
    _5557 = NOVALUE;
    if (_5558 == 0)
    {
        _5558 = NOVALUE;
        goto L16; // [416] 449
    }
    else{
        _5558 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[1], text_in, esc & esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5559 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5559)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5559)) {
        Prepend(&_5560, _5559, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5560, _esc_9950, _5559);
    }
    _5559 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5561 = (int)*(((s1_ptr)_2)->base + 1);
    {
        int concat_list[3];

        concat_list[0] = _5561;
        concat_list[1] = _esc_9950;
        concat_list[2] = _esc_9950;
        Concat_N((object_ptr)&_5562, concat_list, 3);
    }
    _5561 = NOVALUE;
    RefDS(_text_in_9947);
    _0 = _text_in_9947;
    _text_in_9947 = _12match_replace(_5560, _text_in_9947, _5562, 0);
    DeRefDS(_0);
    _5560 = NOVALUE;
    _5562 = NOVALUE;
L16: 

    /** 				text_in = match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5564 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5565 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5565)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5565)) {
        Prepend(&_5566, _5565, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5566, _esc_9950, _5565);
    }
    _5565 = NOVALUE;
    Ref(_5564);
    RefDS(_text_in_9947);
    _0 = _text_in_9947;
    _text_in_9947 = _12match_replace(_5564, _text_in_9947, _5566, 0);
    DeRefDS(_0);
    _5564 = NOVALUE;
    _5566 = NOVALUE;
L15: 

    /** 			if match(quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5568 = (int)*(((s1_ptr)_2)->base + 2);
    _5569 = e_match_from(_5568, _text_in_9947, 1);
    _5568 = NOVALUE;
    if (_5569 == 0)
    {
        _5569 = NOVALUE;
        goto L17; // [484] 559
    }
    else{
        _5569 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5570 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5570)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5570)) {
        Prepend(&_5571, _5570, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5571, _esc_9950, _5570);
    }
    _5570 = NOVALUE;
    _5572 = e_match_from(_5571, _text_in_9947, 1);
    DeRefDS(_5571);
    _5571 = NOVALUE;
    if (_5572 == 0)
    {
        _5572 = NOVALUE;
        goto L18; // [502] 535
    }
    else{
        _5572 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[2], text_in, esc & esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5573 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5573)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5573)) {
        Prepend(&_5574, _5573, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5574, _esc_9950, _5573);
    }
    _5573 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5575 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5575;
        concat_list[1] = _esc_9950;
        concat_list[2] = _esc_9950;
        Concat_N((object_ptr)&_5576, concat_list, 3);
    }
    _5575 = NOVALUE;
    RefDS(_text_in_9947);
    _0 = _text_in_9947;
    _text_in_9947 = _12match_replace(_5574, _text_in_9947, _5576, 0);
    DeRefDS(_0);
    _5574 = NOVALUE;
    _5576 = NOVALUE;
L18: 

    /** 				text_in = search:match_replace(quote_pair[2], text_in, esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5578 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5579 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9950) && IS_ATOM(_5579)) {
    }
    else if (IS_ATOM(_esc_9950) && IS_SEQUENCE(_5579)) {
        Prepend(&_5580, _5579, _esc_9950);
    }
    else {
        Concat((object_ptr)&_5580, _esc_9950, _5579);
    }
    _5579 = NOVALUE;
    Ref(_5578);
    RefDS(_text_in_9947);
    _0 = _text_in_9947;
    _text_in_9947 = _12match_replace(_5578, _text_in_9947, _5580, 0);
    DeRefDS(_0);
    _5578 = NOVALUE;
    _5580 = NOVALUE;
L17: 
L12: 
LE: 

    /** 	return quote_pair[1] & text_in & quote_pair[2]*/
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5582 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9948);
    _5583 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5583;
        concat_list[1] = _text_in_9947;
        concat_list[2] = _5582;
        Concat_N((object_ptr)&_5584, concat_list, 3);
    }
    _5583 = NOVALUE;
    _5582 = NOVALUE;
    DeRefDS(_text_in_9947);
    DeRef(_quote_pair_9948);
    DeRef(_sp_9952);
    return _5584;
    ;
}


int _10format(int _format_pattern_10168, int _arg_list_10169)
{
    int _result_10170 = NOVALUE;
    int _in_token_10171 = NOVALUE;
    int _tch_10172 = NOVALUE;
    int _i_10173 = NOVALUE;
    int _tend_10174 = NOVALUE;
    int _cap_10175 = NOVALUE;
    int _align_10176 = NOVALUE;
    int _psign_10177 = NOVALUE;
    int _msign_10178 = NOVALUE;
    int _zfill_10179 = NOVALUE;
    int _bwz_10180 = NOVALUE;
    int _spacer_10181 = NOVALUE;
    int _alt_10182 = NOVALUE;
    int _width_10183 = NOVALUE;
    int _decs_10184 = NOVALUE;
    int _pos_10185 = NOVALUE;
    int _argn_10186 = NOVALUE;
    int _argl_10187 = NOVALUE;
    int _trimming_10188 = NOVALUE;
    int _hexout_10189 = NOVALUE;
    int _binout_10190 = NOVALUE;
    int _tsep_10191 = NOVALUE;
    int _istext_10192 = NOVALUE;
    int _prevargv_10193 = NOVALUE;
    int _currargv_10194 = NOVALUE;
    int _idname_10195 = NOVALUE;
    int _envsym_10196 = NOVALUE;
    int _envvar_10197 = NOVALUE;
    int _ep_10198 = NOVALUE;
    int _pflag_10199 = NOVALUE;
    int _count_10200 = NOVALUE;
    int _sp_10271 = NOVALUE;
    int _sp_10306 = NOVALUE;
    int _argtext_10353 = NOVALUE;
    int _tempv_10589 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2492_10644 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2489_10643 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2548_10651 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2545_10650 = NOVALUE;
    int _x_inlined_pretty_sprint_at_2542_10649 = NOVALUE;
    int _msg_inlined_crash_at_2696_10673 = NOVALUE;
    int _dpos_10735 = NOVALUE;
    int _dist_10736 = NOVALUE;
    int _bracketed_10737 = NOVALUE;
    int _6106 = NOVALUE;
    int _6105 = NOVALUE;
    int _6104 = NOVALUE;
    int _6102 = NOVALUE;
    int _6101 = NOVALUE;
    int _6100 = NOVALUE;
    int _6097 = NOVALUE;
    int _6096 = NOVALUE;
    int _6093 = NOVALUE;
    int _6091 = NOVALUE;
    int _6088 = NOVALUE;
    int _6087 = NOVALUE;
    int _6086 = NOVALUE;
    int _6083 = NOVALUE;
    int _6080 = NOVALUE;
    int _6079 = NOVALUE;
    int _6078 = NOVALUE;
    int _6077 = NOVALUE;
    int _6074 = NOVALUE;
    int _6073 = NOVALUE;
    int _6072 = NOVALUE;
    int _6069 = NOVALUE;
    int _6067 = NOVALUE;
    int _6064 = NOVALUE;
    int _6063 = NOVALUE;
    int _6062 = NOVALUE;
    int _6061 = NOVALUE;
    int _6058 = NOVALUE;
    int _6053 = NOVALUE;
    int _6052 = NOVALUE;
    int _6051 = NOVALUE;
    int _6050 = NOVALUE;
    int _6048 = NOVALUE;
    int _6047 = NOVALUE;
    int _6046 = NOVALUE;
    int _6045 = NOVALUE;
    int _6044 = NOVALUE;
    int _6043 = NOVALUE;
    int _6042 = NOVALUE;
    int _6037 = NOVALUE;
    int _6033 = NOVALUE;
    int _6032 = NOVALUE;
    int _6030 = NOVALUE;
    int _6028 = NOVALUE;
    int _6027 = NOVALUE;
    int _6026 = NOVALUE;
    int _6025 = NOVALUE;
    int _6024 = NOVALUE;
    int _6021 = NOVALUE;
    int _6019 = NOVALUE;
    int _6018 = NOVALUE;
    int _6017 = NOVALUE;
    int _6016 = NOVALUE;
    int _6013 = NOVALUE;
    int _6012 = NOVALUE;
    int _6010 = NOVALUE;
    int _6009 = NOVALUE;
    int _6008 = NOVALUE;
    int _6007 = NOVALUE;
    int _6006 = NOVALUE;
    int _6003 = NOVALUE;
    int _6002 = NOVALUE;
    int _6001 = NOVALUE;
    int _5998 = NOVALUE;
    int _5997 = NOVALUE;
    int _5995 = NOVALUE;
    int _5991 = NOVALUE;
    int _5990 = NOVALUE;
    int _5988 = NOVALUE;
    int _5987 = NOVALUE;
    int _5979 = NOVALUE;
    int _5975 = NOVALUE;
    int _5973 = NOVALUE;
    int _5972 = NOVALUE;
    int _5971 = NOVALUE;
    int _5968 = NOVALUE;
    int _5966 = NOVALUE;
    int _5965 = NOVALUE;
    int _5964 = NOVALUE;
    int _5962 = NOVALUE;
    int _5961 = NOVALUE;
    int _5960 = NOVALUE;
    int _5959 = NOVALUE;
    int _5956 = NOVALUE;
    int _5955 = NOVALUE;
    int _5954 = NOVALUE;
    int _5952 = NOVALUE;
    int _5951 = NOVALUE;
    int _5950 = NOVALUE;
    int _5949 = NOVALUE;
    int _5947 = NOVALUE;
    int _5945 = NOVALUE;
    int _5943 = NOVALUE;
    int _5941 = NOVALUE;
    int _5939 = NOVALUE;
    int _5937 = NOVALUE;
    int _5936 = NOVALUE;
    int _5935 = NOVALUE;
    int _5934 = NOVALUE;
    int _5933 = NOVALUE;
    int _5932 = NOVALUE;
    int _5930 = NOVALUE;
    int _5929 = NOVALUE;
    int _5927 = NOVALUE;
    int _5926 = NOVALUE;
    int _5924 = NOVALUE;
    int _5922 = NOVALUE;
    int _5921 = NOVALUE;
    int _5918 = NOVALUE;
    int _5916 = NOVALUE;
    int _5912 = NOVALUE;
    int _5910 = NOVALUE;
    int _5909 = NOVALUE;
    int _5908 = NOVALUE;
    int _5906 = NOVALUE;
    int _5905 = NOVALUE;
    int _5904 = NOVALUE;
    int _5903 = NOVALUE;
    int _5902 = NOVALUE;
    int _5900 = NOVALUE;
    int _5898 = NOVALUE;
    int _5897 = NOVALUE;
    int _5896 = NOVALUE;
    int _5895 = NOVALUE;
    int _5891 = NOVALUE;
    int _5888 = NOVALUE;
    int _5887 = NOVALUE;
    int _5884 = NOVALUE;
    int _5883 = NOVALUE;
    int _5882 = NOVALUE;
    int _5880 = NOVALUE;
    int _5879 = NOVALUE;
    int _5878 = NOVALUE;
    int _5877 = NOVALUE;
    int _5875 = NOVALUE;
    int _5873 = NOVALUE;
    int _5872 = NOVALUE;
    int _5871 = NOVALUE;
    int _5870 = NOVALUE;
    int _5869 = NOVALUE;
    int _5868 = NOVALUE;
    int _5866 = NOVALUE;
    int _5865 = NOVALUE;
    int _5864 = NOVALUE;
    int _5862 = NOVALUE;
    int _5861 = NOVALUE;
    int _5860 = NOVALUE;
    int _5859 = NOVALUE;
    int _5857 = NOVALUE;
    int _5854 = NOVALUE;
    int _5853 = NOVALUE;
    int _5851 = NOVALUE;
    int _5850 = NOVALUE;
    int _5848 = NOVALUE;
    int _5845 = NOVALUE;
    int _5844 = NOVALUE;
    int _5841 = NOVALUE;
    int _5839 = NOVALUE;
    int _5835 = NOVALUE;
    int _5833 = NOVALUE;
    int _5832 = NOVALUE;
    int _5831 = NOVALUE;
    int _5829 = NOVALUE;
    int _5827 = NOVALUE;
    int _5826 = NOVALUE;
    int _5825 = NOVALUE;
    int _5824 = NOVALUE;
    int _5823 = NOVALUE;
    int _5821 = NOVALUE;
    int _5819 = NOVALUE;
    int _5818 = NOVALUE;
    int _5817 = NOVALUE;
    int _5816 = NOVALUE;
    int _5814 = NOVALUE;
    int _5811 = NOVALUE;
    int _5809 = NOVALUE;
    int _5808 = NOVALUE;
    int _5807 = NOVALUE;
    int _5806 = NOVALUE;
    int _5805 = NOVALUE;
    int _5803 = NOVALUE;
    int _5802 = NOVALUE;
    int _5801 = NOVALUE;
    int _5799 = NOVALUE;
    int _5798 = NOVALUE;
    int _5797 = NOVALUE;
    int _5796 = NOVALUE;
    int _5794 = NOVALUE;
    int _5793 = NOVALUE;
    int _5792 = NOVALUE;
    int _5789 = NOVALUE;
    int _5788 = NOVALUE;
    int _5787 = NOVALUE;
    int _5786 = NOVALUE;
    int _5784 = NOVALUE;
    int _5783 = NOVALUE;
    int _5782 = NOVALUE;
    int _5781 = NOVALUE;
    int _5780 = NOVALUE;
    int _5777 = NOVALUE;
    int _5776 = NOVALUE;
    int _5775 = NOVALUE;
    int _5774 = NOVALUE;
    int _5772 = NOVALUE;
    int _5771 = NOVALUE;
    int _5770 = NOVALUE;
    int _5768 = NOVALUE;
    int _5767 = NOVALUE;
    int _5766 = NOVALUE;
    int _5764 = NOVALUE;
    int _5757 = NOVALUE;
    int _5755 = NOVALUE;
    int _5754 = NOVALUE;
    int _5747 = NOVALUE;
    int _5744 = NOVALUE;
    int _5740 = NOVALUE;
    int _5738 = NOVALUE;
    int _5737 = NOVALUE;
    int _5734 = NOVALUE;
    int _5732 = NOVALUE;
    int _5730 = NOVALUE;
    int _5727 = NOVALUE;
    int _5725 = NOVALUE;
    int _5724 = NOVALUE;
    int _5723 = NOVALUE;
    int _5722 = NOVALUE;
    int _5721 = NOVALUE;
    int _5718 = NOVALUE;
    int _5716 = NOVALUE;
    int _5715 = NOVALUE;
    int _5714 = NOVALUE;
    int _5711 = NOVALUE;
    int _5709 = NOVALUE;
    int _5707 = NOVALUE;
    int _5704 = NOVALUE;
    int _5703 = NOVALUE;
    int _5696 = NOVALUE;
    int _5693 = NOVALUE;
    int _5692 = NOVALUE;
    int _5685 = NOVALUE;
    int _5681 = NOVALUE;
    int _5678 = NOVALUE;
    int _5668 = NOVALUE;
    int _5666 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(arg_list) then*/
    _5666 = IS_ATOM(_arg_list_10169);
    if (_5666 == 0)
    {
        _5666 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _5666 = NOVALUE;
    }

    /** 		arg_list = {arg_list}*/
    _0 = _arg_list_10169;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_arg_list_10169);
    *((int *)(_2+4)) = _arg_list_10169;
    _arg_list_10169 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	result = ""*/
    RefDS(_5);
    DeRef(_result_10170);
    _result_10170 = _5;

    /** 	in_token = 0*/
    _in_token_10171 = 0;

    /** 	i = 0*/
    _i_10173 = 0;

    /** 	tend = 0*/
    _tend_10174 = 0;

    /** 	argl = 0*/
    _argl_10187 = 0;

    /** 	spacer = 0*/
    _spacer_10181 = 0;

    /** 	prevargv = 0*/
    DeRef(_prevargv_10193);
    _prevargv_10193 = 0;

    /**     while i < length(format_pattern) do*/
L2: 
    if (IS_SEQUENCE(_format_pattern_10168)){
            _5668 = SEQ_PTR(_format_pattern_10168)->length;
    }
    else {
        _5668 = 1;
    }
    if (_i_10173 >= _5668)
    goto L3; // [63] 3585

    /**     	i += 1*/
    _i_10173 = _i_10173 + 1;

    /**     	tch = format_pattern[i]*/
    _2 = (int)SEQ_PTR(_format_pattern_10168);
    _tch_10172 = (int)*(((s1_ptr)_2)->base + _i_10173);
    if (!IS_ATOM_INT(_tch_10172))
    _tch_10172 = (long)DBL_PTR(_tch_10172)->dbl;

    /**     	if not in_token then*/
    if (_in_token_10171 != 0)
    goto L4; // [81] 210

    /**     		if tch = '[' then*/
    if (_tch_10172 != 91)
    goto L5; // [86] 200

    /**     			in_token = 1*/
    _in_token_10171 = 1;

    /**     			tend = 0*/
    _tend_10174 = 0;

    /** 				cap = 0*/
    _cap_10175 = 0;

    /** 				align = 0*/
    _align_10176 = 0;

    /** 				psign = 0*/
    _psign_10177 = 0;

    /** 				msign = 0*/
    _msign_10178 = 0;

    /** 				zfill = 0*/
    _zfill_10179 = 0;

    /** 				bwz = 0*/
    _bwz_10180 = 0;

    /** 				spacer = 0*/
    _spacer_10181 = 0;

    /** 				alt = 0*/
    _alt_10182 = 0;

    /**     			width = 0*/
    _width_10183 = 0;

    /**     			decs = -1*/
    _decs_10184 = -1;

    /**     			argn = 0*/
    _argn_10186 = 0;

    /**     			hexout = 0*/
    _hexout_10189 = 0;

    /**     			binout = 0*/
    _binout_10190 = 0;

    /**     			trimming = 0*/
    _trimming_10188 = 0;

    /**     			tsep = 0*/
    _tsep_10191 = 0;

    /**     			istext = 0*/
    _istext_10192 = 0;

    /**     			idname = ""*/
    RefDS(_5);
    DeRef(_idname_10195);
    _idname_10195 = _5;

    /**     			envvar = ""*/
    RefDS(_5);
    DeRefi(_envvar_10197);
    _envvar_10197 = _5;

    /**     			envsym = ""*/
    RefDS(_5);
    DeRef(_envsym_10196);
    _envsym_10196 = _5;
    goto L2; // [197] 60
L5: 

    /**     			result &= tch*/
    Append(&_result_10170, _result_10170, _tch_10172);
    goto L2; // [207] 60
L4: 

    /** 			switch tch do*/
    _0 = _tch_10172;
    switch ( _0 ){ 

        /**     			case ']' then*/
        case 93:

        /**     				in_token = 0*/
        _in_token_10171 = 0;

        /**     				tend = i*/
        _tend_10174 = _i_10173;
        goto L6; // [231] 1072

        /**     			case '[' then*/
        case 91:

        /** 	    			result &= tch*/
        Append(&_result_10170, _result_10170, _tch_10172);

        /** 	    			while i < length(format_pattern) do*/
L7: 
        if (IS_SEQUENCE(_format_pattern_10168)){
                _5678 = SEQ_PTR(_format_pattern_10168)->length;
        }
        else {
            _5678 = 1;
        }
        if (_i_10173 >= _5678)
        goto L6; // [251] 1072

        /** 	    				i += 1*/
        _i_10173 = _i_10173 + 1;

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _5681 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (binary_op_a(NOTEQ, _5681, 93)){
            _5681 = NOVALUE;
            goto L7; // [267] 248
        }
        _5681 = NOVALUE;

        /** 	    					in_token = 0*/
        _in_token_10171 = 0;

        /** 	    					tend = 0*/
        _tend_10174 = 0;

        /** 	    					exit*/
        goto L6; // [283] 1072

        /** 	    			end while*/
        goto L7; // [288] 248
        goto L6; // [291] 1072

        /** 	    		case 'w', 'u', 'l' then*/
        case 119:
        case 117:
        case 108:

        /** 	    			cap = tch*/
        _cap_10175 = _tch_10172;
        goto L6; // [306] 1072

        /** 	    		case 'b' then*/
        case 98:

        /** 	    			bwz = 1*/
        _bwz_10180 = 1;
        goto L6; // [317] 1072

        /** 	    		case 's' then*/
        case 115:

        /** 	    			spacer = 1*/
        _spacer_10181 = 1;
        goto L6; // [328] 1072

        /** 	    		case 't' then*/
        case 116:

        /** 	    			trimming = 1*/
        _trimming_10188 = 1;
        goto L6; // [339] 1072

        /** 	    		case 'z' then*/
        case 122:

        /** 	    			zfill = 1*/
        _zfill_10179 = 1;
        goto L6; // [350] 1072

        /** 	    		case 'X' then*/
        case 88:

        /** 	    			hexout = 1*/
        _hexout_10189 = 1;
        goto L6; // [361] 1072

        /** 	    		case 'B' then*/
        case 66:

        /** 	    			binout = 1*/
        _binout_10190 = 1;
        goto L6; // [372] 1072

        /** 	    		case 'c', '<', '>' then*/
        case 99:
        case 60:
        case 62:

        /** 	    			align = tch*/
        _align_10176 = _tch_10172;
        goto L6; // [387] 1072

        /** 	    		case '+' then*/
        case 43:

        /** 	    			psign = 1*/
        _psign_10177 = 1;
        goto L6; // [398] 1072

        /** 	    		case '(' then*/
        case 40:

        /** 	    			msign = 1*/
        _msign_10178 = 1;
        goto L6; // [409] 1072

        /** 	    		case '?' then*/
        case 63:

        /** 	    			alt = 1*/
        _alt_10182 = 1;
        goto L6; // [420] 1072

        /** 	    		case 'T' then*/
        case 84:

        /** 	    			istext = 1*/
        _istext_10192 = 1;
        goto L6; // [431] 1072

        /** 	    		case ':' then*/
        case 58:

        /** 	    			while i < length(format_pattern) do*/
L8: 
        if (IS_SEQUENCE(_format_pattern_10168)){
                _5685 = SEQ_PTR(_format_pattern_10168)->length;
        }
        else {
            _5685 = 1;
        }
        if (_i_10173 >= _5685)
        goto L6; // [445] 1072

        /** 	    				i += 1*/
        _i_10173 = _i_10173 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _tch_10172 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (!IS_ATOM_INT(_tch_10172))
        _tch_10172 = (long)DBL_PTR(_tch_10172)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_10185 = find_from(_tch_10172, _1206, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_10185 != 0)
        goto L9; // [470] 485

        /** 	    					i -= 1*/
        _i_10173 = _i_10173 - 1;

        /** 	    					exit*/
        goto L6; // [482] 1072
L9: 

        /** 	    				width = width * 10 + pos - 1*/
        if (_width_10183 == (short)_width_10183)
        _5692 = _width_10183 * 10;
        else
        _5692 = NewDouble(_width_10183 * (double)10);
        if (IS_ATOM_INT(_5692)) {
            _5693 = _5692 + _pos_10185;
            if ((long)((unsigned long)_5693 + (unsigned long)HIGH_BITS) >= 0) 
            _5693 = NewDouble((double)_5693);
        }
        else {
            _5693 = NewDouble(DBL_PTR(_5692)->dbl + (double)_pos_10185);
        }
        DeRef(_5692);
        _5692 = NOVALUE;
        if (IS_ATOM_INT(_5693)) {
            _width_10183 = _5693 - 1;
        }
        else {
            _width_10183 = NewDouble(DBL_PTR(_5693)->dbl - (double)1);
        }
        DeRef(_5693);
        _5693 = NOVALUE;
        if (!IS_ATOM_INT(_width_10183)) {
            _1 = (long)(DBL_PTR(_width_10183)->dbl);
            if (UNIQUE(DBL_PTR(_width_10183)) && (DBL_PTR(_width_10183)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_width_10183);
            _width_10183 = _1;
        }

        /** 	    				if width = 0 then*/
        if (_width_10183 != 0)
        goto L8; // [505] 442

        /** 	    					zfill = '0'*/
        _zfill_10179 = 48;

        /** 	    			end while*/
        goto L8; // [517] 442
        goto L6; // [520] 1072

        /** 	    		case '.' then*/
        case 46:

        /** 	    			decs = 0*/
        _decs_10184 = 0;

        /** 	    			while i < length(format_pattern) do*/
LA: 
        if (IS_SEQUENCE(_format_pattern_10168)){
                _5696 = SEQ_PTR(_format_pattern_10168)->length;
        }
        else {
            _5696 = 1;
        }
        if (_i_10173 >= _5696)
        goto L6; // [539] 1072

        /** 	    				i += 1*/
        _i_10173 = _i_10173 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _tch_10172 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (!IS_ATOM_INT(_tch_10172))
        _tch_10172 = (long)DBL_PTR(_tch_10172)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_10185 = find_from(_tch_10172, _1206, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_10185 != 0)
        goto LB; // [564] 579

        /** 	    					i -= 1*/
        _i_10173 = _i_10173 - 1;

        /** 	    					exit*/
        goto L6; // [576] 1072
LB: 

        /** 	    				decs = decs * 10 + pos - 1*/
        if (_decs_10184 == (short)_decs_10184)
        _5703 = _decs_10184 * 10;
        else
        _5703 = NewDouble(_decs_10184 * (double)10);
        if (IS_ATOM_INT(_5703)) {
            _5704 = _5703 + _pos_10185;
            if ((long)((unsigned long)_5704 + (unsigned long)HIGH_BITS) >= 0) 
            _5704 = NewDouble((double)_5704);
        }
        else {
            _5704 = NewDouble(DBL_PTR(_5703)->dbl + (double)_pos_10185);
        }
        DeRef(_5703);
        _5703 = NOVALUE;
        if (IS_ATOM_INT(_5704)) {
            _decs_10184 = _5704 - 1;
        }
        else {
            _decs_10184 = NewDouble(DBL_PTR(_5704)->dbl - (double)1);
        }
        DeRef(_5704);
        _5704 = NOVALUE;
        if (!IS_ATOM_INT(_decs_10184)) {
            _1 = (long)(DBL_PTR(_decs_10184)->dbl);
            if (UNIQUE(DBL_PTR(_decs_10184)) && (DBL_PTR(_decs_10184)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_decs_10184);
            _decs_10184 = _1;
        }

        /** 	    			end while*/
        goto LA; // [597] 536
        goto L6; // [600] 1072

        /** 	    		case '{' then*/
        case 123:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_10271 = _i_10173 + 1;

        /** 	    			i = sp*/
        _i_10173 = _sp_10271;

        /** 	    			while i < length(format_pattern) do*/
LC: 
        if (IS_SEQUENCE(_format_pattern_10168)){
                _5707 = SEQ_PTR(_format_pattern_10168)->length;
        }
        else {
            _5707 = 1;
        }
        if (_i_10173 >= _5707)
        goto LD; // [627] 672

        /** 	    				if format_pattern[i] = '}' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _5709 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (binary_op_a(NOTEQ, _5709, 125)){
            _5709 = NOVALUE;
            goto LE; // [637] 646
        }
        _5709 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [643] 672
LE: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _5711 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (binary_op_a(NOTEQ, _5711, 93)){
            _5711 = NOVALUE;
            goto LF; // [652] 661
        }
        _5711 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [658] 672
LF: 

        /** 	    				i += 1*/
        _i_10173 = _i_10173 + 1;

        /** 	    			end while*/
        goto LC; // [669] 624
LD: 

        /** 	    			idname = trim(format_pattern[sp .. i-1]) & '='*/
        _5714 = _i_10173 - 1;
        rhs_slice_target = (object_ptr)&_5715;
        RHS_Slice(_format_pattern_10168, _sp_10271, _5714);
        RefDS(_4494);
        _5716 = _10trim(_5715, _4494, 0);
        _5715 = NOVALUE;
        if (IS_SEQUENCE(_5716) && IS_ATOM(61)) {
            Append(&_idname_10195, _5716, 61);
        }
        else if (IS_ATOM(_5716) && IS_SEQUENCE(61)) {
        }
        else {
            Concat((object_ptr)&_idname_10195, _5716, 61);
            DeRef(_5716);
            _5716 = NOVALUE;
        }
        DeRef(_5716);
        _5716 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _5718 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (binary_op_a(NOTEQ, _5718, 93)){
            _5718 = NOVALUE;
            goto L10; // [699] 710
        }
        _5718 = NOVALUE;

        /**     					i -= 1*/
        _i_10173 = _i_10173 - 1;
L10: 

        /**     				for j = 1 to length(arg_list) do*/
        if (IS_SEQUENCE(_arg_list_10169)){
                _5721 = SEQ_PTR(_arg_list_10169)->length;
        }
        else {
            _5721 = 1;
        }
        {
            int _j_10292;
            _j_10292 = 1;
L11: 
            if (_j_10292 > _5721){
                goto L12; // [715] 797
            }

            /**     					if sequence(arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_10169);
            _5722 = (int)*(((s1_ptr)_2)->base + _j_10292);
            _5723 = IS_SEQUENCE(_5722);
            _5722 = NOVALUE;
            if (_5723 == 0)
            {
                _5723 = NOVALUE;
                goto L13; // [731] 768
            }
            else{
                _5723 = NOVALUE;
            }

            /**     						if search:begins(idname, arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_10169);
            _5724 = (int)*(((s1_ptr)_2)->base + _j_10292);
            RefDS(_idname_10195);
            Ref(_5724);
            _5725 = _12begins(_idname_10195, _5724);
            _5724 = NOVALUE;
            if (_5725 == 0) {
                DeRef(_5725);
                _5725 = NOVALUE;
                goto L14; // [745] 767
            }
            else {
                if (!IS_ATOM_INT(_5725) && DBL_PTR(_5725)->dbl == 0.0){
                    DeRef(_5725);
                    _5725 = NOVALUE;
                    goto L14; // [745] 767
                }
                DeRef(_5725);
                _5725 = NOVALUE;
            }
            DeRef(_5725);
            _5725 = NOVALUE;

            /**     							if argn = 0 then*/
            if (_argn_10186 != 0)
            goto L15; // [752] 766

            /**     								argn = j*/
            _argn_10186 = _j_10292;

            /**     								exit*/
            goto L12; // [763] 797
L15: 
L14: 
L13: 

            /**     					if j = length(arg_list) then*/
            if (IS_SEQUENCE(_arg_list_10169)){
                    _5727 = SEQ_PTR(_arg_list_10169)->length;
            }
            else {
                _5727 = 1;
            }
            if (_j_10292 != _5727)
            goto L16; // [773] 790

            /**     						idname = ""*/
            RefDS(_5);
            DeRef(_idname_10195);
            _idname_10195 = _5;

            /**     						argn = -1*/
            _argn_10186 = -1;
L16: 

            /**     				end for*/
            _j_10292 = _j_10292 + 1;
            goto L11; // [792] 722
L12: 
            ;
        }
        goto L6; // [799] 1072

        /** 	    		case '%' then*/
        case 37:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_10306 = _i_10173 + 1;

        /** 	    			i = sp*/
        _i_10173 = _sp_10306;

        /** 	    			while i < length(format_pattern) do*/
L17: 
        if (IS_SEQUENCE(_format_pattern_10168)){
                _5730 = SEQ_PTR(_format_pattern_10168)->length;
        }
        else {
            _5730 = 1;
        }
        if (_i_10173 >= _5730)
        goto L18; // [826] 871

        /** 	    				if format_pattern[i] = '%' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _5732 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (binary_op_a(NOTEQ, _5732, 37)){
            _5732 = NOVALUE;
            goto L19; // [836] 845
        }
        _5732 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [842] 871
L19: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _5734 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (binary_op_a(NOTEQ, _5734, 93)){
            _5734 = NOVALUE;
            goto L1A; // [851] 860
        }
        _5734 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [857] 871
L1A: 

        /** 	    				i += 1*/
        _i_10173 = _i_10173 + 1;

        /** 	    			end while*/
        goto L17; // [868] 823
L18: 

        /** 	    			envsym = trim(format_pattern[sp .. i-1])*/
        _5737 = _i_10173 - 1;
        rhs_slice_target = (object_ptr)&_5738;
        RHS_Slice(_format_pattern_10168, _sp_10306, _5737);
        RefDS(_4494);
        _0 = _envsym_10196;
        _envsym_10196 = _10trim(_5738, _4494, 0);
        DeRef(_0);
        _5738 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _5740 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (binary_op_a(NOTEQ, _5740, 93)){
            _5740 = NOVALUE;
            goto L1B; // [894] 905
        }
        _5740 = NOVALUE;

        /**     					i -= 1*/
        _i_10173 = _i_10173 - 1;
L1B: 

        /**     				envvar = getenv(envsym)*/
        DeRefi(_envvar_10197);
        _envvar_10197 = EGetEnv(_envsym_10196);

        /**     				argn = -1*/
        _argn_10186 = -1;

        /**     				if atom(envvar) then*/
        _5744 = IS_ATOM(_envvar_10197);
        if (_5744 == 0)
        {
            _5744 = NOVALUE;
            goto L1C; // [920] 929
        }
        else{
            _5744 = NOVALUE;
        }

        /**     					envvar = ""*/
        RefDS(_5);
        DeRefi(_envvar_10197);
        _envvar_10197 = _5;
L1C: 
        goto L6; // [931] 1072

        /** 	    		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' then*/
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:

        /** 	    			if argn = 0 then*/
        if (_argn_10186 != 0)
        goto L6; // [957] 1072

        /** 		    			i -= 1*/
        _i_10173 = _i_10173 - 1;

        /** 		    			while i < length(format_pattern) do*/
L1D: 
        if (IS_SEQUENCE(_format_pattern_10168)){
                _5747 = SEQ_PTR(_format_pattern_10168)->length;
        }
        else {
            _5747 = 1;
        }
        if (_i_10173 >= _5747)
        goto L6; // [975] 1072

        /** 		    				i += 1*/
        _i_10173 = _i_10173 + 1;

        /** 		    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _tch_10172 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (!IS_ATOM_INT(_tch_10172))
        _tch_10172 = (long)DBL_PTR(_tch_10172)->dbl;

        /** 		    				pos = find(tch, "0123456789")*/
        _pos_10185 = find_from(_tch_10172, _1206, 1);

        /** 		    				if pos = 0 then*/
        if (_pos_10185 != 0)
        goto L1E; // [1000] 1015

        /** 		    					i -= 1*/
        _i_10173 = _i_10173 - 1;

        /** 		    					exit*/
        goto L6; // [1012] 1072
L1E: 

        /** 		    				argn = argn * 10 + pos - 1*/
        if (_argn_10186 == (short)_argn_10186)
        _5754 = _argn_10186 * 10;
        else
        _5754 = NewDouble(_argn_10186 * (double)10);
        if (IS_ATOM_INT(_5754)) {
            _5755 = _5754 + _pos_10185;
            if ((long)((unsigned long)_5755 + (unsigned long)HIGH_BITS) >= 0) 
            _5755 = NewDouble((double)_5755);
        }
        else {
            _5755 = NewDouble(DBL_PTR(_5754)->dbl + (double)_pos_10185);
        }
        DeRef(_5754);
        _5754 = NOVALUE;
        if (IS_ATOM_INT(_5755)) {
            _argn_10186 = _5755 - 1;
        }
        else {
            _argn_10186 = NewDouble(DBL_PTR(_5755)->dbl - (double)1);
        }
        DeRef(_5755);
        _5755 = NOVALUE;
        if (!IS_ATOM_INT(_argn_10186)) {
            _1 = (long)(DBL_PTR(_argn_10186)->dbl);
            if (UNIQUE(DBL_PTR(_argn_10186)) && (DBL_PTR(_argn_10186)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_argn_10186);
            _argn_10186 = _1;
        }

        /** 		    			end while*/
        goto L1D; // [1033] 972
        goto L6; // [1037] 1072

        /** 	    		case ',' then*/
        case 44:

        /** 	    			if i < length(format_pattern) then*/
        if (IS_SEQUENCE(_format_pattern_10168)){
                _5757 = SEQ_PTR(_format_pattern_10168)->length;
        }
        else {
            _5757 = 1;
        }
        if (_i_10173 >= _5757)
        goto L6; // [1048] 1072

        /** 	    				i +=1*/
        _i_10173 = _i_10173 + 1;

        /** 	    				tsep = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_10168);
        _tsep_10191 = (int)*(((s1_ptr)_2)->base + _i_10173);
        if (!IS_ATOM_INT(_tsep_10191))
        _tsep_10191 = (long)DBL_PTR(_tsep_10191)->dbl;
        goto L6; // [1065] 1072

        /** 	    		case else*/
        default:
    ;}L6: 

    /**     		if tend > 0 then*/
    if (_tend_10174 <= 0)
    goto L1F; // [1074] 3577

    /**     			sequence argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_10353);
    _argtext_10353 = _5;

    /**     			if argn = 0 then*/
    if (_argn_10186 != 0)
    goto L20; // [1089] 1100

    /**     				argn = argl + 1*/
    _argn_10186 = _argl_10187 + 1;
L20: 

    /**     			argl = argn*/
    _argl_10187 = _argn_10186;

    /**     			if argn < 1 or argn > length(arg_list) then*/
    _5764 = (_argn_10186 < 1);
    if (_5764 != 0) {
        goto L21; // [1111] 1127
    }
    if (IS_SEQUENCE(_arg_list_10169)){
            _5766 = SEQ_PTR(_arg_list_10169)->length;
    }
    else {
        _5766 = 1;
    }
    _5767 = (_argn_10186 > _5766);
    _5766 = NOVALUE;
    if (_5767 == 0)
    {
        DeRef(_5767);
        _5767 = NOVALUE;
        goto L22; // [1123] 1169
    }
    else{
        DeRef(_5767);
        _5767 = NOVALUE;
    }
L21: 

    /**     				if length(envvar) > 0 then*/
    if (IS_SEQUENCE(_envvar_10197)){
            _5768 = SEQ_PTR(_envvar_10197)->length;
    }
    else {
        _5768 = 1;
    }
    if (_5768 <= 0)
    goto L23; // [1134] 1153

    /**     					argtext = envvar*/
    Ref(_envvar_10197);
    DeRef(_argtext_10353);
    _argtext_10353 = _envvar_10197;

    /** 	    				currargv = envvar*/
    Ref(_envvar_10197);
    DeRef(_currargv_10194);
    _currargv_10194 = _envvar_10197;
    goto L24; // [1150] 2618
L23: 

    /**     					argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_10353);
    _argtext_10353 = _5;

    /** 	    				currargv =""*/
    RefDS(_5);
    DeRef(_currargv_10194);
    _currargv_10194 = _5;
    goto L24; // [1166] 2618
L22: 

    /** 					if string(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5770 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    Ref(_5770);
    _5771 = _9string(_5770);
    _5770 = NOVALUE;
    if (_5771 == 0) {
        DeRef(_5771);
        _5771 = NOVALUE;
        goto L25; // [1179] 1229
    }
    else {
        if (!IS_ATOM_INT(_5771) && DBL_PTR(_5771)->dbl == 0.0){
            DeRef(_5771);
            _5771 = NOVALUE;
            goto L25; // [1179] 1229
        }
        DeRef(_5771);
        _5771 = NOVALUE;
    }
    DeRef(_5771);
    _5771 = NOVALUE;

    /** 						if length(idname) > 0 then*/
    if (IS_SEQUENCE(_idname_10195)){
            _5772 = SEQ_PTR(_idname_10195)->length;
    }
    else {
        _5772 = 1;
    }
    if (_5772 <= 0)
    goto L26; // [1189] 1217

    /** 							argtext = arg_list[argn][length(idname) + 1 .. $]*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5774 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (IS_SEQUENCE(_idname_10195)){
            _5775 = SEQ_PTR(_idname_10195)->length;
    }
    else {
        _5775 = 1;
    }
    _5776 = _5775 + 1;
    _5775 = NOVALUE;
    if (IS_SEQUENCE(_5774)){
            _5777 = SEQ_PTR(_5774)->length;
    }
    else {
        _5777 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_5774, _5776, _5777);
    _5774 = NOVALUE;
    goto L27; // [1214] 2611
L26: 

    /** 							argtext = arg_list[argn]*/
    DeRef(_argtext_10353);
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _argtext_10353 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    Ref(_argtext_10353);
    goto L27; // [1226] 2611
L25: 

    /** 					elsif integer(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5780 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (IS_ATOM_INT(_5780))
    _5781 = 1;
    else if (IS_ATOM_DBL(_5780))
    _5781 = IS_ATOM_INT(DoubleToInt(_5780));
    else
    _5781 = 0;
    _5780 = NOVALUE;
    if (_5781 == 0)
    {
        _5781 = NOVALUE;
        goto L28; // [1238] 1783
    }
    else{
        _5781 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_10192 == 0)
    {
        goto L29; // [1245] 1269
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(arg_list[argn]))}*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5782 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    Ref(_5782);
    _5783 = _18abs(_5782);
    _5782 = NOVALUE;
    _5784 = binary_op(AND_BITS, _2108, _5783);
    DeRef(_5783);
    _5783 = NOVALUE;
    _0 = _argtext_10353;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5784;
    _argtext_10353 = MAKE_SEQ(_1);
    DeRef(_0);
    _5784 = NOVALUE;
    goto L27; // [1266] 2611
L29: 

    /** 						elsif bwz != 0 and arg_list[argn] = 0 then*/
    _5786 = (_bwz_10180 != 0);
    if (_5786 == 0) {
        goto L2A; // [1277] 1304
    }
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5788 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (IS_ATOM_INT(_5788)) {
        _5789 = (_5788 == 0);
    }
    else {
        _5789 = binary_op(EQUALS, _5788, 0);
    }
    _5788 = NOVALUE;
    if (_5789 == 0) {
        DeRef(_5789);
        _5789 = NOVALUE;
        goto L2A; // [1290] 1304
    }
    else {
        if (!IS_ATOM_INT(_5789) && DBL_PTR(_5789)->dbl == 0.0){
            DeRef(_5789);
            _5789 = NOVALUE;
            goto L2A; // [1290] 1304
        }
        DeRef(_5789);
        _5789 = NOVALUE;
    }
    DeRef(_5789);
    _5789 = NOVALUE;

    /** 							argtext = repeat(' ', width)*/
    DeRef(_argtext_10353);
    _argtext_10353 = Repeat(32, _width_10183);
    goto L27; // [1301] 2611
L2A: 

    /** 						elsif binout = 1 then*/
    if (_binout_10190 != 1)
    goto L2B; // [1308] 1447

    /** 							argtext = stdseq:reverse( convert:int_to_bits(arg_list[argn], 32)) + '0'*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5792 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    Ref(_5792);
    _5793 = _11int_to_bits(_5792, 32);
    _5792 = NOVALUE;
    _5794 = _21reverse(_5793, 1, 0);
    _5793 = NOVALUE;
    DeRef(_argtext_10353);
    if (IS_ATOM_INT(_5794)) {
        _argtext_10353 = _5794 + 48;
        if ((long)((unsigned long)_argtext_10353 + (unsigned long)HIGH_BITS) >= 0) 
        _argtext_10353 = NewDouble((double)_argtext_10353);
    }
    else {
        _argtext_10353 = binary_op(PLUS, _5794, 48);
    }
    DeRef(_5794);
    _5794 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5796 = (_zfill_10179 != 0);
    if (_5796 == 0) {
        goto L2C; // [1343] 1389
    }
    _5798 = (_width_10183 > 0);
    if (_5798 == 0)
    {
        DeRef(_5798);
        _5798 = NOVALUE;
        goto L2C; // [1354] 1389
    }
    else{
        DeRef(_5798);
        _5798 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5799 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5799 = 1;
    }
    if (_width_10183 <= _5799)
    goto L27; // [1364] 2611

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5801 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5801 = 1;
    }
    _5802 = _width_10183 - _5801;
    _5801 = NOVALUE;
    _5803 = Repeat(48, _5802);
    _5802 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _5803, _argtext_10353);
    DeRefDS(_5803);
    _5803 = NOVALUE;
    DeRef(_5803);
    _5803 = NOVALUE;
    goto L27; // [1386] 2611
L2C: 

    /** 								count = 1*/
    _count_10200 = 1;

    /** 								while count < length(argtext) and argtext[count] = '0' do*/
L2D: 
    if (IS_SEQUENCE(_argtext_10353)){
            _5805 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5805 = 1;
    }
    _5806 = (_count_10200 < _5805);
    _5805 = NOVALUE;
    if (_5806 == 0) {
        goto L2E; // [1406] 1433
    }
    _2 = (int)SEQ_PTR(_argtext_10353);
    _5808 = (int)*(((s1_ptr)_2)->base + _count_10200);
    if (IS_ATOM_INT(_5808)) {
        _5809 = (_5808 == 48);
    }
    else {
        _5809 = binary_op(EQUALS, _5808, 48);
    }
    _5808 = NOVALUE;
    if (_5809 <= 0) {
        if (_5809 == 0) {
            DeRef(_5809);
            _5809 = NOVALUE;
            goto L2E; // [1419] 1433
        }
        else {
            if (!IS_ATOM_INT(_5809) && DBL_PTR(_5809)->dbl == 0.0){
                DeRef(_5809);
                _5809 = NOVALUE;
                goto L2E; // [1419] 1433
            }
            DeRef(_5809);
            _5809 = NOVALUE;
        }
    }
    DeRef(_5809);
    _5809 = NOVALUE;

    /** 									count += 1*/
    _count_10200 = _count_10200 + 1;

    /** 								end while*/
    goto L2D; // [1430] 1399
L2E: 

    /** 								argtext = argtext[count .. $]*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5811 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5811 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, _count_10200, _5811);
    goto L27; // [1444] 2611
L2B: 

    /** 						elsif hexout = 0 then*/
    if (_hexout_10189 != 0)
    goto L2F; // [1451] 1717

    /** 							argtext = sprintf("%d", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5814 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    DeRef(_argtext_10353);
    _argtext_10353 = EPrintf(-9999999, _967, _5814);
    _5814 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5816 = (_zfill_10179 != 0);
    if (_5816 == 0) {
        goto L30; // [1473] 1570
    }
    _5818 = (_width_10183 > 0);
    if (_5818 == 0)
    {
        DeRef(_5818);
        _5818 = NOVALUE;
        goto L30; // [1484] 1570
    }
    else{
        DeRef(_5818);
        _5818 = NOVALUE;
    }

    /** 								if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    _5819 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5819, 45)){
        _5819 = NOVALUE;
        goto L31; // [1493] 1539
    }
    _5819 = NOVALUE;

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5821 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5821 = 1;
    }
    if (_width_10183 <= _5821)
    goto L32; // [1504] 1569

    /** 										argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5823 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5823 = 1;
    }
    _5824 = _width_10183 - _5823;
    _5823 = NOVALUE;
    _5825 = Repeat(48, _5824);
    _5824 = NOVALUE;
    if (IS_SEQUENCE(_argtext_10353)){
            _5826 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5826 = 1;
    }
    rhs_slice_target = (object_ptr)&_5827;
    RHS_Slice(_argtext_10353, 2, _5826);
    {
        int concat_list[3];

        concat_list[0] = _5827;
        concat_list[1] = _5825;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_5827);
    _5827 = NOVALUE;
    DeRefDS(_5825);
    _5825 = NOVALUE;
    goto L32; // [1536] 1569
L31: 

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5829 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5829 = 1;
    }
    if (_width_10183 <= _5829)
    goto L33; // [1546] 1568

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5831 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5831 = 1;
    }
    _5832 = _width_10183 - _5831;
    _5831 = NOVALUE;
    _5833 = Repeat(48, _5832);
    _5832 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _5833, _argtext_10353);
    DeRefDS(_5833);
    _5833 = NOVALUE;
    DeRef(_5833);
    _5833 = NOVALUE;
L33: 
L32: 
L30: 

    /** 							if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5835 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (binary_op_a(LESSEQ, _5835, 0)){
        _5835 = NOVALUE;
        goto L34; // [1576] 1624
    }
    _5835 = NOVALUE;

    /** 								if psign then*/
    if (_psign_10177 == 0)
    {
        goto L27; // [1584] 2611
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_10179 != 0)
    goto L35; // [1589] 1602

    /** 										argtext = '+' & argtext*/
    Prepend(&_argtext_10353, _argtext_10353, 43);
    goto L27; // [1599] 2611
L35: 

    /** 									elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    _5839 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5839, 48)){
        _5839 = NOVALUE;
        goto L27; // [1608] 2611
    }
    _5839 = NOVALUE;

    /** 										argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_10353 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [1621] 2611
L34: 

    /** 							elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5841 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (binary_op_a(GREATEREQ, _5841, 0)){
        _5841 = NOVALUE;
        goto L27; // [1630] 2611
    }
    _5841 = NOVALUE;

    /** 								if msign then*/
    if (_msign_10178 == 0)
    {
        goto L27; // [1638] 2611
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_10179 != 0)
    goto L36; // [1643] 1666

    /** 										argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5844 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5844 = 1;
    }
    rhs_slice_target = (object_ptr)&_5845;
    RHS_Slice(_argtext_10353, 2, _5844);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5845;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_5845);
    _5845 = NOVALUE;
    goto L27; // [1663] 2611
L36: 

    /** 										if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    _5848 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5848, 48)){
        _5848 = NOVALUE;
        goto L37; // [1672] 1695
    }
    _5848 = NOVALUE;

    /** 											argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5850 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5850 = 1;
    }
    rhs_slice_target = (object_ptr)&_5851;
    RHS_Slice(_argtext_10353, 3, _5850);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5851;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_5851);
    _5851 = NOVALUE;
    goto L27; // [1692] 2611
L37: 

    /** 											argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5853 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5853 = 1;
    }
    rhs_slice_target = (object_ptr)&_5854;
    RHS_Slice(_argtext_10353, 2, _5853);
    Append(&_argtext_10353, _5854, 41);
    DeRefDS(_5854);
    _5854 = NOVALUE;
    goto L27; // [1714] 2611
L2F: 

    /** 							argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5857 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    DeRef(_argtext_10353);
    _argtext_10353 = EPrintf(-9999999, _5856, _5857);
    _5857 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5859 = (_zfill_10179 != 0);
    if (_5859 == 0) {
        goto L27; // [1735] 2611
    }
    _5861 = (_width_10183 > 0);
    if (_5861 == 0)
    {
        DeRef(_5861);
        _5861 = NOVALUE;
        goto L27; // [1746] 2611
    }
    else{
        DeRef(_5861);
        _5861 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5862 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5862 = 1;
    }
    if (_width_10183 <= _5862)
    goto L27; // [1756] 2611

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5864 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5864 = 1;
    }
    _5865 = _width_10183 - _5864;
    _5864 = NOVALUE;
    _5866 = Repeat(48, _5865);
    _5865 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _5866, _argtext_10353);
    DeRefDS(_5866);
    _5866 = NOVALUE;
    DeRef(_5866);
    _5866 = NOVALUE;
    goto L27; // [1780] 2611
L28: 

    /** 					elsif atom(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5868 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    _5869 = IS_ATOM(_5868);
    _5868 = NOVALUE;
    if (_5869 == 0)
    {
        _5869 = NOVALUE;
        goto L38; // [1792] 2195
    }
    else{
        _5869 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_10192 == 0)
    {
        goto L39; // [1799] 1826
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(arg_list[argn])))}*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5870 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (IS_ATOM_INT(_5870))
    _5871 = e_floor(_5870);
    else
    _5871 = unary_op(FLOOR, _5870);
    _5870 = NOVALUE;
    _5872 = _18abs(_5871);
    _5871 = NOVALUE;
    _5873 = binary_op(AND_BITS, _2108, _5872);
    DeRef(_5872);
    _5872 = NOVALUE;
    _0 = _argtext_10353;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5873;
    _argtext_10353 = MAKE_SEQ(_1);
    DeRef(_0);
    _5873 = NOVALUE;
    goto L27; // [1823] 2611
L39: 

    /** 							if hexout then*/
    if (_hexout_10189 == 0)
    {
        goto L3A; // [1830] 1898
    }
    else{
    }

    /** 								argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5875 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    DeRef(_argtext_10353);
    _argtext_10353 = EPrintf(-9999999, _5856, _5875);
    _5875 = NOVALUE;

    /** 								if zfill != 0 and width > 0 then*/
    _5877 = (_zfill_10179 != 0);
    if (_5877 == 0) {
        goto L27; // [1851] 2611
    }
    _5879 = (_width_10183 > 0);
    if (_5879 == 0)
    {
        DeRef(_5879);
        _5879 = NOVALUE;
        goto L27; // [1862] 2611
    }
    else{
        DeRef(_5879);
        _5879 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5880 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5880 = 1;
    }
    if (_width_10183 <= _5880)
    goto L27; // [1872] 2611

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5882 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5882 = 1;
    }
    _5883 = _width_10183 - _5882;
    _5882 = NOVALUE;
    _5884 = Repeat(48, _5883);
    _5883 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _5884, _argtext_10353);
    DeRefDS(_5884);
    _5884 = NOVALUE;
    DeRef(_5884);
    _5884 = NOVALUE;
    goto L27; // [1895] 2611
L3A: 

    /** 								argtext = trim(sprintf("%15.15g", arg_list[argn]))*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5887 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    _5888 = EPrintf(-9999999, _5886, _5887);
    _5887 = NOVALUE;
    RefDS(_4494);
    _0 = _argtext_10353;
    _argtext_10353 = _10trim(_5888, _4494, 0);
    DeRef(_0);
    _5888 = NOVALUE;

    /** 								while ep != 0 with entry do*/
    goto L3B; // [1918] 1941
L3C: 
    if (_ep_10198 == 0)
    goto L3D; // [1923] 1953

    /** 									argtext = remove(argtext, ep+2)*/
    _5891 = _ep_10198 + 2;
    if ((long)((unsigned long)_5891 + (unsigned long)HIGH_BITS) >= 0) 
    _5891 = NewDouble((double)_5891);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_10353);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5891)) ? _5891 : (long)(DBL_PTR(_5891)->dbl);
        int stop = (IS_ATOM_INT(_5891)) ? _5891 : (long)(DBL_PTR(_5891)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_10353), start, &_argtext_10353 );
            }
            else Tail(SEQ_PTR(_argtext_10353), stop+1, &_argtext_10353);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_10353), start, &_argtext_10353);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_10353 = Remove_elements(start, stop, (SEQ_PTR(_argtext_10353)->ref == 1));
        }
    }
    DeRef(_5891);
    _5891 = NOVALUE;
    _5891 = NOVALUE;

    /** 								entry*/
L3B: 

    /** 									ep = match("e+0", argtext)*/
    _ep_10198 = e_match_from(_5893, _argtext_10353, 1);

    /** 								end while*/
    goto L3C; // [1950] 1921
L3D: 

    /** 								if zfill != 0 and width > 0 then*/
    _5895 = (_zfill_10179 != 0);
    if (_5895 == 0) {
        goto L3E; // [1961] 2046
    }
    _5897 = (_width_10183 > 0);
    if (_5897 == 0)
    {
        DeRef(_5897);
        _5897 = NOVALUE;
        goto L3E; // [1972] 2046
    }
    else{
        DeRef(_5897);
        _5897 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5898 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5898 = 1;
    }
    if (_width_10183 <= _5898)
    goto L3F; // [1982] 2045

    /** 										if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    _5900 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5900, 45)){
        _5900 = NOVALUE;
        goto L40; // [1992] 2026
    }
    _5900 = NOVALUE;

    /** 											argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5902 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5902 = 1;
    }
    _5903 = _width_10183 - _5902;
    _5902 = NOVALUE;
    _5904 = Repeat(48, _5903);
    _5903 = NOVALUE;
    if (IS_SEQUENCE(_argtext_10353)){
            _5905 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5905 = 1;
    }
    rhs_slice_target = (object_ptr)&_5906;
    RHS_Slice(_argtext_10353, 2, _5905);
    {
        int concat_list[3];

        concat_list[0] = _5906;
        concat_list[1] = _5904;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_5906);
    _5906 = NOVALUE;
    DeRefDS(_5904);
    _5904 = NOVALUE;
    goto L41; // [2023] 2044
L40: 

    /** 											argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5908 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5908 = 1;
    }
    _5909 = _width_10183 - _5908;
    _5908 = NOVALUE;
    _5910 = Repeat(48, _5909);
    _5909 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _5910, _argtext_10353);
    DeRefDS(_5910);
    _5910 = NOVALUE;
    DeRef(_5910);
    _5910 = NOVALUE;
L41: 
L3F: 
L3E: 

    /** 								if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5912 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (binary_op_a(LESSEQ, _5912, 0)){
        _5912 = NOVALUE;
        goto L42; // [2052] 2100
    }
    _5912 = NOVALUE;

    /** 									if psign  then*/
    if (_psign_10177 == 0)
    {
        goto L27; // [2060] 2611
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_10179 != 0)
    goto L43; // [2065] 2078

    /** 											argtext = '+' & argtext*/
    Prepend(&_argtext_10353, _argtext_10353, 43);
    goto L27; // [2075] 2611
L43: 

    /** 										elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    _5916 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5916, 48)){
        _5916 = NOVALUE;
        goto L27; // [2084] 2611
    }
    _5916 = NOVALUE;

    /** 											argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_10353 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [2097] 2611
L42: 

    /** 								elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5918 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (binary_op_a(GREATEREQ, _5918, 0)){
        _5918 = NOVALUE;
        goto L27; // [2106] 2611
    }
    _5918 = NOVALUE;

    /** 									if msign then*/
    if (_msign_10178 == 0)
    {
        goto L27; // [2114] 2611
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_10179 != 0)
    goto L44; // [2119] 2142

    /** 											argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5921 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5921 = 1;
    }
    rhs_slice_target = (object_ptr)&_5922;
    RHS_Slice(_argtext_10353, 2, _5921);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5922;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_5922);
    _5922 = NOVALUE;
    goto L27; // [2139] 2611
L44: 

    /** 											if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    _5924 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5924, 48)){
        _5924 = NOVALUE;
        goto L45; // [2148] 2171
    }
    _5924 = NOVALUE;

    /** 												argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5926 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5926 = 1;
    }
    rhs_slice_target = (object_ptr)&_5927;
    RHS_Slice(_argtext_10353, 3, _5926);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5927;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_5927);
    _5927 = NOVALUE;
    goto L27; // [2168] 2611
L45: 

    /** 												argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5929 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5929 = 1;
    }
    rhs_slice_target = (object_ptr)&_5930;
    RHS_Slice(_argtext_10353, 2, _5929);
    Append(&_argtext_10353, _5930, 41);
    DeRefDS(_5930);
    _5930 = NOVALUE;
    goto L27; // [2192] 2611
L38: 

    /** 						if alt != 0 and length(arg_list[argn]) = 2 then*/
    _5932 = (_alt_10182 != 0);
    if (_5932 == 0) {
        goto L46; // [2203] 2522
    }
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5934 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    if (IS_SEQUENCE(_5934)){
            _5935 = SEQ_PTR(_5934)->length;
    }
    else {
        _5935 = 1;
    }
    _5934 = NOVALUE;
    _5936 = (_5935 == 2);
    _5935 = NOVALUE;
    if (_5936 == 0)
    {
        DeRef(_5936);
        _5936 = NOVALUE;
        goto L46; // [2219] 2522
    }
    else{
        DeRef(_5936);
        _5936 = NOVALUE;
    }

    /** 							object tempv*/

    /** 							if atom(prevargv) then*/
    _5937 = IS_ATOM(_prevargv_10193);
    if (_5937 == 0)
    {
        _5937 = NOVALUE;
        goto L47; // [2229] 2265
    }
    else{
        _5937 = NOVALUE;
    }

    /** 								if prevargv != 1 then*/
    if (binary_op_a(EQUALS, _prevargv_10193, 1)){
        goto L48; // [2234] 2251
    }

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5939 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    DeRef(_tempv_10589);
    _2 = (int)SEQ_PTR(_5939);
    _tempv_10589 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_10589);
    _5939 = NOVALUE;
    goto L49; // [2248] 2299
L48: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5941 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    DeRef(_tempv_10589);
    _2 = (int)SEQ_PTR(_5941);
    _tempv_10589 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_10589);
    _5941 = NOVALUE;
    goto L49; // [2262] 2299
L47: 

    /** 								if length(prevargv) = 0 then*/
    if (IS_SEQUENCE(_prevargv_10193)){
            _5943 = SEQ_PTR(_prevargv_10193)->length;
    }
    else {
        _5943 = 1;
    }
    if (_5943 != 0)
    goto L4A; // [2270] 2287

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5945 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    DeRef(_tempv_10589);
    _2 = (int)SEQ_PTR(_5945);
    _tempv_10589 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_10589);
    _5945 = NOVALUE;
    goto L4B; // [2284] 2298
L4A: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5947 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    DeRef(_tempv_10589);
    _2 = (int)SEQ_PTR(_5947);
    _tempv_10589 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_10589);
    _5947 = NOVALUE;
L4B: 
L49: 

    /** 							if string(tempv) then*/
    Ref(_tempv_10589);
    _5949 = _9string(_tempv_10589);
    if (_5949 == 0) {
        DeRef(_5949);
        _5949 = NOVALUE;
        goto L4C; // [2307] 2320
    }
    else {
        if (!IS_ATOM_INT(_5949) && DBL_PTR(_5949)->dbl == 0.0){
            DeRef(_5949);
            _5949 = NOVALUE;
            goto L4C; // [2307] 2320
        }
        DeRef(_5949);
        _5949 = NOVALUE;
    }
    DeRef(_5949);
    _5949 = NOVALUE;

    /** 								argtext = tempv*/
    Ref(_tempv_10589);
    DeRef(_argtext_10353);
    _argtext_10353 = _tempv_10589;
    goto L4D; // [2317] 2517
L4C: 

    /** 							elsif integer(tempv) then*/
    if (IS_ATOM_INT(_tempv_10589))
    _5950 = 1;
    else if (IS_ATOM_DBL(_tempv_10589))
    _5950 = IS_ATOM_INT(DoubleToInt(_tempv_10589));
    else
    _5950 = 0;
    if (_5950 == 0)
    {
        _5950 = NOVALUE;
        goto L4E; // [2325] 2391
    }
    else{
        _5950 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_10192 == 0)
    {
        goto L4F; // [2330] 2350
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(tempv))}*/
    Ref(_tempv_10589);
    _5951 = _18abs(_tempv_10589);
    _5952 = binary_op(AND_BITS, _2108, _5951);
    DeRef(_5951);
    _5951 = NOVALUE;
    _0 = _argtext_10353;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5952;
    _argtext_10353 = MAKE_SEQ(_1);
    DeRef(_0);
    _5952 = NOVALUE;
    goto L4D; // [2347] 2517
L4F: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5954 = (_bwz_10180 != 0);
    if (_5954 == 0) {
        goto L50; // [2358] 2381
    }
    if (IS_ATOM_INT(_tempv_10589)) {
        _5956 = (_tempv_10589 == 0);
    }
    else {
        _5956 = binary_op(EQUALS, _tempv_10589, 0);
    }
    if (_5956 == 0) {
        DeRef(_5956);
        _5956 = NOVALUE;
        goto L50; // [2367] 2381
    }
    else {
        if (!IS_ATOM_INT(_5956) && DBL_PTR(_5956)->dbl == 0.0){
            DeRef(_5956);
            _5956 = NOVALUE;
            goto L50; // [2367] 2381
        }
        DeRef(_5956);
        _5956 = NOVALUE;
    }
    DeRef(_5956);
    _5956 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_10353);
    _argtext_10353 = Repeat(32, _width_10183);
    goto L4D; // [2378] 2517
L50: 

    /** 									argtext = sprintf("%d", tempv)*/
    DeRef(_argtext_10353);
    _argtext_10353 = EPrintf(-9999999, _967, _tempv_10589);
    goto L4D; // [2388] 2517
L4E: 

    /** 							elsif atom(tempv) then*/
    _5959 = IS_ATOM(_tempv_10589);
    if (_5959 == 0)
    {
        _5959 = NOVALUE;
        goto L51; // [2396] 2473
    }
    else{
        _5959 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_10192 == 0)
    {
        goto L52; // [2401] 2424
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(tempv)))}*/
    if (IS_ATOM_INT(_tempv_10589))
    _5960 = e_floor(_tempv_10589);
    else
    _5960 = unary_op(FLOOR, _tempv_10589);
    _5961 = _18abs(_5960);
    _5960 = NOVALUE;
    _5962 = binary_op(AND_BITS, _2108, _5961);
    DeRef(_5961);
    _5961 = NOVALUE;
    _0 = _argtext_10353;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5962;
    _argtext_10353 = MAKE_SEQ(_1);
    DeRef(_0);
    _5962 = NOVALUE;
    goto L4D; // [2421] 2517
L52: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5964 = (_bwz_10180 != 0);
    if (_5964 == 0) {
        goto L53; // [2432] 2455
    }
    if (IS_ATOM_INT(_tempv_10589)) {
        _5966 = (_tempv_10589 == 0);
    }
    else {
        _5966 = binary_op(EQUALS, _tempv_10589, 0);
    }
    if (_5966 == 0) {
        DeRef(_5966);
        _5966 = NOVALUE;
        goto L53; // [2441] 2455
    }
    else {
        if (!IS_ATOM_INT(_5966) && DBL_PTR(_5966)->dbl == 0.0){
            DeRef(_5966);
            _5966 = NOVALUE;
            goto L53; // [2441] 2455
        }
        DeRef(_5966);
        _5966 = NOVALUE;
    }
    DeRef(_5966);
    _5966 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_10353);
    _argtext_10353 = Repeat(32, _width_10183);
    goto L4D; // [2452] 2517
L53: 

    /** 									argtext = trim(sprintf("%15.15g", tempv))*/
    _5968 = EPrintf(-9999999, _5886, _tempv_10589);
    RefDS(_4494);
    _0 = _argtext_10353;
    _argtext_10353 = _10trim(_5968, _4494, 0);
    DeRef(_0);
    _5968 = NOVALUE;
    goto L4D; // [2470] 2517
L51: 

    /** 								argtext = pretty:pretty_sprint( tempv,*/
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_967);
    *((int *)(_2+20)) = _967;
    RefDS(_5970);
    *((int *)(_2+24)) = _5970;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5971 = MAKE_SEQ(_1);
    DeRef(_options_inlined_pretty_sprint_at_2489_10643);
    _options_inlined_pretty_sprint_at_2489_10643 = _5971;
    _5971 = NOVALUE;

    /** 	pretty_printing = 0*/
    _24pretty_printing_8608 = 0;

    /** 	pretty( x, options )*/
    Ref(_tempv_10589);
    RefDS(_options_inlined_pretty_sprint_at_2489_10643);
    _24pretty(_tempv_10589, _options_inlined_pretty_sprint_at_2489_10643);

    /** 	return pretty_line*/
    RefDS(_24pretty_line_8611);
    DeRef(_argtext_10353);
    _argtext_10353 = _24pretty_line_8611;
    DeRef(_options_inlined_pretty_sprint_at_2489_10643);
    _options_inlined_pretty_sprint_at_2489_10643 = NOVALUE;
L4D: 
    DeRef(_tempv_10589);
    _tempv_10589 = NOVALUE;
    goto L54; // [2519] 2598
L46: 

    /** 							argtext = pretty:pretty_sprint( arg_list[argn],*/
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _5972 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_967);
    *((int *)(_2+20)) = _967;
    RefDS(_5970);
    *((int *)(_2+24)) = _5970;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5973 = MAKE_SEQ(_1);
    Ref(_5972);
    DeRef(_x_inlined_pretty_sprint_at_2542_10649);
    _x_inlined_pretty_sprint_at_2542_10649 = _5972;
    _5972 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2545_10650);
    _options_inlined_pretty_sprint_at_2545_10650 = _5973;
    _5973 = NOVALUE;

    /** 	pretty_printing = 0*/
    _24pretty_printing_8608 = 0;

    /** 	pretty( x, options )*/
    Ref(_x_inlined_pretty_sprint_at_2542_10649);
    RefDS(_options_inlined_pretty_sprint_at_2545_10650);
    _24pretty(_x_inlined_pretty_sprint_at_2542_10649, _options_inlined_pretty_sprint_at_2545_10650);

    /** 	return pretty_line*/
    RefDS(_24pretty_line_8611);
    DeRef(_argtext_10353);
    _argtext_10353 = _24pretty_line_8611;
    DeRef(_x_inlined_pretty_sprint_at_2542_10649);
    _x_inlined_pretty_sprint_at_2542_10649 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2545_10650);
    _options_inlined_pretty_sprint_at_2545_10650 = NOVALUE;

    /** 						while ep != 0 with entry do*/
    goto L54; // [2575] 2598
L55: 
    if (_ep_10198 == 0)
    goto L56; // [2580] 2610

    /** 							argtext = remove(argtext, ep+2)*/
    _5975 = _ep_10198 + 2;
    if ((long)((unsigned long)_5975 + (unsigned long)HIGH_BITS) >= 0) 
    _5975 = NewDouble((double)_5975);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_10353);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5975)) ? _5975 : (long)(DBL_PTR(_5975)->dbl);
        int stop = (IS_ATOM_INT(_5975)) ? _5975 : (long)(DBL_PTR(_5975)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_10353), start, &_argtext_10353 );
            }
            else Tail(SEQ_PTR(_argtext_10353), stop+1, &_argtext_10353);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_10353), start, &_argtext_10353);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_10353 = Remove_elements(start, stop, (SEQ_PTR(_argtext_10353)->ref == 1));
        }
    }
    DeRef(_5975);
    _5975 = NOVALUE;
    _5975 = NOVALUE;

    /** 						entry*/
L54: 

    /** 							ep = match("e+0", argtext)*/
    _ep_10198 = e_match_from(_5893, _argtext_10353, 1);

    /** 						end while*/
    goto L55; // [2607] 2578
L56: 
L27: 

    /** 	    			currargv = arg_list[argn]*/
    DeRef(_currargv_10194);
    _2 = (int)SEQ_PTR(_arg_list_10169);
    _currargv_10194 = (int)*(((s1_ptr)_2)->base + _argn_10186);
    Ref(_currargv_10194);
L24: 

    /**     			if length(argtext) > 0 then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5979 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5979 = 1;
    }
    if (_5979 <= 0)
    goto L57; // [2623] 3533

    /**     				switch cap do*/
    _0 = _cap_10175;
    switch ( _0 ){ 

        /**     					case 'u' then*/
        case 117:

        /**     						argtext = upper(argtext)*/
        RefDS(_argtext_10353);
        _0 = _argtext_10353;
        _argtext_10353 = _10upper(_argtext_10353);
        DeRefDS(_0);
        goto L58; // [2648] 2714

        /**     					case 'l' then*/
        case 108:

        /**     						argtext = lower(argtext)*/
        RefDS(_argtext_10353);
        _0 = _argtext_10353;
        _argtext_10353 = _10lower(_argtext_10353);
        DeRefDS(_0);
        goto L58; // [2662] 2714

        /**     					case 'w' then*/
        case 119:

        /**     						argtext = proper(argtext)*/
        RefDS(_argtext_10353);
        _0 = _argtext_10353;
        _argtext_10353 = _10proper(_argtext_10353);
        DeRefDS(_0);
        goto L58; // [2676] 2714

        /**     					case 0 then*/
        case 0:

        /** 							cap = cap*/
        _cap_10175 = _cap_10175;
        goto L58; // [2687] 2714

        /**     					case else*/
        default:

        /**     						error:crash("logic error: 'cap' mode in format.")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_2696_10673);
        _msg_inlined_crash_at_2696_10673 = EPrintf(-9999999, _5986, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_2696_10673);

        /** end procedure*/
        goto L59; // [2708] 2711
L59: 
        DeRefi(_msg_inlined_crash_at_2696_10673);
        _msg_inlined_crash_at_2696_10673 = NOVALUE;
    ;}L58: 

    /** 					if atom(currargv) then*/
    _5987 = IS_ATOM(_currargv_10194);
    if (_5987 == 0)
    {
        _5987 = NOVALUE;
        goto L5A; // [2721] 2964
    }
    else{
        _5987 = NOVALUE;
    }

    /** 						if find('e', argtext) = 0 then*/
    _5988 = find_from(101, _argtext_10353, 1);
    if (_5988 != 0)
    goto L5B; // [2731] 2963

    /** 							pflag = 0*/
    _pflag_10199 = 0;

    /** 							if msign and currargv < 0 then*/
    if (_msign_10178 == 0) {
        goto L5C; // [2744] 2762
    }
    if (IS_ATOM_INT(_currargv_10194)) {
        _5991 = (_currargv_10194 < 0);
    }
    else {
        _5991 = binary_op(LESS, _currargv_10194, 0);
    }
    if (_5991 == 0) {
        DeRef(_5991);
        _5991 = NOVALUE;
        goto L5C; // [2753] 2762
    }
    else {
        if (!IS_ATOM_INT(_5991) && DBL_PTR(_5991)->dbl == 0.0){
            DeRef(_5991);
            _5991 = NOVALUE;
            goto L5C; // [2753] 2762
        }
        DeRef(_5991);
        _5991 = NOVALUE;
    }
    DeRef(_5991);
    _5991 = NOVALUE;

    /** 								pflag = 1*/
    _pflag_10199 = 1;
L5C: 

    /** 							if decs != -1 then*/
    if (_decs_10184 == -1)
    goto L5D; // [2766] 2962

    /** 								pos = find('.', argtext)*/
    _pos_10185 = find_from(46, _argtext_10353, 1);

    /** 								if pos then*/
    if (_pos_10185 == 0)
    {
        goto L5E; // [2779] 2907
    }
    else{
    }

    /** 									if decs = 0 then*/
    if (_decs_10184 != 0)
    goto L5F; // [2784] 2802

    /** 										argtext = argtext [1 .. pos-1 ]*/
    _5995 = _pos_10185 - 1;
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, 1, _5995);
    goto L60; // [2799] 2961
L5F: 

    /** 										pos = length(argtext) - pos - pflag*/
    if (IS_SEQUENCE(_argtext_10353)){
            _5997 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _5997 = 1;
    }
    _5998 = _5997 - _pos_10185;
    if ((long)((unsigned long)_5998 +(unsigned long) HIGH_BITS) >= 0){
        _5998 = NewDouble((double)_5998);
    }
    _5997 = NOVALUE;
    if (IS_ATOM_INT(_5998)) {
        _pos_10185 = _5998 - _pflag_10199;
    }
    else {
        _pos_10185 = NewDouble(DBL_PTR(_5998)->dbl - (double)_pflag_10199);
    }
    DeRef(_5998);
    _5998 = NOVALUE;
    if (!IS_ATOM_INT(_pos_10185)) {
        _1 = (long)(DBL_PTR(_pos_10185)->dbl);
        if (UNIQUE(DBL_PTR(_pos_10185)) && (DBL_PTR(_pos_10185)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_10185);
        _pos_10185 = _1;
    }

    /** 										if pos > decs then*/
    if (_pos_10185 <= _decs_10184)
    goto L61; // [2819] 2844

    /** 											argtext = argtext[ 1 .. $ - pos + decs ]*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6001 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6001 = 1;
    }
    _6002 = _6001 - _pos_10185;
    if ((long)((unsigned long)_6002 +(unsigned long) HIGH_BITS) >= 0){
        _6002 = NewDouble((double)_6002);
    }
    _6001 = NOVALUE;
    if (IS_ATOM_INT(_6002)) {
        _6003 = _6002 + _decs_10184;
    }
    else {
        _6003 = NewDouble(DBL_PTR(_6002)->dbl + (double)_decs_10184);
    }
    DeRef(_6002);
    _6002 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, 1, _6003);
    goto L60; // [2841] 2961
L61: 

    /** 										elsif pos < decs then*/
    if (_pos_10185 >= _decs_10184)
    goto L60; // [2846] 2961

    /** 											if pflag then*/
    if (_pflag_10199 == 0)
    {
        goto L62; // [2852] 2886
    }
    else{
    }

    /** 												argtext = argtext[ 1 .. $ - 1 ] & repeat('0', decs - pos) & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6006 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6006 = 1;
    }
    _6007 = _6006 - 1;
    _6006 = NOVALUE;
    rhs_slice_target = (object_ptr)&_6008;
    RHS_Slice(_argtext_10353, 1, _6007);
    _6009 = _decs_10184 - _pos_10185;
    _6010 = Repeat(48, _6009);
    _6009 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _6010;
        concat_list[2] = _6008;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_6010);
    _6010 = NOVALUE;
    DeRefDS(_6008);
    _6008 = NOVALUE;
    goto L60; // [2883] 2961
L62: 

    /** 												argtext = argtext & repeat('0', decs - pos)*/
    _6012 = _decs_10184 - _pos_10185;
    _6013 = Repeat(48, _6012);
    _6012 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _argtext_10353, _6013);
    DeRefDS(_6013);
    _6013 = NOVALUE;
    goto L60; // [2904] 2961
L5E: 

    /** 								elsif decs > 0 then*/
    if (_decs_10184 <= 0)
    goto L63; // [2909] 2960

    /** 									if pflag then*/
    if (_pflag_10199 == 0)
    {
        goto L64; // [2915] 2946
    }
    else{
    }

    /** 										argtext = argtext[1 .. $ - 1] & '.' & repeat('0', decs) & ')'*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6016 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6016 = 1;
    }
    _6017 = _6016 - 1;
    _6016 = NOVALUE;
    rhs_slice_target = (object_ptr)&_6018;
    RHS_Slice(_argtext_10353, 1, _6017);
    _6019 = Repeat(48, _decs_10184);
    {
        int concat_list[4];

        concat_list[0] = 41;
        concat_list[1] = _6019;
        concat_list[2] = 46;
        concat_list[3] = _6018;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 4);
    }
    DeRefDS(_6019);
    _6019 = NOVALUE;
    DeRefDS(_6018);
    _6018 = NOVALUE;
    goto L65; // [2943] 2959
L64: 

    /** 										argtext = argtext & '.' & repeat('0', decs)*/
    _6021 = Repeat(48, _decs_10184);
    {
        int concat_list[3];

        concat_list[0] = _6021;
        concat_list[1] = 46;
        concat_list[2] = _argtext_10353;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_6021);
    _6021 = NOVALUE;
L65: 
L63: 
L60: 
L5D: 
L5B: 
L5A: 

    /**     				if align = 0 then*/
    if (_align_10176 != 0)
    goto L66; // [2968] 2995

    /**     					if atom(currargv) then*/
    _6024 = IS_ATOM(_currargv_10194);
    if (_6024 == 0)
    {
        _6024 = NOVALUE;
        goto L67; // [2977] 2988
    }
    else{
        _6024 = NOVALUE;
    }

    /**     						align = '>'*/
    _align_10176 = 62;
    goto L68; // [2985] 2994
L67: 

    /**     						align = '<'*/
    _align_10176 = 60;
L68: 
L66: 

    /**     				if atom(currargv) then*/
    _6025 = IS_ATOM(_currargv_10194);
    if (_6025 == 0)
    {
        _6025 = NOVALUE;
        goto L69; // [3000] 3237
    }
    else{
        _6025 = NOVALUE;
    }

    /** 	    				if tsep != 0 and zfill = 0 then*/
    _6026 = (_tsep_10191 != 0);
    if (_6026 == 0) {
        goto L6A; // [3011] 3234
    }
    _6028 = (_zfill_10179 == 0);
    if (_6028 == 0)
    {
        DeRef(_6028);
        _6028 = NOVALUE;
        goto L6A; // [3022] 3234
    }
    else{
        DeRef(_6028);
        _6028 = NOVALUE;
    }

    /** 	    					integer dpos*/

    /** 	    					integer dist*/

    /** 	    					integer bracketed*/

    /** 	    					if binout or hexout then*/
    if (_binout_10190 != 0) {
        goto L6B; // [3035] 3044
    }
    if (_hexout_10189 == 0)
    {
        goto L6C; // [3040] 3057
    }
    else{
    }
L6B: 

    /** 	    						dist = 4*/
    _dist_10736 = 4;

    /** 							psign = 0*/
    _psign_10177 = 0;
    goto L6D; // [3054] 3063
L6C: 

    /** 	    						dist = 3*/
    _dist_10736 = 3;
L6D: 

    /** 	    					bracketed = (argtext[1] = '(')*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    _6030 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6030)) {
        _bracketed_10737 = (_6030 == 40);
    }
    else {
        _bracketed_10737 = binary_op(EQUALS, _6030, 40);
    }
    _6030 = NOVALUE;
    if (!IS_ATOM_INT(_bracketed_10737)) {
        _1 = (long)(DBL_PTR(_bracketed_10737)->dbl);
        if (UNIQUE(DBL_PTR(_bracketed_10737)) && (DBL_PTR(_bracketed_10737)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bracketed_10737);
        _bracketed_10737 = _1;
    }

    /** 	    					if bracketed then*/
    if (_bracketed_10737 == 0)
    {
        goto L6E; // [3077] 3095
    }
    else{
    }

    /** 	    						argtext = argtext[2 .. $-1]*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6032 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6032 = 1;
    }
    _6033 = _6032 - 1;
    _6032 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, 2, _6033);
L6E: 

    /** 	    					dpos = find('.', argtext)*/
    _dpos_10735 = find_from(46, _argtext_10353, 1);

    /** 	    					if dpos = 0 then*/
    if (_dpos_10735 != 0)
    goto L6F; // [3104] 3120

    /** 	    						dpos = length(argtext) + 1*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6037 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6037 = 1;
    }
    _dpos_10735 = _6037 + 1;
    _6037 = NOVALUE;
    goto L70; // [3117] 3134
L6F: 

    /** 	    						if tsep = '.' then*/
    if (_tsep_10191 != 46)
    goto L71; // [3122] 3133

    /** 	    							argtext[dpos] = ','*/
    _2 = (int)SEQ_PTR(_argtext_10353);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_10353 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _dpos_10735);
    _1 = *(int *)_2;
    *(int *)_2 = 44;
    DeRef(_1);
L71: 
L70: 

    /** 	    					while dpos > dist do*/
L72: 
    if (_dpos_10735 <= _dist_10736)
    goto L73; // [3141] 3219

    /** 	    						dpos -= dist*/
    _dpos_10735 = _dpos_10735 - _dist_10736;

    /** 	    						if dpos > 1 + (currargv < 0) * not msign + (currargv > 0) * psign then*/
    if (IS_ATOM_INT(_currargv_10194)) {
        _6042 = (_currargv_10194 < 0);
    }
    else {
        _6042 = binary_op(LESS, _currargv_10194, 0);
    }
    _6043 = (_msign_10178 == 0);
    if (IS_ATOM_INT(_6042)) {
        if (_6042 == (short)_6042 && _6043 <= INT15 && _6043 >= -INT15)
        _6044 = _6042 * _6043;
        else
        _6044 = NewDouble(_6042 * (double)_6043);
    }
    else {
        _6044 = binary_op(MULTIPLY, _6042, _6043);
    }
    DeRef(_6042);
    _6042 = NOVALUE;
    _6043 = NOVALUE;
    if (IS_ATOM_INT(_6044)) {
        _6045 = _6044 + 1;
        if (_6045 > MAXINT){
            _6045 = NewDouble((double)_6045);
        }
    }
    else
    _6045 = binary_op(PLUS, 1, _6044);
    DeRef(_6044);
    _6044 = NOVALUE;
    if (IS_ATOM_INT(_currargv_10194)) {
        _6046 = (_currargv_10194 > 0);
    }
    else {
        _6046 = binary_op(GREATER, _currargv_10194, 0);
    }
    if (IS_ATOM_INT(_6046)) {
        if (_6046 == (short)_6046 && _psign_10177 <= INT15 && _psign_10177 >= -INT15)
        _6047 = _6046 * _psign_10177;
        else
        _6047 = NewDouble(_6046 * (double)_psign_10177);
    }
    else {
        _6047 = binary_op(MULTIPLY, _6046, _psign_10177);
    }
    DeRef(_6046);
    _6046 = NOVALUE;
    if (IS_ATOM_INT(_6045) && IS_ATOM_INT(_6047)) {
        _6048 = _6045 + _6047;
        if ((long)((unsigned long)_6048 + (unsigned long)HIGH_BITS) >= 0) 
        _6048 = NewDouble((double)_6048);
    }
    else {
        _6048 = binary_op(PLUS, _6045, _6047);
    }
    DeRef(_6045);
    _6045 = NOVALUE;
    DeRef(_6047);
    _6047 = NOVALUE;
    if (binary_op_a(LESSEQ, _dpos_10735, _6048)){
        DeRef(_6048);
        _6048 = NOVALUE;
        goto L72; // [3184] 3139
    }
    DeRef(_6048);
    _6048 = NOVALUE;

    /** 	    							argtext = argtext[1.. dpos - 1] & tsep & argtext[dpos .. $]*/
    _6050 = _dpos_10735 - 1;
    rhs_slice_target = (object_ptr)&_6051;
    RHS_Slice(_argtext_10353, 1, _6050);
    if (IS_SEQUENCE(_argtext_10353)){
            _6052 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6052 = 1;
    }
    rhs_slice_target = (object_ptr)&_6053;
    RHS_Slice(_argtext_10353, _dpos_10735, _6052);
    {
        int concat_list[3];

        concat_list[0] = _6053;
        concat_list[1] = _tsep_10191;
        concat_list[2] = _6051;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_6053);
    _6053 = NOVALUE;
    DeRefDS(_6051);
    _6051 = NOVALUE;

    /** 	    					end while*/
    goto L72; // [3216] 3139
L73: 

    /** 	    					if bracketed then*/
    if (_bracketed_10737 == 0)
    {
        goto L74; // [3221] 3233
    }
    else{
    }

    /** 	    						argtext = '(' & argtext & ')'*/
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _argtext_10353;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
L74: 
L6A: 
L69: 

    /**     				if width <= 0 then*/
    if (_width_10183 > 0)
    goto L75; // [3241] 3251

    /**     					width = length(argtext)*/
    if (IS_SEQUENCE(_argtext_10353)){
            _width_10183 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _width_10183 = 1;
    }
L75: 

    /**     				if width < length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6058 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6058 = 1;
    }
    if (_width_10183 >= _6058)
    goto L76; // [3256] 3387

    /**     					if align = '>' then*/
    if (_align_10176 != 62)
    goto L77; // [3262] 3290

    /**     						argtext = argtext[ $ - width + 1 .. $]*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6061 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6061 = 1;
    }
    _6062 = _6061 - _width_10183;
    if ((long)((unsigned long)_6062 +(unsigned long) HIGH_BITS) >= 0){
        _6062 = NewDouble((double)_6062);
    }
    _6061 = NOVALUE;
    if (IS_ATOM_INT(_6062)) {
        _6063 = _6062 + 1;
        if (_6063 > MAXINT){
            _6063 = NewDouble((double)_6063);
        }
    }
    else
    _6063 = binary_op(PLUS, 1, _6062);
    DeRef(_6062);
    _6062 = NOVALUE;
    if (IS_SEQUENCE(_argtext_10353)){
            _6064 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6064 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, _6063, _6064);
    goto L78; // [3287] 3524
L77: 

    /**     					elsif align = 'c' then*/
    if (_align_10176 != 99)
    goto L79; // [3292] 3376

    /**     						pos = length(argtext) - width*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6067 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6067 = 1;
    }
    _pos_10185 = _6067 - _width_10183;
    _6067 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _6069 = (_pos_10185 % 2);
    if (_6069 != 0)
    goto L7A; // [3311] 3344

    /**     							pos = pos / 2*/
    if (_pos_10185 & 1) {
        _pos_10185 = NewDouble((_pos_10185 >> 1) + 0.5);
    }
    else
    _pos_10185 = _pos_10185 >> 1;
    if (!IS_ATOM_INT(_pos_10185)) {
        _1 = (long)(DBL_PTR(_pos_10185)->dbl);
        if (UNIQUE(DBL_PTR(_pos_10185)) && (DBL_PTR(_pos_10185)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_10185);
        _pos_10185 = _1;
    }

    /**     							argtext = argtext[ pos + 1 .. $ - pos ]*/
    _6072 = _pos_10185 + 1;
    if (_6072 > MAXINT){
        _6072 = NewDouble((double)_6072);
    }
    if (IS_SEQUENCE(_argtext_10353)){
            _6073 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6073 = 1;
    }
    _6074 = _6073 - _pos_10185;
    _6073 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, _6072, _6074);
    goto L78; // [3341] 3524
L7A: 

    /**     							pos = floor(pos / 2)*/
    _pos_10185 = _pos_10185 >> 1;

    /**     							argtext = argtext[ pos + 1 .. $ - pos - 1]*/
    _6077 = _pos_10185 + 1;
    if (_6077 > MAXINT){
        _6077 = NewDouble((double)_6077);
    }
    if (IS_SEQUENCE(_argtext_10353)){
            _6078 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6078 = 1;
    }
    _6079 = _6078 - _pos_10185;
    if ((long)((unsigned long)_6079 +(unsigned long) HIGH_BITS) >= 0){
        _6079 = NewDouble((double)_6079);
    }
    _6078 = NOVALUE;
    if (IS_ATOM_INT(_6079)) {
        _6080 = _6079 - 1;
    }
    else {
        _6080 = NewDouble(DBL_PTR(_6079)->dbl - (double)1);
    }
    DeRef(_6079);
    _6079 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, _6077, _6080);
    goto L78; // [3373] 3524
L79: 

    /**     						argtext = argtext[ 1 .. width]*/
    rhs_slice_target = (object_ptr)&_argtext_10353;
    RHS_Slice(_argtext_10353, 1, _width_10183);
    goto L78; // [3384] 3524
L76: 

    /**     				elsif width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6083 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6083 = 1;
    }
    if (_width_10183 <= _6083)
    goto L7B; // [3392] 3523

    /** 						if align = '>' then*/
    if (_align_10176 != 62)
    goto L7C; // [3398] 3422

    /** 							argtext = repeat(' ', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6086 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6086 = 1;
    }
    _6087 = _width_10183 - _6086;
    _6086 = NOVALUE;
    _6088 = Repeat(32, _6087);
    _6087 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _6088, _argtext_10353);
    DeRefDS(_6088);
    _6088 = NOVALUE;
    DeRef(_6088);
    _6088 = NOVALUE;
    goto L7D; // [3419] 3522
L7C: 

    /**     					elsif align = 'c' then*/
    if (_align_10176 != 99)
    goto L7E; // [3424] 3504

    /**     						pos = width - length(argtext)*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6091 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6091 = 1;
    }
    _pos_10185 = _width_10183 - _6091;
    _6091 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _6093 = (_pos_10185 % 2);
    if (_6093 != 0)
    goto L7F; // [3443] 3474

    /**     							pos = pos / 2*/
    if (_pos_10185 & 1) {
        _pos_10185 = NewDouble((_pos_10185 >> 1) + 0.5);
    }
    else
    _pos_10185 = _pos_10185 >> 1;
    if (!IS_ATOM_INT(_pos_10185)) {
        _1 = (long)(DBL_PTR(_pos_10185)->dbl);
        if (UNIQUE(DBL_PTR(_pos_10185)) && (DBL_PTR(_pos_10185)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_10185);
        _pos_10185 = _1;
    }

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos)*/
    _6096 = Repeat(32, _pos_10185);
    _6097 = Repeat(32, _pos_10185);
    {
        int concat_list[3];

        concat_list[0] = _6097;
        concat_list[1] = _argtext_10353;
        concat_list[2] = _6096;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_6097);
    _6097 = NOVALUE;
    DeRefDS(_6096);
    _6096 = NOVALUE;
    goto L7D; // [3471] 3522
L7F: 

    /**     							pos = floor(pos / 2)*/
    _pos_10185 = _pos_10185 >> 1;

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos + 1)*/
    _6100 = Repeat(32, _pos_10185);
    _6101 = _pos_10185 + 1;
    _6102 = Repeat(32, _6101);
    _6101 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _6102;
        concat_list[1] = _argtext_10353;
        concat_list[2] = _6100;
        Concat_N((object_ptr)&_argtext_10353, concat_list, 3);
    }
    DeRefDS(_6102);
    _6102 = NOVALUE;
    DeRefDS(_6100);
    _6100 = NOVALUE;
    goto L7D; // [3501] 3522
L7E: 

    /** 							argtext = argtext & repeat(' ', width - length(argtext))*/
    if (IS_SEQUENCE(_argtext_10353)){
            _6104 = SEQ_PTR(_argtext_10353)->length;
    }
    else {
        _6104 = 1;
    }
    _6105 = _width_10183 - _6104;
    _6104 = NOVALUE;
    _6106 = Repeat(32, _6105);
    _6105 = NOVALUE;
    Concat((object_ptr)&_argtext_10353, _argtext_10353, _6106);
    DeRefDS(_6106);
    _6106 = NOVALUE;
L7D: 
L7B: 
L78: 

    /**     				result &= argtext*/
    Concat((object_ptr)&_result_10170, _result_10170, _argtext_10353);
    goto L80; // [3530] 3546
L57: 

    /**     				if spacer then*/
    if (_spacer_10181 == 0)
    {
        goto L81; // [3535] 3545
    }
    else{
    }

    /**     					result &= ' '*/
    Append(&_result_10170, _result_10170, 32);
L81: 
L80: 

    /**    				if trimming then*/
    if (_trimming_10188 == 0)
    {
        goto L82; // [3550] 3564
    }
    else{
    }

    /**    					result = trim(result)*/
    RefDS(_result_10170);
    RefDS(_4494);
    _0 = _result_10170;
    _result_10170 = _10trim(_result_10170, _4494, 0);
    DeRefDS(_0);
L82: 

    /**     			tend = 0*/
    _tend_10174 = 0;

    /** 		    	prevargv = currargv*/
    Ref(_currargv_10194);
    DeRef(_prevargv_10193);
    _prevargv_10193 = _currargv_10194;
L1F: 
    DeRef(_argtext_10353);
    _argtext_10353 = NOVALUE;

    /**     end while*/
    goto L2; // [3582] 60
L3: 

    /** 	return result*/
    DeRefDS(_format_pattern_10168);
    DeRef(_arg_list_10169);
    DeRef(_prevargv_10193);
    DeRef(_currargv_10194);
    DeRef(_idname_10195);
    DeRef(_envsym_10196);
    DeRefi(_envvar_10197);
    DeRef(_6069);
    _6069 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    DeRef(_5995);
    _5995 = NOVALUE;
    DeRef(_6063);
    _6063 = NOVALUE;
    DeRef(_5714);
    _5714 = NOVALUE;
    DeRef(_6074);
    _6074 = NOVALUE;
    DeRef(_5737);
    _5737 = NOVALUE;
    DeRef(_5764);
    _5764 = NOVALUE;
    DeRef(_5816);
    _5816 = NOVALUE;
    DeRef(_6033);
    _6033 = NOVALUE;
    DeRef(_6093);
    _6093 = NOVALUE;
    DeRef(_6026);
    _6026 = NOVALUE;
    DeRef(_6017);
    _6017 = NOVALUE;
    _5934 = NOVALUE;
    DeRef(_6003);
    _6003 = NOVALUE;
    DeRef(_5895);
    _5895 = NOVALUE;
    DeRef(_6080);
    _6080 = NOVALUE;
    DeRef(_5964);
    _5964 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6072);
    _6072 = NOVALUE;
    DeRef(_5859);
    _5859 = NOVALUE;
    DeRef(_5932);
    _5932 = NOVALUE;
    DeRef(_5796);
    _5796 = NOVALUE;
    DeRef(_5806);
    _5806 = NOVALUE;
    DeRef(_6007);
    _6007 = NOVALUE;
    DeRef(_5877);
    _5877 = NOVALUE;
    DeRef(_5954);
    _5954 = NOVALUE;
    DeRef(_5776);
    _5776 = NOVALUE;
    DeRef(_5786);
    _5786 = NOVALUE;
    return _result_10170;
    ;
}



// 0xE5E423D0
