// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _64e_to_c(int _ee_22984)
{
    int _ce_22985 = NOVALUE;
    int _13470 = NOVALUE;
    int _13469 = NOVALUE;
    int _13468 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer ce = 0*/
    _ce_22985 = 0;

    /** 	for i = 1 to length(ee) do*/
    if (IS_SEQUENCE(_ee_22984)){
            _13468 = SEQ_PTR(_ee_22984)->length;
    }
    else {
        _13468 = 1;
    }
    {
        int _i_22987;
        _i_22987 = 1;
L1: 
        if (_i_22987 > _13468){
            goto L2; // [11] 43
        }

        /** 		ce = or_bits(ce, conv_table[ee[i]])*/
        _2 = (int)SEQ_PTR(_ee_22984);
        _13469 = (int)*(((s1_ptr)_2)->base + _i_22987);
        _2 = (int)SEQ_PTR(_64conv_table_22978);
        if (!IS_ATOM_INT(_13469)){
            _13470 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_13469)->dbl));
        }
        else{
            _13470 = (int)*(((s1_ptr)_2)->base + _13469);
        }
        {unsigned long tu;
             tu = (unsigned long)_ce_22985 | (unsigned long)_13470;
             _ce_22985 = MAKE_UINT(tu);
        }
        _13470 = NOVALUE;
        if (!IS_ATOM_INT(_ce_22985)) {
            _1 = (long)(DBL_PTR(_ce_22985)->dbl);
            if (UNIQUE(DBL_PTR(_ce_22985)) && (DBL_PTR(_ce_22985)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ce_22985);
            _ce_22985 = _1;
        }

        /** 	end for*/
        _i_22987 = _i_22987 + 1;
        goto L1; // [38] 18
L2: 
        ;
    }

    /** 	return ce*/
    DeRef(_ee_22984);
    _13469 = NOVALUE;
    return _ce_22985;
    ;
}


int _64c_to_e(int _ce_22994)
{
    int _ee_22995 = NOVALUE;
    int _i_22996 = NOVALUE;
    int _13475 = NOVALUE;
    int _13473 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence ee = {}*/
    RefDS(_5);
    DeRefi(_ee_22995);
    _ee_22995 = _5;

    /** 	integer i = 1*/
    _i_22996 = 1;

    /** 	while i <= C_FE_INEXACT do*/
L1: 
    if (_i_22996 > 32)
    goto L2; // [22] 60

    /** 		if and_bits(ce, i) = i then*/
    {unsigned long tu;
         tu = (unsigned long)_ce_22994 & (unsigned long)_i_22996;
         _13473 = MAKE_UINT(tu);
    }
    if (_13473 != _i_22996)
    goto L3; // [32] 49

    /** 			ee = append(ee, rev[i])*/
    _2 = (int)SEQ_PTR(_64rev_22980);
    _13475 = (int)*(((s1_ptr)_2)->base + _i_22996);
    Append(&_ee_22995, _ee_22995, _13475);
    _13475 = NOVALUE;
L3: 

    /** 		i *= 2*/
    _i_22996 = _i_22996 + _i_22996;

    /** 	end while*/
    goto L1; // [57] 20
L2: 

    /** 	return ee*/
    DeRef(_13473);
    _13473 = NOVALUE;
    return _ee_22995;
    ;
}


int _64clear(int _e_p_23007)
{
    int _ee_23008 = NOVALUE;
    int _13483 = NOVALUE;
    int _13482 = NOVALUE;
    int _13481 = NOVALUE;
    int _13478 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(e_p) then*/
    if (IS_ATOM_INT(_e_p_23007))
    _13478 = 1;
    else if (IS_ATOM_DBL(_e_p_23007))
    _13478 = IS_ATOM_INT(DoubleToInt(_e_p_23007));
    else
    _13478 = 0;
    if (_13478 == 0)
    {
        _13478 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _13478 = NOVALUE;
    }

    /** 		ee = {e_p}*/
    _0 = _ee_23008;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_e_p_23007);
    *((int *)(_2+4)) = _e_p_23007;
    _ee_23008 = MAKE_SEQ(_1);
    DeRef(_0);
    goto L2; // [15] 24
L1: 

    /** 		ee = e_p*/
    Ref(_e_p_23007);
    DeRef(_ee_23008);
    _ee_23008 = _e_p_23007;
L2: 

    /** 	if feclearexcept != -1 then*/
    if (binary_op_a(EQUALS, _64feclearexcept_22910, -1)){
        goto L3; // [28] 58
    }

    /** 		return c_func(feclearexcept, {e_to_c(ee)})*/
    Ref(_ee_23008);
    _13481 = _64e_to_c(_ee_23008);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13481;
    _13482 = MAKE_SEQ(_1);
    _13481 = NOVALUE;
    _13483 = call_c(1, _64feclearexcept_22910, _13482);
    DeRefDS(_13482);
    _13482 = NOVALUE;
    DeRef(_e_p_23007);
    DeRef(_ee_23008);
    return _13483;
    goto L4; // [55] 65
L3: 

    /** 		return -1*/
    DeRef(_e_p_23007);
    DeRef(_ee_23008);
    DeRef(_13483);
    _13483 = NOVALUE;
    return -1;
L4: 
    ;
}


int _64raise(int _e_p_23021)
{
    int _ee_23022 = NOVALUE;
    int _13489 = NOVALUE;
    int _13488 = NOVALUE;
    int _13487 = NOVALUE;
    int _13484 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(e_p) then*/
    _13484 = 1;
    if (_13484 == 0)
    {
        _13484 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _13484 = NOVALUE;
    }

    /** 		ee = {e_p}*/
    _0 = _ee_23022;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _e_p_23021;
    _ee_23022 = MAKE_SEQ(_1);
    DeRefi(_0);
    goto L2; // [15] 24
L1: 

    /** 		ee = e_p*/
    DeRefi(_ee_23022);
    _ee_23022 = _e_p_23021;
L2: 

    /** 	if feraiseexcept = -1 then*/
    if (binary_op_a(NOTEQ, _64feraiseexcept_22919, -1)){
        goto L3; // [28] 39
    }

    /** 		return -1*/
    DeRefi(_ee_23022);
    return -1;
L3: 

    /** 	return c_func(feraiseexcept, {e_to_c(ee)})*/
    Ref(_ee_23022);
    _13487 = _64e_to_c(_ee_23022);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13487;
    _13488 = MAKE_SEQ(_1);
    _13487 = NOVALUE;
    _13489 = call_c(1, _64feraiseexcept_22919, _13488);
    DeRefDS(_13488);
    _13488 = NOVALUE;
    DeRefi(_ee_23022);
    return _13489;
    ;
}


int _64test(int _e_p_23034)
{
    int _ee_23035 = NOVALUE;
    int _ce_23042 = NOVALUE;
    int _ans_23046 = NOVALUE;
    int _13497 = NOVALUE;
    int _13494 = NOVALUE;
    int _13493 = NOVALUE;
    int _13490 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(e_p) then*/
    _13490 = 1;
    if (_13490 == 0)
    {
        _13490 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _13490 = NOVALUE;
    }

    /** 		ee = {e_p}*/
    _0 = _ee_23035;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _e_p_23034;
    _ee_23035 = MAKE_SEQ(_1);
    DeRefi(_0);
    goto L2; // [15] 24
L1: 

    /** 		ee = e_p*/
    DeRefi(_ee_23035);
    _ee_23035 = _e_p_23034;
L2: 

    /** 	if fetestexcept = -1 then*/
    if (binary_op_a(NOTEQ, _64fetestexcept_22928, -1)){
        goto L3; // [28] 39
    }

    /** 		return 0*/
    DeRefi(_ee_23035);
    DeRef(_ans_23046);
    return 0;
L3: 

    /** 	integer ce = c_func(fetestexcept, {e_to_c(ee)})*/
    Ref(_ee_23035);
    _13493 = _64e_to_c(_ee_23035);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _13493;
    _13494 = MAKE_SEQ(_1);
    _13493 = NOVALUE;
    _ce_23042 = call_c(1, _64fetestexcept_22928, _13494);
    DeRefDS(_13494);
    _13494 = NOVALUE;
    if (!IS_ATOM_INT(_ce_23042)) {
        _1 = (long)(DBL_PTR(_ce_23042)->dbl);
        if (UNIQUE(DBL_PTR(_ce_23042)) && (DBL_PTR(_ce_23042)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ce_23042);
        _ce_23042 = _1;
    }

    /** 	sequence ans = c_to_e(ce)*/
    _0 = _ans_23046;
    _ans_23046 = _64c_to_e(_ce_23042);
    DeRef(_0);

    /** 	if integer(e_p) then*/
    _13497 = 1;
    if (_13497 == 0)
    {
        _13497 = NOVALUE;
        goto L4; // [73] 83
    }
    else{
        _13497 = NOVALUE;
    }

    /** 		return ce*/
    DeRefi(_ee_23035);
    DeRefDS(_ans_23046);
    return _ce_23042;
L4: 

    /** 	return ans*/
    DeRefi(_ee_23035);
    return _ans_23046;
    ;
}



// 0xBD044BD6
