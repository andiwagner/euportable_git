// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _45add_options(int _new_options_49936)
{
    int _0, _1, _2;
    

    /** 	options = splice(options, new_options, COMMON_OPTIONS_SPLICE_IDX)*/
    {
        s1_ptr assign_space;
        insert_pos = 15;
        if (insert_pos <= 0) {
            Concat(&_45options_49932,_new_options_49936,_45options_49932);
        }
        else if (insert_pos > SEQ_PTR(_45options_49932)->length){
            Concat(&_45options_49932,_45options_49932,_new_options_49936);
        }
        else if (IS_SEQUENCE(_new_options_49936)) {
            if( _45options_49932 != _45options_49932 || SEQ_PTR( _45options_49932 )->ref != 1 ){
                DeRef( _45options_49932 );
                RefDS( _45options_49932 );
            }
            assign_space = Add_internal_space( _45options_49932, insert_pos,((s1_ptr)SEQ_PTR(_new_options_49936))->length);
            assign_slice_seq = &assign_space;
            assign_space = Copy_elements( insert_pos, SEQ_PTR(_new_options_49936), _45options_49932 == _45options_49932 );
            _45options_49932 = MAKE_SEQ( assign_space );
        }
        else {
            if( _45options_49932 == _45options_49932 && SEQ_PTR( _45options_49932 )->ref == 1 ){
                _45options_49932 = Insert( _45options_49932, _new_options_49936, insert_pos);
            }
            else {
                DeRef( _45options_49932 );
                RefDS( _45options_49932 );
                _45options_49932 = Insert( _45options_49932, _new_options_49936, insert_pos);
            }
        }
    }

    /** end procedure*/
    DeRefDS(_new_options_49936);
    return;
    ;
}


int _45get_options()
{
    int _0, _1, _2;
    

    /** 	return options*/
    RefDS(_45options_49932);
    return _45options_49932;
    ;
}


int _45get_common_options()
{
    int _0, _1, _2;
    

    /** 	return COMMON_OPTIONS*/
    RefDS(_45COMMON_OPTIONS_49830);
    return _45COMMON_OPTIONS_49830;
    ;
}


int _45get_switches()
{
    int _0, _1, _2;
    

    /** 	return switches*/
    RefDS(_45switches_49829);
    return _45switches_49829;
    ;
}


void _45show_copyrights()
{
    int _notices_49946 = NOVALUE;
    int _26270 = NOVALUE;
    int _26269 = NOVALUE;
    int _26267 = NOVALUE;
    int _26266 = NOVALUE;
    int _26265 = NOVALUE;
    int _26264 = NOVALUE;
    int _26262 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence notices = all_copyrights()*/
    _0 = _notices_49946;
    _notices_49946 = _26all_copyrights();
    DeRef(_0);

    /** 	for i = 1 to length(notices) do*/
    if (IS_SEQUENCE(_notices_49946)){
            _26262 = SEQ_PTR(_notices_49946)->length;
    }
    else {
        _26262 = 1;
    }
    {
        int _i_49950;
        _i_49950 = 1;
L1: 
        if (_i_49950 > _26262){
            goto L2; // [13] 60
        }

        /** 		printf(2, "%s\n  %s\n\n", { notices[i][1], match_replace("\n", notices[i][2], "\n  ") })*/
        _2 = (int)SEQ_PTR(_notices_49946);
        _26264 = (int)*(((s1_ptr)_2)->base + _i_49950);
        _2 = (int)SEQ_PTR(_26264);
        _26265 = (int)*(((s1_ptr)_2)->base + 1);
        _26264 = NOVALUE;
        _2 = (int)SEQ_PTR(_notices_49946);
        _26266 = (int)*(((s1_ptr)_2)->base + _i_49950);
        _2 = (int)SEQ_PTR(_26266);
        _26267 = (int)*(((s1_ptr)_2)->base + 2);
        _26266 = NOVALUE;
        RefDS(_22815);
        Ref(_26267);
        RefDS(_26268);
        _26269 = _12match_replace(_22815, _26267, _26268, 0);
        _26267 = NOVALUE;
        Ref(_26265);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _26265;
        ((int *)_2)[2] = _26269;
        _26270 = MAKE_SEQ(_1);
        _26269 = NOVALUE;
        _26265 = NOVALUE;
        EPrintf(2, _26263, _26270);
        DeRefDS(_26270);
        _26270 = NOVALUE;

        /** 	end for*/
        _i_49950 = _i_49950 + 1;
        goto L1; // [55] 20
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_notices_49946);
    return;
    ;
}


void _45show_banner()
{
    int _version_type_inlined_version_type_at_212_50013 = NOVALUE;
    int _version_string_short_1__tmp_at192_50011 = NOVALUE;
    int _version_string_short_inlined_version_string_short_at_192_50010 = NOVALUE;
    int _version_revision_inlined_version_revision_at_121_49992 = NOVALUE;
    int _platform_name_inlined_platform_name_at_83_49984 = NOVALUE;
    int _prod_name_49963 = NOVALUE;
    int _memory_type_49964 = NOVALUE;
    int _misc_info_49982 = NOVALUE;
    int _EuConsole_49996 = NOVALUE;
    int _26294 = NOVALUE;
    int _26293 = NOVALUE;
    int _26292 = NOVALUE;
    int _26289 = NOVALUE;
    int _26288 = NOVALUE;
    int _26284 = NOVALUE;
    int _26283 = NOVALUE;
    int _26282 = NOVALUE;
    int _26280 = NOVALUE;
    int _26278 = NOVALUE;
    int _26277 = NOVALUE;
    int _26272 = NOVALUE;
    int _26271 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if INTERPRET and not BIND then*/
    if (_38INTERPRET_16561 == 0) {
        goto L1; // [5] 31
    }
    _26272 = (_38BIND_16567 == 0);
    if (_26272 == 0)
    {
        DeRef(_26272);
        _26272 = NOVALUE;
        goto L1; // [15] 31
    }
    else{
        DeRef(_26272);
        _26272 = NOVALUE;
    }

    /** 		prod_name = GetMsgText(270,0)*/
    RefDS(_22663);
    _0 = _prod_name_49963;
    _prod_name_49963 = _47GetMsgText(270, 0, _22663);
    DeRef(_0);
    goto L2; // [28] 70
L1: 

    /** 	elsif TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L3; // [35] 51
    }
    else{
    }

    /** 		prod_name = GetMsgText(271,0)*/
    RefDS(_22663);
    _0 = _prod_name_49963;
    _prod_name_49963 = _47GetMsgText(271, 0, _22663);
    DeRef(_0);
    goto L2; // [48] 70
L3: 

    /** 	elsif BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L4; // [55] 69
    }
    else{
    }

    /** 		prod_name = GetMsgText(272,0)*/
    RefDS(_22663);
    _0 = _prod_name_49963;
    _prod_name_49963 = _47GetMsgText(272, 0, _22663);
    DeRef(_0);
L4: 
L2: 

    /** 	ifdef EU_MANAGED_MEM then*/

    /** 		memory_type = GetMsgText(274,0)*/
    RefDS(_22663);
    _0 = _memory_type_49964;
    _memory_type_49964 = _47GetMsgText(274, 0, _22663);
    DeRef(_0);

    /** 	sequence misc_info = {*/

    /** 	ifdef WINDOWS then*/

    /** 		return "Windows"*/
    RefDS(_6151);
    DeRefi(_platform_name_inlined_platform_name_at_83_49984);
    _platform_name_inlined_platform_name_at_83_49984 = _6151;
    _26277 = _26version_date(0);
    _26278 = _26version_node(0);
    _0 = _misc_info_49982;
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_platform_name_inlined_platform_name_at_83_49984);
    *((int *)(_2+4)) = _platform_name_inlined_platform_name_at_83_49984;
    RefDS(_memory_type_49964);
    *((int *)(_2+8)) = _memory_type_49964;
    RefDS(_22663);
    *((int *)(_2+12)) = _22663;
    *((int *)(_2+16)) = _26277;
    *((int *)(_2+20)) = _26278;
    _misc_info_49982 = MAKE_SEQ(_1);
    DeRef(_0);
    _26278 = NOVALUE;
    _26277 = NOVALUE;

    /** 	if info:is_developmental then*/
    if (_26is_developmental_10914 == 0)
    {
        goto L5; // [114] 150
    }
    else{
    }

    /** 		misc_info[$] = sprintf("%d:%s", { info:version_revision(), info:version_node() })*/
    _26280 = 5;

    /** 	return version_info[REVISION]*/
    DeRef(_version_revision_inlined_version_revision_at_121_49992);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _version_revision_inlined_version_revision_at_121_49992 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_version_revision_inlined_version_revision_at_121_49992);
    _26282 = _26version_node(0);
    Ref(_version_revision_inlined_version_revision_at_121_49992);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _version_revision_inlined_version_revision_at_121_49992;
    ((int *)_2)[2] = _26282;
    _26283 = MAKE_SEQ(_1);
    _26282 = NOVALUE;
    _26284 = EPrintf(-9999999, _26281, _26283);
    DeRefDS(_26283);
    _26283 = NOVALUE;
    _2 = (int)SEQ_PTR(_misc_info_49982);
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _26284;
    if( _1 != _26284 ){
        DeRef(_1);
    }
    _26284 = NOVALUE;
L5: 

    /** 	object EuConsole = getenv("EUCONS")*/
    DeRefi(_EuConsole_49996);
    _EuConsole_49996 = EGetEnv(_26285);

    /** 	if equal(EuConsole, "1") then*/
    if (_EuConsole_49996 == _26287)
    _26288 = 1;
    else if (IS_ATOM_INT(_EuConsole_49996) && IS_ATOM_INT(_26287))
    _26288 = 0;
    else
    _26288 = (compare(_EuConsole_49996, _26287) == 0);
    if (_26288 == 0)
    {
        _26288 = NOVALUE;
        goto L6; // [161] 179
    }
    else{
        _26288 = NOVALUE;
    }

    /** 		misc_info[3] = GetMsgText(275,0)*/
    RefDS(_22663);
    _26289 = _47GetMsgText(275, 0, _22663);
    _2 = (int)SEQ_PTR(_misc_info_49982);
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _26289;
    if( _1 != _26289 ){
        DeRef(_1);
    }
    _26289 = NOVALUE;
    goto L7; // [176] 187
L6: 

    /** 		misc_info = remove(misc_info, 3)*/
    {
        s1_ptr assign_space = SEQ_PTR(_misc_info_49982);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(3)) ? 3 : (long)(DBL_PTR(3)->dbl);
        int stop = (IS_ATOM_INT(3)) ? 3 : (long)(DBL_PTR(3)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_misc_info_49982), start, &_misc_info_49982 );
            }
            else Tail(SEQ_PTR(_misc_info_49982), stop+1, &_misc_info_49982);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_misc_info_49982), start, &_misc_info_49982);
        }
        else {
            assign_slice_seq = &assign_space;
            _misc_info_49982 = Remove_elements(start, stop, (SEQ_PTR(_misc_info_49982)->ref == 1));
        }
    }
L7: 

    /** 	screen_output(STDERR, sprintf("%s v%s %s\n   %s, %s\n   Revision Date: %s, Id: %s\n", {*/

    /** 	return sprintf("%d.%d.%d", version_info[MAJ_VER..PAT_VER])*/
    rhs_slice_target = (object_ptr)&_version_string_short_1__tmp_at192_50011;
    RHS_Slice(_26version_info_10912, 1, 3);
    DeRefi(_version_string_short_inlined_version_string_short_at_192_50010);
    _version_string_short_inlined_version_string_short_at_192_50010 = EPrintf(-9999999, _6205, _version_string_short_1__tmp_at192_50011);
    DeRef(_version_string_short_1__tmp_at192_50011);
    _version_string_short_1__tmp_at192_50011 = NOVALUE;

    /** 	return version_info[VER_TYPE]*/
    DeRef(_version_type_inlined_version_type_at_212_50013);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _version_type_inlined_version_type_at_212_50013 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_version_type_inlined_version_type_at_212_50013);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_prod_name_49963);
    *((int *)(_2+4)) = _prod_name_49963;
    RefDS(_version_string_short_inlined_version_string_short_at_192_50010);
    *((int *)(_2+8)) = _version_string_short_inlined_version_string_short_at_192_50010;
    Ref(_version_type_inlined_version_type_at_212_50013);
    *((int *)(_2+12)) = _version_type_inlined_version_type_at_212_50013;
    _26292 = MAKE_SEQ(_1);
    Concat((object_ptr)&_26293, _26292, _misc_info_49982);
    DeRefDS(_26292);
    _26292 = NOVALUE;
    DeRef(_26292);
    _26292 = NOVALUE;
    _26294 = EPrintf(-9999999, _26291, _26293);
    DeRefDS(_26293);
    _26293 = NOVALUE;
    _46screen_output(2, _26294);
    _26294 = NOVALUE;

    /** end procedure*/
    DeRefDS(_prod_name_49963);
    DeRef(_memory_type_49964);
    DeRefDS(_misc_info_49982);
    DeRefi(_EuConsole_49996);
    return;
    ;
}


int _45find_opt(int _name_type_50025, int _opt_50026, int _opts_50027)
{
    int _o_50031 = NOVALUE;
    int _has_case_50033 = NOVALUE;
    int _26307 = NOVALUE;
    int _26306 = NOVALUE;
    int _26305 = NOVALUE;
    int _26304 = NOVALUE;
    int _26303 = NOVALUE;
    int _26302 = NOVALUE;
    int _26301 = NOVALUE;
    int _26300 = NOVALUE;
    int _26299 = NOVALUE;
    int _26297 = NOVALUE;
    int _26295 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(opts) do*/
    if (IS_SEQUENCE(_opts_50027)){
            _26295 = SEQ_PTR(_opts_50027)->length;
    }
    else {
        _26295 = 1;
    }
    {
        int _i_50029;
        _i_50029 = 1;
L1: 
        if (_i_50029 > _26295){
            goto L2; // [12] 115
        }

        /** 		sequence o = opts[i]		*/
        DeRef(_o_50031);
        _2 = (int)SEQ_PTR(_opts_50027);
        _o_50031 = (int)*(((s1_ptr)_2)->base + _i_50029);
        Ref(_o_50031);

        /** 		integer has_case = find(HAS_CASE, o[OPTIONS])*/
        _2 = (int)SEQ_PTR(_o_50031);
        _26297 = (int)*(((s1_ptr)_2)->base + 4);
        _has_case_50033 = find_from(99, _26297, 1);
        _26297 = NOVALUE;

        /** 		if has_case and equal(o[name_type], opt) then*/
        if (_has_case_50033 == 0) {
            goto L3; // [44] 69
        }
        _2 = (int)SEQ_PTR(_o_50031);
        _26300 = (int)*(((s1_ptr)_2)->base + _name_type_50025);
        if (_26300 == _opt_50026)
        _26301 = 1;
        else if (IS_ATOM_INT(_26300) && IS_ATOM_INT(_opt_50026))
        _26301 = 0;
        else
        _26301 = (compare(_26300, _opt_50026) == 0);
        _26300 = NOVALUE;
        if (_26301 == 0)
        {
            _26301 = NOVALUE;
            goto L3; // [57] 69
        }
        else{
            _26301 = NOVALUE;
        }

        /** 			return o*/
        DeRefDS(_opt_50026);
        DeRefDS(_opts_50027);
        return _o_50031;
        goto L4; // [66] 106
L3: 

        /** 		elsif not has_case and equal(text:lower(o[name_type]), text:lower(opt)) then*/
        _26302 = (_has_case_50033 == 0);
        if (_26302 == 0) {
            goto L5; // [74] 105
        }
        _2 = (int)SEQ_PTR(_o_50031);
        _26304 = (int)*(((s1_ptr)_2)->base + _name_type_50025);
        Ref(_26304);
        _26305 = _10lower(_26304);
        _26304 = NOVALUE;
        RefDS(_opt_50026);
        _26306 = _10lower(_opt_50026);
        if (_26305 == _26306)
        _26307 = 1;
        else if (IS_ATOM_INT(_26305) && IS_ATOM_INT(_26306))
        _26307 = 0;
        else
        _26307 = (compare(_26305, _26306) == 0);
        DeRef(_26305);
        _26305 = NOVALUE;
        DeRef(_26306);
        _26306 = NOVALUE;
        if (_26307 == 0)
        {
            _26307 = NOVALUE;
            goto L5; // [95] 105
        }
        else{
            _26307 = NOVALUE;
        }

        /** 			return o*/
        DeRefDS(_opt_50026);
        DeRefDS(_opts_50027);
        DeRef(_26302);
        _26302 = NOVALUE;
        return _o_50031;
L5: 
L4: 
        DeRef(_o_50031);
        _o_50031 = NOVALUE;

        /** 	end for*/
        _i_50029 = _i_50029 + 1;
        goto L1; // [110] 19
L2: 
        ;
    }

    /** 	return {}*/
    RefDS(_22663);
    DeRefDS(_opt_50026);
    DeRefDS(_opts_50027);
    DeRef(_26302);
    _26302 = NOVALUE;
    return _22663;
    ;
}


int _45merge_parameters(int _a_50050, int _b_50051, int _opts_50052, int _dedupe_50053)
{
    int _i_50054 = NOVALUE;
    int _opt_50058 = NOVALUE;
    int _this_opt_50064 = NOVALUE;
    int _bi_50065 = NOVALUE;
    int _beginLen_50125 = NOVALUE;
    int _first_extra_50147 = NOVALUE;
    int _opt_50151 = NOVALUE;
    int _this_opt_50156 = NOVALUE;
    int _26401 = NOVALUE;
    int _26400 = NOVALUE;
    int _26397 = NOVALUE;
    int _26396 = NOVALUE;
    int _26395 = NOVALUE;
    int _26393 = NOVALUE;
    int _26392 = NOVALUE;
    int _26391 = NOVALUE;
    int _26390 = NOVALUE;
    int _26388 = NOVALUE;
    int _26387 = NOVALUE;
    int _26385 = NOVALUE;
    int _26384 = NOVALUE;
    int _26383 = NOVALUE;
    int _26382 = NOVALUE;
    int _26381 = NOVALUE;
    int _26380 = NOVALUE;
    int _26379 = NOVALUE;
    int _26377 = NOVALUE;
    int _26374 = NOVALUE;
    int _26373 = NOVALUE;
    int _26368 = NOVALUE;
    int _26366 = NOVALUE;
    int _26365 = NOVALUE;
    int _26364 = NOVALUE;
    int _26363 = NOVALUE;
    int _26362 = NOVALUE;
    int _26361 = NOVALUE;
    int _26360 = NOVALUE;
    int _26359 = NOVALUE;
    int _26355 = NOVALUE;
    int _26354 = NOVALUE;
    int _26353 = NOVALUE;
    int _26352 = NOVALUE;
    int _26351 = NOVALUE;
    int _26350 = NOVALUE;
    int _26349 = NOVALUE;
    int _26348 = NOVALUE;
    int _26347 = NOVALUE;
    int _26346 = NOVALUE;
    int _26345 = NOVALUE;
    int _26344 = NOVALUE;
    int _26343 = NOVALUE;
    int _26342 = NOVALUE;
    int _26341 = NOVALUE;
    int _26339 = NOVALUE;
    int _26338 = NOVALUE;
    int _26337 = NOVALUE;
    int _26336 = NOVALUE;
    int _26335 = NOVALUE;
    int _26334 = NOVALUE;
    int _26333 = NOVALUE;
    int _26332 = NOVALUE;
    int _26330 = NOVALUE;
    int _26329 = NOVALUE;
    int _26328 = NOVALUE;
    int _26327 = NOVALUE;
    int _26325 = NOVALUE;
    int _26324 = NOVALUE;
    int _26323 = NOVALUE;
    int _26322 = NOVALUE;
    int _26321 = NOVALUE;
    int _26320 = NOVALUE;
    int _26319 = NOVALUE;
    int _26317 = NOVALUE;
    int _26316 = NOVALUE;
    int _26314 = NOVALUE;
    int _26311 = NOVALUE;
    int _26308 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_dedupe_50053)) {
        _1 = (long)(DBL_PTR(_dedupe_50053)->dbl);
        if (UNIQUE(DBL_PTR(_dedupe_50053)) && (DBL_PTR(_dedupe_50053)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_dedupe_50053);
        _dedupe_50053 = _1;
    }

    /** 	integer i = 1*/
    _i_50054 = 1;

    /** 	while i <= length(a) do*/
L1: 
    if (IS_SEQUENCE(_a_50050)){
            _26308 = SEQ_PTR(_a_50050)->length;
    }
    else {
        _26308 = 1;
    }
    if (_i_50054 > _26308)
    goto L2; // [22] 473

    /** 		sequence opt = a[i]*/
    DeRef(_opt_50058);
    _2 = (int)SEQ_PTR(_a_50050);
    _opt_50058 = (int)*(((s1_ptr)_2)->base + _i_50054);
    Ref(_opt_50058);

    /** 		if length(opt) < 2 then*/
    if (IS_SEQUENCE(_opt_50058)){
            _26311 = SEQ_PTR(_opt_50058)->length;
    }
    else {
        _26311 = 1;
    }
    if (_26311 >= 2)
    goto L3; // [39] 56

    /** 			i += 1*/
    _i_50054 = _i_50054 + 1;

    /** 			continue*/
    DeRefDS(_opt_50058);
    _opt_50058 = NOVALUE;
    DeRef(_this_opt_50064);
    _this_opt_50064 = NOVALUE;
    goto L1; // [53] 19
L3: 

    /** 		sequence this_opt = {}*/
    RefDS(_22663);
    DeRef(_this_opt_50064);
    _this_opt_50064 = _22663;

    /** 		integer bi = 0*/
    _bi_50065 = 0;

    /** 		if opt[2] = '-' then*/
    _2 = (int)SEQ_PTR(_opt_50058);
    _26314 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _26314, 45)){
        _26314 = NOVALUE;
        goto L4; // [74] 151
    }
    _26314 = NOVALUE;

    /** 			this_opt = find_opt(LONGNAME, opt[3..$], opts)*/
    if (IS_SEQUENCE(_opt_50058)){
            _26316 = SEQ_PTR(_opt_50058)->length;
    }
    else {
        _26316 = 1;
    }
    rhs_slice_target = (object_ptr)&_26317;
    RHS_Slice(_opt_50058, 3, _26316);
    RefDS(_opts_50052);
    _0 = _this_opt_50064;
    _this_opt_50064 = _45find_opt(2, _26317, _opts_50052);
    DeRefDS(_0);
    _26317 = NOVALUE;

    /** 			for j = 1 to length(b) do*/
    if (IS_SEQUENCE(_b_50051)){
            _26319 = SEQ_PTR(_b_50051)->length;
    }
    else {
        _26319 = 1;
    }
    {
        int _j_50073;
        _j_50073 = 1;
L5: 
        if (_j_50073 > _26319){
            goto L6; // [103] 148
        }

        /** 				if equal(text:lower(b[j]), text:lower(opt)) then*/
        _2 = (int)SEQ_PTR(_b_50051);
        _26320 = (int)*(((s1_ptr)_2)->base + _j_50073);
        Ref(_26320);
        _26321 = _10lower(_26320);
        _26320 = NOVALUE;
        RefDS(_opt_50058);
        _26322 = _10lower(_opt_50058);
        if (_26321 == _26322)
        _26323 = 1;
        else if (IS_ATOM_INT(_26321) && IS_ATOM_INT(_26322))
        _26323 = 0;
        else
        _26323 = (compare(_26321, _26322) == 0);
        DeRef(_26321);
        _26321 = NOVALUE;
        DeRef(_26322);
        _26322 = NOVALUE;
        if (_26323 == 0)
        {
            _26323 = NOVALUE;
            goto L7; // [128] 141
        }
        else{
            _26323 = NOVALUE;
        }

        /** 					bi = j*/
        _bi_50065 = _j_50073;

        /** 					exit*/
        goto L6; // [138] 148
L7: 

        /** 			end for*/
        _j_50073 = _j_50073 + 1;
        goto L5; // [143] 110
L6: 
        ;
    }
    goto L8; // [148] 296
L4: 

    /** 		elsif opt[1] = '-' or opt[1] = '/' then*/
    _2 = (int)SEQ_PTR(_opt_50058);
    _26324 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26324)) {
        _26325 = (_26324 == 45);
    }
    else {
        _26325 = binary_op(EQUALS, _26324, 45);
    }
    _26324 = NOVALUE;
    if (IS_ATOM_INT(_26325)) {
        if (_26325 != 0) {
            goto L9; // [161] 178
        }
    }
    else {
        if (DBL_PTR(_26325)->dbl != 0.0) {
            goto L9; // [161] 178
        }
    }
    _2 = (int)SEQ_PTR(_opt_50058);
    _26327 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26327)) {
        _26328 = (_26327 == 47);
    }
    else {
        _26328 = binary_op(EQUALS, _26327, 47);
    }
    _26327 = NOVALUE;
    if (_26328 == 0) {
        DeRef(_26328);
        _26328 = NOVALUE;
        goto LA; // [174] 295
    }
    else {
        if (!IS_ATOM_INT(_26328) && DBL_PTR(_26328)->dbl == 0.0){
            DeRef(_26328);
            _26328 = NOVALUE;
            goto LA; // [174] 295
        }
        DeRef(_26328);
        _26328 = NOVALUE;
    }
    DeRef(_26328);
    _26328 = NOVALUE;
L9: 

    /** 			this_opt = find_opt(SHORTNAME, opt[2..$], opts)*/
    if (IS_SEQUENCE(_opt_50058)){
            _26329 = SEQ_PTR(_opt_50058)->length;
    }
    else {
        _26329 = 1;
    }
    rhs_slice_target = (object_ptr)&_26330;
    RHS_Slice(_opt_50058, 2, _26329);
    RefDS(_opts_50052);
    _0 = _this_opt_50064;
    _this_opt_50064 = _45find_opt(1, _26330, _opts_50052);
    DeRef(_0);
    _26330 = NOVALUE;

    /** 			for j = 1 to length(b) do*/
    if (IS_SEQUENCE(_b_50051)){
            _26332 = SEQ_PTR(_b_50051)->length;
    }
    else {
        _26332 = 1;
    }
    {
        int _j_50090;
        _j_50090 = 1;
LB: 
        if (_j_50090 > _26332){
            goto LC; // [203] 294
        }

        /** 				if equal(text:lower(b[j]), '-' & text:lower(opt[2..$])) or */
        _2 = (int)SEQ_PTR(_b_50051);
        _26333 = (int)*(((s1_ptr)_2)->base + _j_50090);
        Ref(_26333);
        _26334 = _10lower(_26333);
        _26333 = NOVALUE;
        if (IS_SEQUENCE(_opt_50058)){
                _26335 = SEQ_PTR(_opt_50058)->length;
        }
        else {
            _26335 = 1;
        }
        rhs_slice_target = (object_ptr)&_26336;
        RHS_Slice(_opt_50058, 2, _26335);
        _26337 = _10lower(_26336);
        _26336 = NOVALUE;
        if (IS_SEQUENCE(45) && IS_ATOM(_26337)) {
        }
        else if (IS_ATOM(45) && IS_SEQUENCE(_26337)) {
            Prepend(&_26338, _26337, 45);
        }
        else {
            Concat((object_ptr)&_26338, 45, _26337);
        }
        DeRef(_26337);
        _26337 = NOVALUE;
        if (_26334 == _26338)
        _26339 = 1;
        else if (IS_ATOM_INT(_26334) && IS_ATOM_INT(_26338))
        _26339 = 0;
        else
        _26339 = (compare(_26334, _26338) == 0);
        DeRef(_26334);
        _26334 = NOVALUE;
        DeRefDS(_26338);
        _26338 = NOVALUE;
        if (_26339 != 0) {
            goto LD; // [240] 277
        }
        _2 = (int)SEQ_PTR(_b_50051);
        _26341 = (int)*(((s1_ptr)_2)->base + _j_50090);
        Ref(_26341);
        _26342 = _10lower(_26341);
        _26341 = NOVALUE;
        if (IS_SEQUENCE(_opt_50058)){
                _26343 = SEQ_PTR(_opt_50058)->length;
        }
        else {
            _26343 = 1;
        }
        rhs_slice_target = (object_ptr)&_26344;
        RHS_Slice(_opt_50058, 2, _26343);
        _26345 = _10lower(_26344);
        _26344 = NOVALUE;
        if (IS_SEQUENCE(47) && IS_ATOM(_26345)) {
        }
        else if (IS_ATOM(47) && IS_SEQUENCE(_26345)) {
            Prepend(&_26346, _26345, 47);
        }
        else {
            Concat((object_ptr)&_26346, 47, _26345);
        }
        DeRef(_26345);
        _26345 = NOVALUE;
        if (_26342 == _26346)
        _26347 = 1;
        else if (IS_ATOM_INT(_26342) && IS_ATOM_INT(_26346))
        _26347 = 0;
        else
        _26347 = (compare(_26342, _26346) == 0);
        DeRef(_26342);
        _26342 = NOVALUE;
        DeRefDS(_26346);
        _26346 = NOVALUE;
        if (_26347 == 0)
        {
            _26347 = NOVALUE;
            goto LE; // [273] 287
        }
        else{
            _26347 = NOVALUE;
        }
LD: 

        /** 					bi = j*/
        _bi_50065 = _j_50090;

        /** 					exit*/
        goto LC; // [284] 294
LE: 

        /** 			end for*/
        _j_50090 = _j_50090 + 1;
        goto LB; // [289] 210
LC: 
        ;
    }
LA: 
L8: 

    /** 		if length(this_opt) and not find(MULTIPLE, this_opt[OPTIONS]) then*/
    if (IS_SEQUENCE(_this_opt_50064)){
            _26348 = SEQ_PTR(_this_opt_50064)->length;
    }
    else {
        _26348 = 1;
    }
    if (_26348 == 0) {
        goto LF; // [301] 459
    }
    _2 = (int)SEQ_PTR(_this_opt_50064);
    _26350 = (int)*(((s1_ptr)_2)->base + 4);
    _26351 = find_from(42, _26350, 1);
    _26350 = NOVALUE;
    _26352 = (_26351 == 0);
    _26351 = NOVALUE;
    if (_26352 == 0)
    {
        DeRef(_26352);
        _26352 = NOVALUE;
        goto LF; // [322] 459
    }
    else{
        DeRef(_26352);
        _26352 = NOVALUE;
    }

    /** 			if bi then*/
    if (_bi_50065 == 0)
    {
        goto L10; // [327] 373
    }
    else{
    }

    /** 				if find(HAS_PARAMETER, this_opt[OPTIONS]) then*/
    _2 = (int)SEQ_PTR(_this_opt_50064);
    _26353 = (int)*(((s1_ptr)_2)->base + 4);
    _26354 = find_from(112, _26353, 1);
    _26353 = NOVALUE;
    if (_26354 == 0)
    {
        _26354 = NOVALUE;
        goto L11; // [345] 362
    }
    else{
        _26354 = NOVALUE;
    }

    /** 					a = remove(a, i, i + 1)*/
    _26355 = _i_50054 + 1;
    if (_26355 > MAXINT){
        _26355 = NewDouble((double)_26355);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_a_50050);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_i_50054)) ? _i_50054 : (long)(DBL_PTR(_i_50054)->dbl);
        int stop = (IS_ATOM_INT(_26355)) ? _26355 : (long)(DBL_PTR(_26355)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_a_50050), start, &_a_50050 );
            }
            else Tail(SEQ_PTR(_a_50050), stop+1, &_a_50050);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_a_50050), start, &_a_50050);
        }
        else {
            assign_slice_seq = &assign_space;
            _a_50050 = Remove_elements(start, stop, (SEQ_PTR(_a_50050)->ref == 1));
        }
    }
    DeRef(_26355);
    _26355 = NOVALUE;
    goto L12; // [359] 466
L11: 

    /** 					a = remove(a, i)*/
    {
        s1_ptr assign_space = SEQ_PTR(_a_50050);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_i_50054)) ? _i_50054 : (long)(DBL_PTR(_i_50054)->dbl);
        int stop = (IS_ATOM_INT(_i_50054)) ? _i_50054 : (long)(DBL_PTR(_i_50054)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_a_50050), start, &_a_50050 );
            }
            else Tail(SEQ_PTR(_a_50050), stop+1, &_a_50050);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_a_50050), start, &_a_50050);
        }
        else {
            assign_slice_seq = &assign_space;
            _a_50050 = Remove_elements(start, stop, (SEQ_PTR(_a_50050)->ref == 1));
        }
    }
    goto L12; // [370] 466
L10: 

    /** 				integer beginLen = length(a)*/
    if (IS_SEQUENCE(_a_50050)){
            _beginLen_50125 = SEQ_PTR(_a_50050)->length;
    }
    else {
        _beginLen_50125 = 1;
    }

    /** 				if dedupe = 0 and i < beginLen then*/
    _26359 = (_dedupe_50053 == 0);
    if (_26359 == 0) {
        goto L13; // [384] 446
    }
    _26361 = (_i_50054 < _beginLen_50125);
    if (_26361 == 0)
    {
        DeRef(_26361);
        _26361 = NOVALUE;
        goto L13; // [393] 446
    }
    else{
        DeRef(_26361);
        _26361 = NOVALUE;
    }

    /** 					a = merge_parameters( a[i + 1..$], a[1..i], opts, 1)*/
    _26362 = _i_50054 + 1;
    if (_26362 > MAXINT){
        _26362 = NewDouble((double)_26362);
    }
    if (IS_SEQUENCE(_a_50050)){
            _26363 = SEQ_PTR(_a_50050)->length;
    }
    else {
        _26363 = 1;
    }
    rhs_slice_target = (object_ptr)&_26364;
    RHS_Slice(_a_50050, _26362, _26363);
    rhs_slice_target = (object_ptr)&_26365;
    RHS_Slice(_a_50050, 1, _i_50054);
    RefDS(_opts_50052);
    DeRef(_26366);
    _26366 = _opts_50052;
    _0 = _a_50050;
    _a_50050 = _45merge_parameters(_26364, _26365, _26366, 1);
    DeRefDS(_0);
    _26364 = NOVALUE;
    _26365 = NOVALUE;
    _26366 = NOVALUE;

    /** 					if beginLen = length(a) then*/
    if (IS_SEQUENCE(_a_50050)){
            _26368 = SEQ_PTR(_a_50050)->length;
    }
    else {
        _26368 = 1;
    }
    if (_beginLen_50125 != _26368)
    goto L14; // [432] 453

    /** 						i += 1*/
    _i_50054 = _i_50054 + 1;
    goto L14; // [443] 453
L13: 

    /** 					i += 1*/
    _i_50054 = _i_50054 + 1;
L14: 
    goto L12; // [456] 466
LF: 

    /** 			i += 1*/
    _i_50054 = _i_50054 + 1;
L12: 
    DeRef(_opt_50058);
    _opt_50058 = NOVALUE;
    DeRef(_this_opt_50064);
    _this_opt_50064 = NOVALUE;

    /** 	end while*/
    goto L1; // [470] 19
L2: 

    /** 	if dedupe then*/
    if (_dedupe_50053 == 0)
    {
        goto L15; // [475] 489
    }
    else{
    }

    /** 		return b & a*/
    Concat((object_ptr)&_26373, _b_50051, _a_50050);
    DeRefDS(_a_50050);
    DeRefDS(_b_50051);
    DeRefDS(_opts_50052);
    DeRef(_26359);
    _26359 = NOVALUE;
    DeRef(_26325);
    _26325 = NOVALUE;
    DeRef(_26362);
    _26362 = NOVALUE;
    return _26373;
L15: 

    /** 	integer first_extra = 0*/
    _first_extra_50147 = 0;

    /** 	i = 1*/
    _i_50054 = 1;

    /** 	while i <= length(b) do*/
L16: 
    if (IS_SEQUENCE(_b_50051)){
            _26374 = SEQ_PTR(_b_50051)->length;
    }
    else {
        _26374 = 1;
    }
    if (_i_50054 > _26374)
    goto L17; // [507] 706

    /** 		sequence opt = b[i]*/
    DeRef(_opt_50151);
    _2 = (int)SEQ_PTR(_b_50051);
    _opt_50151 = (int)*(((s1_ptr)_2)->base + _i_50054);
    Ref(_opt_50151);

    /** 		if length(opt) <= 1 then*/
    if (IS_SEQUENCE(_opt_50151)){
            _26377 = SEQ_PTR(_opt_50151)->length;
    }
    else {
        _26377 = 1;
    }
    if (_26377 > 1)
    goto L18; // [524] 540

    /** 			first_extra = i*/
    _first_extra_50147 = _i_50054;

    /** 			exit*/
    DeRefDS(_opt_50151);
    _opt_50151 = NOVALUE;
    DeRef(_this_opt_50156);
    _this_opt_50156 = NOVALUE;
    goto L17; // [537] 706
L18: 

    /** 		sequence this_opt = {}*/
    RefDS(_22663);
    DeRef(_this_opt_50156);
    _this_opt_50156 = _22663;

    /** 		if opt[2] = '-' and opt[1] = '-' then*/
    _2 = (int)SEQ_PTR(_opt_50151);
    _26379 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_26379)) {
        _26380 = (_26379 == 45);
    }
    else {
        _26380 = binary_op(EQUALS, _26379, 45);
    }
    _26379 = NOVALUE;
    if (IS_ATOM_INT(_26380)) {
        if (_26380 == 0) {
            goto L19; // [557] 596
        }
    }
    else {
        if (DBL_PTR(_26380)->dbl == 0.0) {
            goto L19; // [557] 596
        }
    }
    _2 = (int)SEQ_PTR(_opt_50151);
    _26382 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26382)) {
        _26383 = (_26382 == 45);
    }
    else {
        _26383 = binary_op(EQUALS, _26382, 45);
    }
    _26382 = NOVALUE;
    if (_26383 == 0) {
        DeRef(_26383);
        _26383 = NOVALUE;
        goto L19; // [570] 596
    }
    else {
        if (!IS_ATOM_INT(_26383) && DBL_PTR(_26383)->dbl == 0.0){
            DeRef(_26383);
            _26383 = NOVALUE;
            goto L19; // [570] 596
        }
        DeRef(_26383);
        _26383 = NOVALUE;
    }
    DeRef(_26383);
    _26383 = NOVALUE;

    /** 			this_opt = find_opt(LONGNAME, opt[3..$], opts)*/
    if (IS_SEQUENCE(_opt_50151)){
            _26384 = SEQ_PTR(_opt_50151)->length;
    }
    else {
        _26384 = 1;
    }
    rhs_slice_target = (object_ptr)&_26385;
    RHS_Slice(_opt_50151, 3, _26384);
    RefDS(_opts_50052);
    _0 = _this_opt_50156;
    _this_opt_50156 = _45find_opt(2, _26385, _opts_50052);
    DeRef(_0);
    _26385 = NOVALUE;
    goto L1A; // [593] 645
L19: 

    /** 		elsif opt[1] = '-' or opt[1] = '/' then*/
    _2 = (int)SEQ_PTR(_opt_50151);
    _26387 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26387)) {
        _26388 = (_26387 == 45);
    }
    else {
        _26388 = binary_op(EQUALS, _26387, 45);
    }
    _26387 = NOVALUE;
    if (IS_ATOM_INT(_26388)) {
        if (_26388 != 0) {
            goto L1B; // [606] 623
        }
    }
    else {
        if (DBL_PTR(_26388)->dbl != 0.0) {
            goto L1B; // [606] 623
        }
    }
    _2 = (int)SEQ_PTR(_opt_50151);
    _26390 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_26390)) {
        _26391 = (_26390 == 47);
    }
    else {
        _26391 = binary_op(EQUALS, _26390, 47);
    }
    _26390 = NOVALUE;
    if (_26391 == 0) {
        DeRef(_26391);
        _26391 = NOVALUE;
        goto L1C; // [619] 644
    }
    else {
        if (!IS_ATOM_INT(_26391) && DBL_PTR(_26391)->dbl == 0.0){
            DeRef(_26391);
            _26391 = NOVALUE;
            goto L1C; // [619] 644
        }
        DeRef(_26391);
        _26391 = NOVALUE;
    }
    DeRef(_26391);
    _26391 = NOVALUE;
L1B: 

    /** 			this_opt = find_opt(SHORTNAME, opt[2..$], opts)*/
    if (IS_SEQUENCE(_opt_50151)){
            _26392 = SEQ_PTR(_opt_50151)->length;
    }
    else {
        _26392 = 1;
    }
    rhs_slice_target = (object_ptr)&_26393;
    RHS_Slice(_opt_50151, 2, _26392);
    RefDS(_opts_50052);
    _0 = _this_opt_50156;
    _this_opt_50156 = _45find_opt(1, _26393, _opts_50052);
    DeRef(_0);
    _26393 = NOVALUE;
L1C: 
L1A: 

    /** 		if length(this_opt) then*/
    if (IS_SEQUENCE(_this_opt_50156)){
            _26395 = SEQ_PTR(_this_opt_50156)->length;
    }
    else {
        _26395 = 1;
    }
    if (_26395 == 0)
    {
        _26395 = NOVALUE;
        goto L1D; // [650] 681
    }
    else{
        _26395 = NOVALUE;
    }

    /** 			if find(HAS_PARAMETER, this_opt[OPTIONS]) then*/
    _2 = (int)SEQ_PTR(_this_opt_50156);
    _26396 = (int)*(((s1_ptr)_2)->base + 4);
    _26397 = find_from(112, _26396, 1);
    _26396 = NOVALUE;
    if (_26397 == 0)
    {
        _26397 = NOVALUE;
        goto L1E; // [668] 693
    }
    else{
        _26397 = NOVALUE;
    }

    /** 				i += 1*/
    _i_50054 = _i_50054 + 1;
    goto L1E; // [678] 693
L1D: 

    /** 			first_extra = i*/
    _first_extra_50147 = _i_50054;

    /** 			exit*/
    DeRef(_opt_50151);
    _opt_50151 = NOVALUE;
    DeRef(_this_opt_50156);
    _this_opt_50156 = NOVALUE;
    goto L17; // [690] 706
L1E: 

    /** 		i += 1*/
    _i_50054 = _i_50054 + 1;
    DeRef(_opt_50151);
    _opt_50151 = NOVALUE;
    DeRef(_this_opt_50156);
    _this_opt_50156 = NOVALUE;

    /** 	end while*/
    goto L16; // [703] 504
L17: 

    /** 	if first_extra then*/
    if (_first_extra_50147 == 0)
    {
        goto L1F; // [708] 723
    }
    else{
    }

    /** 		return splice(b, a, first_extra)*/
    {
        s1_ptr assign_space;
        insert_pos = _first_extra_50147;
        if (insert_pos <= 0) {
            Concat(&_26400,_a_50050,_b_50051);
        }
        else if (insert_pos > SEQ_PTR(_b_50051)->length){
            Concat(&_26400,_b_50051,_a_50050);
        }
        else if (IS_SEQUENCE(_a_50050)) {
            if( _26400 != _b_50051 || SEQ_PTR( _b_50051 )->ref != 1 ){
                DeRef( _26400 );
                RefDS( _b_50051 );
            }
            assign_space = Add_internal_space( _b_50051, insert_pos,((s1_ptr)SEQ_PTR(_a_50050))->length);
            assign_slice_seq = &assign_space;
            assign_space = Copy_elements( insert_pos, SEQ_PTR(_a_50050), _b_50051 == _26400 );
            _26400 = MAKE_SEQ( assign_space );
        }
        else {
            if( _26400 == _b_50051 && SEQ_PTR( _b_50051 )->ref == 1 ){
                _26400 = Insert( _b_50051, _a_50050, insert_pos);
            }
            else {
                DeRef( _26400 );
                RefDS( _b_50051 );
                _26400 = Insert( _b_50051, _a_50050, insert_pos);
            }
        }
    }
    DeRefDS(_a_50050);
    DeRefDS(_b_50051);
    DeRefDS(_opts_50052);
    DeRef(_26359);
    _26359 = NOVALUE;
    DeRef(_26325);
    _26325 = NOVALUE;
    DeRef(_26362);
    _26362 = NOVALUE;
    DeRef(_26373);
    _26373 = NOVALUE;
    DeRef(_26380);
    _26380 = NOVALUE;
    DeRef(_26388);
    _26388 = NOVALUE;
    return _26400;
L1F: 

    /** 	return b & a*/
    Concat((object_ptr)&_26401, _b_50051, _a_50050);
    DeRefDS(_a_50050);
    DeRefDS(_b_50051);
    DeRefDS(_opts_50052);
    DeRef(_26359);
    _26359 = NOVALUE;
    DeRef(_26325);
    _26325 = NOVALUE;
    DeRef(_26362);
    _26362 = NOVALUE;
    DeRef(_26373);
    _26373 = NOVALUE;
    DeRef(_26400);
    _26400 = NOVALUE;
    DeRef(_26380);
    _26380 = NOVALUE;
    DeRef(_26388);
    _26388 = NOVALUE;
    return _26401;
    ;
}


int _45validate_opt(int _opt_type_50189, int _arg_50190, int _args_50191, int _ix_50192)
{
    int _opt_50193 = NOVALUE;
    int _this_opt_50201 = NOVALUE;
    int _26420 = NOVALUE;
    int _26419 = NOVALUE;
    int _26418 = NOVALUE;
    int _26417 = NOVALUE;
    int _26416 = NOVALUE;
    int _26414 = NOVALUE;
    int _26413 = NOVALUE;
    int _26412 = NOVALUE;
    int _26411 = NOVALUE;
    int _26410 = NOVALUE;
    int _26408 = NOVALUE;
    int _26405 = NOVALUE;
    int _26403 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if opt_type = SHORTNAME then*/
    if (_opt_type_50189 != 1)
    goto L1; // [13] 30

    /** 		opt = arg[2..$]*/
    if (IS_SEQUENCE(_arg_50190)){
            _26403 = SEQ_PTR(_arg_50190)->length;
    }
    else {
        _26403 = 1;
    }
    rhs_slice_target = (object_ptr)&_opt_50193;
    RHS_Slice(_arg_50190, 2, _26403);
    goto L2; // [27] 41
L1: 

    /** 		opt = arg[3..$]*/
    if (IS_SEQUENCE(_arg_50190)){
            _26405 = SEQ_PTR(_arg_50190)->length;
    }
    else {
        _26405 = 1;
    }
    rhs_slice_target = (object_ptr)&_opt_50193;
    RHS_Slice(_arg_50190, 3, _26405);
L2: 

    /** 	sequence this_opt = find_opt( opt_type, opt, options )*/
    RefDS(_opt_50193);
    RefDS(_45options_49932);
    _0 = _this_opt_50201;
    _this_opt_50201 = _45find_opt(_opt_type_50189, _opt_50193, _45options_49932);
    DeRef(_0);

    /** 	if not length( this_opt ) then*/
    if (IS_SEQUENCE(_this_opt_50201)){
            _26408 = SEQ_PTR(_this_opt_50201)->length;
    }
    else {
        _26408 = 1;
    }
    if (_26408 != 0)
    goto L3; // [60] 74
    _26408 = NOVALUE;

    /** 		return { 0, 0 }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _26410 = MAKE_SEQ(_1);
    DeRefDS(_arg_50190);
    DeRefDS(_args_50191);
    DeRefDS(_opt_50193);
    DeRefDS(_this_opt_50201);
    return _26410;
L3: 

    /** 	if find( HAS_PARAMETER, this_opt[OPTIONS] ) then*/
    _2 = (int)SEQ_PTR(_this_opt_50201);
    _26411 = (int)*(((s1_ptr)_2)->base + 4);
    _26412 = find_from(112, _26411, 1);
    _26411 = NOVALUE;
    if (_26412 == 0)
    {
        _26412 = NOVALUE;
        goto L4; // [89] 139
    }
    else{
        _26412 = NOVALUE;
    }

    /** 		if ix = length( args ) - 1 then*/
    if (IS_SEQUENCE(_args_50191)){
            _26413 = SEQ_PTR(_args_50191)->length;
    }
    else {
        _26413 = 1;
    }
    _26414 = _26413 - 1;
    _26413 = NOVALUE;
    if (_ix_50192 != _26414)
    goto L5; // [101] 121

    /** 			CompileErr( MISSING_CMD_PARAMETER, { arg } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_arg_50190);
    *((int *)(_2+4)) = _arg_50190;
    _26416 = MAKE_SEQ(_1);
    _46CompileErr(353, _26416, 0);
    _26416 = NOVALUE;
    goto L6; // [118] 154
L5: 

    /** 			return { ix, ix + 2 }*/
    _26417 = _ix_50192 + 2;
    if ((long)((unsigned long)_26417 + (unsigned long)HIGH_BITS) >= 0) 
    _26417 = NewDouble((double)_26417);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _ix_50192;
    ((int *)_2)[2] = _26417;
    _26418 = MAKE_SEQ(_1);
    _26417 = NOVALUE;
    DeRefDS(_arg_50190);
    DeRefDS(_args_50191);
    DeRef(_opt_50193);
    DeRef(_this_opt_50201);
    DeRef(_26410);
    _26410 = NOVALUE;
    DeRef(_26414);
    _26414 = NOVALUE;
    return _26418;
    goto L6; // [136] 154
L4: 

    /** 		return { ix, ix + 1 }*/
    _26419 = _ix_50192 + 1;
    if (_26419 > MAXINT){
        _26419 = NewDouble((double)_26419);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _ix_50192;
    ((int *)_2)[2] = _26419;
    _26420 = MAKE_SEQ(_1);
    _26419 = NOVALUE;
    DeRefDS(_arg_50190);
    DeRefDS(_args_50191);
    DeRef(_opt_50193);
    DeRef(_this_opt_50201);
    DeRef(_26410);
    _26410 = NOVALUE;
    DeRef(_26414);
    _26414 = NOVALUE;
    DeRef(_26418);
    _26418 = NOVALUE;
    return _26420;
L6: 
    ;
}


int _45find_next_opt(int _ix_50226, int _args_50227)
{
    int _arg_50231 = NOVALUE;
    int _26442 = NOVALUE;
    int _26441 = NOVALUE;
    int _26439 = NOVALUE;
    int _26438 = NOVALUE;
    int _26437 = NOVALUE;
    int _26436 = NOVALUE;
    int _26435 = NOVALUE;
    int _26434 = NOVALUE;
    int _26433 = NOVALUE;
    int _26432 = NOVALUE;
    int _26430 = NOVALUE;
    int _26428 = NOVALUE;
    int _26426 = NOVALUE;
    int _26424 = NOVALUE;
    int _26421 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while ix < length( args ) do*/
L1: 
    if (IS_SEQUENCE(_args_50227)){
            _26421 = SEQ_PTR(_args_50227)->length;
    }
    else {
        _26421 = 1;
    }
    if (_ix_50226 >= _26421)
    goto L2; // [13] 161

    /** 		sequence arg = args[ix]*/
    DeRef(_arg_50231);
    _2 = (int)SEQ_PTR(_args_50227);
    _arg_50231 = (int)*(((s1_ptr)_2)->base + _ix_50226);
    Ref(_arg_50231);

    /** 		if length( arg ) > 1 then*/
    if (IS_SEQUENCE(_arg_50231)){
            _26424 = SEQ_PTR(_arg_50231)->length;
    }
    else {
        _26424 = 1;
    }
    if (_26424 <= 1)
    goto L3; // [30] 133

    /** 			if arg[1] = '-' then*/
    _2 = (int)SEQ_PTR(_arg_50231);
    _26426 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _26426, 45)){
        _26426 = NOVALUE;
        goto L4; // [40] 115
    }
    _26426 = NOVALUE;

    /** 				if arg[2] = '-' then*/
    _2 = (int)SEQ_PTR(_arg_50231);
    _26428 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _26428, 45)){
        _26428 = NOVALUE;
        goto L5; // [50] 96
    }
    _26428 = NOVALUE;

    /** 					if length( arg ) = 2 then*/
    if (IS_SEQUENCE(_arg_50231)){
            _26430 = SEQ_PTR(_arg_50231)->length;
    }
    else {
        _26430 = 1;
    }
    if (_26430 != 2)
    goto L6; // [59] 78

    /** 						return { 0, ix - 1 }*/
    _26432 = _ix_50226 - 1;
    if ((long)((unsigned long)_26432 +(unsigned long) HIGH_BITS) >= 0){
        _26432 = NewDouble((double)_26432);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26432;
    _26433 = MAKE_SEQ(_1);
    _26432 = NOVALUE;
    DeRefDS(_arg_50231);
    DeRefDS(_args_50227);
    return _26433;
L6: 

    /** 					return validate_opt( LONGNAME, arg, args, ix )*/
    RefDS(_arg_50231);
    RefDS(_args_50227);
    _26434 = _45validate_opt(2, _arg_50231, _args_50227, _ix_50226);
    DeRefDS(_arg_50231);
    DeRefDS(_args_50227);
    DeRef(_26433);
    _26433 = NOVALUE;
    return _26434;
    goto L7; // [93] 148
L5: 

    /** 					return validate_opt( SHORTNAME, arg, args, ix )*/
    RefDS(_arg_50231);
    RefDS(_args_50227);
    _26435 = _45validate_opt(1, _arg_50231, _args_50227, _ix_50226);
    DeRefDS(_arg_50231);
    DeRefDS(_args_50227);
    DeRef(_26433);
    _26433 = NOVALUE;
    DeRef(_26434);
    _26434 = NOVALUE;
    return _26435;
    goto L7; // [112] 148
L4: 

    /** 				return {0, ix-1}*/
    _26436 = _ix_50226 - 1;
    if ((long)((unsigned long)_26436 +(unsigned long) HIGH_BITS) >= 0){
        _26436 = NewDouble((double)_26436);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26436;
    _26437 = MAKE_SEQ(_1);
    _26436 = NOVALUE;
    DeRef(_arg_50231);
    DeRefDS(_args_50227);
    DeRef(_26433);
    _26433 = NOVALUE;
    DeRef(_26434);
    _26434 = NOVALUE;
    DeRef(_26435);
    _26435 = NOVALUE;
    return _26437;
    goto L7; // [130] 148
L3: 

    /** 			return { 0, ix-1 }*/
    _26438 = _ix_50226 - 1;
    if ((long)((unsigned long)_26438 +(unsigned long) HIGH_BITS) >= 0){
        _26438 = NewDouble((double)_26438);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26438;
    _26439 = MAKE_SEQ(_1);
    _26438 = NOVALUE;
    DeRef(_arg_50231);
    DeRefDS(_args_50227);
    DeRef(_26433);
    _26433 = NOVALUE;
    DeRef(_26434);
    _26434 = NOVALUE;
    DeRef(_26435);
    _26435 = NOVALUE;
    DeRef(_26437);
    _26437 = NOVALUE;
    return _26439;
L7: 

    /** 		ix += 1*/
    _ix_50226 = _ix_50226 + 1;
    DeRef(_arg_50231);
    _arg_50231 = NOVALUE;

    /** 	end while*/
    goto L1; // [158] 10
L2: 

    /** 	return {0, ix-1}*/
    _26441 = _ix_50226 - 1;
    if ((long)((unsigned long)_26441 +(unsigned long) HIGH_BITS) >= 0){
        _26441 = NewDouble((double)_26441);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _26441;
    _26442 = MAKE_SEQ(_1);
    _26441 = NOVALUE;
    DeRefDS(_args_50227);
    DeRef(_26433);
    _26433 = NOVALUE;
    DeRef(_26434);
    _26434 = NOVALUE;
    DeRef(_26435);
    _26435 = NOVALUE;
    DeRef(_26437);
    _26437 = NOVALUE;
    DeRef(_26439);
    _26439 = NOVALUE;
    return _26442;
    ;
}


int _45expand_config_options(int _args_50261)
{
    int _idx_50262 = NOVALUE;
    int _next_idx_50263 = NOVALUE;
    int _files_50264 = NOVALUE;
    int _cmd_1_2_50265 = NOVALUE;
    int _26465 = NOVALUE;
    int _26464 = NOVALUE;
    int _26463 = NOVALUE;
    int _26462 = NOVALUE;
    int _26461 = NOVALUE;
    int _26460 = NOVALUE;
    int _26459 = NOVALUE;
    int _26458 = NOVALUE;
    int _26457 = NOVALUE;
    int _26452 = NOVALUE;
    int _26450 = NOVALUE;
    int _26449 = NOVALUE;
    int _26448 = NOVALUE;
    int _26446 = NOVALUE;
    int _26445 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer idx = 1*/
    _idx_50262 = 1;

    /** 	sequence files = {}*/
    RefDS(_22663);
    DeRef(_files_50264);
    _files_50264 = _22663;

    /** 	sequence cmd_1_2 = args[1..2]*/
    rhs_slice_target = (object_ptr)&_cmd_1_2_50265;
    RHS_Slice(_args_50261, 1, 2);

    /** 	args = remove( args, 1, 2 )*/
    {
        s1_ptr assign_space = SEQ_PTR(_args_50261);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(1)) ? 1 : (long)(DBL_PTR(1)->dbl);
        int stop = (IS_ATOM_INT(2)) ? 2 : (long)(DBL_PTR(2)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_args_50261), start, &_args_50261 );
            }
            else Tail(SEQ_PTR(_args_50261), stop+1, &_args_50261);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_args_50261), start, &_args_50261);
        }
        else {
            assign_slice_seq = &assign_space;
            _args_50261 = Remove_elements(start, stop, (SEQ_PTR(_args_50261)->ref == 1));
        }
    }

    /** 	while idx with entry do*/
    goto L1; // [31] 94
L2: 
    if (_idx_50262 == 0)
    {
        goto L3; // [34] 114
    }
    else{
    }

    /** 		if equal(upper(args[idx]), "-C") then*/
    _2 = (int)SEQ_PTR(_args_50261);
    _26445 = (int)*(((s1_ptr)_2)->base + _idx_50262);
    Ref(_26445);
    _26446 = _10upper(_26445);
    _26445 = NOVALUE;
    if (_26446 == _26447)
    _26448 = 1;
    else if (IS_ATOM_INT(_26446) && IS_ATOM_INT(_26447))
    _26448 = 0;
    else
    _26448 = (compare(_26446, _26447) == 0);
    DeRef(_26446);
    _26446 = NOVALUE;
    if (_26448 == 0)
    {
        _26448 = NOVALUE;
        goto L4; // [51] 82
    }
    else{
        _26448 = NOVALUE;
    }

    /** 			files = append( files, args[idx+1] )*/
    _26449 = _idx_50262 + 1;
    _2 = (int)SEQ_PTR(_args_50261);
    _26450 = (int)*(((s1_ptr)_2)->base + _26449);
    Ref(_26450);
    Append(&_files_50264, _files_50264, _26450);
    _26450 = NOVALUE;

    /** 			args = remove( args, idx, idx + 1 )*/
    _26452 = _idx_50262 + 1;
    if (_26452 > MAXINT){
        _26452 = NewDouble((double)_26452);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_args_50261);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_idx_50262)) ? _idx_50262 : (long)(DBL_PTR(_idx_50262)->dbl);
        int stop = (IS_ATOM_INT(_26452)) ? _26452 : (long)(DBL_PTR(_26452)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_args_50261), start, &_args_50261 );
            }
            else Tail(SEQ_PTR(_args_50261), stop+1, &_args_50261);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_args_50261), start, &_args_50261);
        }
        else {
            assign_slice_seq = &assign_space;
            _args_50261 = Remove_elements(start, stop, (SEQ_PTR(_args_50261)->ref == 1));
        }
    }
    DeRef(_26452);
    _26452 = NOVALUE;
    goto L5; // [79] 91
L4: 

    /** 			idx = next_idx[2]*/
    _2 = (int)SEQ_PTR(_next_idx_50263);
    _idx_50262 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_idx_50262))
    _idx_50262 = (long)DBL_PTR(_idx_50262)->dbl;
L5: 

    /** 	entry*/
L1: 

    /** 		next_idx = find_next_opt( idx, args )*/
    RefDS(_args_50261);
    _0 = _next_idx_50263;
    _next_idx_50263 = _45find_next_opt(_idx_50262, _args_50261);
    DeRef(_0);

    /** 		idx = next_idx[1]*/
    _2 = (int)SEQ_PTR(_next_idx_50263);
    _idx_50262 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_idx_50262))
    _idx_50262 = (long)DBL_PTR(_idx_50262)->dbl;

    /** 	end while*/
    goto L2; // [111] 34
L3: 

    /** 	return cmd_1_2 & merge_parameters( GetDefaultArgs( files ), args[1..next_idx[2]], options, 1 ) & args[next_idx[2]+1..$]*/
    RefDS(_files_50264);
    _26457 = _44GetDefaultArgs(_files_50264);
    _2 = (int)SEQ_PTR(_next_idx_50263);
    _26458 = (int)*(((s1_ptr)_2)->base + 2);
    rhs_slice_target = (object_ptr)&_26459;
    RHS_Slice(_args_50261, 1, _26458);
    RefDS(_45options_49932);
    _26460 = _45merge_parameters(_26457, _26459, _45options_49932, 1);
    _26457 = NOVALUE;
    _26459 = NOVALUE;
    _2 = (int)SEQ_PTR(_next_idx_50263);
    _26461 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_26461)) {
        _26462 = _26461 + 1;
        if (_26462 > MAXINT){
            _26462 = NewDouble((double)_26462);
        }
    }
    else
    _26462 = binary_op(PLUS, 1, _26461);
    _26461 = NOVALUE;
    if (IS_SEQUENCE(_args_50261)){
            _26463 = SEQ_PTR(_args_50261)->length;
    }
    else {
        _26463 = 1;
    }
    rhs_slice_target = (object_ptr)&_26464;
    RHS_Slice(_args_50261, _26462, _26463);
    {
        int concat_list[3];

        concat_list[0] = _26464;
        concat_list[1] = _26460;
        concat_list[2] = _cmd_1_2_50265;
        Concat_N((object_ptr)&_26465, concat_list, 3);
    }
    DeRefDS(_26464);
    _26464 = NOVALUE;
    DeRef(_26460);
    _26460 = NOVALUE;
    DeRefDS(_args_50261);
    DeRefDS(_next_idx_50263);
    DeRefDS(_files_50264);
    DeRefDS(_cmd_1_2_50265);
    DeRef(_26449);
    _26449 = NOVALUE;
    _26458 = NOVALUE;
    DeRef(_26462);
    _26462 = NOVALUE;
    return _26465;
    ;
}


void _45handle_common_options(int _opts_50296)
{
    int _opt_keys_50297 = NOVALUE;
    int _option_w_50299 = NOVALUE;
    int _key_50303 = NOVALUE;
    int _val_50305 = NOVALUE;
    int _this_warn_50351 = NOVALUE;
    int _auto_add_warn_50353 = NOVALUE;
    int _n_50359 = NOVALUE;
    int _this_warn_50382 = NOVALUE;
    int _auto_add_warn_50384 = NOVALUE;
    int _n_50390 = NOVALUE;
    int _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426 = NOVALUE;
    int _prompt_inlined_maybe_any_key_at_615_50425 = NOVALUE;
    int _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438 = NOVALUE;
    int _prompt_inlined_maybe_any_key_at_689_50437 = NOVALUE;
    int _26518 = NOVALUE;
    int _26517 = NOVALUE;
    int _26516 = NOVALUE;
    int _26515 = NOVALUE;
    int _26514 = NOVALUE;
    int _26513 = NOVALUE;
    int _26512 = NOVALUE;
    int _26511 = NOVALUE;
    int _26510 = NOVALUE;
    int _26508 = NOVALUE;
    int _26506 = NOVALUE;
    int _26505 = NOVALUE;
    int _26504 = NOVALUE;
    int _26499 = NOVALUE;
    int _26497 = NOVALUE;
    int _26495 = NOVALUE;
    int _26492 = NOVALUE;
    int _26491 = NOVALUE;
    int _26486 = NOVALUE;
    int _26484 = NOVALUE;
    int _26482 = NOVALUE;
    int _26480 = NOVALUE;
    int _26479 = NOVALUE;
    int _26478 = NOVALUE;
    int _26477 = NOVALUE;
    int _26476 = NOVALUE;
    int _26475 = NOVALUE;
    int _26473 = NOVALUE;
    int _26472 = NOVALUE;
    int _26467 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence opt_keys = m:keys(opts)*/
    Ref(_opts_50296);
    _0 = _opt_keys_50297;
    _opt_keys_50297 = _29keys(_opts_50296, 0);
    DeRef(_0);

    /** 	integer option_w = 0*/
    _option_w_50299 = 0;

    /** 	for idx = 1 to length(opt_keys) do*/
    if (IS_SEQUENCE(_opt_keys_50297)){
            _26467 = SEQ_PTR(_opt_keys_50297)->length;
    }
    else {
        _26467 = 1;
    }
    {
        int _idx_50301;
        _idx_50301 = 1;
L1: 
        if (_idx_50301 > _26467){
            goto L2; // [20] 731
        }

        /** 		sequence key = opt_keys[idx]*/
        DeRef(_key_50303);
        _2 = (int)SEQ_PTR(_opt_keys_50297);
        _key_50303 = (int)*(((s1_ptr)_2)->base + _idx_50301);
        Ref(_key_50303);

        /** 		object val = m:get(opts, key)*/
        Ref(_opts_50296);
        RefDS(_key_50303);
        _0 = _val_50305;
        _val_50305 = _29get(_opts_50296, _key_50303, 0);
        DeRef(_0);

        /** 		switch key do*/
        _1 = find(_key_50303, _26470);
        switch ( _1 ){ 

            /** 			case "i" then*/
            case 1:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50305)){
                    _26472 = SEQ_PTR(_val_50305)->length;
            }
            else {
                _26472 = 1;
            }
            {
                int _i_50311;
                _i_50311 = 1;
L3: 
                if (_i_50311 > _26472){
                    goto L4; // [59] 82
                }

                /** 					add_include_directory(val[i])*/
                _2 = (int)SEQ_PTR(_val_50305);
                _26473 = (int)*(((s1_ptr)_2)->base + _i_50311);
                Ref(_26473);
                _44add_include_directory(_26473);
                _26473 = NOVALUE;

                /** 				end for*/
                _i_50311 = _i_50311 + 1;
                goto L3; // [77] 66
L4: 
                ;
            }
            goto L5; // [82] 722

            /** 			case "d" then*/
            case 2:

            /** 				OpDefines &= val*/
            if (IS_SEQUENCE(_38OpDefines_17019) && IS_ATOM(_val_50305)) {
                Ref(_val_50305);
                Append(&_38OpDefines_17019, _38OpDefines_17019, _val_50305);
            }
            else if (IS_ATOM(_38OpDefines_17019) && IS_SEQUENCE(_val_50305)) {
            }
            else {
                Concat((object_ptr)&_38OpDefines_17019, _38OpDefines_17019, _val_50305);
            }
            goto L5; // [98] 722

            /** 			case "batch" then*/
            case 3:

            /** 				batch_job = 1*/
            _38batch_job_16959 = 1;
            goto L5; // [111] 722

            /** 			case "test" then*/
            case 4:

            /** 				test_only = 1*/
            _38test_only_16958 = 1;
            goto L5; // [124] 722

            /** 			case "strict" then*/
            case 5:

            /** 				Strict_is_on = 1*/
            _38Strict_is_on_17011 = 1;
            goto L5; // [137] 722

            /** 			case "p" then*/
            case 6:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50305)){
                    _26475 = SEQ_PTR(_val_50305)->length;
            }
            else {
                _26475 = 1;
            }
            {
                int _i_50326;
                _i_50326 = 1;
L6: 
                if (_i_50326 > _26475){
                    goto L7; // [148] 173
                }

                /** 					add_preprocessor(val[i])*/
                _2 = (int)SEQ_PTR(_val_50305);
                _26476 = (int)*(((s1_ptr)_2)->base + _i_50326);
                Ref(_26476);
                _66add_preprocessor(_26476, 0, 0);
                _26476 = NOVALUE;

                /** 				end for*/
                _i_50326 = _i_50326 + 1;
                goto L6; // [168] 155
L7: 
                ;
            }
            goto L5; // [173] 722

            /** 			case "pf" then*/
            case 7:

            /** 				force_preprocessor = 1*/
            _35force_preprocessor_15613 = 1;
            goto L5; // [186] 722

            /** 			case "l" then*/
            case 8:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50305)){
                    _26477 = SEQ_PTR(_val_50305)->length;
            }
            else {
                _26477 = 1;
            }
            {
                int _i_50334;
                _i_50334 = 1;
L8: 
                if (_i_50334 > _26477){
                    goto L9; // [197] 238
                }

                /** 					LocalizeQual = append(LocalizeQual, (filter(lower(val[i]), STDFLTR_ALPHA)))*/
                _2 = (int)SEQ_PTR(_val_50305);
                _26478 = (int)*(((s1_ptr)_2)->base + _i_50334);
                Ref(_26478);
                _26479 = _10lower(_26478);
                _26478 = NOVALUE;
                RefDS(_22663);
                RefDS(_5);
                _26480 = _21filter(_26479, _21STDFLTR_ALPHA_6119, _22663, _5);
                _26479 = NOVALUE;
                Ref(_26480);
                Append(&_35LocalizeQual_15614, _35LocalizeQual_15614, _26480);
                DeRef(_26480);
                _26480 = NOVALUE;

                /** 				end for*/
                _i_50334 = _i_50334 + 1;
                goto L8; // [233] 204
L9: 
                ;
            }
            goto L5; // [238] 722

            /** 			case "ldb" then*/
            case 9:

            /** 				LocalDB = val*/
            Ref(_val_50305);
            DeRef(_35LocalDB_15615);
            _35LocalDB_15615 = _val_50305;
            goto L5; // [251] 722

            /** 			case "w" then*/
            case 10:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50305)){
                    _26482 = SEQ_PTR(_val_50305)->length;
            }
            else {
                _26482 = 1;
            }
            {
                int _i_50349;
                _i_50349 = 1;
LA: 
                if (_i_50349 > _26482){
                    goto LB; // [262] 392
                }

                /** 					sequence this_warn = val[i]*/
                DeRef(_this_warn_50351);
                _2 = (int)SEQ_PTR(_val_50305);
                _this_warn_50351 = (int)*(((s1_ptr)_2)->base + _i_50349);
                Ref(_this_warn_50351);

                /** 					integer auto_add_warn = 0*/
                _auto_add_warn_50353 = 0;

                /** 					if this_warn[1] = '+' then*/
                _2 = (int)SEQ_PTR(_this_warn_50351);
                _26484 = (int)*(((s1_ptr)_2)->base + 1);
                if (binary_op_a(NOTEQ, _26484, 43)){
                    _26484 = NOVALUE;
                    goto LC; // [288] 308
                }
                _26484 = NOVALUE;

                /** 						auto_add_warn = 1*/
                _auto_add_warn_50353 = 1;

                /** 						this_warn = this_warn[2 .. $]*/
                if (IS_SEQUENCE(_this_warn_50351)){
                        _26486 = SEQ_PTR(_this_warn_50351)->length;
                }
                else {
                    _26486 = 1;
                }
                rhs_slice_target = (object_ptr)&_this_warn_50351;
                RHS_Slice(_this_warn_50351, 2, _26486);
LC: 

                /** 					integer n = find(this_warn, warning_names)*/
                _n_50359 = find_from(_this_warn_50351, _38warning_names_16990, 1);

                /** 					if n != 0 then*/
                if (_n_50359 == 0)
                goto LD; // [319] 383

                /** 						if auto_add_warn or option_w = 1 then*/
                if (_auto_add_warn_50353 != 0) {
                    goto LE; // [325] 338
                }
                _26491 = (_option_w_50299 == 1);
                if (_26491 == 0)
                {
                    DeRef(_26491);
                    _26491 = NOVALUE;
                    goto LF; // [334] 357
                }
                else{
                    DeRef(_26491);
                    _26491 = NOVALUE;
                }
LE: 

                /** 							OpWarning = or_bits(OpWarning, warning_flags[n])*/
                _2 = (int)SEQ_PTR(_38warning_flags_16988);
                _26492 = (int)*(((s1_ptr)_2)->base + _n_50359);
                {unsigned long tu;
                     tu = (unsigned long)_38OpWarning_17013 | (unsigned long)_26492;
                     _38OpWarning_17013 = MAKE_UINT(tu);
                }
                _26492 = NOVALUE;
                if (!IS_ATOM_INT(_38OpWarning_17013)) {
                    _1 = (long)(DBL_PTR(_38OpWarning_17013)->dbl);
                    if (UNIQUE(DBL_PTR(_38OpWarning_17013)) && (DBL_PTR(_38OpWarning_17013)->cleanup != 0))
                    RTFatal("Cannot assign value with a destructor to an integer");                    DeRefDS(_38OpWarning_17013);
                    _38OpWarning_17013 = _1;
                }
                goto L10; // [354] 373
LF: 

                /** 							option_w = 1*/
                _option_w_50299 = 1;

                /** 							OpWarning = warning_flags[n]*/
                _2 = (int)SEQ_PTR(_38warning_flags_16988);
                _38OpWarning_17013 = (int)*(((s1_ptr)_2)->base + _n_50359);
L10: 

                /** 						prev_OpWarning = OpWarning*/
                _38prev_OpWarning_17014 = _38OpWarning_17013;
LD: 
                DeRef(_this_warn_50351);
                _this_warn_50351 = NOVALUE;

                /** 				end for*/
                _i_50349 = _i_50349 + 1;
                goto LA; // [387] 269
LB: 
                ;
            }
            goto L5; // [392] 722

            /** 			case "x" then*/
            case 11:

            /** 				for i = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_50305)){
                    _26495 = SEQ_PTR(_val_50305)->length;
            }
            else {
                _26495 = 1;
            }
            {
                int _i_50380;
                _i_50380 = 1;
L11: 
                if (_i_50380 > _26495){
                    goto L12; // [403] 542
                }

                /** 					sequence this_warn = val[i]*/
                DeRef(_this_warn_50382);
                _2 = (int)SEQ_PTR(_val_50305);
                _this_warn_50382 = (int)*(((s1_ptr)_2)->base + _i_50380);
                Ref(_this_warn_50382);

                /** 					integer auto_add_warn = 0*/
                _auto_add_warn_50384 = 0;

                /** 					if this_warn[1] = '+' then*/
                _2 = (int)SEQ_PTR(_this_warn_50382);
                _26497 = (int)*(((s1_ptr)_2)->base + 1);
                if (binary_op_a(NOTEQ, _26497, 43)){
                    _26497 = NOVALUE;
                    goto L13; // [429] 449
                }
                _26497 = NOVALUE;

                /** 						auto_add_warn = 1*/
                _auto_add_warn_50384 = 1;

                /** 						this_warn = this_warn[2 .. $]*/
                if (IS_SEQUENCE(_this_warn_50382)){
                        _26499 = SEQ_PTR(_this_warn_50382)->length;
                }
                else {
                    _26499 = 1;
                }
                rhs_slice_target = (object_ptr)&_this_warn_50382;
                RHS_Slice(_this_warn_50382, 2, _26499);
L13: 

                /** 					integer n = find(this_warn, warning_names)*/
                _n_50390 = find_from(_this_warn_50382, _38warning_names_16990, 1);

                /** 					if n != 0 then*/
                if (_n_50390 == 0)
                goto L14; // [460] 533

                /** 						if auto_add_warn or option_w = -1 then*/
                if (_auto_add_warn_50384 != 0) {
                    goto L15; // [466] 479
                }
                _26504 = (_option_w_50299 == -1);
                if (_26504 == 0)
                {
                    DeRef(_26504);
                    _26504 = NOVALUE;
                    goto L16; // [475] 501
                }
                else{
                    DeRef(_26504);
                    _26504 = NOVALUE;
                }
L15: 

                /** 							OpWarning = and_bits(OpWarning, not_bits(warning_flags[n]))*/
                _2 = (int)SEQ_PTR(_38warning_flags_16988);
                _26505 = (int)*(((s1_ptr)_2)->base + _n_50390);
                _26506 = not_bits(_26505);
                _26505 = NOVALUE;
                if (IS_ATOM_INT(_26506)) {
                    {unsigned long tu;
                         tu = (unsigned long)_38OpWarning_17013 & (unsigned long)_26506;
                         _38OpWarning_17013 = MAKE_UINT(tu);
                    }
                }
                else {
                    temp_d.dbl = (double)_38OpWarning_17013;
                    _38OpWarning_17013 = Dand_bits(&temp_d, DBL_PTR(_26506));
                }
                DeRef(_26506);
                _26506 = NOVALUE;
                if (!IS_ATOM_INT(_38OpWarning_17013)) {
                    _1 = (long)(DBL_PTR(_38OpWarning_17013)->dbl);
                    if (UNIQUE(DBL_PTR(_38OpWarning_17013)) && (DBL_PTR(_38OpWarning_17013)->cleanup != 0))
                    RTFatal("Cannot assign value with a destructor to an integer");                    DeRefDS(_38OpWarning_17013);
                    _38OpWarning_17013 = _1;
                }
                goto L17; // [498] 523
L16: 

                /** 							option_w = -1*/
                _option_w_50299 = -1;

                /** 							OpWarning = all_warning_flag - warning_flags[n]*/
                _2 = (int)SEQ_PTR(_38warning_flags_16988);
                _26508 = (int)*(((s1_ptr)_2)->base + _n_50390);
                _38OpWarning_17013 = 32767 - _26508;
                _26508 = NOVALUE;
L17: 

                /** 						prev_OpWarning = OpWarning*/
                _38prev_OpWarning_17014 = _38OpWarning_17013;
L14: 
                DeRef(_this_warn_50382);
                _this_warn_50382 = NOVALUE;

                /** 				end for*/
                _i_50380 = _i_50380 + 1;
                goto L11; // [537] 410
L12: 
                ;
            }
            goto L5; // [542] 722

            /** 			case "wf" then*/
            case 12:

            /** 				TempWarningName = val*/
            Ref(_val_50305);
            DeRef(_38TempWarningName_16960);
            _38TempWarningName_16960 = _val_50305;

            /** 			  	error:warning_file(TempWarningName)*/
            Ref(_38TempWarningName_16960);
            _8warning_file(_38TempWarningName_16960);
            goto L5; // [560] 722

            /** 			case "v", "version" then*/
            case 13:
            case 14:

            /** 				show_banner()*/
            _45show_banner();

            /** 				if not batch_job and not test_only then*/
            _26510 = (_38batch_job_16959 == 0);
            if (_26510 == 0) {
                goto L18; // [579] 632
            }
            _26512 = (_38test_only_16958 == 0);
            if (_26512 == 0)
            {
                DeRef(_26512);
                _26512 = NOVALUE;
                goto L18; // [589] 632
            }
            else{
                DeRef(_26512);
                _26512 = NOVALUE;
            }

            /** 					console:maybe_any_key(GetMsgText(278,0), 2)*/
            RefDS(_22663);
            _26513 = _47GetMsgText(278, 0, _22663);
            DeRef(_prompt_inlined_maybe_any_key_at_615_50425);
            _prompt_inlined_maybe_any_key_at_615_50425 = _26513;
            _26513 = NOVALUE;

            /** 	if not has_console() then*/

            /** 	return machine_func(M_HAS_CONSOLE, 0)*/
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426 = machine(99, 0);
            if (IS_ATOM_INT(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426)) {
                if (_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426 != 0){
                    goto L19; // [614] 629
                }
            }
            else {
                if (DBL_PTR(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426)->dbl != 0.0){
                    goto L19; // [614] 629
                }
            }

            /** 		any_key(prompt, con)*/
            Ref(_prompt_inlined_maybe_any_key_at_615_50425);
            _28any_key(_prompt_inlined_maybe_any_key_at_615_50425, 2);

            /** end procedure*/
            goto L19; // [626] 629
L19: 
            DeRef(_prompt_inlined_maybe_any_key_at_615_50425);
            _prompt_inlined_maybe_any_key_at_615_50425 = NOVALUE;
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_618_50426 = NOVALUE;
L18: 

            /** 				abort(0)*/
            UserCleanup(0);
            goto L5; // [636] 722

            /** 			case "copyright" then*/
            case 15:

            /** 				show_copyrights()*/
            _45show_copyrights();

            /** 				if not batch_job and not test_only then*/
            _26514 = (_38batch_job_16959 == 0);
            if (_26514 == 0) {
                goto L1A; // [653] 706
            }
            _26516 = (_38test_only_16958 == 0);
            if (_26516 == 0)
            {
                DeRef(_26516);
                _26516 = NOVALUE;
                goto L1A; // [663] 706
            }
            else{
                DeRef(_26516);
                _26516 = NOVALUE;
            }

            /** 					console:maybe_any_key(GetMsgText(278,0), 2)*/
            RefDS(_22663);
            _26517 = _47GetMsgText(278, 0, _22663);
            DeRef(_prompt_inlined_maybe_any_key_at_689_50437);
            _prompt_inlined_maybe_any_key_at_689_50437 = _26517;
            _26517 = NOVALUE;

            /** 	if not has_console() then*/

            /** 	return machine_func(M_HAS_CONSOLE, 0)*/
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438 = machine(99, 0);
            if (IS_ATOM_INT(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438)) {
                if (_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438 != 0){
                    goto L1B; // [688] 703
                }
            }
            else {
                if (DBL_PTR(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438)->dbl != 0.0){
                    goto L1B; // [688] 703
                }
            }

            /** 		any_key(prompt, con)*/
            Ref(_prompt_inlined_maybe_any_key_at_689_50437);
            _28any_key(_prompt_inlined_maybe_any_key_at_689_50437, 2);

            /** end procedure*/
            goto L1B; // [700] 703
L1B: 
            DeRef(_prompt_inlined_maybe_any_key_at_689_50437);
            _prompt_inlined_maybe_any_key_at_689_50437 = NOVALUE;
            DeRef(_has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438);
            _has_console_inlined_has_console_at_6_inlined_maybe_any_key_at_692_50438 = NOVALUE;
L1A: 

            /** 				abort(0)*/
            UserCleanup(0);
            goto L5; // [710] 722

            /** 			case "eudir" then*/
            case 16:

            /** 				set_eudir( val )*/
            Ref(_val_50305);
            _35set_eudir(_val_50305);
        ;}L5: 
        DeRef(_key_50303);
        _key_50303 = NOVALUE;
        DeRef(_val_50305);
        _val_50305 = NOVALUE;

        /** 	end for*/
        _idx_50301 = _idx_50301 + 1;
        goto L1; // [726] 27
L2: 
        ;
    }

    /** 	if length(LocalizeQual) = 0 then*/
    if (IS_SEQUENCE(_35LocalizeQual_15614)){
            _26518 = SEQ_PTR(_35LocalizeQual_15614)->length;
    }
    else {
        _26518 = 1;
    }
    if (_26518 != 0)
    goto L1C; // [738] 751

    /** 		LocalizeQual = {"en"}*/
    _0 = _35LocalizeQual_15614;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_26520);
    *((int *)(_2+4)) = _26520;
    _35LocalizeQual_15614 = MAKE_SEQ(_1);
    DeRefDS(_0);
L1C: 

    /** end procedure*/
    DeRef(_opts_50296);
    DeRef(_opt_keys_50297);
    DeRef(_26510);
    _26510 = NOVALUE;
    DeRef(_26514);
    _26514 = NOVALUE;
    return;
    ;
}


void _45finalize_command_line(int _opts_50450)
{
    int _extras_50457 = NOVALUE;
    int _pairs_50462 = NOVALUE;
    int _pair_50467 = NOVALUE;
    int _26550 = NOVALUE;
    int _26548 = NOVALUE;
    int _26545 = NOVALUE;
    int _26544 = NOVALUE;
    int _26543 = NOVALUE;
    int _26542 = NOVALUE;
    int _26541 = NOVALUE;
    int _26540 = NOVALUE;
    int _26539 = NOVALUE;
    int _26538 = NOVALUE;
    int _26537 = NOVALUE;
    int _26536 = NOVALUE;
    int _26535 = NOVALUE;
    int _26534 = NOVALUE;
    int _26533 = NOVALUE;
    int _26532 = NOVALUE;
    int _26531 = NOVALUE;
    int _26530 = NOVALUE;
    int _26529 = NOVALUE;
    int _26528 = NOVALUE;
    int _26526 = NOVALUE;
    int _26523 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if Strict_is_on then -- overrides any -W/-X switches*/
    if (_38Strict_is_on_17011 == 0)
    {
        goto L1; // [5] 27
    }
    else{
    }

    /** 		OpWarning = all_warning_flag*/
    _38OpWarning_17013 = 32767;

    /** 		prev_OpWarning = OpWarning*/
    _38prev_OpWarning_17014 = 32767;
L1: 

    /** 	sequence extras = m:get(opts, cmdline:EXTRAS)*/
    Ref(_opts_50450);
    RefDS(_27EXTRAS_14471);
    _0 = _extras_50457;
    _extras_50457 = _29get(_opts_50450, _27EXTRAS_14471, 0);
    DeRef(_0);

    /** 	if length(extras) > 0 then*/
    if (IS_SEQUENCE(_extras_50457)){
            _26523 = SEQ_PTR(_extras_50457)->length;
    }
    else {
        _26523 = 1;
    }
    if (_26523 <= 0)
    goto L2; // [44] 270

    /** 		sequence pairs = m:pairs( opts )*/
    Ref(_opts_50450);
    _0 = _pairs_50462;
    _pairs_50462 = _29pairs(_opts_50450, 0);
    DeRef(_0);

    /** 		for i = 1 to length( pairs ) do*/
    if (IS_SEQUENCE(_pairs_50462)){
            _26526 = SEQ_PTR(_pairs_50462)->length;
    }
    else {
        _26526 = 1;
    }
    {
        int _i_50465;
        _i_50465 = 1;
L3: 
        if (_i_50465 > _26526){
            goto L4; // [62] 237
        }

        /** 			sequence pair = pairs[i]*/
        DeRef(_pair_50467);
        _2 = (int)SEQ_PTR(_pairs_50462);
        _pair_50467 = (int)*(((s1_ptr)_2)->base + _i_50465);
        Ref(_pair_50467);

        /** 			if equal( pair[1], cmdline:EXTRAS ) then*/
        _2 = (int)SEQ_PTR(_pair_50467);
        _26528 = (int)*(((s1_ptr)_2)->base + 1);
        if (_26528 == _27EXTRAS_14471)
        _26529 = 1;
        else if (IS_ATOM_INT(_26528) && IS_ATOM_INT(_27EXTRAS_14471))
        _26529 = 0;
        else
        _26529 = (compare(_26528, _27EXTRAS_14471) == 0);
        _26528 = NOVALUE;
        if (_26529 == 0)
        {
            _26529 = NOVALUE;
            goto L5; // [89] 99
        }
        else{
            _26529 = NOVALUE;
        }

        /** 				continue*/
        DeRefDS(_pair_50467);
        _pair_50467 = NOVALUE;
        goto L6; // [96] 232
L5: 

        /** 			pair[1] = prepend( pair[1], '-' )*/
        _2 = (int)SEQ_PTR(_pair_50467);
        _26530 = (int)*(((s1_ptr)_2)->base + 1);
        Prepend(&_26531, _26530, 45);
        _26530 = NOVALUE;
        _2 = (int)SEQ_PTR(_pair_50467);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _pair_50467 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _26531;
        if( _1 != _26531 ){
            DeRef(_1);
        }
        _26531 = NOVALUE;

        /** 			if sequence( pair[2] ) then*/
        _2 = (int)SEQ_PTR(_pair_50467);
        _26532 = (int)*(((s1_ptr)_2)->base + 2);
        _26533 = IS_SEQUENCE(_26532);
        _26532 = NOVALUE;
        if (_26533 == 0)
        {
            _26533 = NOVALUE;
            goto L7; // [122] 215
        }
        else{
            _26533 = NOVALUE;
        }

        /** 				if length( pair[2] ) and sequence( pair[2][1] ) then*/
        _2 = (int)SEQ_PTR(_pair_50467);
        _26534 = (int)*(((s1_ptr)_2)->base + 2);
        if (IS_SEQUENCE(_26534)){
                _26535 = SEQ_PTR(_26534)->length;
        }
        else {
            _26535 = 1;
        }
        _26534 = NOVALUE;
        if (_26535 == 0) {
            goto L8; // [134] 203
        }
        _2 = (int)SEQ_PTR(_pair_50467);
        _26537 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_26537);
        _26538 = (int)*(((s1_ptr)_2)->base + 1);
        _26537 = NOVALUE;
        _26539 = IS_SEQUENCE(_26538);
        _26538 = NOVALUE;
        if (_26539 == 0)
        {
            _26539 = NOVALUE;
            goto L8; // [150] 203
        }
        else{
            _26539 = NOVALUE;
        }

        /** 					for j = 1 to length( pair[2] ) do*/
        _2 = (int)SEQ_PTR(_pair_50467);
        _26540 = (int)*(((s1_ptr)_2)->base + 2);
        if (IS_SEQUENCE(_26540)){
                _26541 = SEQ_PTR(_26540)->length;
        }
        else {
            _26541 = 1;
        }
        _26540 = NOVALUE;
        {
            int _j_50485;
            _j_50485 = 1;
L9: 
            if (_j_50485 > _26541){
                goto LA; // [162] 200
            }

            /** 						switches &= { pair[1], pair[2][j] }*/
            _2 = (int)SEQ_PTR(_pair_50467);
            _26542 = (int)*(((s1_ptr)_2)->base + 1);
            _2 = (int)SEQ_PTR(_pair_50467);
            _26543 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_26543);
            _26544 = (int)*(((s1_ptr)_2)->base + _j_50485);
            _26543 = NOVALUE;
            Ref(_26544);
            Ref(_26542);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _26542;
            ((int *)_2)[2] = _26544;
            _26545 = MAKE_SEQ(_1);
            _26544 = NOVALUE;
            _26542 = NOVALUE;
            Concat((object_ptr)&_45switches_49829, _45switches_49829, _26545);
            DeRefDS(_26545);
            _26545 = NOVALUE;

            /** 					end for*/
            _j_50485 = _j_50485 + 1;
            goto L9; // [195] 169
LA: 
            ;
        }
        goto LB; // [200] 228
L8: 

        /** 					switches &= pair*/
        Concat((object_ptr)&_45switches_49829, _45switches_49829, _pair_50467);
        goto LB; // [212] 228
L7: 

        /** 				switches = append( switches, pair[1] )*/
        _2 = (int)SEQ_PTR(_pair_50467);
        _26548 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_26548);
        Append(&_45switches_49829, _45switches_49829, _26548);
        _26548 = NOVALUE;
LB: 
        DeRef(_pair_50467);
        _pair_50467 = NOVALUE;

        /** 		end for*/
L6: 
        _i_50465 = _i_50465 + 1;
        goto L3; // [232] 69
L4: 
        ;
    }

    /** 		Argv = Argv[2..3] & extras*/
    rhs_slice_target = (object_ptr)&_26550;
    RHS_Slice(_38Argv_16957, 2, 3);
    Concat((object_ptr)&_38Argv_16957, _26550, _extras_50457);
    DeRefDS(_26550);
    _26550 = NOVALUE;
    DeRef(_26550);
    _26550 = NOVALUE;

    /** 		Argc = length(Argv)*/
    if (IS_SEQUENCE(_38Argv_16957)){
            _38Argc_16956 = SEQ_PTR(_38Argv_16957)->length;
    }
    else {
        _38Argc_16956 = 1;
    }

    /** 		src_name = extras[1]*/
    DeRef(_45src_name_49828);
    _2 = (int)SEQ_PTR(_extras_50457);
    _45src_name_49828 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_45src_name_49828);
L2: 
    DeRef(_pairs_50462);
    _pairs_50462 = NOVALUE;

    /** end procedure*/
    DeRef(_opts_50450);
    DeRef(_extras_50457);
    _26534 = NOVALUE;
    _26540 = NOVALUE;
    return;
    ;
}



// 0x5A8A38A0
