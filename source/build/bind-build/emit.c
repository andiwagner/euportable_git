// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _43Push(int _x_51473)
{
    int _27130 = NOVALUE;
    int _27128 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_51473)) {
        _1 = (long)(DBL_PTR(_x_51473)->dbl);
        if (UNIQUE(DBL_PTR(_x_51473)) && (DBL_PTR(_x_51473)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51473);
        _x_51473 = _1;
    }

    /** 	cgi += 1*/
    _43cgi_51234 = _43cgi_51234 + 1;

    /** 	if cgi > length(cg_stack) then*/
    if (IS_SEQUENCE(_43cg_stack_51233)){
            _27128 = SEQ_PTR(_43cg_stack_51233)->length;
    }
    else {
        _27128 = 1;
    }
    if (_43cgi_51234 <= _27128)
    goto L1; // [20] 37

    /** 		cg_stack &= repeat(0, 400)*/
    _27130 = Repeat(0, 400);
    Concat((object_ptr)&_43cg_stack_51233, _43cg_stack_51233, _27130);
    DeRefDS(_27130);
    _27130 = NOVALUE;
L1: 

    /** 	cg_stack[cgi] = x*/
    _2 = (int)SEQ_PTR(_43cg_stack_51233);
    _2 = (int)(((s1_ptr)_2)->base + _43cgi_51234);
    _1 = *(int *)_2;
    *(int *)_2 = _x_51473;
    DeRef(_1);

    /** end procedure*/
    return;
    ;
}


int _43Top()
{
    int _27132 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return cg_stack[cgi]*/
    _2 = (int)SEQ_PTR(_43cg_stack_51233);
    _27132 = (int)*(((s1_ptr)_2)->base + _43cgi_51234);
    Ref(_27132);
    return _27132;
    ;
}


int _43Pop()
{
    int _t_51486 = NOVALUE;
    int _s_51492 = NOVALUE;
    int _27144 = NOVALUE;
    int _27142 = NOVALUE;
    int _27140 = NOVALUE;
    int _27137 = NOVALUE;
    int _27136 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	t = cg_stack[cgi]*/
    _2 = (int)SEQ_PTR(_43cg_stack_51233);
    _t_51486 = (int)*(((s1_ptr)_2)->base + _43cgi_51234);
    if (!IS_ATOM_INT(_t_51486)){
        _t_51486 = (long)DBL_PTR(_t_51486)->dbl;
    }

    /** 	cgi -= 1*/
    _43cgi_51234 = _43cgi_51234 - 1;

    /** 	if t > 0 then*/
    if (_t_51486 <= 0)
    goto L1; // [23] 116

    /** 		symtab_index s = t -- for type checking*/
    _s_51492 = _t_51486;

    /** 		if SymTab[t][S_MODE] = M_TEMP then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27136 = (int)*(((s1_ptr)_2)->base + _t_51486);
    _2 = (int)SEQ_PTR(_27136);
    _27137 = (int)*(((s1_ptr)_2)->base + 3);
    _27136 = NOVALUE;
    if (binary_op_a(NOTEQ, _27137, 3)){
        _27137 = NOVALUE;
        goto L2; // [50] 115
    }
    _27137 = NOVALUE;

    /** 			if use_private_list = 0 then  -- no problem with reusing the temp*/
    if (_38use_private_list_17079 != 0)
    goto L3; // [58] 82

    /** 				SymTab[t][S_SCOPE] = FREE -- mark it as being free*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_t_51486 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _27140 = NOVALUE;
    goto L4; // [79] 114
L3: 

    /** 			elsif find(t, private_sym) = 0 then*/
    _27142 = find_from(_t_51486, _38private_sym_17078, 1);
    if (_27142 != 0)
    goto L5; // [91] 113

    /** 				SymTab[t][S_SCOPE] = FREE -- mark it as being free*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_t_51486 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _27144 = NOVALUE;
L5: 
L4: 
L2: 
L1: 

    /** 	return t*/
    return _t_51486;
    ;
}


void _43TempKeep(int _x_51520)
{
    int _27151 = NOVALUE;
    int _27150 = NOVALUE;
    int _27149 = NOVALUE;
    int _27148 = NOVALUE;
    int _27147 = NOVALUE;
    int _27146 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_x_51520)) {
        _1 = (long)(DBL_PTR(_x_51520)->dbl);
        if (UNIQUE(DBL_PTR(_x_51520)) && (DBL_PTR(_x_51520)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51520);
        _x_51520 = _1;
    }

    /** 	if x > 0 and SymTab[x][S_MODE] = M_TEMP then*/
    _27146 = (_x_51520 > 0);
    if (_27146 == 0) {
        goto L1; // [9] 53
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27148 = (int)*(((s1_ptr)_2)->base + _x_51520);
    _2 = (int)SEQ_PTR(_27148);
    _27149 = (int)*(((s1_ptr)_2)->base + 3);
    _27148 = NOVALUE;
    if (IS_ATOM_INT(_27149)) {
        _27150 = (_27149 == 3);
    }
    else {
        _27150 = binary_op(EQUALS, _27149, 3);
    }
    _27149 = NOVALUE;
    if (_27150 == 0) {
        DeRef(_27150);
        _27150 = NOVALUE;
        goto L1; // [32] 53
    }
    else {
        if (!IS_ATOM_INT(_27150) && DBL_PTR(_27150)->dbl == 0.0){
            DeRef(_27150);
            _27150 = NOVALUE;
            goto L1; // [32] 53
        }
        DeRef(_27150);
        _27150 = NOVALUE;
    }
    DeRef(_27150);
    _27150 = NOVALUE;

    /** 		SymTab[x][S_SCOPE] = IN_USE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_x_51520 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _27151 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_27146);
    _27146 = NOVALUE;
    return;
    ;
}


void _43TempFree(int _x_51538)
{
    int _27157 = NOVALUE;
    int _27155 = NOVALUE;
    int _27154 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_x_51538)) {
        _1 = (long)(DBL_PTR(_x_51538)->dbl);
        if (UNIQUE(DBL_PTR(_x_51538)) && (DBL_PTR(_x_51538)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51538);
        _x_51538 = _1;
    }

    /** 	if x > 0 then*/
    if (_x_51538 <= 0)
    goto L1; // [5] 53

    /** 		if SymTab[x][S_MODE] = M_TEMP then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27154 = (int)*(((s1_ptr)_2)->base + _x_51538);
    _2 = (int)SEQ_PTR(_27154);
    _27155 = (int)*(((s1_ptr)_2)->base + 3);
    _27154 = NOVALUE;
    if (binary_op_a(NOTEQ, _27155, 3)){
        _27155 = NOVALUE;
        goto L2; // [25] 52
    }
    _27155 = NOVALUE;

    /** 			SymTab[x][S_SCOPE] = FREE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_x_51538 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _27157 = NOVALUE;

    /** 			clear_temp( x )*/
    _43clear_temp(_x_51538);
L2: 
L1: 

    /** end procedure*/
    return;
    ;
}


void _43TempInteger(int _x_51557)
{
    int _27164 = NOVALUE;
    int _27163 = NOVALUE;
    int _27162 = NOVALUE;
    int _27161 = NOVALUE;
    int _27160 = NOVALUE;
    int _27159 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_x_51557)) {
        _1 = (long)(DBL_PTR(_x_51557)->dbl);
        if (UNIQUE(DBL_PTR(_x_51557)) && (DBL_PTR(_x_51557)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_51557);
        _x_51557 = _1;
    }

    /** 	if x > 0 and SymTab[x][S_MODE] = M_TEMP then*/
    _27159 = (_x_51557 > 0);
    if (_27159 == 0) {
        goto L1; // [9] 53
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27161 = (int)*(((s1_ptr)_2)->base + _x_51557);
    _2 = (int)SEQ_PTR(_27161);
    _27162 = (int)*(((s1_ptr)_2)->base + 3);
    _27161 = NOVALUE;
    if (IS_ATOM_INT(_27162)) {
        _27163 = (_27162 == 3);
    }
    else {
        _27163 = binary_op(EQUALS, _27162, 3);
    }
    _27162 = NOVALUE;
    if (_27163 == 0) {
        DeRef(_27163);
        _27163 = NOVALUE;
        goto L1; // [32] 53
    }
    else {
        if (!IS_ATOM_INT(_27163) && DBL_PTR(_27163)->dbl == 0.0){
            DeRef(_27163);
            _27163 = NOVALUE;
            goto L1; // [32] 53
        }
        DeRef(_27163);
        _27163 = NOVALUE;
    }
    DeRef(_27163);
    _27163 = NOVALUE;

    /** 		SymTab[x][S_USAGE] = T_INTEGER*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_x_51557 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _27164 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_27159);
    _27159 = NOVALUE;
    return;
    ;
}


int _43LexName(int _t_51574, int _defname_51575)
{
    int _name_51577 = NOVALUE;
    int _27173 = NOVALUE;
    int _27171 = NOVALUE;
    int _27169 = NOVALUE;
    int _27168 = NOVALUE;
    int _27167 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_t_51574)) {
        _1 = (long)(DBL_PTR(_t_51574)->dbl);
        if (UNIQUE(DBL_PTR(_t_51574)) && (DBL_PTR(_t_51574)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t_51574);
        _t_51574 = _1;
    }

    /** 	for i = 1 to length(token_name) do*/
    _27167 = 80;
    {
        int _i_51579;
        _i_51579 = 1;
L1: 
        if (_i_51579 > 80){
            goto L2; // [12] 82
        }

        /** 		if t = token_name[i][LEX_NUMBER] then*/
        _2 = (int)SEQ_PTR(_43token_name_51241);
        _27168 = (int)*(((s1_ptr)_2)->base + _i_51579);
        _2 = (int)SEQ_PTR(_27168);
        _27169 = (int)*(((s1_ptr)_2)->base + 1);
        _27168 = NOVALUE;
        if (binary_op_a(NOTEQ, _t_51574, _27169)){
            _27169 = NOVALUE;
            goto L3; // [31] 75
        }
        _27169 = NOVALUE;

        /** 			name = token_name[i][LEX_NAME]*/
        _2 = (int)SEQ_PTR(_43token_name_51241);
        _27171 = (int)*(((s1_ptr)_2)->base + _i_51579);
        DeRef(_name_51577);
        _2 = (int)SEQ_PTR(_27171);
        _name_51577 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_name_51577);
        _27171 = NOVALUE;

        /** 			if not find(' ', name) then*/
        _27173 = find_from(32, _name_51577, 1);
        if (_27173 != 0)
        goto L4; // [56] 68
        _27173 = NOVALUE;

        /** 				name = "'" & name & "'"*/
        {
            int concat_list[3];

            concat_list[0] = _27175;
            concat_list[1] = _name_51577;
            concat_list[2] = _27175;
            Concat_N((object_ptr)&_name_51577, concat_list, 3);
        }
L4: 

        /** 			return name*/
        DeRefDS(_defname_51575);
        return _name_51577;
L3: 

        /** 	end for*/
        _i_51579 = _i_51579 + 1;
        goto L1; // [77] 19
L2: 
        ;
    }

    /** 	return defname -- try to avoid this case*/
    DeRef(_name_51577);
    return _defname_51575;
    ;
}


void _43InitEmit()
{
    int _0, _1, _2;
    

    /** 	cg_stack = repeat(0, 400)*/
    DeRef(_43cg_stack_51233);
    _43cg_stack_51233 = Repeat(0, 400);

    /** 	cgi = 0*/
    _43cgi_51234 = 0;

    /** end procedure*/
    return;
    ;
}


int _43IsInteger(int _sym_51598)
{
    int _mode_51599 = NOVALUE;
    int _t_51601 = NOVALUE;
    int _pt_51602 = NOVALUE;
    int _27198 = NOVALUE;
    int _27197 = NOVALUE;
    int _27195 = NOVALUE;
    int _27194 = NOVALUE;
    int _27193 = NOVALUE;
    int _27191 = NOVALUE;
    int _27190 = NOVALUE;
    int _27189 = NOVALUE;
    int _27188 = NOVALUE;
    int _27186 = NOVALUE;
    int _27182 = NOVALUE;
    int _27179 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_51598)) {
        _1 = (long)(DBL_PTR(_sym_51598)->dbl);
        if (UNIQUE(DBL_PTR(_sym_51598)) && (DBL_PTR(_sym_51598)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_51598);
        _sym_51598 = _1;
    }

    /** 	if sym < 1 then*/
    if (_sym_51598 >= 1)
    goto L1; // [5] 16

    /** 		return 0*/
    return 0;
L1: 

    /** 	mode = SymTab[sym][S_MODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27179 = (int)*(((s1_ptr)_2)->base + _sym_51598);
    _2 = (int)SEQ_PTR(_27179);
    _mode_51599 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_51599)){
        _mode_51599 = (long)DBL_PTR(_mode_51599)->dbl;
    }
    _27179 = NOVALUE;

    /** 	if mode = M_NORMAL then*/
    if (_mode_51599 != 1)
    goto L2; // [36] 136

    /** 		t = SymTab[sym][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27182 = (int)*(((s1_ptr)_2)->base + _sym_51598);
    _2 = (int)SEQ_PTR(_27182);
    _t_51601 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_t_51601)){
        _t_51601 = (long)DBL_PTR(_t_51601)->dbl;
    }
    _27182 = NOVALUE;

    /** 		if t = integer_type then*/
    if (_t_51601 != _55integer_type_47061)
    goto L3; // [60] 73

    /** 			return TRUE*/
    return _9TRUE_428;
L3: 

    /** 		if t > 0 then*/
    if (_t_51601 <= 0)
    goto L4; // [75] 215

    /** 			pt = SymTab[t][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27186 = (int)*(((s1_ptr)_2)->base + _t_51601);
    _2 = (int)SEQ_PTR(_27186);
    _pt_51602 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_pt_51602)){
        _pt_51602 = (long)DBL_PTR(_pt_51602)->dbl;
    }
    _27186 = NOVALUE;

    /** 			if pt and SymTab[pt][S_VTYPE] = integer_type then*/
    if (_pt_51602 == 0) {
        goto L4; // [97] 215
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27189 = (int)*(((s1_ptr)_2)->base + _pt_51602);
    _2 = (int)SEQ_PTR(_27189);
    _27190 = (int)*(((s1_ptr)_2)->base + 15);
    _27189 = NOVALUE;
    if (IS_ATOM_INT(_27190)) {
        _27191 = (_27190 == _55integer_type_47061);
    }
    else {
        _27191 = binary_op(EQUALS, _27190, _55integer_type_47061);
    }
    _27190 = NOVALUE;
    if (_27191 == 0) {
        DeRef(_27191);
        _27191 = NOVALUE;
        goto L4; // [120] 215
    }
    else {
        if (!IS_ATOM_INT(_27191) && DBL_PTR(_27191)->dbl == 0.0){
            DeRef(_27191);
            _27191 = NOVALUE;
            goto L4; // [120] 215
        }
        DeRef(_27191);
        _27191 = NOVALUE;
    }
    DeRef(_27191);
    _27191 = NOVALUE;

    /** 				return TRUE   -- usertype(integer x)*/
    return _9TRUE_428;
    goto L4; // [133] 215
L2: 

    /** 	elsif mode = M_CONSTANT then*/
    if (_mode_51599 != 2)
    goto L5; // [140] 176

    /** 		if integer(SymTab[sym][S_OBJ]) then  -- bug fixed: can't allow PLUS1_I op*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27193 = (int)*(((s1_ptr)_2)->base + _sym_51598);
    _2 = (int)SEQ_PTR(_27193);
    _27194 = (int)*(((s1_ptr)_2)->base + 1);
    _27193 = NOVALUE;
    if (IS_ATOM_INT(_27194))
    _27195 = 1;
    else if (IS_ATOM_DBL(_27194))
    _27195 = IS_ATOM_INT(DoubleToInt(_27194));
    else
    _27195 = 0;
    _27194 = NOVALUE;
    if (_27195 == 0)
    {
        _27195 = NOVALUE;
        goto L4; // [161] 215
    }
    else{
        _27195 = NOVALUE;
    }

    /** 			return TRUE*/
    return _9TRUE_428;
    goto L4; // [173] 215
L5: 

    /** 	elsif mode = M_TEMP then*/
    if (_mode_51599 != 3)
    goto L6; // [180] 214

    /** 		if SymTab[sym][S_USAGE] = T_INTEGER then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27197 = (int)*(((s1_ptr)_2)->base + _sym_51598);
    _2 = (int)SEQ_PTR(_27197);
    _27198 = (int)*(((s1_ptr)_2)->base + 5);
    _27197 = NOVALUE;
    if (binary_op_a(NOTEQ, _27198, 1)){
        _27198 = NOVALUE;
        goto L7; // [200] 213
    }
    _27198 = NOVALUE;

    /** 			return TRUE*/
    return _9TRUE_428;
L7: 
L6: 
L4: 

    /** 	return FALSE*/
    return _9FALSE_426;
    ;
}


void _43emit(int _val_51659)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_val_51659)) {
        _1 = (long)(DBL_PTR(_val_51659)->dbl);
        if (UNIQUE(DBL_PTR(_val_51659)) && (DBL_PTR(_val_51659)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_val_51659);
        _val_51659 = _1;
    }

    /** 	Code = append(Code, val)*/
    Append(&_38Code_17038, _38Code_17038, _val_51659);

    /** end procedure*/
    return;
    ;
}


void _43emit_opnd(int _opnd_51666)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opnd_51666)) {
        _1 = (long)(DBL_PTR(_opnd_51666)->dbl);
        if (UNIQUE(DBL_PTR(_opnd_51666)) && (DBL_PTR(_opnd_51666)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opnd_51666);
        _opnd_51666 = _1;
    }

    /** 		Push(opnd)*/
    _43Push(_opnd_51666);

    /** 		previous_op = -1  -- N.B.*/
    _38previous_op_17062 = -1;

    /** end procedure*/
    return;
    ;
}


void _43emit_addr(int _x_51670)
{
    int _0, _1, _2;
    

    /** 		Code = append(Code, x)*/
    Ref(_x_51670);
    Append(&_38Code_17038, _38Code_17038, _x_51670);

    /** end procedure*/
    DeRef(_x_51670);
    return;
    ;
}


void _43emit_opcode(int _op_51676)
{
    int _0, _1, _2;
    

    /** 	Code = append(Code, op)*/
    Append(&_38Code_17038, _38Code_17038, _op_51676);

    /** end procedure*/
    return;
    ;
}


void _43emit_temp(int _tempsym_51710, int _referenced_51711)
{
    int _27229 = NOVALUE;
    int _27228 = NOVALUE;
    int _27227 = NOVALUE;
    int _27226 = NOVALUE;
    int _27225 = NOVALUE;
    int _27224 = NOVALUE;
    int _27223 = NOVALUE;
    int _27222 = NOVALUE;
    int _27221 = NOVALUE;
    int _27220 = NOVALUE;
    int _27219 = NOVALUE;
    int _27218 = NOVALUE;
    int _27217 = NOVALUE;
    int _27216 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_referenced_51711)) {
        _1 = (long)(DBL_PTR(_referenced_51711)->dbl);
        if (UNIQUE(DBL_PTR(_referenced_51711)) && (DBL_PTR(_referenced_51711)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_referenced_51711);
        _referenced_51711 = _1;
    }

    /** 	if not TRANSLATE  then -- translator has its own way of handling temps*/
    if (_38TRANSLATE_16564 != 0)
    goto L1; // [7] 129

    /** 		if sequence(tempsym) then*/
    _27216 = IS_SEQUENCE(_tempsym_51710);
    if (_27216 == 0)
    {
        _27216 = NOVALUE;
        goto L2; // [15] 53
    }
    else{
        _27216 = NOVALUE;
    }

    /** 			for i = 1 to length(tempsym) do*/
    if (IS_SEQUENCE(_tempsym_51710)){
            _27217 = SEQ_PTR(_tempsym_51710)->length;
    }
    else {
        _27217 = 1;
    }
    {
        int _i_51718;
        _i_51718 = 1;
L3: 
        if (_i_51718 > _27217){
            goto L4; // [23] 50
        }

        /** 				emit_temp( tempsym[i], referenced )*/
        _2 = (int)SEQ_PTR(_tempsym_51710);
        _27218 = (int)*(((s1_ptr)_2)->base + _i_51718);
        DeRef(_27219);
        _27219 = _referenced_51711;
        Ref(_27218);
        _43emit_temp(_27218, _27219);
        _27218 = NOVALUE;
        _27219 = NOVALUE;

        /** 			end for*/
        _i_51718 = _i_51718 + 1;
        goto L3; // [45] 30
L4: 
        ;
    }
    goto L5; // [50] 128
L2: 

    /** 		elsif tempsym > 0*/
    if (IS_ATOM_INT(_tempsym_51710)) {
        _27220 = (_tempsym_51710 > 0);
    }
    else {
        _27220 = binary_op(GREATER, _tempsym_51710, 0);
    }
    if (IS_ATOM_INT(_27220)) {
        if (_27220 == 0) {
            DeRef(_27221);
            _27221 = 0;
            goto L6; // [59] 77
        }
    }
    else {
        if (DBL_PTR(_27220)->dbl == 0.0) {
            DeRef(_27221);
            _27221 = 0;
            goto L6; // [59] 77
        }
    }
    Ref(_tempsym_51710);
    _27222 = _55sym_mode(_tempsym_51710);
    if (IS_ATOM_INT(_27222)) {
        _27223 = (_27222 == 3);
    }
    else {
        _27223 = binary_op(EQUALS, _27222, 3);
    }
    DeRef(_27222);
    _27222 = NOVALUE;
    DeRef(_27221);
    if (IS_ATOM_INT(_27223))
    _27221 = (_27223 != 0);
    else
    _27221 = DBL_PTR(_27223)->dbl != 0.0;
L6: 
    if (_27221 == 0) {
        _27224 = 0;
        goto L7; // [77] 92
    }
    Ref(_tempsym_51710);
    _27225 = _43IsInteger(_tempsym_51710);
    if (IS_ATOM_INT(_27225)) {
        _27226 = (_27225 == 0);
    }
    else {
        _27226 = unary_op(NOT, _27225);
    }
    DeRef(_27225);
    _27225 = NOVALUE;
    if (IS_ATOM_INT(_27226))
    _27224 = (_27226 != 0);
    else
    _27224 = DBL_PTR(_27226)->dbl != 0.0;
L7: 
    if (_27224 == 0) {
        goto L8; // [92] 127
    }
    _27228 = find_from(_tempsym_51710, _43emitted_temps_51706, 1);
    _27229 = (_27228 == 0);
    _27228 = NOVALUE;
    if (_27229 == 0)
    {
        DeRef(_27229);
        _27229 = NOVALUE;
        goto L8; // [107] 127
    }
    else{
        DeRef(_27229);
        _27229 = NOVALUE;
    }

    /** 			emitted_temps &= tempsym*/
    if (IS_SEQUENCE(_43emitted_temps_51706) && IS_ATOM(_tempsym_51710)) {
        Ref(_tempsym_51710);
        Append(&_43emitted_temps_51706, _43emitted_temps_51706, _tempsym_51710);
    }
    else if (IS_ATOM(_43emitted_temps_51706) && IS_SEQUENCE(_tempsym_51710)) {
    }
    else {
        Concat((object_ptr)&_43emitted_temps_51706, _43emitted_temps_51706, _tempsym_51710);
    }

    /** 			emitted_temp_referenced &= referenced*/
    Append(&_43emitted_temp_referenced_51707, _43emitted_temp_referenced_51707, _referenced_51711);
L8: 
L5: 
L1: 

    /** end procedure*/
    DeRef(_tempsym_51710);
    DeRef(_27220);
    _27220 = NOVALUE;
    DeRef(_27226);
    _27226 = NOVALUE;
    DeRef(_27223);
    _27223 = NOVALUE;
    return;
    ;
}


void _43flush_temps(int _except_for_51740)
{
    int _refs_51743 = NOVALUE;
    int _novalues_51744 = NOVALUE;
    int _sym_51749 = NOVALUE;
    int _27244 = NOVALUE;
    int _27243 = NOVALUE;
    int _27242 = NOVALUE;
    int _27241 = NOVALUE;
    int _27239 = NOVALUE;
    int _27235 = NOVALUE;
    int _27234 = NOVALUE;
    int _27232 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [7] 16
    }
    else{
    }

    /** 		return*/
    DeRefDS(_except_for_51740);
    DeRef(_refs_51743);
    DeRefi(_novalues_51744);
    return;
L1: 

    /** 	sequence*/

    /** 		refs = {},*/
    RefDS(_22663);
    DeRef(_refs_51743);
    _refs_51743 = _22663;

    /** 		novalues = {}*/
    RefDS(_22663);
    DeRefi(_novalues_51744);
    _novalues_51744 = _22663;

    /** 	derefs = {}*/
    RefDS(_22663);
    DeRefi(_43derefs_51737);
    _43derefs_51737 = _22663;

    /** 	for i = 1 to length( emitted_temps ) do*/
    if (IS_SEQUENCE(_43emitted_temps_51706)){
            _27232 = SEQ_PTR(_43emitted_temps_51706)->length;
    }
    else {
        _27232 = 1;
    }
    {
        int _i_51746;
        _i_51746 = 1;
L2: 
        if (_i_51746 > _27232){
            goto L3; // [46] 119
        }

        /** 		symtab_index sym = emitted_temps[i]*/
        _2 = (int)SEQ_PTR(_43emitted_temps_51706);
        _sym_51749 = (int)*(((s1_ptr)_2)->base + _i_51746);
        if (!IS_ATOM_INT(_sym_51749)){
            _sym_51749 = (long)DBL_PTR(_sym_51749)->dbl;
        }

        /** 		if find( sym, except_for ) then*/
        _27234 = find_from(_sym_51749, _except_for_51740, 1);
        if (_27234 == 0)
        {
            _27234 = NOVALUE;
            goto L4; // [70] 80
        }
        else{
            _27234 = NOVALUE;
        }

        /** 			continue*/
        goto L5; // [77] 114
L4: 

        /** 		if emitted_temp_referenced[i] = NEW_REFERENCE then*/
        _2 = (int)SEQ_PTR(_43emitted_temp_referenced_51707);
        _27235 = (int)*(((s1_ptr)_2)->base + _i_51746);
        if (binary_op_a(NOTEQ, _27235, 1)){
            _27235 = NOVALUE;
            goto L6; // [88] 103
        }
        _27235 = NOVALUE;

        /** 			derefs &= sym*/
        Append(&_43derefs_51737, _43derefs_51737, _sym_51749);
        goto L7; // [100] 110
L6: 

        /** 			novalues &= sym*/
        Append(&_novalues_51744, _novalues_51744, _sym_51749);
L7: 

        /** 	end for*/
L5: 
        _i_51746 = _i_51746 + 1;
        goto L2; // [114] 53
L3: 
        ;
    }

    /** 	if not length( except_for ) then*/
    if (IS_SEQUENCE(_except_for_51740)){
            _27239 = SEQ_PTR(_except_for_51740)->length;
    }
    else {
        _27239 = 1;
    }
    if (_27239 != 0)
    goto L8; // [124] 132
    _27239 = NOVALUE;

    /** 		clear_last()*/
    _43clear_last();
L8: 

    /** 	for i = 1 to length( derefs ) do*/
    if (IS_SEQUENCE(_43derefs_51737)){
            _27241 = SEQ_PTR(_43derefs_51737)->length;
    }
    else {
        _27241 = 1;
    }
    {
        int _i_51764;
        _i_51764 = 1;
L9: 
        if (_i_51764 > _27241){
            goto LA; // [139] 171
        }

        /** 		emit( DEREF_TEMP )*/
        _43emit(208);

        /** 		emit( derefs[i] )*/
        _2 = (int)SEQ_PTR(_43derefs_51737);
        _27242 = (int)*(((s1_ptr)_2)->base + _i_51764);
        _43emit(_27242);
        _27242 = NOVALUE;

        /** 	end for*/
        _i_51764 = _i_51764 + 1;
        goto L9; // [166] 146
LA: 
        ;
    }

    /** 	for i = 1 to length( novalues ) do*/
    if (IS_SEQUENCE(_novalues_51744)){
            _27243 = SEQ_PTR(_novalues_51744)->length;
    }
    else {
        _27243 = 1;
    }
    {
        int _i_51769;
        _i_51769 = 1;
LB: 
        if (_i_51769 > _27243){
            goto LC; // [176] 206
        }

        /** 		emit( NOVALUE_TEMP )*/
        _43emit(209);

        /** 		emit( novalues[i] )*/
        _2 = (int)SEQ_PTR(_novalues_51744);
        _27244 = (int)*(((s1_ptr)_2)->base + _i_51769);
        _43emit(_27244);
        _27244 = NOVALUE;

        /** 	end for*/
        _i_51769 = _i_51769 + 1;
        goto LB; // [201] 183
LC: 
        ;
    }

    /** 	emitted_temps = {}*/
    RefDS(_22663);
    DeRef(_43emitted_temps_51706);
    _43emitted_temps_51706 = _22663;

    /** 	emitted_temp_referenced = {}*/
    RefDS(_22663);
    DeRef(_43emitted_temp_referenced_51707);
    _43emitted_temp_referenced_51707 = _22663;

    /** end procedure*/
    DeRefDS(_except_for_51740);
    DeRef(_refs_51743);
    DeRefi(_novalues_51744);
    return;
    ;
}


void _43flush_temp(int _temp_51776)
{
    int _except_for_51777 = NOVALUE;
    int _ix_51778 = NOVALUE;
    int _27246 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_temp_51776)) {
        _1 = (long)(DBL_PTR(_temp_51776)->dbl);
        if (UNIQUE(DBL_PTR(_temp_51776)) && (DBL_PTR(_temp_51776)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_temp_51776);
        _temp_51776 = _1;
    }

    /** 	sequence except_for = emitted_temps*/
    RefDS(_43emitted_temps_51706);
    DeRef(_except_for_51777);
    _except_for_51777 = _43emitted_temps_51706;

    /** 	integer ix = find( temp, emitted_temps )*/
    _ix_51778 = find_from(_temp_51776, _43emitted_temps_51706, 1);

    /** 	if ix then*/
    if (_ix_51778 == 0)
    {
        goto L1; // [23] 37
    }
    else{
    }

    /** 		flush_temps( remove( except_for, ix ) )*/
    {
        s1_ptr assign_space = SEQ_PTR(_except_for_51777);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_51778)) ? _ix_51778 : (long)(DBL_PTR(_ix_51778)->dbl);
        int stop = (IS_ATOM_INT(_ix_51778)) ? _ix_51778 : (long)(DBL_PTR(_ix_51778)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
            RefDS(_except_for_51777);
            DeRef(_27246);
            _27246 = _except_for_51777;
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_except_for_51777), start, &_27246 );
            }
            else Tail(SEQ_PTR(_except_for_51777), stop+1, &_27246);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_except_for_51777), start, &_27246);
        }
        else {
            assign_slice_seq = &assign_space;
            _1 = Remove_elements(start, stop, 0);
            DeRef(_27246);
            _27246 = _1;
        }
    }
    _43flush_temps(_27246);
    _27246 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_except_for_51777);
    return;
    ;
}


void _43check_for_temps()
{
    int _27253 = NOVALUE;
    int _27252 = NOVALUE;
    int _27251 = NOVALUE;
    int _27250 = NOVALUE;
    int _27248 = NOVALUE;
    int _27247 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if TRANSLATE or last_op < 1 or last_pc < 1 then*/
    if (_38TRANSLATE_16564 != 0) {
        _27247 = 1;
        goto L1; // [5] 19
    }
    _27248 = (_43last_op_52112 < 1);
    _27247 = (_27248 != 0);
L1: 
    if (_27247 != 0) {
        goto L2; // [19] 34
    }
    _27250 = (_43last_pc_52113 < 1);
    if (_27250 == 0)
    {
        DeRef(_27250);
        _27250 = NOVALUE;
        goto L3; // [30] 40
    }
    else{
        DeRef(_27250);
        _27250 = NOVALUE;
    }
L2: 

    /** 		return*/
    DeRef(_27248);
    _27248 = NOVALUE;
    return;
L3: 

    /** 	emit_temp( get_target_sym( current_op( last_pc ) ), op_temp_ref[last_op] )*/
    RefDS(_38Code_17038);
    _27251 = _67current_op(_43last_pc_52113, _38Code_17038);
    _27252 = _67get_target_sym(_27251);
    _27251 = NOVALUE;
    _2 = (int)SEQ_PTR(_43op_temp_ref_51928);
    _27253 = (int)*(((s1_ptr)_2)->base + _43last_op_52112);
    _43emit_temp(_27252, _27253);
    _27252 = NOVALUE;
    _27253 = NOVALUE;

    /** end procedure*/
    DeRef(_27248);
    _27248 = NOVALUE;
    return;
    ;
}


void _43clear_temp(int _tempsym_51803)
{
    int _ix_51804 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tempsym_51803)) {
        _1 = (long)(DBL_PTR(_tempsym_51803)->dbl);
        if (UNIQUE(DBL_PTR(_tempsym_51803)) && (DBL_PTR(_tempsym_51803)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tempsym_51803);
        _tempsym_51803 = _1;
    }

    /** 	integer ix = find( tempsym, emitted_temps )*/
    _ix_51804 = find_from(_tempsym_51803, _43emitted_temps_51706, 1);

    /** 	if ix then*/
    if (_ix_51804 == 0)
    {
        goto L1; // [14] 36
    }
    else{
    }

    /** 		emitted_temps = remove( emitted_temps, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_43emitted_temps_51706);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_51804)) ? _ix_51804 : (long)(DBL_PTR(_ix_51804)->dbl);
        int stop = (IS_ATOM_INT(_ix_51804)) ? _ix_51804 : (long)(DBL_PTR(_ix_51804)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_43emitted_temps_51706), start, &_43emitted_temps_51706 );
            }
            else Tail(SEQ_PTR(_43emitted_temps_51706), stop+1, &_43emitted_temps_51706);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_43emitted_temps_51706), start, &_43emitted_temps_51706);
        }
        else {
            assign_slice_seq = &assign_space;
            _43emitted_temps_51706 = Remove_elements(start, stop, (SEQ_PTR(_43emitted_temps_51706)->ref == 1));
        }
    }

    /** 		emitted_temp_referenced = remove( emitted_temp_referenced, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_43emitted_temp_referenced_51707);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_51804)) ? _ix_51804 : (long)(DBL_PTR(_ix_51804)->dbl);
        int stop = (IS_ATOM_INT(_ix_51804)) ? _ix_51804 : (long)(DBL_PTR(_ix_51804)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_43emitted_temp_referenced_51707), start, &_43emitted_temp_referenced_51707 );
            }
            else Tail(SEQ_PTR(_43emitted_temp_referenced_51707), stop+1, &_43emitted_temp_referenced_51707);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_43emitted_temp_referenced_51707), start, &_43emitted_temp_referenced_51707);
        }
        else {
            assign_slice_seq = &assign_space;
            _43emitted_temp_referenced_51707 = Remove_elements(start, stop, (SEQ_PTR(_43emitted_temp_referenced_51707)->ref == 1));
        }
    }
L1: 

    /** end procedure*/
    return;
    ;
}


int _43pop_temps()
{
    int _new_emitted_51811 = NOVALUE;
    int _new_referenced_51812 = NOVALUE;
    int _27257 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence new_emitted  = emitted_temps*/
    RefDS(_43emitted_temps_51706);
    DeRef(_new_emitted_51811);
    _new_emitted_51811 = _43emitted_temps_51706;

    /** 	sequence new_referenced = emitted_temp_referenced*/
    RefDS(_43emitted_temp_referenced_51707);
    DeRef(_new_referenced_51812);
    _new_referenced_51812 = _43emitted_temp_referenced_51707;

    /** 	emitted_temps  = {}*/
    RefDS(_22663);
    DeRefDS(_43emitted_temps_51706);
    _43emitted_temps_51706 = _22663;

    /** 	emitted_temp_referenced = {}*/
    RefDS(_22663);
    DeRefDS(_43emitted_temp_referenced_51707);
    _43emitted_temp_referenced_51707 = _22663;

    /** 	return { new_emitted, new_referenced }*/
    RefDS(_new_referenced_51812);
    RefDS(_new_emitted_51811);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _new_emitted_51811;
    ((int *)_2)[2] = _new_referenced_51812;
    _27257 = MAKE_SEQ(_1);
    DeRefDS(_new_emitted_51811);
    DeRefDS(_new_referenced_51812);
    return _27257;
    ;
}


int _43get_temps(int _add_to_51816)
{
    int _27262 = NOVALUE;
    int _27261 = NOVALUE;
    int _27260 = NOVALUE;
    int _27259 = NOVALUE;
    int _0, _1, _2;
    

    /** 	add_to[1] &= emitted_temps*/
    _2 = (int)SEQ_PTR(_add_to_51816);
    _27259 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_27259) && IS_ATOM(_43emitted_temps_51706)) {
    }
    else if (IS_ATOM(_27259) && IS_SEQUENCE(_43emitted_temps_51706)) {
        Ref(_27259);
        Prepend(&_27260, _43emitted_temps_51706, _27259);
    }
    else {
        Concat((object_ptr)&_27260, _27259, _43emitted_temps_51706);
        _27259 = NOVALUE;
    }
    _27259 = NOVALUE;
    _2 = (int)SEQ_PTR(_add_to_51816);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _add_to_51816 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _27260;
    if( _1 != _27260 ){
        DeRef(_1);
    }
    _27260 = NOVALUE;

    /** 	add_to[2] &= emitted_temp_referenced*/
    _2 = (int)SEQ_PTR(_add_to_51816);
    _27261 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_27261) && IS_ATOM(_43emitted_temp_referenced_51707)) {
    }
    else if (IS_ATOM(_27261) && IS_SEQUENCE(_43emitted_temp_referenced_51707)) {
        Ref(_27261);
        Prepend(&_27262, _43emitted_temp_referenced_51707, _27261);
    }
    else {
        Concat((object_ptr)&_27262, _27261, _43emitted_temp_referenced_51707);
        _27261 = NOVALUE;
    }
    _27261 = NOVALUE;
    _2 = (int)SEQ_PTR(_add_to_51816);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _add_to_51816 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _27262;
    if( _1 != _27262 ){
        DeRef(_1);
    }
    _27262 = NOVALUE;

    /** 	return add_to*/
    return _add_to_51816;
    ;
}


void _43push_temps(int _temps_51824)
{
    int _27265 = NOVALUE;
    int _27263 = NOVALUE;
    int _0, _1, _2;
    

    /** 	emitted_temps &= temps[1]*/
    _2 = (int)SEQ_PTR(_temps_51824);
    _27263 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_43emitted_temps_51706) && IS_ATOM(_27263)) {
        Ref(_27263);
        Append(&_43emitted_temps_51706, _43emitted_temps_51706, _27263);
    }
    else if (IS_ATOM(_43emitted_temps_51706) && IS_SEQUENCE(_27263)) {
    }
    else {
        Concat((object_ptr)&_43emitted_temps_51706, _43emitted_temps_51706, _27263);
    }
    _27263 = NOVALUE;

    /** 	emitted_temp_referenced &= temps[2]*/
    _2 = (int)SEQ_PTR(_temps_51824);
    _27265 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_43emitted_temp_referenced_51707) && IS_ATOM(_27265)) {
        Ref(_27265);
        Append(&_43emitted_temp_referenced_51707, _43emitted_temp_referenced_51707, _27265);
    }
    else if (IS_ATOM(_43emitted_temp_referenced_51707) && IS_SEQUENCE(_27265)) {
    }
    else {
        Concat((object_ptr)&_43emitted_temp_referenced_51707, _43emitted_temp_referenced_51707, _27265);
    }
    _27265 = NOVALUE;

    /** 	flush_temps()*/
    RefDS(_22663);
    _43flush_temps(_22663);

    /** end procedure*/
    DeRefDS(_temps_51824);
    return;
    ;
}


void _43backpatch(int _index_51831, int _val_51832)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_index_51831)) {
        _1 = (long)(DBL_PTR(_index_51831)->dbl);
        if (UNIQUE(DBL_PTR(_index_51831)) && (DBL_PTR(_index_51831)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_51831);
        _index_51831 = _1;
    }
    if (!IS_ATOM_INT(_val_51832)) {
        _1 = (long)(DBL_PTR(_val_51832)->dbl);
        if (UNIQUE(DBL_PTR(_val_51832)) && (DBL_PTR(_val_51832)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_val_51832);
        _val_51832 = _1;
    }

    /** 		Code[index] = val*/
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index_51831);
    _1 = *(int *)_2;
    *(int *)_2 = _val_51832;
    DeRef(_1);

    /** end procedure*/
    return;
    ;
}


void _43cont11ii(int _op_52013, int _ii_52015)
{
    int _t_52016 = NOVALUE;
    int _source_52017 = NOVALUE;
    int _c_52018 = NOVALUE;
    int _27274 = NOVALUE;
    int _27273 = NOVALUE;
    int _27271 = NOVALUE;
    int _0, _1, _2;
    

    /** 	emit_opcode(op)*/
    _43emit_opcode(_op_52013);

    /** 	source = Pop()*/
    _source_52017 = _43Pop();
    if (!IS_ATOM_INT(_source_52017)) {
        _1 = (long)(DBL_PTR(_source_52017)->dbl);
        if (UNIQUE(DBL_PTR(_source_52017)) && (DBL_PTR(_source_52017)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_source_52017);
        _source_52017 = _1;
    }

    /** 	emit_addr(source)*/
    _43emit_addr(_source_52017);

    /** 	assignable = TRUE*/
    _43assignable_51236 = _9TRUE_428;

    /** 	t = op_result[op]*/
    _2 = (int)SEQ_PTR(_43op_result_51834);
    _t_52016 = (int)*(((s1_ptr)_2)->base + _op_52013);

    /** 	if t = T_INTEGER or (ii and IsInteger(source)) then*/
    _27271 = (_t_52016 == 1);
    if (_27271 != 0) {
        goto L1; // [43] 64
    }
    if (_ii_52015 == 0) {
        _27273 = 0;
        goto L2; // [47] 59
    }
    _27274 = _43IsInteger(_source_52017);
    if (IS_ATOM_INT(_27274))
    _27273 = (_27274 != 0);
    else
    _27273 = DBL_PTR(_27274)->dbl != 0.0;
L2: 
    if (_27273 == 0)
    {
        _27273 = NOVALUE;
        goto L3; // [60] 80
    }
    else{
        _27273 = NOVALUE;
    }
L1: 

    /** 		c = NewTempSym()*/
    _c_52018 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_c_52018)) {
        _1 = (long)(DBL_PTR(_c_52018)->dbl);
        if (UNIQUE(DBL_PTR(_c_52018)) && (DBL_PTR(_c_52018)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52018);
        _c_52018 = _1;
    }

    /** 		TempInteger(c)*/
    _43TempInteger(_c_52018);
    goto L4; // [77] 95
L3: 

    /** 		c = NewTempSym() -- allocate *after* checking opnd type*/
    _c_52018 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_c_52018)) {
        _1 = (long)(DBL_PTR(_c_52018)->dbl);
        if (UNIQUE(DBL_PTR(_c_52018)) && (DBL_PTR(_c_52018)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52018);
        _c_52018 = _1;
    }

    /** 		emit_temp( c, NEW_REFERENCE )*/
    _43emit_temp(_c_52018, 1);
L4: 

    /** 	Push(c)*/
    _43Push(_c_52018);

    /** 	emit_addr(c)*/
    _43emit_addr(_c_52018);

    /** end procedure*/
    DeRef(_27271);
    _27271 = NOVALUE;
    DeRef(_27274);
    _27274 = NOVALUE;
    return;
    ;
}


void _43cont21d(int _op_52035, int _a_52036, int _b_52037, int _ii_52039)
{
    int _c_52040 = NOVALUE;
    int _t_52041 = NOVALUE;
    int _27284 = NOVALUE;
    int _27283 = NOVALUE;
    int _27282 = NOVALUE;
    int _27281 = NOVALUE;
    int _27279 = NOVALUE;
    int _0, _1, _2;
    

    /** 	assignable = TRUE*/
    _43assignable_51236 = _9TRUE_428;

    /** 	t = op_result[op]*/
    _2 = (int)SEQ_PTR(_43op_result_51834);
    _t_52041 = (int)*(((s1_ptr)_2)->base + _op_52035);

    /** 	if op = C_FUNC then*/
    if (_op_52035 != 133)
    goto L1; // [26] 38

    /** 		emit_addr(CurrentSub)*/
    _43emit_addr(_38CurrentSub_16954);
L1: 

    /** 	if t = T_INTEGER or (ii and IsInteger(a) and IsInteger(b)) then*/
    _27279 = (_t_52041 == 1);
    if (_27279 != 0) {
        goto L2; // [46] 79
    }
    if (_ii_52039 == 0) {
        _27281 = 0;
        goto L3; // [50] 62
    }
    _27282 = _43IsInteger(_a_52036);
    if (IS_ATOM_INT(_27282))
    _27281 = (_27282 != 0);
    else
    _27281 = DBL_PTR(_27282)->dbl != 0.0;
L3: 
    if (_27281 == 0) {
        DeRef(_27283);
        _27283 = 0;
        goto L4; // [62] 74
    }
    _27284 = _43IsInteger(_b_52037);
    if (IS_ATOM_INT(_27284))
    _27283 = (_27284 != 0);
    else
    _27283 = DBL_PTR(_27284)->dbl != 0.0;
L4: 
    if (_27283 == 0)
    {
        _27283 = NOVALUE;
        goto L5; // [75] 95
    }
    else{
        _27283 = NOVALUE;
    }
L2: 

    /** 		c = NewTempSym()*/
    _c_52040 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_c_52040)) {
        _1 = (long)(DBL_PTR(_c_52040)->dbl);
        if (UNIQUE(DBL_PTR(_c_52040)) && (DBL_PTR(_c_52040)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52040);
        _c_52040 = _1;
    }

    /** 		TempInteger(c)*/
    _43TempInteger(_c_52040);
    goto L6; // [92] 110
L5: 

    /** 		c = NewTempSym() -- allocate *after* checking opnd types*/
    _c_52040 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_c_52040)) {
        _1 = (long)(DBL_PTR(_c_52040)->dbl);
        if (UNIQUE(DBL_PTR(_c_52040)) && (DBL_PTR(_c_52040)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_52040);
        _c_52040 = _1;
    }

    /** 		emit_temp( c, NEW_REFERENCE )*/
    _43emit_temp(_c_52040, 1);
L6: 

    /** 	Push(c)*/
    _43Push(_c_52040);

    /** 	emit_addr(c)*/
    _43emit_addr(_c_52040);

    /** end procedure*/
    DeRef(_27279);
    _27279 = NOVALUE;
    DeRef(_27282);
    _27282 = NOVALUE;
    DeRef(_27284);
    _27284 = NOVALUE;
    return;
    ;
}


void _43cont21ii(int _op_52063, int _ii_52065)
{
    int _a_52066 = NOVALUE;
    int _b_52067 = NOVALUE;
    int _0, _1, _2;
    

    /** 	b = Pop()*/
    _b_52067 = _43Pop();
    if (!IS_ATOM_INT(_b_52067)) {
        _1 = (long)(DBL_PTR(_b_52067)->dbl);
        if (UNIQUE(DBL_PTR(_b_52067)) && (DBL_PTR(_b_52067)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_52067);
        _b_52067 = _1;
    }

    /** 	emit_opcode(op)*/
    _43emit_opcode(_op_52063);

    /** 	a = Pop()*/
    _a_52066 = _43Pop();
    if (!IS_ATOM_INT(_a_52066)) {
        _1 = (long)(DBL_PTR(_a_52066)->dbl);
        if (UNIQUE(DBL_PTR(_a_52066)) && (DBL_PTR(_a_52066)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_52066);
        _a_52066 = _1;
    }

    /** 	emit_addr(a)*/
    _43emit_addr(_a_52066);

    /** 	emit_addr(b)*/
    _43emit_addr(_b_52067);

    /** 	cont21d(op, a, b, ii)*/
    _43cont21d(_op_52063, _a_52066, _b_52067, _ii_52065);

    /** end procedure*/
    return;
    ;
}


int _43good_string(int _elements_52072)
{
    int _obj_52073 = NOVALUE;
    int _ep_52075 = NOVALUE;
    int _e_52077 = NOVALUE;
    int _element_vals_52078 = NOVALUE;
    int _27307 = NOVALUE;
    int _27306 = NOVALUE;
    int _27305 = NOVALUE;
    int _27304 = NOVALUE;
    int _27303 = NOVALUE;
    int _27302 = NOVALUE;
    int _27301 = NOVALUE;
    int _27300 = NOVALUE;
    int _27299 = NOVALUE;
    int _27298 = NOVALUE;
    int _27297 = NOVALUE;
    int _27295 = NOVALUE;
    int _27292 = NOVALUE;
    int _27291 = NOVALUE;
    int _27290 = NOVALUE;
    int _27289 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence element_vals*/

    /** 	if TRANSLATE and length(elements) > 10000 then*/
    if (_38TRANSLATE_16564 == 0) {
        goto L1; // [9] 31
    }
    if (IS_SEQUENCE(_elements_52072)){
            _27290 = SEQ_PTR(_elements_52072)->length;
    }
    else {
        _27290 = 1;
    }
    _27291 = (_27290 > 10000);
    _27290 = NOVALUE;
    if (_27291 == 0)
    {
        DeRef(_27291);
        _27291 = NOVALUE;
        goto L1; // [21] 31
    }
    else{
        DeRef(_27291);
        _27291 = NOVALUE;
    }

    /** 		return -1 -- A huge string might upset the C compiler.*/
    DeRefDS(_elements_52072);
    DeRef(_obj_52073);
    DeRef(_element_vals_52078);
    return -1;
L1: 

    /** 	element_vals = {}*/
    RefDS(_22663);
    DeRef(_element_vals_52078);
    _element_vals_52078 = _22663;

    /** 	for i = 1 to length(elements) do*/
    if (IS_SEQUENCE(_elements_52072)){
            _27292 = SEQ_PTR(_elements_52072)->length;
    }
    else {
        _27292 = 1;
    }
    {
        int _i_52085;
        _i_52085 = 1;
L2: 
        if (_i_52085 > _27292){
            goto L3; // [43] 183
        }

        /** 		ep = elements[i]*/
        _2 = (int)SEQ_PTR(_elements_52072);
        _ep_52075 = (int)*(((s1_ptr)_2)->base + _i_52085);
        if (!IS_ATOM_INT(_ep_52075)){
            _ep_52075 = (long)DBL_PTR(_ep_52075)->dbl;
        }

        /** 		if ep < 1 then*/
        if (_ep_52075 >= 1)
        goto L4; // [60] 71

        /** 			return -1*/
        DeRefDS(_elements_52072);
        DeRef(_obj_52073);
        DeRef(_element_vals_52078);
        return -1;
L4: 

        /** 		e = ep*/
        _e_52077 = _ep_52075;

        /** 		obj = SymTab[e][S_OBJ]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27295 = (int)*(((s1_ptr)_2)->base + _e_52077);
        DeRef(_obj_52073);
        _2 = (int)SEQ_PTR(_27295);
        _obj_52073 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_obj_52073);
        _27295 = NOVALUE;

        /** 		if SymTab[e][S_MODE] = M_CONSTANT and*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27297 = (int)*(((s1_ptr)_2)->base + _e_52077);
        _2 = (int)SEQ_PTR(_27297);
        _27298 = (int)*(((s1_ptr)_2)->base + 3);
        _27297 = NOVALUE;
        if (IS_ATOM_INT(_27298)) {
            _27299 = (_27298 == 2);
        }
        else {
            _27299 = binary_op(EQUALS, _27298, 2);
        }
        _27298 = NOVALUE;
        if (IS_ATOM_INT(_27299)) {
            if (_27299 == 0) {
                DeRef(_27300);
                _27300 = 0;
                goto L5; // [112] 123
            }
        }
        else {
            if (DBL_PTR(_27299)->dbl == 0.0) {
                DeRef(_27300);
                _27300 = 0;
                goto L5; // [112] 123
            }
        }
        if (IS_ATOM_INT(_obj_52073))
        _27301 = 1;
        else if (IS_ATOM_DBL(_obj_52073))
        _27301 = IS_ATOM_INT(DoubleToInt(_obj_52073));
        else
        _27301 = 0;
        DeRef(_27300);
        _27300 = (_27301 != 0);
L5: 
        if (_27300 == 0) {
            goto L6; // [123] 169
        }
        _27303 = (_38TRANSLATE_16564 == 0);
        if (_27303 != 0) {
            DeRef(_27304);
            _27304 = 1;
            goto L7; // [132] 156
        }
        if (IS_ATOM_INT(_obj_52073)) {
            _27305 = (_obj_52073 >= 1);
        }
        else {
            _27305 = binary_op(GREATEREQ, _obj_52073, 1);
        }
        if (IS_ATOM_INT(_27305)) {
            if (_27305 == 0) {
                DeRef(_27306);
                _27306 = 0;
                goto L8; // [140] 152
            }
        }
        else {
            if (DBL_PTR(_27305)->dbl == 0.0) {
                DeRef(_27306);
                _27306 = 0;
                goto L8; // [140] 152
            }
        }
        if (IS_ATOM_INT(_obj_52073)) {
            _27307 = (_obj_52073 <= 255);
        }
        else {
            _27307 = binary_op(LESSEQ, _obj_52073, 255);
        }
        DeRef(_27306);
        if (IS_ATOM_INT(_27307))
        _27306 = (_27307 != 0);
        else
        _27306 = DBL_PTR(_27307)->dbl != 0.0;
L8: 
        DeRef(_27304);
        _27304 = (_27306 != 0);
L7: 
        if (_27304 == 0)
        {
            _27304 = NOVALUE;
            goto L6; // [157] 169
        }
        else{
            _27304 = NOVALUE;
        }

        /** 			element_vals = prepend(element_vals, obj)*/
        Ref(_obj_52073);
        Prepend(&_element_vals_52078, _element_vals_52078, _obj_52073);
        goto L9; // [166] 176
L6: 

        /** 			return -1*/
        DeRefDS(_elements_52072);
        DeRef(_obj_52073);
        DeRef(_element_vals_52078);
        DeRef(_27303);
        _27303 = NOVALUE;
        DeRef(_27299);
        _27299 = NOVALUE;
        DeRef(_27305);
        _27305 = NOVALUE;
        DeRef(_27307);
        _27307 = NOVALUE;
        return -1;
L9: 

        /** 	end for*/
        _i_52085 = _i_52085 + 1;
        goto L2; // [178] 50
L3: 
        ;
    }

    /** 	return element_vals*/
    DeRefDS(_elements_52072);
    DeRef(_obj_52073);
    DeRef(_27303);
    _27303 = NOVALUE;
    DeRef(_27299);
    _27299 = NOVALUE;
    DeRef(_27305);
    _27305 = NOVALUE;
    DeRef(_27307);
    _27307 = NOVALUE;
    return _element_vals_52078;
    ;
}


int _43Last_op()
{
    int _0, _1, _2;
    

    /** 	return last_op*/
    return _43last_op_52112;
    ;
}


int _43Last_pc()
{
    int _0, _1, _2;
    

    /** 	return last_pc*/
    return _43last_pc_52113;
    ;
}


void _43move_last_pc(int _amount_52120)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_amount_52120)) {
        _1 = (long)(DBL_PTR(_amount_52120)->dbl);
        if (UNIQUE(DBL_PTR(_amount_52120)) && (DBL_PTR(_amount_52120)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_amount_52120);
        _amount_52120 = _1;
    }

    /** 	if last_pc > 0 then*/
    if (_43last_pc_52113 <= 0)
    goto L1; // [7] 20

    /** 		last_pc += amount*/
    _43last_pc_52113 = _43last_pc_52113 + _amount_52120;
L1: 

    /** end procedure*/
    return;
    ;
}


void _43clear_last()
{
    int _0, _1, _2;
    

    /** 	last_op = 0*/
    _43last_op_52112 = 0;

    /** 	last_pc = 0*/
    _43last_pc_52113 = 0;

    /** end procedure*/
    return;
    ;
}


void _43clear_op()
{
    int _0, _1, _2;
    

    /** 	previous_op = -1*/
    _38previous_op_17062 = -1;

    /** 	assignable = FALSE*/
    _43assignable_51236 = _9FALSE_426;

    /** end procedure*/
    return;
    ;
}


void _43inlined_function()
{
    int _0, _1, _2;
    

    /** 	previous_op = PROC*/
    _38previous_op_17062 = 27;

    /** 	assignable = TRUE*/
    _43assignable_51236 = _9TRUE_428;

    /** 	inlined = TRUE*/
    _43inlined_52131 = _9TRUE_428;

    /** end procedure*/
    return;
    ;
}


void _43add_inline_target(int _pc_52142)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_52142)) {
        _1 = (long)(DBL_PTR(_pc_52142)->dbl);
        if (UNIQUE(DBL_PTR(_pc_52142)) && (DBL_PTR(_pc_52142)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_52142);
        _pc_52142 = _1;
    }

    /** 	inlined_targets &= pc*/
    Append(&_43inlined_targets_52139, _43inlined_targets_52139, _pc_52142);

    /** end procedure*/
    return;
    ;
}


void _43clear_inline_targets()
{
    int _0, _1, _2;
    

    /** 	inlined_targets = {}*/
    RefDS(_22663);
    DeRefi(_43inlined_targets_52139);
    _43inlined_targets_52139 = _22663;

    /** end procedure*/
    return;
    ;
}


void _43emit_inline(int _code_52148)
{
    int _0, _1, _2;
    

    /** 	last_pc = 0*/
    _43last_pc_52113 = 0;

    /** 	last_op = 0*/
    _43last_op_52112 = 0;

    /** 	Code &= code*/
    Concat((object_ptr)&_38Code_17038, _38Code_17038, _code_52148);

    /** end procedure*/
    DeRefDS(_code_52148);
    return;
    ;
}


void _43emit_op(int _op_52153)
{
    int _a_52155 = NOVALUE;
    int _b_52156 = NOVALUE;
    int _c_52157 = NOVALUE;
    int _d_52158 = NOVALUE;
    int _source_52159 = NOVALUE;
    int _target_52160 = NOVALUE;
    int _subsym_52161 = NOVALUE;
    int _lhs_var_52163 = NOVALUE;
    int _ib_52164 = NOVALUE;
    int _ic_52165 = NOVALUE;
    int _n_52166 = NOVALUE;
    int _obj_52167 = NOVALUE;
    int _elements_52168 = NOVALUE;
    int _element_vals_52169 = NOVALUE;
    int _last_pc_backup_52170 = NOVALUE;
    int _last_op_backup_52171 = NOVALUE;
    int _temp_52180 = NOVALUE;
    int _real_op_52468 = NOVALUE;
    int _ref_52475 = NOVALUE;
    int _paths_52505 = NOVALUE;
    int _if_code_52585 = NOVALUE;
    int _if_code_52624 = NOVALUE;
    int _Top_inlined_Top_at_5200_53191 = NOVALUE;
    int _element_53262 = NOVALUE;
    int _Top_inlined_Top_at_6755_53409 = NOVALUE;
    int _32365 = NOVALUE;
    int _32364 = NOVALUE;
    int _27888 = NOVALUE;
    int _27884 = NOVALUE;
    int _27881 = NOVALUE;
    int _27879 = NOVALUE;
    int _27873 = NOVALUE;
    int _27872 = NOVALUE;
    int _27871 = NOVALUE;
    int _27869 = NOVALUE;
    int _27867 = NOVALUE;
    int _27866 = NOVALUE;
    int _27865 = NOVALUE;
    int _27864 = NOVALUE;
    int _27863 = NOVALUE;
    int _27862 = NOVALUE;
    int _27861 = NOVALUE;
    int _27860 = NOVALUE;
    int _27859 = NOVALUE;
    int _27858 = NOVALUE;
    int _27857 = NOVALUE;
    int _27855 = NOVALUE;
    int _27854 = NOVALUE;
    int _27853 = NOVALUE;
    int _27851 = NOVALUE;
    int _27850 = NOVALUE;
    int _27849 = NOVALUE;
    int _27848 = NOVALUE;
    int _27847 = NOVALUE;
    int _27846 = NOVALUE;
    int _27845 = NOVALUE;
    int _27844 = NOVALUE;
    int _27843 = NOVALUE;
    int _27840 = NOVALUE;
    int _27837 = NOVALUE;
    int _27836 = NOVALUE;
    int _27835 = NOVALUE;
    int _27834 = NOVALUE;
    int _27832 = NOVALUE;
    int _27830 = NOVALUE;
    int _27818 = NOVALUE;
    int _27810 = NOVALUE;
    int _27807 = NOVALUE;
    int _27806 = NOVALUE;
    int _27805 = NOVALUE;
    int _27804 = NOVALUE;
    int _27801 = NOVALUE;
    int _27800 = NOVALUE;
    int _27799 = NOVALUE;
    int _27798 = NOVALUE;
    int _27797 = NOVALUE;
    int _27796 = NOVALUE;
    int _27795 = NOVALUE;
    int _27794 = NOVALUE;
    int _27793 = NOVALUE;
    int _27792 = NOVALUE;
    int _27791 = NOVALUE;
    int _27789 = NOVALUE;
    int _27785 = NOVALUE;
    int _27784 = NOVALUE;
    int _27783 = NOVALUE;
    int _27782 = NOVALUE;
    int _27781 = NOVALUE;
    int _27780 = NOVALUE;
    int _27779 = NOVALUE;
    int _27778 = NOVALUE;
    int _27777 = NOVALUE;
    int _27776 = NOVALUE;
    int _27775 = NOVALUE;
    int _27773 = NOVALUE;
    int _27768 = NOVALUE;
    int _27767 = NOVALUE;
    int _27765 = NOVALUE;
    int _27764 = NOVALUE;
    int _27763 = NOVALUE;
    int _27761 = NOVALUE;
    int _27758 = NOVALUE;
    int _27754 = NOVALUE;
    int _27751 = NOVALUE;
    int _27750 = NOVALUE;
    int _27749 = NOVALUE;
    int _27748 = NOVALUE;
    int _27747 = NOVALUE;
    int _27746 = NOVALUE;
    int _27744 = NOVALUE;
    int _27743 = NOVALUE;
    int _27741 = NOVALUE;
    int _27740 = NOVALUE;
    int _27739 = NOVALUE;
    int _27738 = NOVALUE;
    int _27737 = NOVALUE;
    int _27736 = NOVALUE;
    int _27735 = NOVALUE;
    int _27734 = NOVALUE;
    int _27733 = NOVALUE;
    int _27732 = NOVALUE;
    int _27730 = NOVALUE;
    int _27729 = NOVALUE;
    int _27728 = NOVALUE;
    int _27727 = NOVALUE;
    int _27726 = NOVALUE;
    int _27725 = NOVALUE;
    int _27724 = NOVALUE;
    int _27723 = NOVALUE;
    int _27722 = NOVALUE;
    int _27721 = NOVALUE;
    int _27720 = NOVALUE;
    int _27719 = NOVALUE;
    int _27718 = NOVALUE;
    int _27717 = NOVALUE;
    int _27716 = NOVALUE;
    int _27714 = NOVALUE;
    int _27711 = NOVALUE;
    int _27710 = NOVALUE;
    int _27709 = NOVALUE;
    int _27708 = NOVALUE;
    int _27707 = NOVALUE;
    int _27706 = NOVALUE;
    int _27705 = NOVALUE;
    int _27704 = NOVALUE;
    int _27703 = NOVALUE;
    int _27702 = NOVALUE;
    int _27701 = NOVALUE;
    int _27700 = NOVALUE;
    int _27699 = NOVALUE;
    int _27698 = NOVALUE;
    int _27697 = NOVALUE;
    int _27695 = NOVALUE;
    int _27691 = NOVALUE;
    int _27688 = NOVALUE;
    int _27686 = NOVALUE;
    int _27685 = NOVALUE;
    int _27683 = NOVALUE;
    int _27682 = NOVALUE;
    int _27681 = NOVALUE;
    int _27680 = NOVALUE;
    int _27679 = NOVALUE;
    int _27678 = NOVALUE;
    int _27677 = NOVALUE;
    int _27676 = NOVALUE;
    int _27675 = NOVALUE;
    int _27674 = NOVALUE;
    int _27673 = NOVALUE;
    int _27672 = NOVALUE;
    int _27671 = NOVALUE;
    int _27670 = NOVALUE;
    int _27669 = NOVALUE;
    int _27668 = NOVALUE;
    int _27667 = NOVALUE;
    int _27666 = NOVALUE;
    int _27665 = NOVALUE;
    int _27663 = NOVALUE;
    int _27661 = NOVALUE;
    int _27660 = NOVALUE;
    int _27658 = NOVALUE;
    int _27648 = NOVALUE;
    int _27647 = NOVALUE;
    int _27646 = NOVALUE;
    int _27645 = NOVALUE;
    int _27644 = NOVALUE;
    int _27643 = NOVALUE;
    int _27642 = NOVALUE;
    int _27641 = NOVALUE;
    int _27640 = NOVALUE;
    int _27639 = NOVALUE;
    int _27638 = NOVALUE;
    int _27637 = NOVALUE;
    int _27636 = NOVALUE;
    int _27635 = NOVALUE;
    int _27634 = NOVALUE;
    int _27633 = NOVALUE;
    int _27632 = NOVALUE;
    int _27631 = NOVALUE;
    int _27630 = NOVALUE;
    int _27629 = NOVALUE;
    int _27628 = NOVALUE;
    int _27627 = NOVALUE;
    int _27626 = NOVALUE;
    int _27625 = NOVALUE;
    int _27619 = NOVALUE;
    int _27618 = NOVALUE;
    int _27615 = NOVALUE;
    int _27612 = NOVALUE;
    int _27611 = NOVALUE;
    int _27610 = NOVALUE;
    int _27609 = NOVALUE;
    int _27608 = NOVALUE;
    int _27607 = NOVALUE;
    int _27606 = NOVALUE;
    int _27605 = NOVALUE;
    int _27604 = NOVALUE;
    int _27603 = NOVALUE;
    int _27601 = NOVALUE;
    int _27600 = NOVALUE;
    int _27599 = NOVALUE;
    int _27597 = NOVALUE;
    int _27595 = NOVALUE;
    int _27594 = NOVALUE;
    int _27593 = NOVALUE;
    int _27592 = NOVALUE;
    int _27591 = NOVALUE;
    int _27590 = NOVALUE;
    int _27589 = NOVALUE;
    int _27588 = NOVALUE;
    int _27587 = NOVALUE;
    int _27586 = NOVALUE;
    int _27584 = NOVALUE;
    int _27583 = NOVALUE;
    int _27581 = NOVALUE;
    int _27580 = NOVALUE;
    int _27579 = NOVALUE;
    int _27578 = NOVALUE;
    int _27577 = NOVALUE;
    int _27575 = NOVALUE;
    int _27574 = NOVALUE;
    int _27573 = NOVALUE;
    int _27572 = NOVALUE;
    int _27571 = NOVALUE;
    int _27570 = NOVALUE;
    int _27569 = NOVALUE;
    int _27568 = NOVALUE;
    int _27566 = NOVALUE;
    int _27565 = NOVALUE;
    int _27564 = NOVALUE;
    int _27563 = NOVALUE;
    int _27562 = NOVALUE;
    int _27560 = NOVALUE;
    int _27559 = NOVALUE;
    int _27557 = NOVALUE;
    int _27556 = NOVALUE;
    int _27555 = NOVALUE;
    int _27554 = NOVALUE;
    int _27553 = NOVALUE;
    int _27551 = NOVALUE;
    int _27549 = NOVALUE;
    int _27547 = NOVALUE;
    int _27546 = NOVALUE;
    int _27544 = NOVALUE;
    int _27543 = NOVALUE;
    int _27542 = NOVALUE;
    int _27541 = NOVALUE;
    int _27540 = NOVALUE;
    int _27539 = NOVALUE;
    int _27538 = NOVALUE;
    int _27537 = NOVALUE;
    int _27536 = NOVALUE;
    int _27535 = NOVALUE;
    int _27534 = NOVALUE;
    int _27533 = NOVALUE;
    int _27532 = NOVALUE;
    int _27531 = NOVALUE;
    int _27530 = NOVALUE;
    int _27529 = NOVALUE;
    int _27528 = NOVALUE;
    int _27525 = NOVALUE;
    int _27524 = NOVALUE;
    int _27522 = NOVALUE;
    int _27521 = NOVALUE;
    int _27520 = NOVALUE;
    int _27519 = NOVALUE;
    int _27518 = NOVALUE;
    int _27516 = NOVALUE;
    int _27514 = NOVALUE;
    int _27513 = NOVALUE;
    int _27512 = NOVALUE;
    int _27511 = NOVALUE;
    int _27510 = NOVALUE;
    int _27509 = NOVALUE;
    int _27508 = NOVALUE;
    int _27507 = NOVALUE;
    int _27506 = NOVALUE;
    int _27503 = NOVALUE;
    int _27502 = NOVALUE;
    int _27500 = NOVALUE;
    int _27499 = NOVALUE;
    int _27498 = NOVALUE;
    int _27497 = NOVALUE;
    int _27496 = NOVALUE;
    int _27493 = NOVALUE;
    int _27492 = NOVALUE;
    int _27491 = NOVALUE;
    int _27490 = NOVALUE;
    int _27489 = NOVALUE;
    int _27488 = NOVALUE;
    int _27487 = NOVALUE;
    int _27482 = NOVALUE;
    int _27481 = NOVALUE;
    int _27480 = NOVALUE;
    int _27478 = NOVALUE;
    int _27477 = NOVALUE;
    int _27475 = NOVALUE;
    int _27474 = NOVALUE;
    int _27469 = NOVALUE;
    int _27468 = NOVALUE;
    int _27467 = NOVALUE;
    int _27466 = NOVALUE;
    int _27465 = NOVALUE;
    int _27459 = NOVALUE;
    int _27458 = NOVALUE;
    int _27456 = NOVALUE;
    int _27455 = NOVALUE;
    int _27454 = NOVALUE;
    int _27453 = NOVALUE;
    int _27452 = NOVALUE;
    int _27451 = NOVALUE;
    int _27449 = NOVALUE;
    int _27448 = NOVALUE;
    int _27447 = NOVALUE;
    int _27446 = NOVALUE;
    int _27445 = NOVALUE;
    int _27444 = NOVALUE;
    int _27443 = NOVALUE;
    int _27442 = NOVALUE;
    int _27441 = NOVALUE;
    int _27440 = NOVALUE;
    int _27439 = NOVALUE;
    int _27438 = NOVALUE;
    int _27437 = NOVALUE;
    int _27436 = NOVALUE;
    int _27434 = NOVALUE;
    int _27433 = NOVALUE;
    int _27432 = NOVALUE;
    int _27431 = NOVALUE;
    int _27428 = NOVALUE;
    int _27427 = NOVALUE;
    int _27426 = NOVALUE;
    int _27425 = NOVALUE;
    int _27423 = NOVALUE;
    int _27422 = NOVALUE;
    int _27421 = NOVALUE;
    int _27420 = NOVALUE;
    int _27418 = NOVALUE;
    int _27417 = NOVALUE;
    int _27416 = NOVALUE;
    int _27415 = NOVALUE;
    int _27414 = NOVALUE;
    int _27413 = NOVALUE;
    int _27412 = NOVALUE;
    int _27411 = NOVALUE;
    int _27410 = NOVALUE;
    int _27409 = NOVALUE;
    int _27408 = NOVALUE;
    int _27407 = NOVALUE;
    int _27406 = NOVALUE;
    int _27405 = NOVALUE;
    int _27403 = NOVALUE;
    int _27402 = NOVALUE;
    int _27401 = NOVALUE;
    int _27400 = NOVALUE;
    int _27399 = NOVALUE;
    int _27397 = NOVALUE;
    int _27396 = NOVALUE;
    int _27395 = NOVALUE;
    int _27394 = NOVALUE;
    int _27393 = NOVALUE;
    int _27389 = NOVALUE;
    int _27388 = NOVALUE;
    int _27387 = NOVALUE;
    int _27386 = NOVALUE;
    int _27385 = NOVALUE;
    int _27383 = NOVALUE;
    int _27382 = NOVALUE;
    int _27381 = NOVALUE;
    int _27380 = NOVALUE;
    int _27379 = NOVALUE;
    int _27378 = NOVALUE;
    int _27377 = NOVALUE;
    int _27376 = NOVALUE;
    int _27375 = NOVALUE;
    int _27374 = NOVALUE;
    int _27373 = NOVALUE;
    int _27372 = NOVALUE;
    int _27371 = NOVALUE;
    int _27370 = NOVALUE;
    int _27369 = NOVALUE;
    int _27368 = NOVALUE;
    int _27367 = NOVALUE;
    int _27366 = NOVALUE;
    int _27364 = NOVALUE;
    int _27363 = NOVALUE;
    int _27362 = NOVALUE;
    int _27361 = NOVALUE;
    int _27360 = NOVALUE;
    int _27359 = NOVALUE;
    int _27358 = NOVALUE;
    int _27357 = NOVALUE;
    int _27356 = NOVALUE;
    int _27354 = NOVALUE;
    int _27353 = NOVALUE;
    int _27352 = NOVALUE;
    int _27350 = NOVALUE;
    int _27349 = NOVALUE;
    int _27347 = NOVALUE;
    int _27345 = NOVALUE;
    int _27344 = NOVALUE;
    int _27343 = NOVALUE;
    int _27342 = NOVALUE;
    int _27341 = NOVALUE;
    int _27340 = NOVALUE;
    int _27336 = NOVALUE;
    int _27335 = NOVALUE;
    int _27334 = NOVALUE;
    int _27332 = NOVALUE;
    int _27331 = NOVALUE;
    int _27330 = NOVALUE;
    int _27329 = NOVALUE;
    int _27328 = NOVALUE;
    int _27327 = NOVALUE;
    int _27326 = NOVALUE;
    int _27325 = NOVALUE;
    int _27324 = NOVALUE;
    int _27323 = NOVALUE;
    int _27322 = NOVALUE;
    int _27321 = NOVALUE;
    int _27320 = NOVALUE;
    int _27319 = NOVALUE;
    int _27318 = NOVALUE;
    int _27313 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_op_52153)) {
        _1 = (long)(DBL_PTR(_op_52153)->dbl);
        if (UNIQUE(DBL_PTR(_op_52153)) && (DBL_PTR(_op_52153)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_52153);
        _op_52153 = _1;
    }

    /** 	integer ib, ic, n*/

    /** 	object obj*/

    /** 	sequence elements*/

    /** 	object element_vals*/

    /** 	check_for_temps()*/
    _43check_for_temps();

    /** 	integer last_pc_backup = last_pc*/
    _last_pc_backup_52170 = _43last_pc_52113;

    /** 	integer last_op_backup = last_op*/
    _last_op_backup_52171 = _43last_op_52112;

    /** 	last_op = op*/
    _43last_op_52112 = _op_52153;

    /** 	last_pc = length(Code) + 1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _27313 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _27313 = 1;
    }
    _43last_pc_52113 = _27313 + 1;
    _27313 = NOVALUE;

    /** 	switch op label "EMIT" do*/
    _0 = _op_52153;
    switch ( _0 ){ 

        /** 	case ASSIGN then*/
        case 18:

        /** 		sequence temp = {}*/
        RefDS(_22663);
        DeRef(_temp_52180);
        _temp_52180 = _22663;

        /** 		if not TRANSLATE and*/
        _27318 = (_38TRANSLATE_16564 == 0);
        if (_27318 == 0) {
            goto L1; // [70] 202
        }
        _27320 = (_38previous_op_17062 == 92);
        if (_27320 != 0) {
            DeRef(_27321);
            _27321 = 1;
            goto L2; // [82] 98
        }
        _27322 = (_38previous_op_17062 == 25);
        _27321 = (_27322 != 0);
L2: 
        if (_27321 == 0)
        {
            _27321 = NOVALUE;
            goto L1; // [99] 202
        }
        else{
            _27321 = NOVALUE;
        }

        /** 			while Code[$-1] = DEREF_TEMP and find( Code[$], derefs ) do*/
L3: 
        if (IS_SEQUENCE(_38Code_17038)){
                _27323 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27323 = 1;
        }
        _27324 = _27323 - 1;
        _27323 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27325 = (int)*(((s1_ptr)_2)->base + _27324);
        if (IS_ATOM_INT(_27325)) {
            _27326 = (_27325 == 208);
        }
        else {
            _27326 = binary_op(EQUALS, _27325, 208);
        }
        _27325 = NOVALUE;
        if (IS_ATOM_INT(_27326)) {
            if (_27326 == 0) {
                goto L4; // [126] 201
            }
        }
        else {
            if (DBL_PTR(_27326)->dbl == 0.0) {
                goto L4; // [126] 201
            }
        }
        if (IS_SEQUENCE(_38Code_17038)){
                _27328 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27328 = 1;
        }
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27329 = (int)*(((s1_ptr)_2)->base + _27328);
        _27330 = find_from(_27329, _43derefs_51737, 1);
        _27329 = NOVALUE;
        if (_27330 == 0)
        {
            _27330 = NOVALUE;
            goto L4; // [147] 201
        }
        else{
            _27330 = NOVALUE;
        }

        /** 				temp &= Code[$]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27331 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27331 = 1;
        }
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27332 = (int)*(((s1_ptr)_2)->base + _27331);
        if (IS_SEQUENCE(_temp_52180) && IS_ATOM(_27332)) {
            Ref(_27332);
            Append(&_temp_52180, _temp_52180, _27332);
        }
        else if (IS_ATOM(_temp_52180) && IS_SEQUENCE(_27332)) {
        }
        else {
            Concat((object_ptr)&_temp_52180, _temp_52180, _27332);
        }
        _27332 = NOVALUE;

        /** 				Code = remove( Code, length(Code)-1, length(Code) )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27334 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27334 = 1;
        }
        _27335 = _27334 - 1;
        _27334 = NOVALUE;
        if (IS_SEQUENCE(_38Code_17038)){
                _27336 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27336 = 1;
        }
        {
            s1_ptr assign_space = SEQ_PTR(_38Code_17038);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_27335)) ? _27335 : (long)(DBL_PTR(_27335)->dbl);
            int stop = (IS_ATOM_INT(_27336)) ? _27336 : (long)(DBL_PTR(_27336)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_38Code_17038), start, &_38Code_17038 );
                }
                else Tail(SEQ_PTR(_38Code_17038), stop+1, &_38Code_17038);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_38Code_17038), start, &_38Code_17038);
            }
            else {
                assign_slice_seq = &assign_space;
                _38Code_17038 = Remove_elements(start, stop, (SEQ_PTR(_38Code_17038)->ref == 1));
            }
        }
        _27335 = NOVALUE;
        _27336 = NOVALUE;

        /** 				emit_temp( temp, NEW_REFERENCE )*/
        RefDS(_temp_52180);
        _43emit_temp(_temp_52180, 1);

        /** 			end while*/
        goto L3; // [198] 107
L4: 
L1: 

        /** 		source = Pop()*/
        _source_52159 = _43Pop();
        if (!IS_ATOM_INT(_source_52159)) {
            _1 = (long)(DBL_PTR(_source_52159)->dbl);
            if (UNIQUE(DBL_PTR(_source_52159)) && (DBL_PTR(_source_52159)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_source_52159);
            _source_52159 = _1;
        }

        /** 		target = Pop()*/
        _target_52160 = _43Pop();
        if (!IS_ATOM_INT(_target_52160)) {
            _1 = (long)(DBL_PTR(_target_52160)->dbl);
            if (UNIQUE(DBL_PTR(_target_52160)) && (DBL_PTR(_target_52160)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_target_52160);
            _target_52160 = _1;
        }

        /** 		if assignable then*/
        if (_43assignable_51236 == 0)
        {
            goto L5; // [220] 606
        }
        else{
        }

        /** 			if inlined then*/
        if (_43inlined_52131 == 0)
        {
            goto L6; // [227] 331
        }
        else{
        }

        /** 				inlined = 0*/
        _43inlined_52131 = 0;

        /** 				if length( inlined_targets ) then*/
        if (IS_SEQUENCE(_43inlined_targets_52139)){
                _27340 = SEQ_PTR(_43inlined_targets_52139)->length;
        }
        else {
            _27340 = 1;
        }
        if (_27340 == 0)
        {
            _27340 = NOVALUE;
            goto L7; // [242] 300
        }
        else{
            _27340 = NOVALUE;
        }

        /** 					for i = 1 to length( inlined_targets ) do*/
        if (IS_SEQUENCE(_43inlined_targets_52139)){
                _27341 = SEQ_PTR(_43inlined_targets_52139)->length;
        }
        else {
            _27341 = 1;
        }
        {
            int _i_52223;
            _i_52223 = 1;
L8: 
            if (_i_52223 > _27341){
                goto L9; // [252] 280
            }

            /** 						Code[inlined_targets[i]] = target*/
            _2 = (int)SEQ_PTR(_43inlined_targets_52139);
            _27342 = (int)*(((s1_ptr)_2)->base + _i_52223);
            _2 = (int)SEQ_PTR(_38Code_17038);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _38Code_17038 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _27342);
            _1 = *(int *)_2;
            *(int *)_2 = _target_52160;
            DeRef(_1);

            /** 					end for*/
            _i_52223 = _i_52223 + 1;
            goto L8; // [275] 259
L9: 
            ;
        }

        /** 					clear_inline_targets()*/

        /** 	inlined_targets = {}*/
        RefDS(_22663);
        DeRefi(_43inlined_targets_52139);
        _43inlined_targets_52139 = _22663;

        /** end procedure*/
        goto LA; // [291] 294
LA: 

        /** 					op = -1*/
        _op_52153 = -1;
L7: 

        /** 				assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 				clear_last()*/

        /** 	last_op = 0*/
        _43last_op_52112 = 0;

        /** 	last_pc = 0*/
        _43last_pc_52113 = 0;

        /** end procedure*/
        goto LB; // [321] 324
LB: 

        /** 				break "EMIT"*/
        DeRef(_temp_52180);
        _temp_52180 = NOVALUE;
        goto LC; // [328] 7417
L6: 

        /** 			clear_temp( Code[$] )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27343 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27343 = 1;
        }
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27344 = (int)*(((s1_ptr)_2)->base + _27343);
        Ref(_27344);
        _43clear_temp(_27344);
        _27344 = NOVALUE;

        /** 			Code = remove( Code, length( Code ) ) -- drop previous target*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27345 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27345 = 1;
        }
        {
            s1_ptr assign_space = SEQ_PTR(_38Code_17038);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_27345)) ? _27345 : (long)(DBL_PTR(_27345)->dbl);
            int stop = (IS_ATOM_INT(_27345)) ? _27345 : (long)(DBL_PTR(_27345)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_38Code_17038), start, &_38Code_17038 );
                }
                else Tail(SEQ_PTR(_38Code_17038), stop+1, &_38Code_17038);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_38Code_17038), start, &_38Code_17038);
            }
            else {
                assign_slice_seq = &assign_space;
                _38Code_17038 = Remove_elements(start, stop, (SEQ_PTR(_38Code_17038)->ref == 1));
            }
        }
        _27345 = NOVALUE;
        _27345 = NOVALUE;

        /** 			op = previous_op -- keep same previous op*/
        _op_52153 = _38previous_op_17062;

        /** 			if IsInteger(target) then*/
        _27347 = _43IsInteger(_target_52160);
        if (_27347 == 0) {
            DeRef(_27347);
            _27347 = NOVALUE;
            goto LD; // [377] 593
        }
        else {
            if (!IS_ATOM_INT(_27347) && DBL_PTR(_27347)->dbl == 0.0){
                DeRef(_27347);
                _27347 = NOVALUE;
                goto LD; // [377] 593
            }
            DeRef(_27347);
            _27347 = NOVALUE;
        }
        DeRef(_27347);
        _27347 = NOVALUE;

        /** 				if previous_op = RHS_SUBS then*/
        if (_38previous_op_17062 != 25)
        goto LE; // [386] 417

        /** 					op = RHS_SUBS_I*/
        _op_52153 = 114;

        /** 					backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27349 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27349 = 1;
        }
        _27350 = _27349 - 2;
        _27349 = NOVALUE;
        _43backpatch(_27350, 114);
        _27350 = NOVALUE;
        goto LF; // [414] 592
LE: 

        /** 				elsif previous_op = PLUS1 then*/
        if (_38previous_op_17062 != 93)
        goto L10; // [423] 454

        /** 					op = PLUS1_I*/
        _op_52153 = 117;

        /** 					backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27352 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27352 = 1;
        }
        _27353 = _27352 - 2;
        _27352 = NOVALUE;
        _43backpatch(_27353, 117);
        _27353 = NOVALUE;
        goto LF; // [451] 592
L10: 

        /** 				elsif previous_op = PLUS or previous_op = MINUS then*/
        _27354 = (_38previous_op_17062 == 11);
        if (_27354 != 0) {
            goto L11; // [464] 481
        }
        _27356 = (_38previous_op_17062 == 10);
        if (_27356 == 0)
        {
            DeRef(_27356);
            _27356 = NOVALUE;
            goto L12; // [477] 572
        }
        else{
            DeRef(_27356);
            _27356 = NOVALUE;
        }
L11: 

        /** 					if IsInteger(Code[$]) and*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27357 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27357 = 1;
        }
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27358 = (int)*(((s1_ptr)_2)->base + _27357);
        Ref(_27358);
        _27359 = _43IsInteger(_27358);
        _27358 = NOVALUE;
        if (IS_ATOM_INT(_27359)) {
            if (_27359 == 0) {
                goto LF; // [496] 592
            }
        }
        else {
            if (DBL_PTR(_27359)->dbl == 0.0) {
                goto LF; // [496] 592
            }
        }
        if (IS_SEQUENCE(_38Code_17038)){
                _27361 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27361 = 1;
        }
        _27362 = _27361 - 1;
        _27361 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27363 = (int)*(((s1_ptr)_2)->base + _27362);
        Ref(_27363);
        _27364 = _43IsInteger(_27363);
        _27363 = NOVALUE;
        if (_27364 == 0) {
            DeRef(_27364);
            _27364 = NOVALUE;
            goto LF; // [518] 592
        }
        else {
            if (!IS_ATOM_INT(_27364) && DBL_PTR(_27364)->dbl == 0.0){
                DeRef(_27364);
                _27364 = NOVALUE;
                goto LF; // [518] 592
            }
            DeRef(_27364);
            _27364 = NOVALUE;
        }
        DeRef(_27364);
        _27364 = NOVALUE;

        /** 						if previous_op = PLUS then*/
        if (_38previous_op_17062 != 11)
        goto L13; // [527] 543

        /** 							op = PLUS_I*/
        _op_52153 = 115;
        goto L14; // [540] 553
L13: 

        /** 							op = MINUS_I*/
        _op_52153 = 116;
L14: 

        /** 						backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27366 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27366 = 1;
        }
        _27367 = _27366 - 2;
        _27366 = NOVALUE;
        _43backpatch(_27367, _op_52153);
        _27367 = NOVALUE;
        goto LF; // [569] 592
L12: 

        /** 					if IsInteger(source) then*/
        _27368 = _43IsInteger(_source_52159);
        if (_27368 == 0) {
            DeRef(_27368);
            _27368 = NOVALUE;
            goto L15; // [578] 591
        }
        else {
            if (!IS_ATOM_INT(_27368) && DBL_PTR(_27368)->dbl == 0.0){
                DeRef(_27368);
                _27368 = NOVALUE;
                goto L15; // [578] 591
            }
            DeRef(_27368);
            _27368 = NOVALUE;
        }
        DeRef(_27368);
        _27368 = NOVALUE;

        /** 						op = ASSIGN_I -- fake to avoid subsequent check*/
        _op_52153 = 113;
L15: 
LF: 
LD: 

        /** 			last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto L16; // [603] 748
L5: 

        /** 			if IsInteger(source) and IsInteger(target) then*/
        _27369 = _43IsInteger(_source_52159);
        if (IS_ATOM_INT(_27369)) {
            if (_27369 == 0) {
                goto L17; // [612] 634
            }
        }
        else {
            if (DBL_PTR(_27369)->dbl == 0.0) {
                goto L17; // [612] 634
            }
        }
        _27371 = _43IsInteger(_target_52160);
        if (_27371 == 0) {
            DeRef(_27371);
            _27371 = NOVALUE;
            goto L17; // [621] 634
        }
        else {
            if (!IS_ATOM_INT(_27371) && DBL_PTR(_27371)->dbl == 0.0){
                DeRef(_27371);
                _27371 = NOVALUE;
                goto L17; // [621] 634
            }
            DeRef(_27371);
            _27371 = NOVALUE;
        }
        DeRef(_27371);
        _27371 = NOVALUE;

        /** 				op = ASSIGN_I*/
        _op_52153 = 113;
L17: 

        /** 			if source > 0 and target > 0 and*/
        _27372 = (_source_52159 > 0);
        if (_27372 == 0) {
            _27373 = 0;
            goto L18; // [640] 652
        }
        _27374 = (_target_52160 > 0);
        _27373 = (_27374 != 0);
L18: 
        if (_27373 == 0) {
            _27375 = 0;
            goto L19; // [652] 678
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27376 = (int)*(((s1_ptr)_2)->base + _source_52159);
        _2 = (int)SEQ_PTR(_27376);
        _27377 = (int)*(((s1_ptr)_2)->base + 3);
        _27376 = NOVALUE;
        if (IS_ATOM_INT(_27377)) {
            _27378 = (_27377 == 2);
        }
        else {
            _27378 = binary_op(EQUALS, _27377, 2);
        }
        _27377 = NOVALUE;
        if (IS_ATOM_INT(_27378))
        _27375 = (_27378 != 0);
        else
        _27375 = DBL_PTR(_27378)->dbl != 0.0;
L19: 
        if (_27375 == 0) {
            goto L1A; // [678] 732
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27380 = (int)*(((s1_ptr)_2)->base + _target_52160);
        _2 = (int)SEQ_PTR(_27380);
        _27381 = (int)*(((s1_ptr)_2)->base + 3);
        _27380 = NOVALUE;
        if (IS_ATOM_INT(_27381)) {
            _27382 = (_27381 == 2);
        }
        else {
            _27382 = binary_op(EQUALS, _27381, 2);
        }
        _27381 = NOVALUE;
        if (_27382 == 0) {
            DeRef(_27382);
            _27382 = NOVALUE;
            goto L1A; // [701] 732
        }
        else {
            if (!IS_ATOM_INT(_27382) && DBL_PTR(_27382)->dbl == 0.0){
                DeRef(_27382);
                _27382 = NOVALUE;
                goto L1A; // [701] 732
            }
            DeRef(_27382);
            _27382 = NOVALUE;
        }
        DeRef(_27382);
        _27382 = NOVALUE;

        /** 				SymTab[target][S_OBJ] = SymTab[source][S_OBJ]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_target_52160 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27385 = (int)*(((s1_ptr)_2)->base + _source_52159);
        _2 = (int)SEQ_PTR(_27385);
        _27386 = (int)*(((s1_ptr)_2)->base + 1);
        _27385 = NOVALUE;
        Ref(_27386);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _27386;
        if( _1 != _27386 ){
            DeRef(_1);
        }
        _27386 = NOVALUE;
        _27383 = NOVALUE;
L1A: 

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 			emit_addr(source)*/
        _43emit_addr(_source_52159);

        /** 			last_op = op*/
        _43last_op_52112 = _op_52153;
L16: 

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 		emit_addr(target)*/
        _43emit_addr(_target_52160);

        /** 		if length(temp) then*/
        if (IS_SEQUENCE(_temp_52180)){
                _27387 = SEQ_PTR(_temp_52180)->length;
        }
        else {
            _27387 = 1;
        }
        if (_27387 == 0)
        {
            _27387 = NOVALUE;
            goto L1B; // [765] 797
        }
        else{
            _27387 = NOVALUE;
        }

        /** 			for i = 1 to length( temp ) do*/
        if (IS_SEQUENCE(_temp_52180)){
                _27388 = SEQ_PTR(_temp_52180)->length;
        }
        else {
            _27388 = 1;
        }
        {
            int _i_52326;
            _i_52326 = 1;
L1C: 
            if (_i_52326 > _27388){
                goto L1D; // [773] 796
            }

            /** 				flush_temp( temp[i] )*/
            _2 = (int)SEQ_PTR(_temp_52180);
            _27389 = (int)*(((s1_ptr)_2)->base + _i_52326);
            Ref(_27389);
            _43flush_temp(_27389);
            _27389 = NOVALUE;

            /** 			end for*/
            _i_52326 = _i_52326 + 1;
            goto L1C; // [791] 780
L1D: 
            ;
        }
L1B: 
        DeRef(_temp_52180);
        _temp_52180 = NOVALUE;
        goto LC; // [799] 7417

        /** 	case RHS_SUBS then*/
        case 25:

        /** 		b = Pop() -- subscript*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		c = Pop() -- sequence*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		target = NewTempSym() -- target*/
        _target_52160 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_target_52160)) {
            _1 = (long)(DBL_PTR(_target_52160)->dbl);
            if (UNIQUE(DBL_PTR(_target_52160)) && (DBL_PTR(_target_52160)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_target_52160);
            _target_52160 = _1;
        }

        /** 		if c < 0 or length(SymTab[c]) < S_VTYPE or SymTab[c][S_VTYPE] < 0 then -- forward reference*/
        _27393 = (_c_52157 < 0);
        if (_27393 != 0) {
            _27394 = 1;
            goto L1E; // [833] 856
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27395 = (int)*(((s1_ptr)_2)->base + _c_52157);
        if (IS_SEQUENCE(_27395)){
                _27396 = SEQ_PTR(_27395)->length;
        }
        else {
            _27396 = 1;
        }
        _27395 = NOVALUE;
        _27397 = (_27396 < 15);
        _27396 = NOVALUE;
        _27394 = (_27397 != 0);
L1E: 
        if (_27394 != 0) {
            goto L1F; // [856] 881
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27399 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27399);
        _27400 = (int)*(((s1_ptr)_2)->base + 15);
        _27399 = NOVALUE;
        if (IS_ATOM_INT(_27400)) {
            _27401 = (_27400 < 0);
        }
        else {
            _27401 = binary_op(LESS, _27400, 0);
        }
        _27400 = NOVALUE;
        if (_27401 == 0) {
            DeRef(_27401);
            _27401 = NOVALUE;
            goto L20; // [877] 893
        }
        else {
            if (!IS_ATOM_INT(_27401) && DBL_PTR(_27401)->dbl == 0.0){
                DeRef(_27401);
                _27401 = NOVALUE;
                goto L20; // [877] 893
            }
            DeRef(_27401);
            _27401 = NOVALUE;
        }
        DeRef(_27401);
        _27401 = NOVALUE;
L1F: 

        /** 			op = RHS_SUBS_CHECK*/
        _op_52153 = 92;
        goto L21; // [890] 1054
L20: 

        /** 		elsif SymTab[c][S_MODE] = M_NORMAL then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27402 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27402);
        _27403 = (int)*(((s1_ptr)_2)->base + 3);
        _27402 = NOVALUE;
        if (binary_op_a(NOTEQ, _27403, 1)){
            _27403 = NOVALUE;
            goto L22; // [909] 996
        }
        _27403 = NOVALUE;

        /** 			if SymTab[c][S_VTYPE] != sequence_type and*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27405 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27405);
        _27406 = (int)*(((s1_ptr)_2)->base + 15);
        _27405 = NOVALUE;
        if (IS_ATOM_INT(_27406)) {
            _27407 = (_27406 != _55sequence_type_47059);
        }
        else {
            _27407 = binary_op(NOTEQ, _27406, _55sequence_type_47059);
        }
        _27406 = NOVALUE;
        if (IS_ATOM_INT(_27407)) {
            if (_27407 == 0) {
                goto L21; // [933] 1054
            }
        }
        else {
            if (DBL_PTR(_27407)->dbl == 0.0) {
                goto L21; // [933] 1054
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27409 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27409);
        _27410 = (int)*(((s1_ptr)_2)->base + 15);
        _27409 = NOVALUE;
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_27410)){
            _27411 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27410)->dbl));
        }
        else{
            _27411 = (int)*(((s1_ptr)_2)->base + _27410);
        }
        _2 = (int)SEQ_PTR(_27411);
        _27412 = (int)*(((s1_ptr)_2)->base + 2);
        _27411 = NOVALUE;
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_27412)){
            _27413 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27412)->dbl));
        }
        else{
            _27413 = (int)*(((s1_ptr)_2)->base + _27412);
        }
        _2 = (int)SEQ_PTR(_27413);
        _27414 = (int)*(((s1_ptr)_2)->base + 15);
        _27413 = NOVALUE;
        if (IS_ATOM_INT(_27414)) {
            _27415 = (_27414 != _55sequence_type_47059);
        }
        else {
            _27415 = binary_op(NOTEQ, _27414, _55sequence_type_47059);
        }
        _27414 = NOVALUE;
        if (_27415 == 0) {
            DeRef(_27415);
            _27415 = NOVALUE;
            goto L21; // [980] 1054
        }
        else {
            if (!IS_ATOM_INT(_27415) && DBL_PTR(_27415)->dbl == 0.0){
                DeRef(_27415);
                _27415 = NOVALUE;
                goto L21; // [980] 1054
            }
            DeRef(_27415);
            _27415 = NOVALUE;
        }
        DeRef(_27415);
        _27415 = NOVALUE;

        /** 				op = RHS_SUBS_CHECK*/
        _op_52153 = 92;
        goto L21; // [993] 1054
L22: 

        /** 		elsif SymTab[c][S_MODE] != M_CONSTANT or*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27416 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27416);
        _27417 = (int)*(((s1_ptr)_2)->base + 3);
        _27416 = NOVALUE;
        if (IS_ATOM_INT(_27417)) {
            _27418 = (_27417 != 2);
        }
        else {
            _27418 = binary_op(NOTEQ, _27417, 2);
        }
        _27417 = NOVALUE;
        if (IS_ATOM_INT(_27418)) {
            if (_27418 != 0) {
                goto L23; // [1016] 1043
            }
        }
        else {
            if (DBL_PTR(_27418)->dbl != 0.0) {
                goto L23; // [1016] 1043
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27420 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27420);
        _27421 = (int)*(((s1_ptr)_2)->base + 1);
        _27420 = NOVALUE;
        _27422 = IS_SEQUENCE(_27421);
        _27421 = NOVALUE;
        _27423 = (_27422 == 0);
        _27422 = NOVALUE;
        if (_27423 == 0)
        {
            DeRef(_27423);
            _27423 = NOVALUE;
            goto L24; // [1039] 1053
        }
        else{
            DeRef(_27423);
            _27423 = NOVALUE;
        }
L23: 

        /** 			op = RHS_SUBS_CHECK*/
        _op_52153 = 92;
L24: 
L21: 

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 		Push(target)*/
        _43Push(_target_52160);

        /** 		emit_addr(target)*/
        _43emit_addr(_target_52160);

        /** 		emit_temp(target, NEW_REFERENCE)*/
        _43emit_temp(_target_52160, 1);

        /** 		current_sequence = append(current_sequence, target)*/
        Append(&_43current_sequence_51226, _43current_sequence_51226, _target_52160);

        /** 		flush_temp( Code[$-2] )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27425 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27425 = 1;
        }
        _27426 = _27425 - 2;
        _27425 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27427 = (int)*(((s1_ptr)_2)->base + _27426);
        Ref(_27427);
        _43flush_temp(_27427);
        _27427 = NOVALUE;
        goto LC; // [1118] 7417

        /** 	case PROC then -- procedure, function and type calls*/
        case 27:

        /** 		assignable = FALSE -- assume for now*/
        _43assignable_51236 = _9FALSE_426;

        /** 		subsym = op_info1*/
        _subsym_52161 = _43op_info1_51218;

        /** 		n = SymTab[subsym][S_NUM_ARGS]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27428 = (int)*(((s1_ptr)_2)->base + _subsym_52161);
        _2 = (int)SEQ_PTR(_27428);
        if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
            _n_52166 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
        }
        else{
            _n_52166 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
        }
        if (!IS_ATOM_INT(_n_52166)){
            _n_52166 = (long)DBL_PTR(_n_52166)->dbl;
        }
        _27428 = NOVALUE;

        /** 		if subsym = CurrentSub then*/
        if (_subsym_52161 != _38CurrentSub_16954)
        goto L25; // [1160] 1345

        /** 			for i = cgi-n+1 to cgi do*/
        _27431 = _43cgi_51234 - _n_52166;
        if ((long)((unsigned long)_27431 +(unsigned long) HIGH_BITS) >= 0){
            _27431 = NewDouble((double)_27431);
        }
        if (IS_ATOM_INT(_27431)) {
            _27432 = _27431 + 1;
            if (_27432 > MAXINT){
                _27432 = NewDouble((double)_27432);
            }
        }
        else
        _27432 = binary_op(PLUS, 1, _27431);
        DeRef(_27431);
        _27431 = NOVALUE;
        _27433 = _43cgi_51234;
        {
            int _i_52412;
            Ref(_27432);
            _i_52412 = _27432;
L26: 
            if (binary_op_a(GREATER, _i_52412, _27433)){
                goto L27; // [1181] 1344
            }

            /** 				if cg_stack[i] > 0 then -- if it's a forward reference, it's not a private*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412)){
                _27434 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            }
            else{
                _27434 = (int)*(((s1_ptr)_2)->base + _i_52412);
            }
            if (binary_op_a(LESSEQ, _27434, 0)){
                _27434 = NOVALUE;
                goto L28; // [1196] 1337
            }
            _27434 = NOVALUE;

            /** 					if SymTab[cg_stack[i]][S_SCOPE] = SC_PRIVATE and*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412)){
                _27436 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            }
            else{
                _27436 = (int)*(((s1_ptr)_2)->base + _i_52412);
            }
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!IS_ATOM_INT(_27436)){
                _27437 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27436)->dbl));
            }
            else{
                _27437 = (int)*(((s1_ptr)_2)->base + _27436);
            }
            _2 = (int)SEQ_PTR(_27437);
            _27438 = (int)*(((s1_ptr)_2)->base + 4);
            _27437 = NOVALUE;
            if (IS_ATOM_INT(_27438)) {
                _27439 = (_27438 == 3);
            }
            else {
                _27439 = binary_op(EQUALS, _27438, 3);
            }
            _27438 = NOVALUE;
            if (IS_ATOM_INT(_27439)) {
                if (_27439 == 0) {
                    goto L29; // [1226] 1304
                }
            }
            else {
                if (DBL_PTR(_27439)->dbl == 0.0) {
                    goto L29; // [1226] 1304
                }
            }
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412)){
                _27441 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            }
            else{
                _27441 = (int)*(((s1_ptr)_2)->base + _i_52412);
            }
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!IS_ATOM_INT(_27441)){
                _27442 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27441)->dbl));
            }
            else{
                _27442 = (int)*(((s1_ptr)_2)->base + _27441);
            }
            _2 = (int)SEQ_PTR(_27442);
            _27443 = (int)*(((s1_ptr)_2)->base + 16);
            _27442 = NOVALUE;
            if (IS_ATOM_INT(_27443) && IS_ATOM_INT(_i_52412)) {
                _27444 = (_27443 < _i_52412);
            }
            else {
                _27444 = binary_op(LESS, _27443, _i_52412);
            }
            _27443 = NOVALUE;
            if (_27444 == 0) {
                DeRef(_27444);
                _27444 = NOVALUE;
                goto L29; // [1253] 1304
            }
            else {
                if (!IS_ATOM_INT(_27444) && DBL_PTR(_27444)->dbl == 0.0){
                    DeRef(_27444);
                    _27444 = NOVALUE;
                    goto L29; // [1253] 1304
                }
                DeRef(_27444);
                _27444 = NOVALUE;
            }
            DeRef(_27444);
            _27444 = NOVALUE;

            /** 						emit_opcode(ASSIGN)*/
            _43emit_opcode(18);

            /** 						emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412)){
                _27445 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            }
            else{
                _27445 = (int)*(((s1_ptr)_2)->base + _i_52412);
            }
            Ref(_27445);
            _43emit_addr(_27445);
            _27445 = NOVALUE;

            /** 						cg_stack[i] = NewTempSym()*/
            _27446 = _55NewTempSym(0);
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_52412);
            _1 = *(int *)_2;
            *(int *)_2 = _27446;
            if( _1 != _27446 ){
                DeRef(_1);
            }
            _27446 = NOVALUE;

            /** 						emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412)){
                _27447 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            }
            else{
                _27447 = (int)*(((s1_ptr)_2)->base + _i_52412);
            }
            Ref(_27447);
            _43emit_addr(_27447);
            _27447 = NOVALUE;

            /** 						check_for_temps()*/
            _43check_for_temps();
            goto L2A; // [1301] 1336
L29: 

            /** 					elsif sym_mode( cg_stack[i] ) = M_TEMP then*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412)){
                _27448 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            }
            else{
                _27448 = (int)*(((s1_ptr)_2)->base + _i_52412);
            }
            Ref(_27448);
            _27449 = _55sym_mode(_27448);
            _27448 = NOVALUE;
            if (binary_op_a(NOTEQ, _27449, 3)){
                DeRef(_27449);
                _27449 = NOVALUE;
                goto L2B; // [1318] 1335
            }
            DeRef(_27449);
            _27449 = NOVALUE;

            /** 						emit_temp( cg_stack[i], NEW_REFERENCE )*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52412)){
                _27451 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52412)->dbl));
            }
            else{
                _27451 = (int)*(((s1_ptr)_2)->base + _i_52412);
            }
            Ref(_27451);
            _43emit_temp(_27451, 1);
            _27451 = NOVALUE;
L2B: 
L2A: 
L28: 

            /** 			end for*/
            _0 = _i_52412;
            if (IS_ATOM_INT(_i_52412)) {
                _i_52412 = _i_52412 + 1;
                if ((long)((unsigned long)_i_52412 +(unsigned long) HIGH_BITS) >= 0){
                    _i_52412 = NewDouble((double)_i_52412);
                }
            }
            else {
                _i_52412 = binary_op_a(PLUS, _i_52412, 1);
            }
            DeRef(_0);
            goto L26; // [1339] 1188
L27: 
            ;
            DeRef(_i_52412);
        }
L25: 

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(subsym)*/
        _43emit_addr(_subsym_52161);

        /** 		for i = cgi-n+1 to cgi do*/
        _27452 = _43cgi_51234 - _n_52166;
        if ((long)((unsigned long)_27452 +(unsigned long) HIGH_BITS) >= 0){
            _27452 = NewDouble((double)_27452);
        }
        if (IS_ATOM_INT(_27452)) {
            _27453 = _27452 + 1;
            if (_27453 > MAXINT){
                _27453 = NewDouble((double)_27453);
            }
        }
        else
        _27453 = binary_op(PLUS, 1, _27452);
        DeRef(_27452);
        _27452 = NOVALUE;
        _27454 = _43cgi_51234;
        {
            int _i_52447;
            Ref(_27453);
            _i_52447 = _27453;
L2C: 
            if (binary_op_a(GREATER, _i_52447, _27454)){
                goto L2D; // [1372] 1409
            }

            /** 			emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52447)){
                _27455 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52447)->dbl));
            }
            else{
                _27455 = (int)*(((s1_ptr)_2)->base + _i_52447);
            }
            Ref(_27455);
            _43emit_addr(_27455);
            _27455 = NOVALUE;

            /** 			emit_temp( cg_stack[i], NEW_REFERENCE )*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52447)){
                _27456 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52447)->dbl));
            }
            else{
                _27456 = (int)*(((s1_ptr)_2)->base + _i_52447);
            }
            Ref(_27456);
            _43emit_temp(_27456, 1);
            _27456 = NOVALUE;

            /** 		end for*/
            _0 = _i_52447;
            if (IS_ATOM_INT(_i_52447)) {
                _i_52447 = _i_52447 + 1;
                if ((long)((unsigned long)_i_52447 +(unsigned long) HIGH_BITS) >= 0){
                    _i_52447 = NewDouble((double)_i_52447);
                }
            }
            else {
                _i_52447 = binary_op_a(PLUS, _i_52447, 1);
            }
            DeRef(_0);
            goto L2C; // [1404] 1379
L2D: 
            ;
            DeRef(_i_52447);
        }

        /** 		cgi -= n*/
        _43cgi_51234 = _43cgi_51234 - _n_52166;

        /** 		if SymTab[subsym][S_TOKEN] != PROC then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27458 = (int)*(((s1_ptr)_2)->base + _subsym_52161);
        _2 = (int)SEQ_PTR(_27458);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _27459 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _27459 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _27458 = NOVALUE;
        if (binary_op_a(EQUALS, _27459, 27)){
            _27459 = NOVALUE;
            goto LC; // [1433] 7417
        }
        _27459 = NOVALUE;

        /** 			assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 			c = NewTempSym() -- put final result in temp*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);

        /** 			Push(c)*/
        _43Push(_c_52157);

        /** 			emit_addr(c)*/
        _43emit_addr(_c_52157);
        goto LC; // [1469] 7417

        /** 	case PROC_FORWARD, FUNC_FORWARD then*/
        case 195:
        case 196:

        /** 		assignable = FALSE -- assume for now*/
        _43assignable_51236 = _9FALSE_426;

        /** 		integer real_op*/

        /** 		if op = PROC_FORWARD then*/
        if (_op_52153 != 195)
        goto L2E; // [1490] 1506

        /** 			real_op = PROC*/
        _real_op_52468 = 27;
        goto L2F; // [1503] 1516
L2E: 

        /** 			real_op = FUNC*/
        _real_op_52468 = 501;
L2F: 

        /** 		integer ref*/

        /** 		ref = new_forward_reference( real_op, op_info1, real_op )*/
        _ref_52475 = _40new_forward_reference(_real_op_52468, _43op_info1_51218, _real_op_52468);
        if (!IS_ATOM_INT(_ref_52475)) {
            _1 = (long)(DBL_PTR(_ref_52475)->dbl);
            if (UNIQUE(DBL_PTR(_ref_52475)) && (DBL_PTR(_ref_52475)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ref_52475);
            _ref_52475 = _1;
        }

        /** 		n = Pop() -- number of known args*/
        _n_52166 = _43Pop();
        if (!IS_ATOM_INT(_n_52166)) {
            _1 = (long)(DBL_PTR(_n_52166)->dbl);
            if (UNIQUE(DBL_PTR(_n_52166)) && (DBL_PTR(_n_52166)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_n_52166);
            _n_52166 = _1;
        }

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(ref)*/
        _43emit_addr(_ref_52475);

        /** 		emit_addr( n ) -- this changes to be the "next" instruction*/
        _43emit_addr(_n_52166);

        /** 		for i = cgi-n+1 to cgi do*/
        _27465 = _43cgi_51234 - _n_52166;
        if ((long)((unsigned long)_27465 +(unsigned long) HIGH_BITS) >= 0){
            _27465 = NewDouble((double)_27465);
        }
        if (IS_ATOM_INT(_27465)) {
            _27466 = _27465 + 1;
            if (_27466 > MAXINT){
                _27466 = NewDouble((double)_27466);
            }
        }
        else
        _27466 = binary_op(PLUS, 1, _27465);
        DeRef(_27465);
        _27465 = NOVALUE;
        _27467 = _43cgi_51234;
        {
            int _i_52480;
            Ref(_27466);
            _i_52480 = _27466;
L30: 
            if (binary_op_a(GREATER, _i_52480, _27467)){
                goto L31; // [1571] 1608
            }

            /** 			emit_addr(cg_stack[i])*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52480)){
                _27468 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52480)->dbl));
            }
            else{
                _27468 = (int)*(((s1_ptr)_2)->base + _i_52480);
            }
            Ref(_27468);
            _43emit_addr(_27468);
            _27468 = NOVALUE;

            /** 			emit_temp( cg_stack[i], NEW_REFERENCE )*/
            _2 = (int)SEQ_PTR(_43cg_stack_51233);
            if (!IS_ATOM_INT(_i_52480)){
                _27469 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_52480)->dbl));
            }
            else{
                _27469 = (int)*(((s1_ptr)_2)->base + _i_52480);
            }
            Ref(_27469);
            _43emit_temp(_27469, 1);
            _27469 = NOVALUE;

            /** 		end for*/
            _0 = _i_52480;
            if (IS_ATOM_INT(_i_52480)) {
                _i_52480 = _i_52480 + 1;
                if ((long)((unsigned long)_i_52480 +(unsigned long) HIGH_BITS) >= 0){
                    _i_52480 = NewDouble((double)_i_52480);
                }
            }
            else {
                _i_52480 = binary_op_a(PLUS, _i_52480, 1);
            }
            DeRef(_0);
            goto L30; // [1603] 1578
L31: 
            ;
            DeRef(_i_52480);
        }

        /** 		cgi -= n*/
        _43cgi_51234 = _43cgi_51234 - _n_52166;

        /** 		if op != PROC_FORWARD then*/
        if (_op_52153 == 195)
        goto L32; // [1620] 1656

        /** 			assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 			c = NewTempSym() -- put final result in temp*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 			Push(c)*/
        _43Push(_c_52157);

        /** 			emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);
L32: 
        goto LC; // [1658] 7417

        /** 	case WARNING then*/
        case 506:

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 	    a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		Warning(SymTab[a][S_OBJ], custom_warning_flag,"")*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27474 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27474);
        _27475 = (int)*(((s1_ptr)_2)->base + 1);
        _27474 = NOVALUE;
        Ref(_27475);
        RefDS(_22663);
        _46Warning(_27475, 64, _22663);
        _27475 = NOVALUE;
        goto LC; // [1699] 7417

        /** 	case INCLUDE_PATHS then*/
        case 507:

        /** 		sequence paths*/

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 	    a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 	    emit_opcode(RIGHT_BRACE_N)*/
        _43emit_opcode(31);

        /** 	    paths = Include_paths(SymTab[a][S_OBJ])*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27477 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27477);
        _27478 = (int)*(((s1_ptr)_2)->base + 1);
        _27477 = NOVALUE;
        Ref(_27478);
        _0 = _paths_52505;
        _paths_52505 = _44Include_paths(_27478);
        DeRef(_0);
        _27478 = NOVALUE;

        /** 	    emit(length(paths))*/
        if (IS_SEQUENCE(_paths_52505)){
                _27480 = SEQ_PTR(_paths_52505)->length;
        }
        else {
            _27480 = 1;
        }
        _43emit(_27480);
        _27480 = NOVALUE;

        /** 	    for i=length(paths) to 1 by -1 do*/
        if (IS_SEQUENCE(_paths_52505)){
                _27481 = SEQ_PTR(_paths_52505)->length;
        }
        else {
            _27481 = 1;
        }
        {
            int _i_52517;
            _i_52517 = _27481;
L33: 
            if (_i_52517 < 1){
                goto L34; // [1761] 1792
            }

            /** 	        c = NewStringSym(paths[i])*/
            _2 = (int)SEQ_PTR(_paths_52505);
            _27482 = (int)*(((s1_ptr)_2)->base + _i_52517);
            Ref(_27482);
            _c_52157 = _55NewStringSym(_27482);
            _27482 = NOVALUE;
            if (!IS_ATOM_INT(_c_52157)) {
                _1 = (long)(DBL_PTR(_c_52157)->dbl);
                if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_c_52157);
                _c_52157 = _1;
            }

            /** 	        emit_addr(c)*/
            _43emit_addr(_c_52157);

            /** 	    end for*/
            _i_52517 = _i_52517 + -1;
            goto L33; // [1787] 1768
L34: 
            ;
        }

        /** 	    b = NewTempSym()*/
        _b_52156 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		emit_temp(b, NEW_REFERENCE)*/
        _43emit_temp(_b_52156, 1);

        /** 	    Push(b)*/
        _43Push(_b_52156);

        /** 	    emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		last_op = RIGHT_BRACE_N*/
        _43last_op_52112 = 31;

        /** 		op = last_op*/
        _op_52153 = 31;
        DeRef(_paths_52505);
        _paths_52505 = NOVALUE;
        goto LC; // [1834] 7417

        /** 	case NOP1, NOP2, NOPWHILE, PRIVATE_INIT_CHECK, GLOBAL_INIT_CHECK,*/
        case 159:
        case 110:
        case 158:
        case 30:
        case 109:
        case 58:
        case 59:
        case 61:
        case 184:
        case 22:
        case 23:
        case 188:
        case 189:
        case 88:
        case 43:
        case 90:
        case 89:
        case 87:
        case 135:
        case 156:
        case 169:
        case 175:
        case 174:
        case 187:
        case 210:
        case 211:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 		if op = UPDATE_GLOBALS then*/
        if (_op_52153 != 89)
        goto LC; // [1906] 7417

        /** 			last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto LC; // [1921] 7417

        /** 	case IF, WHILE then*/
        case 20:
        case 47:

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 		if previous_op >= LESS and previous_op <= NOT then*/
        _27487 = (_38previous_op_17062 >= 1);
        if (_27487 == 0) {
            goto L35; // [1953] 2245
        }
        _27489 = (_38previous_op_17062 <= 7);
        if (_27489 == 0)
        {
            DeRef(_27489);
            _27489 = NOVALUE;
            goto L35; // [1966] 2245
        }
        else{
            DeRef(_27489);
            _27489 = NOVALUE;
        }

        /** 			clear_temp( Code[$] )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27490 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27490 = 1;
        }
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27491 = (int)*(((s1_ptr)_2)->base + _27490);
        Ref(_27491);
        _43clear_temp(_27491);
        _27491 = NOVALUE;

        /** 			Code = Code[1..$-1]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27492 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27492 = 1;
        }
        _27493 = _27492 - 1;
        _27492 = NOVALUE;
        rhs_slice_target = (object_ptr)&_38Code_17038;
        RHS_Slice(_38Code_17038, 1, _27493);

        /** 			if previous_op = NOT then*/
        if (_38previous_op_17062 != 7)
        goto L36; // [2007] 2087

        /** 				op = NOT_IFW*/
        _op_52153 = 108;

        /** 				backpatch(length(Code) - 1, op)*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27496 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27496 = 1;
        }
        _27497 = _27496 - 1;
        _27496 = NOVALUE;
        _43backpatch(_27497, 108);
        _27497 = NOVALUE;

        /** 				sequence if_code = Code[$-1..$]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27498 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27498 = 1;
        }
        _27499 = _27498 - 1;
        _27498 = NOVALUE;
        if (IS_SEQUENCE(_38Code_17038)){
                _27500 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27500 = 1;
        }
        rhs_slice_target = (object_ptr)&_if_code_52585;
        RHS_Slice(_38Code_17038, _27499, _27500);

        /** 				Code = Code[1..$-2]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27502 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27502 = 1;
        }
        _27503 = _27502 - 2;
        _27502 = NOVALUE;
        rhs_slice_target = (object_ptr)&_38Code_17038;
        RHS_Slice(_38Code_17038, 1, _27503);

        /** 				Code &= if_code*/
        Concat((object_ptr)&_38Code_17038, _38Code_17038, _if_code_52585);
        DeRefDS(_if_code_52585);
        _if_code_52585 = NOVALUE;
        goto L37; // [2084] 2232
L36: 

        /** 				if IsInteger(Code[$-1]) and IsInteger(Code[$]) then*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27506 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27506 = 1;
        }
        _27507 = _27506 - 1;
        _27506 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27508 = (int)*(((s1_ptr)_2)->base + _27507);
        Ref(_27508);
        _27509 = _43IsInteger(_27508);
        _27508 = NOVALUE;
        if (IS_ATOM_INT(_27509)) {
            if (_27509 == 0) {
                goto L38; // [2106] 2148
            }
        }
        else {
            if (DBL_PTR(_27509)->dbl == 0.0) {
                goto L38; // [2106] 2148
            }
        }
        if (IS_SEQUENCE(_38Code_17038)){
                _27511 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27511 = 1;
        }
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27512 = (int)*(((s1_ptr)_2)->base + _27511);
        Ref(_27512);
        _27513 = _43IsInteger(_27512);
        _27512 = NOVALUE;
        if (_27513 == 0) {
            DeRef(_27513);
            _27513 = NOVALUE;
            goto L38; // [2124] 2148
        }
        else {
            if (!IS_ATOM_INT(_27513) && DBL_PTR(_27513)->dbl == 0.0){
                DeRef(_27513);
                _27513 = NOVALUE;
                goto L38; // [2124] 2148
            }
            DeRef(_27513);
            _27513 = NOVALUE;
        }
        DeRef(_27513);
        _27513 = NOVALUE;

        /** 					op = previous_op + LESS_IFW_I - LESS*/
        _27514 = _38previous_op_17062 + 119;
        if ((long)((unsigned long)_27514 + (unsigned long)HIGH_BITS) >= 0) 
        _27514 = NewDouble((double)_27514);
        if (IS_ATOM_INT(_27514)) {
            _op_52153 = _27514 - 1;
        }
        else {
            _op_52153 = NewDouble(DBL_PTR(_27514)->dbl - (double)1);
        }
        DeRef(_27514);
        _27514 = NOVALUE;
        if (!IS_ATOM_INT(_op_52153)) {
            _1 = (long)(DBL_PTR(_op_52153)->dbl);
            if (UNIQUE(DBL_PTR(_op_52153)) && (DBL_PTR(_op_52153)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_op_52153);
            _op_52153 = _1;
        }
        goto L39; // [2145] 2167
L38: 

        /** 					op = previous_op + LESS_IFW - LESS*/
        _27516 = _38previous_op_17062 + 102;
        if ((long)((unsigned long)_27516 + (unsigned long)HIGH_BITS) >= 0) 
        _27516 = NewDouble((double)_27516);
        if (IS_ATOM_INT(_27516)) {
            _op_52153 = _27516 - 1;
        }
        else {
            _op_52153 = NewDouble(DBL_PTR(_27516)->dbl - (double)1);
        }
        DeRef(_27516);
        _27516 = NOVALUE;
        if (!IS_ATOM_INT(_op_52153)) {
            _1 = (long)(DBL_PTR(_op_52153)->dbl);
            if (UNIQUE(DBL_PTR(_op_52153)) && (DBL_PTR(_op_52153)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_op_52153);
            _op_52153 = _1;
        }
L39: 

        /** 				backpatch(length(Code) - 2, op)*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27518 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27518 = 1;
        }
        _27519 = _27518 - 2;
        _27518 = NOVALUE;
        _43backpatch(_27519, _op_52153);
        _27519 = NOVALUE;

        /** 				sequence if_code = Code[$-2..$]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27520 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27520 = 1;
        }
        _27521 = _27520 - 2;
        _27520 = NOVALUE;
        if (IS_SEQUENCE(_38Code_17038)){
                _27522 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27522 = 1;
        }
        rhs_slice_target = (object_ptr)&_if_code_52624;
        RHS_Slice(_38Code_17038, _27521, _27522);

        /** 				Code = Code[1..$-3]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27524 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27524 = 1;
        }
        _27525 = _27524 - 3;
        _27524 = NOVALUE;
        rhs_slice_target = (object_ptr)&_38Code_17038;
        RHS_Slice(_38Code_17038, 1, _27525);

        /** 				Code &= if_code*/
        Concat((object_ptr)&_38Code_17038, _38Code_17038, _if_code_52624);
        DeRefDS(_if_code_52624);
        _if_code_52624 = NOVALUE;
L37: 

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;

        /** 			last_op = op*/
        _43last_op_52112 = _op_52153;
        goto LC; // [2242] 7417
L35: 

        /** 		elsif op = WHILE and*/
        _27528 = (_op_52153 == 47);
        if (_27528 == 0) {
            _27529 = 0;
            goto L3A; // [2253] 2265
        }
        _27530 = (_a_52155 > 0);
        _27529 = (_27530 != 0);
L3A: 
        if (_27529 == 0) {
            _27531 = 0;
            goto L3B; // [2265] 2291
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27532 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27532);
        _27533 = (int)*(((s1_ptr)_2)->base + 3);
        _27532 = NOVALUE;
        if (IS_ATOM_INT(_27533)) {
            _27534 = (_27533 == 2);
        }
        else {
            _27534 = binary_op(EQUALS, _27533, 2);
        }
        _27533 = NOVALUE;
        if (IS_ATOM_INT(_27534))
        _27531 = (_27534 != 0);
        else
        _27531 = DBL_PTR(_27534)->dbl != 0.0;
L3B: 
        if (_27531 == 0) {
            _27535 = 0;
            goto L3C; // [2291] 2314
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27536 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27536);
        _27537 = (int)*(((s1_ptr)_2)->base + 1);
        _27536 = NOVALUE;
        if (IS_ATOM_INT(_27537))
        _27538 = 1;
        else if (IS_ATOM_DBL(_27537))
        _27538 = IS_ATOM_INT(DoubleToInt(_27537));
        else
        _27538 = 0;
        _27537 = NOVALUE;
        _27535 = (_27538 != 0);
L3C: 
        if (_27535 == 0) {
            goto L3D; // [2314] 2363
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27540 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27540);
        _27541 = (int)*(((s1_ptr)_2)->base + 1);
        _27540 = NOVALUE;
        if (_27541 == 0)
        _27542 = 1;
        else if (IS_ATOM_INT(_27541) && IS_ATOM_INT(0))
        _27542 = 0;
        else
        _27542 = (compare(_27541, 0) == 0);
        _27541 = NOVALUE;
        _27543 = (_27542 == 0);
        _27542 = NOVALUE;
        if (_27543 == 0)
        {
            DeRef(_27543);
            _27543 = NOVALUE;
            goto L3D; // [2338] 2363
        }
        else{
            DeRef(_27543);
            _27543 = NOVALUE;
        }

        /** 			optimized_while = TRUE   -- while TRUE ... emit nothing*/
        _43optimized_while_51220 = _9TRUE_428;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;

        /** 			last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;
        goto LC; // [2360] 7417
L3D: 

        /** 			flush_temps( {a} )*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _a_52155;
        _27544 = MAKE_SEQ(_1);
        _43flush_temps(_27544);
        _27544 = NOVALUE;

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 			emit_addr(a)*/
        _43emit_addr(_a_52155);
        goto LC; // [2383] 7417

        /** 	case INTEGER_CHECK then*/
        case 96:

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 		if previous_op = ASSIGN then*/
        if (_38previous_op_17062 != 18)
        goto L3E; // [2402] 2461

        /** 			c = Code[$-1]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27546 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27546 = 1;
        }
        _27547 = _27546 - 1;
        _27546 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _c_52157 = (int)*(((s1_ptr)_2)->base + _27547);
        if (!IS_ATOM_INT(_c_52157)){
            _c_52157 = (long)DBL_PTR(_c_52157)->dbl;
        }

        /** 			if not IsInteger(c) then*/
        _27549 = _43IsInteger(_c_52157);
        if (IS_ATOM_INT(_27549)) {
            if (_27549 != 0){
                DeRef(_27549);
                _27549 = NOVALUE;
                goto L3F; // [2429] 2447
            }
        }
        else {
            if (DBL_PTR(_27549)->dbl != 0.0){
                DeRef(_27549);
                _27549 = NOVALUE;
                goto L3F; // [2429] 2447
            }
        }
        DeRef(_27549);
        _27549 = NOVALUE;

        /** 				emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 				emit_addr(op_info1)*/
        _43emit_addr(_43op_info1_51218);
        goto L40; // [2444] 2518
L3F: 

        /** 				last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 				last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto L40; // [2458] 2518
L3E: 

        /** 		elsif previous_op = -1 or*/
        _27551 = (_38previous_op_17062 == -1);
        if (_27551 != 0) {
            goto L41; // [2469] 2492
        }
        _2 = (int)SEQ_PTR(_43op_result_51834);
        _27553 = (int)*(((s1_ptr)_2)->base + _38previous_op_17062);
        _27554 = (_27553 != 1);
        _27553 = NOVALUE;
        if (_27554 == 0)
        {
            DeRef(_27554);
            _27554 = NOVALUE;
            goto L42; // [2488] 2507
        }
        else{
            DeRef(_27554);
            _27554 = NOVALUE;
        }
L41: 

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 			emit_addr(op_info1)*/
        _43emit_addr(_43op_info1_51218);
        goto L40; // [2504] 2518
L42: 

        /** 			last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
L40: 

        /** 		clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27555 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27555 = 1;
        }
        _27556 = _27555 - 1;
        _27555 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27557 = (int)*(((s1_ptr)_2)->base + _27556);
        Ref(_27557);
        _43clear_temp(_27557);
        _27557 = NOVALUE;
        goto LC; // [2536] 7417

        /** 	case SEQUENCE_CHECK then*/
        case 97:

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 		if previous_op = ASSIGN then*/
        if (_38previous_op_17062 != 18)
        goto L43; // [2555] 2682

        /** 			c = Code[$-1]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27559 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27559 = 1;
        }
        _27560 = _27559 - 1;
        _27559 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _c_52157 = (int)*(((s1_ptr)_2)->base + _27560);
        if (!IS_ATOM_INT(_c_52157)){
            _c_52157 = (long)DBL_PTR(_c_52157)->dbl;
        }

        /** 			if c < 1 or*/
        _27562 = (_c_52157 < 1);
        if (_27562 != 0) {
            _27563 = 1;
            goto L44; // [2582] 2608
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27564 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27564);
        _27565 = (int)*(((s1_ptr)_2)->base + 3);
        _27564 = NOVALUE;
        if (IS_ATOM_INT(_27565)) {
            _27566 = (_27565 != 2);
        }
        else {
            _27566 = binary_op(NOTEQ, _27565, 2);
        }
        _27565 = NOVALUE;
        if (IS_ATOM_INT(_27566))
        _27563 = (_27566 != 0);
        else
        _27563 = DBL_PTR(_27566)->dbl != 0.0;
L44: 
        if (_27563 != 0) {
            goto L45; // [2608] 2635
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27568 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27568);
        _27569 = (int)*(((s1_ptr)_2)->base + 1);
        _27568 = NOVALUE;
        _27570 = IS_SEQUENCE(_27569);
        _27569 = NOVALUE;
        _27571 = (_27570 == 0);
        _27570 = NOVALUE;
        if (_27571 == 0)
        {
            DeRef(_27571);
            _27571 = NOVALUE;
            goto L46; // [2631] 2668
        }
        else{
            DeRef(_27571);
            _27571 = NOVALUE;
        }
L45: 

        /** 				emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 				emit_addr(op_info1)*/
        _43emit_addr(_43op_info1_51218);

        /** 				clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27572 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27572 = 1;
        }
        _27573 = _27572 - 1;
        _27572 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27574 = (int)*(((s1_ptr)_2)->base + _27573);
        Ref(_27574);
        _43clear_temp(_27574);
        _27574 = NOVALUE;
        goto LC; // [2665] 7417
L46: 

        /** 				last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 				last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto LC; // [2679] 7417
L43: 

        /** 		elsif previous_op = -1 or op_result[previous_op] != T_SEQUENCE then*/
        _27575 = (_38previous_op_17062 == -1);
        if (_27575 != 0) {
            goto L47; // [2690] 2713
        }
        _2 = (int)SEQ_PTR(_43op_result_51834);
        _27577 = (int)*(((s1_ptr)_2)->base + _38previous_op_17062);
        _27578 = (_27577 != 2);
        _27577 = NOVALUE;
        if (_27578 == 0)
        {
            DeRef(_27578);
            _27578 = NOVALUE;
            goto L48; // [2709] 2746
        }
        else{
            DeRef(_27578);
            _27578 = NOVALUE;
        }
L47: 

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 			emit_addr(op_info1)*/
        _43emit_addr(_43op_info1_51218);

        /** 			clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27579 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27579 = 1;
        }
        _27580 = _27579 - 1;
        _27579 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27581 = (int)*(((s1_ptr)_2)->base + _27580);
        Ref(_27581);
        _43clear_temp(_27581);
        _27581 = NOVALUE;
        goto LC; // [2743] 7417
L48: 

        /** 			last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto LC; // [2757] 7417

        /** 	case ATOM_CHECK then*/
        case 101:

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 		if previous_op = ASSIGN then*/
        if (_38previous_op_17062 != 18)
        goto L49; // [2776] 2975

        /** 			c = Code[$-1]*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27583 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27583 = 1;
        }
        _27584 = _27583 - 1;
        _27583 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _c_52157 = (int)*(((s1_ptr)_2)->base + _27584);
        if (!IS_ATOM_INT(_c_52157)){
            _c_52157 = (long)DBL_PTR(_c_52157)->dbl;
        }

        /** 			if c > 1*/
        _27586 = (_c_52157 > 1);
        if (_27586 == 0) {
            goto L4A; // [2803] 2924
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27588 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27588);
        _27589 = (int)*(((s1_ptr)_2)->base + 3);
        _27588 = NOVALUE;
        if (IS_ATOM_INT(_27589)) {
            _27590 = (_27589 == 2);
        }
        else {
            _27590 = binary_op(EQUALS, _27589, 2);
        }
        _27589 = NOVALUE;
        if (_27590 == 0) {
            DeRef(_27590);
            _27590 = NOVALUE;
            goto L4A; // [2826] 2924
        }
        else {
            if (!IS_ATOM_INT(_27590) && DBL_PTR(_27590)->dbl == 0.0){
                DeRef(_27590);
                _27590 = NOVALUE;
                goto L4A; // [2826] 2924
            }
            DeRef(_27590);
            _27590 = NOVALUE;
        }
        DeRef(_27590);
        _27590 = NOVALUE;

        /** 				if sequence( SymTab[c][S_OBJ] ) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27591 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27591);
        _27592 = (int)*(((s1_ptr)_2)->base + 1);
        _27591 = NOVALUE;
        _27593 = IS_SEQUENCE(_27592);
        _27592 = NOVALUE;
        if (_27593 == 0)
        {
            _27593 = NOVALUE;
            goto L4B; // [2846] 2875
        }
        else{
            _27593 = NOVALUE;
        }

        /** 					ThisLine = ExprLine*/
        RefDS(_41ExprLine_57182);
        DeRef(_46ThisLine_49482);
        _46ThisLine_49482 = _41ExprLine_57182;

        /** 					bp = expr_bp*/
        _46bp_49486 = _41expr_bp_57183;

        /** 					CompileErr( 346 )*/
        RefDS(_22663);
        _46CompileErr(346, _22663, 0);
        goto L4C; // [2872] 3054
L4B: 

        /** 				elsif SymTab[c][S_OBJ] = NOVALUE then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27594 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27594);
        _27595 = (int)*(((s1_ptr)_2)->base + 1);
        _27594 = NOVALUE;
        if (binary_op_a(NOTEQ, _27595, _38NOVALUE_16800)){
            _27595 = NOVALUE;
            goto L4D; // [2891] 2910
        }
        _27595 = NOVALUE;

        /** 					emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 					emit_addr(op_info1)*/
        _43emit_addr(_43op_info1_51218);
        goto L4C; // [2907] 3054
L4D: 

        /** 					last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 					last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto L4C; // [2921] 3054
L4A: 

        /** 			elsif c < 1 */
        _27597 = (_c_52157 < 1);
        if (_27597 != 0) {
            goto L4E; // [2930] 2946
        }
        _27599 = _43IsInteger(_c_52157);
        if (IS_ATOM_INT(_27599)) {
            _27600 = (_27599 == 0);
        }
        else {
            _27600 = unary_op(NOT, _27599);
        }
        DeRef(_27599);
        _27599 = NOVALUE;
        if (_27600 == 0) {
            DeRef(_27600);
            _27600 = NOVALUE;
            goto L4F; // [2942] 2961
        }
        else {
            if (!IS_ATOM_INT(_27600) && DBL_PTR(_27600)->dbl == 0.0){
                DeRef(_27600);
                _27600 = NOVALUE;
                goto L4F; // [2942] 2961
            }
            DeRef(_27600);
            _27600 = NOVALUE;
        }
        DeRef(_27600);
        _27600 = NOVALUE;
L4E: 

        /** 				emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 				emit_addr(op_info1)*/
        _43emit_addr(_43op_info1_51218);
        goto L4C; // [2958] 3054
L4F: 

        /** 				last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 				last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto L4C; // [2972] 3054
L49: 

        /** 		elsif previous_op = -1 or*/
        _27601 = (_38previous_op_17062 == -1);
        if (_27601 != 0) {
            goto L50; // [2983] 3028
        }
        _2 = (int)SEQ_PTR(_43op_result_51834);
        _27603 = (int)*(((s1_ptr)_2)->base + _38previous_op_17062);
        _27604 = (_27603 != 1);
        _27603 = NOVALUE;
        if (_27604 == 0) {
            DeRef(_27605);
            _27605 = 0;
            goto L51; // [3001] 3023
        }
        _2 = (int)SEQ_PTR(_43op_result_51834);
        _27606 = (int)*(((s1_ptr)_2)->base + _38previous_op_17062);
        _27607 = (_27606 != 3);
        _27606 = NOVALUE;
        _27605 = (_27607 != 0);
L51: 
        if (_27605 == 0)
        {
            _27605 = NOVALUE;
            goto L52; // [3024] 3043
        }
        else{
            _27605 = NOVALUE;
        }
L50: 

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 			emit_addr(op_info1)*/
        _43emit_addr(_43op_info1_51218);
        goto L4C; // [3040] 3054
L52: 

        /** 			last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
L4C: 

        /** 		clear_temp( Code[$-1] )*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27608 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27608 = 1;
        }
        _27609 = _27608 - 1;
        _27608 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27610 = (int)*(((s1_ptr)_2)->base + _27609);
        Ref(_27610);
        _43clear_temp(_27610);
        _27610 = NOVALUE;
        goto LC; // [3072] 7417

        /** 	case RIGHT_BRACE_N then*/
        case 31:

        /** 		n = op_info1*/
        _n_52166 = _43op_info1_51218;

        /** 		elements = {}*/
        RefDS(_22663);
        DeRef(_elements_52168);
        _elements_52168 = _22663;

        /** 		for i = 1 to n do*/
        _27611 = _n_52166;
        {
            int _i_52804;
            _i_52804 = 1;
L53: 
            if (_i_52804 > _27611){
                goto L54; // [3097] 3120
            }

            /** 			elements = append(elements, Pop())*/
            _27612 = _43Pop();
            Ref(_27612);
            Append(&_elements_52168, _elements_52168, _27612);
            DeRef(_27612);
            _27612 = NOVALUE;

            /** 		end for*/
            _i_52804 = _i_52804 + 1;
            goto L53; // [3115] 3104
L54: 
            ;
        }

        /** 		element_vals = good_string(elements)*/
        RefDS(_elements_52168);
        _0 = _element_vals_52169;
        _element_vals_52169 = _43good_string(_elements_52168);
        DeRef(_0);

        /** 		if sequence(element_vals) then*/
        _27615 = IS_SEQUENCE(_element_vals_52169);
        if (_27615 == 0)
        {
            _27615 = NOVALUE;
            goto L55; // [3131] 3162
        }
        else{
            _27615 = NOVALUE;
        }

        /** 			c = NewStringSym(element_vals)  -- make a string literal*/
        Ref(_element_vals_52169);
        _c_52157 = _55NewStringSym(_element_vals_52169);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 			assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 			last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto L56; // [3159] 3253
L55: 

        /** 			if n = 2 then*/
        if (_n_52166 != 2)
        goto L57; // [3164] 3187

        /** 				emit_opcode(RIGHT_BRACE_2) -- faster op for two items*/
        _43emit_opcode(85);

        /** 				last_op = RIGHT_BRACE_2*/
        _43last_op_52112 = 85;
        goto L58; // [3184] 3198
L57: 

        /** 				emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 				emit(n)*/
        _43emit(_n_52166);
L58: 

        /** 			for i = 1 to n do*/
        _27618 = _n_52166;
        {
            int _i_52821;
            _i_52821 = 1;
L59: 
            if (_i_52821 > _27618){
                goto L5A; // [3203] 3226
            }

            /** 				emit_addr(elements[i])*/
            _2 = (int)SEQ_PTR(_elements_52168);
            _27619 = (int)*(((s1_ptr)_2)->base + _i_52821);
            Ref(_27619);
            _43emit_addr(_27619);
            _27619 = NOVALUE;

            /** 			end for*/
            _i_52821 = _i_52821 + 1;
            goto L59; // [3221] 3210
L5A: 
            ;
        }

        /** 			c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 			emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);

        /** 			assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;
L56: 

        /** 		Push(c)*/
        _43Push(_c_52157);
        goto LC; // [3258] 7417

        /** 	case ASSIGN_SUBS2,      -- can't change the op*/
        case 148:
        case 16:
        case 162:

        /** 		b = Pop() -- rhs value*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		a = Pop() -- subscript*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		c = Pop() -- sequence*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		if op = ASSIGN_SUBS then*/
        if (_op_52153 != 16)
        goto L5B; // [3293] 3485

        /** 			if (previous_op != LHS_SUBS) and*/
        _27625 = (_38previous_op_17062 != 95);
        if (_27625 == 0) {
            _27626 = 0;
            goto L5C; // [3307] 3319
        }
        _27627 = (_c_52157 > 0);
        _27626 = (_27627 != 0);
L5C: 
        if (_27626 == 0) {
            goto L5D; // [3319] 3457
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27629 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27629);
        _27630 = (int)*(((s1_ptr)_2)->base + 3);
        _27629 = NOVALUE;
        if (IS_ATOM_INT(_27630)) {
            _27631 = (_27630 != 1);
        }
        else {
            _27631 = binary_op(NOTEQ, _27630, 1);
        }
        _27630 = NOVALUE;
        if (IS_ATOM_INT(_27631)) {
            if (_27631 != 0) {
                DeRef(_27632);
                _27632 = 1;
                goto L5E; // [3341] 3441
            }
        }
        else {
            if (DBL_PTR(_27631)->dbl != 0.0) {
                DeRef(_27632);
                _27632 = 1;
                goto L5E; // [3341] 3441
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27633 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27633);
        _27634 = (int)*(((s1_ptr)_2)->base + 15);
        _27633 = NOVALUE;
        if (IS_ATOM_INT(_27634)) {
            _27635 = (_27634 != _55sequence_type_47059);
        }
        else {
            _27635 = binary_op(NOTEQ, _27634, _55sequence_type_47059);
        }
        _27634 = NOVALUE;
        if (IS_ATOM_INT(_27635)) {
            if (_27635 == 0) {
                DeRef(_27636);
                _27636 = 0;
                goto L5F; // [3363] 3437
            }
        }
        else {
            if (DBL_PTR(_27635)->dbl == 0.0) {
                DeRef(_27636);
                _27636 = 0;
                goto L5F; // [3363] 3437
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27637 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27637);
        _27638 = (int)*(((s1_ptr)_2)->base + 15);
        _27637 = NOVALUE;
        if (IS_ATOM_INT(_27638)) {
            _27639 = (_27638 > 0);
        }
        else {
            _27639 = binary_op(GREATER, _27638, 0);
        }
        _27638 = NOVALUE;
        if (IS_ATOM_INT(_27639)) {
            if (_27639 == 0) {
                DeRef(_27640);
                _27640 = 0;
                goto L60; // [3383] 3433
            }
        }
        else {
            if (DBL_PTR(_27639)->dbl == 0.0) {
                DeRef(_27640);
                _27640 = 0;
                goto L60; // [3383] 3433
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27641 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27641);
        _27642 = (int)*(((s1_ptr)_2)->base + 15);
        _27641 = NOVALUE;
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_27642)){
            _27643 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27642)->dbl));
        }
        else{
            _27643 = (int)*(((s1_ptr)_2)->base + _27642);
        }
        _2 = (int)SEQ_PTR(_27643);
        _27644 = (int)*(((s1_ptr)_2)->base + 2);
        _27643 = NOVALUE;
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_27644)){
            _27645 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27644)->dbl));
        }
        else{
            _27645 = (int)*(((s1_ptr)_2)->base + _27644);
        }
        _2 = (int)SEQ_PTR(_27645);
        _27646 = (int)*(((s1_ptr)_2)->base + 15);
        _27645 = NOVALUE;
        if (IS_ATOM_INT(_27646)) {
            _27647 = (_27646 != _55sequence_type_47059);
        }
        else {
            _27647 = binary_op(NOTEQ, _27646, _55sequence_type_47059);
        }
        _27646 = NOVALUE;
        DeRef(_27640);
        if (IS_ATOM_INT(_27647))
        _27640 = (_27647 != 0);
        else
        _27640 = DBL_PTR(_27647)->dbl != 0.0;
L60: 
        DeRef(_27636);
        _27636 = (_27640 != 0);
L5F: 
        DeRef(_27632);
        _27632 = (_27636 != 0);
L5E: 
        if (_27632 == 0)
        {
            _27632 = NOVALUE;
            goto L5D; // [3442] 3457
        }
        else{
            _27632 = NOVALUE;
        }

        /** 				op = ASSIGN_SUBS_CHECK*/
        _op_52153 = 84;
        goto L61; // [3454] 3477
L5D: 

        /** 				if IsInteger(b) then*/
        _27648 = _43IsInteger(_b_52156);
        if (_27648 == 0) {
            DeRef(_27648);
            _27648 = NOVALUE;
            goto L62; // [3463] 3476
        }
        else {
            if (!IS_ATOM_INT(_27648) && DBL_PTR(_27648)->dbl == 0.0){
                DeRef(_27648);
                _27648 = NOVALUE;
                goto L62; // [3463] 3476
            }
            DeRef(_27648);
            _27648 = NOVALUE;
        }
        DeRef(_27648);
        _27648 = NOVALUE;

        /** 					op = ASSIGN_SUBS_I*/
        _op_52153 = 118;
L62: 
L61: 

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);
        goto L63; // [3482] 3511
L5B: 

        /** 		elsif op = PASSIGN_SUBS then*/
        if (_op_52153 != 162)
        goto L64; // [3489] 3503

        /** 			emit_opcode(PASSIGN_SUBS) -- always*/
        _43emit_opcode(162);
        goto L63; // [3500] 3511
L64: 

        /** 			emit_opcode(ASSIGN_SUBS) -- always*/
        _43emit_opcode(16);
L63: 

        /** 		emit_addr(c) -- sequence*/
        _43emit_addr(_c_52157);

        /** 		emit_addr(a) -- subscript*/
        _43emit_addr(_a_52155);

        /** 		emit_addr(b) -- rhs value*/
        _43emit_addr(_b_52156);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [3533] 7417

        /** 	case LHS_SUBS, LHS_SUBS1, LHS_SUBS1_COPY then*/
        case 95:
        case 161:
        case 166:

        /** 		a = Pop() -- subs*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		lhs_var = Pop() -- sequence*/
        _lhs_var_52163 = _43Pop();
        if (!IS_ATOM_INT(_lhs_var_52163)) {
            _1 = (long)(DBL_PTR(_lhs_var_52163)->dbl);
            if (UNIQUE(DBL_PTR(_lhs_var_52163)) && (DBL_PTR(_lhs_var_52163)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_lhs_var_52163);
            _lhs_var_52163 = _1;
        }

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(lhs_var)*/
        _43emit_addr(_lhs_var_52163);

        /** 		emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 		if op = LHS_SUBS then*/
        if (_op_52153 != 95)
        goto L65; // [3576] 3607

        /** 			TempKeep(lhs_var) -- should be lhs_target_temp*/
        _43TempKeep(_lhs_var_52163);

        /** 			emit_addr(lhs_target_temp)*/
        _43emit_addr(_43lhs_target_temp_51232);

        /** 			Push(lhs_target_temp)*/
        _43Push(_43lhs_target_temp_51232);

        /** 			emit_addr(0) -- place holder*/
        _43emit_addr(0);
        goto L66; // [3604] 3661
L65: 

        /** 			lhs_target_temp = NewTempSym() -- use same temp for all subscripts*/
        _0 = _55NewTempSym(0);
        _43lhs_target_temp_51232 = _0;
        if (!IS_ATOM_INT(_43lhs_target_temp_51232)) {
            _1 = (long)(DBL_PTR(_43lhs_target_temp_51232)->dbl);
            if (UNIQUE(DBL_PTR(_43lhs_target_temp_51232)) && (DBL_PTR(_43lhs_target_temp_51232)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_43lhs_target_temp_51232);
            _43lhs_target_temp_51232 = _1;
        }

        /** 			emit_addr(lhs_target_temp) -- target temp holds pointer to sequence*/
        _43emit_addr(_43lhs_target_temp_51232);

        /** 			emit_temp(lhs_target_temp, NEW_REFERENCE )*/
        _43emit_temp(_43lhs_target_temp_51232, 1);

        /** 			Push(lhs_target_temp)*/
        _43Push(_43lhs_target_temp_51232);

        /** 			lhs_subs1_copy_temp = NewTempSym() -- place to copy (may be ignored)*/
        _0 = _55NewTempSym(0);
        _43lhs_subs1_copy_temp_51231 = _0;
        if (!IS_ATOM_INT(_43lhs_subs1_copy_temp_51231)) {
            _1 = (long)(DBL_PTR(_43lhs_subs1_copy_temp_51231)->dbl);
            if (UNIQUE(DBL_PTR(_43lhs_subs1_copy_temp_51231)) && (DBL_PTR(_43lhs_subs1_copy_temp_51231)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_43lhs_subs1_copy_temp_51231);
            _43lhs_subs1_copy_temp_51231 = _1;
        }

        /** 			emit_addr(lhs_subs1_copy_temp)*/
        _43emit_addr(_43lhs_subs1_copy_temp_51231);

        /** 			emit_temp( lhs_subs1_copy_temp, NEW_REFERENCE )*/
        _43emit_temp(_43lhs_subs1_copy_temp_51231, 1);
L66: 

        /** 		current_sequence = append(current_sequence, lhs_target_temp)*/
        Append(&_43current_sequence_51226, _43current_sequence_51226, _43lhs_target_temp_51232);

        /** 		assignable = FALSE  -- need to update current_sequence like in RHS_SUBS*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [3678] 7417

        /** 	case RAND, PEEK, PEEK4S, PEEK4U, NOT_BITS, NOT,*/
        case 62:
        case 127:
        case 139:
        case 140:
        case 51:
        case 7:
        case 173:
        case 180:
        case 179:
        case 181:
        case 182:

        /** 		cont11ii(op, TRUE)*/
        _43cont11ii(_op_52153, _9TRUE_428);
        goto LC; // [3712] 7417

        /** 	case UMINUS then*/
        case 12:

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		if a > 0 then*/
        if (_a_52155 <= 0)
        goto L67; // [3727] 3973

        /** 			obj = SymTab[a][S_OBJ]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27658 = (int)*(((s1_ptr)_2)->base + _a_52155);
        DeRef(_obj_52167);
        _2 = (int)SEQ_PTR(_27658);
        _obj_52167 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_obj_52167);
        _27658 = NOVALUE;

        /** 			if SymTab[a][S_MODE] = M_CONSTANT then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27660 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27660);
        _27661 = (int)*(((s1_ptr)_2)->base + 3);
        _27660 = NOVALUE;
        if (binary_op_a(NOTEQ, _27661, 2)){
            _27661 = NOVALUE;
            goto L68; // [3761] 3885
        }
        _27661 = NOVALUE;

        /** 				if integer(obj) then*/
        if (IS_ATOM_INT(_obj_52167))
        _27663 = 1;
        else if (IS_ATOM_DBL(_obj_52167))
        _27663 = IS_ATOM_INT(DoubleToInt(_obj_52167));
        else
        _27663 = 0;
        if (_27663 == 0)
        {
            _27663 = NOVALUE;
            goto L69; // [3770] 3824
        }
        else{
            _27663 = NOVALUE;
        }

        /** 					if obj = MININT then*/
        if (binary_op_a(NOTEQ, _obj_52167, -1073741824)){
            goto L6A; // [3777] 3798
        }

        /** 						Push(NewDoubleSym(-MININT))*/
        if ((unsigned long)-1073741824 == 0xC0000000)
        _27665 = (int)NewDouble((double)-0xC0000000);
        else
        _27665 = - -1073741824;
        _27666 = _55NewDoubleSym(_27665);
        _27665 = NOVALUE;
        _43Push(_27666);
        _27666 = NOVALUE;
        goto L6B; // [3795] 3811
L6A: 

        /** 						Push(NewIntSym(-obj))*/
        if (IS_ATOM_INT(_obj_52167)) {
            if ((unsigned long)_obj_52167 == 0xC0000000)
            _27667 = (int)NewDouble((double)-0xC0000000);
            else
            _27667 = - _obj_52167;
        }
        else {
            _27667 = unary_op(UMINUS, _obj_52167);
        }
        _27668 = _55NewIntSym(_27667);
        _27667 = NOVALUE;
        _43Push(_27668);
        _27668 = NOVALUE;
L6B: 

        /** 					last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;

        /** 					last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;
        goto LC; // [3821] 7417
L69: 

        /** 				elsif atom(obj) and obj != NOVALUE then*/
        _27669 = IS_ATOM(_obj_52167);
        if (_27669 == 0) {
            goto L6C; // [3829] 3868
        }
        if (IS_ATOM_INT(_obj_52167) && IS_ATOM_INT(_38NOVALUE_16800)) {
            _27671 = (_obj_52167 != _38NOVALUE_16800);
        }
        else {
            _27671 = binary_op(NOTEQ, _obj_52167, _38NOVALUE_16800);
        }
        if (_27671 == 0) {
            DeRef(_27671);
            _27671 = NOVALUE;
            goto L6C; // [3840] 3868
        }
        else {
            if (!IS_ATOM_INT(_27671) && DBL_PTR(_27671)->dbl == 0.0){
                DeRef(_27671);
                _27671 = NOVALUE;
                goto L6C; // [3840] 3868
            }
            DeRef(_27671);
            _27671 = NOVALUE;
        }
        DeRef(_27671);
        _27671 = NOVALUE;

        /** 					Push(NewDoubleSym(-obj))*/
        if (IS_ATOM_INT(_obj_52167)) {
            if ((unsigned long)_obj_52167 == 0xC0000000)
            _27672 = (int)NewDouble((double)-0xC0000000);
            else
            _27672 = - _obj_52167;
        }
        else {
            _27672 = unary_op(UMINUS, _obj_52167);
        }
        _27673 = _55NewDoubleSym(_27672);
        _27672 = NOVALUE;
        _43Push(_27673);
        _27673 = NOVALUE;

        /** 					last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;

        /** 					last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;
        goto LC; // [3865] 7417
L6C: 

        /** 					Push(a)*/
        _43Push(_a_52155);

        /** 					cont11ii(op, FALSE)*/
        _43cont11ii(_op_52153, _9FALSE_426);
        goto LC; // [3882] 7417
L68: 

        /** 			elsif TRANSLATE and SymTab[a][S_MODE] = M_TEMP and*/
        if (_38TRANSLATE_16564 == 0) {
            _27674 = 0;
            goto L6D; // [3889] 3915
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27675 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27675);
        _27676 = (int)*(((s1_ptr)_2)->base + 3);
        _27675 = NOVALUE;
        if (IS_ATOM_INT(_27676)) {
            _27677 = (_27676 == 3);
        }
        else {
            _27677 = binary_op(EQUALS, _27676, 3);
        }
        _27676 = NOVALUE;
        if (IS_ATOM_INT(_27677))
        _27674 = (_27677 != 0);
        else
        _27674 = DBL_PTR(_27677)->dbl != 0.0;
L6D: 
        if (_27674 == 0) {
            goto L6E; // [3915] 3956
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27679 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27679);
        _27680 = (int)*(((s1_ptr)_2)->base + 36);
        _27679 = NOVALUE;
        if (IS_ATOM_INT(_27680)) {
            _27681 = (_27680 == 2);
        }
        else {
            _27681 = binary_op(EQUALS, _27680, 2);
        }
        _27680 = NOVALUE;
        if (_27681 == 0) {
            DeRef(_27681);
            _27681 = NOVALUE;
            goto L6E; // [3938] 3956
        }
        else {
            if (!IS_ATOM_INT(_27681) && DBL_PTR(_27681)->dbl == 0.0){
                DeRef(_27681);
                _27681 = NOVALUE;
                goto L6E; // [3938] 3956
            }
            DeRef(_27681);
            _27681 = NOVALUE;
        }
        DeRef(_27681);
        _27681 = NOVALUE;

        /** 				Push(NewDoubleSym(-obj))*/
        if (IS_ATOM_INT(_obj_52167)) {
            if ((unsigned long)_obj_52167 == 0xC0000000)
            _27682 = (int)NewDouble((double)-0xC0000000);
            else
            _27682 = - _obj_52167;
        }
        else {
            _27682 = unary_op(UMINUS, _obj_52167);
        }
        _27683 = _55NewDoubleSym(_27682);
        _27682 = NOVALUE;
        _43Push(_27683);
        _27683 = NOVALUE;
        goto LC; // [3953] 7417
L6E: 

        /** 				Push(a)*/
        _43Push(_a_52155);

        /** 				cont11ii(op, FALSE)*/
        _43cont11ii(_op_52153, _9FALSE_426);
        goto LC; // [3970] 7417
L67: 

        /** 			Push(a)*/
        _43Push(_a_52155);

        /** 			cont11ii(op, FALSE)*/
        _43cont11ii(_op_52153, _9FALSE_426);
        goto LC; // [3987] 7417

        /** 	case LENGTH, GETC, SQRT, SIN, COS, TAN, ARCTAN, LOG, GETS, GETENV then*/
        case 42:
        case 33:
        case 41:
        case 80:
        case 81:
        case 82:
        case 73:
        case 74:
        case 17:
        case 91:

        /** 		cont11ii(op, FALSE)*/
        _43cont11ii(_op_52153, _9FALSE_426);
        goto LC; // [4019] 7417

        /** 	case IS_AN_INTEGER, IS_AN_ATOM, IS_A_SEQUENCE, IS_AN_OBJECT then*/
        case 94:
        case 67:
        case 68:
        case 40:

        /** 		cont11ii(op, FALSE)*/
        _43cont11ii(_op_52153, _9FALSE_426);
        goto LC; // [4039] 7417

        /** 	case ROUTINE_ID then*/
        case 134:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		source = Pop()*/
        _source_52159 = _43Pop();
        if (!IS_ATOM_INT(_source_52159)) {
            _1 = (long)(DBL_PTR(_source_52159)->dbl);
            if (UNIQUE(DBL_PTR(_source_52159)) && (DBL_PTR(_source_52159)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_source_52159);
            _source_52159 = _1;
        }

        /** 		if TRANSLATE then*/
        if (_38TRANSLATE_16564 == 0)
        {
            goto L6F; // [4061] 4105
        }
        else{
        }

        /** 			emit_addr(num_routines-1)*/
        _27685 = _38num_routines_16955 - 1;
        if ((long)((unsigned long)_27685 +(unsigned long) HIGH_BITS) >= 0){
            _27685 = NewDouble((double)_27685);
        }
        _43emit_addr(_27685);
        _27685 = NOVALUE;

        /** 			last_routine_id = num_routines*/
        _43last_routine_id_51223 = _38num_routines_16955;

        /** 			last_max_params = max_params*/
        _43last_max_params_51225 = _43max_params_51224;

        /** 			MarkTargets(source, S_RI_TARGET)*/
        _32365 = _55MarkTargets(_source_52159, 53);
        DeRef(_32365);
        _32365 = NOVALUE;
        goto L70; // [4102] 4142
L6F: 

        /** 			emit_addr(CurrentSub)*/
        _43emit_addr(_38CurrentSub_16954);

        /** 			emit_addr(length(SymTab))*/
        if (IS_SEQUENCE(_35SymTab_15595)){
                _27686 = SEQ_PTR(_35SymTab_15595)->length;
        }
        else {
            _27686 = 1;
        }
        _43emit_addr(_27686);
        _27686 = NOVALUE;

        /** 			if BIND then*/
        if (_38BIND_16567 == 0)
        {
            goto L71; // [4126] 4141
        }
        else{
        }

        /** 				MarkTargets(source, S_NREFS)*/
        _32364 = _55MarkTargets(_source_52159, 12);
        DeRef(_32364);
        _32364 = NOVALUE;
L71: 
L70: 

        /** 		emit_addr(source)*/
        _43emit_addr(_source_52159);

        /** 		emit_addr(current_file_no)  -- necessary at top level*/
        _43emit_addr(_38current_file_no_16946);

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		TempInteger(c) -- result will always be an integer*/
        _43TempInteger(_c_52157);

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);
        goto LC; // [4184] 7417

        /** 	case SC1_OR, SC1_AND then*/
        case 143:
        case 141:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(Pop())*/
        _27688 = _43Pop();
        _43emit_addr(_27688);
        _27688 = NOVALUE;

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [4230] 7417

        /** 	case SYSTEM, PUTS, PRINT, QPRINT, POSITION, MACHINE_PROC,*/
        case 99:
        case 44:
        case 19:
        case 36:
        case 60:
        case 112:
        case 132:
        case 128:
        case 138:
        case 168:
        case 178:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27691 = _43Pop();
        _43emit_addr(_27691);
        _27691 = NOVALUE;

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		if op = C_PROC then*/
        if (_op_52153 != 132)
        goto L72; // [4285] 4297

        /** 			emit_addr(CurrentSub)*/
        _43emit_addr(_38CurrentSub_16954);
L72: 

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [4304] 7417

        /** 	case EQUALS, LESS, GREATER, NOTEQ, LESSEQ, GREATEREQ,*/
        case 3:
        case 1:
        case 6:
        case 4:
        case 5:
        case 2:
        case 8:
        case 9:
        case 152:
        case 71:
        case 56:
        case 24:
        case 26:

        /** 		cont21ii(op, TRUE)  -- both integer args => integer result*/
        _43cont21ii(_op_52153, _9TRUE_428);
        goto LC; // [4342] 7417

        /** 	case PLUS then -- elsif op = PLUS then*/
        case 11:

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		if b < 1 or a < 1 then*/
        _27695 = (_b_52156 < 1);
        if (_27695 != 0) {
            goto L73; // [4368] 4381
        }
        _27697 = (_a_52155 < 1);
        if (_27697 == 0)
        {
            DeRef(_27697);
            _27697 = NOVALUE;
            goto L74; // [4377] 4402
        }
        else{
            DeRef(_27697);
            _27697 = NOVALUE;
        }
L73: 

        /** 			Push(a)*/
        _43Push(_a_52155);

        /** 			Push(b)*/
        _43Push(_b_52156);

        /** 			cont21ii(op, FALSE)*/
        _43cont21ii(_op_52153, _9FALSE_426);
        goto LC; // [4399] 7417
L74: 

        /** 		elsif SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 1) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27698 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27698);
        _27699 = (int)*(((s1_ptr)_2)->base + 3);
        _27698 = NOVALUE;
        if (IS_ATOM_INT(_27699)) {
            _27700 = (_27699 == 2);
        }
        else {
            _27700 = binary_op(EQUALS, _27699, 2);
        }
        _27699 = NOVALUE;
        if (IS_ATOM_INT(_27700)) {
            if (_27700 == 0) {
                goto L75; // [4422] 4483
            }
        }
        else {
            if (DBL_PTR(_27700)->dbl == 0.0) {
                goto L75; // [4422] 4483
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27702 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27702);
        _27703 = (int)*(((s1_ptr)_2)->base + 1);
        _27702 = NOVALUE;
        if (_27703 == 1)
        _27704 = 1;
        else if (IS_ATOM_INT(_27703) && IS_ATOM_INT(1))
        _27704 = 0;
        else
        _27704 = (compare(_27703, 1) == 0);
        _27703 = NOVALUE;
        if (_27704 == 0)
        {
            _27704 = NOVALUE;
            goto L75; // [4443] 4483
        }
        else{
            _27704 = NOVALUE;
        }

        /** 			op = PLUS1*/
        _op_52153 = 93;

        /** 			emit_opcode(op)*/
        _43emit_opcode(93);

        /** 			emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 			emit_addr(0)*/
        _43emit_addr(0);

        /** 			cont21d(op, a, b, FALSE)*/
        _43cont21d(93, _a_52155, _b_52156, _9FALSE_426);
        goto LC; // [4480] 7417
L75: 

        /** 		elsif SymTab[a][S_MODE] = M_CONSTANT and equal(SymTab[a][S_OBJ], 1) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27705 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27705);
        _27706 = (int)*(((s1_ptr)_2)->base + 3);
        _27705 = NOVALUE;
        if (IS_ATOM_INT(_27706)) {
            _27707 = (_27706 == 2);
        }
        else {
            _27707 = binary_op(EQUALS, _27706, 2);
        }
        _27706 = NOVALUE;
        if (IS_ATOM_INT(_27707)) {
            if (_27707 == 0) {
                goto L76; // [4503] 4564
            }
        }
        else {
            if (DBL_PTR(_27707)->dbl == 0.0) {
                goto L76; // [4503] 4564
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27709 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27709);
        _27710 = (int)*(((s1_ptr)_2)->base + 1);
        _27709 = NOVALUE;
        if (_27710 == 1)
        _27711 = 1;
        else if (IS_ATOM_INT(_27710) && IS_ATOM_INT(1))
        _27711 = 0;
        else
        _27711 = (compare(_27710, 1) == 0);
        _27710 = NOVALUE;
        if (_27711 == 0)
        {
            _27711 = NOVALUE;
            goto L76; // [4524] 4564
        }
        else{
            _27711 = NOVALUE;
        }

        /** 			op = PLUS1*/
        _op_52153 = 93;

        /** 			emit_opcode(op)*/
        _43emit_opcode(93);

        /** 			emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 			emit_addr(0)*/
        _43emit_addr(0);

        /** 			cont21d(op, a, b, FALSE)*/
        _43cont21d(93, _a_52155, _b_52156, _9FALSE_426);
        goto LC; // [4561] 7417
L76: 

        /** 			Push(a)*/
        _43Push(_a_52155);

        /** 			Push(b)*/
        _43Push(_b_52156);

        /** 			cont21ii(op, FALSE)*/
        _43cont21ii(_op_52153, _9FALSE_426);
        goto LC; // [4583] 7417

        /** 	case rw:MULTIPLY then*/
        case 13:

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		if a < 1 or b < 1 then*/
        _27714 = (_a_52155 < 1);
        if (_27714 != 0) {
            goto L77; // [4609] 4622
        }
        _27716 = (_b_52156 < 1);
        if (_27716 == 0)
        {
            DeRef(_27716);
            _27716 = NOVALUE;
            goto L78; // [4618] 4643
        }
        else{
            DeRef(_27716);
            _27716 = NOVALUE;
        }
L77: 

        /** 			Push(a)*/
        _43Push(_a_52155);

        /** 			Push(b)*/
        _43Push(_b_52156);

        /** 			cont21ii(op, FALSE)*/
        _43cont21ii(_op_52153, _9FALSE_426);
        goto LC; // [4640] 7417
L78: 

        /** 		elsif SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 2) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27717 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27717);
        _27718 = (int)*(((s1_ptr)_2)->base + 3);
        _27717 = NOVALUE;
        if (IS_ATOM_INT(_27718)) {
            _27719 = (_27718 == 2);
        }
        else {
            _27719 = binary_op(EQUALS, _27718, 2);
        }
        _27718 = NOVALUE;
        if (IS_ATOM_INT(_27719)) {
            if (_27719 == 0) {
                goto L79; // [4663] 4724
            }
        }
        else {
            if (DBL_PTR(_27719)->dbl == 0.0) {
                goto L79; // [4663] 4724
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27721 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27721);
        _27722 = (int)*(((s1_ptr)_2)->base + 1);
        _27721 = NOVALUE;
        if (_27722 == 2)
        _27723 = 1;
        else if (IS_ATOM_INT(_27722) && IS_ATOM_INT(2))
        _27723 = 0;
        else
        _27723 = (compare(_27722, 2) == 0);
        _27722 = NOVALUE;
        if (_27723 == 0)
        {
            _27723 = NOVALUE;
            goto L79; // [4684] 4724
        }
        else{
            _27723 = NOVALUE;
        }

        /** 			op = PLUS*/
        _op_52153 = 11;

        /** 			emit_opcode(op)*/
        _43emit_opcode(11);

        /** 			emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 			emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 			cont21d(op, a, b, FALSE)*/
        _43cont21d(11, _a_52155, _b_52156, _9FALSE_426);
        goto LC; // [4721] 7417
L79: 

        /** 		elsif SymTab[a][S_MODE] = M_CONSTANT and equal(SymTab[a][S_OBJ], 2) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27724 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27724);
        _27725 = (int)*(((s1_ptr)_2)->base + 3);
        _27724 = NOVALUE;
        if (IS_ATOM_INT(_27725)) {
            _27726 = (_27725 == 2);
        }
        else {
            _27726 = binary_op(EQUALS, _27725, 2);
        }
        _27725 = NOVALUE;
        if (IS_ATOM_INT(_27726)) {
            if (_27726 == 0) {
                goto L7A; // [4744] 4805
            }
        }
        else {
            if (DBL_PTR(_27726)->dbl == 0.0) {
                goto L7A; // [4744] 4805
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27728 = (int)*(((s1_ptr)_2)->base + _a_52155);
        _2 = (int)SEQ_PTR(_27728);
        _27729 = (int)*(((s1_ptr)_2)->base + 1);
        _27728 = NOVALUE;
        if (_27729 == 2)
        _27730 = 1;
        else if (IS_ATOM_INT(_27729) && IS_ATOM_INT(2))
        _27730 = 0;
        else
        _27730 = (compare(_27729, 2) == 0);
        _27729 = NOVALUE;
        if (_27730 == 0)
        {
            _27730 = NOVALUE;
            goto L7A; // [4765] 4805
        }
        else{
            _27730 = NOVALUE;
        }

        /** 			op = PLUS*/
        _op_52153 = 11;

        /** 			emit_opcode(op)*/
        _43emit_opcode(11);

        /** 			emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 			emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 			cont21d(op, a, b, FALSE)*/
        _43cont21d(11, _a_52155, _b_52156, _9FALSE_426);
        goto LC; // [4802] 7417
L7A: 

        /** 			Push(a)*/
        _43Push(_a_52155);

        /** 			Push(b)*/
        _43Push(_b_52156);

        /** 			cont21ii(op, FALSE)*/
        _43cont21ii(_op_52153, _9FALSE_426);
        goto LC; // [4824] 7417

        /** 	case rw:DIVIDE then*/
        case 14:

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		if b > 0 and SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 2) then*/
        _27732 = (_b_52156 > 0);
        if (_27732 == 0) {
            _27733 = 0;
            goto L7B; // [4843] 4869
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27734 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27734);
        _27735 = (int)*(((s1_ptr)_2)->base + 3);
        _27734 = NOVALUE;
        if (IS_ATOM_INT(_27735)) {
            _27736 = (_27735 == 2);
        }
        else {
            _27736 = binary_op(EQUALS, _27735, 2);
        }
        _27735 = NOVALUE;
        if (IS_ATOM_INT(_27736))
        _27733 = (_27736 != 0);
        else
        _27733 = DBL_PTR(_27736)->dbl != 0.0;
L7B: 
        if (_27733 == 0) {
            goto L7C; // [4869] 4940
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27738 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27738);
        _27739 = (int)*(((s1_ptr)_2)->base + 1);
        _27738 = NOVALUE;
        if (_27739 == 2)
        _27740 = 1;
        else if (IS_ATOM_INT(_27739) && IS_ATOM_INT(2))
        _27740 = 0;
        else
        _27740 = (compare(_27739, 2) == 0);
        _27739 = NOVALUE;
        if (_27740 == 0)
        {
            _27740 = NOVALUE;
            goto L7C; // [4890] 4940
        }
        else{
            _27740 = NOVALUE;
        }

        /** 			op = DIV2*/
        _op_52153 = 98;

        /** 			emit_opcode(op)*/
        _43emit_opcode(98);

        /** 			emit_addr(Pop()) -- n.b. "a" hasn't been set*/
        _27741 = _43Pop();
        _43emit_addr(_27741);
        _27741 = NOVALUE;

        /** 			a = 0*/
        _a_52155 = 0;

        /** 			emit_addr(0)*/
        _43emit_addr(0);

        /** 			cont21d(op, a, b, FALSE)  -- could have fractional result*/
        _43cont21d(98, 0, _b_52156, _9FALSE_426);
        goto LC; // [4937] 7417
L7C: 

        /** 			Push(b)*/
        _43Push(_b_52156);

        /** 			cont21ii(op, FALSE)*/
        _43cont21ii(_op_52153, _9FALSE_426);
        goto LC; // [4954] 7417

        /** 	case FLOOR then*/
        case 83:

        /** 		if previous_op = rw:DIVIDE then*/
        if (_38previous_op_17062 != 14)
        goto L7D; // [4964] 5012

        /** 			op = FLOOR_DIV*/
        _op_52153 = 63;

        /** 			backpatch(length(Code) - 3, op)*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27743 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27743 = 1;
        }
        _27744 = _27743 - 3;
        _27743 = NOVALUE;
        _43backpatch(_27744, 63);
        _27744 = NOVALUE;

        /** 			assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 			last_op = op*/
        _43last_op_52112 = 63;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto LC; // [5009] 7417
L7D: 

        /** 		elsif previous_op = DIV2 then*/
        if (_38previous_op_17062 != 98)
        goto L7E; // [5018] 5105

        /** 			op = FLOOR_DIV2*/
        _op_52153 = 66;

        /** 			backpatch(length(Code) - 3, op)*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27746 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27746 = 1;
        }
        _27747 = _27746 - 3;
        _27746 = NOVALUE;
        _43backpatch(_27747, 66);
        _27747 = NOVALUE;

        /** 			assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 			if IsInteger(Code[$-2]) then*/
        if (IS_SEQUENCE(_38Code_17038)){
                _27748 = SEQ_PTR(_38Code_17038)->length;
        }
        else {
            _27748 = 1;
        }
        _27749 = _27748 - 2;
        _27748 = NOVALUE;
        _2 = (int)SEQ_PTR(_38Code_17038);
        _27750 = (int)*(((s1_ptr)_2)->base + _27749);
        Ref(_27750);
        _27751 = _43IsInteger(_27750);
        _27750 = NOVALUE;
        if (_27751 == 0) {
            DeRef(_27751);
            _27751 = NOVALUE;
            goto L7F; // [5072] 5092
        }
        else {
            if (!IS_ATOM_INT(_27751) && DBL_PTR(_27751)->dbl == 0.0){
                DeRef(_27751);
                _27751 = NOVALUE;
                goto L7F; // [5072] 5092
            }
            DeRef(_27751);
            _27751 = NOVALUE;
        }
        DeRef(_27751);
        _27751 = NOVALUE;

        /** 				TempInteger(Top()) --mark temp as integer type*/

        /** 	return cg_stack[cgi]*/
        DeRef(_Top_inlined_Top_at_5200_53191);
        _2 = (int)SEQ_PTR(_43cg_stack_51233);
        _Top_inlined_Top_at_5200_53191 = (int)*(((s1_ptr)_2)->base + _43cgi_51234);
        Ref(_Top_inlined_Top_at_5200_53191);
        Ref(_Top_inlined_Top_at_5200_53191);
        _43TempInteger(_Top_inlined_Top_at_5200_53191);
L7F: 

        /** 			last_op = op*/
        _43last_op_52112 = _op_52153;

        /** 			last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto LC; // [5102] 7417
L7E: 

        /** 			cont11ii(op, TRUE)*/
        _43cont11ii(_op_52153, _9TRUE_428);
        goto LC; // [5114] 7417

        /** 	case MINUS, rw:APPEND, PREPEND, COMPARE, EQUAL,*/
        case 10:
        case 35:
        case 57:
        case 76:
        case 153:
        case 154:
        case 15:
        case 32:
        case 111:
        case 133:
        case 53:
        case 167:
        case 194:
        case 198:
        case 199:
        case 204:

        /** 		cont21ii(op, FALSE)*/
        _43cont21ii(_op_52153, _9FALSE_426);
        goto LC; // [5158] 7417

        /** 	case SC2_NULL then  -- correct the stack - we aren't emitting anything*/
        case 145:

        /** 		c = Pop()*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		TempKeep(c)*/
        _43TempKeep(_c_52157);

        /** 		b = Pop()  -- remove SC1's temp*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;

        /** 		last_op = last_op_backup*/
        _43last_op_52112 = _last_op_backup_52171;

        /** 		last_pc = last_pc_backup*/
        _43last_pc_52113 = _last_pc_backup_52170;
        goto LC; // [5205] 7417

        /** 	case SC2_AND, SC2_OR then*/
        case 142:
        case 144:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(Pop())*/
        _27754 = _43Pop();
        _43emit_addr(_27754);
        _27754 = NOVALUE;

        /** 		c = Pop()*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		TempKeep(c)*/
        _43TempKeep(_c_52157);

        /** 		emit_addr(c) -- target*/
        _43emit_addr(_c_52157);

        /** 		TempInteger(c)*/
        _43TempInteger(_c_52157);

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [5260] 7417

        /** 	case MEM_COPY, MEM_SET, PRINTF then*/
        case 130:
        case 131:
        case 38:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		c = Pop()*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27758 = _43Pop();
        _43emit_addr(_27758);
        _27758 = NOVALUE;

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [5314] 7417

        /** 	case RHS_SLICE, FIND, MATCH, FIND_FROM, MATCH_FROM, SPLICE, INSERT, REMOVE, OPEN then*/
        case 46:
        case 77:
        case 78:
        case 176:
        case 177:
        case 190:
        case 191:
        case 200:
        case 37:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		c = Pop()*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27761 = _43Pop();
        _43emit_addr(_27761);
        _27761 = NOVALUE;

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		if op = FIND or op = FIND_FROM or op = OPEN then*/
        _27763 = (_op_52153 == 77);
        if (_27763 != 0) {
            _27764 = 1;
            goto L80; // [5389] 5403
        }
        _27765 = (_op_52153 == 176);
        _27764 = (_27765 != 0);
L80: 
        if (_27764 != 0) {
            goto L81; // [5403] 5418
        }
        _27767 = (_op_52153 == 37);
        if (_27767 == 0)
        {
            DeRef(_27767);
            _27767 = NOVALUE;
            goto L82; // [5414] 5426
        }
        else{
            DeRef(_27767);
            _27767 = NOVALUE;
        }
L81: 

        /** 			TempInteger( c )*/
        _43TempInteger(_c_52157);
        goto L83; // [5423] 5433
L82: 

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);
L83: 

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);
        goto LC; // [5450] 7417

        /** 	case CONCAT_N then     -- concatenate 3 or more items*/
        case 157:

        /** 		n = op_info1  -- number of items to concatenate*/
        _n_52166 = _43op_info1_51218;

        /** 		emit_opcode(CONCAT_N)*/
        _43emit_opcode(157);

        /** 		emit(n)*/
        _43emit(_n_52166);

        /** 		for i = 1 to n do*/
        _27768 = _n_52166;
        {
            int _i_53259;
            _i_53259 = 1;
L84: 
            if (_i_53259 > _27768){
                goto L85; // [5480] 5508
            }

            /** 			symtab_index element = Pop()*/
            _element_53262 = _43Pop();
            if (!IS_ATOM_INT(_element_53262)) {
                _1 = (long)(DBL_PTR(_element_53262)->dbl);
                if (UNIQUE(DBL_PTR(_element_53262)) && (DBL_PTR(_element_53262)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_element_53262);
                _element_53262 = _1;
            }

            /** 			emit_addr( element )  -- reverse order*/
            _43emit_addr(_element_53262);

            /** 		end for*/
            _i_53259 = _i_53259 + 1;
            goto L84; // [5503] 5487
L85: 
            ;
        }

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 		Push(c)*/
        _43Push(_c_52157);
        goto LC; // [5539] 7417

        /** 	case FOR then*/
        case 21:

        /** 		c = Pop() -- increment*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		TempKeep(c)*/
        _43TempKeep(_c_52157);

        /** 		ic = IsInteger(c)*/
        _ic_52165 = _43IsInteger(_c_52157);
        if (!IS_ATOM_INT(_ic_52165)) {
            _1 = (long)(DBL_PTR(_ic_52165)->dbl);
            if (UNIQUE(DBL_PTR(_ic_52165)) && (DBL_PTR(_ic_52165)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ic_52165);
            _ic_52165 = _1;
        }

        /** 		if c < 1 or*/
        _27773 = (_c_52157 < 1);
        if (_27773 != 0) {
            goto L86; // [5571] 5650
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27775 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27775);
        _27776 = (int)*(((s1_ptr)_2)->base + 3);
        _27775 = NOVALUE;
        if (IS_ATOM_INT(_27776)) {
            _27777 = (_27776 == 1);
        }
        else {
            _27777 = binary_op(EQUALS, _27776, 1);
        }
        _27776 = NOVALUE;
        if (IS_ATOM_INT(_27777)) {
            if (_27777 == 0) {
                DeRef(_27778);
                _27778 = 0;
                goto L87; // [5593] 5619
            }
        }
        else {
            if (DBL_PTR(_27777)->dbl == 0.0) {
                DeRef(_27778);
                _27778 = 0;
                goto L87; // [5593] 5619
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27779 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27779);
        _27780 = (int)*(((s1_ptr)_2)->base + 4);
        _27779 = NOVALUE;
        if (IS_ATOM_INT(_27780)) {
            _27781 = (_27780 != 2);
        }
        else {
            _27781 = binary_op(NOTEQ, _27780, 2);
        }
        _27780 = NOVALUE;
        DeRef(_27778);
        if (IS_ATOM_INT(_27781))
        _27778 = (_27781 != 0);
        else
        _27778 = DBL_PTR(_27781)->dbl != 0.0;
L87: 
        if (_27778 == 0) {
            DeRef(_27782);
            _27782 = 0;
            goto L88; // [5619] 5645
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27783 = (int)*(((s1_ptr)_2)->base + _c_52157);
        _2 = (int)SEQ_PTR(_27783);
        _27784 = (int)*(((s1_ptr)_2)->base + 4);
        _27783 = NOVALUE;
        if (IS_ATOM_INT(_27784)) {
            _27785 = (_27784 != 4);
        }
        else {
            _27785 = binary_op(NOTEQ, _27784, 4);
        }
        _27784 = NOVALUE;
        if (IS_ATOM_INT(_27785))
        _27782 = (_27785 != 0);
        else
        _27782 = DBL_PTR(_27785)->dbl != 0.0;
L88: 
        if (_27782 == 0)
        {
            _27782 = NOVALUE;
            goto L89; // [5646] 5687
        }
        else{
            _27782 = NOVALUE;
        }
L86: 

        /** 			emit_opcode(ASSIGN)*/
        _43emit_opcode(18);

        /** 			emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 			c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 			if ic then*/
        if (_ic_52165 == 0)
        {
            goto L8A; // [5672] 5681
        }
        else{
        }

        /** 				TempInteger( c )*/
        _43TempInteger(_c_52157);
L8A: 

        /** 			emit_addr(c)*/
        _43emit_addr(_c_52157);
L89: 

        /** 		b = Pop() -- limit*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		TempKeep(b)*/
        _43TempKeep(_b_52156);

        /** 		ib = IsInteger(b)*/
        _ib_52164 = _43IsInteger(_b_52156);
        if (!IS_ATOM_INT(_ib_52164)) {
            _1 = (long)(DBL_PTR(_ib_52164)->dbl);
            if (UNIQUE(DBL_PTR(_ib_52164)) && (DBL_PTR(_ib_52164)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ib_52164);
            _ib_52164 = _1;
        }

        /** 		if b < 1 or*/
        _27789 = (_b_52156 < 1);
        if (_27789 != 0) {
            goto L8B; // [5713] 5792
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27791 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27791);
        _27792 = (int)*(((s1_ptr)_2)->base + 3);
        _27791 = NOVALUE;
        if (IS_ATOM_INT(_27792)) {
            _27793 = (_27792 == 1);
        }
        else {
            _27793 = binary_op(EQUALS, _27792, 1);
        }
        _27792 = NOVALUE;
        if (IS_ATOM_INT(_27793)) {
            if (_27793 == 0) {
                DeRef(_27794);
                _27794 = 0;
                goto L8C; // [5735] 5761
            }
        }
        else {
            if (DBL_PTR(_27793)->dbl == 0.0) {
                DeRef(_27794);
                _27794 = 0;
                goto L8C; // [5735] 5761
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27795 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27795);
        _27796 = (int)*(((s1_ptr)_2)->base + 4);
        _27795 = NOVALUE;
        if (IS_ATOM_INT(_27796)) {
            _27797 = (_27796 != 2);
        }
        else {
            _27797 = binary_op(NOTEQ, _27796, 2);
        }
        _27796 = NOVALUE;
        DeRef(_27794);
        if (IS_ATOM_INT(_27797))
        _27794 = (_27797 != 0);
        else
        _27794 = DBL_PTR(_27797)->dbl != 0.0;
L8C: 
        if (_27794 == 0) {
            DeRef(_27798);
            _27798 = 0;
            goto L8D; // [5761] 5787
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27799 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27799);
        _27800 = (int)*(((s1_ptr)_2)->base + 4);
        _27799 = NOVALUE;
        if (IS_ATOM_INT(_27800)) {
            _27801 = (_27800 != 4);
        }
        else {
            _27801 = binary_op(NOTEQ, _27800, 4);
        }
        _27800 = NOVALUE;
        if (IS_ATOM_INT(_27801))
        _27798 = (_27801 != 0);
        else
        _27798 = DBL_PTR(_27801)->dbl != 0.0;
L8D: 
        if (_27798 == 0)
        {
            _27798 = NOVALUE;
            goto L8E; // [5788] 5829
        }
        else{
            _27798 = NOVALUE;
        }
L8B: 

        /** 			emit_opcode(ASSIGN)*/
        _43emit_opcode(18);

        /** 			emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 			b = NewTempSym()*/
        _b_52156 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 			if ib then*/
        if (_ib_52164 == 0)
        {
            goto L8F; // [5814] 5823
        }
        else{
        }

        /** 				TempInteger( b )*/
        _43TempInteger(_b_52156);
L8F: 

        /** 			emit_addr(b)*/
        _43emit_addr(_b_52156);
L8E: 

        /** 		a = Pop() -- initial value*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		if IsInteger(a) and ib and ic then*/
        _27804 = _43IsInteger(_a_52155);
        if (IS_ATOM_INT(_27804)) {
            if (_27804 == 0) {
                DeRef(_27805);
                _27805 = 0;
                goto L90; // [5842] 5850
            }
        }
        else {
            if (DBL_PTR(_27804)->dbl == 0.0) {
                DeRef(_27805);
                _27805 = 0;
                goto L90; // [5842] 5850
            }
        }
        DeRef(_27805);
        _27805 = (_ib_52164 != 0);
L90: 
        if (_27805 == 0) {
            goto L91; // [5850] 5889
        }
        if (_ic_52165 == 0)
        {
            goto L91; // [5855] 5889
        }
        else{
        }

        /** 			SymTab[op_info1][S_VTYPE] = integer_type*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_43op_info1_51218 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 15);
        _1 = *(int *)_2;
        *(int *)_2 = _55integer_type_47061;
        DeRef(_1);
        _27807 = NOVALUE;

        /** 			op = FOR_I*/
        _op_52153 = 125;
        goto L92; // [5886] 5899
L91: 

        /** 			op = FOR*/
        _op_52153 = 21;
L92: 

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 		emit_addr(CurrentSub) -- in case recursion check is needed*/
        _43emit_addr(_38CurrentSub_16954);

        /** 		Push(b)*/
        _43Push(_b_52156);

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [5943] 7417

        /** 	case ENDFOR_GENERAL, ENDFOR_INT_UP1 then  -- all ENDFORs*/
        case 39:
        case 54:

        /** 		emit_opcode(op) -- will be patched at runtime*/
        _43emit_opcode(_op_52153);

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		emit_addr(op_info2) -- address of top of loop*/
        _43emit_addr(_43op_info2_51219);

        /** 		emit_addr(Pop())    -- limit*/
        _27810 = _43Pop();
        _43emit_addr(_27810);
        _27810 = NOVALUE;

        /** 		emit_addr(op_info1) -- loop var*/
        _43emit_addr(_43op_info1_51218);

        /** 		emit_addr(a)        -- increment - not always used -*/
        _43emit_addr(_a_52155);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [5997] 7417

        /** 	case ASSIGN_OP_SUBS, PASSIGN_OP_SUBS then*/
        case 149:
        case 164:

        /** 		b = Pop()      -- rhs value, keep on stack*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		TempKeep(b)*/
        _43TempKeep(_b_52156);

        /** 		a = Pop()      -- subscript, keep on stack*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		TempKeep(a)*/
        _43TempKeep(_a_52155);

        /** 		c = Pop()      -- lhs sequence, keep on stack*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		TempKeep(c)*/
        _43TempKeep(_c_52157);

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 		d = NewTempSym()*/
        _d_52158 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_d_52158)) {
            _1 = (long)(DBL_PTR(_d_52158)->dbl);
            if (UNIQUE(DBL_PTR(_d_52158)) && (DBL_PTR(_d_52158)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_52158);
            _d_52158 = _1;
        }

        /** 		emit_addr(d)   -- place to store result*/
        _43emit_addr(_d_52158);

        /** 		emit_temp( d, NEW_REFERENCE )*/
        _43emit_temp(_d_52158, 1);

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		Push(a)*/
        _43Push(_a_52155);

        /** 		Push(d)*/
        _43Push(_d_52158);

        /** 		Push(b)*/
        _43Push(_b_52156);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6102] 7417

        /** 	case ASSIGN_SLICE, PASSIGN_SLICE then*/
        case 45:
        case 163:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		b = Pop() -- rhs value*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		a = Pop() -- 2nd subs*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		c = Pop() -- 1st subs*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		emit_addr(Pop()) -- sequence*/
        _27818 = _43Pop();
        _43emit_addr(_27818);
        _27818 = NOVALUE;

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6166] 7417

        /** 	case REPLACE then*/
        case 201:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		b = Pop()  -- source*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		a = Pop()  -- replacement*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		c = Pop()  -- start of replaced slice*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		d = Pop()  -- end of replaced slice*/
        _d_52158 = _43Pop();
        if (!IS_ATOM_INT(_d_52158)) {
            _1 = (long)(DBL_PTR(_d_52158)->dbl);
            if (UNIQUE(DBL_PTR(_d_52158)) && (DBL_PTR(_d_52158)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_52158);
            _d_52158 = _1;
        }

        /** 		emit_addr(d)*/
        _43emit_addr(_d_52158);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)     -- place to store result*/
        _43emit_addr(_c_52157);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;
        goto LC; // [6256] 7417

        /** 	case ASSIGN_OP_SLICE, PASSIGN_OP_SLICE then*/
        case 150:
        case 165:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		b = Pop()        -- rhs value not used*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		TempKeep(b)*/
        _43TempKeep(_b_52156);

        /** 		a = Pop()        -- 2nd subs*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		TempKeep(a)*/
        _43TempKeep(_a_52155);

        /** 		c = Pop()        -- 1st subs*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		TempKeep(c)*/
        _43TempKeep(_c_52157);

        /** 		d = Pop()*/
        _d_52158 = _43Pop();
        if (!IS_ATOM_INT(_d_52158)) {
            _1 = (long)(DBL_PTR(_d_52158)->dbl);
            if (UNIQUE(DBL_PTR(_d_52158)) && (DBL_PTR(_d_52158)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_52158);
            _d_52158 = _1;
        }

        /** 		TempKeep(d)      -- sequence*/
        _43TempKeep(_d_52158);

        /** 		emit_addr(d)*/
        _43emit_addr(_d_52158);

        /** 		Push(d)*/
        _43Push(_d_52158);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 		Push(a)*/
        _43Push(_a_52155);

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)     -- place to store result*/
        _43emit_addr(_c_52157);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);

        /** 		Push(b)*/
        _43Push(_b_52156);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6383] 7417

        /** 	case CALL_PROC then*/
        case 136:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27830 = _43Pop();
        _43emit_addr(_27830);
        _27830 = NOVALUE;

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6421] 7417

        /** 	case CALL_FUNC then*/
        case 137:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		emit_addr(Pop())*/
        _27832 = _43Pop();
        _43emit_addr(_27832);
        _27832 = NOVALUE;

        /** 		emit_addr(b)*/
        _43emit_addr(_b_52156);

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);
        goto LC; // [6483] 7417

        /** 	case EXIT_BLOCK then*/
        case 206:

        /** 		emit_opcode( op )*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr( Pop() )*/
        _27834 = _43Pop();
        _43emit_addr(_27834);
        _27834 = NOVALUE;

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6509] 7417

        /** 	case RETURNP then*/
        case 29:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(CurrentSub)*/
        _43emit_addr(_38CurrentSub_16954);

        /** 		emit_addr(top_block())*/
        _27835 = _68top_block(0);
        _43emit_addr(_27835);
        _27835 = NOVALUE;

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6543] 7417

        /** 	case RETURNF then*/
        case 28:

        /** 		clear_temp( Top() )*/

        /** 	return cg_stack[cgi]*/
        DeRef(_Top_inlined_Top_at_6755_53409);
        _2 = (int)SEQ_PTR(_43cg_stack_51233);
        _Top_inlined_Top_at_6755_53409 = (int)*(((s1_ptr)_2)->base + _43cgi_51234);
        Ref(_Top_inlined_Top_at_6755_53409);
        Ref(_Top_inlined_Top_at_6755_53409);
        _43clear_temp(_Top_inlined_Top_at_6755_53409);

        /** 		flush_temps()*/
        RefDS(_22663);
        _43flush_temps(_22663);

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(CurrentSub)*/
        _43emit_addr(_38CurrentSub_16954);

        /** 		emit_addr(Least_block())*/
        _27836 = _68Least_block();
        _43emit_addr(_27836);
        _27836 = NOVALUE;

        /** 		emit_addr(Pop())*/
        _27837 = _43Pop();
        _43emit_addr(_27837);
        _27837 = NOVALUE;

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6605] 7417

        /** 	case RETURNT then*/
        case 34:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6623] 7417

        /** 	case DATE, TIME, SPACE_USED, GET_KEY, TASK_LIST,*/
        case 69:
        case 70:
        case 75:
        case 79:
        case 172:
        case 100:
        case 183:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;

        /** 		if op = GET_KEY then  -- it's in op_result as integer*/
        if (_op_52153 != 79)
        goto L93; // [6665] 6677

        /** 			TempInteger(c)*/
        _43TempInteger(_c_52157);
        goto L94; // [6674] 6684
L93: 

        /** 			emit_temp( c, NEW_REFERENCE )*/
        _43emit_temp(_c_52157, 1);
L94: 

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);
        goto LC; // [6694] 7417

        /** 	case CLOSE, ABORT, CALL, DELETE_OBJECT then*/
        case 86:
        case 126:
        case 129:
        case 205:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(Pop())*/
        _27840 = _43Pop();
        _43emit_addr(_27840);
        _27840 = NOVALUE;

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6726] 7417

        /** 	case POWER then*/
        case 72:

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		if b > 0 and SymTab[b][S_MODE] = M_CONSTANT and equal(SymTab[b][S_OBJ], 2) then*/
        _27843 = (_b_52156 > 0);
        if (_27843 == 0) {
            _27844 = 0;
            goto L95; // [6752] 6778
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27845 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27845);
        _27846 = (int)*(((s1_ptr)_2)->base + 3);
        _27845 = NOVALUE;
        if (IS_ATOM_INT(_27846)) {
            _27847 = (_27846 == 2);
        }
        else {
            _27847 = binary_op(EQUALS, _27846, 2);
        }
        _27846 = NOVALUE;
        if (IS_ATOM_INT(_27847))
        _27844 = (_27847 != 0);
        else
        _27844 = DBL_PTR(_27847)->dbl != 0.0;
L95: 
        if (_27844 == 0) {
            goto L96; // [6778] 6835
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _27849 = (int)*(((s1_ptr)_2)->base + _b_52156);
        _2 = (int)SEQ_PTR(_27849);
        _27850 = (int)*(((s1_ptr)_2)->base + 1);
        _27849 = NOVALUE;
        if (_27850 == 2)
        _27851 = 1;
        else if (IS_ATOM_INT(_27850) && IS_ATOM_INT(2))
        _27851 = 0;
        else
        _27851 = (compare(_27850, 2) == 0);
        _27850 = NOVALUE;
        if (_27851 == 0)
        {
            _27851 = NOVALUE;
            goto L96; // [6799] 6835
        }
        else{
            _27851 = NOVALUE;
        }

        /** 			op = rw:MULTIPLY*/
        _op_52153 = 13;

        /** 			emit_opcode(op)*/
        _43emit_opcode(13);

        /** 			emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 			emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 			cont21d(op, a, b, FALSE)*/
        _43cont21d(13, _a_52155, _b_52156, _9FALSE_426);
        goto LC; // [6832] 7417
L96: 

        /** 			Push(a)*/
        _43Push(_a_52155);

        /** 			Push(b)*/
        _43Push(_b_52156);

        /** 			cont21ii(op, FALSE)*/
        _43cont21ii(_op_52153, _9FALSE_426);
        goto LC; // [6854] 7417

        /** 	case TYPE_CHECK then*/
        case 65:

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		c = Pop()*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [6879] 7417

        /** 	case DOLLAR then*/
        case -22:

        /** 		if current_sequence[$] < 0 or SymTab[current_sequence[$]][S_SCOPE] = SC_UNDEFINED then*/
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _27853 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _27853 = 1;
        }
        _2 = (int)SEQ_PTR(_43current_sequence_51226);
        _27854 = (int)*(((s1_ptr)_2)->base + _27853);
        if (IS_ATOM_INT(_27854)) {
            _27855 = (_27854 < 0);
        }
        else {
            _27855 = binary_op(LESS, _27854, 0);
        }
        _27854 = NOVALUE;
        if (IS_ATOM_INT(_27855)) {
            if (_27855 != 0) {
                goto L97; // [6900] 6936
            }
        }
        else {
            if (DBL_PTR(_27855)->dbl != 0.0) {
                goto L97; // [6900] 6936
            }
        }
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _27857 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _27857 = 1;
        }
        _2 = (int)SEQ_PTR(_43current_sequence_51226);
        _27858 = (int)*(((s1_ptr)_2)->base + _27857);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_27858)){
            _27859 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27858)->dbl));
        }
        else{
            _27859 = (int)*(((s1_ptr)_2)->base + _27858);
        }
        _2 = (int)SEQ_PTR(_27859);
        _27860 = (int)*(((s1_ptr)_2)->base + 4);
        _27859 = NOVALUE;
        if (IS_ATOM_INT(_27860)) {
            _27861 = (_27860 == 9);
        }
        else {
            _27861 = binary_op(EQUALS, _27860, 9);
        }
        _27860 = NOVALUE;
        if (_27861 == 0) {
            DeRef(_27861);
            _27861 = NOVALUE;
            goto L98; // [6932] 7006
        }
        else {
            if (!IS_ATOM_INT(_27861) && DBL_PTR(_27861)->dbl == 0.0){
                DeRef(_27861);
                _27861 = NOVALUE;
                goto L98; // [6932] 7006
            }
            DeRef(_27861);
            _27861 = NOVALUE;
        }
        DeRef(_27861);
        _27861 = NOVALUE;
L97: 

        /** 			if lhs_ptr and length(current_sequence) = 1 then*/
        if (_43lhs_ptr_51228 == 0) {
            goto L99; // [6940] 6969
        }
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _27863 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _27863 = 1;
        }
        _27864 = (_27863 == 1);
        _27863 = NOVALUE;
        if (_27864 == 0)
        {
            DeRef(_27864);
            _27864 = NOVALUE;
            goto L99; // [6954] 6969
        }
        else{
            DeRef(_27864);
            _27864 = NOVALUE;
        }

        /** 				c = PLENGTH*/
        _c_52157 = 160;
        goto L9A; // [6966] 6979
L99: 

        /** 				c = LENGTH*/
        _c_52157 = 42;
L9A: 

        /** 			c = - new_forward_reference( VARIABLE, current_sequence[$], c )*/
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _27865 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _27865 = 1;
        }
        _2 = (int)SEQ_PTR(_43current_sequence_51226);
        _27866 = (int)*(((s1_ptr)_2)->base + _27865);
        Ref(_27866);
        _27867 = _40new_forward_reference(-100, _27866, _c_52157);
        _27866 = NOVALUE;
        if (IS_ATOM_INT(_27867)) {
            if ((unsigned long)_27867 == 0xC0000000)
            _c_52157 = (int)NewDouble((double)-0xC0000000);
            else
            _c_52157 = - _27867;
        }
        else {
            _c_52157 = unary_op(UMINUS, _27867);
        }
        DeRef(_27867);
        _27867 = NOVALUE;
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }
        goto L9B; // [7003] 7020
L98: 

        /** 			c = current_sequence[$]*/
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _27869 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _27869 = 1;
        }
        _2 = (int)SEQ_PTR(_43current_sequence_51226);
        _c_52157 = (int)*(((s1_ptr)_2)->base + _27869);
        if (!IS_ATOM_INT(_c_52157)){
            _c_52157 = (long)DBL_PTR(_c_52157)->dbl;
        }
L9B: 

        /** 		if lhs_ptr and length(current_sequence) = 1 then*/
        if (_43lhs_ptr_51228 == 0) {
            goto L9C; // [7024] 7051
        }
        if (IS_SEQUENCE(_43current_sequence_51226)){
                _27872 = SEQ_PTR(_43current_sequence_51226)->length;
        }
        else {
            _27872 = 1;
        }
        _27873 = (_27872 == 1);
        _27872 = NOVALUE;
        if (_27873 == 0)
        {
            DeRef(_27873);
            _27873 = NOVALUE;
            goto L9C; // [7038] 7051
        }
        else{
            DeRef(_27873);
            _27873 = NOVALUE;
        }

        /** 			emit_opcode(PLENGTH)*/
        _43emit_opcode(160);
        goto L9D; // [7048] 7059
L9C: 

        /** 			emit_opcode(LENGTH)*/
        _43emit_opcode(42);
L9D: 

        /** 		emit_addr( c )*/
        _43emit_addr(_c_52157);

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		TempInteger(c)*/
        _43TempInteger(_c_52157);

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		assignable = FALSE -- it wouldn't be assigned anyway*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [7094] 7417

        /** 	case TASK_SELF then*/
        case 170:

        /** 		c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		Push(c)*/
        _43Push(_c_52157);

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 		assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;
        goto LC; // [7130] 7417

        /** 	case SWITCH then*/
        case 185:

        /** 		emit_opcode( op )*/
        _43emit_opcode(_op_52153);

        /** 		c = Pop()*/
        _c_52157 = _43Pop();
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 		b = Pop()*/
        _b_52156 = _43Pop();
        if (!IS_ATOM_INT(_b_52156)) {
            _1 = (long)(DBL_PTR(_b_52156)->dbl);
            if (UNIQUE(DBL_PTR(_b_52156)) && (DBL_PTR(_b_52156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_b_52156);
            _b_52156 = _1;
        }

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		emit_addr( a ) -- Switch Expr*/
        _43emit_addr(_a_52155);

        /** 		emit_addr( b ) -- Case values*/
        _43emit_addr(_b_52156);

        /** 		emit_addr( c ) -- Jump table*/
        _43emit_addr(_c_52157);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [7184] 7417

        /** 	case CASE then*/
        case 186:

        /** 		emit_opcode( op )*/
        _43emit_opcode(_op_52153);

        /** 		emit( cg_stack[cgi] )  -- the case index*/
        _2 = (int)SEQ_PTR(_43cg_stack_51233);
        _27879 = (int)*(((s1_ptr)_2)->base + _43cgi_51234);
        Ref(_27879);
        _43emit(_27879);
        _27879 = NOVALUE;

        /** 		cgi -= 1*/
        _43cgi_51234 = _43cgi_51234 - 1;
        goto LC; // [7216] 7417

        /** 	case PLATFORM then*/
        case 155:

        /** 		if BIND and shroud_only then*/
        if (_38BIND_16567 == 0) {
            goto L9E; // [7226] 7274
        }
        if (_38shroud_only_16944 == 0)
        {
            goto L9E; // [7233] 7274
        }
        else{
        }

        /** 			c = NewTempSym()*/
        _c_52157 = _55NewTempSym(0);
        if (!IS_ATOM_INT(_c_52157)) {
            _1 = (long)(DBL_PTR(_c_52157)->dbl);
            if (UNIQUE(DBL_PTR(_c_52157)) && (DBL_PTR(_c_52157)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_c_52157);
            _c_52157 = _1;
        }

        /** 			TempInteger(c)*/
        _43TempInteger(_c_52157);

        /** 			Push(c)*/
        _43Push(_c_52157);

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 			emit_addr(c)*/
        _43emit_addr(_c_52157);

        /** 			assignable = TRUE*/
        _43assignable_51236 = _9TRUE_428;
        goto LC; // [7271] 7417
L9E: 

        /** 			n = host_platform()*/
        _n_52166 = _42host_platform();
        if (!IS_ATOM_INT(_n_52166)) {
            _1 = (long)(DBL_PTR(_n_52166)->dbl);
            if (UNIQUE(DBL_PTR(_n_52166)) && (DBL_PTR(_n_52166)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_n_52166);
            _n_52166 = _1;
        }

        /** 			Push(NewIntSym(n))*/
        _27884 = _55NewIntSym(_n_52166);
        _43Push(_27884);
        _27884 = NOVALUE;

        /** 			assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [7298] 7417

        /** 	case PROFILE, TASK_SUSPEND then*/
        case 151:
        case 171:

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 		emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [7330] 7417

        /** 	case TRACE then*/
        case 64:

        /** 		a = Pop()*/
        _a_52155 = _43Pop();
        if (!IS_ATOM_INT(_a_52155)) {
            _1 = (long)(DBL_PTR(_a_52155)->dbl);
            if (UNIQUE(DBL_PTR(_a_52155)) && (DBL_PTR(_a_52155)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_a_52155);
            _a_52155 = _1;
        }

        /** 		if OpTrace then*/
        if (_38OpTrace_17015 == 0)
        {
            goto L9F; // [7347] 7393
        }
        else{
        }

        /** 			emit_opcode(op)*/
        _43emit_opcode(_op_52153);

        /** 			emit_addr(a)*/
        _43emit_addr(_a_52155);

        /** 			if TRANSLATE then*/
        if (_38TRANSLATE_16564 == 0)
        {
            goto LA0; // [7364] 7392
        }
        else{
        }

        /** 				if not trace_called then*/
        if (_43trace_called_51221 != 0)
        goto LA1; // [7371] 7382

        /** 					Warning(217,0)*/
        RefDS(_22663);
        _46Warning(217, 0, _22663);
LA1: 

        /** 				trace_called = TRUE*/
        _43trace_called_51221 = _9TRUE_428;
LA0: 
L9F: 

        /** 		assignable = FALSE*/
        _43assignable_51236 = _9FALSE_426;
        goto LC; // [7400] 7417

        /** 	case else*/
        default:

        /** 		InternalErr(259, {op})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _op_52153;
        _27888 = MAKE_SEQ(_1);
        _46InternalErr(259, _27888);
        _27888 = NOVALUE;
    ;}LC: 

    /** 	previous_op = op*/
    _38previous_op_17062 = _op_52153;

    /** 	inlined = 0*/
    _43inlined_52131 = 0;

    /** end procedure*/
    DeRef(_obj_52167);
    DeRef(_elements_52168);
    DeRef(_element_vals_52169);
    DeRef(_27466);
    _27466 = NOVALUE;
    _27412 = NOVALUE;
    _27436 = NOVALUE;
    DeRef(_27378);
    _27378 = NOVALUE;
    DeRef(_27507);
    _27507 = NOVALUE;
    DeRef(_27426);
    _27426 = NOVALUE;
    DeRef(_27487);
    _27487 = NOVALUE;
    DeRef(_27562);
    _27562 = NOVALUE;
    DeRef(_27604);
    _27604 = NOVALUE;
    DeRef(_27607);
    _27607 = NOVALUE;
    DeRef(_27763);
    _27763 = NOVALUE;
    DeRef(_27785);
    _27785 = NOVALUE;
    DeRef(_27322);
    _27322 = NOVALUE;
    DeRef(_27493);
    _27493 = NOVALUE;
    DeRef(_27503);
    _27503 = NOVALUE;
    DeRef(_27534);
    _27534 = NOVALUE;
    DeRef(_27551);
    _27551 = NOVALUE;
    DeRef(_27631);
    _27631 = NOVALUE;
    DeRef(_27707);
    _27707 = NOVALUE;
    DeRef(_27843);
    _27843 = NOVALUE;
    _27858 = NOVALUE;
    DeRef(_27418);
    _27418 = NOVALUE;
    DeRef(_27700);
    _27700 = NOVALUE;
    DeRef(_27318);
    _27318 = NOVALUE;
    DeRef(_27525);
    _27525 = NOVALUE;
    DeRef(_27597);
    _27597 = NOVALUE;
    DeRef(_27797);
    _27797 = NOVALUE;
    DeRef(_27362);
    _27362 = NOVALUE;
    DeRef(_27584);
    _27584 = NOVALUE;
    DeRef(_27521);
    _27521 = NOVALUE;
    _27644 = NOVALUE;
    DeRef(_27647);
    _27647 = NOVALUE;
    DeRef(_27573);
    _27573 = NOVALUE;
    DeRef(_27609);
    _27609 = NOVALUE;
    DeRef(_27635);
    _27635 = NOVALUE;
    DeRef(_27695);
    _27695 = NOVALUE;
    DeRef(_27801);
    _27801 = NOVALUE;
    DeRef(_27354);
    _27354 = NOVALUE;
    DeRef(_27714);
    _27714 = NOVALUE;
    DeRef(_27372);
    _27372 = NOVALUE;
    DeRef(_27509);
    _27509 = NOVALUE;
    DeRef(_27627);
    _27627 = NOVALUE;
    DeRef(_27677);
    _27677 = NOVALUE;
    DeRef(_27407);
    _27407 = NOVALUE;
    DeRef(_27639);
    _27639 = NOVALUE;
    DeRef(_27393);
    _27393 = NOVALUE;
    DeRef(_27575);
    _27575 = NOVALUE;
    DeRef(_27560);
    _27560 = NOVALUE;
    _27642 = NOVALUE;
    DeRef(_27547);
    _27547 = NOVALUE;
    DeRef(_27556);
    _27556 = NOVALUE;
    DeRef(_27601);
    _27601 = NOVALUE;
    DeRef(_27773);
    _27773 = NOVALUE;
    DeRef(_27324);
    _27324 = NOVALUE;
    DeRef(_27453);
    _27453 = NOVALUE;
    DeRef(_27566);
    _27566 = NOVALUE;
    DeRef(_27765);
    _27765 = NOVALUE;
    DeRef(_27847);
    _27847 = NOVALUE;
    DeRef(_27528);
    _27528 = NOVALUE;
    DeRef(_27736);
    _27736 = NOVALUE;
    DeRef(_27789);
    _27789 = NOVALUE;
    DeRef(_27359);
    _27359 = NOVALUE;
    DeRef(_27719);
    _27719 = NOVALUE;
    DeRef(_27432);
    _27432 = NOVALUE;
    DeRef(_27580);
    _27580 = NOVALUE;
    DeRef(_27726);
    _27726 = NOVALUE;
    DeRef(_27749);
    _27749 = NOVALUE;
    DeRef(_27320);
    _27320 = NOVALUE;
    DeRef(_27374);
    _27374 = NOVALUE;
    DeRef(_27777);
    _27777 = NOVALUE;
    DeRef(_27326);
    _27326 = NOVALUE;
    _27395 = NOVALUE;
    DeRef(_27397);
    _27397 = NOVALUE;
    _27410 = NOVALUE;
    _27441 = NOVALUE;
    DeRef(_27499);
    _27499 = NOVALUE;
    DeRef(_27530);
    _27530 = NOVALUE;
    DeRef(_27781);
    _27781 = NOVALUE;
    DeRef(_27369);
    _27369 = NOVALUE;
    DeRef(_27732);
    _27732 = NOVALUE;
    DeRef(_27804);
    _27804 = NOVALUE;
    _27342 = NOVALUE;
    DeRef(_27439);
    _27439 = NOVALUE;
    DeRef(_27793);
    _27793 = NOVALUE;
    DeRef(_27586);
    _27586 = NOVALUE;
    DeRef(_27625);
    _27625 = NOVALUE;
    DeRef(_27855);
    _27855 = NOVALUE;
    return;
    ;
}


void _43emit_assign_op(int _op_53560)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_op_53560)) {
        _1 = (long)(DBL_PTR(_op_53560)->dbl);
        if (UNIQUE(DBL_PTR(_op_53560)) && (DBL_PTR(_op_53560)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_53560);
        _op_53560 = _1;
    }

    /** 	if op = PLUS_EQUALS then*/
    if (_op_53560 != 515)
    goto L1; // [7] 21

    /** 		emit_op(PLUS)*/
    _43emit_op(11);
    goto L2; // [18] 86
L1: 

    /** 	elsif op = MINUS_EQUALS then*/
    if (_op_53560 != 516)
    goto L3; // [25] 39

    /** 		emit_op(MINUS)*/
    _43emit_op(10);
    goto L2; // [36] 86
L3: 

    /** 	elsif op = MULTIPLY_EQUALS then*/
    if (_op_53560 != 517)
    goto L4; // [43] 55

    /** 		emit_op(rw:MULTIPLY)*/
    _43emit_op(13);
    goto L2; // [52] 86
L4: 

    /** 	elsif op = DIVIDE_EQUALS then*/
    if (_op_53560 != 518)
    goto L5; // [59] 71

    /** 		emit_op(rw:DIVIDE)*/
    _43emit_op(14);
    goto L2; // [68] 86
L5: 

    /** 	elsif op = CONCAT_EQUALS then*/
    if (_op_53560 != 519)
    goto L6; // [75] 85

    /** 		emit_op(rw:CONCAT)*/
    _43emit_op(15);
L6: 
L2: 

    /** end procedure*/
    return;
    ;
}


void _43StartSourceLine(int _sl_53580, int _dup_ok_53581, int _emit_coverage_53582)
{
    int _line_span_53585 = NOVALUE;
    int _27910 = NOVALUE;
    int _27908 = NOVALUE;
    int _27907 = NOVALUE;
    int _27906 = NOVALUE;
    int _27905 = NOVALUE;
    int _27904 = NOVALUE;
    int _27902 = NOVALUE;
    int _27899 = NOVALUE;
    int _27897 = NOVALUE;
    int _27896 = NOVALUE;
    int _27895 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sl_53580)) {
        _1 = (long)(DBL_PTR(_sl_53580)->dbl);
        if (UNIQUE(DBL_PTR(_sl_53580)) && (DBL_PTR(_sl_53580)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sl_53580);
        _sl_53580 = _1;
    }
    if (!IS_ATOM_INT(_dup_ok_53581)) {
        _1 = (long)(DBL_PTR(_dup_ok_53581)->dbl);
        if (UNIQUE(DBL_PTR(_dup_ok_53581)) && (DBL_PTR(_dup_ok_53581)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_dup_ok_53581);
        _dup_ok_53581 = _1;
    }
    if (!IS_ATOM_INT(_emit_coverage_53582)) {
        _1 = (long)(DBL_PTR(_emit_coverage_53582)->dbl);
        if (UNIQUE(DBL_PTR(_emit_coverage_53582)) && (DBL_PTR(_emit_coverage_53582)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_emit_coverage_53582);
        _emit_coverage_53582 = _1;
    }

    /** 	if gline_number = LastLineNumber then*/
    if (_38gline_number_16951 != _62LastLineNumber_24627)
    goto L1; // [13] 66

    /** 		if length(LineTable) then*/
    if (IS_SEQUENCE(_38LineTable_17039)){
            _27895 = SEQ_PTR(_38LineTable_17039)->length;
    }
    else {
        _27895 = 1;
    }
    if (_27895 == 0)
    {
        _27895 = NOVALUE;
        goto L2; // [24] 55
    }
    else{
        _27895 = NOVALUE;
    }

    /** 			if dup_ok then*/
    if (_dup_ok_53581 == 0)
    {
        goto L3; // [29] 47
    }
    else{
    }

    /** 				emit_op( STARTLINE )*/
    _43emit_op(58);

    /** 				emit_addr( gline_number )*/
    _43emit_addr(_38gline_number_16951);
L3: 

    /** 			return -- ignore duplicates*/
    return;
    goto L4; // [52] 65
L2: 

    /** 			sl = FALSE -- top-level new statement to execute on same line*/
    _sl_53580 = _9FALSE_426;
L4: 
L1: 

    /** 	LastLineNumber = gline_number*/
    _62LastLineNumber_24627 = _38gline_number_16951;

    /** 	line_span = gline_number - SymTab[CurrentSub][S_FIRSTLINE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27896 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_27896);
    if (!IS_ATOM_INT(_38S_FIRSTLINE_16638)){
        _27897 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FIRSTLINE_16638)->dbl));
    }
    else{
        _27897 = (int)*(((s1_ptr)_2)->base + _38S_FIRSTLINE_16638);
    }
    _27896 = NOVALUE;
    if (IS_ATOM_INT(_27897)) {
        _line_span_53585 = _38gline_number_16951 - _27897;
    }
    else {
        _line_span_53585 = binary_op(MINUS, _38gline_number_16951, _27897);
    }
    _27897 = NOVALUE;
    if (!IS_ATOM_INT(_line_span_53585)) {
        _1 = (long)(DBL_PTR(_line_span_53585)->dbl);
        if (UNIQUE(DBL_PTR(_line_span_53585)) && (DBL_PTR(_line_span_53585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_line_span_53585);
        _line_span_53585 = _1;
    }

    /** 	while length(LineTable) < line_span do*/
L5: 
    if (IS_SEQUENCE(_38LineTable_17039)){
            _27899 = SEQ_PTR(_38LineTable_17039)->length;
    }
    else {
        _27899 = 1;
    }
    if (_27899 >= _line_span_53585)
    goto L6; // [109] 128

    /** 		LineTable = append(LineTable, -1) -- filler*/
    Append(&_38LineTable_17039, _38LineTable_17039, -1);

    /** 	end while*/
    goto L5; // [125] 104
L6: 

    /** 	LineTable = append(LineTable, length(Code))*/
    if (IS_SEQUENCE(_38Code_17038)){
            _27902 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _27902 = 1;
    }
    Append(&_38LineTable_17039, _38LineTable_17039, _27902);
    _27902 = NOVALUE;

    /** 	if sl and (TRANSLATE or (OpTrace or OpProfileStatement)) then*/
    if (_sl_53580 == 0) {
        goto L7; // [145] 190
    }
    if (_38TRANSLATE_16564 != 0) {
        DeRef(_27905);
        _27905 = 1;
        goto L8; // [151] 171
    }
    if (_38OpTrace_17015 != 0) {
        _27906 = 1;
        goto L9; // [157] 167
    }
    _27906 = (_38OpProfileStatement_17017 != 0);
L9: 
    DeRef(_27905);
    _27905 = (_27906 != 0);
L8: 
    if (_27905 == 0)
    {
        _27905 = NOVALUE;
        goto L7; // [172] 190
    }
    else{
        _27905 = NOVALUE;
    }

    /** 		emit_op(STARTLINE)*/
    _43emit_op(58);

    /** 		emit_addr(gline_number)*/
    _43emit_addr(_38gline_number_16951);
L7: 

    /** 	if (sl and emit_coverage = COVERAGE_INCLUDE) or emit_coverage = COVERAGE_OVERRIDE then*/
    if (_sl_53580 == 0) {
        _27907 = 0;
        goto LA; // [192] 206
    }
    _27908 = (_emit_coverage_53582 == 2);
    _27907 = (_27908 != 0);
LA: 
    if (_27907 != 0) {
        goto LB; // [206] 221
    }
    _27910 = (_emit_coverage_53582 == 3);
    if (_27910 == 0)
    {
        DeRef(_27910);
        _27910 = NOVALUE;
        goto LC; // [217] 229
    }
    else{
        DeRef(_27910);
        _27910 = NOVALUE;
    }
LB: 

    /** 		include_line( gline_number )*/
    _52include_line(_38gline_number_16951);
LC: 

    /** end procedure*/
    DeRef(_27908);
    _27908 = NOVALUE;
    return;
    ;
}


int _43has_forward_params(int _sym_53639)
{
    int _27916 = NOVALUE;
    int _27915 = NOVALUE;
    int _27914 = NOVALUE;
    int _27913 = NOVALUE;
    int _27912 = NOVALUE;
    int _27911 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_53639)) {
        _1 = (long)(DBL_PTR(_sym_53639)->dbl);
        if (UNIQUE(DBL_PTR(_sym_53639)) && (DBL_PTR(_sym_53639)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_53639);
        _sym_53639 = _1;
    }

    /** 	for i = cgi - (SymTab[sym][S_NUM_ARGS]-1) to cgi do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _27911 = (int)*(((s1_ptr)_2)->base + _sym_53639);
    _2 = (int)SEQ_PTR(_27911);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _27912 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _27912 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _27911 = NOVALUE;
    if (IS_ATOM_INT(_27912)) {
        _27913 = _27912 - 1;
        if ((long)((unsigned long)_27913 +(unsigned long) HIGH_BITS) >= 0){
            _27913 = NewDouble((double)_27913);
        }
    }
    else {
        _27913 = binary_op(MINUS, _27912, 1);
    }
    _27912 = NOVALUE;
    if (IS_ATOM_INT(_27913)) {
        _27914 = _43cgi_51234 - _27913;
        if ((long)((unsigned long)_27914 +(unsigned long) HIGH_BITS) >= 0){
            _27914 = NewDouble((double)_27914);
        }
    }
    else {
        _27914 = binary_op(MINUS, _43cgi_51234, _27913);
    }
    DeRef(_27913);
    _27913 = NOVALUE;
    _27915 = _43cgi_51234;
    {
        int _i_53641;
        Ref(_27914);
        _i_53641 = _27914;
L1: 
        if (binary_op_a(GREATER, _i_53641, _27915)){
            goto L2; // [32] 65
        }

        /** 		if cg_stack[i] < 0 then*/
        _2 = (int)SEQ_PTR(_43cg_stack_51233);
        if (!IS_ATOM_INT(_i_53641)){
            _27916 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_53641)->dbl));
        }
        else{
            _27916 = (int)*(((s1_ptr)_2)->base + _i_53641);
        }
        if (binary_op_a(GREATEREQ, _27916, 0)){
            _27916 = NOVALUE;
            goto L3; // [47] 58
        }
        _27916 = NOVALUE;

        /** 			return 1*/
        DeRef(_i_53641);
        DeRef(_27914);
        _27914 = NOVALUE;
        return 1;
L3: 

        /** 	end for*/
        _0 = _i_53641;
        if (IS_ATOM_INT(_i_53641)) {
            _i_53641 = _i_53641 + 1;
            if ((long)((unsigned long)_i_53641 +(unsigned long) HIGH_BITS) >= 0){
                _i_53641 = NewDouble((double)_i_53641);
            }
        }
        else {
            _i_53641 = binary_op_a(PLUS, _i_53641, 1);
        }
        DeRef(_0);
        goto L1; // [60] 39
L2: 
        ;
        DeRef(_i_53641);
    }

    /** 	return 0*/
    DeRef(_27914);
    _27914 = NOVALUE;
    return 0;
    ;
}



// 0x4E62F6B9
