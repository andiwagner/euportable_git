// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _7open_dll(int _file_name_919)
{
    int _fh_929 = NOVALUE;
    int _362 = NOVALUE;
    int _360 = NOVALUE;
    int _359 = NOVALUE;
    int _358 = NOVALUE;
    int _357 = NOVALUE;
    int _356 = NOVALUE;
    int _355 = NOVALUE;
    int _354 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(file_name) > 0 and types:string(file_name) then*/
    if (IS_SEQUENCE(_file_name_919)){
            _354 = SEQ_PTR(_file_name_919)->length;
    }
    else {
        _354 = 1;
    }
    _355 = (_354 > 0);
    _354 = NOVALUE;
    if (_355 == 0) {
        goto L1; // [12] 35
    }
    RefDS(_file_name_919);
    _357 = _9string(_file_name_919);
    if (_357 == 0) {
        DeRef(_357);
        _357 = NOVALUE;
        goto L1; // [21] 35
    }
    else {
        if (!IS_ATOM_INT(_357) && DBL_PTR(_357)->dbl == 0.0){
            DeRef(_357);
            _357 = NOVALUE;
            goto L1; // [21] 35
        }
        DeRef(_357);
        _357 = NOVALUE;
    }
    DeRef(_357);
    _357 = NOVALUE;

    /** 		return machine_func(M_OPEN_DLL, file_name)*/
    _358 = machine(50, _file_name_919);
    DeRefDS(_file_name_919);
    DeRef(_355);
    _355 = NOVALUE;
    return _358;
L1: 

    /** 	for idx = 1 to length(file_name) do*/
    if (IS_SEQUENCE(_file_name_919)){
            _359 = SEQ_PTR(_file_name_919)->length;
    }
    else {
        _359 = 1;
    }
    {
        int _idx_927;
        _idx_927 = 1;
L2: 
        if (_idx_927 > _359){
            goto L3; // [40] 82
        }

        /** 		atom fh = machine_func(M_OPEN_DLL, file_name[idx])*/
        _2 = (int)SEQ_PTR(_file_name_919);
        _360 = (int)*(((s1_ptr)_2)->base + _idx_927);
        DeRef(_fh_929);
        _fh_929 = machine(50, _360);
        _360 = NOVALUE;

        /** 		if not fh = 0 then*/
        if (IS_ATOM_INT(_fh_929)) {
            _362 = (_fh_929 == 0);
        }
        else {
            _362 = unary_op(NOT, _fh_929);
        }
        if (_362 != 0)
        goto L4; // [62] 73

        /** 			return fh*/
        DeRefDS(_file_name_919);
        DeRef(_355);
        _355 = NOVALUE;
        DeRef(_358);
        _358 = NOVALUE;
        _362 = NOVALUE;
        return _fh_929;
L4: 
        DeRef(_fh_929);
        _fh_929 = NOVALUE;

        /** 	end for*/
        _idx_927 = _idx_927 + 1;
        goto L2; // [77] 47
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_file_name_919);
    DeRef(_355);
    _355 = NOVALUE;
    DeRef(_358);
    _358 = NOVALUE;
    DeRef(_362);
    _362 = NOVALUE;
    return 0;
    ;
}


int _7define_c_proc(int _lib_943, int _routine_name_944, int _arg_types_945)
{
    int _safe_address_inlined_safe_address_at_13_950 = NOVALUE;
    int _msg_inlined_crash_at_28_954 = NOVALUE;
    int _371 = NOVALUE;
    int _370 = NOVALUE;
    int _368 = NOVALUE;
    int _367 = NOVALUE;
    int _366 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _366 = 0;
    if (_366 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_950 = 1;
    _368 = (1 == 0);
    if (_368 == 0)
    {
        DeRef(_368);
        _368 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_368);
        _368 = NOVALUE;
    }

    /**         error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_954);
    _msg_inlined_crash_at_28_954 = EPrintf(-9999999, _369, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_954);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_954);
    _msg_inlined_crash_at_28_954 = NOVALUE;
L1: 

    /** 	return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, 0})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_943);
    *((int *)(_2+4)) = _lib_943;
    Ref(_routine_name_944);
    *((int *)(_2+8)) = _routine_name_944;
    RefDS(_arg_types_945);
    *((int *)(_2+12)) = _arg_types_945;
    *((int *)(_2+16)) = 0;
    _370 = MAKE_SEQ(_1);
    _371 = machine(51, _370);
    DeRefDS(_370);
    _370 = NOVALUE;
    DeRef(_lib_943);
    DeRefi(_routine_name_944);
    DeRefDSi(_arg_types_945);
    return _371;
    ;
}


int _7define_c_func(int _lib_959, int _routine_name_960, int _arg_types_961, int _return_type_962)
{
    int _safe_address_inlined_safe_address_at_13_967 = NOVALUE;
    int _msg_inlined_crash_at_28_970 = NOVALUE;
    int _376 = NOVALUE;
    int _375 = NOVALUE;
    int _374 = NOVALUE;
    int _373 = NOVALUE;
    int _372 = NOVALUE;
    int _0, _1, _2;
    

    /** 	  if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _372 = 0;
    if (_372 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_967 = 1;
    _374 = (1 == 0);
    if (_374 == 0)
    {
        DeRef(_374);
        _374 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_374);
        _374 = NOVALUE;
    }

    /** 	      error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_970);
    _msg_inlined_crash_at_28_970 = EPrintf(-9999999, _369, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_970);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_970);
    _msg_inlined_crash_at_28_970 = NOVALUE;
L1: 

    /** 	  return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, return_type})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_959);
    *((int *)(_2+4)) = _lib_959;
    Ref(_routine_name_960);
    *((int *)(_2+8)) = _routine_name_960;
    RefDS(_arg_types_961);
    *((int *)(_2+12)) = _arg_types_961;
    *((int *)(_2+16)) = _return_type_962;
    _375 = MAKE_SEQ(_1);
    _376 = machine(51, _375);
    DeRefDS(_375);
    _375 = NOVALUE;
    DeRef(_lib_959);
    DeRefi(_routine_name_960);
    DeRefDSi(_arg_types_961);
    return _376;
    ;
}



// 0xD5A5ADBA
