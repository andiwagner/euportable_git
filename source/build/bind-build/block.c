// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _68block_type_name(int _opcode_46631)
{
    int _24832 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46631)) {
        _1 = (long)(DBL_PTR(_opcode_46631)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46631)) && (DBL_PTR(_opcode_46631)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46631);
        _opcode_46631 = _1;
    }

    /** 	switch opcode do*/
    _0 = _opcode_46631;
    switch ( _0 ){ 

        /** 		case LOOP then*/
        case 422:

        /** 			return "LOOP"*/
        RefDS(_24830);
        return _24830;
        goto L1; // [20] 63

        /** 		case PROC then*/
        case 27:

        /** 			return "PROC"*/
        RefDS(_22421);
        return _22421;
        goto L1; // [32] 63

        /** 		case FUNC then*/
        case 501:

        /** 			return "FUNC"*/
        RefDS(_24831);
        return _24831;
        goto L1; // [44] 63

        /** 		case else*/
        default:

        /** 			return opnames[opcode]*/
        _2 = (int)SEQ_PTR(_61opnames_22681);
        _24832 = (int)*(((s1_ptr)_2)->base + _opcode_46631);
        RefDS(_24832);
        return _24832;
    ;}L1: 
    ;
}


void _68check_block(int _got_46647)
{
    int _expected_46648 = NOVALUE;
    int _24840 = NOVALUE;
    int _24839 = NOVALUE;
    int _24838 = NOVALUE;
    int _24834 = NOVALUE;
    int _24833 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer expected = block_stack[$][BLOCK_OPCODE]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24833 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24833 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24834 = (int)*(((s1_ptr)_2)->base + _24833);
    _2 = (int)SEQ_PTR(_24834);
    _expected_46648 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_expected_46648)){
        _expected_46648 = (long)DBL_PTR(_expected_46648)->dbl;
    }
    _24834 = NOVALUE;

    /** 	if got = FUNC then*/
    if (_got_46647 != 501)
    goto L1; // [26] 40

    /** 		got = PROC*/
    _got_46647 = 27;
L1: 

    /** 	if got != expected then*/
    if (_got_46647 == _expected_46648)
    goto L2; // [42] 66

    /** 		CompileErr( 79, {block_type_name( expected ), block_type_name( got)} )*/
    _24838 = _68block_type_name(_expected_46648);
    _24839 = _68block_type_name(_got_46647);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24838;
    ((int *)_2)[2] = _24839;
    _24840 = MAKE_SEQ(_1);
    _24839 = NOVALUE;
    _24838 = NOVALUE;
    _46CompileErr(79, _24840, 0);
    _24840 = NOVALUE;
L2: 

    /** end procedure*/
    return;
    ;
}


void _68Block_var(int _sym_46665)
{
    int _block_46666 = NOVALUE;
    int _24861 = NOVALUE;
    int _24860 = NOVALUE;
    int _24859 = NOVALUE;
    int _24857 = NOVALUE;
    int _24856 = NOVALUE;
    int _24854 = NOVALUE;
    int _24853 = NOVALUE;
    int _24852 = NOVALUE;
    int _24851 = NOVALUE;
    int _24850 = NOVALUE;
    int _24849 = NOVALUE;
    int _24848 = NOVALUE;
    int _24846 = NOVALUE;
    int _24844 = NOVALUE;
    int _24843 = NOVALUE;
    int _24841 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_46665)) {
        _1 = (long)(DBL_PTR(_sym_46665)->dbl);
        if (UNIQUE(DBL_PTR(_sym_46665)) && (DBL_PTR(_sym_46665)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_46665);
        _sym_46665 = _1;
    }

    /** 	sequence block = block_stack[$]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24841 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24841 = 1;
    }
    DeRef(_block_46666);
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _block_46666 = (int)*(((s1_ptr)_2)->base + _24841);
    Ref(_block_46666);

    /** 	block_stack[$] = 0*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24843 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24843 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _2 = (int)(((s1_ptr)_2)->base + _24843);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if length(block_stack) > 1 then*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24844 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24844 = 1;
    }
    if (_24844 <= 1)
    goto L1; // [34] 60

    /** 		SymTab[sym][S_BLOCK] = block[BLOCK_SYM]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_46665 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_block_46666);
    _24848 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24848);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_BLOCK_16618))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_BLOCK_16618)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_BLOCK_16618);
    _1 = *(int *)_2;
    *(int *)_2 = _24848;
    if( _1 != _24848 ){
        DeRef(_1);
    }
    _24848 = NOVALUE;
    _24846 = NOVALUE;
L1: 

    /** 	if length(block[BLOCK_VARS]) then*/
    _2 = (int)SEQ_PTR(_block_46666);
    _24849 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_24849)){
            _24850 = SEQ_PTR(_24849)->length;
    }
    else {
        _24850 = 1;
    }
    _24849 = NOVALUE;
    if (_24850 == 0)
    {
        _24850 = NOVALUE;
        goto L2; // [71] 105
    }
    else{
        _24850 = NOVALUE;
    }

    /** 		SymTab[block[BLOCK_VARS][$]][S_NEXT_IN_BLOCK] = sym*/
    _2 = (int)SEQ_PTR(_block_46666);
    _24851 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_24851)){
            _24852 = SEQ_PTR(_24851)->length;
    }
    else {
        _24852 = 1;
    }
    _2 = (int)SEQ_PTR(_24851);
    _24853 = (int)*(((s1_ptr)_2)->base + _24852);
    _24851 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_24853))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_24853)->dbl));
    else
    _3 = (int)(_24853 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NEXT_IN_BLOCK_16590))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NEXT_IN_BLOCK_16590)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NEXT_IN_BLOCK_16590);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_46665;
    DeRef(_1);
    _24854 = NOVALUE;
    goto L3; // [102] 127
L2: 

    /** 		SymTab[block[BLOCK_SYM]][S_NEXT_IN_BLOCK] = sym*/
    _2 = (int)SEQ_PTR(_block_46666);
    _24856 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_24856))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_24856)->dbl));
    else
    _3 = (int)(_24856 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NEXT_IN_BLOCK_16590))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NEXT_IN_BLOCK_16590)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NEXT_IN_BLOCK_16590);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_46665;
    DeRef(_1);
    _24857 = NOVALUE;
L3: 

    /** 	block[BLOCK_VARS] &= sym*/
    _2 = (int)SEQ_PTR(_block_46666);
    _24859 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_24859) && IS_ATOM(_sym_46665)) {
        Append(&_24860, _24859, _sym_46665);
    }
    else if (IS_ATOM(_24859) && IS_SEQUENCE(_sym_46665)) {
    }
    else {
        Concat((object_ptr)&_24860, _24859, _sym_46665);
        _24859 = NOVALUE;
    }
    _24859 = NOVALUE;
    _2 = (int)SEQ_PTR(_block_46666);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46666 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _24860;
    if( _1 != _24860 ){
        DeRef(_1);
    }
    _24860 = NOVALUE;

    /** 	block_stack[$] = block*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24861 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24861 = 1;
    }
    RefDS(_block_46666);
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _2 = (int)(((s1_ptr)_2)->base + _24861);
    _1 = *(int *)_2;
    *(int *)_2 = _block_46666;
    DeRef(_1);

    /** 	ifdef BDEBUG then*/

    /** end procedure*/
    DeRefDS(_block_46666);
    _24849 = NOVALUE;
    _24853 = NOVALUE;
    _24856 = NOVALUE;
    return;
    ;
}


void _68NewBlock(int _opcode_46700, int _block_label_46701)
{
    int _block_46719 = NOVALUE;
    int _24875 = NOVALUE;
    int _24874 = NOVALUE;
    int _24873 = NOVALUE;
    int _24871 = NOVALUE;
    int _24869 = NOVALUE;
    int _24868 = NOVALUE;
    int _24866 = NOVALUE;
    int _24865 = NOVALUE;
    int _24863 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab = append( SymTab, repeat( 0, SIZEOF_BLOCK_ENTRY ) )*/
    _24863 = Repeat(0, _38SIZEOF_BLOCK_ENTRY_16730);
    RefDS(_24863);
    Append(&_35SymTab_15595, _35SymTab_15595, _24863);
    DeRefDS(_24863);
    _24863 = NOVALUE;

    /** 	SymTab[$][S_MODE] = M_BLOCK*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _24865 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _24865 = 1;
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_24865 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _24866 = NOVALUE;

    /** 	SymTab[$][S_FIRST_LINE] = gline_number*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _24868 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _24868 = 1;
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_24868 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FIRST_LINE_16623))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FIRST_LINE_16623)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FIRST_LINE_16623);
    _1 = *(int *)_2;
    *(int *)_2 = _38gline_number_16951;
    DeRef(_1);
    _24869 = NOVALUE;

    /** 	sequence block = repeat( 0, BLOCK_SIZE-1 )*/
    _24871 = 6;
    DeRef(_block_46719);
    _block_46719 = Repeat(0, 6);
    _24871 = NOVALUE;

    /** 	block[BLOCK_SYM]    = length(SymTab)*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _24873 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _24873 = 1;
    }
    _2 = (int)SEQ_PTR(_block_46719);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46719 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _24873;
    if( _1 != _24873 ){
    }
    _24873 = NOVALUE;

    /** 	block[BLOCK_OPCODE] = opcode*/
    _2 = (int)SEQ_PTR(_block_46719);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46719 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    *(int *)_2 = _opcode_46700;

    /** 	block[BLOCK_LABEL]  = block_label*/
    Ref(_block_label_46701);
    _2 = (int)SEQ_PTR(_block_46719);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46719 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    *(int *)_2 = _block_label_46701;

    /** 	block[BLOCK_START]  = length(Code) + 1*/
    if (IS_SEQUENCE(_38Code_17038)){
            _24874 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _24874 = 1;
    }
    _24875 = _24874 + 1;
    _24874 = NOVALUE;
    _2 = (int)SEQ_PTR(_block_46719);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46719 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _24875;
    if( _1 != _24875 ){
        DeRef(_1);
    }
    _24875 = NOVALUE;

    /** 	block[BLOCK_VARS]   = {}*/
    RefDS(_22663);
    _2 = (int)SEQ_PTR(_block_46719);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _block_46719 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _22663;
    DeRef(_1);

    /** 	block_stack = append( block_stack, block )*/
    RefDS(_block_46719);
    Append(&_68block_stack_46621, _68block_stack_46621, _block_46719);

    /** 	current_block = length(SymTab)*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _68current_block_46628 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _68current_block_46628 = 1;
    }

    /** end procedure*/
    DeRef(_block_label_46701);
    DeRefDS(_block_46719);
    return;
    ;
}


void _68Start_block(int _opcode_46732, int _block_label_46733)
{
    int _last_block_46735 = NOVALUE;
    int _label_name_46763 = NOVALUE;
    int _24897 = NOVALUE;
    int _24896 = NOVALUE;
    int _24895 = NOVALUE;
    int _24892 = NOVALUE;
    int _24891 = NOVALUE;
    int _24889 = NOVALUE;
    int _24888 = NOVALUE;
    int _24887 = NOVALUE;
    int _24886 = NOVALUE;
    int _24885 = NOVALUE;
    int _24882 = NOVALUE;
    int _24880 = NOVALUE;
    int _24879 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_opcode_46732)) {
        _1 = (long)(DBL_PTR(_opcode_46732)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46732)) && (DBL_PTR(_opcode_46732)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46732);
        _opcode_46732 = _1;
    }

    /** 	symtab_index last_block = current_block*/
    _last_block_46735 = _68current_block_46628;

    /** 	if opcode = FUNC then*/
    if (_opcode_46732 != 501)
    goto L1; // [16] 30

    /** 		opcode = PROC*/
    _opcode_46732 = 27;
L1: 

    /** 	NewBlock( opcode, block_label )*/
    Ref(_block_label_46733);
    _68NewBlock(_opcode_46732, _block_label_46733);

    /** 	if find(opcode, RTN_TOKS) then*/
    _24879 = find_from(_opcode_46732, _39RTN_TOKS_16547, 1);
    if (_24879 == 0)
    {
        _24879 = NOVALUE;
        goto L2; // [45] 105
    }
    else{
        _24879 = NOVALUE;
    }

    /** 		SymTab[block_label][S_BLOCK] = current_block*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_block_label_46733))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_block_label_46733)->dbl));
    else
    _3 = (int)(_block_label_46733 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_BLOCK_16618))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_BLOCK_16618)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_BLOCK_16618);
    _1 = *(int *)_2;
    *(int *)_2 = _68current_block_46628;
    DeRef(_1);
    _24880 = NOVALUE;

    /** 		SymTab[current_block][S_NAME] = sprintf("BLOCK: %s", {SymTab[block_label][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_68current_block_46628 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_block_label_46733)){
        _24885 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_block_label_46733)->dbl));
    }
    else{
        _24885 = (int)*(((s1_ptr)_2)->base + _block_label_46733);
    }
    _2 = (int)SEQ_PTR(_24885);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _24886 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _24886 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _24885 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24886);
    *((int *)(_2+4)) = _24886;
    _24887 = MAKE_SEQ(_1);
    _24886 = NOVALUE;
    _24888 = EPrintf(-9999999, _24884, _24887);
    DeRefDS(_24887);
    _24887 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NAME_16598))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NAME_16598);
    _1 = *(int *)_2;
    *(int *)_2 = _24888;
    if( _1 != _24888 ){
        DeRef(_1);
    }
    _24888 = NOVALUE;
    _24882 = NOVALUE;
    goto L3; // [102] 185
L2: 

    /** 	elsif current_block then*/
    if (_68current_block_46628 == 0)
    {
        goto L4; // [109] 182
    }
    else{
    }

    /** 		SymTab[current_block][S_BLOCK] = last_block*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_68current_block_46628 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_BLOCK_16618))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_BLOCK_16618)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_BLOCK_16618);
    _1 = *(int *)_2;
    *(int *)_2 = _last_block_46735;
    DeRef(_1);
    _24889 = NOVALUE;

    /** 		sequence label_name = ""*/
    RefDS(_22663);
    DeRef(_label_name_46763);
    _label_name_46763 = _22663;

    /** 		if sequence(block_label) then*/
    _24891 = IS_SEQUENCE(_block_label_46733);
    if (_24891 == 0)
    {
        _24891 = NOVALUE;
        goto L5; // [141] 152
    }
    else{
        _24891 = NOVALUE;
    }

    /** 			label_name = block_label*/
    Ref(_block_label_46733);
    DeRefDS(_label_name_46763);
    _label_name_46763 = _block_label_46733;
L5: 

    /** 		SymTab[current_block][S_NAME] = sprintf( "BLOCK: %s-%s", {block_type_name(opcode), label_name})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_68current_block_46628 + ((s1_ptr)_2)->base);
    _24895 = _68block_type_name(_opcode_46732);
    RefDS(_label_name_46763);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24895;
    ((int *)_2)[2] = _label_name_46763;
    _24896 = MAKE_SEQ(_1);
    _24895 = NOVALUE;
    _24897 = EPrintf(-9999999, _24894, _24896);
    DeRefDS(_24896);
    _24896 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NAME_16598))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NAME_16598);
    _1 = *(int *)_2;
    *(int *)_2 = _24897;
    if( _1 != _24897 ){
        DeRef(_1);
    }
    _24897 = NOVALUE;
    _24892 = NOVALUE;
L4: 
    DeRef(_label_name_46763);
    _label_name_46763 = NOVALUE;
L3: 

    /** 	ifdef BDEBUG then*/

    /** end procedure*/
    DeRef(_block_label_46733);
    return;
    ;
}


void _68block_label(int _label_name_46779)
{
    int _24911 = NOVALUE;
    int _24910 = NOVALUE;
    int _24909 = NOVALUE;
    int _24908 = NOVALUE;
    int _24907 = NOVALUE;
    int _24906 = NOVALUE;
    int _24904 = NOVALUE;
    int _24902 = NOVALUE;
    int _24901 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	block_stack[$][BLOCK_LABEL] = label_name*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24901 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24901 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _68block_stack_46621 = MAKE_SEQ(_2);
    }
    _3 = (int)(_24901 + ((s1_ptr)_2)->base);
    RefDS(_label_name_46779);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _label_name_46779;
    DeRef(_1);
    _24902 = NOVALUE;

    /** 	SymTab[current_block][S_NAME] = sprintf( "BLOCK: %s-%s", */
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_68current_block_46628 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24906 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24906 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24907 = (int)*(((s1_ptr)_2)->base + _24906);
    _2 = (int)SEQ_PTR(_24907);
    _24908 = (int)*(((s1_ptr)_2)->base + 2);
    _24907 = NOVALUE;
    Ref(_24908);
    _24909 = _68block_type_name(_24908);
    _24908 = NOVALUE;
    RefDS(_label_name_46779);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24909;
    ((int *)_2)[2] = _label_name_46779;
    _24910 = MAKE_SEQ(_1);
    _24909 = NOVALUE;
    _24911 = EPrintf(-9999999, _24894, _24910);
    DeRefDS(_24910);
    _24910 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NAME_16598))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NAME_16598);
    _1 = *(int *)_2;
    *(int *)_2 = _24911;
    if( _1 != _24911 ){
        DeRef(_1);
    }
    _24911 = NOVALUE;
    _24904 = NOVALUE;

    /** end procedure*/
    DeRefDS(_label_name_46779);
    return;
    ;
}


int _68pop_block()
{
    int _block_46798 = NOVALUE;
    int _block_vars_46811 = NOVALUE;
    int _24940 = NOVALUE;
    int _24938 = NOVALUE;
    int _24937 = NOVALUE;
    int _24936 = NOVALUE;
    int _24935 = NOVALUE;
    int _24933 = NOVALUE;
    int _24932 = NOVALUE;
    int _24931 = NOVALUE;
    int _24930 = NOVALUE;
    int _24929 = NOVALUE;
    int _24928 = NOVALUE;
    int _24927 = NOVALUE;
    int _24926 = NOVALUE;
    int _24925 = NOVALUE;
    int _24924 = NOVALUE;
    int _24920 = NOVALUE;
    int _24919 = NOVALUE;
    int _24917 = NOVALUE;
    int _24916 = NOVALUE;
    int _24914 = NOVALUE;
    int _24912 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if not length(block_stack) then*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24912 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24912 = 1;
    }
    if (_24912 != 0)
    goto L1; // [8] 18
    _24912 = NOVALUE;

    /** 		return 0*/
    DeRef(_block_46798);
    DeRef(_block_vars_46811);
    return 0;
L1: 

    /** 	sequence  block = block_stack[$]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24914 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24914 = 1;
    }
    DeRef(_block_46798);
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _block_46798 = (int)*(((s1_ptr)_2)->base + _24914);
    Ref(_block_46798);

    /** 	block_stack = block_stack[1..$-1]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24916 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24916 = 1;
    }
    _24917 = _24916 - 1;
    _24916 = NOVALUE;
    rhs_slice_target = (object_ptr)&_68block_stack_46621;
    RHS_Slice(_68block_stack_46621, 1, _24917);

    /** 	SymTab[block[BLOCK_SYM]][S_LAST_LINE] = gline_number*/
    _2 = (int)SEQ_PTR(_block_46798);
    _24919 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_24919))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_24919)->dbl));
    else
    _3 = (int)(_24919 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_LAST_LINE_16628))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LAST_LINE_16628)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_LAST_LINE_16628);
    _1 = *(int *)_2;
    *(int *)_2 = _38gline_number_16951;
    DeRef(_1);
    _24920 = NOVALUE;

    /** 	ifdef BDEBUG then*/

    /** 	sequence block_vars = block[BLOCK_VARS]*/
    DeRef(_block_vars_46811);
    _2 = (int)SEQ_PTR(_block_46798);
    _block_vars_46811 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_block_vars_46811);

    /** 	for sx = 1 to length( block_vars ) do*/
    if (IS_SEQUENCE(_block_vars_46811)){
            _24924 = SEQ_PTR(_block_vars_46811)->length;
    }
    else {
        _24924 = 1;
    }
    {
        int _sx_46814;
        _sx_46814 = 1;
L2: 
        if (_sx_46814 > _24924){
            goto L3; // [87] 176
        }

        /** 		if SymTab[block_vars[sx]][S_MODE] = M_NORMAL */
        _2 = (int)SEQ_PTR(_block_vars_46811);
        _24925 = (int)*(((s1_ptr)_2)->base + _sx_46814);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_24925)){
            _24926 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_24925)->dbl));
        }
        else{
            _24926 = (int)*(((s1_ptr)_2)->base + _24925);
        }
        _2 = (int)SEQ_PTR(_24926);
        _24927 = (int)*(((s1_ptr)_2)->base + 3);
        _24926 = NOVALUE;
        if (IS_ATOM_INT(_24927)) {
            _24928 = (_24927 == 1);
        }
        else {
            _24928 = binary_op(EQUALS, _24927, 1);
        }
        _24927 = NOVALUE;
        if (IS_ATOM_INT(_24928)) {
            if (_24928 == 0) {
                goto L4; // [118] 169
            }
        }
        else {
            if (DBL_PTR(_24928)->dbl == 0.0) {
                goto L4; // [118] 169
            }
        }
        _2 = (int)SEQ_PTR(_block_vars_46811);
        _24930 = (int)*(((s1_ptr)_2)->base + _sx_46814);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_24930)){
            _24931 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_24930)->dbl));
        }
        else{
            _24931 = (int)*(((s1_ptr)_2)->base + _24930);
        }
        _2 = (int)SEQ_PTR(_24931);
        _24932 = (int)*(((s1_ptr)_2)->base + 4);
        _24931 = NOVALUE;
        if (IS_ATOM_INT(_24932)) {
            _24933 = (_24932 <= 5);
        }
        else {
            _24933 = binary_op(LESSEQ, _24932, 5);
        }
        _24932 = NOVALUE;
        if (_24933 == 0) {
            DeRef(_24933);
            _24933 = NOVALUE;
            goto L4; // [145] 169
        }
        else {
            if (!IS_ATOM_INT(_24933) && DBL_PTR(_24933)->dbl == 0.0){
                DeRef(_24933);
                _24933 = NOVALUE;
                goto L4; // [145] 169
            }
            DeRef(_24933);
            _24933 = NOVALUE;
        }
        DeRef(_24933);
        _24933 = NOVALUE;

        /** 			ifdef BDEBUG then*/

        /** 			Hide( block_vars[sx] )*/
        _2 = (int)SEQ_PTR(_block_vars_46811);
        _24935 = (int)*(((s1_ptr)_2)->base + _sx_46814);
        Ref(_24935);
        _55Hide(_24935);
        _24935 = NOVALUE;

        /** 			LintCheck( block_vars[sx] )*/
        _2 = (int)SEQ_PTR(_block_vars_46811);
        _24936 = (int)*(((s1_ptr)_2)->base + _sx_46814);
        Ref(_24936);
        _55LintCheck(_24936);
        _24936 = NOVALUE;
L4: 

        /** 	end for*/
        _sx_46814 = _sx_46814 + 1;
        goto L2; // [171] 94
L3: 
        ;
    }

    /** 	current_block = block_stack[$][BLOCK_SYM]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24937 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24937 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24938 = (int)*(((s1_ptr)_2)->base + _24937);
    _2 = (int)SEQ_PTR(_24938);
    _68current_block_46628 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_68current_block_46628)){
        _68current_block_46628 = (long)DBL_PTR(_68current_block_46628)->dbl;
    }
    _24938 = NOVALUE;

    /** 	return block[BLOCK_SYM]*/
    _2 = (int)SEQ_PTR(_block_46798);
    _24940 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24940);
    DeRefDS(_block_46798);
    DeRef(_block_vars_46811);
    DeRef(_24917);
    _24917 = NOVALUE;
    _24919 = NOVALUE;
    _24925 = NOVALUE;
    _24930 = NOVALUE;
    DeRef(_24928);
    _24928 = NOVALUE;
    return _24940;
    ;
}


int _68top_block(int _offset_46843)
{
    int _24948 = NOVALUE;
    int _24947 = NOVALUE;
    int _24946 = NOVALUE;
    int _24945 = NOVALUE;
    int _24944 = NOVALUE;
    int _24943 = NOVALUE;
    int _24941 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_offset_46843)) {
        _1 = (long)(DBL_PTR(_offset_46843)->dbl);
        if (UNIQUE(DBL_PTR(_offset_46843)) && (DBL_PTR(_offset_46843)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_46843);
        _offset_46843 = _1;
    }

    /** 	if offset >= length(block_stack) then*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24941 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24941 = 1;
    }
    if (_offset_46843 < _24941)
    goto L1; // [10] 33

    /** 		CompileErr(107, {offset,length(block_stack)})*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24943 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24943 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _offset_46843;
    ((int *)_2)[2] = _24943;
    _24944 = MAKE_SEQ(_1);
    _24943 = NOVALUE;
    _46CompileErr(107, _24944, 0);
    _24944 = NOVALUE;
    goto L2; // [30] 59
L1: 

    /** 		return block_stack[$-offset][BLOCK_SYM]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24945 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24945 = 1;
    }
    _24946 = _24945 - _offset_46843;
    _24945 = NOVALUE;
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24947 = (int)*(((s1_ptr)_2)->base + _24946);
    _2 = (int)SEQ_PTR(_24947);
    _24948 = (int)*(((s1_ptr)_2)->base + 1);
    _24947 = NOVALUE;
    Ref(_24948);
    _24946 = NOVALUE;
    return _24948;
L2: 
    ;
}


void _68End_block(int _opcode_46857)
{
    int _ix_46868 = NOVALUE;
    int _24956 = NOVALUE;
    int _24953 = NOVALUE;
    int _24952 = NOVALUE;
    int _24951 = NOVALUE;
    int _24950 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46857)) {
        _1 = (long)(DBL_PTR(_opcode_46857)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46857)) && (DBL_PTR(_opcode_46857)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46857);
        _opcode_46857 = _1;
    }

    /** 	if opcode = FUNC then*/
    if (_opcode_46857 != 501)
    goto L1; // [7] 21

    /** 		opcode = PROC*/
    _opcode_46857 = 27;
L1: 

    /** 	check_block( opcode )*/
    _68check_block(_opcode_46857);

    /** 	if not length(block_stack[$][BLOCK_VARS]) then*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24950 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24950 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24951 = (int)*(((s1_ptr)_2)->base + _24950);
    _2 = (int)SEQ_PTR(_24951);
    _24952 = (int)*(((s1_ptr)_2)->base + 6);
    _24951 = NOVALUE;
    if (IS_SEQUENCE(_24952)){
            _24953 = SEQ_PTR(_24952)->length;
    }
    else {
        _24953 = 1;
    }
    _24952 = NOVALUE;
    if (_24953 != 0)
    goto L2; // [46] 66
    _24953 = NOVALUE;

    /** 		integer ix = 1*/
    _ix_46868 = 1;

    /** 		ix = pop_block()*/
    _ix_46868 = _68pop_block();
    if (!IS_ATOM_INT(_ix_46868)) {
        _1 = (long)(DBL_PTR(_ix_46868)->dbl);
        if (UNIQUE(DBL_PTR(_ix_46868)) && (DBL_PTR(_ix_46868)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ix_46868);
        _ix_46868 = _1;
    }
    goto L3; // [63] 82
L2: 

    /** 		Push( pop_block() )*/
    _24956 = _68pop_block();
    _43Push(_24956);
    _24956 = NOVALUE;

    /** 		emit_op( EXIT_BLOCK )*/
    _43emit_op(206);
L3: 

    /** end procedure*/
    _24952 = NOVALUE;
    return;
    ;
}


int _68End_inline_block(int _opcode_46877)
{
    int _24963 = NOVALUE;
    int _24962 = NOVALUE;
    int _24961 = NOVALUE;
    int _24960 = NOVALUE;
    int _24959 = NOVALUE;
    int _24958 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46877)) {
        _1 = (long)(DBL_PTR(_opcode_46877)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46877)) && (DBL_PTR(_opcode_46877)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46877);
        _opcode_46877 = _1;
    }

    /** 	if opcode = FUNC then*/
    if (_opcode_46877 != 501)
    goto L1; // [7] 21

    /** 		opcode = PROC*/
    _opcode_46877 = 27;
L1: 

    /** 	if length(block_stack[$][BLOCK_VARS]) then*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24958 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24958 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24959 = (int)*(((s1_ptr)_2)->base + _24958);
    _2 = (int)SEQ_PTR(_24959);
    _24960 = (int)*(((s1_ptr)_2)->base + 6);
    _24959 = NOVALUE;
    if (IS_SEQUENCE(_24960)){
            _24961 = SEQ_PTR(_24960)->length;
    }
    else {
        _24961 = 1;
    }
    _24960 = NOVALUE;
    if (_24961 == 0)
    {
        _24961 = NOVALUE;
        goto L2; // [41] 62
    }
    else{
        _24961 = NOVALUE;
    }

    /** 		return { EXIT_BLOCK, pop_block() }*/
    _24962 = _68pop_block();
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 206;
    ((int *)_2)[2] = _24962;
    _24963 = MAKE_SEQ(_1);
    _24962 = NOVALUE;
    _24960 = NOVALUE;
    return _24963;
    goto L3; // [59] 74
L2: 

    /** 		Drop_block( opcode )*/
    _68Drop_block(_opcode_46877);

    /** 		return {}*/
    RefDS(_22663);
    _24960 = NOVALUE;
    DeRef(_24963);
    _24963 = NOVALUE;
    return _22663;
L3: 
    ;
}


void _68Sibling_block(int _opcode_46894)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46894)) {
        _1 = (long)(DBL_PTR(_opcode_46894)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46894)) && (DBL_PTR(_opcode_46894)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46894);
        _opcode_46894 = _1;
    }

    /** 	End_block( opcode )*/
    _68End_block(_opcode_46894);

    /** 	Start_block( opcode )*/
    _68Start_block(_opcode_46894, 0);

    /** end procedure*/
    return;
    ;
}


void _68Leave_block(int _offset_46897)
{
    int _24969 = NOVALUE;
    int _24968 = NOVALUE;
    int _24967 = NOVALUE;
    int _24966 = NOVALUE;
    int _24965 = NOVALUE;
    int _24964 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_offset_46897)) {
        _1 = (long)(DBL_PTR(_offset_46897)->dbl);
        if (UNIQUE(DBL_PTR(_offset_46897)) && (DBL_PTR(_offset_46897)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_46897);
        _offset_46897 = _1;
    }

    /** 	if length( block_stack[$-offset][BLOCK_VARS]) then*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24964 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24964 = 1;
    }
    _24965 = _24964 - _offset_46897;
    _24964 = NOVALUE;
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24966 = (int)*(((s1_ptr)_2)->base + _24965);
    _2 = (int)SEQ_PTR(_24966);
    _24967 = (int)*(((s1_ptr)_2)->base + 6);
    _24966 = NOVALUE;
    if (IS_SEQUENCE(_24967)){
            _24968 = SEQ_PTR(_24967)->length;
    }
    else {
        _24968 = 1;
    }
    _24967 = NOVALUE;
    if (_24968 == 0)
    {
        _24968 = NOVALUE;
        goto L1; // [27] 47
    }
    else{
        _24968 = NOVALUE;
    }

    /** 		Push( top_block( offset ) )*/
    _24969 = _68top_block(_offset_46897);
    _43Push(_24969);
    _24969 = NOVALUE;

    /** 		emit_op( EXIT_BLOCK )*/
    _43emit_op(206);
L1: 

    /** end procedure*/
    DeRef(_24965);
    _24965 = NOVALUE;
    _24967 = NOVALUE;
    return;
    ;
}


void _68Leave_blocks(int _blocks_46917, int _block_type_46918)
{
    int _bx_46919 = NOVALUE;
    int _Block_opcode_3__tmp_at29_46926 = NOVALUE;
    int _Block_opcode_2__tmp_at29_46925 = NOVALUE;
    int _Block_opcode_1__tmp_at29_46924 = NOVALUE;
    int _Block_opcode_inlined_Block_opcode_at_29_46923 = NOVALUE;
    int _24982 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_blocks_46917)) {
        _1 = (long)(DBL_PTR(_blocks_46917)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_46917)) && (DBL_PTR(_blocks_46917)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_46917);
        _blocks_46917 = _1;
    }
    if (!IS_ATOM_INT(_block_type_46918)) {
        _1 = (long)(DBL_PTR(_block_type_46918)->dbl);
        if (UNIQUE(DBL_PTR(_block_type_46918)) && (DBL_PTR(_block_type_46918)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_block_type_46918);
        _block_type_46918 = _1;
    }

    /** 	integer bx = 0*/
    _bx_46919 = 0;

    /** 	while blocks do*/
L1: 
    if (_blocks_46917 == 0)
    {
        goto L2; // [15] 125
    }
    else{
    }

    /** 		Leave_block( bx )*/
    _68Leave_block(_bx_46919);

    /** 		if block_type then*/
    if (_block_type_46918 == 0)
    {
        goto L3; // [25] 107
    }
    else{
    }

    /** 			switch Block_opcode( bx ) do*/

    /** 	return block_stack[$-bx][BLOCK_OPCODE]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _Block_opcode_1__tmp_at29_46924 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _Block_opcode_1__tmp_at29_46924 = 1;
    }
    _Block_opcode_2__tmp_at29_46925 = _Block_opcode_1__tmp_at29_46924 - _bx_46919;
    DeRef(_Block_opcode_3__tmp_at29_46926);
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _Block_opcode_3__tmp_at29_46926 = (int)*(((s1_ptr)_2)->base + _Block_opcode_2__tmp_at29_46925);
    Ref(_Block_opcode_3__tmp_at29_46926);
    DeRef(_Block_opcode_inlined_Block_opcode_at_29_46923);
    _2 = (int)SEQ_PTR(_Block_opcode_3__tmp_at29_46926);
    _Block_opcode_inlined_Block_opcode_at_29_46923 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_Block_opcode_inlined_Block_opcode_at_29_46923);
    DeRef(_Block_opcode_3__tmp_at29_46926);
    _Block_opcode_3__tmp_at29_46926 = NOVALUE;
    if (IS_SEQUENCE(_Block_opcode_inlined_Block_opcode_at_29_46923) ){
        goto L4; // [54] 86
    }
    if(!IS_ATOM_INT(_Block_opcode_inlined_Block_opcode_at_29_46923)){
        if( (DBL_PTR(_Block_opcode_inlined_Block_opcode_at_29_46923)->dbl != (double) ((int) DBL_PTR(_Block_opcode_inlined_Block_opcode_at_29_46923)->dbl) ) ){
            goto L4; // [54] 86
        }
        _0 = (int) DBL_PTR(_Block_opcode_inlined_Block_opcode_at_29_46923)->dbl;
    }
    else {
        _0 = _Block_opcode_inlined_Block_opcode_at_29_46923;
    };
    switch ( _0 ){ 

        /** 				case FOR, WHILE, LOOP then*/
        case 21:
        case 47:
        case 422:

        /** 					if block_type = LOOP_BLOCK then*/
        if (_block_type_46918 != 1)
        goto L5; // [71] 114

        /** 						blocks -= 1*/
        _blocks_46917 = _blocks_46917 - 1;
        goto L5; // [82] 114

        /** 				case else*/
        default:
L4: 

        /** 					if block_type = CONDITIONAL_BLOCK then*/
        if (_block_type_46918 != 2)
        goto L6; // [92] 103

        /** 						blocks -= 1*/
        _blocks_46917 = _blocks_46917 - 1;
L6: 
    ;}    goto L5; // [104] 114
L3: 

    /** 			blocks -= 1*/
    _blocks_46917 = _blocks_46917 - 1;
L5: 

    /** 		bx += 1*/
    _bx_46919 = _bx_46919 + 1;

    /** 	end while*/
    goto L1; // [122] 15
L2: 

    /** 	for i = 0 to blocks - 1 do*/
    _24982 = _blocks_46917 - 1;
    if ((long)((unsigned long)_24982 +(unsigned long) HIGH_BITS) >= 0){
        _24982 = NewDouble((double)_24982);
    }
    {
        int _i_46944;
        _i_46944 = 0;
L7: 
        if (binary_op_a(GREATER, _i_46944, _24982)){
            goto L8; // [131] 150
        }

        /** 		Leave_block( i )*/
        Ref(_i_46944);
        _68Leave_block(_i_46944);

        /** 	end for*/
        _0 = _i_46944;
        if (IS_ATOM_INT(_i_46944)) {
            _i_46944 = _i_46944 + 1;
            if ((long)((unsigned long)_i_46944 +(unsigned long) HIGH_BITS) >= 0){
                _i_46944 = NewDouble((double)_i_46944);
            }
        }
        else {
            _i_46944 = binary_op_a(PLUS, _i_46944, 1);
        }
        DeRef(_0);
        goto L7; // [145] 138
L8: 
        ;
        DeRef(_i_46944);
    }

    /** end procedure*/
    DeRef(_24982);
    _24982 = NOVALUE;
    return;
    ;
}


void _68Drop_block(int _opcode_46948)
{
    int _x_46950 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_opcode_46948)) {
        _1 = (long)(DBL_PTR(_opcode_46948)->dbl);
        if (UNIQUE(DBL_PTR(_opcode_46948)) && (DBL_PTR(_opcode_46948)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_opcode_46948);
        _opcode_46948 = _1;
    }

    /** 	check_block( opcode )*/
    _68check_block(_opcode_46948);

    /** 	symtab_index x = pop_block()*/
    _x_46950 = _68pop_block();
    if (!IS_ATOM_INT(_x_46950)) {
        _1 = (long)(DBL_PTR(_x_46950)->dbl);
        if (UNIQUE(DBL_PTR(_x_46950)) && (DBL_PTR(_x_46950)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_46950);
        _x_46950 = _1;
    }

    /** end procedure*/
    return;
    ;
}


void _68Pop_block_var()
{
    int _sym_46955 = NOVALUE;
    int _block_sym_46962 = NOVALUE;
    int _25010 = NOVALUE;
    int _25009 = NOVALUE;
    int _25008 = NOVALUE;
    int _25007 = NOVALUE;
    int _25006 = NOVALUE;
    int _25005 = NOVALUE;
    int _25004 = NOVALUE;
    int _25003 = NOVALUE;
    int _25001 = NOVALUE;
    int _25000 = NOVALUE;
    int _24998 = NOVALUE;
    int _24997 = NOVALUE;
    int _24995 = NOVALUE;
    int _24992 = NOVALUE;
    int _24990 = NOVALUE;
    int _24989 = NOVALUE;
    int _24987 = NOVALUE;
    int _24986 = NOVALUE;
    int _24985 = NOVALUE;
    int _24984 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index sym = block_stack[$][BLOCK_VARS][$]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24984 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24984 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24985 = (int)*(((s1_ptr)_2)->base + _24984);
    _2 = (int)SEQ_PTR(_24985);
    _24986 = (int)*(((s1_ptr)_2)->base + 6);
    _24985 = NOVALUE;
    if (IS_SEQUENCE(_24986)){
            _24987 = SEQ_PTR(_24986)->length;
    }
    else {
        _24987 = 1;
    }
    _2 = (int)SEQ_PTR(_24986);
    _sym_46955 = (int)*(((s1_ptr)_2)->base + _24987);
    if (!IS_ATOM_INT(_sym_46955)){
        _sym_46955 = (long)DBL_PTR(_sym_46955)->dbl;
    }
    _24986 = NOVALUE;

    /** 	symtab_index block_sym = block_stack[$][BLOCK_SYM]*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _24989 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _24989 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _24990 = (int)*(((s1_ptr)_2)->base + _24989);
    _2 = (int)SEQ_PTR(_24990);
    _block_sym_46962 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_block_sym_46962)){
        _block_sym_46962 = (long)DBL_PTR(_block_sym_46962)->dbl;
    }
    _24990 = NOVALUE;

    /** 	while sym_next_in_block( block_sym ) != sym do*/
L1: 
    _24992 = _55sym_next_in_block(_block_sym_46962);
    if (binary_op_a(EQUALS, _24992, _sym_46955)){
        DeRef(_24992);
        _24992 = NOVALUE;
        goto L2; // [55] 72
    }
    DeRef(_24992);
    _24992 = NOVALUE;

    /** 		block_sym = sym_next_in_block( block_sym )*/
    _block_sym_46962 = _55sym_next_in_block(_block_sym_46962);
    if (!IS_ATOM_INT(_block_sym_46962)) {
        _1 = (long)(DBL_PTR(_block_sym_46962)->dbl);
        if (UNIQUE(DBL_PTR(_block_sym_46962)) && (DBL_PTR(_block_sym_46962)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_block_sym_46962);
        _block_sym_46962 = _1;
    }

    /** 	end while*/
    goto L1; // [69] 51
L2: 

    /** 	SymTab[block_sym][S_NEXT_IN_BLOCK] = sym_next_in_block( sym )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_block_sym_46962 + ((s1_ptr)_2)->base);
    _24997 = _55sym_next_in_block(_sym_46955);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NEXT_IN_BLOCK_16590))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NEXT_IN_BLOCK_16590)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NEXT_IN_BLOCK_16590);
    _1 = *(int *)_2;
    *(int *)_2 = _24997;
    if( _1 != _24997 ){
        DeRef(_1);
    }
    _24997 = NOVALUE;
    _24995 = NOVALUE;

    /** 	SymTab[sym][S_NEXT_IN_BLOCK] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_46955 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NEXT_IN_BLOCK_16590))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NEXT_IN_BLOCK_16590)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NEXT_IN_BLOCK_16590);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _24998 = NOVALUE;

    /** 	block_stack[$][BLOCK_VARS] = eu:remove( block_stack[$][BLOCK_VARS], */
    if (IS_SEQUENCE(_68block_stack_46621)){
            _25000 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _25000 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _68block_stack_46621 = MAKE_SEQ(_2);
    }
    _3 = (int)(_25000 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_68block_stack_46621)){
            _25003 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _25003 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _25004 = (int)*(((s1_ptr)_2)->base + _25003);
    _2 = (int)SEQ_PTR(_25004);
    _25005 = (int)*(((s1_ptr)_2)->base + 6);
    _25004 = NOVALUE;
    if (IS_SEQUENCE(_68block_stack_46621)){
            _25006 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _25006 = 1;
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _25007 = (int)*(((s1_ptr)_2)->base + _25006);
    _2 = (int)SEQ_PTR(_25007);
    _25008 = (int)*(((s1_ptr)_2)->base + 6);
    _25007 = NOVALUE;
    if (IS_SEQUENCE(_25008)){
            _25009 = SEQ_PTR(_25008)->length;
    }
    else {
        _25009 = 1;
    }
    _25008 = NOVALUE;
    {
        s1_ptr assign_space = SEQ_PTR(_25005);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_25009)) ? _25009 : (long)(DBL_PTR(_25009)->dbl);
        int stop = (IS_ATOM_INT(_25009)) ? _25009 : (long)(DBL_PTR(_25009)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
            RefDS(_25005);
            DeRef(_25010);
            _25010 = _25005;
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_25005), start, &_25010 );
            }
            else Tail(SEQ_PTR(_25005), stop+1, &_25010);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_25005), start, &_25010);
        }
        else {
            assign_slice_seq = &assign_space;
            _1 = Remove_elements(start, stop, 0);
            DeRef(_25010);
            _25010 = _1;
        }
    }
    _25005 = NOVALUE;
    _25009 = NOVALUE;
    _25009 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _25010;
    if( _1 != _25010 ){
        DeRef(_1);
    }
    _25010 = NOVALUE;
    _25001 = NOVALUE;

    /** end procedure*/
    _25008 = NOVALUE;
    return;
    ;
}


void _68Goto_block(int _from_block_46996, int _to_block_46998, int _pc_46999)
{
    int _code_47000 = NOVALUE;
    int _next_block_47002 = NOVALUE;
    int _25021 = NOVALUE;
    int _25018 = NOVALUE;
    int _25017 = NOVALUE;
    int _25016 = NOVALUE;
    int _25015 = NOVALUE;
    int _25014 = NOVALUE;
    int _25013 = NOVALUE;
    int _25012 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_block_46996)) {
        _1 = (long)(DBL_PTR(_from_block_46996)->dbl);
        if (UNIQUE(DBL_PTR(_from_block_46996)) && (DBL_PTR(_from_block_46996)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_block_46996);
        _from_block_46996 = _1;
    }
    if (!IS_ATOM_INT(_to_block_46998)) {
        _1 = (long)(DBL_PTR(_to_block_46998)->dbl);
        if (UNIQUE(DBL_PTR(_to_block_46998)) && (DBL_PTR(_to_block_46998)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_to_block_46998);
        _to_block_46998 = _1;
    }
    if (!IS_ATOM_INT(_pc_46999)) {
        _1 = (long)(DBL_PTR(_pc_46999)->dbl);
        if (UNIQUE(DBL_PTR(_pc_46999)) && (DBL_PTR(_pc_46999)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_46999);
        _pc_46999 = _1;
    }

    /** 	sequence code = {}*/
    RefDS(_22663);
    DeRefi(_code_47000);
    _code_47000 = _22663;

    /** 	symtab_index next_block = sym_block( from_block )*/
    _next_block_47002 = _55sym_block(_from_block_46996);
    if (!IS_ATOM_INT(_next_block_47002)) {
        _1 = (long)(DBL_PTR(_next_block_47002)->dbl);
        if (UNIQUE(DBL_PTR(_next_block_47002)) && (DBL_PTR(_next_block_47002)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_block_47002);
        _next_block_47002 = _1;
    }

    /** 	while next_block */
L1: 
    if (_next_block_47002 == 0) {
        _25012 = 0;
        goto L2; // [27] 39
    }
    _25013 = (_from_block_46996 != _to_block_46998);
    _25012 = (_25013 != 0);
L2: 
    if (_25012 == 0) {
        goto L3; // [39] 93
    }
    _25015 = _55sym_token(_next_block_47002);
    _25016 = find_from(_25015, _39RTN_TOKS_16547, 1);
    DeRef(_25015);
    _25015 = NOVALUE;
    _25017 = (_25016 == 0);
    _25016 = NOVALUE;
    if (_25017 == 0)
    {
        DeRef(_25017);
        _25017 = NOVALUE;
        goto L3; // [58] 93
    }
    else{
        DeRef(_25017);
        _25017 = NOVALUE;
    }

    /** 		code &= { EXIT_BLOCK, from_block }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 206;
    ((int *)_2)[2] = _from_block_46996;
    _25018 = MAKE_SEQ(_1);
    Concat((object_ptr)&_code_47000, _code_47000, _25018);
    DeRefDS(_25018);
    _25018 = NOVALUE;

    /** 		from_block = next_block*/
    _from_block_46996 = _next_block_47002;

    /** 		next_block = sym_block( next_block )*/
    _next_block_47002 = _55sym_block(_next_block_47002);
    if (!IS_ATOM_INT(_next_block_47002)) {
        _1 = (long)(DBL_PTR(_next_block_47002)->dbl);
        if (UNIQUE(DBL_PTR(_next_block_47002)) && (DBL_PTR(_next_block_47002)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_block_47002);
        _next_block_47002 = _1;
    }

    /** 	end while*/
    goto L1; // [90] 27
L3: 

    /** 	if length(code) then*/
    if (IS_SEQUENCE(_code_47000)){
            _25021 = SEQ_PTR(_code_47000)->length;
    }
    else {
        _25021 = 1;
    }
    if (_25021 == 0)
    {
        _25021 = NOVALUE;
        goto L4; // [98] 127
    }
    else{
        _25021 = NOVALUE;
    }

    /** 		if pc then*/
    if (_pc_46999 == 0)
    {
        goto L5; // [103] 115
    }
    else{
    }

    /** 			insert_code( code, pc )*/
    RefDS(_code_47000);
    _67insert_code(_code_47000, _pc_46999);
    goto L6; // [112] 126
L5: 

    /** 			Code &= code*/
    Concat((object_ptr)&_38Code_17038, _38Code_17038, _code_47000);
L6: 
L4: 

    /** end procedure*/
    DeRefi(_code_47000);
    DeRef(_25013);
    _25013 = NOVALUE;
    return;
    ;
}


void _68blocks_info()
{
    int _0, _1, _2;
    

    /** 	? block_stack*/
    StdPrint(1, _68block_stack_46621, 1);

    /** end procedure*/
    return;
    ;
}


int _68Least_block()
{
    int _ix_47030 = NOVALUE;
    int _sub_block_47033 = NOVALUE;
    int _25035 = NOVALUE;
    int _25034 = NOVALUE;
    int _25032 = NOVALUE;
    int _25031 = NOVALUE;
    int _25030 = NOVALUE;
    int _25029 = NOVALUE;
    int _25028 = NOVALUE;
    int _25027 = NOVALUE;
    int _25026 = NOVALUE;
    int _25025 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer ix = length( block_stack )*/
    if (IS_SEQUENCE(_68block_stack_46621)){
            _ix_47030 = SEQ_PTR(_68block_stack_46621)->length;
    }
    else {
        _ix_47030 = 1;
    }

    /** 	symtab_index sub_block = sym_block( CurrentSub )*/
    _sub_block_47033 = _55sym_block(_38CurrentSub_16954);
    if (!IS_ATOM_INT(_sub_block_47033)) {
        _1 = (long)(DBL_PTR(_sub_block_47033)->dbl);
        if (UNIQUE(DBL_PTR(_sub_block_47033)) && (DBL_PTR(_sub_block_47033)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_block_47033);
        _sub_block_47033 = _1;
    }

    /** 	while not length( block_stack[ix][BLOCK_VARS] ) */
L1: 
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _25025 = (int)*(((s1_ptr)_2)->base + _ix_47030);
    _2 = (int)SEQ_PTR(_25025);
    _25026 = (int)*(((s1_ptr)_2)->base + 6);
    _25025 = NOVALUE;
    if (IS_SEQUENCE(_25026)){
            _25027 = SEQ_PTR(_25026)->length;
    }
    else {
        _25027 = 1;
    }
    _25026 = NOVALUE;
    _25028 = (_25027 == 0);
    _25027 = NOVALUE;
    if (_25028 == 0) {
        goto L2; // [41] 76
    }
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _25030 = (int)*(((s1_ptr)_2)->base + _ix_47030);
    _2 = (int)SEQ_PTR(_25030);
    _25031 = (int)*(((s1_ptr)_2)->base + 1);
    _25030 = NOVALUE;
    if (IS_ATOM_INT(_25031)) {
        _25032 = (_25031 != _sub_block_47033);
    }
    else {
        _25032 = binary_op(NOTEQ, _25031, _sub_block_47033);
    }
    _25031 = NOVALUE;
    if (_25032 <= 0) {
        if (_25032 == 0) {
            DeRef(_25032);
            _25032 = NOVALUE;
            goto L2; // [62] 76
        }
        else {
            if (!IS_ATOM_INT(_25032) && DBL_PTR(_25032)->dbl == 0.0){
                DeRef(_25032);
                _25032 = NOVALUE;
                goto L2; // [62] 76
            }
            DeRef(_25032);
            _25032 = NOVALUE;
        }
    }
    DeRef(_25032);
    _25032 = NOVALUE;

    /** 		ix -= 1	*/
    _ix_47030 = _ix_47030 - 1;

    /** 	end while*/
    goto L1; // [73] 23
L2: 

    /** 	return block_stack[ix][BLOCK_SYM]*/
    _2 = (int)SEQ_PTR(_68block_stack_46621);
    _25034 = (int)*(((s1_ptr)_2)->base + _ix_47030);
    _2 = (int)SEQ_PTR(_25034);
    _25035 = (int)*(((s1_ptr)_2)->base + 1);
    _25034 = NOVALUE;
    Ref(_25035);
    _25026 = NOVALUE;
    DeRef(_25028);
    _25028 = NOVALUE;
    return _25035;
    ;
}



// 0x77448B76
