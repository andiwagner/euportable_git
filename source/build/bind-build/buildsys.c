// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _57find_file(int _fname_45187)
{
    int _24123 = NOVALUE;
    int _24122 = NOVALUE;
    int _24121 = NOVALUE;
    int _24120 = NOVALUE;
    int _24118 = NOVALUE;
    int _24117 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(inc_dirs) do*/
    _24117 = 6;
    {
        int _i_45189;
        _i_45189 = 1;
L1: 
        if (_i_45189 > 6){
            goto L2; // [10] 64
        }

        /** 		if file_exists(inc_dirs[i] & "/" & fname) then*/
        _2 = (int)SEQ_PTR(_57inc_dirs_45176);
        _24118 = (int)*(((s1_ptr)_2)->base + _i_45189);
        {
            int concat_list[3];

            concat_list[0] = _fname_45187;
            concat_list[1] = _24119;
            concat_list[2] = _24118;
            Concat_N((object_ptr)&_24120, concat_list, 3);
        }
        _24118 = NOVALUE;
        _24121 = _13file_exists(_24120);
        _24120 = NOVALUE;
        if (_24121 == 0) {
            DeRef(_24121);
            _24121 = NOVALUE;
            goto L3; // [35] 57
        }
        else {
            if (!IS_ATOM_INT(_24121) && DBL_PTR(_24121)->dbl == 0.0){
                DeRef(_24121);
                _24121 = NOVALUE;
                goto L3; // [35] 57
            }
            DeRef(_24121);
            _24121 = NOVALUE;
        }
        DeRef(_24121);
        _24121 = NOVALUE;

        /** 			return inc_dirs[i] & "/" & fname*/
        _2 = (int)SEQ_PTR(_57inc_dirs_45176);
        _24122 = (int)*(((s1_ptr)_2)->base + _i_45189);
        {
            int concat_list[3];

            concat_list[0] = _fname_45187;
            concat_list[1] = _24119;
            concat_list[2] = _24122;
            Concat_N((object_ptr)&_24123, concat_list, 3);
        }
        _24122 = NOVALUE;
        DeRefDS(_fname_45187);
        return _24123;
L3: 

        /** 	end for*/
        _i_45189 = _i_45189 + 1;
        goto L1; // [59] 17
L2: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_fname_45187);
    DeRef(_24123);
    _24123 = NOVALUE;
    return 0;
    ;
}


int _57find_all_includes(int _fname_45201, int _includes_45202)
{
    int _lines_45203 = NOVALUE;
    int _m_45209 = NOVALUE;
    int _full_fname_45214 = NOVALUE;
    int _24137 = NOVALUE;
    int _24136 = NOVALUE;
    int _24134 = NOVALUE;
    int _24132 = NOVALUE;
    int _24131 = NOVALUE;
    int _24129 = NOVALUE;
    int _24128 = NOVALUE;
    int _24126 = NOVALUE;
    int _24125 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence lines = read_lines(fname)*/
    RefDS(_fname_45201);
    _0 = _lines_45203;
    _lines_45203 = _16read_lines(_fname_45201);
    DeRef(_0);

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_45203)){
            _24125 = SEQ_PTR(_lines_45203)->length;
    }
    else {
        _24125 = 1;
    }
    {
        int _i_45207;
        _i_45207 = 1;
L1: 
        if (_i_45207 > _24125){
            goto L2; // [18] 116
        }

        /** 		object m = regex:matches(re_include, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_45203);
        _24126 = (int)*(((s1_ptr)_2)->base + _i_45207);
        Ref(_57re_include_45173);
        Ref(_24126);
        _0 = _m_45209;
        _m_45209 = _53matches(_57re_include_45173, _24126, 1, 0);
        DeRef(_0);
        _24126 = NOVALUE;

        /** 		if sequence(m) then*/
        _24128 = IS_SEQUENCE(_m_45209);
        if (_24128 == 0)
        {
            _24128 = NOVALUE;
            goto L3; // [45] 105
        }
        else{
            _24128 = NOVALUE;
        }

        /** 			object full_fname = find_file(m[3])*/
        _2 = (int)SEQ_PTR(_m_45209);
        _24129 = (int)*(((s1_ptr)_2)->base + 3);
        Ref(_24129);
        _0 = _full_fname_45214;
        _full_fname_45214 = _57find_file(_24129);
        DeRef(_0);
        _24129 = NOVALUE;

        /** 			if sequence(full_fname) then*/
        _24131 = IS_SEQUENCE(_full_fname_45214);
        if (_24131 == 0)
        {
            _24131 = NOVALUE;
            goto L4; // [63] 104
        }
        else{
            _24131 = NOVALUE;
        }

        /** 				if eu:find(full_fname, includes) = 0 then*/
        _24132 = find_from(_full_fname_45214, _includes_45202, 1);
        if (_24132 != 0)
        goto L5; // [73] 103

        /** 					includes &= { full_fname }*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_full_fname_45214);
        *((int *)(_2+4)) = _full_fname_45214;
        _24134 = MAKE_SEQ(_1);
        Concat((object_ptr)&_includes_45202, _includes_45202, _24134);
        DeRefDS(_24134);
        _24134 = NOVALUE;

        /** 					includes = find_all_includes(full_fname, includes)*/
        Ref(_full_fname_45214);
        DeRef(_24136);
        _24136 = _full_fname_45214;
        RefDS(_includes_45202);
        DeRef(_24137);
        _24137 = _includes_45202;
        _0 = _includes_45202;
        _includes_45202 = _57find_all_includes(_24136, _24137);
        DeRefDS(_0);
        _24136 = NOVALUE;
        _24137 = NOVALUE;
L5: 
L4: 
L3: 
        DeRef(_full_fname_45214);
        _full_fname_45214 = NOVALUE;
        DeRef(_m_45209);
        _m_45209 = NOVALUE;

        /** 	end for*/
        _i_45207 = _i_45207 + 1;
        goto L1; // [111] 25
L2: 
        ;
    }

    /** 	return includes*/
    DeRefDS(_fname_45201);
    DeRef(_lines_45203);
    return _includes_45202;
    ;
}


int _57quick_has_changed(int _fname_45229)
{
    int _d1_45230 = NOVALUE;
    int _all_files_45240 = NOVALUE;
    int _d2_45246 = NOVALUE;
    int _diff_2__tmp_at88_45256 = NOVALUE;
    int _diff_1__tmp_at88_45255 = NOVALUE;
    int _diff_inlined_diff_at_88_45254 = NOVALUE;
    int _24149 = NOVALUE;
    int _24147 = NOVALUE;
    int _24146 = NOVALUE;
    int _24144 = NOVALUE;
    int _24143 = NOVALUE;
    int _24141 = NOVALUE;
    int _24139 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d1 = file_timestamp(output_dir & filebase(fname) & ".bld")*/
    RefDS(_fname_45229);
    _24139 = _13filebase(_fname_45229);
    {
        int concat_list[3];

        concat_list[0] = _24140;
        concat_list[1] = _24139;
        concat_list[2] = _59output_dir_42630;
        Concat_N((object_ptr)&_24141, concat_list, 3);
    }
    DeRef(_24139);
    _24139 = NOVALUE;
    _0 = _d1_45230;
    _d1_45230 = _13file_timestamp(_24141);
    DeRef(_0);
    _24141 = NOVALUE;

    /** 	if atom(d1) then*/
    _24143 = IS_ATOM(_d1_45230);
    if (_24143 == 0)
    {
        _24143 = NOVALUE;
        goto L1; // [26] 36
    }
    else{
        _24143 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_fname_45229);
    DeRef(_d1_45230);
    DeRef(_all_files_45240);
    return 1;
L1: 

    /** 	sequence all_files = append(find_all_includes(fname), fname)*/
    RefDS(_fname_45229);
    RefDS(_22663);
    _24144 = _57find_all_includes(_fname_45229, _22663);
    RefDS(_fname_45229);
    Append(&_all_files_45240, _24144, _fname_45229);
    DeRef(_24144);
    _24144 = NOVALUE;

    /** 	for i = 1 to length(all_files) do*/
    if (IS_SEQUENCE(_all_files_45240)){
            _24146 = SEQ_PTR(_all_files_45240)->length;
    }
    else {
        _24146 = 1;
    }
    {
        int _i_45244;
        _i_45244 = 1;
L2: 
        if (_i_45244 > _24146){
            goto L3; // [52] 123
        }

        /** 		object d2 = file_timestamp(all_files[i])*/
        _2 = (int)SEQ_PTR(_all_files_45240);
        _24147 = (int)*(((s1_ptr)_2)->base + _i_45244);
        Ref(_24147);
        _0 = _d2_45246;
        _d2_45246 = _13file_timestamp(_24147);
        DeRef(_0);
        _24147 = NOVALUE;

        /** 		if atom(d2) then*/
        _24149 = IS_ATOM(_d2_45246);
        if (_24149 == 0)
        {
            _24149 = NOVALUE;
            goto L4; // [74] 84
        }
        else{
            _24149 = NOVALUE;
        }

        /** 			return 1*/
        DeRef(_d2_45246);
        DeRefDS(_fname_45229);
        DeRef(_d1_45230);
        DeRefDS(_all_files_45240);
        return 1;
L4: 

        /** 		if datetime:diff(d1, d2) > 0 then*/

        /** 	return datetimeToSeconds(dt2) - datetimeToSeconds(dt1)*/
        Ref(_d2_45246);
        _0 = _diff_1__tmp_at88_45255;
        _diff_1__tmp_at88_45255 = _14datetimeToSeconds(_d2_45246);
        DeRef(_0);
        Ref(_d1_45230);
        _0 = _diff_2__tmp_at88_45256;
        _diff_2__tmp_at88_45256 = _14datetimeToSeconds(_d1_45230);
        DeRef(_0);
        DeRef(_diff_inlined_diff_at_88_45254);
        if (IS_ATOM_INT(_diff_1__tmp_at88_45255) && IS_ATOM_INT(_diff_2__tmp_at88_45256)) {
            _diff_inlined_diff_at_88_45254 = _diff_1__tmp_at88_45255 - _diff_2__tmp_at88_45256;
            if ((long)((unsigned long)_diff_inlined_diff_at_88_45254 +(unsigned long) HIGH_BITS) >= 0){
                _diff_inlined_diff_at_88_45254 = NewDouble((double)_diff_inlined_diff_at_88_45254);
            }
        }
        else {
            _diff_inlined_diff_at_88_45254 = binary_op(MINUS, _diff_1__tmp_at88_45255, _diff_2__tmp_at88_45256);
        }
        DeRef(_diff_1__tmp_at88_45255);
        _diff_1__tmp_at88_45255 = NOVALUE;
        DeRef(_diff_2__tmp_at88_45256);
        _diff_2__tmp_at88_45256 = NOVALUE;
        if (binary_op_a(LESSEQ, _diff_inlined_diff_at_88_45254, 0)){
            goto L5; // [103] 114
        }

        /** 			return 1*/
        DeRef(_d2_45246);
        DeRefDS(_fname_45229);
        DeRef(_d1_45230);
        DeRef(_all_files_45240);
        return 1;
L5: 
        DeRef(_d2_45246);
        _d2_45246 = NOVALUE;

        /** 	end for*/
        _i_45244 = _i_45244 + 1;
        goto L2; // [118] 59
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_fname_45229);
    DeRef(_d1_45230);
    DeRef(_all_files_45240);
    return 0;
    ;
}


void _57update_checksum(int _raw_data_45310)
{
    int _24169 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cfile_check = xor_bits(cfile_check, hash( raw_data, stdhash:HSIEH32))*/
    _24169 = calc_hash(_raw_data_45310, -5);
    _0 = _57cfile_check_45287;
    if (IS_ATOM_INT(_57cfile_check_45287) && IS_ATOM_INT(_24169)) {
        {unsigned long tu;
             tu = (unsigned long)_57cfile_check_45287 ^ (unsigned long)_24169;
             _57cfile_check_45287 = MAKE_UINT(tu);
        }
    }
    else {
        if (IS_ATOM_INT(_57cfile_check_45287)) {
            temp_d.dbl = (double)_57cfile_check_45287;
            _57cfile_check_45287 = Dxor_bits(&temp_d, DBL_PTR(_24169));
        }
        else {
            if (IS_ATOM_INT(_24169)) {
                temp_d.dbl = (double)_24169;
                _57cfile_check_45287 = Dxor_bits(DBL_PTR(_57cfile_check_45287), &temp_d);
            }
            else
            _57cfile_check_45287 = Dxor_bits(DBL_PTR(_57cfile_check_45287), DBL_PTR(_24169));
        }
    }
    DeRef(_0);
    DeRef(_24169);
    _24169 = NOVALUE;

    /** end procedure*/
    DeRef(_raw_data_45310);
    return;
    ;
}


void _57write_checksum(int _file_45315)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_file_45315)) {
        _1 = (long)(DBL_PTR(_file_45315)->dbl);
        if (UNIQUE(DBL_PTR(_file_45315)) && (DBL_PTR(_file_45315)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_45315);
        _file_45315 = _1;
    }

    /** 	printf( file, "\n// 0x%08x\n", cfile_check )*/
    EPrintf(_file_45315, _24171, _57cfile_check_45287);

    /** 	cfile_check = 0*/
    DeRef(_57cfile_check_45287);
    _57cfile_check_45287 = 0;

    /** end procedure*/
    return;
    ;
}


int _57find_file_element(int _needle_45319, int _files_45320)
{
    int _24187 = NOVALUE;
    int _24186 = NOVALUE;
    int _24185 = NOVALUE;
    int _24184 = NOVALUE;
    int _24183 = NOVALUE;
    int _24182 = NOVALUE;
    int _24181 = NOVALUE;
    int _24180 = NOVALUE;
    int _24179 = NOVALUE;
    int _24178 = NOVALUE;
    int _24177 = NOVALUE;
    int _24176 = NOVALUE;
    int _24175 = NOVALUE;
    int _24174 = NOVALUE;
    int _24173 = NOVALUE;
    int _24172 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for j = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_45320)){
            _24172 = SEQ_PTR(_files_45320)->length;
    }
    else {
        _24172 = 1;
    }
    {
        int _j_45322;
        _j_45322 = 1;
L1: 
        if (_j_45322 > _24172){
            goto L2; // [10] 50
        }

        /** 		if equal(files[j][D_NAME],needle) then*/
        _2 = (int)SEQ_PTR(_files_45320);
        _24173 = (int)*(((s1_ptr)_2)->base + _j_45322);
        _2 = (int)SEQ_PTR(_24173);
        _24174 = (int)*(((s1_ptr)_2)->base + 1);
        _24173 = NOVALUE;
        if (_24174 == _needle_45319)
        _24175 = 1;
        else if (IS_ATOM_INT(_24174) && IS_ATOM_INT(_needle_45319))
        _24175 = 0;
        else
        _24175 = (compare(_24174, _needle_45319) == 0);
        _24174 = NOVALUE;
        if (_24175 == 0)
        {
            _24175 = NOVALUE;
            goto L3; // [33] 43
        }
        else{
            _24175 = NOVALUE;
        }

        /** 			return j*/
        DeRefDS(_needle_45319);
        DeRefDS(_files_45320);
        return _j_45322;
L3: 

        /** 	end for*/
        _j_45322 = _j_45322 + 1;
        goto L1; // [45] 17
L2: 
        ;
    }

    /** 	for j = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_45320)){
            _24176 = SEQ_PTR(_files_45320)->length;
    }
    else {
        _24176 = 1;
    }
    {
        int _j_45330;
        _j_45330 = 1;
L4: 
        if (_j_45330 > _24176){
            goto L5; // [55] 103
        }

        /** 		if equal(lower(files[j][D_NAME]),lower(needle)) then*/
        _2 = (int)SEQ_PTR(_files_45320);
        _24177 = (int)*(((s1_ptr)_2)->base + _j_45330);
        _2 = (int)SEQ_PTR(_24177);
        _24178 = (int)*(((s1_ptr)_2)->base + 1);
        _24177 = NOVALUE;
        Ref(_24178);
        _24179 = _10lower(_24178);
        _24178 = NOVALUE;
        RefDS(_needle_45319);
        _24180 = _10lower(_needle_45319);
        if (_24179 == _24180)
        _24181 = 1;
        else if (IS_ATOM_INT(_24179) && IS_ATOM_INT(_24180))
        _24181 = 0;
        else
        _24181 = (compare(_24179, _24180) == 0);
        DeRef(_24179);
        _24179 = NOVALUE;
        DeRef(_24180);
        _24180 = NOVALUE;
        if (_24181 == 0)
        {
            _24181 = NOVALUE;
            goto L6; // [86] 96
        }
        else{
            _24181 = NOVALUE;
        }

        /** 			return j*/
        DeRefDS(_needle_45319);
        DeRefDS(_files_45320);
        return _j_45330;
L6: 

        /** 	end for*/
        _j_45330 = _j_45330 + 1;
        goto L4; // [98] 62
L5: 
        ;
    }

    /** 	for j = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_45320)){
            _24182 = SEQ_PTR(_files_45320)->length;
    }
    else {
        _24182 = 1;
    }
    {
        int _j_45342;
        _j_45342 = 1;
L7: 
        if (_j_45342 > _24182){
            goto L8; // [108] 156
        }

        /** 		if equal(lower(files[j][D_ALTNAME]),lower(needle)) then*/
        _2 = (int)SEQ_PTR(_files_45320);
        _24183 = (int)*(((s1_ptr)_2)->base + _j_45342);
        _2 = (int)SEQ_PTR(_24183);
        _24184 = (int)*(((s1_ptr)_2)->base + 11);
        _24183 = NOVALUE;
        Ref(_24184);
        _24185 = _10lower(_24184);
        _24184 = NOVALUE;
        RefDS(_needle_45319);
        _24186 = _10lower(_needle_45319);
        if (_24185 == _24186)
        _24187 = 1;
        else if (IS_ATOM_INT(_24185) && IS_ATOM_INT(_24186))
        _24187 = 0;
        else
        _24187 = (compare(_24185, _24186) == 0);
        DeRef(_24185);
        _24185 = NOVALUE;
        DeRef(_24186);
        _24186 = NOVALUE;
        if (_24187 == 0)
        {
            _24187 = NOVALUE;
            goto L9; // [139] 149
        }
        else{
            _24187 = NOVALUE;
        }

        /** 			return j*/
        DeRefDS(_needle_45319);
        DeRefDS(_files_45320);
        return _j_45342;
L9: 

        /** 	end for*/
        _j_45342 = _j_45342 + 1;
        goto L7; // [151] 115
L8: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_needle_45319);
    DeRefDS(_files_45320);
    return 0;
    ;
}


int _57adjust_for_command_line_passing(int _long_path_45363)
{
    int _slash_45364 = NOVALUE;
    int _longs_45372 = NOVALUE;
    int _short_path_45378 = NOVALUE;
    int _files_45384 = NOVALUE;
    int _file_location_45387 = NOVALUE;
    int _24226 = NOVALUE;
    int _24225 = NOVALUE;
    int _24223 = NOVALUE;
    int _24222 = NOVALUE;
    int _24220 = NOVALUE;
    int _24219 = NOVALUE;
    int _24217 = NOVALUE;
    int _24216 = NOVALUE;
    int _24213 = NOVALUE;
    int _24212 = NOVALUE;
    int _24210 = NOVALUE;
    int _24209 = NOVALUE;
    int _24208 = NOVALUE;
    int _24207 = NOVALUE;
    int _24206 = NOVALUE;
    int _24204 = NOVALUE;
    int _24203 = NOVALUE;
    int _24201 = NOVALUE;
    int _24199 = NOVALUE;
    int _24197 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if compiler_type = COMPILER_GCC then*/

    /** 	elsif compiler_type = COMPILER_WATCOM then*/

    /** 		slash = SLASH*/
    _slash_45364 = 92;

    /** 	ifdef UNIX then*/

    /** 		long_path = regex:find_replace(quote_pattern, long_path, "")*/
    Ref(_57quote_pattern_45356);
    RefDS(_long_path_45363);
    RefDS(_22663);
    _0 = _long_path_45363;
    _long_path_45363 = _53find_replace(_57quote_pattern_45356, _long_path_45363, _22663, 1, 0);
    DeRefDS(_0);

    /** 		sequence longs = split( slash_pattern, long_path )*/
    Ref(_57slash_pattern_45353);
    RefDS(_long_path_45363);
    _0 = _longs_45372;
    _longs_45372 = _53split(_57slash_pattern_45353, _long_path_45363, 1, 0);
    DeRef(_0);

    /** 		if length(longs)=0 then*/
    if (IS_SEQUENCE(_longs_45372)){
            _24197 = SEQ_PTR(_longs_45372)->length;
    }
    else {
        _24197 = 1;
    }
    if (_24197 != 0)
    goto L1; // [83] 94

    /** 			return long_path*/
    DeRefDS(_longs_45372);
    DeRef(_short_path_45378);
    return _long_path_45363;
L1: 

    /** 		sequence short_path = longs[1] & slash*/
    _2 = (int)SEQ_PTR(_longs_45372);
    _24199 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_24199) && IS_ATOM(_slash_45364)) {
        Append(&_short_path_45378, _24199, _slash_45364);
    }
    else if (IS_ATOM(_24199) && IS_SEQUENCE(_slash_45364)) {
    }
    else {
        Concat((object_ptr)&_short_path_45378, _24199, _slash_45364);
        _24199 = NOVALUE;
    }
    _24199 = NOVALUE;

    /** 		for i = 2 to length(longs) do*/
    if (IS_SEQUENCE(_longs_45372)){
            _24201 = SEQ_PTR(_longs_45372)->length;
    }
    else {
        _24201 = 1;
    }
    {
        int _i_45382;
        _i_45382 = 2;
L2: 
        if (_i_45382 > _24201){
            goto L3; // [111] 270
        }

        /** 			object files = dir(short_path)*/
        RefDS(_short_path_45378);
        _0 = _files_45384;
        _files_45384 = _13dir(_short_path_45378);
        DeRef(_0);

        /** 			integer file_location = 0*/
        _file_location_45387 = 0;

        /** 			if sequence(files) then*/
        _24203 = IS_SEQUENCE(_files_45384);
        if (_24203 == 0)
        {
            _24203 = NOVALUE;
            goto L4; // [134] 151
        }
        else{
            _24203 = NOVALUE;
        }

        /** 				file_location = find_file_element(longs[i], files)*/
        _2 = (int)SEQ_PTR(_longs_45372);
        _24204 = (int)*(((s1_ptr)_2)->base + _i_45382);
        Ref(_24204);
        Ref(_files_45384);
        _file_location_45387 = _57find_file_element(_24204, _files_45384);
        _24204 = NOVALUE;
        if (!IS_ATOM_INT(_file_location_45387)) {
            _1 = (long)(DBL_PTR(_file_location_45387)->dbl);
            if (UNIQUE(DBL_PTR(_file_location_45387)) && (DBL_PTR(_file_location_45387)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_file_location_45387);
            _file_location_45387 = _1;
        }
L4: 

        /** 			if file_location then*/
        if (_file_location_45387 == 0)
        {
            goto L5; // [153] 219
        }
        else{
        }

        /** 				if sequence(files[file_location][D_ALTNAME]) then*/
        _2 = (int)SEQ_PTR(_files_45384);
        _24206 = (int)*(((s1_ptr)_2)->base + _file_location_45387);
        _2 = (int)SEQ_PTR(_24206);
        _24207 = (int)*(((s1_ptr)_2)->base + 11);
        _24206 = NOVALUE;
        _24208 = IS_SEQUENCE(_24207);
        _24207 = NOVALUE;
        if (_24208 == 0)
        {
            _24208 = NOVALUE;
            goto L6; // [171] 193
        }
        else{
            _24208 = NOVALUE;
        }

        /** 					short_path &= files[file_location][D_ALTNAME]*/
        _2 = (int)SEQ_PTR(_files_45384);
        _24209 = (int)*(((s1_ptr)_2)->base + _file_location_45387);
        _2 = (int)SEQ_PTR(_24209);
        _24210 = (int)*(((s1_ptr)_2)->base + 11);
        _24209 = NOVALUE;
        if (IS_SEQUENCE(_short_path_45378) && IS_ATOM(_24210)) {
            Ref(_24210);
            Append(&_short_path_45378, _short_path_45378, _24210);
        }
        else if (IS_ATOM(_short_path_45378) && IS_SEQUENCE(_24210)) {
        }
        else {
            Concat((object_ptr)&_short_path_45378, _short_path_45378, _24210);
        }
        _24210 = NOVALUE;
        goto L7; // [190] 210
L6: 

        /** 					short_path &= files[file_location][D_NAME]*/
        _2 = (int)SEQ_PTR(_files_45384);
        _24212 = (int)*(((s1_ptr)_2)->base + _file_location_45387);
        _2 = (int)SEQ_PTR(_24212);
        _24213 = (int)*(((s1_ptr)_2)->base + 1);
        _24212 = NOVALUE;
        if (IS_SEQUENCE(_short_path_45378) && IS_ATOM(_24213)) {
            Ref(_24213);
            Append(&_short_path_45378, _short_path_45378, _24213);
        }
        else if (IS_ATOM(_short_path_45378) && IS_SEQUENCE(_24213)) {
        }
        else {
            Concat((object_ptr)&_short_path_45378, _short_path_45378, _24213);
        }
        _24213 = NOVALUE;
L7: 

        /** 				short_path &= slash*/
        Append(&_short_path_45378, _short_path_45378, _slash_45364);
        goto L8; // [216] 261
L5: 

        /** 				if not find(' ',longs[i]) then*/
        _2 = (int)SEQ_PTR(_longs_45372);
        _24216 = (int)*(((s1_ptr)_2)->base + _i_45382);
        _24217 = find_from(32, _24216, 1);
        _24216 = NOVALUE;
        if (_24217 != 0)
        goto L9; // [230] 254
        _24217 = NOVALUE;

        /** 					short_path &= longs[i] & slash*/
        _2 = (int)SEQ_PTR(_longs_45372);
        _24219 = (int)*(((s1_ptr)_2)->base + _i_45382);
        if (IS_SEQUENCE(_24219) && IS_ATOM(_slash_45364)) {
            Append(&_24220, _24219, _slash_45364);
        }
        else if (IS_ATOM(_24219) && IS_SEQUENCE(_slash_45364)) {
        }
        else {
            Concat((object_ptr)&_24220, _24219, _slash_45364);
            _24219 = NOVALUE;
        }
        _24219 = NOVALUE;
        Concat((object_ptr)&_short_path_45378, _short_path_45378, _24220);
        DeRefDS(_24220);
        _24220 = NOVALUE;

        /** 					continue*/
        DeRef(_files_45384);
        _files_45384 = NOVALUE;
        goto LA; // [251] 265
L9: 

        /** 				return 0*/
        DeRef(_files_45384);
        DeRefDS(_long_path_45363);
        DeRef(_longs_45372);
        DeRef(_short_path_45378);
        return 0;
L8: 
        DeRef(_files_45384);
        _files_45384 = NOVALUE;

        /** 		end for -- i*/
LA: 
        _i_45382 = _i_45382 + 1;
        goto L2; // [265] 118
L3: 
        ;
    }

    /** 		if short_path[$] = slash then*/
    if (IS_SEQUENCE(_short_path_45378)){
            _24222 = SEQ_PTR(_short_path_45378)->length;
    }
    else {
        _24222 = 1;
    }
    _2 = (int)SEQ_PTR(_short_path_45378);
    _24223 = (int)*(((s1_ptr)_2)->base + _24222);
    if (binary_op_a(NOTEQ, _24223, _slash_45364)){
        _24223 = NOVALUE;
        goto LB; // [279] 298
    }
    _24223 = NOVALUE;

    /** 			short_path = short_path[1..$-1]*/
    if (IS_SEQUENCE(_short_path_45378)){
            _24225 = SEQ_PTR(_short_path_45378)->length;
    }
    else {
        _24225 = 1;
    }
    _24226 = _24225 - 1;
    _24225 = NOVALUE;
    rhs_slice_target = (object_ptr)&_short_path_45378;
    RHS_Slice(_short_path_45378, 1, _24226);
LB: 

    /** 		return short_path*/
    DeRefDS(_long_path_45363);
    DeRef(_longs_45372);
    DeRef(_24226);
    _24226 = NOVALUE;
    return _short_path_45378;
    ;
}


int _57adjust_for_build_file(int _long_path_45425)
{
    int _short_path_45426 = NOVALUE;
    int _24234 = NOVALUE;
    int _24233 = NOVALUE;
    int _24232 = NOVALUE;
    int _24231 = NOVALUE;
    int _24230 = NOVALUE;
    int _24229 = NOVALUE;
    int _0, _1, _2;
    

    /**     object short_path = adjust_for_command_line_passing(long_path)*/
    RefDS(_long_path_45425);
    _0 = _short_path_45426;
    _short_path_45426 = _57adjust_for_command_line_passing(_long_path_45425);
    DeRef(_0);

    /**     if atom(short_path) then*/
    _24229 = IS_ATOM(_short_path_45426);
    if (_24229 == 0)
    {
        _24229 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _24229 = NOVALUE;
    }

    /**     	return short_path*/
    DeRefDS(_long_path_45425);
    return _short_path_45426;
L1: 

    /** 	if compiler_type = COMPILER_GCC and build_system_type != BUILD_DIRECT and TWINDOWS then*/
    _24230 = (0 == 1);
    if (_24230 == 0) {
        _24231 = 0;
        goto L2; // [34] 50
    }
    _24232 = (3 != 3);
    _24231 = (_24232 != 0);
L2: 
    if (_24231 == 0) {
        goto L3; // [50] 73
    }
    if (_42TWINDOWS_17110 == 0)
    {
        goto L3; // [57] 73
    }
    else{
    }

    /** 		return windows_to_mingw_path(short_path)*/
    Ref(_short_path_45426);
    _24234 = _57windows_to_mingw_path(_short_path_45426);
    DeRefDS(_long_path_45425);
    DeRef(_short_path_45426);
    DeRef(_24230);
    _24230 = NOVALUE;
    DeRef(_24232);
    _24232 = NOVALUE;
    return _24234;
    goto L4; // [70] 80
L3: 

    /** 		return short_path*/
    DeRefDS(_long_path_45425);
    DeRef(_24230);
    _24230 = NOVALUE;
    DeRef(_24232);
    _24232 = NOVALUE;
    DeRef(_24234);
    _24234 = NOVALUE;
    return _short_path_45426;
L4: 
    ;
}


int _57setup_build()
{
    int _c_exe_45441 = NOVALUE;
    int _c_flags_45442 = NOVALUE;
    int _l_exe_45443 = NOVALUE;
    int _l_flags_45444 = NOVALUE;
    int _obj_ext_45445 = NOVALUE;
    int _exe_ext_45446 = NOVALUE;
    int _l_flags_begin_45447 = NOVALUE;
    int _rc_comp_45448 = NOVALUE;
    int _l_names_45449 = NOVALUE;
    int _l_ext_45450 = NOVALUE;
    int _t_slash_45451 = NOVALUE;
    int _eudir_45470 = NOVALUE;
    int _compile_dir_45528 = NOVALUE;
    int _24360 = NOVALUE;
    int _24359 = NOVALUE;
    int _24358 = NOVALUE;
    int _24355 = NOVALUE;
    int _24354 = NOVALUE;
    int _24351 = NOVALUE;
    int _24350 = NOVALUE;
    int _24343 = NOVALUE;
    int _24342 = NOVALUE;
    int _24337 = NOVALUE;
    int _24336 = NOVALUE;
    int _24331 = NOVALUE;
    int _24330 = NOVALUE;
    int _24327 = NOVALUE;
    int _24326 = NOVALUE;
    int _24316 = NOVALUE;
    int _24315 = NOVALUE;
    int _24300 = NOVALUE;
    int _24299 = NOVALUE;
    int _24295 = NOVALUE;
    int _24293 = NOVALUE;
    int _24292 = NOVALUE;
    int _24291 = NOVALUE;
    int _24290 = NOVALUE;
    int _24278 = NOVALUE;
    int _24277 = NOVALUE;
    int _24274 = NOVALUE;
    int _24262 = NOVALUE;
    int _24258 = NOVALUE;
    int _24255 = NOVALUE;
    int _24254 = NOVALUE;
    int _24253 = NOVALUE;
    int _24250 = NOVALUE;
    int _24249 = NOVALUE;
    int _24248 = NOVALUE;
    int _24245 = NOVALUE;
    int _24242 = NOVALUE;
    int _24235 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence c_exe   = "", c_flags = "", l_exe   = "", l_flags = "", obj_ext = "",*/
    RefDS(_22663);
    DeRefi(_c_exe_45441);
    _c_exe_45441 = _22663;
    RefDS(_22663);
    DeRef(_c_flags_45442);
    _c_flags_45442 = _22663;
    RefDS(_22663);
    DeRefi(_l_exe_45443);
    _l_exe_45443 = _22663;
    RefDS(_22663);
    DeRefi(_l_flags_45444);
    _l_flags_45444 = _22663;
    RefDS(_22663);
    DeRefi(_obj_ext_45445);
    _obj_ext_45445 = _22663;

    /** 		exe_ext = "", l_flags_begin = "", rc_comp = "", l_names, l_ext, t_slash*/
    RefDS(_22663);
    DeRefi(_exe_ext_45446);
    _exe_ext_45446 = _22663;
    RefDS(_22663);
    DeRefi(_l_flags_begin_45447);
    _l_flags_begin_45447 = _22663;
    RefDS(_22663);
    DeRef(_rc_comp_45448);
    _rc_comp_45448 = _22663;

    /** 	if length(user_library) = 0 then*/
    if (IS_SEQUENCE(_59user_library_42620)){
            _24235 = SEQ_PTR(_59user_library_42620)->length;
    }
    else {
        _24235 = 1;
    }
    if (_24235 != 0)
    goto L1; // [52] 261

    /** 		if debug_option then*/
    if (_59debug_option_42618 == 0)
    {
        goto L2; // [60] 72
    }
    else{
    }

    /** 			l_names = { "eudbg", "eu" }*/
    RefDS(_24238);
    RefDS(_24237);
    DeRef(_l_names_45449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24237;
    ((int *)_2)[2] = _24238;
    _l_names_45449 = MAKE_SEQ(_1);
    goto L3; // [69] 79
L2: 

    /** 			l_names = { "eu", "eudbg" }*/
    RefDS(_24237);
    RefDS(_24238);
    DeRef(_l_names_45449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24238;
    ((int *)_2)[2] = _24237;
    _l_names_45449 = MAKE_SEQ(_1);
L3: 

    /** 		if TUNIX or compiler_type = COMPILER_GCC then*/
    if (0 != 0) {
        goto L4; // [83] 100
    }
    _24242 = (0 == 1);
    if (_24242 == 0)
    {
        DeRef(_24242);
        _24242 = NOVALUE;
        goto L5; // [96] 117
    }
    else{
        DeRef(_24242);
        _24242 = NOVALUE;
    }
L4: 

    /** 			l_ext = "a"*/
    RefDS(_22901);
    DeRefi(_l_ext_45450);
    _l_ext_45450 = _22901;

    /** 			t_slash = "/"*/
    RefDS(_24119);
    DeRefi(_t_slash_45451);
    _t_slash_45451 = _24119;
    goto L6; // [114] 140
L5: 

    /** 		elsif TWINDOWS then*/
    if (_42TWINDOWS_17110 == 0)
    {
        goto L7; // [121] 139
    }
    else{
    }

    /** 			l_ext = "lib"*/
    RefDS(_24243);
    DeRefi(_l_ext_45450);
    _l_ext_45450 = _24243;

    /** 			t_slash = "\\"*/
    RefDS(_22990);
    DeRefi(_t_slash_45451);
    _t_slash_45451 = _22990;
L7: 
L6: 

    /** 		object eudir = get_eucompiledir()*/
    _0 = _eudir_45470;
    _eudir_45470 = _59get_eucompiledir();
    DeRef(_0);

    /** 		if not file_exists(eudir) then*/
    Ref(_eudir_45470);
    _24245 = _13file_exists(_eudir_45470);
    if (IS_ATOM_INT(_24245)) {
        if (_24245 != 0){
            DeRef(_24245);
            _24245 = NOVALUE;
            goto L8; // [151] 172
        }
    }
    else {
        if (DBL_PTR(_24245)->dbl != 0.0){
            DeRef(_24245);
            _24245 = NOVALUE;
            goto L8; // [151] 172
        }
    }
    DeRef(_24245);
    _24245 = NOVALUE;

    /** 			printf(2,"Supplied directory \'%s\' is not a valid EUDIR\n",{get_eucompiledir()})*/
    _24248 = _59get_eucompiledir();
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24248;
    _24249 = MAKE_SEQ(_1);
    _24248 = NOVALUE;
    EPrintf(2, _24247, _24249);
    DeRefDS(_24249);
    _24249 = NOVALUE;

    /** 			abort(1)*/
    UserCleanup(1);
L8: 

    /** 		for tk = 1 to length(l_names) label "translation kind" do*/
    _24250 = 2;
    {
        int _tk_45482;
        _tk_45482 = 1;
L9: 
        if (_tk_45482 > 2){
            goto LA; // [179] 260
        }

        /** 			user_library = eudir & sprintf("%sbin%s%s.%s",{t_slash, t_slash, l_names[tk],l_ext})*/
        _2 = (int)SEQ_PTR(_l_names_45449);
        _24253 = (int)*(((s1_ptr)_2)->base + _tk_45482);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        RefDSn(_t_slash_45451, 2);
        *((int *)(_2+4)) = _t_slash_45451;
        *((int *)(_2+8)) = _t_slash_45451;
        RefDS(_24253);
        *((int *)(_2+12)) = _24253;
        RefDS(_l_ext_45450);
        *((int *)(_2+16)) = _l_ext_45450;
        _24254 = MAKE_SEQ(_1);
        _24253 = NOVALUE;
        _24255 = EPrintf(-9999999, _24252, _24254);
        DeRefDS(_24254);
        _24254 = NOVALUE;
        if (IS_SEQUENCE(_eudir_45470) && IS_ATOM(_24255)) {
        }
        else if (IS_ATOM(_eudir_45470) && IS_SEQUENCE(_24255)) {
            Ref(_eudir_45470);
            Prepend(&_59user_library_42620, _24255, _eudir_45470);
        }
        else {
            Concat((object_ptr)&_59user_library_42620, _eudir_45470, _24255);
        }
        DeRefDS(_24255);
        _24255 = NOVALUE;

        /** 			if TUNIX or compiler_type = COMPILER_GCC then*/
        if (0 != 0) {
            goto LB; // [217] 234
        }
        _24258 = (0 == 1);
        if (_24258 == 0)
        {
            DeRef(_24258);
            _24258 = NOVALUE;
            goto LC; // [230] 237
        }
        else{
            DeRef(_24258);
            _24258 = NOVALUE;
        }
LB: 

        /** 				ifdef UNIX then*/
LC: 

        /** 			if file_exists(user_library) then*/
        RefDS(_59user_library_42620);
        _24262 = _13file_exists(_59user_library_42620);
        if (_24262 == 0) {
            DeRef(_24262);
            _24262 = NOVALUE;
            goto LD; // [245] 253
        }
        else {
            if (!IS_ATOM_INT(_24262) && DBL_PTR(_24262)->dbl == 0.0){
                DeRef(_24262);
                _24262 = NOVALUE;
                goto LD; // [245] 253
            }
            DeRef(_24262);
            _24262 = NOVALUE;
        }
        DeRef(_24262);
        _24262 = NOVALUE;

        /** 				exit "translation kind"*/
        goto LA; // [250] 260
LD: 

        /** 		end for -- tk*/
        _tk_45482 = _tk_45482 + 1;
        goto L9; // [255] 186
LA: 
        ;
    }
L1: 
    DeRef(_eudir_45470);
    _eudir_45470 = NOVALUE;

    /** 	user_library = adjust_for_build_file(user_library)*/
    RefDS(_59user_library_42620);
    _0 = _57adjust_for_build_file(_59user_library_42620);
    DeRefDS(_59user_library_42620);
    _59user_library_42620 = _0;

    /** 	if TWINDOWS then*/
    if (_42TWINDOWS_17110 == 0)
    {
        goto LE; // [277] 334
    }
    else{
    }

    /** 		if compiler_type = COMPILER_WATCOM then*/

    /** 			c_flags &= " -DEWINDOWS"*/
    Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24267);

    /** 		if dll_option then*/
    if (_59dll_option_42608 == 0)
    {
        goto LF; // [310] 323
    }
    else{
    }

    /** 			exe_ext = ".dll"*/
    RefDS(_24269);
    DeRefi(_exe_ext_45446);
    _exe_ext_45446 = _24269;
    goto L10; // [320] 375
LF: 

    /** 			exe_ext = ".exe"*/
    RefDS(_24270);
    DeRefi(_exe_ext_45446);
    _exe_ext_45446 = _24270;
    goto L10; // [331] 375
LE: 

    /** 	elsif TOSX then*/

    /** 		if dll_option then*/
    if (_59dll_option_42608 == 0)
    {
        goto L11; // [363] 374
    }
    else{
    }

    /** 			exe_ext = ".so"*/
    RefDS(_24272);
    DeRefi(_exe_ext_45446);
    _exe_ext_45446 = _24272;
L11: 
L10: 

    /** 	object compile_dir = get_eucompiledir()*/
    _0 = _compile_dir_45528;
    _compile_dir_45528 = _59get_eucompiledir();
    DeRef(_0);

    /** 	if not file_exists(compile_dir) then*/
    Ref(_compile_dir_45528);
    _24274 = _13file_exists(_compile_dir_45528);
    if (IS_ATOM_INT(_24274)) {
        if (_24274 != 0){
            DeRef(_24274);
            _24274 = NOVALUE;
            goto L12; // [386] 407
        }
    }
    else {
        if (DBL_PTR(_24274)->dbl != 0.0){
            DeRef(_24274);
            _24274 = NOVALUE;
            goto L12; // [386] 407
        }
    }
    DeRef(_24274);
    _24274 = NOVALUE;

    /** 		printf(2,"Couldn't get include directory '%s'",{get_eucompiledir()})*/
    _24277 = _59get_eucompiledir();
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24277;
    _24278 = MAKE_SEQ(_1);
    _24277 = NOVALUE;
    EPrintf(2, _24276, _24278);
    DeRefDS(_24278);
    _24278 = NOVALUE;

    /** 		abort(1)*/
    UserCleanup(1);
L12: 

    /** 	switch compiler_type do*/
    _0 = 0;
    switch ( _0 ){ 

        /** 		case COMPILER_GCC then*/
        case 1:

        /** 			c_exe = "gcc"*/
        RefDS(_24281);
        DeRefi(_c_exe_45441);
        _c_exe_45441 = _24281;

        /** 			l_exe = "gcc"*/
        RefDS(_24281);
        DeRefi(_l_exe_45443);
        _l_exe_45443 = _24281;

        /** 			obj_ext = "o"*/
        RefDS(_24282);
        DeRefi(_obj_ext_45445);
        _obj_ext_45445 = _24282;

        /** 			if debug_option then*/
        if (_59debug_option_42618 == 0)
        {
            goto L13; // [445] 457
        }
        else{
        }

        /** 				c_flags &= " -g3"*/
        Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24283);
        goto L14; // [454] 464
L13: 

        /** 				c_flags &= " -fomit-frame-pointer"*/
        Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24285);
L14: 

        /** 			if dll_option then*/
        if (_59dll_option_42608 == 0)
        {
            goto L15; // [468] 478
        }
        else{
        }

        /** 				c_flags &= " -fPIC"*/
        Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24287);
L15: 

        /** 			c_flags &= sprintf(" -c -w -fsigned-char -O2 -m32 -I%s -ffast-math",*/
        _24290 = _59get_eucompiledir();
        _24291 = _57adjust_for_build_file(_24290);
        _24290 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24291;
        _24292 = MAKE_SEQ(_1);
        _24291 = NOVALUE;
        _24293 = EPrintf(-9999999, _24289, _24292);
        DeRefDS(_24292);
        _24292 = NOVALUE;
        Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24293);
        DeRefDS(_24293);
        _24293 = NOVALUE;

        /** 			if TWINDOWS and mno_cygwin then*/
        if (_42TWINDOWS_17110 == 0) {
            goto L16; // [503] 520
        }
        goto L16; // [510] 520

        /** 				c_flags &= " -mno-cygwin"*/
        Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24296);
L16: 

        /** 			l_flags = sprintf( "%s -m32 ", { adjust_for_build_file(user_library) })*/
        RefDS(_59user_library_42620);
        _24299 = _57adjust_for_build_file(_59user_library_42620);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24299;
        _24300 = MAKE_SEQ(_1);
        _24299 = NOVALUE;
        DeRefi(_l_flags_45444);
        _l_flags_45444 = EPrintf(-9999999, _24298, _24300);
        DeRefDS(_24300);
        _24300 = NOVALUE;

        /** 			if dll_option then*/
        if (_59dll_option_42608 == 0)
        {
            goto L17; // [540] 550
        }
        else{
        }

        /** 				l_flags &= " -shared "*/
        Concat((object_ptr)&_l_flags_45444, _l_flags_45444, _24302);
L17: 

        /** 			if TLINUX then*/

        /** 			elsif TBSD then*/

        /** 			elsif TOSX then*/

        /** 			elsif TWINDOWS then*/
        if (_42TWINDOWS_17110 == 0)
        {
            goto L18; // [602] 634
        }
        else{
        }

        /** 				if mno_cygwin then*/

        /** 				if not con_option then*/
        if (_59con_option_42610 != 0)
        goto L19; // [623] 633

        /** 					l_flags &= " -mwindows"*/
        Concat((object_ptr)&_l_flags_45444, _l_flags_45444, _24312);
L19: 
L18: 

        /** 			rc_comp = "windres -DSRCDIR=\"" & adjust_for_build_file(current_dir()) & "\" [1] -O coff -o [2]"*/
        _24315 = _13current_dir();
        _24316 = _57adjust_for_build_file(_24315);
        _24315 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _24317;
            concat_list[1] = _24316;
            concat_list[2] = _24314;
            Concat_N((object_ptr)&_rc_comp_45448, concat_list, 3);
        }
        DeRef(_24316);
        _24316 = NOVALUE;
        goto L1A; // [650] 849

        /** 		case COMPILER_WATCOM then*/
        case 2:

        /** 			c_exe = "wcc386"*/
        RefDS(_24319);
        DeRefi(_c_exe_45441);
        _c_exe_45441 = _24319;

        /** 			l_exe = "wlink"*/
        RefDS(_24320);
        DeRefi(_l_exe_45443);
        _l_exe_45443 = _24320;

        /** 			obj_ext = "obj"*/
        RefDS(_24321);
        DeRefi(_obj_ext_45445);
        _obj_ext_45445 = _24321;

        /** 			if debug_option then*/
        if (_59debug_option_42618 == 0)
        {
            goto L1B; // [681] 698
        }
        else{
        }

        /** 				c_flags = " /d3"*/
        RefDS(_24322);
        DeRef(_c_flags_45442);
        _c_flags_45442 = _24322;

        /** 				l_flags_begin &= " DEBUG ALL "*/
        Concat((object_ptr)&_l_flags_begin_45447, _l_flags_begin_45447, _24323);
L1B: 

        /** 			l_flags &= sprintf(" OPTION STACK=%d ", { total_stack_size })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _59total_stack_size_42631;
        _24326 = MAKE_SEQ(_1);
        _24327 = EPrintf(-9999999, _24325, _24326);
        DeRefDS(_24326);
        _24326 = NOVALUE;
        Concat((object_ptr)&_l_flags_45444, _l_flags_45444, _24327);
        DeRefDS(_24327);
        _24327 = NOVALUE;

        /** 			l_flags &= sprintf(" COMMIT STACK=%d ", { total_stack_size })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _59total_stack_size_42631;
        _24330 = MAKE_SEQ(_1);
        _24331 = EPrintf(-9999999, _24329, _24330);
        DeRefDS(_24330);
        _24330 = NOVALUE;
        Concat((object_ptr)&_l_flags_45444, _l_flags_45444, _24331);
        DeRefDS(_24331);
        _24331 = NOVALUE;

        /** 			l_flags &= " OPTION QUIET OPTION ELIMINATE OPTION CASEEXACT"*/
        Concat((object_ptr)&_l_flags_45444, _l_flags_45444, _24333);

        /** 			if dll_option then*/
        if (_59dll_option_42608 == 0)
        {
            goto L1C; // [740] 766
        }
        else{
        }

        /** 				c_flags &= " /bd /bt=nt /mf /w0 /zq /j /zp4 /fp5 /fpi87 /5r /otimra /s /I" & adjust_for_build_file(compile_dir) */
        Ref(_compile_dir_45528);
        _24336 = _57adjust_for_build_file(_compile_dir_45528);
        if (IS_SEQUENCE(_24335) && IS_ATOM(_24336)) {
            Ref(_24336);
            Append(&_24337, _24335, _24336);
        }
        else if (IS_ATOM(_24335) && IS_SEQUENCE(_24336)) {
        }
        else {
            Concat((object_ptr)&_24337, _24335, _24336);
        }
        DeRef(_24336);
        _24336 = NOVALUE;
        Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24337);
        DeRefDS(_24337);
        _24337 = NOVALUE;

        /** 				l_flags &= " SYSTEM NT_DLL initinstance terminstance"*/
        Concat((object_ptr)&_l_flags_45444, _l_flags_45444, _24339);
        goto L1D; // [763] 804
L1C: 

        /** 				c_flags &= " /bt=nt /mf /w0 /zq /j /zp4 /fp5 /fpi87 /5r /otimra /s /I" & adjust_for_build_file(compile_dir)*/
        Ref(_compile_dir_45528);
        _24342 = _57adjust_for_build_file(_compile_dir_45528);
        if (IS_SEQUENCE(_24341) && IS_ATOM(_24342)) {
            Ref(_24342);
            Append(&_24343, _24341, _24342);
        }
        else if (IS_ATOM(_24341) && IS_SEQUENCE(_24342)) {
        }
        else {
            Concat((object_ptr)&_24343, _24341, _24342);
        }
        DeRef(_24342);
        _24342 = NOVALUE;
        Concat((object_ptr)&_c_flags_45442, _c_flags_45442, _24343);
        DeRefDS(_24343);
        _24343 = NOVALUE;

        /** 				if con_option then*/
        if (_59con_option_42610 == 0)
        {
            goto L1E; // [784] 796
        }
        else{
        }

        /** 					l_flags = " SYSTEM NT" & l_flags*/
        Concat((object_ptr)&_l_flags_45444, _24345, _l_flags_45444);
        goto L1F; // [793] 803
L1E: 

        /** 					l_flags = " SYSTEM NT_WIN RUNTIME WINDOWS=4.0" & l_flags*/
        Concat((object_ptr)&_l_flags_45444, _24347, _l_flags_45444);
L1F: 
L1D: 

        /** 			l_flags &= sprintf(" FILE %s", { (user_library) })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_59user_library_42620);
        *((int *)(_2+4)) = _59user_library_42620;
        _24350 = MAKE_SEQ(_1);
        _24351 = EPrintf(-9999999, _24349, _24350);
        DeRefDS(_24350);
        _24350 = NOVALUE;
        Concat((object_ptr)&_l_flags_45444, _l_flags_45444, _24351);
        DeRefDS(_24351);
        _24351 = NOVALUE;

        /** 			rc_comp = "wrc -DSRCDIR=\"" & adjust_for_build_file(current_dir()) & "\" -q -fo=[2] -ad [1] [3]"*/
        _24354 = _13current_dir();
        _24355 = _57adjust_for_build_file(_24354);
        _24354 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _24356;
            concat_list[1] = _24355;
            concat_list[2] = _24353;
            Concat_N((object_ptr)&_rc_comp_45448, concat_list, 3);
        }
        DeRef(_24355);
        _24355 = NOVALUE;
        goto L1A; // [835] 849

        /** 		case else*/
        default:

        /** 			CompileErr(43)*/
        RefDS(_22663);
        _46CompileErr(43, _22663, 0);
    ;}L1A: 

    /** 	if length(cflags) then*/
    _24358 = 0;

    /** 	if length(lflags) then*/
    _24359 = 0;

    /** 	return { */
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_c_exe_45441);
    *((int *)(_2+4)) = _c_exe_45441;
    RefDS(_c_flags_45442);
    *((int *)(_2+8)) = _c_flags_45442;
    RefDS(_l_exe_45443);
    *((int *)(_2+12)) = _l_exe_45443;
    RefDS(_l_flags_45444);
    *((int *)(_2+16)) = _l_flags_45444;
    RefDS(_obj_ext_45445);
    *((int *)(_2+20)) = _obj_ext_45445;
    RefDS(_exe_ext_45446);
    *((int *)(_2+24)) = _exe_ext_45446;
    RefDS(_l_flags_begin_45447);
    *((int *)(_2+28)) = _l_flags_begin_45447;
    RefDS(_rc_comp_45448);
    *((int *)(_2+32)) = _rc_comp_45448;
    _24360 = MAKE_SEQ(_1);
    DeRefDSi(_c_exe_45441);
    DeRefDS(_c_flags_45442);
    DeRefDSi(_l_exe_45443);
    DeRefDSi(_l_flags_45444);
    DeRefDSi(_obj_ext_45445);
    DeRefDSi(_exe_ext_45446);
    DeRefDSi(_l_flags_begin_45447);
    DeRefDS(_rc_comp_45448);
    DeRef(_l_names_45449);
    DeRefi(_l_ext_45450);
    DeRefi(_t_slash_45451);
    DeRef(_compile_dir_45528);
    return _24360;
    ;
}


void _57ensure_exename(int _ext_45664)
{
    int _24367 = NOVALUE;
    int _24366 = NOVALUE;
    int _24365 = NOVALUE;
    int _24364 = NOVALUE;
    int _24362 = NOVALUE;
    int _24361 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(exe_name[D_ALTNAME]) = 0 then*/
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24361 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24361)){
            _24362 = SEQ_PTR(_24361)->length;
    }
    else {
        _24362 = 1;
    }
    _24361 = NOVALUE;
    if (_24362 != 0)
    goto L1; // [16] 67

    /** 		exe_name[D_NAME] = current_dir() & SLASH & file0 & ext*/
    _24364 = _13current_dir();
    {
        int concat_list[4];

        concat_list[0] = _ext_45664;
        concat_list[1] = _59file0_44445;
        concat_list[2] = 92;
        concat_list[3] = _24364;
        Concat_N((object_ptr)&_24365, concat_list, 4);
    }
    DeRef(_24364);
    _24364 = NOVALUE;
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _24365;
    if( _1 != _24365 ){
        DeRef(_1);
    }
    _24365 = NOVALUE;

    /** 		exe_name[D_ALTNAME] = adjust_for_command_line_passing(exe_name[D_NAME])*/
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24366 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24366);
    _24367 = _57adjust_for_command_line_passing(_24366);
    _24366 = NOVALUE;
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _24367;
    if( _1 != _24367 ){
        DeRef(_1);
    }
    _24367 = NOVALUE;
L1: 

    /** end procedure*/
    DeRefDS(_ext_45664);
    _24361 = NOVALUE;
    return;
    ;
}


void _57write_objlink_file()
{
    int _settings_45682 = NOVALUE;
    int _fh_45684 = NOVALUE;
    int _s_45732 = NOVALUE;
    int _24416 = NOVALUE;
    int _24414 = NOVALUE;
    int _24413 = NOVALUE;
    int _24412 = NOVALUE;
    int _24411 = NOVALUE;
    int _24410 = NOVALUE;
    int _24409 = NOVALUE;
    int _24408 = NOVALUE;
    int _24407 = NOVALUE;
    int _24406 = NOVALUE;
    int _24405 = NOVALUE;
    int _24404 = NOVALUE;
    int _24403 = NOVALUE;
    int _24401 = NOVALUE;
    int _24400 = NOVALUE;
    int _24399 = NOVALUE;
    int _24398 = NOVALUE;
    int _24396 = NOVALUE;
    int _24395 = NOVALUE;
    int _24394 = NOVALUE;
    int _24393 = NOVALUE;
    int _24392 = NOVALUE;
    int _24391 = NOVALUE;
    int _24385 = NOVALUE;
    int _24384 = NOVALUE;
    int _24381 = NOVALUE;
    int _24380 = NOVALUE;
    int _24379 = NOVALUE;
    int _24378 = NOVALUE;
    int _24377 = NOVALUE;
    int _24375 = NOVALUE;
    int _24374 = NOVALUE;
    int _24373 = NOVALUE;
    int _24370 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence settings = setup_build()*/
    _0 = _settings_45682;
    _settings_45682 = _57setup_build();
    DeRef(_0);

    /** 	integer fh = open(output_dir & file0 & ".lnk", "wb")*/
    {
        int concat_list[3];

        concat_list[0] = _24369;
        concat_list[1] = _59file0_44445;
        concat_list[2] = _59output_dir_42630;
        Concat_N((object_ptr)&_24370, concat_list, 3);
    }
    _fh_45684 = EOpen(_24370, _24371, 0);
    DeRefDS(_24370);
    _24370 = NOVALUE;

    /** 	ensure_exename(settings[SETUP_EXE_EXT])*/
    _2 = (int)SEQ_PTR(_settings_45682);
    _24373 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24373);
    _57ensure_exename(_24373);
    _24373 = NOVALUE;

    /** 	if length(settings[SETUP_LFLAGS_BEGIN]) > 0 then*/
    _2 = (int)SEQ_PTR(_settings_45682);
    _24374 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_24374)){
            _24375 = SEQ_PTR(_24374)->length;
    }
    else {
        _24375 = 1;
    }
    _24374 = NOVALUE;
    if (_24375 <= 0)
    goto L1; // [47] 69

    /** 		puts(fh, settings[SETUP_LFLAGS_BEGIN] & HOSTNL)*/
    _2 = (int)SEQ_PTR(_settings_45682);
    _24377 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_24377) && IS_ATOM(_42HOSTNL_17125)) {
    }
    else if (IS_ATOM(_24377) && IS_SEQUENCE(_42HOSTNL_17125)) {
        Ref(_24377);
        Prepend(&_24378, _42HOSTNL_17125, _24377);
    }
    else {
        Concat((object_ptr)&_24378, _24377, _42HOSTNL_17125);
        _24377 = NOVALUE;
    }
    _24377 = NOVALUE;
    EPuts(_fh_45684, _24378); // DJP 
    DeRefDS(_24378);
    _24378 = NOVALUE;
L1: 

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_59generated_files_42612)){
            _24379 = SEQ_PTR(_59generated_files_42612)->length;
    }
    else {
        _24379 = 1;
    }
    {
        int _i_45700;
        _i_45700 = 1;
L2: 
        if (_i_45700 > _24379){
            goto L3; // [76] 140
        }

        /** 		if match(".o", generated_files[i]) then*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24380 = (int)*(((s1_ptr)_2)->base + _i_45700);
        _24381 = e_match_from(_23709, _24380, 1);
        _24380 = NOVALUE;
        if (_24381 == 0)
        {
            _24381 = NOVALUE;
            goto L4; // [96] 133
        }
        else{
            _24381 = NOVALUE;
        }

        /** 			if compiler_type = COMPILER_WATCOM then*/

        /** 			puts(fh, generated_files[i] & HOSTNL)*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24384 = (int)*(((s1_ptr)_2)->base + _i_45700);
        Concat((object_ptr)&_24385, _24384, _42HOSTNL_17125);
        _24384 = NOVALUE;
        _24384 = NOVALUE;
        EPuts(_fh_45684, _24385); // DJP 
        DeRefDS(_24385);
        _24385 = NOVALUE;
L4: 

        /** 	end for*/
        _i_45700 = _i_45700 + 1;
        goto L2; // [135] 83
L3: 
        ;
    }

    /** 	if compiler_type = COMPILER_WATCOM then*/

    /** 	puts(fh, trim(settings[SETUP_LFLAGS] & HOSTNL))*/
    _2 = (int)SEQ_PTR(_settings_45682);
    _24391 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_SEQUENCE(_24391) && IS_ATOM(_42HOSTNL_17125)) {
    }
    else if (IS_ATOM(_24391) && IS_SEQUENCE(_42HOSTNL_17125)) {
        Ref(_24391);
        Prepend(&_24392, _42HOSTNL_17125, _24391);
    }
    else {
        Concat((object_ptr)&_24392, _24391, _42HOSTNL_17125);
        _24391 = NOVALUE;
    }
    _24391 = NOVALUE;
    RefDS(_4494);
    _24393 = _10trim(_24392, _4494, 0);
    _24392 = NOVALUE;
    EPuts(_fh_45684, _24393); // DJP 
    DeRef(_24393);
    _24393 = NOVALUE;

    /** 	if compiler_type = COMPILER_WATCOM and dll_option then*/
    _24394 = (0 == 2);
    if (_24394 == 0) {
        goto L5; // [208] 375
    }
    if (_59dll_option_42608 == 0)
    {
        goto L5; // [215] 375
    }
    else{
    }

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45684, _42HOSTNL_17125); // DJP 

    /** 		object s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _24396 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
    DeRef(_s_45732);
    _2 = (int)SEQ_PTR(_24396);
    _s_45732 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_s_45732);
    _24396 = NOVALUE;

    /** 		while s do*/
L6: 
    if (_s_45732 <= 0) {
        if (_s_45732 == 0) {
            goto L7; // [246] 374
        }
        else {
            if (!IS_ATOM_INT(_s_45732) && DBL_PTR(_s_45732)->dbl == 0.0){
                goto L7; // [246] 374
            }
        }
    }

    /** 			if eu:find(SymTab[s][S_TOKEN], RTN_TOKS) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_s_45732)){
        _24398 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45732)->dbl));
    }
    else{
        _24398 = (int)*(((s1_ptr)_2)->base + _s_45732);
    }
    _2 = (int)SEQ_PTR(_24398);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _24399 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _24399 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _24398 = NOVALUE;
    _24400 = find_from(_24399, _39RTN_TOKS_16547, 1);
    _24399 = NOVALUE;
    if (_24400 == 0)
    {
        _24400 = NOVALUE;
        goto L8; // [270] 355
    }
    else{
        _24400 = NOVALUE;
    }

    /** 				if is_exported( s ) then*/
    Ref(_s_45732);
    _24401 = _59is_exported(_s_45732);
    if (_24401 == 0) {
        DeRef(_24401);
        _24401 = NOVALUE;
        goto L9; // [279] 354
    }
    else {
        if (!IS_ATOM_INT(_24401) && DBL_PTR(_24401)->dbl == 0.0){
            DeRef(_24401);
            _24401 = NOVALUE;
            goto L9; // [279] 354
        }
        DeRef(_24401);
        _24401 = NOVALUE;
    }
    DeRef(_24401);
    _24401 = NOVALUE;

    /** 					printf(fh, "EXPORT %s='__%d%s@%d'" & HOSTNL,*/
    Concat((object_ptr)&_24403, _24402, _42HOSTNL_17125);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_s_45732)){
        _24404 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45732)->dbl));
    }
    else{
        _24404 = (int)*(((s1_ptr)_2)->base + _s_45732);
    }
    _2 = (int)SEQ_PTR(_24404);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _24405 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _24405 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _24404 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_s_45732)){
        _24406 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45732)->dbl));
    }
    else{
        _24406 = (int)*(((s1_ptr)_2)->base + _s_45732);
    }
    _2 = (int)SEQ_PTR(_24406);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _24407 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _24407 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _24406 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_s_45732)){
        _24408 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45732)->dbl));
    }
    else{
        _24408 = (int)*(((s1_ptr)_2)->base + _s_45732);
    }
    _2 = (int)SEQ_PTR(_24408);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _24409 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _24409 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _24408 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_s_45732)){
        _24410 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45732)->dbl));
    }
    else{
        _24410 = (int)*(((s1_ptr)_2)->base + _s_45732);
    }
    _2 = (int)SEQ_PTR(_24410);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _24411 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _24411 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _24410 = NOVALUE;
    if (IS_ATOM_INT(_24411)) {
        if (_24411 == (short)_24411)
        _24412 = _24411 * 4;
        else
        _24412 = NewDouble(_24411 * (double)4);
    }
    else {
        _24412 = binary_op(MULTIPLY, _24411, 4);
    }
    _24411 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24405);
    *((int *)(_2+4)) = _24405;
    Ref(_24407);
    *((int *)(_2+8)) = _24407;
    Ref(_24409);
    *((int *)(_2+12)) = _24409;
    *((int *)(_2+16)) = _24412;
    _24413 = MAKE_SEQ(_1);
    _24412 = NOVALUE;
    _24409 = NOVALUE;
    _24407 = NOVALUE;
    _24405 = NOVALUE;
    EPrintf(_fh_45684, _24403, _24413);
    DeRefDS(_24403);
    _24403 = NOVALUE;
    DeRefDS(_24413);
    _24413 = NOVALUE;
L9: 
L8: 

    /** 			s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_s_45732)){
        _24414 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_s_45732)->dbl));
    }
    else{
        _24414 = (int)*(((s1_ptr)_2)->base + _s_45732);
    }
    DeRef(_s_45732);
    _2 = (int)SEQ_PTR(_24414);
    _s_45732 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_s_45732);
    _24414 = NOVALUE;

    /** 		end while*/
    goto L6; // [371] 246
L7: 
L5: 
    DeRef(_s_45732);
    _s_45732 = NOVALUE;

    /** 	close(fh)*/
    EClose(_fh_45684);

    /** 	generated_files = append(generated_files, file0 & ".lnk")*/
    Concat((object_ptr)&_24416, _59file0_44445, _24369);
    RefDS(_24416);
    Append(&_59generated_files_42612, _59generated_files_42612, _24416);
    DeRefDS(_24416);
    _24416 = NOVALUE;

    /** end procedure*/
    DeRef(_settings_45682);
    _24374 = NOVALUE;
    DeRef(_24394);
    _24394 = NOVALUE;
    return;
    ;
}


void _57write_makefile_srcobj_list(int _fh_45781)
{
    int _24441 = NOVALUE;
    int _24440 = NOVALUE;
    int _24439 = NOVALUE;
    int _24438 = NOVALUE;
    int _24437 = NOVALUE;
    int _24435 = NOVALUE;
    int _24434 = NOVALUE;
    int _24433 = NOVALUE;
    int _24432 = NOVALUE;
    int _24431 = NOVALUE;
    int _24430 = NOVALUE;
    int _24429 = NOVALUE;
    int _24427 = NOVALUE;
    int _24426 = NOVALUE;
    int _24424 = NOVALUE;
    int _24423 = NOVALUE;
    int _24422 = NOVALUE;
    int _24421 = NOVALUE;
    int _24420 = NOVALUE;
    int _24419 = NOVALUE;
    int _0, _1, _2;
    

    /** 	printf(fh, "%s_SOURCES =", { upper(file0) })*/
    RefDS(_59file0_44445);
    _24419 = _10upper(_59file0_44445);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24419;
    _24420 = MAKE_SEQ(_1);
    _24419 = NOVALUE;
    EPrintf(_fh_45781, _24418, _24420);
    DeRefDS(_24420);
    _24420 = NOVALUE;

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_59generated_files_42612)){
            _24421 = SEQ_PTR(_59generated_files_42612)->length;
    }
    else {
        _24421 = 1;
    }
    {
        int _i_45788;
        _i_45788 = 1;
L1: 
        if (_i_45788 > _24421){
            goto L2; // [26] 75
        }

        /** 		if generated_files[i][$] = 'c' then*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24422 = (int)*(((s1_ptr)_2)->base + _i_45788);
        if (IS_SEQUENCE(_24422)){
                _24423 = SEQ_PTR(_24422)->length;
        }
        else {
            _24423 = 1;
        }
        _2 = (int)SEQ_PTR(_24422);
        _24424 = (int)*(((s1_ptr)_2)->base + _24423);
        _24422 = NOVALUE;
        if (binary_op_a(NOTEQ, _24424, 99)){
            _24424 = NOVALUE;
            goto L3; // [48] 68
        }
        _24424 = NOVALUE;

        /** 			puts(fh, " " & generated_files[i])*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24426 = (int)*(((s1_ptr)_2)->base + _i_45788);
        Concat((object_ptr)&_24427, _23949, _24426);
        _24426 = NOVALUE;
        EPuts(_fh_45781, _24427); // DJP 
        DeRefDS(_24427);
        _24427 = NOVALUE;
L3: 

        /** 	end for*/
        _i_45788 = _i_45788 + 1;
        goto L1; // [70] 33
L2: 
        ;
    }

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45781, _42HOSTNL_17125); // DJP 

    /** 	printf(fh, "%s_OBJECTS =", { upper(file0) })*/
    RefDS(_59file0_44445);
    _24429 = _10upper(_59file0_44445);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24429;
    _24430 = MAKE_SEQ(_1);
    _24429 = NOVALUE;
    EPrintf(_fh_45781, _24428, _24430);
    DeRefDS(_24430);
    _24430 = NOVALUE;

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_59generated_files_42612)){
            _24431 = SEQ_PTR(_59generated_files_42612)->length;
    }
    else {
        _24431 = 1;
    }
    {
        int _i_45807;
        _i_45807 = 1;
L4: 
        if (_i_45807 > _24431){
            goto L5; // [105] 151
        }

        /** 		if match(".o", generated_files[i]) then*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24432 = (int)*(((s1_ptr)_2)->base + _i_45807);
        _24433 = e_match_from(_23709, _24432, 1);
        _24432 = NOVALUE;
        if (_24433 == 0)
        {
            _24433 = NOVALUE;
            goto L6; // [125] 144
        }
        else{
            _24433 = NOVALUE;
        }

        /** 			puts(fh, " " & generated_files[i])*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24434 = (int)*(((s1_ptr)_2)->base + _i_45807);
        Concat((object_ptr)&_24435, _23949, _24434);
        _24434 = NOVALUE;
        EPuts(_fh_45781, _24435); // DJP 
        DeRefDS(_24435);
        _24435 = NOVALUE;
L6: 

        /** 	end for*/
        _i_45807 = _i_45807 + 1;
        goto L4; // [146] 112
L5: 
        ;
    }

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45781, _42HOSTNL_17125); // DJP 

    /** 	printf(fh, "%s_GENERATED_FILES = ", { upper(file0) })*/
    RefDS(_59file0_44445);
    _24437 = _10upper(_59file0_44445);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24437;
    _24438 = MAKE_SEQ(_1);
    _24437 = NOVALUE;
    EPrintf(_fh_45781, _24436, _24438);
    DeRefDS(_24438);
    _24438 = NOVALUE;

    /** 	for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_59generated_files_42612)){
            _24439 = SEQ_PTR(_59generated_files_42612)->length;
    }
    else {
        _24439 = 1;
    }
    {
        int _i_45824;
        _i_45824 = 1;
L7: 
        if (_i_45824 > _24439){
            goto L8; // [181] 210
        }

        /** 		puts(fh, " " & generated_files[i])*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24440 = (int)*(((s1_ptr)_2)->base + _i_45824);
        Concat((object_ptr)&_24441, _23949, _24440);
        _24440 = NOVALUE;
        EPuts(_fh_45781, _24441); // DJP 
        DeRefDS(_24441);
        _24441 = NOVALUE;

        /** 	end for*/
        _i_45824 = _i_45824 + 1;
        goto L7; // [205] 188
L8: 
        ;
    }

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45781, _42HOSTNL_17125); // DJP 

    /** end procedure*/
    return;
    ;
}


int _57windows_to_mingw_path(int _s_45833)
{
    int _24443 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef TEST_FOR_WIN9X_ON_MING then*/

    /** 	return search:find_replace('\\',s,'/')*/
    RefDS(_s_45833);
    _24443 = _12find_replace(92, _s_45833, 47, 0);
    DeRefDS(_s_45833);
    return _24443;
    ;
}


void _57write_makefile_full()
{
    int _settings_45838 = NOVALUE;
    int _fh_45841 = NOVALUE;
    int _24567 = NOVALUE;
    int _24565 = NOVALUE;
    int _24563 = NOVALUE;
    int _24562 = NOVALUE;
    int _24561 = NOVALUE;
    int _24560 = NOVALUE;
    int _24559 = NOVALUE;
    int _24557 = NOVALUE;
    int _24556 = NOVALUE;
    int _24554 = NOVALUE;
    int _24553 = NOVALUE;
    int _24552 = NOVALUE;
    int _24551 = NOVALUE;
    int _24549 = NOVALUE;
    int _24548 = NOVALUE;
    int _24546 = NOVALUE;
    int _24545 = NOVALUE;
    int _24543 = NOVALUE;
    int _24542 = NOVALUE;
    int _24541 = NOVALUE;
    int _24540 = NOVALUE;
    int _24539 = NOVALUE;
    int _24538 = NOVALUE;
    int _24537 = NOVALUE;
    int _24536 = NOVALUE;
    int _24534 = NOVALUE;
    int _24533 = NOVALUE;
    int _24532 = NOVALUE;
    int _24531 = NOVALUE;
    int _24530 = NOVALUE;
    int _24529 = NOVALUE;
    int _24528 = NOVALUE;
    int _24527 = NOVALUE;
    int _24526 = NOVALUE;
    int _24525 = NOVALUE;
    int _24524 = NOVALUE;
    int _24523 = NOVALUE;
    int _24522 = NOVALUE;
    int _24460 = NOVALUE;
    int _24459 = NOVALUE;
    int _24458 = NOVALUE;
    int _24456 = NOVALUE;
    int _24455 = NOVALUE;
    int _24454 = NOVALUE;
    int _24452 = NOVALUE;
    int _24451 = NOVALUE;
    int _24450 = NOVALUE;
    int _24447 = NOVALUE;
    int _24445 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence settings = setup_build()*/
    _0 = _settings_45838;
    _settings_45838 = _57setup_build();
    DeRef(_0);

    /** 	ensure_exename(settings[SETUP_EXE_EXT])*/
    _2 = (int)SEQ_PTR(_settings_45838);
    _24445 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24445);
    _57ensure_exename(_24445);
    _24445 = NOVALUE;

    /** 	integer fh = open(output_dir & file0 & ".mak", "wb")*/
    {
        int concat_list[3];

        concat_list[0] = _24446;
        concat_list[1] = _59file0_44445;
        concat_list[2] = _59output_dir_42630;
        Concat_N((object_ptr)&_24447, concat_list, 3);
    }
    _fh_45841 = EOpen(_24447, _24371, 0);
    DeRefDS(_24447);
    _24447 = NOVALUE;

    /** 	printf(fh, "CC     = %s" & HOSTNL, { settings[SETUP_CEXE] })*/
    Concat((object_ptr)&_24450, _24449, _42HOSTNL_17125);
    _2 = (int)SEQ_PTR(_settings_45838);
    _24451 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24451);
    *((int *)(_2+4)) = _24451;
    _24452 = MAKE_SEQ(_1);
    _24451 = NOVALUE;
    EPrintf(_fh_45841, _24450, _24452);
    DeRefDS(_24450);
    _24450 = NOVALUE;
    DeRefDS(_24452);
    _24452 = NOVALUE;

    /** 	printf(fh, "CFLAGS = %s" & HOSTNL, { settings[SETUP_CFLAGS] })*/
    Concat((object_ptr)&_24454, _24453, _42HOSTNL_17125);
    _2 = (int)SEQ_PTR(_settings_45838);
    _24455 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24455);
    *((int *)(_2+4)) = _24455;
    _24456 = MAKE_SEQ(_1);
    _24455 = NOVALUE;
    EPrintf(_fh_45841, _24454, _24456);
    DeRefDS(_24454);
    _24454 = NOVALUE;
    DeRefDS(_24456);
    _24456 = NOVALUE;

    /** 	printf(fh, "LINKER = %s" & HOSTNL, { settings[SETUP_LEXE] })*/
    Concat((object_ptr)&_24458, _24457, _42HOSTNL_17125);
    _2 = (int)SEQ_PTR(_settings_45838);
    _24459 = (int)*(((s1_ptr)_2)->base + 3);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24459);
    *((int *)(_2+4)) = _24459;
    _24460 = MAKE_SEQ(_1);
    _24459 = NOVALUE;
    EPrintf(_fh_45841, _24458, _24460);
    DeRefDS(_24458);
    _24458 = NOVALUE;
    DeRefDS(_24460);
    _24460 = NOVALUE;

    /** 	if compiler_type = COMPILER_GCC then*/

    /** 		write_objlink_file()*/
    _57write_objlink_file();

    /** 	write_makefile_srcobj_list(fh)*/
    _57write_makefile_srcobj_list(_fh_45841);

    /** 	puts(fh, HOSTNL)*/
    EPuts(_fh_45841, _42HOSTNL_17125); // DJP 

    /** 	if compiler_type = COMPILER_WATCOM then*/

    /** 		printf(fh, "%s: $(%s_OBJECTS) %s %s" & HOSTNL, { adjust_for_build_file(exe_name[D_ALTNAME]), upper(file0), user_library, rc_file[D_ALTNAME] })*/
    Concat((object_ptr)&_24522, _24521, _42HOSTNL_17125);
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24523 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_24523);
    _24524 = _57adjust_for_build_file(_24523);
    _24523 = NOVALUE;
    RefDS(_59file0_44445);
    _24525 = _10upper(_59file0_44445);
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24526 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24524;
    *((int *)(_2+8)) = _24525;
    RefDS(_59user_library_42620);
    *((int *)(_2+12)) = _59user_library_42620;
    RefDS(_24526);
    *((int *)(_2+16)) = _24526;
    _24527 = MAKE_SEQ(_1);
    _24526 = NOVALUE;
    _24525 = NOVALUE;
    _24524 = NOVALUE;
    EPrintf(_fh_45841, _24522, _24527);
    DeRefDS(_24522);
    _24522 = NOVALUE;
    DeRefDS(_24527);
    _24527 = NOVALUE;

    /** 		if length(rc_file[D_ALTNAME]) then*/
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24528 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24528)){
            _24529 = SEQ_PTR(_24528)->length;
    }
    else {
        _24529 = 1;
    }
    _24528 = NOVALUE;
    if (_24529 == 0)
    {
        _24529 = NOVALUE;
        goto L1; // [653] 699
    }
    else{
        _24529 = NOVALUE;
    }

    /** 			writef(fh, "\t" & settings[SETUP_RC_COMPILER] & HOSTNL, { rc_file[D_ALTNAME], res_file[D_ALTNAME] })*/
    _2 = (int)SEQ_PTR(_settings_45838);
    _24530 = (int)*(((s1_ptr)_2)->base + 8);
    {
        int concat_list[3];

        concat_list[0] = _42HOSTNL_17125;
        concat_list[1] = _24530;
        concat_list[2] = _24480;
        Concat_N((object_ptr)&_24531, concat_list, 3);
    }
    _24530 = NOVALUE;
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24532 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24533 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_24533);
    RefDS(_24532);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24532;
    ((int *)_2)[2] = _24533;
    _24534 = MAKE_SEQ(_1);
    _24533 = NOVALUE;
    _24532 = NOVALUE;
    _16writef(_fh_45841, _24531, _24534, 0);
    _24531 = NOVALUE;
    _24534 = NOVALUE;
L1: 

    /** 		printf(fh, "\t$(LINKER) -o %s $(%s_OBJECTS) %s $(LFLAGS)" & HOSTNL, {*/
    Concat((object_ptr)&_24536, _24535, _42HOSTNL_17125);
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24537 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_59file0_44445);
    _24538 = _10upper(_59file0_44445);
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24539 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24539)){
            _24540 = SEQ_PTR(_24539)->length;
    }
    else {
        _24540 = 1;
    }
    _24539 = NOVALUE;
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24541 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_24541);
    RefDS(_22663);
    _24542 = _58iif(_24540, _24541, _22663);
    _24540 = NOVALUE;
    _24541 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24537);
    *((int *)(_2+4)) = _24537;
    *((int *)(_2+8)) = _24538;
    *((int *)(_2+12)) = _24542;
    _24543 = MAKE_SEQ(_1);
    _24542 = NOVALUE;
    _24538 = NOVALUE;
    _24537 = NOVALUE;
    EPrintf(_fh_45841, _24536, _24543);
    DeRefDS(_24536);
    _24536 = NOVALUE;
    DeRefDS(_24543);
    _24543 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45841, _42HOSTNL_17125); // DJP 

    /** 		printf(fh, ".PHONY: %s-clean %s-clean-all" & HOSTNL, { file0, file0 })*/
    Concat((object_ptr)&_24545, _24544, _42HOSTNL_17125);
    RefDS(_59file0_44445);
    RefDS(_59file0_44445);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _59file0_44445;
    ((int *)_2)[2] = _59file0_44445;
    _24546 = MAKE_SEQ(_1);
    EPrintf(_fh_45841, _24545, _24546);
    DeRefDS(_24545);
    _24545 = NOVALUE;
    DeRefDS(_24546);
    _24546 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45841, _42HOSTNL_17125); // DJP 

    /** 		printf(fh, "%s-clean:" & HOSTNL, { file0 })*/
    Concat((object_ptr)&_24548, _24547, _42HOSTNL_17125);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_59file0_44445);
    *((int *)(_2+4)) = _59file0_44445;
    _24549 = MAKE_SEQ(_1);
    EPrintf(_fh_45841, _24548, _24549);
    DeRefDS(_24548);
    _24548 = NOVALUE;
    DeRefDS(_24549);
    _24549 = NOVALUE;

    /** 		printf(fh, "\trm -rf $(%s_OBJECTS) %s" & HOSTNL, { upper(file0), res_file[D_ALTNAME] })*/
    Concat((object_ptr)&_24551, _24550, _42HOSTNL_17125);
    RefDS(_59file0_44445);
    _24552 = _10upper(_59file0_44445);
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24553 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_24553);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24552;
    ((int *)_2)[2] = _24553;
    _24554 = MAKE_SEQ(_1);
    _24553 = NOVALUE;
    _24552 = NOVALUE;
    EPrintf(_fh_45841, _24551, _24554);
    DeRefDS(_24551);
    _24551 = NOVALUE;
    DeRefDS(_24554);
    _24554 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45841, _42HOSTNL_17125); // DJP 

    /** 		printf(fh, "%s-clean-all: %s-clean" & HOSTNL, { file0, file0 })*/
    Concat((object_ptr)&_24556, _24555, _42HOSTNL_17125);
    RefDS(_59file0_44445);
    RefDS(_59file0_44445);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _59file0_44445;
    ((int *)_2)[2] = _59file0_44445;
    _24557 = MAKE_SEQ(_1);
    EPrintf(_fh_45841, _24556, _24557);
    DeRefDS(_24556);
    _24556 = NOVALUE;
    DeRefDS(_24557);
    _24557 = NOVALUE;

    /** 		printf(fh, "\trm -rf $(%s_SOURCES) %s %s" & HOSTNL, { upper(file0), res_file[D_ALTNAME], exe_name[D_ALTNAME] })*/
    Concat((object_ptr)&_24559, _24558, _42HOSTNL_17125);
    RefDS(_59file0_44445);
    _24560 = _10upper(_59file0_44445);
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24561 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24562 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24560;
    RefDS(_24561);
    *((int *)(_2+8)) = _24561;
    Ref(_24562);
    *((int *)(_2+12)) = _24562;
    _24563 = MAKE_SEQ(_1);
    _24562 = NOVALUE;
    _24561 = NOVALUE;
    _24560 = NOVALUE;
    EPrintf(_fh_45841, _24559, _24563);
    DeRefDS(_24559);
    _24559 = NOVALUE;
    DeRefDS(_24563);
    _24563 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45841, _42HOSTNL_17125); // DJP 

    /** 		puts(fh, "%.o: %.c" & HOSTNL)*/
    Concat((object_ptr)&_24565, _24564, _42HOSTNL_17125);
    EPuts(_fh_45841, _24565); // DJP 
    DeRefDS(_24565);
    _24565 = NOVALUE;

    /** 		puts(fh, "\t$(CC) $(CFLAGS) $*.c -o $*.o" & HOSTNL)*/
    Concat((object_ptr)&_24567, _24566, _42HOSTNL_17125);
    EPuts(_fh_45841, _24567); // DJP 
    DeRefDS(_24567);
    _24567 = NOVALUE;

    /** 		puts(fh, HOSTNL)*/
    EPuts(_fh_45841, _42HOSTNL_17125); // DJP 

    /** 	close(fh)*/
    EClose(_fh_45841);

    /** end procedure*/
    DeRef(_settings_45838);
    _24528 = NOVALUE;
    _24539 = NOVALUE;
    return;
    ;
}


void _57write_makefile_partial()
{
    int _settings_46065 = NOVALUE;
    int _fh_46067 = NOVALUE;
    int _24569 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence settings = setup_build()*/
    _0 = _settings_46065;
    _settings_46065 = _57setup_build();
    DeRef(_0);

    /** 	integer fh = open(output_dir & file0 & ".mak", "wb")*/
    {
        int concat_list[3];

        concat_list[0] = _24446;
        concat_list[1] = _59file0_44445;
        concat_list[2] = _59output_dir_42630;
        Concat_N((object_ptr)&_24569, concat_list, 3);
    }
    _fh_46067 = EOpen(_24569, _24371, 0);
    DeRefDS(_24569);
    _24569 = NOVALUE;

    /** 	write_makefile_srcobj_list(fh)*/
    _57write_makefile_srcobj_list(_fh_46067);

    /** 	close(fh)*/
    EClose(_fh_46067);

    /** end procedure*/
    DeRefDS(_settings_46065);
    return;
    ;
}


void _57build_direct(int _link_only_46074, int _the_file0_46075)
{
    int _cmd_46081 = NOVALUE;
    int _objs_46082 = NOVALUE;
    int _settings_46083 = NOVALUE;
    int _cwd_46085 = NOVALUE;
    int _status_46088 = NOVALUE;
    int _link_files_46117 = NOVALUE;
    int _pdone_46143 = NOVALUE;
    int _files_46189 = NOVALUE;
    int _32374 = NOVALUE;
    int _32373 = NOVALUE;
    int _32372 = NOVALUE;
    int _32371 = NOVALUE;
    int _32370 = NOVALUE;
    int _32368 = NOVALUE;
    int _24713 = NOVALUE;
    int _24712 = NOVALUE;
    int _24711 = NOVALUE;
    int _24710 = NOVALUE;
    int _24709 = NOVALUE;
    int _24708 = NOVALUE;
    int _24707 = NOVALUE;
    int _24705 = NOVALUE;
    int _24704 = NOVALUE;
    int _24703 = NOVALUE;
    int _24702 = NOVALUE;
    int _24698 = NOVALUE;
    int _24697 = NOVALUE;
    int _24696 = NOVALUE;
    int _24695 = NOVALUE;
    int _24694 = NOVALUE;
    int _24693 = NOVALUE;
    int _24692 = NOVALUE;
    int _24691 = NOVALUE;
    int _24690 = NOVALUE;
    int _24689 = NOVALUE;
    int _24688 = NOVALUE;
    int _24687 = NOVALUE;
    int _24686 = NOVALUE;
    int _24685 = NOVALUE;
    int _24684 = NOVALUE;
    int _24681 = NOVALUE;
    int _24680 = NOVALUE;
    int _24679 = NOVALUE;
    int _24678 = NOVALUE;
    int _24675 = NOVALUE;
    int _24673 = NOVALUE;
    int _24672 = NOVALUE;
    int _24671 = NOVALUE;
    int _24670 = NOVALUE;
    int _24669 = NOVALUE;
    int _24668 = NOVALUE;
    int _24665 = NOVALUE;
    int _24664 = NOVALUE;
    int _24660 = NOVALUE;
    int _24659 = NOVALUE;
    int _24658 = NOVALUE;
    int _24654 = NOVALUE;
    int _24653 = NOVALUE;
    int _24652 = NOVALUE;
    int _24651 = NOVALUE;
    int _24650 = NOVALUE;
    int _24649 = NOVALUE;
    int _24648 = NOVALUE;
    int _24647 = NOVALUE;
    int _24646 = NOVALUE;
    int _24645 = NOVALUE;
    int _24644 = NOVALUE;
    int _24643 = NOVALUE;
    int _24641 = NOVALUE;
    int _24640 = NOVALUE;
    int _24639 = NOVALUE;
    int _24638 = NOVALUE;
    int _24637 = NOVALUE;
    int _24635 = NOVALUE;
    int _24634 = NOVALUE;
    int _24633 = NOVALUE;
    int _24632 = NOVALUE;
    int _24631 = NOVALUE;
    int _24629 = NOVALUE;
    int _24627 = NOVALUE;
    int _24626 = NOVALUE;
    int _24625 = NOVALUE;
    int _24624 = NOVALUE;
    int _24622 = NOVALUE;
    int _24621 = NOVALUE;
    int _24620 = NOVALUE;
    int _24617 = NOVALUE;
    int _24616 = NOVALUE;
    int _24615 = NOVALUE;
    int _24614 = NOVALUE;
    int _24613 = NOVALUE;
    int _24612 = NOVALUE;
    int _24611 = NOVALUE;
    int _24610 = NOVALUE;
    int _24609 = NOVALUE;
    int _24608 = NOVALUE;
    int _24605 = NOVALUE;
    int _24604 = NOVALUE;
    int _24601 = NOVALUE;
    int _24599 = NOVALUE;
    int _24598 = NOVALUE;
    int _24597 = NOVALUE;
    int _24596 = NOVALUE;
    int _24593 = NOVALUE;
    int _24592 = NOVALUE;
    int _24591 = NOVALUE;
    int _24590 = NOVALUE;
    int _24588 = NOVALUE;
    int _24587 = NOVALUE;
    int _24586 = NOVALUE;
    int _24585 = NOVALUE;
    int _24584 = NOVALUE;
    int _24581 = NOVALUE;
    int _24575 = NOVALUE;
    int _24571 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_link_only_46074)) {
        _1 = (long)(DBL_PTR(_link_only_46074)->dbl);
        if (UNIQUE(DBL_PTR(_link_only_46074)) && (DBL_PTR(_link_only_46074)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_link_only_46074);
        _link_only_46074 = _1;
    }

    /** 	if length(the_file0) then*/
    if (IS_SEQUENCE(_the_file0_46075)){
            _24571 = SEQ_PTR(_the_file0_46075)->length;
    }
    else {
        _24571 = 1;
    }
    if (_24571 == 0)
    {
        _24571 = NOVALUE;
        goto L1; // [10] 22
    }
    else{
        _24571 = NOVALUE;
    }

    /** 		file0 = filebase(the_file0)*/
    RefDS(_the_file0_46075);
    _0 = _13filebase(_the_file0_46075);
    DeRef(_59file0_44445);
    _59file0_44445 = _0;
L1: 

    /** 	sequence cmd, objs = "", settings = setup_build(), cwd = current_dir()*/
    RefDS(_22663);
    DeRef(_objs_46082);
    _objs_46082 = _22663;
    _0 = _settings_46083;
    _settings_46083 = _57setup_build();
    DeRef(_0);
    _0 = _cwd_46085;
    _cwd_46085 = _13current_dir();
    DeRef(_0);

    /** 	integer status*/

    /** 	ensure_exename(settings[SETUP_EXE_EXT])*/
    _2 = (int)SEQ_PTR(_settings_46083);
    _24575 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24575);
    _57ensure_exename(_24575);
    _24575 = NOVALUE;

    /** 	if not link_only then*/
    if (_link_only_46074 != 0)
    goto L2; // [54] 122

    /** 		switch compiler_type do*/
    _0 = 0;
    switch ( _0 ){ 

        /** 			case COMPILER_GCC then*/
        case 1:

        /** 				if not silent then*/
        if (_38silent_17081 != 0)
        goto L3; // [74] 121

        /** 					ShowMsg(1, 176, {"GCC"})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24580);
        *((int *)(_2+4)) = _24580;
        _24581 = MAKE_SEQ(_1);
        _47ShowMsg(1, 176, _24581, 1);
        _24581 = NOVALUE;
        goto L3; // [90] 121

        /** 			case COMPILER_WATCOM then*/
        case 2:

        /** 				write_objlink_file()*/
        _57write_objlink_file();

        /** 				if not silent then*/
        if (_38silent_17081 != 0)
        goto L4; // [104] 120

        /** 					ShowMsg(1, 176, {"Watcom"})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24583);
        *((int *)(_2+4)) = _24583;
        _24584 = MAKE_SEQ(_1);
        _47ShowMsg(1, 176, _24584, 1);
        _24584 = NOVALUE;
L4: 
    ;}L3: 
L2: 

    /** 	if sequence(output_dir) and length(output_dir) > 0 then*/
    _24585 = 1;
    if (_24585 == 0) {
        goto L5; // [129] 157
    }
    _24587 = 0;
    _24588 = (0 > 0);
    _24587 = NOVALUE;
    if (_24588 == 0)
    {
        DeRef(_24588);
        _24588 = NOVALUE;
        goto L5; // [143] 157
    }
    else{
        DeRef(_24588);
        _24588 = NOVALUE;
    }

    /** 		chdir(output_dir)*/
    RefDS(_59output_dir_42630);
    _32374 = _13chdir(_59output_dir_42630);
    DeRef(_32374);
    _32374 = NOVALUE;
L5: 

    /** 	sequence link_files = {}*/
    RefDS(_22663);
    DeRef(_link_files_46117);
    _link_files_46117 = _22663;

    /** 	if not link_only then*/
    if (_link_only_46074 != 0)
    goto L6; // [166] 474

    /** 		for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_59generated_files_42612)){
            _24590 = SEQ_PTR(_59generated_files_42612)->length;
    }
    else {
        _24590 = 1;
    }
    {
        int _i_46121;
        _i_46121 = 1;
L7: 
        if (_i_46121 > _24590){
            goto L8; // [176] 471
        }

        /** 			if generated_files[i][$] = 'c' then*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24591 = (int)*(((s1_ptr)_2)->base + _i_46121);
        if (IS_SEQUENCE(_24591)){
                _24592 = SEQ_PTR(_24591)->length;
        }
        else {
            _24592 = 1;
        }
        _2 = (int)SEQ_PTR(_24591);
        _24593 = (int)*(((s1_ptr)_2)->base + _24592);
        _24591 = NOVALUE;
        if (binary_op_a(NOTEQ, _24593, 99)){
            _24593 = NOVALUE;
            goto L9; // [198] 430
        }
        _24593 = NOVALUE;

        /** 				cmd = sprintf("%s %s %s", { settings[SETUP_CEXE], settings[SETUP_CFLAGS],*/
        _2 = (int)SEQ_PTR(_settings_46083);
        _24596 = (int)*(((s1_ptr)_2)->base + 1);
        _2 = (int)SEQ_PTR(_settings_46083);
        _24597 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24598 = (int)*(((s1_ptr)_2)->base + _i_46121);
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_24596);
        *((int *)(_2+4)) = _24596;
        Ref(_24597);
        *((int *)(_2+8)) = _24597;
        RefDS(_24598);
        *((int *)(_2+12)) = _24598;
        _24599 = MAKE_SEQ(_1);
        _24598 = NOVALUE;
        _24597 = NOVALUE;
        _24596 = NOVALUE;
        DeRef(_cmd_46081);
        _cmd_46081 = EPrintf(-9999999, _24595, _24599);
        DeRefDS(_24599);
        _24599 = NOVALUE;

        /** 				link_files = append(link_files, generated_files[i])*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24601 = (int)*(((s1_ptr)_2)->base + _i_46121);
        RefDS(_24601);
        Append(&_link_files_46117, _link_files_46117, _24601);
        _24601 = NOVALUE;

        /** 				if not silent then*/
        if (_38silent_17081 != 0)
        goto LA; // [248] 370

        /** 					atom pdone = 100 * (i / length(generated_files))*/
        if (IS_SEQUENCE(_59generated_files_42612)){
                _24604 = SEQ_PTR(_59generated_files_42612)->length;
        }
        else {
            _24604 = 1;
        }
        _24605 = (_i_46121 % _24604) ? NewDouble((double)_i_46121 / _24604) : (_i_46121 / _24604);
        _24604 = NOVALUE;
        DeRef(_pdone_46143);
        if (IS_ATOM_INT(_24605)) {
            if (_24605 <= INT15 && _24605 >= -INT15)
            _pdone_46143 = 100 * _24605;
            else
            _pdone_46143 = NewDouble(100 * (double)_24605);
        }
        else {
            _pdone_46143 = NewDouble((double)100 * DBL_PTR(_24605)->dbl);
        }
        DeRef(_24605);
        _24605 = NOVALUE;

        /** 					if not verbose then*/
        if (_38verbose_17084 != 0)
        goto LB; // [270] 356

        /** 						if 0 and outdated_files[i] = 0 and force_build = 0 then*/
        if (0 == 0) {
            _24608 = 0;
            goto LC; // [275] 293
        }
        _2 = (int)SEQ_PTR(_59outdated_files_42613);
        _24609 = (int)*(((s1_ptr)_2)->base + _i_46121);
        if (IS_ATOM_INT(_24609)) {
            _24610 = (_24609 == 0);
        }
        else {
            _24610 = binary_op(EQUALS, _24609, 0);
        }
        _24609 = NOVALUE;
        if (IS_ATOM_INT(_24610))
        _24608 = (_24610 != 0);
        else
        _24608 = DBL_PTR(_24610)->dbl != 0.0;
LC: 
        if (_24608 == 0) {
            goto LD; // [293] 334
        }
        _24612 = (0 == 0);
        if (_24612 == 0)
        {
            DeRef(_24612);
            _24612 = NOVALUE;
            goto LD; // [304] 334
        }
        else{
            DeRef(_24612);
            _24612 = NOVALUE;
        }

        /** 							ShowMsg(1, 325, { pdone, generated_files[i] })*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24613 = (int)*(((s1_ptr)_2)->base + _i_46121);
        RefDS(_24613);
        Ref(_pdone_46143);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _pdone_46143;
        ((int *)_2)[2] = _24613;
        _24614 = MAKE_SEQ(_1);
        _24613 = NOVALUE;
        _47ShowMsg(1, 325, _24614, 1);
        _24614 = NOVALUE;

        /** 							continue*/
        DeRef(_pdone_46143);
        _pdone_46143 = NOVALUE;
        goto LE; // [329] 466
        goto LF; // [331] 369
LD: 

        /** 							ShowMsg(1, 163, { pdone, generated_files[i] })*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24615 = (int)*(((s1_ptr)_2)->base + _i_46121);
        RefDS(_24615);
        Ref(_pdone_46143);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _pdone_46143;
        ((int *)_2)[2] = _24615;
        _24616 = MAKE_SEQ(_1);
        _24615 = NOVALUE;
        _47ShowMsg(1, 163, _24616, 1);
        _24616 = NOVALUE;
        goto LF; // [353] 369
LB: 

        /** 						ShowMsg(1, 163, { pdone, cmd })*/
        RefDS(_cmd_46081);
        Ref(_pdone_46143);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _pdone_46143;
        ((int *)_2)[2] = _cmd_46081;
        _24617 = MAKE_SEQ(_1);
        _47ShowMsg(1, 163, _24617, 1);
        _24617 = NOVALUE;
LF: 
LA: 
        DeRef(_pdone_46143);
        _pdone_46143 = NOVALUE;

        /** 				status = system_exec(cmd, 0)*/
        _status_46088 = system_exec_call(_cmd_46081, 0);

        /** 				if status != 0 then*/
        if (_status_46088 == 0)
        goto L10; // [380] 464

        /** 					ShowMsg(2, 164, { generated_files[i] })*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24620 = (int)*(((s1_ptr)_2)->base + _i_46121);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24620);
        *((int *)(_2+4)) = _24620;
        _24621 = MAKE_SEQ(_1);
        _24620 = NOVALUE;
        _47ShowMsg(2, 164, _24621, 1);
        _24621 = NOVALUE;

        /** 					ShowMsg(2, 165, { status, cmd })*/
        RefDS(_cmd_46081);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _status_46088;
        ((int *)_2)[2] = _cmd_46081;
        _24622 = MAKE_SEQ(_1);
        _47ShowMsg(2, 165, _24622, 1);
        _24622 = NOVALUE;

        /** 					goto "build_direct_cleanup"*/
        goto G11;
        goto L10; // [427] 464
L9: 

        /** 			elsif match(".o", generated_files[i]) then*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24624 = (int)*(((s1_ptr)_2)->base + _i_46121);
        _24625 = e_match_from(_23709, _24624, 1);
        _24624 = NOVALUE;
        if (_24625 == 0)
        {
            _24625 = NOVALUE;
            goto L12; // [443] 463
        }
        else{
            _24625 = NOVALUE;
        }

        /** 				objs &= " " & generated_files[i]*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24626 = (int)*(((s1_ptr)_2)->base + _i_46121);
        Concat((object_ptr)&_24627, _23949, _24626);
        _24626 = NOVALUE;
        Concat((object_ptr)&_objs_46082, _objs_46082, _24627);
        DeRefDS(_24627);
        _24627 = NOVALUE;
L12: 
L10: 

        /** 		end for*/
LE: 
        _i_46121 = _i_46121 + 1;
        goto L7; // [466] 183
L8: 
        ;
    }
    goto L13; // [471] 535
L6: 

    /** 		object files = read_lines(file0 & ".bld")*/
    Concat((object_ptr)&_24629, _59file0_44445, _24140);
    _0 = _files_46189;
    _files_46189 = _16read_lines(_24629);
    DeRef(_0);
    _24629 = NOVALUE;

    /** 		for i = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_46189)){
            _24631 = SEQ_PTR(_files_46189)->length;
    }
    else {
        _24631 = 1;
    }
    {
        int _i_46195;
        _i_46195 = 1;
L14: 
        if (_i_46195 > _24631){
            goto L15; // [491] 532
        }

        /** 			objs &= " " & filebase(files[i]) & "." & settings[SETUP_OBJ_EXT]*/
        _2 = (int)SEQ_PTR(_files_46189);
        _24632 = (int)*(((s1_ptr)_2)->base + _i_46195);
        Ref(_24632);
        _24633 = _13filebase(_24632);
        _24632 = NOVALUE;
        _2 = (int)SEQ_PTR(_settings_46083);
        _24634 = (int)*(((s1_ptr)_2)->base + 5);
        {
            int concat_list[4];

            concat_list[0] = _24634;
            concat_list[1] = _23748;
            concat_list[2] = _24633;
            concat_list[3] = _23949;
            Concat_N((object_ptr)&_24635, concat_list, 4);
        }
        _24634 = NOVALUE;
        DeRef(_24633);
        _24633 = NOVALUE;
        Concat((object_ptr)&_objs_46082, _objs_46082, _24635);
        DeRefDS(_24635);
        _24635 = NOVALUE;

        /** 		end for*/
        _i_46195 = _i_46195 + 1;
        goto L14; // [527] 498
L15: 
        ;
    }
    DeRef(_files_46189);
    _files_46189 = NOVALUE;
L13: 

    /** 	if keep and not link_only and length(link_files) then*/
    if (_59keep_42615 == 0) {
        _24637 = 0;
        goto L16; // [539] 550
    }
    _24638 = (_link_only_46074 == 0);
    _24637 = (_24638 != 0);
L16: 
    if (_24637 == 0) {
        goto L17; // [550] 579
    }
    if (IS_SEQUENCE(_link_files_46117)){
            _24640 = SEQ_PTR(_link_files_46117)->length;
    }
    else {
        _24640 = 1;
    }
    if (_24640 == 0)
    {
        _24640 = NOVALUE;
        goto L17; // [558] 579
    }
    else{
        _24640 = NOVALUE;
    }

    /** 		write_lines(file0 & ".bld", link_files)*/
    Concat((object_ptr)&_24641, _59file0_44445, _24140);
    RefDS(_link_files_46117);
    _32373 = _16write_lines(_24641, _link_files_46117);
    _24641 = NOVALUE;
    DeRef(_32373);
    _32373 = NOVALUE;
    goto L18; // [576] 603
L17: 

    /** 	elsif keep = 0 then*/
    if (_59keep_42615 != 0)
    goto L19; // [583] 602

    /** 		delete_file(file0 & ".bld")*/
    Concat((object_ptr)&_24643, _59file0_44445, _24140);
    _32372 = _13delete_file(_24643);
    _24643 = NOVALUE;
    DeRef(_32372);
    _32372 = NOVALUE;
L19: 
L18: 

    /** 	if length(rc_file[D_ALTNAME]) and length(settings[SETUP_RC_COMPILER]) and compiler_type = COMPILER_GCC then*/
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24644 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24644)){
            _24645 = SEQ_PTR(_24644)->length;
    }
    else {
        _24645 = 1;
    }
    _24644 = NOVALUE;
    if (_24645 == 0) {
        _24646 = 0;
        goto L1A; // [616] 633
    }
    _2 = (int)SEQ_PTR(_settings_46083);
    _24647 = (int)*(((s1_ptr)_2)->base + 8);
    if (IS_SEQUENCE(_24647)){
            _24648 = SEQ_PTR(_24647)->length;
    }
    else {
        _24648 = 1;
    }
    _24647 = NOVALUE;
    _24646 = (_24648 != 0);
L1A: 
    if (_24646 == 0) {
        goto L1B; // [633] 738
    }
    _24650 = (0 == 1);
    if (_24650 == 0)
    {
        DeRef(_24650);
        _24650 = NOVALUE;
        goto L1B; // [646] 738
    }
    else{
        DeRef(_24650);
        _24650 = NOVALUE;
    }

    /** 		cmd = text:format(settings[SETUP_RC_COMPILER], { rc_file[D_ALTNAME], res_file[D_ALTNAME] })*/
    _2 = (int)SEQ_PTR(_settings_46083);
    _24651 = (int)*(((s1_ptr)_2)->base + 8);
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24652 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24653 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_24653);
    RefDS(_24652);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24652;
    ((int *)_2)[2] = _24653;
    _24654 = MAKE_SEQ(_1);
    _24653 = NOVALUE;
    _24652 = NOVALUE;
    Ref(_24651);
    _0 = _cmd_46081;
    _cmd_46081 = _10format(_24651, _24654);
    DeRef(_0);
    _24651 = NOVALUE;
    _24654 = NOVALUE;

    /** 		status = system_exec(cmd, 0)*/
    _status_46088 = system_exec_call(_cmd_46081, 0);

    /** 		if status != 0 then*/
    if (_status_46088 == 0)
    goto L1C; // [692] 737

    /** 			ShowMsg(2, 350, { rc_file[D_NAME] })*/
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24658 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_24658);
    *((int *)(_2+4)) = _24658;
    _24659 = MAKE_SEQ(_1);
    _24658 = NOVALUE;
    _47ShowMsg(2, 350, _24659, 1);
    _24659 = NOVALUE;

    /** 			ShowMsg(2, 169, { status, cmd })*/
    RefDS(_cmd_46081);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _status_46088;
    ((int *)_2)[2] = _cmd_46081;
    _24660 = MAKE_SEQ(_1);
    _47ShowMsg(2, 169, _24660, 1);
    _24660 = NOVALUE;

    /** 			goto "build_direct_cleanup"*/
    goto G11;
L1C: 
L1B: 

    /** 	switch compiler_type do*/
    _0 = 0;
    switch ( _0 ){ 

        /** 		case COMPILER_WATCOM then*/
        case 2:

        /** 			cmd = sprintf("%s @%s.lnk", { settings[SETUP_LEXE], file0 })*/
        _2 = (int)SEQ_PTR(_settings_46083);
        _24664 = (int)*(((s1_ptr)_2)->base + 3);
        RefDS(_59file0_44445);
        Ref(_24664);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _24664;
        ((int *)_2)[2] = _59file0_44445;
        _24665 = MAKE_SEQ(_1);
        _24664 = NOVALUE;
        DeRef(_cmd_46081);
        _cmd_46081 = EPrintf(-9999999, _24663, _24665);
        DeRefDS(_24665);
        _24665 = NOVALUE;
        goto L1D; // [769] 848

        /** 		case COMPILER_GCC then*/
        case 1:

        /** 			cmd = sprintf("%s -o %s %s %s %s", { */
        _2 = (int)SEQ_PTR(_settings_46083);
        _24668 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_57exe_name_45273);
        _24669 = (int)*(((s1_ptr)_2)->base + 11);
        Ref(_24669);
        _24670 = _57adjust_for_build_file(_24669);
        _24669 = NOVALUE;
        _2 = (int)SEQ_PTR(_57res_file_45285);
        _24671 = (int)*(((s1_ptr)_2)->base + 11);
        _2 = (int)SEQ_PTR(_settings_46083);
        _24672 = (int)*(((s1_ptr)_2)->base + 4);
        _1 = NewS1(5);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_24668);
        *((int *)(_2+4)) = _24668;
        *((int *)(_2+8)) = _24670;
        RefDS(_objs_46082);
        *((int *)(_2+12)) = _objs_46082;
        RefDS(_24671);
        *((int *)(_2+16)) = _24671;
        Ref(_24672);
        *((int *)(_2+20)) = _24672;
        _24673 = MAKE_SEQ(_1);
        _24672 = NOVALUE;
        _24671 = NOVALUE;
        _24670 = NOVALUE;
        _24668 = NOVALUE;
        DeRef(_cmd_46081);
        _cmd_46081 = EPrintf(-9999999, _24667, _24673);
        DeRefDS(_24673);
        _24673 = NOVALUE;
        goto L1D; // [821] 848

        /** 		case else*/
        default:

        /** 			ShowMsg(2, 167, { compiler_type })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 0;
        _24675 = MAKE_SEQ(_1);
        _47ShowMsg(2, 167, _24675, 1);
        _24675 = NOVALUE;

        /** 			goto "build_direct_cleanup"*/
        goto G11;
    ;}L1D: 

    /** 	if not silent then*/
    if (_38silent_17081 != 0)
    goto L1E; // [852] 906

    /** 		if not verbose then*/
    if (_38verbose_17084 != 0)
    goto L1F; // [859] 890

    /** 			ShowMsg(1, 166, { abbreviate_path(exe_name[D_NAME]) })*/
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24678 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24678);
    RefDS(_22663);
    _24679 = _13abbreviate_path(_24678, _22663);
    _24678 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _24679;
    _24680 = MAKE_SEQ(_1);
    _24679 = NOVALUE;
    _47ShowMsg(1, 166, _24680, 1);
    _24680 = NOVALUE;
    goto L20; // [887] 905
L1F: 

    /** 			ShowMsg(1, 166, { cmd })*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_cmd_46081);
    *((int *)(_2+4)) = _cmd_46081;
    _24681 = MAKE_SEQ(_1);
    _47ShowMsg(1, 166, _24681, 1);
    _24681 = NOVALUE;
L20: 
L1E: 

    /** 	status = system_exec(cmd, 0)*/
    _status_46088 = system_exec_call(_cmd_46081, 0);

    /** 	if status != 0 then*/
    if (_status_46088 == 0)
    goto L21; // [916] 959

    /** 		ShowMsg(2, 168, { exe_name[D_NAME] })*/
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24684 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_24684);
    *((int *)(_2+4)) = _24684;
    _24685 = MAKE_SEQ(_1);
    _24684 = NOVALUE;
    _47ShowMsg(2, 168, _24685, 1);
    _24685 = NOVALUE;

    /** 		ShowMsg(2, 169, { status, cmd })*/
    RefDS(_cmd_46081);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _status_46088;
    ((int *)_2)[2] = _cmd_46081;
    _24686 = MAKE_SEQ(_1);
    _47ShowMsg(2, 169, _24686, 1);
    _24686 = NOVALUE;

    /** 		goto "build_direct_cleanup"*/
    goto G11;
L21: 

    /** 	if length(rc_file[D_ALTNAME]) and length(settings[SETUP_RC_COMPILER]) and compiler_type = COMPILER_WATCOM then*/
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24687 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24687)){
            _24688 = SEQ_PTR(_24687)->length;
    }
    else {
        _24688 = 1;
    }
    _24687 = NOVALUE;
    if (_24688 == 0) {
        _24689 = 0;
        goto L22; // [972] 989
    }
    _2 = (int)SEQ_PTR(_settings_46083);
    _24690 = (int)*(((s1_ptr)_2)->base + 8);
    if (IS_SEQUENCE(_24690)){
            _24691 = SEQ_PTR(_24690)->length;
    }
    else {
        _24691 = 1;
    }
    _24690 = NOVALUE;
    _24689 = (_24691 != 0);
L22: 
    if (_24689 == 0) {
        goto L23; // [989] 1112
    }
    _24693 = (0 == 2);
    if (_24693 == 0)
    {
        DeRef(_24693);
        _24693 = NOVALUE;
        goto L23; // [1002] 1112
    }
    else{
        DeRef(_24693);
        _24693 = NOVALUE;
    }

    /** 		cmd = text:format(settings[SETUP_RC_COMPILER], { rc_file[D_ALTNAME], res_file[D_ALTNAME], exe_name[D_ALTNAME] })*/
    _2 = (int)SEQ_PTR(_settings_46083);
    _24694 = (int)*(((s1_ptr)_2)->base + 8);
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24695 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24696 = (int)*(((s1_ptr)_2)->base + 11);
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24697 = (int)*(((s1_ptr)_2)->base + 11);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_24695);
    *((int *)(_2+4)) = _24695;
    RefDS(_24696);
    *((int *)(_2+8)) = _24696;
    Ref(_24697);
    *((int *)(_2+12)) = _24697;
    _24698 = MAKE_SEQ(_1);
    _24697 = NOVALUE;
    _24696 = NOVALUE;
    _24695 = NOVALUE;
    Ref(_24694);
    _0 = _cmd_46081;
    _cmd_46081 = _10format(_24694, _24698);
    DeRef(_0);
    _24694 = NOVALUE;
    _24698 = NOVALUE;

    /** 		status = system_exec(cmd, 0)*/
    _status_46088 = system_exec_call(_cmd_46081, 0);

    /** 		if status != 0 then*/
    if (_status_46088 == 0)
    goto L24; // [1058] 1111

    /** 			ShowMsg(2, 187, { rc_file[D_NAME], exe_name[D_NAME] })*/
    _2 = (int)SEQ_PTR(_57rc_file_45279);
    _24702 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_57exe_name_45273);
    _24703 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_24703);
    RefDS(_24702);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24702;
    ((int *)_2)[2] = _24703;
    _24704 = MAKE_SEQ(_1);
    _24703 = NOVALUE;
    _24702 = NOVALUE;
    _47ShowMsg(2, 187, _24704, 1);
    _24704 = NOVALUE;

    /** 			ShowMsg(2, 169, { status, cmd })*/
    RefDS(_cmd_46081);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _status_46088;
    ((int *)_2)[2] = _cmd_46081;
    _24705 = MAKE_SEQ(_1);
    _47ShowMsg(2, 169, _24705, 1);
    _24705 = NOVALUE;

    /** 			goto "build_direct_cleanup"*/
    goto G11;
L24: 
L23: 

    /** label "build_direct_cleanup"*/
G11:

    /** 	if keep = 0 then*/
    if (_59keep_42615 != 0)
    goto L25; // [1120] 1267

    /** 		for i = 1 to length(generated_files) do*/
    if (IS_SEQUENCE(_59generated_files_42612)){
            _24707 = SEQ_PTR(_59generated_files_42612)->length;
    }
    else {
        _24707 = 1;
    }
    {
        int _i_46322;
        _i_46322 = 1;
L26: 
        if (_i_46322 > _24707){
            goto L27; // [1131] 1185
        }

        /** 			if verbose then*/
        if (_38verbose_17084 == 0)
        {
            goto L28; // [1142] 1164
        }
        else{
        }

        /** 				ShowMsg(1, 347, { generated_files[i] })*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24708 = (int)*(((s1_ptr)_2)->base + _i_46322);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_24708);
        *((int *)(_2+4)) = _24708;
        _24709 = MAKE_SEQ(_1);
        _24708 = NOVALUE;
        _47ShowMsg(1, 347, _24709, 1);
        _24709 = NOVALUE;
L28: 

        /** 			delete_file(generated_files[i])*/
        _2 = (int)SEQ_PTR(_59generated_files_42612);
        _24710 = (int)*(((s1_ptr)_2)->base + _i_46322);
        RefDS(_24710);
        _32371 = _13delete_file(_24710);
        _24710 = NOVALUE;
        DeRef(_32371);
        _32371 = NOVALUE;

        /** 		end for*/
        _i_46322 = _i_46322 + 1;
        goto L26; // [1180] 1138
L27: 
        ;
    }

    /** 		if length(res_file[D_ALTNAME]) then*/
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24711 = (int)*(((s1_ptr)_2)->base + 11);
    if (IS_SEQUENCE(_24711)){
            _24712 = SEQ_PTR(_24711)->length;
    }
    else {
        _24712 = 1;
    }
    _24711 = NOVALUE;
    if (_24712 == 0)
    {
        _24712 = NOVALUE;
        goto L29; // [1198] 1218
    }
    else{
        _24712 = NOVALUE;
    }

    /** 			delete_file(res_file[D_ALTNAME])*/
    _2 = (int)SEQ_PTR(_57res_file_45285);
    _24713 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_24713);
    _32370 = _13delete_file(_24713);
    _24713 = NOVALUE;
    DeRef(_32370);
    _32370 = NOVALUE;
L29: 

    /** 		if remove_output_dir then*/
L25: 

    /** 	chdir(cwd)*/
    RefDS(_cwd_46085);
    _32368 = _13chdir(_cwd_46085);
    DeRef(_32368);
    _32368 = NOVALUE;

    /** end procedure*/
    DeRefDS(_the_file0_46075);
    DeRef(_cmd_46081);
    DeRef(_objs_46082);
    DeRef(_settings_46083);
    DeRefDS(_cwd_46085);
    DeRef(_link_files_46117);
    DeRef(_24638);
    _24638 = NOVALUE;
    DeRef(_24610);
    _24610 = NOVALUE;
    _24644 = NOVALUE;
    _24647 = NOVALUE;
    _24687 = NOVALUE;
    _24690 = NOVALUE;
    _24711 = NOVALUE;
    return;
    ;
}


void _57write_buildfile()
{
    int _make_command_46363 = NOVALUE;
    int _settings_46403 = NOVALUE;
    int _24741 = NOVALUE;
    int _24740 = NOVALUE;
    int _24736 = NOVALUE;
    int _24735 = NOVALUE;
    int _24734 = NOVALUE;
    int _24732 = NOVALUE;
    int _24731 = NOVALUE;
    int _24730 = NOVALUE;
    int _24729 = NOVALUE;
    int _24728 = NOVALUE;
    int _24727 = NOVALUE;
    int _24726 = NOVALUE;
    int _24725 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch build_system_type do*/
    _0 = 3;
    switch ( _0 ){ 

        /** 		case BUILD_MAKEFILE_FULL then*/
        case 2:

        /** 			write_makefile_full()*/
        _57write_makefile_full();

        /** 			if not silent then*/
        if (_38silent_17081 != 0)
        goto L1; // [22] 138

        /** 				sequence make_command*/

        /** 				if compiler_type = COMPILER_WATCOM then*/

        /** 					make_command = "make -f "*/
        RefDS(_24724);
        DeRefi(_make_command_46363);
        _make_command_46363 = _24724;

        /** 				ShowMsg(1, 170, { cfile_count + 2 })*/
        _24725 = _38cfile_count_17027 + 2;
        if ((long)((unsigned long)_24725 + (unsigned long)HIGH_BITS) >= 0) 
        _24725 = NewDouble((double)_24725);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24725;
        _24726 = MAKE_SEQ(_1);
        _24725 = NOVALUE;
        _47ShowMsg(1, 170, _24726, 1);
        _24726 = NOVALUE;

        /** 				if sequence(output_dir) and length(output_dir) > 0 then*/
        _24727 = 1;
        if (_24727 == 0) {
            goto L2; // [80] 120
        }
        _24729 = 0;
        _24730 = (0 > 0);
        _24729 = NOVALUE;
        if (_24730 == 0)
        {
            DeRef(_24730);
            _24730 = NOVALUE;
            goto L2; // [94] 120
        }
        else{
            DeRef(_24730);
            _24730 = NOVALUE;
        }

        /** 					ShowMsg(1, 174, { output_dir, make_command, file0 })*/
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_59output_dir_42630);
        *((int *)(_2+4)) = _59output_dir_42630;
        RefDS(_make_command_46363);
        *((int *)(_2+8)) = _make_command_46363;
        RefDS(_59file0_44445);
        *((int *)(_2+12)) = _59file0_44445;
        _24731 = MAKE_SEQ(_1);
        _47ShowMsg(1, 174, _24731, 1);
        _24731 = NOVALUE;
        goto L3; // [117] 137
L2: 

        /** 					ShowMsg(1, 172, { make_command, file0 })*/
        RefDS(_59file0_44445);
        RefDS(_make_command_46363);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _make_command_46363;
        ((int *)_2)[2] = _59file0_44445;
        _24732 = MAKE_SEQ(_1);
        _47ShowMsg(1, 172, _24732, 1);
        _24732 = NOVALUE;
L3: 
L1: 
        DeRefi(_make_command_46363);
        _make_command_46363 = NOVALUE;
        goto L4; // [140] 265

        /** 		case BUILD_MAKEFILE_PARTIAL then*/
        case 1:

        /** 			write_makefile_partial()*/
        _57write_makefile_partial();

        /** 			if not silent then*/
        if (_38silent_17081 != 0)
        goto L4; // [154] 265

        /** 				ShowMsg(1, 170, { cfile_count + 2 })*/
        _24734 = _38cfile_count_17027 + 2;
        if ((long)((unsigned long)_24734 + (unsigned long)HIGH_BITS) >= 0) 
        _24734 = NewDouble((double)_24734);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24734;
        _24735 = MAKE_SEQ(_1);
        _24734 = NOVALUE;
        _47ShowMsg(1, 170, _24735, 1);
        _24735 = NOVALUE;

        /** 				ShowMsg(1, 173, { file0 })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_59file0_44445);
        *((int *)(_2+4)) = _59file0_44445;
        _24736 = MAKE_SEQ(_1);
        _47ShowMsg(1, 173, _24736, 1);
        _24736 = NOVALUE;
        goto L4; // [190] 265

        /** 		case BUILD_DIRECT then*/
        case 3:

        /** 			build_direct()*/
        RefDS(_22663);
        _57build_direct(0, _22663);

        /** 			if not silent then*/
        if (_38silent_17081 != 0)
        goto L5; // [206] 217

        /** 				sequence settings = setup_build()*/
        _0 = _settings_46403;
        _settings_46403 = _57setup_build();
        DeRef(_0);
L5: 
        DeRef(_settings_46403);
        _settings_46403 = NOVALUE;
        goto L4; // [219] 265

        /** 		case BUILD_NONE then*/
        case 0:

        /** 			if not silent then*/
        if (_38silent_17081 != 0)
        goto L4; // [229] 265

        /** 				ShowMsg(1, 170, { cfile_count + 2 })*/
        _24740 = _38cfile_count_17027 + 2;
        if ((long)((unsigned long)_24740 + (unsigned long)HIGH_BITS) >= 0) 
        _24740 = NewDouble((double)_24740);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _24740;
        _24741 = MAKE_SEQ(_1);
        _24740 = NOVALUE;
        _47ShowMsg(1, 170, _24741, 1);
        _24741 = NOVALUE;
        goto L4; // [251] 265

        /** 		case else*/
        default:

        /** 			CompileErr(151)*/
        RefDS(_22663);
        _46CompileErr(151, _22663, 0);
    ;}L4: 

    /** end procedure*/
    return;
    ;
}



// 0x01C08F74
