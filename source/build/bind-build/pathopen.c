// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _44convert_from_OEM(int _s_50528)
{
    int _ls_50529 = NOVALUE;
    int _rc_50530 = NOVALUE;
    int _26576 = NOVALUE;
    int _26575 = NOVALUE;
    int _26573 = NOVALUE;
    int _26572 = NOVALUE;
    int _26569 = NOVALUE;
    int _26568 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ls=length(s)*/
    if (IS_SEQUENCE(_s_50528)){
            _ls_50529 = SEQ_PTR(_s_50528)->length;
    }
    else {
        _ls_50529 = 1;
    }

    /** 	if ls>convert_length then*/
    if (_ls_50529 <= _44convert_length_50508)
    goto L1; // [12] 47

    /** 		free(convert_buffer)*/
    Ref(_44convert_buffer_50507);
    _4free(_44convert_buffer_50507);

    /** 		convert_length=and_bits(ls+15,-16)+1*/
    _26568 = _ls_50529 + 15;
    {unsigned long tu;
         tu = (unsigned long)_26568 & (unsigned long)-16;
         _26569 = MAKE_UINT(tu);
    }
    _26568 = NOVALUE;
    if (IS_ATOM_INT(_26569)) {
        _44convert_length_50508 = _26569 + 1;
    }
    else
    { // coercing _44convert_length_50508 to an integer 1
        _44convert_length_50508 = 1+(long)(DBL_PTR(_26569)->dbl);
        if( !IS_ATOM_INT(_44convert_length_50508) ){
            _44convert_length_50508 = (object)DBL_PTR(_44convert_length_50508)->dbl;
        }
    }
    DeRef(_26569);
    _26569 = NOVALUE;

    /** 		convert_buffer=allocate(convert_length)*/
    _0 = _4allocate(_44convert_length_50508, 0);
    DeRef(_44convert_buffer_50507);
    _44convert_buffer_50507 = _0;
L1: 

    /** 	poke(convert_buffer,s)*/
    if (IS_ATOM_INT(_44convert_buffer_50507)){
        poke_addr = (unsigned char *)_44convert_buffer_50507;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_44convert_buffer_50507)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_50528);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	poke(convert_buffer+ls,0)*/
    if (IS_ATOM_INT(_44convert_buffer_50507)) {
        _26572 = _44convert_buffer_50507 + _ls_50529;
        if ((long)((unsigned long)_26572 + (unsigned long)HIGH_BITS) >= 0) 
        _26572 = NewDouble((double)_26572);
    }
    else {
        _26572 = NewDouble(DBL_PTR(_44convert_buffer_50507)->dbl + (double)_ls_50529);
    }
    if (IS_ATOM_INT(_26572)){
        poke_addr = (unsigned char *)_26572;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_26572)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_26572);
    _26572 = NOVALUE;

    /** 	rc=c_func(oem2char,{convert_buffer,convert_buffer}) -- always nonzero*/
    Ref(_44convert_buffer_50507);
    Ref(_44convert_buffer_50507);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _44convert_buffer_50507;
    ((int *)_2)[2] = _44convert_buffer_50507;
    _26573 = MAKE_SEQ(_1);
    _rc_50530 = call_c(1, _44oem2char_50506, _26573);
    DeRefDS(_26573);
    _26573 = NOVALUE;
    if (!IS_ATOM_INT(_rc_50530)) {
        _1 = (long)(DBL_PTR(_rc_50530)->dbl);
        if (UNIQUE(DBL_PTR(_rc_50530)) && (DBL_PTR(_rc_50530)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rc_50530);
        _rc_50530 = _1;
    }

    /** 	return peek({convert_buffer,ls}) */
    Ref(_44convert_buffer_50507);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _44convert_buffer_50507;
    ((int *)_2)[2] = _ls_50529;
    _26575 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_26575);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _26576 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_26575);
    _26575 = NOVALUE;
    DeRefDS(_s_50528);
    return _26576;
    ;
}


int _44exe_path()
{
    int _26597 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(exe_path_cache) then*/
    _26597 = IS_SEQUENCE(_44exe_path_cache_50584);
    if (_26597 == 0)
    {
        _26597 = NOVALUE;
        goto L1; // [8] 20
    }
    else{
        _26597 = NOVALUE;
    }

    /** 		return exe_path_cache*/
    Ref(_44exe_path_cache_50584);
    return _44exe_path_cache_50584;
L1: 

    /** 	exe_path_cache = command_line()*/
    DeRef(_44exe_path_cache_50584);
    _44exe_path_cache_50584 = Command_Line();

    /** 	exe_path_cache = exe_path_cache[1]*/
    _0 = _44exe_path_cache_50584;
    _2 = (int)SEQ_PTR(_44exe_path_cache_50584);
    _44exe_path_cache_50584 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_44exe_path_cache_50584);
    DeRefDS(_0);

    /** 	return exe_path_cache*/
    RefDS(_44exe_path_cache_50584);
    return _44exe_path_cache_50584;
    ;
}


int _44check_cache(int _env_50596, int _inc_path_50597)
{
    int _delim_50598 = NOVALUE;
    int _pos_50599 = NOVALUE;
    int _26648 = NOVALUE;
    int _26647 = NOVALUE;
    int _26646 = NOVALUE;
    int _26645 = NOVALUE;
    int _26643 = NOVALUE;
    int _26642 = NOVALUE;
    int _26641 = NOVALUE;
    int _26640 = NOVALUE;
    int _26639 = NOVALUE;
    int _26638 = NOVALUE;
    int _26637 = NOVALUE;
    int _26636 = NOVALUE;
    int _26635 = NOVALUE;
    int _26634 = NOVALUE;
    int _26633 = NOVALUE;
    int _26629 = NOVALUE;
    int _26628 = NOVALUE;
    int _26627 = NOVALUE;
    int _26626 = NOVALUE;
    int _26625 = NOVALUE;
    int _26624 = NOVALUE;
    int _26623 = NOVALUE;
    int _26622 = NOVALUE;
    int _26620 = NOVALUE;
    int _26619 = NOVALUE;
    int _26618 = NOVALUE;
    int _26617 = NOVALUE;
    int _26616 = NOVALUE;
    int _26615 = NOVALUE;
    int _26613 = NOVALUE;
    int _26612 = NOVALUE;
    int _26611 = NOVALUE;
    int _26610 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not num_var then -- first time the var is accessed, add cache entry*/
    if (_44num_var_50549 != 0)
    goto L1; // [9] 94

    /** 		cache_vars = append(cache_vars,env)*/
    RefDS(_env_50596);
    Append(&_44cache_vars_50550, _44cache_vars_50550, _env_50596);

    /** 		cache_strings = append(cache_strings,inc_path)*/
    RefDS(_inc_path_50597);
    Append(&_44cache_strings_50551, _44cache_strings_50551, _inc_path_50597);

    /** 		cache_substrings = append(cache_substrings,{})*/
    RefDS(_22663);
    Append(&_44cache_substrings_50552, _44cache_substrings_50552, _22663);

    /** 		cache_starts = append(cache_starts,{})*/
    RefDS(_22663);
    Append(&_44cache_starts_50553, _44cache_starts_50553, _22663);

    /** 		cache_ends = append(cache_ends,{})*/
    RefDS(_22663);
    Append(&_44cache_ends_50554, _44cache_ends_50554, _22663);

    /** 		ifdef WINDOWS then*/

    /** 			cache_converted = append(cache_converted,{})*/
    RefDS(_22663);
    Append(&_44cache_converted_50555, _44cache_converted_50555, _22663);

    /** 		num_var = length(cache_vars)*/
    if (IS_SEQUENCE(_44cache_vars_50550)){
            _44num_var_50549 = SEQ_PTR(_44cache_vars_50550)->length;
    }
    else {
        _44num_var_50549 = 1;
    }

    /** 		cache_complete &= 0*/
    Append(&_44cache_complete_50556, _44cache_complete_50556, 0);

    /** 		cache_delims &= 0*/
    Append(&_44cache_delims_50557, _44cache_delims_50557, 0);

    /** 		return 0*/
    DeRefDS(_env_50596);
    DeRefDSi(_inc_path_50597);
    return 0;
    goto L2; // [91] 456
L1: 

    /** 		if compare(inc_path,cache_strings[num_var]) then*/
    _2 = (int)SEQ_PTR(_44cache_strings_50551);
    _26610 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    if (IS_ATOM_INT(_inc_path_50597) && IS_ATOM_INT(_26610)){
        _26611 = (_inc_path_50597 < _26610) ? -1 : (_inc_path_50597 > _26610);
    }
    else{
        _26611 = compare(_inc_path_50597, _26610);
    }
    _26610 = NOVALUE;
    if (_26611 == 0)
    {
        _26611 = NOVALUE;
        goto L3; // [108] 455
    }
    else{
        _26611 = NOVALUE;
    }

    /** 			cache_strings[num_var] = inc_path*/
    RefDS(_inc_path_50597);
    _2 = (int)SEQ_PTR(_44cache_strings_50551);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _44cache_strings_50551 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
    _1 = *(int *)_2;
    *(int *)_2 = _inc_path_50597;
    DeRefDS(_1);

    /** 			cache_complete[num_var] = 0*/
    _2 = (int)SEQ_PTR(_44cache_complete_50556);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _44cache_complete_50556 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
    *(int *)_2 = 0;

    /** 			if match(cache_strings[num_var],inc_path)!=1 then -- try to salvage what we can*/
    _2 = (int)SEQ_PTR(_44cache_strings_50551);
    _26612 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    _26613 = e_match_from(_26612, _inc_path_50597, 1);
    _26612 = NOVALUE;
    if (_26613 == 1)
    goto L4; // [146] 454

    /** 				pos = -1*/
    _pos_50599 = -1;

    /** 				for i=1 to length(cache_strings[num_var]) do*/
    _2 = (int)SEQ_PTR(_44cache_strings_50551);
    _26615 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    if (IS_SEQUENCE(_26615)){
            _26616 = SEQ_PTR(_26615)->length;
    }
    else {
        _26616 = 1;
    }
    _26615 = NOVALUE;
    {
        int _i_50620;
        _i_50620 = 1;
L5: 
        if (_i_50620 > _26616){
            goto L6; // [168] 453
        }

        /** 					if cache_ends[num_var][i] > length(inc_path) or */
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        _26617 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        _2 = (int)SEQ_PTR(_26617);
        _26618 = (int)*(((s1_ptr)_2)->base + _i_50620);
        _26617 = NOVALUE;
        if (IS_SEQUENCE(_inc_path_50597)){
                _26619 = SEQ_PTR(_inc_path_50597)->length;
        }
        else {
            _26619 = 1;
        }
        if (IS_ATOM_INT(_26618)) {
            _26620 = (_26618 > _26619);
        }
        else {
            _26620 = binary_op(GREATER, _26618, _26619);
        }
        _26618 = NOVALUE;
        _26619 = NOVALUE;
        if (IS_ATOM_INT(_26620)) {
            if (_26620 != 0) {
                goto L7; // [196] 250
            }
        }
        else {
            if (DBL_PTR(_26620)->dbl != 0.0) {
                goto L7; // [196] 250
            }
        }
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        _26622 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        _2 = (int)SEQ_PTR(_26622);
        _26623 = (int)*(((s1_ptr)_2)->base + _i_50620);
        _26622 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_starts_50553);
        _26624 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        _2 = (int)SEQ_PTR(_26624);
        _26625 = (int)*(((s1_ptr)_2)->base + _i_50620);
        _26624 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        _26626 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        _2 = (int)SEQ_PTR(_26626);
        _26627 = (int)*(((s1_ptr)_2)->base + _i_50620);
        _26626 = NOVALUE;
        rhs_slice_target = (object_ptr)&_26628;
        RHS_Slice(_inc_path_50597, _26625, _26627);
        if (IS_ATOM_INT(_26623) && IS_ATOM_INT(_26628)){
            _26629 = (_26623 < _26628) ? -1 : (_26623 > _26628);
        }
        else{
            _26629 = compare(_26623, _26628);
        }
        _26623 = NOVALUE;
        DeRefDS(_26628);
        _26628 = NOVALUE;
        if (_26629 == 0)
        {
            _26629 = NOVALUE;
            goto L8; // [246] 261
        }
        else{
            _26629 = NOVALUE;
        }
L7: 

        /** 						pos = i-1*/
        _pos_50599 = _i_50620 - 1;

        /** 						exit*/
        goto L6; // [258] 453
L8: 

        /** 					if pos = 0 then*/
        if (_pos_50599 != 0)
        goto L9; // [263] 276

        /** 						return 0*/
        DeRefDS(_env_50596);
        DeRefDSi(_inc_path_50597);
        _26615 = NOVALUE;
        DeRef(_26620);
        _26620 = NOVALUE;
        _26625 = NOVALUE;
        _26627 = NOVALUE;
        return 0;
        goto LA; // [273] 446
L9: 

        /** 					elsif pos >0 then -- crop cache data*/
        if (_pos_50599 <= 0)
        goto LB; // [278] 445

        /** 						cache_substrings[num_var] = cache_substrings[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        _26633 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        rhs_slice_target = (object_ptr)&_26634;
        RHS_Slice(_26633, 1, _pos_50599);
        _26633 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_substrings_50552 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26634;
        if( _1 != _26634 ){
            DeRefDS(_1);
        }
        _26634 = NOVALUE;

        /** 						cache_starts[num_var] = cache_starts[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_44cache_starts_50553);
        _26635 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        rhs_slice_target = (object_ptr)&_26636;
        RHS_Slice(_26635, 1, _pos_50599);
        _26635 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_starts_50553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_starts_50553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26636;
        if( _1 != _26636 ){
            DeRef(_1);
        }
        _26636 = NOVALUE;

        /** 						cache_ends[num_var] = cache_ends[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        _26637 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        rhs_slice_target = (object_ptr)&_26638;
        RHS_Slice(_26637, 1, _pos_50599);
        _26637 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_ends_50554 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26638;
        if( _1 != _26638 ){
            DeRef(_1);
        }
        _26638 = NOVALUE;

        /** 						ifdef WINDOWS then*/

        /** 							cache_converted[num_var] = cache_converted[num_var][1..pos]*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26639 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        rhs_slice_target = (object_ptr)&_26640;
        RHS_Slice(_26639, 1, _pos_50599);
        _26639 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26640;
        if( _1 != _26640 ){
            DeRef(_1);
        }
        _26640 = NOVALUE;

        /** 						delim = cache_ends[num_var][$]+1*/
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        _26641 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26641)){
                _26642 = SEQ_PTR(_26641)->length;
        }
        else {
            _26642 = 1;
        }
        _2 = (int)SEQ_PTR(_26641);
        _26643 = (int)*(((s1_ptr)_2)->base + _26642);
        _26641 = NOVALUE;
        if (IS_ATOM_INT(_26643)) {
            _delim_50598 = _26643 + 1;
        }
        else
        { // coercing _delim_50598 to an integer 1
            _delim_50598 = 1+(long)(DBL_PTR(_26643)->dbl);
            if( !IS_ATOM_INT(_delim_50598) ){
                _delim_50598 = (object)DBL_PTR(_delim_50598)->dbl;
            }
        }
        _26643 = NOVALUE;

        /** 						while delim <= length(inc_path) and delim != PATH_SEPARATOR do*/
LC: 
        if (IS_SEQUENCE(_inc_path_50597)){
                _26645 = SEQ_PTR(_inc_path_50597)->length;
        }
        else {
            _26645 = 1;
        }
        _26646 = (_delim_50598 <= _26645);
        _26645 = NOVALUE;
        if (_26646 == 0) {
            goto LD; // [409] 434
        }
        _26648 = (_delim_50598 != 59);
        if (_26648 == 0)
        {
            DeRef(_26648);
            _26648 = NOVALUE;
            goto LD; // [420] 434
        }
        else{
            DeRef(_26648);
            _26648 = NOVALUE;
        }

        /** 							delim+=1*/
        _delim_50598 = _delim_50598 + 1;

        /** 						end while*/
        goto LC; // [431] 402
LD: 

        /** 						cache_delims[num_var] = delim*/
        _2 = (int)SEQ_PTR(_44cache_delims_50557);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_delims_50557 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        *(int *)_2 = _delim_50598;
LB: 
LA: 

        /** 				end for*/
        _i_50620 = _i_50620 + 1;
        goto L5; // [448] 175
L6: 
        ;
    }
L4: 
L3: 
L2: 

    /** 	return 1*/
    DeRefDS(_env_50596);
    DeRefDSi(_inc_path_50597);
    _26615 = NOVALUE;
    DeRef(_26646);
    _26646 = NOVALUE;
    DeRef(_26620);
    _26620 = NOVALUE;
    _26625 = NOVALUE;
    _26627 = NOVALUE;
    return 1;
    ;
}


int _44get_conf_dirs()
{
    int _delimiter_50663 = NOVALUE;
    int _dirs_50664 = NOVALUE;
    int _26653 = NOVALUE;
    int _26651 = NOVALUE;
    int _26650 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef UNIX then*/

    /** 		delimiter = ';'*/
    _delimiter_50663 = 59;

    /** 	dirs = ""*/
    RefDS(_22663);
    DeRef(_dirs_50664);
    _dirs_50664 = _22663;

    /** 	for i = 1 to length(config_inc_paths) do*/
    if (IS_SEQUENCE(_44config_inc_paths_50558)){
            _26650 = SEQ_PTR(_44config_inc_paths_50558)->length;
    }
    else {
        _26650 = 1;
    }
    {
        int _i_50666;
        _i_50666 = 1;
L1: 
        if (_i_50666 > _26650){
            goto L2; // [22] 68
        }

        /** 		dirs &= config_inc_paths[i]*/
        _2 = (int)SEQ_PTR(_44config_inc_paths_50558);
        _26651 = (int)*(((s1_ptr)_2)->base + _i_50666);
        Concat((object_ptr)&_dirs_50664, _dirs_50664, _26651);
        _26651 = NOVALUE;

        /** 		if i != length(config_inc_paths) then*/
        if (IS_SEQUENCE(_44config_inc_paths_50558)){
                _26653 = SEQ_PTR(_44config_inc_paths_50558)->length;
        }
        else {
            _26653 = 1;
        }
        if (_i_50666 == _26653)
        goto L3; // [48] 61

        /** 			dirs &= delimiter*/
        Append(&_dirs_50664, _dirs_50664, _delimiter_50663);
L3: 

        /** 	end for*/
        _i_50666 = _i_50666 + 1;
        goto L1; // [63] 29
L2: 
        ;
    }

    /** 	return dirs*/
    return _dirs_50664;
    ;
}


int _44strip_file_from_path(int _full_path_50676)
{
    int _26659 = NOVALUE;
    int _26657 = NOVALUE;
    int _26656 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length(full_path) to 1 by -1 do*/
    if (IS_SEQUENCE(_full_path_50676)){
            _26656 = SEQ_PTR(_full_path_50676)->length;
    }
    else {
        _26656 = 1;
    }
    {
        int _i_50678;
        _i_50678 = _26656;
L1: 
        if (_i_50678 < 1){
            goto L2; // [8] 46
        }

        /** 		if full_path[i] = SLASH then*/
        _2 = (int)SEQ_PTR(_full_path_50676);
        _26657 = (int)*(((s1_ptr)_2)->base + _i_50678);
        if (binary_op_a(NOTEQ, _26657, 92)){
            _26657 = NOVALUE;
            goto L3; // [23] 39
        }
        _26657 = NOVALUE;

        /** 			return full_path[1..i]*/
        rhs_slice_target = (object_ptr)&_26659;
        RHS_Slice(_full_path_50676, 1, _i_50678);
        DeRefDS(_full_path_50676);
        return _26659;
L3: 

        /** 	end for*/
        _i_50678 = _i_50678 + -1;
        goto L1; // [41] 15
L2: 
        ;
    }

    /** 	return ""*/
    RefDS(_22663);
    DeRefDS(_full_path_50676);
    DeRef(_26659);
    _26659 = NOVALUE;
    return _22663;
    ;
}


int _44expand_path(int _path_50687, int _prefix_50688)
{
    int _absolute_50689 = NOVALUE;
    int _26674 = NOVALUE;
    int _26673 = NOVALUE;
    int _26672 = NOVALUE;
    int _26671 = NOVALUE;
    int _26670 = NOVALUE;
    int _26669 = NOVALUE;
    int _26665 = NOVALUE;
    int _26664 = NOVALUE;
    int _26663 = NOVALUE;
    int _26660 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(path) then*/
    if (IS_SEQUENCE(_path_50687)){
            _26660 = SEQ_PTR(_path_50687)->length;
    }
    else {
        _26660 = 1;
    }
    if (_26660 != 0)
    goto L1; // [10] 22
    _26660 = NOVALUE;

    /** 		return pwd*/
    RefDS(_44pwd_50585);
    DeRefDS(_path_50687);
    DeRefDS(_prefix_50688);
    return _44pwd_50585;
L1: 

    /** 	ifdef UNIX then*/

    /** 		absolute = find(path[1], SLASH_CHARS) or find(':', path)*/
    _2 = (int)SEQ_PTR(_path_50687);
    _26663 = (int)*(((s1_ptr)_2)->base + 1);
    _26664 = find_from(_26663, _42SLASH_CHARS_17124, 1);
    _26663 = NOVALUE;
    _26665 = find_from(58, _path_50687, 1);
    _absolute_50689 = (_26664 != 0 || _26665 != 0);
    _26664 = NOVALUE;
    _26665 = NOVALUE;

    /** 	if not absolute then*/
    if (_absolute_50689 != 0)
    goto L2; // [50] 64

    /** 		path = prefix & SLASH & path*/
    {
        int concat_list[3];

        concat_list[0] = _path_50687;
        concat_list[1] = 92;
        concat_list[2] = _prefix_50688;
        Concat_N((object_ptr)&_path_50687, concat_list, 3);
    }
L2: 

    /** 	if length(path) and not find(path[$], SLASH_CHARS) then*/
    if (IS_SEQUENCE(_path_50687)){
            _26669 = SEQ_PTR(_path_50687)->length;
    }
    else {
        _26669 = 1;
    }
    if (_26669 == 0) {
        goto L3; // [69] 103
    }
    if (IS_SEQUENCE(_path_50687)){
            _26671 = SEQ_PTR(_path_50687)->length;
    }
    else {
        _26671 = 1;
    }
    _2 = (int)SEQ_PTR(_path_50687);
    _26672 = (int)*(((s1_ptr)_2)->base + _26671);
    _26673 = find_from(_26672, _42SLASH_CHARS_17124, 1);
    _26672 = NOVALUE;
    _26674 = (_26673 == 0);
    _26673 = NOVALUE;
    if (_26674 == 0)
    {
        DeRef(_26674);
        _26674 = NOVALUE;
        goto L3; // [91] 103
    }
    else{
        DeRef(_26674);
        _26674 = NOVALUE;
    }

    /** 		path &= SLASH*/
    Append(&_path_50687, _path_50687, 92);
L3: 

    /** 	return path*/
    DeRefDS(_prefix_50688);
    return _path_50687;
    ;
}


void _44add_include_directory(int _path_50715)
{
    int _26677 = NOVALUE;
    int _0, _1, _2;
    

    /** 	path = expand_path( path, pwd )*/
    RefDS(_path_50715);
    RefDS(_44pwd_50585);
    _0 = _path_50715;
    _path_50715 = _44expand_path(_path_50715, _44pwd_50585);
    DeRefDS(_0);

    /** 	if not find( path, config_inc_paths ) then*/
    _26677 = find_from(_path_50715, _44config_inc_paths_50558, 1);
    if (_26677 != 0)
    goto L1; // [23] 35
    _26677 = NOVALUE;

    /** 		config_inc_paths = append( config_inc_paths, path )*/
    RefDS(_path_50715);
    Append(&_44config_inc_paths_50558, _44config_inc_paths_50558, _path_50715);
L1: 

    /** end procedure*/
    DeRefDS(_path_50715);
    return;
    ;
}


int _44load_euphoria_config(int _file_50724)
{
    int _fn_50725 = NOVALUE;
    int _in_50726 = NOVALUE;
    int _spos_50727 = NOVALUE;
    int _epos_50728 = NOVALUE;
    int _conf_path_50729 = NOVALUE;
    int _new_args_50730 = NOVALUE;
    int _arg_50731 = NOVALUE;
    int _parm_50732 = NOVALUE;
    int _section_50733 = NOVALUE;
    int _needed_50830 = NOVALUE;
    int _26775 = NOVALUE;
    int _26774 = NOVALUE;
    int _26771 = NOVALUE;
    int _26770 = NOVALUE;
    int _26768 = NOVALUE;
    int _26767 = NOVALUE;
    int _26745 = NOVALUE;
    int _26742 = NOVALUE;
    int _26741 = NOVALUE;
    int _26739 = NOVALUE;
    int _26735 = NOVALUE;
    int _26733 = NOVALUE;
    int _26731 = NOVALUE;
    int _26729 = NOVALUE;
    int _26727 = NOVALUE;
    int _26725 = NOVALUE;
    int _26724 = NOVALUE;
    int _26723 = NOVALUE;
    int _26722 = NOVALUE;
    int _26721 = NOVALUE;
    int _26720 = NOVALUE;
    int _26719 = NOVALUE;
    int _26718 = NOVALUE;
    int _26716 = NOVALUE;
    int _26714 = NOVALUE;
    int _26712 = NOVALUE;
    int _26708 = NOVALUE;
    int _26707 = NOVALUE;
    int _26702 = NOVALUE;
    int _26700 = NOVALUE;
    int _26698 = NOVALUE;
    int _26697 = NOVALUE;
    int _26689 = NOVALUE;
    int _26683 = NOVALUE;
    int _26682 = NOVALUE;
    int _26680 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence new_args = {}*/
    RefDS(_22663);
    DeRef(_new_args_50730);
    _new_args_50730 = _22663;

    /** 	if file_type(file) = FILETYPE_DIRECTORY then*/
    RefDS(_file_50724);
    _26680 = _13file_type(_file_50724);
    if (binary_op_a(NOTEQ, _26680, 2)){
        DeRef(_26680);
        _26680 = NOVALUE;
        goto L1; // [18] 53
    }
    DeRef(_26680);
    _26680 = NOVALUE;

    /** 		if file[$] != SLASH then*/
    if (IS_SEQUENCE(_file_50724)){
            _26682 = SEQ_PTR(_file_50724)->length;
    }
    else {
        _26682 = 1;
    }
    _2 = (int)SEQ_PTR(_file_50724);
    _26683 = (int)*(((s1_ptr)_2)->base + _26682);
    if (binary_op_a(EQUALS, _26683, 92)){
        _26683 = NOVALUE;
        goto L2; // [33] 46
    }
    _26683 = NOVALUE;

    /** 			file &= SLASH*/
    Append(&_file_50724, _file_50724, 92);
L2: 

    /** 		file &= "eu.cfg"*/
    Concat((object_ptr)&_file_50724, _file_50724, _26686);
L1: 

    /** 	conf_path = canonical_path( file,,CORRECT )*/
    RefDS(_file_50724);
    _0 = _conf_path_50729;
    _conf_path_50729 = _13canonical_path(_file_50724, 0, 2);
    DeRef(_0);

    /** 	if find(conf_path, seen_conf) != 0 then*/
    _26689 = find_from(_conf_path_50729, _44seen_conf_50721, 1);
    if (_26689 == 0)
    goto L3; // [74] 85

    /** 		return {}*/
    RefDS(_22663);
    DeRefDS(_file_50724);
    DeRefi(_in_50726);
    DeRefDS(_conf_path_50729);
    DeRef(_new_args_50730);
    DeRefi(_arg_50731);
    DeRefi(_parm_50732);
    DeRef(_section_50733);
    return _22663;
L3: 

    /** 	seen_conf = append(seen_conf, conf_path)*/
    RefDS(_conf_path_50729);
    Append(&_44seen_conf_50721, _44seen_conf_50721, _conf_path_50729);

    /** 	section = "all"*/
    RefDS(_26692);
    DeRef(_section_50733);
    _section_50733 = _26692;

    /** 	fn = open( conf_path, "r" )*/
    _fn_50725 = EOpen(_conf_path_50729, _26693, 0);

    /** 	if fn = -1 then return {} end if*/
    if (_fn_50725 != -1)
    goto L4; // [109] 118
    RefDS(_22663);
    DeRefDS(_file_50724);
    DeRefi(_in_50726);
    DeRefDS(_conf_path_50729);
    DeRef(_new_args_50730);
    DeRefi(_arg_50731);
    DeRefi(_parm_50732);
    DeRefDSi(_section_50733);
    return _22663;
L4: 

    /** 	in = gets( fn )*/
    DeRefi(_in_50726);
    _in_50726 = EGets(_fn_50725);

    /** 	while sequence( in ) do*/
L5: 
    _26697 = IS_SEQUENCE(_in_50726);
    if (_26697 == 0)
    {
        _26697 = NOVALUE;
        goto L6; // [131] 771
    }
    else{
        _26697 = NOVALUE;
    }

    /** 		spos = 1*/
    _spos_50727 = 1;

    /** 		while spos <= length(in) do*/
L7: 
    if (IS_SEQUENCE(_in_50726)){
            _26698 = SEQ_PTR(_in_50726)->length;
    }
    else {
        _26698 = 1;
    }
    if (_spos_50727 > _26698)
    goto L8; // [147] 182

    /** 			if find( in[spos], "\n\r \t" ) = 0 then*/
    _2 = (int)SEQ_PTR(_in_50726);
    _26700 = (int)*(((s1_ptr)_2)->base + _spos_50727);
    _26702 = find_from(_26700, _26701, 1);
    _26700 = NOVALUE;
    if (_26702 != 0)
    goto L9; // [162] 171

    /** 				exit*/
    goto L8; // [168] 182
L9: 

    /** 			spos += 1*/
    _spos_50727 = _spos_50727 + 1;

    /** 		end while*/
    goto L7; // [179] 144
L8: 

    /** 		epos = length(in)*/
    if (IS_SEQUENCE(_in_50726)){
            _epos_50728 = SEQ_PTR(_in_50726)->length;
    }
    else {
        _epos_50728 = 1;
    }

    /** 		while epos >= spos do*/
LA: 
    if (_epos_50728 < _spos_50727)
    goto LB; // [192] 227

    /** 			if find( in[epos], "\n\r \t" ) = 0 then*/
    _2 = (int)SEQ_PTR(_in_50726);
    _26707 = (int)*(((s1_ptr)_2)->base + _epos_50728);
    _26708 = find_from(_26707, _26701, 1);
    _26707 = NOVALUE;
    if (_26708 != 0)
    goto LC; // [207] 216

    /** 				exit*/
    goto LB; // [213] 227
LC: 

    /** 			epos -= 1*/
    _epos_50728 = _epos_50728 - 1;

    /** 		end while*/
    goto LA; // [224] 192
LB: 

    /** 		in = in[spos .. epos]		*/
    rhs_slice_target = (object_ptr)&_in_50726;
    RHS_Slice(_in_50726, _spos_50727, _epos_50728);

    /** 		arg = ""*/
    RefDS(_22663);
    DeRefi(_arg_50731);
    _arg_50731 = _22663;

    /** 		parm = ""*/
    RefDS(_22663);
    DeRefi(_parm_50732);
    _parm_50732 = _22663;

    /** 		if length(in) > 0 then*/
    if (IS_SEQUENCE(_in_50726)){
            _26712 = SEQ_PTR(_in_50726)->length;
    }
    else {
        _26712 = 1;
    }
    if (_26712 <= 0)
    goto LD; // [253] 477

    /** 			if in[1] = '[' then*/
    _2 = (int)SEQ_PTR(_in_50726);
    _26714 = (int)*(((s1_ptr)_2)->base + 1);
    if (_26714 != 91)
    goto LE; // [263] 354

    /** 				section = in[2..$]*/
    if (IS_SEQUENCE(_in_50726)){
            _26716 = SEQ_PTR(_in_50726)->length;
    }
    else {
        _26716 = 1;
    }
    rhs_slice_target = (object_ptr)&_section_50733;
    RHS_Slice(_in_50726, 2, _26716);

    /** 				if length(section) > 0 and section[$] = ']' then*/
    if (IS_SEQUENCE(_section_50733)){
            _26718 = SEQ_PTR(_section_50733)->length;
    }
    else {
        _26718 = 1;
    }
    _26719 = (_26718 > 0);
    _26718 = NOVALUE;
    if (_26719 == 0) {
        goto LF; // [286] 320
    }
    if (IS_SEQUENCE(_section_50733)){
            _26721 = SEQ_PTR(_section_50733)->length;
    }
    else {
        _26721 = 1;
    }
    _2 = (int)SEQ_PTR(_section_50733);
    _26722 = (int)*(((s1_ptr)_2)->base + _26721);
    _26723 = (_26722 == 93);
    _26722 = NOVALUE;
    if (_26723 == 0)
    {
        DeRef(_26723);
        _26723 = NOVALUE;
        goto LF; // [302] 320
    }
    else{
        DeRef(_26723);
        _26723 = NOVALUE;
    }

    /** 					section = section[1..$-1]*/
    if (IS_SEQUENCE(_section_50733)){
            _26724 = SEQ_PTR(_section_50733)->length;
    }
    else {
        _26724 = 1;
    }
    _26725 = _26724 - 1;
    _26724 = NOVALUE;
    rhs_slice_target = (object_ptr)&_section_50733;
    RHS_Slice(_section_50733, 1, _26725);
LF: 

    /** 				section = lower(trim(section))*/
    RefDS(_section_50733);
    RefDS(_4494);
    _26727 = _10trim(_section_50733, _4494, 0);
    _0 = _section_50733;
    _section_50733 = _10lower(_26727);
    DeRefDS(_0);
    _26727 = NOVALUE;

    /** 				if length(section) = 0 then*/
    if (IS_SEQUENCE(_section_50733)){
            _26729 = SEQ_PTR(_section_50733)->length;
    }
    else {
        _26729 = 1;
    }
    if (_26729 != 0)
    goto L10; // [339] 476

    /** 					section = "all"*/
    RefDS(_26692);
    DeRefDS(_section_50733);
    _section_50733 = _26692;
    goto L10; // [351] 476
LE: 

    /** 			elsif length(in) > 2 then*/
    if (IS_SEQUENCE(_in_50726)){
            _26731 = SEQ_PTR(_in_50726)->length;
    }
    else {
        _26731 = 1;
    }
    if (_26731 <= 2)
    goto L11; // [359] 461

    /** 				if in[1] = '-' then*/
    _2 = (int)SEQ_PTR(_in_50726);
    _26733 = (int)*(((s1_ptr)_2)->base + 1);
    if (_26733 != 45)
    goto L12; // [369] 443

    /** 					if in[2] != '-' then*/
    _2 = (int)SEQ_PTR(_in_50726);
    _26735 = (int)*(((s1_ptr)_2)->base + 2);
    if (_26735 == 45)
    goto L10; // [379] 476

    /** 						spos = find(' ', in)*/
    _spos_50727 = find_from(32, _in_50726, 1);

    /** 						if spos = 0 then*/
    if (_spos_50727 != 0)
    goto L13; // [392] 413

    /** 							arg = in*/
    Ref(_in_50726);
    DeRefi(_arg_50731);
    _arg_50731 = _in_50726;

    /** 							parm = ""*/
    RefDS(_22663);
    DeRefi(_parm_50732);
    _parm_50732 = _22663;
    goto L10; // [410] 476
L13: 

    /** 							arg = in[1..spos - 1]*/
    _26739 = _spos_50727 - 1;
    rhs_slice_target = (object_ptr)&_arg_50731;
    RHS_Slice(_in_50726, 1, _26739);

    /** 							parm = in[spos + 1 .. $]*/
    _26741 = _spos_50727 + 1;
    if (_26741 > MAXINT){
        _26741 = NewDouble((double)_26741);
    }
    if (IS_SEQUENCE(_in_50726)){
            _26742 = SEQ_PTR(_in_50726)->length;
    }
    else {
        _26742 = 1;
    }
    rhs_slice_target = (object_ptr)&_parm_50732;
    RHS_Slice(_in_50726, _26741, _26742);
    goto L10; // [440] 476
L12: 

    /** 					arg = "-i"*/
    RefDS(_26744);
    DeRefi(_arg_50731);
    _arg_50731 = _26744;

    /** 					parm = in*/
    Ref(_in_50726);
    DeRefi(_parm_50732);
    _parm_50732 = _in_50726;
    goto L10; // [458] 476
L11: 

    /** 				arg = "-i"*/
    RefDS(_26744);
    DeRefi(_arg_50731);
    _arg_50731 = _26744;

    /** 				parm = in*/
    Ref(_in_50726);
    DeRefi(_parm_50732);
    _parm_50732 = _in_50726;
L10: 
LD: 

    /** 		if length(arg) > 0 then*/
    if (IS_SEQUENCE(_arg_50731)){
            _26745 = SEQ_PTR(_arg_50731)->length;
    }
    else {
        _26745 = 1;
    }
    if (_26745 <= 0)
    goto L14; // [482] 759

    /** 			integer needed = 0*/
    _needed_50830 = 0;

    /** 			switch section do*/
    _1 = find(_section_50733, _26747);
    switch ( _1 ){ 

        /** 				case "all" then*/
        case 1:

        /** 					needed = 1*/
        _needed_50830 = 1;
        goto L15; // [507] 691

        /** 				case "windows" then*/
        case 2:

        /** 					needed = TWINDOWS*/
        _needed_50830 = _42TWINDOWS_17110;
        goto L15; // [522] 691

        /** 				case "unix" then*/
        case 3:

        /** 					needed = TUNIX*/
        _needed_50830 = 0;
        goto L15; // [537] 691

        /** 				case "translate" then*/
        case 4:

        /** 					needed = TRANSLATE*/
        _needed_50830 = _38TRANSLATE_16564;
        goto L15; // [552] 691

        /** 				case "translate:windows" then*/
        case 5:

        /** 					needed = TRANSLATE and TWINDOWS*/
        _needed_50830 = (_38TRANSLATE_16564 != 0 && _42TWINDOWS_17110 != 0);
        goto L15; // [570] 691

        /** 				case "translate:unix" then*/
        case 6:

        /** 					needed = TRANSLATE and TUNIX*/
        _needed_50830 = (_38TRANSLATE_16564 != 0 && 0 != 0);
        goto L15; // [588] 691

        /** 				case "interpret" then*/
        case 7:

        /** 					needed = INTERPRET*/
        _needed_50830 = _38INTERPRET_16561;
        goto L15; // [603] 691

        /** 				case "interpret:windows" then*/
        case 8:

        /** 					needed = INTERPRET and TWINDOWS*/
        _needed_50830 = (_38INTERPRET_16561 != 0 && _42TWINDOWS_17110 != 0);
        goto L15; // [621] 691

        /** 				case "interpret:unix" then*/
        case 9:

        /** 					needed = INTERPRET and TUNIX*/
        _needed_50830 = (_38INTERPRET_16561 != 0 && 0 != 0);
        goto L15; // [639] 691

        /** 				case "bind" then*/
        case 10:

        /** 					needed = BIND*/
        _needed_50830 = _38BIND_16567;
        goto L15; // [654] 691

        /** 				case "bind:windows" then*/
        case 11:

        /** 					needed = BIND and TWINDOWS*/
        _needed_50830 = (_38BIND_16567 != 0 && _42TWINDOWS_17110 != 0);
        goto L15; // [672] 691

        /** 				case "bind:unix" then*/
        case 12:

        /** 					needed = BIND and TUNIX*/
        _needed_50830 = (_38BIND_16567 != 0 && 0 != 0);
    ;}L15: 

    /** 			if needed then*/
    if (_needed_50830 == 0)
    {
        goto L16; // [693] 758
    }
    else{
    }

    /** 				if equal(arg, "-c") then*/
    if (_arg_50731 == _26766)
    _26767 = 1;
    else if (IS_ATOM_INT(_arg_50731) && IS_ATOM_INT(_26766))
    _26767 = 0;
    else
    _26767 = (compare(_arg_50731, _26766) == 0);
    if (_26767 == 0)
    {
        _26767 = NOVALUE;
        goto L17; // [702] 731
    }
    else{
        _26767 = NOVALUE;
    }

    /** 					if length(parm) > 0 then*/
    if (IS_SEQUENCE(_parm_50732)){
            _26768 = SEQ_PTR(_parm_50732)->length;
    }
    else {
        _26768 = 1;
    }
    if (_26768 <= 0)
    goto L18; // [710] 757

    /** 						new_args &= load_euphoria_config(parm)*/
    RefDS(_parm_50732);
    DeRef(_26770);
    _26770 = _parm_50732;
    _26771 = _44load_euphoria_config(_26770);
    _26770 = NOVALUE;
    if (IS_SEQUENCE(_new_args_50730) && IS_ATOM(_26771)) {
        Ref(_26771);
        Append(&_new_args_50730, _new_args_50730, _26771);
    }
    else if (IS_ATOM(_new_args_50730) && IS_SEQUENCE(_26771)) {
    }
    else {
        Concat((object_ptr)&_new_args_50730, _new_args_50730, _26771);
    }
    DeRef(_26771);
    _26771 = NOVALUE;
    goto L18; // [728] 757
L17: 

    /** 					new_args = append(new_args, arg)*/
    RefDS(_arg_50731);
    Append(&_new_args_50730, _new_args_50730, _arg_50731);

    /** 					if length(parm > 0) then*/
    _26774 = binary_op(GREATER, _parm_50732, 0);
    if (IS_SEQUENCE(_26774)){
            _26775 = SEQ_PTR(_26774)->length;
    }
    else {
        _26775 = 1;
    }
    DeRefDS(_26774);
    _26774 = NOVALUE;
    if (_26775 == 0)
    {
        _26775 = NOVALUE;
        goto L19; // [746] 756
    }
    else{
        _26775 = NOVALUE;
    }

    /** 						new_args = append(new_args, parm)*/
    RefDS(_parm_50732);
    Append(&_new_args_50730, _new_args_50730, _parm_50732);
L19: 
L18: 
L16: 
L14: 

    /** 		in = gets( fn )*/
    DeRefi(_in_50726);
    _in_50726 = EGets(_fn_50725);

    /** 	end while*/
    goto L5; // [768] 128
L6: 

    /** 	close(fn)*/
    EClose(_fn_50725);

    /** 	return new_args*/
    DeRefDS(_file_50724);
    DeRefi(_in_50726);
    DeRef(_conf_path_50729);
    DeRefi(_arg_50731);
    DeRefi(_parm_50732);
    DeRef(_section_50733);
    _26714 = NOVALUE;
    DeRef(_26719);
    _26719 = NOVALUE;
    DeRef(_26725);
    _26725 = NOVALUE;
    _26733 = NOVALUE;
    _26735 = NOVALUE;
    DeRef(_26739);
    _26739 = NOVALUE;
    DeRef(_26741);
    _26741 = NOVALUE;
    _26774 = NOVALUE;
    return _new_args_50730;
    ;
}


int _44GetDefaultArgs(int _user_files_50898)
{
    int _env_50899 = NOVALUE;
    int _default_args_50900 = NOVALUE;
    int _conf_file_50901 = NOVALUE;
    int _cmd_options_50903 = NOVALUE;
    int _user_config_50909 = NOVALUE;
    int _26818 = NOVALUE;
    int _26817 = NOVALUE;
    int _26816 = NOVALUE;
    int _26813 = NOVALUE;
    int _26812 = NOVALUE;
    int _26811 = NOVALUE;
    int _26810 = NOVALUE;
    int _26807 = NOVALUE;
    int _26806 = NOVALUE;
    int _26805 = NOVALUE;
    int _26804 = NOVALUE;
    int _26800 = NOVALUE;
    int _26799 = NOVALUE;
    int _26798 = NOVALUE;
    int _26796 = NOVALUE;
    int _26790 = NOVALUE;
    int _26789 = NOVALUE;
    int _26787 = NOVALUE;
    int _26785 = NOVALUE;
    int _26784 = NOVALUE;
    int _26780 = NOVALUE;
    int _26779 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence default_args = {}*/
    RefDS(_22663);
    DeRef(_default_args_50900);
    _default_args_50900 = _22663;

    /** 	sequence conf_file = "eu.cfg"*/
    RefDS(_26686);
    DeRefi(_conf_file_50901);
    _conf_file_50901 = _26686;

    /** 	if loaded_config_inc_paths then return "" end if*/
    if (_44loaded_config_inc_paths_50583 == 0)
    {
        goto L1; // [21] 29
    }
    else{
    }
    RefDS(_22663);
    DeRefDS(_user_files_50898);
    DeRef(_env_50899);
    DeRefDS(_default_args_50900);
    DeRefDSi(_conf_file_50901);
    DeRef(_cmd_options_50903);
    return _22663;
L1: 

    /** 	loaded_config_inc_paths = 1*/
    _44loaded_config_inc_paths_50583 = 1;

    /** 	sequence cmd_options = get_options()*/
    _0 = _cmd_options_50903;
    _cmd_options_50903 = _45get_options();
    DeRef(_0);

    /** 	default_args = {}*/
    RefDS(_22663);
    DeRef(_default_args_50900);
    _default_args_50900 = _22663;

    /** 	for i = 1 to length( user_files ) do*/
    if (IS_SEQUENCE(_user_files_50898)){
            _26779 = SEQ_PTR(_user_files_50898)->length;
    }
    else {
        _26779 = 1;
    }
    {
        int _i_50907;
        _i_50907 = 1;
L2: 
        if (_i_50907 > _26779){
            goto L3; // [53] 92
        }

        /** 		sequence user_config = load_euphoria_config( user_files[i] )*/
        _2 = (int)SEQ_PTR(_user_files_50898);
        _26780 = (int)*(((s1_ptr)_2)->base + _i_50907);
        Ref(_26780);
        _0 = _user_config_50909;
        _user_config_50909 = _44load_euphoria_config(_26780);
        DeRef(_0);
        _26780 = NOVALUE;

        /** 		default_args = merge_parameters( user_config, default_args, cmd_options, 1 )*/
        RefDS(_user_config_50909);
        RefDS(_default_args_50900);
        RefDS(_cmd_options_50903);
        _0 = _default_args_50900;
        _default_args_50900 = _45merge_parameters(_user_config_50909, _default_args_50900, _cmd_options_50903, 1);
        DeRefDS(_0);
        DeRefDS(_user_config_50909);
        _user_config_50909 = NOVALUE;

        /** 	end for*/
        _i_50907 = _i_50907 + 1;
        goto L2; // [87] 60
L3: 
        ;
    }

    /** 	default_args = merge_parameters( load_euphoria_config("./" & conf_file), default_args, cmd_options, 1 )*/
    Concat((object_ptr)&_26784, _26783, _conf_file_50901);
    _26785 = _44load_euphoria_config(_26784);
    _26784 = NOVALUE;
    RefDS(_default_args_50900);
    RefDS(_cmd_options_50903);
    _0 = _default_args_50900;
    _default_args_50900 = _45merge_parameters(_26785, _default_args_50900, _cmd_options_50903, 1);
    DeRefDS(_0);
    _26785 = NOVALUE;

    /** 	env = strip_file_from_path( exe_path() )*/
    _26787 = _44exe_path();
    _0 = _env_50899;
    _env_50899 = _44strip_file_from_path(_26787);
    DeRef(_0);
    _26787 = NOVALUE;

    /** 	default_args = merge_parameters( load_euphoria_config( env & conf_file ), default_args, cmd_options, 1 )*/
    if (IS_SEQUENCE(_env_50899) && IS_ATOM(_conf_file_50901)) {
    }
    else if (IS_ATOM(_env_50899) && IS_SEQUENCE(_conf_file_50901)) {
        Ref(_env_50899);
        Prepend(&_26789, _conf_file_50901, _env_50899);
    }
    else {
        Concat((object_ptr)&_26789, _env_50899, _conf_file_50901);
    }
    _26790 = _44load_euphoria_config(_26789);
    _26789 = NOVALUE;
    RefDS(_default_args_50900);
    RefDS(_cmd_options_50903);
    _0 = _default_args_50900;
    _default_args_50900 = _45merge_parameters(_26790, _default_args_50900, _cmd_options_50903, 1);
    DeRefDS(_0);
    _26790 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 		env = getenv( "ALLUSERSPROFILE" )*/
    DeRef(_env_50899);
    _env_50899 = EGetEnv(_26794);

    /** 		if sequence(env) then*/
    _26796 = IS_SEQUENCE(_env_50899);
    if (_26796 == 0)
    {
        _26796 = NOVALUE;
        goto L4; // [151] 179
    }
    else{
        _26796 = NOVALUE;
    }

    /** 			default_args = merge_parameters( load_euphoria_config( expand_path( "euphoria", env ) & conf_file ), default_args, cmd_options, 1 )*/
    RefDS(_26797);
    Ref(_env_50899);
    _26798 = _44expand_path(_26797, _env_50899);
    if (IS_SEQUENCE(_26798) && IS_ATOM(_conf_file_50901)) {
    }
    else if (IS_ATOM(_26798) && IS_SEQUENCE(_conf_file_50901)) {
        Ref(_26798);
        Prepend(&_26799, _conf_file_50901, _26798);
    }
    else {
        Concat((object_ptr)&_26799, _26798, _conf_file_50901);
        DeRef(_26798);
        _26798 = NOVALUE;
    }
    DeRef(_26798);
    _26798 = NOVALUE;
    _26800 = _44load_euphoria_config(_26799);
    _26799 = NOVALUE;
    RefDS(_default_args_50900);
    RefDS(_cmd_options_50903);
    _0 = _default_args_50900;
    _default_args_50900 = _45merge_parameters(_26800, _default_args_50900, _cmd_options_50903, 1);
    DeRefDS(_0);
    _26800 = NOVALUE;
L4: 

    /** 		env = getenv( "APPDATA" )*/
    DeRef(_env_50899);
    _env_50899 = EGetEnv(_26802);

    /** 		if sequence(env) then*/
    _26804 = IS_SEQUENCE(_env_50899);
    if (_26804 == 0)
    {
        _26804 = NOVALUE;
        goto L5; // [189] 217
    }
    else{
        _26804 = NOVALUE;
    }

    /** 			default_args = merge_parameters( load_euphoria_config( expand_path( "euphoria", env ) & conf_file ), default_args, cmd_options, 1 )*/
    RefDS(_26797);
    Ref(_env_50899);
    _26805 = _44expand_path(_26797, _env_50899);
    if (IS_SEQUENCE(_26805) && IS_ATOM(_conf_file_50901)) {
    }
    else if (IS_ATOM(_26805) && IS_SEQUENCE(_conf_file_50901)) {
        Ref(_26805);
        Prepend(&_26806, _conf_file_50901, _26805);
    }
    else {
        Concat((object_ptr)&_26806, _26805, _conf_file_50901);
        DeRef(_26805);
        _26805 = NOVALUE;
    }
    DeRef(_26805);
    _26805 = NOVALUE;
    _26807 = _44load_euphoria_config(_26806);
    _26806 = NOVALUE;
    RefDS(_default_args_50900);
    RefDS(_cmd_options_50903);
    _0 = _default_args_50900;
    _default_args_50900 = _45merge_parameters(_26807, _default_args_50900, _cmd_options_50903, 1);
    DeRefDS(_0);
    _26807 = NOVALUE;
L5: 

    /** 		env = getenv( "HOMEPATH" )*/
    DeRef(_env_50899);
    _env_50899 = EGetEnv(_26582);

    /** 		if sequence(env) then*/
    _26810 = IS_SEQUENCE(_env_50899);
    if (_26810 == 0)
    {
        _26810 = NOVALUE;
        goto L6; // [227] 256
    }
    else{
        _26810 = NOVALUE;
    }

    /** 			default_args = merge_parameters( load_euphoria_config( getenv( "HOMEDRIVE" ) & env & "\\" & conf_file ), default_args, cmd_options, 1 )*/
    _26811 = EGetEnv(_26584);
    {
        int concat_list[4];

        concat_list[0] = _conf_file_50901;
        concat_list[1] = _22990;
        concat_list[2] = _env_50899;
        concat_list[3] = _26811;
        Concat_N((object_ptr)&_26812, concat_list, 4);
    }
    DeRef(_26811);
    _26811 = NOVALUE;
    _26813 = _44load_euphoria_config(_26812);
    _26812 = NOVALUE;
    RefDS(_default_args_50900);
    RefDS(_cmd_options_50903);
    _0 = _default_args_50900;
    _default_args_50900 = _45merge_parameters(_26813, _default_args_50900, _cmd_options_50903, 1);
    DeRefDS(_0);
    _26813 = NOVALUE;
L6: 

    /** 	env = get_eudir()*/
    _0 = _env_50899;
    _env_50899 = _35get_eudir();
    DeRef(_0);

    /** 	if sequence(env) then*/
    _26816 = IS_SEQUENCE(_env_50899);
    if (_26816 == 0)
    {
        _26816 = NOVALUE;
        goto L7; // [266] 291
    }
    else{
        _26816 = NOVALUE;
    }

    /** 		default_args = merge_parameters( load_euphoria_config(env & "/" & conf_file), default_args, cmd_options, 1 )*/
    {
        int concat_list[3];

        concat_list[0] = _conf_file_50901;
        concat_list[1] = _24119;
        concat_list[2] = _env_50899;
        Concat_N((object_ptr)&_26817, concat_list, 3);
    }
    _26818 = _44load_euphoria_config(_26817);
    _26817 = NOVALUE;
    RefDS(_default_args_50900);
    RefDS(_cmd_options_50903);
    _0 = _default_args_50900;
    _default_args_50900 = _45merge_parameters(_26818, _default_args_50900, _cmd_options_50903, 1);
    DeRefDS(_0);
    _26818 = NOVALUE;
L7: 

    /** 	return default_args*/
    DeRefDS(_user_files_50898);
    DeRef(_env_50899);
    DeRefi(_conf_file_50901);
    DeRef(_cmd_options_50903);
    return _default_args_50900;
    ;
}


int _44ConfPath(int _file_name_50964)
{
    int _file_path_50965 = NOVALUE;
    int _try_50966 = NOVALUE;
    int _26825 = NOVALUE;
    int _26821 = NOVALUE;
    int _26820 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(config_inc_paths) do*/
    if (IS_SEQUENCE(_44config_inc_paths_50558)){
            _26820 = SEQ_PTR(_44config_inc_paths_50558)->length;
    }
    else {
        _26820 = 1;
    }
    {
        int _i_50968;
        _i_50968 = 1;
L1: 
        if (_i_50968 > _26820){
            goto L2; // [10] 60
        }

        /** 		file_path = config_inc_paths[i] & file_name*/
        _2 = (int)SEQ_PTR(_44config_inc_paths_50558);
        _26821 = (int)*(((s1_ptr)_2)->base + _i_50968);
        Concat((object_ptr)&_file_path_50965, _26821, _file_name_50964);
        _26821 = NOVALUE;
        _26821 = NOVALUE;

        /** 		try = open( file_path, "r" )*/
        _try_50966 = EOpen(_file_path_50965, _26693, 0);

        /** 		if try != -1 then*/
        if (_try_50966 == -1)
        goto L3; // [38] 53

        /** 			return {file_path, try}*/
        RefDS(_file_path_50965);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_50965;
        ((int *)_2)[2] = _try_50966;
        _26825 = MAKE_SEQ(_1);
        DeRefDS(_file_name_50964);
        DeRefDS(_file_path_50965);
        return _26825;
L3: 

        /** 	end for*/
        _i_50968 = _i_50968 + 1;
        goto L1; // [55] 17
L2: 
        ;
    }

    /** 	return -1*/
    DeRefDS(_file_name_50964);
    DeRef(_file_path_50965);
    DeRef(_26825);
    _26825 = NOVALUE;
    return -1;
    ;
}


int _44ScanPath(int _file_name_50978, int _env_50979, int _flag_50980)
{
    int _inc_path_50981 = NOVALUE;
    int _full_path_50982 = NOVALUE;
    int _file_path_50983 = NOVALUE;
    int _strings_50984 = NOVALUE;
    int _end_path_50985 = NOVALUE;
    int _start_path_50986 = NOVALUE;
    int _try_50987 = NOVALUE;
    int _use_cache_50988 = NOVALUE;
    int _pos_50989 = NOVALUE;
    int _26903 = NOVALUE;
    int _26902 = NOVALUE;
    int _26901 = NOVALUE;
    int _26900 = NOVALUE;
    int _26899 = NOVALUE;
    int _26898 = NOVALUE;
    int _26897 = NOVALUE;
    int _26896 = NOVALUE;
    int _26895 = NOVALUE;
    int _26894 = NOVALUE;
    int _26893 = NOVALUE;
    int _26892 = NOVALUE;
    int _26891 = NOVALUE;
    int _26890 = NOVALUE;
    int _26889 = NOVALUE;
    int _26888 = NOVALUE;
    int _26883 = NOVALUE;
    int _26882 = NOVALUE;
    int _26881 = NOVALUE;
    int _26880 = NOVALUE;
    int _26879 = NOVALUE;
    int _26875 = NOVALUE;
    int _26874 = NOVALUE;
    int _26873 = NOVALUE;
    int _26872 = NOVALUE;
    int _26871 = NOVALUE;
    int _26870 = NOVALUE;
    int _26868 = NOVALUE;
    int _26866 = NOVALUE;
    int _26865 = NOVALUE;
    int _26863 = NOVALUE;
    int _26862 = NOVALUE;
    int _26861 = NOVALUE;
    int _26858 = NOVALUE;
    int _26857 = NOVALUE;
    int _26855 = NOVALUE;
    int _26854 = NOVALUE;
    int _26853 = NOVALUE;
    int _26851 = NOVALUE;
    int _26849 = NOVALUE;
    int _26844 = NOVALUE;
    int _26843 = NOVALUE;
    int _26842 = NOVALUE;
    int _26841 = NOVALUE;
    int _26840 = NOVALUE;
    int _26835 = NOVALUE;
    int _26827 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_flag_50980)) {
        _1 = (long)(DBL_PTR(_flag_50980)->dbl);
        if (UNIQUE(DBL_PTR(_flag_50980)) && (DBL_PTR(_flag_50980)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_flag_50980);
        _flag_50980 = _1;
    }

    /** 	inc_path = getenv(env)*/
    DeRefi(_inc_path_50981);
    _inc_path_50981 = EGetEnv(_env_50979);

    /** 	if compare(inc_path,{})!=1 then -- nothing to do, just fail*/
    if (IS_ATOM_INT(_inc_path_50981) && IS_ATOM_INT(_22663)){
        _26827 = (_inc_path_50981 < _22663) ? -1 : (_inc_path_50981 > _22663);
    }
    else{
        _26827 = compare(_inc_path_50981, _22663);
    }
    if (_26827 == 1)
    goto L1; // [18] 29

    /** 		return -1*/
    DeRefDS(_file_name_50978);
    DeRefDS(_env_50979);
    DeRefi(_inc_path_50981);
    DeRef(_full_path_50982);
    DeRef(_file_path_50983);
    DeRef(_strings_50984);
    return -1;
L1: 

    /** 	num_var = find(env,cache_vars)*/
    _44num_var_50549 = find_from(_env_50979, _44cache_vars_50550, 1);

    /** 	use_cache = check_cache(env,inc_path)*/
    RefDS(_env_50979);
    Ref(_inc_path_50981);
    _use_cache_50988 = _44check_cache(_env_50979, _inc_path_50981);
    if (!IS_ATOM_INT(_use_cache_50988)) {
        _1 = (long)(DBL_PTR(_use_cache_50988)->dbl);
        if (UNIQUE(DBL_PTR(_use_cache_50988)) && (DBL_PTR(_use_cache_50988)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_use_cache_50988);
        _use_cache_50988 = _1;
    }

    /** 	inc_path = append(inc_path, PATH_SEPARATOR)*/
    Append(&_inc_path_50981, _inc_path_50981, 59);

    /** 	file_name = SLASH & file_name*/
    Prepend(&_file_name_50978, _file_name_50978, 92);

    /** 	if flag then*/
    if (_flag_50980 == 0)
    {
        goto L2; // [65] 77
    }
    else{
    }

    /** 		file_name = include_subfolder & file_name*/
    Concat((object_ptr)&_file_name_50978, _44include_subfolder_50545, _file_name_50978);
L2: 

    /** 	strings = cache_substrings[num_var]*/
    DeRef(_strings_50984);
    _2 = (int)SEQ_PTR(_44cache_substrings_50552);
    _strings_50984 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    RefDS(_strings_50984);

    /** 	if use_cache then*/
    if (_use_cache_50988 == 0)
    {
        goto L3; // [91] 292
    }
    else{
    }

    /** 		for i=1 to length(strings) do*/
    if (IS_SEQUENCE(_strings_50984)){
            _26835 = SEQ_PTR(_strings_50984)->length;
    }
    else {
        _26835 = 1;
    }
    {
        int _i_51005;
        _i_51005 = 1;
L4: 
        if (_i_51005 > _26835){
            goto L5; // [99] 252
        }

        /** 			full_path = strings[i]*/
        DeRef(_full_path_50982);
        _2 = (int)SEQ_PTR(_strings_50984);
        _full_path_50982 = (int)*(((s1_ptr)_2)->base + _i_51005);
        Ref(_full_path_50982);

        /** 			file_path = full_path & file_name*/
        Concat((object_ptr)&_file_path_50983, _full_path_50982, _file_name_50978);

        /** 			try = open_locked(file_path)    */
        RefDS(_file_path_50983);
        _try_50987 = _35open_locked(_file_path_50983);
        if (!IS_ATOM_INT(_try_50987)) {
            _1 = (long)(DBL_PTR(_try_50987)->dbl);
            if (UNIQUE(DBL_PTR(_try_50987)) && (DBL_PTR(_try_50987)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_50987);
            _try_50987 = _1;
        }

        /** 			if try != -1 then*/
        if (_try_50987 == -1)
        goto L6; // [130] 145

        /** 				return {file_path,try}*/
        RefDS(_file_path_50983);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_50983;
        ((int *)_2)[2] = _try_50987;
        _26840 = MAKE_SEQ(_1);
        DeRefDS(_file_name_50978);
        DeRefDS(_env_50979);
        DeRefi(_inc_path_50981);
        DeRefDS(_full_path_50982);
        DeRefDS(_file_path_50983);
        DeRefDS(_strings_50984);
        return _26840;
L6: 

        /** 			ifdef WINDOWS then */

        /** 				if sequence(cache_converted[num_var][i]) then*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26841 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        _2 = (int)SEQ_PTR(_26841);
        _26842 = (int)*(((s1_ptr)_2)->base + _i_51005);
        _26841 = NOVALUE;
        _26843 = IS_SEQUENCE(_26842);
        _26842 = NOVALUE;
        if (_26843 == 0)
        {
            _26843 = NOVALUE;
            goto L7; // [164] 245
        }
        else{
            _26843 = NOVALUE;
        }

        /** 					full_path = cache_converted[num_var][i]*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26844 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        DeRef(_full_path_50982);
        _2 = (int)SEQ_PTR(_26844);
        _full_path_50982 = (int)*(((s1_ptr)_2)->base + _i_51005);
        Ref(_full_path_50982);
        _26844 = NOVALUE;

        /** 					file_path = full_path & file_name*/
        Concat((object_ptr)&_file_path_50983, _full_path_50982, _file_name_50978);

        /** 					try = open_locked(file_path)*/
        RefDS(_file_path_50983);
        _try_50987 = _35open_locked(_file_path_50983);
        if (!IS_ATOM_INT(_try_50987)) {
            _1 = (long)(DBL_PTR(_try_50987)->dbl);
            if (UNIQUE(DBL_PTR(_try_50987)) && (DBL_PTR(_try_50987)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_50987);
            _try_50987 = _1;
        }

        /** 					if try != -1 then*/
        if (_try_50987 == -1)
        goto L8; // [199] 244

        /** 						cache_converted[num_var][i] = 0*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _3 = (int)(_44num_var_50549 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_51005);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);
        _26849 = NOVALUE;

        /** 						cache_substrings[num_var][i] = full_path*/
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_substrings_50552 = MAKE_SEQ(_2);
        }
        _3 = (int)(_44num_var_50549 + ((s1_ptr)_2)->base);
        RefDS(_full_path_50982);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_51005);
        _1 = *(int *)_2;
        *(int *)_2 = _full_path_50982;
        DeRef(_1);
        _26851 = NOVALUE;

        /** 						return {file_path,try}*/
        RefDS(_file_path_50983);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_50983;
        ((int *)_2)[2] = _try_50987;
        _26853 = MAKE_SEQ(_1);
        DeRefDS(_file_name_50978);
        DeRefDS(_env_50979);
        DeRefi(_inc_path_50981);
        DeRefDS(_full_path_50982);
        DeRefDS(_file_path_50983);
        DeRef(_strings_50984);
        DeRef(_26840);
        _26840 = NOVALUE;
        return _26853;
L8: 
L7: 

        /** 		end for*/
        _i_51005 = _i_51005 + 1;
        goto L4; // [247] 106
L5: 
        ;
    }

    /** 		if cache_complete[num_var] then -- nothing to scan*/
    _2 = (int)SEQ_PTR(_44cache_complete_50556);
    _26854 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    if (_26854 == 0)
    {
        _26854 = NOVALUE;
        goto L9; // [262] 274
    }
    else{
        _26854 = NOVALUE;
    }

    /** 			return -1*/
    DeRefDS(_file_name_50978);
    DeRefDS(_env_50979);
    DeRefi(_inc_path_50981);
    DeRef(_full_path_50982);
    DeRef(_file_path_50983);
    DeRef(_strings_50984);
    DeRef(_26840);
    _26840 = NOVALUE;
    DeRef(_26853);
    _26853 = NOVALUE;
    return -1;
    goto LA; // [271] 298
L9: 

    /** 			pos = cache_delims[num_var]+1 -- scan remainder, starting from as far sa possible*/
    _2 = (int)SEQ_PTR(_44cache_delims_50557);
    _26855 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    _pos_50989 = _26855 + 1;
    _26855 = NOVALUE;
    goto LA; // [289] 298
L3: 

    /** 		pos = 1*/
    _pos_50989 = 1;
LA: 

    /** 	start_path = 0*/
    _start_path_50986 = 0;

    /** 	for p = pos to length(inc_path) do*/
    if (IS_SEQUENCE(_inc_path_50981)){
            _26857 = SEQ_PTR(_inc_path_50981)->length;
    }
    else {
        _26857 = 1;
    }
    {
        int _p_51037;
        _p_51037 = _pos_50989;
LB: 
        if (_p_51037 > _26857){
            goto LC; // [310] 716
        }

        /** 		if inc_path[p] = PATH_SEPARATOR then*/
        _2 = (int)SEQ_PTR(_inc_path_50981);
        _26858 = (int)*(((s1_ptr)_2)->base + _p_51037);
        if (_26858 != 59)
        goto LD; // [325] 665

        /** 			cache_delims[num_var] = p*/
        _2 = (int)SEQ_PTR(_44cache_delims_50557);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_delims_50557 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        *(int *)_2 = _p_51037;

        /** 			end_path = p-1*/
        _end_path_50985 = _p_51037 - 1;

        /** 			while end_path >= start_path and find(inc_path[end_path], " \t" & SLASH_CHARS) do*/
LE: 
        _26861 = (_end_path_50985 >= _start_path_50986);
        if (_26861 == 0) {
            goto LF; // [354] 388
        }
        _2 = (int)SEQ_PTR(_inc_path_50981);
        _26863 = (int)*(((s1_ptr)_2)->base + _end_path_50985);
        Concat((object_ptr)&_26865, _26864, _42SLASH_CHARS_17124);
        _26866 = find_from(_26863, _26865, 1);
        _26863 = NOVALUE;
        DeRefDS(_26865);
        _26865 = NOVALUE;
        if (_26866 == 0)
        {
            _26866 = NOVALUE;
            goto LF; // [374] 388
        }
        else{
            _26866 = NOVALUE;
        }

        /** 				end_path-=1*/
        _end_path_50985 = _end_path_50985 - 1;

        /** 			end while*/
        goto LE; // [385] 350
LF: 

        /** 			if start_path and end_path then*/
        if (_start_path_50986 == 0) {
            goto L10; // [390] 709
        }
        if (_end_path_50985 == 0)
        {
            goto L10; // [395] 709
        }
        else{
        }

        /** 				full_path = inc_path[start_path..end_path]*/
        rhs_slice_target = (object_ptr)&_full_path_50982;
        RHS_Slice(_inc_path_50981, _start_path_50986, _end_path_50985);

        /** 				cache_substrings[num_var] = append(cache_substrings[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        _26870 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        RefDS(_full_path_50982);
        Append(&_26871, _26870, _full_path_50982);
        _26870 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_substrings_50552 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26871;
        if( _1 != _26871 ){
            DeRefDS(_1);
        }
        _26871 = NOVALUE;

        /** 				cache_starts[num_var] &= start_path*/
        _2 = (int)SEQ_PTR(_44cache_starts_50553);
        _26872 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26872) && IS_ATOM(_start_path_50986)) {
            Append(&_26873, _26872, _start_path_50986);
        }
        else if (IS_ATOM(_26872) && IS_SEQUENCE(_start_path_50986)) {
        }
        else {
            Concat((object_ptr)&_26873, _26872, _start_path_50986);
            _26872 = NOVALUE;
        }
        _26872 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_starts_50553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_starts_50553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26873;
        if( _1 != _26873 ){
            DeRef(_1);
        }
        _26873 = NOVALUE;

        /** 				cache_ends[num_var] &= end_path*/
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        _26874 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26874) && IS_ATOM(_end_path_50985)) {
            Append(&_26875, _26874, _end_path_50985);
        }
        else if (IS_ATOM(_26874) && IS_SEQUENCE(_end_path_50985)) {
        }
        else {
            Concat((object_ptr)&_26875, _26874, _end_path_50985);
            _26874 = NOVALUE;
        }
        _26874 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_ends_50554 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26875;
        if( _1 != _26875 ){
            DeRef(_1);
        }
        _26875 = NOVALUE;

        /** 				file_path = full_path & file_name  */
        Concat((object_ptr)&_file_path_50983, _full_path_50982, _file_name_50978);

        /** 				try = open_locked(file_path)*/
        RefDS(_file_path_50983);
        _try_50987 = _35open_locked(_file_path_50983);
        if (!IS_ATOM_INT(_try_50987)) {
            _1 = (long)(DBL_PTR(_try_50987)->dbl);
            if (UNIQUE(DBL_PTR(_try_50987)) && (DBL_PTR(_try_50987)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_50987);
            _try_50987 = _1;
        }

        /** 				if try != -1 then -- valid path, no point trying to convert*/
        if (_try_50987 == -1)
        goto L11; // [479] 514

        /** 					ifdef WINDOWS then*/

        /** 						cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26879 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26879) && IS_ATOM(0)) {
            Append(&_26880, _26879, 0);
        }
        else if (IS_ATOM(_26879) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26880, _26879, 0);
            _26879 = NOVALUE;
        }
        _26879 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26880;
        if( _1 != _26880 ){
            DeRef(_1);
        }
        _26880 = NOVALUE;

        /** 					return {file_path,try}*/
        RefDS(_file_path_50983);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_50983;
        ((int *)_2)[2] = _try_50987;
        _26881 = MAKE_SEQ(_1);
        DeRefDS(_file_name_50978);
        DeRefDS(_env_50979);
        DeRefi(_inc_path_50981);
        DeRefDSi(_full_path_50982);
        DeRefDS(_file_path_50983);
        DeRef(_strings_50984);
        DeRef(_26840);
        _26840 = NOVALUE;
        DeRef(_26853);
        _26853 = NOVALUE;
        _26858 = NOVALUE;
        DeRef(_26861);
        _26861 = NOVALUE;
        return _26881;
L11: 

        /** 				ifdef WINDOWS then*/

        /** 					if find(1, full_path>=128) then*/
        _26882 = binary_op(GREATEREQ, _full_path_50982, 128);
        _26883 = find_from(1, _26882, 1);
        DeRefDS(_26882);
        _26882 = NOVALUE;
        if (_26883 == 0)
        {
            _26883 = NOVALUE;
            goto L12; // [527] 637
        }
        else{
            _26883 = NOVALUE;
        }

        /** 						full_path = convert_from_OEM(full_path)*/
        RefDS(_full_path_50982);
        _0 = _full_path_50982;
        _full_path_50982 = _44convert_from_OEM(_full_path_50982);
        DeRefDS(_0);

        /** 						file_path = full_path & file_name*/
        Concat((object_ptr)&_file_path_50983, _full_path_50982, _file_name_50978);

        /** 						try = open_locked(file_path)*/
        RefDS(_file_path_50983);
        _try_50987 = _35open_locked(_file_path_50983);
        if (!IS_ATOM_INT(_try_50987)) {
            _1 = (long)(DBL_PTR(_try_50987)->dbl);
            if (UNIQUE(DBL_PTR(_try_50987)) && (DBL_PTR(_try_50987)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_try_50987);
            _try_50987 = _1;
        }

        /** 						if try != -1 then -- that was it; record translation as the valid path*/
        if (_try_50987 == -1)
        goto L13; // [554] 611

        /** 							cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26888 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26888) && IS_ATOM(0)) {
            Append(&_26889, _26888, 0);
        }
        else if (IS_ATOM(_26888) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26889, _26888, 0);
            _26888 = NOVALUE;
        }
        _26888 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26889;
        if( _1 != _26889 ){
            DeRef(_1);
        }
        _26889 = NOVALUE;

        /** 							cache_substrings[num_var] = append(cache_substrings[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        _26890 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        RefDS(_full_path_50982);
        Append(&_26891, _26890, _full_path_50982);
        _26890 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_substrings_50552 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26891;
        if( _1 != _26891 ){
            DeRefDS(_1);
        }
        _26891 = NOVALUE;

        /** 							return {file_path,try}*/
        RefDS(_file_path_50983);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _file_path_50983;
        ((int *)_2)[2] = _try_50987;
        _26892 = MAKE_SEQ(_1);
        DeRefDS(_file_name_50978);
        DeRefDS(_env_50979);
        DeRefi(_inc_path_50981);
        DeRefDS(_full_path_50982);
        DeRefDS(_file_path_50983);
        DeRef(_strings_50984);
        DeRef(_26840);
        _26840 = NOVALUE;
        DeRef(_26853);
        _26853 = NOVALUE;
        _26858 = NOVALUE;
        DeRef(_26861);
        _26861 = NOVALUE;
        DeRef(_26881);
        _26881 = NOVALUE;
        return _26892;
        goto L14; // [608] 656
L13: 

        /** 							cache_converted[num_var] = append(cache_converted[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26893 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        RefDS(_full_path_50982);
        Append(&_26894, _26893, _full_path_50982);
        _26893 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26894;
        if( _1 != _26894 ){
            DeRef(_1);
        }
        _26894 = NOVALUE;
        goto L14; // [634] 656
L12: 

        /** 						cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26895 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26895) && IS_ATOM(0)) {
            Append(&_26896, _26895, 0);
        }
        else if (IS_ATOM(_26895) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26896, _26895, 0);
            _26895 = NOVALUE;
        }
        _26895 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26896;
        if( _1 != _26896 ){
            DeRef(_1);
        }
        _26896 = NOVALUE;
L14: 

        /** 				start_path = 0*/
        _start_path_50986 = 0;
        goto L10; // [662] 709
LD: 

        /** 		elsif not start_path and (inc_path[p] != ' ' and inc_path[p] != '\t') then*/
        _26897 = (_start_path_50986 == 0);
        if (_26897 == 0) {
            goto L15; // [670] 708
        }
        _2 = (int)SEQ_PTR(_inc_path_50981);
        _26899 = (int)*(((s1_ptr)_2)->base + _p_51037);
        _26900 = (_26899 != 32);
        _26899 = NOVALUE;
        if (_26900 == 0) {
            DeRef(_26901);
            _26901 = 0;
            goto L16; // [682] 698
        }
        _2 = (int)SEQ_PTR(_inc_path_50981);
        _26902 = (int)*(((s1_ptr)_2)->base + _p_51037);
        _26903 = (_26902 != 9);
        _26902 = NOVALUE;
        _26901 = (_26903 != 0);
L16: 
        if (_26901 == 0)
        {
            _26901 = NOVALUE;
            goto L15; // [699] 708
        }
        else{
            _26901 = NOVALUE;
        }

        /** 			start_path = p*/
        _start_path_50986 = _p_51037;
L15: 
L10: 

        /** 	end for*/
        _p_51037 = _p_51037 + 1;
        goto LB; // [711] 317
LC: 
        ;
    }

    /** 	cache_complete[num_var] = 1*/
    _2 = (int)SEQ_PTR(_44cache_complete_50556);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _44cache_complete_50556 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
    *(int *)_2 = 1;

    /** 	return -1*/
    DeRefDS(_file_name_50978);
    DeRefDS(_env_50979);
    DeRefi(_inc_path_50981);
    DeRef(_full_path_50982);
    DeRef(_file_path_50983);
    DeRef(_strings_50984);
    DeRef(_26840);
    _26840 = NOVALUE;
    DeRef(_26853);
    _26853 = NOVALUE;
    _26858 = NOVALUE;
    DeRef(_26861);
    _26861 = NOVALUE;
    DeRef(_26881);
    _26881 = NOVALUE;
    DeRef(_26892);
    _26892 = NOVALUE;
    DeRef(_26897);
    _26897 = NOVALUE;
    DeRef(_26900);
    _26900 = NOVALUE;
    DeRef(_26903);
    _26903 = NOVALUE;
    return -1;
    ;
}


int _44Include_paths(int _add_converted_51101)
{
    int _status_51102 = NOVALUE;
    int _pos_51103 = NOVALUE;
    int _inc_path_51104 = NOVALUE;
    int _full_path_51105 = NOVALUE;
    int _start_path_51106 = NOVALUE;
    int _end_path_51107 = NOVALUE;
    int _eudir_path_51123 = NOVALUE;
    int _26964 = NOVALUE;
    int _26963 = NOVALUE;
    int _26962 = NOVALUE;
    int _26961 = NOVALUE;
    int _26960 = NOVALUE;
    int _26959 = NOVALUE;
    int _26958 = NOVALUE;
    int _26956 = NOVALUE;
    int _26955 = NOVALUE;
    int _26954 = NOVALUE;
    int _26953 = NOVALUE;
    int _26952 = NOVALUE;
    int _26951 = NOVALUE;
    int _26950 = NOVALUE;
    int _26949 = NOVALUE;
    int _26948 = NOVALUE;
    int _26947 = NOVALUE;
    int _26946 = NOVALUE;
    int _26945 = NOVALUE;
    int _26944 = NOVALUE;
    int _26943 = NOVALUE;
    int _26942 = NOVALUE;
    int _26941 = NOVALUE;
    int _26940 = NOVALUE;
    int _26939 = NOVALUE;
    int _26938 = NOVALUE;
    int _26937 = NOVALUE;
    int _26936 = NOVALUE;
    int _26934 = NOVALUE;
    int _26932 = NOVALUE;
    int _26931 = NOVALUE;
    int _26930 = NOVALUE;
    int _26929 = NOVALUE;
    int _26928 = NOVALUE;
    int _26925 = NOVALUE;
    int _26924 = NOVALUE;
    int _26922 = NOVALUE;
    int _26920 = NOVALUE;
    int _26918 = NOVALUE;
    int _26917 = NOVALUE;
    int _26915 = NOVALUE;
    int _26912 = NOVALUE;
    int _26910 = NOVALUE;
    int _26905 = NOVALUE;
    int _26904 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_add_converted_51101)) {
        _1 = (long)(DBL_PTR(_add_converted_51101)->dbl);
        if (UNIQUE(DBL_PTR(_add_converted_51101)) && (DBL_PTR(_add_converted_51101)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_add_converted_51101);
        _add_converted_51101 = _1;
    }

    /** 	if length(include_Paths) then*/
    if (IS_SEQUENCE(_44include_Paths_51098)){
            _26904 = SEQ_PTR(_44include_Paths_51098)->length;
    }
    else {
        _26904 = 1;
    }
    if (_26904 == 0)
    {
        _26904 = NOVALUE;
        goto L1; // [10] 22
    }
    else{
        _26904 = NOVALUE;
    }

    /** 		return include_Paths*/
    RefDS(_44include_Paths_51098);
    DeRefi(_inc_path_51104);
    DeRefi(_full_path_51105);
    DeRef(_eudir_path_51123);
    return _44include_Paths_51098;
L1: 

    /** 	include_Paths = append(config_inc_paths, current_dir())*/
    _26905 = _13current_dir();
    Ref(_26905);
    Append(&_44include_Paths_51098, _44config_inc_paths_50558, _26905);
    DeRef(_26905);
    _26905 = NOVALUE;

    /** 	num_var = find("EUINC", cache_vars)*/
    _44num_var_50549 = find_from(_26907, _44cache_vars_50550, 1);

    /** 	inc_path = getenv("EUINC")*/
    DeRefi(_inc_path_51104);
    _inc_path_51104 = EGetEnv(_26907);

    /** 	if atom(inc_path) then*/
    _26910 = IS_ATOM(_inc_path_51104);
    if (_26910 == 0)
    {
        _26910 = NOVALUE;
        goto L2; // [52] 61
    }
    else{
        _26910 = NOVALUE;
    }

    /** 		inc_path = ""*/
    RefDS(_22663);
    DeRefi(_inc_path_51104);
    _inc_path_51104 = _22663;
L2: 

    /** 	status = check_cache("EUINC", inc_path)*/
    RefDS(_26907);
    Ref(_inc_path_51104);
    _status_51102 = _44check_cache(_26907, _inc_path_51104);
    if (!IS_ATOM_INT(_status_51102)) {
        _1 = (long)(DBL_PTR(_status_51102)->dbl);
        if (UNIQUE(DBL_PTR(_status_51102)) && (DBL_PTR(_status_51102)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_status_51102);
        _status_51102 = _1;
    }

    /** 	if length(inc_path) then*/
    if (IS_SEQUENCE(_inc_path_51104)){
            _26912 = SEQ_PTR(_inc_path_51104)->length;
    }
    else {
        _26912 = 1;
    }
    if (_26912 == 0)
    {
        _26912 = NOVALUE;
        goto L3; // [75] 87
    }
    else{
        _26912 = NOVALUE;
    }

    /** 		inc_path = append(inc_path, PATH_SEPARATOR)*/
    Append(&_inc_path_51104, _inc_path_51104, 59);
L3: 

    /** 	object eudir_path = get_eudir()*/
    _0 = _eudir_path_51123;
    _eudir_path_51123 = _35get_eudir();
    DeRef(_0);

    /** 	if sequence(eudir_path) then*/
    _26915 = IS_SEQUENCE(_eudir_path_51123);
    if (_26915 == 0)
    {
        _26915 = NOVALUE;
        goto L4; // [97] 117
    }
    else{
        _26915 = NOVALUE;
    }

    /** 		include_Paths = append(include_Paths, sprintf("%s/include", { eudir_path }))*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_eudir_path_51123);
    *((int *)(_2+4)) = _eudir_path_51123;
    _26917 = MAKE_SEQ(_1);
    _26918 = EPrintf(-9999999, _26916, _26917);
    DeRefDS(_26917);
    _26917 = NOVALUE;
    RefDS(_26918);
    Append(&_44include_Paths_51098, _44include_Paths_51098, _26918);
    DeRefDS(_26918);
    _26918 = NOVALUE;
L4: 

    /** 	if status then*/
    if (_status_51102 == 0)
    {
        goto L5; // [119] 161
    }
    else{
    }

    /** 		if cache_complete[num_var] then*/
    _2 = (int)SEQ_PTR(_44cache_complete_50556);
    _26920 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    if (_26920 == 0)
    {
        _26920 = NOVALUE;
        goto L6; // [132] 144
    }
    else{
        _26920 = NOVALUE;
    }

    /** 			goto "cache done"*/
    goto G7;
L6: 

    /** 		pos = cache_delims[num_var]+1*/
    _2 = (int)SEQ_PTR(_44cache_delims_50557);
    _26922 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    _pos_51103 = _26922 + 1;
    _26922 = NOVALUE;
    goto L8; // [158] 167
L5: 

    /**         pos = 1*/
    _pos_51103 = 1;
L8: 

    /** 	start_path = 0*/
    _start_path_51106 = 0;

    /** 	for p = pos to length(inc_path) do*/
    if (IS_SEQUENCE(_inc_path_51104)){
            _26924 = SEQ_PTR(_inc_path_51104)->length;
    }
    else {
        _26924 = 1;
    }
    {
        int _p_51140;
        _p_51140 = _pos_51103;
L9: 
        if (_p_51140 > _26924){
            goto LA; // [179] 456
        }

        /** 		if inc_path[p] = PATH_SEPARATOR then*/
        _2 = (int)SEQ_PTR(_inc_path_51104);
        _26925 = (int)*(((s1_ptr)_2)->base + _p_51140);
        if (_26925 != 59)
        goto LB; // [194] 405

        /** 			cache_delims[num_var] = p*/
        _2 = (int)SEQ_PTR(_44cache_delims_50557);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_delims_50557 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        *(int *)_2 = _p_51140;

        /** 			end_path = p-1*/
        _end_path_51107 = _p_51140 - 1;

        /** 			while end_path >= start_path and find(inc_path[end_path]," \t" & SLASH_CHARS) do*/
LC: 
        _26928 = (_end_path_51107 >= _start_path_51106);
        if (_26928 == 0) {
            goto LD; // [223] 257
        }
        _2 = (int)SEQ_PTR(_inc_path_51104);
        _26930 = (int)*(((s1_ptr)_2)->base + _end_path_51107);
        Concat((object_ptr)&_26931, _26864, _42SLASH_CHARS_17124);
        _26932 = find_from(_26930, _26931, 1);
        _26930 = NOVALUE;
        DeRefDS(_26931);
        _26931 = NOVALUE;
        if (_26932 == 0)
        {
            _26932 = NOVALUE;
            goto LD; // [243] 257
        }
        else{
            _26932 = NOVALUE;
        }

        /** 				end_path -= 1*/
        _end_path_51107 = _end_path_51107 - 1;

        /** 			end while*/
        goto LC; // [254] 219
LD: 

        /** 			if start_path and end_path then*/
        if (_start_path_51106 == 0) {
            goto LE; // [259] 449
        }
        if (_end_path_51107 == 0)
        {
            goto LE; // [264] 449
        }
        else{
        }

        /** 				full_path = inc_path[start_path..end_path]*/
        rhs_slice_target = (object_ptr)&_full_path_51105;
        RHS_Slice(_inc_path_51104, _start_path_51106, _end_path_51107);

        /** 				cache_substrings[num_var] = append(cache_substrings[num_var],full_path)*/
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        _26936 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        RefDS(_full_path_51105);
        Append(&_26937, _26936, _full_path_51105);
        _26936 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_substrings_50552);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_substrings_50552 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26937;
        if( _1 != _26937 ){
            DeRefDS(_1);
        }
        _26937 = NOVALUE;

        /** 				cache_starts[num_var] &= start_path*/
        _2 = (int)SEQ_PTR(_44cache_starts_50553);
        _26938 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26938) && IS_ATOM(_start_path_51106)) {
            Append(&_26939, _26938, _start_path_51106);
        }
        else if (IS_ATOM(_26938) && IS_SEQUENCE(_start_path_51106)) {
        }
        else {
            Concat((object_ptr)&_26939, _26938, _start_path_51106);
            _26938 = NOVALUE;
        }
        _26938 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_starts_50553);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_starts_50553 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26939;
        if( _1 != _26939 ){
            DeRef(_1);
        }
        _26939 = NOVALUE;

        /** 				cache_ends[num_var] &= end_path*/
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        _26940 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26940) && IS_ATOM(_end_path_51107)) {
            Append(&_26941, _26940, _end_path_51107);
        }
        else if (IS_ATOM(_26940) && IS_SEQUENCE(_end_path_51107)) {
        }
        else {
            Concat((object_ptr)&_26941, _26940, _end_path_51107);
            _26940 = NOVALUE;
        }
        _26940 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_ends_50554);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_ends_50554 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26941;
        if( _1 != _26941 ){
            DeRef(_1);
        }
        _26941 = NOVALUE;

        /** 				ifdef WINDOWS then*/

        /** 					if find(1, full_path>=128) then*/
        _26942 = binary_op(GREATEREQ, _full_path_51105, 128);
        _26943 = find_from(1, _26942, 1);
        DeRefDS(_26942);
        _26942 = NOVALUE;
        if (_26943 == 0)
        {
            _26943 = NOVALUE;
            goto LF; // [345] 377
        }
        else{
            _26943 = NOVALUE;
        }

        /** 						cache_converted[num_var] = append(cache_converted[num_var], */
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26944 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        RefDS(_full_path_51105);
        _26945 = _44convert_from_OEM(_full_path_51105);
        Ref(_26945);
        Append(&_26946, _26944, _26945);
        _26944 = NOVALUE;
        DeRef(_26945);
        _26945 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26946;
        if( _1 != _26946 ){
            DeRef(_1);
        }
        _26946 = NOVALUE;
        goto L10; // [374] 396
LF: 

        /** 						cache_converted[num_var] &= 0*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26947 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        if (IS_SEQUENCE(_26947) && IS_ATOM(0)) {
            Append(&_26948, _26947, 0);
        }
        else if (IS_ATOM(_26947) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_26948, _26947, 0);
            _26947 = NOVALUE;
        }
        _26947 = NOVALUE;
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _44cache_converted_50555 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
        _1 = *(int *)_2;
        *(int *)_2 = _26948;
        if( _1 != _26948 ){
            DeRef(_1);
        }
        _26948 = NOVALUE;
L10: 

        /** 				start_path = 0*/
        _start_path_51106 = 0;
        goto LE; // [402] 449
LB: 

        /** 		elsif not start_path and (inc_path[p] != ' ' and inc_path[p] != '\t') then*/
        _26949 = (_start_path_51106 == 0);
        if (_26949 == 0) {
            goto L11; // [410] 448
        }
        _2 = (int)SEQ_PTR(_inc_path_51104);
        _26951 = (int)*(((s1_ptr)_2)->base + _p_51140);
        _26952 = (_26951 != 32);
        _26951 = NOVALUE;
        if (_26952 == 0) {
            DeRef(_26953);
            _26953 = 0;
            goto L12; // [422] 438
        }
        _2 = (int)SEQ_PTR(_inc_path_51104);
        _26954 = (int)*(((s1_ptr)_2)->base + _p_51140);
        _26955 = (_26954 != 9);
        _26954 = NOVALUE;
        _26953 = (_26955 != 0);
L12: 
        if (_26953 == 0)
        {
            _26953 = NOVALUE;
            goto L11; // [439] 448
        }
        else{
            _26953 = NOVALUE;
        }

        /** 			start_path = p*/
        _start_path_51106 = _p_51140;
L11: 
LE: 

        /** 	end for*/
        _p_51140 = _p_51140 + 1;
        goto L9; // [451] 186
LA: 
        ;
    }

    /** label "cache done"*/
G7:

    /** 	include_Paths &= cache_substrings[num_var]*/
    _2 = (int)SEQ_PTR(_44cache_substrings_50552);
    _26956 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    Concat((object_ptr)&_44include_Paths_51098, _44include_Paths_51098, _26956);
    _26956 = NOVALUE;

    /** 	cache_complete[num_var] = 1*/
    _2 = (int)SEQ_PTR(_44cache_complete_50556);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _44cache_complete_50556 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _44num_var_50549);
    *(int *)_2 = 1;

    /** 	ifdef WINDOWS then*/

    /** 		if add_converted then*/
    if (_add_converted_51101 == 0)
    {
        goto L13; // [490] 562
    }
    else{
    }

    /** 	    	for i=1 to length(cache_converted[num_var]) do*/
    _2 = (int)SEQ_PTR(_44cache_converted_50555);
    _26958 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
    if (IS_SEQUENCE(_26958)){
            _26959 = SEQ_PTR(_26958)->length;
    }
    else {
        _26959 = 1;
    }
    _26958 = NOVALUE;
    {
        int _i_51185;
        _i_51185 = 1;
L14: 
        if (_i_51185 > _26959){
            goto L15; // [506] 561
        }

        /** 	        	if sequence(cache_converted[num_var][i]) then*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26960 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        _2 = (int)SEQ_PTR(_26960);
        _26961 = (int)*(((s1_ptr)_2)->base + _i_51185);
        _26960 = NOVALUE;
        _26962 = IS_SEQUENCE(_26961);
        _26961 = NOVALUE;
        if (_26962 == 0)
        {
            _26962 = NOVALUE;
            goto L16; // [530] 554
        }
        else{
            _26962 = NOVALUE;
        }

        /** 		        	include_Paths = append(include_Paths, cache_converted[num_var][i])*/
        _2 = (int)SEQ_PTR(_44cache_converted_50555);
        _26963 = (int)*(((s1_ptr)_2)->base + _44num_var_50549);
        _2 = (int)SEQ_PTR(_26963);
        _26964 = (int)*(((s1_ptr)_2)->base + _i_51185);
        _26963 = NOVALUE;
        Ref(_26964);
        Append(&_44include_Paths_51098, _44include_Paths_51098, _26964);
        _26964 = NOVALUE;
L16: 

        /** 			end for*/
        _i_51185 = _i_51185 + 1;
        goto L14; // [556] 513
L15: 
        ;
    }
L13: 

    /** 	return include_Paths*/
    RefDS(_44include_Paths_51098);
    DeRefi(_inc_path_51104);
    DeRefi(_full_path_51105);
    DeRef(_eudir_path_51123);
    _26925 = NOVALUE;
    DeRef(_26928);
    _26928 = NOVALUE;
    DeRef(_26949);
    _26949 = NOVALUE;
    _26958 = NOVALUE;
    DeRef(_26952);
    _26952 = NOVALUE;
    DeRef(_26955);
    _26955 = NOVALUE;
    return _44include_Paths_51098;
    ;
}


int _44e_path_find(int _name_51197)
{
    int _scan_result_51198 = NOVALUE;
    int _26974 = NOVALUE;
    int _26973 = NOVALUE;
    int _26972 = NOVALUE;
    int _26969 = NOVALUE;
    int _26968 = NOVALUE;
    int _26967 = NOVALUE;
    int _26966 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if file_exists(name) then*/
    RefDS(_name_51197);
    _26966 = _13file_exists(_name_51197);
    if (_26966 == 0) {
        DeRef(_26966);
        _26966 = NOVALUE;
        goto L1; // [9] 19
    }
    else {
        if (!IS_ATOM_INT(_26966) && DBL_PTR(_26966)->dbl == 0.0){
            DeRef(_26966);
            _26966 = NOVALUE;
            goto L1; // [9] 19
        }
        DeRef(_26966);
        _26966 = NOVALUE;
    }
    DeRef(_26966);
    _26966 = NOVALUE;

    /** 		return name*/
    DeRef(_scan_result_51198);
    return _name_51197;
L1: 

    /** 	for i = 1 to length(SLASH_CHARS) do*/
    if (IS_SEQUENCE(_42SLASH_CHARS_17124)){
            _26967 = SEQ_PTR(_42SLASH_CHARS_17124)->length;
    }
    else {
        _26967 = 1;
    }
    {
        int _i_51203;
        _i_51203 = 1;
L2: 
        if (_i_51203 > _26967){
            goto L3; // [26] 63
        }

        /** 		if find(SLASH_CHARS[i], name) then*/
        _2 = (int)SEQ_PTR(_42SLASH_CHARS_17124);
        _26968 = (int)*(((s1_ptr)_2)->base + _i_51203);
        _26969 = find_from(_26968, _name_51197, 1);
        _26968 = NOVALUE;
        if (_26969 == 0)
        {
            _26969 = NOVALUE;
            goto L4; // [46] 56
        }
        else{
            _26969 = NOVALUE;
        }

        /** 			return -1*/
        DeRefDS(_name_51197);
        DeRef(_scan_result_51198);
        return -1;
L4: 

        /** 	end for*/
        _i_51203 = _i_51203 + 1;
        goto L2; // [58] 33
L3: 
        ;
    }

    /** 	scan_result = ScanPath(name, "PATH", 0)*/
    RefDS(_name_51197);
    RefDS(_26970);
    _0 = _scan_result_51198;
    _scan_result_51198 = _44ScanPath(_name_51197, _26970, 0);
    DeRef(_0);

    /** 	if sequence(scan_result) then*/
    _26972 = IS_SEQUENCE(_scan_result_51198);
    if (_26972 == 0)
    {
        _26972 = NOVALUE;
        goto L5; // [76] 98
    }
    else{
        _26972 = NOVALUE;
    }

    /** 		close(scan_result[2])*/
    _2 = (int)SEQ_PTR(_scan_result_51198);
    _26973 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_26973))
    EClose(_26973);
    else
    EClose((int)DBL_PTR(_26973)->dbl);
    _26973 = NOVALUE;

    /** 		return scan_result[1]*/
    _2 = (int)SEQ_PTR(_scan_result_51198);
    _26974 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_26974);
    DeRefDS(_name_51197);
    DeRef(_scan_result_51198);
    return _26974;
L5: 

    /** 	return -1*/
    DeRefDS(_name_51197);
    DeRef(_scan_result_51198);
    _26974 = NOVALUE;
    return -1;
    ;
}



// 0xECF2A735
