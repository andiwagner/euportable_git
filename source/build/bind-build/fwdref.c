// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _40clear_fwd_refs()
{
    int _0, _1, _2;
    

    /** 	fwdref_count = 0*/
    _40fwdref_count_62534 = 0;

    /** end procedure*/
    return;
    ;
}


int _40get_fwdref_count()
{
    int _0, _1, _2;
    

    /** 	return fwdref_count*/
    return _40fwdref_count_62534;
    ;
}


void _40set_glabel_block(int _ref_62541, int _block_62543)
{
    int _31512 = NOVALUE;
    int _31511 = NOVALUE;
    int _31509 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62541)) {
        _1 = (long)(DBL_PTR(_ref_62541)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62541)) && (DBL_PTR(_ref_62541)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62541);
        _ref_62541 = _1;
    }
    if (!IS_ATOM_INT(_block_62543)) {
        _1 = (long)(DBL_PTR(_block_62543)->dbl);
        if (UNIQUE(DBL_PTR(_block_62543)) && (DBL_PTR(_block_62543)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_block_62543);
        _block_62543 = _1;
    }

    /** 	forward_references[ref][FR_DATA] &= block*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62541 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _31511 = (int)*(((s1_ptr)_2)->base + 12);
    _31509 = NOVALUE;
    if (IS_SEQUENCE(_31511) && IS_ATOM(_block_62543)) {
        Append(&_31512, _31511, _block_62543);
    }
    else if (IS_ATOM(_31511) && IS_SEQUENCE(_block_62543)) {
    }
    else {
        Concat((object_ptr)&_31512, _31511, _block_62543);
        _31511 = NOVALUE;
    }
    _31511 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _31512;
    if( _1 != _31512 ){
        DeRef(_1);
    }
    _31512 = NOVALUE;
    _31509 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _40replace_code(int _code_62555, int _start_62556, int _finish_62557, int _subprog_62558)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_finish_62557)) {
        _1 = (long)(DBL_PTR(_finish_62557)->dbl);
        if (UNIQUE(DBL_PTR(_finish_62557)) && (DBL_PTR(_finish_62557)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_finish_62557);
        _finish_62557 = _1;
    }
    if (!IS_ATOM_INT(_subprog_62558)) {
        _1 = (long)(DBL_PTR(_subprog_62558)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_62558)) && (DBL_PTR(_subprog_62558)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_62558);
        _subprog_62558 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_62558;

    /** 	shift:replace_code( code, start, finish )*/
    RefDS(_code_62555);
    _67replace_code(_code_62555, _start_62556, _finish_62557);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    DeRefDS(_code_62555);
    return;
    ;
}


void _40resolved_reference(int _ref_62561)
{
    int _file_62562 = NOVALUE;
    int _subprog_62565 = NOVALUE;
    int _tx_62568 = NOVALUE;
    int _ax_62569 = NOVALUE;
    int _sp_62570 = NOVALUE;
    int _r_62585 = NOVALUE;
    int _r_62603 = NOVALUE;
    int _31536 = NOVALUE;
    int _31535 = NOVALUE;
    int _31534 = NOVALUE;
    int _31532 = NOVALUE;
    int _31529 = NOVALUE;
    int _31527 = NOVALUE;
    int _31525 = NOVALUE;
    int _31524 = NOVALUE;
    int _31522 = NOVALUE;
    int _31520 = NOVALUE;
    int _31518 = NOVALUE;
    int _31517 = NOVALUE;
    int _31515 = NOVALUE;
    int _31513 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 		file    = forward_references[ref][FR_FILE],*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31513 = (int)*(((s1_ptr)_2)->base + _ref_62561);
    _2 = (int)SEQ_PTR(_31513);
    _file_62562 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_file_62562)){
        _file_62562 = (long)DBL_PTR(_file_62562)->dbl;
    }
    _31513 = NOVALUE;

    /** 		subprog = forward_references[ref][FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31515 = (int)*(((s1_ptr)_2)->base + _ref_62561);
    _2 = (int)SEQ_PTR(_31515);
    _subprog_62565 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_subprog_62565)){
        _subprog_62565 = (long)DBL_PTR(_subprog_62565)->dbl;
    }
    _31515 = NOVALUE;

    /** 		tx = 0,*/
    _tx_62568 = 0;

    /** 		ax = 0,*/
    _ax_62569 = 0;

    /** 		sp = 0*/
    _sp_62570 = 0;

    /** 	if forward_references[ref][FR_SUBPROG] = TopLevelSub then*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31517 = (int)*(((s1_ptr)_2)->base + _ref_62561);
    _2 = (int)SEQ_PTR(_31517);
    _31518 = (int)*(((s1_ptr)_2)->base + 4);
    _31517 = NOVALUE;
    if (binary_op_a(NOTEQ, _31518, _38TopLevelSub_16953)){
        _31518 = NOVALUE;
        goto L1; // [66] 86
    }
    _31518 = NOVALUE;

    /** 		tx = find( ref, toplevel_references[file] )*/
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    _31520 = (int)*(((s1_ptr)_2)->base + _file_62562);
    _tx_62568 = find_from(_ref_62561, _31520, 1);
    _31520 = NOVALUE;
    goto L2; // [83] 117
L1: 

    /** 		sp = find( subprog, active_subprogs[file] )*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _31522 = (int)*(((s1_ptr)_2)->base + _file_62562);
    _sp_62570 = find_from(_subprog_62565, _31522, 1);
    _31522 = NOVALUE;

    /** 		ax = find( ref, active_references[file][sp] )*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    _31524 = (int)*(((s1_ptr)_2)->base + _file_62562);
    _2 = (int)SEQ_PTR(_31524);
    _31525 = (int)*(((s1_ptr)_2)->base + _sp_62570);
    _31524 = NOVALUE;
    _ax_62569 = find_from(_ref_62561, _31525, 1);
    _31525 = NOVALUE;
L2: 

    /** 	if ax then*/
    if (_ax_62569 == 0)
    {
        goto L3; // [119] 259
    }
    else{
    }

    /** 		sequence r = active_references[file][sp] */
    _2 = (int)SEQ_PTR(_40active_references_62505);
    _31527 = (int)*(((s1_ptr)_2)->base + _file_62562);
    DeRef(_r_62585);
    _2 = (int)SEQ_PTR(_31527);
    _r_62585 = (int)*(((s1_ptr)_2)->base + _sp_62570);
    Ref(_r_62585);
    _31527 = NOVALUE;

    /** 		active_references[file][sp] = 0*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_references_62505 = MAKE_SEQ(_2);
    }
    _3 = (int)(_file_62562 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _sp_62570);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31529 = NOVALUE;

    /** 		r = remove( r, ax )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62585);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ax_62569)) ? _ax_62569 : (long)(DBL_PTR(_ax_62569)->dbl);
        int stop = (IS_ATOM_INT(_ax_62569)) ? _ax_62569 : (long)(DBL_PTR(_ax_62569)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62585), start, &_r_62585 );
            }
            else Tail(SEQ_PTR(_r_62585), stop+1, &_r_62585);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62585), start, &_r_62585);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62585 = Remove_elements(start, stop, (SEQ_PTR(_r_62585)->ref == 1));
        }
    }

    /** 		active_references[file][sp] = r*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_references_62505 = MAKE_SEQ(_2);
    }
    _3 = (int)(_file_62562 + ((s1_ptr)_2)->base);
    RefDS(_r_62585);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _sp_62570);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62585;
    DeRef(_1);
    _31532 = NOVALUE;

    /** 		if not length( active_references[file][sp] ) then*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    _31534 = (int)*(((s1_ptr)_2)->base + _file_62562);
    _2 = (int)SEQ_PTR(_31534);
    _31535 = (int)*(((s1_ptr)_2)->base + _sp_62570);
    _31534 = NOVALUE;
    if (IS_SEQUENCE(_31535)){
            _31536 = SEQ_PTR(_31535)->length;
    }
    else {
        _31536 = 1;
    }
    _31535 = NOVALUE;
    if (_31536 != 0)
    goto L4; // [184] 254
    _31536 = NOVALUE;

    /** 			r = active_references[file]*/
    DeRefDS(_r_62585);
    _2 = (int)SEQ_PTR(_40active_references_62505);
    _r_62585 = (int)*(((s1_ptr)_2)->base + _file_62562);
    Ref(_r_62585);

    /** 			active_references[file] = 0*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_references_62505 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62562);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			r = remove( r, sp )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62585);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_sp_62570)) ? _sp_62570 : (long)(DBL_PTR(_sp_62570)->dbl);
        int stop = (IS_ATOM_INT(_sp_62570)) ? _sp_62570 : (long)(DBL_PTR(_sp_62570)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62585), start, &_r_62585 );
            }
            else Tail(SEQ_PTR(_r_62585), stop+1, &_r_62585);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62585), start, &_r_62585);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62585 = Remove_elements(start, stop, (SEQ_PTR(_r_62585)->ref == 1));
        }
    }

    /** 			active_references[file] = r*/
    RefDS(_r_62585);
    _2 = (int)SEQ_PTR(_40active_references_62505);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_references_62505 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62562);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62585;
    DeRef(_1);

    /** 			r = active_subprogs[file]*/
    DeRefDS(_r_62585);
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _r_62585 = (int)*(((s1_ptr)_2)->base + _file_62562);
    Ref(_r_62585);

    /** 			active_subprogs[file] = 0*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_subprogs_62504 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62562);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			r = remove( r,   sp )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62585);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_sp_62570)) ? _sp_62570 : (long)(DBL_PTR(_sp_62570)->dbl);
        int stop = (IS_ATOM_INT(_sp_62570)) ? _sp_62570 : (long)(DBL_PTR(_sp_62570)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62585), start, &_r_62585 );
            }
            else Tail(SEQ_PTR(_r_62585), stop+1, &_r_62585);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62585), start, &_r_62585);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62585 = Remove_elements(start, stop, (SEQ_PTR(_r_62585)->ref == 1));
        }
    }

    /** 			active_subprogs[file] = r*/
    RefDS(_r_62585);
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_subprogs_62504 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62562);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62585;
    DeRef(_1);
L4: 
    DeRef(_r_62585);
    _r_62585 = NOVALUE;
    goto L5; // [256] 309
L3: 

    /** 	elsif tx then*/
    if (_tx_62568 == 0)
    {
        goto L6; // [261] 302
    }
    else{
    }

    /** 		sequence r = toplevel_references[file]*/
    DeRef(_r_62603);
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    _r_62603 = (int)*(((s1_ptr)_2)->base + _file_62562);
    Ref(_r_62603);

    /** 		toplevel_references[file] = 0*/
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40toplevel_references_62506 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62562);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		r = remove( r, tx )*/
    {
        s1_ptr assign_space = SEQ_PTR(_r_62603);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_tx_62568)) ? _tx_62568 : (long)(DBL_PTR(_tx_62568)->dbl);
        int stop = (IS_ATOM_INT(_tx_62568)) ? _tx_62568 : (long)(DBL_PTR(_tx_62568)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_r_62603), start, &_r_62603 );
            }
            else Tail(SEQ_PTR(_r_62603), stop+1, &_r_62603);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_r_62603), start, &_r_62603);
        }
        else {
            assign_slice_seq = &assign_space;
            _r_62603 = Remove_elements(start, stop, (SEQ_PTR(_r_62603)->ref == 1));
        }
    }

    /** 		toplevel_references[file] = r*/
    RefDS(_r_62603);
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40toplevel_references_62506 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_62562);
    _1 = *(int *)_2;
    *(int *)_2 = _r_62603;
    DeRef(_1);
    DeRefDS(_r_62603);
    _r_62603 = NOVALUE;
    goto L5; // [299] 309
L6: 

    /** 		InternalErr( 260 )*/
    RefDS(_22663);
    _46InternalErr(260, _22663);
L5: 

    /** 	inactive_references &= ref*/
    Append(&_40inactive_references_62507, _40inactive_references_62507, _ref_62561);

    /** 	forward_references[ref] = 0*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ref_62561);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** end procedure*/
    _31535 = NOVALUE;
    return;
    ;
}


void _40set_code(int _ref_62617)
{
    int _31552 = NOVALUE;
    int _31550 = NOVALUE;
    int _31548 = NOVALUE;
    int _31545 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	patch_code_sub = forward_references[ref][FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31545 = (int)*(((s1_ptr)_2)->base + _ref_62617);
    _2 = (int)SEQ_PTR(_31545);
    _40patch_code_sub_62612 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_40patch_code_sub_62612)){
        _40patch_code_sub_62612 = (long)DBL_PTR(_40patch_code_sub_62612)->dbl;
    }
    _31545 = NOVALUE;

    /** 	if patch_code_sub != CurrentSub then*/
    if (_40patch_code_sub_62612 == _38CurrentSub_16954)
    goto L1; // [25] 121

    /** 		patch_code_temp = Code*/
    RefDS(_38Code_17038);
    DeRef(_40patch_code_temp_62609);
    _40patch_code_temp_62609 = _38Code_17038;

    /** 		patch_linetab_temp = LineTable*/
    RefDS(_38LineTable_17039);
    DeRef(_40patch_linetab_temp_62610);
    _40patch_linetab_temp_62610 = _38LineTable_17039;

    /** 		Code = SymTab[patch_code_sub][S_CODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31548 = (int)*(((s1_ptr)_2)->base + _40patch_code_sub_62612);
    DeRefDS(_38Code_17038);
    _2 = (int)SEQ_PTR(_31548);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _38Code_17038 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _38Code_17038 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    Ref(_38Code_17038);
    _31548 = NOVALUE;

    /** 		SymTab[patch_code_sub][S_CODE] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_40patch_code_sub_62612 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _31550 = NOVALUE;

    /** 		LineTable = SymTab[patch_code_sub][S_LINETAB]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31552 = (int)*(((s1_ptr)_2)->base + _40patch_code_sub_62612);
    DeRefDS(_38LineTable_17039);
    _2 = (int)SEQ_PTR(_31552);
    if (!IS_ATOM_INT(_38S_LINETAB_16633)){
        _38LineTable_17039 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    }
    else{
        _38LineTable_17039 = (int)*(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    }
    Ref(_38LineTable_17039);
    _31552 = NOVALUE;

    /** 		patch_current_sub = CurrentSub*/
    _40patch_current_sub_62614 = _38CurrentSub_16954;

    /** 		CurrentSub = patch_code_sub*/
    _38CurrentSub_16954 = _40patch_code_sub_62612;
    goto L2; // [118] 131
L1: 

    /** 		patch_current_sub = patch_code_sub*/
    _40patch_current_sub_62614 = _40patch_code_sub_62612;
L2: 

    /** end procedure*/
    return;
    ;
}


void _40reset_code()
{
    int _31556 = NOVALUE;
    int _31554 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	SymTab[patch_code_sub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_40patch_code_sub_62612 + ((s1_ptr)_2)->base);
    RefDS(_38Code_17038);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _38Code_17038;
    DeRef(_1);
    _31554 = NOVALUE;

    /** 	SymTab[patch_code_sub][S_LINETAB] = LineTable*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_40patch_code_sub_62612 + ((s1_ptr)_2)->base);
    RefDS(_38LineTable_17039);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_LINETAB_16633))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_LINETAB_16633);
    _1 = *(int *)_2;
    *(int *)_2 = _38LineTable_17039;
    DeRef(_1);
    _31556 = NOVALUE;

    /** 	if patch_code_sub != patch_current_sub then*/
    if (_40patch_code_sub_62612 == _40patch_current_sub_62614)
    goto L1; // [45] 77

    /** 		CurrentSub = patch_current_sub*/
    _38CurrentSub_16954 = _40patch_current_sub_62614;

    /** 		Code = patch_code_temp*/
    RefDS(_40patch_code_temp_62609);
    DeRefDS(_38Code_17038);
    _38Code_17038 = _40patch_code_temp_62609;

    /** 		LineTable = patch_linetab_temp*/
    RefDS(_40patch_linetab_temp_62610);
    DeRefDS(_38LineTable_17039);
    _38LineTable_17039 = _40patch_linetab_temp_62610;
L1: 

    /** 	patch_code_temp = {}*/
    RefDS(_22663);
    DeRef(_40patch_code_temp_62609);
    _40patch_code_temp_62609 = _22663;

    /** 	patch_linetab_temp = {}*/
    RefDS(_22663);
    DeRef(_40patch_linetab_temp_62610);
    _40patch_linetab_temp_62610 = _22663;

    /** end procedure*/
    return;
    ;
}


void _40set_data(int _ref_62661, int _data_62662)
{
    int _31559 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62661)) {
        _1 = (long)(DBL_PTR(_ref_62661)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62661)) && (DBL_PTR(_ref_62661)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62661);
        _ref_62661 = _1;
    }

    /** 	forward_references[ref][FR_DATA] = data*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62661 + ((s1_ptr)_2)->base);
    Ref(_data_62662);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _data_62662;
    DeRef(_1);
    _31559 = NOVALUE;

    /** end procedure*/
    DeRef(_data_62662);
    return;
    ;
}


void _40add_data(int _ref_62667, int _data_62668)
{
    int _31565 = NOVALUE;
    int _31564 = NOVALUE;
    int _31563 = NOVALUE;
    int _31561 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62667)) {
        _1 = (long)(DBL_PTR(_ref_62667)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62667)) && (DBL_PTR(_ref_62667)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62667);
        _ref_62667 = _1;
    }

    /** 	forward_references[ref][FR_DATA] = append( forward_references[ref][FR_DATA], data )*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62667 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31563 = (int)*(((s1_ptr)_2)->base + _ref_62667);
    _2 = (int)SEQ_PTR(_31563);
    _31564 = (int)*(((s1_ptr)_2)->base + 12);
    _31563 = NOVALUE;
    Ref(_data_62668);
    Append(&_31565, _31564, _data_62668);
    _31564 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _31565;
    if( _1 != _31565 ){
        DeRef(_1);
    }
    _31565 = NOVALUE;
    _31561 = NOVALUE;

    /** end procedure*/
    DeRef(_data_62668);
    return;
    ;
}


void _40set_line(int _ref_62676, int _line_no_62677, int _this_line_62678, int _bp_62679)
{
    int _31570 = NOVALUE;
    int _31568 = NOVALUE;
    int _31566 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ref_62676)) {
        _1 = (long)(DBL_PTR(_ref_62676)->dbl);
        if (UNIQUE(DBL_PTR(_ref_62676)) && (DBL_PTR(_ref_62676)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_62676);
        _ref_62676 = _1;
    }
    if (!IS_ATOM_INT(_line_no_62677)) {
        _1 = (long)(DBL_PTR(_line_no_62677)->dbl);
        if (UNIQUE(DBL_PTR(_line_no_62677)) && (DBL_PTR(_line_no_62677)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_line_no_62677);
        _line_no_62677 = _1;
    }
    if (!IS_ATOM_INT(_bp_62679)) {
        _1 = (long)(DBL_PTR(_bp_62679)->dbl);
        if (UNIQUE(DBL_PTR(_bp_62679)) && (DBL_PTR(_bp_62679)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bp_62679);
        _bp_62679 = _1;
    }

    /** 	forward_references[ref][FR_LINE] = line_no*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62676 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _line_no_62677;
    DeRef(_1);
    _31566 = NOVALUE;

    /** 	forward_references[ref][FR_THISLINE] = this_line*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62676 + ((s1_ptr)_2)->base);
    RefDS(_this_line_62678);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _this_line_62678;
    DeRef(_1);
    _31568 = NOVALUE;

    /** 	forward_references[ref][FR_BP] = bp*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_62676 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _bp_62679;
    DeRef(_1);
    _31570 = NOVALUE;

    /** end procedure*/
    DeRefDS(_this_line_62678);
    return;
    ;
}


void _40add_private_symbol(int _sym_62691, int _name_62692)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_62691)) {
        _1 = (long)(DBL_PTR(_sym_62691)->dbl);
        if (UNIQUE(DBL_PTR(_sym_62691)) && (DBL_PTR(_sym_62691)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_62691);
        _sym_62691 = _1;
    }

    /** 	fwd_private_sym &= sym*/
    Append(&_40fwd_private_sym_62686, _40fwd_private_sym_62686, _sym_62691);

    /** 	fwd_private_name = append( fwd_private_name, name )*/
    RefDS(_name_62692);
    Append(&_40fwd_private_name_62687, _40fwd_private_name_62687, _name_62692);

    /** end procedure*/
    DeRefDS(_name_62692);
    return;
    ;
}


void _40patch_forward_goto(int _tok_62700, int _ref_62701)
{
    int _fr_62702 = NOVALUE;
    int _31586 = NOVALUE;
    int _31585 = NOVALUE;
    int _31584 = NOVALUE;
    int _31583 = NOVALUE;
    int _31582 = NOVALUE;
    int _31581 = NOVALUE;
    int _31580 = NOVALUE;
    int _31579 = NOVALUE;
    int _31577 = NOVALUE;
    int _31576 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_62702);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_62702 = (int)*(((s1_ptr)_2)->base + _ref_62701);
    Ref(_fr_62702);

    /** 	set_code( ref )*/
    _40set_code(_ref_62701);

    /** 	shifting_sub = fr[FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_fr_62702);
    _40shifting_sub_62533 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_40shifting_sub_62533))
    _40shifting_sub_62533 = (long)DBL_PTR(_40shifting_sub_62533)->dbl;

    /** 	if length( fr[FR_DATA] ) = 2 then*/
    _2 = (int)SEQ_PTR(_fr_62702);
    _31576 = (int)*(((s1_ptr)_2)->base + 12);
    if (IS_SEQUENCE(_31576)){
            _31577 = SEQ_PTR(_31576)->length;
    }
    else {
        _31577 = 1;
    }
    _31576 = NOVALUE;
    if (_31577 != 2)
    goto L1; // [37] 68

    /** 		prep_forward_error( ref )*/
    _40prep_forward_error(_ref_62701);

    /** 		CompileErr( 156, { fr[FR_DATA][2] })*/
    _2 = (int)SEQ_PTR(_fr_62702);
    _31579 = (int)*(((s1_ptr)_2)->base + 12);
    _2 = (int)SEQ_PTR(_31579);
    _31580 = (int)*(((s1_ptr)_2)->base + 2);
    _31579 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31580);
    *((int *)(_2+4)) = _31580;
    _31581 = MAKE_SEQ(_1);
    _31580 = NOVALUE;
    _46CompileErr(156, _31581, 0);
    _31581 = NOVALUE;
L1: 

    /** 	Goto_block(  fr[FR_DATA][1], fr[FR_DATA][3], fr[FR_PC] )*/
    _2 = (int)SEQ_PTR(_fr_62702);
    _31582 = (int)*(((s1_ptr)_2)->base + 12);
    _2 = (int)SEQ_PTR(_31582);
    _31583 = (int)*(((s1_ptr)_2)->base + 1);
    _31582 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_62702);
    _31584 = (int)*(((s1_ptr)_2)->base + 12);
    _2 = (int)SEQ_PTR(_31584);
    _31585 = (int)*(((s1_ptr)_2)->base + 3);
    _31584 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_62702);
    _31586 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_31583);
    Ref(_31585);
    Ref(_31586);
    _68Goto_block(_31583, _31585, _31586);
    _31583 = NOVALUE;
    _31585 = NOVALUE;
    _31586 = NOVALUE;

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** 	reset_code()*/
    _40reset_code();

    /** 	resolved_reference( ref )*/
    _40resolved_reference(_ref_62701);

    /** end procedure*/
    DeRefDS(_fr_62702);
    _31576 = NOVALUE;
    return;
    ;
}


void _40patch_forward_call(int _tok_62723, int _ref_62724)
{
    int _fr_62725 = NOVALUE;
    int _sub_62728 = NOVALUE;
    int _defarg_62734 = NOVALUE;
    int _paramsym_62738 = NOVALUE;
    int _old_62741 = NOVALUE;
    int _tx_62745 = NOVALUE;
    int _code_sub_62755 = NOVALUE;
    int _args_62757 = NOVALUE;
    int _is_func_62762 = NOVALUE;
    int _real_file_62776 = NOVALUE;
    int _code_62780 = NOVALUE;
    int _temp_sub_62782 = NOVALUE;
    int _pc_62784 = NOVALUE;
    int _next_pc_62786 = NOVALUE;
    int _supplied_args_62787 = NOVALUE;
    int _name_62790 = NOVALUE;
    int _old_temps_allocated_62814 = NOVALUE;
    int _temp_target_62823 = NOVALUE;
    int _converted_code_62826 = NOVALUE;
    int _target_62842 = NOVALUE;
    int _has_defaults_62848 = NOVALUE;
    int _goto_target_62849 = NOVALUE;
    int _defarg_62852 = NOVALUE;
    int _code_len_62853 = NOVALUE;
    int _extra_default_args_62855 = NOVALUE;
    int _param_sym_62858 = NOVALUE;
    int _params_62859 = NOVALUE;
    int _orig_code_62861 = NOVALUE;
    int _orig_linetable_62862 = NOVALUE;
    int _ar_sp_62865 = NOVALUE;
    int _pre_refs_62869 = NOVALUE;
    int _old_fwd_params_62884 = NOVALUE;
    int _temp_shifting_sub_62925 = NOVALUE;
    int _new_code_62929 = NOVALUE;
    int _routine_type_62938 = NOVALUE;
    int _32339 = NOVALUE;
    int _31723 = NOVALUE;
    int _31722 = NOVALUE;
    int _31721 = NOVALUE;
    int _31719 = NOVALUE;
    int _31718 = NOVALUE;
    int _31717 = NOVALUE;
    int _31716 = NOVALUE;
    int _31715 = NOVALUE;
    int _31714 = NOVALUE;
    int _31713 = NOVALUE;
    int _31712 = NOVALUE;
    int _31711 = NOVALUE;
    int _31710 = NOVALUE;
    int _31709 = NOVALUE;
    int _31708 = NOVALUE;
    int _31707 = NOVALUE;
    int _31705 = NOVALUE;
    int _31704 = NOVALUE;
    int _31703 = NOVALUE;
    int _31702 = NOVALUE;
    int _31701 = NOVALUE;
    int _31700 = NOVALUE;
    int _31699 = NOVALUE;
    int _31698 = NOVALUE;
    int _31696 = NOVALUE;
    int _31693 = NOVALUE;
    int _31692 = NOVALUE;
    int _31691 = NOVALUE;
    int _31690 = NOVALUE;
    int _31685 = NOVALUE;
    int _31684 = NOVALUE;
    int _31683 = NOVALUE;
    int _31682 = NOVALUE;
    int _31681 = NOVALUE;
    int _31679 = NOVALUE;
    int _31678 = NOVALUE;
    int _31677 = NOVALUE;
    int _31676 = NOVALUE;
    int _31675 = NOVALUE;
    int _31674 = NOVALUE;
    int _31672 = NOVALUE;
    int _31671 = NOVALUE;
    int _31669 = NOVALUE;
    int _31668 = NOVALUE;
    int _31667 = NOVALUE;
    int _31666 = NOVALUE;
    int _31664 = NOVALUE;
    int _31662 = NOVALUE;
    int _31661 = NOVALUE;
    int _31660 = NOVALUE;
    int _31658 = NOVALUE;
    int _31657 = NOVALUE;
    int _31655 = NOVALUE;
    int _31653 = NOVALUE;
    int _31650 = NOVALUE;
    int _31646 = NOVALUE;
    int _31644 = NOVALUE;
    int _31643 = NOVALUE;
    int _31641 = NOVALUE;
    int _31640 = NOVALUE;
    int _31639 = NOVALUE;
    int _31638 = NOVALUE;
    int _31636 = NOVALUE;
    int _31635 = NOVALUE;
    int _31634 = NOVALUE;
    int _31633 = NOVALUE;
    int _31632 = NOVALUE;
    int _31630 = NOVALUE;
    int _31629 = NOVALUE;
    int _31628 = NOVALUE;
    int _31627 = NOVALUE;
    int _31626 = NOVALUE;
    int _31625 = NOVALUE;
    int _31624 = NOVALUE;
    int _31623 = NOVALUE;
    int _31622 = NOVALUE;
    int _31620 = NOVALUE;
    int _31619 = NOVALUE;
    int _31618 = NOVALUE;
    int _31617 = NOVALUE;
    int _31616 = NOVALUE;
    int _31613 = NOVALUE;
    int _31609 = NOVALUE;
    int _31608 = NOVALUE;
    int _31607 = NOVALUE;
    int _31606 = NOVALUE;
    int _31605 = NOVALUE;
    int _31604 = NOVALUE;
    int _31602 = NOVALUE;
    int _31599 = NOVALUE;
    int _31597 = NOVALUE;
    int _31596 = NOVALUE;
    int _31594 = NOVALUE;
    int _31591 = NOVALUE;
    int _31590 = NOVALUE;
    int _31589 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_62725);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_62725 = (int)*(((s1_ptr)_2)->base + _ref_62724);
    Ref(_fr_62725);

    /** 	symtab_index sub = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_62723);
    _sub_62728 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sub_62728)){
        _sub_62728 = (long)DBL_PTR(_sub_62728)->dbl;
    }

    /** 	if sequence( fr[FR_DATA] ) then*/
    _2 = (int)SEQ_PTR(_fr_62725);
    _31589 = (int)*(((s1_ptr)_2)->base + 12);
    _31590 = IS_SEQUENCE(_31589);
    _31589 = NOVALUE;
    if (_31590 == 0)
    {
        _31590 = NOVALUE;
        goto L1; // [34] 121
    }
    else{
        _31590 = NOVALUE;
    }

    /** 		sequence defarg = fr[FR_DATA][1]*/
    _2 = (int)SEQ_PTR(_fr_62725);
    _31591 = (int)*(((s1_ptr)_2)->base + 12);
    DeRef(_defarg_62734);
    _2 = (int)SEQ_PTR(_31591);
    _defarg_62734 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_defarg_62734);
    _31591 = NOVALUE;

    /** 		symtab_index paramsym = defarg[2]*/
    _2 = (int)SEQ_PTR(_defarg_62734);
    _paramsym_62738 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_paramsym_62738)){
        _paramsym_62738 = (long)DBL_PTR(_paramsym_62738)->dbl;
    }

    /** 		token old = { RECORDED, defarg[3] }*/
    _2 = (int)SEQ_PTR(_defarg_62734);
    _31594 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_31594);
    DeRef(_old_62741);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _31594;
    _old_62741 = MAKE_SEQ(_1);
    _31594 = NOVALUE;

    /** 		integer tx = find( old, SymTab[paramsym][S_CODE] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31596 = (int)*(((s1_ptr)_2)->base + _paramsym_62738);
    _2 = (int)SEQ_PTR(_31596);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _31597 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _31597 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _31596 = NOVALUE;
    _tx_62745 = find_from(_old_62741, _31597, 1);
    _31597 = NOVALUE;

    /** 		SymTab[paramsym][S_CODE][tx] = tok*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_paramsym_62738 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _3 = (int)(_38S_CODE_16610 + ((s1_ptr)_2)->base);
    _31599 = NOVALUE;
    Ref(_tok_62723);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _tx_62745);
    _1 = *(int *)_2;
    *(int *)_2 = _tok_62723;
    DeRef(_1);
    _31599 = NOVALUE;

    /** 		resolved_reference( ref )*/
    _40resolved_reference(_ref_62724);

    /** 		return*/
    DeRefDS(_defarg_62734);
    DeRefDS(_old_62741);
    DeRef(_tok_62723);
    DeRefDS(_fr_62725);
    DeRef(_code_62780);
    DeRef(_name_62790);
    DeRef(_params_62859);
    DeRef(_orig_code_62861);
    DeRef(_orig_linetable_62862);
    DeRef(_old_fwd_params_62884);
    DeRef(_new_code_62929);
    return;
L1: 
    DeRef(_defarg_62734);
    _defarg_62734 = NOVALUE;
    DeRef(_old_62741);
    _old_62741 = NOVALUE;

    /** 	integer code_sub = fr[FR_SUBPROG]*/
    _2 = (int)SEQ_PTR(_fr_62725);
    _code_sub_62755 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_code_sub_62755))
    _code_sub_62755 = (long)DBL_PTR(_code_sub_62755)->dbl;

    /** 	integer args = SymTab[sub][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31602 = (int)*(((s1_ptr)_2)->base + _sub_62728);
    _2 = (int)SEQ_PTR(_31602);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _args_62757 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _args_62757 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    if (!IS_ATOM_INT(_args_62757)){
        _args_62757 = (long)DBL_PTR(_args_62757)->dbl;
    }
    _31602 = NOVALUE;

    /** 	integer is_func = (SymTab[sub][S_TOKEN] = FUNC) or (SymTab[sub][S_TOKEN] = TYPE)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31604 = (int)*(((s1_ptr)_2)->base + _sub_62728);
    _2 = (int)SEQ_PTR(_31604);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _31605 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _31605 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _31604 = NOVALUE;
    if (IS_ATOM_INT(_31605)) {
        _31606 = (_31605 == 501);
    }
    else {
        _31606 = binary_op(EQUALS, _31605, 501);
    }
    _31605 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31607 = (int)*(((s1_ptr)_2)->base + _sub_62728);
    _2 = (int)SEQ_PTR(_31607);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _31608 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _31608 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _31607 = NOVALUE;
    if (IS_ATOM_INT(_31608)) {
        _31609 = (_31608 == 504);
    }
    else {
        _31609 = binary_op(EQUALS, _31608, 504);
    }
    _31608 = NOVALUE;
    if (IS_ATOM_INT(_31606) && IS_ATOM_INT(_31609)) {
        _is_func_62762 = (_31606 != 0 || _31609 != 0);
    }
    else {
        _is_func_62762 = binary_op(OR, _31606, _31609);
    }
    DeRef(_31606);
    _31606 = NOVALUE;
    DeRef(_31609);
    _31609 = NOVALUE;
    if (!IS_ATOM_INT(_is_func_62762)) {
        _1 = (long)(DBL_PTR(_is_func_62762)->dbl);
        if (UNIQUE(DBL_PTR(_is_func_62762)) && (DBL_PTR(_is_func_62762)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_func_62762);
        _is_func_62762 = _1;
    }

    /** 	integer real_file = current_file_no*/
    _real_file_62776 = _38current_file_no_16946;

    /** 	current_file_no = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_62725);
    _38current_file_no_16946 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_38current_file_no_16946)){
        _38current_file_no_16946 = (long)DBL_PTR(_38current_file_no_16946)->dbl;
    }

    /** 	set_code( ref )*/
    _40set_code(_ref_62724);

    /** 	sequence code = Code*/
    RefDS(_38Code_17038);
    DeRef(_code_62780);
    _code_62780 = _38Code_17038;

    /** 	integer temp_sub = CurrentSub*/
    _temp_sub_62782 = _38CurrentSub_16954;

    /** 	integer pc = fr[FR_PC]*/
    _2 = (int)SEQ_PTR(_fr_62725);
    _pc_62784 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_pc_62784))
    _pc_62784 = (long)DBL_PTR(_pc_62784)->dbl;

    /** 	integer next_pc = pc*/
    _next_pc_62786 = _pc_62784;

    /** 	integer supplied_args = code[pc+2]*/
    _31613 = _pc_62784 + 2;
    _2 = (int)SEQ_PTR(_code_62780);
    _supplied_args_62787 = (int)*(((s1_ptr)_2)->base + _31613);
    if (!IS_ATOM_INT(_supplied_args_62787))
    _supplied_args_62787 = (long)DBL_PTR(_supplied_args_62787)->dbl;

    /** 	sequence name = fr[FR_NAME]*/
    DeRef(_name_62790);
    _2 = (int)SEQ_PTR(_fr_62725);
    _name_62790 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_name_62790);

    /** 	if Code[pc] != FUNC_FORWARD and Code[pc] != PROC_FORWARD then*/
    _2 = (int)SEQ_PTR(_38Code_17038);
    _31616 = (int)*(((s1_ptr)_2)->base + _pc_62784);
    if (IS_ATOM_INT(_31616)) {
        _31617 = (_31616 != 196);
    }
    else {
        _31617 = binary_op(NOTEQ, _31616, 196);
    }
    _31616 = NOVALUE;
    if (IS_ATOM_INT(_31617)) {
        if (_31617 == 0) {
            goto L2; // [280] 350
        }
    }
    else {
        if (DBL_PTR(_31617)->dbl == 0.0) {
            goto L2; // [280] 350
        }
    }
    _2 = (int)SEQ_PTR(_38Code_17038);
    _31619 = (int)*(((s1_ptr)_2)->base + _pc_62784);
    if (IS_ATOM_INT(_31619)) {
        _31620 = (_31619 != 195);
    }
    else {
        _31620 = binary_op(NOTEQ, _31619, 195);
    }
    _31619 = NOVALUE;
    if (_31620 == 0) {
        DeRef(_31620);
        _31620 = NOVALUE;
        goto L2; // [297] 350
    }
    else {
        if (!IS_ATOM_INT(_31620) && DBL_PTR(_31620)->dbl == 0.0){
            DeRef(_31620);
            _31620 = NOVALUE;
            goto L2; // [297] 350
        }
        DeRef(_31620);
        _31620 = NOVALUE;
    }
    DeRef(_31620);
    _31620 = NOVALUE;

    /** 		prep_forward_error( ref )*/
    _40prep_forward_error(_ref_62724);

    /** 		CompileErr( "The forward call to [4] wasn't where we thought it would be: [1]:[2]:[3]",*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _31622 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _2 = (int)SEQ_PTR(_fr_62725);
    _31623 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_31623);
    _31624 = _55sym_name(_31623);
    _31623 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_62725);
    _31625 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_fr_62725);
    _31626 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31622);
    *((int *)(_2+4)) = _31622;
    *((int *)(_2+8)) = _31624;
    Ref(_31625);
    *((int *)(_2+12)) = _31625;
    Ref(_31626);
    *((int *)(_2+16)) = _31626;
    _31627 = MAKE_SEQ(_1);
    _31626 = NOVALUE;
    _31625 = NOVALUE;
    _31624 = NOVALUE;
    _31622 = NOVALUE;
    RefDS(_31621);
    _46CompileErr(_31621, _31627, 0);
    _31627 = NOVALUE;
L2: 

    /** 	integer old_temps_allocated = temps_allocated*/
    _old_temps_allocated_62814 = _55temps_allocated_47575;

    /** 	temps_allocated = 0*/
    _55temps_allocated_47575 = 0;

    /** 	if is_func and fr[FR_OP] = PROC then*/
    if (_is_func_62762 == 0) {
        goto L3; // [368] 458
    }
    _2 = (int)SEQ_PTR(_fr_62725);
    _31629 = (int)*(((s1_ptr)_2)->base + 10);
    if (IS_ATOM_INT(_31629)) {
        _31630 = (_31629 == 27);
    }
    else {
        _31630 = binary_op(EQUALS, _31629, 27);
    }
    _31629 = NOVALUE;
    if (_31630 == 0) {
        DeRef(_31630);
        _31630 = NOVALUE;
        goto L3; // [385] 458
    }
    else {
        if (!IS_ATOM_INT(_31630) && DBL_PTR(_31630)->dbl == 0.0){
            DeRef(_31630);
            _31630 = NOVALUE;
            goto L3; // [385] 458
        }
        DeRef(_31630);
        _31630 = NOVALUE;
    }
    DeRef(_31630);
    _31630 = NOVALUE;

    /** 		symtab_index temp_target = NewTempSym()*/
    _temp_target_62823 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_temp_target_62823)) {
        _1 = (long)(DBL_PTR(_temp_target_62823)->dbl);
        if (UNIQUE(DBL_PTR(_temp_target_62823)) && (DBL_PTR(_temp_target_62823)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_temp_target_62823);
        _temp_target_62823 = _1;
    }

    /** 		sequence converted_code = */
    _31632 = _pc_62784 + 1;
    if (_31632 > MAXINT){
        _31632 = NewDouble((double)_31632);
    }
    _31633 = _pc_62784 + 2;
    if ((long)((unsigned long)_31633 + (unsigned long)HIGH_BITS) >= 0) 
    _31633 = NewDouble((double)_31633);
    if (IS_ATOM_INT(_31633)) {
        _31634 = _31633 + _supplied_args_62787;
    }
    else {
        _31634 = NewDouble(DBL_PTR(_31633)->dbl + (double)_supplied_args_62787);
    }
    DeRef(_31633);
    _31633 = NOVALUE;
    rhs_slice_target = (object_ptr)&_31635;
    RHS_Slice(_38Code_17038, _31632, _31634);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 208;
    ((int *)_2)[2] = _temp_target_62823;
    _31636 = MAKE_SEQ(_1);
    {
        int concat_list[4];

        concat_list[0] = _31636;
        concat_list[1] = _temp_target_62823;
        concat_list[2] = _31635;
        concat_list[3] = 196;
        Concat_N((object_ptr)&_converted_code_62826, concat_list, 4);
    }
    DeRefDS(_31636);
    _31636 = NOVALUE;
    DeRefDS(_31635);
    _31635 = NOVALUE;

    /** 		replace_code( converted_code, pc, pc + 2 + supplied_args, code_sub )*/
    _31638 = _pc_62784 + 2;
    if ((long)((unsigned long)_31638 + (unsigned long)HIGH_BITS) >= 0) 
    _31638 = NewDouble((double)_31638);
    if (IS_ATOM_INT(_31638)) {
        _31639 = _31638 + _supplied_args_62787;
        if ((long)((unsigned long)_31639 + (unsigned long)HIGH_BITS) >= 0) 
        _31639 = NewDouble((double)_31639);
    }
    else {
        _31639 = NewDouble(DBL_PTR(_31638)->dbl + (double)_supplied_args_62787);
    }
    DeRef(_31638);
    _31638 = NOVALUE;
    RefDS(_converted_code_62826);
    _40replace_code(_converted_code_62826, _pc_62784, _31639, _code_sub_62755);
    _31639 = NOVALUE;

    /** 		code = Code*/
    RefDS(_38Code_17038);
    DeRef(_code_62780);
    _code_62780 = _38Code_17038;
L3: 
    DeRef(_converted_code_62826);
    _converted_code_62826 = NOVALUE;

    /** 	next_pc +=*/
    _31640 = 3 + _supplied_args_62787;
    if ((long)((unsigned long)_31640 + (unsigned long)HIGH_BITS) >= 0) 
    _31640 = NewDouble((double)_31640);
    if (IS_ATOM_INT(_31640)) {
        _31641 = _31640 + _is_func_62762;
        if ((long)((unsigned long)_31641 + (unsigned long)HIGH_BITS) >= 0) 
        _31641 = NewDouble((double)_31641);
    }
    else {
        _31641 = NewDouble(DBL_PTR(_31640)->dbl + (double)_is_func_62762);
    }
    DeRef(_31640);
    _31640 = NOVALUE;
    if (IS_ATOM_INT(_31641)) {
        _next_pc_62786 = _next_pc_62786 + _31641;
    }
    else {
        _next_pc_62786 = NewDouble((double)_next_pc_62786 + DBL_PTR(_31641)->dbl);
    }
    DeRef(_31641);
    _31641 = NOVALUE;
    if (!IS_ATOM_INT(_next_pc_62786)) {
        _1 = (long)(DBL_PTR(_next_pc_62786)->dbl);
        if (UNIQUE(DBL_PTR(_next_pc_62786)) && (DBL_PTR(_next_pc_62786)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_pc_62786);
        _next_pc_62786 = _1;
    }

    /** 	integer target*/

    /** 	if is_func then*/
    if (_is_func_62762 == 0)
    {
        goto L4; // [480] 502
    }
    else{
    }

    /** 		target = Code[pc + 3 + supplied_args]*/
    _31643 = _pc_62784 + 3;
    if ((long)((unsigned long)_31643 + (unsigned long)HIGH_BITS) >= 0) 
    _31643 = NewDouble((double)_31643);
    if (IS_ATOM_INT(_31643)) {
        _31644 = _31643 + _supplied_args_62787;
    }
    else {
        _31644 = NewDouble(DBL_PTR(_31643)->dbl + (double)_supplied_args_62787);
    }
    DeRef(_31643);
    _31643 = NOVALUE;
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!IS_ATOM_INT(_31644)){
        _target_62842 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31644)->dbl));
    }
    else{
        _target_62842 = (int)*(((s1_ptr)_2)->base + _31644);
    }
    if (!IS_ATOM_INT(_target_62842)){
        _target_62842 = (long)DBL_PTR(_target_62842)->dbl;
    }
L4: 

    /** 	integer has_defaults = 0*/
    _has_defaults_62848 = 0;

    /** 	integer goto_target = length( code ) + 1*/
    if (IS_SEQUENCE(_code_62780)){
            _31646 = SEQ_PTR(_code_62780)->length;
    }
    else {
        _31646 = 1;
    }
    _goto_target_62849 = _31646 + 1;
    _31646 = NOVALUE;

    /** 	integer defarg = 0*/
    _defarg_62852 = 0;

    /** 	integer code_len = length(code)*/
    if (IS_SEQUENCE(_code_62780)){
            _code_len_62853 = SEQ_PTR(_code_62780)->length;
    }
    else {
        _code_len_62853 = 1;
    }

    /** 	integer extra_default_args = 0*/
    _extra_default_args_62855 = 0;

    /** 	set_dont_read( 1 )*/
    _62set_dont_read(1);

    /** 	reset_private_lists()*/

    /** 	fwd_private_sym  = {}*/
    RefDS(_22663);
    DeRefi(_40fwd_private_sym_62686);
    _40fwd_private_sym_62686 = _22663;

    /** 	fwd_private_name = {}*/
    RefDS(_22663);
    DeRef(_40fwd_private_name_62687);
    _40fwd_private_name_62687 = _22663;

    /** end procedure*/
    goto L5; // [554] 557
L5: 

    /** 	integer param_sym = sub*/
    _param_sym_62858 = _sub_62728;

    /** 	sequence params = repeat( 0, args )*/
    DeRef(_params_62859);
    _params_62859 = Repeat(0, _args_62757);

    /** 	sequence orig_code = code*/
    RefDS(_code_62780);
    DeRef(_orig_code_62861);
    _orig_code_62861 = _code_62780;

    /** 	sequence orig_linetable = LineTable*/
    RefDS(_38LineTable_17039);
    DeRef(_orig_linetable_62862);
    _orig_linetable_62862 = _38LineTable_17039;

    /** 	Code = {}*/
    RefDS(_22663);
    DeRef(_38Code_17038);
    _38Code_17038 = _22663;

    /** 	integer ar_sp = find( code_sub, active_subprogs[current_file_no] )*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _31650 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _ar_sp_62865 = find_from(_code_sub_62755, _31650, 1);
    _31650 = NOVALUE;

    /** 	integer pre_refs*/

    /** 	if code_sub = TopLevelSub then*/
    if (_code_sub_62755 != _38TopLevelSub_16953)
    goto L6; // [614] 634

    /** 		pre_refs = length( toplevel_references[current_file_no] )*/
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    _31653 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_31653)){
            _pre_refs_62869 = SEQ_PTR(_31653)->length;
    }
    else {
        _pre_refs_62869 = 1;
    }
    _31653 = NOVALUE;
    goto L7; // [631] 667
L6: 

    /** 		ar_sp = find( code_sub, active_subprogs[current_file_no] )*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _31655 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _ar_sp_62865 = find_from(_code_sub_62755, _31655, 1);
    _31655 = NOVALUE;

    /** 		pre_refs = length( active_references[current_file_no][ar_sp] )*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    _31657 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _2 = (int)SEQ_PTR(_31657);
    _31658 = (int)*(((s1_ptr)_2)->base + _ar_sp_62865);
    _31657 = NOVALUE;
    if (IS_SEQUENCE(_31658)){
            _pre_refs_62869 = SEQ_PTR(_31658)->length;
    }
    else {
        _pre_refs_62869 = 1;
    }
    _31658 = NOVALUE;
L7: 

    /** 	sequence old_fwd_params = {}*/
    RefDS(_22663);
    DeRef(_old_fwd_params_62884);
    _old_fwd_params_62884 = _22663;

    /** 	for i = pc + 3 to pc + args + 2 do*/
    _31660 = _pc_62784 + 3;
    if ((long)((unsigned long)_31660 + (unsigned long)HIGH_BITS) >= 0) 
    _31660 = NewDouble((double)_31660);
    _31661 = _pc_62784 + _args_62757;
    if ((long)((unsigned long)_31661 + (unsigned long)HIGH_BITS) >= 0) 
    _31661 = NewDouble((double)_31661);
    if (IS_ATOM_INT(_31661)) {
        _31662 = _31661 + 2;
        if ((long)((unsigned long)_31662 + (unsigned long)HIGH_BITS) >= 0) 
        _31662 = NewDouble((double)_31662);
    }
    else {
        _31662 = NewDouble(DBL_PTR(_31661)->dbl + (double)2);
    }
    DeRef(_31661);
    _31661 = NOVALUE;
    {
        int _i_62886;
        Ref(_31660);
        _i_62886 = _31660;
L8: 
        if (binary_op_a(GREATER, _i_62886, _31662)){
            goto L9; // [688] 849
        }

        /** 		defarg += 1*/
        _defarg_62852 = _defarg_62852 + 1;

        /** 		param_sym = SymTab[param_sym][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _31664 = (int)*(((s1_ptr)_2)->base + _param_sym_62858);
        _2 = (int)SEQ_PTR(_31664);
        _param_sym_62858 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_sym_62858)){
            _param_sym_62858 = (long)DBL_PTR(_param_sym_62858)->dbl;
        }
        _31664 = NOVALUE;

        /** 		if defarg > supplied_args or i > length( code ) or not code[i] then*/
        _31666 = (_defarg_62852 > _supplied_args_62787);
        if (_31666 != 0) {
            _31667 = 1;
            goto LA; // [723] 738
        }
        if (IS_SEQUENCE(_code_62780)){
                _31668 = SEQ_PTR(_code_62780)->length;
        }
        else {
            _31668 = 1;
        }
        if (IS_ATOM_INT(_i_62886)) {
            _31669 = (_i_62886 > _31668);
        }
        else {
            _31669 = (DBL_PTR(_i_62886)->dbl > (double)_31668);
        }
        _31668 = NOVALUE;
        _31667 = (_31669 != 0);
LA: 
        if (_31667 != 0) {
            goto LB; // [738] 754
        }
        _2 = (int)SEQ_PTR(_code_62780);
        if (!IS_ATOM_INT(_i_62886)){
            _31671 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62886)->dbl));
        }
        else{
            _31671 = (int)*(((s1_ptr)_2)->base + _i_62886);
        }
        if (IS_ATOM_INT(_31671)) {
            _31672 = (_31671 == 0);
        }
        else {
            _31672 = unary_op(NOT, _31671);
        }
        _31671 = NOVALUE;
        if (_31672 == 0) {
            DeRef(_31672);
            _31672 = NOVALUE;
            goto LC; // [750] 804
        }
        else {
            if (!IS_ATOM_INT(_31672) && DBL_PTR(_31672)->dbl == 0.0){
                DeRef(_31672);
                _31672 = NOVALUE;
                goto LC; // [750] 804
            }
            DeRef(_31672);
            _31672 = NOVALUE;
        }
        DeRef(_31672);
        _31672 = NOVALUE;
LB: 

        /** 			has_defaults = 1*/
        _has_defaults_62848 = 1;

        /** 			extra_default_args += 1*/
        _extra_default_args_62855 = _extra_default_args_62855 + 1;

        /** 			show_params( sub )*/
        _55show_params(_sub_62728);

        /** 			set_error_info( ref )*/
        _40set_error_info(_ref_62724);

        /** 			Parse_default_arg(sub, defarg, fwd_private_name, fwd_private_sym) --call_proc( parse_arg_rid, { sub, defarg, fwd_private_name, fwd_private_sym } )*/
        RefDS(_40fwd_private_name_62687);
        RefDS(_40fwd_private_sym_62686);
        _41Parse_default_arg(_sub_62728, _defarg_62852, _40fwd_private_name_62687, _40fwd_private_sym_62686);

        /** 			hide_params( sub )*/
        _55hide_params(_sub_62728);

        /** 			params[defarg] = Pop()*/
        _31674 = _43Pop();
        _2 = (int)SEQ_PTR(_params_62859);
        _2 = (int)(((s1_ptr)_2)->base + _defarg_62852);
        _1 = *(int *)_2;
        *(int *)_2 = _31674;
        if( _1 != _31674 ){
            DeRef(_1);
        }
        _31674 = NOVALUE;
        goto LD; // [801] 842
LC: 

        /** 			extra_default_args = 0*/
        _extra_default_args_62855 = 0;

        /** 			add_private_symbol( code[i], SymTab[param_sym][S_NAME] )*/
        _2 = (int)SEQ_PTR(_code_62780);
        if (!IS_ATOM_INT(_i_62886)){
            _31675 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62886)->dbl));
        }
        else{
            _31675 = (int)*(((s1_ptr)_2)->base + _i_62886);
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _31676 = (int)*(((s1_ptr)_2)->base + _param_sym_62858);
        _2 = (int)SEQ_PTR(_31676);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _31677 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _31677 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        _31676 = NOVALUE;
        Ref(_31675);
        Ref(_31677);
        _40add_private_symbol(_31675, _31677);
        _31675 = NOVALUE;
        _31677 = NOVALUE;

        /** 			params[defarg] = code[i]*/
        _2 = (int)SEQ_PTR(_code_62780);
        if (!IS_ATOM_INT(_i_62886)){
            _31678 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62886)->dbl));
        }
        else{
            _31678 = (int)*(((s1_ptr)_2)->base + _i_62886);
        }
        Ref(_31678);
        _2 = (int)SEQ_PTR(_params_62859);
        _2 = (int)(((s1_ptr)_2)->base + _defarg_62852);
        _1 = *(int *)_2;
        *(int *)_2 = _31678;
        if( _1 != _31678 ){
            DeRef(_1);
        }
        _31678 = NOVALUE;
LD: 

        /** 	end for*/
        _0 = _i_62886;
        if (IS_ATOM_INT(_i_62886)) {
            _i_62886 = _i_62886 + 1;
            if ((long)((unsigned long)_i_62886 +(unsigned long) HIGH_BITS) >= 0){
                _i_62886 = NewDouble((double)_i_62886);
            }
        }
        else {
            _i_62886 = binary_op_a(PLUS, _i_62886, 1);
        }
        DeRef(_0);
        goto L8; // [844] 695
L9: 
        ;
        DeRef(_i_62886);
    }

    /** 	SymTab[code_sub][S_STACK_SPACE] += temps_allocated*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_code_sub_62755 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658)){
        _31681 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    }
    else{
        _31681 = (int)*(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    }
    _31679 = NOVALUE;
    if (IS_ATOM_INT(_31681)) {
        _31682 = _31681 + _55temps_allocated_47575;
        if ((long)((unsigned long)_31682 + (unsigned long)HIGH_BITS) >= 0) 
        _31682 = NewDouble((double)_31682);
    }
    else {
        _31682 = binary_op(PLUS, _31681, _55temps_allocated_47575);
    }
    _31681 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    _1 = *(int *)_2;
    *(int *)_2 = _31682;
    if( _1 != _31682 ){
        DeRef(_1);
    }
    _31682 = NOVALUE;
    _31679 = NOVALUE;

    /** 	temps_allocated = old_temps_allocated*/
    _55temps_allocated_47575 = _old_temps_allocated_62814;

    /** 	integer temp_shifting_sub = shifting_sub*/
    _temp_shifting_sub_62925 = _40shifting_sub_62533;

    /** 	shift( -pc, pc-1 )*/
    if ((unsigned long)_pc_62784 == 0xC0000000)
    _31683 = (int)NewDouble((double)-0xC0000000);
    else
    _31683 = - _pc_62784;
    _31684 = _pc_62784 - 1;
    if ((long)((unsigned long)_31684 +(unsigned long) HIGH_BITS) >= 0){
        _31684 = NewDouble((double)_31684);
    }
    Ref(_31683);
    DeRef(_32339);
    _32339 = _31683;
    _67shift(_31683, _31684, _32339);
    _31683 = NOVALUE;
    _31684 = NOVALUE;
    _32339 = NOVALUE;

    /** 	sequence new_code = Code*/
    RefDS(_38Code_17038);
    DeRef(_new_code_62929);
    _new_code_62929 = _38Code_17038;

    /** 	Code = orig_code*/
    RefDS(_orig_code_62861);
    DeRefDS(_38Code_17038);
    _38Code_17038 = _orig_code_62861;

    /** 	LineTable = orig_linetable*/
    RefDS(_orig_linetable_62862);
    DeRef(_38LineTable_17039);
    _38LineTable_17039 = _orig_linetable_62862;

    /** 	set_dont_read( 0 )*/
    _62set_dont_read(0);

    /** 	current_file_no = real_file*/
    _38current_file_no_16946 = _real_file_62776;

    /** 	if args != ( supplied_args + extra_default_args ) then*/
    _31685 = _supplied_args_62787 + _extra_default_args_62855;
    if ((long)((unsigned long)_31685 + (unsigned long)HIGH_BITS) >= 0) 
    _31685 = NewDouble((double)_31685);
    if (binary_op_a(EQUALS, _args_62757, _31685)){
        DeRef(_31685);
        _31685 = NOVALUE;
        goto LE; // [946] 1028
    }
    DeRef(_31685);
    _31685 = NOVALUE;

    /** 		sequence routine_type*/

    /** 		if is_func then */
    if (_is_func_62762 == 0)
    {
        goto LF; // [954] 967
    }
    else{
    }

    /** 			routine_type = "function"*/
    RefDS(_27029);
    DeRefi(_routine_type_62938);
    _routine_type_62938 = _27029;
    goto L10; // [964] 975
LF: 

    /** 			routine_type = "procedure"*/
    RefDS(_27083);
    DeRefi(_routine_type_62938);
    _routine_type_62938 = _27083;
L10: 

    /** 		current_file_no = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_62725);
    _38current_file_no_16946 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_38current_file_no_16946)){
        _38current_file_no_16946 = (long)DBL_PTR(_38current_file_no_16946)->dbl;
    }

    /** 		line_number = fr[FR_LINE]*/
    _2 = (int)SEQ_PTR(_fr_62725);
    _38line_number_16947 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_38line_number_16947)){
        _38line_number_16947 = (long)DBL_PTR(_38line_number_16947)->dbl;
    }

    /** 		CompileErr( 158,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _31690 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _31691 = _supplied_args_62787 + _extra_default_args_62855;
    if ((long)((unsigned long)_31691 + (unsigned long)HIGH_BITS) >= 0) 
    _31691 = NewDouble((double)_31691);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_31690);
    *((int *)(_2+4)) = _31690;
    *((int *)(_2+8)) = _38line_number_16947;
    RefDS(_routine_type_62938);
    *((int *)(_2+12)) = _routine_type_62938;
    RefDS(_name_62790);
    *((int *)(_2+16)) = _name_62790;
    *((int *)(_2+20)) = _args_62757;
    *((int *)(_2+24)) = _31691;
    _31692 = MAKE_SEQ(_1);
    _31691 = NOVALUE;
    _31690 = NOVALUE;
    _46CompileErr(158, _31692, 0);
    _31692 = NOVALUE;
LE: 
    DeRefi(_routine_type_62938);
    _routine_type_62938 = NOVALUE;

    /** 	new_code &= PROC & sub & params*/
    {
        int concat_list[3];

        concat_list[0] = _params_62859;
        concat_list[1] = _sub_62728;
        concat_list[2] = 27;
        Concat_N((object_ptr)&_31693, concat_list, 3);
    }
    Concat((object_ptr)&_new_code_62929, _new_code_62929, _31693);
    DeRefDS(_31693);
    _31693 = NOVALUE;

    /** 	if is_func then*/
    if (_is_func_62762 == 0)
    {
        goto L11; // [1046] 1058
    }
    else{
    }

    /** 		new_code &= target*/
    Append(&_new_code_62929, _new_code_62929, _target_62842);
L11: 

    /** 	replace_code( new_code, pc, next_pc - 1, code_sub )*/
    _31696 = _next_pc_62786 - 1;
    if ((long)((unsigned long)_31696 +(unsigned long) HIGH_BITS) >= 0){
        _31696 = NewDouble((double)_31696);
    }
    RefDS(_new_code_62929);
    _40replace_code(_new_code_62929, _pc_62784, _31696, _code_sub_62755);
    _31696 = NOVALUE;

    /** 	if code_sub = TopLevelSub then*/
    if (_code_sub_62755 != _38TopLevelSub_16953)
    goto L12; // [1074] 1161

    /** 		for i = pre_refs + 1 to length( toplevel_references[fr[FR_FILE]] ) do*/
    _31698 = _pre_refs_62869 + 1;
    if (_31698 > MAXINT){
        _31698 = NewDouble((double)_31698);
    }
    _2 = (int)SEQ_PTR(_fr_62725);
    _31699 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    if (!IS_ATOM_INT(_31699)){
        _31700 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31699)->dbl));
    }
    else{
        _31700 = (int)*(((s1_ptr)_2)->base + _31699);
    }
    if (IS_SEQUENCE(_31700)){
            _31701 = SEQ_PTR(_31700)->length;
    }
    else {
        _31701 = 1;
    }
    _31700 = NOVALUE;
    {
        int _i_62963;
        Ref(_31698);
        _i_62963 = _31698;
L13: 
        if (binary_op_a(GREATER, _i_62963, _31701)){
            goto L14; // [1101] 1158
        }

        /** 			forward_references[toplevel_references[fr[FR_FILE]][i]][FR_PC] += pc - 1*/
        _2 = (int)SEQ_PTR(_fr_62725);
        _31702 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_40toplevel_references_62506);
        if (!IS_ATOM_INT(_31702)){
            _31703 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31702)->dbl));
        }
        else{
            _31703 = (int)*(((s1_ptr)_2)->base + _31702);
        }
        _2 = (int)SEQ_PTR(_31703);
        if (!IS_ATOM_INT(_i_62963)){
            _31704 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62963)->dbl));
        }
        else{
            _31704 = (int)*(((s1_ptr)_2)->base + _i_62963);
        }
        _31703 = NOVALUE;
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _40forward_references_62503 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31704))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31704)->dbl));
        else
        _3 = (int)(_31704 + ((s1_ptr)_2)->base);
        _31707 = _pc_62784 - 1;
        if ((long)((unsigned long)_31707 +(unsigned long) HIGH_BITS) >= 0){
            _31707 = NewDouble((double)_31707);
        }
        _2 = (int)SEQ_PTR(*(int *)_3);
        _31708 = (int)*(((s1_ptr)_2)->base + 5);
        _31705 = NOVALUE;
        if (IS_ATOM_INT(_31708) && IS_ATOM_INT(_31707)) {
            _31709 = _31708 + _31707;
            if ((long)((unsigned long)_31709 + (unsigned long)HIGH_BITS) >= 0) 
            _31709 = NewDouble((double)_31709);
        }
        else {
            _31709 = binary_op(PLUS, _31708, _31707);
        }
        _31708 = NOVALUE;
        DeRef(_31707);
        _31707 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _31709;
        if( _1 != _31709 ){
            DeRef(_1);
        }
        _31709 = NOVALUE;
        _31705 = NOVALUE;

        /** 		end for*/
        _0 = _i_62963;
        if (IS_ATOM_INT(_i_62963)) {
            _i_62963 = _i_62963 + 1;
            if ((long)((unsigned long)_i_62963 +(unsigned long) HIGH_BITS) >= 0){
                _i_62963 = NewDouble((double)_i_62963);
            }
        }
        else {
            _i_62963 = binary_op_a(PLUS, _i_62963, 1);
        }
        DeRef(_0);
        goto L13; // [1153] 1108
L14: 
        ;
        DeRef(_i_62963);
    }
    goto L15; // [1158] 1250
L12: 

    /** 		for i = pre_refs + 1 to length( active_references[fr[FR_FILE]][ar_sp] ) do*/
    _31710 = _pre_refs_62869 + 1;
    if (_31710 > MAXINT){
        _31710 = NewDouble((double)_31710);
    }
    _2 = (int)SEQ_PTR(_fr_62725);
    _31711 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_40active_references_62505);
    if (!IS_ATOM_INT(_31711)){
        _31712 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31711)->dbl));
    }
    else{
        _31712 = (int)*(((s1_ptr)_2)->base + _31711);
    }
    _2 = (int)SEQ_PTR(_31712);
    _31713 = (int)*(((s1_ptr)_2)->base + _ar_sp_62865);
    _31712 = NOVALUE;
    if (IS_SEQUENCE(_31713)){
            _31714 = SEQ_PTR(_31713)->length;
    }
    else {
        _31714 = 1;
    }
    _31713 = NOVALUE;
    {
        int _i_62978;
        Ref(_31710);
        _i_62978 = _31710;
L16: 
        if (binary_op_a(GREATER, _i_62978, _31714)){
            goto L17; // [1188] 1249
        }

        /** 			forward_references[active_references[fr[FR_FILE]][ar_sp][i]][FR_PC] += pc - 1*/
        _2 = (int)SEQ_PTR(_fr_62725);
        _31715 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_40active_references_62505);
        if (!IS_ATOM_INT(_31715)){
            _31716 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31715)->dbl));
        }
        else{
            _31716 = (int)*(((s1_ptr)_2)->base + _31715);
        }
        _2 = (int)SEQ_PTR(_31716);
        _31717 = (int)*(((s1_ptr)_2)->base + _ar_sp_62865);
        _31716 = NOVALUE;
        _2 = (int)SEQ_PTR(_31717);
        if (!IS_ATOM_INT(_i_62978)){
            _31718 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_62978)->dbl));
        }
        else{
            _31718 = (int)*(((s1_ptr)_2)->base + _i_62978);
        }
        _31717 = NOVALUE;
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _40forward_references_62503 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31718))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31718)->dbl));
        else
        _3 = (int)(_31718 + ((s1_ptr)_2)->base);
        _31721 = _pc_62784 - 1;
        if ((long)((unsigned long)_31721 +(unsigned long) HIGH_BITS) >= 0){
            _31721 = NewDouble((double)_31721);
        }
        _2 = (int)SEQ_PTR(*(int *)_3);
        _31722 = (int)*(((s1_ptr)_2)->base + 5);
        _31719 = NOVALUE;
        if (IS_ATOM_INT(_31722) && IS_ATOM_INT(_31721)) {
            _31723 = _31722 + _31721;
            if ((long)((unsigned long)_31723 + (unsigned long)HIGH_BITS) >= 0) 
            _31723 = NewDouble((double)_31723);
        }
        else {
            _31723 = binary_op(PLUS, _31722, _31721);
        }
        _31722 = NOVALUE;
        DeRef(_31721);
        _31721 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _31723;
        if( _1 != _31723 ){
            DeRef(_1);
        }
        _31723 = NOVALUE;
        _31719 = NOVALUE;

        /** 		end for*/
        _0 = _i_62978;
        if (IS_ATOM_INT(_i_62978)) {
            _i_62978 = _i_62978 + 1;
            if ((long)((unsigned long)_i_62978 +(unsigned long) HIGH_BITS) >= 0){
                _i_62978 = NewDouble((double)_i_62978);
            }
        }
        else {
            _i_62978 = binary_op_a(PLUS, _i_62978, 1);
        }
        DeRef(_0);
        goto L16; // [1244] 1195
L17: 
        ;
        DeRef(_i_62978);
    }
L15: 

    /** 	reset_code()*/
    _40reset_code();

    /** 	resolved_reference( ref )*/
    _40resolved_reference(_ref_62724);

    /** end procedure*/
    DeRef(_tok_62723);
    DeRef(_fr_62725);
    DeRef(_code_62780);
    DeRef(_name_62790);
    DeRef(_params_62859);
    DeRef(_orig_code_62861);
    DeRef(_orig_linetable_62862);
    DeRef(_old_fwd_params_62884);
    DeRef(_new_code_62929);
    DeRef(_31660);
    _31660 = NOVALUE;
    _31711 = NOVALUE;
    DeRef(_31710);
    _31710 = NOVALUE;
    DeRef(_31666);
    _31666 = NOVALUE;
    _31699 = NOVALUE;
    DeRef(_31669);
    _31669 = NOVALUE;
    _31704 = NOVALUE;
    _31702 = NOVALUE;
    DeRef(_31617);
    _31617 = NOVALUE;
    _31715 = NOVALUE;
    DeRef(_31613);
    _31613 = NOVALUE;
    DeRef(_31644);
    _31644 = NOVALUE;
    _31658 = NOVALUE;
    DeRef(_31698);
    _31698 = NOVALUE;
    DeRef(_31662);
    _31662 = NOVALUE;
    _31653 = NOVALUE;
    DeRef(_31634);
    _31634 = NOVALUE;
    _31713 = NOVALUE;
    DeRef(_31632);
    _31632 = NOVALUE;
    _31700 = NOVALUE;
    _31718 = NOVALUE;
    return;
    ;
}


void _40set_error_info(int _ref_62995)
{
    int _fr_62996 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_62996);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_62996 = (int)*(((s1_ptr)_2)->base + _ref_62995);
    Ref(_fr_62996);

    /** 	ThisLine        = fr[FR_THISLINE]*/
    DeRef(_46ThisLine_49482);
    _2 = (int)SEQ_PTR(_fr_62996);
    _46ThisLine_49482 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_46ThisLine_49482);

    /** 	bp              = fr[FR_BP]*/
    _2 = (int)SEQ_PTR(_fr_62996);
    _46bp_49486 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_46bp_49486)){
        _46bp_49486 = (long)DBL_PTR(_46bp_49486)->dbl;
    }

    /** 	line_number     = fr[FR_LINE]*/
    _2 = (int)SEQ_PTR(_fr_62996);
    _38line_number_16947 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_38line_number_16947)){
        _38line_number_16947 = (long)DBL_PTR(_38line_number_16947)->dbl;
    }

    /** 	current_file_no = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_62996);
    _38current_file_no_16946 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_38current_file_no_16946)){
        _38current_file_no_16946 = (long)DBL_PTR(_38current_file_no_16946)->dbl;
    }

    /** end procedure*/
    DeRefDS(_fr_62996);
    return;
    ;
}


void _40patch_forward_variable(int _tok_63009, int _ref_63010)
{
    int _fr_63011 = NOVALUE;
    int _sym_63014 = NOVALUE;
    int _pc_63066 = NOVALUE;
    int _vx_63070 = NOVALUE;
    int _d_63087 = NOVALUE;
    int _param_63097 = NOVALUE;
    int _old_63100 = NOVALUE;
    int _new_63105 = NOVALUE;
    int _31780 = NOVALUE;
    int _31779 = NOVALUE;
    int _31778 = NOVALUE;
    int _31776 = NOVALUE;
    int _31773 = NOVALUE;
    int _31771 = NOVALUE;
    int _31770 = NOVALUE;
    int _31769 = NOVALUE;
    int _31768 = NOVALUE;
    int _31766 = NOVALUE;
    int _31765 = NOVALUE;
    int _31764 = NOVALUE;
    int _31763 = NOVALUE;
    int _31762 = NOVALUE;
    int _31760 = NOVALUE;
    int _31758 = NOVALUE;
    int _31755 = NOVALUE;
    int _31754 = NOVALUE;
    int _31753 = NOVALUE;
    int _31751 = NOVALUE;
    int _31750 = NOVALUE;
    int _31749 = NOVALUE;
    int _31748 = NOVALUE;
    int _31746 = NOVALUE;
    int _31744 = NOVALUE;
    int _31743 = NOVALUE;
    int _31742 = NOVALUE;
    int _31741 = NOVALUE;
    int _31740 = NOVALUE;
    int _31739 = NOVALUE;
    int _31738 = NOVALUE;
    int _31737 = NOVALUE;
    int _31736 = NOVALUE;
    int _31735 = NOVALUE;
    int _31734 = NOVALUE;
    int _31733 = NOVALUE;
    int _31732 = NOVALUE;
    int _31731 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63011);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_63011 = (int)*(((s1_ptr)_2)->base + _ref_63010);
    Ref(_fr_63011);

    /** 	symtab_index sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63009);
    _sym_63014 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_63014)){
        _sym_63014 = (long)DBL_PTR(_sym_63014)->dbl;
    }

    /** 	if SymTab[sym][S_FILE_NO] = fr[FR_FILE] */
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31731 = (int)*(((s1_ptr)_2)->base + _sym_63014);
    _2 = (int)SEQ_PTR(_31731);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _31732 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _31732 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _31731 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_63011);
    _31733 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_31732) && IS_ATOM_INT(_31733)) {
        _31734 = (_31732 == _31733);
    }
    else {
        _31734 = binary_op(EQUALS, _31732, _31733);
    }
    _31732 = NOVALUE;
    _31733 = NOVALUE;
    if (IS_ATOM_INT(_31734)) {
        if (_31734 == 0) {
            goto L1; // [47] 73
        }
    }
    else {
        if (DBL_PTR(_31734)->dbl == 0.0) {
            goto L1; // [47] 73
        }
    }
    _2 = (int)SEQ_PTR(_fr_63011);
    _31736 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_31736)) {
        _31737 = (_31736 == _38TopLevelSub_16953);
    }
    else {
        _31737 = binary_op(EQUALS, _31736, _38TopLevelSub_16953);
    }
    _31736 = NOVALUE;
    if (_31737 == 0) {
        DeRef(_31737);
        _31737 = NOVALUE;
        goto L1; // [64] 73
    }
    else {
        if (!IS_ATOM_INT(_31737) && DBL_PTR(_31737)->dbl == 0.0){
            DeRef(_31737);
            _31737 = NOVALUE;
            goto L1; // [64] 73
        }
        DeRef(_31737);
        _31737 = NOVALUE;
    }
    DeRef(_31737);
    _31737 = NOVALUE;

    /** 		return*/
    DeRef(_tok_63009);
    DeRef(_fr_63011);
    DeRef(_31734);
    _31734 = NOVALUE;
    return;
L1: 

    /** 	if fr[FR_OP] = ASSIGN and SymTab[sym][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_fr_63011);
    _31738 = (int)*(((s1_ptr)_2)->base + 10);
    if (IS_ATOM_INT(_31738)) {
        _31739 = (_31738 == 18);
    }
    else {
        _31739 = binary_op(EQUALS, _31738, 18);
    }
    _31738 = NOVALUE;
    if (IS_ATOM_INT(_31739)) {
        if (_31739 == 0) {
            goto L2; // [87] 126
        }
    }
    else {
        if (DBL_PTR(_31739)->dbl == 0.0) {
            goto L2; // [87] 126
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31741 = (int)*(((s1_ptr)_2)->base + _sym_63014);
    _2 = (int)SEQ_PTR(_31741);
    _31742 = (int)*(((s1_ptr)_2)->base + 3);
    _31741 = NOVALUE;
    if (IS_ATOM_INT(_31742)) {
        _31743 = (_31742 == 2);
    }
    else {
        _31743 = binary_op(EQUALS, _31742, 2);
    }
    _31742 = NOVALUE;
    if (_31743 == 0) {
        DeRef(_31743);
        _31743 = NOVALUE;
        goto L2; // [110] 126
    }
    else {
        if (!IS_ATOM_INT(_31743) && DBL_PTR(_31743)->dbl == 0.0){
            DeRef(_31743);
            _31743 = NOVALUE;
            goto L2; // [110] 126
        }
        DeRef(_31743);
        _31743 = NOVALUE;
    }
    DeRef(_31743);
    _31743 = NOVALUE;

    /** 		prep_forward_error( ref )*/
    _40prep_forward_error(_ref_63010);

    /** 		CompileErr( 110 )*/
    RefDS(_22663);
    _46CompileErr(110, _22663, 0);
L2: 

    /** 	if fr[FR_OP] = ASSIGN then*/
    _2 = (int)SEQ_PTR(_fr_63011);
    _31744 = (int)*(((s1_ptr)_2)->base + 10);
    if (binary_op_a(NOTEQ, _31744, 18)){
        _31744 = NOVALUE;
        goto L3; // [136] 176
    }
    _31744 = NOVALUE;

    /** 		SymTab[sym][S_USAGE] = or_bits( U_WRITTEN, SymTab[sym][S_USAGE] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_63014 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31748 = (int)*(((s1_ptr)_2)->base + _sym_63014);
    _2 = (int)SEQ_PTR(_31748);
    _31749 = (int)*(((s1_ptr)_2)->base + 5);
    _31748 = NOVALUE;
    if (IS_ATOM_INT(_31749)) {
        {unsigned long tu;
             tu = (unsigned long)2 | (unsigned long)_31749;
             _31750 = MAKE_UINT(tu);
        }
    }
    else {
        _31750 = binary_op(OR_BITS, 2, _31749);
    }
    _31749 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _31750;
    if( _1 != _31750 ){
        DeRef(_1);
    }
    _31750 = NOVALUE;
    _31746 = NOVALUE;
    goto L4; // [173] 210
L3: 

    /** 		SymTab[sym][S_USAGE] = or_bits( U_READ, SymTab[sym][S_USAGE] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_63014 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31753 = (int)*(((s1_ptr)_2)->base + _sym_63014);
    _2 = (int)SEQ_PTR(_31753);
    _31754 = (int)*(((s1_ptr)_2)->base + 5);
    _31753 = NOVALUE;
    if (IS_ATOM_INT(_31754)) {
        {unsigned long tu;
             tu = (unsigned long)1 | (unsigned long)_31754;
             _31755 = MAKE_UINT(tu);
        }
    }
    else {
        _31755 = binary_op(OR_BITS, 1, _31754);
    }
    _31754 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _31755;
    if( _1 != _31755 ){
        DeRef(_1);
    }
    _31755 = NOVALUE;
    _31751 = NOVALUE;
L4: 

    /** 	set_code( ref )*/
    _40set_code(_ref_63010);

    /** 	integer pc = fr[FR_PC]*/
    _2 = (int)SEQ_PTR(_fr_63011);
    _pc_63066 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_pc_63066))
    _pc_63066 = (long)DBL_PTR(_pc_63066)->dbl;

    /** 	if pc < 1 then*/
    if (_pc_63066 >= 1)
    goto L5; // [225] 235

    /** 		pc = 1*/
    _pc_63066 = 1;
L5: 

    /** 	integer vx = find( -ref, Code, pc )*/
    if ((unsigned long)_ref_63010 == 0xC0000000)
    _31758 = (int)NewDouble((double)-0xC0000000);
    else
    _31758 = - _ref_63010;
    _vx_63070 = find_from(_31758, _38Code_17038, _pc_63066);
    DeRef(_31758);
    _31758 = NOVALUE;

    /** 	if vx then*/
    if (_vx_63070 == 0)
    {
        goto L6; // [249] 291
    }
    else{
    }

    /** 		while vx do*/
L7: 
    if (_vx_63070 == 0)
    {
        goto L8; // [257] 285
    }
    else{
    }

    /** 			Code[vx] = sym*/
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _vx_63070);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_63014;
    DeRef(_1);

    /** 			vx = find( -ref, Code, vx )*/
    if ((unsigned long)_ref_63010 == 0xC0000000)
    _31760 = (int)NewDouble((double)-0xC0000000);
    else
    _31760 = - _ref_63010;
    _vx_63070 = find_from(_31760, _38Code_17038, _vx_63070);
    DeRef(_31760);
    _31760 = NOVALUE;

    /** 		end while*/
    goto L7; // [282] 257
L8: 

    /** 		resolved_reference( ref )*/
    _40resolved_reference(_ref_63010);
L6: 

    /** 	if sequence( fr[FR_DATA] ) then*/
    _2 = (int)SEQ_PTR(_fr_63011);
    _31762 = (int)*(((s1_ptr)_2)->base + 12);
    _31763 = IS_SEQUENCE(_31762);
    _31762 = NOVALUE;
    if (_31763 == 0)
    {
        _31763 = NOVALUE;
        goto L9; // [302] 438
    }
    else{
        _31763 = NOVALUE;
    }

    /** 		for i = 1 to length( fr[FR_DATA] ) do*/
    _2 = (int)SEQ_PTR(_fr_63011);
    _31764 = (int)*(((s1_ptr)_2)->base + 12);
    if (IS_SEQUENCE(_31764)){
            _31765 = SEQ_PTR(_31764)->length;
    }
    else {
        _31765 = 1;
    }
    _31764 = NOVALUE;
    {
        int _i_63084;
        _i_63084 = 1;
LA: 
        if (_i_63084 > _31765){
            goto LB; // [316] 432
        }

        /** 			object d = fr[FR_DATA][i]*/
        _2 = (int)SEQ_PTR(_fr_63011);
        _31766 = (int)*(((s1_ptr)_2)->base + 12);
        DeRef(_d_63087);
        _2 = (int)SEQ_PTR(_31766);
        _d_63087 = (int)*(((s1_ptr)_2)->base + _i_63084);
        Ref(_d_63087);
        _31766 = NOVALUE;

        /** 			if sequence( d ) and d[1] = PAM_RECORD then*/
        _31768 = IS_SEQUENCE(_d_63087);
        if (_31768 == 0) {
            goto LC; // [340] 421
        }
        _2 = (int)SEQ_PTR(_d_63087);
        _31770 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_31770)) {
            _31771 = (_31770 == 1);
        }
        else {
            _31771 = binary_op(EQUALS, _31770, 1);
        }
        _31770 = NOVALUE;
        if (_31771 == 0) {
            DeRef(_31771);
            _31771 = NOVALUE;
            goto LC; // [355] 421
        }
        else {
            if (!IS_ATOM_INT(_31771) && DBL_PTR(_31771)->dbl == 0.0){
                DeRef(_31771);
                _31771 = NOVALUE;
                goto LC; // [355] 421
            }
            DeRef(_31771);
            _31771 = NOVALUE;
        }
        DeRef(_31771);
        _31771 = NOVALUE;

        /** 				symtab_index param = d[2]*/
        _2 = (int)SEQ_PTR(_d_63087);
        _param_63097 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_63097)){
            _param_63097 = (long)DBL_PTR(_param_63097)->dbl;
        }

        /** 				token old = {RECORDED, d[3]}*/
        _2 = (int)SEQ_PTR(_d_63087);
        _31773 = (int)*(((s1_ptr)_2)->base + 3);
        Ref(_31773);
        DeRef(_old_63100);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 508;
        ((int *)_2)[2] = _31773;
        _old_63100 = MAKE_SEQ(_1);
        _31773 = NOVALUE;

        /** 				token new = {VARIABLE, sym}*/
        DeRefi(_new_63105);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -100;
        ((int *)_2)[2] = _sym_63014;
        _new_63105 = MAKE_SEQ(_1);

        /** 				SymTab[param][S_CODE] = find_replace( old, SymTab[param][S_CODE], new )*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_param_63097 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _31778 = (int)*(((s1_ptr)_2)->base + _param_63097);
        _2 = (int)SEQ_PTR(_31778);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _31779 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _31779 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        _31778 = NOVALUE;
        RefDS(_old_63100);
        Ref(_31779);
        RefDS(_new_63105);
        _31780 = _12find_replace(_old_63100, _31779, _new_63105, 0);
        _31779 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_38S_CODE_16610))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
        _1 = *(int *)_2;
        *(int *)_2 = _31780;
        if( _1 != _31780 ){
            DeRef(_1);
        }
        _31780 = NOVALUE;
        _31776 = NOVALUE;
LC: 
        DeRef(_old_63100);
        _old_63100 = NOVALUE;
        DeRefi(_new_63105);
        _new_63105 = NOVALUE;
        DeRef(_d_63087);
        _d_63087 = NOVALUE;

        /** 		end for*/
        _i_63084 = _i_63084 + 1;
        goto LA; // [427] 323
LB: 
        ;
    }

    /** 		resolved_reference( ref )*/
    _40resolved_reference(_ref_63010);
L9: 

    /** 	reset_code()*/
    _40reset_code();

    /** end procedure*/
    DeRef(_tok_63009);
    DeRef(_fr_63011);
    _31764 = NOVALUE;
    DeRef(_31739);
    _31739 = NOVALUE;
    DeRef(_31734);
    _31734 = NOVALUE;
    return;
    ;
}


void _40patch_forward_init_check(int _tok_63121, int _ref_63122)
{
    int _fr_63123 = NOVALUE;
    int _31788 = NOVALUE;
    int _31787 = NOVALUE;
    int _31786 = NOVALUE;
    int _31784 = NOVALUE;
    int _31783 = NOVALUE;
    int _31782 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63123);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_63123 = (int)*(((s1_ptr)_2)->base + _ref_63122);
    Ref(_fr_63123);

    /** 	set_code( ref )*/
    _40set_code(_ref_63122);

    /** 	if sequence( fr[FR_DATA] ) then*/
    _2 = (int)SEQ_PTR(_fr_63123);
    _31782 = (int)*(((s1_ptr)_2)->base + 12);
    _31783 = IS_SEQUENCE(_31782);
    _31782 = NOVALUE;
    if (_31783 == 0)
    {
        _31783 = NOVALUE;
        goto L1; // [29] 40
    }
    else{
        _31783 = NOVALUE;
    }

    /** 		resolved_reference( ref )*/
    _40resolved_reference(_ref_63122);
    goto L2; // [37] 91
L1: 

    /** 	elsif fr[FR_PC] > 0 then*/
    _2 = (int)SEQ_PTR(_fr_63123);
    _31784 = (int)*(((s1_ptr)_2)->base + 5);
    if (binary_op_a(LESSEQ, _31784, 0)){
        _31784 = NOVALUE;
        goto L3; // [48] 84
    }
    _31784 = NOVALUE;

    /** 		Code[fr[FR_PC]+1] = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_fr_63123);
    _31786 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_31786)) {
        _31787 = _31786 + 1;
        if (_31787 > MAXINT){
            _31787 = NewDouble((double)_31787);
        }
    }
    else
    _31787 = binary_op(PLUS, 1, _31786);
    _31786 = NOVALUE;
    _2 = (int)SEQ_PTR(_tok_63121);
    _31788 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31788);
    _2 = (int)SEQ_PTR(_38Code_17038);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38Code_17038 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31787))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31787)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _31787);
    _1 = *(int *)_2;
    *(int *)_2 = _31788;
    if( _1 != _31788 ){
        DeRef(_1);
    }
    _31788 = NOVALUE;

    /** 		resolved_reference( ref )*/
    _40resolved_reference(_ref_63122);
    goto L2; // [81] 91
L3: 

    /** 		forward_error( tok, ref )*/
    Ref(_tok_63121);
    _40forward_error(_tok_63121, _ref_63122);
L2: 

    /** 	reset_code()*/
    _40reset_code();

    /** end procedure*/
    DeRef(_tok_63121);
    DeRef(_fr_63123);
    DeRef(_31787);
    _31787 = NOVALUE;
    return;
    ;
}


int _40expected_name(int _id_63140)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_id_63140)) {
        _1 = (long)(DBL_PTR(_id_63140)->dbl);
        if (UNIQUE(DBL_PTR(_id_63140)) && (DBL_PTR(_id_63140)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_id_63140);
        _id_63140 = _1;
    }

    /** 	switch id with fallthru do*/
    _0 = _id_63140;
    switch ( _0 ){ 

        /** 		case PROC then*/
        case 27:
        case 195:

        /** 			return "a procedure"*/
        RefDS(_27081);
        return _27081;

        /** 		case FUNC then*/
        case 501:
        case 196:

        /** 			return "a function"*/
        RefDS(_27027);
        return _27027;

        /** 		case VARIABLE then*/
        case -100:

        /** 			return "a variable, constant or enum"*/
        RefDS(_31791);
        return _31791;

        /** 		case else*/
        default:

        /** 			return "something"*/
        RefDS(_31792);
        return _31792;
    ;}    ;
}


void _40patch_forward_type(int _tok_63157, int _ref_63158)
{
    int _fr_63159 = NOVALUE;
    int _syms_63161 = NOVALUE;
    int _31804 = NOVALUE;
    int _31803 = NOVALUE;
    int _31801 = NOVALUE;
    int _31800 = NOVALUE;
    int _31799 = NOVALUE;
    int _31797 = NOVALUE;
    int _31796 = NOVALUE;
    int _31795 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63159);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_63159 = (int)*(((s1_ptr)_2)->base + _ref_63158);
    Ref(_fr_63159);

    /** 	sequence syms = fr[FR_DATA]*/
    DeRef(_syms_63161);
    _2 = (int)SEQ_PTR(_fr_63159);
    _syms_63161 = (int)*(((s1_ptr)_2)->base + 12);
    Ref(_syms_63161);

    /** 	for i = 2 to length( syms ) do*/
    if (IS_SEQUENCE(_syms_63161)){
            _31795 = SEQ_PTR(_syms_63161)->length;
    }
    else {
        _31795 = 1;
    }
    {
        int _i_63164;
        _i_63164 = 2;
L1: 
        if (_i_63164 > _31795){
            goto L2; // [28] 104
        }

        /** 		SymTab[syms[i]][S_VTYPE] = tok[T_SYM]*/
        _2 = (int)SEQ_PTR(_syms_63161);
        _31796 = (int)*(((s1_ptr)_2)->base + _i_63164);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31796))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31796)->dbl));
        else
        _3 = (int)(_31796 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_tok_63157);
        _31799 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31799);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 15);
        _1 = *(int *)_2;
        *(int *)_2 = _31799;
        if( _1 != _31799 ){
            DeRef(_1);
        }
        _31799 = NOVALUE;
        _31797 = NOVALUE;

        /** 		if TRANSLATE then*/
        if (_38TRANSLATE_16564 == 0)
        {
            goto L3; // [64] 97
        }
        else{
        }

        /** 			SymTab[syms[i]][S_GTYPE] = CompileType(tok[T_SYM])*/
        _2 = (int)SEQ_PTR(_syms_63161);
        _31800 = (int)*(((s1_ptr)_2)->base + _i_63164);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_31800))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31800)->dbl));
        else
        _3 = (int)(_31800 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_tok_63157);
        _31803 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_31803);
        _31804 = _41CompileType(_31803);
        _31803 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 36);
        _1 = *(int *)_2;
        *(int *)_2 = _31804;
        if( _1 != _31804 ){
            DeRef(_1);
        }
        _31804 = NOVALUE;
        _31801 = NOVALUE;
L3: 

        /** 	end for*/
        _i_63164 = _i_63164 + 1;
        goto L1; // [99] 35
L2: 
        ;
    }

    /** 	resolved_reference( ref )*/
    _40resolved_reference(_ref_63158);

    /** end procedure*/
    DeRef(_tok_63157);
    DeRef(_fr_63159);
    DeRef(_syms_63161);
    _31796 = NOVALUE;
    _31800 = NOVALUE;
    return;
    ;
}


void _40patch_forward_case(int _tok_63187, int _ref_63188)
{
    int _fr_63189 = NOVALUE;
    int _switch_pc_63191 = NOVALUE;
    int _case_sym_63194 = NOVALUE;
    int _case_values_63223 = NOVALUE;
    int _cx_63228 = NOVALUE;
    int _negative_63236 = NOVALUE;
    int _31842 = NOVALUE;
    int _31841 = NOVALUE;
    int _31840 = NOVALUE;
    int _31839 = NOVALUE;
    int _31838 = NOVALUE;
    int _31837 = NOVALUE;
    int _31835 = NOVALUE;
    int _31833 = NOVALUE;
    int _31832 = NOVALUE;
    int _31830 = NOVALUE;
    int _31829 = NOVALUE;
    int _31826 = NOVALUE;
    int _31824 = NOVALUE;
    int _31823 = NOVALUE;
    int _31822 = NOVALUE;
    int _31821 = NOVALUE;
    int _31820 = NOVALUE;
    int _31819 = NOVALUE;
    int _31818 = NOVALUE;
    int _31817 = NOVALUE;
    int _31816 = NOVALUE;
    int _31814 = NOVALUE;
    int _31813 = NOVALUE;
    int _31812 = NOVALUE;
    int _31811 = NOVALUE;
    int _31809 = NOVALUE;
    int _31807 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63189);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_63189 = (int)*(((s1_ptr)_2)->base + _ref_63188);
    Ref(_fr_63189);

    /** 	integer switch_pc = fr[FR_DATA]*/
    _2 = (int)SEQ_PTR(_fr_63189);
    _switch_pc_63191 = (int)*(((s1_ptr)_2)->base + 12);
    if (!IS_ATOM_INT(_switch_pc_63191))
    _switch_pc_63191 = (long)DBL_PTR(_switch_pc_63191)->dbl;

    /** 	if fr[FR_SUBPROG] = TopLevelSub then*/
    _2 = (int)SEQ_PTR(_fr_63189);
    _31807 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _31807, _38TopLevelSub_16953)){
        _31807 = NOVALUE;
        goto L1; // [31] 52
    }
    _31807 = NOVALUE;

    /** 		case_sym = Code[switch_pc + 2]*/
    _31809 = _switch_pc_63191 + 2;
    _2 = (int)SEQ_PTR(_38Code_17038);
    _case_sym_63194 = (int)*(((s1_ptr)_2)->base + _31809);
    if (!IS_ATOM_INT(_case_sym_63194)){
        _case_sym_63194 = (long)DBL_PTR(_case_sym_63194)->dbl;
    }
    goto L2; // [49] 83
L1: 

    /** 		case_sym = SymTab[fr[FR_SUBPROG]][S_CODE][switch_pc + 2]*/
    _2 = (int)SEQ_PTR(_fr_63189);
    _31811 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31811)){
        _31812 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31811)->dbl));
    }
    else{
        _31812 = (int)*(((s1_ptr)_2)->base + _31811);
    }
    _2 = (int)SEQ_PTR(_31812);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _31813 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _31813 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    _31812 = NOVALUE;
    _31814 = _switch_pc_63191 + 2;
    _2 = (int)SEQ_PTR(_31813);
    _case_sym_63194 = (int)*(((s1_ptr)_2)->base + _31814);
    if (!IS_ATOM_INT(_case_sym_63194)){
        _case_sym_63194 = (long)DBL_PTR(_case_sym_63194)->dbl;
    }
    _31813 = NOVALUE;
L2: 

    /** 	if SymTab[tok[T_SYM]][S_FILE_NO] = fr[FR_FILE] and fr[FR_SUBPROG] = TopLevelSub then*/
    _2 = (int)SEQ_PTR(_tok_63187);
    _31816 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31816)){
        _31817 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31816)->dbl));
    }
    else{
        _31817 = (int)*(((s1_ptr)_2)->base + _31816);
    }
    _2 = (int)SEQ_PTR(_31817);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _31818 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _31818 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _31817 = NOVALUE;
    _2 = (int)SEQ_PTR(_fr_63189);
    _31819 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_31818) && IS_ATOM_INT(_31819)) {
        _31820 = (_31818 == _31819);
    }
    else {
        _31820 = binary_op(EQUALS, _31818, _31819);
    }
    _31818 = NOVALUE;
    _31819 = NOVALUE;
    if (IS_ATOM_INT(_31820)) {
        if (_31820 == 0) {
            goto L3; // [113] 139
        }
    }
    else {
        if (DBL_PTR(_31820)->dbl == 0.0) {
            goto L3; // [113] 139
        }
    }
    _2 = (int)SEQ_PTR(_fr_63189);
    _31822 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_31822)) {
        _31823 = (_31822 == _38TopLevelSub_16953);
    }
    else {
        _31823 = binary_op(EQUALS, _31822, _38TopLevelSub_16953);
    }
    _31822 = NOVALUE;
    if (_31823 == 0) {
        DeRef(_31823);
        _31823 = NOVALUE;
        goto L3; // [130] 139
    }
    else {
        if (!IS_ATOM_INT(_31823) && DBL_PTR(_31823)->dbl == 0.0){
            DeRef(_31823);
            _31823 = NOVALUE;
            goto L3; // [130] 139
        }
        DeRef(_31823);
        _31823 = NOVALUE;
    }
    DeRef(_31823);
    _31823 = NOVALUE;

    /** 		return*/
    DeRef(_tok_63187);
    DeRef(_fr_63189);
    DeRef(_case_values_63223);
    DeRef(_31809);
    _31809 = NOVALUE;
    _31811 = NOVALUE;
    _31816 = NOVALUE;
    DeRef(_31814);
    _31814 = NOVALUE;
    DeRef(_31820);
    _31820 = NOVALUE;
    return;
L3: 

    /** 	sequence case_values = SymTab[case_sym][S_OBJ]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31824 = (int)*(((s1_ptr)_2)->base + _case_sym_63194);
    DeRef(_case_values_63223);
    _2 = (int)SEQ_PTR(_31824);
    _case_values_63223 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_case_values_63223);
    _31824 = NOVALUE;

    /** 	integer cx = find( { ref }, case_values )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ref_63188;
    _31826 = MAKE_SEQ(_1);
    _cx_63228 = find_from(_31826, _case_values_63223, 1);
    DeRefDS(_31826);
    _31826 = NOVALUE;

    /** 	if not cx then*/
    if (_cx_63228 != 0)
    goto L4; // [170] 188

    /** 		cx = find( { -ref }, case_values )*/
    if ((unsigned long)_ref_63188 == 0xC0000000)
    _31829 = (int)NewDouble((double)-0xC0000000);
    else
    _31829 = - _ref_63188;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _31829;
    _31830 = MAKE_SEQ(_1);
    _31829 = NOVALUE;
    _cx_63228 = find_from(_31830, _case_values_63223, 1);
    DeRefDS(_31830);
    _31830 = NOVALUE;
L4: 

    /**  	ifdef DEBUG then	*/

    /** 	integer negative = 0*/
    _negative_63236 = 0;

    /** 	if case_values[cx][1] < 0 then*/
    _2 = (int)SEQ_PTR(_case_values_63223);
    _31832 = (int)*(((s1_ptr)_2)->base + _cx_63228);
    _2 = (int)SEQ_PTR(_31832);
    _31833 = (int)*(((s1_ptr)_2)->base + 1);
    _31832 = NOVALUE;
    if (binary_op_a(GREATEREQ, _31833, 0)){
        _31833 = NOVALUE;
        goto L5; // [205] 234
    }
    _31833 = NOVALUE;

    /** 		negative = 1*/
    _negative_63236 = 1;

    /** 		case_values[cx][1] *= -1*/
    _2 = (int)SEQ_PTR(_case_values_63223);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _case_values_63223 = MAKE_SEQ(_2);
    }
    _3 = (int)(_cx_63228 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _31837 = (int)*(((s1_ptr)_2)->base + 1);
    _31835 = NOVALUE;
    if (IS_ATOM_INT(_31837)) {
        if (_31837 == (short)_31837)
        _31838 = _31837 * -1;
        else
        _31838 = NewDouble(_31837 * (double)-1);
    }
    else {
        _31838 = binary_op(MULTIPLY, _31837, -1);
    }
    _31837 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _31838;
    if( _1 != _31838 ){
        DeRef(_1);
    }
    _31838 = NOVALUE;
    _31835 = NOVALUE;
L5: 

    /** 	if negative then*/
    if (_negative_63236 == 0)
    {
        goto L6; // [236] 257
    }
    else{
    }

    /** 		case_values[cx] = - tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63187);
    _31839 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_31839)) {
        if ((unsigned long)_31839 == 0xC0000000)
        _31840 = (int)NewDouble((double)-0xC0000000);
        else
        _31840 = - _31839;
    }
    else {
        _31840 = unary_op(UMINUS, _31839);
    }
    _31839 = NOVALUE;
    _2 = (int)SEQ_PTR(_case_values_63223);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _case_values_63223 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _cx_63228);
    _1 = *(int *)_2;
    *(int *)_2 = _31840;
    if( _1 != _31840 ){
        DeRef(_1);
    }
    _31840 = NOVALUE;
    goto L7; // [254] 270
L6: 

    /** 		case_values[cx] = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63187);
    _31841 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_31841);
    _2 = (int)SEQ_PTR(_case_values_63223);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _case_values_63223 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _cx_63228);
    _1 = *(int *)_2;
    *(int *)_2 = _31841;
    if( _1 != _31841 ){
        DeRef(_1);
    }
    _31841 = NOVALUE;
L7: 

    /** 	SymTab[case_sym][S_OBJ] = case_values*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_case_sym_63194 + ((s1_ptr)_2)->base);
    RefDS(_case_values_63223);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _case_values_63223;
    DeRef(_1);
    _31842 = NOVALUE;

    /** 	resolved_reference( ref )*/
    _40resolved_reference(_ref_63188);

    /** end procedure*/
    DeRef(_tok_63187);
    DeRef(_fr_63189);
    DeRefDS(_case_values_63223);
    DeRef(_31809);
    _31809 = NOVALUE;
    _31811 = NOVALUE;
    _31816 = NOVALUE;
    DeRef(_31814);
    _31814 = NOVALUE;
    DeRef(_31820);
    _31820 = NOVALUE;
    return;
    ;
}


void _40patch_forward_type_check(int _tok_63259, int _ref_63260)
{
    int _fr_63261 = NOVALUE;
    int _which_type_63264 = NOVALUE;
    int _var_63266 = NOVALUE;
    int _pc_63299 = NOVALUE;
    int _with_type_check_63301 = NOVALUE;
    int _c_63331 = NOVALUE;
    int _subprog_inlined_insert_code_at_344_63340 = NOVALUE;
    int _code_inlined_insert_code_at_341_63339 = NOVALUE;
    int _subprog_inlined_insert_code_at_429_63356 = NOVALUE;
    int _code_inlined_insert_code_at_426_63355 = NOVALUE;
    int _subprog_inlined_insert_code_at_493_63366 = NOVALUE;
    int _code_inlined_insert_code_at_490_63365 = NOVALUE;
    int _subprog_inlined_insert_code_at_557_63376 = NOVALUE;
    int _code_inlined_insert_code_at_554_63375 = NOVALUE;
    int _start_pc_63383 = NOVALUE;
    int _subprog_inlined_insert_code_at_667_63400 = NOVALUE;
    int _code_inlined_insert_code_at_664_63399 = NOVALUE;
    int _c_63403 = NOVALUE;
    int _subprog_inlined_insert_code_at_765_63419 = NOVALUE;
    int _code_inlined_insert_code_at_762_63418 = NOVALUE;
    int _start_pc_63430 = NOVALUE;
    int _subprog_inlined_insert_code_at_912_63450 = NOVALUE;
    int _code_inlined_insert_code_at_909_63449 = NOVALUE;
    int _subprog_inlined_insert_code_at_1015_63471 = NOVALUE;
    int _code_inlined_insert_code_at_1012_63470 = NOVALUE;
    int _31932 = NOVALUE;
    int _31931 = NOVALUE;
    int _31930 = NOVALUE;
    int _31929 = NOVALUE;
    int _31928 = NOVALUE;
    int _31927 = NOVALUE;
    int _31926 = NOVALUE;
    int _31924 = NOVALUE;
    int _31922 = NOVALUE;
    int _31921 = NOVALUE;
    int _31920 = NOVALUE;
    int _31919 = NOVALUE;
    int _31918 = NOVALUE;
    int _31917 = NOVALUE;
    int _31916 = NOVALUE;
    int _31914 = NOVALUE;
    int _31913 = NOVALUE;
    int _31912 = NOVALUE;
    int _31911 = NOVALUE;
    int _31910 = NOVALUE;
    int _31909 = NOVALUE;
    int _31907 = NOVALUE;
    int _31906 = NOVALUE;
    int _31905 = NOVALUE;
    int _31904 = NOVALUE;
    int _31902 = NOVALUE;
    int _31901 = NOVALUE;
    int _31898 = NOVALUE;
    int _31897 = NOVALUE;
    int _31895 = NOVALUE;
    int _31894 = NOVALUE;
    int _31893 = NOVALUE;
    int _31892 = NOVALUE;
    int _31891 = NOVALUE;
    int _31890 = NOVALUE;
    int _31888 = NOVALUE;
    int _31887 = NOVALUE;
    int _31884 = NOVALUE;
    int _31883 = NOVALUE;
    int _31880 = NOVALUE;
    int _31879 = NOVALUE;
    int _31875 = NOVALUE;
    int _31874 = NOVALUE;
    int _31872 = NOVALUE;
    int _31871 = NOVALUE;
    int _31869 = NOVALUE;
    int _31868 = NOVALUE;
    int _31865 = NOVALUE;
    int _31862 = NOVALUE;
    int _31860 = NOVALUE;
    int _31857 = NOVALUE;
    int _31856 = NOVALUE;
    int _31853 = NOVALUE;
    int _31848 = NOVALUE;
    int _31847 = NOVALUE;
    int _31845 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence fr = forward_references[ref]*/
    DeRef(_fr_63261);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _fr_63261 = (int)*(((s1_ptr)_2)->base + _ref_63260);
    Ref(_fr_63261);

    /** 	if fr[FR_OP] = TYPE_CHECK_FORWARD then*/
    _2 = (int)SEQ_PTR(_fr_63261);
    _31845 = (int)*(((s1_ptr)_2)->base + 10);
    if (binary_op_a(NOTEQ, _31845, 197)){
        _31845 = NOVALUE;
        goto L1; // [23] 88
    }
    _31845 = NOVALUE;

    /** 		which_type = SymTab[tok[T_SYM]][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_tok_63259);
    _31847 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31847)){
        _31848 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31847)->dbl));
    }
    else{
        _31848 = (int)*(((s1_ptr)_2)->base + _31847);
    }
    _2 = (int)SEQ_PTR(_31848);
    _which_type_63264 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_which_type_63264)){
        _which_type_63264 = (long)DBL_PTR(_which_type_63264)->dbl;
    }
    _31848 = NOVALUE;

    /** 		if not which_type then*/
    if (_which_type_63264 != 0)
    goto L2; // [51] 74

    /** 			which_type = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63259);
    _which_type_63264 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_which_type_63264)){
        _which_type_63264 = (long)DBL_PTR(_which_type_63264)->dbl;
    }

    /** 			var = 0*/
    _var_63266 = 0;
    goto L3; // [71] 150
L2: 

    /** 			var = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63259);
    _var_63266 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_var_63266)){
        _var_63266 = (long)DBL_PTR(_var_63266)->dbl;
    }
    goto L3; // [85] 150
L1: 

    /** 	elsif fr[FR_OP] = TYPE then*/
    _2 = (int)SEQ_PTR(_fr_63261);
    _31853 = (int)*(((s1_ptr)_2)->base + 10);
    if (binary_op_a(NOTEQ, _31853, 504)){
        _31853 = NOVALUE;
        goto L4; // [98] 122
    }
    _31853 = NOVALUE;

    /** 		which_type = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_63259);
    _which_type_63264 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_which_type_63264)){
        _which_type_63264 = (long)DBL_PTR(_which_type_63264)->dbl;
    }

    /** 		var = 0*/
    _var_63266 = 0;
    goto L3; // [119] 150
L4: 

    /** 		prep_forward_error( ref )*/
    _40prep_forward_error(_ref_63260);

    /** 		InternalErr( 262, { TYPE_CHECK, TYPE_CHECK_FORWARD, fr[FR_OP] })*/
    _2 = (int)SEQ_PTR(_fr_63261);
    _31856 = (int)*(((s1_ptr)_2)->base + 10);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 65;
    *((int *)(_2+8)) = 197;
    Ref(_31856);
    *((int *)(_2+12)) = _31856;
    _31857 = MAKE_SEQ(_1);
    _31856 = NOVALUE;
    _46InternalErr(262, _31857);
    _31857 = NOVALUE;
L3: 

    /** 	if which_type < 0 then*/
    if (_which_type_63264 >= 0)
    goto L5; // [154] 164

    /** 		return*/
    DeRef(_tok_63259);
    DeRef(_fr_63261);
    _31847 = NOVALUE;
    return;
L5: 

    /** 	set_code( ref )*/
    _40set_code(_ref_63260);

    /** 	integer pc = fr[FR_PC]*/
    _2 = (int)SEQ_PTR(_fr_63261);
    _pc_63299 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_pc_63299))
    _pc_63299 = (long)DBL_PTR(_pc_63299)->dbl;

    /** 	integer with_type_check = Code[pc + 2]*/
    _31860 = _pc_63299 + 2;
    _2 = (int)SEQ_PTR(_38Code_17038);
    _with_type_check_63301 = (int)*(((s1_ptr)_2)->base + _31860);
    if (!IS_ATOM_INT(_with_type_check_63301)){
        _with_type_check_63301 = (long)DBL_PTR(_with_type_check_63301)->dbl;
    }

    /** 	if Code[pc] != TYPE_CHECK_FORWARD then*/
    _2 = (int)SEQ_PTR(_38Code_17038);
    _31862 = (int)*(((s1_ptr)_2)->base + _pc_63299);
    if (binary_op_a(EQUALS, _31862, 197)){
        _31862 = NOVALUE;
        goto L6; // [201] 212
    }
    _31862 = NOVALUE;

    /** 		forward_error( tok, ref )*/
    Ref(_tok_63259);
    _40forward_error(_tok_63259, _ref_63260);
L6: 

    /** 	if not var then*/
    if (_var_63266 != 0)
    goto L7; // [216] 234

    /** 		var = Code[pc+1]*/
    _31865 = _pc_63299 + 1;
    _2 = (int)SEQ_PTR(_38Code_17038);
    _var_63266 = (int)*(((s1_ptr)_2)->base + _31865);
    if (!IS_ATOM_INT(_var_63266)){
        _var_63266 = (long)DBL_PTR(_var_63266)->dbl;
    }
L7: 

    /** 	if var < 0 then*/
    if (_var_63266 >= 0)
    goto L8; // [236] 246

    /** 		return*/
    DeRef(_tok_63259);
    DeRef(_fr_63261);
    _31847 = NOVALUE;
    DeRef(_31860);
    _31860 = NOVALUE;
    DeRef(_31865);
    _31865 = NOVALUE;
    return;
L8: 

    /** 	replace_code( {}, pc, pc + 2, fr[FR_SUBPROG])*/
    _31868 = _pc_63299 + 2;
    if ((long)((unsigned long)_31868 + (unsigned long)HIGH_BITS) >= 0) 
    _31868 = NewDouble((double)_31868);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31869 = (int)*(((s1_ptr)_2)->base + 4);
    RefDS(_22663);
    Ref(_31869);
    _40replace_code(_22663, _pc_63299, _31868, _31869);
    _31868 = NOVALUE;
    _31869 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L9; // [268] 376
    }
    else{
    }

    /** 		if with_type_check then*/
    if (_with_type_check_63301 == 0)
    {
        goto LA; // [273] 795
    }
    else{
    }

    /** 			if which_type != object_type then*/
    if (_which_type_63264 == _55object_type_47055)
    goto LA; // [280] 795

    /** 				if SymTab[which_type][S_EFFECT] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31871 = (int)*(((s1_ptr)_2)->base + _which_type_63264);
    _2 = (int)SEQ_PTR(_31871);
    _31872 = (int)*(((s1_ptr)_2)->base + 23);
    _31871 = NOVALUE;
    if (_31872 == 0) {
        _31872 = NOVALUE;
        goto LB; // [298] 369
    }
    else {
        if (!IS_ATOM_INT(_31872) && DBL_PTR(_31872)->dbl == 0.0){
            _31872 = NOVALUE;
            goto LB; // [298] 369
        }
        _31872 = NOVALUE;
    }
    _31872 = NOVALUE;

    /** 					integer c = NewTempSym()*/
    _c_63331 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_c_63331)) {
        _1 = (long)(DBL_PTR(_c_63331)->dbl);
        if (UNIQUE(DBL_PTR(_c_63331)) && (DBL_PTR(_c_63331)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_63331);
        _c_63331 = _1;
    }

    /** 					insert_code( { PROC, which_type, var, c, TYPE_CHECK }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 27;
    *((int *)(_2+8)) = _which_type_63264;
    *((int *)(_2+12)) = _var_63266;
    *((int *)(_2+16)) = _c_63331;
    *((int *)(_2+20)) = 65;
    _31874 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31875 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_341_63339);
    _code_inlined_insert_code_at_341_63339 = _31874;
    _31874 = NOVALUE;
    Ref(_31875);
    DeRef(_subprog_inlined_insert_code_at_344_63340);
    _subprog_inlined_insert_code_at_344_63340 = _31875;
    _31875 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_344_63340)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_344_63340)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_344_63340)) && (DBL_PTR(_subprog_inlined_insert_code_at_344_63340)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_344_63340);
        _subprog_inlined_insert_code_at_344_63340 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_344_63340;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_341_63339);
    _67insert_code(_code_inlined_insert_code_at_341_63339, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto LC; // [357] 360
LC: 
    DeRefi(_code_inlined_insert_code_at_341_63339);
    _code_inlined_insert_code_at_341_63339 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_344_63340);
    _subprog_inlined_insert_code_at_344_63340 = NOVALUE;

    /** 					pc += 5*/
    _pc_63299 = _pc_63299 + 5;
LB: 
    goto LA; // [373] 795
L9: 

    /** 		if with_type_check then*/
    if (_with_type_check_63301 == 0)
    {
        goto LD; // [378] 794
    }
    else{
    }

    /** 			if which_type = object_type then*/
    if (_which_type_63264 != _55object_type_47055)
    goto LE; // [385] 392
    goto LF; // [389] 793
LE: 

    /** 				if which_type = integer_type then*/
    if (_which_type_63264 != _55integer_type_47061)
    goto L10; // [396] 456

    /** 					insert_code( { INTEGER_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 96;
    ((int *)_2)[2] = _var_63266;
    _31879 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31880 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_426_63355);
    _code_inlined_insert_code_at_426_63355 = _31879;
    _31879 = NOVALUE;
    Ref(_31880);
    DeRef(_subprog_inlined_insert_code_at_429_63356);
    _subprog_inlined_insert_code_at_429_63356 = _31880;
    _31880 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_429_63356)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_429_63356)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_429_63356)) && (DBL_PTR(_subprog_inlined_insert_code_at_429_63356)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_429_63356);
        _subprog_inlined_insert_code_at_429_63356 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_429_63356;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_426_63355);
    _67insert_code(_code_inlined_insert_code_at_426_63355, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto L11; // [442] 445
L11: 
    DeRefi(_code_inlined_insert_code_at_426_63355);
    _code_inlined_insert_code_at_426_63355 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_429_63356);
    _subprog_inlined_insert_code_at_429_63356 = NOVALUE;

    /** 					pc += 2*/
    _pc_63299 = _pc_63299 + 2;
    goto L12; // [453] 792
L10: 

    /** 				elsif which_type = sequence_type then*/
    if (_which_type_63264 != _55sequence_type_47059)
    goto L13; // [460] 520

    /** 					insert_code( { SEQUENCE_CHECK, var }, pc, fr[FR_SUBPROG])*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 97;
    ((int *)_2)[2] = _var_63266;
    _31883 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31884 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_490_63365);
    _code_inlined_insert_code_at_490_63365 = _31883;
    _31883 = NOVALUE;
    Ref(_31884);
    DeRef(_subprog_inlined_insert_code_at_493_63366);
    _subprog_inlined_insert_code_at_493_63366 = _31884;
    _31884 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_493_63366)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_493_63366)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_493_63366)) && (DBL_PTR(_subprog_inlined_insert_code_at_493_63366)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_493_63366);
        _subprog_inlined_insert_code_at_493_63366 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_493_63366;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_490_63365);
    _67insert_code(_code_inlined_insert_code_at_490_63365, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto L14; // [506] 509
L14: 
    DeRefi(_code_inlined_insert_code_at_490_63365);
    _code_inlined_insert_code_at_490_63365 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_493_63366);
    _subprog_inlined_insert_code_at_493_63366 = NOVALUE;

    /** 					pc += 2*/
    _pc_63299 = _pc_63299 + 2;
    goto L12; // [517] 792
L13: 

    /** 				elsif which_type = atom_type then*/
    if (_which_type_63264 != _55atom_type_47057)
    goto L15; // [524] 584

    /** 					insert_code( { ATOM_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 101;
    ((int *)_2)[2] = _var_63266;
    _31887 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31888 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_554_63375);
    _code_inlined_insert_code_at_554_63375 = _31887;
    _31887 = NOVALUE;
    Ref(_31888);
    DeRef(_subprog_inlined_insert_code_at_557_63376);
    _subprog_inlined_insert_code_at_557_63376 = _31888;
    _31888 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_557_63376)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_557_63376)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_557_63376)) && (DBL_PTR(_subprog_inlined_insert_code_at_557_63376)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_557_63376);
        _subprog_inlined_insert_code_at_557_63376 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_557_63376;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_554_63375);
    _67insert_code(_code_inlined_insert_code_at_554_63375, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto L16; // [570] 573
L16: 
    DeRefi(_code_inlined_insert_code_at_554_63375);
    _code_inlined_insert_code_at_554_63375 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_557_63376);
    _subprog_inlined_insert_code_at_557_63376 = NOVALUE;

    /** 					pc += 2*/
    _pc_63299 = _pc_63299 + 2;
    goto L12; // [581] 792
L15: 

    /** 				elsif SymTab[which_type][S_NEXT] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31890 = (int)*(((s1_ptr)_2)->base + _which_type_63264);
    _2 = (int)SEQ_PTR(_31890);
    _31891 = (int)*(((s1_ptr)_2)->base + 2);
    _31890 = NOVALUE;
    if (_31891 == 0) {
        _31891 = NOVALUE;
        goto L17; // [598] 789
    }
    else {
        if (!IS_ATOM_INT(_31891) && DBL_PTR(_31891)->dbl == 0.0){
            _31891 = NOVALUE;
            goto L17; // [598] 789
        }
        _31891 = NOVALUE;
    }
    _31891 = NOVALUE;

    /** 					integer start_pc = pc*/
    _start_pc_63383 = _pc_63299;

    /** 					if SymTab[SymTab[which_type][S_NEXT]][S_VTYPE] = integer_type then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31892 = (int)*(((s1_ptr)_2)->base + _which_type_63264);
    _2 = (int)SEQ_PTR(_31892);
    _31893 = (int)*(((s1_ptr)_2)->base + 2);
    _31892 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31893)){
        _31894 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31893)->dbl));
    }
    else{
        _31894 = (int)*(((s1_ptr)_2)->base + _31893);
    }
    _2 = (int)SEQ_PTR(_31894);
    _31895 = (int)*(((s1_ptr)_2)->base + 15);
    _31894 = NOVALUE;
    if (binary_op_a(NOTEQ, _31895, _55integer_type_47061)){
        _31895 = NOVALUE;
        goto L18; // [634] 692
    }
    _31895 = NOVALUE;

    /** 						insert_code( { INTEGER_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 96;
    ((int *)_2)[2] = _var_63266;
    _31897 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31898 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_664_63399);
    _code_inlined_insert_code_at_664_63399 = _31897;
    _31897 = NOVALUE;
    Ref(_31898);
    DeRef(_subprog_inlined_insert_code_at_667_63400);
    _subprog_inlined_insert_code_at_667_63400 = _31898;
    _31898 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_667_63400)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_667_63400)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_667_63400)) && (DBL_PTR(_subprog_inlined_insert_code_at_667_63400)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_667_63400);
        _subprog_inlined_insert_code_at_667_63400 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_667_63400;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_664_63399);
    _67insert_code(_code_inlined_insert_code_at_664_63399, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto L19; // [680] 683
L19: 
    DeRefi(_code_inlined_insert_code_at_664_63399);
    _code_inlined_insert_code_at_664_63399 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_667_63400);
    _subprog_inlined_insert_code_at_667_63400 = NOVALUE;

    /** 						pc += 2*/
    _pc_63299 = _pc_63299 + 2;
L18: 

    /** 					symtab_index c = NewTempSym()*/
    _c_63403 = _55NewTempSym(0);
    if (!IS_ATOM_INT(_c_63403)) {
        _1 = (long)(DBL_PTR(_c_63403)->dbl);
        if (UNIQUE(DBL_PTR(_c_63403)) && (DBL_PTR(_c_63403)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_63403);
        _c_63403 = _1;
    }

    /** 					SymTab[fr[FR_SUBPROG]][S_STACK_SPACE] += 1*/
    _2 = (int)SEQ_PTR(_fr_63261);
    _31901 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_31901))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_31901)->dbl));
    else
    _3 = (int)(_31901 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658)){
        _31904 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    }
    else{
        _31904 = (int)*(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    }
    _31902 = NOVALUE;
    if (IS_ATOM_INT(_31904)) {
        _31905 = _31904 + 1;
        if (_31905 > MAXINT){
            _31905 = NewDouble((double)_31905);
        }
    }
    else
    _31905 = binary_op(PLUS, 1, _31904);
    _31904 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    _1 = *(int *)_2;
    *(int *)_2 = _31905;
    if( _1 != _31905 ){
        DeRef(_1);
    }
    _31905 = NOVALUE;
    _31902 = NOVALUE;

    /** 					insert_code( { PROC, which_type, var, c, TYPE_CHECK }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 27;
    *((int *)(_2+8)) = _which_type_63264;
    *((int *)(_2+12)) = _var_63266;
    *((int *)(_2+16)) = _c_63403;
    *((int *)(_2+20)) = 65;
    _31906 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31907 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_762_63418);
    _code_inlined_insert_code_at_762_63418 = _31906;
    _31906 = NOVALUE;
    Ref(_31907);
    DeRef(_subprog_inlined_insert_code_at_765_63419);
    _subprog_inlined_insert_code_at_765_63419 = _31907;
    _31907 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_765_63419)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_765_63419)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_765_63419)) && (DBL_PTR(_subprog_inlined_insert_code_at_765_63419)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_765_63419);
        _subprog_inlined_insert_code_at_765_63419 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_765_63419;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_762_63418);
    _67insert_code(_code_inlined_insert_code_at_762_63418, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto L1A; // [777] 780
L1A: 
    DeRefi(_code_inlined_insert_code_at_762_63418);
    _code_inlined_insert_code_at_762_63418 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_765_63419);
    _subprog_inlined_insert_code_at_765_63419 = NOVALUE;

    /** 					pc += 4*/
    _pc_63299 = _pc_63299 + 4;
L17: 
L12: 
LF: 
LD: 
LA: 

    /** 	if (TRANSLATE or not with_type_check) and SymTab[which_type][S_NEXT] then*/
    if (_38TRANSLATE_16564 != 0) {
        _31909 = 1;
        goto L1B; // [799] 810
    }
    _31910 = (_with_type_check_63301 == 0);
    _31909 = (_31910 != 0);
L1B: 
    if (_31909 == 0) {
        goto L1C; // [810] 1041
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31912 = (int)*(((s1_ptr)_2)->base + _which_type_63264);
    _2 = (int)SEQ_PTR(_31912);
    _31913 = (int)*(((s1_ptr)_2)->base + 2);
    _31912 = NOVALUE;
    if (_31913 == 0) {
        _31913 = NOVALUE;
        goto L1C; // [827] 1041
    }
    else {
        if (!IS_ATOM_INT(_31913) && DBL_PTR(_31913)->dbl == 0.0){
            _31913 = NOVALUE;
            goto L1C; // [827] 1041
        }
        _31913 = NOVALUE;
    }
    _31913 = NOVALUE;

    /** 		integer start_pc = pc*/
    _start_pc_63430 = _pc_63299;

    /** 		if which_type = sequence_type or*/
    _31914 = (_which_type_63264 == _55sequence_type_47059);
    if (_31914 != 0) {
        goto L1D; // [843] 882
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31916 = (int)*(((s1_ptr)_2)->base + _which_type_63264);
    _2 = (int)SEQ_PTR(_31916);
    _31917 = (int)*(((s1_ptr)_2)->base + 2);
    _31916 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31917)){
        _31918 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31917)->dbl));
    }
    else{
        _31918 = (int)*(((s1_ptr)_2)->base + _31917);
    }
    _2 = (int)SEQ_PTR(_31918);
    _31919 = (int)*(((s1_ptr)_2)->base + 15);
    _31918 = NOVALUE;
    if (IS_ATOM_INT(_31919)) {
        _31920 = (_31919 == _55sequence_type_47059);
    }
    else {
        _31920 = binary_op(EQUALS, _31919, _55sequence_type_47059);
    }
    _31919 = NOVALUE;
    if (_31920 == 0) {
        DeRef(_31920);
        _31920 = NOVALUE;
        goto L1E; // [878] 938
    }
    else {
        if (!IS_ATOM_INT(_31920) && DBL_PTR(_31920)->dbl == 0.0){
            DeRef(_31920);
            _31920 = NOVALUE;
            goto L1E; // [878] 938
        }
        DeRef(_31920);
        _31920 = NOVALUE;
    }
    DeRef(_31920);
    _31920 = NOVALUE;
L1D: 

    /** 			insert_code( { SEQUENCE_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 97;
    ((int *)_2)[2] = _var_63266;
    _31921 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31922 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_909_63449);
    _code_inlined_insert_code_at_909_63449 = _31921;
    _31921 = NOVALUE;
    Ref(_31922);
    DeRef(_subprog_inlined_insert_code_at_912_63450);
    _subprog_inlined_insert_code_at_912_63450 = _31922;
    _31922 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_912_63450)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_912_63450)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_912_63450)) && (DBL_PTR(_subprog_inlined_insert_code_at_912_63450)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_912_63450);
        _subprog_inlined_insert_code_at_912_63450 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_912_63450;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_909_63449);
    _67insert_code(_code_inlined_insert_code_at_909_63449, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto L1F; // [924] 927
L1F: 
    DeRefi(_code_inlined_insert_code_at_909_63449);
    _code_inlined_insert_code_at_909_63449 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_912_63450);
    _subprog_inlined_insert_code_at_912_63450 = NOVALUE;

    /** 			pc += 2*/
    _pc_63299 = _pc_63299 + 2;
    goto L20; // [935] 1040
L1E: 

    /** 		elsif which_type = integer_type or*/
    _31924 = (_which_type_63264 == _55integer_type_47061);
    if (_31924 != 0) {
        goto L21; // [946] 985
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _31926 = (int)*(((s1_ptr)_2)->base + _which_type_63264);
    _2 = (int)SEQ_PTR(_31926);
    _31927 = (int)*(((s1_ptr)_2)->base + 2);
    _31926 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_31927)){
        _31928 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31927)->dbl));
    }
    else{
        _31928 = (int)*(((s1_ptr)_2)->base + _31927);
    }
    _2 = (int)SEQ_PTR(_31928);
    _31929 = (int)*(((s1_ptr)_2)->base + 15);
    _31928 = NOVALUE;
    if (IS_ATOM_INT(_31929)) {
        _31930 = (_31929 == _55integer_type_47061);
    }
    else {
        _31930 = binary_op(EQUALS, _31929, _55integer_type_47061);
    }
    _31929 = NOVALUE;
    if (_31930 == 0) {
        DeRef(_31930);
        _31930 = NOVALUE;
        goto L22; // [981] 1039
    }
    else {
        if (!IS_ATOM_INT(_31930) && DBL_PTR(_31930)->dbl == 0.0){
            DeRef(_31930);
            _31930 = NOVALUE;
            goto L22; // [981] 1039
        }
        DeRef(_31930);
        _31930 = NOVALUE;
    }
    DeRef(_31930);
    _31930 = NOVALUE;
L21: 

    /** 			insert_code( { INTEGER_CHECK, var }, pc, fr[FR_SUBPROG] )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 96;
    ((int *)_2)[2] = _var_63266;
    _31931 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_fr_63261);
    _31932 = (int)*(((s1_ptr)_2)->base + 4);
    DeRefi(_code_inlined_insert_code_at_1012_63470);
    _code_inlined_insert_code_at_1012_63470 = _31931;
    _31931 = NOVALUE;
    Ref(_31932);
    DeRef(_subprog_inlined_insert_code_at_1015_63471);
    _subprog_inlined_insert_code_at_1015_63471 = _31932;
    _31932 = NOVALUE;
    if (!IS_ATOM_INT(_subprog_inlined_insert_code_at_1015_63471)) {
        _1 = (long)(DBL_PTR(_subprog_inlined_insert_code_at_1015_63471)->dbl);
        if (UNIQUE(DBL_PTR(_subprog_inlined_insert_code_at_1015_63471)) && (DBL_PTR(_subprog_inlined_insert_code_at_1015_63471)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_subprog_inlined_insert_code_at_1015_63471);
        _subprog_inlined_insert_code_at_1015_63471 = _1;
    }

    /** 	shifting_sub = subprog*/
    _40shifting_sub_62533 = _subprog_inlined_insert_code_at_1015_63471;

    /** 	shift:insert_code( code, index )*/
    RefDS(_code_inlined_insert_code_at_1012_63470);
    _67insert_code(_code_inlined_insert_code_at_1012_63470, _pc_63299);

    /** 	shifting_sub = 0*/
    _40shifting_sub_62533 = 0;

    /** end procedure*/
    goto L23; // [1027] 1030
L23: 
    DeRefi(_code_inlined_insert_code_at_1012_63470);
    _code_inlined_insert_code_at_1012_63470 = NOVALUE;
    DeRef(_subprog_inlined_insert_code_at_1015_63471);
    _subprog_inlined_insert_code_at_1015_63471 = NOVALUE;

    /** 			pc += 4*/
    _pc_63299 = _pc_63299 + 4;
L22: 
L20: 
L1C: 

    /** 	resolved_reference( ref )*/
    _40resolved_reference(_ref_63260);

    /** 	reset_code()*/
    _40reset_code();

    /** end procedure*/
    DeRef(_tok_63259);
    DeRef(_fr_63261);
    _31847 = NOVALUE;
    DeRef(_31860);
    _31860 = NOVALUE;
    DeRef(_31865);
    _31865 = NOVALUE;
    _31893 = NOVALUE;
    _31901 = NOVALUE;
    DeRef(_31910);
    _31910 = NOVALUE;
    DeRef(_31914);
    _31914 = NOVALUE;
    _31917 = NOVALUE;
    DeRef(_31924);
    _31924 = NOVALUE;
    _31927 = NOVALUE;
    return;
    ;
}


void _40prep_forward_error(int _ref_63475)
{
    int _31940 = NOVALUE;
    int _31938 = NOVALUE;
    int _31936 = NOVALUE;
    int _31934 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ref_63475)) {
        _1 = (long)(DBL_PTR(_ref_63475)->dbl);
        if (UNIQUE(DBL_PTR(_ref_63475)) && (DBL_PTR(_ref_63475)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_63475);
        _ref_63475 = _1;
    }

    /** 	ThisLine = forward_references[ref][FR_THISLINE]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31934 = (int)*(((s1_ptr)_2)->base + _ref_63475);
    DeRef(_46ThisLine_49482);
    _2 = (int)SEQ_PTR(_31934);
    _46ThisLine_49482 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_46ThisLine_49482);
    _31934 = NOVALUE;

    /** 	bp = forward_references[ref][FR_BP]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31936 = (int)*(((s1_ptr)_2)->base + _ref_63475);
    _2 = (int)SEQ_PTR(_31936);
    _46bp_49486 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_46bp_49486)){
        _46bp_49486 = (long)DBL_PTR(_46bp_49486)->dbl;
    }
    _31936 = NOVALUE;

    /** 	line_number = forward_references[ref][FR_LINE]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31938 = (int)*(((s1_ptr)_2)->base + _ref_63475);
    _2 = (int)SEQ_PTR(_31938);
    _38line_number_16947 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_38line_number_16947)){
        _38line_number_16947 = (long)DBL_PTR(_38line_number_16947)->dbl;
    }
    _31938 = NOVALUE;

    /** 	current_file_no = forward_references[ref][FR_FILE]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31940 = (int)*(((s1_ptr)_2)->base + _ref_63475);
    _2 = (int)SEQ_PTR(_31940);
    _38current_file_no_16946 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_38current_file_no_16946)){
        _38current_file_no_16946 = (long)DBL_PTR(_38current_file_no_16946)->dbl;
    }
    _31940 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _40forward_error(int _tok_63491, int _ref_63492)
{
    int _31947 = NOVALUE;
    int _31946 = NOVALUE;
    int _31945 = NOVALUE;
    int _31944 = NOVALUE;
    int _31943 = NOVALUE;
    int _31942 = NOVALUE;
    int _0, _1, _2;
    

    /** 	prep_forward_error( ref )*/
    _40prep_forward_error(_ref_63492);

    /** 	CompileErr(68, { expected_name( forward_references[ref][FR_TYPE] ),*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31942 = (int)*(((s1_ptr)_2)->base + _ref_63492);
    _2 = (int)SEQ_PTR(_31942);
    _31943 = (int)*(((s1_ptr)_2)->base + 1);
    _31942 = NOVALUE;
    Ref(_31943);
    _31944 = _40expected_name(_31943);
    _31943 = NOVALUE;
    _2 = (int)SEQ_PTR(_tok_63491);
    _31945 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_31945);
    _31946 = _40expected_name(_31945);
    _31945 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _31944;
    ((int *)_2)[2] = _31946;
    _31947 = MAKE_SEQ(_1);
    _31946 = NOVALUE;
    _31944 = NOVALUE;
    _46CompileErr(68, _31947, 0);
    _31947 = NOVALUE;

    /** end procedure*/
    DeRef(_tok_63491);
    return;
    ;
}


int _40find_reference(int _fr_63503)
{
    int _name_63504 = NOVALUE;
    int _file_63506 = NOVALUE;
    int _ns_file_63508 = NOVALUE;
    int _ix_63509 = NOVALUE;
    int _ns_63512 = NOVALUE;
    int _ns_tok_63516 = NOVALUE;
    int _tok_63528 = NOVALUE;
    int _31958 = NOVALUE;
    int _31955 = NOVALUE;
    int _31953 = NOVALUE;
    int _31951 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence name = fr[FR_NAME]*/
    DeRef(_name_63504);
    _2 = (int)SEQ_PTR(_fr_63503);
    _name_63504 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_name_63504);

    /** 	integer file  = fr[FR_FILE]*/
    _2 = (int)SEQ_PTR(_fr_63503);
    _file_63506 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_file_63506))
    _file_63506 = (long)DBL_PTR(_file_63506)->dbl;

    /** 	integer ns_file = -1*/
    _ns_file_63508 = -1;

    /** 	integer ix = find( ':', name )*/
    _ix_63509 = find_from(58, _name_63504, 1);

    /** 	if ix then*/
    if (_ix_63509 == 0)
    {
        goto L1; // [35] 91
    }
    else{
    }

    /** 		sequence ns = name[1..ix-1]*/
    _31951 = _ix_63509 - 1;
    rhs_slice_target = (object_ptr)&_ns_63512;
    RHS_Slice(_name_63504, 1, _31951);

    /** 		token ns_tok = keyfind( ns, ns_file, file, 1, fr[FR_HASHVAL] )*/
    _2 = (int)SEQ_PTR(_fr_63503);
    _31953 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_ns_63512);
    Ref(_31953);
    _0 = _ns_tok_63516;
    _ns_tok_63516 = _55keyfind(_ns_63512, -1, _file_63506, 1, _31953);
    DeRef(_0);
    _31953 = NOVALUE;

    /** 		if ns_tok[T_ID] != NAMESPACE then*/
    _2 = (int)SEQ_PTR(_ns_tok_63516);
    _31955 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _31955, 523)){
        _31955 = NOVALUE;
        goto L2; // [75] 86
    }
    _31955 = NOVALUE;

    /** 			return ns_tok*/
    DeRefDS(_ns_63512);
    DeRefDS(_fr_63503);
    DeRefDS(_name_63504);
    DeRef(_tok_63528);
    _31951 = NOVALUE;
    return _ns_tok_63516;
L2: 
    DeRef(_ns_63512);
    _ns_63512 = NOVALUE;
    DeRef(_ns_tok_63516);
    _ns_tok_63516 = NOVALUE;
    goto L3; // [88] 100
L1: 

    /** 		ns_file = fr[FR_QUALIFIED]*/
    _2 = (int)SEQ_PTR(_fr_63503);
    _ns_file_63508 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_ns_file_63508))
    _ns_file_63508 = (long)DBL_PTR(_ns_file_63508)->dbl;
L3: 

    /** 	No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 	object tok = keyfind( name, ns_file, file, , fr[FR_HASHVAL] )*/
    _2 = (int)SEQ_PTR(_fr_63503);
    _31958 = (int)*(((s1_ptr)_2)->base + 11);
    RefDS(_name_63504);
    Ref(_31958);
    _0 = _tok_63528;
    _tok_63528 = _55keyfind(_name_63504, _ns_file_63508, _file_63506, 0, _31958);
    DeRef(_0);
    _31958 = NOVALUE;

    /** 	No_new_entry = 0*/
    _55No_new_entry_48227 = 0;

    /** 	return tok*/
    DeRefDS(_fr_63503);
    DeRefDS(_name_63504);
    DeRef(_31951);
    _31951 = NOVALUE;
    return _tok_63528;
    ;
}


void _40register_forward_type(int _sym_63536, int _ref_63537)
{
    int _31965 = NOVALUE;
    int _31964 = NOVALUE;
    int _31962 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_63536)) {
        _1 = (long)(DBL_PTR(_sym_63536)->dbl);
        if (UNIQUE(DBL_PTR(_sym_63536)) && (DBL_PTR(_sym_63536)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_63536);
        _sym_63536 = _1;
    }
    if (!IS_ATOM_INT(_ref_63537)) {
        _1 = (long)(DBL_PTR(_ref_63537)->dbl);
        if (UNIQUE(DBL_PTR(_ref_63537)) && (DBL_PTR(_ref_63537)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_63537);
        _ref_63537 = _1;
    }

    /** 	if ref < 0 then*/
    if (_ref_63537 >= 0)
    goto L1; // [7] 19

    /** 		ref = -ref*/
    _ref_63537 = - _ref_63537;
L1: 

    /** 	forward_references[ref][FR_DATA] &= sym*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63537 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _31964 = (int)*(((s1_ptr)_2)->base + 12);
    _31962 = NOVALUE;
    if (IS_SEQUENCE(_31964) && IS_ATOM(_sym_63536)) {
        Append(&_31965, _31964, _sym_63536);
    }
    else if (IS_ATOM(_31964) && IS_SEQUENCE(_sym_63536)) {
    }
    else {
        Concat((object_ptr)&_31965, _31964, _sym_63536);
        _31964 = NOVALUE;
    }
    _31964 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _31965;
    if( _1 != _31965 ){
        DeRef(_1);
    }
    _31965 = NOVALUE;
    _31962 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _40forward_reference(int _ref_63547)
{
    int _31978 = NOVALUE;
    int _31977 = NOVALUE;
    int _31976 = NOVALUE;
    int _31975 = NOVALUE;
    int _31974 = NOVALUE;
    int _31973 = NOVALUE;
    int _31972 = NOVALUE;
    int _31970 = NOVALUE;
    int _31969 = NOVALUE;
    int _31968 = NOVALUE;
    int _31967 = NOVALUE;
    int _31966 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ref_63547)) {
        _1 = (long)(DBL_PTR(_ref_63547)->dbl);
        if (UNIQUE(DBL_PTR(_ref_63547)) && (DBL_PTR(_ref_63547)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ref_63547);
        _ref_63547 = _1;
    }

    /** 	if 0 > ref and ref >= -length( forward_references ) then*/
    _31966 = (0 > _ref_63547);
    if (_31966 == 0) {
        goto L1; // [9] 95
    }
    if (IS_SEQUENCE(_40forward_references_62503)){
            _31968 = SEQ_PTR(_40forward_references_62503)->length;
    }
    else {
        _31968 = 1;
    }
    _31969 = - _31968;
    _31970 = (_ref_63547 >= _31969);
    _31969 = NOVALUE;
    if (_31970 == 0)
    {
        DeRef(_31970);
        _31970 = NOVALUE;
        goto L1; // [26] 95
    }
    else{
        DeRef(_31970);
        _31970 = NOVALUE;
    }

    /** 		ref = -ref*/
    _ref_63547 = - _ref_63547;

    /** 		if integer(forward_references[ref][FR_FILE]) and*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31972 = (int)*(((s1_ptr)_2)->base + _ref_63547);
    _2 = (int)SEQ_PTR(_31972);
    _31973 = (int)*(((s1_ptr)_2)->base + 3);
    _31972 = NOVALUE;
    if (IS_ATOM_INT(_31973))
    _31974 = 1;
    else if (IS_ATOM_DBL(_31973))
    _31974 = IS_ATOM_INT(DoubleToInt(_31973));
    else
    _31974 = 0;
    _31973 = NOVALUE;
    if (_31974 == 0) {
        goto L2; // [53] 85
    }
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _31976 = (int)*(((s1_ptr)_2)->base + _ref_63547);
    _2 = (int)SEQ_PTR(_31976);
    _31977 = (int)*(((s1_ptr)_2)->base + 5);
    _31976 = NOVALUE;
    if (IS_ATOM_INT(_31977))
    _31978 = 1;
    else if (IS_ATOM_DBL(_31977))
    _31978 = IS_ATOM_INT(DoubleToInt(_31977));
    else
    _31978 = 0;
    _31977 = NOVALUE;
    if (_31978 == 0)
    {
        _31978 = NOVALUE;
        goto L2; // [73] 85
    }
    else{
        _31978 = NOVALUE;
    }

    /** 				return 1*/
    DeRef(_31966);
    _31966 = NOVALUE;
    return 1;
    goto L3; // [82] 102
L2: 

    /** 			return 0*/
    DeRef(_31966);
    _31966 = NOVALUE;
    return 0;
    goto L3; // [92] 102
L1: 

    /** 		return 0*/
    DeRef(_31966);
    _31966 = NOVALUE;
    return 0;
L3: 
    ;
}


int _40new_forward_reference(int _fwd_op_63567, int _sym_63569, int _op_63570)
{
    int _ref_63571 = NOVALUE;
    int _len_63572 = NOVALUE;
    int _hashval_63602 = NOVALUE;
    int _default_sym_63677 = NOVALUE;
    int _param_63680 = NOVALUE;
    int _set_data_2__tmp_at616_63697 = NOVALUE;
    int _set_data_1__tmp_at616_63696 = NOVALUE;
    int _data_inlined_set_data_at_613_63695 = NOVALUE;
    int _32051 = NOVALUE;
    int _32050 = NOVALUE;
    int _32049 = NOVALUE;
    int _32046 = NOVALUE;
    int _32044 = NOVALUE;
    int _32043 = NOVALUE;
    int _32041 = NOVALUE;
    int _32040 = NOVALUE;
    int _32039 = NOVALUE;
    int _32037 = NOVALUE;
    int _32035 = NOVALUE;
    int _32033 = NOVALUE;
    int _32030 = NOVALUE;
    int _32029 = NOVALUE;
    int _32027 = NOVALUE;
    int _32025 = NOVALUE;
    int _32023 = NOVALUE;
    int _32021 = NOVALUE;
    int _32020 = NOVALUE;
    int _32019 = NOVALUE;
    int _32017 = NOVALUE;
    int _32014 = NOVALUE;
    int _32012 = NOVALUE;
    int _32010 = NOVALUE;
    int _32009 = NOVALUE;
    int _32008 = NOVALUE;
    int _32007 = NOVALUE;
    int _32005 = NOVALUE;
    int _32002 = NOVALUE;
    int _32001 = NOVALUE;
    int _32000 = NOVALUE;
    int _31998 = NOVALUE;
    int _31997 = NOVALUE;
    int _31996 = NOVALUE;
    int _31995 = NOVALUE;
    int _31993 = NOVALUE;
    int _31992 = NOVALUE;
    int _31991 = NOVALUE;
    int _31990 = NOVALUE;
    int _31988 = NOVALUE;
    int _31985 = NOVALUE;
    int _31984 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_fwd_op_63567)) {
        _1 = (long)(DBL_PTR(_fwd_op_63567)->dbl);
        if (UNIQUE(DBL_PTR(_fwd_op_63567)) && (DBL_PTR(_fwd_op_63567)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwd_op_63567);
        _fwd_op_63567 = _1;
    }
    if (!IS_ATOM_INT(_sym_63569)) {
        _1 = (long)(DBL_PTR(_sym_63569)->dbl);
        if (UNIQUE(DBL_PTR(_sym_63569)) && (DBL_PTR(_sym_63569)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_63569);
        _sym_63569 = _1;
    }
    if (!IS_ATOM_INT(_op_63570)) {
        _1 = (long)(DBL_PTR(_op_63570)->dbl);
        if (UNIQUE(DBL_PTR(_op_63570)) && (DBL_PTR(_op_63570)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_63570);
        _op_63570 = _1;
    }

    /** 		len = length( inactive_references )*/
    if (IS_SEQUENCE(_40inactive_references_62507)){
            _len_63572 = SEQ_PTR(_40inactive_references_62507)->length;
    }
    else {
        _len_63572 = 1;
    }

    /** 	if len then*/
    if (_len_63572 == 0)
    {
        goto L1; // [16] 39
    }
    else{
    }

    /** 		ref = inactive_references[len]*/
    _2 = (int)SEQ_PTR(_40inactive_references_62507);
    _ref_63571 = (int)*(((s1_ptr)_2)->base + _len_63572);
    if (!IS_ATOM_INT(_ref_63571))
    _ref_63571 = (long)DBL_PTR(_ref_63571)->dbl;

    /** 		inactive_references = remove( inactive_references, len, len )*/
    {
        s1_ptr assign_space = SEQ_PTR(_40inactive_references_62507);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_len_63572)) ? _len_63572 : (long)(DBL_PTR(_len_63572)->dbl);
        int stop = (IS_ATOM_INT(_len_63572)) ? _len_63572 : (long)(DBL_PTR(_len_63572)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_40inactive_references_62507), start, &_40inactive_references_62507 );
            }
            else Tail(SEQ_PTR(_40inactive_references_62507), stop+1, &_40inactive_references_62507);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_40inactive_references_62507), start, &_40inactive_references_62507);
        }
        else {
            assign_slice_seq = &assign_space;
            _40inactive_references_62507 = Remove_elements(start, stop, (SEQ_PTR(_40inactive_references_62507)->ref == 1));
        }
    }
    goto L2; // [36] 55
L1: 

    /** 		forward_references &= 0*/
    Append(&_40forward_references_62503, _40forward_references_62503, 0);

    /** 		ref = length( forward_references )*/
    if (IS_SEQUENCE(_40forward_references_62503)){
            _ref_63571 = SEQ_PTR(_40forward_references_62503)->length;
    }
    else {
        _ref_63571 = 1;
    }
L2: 

    /** 	forward_references[ref] = repeat( 0, FR_SIZE )*/
    _31984 = Repeat(0, 12);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ref_63571);
    _1 = *(int *)_2;
    *(int *)_2 = _31984;
    if( _1 != _31984 ){
        DeRef(_1);
    }
    _31984 = NOVALUE;

    /** 	forward_references[ref][FR_TYPE]      = fwd_op*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _fwd_op_63567;
    DeRef(_1);
    _31985 = NOVALUE;

    /** 	if sym < 0 then*/
    if (_sym_63569 >= 0)
    goto L3; // [88] 155

    /** 		forward_references[ref][FR_NAME] = forward_references[-sym][FR_NAME]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    if ((unsigned long)_sym_63569 == 0xC0000000)
    _31990 = (int)NewDouble((double)-0xC0000000);
    else
    _31990 = - _sym_63569;
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!IS_ATOM_INT(_31990)){
        _31991 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31990)->dbl));
    }
    else{
        _31991 = (int)*(((s1_ptr)_2)->base + _31990);
    }
    _2 = (int)SEQ_PTR(_31991);
    _31992 = (int)*(((s1_ptr)_2)->base + 2);
    _31991 = NOVALUE;
    Ref(_31992);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _31992;
    if( _1 != _31992 ){
        DeRef(_1);
    }
    _31992 = NOVALUE;
    _31988 = NOVALUE;

    /** 		forward_references[ref][FR_HASHVAL] = forward_references[-sym][FR_HASHVAL]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    if ((unsigned long)_sym_63569 == 0xC0000000)
    _31995 = (int)NewDouble((double)-0xC0000000);
    else
    _31995 = - _sym_63569;
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!IS_ATOM_INT(_31995)){
        _31996 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_31995)->dbl));
    }
    else{
        _31996 = (int)*(((s1_ptr)_2)->base + _31995);
    }
    _2 = (int)SEQ_PTR(_31996);
    _31997 = (int)*(((s1_ptr)_2)->base + 11);
    _31996 = NOVALUE;
    Ref(_31997);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _31997;
    if( _1 != _31997 ){
        DeRef(_1);
    }
    _31997 = NOVALUE;
    _31993 = NOVALUE;
    goto L4; // [152] 262
L3: 

    /** 		forward_references[ref][FR_NAME] = SymTab[sym][S_NAME]*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32000 = (int)*(((s1_ptr)_2)->base + _sym_63569);
    _2 = (int)SEQ_PTR(_32000);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _32001 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _32001 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _32000 = NOVALUE;
    Ref(_32001);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _32001;
    if( _1 != _32001 ){
        DeRef(_1);
    }
    _32001 = NOVALUE;
    _31998 = NOVALUE;

    /** 		integer hashval = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32002 = (int)*(((s1_ptr)_2)->base + _sym_63569);
    _2 = (int)SEQ_PTR(_32002);
    _hashval_63602 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_hashval_63602)){
        _hashval_63602 = (long)DBL_PTR(_hashval_63602)->dbl;
    }
    _32002 = NOVALUE;

    /** 		if 0 = hashval then*/
    if (0 != _hashval_63602)
    goto L5; // [200] 238

    /** 			forward_references[ref][FR_HASHVAL] = hashfn( forward_references[ref][FR_NAME] )*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    _32007 = (int)*(((s1_ptr)_2)->base + _ref_63571);
    _2 = (int)SEQ_PTR(_32007);
    _32008 = (int)*(((s1_ptr)_2)->base + 2);
    _32007 = NOVALUE;
    Ref(_32008);
    _32009 = _55hashfn(_32008);
    _32008 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _32009;
    if( _1 != _32009 ){
        DeRef(_1);
    }
    _32009 = NOVALUE;
    _32005 = NOVALUE;
    goto L6; // [235] 259
L5: 

    /** 			forward_references[ref][FR_HASHVAL] = hashval*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _hashval_63602;
    DeRef(_1);
    _32010 = NOVALUE;

    /** 			remove_symbol( sym )*/
    _55remove_symbol(_sym_63569);
L6: 
L4: 

    /** 	forward_references[ref][FR_FILE]      = current_file_no*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _38current_file_no_16946;
    DeRef(_1);
    _32012 = NOVALUE;

    /** 	forward_references[ref][FR_SUBPROG]   = CurrentSub*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _38CurrentSub_16954;
    DeRef(_1);
    _32014 = NOVALUE;

    /** 	if fwd_op != TYPE then*/
    if (_fwd_op_63567 == 504)
    goto L7; // [300] 329

    /** 		forward_references[ref][FR_PC]        = length( Code ) + 1*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_38Code_17038)){
            _32019 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _32019 = 1;
    }
    _32020 = _32019 + 1;
    _32019 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _32020;
    if( _1 != _32020 ){
        DeRef(_1);
    }
    _32020 = NOVALUE;
    _32017 = NOVALUE;
L7: 

    /** 	forward_references[ref][FR_LINE]      = fwd_line_number*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _38fwd_line_number_16948;
    DeRef(_1);
    _32021 = NOVALUE;

    /** 	forward_references[ref][FR_THISLINE]  = ForwardLine*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    Ref(_46ForwardLine_49483);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _46ForwardLine_49483;
    DeRef(_1);
    _32023 = NOVALUE;

    /** 	forward_references[ref][FR_BP]        = forward_bp*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _46forward_bp_49487;
    DeRef(_1);
    _32025 = NOVALUE;

    /** 	forward_references[ref][FR_QUALIFIED] = get_qualified_fwd()*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _32029 = _62get_qualified_fwd();
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _32029;
    if( _1 != _32029 ){
        DeRef(_1);
    }
    _32029 = NOVALUE;
    _32027 = NOVALUE;

    /** 	forward_references[ref][FR_OP]        = op*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = _op_63570;
    DeRef(_1);
    _32030 = NOVALUE;

    /** 	if op = GOTO then*/
    if (_op_63570 != 188)
    goto L8; // [417] 441

    /** 		forward_references[ref][FR_DATA] = { sym }*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _sym_63569;
    _32035 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _32035;
    if( _1 != _32035 ){
        DeRef(_1);
    }
    _32035 = NOVALUE;
    _32033 = NOVALUE;
L8: 

    /** 	if CurrentSub = TopLevelSub then*/
    if (_38CurrentSub_16954 != _38TopLevelSub_16953)
    goto L9; // [447] 509

    /** 		if length( toplevel_references ) < current_file_no then*/
    if (IS_SEQUENCE(_40toplevel_references_62506)){
            _32037 = SEQ_PTR(_40toplevel_references_62506)->length;
    }
    else {
        _32037 = 1;
    }
    if (_32037 >= _38current_file_no_16946)
    goto LA; // [460] 488

    /** 			toplevel_references &= repeat( {}, current_file_no - length( toplevel_references ) )*/
    if (IS_SEQUENCE(_40toplevel_references_62506)){
            _32039 = SEQ_PTR(_40toplevel_references_62506)->length;
    }
    else {
        _32039 = 1;
    }
    _32040 = _38current_file_no_16946 - _32039;
    _32039 = NOVALUE;
    _32041 = Repeat(_22663, _32040);
    _32040 = NOVALUE;
    Concat((object_ptr)&_40toplevel_references_62506, _40toplevel_references_62506, _32041);
    DeRefDS(_32041);
    _32041 = NOVALUE;
LA: 

    /** 		toplevel_references[current_file_no] &= ref*/
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    _32043 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_32043) && IS_ATOM(_ref_63571)) {
        Append(&_32044, _32043, _ref_63571);
    }
    else if (IS_ATOM(_32043) && IS_SEQUENCE(_ref_63571)) {
    }
    else {
        Concat((object_ptr)&_32044, _32043, _ref_63571);
        _32043 = NOVALUE;
    }
    _32043 = NOVALUE;
    _2 = (int)SEQ_PTR(_40toplevel_references_62506);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40toplevel_references_62506 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _32044;
    if( _1 != _32044 ){
        DeRef(_1);
    }
    _32044 = NOVALUE;
    goto LB; // [506] 635
L9: 

    /** 		add_active_reference( ref )*/
    _40add_active_reference(_ref_63571, _38current_file_no_16946);

    /** 		if Parser_mode = PAM_RECORD then*/
    if (_38Parser_mode_17071 != 1)
    goto LC; // [523] 632

    /** 			symtab_pointer default_sym = CurrentSub*/
    _default_sym_63677 = _38CurrentSub_16954;

    /** 			symtab_pointer param = 0*/
    _param_63680 = 0;

    /** 			while default_sym with entry do*/
    goto LD; // [545] 574
LE: 
    if (_default_sym_63677 == 0)
    {
        goto LF; // [548] 587
    }
    else{
    }

    /** 				if sym_scope( default_sym ) = SC_PRIVATE then*/
    _32046 = _55sym_scope(_default_sym_63677);
    if (binary_op_a(NOTEQ, _32046, 3)){
        DeRef(_32046);
        _32046 = NOVALUE;
        goto L10; // [559] 571
    }
    DeRef(_32046);
    _32046 = NOVALUE;

    /** 					param = default_sym*/
    _param_63680 = _default_sym_63677;
L10: 

    /** 			entry*/
LD: 

    /** 				default_sym = sym_next( default_sym )*/
    _default_sym_63677 = _55sym_next(_default_sym_63677);
    if (!IS_ATOM_INT(_default_sym_63677)) {
        _1 = (long)(DBL_PTR(_default_sym_63677)->dbl);
        if (UNIQUE(DBL_PTR(_default_sym_63677)) && (DBL_PTR(_default_sym_63677)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_default_sym_63677);
        _default_sym_63677 = _1;
    }

    /** 			end while*/
    goto LE; // [584] 548
LF: 

    /** 			set_data( ref, {{ PAM_RECORD, param, length( Recorded_sym ) }} )*/
    if (IS_SEQUENCE(_38Recorded_sym_17074)){
            _32049 = SEQ_PTR(_38Recorded_sym_17074)->length;
    }
    else {
        _32049 = 1;
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = _param_63680;
    *((int *)(_2+12)) = _32049;
    _32050 = MAKE_SEQ(_1);
    _32049 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _32050;
    _32051 = MAKE_SEQ(_1);
    _32050 = NOVALUE;
    DeRef(_data_inlined_set_data_at_613_63695);
    _data_inlined_set_data_at_613_63695 = _32051;
    _32051 = NOVALUE;

    /** 	forward_references[ref][FR_DATA] = data*/
    _2 = (int)SEQ_PTR(_40forward_references_62503);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40forward_references_62503 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ref_63571 + ((s1_ptr)_2)->base);
    RefDS(_data_inlined_set_data_at_613_63695);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _data_inlined_set_data_at_613_63695;
    DeRef(_1);

    /** end procedure*/
    goto L11; // [626] 629
L11: 
    DeRef(_data_inlined_set_data_at_613_63695);
    _data_inlined_set_data_at_613_63695 = NOVALUE;
LC: 
LB: 

    /** 	fwdref_count += 1*/
    _40fwdref_count_62534 = _40fwdref_count_62534 + 1;

    /** 	return ref*/
    DeRef(_31990);
    _31990 = NOVALUE;
    DeRef(_31995);
    _31995 = NOVALUE;
    return _ref_63571;
    ;
}


void _40add_active_reference(int _ref_63701, int _file_no_63702)
{
    int _sp_63716 = NOVALUE;
    int _32075 = NOVALUE;
    int _32074 = NOVALUE;
    int _32072 = NOVALUE;
    int _32071 = NOVALUE;
    int _32070 = NOVALUE;
    int _32068 = NOVALUE;
    int _32067 = NOVALUE;
    int _32066 = NOVALUE;
    int _32063 = NOVALUE;
    int _32061 = NOVALUE;
    int _32060 = NOVALUE;
    int _32059 = NOVALUE;
    int _32057 = NOVALUE;
    int _32056 = NOVALUE;
    int _32055 = NOVALUE;
    int _32053 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if length( active_references ) < file_no then*/
    if (IS_SEQUENCE(_40active_references_62505)){
            _32053 = SEQ_PTR(_40active_references_62505)->length;
    }
    else {
        _32053 = 1;
    }
    if (_32053 >= _file_no_63702)
    goto L1; // [12] 59

    /** 		active_references &= repeat( {}, file_no - length( active_references ) )*/
    if (IS_SEQUENCE(_40active_references_62505)){
            _32055 = SEQ_PTR(_40active_references_62505)->length;
    }
    else {
        _32055 = 1;
    }
    _32056 = _file_no_63702 - _32055;
    _32055 = NOVALUE;
    _32057 = Repeat(_22663, _32056);
    _32056 = NOVALUE;
    Concat((object_ptr)&_40active_references_62505, _40active_references_62505, _32057);
    DeRefDS(_32057);
    _32057 = NOVALUE;

    /** 		active_subprogs   &= repeat( {}, file_no - length( active_subprogs ) )*/
    if (IS_SEQUENCE(_40active_subprogs_62504)){
            _32059 = SEQ_PTR(_40active_subprogs_62504)->length;
    }
    else {
        _32059 = 1;
    }
    _32060 = _file_no_63702 - _32059;
    _32059 = NOVALUE;
    _32061 = Repeat(_22663, _32060);
    _32060 = NOVALUE;
    Concat((object_ptr)&_40active_subprogs_62504, _40active_subprogs_62504, _32061);
    DeRefDS(_32061);
    _32061 = NOVALUE;
L1: 

    /** 	integer sp = find( CurrentSub, active_subprogs[file_no] )*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _32063 = (int)*(((s1_ptr)_2)->base + _file_no_63702);
    _sp_63716 = find_from(_38CurrentSub_16954, _32063, 1);
    _32063 = NOVALUE;

    /** 	if not sp then*/
    if (_sp_63716 != 0)
    goto L2; // [76] 127

    /** 		active_subprogs[file_no] &= CurrentSub*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _32066 = (int)*(((s1_ptr)_2)->base + _file_no_63702);
    if (IS_SEQUENCE(_32066) && IS_ATOM(_38CurrentSub_16954)) {
        Append(&_32067, _32066, _38CurrentSub_16954);
    }
    else if (IS_ATOM(_32066) && IS_SEQUENCE(_38CurrentSub_16954)) {
    }
    else {
        Concat((object_ptr)&_32067, _32066, _38CurrentSub_16954);
        _32066 = NOVALUE;
    }
    _32066 = NOVALUE;
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_subprogs_62504 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_no_63702);
    _1 = *(int *)_2;
    *(int *)_2 = _32067;
    if( _1 != _32067 ){
        DeRef(_1);
    }
    _32067 = NOVALUE;

    /** 		sp = length( active_subprogs[file_no] )*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _32068 = (int)*(((s1_ptr)_2)->base + _file_no_63702);
    if (IS_SEQUENCE(_32068)){
            _sp_63716 = SEQ_PTR(_32068)->length;
    }
    else {
        _sp_63716 = 1;
    }
    _32068 = NOVALUE;

    /** 		active_references[file_no] = append( active_references[file_no], {} )*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    _32070 = (int)*(((s1_ptr)_2)->base + _file_no_63702);
    RefDS(_22663);
    Append(&_32071, _32070, _22663);
    _32070 = NOVALUE;
    _2 = (int)SEQ_PTR(_40active_references_62505);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_references_62505 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _file_no_63702);
    _1 = *(int *)_2;
    *(int *)_2 = _32071;
    if( _1 != _32071 ){
        DeRef(_1);
    }
    _32071 = NOVALUE;
L2: 

    /** 	active_references[file_no][sp] &= ref*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _40active_references_62505 = MAKE_SEQ(_2);
    }
    _3 = (int)(_file_no_63702 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _32074 = (int)*(((s1_ptr)_2)->base + _sp_63716);
    _32072 = NOVALUE;
    if (IS_SEQUENCE(_32074) && IS_ATOM(_ref_63701)) {
        Append(&_32075, _32074, _ref_63701);
    }
    else if (IS_ATOM(_32074) && IS_SEQUENCE(_ref_63701)) {
    }
    else {
        Concat((object_ptr)&_32075, _32074, _ref_63701);
        _32074 = NOVALUE;
    }
    _32074 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _sp_63716);
    _1 = *(int *)_2;
    *(int *)_2 = _32075;
    if( _1 != _32075 ){
        DeRef(_1);
    }
    _32075 = NOVALUE;
    _32072 = NOVALUE;

    /** end procedure*/
    _32068 = NOVALUE;
    return;
    ;
}


int _40resolve_file(int _refs_63753, int _report_errors_63754, int _unincluded_ok_63755)
{
    int _errors_63756 = NOVALUE;
    int _ref_63760 = NOVALUE;
    int _fr_63762 = NOVALUE;
    int _tok_63775 = NOVALUE;
    int _code_sub_63783 = NOVALUE;
    int _fr_type_63785 = NOVALUE;
    int _sym_tok_63787 = NOVALUE;
    int _32129 = NOVALUE;
    int _32128 = NOVALUE;
    int _32127 = NOVALUE;
    int _32126 = NOVALUE;
    int _32125 = NOVALUE;
    int _32124 = NOVALUE;
    int _32119 = NOVALUE;
    int _32118 = NOVALUE;
    int _32117 = NOVALUE;
    int _32115 = NOVALUE;
    int _32114 = NOVALUE;
    int _32111 = NOVALUE;
    int _32110 = NOVALUE;
    int _32109 = NOVALUE;
    int _32105 = NOVALUE;
    int _32104 = NOVALUE;
    int _32096 = NOVALUE;
    int _32094 = NOVALUE;
    int _32093 = NOVALUE;
    int _32092 = NOVALUE;
    int _32091 = NOVALUE;
    int _32090 = NOVALUE;
    int _32089 = NOVALUE;
    int _32086 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence errors = {}*/
    RefDS(_22663);
    DeRefi(_errors_63756);
    _errors_63756 = _22663;

    /** 	for ar = length( refs ) to 1 by -1 do*/
    if (IS_SEQUENCE(_refs_63753)){
            _32086 = SEQ_PTR(_refs_63753)->length;
    }
    else {
        _32086 = 1;
    }
    {
        int _ar_63758;
        _ar_63758 = _32086;
L1: 
        if (_ar_63758 < 1){
            goto L2; // [19] 491
        }

        /** 		integer ref = refs[ar]*/
        _2 = (int)SEQ_PTR(_refs_63753);
        _ref_63760 = (int)*(((s1_ptr)_2)->base + _ar_63758);
        if (!IS_ATOM_INT(_ref_63760))
        _ref_63760 = (long)DBL_PTR(_ref_63760)->dbl;

        /** 		sequence fr = forward_references[ref]*/
        DeRef(_fr_63762);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        _fr_63762 = (int)*(((s1_ptr)_2)->base + _ref_63760);
        Ref(_fr_63762);

        /** 		if include_matrix[fr[FR_FILE]][current_file_no] = NOT_INCLUDED and not unincluded_ok then*/
        _2 = (int)SEQ_PTR(_fr_63762);
        _32089 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!IS_ATOM_INT(_32089)){
            _32090 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32089)->dbl));
        }
        else{
            _32090 = (int)*(((s1_ptr)_2)->base + _32089);
        }
        _2 = (int)SEQ_PTR(_32090);
        _32091 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
        _32090 = NOVALUE;
        if (IS_ATOM_INT(_32091)) {
            _32092 = (_32091 == 0);
        }
        else {
            _32092 = binary_op(EQUALS, _32091, 0);
        }
        _32091 = NOVALUE;
        if (IS_ATOM_INT(_32092)) {
            if (_32092 == 0) {
                goto L3; // [68] 86
            }
        }
        else {
            if (DBL_PTR(_32092)->dbl == 0.0) {
                goto L3; // [68] 86
            }
        }
        _32094 = (_unincluded_ok_63755 == 0);
        if (_32094 == 0)
        {
            DeRef(_32094);
            _32094 = NOVALUE;
            goto L3; // [76] 86
        }
        else{
            DeRef(_32094);
            _32094 = NOVALUE;
        }

        /** 			continue*/
        DeRef(_fr_63762);
        _fr_63762 = NOVALUE;
        DeRef(_tok_63775);
        _tok_63775 = NOVALUE;
        goto L4; // [83] 486
L3: 

        /** 		token tok = find_reference( fr )*/
        RefDS(_fr_63762);
        _0 = _tok_63775;
        _tok_63775 = _40find_reference(_fr_63762);
        DeRef(_0);

        /** 		if tok[T_ID] = IGNORED then*/
        _2 = (int)SEQ_PTR(_tok_63775);
        _32096 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _32096, 509)){
            _32096 = NOVALUE;
            goto L5; // [102] 119
        }
        _32096 = NOVALUE;

        /** 			errors &= ref*/
        Append(&_errors_63756, _errors_63756, _ref_63760);

        /** 			continue*/
        DeRefDS(_fr_63762);
        _fr_63762 = NOVALUE;
        DeRef(_tok_63775);
        _tok_63775 = NOVALUE;
        goto L4; // [116] 486
L5: 

        /** 		integer code_sub = fr[FR_SUBPROG]*/
        _2 = (int)SEQ_PTR(_fr_63762);
        _code_sub_63783 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_code_sub_63783))
        _code_sub_63783 = (long)DBL_PTR(_code_sub_63783)->dbl;

        /** 		integer fr_type  = fr[FR_TYPE]*/
        _2 = (int)SEQ_PTR(_fr_63762);
        _fr_type_63785 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_fr_type_63785))
        _fr_type_63785 = (long)DBL_PTR(_fr_type_63785)->dbl;

        /** 		integer sym_tok*/

        /** 		switch fr_type label "fr_type" do*/
        _0 = _fr_type_63785;
        switch ( _0 ){ 

            /** 			case PROC, FUNC then*/
            case 27:
            case 501:

            /** 				sym_tok = SymTab[tok[T_SYM]][S_TOKEN]*/
            _2 = (int)SEQ_PTR(_tok_63775);
            _32104 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!IS_ATOM_INT(_32104)){
                _32105 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32104)->dbl));
            }
            else{
                _32105 = (int)*(((s1_ptr)_2)->base + _32104);
            }
            _2 = (int)SEQ_PTR(_32105);
            if (!IS_ATOM_INT(_38S_TOKEN_16603)){
                _sym_tok_63787 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
            }
            else{
                _sym_tok_63787 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
            }
            if (!IS_ATOM_INT(_sym_tok_63787)){
                _sym_tok_63787 = (long)DBL_PTR(_sym_tok_63787)->dbl;
            }
            _32105 = NOVALUE;

            /** 				if sym_tok = TYPE then*/
            if (_sym_tok_63787 != 504)
            goto L6; // [176] 190

            /** 					sym_tok = FUNC*/
            _sym_tok_63787 = 501;
L6: 

            /** 				if sym_tok != fr_type then*/
            if (_sym_tok_63787 == _fr_type_63785)
            goto L7; // [192] 226

            /** 					if sym_tok != FUNC and fr_type != PROC then*/
            _32109 = (_sym_tok_63787 != 501);
            if (_32109 == 0) {
                goto L8; // [204] 225
            }
            _32111 = (_fr_type_63785 != 27);
            if (_32111 == 0)
            {
                DeRef(_32111);
                _32111 = NOVALUE;
                goto L8; // [215] 225
            }
            else{
                DeRef(_32111);
                _32111 = NOVALUE;
            }

            /** 						forward_error( tok, ref )*/
            Ref(_tok_63775);
            _40forward_error(_tok_63775, _ref_63760);
L8: 
L7: 

            /** 				switch sym_tok do*/
            _0 = _sym_tok_63787;
            switch ( _0 ){ 

                /** 					case PROC, FUNC then*/
                case 27:
                case 501:

                /** 						patch_forward_call( tok, ref )*/
                Ref(_tok_63775);
                _40patch_forward_call(_tok_63775, _ref_63760);

                /** 						break "fr_type"*/
                goto L9; // [247] 456
                goto L9; // [249] 456

                /** 					case else*/
                default:

                /** 						forward_error( tok, ref )*/
                Ref(_tok_63775);
                _40forward_error(_tok_63775, _ref_63760);
            ;}            goto L9; // [262] 456

            /** 			case VARIABLE then*/
            case -100:

            /** 				sym_tok = SymTab[tok[T_SYM]][S_TOKEN]*/
            _2 = (int)SEQ_PTR(_tok_63775);
            _32114 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!IS_ATOM_INT(_32114)){
                _32115 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32114)->dbl));
            }
            else{
                _32115 = (int)*(((s1_ptr)_2)->base + _32114);
            }
            _2 = (int)SEQ_PTR(_32115);
            if (!IS_ATOM_INT(_38S_TOKEN_16603)){
                _sym_tok_63787 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
            }
            else{
                _sym_tok_63787 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
            }
            if (!IS_ATOM_INT(_sym_tok_63787)){
                _sym_tok_63787 = (long)DBL_PTR(_sym_tok_63787)->dbl;
            }
            _32115 = NOVALUE;

            /** 				if SymTab[tok[T_SYM]][S_SCOPE] = SC_UNDEFINED then*/
            _2 = (int)SEQ_PTR(_tok_63775);
            _32117 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!IS_ATOM_INT(_32117)){
                _32118 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32117)->dbl));
            }
            else{
                _32118 = (int)*(((s1_ptr)_2)->base + _32117);
            }
            _2 = (int)SEQ_PTR(_32118);
            _32119 = (int)*(((s1_ptr)_2)->base + 4);
            _32118 = NOVALUE;
            if (binary_op_a(NOTEQ, _32119, 9)){
                _32119 = NOVALUE;
                goto LA; // [312] 329
            }
            _32119 = NOVALUE;

            /** 					errors &= ref*/
            Append(&_errors_63756, _errors_63756, _ref_63760);

            /** 					continue*/
            DeRef(_fr_63762);
            _fr_63762 = NOVALUE;
            DeRef(_tok_63775);
            _tok_63775 = NOVALUE;
            goto L4; // [326] 486
LA: 

            /** 				switch sym_tok do*/
            _0 = _sym_tok_63787;
            switch ( _0 ){ 

                /** 					case CONSTANT, ENUM, VARIABLE then*/
                case 417:
                case 427:
                case -100:

                /** 						patch_forward_variable( tok, ref )*/
                Ref(_tok_63775);
                _40patch_forward_variable(_tok_63775, _ref_63760);

                /** 						break "fr_type"*/
                goto L9; // [352] 456
                goto L9; // [354] 456

                /** 					case else*/
                default:

                /** 						forward_error( tok, ref )*/
                Ref(_tok_63775);
                _40forward_error(_tok_63775, _ref_63760);
            ;}            goto L9; // [367] 456

            /** 			case TYPE_CHECK then*/
            case 65:

            /** 				patch_forward_type_check( tok, ref )*/
            Ref(_tok_63775);
            _40patch_forward_type_check(_tok_63775, _ref_63760);
            goto L9; // [379] 456

            /** 			case GLOBAL_INIT_CHECK then*/
            case 109:

            /** 				patch_forward_init_check( tok, ref )*/
            Ref(_tok_63775);
            _40patch_forward_init_check(_tok_63775, _ref_63760);
            goto L9; // [391] 456

            /** 			case CASE then*/
            case 186:

            /** 				patch_forward_case( tok, ref )*/
            Ref(_tok_63775);
            _40patch_forward_case(_tok_63775, _ref_63760);
            goto L9; // [403] 456

            /** 			case TYPE then*/
            case 504:

            /** 				patch_forward_type( tok, ref )*/
            Ref(_tok_63775);
            _40patch_forward_type(_tok_63775, _ref_63760);
            goto L9; // [415] 456

            /** 			case GOTO then*/
            case 188:

            /** 				patch_forward_goto( tok, ref )*/
            Ref(_tok_63775);
            _40patch_forward_goto(_tok_63775, _ref_63760);
            goto L9; // [427] 456

            /** 			case else*/
            default:

            /** 				InternalErr( 263, {fr[FR_TYPE], fr[FR_NAME]})*/
            _2 = (int)SEQ_PTR(_fr_63762);
            _32124 = (int)*(((s1_ptr)_2)->base + 1);
            _2 = (int)SEQ_PTR(_fr_63762);
            _32125 = (int)*(((s1_ptr)_2)->base + 2);
            Ref(_32125);
            Ref(_32124);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _32124;
            ((int *)_2)[2] = _32125;
            _32126 = MAKE_SEQ(_1);
            _32125 = NOVALUE;
            _32124 = NOVALUE;
            _46InternalErr(263, _32126);
            _32126 = NOVALUE;
        ;}L9: 

        /** 		if report_errors and sequence( forward_references[ref] ) then*/
        if (_report_errors_63754 == 0) {
            goto LB; // [458] 482
        }
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        _32128 = (int)*(((s1_ptr)_2)->base + _ref_63760);
        _32129 = IS_SEQUENCE(_32128);
        _32128 = NOVALUE;
        if (_32129 == 0)
        {
            _32129 = NOVALUE;
            goto LB; // [472] 482
        }
        else{
            _32129 = NOVALUE;
        }

        /** 			errors &= ref*/
        Append(&_errors_63756, _errors_63756, _ref_63760);
LB: 
        DeRef(_fr_63762);
        _fr_63762 = NOVALUE;
        DeRef(_tok_63775);
        _tok_63775 = NOVALUE;

        /** 	end for*/
L4: 
        _ar_63758 = _ar_63758 + -1;
        goto L1; // [486] 26
L2: 
        ;
    }

    /** 	return errors*/
    DeRefDS(_refs_63753);
    _32089 = NOVALUE;
    _32104 = NOVALUE;
    DeRef(_32092);
    _32092 = NOVALUE;
    DeRef(_32109);
    _32109 = NOVALUE;
    _32114 = NOVALUE;
    _32117 = NOVALUE;
    return _errors_63756;
    ;
}


int _40file_name_based_symindex_compare(int _si1_63865, int _si2_63866)
{
    int _fn1_63887 = NOVALUE;
    int _fn2_63892 = NOVALUE;
    int _32158 = NOVALUE;
    int _32157 = NOVALUE;
    int _32156 = NOVALUE;
    int _32155 = NOVALUE;
    int _32154 = NOVALUE;
    int _32153 = NOVALUE;
    int _32152 = NOVALUE;
    int _32151 = NOVALUE;
    int _32150 = NOVALUE;
    int _32149 = NOVALUE;
    int _32148 = NOVALUE;
    int _32147 = NOVALUE;
    int _32145 = NOVALUE;
    int _32143 = NOVALUE;
    int _32142 = NOVALUE;
    int _32141 = NOVALUE;
    int _32140 = NOVALUE;
    int _32139 = NOVALUE;
    int _32138 = NOVALUE;
    int _32137 = NOVALUE;
    int _32136 = NOVALUE;
    int _32135 = NOVALUE;
    int _32134 = NOVALUE;
    int _32132 = NOVALUE;
    int _32131 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_si1_63865)) {
        _1 = (long)(DBL_PTR(_si1_63865)->dbl);
        if (UNIQUE(DBL_PTR(_si1_63865)) && (DBL_PTR(_si1_63865)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_si1_63865);
        _si1_63865 = _1;
    }
    if (!IS_ATOM_INT(_si2_63866)) {
        _1 = (long)(DBL_PTR(_si2_63866)->dbl);
        if (UNIQUE(DBL_PTR(_si2_63866)) && (DBL_PTR(_si2_63866)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_si2_63866);
        _si2_63866 = _1;
    }

    /** 	if not symtab_index(si1) or not symtab_index(si2) then*/
    _32131 = _38symtab_index(_si1_63865);
    if (IS_ATOM_INT(_32131)) {
        _32132 = (_32131 == 0);
    }
    else {
        _32132 = unary_op(NOT, _32131);
    }
    DeRef(_32131);
    _32131 = NOVALUE;
    if (IS_ATOM_INT(_32132)) {
        if (_32132 != 0) {
            goto L1; // [14] 30
        }
    }
    else {
        if (DBL_PTR(_32132)->dbl != 0.0) {
            goto L1; // [14] 30
        }
    }
    _32134 = _38symtab_index(_si2_63866);
    if (IS_ATOM_INT(_32134)) {
        _32135 = (_32134 == 0);
    }
    else {
        _32135 = unary_op(NOT, _32134);
    }
    DeRef(_32134);
    _32134 = NOVALUE;
    if (_32135 == 0) {
        DeRef(_32135);
        _32135 = NOVALUE;
        goto L2; // [26] 37
    }
    else {
        if (!IS_ATOM_INT(_32135) && DBL_PTR(_32135)->dbl == 0.0){
            DeRef(_32135);
            _32135 = NOVALUE;
            goto L2; // [26] 37
        }
        DeRef(_32135);
        _32135 = NOVALUE;
    }
    DeRef(_32135);
    _32135 = NOVALUE;
L1: 

    /** 		return 1 -- put non symbols last*/
    DeRef(_32132);
    _32132 = NOVALUE;
    return 1;
L2: 

    /** 	if S_FILE_NO <= length(SymTab[si1]) and S_FILE_NO <= length(SymTab[si2]) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32136 = (int)*(((s1_ptr)_2)->base + _si1_63865);
    if (IS_SEQUENCE(_32136)){
            _32137 = SEQ_PTR(_32136)->length;
    }
    else {
        _32137 = 1;
    }
    _32136 = NOVALUE;
    if (IS_ATOM_INT(_38S_FILE_NO_16594)) {
        _32138 = (_38S_FILE_NO_16594 <= _32137);
    }
    else {
        _32138 = binary_op(LESSEQ, _38S_FILE_NO_16594, _32137);
    }
    _32137 = NOVALUE;
    if (IS_ATOM_INT(_32138)) {
        if (_32138 == 0) {
            goto L3; // [54] 186
        }
    }
    else {
        if (DBL_PTR(_32138)->dbl == 0.0) {
            goto L3; // [54] 186
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32140 = (int)*(((s1_ptr)_2)->base + _si2_63866);
    if (IS_SEQUENCE(_32140)){
            _32141 = SEQ_PTR(_32140)->length;
    }
    else {
        _32141 = 1;
    }
    _32140 = NOVALUE;
    if (IS_ATOM_INT(_38S_FILE_NO_16594)) {
        _32142 = (_38S_FILE_NO_16594 <= _32141);
    }
    else {
        _32142 = binary_op(LESSEQ, _38S_FILE_NO_16594, _32141);
    }
    _32141 = NOVALUE;
    if (_32142 == 0) {
        DeRef(_32142);
        _32142 = NOVALUE;
        goto L3; // [74] 186
    }
    else {
        if (!IS_ATOM_INT(_32142) && DBL_PTR(_32142)->dbl == 0.0){
            DeRef(_32142);
            _32142 = NOVALUE;
            goto L3; // [74] 186
        }
        DeRef(_32142);
        _32142 = NOVALUE;
    }
    DeRef(_32142);
    _32142 = NOVALUE;

    /** 		integer fn1 = SymTab[si1][S_FILE_NO], fn2 = SymTab[si2][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32143 = (int)*(((s1_ptr)_2)->base + _si1_63865);
    _2 = (int)SEQ_PTR(_32143);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _fn1_63887 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _fn1_63887 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_fn1_63887)){
        _fn1_63887 = (long)DBL_PTR(_fn1_63887)->dbl;
    }
    _32143 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32145 = (int)*(((s1_ptr)_2)->base + _si2_63866);
    _2 = (int)SEQ_PTR(_32145);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _fn2_63892 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _fn2_63892 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_fn2_63892)){
        _fn2_63892 = (long)DBL_PTR(_fn2_63892)->dbl;
    }
    _32145 = NOVALUE;

    /** 		if find(1,{fn1,fn2} > length(known_files) or {fn1,fn2} <= 0) then*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn1_63887;
    ((int *)_2)[2] = _fn2_63892;
    _32147 = MAKE_SEQ(_1);
    if (IS_SEQUENCE(_35known_files_15596)){
            _32148 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _32148 = 1;
    }
    _32149 = binary_op(GREATER, _32147, _32148);
    DeRefDS(_32147);
    _32147 = NOVALUE;
    _32148 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn1_63887;
    ((int *)_2)[2] = _fn2_63892;
    _32150 = MAKE_SEQ(_1);
    _32151 = binary_op(LESSEQ, _32150, 0);
    DeRefDS(_32150);
    _32150 = NOVALUE;
    _32152 = binary_op(OR, _32149, _32151);
    DeRefDS(_32149);
    _32149 = NOVALUE;
    DeRefDS(_32151);
    _32151 = NOVALUE;
    _32153 = find_from(1, _32152, 1);
    DeRefDS(_32152);
    _32152 = NOVALUE;
    if (_32153 == 0)
    {
        _32153 = NOVALUE;
        goto L4; // [139] 149
    }
    else{
        _32153 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_32132);
    _32132 = NOVALUE;
    _32136 = NOVALUE;
    DeRef(_32138);
    _32138 = NOVALUE;
    _32140 = NOVALUE;
    return 1;
L4: 

    /** 		return compare(abbreviate_path(known_files[fn1]),*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _32154 = (int)*(((s1_ptr)_2)->base + _fn1_63887);
    Ref(_32154);
    RefDS(_22663);
    _32155 = _13abbreviate_path(_32154, _22663);
    _32154 = NOVALUE;
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _32156 = (int)*(((s1_ptr)_2)->base + _fn2_63892);
    Ref(_32156);
    RefDS(_22663);
    _32157 = _13abbreviate_path(_32156, _22663);
    _32156 = NOVALUE;
    if (IS_ATOM_INT(_32155) && IS_ATOM_INT(_32157)){
        _32158 = (_32155 < _32157) ? -1 : (_32155 > _32157);
    }
    else{
        _32158 = compare(_32155, _32157);
    }
    DeRef(_32155);
    _32155 = NOVALUE;
    DeRef(_32157);
    _32157 = NOVALUE;
    DeRef(_32132);
    _32132 = NOVALUE;
    _32136 = NOVALUE;
    DeRef(_32138);
    _32138 = NOVALUE;
    _32140 = NOVALUE;
    return _32158;
    goto L5; // [183] 193
L3: 

    /** 		return 1 -- put non-names last*/
    DeRef(_32132);
    _32132 = NOVALUE;
    _32136 = NOVALUE;
    DeRef(_32138);
    _32138 = NOVALUE;
    _32140 = NOVALUE;
    return 1;
L5: 
    ;
}


void _40Resolve_forward_references(int _report_errors_63918)
{
    int _errors_63919 = NOVALUE;
    int _unincluded_ok_63920 = NOVALUE;
    int _msg_63981 = NOVALUE;
    int _errloc_63982 = NOVALUE;
    int _ref_63987 = NOVALUE;
    int _tok_64003 = NOVALUE;
    int _THIS_SCOPE_64005 = NOVALUE;
    int _THESE_GLOBALS_64006 = NOVALUE;
    int _syms_64064 = NOVALUE;
    int _s_64085 = NOVALUE;
    int _32289 = NOVALUE;
    int _32287 = NOVALUE;
    int _32282 = NOVALUE;
    int _32279 = NOVALUE;
    int _32277 = NOVALUE;
    int _32276 = NOVALUE;
    int _32275 = NOVALUE;
    int _32274 = NOVALUE;
    int _32273 = NOVALUE;
    int _32272 = NOVALUE;
    int _32271 = NOVALUE;
    int _32269 = NOVALUE;
    int _32268 = NOVALUE;
    int _32267 = NOVALUE;
    int _32265 = NOVALUE;
    int _32263 = NOVALUE;
    int _32262 = NOVALUE;
    int _32261 = NOVALUE;
    int _32260 = NOVALUE;
    int _32259 = NOVALUE;
    int _32258 = NOVALUE;
    int _32255 = NOVALUE;
    int _32251 = NOVALUE;
    int _32250 = NOVALUE;
    int _32249 = NOVALUE;
    int _32248 = NOVALUE;
    int _32247 = NOVALUE;
    int _32246 = NOVALUE;
    int _32243 = NOVALUE;
    int _32242 = NOVALUE;
    int _32241 = NOVALUE;
    int _32240 = NOVALUE;
    int _32239 = NOVALUE;
    int _32238 = NOVALUE;
    int _32235 = NOVALUE;
    int _32234 = NOVALUE;
    int _32233 = NOVALUE;
    int _32232 = NOVALUE;
    int _32231 = NOVALUE;
    int _32230 = NOVALUE;
    int _32229 = NOVALUE;
    int _32228 = NOVALUE;
    int _32227 = NOVALUE;
    int _32226 = NOVALUE;
    int _32223 = NOVALUE;
    int _32221 = NOVALUE;
    int _32218 = NOVALUE;
    int _32216 = NOVALUE;
    int _32214 = NOVALUE;
    int _32213 = NOVALUE;
    int _32211 = NOVALUE;
    int _32210 = NOVALUE;
    int _32209 = NOVALUE;
    int _32208 = NOVALUE;
    int _32207 = NOVALUE;
    int _32205 = NOVALUE;
    int _32204 = NOVALUE;
    int _32202 = NOVALUE;
    int _32201 = NOVALUE;
    int _32199 = NOVALUE;
    int _32198 = NOVALUE;
    int _32196 = NOVALUE;
    int _32195 = NOVALUE;
    int _32194 = NOVALUE;
    int _32193 = NOVALUE;
    int _32192 = NOVALUE;
    int _32191 = NOVALUE;
    int _32190 = NOVALUE;
    int _32189 = NOVALUE;
    int _32188 = NOVALUE;
    int _32187 = NOVALUE;
    int _32186 = NOVALUE;
    int _32185 = NOVALUE;
    int _32184 = NOVALUE;
    int _32183 = NOVALUE;
    int _32182 = NOVALUE;
    int _32181 = NOVALUE;
    int _32179 = NOVALUE;
    int _32178 = NOVALUE;
    int _32177 = NOVALUE;
    int _32176 = NOVALUE;
    int _32174 = NOVALUE;
    int _32173 = NOVALUE;
    int _32171 = NOVALUE;
    int _32170 = NOVALUE;
    int _32169 = NOVALUE;
    int _32168 = NOVALUE;
    int _32166 = NOVALUE;
    int _32165 = NOVALUE;
    int _32164 = NOVALUE;
    int _32163 = NOVALUE;
    int _32161 = NOVALUE;
    int _32160 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_report_errors_63918)) {
        _1 = (long)(DBL_PTR(_report_errors_63918)->dbl);
        if (UNIQUE(DBL_PTR(_report_errors_63918)) && (DBL_PTR(_report_errors_63918)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_report_errors_63918);
        _report_errors_63918 = _1;
    }

    /** 	sequence errors = {}*/
    RefDS(_22663);
    DeRef(_errors_63919);
    _errors_63919 = _22663;

    /** 	integer unincluded_ok = get_resolve_unincluded_globals()*/
    _unincluded_ok_63920 = _55get_resolve_unincluded_globals();
    if (!IS_ATOM_INT(_unincluded_ok_63920)) {
        _1 = (long)(DBL_PTR(_unincluded_ok_63920)->dbl);
        if (UNIQUE(DBL_PTR(_unincluded_ok_63920)) && (DBL_PTR(_unincluded_ok_63920)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_unincluded_ok_63920);
        _unincluded_ok_63920 = _1;
    }

    /** 	if length( active_references ) < length( known_files ) then*/
    if (IS_SEQUENCE(_40active_references_62505)){
            _32160 = SEQ_PTR(_40active_references_62505)->length;
    }
    else {
        _32160 = 1;
    }
    if (IS_SEQUENCE(_35known_files_15596)){
            _32161 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _32161 = 1;
    }
    if (_32160 >= _32161)
    goto L1; // [29] 86

    /** 		active_references &= repeat( {}, length( known_files ) - length( active_references ) )*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _32163 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _32163 = 1;
    }
    if (IS_SEQUENCE(_40active_references_62505)){
            _32164 = SEQ_PTR(_40active_references_62505)->length;
    }
    else {
        _32164 = 1;
    }
    _32165 = _32163 - _32164;
    _32163 = NOVALUE;
    _32164 = NOVALUE;
    _32166 = Repeat(_22663, _32165);
    _32165 = NOVALUE;
    Concat((object_ptr)&_40active_references_62505, _40active_references_62505, _32166);
    DeRefDS(_32166);
    _32166 = NOVALUE;

    /** 		active_subprogs   &= repeat( {}, length( known_files ) - length( active_subprogs ) )*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _32168 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _32168 = 1;
    }
    if (IS_SEQUENCE(_40active_subprogs_62504)){
            _32169 = SEQ_PTR(_40active_subprogs_62504)->length;
    }
    else {
        _32169 = 1;
    }
    _32170 = _32168 - _32169;
    _32168 = NOVALUE;
    _32169 = NOVALUE;
    _32171 = Repeat(_22663, _32170);
    _32170 = NOVALUE;
    Concat((object_ptr)&_40active_subprogs_62504, _40active_subprogs_62504, _32171);
    DeRefDS(_32171);
    _32171 = NOVALUE;
L1: 

    /** 	if length( toplevel_references ) < length( known_files ) then*/
    if (IS_SEQUENCE(_40toplevel_references_62506)){
            _32173 = SEQ_PTR(_40toplevel_references_62506)->length;
    }
    else {
        _32173 = 1;
    }
    if (IS_SEQUENCE(_35known_files_15596)){
            _32174 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _32174 = 1;
    }
    if (_32173 >= _32174)
    goto L2; // [98] 129

    /** 		toplevel_references &= repeat( {}, length( known_files ) - length( toplevel_references ) )*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _32176 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _32176 = 1;
    }
    if (IS_SEQUENCE(_40toplevel_references_62506)){
            _32177 = SEQ_PTR(_40toplevel_references_62506)->length;
    }
    else {
        _32177 = 1;
    }
    _32178 = _32176 - _32177;
    _32176 = NOVALUE;
    _32177 = NOVALUE;
    _32179 = Repeat(_22663, _32178);
    _32178 = NOVALUE;
    Concat((object_ptr)&_40toplevel_references_62506, _40toplevel_references_62506, _32179);
    DeRefDS(_32179);
    _32179 = NOVALUE;
L2: 

    /** 	for i = 1 to length( active_subprogs ) do*/
    if (IS_SEQUENCE(_40active_subprogs_62504)){
            _32181 = SEQ_PTR(_40active_subprogs_62504)->length;
    }
    else {
        _32181 = 1;
    }
    {
        int _i_63952;
        _i_63952 = 1;
L3: 
        if (_i_63952 > _32181){
            goto L4; // [136] 280
        }

        /** 		if (length( active_subprogs[i] ) or length(toplevel_references[i])) */
        _2 = (int)SEQ_PTR(_40active_subprogs_62504);
        _32182 = (int)*(((s1_ptr)_2)->base + _i_63952);
        if (IS_SEQUENCE(_32182)){
                _32183 = SEQ_PTR(_32182)->length;
        }
        else {
            _32183 = 1;
        }
        _32182 = NOVALUE;
        if (_32183 != 0) {
            _32184 = 1;
            goto L5; // [154] 171
        }
        _2 = (int)SEQ_PTR(_40toplevel_references_62506);
        _32185 = (int)*(((s1_ptr)_2)->base + _i_63952);
        if (IS_SEQUENCE(_32185)){
                _32186 = SEQ_PTR(_32185)->length;
        }
        else {
            _32186 = 1;
        }
        _32185 = NOVALUE;
        _32184 = (_32186 != 0);
L5: 
        if (_32184 == 0) {
            goto L6; // [171] 273
        }
        _32188 = (_i_63952 == _38current_file_no_16946);
        if (_32188 != 0) {
            _32189 = 1;
            goto L7; // [181] 195
        }
        _2 = (int)SEQ_PTR(_35finished_files_15598);
        _32190 = (int)*(((s1_ptr)_2)->base + _i_63952);
        _32189 = (_32190 != 0);
L7: 
        if (_32189 != 0) {
            DeRef(_32191);
            _32191 = 1;
            goto L8; // [195] 203
        }
        _32191 = (_unincluded_ok_63920 != 0);
L8: 
        if (_32191 == 0)
        {
            _32191 = NOVALUE;
            goto L6; // [204] 273
        }
        else{
            _32191 = NOVALUE;
        }

        /** 			for j = length( active_references[i] ) to 1 by -1 do*/
        _2 = (int)SEQ_PTR(_40active_references_62505);
        _32192 = (int)*(((s1_ptr)_2)->base + _i_63952);
        if (IS_SEQUENCE(_32192)){
                _32193 = SEQ_PTR(_32192)->length;
        }
        else {
            _32193 = 1;
        }
        _32192 = NOVALUE;
        {
            int _j_63968;
            _j_63968 = _32193;
L9: 
            if (_j_63968 < 1){
                goto LA; // [218] 254
            }

            /** 				errors &= resolve_file( active_references[i][j], report_errors, unincluded_ok )*/
            _2 = (int)SEQ_PTR(_40active_references_62505);
            _32194 = (int)*(((s1_ptr)_2)->base + _i_63952);
            _2 = (int)SEQ_PTR(_32194);
            _32195 = (int)*(((s1_ptr)_2)->base + _j_63968);
            _32194 = NOVALUE;
            Ref(_32195);
            _32196 = _40resolve_file(_32195, _report_errors_63918, _unincluded_ok_63920);
            _32195 = NOVALUE;
            if (IS_SEQUENCE(_errors_63919) && IS_ATOM(_32196)) {
                Ref(_32196);
                Append(&_errors_63919, _errors_63919, _32196);
            }
            else if (IS_ATOM(_errors_63919) && IS_SEQUENCE(_32196)) {
            }
            else {
                Concat((object_ptr)&_errors_63919, _errors_63919, _32196);
            }
            DeRef(_32196);
            _32196 = NOVALUE;

            /** 			end for*/
            _j_63968 = _j_63968 + -1;
            goto L9; // [249] 225
LA: 
            ;
        }

        /** 			errors &= resolve_file( toplevel_references[i], report_errors, unincluded_ok )*/
        _2 = (int)SEQ_PTR(_40toplevel_references_62506);
        _32198 = (int)*(((s1_ptr)_2)->base + _i_63952);
        Ref(_32198);
        _32199 = _40resolve_file(_32198, _report_errors_63918, _unincluded_ok_63920);
        _32198 = NOVALUE;
        if (IS_SEQUENCE(_errors_63919) && IS_ATOM(_32199)) {
            Ref(_32199);
            Append(&_errors_63919, _errors_63919, _32199);
        }
        else if (IS_ATOM(_errors_63919) && IS_SEQUENCE(_32199)) {
        }
        else {
            Concat((object_ptr)&_errors_63919, _errors_63919, _32199);
        }
        DeRef(_32199);
        _32199 = NOVALUE;
L6: 

        /** 	end for*/
        _i_63952 = _i_63952 + 1;
        goto L3; // [275] 143
L4: 
        ;
    }

    /** 	if report_errors and length( errors ) then*/
    if (_report_errors_63918 == 0) {
        goto LB; // [282] 900
    }
    if (IS_SEQUENCE(_errors_63919)){
            _32202 = SEQ_PTR(_errors_63919)->length;
    }
    else {
        _32202 = 1;
    }
    if (_32202 == 0)
    {
        _32202 = NOVALUE;
        goto LB; // [290] 900
    }
    else{
        _32202 = NOVALUE;
    }

    /** 		sequence msg = ""*/
    RefDS(_22663);
    DeRefi(_msg_63981);
    _msg_63981 = _22663;

    /** 		sequence errloc = "Internal Error - Unknown Error Message"*/
    RefDS(_32203);
    DeRefi(_errloc_63982);
    _errloc_63982 = _32203;

    /** 		for e = length(errors) to 1 by -1 do*/
    if (IS_SEQUENCE(_errors_63919)){
            _32204 = SEQ_PTR(_errors_63919)->length;
    }
    else {
        _32204 = 1;
    }
    {
        int _e_63985;
        _e_63985 = _32204;
LC: 
        if (_e_63985 < 1){
            goto LD; // [312] 874
        }

        /** 			sequence ref = forward_references[errors[e]]*/
        _2 = (int)SEQ_PTR(_errors_63919);
        _32205 = (int)*(((s1_ptr)_2)->base + _e_63985);
        DeRef(_ref_63987);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!IS_ATOM_INT(_32205)){
            _ref_63987 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32205)->dbl));
        }
        else{
            _ref_63987 = (int)*(((s1_ptr)_2)->base + _32205);
        }
        Ref(_ref_63987);

        /** 			if (ref[FR_TYPE] = TYPE_CHECK and ref[FR_OP] = TYPE_CHECK) or ref[FR_TYPE] = GLOBAL_INIT_CHECK then*/
        _2 = (int)SEQ_PTR(_ref_63987);
        _32207 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32207)) {
            _32208 = (_32207 == 65);
        }
        else {
            _32208 = binary_op(EQUALS, _32207, 65);
        }
        _32207 = NOVALUE;
        if (IS_ATOM_INT(_32208)) {
            if (_32208 == 0) {
                DeRef(_32209);
                _32209 = 0;
                goto LE; // [347] 367
            }
        }
        else {
            if (DBL_PTR(_32208)->dbl == 0.0) {
                DeRef(_32209);
                _32209 = 0;
                goto LE; // [347] 367
            }
        }
        _2 = (int)SEQ_PTR(_ref_63987);
        _32210 = (int)*(((s1_ptr)_2)->base + 10);
        if (IS_ATOM_INT(_32210)) {
            _32211 = (_32210 == 65);
        }
        else {
            _32211 = binary_op(EQUALS, _32210, 65);
        }
        _32210 = NOVALUE;
        DeRef(_32209);
        if (IS_ATOM_INT(_32211))
        _32209 = (_32211 != 0);
        else
        _32209 = DBL_PTR(_32211)->dbl != 0.0;
LE: 
        if (_32209 != 0) {
            goto LF; // [367] 388
        }
        _2 = (int)SEQ_PTR(_ref_63987);
        _32213 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32213)) {
            _32214 = (_32213 == 109);
        }
        else {
            _32214 = binary_op(EQUALS, _32213, 109);
        }
        _32213 = NOVALUE;
        if (_32214 == 0) {
            DeRef(_32214);
            _32214 = NOVALUE;
            goto L10; // [384] 397
        }
        else {
            if (!IS_ATOM_INT(_32214) && DBL_PTR(_32214)->dbl == 0.0){
                DeRef(_32214);
                _32214 = NOVALUE;
                goto L10; // [384] 397
            }
            DeRef(_32214);
            _32214 = NOVALUE;
        }
        DeRef(_32214);
        _32214 = NOVALUE;
LF: 

        /** 				continue*/
        DeRef(_ref_63987);
        _ref_63987 = NOVALUE;
        goto L11; // [392] 869
        goto L12; // [394] 827
L10: 

        /** 				object tok = find_reference(ref)*/
        RefDS(_ref_63987);
        _0 = _tok_64003;
        _tok_64003 = _40find_reference(_ref_63987);
        DeRef(_0);

        /** 				integer THIS_SCOPE = 3*/
        _THIS_SCOPE_64005 = 3;

        /** 				integer THESE_GLOBALS = 4*/
        _THESE_GLOBALS_64006 = 4;

        /** 				if tok[T_ID] = IGNORED then*/
        _2 = (int)SEQ_PTR(_tok_64003);
        _32216 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _32216, 509)){
            _32216 = NOVALUE;
            goto L13; // [423] 798
        }
        _32216 = NOVALUE;

        /** 					switch tok[THIS_SCOPE] do*/
        _2 = (int)SEQ_PTR(_tok_64003);
        _32218 = (int)*(((s1_ptr)_2)->base + 3);
        if (IS_SEQUENCE(_32218) ){
            goto L14; // [433] 794
        }
        if(!IS_ATOM_INT(_32218)){
            if( (DBL_PTR(_32218)->dbl != (double) ((int) DBL_PTR(_32218)->dbl) ) ){
                goto L14; // [433] 794
            }
            _0 = (int) DBL_PTR(_32218)->dbl;
        }
        else {
            _0 = _32218;
        };
        _32218 = NOVALUE;
        switch ( _0 ){ 

            /** 						case SC_UNDEFINED then*/
            case 9:

            /** 							if ref[FR_QUALIFIED] != -1 then*/
            _2 = (int)SEQ_PTR(_ref_63987);
            _32221 = (int)*(((s1_ptr)_2)->base + 9);
            if (binary_op_a(EQUALS, _32221, -1)){
                _32221 = NOVALUE;
                goto L15; // [450] 580
            }
            _32221 = NOVALUE;

            /** 								if ref[FR_QUALIFIED] > 0 then*/
            _2 = (int)SEQ_PTR(_ref_63987);
            _32223 = (int)*(((s1_ptr)_2)->base + 9);
            if (binary_op_a(LESSEQ, _32223, 0)){
                _32223 = NOVALUE;
                goto L16; // [462] 535
            }
            _32223 = NOVALUE;

            /** 									errloc = sprintf("\t\'%s\' (%s:%d) was not declared in \'%s\'.\n", */
            _2 = (int)SEQ_PTR(_ref_63987);
            _32226 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_63987);
            _32227 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_35known_files_15596);
            if (!IS_ATOM_INT(_32227)){
                _32228 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32227)->dbl));
            }
            else{
                _32228 = (int)*(((s1_ptr)_2)->base + _32227);
            }
            Ref(_32228);
            RefDS(_22663);
            _32229 = _13abbreviate_path(_32228, _22663);
            _32228 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_63987);
            _32230 = (int)*(((s1_ptr)_2)->base + 6);
            _2 = (int)SEQ_PTR(_ref_63987);
            _32231 = (int)*(((s1_ptr)_2)->base + 9);
            _2 = (int)SEQ_PTR(_35known_files_15596);
            if (!IS_ATOM_INT(_32231)){
                _32232 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32231)->dbl));
            }
            else{
                _32232 = (int)*(((s1_ptr)_2)->base + _32231);
            }
            Ref(_32232);
            RefDS(_22663);
            _32233 = _13abbreviate_path(_32232, _22663);
            _32232 = NOVALUE;
            _32234 = _12find_replace(92, _32233, 47, 0);
            _32233 = NOVALUE;
            _1 = NewS1(4);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32226);
            *((int *)(_2+4)) = _32226;
            *((int *)(_2+8)) = _32229;
            Ref(_32230);
            *((int *)(_2+12)) = _32230;
            *((int *)(_2+16)) = _32234;
            _32235 = MAKE_SEQ(_1);
            _32234 = NOVALUE;
            _32230 = NOVALUE;
            _32229 = NOVALUE;
            _32226 = NOVALUE;
            DeRefi(_errloc_63982);
            _errloc_63982 = EPrintf(-9999999, _32225, _32235);
            DeRefDS(_32235);
            _32235 = NOVALUE;
            goto L17; // [532] 797
L16: 

            /** 									errloc = sprintf("\t\'%s\' (%s:%d) is not a builtin.\n", */
            _2 = (int)SEQ_PTR(_ref_63987);
            _32238 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_63987);
            _32239 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_35known_files_15596);
            if (!IS_ATOM_INT(_32239)){
                _32240 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32239)->dbl));
            }
            else{
                _32240 = (int)*(((s1_ptr)_2)->base + _32239);
            }
            Ref(_32240);
            RefDS(_22663);
            _32241 = _13abbreviate_path(_32240, _22663);
            _32240 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_63987);
            _32242 = (int)*(((s1_ptr)_2)->base + 6);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32238);
            *((int *)(_2+4)) = _32238;
            *((int *)(_2+8)) = _32241;
            Ref(_32242);
            *((int *)(_2+12)) = _32242;
            _32243 = MAKE_SEQ(_1);
            _32242 = NOVALUE;
            _32241 = NOVALUE;
            _32238 = NOVALUE;
            DeRefi(_errloc_63982);
            _errloc_63982 = EPrintf(-9999999, _32237, _32243);
            DeRefDS(_32243);
            _32243 = NOVALUE;
            goto L17; // [577] 797
L15: 

            /** 								errloc = sprintf("\t\'%s\' (%s:%d) has not been declared.\n", */
            _2 = (int)SEQ_PTR(_ref_63987);
            _32246 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_63987);
            _32247 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_35known_files_15596);
            if (!IS_ATOM_INT(_32247)){
                _32248 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32247)->dbl));
            }
            else{
                _32248 = (int)*(((s1_ptr)_2)->base + _32247);
            }
            Ref(_32248);
            RefDS(_22663);
            _32249 = _13abbreviate_path(_32248, _22663);
            _32248 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_63987);
            _32250 = (int)*(((s1_ptr)_2)->base + 6);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32246);
            *((int *)(_2+4)) = _32246;
            *((int *)(_2+8)) = _32249;
            Ref(_32250);
            *((int *)(_2+12)) = _32250;
            _32251 = MAKE_SEQ(_1);
            _32250 = NOVALUE;
            _32249 = NOVALUE;
            _32246 = NOVALUE;
            DeRefi(_errloc_63982);
            _errloc_63982 = EPrintf(-9999999, _32245, _32251);
            DeRefDS(_32251);
            _32251 = NOVALUE;
            goto L17; // [622] 797

            /** 						case SC_MULTIPLY_DEFINED then*/
            case 10:

            /** 							sequence syms = tok[THESE_GLOBALS] -- there should be no forward references in here.*/
            DeRef(_syms_64064);
            _2 = (int)SEQ_PTR(_tok_64003);
            _syms_64064 = (int)*(((s1_ptr)_2)->base + _THESE_GLOBALS_64006);
            Ref(_syms_64064);

            /** 							syms = custom_sort(routine_id("file_name_based_symindex_compare"), syms,, ASCENDING)*/
            _32255 = CRoutineId(1355, 40, _32254);
            RefDS(_syms_64064);
            RefDS(_22663);
            _0 = _syms_64064;
            _syms_64064 = _22custom_sort(_32255, _syms_64064, _22663, 1);
            DeRefDS(_0);
            _32255 = NOVALUE;

            /** 							errloc = sprintf("\t\'%s\' (%s:%d) has been declared more than once.\n", */
            _2 = (int)SEQ_PTR(_ref_63987);
            _32258 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_ref_63987);
            _32259 = (int)*(((s1_ptr)_2)->base + 3);
            _2 = (int)SEQ_PTR(_35known_files_15596);
            if (!IS_ATOM_INT(_32259)){
                _32260 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32259)->dbl));
            }
            else{
                _32260 = (int)*(((s1_ptr)_2)->base + _32259);
            }
            Ref(_32260);
            RefDS(_22663);
            _32261 = _13abbreviate_path(_32260, _22663);
            _32260 = NOVALUE;
            _2 = (int)SEQ_PTR(_ref_63987);
            _32262 = (int)*(((s1_ptr)_2)->base + 6);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_32258);
            *((int *)(_2+4)) = _32258;
            *((int *)(_2+8)) = _32261;
            Ref(_32262);
            *((int *)(_2+12)) = _32262;
            _32263 = MAKE_SEQ(_1);
            _32262 = NOVALUE;
            _32261 = NOVALUE;
            _32258 = NOVALUE;
            DeRefi(_errloc_63982);
            _errloc_63982 = EPrintf(-9999999, _32257, _32263);
            DeRefDS(_32263);
            _32263 = NOVALUE;

            /** 							for si = 1 to length(syms) do*/
            if (IS_SEQUENCE(_syms_64064)){
                    _32265 = SEQ_PTR(_syms_64064)->length;
            }
            else {
                _32265 = 1;
            }
            {
                int _si_64082;
                _si_64082 = 1;
L18: 
                if (_si_64082 > _32265){
                    goto L19; // [700] 788
                }

                /** 								symtab_index s = syms[si] */
                _2 = (int)SEQ_PTR(_syms_64064);
                _s_64085 = (int)*(((s1_ptr)_2)->base + _si_64082);
                if (!IS_ATOM_INT(_s_64085)){
                    _s_64085 = (long)DBL_PTR(_s_64085)->dbl;
                }

                /** 								if equal(ref[FR_NAME], sym_name(s)) then*/
                _2 = (int)SEQ_PTR(_ref_63987);
                _32267 = (int)*(((s1_ptr)_2)->base + 2);
                _32268 = _55sym_name(_s_64085);
                if (_32267 == _32268)
                _32269 = 1;
                else if (IS_ATOM_INT(_32267) && IS_ATOM_INT(_32268))
                _32269 = 0;
                else
                _32269 = (compare(_32267, _32268) == 0);
                _32267 = NOVALUE;
                DeRef(_32268);
                _32268 = NOVALUE;
                if (_32269 == 0)
                {
                    _32269 = NOVALUE;
                    goto L1A; // [731] 779
                }
                else{
                    _32269 = NOVALUE;
                }

                /** 									errloc &= sprintf("\t\tin %s\n", */
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _32271 = (int)*(((s1_ptr)_2)->base + _s_64085);
                _2 = (int)SEQ_PTR(_32271);
                if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
                    _32272 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
                }
                else{
                    _32272 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
                }
                _32271 = NOVALUE;
                _2 = (int)SEQ_PTR(_35known_files_15596);
                if (!IS_ATOM_INT(_32272)){
                    _32273 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32272)->dbl));
                }
                else{
                    _32273 = (int)*(((s1_ptr)_2)->base + _32272);
                }
                Ref(_32273);
                RefDS(_22663);
                _32274 = _13abbreviate_path(_32273, _22663);
                _32273 = NOVALUE;
                _32275 = _12find_replace(92, _32274, 47, 0);
                _32274 = NOVALUE;
                _1 = NewS1(1);
                _2 = (int)((s1_ptr)_1)->base;
                *((int *)(_2+4)) = _32275;
                _32276 = MAKE_SEQ(_1);
                _32275 = NOVALUE;
                _32277 = EPrintf(-9999999, _32270, _32276);
                DeRefDS(_32276);
                _32276 = NOVALUE;
                Concat((object_ptr)&_errloc_63982, _errloc_63982, _32277);
                DeRefDS(_32277);
                _32277 = NOVALUE;
L1A: 

                /** 							end for*/
                _si_64082 = _si_64082 + 1;
                goto L18; // [783] 707
L19: 
                ;
            }
            DeRef(_syms_64064);
            _syms_64064 = NOVALUE;
            goto L17; // [790] 797

            /** 						case else */
            default:
L14: 
        ;}L17: 
L13: 

        /** 				if not match(errloc, msg) then*/
        _32279 = e_match_from(_errloc_63982, _msg_63981, 1);
        if (_32279 != 0)
        goto L1B; // [805] 824
        _32279 = NOVALUE;

        /** 					msg &= errloc*/
        Concat((object_ptr)&_msg_63981, _msg_63981, _errloc_63982);

        /** 					prep_forward_error( errors[e] )*/
        _2 = (int)SEQ_PTR(_errors_63919);
        _32282 = (int)*(((s1_ptr)_2)->base + _e_63985);
        Ref(_32282);
        _40prep_forward_error(_32282);
        _32282 = NOVALUE;
L1B: 
        DeRef(_tok_64003);
        _tok_64003 = NOVALUE;
L12: 

        /** 			ThisLine    = ref[FR_THISLINE]*/
        DeRef(_46ThisLine_49482);
        _2 = (int)SEQ_PTR(_ref_63987);
        _46ThisLine_49482 = (int)*(((s1_ptr)_2)->base + 7);
        Ref(_46ThisLine_49482);

        /** 			bp          = ref[FR_BP]*/
        _2 = (int)SEQ_PTR(_ref_63987);
        _46bp_49486 = (int)*(((s1_ptr)_2)->base + 8);
        if (!IS_ATOM_INT(_46bp_49486)){
            _46bp_49486 = (long)DBL_PTR(_46bp_49486)->dbl;
        }

        /** 			CurrentSub  = ref[FR_SUBPROG]*/
        _2 = (int)SEQ_PTR(_ref_63987);
        _38CurrentSub_16954 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_38CurrentSub_16954)){
            _38CurrentSub_16954 = (long)DBL_PTR(_38CurrentSub_16954)->dbl;
        }

        /** 			line_number = ref[FR_LINE]*/
        _2 = (int)SEQ_PTR(_ref_63987);
        _38line_number_16947 = (int)*(((s1_ptr)_2)->base + 6);
        if (!IS_ATOM_INT(_38line_number_16947)){
            _38line_number_16947 = (long)DBL_PTR(_38line_number_16947)->dbl;
        }
        DeRefDS(_ref_63987);
        _ref_63987 = NOVALUE;

        /** 		end for*/
L11: 
        _e_63985 = _e_63985 + -1;
        goto LC; // [869] 319
LD: 
        ;
    }

    /** 		if length(msg) > 0 then*/
    if (IS_SEQUENCE(_msg_63981)){
            _32287 = SEQ_PTR(_msg_63981)->length;
    }
    else {
        _32287 = 1;
    }
    if (_32287 <= 0)
    goto L1C; // [879] 895

    /** 			CompileErr( 74, {msg} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_msg_63981);
    *((int *)(_2+4)) = _msg_63981;
    _32289 = MAKE_SEQ(_1);
    _46CompileErr(74, _32289, 0);
    _32289 = NOVALUE;
L1C: 
    DeRefi(_msg_63981);
    _msg_63981 = NOVALUE;
    DeRefi(_errloc_63982);
    _errloc_63982 = NOVALUE;
    goto L1D; // [897] 935
LB: 

    /** 	elsif report_errors then*/
    if (_report_errors_63918 == 0)
    {
        goto L1E; // [902] 934
    }
    else{
    }

    /** 		forward_references  = {}*/
    RefDS(_22663);
    DeRef(_40forward_references_62503);
    _40forward_references_62503 = _22663;

    /** 		active_references   = {}*/
    RefDS(_22663);
    DeRef(_40active_references_62505);
    _40active_references_62505 = _22663;

    /** 		toplevel_references = {}*/
    RefDS(_22663);
    DeRef(_40toplevel_references_62506);
    _40toplevel_references_62506 = _22663;

    /** 		inactive_references = {}*/
    RefDS(_22663);
    DeRef(_40inactive_references_62507);
    _40inactive_references_62507 = _22663;
L1E: 
L1D: 

    /** 	clear_last()*/
    _43clear_last();

    /** end procedure*/
    DeRef(_errors_63919);
    _32182 = NOVALUE;
    _32185 = NOVALUE;
    DeRef(_32188);
    _32188 = NOVALUE;
    _32190 = NOVALUE;
    _32192 = NOVALUE;
    _32205 = NOVALUE;
    _32272 = NOVALUE;
    DeRef(_32208);
    _32208 = NOVALUE;
    DeRef(_32211);
    _32211 = NOVALUE;
    _32227 = NOVALUE;
    _32239 = NOVALUE;
    _32247 = NOVALUE;
    _32231 = NOVALUE;
    _32259 = NOVALUE;
    return;
    ;
}


void _40shift_these(int _refs_64129, int _pc_64130, int _amount_64131)
{
    int _fr_64135 = NOVALUE;
    int _32307 = NOVALUE;
    int _32306 = NOVALUE;
    int _32305 = NOVALUE;
    int _32304 = NOVALUE;
    int _32303 = NOVALUE;
    int _32302 = NOVALUE;
    int _32301 = NOVALUE;
    int _32300 = NOVALUE;
    int _32299 = NOVALUE;
    int _32298 = NOVALUE;
    int _32296 = NOVALUE;
    int _32294 = NOVALUE;
    int _32293 = NOVALUE;
    int _32291 = NOVALUE;
    int _32290 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length( refs ) to 1 by -1 do*/
    if (IS_SEQUENCE(_refs_64129)){
            _32290 = SEQ_PTR(_refs_64129)->length;
    }
    else {
        _32290 = 1;
    }
    {
        int _i_64133;
        _i_64133 = _32290;
L1: 
        if (_i_64133 < 1){
            goto L2; // [12] 159
        }

        /** 		sequence fr = forward_references[refs[i]]*/
        _2 = (int)SEQ_PTR(_refs_64129);
        _32291 = (int)*(((s1_ptr)_2)->base + _i_64133);
        DeRef(_fr_64135);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!IS_ATOM_INT(_32291)){
            _fr_64135 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32291)->dbl));
        }
        else{
            _fr_64135 = (int)*(((s1_ptr)_2)->base + _32291);
        }
        Ref(_fr_64135);

        /** 		forward_references[refs[i]] = 0*/
        _2 = (int)SEQ_PTR(_refs_64129);
        _32293 = (int)*(((s1_ptr)_2)->base + _i_64133);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _40forward_references_62503 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32293))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32293)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32293);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);

        /** 		if fr[FR_SUBPROG] = shifting_sub then*/
        _2 = (int)SEQ_PTR(_fr_64135);
        _32294 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(NOTEQ, _32294, _40shifting_sub_62533)){
            _32294 = NOVALUE;
            goto L3; // [55] 138
        }
        _32294 = NOVALUE;

        /** 			if fr[FR_PC] >= pc then*/
        _2 = (int)SEQ_PTR(_fr_64135);
        _32296 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(LESS, _32296, _pc_64130)){
            _32296 = NOVALUE;
            goto L4; // [67] 137
        }
        _32296 = NOVALUE;

        /** 				fr[FR_PC] += amount*/
        _2 = (int)SEQ_PTR(_fr_64135);
        _32298 = (int)*(((s1_ptr)_2)->base + 5);
        if (IS_ATOM_INT(_32298)) {
            _32299 = _32298 + _amount_64131;
            if ((long)((unsigned long)_32299 + (unsigned long)HIGH_BITS) >= 0) 
            _32299 = NewDouble((double)_32299);
        }
        else {
            _32299 = binary_op(PLUS, _32298, _amount_64131);
        }
        _32298 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64135);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64135 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _32299;
        if( _1 != _32299 ){
            DeRef(_1);
        }
        _32299 = NOVALUE;

        /** 				if fr[FR_TYPE] = CASE*/
        _2 = (int)SEQ_PTR(_fr_64135);
        _32300 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32300)) {
            _32301 = (_32300 == 186);
        }
        else {
            _32301 = binary_op(EQUALS, _32300, 186);
        }
        _32300 = NOVALUE;
        if (IS_ATOM_INT(_32301)) {
            if (_32301 == 0) {
                goto L5; // [101] 136
            }
        }
        else {
            if (DBL_PTR(_32301)->dbl == 0.0) {
                goto L5; // [101] 136
            }
        }
        _2 = (int)SEQ_PTR(_fr_64135);
        _32303 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32303)) {
            _32304 = (_32303 >= _pc_64130);
        }
        else {
            _32304 = binary_op(GREATEREQ, _32303, _pc_64130);
        }
        _32303 = NOVALUE;
        if (_32304 == 0) {
            DeRef(_32304);
            _32304 = NOVALUE;
            goto L5; // [116] 136
        }
        else {
            if (!IS_ATOM_INT(_32304) && DBL_PTR(_32304)->dbl == 0.0){
                DeRef(_32304);
                _32304 = NOVALUE;
                goto L5; // [116] 136
            }
            DeRef(_32304);
            _32304 = NOVALUE;
        }
        DeRef(_32304);
        _32304 = NOVALUE;

        /** 					fr[FR_DATA] += amount*/
        _2 = (int)SEQ_PTR(_fr_64135);
        _32305 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32305)) {
            _32306 = _32305 + _amount_64131;
            if ((long)((unsigned long)_32306 + (unsigned long)HIGH_BITS) >= 0) 
            _32306 = NewDouble((double)_32306);
        }
        else {
            _32306 = binary_op(PLUS, _32305, _amount_64131);
        }
        _32305 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64135);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64135 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 12);
        _1 = *(int *)_2;
        *(int *)_2 = _32306;
        if( _1 != _32306 ){
            DeRef(_1);
        }
        _32306 = NOVALUE;
L5: 
L4: 
L3: 

        /** 		forward_references[refs[i]] = fr*/
        _2 = (int)SEQ_PTR(_refs_64129);
        _32307 = (int)*(((s1_ptr)_2)->base + _i_64133);
        RefDS(_fr_64135);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _40forward_references_62503 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32307))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32307)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32307);
        _1 = *(int *)_2;
        *(int *)_2 = _fr_64135;
        DeRef(_1);
        DeRefDS(_fr_64135);
        _fr_64135 = NOVALUE;

        /** 	end for*/
        _i_64133 = _i_64133 + -1;
        goto L1; // [154] 19
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_refs_64129);
    _32291 = NOVALUE;
    _32293 = NOVALUE;
    _32307 = NOVALUE;
    DeRef(_32301);
    _32301 = NOVALUE;
    return;
    ;
}


void _40shift_top(int _refs_64159, int _pc_64160, int _amount_64161)
{
    int _fr_64165 = NOVALUE;
    int _32323 = NOVALUE;
    int _32322 = NOVALUE;
    int _32321 = NOVALUE;
    int _32320 = NOVALUE;
    int _32319 = NOVALUE;
    int _32318 = NOVALUE;
    int _32317 = NOVALUE;
    int _32316 = NOVALUE;
    int _32315 = NOVALUE;
    int _32314 = NOVALUE;
    int _32312 = NOVALUE;
    int _32311 = NOVALUE;
    int _32309 = NOVALUE;
    int _32308 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length( refs ) to 1 by -1 do*/
    if (IS_SEQUENCE(_refs_64159)){
            _32308 = SEQ_PTR(_refs_64159)->length;
    }
    else {
        _32308 = 1;
    }
    {
        int _i_64163;
        _i_64163 = _32308;
L1: 
        if (_i_64163 < 1){
            goto L2; // [12] 144
        }

        /** 		sequence fr = forward_references[refs[i]]*/
        _2 = (int)SEQ_PTR(_refs_64159);
        _32309 = (int)*(((s1_ptr)_2)->base + _i_64163);
        DeRef(_fr_64165);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!IS_ATOM_INT(_32309)){
            _fr_64165 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32309)->dbl));
        }
        else{
            _fr_64165 = (int)*(((s1_ptr)_2)->base + _32309);
        }
        Ref(_fr_64165);

        /** 		forward_references[refs[i]] = 0*/
        _2 = (int)SEQ_PTR(_refs_64159);
        _32311 = (int)*(((s1_ptr)_2)->base + _i_64163);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _40forward_references_62503 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32311))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32311)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32311);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);

        /** 		if fr[FR_PC] >= pc then*/
        _2 = (int)SEQ_PTR(_fr_64165);
        _32312 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(LESS, _32312, _pc_64160)){
            _32312 = NOVALUE;
            goto L3; // [53] 123
        }
        _32312 = NOVALUE;

        /** 			fr[FR_PC] += amount*/
        _2 = (int)SEQ_PTR(_fr_64165);
        _32314 = (int)*(((s1_ptr)_2)->base + 5);
        if (IS_ATOM_INT(_32314)) {
            _32315 = _32314 + _amount_64161;
            if ((long)((unsigned long)_32315 + (unsigned long)HIGH_BITS) >= 0) 
            _32315 = NewDouble((double)_32315);
        }
        else {
            _32315 = binary_op(PLUS, _32314, _amount_64161);
        }
        _32314 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64165);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64165 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _32315;
        if( _1 != _32315 ){
            DeRef(_1);
        }
        _32315 = NOVALUE;

        /** 			if fr[FR_TYPE] = CASE*/
        _2 = (int)SEQ_PTR(_fr_64165);
        _32316 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32316)) {
            _32317 = (_32316 == 186);
        }
        else {
            _32317 = binary_op(EQUALS, _32316, 186);
        }
        _32316 = NOVALUE;
        if (IS_ATOM_INT(_32317)) {
            if (_32317 == 0) {
                goto L4; // [87] 122
            }
        }
        else {
            if (DBL_PTR(_32317)->dbl == 0.0) {
                goto L4; // [87] 122
            }
        }
        _2 = (int)SEQ_PTR(_fr_64165);
        _32319 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32319)) {
            _32320 = (_32319 >= _pc_64160);
        }
        else {
            _32320 = binary_op(GREATEREQ, _32319, _pc_64160);
        }
        _32319 = NOVALUE;
        if (_32320 == 0) {
            DeRef(_32320);
            _32320 = NOVALUE;
            goto L4; // [102] 122
        }
        else {
            if (!IS_ATOM_INT(_32320) && DBL_PTR(_32320)->dbl == 0.0){
                DeRef(_32320);
                _32320 = NOVALUE;
                goto L4; // [102] 122
            }
            DeRef(_32320);
            _32320 = NOVALUE;
        }
        DeRef(_32320);
        _32320 = NOVALUE;

        /** 				fr[FR_DATA] += amount*/
        _2 = (int)SEQ_PTR(_fr_64165);
        _32321 = (int)*(((s1_ptr)_2)->base + 12);
        if (IS_ATOM_INT(_32321)) {
            _32322 = _32321 + _amount_64161;
            if ((long)((unsigned long)_32322 + (unsigned long)HIGH_BITS) >= 0) 
            _32322 = NewDouble((double)_32322);
        }
        else {
            _32322 = binary_op(PLUS, _32321, _amount_64161);
        }
        _32321 = NOVALUE;
        _2 = (int)SEQ_PTR(_fr_64165);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _fr_64165 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 12);
        _1 = *(int *)_2;
        *(int *)_2 = _32322;
        if( _1 != _32322 ){
            DeRef(_1);
        }
        _32322 = NOVALUE;
L4: 
L3: 

        /** 		forward_references[refs[i]] = fr*/
        _2 = (int)SEQ_PTR(_refs_64159);
        _32323 = (int)*(((s1_ptr)_2)->base + _i_64163);
        RefDS(_fr_64165);
        _2 = (int)SEQ_PTR(_40forward_references_62503);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _40forward_references_62503 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_32323))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_32323)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _32323);
        _1 = *(int *)_2;
        *(int *)_2 = _fr_64165;
        DeRef(_1);
        DeRefDS(_fr_64165);
        _fr_64165 = NOVALUE;

        /** 	end for*/
        _i_64163 = _i_64163 + -1;
        goto L1; // [139] 19
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_refs_64159);
    _32309 = NOVALUE;
    _32311 = NOVALUE;
    _32323 = NOVALUE;
    DeRef(_32317);
    _32317 = NOVALUE;
    return;
    ;
}


void _40shift_fwd_refs(int _pc_64186, int _amount_64187)
{
    int _file_64198 = NOVALUE;
    int _sp_64203 = NOVALUE;
    int _32333 = NOVALUE;
    int _32332 = NOVALUE;
    int _32330 = NOVALUE;
    int _32328 = NOVALUE;
    int _32327 = NOVALUE;
    int _32326 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_64186)) {
        _1 = (long)(DBL_PTR(_pc_64186)->dbl);
        if (UNIQUE(DBL_PTR(_pc_64186)) && (DBL_PTR(_pc_64186)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_64186);
        _pc_64186 = _1;
    }
    if (!IS_ATOM_INT(_amount_64187)) {
        _1 = (long)(DBL_PTR(_amount_64187)->dbl);
        if (UNIQUE(DBL_PTR(_amount_64187)) && (DBL_PTR(_amount_64187)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_amount_64187);
        _amount_64187 = _1;
    }

    /** 	if not shifting_sub then*/
    if (_40shifting_sub_62533 != 0)
    goto L1; // [9] 18

    /** 		return*/
    return;
L1: 

    /** 	if shifting_sub = TopLevelSub then*/
    if (_40shifting_sub_62533 != _38TopLevelSub_16953)
    goto L2; // [24] 65

    /** 		for file = 1 to length( toplevel_references ) do*/
    if (IS_SEQUENCE(_40toplevel_references_62506)){
            _32326 = SEQ_PTR(_40toplevel_references_62506)->length;
    }
    else {
        _32326 = 1;
    }
    {
        int _file_64194;
        _file_64194 = 1;
L3: 
        if (_file_64194 > _32326){
            goto L4; // [35] 62
        }

        /** 			shift_top( toplevel_references[file], pc, amount )*/
        _2 = (int)SEQ_PTR(_40toplevel_references_62506);
        _32327 = (int)*(((s1_ptr)_2)->base + _file_64194);
        Ref(_32327);
        _40shift_top(_32327, _pc_64186, _amount_64187);
        _32327 = NOVALUE;

        /** 		end for*/
        _file_64194 = _file_64194 + 1;
        goto L3; // [57] 42
L4: 
        ;
    }
    goto L5; // [62] 118
L2: 

    /** 		integer file = SymTab[shifting_sub][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32328 = (int)*(((s1_ptr)_2)->base + _40shifting_sub_62533);
    _2 = (int)SEQ_PTR(_32328);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _file_64198 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _file_64198 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_file_64198)){
        _file_64198 = (long)DBL_PTR(_file_64198)->dbl;
    }
    _32328 = NOVALUE;

    /** 		integer sp   = find( shifting_sub, active_subprogs[file] )*/
    _2 = (int)SEQ_PTR(_40active_subprogs_62504);
    _32330 = (int)*(((s1_ptr)_2)->base + _file_64198);
    _sp_64203 = find_from(_40shifting_sub_62533, _32330, 1);
    _32330 = NOVALUE;

    /** 		shift_these( active_references[file][sp], pc, amount )*/
    _2 = (int)SEQ_PTR(_40active_references_62505);
    _32332 = (int)*(((s1_ptr)_2)->base + _file_64198);
    _2 = (int)SEQ_PTR(_32332);
    _32333 = (int)*(((s1_ptr)_2)->base + _sp_64203);
    _32332 = NOVALUE;
    Ref(_32333);
    _40shift_these(_32333, _pc_64186, _amount_64187);
    _32333 = NOVALUE;
L5: 

    /** end procedure*/
    return;
    ;
}



// 0xB11EF5CB
