// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _55hashfn(int _name_47070)
{
    int _len_47071 = NOVALUE;
    int _val_47072 = NOVALUE;
    int _int_47073 = NOVALUE;
    int _25061 = NOVALUE;
    int _25060 = NOVALUE;
    int _25057 = NOVALUE;
    int _25056 = NOVALUE;
    int _25045 = NOVALUE;
    int _25041 = NOVALUE;
    int _0, _1, _2;
    

    /** 	len = length(name)*/
    if (IS_SEQUENCE(_name_47070)){
            _len_47071 = SEQ_PTR(_name_47070)->length;
    }
    else {
        _len_47071 = 1;
    }

    /** 	val = name[1]*/
    _2 = (int)SEQ_PTR(_name_47070);
    _val_47072 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_val_47072))
    _val_47072 = (long)DBL_PTR(_val_47072)->dbl;

    /** 	int = name[$]*/
    if (IS_SEQUENCE(_name_47070)){
            _25041 = SEQ_PTR(_name_47070)->length;
    }
    else {
        _25041 = 1;
    }
    _2 = (int)SEQ_PTR(_name_47070);
    _int_47073 = (int)*(((s1_ptr)_2)->base + _25041);
    if (!IS_ATOM_INT(_int_47073))
    _int_47073 = (long)DBL_PTR(_int_47073)->dbl;

    /** 	int *= 256*/
    _int_47073 = _int_47073 * 256;

    /** 	val *= 2*/
    _val_47072 = _val_47072 + _val_47072;

    /** 	val += int + len*/
    _25045 = _int_47073 + _len_47071;
    if ((long)((unsigned long)_25045 + (unsigned long)HIGH_BITS) >= 0) 
    _25045 = NewDouble((double)_25045);
    if (IS_ATOM_INT(_25045)) {
        _val_47072 = _val_47072 + _25045;
    }
    else {
        _val_47072 = NewDouble((double)_val_47072 + DBL_PTR(_25045)->dbl);
    }
    DeRef(_25045);
    _25045 = NOVALUE;
    if (!IS_ATOM_INT(_val_47072)) {
        _1 = (long)(DBL_PTR(_val_47072)->dbl);
        if (UNIQUE(DBL_PTR(_val_47072)) && (DBL_PTR(_val_47072)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_val_47072);
        _val_47072 = _1;
    }

    /** 	if len = 3 then*/
    if (_len_47071 != 3)
    goto L1; // [51] 78

    /** 		val *= 32*/
    _val_47072 = _val_47072 * 32;

    /** 		int = name[2]*/
    _2 = (int)SEQ_PTR(_name_47070);
    _int_47073 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_int_47073))
    _int_47073 = (long)DBL_PTR(_int_47073)->dbl;

    /** 		val += int*/
    _val_47072 = _val_47072 + _int_47073;
    goto L2; // [75] 133
L1: 

    /** 	elsif len > 3 then*/
    if (_len_47071 <= 3)
    goto L3; // [80] 132

    /** 		val *= 32*/
    _val_47072 = _val_47072 * 32;

    /** 		int = name[2]*/
    _2 = (int)SEQ_PTR(_name_47070);
    _int_47073 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_int_47073))
    _int_47073 = (long)DBL_PTR(_int_47073)->dbl;

    /** 		val += int*/
    _val_47072 = _val_47072 + _int_47073;

    /** 		val *= 32*/
    _val_47072 = _val_47072 * 32;

    /** 		int = name[$-1]*/
    if (IS_SEQUENCE(_name_47070)){
            _25056 = SEQ_PTR(_name_47070)->length;
    }
    else {
        _25056 = 1;
    }
    _25057 = _25056 - 1;
    _25056 = NOVALUE;
    _2 = (int)SEQ_PTR(_name_47070);
    _int_47073 = (int)*(((s1_ptr)_2)->base + _25057);
    if (!IS_ATOM_INT(_int_47073))
    _int_47073 = (long)DBL_PTR(_int_47073)->dbl;

    /** 		val += int*/
    _val_47072 = _val_47072 + _int_47073;
L3: 
L2: 

    /** 	return remainder(val, NBUCKETS) + 1*/
    _25060 = (_val_47072 % 2003);
    _25061 = _25060 + 1;
    _25060 = NOVALUE;
    DeRefDS(_name_47070);
    DeRef(_25057);
    _25057 = NOVALUE;
    return _25061;
    ;
}


void _55remove_symbol(int _sym_47102)
{
    int _hash_47103 = NOVALUE;
    int _st_ptr_47104 = NOVALUE;
    int _25076 = NOVALUE;
    int _25075 = NOVALUE;
    int _25073 = NOVALUE;
    int _25072 = NOVALUE;
    int _25071 = NOVALUE;
    int _25069 = NOVALUE;
    int _25067 = NOVALUE;
    int _25066 = NOVALUE;
    int _25065 = NOVALUE;
    int _25062 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_47102)) {
        _1 = (long)(DBL_PTR(_sym_47102)->dbl);
        if (UNIQUE(DBL_PTR(_sym_47102)) && (DBL_PTR(_sym_47102)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_47102);
        _sym_47102 = _1;
    }

    /** 	hash = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25062 = (int)*(((s1_ptr)_2)->base + _sym_47102);
    _2 = (int)SEQ_PTR(_25062);
    _hash_47103 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_hash_47103)){
        _hash_47103 = (long)DBL_PTR(_hash_47103)->dbl;
    }
    _25062 = NOVALUE;

    /** 	st_ptr = buckets[hash]*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _st_ptr_47104 = (int)*(((s1_ptr)_2)->base + _hash_47103);
    if (!IS_ATOM_INT(_st_ptr_47104))
    _st_ptr_47104 = (long)DBL_PTR(_st_ptr_47104)->dbl;

    /** 	while st_ptr and st_ptr != sym do*/
L1: 
    if (_st_ptr_47104 == 0) {
        goto L2; // [32] 65
    }
    _25066 = (_st_ptr_47104 != _sym_47102);
    if (_25066 == 0)
    {
        DeRef(_25066);
        _25066 = NOVALUE;
        goto L2; // [41] 65
    }
    else{
        DeRef(_25066);
        _25066 = NOVALUE;
    }

    /** 		st_ptr = SymTab[st_ptr][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25067 = (int)*(((s1_ptr)_2)->base + _st_ptr_47104);
    _2 = (int)SEQ_PTR(_25067);
    _st_ptr_47104 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_st_ptr_47104)){
        _st_ptr_47104 = (long)DBL_PTR(_st_ptr_47104)->dbl;
    }
    _25067 = NOVALUE;

    /** 	end while*/
    goto L1; // [62] 32
L2: 

    /** 	if st_ptr then*/
    if (_st_ptr_47104 == 0)
    {
        goto L3; // [67] 134
    }
    else{
    }

    /** 		if st_ptr = buckets[hash] then*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _25069 = (int)*(((s1_ptr)_2)->base + _hash_47103);
    if (binary_op_a(NOTEQ, _st_ptr_47104, _25069)){
        _25069 = NOVALUE;
        goto L4; // [78] 105
    }
    _25069 = NOVALUE;

    /** 			buckets[hash] = SymTab[st_ptr][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25071 = (int)*(((s1_ptr)_2)->base + _st_ptr_47104);
    _2 = (int)SEQ_PTR(_25071);
    _25072 = (int)*(((s1_ptr)_2)->base + 9);
    _25071 = NOVALUE;
    Ref(_25072);
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _2 = (int)(((s1_ptr)_2)->base + _hash_47103);
    _1 = *(int *)_2;
    *(int *)_2 = _25072;
    if( _1 != _25072 ){
        DeRef(_1);
    }
    _25072 = NOVALUE;
    goto L5; // [102] 133
L4: 

    /** 			SymTab[st_ptr][S_SAMEHASH] = SymTab[sym][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_st_ptr_47104 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25075 = (int)*(((s1_ptr)_2)->base + _sym_47102);
    _2 = (int)SEQ_PTR(_25075);
    _25076 = (int)*(((s1_ptr)_2)->base + 9);
    _25075 = NOVALUE;
    Ref(_25076);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _25076;
    if( _1 != _25076 ){
        DeRef(_1);
    }
    _25076 = NOVALUE;
    _25073 = NOVALUE;
L5: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _55NewBasicEntry(int _name_47136, int _varnum_47137, int _scope_47138, int _token_47139, int _hashval_47140, int _samehash_47142, int _type_sym_47144)
{
    int _new_47145 = NOVALUE;
    int _25085 = NOVALUE;
    int _25083 = NOVALUE;
    int _25082 = NOVALUE;
    int _25081 = NOVALUE;
    int _25080 = NOVALUE;
    int _25079 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_varnum_47137)) {
        _1 = (long)(DBL_PTR(_varnum_47137)->dbl);
        if (UNIQUE(DBL_PTR(_varnum_47137)) && (DBL_PTR(_varnum_47137)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_varnum_47137);
        _varnum_47137 = _1;
    }
    if (!IS_ATOM_INT(_scope_47138)) {
        _1 = (long)(DBL_PTR(_scope_47138)->dbl);
        if (UNIQUE(DBL_PTR(_scope_47138)) && (DBL_PTR(_scope_47138)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_47138);
        _scope_47138 = _1;
    }
    if (!IS_ATOM_INT(_token_47139)) {
        _1 = (long)(DBL_PTR(_token_47139)->dbl);
        if (UNIQUE(DBL_PTR(_token_47139)) && (DBL_PTR(_token_47139)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_token_47139);
        _token_47139 = _1;
    }
    if (!IS_ATOM_INT(_hashval_47140)) {
        _1 = (long)(DBL_PTR(_hashval_47140)->dbl);
        if (UNIQUE(DBL_PTR(_hashval_47140)) && (DBL_PTR(_hashval_47140)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hashval_47140);
        _hashval_47140 = _1;
    }
    if (!IS_ATOM_INT(_samehash_47142)) {
        _1 = (long)(DBL_PTR(_samehash_47142)->dbl);
        if (UNIQUE(DBL_PTR(_samehash_47142)) && (DBL_PTR(_samehash_47142)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_samehash_47142);
        _samehash_47142 = _1;
    }
    if (!IS_ATOM_INT(_type_sym_47144)) {
        _1 = (long)(DBL_PTR(_type_sym_47144)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_47144)) && (DBL_PTR(_type_sym_47144)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_47144);
        _type_sym_47144 = _1;
    }

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [19] 33
    }
    else{
    }

    /** 		new = repeat(0, SIZEOF_ROUTINE_ENTRY)*/
    DeRef(_new_47145);
    _new_47145 = Repeat(0, _38SIZEOF_ROUTINE_ENTRY_16724);
    goto L2; // [30] 42
L1: 

    /** 		new = repeat(0, SIZEOF_VAR_ENTRY)*/
    DeRef(_new_47145);
    _new_47145 = Repeat(0, _38SIZEOF_VAR_ENTRY_16727);
L2: 

    /** 	new[S_NEXT] = 0*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	new[S_NAME] = name*/
    RefDS(_name_47136);
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_NAME_16598))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_NAME_16598);
    _1 = *(int *)_2;
    *(int *)_2 = _name_47136;
    DeRef(_1);

    /** 	new[S_SCOPE] = scope*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _scope_47138;
    DeRef(_1);

    /** 	new[S_MODE] = M_NORMAL*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);

    /** 	new[S_USAGE] = U_UNUSED*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	new[S_FILE_NO] = current_file_no*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FILE_NO_16594))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    _1 = *(int *)_2;
    *(int *)_2 = _38current_file_no_16946;
    DeRef(_1);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L3; // [102] 327
    }
    else{
    }

    /** 		new[S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_GTYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 38);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_SEQ_ELEM_NEW] = TYPE_NULL -- starting point for ORing*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ARG_TYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 43);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_ARG_TYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 44);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ARG_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 45);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		new[S_ARG_SEQ_ELEM_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 46);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ARG_MIN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 47);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);

    /** 		new[S_ARG_MIN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _25079 = (int)NewDouble((double)-0xC0000000);
        else
        _25079 = - _38NOVALUE_16800;
    }
    else {
        _25079 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 49);
    _1 = *(int *)_2;
    *(int *)_2 = _25079;
    if( _1 != _25079 ){
        DeRef(_1);
    }
    _25079 = NOVALUE;

    /** 		new[S_ARG_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 51);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);

    /** 		new[S_ARG_SEQ_LEN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _25080 = (int)NewDouble((double)-0xC0000000);
        else
        _25080 = - _38NOVALUE_16800;
    }
    else {
        _25080 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 52);
    _1 = *(int *)_2;
    *(int *)_2 = _25080;
    if( _1 != _25080 ){
        DeRef(_1);
    }
    _25080 = NOVALUE;

    /** 		new[S_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);

    /** 		new[S_SEQ_LEN_NEW] = -NOVALUE -- no idea yet*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _25081 = (int)NewDouble((double)-0xC0000000);
        else
        _25081 = - _38NOVALUE_16800;
    }
    else {
        _25081 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _25081;
    if( _1 != _25081 ){
        DeRef(_1);
    }
    _25081 = NOVALUE;

    /** 		new[S_NREFS] = 0*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_ONE_REF] = TRUE          -- assume TRUE until we find otherwise*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 35);
    _1 = *(int *)_2;
    *(int *)_2 = _9TRUE_428;
    DeRef(_1);

    /** 		new[S_RI_TARGET] = 0*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 53);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		new[S_OBJ_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = -1073741824;
    DeRef(_1);

    /** 		new[S_OBJ_MIN_NEW] = -NOVALUE -- no idea yet*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _25082 = (int)NewDouble((double)-0xC0000000);
        else
        _25082 = - _38NOVALUE_16800;
    }
    else {
        _25082 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _25082;
    if( _1 != _25082 ){
        DeRef(_1);
    }
    _25082 = NOVALUE;

    /** 		new[S_OBJ_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = 1073741823;
    DeRef(_1);

    /** 		new[S_OBJ_MAX_NEW] = -NOVALUE -- missing from C code? (not needed)*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _25083 = (int)NewDouble((double)-0xC0000000);
        else
        _25083 = - _38NOVALUE_16800;
    }
    else {
        _25083 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 42);
    _1 = *(int *)_2;
    *(int *)_2 = _25083;
    if( _1 != _25083 ){
        DeRef(_1);
    }
    _25083 = NOVALUE;
L3: 

    /** 	new[S_TOKEN] = token*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_TOKEN_16603))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    _1 = *(int *)_2;
    *(int *)_2 = _token_47139;
    DeRef(_1);

    /** 	new[S_VARNUM] = varnum*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 16);
    _1 = *(int *)_2;
    *(int *)_2 = _varnum_47137;
    DeRef(_1);

    /** 	new[S_INITLEVEL] = -1*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = -1;
    DeRef(_1);

    /** 	new[S_VTYPE] = type_sym*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _type_sym_47144;
    DeRef(_1);

    /** 	new[S_HASHVAL] = hashval*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _hashval_47140;
    DeRef(_1);

    /** 	new[S_SAMEHASH] = samehash*/
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _samehash_47142;
    DeRef(_1);

    /** 	new[S_OBJ] = NOVALUE -- important*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_new_47145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_47145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);

    /** 	SymTab = append(SymTab, new)*/
    RefDS(_new_47145);
    Append(&_35SymTab_15595, _35SymTab_15595, _new_47145);

    /** 	return length(SymTab)*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _25085 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _25085 = 1;
    }
    DeRefDS(_name_47136);
    DeRefDS(_new_47145);
    return _25085;
    ;
}


int _55NewEntry(int _name_47224, int _varnum_47225, int _scope_47226, int _token_47227, int _hashval_47228, int _samehash_47230, int _type_sym_47232)
{
    int _new_47234 = NOVALUE;
    int _25087 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_varnum_47225)) {
        _1 = (long)(DBL_PTR(_varnum_47225)->dbl);
        if (UNIQUE(DBL_PTR(_varnum_47225)) && (DBL_PTR(_varnum_47225)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_varnum_47225);
        _varnum_47225 = _1;
    }
    if (!IS_ATOM_INT(_scope_47226)) {
        _1 = (long)(DBL_PTR(_scope_47226)->dbl);
        if (UNIQUE(DBL_PTR(_scope_47226)) && (DBL_PTR(_scope_47226)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_47226);
        _scope_47226 = _1;
    }
    if (!IS_ATOM_INT(_token_47227)) {
        _1 = (long)(DBL_PTR(_token_47227)->dbl);
        if (UNIQUE(DBL_PTR(_token_47227)) && (DBL_PTR(_token_47227)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_token_47227);
        _token_47227 = _1;
    }
    if (!IS_ATOM_INT(_hashval_47228)) {
        _1 = (long)(DBL_PTR(_hashval_47228)->dbl);
        if (UNIQUE(DBL_PTR(_hashval_47228)) && (DBL_PTR(_hashval_47228)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hashval_47228);
        _hashval_47228 = _1;
    }
    if (!IS_ATOM_INT(_samehash_47230)) {
        _1 = (long)(DBL_PTR(_samehash_47230)->dbl);
        if (UNIQUE(DBL_PTR(_samehash_47230)) && (DBL_PTR(_samehash_47230)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_samehash_47230);
        _samehash_47230 = _1;
    }
    if (!IS_ATOM_INT(_type_sym_47232)) {
        _1 = (long)(DBL_PTR(_type_sym_47232)->dbl);
        if (UNIQUE(DBL_PTR(_type_sym_47232)) && (DBL_PTR(_type_sym_47232)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type_sym_47232);
        _type_sym_47232 = _1;
    }

    /** 	symtab_index new = NewBasicEntry( name, varnum, scope, token, hashval, samehash, type_sym )*/
    RefDS(_name_47224);
    _new_47234 = _55NewBasicEntry(_name_47224, _varnum_47225, _scope_47226, _token_47227, _hashval_47228, _samehash_47230, _type_sym_47232);
    if (!IS_ATOM_INT(_new_47234)) {
        _1 = (long)(DBL_PTR(_new_47234)->dbl);
        if (UNIQUE(DBL_PTR(_new_47234)) && (DBL_PTR(_new_47234)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_47234);
        _new_47234 = _1;
    }

    /** 	if last_sym then*/
    if (_55last_sym_47064 == 0)
    {
        goto L1; // [33] 54
    }
    else{
    }

    /** 		SymTab[last_sym][S_NEXT] = new*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_55last_sym_47064 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _new_47234;
    DeRef(_1);
    _25087 = NOVALUE;
L1: 

    /** 	last_sym = new*/
    _55last_sym_47064 = _new_47234;

    /** 	if type_sym < 0 then*/
    if (_type_sym_47232 >= 0)
    goto L2; // [63] 76

    /** 		register_forward_type( last_sym, type_sym )*/
    _40register_forward_type(_55last_sym_47064, _type_sym_47232);
L2: 

    /** 	return last_sym*/
    DeRefDS(_name_47224);
    return _55last_sym_47064;
    ;
}


int _55tmp_alloc()
{
    int _new_entry_47249 = NOVALUE;
    int _25101 = NOVALUE;
    int _25099 = NOVALUE;
    int _25096 = NOVALUE;
    int _25093 = NOVALUE;
    int _25092 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence new_entry = repeat( 0, SIZEOF_TEMP_ENTRY )*/
    DeRef(_new_entry_47249);
    _new_entry_47249 = Repeat(0, _38SIZEOF_TEMP_ENTRY_16733);

    /** 	new_entry[S_USAGE] = T_UNKNOWN*/
    _2 = (int)SEQ_PTR(_new_entry_47249);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47249 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    *(int *)_2 = 4;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L1; // [23] 132
    }
    else{
    }

    /** 		new_entry[S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_new_entry_47249);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47249 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    *(int *)_2 = 16;

    /** 		new_entry[S_OBJ_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_new_entry_47249);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47249 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    *(int *)_2 = -1073741824;

    /** 		new_entry[S_OBJ_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_new_entry_47249);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47249 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    *(int *)_2 = 1073741823;

    /** 		new_entry[S_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_new_entry_47249);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47249 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    *(int *)_2 = _38NOVALUE_16800;

    /** 		new_entry[S_SEQ_ELEM] = TYPE_OBJECT  -- other fields set later*/
    _2 = (int)SEQ_PTR(_new_entry_47249);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47249 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);

    /** 		if length(temp_name_type)+1 = 8087 then*/
    if (IS_SEQUENCE(_38temp_name_type_17031)){
            _25092 = SEQ_PTR(_38temp_name_type_17031)->length;
    }
    else {
        _25092 = 1;
    }
    _25093 = _25092 + 1;
    _25092 = NOVALUE;
    if (_25093 != 8087)
    goto L2; // [87] 106

    /** 			temp_name_type = append(temp_name_type, {0, 0})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _25096 = MAKE_SEQ(_1);
    RefDS(_25096);
    Append(&_38temp_name_type_17031, _38temp_name_type_17031, _25096);
    DeRefDS(_25096);
    _25096 = NOVALUE;
L2: 

    /** 		temp_name_type = append(temp_name_type, TYPES_OBNL)*/
    RefDS(_56TYPES_OBNL_46441);
    Append(&_38temp_name_type_17031, _38temp_name_type_17031, _56TYPES_OBNL_46441);

    /** 		new_entry[S_TEMP_NAME] = length(temp_name_type)*/
    if (IS_SEQUENCE(_38temp_name_type_17031)){
            _25099 = SEQ_PTR(_38temp_name_type_17031)->length;
    }
    else {
        _25099 = 1;
    }
    _2 = (int)SEQ_PTR(_new_entry_47249);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _new_entry_47249 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 34);
    _1 = *(int *)_2;
    *(int *)_2 = _25099;
    if( _1 != _25099 ){
        DeRef(_1);
    }
    _25099 = NOVALUE;
L1: 

    /** 	SymTab = append(SymTab, new_entry )*/
    RefDS(_new_entry_47249);
    Append(&_35SymTab_15595, _35SymTab_15595, _new_entry_47249);

    /** 	return length( SymTab )*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _25101 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _25101 = 1;
    }
    DeRefDS(_new_entry_47249);
    DeRef(_25093);
    _25093 = NOVALUE;
    return _25101;
    ;
}


void _55DefinedYet(int _sym_47318)
{
    int _25121 = NOVALUE;
    int _25120 = NOVALUE;
    int _25119 = NOVALUE;
    int _25117 = NOVALUE;
    int _25116 = NOVALUE;
    int _25114 = NOVALUE;
    int _25113 = NOVALUE;
    int _25112 = NOVALUE;
    int _25111 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_47318)) {
        _1 = (long)(DBL_PTR(_sym_47318)->dbl);
        if (UNIQUE(DBL_PTR(_sym_47318)) && (DBL_PTR(_sym_47318)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_47318);
        _sym_47318 = _1;
    }

    /** 	if not find(SymTab[sym][S_SCOPE],*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25111 = (int)*(((s1_ptr)_2)->base + _sym_47318);
    _2 = (int)SEQ_PTR(_25111);
    _25112 = (int)*(((s1_ptr)_2)->base + 4);
    _25111 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 9;
    *((int *)(_2+8)) = 10;
    *((int *)(_2+12)) = 7;
    _25113 = MAKE_SEQ(_1);
    _25114 = find_from(_25112, _25113, 1);
    _25112 = NOVALUE;
    DeRefDS(_25113);
    _25113 = NOVALUE;
    if (_25114 != 0)
    goto L1; // [34] 82
    _25114 = NOVALUE;

    /** 		if SymTab[sym][S_FILE_NO] = current_file_no then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25116 = (int)*(((s1_ptr)_2)->base + _sym_47318);
    _2 = (int)SEQ_PTR(_25116);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25117 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25117 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25116 = NOVALUE;
    if (binary_op_a(NOTEQ, _25117, _38current_file_no_16946)){
        _25117 = NOVALUE;
        goto L2; // [53] 81
    }
    _25117 = NOVALUE;

    /** 			CompileErr(31, {SymTab[sym][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25119 = (int)*(((s1_ptr)_2)->base + _sym_47318);
    _2 = (int)SEQ_PTR(_25119);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25120 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25120 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25119 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_25120);
    *((int *)(_2+4)) = _25120;
    _25121 = MAKE_SEQ(_1);
    _25120 = NOVALUE;
    _46CompileErr(31, _25121, 0);
    _25121 = NOVALUE;
L2: 
L1: 

    /** end procedure*/
    return;
    ;
}


int _55name_ext(int _s_47345)
{
    int _25128 = NOVALUE;
    int _25127 = NOVALUE;
    int _25126 = NOVALUE;
    int _25125 = NOVALUE;
    int _25123 = NOVALUE;
    int _25122 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length(s) to 1 by -1 do*/
    if (IS_SEQUENCE(_s_47345)){
            _25122 = SEQ_PTR(_s_47345)->length;
    }
    else {
        _25122 = 1;
    }
    {
        int _i_47347;
        _i_47347 = _25122;
L1: 
        if (_i_47347 < 1){
            goto L2; // [8] 55
        }

        /** 		if find(s[i], "/\\:") then*/
        _2 = (int)SEQ_PTR(_s_47345);
        _25123 = (int)*(((s1_ptr)_2)->base + _i_47347);
        _25125 = find_from(_25123, _25124, 1);
        _25123 = NOVALUE;
        if (_25125 == 0)
        {
            _25125 = NOVALUE;
            goto L3; // [26] 48
        }
        else{
            _25125 = NOVALUE;
        }

        /** 			return s[i+1 .. $]*/
        _25126 = _i_47347 + 1;
        if (IS_SEQUENCE(_s_47345)){
                _25127 = SEQ_PTR(_s_47345)->length;
        }
        else {
            _25127 = 1;
        }
        rhs_slice_target = (object_ptr)&_25128;
        RHS_Slice(_s_47345, _25126, _25127);
        DeRefDS(_s_47345);
        _25126 = NOVALUE;
        return _25128;
L3: 

        /** 	end for*/
        _i_47347 = _i_47347 + -1;
        goto L1; // [50] 15
L2: 
        ;
    }

    /** 	return s*/
    DeRef(_25126);
    _25126 = NOVALUE;
    DeRef(_25128);
    _25128 = NOVALUE;
    return _s_47345;
    ;
}


int _55NewStringSym(int _s_47364)
{
    int _p_47366 = NOVALUE;
    int _tp_47367 = NOVALUE;
    int _prev_47368 = NOVALUE;
    int _search_count_47369 = NOVALUE;
    int _25172 = NOVALUE;
    int _25170 = NOVALUE;
    int _25169 = NOVALUE;
    int _25168 = NOVALUE;
    int _25166 = NOVALUE;
    int _25165 = NOVALUE;
    int _25162 = NOVALUE;
    int _25160 = NOVALUE;
    int _25158 = NOVALUE;
    int _25157 = NOVALUE;
    int _25156 = NOVALUE;
    int _25154 = NOVALUE;
    int _25152 = NOVALUE;
    int _25150 = NOVALUE;
    int _25148 = NOVALUE;
    int _25145 = NOVALUE;
    int _25143 = NOVALUE;
    int _25142 = NOVALUE;
    int _25141 = NOVALUE;
    int _25139 = NOVALUE;
    int _25137 = NOVALUE;
    int _25136 = NOVALUE;
    int _25135 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer search_count*/

    /** 	tp = literal_init*/
    _tp_47367 = _55literal_init_47063;

    /** 	prev = 0*/
    _prev_47368 = 0;

    /** 	search_count = 0*/
    _search_count_47369 = 0;

    /** 	while tp != 0 do*/
L1: 
    if (_tp_47367 == 0)
    goto L2; // [31] 170

    /** 		search_count += 1*/
    _search_count_47369 = _search_count_47369 + 1;

    /** 		if search_count > SEARCH_LIMIT then  -- avoid n-squared algorithm*/
    if (binary_op_a(LESSEQ, _search_count_47369, _55SEARCH_LIMIT_47356)){
        goto L3; // [45] 54
    }

    /** 			exit*/
    goto L2; // [51] 170
L3: 

    /** 		if equal(s, SymTab[tp][S_OBJ]) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25135 = (int)*(((s1_ptr)_2)->base + _tp_47367);
    _2 = (int)SEQ_PTR(_25135);
    _25136 = (int)*(((s1_ptr)_2)->base + 1);
    _25135 = NOVALUE;
    if (_s_47364 == _25136)
    _25137 = 1;
    else if (IS_ATOM_INT(_s_47364) && IS_ATOM_INT(_25136))
    _25137 = 0;
    else
    _25137 = (compare(_s_47364, _25136) == 0);
    _25136 = NOVALUE;
    if (_25137 == 0)
    {
        _25137 = NOVALUE;
        goto L4; // [72] 142
    }
    else{
        _25137 = NOVALUE;
    }

    /** 			if tp != literal_init then*/
    if (_tp_47367 == _55literal_init_47063)
    goto L5; // [79] 135

    /** 				SymTab[prev][S_NEXT] = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_prev_47368 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25141 = (int)*(((s1_ptr)_2)->base + _tp_47367);
    _2 = (int)SEQ_PTR(_25141);
    _25142 = (int)*(((s1_ptr)_2)->base + 2);
    _25141 = NOVALUE;
    Ref(_25142);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25142;
    if( _1 != _25142 ){
        DeRef(_1);
    }
    _25142 = NOVALUE;
    _25139 = NOVALUE;

    /** 				SymTab[tp][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_tp_47367 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _55literal_init_47063;
    DeRef(_1);
    _25143 = NOVALUE;

    /** 				literal_init = tp*/
    _55literal_init_47063 = _tp_47367;
L5: 

    /** 			return tp*/
    DeRefDS(_s_47364);
    return _tp_47367;
L4: 

    /** 		prev = tp*/
    _prev_47368 = _tp_47367;

    /** 		tp = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25145 = (int)*(((s1_ptr)_2)->base + _tp_47367);
    _2 = (int)SEQ_PTR(_25145);
    _tp_47367 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_tp_47367)){
        _tp_47367 = (long)DBL_PTR(_tp_47367)->dbl;
    }
    _25145 = NOVALUE;

    /** 	end while*/
    goto L1; // [167] 31
L2: 

    /** 	p = tmp_alloc()*/
    _p_47366 = _55tmp_alloc();
    if (!IS_ATOM_INT(_p_47366)) {
        _1 = (long)(DBL_PTR(_p_47366)->dbl);
        if (UNIQUE(DBL_PTR(_p_47366)) && (DBL_PTR(_p_47366)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47366);
        _p_47366 = _1;
    }

    /** 	SymTab[p][S_OBJ] = s*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    RefDS(_s_47364);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _s_47364;
    DeRef(_1);
    _25148 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L6; // [196] 346
    }
    else{
    }

    /** 		SymTab[p][S_MODE] = M_TEMP    -- override CONSTANT for compile*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25150 = NOVALUE;

    /** 		SymTab[p][S_GTYPE] = TYPE_SEQUENCE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 8;
    DeRef(_1);
    _25152 = NOVALUE;

    /** 		SymTab[p][S_SEQ_LEN] = length(s)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_s_47364)){
            _25156 = SEQ_PTR(_s_47364)->length;
    }
    else {
        _25156 = 1;
    }
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _25156;
    if( _1 != _25156 ){
        DeRef(_1);
    }
    _25156 = NOVALUE;
    _25154 = NOVALUE;

    /** 		if SymTab[p][S_SEQ_LEN] > 0 then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25157 = (int)*(((s1_ptr)_2)->base + _p_47366);
    _2 = (int)SEQ_PTR(_25157);
    _25158 = (int)*(((s1_ptr)_2)->base + 32);
    _25157 = NOVALUE;
    if (binary_op_a(LESSEQ, _25158, 0)){
        _25158 = NOVALUE;
        goto L7; // [265] 289
    }
    _25158 = NOVALUE;

    /** 			SymTab[p][S_SEQ_ELEM] = TYPE_INTEGER*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _25160 = NOVALUE;
    goto L8; // [286] 307
L7: 

    /** 			SymTab[p][S_SEQ_ELEM] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _25162 = NOVALUE;
L8: 

    /** 		c_printf("int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25165 = (int)*(((s1_ptr)_2)->base + _p_47366);
    _2 = (int)SEQ_PTR(_25165);
    _25166 = (int)*(((s1_ptr)_2)->base + 34);
    _25165 = NOVALUE;
    RefDS(_25164);
    Ref(_25166);
    _56c_printf(_25164, _25166);
    _25166 = NOVALUE;

    /** 		c_hprintf("extern int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25168 = (int)*(((s1_ptr)_2)->base + _p_47366);
    _2 = (int)SEQ_PTR(_25168);
    _25169 = (int)*(((s1_ptr)_2)->base + 34);
    _25168 = NOVALUE;
    RefDS(_25167);
    Ref(_25169);
    _56c_hprintf(_25167, _25169);
    _25169 = NOVALUE;
    goto L9; // [343] 364
L6: 

    /** 		SymTab[p][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25170 = NOVALUE;
L9: 

    /** 	SymTab[p][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47366 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _55literal_init_47063;
    DeRef(_1);
    _25172 = NOVALUE;

    /** 	literal_init = p*/
    _55literal_init_47063 = _p_47366;

    /** 	return p*/
    DeRefDS(_s_47364);
    return _p_47366;
    ;
}


int _55NewIntSym(int _int_val_47462)
{
    int _p_47464 = NOVALUE;
    int _x_47465 = NOVALUE;
    int _25191 = NOVALUE;
    int _25189 = NOVALUE;
    int _25185 = NOVALUE;
    int _25183 = NOVALUE;
    int _25181 = NOVALUE;
    int _25179 = NOVALUE;
    int _25177 = NOVALUE;
    int _25175 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_int_val_47462)) {
        _1 = (long)(DBL_PTR(_int_val_47462)->dbl);
        if (UNIQUE(DBL_PTR(_int_val_47462)) && (DBL_PTR(_int_val_47462)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_int_val_47462);
        _int_val_47462 = _1;
    }

    /** 	integer x*/

    /** 	x = find(int_val, lastintval)*/
    _x_47465 = find_from(_int_val_47462, _55lastintval_47065, 1);

    /** 	if x then*/
    if (_x_47465 == 0)
    {
        goto L1; // [16] 34
    }
    else{
    }

    /** 		return lastintsym[x]  -- saves space, helps Translator reduce code size*/
    _2 = (int)SEQ_PTR(_55lastintsym_47066);
    _25175 = (int)*(((s1_ptr)_2)->base + _x_47465);
    return _25175;
    goto L2; // [31] 180
L1: 

    /** 		p = tmp_alloc()*/
    _p_47464 = _55tmp_alloc();
    if (!IS_ATOM_INT(_p_47464)) {
        _1 = (long)(DBL_PTR(_p_47464)->dbl);
        if (UNIQUE(DBL_PTR(_p_47464)) && (DBL_PTR(_p_47464)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47464);
        _p_47464 = _1;
    }

    /** 		SymTab[p][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47464 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25177 = NOVALUE;

    /** 		SymTab[p][S_OBJ] = int_val*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47464 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _int_val_47462;
    DeRef(_1);
    _25179 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L3; // [77] 128
    }
    else{
    }

    /** 			SymTab[p][S_OBJ_MIN] = int_val*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47464 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _int_val_47462;
    DeRef(_1);
    _25181 = NOVALUE;

    /** 			SymTab[p][S_OBJ_MAX] = int_val*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47464 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _int_val_47462;
    DeRef(_1);
    _25183 = NOVALUE;

    /** 			SymTab[p][S_GTYPE] = TYPE_INTEGER*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47464 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _25185 = NOVALUE;
L3: 

    /** 		lastintval = prepend(lastintval, int_val)*/
    Prepend(&_55lastintval_47065, _55lastintval_47065, _int_val_47462);

    /** 		lastintsym = prepend(lastintsym, p)*/
    Prepend(&_55lastintsym_47066, _55lastintsym_47066, _p_47464);

    /** 		if length(lastintval) > SEARCH_LIMIT then*/
    if (IS_SEQUENCE(_55lastintval_47065)){
            _25189 = SEQ_PTR(_55lastintval_47065)->length;
    }
    else {
        _25189 = 1;
    }
    if (binary_op_a(LESSEQ, _25189, _55SEARCH_LIMIT_47356)){
        _25189 = NOVALUE;
        goto L4; // [153] 173
    }
    _25189 = NOVALUE;

    /** 			lastintval = lastintval[1..floor(SEARCH_LIMIT/2)]*/
    if (IS_ATOM_INT(_55SEARCH_LIMIT_47356)) {
        _25191 = _55SEARCH_LIMIT_47356 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _55SEARCH_LIMIT_47356, 2);
        _25191 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    rhs_slice_target = (object_ptr)&_55lastintval_47065;
    RHS_Slice(_55lastintval_47065, 1, _25191);
L4: 

    /** 		return p*/
    _25175 = NOVALUE;
    DeRef(_25191);
    _25191 = NOVALUE;
    return _p_47464;
L2: 
    ;
}


int _55NewDoubleSym(int _d_47504)
{
    int _p_47506 = NOVALUE;
    int _tp_47507 = NOVALUE;
    int _prev_47508 = NOVALUE;
    int _search_count_47509 = NOVALUE;
    int _25221 = NOVALUE;
    int _25220 = NOVALUE;
    int _25219 = NOVALUE;
    int _25218 = NOVALUE;
    int _25217 = NOVALUE;
    int _25215 = NOVALUE;
    int _25213 = NOVALUE;
    int _25211 = NOVALUE;
    int _25209 = NOVALUE;
    int _25206 = NOVALUE;
    int _25204 = NOVALUE;
    int _25203 = NOVALUE;
    int _25202 = NOVALUE;
    int _25200 = NOVALUE;
    int _25198 = NOVALUE;
    int _25197 = NOVALUE;
    int _25196 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer search_count*/

    /** 	tp = literal_init*/
    _tp_47507 = _55literal_init_47063;

    /** 	prev = 0*/
    _prev_47508 = 0;

    /** 	search_count = 0*/
    _search_count_47509 = 0;

    /** 	while tp != 0 do*/
L1: 
    if (_tp_47507 == 0)
    goto L2; // [29] 168

    /** 		search_count += 1*/
    _search_count_47509 = _search_count_47509 + 1;

    /** 		if search_count > SEARCH_LIMIT then  -- avoid n-squared algorithm*/
    if (binary_op_a(LESSEQ, _search_count_47509, _55SEARCH_LIMIT_47356)){
        goto L3; // [43] 52
    }

    /** 			exit*/
    goto L2; // [49] 168
L3: 

    /** 		if equal(d, SymTab[tp][S_OBJ]) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25196 = (int)*(((s1_ptr)_2)->base + _tp_47507);
    _2 = (int)SEQ_PTR(_25196);
    _25197 = (int)*(((s1_ptr)_2)->base + 1);
    _25196 = NOVALUE;
    if (_d_47504 == _25197)
    _25198 = 1;
    else if (IS_ATOM_INT(_d_47504) && IS_ATOM_INT(_25197))
    _25198 = 0;
    else
    _25198 = (compare(_d_47504, _25197) == 0);
    _25197 = NOVALUE;
    if (_25198 == 0)
    {
        _25198 = NOVALUE;
        goto L4; // [70] 140
    }
    else{
        _25198 = NOVALUE;
    }

    /** 			if tp != literal_init then*/
    if (_tp_47507 == _55literal_init_47063)
    goto L5; // [77] 133

    /** 				SymTab[prev][S_NEXT] = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_prev_47508 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25202 = (int)*(((s1_ptr)_2)->base + _tp_47507);
    _2 = (int)SEQ_PTR(_25202);
    _25203 = (int)*(((s1_ptr)_2)->base + 2);
    _25202 = NOVALUE;
    Ref(_25203);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25203;
    if( _1 != _25203 ){
        DeRef(_1);
    }
    _25203 = NOVALUE;
    _25200 = NOVALUE;

    /** 				SymTab[tp][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_tp_47507 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _55literal_init_47063;
    DeRef(_1);
    _25204 = NOVALUE;

    /** 				literal_init = tp*/
    _55literal_init_47063 = _tp_47507;
L5: 

    /** 			return tp*/
    DeRef(_d_47504);
    return _tp_47507;
L4: 

    /** 		prev = tp*/
    _prev_47508 = _tp_47507;

    /** 		tp = SymTab[tp][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25206 = (int)*(((s1_ptr)_2)->base + _tp_47507);
    _2 = (int)SEQ_PTR(_25206);
    _tp_47507 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_tp_47507)){
        _tp_47507 = (long)DBL_PTR(_tp_47507)->dbl;
    }
    _25206 = NOVALUE;

    /** 	end while*/
    goto L1; // [165] 29
L2: 

    /** 	p = tmp_alloc()*/
    _p_47506 = _55tmp_alloc();
    if (!IS_ATOM_INT(_p_47506)) {
        _1 = (long)(DBL_PTR(_p_47506)->dbl);
        if (UNIQUE(DBL_PTR(_p_47506)) && (DBL_PTR(_p_47506)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47506);
        _p_47506 = _1;
    }

    /** 	SymTab[p][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47506 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25209 = NOVALUE;

    /** 	SymTab[p][S_OBJ] = d*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47506 + ((s1_ptr)_2)->base);
    Ref(_d_47504);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _d_47504;
    DeRef(_1);
    _25211 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L6; // [211] 285
    }
    else{
    }

    /** 		SymTab[p][S_MODE] = M_TEMP  -- override CONSTANT for compile*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47506 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25213 = NOVALUE;

    /** 		SymTab[p][S_GTYPE] = TYPE_DOUBLE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47506 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25215 = NOVALUE;

    /** 		c_printf("int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25217 = (int)*(((s1_ptr)_2)->base + _p_47506);
    _2 = (int)SEQ_PTR(_25217);
    _25218 = (int)*(((s1_ptr)_2)->base + 34);
    _25217 = NOVALUE;
    RefDS(_25164);
    Ref(_25218);
    _56c_printf(_25164, _25218);
    _25218 = NOVALUE;

    /** 		c_hprintf("extern int _%d;\n", SymTab[p][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25219 = (int)*(((s1_ptr)_2)->base + _p_47506);
    _2 = (int)SEQ_PTR(_25219);
    _25220 = (int)*(((s1_ptr)_2)->base + 34);
    _25219 = NOVALUE;
    RefDS(_25167);
    Ref(_25220);
    _56c_hprintf(_25167, _25220);
    _25220 = NOVALUE;
L6: 

    /** 	SymTab[p][S_NEXT] = literal_init*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47506 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _55literal_init_47063;
    DeRef(_1);
    _25221 = NOVALUE;

    /** 	literal_init = p*/
    _55literal_init_47063 = _p_47506;

    /** 	return p*/
    DeRef(_d_47504);
    return _p_47506;
    ;
}


int _55NewTempSym(int _inlining_47578)
{
    int _p_47580 = NOVALUE;
    int _q_47581 = NOVALUE;
    int _25270 = NOVALUE;
    int _25268 = NOVALUE;
    int _25266 = NOVALUE;
    int _25264 = NOVALUE;
    int _25262 = NOVALUE;
    int _25260 = NOVALUE;
    int _25259 = NOVALUE;
    int _25258 = NOVALUE;
    int _25256 = NOVALUE;
    int _25255 = NOVALUE;
    int _25254 = NOVALUE;
    int _25252 = NOVALUE;
    int _25250 = NOVALUE;
    int _25247 = NOVALUE;
    int _25246 = NOVALUE;
    int _25245 = NOVALUE;
    int _25243 = NOVALUE;
    int _25241 = NOVALUE;
    int _25240 = NOVALUE;
    int _25239 = NOVALUE;
    int _25237 = NOVALUE;
    int _25235 = NOVALUE;
    int _25230 = NOVALUE;
    int _25229 = NOVALUE;
    int _25228 = NOVALUE;
    int _25227 = NOVALUE;
    int _25226 = NOVALUE;
    int _25225 = NOVALUE;
    int _25223 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_inlining_47578)) {
        _1 = (long)(DBL_PTR(_inlining_47578)->dbl);
        if (UNIQUE(DBL_PTR(_inlining_47578)) && (DBL_PTR(_inlining_47578)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_inlining_47578);
        _inlining_47578 = _1;
    }

    /** 	if inlining then*/
    if (_inlining_47578 == 0)
    {
        goto L1; // [5] 85
    }
    else{
    }

    /** 		p = SymTab[CurrentSub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25223 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25223);
    if (!IS_ATOM_INT(_38S_TEMPS_16643)){
        _p_47580 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    }
    else{
        _p_47580 = (int)*(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    }
    if (!IS_ATOM_INT(_p_47580)){
        _p_47580 = (long)DBL_PTR(_p_47580)->dbl;
    }
    _25223 = NOVALUE;

    /** 		while p != 0 and SymTab[p][S_SCOPE] != FREE do*/
L2: 
    _25225 = (_p_47580 != 0);
    if (_25225 == 0) {
        goto L3; // [35] 93
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25227 = (int)*(((s1_ptr)_2)->base + _p_47580);
    _2 = (int)SEQ_PTR(_25227);
    _25228 = (int)*(((s1_ptr)_2)->base + 4);
    _25227 = NOVALUE;
    if (IS_ATOM_INT(_25228)) {
        _25229 = (_25228 != 0);
    }
    else {
        _25229 = binary_op(NOTEQ, _25228, 0);
    }
    _25228 = NOVALUE;
    if (_25229 <= 0) {
        if (_25229 == 0) {
            DeRef(_25229);
            _25229 = NOVALUE;
            goto L3; // [58] 93
        }
        else {
            if (!IS_ATOM_INT(_25229) && DBL_PTR(_25229)->dbl == 0.0){
                DeRef(_25229);
                _25229 = NOVALUE;
                goto L3; // [58] 93
            }
            DeRef(_25229);
            _25229 = NOVALUE;
        }
    }
    DeRef(_25229);
    _25229 = NOVALUE;

    /** 			p = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25230 = (int)*(((s1_ptr)_2)->base + _p_47580);
    _2 = (int)SEQ_PTR(_25230);
    _p_47580 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_47580)){
        _p_47580 = (long)DBL_PTR(_p_47580)->dbl;
    }
    _25230 = NOVALUE;

    /** 		end while*/
    goto L2; // [79] 31
    goto L3; // [82] 93
L1: 

    /** 		p = 0*/
    _p_47580 = 0;
L3: 

    /** 	if p = 0 then*/
    if (_p_47580 != 0)
    goto L4; // [97] 213

    /** 		temps_allocated += 1*/
    _55temps_allocated_47575 = _55temps_allocated_47575 + 1;

    /** 		p = tmp_alloc()*/
    _p_47580 = _55tmp_alloc();
    if (!IS_ATOM_INT(_p_47580)) {
        _1 = (long)(DBL_PTR(_p_47580)->dbl);
        if (UNIQUE(DBL_PTR(_p_47580)) && (DBL_PTR(_p_47580)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_p_47580);
        _p_47580 = _1;
    }

    /** 		SymTab[p][S_MODE] = M_TEMP*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25235 = NOVALUE;

    /** 		SymTab[p][S_NEXT] = SymTab[CurrentSub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25239 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25239);
    if (!IS_ATOM_INT(_38S_TEMPS_16643)){
        _25240 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    }
    else{
        _25240 = (int)*(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    }
    _25239 = NOVALUE;
    Ref(_25240);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25240;
    if( _1 != _25240 ){
        DeRef(_1);
    }
    _25240 = NOVALUE;
    _25237 = NOVALUE;

    /** 		SymTab[CurrentSub][S_TEMPS] = p*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_TEMPS_16643))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    _1 = *(int *)_2;
    *(int *)_2 = _p_47580;
    DeRef(_1);
    _25241 = NOVALUE;

    /** 		if inlining then*/
    if (_inlining_47578 == 0)
    {
        goto L5; // [181] 343
    }
    else{
    }

    /** 			SymTab[CurrentSub][S_STACK_SPACE] += 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658)){
        _25245 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    }
    else{
        _25245 = (int)*(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    }
    _25243 = NOVALUE;
    if (IS_ATOM_INT(_25245)) {
        _25246 = _25245 + 1;
        if (_25246 > MAXINT){
            _25246 = NewDouble((double)_25246);
        }
    }
    else
    _25246 = binary_op(PLUS, 1, _25245);
    _25245 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    _1 = *(int *)_2;
    *(int *)_2 = _25246;
    if( _1 != _25246 ){
        DeRef(_1);
    }
    _25246 = NOVALUE;
    _25243 = NOVALUE;
    goto L5; // [210] 343
L4: 

    /** 	elsif TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L6; // [217] 342
    }
    else{
    }

    /** 		SymTab[p][S_SCOPE] = DELETED*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _25247 = NOVALUE;

    /** 		q = tmp_alloc()*/
    _q_47581 = _55tmp_alloc();
    if (!IS_ATOM_INT(_q_47581)) {
        _1 = (long)(DBL_PTR(_q_47581)->dbl);
        if (UNIQUE(DBL_PTR(_q_47581)) && (DBL_PTR(_q_47581)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_q_47581);
        _q_47581 = _1;
    }

    /** 		SymTab[q][S_MODE] = M_TEMP*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_q_47581 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _25250 = NOVALUE;

    /** 		SymTab[q][S_TEMP_NAME] = SymTab[p][S_TEMP_NAME]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_q_47581 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25254 = (int)*(((s1_ptr)_2)->base + _p_47580);
    _2 = (int)SEQ_PTR(_25254);
    _25255 = (int)*(((s1_ptr)_2)->base + 34);
    _25254 = NOVALUE;
    Ref(_25255);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 34);
    _1 = *(int *)_2;
    *(int *)_2 = _25255;
    if( _1 != _25255 ){
        DeRef(_1);
    }
    _25255 = NOVALUE;
    _25252 = NOVALUE;

    /** 		SymTab[q][S_NEXT] = SymTab[CurrentSub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_q_47581 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25258 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25258);
    if (!IS_ATOM_INT(_38S_TEMPS_16643)){
        _25259 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    }
    else{
        _25259 = (int)*(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    }
    _25258 = NOVALUE;
    Ref(_25259);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _25259;
    if( _1 != _25259 ){
        DeRef(_1);
    }
    _25259 = NOVALUE;
    _25256 = NOVALUE;

    /** 		SymTab[CurrentSub][S_TEMPS] = q*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_TEMPS_16643))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    _1 = *(int *)_2;
    *(int *)_2 = _q_47581;
    DeRef(_1);
    _25260 = NOVALUE;

    /** 		p = q*/
    _p_47580 = _q_47581;
L6: 
L5: 

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L7; // [347] 385
    }
    else{
    }

    /** 		SymTab[p][S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _25262 = NOVALUE;

    /** 		SymTab[p][S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    _25264 = NOVALUE;
L7: 

    /** 	SymTab[p][S_OBJ] = NOVALUE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    _25266 = NOVALUE;

    /** 	SymTab[p][S_USAGE] = T_UNKNOWN*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 4;
    DeRef(_1);
    _25268 = NOVALUE;

    /** 	SymTab[p][S_SCOPE] = IN_USE*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47580 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _25270 = NOVALUE;

    /** 	return p*/
    DeRef(_25225);
    _25225 = NOVALUE;
    return _p_47580;
    ;
}


void _55InitSymTab()
{
    int _hashval_47697 = NOVALUE;
    int _len_47698 = NOVALUE;
    int _s_47700 = NOVALUE;
    int _st_index_47701 = NOVALUE;
    int _kname_47702 = NOVALUE;
    int _fixups_47703 = NOVALUE;
    int _si_47843 = NOVALUE;
    int _sj_47844 = NOVALUE;
    int _25854 = NOVALUE;
    int _25853 = NOVALUE;
    int _25386 = NOVALUE;
    int _25385 = NOVALUE;
    int _25384 = NOVALUE;
    int _25383 = NOVALUE;
    int _25382 = NOVALUE;
    int _25380 = NOVALUE;
    int _25379 = NOVALUE;
    int _25378 = NOVALUE;
    int _25377 = NOVALUE;
    int _25375 = NOVALUE;
    int _25373 = NOVALUE;
    int _25371 = NOVALUE;
    int _25370 = NOVALUE;
    int _25368 = NOVALUE;
    int _25366 = NOVALUE;
    int _25364 = NOVALUE;
    int _25363 = NOVALUE;
    int _25361 = NOVALUE;
    int _25360 = NOVALUE;
    int _25359 = NOVALUE;
    int _25358 = NOVALUE;
    int _25357 = NOVALUE;
    int _25354 = NOVALUE;
    int _25353 = NOVALUE;
    int _25352 = NOVALUE;
    int _25350 = NOVALUE;
    int _25349 = NOVALUE;
    int _25348 = NOVALUE;
    int _25346 = NOVALUE;
    int _25345 = NOVALUE;
    int _25344 = NOVALUE;
    int _25341 = NOVALUE;
    int _25339 = NOVALUE;
    int _25337 = NOVALUE;
    int _25336 = NOVALUE;
    int _25333 = NOVALUE;
    int _25332 = NOVALUE;
    int _25330 = NOVALUE;
    int _25328 = NOVALUE;
    int _25326 = NOVALUE;
    int _25323 = NOVALUE;
    int _25322 = NOVALUE;
    int _25321 = NOVALUE;
    int _25318 = NOVALUE;
    int _25317 = NOVALUE;
    int _25315 = NOVALUE;
    int _25314 = NOVALUE;
    int _25312 = NOVALUE;
    int _25311 = NOVALUE;
    int _25310 = NOVALUE;
    int _25308 = NOVALUE;
    int _25306 = NOVALUE;
    int _25305 = NOVALUE;
    int _25303 = NOVALUE;
    int _25302 = NOVALUE;
    int _25301 = NOVALUE;
    int _25299 = NOVALUE;
    int _25298 = NOVALUE;
    int _25297 = NOVALUE;
    int _25295 = NOVALUE;
    int _25294 = NOVALUE;
    int _25293 = NOVALUE;
    int _25291 = NOVALUE;
    int _25290 = NOVALUE;
    int _25289 = NOVALUE;
    int _25288 = NOVALUE;
    int _25287 = NOVALUE;
    int _25286 = NOVALUE;
    int _25285 = NOVALUE;
    int _25284 = NOVALUE;
    int _25283 = NOVALUE;
    int _25282 = NOVALUE;
    int _25280 = NOVALUE;
    int _25279 = NOVALUE;
    int _25278 = NOVALUE;
    int _25277 = NOVALUE;
    int _25273 = NOVALUE;
    int _25272 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence kname, fixups = {}*/
    RefDS(_22663);
    DeRefi(_fixups_47703);
    _fixups_47703 = _22663;

    /** 	for k = 1 to length(keylist) do*/
    if (IS_SEQUENCE(_65keylist_23552)){
            _25272 = SEQ_PTR(_65keylist_23552)->length;
    }
    else {
        _25272 = 1;
    }
    {
        int _k_47705;
        _k_47705 = 1;
L1: 
        if (_k_47705 > _25272){
            goto L2; // [15] 560
        }

        /** 		kname = keylist[k][K_NAME]*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25273 = (int)*(((s1_ptr)_2)->base + _k_47705);
        DeRef(_kname_47702);
        _2 = (int)SEQ_PTR(_25273);
        _kname_47702 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_kname_47702);
        _25273 = NOVALUE;

        /** 		len = length(kname)*/
        if (IS_SEQUENCE(_kname_47702)){
                _len_47698 = SEQ_PTR(_kname_47702)->length;
        }
        else {
            _len_47698 = 1;
        }

        /** 		hashval = hashfn(kname)*/
        RefDS(_kname_47702);
        _hashval_47697 = _55hashfn(_kname_47702);
        if (!IS_ATOM_INT(_hashval_47697)) {
            _1 = (long)(DBL_PTR(_hashval_47697)->dbl);
            if (UNIQUE(DBL_PTR(_hashval_47697)) && (DBL_PTR(_hashval_47697)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_hashval_47697);
            _hashval_47697 = _1;
        }

        /** 		st_index = NewEntry(kname,*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25277 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25277);
        _25278 = (int)*(((s1_ptr)_2)->base + 2);
        _25277 = NOVALUE;
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25279 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25279);
        _25280 = (int)*(((s1_ptr)_2)->base + 3);
        _25279 = NOVALUE;
        RefDS(_kname_47702);
        Ref(_25278);
        Ref(_25280);
        _st_index_47701 = _55NewEntry(_kname_47702, 0, _25278, _25280, _hashval_47697, 0, 0);
        _25278 = NOVALUE;
        _25280 = NOVALUE;
        if (!IS_ATOM_INT(_st_index_47701)) {
            _1 = (long)(DBL_PTR(_st_index_47701)->dbl);
            if (UNIQUE(DBL_PTR(_st_index_47701)) && (DBL_PTR(_st_index_47701)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_st_index_47701);
            _st_index_47701 = _1;
        }

        /** 		if find(keylist[k][K_TOKEN], RTN_TOKS) then*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25282 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25282);
        _25283 = (int)*(((s1_ptr)_2)->base + 3);
        _25282 = NOVALUE;
        _25284 = find_from(_25283, _39RTN_TOKS_16547, 1);
        _25283 = NOVALUE;
        if (_25284 == 0)
        {
            _25284 = NOVALUE;
            goto L3; // [110] 325
        }
        else{
            _25284 = NOVALUE;
        }

        /** 			SymTab[st_index] = SymTab[st_index] &*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25285 = (int)*(((s1_ptr)_2)->base + _st_index_47701);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25286 = (int)*(((s1_ptr)_2)->base + _st_index_47701);
        if (IS_SEQUENCE(_25286)){
                _25287 = SEQ_PTR(_25286)->length;
        }
        else {
            _25287 = 1;
        }
        _25286 = NOVALUE;
        _25288 = _38SIZEOF_ROUTINE_ENTRY_16724 - _25287;
        _25287 = NOVALUE;
        _25289 = Repeat(0, _25288);
        _25288 = NOVALUE;
        if (IS_SEQUENCE(_25285) && IS_ATOM(_25289)) {
        }
        else if (IS_ATOM(_25285) && IS_SEQUENCE(_25289)) {
            Ref(_25285);
            Prepend(&_25290, _25289, _25285);
        }
        else {
            Concat((object_ptr)&_25290, _25285, _25289);
            _25285 = NOVALUE;
        }
        _25285 = NOVALUE;
        DeRefDS(_25289);
        _25289 = NOVALUE;
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _st_index_47701);
        _1 = *(int *)_2;
        *(int *)_2 = _25290;
        if( _1 != _25290 ){
            DeRef(_1);
        }
        _25290 = NOVALUE;

        /** 			SymTab[st_index][S_NUM_ARGS] = keylist[k][K_NUM_ARGS]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25293 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25293);
        _25294 = (int)*(((s1_ptr)_2)->base + 5);
        _25293 = NOVALUE;
        Ref(_25294);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_38S_NUM_ARGS_16649))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
        _1 = *(int *)_2;
        *(int *)_2 = _25294;
        if( _1 != _25294 ){
            DeRef(_1);
        }
        _25294 = NOVALUE;
        _25291 = NOVALUE;

        /** 			SymTab[st_index][S_OPCODE] = keylist[k][K_OPCODE]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25297 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25297);
        _25298 = (int)*(((s1_ptr)_2)->base + 4);
        _25297 = NOVALUE;
        Ref(_25298);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 21);
        _1 = *(int *)_2;
        *(int *)_2 = _25298;
        if( _1 != _25298 ){
            DeRef(_1);
        }
        _25298 = NOVALUE;
        _25295 = NOVALUE;

        /** 			SymTab[st_index][S_EFFECT] = keylist[k][K_EFFECT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25301 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25301);
        _25302 = (int)*(((s1_ptr)_2)->base + 6);
        _25301 = NOVALUE;
        Ref(_25302);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 23);
        _1 = *(int *)_2;
        *(int *)_2 = _25302;
        if( _1 != _25302 ){
            DeRef(_1);
        }
        _25302 = NOVALUE;
        _25299 = NOVALUE;

        /** 			SymTab[st_index][S_REFLIST] = {}*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
        RefDS(_22663);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 24);
        _1 = *(int *)_2;
        *(int *)_2 = _22663;
        DeRef(_1);
        _25303 = NOVALUE;

        /** 			if length(keylist[k]) > K_EFFECT then*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25305 = (int)*(((s1_ptr)_2)->base + _k_47705);
        if (IS_SEQUENCE(_25305)){
                _25306 = SEQ_PTR(_25305)->length;
        }
        else {
            _25306 = 1;
        }
        _25305 = NOVALUE;
        if (_25306 <= 6)
        goto L4; // [259] 324

        /** 			    SymTab[st_index][S_CODE] = keylist[k][K_CODE]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25310 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25310);
        _25311 = (int)*(((s1_ptr)_2)->base + 7);
        _25310 = NOVALUE;
        Ref(_25311);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_38S_CODE_16610))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
        _1 = *(int *)_2;
        *(int *)_2 = _25311;
        if( _1 != _25311 ){
            DeRef(_1);
        }
        _25311 = NOVALUE;
        _25308 = NOVALUE;

        /** 			    SymTab[st_index][S_DEF_ARGS] = keylist[k][K_DEF_ARGS]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25314 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25314);
        _25315 = (int)*(((s1_ptr)_2)->base + 8);
        _25314 = NOVALUE;
        Ref(_25315);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 28);
        _1 = *(int *)_2;
        *(int *)_2 = _25315;
        if( _1 != _25315 ){
            DeRef(_1);
        }
        _25315 = NOVALUE;
        _25312 = NOVALUE;

        /** 			    fixups &= st_index*/
        Append(&_fixups_47703, _fixups_47703, _st_index_47701);
L4: 
L3: 

        /** 		if keylist[k][K_TOKEN] = PROC then*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25317 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25317);
        _25318 = (int)*(((s1_ptr)_2)->base + 3);
        _25317 = NOVALUE;
        if (binary_op_a(NOTEQ, _25318, 27)){
            _25318 = NOVALUE;
            goto L5; // [341] 365
        }
        _25318 = NOVALUE;

        /** 			if equal(kname, "<TopLevel>") then*/
        if (_kname_47702 == _25320)
        _25321 = 1;
        else if (IS_ATOM_INT(_kname_47702) && IS_ATOM_INT(_25320))
        _25321 = 0;
        else
        _25321 = (compare(_kname_47702, _25320) == 0);
        if (_25321 == 0)
        {
            _25321 = NOVALUE;
            goto L6; // [351] 462
        }
        else{
            _25321 = NOVALUE;
        }

        /** 				TopLevelSub = st_index*/
        _38TopLevelSub_16953 = _st_index_47701;
        goto L6; // [362] 462
L5: 

        /** 		elsif keylist[k][K_TOKEN] = TYPE then*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _25322 = (int)*(((s1_ptr)_2)->base + _k_47705);
        _2 = (int)SEQ_PTR(_25322);
        _25323 = (int)*(((s1_ptr)_2)->base + 3);
        _25322 = NOVALUE;
        if (binary_op_a(NOTEQ, _25323, 504)){
            _25323 = NOVALUE;
            goto L7; // [381] 461
        }
        _25323 = NOVALUE;

        /** 			if equal(kname, "object") then*/
        if (_kname_47702 == _25325)
        _25326 = 1;
        else if (IS_ATOM_INT(_kname_47702) && IS_ATOM_INT(_25325))
        _25326 = 0;
        else
        _25326 = (compare(_kname_47702, _25325) == 0);
        if (_25326 == 0)
        {
            _25326 = NOVALUE;
            goto L8; // [391] 404
        }
        else{
            _25326 = NOVALUE;
        }

        /** 				object_type = st_index*/
        _55object_type_47055 = _st_index_47701;
        goto L9; // [401] 460
L8: 

        /** 			elsif equal(kname, "atom") then*/
        if (_kname_47702 == _25327)
        _25328 = 1;
        else if (IS_ATOM_INT(_kname_47702) && IS_ATOM_INT(_25327))
        _25328 = 0;
        else
        _25328 = (compare(_kname_47702, _25327) == 0);
        if (_25328 == 0)
        {
            _25328 = NOVALUE;
            goto LA; // [410] 423
        }
        else{
            _25328 = NOVALUE;
        }

        /** 				atom_type = st_index*/
        _55atom_type_47057 = _st_index_47701;
        goto L9; // [420] 460
LA: 

        /** 			elsif equal(kname, "integer") then*/
        if (_kname_47702 == _25329)
        _25330 = 1;
        else if (IS_ATOM_INT(_kname_47702) && IS_ATOM_INT(_25329))
        _25330 = 0;
        else
        _25330 = (compare(_kname_47702, _25329) == 0);
        if (_25330 == 0)
        {
            _25330 = NOVALUE;
            goto LB; // [429] 442
        }
        else{
            _25330 = NOVALUE;
        }

        /** 				integer_type = st_index*/
        _55integer_type_47061 = _st_index_47701;
        goto L9; // [439] 460
LB: 

        /** 			elsif equal(kname, "sequence") then*/
        if (_kname_47702 == _25331)
        _25332 = 1;
        else if (IS_ATOM_INT(_kname_47702) && IS_ATOM_INT(_25331))
        _25332 = 0;
        else
        _25332 = (compare(_kname_47702, _25331) == 0);
        if (_25332 == 0)
        {
            _25332 = NOVALUE;
            goto LC; // [448] 459
        }
        else{
            _25332 = NOVALUE;
        }

        /** 				sequence_type = st_index*/
        _55sequence_type_47059 = _st_index_47701;
LC: 
L9: 
L7: 
L6: 

        /** 		if buckets[hashval] = 0 then*/
        _2 = (int)SEQ_PTR(_55buckets_47051);
        _25333 = (int)*(((s1_ptr)_2)->base + _hashval_47697);
        if (binary_op_a(NOTEQ, _25333, 0)){
            _25333 = NOVALUE;
            goto LD; // [470] 485
        }
        _25333 = NOVALUE;

        /** 			buckets[hashval] = st_index*/
        _2 = (int)SEQ_PTR(_55buckets_47051);
        _2 = (int)(((s1_ptr)_2)->base + _hashval_47697);
        _1 = *(int *)_2;
        *(int *)_2 = _st_index_47701;
        DeRef(_1);
        goto LE; // [482] 553
LD: 

        /** 			s = buckets[hashval]*/
        _2 = (int)SEQ_PTR(_55buckets_47051);
        _s_47700 = (int)*(((s1_ptr)_2)->base + _hashval_47697);
        if (!IS_ATOM_INT(_s_47700)){
            _s_47700 = (long)DBL_PTR(_s_47700)->dbl;
        }

        /** 			while SymTab[s][S_SAMEHASH] != 0 do*/
LF: 
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25336 = (int)*(((s1_ptr)_2)->base + _s_47700);
        _2 = (int)SEQ_PTR(_25336);
        _25337 = (int)*(((s1_ptr)_2)->base + 9);
        _25336 = NOVALUE;
        if (binary_op_a(EQUALS, _25337, 0)){
            _25337 = NOVALUE;
            goto L10; // [512] 537
        }
        _25337 = NOVALUE;

        /** 				s = SymTab[s][S_SAMEHASH]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25339 = (int)*(((s1_ptr)_2)->base + _s_47700);
        _2 = (int)SEQ_PTR(_25339);
        _s_47700 = (int)*(((s1_ptr)_2)->base + 9);
        if (!IS_ATOM_INT(_s_47700)){
            _s_47700 = (long)DBL_PTR(_s_47700)->dbl;
        }
        _25339 = NOVALUE;

        /** 			end while*/
        goto LF; // [534] 500
L10: 

        /** 			SymTab[s][S_SAMEHASH] = st_index*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_s_47700 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 9);
        _1 = *(int *)_2;
        *(int *)_2 = _st_index_47701;
        DeRef(_1);
        _25341 = NOVALUE;
LE: 

        /** 	end for*/
        _k_47705 = _k_47705 + 1;
        goto L1; // [555] 22
L2: 
        ;
    }

    /** 	file_start_sym = length(SymTab)*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _38file_start_sym_16952 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _38file_start_sym_16952 = 1;
    }

    /** 	sequence si, sj*/

    /** 	CurrentSub = TopLevelSub*/
    _38CurrentSub_16954 = _38TopLevelSub_16953;

    /** 	for i=1 to length(fixups) do*/
    if (IS_SEQUENCE(_fixups_47703)){
            _25344 = SEQ_PTR(_fixups_47703)->length;
    }
    else {
        _25344 = 1;
    }
    {
        int _i_47848;
        _i_47848 = 1;
L11: 
        if (_i_47848 > _25344){
            goto L12; // [585] 945
        }

        /** 	    si = SymTab[fixups[i]][S_CODE] -- seq of either 0's or sequences of tokens*/
        _2 = (int)SEQ_PTR(_fixups_47703);
        _25345 = (int)*(((s1_ptr)_2)->base + _i_47848);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25346 = (int)*(((s1_ptr)_2)->base + _25345);
        DeRef(_si_47843);
        _2 = (int)SEQ_PTR(_25346);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _si_47843 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _si_47843 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        Ref(_si_47843);
        _25346 = NOVALUE;

        /** 	    for j=1 to length(si) do*/
        if (IS_SEQUENCE(_si_47843)){
                _25348 = SEQ_PTR(_si_47843)->length;
        }
        else {
            _25348 = 1;
        }
        {
            int _j_47856;
            _j_47856 = 1;
L13: 
            if (_j_47856 > _25348){
                goto L14; // [617] 919
            }

            /** 	        if sequence(si[j]) then*/
            _2 = (int)SEQ_PTR(_si_47843);
            _25349 = (int)*(((s1_ptr)_2)->base + _j_47856);
            _25350 = IS_SEQUENCE(_25349);
            _25349 = NOVALUE;
            if (_25350 == 0)
            {
                _25350 = NOVALUE;
                goto L15; // [633] 912
            }
            else{
                _25350 = NOVALUE;
            }

            /** 	            sj = si[j] -- a sequence of tokens*/
            DeRef(_sj_47844);
            _2 = (int)SEQ_PTR(_si_47843);
            _sj_47844 = (int)*(((s1_ptr)_2)->base + _j_47856);
            Ref(_sj_47844);

            /** 				for ij=1 to length(sj) do*/
            if (IS_SEQUENCE(_sj_47844)){
                    _25352 = SEQ_PTR(_sj_47844)->length;
            }
            else {
                _25352 = 1;
            }
            {
                int _ij_47863;
                _ij_47863 = 1;
L16: 
                if (_ij_47863 > _25352){
                    goto L17; // [649] 905
                }

                /** 	                switch sj[ij][T_ID] with fallthru do*/
                _2 = (int)SEQ_PTR(_sj_47844);
                _25353 = (int)*(((s1_ptr)_2)->base + _ij_47863);
                _2 = (int)SEQ_PTR(_25353);
                _25354 = (int)*(((s1_ptr)_2)->base + 1);
                _25353 = NOVALUE;
                if (IS_SEQUENCE(_25354) ){
                    goto L18; // [668] 898
                }
                if(!IS_ATOM_INT(_25354)){
                    if( (DBL_PTR(_25354)->dbl != (double) ((int) DBL_PTR(_25354)->dbl) ) ){
                        goto L18; // [668] 898
                    }
                    _0 = (int) DBL_PTR(_25354)->dbl;
                }
                else {
                    _0 = _25354;
                };
                _25354 = NOVALUE;
                switch ( _0 ){ 

                    /** 	                    case ATOM then -- must create a lasting temp*/
                    case 502:

                    /** 	                    	if integer(sj[ij][T_SYM]) then*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    _25357 = (int)*(((s1_ptr)_2)->base + _ij_47863);
                    _2 = (int)SEQ_PTR(_25357);
                    _25358 = (int)*(((s1_ptr)_2)->base + 2);
                    _25357 = NOVALUE;
                    if (IS_ATOM_INT(_25358))
                    _25359 = 1;
                    else if (IS_ATOM_DBL(_25358))
                    _25359 = IS_ATOM_INT(DoubleToInt(_25358));
                    else
                    _25359 = 0;
                    _25358 = NOVALUE;
                    if (_25359 == 0)
                    {
                        _25359 = NOVALUE;
                        goto L19; // [692] 716
                    }
                    else{
                        _25359 = NOVALUE;
                    }

                    /** 								st_index = NewIntSym(sj[ij][T_SYM])*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    _25360 = (int)*(((s1_ptr)_2)->base + _ij_47863);
                    _2 = (int)SEQ_PTR(_25360);
                    _25361 = (int)*(((s1_ptr)_2)->base + 2);
                    _25360 = NOVALUE;
                    Ref(_25361);
                    _st_index_47701 = _55NewIntSym(_25361);
                    _25361 = NOVALUE;
                    if (!IS_ATOM_INT(_st_index_47701)) {
                        _1 = (long)(DBL_PTR(_st_index_47701)->dbl);
                        if (UNIQUE(DBL_PTR(_st_index_47701)) && (DBL_PTR(_st_index_47701)->cleanup != 0))
                        RTFatal("Cannot assign value with a destructor to an integer");                        DeRefDS(_st_index_47701);
                        _st_index_47701 = _1;
                    }
                    goto L1A; // [713] 735
L19: 

                    /** 								st_index = NewDoubleSym(sj[ij][T_SYM])*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    _25363 = (int)*(((s1_ptr)_2)->base + _ij_47863);
                    _2 = (int)SEQ_PTR(_25363);
                    _25364 = (int)*(((s1_ptr)_2)->base + 2);
                    _25363 = NOVALUE;
                    Ref(_25364);
                    _st_index_47701 = _55NewDoubleSym(_25364);
                    _25364 = NOVALUE;
                    if (!IS_ATOM_INT(_st_index_47701)) {
                        _1 = (long)(DBL_PTR(_st_index_47701)->dbl);
                        if (UNIQUE(DBL_PTR(_st_index_47701)) && (DBL_PTR(_st_index_47701)->cleanup != 0))
                        RTFatal("Cannot assign value with a destructor to an integer");                        DeRefDS(_st_index_47701);
                        _st_index_47701 = _1;
                    }
L1A: 

                    /** 							SymTab[st_index][S_SCOPE] = IN_USE -- TempKeep()*/
                    _2 = (int)SEQ_PTR(_35SymTab_15595);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _35SymTab_15595 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 4);
                    _1 = *(int *)_2;
                    *(int *)_2 = 1;
                    DeRef(_1);
                    _25366 = NOVALUE;

                    /** 							sj[ij][T_SYM] = st_index*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47844 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_ij_47863 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 2);
                    _1 = *(int *)_2;
                    *(int *)_2 = _st_index_47701;
                    DeRef(_1);
                    _25368 = NOVALUE;

                    /** 							break*/
                    goto L18; // [769] 898

                    /** 						case STRING then -- same*/
                    case 503:

                    /** 	                    	st_index = NewStringSym(sj[ij][T_SYM])*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    _25370 = (int)*(((s1_ptr)_2)->base + _ij_47863);
                    _2 = (int)SEQ_PTR(_25370);
                    _25371 = (int)*(((s1_ptr)_2)->base + 2);
                    _25370 = NOVALUE;
                    Ref(_25371);
                    _st_index_47701 = _55NewStringSym(_25371);
                    _25371 = NOVALUE;
                    if (!IS_ATOM_INT(_st_index_47701)) {
                        _1 = (long)(DBL_PTR(_st_index_47701)->dbl);
                        if (UNIQUE(DBL_PTR(_st_index_47701)) && (DBL_PTR(_st_index_47701)->cleanup != 0))
                        RTFatal("Cannot assign value with a destructor to an integer");                        DeRefDS(_st_index_47701);
                        _st_index_47701 = _1;
                    }

                    /** 							SymTab[st_index][S_SCOPE] = IN_USE -- TempKeep()*/
                    _2 = (int)SEQ_PTR(_35SymTab_15595);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _35SymTab_15595 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_st_index_47701 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 4);
                    _1 = *(int *)_2;
                    *(int *)_2 = 1;
                    DeRef(_1);
                    _25373 = NOVALUE;

                    /** 							sj[ij][T_SYM] = st_index*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47844 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_ij_47863 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 2);
                    _1 = *(int *)_2;
                    *(int *)_2 = _st_index_47701;
                    DeRef(_1);
                    _25375 = NOVALUE;

                    /** 							break*/
                    goto L18; // [825] 898

                    /** 						case BUILT_IN then -- name of a builtin in econd field*/
                    case 511:

                    /**                             sj[ij] = keyfind(sj[ij][T_SYM],-1)*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    _25377 = (int)*(((s1_ptr)_2)->base + _ij_47863);
                    _2 = (int)SEQ_PTR(_25377);
                    _25378 = (int)*(((s1_ptr)_2)->base + 2);
                    _25377 = NOVALUE;
                    Ref(_25378);
                    DeRef(_25853);
                    _25853 = _25378;
                    _25854 = _55hashfn(_25853);
                    _25853 = NOVALUE;
                    Ref(_25378);
                    _25379 = _55keyfind(_25378, -1, _38current_file_no_16946, 0, _25854);
                    _25378 = NOVALUE;
                    _25854 = NOVALUE;
                    _2 = (int)SEQ_PTR(_sj_47844);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47844 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + _ij_47863);
                    _1 = *(int *)_2;
                    *(int *)_2 = _25379;
                    if( _1 != _25379 ){
                        DeRef(_1);
                    }
                    _25379 = NOVALUE;

                    /** 							break*/
                    goto L18; // [866] 898

                    /** 						case DEF_PARAM then*/
                    case 510:

                    /** 							sj[ij][T_SYM] &= fixups[i]*/
                    _2 = (int)SEQ_PTR(_sj_47844);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        _sj_47844 = MAKE_SEQ(_2);
                    }
                    _3 = (int)(_ij_47863 + ((s1_ptr)_2)->base);
                    _2 = (int)SEQ_PTR(_fixups_47703);
                    _25382 = (int)*(((s1_ptr)_2)->base + _i_47848);
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    _25383 = (int)*(((s1_ptr)_2)->base + 2);
                    _25380 = NOVALUE;
                    if (IS_SEQUENCE(_25383) && IS_ATOM(_25382)) {
                        Append(&_25384, _25383, _25382);
                    }
                    else if (IS_ATOM(_25383) && IS_SEQUENCE(_25382)) {
                    }
                    else {
                        Concat((object_ptr)&_25384, _25383, _25382);
                        _25383 = NOVALUE;
                    }
                    _25383 = NOVALUE;
                    _25382 = NOVALUE;
                    _2 = (int)SEQ_PTR(*(int *)_3);
                    if (!UNIQUE(_2)) {
                        _2 = (int)SequenceCopy((s1_ptr)_2);
                        *(int *)_3 = MAKE_SEQ(_2);
                    }
                    _2 = (int)(((s1_ptr)_2)->base + 2);
                    _1 = *(int *)_2;
                    *(int *)_2 = _25384;
                    if( _1 != _25384 ){
                        DeRef(_1);
                    }
                    _25384 = NOVALUE;
                    _25380 = NOVALUE;
                ;}L18: 

                /** 				end for*/
                _ij_47863 = _ij_47863 + 1;
                goto L16; // [900] 656
L17: 
                ;
            }

            /** 				si[j] = sj*/
            RefDS(_sj_47844);
            _2 = (int)SEQ_PTR(_si_47843);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _si_47843 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_47856);
            _1 = *(int *)_2;
            *(int *)_2 = _sj_47844;
            DeRef(_1);
L15: 

            /** 		end for*/
            _j_47856 = _j_47856 + 1;
            goto L13; // [914] 624
L14: 
            ;
        }

        /** 		SymTab[fixups[i]][S_CODE] = si*/
        _2 = (int)SEQ_PTR(_fixups_47703);
        _25385 = (int)*(((s1_ptr)_2)->base + _i_47848);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_25385 + ((s1_ptr)_2)->base);
        RefDS(_si_47843);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_38S_CODE_16610))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
        _1 = *(int *)_2;
        *(int *)_2 = _si_47843;
        DeRef(_1);
        _25386 = NOVALUE;

        /** 	end for*/
        _i_47848 = _i_47848 + 1;
        goto L11; // [940] 592
L12: 
        ;
    }

    /** end procedure*/
    DeRef(_kname_47702);
    DeRefi(_fixups_47703);
    DeRef(_si_47843);
    DeRef(_sj_47844);
    _25305 = NOVALUE;
    _25286 = NOVALUE;
    _25345 = NOVALUE;
    _25385 = NOVALUE;
    return;
    ;
}


void _55add_ref(int _tok_47931)
{
    int _s_47933 = NOVALUE;
    int _25402 = NOVALUE;
    int _25401 = NOVALUE;
    int _25399 = NOVALUE;
    int _25398 = NOVALUE;
    int _25397 = NOVALUE;
    int _25395 = NOVALUE;
    int _25394 = NOVALUE;
    int _25393 = NOVALUE;
    int _25392 = NOVALUE;
    int _25391 = NOVALUE;
    int _25390 = NOVALUE;
    int _25389 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	s = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_47931);
    _s_47933 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_47933)){
        _s_47933 = (long)DBL_PTR(_s_47933)->dbl;
    }

    /** 	if s != CurrentSub and -- ignore self-ref's*/
    _25389 = (_s_47933 != _38CurrentSub_16954);
    if (_25389 == 0) {
        goto L1; // [19] 98
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25391 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25391);
    _25392 = (int)*(((s1_ptr)_2)->base + 24);
    _25391 = NOVALUE;
    _25393 = find_from(_s_47933, _25392, 1);
    _25392 = NOVALUE;
    _25394 = (_25393 == 0);
    _25393 = NOVALUE;
    if (_25394 == 0)
    {
        DeRef(_25394);
        _25394 = NOVALUE;
        goto L1; // [46] 98
    }
    else{
        DeRef(_25394);
        _25394 = NOVALUE;
    }

    /** 		SymTab[s][S_NREFS] += 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_47933 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25397 = (int)*(((s1_ptr)_2)->base + 12);
    _25395 = NOVALUE;
    if (IS_ATOM_INT(_25397)) {
        _25398 = _25397 + 1;
        if (_25398 > MAXINT){
            _25398 = NewDouble((double)_25398);
        }
    }
    else
    _25398 = binary_op(PLUS, 1, _25397);
    _25397 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _25398;
    if( _1 != _25398 ){
        DeRef(_1);
    }
    _25398 = NOVALUE;
    _25395 = NOVALUE;

    /** 		SymTab[CurrentSub][S_REFLIST] &= s*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25401 = (int)*(((s1_ptr)_2)->base + 24);
    _25399 = NOVALUE;
    if (IS_SEQUENCE(_25401) && IS_ATOM(_s_47933)) {
        Append(&_25402, _25401, _s_47933);
    }
    else if (IS_ATOM(_25401) && IS_SEQUENCE(_s_47933)) {
    }
    else {
        Concat((object_ptr)&_25402, _25401, _s_47933);
        _25401 = NOVALUE;
    }
    _25401 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 24);
    _1 = *(int *)_2;
    *(int *)_2 = _25402;
    if( _1 != _25402 ){
        DeRef(_1);
    }
    _25402 = NOVALUE;
    _25399 = NOVALUE;
L1: 

    /** end procedure*/
    DeRef(_tok_47931);
    DeRef(_25389);
    _25389 = NOVALUE;
    return;
    ;
}


void _55mark_all(int _attribute_47963)
{
    int _p_47966 = NOVALUE;
    int _sym_file_47973 = NOVALUE;
    int _scope_47990 = NOVALUE;
    int _25434 = NOVALUE;
    int _25433 = NOVALUE;
    int _25432 = NOVALUE;
    int _25430 = NOVALUE;
    int _25428 = NOVALUE;
    int _25427 = NOVALUE;
    int _25426 = NOVALUE;
    int _25425 = NOVALUE;
    int _25424 = NOVALUE;
    int _25422 = NOVALUE;
    int _25421 = NOVALUE;
    int _25420 = NOVALUE;
    int _25419 = NOVALUE;
    int _25415 = NOVALUE;
    int _25414 = NOVALUE;
    int _25413 = NOVALUE;
    int _25411 = NOVALUE;
    int _25410 = NOVALUE;
    int _25408 = NOVALUE;
    int _25406 = NOVALUE;
    int _25403 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if just_mark_everything_from then*/
    if (_55just_mark_everything_from_47960 == 0)
    {
        goto L1; // [7] 270
    }
    else{
    }

    /** 		symtab_pointer p = SymTab[just_mark_everything_from][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25403 = (int)*(((s1_ptr)_2)->base + _55just_mark_everything_from_47960);
    _2 = (int)SEQ_PTR(_25403);
    _p_47966 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_47966)){
        _p_47966 = (long)DBL_PTR(_p_47966)->dbl;
    }
    _25403 = NOVALUE;

    /** 		while p != 0 do*/
L2: 
    if (_p_47966 == 0)
    goto L3; // [33] 269

    /** 			integer sym_file = SymTab[p][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25406 = (int)*(((s1_ptr)_2)->base + _p_47966);
    _2 = (int)SEQ_PTR(_25406);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _sym_file_47973 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _sym_file_47973 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_sym_file_47973)){
        _sym_file_47973 = (long)DBL_PTR(_sym_file_47973)->dbl;
    }
    _25406 = NOVALUE;

    /** 			just_mark_everything_from = p*/
    _55just_mark_everything_from_47960 = _p_47966;

    /** 			if sym_file = current_file_no or find( sym_file, recheck_files ) then*/
    _25408 = (_sym_file_47973 == _38current_file_no_16946);
    if (_25408 != 0) {
        goto L4; // [68] 84
    }
    _25410 = find_from(_sym_file_47973, _55recheck_files_48033, 1);
    if (_25410 == 0)
    {
        _25410 = NOVALUE;
        goto L5; // [80] 108
    }
    else{
        _25410 = NOVALUE;
    }
L4: 

    /** 				SymTab[p][attribute] += 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_p_47966 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25413 = (int)*(((s1_ptr)_2)->base + _attribute_47963);
    _25411 = NOVALUE;
    if (IS_ATOM_INT(_25413)) {
        _25414 = _25413 + 1;
        if (_25414 > MAXINT){
            _25414 = NewDouble((double)_25414);
        }
    }
    else
    _25414 = binary_op(PLUS, 1, _25413);
    _25413 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _attribute_47963);
    _1 = *(int *)_2;
    *(int *)_2 = _25414;
    if( _1 != _25414 ){
        DeRef(_1);
    }
    _25414 = NOVALUE;
    _25411 = NOVALUE;
    goto L6; // [105] 246
L5: 

    /** 				integer scope = SymTab[p][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25415 = (int)*(((s1_ptr)_2)->base + _p_47966);
    _2 = (int)SEQ_PTR(_25415);
    _scope_47990 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_47990)){
        _scope_47990 = (long)DBL_PTR(_scope_47990)->dbl;
    }
    _25415 = NOVALUE;

    /** 				switch scope with fallthru do*/
    _0 = _scope_47990;
    switch ( _0 ){ 

        /** 					case SC_PUBLIC then*/
        case 13:

        /** 						if and_bits( DIRECT_OR_PUBLIC_INCLUDE, include_matrix[current_file_no][sym_file] ) then*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _25419 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
        _2 = (int)SEQ_PTR(_25419);
        _25420 = (int)*(((s1_ptr)_2)->base + _sym_file_47973);
        _25419 = NOVALUE;
        if (IS_ATOM_INT(_25420)) {
            {unsigned long tu;
                 tu = (unsigned long)6 & (unsigned long)_25420;
                 _25421 = MAKE_UINT(tu);
            }
        }
        else {
            _25421 = binary_op(AND_BITS, 6, _25420);
        }
        _25420 = NOVALUE;
        if (_25421 == 0) {
            DeRef(_25421);
            _25421 = NOVALUE;
            goto L7; // [155] 243
        }
        else {
            if (!IS_ATOM_INT(_25421) && DBL_PTR(_25421)->dbl == 0.0){
                DeRef(_25421);
                _25421 = NOVALUE;
                goto L7; // [155] 243
            }
            DeRef(_25421);
            _25421 = NOVALUE;
        }
        DeRef(_25421);
        _25421 = NOVALUE;

        /** 							SymTab[p][attribute] += 1*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_47966 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _25424 = (int)*(((s1_ptr)_2)->base + _attribute_47963);
        _25422 = NOVALUE;
        if (IS_ATOM_INT(_25424)) {
            _25425 = _25424 + 1;
            if (_25425 > MAXINT){
                _25425 = NewDouble((double)_25425);
            }
        }
        else
        _25425 = binary_op(PLUS, 1, _25424);
        _25424 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _attribute_47963);
        _1 = *(int *)_2;
        *(int *)_2 = _25425;
        if( _1 != _25425 ){
            DeRef(_1);
        }
        _25425 = NOVALUE;
        _25422 = NOVALUE;

        /** 						break*/
        goto L7; // [182] 243

        /** 					case SC_EXPORT then*/
        case 11:

        /** 						if not and_bits( DIRECT_INCLUDE, include_matrix[current_file_no][sym_file] ) then*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _25426 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
        _2 = (int)SEQ_PTR(_25426);
        _25427 = (int)*(((s1_ptr)_2)->base + _sym_file_47973);
        _25426 = NOVALUE;
        if (IS_ATOM_INT(_25427)) {
            {unsigned long tu;
                 tu = (unsigned long)2 & (unsigned long)_25427;
                 _25428 = MAKE_UINT(tu);
            }
        }
        else {
            _25428 = binary_op(AND_BITS, 2, _25427);
        }
        _25427 = NOVALUE;
        if (IS_ATOM_INT(_25428)) {
            if (_25428 != 0){
                DeRef(_25428);
                _25428 = NOVALUE;
                goto L8; // [208] 216
            }
        }
        else {
            if (DBL_PTR(_25428)->dbl != 0.0){
                DeRef(_25428);
                _25428 = NOVALUE;
                goto L8; // [208] 216
            }
        }
        DeRef(_25428);
        _25428 = NOVALUE;

        /** 							break*/
        goto L9; // [213] 217
L8: 
L9: 

        /** 					case SC_GLOBAL then*/
        case 6:

        /** 						SymTab[p][attribute] += 1*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_47966 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _25432 = (int)*(((s1_ptr)_2)->base + _attribute_47963);
        _25430 = NOVALUE;
        if (IS_ATOM_INT(_25432)) {
            _25433 = _25432 + 1;
            if (_25433 > MAXINT){
                _25433 = NewDouble((double)_25433);
            }
        }
        else
        _25433 = binary_op(PLUS, 1, _25432);
        _25432 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _attribute_47963);
        _1 = *(int *)_2;
        *(int *)_2 = _25433;
        if( _1 != _25433 ){
            DeRef(_1);
        }
        _25433 = NOVALUE;
        _25430 = NOVALUE;
    ;}L7: 
L6: 

    /** 			p = SymTab[p][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25434 = (int)*(((s1_ptr)_2)->base + _p_47966);
    _2 = (int)SEQ_PTR(_25434);
    _p_47966 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_47966)){
        _p_47966 = (long)DBL_PTR(_p_47966)->dbl;
    }
    _25434 = NOVALUE;

    /** 		end while*/
    goto L2; // [266] 33
L3: 
L1: 

    /** end procedure*/
    DeRef(_25408);
    _25408 = NOVALUE;
    return;
    ;
}


void _55mark_final_targets()
{
    int _marked_48048 = NOVALUE;
    int _25440 = NOVALUE;
    int _25438 = NOVALUE;
    int _25437 = NOVALUE;
    int _25436 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if just_mark_everything_from then*/
    if (_55just_mark_everything_from_47960 == 0)
    {
        goto L1; // [5] 44
    }
    else{
    }

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2; // [12] 25
    }
    else{
    }

    /** 			mark_all( S_RI_TARGET )*/
    _55mark_all(53);
    goto L3; // [22] 152
L2: 

    /** 		elsif BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L3; // [29] 152
    }
    else{
    }

    /** 			mark_all( S_NREFS )*/
    _55mark_all(12);
    goto L3; // [41] 152
L1: 

    /** 	elsif length( recheck_targets ) then*/
    if (IS_SEQUENCE(_55recheck_targets_48032)){
            _25436 = SEQ_PTR(_55recheck_targets_48032)->length;
    }
    else {
        _25436 = 1;
    }
    if (_25436 == 0)
    {
        _25436 = NOVALUE;
        goto L4; // [51] 151
    }
    else{
        _25436 = NOVALUE;
    }

    /** 		for i = length( recheck_targets ) to 1 by -1 do*/
    if (IS_SEQUENCE(_55recheck_targets_48032)){
            _25437 = SEQ_PTR(_55recheck_targets_48032)->length;
    }
    else {
        _25437 = 1;
    }
    {
        int _i_48046;
        _i_48046 = _25437;
L5: 
        if (_i_48046 < 1){
            goto L6; // [61] 150
        }

        /** 			integer marked = 0*/
        _marked_48048 = 0;

        /** 			if TRANSLATE then*/
        if (_38TRANSLATE_16564 == 0)
        {
            goto L7; // [77] 100
        }
        else{
        }

        /** 				marked = MarkTargets( recheck_targets[i], S_RI_TARGET )*/
        _2 = (int)SEQ_PTR(_55recheck_targets_48032);
        _25438 = (int)*(((s1_ptr)_2)->base + _i_48046);
        Ref(_25438);
        _marked_48048 = _55MarkTargets(_25438, 53);
        _25438 = NOVALUE;
        if (!IS_ATOM_INT(_marked_48048)) {
            _1 = (long)(DBL_PTR(_marked_48048)->dbl);
            if (UNIQUE(DBL_PTR(_marked_48048)) && (DBL_PTR(_marked_48048)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_marked_48048);
            _marked_48048 = _1;
        }
        goto L8; // [97] 126
L7: 

        /** 			elsif BIND then*/
        if (_38BIND_16567 == 0)
        {
            goto L9; // [104] 125
        }
        else{
        }

        /** 				marked = MarkTargets( recheck_targets[i], S_NREFS )*/
        _2 = (int)SEQ_PTR(_55recheck_targets_48032);
        _25440 = (int)*(((s1_ptr)_2)->base + _i_48046);
        Ref(_25440);
        _marked_48048 = _55MarkTargets(_25440, 12);
        _25440 = NOVALUE;
        if (!IS_ATOM_INT(_marked_48048)) {
            _1 = (long)(DBL_PTR(_marked_48048)->dbl);
            if (UNIQUE(DBL_PTR(_marked_48048)) && (DBL_PTR(_marked_48048)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_marked_48048);
            _marked_48048 = _1;
        }
L9: 
L8: 

        /** 			if marked then*/
        if (_marked_48048 == 0)
        {
            goto LA; // [128] 141
        }
        else{
        }

        /** 				recheck_targets = remove( recheck_targets, i )*/
        {
            s1_ptr assign_space = SEQ_PTR(_55recheck_targets_48032);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_48046)) ? _i_48046 : (long)(DBL_PTR(_i_48046)->dbl);
            int stop = (IS_ATOM_INT(_i_48046)) ? _i_48046 : (long)(DBL_PTR(_i_48046)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_55recheck_targets_48032), start, &_55recheck_targets_48032 );
                }
                else Tail(SEQ_PTR(_55recheck_targets_48032), stop+1, &_55recheck_targets_48032);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_55recheck_targets_48032), start, &_55recheck_targets_48032);
            }
            else {
                assign_slice_seq = &assign_space;
                _55recheck_targets_48032 = Remove_elements(start, stop, (SEQ_PTR(_55recheck_targets_48032)->ref == 1));
            }
        }
LA: 

        /** 		end for*/
        _i_48046 = _i_48046 + -1;
        goto L5; // [145] 68
L6: 
        ;
    }
L4: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _55is_routine(int _sym_48066)
{
    int _tok_48067 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer tok = sym_token( sym )*/
    _tok_48067 = _55sym_token(_sym_48066);
    if (!IS_ATOM_INT(_tok_48067)) {
        _1 = (long)(DBL_PTR(_tok_48067)->dbl);
        if (UNIQUE(DBL_PTR(_tok_48067)) && (DBL_PTR(_tok_48067)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tok_48067);
        _tok_48067 = _1;
    }

    /** 	switch tok do*/
    _0 = _tok_48067;
    switch ( _0 ){ 

        /** 		case FUNC, PROC, TYPE then*/
        case 501:
        case 27:
        case 504:

        /** 			return 1*/
        return 1;
        goto L1; // [32] 45

        /** 		case else*/
        default:

        /** 			return 0*/
        return 0;
    ;}L1: 
    ;
}


int _55is_visible(int _sym_48080, int _from_file_48081)
{
    int _scope_48082 = NOVALUE;
    int _sym_file_48085 = NOVALUE;
    int _visible_mask_48090 = NOVALUE;
    int _25454 = NOVALUE;
    int _25453 = NOVALUE;
    int _25452 = NOVALUE;
    int _25451 = NOVALUE;
    int _25447 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer scope = sym_scope( sym )*/
    _scope_48082 = _55sym_scope(_sym_48080);
    if (!IS_ATOM_INT(_scope_48082)) {
        _1 = (long)(DBL_PTR(_scope_48082)->dbl);
        if (UNIQUE(DBL_PTR(_scope_48082)) && (DBL_PTR(_scope_48082)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_48082);
        _scope_48082 = _1;
    }

    /** 	integer sym_file = SymTab[sym][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25447 = (int)*(((s1_ptr)_2)->base + _sym_48080);
    _2 = (int)SEQ_PTR(_25447);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _sym_file_48085 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _sym_file_48085 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_sym_file_48085)){
        _sym_file_48085 = (long)DBL_PTR(_sym_file_48085)->dbl;
    }
    _25447 = NOVALUE;

    /** 	switch scope do*/
    _0 = _scope_48082;
    switch ( _0 ){ 

        /** 		case SC_PUBLIC then*/
        case 13:

        /** 			visible_mask = DIRECT_OR_PUBLIC_INCLUDE*/
        _visible_mask_48090 = 6;
        goto L1; // [49] 93

        /** 		case SC_EXPORT then*/
        case 11:

        /** 			visible_mask = DIRECT_INCLUDE*/
        _visible_mask_48090 = 2;
        goto L1; // [64] 93

        /** 		case SC_GLOBAL then*/
        case 6:

        /** 			return 1*/
        return 1;
        goto L1; // [76] 93

        /** 		case else*/
        default:

        /** 			return from_file = sym_file*/
        _25451 = (_from_file_48081 == _sym_file_48085);
        return _25451;
    ;}L1: 

    /** 	return and_bits( visible_mask, include_matrix[from_file][sym_file] )*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _25452 = (int)*(((s1_ptr)_2)->base + _from_file_48081);
    _2 = (int)SEQ_PTR(_25452);
    _25453 = (int)*(((s1_ptr)_2)->base + _sym_file_48085);
    _25452 = NOVALUE;
    if (IS_ATOM_INT(_25453)) {
        {unsigned long tu;
             tu = (unsigned long)_visible_mask_48090 & (unsigned long)_25453;
             _25454 = MAKE_UINT(tu);
        }
    }
    else {
        _25454 = binary_op(AND_BITS, _visible_mask_48090, _25453);
    }
    _25453 = NOVALUE;
    DeRef(_25451);
    _25451 = NOVALUE;
    return _25454;
    ;
}


int _55MarkTargets(int _s_48110, int _attribute_48111)
{
    int _p_48113 = NOVALUE;
    int _sname_48114 = NOVALUE;
    int _string_48115 = NOVALUE;
    int _colon_48116 = NOVALUE;
    int _h_48117 = NOVALUE;
    int _scope_48118 = NOVALUE;
    int _found_48139 = NOVALUE;
    int _25506 = NOVALUE;
    int _25502 = NOVALUE;
    int _25500 = NOVALUE;
    int _25499 = NOVALUE;
    int _25498 = NOVALUE;
    int _25497 = NOVALUE;
    int _25495 = NOVALUE;
    int _25494 = NOVALUE;
    int _25493 = NOVALUE;
    int _25492 = NOVALUE;
    int _25491 = NOVALUE;
    int _25489 = NOVALUE;
    int _25488 = NOVALUE;
    int _25487 = NOVALUE;
    int _25485 = NOVALUE;
    int _25483 = NOVALUE;
    int _25481 = NOVALUE;
    int _25480 = NOVALUE;
    int _25479 = NOVALUE;
    int _25478 = NOVALUE;
    int _25476 = NOVALUE;
    int _25475 = NOVALUE;
    int _25474 = NOVALUE;
    int _25473 = NOVALUE;
    int _25471 = NOVALUE;
    int _25470 = NOVALUE;
    int _25466 = NOVALUE;
    int _25465 = NOVALUE;
    int _25464 = NOVALUE;
    int _25463 = NOVALUE;
    int _25462 = NOVALUE;
    int _25461 = NOVALUE;
    int _25460 = NOVALUE;
    int _25459 = NOVALUE;
    int _25458 = NOVALUE;
    int _25457 = NOVALUE;
    int _25456 = NOVALUE;
    int _25455 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_48110)) {
        _1 = (long)(DBL_PTR(_s_48110)->dbl);
        if (UNIQUE(DBL_PTR(_s_48110)) && (DBL_PTR(_s_48110)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48110);
        _s_48110 = _1;
    }
    if (!IS_ATOM_INT(_attribute_48111)) {
        _1 = (long)(DBL_PTR(_attribute_48111)->dbl);
        if (UNIQUE(DBL_PTR(_attribute_48111)) && (DBL_PTR(_attribute_48111)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_attribute_48111);
        _attribute_48111 = _1;
    }

    /** 	sequence sname*/

    /** 	sequence string*/

    /** 	integer colon, h*/

    /** 	integer scope*/

    /** 	if (SymTab[s][S_MODE] = M_TEMP or*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25455 = (int)*(((s1_ptr)_2)->base + _s_48110);
    _2 = (int)SEQ_PTR(_25455);
    _25456 = (int)*(((s1_ptr)_2)->base + 3);
    _25455 = NOVALUE;
    if (IS_ATOM_INT(_25456)) {
        _25457 = (_25456 == 3);
    }
    else {
        _25457 = binary_op(EQUALS, _25456, 3);
    }
    _25456 = NOVALUE;
    if (IS_ATOM_INT(_25457)) {
        if (_25457 != 0) {
            _25458 = 1;
            goto L1; // [33] 59
        }
    }
    else {
        if (DBL_PTR(_25457)->dbl != 0.0) {
            _25458 = 1;
            goto L1; // [33] 59
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25459 = (int)*(((s1_ptr)_2)->base + _s_48110);
    _2 = (int)SEQ_PTR(_25459);
    _25460 = (int)*(((s1_ptr)_2)->base + 3);
    _25459 = NOVALUE;
    if (IS_ATOM_INT(_25460)) {
        _25461 = (_25460 == 2);
    }
    else {
        _25461 = binary_op(EQUALS, _25460, 2);
    }
    _25460 = NOVALUE;
    DeRef(_25458);
    if (IS_ATOM_INT(_25461))
    _25458 = (_25461 != 0);
    else
    _25458 = DBL_PTR(_25461)->dbl != 0.0;
L1: 
    if (_25458 == 0) {
        goto L2; // [59] 440
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25463 = (int)*(((s1_ptr)_2)->base + _s_48110);
    _2 = (int)SEQ_PTR(_25463);
    _25464 = (int)*(((s1_ptr)_2)->base + 1);
    _25463 = NOVALUE;
    _25465 = IS_SEQUENCE(_25464);
    _25464 = NOVALUE;
    if (_25465 == 0)
    {
        _25465 = NOVALUE;
        goto L2; // [79] 440
    }
    else{
        _25465 = NOVALUE;
    }

    /** 		integer found = 0*/
    _found_48139 = 0;

    /** 		string = SymTab[s][S_OBJ]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25466 = (int)*(((s1_ptr)_2)->base + _s_48110);
    DeRef(_string_48115);
    _2 = (int)SEQ_PTR(_25466);
    _string_48115 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_string_48115);
    _25466 = NOVALUE;

    /** 		colon = find(':', string)*/
    _colon_48116 = find_from(58, _string_48115, 1);

    /** 		if colon = 0 then*/
    if (_colon_48116 != 0)
    goto L3; // [112] 126

    /** 			sname = string*/
    RefDS(_string_48115);
    DeRef(_sname_48114);
    _sname_48114 = _string_48115;
    goto L4; // [123] 200
L3: 

    /** 			sname = string[colon+1..$]  -- ignore namespace part*/
    _25470 = _colon_48116 + 1;
    if (_25470 > MAXINT){
        _25470 = NewDouble((double)_25470);
    }
    if (IS_SEQUENCE(_string_48115)){
            _25471 = SEQ_PTR(_string_48115)->length;
    }
    else {
        _25471 = 1;
    }
    rhs_slice_target = (object_ptr)&_sname_48114;
    RHS_Slice(_string_48115, _25470, _25471);

    /** 			while length(sname) and sname[1] = ' ' or sname[1] = '\t' do*/
L5: 
    if (IS_SEQUENCE(_sname_48114)){
            _25473 = SEQ_PTR(_sname_48114)->length;
    }
    else {
        _25473 = 1;
    }
    if (_25473 == 0) {
        _25474 = 0;
        goto L6; // [148] 164
    }
    _2 = (int)SEQ_PTR(_sname_48114);
    _25475 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_25475)) {
        _25476 = (_25475 == 32);
    }
    else {
        _25476 = binary_op(EQUALS, _25475, 32);
    }
    _25475 = NOVALUE;
    if (IS_ATOM_INT(_25476))
    _25474 = (_25476 != 0);
    else
    _25474 = DBL_PTR(_25476)->dbl != 0.0;
L6: 
    if (_25474 != 0) {
        goto L7; // [164] 181
    }
    _2 = (int)SEQ_PTR(_sname_48114);
    _25478 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_25478)) {
        _25479 = (_25478 == 9);
    }
    else {
        _25479 = binary_op(EQUALS, _25478, 9);
    }
    _25478 = NOVALUE;
    if (_25479 <= 0) {
        if (_25479 == 0) {
            DeRef(_25479);
            _25479 = NOVALUE;
            goto L8; // [177] 199
        }
        else {
            if (!IS_ATOM_INT(_25479) && DBL_PTR(_25479)->dbl == 0.0){
                DeRef(_25479);
                _25479 = NOVALUE;
                goto L8; // [177] 199
            }
            DeRef(_25479);
            _25479 = NOVALUE;
        }
    }
    DeRef(_25479);
    _25479 = NOVALUE;
L7: 

    /** 				sname = tail( sname, length( sname ) -1 )*/
    if (IS_SEQUENCE(_sname_48114)){
            _25480 = SEQ_PTR(_sname_48114)->length;
    }
    else {
        _25480 = 1;
    }
    _25481 = _25480 - 1;
    _25480 = NOVALUE;
    {
        int len = SEQ_PTR(_sname_48114)->length;
        int size = (IS_ATOM_INT(_25481)) ? _25481 : (long)(DBL_PTR(_25481)->dbl);
        if (size <= 0) {
            DeRef(_sname_48114);
            _sname_48114 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_sname_48114);
            DeRef(_sname_48114);
            _sname_48114 = _sname_48114;
        }
        else Tail(SEQ_PTR(_sname_48114), len-size+1, &_sname_48114);
    }
    _25481 = NOVALUE;

    /** 			end while*/
    goto L5; // [196] 145
L8: 
L4: 

    /** 		if length(sname) = 0 then*/
    if (IS_SEQUENCE(_sname_48114)){
            _25483 = SEQ_PTR(_sname_48114)->length;
    }
    else {
        _25483 = 1;
    }
    if (_25483 != 0)
    goto L9; // [207] 218

    /** 			return 1*/
    DeRefDS(_sname_48114);
    DeRef(_string_48115);
    DeRef(_25470);
    _25470 = NOVALUE;
    DeRef(_25457);
    _25457 = NOVALUE;
    DeRef(_25461);
    _25461 = NOVALUE;
    DeRef(_25476);
    _25476 = NOVALUE;
    return 1;
L9: 

    /** 		h = buckets[hashfn(sname)]*/
    RefDS(_sname_48114);
    _25485 = _55hashfn(_sname_48114);
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_25485)){
        _h_48117 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25485)->dbl));
    }
    else{
        _h_48117 = (int)*(((s1_ptr)_2)->base + _25485);
    }
    if (!IS_ATOM_INT(_h_48117))
    _h_48117 = (long)DBL_PTR(_h_48117)->dbl;

    /** 		while h do*/
LA: 
    if (_h_48117 == 0)
    {
        goto LB; // [235] 381
    }
    else{
    }

    /** 			if equal(sname, SymTab[h][S_NAME]) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25487 = (int)*(((s1_ptr)_2)->base + _h_48117);
    _2 = (int)SEQ_PTR(_25487);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25488 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25488 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25487 = NOVALUE;
    if (_sname_48114 == _25488)
    _25489 = 1;
    else if (IS_ATOM_INT(_sname_48114) && IS_ATOM_INT(_25488))
    _25489 = 0;
    else
    _25489 = (compare(_sname_48114, _25488) == 0);
    _25488 = NOVALUE;
    if (_25489 == 0)
    {
        _25489 = NOVALUE;
        goto LC; // [256] 360
    }
    else{
        _25489 = NOVALUE;
    }

    /** 				if attribute = S_NREFS then*/
    if (_attribute_48111 != 12)
    goto LD; // [263] 289

    /** 					if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto LE; // [271] 359
    }
    else{
    }

    /** 						add_ref({PROC, h})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 27;
    ((int *)_2)[2] = _h_48117;
    _25491 = MAKE_SEQ(_1);
    _55add_ref(_25491);
    _25491 = NOVALUE;
    goto LE; // [286] 359
LD: 

    /** 				elsif is_routine( h ) and is_visible( h, current_file_no ) then*/
    _25492 = _55is_routine(_h_48117);
    if (IS_ATOM_INT(_25492)) {
        if (_25492 == 0) {
            goto LF; // [295] 358
        }
    }
    else {
        if (DBL_PTR(_25492)->dbl == 0.0) {
            goto LF; // [295] 358
        }
    }
    _25494 = _55is_visible(_h_48117, _38current_file_no_16946);
    if (_25494 == 0) {
        DeRef(_25494);
        _25494 = NOVALUE;
        goto LF; // [307] 358
    }
    else {
        if (!IS_ATOM_INT(_25494) && DBL_PTR(_25494)->dbl == 0.0){
            DeRef(_25494);
            _25494 = NOVALUE;
            goto LF; // [307] 358
        }
        DeRef(_25494);
        _25494 = NOVALUE;
    }
    DeRef(_25494);
    _25494 = NOVALUE;

    /** 					SymTab[h][attribute] += 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_h_48117 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _25497 = (int)*(((s1_ptr)_2)->base + _attribute_48111);
    _25495 = NOVALUE;
    if (IS_ATOM_INT(_25497)) {
        _25498 = _25497 + 1;
        if (_25498 > MAXINT){
            _25498 = NewDouble((double)_25498);
        }
    }
    else
    _25498 = binary_op(PLUS, 1, _25497);
    _25497 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _attribute_48111);
    _1 = *(int *)_2;
    *(int *)_2 = _25498;
    if( _1 != _25498 ){
        DeRef(_1);
    }
    _25498 = NOVALUE;
    _25495 = NOVALUE;

    /** 					if current_file_no = SymTab[h][S_FILE_NO] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25499 = (int)*(((s1_ptr)_2)->base + _h_48117);
    _2 = (int)SEQ_PTR(_25499);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25500 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25500 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25499 = NOVALUE;
    if (binary_op_a(NOTEQ, _38current_file_no_16946, _25500)){
        _25500 = NOVALUE;
        goto L10; // [347] 357
    }
    _25500 = NOVALUE;

    /** 						found = 1*/
    _found_48139 = 1;
L10: 
LF: 
LE: 
LC: 

    /** 			h = SymTab[h][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25502 = (int)*(((s1_ptr)_2)->base + _h_48117);
    _2 = (int)SEQ_PTR(_25502);
    _h_48117 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_h_48117)){
        _h_48117 = (long)DBL_PTR(_h_48117)->dbl;
    }
    _25502 = NOVALUE;

    /** 		end while*/
    goto LA; // [378] 235
LB: 

    /** 		if not found then*/
    if (_found_48139 != 0)
    goto L11; // [383] 429

    /** 			just_mark_everything_from = TopLevelSub*/
    _55just_mark_everything_from_47960 = _38TopLevelSub_16953;

    /** 			recheck_targets &= s*/
    Append(&_55recheck_targets_48032, _55recheck_targets_48032, _s_48110);

    /** 			if not find( current_file_no, recheck_files ) then*/
    _25506 = find_from(_38current_file_no_16946, _55recheck_files_48033, 1);
    if (_25506 != 0)
    goto L12; // [414] 428
    _25506 = NOVALUE;

    /** 				recheck_files &= current_file_no*/
    Append(&_55recheck_files_48033, _55recheck_files_48033, _38current_file_no_16946);
L12: 
L11: 

    /** 		return found*/
    DeRef(_sname_48114);
    DeRef(_string_48115);
    DeRef(_25470);
    _25470 = NOVALUE;
    DeRef(_25457);
    _25457 = NOVALUE;
    DeRef(_25461);
    _25461 = NOVALUE;
    DeRef(_25485);
    _25485 = NOVALUE;
    DeRef(_25476);
    _25476 = NOVALUE;
    DeRef(_25492);
    _25492 = NOVALUE;
    return _found_48139;
    goto L13; // [437] 469
L2: 

    /** 		if not just_mark_everything_from then*/
    if (_55just_mark_everything_from_47960 != 0)
    goto L14; // [444] 457

    /** 			just_mark_everything_from = TopLevelSub*/
    _55just_mark_everything_from_47960 = _38TopLevelSub_16953;
L14: 

    /** 		mark_all( attribute )*/
    _55mark_all(_attribute_48111);

    /** 		return 1*/
    DeRef(_sname_48114);
    DeRef(_string_48115);
    DeRef(_25470);
    _25470 = NOVALUE;
    DeRef(_25457);
    _25457 = NOVALUE;
    DeRef(_25461);
    _25461 = NOVALUE;
    DeRef(_25485);
    _25485 = NOVALUE;
    DeRef(_25476);
    _25476 = NOVALUE;
    DeRef(_25492);
    _25492 = NOVALUE;
    return 1;
L13: 
    ;
}


void _55resolve_unincluded_globals(int _ok_48224)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ok_48224)) {
        _1 = (long)(DBL_PTR(_ok_48224)->dbl);
        if (UNIQUE(DBL_PTR(_ok_48224)) && (DBL_PTR(_ok_48224)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ok_48224);
        _ok_48224 = _1;
    }

    /** 	Resolve_unincluded_globals = ok*/
    _55Resolve_unincluded_globals_48221 = _ok_48224;

    /** end procedure*/
    return;
    ;
}


int _55get_resolve_unincluded_globals()
{
    int _0, _1, _2;
    

    /** 	return Resolve_unincluded_globals*/
    return _55Resolve_unincluded_globals_48221;
    ;
}


int _55keyfind(int _word_48230, int _file_no_48231, int _scanning_file_48232, int _namespace_ok_48235, int _hashval_48236)
{
    int _msg_48238 = NOVALUE;
    int _b_name_48239 = NOVALUE;
    int _scope_48240 = NOVALUE;
    int _defined_48241 = NOVALUE;
    int _ix_48242 = NOVALUE;
    int _st_ptr_48244 = NOVALUE;
    int _st_builtin_48245 = NOVALUE;
    int _tok_48247 = NOVALUE;
    int _gtok_48248 = NOVALUE;
    int _any_symbol_48251 = NOVALUE;
    int _tok_file_48419 = NOVALUE;
    int _good_48426 = NOVALUE;
    int _include_type_48436 = NOVALUE;
    int _msg_file_48492 = NOVALUE;
    int _25701 = NOVALUE;
    int _25700 = NOVALUE;
    int _25698 = NOVALUE;
    int _25696 = NOVALUE;
    int _25695 = NOVALUE;
    int _25694 = NOVALUE;
    int _25693 = NOVALUE;
    int _25692 = NOVALUE;
    int _25690 = NOVALUE;
    int _25688 = NOVALUE;
    int _25687 = NOVALUE;
    int _25686 = NOVALUE;
    int _25685 = NOVALUE;
    int _25684 = NOVALUE;
    int _25683 = NOVALUE;
    int _25682 = NOVALUE;
    int _25681 = NOVALUE;
    int _25679 = NOVALUE;
    int _25678 = NOVALUE;
    int _25677 = NOVALUE;
    int _25676 = NOVALUE;
    int _25675 = NOVALUE;
    int _25674 = NOVALUE;
    int _25673 = NOVALUE;
    int _25672 = NOVALUE;
    int _25671 = NOVALUE;
    int _25670 = NOVALUE;
    int _25669 = NOVALUE;
    int _25668 = NOVALUE;
    int _25667 = NOVALUE;
    int _25666 = NOVALUE;
    int _25665 = NOVALUE;
    int _25664 = NOVALUE;
    int _25663 = NOVALUE;
    int _25661 = NOVALUE;
    int _25660 = NOVALUE;
    int _25657 = NOVALUE;
    int _25653 = NOVALUE;
    int _25651 = NOVALUE;
    int _25650 = NOVALUE;
    int _25649 = NOVALUE;
    int _25648 = NOVALUE;
    int _25647 = NOVALUE;
    int _25645 = NOVALUE;
    int _25644 = NOVALUE;
    int _25643 = NOVALUE;
    int _25642 = NOVALUE;
    int _25640 = NOVALUE;
    int _25637 = NOVALUE;
    int _25636 = NOVALUE;
    int _25635 = NOVALUE;
    int _25634 = NOVALUE;
    int _25632 = NOVALUE;
    int _25629 = NOVALUE;
    int _25628 = NOVALUE;
    int _25627 = NOVALUE;
    int _25626 = NOVALUE;
    int _25625 = NOVALUE;
    int _25624 = NOVALUE;
    int _25623 = NOVALUE;
    int _25620 = NOVALUE;
    int _25619 = NOVALUE;
    int _25617 = NOVALUE;
    int _25615 = NOVALUE;
    int _25613 = NOVALUE;
    int _25612 = NOVALUE;
    int _25611 = NOVALUE;
    int _25607 = NOVALUE;
    int _25606 = NOVALUE;
    int _25601 = NOVALUE;
    int _25599 = NOVALUE;
    int _25597 = NOVALUE;
    int _25596 = NOVALUE;
    int _25592 = NOVALUE;
    int _25591 = NOVALUE;
    int _25589 = NOVALUE;
    int _25588 = NOVALUE;
    int _25586 = NOVALUE;
    int _25585 = NOVALUE;
    int _25584 = NOVALUE;
    int _25583 = NOVALUE;
    int _25582 = NOVALUE;
    int _25580 = NOVALUE;
    int _25579 = NOVALUE;
    int _25578 = NOVALUE;
    int _25577 = NOVALUE;
    int _25576 = NOVALUE;
    int _25575 = NOVALUE;
    int _25574 = NOVALUE;
    int _25573 = NOVALUE;
    int _25572 = NOVALUE;
    int _25571 = NOVALUE;
    int _25570 = NOVALUE;
    int _25569 = NOVALUE;
    int _25568 = NOVALUE;
    int _25567 = NOVALUE;
    int _25566 = NOVALUE;
    int _25565 = NOVALUE;
    int _25564 = NOVALUE;
    int _25563 = NOVALUE;
    int _25562 = NOVALUE;
    int _25561 = NOVALUE;
    int _25560 = NOVALUE;
    int _25559 = NOVALUE;
    int _25557 = NOVALUE;
    int _25556 = NOVALUE;
    int _25554 = NOVALUE;
    int _25553 = NOVALUE;
    int _25552 = NOVALUE;
    int _25551 = NOVALUE;
    int _25550 = NOVALUE;
    int _25548 = NOVALUE;
    int _25547 = NOVALUE;
    int _25546 = NOVALUE;
    int _25544 = NOVALUE;
    int _25543 = NOVALUE;
    int _25542 = NOVALUE;
    int _25541 = NOVALUE;
    int _25540 = NOVALUE;
    int _25539 = NOVALUE;
    int _25538 = NOVALUE;
    int _25536 = NOVALUE;
    int _25535 = NOVALUE;
    int _25530 = NOVALUE;
    int _25527 = NOVALUE;
    int _25526 = NOVALUE;
    int _25525 = NOVALUE;
    int _25524 = NOVALUE;
    int _25523 = NOVALUE;
    int _25522 = NOVALUE;
    int _25521 = NOVALUE;
    int _25520 = NOVALUE;
    int _25519 = NOVALUE;
    int _25518 = NOVALUE;
    int _25517 = NOVALUE;
    int _25516 = NOVALUE;
    int _25515 = NOVALUE;
    int _25514 = NOVALUE;
    int _25513 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_file_no_48231)) {
        _1 = (long)(DBL_PTR(_file_no_48231)->dbl);
        if (UNIQUE(DBL_PTR(_file_no_48231)) && (DBL_PTR(_file_no_48231)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_no_48231);
        _file_no_48231 = _1;
    }
    if (!IS_ATOM_INT(_scanning_file_48232)) {
        _1 = (long)(DBL_PTR(_scanning_file_48232)->dbl);
        if (UNIQUE(DBL_PTR(_scanning_file_48232)) && (DBL_PTR(_scanning_file_48232)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scanning_file_48232);
        _scanning_file_48232 = _1;
    }
    if (!IS_ATOM_INT(_namespace_ok_48235)) {
        _1 = (long)(DBL_PTR(_namespace_ok_48235)->dbl);
        if (UNIQUE(DBL_PTR(_namespace_ok_48235)) && (DBL_PTR(_namespace_ok_48235)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_namespace_ok_48235);
        _namespace_ok_48235 = _1;
    }
    if (!IS_ATOM_INT(_hashval_48236)) {
        _1 = (long)(DBL_PTR(_hashval_48236)->dbl);
        if (UNIQUE(DBL_PTR(_hashval_48236)) && (DBL_PTR(_hashval_48236)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hashval_48236);
        _hashval_48236 = _1;
    }

    /** 	dup_globals = {}*/
    RefDS(_22663);
    DeRef(_55dup_globals_48216);
    _55dup_globals_48216 = _22663;

    /** 	dup_overrides = {}*/
    RefDS(_22663);
    DeRefi(_55dup_overrides_48217);
    _55dup_overrides_48217 = _22663;

    /** 	in_include_path = {}*/
    RefDS(_22663);
    DeRef(_55in_include_path_48218);
    _55in_include_path_48218 = _22663;

    /** 	symbol_resolution_warning = ""*/
    RefDS(_22663);
    DeRef(_38symbol_resolution_warning_17065);
    _38symbol_resolution_warning_17065 = _22663;

    /** 	st_builtin = 0*/
    _st_builtin_48245 = 0;

    /** 	ifdef EUDIS then*/

    /** 	st_ptr = buckets[hashval]*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _st_ptr_48244 = (int)*(((s1_ptr)_2)->base + _hashval_48236);
    if (!IS_ATOM_INT(_st_ptr_48244)){
        _st_ptr_48244 = (long)DBL_PTR(_st_ptr_48244)->dbl;
    }

    /** 	integer any_symbol = namespace_ok = -1*/
    _any_symbol_48251 = (_namespace_ok_48235 == -1);

    /** 	while st_ptr do*/
L1: 
    if (_st_ptr_48244 == 0)
    {
        goto L2; // [69] 1033
    }
    else{
    }

    /** 		if SymTab[st_ptr][S_SCOPE] != SC_UNDEFINED */
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25513 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25513);
    _25514 = (int)*(((s1_ptr)_2)->base + 4);
    _25513 = NOVALUE;
    if (IS_ATOM_INT(_25514)) {
        _25515 = (_25514 != 9);
    }
    else {
        _25515 = binary_op(NOTEQ, _25514, 9);
    }
    _25514 = NOVALUE;
    if (IS_ATOM_INT(_25515)) {
        if (_25515 == 0) {
            DeRef(_25516);
            _25516 = 0;
            goto L3; // [92] 116
        }
    }
    else {
        if (DBL_PTR(_25515)->dbl == 0.0) {
            DeRef(_25516);
            _25516 = 0;
            goto L3; // [92] 116
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25517 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25517);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25518 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25518 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25517 = NOVALUE;
    if (_word_48230 == _25518)
    _25519 = 1;
    else if (IS_ATOM_INT(_word_48230) && IS_ATOM_INT(_25518))
    _25519 = 0;
    else
    _25519 = (compare(_word_48230, _25518) == 0);
    _25518 = NOVALUE;
    DeRef(_25516);
    _25516 = (_25519 != 0);
L3: 
    if (_25516 == 0) {
        goto L4; // [116] 1012
    }
    if (_any_symbol_48251 != 0) {
        DeRef(_25521);
        _25521 = 1;
        goto L5; // [120] 150
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25522 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25522);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _25523 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _25523 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _25522 = NOVALUE;
    if (IS_ATOM_INT(_25523)) {
        _25524 = (_25523 == 523);
    }
    else {
        _25524 = binary_op(EQUALS, _25523, 523);
    }
    _25523 = NOVALUE;
    if (IS_ATOM_INT(_25524)) {
        _25525 = (_namespace_ok_48235 == _25524);
    }
    else {
        _25525 = binary_op(EQUALS, _namespace_ok_48235, _25524);
    }
    DeRef(_25524);
    _25524 = NOVALUE;
    if (IS_ATOM_INT(_25525))
    _25521 = (_25525 != 0);
    else
    _25521 = DBL_PTR(_25525)->dbl != 0.0;
L5: 
    if (_25521 == 0)
    {
        _25521 = NOVALUE;
        goto L4; // [151] 1012
    }
    else{
        _25521 = NOVALUE;
    }

    /** 			tok = {SymTab[st_ptr][S_TOKEN], st_ptr}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25526 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25526);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _25527 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _25527 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _25526 = NOVALUE;
    Ref(_25527);
    DeRef(_tok_48247);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25527;
    ((int *)_2)[2] = _st_ptr_48244;
    _tok_48247 = MAKE_SEQ(_1);
    _25527 = NOVALUE;

    /** 			if file_no = -1 then*/
    if (_file_no_48231 != -1)
    goto L6; // [174] 714

    /** 				scope = SymTab[st_ptr][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25530 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25530);
    _scope_48240 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_48240)){
        _scope_48240 = (long)DBL_PTR(_scope_48240)->dbl;
    }
    _25530 = NOVALUE;

    /** 				switch scope with fallthru do*/
    _0 = _scope_48240;
    switch ( _0 ){ 

        /** 				case SC_OVERRIDE then*/
        case 12:

        /** 					dup_overrides &= st_ptr*/
        Append(&_55dup_overrides_48217, _55dup_overrides_48217, _st_ptr_48244);

        /** 					break*/
        goto L7; // [215] 1011

        /** 				case SC_PREDEF then*/
        case 7:

        /** 					st_builtin = st_ptr*/
        _st_builtin_48245 = _st_ptr_48244;

        /** 					break*/
        goto L7; // [230] 1011

        /** 				case SC_GLOBAL then*/
        case 6:

        /** 					if scanning_file = SymTab[st_ptr][S_FILE_NO] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25535 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25535);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25536 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25536 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25535 = NOVALUE;
        if (binary_op_a(NOTEQ, _scanning_file_48232, _25536)){
            _25536 = NOVALUE;
            goto L8; // [250] 274
        }
        _25536 = NOVALUE;

        /** 						if BIND then*/
        if (_38BIND_16567 == 0)
        {
            goto L9; // [258] 267
        }
        else{
        }

        /** 							add_ref(tok)*/
        Ref(_tok_48247);
        _55add_ref(_tok_48247);
L9: 

        /** 						return tok*/
        DeRefDS(_word_48230);
        DeRef(_msg_48238);
        DeRef(_b_name_48239);
        DeRef(_gtok_48248);
        DeRef(_25525);
        _25525 = NOVALUE;
        DeRef(_25515);
        _25515 = NOVALUE;
        return _tok_48247;
L8: 

        /** 					if Resolve_unincluded_globals */
        if (_55Resolve_unincluded_globals_48221 != 0) {
            _25538 = 1;
            goto LA; // [278] 322
        }
        _2 = (int)SEQ_PTR(_35finished_files_15598);
        _25539 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
        if (_25539 == 0) {
            _25540 = 0;
            goto LB; // [288] 318
        }
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _25541 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25542 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25542);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25543 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25543 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25542 = NOVALUE;
        _2 = (int)SEQ_PTR(_25541);
        if (!IS_ATOM_INT(_25543)){
            _25544 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25543)->dbl));
        }
        else{
            _25544 = (int)*(((s1_ptr)_2)->base + _25543);
        }
        _25541 = NOVALUE;
        if (IS_ATOM_INT(_25544))
        _25540 = (_25544 != 0);
        else
        _25540 = DBL_PTR(_25544)->dbl != 0.0;
LB: 
        _25538 = (_25540 != 0);
LA: 
        if (_25538 != 0) {
            goto LC; // [322] 349
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25546 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25546);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _25547 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _25547 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _25546 = NOVALUE;
        if (IS_ATOM_INT(_25547)) {
            _25548 = (_25547 == 523);
        }
        else {
            _25548 = binary_op(EQUALS, _25547, 523);
        }
        _25547 = NOVALUE;
        if (_25548 == 0) {
            DeRef(_25548);
            _25548 = NOVALUE;
            goto L7; // [345] 1011
        }
        else {
            if (!IS_ATOM_INT(_25548) && DBL_PTR(_25548)->dbl == 0.0){
                DeRef(_25548);
                _25548 = NOVALUE;
                goto L7; // [345] 1011
            }
            DeRef(_25548);
            _25548 = NOVALUE;
        }
        DeRef(_25548);
        _25548 = NOVALUE;
LC: 

        /** 						gtok = tok*/
        Ref(_tok_48247);
        DeRef(_gtok_48248);
        _gtok_48248 = _tok_48247;

        /** 						dup_globals &= st_ptr*/
        Append(&_55dup_globals_48216, _55dup_globals_48216, _st_ptr_48244);

        /** 						in_include_path &= include_matrix[scanning_file][SymTab[st_ptr][S_FILE_NO]] != 0*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _25550 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25551 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25551);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25552 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25552 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25551 = NOVALUE;
        _2 = (int)SEQ_PTR(_25550);
        if (!IS_ATOM_INT(_25552)){
            _25553 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25552)->dbl));
        }
        else{
            _25553 = (int)*(((s1_ptr)_2)->base + _25552);
        }
        _25550 = NOVALUE;
        if (IS_ATOM_INT(_25553)) {
            _25554 = (_25553 != 0);
        }
        else {
            _25554 = binary_op(NOTEQ, _25553, 0);
        }
        _25553 = NOVALUE;
        if (IS_SEQUENCE(_55in_include_path_48218) && IS_ATOM(_25554)) {
            Ref(_25554);
            Append(&_55in_include_path_48218, _55in_include_path_48218, _25554);
        }
        else if (IS_ATOM(_55in_include_path_48218) && IS_SEQUENCE(_25554)) {
        }
        else {
            Concat((object_ptr)&_55in_include_path_48218, _55in_include_path_48218, _25554);
        }
        DeRef(_25554);
        _25554 = NOVALUE;

        /** 					break*/
        goto L7; // [399] 1011

        /** 				case SC_PUBLIC, SC_EXPORT then*/
        case 13:
        case 11:

        /** 					if scanning_file = SymTab[st_ptr][S_FILE_NO] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25556 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25556);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25557 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25557 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25556 = NOVALUE;
        if (binary_op_a(NOTEQ, _scanning_file_48232, _25557)){
            _25557 = NOVALUE;
            goto LD; // [421] 445
        }
        _25557 = NOVALUE;

        /** 						if BIND then*/
        if (_38BIND_16567 == 0)
        {
            goto LE; // [429] 438
        }
        else{
        }

        /** 							add_ref(tok)*/
        Ref(_tok_48247);
        _55add_ref(_tok_48247);
LE: 

        /** 						return tok*/
        DeRefDS(_word_48230);
        DeRef(_msg_48238);
        DeRef(_b_name_48239);
        DeRef(_gtok_48248);
        DeRef(_25525);
        _25525 = NOVALUE;
        DeRef(_25515);
        _25515 = NOVALUE;
        _25539 = NOVALUE;
        _25544 = NOVALUE;
        _25543 = NOVALUE;
        _25552 = NOVALUE;
        return _tok_48247;
LD: 

        /** 					if (finished_files[scanning_file] -- everything this file needs has been read in*/
        _2 = (int)SEQ_PTR(_35finished_files_15598);
        _25559 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
        if (_25559 != 0) {
            _25560 = 1;
            goto LF; // [453] 487
        }
        if (_namespace_ok_48235 == 0) {
            _25561 = 0;
            goto L10; // [457] 483
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25562 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25562);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _25563 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _25563 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _25562 = NOVALUE;
        if (IS_ATOM_INT(_25563)) {
            _25564 = (_25563 == 523);
        }
        else {
            _25564 = binary_op(EQUALS, _25563, 523);
        }
        _25563 = NOVALUE;
        if (IS_ATOM_INT(_25564))
        _25561 = (_25564 != 0);
        else
        _25561 = DBL_PTR(_25564)->dbl != 0.0;
L10: 
        _25560 = (_25561 != 0);
LF: 
        if (_25560 == 0) {
            goto L7; // [487] 1011
        }
        _25566 = (_scope_48240 == 13);
        if (_25566 == 0) {
            _25567 = 0;
            goto L11; // [497] 533
        }
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _25568 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25569 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25569);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25570 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25570 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25569 = NOVALUE;
        _2 = (int)SEQ_PTR(_25568);
        if (!IS_ATOM_INT(_25570)){
            _25571 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25570)->dbl));
        }
        else{
            _25571 = (int)*(((s1_ptr)_2)->base + _25570);
        }
        _25568 = NOVALUE;
        if (IS_ATOM_INT(_25571)) {
            {unsigned long tu;
                 tu = (unsigned long)6 & (unsigned long)_25571;
                 _25572 = MAKE_UINT(tu);
            }
        }
        else {
            _25572 = binary_op(AND_BITS, 6, _25571);
        }
        _25571 = NOVALUE;
        if (IS_ATOM_INT(_25572))
        _25567 = (_25572 != 0);
        else
        _25567 = DBL_PTR(_25572)->dbl != 0.0;
L11: 
        if (_25567 != 0) {
            DeRef(_25573);
            _25573 = 1;
            goto L12; // [533] 583
        }
        _25574 = (_scope_48240 == 11);
        if (_25574 == 0) {
            _25575 = 0;
            goto L13; // [543] 579
        }
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _25576 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25577 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25577);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25578 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25578 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25577 = NOVALUE;
        _2 = (int)SEQ_PTR(_25576);
        if (!IS_ATOM_INT(_25578)){
            _25579 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25578)->dbl));
        }
        else{
            _25579 = (int)*(((s1_ptr)_2)->base + _25578);
        }
        _25576 = NOVALUE;
        if (IS_ATOM_INT(_25579)) {
            {unsigned long tu;
                 tu = (unsigned long)2 & (unsigned long)_25579;
                 _25580 = MAKE_UINT(tu);
            }
        }
        else {
            _25580 = binary_op(AND_BITS, 2, _25579);
        }
        _25579 = NOVALUE;
        if (IS_ATOM_INT(_25580))
        _25575 = (_25580 != 0);
        else
        _25575 = DBL_PTR(_25580)->dbl != 0.0;
L13: 
        DeRef(_25573);
        _25573 = (_25575 != 0);
L12: 
        if (_25573 == 0)
        {
            _25573 = NOVALUE;
            goto L7; // [584] 1011
        }
        else{
            _25573 = NOVALUE;
        }

        /** 						gtok = tok*/
        Ref(_tok_48247);
        DeRef(_gtok_48248);
        _gtok_48248 = _tok_48247;

        /** 						dup_globals &= st_ptr*/
        Append(&_55dup_globals_48216, _55dup_globals_48216, _st_ptr_48244);

        /** 						in_include_path &= include_matrix[scanning_file][SymTab[st_ptr][S_FILE_NO]] != 0 --symbol_in_include_path( st_ptr, scanning_file, {} )*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _25582 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25583 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25583);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25584 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25584 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25583 = NOVALUE;
        _2 = (int)SEQ_PTR(_25582);
        if (!IS_ATOM_INT(_25584)){
            _25585 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25584)->dbl));
        }
        else{
            _25585 = (int)*(((s1_ptr)_2)->base + _25584);
        }
        _25582 = NOVALUE;
        if (IS_ATOM_INT(_25585)) {
            _25586 = (_25585 != 0);
        }
        else {
            _25586 = binary_op(NOTEQ, _25585, 0);
        }
        _25585 = NOVALUE;
        if (IS_SEQUENCE(_55in_include_path_48218) && IS_ATOM(_25586)) {
            Ref(_25586);
            Append(&_55in_include_path_48218, _55in_include_path_48218, _25586);
        }
        else if (IS_ATOM(_55in_include_path_48218) && IS_SEQUENCE(_25586)) {
        }
        else {
            Concat((object_ptr)&_55in_include_path_48218, _55in_include_path_48218, _25586);
        }
        DeRef(_25586);
        _25586 = NOVALUE;

        /** ifdef STDDEBUG then*/

        /** 					break*/
        goto L7; // [639] 1011

        /** 				case SC_LOCAL then*/
        case 5:

        /** 					if scanning_file = SymTab[st_ptr][S_FILE_NO] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25588 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
        _2 = (int)SEQ_PTR(_25588);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25589 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25589 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25588 = NOVALUE;
        if (binary_op_a(NOTEQ, _scanning_file_48232, _25589)){
            _25589 = NOVALUE;
            goto L7; // [659] 1011
        }
        _25589 = NOVALUE;

        /** 						if BIND then*/
        if (_38BIND_16567 == 0)
        {
            goto L14; // [667] 676
        }
        else{
        }

        /** 							add_ref(tok)*/
        Ref(_tok_48247);
        _55add_ref(_tok_48247);
L14: 

        /** 						return tok*/
        DeRefDS(_word_48230);
        DeRef(_msg_48238);
        DeRef(_b_name_48239);
        DeRef(_gtok_48248);
        DeRef(_25525);
        _25525 = NOVALUE;
        DeRef(_25515);
        _25515 = NOVALUE;
        _25539 = NOVALUE;
        _25544 = NOVALUE;
        _25543 = NOVALUE;
        _25559 = NOVALUE;
        _25552 = NOVALUE;
        DeRef(_25566);
        _25566 = NOVALUE;
        DeRef(_25564);
        _25564 = NOVALUE;
        DeRef(_25574);
        _25574 = NOVALUE;
        _25570 = NOVALUE;
        DeRef(_25572);
        _25572 = NOVALUE;
        _25578 = NOVALUE;
        DeRef(_25580);
        _25580 = NOVALUE;
        _25584 = NOVALUE;
        return _tok_48247;

        /** 					break*/
        goto L7; // [685] 1011

        /** 				case else*/
        default:

        /** 					if BIND then*/
        if (_38BIND_16567 == 0)
        {
            goto L15; // [695] 704
        }
        else{
        }

        /** 						add_ref(tok)*/
        Ref(_tok_48247);
        _55add_ref(_tok_48247);
L15: 

        /** 					return tok -- keyword, private*/
        DeRefDS(_word_48230);
        DeRef(_msg_48238);
        DeRef(_b_name_48239);
        DeRef(_gtok_48248);
        DeRef(_25525);
        _25525 = NOVALUE;
        DeRef(_25515);
        _25515 = NOVALUE;
        _25539 = NOVALUE;
        _25544 = NOVALUE;
        _25543 = NOVALUE;
        _25559 = NOVALUE;
        _25552 = NOVALUE;
        DeRef(_25566);
        _25566 = NOVALUE;
        DeRef(_25564);
        _25564 = NOVALUE;
        DeRef(_25574);
        _25574 = NOVALUE;
        _25570 = NOVALUE;
        DeRef(_25572);
        _25572 = NOVALUE;
        _25578 = NOVALUE;
        DeRef(_25580);
        _25580 = NOVALUE;
        _25584 = NOVALUE;
        return _tok_48247;
    ;}    goto L7; // [711] 1011
L6: 

    /** 				scope = SymTab[tok[T_SYM]][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_tok_48247);
    _25591 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_25591)){
        _25592 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25591)->dbl));
    }
    else{
        _25592 = (int)*(((s1_ptr)_2)->base + _25591);
    }
    _2 = (int)SEQ_PTR(_25592);
    _scope_48240 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_48240)){
        _scope_48240 = (long)DBL_PTR(_scope_48240)->dbl;
    }
    _25592 = NOVALUE;

    /** 				if not file_no then*/
    if (_file_no_48231 != 0)
    goto L16; // [738] 772

    /** 					if scope = SC_PREDEF then*/
    if (_scope_48240 != 7)
    goto L17; // [745] 1010

    /** 						if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L18; // [753] 762
    }
    else{
    }

    /** 							add_ref( tok )*/
    Ref(_tok_48247);
    _55add_ref(_tok_48247);
L18: 

    /** 						return tok*/
    DeRefDS(_word_48230);
    DeRef(_msg_48238);
    DeRef(_b_name_48239);
    DeRef(_gtok_48248);
    DeRef(_25525);
    _25525 = NOVALUE;
    DeRef(_25515);
    _25515 = NOVALUE;
    _25539 = NOVALUE;
    _25544 = NOVALUE;
    _25543 = NOVALUE;
    _25559 = NOVALUE;
    _25552 = NOVALUE;
    DeRef(_25566);
    _25566 = NOVALUE;
    DeRef(_25564);
    _25564 = NOVALUE;
    DeRef(_25574);
    _25574 = NOVALUE;
    _25570 = NOVALUE;
    DeRef(_25572);
    _25572 = NOVALUE;
    _25591 = NOVALUE;
    _25578 = NOVALUE;
    DeRef(_25580);
    _25580 = NOVALUE;
    _25584 = NOVALUE;
    return _tok_48247;
    goto L17; // [769] 1010
L16: 

    /** 					integer tok_file = SymTab[tok[T_SYM]][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_tok_48247);
    _25596 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_25596)){
        _25597 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25596)->dbl));
    }
    else{
        _25597 = (int)*(((s1_ptr)_2)->base + _25596);
    }
    _2 = (int)SEQ_PTR(_25597);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _tok_file_48419 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _tok_file_48419 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_tok_file_48419)){
        _tok_file_48419 = (long)DBL_PTR(_tok_file_48419)->dbl;
    }
    _25597 = NOVALUE;

    /** 					integer good = 0*/
    _good_48426 = 0;

    /** 					if scope = SC_PRIVATE or scope = SC_PREDEF then*/
    _25599 = (_scope_48240 == 3);
    if (_25599 != 0) {
        goto L19; // [807] 940
    }
    _25601 = (_scope_48240 == 7);
    if (_25601 == 0)
    {
        DeRef(_25601);
        _25601 = NOVALUE;
        goto L1A; // [818] 825
    }
    else{
        DeRef(_25601);
        _25601 = NOVALUE;
    }
    goto L19; // [822] 940
L1A: 

    /** 					elsif file_no = tok_file then*/
    if (_file_no_48231 != _tok_file_48419)
    goto L1B; // [827] 839

    /** 						good = 1*/
    _good_48426 = 1;
    goto L19; // [836] 940
L1B: 

    /** 						integer include_type = 0*/
    _include_type_48436 = 0;

    /** 						switch scope do*/
    _0 = _scope_48240;
    switch ( _0 ){ 

        /** 							case SC_GLOBAL then*/
        case 6:

        /** 								if Resolve_unincluded_globals then*/
        if (_55Resolve_unincluded_globals_48221 == 0)
        {
            goto L1C; // [859] 874
        }
        else{
        }

        /** 									include_type = ANY_INCLUDE*/
        _include_type_48436 = 7;
        goto L1D; // [871] 919
L1C: 

        /** 									include_type = DIRECT_OR_PUBLIC_INCLUDE*/
        _include_type_48436 = 6;
        goto L1D; // [884] 919

        /** 							case SC_PUBLIC then*/
        case 13:

        /** 								if tok_file != file_no then*/
        if (_tok_file_48419 == _file_no_48231)
        goto L1E; // [892] 908

        /** 									include_type = PUBLIC_INCLUDE*/
        _include_type_48436 = 4;
        goto L1F; // [905] 918
L1E: 

        /** 									include_type = DIRECT_OR_PUBLIC_INCLUDE*/
        _include_type_48436 = 6;
L1F: 
    ;}L1D: 

    /** 						good = and_bits( include_type, include_matrix[file_no][tok_file] )*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _25606 = (int)*(((s1_ptr)_2)->base + _file_no_48231);
    _2 = (int)SEQ_PTR(_25606);
    _25607 = (int)*(((s1_ptr)_2)->base + _tok_file_48419);
    _25606 = NOVALUE;
    if (IS_ATOM_INT(_25607)) {
        {unsigned long tu;
             tu = (unsigned long)_include_type_48436 & (unsigned long)_25607;
             _good_48426 = MAKE_UINT(tu);
        }
    }
    else {
        _good_48426 = binary_op(AND_BITS, _include_type_48436, _25607);
    }
    _25607 = NOVALUE;
    if (!IS_ATOM_INT(_good_48426)) {
        _1 = (long)(DBL_PTR(_good_48426)->dbl);
        if (UNIQUE(DBL_PTR(_good_48426)) && (DBL_PTR(_good_48426)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_good_48426);
        _good_48426 = _1;
    }
L19: 

    /** 					if good then*/
    if (_good_48426 == 0)
    {
        goto L20; // [942] 1007
    }
    else{
    }

    /** 						if file_no = tok_file then*/
    if (_file_no_48231 != _tok_file_48419)
    goto L21; // [947] 971

    /** 							if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L22; // [955] 964
    }
    else{
    }

    /** 								add_ref(tok)*/
    Ref(_tok_48247);
    _55add_ref(_tok_48247);
L22: 

    /** 							return tok*/
    DeRefDS(_word_48230);
    DeRef(_msg_48238);
    DeRef(_b_name_48239);
    DeRef(_gtok_48248);
    DeRef(_25525);
    _25525 = NOVALUE;
    DeRef(_25515);
    _25515 = NOVALUE;
    _25539 = NOVALUE;
    _25544 = NOVALUE;
    _25543 = NOVALUE;
    _25559 = NOVALUE;
    _25552 = NOVALUE;
    DeRef(_25566);
    _25566 = NOVALUE;
    DeRef(_25564);
    _25564 = NOVALUE;
    DeRef(_25574);
    _25574 = NOVALUE;
    _25570 = NOVALUE;
    DeRef(_25572);
    _25572 = NOVALUE;
    _25591 = NOVALUE;
    _25578 = NOVALUE;
    DeRef(_25580);
    _25580 = NOVALUE;
    _25584 = NOVALUE;
    _25596 = NOVALUE;
    DeRef(_25599);
    _25599 = NOVALUE;
    return _tok_48247;
L21: 

    /** 						gtok = tok*/
    Ref(_tok_48247);
    DeRef(_gtok_48248);
    _gtok_48248 = _tok_48247;

    /** 						dup_globals &= st_ptr*/
    Append(&_55dup_globals_48216, _55dup_globals_48216, _st_ptr_48244);

    /** 						in_include_path &= include_matrix[scanning_file][tok_file] != 0*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _25611 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
    _2 = (int)SEQ_PTR(_25611);
    _25612 = (int)*(((s1_ptr)_2)->base + _tok_file_48419);
    _25611 = NOVALUE;
    if (IS_ATOM_INT(_25612)) {
        _25613 = (_25612 != 0);
    }
    else {
        _25613 = binary_op(NOTEQ, _25612, 0);
    }
    _25612 = NOVALUE;
    if (IS_SEQUENCE(_55in_include_path_48218) && IS_ATOM(_25613)) {
        Ref(_25613);
        Append(&_55in_include_path_48218, _55in_include_path_48218, _25613);
    }
    else if (IS_ATOM(_55in_include_path_48218) && IS_SEQUENCE(_25613)) {
    }
    else {
        Concat((object_ptr)&_55in_include_path_48218, _55in_include_path_48218, _25613);
    }
    DeRef(_25613);
    _25613 = NOVALUE;
L20: 
L17: 
L7: 
L4: 

    /** 		st_ptr = SymTab[st_ptr][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25615 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25615);
    _st_ptr_48244 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_st_ptr_48244)){
        _st_ptr_48244 = (long)DBL_PTR(_st_ptr_48244)->dbl;
    }
    _25615 = NOVALUE;

    /** 	end while*/
    goto L1; // [1030] 69
L2: 

    /** 	if length(dup_overrides) then*/
    if (IS_SEQUENCE(_55dup_overrides_48217)){
            _25617 = SEQ_PTR(_55dup_overrides_48217)->length;
    }
    else {
        _25617 = 1;
    }
    if (_25617 == 0)
    {
        _25617 = NOVALUE;
        goto L23; // [1040] 1093
    }
    else{
        _25617 = NOVALUE;
    }

    /** 		st_ptr = dup_overrides[1]*/
    _2 = (int)SEQ_PTR(_55dup_overrides_48217);
    _st_ptr_48244 = (int)*(((s1_ptr)_2)->base + 1);

    /** 		tok = {SymTab[st_ptr][S_TOKEN], st_ptr}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25619 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25619);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _25620 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _25620 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _25619 = NOVALUE;
    Ref(_25620);
    DeRef(_tok_48247);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25620;
    ((int *)_2)[2] = _st_ptr_48244;
    _tok_48247 = MAKE_SEQ(_1);
    _25620 = NOVALUE;

    /** 			if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L24; // [1075] 1084
    }
    else{
    }

    /** 				add_ref(tok)*/
    RefDS(_tok_48247);
    _55add_ref(_tok_48247);
L24: 

    /** 			return tok*/
    DeRefDS(_word_48230);
    DeRef(_msg_48238);
    DeRef(_b_name_48239);
    DeRef(_gtok_48248);
    DeRef(_25525);
    _25525 = NOVALUE;
    DeRef(_25515);
    _25515 = NOVALUE;
    _25539 = NOVALUE;
    _25544 = NOVALUE;
    _25543 = NOVALUE;
    _25559 = NOVALUE;
    _25552 = NOVALUE;
    DeRef(_25566);
    _25566 = NOVALUE;
    DeRef(_25564);
    _25564 = NOVALUE;
    DeRef(_25574);
    _25574 = NOVALUE;
    _25570 = NOVALUE;
    DeRef(_25572);
    _25572 = NOVALUE;
    _25591 = NOVALUE;
    _25578 = NOVALUE;
    DeRef(_25580);
    _25580 = NOVALUE;
    _25584 = NOVALUE;
    _25596 = NOVALUE;
    DeRef(_25599);
    _25599 = NOVALUE;
    return _tok_48247;
    goto L25; // [1090] 1320
L23: 

    /** 	elsif st_builtin != 0 then*/
    if (_st_builtin_48245 == 0)
    goto L26; // [1095] 1319

    /** 		if length(dup_globals) and find(SymTab[st_builtin][S_NAME], builtin_warnings) = 0 then*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25623 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25623 = 1;
    }
    if (_25623 == 0) {
        goto L27; // [1106] 1279
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25625 = (int)*(((s1_ptr)_2)->base + _st_builtin_48245);
    _2 = (int)SEQ_PTR(_25625);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25626 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25626 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25625 = NOVALUE;
    _25627 = find_from(_25626, _55builtin_warnings_48220, 1);
    _25626 = NOVALUE;
    _25628 = (_25627 == 0);
    _25627 = NOVALUE;
    if (_25628 == 0)
    {
        DeRef(_25628);
        _25628 = NOVALUE;
        goto L27; // [1134] 1279
    }
    else{
        DeRef(_25628);
        _25628 = NOVALUE;
    }

    /** 			sequence msg_file */

    /** 			b_name = SymTab[st_builtin][S_NAME]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25629 = (int)*(((s1_ptr)_2)->base + _st_builtin_48245);
    DeRef(_b_name_48239);
    _2 = (int)SEQ_PTR(_25629);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _b_name_48239 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _b_name_48239 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_b_name_48239);
    _25629 = NOVALUE;

    /** 			builtin_warnings = append(builtin_warnings, b_name)*/
    RefDS(_b_name_48239);
    Append(&_55builtin_warnings_48220, _55builtin_warnings_48220, _b_name_48239);

    /** 			if length(dup_globals) > 1 then*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25632 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25632 = 1;
    }
    if (_25632 <= 1)
    goto L28; // [1170] 1184

    /** 				msg = "\n"*/
    RefDS(_22815);
    DeRef(_msg_48238);
    _msg_48238 = _22815;
    goto L29; // [1181] 1192
L28: 

    /** 				msg = ""*/
    RefDS(_22663);
    DeRef(_msg_48238);
    _msg_48238 = _22663;
L29: 

    /** 			for i = 1 to length(dup_globals) do*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25634 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25634 = 1;
    }
    {
        int _i_48503;
        _i_48503 = 1;
L2A: 
        if (_i_48503 > _25634){
            goto L2B; // [1199] 1255
        }

        /** 				msg_file = known_files[SymTab[dup_globals[i]][S_FILE_NO]]*/
        _2 = (int)SEQ_PTR(_55dup_globals_48216);
        _25635 = (int)*(((s1_ptr)_2)->base + _i_48503);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_25635)){
            _25636 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25635)->dbl));
        }
        else{
            _25636 = (int)*(((s1_ptr)_2)->base + _25635);
        }
        _2 = (int)SEQ_PTR(_25636);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _25637 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _25637 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _25636 = NOVALUE;
        DeRef(_msg_file_48492);
        _2 = (int)SEQ_PTR(_35known_files_15596);
        if (!IS_ATOM_INT(_25637)){
            _msg_file_48492 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25637)->dbl));
        }
        else{
            _msg_file_48492 = (int)*(((s1_ptr)_2)->base + _25637);
        }
        Ref(_msg_file_48492);

        /** 				msg &= "    " & msg_file & "\n"*/
        {
            int concat_list[3];

            concat_list[0] = _22815;
            concat_list[1] = _msg_file_48492;
            concat_list[2] = _25639;
            Concat_N((object_ptr)&_25640, concat_list, 3);
        }
        Concat((object_ptr)&_msg_48238, _msg_48238, _25640);
        DeRefDS(_25640);
        _25640 = NOVALUE;

        /** 			end for*/
        _i_48503 = _i_48503 + 1;
        goto L2A; // [1250] 1206
L2B: 
        ;
    }

    /** 			Warning(234, builtin_chosen_warning_flag, {b_name, known_files[scanning_file], msg})*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _25642 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_b_name_48239);
    *((int *)(_2+4)) = _b_name_48239;
    Ref(_25642);
    *((int *)(_2+8)) = _25642;
    RefDS(_msg_48238);
    *((int *)(_2+12)) = _msg_48238;
    _25643 = MAKE_SEQ(_1);
    _25642 = NOVALUE;
    _46Warning(234, 8, _25643);
    _25643 = NOVALUE;
L27: 
    DeRef(_msg_file_48492);
    _msg_file_48492 = NOVALUE;

    /** 		tok = {SymTab[st_builtin][S_TOKEN], st_builtin}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25644 = (int)*(((s1_ptr)_2)->base + _st_builtin_48245);
    _2 = (int)SEQ_PTR(_25644);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _25645 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _25645 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _25644 = NOVALUE;
    Ref(_25645);
    DeRef(_tok_48247);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25645;
    ((int *)_2)[2] = _st_builtin_48245;
    _tok_48247 = MAKE_SEQ(_1);
    _25645 = NOVALUE;

    /** 		if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L2C; // [1303] 1312
    }
    else{
    }

    /** 			add_ref(tok)*/
    RefDS(_tok_48247);
    _55add_ref(_tok_48247);
L2C: 

    /** 		return tok*/
    DeRefDS(_word_48230);
    DeRef(_msg_48238);
    DeRef(_b_name_48239);
    DeRef(_gtok_48248);
    DeRef(_25525);
    _25525 = NOVALUE;
    DeRef(_25515);
    _25515 = NOVALUE;
    _25539 = NOVALUE;
    _25544 = NOVALUE;
    _25543 = NOVALUE;
    _25559 = NOVALUE;
    _25552 = NOVALUE;
    DeRef(_25566);
    _25566 = NOVALUE;
    DeRef(_25564);
    _25564 = NOVALUE;
    DeRef(_25574);
    _25574 = NOVALUE;
    _25570 = NOVALUE;
    DeRef(_25572);
    _25572 = NOVALUE;
    _25591 = NOVALUE;
    _25578 = NOVALUE;
    DeRef(_25580);
    _25580 = NOVALUE;
    _25584 = NOVALUE;
    _25596 = NOVALUE;
    DeRef(_25599);
    _25599 = NOVALUE;
    _25635 = NOVALUE;
    _25637 = NOVALUE;
    return _tok_48247;
L26: 
L25: 

    /** ifdef STDDEBUG then*/

    /** 	if length(dup_globals) > 1 and find( 1, in_include_path ) then*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25647 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25647 = 1;
    }
    _25648 = (_25647 > 1);
    _25647 = NOVALUE;
    if (_25648 == 0) {
        goto L2D; // [1333] 1452
    }
    _25650 = find_from(1, _55in_include_path_48218, 1);
    if (_25650 == 0)
    {
        _25650 = NOVALUE;
        goto L2D; // [1345] 1452
    }
    else{
        _25650 = NOVALUE;
    }

    /** 		ix = 1*/
    _ix_48242 = 1;

    /** 		while ix <= length(dup_globals) do*/
L2E: 
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25651 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25651 = 1;
    }
    if (_ix_48242 > _25651)
    goto L2F; // [1363] 1411

    /** 			if in_include_path[ix] then*/
    _2 = (int)SEQ_PTR(_55in_include_path_48218);
    _25653 = (int)*(((s1_ptr)_2)->base + _ix_48242);
    if (_25653 == 0) {
        _25653 = NOVALUE;
        goto L30; // [1375] 1387
    }
    else {
        if (!IS_ATOM_INT(_25653) && DBL_PTR(_25653)->dbl == 0.0){
            _25653 = NOVALUE;
            goto L30; // [1375] 1387
        }
        _25653 = NOVALUE;
    }
    _25653 = NOVALUE;

    /** 				ix += 1*/
    _ix_48242 = _ix_48242 + 1;
    goto L2E; // [1384] 1358
L30: 

    /** 				dup_globals     = remove( dup_globals, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_55dup_globals_48216);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_48242)) ? _ix_48242 : (long)(DBL_PTR(_ix_48242)->dbl);
        int stop = (IS_ATOM_INT(_ix_48242)) ? _ix_48242 : (long)(DBL_PTR(_ix_48242)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_55dup_globals_48216), start, &_55dup_globals_48216 );
            }
            else Tail(SEQ_PTR(_55dup_globals_48216), stop+1, &_55dup_globals_48216);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_55dup_globals_48216), start, &_55dup_globals_48216);
        }
        else {
            assign_slice_seq = &assign_space;
            _55dup_globals_48216 = Remove_elements(start, stop, (SEQ_PTR(_55dup_globals_48216)->ref == 1));
        }
    }

    /** 				in_include_path = remove( in_include_path, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_55in_include_path_48218);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_48242)) ? _ix_48242 : (long)(DBL_PTR(_ix_48242)->dbl);
        int stop = (IS_ATOM_INT(_ix_48242)) ? _ix_48242 : (long)(DBL_PTR(_ix_48242)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_55in_include_path_48218), start, &_55in_include_path_48218 );
            }
            else Tail(SEQ_PTR(_55in_include_path_48218), stop+1, &_55in_include_path_48218);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_55in_include_path_48218), start, &_55in_include_path_48218);
        }
        else {
            assign_slice_seq = &assign_space;
            _55in_include_path_48218 = Remove_elements(start, stop, (SEQ_PTR(_55in_include_path_48218)->ref == 1));
        }
    }

    /** 		end while*/
    goto L2E; // [1408] 1358
L2F: 

    /** 		if length(dup_globals) = 1 then*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25657 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25657 = 1;
    }
    if (_25657 != 1)
    goto L31; // [1418] 1451

    /** 				st_ptr = dup_globals[1]*/
    _2 = (int)SEQ_PTR(_55dup_globals_48216);
    _st_ptr_48244 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_st_ptr_48244)){
        _st_ptr_48244 = (long)DBL_PTR(_st_ptr_48244)->dbl;
    }

    /** 				gtok = {SymTab[st_ptr][S_TOKEN], st_ptr}*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25660 = (int)*(((s1_ptr)_2)->base + _st_ptr_48244);
    _2 = (int)SEQ_PTR(_25660);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _25661 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _25661 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _25660 = NOVALUE;
    Ref(_25661);
    DeRef(_gtok_48248);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25661;
    ((int *)_2)[2] = _st_ptr_48244;
    _gtok_48248 = MAKE_SEQ(_1);
    _25661 = NOVALUE;
L31: 
L2D: 

    /** ifdef STDDEBUG then*/

    /** 	if length(dup_globals) = 1 and st_builtin = 0 then*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25663 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25663 = 1;
    }
    _25664 = (_25663 == 1);
    _25663 = NOVALUE;
    if (_25664 == 0) {
        goto L32; // [1465] 1642
    }
    _25666 = (_st_builtin_48245 == 0);
    if (_25666 == 0)
    {
        DeRef(_25666);
        _25666 = NOVALUE;
        goto L32; // [1474] 1642
    }
    else{
        DeRef(_25666);
        _25666 = NOVALUE;
    }

    /** 		if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L33; // [1481] 1492
    }
    else{
    }

    /** 			add_ref(gtok)*/
    Ref(_gtok_48248);
    _55add_ref(_gtok_48248);
L33: 

    /** 		if not in_include_path[1] and*/
    _2 = (int)SEQ_PTR(_55in_include_path_48218);
    _25667 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_25667)) {
        _25668 = (_25667 == 0);
    }
    else {
        _25668 = unary_op(NOT, _25667);
    }
    _25667 = NOVALUE;
    if (IS_ATOM_INT(_25668)) {
        if (_25668 == 0) {
            goto L34; // [1503] 1635
        }
    }
    else {
        if (DBL_PTR(_25668)->dbl == 0.0) {
            goto L34; // [1503] 1635
        }
    }
    _2 = (int)SEQ_PTR(_gtok_48248);
    _25670 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_25670)){
        _25671 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25670)->dbl));
    }
    else{
        _25671 = (int)*(((s1_ptr)_2)->base + _25670);
    }
    _2 = (int)SEQ_PTR(_25671);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25672 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25672 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25671 = NOVALUE;
    Ref(_25672);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _scanning_file_48232;
    ((int *)_2)[2] = _25672;
    _25673 = MAKE_SEQ(_1);
    _25672 = NOVALUE;
    _25674 = find_from(_25673, _55include_warnings_48219, 1);
    DeRefDS(_25673);
    _25673 = NOVALUE;
    _25675 = (_25674 == 0);
    _25674 = NOVALUE;
    if (_25675 == 0)
    {
        DeRef(_25675);
        _25675 = NOVALUE;
        goto L34; // [1542] 1635
    }
    else{
        DeRef(_25675);
        _25675 = NOVALUE;
    }

    /** 			include_warnings = prepend( include_warnings,*/
    _2 = (int)SEQ_PTR(_gtok_48248);
    _25676 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_25676)){
        _25677 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25676)->dbl));
    }
    else{
        _25677 = (int)*(((s1_ptr)_2)->base + _25676);
    }
    _2 = (int)SEQ_PTR(_25677);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25678 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25678 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25677 = NOVALUE;
    Ref(_25678);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _scanning_file_48232;
    ((int *)_2)[2] = _25678;
    _25679 = MAKE_SEQ(_1);
    _25678 = NOVALUE;
    RefDS(_25679);
    Prepend(&_55include_warnings_48219, _55include_warnings_48219, _25679);
    DeRefDS(_25679);
    _25679 = NOVALUE;

    /** ifdef STDDEBUG then*/

    /** 				symbol_resolution_warning = GetMsgText(233,0,*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _25681 = (int)*(((s1_ptr)_2)->base + _scanning_file_48232);
    Ref(_25681);
    _25682 = _55name_ext(_25681);
    _25681 = NOVALUE;
    _2 = (int)SEQ_PTR(_gtok_48248);
    _25683 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_25683)){
        _25684 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25683)->dbl));
    }
    else{
        _25684 = (int)*(((s1_ptr)_2)->base + _25683);
    }
    _2 = (int)SEQ_PTR(_25684);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25685 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25685 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25684 = NOVALUE;
    _2 = (int)SEQ_PTR(_35known_files_15596);
    if (!IS_ATOM_INT(_25685)){
        _25686 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25685)->dbl));
    }
    else{
        _25686 = (int)*(((s1_ptr)_2)->base + _25685);
    }
    Ref(_25686);
    _25687 = _55name_ext(_25686);
    _25686 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _25682;
    *((int *)(_2+8)) = _38line_number_16947;
    RefDS(_word_48230);
    *((int *)(_2+12)) = _word_48230;
    *((int *)(_2+16)) = _25687;
    _25688 = MAKE_SEQ(_1);
    _25687 = NOVALUE;
    _25682 = NOVALUE;
    _0 = _47GetMsgText(233, 0, _25688);
    DeRef(_38symbol_resolution_warning_17065);
    _38symbol_resolution_warning_17065 = _0;
    _25688 = NOVALUE;
L34: 

    /** 		return gtok*/
    DeRefDS(_word_48230);
    DeRef(_msg_48238);
    DeRef(_b_name_48239);
    DeRef(_tok_48247);
    _25670 = NOVALUE;
    _25676 = NOVALUE;
    _25570 = NOVALUE;
    _25584 = NOVALUE;
    _25539 = NOVALUE;
    DeRef(_25580);
    _25580 = NOVALUE;
    DeRef(_25564);
    _25564 = NOVALUE;
    DeRef(_25648);
    _25648 = NOVALUE;
    _25552 = NOVALUE;
    DeRef(_25572);
    _25572 = NOVALUE;
    _25559 = NOVALUE;
    _25685 = NOVALUE;
    DeRef(_25525);
    _25525 = NOVALUE;
    _25591 = NOVALUE;
    _25596 = NOVALUE;
    DeRef(_25668);
    _25668 = NOVALUE;
    _25635 = NOVALUE;
    _25544 = NOVALUE;
    DeRef(_25566);
    _25566 = NOVALUE;
    DeRef(_25599);
    _25599 = NOVALUE;
    _25683 = NOVALUE;
    DeRef(_25515);
    _25515 = NOVALUE;
    _25543 = NOVALUE;
    _25637 = NOVALUE;
    DeRef(_25574);
    _25574 = NOVALUE;
    _25578 = NOVALUE;
    DeRef(_25664);
    _25664 = NOVALUE;
    return _gtok_48248;
L32: 

    /** 	if length(dup_globals) = 0 then*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25690 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25690 = 1;
    }
    if (_25690 != 0)
    goto L35; // [1649] 1723

    /** 		defined = SC_UNDEFINED*/
    _defined_48241 = 9;

    /** 		if fwd_line_number then*/
    if (_38fwd_line_number_16948 == 0)
    {
        goto L36; // [1666] 1695
    }
    else{
    }

    /** 			last_ForwardLine     = ForwardLine*/
    Ref(_46ForwardLine_49483);
    DeRef(_46last_ForwardLine_49485);
    _46last_ForwardLine_49485 = _46ForwardLine_49483;

    /** 			last_forward_bp      = forward_bp*/
    _46last_forward_bp_49489 = _46forward_bp_49487;

    /** 			last_fwd_line_number = fwd_line_number*/
    _38last_fwd_line_number_16950 = _38fwd_line_number_16948;
L36: 

    /** 		ForwardLine = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_46ForwardLine_49483);
    _46ForwardLine_49483 = _46ThisLine_49482;

    /** 		forward_bp = bp*/
    _46forward_bp_49487 = _46bp_49486;

    /** 		fwd_line_number = line_number*/
    _38fwd_line_number_16948 = _38line_number_16947;
    goto L37; // [1720] 1766
L35: 

    /** 	elsif length(dup_globals) then*/
    if (IS_SEQUENCE(_55dup_globals_48216)){
            _25692 = SEQ_PTR(_55dup_globals_48216)->length;
    }
    else {
        _25692 = 1;
    }
    if (_25692 == 0)
    {
        _25692 = NOVALUE;
        goto L38; // [1730] 1745
    }
    else{
        _25692 = NOVALUE;
    }

    /** 		defined = SC_MULTIPLY_DEFINED*/
    _defined_48241 = 10;
    goto L37; // [1742] 1766
L38: 

    /** 	elsif length(dup_overrides) then*/
    if (IS_SEQUENCE(_55dup_overrides_48217)){
            _25693 = SEQ_PTR(_55dup_overrides_48217)->length;
    }
    else {
        _25693 = 1;
    }
    if (_25693 == 0)
    {
        _25693 = NOVALUE;
        goto L39; // [1752] 1765
    }
    else{
        _25693 = NOVALUE;
    }

    /** 		defined = SC_OVERRIDE*/
    _defined_48241 = 12;
L39: 
L37: 

    /** 	if No_new_entry then*/
    if (_55No_new_entry_48227 == 0)
    {
        goto L3A; // [1770] 1793
    }
    else{
    }

    /** 		return {IGNORED,word,defined,dup_globals}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 509;
    RefDS(_word_48230);
    *((int *)(_2+8)) = _word_48230;
    *((int *)(_2+12)) = _defined_48241;
    RefDS(_55dup_globals_48216);
    *((int *)(_2+16)) = _55dup_globals_48216;
    _25694 = MAKE_SEQ(_1);
    DeRefDS(_word_48230);
    DeRef(_msg_48238);
    DeRef(_b_name_48239);
    DeRef(_tok_48247);
    DeRef(_gtok_48248);
    _25670 = NOVALUE;
    _25676 = NOVALUE;
    _25570 = NOVALUE;
    _25584 = NOVALUE;
    _25539 = NOVALUE;
    DeRef(_25580);
    _25580 = NOVALUE;
    DeRef(_25564);
    _25564 = NOVALUE;
    DeRef(_25648);
    _25648 = NOVALUE;
    _25552 = NOVALUE;
    DeRef(_25572);
    _25572 = NOVALUE;
    _25559 = NOVALUE;
    _25685 = NOVALUE;
    DeRef(_25525);
    _25525 = NOVALUE;
    _25591 = NOVALUE;
    _25596 = NOVALUE;
    DeRef(_25668);
    _25668 = NOVALUE;
    _25635 = NOVALUE;
    _25544 = NOVALUE;
    DeRef(_25566);
    _25566 = NOVALUE;
    DeRef(_25599);
    _25599 = NOVALUE;
    _25683 = NOVALUE;
    DeRef(_25515);
    _25515 = NOVALUE;
    _25543 = NOVALUE;
    _25637 = NOVALUE;
    DeRef(_25574);
    _25574 = NOVALUE;
    _25578 = NOVALUE;
    DeRef(_25664);
    _25664 = NOVALUE;
    return _25694;
L3A: 

    /** 	tok = {VARIABLE, NewEntry(word, 0, defined,*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _25695 = (int)*(((s1_ptr)_2)->base + _hashval_48236);
    RefDS(_word_48230);
    Ref(_25695);
    _25696 = _55NewEntry(_word_48230, 0, _defined_48241, -100, _hashval_48236, _25695, 0);
    _25695 = NOVALUE;
    DeRef(_tok_48247);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _25696;
    _tok_48247 = MAKE_SEQ(_1);
    _25696 = NOVALUE;

    /** 	buckets[hashval] = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_48247);
    _25698 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_25698);
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _2 = (int)(((s1_ptr)_2)->base + _hashval_48236);
    _1 = *(int *)_2;
    *(int *)_2 = _25698;
    if( _1 != _25698 ){
        DeRef(_1);
    }
    _25698 = NOVALUE;

    /** 	if file_no != -1 then*/
    if (_file_no_48231 == -1)
    goto L3B; // [1837] 1863

    /** 		SymTab[tok[T_SYM]][S_FILE_NO] = file_no*/
    _2 = (int)SEQ_PTR(_tok_48247);
    _25700 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_25700))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25700)->dbl));
    else
    _3 = (int)(_25700 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FILE_NO_16594))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    _1 = *(int *)_2;
    *(int *)_2 = _file_no_48231;
    DeRef(_1);
    _25701 = NOVALUE;
L3B: 

    /** 	return tok  -- no ref on newly declared symbol*/
    DeRefDS(_word_48230);
    DeRef(_msg_48238);
    DeRef(_b_name_48239);
    DeRef(_gtok_48248);
    _25670 = NOVALUE;
    _25676 = NOVALUE;
    _25570 = NOVALUE;
    _25584 = NOVALUE;
    _25539 = NOVALUE;
    DeRef(_25580);
    _25580 = NOVALUE;
    DeRef(_25564);
    _25564 = NOVALUE;
    DeRef(_25648);
    _25648 = NOVALUE;
    _25552 = NOVALUE;
    DeRef(_25572);
    _25572 = NOVALUE;
    _25559 = NOVALUE;
    _25685 = NOVALUE;
    DeRef(_25525);
    _25525 = NOVALUE;
    _25591 = NOVALUE;
    _25596 = NOVALUE;
    DeRef(_25668);
    _25668 = NOVALUE;
    _25635 = NOVALUE;
    _25544 = NOVALUE;
    _25700 = NOVALUE;
    DeRef(_25566);
    _25566 = NOVALUE;
    DeRef(_25599);
    _25599 = NOVALUE;
    _25683 = NOVALUE;
    DeRef(_25515);
    _25515 = NOVALUE;
    DeRef(_25694);
    _25694 = NOVALUE;
    _25543 = NOVALUE;
    _25637 = NOVALUE;
    DeRef(_25574);
    _25574 = NOVALUE;
    _25578 = NOVALUE;
    DeRef(_25664);
    _25664 = NOVALUE;
    return _tok_48247;
    ;
}


void _55Hide(int _s_48640)
{
    int _prev_48642 = NOVALUE;
    int _p_48643 = NOVALUE;
    int _25721 = NOVALUE;
    int _25720 = NOVALUE;
    int _25719 = NOVALUE;
    int _25717 = NOVALUE;
    int _25716 = NOVALUE;
    int _25715 = NOVALUE;
    int _25714 = NOVALUE;
    int _25713 = NOVALUE;
    int _25709 = NOVALUE;
    int _25708 = NOVALUE;
    int _25707 = NOVALUE;
    int _25706 = NOVALUE;
    int _25704 = NOVALUE;
    int _25703 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_48640)) {
        _1 = (long)(DBL_PTR(_s_48640)->dbl);
        if (UNIQUE(DBL_PTR(_s_48640)) && (DBL_PTR(_s_48640)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48640);
        _s_48640 = _1;
    }

    /** 	p = buckets[SymTab[s][S_HASHVAL]]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25703 = (int)*(((s1_ptr)_2)->base + _s_48640);
    _2 = (int)SEQ_PTR(_25703);
    _25704 = (int)*(((s1_ptr)_2)->base + 11);
    _25703 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_25704)){
        _p_48643 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25704)->dbl));
    }
    else{
        _p_48643 = (int)*(((s1_ptr)_2)->base + _25704);
    }
    if (!IS_ATOM_INT(_p_48643)){
        _p_48643 = (long)DBL_PTR(_p_48643)->dbl;
    }

    /** 	prev = 0*/
    _prev_48642 = 0;

    /** 	while p != s and p != 0 do*/
L1: 
    _25706 = (_p_48643 != _s_48640);
    if (_25706 == 0) {
        goto L2; // [41] 81
    }
    _25708 = (_p_48643 != 0);
    if (_25708 == 0)
    {
        DeRef(_25708);
        _25708 = NOVALUE;
        goto L2; // [50] 81
    }
    else{
        DeRef(_25708);
        _25708 = NOVALUE;
    }

    /** 		prev = p*/
    _prev_48642 = _p_48643;

    /** 		p = SymTab[p][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25709 = (int)*(((s1_ptr)_2)->base + _p_48643);
    _2 = (int)SEQ_PTR(_25709);
    _p_48643 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_p_48643)){
        _p_48643 = (long)DBL_PTR(_p_48643)->dbl;
    }
    _25709 = NOVALUE;

    /** 	end while*/
    goto L1; // [78] 37
L2: 

    /** 	if p = 0 then*/
    if (_p_48643 != 0)
    goto L3; // [83] 93

    /** 		return -- already hidden*/
    _25704 = NOVALUE;
    DeRef(_25706);
    _25706 = NOVALUE;
    return;
L3: 

    /** 	if prev = 0 then*/
    if (_prev_48642 != 0)
    goto L4; // [95] 134

    /** 		buckets[SymTab[s][S_HASHVAL]] = SymTab[s][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25713 = (int)*(((s1_ptr)_2)->base + _s_48640);
    _2 = (int)SEQ_PTR(_25713);
    _25714 = (int)*(((s1_ptr)_2)->base + 11);
    _25713 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25715 = (int)*(((s1_ptr)_2)->base + _s_48640);
    _2 = (int)SEQ_PTR(_25715);
    _25716 = (int)*(((s1_ptr)_2)->base + 9);
    _25715 = NOVALUE;
    Ref(_25716);
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_25714))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25714)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25714);
    _1 = *(int *)_2;
    *(int *)_2 = _25716;
    if( _1 != _25716 ){
        DeRef(_1);
    }
    _25716 = NOVALUE;
    goto L5; // [131] 162
L4: 

    /** 		SymTab[prev][S_SAMEHASH] = SymTab[s][S_SAMEHASH]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_prev_48642 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25719 = (int)*(((s1_ptr)_2)->base + _s_48640);
    _2 = (int)SEQ_PTR(_25719);
    _25720 = (int)*(((s1_ptr)_2)->base + 9);
    _25719 = NOVALUE;
    Ref(_25720);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _25720;
    if( _1 != _25720 ){
        DeRef(_1);
    }
    _25720 = NOVALUE;
    _25717 = NOVALUE;
L5: 

    /** 	SymTab[s][S_SAMEHASH] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_48640 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _25721 = NOVALUE;

    /** end procedure*/
    _25704 = NOVALUE;
    DeRef(_25706);
    _25706 = NOVALUE;
    _25714 = NOVALUE;
    return;
    ;
}


void _55Show(int _s_48685)
{
    int _p_48687 = NOVALUE;
    int _25733 = NOVALUE;
    int _25732 = NOVALUE;
    int _25730 = NOVALUE;
    int _25729 = NOVALUE;
    int _25727 = NOVALUE;
    int _25726 = NOVALUE;
    int _25724 = NOVALUE;
    int _25723 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_48685)) {
        _1 = (long)(DBL_PTR(_s_48685)->dbl);
        if (UNIQUE(DBL_PTR(_s_48685)) && (DBL_PTR(_s_48685)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48685);
        _s_48685 = _1;
    }

    /** 	p = buckets[SymTab[s][S_HASHVAL]]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25723 = (int)*(((s1_ptr)_2)->base + _s_48685);
    _2 = (int)SEQ_PTR(_25723);
    _25724 = (int)*(((s1_ptr)_2)->base + 11);
    _25723 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_25724)){
        _p_48687 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_25724)->dbl));
    }
    else{
        _p_48687 = (int)*(((s1_ptr)_2)->base + _25724);
    }
    if (!IS_ATOM_INT(_p_48687)){
        _p_48687 = (long)DBL_PTR(_p_48687)->dbl;
    }

    /** 	if SymTab[s][S_SAMEHASH] or p = s then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25726 = (int)*(((s1_ptr)_2)->base + _s_48685);
    _2 = (int)SEQ_PTR(_25726);
    _25727 = (int)*(((s1_ptr)_2)->base + 9);
    _25726 = NOVALUE;
    if (IS_ATOM_INT(_25727)) {
        if (_25727 != 0) {
            goto L1; // [39] 52
        }
    }
    else {
        if (DBL_PTR(_25727)->dbl != 0.0) {
            goto L1; // [39] 52
        }
    }
    _25729 = (_p_48687 == _s_48685);
    if (_25729 == 0)
    {
        DeRef(_25729);
        _25729 = NOVALUE;
        goto L2; // [48] 58
    }
    else{
        DeRef(_25729);
        _25729 = NOVALUE;
    }
L1: 

    /** 		return*/
    _25724 = NOVALUE;
    _25727 = NOVALUE;
    return;
L2: 

    /** 	SymTab[s][S_SAMEHASH] = p*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_48685 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _p_48687;
    DeRef(_1);
    _25730 = NOVALUE;

    /** 	buckets[SymTab[s][S_HASHVAL]] = s*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25732 = (int)*(((s1_ptr)_2)->base + _s_48685);
    _2 = (int)SEQ_PTR(_25732);
    _25733 = (int)*(((s1_ptr)_2)->base + 11);
    _25732 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    if (!IS_ATOM_INT(_25733))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_25733)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _25733);
    _1 = *(int *)_2;
    *(int *)_2 = _s_48685;
    DeRef(_1);

    /** end procedure*/
    _25724 = NOVALUE;
    _25727 = NOVALUE;
    _25733 = NOVALUE;
    return;
    ;
}


void _55hide_params(int _s_48711)
{
    int _param_48713 = NOVALUE;
    int _25736 = NOVALUE;
    int _25735 = NOVALUE;
    int _25734 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_48711)) {
        _1 = (long)(DBL_PTR(_s_48711)->dbl);
        if (UNIQUE(DBL_PTR(_s_48711)) && (DBL_PTR(_s_48711)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48711);
        _s_48711 = _1;
    }

    /** 	symtab_index param = s*/
    _param_48713 = _s_48711;

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25734 = (int)*(((s1_ptr)_2)->base + _s_48711);
    _2 = (int)SEQ_PTR(_25734);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _25735 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _25735 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _25734 = NOVALUE;
    {
        int _i_48715;
        _i_48715 = 1;
L1: 
        if (binary_op_a(GREATER, _i_48715, _25735)){
            goto L2; // [24] 59
        }

        /** 		param = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25736 = (int)*(((s1_ptr)_2)->base + _s_48711);
        _2 = (int)SEQ_PTR(_25736);
        _param_48713 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_48713)){
            _param_48713 = (long)DBL_PTR(_param_48713)->dbl;
        }
        _25736 = NOVALUE;

        /** 		Hide( param )*/
        _55Hide(_param_48713);

        /** 	end for*/
        _0 = _i_48715;
        if (IS_ATOM_INT(_i_48715)) {
            _i_48715 = _i_48715 + 1;
            if ((long)((unsigned long)_i_48715 +(unsigned long) HIGH_BITS) >= 0){
                _i_48715 = NewDouble((double)_i_48715);
            }
        }
        else {
            _i_48715 = binary_op_a(PLUS, _i_48715, 1);
        }
        DeRef(_0);
        goto L1; // [54] 31
L2: 
        ;
        DeRef(_i_48715);
    }

    /** end procedure*/
    _25735 = NOVALUE;
    return;
    ;
}


void _55show_params(int _s_48727)
{
    int _param_48729 = NOVALUE;
    int _25740 = NOVALUE;
    int _25739 = NOVALUE;
    int _25738 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_48727)) {
        _1 = (long)(DBL_PTR(_s_48727)->dbl);
        if (UNIQUE(DBL_PTR(_s_48727)) && (DBL_PTR(_s_48727)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48727);
        _s_48727 = _1;
    }

    /** 	symtab_index param = s*/
    _param_48729 = _s_48727;

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25738 = (int)*(((s1_ptr)_2)->base + _s_48727);
    _2 = (int)SEQ_PTR(_25738);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _25739 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _25739 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _25738 = NOVALUE;
    {
        int _i_48731;
        _i_48731 = 1;
L1: 
        if (binary_op_a(GREATER, _i_48731, _25739)){
            goto L2; // [24] 59
        }

        /** 		param = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25740 = (int)*(((s1_ptr)_2)->base + _s_48727);
        _2 = (int)SEQ_PTR(_25740);
        _param_48729 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_param_48729)){
            _param_48729 = (long)DBL_PTR(_param_48729)->dbl;
        }
        _25740 = NOVALUE;

        /** 		Show( param )*/
        _55Show(_param_48729);

        /** 	end for*/
        _0 = _i_48731;
        if (IS_ATOM_INT(_i_48731)) {
            _i_48731 = _i_48731 + 1;
            if ((long)((unsigned long)_i_48731 +(unsigned long) HIGH_BITS) >= 0){
                _i_48731 = NewDouble((double)_i_48731);
            }
        }
        else {
            _i_48731 = binary_op_a(PLUS, _i_48731, 1);
        }
        DeRef(_0);
        goto L1; // [54] 31
L2: 
        ;
        DeRef(_i_48731);
    }

    /** end procedure*/
    _25739 = NOVALUE;
    return;
    ;
}


void _55LintCheck(int _s_48743)
{
    int _warn_level_48744 = NOVALUE;
    int _file_48745 = NOVALUE;
    int _vscope_48746 = NOVALUE;
    int _vname_48747 = NOVALUE;
    int _vusage_48748 = NOVALUE;
    int _25801 = NOVALUE;
    int _25800 = NOVALUE;
    int _25799 = NOVALUE;
    int _25798 = NOVALUE;
    int _25797 = NOVALUE;
    int _25796 = NOVALUE;
    int _25794 = NOVALUE;
    int _25793 = NOVALUE;
    int _25792 = NOVALUE;
    int _25791 = NOVALUE;
    int _25790 = NOVALUE;
    int _25789 = NOVALUE;
    int _25786 = NOVALUE;
    int _25785 = NOVALUE;
    int _25784 = NOVALUE;
    int _25783 = NOVALUE;
    int _25782 = NOVALUE;
    int _25781 = NOVALUE;
    int _25779 = NOVALUE;
    int _25777 = NOVALUE;
    int _25776 = NOVALUE;
    int _25774 = NOVALUE;
    int _25773 = NOVALUE;
    int _25771 = NOVALUE;
    int _25770 = NOVALUE;
    int _25769 = NOVALUE;
    int _25768 = NOVALUE;
    int _25766 = NOVALUE;
    int _25765 = NOVALUE;
    int _25761 = NOVALUE;
    int _25758 = NOVALUE;
    int _25757 = NOVALUE;
    int _25756 = NOVALUE;
    int _25755 = NOVALUE;
    int _25752 = NOVALUE;
    int _25751 = NOVALUE;
    int _25746 = NOVALUE;
    int _25744 = NOVALUE;
    int _25742 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_48743)) {
        _1 = (long)(DBL_PTR(_s_48743)->dbl);
        if (UNIQUE(DBL_PTR(_s_48743)) && (DBL_PTR(_s_48743)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_48743);
        _s_48743 = _1;
    }

    /** 	vusage = SymTab[s][S_USAGE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25742 = (int)*(((s1_ptr)_2)->base + _s_48743);
    _2 = (int)SEQ_PTR(_25742);
    _vusage_48748 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_vusage_48748)){
        _vusage_48748 = (long)DBL_PTR(_vusage_48748)->dbl;
    }
    _25742 = NOVALUE;

    /** 	vscope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25744 = (int)*(((s1_ptr)_2)->base + _s_48743);
    _2 = (int)SEQ_PTR(_25744);
    _vscope_48746 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_vscope_48746)){
        _vscope_48746 = (long)DBL_PTR(_vscope_48746)->dbl;
    }
    _25744 = NOVALUE;

    /** 	vname = SymTab[s][S_NAME]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25746 = (int)*(((s1_ptr)_2)->base + _s_48743);
    DeRef(_vname_48747);
    _2 = (int)SEQ_PTR(_25746);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _vname_48747 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _vname_48747 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_vname_48747);
    _25746 = NOVALUE;

    /** 	switch vusage do*/
    _0 = _vusage_48748;
    switch ( _0 ){ 

        /** 		case U_UNUSED then*/
        case 0:

        /** 			warn_level = 1*/
        _warn_level_48744 = 1;
        goto L1; // [67] 193

        /** 		case U_WRITTEN then -- Set but never read*/
        case 2:

        /** 			warn_level = 2*/
        _warn_level_48744 = 2;

        /** 			if vscope > SC_LOCAL then*/
        if (_vscope_48746 <= 5)
        goto L2; // [82] 94

        /** 				warn_level = 0 */
        _warn_level_48744 = 0;
        goto L1; // [91] 193
L2: 

        /** 			elsif SymTab[s][S_MODE] = M_CONSTANT then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25751 = (int)*(((s1_ptr)_2)->base + _s_48743);
        _2 = (int)SEQ_PTR(_25751);
        _25752 = (int)*(((s1_ptr)_2)->base + 3);
        _25751 = NOVALUE;
        if (binary_op_a(NOTEQ, _25752, 2)){
            _25752 = NOVALUE;
            goto L1; // [110] 193
        }
        _25752 = NOVALUE;

        /** 				if not Strict_is_on then*/
        if (_38Strict_is_on_17011 != 0)
        goto L1; // [118] 193

        /** 					warn_level = 0 */
        _warn_level_48744 = 0;
        goto L1; // [129] 193

        /** 		case U_READ then -- Read but never set*/
        case 1:

        /** 			if SymTab[s][S_VARNUM] >= SymTab[CurrentSub][S_NUM_ARGS] then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25755 = (int)*(((s1_ptr)_2)->base + _s_48743);
        _2 = (int)SEQ_PTR(_25755);
        _25756 = (int)*(((s1_ptr)_2)->base + 16);
        _25755 = NOVALUE;
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25757 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
        _2 = (int)SEQ_PTR(_25757);
        if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
            _25758 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
        }
        else{
            _25758 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
        }
        _25757 = NOVALUE;
        if (binary_op_a(LESS, _25756, _25758)){
            _25756 = NOVALUE;
            _25758 = NOVALUE;
            goto L3; // [163] 175
        }
        _25756 = NOVALUE;
        _25758 = NOVALUE;

        /** 		    	warn_level = 3*/
        _warn_level_48744 = 3;
        goto L1; // [172] 193
L3: 

        /** 		    	warn_level = 0*/
        _warn_level_48744 = 0;
        goto L1; // [181] 193

        /** 	    case else*/
        default:

        /** 	    	warn_level = 0*/
        _warn_level_48744 = 0;
    ;}L1: 

    /** 	if warn_level = 0 then*/
    if (_warn_level_48744 != 0)
    goto L4; // [197] 207

    /** 		return*/
    DeRef(_file_48745);
    DeRef(_vname_48747);
    return;
L4: 

    /** 	file = abbreviate_path(known_files[current_file_no])*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _25761 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    Ref(_25761);
    RefDS(_22663);
    _0 = _file_48745;
    _file_48745 = _13abbreviate_path(_25761, _22663);
    DeRef(_0);
    _25761 = NOVALUE;

    /** 	if warn_level = 3 then*/
    if (_warn_level_48744 != 3)
    goto L5; // [226] 308

    /** 		if vscope = SC_LOCAL then*/
    if (_vscope_48746 != 5)
    goto L6; // [234] 275

    /** 			if current_file_no = SymTab[s][S_FILE_NO] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25765 = (int)*(((s1_ptr)_2)->base + _s_48743);
    _2 = (int)SEQ_PTR(_25765);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25766 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25766 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25765 = NOVALUE;
    if (binary_op_a(NOTEQ, _38current_file_no_16946, _25766)){
        _25766 = NOVALUE;
        goto L7; // [254] 602
    }
    _25766 = NOVALUE;

    /** 				Warning(226, no_value_warning_flag, {file,  vname})*/
    RefDS(_vname_48747);
    RefDS(_file_48745);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48745;
    ((int *)_2)[2] = _vname_48747;
    _25768 = MAKE_SEQ(_1);
    _46Warning(226, 32, _25768);
    _25768 = NOVALUE;
    goto L7; // [272] 602
L6: 

    /** 			Warning(227, no_value_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25769 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25769);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25770 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25770 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25769 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48745);
    *((int *)(_2+4)) = _file_48745;
    RefDS(_vname_48747);
    *((int *)(_2+8)) = _vname_48747;
    Ref(_25770);
    *((int *)(_2+12)) = _25770;
    _25771 = MAKE_SEQ(_1);
    _25770 = NOVALUE;
    _46Warning(227, 32, _25771);
    _25771 = NOVALUE;
    goto L7; // [305] 602
L5: 

    /** 		if vscope = SC_LOCAL then*/
    if (_vscope_48746 != 5)
    goto L8; // [312] 412

    /** 			if current_file_no = SymTab[s][S_FILE_NO] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25773 = (int)*(((s1_ptr)_2)->base + _s_48743);
    _2 = (int)SEQ_PTR(_25773);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25774 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25774 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25773 = NOVALUE;
    if (binary_op_a(NOTEQ, _38current_file_no_16946, _25774)){
        _25774 = NOVALUE;
        goto L9; // [332] 601
    }
    _25774 = NOVALUE;

    /** 				if SymTab[s][S_MODE] = M_CONSTANT then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25776 = (int)*(((s1_ptr)_2)->base + _s_48743);
    _2 = (int)SEQ_PTR(_25776);
    _25777 = (int)*(((s1_ptr)_2)->base + 3);
    _25776 = NOVALUE;
    if (binary_op_a(NOTEQ, _25777, 2)){
        _25777 = NOVALUE;
        goto LA; // [352] 372
    }
    _25777 = NOVALUE;

    /** 					Warning(228, not_used_warning_flag, {file,  vname})*/
    RefDS(_vname_48747);
    RefDS(_file_48745);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48745;
    ((int *)_2)[2] = _vname_48747;
    _25779 = MAKE_SEQ(_1);
    _46Warning(228, 16, _25779);
    _25779 = NOVALUE;
    goto L9; // [369] 601
LA: 

    /** 				elsif warn_level = 1 then*/
    if (_warn_level_48744 != 1)
    goto LB; // [374] 394

    /** 					Warning(229, not_used_warning_flag, {file,  vname})*/
    RefDS(_vname_48747);
    RefDS(_file_48745);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48745;
    ((int *)_2)[2] = _vname_48747;
    _25781 = MAKE_SEQ(_1);
    _46Warning(229, 16, _25781);
    _25781 = NOVALUE;
    goto L9; // [391] 601
LB: 

    /** 					Warning(320, not_used_warning_flag, {file,  vname})*/
    RefDS(_vname_48747);
    RefDS(_file_48745);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_48745;
    ((int *)_2)[2] = _vname_48747;
    _25782 = MAKE_SEQ(_1);
    _46Warning(320, 16, _25782);
    _25782 = NOVALUE;
    goto L9; // [409] 601
L8: 

    /** 			if SymTab[s][S_VARNUM] < SymTab[CurrentSub][S_NUM_ARGS] then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25783 = (int)*(((s1_ptr)_2)->base + _s_48743);
    _2 = (int)SEQ_PTR(_25783);
    _25784 = (int)*(((s1_ptr)_2)->base + 16);
    _25783 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25785 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25785);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _25786 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _25786 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _25785 = NOVALUE;
    if (binary_op_a(GREATEREQ, _25784, _25786)){
        _25784 = NOVALUE;
        _25786 = NOVALUE;
        goto LC; // [440] 523
    }
    _25784 = NOVALUE;
    _25786 = NOVALUE;

    /** 				if warn_level = 1 then*/
    if (_warn_level_48744 != 1)
    goto LD; // [446] 490

    /** 					if Strict_is_on then*/
    if (_38Strict_is_on_17011 == 0)
    {
        goto LE; // [454] 600
    }
    else{
    }

    /** 						Warning(230, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25789 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25789);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25790 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25790 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25789 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48745);
    *((int *)(_2+4)) = _file_48745;
    RefDS(_vname_48747);
    *((int *)(_2+8)) = _vname_48747;
    Ref(_25790);
    *((int *)(_2+12)) = _25790;
    _25791 = MAKE_SEQ(_1);
    _25790 = NOVALUE;
    _46Warning(230, 16, _25791);
    _25791 = NOVALUE;
    goto LE; // [487] 600
LD: 

    /** 					Warning(321, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25792 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25792);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25793 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25793 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25792 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48745);
    *((int *)(_2+4)) = _file_48745;
    RefDS(_vname_48747);
    *((int *)(_2+8)) = _vname_48747;
    Ref(_25793);
    *((int *)(_2+12)) = _25793;
    _25794 = MAKE_SEQ(_1);
    _25793 = NOVALUE;
    _46Warning(321, 16, _25794);
    _25794 = NOVALUE;
    goto LE; // [520] 600
LC: 

    /** 				if warn_level = 1 then*/
    if (_warn_level_48744 != 1)
    goto LF; // [525] 569

    /** 					if Strict_is_on then*/
    if (_38Strict_is_on_17011 == 0)
    {
        goto L10; // [533] 599
    }
    else{
    }

    /** 						Warning(231, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25796 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25796);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25797 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25797 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25796 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48745);
    *((int *)(_2+4)) = _file_48745;
    RefDS(_vname_48747);
    *((int *)(_2+8)) = _vname_48747;
    Ref(_25797);
    *((int *)(_2+12)) = _25797;
    _25798 = MAKE_SEQ(_1);
    _25797 = NOVALUE;
    _46Warning(231, 16, _25798);
    _25798 = NOVALUE;
    goto L10; // [566] 599
LF: 

    /** 					Warning(322, not_used_warning_flag, {file,  vname, SymTab[CurrentSub][S_NAME]})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25799 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25799);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25800 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25800 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25799 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_file_48745);
    *((int *)(_2+4)) = _file_48745;
    RefDS(_vname_48747);
    *((int *)(_2+8)) = _vname_48747;
    Ref(_25800);
    *((int *)(_2+12)) = _25800;
    _25801 = MAKE_SEQ(_1);
    _25800 = NOVALUE;
    _46Warning(322, 16, _25801);
    _25801 = NOVALUE;
L10: 
LE: 
L9: 
L7: 

    /** end procedure*/
    DeRef(_file_48745);
    DeRef(_vname_48747);
    return;
    ;
}


void _55HideLocals()
{
    int _s_48914 = NOVALUE;
    int _25812 = NOVALUE;
    int _25810 = NOVALUE;
    int _25809 = NOVALUE;
    int _25808 = NOVALUE;
    int _25807 = NOVALUE;
    int _25806 = NOVALUE;
    int _25805 = NOVALUE;
    int _25804 = NOVALUE;
    int _25803 = NOVALUE;
    int _25802 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = file_start_sym*/
    _s_48914 = _38file_start_sym_16952;

    /** 	while s do*/
L1: 
    if (_s_48914 == 0)
    {
        goto L2; // [15] 117
    }
    else{
    }

    /** 		if SymTab[s][S_SCOPE] = SC_LOCAL and*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25802 = (int)*(((s1_ptr)_2)->base + _s_48914);
    _2 = (int)SEQ_PTR(_25802);
    _25803 = (int)*(((s1_ptr)_2)->base + 4);
    _25802 = NOVALUE;
    if (IS_ATOM_INT(_25803)) {
        _25804 = (_25803 == 5);
    }
    else {
        _25804 = binary_op(EQUALS, _25803, 5);
    }
    _25803 = NOVALUE;
    if (IS_ATOM_INT(_25804)) {
        if (_25804 == 0) {
            goto L3; // [38] 96
        }
    }
    else {
        if (DBL_PTR(_25804)->dbl == 0.0) {
            goto L3; // [38] 96
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25806 = (int)*(((s1_ptr)_2)->base + _s_48914);
    _2 = (int)SEQ_PTR(_25806);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _25807 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _25807 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _25806 = NOVALUE;
    if (IS_ATOM_INT(_25807)) {
        _25808 = (_25807 == _38current_file_no_16946);
    }
    else {
        _25808 = binary_op(EQUALS, _25807, _38current_file_no_16946);
    }
    _25807 = NOVALUE;
    if (_25808 == 0) {
        DeRef(_25808);
        _25808 = NOVALUE;
        goto L3; // [61] 96
    }
    else {
        if (!IS_ATOM_INT(_25808) && DBL_PTR(_25808)->dbl == 0.0){
            DeRef(_25808);
            _25808 = NOVALUE;
            goto L3; // [61] 96
        }
        DeRef(_25808);
        _25808 = NOVALUE;
    }
    DeRef(_25808);
    _25808 = NOVALUE;

    /** 			Hide(s)*/
    _55Hide(_s_48914);

    /** 			if SymTab[s][S_TOKEN] = VARIABLE then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25809 = (int)*(((s1_ptr)_2)->base + _s_48914);
    _2 = (int)SEQ_PTR(_25809);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _25810 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _25810 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _25809 = NOVALUE;
    if (binary_op_a(NOTEQ, _25810, -100)){
        _25810 = NOVALUE;
        goto L4; // [85] 95
    }
    _25810 = NOVALUE;

    /** 				LintCheck(s)*/
    _55LintCheck(_s_48914);
L4: 
L3: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25812 = (int)*(((s1_ptr)_2)->base + _s_48914);
    _2 = (int)SEQ_PTR(_25812);
    _s_48914 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_48914)){
        _s_48914 = (long)DBL_PTR(_s_48914)->dbl;
    }
    _25812 = NOVALUE;

    /** 	end while*/
    goto L1; // [114] 15
L2: 

    /** end procedure*/
    DeRef(_25804);
    _25804 = NOVALUE;
    return;
    ;
}


int _55sym_name(int _sym_48945)
{
    int _25815 = NOVALUE;
    int _25814 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48945)) {
        _1 = (long)(DBL_PTR(_sym_48945)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48945)) && (DBL_PTR(_sym_48945)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48945);
        _sym_48945 = _1;
    }

    /** 	return SymTab[sym][S_NAME]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25814 = (int)*(((s1_ptr)_2)->base + _sym_48945);
    _2 = (int)SEQ_PTR(_25814);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _25815 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _25815 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _25814 = NOVALUE;
    Ref(_25815);
    return _25815;
    ;
}


int _55sym_token(int _sym_48953)
{
    int _25817 = NOVALUE;
    int _25816 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48953)) {
        _1 = (long)(DBL_PTR(_sym_48953)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48953)) && (DBL_PTR(_sym_48953)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48953);
        _sym_48953 = _1;
    }

    /** 	return SymTab[sym][S_TOKEN]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25816 = (int)*(((s1_ptr)_2)->base + _sym_48953);
    _2 = (int)SEQ_PTR(_25816);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _25817 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _25817 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _25816 = NOVALUE;
    Ref(_25817);
    return _25817;
    ;
}


int _55sym_scope(int _sym_48961)
{
    int _25819 = NOVALUE;
    int _25818 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48961)) {
        _1 = (long)(DBL_PTR(_sym_48961)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48961)) && (DBL_PTR(_sym_48961)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48961);
        _sym_48961 = _1;
    }

    /** 	return SymTab[sym][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25818 = (int)*(((s1_ptr)_2)->base + _sym_48961);
    _2 = (int)SEQ_PTR(_25818);
    _25819 = (int)*(((s1_ptr)_2)->base + 4);
    _25818 = NOVALUE;
    Ref(_25819);
    return _25819;
    ;
}


int _55sym_mode(int _sym_48969)
{
    int _25821 = NOVALUE;
    int _25820 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48969)) {
        _1 = (long)(DBL_PTR(_sym_48969)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48969)) && (DBL_PTR(_sym_48969)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48969);
        _sym_48969 = _1;
    }

    /** 	return SymTab[sym][S_MODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25820 = (int)*(((s1_ptr)_2)->base + _sym_48969);
    _2 = (int)SEQ_PTR(_25820);
    _25821 = (int)*(((s1_ptr)_2)->base + 3);
    _25820 = NOVALUE;
    Ref(_25821);
    return _25821;
    ;
}


int _55sym_obj(int _sym_48977)
{
    int _25823 = NOVALUE;
    int _25822 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48977)) {
        _1 = (long)(DBL_PTR(_sym_48977)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48977)) && (DBL_PTR(_sym_48977)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48977);
        _sym_48977 = _1;
    }

    /** 	return SymTab[sym][S_OBJ]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25822 = (int)*(((s1_ptr)_2)->base + _sym_48977);
    _2 = (int)SEQ_PTR(_25822);
    _25823 = (int)*(((s1_ptr)_2)->base + 1);
    _25822 = NOVALUE;
    Ref(_25823);
    return _25823;
    ;
}


int _55sym_next(int _sym_48985)
{
    int _25825 = NOVALUE;
    int _25824 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48985)) {
        _1 = (long)(DBL_PTR(_sym_48985)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48985)) && (DBL_PTR(_sym_48985)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48985);
        _sym_48985 = _1;
    }

    /** 	return SymTab[sym][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25824 = (int)*(((s1_ptr)_2)->base + _sym_48985);
    _2 = (int)SEQ_PTR(_25824);
    _25825 = (int)*(((s1_ptr)_2)->base + 2);
    _25824 = NOVALUE;
    Ref(_25825);
    return _25825;
    ;
}


int _55sym_block(int _sym_48993)
{
    int _25827 = NOVALUE;
    int _25826 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_48993)) {
        _1 = (long)(DBL_PTR(_sym_48993)->dbl);
        if (UNIQUE(DBL_PTR(_sym_48993)) && (DBL_PTR(_sym_48993)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_48993);
        _sym_48993 = _1;
    }

    /** 	return SymTab[sym][S_BLOCK]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25826 = (int)*(((s1_ptr)_2)->base + _sym_48993);
    _2 = (int)SEQ_PTR(_25826);
    if (!IS_ATOM_INT(_38S_BLOCK_16618)){
        _25827 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_BLOCK_16618)->dbl));
    }
    else{
        _25827 = (int)*(((s1_ptr)_2)->base + _38S_BLOCK_16618);
    }
    _25826 = NOVALUE;
    Ref(_25827);
    return _25827;
    ;
}


int _55sym_next_in_block(int _sym_49001)
{
    int _25829 = NOVALUE;
    int _25828 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49001)) {
        _1 = (long)(DBL_PTR(_sym_49001)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49001)) && (DBL_PTR(_sym_49001)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49001);
        _sym_49001 = _1;
    }

    /** 	return SymTab[sym][S_NEXT_IN_BLOCK]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25828 = (int)*(((s1_ptr)_2)->base + _sym_49001);
    _2 = (int)SEQ_PTR(_25828);
    if (!IS_ATOM_INT(_38S_NEXT_IN_BLOCK_16590)){
        _25829 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NEXT_IN_BLOCK_16590)->dbl));
    }
    else{
        _25829 = (int)*(((s1_ptr)_2)->base + _38S_NEXT_IN_BLOCK_16590);
    }
    _25828 = NOVALUE;
    Ref(_25829);
    return _25829;
    ;
}


int _55sym_usage(int _sym_49009)
{
    int _25831 = NOVALUE;
    int _25830 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_49009)) {
        _1 = (long)(DBL_PTR(_sym_49009)->dbl);
        if (UNIQUE(DBL_PTR(_sym_49009)) && (DBL_PTR(_sym_49009)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_49009);
        _sym_49009 = _1;
    }

    /** 	return SymTab[sym][S_USAGE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25830 = (int)*(((s1_ptr)_2)->base + _sym_49009);
    _2 = (int)SEQ_PTR(_25830);
    _25831 = (int)*(((s1_ptr)_2)->base + 5);
    _25830 = NOVALUE;
    Ref(_25831);
    return _25831;
    ;
}


int _55calc_stack_required(int _sub_49017)
{
    int _required_49018 = NOVALUE;
    int _arg_49023 = NOVALUE;
    int _25851 = NOVALUE;
    int _25847 = NOVALUE;
    int _25845 = NOVALUE;
    int _25843 = NOVALUE;
    int _25842 = NOVALUE;
    int _25841 = NOVALUE;
    int _25840 = NOVALUE;
    int _25839 = NOVALUE;
    int _25837 = NOVALUE;
    int _25836 = NOVALUE;
    int _25834 = NOVALUE;
    int _25832 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sub_49017)) {
        _1 = (long)(DBL_PTR(_sub_49017)->dbl);
        if (UNIQUE(DBL_PTR(_sub_49017)) && (DBL_PTR(_sub_49017)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_49017);
        _sub_49017 = _1;
    }

    /** 	integer required = SymTab[sub][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25832 = (int)*(((s1_ptr)_2)->base + _sub_49017);
    _2 = (int)SEQ_PTR(_25832);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _required_49018 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _required_49018 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    if (!IS_ATOM_INT(_required_49018)){
        _required_49018 = (long)DBL_PTR(_required_49018)->dbl;
    }
    _25832 = NOVALUE;

    /** 	integer arg = SymTab[sub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25834 = (int)*(((s1_ptr)_2)->base + _sub_49017);
    _2 = (int)SEQ_PTR(_25834);
    _arg_49023 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_49023)){
        _arg_49023 = (long)DBL_PTR(_arg_49023)->dbl;
    }
    _25834 = NOVALUE;

    /** 	for i = 1 to required do*/
    _25836 = _required_49018;
    {
        int _i_49029;
        _i_49029 = 1;
L1: 
        if (_i_49029 > _25836){
            goto L2; // [40] 70
        }

        /** 		arg = SymTab[arg][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _25837 = (int)*(((s1_ptr)_2)->base + _arg_49023);
        _2 = (int)SEQ_PTR(_25837);
        _arg_49023 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_arg_49023)){
            _arg_49023 = (long)DBL_PTR(_arg_49023)->dbl;
        }
        _25837 = NOVALUE;

        /** 	end for*/
        _i_49029 = _i_49029 + 1;
        goto L1; // [65] 47
L2: 
        ;
    }

    /** 	while arg != 0 and SymTab[arg][S_SCOPE] <= SC_PRIVATE do*/
L3: 
    _25839 = (_arg_49023 != 0);
    if (_25839 == 0) {
        goto L4; // [79] 132
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25841 = (int)*(((s1_ptr)_2)->base + _arg_49023);
    _2 = (int)SEQ_PTR(_25841);
    _25842 = (int)*(((s1_ptr)_2)->base + 4);
    _25841 = NOVALUE;
    if (IS_ATOM_INT(_25842)) {
        _25843 = (_25842 <= 3);
    }
    else {
        _25843 = binary_op(LESSEQ, _25842, 3);
    }
    _25842 = NOVALUE;
    if (_25843 <= 0) {
        if (_25843 == 0) {
            DeRef(_25843);
            _25843 = NOVALUE;
            goto L4; // [102] 132
        }
        else {
            if (!IS_ATOM_INT(_25843) && DBL_PTR(_25843)->dbl == 0.0){
                DeRef(_25843);
                _25843 = NOVALUE;
                goto L4; // [102] 132
            }
            DeRef(_25843);
            _25843 = NOVALUE;
        }
    }
    DeRef(_25843);
    _25843 = NOVALUE;

    /** 		required += 1*/
    _required_49018 = _required_49018 + 1;

    /** 		arg = SymTab[arg][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25845 = (int)*(((s1_ptr)_2)->base + _arg_49023);
    _2 = (int)SEQ_PTR(_25845);
    _arg_49023 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_49023)){
        _arg_49023 = (long)DBL_PTR(_arg_49023)->dbl;
    }
    _25845 = NOVALUE;

    /** 	end while*/
    goto L3; // [129] 75
L4: 

    /** 	arg = SymTab[sub][S_TEMPS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25847 = (int)*(((s1_ptr)_2)->base + _sub_49017);
    _2 = (int)SEQ_PTR(_25847);
    if (!IS_ATOM_INT(_38S_TEMPS_16643)){
        _arg_49023 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
    }
    else{
        _arg_49023 = (int)*(((s1_ptr)_2)->base + _38S_TEMPS_16643);
    }
    if (!IS_ATOM_INT(_arg_49023)){
        _arg_49023 = (long)DBL_PTR(_arg_49023)->dbl;
    }
    _25847 = NOVALUE;

    /** 	while arg != 0 do*/
L5: 
    if (_arg_49023 == 0)
    goto L6; // [153] 184

    /** 		required += 1*/
    _required_49018 = _required_49018 + 1;

    /** 		arg = SymTab[arg][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25851 = (int)*(((s1_ptr)_2)->base + _arg_49023);
    _2 = (int)SEQ_PTR(_25851);
    _arg_49023 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_arg_49023)){
        _arg_49023 = (long)DBL_PTR(_arg_49023)->dbl;
    }
    _25851 = NOVALUE;

    /** 	end while*/
    goto L5; // [181] 153
L6: 

    /** 	return required*/
    DeRef(_25839);
    _25839 = NOVALUE;
    return _required_49018;
    ;
}



// 0x6141447B
