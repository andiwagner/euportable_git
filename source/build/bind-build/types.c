// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _9char_test(int _test_data_473, int _char_set_474)
{
    int _lChr_475 = NOVALUE;
    int _134 = NOVALUE;
    int _133 = NOVALUE;
    int _132 = NOVALUE;
    int _131 = NOVALUE;
    int _130 = NOVALUE;
    int _129 = NOVALUE;
    int _128 = NOVALUE;
    int _127 = NOVALUE;
    int _126 = NOVALUE;
    int _125 = NOVALUE;
    int _124 = NOVALUE;
    int _121 = NOVALUE;
    int _120 = NOVALUE;
    int _119 = NOVALUE;
    int _118 = NOVALUE;
    int _116 = NOVALUE;
    int _114 = NOVALUE;
    int _113 = NOVALUE;
    int _112 = NOVALUE;
    int _111 = NOVALUE;
    int _110 = NOVALUE;
    int _109 = NOVALUE;
    int _108 = NOVALUE;
    int _107 = NOVALUE;
    int _106 = NOVALUE;
    int _105 = NOVALUE;
    int _104 = NOVALUE;
    int _103 = NOVALUE;
    int _102 = NOVALUE;
    int _101 = NOVALUE;
    int _100 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(test_data) then*/
    if (IS_ATOM_INT(_test_data_473))
    _100 = 1;
    else if (IS_ATOM_DBL(_test_data_473))
    _100 = IS_ATOM_INT(DoubleToInt(_test_data_473));
    else
    _100 = 0;
    if (_100 == 0)
    {
        _100 = NOVALUE;
        goto L1; // [8] 115
    }
    else{
        _100 = NOVALUE;
    }

    /** 		if sequence(char_set[1]) then*/
    _2 = (int)SEQ_PTR(_char_set_474);
    _101 = (int)*(((s1_ptr)_2)->base + 1);
    _102 = IS_SEQUENCE(_101);
    _101 = NOVALUE;
    if (_102 == 0)
    {
        _102 = NOVALUE;
        goto L2; // [20] 96
    }
    else{
        _102 = NOVALUE;
    }

    /** 			for j = 1 to length(char_set) do*/
    if (IS_SEQUENCE(_char_set_474)){
            _103 = SEQ_PTR(_char_set_474)->length;
    }
    else {
        _103 = 1;
    }
    {
        int _j_482;
        _j_482 = 1;
L3: 
        if (_j_482 > _103){
            goto L4; // [28] 85
        }

        /** 				if test_data >= char_set[j][1] and test_data <= char_set[j][2] then */
        _2 = (int)SEQ_PTR(_char_set_474);
        _104 = (int)*(((s1_ptr)_2)->base + _j_482);
        _2 = (int)SEQ_PTR(_104);
        _105 = (int)*(((s1_ptr)_2)->base + 1);
        _104 = NOVALUE;
        if (IS_ATOM_INT(_test_data_473) && IS_ATOM_INT(_105)) {
            _106 = (_test_data_473 >= _105);
        }
        else {
            _106 = binary_op(GREATEREQ, _test_data_473, _105);
        }
        _105 = NOVALUE;
        if (IS_ATOM_INT(_106)) {
            if (_106 == 0) {
                goto L5; // [49] 78
            }
        }
        else {
            if (DBL_PTR(_106)->dbl == 0.0) {
                goto L5; // [49] 78
            }
        }
        _2 = (int)SEQ_PTR(_char_set_474);
        _108 = (int)*(((s1_ptr)_2)->base + _j_482);
        _2 = (int)SEQ_PTR(_108);
        _109 = (int)*(((s1_ptr)_2)->base + 2);
        _108 = NOVALUE;
        if (IS_ATOM_INT(_test_data_473) && IS_ATOM_INT(_109)) {
            _110 = (_test_data_473 <= _109);
        }
        else {
            _110 = binary_op(LESSEQ, _test_data_473, _109);
        }
        _109 = NOVALUE;
        if (_110 == 0) {
            DeRef(_110);
            _110 = NOVALUE;
            goto L5; // [66] 78
        }
        else {
            if (!IS_ATOM_INT(_110) && DBL_PTR(_110)->dbl == 0.0){
                DeRef(_110);
                _110 = NOVALUE;
                goto L5; // [66] 78
            }
            DeRef(_110);
            _110 = NOVALUE;
        }
        DeRef(_110);
        _110 = NOVALUE;

        /** 					return TRUE */
        DeRef(_test_data_473);
        DeRefDS(_char_set_474);
        DeRef(_106);
        _106 = NOVALUE;
        return _9TRUE_428;
L5: 

        /** 			end for*/
        _j_482 = _j_482 + 1;
        goto L3; // [80] 35
L4: 
        ;
    }

    /** 			return FALSE*/
    DeRef(_test_data_473);
    DeRefDS(_char_set_474);
    DeRef(_106);
    _106 = NOVALUE;
    return _9FALSE_426;
    goto L6; // [93] 328
L2: 

    /** 			return find(test_data, char_set) > 0*/
    _111 = find_from(_test_data_473, _char_set_474, 1);
    _112 = (_111 > 0);
    _111 = NOVALUE;
    DeRef(_test_data_473);
    DeRefDS(_char_set_474);
    DeRef(_106);
    _106 = NOVALUE;
    return _112;
    goto L6; // [112] 328
L1: 

    /** 	elsif sequence(test_data) then*/
    _113 = IS_SEQUENCE(_test_data_473);
    if (_113 == 0)
    {
        _113 = NOVALUE;
        goto L7; // [120] 319
    }
    else{
        _113 = NOVALUE;
    }

    /** 		if length(test_data) = 0 then */
    if (IS_SEQUENCE(_test_data_473)){
            _114 = SEQ_PTR(_test_data_473)->length;
    }
    else {
        _114 = 1;
    }
    if (_114 != 0)
    goto L8; // [128] 141

    /** 			return FALSE */
    DeRef(_test_data_473);
    DeRefDS(_char_set_474);
    DeRef(_112);
    _112 = NOVALUE;
    DeRef(_106);
    _106 = NOVALUE;
    return _9FALSE_426;
L8: 

    /** 		for i = 1 to length(test_data) label "NXTCHR" do*/
    if (IS_SEQUENCE(_test_data_473)){
            _116 = SEQ_PTR(_test_data_473)->length;
    }
    else {
        _116 = 1;
    }
    {
        int _i_501;
        _i_501 = 1;
L9: 
        if (_i_501 > _116){
            goto LA; // [146] 308
        }

        /** 			if sequence(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_473);
        _118 = (int)*(((s1_ptr)_2)->base + _i_501);
        _119 = IS_SEQUENCE(_118);
        _118 = NOVALUE;
        if (_119 == 0)
        {
            _119 = NOVALUE;
            goto LB; // [162] 174
        }
        else{
            _119 = NOVALUE;
        }

        /** 				return FALSE*/
        DeRef(_test_data_473);
        DeRefDS(_char_set_474);
        DeRef(_112);
        _112 = NOVALUE;
        DeRef(_106);
        _106 = NOVALUE;
        return _9FALSE_426;
LB: 

        /** 			if not integer(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_473);
        _120 = (int)*(((s1_ptr)_2)->base + _i_501);
        if (IS_ATOM_INT(_120))
        _121 = 1;
        else if (IS_ATOM_DBL(_120))
        _121 = IS_ATOM_INT(DoubleToInt(_120));
        else
        _121 = 0;
        _120 = NOVALUE;
        if (_121 != 0)
        goto LC; // [183] 195
        _121 = NOVALUE;

        /** 				return FALSE*/
        DeRef(_test_data_473);
        DeRefDS(_char_set_474);
        DeRef(_112);
        _112 = NOVALUE;
        DeRef(_106);
        _106 = NOVALUE;
        return _9FALSE_426;
LC: 

        /** 			lChr = test_data[i]*/
        _2 = (int)SEQ_PTR(_test_data_473);
        _lChr_475 = (int)*(((s1_ptr)_2)->base + _i_501);
        if (!IS_ATOM_INT(_lChr_475)){
            _lChr_475 = (long)DBL_PTR(_lChr_475)->dbl;
        }

        /** 			if sequence(char_set[1]) then*/
        _2 = (int)SEQ_PTR(_char_set_474);
        _124 = (int)*(((s1_ptr)_2)->base + 1);
        _125 = IS_SEQUENCE(_124);
        _124 = NOVALUE;
        if (_125 == 0)
        {
            _125 = NOVALUE;
            goto LD; // [212] 276
        }
        else{
            _125 = NOVALUE;
        }

        /** 				for j = 1 to length(char_set) do*/
        if (IS_SEQUENCE(_char_set_474)){
                _126 = SEQ_PTR(_char_set_474)->length;
        }
        else {
            _126 = 1;
        }
        {
            int _j_516;
            _j_516 = 1;
LE: 
            if (_j_516 > _126){
                goto LF; // [220] 273
            }

            /** 					if lChr >= char_set[j][1] and lChr <= char_set[j][2] then*/
            _2 = (int)SEQ_PTR(_char_set_474);
            _127 = (int)*(((s1_ptr)_2)->base + _j_516);
            _2 = (int)SEQ_PTR(_127);
            _128 = (int)*(((s1_ptr)_2)->base + 1);
            _127 = NOVALUE;
            if (IS_ATOM_INT(_128)) {
                _129 = (_lChr_475 >= _128);
            }
            else {
                _129 = binary_op(GREATEREQ, _lChr_475, _128);
            }
            _128 = NOVALUE;
            if (IS_ATOM_INT(_129)) {
                if (_129 == 0) {
                    goto L10; // [241] 266
                }
            }
            else {
                if (DBL_PTR(_129)->dbl == 0.0) {
                    goto L10; // [241] 266
                }
            }
            _2 = (int)SEQ_PTR(_char_set_474);
            _131 = (int)*(((s1_ptr)_2)->base + _j_516);
            _2 = (int)SEQ_PTR(_131);
            _132 = (int)*(((s1_ptr)_2)->base + 2);
            _131 = NOVALUE;
            if (IS_ATOM_INT(_132)) {
                _133 = (_lChr_475 <= _132);
            }
            else {
                _133 = binary_op(LESSEQ, _lChr_475, _132);
            }
            _132 = NOVALUE;
            if (_133 == 0) {
                DeRef(_133);
                _133 = NOVALUE;
                goto L10; // [258] 266
            }
            else {
                if (!IS_ATOM_INT(_133) && DBL_PTR(_133)->dbl == 0.0){
                    DeRef(_133);
                    _133 = NOVALUE;
                    goto L10; // [258] 266
                }
                DeRef(_133);
                _133 = NOVALUE;
            }
            DeRef(_133);
            _133 = NOVALUE;

            /** 						continue "NXTCHR" */
            goto L11; // [263] 303
L10: 

            /** 				end for*/
            _j_516 = _j_516 + 1;
            goto LE; // [268] 227
LF: 
            ;
        }
        goto L12; // [273] 293
LD: 

        /** 				if find(lChr, char_set) > 0 then*/
        _134 = find_from(_lChr_475, _char_set_474, 1);
        if (_134 <= 0)
        goto L13; // [283] 292

        /** 					continue "NXTCHR"*/
        goto L11; // [289] 303
L13: 
L12: 

        /** 			return FALSE*/
        DeRef(_test_data_473);
        DeRefDS(_char_set_474);
        DeRef(_112);
        _112 = NOVALUE;
        DeRef(_106);
        _106 = NOVALUE;
        DeRef(_129);
        _129 = NOVALUE;
        return _9FALSE_426;

        /** 		end for*/
L11: 
        _i_501 = _i_501 + 1;
        goto L9; // [303] 153
LA: 
        ;
    }

    /** 		return TRUE*/
    DeRef(_test_data_473);
    DeRefDS(_char_set_474);
    DeRef(_112);
    _112 = NOVALUE;
    DeRef(_106);
    _106 = NOVALUE;
    DeRef(_129);
    _129 = NOVALUE;
    return _9TRUE_428;
    goto L6; // [316] 328
L7: 

    /** 		return FALSE*/
    DeRef(_test_data_473);
    DeRefDS(_char_set_474);
    DeRef(_112);
    _112 = NOVALUE;
    DeRef(_106);
    _106 = NOVALUE;
    DeRef(_129);
    _129 = NOVALUE;
    return _9FALSE_426;
L6: 
    ;
}


void _9set_default_charsets()
{
    int _202 = NOVALUE;
    int _200 = NOVALUE;
    int _199 = NOVALUE;
    int _197 = NOVALUE;
    int _194 = NOVALUE;
    int _193 = NOVALUE;
    int _192 = NOVALUE;
    int _191 = NOVALUE;
    int _188 = NOVALUE;
    int _186 = NOVALUE;
    int _175 = NOVALUE;
    int _168 = NOVALUE;
    int _164 = NOVALUE;
    int _156 = NOVALUE;
    int _153 = NOVALUE;
    int _152 = NOVALUE;
    int _151 = NOVALUE;
    int _148 = NOVALUE;
    int _145 = NOVALUE;
    int _137 = NOVALUE;
    int _136 = NOVALUE;
    int _0, _1, _2;
    

    /** 	Defined_Sets = repeat(0, CS_LAST - CS_FIRST - 1)*/
    _136 = 20;
    _137 = 19;
    _136 = NOVALUE;
    DeRef(_9Defined_Sets_531);
    _9Defined_Sets_531 = Repeat(0, 19);
    _137 = NOVALUE;

    /** 	Defined_Sets[CS_Alphabetic	] = {{'a', 'z'}, {'A', 'Z'}}*/
    RefDS(_144);
    RefDS(_141);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _141;
    ((int *)_2)[2] = _144;
    _145 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 12);
    *(int *)_2 = _145;
    if( _1 != _145 ){
    }
    _145 = NOVALUE;

    /** 	Defined_Sets[CS_Alphanumeric] = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_147);
    *((int *)(_2+4)) = _147;
    RefDS(_141);
    *((int *)(_2+8)) = _141;
    RefDS(_144);
    *((int *)(_2+12)) = _144;
    _148 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = _148;
    if( _1 != _148 ){
        DeRef(_1);
    }
    _148 = NOVALUE;

    /** 	Defined_Sets[CS_Identifier]   = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}, {'_', '_'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_147);
    *((int *)(_2+4)) = _147;
    RefDS(_141);
    *((int *)(_2+8)) = _141;
    RefDS(_144);
    *((int *)(_2+12)) = _144;
    RefDS(_150);
    *((int *)(_2+16)) = _150;
    _151 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _151;
    if( _1 != _151 ){
        DeRef(_1);
    }
    _151 = NOVALUE;

    /** 	Defined_Sets[CS_Uppercase 	] = {{'A', 'Z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_144);
    *((int *)(_2+4)) = _144;
    _152 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _152;
    if( _1 != _152 ){
        DeRef(_1);
    }
    _152 = NOVALUE;

    /** 	Defined_Sets[CS_Lowercase 	] = {{'a', 'z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_141);
    *((int *)(_2+4)) = _141;
    _153 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _153;
    if( _1 != _153 ){
        DeRef(_1);
    }
    _153 = NOVALUE;

    /** 	Defined_Sets[CS_Printable 	] = {{' ', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_155);
    *((int *)(_2+4)) = _155;
    _156 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _156;
    if( _1 != _156 ){
        DeRef(_1);
    }
    _156 = NOVALUE;

    /** 	Defined_Sets[CS_Displayable ] = {{' ', '~'}, "  ", "\t\t", "\n\n", "\r\r", {8,8}, {7,7} }*/
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_155);
    *((int *)(_2+4)) = _155;
    RefDS(_157);
    *((int *)(_2+8)) = _157;
    RefDS(_158);
    *((int *)(_2+12)) = _158;
    RefDS(_159);
    *((int *)(_2+16)) = _159;
    RefDS(_160);
    *((int *)(_2+20)) = _160;
    RefDS(_161);
    *((int *)(_2+24)) = _161;
    RefDS(_163);
    *((int *)(_2+28)) = _163;
    _164 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _164;
    if( _1 != _164 ){
        DeRef(_1);
    }
    _164 = NOVALUE;

    /** 	Defined_Sets[CS_Whitespace 	] = " \t\n\r" & 11 & 160*/
    {
        int concat_list[3];

        concat_list[0] = 160;
        concat_list[1] = 11;
        concat_list[2] = _165;
        Concat_N((object_ptr)&_168, concat_list, 3);
    }
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _168;
    if( _1 != _168 ){
        DeRef(_1);
    }
    _168 = NOVALUE;

    /** 	Defined_Sets[CS_Consonant 	] = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"*/
    RefDS(_169);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _169;
    DeRef(_1);

    /** 	Defined_Sets[CS_Vowel 		] = "aeiouAEIOU"*/
    RefDS(_170);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _170;
    DeRef(_1);

    /** 	Defined_Sets[CS_Hexadecimal ] = {{'0', '9'}, {'A', 'F'},{'a', 'f'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_147);
    *((int *)(_2+4)) = _147;
    RefDS(_172);
    *((int *)(_2+8)) = _172;
    RefDS(_174);
    *((int *)(_2+12)) = _174;
    _175 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _175;
    if( _1 != _175 ){
        DeRef(_1);
    }
    _175 = NOVALUE;

    /** 	Defined_Sets[CS_Punctuation ] = {{' ', '/'}, {':', '?'}, {'[', '`'}, {'{', '~'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_177);
    *((int *)(_2+4)) = _177;
    RefDS(_180);
    *((int *)(_2+8)) = _180;
    RefDS(_183);
    *((int *)(_2+12)) = _183;
    RefDS(_185);
    *((int *)(_2+16)) = _185;
    _186 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _186;
    if( _1 != _186 ){
        DeRef(_1);
    }
    _186 = NOVALUE;

    /** 	Defined_Sets[CS_Control 	] = {{0, 31}, {127, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 31;
    _188 = MAKE_SEQ(_1);
    RefDS(_190);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _188;
    ((int *)_2)[2] = _190;
    _191 = MAKE_SEQ(_1);
    _188 = NOVALUE;
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = _191;
    if( _1 != _191 ){
        DeRef(_1);
    }
    _191 = NOVALUE;

    /** 	Defined_Sets[CS_ASCII 		] = {{0, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 127;
    _192 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _192;
    _193 = MAKE_SEQ(_1);
    _192 = NOVALUE;
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 13);
    _1 = *(int *)_2;
    *(int *)_2 = _193;
    if( _1 != _193 ){
        DeRef(_1);
    }
    _193 = NOVALUE;

    /** 	Defined_Sets[CS_Digit 		] = {{'0', '9'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_147);
    *((int *)(_2+4)) = _147;
    _194 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _194;
    if( _1 != _194 ){
        DeRef(_1);
    }
    _194 = NOVALUE;

    /** 	Defined_Sets[CS_Graphic 	] = {{'!', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_196);
    *((int *)(_2+4)) = _196;
    _197 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 16);
    _1 = *(int *)_2;
    *(int *)_2 = _197;
    if( _1 != _197 ){
        DeRef(_1);
    }
    _197 = NOVALUE;

    /** 	Defined_Sets[CS_Bytes	 	] = {{0, 255}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 255;
    _199 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _199;
    _200 = MAKE_SEQ(_1);
    _199 = NOVALUE;
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 17);
    _1 = *(int *)_2;
    *(int *)_2 = _200;
    if( _1 != _200 ){
        DeRef(_1);
    }
    _200 = NOVALUE;

    /** 	Defined_Sets[CS_SpecWord 	] = "_"*/
    RefDS(_201);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 18);
    _1 = *(int *)_2;
    *(int *)_2 = _201;
    DeRef(_1);

    /** 	Defined_Sets[CS_Boolean     ] = {TRUE,FALSE}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _9TRUE_428;
    ((int *)_2)[2] = _9FALSE_426;
    _202 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _2 = (int)(((s1_ptr)_2)->base + 19);
    _1 = *(int *)_2;
    *(int *)_2 = _202;
    if( _1 != _202 ){
        DeRef(_1);
    }
    _202 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _9get_charsets()
{
    int _result__603 = NOVALUE;
    int _206 = NOVALUE;
    int _205 = NOVALUE;
    int _204 = NOVALUE;
    int _203 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_ = {}*/
    RefDS(_5);
    DeRef(_result__603);
    _result__603 = _5;

    /** 	for i = CS_FIRST + 1 to CS_LAST - 1 do*/
    _203 = 1;
    _204 = 19;
    {
        int _i_605;
        _i_605 = 1;
L1: 
        if (_i_605 > 19){
            goto L2; // [22] 52
        }

        /** 		result_ = append(result_, {i, Defined_Sets[i]} )*/
        _2 = (int)SEQ_PTR(_9Defined_Sets_531);
        _205 = (int)*(((s1_ptr)_2)->base + _i_605);
        Ref(_205);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_605;
        ((int *)_2)[2] = _205;
        _206 = MAKE_SEQ(_1);
        _205 = NOVALUE;
        RefDS(_206);
        Append(&_result__603, _result__603, _206);
        DeRefDS(_206);
        _206 = NOVALUE;

        /** 	end for*/
        _i_605 = _i_605 + 1;
        goto L1; // [47] 29
L2: 
        ;
    }

    /** 	return result_*/
    DeRef(_203);
    _203 = NOVALUE;
    DeRef(_204);
    _204 = NOVALUE;
    return _result__603;
    ;
}


void _9set_charsets(int _charset_list_613)
{
    int _229 = NOVALUE;
    int _228 = NOVALUE;
    int _227 = NOVALUE;
    int _226 = NOVALUE;
    int _225 = NOVALUE;
    int _224 = NOVALUE;
    int _223 = NOVALUE;
    int _222 = NOVALUE;
    int _221 = NOVALUE;
    int _220 = NOVALUE;
    int _219 = NOVALUE;
    int _218 = NOVALUE;
    int _217 = NOVALUE;
    int _216 = NOVALUE;
    int _215 = NOVALUE;
    int _214 = NOVALUE;
    int _213 = NOVALUE;
    int _212 = NOVALUE;
    int _211 = NOVALUE;
    int _210 = NOVALUE;
    int _209 = NOVALUE;
    int _208 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(charset_list) do*/
    if (IS_SEQUENCE(_charset_list_613)){
            _208 = SEQ_PTR(_charset_list_613)->length;
    }
    else {
        _208 = 1;
    }
    {
        int _i_615;
        _i_615 = 1;
L1: 
        if (_i_615 > _208){
            goto L2; // [8] 133
        }

        /** 		if sequence(charset_list[i]) and length(charset_list[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_charset_list_613);
        _209 = (int)*(((s1_ptr)_2)->base + _i_615);
        _210 = IS_SEQUENCE(_209);
        _209 = NOVALUE;
        if (_210 == 0) {
            goto L3; // [24] 126
        }
        _2 = (int)SEQ_PTR(_charset_list_613);
        _212 = (int)*(((s1_ptr)_2)->base + _i_615);
        if (IS_SEQUENCE(_212)){
                _213 = SEQ_PTR(_212)->length;
        }
        else {
            _213 = 1;
        }
        _212 = NOVALUE;
        _214 = (_213 == 2);
        _213 = NOVALUE;
        if (_214 == 0)
        {
            DeRef(_214);
            _214 = NOVALUE;
            goto L3; // [40] 126
        }
        else{
            DeRef(_214);
            _214 = NOVALUE;
        }

        /** 			if integer(charset_list[i][1]) and charset_list[i][1] > CS_FIRST and charset_list[i][1] < CS_LAST then*/
        _2 = (int)SEQ_PTR(_charset_list_613);
        _215 = (int)*(((s1_ptr)_2)->base + _i_615);
        _2 = (int)SEQ_PTR(_215);
        _216 = (int)*(((s1_ptr)_2)->base + 1);
        _215 = NOVALUE;
        if (IS_ATOM_INT(_216))
        _217 = 1;
        else if (IS_ATOM_DBL(_216))
        _217 = IS_ATOM_INT(DoubleToInt(_216));
        else
        _217 = 0;
        _216 = NOVALUE;
        if (_217 == 0) {
            _218 = 0;
            goto L4; // [56] 78
        }
        _2 = (int)SEQ_PTR(_charset_list_613);
        _219 = (int)*(((s1_ptr)_2)->base + _i_615);
        _2 = (int)SEQ_PTR(_219);
        _220 = (int)*(((s1_ptr)_2)->base + 1);
        _219 = NOVALUE;
        if (IS_ATOM_INT(_220)) {
            _221 = (_220 > 0);
        }
        else {
            _221 = binary_op(GREATER, _220, 0);
        }
        _220 = NOVALUE;
        if (IS_ATOM_INT(_221))
        _218 = (_221 != 0);
        else
        _218 = DBL_PTR(_221)->dbl != 0.0;
L4: 
        if (_218 == 0) {
            goto L5; // [78] 125
        }
        _2 = (int)SEQ_PTR(_charset_list_613);
        _223 = (int)*(((s1_ptr)_2)->base + _i_615);
        _2 = (int)SEQ_PTR(_223);
        _224 = (int)*(((s1_ptr)_2)->base + 1);
        _223 = NOVALUE;
        if (IS_ATOM_INT(_224)) {
            _225 = (_224 < 20);
        }
        else {
            _225 = binary_op(LESS, _224, 20);
        }
        _224 = NOVALUE;
        if (_225 == 0) {
            DeRef(_225);
            _225 = NOVALUE;
            goto L5; // [97] 125
        }
        else {
            if (!IS_ATOM_INT(_225) && DBL_PTR(_225)->dbl == 0.0){
                DeRef(_225);
                _225 = NOVALUE;
                goto L5; // [97] 125
            }
            DeRef(_225);
            _225 = NOVALUE;
        }
        DeRef(_225);
        _225 = NOVALUE;

        /** 				Defined_Sets[charset_list[i][1]] = charset_list[i][2]*/
        _2 = (int)SEQ_PTR(_charset_list_613);
        _226 = (int)*(((s1_ptr)_2)->base + _i_615);
        _2 = (int)SEQ_PTR(_226);
        _227 = (int)*(((s1_ptr)_2)->base + 1);
        _226 = NOVALUE;
        _2 = (int)SEQ_PTR(_charset_list_613);
        _228 = (int)*(((s1_ptr)_2)->base + _i_615);
        _2 = (int)SEQ_PTR(_228);
        _229 = (int)*(((s1_ptr)_2)->base + 2);
        _228 = NOVALUE;
        Ref(_229);
        _2 = (int)SEQ_PTR(_9Defined_Sets_531);
        if (!IS_ATOM_INT(_227))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_227)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _227);
        _1 = *(int *)_2;
        *(int *)_2 = _229;
        if( _1 != _229 ){
            DeRef(_1);
        }
        _229 = NOVALUE;
L5: 
L3: 

        /** 	end for*/
        _i_615 = _i_615 + 1;
        goto L1; // [128] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_charset_list_613);
    _212 = NOVALUE;
    _227 = NOVALUE;
    DeRef(_221);
    _221 = NOVALUE;
    return;
    ;
}


int _9boolean(int _test_data_642)
{
    int _232 = NOVALUE;
    int _231 = NOVALUE;
    int _230 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return find(test_data,{1,0}) != 0*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _230 = MAKE_SEQ(_1);
    _231 = find_from(_test_data_642, _230, 1);
    DeRefDS(_230);
    _230 = NOVALUE;
    _232 = (_231 != 0);
    _231 = NOVALUE;
    DeRef(_test_data_642);
    return _232;
    ;
}


int _9t_boolean(int _test_data_648)
{
    int _234 = NOVALUE;
    int _233 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Boolean])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _233 = (int)*(((s1_ptr)_2)->base + 19);
    Ref(_test_data_648);
    Ref(_233);
    _234 = _9char_test(_test_data_648, _233);
    _233 = NOVALUE;
    DeRef(_test_data_648);
    return _234;
    ;
}


int _9t_alnum(int _test_data_653)
{
    int _236 = NOVALUE;
    int _235 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphanumeric])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _235 = (int)*(((s1_ptr)_2)->base + 10);
    Ref(_test_data_653);
    Ref(_235);
    _236 = _9char_test(_test_data_653, _235);
    _235 = NOVALUE;
    DeRef(_test_data_653);
    return _236;
    ;
}


int _9t_identifier(int _test_data_658)
{
    int _246 = NOVALUE;
    int _245 = NOVALUE;
    int _244 = NOVALUE;
    int _243 = NOVALUE;
    int _242 = NOVALUE;
    int _241 = NOVALUE;
    int _240 = NOVALUE;
    int _239 = NOVALUE;
    int _238 = NOVALUE;
    int _237 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if t_digit(test_data) then*/
    Ref(_test_data_658);
    _237 = _9t_digit(_test_data_658);
    if (_237 == 0) {
        DeRef(_237);
        _237 = NOVALUE;
        goto L1; // [7] 19
    }
    else {
        if (!IS_ATOM_INT(_237) && DBL_PTR(_237)->dbl == 0.0){
            DeRef(_237);
            _237 = NOVALUE;
            goto L1; // [7] 19
        }
        DeRef(_237);
        _237 = NOVALUE;
    }
    DeRef(_237);
    _237 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_658);
    return 0;
    goto L2; // [16] 63
L1: 

    /** 	elsif sequence(test_data) and length(test_data) > 0 and t_digit(test_data[1]) then*/
    _238 = IS_SEQUENCE(_test_data_658);
    if (_238 == 0) {
        _239 = 0;
        goto L3; // [24] 39
    }
    if (IS_SEQUENCE(_test_data_658)){
            _240 = SEQ_PTR(_test_data_658)->length;
    }
    else {
        _240 = 1;
    }
    _241 = (_240 > 0);
    _240 = NOVALUE;
    _239 = (_241 != 0);
L3: 
    if (_239 == 0) {
        goto L4; // [39] 62
    }
    _2 = (int)SEQ_PTR(_test_data_658);
    _243 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_243);
    _244 = _9t_digit(_243);
    _243 = NOVALUE;
    if (_244 == 0) {
        DeRef(_244);
        _244 = NOVALUE;
        goto L4; // [52] 62
    }
    else {
        if (!IS_ATOM_INT(_244) && DBL_PTR(_244)->dbl == 0.0){
            DeRef(_244);
            _244 = NOVALUE;
            goto L4; // [52] 62
        }
        DeRef(_244);
        _244 = NOVALUE;
    }
    DeRef(_244);
    _244 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_658);
    DeRef(_241);
    _241 = NOVALUE;
    return 0;
L4: 
L2: 

    /** 	return char_test(test_data, Defined_Sets[CS_Identifier])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _245 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_test_data_658);
    Ref(_245);
    _246 = _9char_test(_test_data_658, _245);
    _245 = NOVALUE;
    DeRef(_test_data_658);
    DeRef(_241);
    _241 = NOVALUE;
    return _246;
    ;
}


int _9t_alpha(int _test_data_675)
{
    int _248 = NOVALUE;
    int _247 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphabetic])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _247 = (int)*(((s1_ptr)_2)->base + 12);
    Ref(_test_data_675);
    Ref(_247);
    _248 = _9char_test(_test_data_675, _247);
    _247 = NOVALUE;
    DeRef(_test_data_675);
    return _248;
    ;
}


int _9t_ascii(int _test_data_680)
{
    int _250 = NOVALUE;
    int _249 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_ASCII])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _249 = (int)*(((s1_ptr)_2)->base + 13);
    Ref(_test_data_680);
    Ref(_249);
    _250 = _9char_test(_test_data_680, _249);
    _249 = NOVALUE;
    DeRef(_test_data_680);
    return _250;
    ;
}


int _9t_cntrl(int _test_data_685)
{
    int _252 = NOVALUE;
    int _251 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Control])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _251 = (int)*(((s1_ptr)_2)->base + 14);
    Ref(_test_data_685);
    Ref(_251);
    _252 = _9char_test(_test_data_685, _251);
    _251 = NOVALUE;
    DeRef(_test_data_685);
    return _252;
    ;
}


int _9t_digit(int _test_data_690)
{
    int _254 = NOVALUE;
    int _253 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Digit])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _253 = (int)*(((s1_ptr)_2)->base + 15);
    Ref(_test_data_690);
    Ref(_253);
    _254 = _9char_test(_test_data_690, _253);
    _253 = NOVALUE;
    DeRef(_test_data_690);
    return _254;
    ;
}


int _9t_graph(int _test_data_695)
{
    int _256 = NOVALUE;
    int _255 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Graphic])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _255 = (int)*(((s1_ptr)_2)->base + 16);
    Ref(_test_data_695);
    Ref(_255);
    _256 = _9char_test(_test_data_695, _255);
    _255 = NOVALUE;
    DeRef(_test_data_695);
    return _256;
    ;
}


int _9t_specword(int _test_data_700)
{
    int _258 = NOVALUE;
    int _257 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_SpecWord])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _257 = (int)*(((s1_ptr)_2)->base + 18);
    Ref(_test_data_700);
    Ref(_257);
    _258 = _9char_test(_test_data_700, _257);
    _257 = NOVALUE;
    DeRef(_test_data_700);
    return _258;
    ;
}


int _9t_bytearray(int _test_data_705)
{
    int _260 = NOVALUE;
    int _259 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Bytes])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _259 = (int)*(((s1_ptr)_2)->base + 17);
    Ref(_test_data_705);
    Ref(_259);
    _260 = _9char_test(_test_data_705, _259);
    _259 = NOVALUE;
    DeRef(_test_data_705);
    return _260;
    ;
}


int _9t_lower(int _test_data_710)
{
    int _262 = NOVALUE;
    int _261 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Lowercase])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _261 = (int)*(((s1_ptr)_2)->base + 8);
    Ref(_test_data_710);
    Ref(_261);
    _262 = _9char_test(_test_data_710, _261);
    _261 = NOVALUE;
    DeRef(_test_data_710);
    return _262;
    ;
}


int _9t_print(int _test_data_715)
{
    int _264 = NOVALUE;
    int _263 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Printable])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _263 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_test_data_715);
    Ref(_263);
    _264 = _9char_test(_test_data_715, _263);
    _263 = NOVALUE;
    DeRef(_test_data_715);
    return _264;
    ;
}


int _9t_display(int _test_data_720)
{
    int _266 = NOVALUE;
    int _265 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Displayable])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _265 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_test_data_720);
    Ref(_265);
    _266 = _9char_test(_test_data_720, _265);
    _265 = NOVALUE;
    DeRef(_test_data_720);
    return _266;
    ;
}


int _9t_punct(int _test_data_725)
{
    int _268 = NOVALUE;
    int _267 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Punctuation])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _267 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_test_data_725);
    Ref(_267);
    _268 = _9char_test(_test_data_725, _267);
    _267 = NOVALUE;
    DeRef(_test_data_725);
    return _268;
    ;
}


int _9t_space(int _test_data_730)
{
    int _270 = NOVALUE;
    int _269 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Whitespace])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _269 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_test_data_730);
    Ref(_269);
    _270 = _9char_test(_test_data_730, _269);
    _269 = NOVALUE;
    DeRef(_test_data_730);
    return _270;
    ;
}


int _9t_upper(int _test_data_735)
{
    int _272 = NOVALUE;
    int _271 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Uppercase])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _271 = (int)*(((s1_ptr)_2)->base + 9);
    Ref(_test_data_735);
    Ref(_271);
    _272 = _9char_test(_test_data_735, _271);
    _271 = NOVALUE;
    DeRef(_test_data_735);
    return _272;
    ;
}


int _9t_xdigit(int _test_data_740)
{
    int _274 = NOVALUE;
    int _273 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Hexadecimal])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _273 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_test_data_740);
    Ref(_273);
    _274 = _9char_test(_test_data_740, _273);
    _273 = NOVALUE;
    DeRef(_test_data_740);
    return _274;
    ;
}


int _9t_vowel(int _test_data_745)
{
    int _276 = NOVALUE;
    int _275 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Vowel])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _275 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_test_data_745);
    Ref(_275);
    _276 = _9char_test(_test_data_745, _275);
    _275 = NOVALUE;
    DeRef(_test_data_745);
    return _276;
    ;
}


int _9t_consonant(int _test_data_750)
{
    int _278 = NOVALUE;
    int _277 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Consonant])*/
    _2 = (int)SEQ_PTR(_9Defined_Sets_531);
    _277 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_test_data_750);
    Ref(_277);
    _278 = _9char_test(_test_data_750, _277);
    _277 = NOVALUE;
    DeRef(_test_data_750);
    return _278;
    ;
}


int _9integer_array(int _x_755)
{
    int _283 = NOVALUE;
    int _282 = NOVALUE;
    int _281 = NOVALUE;
    int _279 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _279 = IS_SEQUENCE(_x_755);
    if (_279 != 0)
    goto L1; // [6] 16
    _279 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_755);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_755)){
            _281 = SEQ_PTR(_x_755)->length;
    }
    else {
        _281 = 1;
    }
    {
        int _i_760;
        _i_760 = 1;
L2: 
        if (_i_760 > _281){
            goto L3; // [21] 54
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_755);
        _282 = (int)*(((s1_ptr)_2)->base + _i_760);
        if (IS_ATOM_INT(_282))
        _283 = 1;
        else if (IS_ATOM_DBL(_282))
        _283 = IS_ATOM_INT(DoubleToInt(_282));
        else
        _283 = 0;
        _282 = NOVALUE;
        if (_283 != 0)
        goto L4; // [37] 47
        _283 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_755);
        return 0;
L4: 

        /** 	end for*/
        _i_760 = _i_760 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_755);
    return 1;
    ;
}


int _9t_text(int _x_768)
{
    int _291 = NOVALUE;
    int _289 = NOVALUE;
    int _288 = NOVALUE;
    int _287 = NOVALUE;
    int _285 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _285 = IS_SEQUENCE(_x_768);
    if (_285 != 0)
    goto L1; // [6] 16
    _285 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_768);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_768)){
            _287 = SEQ_PTR(_x_768)->length;
    }
    else {
        _287 = 1;
    }
    {
        int _i_773;
        _i_773 = 1;
L2: 
        if (_i_773 > _287){
            goto L3; // [21] 71
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_768);
        _288 = (int)*(((s1_ptr)_2)->base + _i_773);
        if (IS_ATOM_INT(_288))
        _289 = 1;
        else if (IS_ATOM_DBL(_288))
        _289 = IS_ATOM_INT(DoubleToInt(_288));
        else
        _289 = 0;
        _288 = NOVALUE;
        if (_289 != 0)
        goto L4; // [37] 47
        _289 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_768);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_768);
        _291 = (int)*(((s1_ptr)_2)->base + _i_773);
        if (binary_op_a(GREATEREQ, _291, 0)){
            _291 = NOVALUE;
            goto L5; // [53] 64
        }
        _291 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_768);
        return 0;
L5: 

        /** 	end for*/
        _i_773 = _i_773 + 1;
        goto L2; // [66] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_768);
    return 1;
    ;
}


int _9number_array(int _x_784)
{
    int _297 = NOVALUE;
    int _296 = NOVALUE;
    int _295 = NOVALUE;
    int _293 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _293 = IS_SEQUENCE(_x_784);
    if (_293 != 0)
    goto L1; // [6] 16
    _293 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_784);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_784)){
            _295 = SEQ_PTR(_x_784)->length;
    }
    else {
        _295 = 1;
    }
    {
        int _i_789;
        _i_789 = 1;
L2: 
        if (_i_789 > _295){
            goto L3; // [21] 54
        }

        /** 		if not atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_784);
        _296 = (int)*(((s1_ptr)_2)->base + _i_789);
        _297 = IS_ATOM(_296);
        _296 = NOVALUE;
        if (_297 != 0)
        goto L4; // [37] 47
        _297 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_784);
        return 0;
L4: 

        /** 	end for*/
        _i_789 = _i_789 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_784);
    return 1;
    ;
}


int _9sequence_array(int _x_797)
{
    int _303 = NOVALUE;
    int _302 = NOVALUE;
    int _301 = NOVALUE;
    int _299 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _299 = IS_SEQUENCE(_x_797);
    if (_299 != 0)
    goto L1; // [6] 16
    _299 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_797);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_797)){
            _301 = SEQ_PTR(_x_797)->length;
    }
    else {
        _301 = 1;
    }
    {
        int _i_802;
        _i_802 = 1;
L2: 
        if (_i_802 > _301){
            goto L3; // [21] 54
        }

        /** 		if not sequence(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_797);
        _302 = (int)*(((s1_ptr)_2)->base + _i_802);
        _303 = IS_SEQUENCE(_302);
        _302 = NOVALUE;
        if (_303 != 0)
        goto L4; // [37] 47
        _303 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_797);
        return 0;
L4: 

        /** 	end for*/
        _i_802 = _i_802 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_797);
    return 1;
    ;
}


int _9ascii_string(int _x_810)
{
    int _313 = NOVALUE;
    int _311 = NOVALUE;
    int _309 = NOVALUE;
    int _308 = NOVALUE;
    int _307 = NOVALUE;
    int _305 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _305 = IS_SEQUENCE(_x_810);
    if (_305 != 0)
    goto L1; // [6] 16
    _305 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_810);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_810)){
            _307 = SEQ_PTR(_x_810)->length;
    }
    else {
        _307 = 1;
    }
    {
        int _i_815;
        _i_815 = 1;
L2: 
        if (_i_815 > _307){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_810);
        _308 = (int)*(((s1_ptr)_2)->base + _i_815);
        if (IS_ATOM_INT(_308))
        _309 = 1;
        else if (IS_ATOM_DBL(_308))
        _309 = IS_ATOM_INT(DoubleToInt(_308));
        else
        _309 = 0;
        _308 = NOVALUE;
        if (_309 != 0)
        goto L4; // [37] 47
        _309 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_810);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_810);
        _311 = (int)*(((s1_ptr)_2)->base + _i_815);
        if (binary_op_a(GREATEREQ, _311, 0)){
            _311 = NOVALUE;
            goto L5; // [53] 64
        }
        _311 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_810);
        return 0;
L5: 

        /** 		if x[i] > 127 then*/
        _2 = (int)SEQ_PTR(_x_810);
        _313 = (int)*(((s1_ptr)_2)->base + _i_815);
        if (binary_op_a(LESSEQ, _313, 127)){
            _313 = NOVALUE;
            goto L6; // [70] 81
        }
        _313 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_810);
        return 0;
L6: 

        /** 	end for*/
        _i_815 = _i_815 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_810);
    return 1;
    ;
}


int _9string(int _x_829)
{
    int _323 = NOVALUE;
    int _321 = NOVALUE;
    int _319 = NOVALUE;
    int _318 = NOVALUE;
    int _317 = NOVALUE;
    int _315 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _315 = IS_SEQUENCE(_x_829);
    if (_315 != 0)
    goto L1; // [6] 16
    _315 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_829);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_829)){
            _317 = SEQ_PTR(_x_829)->length;
    }
    else {
        _317 = 1;
    }
    {
        int _i_834;
        _i_834 = 1;
L2: 
        if (_i_834 > _317){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_829);
        _318 = (int)*(((s1_ptr)_2)->base + _i_834);
        if (IS_ATOM_INT(_318))
        _319 = 1;
        else if (IS_ATOM_DBL(_318))
        _319 = IS_ATOM_INT(DoubleToInt(_318));
        else
        _319 = 0;
        _318 = NOVALUE;
        if (_319 != 0)
        goto L4; // [37] 47
        _319 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_829);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_829);
        _321 = (int)*(((s1_ptr)_2)->base + _i_834);
        if (binary_op_a(GREATEREQ, _321, 0)){
            _321 = NOVALUE;
            goto L5; // [53] 64
        }
        _321 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_829);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_829);
        _323 = (int)*(((s1_ptr)_2)->base + _i_834);
        if (binary_op_a(LESSEQ, _323, 255)){
            _323 = NOVALUE;
            goto L6; // [70] 81
        }
        _323 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_829);
        return 0;
L6: 

        /** 	end for*/
        _i_834 = _i_834 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_829);
    return 1;
    ;
}


int _9cstring(int _x_848)
{
    int _333 = NOVALUE;
    int _331 = NOVALUE;
    int _329 = NOVALUE;
    int _328 = NOVALUE;
    int _327 = NOVALUE;
    int _325 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _325 = IS_SEQUENCE(_x_848);
    if (_325 != 0)
    goto L1; // [6] 16
    _325 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_848);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_848)){
            _327 = SEQ_PTR(_x_848)->length;
    }
    else {
        _327 = 1;
    }
    {
        int _i_853;
        _i_853 = 1;
L2: 
        if (_i_853 > _327){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_848);
        _328 = (int)*(((s1_ptr)_2)->base + _i_853);
        if (IS_ATOM_INT(_328))
        _329 = 1;
        else if (IS_ATOM_DBL(_328))
        _329 = IS_ATOM_INT(DoubleToInt(_328));
        else
        _329 = 0;
        _328 = NOVALUE;
        if (_329 != 0)
        goto L4; // [37] 47
        _329 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_848);
        return 0;
L4: 

        /** 		if x[i] <= 0 then*/
        _2 = (int)SEQ_PTR(_x_848);
        _331 = (int)*(((s1_ptr)_2)->base + _i_853);
        if (binary_op_a(GREATER, _331, 0)){
            _331 = NOVALUE;
            goto L5; // [53] 64
        }
        _331 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_848);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_848);
        _333 = (int)*(((s1_ptr)_2)->base + _i_853);
        if (binary_op_a(LESSEQ, _333, 255)){
            _333 = NOVALUE;
            goto L6; // [70] 81
        }
        _333 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_848);
        return 0;
L6: 

        /** 	end for*/
        _i_853 = _i_853 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_848);
    return 1;
    ;
}



// 0x528B43FA
