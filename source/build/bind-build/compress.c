// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _36compress(int _x_15769)
{
    int _x4_15770 = NOVALUE;
    int _s_15771 = NOVALUE;
    int _9054 = NOVALUE;
    int _9053 = NOVALUE;
    int _9052 = NOVALUE;
    int _9050 = NOVALUE;
    int _9049 = NOVALUE;
    int _9047 = NOVALUE;
    int _9045 = NOVALUE;
    int _9044 = NOVALUE;
    int _9043 = NOVALUE;
    int _9042 = NOVALUE;
    int _9040 = NOVALUE;
    int _9038 = NOVALUE;
    int _9037 = NOVALUE;
    int _9036 = NOVALUE;
    int _9035 = NOVALUE;
    int _9034 = NOVALUE;
    int _9033 = NOVALUE;
    int _9032 = NOVALUE;
    int _9031 = NOVALUE;
    int _9030 = NOVALUE;
    int _9028 = NOVALUE;
    int _9027 = NOVALUE;
    int _9026 = NOVALUE;
    int _9025 = NOVALUE;
    int _9024 = NOVALUE;
    int _9023 = NOVALUE;
    int _9021 = NOVALUE;
    int _9020 = NOVALUE;
    int _9019 = NOVALUE;
    int _9018 = NOVALUE;
    int _9017 = NOVALUE;
    int _9016 = NOVALUE;
    int _9015 = NOVALUE;
    int _9014 = NOVALUE;
    int _9013 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_15769))
    _9013 = 1;
    else if (IS_ATOM_DBL(_x_15769))
    _9013 = IS_ATOM_INT(DoubleToInt(_x_15769));
    else
    _9013 = 0;
    if (_9013 == 0)
    {
        _9013 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _9013 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_15769)) {
        _9014 = (_x_15769 >= -2);
    }
    else {
        _9014 = binary_op(GREATEREQ, _x_15769, -2);
    }
    if (IS_ATOM_INT(_9014)) {
        if (_9014 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_9014)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_15769)) {
        _9016 = (_x_15769 <= 246);
    }
    else {
        _9016 = binary_op(LESSEQ, _x_15769, 246);
    }
    if (_9016 == 0) {
        DeRef(_9016);
        _9016 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_9016) && DBL_PTR(_9016)->dbl == 0.0){
            DeRef(_9016);
            _9016 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_9016);
        _9016 = NOVALUE;
    }
    DeRef(_9016);
    _9016 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_15769)) {
        _9017 = _x_15769 - -2;
        if ((long)((unsigned long)_9017 +(unsigned long) HIGH_BITS) >= 0){
            _9017 = NewDouble((double)_9017);
        }
    }
    else {
        _9017 = binary_op(MINUS, _x_15769, -2);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9017;
    _9018 = MAKE_SEQ(_1);
    _9017 = NOVALUE;
    DeRef(_x_15769);
    DeRef(_x4_15770);
    DeRef(_s_15771);
    DeRef(_9014);
    _9014 = NOVALUE;
    return _9018;
    goto L3; // [41] 319
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_15769)) {
        _9019 = (_x_15769 >= _36MIN2B_15752);
    }
    else {
        _9019 = binary_op(GREATEREQ, _x_15769, _36MIN2B_15752);
    }
    if (IS_ATOM_INT(_9019)) {
        if (_9019 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_9019)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_15769)) {
        _9021 = (_x_15769 <= 32767);
    }
    else {
        _9021 = binary_op(LESSEQ, _x_15769, 32767);
    }
    if (_9021 == 0) {
        DeRef(_9021);
        _9021 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_9021) && DBL_PTR(_9021)->dbl == 0.0){
            DeRef(_9021);
            _9021 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_9021);
        _9021 = NOVALUE;
    }
    DeRef(_9021);
    _9021 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_15769;
    if (IS_ATOM_INT(_x_15769)) {
        _x_15769 = _x_15769 - _36MIN2B_15752;
        if ((long)((unsigned long)_x_15769 +(unsigned long) HIGH_BITS) >= 0){
            _x_15769 = NewDouble((double)_x_15769);
        }
    }
    else {
        _x_15769 = binary_op(MINUS, _x_15769, _36MIN2B_15752);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_15769)) {
        {unsigned long tu;
             tu = (unsigned long)_x_15769 & (unsigned long)255;
             _9023 = MAKE_UINT(tu);
        }
    }
    else {
        _9023 = binary_op(AND_BITS, _x_15769, 255);
    }
    if (IS_ATOM_INT(_x_15769)) {
        if (256 > 0 && _x_15769 >= 0) {
            _9024 = _x_15769 / 256;
        }
        else {
            temp_dbl = floor((double)_x_15769 / (double)256);
            if (_x_15769 != MININT)
            _9024 = (long)temp_dbl;
            else
            _9024 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15769, 256);
        _9024 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _9023;
    *((int *)(_2+12)) = _9024;
    _9025 = MAKE_SEQ(_1);
    _9024 = NOVALUE;
    _9023 = NOVALUE;
    DeRef(_x_15769);
    DeRef(_x4_15770);
    DeRef(_s_15771);
    DeRef(_9014);
    _9014 = NOVALUE;
    DeRef(_9018);
    _9018 = NOVALUE;
    DeRef(_9019);
    _9019 = NOVALUE;
    return _9025;
    goto L3; // [94] 319
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_15769)) {
        _9026 = (_x_15769 >= _36MIN3B_15758);
    }
    else {
        _9026 = binary_op(GREATEREQ, _x_15769, _36MIN3B_15758);
    }
    if (IS_ATOM_INT(_9026)) {
        if (_9026 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_9026)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_15769)) {
        _9028 = (_x_15769 <= 8388607);
    }
    else {
        _9028 = binary_op(LESSEQ, _x_15769, 8388607);
    }
    if (_9028 == 0) {
        DeRef(_9028);
        _9028 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_9028) && DBL_PTR(_9028)->dbl == 0.0){
            DeRef(_9028);
            _9028 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_9028);
        _9028 = NOVALUE;
    }
    DeRef(_9028);
    _9028 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_15769;
    if (IS_ATOM_INT(_x_15769)) {
        _x_15769 = _x_15769 - _36MIN3B_15758;
        if ((long)((unsigned long)_x_15769 +(unsigned long) HIGH_BITS) >= 0){
            _x_15769 = NewDouble((double)_x_15769);
        }
    }
    else {
        _x_15769 = binary_op(MINUS, _x_15769, _36MIN3B_15758);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_15769)) {
        {unsigned long tu;
             tu = (unsigned long)_x_15769 & (unsigned long)255;
             _9030 = MAKE_UINT(tu);
        }
    }
    else {
        _9030 = binary_op(AND_BITS, _x_15769, 255);
    }
    if (IS_ATOM_INT(_x_15769)) {
        if (256 > 0 && _x_15769 >= 0) {
            _9031 = _x_15769 / 256;
        }
        else {
            temp_dbl = floor((double)_x_15769 / (double)256);
            if (_x_15769 != MININT)
            _9031 = (long)temp_dbl;
            else
            _9031 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15769, 256);
        _9031 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_9031)) {
        {unsigned long tu;
             tu = (unsigned long)_9031 & (unsigned long)255;
             _9032 = MAKE_UINT(tu);
        }
    }
    else {
        _9032 = binary_op(AND_BITS, _9031, 255);
    }
    DeRef(_9031);
    _9031 = NOVALUE;
    if (IS_ATOM_INT(_x_15769)) {
        if (65536 > 0 && _x_15769 >= 0) {
            _9033 = _x_15769 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_15769 / (double)65536);
            if (_x_15769 != MININT)
            _9033 = (long)temp_dbl;
            else
            _9033 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15769, 65536);
        _9033 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _9030;
    *((int *)(_2+12)) = _9032;
    *((int *)(_2+16)) = _9033;
    _9034 = MAKE_SEQ(_1);
    _9033 = NOVALUE;
    _9032 = NOVALUE;
    _9030 = NOVALUE;
    DeRef(_x_15769);
    DeRef(_x4_15770);
    DeRef(_s_15771);
    DeRef(_9014);
    _9014 = NOVALUE;
    DeRef(_9018);
    _9018 = NOVALUE;
    DeRef(_9019);
    _9019 = NOVALUE;
    DeRef(_9025);
    _9025 = NOVALUE;
    DeRef(_9026);
    _9026 = NOVALUE;
    return _9034;
    goto L3; // [156] 319
L5: 

    /** 			return I4B & int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_15769) && IS_ATOM_INT(_36MIN4B_15764)) {
        _9035 = _x_15769 - _36MIN4B_15764;
        if ((long)((unsigned long)_9035 +(unsigned long) HIGH_BITS) >= 0){
            _9035 = NewDouble((double)_9035);
        }
    }
    else {
        _9035 = binary_op(MINUS, _x_15769, _36MIN4B_15764);
    }
    _9036 = _11int_to_bytes(_9035);
    _9035 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_9036)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_9036)) {
        Prepend(&_9037, _9036, 251);
    }
    else {
        Concat((object_ptr)&_9037, 251, _9036);
    }
    DeRef(_9036);
    _9036 = NOVALUE;
    DeRef(_x_15769);
    DeRef(_x4_15770);
    DeRef(_s_15771);
    DeRef(_9014);
    _9014 = NOVALUE;
    DeRef(_9018);
    _9018 = NOVALUE;
    DeRef(_9019);
    _9019 = NOVALUE;
    DeRef(_9025);
    _9025 = NOVALUE;
    DeRef(_9026);
    _9026 = NOVALUE;
    DeRef(_9034);
    _9034 = NOVALUE;
    return _9037;
    goto L3; // [180] 319
L1: 

    /** 	elsif atom(x) then*/
    _9038 = IS_ATOM(_x_15769);
    if (_9038 == 0)
    {
        _9038 = NOVALUE;
        goto L6; // [188] 240
    }
    else{
        _9038 = NOVALUE;
    }

    /** 		x4 = atom_to_float32(x)*/
    Ref(_x_15769);
    _0 = _x4_15770;
    _x4_15770 = _11atom_to_float32(_x_15769);
    DeRef(_0);

    /** 		if x = float32_to_atom(x4) then*/
    RefDS(_x4_15770);
    _9040 = _11float32_to_atom(_x4_15770);
    if (binary_op_a(NOTEQ, _x_15769, _9040)){
        DeRef(_9040);
        _9040 = NOVALUE;
        goto L7; // [205] 222
    }
    DeRef(_9040);
    _9040 = NOVALUE;

    /** 			return F4B & x4*/
    Prepend(&_9042, _x4_15770, 252);
    DeRef(_x_15769);
    DeRefDS(_x4_15770);
    DeRef(_s_15771);
    DeRef(_9014);
    _9014 = NOVALUE;
    DeRef(_9018);
    _9018 = NOVALUE;
    DeRef(_9019);
    _9019 = NOVALUE;
    DeRef(_9025);
    _9025 = NOVALUE;
    DeRef(_9026);
    _9026 = NOVALUE;
    DeRef(_9034);
    _9034 = NOVALUE;
    DeRef(_9037);
    _9037 = NOVALUE;
    return _9042;
    goto L3; // [219] 319
L7: 

    /** 			return F8B & atom_to_float64(x)*/
    Ref(_x_15769);
    _9043 = _11atom_to_float64(_x_15769);
    if (IS_SEQUENCE(253) && IS_ATOM(_9043)) {
    }
    else if (IS_ATOM(253) && IS_SEQUENCE(_9043)) {
        Prepend(&_9044, _9043, 253);
    }
    else {
        Concat((object_ptr)&_9044, 253, _9043);
    }
    DeRef(_9043);
    _9043 = NOVALUE;
    DeRef(_x_15769);
    DeRef(_x4_15770);
    DeRef(_s_15771);
    DeRef(_9014);
    _9014 = NOVALUE;
    DeRef(_9018);
    _9018 = NOVALUE;
    DeRef(_9019);
    _9019 = NOVALUE;
    DeRef(_9025);
    _9025 = NOVALUE;
    DeRef(_9026);
    _9026 = NOVALUE;
    DeRef(_9034);
    _9034 = NOVALUE;
    DeRef(_9037);
    _9037 = NOVALUE;
    DeRef(_9042);
    _9042 = NOVALUE;
    return _9044;
    goto L3; // [237] 319
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_15769)){
            _9045 = SEQ_PTR(_x_15769)->length;
    }
    else {
        _9045 = 1;
    }
    if (_9045 > 255)
    goto L8; // [245] 261

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_15769)){
            _9047 = SEQ_PTR(_x_15769)->length;
    }
    else {
        _9047 = 1;
    }
    DeRef(_s_15771);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _9047;
    _s_15771 = MAKE_SEQ(_1);
    _9047 = NOVALUE;
    goto L9; // [258] 275
L8: 

    /** 			s = S4B & int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_15769)){
            _9049 = SEQ_PTR(_x_15769)->length;
    }
    else {
        _9049 = 1;
    }
    _9050 = _11int_to_bytes(_9049);
    _9049 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_9050)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_9050)) {
        Prepend(&_s_15771, _9050, 255);
    }
    else {
        Concat((object_ptr)&_s_15771, 255, _9050);
    }
    DeRef(_9050);
    _9050 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_15769)){
            _9052 = SEQ_PTR(_x_15769)->length;
    }
    else {
        _9052 = 1;
    }
    {
        int _i_15828;
        _i_15828 = 1;
LA: 
        if (_i_15828 > _9052){
            goto LB; // [280] 310
        }

        /** 			s &= compress(x[i])*/
        _2 = (int)SEQ_PTR(_x_15769);
        _9053 = (int)*(((s1_ptr)_2)->base + _i_15828);
        Ref(_9053);
        _9054 = _36compress(_9053);
        _9053 = NOVALUE;
        if (IS_SEQUENCE(_s_15771) && IS_ATOM(_9054)) {
            Ref(_9054);
            Append(&_s_15771, _s_15771, _9054);
        }
        else if (IS_ATOM(_s_15771) && IS_SEQUENCE(_9054)) {
        }
        else {
            Concat((object_ptr)&_s_15771, _s_15771, _9054);
        }
        DeRef(_9054);
        _9054 = NOVALUE;

        /** 		end for*/
        _i_15828 = _i_15828 + 1;
        goto LA; // [305] 287
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_15769);
    DeRef(_x4_15770);
    DeRef(_9014);
    _9014 = NOVALUE;
    DeRef(_9018);
    _9018 = NOVALUE;
    DeRef(_9019);
    _9019 = NOVALUE;
    DeRef(_9025);
    _9025 = NOVALUE;
    DeRef(_9026);
    _9026 = NOVALUE;
    DeRef(_9034);
    _9034 = NOVALUE;
    DeRef(_9037);
    _9037 = NOVALUE;
    DeRef(_9042);
    _9042 = NOVALUE;
    DeRef(_9044);
    _9044 = NOVALUE;
    return _s_15771;
L3: 
    ;
}


void _36init_compress()
{
    int _0, _1, _2;
    

    /** 	comp_cache = repeat({}, COMP_CACHE_SIZE)*/
    DeRef(_36comp_cache_15839);
    _36comp_cache_15839 = Repeat(_5, 64);

    /** end procedure*/
    return;
    ;
}


void _36fcompress(int _f_15845, int _x_15846)
{
    int _x4_15847 = NOVALUE;
    int _s_15848 = NOVALUE;
    int _p_15849 = NOVALUE;
    int _9106 = NOVALUE;
    int _9105 = NOVALUE;
    int _9104 = NOVALUE;
    int _9102 = NOVALUE;
    int _9101 = NOVALUE;
    int _9099 = NOVALUE;
    int _9097 = NOVALUE;
    int _9096 = NOVALUE;
    int _9095 = NOVALUE;
    int _9094 = NOVALUE;
    int _9092 = NOVALUE;
    int _9090 = NOVALUE;
    int _9089 = NOVALUE;
    int _9088 = NOVALUE;
    int _9087 = NOVALUE;
    int _9086 = NOVALUE;
    int _9085 = NOVALUE;
    int _9084 = NOVALUE;
    int _9083 = NOVALUE;
    int _9082 = NOVALUE;
    int _9080 = NOVALUE;
    int _9079 = NOVALUE;
    int _9078 = NOVALUE;
    int _9077 = NOVALUE;
    int _9076 = NOVALUE;
    int _9075 = NOVALUE;
    int _9073 = NOVALUE;
    int _9072 = NOVALUE;
    int _9071 = NOVALUE;
    int _9070 = NOVALUE;
    int _9069 = NOVALUE;
    int _9068 = NOVALUE;
    int _9066 = NOVALUE;
    int _9065 = NOVALUE;
    int _9064 = NOVALUE;
    int _9063 = NOVALUE;
    int _9062 = NOVALUE;
    int _9061 = NOVALUE;
    int _9060 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_f_15845)) {
        _1 = (long)(DBL_PTR(_f_15845)->dbl);
        if (UNIQUE(DBL_PTR(_f_15845)) && (DBL_PTR(_f_15845)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_f_15845);
        _f_15845 = _1;
    }

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_15846))
    _9060 = 1;
    else if (IS_ATOM_DBL(_x_15846))
    _9060 = IS_ATOM_INT(DoubleToInt(_x_15846));
    else
    _9060 = 0;
    if (_9060 == 0)
    {
        _9060 = NOVALUE;
        goto L1; // [8] 232
    }
    else{
        _9060 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= max1b then*/
    if (IS_ATOM_INT(_x_15846)) {
        _9061 = (_x_15846 >= -2);
    }
    else {
        _9061 = binary_op(GREATEREQ, _x_15846, -2);
    }
    if (IS_ATOM_INT(_9061)) {
        if (_9061 == 0) {
            goto L2; // [17] 43
        }
    }
    else {
        if (DBL_PTR(_9061)->dbl == 0.0) {
            goto L2; // [17] 43
        }
    }
    if (IS_ATOM_INT(_x_15846)) {
        _9063 = (_x_15846 <= 182);
    }
    else {
        _9063 = binary_op(LESSEQ, _x_15846, 182);
    }
    if (_9063 == 0) {
        DeRef(_9063);
        _9063 = NOVALUE;
        goto L2; // [28] 43
    }
    else {
        if (!IS_ATOM_INT(_9063) && DBL_PTR(_9063)->dbl == 0.0){
            DeRef(_9063);
            _9063 = NOVALUE;
            goto L2; // [28] 43
        }
        DeRef(_9063);
        _9063 = NOVALUE;
    }
    DeRef(_9063);
    _9063 = NOVALUE;

    /** 			puts(f, x - MIN1B) -- normal, quite small integer*/
    if (IS_ATOM_INT(_x_15846)) {
        _9064 = _x_15846 - -2;
        if ((long)((unsigned long)_9064 +(unsigned long) HIGH_BITS) >= 0){
            _9064 = NewDouble((double)_9064);
        }
    }
    else {
        _9064 = binary_op(MINUS, _x_15846, -2);
    }
    EPuts(_f_15845, _9064); // DJP 
    DeRef(_9064);
    _9064 = NOVALUE;
    goto L3; // [40] 362
L2: 

    /** 			p = 1 + and_bits(x, COMP_CACHE_SIZE-1)*/
    _9065 = 63;
    if (IS_ATOM_INT(_x_15846)) {
        {unsigned long tu;
             tu = (unsigned long)_x_15846 & (unsigned long)63;
             _9066 = MAKE_UINT(tu);
        }
    }
    else {
        _9066 = binary_op(AND_BITS, _x_15846, 63);
    }
    _9065 = NOVALUE;
    if (IS_ATOM_INT(_9066)) {
        _p_15849 = _9066 + 1;
    }
    else
    { // coercing _p_15849 to an integer 1
        _p_15849 = 1+(long)(DBL_PTR(_9066)->dbl);
        if( !IS_ATOM_INT(_p_15849) ){
            _p_15849 = (object)DBL_PTR(_p_15849)->dbl;
        }
    }
    DeRef(_9066);
    _9066 = NOVALUE;

    /** 			if equal(comp_cache[p], x) then*/
    _2 = (int)SEQ_PTR(_36comp_cache_15839);
    _9068 = (int)*(((s1_ptr)_2)->base + _p_15849);
    if (_9068 == _x_15846)
    _9069 = 1;
    else if (IS_ATOM_INT(_9068) && IS_ATOM_INT(_x_15846))
    _9069 = 0;
    else
    _9069 = (compare(_9068, _x_15846) == 0);
    _9068 = NOVALUE;
    if (_9069 == 0)
    {
        _9069 = NOVALUE;
        goto L4; // [69] 86
    }
    else{
        _9069 = NOVALUE;
    }

    /** 				puts(f, CACHE0 + p) -- output the cache slot number*/
    _9070 = 184 + _p_15849;
    if ((long)((unsigned long)_9070 + (unsigned long)HIGH_BITS) >= 0) 
    _9070 = NewDouble((double)_9070);
    EPuts(_f_15845, _9070); // DJP 
    DeRef(_9070);
    _9070 = NOVALUE;
    goto L3; // [83] 362
L4: 

    /** 				comp_cache[p] = x -- store it in cache slot p*/
    Ref(_x_15846);
    _2 = (int)SEQ_PTR(_36comp_cache_15839);
    _2 = (int)(((s1_ptr)_2)->base + _p_15849);
    _1 = *(int *)_2;
    *(int *)_2 = _x_15846;
    DeRef(_1);

    /** 				if x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_15846)) {
        _9071 = (_x_15846 >= _36MIN2B_15752);
    }
    else {
        _9071 = binary_op(GREATEREQ, _x_15846, _36MIN2B_15752);
    }
    if (IS_ATOM_INT(_9071)) {
        if (_9071 == 0) {
            goto L5; // [102] 146
        }
    }
    else {
        if (DBL_PTR(_9071)->dbl == 0.0) {
            goto L5; // [102] 146
        }
    }
    if (IS_ATOM_INT(_x_15846)) {
        _9073 = (_x_15846 <= 32767);
    }
    else {
        _9073 = binary_op(LESSEQ, _x_15846, 32767);
    }
    if (_9073 == 0) {
        DeRef(_9073);
        _9073 = NOVALUE;
        goto L5; // [113] 146
    }
    else {
        if (!IS_ATOM_INT(_9073) && DBL_PTR(_9073)->dbl == 0.0){
            DeRef(_9073);
            _9073 = NOVALUE;
            goto L5; // [113] 146
        }
        DeRef(_9073);
        _9073 = NOVALUE;
    }
    DeRef(_9073);
    _9073 = NOVALUE;

    /** 					x -= MIN2B*/
    _0 = _x_15846;
    if (IS_ATOM_INT(_x_15846)) {
        _x_15846 = _x_15846 - _36MIN2B_15752;
        if ((long)((unsigned long)_x_15846 +(unsigned long) HIGH_BITS) >= 0){
            _x_15846 = NewDouble((double)_x_15846);
        }
    }
    else {
        _x_15846 = binary_op(MINUS, _x_15846, _36MIN2B_15752);
    }
    DeRef(_0);

    /** 					puts(f, {I2B, and_bits(x, #FF), floor(x / #100)})*/
    if (IS_ATOM_INT(_x_15846)) {
        {unsigned long tu;
             tu = (unsigned long)_x_15846 & (unsigned long)255;
             _9075 = MAKE_UINT(tu);
        }
    }
    else {
        _9075 = binary_op(AND_BITS, _x_15846, 255);
    }
    if (IS_ATOM_INT(_x_15846)) {
        if (256 > 0 && _x_15846 >= 0) {
            _9076 = _x_15846 / 256;
        }
        else {
            temp_dbl = floor((double)_x_15846 / (double)256);
            if (_x_15846 != MININT)
            _9076 = (long)temp_dbl;
            else
            _9076 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15846, 256);
        _9076 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _9075;
    *((int *)(_2+12)) = _9076;
    _9077 = MAKE_SEQ(_1);
    _9076 = NOVALUE;
    _9075 = NOVALUE;
    EPuts(_f_15845, _9077); // DJP 
    DeRefDS(_9077);
    _9077 = NOVALUE;
    goto L3; // [143] 362
L5: 

    /** 				elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_15846)) {
        _9078 = (_x_15846 >= _36MIN3B_15758);
    }
    else {
        _9078 = binary_op(GREATEREQ, _x_15846, _36MIN3B_15758);
    }
    if (IS_ATOM_INT(_9078)) {
        if (_9078 == 0) {
            goto L6; // [154] 207
        }
    }
    else {
        if (DBL_PTR(_9078)->dbl == 0.0) {
            goto L6; // [154] 207
        }
    }
    if (IS_ATOM_INT(_x_15846)) {
        _9080 = (_x_15846 <= 8388607);
    }
    else {
        _9080 = binary_op(LESSEQ, _x_15846, 8388607);
    }
    if (_9080 == 0) {
        DeRef(_9080);
        _9080 = NOVALUE;
        goto L6; // [165] 207
    }
    else {
        if (!IS_ATOM_INT(_9080) && DBL_PTR(_9080)->dbl == 0.0){
            DeRef(_9080);
            _9080 = NOVALUE;
            goto L6; // [165] 207
        }
        DeRef(_9080);
        _9080 = NOVALUE;
    }
    DeRef(_9080);
    _9080 = NOVALUE;

    /** 					x -= MIN3B*/
    _0 = _x_15846;
    if (IS_ATOM_INT(_x_15846)) {
        _x_15846 = _x_15846 - _36MIN3B_15758;
        if ((long)((unsigned long)_x_15846 +(unsigned long) HIGH_BITS) >= 0){
            _x_15846 = NewDouble((double)_x_15846);
        }
    }
    else {
        _x_15846 = binary_op(MINUS, _x_15846, _36MIN3B_15758);
    }
    DeRef(_0);

    /** 					puts(f, {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)})*/
    if (IS_ATOM_INT(_x_15846)) {
        {unsigned long tu;
             tu = (unsigned long)_x_15846 & (unsigned long)255;
             _9082 = MAKE_UINT(tu);
        }
    }
    else {
        _9082 = binary_op(AND_BITS, _x_15846, 255);
    }
    if (IS_ATOM_INT(_x_15846)) {
        if (256 > 0 && _x_15846 >= 0) {
            _9083 = _x_15846 / 256;
        }
        else {
            temp_dbl = floor((double)_x_15846 / (double)256);
            if (_x_15846 != MININT)
            _9083 = (long)temp_dbl;
            else
            _9083 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15846, 256);
        _9083 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_9083)) {
        {unsigned long tu;
             tu = (unsigned long)_9083 & (unsigned long)255;
             _9084 = MAKE_UINT(tu);
        }
    }
    else {
        _9084 = binary_op(AND_BITS, _9083, 255);
    }
    DeRef(_9083);
    _9083 = NOVALUE;
    if (IS_ATOM_INT(_x_15846)) {
        if (65536 > 0 && _x_15846 >= 0) {
            _9085 = _x_15846 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_15846 / (double)65536);
            if (_x_15846 != MININT)
            _9085 = (long)temp_dbl;
            else
            _9085 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15846, 65536);
        _9085 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _9082;
    *((int *)(_2+12)) = _9084;
    *((int *)(_2+16)) = _9085;
    _9086 = MAKE_SEQ(_1);
    _9085 = NOVALUE;
    _9084 = NOVALUE;
    _9082 = NOVALUE;
    EPuts(_f_15845, _9086); // DJP 
    DeRefDS(_9086);
    _9086 = NOVALUE;
    goto L3; // [204] 362
L6: 

    /** 					puts(f, I4B & int_to_bytes(x-MIN4B))*/
    if (IS_ATOM_INT(_x_15846) && IS_ATOM_INT(_36MIN4B_15764)) {
        _9087 = _x_15846 - _36MIN4B_15764;
        if ((long)((unsigned long)_9087 +(unsigned long) HIGH_BITS) >= 0){
            _9087 = NewDouble((double)_9087);
        }
    }
    else {
        _9087 = binary_op(MINUS, _x_15846, _36MIN4B_15764);
    }
    _9088 = _11int_to_bytes(_9087);
    _9087 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_9088)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_9088)) {
        Prepend(&_9089, _9088, 251);
    }
    else {
        Concat((object_ptr)&_9089, 251, _9088);
    }
    DeRef(_9088);
    _9088 = NOVALUE;
    EPuts(_f_15845, _9089); // DJP 
    DeRefDS(_9089);
    _9089 = NOVALUE;
    goto L3; // [229] 362
L1: 

    /** 	elsif atom(x) then*/
    _9090 = IS_ATOM(_x_15846);
    if (_9090 == 0)
    {
        _9090 = NOVALUE;
        goto L7; // [237] 287
    }
    else{
        _9090 = NOVALUE;
    }

    /** 		x4 = atom_to_float32(x)*/
    Ref(_x_15846);
    _0 = _x4_15847;
    _x4_15847 = _11atom_to_float32(_x_15846);
    DeRef(_0);

    /** 		if x = float32_to_atom(x4) then*/
    RefDS(_x4_15847);
    _9092 = _11float32_to_atom(_x4_15847);
    if (binary_op_a(NOTEQ, _x_15846, _9092)){
        DeRef(_9092);
        _9092 = NOVALUE;
        goto L8; // [254] 270
    }
    DeRef(_9092);
    _9092 = NOVALUE;

    /** 			puts(f, F4B & x4)*/
    Prepend(&_9094, _x4_15847, 252);
    EPuts(_f_15845, _9094); // DJP 
    DeRefDS(_9094);
    _9094 = NOVALUE;
    goto L3; // [267] 362
L8: 

    /** 			puts(f, F8B & atom_to_float64(x))*/
    Ref(_x_15846);
    _9095 = _11atom_to_float64(_x_15846);
    if (IS_SEQUENCE(253) && IS_ATOM(_9095)) {
    }
    else if (IS_ATOM(253) && IS_SEQUENCE(_9095)) {
        Prepend(&_9096, _9095, 253);
    }
    else {
        Concat((object_ptr)&_9096, 253, _9095);
    }
    DeRef(_9095);
    _9095 = NOVALUE;
    EPuts(_f_15845, _9096); // DJP 
    DeRefDS(_9096);
    _9096 = NOVALUE;
    goto L3; // [284] 362
L7: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_15846)){
            _9097 = SEQ_PTR(_x_15846)->length;
    }
    else {
        _9097 = 1;
    }
    if (_9097 > 255)
    goto L9; // [292] 308

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_15846)){
            _9099 = SEQ_PTR(_x_15846)->length;
    }
    else {
        _9099 = 1;
    }
    DeRef(_s_15848);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _9099;
    _s_15848 = MAKE_SEQ(_1);
    _9099 = NOVALUE;
    goto LA; // [305] 322
L9: 

    /** 			s = S4B & int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_15846)){
            _9101 = SEQ_PTR(_x_15846)->length;
    }
    else {
        _9101 = 1;
    }
    _9102 = _11int_to_bytes(_9101);
    _9101 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_9102)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_9102)) {
        Prepend(&_s_15848, _9102, 255);
    }
    else {
        Concat((object_ptr)&_s_15848, 255, _9102);
    }
    DeRef(_9102);
    _9102 = NOVALUE;
LA: 

    /** 		puts(f, s)*/
    EPuts(_f_15845, _s_15848); // DJP 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_15846)){
            _9104 = SEQ_PTR(_x_15846)->length;
    }
    else {
        _9104 = 1;
    }
    {
        int _i_15914;
        _i_15914 = 1;
LB: 
        if (_i_15914 > _9104){
            goto LC; // [334] 361
        }

        /** 			fcompress(f, x[i])*/
        _2 = (int)SEQ_PTR(_x_15846);
        _9105 = (int)*(((s1_ptr)_2)->base + _i_15914);
        DeRef(_9106);
        _9106 = _f_15845;
        Ref(_9105);
        _36fcompress(_9106, _9105);
        _9106 = NOVALUE;
        _9105 = NOVALUE;

        /** 		end for*/
        _i_15914 = _i_15914 + 1;
        goto LB; // [356] 341
LC: 
        ;
    }
L3: 

    /** end procedure*/
    DeRef(_x_15846);
    DeRef(_x4_15847);
    DeRef(_s_15848);
    DeRef(_9061);
    _9061 = NOVALUE;
    DeRef(_9071);
    _9071 = NOVALUE;
    DeRef(_9078);
    _9078 = NOVALUE;
    return;
    ;
}


int _36get4()
{
    int _9115 = NOVALUE;
    int _9114 = NOVALUE;
    int _9113 = NOVALUE;
    int _9112 = NOVALUE;
    int _9111 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(current_db))*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9111 = getKBchar();
        }
        else
        _9111 = getc(last_r_file_ptr);
    }
    else
    _9111 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_36mem0_15919)){
        poke_addr = (unsigned char *)_36mem0_15919;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_36mem0_15919)->dbl);
    }
    *poke_addr = (unsigned char)_9111;
    _9111 = NOVALUE;

    /** 	poke(mem1, getc(current_db))*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9112 = getKBchar();
        }
        else
        _9112 = getc(last_r_file_ptr);
    }
    else
    _9112 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_36mem1_15920)){
        poke_addr = (unsigned char *)_36mem1_15920;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_36mem1_15920)->dbl);
    }
    *poke_addr = (unsigned char)_9112;
    _9112 = NOVALUE;

    /** 	poke(mem2, getc(current_db))*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9113 = getKBchar();
        }
        else
        _9113 = getc(last_r_file_ptr);
    }
    else
    _9113 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_36mem2_15921)){
        poke_addr = (unsigned char *)_36mem2_15921;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_36mem2_15921)->dbl);
    }
    *poke_addr = (unsigned char)_9113;
    _9113 = NOVALUE;

    /** 	poke(mem3, getc(current_db))*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9114 = getKBchar();
        }
        else
        _9114 = getc(last_r_file_ptr);
    }
    else
    _9114 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_36mem3_15922)){
        poke_addr = (unsigned char *)_36mem3_15922;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_36mem3_15922)->dbl);
    }
    *poke_addr = (unsigned char)_9114;
    _9114 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_36mem0_15919)) {
        _9115 = *(unsigned long *)_36mem0_15919;
        if ((unsigned)_9115 > (unsigned)MAXINT)
        _9115 = NewDouble((double)(unsigned long)_9115);
    }
    else {
        _9115 = *(unsigned long *)(unsigned long)(DBL_PTR(_36mem0_15919)->dbl);
        if ((unsigned)_9115 > (unsigned)MAXINT)
        _9115 = NewDouble((double)(unsigned long)_9115);
    }
    return _9115;
    ;
}


int _36fdecompress(int _c_15937)
{
    int _s_15938 = NOVALUE;
    int _len_15939 = NOVALUE;
    int _ival_15940 = NOVALUE;
    int _9183 = NOVALUE;
    int _9182 = NOVALUE;
    int _9181 = NOVALUE;
    int _9180 = NOVALUE;
    int _9178 = NOVALUE;
    int _9177 = NOVALUE;
    int _9173 = NOVALUE;
    int _9168 = NOVALUE;
    int _9167 = NOVALUE;
    int _9166 = NOVALUE;
    int _9165 = NOVALUE;
    int _9164 = NOVALUE;
    int _9163 = NOVALUE;
    int _9162 = NOVALUE;
    int _9161 = NOVALUE;
    int _9160 = NOVALUE;
    int _9159 = NOVALUE;
    int _9157 = NOVALUE;
    int _9156 = NOVALUE;
    int _9155 = NOVALUE;
    int _9154 = NOVALUE;
    int _9153 = NOVALUE;
    int _9152 = NOVALUE;
    int _9150 = NOVALUE;
    int _9149 = NOVALUE;
    int _9148 = NOVALUE;
    int _9146 = NOVALUE;
    int _9144 = NOVALUE;
    int _9143 = NOVALUE;
    int _9142 = NOVALUE;
    int _9140 = NOVALUE;
    int _9139 = NOVALUE;
    int _9138 = NOVALUE;
    int _9137 = NOVALUE;
    int _9136 = NOVALUE;
    int _9135 = NOVALUE;
    int _9134 = NOVALUE;
    int _9132 = NOVALUE;
    int _9131 = NOVALUE;
    int _9130 = NOVALUE;
    int _9128 = NOVALUE;
    int _9127 = NOVALUE;
    int _9126 = NOVALUE;
    int _9125 = NOVALUE;
    int _9123 = NOVALUE;
    int _9122 = NOVALUE;
    int _9120 = NOVALUE;
    int _9119 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_c_15937)) {
        _1 = (long)(DBL_PTR(_c_15937)->dbl);
        if (UNIQUE(DBL_PTR(_c_15937)) && (DBL_PTR(_c_15937)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_15937);
        _c_15937 = _1;
    }

    /** 	if c = 0 then*/
    if (_c_15937 != 0)
    goto L1; // [5] 70

    /** 		c = getc(current_db)*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_15937 = getKBchar();
        }
        else
        _c_15937 = getc(last_r_file_ptr);
    }
    else
    _c_15937 = getc(last_r_file_ptr);

    /** 		if c <= CACHE0 then*/
    if (_c_15937 > 184)
    goto L2; // [20] 37

    /** 			return c + MIN1B  -- a normal, quite small integer*/
    _9119 = _c_15937 + -2;
    DeRef(_s_15938);
    return _9119;
    goto L3; // [34] 69
L2: 

    /** 		elsif c <= CACHE0 + COMP_CACHE_SIZE then*/
    _9120 = 248;
    if (_c_15937 > 248)
    goto L4; // [45] 68

    /** 			return comp_cache[c-CACHE0]*/
    _9122 = _c_15937 - 184;
    _2 = (int)SEQ_PTR(_36comp_cache_15839);
    _9123 = (int)*(((s1_ptr)_2)->base + _9122);
    Ref(_9123);
    DeRef(_s_15938);
    DeRef(_9119);
    _9119 = NOVALUE;
    _9120 = NOVALUE;
    _9122 = NOVALUE;
    return _9123;
L4: 
L3: 
L1: 

    /** 	if c = I2B then*/
    if (_c_15937 != 249)
    goto L5; // [72] 133

    /** 		ival = getc(current_db) +*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9125 = getKBchar();
        }
        else
        _9125 = getc(last_r_file_ptr);
    }
    else
    _9125 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9126 = getKBchar();
        }
        else
        _9126 = getc(last_r_file_ptr);
    }
    else
    _9126 = getc(last_r_file_ptr);
    _9127 = 256 * _9126;
    _9126 = NOVALUE;
    _9128 = _9125 + _9127;
    _9125 = NOVALUE;
    _9127 = NOVALUE;
    _ival_15940 = _9128 + _36MIN2B_15752;
    _9128 = NOVALUE;

    /** 		comp_cache[1 + and_bits(ival, COMP_CACHE_SIZE-1)] = ival*/
    _9130 = 63;
    {unsigned long tu;
         tu = (unsigned long)_ival_15940 & (unsigned long)63;
         _9131 = MAKE_UINT(tu);
    }
    _9130 = NOVALUE;
    if (IS_ATOM_INT(_9131)) {
        _9132 = _9131 + 1;
    }
    else
    _9132 = binary_op(PLUS, 1, _9131);
    DeRef(_9131);
    _9131 = NOVALUE;
    _2 = (int)SEQ_PTR(_36comp_cache_15839);
    if (!IS_ATOM_INT(_9132))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_9132)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _9132);
    _1 = *(int *)_2;
    *(int *)_2 = _ival_15940;
    DeRef(_1);

    /** 		return ival*/
    DeRef(_s_15938);
    DeRef(_9119);
    _9119 = NOVALUE;
    DeRef(_9120);
    _9120 = NOVALUE;
    DeRef(_9122);
    _9122 = NOVALUE;
    _9123 = NOVALUE;
    DeRef(_9132);
    _9132 = NOVALUE;
    return _ival_15940;
    goto L6; // [130] 514
L5: 

    /** 	elsif c = I3B then*/
    if (_c_15937 != 250)
    goto L7; // [135] 209

    /** 		ival = getc(current_db) +*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9134 = getKBchar();
        }
        else
        _9134 = getc(last_r_file_ptr);
    }
    else
    _9134 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9135 = getKBchar();
        }
        else
        _9135 = getc(last_r_file_ptr);
    }
    else
    _9135 = getc(last_r_file_ptr);
    _9136 = 256 * _9135;
    _9135 = NOVALUE;
    _9137 = _9134 + _9136;
    _9134 = NOVALUE;
    _9136 = NOVALUE;
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9138 = getKBchar();
        }
        else
        _9138 = getc(last_r_file_ptr);
    }
    else
    _9138 = getc(last_r_file_ptr);
    _9139 = 65536 * _9138;
    _9138 = NOVALUE;
    _9140 = _9137 + _9139;
    _9137 = NOVALUE;
    _9139 = NOVALUE;
    _ival_15940 = _9140 + _36MIN3B_15758;
    _9140 = NOVALUE;

    /** 		comp_cache[1 + and_bits(ival, COMP_CACHE_SIZE-1)] = ival*/
    _9142 = 63;
    {unsigned long tu;
         tu = (unsigned long)_ival_15940 & (unsigned long)63;
         _9143 = MAKE_UINT(tu);
    }
    _9142 = NOVALUE;
    if (IS_ATOM_INT(_9143)) {
        _9144 = _9143 + 1;
    }
    else
    _9144 = binary_op(PLUS, 1, _9143);
    DeRef(_9143);
    _9143 = NOVALUE;
    _2 = (int)SEQ_PTR(_36comp_cache_15839);
    if (!IS_ATOM_INT(_9144))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_9144)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _9144);
    _1 = *(int *)_2;
    *(int *)_2 = _ival_15940;
    DeRef(_1);

    /** 		return ival*/
    DeRef(_s_15938);
    DeRef(_9119);
    _9119 = NOVALUE;
    DeRef(_9120);
    _9120 = NOVALUE;
    DeRef(_9122);
    _9122 = NOVALUE;
    _9123 = NOVALUE;
    DeRef(_9132);
    _9132 = NOVALUE;
    DeRef(_9144);
    _9144 = NOVALUE;
    return _ival_15940;
    goto L6; // [206] 514
L7: 

    /** 	elsif c = I4B  then*/
    if (_c_15937 != 251)
    goto L8; // [211] 257

    /** 		ival = get4() + MIN4B*/
    _9146 = _36get4();
    if (IS_ATOM_INT(_9146) && IS_ATOM_INT(_36MIN4B_15764)) {
        _ival_15940 = _9146 + _36MIN4B_15764;
    }
    else {
        _ival_15940 = binary_op(PLUS, _9146, _36MIN4B_15764);
    }
    DeRef(_9146);
    _9146 = NOVALUE;
    if (!IS_ATOM_INT(_ival_15940)) {
        _1 = (long)(DBL_PTR(_ival_15940)->dbl);
        if (UNIQUE(DBL_PTR(_ival_15940)) && (DBL_PTR(_ival_15940)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ival_15940);
        _ival_15940 = _1;
    }

    /** 		comp_cache[1 + and_bits(ival, COMP_CACHE_SIZE-1)] = ival*/
    _9148 = 63;
    {unsigned long tu;
         tu = (unsigned long)_ival_15940 & (unsigned long)63;
         _9149 = MAKE_UINT(tu);
    }
    _9148 = NOVALUE;
    if (IS_ATOM_INT(_9149)) {
        _9150 = _9149 + 1;
    }
    else
    _9150 = binary_op(PLUS, 1, _9149);
    DeRef(_9149);
    _9149 = NOVALUE;
    _2 = (int)SEQ_PTR(_36comp_cache_15839);
    if (!IS_ATOM_INT(_9150))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_9150)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _9150);
    _1 = *(int *)_2;
    *(int *)_2 = _ival_15940;
    DeRef(_1);

    /** 		return ival*/
    DeRef(_s_15938);
    DeRef(_9119);
    _9119 = NOVALUE;
    DeRef(_9120);
    _9120 = NOVALUE;
    DeRef(_9122);
    _9122 = NOVALUE;
    _9123 = NOVALUE;
    DeRef(_9132);
    _9132 = NOVALUE;
    DeRef(_9144);
    _9144 = NOVALUE;
    DeRef(_9150);
    _9150 = NOVALUE;
    return _ival_15940;
    goto L6; // [254] 514
L8: 

    /** 	elsif c = F4B then*/
    if (_c_15937 != 252)
    goto L9; // [259] 303

    /** 		return float32_to_atom({getc(current_db), getc(current_db),*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9152 = getKBchar();
        }
        else
        _9152 = getc(last_r_file_ptr);
    }
    else
    _9152 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9153 = getKBchar();
        }
        else
        _9153 = getc(last_r_file_ptr);
    }
    else
    _9153 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9154 = getKBchar();
        }
        else
        _9154 = getc(last_r_file_ptr);
    }
    else
    _9154 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9155 = getKBchar();
        }
        else
        _9155 = getc(last_r_file_ptr);
    }
    else
    _9155 = getc(last_r_file_ptr);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9152;
    *((int *)(_2+8)) = _9153;
    *((int *)(_2+12)) = _9154;
    *((int *)(_2+16)) = _9155;
    _9156 = MAKE_SEQ(_1);
    _9155 = NOVALUE;
    _9154 = NOVALUE;
    _9153 = NOVALUE;
    _9152 = NOVALUE;
    _9157 = _11float32_to_atom(_9156);
    _9156 = NOVALUE;
    DeRef(_s_15938);
    DeRef(_9119);
    _9119 = NOVALUE;
    DeRef(_9120);
    _9120 = NOVALUE;
    DeRef(_9122);
    _9122 = NOVALUE;
    _9123 = NOVALUE;
    DeRef(_9132);
    _9132 = NOVALUE;
    DeRef(_9144);
    _9144 = NOVALUE;
    DeRef(_9150);
    _9150 = NOVALUE;
    return _9157;
    goto L6; // [300] 514
L9: 

    /** 	elsif c = F8B then*/
    if (_c_15937 != 253)
    goto LA; // [305] 373

    /** 		return float64_to_atom({getc(current_db), getc(current_db),*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9159 = getKBchar();
        }
        else
        _9159 = getc(last_r_file_ptr);
    }
    else
    _9159 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9160 = getKBchar();
        }
        else
        _9160 = getc(last_r_file_ptr);
    }
    else
    _9160 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9161 = getKBchar();
        }
        else
        _9161 = getc(last_r_file_ptr);
    }
    else
    _9161 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9162 = getKBchar();
        }
        else
        _9162 = getc(last_r_file_ptr);
    }
    else
    _9162 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9163 = getKBchar();
        }
        else
        _9163 = getc(last_r_file_ptr);
    }
    else
    _9163 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9164 = getKBchar();
        }
        else
        _9164 = getc(last_r_file_ptr);
    }
    else
    _9164 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9165 = getKBchar();
        }
        else
        _9165 = getc(last_r_file_ptr);
    }
    else
    _9165 = getc(last_r_file_ptr);
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _9166 = getKBchar();
        }
        else
        _9166 = getc(last_r_file_ptr);
    }
    else
    _9166 = getc(last_r_file_ptr);
    _1 = NewS1(8);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9159;
    *((int *)(_2+8)) = _9160;
    *((int *)(_2+12)) = _9161;
    *((int *)(_2+16)) = _9162;
    *((int *)(_2+20)) = _9163;
    *((int *)(_2+24)) = _9164;
    *((int *)(_2+28)) = _9165;
    *((int *)(_2+32)) = _9166;
    _9167 = MAKE_SEQ(_1);
    _9166 = NOVALUE;
    _9165 = NOVALUE;
    _9164 = NOVALUE;
    _9163 = NOVALUE;
    _9162 = NOVALUE;
    _9161 = NOVALUE;
    _9160 = NOVALUE;
    _9159 = NOVALUE;
    _9168 = _11float64_to_atom(_9167);
    _9167 = NOVALUE;
    DeRef(_s_15938);
    DeRef(_9119);
    _9119 = NOVALUE;
    DeRef(_9120);
    _9120 = NOVALUE;
    DeRef(_9122);
    _9122 = NOVALUE;
    _9123 = NOVALUE;
    DeRef(_9132);
    _9132 = NOVALUE;
    DeRef(_9144);
    _9144 = NOVALUE;
    DeRef(_9150);
    _9150 = NOVALUE;
    DeRef(_9157);
    _9157 = NOVALUE;
    return _9168;
    goto L6; // [370] 514
LA: 

    /** 		if c = S1B then*/
    if (_c_15937 != 254)
    goto LB; // [375] 389

    /** 			len = getc(current_db)*/
    if (_36current_db_15927 != last_r_file_no) {
        last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
        last_r_file_no = _36current_db_15927;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _len_15939 = getKBchar();
        }
        else
        _len_15939 = getc(last_r_file_ptr);
    }
    else
    _len_15939 = getc(last_r_file_ptr);
    goto LC; // [386] 397
LB: 

    /** 			len = get4()*/
    _len_15939 = _36get4();
    if (!IS_ATOM_INT(_len_15939)) {
        _1 = (long)(DBL_PTR(_len_15939)->dbl);
        if (UNIQUE(DBL_PTR(_len_15939)) && (DBL_PTR(_len_15939)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_15939);
        _len_15939 = _1;
    }
LC: 

    /** 		s = repeat(0, len)*/
    DeRef(_s_15938);
    _s_15938 = Repeat(0, _len_15939);

    /** 		for i = 1 to len do*/
    _9173 = _len_15939;
    {
        int _i_16012;
        _i_16012 = 1;
LD: 
        if (_i_16012 > _9173){
            goto LE; // [410] 507
        }

        /** 			c = getc(current_db)*/
        if (_36current_db_15927 != last_r_file_no) {
            last_r_file_ptr = which_file(_36current_db_15927, EF_READ);
            last_r_file_no = _36current_db_15927;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _c_15937 = getKBchar();
            }
            else
            _c_15937 = getc(last_r_file_ptr);
        }
        else
        _c_15937 = getc(last_r_file_ptr);

        /** 			if c < I2B then*/
        if (_c_15937 >= 249)
        goto LF; // [426] 486

        /** 				if c <= CACHE0 then*/
        if (_c_15937 > 184)
        goto L10; // [434] 451

        /** 					s[i] = c + MIN1B*/
        _9177 = _c_15937 + -2;
        _2 = (int)SEQ_PTR(_s_15938);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_15938 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_16012);
        _1 = *(int *)_2;
        *(int *)_2 = _9177;
        if( _1 != _9177 ){
            DeRef(_1);
        }
        _9177 = NOVALUE;
        goto L11; // [448] 500
L10: 

        /** 				elsif c <= CACHE0 + COMP_CACHE_SIZE then*/
        _9178 = 248;
        if (_c_15937 > 248)
        goto L11; // [459] 500

        /** 					s[i] = comp_cache[c - CACHE0]*/
        _9180 = _c_15937 - 184;
        _2 = (int)SEQ_PTR(_36comp_cache_15839);
        _9181 = (int)*(((s1_ptr)_2)->base + _9180);
        Ref(_9181);
        _2 = (int)SEQ_PTR(_s_15938);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_15938 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_16012);
        _1 = *(int *)_2;
        *(int *)_2 = _9181;
        if( _1 != _9181 ){
            DeRef(_1);
        }
        _9181 = NOVALUE;
        goto L11; // [483] 500
LF: 

        /** 				s[i] = fdecompress(c)*/
        DeRef(_9182);
        _9182 = _c_15937;
        _9183 = _36fdecompress(_9182);
        _9182 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_15938);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_15938 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_16012);
        _1 = *(int *)_2;
        *(int *)_2 = _9183;
        if( _1 != _9183 ){
            DeRef(_1);
        }
        _9183 = NOVALUE;
L11: 

        /** 		end for*/
        _i_16012 = _i_16012 + 1;
        goto LD; // [502] 417
LE: 
        ;
    }

    /** 		return s*/
    DeRef(_9119);
    _9119 = NOVALUE;
    DeRef(_9120);
    _9120 = NOVALUE;
    DeRef(_9122);
    _9122 = NOVALUE;
    _9123 = NOVALUE;
    DeRef(_9132);
    _9132 = NOVALUE;
    DeRef(_9144);
    _9144 = NOVALUE;
    DeRef(_9150);
    _9150 = NOVALUE;
    DeRef(_9178);
    _9178 = NOVALUE;
    DeRef(_9157);
    _9157 = NOVALUE;
    DeRef(_9168);
    _9168 = NOVALUE;
    DeRef(_9180);
    _9180 = NOVALUE;
    return _s_15938;
L6: 
    ;
}



// 0x2F773437
