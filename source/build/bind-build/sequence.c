// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _21reverse(int _target_5495, int _pFrom_5496, int _pTo_5497)
{
    int _uppr_5498 = NOVALUE;
    int _n_5499 = NOVALUE;
    int _lLimit_5500 = NOVALUE;
    int _t_5501 = NOVALUE;
    int _2806 = NOVALUE;
    int _2805 = NOVALUE;
    int _2804 = NOVALUE;
    int _2802 = NOVALUE;
    int _2801 = NOVALUE;
    int _2799 = NOVALUE;
    int _2797 = NOVALUE;
    int _0, _1, _2;
    

    /** 	n = length(target)*/
    if (IS_SEQUENCE(_target_5495)){
            _n_5499 = SEQ_PTR(_target_5495)->length;
    }
    else {
        _n_5499 = 1;
    }

    /** 	if n < 2 then*/
    if (_n_5499 >= 2)
    goto L1; // [12] 23

    /** 		return target*/
    DeRef(_t_5501);
    return _target_5495;
L1: 

    /** 	if pFrom < 1 then*/
    if (_pFrom_5496 >= 1)
    goto L2; // [25] 35

    /** 		pFrom = 1*/
    _pFrom_5496 = 1;
L2: 

    /** 	if pTo < 1 then*/
    if (_pTo_5497 >= 1)
    goto L3; // [37] 48

    /** 		pTo = n + pTo*/
    _pTo_5497 = _n_5499 + _pTo_5497;
L3: 

    /** 	if pTo < pFrom or pFrom >= n then*/
    _2797 = (_pTo_5497 < _pFrom_5496);
    if (_2797 != 0) {
        goto L4; // [54] 67
    }
    _2799 = (_pFrom_5496 >= _n_5499);
    if (_2799 == 0)
    {
        DeRef(_2799);
        _2799 = NOVALUE;
        goto L5; // [63] 74
    }
    else{
        DeRef(_2799);
        _2799 = NOVALUE;
    }
L4: 

    /** 		return target*/
    DeRef(_t_5501);
    DeRef(_2797);
    _2797 = NOVALUE;
    return _target_5495;
L5: 

    /** 	if pTo > n then*/
    if (_pTo_5497 <= _n_5499)
    goto L6; // [76] 86

    /** 		pTo = n*/
    _pTo_5497 = _n_5499;
L6: 

    /** 	lLimit = floor((pFrom+pTo-1)/2)*/
    _2801 = _pFrom_5496 + _pTo_5497;
    if ((long)((unsigned long)_2801 + (unsigned long)HIGH_BITS) >= 0) 
    _2801 = NewDouble((double)_2801);
    if (IS_ATOM_INT(_2801)) {
        _2802 = _2801 - 1;
        if ((long)((unsigned long)_2802 +(unsigned long) HIGH_BITS) >= 0){
            _2802 = NewDouble((double)_2802);
        }
    }
    else {
        _2802 = NewDouble(DBL_PTR(_2801)->dbl - (double)1);
    }
    DeRef(_2801);
    _2801 = NOVALUE;
    if (IS_ATOM_INT(_2802)) {
        _lLimit_5500 = _2802 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _2802, 2);
        _lLimit_5500 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_2802);
    _2802 = NOVALUE;
    if (!IS_ATOM_INT(_lLimit_5500)) {
        _1 = (long)(DBL_PTR(_lLimit_5500)->dbl);
        if (UNIQUE(DBL_PTR(_lLimit_5500)) && (DBL_PTR(_lLimit_5500)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lLimit_5500);
        _lLimit_5500 = _1;
    }

    /** 	t = target*/
    Ref(_target_5495);
    DeRef(_t_5501);
    _t_5501 = _target_5495;

    /** 	uppr = pTo*/
    _uppr_5498 = _pTo_5497;

    /** 	for lowr = pFrom to lLimit do*/
    _2804 = _lLimit_5500;
    {
        int _lowr_5520;
        _lowr_5520 = _pFrom_5496;
L7: 
        if (_lowr_5520 > _2804){
            goto L8; // [119] 159
        }

        /** 		t[uppr] = target[lowr]*/
        _2 = (int)SEQ_PTR(_target_5495);
        _2805 = (int)*(((s1_ptr)_2)->base + _lowr_5520);
        Ref(_2805);
        _2 = (int)SEQ_PTR(_t_5501);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_5501 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _uppr_5498);
        _1 = *(int *)_2;
        *(int *)_2 = _2805;
        if( _1 != _2805 ){
            DeRef(_1);
        }
        _2805 = NOVALUE;

        /** 		t[lowr] = target[uppr]*/
        _2 = (int)SEQ_PTR(_target_5495);
        _2806 = (int)*(((s1_ptr)_2)->base + _uppr_5498);
        Ref(_2806);
        _2 = (int)SEQ_PTR(_t_5501);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_5501 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lowr_5520);
        _1 = *(int *)_2;
        *(int *)_2 = _2806;
        if( _1 != _2806 ){
            DeRef(_1);
        }
        _2806 = NOVALUE;

        /** 		uppr -= 1*/
        _uppr_5498 = _uppr_5498 - 1;

        /** 	end for*/
        _lowr_5520 = _lowr_5520 + 1;
        goto L7; // [154] 126
L8: 
        ;
    }

    /** 	return t*/
    DeRef(_target_5495);
    DeRef(_2797);
    _2797 = NOVALUE;
    return _t_5501;
    ;
}


int _21pad_tail(int _target_5596, int _size_5597, int _ch_5598)
{
    int _2843 = NOVALUE;
    int _2842 = NOVALUE;
    int _2841 = NOVALUE;
    int _2840 = NOVALUE;
    int _2838 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if size <= length(target) then*/
    if (IS_SEQUENCE(_target_5596)){
            _2838 = SEQ_PTR(_target_5596)->length;
    }
    else {
        _2838 = 1;
    }
    if (_size_5597 > _2838)
    goto L1; // [8] 19

    /** 		return target*/
    return _target_5596;
L1: 

    /** 	return target & repeat(ch, size - length(target))*/
    if (IS_SEQUENCE(_target_5596)){
            _2840 = SEQ_PTR(_target_5596)->length;
    }
    else {
        _2840 = 1;
    }
    _2841 = _size_5597 - _2840;
    _2840 = NOVALUE;
    _2842 = Repeat(_ch_5598, _2841);
    _2841 = NOVALUE;
    if (IS_SEQUENCE(_target_5596) && IS_ATOM(_2842)) {
    }
    else if (IS_ATOM(_target_5596) && IS_SEQUENCE(_2842)) {
        Ref(_target_5596);
        Prepend(&_2843, _2842, _target_5596);
    }
    else {
        Concat((object_ptr)&_2843, _target_5596, _2842);
    }
    DeRefDS(_2842);
    _2842 = NOVALUE;
    DeRef(_target_5596);
    return _2843;
    ;
}


int _21filter(int _source_5850, int _rid_5851, int _userdata_5852, int _rangetype_5853)
{
    int _dest_5854 = NOVALUE;
    int _idx_5855 = NOVALUE;
    int _3157 = NOVALUE;
    int _3156 = NOVALUE;
    int _3154 = NOVALUE;
    int _3153 = NOVALUE;
    int _3152 = NOVALUE;
    int _3151 = NOVALUE;
    int _3150 = NOVALUE;
    int _3147 = NOVALUE;
    int _3146 = NOVALUE;
    int _3145 = NOVALUE;
    int _3144 = NOVALUE;
    int _3141 = NOVALUE;
    int _3140 = NOVALUE;
    int _3139 = NOVALUE;
    int _3138 = NOVALUE;
    int _3137 = NOVALUE;
    int _3134 = NOVALUE;
    int _3133 = NOVALUE;
    int _3132 = NOVALUE;
    int _3131 = NOVALUE;
    int _3128 = NOVALUE;
    int _3127 = NOVALUE;
    int _3126 = NOVALUE;
    int _3125 = NOVALUE;
    int _3124 = NOVALUE;
    int _3121 = NOVALUE;
    int _3120 = NOVALUE;
    int _3119 = NOVALUE;
    int _3118 = NOVALUE;
    int _3115 = NOVALUE;
    int _3114 = NOVALUE;
    int _3113 = NOVALUE;
    int _3112 = NOVALUE;
    int _3111 = NOVALUE;
    int _3108 = NOVALUE;
    int _3107 = NOVALUE;
    int _3106 = NOVALUE;
    int _3105 = NOVALUE;
    int _3102 = NOVALUE;
    int _3101 = NOVALUE;
    int _3100 = NOVALUE;
    int _3099 = NOVALUE;
    int _3098 = NOVALUE;
    int _3095 = NOVALUE;
    int _3094 = NOVALUE;
    int _3093 = NOVALUE;
    int _3089 = NOVALUE;
    int _3086 = NOVALUE;
    int _3085 = NOVALUE;
    int _3084 = NOVALUE;
    int _3082 = NOVALUE;
    int _3081 = NOVALUE;
    int _3080 = NOVALUE;
    int _3079 = NOVALUE;
    int _3078 = NOVALUE;
    int _3075 = NOVALUE;
    int _3074 = NOVALUE;
    int _3073 = NOVALUE;
    int _3071 = NOVALUE;
    int _3070 = NOVALUE;
    int _3069 = NOVALUE;
    int _3068 = NOVALUE;
    int _3067 = NOVALUE;
    int _3064 = NOVALUE;
    int _3063 = NOVALUE;
    int _3062 = NOVALUE;
    int _3060 = NOVALUE;
    int _3059 = NOVALUE;
    int _3058 = NOVALUE;
    int _3057 = NOVALUE;
    int _3056 = NOVALUE;
    int _3053 = NOVALUE;
    int _3052 = NOVALUE;
    int _3051 = NOVALUE;
    int _3049 = NOVALUE;
    int _3048 = NOVALUE;
    int _3047 = NOVALUE;
    int _3046 = NOVALUE;
    int _3045 = NOVALUE;
    int _3043 = NOVALUE;
    int _3042 = NOVALUE;
    int _3041 = NOVALUE;
    int _3037 = NOVALUE;
    int _3034 = NOVALUE;
    int _3033 = NOVALUE;
    int _3032 = NOVALUE;
    int _3029 = NOVALUE;
    int _3026 = NOVALUE;
    int _3025 = NOVALUE;
    int _3024 = NOVALUE;
    int _3021 = NOVALUE;
    int _3018 = NOVALUE;
    int _3017 = NOVALUE;
    int _3016 = NOVALUE;
    int _3013 = NOVALUE;
    int _3010 = NOVALUE;
    int _3009 = NOVALUE;
    int _3008 = NOVALUE;
    int _3004 = NOVALUE;
    int _3001 = NOVALUE;
    int _3000 = NOVALUE;
    int _2999 = NOVALUE;
    int _2996 = NOVALUE;
    int _2993 = NOVALUE;
    int _2992 = NOVALUE;
    int _2991 = NOVALUE;
    int _2985 = NOVALUE;
    int _2983 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_5850)){
            _2983 = SEQ_PTR(_source_5850)->length;
    }
    else {
        _2983 = 1;
    }
    if (_2983 != 0)
    goto L1; // [8] 19

    /** 		return source*/
    DeRefDS(_userdata_5852);
    DeRefDS(_rangetype_5853);
    DeRef(_dest_5854);
    return _source_5850;
L1: 

    /** 	dest = repeat(0, length(source))*/
    if (IS_SEQUENCE(_source_5850)){
            _2985 = SEQ_PTR(_source_5850)->length;
    }
    else {
        _2985 = 1;
    }
    DeRef(_dest_5854);
    _dest_5854 = Repeat(0, _2985);
    _2985 = NOVALUE;

    /** 	idx = 0*/
    _idx_5855 = 0;

    /** 	switch rid do*/
    _1 = find(_rid_5851, _2987);
    switch ( _1 ){ 

        /** 		case "<", "lt" then*/
        case 1:
        case 2:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5850)){
                _2991 = SEQ_PTR(_source_5850)->length;
        }
        else {
            _2991 = 1;
        }
        {
            int _a_5867;
            _a_5867 = 1;
L2: 
            if (_a_5867 > _2991){
                goto L3; // [51] 96
            }

            /** 				if compare(source[a], userdata) < 0 then*/
            _2 = (int)SEQ_PTR(_source_5850);
            _2992 = (int)*(((s1_ptr)_2)->base + _a_5867);
            if (IS_ATOM_INT(_2992) && IS_ATOM_INT(_userdata_5852)){
                _2993 = (_2992 < _userdata_5852) ? -1 : (_2992 > _userdata_5852);
            }
            else{
                _2993 = compare(_2992, _userdata_5852);
            }
            _2992 = NOVALUE;
            if (_2993 >= 0)
            goto L4; // [68] 89

            /** 					idx += 1*/
            _idx_5855 = _idx_5855 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5850);
            _2996 = (int)*(((s1_ptr)_2)->base + _a_5867);
            Ref(_2996);
            _2 = (int)SEQ_PTR(_dest_5854);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
            _1 = *(int *)_2;
            *(int *)_2 = _2996;
            if( _1 != _2996 ){
                DeRef(_1);
            }
            _2996 = NOVALUE;
L4: 

            /** 			end for*/
            _a_5867 = _a_5867 + 1;
            goto L2; // [91] 58
L3: 
            ;
        }
        goto L5; // [96] 1304

        /** 		case "<=", "le" then*/
        case 3:
        case 4:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5850)){
                _2999 = SEQ_PTR(_source_5850)->length;
        }
        else {
            _2999 = 1;
        }
        {
            int _a_5879;
            _a_5879 = 1;
L6: 
            if (_a_5879 > _2999){
                goto L7; // [109] 154
            }

            /** 				if compare(source[a], userdata) <= 0 then*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3000 = (int)*(((s1_ptr)_2)->base + _a_5879);
            if (IS_ATOM_INT(_3000) && IS_ATOM_INT(_userdata_5852)){
                _3001 = (_3000 < _userdata_5852) ? -1 : (_3000 > _userdata_5852);
            }
            else{
                _3001 = compare(_3000, _userdata_5852);
            }
            _3000 = NOVALUE;
            if (_3001 > 0)
            goto L8; // [126] 147

            /** 					idx += 1*/
            _idx_5855 = _idx_5855 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3004 = (int)*(((s1_ptr)_2)->base + _a_5879);
            Ref(_3004);
            _2 = (int)SEQ_PTR(_dest_5854);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
            _1 = *(int *)_2;
            *(int *)_2 = _3004;
            if( _1 != _3004 ){
                DeRef(_1);
            }
            _3004 = NOVALUE;
L8: 

            /** 			end for*/
            _a_5879 = _a_5879 + 1;
            goto L6; // [149] 116
L7: 
            ;
        }
        goto L5; // [154] 1304

        /** 		case "=", "==", "eq" then*/
        case 5:
        case 6:
        case 7:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5850)){
                _3008 = SEQ_PTR(_source_5850)->length;
        }
        else {
            _3008 = 1;
        }
        {
            int _a_5892;
            _a_5892 = 1;
L9: 
            if (_a_5892 > _3008){
                goto LA; // [169] 214
            }

            /** 				if compare(source[a], userdata) = 0 then*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3009 = (int)*(((s1_ptr)_2)->base + _a_5892);
            if (IS_ATOM_INT(_3009) && IS_ATOM_INT(_userdata_5852)){
                _3010 = (_3009 < _userdata_5852) ? -1 : (_3009 > _userdata_5852);
            }
            else{
                _3010 = compare(_3009, _userdata_5852);
            }
            _3009 = NOVALUE;
            if (_3010 != 0)
            goto LB; // [186] 207

            /** 					idx += 1*/
            _idx_5855 = _idx_5855 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3013 = (int)*(((s1_ptr)_2)->base + _a_5892);
            Ref(_3013);
            _2 = (int)SEQ_PTR(_dest_5854);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
            _1 = *(int *)_2;
            *(int *)_2 = _3013;
            if( _1 != _3013 ){
                DeRef(_1);
            }
            _3013 = NOVALUE;
LB: 

            /** 			end for*/
            _a_5892 = _a_5892 + 1;
            goto L9; // [209] 176
LA: 
            ;
        }
        goto L5; // [214] 1304

        /** 		case "!=", "ne" then*/
        case 8:
        case 9:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5850)){
                _3016 = SEQ_PTR(_source_5850)->length;
        }
        else {
            _3016 = 1;
        }
        {
            int _a_5904;
            _a_5904 = 1;
LC: 
            if (_a_5904 > _3016){
                goto LD; // [227] 272
            }

            /** 				if compare(source[a], userdata) != 0 then*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3017 = (int)*(((s1_ptr)_2)->base + _a_5904);
            if (IS_ATOM_INT(_3017) && IS_ATOM_INT(_userdata_5852)){
                _3018 = (_3017 < _userdata_5852) ? -1 : (_3017 > _userdata_5852);
            }
            else{
                _3018 = compare(_3017, _userdata_5852);
            }
            _3017 = NOVALUE;
            if (_3018 == 0)
            goto LE; // [244] 265

            /** 					idx += 1*/
            _idx_5855 = _idx_5855 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3021 = (int)*(((s1_ptr)_2)->base + _a_5904);
            Ref(_3021);
            _2 = (int)SEQ_PTR(_dest_5854);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
            _1 = *(int *)_2;
            *(int *)_2 = _3021;
            if( _1 != _3021 ){
                DeRef(_1);
            }
            _3021 = NOVALUE;
LE: 

            /** 			end for*/
            _a_5904 = _a_5904 + 1;
            goto LC; // [267] 234
LD: 
            ;
        }
        goto L5; // [272] 1304

        /** 		case ">", "gt" then*/
        case 10:
        case 11:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5850)){
                _3024 = SEQ_PTR(_source_5850)->length;
        }
        else {
            _3024 = 1;
        }
        {
            int _a_5916;
            _a_5916 = 1;
LF: 
            if (_a_5916 > _3024){
                goto L10; // [285] 330
            }

            /** 				if compare(source[a], userdata) > 0 then*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3025 = (int)*(((s1_ptr)_2)->base + _a_5916);
            if (IS_ATOM_INT(_3025) && IS_ATOM_INT(_userdata_5852)){
                _3026 = (_3025 < _userdata_5852) ? -1 : (_3025 > _userdata_5852);
            }
            else{
                _3026 = compare(_3025, _userdata_5852);
            }
            _3025 = NOVALUE;
            if (_3026 <= 0)
            goto L11; // [302] 323

            /** 					idx += 1*/
            _idx_5855 = _idx_5855 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3029 = (int)*(((s1_ptr)_2)->base + _a_5916);
            Ref(_3029);
            _2 = (int)SEQ_PTR(_dest_5854);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
            _1 = *(int *)_2;
            *(int *)_2 = _3029;
            if( _1 != _3029 ){
                DeRef(_1);
            }
            _3029 = NOVALUE;
L11: 

            /** 			end for*/
            _a_5916 = _a_5916 + 1;
            goto LF; // [325] 292
L10: 
            ;
        }
        goto L5; // [330] 1304

        /** 		case ">=", "ge" then*/
        case 12:
        case 13:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5850)){
                _3032 = SEQ_PTR(_source_5850)->length;
        }
        else {
            _3032 = 1;
        }
        {
            int _a_5928;
            _a_5928 = 1;
L12: 
            if (_a_5928 > _3032){
                goto L13; // [343] 388
            }

            /** 				if compare(source[a], userdata) >= 0 then*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3033 = (int)*(((s1_ptr)_2)->base + _a_5928);
            if (IS_ATOM_INT(_3033) && IS_ATOM_INT(_userdata_5852)){
                _3034 = (_3033 < _userdata_5852) ? -1 : (_3033 > _userdata_5852);
            }
            else{
                _3034 = compare(_3033, _userdata_5852);
            }
            _3033 = NOVALUE;
            if (_3034 < 0)
            goto L14; // [360] 381

            /** 					idx += 1*/
            _idx_5855 = _idx_5855 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3037 = (int)*(((s1_ptr)_2)->base + _a_5928);
            Ref(_3037);
            _2 = (int)SEQ_PTR(_dest_5854);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
            _1 = *(int *)_2;
            *(int *)_2 = _3037;
            if( _1 != _3037 ){
                DeRef(_1);
            }
            _3037 = NOVALUE;
L14: 

            /** 			end for*/
            _a_5928 = _a_5928 + 1;
            goto L12; // [383] 350
L13: 
            ;
        }
        goto L5; // [388] 1304

        /** 		case "in" then*/
        case 14:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_5853, _3039);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3041 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3041 = 1;
            }
            {
                int _a_5942;
                _a_5942 = 1;
L15: 
                if (_a_5942 > _3041){
                    goto L16; // [410] 455
                }

                /** 						if find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3042 = (int)*(((s1_ptr)_2)->base + _a_5942);
                _3043 = find_from(_3042, _userdata_5852, 1);
                _3042 = NOVALUE;
                if (_3043 == 0)
                {
                    _3043 = NOVALUE;
                    goto L17; // [428] 448
                }
                else{
                    _3043 = NOVALUE;
                }

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3045 = (int)*(((s1_ptr)_2)->base + _a_5942);
                Ref(_3045);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3045;
                if( _1 != _3045 ){
                    DeRef(_1);
                }
                _3045 = NOVALUE;
L17: 

                /** 					end for*/
                _a_5942 = _a_5942 + 1;
                goto L15; // [450] 417
L16: 
                ;
            }
            goto L5; // [455] 1304

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3046 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3046 = 1;
            }
            {
                int _a_5951;
                _a_5951 = 1;
L18: 
                if (_a_5951 > _3046){
                    goto L19; // [466] 534
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3047 = (int)*(((s1_ptr)_2)->base + _a_5951);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3048 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3047) && IS_ATOM_INT(_3048)){
                    _3049 = (_3047 < _3048) ? -1 : (_3047 > _3048);
                }
                else{
                    _3049 = compare(_3047, _3048);
                }
                _3047 = NOVALUE;
                _3048 = NOVALUE;
                if (_3049 < 0)
                goto L1A; // [487] 527

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3051 = (int)*(((s1_ptr)_2)->base + _a_5951);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3052 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3051) && IS_ATOM_INT(_3052)){
                    _3053 = (_3051 < _3052) ? -1 : (_3051 > _3052);
                }
                else{
                    _3053 = compare(_3051, _3052);
                }
                _3051 = NOVALUE;
                _3052 = NOVALUE;
                if (_3053 > 0)
                goto L1B; // [505] 526

                /** 								idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3056 = (int)*(((s1_ptr)_2)->base + _a_5951);
                Ref(_3056);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3056;
                if( _1 != _3056 ){
                    DeRef(_1);
                }
                _3056 = NOVALUE;
L1B: 
L1A: 

                /** 					end for*/
                _a_5951 = _a_5951 + 1;
                goto L18; // [529] 473
L19: 
                ;
            }
            goto L5; // [534] 1304

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3057 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3057 = 1;
            }
            {
                int _a_5967;
                _a_5967 = 1;
L1C: 
                if (_a_5967 > _3057){
                    goto L1D; // [545] 613
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3058 = (int)*(((s1_ptr)_2)->base + _a_5967);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3059 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3058) && IS_ATOM_INT(_3059)){
                    _3060 = (_3058 < _3059) ? -1 : (_3058 > _3059);
                }
                else{
                    _3060 = compare(_3058, _3059);
                }
                _3058 = NOVALUE;
                _3059 = NOVALUE;
                if (_3060 < 0)
                goto L1E; // [566] 606

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3062 = (int)*(((s1_ptr)_2)->base + _a_5967);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3063 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3062) && IS_ATOM_INT(_3063)){
                    _3064 = (_3062 < _3063) ? -1 : (_3062 > _3063);
                }
                else{
                    _3064 = compare(_3062, _3063);
                }
                _3062 = NOVALUE;
                _3063 = NOVALUE;
                if (_3064 >= 0)
                goto L1F; // [584] 605

                /** 								idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3067 = (int)*(((s1_ptr)_2)->base + _a_5967);
                Ref(_3067);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3067;
                if( _1 != _3067 ){
                    DeRef(_1);
                }
                _3067 = NOVALUE;
L1F: 
L1E: 

                /** 					end for*/
                _a_5967 = _a_5967 + 1;
                goto L1C; // [608] 552
L1D: 
                ;
            }
            goto L5; // [613] 1304

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3068 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3068 = 1;
            }
            {
                int _a_5983;
                _a_5983 = 1;
L20: 
                if (_a_5983 > _3068){
                    goto L21; // [624] 692
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3069 = (int)*(((s1_ptr)_2)->base + _a_5983);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3070 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3069) && IS_ATOM_INT(_3070)){
                    _3071 = (_3069 < _3070) ? -1 : (_3069 > _3070);
                }
                else{
                    _3071 = compare(_3069, _3070);
                }
                _3069 = NOVALUE;
                _3070 = NOVALUE;
                if (_3071 <= 0)
                goto L22; // [645] 685

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3073 = (int)*(((s1_ptr)_2)->base + _a_5983);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3074 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3073) && IS_ATOM_INT(_3074)){
                    _3075 = (_3073 < _3074) ? -1 : (_3073 > _3074);
                }
                else{
                    _3075 = compare(_3073, _3074);
                }
                _3073 = NOVALUE;
                _3074 = NOVALUE;
                if (_3075 > 0)
                goto L23; // [663] 684

                /** 								idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3078 = (int)*(((s1_ptr)_2)->base + _a_5983);
                Ref(_3078);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3078;
                if( _1 != _3078 ){
                    DeRef(_1);
                }
                _3078 = NOVALUE;
L23: 
L22: 

                /** 					end for*/
                _a_5983 = _a_5983 + 1;
                goto L20; // [687] 631
L21: 
                ;
            }
            goto L5; // [692] 1304

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3079 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3079 = 1;
            }
            {
                int _a_5999;
                _a_5999 = 1;
L24: 
                if (_a_5999 > _3079){
                    goto L25; // [703] 771
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3080 = (int)*(((s1_ptr)_2)->base + _a_5999);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3081 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3080) && IS_ATOM_INT(_3081)){
                    _3082 = (_3080 < _3081) ? -1 : (_3080 > _3081);
                }
                else{
                    _3082 = compare(_3080, _3081);
                }
                _3080 = NOVALUE;
                _3081 = NOVALUE;
                if (_3082 <= 0)
                goto L26; // [724] 764

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3084 = (int)*(((s1_ptr)_2)->base + _a_5999);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3085 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3084) && IS_ATOM_INT(_3085)){
                    _3086 = (_3084 < _3085) ? -1 : (_3084 > _3085);
                }
                else{
                    _3086 = compare(_3084, _3085);
                }
                _3084 = NOVALUE;
                _3085 = NOVALUE;
                if (_3086 >= 0)
                goto L27; // [742] 763

                /** 								idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3089 = (int)*(((s1_ptr)_2)->base + _a_5999);
                Ref(_3089);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3089;
                if( _1 != _3089 ){
                    DeRef(_1);
                }
                _3089 = NOVALUE;
L27: 
L26: 

                /** 					end for*/
                _a_5999 = _a_5999 + 1;
                goto L24; // [766] 710
L25: 
                ;
            }
            goto L5; // [771] 1304

            /** 				case else*/
            case 0:
        ;}        goto L5; // [778] 1304

        /** 		case "out" then*/
        case 15:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_5853, _3091);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3093 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3093 = 1;
            }
            {
                int _a_6020;
                _a_6020 = 1;
L28: 
                if (_a_6020 > _3093){
                    goto L29; // [800] 845
                }

                /** 						if not find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3094 = (int)*(((s1_ptr)_2)->base + _a_6020);
                _3095 = find_from(_3094, _userdata_5852, 1);
                _3094 = NOVALUE;
                if (_3095 != 0)
                goto L2A; // [818] 838
                _3095 = NOVALUE;

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3098 = (int)*(((s1_ptr)_2)->base + _a_6020);
                Ref(_3098);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3098;
                if( _1 != _3098 ){
                    DeRef(_1);
                }
                _3098 = NOVALUE;
L2A: 

                /** 					end for*/
                _a_6020 = _a_6020 + 1;
                goto L28; // [840] 807
L29: 
                ;
            }
            goto L5; // [845] 1304

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3099 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3099 = 1;
            }
            {
                int _a_6030;
                _a_6030 = 1;
L2B: 
                if (_a_6030 > _3099){
                    goto L2C; // [856] 943
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3100 = (int)*(((s1_ptr)_2)->base + _a_6030);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3101 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3100) && IS_ATOM_INT(_3101)){
                    _3102 = (_3100 < _3101) ? -1 : (_3100 > _3101);
                }
                else{
                    _3102 = compare(_3100, _3101);
                }
                _3100 = NOVALUE;
                _3101 = NOVALUE;
                if (_3102 >= 0)
                goto L2D; // [877] 900

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3105 = (int)*(((s1_ptr)_2)->base + _a_6030);
                Ref(_3105);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3105;
                if( _1 != _3105 ){
                    DeRef(_1);
                }
                _3105 = NOVALUE;
                goto L2E; // [897] 936
L2D: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3106 = (int)*(((s1_ptr)_2)->base + _a_6030);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3107 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3106) && IS_ATOM_INT(_3107)){
                    _3108 = (_3106 < _3107) ? -1 : (_3106 > _3107);
                }
                else{
                    _3108 = compare(_3106, _3107);
                }
                _3106 = NOVALUE;
                _3107 = NOVALUE;
                if (_3108 <= 0)
                goto L2F; // [914] 935

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3111 = (int)*(((s1_ptr)_2)->base + _a_6030);
                Ref(_3111);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3111;
                if( _1 != _3111 ){
                    DeRef(_1);
                }
                _3111 = NOVALUE;
L2F: 
L2E: 

                /** 					end for*/
                _a_6030 = _a_6030 + 1;
                goto L2B; // [938] 863
L2C: 
                ;
            }
            goto L5; // [943] 1304

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3112 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3112 = 1;
            }
            {
                int _a_6048;
                _a_6048 = 1;
L30: 
                if (_a_6048 > _3112){
                    goto L31; // [954] 1041
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3113 = (int)*(((s1_ptr)_2)->base + _a_6048);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3114 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3113) && IS_ATOM_INT(_3114)){
                    _3115 = (_3113 < _3114) ? -1 : (_3113 > _3114);
                }
                else{
                    _3115 = compare(_3113, _3114);
                }
                _3113 = NOVALUE;
                _3114 = NOVALUE;
                if (_3115 >= 0)
                goto L32; // [975] 998

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3118 = (int)*(((s1_ptr)_2)->base + _a_6048);
                Ref(_3118);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3118;
                if( _1 != _3118 ){
                    DeRef(_1);
                }
                _3118 = NOVALUE;
                goto L33; // [995] 1034
L32: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3119 = (int)*(((s1_ptr)_2)->base + _a_6048);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3120 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3119) && IS_ATOM_INT(_3120)){
                    _3121 = (_3119 < _3120) ? -1 : (_3119 > _3120);
                }
                else{
                    _3121 = compare(_3119, _3120);
                }
                _3119 = NOVALUE;
                _3120 = NOVALUE;
                if (_3121 < 0)
                goto L34; // [1012] 1033

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3124 = (int)*(((s1_ptr)_2)->base + _a_6048);
                Ref(_3124);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3124;
                if( _1 != _3124 ){
                    DeRef(_1);
                }
                _3124 = NOVALUE;
L34: 
L33: 

                /** 					end for*/
                _a_6048 = _a_6048 + 1;
                goto L30; // [1036] 961
L31: 
                ;
            }
            goto L5; // [1041] 1304

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3125 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3125 = 1;
            }
            {
                int _a_6066;
                _a_6066 = 1;
L35: 
                if (_a_6066 > _3125){
                    goto L36; // [1052] 1139
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3126 = (int)*(((s1_ptr)_2)->base + _a_6066);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3127 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3126) && IS_ATOM_INT(_3127)){
                    _3128 = (_3126 < _3127) ? -1 : (_3126 > _3127);
                }
                else{
                    _3128 = compare(_3126, _3127);
                }
                _3126 = NOVALUE;
                _3127 = NOVALUE;
                if (_3128 > 0)
                goto L37; // [1073] 1096

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3131 = (int)*(((s1_ptr)_2)->base + _a_6066);
                Ref(_3131);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3131;
                if( _1 != _3131 ){
                    DeRef(_1);
                }
                _3131 = NOVALUE;
                goto L38; // [1093] 1132
L37: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3132 = (int)*(((s1_ptr)_2)->base + _a_6066);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3133 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3132) && IS_ATOM_INT(_3133)){
                    _3134 = (_3132 < _3133) ? -1 : (_3132 > _3133);
                }
                else{
                    _3134 = compare(_3132, _3133);
                }
                _3132 = NOVALUE;
                _3133 = NOVALUE;
                if (_3134 <= 0)
                goto L39; // [1110] 1131

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3137 = (int)*(((s1_ptr)_2)->base + _a_6066);
                Ref(_3137);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3137;
                if( _1 != _3137 ){
                    DeRef(_1);
                }
                _3137 = NOVALUE;
L39: 
L38: 

                /** 					end for*/
                _a_6066 = _a_6066 + 1;
                goto L35; // [1134] 1059
L36: 
                ;
            }
            goto L5; // [1139] 1304

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_5850)){
                    _3138 = SEQ_PTR(_source_5850)->length;
            }
            else {
                _3138 = 1;
            }
            {
                int _a_6084;
                _a_6084 = 1;
L3A: 
                if (_a_6084 > _3138){
                    goto L3B; // [1150] 1237
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3139 = (int)*(((s1_ptr)_2)->base + _a_6084);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3140 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_3139) && IS_ATOM_INT(_3140)){
                    _3141 = (_3139 < _3140) ? -1 : (_3139 > _3140);
                }
                else{
                    _3141 = compare(_3139, _3140);
                }
                _3139 = NOVALUE;
                _3140 = NOVALUE;
                if (_3141 > 0)
                goto L3C; // [1171] 1194

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3144 = (int)*(((s1_ptr)_2)->base + _a_6084);
                Ref(_3144);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3144;
                if( _1 != _3144 ){
                    DeRef(_1);
                }
                _3144 = NOVALUE;
                goto L3D; // [1191] 1230
L3C: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3145 = (int)*(((s1_ptr)_2)->base + _a_6084);
                _2 = (int)SEQ_PTR(_userdata_5852);
                _3146 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_3145) && IS_ATOM_INT(_3146)){
                    _3147 = (_3145 < _3146) ? -1 : (_3145 > _3146);
                }
                else{
                    _3147 = compare(_3145, _3146);
                }
                _3145 = NOVALUE;
                _3146 = NOVALUE;
                if (_3147 < 0)
                goto L3E; // [1208] 1229

                /** 							idx += 1*/
                _idx_5855 = _idx_5855 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_5850);
                _3150 = (int)*(((s1_ptr)_2)->base + _a_6084);
                Ref(_3150);
                _2 = (int)SEQ_PTR(_dest_5854);
                _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
                _1 = *(int *)_2;
                *(int *)_2 = _3150;
                if( _1 != _3150 ){
                    DeRef(_1);
                }
                _3150 = NOVALUE;
L3E: 
L3D: 

                /** 					end for*/
                _a_6084 = _a_6084 + 1;
                goto L3A; // [1232] 1157
L3B: 
                ;
            }
            goto L5; // [1237] 1304

            /** 				case else*/
            case 0:
        ;}        goto L5; // [1244] 1304

        /** 		case else*/
        case 0:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_5850)){
                _3151 = SEQ_PTR(_source_5850)->length;
        }
        else {
            _3151 = 1;
        }
        {
            int _a_6103;
            _a_6103 = 1;
L3F: 
            if (_a_6103 > _3151){
                goto L40; // [1255] 1303
            }

            /** 				if call_func(rid, {source[a], userdata}) then*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3152 = (int)*(((s1_ptr)_2)->base + _a_6103);
            Ref(_userdata_5852);
            Ref(_3152);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _3152;
            ((int *)_2)[2] = _userdata_5852;
            _3153 = MAKE_SEQ(_1);
            _3152 = NOVALUE;
            _1 = (int)SEQ_PTR(_3153);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_rid_5851].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8)
                                 );
            DeRef(_3154);
            _3154 = _1;
            DeRefDS(_3153);
            _3153 = NOVALUE;
            if (_3154 == 0) {
                DeRef(_3154);
                _3154 = NOVALUE;
                goto L41; // [1276] 1296
            }
            else {
                if (!IS_ATOM_INT(_3154) && DBL_PTR(_3154)->dbl == 0.0){
                    DeRef(_3154);
                    _3154 = NOVALUE;
                    goto L41; // [1276] 1296
                }
                DeRef(_3154);
                _3154 = NOVALUE;
            }
            DeRef(_3154);
            _3154 = NOVALUE;

            /** 					idx += 1*/
            _idx_5855 = _idx_5855 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_5850);
            _3156 = (int)*(((s1_ptr)_2)->base + _a_6103);
            Ref(_3156);
            _2 = (int)SEQ_PTR(_dest_5854);
            _2 = (int)(((s1_ptr)_2)->base + _idx_5855);
            _1 = *(int *)_2;
            *(int *)_2 = _3156;
            if( _1 != _3156 ){
                DeRef(_1);
            }
            _3156 = NOVALUE;
L41: 

            /** 			end for*/
            _a_6103 = _a_6103 + 1;
            goto L3F; // [1298] 1262
L40: 
            ;
        }
    ;}L5: 

    /** 	return dest[1..idx]*/
    rhs_slice_target = (object_ptr)&_3157;
    RHS_Slice(_dest_5854, 1, _idx_5855);
    DeRefDS(_source_5850);
    DeRef(_userdata_5852);
    DeRef(_rangetype_5853);
    DeRefDS(_dest_5854);
    return _3157;
    ;
}


int _21filter_alpha(int _elem_6115, int _ud_6116)
{
    int _3158 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return t_alpha(elem)*/
    Ref(_elem_6115);
    _3158 = _9t_alpha(_elem_6115);
    DeRef(_elem_6115);
    return _3158;
    ;
}


int _21split(int _st_6160, int _delim_6161, int _no_empty_6162, int _limit_6163)
{
    int _ret_6164 = NOVALUE;
    int _start_6165 = NOVALUE;
    int _pos_6166 = NOVALUE;
    int _k_6218 = NOVALUE;
    int _3226 = NOVALUE;
    int _3224 = NOVALUE;
    int _3223 = NOVALUE;
    int _3219 = NOVALUE;
    int _3218 = NOVALUE;
    int _3217 = NOVALUE;
    int _3214 = NOVALUE;
    int _3213 = NOVALUE;
    int _3208 = NOVALUE;
    int _3207 = NOVALUE;
    int _3203 = NOVALUE;
    int _3199 = NOVALUE;
    int _3197 = NOVALUE;
    int _3196 = NOVALUE;
    int _3192 = NOVALUE;
    int _3190 = NOVALUE;
    int _3189 = NOVALUE;
    int _3188 = NOVALUE;
    int _3187 = NOVALUE;
    int _3184 = NOVALUE;
    int _3183 = NOVALUE;
    int _3182 = NOVALUE;
    int _3181 = NOVALUE;
    int _3180 = NOVALUE;
    int _3178 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence ret = {}*/
    RefDS(_5);
    DeRef(_ret_6164);
    _ret_6164 = _5;

    /** 	if length(st) = 0 then*/
    if (IS_SEQUENCE(_st_6160)){
            _3178 = SEQ_PTR(_st_6160)->length;
    }
    else {
        _3178 = 1;
    }
    if (_3178 != 0)
    goto L1; // [19] 30

    /** 		return ret*/
    DeRefDS(_st_6160);
    DeRefi(_delim_6161);
    return _ret_6164;
L1: 

    /** 	if sequence(delim) then*/
    _3180 = IS_SEQUENCE(_delim_6161);
    if (_3180 == 0)
    {
        _3180 = NOVALUE;
        goto L2; // [35] 211
    }
    else{
        _3180 = NOVALUE;
    }

    /** 		if equal(delim, "") then*/
    if (_delim_6161 == _5)
    _3181 = 1;
    else if (IS_ATOM_INT(_delim_6161) && IS_ATOM_INT(_5))
    _3181 = 0;
    else
    _3181 = (compare(_delim_6161, _5) == 0);
    if (_3181 == 0)
    {
        _3181 = NOVALUE;
        goto L3; // [44] 127
    }
    else{
        _3181 = NOVALUE;
    }

    /** 			for i = 1 to length(st) do*/
    if (IS_SEQUENCE(_st_6160)){
            _3182 = SEQ_PTR(_st_6160)->length;
    }
    else {
        _3182 = 1;
    }
    {
        int _i_6175;
        _i_6175 = 1;
L4: 
        if (_i_6175 > _3182){
            goto L5; // [52] 120
        }

        /** 				st[i] = {st[i]}*/
        _2 = (int)SEQ_PTR(_st_6160);
        _3183 = (int)*(((s1_ptr)_2)->base + _i_6175);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_3183);
        *((int *)(_2+4)) = _3183;
        _3184 = MAKE_SEQ(_1);
        _3183 = NOVALUE;
        _2 = (int)SEQ_PTR(_st_6160);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _st_6160 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_6175);
        _1 = *(int *)_2;
        *(int *)_2 = _3184;
        if( _1 != _3184 ){
            DeRef(_1);
        }
        _3184 = NOVALUE;

        /** 				limit -= 1*/
        _limit_6163 = _limit_6163 - 1;

        /** 				if limit = 0 then*/
        if (_limit_6163 != 0)
        goto L6; // [81] 113

        /** 					st = append(st[1 .. i],st[i+1 .. $])*/
        rhs_slice_target = (object_ptr)&_3187;
        RHS_Slice(_st_6160, 1, _i_6175);
        _3188 = _i_6175 + 1;
        if (IS_SEQUENCE(_st_6160)){
                _3189 = SEQ_PTR(_st_6160)->length;
        }
        else {
            _3189 = 1;
        }
        rhs_slice_target = (object_ptr)&_3190;
        RHS_Slice(_st_6160, _3188, _3189);
        RefDS(_3190);
        Append(&_st_6160, _3187, _3190);
        DeRefDS(_3187);
        _3187 = NOVALUE;
        DeRefDS(_3190);
        _3190 = NOVALUE;

        /** 					exit*/
        goto L5; // [110] 120
L6: 

        /** 			end for*/
        _i_6175 = _i_6175 + 1;
        goto L4; // [115] 59
L5: 
        ;
    }

    /** 			return st*/
    DeRefi(_delim_6161);
    DeRef(_ret_6164);
    DeRef(_3188);
    _3188 = NOVALUE;
    return _st_6160;
L3: 

    /** 		start = 1*/
    _start_6165 = 1;

    /** 		while start <= length(st) do*/
L7: 
    if (IS_SEQUENCE(_st_6160)){
            _3192 = SEQ_PTR(_st_6160)->length;
    }
    else {
        _3192 = 1;
    }
    if (_start_6165 > _3192)
    goto L8; // [140] 290

    /** 			pos = match(delim, st, start)*/
    _pos_6166 = e_match_from(_delim_6161, _st_6160, _start_6165);

    /** 			if pos = 0 then*/
    if (_pos_6166 != 0)
    goto L9; // [153] 162

    /** 				exit*/
    goto L8; // [159] 290
L9: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _3196 = _pos_6166 - 1;
    rhs_slice_target = (object_ptr)&_3197;
    RHS_Slice(_st_6160, _start_6165, _3196);
    RefDS(_3197);
    Append(&_ret_6164, _ret_6164, _3197);
    DeRefDS(_3197);
    _3197 = NOVALUE;

    /** 			start = pos+length(delim)*/
    if (IS_SEQUENCE(_delim_6161)){
            _3199 = SEQ_PTR(_delim_6161)->length;
    }
    else {
        _3199 = 1;
    }
    _start_6165 = _pos_6166 + _3199;
    _3199 = NOVALUE;

    /** 			limit -= 1*/
    _limit_6163 = _limit_6163 - 1;

    /** 			if limit = 0 then*/
    if (_limit_6163 != 0)
    goto L7; // [194] 137

    /** 				exit*/
    goto L8; // [200] 290

    /** 		end while*/
    goto L7; // [205] 137
    goto L8; // [208] 290
L2: 

    /** 		start = 1*/
    _start_6165 = 1;

    /** 		while start <= length(st) do*/
LA: 
    if (IS_SEQUENCE(_st_6160)){
            _3203 = SEQ_PTR(_st_6160)->length;
    }
    else {
        _3203 = 1;
    }
    if (_start_6165 > _3203)
    goto LB; // [224] 289

    /** 			pos = find(delim, st, start)*/
    _pos_6166 = find_from(_delim_6161, _st_6160, _start_6165);

    /** 			if pos = 0 then*/
    if (_pos_6166 != 0)
    goto LC; // [237] 246

    /** 				exit*/
    goto LB; // [243] 289
LC: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _3207 = _pos_6166 - 1;
    rhs_slice_target = (object_ptr)&_3208;
    RHS_Slice(_st_6160, _start_6165, _3207);
    RefDS(_3208);
    Append(&_ret_6164, _ret_6164, _3208);
    DeRefDS(_3208);
    _3208 = NOVALUE;

    /** 			start = pos + 1*/
    _start_6165 = _pos_6166 + 1;

    /** 			limit -= 1*/
    _limit_6163 = _limit_6163 - 1;

    /** 			if limit = 0 then*/
    if (_limit_6163 != 0)
    goto LA; // [275] 221

    /** 				exit*/
    goto LB; // [281] 289

    /** 		end while*/
    goto LA; // [286] 221
LB: 
L8: 

    /** 	ret = append(ret, st[start..$])*/
    if (IS_SEQUENCE(_st_6160)){
            _3213 = SEQ_PTR(_st_6160)->length;
    }
    else {
        _3213 = 1;
    }
    rhs_slice_target = (object_ptr)&_3214;
    RHS_Slice(_st_6160, _start_6165, _3213);
    RefDS(_3214);
    Append(&_ret_6164, _ret_6164, _3214);
    DeRefDS(_3214);
    _3214 = NOVALUE;

    /** 	integer k = length(ret)*/
    if (IS_SEQUENCE(_ret_6164)){
            _k_6218 = SEQ_PTR(_ret_6164)->length;
    }
    else {
        _k_6218 = 1;
    }

    /** 	if no_empty then*/
    if (_no_empty_6162 == 0)
    {
        goto LD; // [313] 378
    }
    else{
    }

    /** 		k = 0*/
    _k_6218 = 0;

    /** 		for i = 1 to length(ret) do*/
    if (IS_SEQUENCE(_ret_6164)){
            _3217 = SEQ_PTR(_ret_6164)->length;
    }
    else {
        _3217 = 1;
    }
    {
        int _i_6222;
        _i_6222 = 1;
LE: 
        if (_i_6222 > _3217){
            goto LF; // [326] 377
        }

        /** 			if length(ret[i]) != 0 then*/
        _2 = (int)SEQ_PTR(_ret_6164);
        _3218 = (int)*(((s1_ptr)_2)->base + _i_6222);
        if (IS_SEQUENCE(_3218)){
                _3219 = SEQ_PTR(_3218)->length;
        }
        else {
            _3219 = 1;
        }
        _3218 = NOVALUE;
        if (_3219 == 0)
        goto L10; // [342] 370

        /** 				k += 1*/
        _k_6218 = _k_6218 + 1;

        /** 				if k != i then*/
        if (_k_6218 == _i_6222)
        goto L11; // [354] 369

        /** 					ret[k] = ret[i]*/
        _2 = (int)SEQ_PTR(_ret_6164);
        _3223 = (int)*(((s1_ptr)_2)->base + _i_6222);
        Ref(_3223);
        _2 = (int)SEQ_PTR(_ret_6164);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_6164 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _k_6218);
        _1 = *(int *)_2;
        *(int *)_2 = _3223;
        if( _1 != _3223 ){
            DeRef(_1);
        }
        _3223 = NOVALUE;
L11: 
L10: 

        /** 		end for*/
        _i_6222 = _i_6222 + 1;
        goto LE; // [372] 333
LF: 
        ;
    }
LD: 

    /** 	if k < length(ret) then*/
    if (IS_SEQUENCE(_ret_6164)){
            _3224 = SEQ_PTR(_ret_6164)->length;
    }
    else {
        _3224 = 1;
    }
    if (_k_6218 >= _3224)
    goto L12; // [383] 401

    /** 		return ret[1 .. k]*/
    rhs_slice_target = (object_ptr)&_3226;
    RHS_Slice(_ret_6164, 1, _k_6218);
    DeRefDS(_st_6160);
    DeRefi(_delim_6161);
    DeRefDS(_ret_6164);
    DeRef(_3196);
    _3196 = NOVALUE;
    DeRef(_3188);
    _3188 = NOVALUE;
    DeRef(_3207);
    _3207 = NOVALUE;
    _3218 = NOVALUE;
    return _3226;
    goto L13; // [398] 408
L12: 

    /** 		return ret*/
    DeRefDS(_st_6160);
    DeRefi(_delim_6161);
    DeRef(_3196);
    _3196 = NOVALUE;
    DeRef(_3188);
    _3188 = NOVALUE;
    DeRef(_3207);
    _3207 = NOVALUE;
    _3218 = NOVALUE;
    DeRef(_3226);
    _3226 = NOVALUE;
    return _ret_6164;
L13: 
    ;
}


int _21join(int _items_6287, int _delim_6288)
{
    int _ret_6290 = NOVALUE;
    int _3261 = NOVALUE;
    int _3260 = NOVALUE;
    int _3258 = NOVALUE;
    int _3257 = NOVALUE;
    int _3256 = NOVALUE;
    int _3255 = NOVALUE;
    int _3253 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(items) then return {} end if*/
    if (IS_SEQUENCE(_items_6287)){
            _3253 = SEQ_PTR(_items_6287)->length;
    }
    else {
        _3253 = 1;
    }
    if (_3253 != 0)
    goto L1; // [8] 16
    _3253 = NOVALUE;
    RefDS(_5);
    DeRefDS(_items_6287);
    DeRef(_ret_6290);
    return _5;
L1: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_6290);
    _ret_6290 = _5;

    /** 	for i=1 to length(items)-1 do*/
    if (IS_SEQUENCE(_items_6287)){
            _3255 = SEQ_PTR(_items_6287)->length;
    }
    else {
        _3255 = 1;
    }
    _3256 = _3255 - 1;
    _3255 = NOVALUE;
    {
        int _i_6295;
        _i_6295 = 1;
L2: 
        if (_i_6295 > _3256){
            goto L3; // [30] 58
        }

        /** 		ret &= items[i] & delim*/
        _2 = (int)SEQ_PTR(_items_6287);
        _3257 = (int)*(((s1_ptr)_2)->base + _i_6295);
        if (IS_SEQUENCE(_3257) && IS_ATOM(_delim_6288)) {
            Append(&_3258, _3257, _delim_6288);
        }
        else if (IS_ATOM(_3257) && IS_SEQUENCE(_delim_6288)) {
        }
        else {
            Concat((object_ptr)&_3258, _3257, _delim_6288);
            _3257 = NOVALUE;
        }
        _3257 = NOVALUE;
        if (IS_SEQUENCE(_ret_6290) && IS_ATOM(_3258)) {
        }
        else if (IS_ATOM(_ret_6290) && IS_SEQUENCE(_3258)) {
            Ref(_ret_6290);
            Prepend(&_ret_6290, _3258, _ret_6290);
        }
        else {
            Concat((object_ptr)&_ret_6290, _ret_6290, _3258);
        }
        DeRefDS(_3258);
        _3258 = NOVALUE;

        /** 	end for*/
        _i_6295 = _i_6295 + 1;
        goto L2; // [53] 37
L3: 
        ;
    }

    /** 	ret &= items[$]*/
    if (IS_SEQUENCE(_items_6287)){
            _3260 = SEQ_PTR(_items_6287)->length;
    }
    else {
        _3260 = 1;
    }
    _2 = (int)SEQ_PTR(_items_6287);
    _3261 = (int)*(((s1_ptr)_2)->base + _3260);
    if (IS_SEQUENCE(_ret_6290) && IS_ATOM(_3261)) {
        Ref(_3261);
        Append(&_ret_6290, _ret_6290, _3261);
    }
    else if (IS_ATOM(_ret_6290) && IS_SEQUENCE(_3261)) {
        Ref(_ret_6290);
        Prepend(&_ret_6290, _3261, _ret_6290);
    }
    else {
        Concat((object_ptr)&_ret_6290, _ret_6290, _3261);
    }
    _3261 = NOVALUE;

    /** 	return ret*/
    DeRefDS(_items_6287);
    DeRef(_3256);
    _3256 = NOVALUE;
    return _ret_6290;
    ;
}


int _21flatten(int _s_6398, int _delim_6399)
{
    int _ret_6400 = NOVALUE;
    int _x_6401 = NOVALUE;
    int _len_6402 = NOVALUE;
    int _pos_6403 = NOVALUE;
    int _temp_6422 = NOVALUE;
    int _3350 = NOVALUE;
    int _3349 = NOVALUE;
    int _3348 = NOVALUE;
    int _3346 = NOVALUE;
    int _3345 = NOVALUE;
    int _3344 = NOVALUE;
    int _3342 = NOVALUE;
    int _3340 = NOVALUE;
    int _3339 = NOVALUE;
    int _3338 = NOVALUE;
    int _3337 = NOVALUE;
    int _3335 = NOVALUE;
    int _3334 = NOVALUE;
    int _3333 = NOVALUE;
    int _3332 = NOVALUE;
    int _3331 = NOVALUE;
    int _3330 = NOVALUE;
    int _3329 = NOVALUE;
    int _3327 = NOVALUE;
    int _3326 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ret = s*/
    RefDS(_s_6398);
    DeRef(_ret_6400);
    _ret_6400 = _s_6398;

    /** 	pos = 1*/
    _pos_6403 = 1;

    /** 	len = length(ret)*/
    if (IS_SEQUENCE(_ret_6400)){
            _len_6402 = SEQ_PTR(_ret_6400)->length;
    }
    else {
        _len_6402 = 1;
    }

    /** 	while pos <= len do*/
L1: 
    if (_pos_6403 > _len_6402)
    goto L2; // [25] 189

    /** 		x = ret[pos]*/
    DeRef(_x_6401);
    _2 = (int)SEQ_PTR(_ret_6400);
    _x_6401 = (int)*(((s1_ptr)_2)->base + _pos_6403);
    Ref(_x_6401);

    /** 		if sequence(x) then*/
    _3326 = IS_SEQUENCE(_x_6401);
    if (_3326 == 0)
    {
        _3326 = NOVALUE;
        goto L3; // [40] 177
    }
    else{
        _3326 = NOVALUE;
    }

    /** 			if length(delim) = 0 then*/
    if (IS_SEQUENCE(_delim_6399)){
            _3327 = SEQ_PTR(_delim_6399)->length;
    }
    else {
        _3327 = 1;
    }
    if (_3327 != 0)
    goto L4; // [48] 92

    /** 				ret = ret[1..pos-1] & flatten(x) & ret[pos+1 .. $]*/
    _3329 = _pos_6403 - 1;
    rhs_slice_target = (object_ptr)&_3330;
    RHS_Slice(_ret_6400, 1, _3329);
    Ref(_x_6401);
    DeRef(_3331);
    _3331 = _x_6401;
    RefDS(_5);
    _3332 = _21flatten(_3331, _5);
    _3331 = NOVALUE;
    _3333 = _pos_6403 + 1;
    if (_3333 > MAXINT){
        _3333 = NewDouble((double)_3333);
    }
    if (IS_SEQUENCE(_ret_6400)){
            _3334 = SEQ_PTR(_ret_6400)->length;
    }
    else {
        _3334 = 1;
    }
    rhs_slice_target = (object_ptr)&_3335;
    RHS_Slice(_ret_6400, _3333, _3334);
    {
        int concat_list[3];

        concat_list[0] = _3335;
        concat_list[1] = _3332;
        concat_list[2] = _3330;
        Concat_N((object_ptr)&_ret_6400, concat_list, 3);
    }
    DeRefDS(_3335);
    _3335 = NOVALUE;
    DeRef(_3332);
    _3332 = NOVALUE;
    DeRefDS(_3330);
    _3330 = NOVALUE;
    goto L5; // [89] 169
L4: 

    /** 				sequence temp = ret[1..pos-1] & flatten(x)*/
    _3337 = _pos_6403 - 1;
    rhs_slice_target = (object_ptr)&_3338;
    RHS_Slice(_ret_6400, 1, _3337);
    Ref(_x_6401);
    DeRef(_3339);
    _3339 = _x_6401;
    RefDS(_5);
    _3340 = _21flatten(_3339, _5);
    _3339 = NOVALUE;
    if (IS_SEQUENCE(_3338) && IS_ATOM(_3340)) {
        Ref(_3340);
        Append(&_temp_6422, _3338, _3340);
    }
    else if (IS_ATOM(_3338) && IS_SEQUENCE(_3340)) {
    }
    else {
        Concat((object_ptr)&_temp_6422, _3338, _3340);
        DeRefDS(_3338);
        _3338 = NOVALUE;
    }
    DeRef(_3338);
    _3338 = NOVALUE;
    DeRef(_3340);
    _3340 = NOVALUE;

    /** 				if pos != length(ret) then*/
    if (IS_SEQUENCE(_ret_6400)){
            _3342 = SEQ_PTR(_ret_6400)->length;
    }
    else {
        _3342 = 1;
    }
    if (_pos_6403 == _3342)
    goto L6; // [120] 147

    /** 					ret = temp &  delim & ret[pos+1 .. $]*/
    _3344 = _pos_6403 + 1;
    if (_3344 > MAXINT){
        _3344 = NewDouble((double)_3344);
    }
    if (IS_SEQUENCE(_ret_6400)){
            _3345 = SEQ_PTR(_ret_6400)->length;
    }
    else {
        _3345 = 1;
    }
    rhs_slice_target = (object_ptr)&_3346;
    RHS_Slice(_ret_6400, _3344, _3345);
    {
        int concat_list[3];

        concat_list[0] = _3346;
        concat_list[1] = _delim_6399;
        concat_list[2] = _temp_6422;
        Concat_N((object_ptr)&_ret_6400, concat_list, 3);
    }
    DeRefDS(_3346);
    _3346 = NOVALUE;
    goto L7; // [144] 166
L6: 

    /** 					ret = temp & ret[pos+1 .. $]*/
    _3348 = _pos_6403 + 1;
    if (_3348 > MAXINT){
        _3348 = NewDouble((double)_3348);
    }
    if (IS_SEQUENCE(_ret_6400)){
            _3349 = SEQ_PTR(_ret_6400)->length;
    }
    else {
        _3349 = 1;
    }
    rhs_slice_target = (object_ptr)&_3350;
    RHS_Slice(_ret_6400, _3348, _3349);
    Concat((object_ptr)&_ret_6400, _temp_6422, _3350);
    DeRefDS(_3350);
    _3350 = NOVALUE;
L7: 
    DeRef(_temp_6422);
    _temp_6422 = NOVALUE;
L5: 

    /** 			len = length(ret)*/
    if (IS_SEQUENCE(_ret_6400)){
            _len_6402 = SEQ_PTR(_ret_6400)->length;
    }
    else {
        _len_6402 = 1;
    }
    goto L1; // [174] 25
L3: 

    /** 			pos += 1*/
    _pos_6403 = _pos_6403 + 1;

    /** 	end while*/
    goto L1; // [186] 25
L2: 

    /** 	return ret*/
    DeRefDS(_s_6398);
    DeRefi(_delim_6399);
    DeRef(_x_6401);
    DeRef(_3329);
    _3329 = NOVALUE;
    DeRef(_3337);
    _3337 = NOVALUE;
    DeRef(_3344);
    _3344 = NOVALUE;
    DeRef(_3333);
    _3333 = NOVALUE;
    DeRef(_3348);
    _3348 = NOVALUE;
    return _ret_6400;
    ;
}



// 0xCF99D388
