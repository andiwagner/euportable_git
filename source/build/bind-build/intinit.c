// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _70intoptions()
{
    int _pause_msg_64310 = NOVALUE;
    int _opts_array_64320 = NOVALUE;
    int _opts_64325 = NOVALUE;
    int _opt_keys_64334 = NOVALUE;
    int _option_w_64336 = NOVALUE;
    int _key_64340 = NOVALUE;
    int _val_64342 = NOVALUE;
    int _32429 = NOVALUE;
    int _32427 = NOVALUE;
    int _32426 = NOVALUE;
    int _32425 = NOVALUE;
    int _32423 = NOVALUE;
    int _32422 = NOVALUE;
    int _32421 = NOVALUE;
    int _32420 = NOVALUE;
    int _32415 = NOVALUE;
    int _32412 = NOVALUE;
    int _32410 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence pause_msg = GetMsgText(278, 0)*/
    RefDS(_22663);
    _0 = _pause_msg_64310;
    _pause_msg_64310 = _47GetMsgText(278, 0, _22663);
    DeRef(_0);

    /** 	Argv = expand_config_options(Argv)*/
    RefDS(_38Argv_16957);
    _0 = _45expand_config_options(_38Argv_16957);
    DeRefDS(_38Argv_16957);
    _38Argv_16957 = _0;

    /** 	Argc = length(Argv)*/
    if (IS_SEQUENCE(_38Argv_16957)){
            _38Argc_16956 = SEQ_PTR(_38Argv_16957)->length;
    }
    else {
        _38Argc_16956 = 1;
    }

    /** 	sequence opts_array = sort( get_options() )*/
    _32410 = _45get_options();
    _0 = _opts_array_64320;
    _opts_array_64320 = _22sort(_32410, 1);
    DeRef(_0);
    _32410 = NOVALUE;

    /** 	m:map opts = cmd_parse( opts_array, */
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 10;
    *((int *)(_2+8)) = 4;
    *((int *)(_2+12)) = 8;
    RefDS(_pause_msg_64310);
    *((int *)(_2+16)) = _pause_msg_64310;
    _32412 = MAKE_SEQ(_1);
    RefDS(_opts_array_64320);
    RefDS(_38Argv_16957);
    _0 = _opts_64325;
    _opts_64325 = _27cmd_parse(_opts_array_64320, _32412, _38Argv_16957);
    DeRef(_0);
    _32412 = NOVALUE;

    /** 	handle_common_options(opts)*/
    Ref(_opts_64325);
    _45handle_common_options(_opts_64325);

    /** 	sequence opt_keys = map:keys(opts)*/
    Ref(_opts_64325);
    _0 = _opt_keys_64334;
    _opt_keys_64334 = _29keys(_opts_64325, 0);
    DeRef(_0);

    /** 	integer option_w = 0*/
    _option_w_64336 = 0;

    /** 	for idx = 1 to length(opt_keys) do*/
    if (IS_SEQUENCE(_opt_keys_64334)){
            _32415 = SEQ_PTR(_opt_keys_64334)->length;
    }
    else {
        _32415 = 1;
    }
    {
        int _idx_64338;
        _idx_64338 = 1;
L1: 
        if (_idx_64338 > _32415){
            goto L2; // [89] 193
        }

        /** 		sequence key = opt_keys[idx]*/
        DeRef(_key_64340);
        _2 = (int)SEQ_PTR(_opt_keys_64334);
        _key_64340 = (int)*(((s1_ptr)_2)->base + _idx_64338);
        Ref(_key_64340);

        /** 		object val = map:get(opts, key)*/
        Ref(_opts_64325);
        RefDS(_key_64340);
        _0 = _val_64342;
        _val_64342 = _29get(_opts_64325, _key_64340, 0);
        DeRef(_0);

        /** 		switch key do*/
        _1 = find(_key_64340, _32418);
        switch ( _1 ){ 

            /** 			case "coverage" then*/
            case 1:

            /** 				for i = 1 to length( val ) do*/
            if (IS_SEQUENCE(_val_64342)){
                    _32420 = SEQ_PTR(_val_64342)->length;
            }
            else {
                _32420 = 1;
            }
            {
                int _i_64348;
                _i_64348 = 1;
L3: 
                if (_i_64348 > _32420){
                    goto L4; // [128] 151
                }

                /** 					add_coverage( val[i] )*/
                _2 = (int)SEQ_PTR(_val_64342);
                _32421 = (int)*(((s1_ptr)_2)->base + _i_64348);
                Ref(_32421);
                _52add_coverage(_32421);
                _32421 = NOVALUE;

                /** 				end for*/
                _i_64348 = _i_64348 + 1;
                goto L3; // [146] 135
L4: 
                ;
            }
            goto L5; // [151] 184

            /** 			case "coverage-db" then*/
            case 2:

            /** 				coverage_db( val )*/
            Ref(_val_64342);
            _52coverage_db(_val_64342);
            goto L5; // [162] 184

            /** 			case "coverage-erase" then*/
            case 3:

            /** 				new_coverage_db()*/
            _52new_coverage_db();
            goto L5; // [172] 184

            /** 			case "coverage-exclude" then*/
            case 4:

            /** 				coverage_exclude( val )*/
            Ref(_val_64342);
            _52coverage_exclude(_val_64342);
        ;}L5: 
        DeRef(_key_64340);
        _key_64340 = NOVALUE;
        DeRef(_val_64342);
        _val_64342 = NOVALUE;

        /** 	end for*/
        _idx_64338 = _idx_64338 + 1;
        goto L1; // [188] 96
L2: 
        ;
    }

    /** 	if length(m:get(opts, cmdline:EXTRAS)) = 0 then*/
    Ref(_opts_64325);
    RefDS(_27EXTRAS_14471);
    _32422 = _29get(_opts_64325, _27EXTRAS_14471, 0);
    if (IS_SEQUENCE(_32422)){
            _32423 = SEQ_PTR(_32422)->length;
    }
    else {
        _32423 = 1;
    }
    DeRef(_32422);
    _32422 = NOVALUE;
    if (_32423 != 0)
    goto L6; // [206] 254

    /** 		show_banner()*/
    _45show_banner();

    /** 		ShowMsg(2, 249)*/
    RefDS(_22663);
    _47ShowMsg(2, 249, _22663, 1);

    /** 		if not batch_job and not test_only then*/
    _32425 = (_38batch_job_16959 == 0);
    if (_32425 == 0) {
        goto L7; // [229] 249
    }
    _32427 = (_38test_only_16958 == 0);
    if (_32427 == 0)
    {
        DeRef(_32427);
        _32427 = NOVALUE;
        goto L7; // [239] 249
    }
    else{
        DeRef(_32427);
        _32427 = NOVALUE;
    }

    /** 			maybe_any_key(pause_msg)*/
    RefDS(_pause_msg_64310);
    _28maybe_any_key(_pause_msg_64310, 1);
L7: 

    /** 		abort(1)*/
    UserCleanup(1);
L6: 

    /** 	OpDefines &= { "EUI" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_32428);
    *((int *)(_2+4)) = _32428;
    _32429 = MAKE_SEQ(_1);
    Concat((object_ptr)&_38OpDefines_17019, _38OpDefines_17019, _32429);
    DeRefDS(_32429);
    _32429 = NOVALUE;

    /** 	finalize_command_line(opts)*/
    Ref(_opts_64325);
    _45finalize_command_line(_opts_64325);

    /** end procedure*/
    DeRef(_pause_msg_64310);
    DeRef(_opts_array_64320);
    DeRef(_opts_64325);
    DeRef(_opt_keys_64334);
    _32422 = NOVALUE;
    DeRef(_32425);
    _32425 = NOVALUE;
    return;
    ;
}



// 0xB88134D2
