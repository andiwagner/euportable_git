// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _47GetMsgText(int _MsgNum_21886, int _WithNum_21887, int _Args_21888)
{
    int _idx_21889 = NOVALUE;
    int _msgtext_21890 = NOVALUE;
    int _12813 = NOVALUE;
    int _12812 = NOVALUE;
    int _12808 = NOVALUE;
    int _12807 = NOVALUE;
    int _12805 = NOVALUE;
    int _12803 = NOVALUE;
    int _12801 = NOVALUE;
    int _12800 = NOVALUE;
    int _12799 = NOVALUE;
    int _12798 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_MsgNum_21886)) {
        _1 = (long)(DBL_PTR(_MsgNum_21886)->dbl);
        if (UNIQUE(DBL_PTR(_MsgNum_21886)) && (DBL_PTR(_MsgNum_21886)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_MsgNum_21886);
        _MsgNum_21886 = _1;
    }

    /** 	integer idx = 1*/
    _idx_21889 = 1;

    /** 	msgtext = get_text( MsgNum, LocalizeQual, LocalDB )*/
    RefDS(_35LocalizeQual_15614);
    RefDS(_35LocalDB_15615);
    _0 = _msgtext_21890;
    _msgtext_21890 = _48get_text(_MsgNum_21886, _35LocalizeQual_15614, _35LocalDB_15615);
    DeRef(_0);

    /** 	if atom(msgtext) then*/
    _12798 = IS_ATOM(_msgtext_21890);
    if (_12798 == 0)
    {
        _12798 = NOVALUE;
        goto L1; // [27] 90
    }
    else{
        _12798 = NOVALUE;
    }

    /** 		for i = 1 to length(StdErrMsgs) do*/
    _12799 = 358;
    {
        int _i_21898;
        _i_21898 = 1;
L2: 
        if (_i_21898 > 358){
            goto L3; // [37] 77
        }

        /** 			if StdErrMsgs[i][1] = MsgNum then*/
        _2 = (int)SEQ_PTR(_47StdErrMsgs_20898);
        _12800 = (int)*(((s1_ptr)_2)->base + _i_21898);
        _2 = (int)SEQ_PTR(_12800);
        _12801 = (int)*(((s1_ptr)_2)->base + 1);
        _12800 = NOVALUE;
        if (binary_op_a(NOTEQ, _12801, _MsgNum_21886)){
            _12801 = NOVALUE;
            goto L4; // [56] 70
        }
        _12801 = NOVALUE;

        /** 				idx = i*/
        _idx_21889 = _i_21898;

        /** 				exit*/
        goto L3; // [67] 77
L4: 

        /** 		end for*/
        _i_21898 = _i_21898 + 1;
        goto L2; // [72] 44
L3: 
        ;
    }

    /** 		msgtext = StdErrMsgs[idx][2]*/
    _2 = (int)SEQ_PTR(_47StdErrMsgs_20898);
    _12803 = (int)*(((s1_ptr)_2)->base + _idx_21889);
    DeRef(_msgtext_21890);
    _2 = (int)SEQ_PTR(_12803);
    _msgtext_21890 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_msgtext_21890);
    _12803 = NOVALUE;
L1: 

    /** 	if atom(Args) or length(Args) != 0 then*/
    _12805 = IS_ATOM(_Args_21888);
    if (_12805 != 0) {
        goto L5; // [95] 111
    }
    if (IS_SEQUENCE(_Args_21888)){
            _12807 = SEQ_PTR(_Args_21888)->length;
    }
    else {
        _12807 = 1;
    }
    _12808 = (_12807 != 0);
    _12807 = NOVALUE;
    if (_12808 == 0)
    {
        DeRef(_12808);
        _12808 = NOVALUE;
        goto L6; // [107] 119
    }
    else{
        DeRef(_12808);
        _12808 = NOVALUE;
    }
L5: 

    /** 		msgtext = format(msgtext, Args)*/
    Ref(_msgtext_21890);
    Ref(_Args_21888);
    _0 = _msgtext_21890;
    _msgtext_21890 = _10format(_msgtext_21890, _Args_21888);
    DeRef(_0);
L6: 

    /** 	if WithNum != 0 then*/
    if (_WithNum_21887 == 0)
    goto L7; // [121] 142

    /** 		return sprintf("<%04d>:: %s", {MsgNum, msgtext})*/
    Ref(_msgtext_21890);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _MsgNum_21886;
    ((int *)_2)[2] = _msgtext_21890;
    _12812 = MAKE_SEQ(_1);
    _12813 = EPrintf(-9999999, _12811, _12812);
    DeRefDS(_12812);
    _12812 = NOVALUE;
    DeRef(_Args_21888);
    DeRef(_msgtext_21890);
    return _12813;
    goto L8; // [139] 149
L7: 

    /** 		return msgtext*/
    DeRef(_Args_21888);
    DeRef(_12813);
    _12813 = NOVALUE;
    return _msgtext_21890;
L8: 
    ;
}


void _47ShowMsg(int _Cons_21921, int _Msg_21922, int _Args_21923, int _NL_21924)
{
    int _12820 = NOVALUE;
    int _12819 = NOVALUE;
    int _12817 = NOVALUE;
    int _12815 = NOVALUE;
    int _12814 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(Msg) then*/
    _12814 = 1;
    if (_12814 == 0)
    {
        _12814 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _12814 = NOVALUE;
    }

    /** 		Msg = GetMsgText(floor(Msg), 0)*/
    _12815 = e_floor(_Msg_21922);
    RefDS(_5);
    _Msg_21922 = _47GetMsgText(_12815, 0, _5);
    _12815 = NOVALUE;
L1: 

    /** 	if atom(Args) or length(Args) != 0 then*/
    _12817 = IS_ATOM(_Args_21923);
    if (_12817 != 0) {
        goto L2; // [30] 46
    }
    if (IS_SEQUENCE(_Args_21923)){
            _12819 = SEQ_PTR(_Args_21923)->length;
    }
    else {
        _12819 = 1;
    }
    _12820 = (_12819 != 0);
    _12819 = NOVALUE;
    if (_12820 == 0)
    {
        DeRef(_12820);
        _12820 = NOVALUE;
        goto L3; // [42] 54
    }
    else{
        DeRef(_12820);
        _12820 = NOVALUE;
    }
L2: 

    /** 		Msg = format(Msg, Args)*/
    Ref(_Msg_21922);
    Ref(_Args_21923);
    _0 = _Msg_21922;
    _Msg_21922 = _10format(_Msg_21922, _Args_21923);
    DeRef(_0);
L3: 

    /** 	puts(Cons, Msg)*/
    EPuts(_Cons_21921, _Msg_21922); // DJP 

    /** 	if NL then*/
    if (_NL_21924 == 0)
    {
        goto L4; // [61] 70
    }
    else{
    }

    /** 		puts(Cons, '\n')*/
    EPuts(_Cons_21921, 10); // DJP 
L4: 

    /** end procedure*/
    DeRef(_Msg_21922);
    DeRef(_Args_21923);
    return;
    ;
}



// 0xBD1EB382
