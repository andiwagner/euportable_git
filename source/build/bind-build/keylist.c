// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _65find_category(int _tokid_24376)
{
    int _catname_24377 = NOVALUE;
    int _14184 = NOVALUE;
    int _14183 = NOVALUE;
    int _14181 = NOVALUE;
    int _14180 = NOVALUE;
    int _14179 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tokid_24376)) {
        _1 = (long)(DBL_PTR(_tokid_24376)->dbl);
        if (UNIQUE(DBL_PTR(_tokid_24376)) && (DBL_PTR(_tokid_24376)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tokid_24376);
        _tokid_24376 = _1;
    }

    /** 	sequence catname = "reserved word"*/
    RefDS(_14178);
    DeRef(_catname_24377);
    _catname_24377 = _14178;

    /** 	for i = 1 to length(token_category) do*/
    _14179 = 73;
    {
        int _i_24380;
        _i_24380 = 1;
L1: 
        if (_i_24380 > 73){
            goto L2; // [17] 72
        }

        /** 		if token_category[i][1] = tokid then*/
        _2 = (int)SEQ_PTR(_39token_category_16472);
        _14180 = (int)*(((s1_ptr)_2)->base + _i_24380);
        _2 = (int)SEQ_PTR(_14180);
        _14181 = (int)*(((s1_ptr)_2)->base + 1);
        _14180 = NOVALUE;
        if (binary_op_a(NOTEQ, _14181, _tokid_24376)){
            _14181 = NOVALUE;
            goto L3; // [36] 65
        }
        _14181 = NOVALUE;

        /** 			catname = token_catname[token_category[i][2]]*/
        _2 = (int)SEQ_PTR(_39token_category_16472);
        _14183 = (int)*(((s1_ptr)_2)->base + _i_24380);
        _2 = (int)SEQ_PTR(_14183);
        _14184 = (int)*(((s1_ptr)_2)->base + 2);
        _14183 = NOVALUE;
        DeRef(_catname_24377);
        _2 = (int)SEQ_PTR(_39token_catname_16459);
        if (!IS_ATOM_INT(_14184)){
            _catname_24377 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14184)->dbl));
        }
        else{
            _catname_24377 = (int)*(((s1_ptr)_2)->base + _14184);
        }
        RefDS(_catname_24377);

        /** 			exit*/
        goto L2; // [62] 72
L3: 

        /** 	end for*/
        _i_24380 = _i_24380 + 1;
        goto L1; // [67] 24
L2: 
        ;
    }

    /** 	return catname*/
    _14184 = NOVALUE;
    return _catname_24377;
    ;
}


int _65find_token_text(int _tokid_24395)
{
    int _14193 = NOVALUE;
    int _14191 = NOVALUE;
    int _14190 = NOVALUE;
    int _14188 = NOVALUE;
    int _14187 = NOVALUE;
    int _14186 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_tokid_24395)) {
        _1 = (long)(DBL_PTR(_tokid_24395)->dbl);
        if (UNIQUE(DBL_PTR(_tokid_24395)) && (DBL_PTR(_tokid_24395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_tokid_24395);
        _tokid_24395 = _1;
    }

    /** 	for i = 1 to length(keylist) do*/
    if (IS_SEQUENCE(_65keylist_23552)){
            _14186 = SEQ_PTR(_65keylist_23552)->length;
    }
    else {
        _14186 = 1;
    }
    {
        int _i_24397;
        _i_24397 = 1;
L1: 
        if (_i_24397 > _14186){
            goto L2; // [10] 57
        }

        /** 		if keylist[i][3] = tokid then*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _14187 = (int)*(((s1_ptr)_2)->base + _i_24397);
        _2 = (int)SEQ_PTR(_14187);
        _14188 = (int)*(((s1_ptr)_2)->base + 3);
        _14187 = NOVALUE;
        if (binary_op_a(NOTEQ, _14188, _tokid_24395)){
            _14188 = NOVALUE;
            goto L3; // [29] 50
        }
        _14188 = NOVALUE;

        /** 			return keylist[i][1]*/
        _2 = (int)SEQ_PTR(_65keylist_23552);
        _14190 = (int)*(((s1_ptr)_2)->base + _i_24397);
        _2 = (int)SEQ_PTR(_14190);
        _14191 = (int)*(((s1_ptr)_2)->base + 1);
        _14190 = NOVALUE;
        Ref(_14191);
        return _14191;
L3: 

        /** 	end for*/
        _i_24397 = _i_24397 + 1;
        goto L1; // [52] 17
L2: 
        ;
    }

    /** 	return LexName(tokid, "unknown word")*/
    RefDS(_14192);
    _14193 = _43LexName(_tokid_24395, _14192);
    _14191 = NOVALUE;
    return _14193;
    ;
}



// 0xAA07379D
