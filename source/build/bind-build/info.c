// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _26version_major()
{
    int _6166 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return version_info[MAJ_VER]*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6166 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_6166);
    return _6166;
    ;
}


int _26version_minor()
{
    int _6167 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return version_info[MIN_VER]*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6167 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_6167);
    return _6167;
    ;
}


int _26version_patch()
{
    int _6168 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return version_info[PAT_VER]*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6168 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_6168);
    return _6168;
    ;
}


int _26version_node(int _full_10950)
{
    int _6175 = NOVALUE;
    int _6174 = NOVALUE;
    int _6173 = NOVALUE;
    int _6172 = NOVALUE;
    int _6171 = NOVALUE;
    int _6170 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or length(version_info[NODE]) < 12 then*/
    if (0 != 0) {
        goto L1; // [5] 29
    }
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6170 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6170)){
            _6171 = SEQ_PTR(_6170)->length;
    }
    else {
        _6171 = 1;
    }
    _6170 = NOVALUE;
    _6172 = (_6171 < 12);
    _6171 = NOVALUE;
    if (_6172 == 0)
    {
        DeRef(_6172);
        _6172 = NOVALUE;
        goto L2; // [25] 44
    }
    else{
        DeRef(_6172);
        _6172 = NOVALUE;
    }
L1: 

    /** 		return version_info[NODE]*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6173 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_6173);
    _6170 = NOVALUE;
    return _6173;
L2: 

    /** 	return version_info[NODE][1..12]*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6174 = (int)*(((s1_ptr)_2)->base + 5);
    rhs_slice_target = (object_ptr)&_6175;
    RHS_Slice(_6174, 1, 12);
    _6174 = NOVALUE;
    _6170 = NOVALUE;
    _6173 = NOVALUE;
    return _6175;
    ;
}


int _26version_date(int _full_10964)
{
    int _6184 = NOVALUE;
    int _6183 = NOVALUE;
    int _6182 = NOVALUE;
    int _6181 = NOVALUE;
    int _6180 = NOVALUE;
    int _6179 = NOVALUE;
    int _6177 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental or length(version_info[REVISION_DATE]) < 10 then*/
    if (_full_10964 != 0) {
        _6177 = 1;
        goto L1; // [5] 15
    }
    _6177 = (_26is_developmental_10914 != 0);
L1: 
    if (_6177 != 0) {
        goto L2; // [15] 39
    }
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6179 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_6179)){
            _6180 = SEQ_PTR(_6179)->length;
    }
    else {
        _6180 = 1;
    }
    _6179 = NOVALUE;
    _6181 = (_6180 < 10);
    _6180 = NOVALUE;
    if (_6181 == 0)
    {
        DeRef(_6181);
        _6181 = NOVALUE;
        goto L3; // [35] 54
    }
    else{
        DeRef(_6181);
        _6181 = NOVALUE;
    }
L2: 

    /** 		return version_info[REVISION_DATE]*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6182 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_6182);
    _6179 = NOVALUE;
    return _6182;
L3: 

    /** 	return version_info[REVISION_DATE][1..10]*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6183 = (int)*(((s1_ptr)_2)->base + 7);
    rhs_slice_target = (object_ptr)&_6184;
    RHS_Slice(_6183, 1, 10);
    _6183 = NOVALUE;
    _6179 = NOVALUE;
    _6182 = NOVALUE;
    return _6184;
    ;
}


int _26version_string(int _full_10979)
{
    int _version_revision_inlined_version_revision_at_49_10988 = NOVALUE;
    int _6204 = NOVALUE;
    int _6203 = NOVALUE;
    int _6202 = NOVALUE;
    int _6201 = NOVALUE;
    int _6200 = NOVALUE;
    int _6199 = NOVALUE;
    int _6198 = NOVALUE;
    int _6197 = NOVALUE;
    int _6195 = NOVALUE;
    int _6194 = NOVALUE;
    int _6193 = NOVALUE;
    int _6192 = NOVALUE;
    int _6191 = NOVALUE;
    int _6190 = NOVALUE;
    int _6189 = NOVALUE;
    int _6188 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental then*/
    if (0 != 0) {
        goto L1; // [5] 16
    }
    if (_26is_developmental_10914 == 0)
    {
        goto L2; // [12] 90
    }
    else{
    }
L1: 

    /** 		return sprintf("%d.%d.%d %s (%d:%s, %s)", {*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6188 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6189 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6190 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6191 = (int)*(((s1_ptr)_2)->base + 4);

    /** 	return version_info[REVISION]*/
    DeRef(_version_revision_inlined_version_revision_at_49_10988);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _version_revision_inlined_version_revision_at_49_10988 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_version_revision_inlined_version_revision_at_49_10988);
    _6192 = _26version_node(0);
    _6193 = _26version_date(_full_10979);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6188);
    *((int *)(_2+4)) = _6188;
    Ref(_6189);
    *((int *)(_2+8)) = _6189;
    Ref(_6190);
    *((int *)(_2+12)) = _6190;
    Ref(_6191);
    *((int *)(_2+16)) = _6191;
    Ref(_version_revision_inlined_version_revision_at_49_10988);
    *((int *)(_2+20)) = _version_revision_inlined_version_revision_at_49_10988;
    *((int *)(_2+24)) = _6192;
    *((int *)(_2+28)) = _6193;
    _6194 = MAKE_SEQ(_1);
    _6193 = NOVALUE;
    _6192 = NOVALUE;
    _6191 = NOVALUE;
    _6190 = NOVALUE;
    _6189 = NOVALUE;
    _6188 = NOVALUE;
    _6195 = EPrintf(-9999999, _6187, _6194);
    DeRefDS(_6194);
    _6194 = NOVALUE;
    return _6195;
    goto L3; // [87] 150
L2: 

    /** 		return sprintf("%d.%d.%d %s (%s, %s)", {*/
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6197 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6198 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6199 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_26version_info_10912);
    _6200 = (int)*(((s1_ptr)_2)->base + 4);
    _6201 = _26version_node(0);
    _6202 = _26version_date(_full_10979);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6197);
    *((int *)(_2+4)) = _6197;
    Ref(_6198);
    *((int *)(_2+8)) = _6198;
    Ref(_6199);
    *((int *)(_2+12)) = _6199;
    Ref(_6200);
    *((int *)(_2+16)) = _6200;
    *((int *)(_2+20)) = _6201;
    *((int *)(_2+24)) = _6202;
    _6203 = MAKE_SEQ(_1);
    _6202 = NOVALUE;
    _6201 = NOVALUE;
    _6200 = NOVALUE;
    _6199 = NOVALUE;
    _6198 = NOVALUE;
    _6197 = NOVALUE;
    _6204 = EPrintf(-9999999, _6196, _6203);
    DeRefDS(_6203);
    _6203 = NOVALUE;
    DeRef(_6195);
    _6195 = NOVALUE;
    return _6204;
L3: 
    ;
}


int _26euphoria_copyright()
{
    int _version_string_long_1__tmp_at2_11022 = NOVALUE;
    int _version_string_long_inlined_version_string_long_at_2_11021 = NOVALUE;
    int _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_11020 = NOVALUE;
    int _6214 = NOVALUE;
    int _6212 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {*/

    /** 	return version_string(full) & " for " & platform_name()*/
    _0 = _version_string_long_1__tmp_at2_11022;
    _version_string_long_1__tmp_at2_11022 = _26version_string(0);
    DeRef(_0);

    /** 	ifdef WINDOWS then*/

    /** 		return "Windows"*/
    RefDS(_6151);
    DeRefi(_platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_11020);
    _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_11020 = _6151;
    {
        int concat_list[3];

        concat_list[0] = _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_11020;
        concat_list[1] = _6209;
        concat_list[2] = _version_string_long_1__tmp_at2_11022;
        Concat_N((object_ptr)&_version_string_long_inlined_version_string_long_at_2_11021, concat_list, 3);
    }
    DeRefi(_platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_11020);
    _platform_name_inlined_platform_name_at_8_inlined_version_string_long_at_2_11020 = NOVALUE;
    DeRef(_version_string_long_1__tmp_at2_11022);
    _version_string_long_1__tmp_at2_11022 = NOVALUE;
    Concat((object_ptr)&_6212, _6211, _version_string_long_inlined_version_string_long_at_2_11021);
    RefDS(_6213);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6212;
    ((int *)_2)[2] = _6213;
    _6214 = MAKE_SEQ(_1);
    _6212 = NOVALUE;
    return _6214;
    ;
}


int _26all_copyrights()
{
    int _pcre_copyright_inlined_pcre_copyright_at_5_11035 = NOVALUE;
    int _6219 = NOVALUE;
    int _6218 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {*/
    _6218 = _26euphoria_copyright();

    /** 	return {*/
    RefDS(_6216);
    RefDS(_6215);
    DeRef(_pcre_copyright_inlined_pcre_copyright_at_5_11035);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6215;
    ((int *)_2)[2] = _6216;
    _pcre_copyright_inlined_pcre_copyright_at_5_11035 = MAKE_SEQ(_1);
    RefDS(_pcre_copyright_inlined_pcre_copyright_at_5_11035);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6218;
    ((int *)_2)[2] = _pcre_copyright_inlined_pcre_copyright_at_5_11035;
    _6219 = MAKE_SEQ(_1);
    _6218 = NOVALUE;
    return _6219;
    ;
}



// 0xEAADF2CE
