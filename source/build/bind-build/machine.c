// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _4allocate(int _n_983, int _cleanup_984)
{
    int _iaddr_985 = NOVALUE;
    int _eaddr_986 = NOVALUE;
    int _380 = NOVALUE;
    int _379 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE then*/

    /** 		iaddr = eu:machine_func( memconst:M_ALLOC, n + memory:BORDER_SPACE * 2)*/
    _379 = 0;
    if (IS_ATOM_INT(_n_983)) {
        _380 = _n_983 + 0;
        if ((long)((unsigned long)_380 + (unsigned long)HIGH_BITS) >= 0) 
        _380 = NewDouble((double)_380);
    }
    else {
        _380 = NewDouble(DBL_PTR(_n_983)->dbl + (double)0);
    }
    _379 = NOVALUE;
    DeRef(_iaddr_985);
    _iaddr_985 = machine(16, _380);
    DeRef(_380);
    _380 = NOVALUE;

    /** 		eaddr = memory:prepare_block( iaddr, n, PAGE_READ_WRITE )*/
    Ref(_iaddr_985);
    Ref(_n_983);
    _0 = _eaddr_986;
    _eaddr_986 = _6prepare_block(_iaddr_985, _n_983, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 	return eaddr*/
    DeRef(_n_983);
    DeRef(_iaddr_985);
    return _eaddr_986;
    ;
}


int _4allocate_data(int _n_996, int _cleanup_997)
{
    int _a_998 = NOVALUE;
    int _sla_1000 = NOVALUE;
    int _385 = NOVALUE;
    int _384 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = eu:machine_func( memconst:M_ALLOC, n+BORDER_SPACE*2)*/
    _384 = 0;
    _385 = 4;
    _384 = NOVALUE;
    DeRef(_a_998);
    _a_998 = machine(16, 4);
    _385 = NOVALUE;

    /** 	sla = memory:prepare_block(a, n, PAGE_READ_WRITE )*/
    Ref(_a_998);
    _0 = _sla_1000;
    _sla_1000 = _6prepare_block(_a_998, 4, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 		return sla*/
    DeRef(_a_998);
    return _sla_1000;
    ;
}


int _4VirtualAlloc(int _addr_1158, int _size_1159, int _allocation_type_1160, int _protect__1161)
{
    int _r1_1162 = NOVALUE;
    int _469 = NOVALUE;
    int _0, _1, _2;
    

    /** 		r1 = c_func( VirtualAlloc_rid, {addr, size, allocation_type, protect_ } )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 1;
    Ref(_allocation_type_1160);
    *((int *)(_2+12)) = _allocation_type_1160;
    *((int *)(_2+16)) = 64;
    _469 = MAKE_SEQ(_1);
    DeRef(_r1_1162);
    _r1_1162 = call_c(1, _4VirtualAlloc_rid_1096, _469);
    DeRefDS(_469);
    _469 = NOVALUE;

    /** 		return r1*/
    DeRef(_allocation_type_1160);
    return _r1_1162;
    ;
}


int _4allocate_string(int _s_1181, int _cleanup_1182)
{
    int _mem_1183 = NOVALUE;
    int _476 = NOVALUE;
    int _475 = NOVALUE;
    int _473 = NOVALUE;
    int _472 = NOVALUE;
    int _0, _1, _2;
    

    /** 	mem = allocate( length(s) + 1) -- Thanks to Igor*/
    if (IS_SEQUENCE(_s_1181)){
            _472 = SEQ_PTR(_s_1181)->length;
    }
    else {
        _472 = 1;
    }
    _473 = _472 + 1;
    _472 = NOVALUE;
    _0 = _mem_1183;
    _mem_1183 = _4allocate(_473, 0);
    DeRef(_0);
    _473 = NOVALUE;

    /** 	if mem then*/
    if (_mem_1183 == 0) {
        goto L1; // [19] 54
    }
    else {
        if (!IS_ATOM_INT(_mem_1183) && DBL_PTR(_mem_1183)->dbl == 0.0){
            goto L1; // [19] 54
        }
    }

    /** 		poke(mem, s)*/
    if (IS_ATOM_INT(_mem_1183)){
        poke_addr = (unsigned char *)_mem_1183;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_mem_1183)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_1181);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 		poke(mem+length(s), 0)  -- Thanks to Aku*/
    if (IS_SEQUENCE(_s_1181)){
            _475 = SEQ_PTR(_s_1181)->length;
    }
    else {
        _475 = 1;
    }
    if (IS_ATOM_INT(_mem_1183)) {
        _476 = _mem_1183 + _475;
        if ((long)((unsigned long)_476 + (unsigned long)HIGH_BITS) >= 0) 
        _476 = NewDouble((double)_476);
    }
    else {
        _476 = NewDouble(DBL_PTR(_mem_1183)->dbl + (double)_475);
    }
    _475 = NOVALUE;
    if (IS_ATOM_INT(_476)){
        poke_addr = (unsigned char *)_476;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_476)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_476);
    _476 = NOVALUE;

    /** 		if cleanup then*/
L1: 

    /** 	return mem*/
    DeRefDS(_s_1181);
    return _mem_1183;
    ;
}


void _4free(int _addr_1290)
{
    int _msg_inlined_crash_at_27_1299 = NOVALUE;
    int _data_inlined_crash_at_24_1298 = NOVALUE;
    int _addr_inlined_deallocate_at_64_1305 = NOVALUE;
    int _msg_inlined_crash_at_106_1310 = NOVALUE;
    int _519 = NOVALUE;
    int _518 = NOVALUE;
    int _517 = NOVALUE;
    int _516 = NOVALUE;
    int _514 = NOVALUE;
    int _513 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if types:number_array (addr) then*/
    Ref(_addr_1290);
    _513 = _9number_array(_addr_1290);
    if (_513 == 0) {
        DeRef(_513);
        _513 = NOVALUE;
        goto L1; // [7] 97
    }
    else {
        if (!IS_ATOM_INT(_513) && DBL_PTR(_513)->dbl == 0.0){
            DeRef(_513);
            _513 = NOVALUE;
            goto L1; // [7] 97
        }
        DeRef(_513);
        _513 = NOVALUE;
    }
    DeRef(_513);
    _513 = NOVALUE;

    /** 		if types:ascii_string(addr) then*/
    Ref(_addr_1290);
    _514 = _9ascii_string(_addr_1290);
    if (_514 == 0) {
        DeRef(_514);
        _514 = NOVALUE;
        goto L2; // [16] 47
    }
    else {
        if (!IS_ATOM_INT(_514) && DBL_PTR(_514)->dbl == 0.0){
            DeRef(_514);
            _514 = NOVALUE;
            goto L2; // [16] 47
        }
        DeRef(_514);
        _514 = NOVALUE;
    }
    DeRef(_514);
    _514 = NOVALUE;

    /** 			error:crash("free(\"%s\") is not a valid address", {addr})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_1290);
    *((int *)(_2+4)) = _addr_1290;
    _516 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_24_1298);
    _data_inlined_crash_at_24_1298 = _516;
    _516 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_27_1299);
    _msg_inlined_crash_at_27_1299 = EPrintf(-9999999, _515, _data_inlined_crash_at_24_1298);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_27_1299);

    /** end procedure*/
    goto L3; // [41] 44
L3: 
    DeRef(_data_inlined_crash_at_24_1298);
    _data_inlined_crash_at_24_1298 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_27_1299);
    _msg_inlined_crash_at_27_1299 = NOVALUE;
L2: 

    /** 		for i = 1 to length(addr) do*/
    if (IS_SEQUENCE(_addr_1290)){
            _517 = SEQ_PTR(_addr_1290)->length;
    }
    else {
        _517 = 1;
    }
    {
        int _i_1301;
        _i_1301 = 1;
L4: 
        if (_i_1301 > _517){
            goto L5; // [52] 89
        }

        /** 			memory:deallocate( addr[i] )*/
        _2 = (int)SEQ_PTR(_addr_1290);
        _518 = (int)*(((s1_ptr)_2)->base + _i_1301);
        Ref(_518);
        DeRef(_addr_inlined_deallocate_at_64_1305);
        _addr_inlined_deallocate_at_64_1305 = _518;
        _518 = NOVALUE;

        /** 	ifdef DATA_EXECUTE and WINDOWS then*/

        /**    	machine_proc( memconst:M_FREE, addr)*/
        machine(17, _addr_inlined_deallocate_at_64_1305);

        /** end procedure*/
        goto L6; // [77] 80
L6: 
        DeRef(_addr_inlined_deallocate_at_64_1305);
        _addr_inlined_deallocate_at_64_1305 = NOVALUE;

        /** 		end for*/
        _i_1301 = _i_1301 + 1;
        goto L4; // [84] 59
L5: 
        ;
    }

    /** 		return*/
    DeRef(_addr_1290);
    return;
    goto L7; // [94] 127
L1: 

    /** 	elsif sequence(addr) then*/
    _519 = IS_SEQUENCE(_addr_1290);
    if (_519 == 0)
    {
        _519 = NOVALUE;
        goto L8; // [102] 126
    }
    else{
        _519 = NOVALUE;
    }

    /** 		error:crash("free() called with nested sequence")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_106_1310);
    _msg_inlined_crash_at_106_1310 = EPrintf(-9999999, _520, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_106_1310);

    /** end procedure*/
    goto L9; // [120] 123
L9: 
    DeRefi(_msg_inlined_crash_at_106_1310);
    _msg_inlined_crash_at_106_1310 = NOVALUE;
L8: 
L7: 

    /** 	if addr = 0 then*/
    if (binary_op_a(NOTEQ, _addr_1290, 0)){
        goto LA; // [129] 139
    }

    /** 		return*/
    DeRef(_addr_1290);
    return;
LA: 

    /** 	memory:deallocate( addr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_1290);

    /** end procedure*/
    goto LB; // [150] 153
LB: 

    /** end procedure*/
    DeRef(_addr_1290);
    return;
    ;
}


void _4free_pointer_array(int _pointers_array_1318)
{
    int _saved_1319 = NOVALUE;
    int _ptr_1320 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom saved = pointers_array*/
    Ref(_pointers_array_1318);
    DeRef(_saved_1319);
    _saved_1319 = _pointers_array_1318;

    /** 	while ptr with entry do*/
    goto L1; // [8] 39
L2: 
    if (_ptr_1320 <= 0) {
        if (_ptr_1320 == 0) {
            goto L3; // [13] 49
        }
        else {
            if (!IS_ATOM_INT(_ptr_1320) && DBL_PTR(_ptr_1320)->dbl == 0.0){
                goto L3; // [13] 49
            }
        }
    }

    /** 		memory:deallocate( ptr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _ptr_1320);

    /** end procedure*/
    goto L4; // [27] 30
L4: 

    /** 		pointers_array += ADDRESS_LENGTH*/
    _0 = _pointers_array_1318;
    if (IS_ATOM_INT(_pointers_array_1318)) {
        _pointers_array_1318 = _pointers_array_1318 + 4;
        if ((long)((unsigned long)_pointers_array_1318 + (unsigned long)HIGH_BITS) >= 0) 
        _pointers_array_1318 = NewDouble((double)_pointers_array_1318);
    }
    else {
        _pointers_array_1318 = NewDouble(DBL_PTR(_pointers_array_1318)->dbl + (double)4);
    }
    DeRef(_0);

    /** 	entry*/
L1: 

    /** 		ptr = peek4u(pointers_array)*/
    DeRef(_ptr_1320);
    if (IS_ATOM_INT(_pointers_array_1318)) {
        _ptr_1320 = *(unsigned long *)_pointers_array_1318;
        if ((unsigned)_ptr_1320 > (unsigned)MAXINT)
        _ptr_1320 = NewDouble((double)(unsigned long)_ptr_1320);
    }
    else {
        _ptr_1320 = *(unsigned long *)(unsigned long)(DBL_PTR(_pointers_array_1318)->dbl);
        if ((unsigned)_ptr_1320 > (unsigned)MAXINT)
        _ptr_1320 = NewDouble((double)(unsigned long)_ptr_1320);
    }

    /** 	end while*/
    goto L2; // [46] 11
L3: 

    /** 	free(saved)*/
    Ref(_saved_1319);
    _4free(_saved_1319);

    /** end procedure*/
    DeRef(_pointers_array_1318);
    DeRef(_saved_1319);
    DeRef(_ptr_1320);
    return;
    ;
}



// 0xC47E83B2
