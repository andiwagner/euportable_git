// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _18abs(int _a_4345)
{
    int _t_4346 = NOVALUE;
    int _2186 = NOVALUE;
    int _2185 = NOVALUE;
    int _2184 = NOVALUE;
    int _2182 = NOVALUE;
    int _2180 = NOVALUE;
    int _2179 = NOVALUE;
    int _2177 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2177 = IS_ATOM(_a_4345);
    if (_2177 == 0)
    {
        _2177 = NOVALUE;
        goto L1; // [6] 35
    }
    else{
        _2177 = NOVALUE;
    }

    /** 		if a >= 0 then*/
    if (binary_op_a(LESS, _a_4345, 0)){
        goto L2; // [11] 24
    }

    /** 			return a*/
    DeRef(_t_4346);
    return _a_4345;
    goto L3; // [21] 34
L2: 

    /** 			return - a*/
    if (IS_ATOM_INT(_a_4345)) {
        if ((unsigned long)_a_4345 == 0xC0000000)
        _2179 = (int)NewDouble((double)-0xC0000000);
        else
        _2179 = - _a_4345;
    }
    else {
        _2179 = unary_op(UMINUS, _a_4345);
    }
    DeRef(_a_4345);
    DeRef(_t_4346);
    return _2179;
L3: 
L1: 

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4345)){
            _2180 = SEQ_PTR(_a_4345)->length;
    }
    else {
        _2180 = 1;
    }
    {
        int _i_4354;
        _i_4354 = 1;
L4: 
        if (_i_4354 > _2180){
            goto L5; // [40] 101
        }

        /** 		t = a[i]*/
        DeRef(_t_4346);
        _2 = (int)SEQ_PTR(_a_4345);
        _t_4346 = (int)*(((s1_ptr)_2)->base + _i_4354);
        Ref(_t_4346);

        /** 		if atom(t) then*/
        _2182 = IS_ATOM(_t_4346);
        if (_2182 == 0)
        {
            _2182 = NOVALUE;
            goto L6; // [58] 80
        }
        else{
            _2182 = NOVALUE;
        }

        /** 			if t < 0 then*/
        if (binary_op_a(GREATEREQ, _t_4346, 0)){
            goto L7; // [63] 94
        }

        /** 				a[i] = - t*/
        if (IS_ATOM_INT(_t_4346)) {
            if ((unsigned long)_t_4346 == 0xC0000000)
            _2184 = (int)NewDouble((double)-0xC0000000);
            else
            _2184 = - _t_4346;
        }
        else {
            _2184 = unary_op(UMINUS, _t_4346);
        }
        _2 = (int)SEQ_PTR(_a_4345);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_4345 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4354);
        _1 = *(int *)_2;
        *(int *)_2 = _2184;
        if( _1 != _2184 ){
            DeRef(_1);
        }
        _2184 = NOVALUE;
        goto L7; // [77] 94
L6: 

        /** 			a[i] = abs(t)*/
        Ref(_t_4346);
        DeRef(_2185);
        _2185 = _t_4346;
        _2186 = _18abs(_2185);
        _2185 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_4345);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_4345 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4354);
        _1 = *(int *)_2;
        *(int *)_2 = _2186;
        if( _1 != _2186 ){
            DeRef(_1);
        }
        _2186 = NOVALUE;
L7: 

        /** 	end for*/
        _i_4354 = _i_4354 + 1;
        goto L4; // [96] 47
L5: 
        ;
    }

    /** 	return a*/
    DeRef(_t_4346);
    DeRef(_2179);
    _2179 = NOVALUE;
    return _a_4345;
    ;
}


int _18max(int _a_4389)
{
    int _b_4390 = NOVALUE;
    int _c_4391 = NOVALUE;
    int _2196 = NOVALUE;
    int _2195 = NOVALUE;
    int _2194 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2194 = IS_ATOM(_a_4389);
    if (_2194 == 0)
    {
        _2194 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2194 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4390);
    DeRef(_c_4391);
    return _a_4389;
L1: 

    /** 	b = mathcons:MINF*/
    RefDS(_20MINF_4323);
    DeRef(_b_4390);
    _b_4390 = _20MINF_4323;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4389)){
            _2195 = SEQ_PTR(_a_4389)->length;
    }
    else {
        _2195 = 1;
    }
    {
        int _i_4395;
        _i_4395 = 1;
L2: 
        if (_i_4395 > _2195){
            goto L3; // [28] 64
        }

        /** 		c = max(a[i])*/
        _2 = (int)SEQ_PTR(_a_4389);
        _2196 = (int)*(((s1_ptr)_2)->base + _i_4395);
        Ref(_2196);
        _0 = _c_4391;
        _c_4391 = _18max(_2196);
        DeRef(_0);
        _2196 = NOVALUE;

        /** 		if c > b then*/
        if (binary_op_a(LESSEQ, _c_4391, _b_4390)){
            goto L4; // [47] 57
        }

        /** 			b = c*/
        Ref(_c_4391);
        DeRef(_b_4390);
        _b_4390 = _c_4391;
L4: 

        /** 	end for*/
        _i_4395 = _i_4395 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4389);
    DeRef(_c_4391);
    return _b_4390;
    ;
}


int _18min(int _a_4403)
{
    int _b_4404 = NOVALUE;
    int _c_4405 = NOVALUE;
    int _2201 = NOVALUE;
    int _2200 = NOVALUE;
    int _2199 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2199 = IS_ATOM(_a_4403);
    if (_2199 == 0)
    {
        _2199 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2199 = NOVALUE;
    }

    /** 			return a*/
    DeRef(_b_4404);
    DeRef(_c_4405);
    return _a_4403;
L1: 

    /** 	b = mathcons:PINF*/
    RefDS(_20PINF_4320);
    DeRef(_b_4404);
    _b_4404 = _20PINF_4320;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4403)){
            _2200 = SEQ_PTR(_a_4403)->length;
    }
    else {
        _2200 = 1;
    }
    {
        int _i_4409;
        _i_4409 = 1;
L2: 
        if (_i_4409 > _2200){
            goto L3; // [28] 64
        }

        /** 		c = min(a[i])*/
        _2 = (int)SEQ_PTR(_a_4403);
        _2201 = (int)*(((s1_ptr)_2)->base + _i_4409);
        Ref(_2201);
        _0 = _c_4405;
        _c_4405 = _18min(_2201);
        DeRef(_0);
        _2201 = NOVALUE;

        /** 			if c < b then*/
        if (binary_op_a(GREATEREQ, _c_4405, _b_4404)){
            goto L4; // [47] 57
        }

        /** 				b = c*/
        Ref(_c_4405);
        DeRef(_b_4404);
        _b_4404 = _c_4405;
L4: 

        /** 	end for*/
        _i_4409 = _i_4409 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4403);
    DeRef(_c_4405);
    return _b_4404;
    ;
}


int _18or_all(int _a_4783)
{
    int _b_4784 = NOVALUE;
    int _2401 = NOVALUE;
    int _2400 = NOVALUE;
    int _2398 = NOVALUE;
    int _2397 = NOVALUE;
    int _2396 = NOVALUE;
    int _2395 = NOVALUE;
    int _2394 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2394 = IS_ATOM(_a_4783);
    if (_2394 == 0)
    {
        _2394 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2394 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4784);
    return _a_4783;
L1: 

    /** 	b = 0*/
    DeRef(_b_4784);
    _b_4784 = 0;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4783)){
            _2395 = SEQ_PTR(_a_4783)->length;
    }
    else {
        _2395 = 1;
    }
    {
        int _i_4788;
        _i_4788 = 1;
L2: 
        if (_i_4788 > _2395){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4783);
        _2396 = (int)*(((s1_ptr)_2)->base + _i_4788);
        _2397 = IS_ATOM(_2396);
        _2396 = NOVALUE;
        if (_2397 == 0)
        {
            _2397 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2397 = NOVALUE;
        }

        /** 			b = or_bits(b, a[i])*/
        _2 = (int)SEQ_PTR(_a_4783);
        _2398 = (int)*(((s1_ptr)_2)->base + _i_4788);
        _0 = _b_4784;
        if (IS_ATOM_INT(_b_4784) && IS_ATOM_INT(_2398)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4784 | (unsigned long)_2398;
                 _b_4784 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4784 = binary_op(OR_BITS, _b_4784, _2398);
        }
        DeRef(_0);
        _2398 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b = or_bits(b, or_all(a[i]))*/
        _2 = (int)SEQ_PTR(_a_4783);
        _2400 = (int)*(((s1_ptr)_2)->base + _i_4788);
        Ref(_2400);
        _2401 = _18or_all(_2400);
        _2400 = NOVALUE;
        _0 = _b_4784;
        if (IS_ATOM_INT(_b_4784) && IS_ATOM_INT(_2401)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4784 | (unsigned long)_2401;
                 _b_4784 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4784 = binary_op(OR_BITS, _b_4784, _2401);
        }
        DeRef(_0);
        DeRef(_2401);
        _2401 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4788 = _i_4788 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4783);
    return _b_4784;
    ;
}



// 0x4232136C
