// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _16get_bytes(int _fn_2263, int _n_2264)
{
    int _s_2265 = NOVALUE;
    int _c_2266 = NOVALUE;
    int _first_2267 = NOVALUE;
    int _last_2268 = NOVALUE;
    int _1009 = NOVALUE;
    int _1006 = NOVALUE;
    int _1004 = NOVALUE;
    int _1003 = NOVALUE;
    int _1002 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_n_2264)) {
        _1 = (long)(DBL_PTR(_n_2264)->dbl);
        if (UNIQUE(DBL_PTR(_n_2264)) && (DBL_PTR(_n_2264)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_2264);
        _n_2264 = _1;
    }

    /** 	if n = 0 then*/
    if (_n_2264 != 0)
    goto L1; // [7] 18

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_2265);
    return _5;
L1: 

    /** 	c = getc(fn)*/
    if (_fn_2263 != last_r_file_no) {
        last_r_file_ptr = which_file(_fn_2263, EF_READ);
        last_r_file_no = _fn_2263;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_2266 = getKBchar();
        }
        else
        _c_2266 = getc(last_r_file_ptr);
    }
    else
    _c_2266 = getc(last_r_file_ptr);

    /** 	if c = EOF then*/
    if (_c_2266 != -1)
    goto L2; // [25] 36

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_2265);
    return _5;
L2: 

    /** 	s = repeat(c, n)*/
    DeRefi(_s_2265);
    _s_2265 = Repeat(_c_2266, _n_2264);

    /** 	last = 1*/
    _last_2268 = 1;

    /** 	while last < n do*/
L3: 
    if (_last_2268 >= _n_2264)
    goto L4; // [52] 159

    /** 		first = last+1*/
    _first_2267 = _last_2268 + 1;

    /** 		last  = last+CHUNK*/
    _last_2268 = _last_2268 + 100;

    /** 		if last > n then*/
    if (_last_2268 <= _n_2264)
    goto L5; // [70] 80

    /** 			last = n*/
    _last_2268 = _n_2264;
L5: 

    /** 		for i = first to last do*/
    _1002 = _last_2268;
    {
        int _i_2282;
        _i_2282 = _first_2267;
L6: 
        if (_i_2282 > _1002){
            goto L7; // [85] 108
        }

        /** 			s[i] = getc(fn)*/
        if (_fn_2263 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_2263, EF_READ);
            last_r_file_no = _fn_2263;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _1003 = getKBchar();
            }
            else
            _1003 = getc(last_r_file_ptr);
        }
        else
        _1003 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_s_2265);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_2265 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2282);
        *(int *)_2 = _1003;
        if( _1 != _1003 ){
        }
        _1003 = NOVALUE;

        /** 		end for*/
        _i_2282 = _i_2282 + 1;
        goto L6; // [103] 92
L7: 
        ;
    }

    /** 		if s[last] = EOF then*/
    _2 = (int)SEQ_PTR(_s_2265);
    _1004 = (int)*(((s1_ptr)_2)->base + _last_2268);
    if (_1004 != -1)
    goto L3; // [114] 52

    /** 			while s[last] = EOF do*/
L8: 
    _2 = (int)SEQ_PTR(_s_2265);
    _1006 = (int)*(((s1_ptr)_2)->base + _last_2268);
    if (_1006 != -1)
    goto L9; // [127] 142

    /** 				last -= 1*/
    _last_2268 = _last_2268 - 1;

    /** 			end while*/
    goto L8; // [139] 123
L9: 

    /** 			return s[1..last]*/
    rhs_slice_target = (object_ptr)&_1009;
    RHS_Slice(_s_2265, 1, _last_2268);
    DeRefDSi(_s_2265);
    _1004 = NOVALUE;
    _1006 = NOVALUE;
    return _1009;

    /** 	end while*/
    goto L3; // [156] 52
L4: 

    /** 	return s*/
    _1004 = NOVALUE;
    _1006 = NOVALUE;
    DeRef(_1009);
    _1009 = NOVALUE;
    return _s_2265;
    ;
}


int _16get_integer32(int _fh_2303)
{
    int _1018 = NOVALUE;
    int _1017 = NOVALUE;
    int _1016 = NOVALUE;
    int _1015 = NOVALUE;
    int _1014 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_2303 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2303, EF_READ);
        last_r_file_no = _fh_2303;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1014 = getKBchar();
        }
        else
        _1014 = getc(last_r_file_ptr);
    }
    else
    _1014 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem0_2293)){
        poke_addr = (unsigned char *)_16mem0_2293;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem0_2293)->dbl);
    }
    *poke_addr = (unsigned char)_1014;
    _1014 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_2303 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2303, EF_READ);
        last_r_file_no = _fh_2303;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1015 = getKBchar();
        }
        else
        _1015 = getc(last_r_file_ptr);
    }
    else
    _1015 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem1_2294)){
        poke_addr = (unsigned char *)_16mem1_2294;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem1_2294)->dbl);
    }
    *poke_addr = (unsigned char)_1015;
    _1015 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_2303 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2303, EF_READ);
        last_r_file_no = _fh_2303;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1016 = getKBchar();
        }
        else
        _1016 = getc(last_r_file_ptr);
    }
    else
    _1016 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem2_2295)){
        poke_addr = (unsigned char *)_16mem2_2295;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem2_2295)->dbl);
    }
    *poke_addr = (unsigned char)_1016;
    _1016 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_2303 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2303, EF_READ);
        last_r_file_no = _fh_2303;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1017 = getKBchar();
        }
        else
        _1017 = getc(last_r_file_ptr);
    }
    else
    _1017 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem3_2296)){
        poke_addr = (unsigned char *)_16mem3_2296;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem3_2296)->dbl);
    }
    *poke_addr = (unsigned char)_1017;
    _1017 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_16mem0_2293)) {
        _1018 = *(unsigned long *)_16mem0_2293;
        if ((unsigned)_1018 > (unsigned)MAXINT)
        _1018 = NewDouble((double)(unsigned long)_1018);
    }
    else {
        _1018 = *(unsigned long *)(unsigned long)(DBL_PTR(_16mem0_2293)->dbl);
        if ((unsigned)_1018 > (unsigned)MAXINT)
        _1018 = NewDouble((double)(unsigned long)_1018);
    }
    return _1018;
    ;
}


int _16get_integer16(int _fh_2311)
{
    int _1021 = NOVALUE;
    int _1020 = NOVALUE;
    int _1019 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_2311 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2311, EF_READ);
        last_r_file_no = _fh_2311;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1019 = getKBchar();
        }
        else
        _1019 = getc(last_r_file_ptr);
    }
    else
    _1019 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem0_2293)){
        poke_addr = (unsigned char *)_16mem0_2293;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem0_2293)->dbl);
    }
    *poke_addr = (unsigned char)_1019;
    _1019 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_2311 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_2311, EF_READ);
        last_r_file_no = _fh_2311;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _1020 = getKBchar();
        }
        else
        _1020 = getc(last_r_file_ptr);
    }
    else
    _1020 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_16mem1_2294)){
        poke_addr = (unsigned char *)_16mem1_2294;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16mem1_2294)->dbl);
    }
    *poke_addr = (unsigned char)_1020;
    _1020 = NOVALUE;

    /** 	return peek2u(mem0)*/
    if (IS_ATOM_INT(_16mem0_2293)) {
        _1021 = *(unsigned short *)_16mem0_2293;
    }
    else {
        _1021 = *(unsigned short *)(unsigned long)(DBL_PTR(_16mem0_2293)->dbl);
    }
    return _1021;
    ;
}


int _16seek(int _fn_2405, int _pos_2406)
{
    int _1068 = NOVALUE;
    int _1067 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_2406);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_2405;
    ((int *)_2)[2] = _pos_2406;
    _1067 = MAKE_SEQ(_1);
    _1068 = machine(19, _1067);
    DeRefDS(_1067);
    _1067 = NOVALUE;
    DeRef(_pos_2406);
    return _1068;
    ;
}


int _16where(int _fn_2411)
{
    int _1069 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_WHERE, fn)*/
    _1069 = machine(20, _fn_2411);
    return _1069;
    ;
}


int _16read_lines(int _file_2430)
{
    int _fn_2431 = NOVALUE;
    int _ret_2432 = NOVALUE;
    int _y_2433 = NOVALUE;
    int _1093 = NOVALUE;
    int _1092 = NOVALUE;
    int _1091 = NOVALUE;
    int _1090 = NOVALUE;
    int _1084 = NOVALUE;
    int _1083 = NOVALUE;
    int _1081 = NOVALUE;
    int _1080 = NOVALUE;
    int _1079 = NOVALUE;
    int _1074 = NOVALUE;
    int _1073 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _1073 = 1;
    if (_1073 == 0)
    {
        _1073 = NOVALUE;
        goto L1; // [6] 37
    }
    else{
        _1073 = NOVALUE;
    }

    /** 		if length(file) = 0 then*/
    if (IS_SEQUENCE(_file_2430)){
            _1074 = SEQ_PTR(_file_2430)->length;
    }
    else {
        _1074 = 1;
    }
    if (_1074 != 0)
    goto L2; // [14] 26

    /** 			fn = 0*/
    DeRef(_fn_2431);
    _fn_2431 = 0;
    goto L3; // [23] 43
L2: 

    /** 			fn = open(file, "r")*/
    DeRef(_fn_2431);
    _fn_2431 = EOpen(_file_2430, _1076, 0);
    goto L3; // [34] 43
L1: 

    /** 		fn = file*/
    Ref(_file_2430);
    DeRef(_fn_2431);
    _fn_2431 = _file_2430;
L3: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_2431, 0)){
        goto L4; // [47] 56
    }
    DeRef(_file_2430);
    DeRef(_fn_2431);
    DeRef(_ret_2432);
    DeRefi(_y_2433);
    return -1;
L4: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_2432);
    _ret_2432 = _5;

    /** 	while sequence(y) with entry do*/
    goto L5; // [63] 125
L6: 
    _1079 = IS_SEQUENCE(_y_2433);
    if (_1079 == 0)
    {
        _1079 = NOVALUE;
        goto L7; // [71] 135
    }
    else{
        _1079 = NOVALUE;
    }

    /** 		if y[$] = '\n' then*/
    if (IS_SEQUENCE(_y_2433)){
            _1080 = SEQ_PTR(_y_2433)->length;
    }
    else {
        _1080 = 1;
    }
    _2 = (int)SEQ_PTR(_y_2433);
    _1081 = (int)*(((s1_ptr)_2)->base + _1080);
    if (_1081 != 10)
    goto L8; // [83] 104

    /** 			y = y[1..$-1]*/
    if (IS_SEQUENCE(_y_2433)){
            _1083 = SEQ_PTR(_y_2433)->length;
    }
    else {
        _1083 = 1;
    }
    _1084 = _1083 - 1;
    _1083 = NOVALUE;
    rhs_slice_target = (object_ptr)&_y_2433;
    RHS_Slice(_y_2433, 1, _1084);

    /** 			ifdef UNIX then*/
L8: 

    /** 		ret = append(ret, y)*/
    Ref(_y_2433);
    Append(&_ret_2432, _ret_2432, _y_2433);

    /** 		if fn = 0 then*/
    if (binary_op_a(NOTEQ, _fn_2431, 0)){
        goto L9; // [112] 122
    }

    /** 			puts(2, '\n')*/
    EPuts(2, 10); // DJP 
L9: 

    /** 	entry*/
L5: 

    /** 		y = gets(fn)*/
    DeRefi(_y_2433);
    _y_2433 = EGets(_fn_2431);

    /** 	end while*/
    goto L6; // [132] 66
L7: 

    /** 	if sequence(file) and length(file) != 0 then*/
    _1090 = IS_SEQUENCE(_file_2430);
    if (_1090 == 0) {
        goto LA; // [140] 160
    }
    if (IS_SEQUENCE(_file_2430)){
            _1092 = SEQ_PTR(_file_2430)->length;
    }
    else {
        _1092 = 1;
    }
    _1093 = (_1092 != 0);
    _1092 = NOVALUE;
    if (_1093 == 0)
    {
        DeRef(_1093);
        _1093 = NOVALUE;
        goto LA; // [152] 160
    }
    else{
        DeRef(_1093);
        _1093 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_2431))
    EClose(_fn_2431);
    else
    EClose((int)DBL_PTR(_fn_2431)->dbl);
LA: 

    /** 	return ret*/
    DeRef(_file_2430);
    DeRef(_fn_2431);
    DeRefi(_y_2433);
    _1081 = NOVALUE;
    DeRef(_1084);
    _1084 = NOVALUE;
    return _ret_2432;
    ;
}


int _16write_lines(int _file_2508, int _lines_2509)
{
    int _fn_2510 = NOVALUE;
    int _1123 = NOVALUE;
    int _1122 = NOVALUE;
    int _1121 = NOVALUE;
    int _1117 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _1117 = 1;
    if (_1117 == 0)
    {
        _1117 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _1117 = NOVALUE;
    }

    /**     	fn = open(file, "w")*/
    DeRef(_fn_2510);
    _fn_2510 = EOpen(_file_2508, _1118, 0);
    goto L2; // [18] 27
L1: 

    /** 		fn = file*/
    Ref(_file_2508);
    DeRef(_fn_2510);
    _fn_2510 = _file_2508;
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_2510, 0)){
        goto L3; // [31] 40
    }
    DeRef(_file_2508);
    DeRefDS(_lines_2509);
    DeRef(_fn_2510);
    return -1;
L3: 

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_2509)){
            _1121 = SEQ_PTR(_lines_2509)->length;
    }
    else {
        _1121 = 1;
    }
    {
        int _i_2519;
        _i_2519 = 1;
L4: 
        if (_i_2519 > _1121){
            goto L5; // [45] 73
        }

        /** 		puts(fn, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_2509);
        _1122 = (int)*(((s1_ptr)_2)->base + _i_2519);
        EPuts(_fn_2510, _1122); // DJP 
        _1122 = NOVALUE;

        /** 		puts(fn, '\n')*/
        EPuts(_fn_2510, 10); // DJP 

        /** 	end for*/
        _i_2519 = _i_2519 + 1;
        goto L4; // [68] 52
L5: 
        ;
    }

    /** 	if sequence(file) then*/
    _1123 = IS_SEQUENCE(_file_2508);
    if (_1123 == 0)
    {
        _1123 = NOVALUE;
        goto L6; // [78] 86
    }
    else{
        _1123 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_2510))
    EClose(_fn_2510);
    else
    EClose((int)DBL_PTR(_fn_2510)->dbl);
L6: 

    /** 	return 1*/
    DeRef(_file_2508);
    DeRefDS(_lines_2509);
    DeRef(_fn_2510);
    return 1;
    ;
}


void _16writef(int _fm_2646, int _data_2647, int _fn_2648, int _data_not_string_2649)
{
    int _real_fn_2650 = NOVALUE;
    int _close_fn_2651 = NOVALUE;
    int _out_style_2652 = NOVALUE;
    int _ts_2655 = NOVALUE;
    int _msg_inlined_crash_at_163_2680 = NOVALUE;
    int _data_inlined_crash_at_160_2679 = NOVALUE;
    int _1201 = NOVALUE;
    int _1199 = NOVALUE;
    int _1198 = NOVALUE;
    int _1197 = NOVALUE;
    int _1191 = NOVALUE;
    int _1190 = NOVALUE;
    int _1189 = NOVALUE;
    int _1188 = NOVALUE;
    int _1187 = NOVALUE;
    int _1186 = NOVALUE;
    int _1184 = NOVALUE;
    int _1183 = NOVALUE;
    int _1182 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer real_fn = 0*/
    _real_fn_2650 = 0;

    /** 	integer close_fn = 0*/
    _close_fn_2651 = 0;

    /** 	sequence out_style = "w"*/
    RefDS(_1118);
    DeRefi(_out_style_2652);
    _out_style_2652 = _1118;

    /** 	if integer(fm) then*/
    _1182 = 1;
    if (_1182 == 0)
    {
        _1182 = NOVALUE;
        goto L1; // [23] 49
    }
    else{
        _1182 = NOVALUE;
    }

    /** 		object ts*/

    /** 		ts = fm*/
    _ts_2655 = _fm_2646;

    /** 		fm = data*/
    RefDS(_data_2647);
    _fm_2646 = _data_2647;

    /** 		data = fn*/
    RefDS(_fn_2648);
    DeRefDS(_data_2647);
    _data_2647 = _fn_2648;

    /** 		fn = ts*/
    DeRefDS(_fn_2648);
    _fn_2648 = _ts_2655;
L1: 

    /** 	if sequence(fn) then*/
    _1183 = IS_SEQUENCE(_fn_2648);
    if (_1183 == 0)
    {
        _1183 = NOVALUE;
        goto L2; // [56] 191
    }
    else{
        _1183 = NOVALUE;
    }

    /** 		if length(fn) = 2 then*/
    if (IS_SEQUENCE(_fn_2648)){
            _1184 = SEQ_PTR(_fn_2648)->length;
    }
    else {
        _1184 = 1;
    }
    if (_1184 != 2)
    goto L3; // [64] 142

    /** 			if sequence(fn[1]) then*/
    _2 = (int)SEQ_PTR(_fn_2648);
    _1186 = (int)*(((s1_ptr)_2)->base + 1);
    _1187 = IS_SEQUENCE(_1186);
    _1186 = NOVALUE;
    if (_1187 == 0)
    {
        _1187 = NOVALUE;
        goto L4; // [77] 141
    }
    else{
        _1187 = NOVALUE;
    }

    /** 				if equal(fn[2], 'a') then*/
    _2 = (int)SEQ_PTR(_fn_2648);
    _1188 = (int)*(((s1_ptr)_2)->base + 2);
    if (_1188 == 97)
    _1189 = 1;
    else if (IS_ATOM_INT(_1188) && IS_ATOM_INT(97))
    _1189 = 0;
    else
    _1189 = (compare(_1188, 97) == 0);
    _1188 = NOVALUE;
    if (_1189 == 0)
    {
        _1189 = NOVALUE;
        goto L5; // [90] 103
    }
    else{
        _1189 = NOVALUE;
    }

    /** 					out_style = "a"*/
    RefDS(_1124);
    DeRefi(_out_style_2652);
    _out_style_2652 = _1124;
    goto L6; // [100] 134
L5: 

    /** 				elsif not equal(fn[2], "a") then*/
    _2 = (int)SEQ_PTR(_fn_2648);
    _1190 = (int)*(((s1_ptr)_2)->base + 2);
    if (_1190 == _1124)
    _1191 = 1;
    else if (IS_ATOM_INT(_1190) && IS_ATOM_INT(_1124))
    _1191 = 0;
    else
    _1191 = (compare(_1190, _1124) == 0);
    _1190 = NOVALUE;
    if (_1191 != 0)
    goto L7; // [113] 126
    _1191 = NOVALUE;

    /** 					out_style = "w"*/
    RefDS(_1118);
    DeRefi(_out_style_2652);
    _out_style_2652 = _1118;
    goto L6; // [123] 134
L7: 

    /** 					out_style = "a"*/
    RefDS(_1124);
    DeRefi(_out_style_2652);
    _out_style_2652 = _1124;
L6: 

    /** 				fn = fn[1]*/
    _0 = _fn_2648;
    _2 = (int)SEQ_PTR(_fn_2648);
    _fn_2648 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_fn_2648);
    DeRef(_0);
L4: 
L3: 

    /** 		real_fn = open(fn, out_style)*/
    _real_fn_2650 = EOpen(_fn_2648, _out_style_2652, 0);

    /** 		if real_fn = -1 then*/
    if (_real_fn_2650 != -1)
    goto L8; // [151] 183

    /** 			error:crash("Unable to write to '%s'", {fn})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_fn_2648);
    *((int *)(_2+4)) = _fn_2648;
    _1197 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_160_2679);
    _data_inlined_crash_at_160_2679 = _1197;
    _1197 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_163_2680);
    _msg_inlined_crash_at_163_2680 = EPrintf(-9999999, _1196, _data_inlined_crash_at_160_2679);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_163_2680);

    /** end procedure*/
    goto L9; // [177] 180
L9: 
    DeRef(_data_inlined_crash_at_160_2679);
    _data_inlined_crash_at_160_2679 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_163_2680);
    _msg_inlined_crash_at_163_2680 = NOVALUE;
L8: 

    /** 		close_fn = 1*/
    _close_fn_2651 = 1;
    goto LA; // [188] 199
L2: 

    /** 		real_fn = fn*/
    Ref(_fn_2648);
    _real_fn_2650 = _fn_2648;
    if (!IS_ATOM_INT(_real_fn_2650)) {
        _1 = (long)(DBL_PTR(_real_fn_2650)->dbl);
        if (UNIQUE(DBL_PTR(_real_fn_2650)) && (DBL_PTR(_real_fn_2650)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_real_fn_2650);
        _real_fn_2650 = _1;
    }
LA: 

    /** 	if equal(data_not_string, 0) then*/
    if (_data_not_string_2649 == 0)
    _1198 = 1;
    else if (IS_ATOM_INT(_data_not_string_2649) && IS_ATOM_INT(0))
    _1198 = 0;
    else
    _1198 = (compare(_data_not_string_2649, 0) == 0);
    if (_1198 == 0)
    {
        _1198 = NOVALUE;
        goto LB; // [205] 225
    }
    else{
        _1198 = NOVALUE;
    }

    /** 		if types:t_display(data) then*/
    Ref(_data_2647);
    _1199 = _9t_display(_data_2647);
    if (_1199 == 0) {
        DeRef(_1199);
        _1199 = NOVALUE;
        goto LC; // [214] 224
    }
    else {
        if (!IS_ATOM_INT(_1199) && DBL_PTR(_1199)->dbl == 0.0){
            DeRef(_1199);
            _1199 = NOVALUE;
            goto LC; // [214] 224
        }
        DeRef(_1199);
        _1199 = NOVALUE;
    }
    DeRef(_1199);
    _1199 = NOVALUE;

    /** 			data = {data}*/
    _0 = _data_2647;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_data_2647);
    *((int *)(_2+4)) = _data_2647;
    _data_2647 = MAKE_SEQ(_1);
    DeRef(_0);
LC: 
LB: 

    /**     puts(real_fn, text:format( fm, data ) )*/
    Ref(_fm_2646);
    Ref(_data_2647);
    _1201 = _10format(_fm_2646, _data_2647);
    EPuts(_real_fn_2650, _1201); // DJP 
    DeRef(_1201);
    _1201 = NOVALUE;

    /**     if close_fn then*/
    if (_close_fn_2651 == 0)
    {
        goto LD; // [237] 245
    }
    else{
    }

    /**     	close(real_fn)*/
    EClose(_real_fn_2650);
LD: 

    /** end procedure*/
    DeRef(_fm_2646);
    DeRef(_data_2647);
    DeRef(_fn_2648);
    DeRefi(_out_style_2652);
    return;
    ;
}



// 0xED36703F
