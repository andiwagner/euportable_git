// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _38print_sym(int _s_16692)
{
    int _s_obj_16695 = NOVALUE;
    int _9453 = NOVALUE;
    int _9452 = NOVALUE;
    int _9448 = NOVALUE;
    int _9446 = NOVALUE;
    int _9445 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_16692)) {
        _1 = (long)(DBL_PTR(_s_16692)->dbl);
        if (UNIQUE(DBL_PTR(_s_16692)) && (DBL_PTR(_s_16692)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_16692);
        _s_16692 = _1;
    }

    /** 	printf(1,"[%d]:\n", {s} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _s_16692;
    _9445 = MAKE_SEQ(_1);
    EPrintf(1, _9444, _9445);
    DeRefDS(_9445);
    _9445 = NOVALUE;

    /** 	object s_obj = SymTab[s][S_OBJ]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _9446 = (int)*(((s1_ptr)_2)->base + _s_16692);
    DeRef(_s_obj_16695);
    _2 = (int)SEQ_PTR(_9446);
    _s_obj_16695 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_s_obj_16695);
    _9446 = NOVALUE;

    /** 	if equal(s_obj,NOVALUE) then */
    if (_s_obj_16695 == _38NOVALUE_16800)
    _9448 = 1;
    else if (IS_ATOM_INT(_s_obj_16695) && IS_ATOM_INT(_38NOVALUE_16800))
    _9448 = 0;
    else
    _9448 = (compare(_s_obj_16695, _38NOVALUE_16800) == 0);
    if (_9448 == 0)
    {
        _9448 = NOVALUE;
        goto L1; // [35] 46
    }
    else{
        _9448 = NOVALUE;
    }

    /** 		puts(1,"S_OBJ=>NOVALUE\n")*/
    EPuts(1, _9449); // DJP 
    goto L2; // [43] 57
L1: 

    /** 		puts(1,"S_OBJ=>")*/
    EPuts(1, _9450); // DJP 

    /** 		? s_obj		*/
    StdPrint(1, _s_obj_16695, 1);
L2: 

    /** 	puts(1,"S_MODE=>")*/
    EPuts(1, _9451); // DJP 

    /** 	switch SymTab[s][S_MODE] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _9452 = (int)*(((s1_ptr)_2)->base + _s_16692);
    _2 = (int)SEQ_PTR(_9452);
    _9453 = (int)*(((s1_ptr)_2)->base + 3);
    _9452 = NOVALUE;
    if (IS_SEQUENCE(_9453) ){
        goto L3; // [76] 124
    }
    if(!IS_ATOM_INT(_9453)){
        if( (DBL_PTR(_9453)->dbl != (double) ((int) DBL_PTR(_9453)->dbl) ) ){
            goto L3; // [76] 124
        }
        _0 = (int) DBL_PTR(_9453)->dbl;
    }
    else {
        _0 = _9453;
    };
    _9453 = NOVALUE;
    switch ( _0 ){ 

        /** 		case M_NORMAL then*/
        case 1:

        /** 			puts(1,"M_NORMAL")*/
        EPuts(1, _9456); // DJP 
        goto L3; // [90] 124

        /** 		case M_TEMP then*/
        case 3:

        /** 			puts(1,"M_TEMP")*/
        EPuts(1, _9457); // DJP 
        goto L3; // [101] 124

        /** 		case M_CONSTANT then*/
        case 2:

        /** 			puts(1,"M_CONSTANT")*/
        EPuts(1, _9458); // DJP 
        goto L3; // [112] 124

        /** 		case M_BLOCK then*/
        case 4:

        /** 			puts(1,"M_BLOCK")*/
        EPuts(1, _9459); // DJP 
    ;}L3: 

    /** 	puts(1,{10,10})*/
    EPuts(1, _9460); // DJP 

    /** end procedure*/
    DeRef(_s_obj_16695);
    return;
    ;
}


int _38symtab_entry(int _x_16805)
{
    int _9499 = NOVALUE;
    int _9498 = NOVALUE;
    int _9497 = NOVALUE;
    int _9496 = NOVALUE;
    int _9495 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return length(x) = SIZEOF_ROUTINE_ENTRY or*/
    if (IS_SEQUENCE(_x_16805)){
            _9495 = SEQ_PTR(_x_16805)->length;
    }
    else {
        _9495 = 1;
    }
    _9496 = (_9495 == _38SIZEOF_ROUTINE_ENTRY_16724);
    _9495 = NOVALUE;
    if (IS_SEQUENCE(_x_16805)){
            _9497 = SEQ_PTR(_x_16805)->length;
    }
    else {
        _9497 = 1;
    }
    _9498 = (_9497 == _38SIZEOF_VAR_ENTRY_16727);
    _9497 = NOVALUE;
    _9499 = (_9496 != 0 || _9498 != 0);
    _9496 = NOVALUE;
    _9498 = NOVALUE;
    DeRefDS(_x_16805);
    return _9499;
    ;
}


int _38symtab_index(int _x_16813)
{
    int _9508 = NOVALUE;
    int _9507 = NOVALUE;
    int _9506 = NOVALUE;
    int _9505 = NOVALUE;
    int _9504 = NOVALUE;
    int _9503 = NOVALUE;
    int _9501 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_16813)) {
        _1 = (long)(DBL_PTR(_x_16813)->dbl);
        if (UNIQUE(DBL_PTR(_x_16813)) && (DBL_PTR(_x_16813)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_16813);
        _x_16813 = _1;
    }

    /** 	if x = 0 then*/
    if (_x_16813 != 0)
    goto L1; // [5] 18

    /** 		return TRUE -- NULL value*/
    return _9TRUE_428;
L1: 

    /** 	if x < 0 or x > length(SymTab) then*/
    _9501 = (_x_16813 < 0);
    if (_9501 != 0) {
        goto L2; // [24] 42
    }
    if (IS_SEQUENCE(_35SymTab_15595)){
            _9503 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _9503 = 1;
    }
    _9504 = (_x_16813 > _9503);
    _9503 = NOVALUE;
    if (_9504 == 0)
    {
        DeRef(_9504);
        _9504 = NOVALUE;
        goto L3; // [38] 51
    }
    else{
        DeRef(_9504);
        _9504 = NOVALUE;
    }
L2: 

    /** 		return FALSE*/
    DeRef(_9501);
    _9501 = NOVALUE;
    return _9FALSE_426;
L3: 

    /** 	return find(length(SymTab[x]), {SIZEOF_VAR_ENTRY, SIZEOF_ROUTINE_ENTRY,*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _9505 = (int)*(((s1_ptr)_2)->base + _x_16813);
    if (IS_SEQUENCE(_9505)){
            _9506 = SEQ_PTR(_9505)->length;
    }
    else {
        _9506 = 1;
    }
    _9505 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _38SIZEOF_VAR_ENTRY_16727;
    *((int *)(_2+8)) = _38SIZEOF_ROUTINE_ENTRY_16724;
    *((int *)(_2+12)) = _38SIZEOF_TEMP_ENTRY_16733;
    *((int *)(_2+16)) = _38SIZEOF_BLOCK_ENTRY_16730;
    _9507 = MAKE_SEQ(_1);
    _9508 = find_from(_9506, _9507, 1);
    _9506 = NOVALUE;
    DeRefDS(_9507);
    _9507 = NOVALUE;
    DeRef(_9501);
    _9501 = NOVALUE;
    _9505 = NOVALUE;
    return _9508;
    ;
}


int _38temp_index(int _x_16831)
{
    int _9512 = NOVALUE;
    int _9511 = NOVALUE;
    int _9510 = NOVALUE;
    int _9509 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_16831)) {
        _1 = (long)(DBL_PTR(_x_16831)->dbl);
        if (UNIQUE(DBL_PTR(_x_16831)) && (DBL_PTR(_x_16831)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_16831);
        _x_16831 = _1;
    }

    /** 	return x >= 0 and x <= length(SymTab)*/
    _9509 = (_x_16831 >= 0);
    if (IS_SEQUENCE(_35SymTab_15595)){
            _9510 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _9510 = 1;
    }
    _9511 = (_x_16831 <= _9510);
    _9510 = NOVALUE;
    _9512 = (_9509 != 0 && _9511 != 0);
    _9509 = NOVALUE;
    _9511 = NOVALUE;
    return _9512;
    ;
}


int _38token(int _t_16842)
{
    int _9556 = NOVALUE;
    int _9555 = NOVALUE;
    int _9554 = NOVALUE;
    int _9553 = NOVALUE;
    int _9552 = NOVALUE;
    int _9551 = NOVALUE;
    int _9550 = NOVALUE;
    int _9549 = NOVALUE;
    int _9548 = NOVALUE;
    int _9547 = NOVALUE;
    int _9546 = NOVALUE;
    int _9545 = NOVALUE;
    int _9544 = NOVALUE;
    int _9543 = NOVALUE;
    int _9542 = NOVALUE;
    int _9541 = NOVALUE;
    int _9540 = NOVALUE;
    int _9539 = NOVALUE;
    int _9538 = NOVALUE;
    int _9537 = NOVALUE;
    int _9536 = NOVALUE;
    int _9535 = NOVALUE;
    int _9534 = NOVALUE;
    int _9533 = NOVALUE;
    int _9532 = NOVALUE;
    int _9531 = NOVALUE;
    int _9530 = NOVALUE;
    int _9529 = NOVALUE;
    int _9528 = NOVALUE;
    int _9527 = NOVALUE;
    int _9526 = NOVALUE;
    int _9525 = NOVALUE;
    int _9524 = NOVALUE;
    int _9523 = NOVALUE;
    int _9522 = NOVALUE;
    int _9521 = NOVALUE;
    int _9520 = NOVALUE;
    int _9518 = NOVALUE;
    int _9517 = NOVALUE;
    int _9515 = NOVALUE;
    int _9514 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(t) then*/
    _9514 = IS_ATOM(_t_16842);
    if (_9514 == 0)
    {
        _9514 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _9514 = NOVALUE;
    }

    /** 		return FALSE*/
    DeRef(_t_16842);
    return _9FALSE_426;
L1: 

    /** 	if length(t) != 2 then*/
    if (IS_SEQUENCE(_t_16842)){
            _9515 = SEQ_PTR(_t_16842)->length;
    }
    else {
        _9515 = 1;
    }
    if (_9515 == 2)
    goto L2; // [23] 36

    /** 		return FALSE*/
    DeRef(_t_16842);
    return _9FALSE_426;
L2: 

    /** 	if not integer(t[T_ID]) then*/
    _2 = (int)SEQ_PTR(_t_16842);
    _9517 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9517))
    _9518 = 1;
    else if (IS_ATOM_DBL(_9517))
    _9518 = IS_ATOM_INT(DoubleToInt(_9517));
    else
    _9518 = 0;
    _9517 = NOVALUE;
    if (_9518 != 0)
    goto L3; // [47] 59
    _9518 = NOVALUE;

    /** 		return FALSE*/
    DeRef(_t_16842);
    return _9FALSE_426;
L3: 

    /** 	if t[T_ID] = VARIABLE and (t[T_SYM] < 0 or symtab_index(t[T_SYM])) then*/
    _2 = (int)SEQ_PTR(_t_16842);
    _9520 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9520)) {
        _9521 = (_9520 == -100);
    }
    else {
        _9521 = binary_op(EQUALS, _9520, -100);
    }
    _9520 = NOVALUE;
    if (IS_ATOM_INT(_9521)) {
        if (_9521 == 0) {
            goto L4; // [73] 118
        }
    }
    else {
        if (DBL_PTR(_9521)->dbl == 0.0) {
            goto L4; // [73] 118
        }
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9523 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_9523)) {
        _9524 = (_9523 < 0);
    }
    else {
        _9524 = binary_op(LESS, _9523, 0);
    }
    _9523 = NOVALUE;
    if (IS_ATOM_INT(_9524)) {
        if (_9524 != 0) {
            DeRef(_9525);
            _9525 = 1;
            goto L5; // [87] 105
        }
    }
    else {
        if (DBL_PTR(_9524)->dbl != 0.0) {
            DeRef(_9525);
            _9525 = 1;
            goto L5; // [87] 105
        }
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9526 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_9526);
    _9527 = _38symtab_index(_9526);
    _9526 = NOVALUE;
    DeRef(_9525);
    if (IS_ATOM_INT(_9527))
    _9525 = (_9527 != 0);
    else
    _9525 = DBL_PTR(_9527)->dbl != 0.0;
L5: 
    if (_9525 == 0)
    {
        _9525 = NOVALUE;
        goto L4; // [106] 118
    }
    else{
        _9525 = NOVALUE;
    }

    /** 		return TRUE*/
    DeRef(_t_16842);
    DeRef(_9521);
    _9521 = NOVALUE;
    DeRef(_9524);
    _9524 = NOVALUE;
    DeRef(_9527);
    _9527 = NOVALUE;
    return _9TRUE_428;
L4: 

    /** 	if QUESTION_MARK <= t[T_ID] and t[T_ID] <= -1 then*/
    _2 = (int)SEQ_PTR(_t_16842);
    _9528 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9528)) {
        _9529 = (-31 <= _9528);
    }
    else {
        _9529 = binary_op(LESSEQ, -31, _9528);
    }
    _9528 = NOVALUE;
    if (IS_ATOM_INT(_9529)) {
        if (_9529 == 0) {
            goto L6; // [132] 159
        }
    }
    else {
        if (DBL_PTR(_9529)->dbl == 0.0) {
            goto L6; // [132] 159
        }
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9531 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9531)) {
        _9532 = (_9531 <= -1);
    }
    else {
        _9532 = binary_op(LESSEQ, _9531, -1);
    }
    _9531 = NOVALUE;
    if (_9532 == 0) {
        DeRef(_9532);
        _9532 = NOVALUE;
        goto L6; // [147] 159
    }
    else {
        if (!IS_ATOM_INT(_9532) && DBL_PTR(_9532)->dbl == 0.0){
            DeRef(_9532);
            _9532 = NOVALUE;
            goto L6; // [147] 159
        }
        DeRef(_9532);
        _9532 = NOVALUE;
    }
    DeRef(_9532);
    _9532 = NOVALUE;

    /** 		return TRUE*/
    DeRef(_t_16842);
    DeRef(_9521);
    _9521 = NOVALUE;
    DeRef(_9524);
    _9524 = NOVALUE;
    DeRef(_9527);
    _9527 = NOVALUE;
    DeRef(_9529);
    _9529 = NOVALUE;
    return _9TRUE_428;
L6: 

    /** 	if t[T_ID] >= 1 and t[T_ID] <= MAX_OPCODE then*/
    _2 = (int)SEQ_PTR(_t_16842);
    _9533 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9533)) {
        _9534 = (_9533 >= 1);
    }
    else {
        _9534 = binary_op(GREATEREQ, _9533, 1);
    }
    _9533 = NOVALUE;
    if (IS_ATOM_INT(_9534)) {
        if (_9534 == 0) {
            goto L7; // [171] 200
        }
    }
    else {
        if (DBL_PTR(_9534)->dbl == 0.0) {
            goto L7; // [171] 200
        }
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9536 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9536)) {
        _9537 = (_9536 <= 213);
    }
    else {
        _9537 = binary_op(LESSEQ, _9536, 213);
    }
    _9536 = NOVALUE;
    if (_9537 == 0) {
        DeRef(_9537);
        _9537 = NOVALUE;
        goto L7; // [188] 200
    }
    else {
        if (!IS_ATOM_INT(_9537) && DBL_PTR(_9537)->dbl == 0.0){
            DeRef(_9537);
            _9537 = NOVALUE;
            goto L7; // [188] 200
        }
        DeRef(_9537);
        _9537 = NOVALUE;
    }
    DeRef(_9537);
    _9537 = NOVALUE;

    /** 		return TRUE*/
    DeRef(_t_16842);
    DeRef(_9521);
    _9521 = NOVALUE;
    DeRef(_9524);
    _9524 = NOVALUE;
    DeRef(_9527);
    _9527 = NOVALUE;
    DeRef(_9529);
    _9529 = NOVALUE;
    DeRef(_9534);
    _9534 = NOVALUE;
    return _9TRUE_428;
L7: 

    /** 	if END <= t[T_ID] and t[T_ID] <=  ROUTINE then*/
    _2 = (int)SEQ_PTR(_t_16842);
    _9538 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9538)) {
        _9539 = (402 <= _9538);
    }
    else {
        _9539 = binary_op(LESSEQ, 402, _9538);
    }
    _9538 = NOVALUE;
    if (IS_ATOM_INT(_9539)) {
        if (_9539 == 0) {
            goto L8; // [214] 306
        }
    }
    else {
        if (DBL_PTR(_9539)->dbl == 0.0) {
            goto L8; // [214] 306
        }
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9541 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9541)) {
        _9542 = (_9541 <= 432);
    }
    else {
        _9542 = binary_op(LESSEQ, _9541, 432);
    }
    _9541 = NOVALUE;
    if (_9542 == 0) {
        DeRef(_9542);
        _9542 = NOVALUE;
        goto L8; // [231] 306
    }
    else {
        if (!IS_ATOM_INT(_9542) && DBL_PTR(_9542)->dbl == 0.0){
            DeRef(_9542);
            _9542 = NOVALUE;
            goto L8; // [231] 306
        }
        DeRef(_9542);
        _9542 = NOVALUE;
    }
    DeRef(_9542);
    _9542 = NOVALUE;

    /** 		if t[T_ID] != IGNORED and t[T_ID] < 500 and symtab_index(t[T_SYM]) = 0 then*/
    _2 = (int)SEQ_PTR(_t_16842);
    _9543 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9543)) {
        _9544 = (_9543 != 509);
    }
    else {
        _9544 = binary_op(NOTEQ, _9543, 509);
    }
    _9543 = NOVALUE;
    if (IS_ATOM_INT(_9544)) {
        if (_9544 == 0) {
            DeRef(_9545);
            _9545 = 0;
            goto L9; // [248] 266
        }
    }
    else {
        if (DBL_PTR(_9544)->dbl == 0.0) {
            DeRef(_9545);
            _9545 = 0;
            goto L9; // [248] 266
        }
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9546 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9546)) {
        _9547 = (_9546 < 500);
    }
    else {
        _9547 = binary_op(LESS, _9546, 500);
    }
    _9546 = NOVALUE;
    DeRef(_9545);
    if (IS_ATOM_INT(_9547))
    _9545 = (_9547 != 0);
    else
    _9545 = DBL_PTR(_9547)->dbl != 0.0;
L9: 
    if (_9545 == 0) {
        goto LA; // [266] 297
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9549 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_9549);
    _9550 = _38symtab_index(_9549);
    _9549 = NOVALUE;
    if (IS_ATOM_INT(_9550)) {
        _9551 = (_9550 == 0);
    }
    else {
        _9551 = binary_op(EQUALS, _9550, 0);
    }
    DeRef(_9550);
    _9550 = NOVALUE;
    if (_9551 == 0) {
        DeRef(_9551);
        _9551 = NOVALUE;
        goto LA; // [285] 297
    }
    else {
        if (!IS_ATOM_INT(_9551) && DBL_PTR(_9551)->dbl == 0.0){
            DeRef(_9551);
            _9551 = NOVALUE;
            goto LA; // [285] 297
        }
        DeRef(_9551);
        _9551 = NOVALUE;
    }
    DeRef(_9551);
    _9551 = NOVALUE;

    /** 			return FALSE*/
    DeRef(_t_16842);
    DeRef(_9521);
    _9521 = NOVALUE;
    DeRef(_9524);
    _9524 = NOVALUE;
    DeRef(_9527);
    _9527 = NOVALUE;
    DeRef(_9529);
    _9529 = NOVALUE;
    DeRef(_9534);
    _9534 = NOVALUE;
    DeRef(_9539);
    _9539 = NOVALUE;
    DeRef(_9544);
    _9544 = NOVALUE;
    DeRef(_9547);
    _9547 = NOVALUE;
    return _9FALSE_426;
LA: 

    /** 		return TRUE*/
    DeRef(_t_16842);
    DeRef(_9521);
    _9521 = NOVALUE;
    DeRef(_9524);
    _9524 = NOVALUE;
    DeRef(_9527);
    _9527 = NOVALUE;
    DeRef(_9529);
    _9529 = NOVALUE;
    DeRef(_9534);
    _9534 = NOVALUE;
    DeRef(_9539);
    _9539 = NOVALUE;
    DeRef(_9544);
    _9544 = NOVALUE;
    DeRef(_9547);
    _9547 = NOVALUE;
    return _9TRUE_428;
L8: 

    /** 	if FUNC <= t[T_ID] and t[T_ID] <= NAMESPACE then*/
    _2 = (int)SEQ_PTR(_t_16842);
    _9552 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9552)) {
        _9553 = (501 <= _9552);
    }
    else {
        _9553 = binary_op(LESSEQ, 501, _9552);
    }
    _9552 = NOVALUE;
    if (IS_ATOM_INT(_9553)) {
        if (_9553 == 0) {
            goto LB; // [320] 349
        }
    }
    else {
        if (DBL_PTR(_9553)->dbl == 0.0) {
            goto LB; // [320] 349
        }
    }
    _2 = (int)SEQ_PTR(_t_16842);
    _9555 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_9555)) {
        _9556 = (_9555 <= 523);
    }
    else {
        _9556 = binary_op(LESSEQ, _9555, 523);
    }
    _9555 = NOVALUE;
    if (_9556 == 0) {
        DeRef(_9556);
        _9556 = NOVALUE;
        goto LB; // [337] 349
    }
    else {
        if (!IS_ATOM_INT(_9556) && DBL_PTR(_9556)->dbl == 0.0){
            DeRef(_9556);
            _9556 = NOVALUE;
            goto LB; // [337] 349
        }
        DeRef(_9556);
        _9556 = NOVALUE;
    }
    DeRef(_9556);
    _9556 = NOVALUE;

    /** 		return TRUE*/
    DeRef(_t_16842);
    DeRef(_9521);
    _9521 = NOVALUE;
    DeRef(_9524);
    _9524 = NOVALUE;
    DeRef(_9527);
    _9527 = NOVALUE;
    DeRef(_9529);
    _9529 = NOVALUE;
    DeRef(_9534);
    _9534 = NOVALUE;
    DeRef(_9539);
    _9539 = NOVALUE;
    DeRef(_9544);
    _9544 = NOVALUE;
    DeRef(_9547);
    _9547 = NOVALUE;
    DeRef(_9553);
    _9553 = NOVALUE;
    return _9TRUE_428;
LB: 

    /** 	return FALSE*/
    DeRef(_t_16842);
    DeRef(_9521);
    _9521 = NOVALUE;
    DeRef(_9524);
    _9524 = NOVALUE;
    DeRef(_9527);
    _9527 = NOVALUE;
    DeRef(_9529);
    _9529 = NOVALUE;
    DeRef(_9534);
    _9534 = NOVALUE;
    DeRef(_9539);
    _9539 = NOVALUE;
    DeRef(_9544);
    _9544 = NOVALUE;
    DeRef(_9547);
    _9547 = NOVALUE;
    DeRef(_9553);
    _9553 = NOVALUE;
    return _9FALSE_426;
    ;
}


int _38sequence_of_tokens(int _x_16915)
{
    int _t_16916 = NOVALUE;
    int _9558 = NOVALUE;
    int _9557 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(x) then*/
    _9557 = IS_ATOM(_x_16915);
    if (_9557 == 0)
    {
        _9557 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _9557 = NOVALUE;
    }

    /** 		return FALSE*/
    DeRef(_x_16915);
    DeRef(_t_16916);
    return _9FALSE_426;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_16915)){
            _9558 = SEQ_PTR(_x_16915)->length;
    }
    else {
        _9558 = 1;
    }
    {
        int _i_16921;
        _i_16921 = 1;
L2: 
        if (_i_16921 > _9558){
            goto L3; // [23] 48
        }

        /** 		type_i = i*/
        _38type_i_16577 = _i_16921;

        /** 		t = x[i]*/
        DeRef(_t_16916);
        _2 = (int)SEQ_PTR(_x_16915);
        _t_16916 = (int)*(((s1_ptr)_2)->base + _i_16921);
        Ref(_t_16916);

        /** 	end for*/
        _i_16921 = _i_16921 + 1;
        goto L2; // [43] 30
L3: 
        ;
    }

    /** 	return TRUE*/
    DeRef(_x_16915);
    DeRef(_t_16916);
    return _9TRUE_428;
    ;
}


int _38sequence_of_opcodes(int _s_16927)
{
    int _oc_16928 = NOVALUE;
    int _9561 = NOVALUE;
    int _9560 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(s) then*/
    _9560 = IS_ATOM(_s_16927);
    if (_9560 == 0)
    {
        _9560 = NOVALUE;
        goto L1; // [6] 18
    }
    else{
        _9560 = NOVALUE;
    }

    /** 		return FALSE*/
    DeRef(_s_16927);
    return _9FALSE_426;
L1: 

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_16927)){
            _9561 = SEQ_PTR(_s_16927)->length;
    }
    else {
        _9561 = 1;
    }
    {
        int _i_16933;
        _i_16933 = 1;
L2: 
        if (_i_16933 > _9561){
            goto L3; // [23] 45
        }

        /** 		oc = s[i]*/
        _2 = (int)SEQ_PTR(_s_16927);
        _oc_16928 = (int)*(((s1_ptr)_2)->base + _i_16933);
        if (!IS_ATOM_INT(_oc_16928)){
            _oc_16928 = (long)DBL_PTR(_oc_16928)->dbl;
        }

        /** 	end for*/
        _i_16933 = _i_16933 + 1;
        goto L2; // [40] 30
L3: 
        ;
    }

    /** 	return TRUE*/
    DeRef(_s_16927);
    return _9TRUE_428;
    ;
}


int _38file(int _f_16939)
{
    int _9565 = NOVALUE;
    int _9564 = NOVALUE;
    int _9563 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_f_16939)) {
        _1 = (long)(DBL_PTR(_f_16939)->dbl);
        if (UNIQUE(DBL_PTR(_f_16939)) && (DBL_PTR(_f_16939)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_f_16939);
        _f_16939 = _1;
    }

    /** 	return f >= -1 and f < 100 -- rough limit*/
    _9563 = (_f_16939 >= -1);
    _9564 = (_f_16939 < 100);
    _9565 = (_9563 != 0 && _9564 != 0);
    _9563 = NOVALUE;
    _9564 = NOVALUE;
    return _9565;
    ;
}


int _38op_code_or_negative_one(int _x_17053)
{
    int _9610 = NOVALUE;
    int _9609 = NOVALUE;
    int _9608 = NOVALUE;
    int _9607 = NOVALUE;
    int _9606 = NOVALUE;
    int _9605 = NOVALUE;
    int _9604 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return integer(x) and (x=-1 or (1 <= x and x <= MAX_OPCODE))*/
    if (IS_ATOM_INT(_x_17053))
    _9604 = 1;
    else if (IS_ATOM_DBL(_x_17053))
    _9604 = IS_ATOM_INT(DoubleToInt(_x_17053));
    else
    _9604 = 0;
    if (IS_ATOM_INT(_x_17053)) {
        _9605 = (_x_17053 == -1);
    }
    else {
        _9605 = binary_op(EQUALS, _x_17053, -1);
    }
    if (IS_ATOM_INT(_x_17053)) {
        _9606 = (1 <= _x_17053);
    }
    else {
        _9606 = binary_op(LESSEQ, 1, _x_17053);
    }
    if (IS_ATOM_INT(_x_17053)) {
        _9607 = (_x_17053 <= 213);
    }
    else {
        _9607 = binary_op(LESSEQ, _x_17053, 213);
    }
    if (IS_ATOM_INT(_9606) && IS_ATOM_INT(_9607)) {
        _9608 = (_9606 != 0 && _9607 != 0);
    }
    else {
        _9608 = binary_op(AND, _9606, _9607);
    }
    DeRef(_9606);
    _9606 = NOVALUE;
    DeRef(_9607);
    _9607 = NOVALUE;
    if (IS_ATOM_INT(_9605) && IS_ATOM_INT(_9608)) {
        _9609 = (_9605 != 0 || _9608 != 0);
    }
    else {
        _9609 = binary_op(OR, _9605, _9608);
    }
    DeRef(_9605);
    _9605 = NOVALUE;
    DeRef(_9608);
    _9608 = NOVALUE;
    if (IS_ATOM_INT(_9609)) {
        _9610 = (_9604 != 0 && _9609 != 0);
    }
    else {
        _9610 = binary_op(AND, _9604, _9609);
    }
    _9604 = NOVALUE;
    DeRef(_9609);
    _9609 = NOVALUE;
    DeRef(_x_17053);
    return _9610;
    ;
}


int _38symtab_pointer(int _x_64211)
{
    int _32338 = NOVALUE;
    int _32337 = NOVALUE;
    int _32336 = NOVALUE;
    int _32335 = NOVALUE;
    int _32334 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_64211)) {
        _1 = (long)(DBL_PTR(_x_64211)->dbl);
        if (UNIQUE(DBL_PTR(_x_64211)) && (DBL_PTR(_x_64211)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_64211);
        _x_64211 = _1;
    }

    /** 	return x = -1 or symtab_index(x) or forward_reference(x)*/
    _32334 = (_x_64211 == -1);
    _32335 = _38symtab_index(_x_64211);
    if (IS_ATOM_INT(_32335)) {
        _32336 = (_32334 != 0 || _32335 != 0);
    }
    else {
        _32336 = binary_op(OR, _32334, _32335);
    }
    _32334 = NOVALUE;
    DeRef(_32335);
    _32335 = NOVALUE;
    _32337 = _40forward_reference(_x_64211);
    if (IS_ATOM_INT(_32336) && IS_ATOM_INT(_32337)) {
        _32338 = (_32336 != 0 || _32337 != 0);
    }
    else {
        _32338 = binary_op(OR, _32336, _32337);
    }
    DeRef(_32336);
    _32336 = NOVALUE;
    DeRef(_32337);
    _32337 = NOVALUE;
    return _32338;
    ;
}



// 0x2F073FE2
