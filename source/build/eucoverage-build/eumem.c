// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _28malloc(int _mem_struct_p_13204, int _cleanup_p_13205)
{
    int _temp__13206 = NOVALUE;
    int _7233 = NOVALUE;
    int _7231 = NOVALUE;
    int _7230 = NOVALUE;
    int _7229 = NOVALUE;
    int _7225 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_cleanup_p_13205)) {
        _1 = (long)(DBL_PTR(_cleanup_p_13205)->dbl);
        if (UNIQUE(DBL_PTR(_cleanup_p_13205)) && (DBL_PTR(_cleanup_p_13205)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_cleanup_p_13205);
        _cleanup_p_13205 = _1;
    }

    /** 	if atom(mem_struct_p) then*/
    _7225 = IS_ATOM(_mem_struct_p_13204);
    if (_7225 == 0)
    {
        _7225 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _7225 = NOVALUE;
    }

    /** 		mem_struct_p = repeat(0, mem_struct_p)*/
    _0 = _mem_struct_p_13204;
    _mem_struct_p_13204 = Repeat(0, _mem_struct_p_13204);
    DeRef(_0);
L1: 

    /** 	if ram_free_list = 0 then*/
    if (_28ram_free_list_13200 != 0)
    goto L2; // [24] 74

    /** 		ram_space = append(ram_space, mem_struct_p)*/
    Ref(_mem_struct_p_13204);
    Append(&_28ram_space_13199, _28ram_space_13199, _mem_struct_p_13204);

    /** 		if cleanup_p then*/
    if (_cleanup_p_13205 == 0)
    {
        goto L3; // [38] 61
    }
    else{
    }

    /** 			return delete_routine( length(ram_space), free_rid )*/
    if (IS_SEQUENCE(_28ram_space_13199)){
            _7229 = SEQ_PTR(_28ram_space_13199)->length;
    }
    else {
        _7229 = 1;
    }
    _7230 = NewDouble( (double) _7229 );
    _1 = (int) _00[_28free_rid_13201].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_28free_rid_13201].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _28free_rid_13201;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_7230)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_7230)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_7230)) ){
        DeRefDS(_7230);
        _7230 = NewDouble( DBL_PTR(_7230)->dbl );
    }
    DBL_PTR(_7230)->cleanup = (cleanup_ptr)_1;
    _7229 = NOVALUE;
    DeRef(_mem_struct_p_13204);
    return _7230;
    goto L4; // [58] 73
L3: 

    /** 			return length(ram_space)*/
    if (IS_SEQUENCE(_28ram_space_13199)){
            _7231 = SEQ_PTR(_28ram_space_13199)->length;
    }
    else {
        _7231 = 1;
    }
    DeRef(_mem_struct_p_13204);
    DeRef(_7230);
    _7230 = NOVALUE;
    return _7231;
L4: 
L2: 

    /** 	temp_ = ram_free_list*/
    _temp__13206 = _28ram_free_list_13200;

    /** 	ram_free_list = ram_space[temp_]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _28ram_free_list_13200 = (int)*(((s1_ptr)_2)->base + _temp__13206);
    if (!IS_ATOM_INT(_28ram_free_list_13200))
    _28ram_free_list_13200 = (long)DBL_PTR(_28ram_free_list_13200)->dbl;

    /** 	ram_space[temp_] = mem_struct_p*/
    Ref(_mem_struct_p_13204);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp__13206);
    _1 = *(int *)_2;
    *(int *)_2 = _mem_struct_p_13204;
    DeRef(_1);

    /** 	if cleanup_p then*/
    if (_cleanup_p_13205 == 0)
    {
        goto L5; // [103] 121
    }
    else{
    }

    /** 		return delete_routine( temp_, free_rid )*/
    _7233 = NewDouble( (double) _temp__13206 );
    _1 = (int) _00[_28free_rid_13201].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_28free_rid_13201].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _28free_rid_13201;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_7233)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_7233)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_7233)) ){
        DeRefDS(_7233);
        _7233 = NewDouble( DBL_PTR(_7233)->dbl );
    }
    DBL_PTR(_7233)->cleanup = (cleanup_ptr)_1;
    DeRef(_mem_struct_p_13204);
    DeRef(_7230);
    _7230 = NOVALUE;
    return _7233;
    goto L6; // [118] 128
L5: 

    /** 		return temp_*/
    DeRef(_mem_struct_p_13204);
    DeRef(_7230);
    _7230 = NOVALUE;
    DeRef(_7233);
    _7233 = NOVALUE;
    return _temp__13206;
L6: 
    ;
}


void _28free(int _mem_p_13224)
{
    int _7235 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if mem_p < 1 then return end if*/
    if (binary_op_a(GREATEREQ, _mem_p_13224, 1)){
        goto L1; // [3] 11
    }
    DeRef(_mem_p_13224);
    return;
L1: 

    /** 	if mem_p > length(ram_space) then return end if*/
    if (IS_SEQUENCE(_28ram_space_13199)){
            _7235 = SEQ_PTR(_28ram_space_13199)->length;
    }
    else {
        _7235 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_13224, _7235)){
        _7235 = NOVALUE;
        goto L2; // [18] 26
    }
    _7235 = NOVALUE;
    DeRef(_mem_p_13224);
    return;
L2: 

    /** 	ram_space[mem_p] = ram_free_list*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_mem_p_13224))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_13224)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _mem_p_13224);
    _1 = *(int *)_2;
    *(int *)_2 = _28ram_free_list_13200;
    DeRef(_1);

    /** 	ram_free_list = floor(mem_p)*/
    if (IS_ATOM_INT(_mem_p_13224))
    _28ram_free_list_13200 = e_floor(_mem_p_13224);
    else
    _28ram_free_list_13200 = unary_op(FLOOR, _mem_p_13224);
    if (!IS_ATOM_INT(_28ram_free_list_13200)) {
        _1 = (long)(DBL_PTR(_28ram_free_list_13200)->dbl);
        if (UNIQUE(DBL_PTR(_28ram_free_list_13200)) && (DBL_PTR(_28ram_free_list_13200)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_28ram_free_list_13200);
        _28ram_free_list_13200 = _1;
    }

    /** end procedure*/
    DeRef(_mem_p_13224);
    return;
    ;
}


int _28valid(int _mem_p_13234, int _mem_struct_p_13235)
{
    int _7249 = NOVALUE;
    int _7248 = NOVALUE;
    int _7246 = NOVALUE;
    int _7245 = NOVALUE;
    int _7244 = NOVALUE;
    int _7242 = NOVALUE;
    int _7239 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(mem_p) then return 0 end if*/
    if (IS_ATOM_INT(_mem_p_13234))
    _7239 = 1;
    else if (IS_ATOM_DBL(_mem_p_13234))
    _7239 = IS_ATOM_INT(DoubleToInt(_mem_p_13234));
    else
    _7239 = 0;
    if (_7239 != 0)
    goto L1; // [6] 14
    _7239 = NOVALUE;
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    return 0;
L1: 

    /** 	if mem_p < 1 then return 0 end if*/
    if (binary_op_a(GREATEREQ, _mem_p_13234, 1)){
        goto L2; // [16] 25
    }
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    return 0;
L2: 

    /** 	if mem_p > length(ram_space) then return 0 end if*/
    if (IS_SEQUENCE(_28ram_space_13199)){
            _7242 = SEQ_PTR(_28ram_space_13199)->length;
    }
    else {
        _7242 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_13234, _7242)){
        _7242 = NOVALUE;
        goto L3; // [32] 41
    }
    _7242 = NOVALUE;
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    return 0;
L3: 

    /** 	if sequence(mem_struct_p) then return 1 end if*/
    _7244 = IS_SEQUENCE(_mem_struct_p_13235);
    if (_7244 == 0)
    {
        _7244 = NOVALUE;
        goto L4; // [46] 54
    }
    else{
        _7244 = NOVALUE;
    }
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    return 1;
L4: 

    /** 	if atom(ram_space[mem_p]) then */
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_mem_p_13234)){
        _7245 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_13234)->dbl));
    }
    else{
        _7245 = (int)*(((s1_ptr)_2)->base + _mem_p_13234);
    }
    _7246 = IS_ATOM(_7245);
    _7245 = NOVALUE;
    if (_7246 == 0)
    {
        _7246 = NOVALUE;
        goto L5; // [65] 88
    }
    else{
        _7246 = NOVALUE;
    }

    /** 		if mem_struct_p >= 0 then*/
    if (binary_op_a(LESS, _mem_struct_p_13235, 0)){
        goto L6; // [70] 81
    }

    /** 			return 0*/
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    return 0;
L6: 

    /** 		return 1*/
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    return 1;
L5: 

    /** 	if length(ram_space[mem_p]) != mem_struct_p then*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_mem_p_13234)){
        _7248 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_13234)->dbl));
    }
    else{
        _7248 = (int)*(((s1_ptr)_2)->base + _mem_p_13234);
    }
    if (IS_SEQUENCE(_7248)){
            _7249 = SEQ_PTR(_7248)->length;
    }
    else {
        _7249 = 1;
    }
    _7248 = NOVALUE;
    if (binary_op_a(EQUALS, _7249, _mem_struct_p_13235)){
        _7249 = NOVALUE;
        goto L7; // [99] 110
    }
    _7249 = NOVALUE;

    /** 		return 0*/
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    _7248 = NOVALUE;
    return 0;
L7: 

    /** 	return 1*/
    DeRef(_mem_p_13234);
    DeRef(_mem_struct_p_13235);
    _7248 = NOVALUE;
    return 1;
    ;
}



// 0x3CB0BFF6
