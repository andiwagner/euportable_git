// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _11allocate(int _n_1283, int _cleanup_1284)
{
    int _iaddr_1285 = NOVALUE;
    int _eaddr_1286 = NOVALUE;
    int _563 = NOVALUE;
    int _562 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE then*/

    /** 		iaddr = eu:machine_func( memconst:M_ALLOC, n + memory:BORDER_SPACE * 2)*/
    _562 = 0;
    _563 = _n_1283 + 0;
    _562 = NOVALUE;
    DeRef(_iaddr_1285);
    _iaddr_1285 = machine(16, _563);
    _563 = NOVALUE;

    /** 		eaddr = memory:prepare_block( iaddr, n, PAGE_READ_WRITE )*/
    Ref(_iaddr_1285);
    _0 = _eaddr_1286;
    _eaddr_1286 = _13prepare_block(_iaddr_1285, _n_1283, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 	return eaddr*/
    DeRef(_iaddr_1285);
    return _eaddr_1286;
    ;
}


int _11allocate_data(int _n_1296, int _cleanup_1297)
{
    int _a_1298 = NOVALUE;
    int _sla_1300 = NOVALUE;
    int _568 = NOVALUE;
    int _567 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = eu:machine_func( memconst:M_ALLOC, n+BORDER_SPACE*2)*/
    _567 = 0;
    _568 = 4;
    _567 = NOVALUE;
    DeRef(_a_1298);
    _a_1298 = machine(16, 4);
    _568 = NOVALUE;

    /** 	sla = memory:prepare_block(a, n, PAGE_READ_WRITE )*/
    Ref(_a_1298);
    _0 = _sla_1300;
    _sla_1300 = _13prepare_block(_a_1298, 4, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 		return sla*/
    DeRef(_a_1298);
    return _sla_1300;
    ;
}


int _11VirtualAlloc(int _addr_1480, int _size_1481, int _allocation_type_1482, int _protect__1483)
{
    int _r1_1484 = NOVALUE;
    int _652 = NOVALUE;
    int _0, _1, _2;
    

    /** 		r1 = c_func( VirtualAlloc_rid, {addr, size, allocation_type, protect_ } )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 1;
    Ref(_allocation_type_1482);
    *((int *)(_2+12)) = _allocation_type_1482;
    *((int *)(_2+16)) = 64;
    _652 = MAKE_SEQ(_1);
    DeRef(_r1_1484);
    _r1_1484 = call_c(1, _11VirtualAlloc_rid_1396, _652);
    DeRefDS(_652);
    _652 = NOVALUE;

    /** 		return r1*/
    DeRef(_allocation_type_1482);
    return _r1_1484;
    ;
}


int _11allocate_string(int _s_1503, int _cleanup_1504)
{
    int _mem_1505 = NOVALUE;
    int _659 = NOVALUE;
    int _658 = NOVALUE;
    int _656 = NOVALUE;
    int _655 = NOVALUE;
    int _0, _1, _2;
    

    /** 	mem = allocate( length(s) + 1) -- Thanks to Igor*/
    if (IS_SEQUENCE(_s_1503)){
            _655 = SEQ_PTR(_s_1503)->length;
    }
    else {
        _655 = 1;
    }
    _656 = _655 + 1;
    _655 = NOVALUE;
    _0 = _mem_1505;
    _mem_1505 = _11allocate(_656, 0);
    DeRef(_0);
    _656 = NOVALUE;

    /** 	if mem then*/
    if (_mem_1505 == 0) {
        goto L1; // [19] 54
    }
    else {
        if (!IS_ATOM_INT(_mem_1505) && DBL_PTR(_mem_1505)->dbl == 0.0){
            goto L1; // [19] 54
        }
    }

    /** 		poke(mem, s)*/
    if (IS_ATOM_INT(_mem_1505)){
        poke_addr = (unsigned char *)_mem_1505;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_mem_1505)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_1503);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 		poke(mem+length(s), 0)  -- Thanks to Aku*/
    if (IS_SEQUENCE(_s_1503)){
            _658 = SEQ_PTR(_s_1503)->length;
    }
    else {
        _658 = 1;
    }
    if (IS_ATOM_INT(_mem_1505)) {
        _659 = _mem_1505 + _658;
        if ((long)((unsigned long)_659 + (unsigned long)HIGH_BITS) >= 0) 
        _659 = NewDouble((double)_659);
    }
    else {
        _659 = NewDouble(DBL_PTR(_mem_1505)->dbl + (double)_658);
    }
    _658 = NOVALUE;
    if (IS_ATOM_INT(_659)){
        poke_addr = (unsigned char *)_659;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_659)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_659);
    _659 = NOVALUE;

    /** 		if cleanup then*/
L1: 

    /** 	return mem*/
    DeRefDS(_s_1503);
    return _mem_1505;
    ;
}


int _11allocate_protect(int _data_1516, int _wordsize_1517, int _protection_1519)
{
    int _iaddr_1520 = NOVALUE;
    int _eaddr_1522 = NOVALUE;
    int _size_1523 = NOVALUE;
    int _first_protection_1525 = NOVALUE;
    int _true_protection_1527 = NOVALUE;
    int _prepare_block_inlined_prepare_block_at_108_1542 = NOVALUE;
    int _msg_inlined_crash_at_192_1555 = NOVALUE;
    int _680 = NOVALUE;
    int _679 = NOVALUE;
    int _677 = NOVALUE;
    int _676 = NOVALUE;
    int _675 = NOVALUE;
    int _671 = NOVALUE;
    int _669 = NOVALUE;
    int _666 = NOVALUE;
    int _665 = NOVALUE;
    int _663 = NOVALUE;
    int _661 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom iaddr = 0*/
    DeRef(_iaddr_1520);
    _iaddr_1520 = 0;

    /** 	integer size*/

    /** 	valid_memory_protection_constant true_protection = protection*/
    _true_protection_1527 = 16;

    /** 	ifdef SAFE then	*/

    /** 	if atom(data) then*/
    _661 = 0;
    if (_661 == 0)
    {
        _661 = NOVALUE;
        goto L1; // [20] 41
    }
    else{
        _661 = NOVALUE;
    }

    /** 		size = data * wordsize*/
    _size_1523 = binary_op(MULTIPLY, _data_1516, 1);
    if (!IS_ATOM_INT(_size_1523)) {
        _1 = (long)(DBL_PTR(_size_1523)->dbl);
        if (UNIQUE(DBL_PTR(_size_1523)) && (DBL_PTR(_size_1523)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_1523);
        _size_1523 = _1;
    }

    /** 		first_protection = true_protection*/
    _first_protection_1525 = 16;
    goto L2; // [38] 62
L1: 

    /** 		size = length(data) * wordsize*/
    if (IS_SEQUENCE(_data_1516)){
            _663 = SEQ_PTR(_data_1516)->length;
    }
    else {
        _663 = 1;
    }
    _size_1523 = _663 * _wordsize_1517;
    _663 = NOVALUE;

    /** 		first_protection = PAGE_READ_WRITE*/
    _first_protection_1525 = 4;
L2: 

    /** 	iaddr = local_allocate_protected_memory( size + memory:BORDER_SPACE * 2, first_protection )*/
    _665 = 0;
    _666 = _size_1523 + 0;
    _665 = NOVALUE;
    _0 = _iaddr_1520;
    _iaddr_1520 = _11local_allocate_protected_memory(_666, _first_protection_1525);
    DeRef(_0);
    _666 = NOVALUE;

    /** 	if iaddr = 0 then*/
    if (binary_op_a(NOTEQ, _iaddr_1520, 0)){
        goto L3; // [83] 94
    }

    /** 		return 0*/
    DeRefi(_data_1516);
    DeRef(_iaddr_1520);
    DeRef(_eaddr_1522);
    return 0;
L3: 

    /** 	eaddr = memory:prepare_block( iaddr, size, protection )*/
    if (!IS_ATOM_INT(_protection_1519)) {
        _1 = (long)(DBL_PTR(_protection_1519)->dbl);
        if (UNIQUE(DBL_PTR(_protection_1519)) && (DBL_PTR(_protection_1519)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_1519);
        _protection_1519 = _1;
    }

    /** 	return addr*/
    Ref(_iaddr_1520);
    DeRef(_eaddr_1522);
    _eaddr_1522 = _iaddr_1520;

    /** 	if eaddr = 0 or atom( data ) then*/
    if (IS_ATOM_INT(_eaddr_1522)) {
        _669 = (_eaddr_1522 == 0);
    }
    else {
        _669 = (DBL_PTR(_eaddr_1522)->dbl == (double)0);
    }
    if (_669 != 0) {
        goto L4; // [112] 124
    }
    _671 = IS_ATOM(_data_1516);
    if (_671 == 0)
    {
        _671 = NOVALUE;
        goto L5; // [120] 131
    }
    else{
        _671 = NOVALUE;
    }
L4: 

    /** 		return eaddr*/
    DeRefi(_data_1516);
    DeRef(_iaddr_1520);
    DeRef(_669);
    _669 = NOVALUE;
    return _eaddr_1522;
L5: 

    /** 	switch wordsize do*/
    _0 = _wordsize_1517;
    switch ( _0 ){ 

        /** 		case 1 then*/
        case 1:

        /** 			eu:poke( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_1522)){
            poke_addr = (unsigned char *)_eaddr_1522;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_eaddr_1522)->dbl);
        }
        if (IS_ATOM_INT(_data_1516)) {
            *poke_addr = (unsigned char)_data_1516;
        }
        else if (IS_ATOM(_data_1516)) {
            _1 = (signed char)DBL_PTR(_data_1516)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_1516);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        goto L6; // [147] 196

        /** 		case 2 then*/
        case 2:

        /** 			eu:poke2( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_1522)){
            poke2_addr = (unsigned short *)_eaddr_1522;
        }
        else {
            poke2_addr = (unsigned short *)(unsigned long)(DBL_PTR(_eaddr_1522)->dbl);
        }
        if (IS_ATOM_INT(_data_1516)) {
            *poke2_addr = (unsigned short)_data_1516;
        }
        else if (IS_ATOM(_data_1516)) {
            _1 = (signed char)DBL_PTR(_data_1516)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_1516);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke2_addr++ = (unsigned short)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned short)DBL_PTR(_2)->dbl;
                    *poke2_addr++ = (unsigned short)_0;
                }
            }
        }
        goto L6; // [158] 196

        /** 		case 4 then*/
        case 4:

        /** 			eu:poke4( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_1522)){
            poke4_addr = (unsigned long *)_eaddr_1522;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_eaddr_1522)->dbl);
        }
        if (IS_ATOM_INT(_data_1516)) {
            *poke4_addr = (unsigned long)_data_1516;
        }
        else if (IS_ATOM(_data_1516)) {
            _1 = (unsigned long)DBL_PTR(_data_1516)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_1516);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        goto L6; // [169] 196

        /** 		case else*/
        default:

        /** 			error:crash("Parameter error: Wrong word size %d in allocate_protect().", wordsize)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_192_1555);
        _msg_inlined_crash_at_192_1555 = EPrintf(-9999999, _674, _wordsize_1517);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_192_1555);

        /** end procedure*/
        goto L7; // [190] 193
L7: 
        DeRefi(_msg_inlined_crash_at_192_1555);
        _msg_inlined_crash_at_192_1555 = NOVALUE;
    ;}L6: 

    /** 	ifdef SAFE then*/

    /** 	if local_change_protection_on_protected_memory( iaddr, size + memory:BORDER_SPACE * 2, true_protection ) = -1 then*/
    _675 = 0;
    _676 = _size_1523 + 0;
    _675 = NOVALUE;
    Ref(_iaddr_1520);
    _677 = _11local_change_protection_on_protected_memory(_iaddr_1520, _676, _true_protection_1527);
    _676 = NOVALUE;
    if (binary_op_a(NOTEQ, _677, -1)){
        DeRef(_677);
        _677 = NOVALUE;
        goto L8; // [214] 238
    }
    DeRef(_677);
    _677 = NOVALUE;

    /** 		local_free_protected_memory( iaddr, size + memory:BORDER_SPACE * 2 )*/
    _679 = 0;
    _680 = _size_1523 + 0;
    _679 = NOVALUE;
    Ref(_iaddr_1520);
    _11local_free_protected_memory(_iaddr_1520, _680);
    _680 = NOVALUE;

    /** 		eaddr = 0*/
    DeRef(_eaddr_1522);
    _eaddr_1522 = 0;
L8: 

    /** 	return eaddr*/
    DeRefi(_data_1516);
    DeRef(_iaddr_1520);
    DeRef(_669);
    _669 = NOVALUE;
    return _eaddr_1522;
    ;
}


int _11local_allocate_protected_memory(int _s_1570, int _first_protection_1571)
{
    int _686 = NOVALUE;
    int _685 = NOVALUE;
    int _684 = NOVALUE;
    int _683 = NOVALUE;
    int _682 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _682 = _13dep_works();
    if (_682 == 0) {
        DeRef(_682);
        _682 = NOVALUE;
        goto L1; // [16] 50
    }
    else {
        if (!IS_ATOM_INT(_682) && DBL_PTR(_682)->dbl == 0.0){
            DeRef(_682);
            _682 = NOVALUE;
            goto L1; // [16] 50
        }
        DeRef(_682);
        _682 = NOVALUE;
    }
    DeRef(_682);
    _682 = NOVALUE;

    /** 			return eu:c_func(VirtualAlloc_rid, */
    {unsigned long tu;
         tu = (unsigned long)8192 | (unsigned long)4096;
         _683 = MAKE_UINT(tu);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = _s_1570;
    *((int *)(_2+12)) = _683;
    *((int *)(_2+16)) = _first_protection_1571;
    _684 = MAKE_SEQ(_1);
    _683 = NOVALUE;
    _685 = call_c(1, _11VirtualAlloc_rid_1396, _684);
    DeRefDS(_684);
    _684 = NOVALUE;
    return _685;
    goto L2; // [47] 65
L1: 

    /** 			return machine_func(M_ALLOC, PAGE_SIZE)*/
    _686 = machine(16, _11PAGE_SIZE_1477);
    DeRef(_685);
    _685 = NOVALUE;
    return _686;
L2: 
    ;
}


int _11local_change_protection_on_protected_memory(int _p_1585, int _s_1586, int _new_protection_1587)
{
    int _689 = NOVALUE;
    int _688 = NOVALUE;
    int _687 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _687 = _13dep_works();
    if (_687 == 0) {
        DeRef(_687);
        _687 = NOVALUE;
        goto L1; // [16] 49
    }
    else {
        if (!IS_ATOM_INT(_687) && DBL_PTR(_687)->dbl == 0.0){
            DeRef(_687);
            _687 = NOVALUE;
            goto L1; // [16] 49
        }
        DeRef(_687);
        _687 = NOVALUE;
    }
    DeRef(_687);
    _687 = NOVALUE;

    /** 			if eu:c_func( VirtualProtect_rid, { p, s, new_protection , oldprotptr } ) = 0 then*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_1585);
    *((int *)(_2+4)) = _p_1585;
    *((int *)(_2+8)) = _s_1586;
    *((int *)(_2+12)) = _new_protection_1587;
    Ref(_11oldprotptr_1566);
    *((int *)(_2+16)) = _11oldprotptr_1566;
    _688 = MAKE_SEQ(_1);
    _689 = call_c(1, _11VirtualProtect_rid_1397, _688);
    DeRefDS(_688);
    _688 = NOVALUE;
    if (binary_op_a(NOTEQ, _689, 0)){
        DeRef(_689);
        _689 = NOVALUE;
        goto L2; // [37] 48
    }
    DeRef(_689);
    _689 = NOVALUE;

    /** 				return -1*/
    DeRef(_p_1585);
    return -1;
L2: 
L1: 

    /** 		return 0*/
    DeRef(_p_1585);
    return 0;
    ;
}


void _11local_free_protected_memory(int _p_1597, int _s_1598)
{
    int _695 = NOVALUE;
    int _694 = NOVALUE;
    int _693 = NOVALUE;
    int _692 = NOVALUE;
    int _691 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _691 = _13dep_works();
    if (_691 == 0) {
        DeRef(_691);
        _691 = NOVALUE;
        goto L1; // [12] 35
    }
    else {
        if (!IS_ATOM_INT(_691) && DBL_PTR(_691)->dbl == 0.0){
            DeRef(_691);
            _691 = NOVALUE;
            goto L1; // [12] 35
        }
        DeRef(_691);
        _691 = NOVALUE;
    }
    DeRef(_691);
    _691 = NOVALUE;

    /** 			c_func(VirtualFree_rid, { p, s, MEM_RELEASE })*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_1597);
    *((int *)(_2+4)) = _p_1597;
    *((int *)(_2+8)) = _s_1598;
    *((int *)(_2+12)) = 32768;
    _692 = MAKE_SEQ(_1);
    _693 = call_c(1, _13VirtualFree_rid_1265, _692);
    DeRefDS(_692);
    _692 = NOVALUE;
    goto L2; // [32] 48
L1: 

    /** 			machine_func(M_FREE, {p})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_1597);
    *((int *)(_2+4)) = _p_1597;
    _694 = MAKE_SEQ(_1);
    _695 = machine(17, _694);
    DeRefDS(_694);
    _694 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_p_1597);
    DeRef(_693);
    _693 = NOVALUE;
    DeRef(_695);
    _695 = NOVALUE;
    return;
    ;
}


void _11free(int _addr_1612)
{
    int _msg_inlined_crash_at_27_1621 = NOVALUE;
    int _data_inlined_crash_at_24_1620 = NOVALUE;
    int _addr_inlined_deallocate_at_64_1627 = NOVALUE;
    int _msg_inlined_crash_at_106_1632 = NOVALUE;
    int _702 = NOVALUE;
    int _701 = NOVALUE;
    int _700 = NOVALUE;
    int _699 = NOVALUE;
    int _697 = NOVALUE;
    int _696 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if types:number_array (addr) then*/
    Ref(_addr_1612);
    _696 = _3number_array(_addr_1612);
    if (_696 == 0) {
        DeRef(_696);
        _696 = NOVALUE;
        goto L1; // [7] 97
    }
    else {
        if (!IS_ATOM_INT(_696) && DBL_PTR(_696)->dbl == 0.0){
            DeRef(_696);
            _696 = NOVALUE;
            goto L1; // [7] 97
        }
        DeRef(_696);
        _696 = NOVALUE;
    }
    DeRef(_696);
    _696 = NOVALUE;

    /** 		if types:ascii_string(addr) then*/
    Ref(_addr_1612);
    _697 = _3ascii_string(_addr_1612);
    if (_697 == 0) {
        DeRef(_697);
        _697 = NOVALUE;
        goto L2; // [16] 47
    }
    else {
        if (!IS_ATOM_INT(_697) && DBL_PTR(_697)->dbl == 0.0){
            DeRef(_697);
            _697 = NOVALUE;
            goto L2; // [16] 47
        }
        DeRef(_697);
        _697 = NOVALUE;
    }
    DeRef(_697);
    _697 = NOVALUE;

    /** 			error:crash("free(\"%s\") is not a valid address", {addr})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_1612);
    *((int *)(_2+4)) = _addr_1612;
    _699 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_24_1620);
    _data_inlined_crash_at_24_1620 = _699;
    _699 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_27_1621);
    _msg_inlined_crash_at_27_1621 = EPrintf(-9999999, _698, _data_inlined_crash_at_24_1620);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_27_1621);

    /** end procedure*/
    goto L3; // [41] 44
L3: 
    DeRef(_data_inlined_crash_at_24_1620);
    _data_inlined_crash_at_24_1620 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_27_1621);
    _msg_inlined_crash_at_27_1621 = NOVALUE;
L2: 

    /** 		for i = 1 to length(addr) do*/
    if (IS_SEQUENCE(_addr_1612)){
            _700 = SEQ_PTR(_addr_1612)->length;
    }
    else {
        _700 = 1;
    }
    {
        int _i_1623;
        _i_1623 = 1;
L4: 
        if (_i_1623 > _700){
            goto L5; // [52] 89
        }

        /** 			memory:deallocate( addr[i] )*/
        _2 = (int)SEQ_PTR(_addr_1612);
        _701 = (int)*(((s1_ptr)_2)->base + _i_1623);
        Ref(_701);
        DeRef(_addr_inlined_deallocate_at_64_1627);
        _addr_inlined_deallocate_at_64_1627 = _701;
        _701 = NOVALUE;

        /** 	ifdef DATA_EXECUTE and WINDOWS then*/

        /**    	machine_proc( memconst:M_FREE, addr)*/
        machine(17, _addr_inlined_deallocate_at_64_1627);

        /** end procedure*/
        goto L6; // [77] 80
L6: 
        DeRef(_addr_inlined_deallocate_at_64_1627);
        _addr_inlined_deallocate_at_64_1627 = NOVALUE;

        /** 		end for*/
        _i_1623 = _i_1623 + 1;
        goto L4; // [84] 59
L5: 
        ;
    }

    /** 		return*/
    DeRef(_addr_1612);
    return;
    goto L7; // [94] 127
L1: 

    /** 	elsif sequence(addr) then*/
    _702 = IS_SEQUENCE(_addr_1612);
    if (_702 == 0)
    {
        _702 = NOVALUE;
        goto L8; // [102] 126
    }
    else{
        _702 = NOVALUE;
    }

    /** 		error:crash("free() called with nested sequence")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_106_1632);
    _msg_inlined_crash_at_106_1632 = EPrintf(-9999999, _703, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_106_1632);

    /** end procedure*/
    goto L9; // [120] 123
L9: 
    DeRefi(_msg_inlined_crash_at_106_1632);
    _msg_inlined_crash_at_106_1632 = NOVALUE;
L8: 
L7: 

    /** 	if addr = 0 then*/
    if (binary_op_a(NOTEQ, _addr_1612, 0)){
        goto LA; // [129] 139
    }

    /** 		return*/
    DeRef(_addr_1612);
    return;
LA: 

    /** 	memory:deallocate( addr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_1612);

    /** end procedure*/
    goto LB; // [150] 153
LB: 

    /** end procedure*/
    DeRef(_addr_1612);
    return;
    ;
}


void _11free_pointer_array(int _pointers_array_1640)
{
    int _saved_1641 = NOVALUE;
    int _ptr_1642 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom saved = pointers_array*/
    Ref(_pointers_array_1640);
    DeRef(_saved_1641);
    _saved_1641 = _pointers_array_1640;

    /** 	while ptr with entry do*/
    goto L1; // [8] 39
L2: 
    if (_ptr_1642 <= 0) {
        if (_ptr_1642 == 0) {
            goto L3; // [13] 49
        }
        else {
            if (!IS_ATOM_INT(_ptr_1642) && DBL_PTR(_ptr_1642)->dbl == 0.0){
                goto L3; // [13] 49
            }
        }
    }

    /** 		memory:deallocate( ptr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _ptr_1642);

    /** end procedure*/
    goto L4; // [27] 30
L4: 

    /** 		pointers_array += ADDRESS_LENGTH*/
    _0 = _pointers_array_1640;
    if (IS_ATOM_INT(_pointers_array_1640)) {
        _pointers_array_1640 = _pointers_array_1640 + 4;
        if ((long)((unsigned long)_pointers_array_1640 + (unsigned long)HIGH_BITS) >= 0) 
        _pointers_array_1640 = NewDouble((double)_pointers_array_1640);
    }
    else {
        _pointers_array_1640 = NewDouble(DBL_PTR(_pointers_array_1640)->dbl + (double)4);
    }
    DeRef(_0);

    /** 	entry*/
L1: 

    /** 		ptr = peek4u(pointers_array)*/
    DeRef(_ptr_1642);
    if (IS_ATOM_INT(_pointers_array_1640)) {
        _ptr_1642 = *(unsigned long *)_pointers_array_1640;
        if ((unsigned)_ptr_1642 > (unsigned)MAXINT)
        _ptr_1642 = NewDouble((double)(unsigned long)_ptr_1642);
    }
    else {
        _ptr_1642 = *(unsigned long *)(unsigned long)(DBL_PTR(_pointers_array_1640)->dbl);
        if ((unsigned)_ptr_1642 > (unsigned)MAXINT)
        _ptr_1642 = NewDouble((double)(unsigned long)_ptr_1642);
    }

    /** 	end while*/
    goto L2; // [46] 11
L3: 

    /** 	free(saved)*/
    Ref(_saved_1641);
    _11free(_saved_1641);

    /** end procedure*/
    DeRef(_pointers_array_1640);
    DeRef(_saved_1641);
    DeRef(_ptr_1642);
    return;
    ;
}



// 0xC9365F7D
