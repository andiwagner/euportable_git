// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _5find_all(int _needle_645, int _haystack_646, int _start_647)
{
    int _kx_648 = NOVALUE;
    int _293 = NOVALUE;
    int _292 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer kx = 0*/
    _kx_648 = 0;

    /** 	while start with entry do*/
    goto L1; // [16] 47
L2: 
    if (_start_647 == 0)
    {
        goto L3; // [19] 61
    }
    else{
    }

    /** 		kx += 1*/
    _kx_648 = _kx_648 + 1;

    /** 		haystack[kx] = start*/
    _2 = (int)SEQ_PTR(_haystack_646);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_646 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _kx_648);
    _1 = *(int *)_2;
    *(int *)_2 = _start_647;
    DeRef(_1);

    /** 		start += 1*/
    _start_647 = _start_647 + 1;

    /** 	entry*/
L1: 

    /** 		start = find(needle, haystack, start)*/
    _start_647 = find_from(_needle_645, _haystack_646, _start_647);

    /** 	end while*/
    goto L2; // [58] 19
L3: 

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _292 = _kx_648 + 1;
    if (_292 > MAXINT){
        _292 = NewDouble((double)_292);
    }
    if (IS_SEQUENCE(_haystack_646)){
            _293 = SEQ_PTR(_haystack_646)->length;
    }
    else {
        _293 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_646);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_292)) ? _292 : (long)(DBL_PTR(_292)->dbl);
        int stop = (IS_ATOM_INT(_293)) ? _293 : (long)(DBL_PTR(_293)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_646), start, &_haystack_646 );
            }
            else Tail(SEQ_PTR(_haystack_646), stop+1, &_haystack_646);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_646), start, &_haystack_646);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_646 = Remove_elements(start, stop, (SEQ_PTR(_haystack_646)->ref == 1));
        }
    }
    DeRef(_292);
    _292 = NOVALUE;
    _293 = NOVALUE;

    /** 	return haystack*/
    return _haystack_646;
    ;
}


int _5rfind(int _needle_765, int _haystack_766, int _start_767)
{
    int _len_769 = NOVALUE;
    int _356 = NOVALUE;
    int _355 = NOVALUE;
    int _352 = NOVALUE;
    int _351 = NOVALUE;
    int _349 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer len = length(haystack)*/
    if (IS_SEQUENCE(_haystack_766)){
            _len_769 = SEQ_PTR(_haystack_766)->length;
    }
    else {
        _len_769 = 1;
    }

    /** 	if start = 0 then start = len end if*/
    if (_start_767 != 0)
    goto L1; // [16] 26
    _start_767 = _len_769;
L1: 

    /** 	if (start > len) or (len + start < 1) then*/
    _349 = (_start_767 > _len_769);
    if (_349 != 0) {
        goto L2; // [32] 49
    }
    _351 = _len_769 + _start_767;
    if ((long)((unsigned long)_351 + (unsigned long)HIGH_BITS) >= 0) 
    _351 = NewDouble((double)_351);
    if (IS_ATOM_INT(_351)) {
        _352 = (_351 < 1);
    }
    else {
        _352 = (DBL_PTR(_351)->dbl < (double)1);
    }
    DeRef(_351);
    _351 = NOVALUE;
    if (_352 == 0)
    {
        DeRef(_352);
        _352 = NOVALUE;
        goto L3; // [45] 56
    }
    else{
        DeRef(_352);
        _352 = NOVALUE;
    }
L2: 

    /** 		return 0*/
    DeRefDS(_haystack_766);
    DeRef(_349);
    _349 = NOVALUE;
    return 0;
L3: 

    /** 	if start < 1 then*/
    if (_start_767 >= 1)
    goto L4; // [58] 71

    /** 		start = len + start*/
    _start_767 = _len_769 + _start_767;
L4: 

    /** 	for i = start to 1 by -1 do*/
    {
        int _i_782;
        _i_782 = _start_767;
L5: 
        if (_i_782 < 1){
            goto L6; // [73] 107
        }

        /** 		if equal(haystack[i], needle) then*/
        _2 = (int)SEQ_PTR(_haystack_766);
        _355 = (int)*(((s1_ptr)_2)->base + _i_782);
        if (_355 == _needle_765)
        _356 = 1;
        else if (IS_ATOM_INT(_355) && IS_ATOM_INT(_needle_765))
        _356 = 0;
        else
        _356 = (compare(_355, _needle_765) == 0);
        _355 = NOVALUE;
        if (_356 == 0)
        {
            _356 = NOVALUE;
            goto L7; // [90] 100
        }
        else{
            _356 = NOVALUE;
        }

        /** 			return i*/
        DeRefDS(_haystack_766);
        DeRef(_349);
        _349 = NOVALUE;
        return _i_782;
L7: 

        /** 	end for*/
        _i_782 = _i_782 + -1;
        goto L5; // [102] 80
L6: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_haystack_766);
    DeRef(_349);
    _349 = NOVALUE;
    return 0;
    ;
}


int _5match_replace(int _needle_802, int _haystack_803, int _replacement_804, int _max_805)
{
    int _posn_806 = NOVALUE;
    int _needle_len_807 = NOVALUE;
    int _replacement_len_808 = NOVALUE;
    int _scan_from_809 = NOVALUE;
    int _cnt_810 = NOVALUE;
    int _372 = NOVALUE;
    int _369 = NOVALUE;
    int _367 = NOVALUE;
    int _365 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if max < 0 then*/

    /** 	cnt = length(haystack)*/
    if (IS_SEQUENCE(_haystack_803)){
            _cnt_810 = SEQ_PTR(_haystack_803)->length;
    }
    else {
        _cnt_810 = 1;
    }

    /** 	if max != 0 then*/

    /** 	if atom(needle) then*/
    _365 = IS_ATOM(_needle_802);
    if (_365 == 0)
    {
        _365 = NOVALUE;
        goto L1; // [46] 56
    }
    else{
        _365 = NOVALUE;
    }

    /** 		needle = {needle}*/
    _0 = _needle_802;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needle_802);
    *((int *)(_2+4)) = _needle_802;
    _needle_802 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	if atom(replacement) then*/
    _367 = IS_ATOM(_replacement_804);
    if (_367 == 0)
    {
        _367 = NOVALUE;
        goto L2; // [61] 71
    }
    else{
        _367 = NOVALUE;
    }

    /** 		replacement = {replacement}*/
    _0 = _replacement_804;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_replacement_804);
    *((int *)(_2+4)) = _replacement_804;
    _replacement_804 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	needle_len = length(needle) - 1*/
    if (IS_SEQUENCE(_needle_802)){
            _369 = SEQ_PTR(_needle_802)->length;
    }
    else {
        _369 = 1;
    }
    _needle_len_807 = _369 - 1;
    _369 = NOVALUE;

    /** 	replacement_len = length(replacement)*/
    if (IS_SEQUENCE(_replacement_804)){
            _replacement_len_808 = SEQ_PTR(_replacement_804)->length;
    }
    else {
        _replacement_len_808 = 1;
    }

    /** 	scan_from = 1*/
    _scan_from_809 = 1;

    /** 	while posn with entry do*/
    goto L3; // [98] 148
L4: 
    if (_posn_806 == 0)
    {
        goto L5; // [103] 162
    }
    else{
    }

    /** 		haystack = replace(haystack, replacement, posn, posn + needle_len)*/
    _372 = _posn_806 + _needle_len_807;
    if ((long)((unsigned long)_372 + (unsigned long)HIGH_BITS) >= 0) 
    _372 = NewDouble((double)_372);
    {
        int p1 = _haystack_803;
        int p2 = _replacement_804;
        int p3 = _posn_806;
        int p4 = _372;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_haystack_803;
        Replace( &replace_params );
    }
    DeRef(_372);
    _372 = NOVALUE;

    /** 		cnt -= 1*/
    _cnt_810 = _cnt_810 - 1;

    /** 		if cnt = 0 then*/
    if (_cnt_810 != 0)
    goto L6; // [128] 137

    /** 			exit*/
    goto L5; // [134] 162
L6: 

    /** 		scan_from = posn + replacement_len*/
    _scan_from_809 = _posn_806 + _replacement_len_808;

    /** 	entry*/
L3: 

    /** 		posn = match(needle, haystack, scan_from)*/
    _posn_806 = e_match_from(_needle_802, _haystack_803, _scan_from_809);

    /** 	end while*/
    goto L4; // [159] 101
L5: 

    /** 	return haystack*/
    DeRef(_needle_802);
    DeRef(_replacement_804);
    return _haystack_803;
    ;
}


int _5binary_search(int _needle_835, int _haystack_836, int _start_point_837, int _end_point_838)
{
    int _lo_839 = NOVALUE;
    int _hi_840 = NOVALUE;
    int _mid_841 = NOVALUE;
    int _c_842 = NOVALUE;
    int _398 = NOVALUE;
    int _390 = NOVALUE;
    int _388 = NOVALUE;
    int _385 = NOVALUE;
    int _384 = NOVALUE;
    int _383 = NOVALUE;
    int _382 = NOVALUE;
    int _379 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lo = start_point*/
    _lo_839 = 1;

    /** 	if end_point <= 0 then*/
    if (_end_point_838 > 0)
    goto L1; // [20] 38

    /** 		hi = length(haystack) + end_point*/
    if (IS_SEQUENCE(_haystack_836)){
            _379 = SEQ_PTR(_haystack_836)->length;
    }
    else {
        _379 = 1;
    }
    _hi_840 = _379 + _end_point_838;
    _379 = NOVALUE;
    goto L2; // [35] 46
L1: 

    /** 		hi = end_point*/
    _hi_840 = _end_point_838;
L2: 

    /** 	if lo<1 then*/
    if (_lo_839 >= 1)
    goto L3; // [48] 60

    /** 		lo=1*/
    _lo_839 = 1;
L3: 

    /** 	if lo > hi and length(haystack) > 0 then*/
    _382 = (_lo_839 > _hi_840);
    if (_382 == 0) {
        goto L4; // [68] 91
    }
    if (IS_SEQUENCE(_haystack_836)){
            _384 = SEQ_PTR(_haystack_836)->length;
    }
    else {
        _384 = 1;
    }
    _385 = (_384 > 0);
    _384 = NOVALUE;
    if (_385 == 0)
    {
        DeRef(_385);
        _385 = NOVALUE;
        goto L4; // [80] 91
    }
    else{
        DeRef(_385);
        _385 = NOVALUE;
    }

    /** 		hi = length(haystack)*/
    if (IS_SEQUENCE(_haystack_836)){
            _hi_840 = SEQ_PTR(_haystack_836)->length;
    }
    else {
        _hi_840 = 1;
    }
L4: 

    /** 	mid = start_point*/
    _mid_841 = _start_point_837;

    /** 	c = 0*/
    _c_842 = 0;

    /** 	while lo <= hi do*/
L5: 
    if (_lo_839 > _hi_840)
    goto L6; // [110] 186

    /** 		mid = floor((lo + hi) / 2)*/
    _388 = _lo_839 + _hi_840;
    if ((long)((unsigned long)_388 + (unsigned long)HIGH_BITS) >= 0) 
    _388 = NewDouble((double)_388);
    if (IS_ATOM_INT(_388)) {
        _mid_841 = _388 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _388, 2);
        _mid_841 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_388);
    _388 = NOVALUE;
    if (!IS_ATOM_INT(_mid_841)) {
        _1 = (long)(DBL_PTR(_mid_841)->dbl);
        if (UNIQUE(DBL_PTR(_mid_841)) && (DBL_PTR(_mid_841)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_841);
        _mid_841 = _1;
    }

    /** 		c = eu:compare(needle, haystack[mid])*/
    _2 = (int)SEQ_PTR(_haystack_836);
    _390 = (int)*(((s1_ptr)_2)->base + _mid_841);
    if (IS_ATOM_INT(_needle_835) && IS_ATOM_INT(_390)){
        _c_842 = (_needle_835 < _390) ? -1 : (_needle_835 > _390);
    }
    else{
        _c_842 = compare(_needle_835, _390);
    }
    _390 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_842 >= 0)
    goto L7; // [142] 157

    /** 			hi = mid - 1*/
    _hi_840 = _mid_841 - 1;
    goto L5; // [154] 110
L7: 

    /** 		elsif c > 0 then*/
    if (_c_842 <= 0)
    goto L8; // [159] 174

    /** 			lo = mid + 1*/
    _lo_839 = _mid_841 + 1;
    goto L5; // [171] 110
L8: 

    /** 			return mid*/
    DeRefDS(_haystack_836);
    DeRef(_382);
    _382 = NOVALUE;
    return _mid_841;

    /** 	end while*/
    goto L5; // [183] 110
L6: 

    /** 	if c > 0 then*/
    if (_c_842 <= 0)
    goto L9; // [188] 201

    /** 		mid += 1*/
    _mid_841 = _mid_841 + 1;
L9: 

    /** 	return -mid*/
    if ((unsigned long)_mid_841 == 0xC0000000)
    _398 = (int)NewDouble((double)-0xC0000000);
    else
    _398 = - _mid_841;
    DeRefDS(_haystack_836);
    DeRef(_382);
    _382 = NOVALUE;
    return _398;
    ;
}


int _5begins(int _sub_text_923, int _full_text_924)
{
    int _436 = NOVALUE;
    int _435 = NOVALUE;
    int _434 = NOVALUE;
    int _432 = NOVALUE;
    int _431 = NOVALUE;
    int _430 = NOVALUE;
    int _429 = NOVALUE;
    int _428 = NOVALUE;
    int _426 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_924)){
            _426 = SEQ_PTR(_full_text_924)->length;
    }
    else {
        _426 = 1;
    }
    if (_426 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_923);
    DeRefDS(_full_text_924);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _428 = IS_ATOM(_sub_text_923);
    if (_428 == 0)
    {
        _428 = NOVALUE;
        goto L2; // [24] 57
    }
    else{
        _428 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[1]) then*/
    _2 = (int)SEQ_PTR(_full_text_924);
    _429 = (int)*(((s1_ptr)_2)->base + 1);
    if (_sub_text_923 == _429)
    _430 = 1;
    else if (IS_ATOM_INT(_sub_text_923) && IS_ATOM_INT(_429))
    _430 = 0;
    else
    _430 = (compare(_sub_text_923, _429) == 0);
    _429 = NOVALUE;
    if (_430 == 0)
    {
        _430 = NOVALUE;
        goto L3; // [37] 49
    }
    else{
        _430 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_923);
    DeRefDS(_full_text_924);
    return 1;
    goto L4; // [46] 56
L3: 

    /** 			return 0*/
    DeRef(_sub_text_923);
    DeRefDS(_full_text_924);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_923)){
            _431 = SEQ_PTR(_sub_text_923)->length;
    }
    else {
        _431 = 1;
    }
    if (IS_SEQUENCE(_full_text_924)){
            _432 = SEQ_PTR(_full_text_924)->length;
    }
    else {
        _432 = 1;
    }
    if (_431 <= _432)
    goto L5; // [65] 76

    /** 		return 0*/
    DeRef(_sub_text_923);
    DeRefDS(_full_text_924);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[1.. length(sub_text)]) then*/
    if (IS_SEQUENCE(_sub_text_923)){
            _434 = SEQ_PTR(_sub_text_923)->length;
    }
    else {
        _434 = 1;
    }
    rhs_slice_target = (object_ptr)&_435;
    RHS_Slice(_full_text_924, 1, _434);
    if (_sub_text_923 == _435)
    _436 = 1;
    else if (IS_ATOM_INT(_sub_text_923) && IS_ATOM_INT(_435))
    _436 = 0;
    else
    _436 = (compare(_sub_text_923, _435) == 0);
    DeRefDS(_435);
    _435 = NOVALUE;
    if (_436 == 0)
    {
        _436 = NOVALUE;
        goto L6; // [90] 102
    }
    else{
        _436 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_923);
    DeRefDS(_full_text_924);
    return 1;
    goto L7; // [99] 109
L6: 

    /** 		return 0*/
    DeRef(_sub_text_923);
    DeRefDS(_full_text_924);
    return 0;
L7: 
    ;
}


int _5vlookup(int _find_item_1053, int _grid_data_1054, int _source_col_1055, int _target_col_1056, int _def_value_1057)
{
    int _513 = NOVALUE;
    int _512 = NOVALUE;
    int _510 = NOVALUE;
    int _509 = NOVALUE;
    int _508 = NOVALUE;
    int _507 = NOVALUE;
    int _506 = NOVALUE;
    int _504 = NOVALUE;
    int _503 = NOVALUE;
    int _502 = NOVALUE;
    int _501 = NOVALUE;
    int _500 = NOVALUE;
    int _0, _1, _2;
    

    /**     for i = 1 to length(grid_data) do*/
    _500 = 24;
    {
        int _i_1059;
        _i_1059 = 1;
L1: 
        if (_i_1059 > 24){
            goto L2; // [16] 117
        }

        /**     	if atom(grid_data[i]) then*/
        _2 = (int)SEQ_PTR(_grid_data_1054);
        _501 = (int)*(((s1_ptr)_2)->base + _i_1059);
        _502 = 0;
        _501 = NOVALUE;
        if (_502 == 0)
        {
            _502 = NOVALUE;
            goto L3; // [32] 40
        }
        else{
            _502 = NOVALUE;
        }

        /**     		continue*/
        goto L4; // [37] 112
L3: 

        /**     	if length(grid_data[i]) < source_col then*/
        _2 = (int)SEQ_PTR(_grid_data_1054);
        _503 = (int)*(((s1_ptr)_2)->base + _i_1059);
        if (IS_SEQUENCE(_503)){
                _504 = SEQ_PTR(_503)->length;
        }
        else {
            _504 = 1;
        }
        _503 = NOVALUE;
        if (_504 >= _source_col_1055)
        goto L5; // [49] 58

        /**     		continue*/
        goto L4; // [55] 112
L5: 

        /**     	if equal(find_item, grid_data[i][source_col]) then*/
        _2 = (int)SEQ_PTR(_grid_data_1054);
        _506 = (int)*(((s1_ptr)_2)->base + _i_1059);
        _2 = (int)SEQ_PTR(_506);
        _507 = (int)*(((s1_ptr)_2)->base + _source_col_1055);
        _506 = NOVALUE;
        if (_find_item_1053 == _507)
        _508 = 1;
        else if (IS_ATOM_INT(_find_item_1053) && IS_ATOM_INT(_507))
        _508 = 0;
        else
        _508 = (compare(_find_item_1053, _507) == 0);
        _507 = NOVALUE;
        if (_508 == 0)
        {
            _508 = NOVALUE;
            goto L6; // [72] 110
        }
        else{
            _508 = NOVALUE;
        }

        /** 	    	if length(grid_data[i]) < target_col then*/
        _2 = (int)SEQ_PTR(_grid_data_1054);
        _509 = (int)*(((s1_ptr)_2)->base + _i_1059);
        if (IS_SEQUENCE(_509)){
                _510 = SEQ_PTR(_509)->length;
        }
        else {
            _510 = 1;
        }
        _509 = NOVALUE;
        if (_510 >= _target_col_1056)
        goto L7; // [84] 95

        /**     			return def_value*/
        DeRefDS(_grid_data_1054);
        _503 = NOVALUE;
        _509 = NOVALUE;
        return _def_value_1057;
L7: 

        /**     		return grid_data[i][target_col]*/
        _2 = (int)SEQ_PTR(_grid_data_1054);
        _512 = (int)*(((s1_ptr)_2)->base + _i_1059);
        _2 = (int)SEQ_PTR(_512);
        _513 = (int)*(((s1_ptr)_2)->base + _target_col_1056);
        _512 = NOVALUE;
        Ref(_513);
        DeRefDS(_grid_data_1054);
        DeRefi(_def_value_1057);
        _503 = NOVALUE;
        _509 = NOVALUE;
        return _513;
L6: 

        /**     end for*/
L4: 
        _i_1059 = _i_1059 + 1;
        goto L1; // [112] 23
L2: 
        ;
    }

    /**     return def_value*/
    DeRefDS(_grid_data_1054);
    _503 = NOVALUE;
    _509 = NOVALUE;
    _513 = NOVALUE;
    return _def_value_1057;
    ;
}



// 0x673C6E99
