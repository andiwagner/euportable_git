// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _2fatal(int _errcode_10907, int _msg_10908, int _routine_name_10909, int _parms_10910)
{
    int _6155 = NOVALUE;
    int _0, _1, _2;
    

    /** 	vLastErrors = append(vLastErrors, {errcode, msg, routine_name, parms})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _errcode_10907;
    RefDS(_msg_10908);
    *((int *)(_2+8)) = _msg_10908;
    RefDS(_routine_name_10909);
    *((int *)(_2+12)) = _routine_name_10909;
    RefDS(_parms_10910);
    *((int *)(_2+16)) = _parms_10910;
    _6155 = MAKE_SEQ(_1);
    RefDS(_6155);
    Append(&_2vLastErrors_10904, _2vLastErrors_10904, _6155);
    DeRefDS(_6155);
    _6155 = NOVALUE;

    /** 	if db_fatal_id >= 0 then*/

    /** end procedure*/
    DeRefDSi(_msg_10908);
    DeRefDSi(_routine_name_10909);
    DeRefDS(_parms_10910);
    return;
    ;
}


int _2get4()
{
    int _6171 = NOVALUE;
    int _6170 = NOVALUE;
    int _6169 = NOVALUE;
    int _6168 = NOVALUE;
    int _6167 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(current_db))*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _6167 = getKBchar();
        }
        else
        _6167 = getc(last_r_file_ptr);
    }
    else
    _6167 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_2mem0_10922)){
        poke_addr = (unsigned char *)_2mem0_10922;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke_addr = (unsigned char)_6167;
    _6167 = NOVALUE;

    /** 	poke(mem1, getc(current_db))*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _6168 = getKBchar();
        }
        else
        _6168 = getc(last_r_file_ptr);
    }
    else
    _6168 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_2mem1_10923)){
        poke_addr = (unsigned char *)_2mem1_10923;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_2mem1_10923)->dbl);
    }
    *poke_addr = (unsigned char)_6168;
    _6168 = NOVALUE;

    /** 	poke(mem2, getc(current_db))*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _6169 = getKBchar();
        }
        else
        _6169 = getc(last_r_file_ptr);
    }
    else
    _6169 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_2mem2_10924)){
        poke_addr = (unsigned char *)_2mem2_10924;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_2mem2_10924)->dbl);
    }
    *poke_addr = (unsigned char)_6169;
    _6169 = NOVALUE;

    /** 	poke(mem3, getc(current_db))*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _6170 = getKBchar();
        }
        else
        _6170 = getc(last_r_file_ptr);
    }
    else
    _6170 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_2mem3_10925)){
        poke_addr = (unsigned char *)_2mem3_10925;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_2mem3_10925)->dbl);
    }
    *poke_addr = (unsigned char)_6170;
    _6170 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_2mem0_10922)) {
        _6171 = *(unsigned long *)_2mem0_10922;
        if ((unsigned)_6171 > (unsigned)MAXINT)
        _6171 = NewDouble((double)(unsigned long)_6171);
    }
    else {
        _6171 = *(unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        if ((unsigned)_6171 > (unsigned)MAXINT)
        _6171 = NewDouble((double)(unsigned long)_6171);
    }
    return _6171;
    ;
}


int _2get_string()
{
    int _where_inlined_where_at_35_10949 = NOVALUE;
    int _s_10939 = NOVALUE;
    int _c_10940 = NOVALUE;
    int _i_10941 = NOVALUE;
    int _6183 = NOVALUE;
    int _6180 = NOVALUE;
    int _6178 = NOVALUE;
    int _6176 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = repeat(0, 256)*/
    DeRefi(_s_10939);
    _s_10939 = Repeat(0, 256);

    /** 	i = 0*/
    _i_10941 = 0;

    /** 	while c with entry do*/
    goto L1; // [16] 95
L2: 
    if (_c_10940 == 0)
    {
        goto L3; // [21] 109
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_10940 != -1)
    goto L4; // [26] 58

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "get_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_35_10949);
    _where_inlined_where_at_35_10949 = machine(20, _2current_db_10880);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_35_10949);
    *((int *)(_2+4)) = _where_inlined_where_at_35_10949;
    _6176 = MAKE_SEQ(_1);
    RefDS(_6174);
    RefDS(_6175);
    _2fatal(900, _6174, _6175, _6176);
    _6176 = NOVALUE;

    /** 			exit*/
    goto L3; // [55] 109
L4: 

    /** 		i += 1*/
    _i_10941 = _i_10941 + 1;

    /** 		if i > length(s) then*/
    if (IS_SEQUENCE(_s_10939)){
            _6178 = SEQ_PTR(_s_10939)->length;
    }
    else {
        _6178 = 1;
    }
    if (_i_10941 <= _6178)
    goto L5; // [71] 86

    /** 			s &= repeat(0, 256)*/
    _6180 = Repeat(0, 256);
    Concat((object_ptr)&_s_10939, _s_10939, _6180);
    DeRefDS(_6180);
    _6180 = NOVALUE;
L5: 

    /** 		s[i] = c*/
    _2 = (int)SEQ_PTR(_s_10939);
    _2 = (int)(((s1_ptr)_2)->base + _i_10941);
    *(int *)_2 = _c_10940;

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_10940 = getKBchar();
        }
        else
        _c_10940 = getc(last_r_file_ptr);
    }
    else
    _c_10940 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [106] 19
L3: 

    /** 	return s[1..i]*/
    rhs_slice_target = (object_ptr)&_6183;
    RHS_Slice(_s_10939, 1, _i_10941);
    DeRefDSi(_s_10939);
    return _6183;
    ;
}


int _2equal_string(int _target_10961)
{
    int _c_10962 = NOVALUE;
    int _i_10963 = NOVALUE;
    int _where_inlined_where_at_31_10969 = NOVALUE;
    int _6194 = NOVALUE;
    int _6193 = NOVALUE;
    int _6190 = NOVALUE;
    int _6188 = NOVALUE;
    int _6186 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = 0*/
    _i_10963 = 0;

    /** 	while c with entry do*/
    goto L1; // [12] 102
L2: 
    if (_c_10962 == 0)
    {
        goto L3; // [17] 116
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_10962 != -1)
    goto L4; // [22] 58

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "equal_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_31_10969);
    _where_inlined_where_at_31_10969 = machine(20, _2current_db_10880);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_31_10969);
    *((int *)(_2+4)) = _where_inlined_where_at_31_10969;
    _6186 = MAKE_SEQ(_1);
    RefDS(_6174);
    RefDS(_6185);
    _2fatal(900, _6174, _6185, _6186);
    _6186 = NOVALUE;

    /** 			return DB_FATAL_FAIL*/
    DeRefDS(_target_10961);
    return -404;
L4: 

    /** 		i += 1*/
    _i_10963 = _i_10963 + 1;

    /** 		if i > length(target) then*/
    if (IS_SEQUENCE(_target_10961)){
            _6188 = SEQ_PTR(_target_10961)->length;
    }
    else {
        _6188 = 1;
    }
    if (_i_10963 <= _6188)
    goto L5; // [71] 82

    /** 			return 0*/
    DeRefDS(_target_10961);
    return 0;
L5: 

    /** 		if target[i] != c then*/
    _2 = (int)SEQ_PTR(_target_10961);
    _6190 = (int)*(((s1_ptr)_2)->base + _i_10963);
    if (binary_op_a(EQUALS, _6190, _c_10962)){
        _6190 = NOVALUE;
        goto L6; // [88] 99
    }
    _6190 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_target_10961);
    return 0;
L6: 

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_10962 = getKBchar();
        }
        else
        _c_10962 = getc(last_r_file_ptr);
    }
    else
    _c_10962 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [113] 15
L3: 

    /** 	return (i = length(target))*/
    if (IS_SEQUENCE(_target_10961)){
            _6193 = SEQ_PTR(_target_10961)->length;
    }
    else {
        _6193 = 1;
    }
    _6194 = (_i_10963 == _6193);
    _6193 = NOVALUE;
    DeRefDS(_target_10961);
    return _6194;
    ;
}


int _2decompress(int _c_11007)
{
    int _s_11008 = NOVALUE;
    int _len_11009 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_180_11044 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_177_11043 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_255_11057 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_252_11056 = NOVALUE;
    int _6249 = NOVALUE;
    int _6248 = NOVALUE;
    int _6247 = NOVALUE;
    int _6244 = NOVALUE;
    int _6239 = NOVALUE;
    int _6238 = NOVALUE;
    int _6237 = NOVALUE;
    int _6236 = NOVALUE;
    int _6235 = NOVALUE;
    int _6234 = NOVALUE;
    int _6233 = NOVALUE;
    int _6232 = NOVALUE;
    int _6231 = NOVALUE;
    int _6230 = NOVALUE;
    int _6229 = NOVALUE;
    int _6228 = NOVALUE;
    int _6227 = NOVALUE;
    int _6226 = NOVALUE;
    int _6225 = NOVALUE;
    int _6224 = NOVALUE;
    int _6223 = NOVALUE;
    int _6222 = NOVALUE;
    int _6221 = NOVALUE;
    int _6220 = NOVALUE;
    int _6219 = NOVALUE;
    int _6218 = NOVALUE;
    int _6217 = NOVALUE;
    int _6216 = NOVALUE;
    int _6215 = NOVALUE;
    int _6214 = NOVALUE;
    int _6213 = NOVALUE;
    int _6212 = NOVALUE;
    int _6211 = NOVALUE;
    int _6208 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_11007 != 0)
    goto L1; // [7] 38

    /** 		c = getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_11007 = getKBchar();
        }
        else
        _c_11007 = getc(last_r_file_ptr);
    }
    else
    _c_11007 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_11007 >= 249)
    goto L2; // [22] 37

    /** 			return c + MIN1B*/
    _6208 = _c_11007 + -9;
    DeRef(_s_11008);
    return _6208;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_11007;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(current_db) +*/
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6211 = getKBchar();
            }
            else
            _6211 = getc(last_r_file_ptr);
        }
        else
        _6211 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6212 = getKBchar();
            }
            else
            _6212 = getc(last_r_file_ptr);
        }
        else
        _6212 = getc(last_r_file_ptr);
        _6213 = 256 * _6212;
        _6212 = NOVALUE;
        _6214 = _6211 + _6213;
        _6211 = NOVALUE;
        _6213 = NOVALUE;
        _6215 = _6214 + _2MIN2B_10990;
        if ((long)((unsigned long)_6215 + (unsigned long)HIGH_BITS) >= 0) 
        _6215 = NewDouble((double)_6215);
        _6214 = NOVALUE;
        DeRef(_s_11008);
        DeRef(_6208);
        _6208 = NOVALUE;
        return _6215;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(current_db) +*/
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6216 = getKBchar();
            }
            else
            _6216 = getc(last_r_file_ptr);
        }
        else
        _6216 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6217 = getKBchar();
            }
            else
            _6217 = getc(last_r_file_ptr);
        }
        else
        _6217 = getc(last_r_file_ptr);
        _6218 = 256 * _6217;
        _6217 = NOVALUE;
        _6219 = _6216 + _6218;
        _6216 = NOVALUE;
        _6218 = NOVALUE;
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6220 = getKBchar();
            }
            else
            _6220 = getc(last_r_file_ptr);
        }
        else
        _6220 = getc(last_r_file_ptr);
        _6221 = 65536 * _6220;
        _6220 = NOVALUE;
        _6222 = _6219 + _6221;
        _6219 = NOVALUE;
        _6221 = NOVALUE;
        _6223 = _6222 + _2MIN3B_10996;
        if ((long)((unsigned long)_6223 + (unsigned long)HIGH_BITS) >= 0) 
        _6223 = NewDouble((double)_6223);
        _6222 = NOVALUE;
        DeRef(_s_11008);
        DeRef(_6208);
        _6208 = NOVALUE;
        DeRef(_6215);
        _6215 = NOVALUE;
        return _6223;

        /** 		case I4B then*/
        case 251:

        /** 			return get4() + MIN4B*/
        _6224 = _2get4();
        if (IS_ATOM_INT(_6224) && IS_ATOM_INT(_2MIN4B_11002)) {
            _6225 = _6224 + _2MIN4B_11002;
            if ((long)((unsigned long)_6225 + (unsigned long)HIGH_BITS) >= 0) 
            _6225 = NewDouble((double)_6225);
        }
        else {
            _6225 = binary_op(PLUS, _6224, _2MIN4B_11002);
        }
        DeRef(_6224);
        _6224 = NOVALUE;
        DeRef(_s_11008);
        DeRef(_6208);
        _6208 = NOVALUE;
        DeRef(_6215);
        _6215 = NOVALUE;
        DeRef(_6223);
        _6223 = NOVALUE;
        return _6225;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(current_db), getc(current_db),*/
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6226 = getKBchar();
            }
            else
            _6226 = getc(last_r_file_ptr);
        }
        else
        _6226 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6227 = getKBchar();
            }
            else
            _6227 = getc(last_r_file_ptr);
        }
        else
        _6227 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6228 = getKBchar();
            }
            else
            _6228 = getc(last_r_file_ptr);
        }
        else
        _6228 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6229 = getKBchar();
            }
            else
            _6229 = getc(last_r_file_ptr);
        }
        else
        _6229 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _6226;
        *((int *)(_2+8)) = _6227;
        *((int *)(_2+12)) = _6228;
        *((int *)(_2+16)) = _6229;
        _6230 = MAKE_SEQ(_1);
        _6229 = NOVALUE;
        _6228 = NOVALUE;
        _6227 = NOVALUE;
        _6226 = NOVALUE;
        DeRefi(_ieee32_inlined_float32_to_atom_at_177_11043);
        _ieee32_inlined_float32_to_atom_at_177_11043 = _6230;
        _6230 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_180_11044);
        _float32_to_atom_inlined_float32_to_atom_at_180_11044 = machine(49, _ieee32_inlined_float32_to_atom_at_177_11043);
        DeRefi(_ieee32_inlined_float32_to_atom_at_177_11043);
        _ieee32_inlined_float32_to_atom_at_177_11043 = NOVALUE;
        DeRef(_s_11008);
        DeRef(_6208);
        _6208 = NOVALUE;
        DeRef(_6215);
        _6215 = NOVALUE;
        DeRef(_6223);
        _6223 = NOVALUE;
        DeRef(_6225);
        _6225 = NOVALUE;
        return _float32_to_atom_inlined_float32_to_atom_at_180_11044;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(current_db), getc(current_db),*/
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6231 = getKBchar();
            }
            else
            _6231 = getc(last_r_file_ptr);
        }
        else
        _6231 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6232 = getKBchar();
            }
            else
            _6232 = getc(last_r_file_ptr);
        }
        else
        _6232 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6233 = getKBchar();
            }
            else
            _6233 = getc(last_r_file_ptr);
        }
        else
        _6233 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6234 = getKBchar();
            }
            else
            _6234 = getc(last_r_file_ptr);
        }
        else
        _6234 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6235 = getKBchar();
            }
            else
            _6235 = getc(last_r_file_ptr);
        }
        else
        _6235 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6236 = getKBchar();
            }
            else
            _6236 = getc(last_r_file_ptr);
        }
        else
        _6236 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6237 = getKBchar();
            }
            else
            _6237 = getc(last_r_file_ptr);
        }
        else
        _6237 = getc(last_r_file_ptr);
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6238 = getKBchar();
            }
            else
            _6238 = getc(last_r_file_ptr);
        }
        else
        _6238 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _6231;
        *((int *)(_2+8)) = _6232;
        *((int *)(_2+12)) = _6233;
        *((int *)(_2+16)) = _6234;
        *((int *)(_2+20)) = _6235;
        *((int *)(_2+24)) = _6236;
        *((int *)(_2+28)) = _6237;
        *((int *)(_2+32)) = _6238;
        _6239 = MAKE_SEQ(_1);
        _6238 = NOVALUE;
        _6237 = NOVALUE;
        _6236 = NOVALUE;
        _6235 = NOVALUE;
        _6234 = NOVALUE;
        _6233 = NOVALUE;
        _6232 = NOVALUE;
        _6231 = NOVALUE;
        DeRefi(_ieee64_inlined_float64_to_atom_at_252_11056);
        _ieee64_inlined_float64_to_atom_at_252_11056 = _6239;
        _6239 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_255_11057);
        _float64_to_atom_inlined_float64_to_atom_at_255_11057 = machine(47, _ieee64_inlined_float64_to_atom_at_252_11056);
        DeRefi(_ieee64_inlined_float64_to_atom_at_252_11056);
        _ieee64_inlined_float64_to_atom_at_252_11056 = NOVALUE;
        DeRef(_s_11008);
        DeRef(_6208);
        _6208 = NOVALUE;
        DeRef(_6215);
        _6215 = NOVALUE;
        DeRef(_6223);
        _6223 = NOVALUE;
        DeRef(_6225);
        _6225 = NOVALUE;
        return _float64_to_atom_inlined_float64_to_atom_at_255_11057;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_11007 != 254)
        goto L3; // [277] 293

        /** 				len = getc(current_db)*/
        if (_2current_db_10880 != last_r_file_no) {
            last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
            last_r_file_no = _2current_db_10880;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_11009 = getKBchar();
            }
            else
            _len_11009 = getc(last_r_file_ptr);
        }
        else
        _len_11009 = getc(last_r_file_ptr);
        goto L4; // [290] 303
L3: 

        /** 				len = get4()*/
        _len_11009 = _2get4();
        if (!IS_ATOM_INT(_len_11009)) {
            _1 = (long)(DBL_PTR(_len_11009)->dbl);
            if (UNIQUE(DBL_PTR(_len_11009)) && (DBL_PTR(_len_11009)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_11009);
            _len_11009 = _1;
        }
L4: 

        /** 			s = repeat(0, len)*/
        DeRef(_s_11008);
        _s_11008 = Repeat(0, _len_11009);

        /** 			for i = 1 to len do*/
        _6244 = _len_11009;
        {
            int _i_11066;
            _i_11066 = 1;
L5: 
            if (_i_11066 > _6244){
                goto L6; // [316] 372
            }

            /** 				c = getc(current_db)*/
            if (_2current_db_10880 != last_r_file_no) {
                last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
                last_r_file_no = _2current_db_10880;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_11007 = getKBchar();
                }
                else
                _c_11007 = getc(last_r_file_ptr);
            }
            else
            _c_11007 = getc(last_r_file_ptr);

            /** 				if c < I2B then*/
            if (_c_11007 >= 249)
            goto L7; // [334] 351

            /** 					s[i] = c + MIN1B*/
            _6247 = _c_11007 + -9;
            _2 = (int)SEQ_PTR(_s_11008);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_11008 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_11066);
            _1 = *(int *)_2;
            *(int *)_2 = _6247;
            if( _1 != _6247 ){
                DeRef(_1);
            }
            _6247 = NOVALUE;
            goto L8; // [348] 365
L7: 

            /** 					s[i] = decompress(c)*/
            DeRef(_6248);
            _6248 = _c_11007;
            _6249 = _2decompress(_6248);
            _6248 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_11008);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_11008 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_11066);
            _1 = *(int *)_2;
            *(int *)_2 = _6249;
            if( _1 != _6249 ){
                DeRef(_1);
            }
            _6249 = NOVALUE;
L8: 

            /** 			end for*/
            _i_11066 = _i_11066 + 1;
            goto L5; // [367] 323
L6: 
            ;
        }

        /** 			return s*/
        DeRef(_6208);
        _6208 = NOVALUE;
        DeRef(_6215);
        _6215 = NOVALUE;
        DeRef(_6223);
        _6223 = NOVALUE;
        DeRef(_6225);
        _6225 = NOVALUE;
        return _s_11008;
    ;}    ;
}


int _2compress(int _x_11077)
{
    int _x4_11078 = NOVALUE;
    int _s_11079 = NOVALUE;
    int _atom_to_float32_inlined_atom_to_float32_at_192_11113 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_203_11116 = NOVALUE;
    int _atom_to_float64_inlined_atom_to_float64_at_229_11121 = NOVALUE;
    int _6288 = NOVALUE;
    int _6287 = NOVALUE;
    int _6286 = NOVALUE;
    int _6284 = NOVALUE;
    int _6283 = NOVALUE;
    int _6281 = NOVALUE;
    int _6279 = NOVALUE;
    int _6278 = NOVALUE;
    int _6277 = NOVALUE;
    int _6275 = NOVALUE;
    int _6274 = NOVALUE;
    int _6273 = NOVALUE;
    int _6272 = NOVALUE;
    int _6271 = NOVALUE;
    int _6270 = NOVALUE;
    int _6269 = NOVALUE;
    int _6268 = NOVALUE;
    int _6267 = NOVALUE;
    int _6265 = NOVALUE;
    int _6264 = NOVALUE;
    int _6263 = NOVALUE;
    int _6262 = NOVALUE;
    int _6261 = NOVALUE;
    int _6260 = NOVALUE;
    int _6258 = NOVALUE;
    int _6257 = NOVALUE;
    int _6256 = NOVALUE;
    int _6255 = NOVALUE;
    int _6254 = NOVALUE;
    int _6253 = NOVALUE;
    int _6252 = NOVALUE;
    int _6251 = NOVALUE;
    int _6250 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_11077))
    _6250 = 1;
    else if (IS_ATOM_DBL(_x_11077))
    _6250 = IS_ATOM_INT(DoubleToInt(_x_11077));
    else
    _6250 = 0;
    if (_6250 == 0)
    {
        _6250 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _6250 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_11077)) {
        _6251 = (_x_11077 >= -9);
    }
    else {
        _6251 = binary_op(GREATEREQ, _x_11077, -9);
    }
    if (IS_ATOM_INT(_6251)) {
        if (_6251 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_6251)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_11077)) {
        _6253 = (_x_11077 <= 239);
    }
    else {
        _6253 = binary_op(LESSEQ, _x_11077, 239);
    }
    if (_6253 == 0) {
        DeRef(_6253);
        _6253 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_6253) && DBL_PTR(_6253)->dbl == 0.0){
            DeRef(_6253);
            _6253 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_6253);
        _6253 = NOVALUE;
    }
    DeRef(_6253);
    _6253 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_11077)) {
        _6254 = _x_11077 - -9;
        if ((long)((unsigned long)_6254 +(unsigned long) HIGH_BITS) >= 0){
            _6254 = NewDouble((double)_6254);
        }
    }
    else {
        _6254 = binary_op(MINUS, _x_11077, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _6254;
    _6255 = MAKE_SEQ(_1);
    _6254 = NOVALUE;
    DeRef(_x_11077);
    DeRefi(_x4_11078);
    DeRef(_s_11079);
    DeRef(_6251);
    _6251 = NOVALUE;
    return _6255;
    goto L3; // [41] 328
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_11077)) {
        _6256 = (_x_11077 >= _2MIN2B_10990);
    }
    else {
        _6256 = binary_op(GREATEREQ, _x_11077, _2MIN2B_10990);
    }
    if (IS_ATOM_INT(_6256)) {
        if (_6256 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_6256)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_11077)) {
        _6258 = (_x_11077 <= 32767);
    }
    else {
        _6258 = binary_op(LESSEQ, _x_11077, 32767);
    }
    if (_6258 == 0) {
        DeRef(_6258);
        _6258 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_6258) && DBL_PTR(_6258)->dbl == 0.0){
            DeRef(_6258);
            _6258 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_6258);
        _6258 = NOVALUE;
    }
    DeRef(_6258);
    _6258 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_11077;
    if (IS_ATOM_INT(_x_11077)) {
        _x_11077 = _x_11077 - _2MIN2B_10990;
        if ((long)((unsigned long)_x_11077 +(unsigned long) HIGH_BITS) >= 0){
            _x_11077 = NewDouble((double)_x_11077);
        }
    }
    else {
        _x_11077 = binary_op(MINUS, _x_11077, _2MIN2B_10990);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_11077)) {
        {unsigned long tu;
             tu = (unsigned long)_x_11077 & (unsigned long)255;
             _6260 = MAKE_UINT(tu);
        }
    }
    else {
        _6260 = binary_op(AND_BITS, _x_11077, 255);
    }
    if (IS_ATOM_INT(_x_11077)) {
        if (256 > 0 && _x_11077 >= 0) {
            _6261 = _x_11077 / 256;
        }
        else {
            temp_dbl = floor((double)_x_11077 / (double)256);
            if (_x_11077 != MININT)
            _6261 = (long)temp_dbl;
            else
            _6261 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_11077, 256);
        _6261 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _6260;
    *((int *)(_2+12)) = _6261;
    _6262 = MAKE_SEQ(_1);
    _6261 = NOVALUE;
    _6260 = NOVALUE;
    DeRef(_x_11077);
    DeRefi(_x4_11078);
    DeRef(_s_11079);
    DeRef(_6251);
    _6251 = NOVALUE;
    DeRef(_6255);
    _6255 = NOVALUE;
    DeRef(_6256);
    _6256 = NOVALUE;
    return _6262;
    goto L3; // [94] 328
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_11077)) {
        _6263 = (_x_11077 >= _2MIN3B_10996);
    }
    else {
        _6263 = binary_op(GREATEREQ, _x_11077, _2MIN3B_10996);
    }
    if (IS_ATOM_INT(_6263)) {
        if (_6263 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_6263)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_11077)) {
        _6265 = (_x_11077 <= 8388607);
    }
    else {
        _6265 = binary_op(LESSEQ, _x_11077, 8388607);
    }
    if (_6265 == 0) {
        DeRef(_6265);
        _6265 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_6265) && DBL_PTR(_6265)->dbl == 0.0){
            DeRef(_6265);
            _6265 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_6265);
        _6265 = NOVALUE;
    }
    DeRef(_6265);
    _6265 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_11077;
    if (IS_ATOM_INT(_x_11077)) {
        _x_11077 = _x_11077 - _2MIN3B_10996;
        if ((long)((unsigned long)_x_11077 +(unsigned long) HIGH_BITS) >= 0){
            _x_11077 = NewDouble((double)_x_11077);
        }
    }
    else {
        _x_11077 = binary_op(MINUS, _x_11077, _2MIN3B_10996);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_11077)) {
        {unsigned long tu;
             tu = (unsigned long)_x_11077 & (unsigned long)255;
             _6267 = MAKE_UINT(tu);
        }
    }
    else {
        _6267 = binary_op(AND_BITS, _x_11077, 255);
    }
    if (IS_ATOM_INT(_x_11077)) {
        if (256 > 0 && _x_11077 >= 0) {
            _6268 = _x_11077 / 256;
        }
        else {
            temp_dbl = floor((double)_x_11077 / (double)256);
            if (_x_11077 != MININT)
            _6268 = (long)temp_dbl;
            else
            _6268 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_11077, 256);
        _6268 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_6268)) {
        {unsigned long tu;
             tu = (unsigned long)_6268 & (unsigned long)255;
             _6269 = MAKE_UINT(tu);
        }
    }
    else {
        _6269 = binary_op(AND_BITS, _6268, 255);
    }
    DeRef(_6268);
    _6268 = NOVALUE;
    if (IS_ATOM_INT(_x_11077)) {
        if (65536 > 0 && _x_11077 >= 0) {
            _6270 = _x_11077 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_11077 / (double)65536);
            if (_x_11077 != MININT)
            _6270 = (long)temp_dbl;
            else
            _6270 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_11077, 65536);
        _6270 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _6267;
    *((int *)(_2+12)) = _6269;
    *((int *)(_2+16)) = _6270;
    _6271 = MAKE_SEQ(_1);
    _6270 = NOVALUE;
    _6269 = NOVALUE;
    _6267 = NOVALUE;
    DeRef(_x_11077);
    DeRefi(_x4_11078);
    DeRef(_s_11079);
    DeRef(_6251);
    _6251 = NOVALUE;
    DeRef(_6255);
    _6255 = NOVALUE;
    DeRef(_6256);
    _6256 = NOVALUE;
    DeRef(_6262);
    _6262 = NOVALUE;
    DeRef(_6263);
    _6263 = NOVALUE;
    return _6271;
    goto L3; // [156] 328
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_11077) && IS_ATOM_INT(_2MIN4B_11002)) {
        _6272 = _x_11077 - _2MIN4B_11002;
        if ((long)((unsigned long)_6272 +(unsigned long) HIGH_BITS) >= 0){
            _6272 = NewDouble((double)_6272);
        }
    }
    else {
        _6272 = binary_op(MINUS, _x_11077, _2MIN4B_11002);
    }
    _6273 = _4int_to_bytes(_6272);
    _6272 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_6273)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_6273)) {
        Prepend(&_6274, _6273, 251);
    }
    else {
        Concat((object_ptr)&_6274, 251, _6273);
    }
    DeRef(_6273);
    _6273 = NOVALUE;
    DeRef(_x_11077);
    DeRefi(_x4_11078);
    DeRef(_s_11079);
    DeRef(_6251);
    _6251 = NOVALUE;
    DeRef(_6255);
    _6255 = NOVALUE;
    DeRef(_6256);
    _6256 = NOVALUE;
    DeRef(_6262);
    _6262 = NOVALUE;
    DeRef(_6263);
    _6263 = NOVALUE;
    DeRef(_6271);
    _6271 = NOVALUE;
    return _6274;
    goto L3; // [180] 328
L1: 

    /** 	elsif atom(x) then*/
    _6275 = IS_ATOM(_x_11077);
    if (_6275 == 0)
    {
        _6275 = NOVALUE;
        goto L6; // [188] 249
    }
    else{
        _6275 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/

    /** 	return machine_func(M_A_TO_F32, a)*/
    DeRefi(_x4_11078);
    _x4_11078 = machine(48, _x_11077);

    /** 		if x = convert:float32_to_atom(x4) then*/

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    DeRef(_float32_to_atom_inlined_float32_to_atom_at_203_11116);
    _float32_to_atom_inlined_float32_to_atom_at_203_11116 = machine(49, _x4_11078);
    if (binary_op_a(NOTEQ, _x_11077, _float32_to_atom_inlined_float32_to_atom_at_203_11116)){
        goto L7; // [211] 228
    }

    /** 			return F4B & x4*/
    Prepend(&_6277, _x4_11078, 252);
    DeRef(_x_11077);
    DeRefDSi(_x4_11078);
    DeRef(_s_11079);
    DeRef(_6251);
    _6251 = NOVALUE;
    DeRef(_6255);
    _6255 = NOVALUE;
    DeRef(_6256);
    _6256 = NOVALUE;
    DeRef(_6262);
    _6262 = NOVALUE;
    DeRef(_6263);
    _6263 = NOVALUE;
    DeRef(_6271);
    _6271 = NOVALUE;
    DeRef(_6274);
    _6274 = NOVALUE;
    return _6277;
    goto L3; // [225] 328
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/

    /** 	return machine_func(M_A_TO_F64, a)*/
    DeRefi(_atom_to_float64_inlined_atom_to_float64_at_229_11121);
    _atom_to_float64_inlined_atom_to_float64_at_229_11121 = machine(46, _x_11077);
    Prepend(&_6278, _atom_to_float64_inlined_atom_to_float64_at_229_11121, 253);
    DeRef(_x_11077);
    DeRefi(_x4_11078);
    DeRef(_s_11079);
    DeRef(_6251);
    _6251 = NOVALUE;
    DeRef(_6255);
    _6255 = NOVALUE;
    DeRef(_6256);
    _6256 = NOVALUE;
    DeRef(_6262);
    _6262 = NOVALUE;
    DeRef(_6263);
    _6263 = NOVALUE;
    DeRef(_6271);
    _6271 = NOVALUE;
    DeRef(_6274);
    _6274 = NOVALUE;
    DeRef(_6277);
    _6277 = NOVALUE;
    return _6278;
    goto L3; // [246] 328
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_11077)){
            _6279 = SEQ_PTR(_x_11077)->length;
    }
    else {
        _6279 = 1;
    }
    if (_6279 > 255)
    goto L8; // [254] 270

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_11077)){
            _6281 = SEQ_PTR(_x_11077)->length;
    }
    else {
        _6281 = 1;
    }
    DeRef(_s_11079);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _6281;
    _s_11079 = MAKE_SEQ(_1);
    _6281 = NOVALUE;
    goto L9; // [267] 284
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_11077)){
            _6283 = SEQ_PTR(_x_11077)->length;
    }
    else {
        _6283 = 1;
    }
    _6284 = _4int_to_bytes(_6283);
    _6283 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_6284)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_6284)) {
        Prepend(&_s_11079, _6284, 255);
    }
    else {
        Concat((object_ptr)&_s_11079, 255, _6284);
    }
    DeRef(_6284);
    _6284 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_11077)){
            _6286 = SEQ_PTR(_x_11077)->length;
    }
    else {
        _6286 = 1;
    }
    {
        int _i_11134;
        _i_11134 = 1;
LA: 
        if (_i_11134 > _6286){
            goto LB; // [289] 319
        }

        /** 			s &= compress(x[i])*/
        _2 = (int)SEQ_PTR(_x_11077);
        _6287 = (int)*(((s1_ptr)_2)->base + _i_11134);
        Ref(_6287);
        _6288 = _2compress(_6287);
        _6287 = NOVALUE;
        if (IS_SEQUENCE(_s_11079) && IS_ATOM(_6288)) {
            Ref(_6288);
            Append(&_s_11079, _s_11079, _6288);
        }
        else if (IS_ATOM(_s_11079) && IS_SEQUENCE(_6288)) {
        }
        else {
            Concat((object_ptr)&_s_11079, _s_11079, _6288);
        }
        DeRef(_6288);
        _6288 = NOVALUE;

        /** 		end for*/
        _i_11134 = _i_11134 + 1;
        goto LA; // [314] 296
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_11077);
    DeRefi(_x4_11078);
    DeRef(_6251);
    _6251 = NOVALUE;
    DeRef(_6255);
    _6255 = NOVALUE;
    DeRef(_6256);
    _6256 = NOVALUE;
    DeRef(_6262);
    _6262 = NOVALUE;
    DeRef(_6263);
    _6263 = NOVALUE;
    DeRef(_6271);
    _6271 = NOVALUE;
    DeRef(_6274);
    _6274 = NOVALUE;
    DeRef(_6277);
    _6277 = NOVALUE;
    DeRef(_6278);
    _6278 = NOVALUE;
    return _s_11079;
L3: 
    ;
}


void _2safe_seek(int _pos_11153, int _msg_11154)
{
    int _eofpos_11155 = NOVALUE;
    int _seek_1__tmp_at34_11163 = NOVALUE;
    int _seek_inlined_seek_at_34_11162 = NOVALUE;
    int _where_inlined_where_at_51_11165 = NOVALUE;
    int _seek_1__tmp_at88_11173 = NOVALUE;
    int _seek_inlined_seek_at_88_11172 = NOVALUE;
    int _where_inlined_where_at_135_11181 = NOVALUE;
    int _6305 = NOVALUE;
    int _6301 = NOVALUE;
    int _6298 = NOVALUE;
    int _6295 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_2current_db_10880 != -1)
    goto L1; // [7] 31

    /** 		fatal(NO_DATABASE, "no current database defined", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_11153);
    *((int *)(_2+4)) = _pos_11153;
    _6295 = MAKE_SEQ(_1);
    RefDS(_6293);
    RefDS(_6294);
    _2fatal(901, _6293, _6294, _6295);
    _6295 = NOVALUE;

    /** 		return*/
    DeRef(_pos_11153);
    DeRef(_eofpos_11155);
    return;
L1: 

    /** 	io:seek(current_db, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at34_11163);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at34_11163 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_34_11162 = machine(19, _seek_1__tmp_at34_11163);
    DeRefi(_seek_1__tmp_at34_11163);
    _seek_1__tmp_at34_11163 = NOVALUE;

    /** 	eofpos = io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_eofpos_11155);
    _eofpos_11155 = machine(20, _2current_db_10880);

    /** 	if pos > eofpos then*/
    if (binary_op_a(LESSEQ, _pos_11153, _eofpos_11155)){
        goto L2; // [61] 85
    }

    /** 		fatal(BAD_SEEK, "io:seeking past EOF", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_11153);
    *((int *)(_2+4)) = _pos_11153;
    _6298 = MAKE_SEQ(_1);
    RefDS(_6297);
    RefDS(_6294);
    _2fatal(902, _6297, _6294, _6298);
    _6298 = NOVALUE;

    /** 		return*/
    DeRef(_pos_11153);
    DeRef(_eofpos_11155);
    return;
L2: 

    /** 	if io:seek(current_db, pos) != 0 then*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_11153);
    DeRef(_seek_1__tmp_at88_11173);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_11153;
    _seek_1__tmp_at88_11173 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_88_11172 = machine(19, _seek_1__tmp_at88_11173);
    DeRef(_seek_1__tmp_at88_11173);
    _seek_1__tmp_at88_11173 = NOVALUE;
    if (_seek_inlined_seek_at_88_11172 == 0)
    goto L3; // [102] 126

    /** 		fatal(BAD_SEEK, "io:seek to position failed", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_11153);
    *((int *)(_2+4)) = _pos_11153;
    _6301 = MAKE_SEQ(_1);
    RefDS(_6300);
    RefDS(_6294);
    _2fatal(902, _6300, _6294, _6301);
    _6301 = NOVALUE;

    /** 		return*/
    DeRef(_pos_11153);
    DeRef(_eofpos_11155);
    return;
L3: 

    /** 	if pos != -1 then*/
    if (binary_op_a(EQUALS, _pos_11153, -1)){
        goto L4; // [128] 168
    }

    /** 		if io:where(current_db) != pos then*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_135_11181);
    _where_inlined_where_at_135_11181 = machine(20, _2current_db_10880);
    if (binary_op_a(EQUALS, _where_inlined_where_at_135_11181, _pos_11153)){
        goto L5; // [143] 167
    }

    /** 			fatal(BAD_SEEK, "io:seek not in position", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_11153);
    *((int *)(_2+4)) = _pos_11153;
    _6305 = MAKE_SEQ(_1);
    RefDS(_6304);
    RefDS(_6294);
    _2fatal(902, _6304, _6294, _6305);
    _6305 = NOVALUE;

    /** 			return*/
    DeRef(_pos_11153);
    DeRef(_eofpos_11155);
    return;
L5: 
L4: 

    /** end procedure*/
    DeRef(_pos_11153);
    DeRef(_eofpos_11155);
    return;
    ;
}


int _2db_get_errors(int _clearing_11187)
{
    int _lErrors_11188 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_clearing_11187)) {
        _1 = (long)(DBL_PTR(_clearing_11187)->dbl);
        if (UNIQUE(DBL_PTR(_clearing_11187)) && (DBL_PTR(_clearing_11187)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_clearing_11187);
        _clearing_11187 = _1;
    }

    /** 	lErrors = vLastErrors*/
    RefDS(_2vLastErrors_10904);
    DeRef(_lErrors_11188);
    _lErrors_11188 = _2vLastErrors_10904;

    /** 	if clearing then*/
    if (_clearing_11187 == 0)
    {
        goto L1; // [16] 27
    }
    else{
    }

    /** 		vLastErrors = {}*/
    RefDS(_5);
    DeRefDS(_2vLastErrors_10904);
    _2vLastErrors_10904 = _5;
L1: 

    /** 	return lErrors*/
    return _lErrors_11188;
    ;
}


void _2db_dump(int _file_id_11192, int _low_level_too_11193)
{
    int _magic_11194 = NOVALUE;
    int _minor_11195 = NOVALUE;
    int _major_11196 = NOVALUE;
    int _fn_11197 = NOVALUE;
    int _tables_11198 = NOVALUE;
    int _ntables_11199 = NOVALUE;
    int _tname_11200 = NOVALUE;
    int _trecords_11201 = NOVALUE;
    int _t_header_11202 = NOVALUE;
    int _tnrecs_11203 = NOVALUE;
    int _key_ptr_11204 = NOVALUE;
    int _data_ptr_11205 = NOVALUE;
    int _size_11206 = NOVALUE;
    int _addr_11207 = NOVALUE;
    int _tindex_11208 = NOVALUE;
    int _fbp_11209 = NOVALUE;
    int _key_11210 = NOVALUE;
    int _data_11211 = NOVALUE;
    int _c_11212 = NOVALUE;
    int _n_11213 = NOVALUE;
    int _tblocks_11214 = NOVALUE;
    int _a_11215 = NOVALUE;
    int _ll_line_11216 = NOVALUE;
    int _hi_11217 = NOVALUE;
    int _ci_11218 = NOVALUE;
    int _now_1__tmp_at87_11233 = NOVALUE;
    int _now_inlined_now_at_87_11232 = NOVALUE;
    int _seek_1__tmp_at115_11238 = NOVALUE;
    int _seek_inlined_seek_at_115_11237 = NOVALUE;
    int _get1_inlined_get1_at_145_11243 = NOVALUE;
    int _get1_inlined_get1_at_173_11249 = NOVALUE;
    int _get1_inlined_get1_at_187_11251 = NOVALUE;
    int _seek_1__tmp_at218_11257 = NOVALUE;
    int _seek_inlined_seek_at_218_11256 = NOVALUE;
    int _seek_1__tmp_at304_11273 = NOVALUE;
    int _seek_inlined_seek_at_304_11272 = NOVALUE;
    int _seek_1__tmp_at592_11314 = NOVALUE;
    int _seek_inlined_seek_at_592_11313 = NOVALUE;
    int _get1_inlined_get1_at_607_11316 = NOVALUE;
    int _get1_inlined_get1_at_646_11322 = NOVALUE;
    int _get1_inlined_get1_at_660_11324 = NOVALUE;
    int _seek_1__tmp_at691_11330 = NOVALUE;
    int _seek_inlined_seek_at_691_11329 = NOVALUE;
    int _where_inlined_where_at_713_11333 = NOVALUE;
    int _seek_1__tmp_at780_11347 = NOVALUE;
    int _seek_inlined_seek_at_780_11346 = NOVALUE;
    int _seek_1__tmp_at871_11368 = NOVALUE;
    int _seek_inlined_seek_at_871_11367 = NOVALUE;
    int _pos_inlined_seek_at_868_11366 = NOVALUE;
    int _seek_1__tmp_at967_11390 = NOVALUE;
    int _seek_inlined_seek_at_967_11389 = NOVALUE;
    int _pos_inlined_seek_at_964_11388 = NOVALUE;
    int _seek_1__tmp_at1003_11397 = NOVALUE;
    int _seek_inlined_seek_at_1003_11396 = NOVALUE;
    int _seek_1__tmp_at1062_11407 = NOVALUE;
    int _seek_inlined_seek_at_1062_11406 = NOVALUE;
    int _seek_1__tmp_at1133_11416 = NOVALUE;
    int _seek_inlined_seek_at_1133_11415 = NOVALUE;
    int _seek_1__tmp_at1167_11421 = NOVALUE;
    int _seek_inlined_seek_at_1167_11420 = NOVALUE;
    int _seek_1__tmp_at1230_11431 = NOVALUE;
    int _seek_inlined_seek_at_1230_11430 = NOVALUE;
    int _6427 = NOVALUE;
    int _6425 = NOVALUE;
    int _6421 = NOVALUE;
    int _6409 = NOVALUE;
    int _6403 = NOVALUE;
    int _6400 = NOVALUE;
    int _6399 = NOVALUE;
    int _6398 = NOVALUE;
    int _6397 = NOVALUE;
    int _6396 = NOVALUE;
    int _6395 = NOVALUE;
    int _6394 = NOVALUE;
    int _6392 = NOVALUE;
    int _6391 = NOVALUE;
    int _6386 = NOVALUE;
    int _6385 = NOVALUE;
    int _6384 = NOVALUE;
    int _6383 = NOVALUE;
    int _6382 = NOVALUE;
    int _6381 = NOVALUE;
    int _6380 = NOVALUE;
    int _6378 = NOVALUE;
    int _6376 = NOVALUE;
    int _6375 = NOVALUE;
    int _6367 = NOVALUE;
    int _6363 = NOVALUE;
    int _6361 = NOVALUE;
    int _6360 = NOVALUE;
    int _6359 = NOVALUE;
    int _6355 = NOVALUE;
    int _6354 = NOVALUE;
    int _6353 = NOVALUE;
    int _6350 = NOVALUE;
    int _6346 = NOVALUE;
    int _6344 = NOVALUE;
    int _6343 = NOVALUE;
    int _6339 = NOVALUE;
    int _6335 = NOVALUE;
    int _6332 = NOVALUE;
    int _6331 = NOVALUE;
    int _6327 = NOVALUE;
    int _6326 = NOVALUE;
    int _6325 = NOVALUE;
    int _6321 = NOVALUE;
    int _6316 = NOVALUE;
    int _6315 = NOVALUE;
    int _6314 = NOVALUE;
    int _6312 = NOVALUE;
    int _6306 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_low_level_too_11193)) {
        _1 = (long)(DBL_PTR(_low_level_too_11193)->dbl);
        if (UNIQUE(DBL_PTR(_low_level_too_11193)) && (DBL_PTR(_low_level_too_11193)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_low_level_too_11193);
        _low_level_too_11193 = _1;
    }

    /** 	if sequence(file_id) then*/
    _6306 = IS_SEQUENCE(_file_id_11192);
    if (_6306 == 0)
    {
        _6306 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _6306 = NOVALUE;
    }

    /** 		fn = open(file_id, "w")*/
    _fn_11197 = EOpen(_file_id_11192, _889, 0);
    goto L2; // [22] 58
L1: 

    /** 	elsif file_id > 0 then*/
    if (binary_op_a(LESSEQ, _file_id_11192, 0)){
        goto L3; // [27] 48
    }

    /** 		fn = file_id*/
    Ref(_file_id_11192);
    _fn_11197 = _file_id_11192;
    if (!IS_ATOM_INT(_fn_11197)) {
        _1 = (long)(DBL_PTR(_fn_11197)->dbl);
        if (UNIQUE(DBL_PTR(_fn_11197)) && (DBL_PTR(_fn_11197)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_11197);
        _fn_11197 = _1;
    }

    /** 		puts(fn, '\n')*/
    EPuts(_fn_11197, 10); // DJP 
    goto L2; // [45] 58
L3: 

    /** 		fn = file_id*/
    Ref(_file_id_11192);
    _fn_11197 = _file_id_11192;
    if (!IS_ATOM_INT(_fn_11197)) {
        _1 = (long)(DBL_PTR(_fn_11197)->dbl);
        if (UNIQUE(DBL_PTR(_fn_11197)) && (DBL_PTR(_fn_11197)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_11197);
        _fn_11197 = _1;
    }
L2: 

    /** 	if fn <= 0 then*/
    if (_fn_11197 > 0)
    goto L4; // [62] 86

    /** 		fatal( BAD_FILE, "bad file", "db_dump", {file_id, low_level_too})*/
    Ref(_file_id_11192);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_id_11192;
    ((int *)_2)[2] = _low_level_too_11193;
    _6312 = MAKE_SEQ(_1);
    RefDS(_6310);
    RefDS(_6311);
    _2fatal(908, _6310, _6311, _6312);
    _6312 = NOVALUE;

    /** 		return*/
    DeRef(_file_id_11192);
    DeRef(_tables_11198);
    DeRef(_ntables_11199);
    DeRef(_tname_11200);
    DeRef(_trecords_11201);
    DeRef(_t_header_11202);
    DeRef(_tnrecs_11203);
    DeRef(_key_ptr_11204);
    DeRef(_data_ptr_11205);
    DeRef(_size_11206);
    DeRef(_addr_11207);
    DeRef(_tindex_11208);
    DeRef(_fbp_11209);
    DeRef(_key_11210);
    DeRef(_data_11211);
    DeRef(_a_11215);
    DeRef(_ll_line_11216);
    return;
L4: 

    /** 	printf(fn, "Database dump as at %s\n", {datetime:format( datetime:now(), "%Y-%m-%d %H:%M:%S")})*/

    /** 	return from_date(date())*/
    DeRefi(_now_1__tmp_at87_11233);
    _now_1__tmp_at87_11233 = Date();
    RefDS(_now_1__tmp_at87_11233);
    _0 = _now_inlined_now_at_87_11232;
    _now_inlined_now_at_87_11232 = _9from_date(_now_1__tmp_at87_11233);
    DeRef(_0);
    DeRefi(_now_1__tmp_at87_11233);
    _now_1__tmp_at87_11233 = NOVALUE;
    Ref(_now_inlined_now_at_87_11232);
    RefDS(_1637);
    _6314 = _9format(_now_inlined_now_at_87_11232, _1637);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _6314;
    _6315 = MAKE_SEQ(_1);
    _6314 = NOVALUE;
    EPrintf(_fn_11197, _6313, _6315);
    DeRefDS(_6315);
    _6315 = NOVALUE;

    /** 	io:seek(current_db, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at115_11238);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at115_11238 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_115_11237 = machine(19, _seek_1__tmp_at115_11238);
    DeRefi(_seek_1__tmp_at115_11238);
    _seek_1__tmp_at115_11238 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return end if*/
    if (IS_SEQUENCE(_2vLastErrors_10904)){
            _6316 = SEQ_PTR(_2vLastErrors_10904)->length;
    }
    else {
        _6316 = 1;
    }
    if (_6316 <= 0)
    goto L5; // [136] 144
    DeRef(_file_id_11192);
    DeRef(_tables_11198);
    DeRef(_ntables_11199);
    DeRef(_tname_11200);
    DeRef(_trecords_11201);
    DeRef(_t_header_11202);
    DeRef(_tnrecs_11203);
    DeRef(_key_ptr_11204);
    DeRef(_data_ptr_11205);
    DeRef(_size_11206);
    DeRef(_addr_11207);
    DeRef(_tindex_11208);
    DeRef(_fbp_11209);
    DeRef(_key_11210);
    DeRef(_data_11211);
    DeRef(_a_11215);
    DeRef(_ll_line_11216);
    return;
L5: 

    /** 	magic = get1()*/

    /** 	return getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_11194 = getKBchar();
        }
        else
        _magic_11194 = getc(last_r_file_ptr);
    }
    else
    _magic_11194 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_magic_11194)) {
        _1 = (long)(DBL_PTR(_magic_11194)->dbl);
        if (UNIQUE(DBL_PTR(_magic_11194)) && (DBL_PTR(_magic_11194)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_magic_11194);
        _magic_11194 = _1;
    }

    /** 	if magic != DB_MAGIC then*/
    if (_magic_11194 == 77)
    goto L6; // [160] 172

    /** 		puts(fn, "This is not a Euphoria Database file.\n")*/
    EPuts(_fn_11197, _6319); // DJP 
    goto L7; // [169] 283
L6: 

    /** 		major = get1()*/

    /** 	return getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _major_11196 = getKBchar();
        }
        else
        _major_11196 = getc(last_r_file_ptr);
    }
    else
    _major_11196 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_major_11196)) {
        _1 = (long)(DBL_PTR(_major_11196)->dbl);
        if (UNIQUE(DBL_PTR(_major_11196)) && (DBL_PTR(_major_11196)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_major_11196);
        _major_11196 = _1;
    }

    /** 		minor = get1()*/

    /** 	return getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _minor_11195 = getKBchar();
        }
        else
        _minor_11195 = getc(last_r_file_ptr);
    }
    else
    _minor_11195 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_minor_11195)) {
        _1 = (long)(DBL_PTR(_minor_11195)->dbl);
        if (UNIQUE(DBL_PTR(_minor_11195)) && (DBL_PTR(_minor_11195)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minor_11195);
        _minor_11195 = _1;
    }

    /** 		printf(fn, "Euphoria Database System Version %d.%d\n\n", {major, minor})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _major_11196;
    ((int *)_2)[2] = _minor_11195;
    _6321 = MAKE_SEQ(_1);
    EPrintf(_fn_11197, _6320, _6321);
    DeRefDS(_6321);
    _6321 = NOVALUE;

    /** 		tables = get4()*/
    _0 = _tables_11198;
    _tables_11198 = _2get4();
    DeRef(_0);

    /** 		io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_11198);
    DeRef(_seek_1__tmp_at218_11257);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _tables_11198;
    _seek_1__tmp_at218_11257 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_218_11256 = machine(19, _seek_1__tmp_at218_11257);
    DeRef(_seek_1__tmp_at218_11257);
    _seek_1__tmp_at218_11257 = NOVALUE;

    /** 		ntables = get4()*/
    _0 = _ntables_11199;
    _ntables_11199 = _2get4();
    DeRef(_0);

    /** 		printf(fn, "The \"%s\" database has %d table",*/
    _6325 = find_from(_2current_db_10880, _2db_file_nums_10884, 1);
    _2 = (int)SEQ_PTR(_2db_names_10883);
    _6326 = (int)*(((s1_ptr)_2)->base + _6325);
    Ref(_ntables_11199);
    Ref(_6326);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6326;
    ((int *)_2)[2] = _ntables_11199;
    _6327 = MAKE_SEQ(_1);
    _6326 = NOVALUE;
    EPrintf(_fn_11197, _6324, _6327);
    DeRefDS(_6327);
    _6327 = NOVALUE;

    /** 		if ntables = 1 then*/
    if (binary_op_a(NOTEQ, _ntables_11199, 1)){
        goto L8; // [264] 276
    }

    /** 			puts(fn, "\n")*/
    EPuts(_fn_11197, _921); // DJP 
    goto L9; // [273] 282
L8: 

    /** 			puts(fn, "s\n")*/
    EPuts(_fn_11197, _6329); // DJP 
L9: 
L7: 

    /** 	if low_level_too then*/
    if (_low_level_too_11193 == 0)
    {
        goto LA; // [285] 589
    }
    else{
    }

    /** 		puts(fn, "            Disk Dump\nDiskAddr " & repeat('-', 58))*/
    _6331 = Repeat(45, 58);
    Concat((object_ptr)&_6332, _6330, _6331);
    DeRefDS(_6331);
    _6331 = NOVALUE;
    EPuts(_fn_11197, _6332); // DJP 
    DeRefDS(_6332);
    _6332 = NOVALUE;

    /** 		io:seek(current_db, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at304_11273);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at304_11273 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_304_11272 = machine(19, _seek_1__tmp_at304_11273);
    DeRefi(_seek_1__tmp_at304_11273);
    _seek_1__tmp_at304_11273 = NOVALUE;

    /** 		a = 0*/
    DeRef(_a_11215);
    _a_11215 = 0;

    /** 		while c >= 0 with entry do*/
    goto LB; // [325] 549
LC: 
    if (_c_11212 < 0)
    goto LD; // [330] 563

    /** 			if c = -1 then*/
    if (_c_11212 != -1)
    goto LE; // [336] 345

    /** 				exit*/
    goto LD; // [342] 563
LE: 

    /** 			if remainder(a, 16) = 0 then*/
    if (IS_ATOM_INT(_a_11215)) {
        _6335 = (_a_11215 % 16);
    }
    else {
        temp_d.dbl = (double)16;
        _6335 = Dremainder(DBL_PTR(_a_11215), &temp_d);
    }
    if (binary_op_a(NOTEQ, _6335, 0)){
        DeRef(_6335);
        _6335 = NOVALUE;
        goto LF; // [351] 432
    }
    DeRef(_6335);
    _6335 = NOVALUE;

    /** 				if a > 0 then*/
    if (binary_op_a(LESSEQ, _a_11215, 0)){
        goto L10; // [357] 376
    }

    /** 					printf(fn, "%s\n", {ll_line})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ll_line_11216);
    *((int *)(_2+4)) = _ll_line_11216;
    _6339 = MAKE_SEQ(_1);
    EPrintf(_fn_11197, _6338, _6339);
    DeRefDS(_6339);
    _6339 = NOVALUE;
    goto L11; // [373] 382
L10: 

    /** 					puts(fn, '\n')*/
    EPuts(_fn_11197, 10); // DJP 
L11: 

    /** 				ll_line = repeat(' ', 67)*/
    DeRef(_ll_line_11216);
    _ll_line_11216 = Repeat(32, 67);

    /** 				ll_line[9] = ':'*/
    _2 = (int)SEQ_PTR(_ll_line_11216);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_11216 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    *(int *)_2 = 58;

    /** 				ll_line[48] = '|'*/
    _2 = (int)SEQ_PTR(_ll_line_11216);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_11216 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 48);
    *(int *)_2 = 124;

    /** 				ll_line[67] = '|'*/
    _2 = (int)SEQ_PTR(_ll_line_11216);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_11216 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 67);
    *(int *)_2 = 124;

    /** 				hi = 11*/
    _hi_11217 = 11;

    /** 				ci = 50*/
    _ci_11218 = 50;

    /** 				ll_line[1..8] = sprintf("%08x", a)*/
    _6343 = EPrintf(-9999999, _6342, _a_11215);
    assign_slice_seq = (s1_ptr *)&_ll_line_11216;
    AssignSlice(1, 8, _6343);
    DeRefDS(_6343);
    _6343 = NOVALUE;
LF: 

    /** 			ll_line[hi .. hi + 1] = sprintf("%02x", c)*/
    _6344 = _hi_11217 + 1;
    if (_6344 > MAXINT){
        _6344 = NewDouble((double)_6344);
    }
    _6346 = EPrintf(-9999999, _6345, _c_11212);
    assign_slice_seq = (s1_ptr *)&_ll_line_11216;
    AssignSlice(_hi_11217, _6344, _6346);
    DeRef(_6344);
    _6344 = NOVALUE;
    DeRefDS(_6346);
    _6346 = NOVALUE;

    /** 			hi += 2*/
    _hi_11217 = _hi_11217 + 2;

    /** 			if eu:find(hi, {19, 28, 38}) then*/
    _6350 = find_from(_hi_11217, _6349, 1);
    if (_6350 == 0)
    {
        _6350 = NOVALUE;
        goto L12; // [466] 492
    }
    else{
        _6350 = NOVALUE;
    }

    /** 				hi += 1*/
    _hi_11217 = _hi_11217 + 1;

    /** 				if hi = 29 then*/
    if (_hi_11217 != 29)
    goto L13; // [479] 491

    /** 					hi = 30*/
    _hi_11217 = 30;
L13: 
L12: 

    /** 			if c > ' ' and c < '~' then*/
    _6353 = (_c_11212 > 32);
    if (_6353 == 0) {
        goto L14; // [498] 521
    }
    _6355 = (_c_11212 < 126);
    if (_6355 == 0)
    {
        DeRef(_6355);
        _6355 = NOVALUE;
        goto L14; // [507] 521
    }
    else{
        DeRef(_6355);
        _6355 = NOVALUE;
    }

    /** 				ll_line[ci] = c*/
    _2 = (int)SEQ_PTR(_ll_line_11216);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_11216 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ci_11218);
    _1 = *(int *)_2;
    *(int *)_2 = _c_11212;
    DeRef(_1);
    goto L15; // [518] 530
L14: 

    /** 				ll_line[ci] = '.'*/
    _2 = (int)SEQ_PTR(_ll_line_11216);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_11216 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ci_11218);
    _1 = *(int *)_2;
    *(int *)_2 = 46;
    DeRef(_1);
L15: 

    /** 			ci += 1*/
    _ci_11218 = _ci_11218 + 1;

    /** 			a += 1*/
    _0 = _a_11215;
    if (IS_ATOM_INT(_a_11215)) {
        _a_11215 = _a_11215 + 1;
        if (_a_11215 > MAXINT){
            _a_11215 = NewDouble((double)_a_11215);
        }
    }
    else
    _a_11215 = binary_op(PLUS, 1, _a_11215);
    DeRef(_0);

    /** 		  entry*/
LB: 

    /** 			c = getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_11212 = getKBchar();
        }
        else
        _c_11212 = getc(last_r_file_ptr);
    }
    else
    _c_11212 = getc(last_r_file_ptr);

    /** 		end while*/
    goto LC; // [560] 328
LD: 

    /** 		printf(fn, "%s\n", {ll_line})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ll_line_11216);
    *((int *)(_2+4)) = _ll_line_11216;
    _6359 = MAKE_SEQ(_1);
    EPrintf(_fn_11197, _6338, _6359);
    DeRefDS(_6359);
    _6359 = NOVALUE;

    /** 		puts(fn, repeat('-', 67) & "\n\n")*/
    _6360 = Repeat(45, 67);
    Concat((object_ptr)&_6361, _6360, _93);
    DeRefDS(_6360);
    _6360 = NOVALUE;
    DeRef(_6360);
    _6360 = NOVALUE;
    EPuts(_fn_11197, _6361); // DJP 
    DeRefDS(_6361);
    _6361 = NOVALUE;
LA: 

    /** 	io:seek(current_db, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at592_11314);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at592_11314 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_592_11313 = machine(19, _seek_1__tmp_at592_11314);
    DeRefi(_seek_1__tmp_at592_11314);
    _seek_1__tmp_at592_11314 = NOVALUE;

    /** 	magic = get1()*/

    /** 	return getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_11194 = getKBchar();
        }
        else
        _magic_11194 = getc(last_r_file_ptr);
    }
    else
    _magic_11194 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_magic_11194)) {
        _1 = (long)(DBL_PTR(_magic_11194)->dbl);
        if (UNIQUE(DBL_PTR(_magic_11194)) && (DBL_PTR(_magic_11194)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_magic_11194);
        _magic_11194 = _1;
    }

    /** 	if magic != DB_MAGIC then*/
    if (_magic_11194 == 77)
    goto L16; // [622] 645

    /** 		if sequence(file_id) then*/
    _6363 = IS_SEQUENCE(_file_id_11192);
    if (_6363 == 0)
    {
        _6363 = NOVALUE;
        goto L17; // [631] 639
    }
    else{
        _6363 = NOVALUE;
    }

    /** 			close(fn)*/
    EClose(_fn_11197);
L17: 

    /** 		return*/
    DeRef(_file_id_11192);
    DeRef(_tables_11198);
    DeRef(_ntables_11199);
    DeRef(_tname_11200);
    DeRef(_trecords_11201);
    DeRef(_t_header_11202);
    DeRef(_tnrecs_11203);
    DeRef(_key_ptr_11204);
    DeRef(_data_ptr_11205);
    DeRef(_size_11206);
    DeRef(_addr_11207);
    DeRef(_tindex_11208);
    DeRef(_fbp_11209);
    DeRef(_key_11210);
    DeRef(_data_11211);
    DeRef(_a_11215);
    DeRef(_ll_line_11216);
    DeRef(_6353);
    _6353 = NOVALUE;
    return;
L16: 

    /** 	major = get1()*/

    /** 	return getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _major_11196 = getKBchar();
        }
        else
        _major_11196 = getc(last_r_file_ptr);
    }
    else
    _major_11196 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_major_11196)) {
        _1 = (long)(DBL_PTR(_major_11196)->dbl);
        if (UNIQUE(DBL_PTR(_major_11196)) && (DBL_PTR(_major_11196)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_major_11196);
        _major_11196 = _1;
    }

    /** 	minor = get1()*/

    /** 	return getc(current_db)*/
    if (_2current_db_10880 != last_r_file_no) {
        last_r_file_ptr = which_file(_2current_db_10880, EF_READ);
        last_r_file_no = _2current_db_10880;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _minor_11195 = getKBchar();
        }
        else
        _minor_11195 = getc(last_r_file_ptr);
    }
    else
    _minor_11195 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_minor_11195)) {
        _1 = (long)(DBL_PTR(_minor_11195)->dbl);
        if (UNIQUE(DBL_PTR(_minor_11195)) && (DBL_PTR(_minor_11195)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minor_11195);
        _minor_11195 = _1;
    }

    /** 	tables = get4()*/
    _0 = _tables_11198;
    _tables_11198 = _2get4();
    DeRef(_0);

    /** 	if low_level_too then printf(fn, "[tables:#%08x]\n", tables) end if*/
    if (_low_level_too_11193 == 0)
    {
        goto L18; // [680] 688
    }
    else{
    }
    EPrintf(_fn_11197, _6365, _tables_11198);
L18: 

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_11198);
    DeRef(_seek_1__tmp_at691_11330);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _tables_11198;
    _seek_1__tmp_at691_11330 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_691_11329 = machine(19, _seek_1__tmp_at691_11330);
    DeRef(_seek_1__tmp_at691_11330);
    _seek_1__tmp_at691_11330 = NOVALUE;

    /** 	ntables = get4()*/
    _0 = _ntables_11199;
    _ntables_11199 = _2get4();
    DeRef(_0);

    /** 	t_header = io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_t_header_11202);
    _t_header_11202 = machine(20, _2current_db_10880);

    /** 	for t = 1 to ntables do*/
    Ref(_ntables_11199);
    DeRef(_6367);
    _6367 = _ntables_11199;
    {
        int _t_11335;
        _t_11335 = 1;
L19: 
        if (binary_op_a(GREATER, _t_11335, _6367)){
            goto L1A; // [726] 1154
        }

        /** 		if low_level_too then printf(fn, "\n---------------\n[table header:#%08x]\n", t_header) end if*/
        if (_low_level_too_11193 == 0)
        {
            goto L1B; // [735] 743
        }
        else{
        }
        EPrintf(_fn_11197, _6368, _t_header_11202);
L1B: 

        /** 		tname = get4()*/
        _0 = _tname_11200;
        _tname_11200 = _2get4();
        DeRef(_0);

        /** 		tnrecs = get4()*/
        _0 = _tnrecs_11203;
        _tnrecs_11203 = _2get4();
        DeRef(_0);

        /** 		tblocks = get4()*/
        _tblocks_11214 = _2get4();
        if (!IS_ATOM_INT(_tblocks_11214)) {
            _1 = (long)(DBL_PTR(_tblocks_11214)->dbl);
            if (UNIQUE(DBL_PTR(_tblocks_11214)) && (DBL_PTR(_tblocks_11214)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tblocks_11214);
            _tblocks_11214 = _1;
        }

        /** 		tindex = get4()*/
        _0 = _tindex_11208;
        _tindex_11208 = _2get4();
        DeRef(_0);

        /** 		if low_level_too then printf(fn, "[table name:#%08x]\n", tname) end if*/
        if (_low_level_too_11193 == 0)
        {
            goto L1C; // [769] 777
        }
        else{
        }
        EPrintf(_fn_11197, _6373, _tname_11200);
L1C: 

        /** 		io:seek(current_db, tname)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_tname_11200);
        DeRef(_seek_1__tmp_at780_11347);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _tname_11200;
        _seek_1__tmp_at780_11347 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_780_11346 = machine(19, _seek_1__tmp_at780_11347);
        DeRef(_seek_1__tmp_at780_11347);
        _seek_1__tmp_at780_11347 = NOVALUE;

        /** 		printf(fn, "\ntable \"%s\", records:%d    indexblks: %d\n\n\n", {get_string(), tnrecs, tblocks})*/
        _6375 = _2get_string();
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _6375;
        Ref(_tnrecs_11203);
        *((int *)(_2+8)) = _tnrecs_11203;
        *((int *)(_2+12)) = _tblocks_11214;
        _6376 = MAKE_SEQ(_1);
        _6375 = NOVALUE;
        EPrintf(_fn_11197, _6374, _6376);
        DeRefDS(_6376);
        _6376 = NOVALUE;

        /** 		if tnrecs > 0 then*/
        if (binary_op_a(LESSEQ, _tnrecs_11203, 0)){
            goto L1D; // [811] 1124
        }

        /** 			for b = 1 to tblocks do*/
        _6378 = _tblocks_11214;
        {
            int _b_11354;
            _b_11354 = 1;
L1E: 
            if (_b_11354 > _6378){
                goto L1F; // [820] 1123
            }

            /** 				if low_level_too then printf(fn, "[table block %d:#%08x]\n", {b, tindex+(b-1)*8}) end if*/
            if (_low_level_too_11193 == 0)
            {
                goto L20; // [829] 853
            }
            else{
            }
            _6380 = _b_11354 - 1;
            if (_6380 == (short)_6380)
            _6381 = _6380 * 8;
            else
            _6381 = NewDouble(_6380 * (double)8);
            _6380 = NOVALUE;
            if (IS_ATOM_INT(_tindex_11208) && IS_ATOM_INT(_6381)) {
                _6382 = _tindex_11208 + _6381;
                if ((long)((unsigned long)_6382 + (unsigned long)HIGH_BITS) >= 0) 
                _6382 = NewDouble((double)_6382);
            }
            else {
                if (IS_ATOM_INT(_tindex_11208)) {
                    _6382 = NewDouble((double)_tindex_11208 + DBL_PTR(_6381)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_6381)) {
                        _6382 = NewDouble(DBL_PTR(_tindex_11208)->dbl + (double)_6381);
                    }
                    else
                    _6382 = NewDouble(DBL_PTR(_tindex_11208)->dbl + DBL_PTR(_6381)->dbl);
                }
            }
            DeRef(_6381);
            _6381 = NOVALUE;
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _b_11354;
            ((int *)_2)[2] = _6382;
            _6383 = MAKE_SEQ(_1);
            _6382 = NOVALUE;
            EPrintf(_fn_11197, _6379, _6383);
            DeRefDS(_6383);
            _6383 = NOVALUE;
L20: 

            /** 				io:seek(current_db, tindex+(b-1)*8)*/
            _6384 = _b_11354 - 1;
            if (_6384 == (short)_6384)
            _6385 = _6384 * 8;
            else
            _6385 = NewDouble(_6384 * (double)8);
            _6384 = NOVALUE;
            if (IS_ATOM_INT(_tindex_11208) && IS_ATOM_INT(_6385)) {
                _6386 = _tindex_11208 + _6385;
                if ((long)((unsigned long)_6386 + (unsigned long)HIGH_BITS) >= 0) 
                _6386 = NewDouble((double)_6386);
            }
            else {
                if (IS_ATOM_INT(_tindex_11208)) {
                    _6386 = NewDouble((double)_tindex_11208 + DBL_PTR(_6385)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_6385)) {
                        _6386 = NewDouble(DBL_PTR(_tindex_11208)->dbl + (double)_6385);
                    }
                    else
                    _6386 = NewDouble(DBL_PTR(_tindex_11208)->dbl + DBL_PTR(_6385)->dbl);
                }
            }
            DeRef(_6385);
            _6385 = NOVALUE;
            DeRef(_pos_inlined_seek_at_868_11366);
            _pos_inlined_seek_at_868_11366 = _6386;
            _6386 = NOVALUE;

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_pos_inlined_seek_at_868_11366);
            DeRef(_seek_1__tmp_at871_11368);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _2current_db_10880;
            ((int *)_2)[2] = _pos_inlined_seek_at_868_11366;
            _seek_1__tmp_at871_11368 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_871_11367 = machine(19, _seek_1__tmp_at871_11368);
            DeRef(_pos_inlined_seek_at_868_11366);
            _pos_inlined_seek_at_868_11366 = NOVALUE;
            DeRef(_seek_1__tmp_at871_11368);
            _seek_1__tmp_at871_11368 = NOVALUE;

            /** 				tnrecs = get4()*/
            _0 = _tnrecs_11203;
            _tnrecs_11203 = _2get4();
            DeRef(_0);

            /** 				trecords = get4()*/
            _0 = _trecords_11201;
            _trecords_11201 = _2get4();
            DeRef(_0);

            /** 				if tnrecs > 0 then*/
            if (binary_op_a(LESSEQ, _tnrecs_11203, 0)){
                goto L21; // [897] 1109
            }

            /** 					printf(fn, "\n--------------------------\nblock #%d, ptrs:%d\n--------------------------\n", {b, tnrecs})*/
            Ref(_tnrecs_11203);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _b_11354;
            ((int *)_2)[2] = _tnrecs_11203;
            _6391 = MAKE_SEQ(_1);
            EPrintf(_fn_11197, _6390, _6391);
            DeRefDS(_6391);
            _6391 = NOVALUE;

            /** 					for r = 1 to tnrecs do*/
            Ref(_tnrecs_11203);
            DeRef(_6392);
            _6392 = _tnrecs_11203;
            {
                int _r_11376;
                _r_11376 = 1;
L22: 
                if (binary_op_a(GREATER, _r_11376, _6392)){
                    goto L23; // [916] 1106
                }

                /** 						if low_level_too then printf(fn, "[record %d:#%08x]\n", {r, trecords+(r-1)*4}) end if*/
                if (_low_level_too_11193 == 0)
                {
                    goto L24; // [925] 949
                }
                else{
                }
                if (IS_ATOM_INT(_r_11376)) {
                    _6394 = _r_11376 - 1;
                    if ((long)((unsigned long)_6394 +(unsigned long) HIGH_BITS) >= 0){
                        _6394 = NewDouble((double)_6394);
                    }
                }
                else {
                    _6394 = NewDouble(DBL_PTR(_r_11376)->dbl - (double)1);
                }
                if (IS_ATOM_INT(_6394)) {
                    if (_6394 == (short)_6394)
                    _6395 = _6394 * 4;
                    else
                    _6395 = NewDouble(_6394 * (double)4);
                }
                else {
                    _6395 = NewDouble(DBL_PTR(_6394)->dbl * (double)4);
                }
                DeRef(_6394);
                _6394 = NOVALUE;
                if (IS_ATOM_INT(_trecords_11201) && IS_ATOM_INT(_6395)) {
                    _6396 = _trecords_11201 + _6395;
                    if ((long)((unsigned long)_6396 + (unsigned long)HIGH_BITS) >= 0) 
                    _6396 = NewDouble((double)_6396);
                }
                else {
                    if (IS_ATOM_INT(_trecords_11201)) {
                        _6396 = NewDouble((double)_trecords_11201 + DBL_PTR(_6395)->dbl);
                    }
                    else {
                        if (IS_ATOM_INT(_6395)) {
                            _6396 = NewDouble(DBL_PTR(_trecords_11201)->dbl + (double)_6395);
                        }
                        else
                        _6396 = NewDouble(DBL_PTR(_trecords_11201)->dbl + DBL_PTR(_6395)->dbl);
                    }
                }
                DeRef(_6395);
                _6395 = NOVALUE;
                Ref(_r_11376);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _r_11376;
                ((int *)_2)[2] = _6396;
                _6397 = MAKE_SEQ(_1);
                _6396 = NOVALUE;
                EPrintf(_fn_11197, _6393, _6397);
                DeRefDS(_6397);
                _6397 = NOVALUE;
L24: 

                /** 						io:seek(current_db, trecords+(r-1)*4)*/
                if (IS_ATOM_INT(_r_11376)) {
                    _6398 = _r_11376 - 1;
                    if ((long)((unsigned long)_6398 +(unsigned long) HIGH_BITS) >= 0){
                        _6398 = NewDouble((double)_6398);
                    }
                }
                else {
                    _6398 = NewDouble(DBL_PTR(_r_11376)->dbl - (double)1);
                }
                if (IS_ATOM_INT(_6398)) {
                    if (_6398 == (short)_6398)
                    _6399 = _6398 * 4;
                    else
                    _6399 = NewDouble(_6398 * (double)4);
                }
                else {
                    _6399 = NewDouble(DBL_PTR(_6398)->dbl * (double)4);
                }
                DeRef(_6398);
                _6398 = NOVALUE;
                if (IS_ATOM_INT(_trecords_11201) && IS_ATOM_INT(_6399)) {
                    _6400 = _trecords_11201 + _6399;
                    if ((long)((unsigned long)_6400 + (unsigned long)HIGH_BITS) >= 0) 
                    _6400 = NewDouble((double)_6400);
                }
                else {
                    if (IS_ATOM_INT(_trecords_11201)) {
                        _6400 = NewDouble((double)_trecords_11201 + DBL_PTR(_6399)->dbl);
                    }
                    else {
                        if (IS_ATOM_INT(_6399)) {
                            _6400 = NewDouble(DBL_PTR(_trecords_11201)->dbl + (double)_6399);
                        }
                        else
                        _6400 = NewDouble(DBL_PTR(_trecords_11201)->dbl + DBL_PTR(_6399)->dbl);
                    }
                }
                DeRef(_6399);
                _6399 = NOVALUE;
                DeRef(_pos_inlined_seek_at_964_11388);
                _pos_inlined_seek_at_964_11388 = _6400;
                _6400 = NOVALUE;

                /** 	return machine_func(M_SEEK, {fn, pos})*/
                Ref(_pos_inlined_seek_at_964_11388);
                DeRef(_seek_1__tmp_at967_11390);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _2current_db_10880;
                ((int *)_2)[2] = _pos_inlined_seek_at_964_11388;
                _seek_1__tmp_at967_11390 = MAKE_SEQ(_1);
                _seek_inlined_seek_at_967_11389 = machine(19, _seek_1__tmp_at967_11390);
                DeRef(_pos_inlined_seek_at_964_11388);
                _pos_inlined_seek_at_964_11388 = NOVALUE;
                DeRef(_seek_1__tmp_at967_11390);
                _seek_1__tmp_at967_11390 = NOVALUE;

                /** 						key_ptr = get4()*/
                _0 = _key_ptr_11204;
                _key_ptr_11204 = _2get4();
                DeRef(_0);

                /** 						if low_level_too then printf(fn, "[key %d:#%08x]\n", {r, key_ptr}) end if*/
                if (_low_level_too_11193 == 0)
                {
                    goto L25; // [988] 1000
                }
                else{
                }
                Ref(_key_ptr_11204);
                Ref(_r_11376);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _r_11376;
                ((int *)_2)[2] = _key_ptr_11204;
                _6403 = MAKE_SEQ(_1);
                EPrintf(_fn_11197, _6402, _6403);
                DeRefDS(_6403);
                _6403 = NOVALUE;
L25: 

                /** 						io:seek(current_db, key_ptr)*/

                /** 	return machine_func(M_SEEK, {fn, pos})*/
                Ref(_key_ptr_11204);
                DeRef(_seek_1__tmp_at1003_11397);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _2current_db_10880;
                ((int *)_2)[2] = _key_ptr_11204;
                _seek_1__tmp_at1003_11397 = MAKE_SEQ(_1);
                _seek_inlined_seek_at_1003_11396 = machine(19, _seek_1__tmp_at1003_11397);
                DeRef(_seek_1__tmp_at1003_11397);
                _seek_1__tmp_at1003_11397 = NOVALUE;

                /** 						data_ptr = get4()*/
                _0 = _data_ptr_11205;
                _data_ptr_11205 = _2get4();
                DeRef(_0);

                /** 						key = decompress(0)*/
                _0 = _key_11210;
                _key_11210 = _2decompress(0);
                DeRef(_0);

                /** 						puts(fn, "  key: ")*/
                EPuts(_fn_11197, _6406); // DJP 

                /** 						pretty:pretty_print(fn, key, {2, 2, 8})*/
                Ref(_key_11210);
                RefDS(_6407);
                _23pretty_print(_fn_11197, _key_11210, _6407);

                /** 						puts(fn, '\n')*/
                EPuts(_fn_11197, 10); // DJP 

                /** 						if low_level_too then printf(fn, "[data %d:#%08x]\n", {r, data_ptr}) end if*/
                if (_low_level_too_11193 == 0)
                {
                    goto L26; // [1047] 1059
                }
                else{
                }
                Ref(_data_ptr_11205);
                Ref(_r_11376);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _r_11376;
                ((int *)_2)[2] = _data_ptr_11205;
                _6409 = MAKE_SEQ(_1);
                EPrintf(_fn_11197, _6408, _6409);
                DeRefDS(_6409);
                _6409 = NOVALUE;
L26: 

                /** 						io:seek(current_db, data_ptr)*/

                /** 	return machine_func(M_SEEK, {fn, pos})*/
                Ref(_data_ptr_11205);
                DeRef(_seek_1__tmp_at1062_11407);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _2current_db_10880;
                ((int *)_2)[2] = _data_ptr_11205;
                _seek_1__tmp_at1062_11407 = MAKE_SEQ(_1);
                _seek_inlined_seek_at_1062_11406 = machine(19, _seek_1__tmp_at1062_11407);
                DeRef(_seek_1__tmp_at1062_11407);
                _seek_1__tmp_at1062_11407 = NOVALUE;

                /** 						data = decompress(0)*/
                _0 = _data_11211;
                _data_11211 = _2decompress(0);
                DeRef(_0);

                /** 						puts(fn, "  data: ")*/
                EPuts(_fn_11197, _6411); // DJP 

                /** 						pretty:pretty_print(fn, data, {2, 2, 9})*/
                Ref(_data_11211);
                RefDS(_6412);
                _23pretty_print(_fn_11197, _data_11211, _6412);

                /** 						puts(fn, "\n\n")*/
                EPuts(_fn_11197, _93); // DJP 

                /** 					end for*/
                _0 = _r_11376;
                if (IS_ATOM_INT(_r_11376)) {
                    _r_11376 = _r_11376 + 1;
                    if ((long)((unsigned long)_r_11376 +(unsigned long) HIGH_BITS) >= 0){
                        _r_11376 = NewDouble((double)_r_11376);
                    }
                }
                else {
                    _r_11376 = binary_op_a(PLUS, _r_11376, 1);
                }
                DeRef(_0);
                goto L22; // [1101] 923
L23: 
                ;
                DeRef(_r_11376);
            }
            goto L27; // [1106] 1116
L21: 

            /** 					printf(fn, "\nblock #%d (empty)\n\n", b)*/
            EPrintf(_fn_11197, _6413, _b_11354);
L27: 

            /** 			end for*/
            _b_11354 = _b_11354 + 1;
            goto L1E; // [1118] 827
L1F: 
            ;
        }
L1D: 

        /** 		t_header += SIZEOF_TABLE_HEADER*/
        _0 = _t_header_11202;
        if (IS_ATOM_INT(_t_header_11202)) {
            _t_header_11202 = _t_header_11202 + 16;
            if ((long)((unsigned long)_t_header_11202 + (unsigned long)HIGH_BITS) >= 0) 
            _t_header_11202 = NewDouble((double)_t_header_11202);
        }
        else {
            _t_header_11202 = NewDouble(DBL_PTR(_t_header_11202)->dbl + (double)16);
        }
        DeRef(_0);

        /** 		io:seek(current_db, t_header)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_t_header_11202);
        DeRef(_seek_1__tmp_at1133_11416);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _t_header_11202;
        _seek_1__tmp_at1133_11416 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_1133_11415 = machine(19, _seek_1__tmp_at1133_11416);
        DeRef(_seek_1__tmp_at1133_11416);
        _seek_1__tmp_at1133_11416 = NOVALUE;

        /** 	end for*/
        _0 = _t_11335;
        if (IS_ATOM_INT(_t_11335)) {
            _t_11335 = _t_11335 + 1;
            if ((long)((unsigned long)_t_11335 +(unsigned long) HIGH_BITS) >= 0){
                _t_11335 = NewDouble((double)_t_11335);
            }
        }
        else {
            _t_11335 = binary_op_a(PLUS, _t_11335, 1);
        }
        DeRef(_0);
        goto L19; // [1149] 733
L1A: 
        ;
        DeRef(_t_11335);
    }

    /** 	if low_level_too then printf(fn, "[free blocks:#%08x]\n", FREE_COUNT) end if*/
    if (_low_level_too_11193 == 0)
    {
        goto L28; // [1156] 1164
    }
    else{
    }
    EPrintf(_fn_11197, _6415, 7);
L28: 

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at1167_11421);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at1167_11421 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1167_11420 = machine(19, _seek_1__tmp_at1167_11421);
    DeRefi(_seek_1__tmp_at1167_11421);
    _seek_1__tmp_at1167_11421 = NOVALUE;

    /** 	n = get4()*/
    _n_11213 = _2get4();
    if (!IS_ATOM_INT(_n_11213)) {
        _1 = (long)(DBL_PTR(_n_11213)->dbl);
        if (UNIQUE(DBL_PTR(_n_11213)) && (DBL_PTR(_n_11213)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_11213);
        _n_11213 = _1;
    }

    /** 	puts(fn, '\n')*/
    EPuts(_fn_11197, 10); // DJP 

    /** 	if n > 0 then*/
    if (_n_11213 <= 0)
    goto L29; // [1197] 1286

    /** 		fbp = get4()*/
    _0 = _fbp_11209;
    _fbp_11209 = _2get4();
    DeRef(_0);

    /** 		printf(fn, "Number of Free blocks: %d ", n)*/
    EPrintf(_fn_11197, _6419, _n_11213);

    /** 		if low_level_too then printf(fn, " [#%08x]:", fbp) end if*/
    if (_low_level_too_11193 == 0)
    {
        goto L2A; // [1214] 1222
    }
    else{
    }
    EPrintf(_fn_11197, _6420, _fbp_11209);
L2A: 

    /** 		puts(fn, '\n')*/
    EPuts(_fn_11197, 10); // DJP 

    /** 		io:seek(current_db, fbp)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_fbp_11209);
    DeRef(_seek_1__tmp_at1230_11431);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _fbp_11209;
    _seek_1__tmp_at1230_11431 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1230_11430 = machine(19, _seek_1__tmp_at1230_11431);
    DeRef(_seek_1__tmp_at1230_11431);
    _seek_1__tmp_at1230_11431 = NOVALUE;

    /** 		for i = 1 to n do*/
    _6421 = _n_11213;
    {
        int _i_11433;
        _i_11433 = 1;
L2B: 
        if (_i_11433 > _6421){
            goto L2C; // [1249] 1283
        }

        /** 			addr = get4()*/
        _0 = _addr_11207;
        _addr_11207 = _2get4();
        DeRef(_0);

        /** 			size = get4()*/
        _0 = _size_11206;
        _size_11206 = _2get4();
        DeRef(_0);

        /** 			printf(fn, "%08x: %6d bytes\n", {addr, size})*/
        Ref(_size_11206);
        Ref(_addr_11207);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _addr_11207;
        ((int *)_2)[2] = _size_11206;
        _6425 = MAKE_SEQ(_1);
        EPrintf(_fn_11197, _6424, _6425);
        DeRefDS(_6425);
        _6425 = NOVALUE;

        /** 		end for*/
        _i_11433 = _i_11433 + 1;
        goto L2B; // [1278] 1256
L2C: 
        ;
    }
    goto L2D; // [1283] 1292
L29: 

    /** 		puts(fn, "No free blocks available.\n")*/
    EPuts(_fn_11197, _6426); // DJP 
L2D: 

    /** 	if sequence(file_id) then*/
    _6427 = IS_SEQUENCE(_file_id_11192);
    if (_6427 == 0)
    {
        _6427 = NOVALUE;
        goto L2E; // [1297] 1305
    }
    else{
        _6427 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_11197);
L2E: 

    /** end procedure*/
    DeRef(_file_id_11192);
    DeRef(_tables_11198);
    DeRef(_ntables_11199);
    DeRef(_tname_11200);
    DeRef(_trecords_11201);
    DeRef(_t_header_11202);
    DeRef(_tnrecs_11203);
    DeRef(_key_ptr_11204);
    DeRef(_data_ptr_11205);
    DeRef(_size_11206);
    DeRef(_addr_11207);
    DeRef(_tindex_11208);
    DeRef(_fbp_11209);
    DeRef(_key_11210);
    DeRef(_data_11211);
    DeRef(_a_11215);
    DeRef(_ll_line_11216);
    DeRef(_6353);
    _6353 = NOVALUE;
    return;
    ;
}


void _2check_free_list()
{
    int _msg_inlined_crash_at_273_11501 = NOVALUE;
    int _msg_inlined_crash_at_233_11494 = NOVALUE;
    int _msg_inlined_crash_at_201_11488 = NOVALUE;
    int _msg_inlined_crash_at_142_11477 = NOVALUE;
    int _msg_inlined_crash_at_87_11468 = NOVALUE;
    int _msg_inlined_crash_at_55_11462 = NOVALUE;
    int _where_inlined_where_at_25_11455 = NOVALUE;
    int _free_count_11445 = NOVALUE;
    int _free_list_11446 = NOVALUE;
    int _addr_11447 = NOVALUE;
    int _size_11448 = NOVALUE;
    int _free_list_space_11449 = NOVALUE;
    int _max_11450 = NOVALUE;
    int _6453 = NOVALUE;
    int _6452 = NOVALUE;
    int _6445 = NOVALUE;
    int _6444 = NOVALUE;
    int _6443 = NOVALUE;
    int _6441 = NOVALUE;
    int _6439 = NOVALUE;
    int _6437 = NOVALUE;
    int _6431 = NOVALUE;
    int _6428 = NOVALUE;
    int _0, _1, _2;
    

    /** 	safe_seek(-1)*/
    RefDS(_5);
    _2safe_seek(-1, _5);

    /** 	if length(vLastErrors) > 0 then return end if*/
    if (IS_SEQUENCE(_2vLastErrors_10904)){
            _6428 = SEQ_PTR(_2vLastErrors_10904)->length;
    }
    else {
        _6428 = 1;
    }
    if (_6428 <= 0)
    goto L1; // [14] 22
    DeRef(_free_count_11445);
    DeRef(_free_list_11446);
    DeRef(_addr_11447);
    DeRef(_size_11448);
    DeRef(_free_list_space_11449);
    DeRef(_max_11450);
    return;
L1: 

    /** 	max = io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_max_11450);
    _max_11450 = machine(20, _2current_db_10880);

    /** 	safe_seek( FREE_COUNT)*/
    RefDS(_5);
    _2safe_seek(7, _5);

    /** 	free_count = get4()*/
    _0 = _free_count_11445;
    _free_count_11445 = _2get4();
    DeRef(_0);

    /** 	if free_count > max/13 then*/
    if (IS_ATOM_INT(_max_11450)) {
        _6431 = (_max_11450 % 13) ? NewDouble((double)_max_11450 / 13) : (_max_11450 / 13);
    }
    else {
        _6431 = NewDouble(DBL_PTR(_max_11450)->dbl / (double)13);
    }
    if (binary_op_a(LESSEQ, _free_count_11445, _6431)){
        DeRef(_6431);
        _6431 = NOVALUE;
        goto L2; // [50] 75
    }
    DeRef(_6431);
    _6431 = NOVALUE;

    /** 		error:crash("free count is too high")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_55_11462);
    _msg_inlined_crash_at_55_11462 = EPrintf(-9999999, _6433, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_55_11462);

    /** end procedure*/
    goto L3; // [69] 72
L3: 
    DeRefi(_msg_inlined_crash_at_55_11462);
    _msg_inlined_crash_at_55_11462 = NOVALUE;
L2: 

    /** 	free_list = get4()*/
    _0 = _free_list_11446;
    _free_list_11446 = _2get4();
    DeRef(_0);

    /** 	if free_list > max then*/
    if (binary_op_a(LESSEQ, _free_list_11446, _max_11450)){
        goto L4; // [82] 107
    }

    /** 		error:crash("bad free list pointer")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_87_11468);
    _msg_inlined_crash_at_87_11468 = EPrintf(-9999999, _6436, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_87_11468);

    /** end procedure*/
    goto L5; // [101] 104
L5: 
    DeRefi(_msg_inlined_crash_at_87_11468);
    _msg_inlined_crash_at_87_11468 = NOVALUE;
L4: 

    /** 	safe_seek( free_list - 4)*/
    if (IS_ATOM_INT(_free_list_11446)) {
        _6437 = _free_list_11446 - 4;
        if ((long)((unsigned long)_6437 +(unsigned long) HIGH_BITS) >= 0){
            _6437 = NewDouble((double)_6437);
        }
    }
    else {
        _6437 = NewDouble(DBL_PTR(_free_list_11446)->dbl - (double)4);
    }
    RefDS(_5);
    _2safe_seek(_6437, _5);
    _6437 = NOVALUE;

    /** 	free_list_space = get4()*/
    _0 = _free_list_space_11449;
    _free_list_space_11449 = _2get4();
    DeRef(_0);

    /** 	if free_list_space > max or free_list_space < 0 then*/
    if (IS_ATOM_INT(_free_list_space_11449) && IS_ATOM_INT(_max_11450)) {
        _6439 = (_free_list_space_11449 > _max_11450);
    }
    else {
        if (IS_ATOM_INT(_free_list_space_11449)) {
            _6439 = ((double)_free_list_space_11449 > DBL_PTR(_max_11450)->dbl);
        }
        else {
            if (IS_ATOM_INT(_max_11450)) {
                _6439 = (DBL_PTR(_free_list_space_11449)->dbl > (double)_max_11450);
            }
            else
            _6439 = (DBL_PTR(_free_list_space_11449)->dbl > DBL_PTR(_max_11450)->dbl);
        }
    }
    if (_6439 != 0) {
        goto L6; // [128] 141
    }
    if (IS_ATOM_INT(_free_list_space_11449)) {
        _6441 = (_free_list_space_11449 < 0);
    }
    else {
        _6441 = (DBL_PTR(_free_list_space_11449)->dbl < (double)0);
    }
    if (_6441 == 0)
    {
        DeRef(_6441);
        _6441 = NOVALUE;
        goto L7; // [137] 162
    }
    else{
        DeRef(_6441);
        _6441 = NOVALUE;
    }
L6: 

    /** 		error:crash("free list space is bad")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_142_11477);
    _msg_inlined_crash_at_142_11477 = EPrintf(-9999999, _6442, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_142_11477);

    /** end procedure*/
    goto L8; // [156] 159
L8: 
    DeRefi(_msg_inlined_crash_at_142_11477);
    _msg_inlined_crash_at_142_11477 = NOVALUE;
L7: 

    /** 	for i = 0 to free_count - 1 do*/
    if (IS_ATOM_INT(_free_count_11445)) {
        _6443 = _free_count_11445 - 1;
        if ((long)((unsigned long)_6443 +(unsigned long) HIGH_BITS) >= 0){
            _6443 = NewDouble((double)_6443);
        }
    }
    else {
        _6443 = NewDouble(DBL_PTR(_free_count_11445)->dbl - (double)1);
    }
    {
        int _i_11479;
        _i_11479 = 0;
L9: 
        if (binary_op_a(GREATER, _i_11479, _6443)){
            goto LA; // [168] 300
        }

        /** 		safe_seek( free_list + i * 8)*/
        if (IS_ATOM_INT(_i_11479)) {
            if (_i_11479 == (short)_i_11479)
            _6444 = _i_11479 * 8;
            else
            _6444 = NewDouble(_i_11479 * (double)8);
        }
        else {
            _6444 = NewDouble(DBL_PTR(_i_11479)->dbl * (double)8);
        }
        if (IS_ATOM_INT(_free_list_11446) && IS_ATOM_INT(_6444)) {
            _6445 = _free_list_11446 + _6444;
            if ((long)((unsigned long)_6445 + (unsigned long)HIGH_BITS) >= 0) 
            _6445 = NewDouble((double)_6445);
        }
        else {
            if (IS_ATOM_INT(_free_list_11446)) {
                _6445 = NewDouble((double)_free_list_11446 + DBL_PTR(_6444)->dbl);
            }
            else {
                if (IS_ATOM_INT(_6444)) {
                    _6445 = NewDouble(DBL_PTR(_free_list_11446)->dbl + (double)_6444);
                }
                else
                _6445 = NewDouble(DBL_PTR(_free_list_11446)->dbl + DBL_PTR(_6444)->dbl);
            }
        }
        DeRef(_6444);
        _6444 = NOVALUE;
        RefDS(_5);
        _2safe_seek(_6445, _5);
        _6445 = NOVALUE;

        /** 		addr = get4()*/
        _0 = _addr_11447;
        _addr_11447 = _2get4();
        DeRef(_0);

        /** 		if addr > max then*/
        if (binary_op_a(LESSEQ, _addr_11447, _max_11450)){
            goto LB; // [196] 221
        }

        /** 			error:crash("bad block address")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_201_11488);
        _msg_inlined_crash_at_201_11488 = EPrintf(-9999999, _6448, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_201_11488);

        /** end procedure*/
        goto LC; // [215] 218
LC: 
        DeRefi(_msg_inlined_crash_at_201_11488);
        _msg_inlined_crash_at_201_11488 = NOVALUE;
LB: 

        /** 		size = get4()*/
        _0 = _size_11448;
        _size_11448 = _2get4();
        DeRef(_0);

        /** 		if size > max then*/
        if (binary_op_a(LESSEQ, _size_11448, _max_11450)){
            goto LD; // [228] 253
        }

        /** 			error:crash("block size too big")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_233_11494);
        _msg_inlined_crash_at_233_11494 = EPrintf(-9999999, _6451, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_233_11494);

        /** end procedure*/
        goto LE; // [247] 250
LE: 
        DeRefi(_msg_inlined_crash_at_233_11494);
        _msg_inlined_crash_at_233_11494 = NOVALUE;
LD: 

        /** 		safe_seek( addr - 4)*/
        if (IS_ATOM_INT(_addr_11447)) {
            _6452 = _addr_11447 - 4;
            if ((long)((unsigned long)_6452 +(unsigned long) HIGH_BITS) >= 0){
                _6452 = NewDouble((double)_6452);
            }
        }
        else {
            _6452 = NewDouble(DBL_PTR(_addr_11447)->dbl - (double)4);
        }
        RefDS(_5);
        _2safe_seek(_6452, _5);
        _6452 = NOVALUE;

        /** 		if get4() > size then*/
        _6453 = _2get4();
        if (binary_op_a(LESSEQ, _6453, _size_11448)){
            DeRef(_6453);
            _6453 = NOVALUE;
            goto LF; // [268] 293
        }
        DeRef(_6453);
        _6453 = NOVALUE;

        /** 			error:crash("bad size in front of free block")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_273_11501);
        _msg_inlined_crash_at_273_11501 = EPrintf(-9999999, _6455, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_273_11501);

        /** end procedure*/
        goto L10; // [287] 290
L10: 
        DeRefi(_msg_inlined_crash_at_273_11501);
        _msg_inlined_crash_at_273_11501 = NOVALUE;
LF: 

        /** 	end for*/
        _0 = _i_11479;
        if (IS_ATOM_INT(_i_11479)) {
            _i_11479 = _i_11479 + 1;
            if ((long)((unsigned long)_i_11479 +(unsigned long) HIGH_BITS) >= 0){
                _i_11479 = NewDouble((double)_i_11479);
            }
        }
        else {
            _i_11479 = binary_op_a(PLUS, _i_11479, 1);
        }
        DeRef(_0);
        goto L9; // [295] 175
LA: 
        ;
        DeRef(_i_11479);
    }

    /** end procedure*/
    DeRef(_free_count_11445);
    DeRef(_free_list_11446);
    DeRef(_addr_11447);
    DeRef(_size_11448);
    DeRef(_free_list_space_11449);
    DeRef(_max_11450);
    DeRef(_6439);
    _6439 = NOVALUE;
    DeRef(_6443);
    _6443 = NOVALUE;
    return;
    ;
}


int _2db_allocate(int _n_11504)
{
    int _free_list_11505 = NOVALUE;
    int _size_11506 = NOVALUE;
    int _size_ptr_11507 = NOVALUE;
    int _addr_11508 = NOVALUE;
    int _free_count_11509 = NOVALUE;
    int _remaining_11510 = NOVALUE;
    int _seek_1__tmp_at4_11513 = NOVALUE;
    int _seek_inlined_seek_at_4_11512 = NOVALUE;
    int _seek_1__tmp_at41_11520 = NOVALUE;
    int _seek_inlined_seek_at_41_11519 = NOVALUE;
    int _seek_1__tmp_at113_11537 = NOVALUE;
    int _seek_inlined_seek_at_113_11536 = NOVALUE;
    int _pos_inlined_seek_at_110_11535 = NOVALUE;
    int _put4_1__tmp_at139_11542 = NOVALUE;
    int _x_inlined_put4_at_136_11541 = NOVALUE;
    int _seek_1__tmp_at169_11545 = NOVALUE;
    int _seek_inlined_seek_at_169_11544 = NOVALUE;
    int _put4_1__tmp_at195_11550 = NOVALUE;
    int _x_inlined_put4_at_192_11549 = NOVALUE;
    int _seek_1__tmp_at246_11558 = NOVALUE;
    int _seek_inlined_seek_at_246_11557 = NOVALUE;
    int _pos_inlined_seek_at_243_11556 = NOVALUE;
    int _put4_1__tmp_at268_11562 = NOVALUE;
    int _x_inlined_put4_at_265_11561 = NOVALUE;
    int _seek_1__tmp_at335_11573 = NOVALUE;
    int _seek_inlined_seek_at_335_11572 = NOVALUE;
    int _pos_inlined_seek_at_332_11571 = NOVALUE;
    int _seek_1__tmp_at366_11577 = NOVALUE;
    int _seek_inlined_seek_at_366_11576 = NOVALUE;
    int _put4_1__tmp_at388_11581 = NOVALUE;
    int _x_inlined_put4_at_385_11580 = NOVALUE;
    int _seek_1__tmp_at425_11586 = NOVALUE;
    int _seek_inlined_seek_at_425_11585 = NOVALUE;
    int _pos_inlined_seek_at_422_11584 = NOVALUE;
    int _put4_1__tmp_at440_11588 = NOVALUE;
    int _seek_1__tmp_at492_11592 = NOVALUE;
    int _seek_inlined_seek_at_492_11591 = NOVALUE;
    int _put4_1__tmp_at514_11596 = NOVALUE;
    int _x_inlined_put4_at_511_11595 = NOVALUE;
    int _where_inlined_where_at_544_11598 = NOVALUE;
    int _6486 = NOVALUE;
    int _6484 = NOVALUE;
    int _6483 = NOVALUE;
    int _6482 = NOVALUE;
    int _6481 = NOVALUE;
    int _6480 = NOVALUE;
    int _6478 = NOVALUE;
    int _6477 = NOVALUE;
    int _6476 = NOVALUE;
    int _6475 = NOVALUE;
    int _6473 = NOVALUE;
    int _6472 = NOVALUE;
    int _6471 = NOVALUE;
    int _6470 = NOVALUE;
    int _6469 = NOVALUE;
    int _6468 = NOVALUE;
    int _6467 = NOVALUE;
    int _6465 = NOVALUE;
    int _6463 = NOVALUE;
    int _6460 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_11513);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at4_11513 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_11512 = machine(19, _seek_1__tmp_at4_11513);
    DeRefi(_seek_1__tmp_at4_11513);
    _seek_1__tmp_at4_11513 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_11509 = _2get4();
    if (!IS_ATOM_INT(_free_count_11509)) {
        _1 = (long)(DBL_PTR(_free_count_11509)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_11509)) && (DBL_PTR(_free_count_11509)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_11509);
        _free_count_11509 = _1;
    }

    /** 	if free_count > 0 then*/
    if (_free_count_11509 <= 0)
    goto L1; // [29] 489

    /** 		free_list = get4()*/
    _0 = _free_list_11505;
    _free_list_11505 = _2get4();
    DeRef(_0);

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_11505);
    DeRef(_seek_1__tmp_at41_11520);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _free_list_11505;
    _seek_1__tmp_at41_11520 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_41_11519 = machine(19, _seek_1__tmp_at41_11520);
    DeRef(_seek_1__tmp_at41_11520);
    _seek_1__tmp_at41_11520 = NOVALUE;

    /** 		size_ptr = free_list + 4*/
    DeRef(_size_ptr_11507);
    if (IS_ATOM_INT(_free_list_11505)) {
        _size_ptr_11507 = _free_list_11505 + 4;
        if ((long)((unsigned long)_size_ptr_11507 + (unsigned long)HIGH_BITS) >= 0) 
        _size_ptr_11507 = NewDouble((double)_size_ptr_11507);
    }
    else {
        _size_ptr_11507 = NewDouble(DBL_PTR(_free_list_11505)->dbl + (double)4);
    }

    /** 		for i = 1 to free_count do*/
    _6460 = _free_count_11509;
    {
        int _i_11523;
        _i_11523 = 1;
L2: 
        if (_i_11523 > _6460){
            goto L3; // [66] 488
        }

        /** 			addr = get4()*/
        _0 = _addr_11508;
        _addr_11508 = _2get4();
        DeRef(_0);

        /** 			size = get4()*/
        _0 = _size_11506;
        _size_11506 = _2get4();
        DeRef(_0);

        /** 			if size >= n+4 then*/
        if (IS_ATOM_INT(_n_11504)) {
            _6463 = _n_11504 + 4;
            if ((long)((unsigned long)_6463 + (unsigned long)HIGH_BITS) >= 0) 
            _6463 = NewDouble((double)_6463);
        }
        else {
            _6463 = NewDouble(DBL_PTR(_n_11504)->dbl + (double)4);
        }
        if (binary_op_a(LESS, _size_11506, _6463)){
            DeRef(_6463);
            _6463 = NOVALUE;
            goto L4; // [89] 475
        }
        DeRef(_6463);
        _6463 = NOVALUE;

        /** 				if size >= n+16 then*/
        if (IS_ATOM_INT(_n_11504)) {
            _6465 = _n_11504 + 16;
            if ((long)((unsigned long)_6465 + (unsigned long)HIGH_BITS) >= 0) 
            _6465 = NewDouble((double)_6465);
        }
        else {
            _6465 = NewDouble(DBL_PTR(_n_11504)->dbl + (double)16);
        }
        if (binary_op_a(LESS, _size_11506, _6465)){
            DeRef(_6465);
            _6465 = NOVALUE;
            goto L5; // [99] 298
        }
        DeRef(_6465);
        _6465 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_11508)) {
            _6467 = _addr_11508 - 4;
            if ((long)((unsigned long)_6467 +(unsigned long) HIGH_BITS) >= 0){
                _6467 = NewDouble((double)_6467);
            }
        }
        else {
            _6467 = NewDouble(DBL_PTR(_addr_11508)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_110_11535);
        _pos_inlined_seek_at_110_11535 = _6467;
        _6467 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_110_11535);
        DeRef(_seek_1__tmp_at113_11537);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _pos_inlined_seek_at_110_11535;
        _seek_1__tmp_at113_11537 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_113_11536 = machine(19, _seek_1__tmp_at113_11537);
        DeRef(_pos_inlined_seek_at_110_11535);
        _pos_inlined_seek_at_110_11535 = NOVALUE;
        DeRef(_seek_1__tmp_at113_11537);
        _seek_1__tmp_at113_11537 = NOVALUE;

        /** 					put4(size-n-4) -- shrink the block*/
        if (IS_ATOM_INT(_size_11506) && IS_ATOM_INT(_n_11504)) {
            _6468 = _size_11506 - _n_11504;
            if ((long)((unsigned long)_6468 +(unsigned long) HIGH_BITS) >= 0){
                _6468 = NewDouble((double)_6468);
            }
        }
        else {
            if (IS_ATOM_INT(_size_11506)) {
                _6468 = NewDouble((double)_size_11506 - DBL_PTR(_n_11504)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_11504)) {
                    _6468 = NewDouble(DBL_PTR(_size_11506)->dbl - (double)_n_11504);
                }
                else
                _6468 = NewDouble(DBL_PTR(_size_11506)->dbl - DBL_PTR(_n_11504)->dbl);
            }
        }
        if (IS_ATOM_INT(_6468)) {
            _6469 = _6468 - 4;
            if ((long)((unsigned long)_6469 +(unsigned long) HIGH_BITS) >= 0){
                _6469 = NewDouble((double)_6469);
            }
        }
        else {
            _6469 = NewDouble(DBL_PTR(_6468)->dbl - (double)4);
        }
        DeRef(_6468);
        _6468 = NOVALUE;
        DeRef(_x_inlined_put4_at_136_11541);
        _x_inlined_put4_at_136_11541 = _6469;
        _6469 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_2mem0_10922)){
            poke4_addr = (unsigned long *)_2mem0_10922;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_136_11541)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_136_11541;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_136_11541)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at139_11542);
        _1 = (int)SEQ_PTR(_2memseq_11142);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at139_11542 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_2current_db_10880, _put4_1__tmp_at139_11542); // DJP 

        /** end procedure*/
        goto L6; // [161] 164
L6: 
        DeRef(_x_inlined_put4_at_136_11541);
        _x_inlined_put4_at_136_11541 = NOVALUE;
        DeRefi(_put4_1__tmp_at139_11542);
        _put4_1__tmp_at139_11542 = NOVALUE;

        /** 					io:seek(current_db, size_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_size_ptr_11507);
        DeRef(_seek_1__tmp_at169_11545);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _size_ptr_11507;
        _seek_1__tmp_at169_11545 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_169_11544 = machine(19, _seek_1__tmp_at169_11545);
        DeRef(_seek_1__tmp_at169_11545);
        _seek_1__tmp_at169_11545 = NOVALUE;

        /** 					put4(size-n-4) -- update size on free list too*/
        if (IS_ATOM_INT(_size_11506) && IS_ATOM_INT(_n_11504)) {
            _6470 = _size_11506 - _n_11504;
            if ((long)((unsigned long)_6470 +(unsigned long) HIGH_BITS) >= 0){
                _6470 = NewDouble((double)_6470);
            }
        }
        else {
            if (IS_ATOM_INT(_size_11506)) {
                _6470 = NewDouble((double)_size_11506 - DBL_PTR(_n_11504)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_11504)) {
                    _6470 = NewDouble(DBL_PTR(_size_11506)->dbl - (double)_n_11504);
                }
                else
                _6470 = NewDouble(DBL_PTR(_size_11506)->dbl - DBL_PTR(_n_11504)->dbl);
            }
        }
        if (IS_ATOM_INT(_6470)) {
            _6471 = _6470 - 4;
            if ((long)((unsigned long)_6471 +(unsigned long) HIGH_BITS) >= 0){
                _6471 = NewDouble((double)_6471);
            }
        }
        else {
            _6471 = NewDouble(DBL_PTR(_6470)->dbl - (double)4);
        }
        DeRef(_6470);
        _6470 = NOVALUE;
        DeRef(_x_inlined_put4_at_192_11549);
        _x_inlined_put4_at_192_11549 = _6471;
        _6471 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_2mem0_10922)){
            poke4_addr = (unsigned long *)_2mem0_10922;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_192_11549)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_192_11549;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_192_11549)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at195_11550);
        _1 = (int)SEQ_PTR(_2memseq_11142);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at195_11550 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_2current_db_10880, _put4_1__tmp_at195_11550); // DJP 

        /** end procedure*/
        goto L7; // [217] 220
L7: 
        DeRef(_x_inlined_put4_at_192_11549);
        _x_inlined_put4_at_192_11549 = NOVALUE;
        DeRefi(_put4_1__tmp_at195_11550);
        _put4_1__tmp_at195_11550 = NOVALUE;

        /** 					addr += size-n-4*/
        if (IS_ATOM_INT(_size_11506) && IS_ATOM_INT(_n_11504)) {
            _6472 = _size_11506 - _n_11504;
            if ((long)((unsigned long)_6472 +(unsigned long) HIGH_BITS) >= 0){
                _6472 = NewDouble((double)_6472);
            }
        }
        else {
            if (IS_ATOM_INT(_size_11506)) {
                _6472 = NewDouble((double)_size_11506 - DBL_PTR(_n_11504)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_11504)) {
                    _6472 = NewDouble(DBL_PTR(_size_11506)->dbl - (double)_n_11504);
                }
                else
                _6472 = NewDouble(DBL_PTR(_size_11506)->dbl - DBL_PTR(_n_11504)->dbl);
            }
        }
        if (IS_ATOM_INT(_6472)) {
            _6473 = _6472 - 4;
            if ((long)((unsigned long)_6473 +(unsigned long) HIGH_BITS) >= 0){
                _6473 = NewDouble((double)_6473);
            }
        }
        else {
            _6473 = NewDouble(DBL_PTR(_6472)->dbl - (double)4);
        }
        DeRef(_6472);
        _6472 = NOVALUE;
        _0 = _addr_11508;
        if (IS_ATOM_INT(_addr_11508) && IS_ATOM_INT(_6473)) {
            _addr_11508 = _addr_11508 + _6473;
            if ((long)((unsigned long)_addr_11508 + (unsigned long)HIGH_BITS) >= 0) 
            _addr_11508 = NewDouble((double)_addr_11508);
        }
        else {
            if (IS_ATOM_INT(_addr_11508)) {
                _addr_11508 = NewDouble((double)_addr_11508 + DBL_PTR(_6473)->dbl);
            }
            else {
                if (IS_ATOM_INT(_6473)) {
                    _addr_11508 = NewDouble(DBL_PTR(_addr_11508)->dbl + (double)_6473);
                }
                else
                _addr_11508 = NewDouble(DBL_PTR(_addr_11508)->dbl + DBL_PTR(_6473)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_6473);
        _6473 = NOVALUE;

        /** 					io:seek(current_db, addr - 4) */
        if (IS_ATOM_INT(_addr_11508)) {
            _6475 = _addr_11508 - 4;
            if ((long)((unsigned long)_6475 +(unsigned long) HIGH_BITS) >= 0){
                _6475 = NewDouble((double)_6475);
            }
        }
        else {
            _6475 = NewDouble(DBL_PTR(_addr_11508)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_243_11556);
        _pos_inlined_seek_at_243_11556 = _6475;
        _6475 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_243_11556);
        DeRef(_seek_1__tmp_at246_11558);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _pos_inlined_seek_at_243_11556;
        _seek_1__tmp_at246_11558 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_246_11557 = machine(19, _seek_1__tmp_at246_11558);
        DeRef(_pos_inlined_seek_at_243_11556);
        _pos_inlined_seek_at_243_11556 = NOVALUE;
        DeRef(_seek_1__tmp_at246_11558);
        _seek_1__tmp_at246_11558 = NOVALUE;

        /** 					put4(n+4)*/
        if (IS_ATOM_INT(_n_11504)) {
            _6476 = _n_11504 + 4;
            if ((long)((unsigned long)_6476 + (unsigned long)HIGH_BITS) >= 0) 
            _6476 = NewDouble((double)_6476);
        }
        else {
            _6476 = NewDouble(DBL_PTR(_n_11504)->dbl + (double)4);
        }
        DeRef(_x_inlined_put4_at_265_11561);
        _x_inlined_put4_at_265_11561 = _6476;
        _6476 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_2mem0_10922)){
            poke4_addr = (unsigned long *)_2mem0_10922;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_265_11561)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_265_11561;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_265_11561)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at268_11562);
        _1 = (int)SEQ_PTR(_2memseq_11142);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at268_11562 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_2current_db_10880, _put4_1__tmp_at268_11562); // DJP 

        /** end procedure*/
        goto L8; // [290] 293
L8: 
        DeRef(_x_inlined_put4_at_265_11561);
        _x_inlined_put4_at_265_11561 = NOVALUE;
        DeRefi(_put4_1__tmp_at268_11562);
        _put4_1__tmp_at268_11562 = NOVALUE;
        goto L9; // [295] 468
L5: 

        /** 					remaining = io:get_bytes(current_db, (free_count-i) * 8)*/
        _6477 = _free_count_11509 - _i_11523;
        if ((long)((unsigned long)_6477 +(unsigned long) HIGH_BITS) >= 0){
            _6477 = NewDouble((double)_6477);
        }
        if (IS_ATOM_INT(_6477)) {
            if (_6477 == (short)_6477)
            _6478 = _6477 * 8;
            else
            _6478 = NewDouble(_6477 * (double)8);
        }
        else {
            _6478 = NewDouble(DBL_PTR(_6477)->dbl * (double)8);
        }
        DeRef(_6477);
        _6477 = NOVALUE;
        _0 = _remaining_11510;
        _remaining_11510 = _15get_bytes(_2current_db_10880, _6478);
        DeRef(_0);
        _6478 = NOVALUE;

        /** 					io:seek(current_db, free_list+8*(i-1))*/
        _6480 = _i_11523 - 1;
        if (_6480 <= INT15)
        _6481 = 8 * _6480;
        else
        _6481 = NewDouble(8 * (double)_6480);
        _6480 = NOVALUE;
        if (IS_ATOM_INT(_free_list_11505) && IS_ATOM_INT(_6481)) {
            _6482 = _free_list_11505 + _6481;
            if ((long)((unsigned long)_6482 + (unsigned long)HIGH_BITS) >= 0) 
            _6482 = NewDouble((double)_6482);
        }
        else {
            if (IS_ATOM_INT(_free_list_11505)) {
                _6482 = NewDouble((double)_free_list_11505 + DBL_PTR(_6481)->dbl);
            }
            else {
                if (IS_ATOM_INT(_6481)) {
                    _6482 = NewDouble(DBL_PTR(_free_list_11505)->dbl + (double)_6481);
                }
                else
                _6482 = NewDouble(DBL_PTR(_free_list_11505)->dbl + DBL_PTR(_6481)->dbl);
            }
        }
        DeRef(_6481);
        _6481 = NOVALUE;
        DeRef(_pos_inlined_seek_at_332_11571);
        _pos_inlined_seek_at_332_11571 = _6482;
        _6482 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_332_11571);
        DeRef(_seek_1__tmp_at335_11573);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _pos_inlined_seek_at_332_11571;
        _seek_1__tmp_at335_11573 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_335_11572 = machine(19, _seek_1__tmp_at335_11573);
        DeRef(_pos_inlined_seek_at_332_11571);
        _pos_inlined_seek_at_332_11571 = NOVALUE;
        DeRef(_seek_1__tmp_at335_11573);
        _seek_1__tmp_at335_11573 = NOVALUE;

        /** 					putn(remaining)*/

        /** 	puts(current_db, s)*/
        EPuts(_2current_db_10880, _remaining_11510); // DJP 

        /** end procedure*/
        goto LA; // [360] 363
LA: 

        /** 					io:seek(current_db, FREE_COUNT)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        DeRefi(_seek_1__tmp_at366_11577);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = 7;
        _seek_1__tmp_at366_11577 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_366_11576 = machine(19, _seek_1__tmp_at366_11577);
        DeRefi(_seek_1__tmp_at366_11577);
        _seek_1__tmp_at366_11577 = NOVALUE;

        /** 					put4(free_count-1)*/
        _6483 = _free_count_11509 - 1;
        if ((long)((unsigned long)_6483 +(unsigned long) HIGH_BITS) >= 0){
            _6483 = NewDouble((double)_6483);
        }
        DeRef(_x_inlined_put4_at_385_11580);
        _x_inlined_put4_at_385_11580 = _6483;
        _6483 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_2mem0_10922)){
            poke4_addr = (unsigned long *)_2mem0_10922;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_385_11580)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_385_11580;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_385_11580)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at388_11581);
        _1 = (int)SEQ_PTR(_2memseq_11142);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at388_11581 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_2current_db_10880, _put4_1__tmp_at388_11581); // DJP 

        /** end procedure*/
        goto LB; // [410] 413
LB: 
        DeRef(_x_inlined_put4_at_385_11580);
        _x_inlined_put4_at_385_11580 = NOVALUE;
        DeRefi(_put4_1__tmp_at388_11581);
        _put4_1__tmp_at388_11581 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_11508)) {
            _6484 = _addr_11508 - 4;
            if ((long)((unsigned long)_6484 +(unsigned long) HIGH_BITS) >= 0){
                _6484 = NewDouble((double)_6484);
            }
        }
        else {
            _6484 = NewDouble(DBL_PTR(_addr_11508)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_422_11584);
        _pos_inlined_seek_at_422_11584 = _6484;
        _6484 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_422_11584);
        DeRef(_seek_1__tmp_at425_11586);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _pos_inlined_seek_at_422_11584;
        _seek_1__tmp_at425_11586 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_425_11585 = machine(19, _seek_1__tmp_at425_11586);
        DeRef(_pos_inlined_seek_at_422_11584);
        _pos_inlined_seek_at_422_11584 = NOVALUE;
        DeRef(_seek_1__tmp_at425_11586);
        _seek_1__tmp_at425_11586 = NOVALUE;

        /** 					put4(size) -- in case size was not updated by db_free()*/

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_2mem0_10922)){
            poke4_addr = (unsigned long *)_2mem0_10922;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        }
        if (IS_ATOM_INT(_size_11506)) {
            *poke4_addr = (unsigned long)_size_11506;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_size_11506)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at440_11588);
        _1 = (int)SEQ_PTR(_2memseq_11142);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at440_11588 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_2current_db_10880, _put4_1__tmp_at440_11588); // DJP 

        /** end procedure*/
        goto LC; // [462] 465
LC: 
        DeRefi(_put4_1__tmp_at440_11588);
        _put4_1__tmp_at440_11588 = NOVALUE;
L9: 

        /** 				return addr*/
        DeRef(_n_11504);
        DeRef(_free_list_11505);
        DeRef(_size_11506);
        DeRef(_size_ptr_11507);
        DeRef(_remaining_11510);
        return _addr_11508;
L4: 

        /** 			size_ptr += 8*/
        _0 = _size_ptr_11507;
        if (IS_ATOM_INT(_size_ptr_11507)) {
            _size_ptr_11507 = _size_ptr_11507 + 8;
            if ((long)((unsigned long)_size_ptr_11507 + (unsigned long)HIGH_BITS) >= 0) 
            _size_ptr_11507 = NewDouble((double)_size_ptr_11507);
        }
        else {
            _size_ptr_11507 = NewDouble(DBL_PTR(_size_ptr_11507)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _i_11523 = _i_11523 + 1;
        goto L2; // [483] 73
L3: 
        ;
    }
L1: 

    /** 	io:seek(current_db, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at492_11592);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at492_11592 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_492_11591 = machine(19, _seek_1__tmp_at492_11592);
    DeRefi(_seek_1__tmp_at492_11592);
    _seek_1__tmp_at492_11592 = NOVALUE;

    /** 	put4(n+4)*/
    if (IS_ATOM_INT(_n_11504)) {
        _6486 = _n_11504 + 4;
        if ((long)((unsigned long)_6486 + (unsigned long)HIGH_BITS) >= 0) 
        _6486 = NewDouble((double)_6486);
    }
    else {
        _6486 = NewDouble(DBL_PTR(_n_11504)->dbl + (double)4);
    }
    DeRef(_x_inlined_put4_at_511_11595);
    _x_inlined_put4_at_511_11595 = _6486;
    _6486 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_511_11595)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_511_11595;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_511_11595)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at514_11596);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at514_11596 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at514_11596); // DJP 

    /** end procedure*/
    goto LD; // [536] 539
LD: 
    DeRef(_x_inlined_put4_at_511_11595);
    _x_inlined_put4_at_511_11595 = NOVALUE;
    DeRefi(_put4_1__tmp_at514_11596);
    _put4_1__tmp_at514_11596 = NOVALUE;

    /** 	return io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_544_11598);
    _where_inlined_where_at_544_11598 = machine(20, _2current_db_10880);
    DeRef(_n_11504);
    DeRef(_free_list_11505);
    DeRef(_size_11506);
    DeRef(_size_ptr_11507);
    DeRef(_addr_11508);
    DeRef(_remaining_11510);
    return _where_inlined_where_at_544_11598;
    ;
}


void _2db_free(int _p_11601)
{
    int _psize_11602 = NOVALUE;
    int _i_11603 = NOVALUE;
    int _size_11604 = NOVALUE;
    int _addr_11605 = NOVALUE;
    int _free_list_11606 = NOVALUE;
    int _free_list_space_11607 = NOVALUE;
    int _new_space_11608 = NOVALUE;
    int _to_be_freed_11609 = NOVALUE;
    int _prev_addr_11610 = NOVALUE;
    int _prev_size_11611 = NOVALUE;
    int _free_count_11612 = NOVALUE;
    int _remaining_11613 = NOVALUE;
    int _seek_1__tmp_at11_11618 = NOVALUE;
    int _seek_inlined_seek_at_11_11617 = NOVALUE;
    int _pos_inlined_seek_at_8_11616 = NOVALUE;
    int _seek_1__tmp_at33_11622 = NOVALUE;
    int _seek_inlined_seek_at_33_11621 = NOVALUE;
    int _seek_1__tmp_at71_11629 = NOVALUE;
    int _seek_inlined_seek_at_71_11628 = NOVALUE;
    int _pos_inlined_seek_at_68_11627 = NOVALUE;
    int _seek_1__tmp_at135_11642 = NOVALUE;
    int _seek_inlined_seek_at_135_11641 = NOVALUE;
    int _seek_1__tmp_at161_11646 = NOVALUE;
    int _seek_inlined_seek_at_161_11645 = NOVALUE;
    int _put4_1__tmp_at176_11648 = NOVALUE;
    int _seek_1__tmp_at206_11651 = NOVALUE;
    int _seek_inlined_seek_at_206_11650 = NOVALUE;
    int _seek_1__tmp_at238_11656 = NOVALUE;
    int _seek_inlined_seek_at_238_11655 = NOVALUE;
    int _s_inlined_putn_at_278_11662 = NOVALUE;
    int _seek_1__tmp_at301_11665 = NOVALUE;
    int _seek_inlined_seek_at_301_11664 = NOVALUE;
    int _seek_1__tmp_at434_11686 = NOVALUE;
    int _seek_inlined_seek_at_434_11685 = NOVALUE;
    int _pos_inlined_seek_at_431_11684 = NOVALUE;
    int _put4_1__tmp_at486_11696 = NOVALUE;
    int _x_inlined_put4_at_483_11695 = NOVALUE;
    int _seek_1__tmp_at527_11702 = NOVALUE;
    int _seek_inlined_seek_at_527_11701 = NOVALUE;
    int _pos_inlined_seek_at_524_11700 = NOVALUE;
    int _seek_1__tmp_at578_11712 = NOVALUE;
    int _seek_inlined_seek_at_578_11711 = NOVALUE;
    int _pos_inlined_seek_at_575_11710 = NOVALUE;
    int _seek_1__tmp_at617_11717 = NOVALUE;
    int _seek_inlined_seek_at_617_11716 = NOVALUE;
    int _put4_1__tmp_at632_11719 = NOVALUE;
    int _put4_1__tmp_at670_11724 = NOVALUE;
    int _x_inlined_put4_at_667_11723 = NOVALUE;
    int _seek_1__tmp_at743_11736 = NOVALUE;
    int _seek_inlined_seek_at_743_11735 = NOVALUE;
    int _pos_inlined_seek_at_740_11734 = NOVALUE;
    int _put4_1__tmp_at758_11738 = NOVALUE;
    int _put4_1__tmp_at795_11742 = NOVALUE;
    int _x_inlined_put4_at_792_11741 = NOVALUE;
    int _seek_1__tmp_at843_11750 = NOVALUE;
    int _seek_inlined_seek_at_843_11749 = NOVALUE;
    int _pos_inlined_seek_at_840_11748 = NOVALUE;
    int _seek_1__tmp_at891_11758 = NOVALUE;
    int _seek_inlined_seek_at_891_11757 = NOVALUE;
    int _put4_1__tmp_at906_11760 = NOVALUE;
    int _seek_1__tmp_at951_11767 = NOVALUE;
    int _seek_inlined_seek_at_951_11766 = NOVALUE;
    int _pos_inlined_seek_at_948_11765 = NOVALUE;
    int _put4_1__tmp_at966_11769 = NOVALUE;
    int _put4_1__tmp_at994_11771 = NOVALUE;
    int _6555 = NOVALUE;
    int _6554 = NOVALUE;
    int _6553 = NOVALUE;
    int _6552 = NOVALUE;
    int _6549 = NOVALUE;
    int _6548 = NOVALUE;
    int _6547 = NOVALUE;
    int _6546 = NOVALUE;
    int _6545 = NOVALUE;
    int _6544 = NOVALUE;
    int _6543 = NOVALUE;
    int _6542 = NOVALUE;
    int _6541 = NOVALUE;
    int _6540 = NOVALUE;
    int _6539 = NOVALUE;
    int _6538 = NOVALUE;
    int _6537 = NOVALUE;
    int _6536 = NOVALUE;
    int _6535 = NOVALUE;
    int _6533 = NOVALUE;
    int _6532 = NOVALUE;
    int _6531 = NOVALUE;
    int _6529 = NOVALUE;
    int _6528 = NOVALUE;
    int _6527 = NOVALUE;
    int _6526 = NOVALUE;
    int _6525 = NOVALUE;
    int _6524 = NOVALUE;
    int _6523 = NOVALUE;
    int _6522 = NOVALUE;
    int _6521 = NOVALUE;
    int _6520 = NOVALUE;
    int _6519 = NOVALUE;
    int _6518 = NOVALUE;
    int _6517 = NOVALUE;
    int _6516 = NOVALUE;
    int _6515 = NOVALUE;
    int _6514 = NOVALUE;
    int _6513 = NOVALUE;
    int _6512 = NOVALUE;
    int _6506 = NOVALUE;
    int _6505 = NOVALUE;
    int _6504 = NOVALUE;
    int _6502 = NOVALUE;
    int _6498 = NOVALUE;
    int _6497 = NOVALUE;
    int _6495 = NOVALUE;
    int _6494 = NOVALUE;
    int _6492 = NOVALUE;
    int _6491 = NOVALUE;
    int _6487 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, p-4)*/
    if (IS_ATOM_INT(_p_11601)) {
        _6487 = _p_11601 - 4;
        if ((long)((unsigned long)_6487 +(unsigned long) HIGH_BITS) >= 0){
            _6487 = NewDouble((double)_6487);
        }
    }
    else {
        _6487 = NewDouble(DBL_PTR(_p_11601)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_11616);
    _pos_inlined_seek_at_8_11616 = _6487;
    _6487 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_11616);
    DeRef(_seek_1__tmp_at11_11618);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_11616;
    _seek_1__tmp_at11_11618 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_11617 = machine(19, _seek_1__tmp_at11_11618);
    DeRef(_pos_inlined_seek_at_8_11616);
    _pos_inlined_seek_at_8_11616 = NOVALUE;
    DeRef(_seek_1__tmp_at11_11618);
    _seek_1__tmp_at11_11618 = NOVALUE;

    /** 	psize = get4()*/
    _0 = _psize_11602;
    _psize_11602 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at33_11622);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at33_11622 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_33_11621 = machine(19, _seek_1__tmp_at33_11622);
    DeRefi(_seek_1__tmp_at33_11622);
    _seek_1__tmp_at33_11622 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_11612 = _2get4();
    if (!IS_ATOM_INT(_free_count_11612)) {
        _1 = (long)(DBL_PTR(_free_count_11612)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_11612)) && (DBL_PTR(_free_count_11612)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_11612);
        _free_count_11612 = _1;
    }

    /** 	free_list = get4()*/
    _0 = _free_list_11606;
    _free_list_11606 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, free_list - 4)*/
    if (IS_ATOM_INT(_free_list_11606)) {
        _6491 = _free_list_11606 - 4;
        if ((long)((unsigned long)_6491 +(unsigned long) HIGH_BITS) >= 0){
            _6491 = NewDouble((double)_6491);
        }
    }
    else {
        _6491 = NewDouble(DBL_PTR(_free_list_11606)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_68_11627);
    _pos_inlined_seek_at_68_11627 = _6491;
    _6491 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_68_11627);
    DeRef(_seek_1__tmp_at71_11629);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_68_11627;
    _seek_1__tmp_at71_11629 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_71_11628 = machine(19, _seek_1__tmp_at71_11629);
    DeRef(_pos_inlined_seek_at_68_11627);
    _pos_inlined_seek_at_68_11627 = NOVALUE;
    DeRef(_seek_1__tmp_at71_11629);
    _seek_1__tmp_at71_11629 = NOVALUE;

    /** 	free_list_space = get4()-4*/
    _6492 = _2get4();
    DeRef(_free_list_space_11607);
    if (IS_ATOM_INT(_6492)) {
        _free_list_space_11607 = _6492 - 4;
        if ((long)((unsigned long)_free_list_space_11607 +(unsigned long) HIGH_BITS) >= 0){
            _free_list_space_11607 = NewDouble((double)_free_list_space_11607);
        }
    }
    else {
        _free_list_space_11607 = binary_op(MINUS, _6492, 4);
    }
    DeRef(_6492);
    _6492 = NOVALUE;

    /** 	if free_list_space < 8 * (free_count+1) then*/
    _6494 = _free_count_11612 + 1;
    if (_6494 > MAXINT){
        _6494 = NewDouble((double)_6494);
    }
    if (IS_ATOM_INT(_6494)) {
        if (_6494 <= INT15 && _6494 >= -INT15)
        _6495 = 8 * _6494;
        else
        _6495 = NewDouble(8 * (double)_6494);
    }
    else {
        _6495 = NewDouble((double)8 * DBL_PTR(_6494)->dbl);
    }
    DeRef(_6494);
    _6494 = NOVALUE;
    if (binary_op_a(GREATEREQ, _free_list_space_11607, _6495)){
        DeRef(_6495);
        _6495 = NOVALUE;
        goto L1; // [104] 318
    }
    DeRef(_6495);
    _6495 = NOVALUE;

    /** 		new_space = floor(free_list_space + free_list_space / 2)*/
    if (IS_ATOM_INT(_free_list_space_11607)) {
        if (_free_list_space_11607 & 1) {
            _6497 = NewDouble((_free_list_space_11607 >> 1) + 0.5);
        }
        else
        _6497 = _free_list_space_11607 >> 1;
    }
    else {
        _6497 = binary_op(DIVIDE, _free_list_space_11607, 2);
    }
    if (IS_ATOM_INT(_free_list_space_11607) && IS_ATOM_INT(_6497)) {
        _6498 = _free_list_space_11607 + _6497;
        if ((long)((unsigned long)_6498 + (unsigned long)HIGH_BITS) >= 0) 
        _6498 = NewDouble((double)_6498);
    }
    else {
        if (IS_ATOM_INT(_free_list_space_11607)) {
            _6498 = NewDouble((double)_free_list_space_11607 + DBL_PTR(_6497)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6497)) {
                _6498 = NewDouble(DBL_PTR(_free_list_space_11607)->dbl + (double)_6497);
            }
            else
            _6498 = NewDouble(DBL_PTR(_free_list_space_11607)->dbl + DBL_PTR(_6497)->dbl);
        }
    }
    DeRef(_6497);
    _6497 = NOVALUE;
    DeRef(_new_space_11608);
    if (IS_ATOM_INT(_6498))
    _new_space_11608 = e_floor(_6498);
    else
    _new_space_11608 = unary_op(FLOOR, _6498);
    DeRef(_6498);
    _6498 = NOVALUE;

    /** 		to_be_freed = free_list*/
    Ref(_free_list_11606);
    DeRef(_to_be_freed_11609);
    _to_be_freed_11609 = _free_list_11606;

    /** 		free_list = db_allocate(new_space)*/
    Ref(_new_space_11608);
    _0 = _free_list_11606;
    _free_list_11606 = _2db_allocate(_new_space_11608);
    DeRef(_0);

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at135_11642);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at135_11642 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_135_11641 = machine(19, _seek_1__tmp_at135_11642);
    DeRefi(_seek_1__tmp_at135_11642);
    _seek_1__tmp_at135_11642 = NOVALUE;

    /** 		free_count = get4() -- db_allocate may have changed it*/
    _free_count_11612 = _2get4();
    if (!IS_ATOM_INT(_free_count_11612)) {
        _1 = (long)(DBL_PTR(_free_count_11612)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_11612)) && (DBL_PTR(_free_count_11612)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_11612);
        _free_count_11612 = _1;
    }

    /** 		io:seek(current_db, FREE_LIST)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at161_11646);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 11;
    _seek_1__tmp_at161_11646 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_161_11645 = machine(19, _seek_1__tmp_at161_11646);
    DeRefi(_seek_1__tmp_at161_11646);
    _seek_1__tmp_at161_11646 = NOVALUE;

    /** 		put4(free_list)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_free_list_11606)) {
        *poke4_addr = (unsigned long)_free_list_11606;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_free_list_11606)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at176_11648);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at176_11648 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at176_11648); // DJP 

    /** end procedure*/
    goto L2; // [198] 201
L2: 
    DeRefi(_put4_1__tmp_at176_11648);
    _put4_1__tmp_at176_11648 = NOVALUE;

    /** 		io:seek(current_db, to_be_freed)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_to_be_freed_11609);
    DeRef(_seek_1__tmp_at206_11651);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _to_be_freed_11609;
    _seek_1__tmp_at206_11651 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_206_11650 = machine(19, _seek_1__tmp_at206_11651);
    DeRef(_seek_1__tmp_at206_11651);
    _seek_1__tmp_at206_11651 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, 8*free_count)*/
    if (_free_count_11612 <= INT15 && _free_count_11612 >= -INT15)
    _6502 = 8 * _free_count_11612;
    else
    _6502 = NewDouble(8 * (double)_free_count_11612);
    _0 = _remaining_11613;
    _remaining_11613 = _15get_bytes(_2current_db_10880, _6502);
    DeRef(_0);
    _6502 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_11606);
    DeRef(_seek_1__tmp_at238_11656);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _free_list_11606;
    _seek_1__tmp_at238_11656 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_238_11655 = machine(19, _seek_1__tmp_at238_11656);
    DeRef(_seek_1__tmp_at238_11656);
    _seek_1__tmp_at238_11656 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_11613); // DJP 

    /** end procedure*/
    goto L3; // [263] 266
L3: 

    /** 		putn(repeat(0, new_space-length(remaining)))*/
    if (IS_SEQUENCE(_remaining_11613)){
            _6504 = SEQ_PTR(_remaining_11613)->length;
    }
    else {
        _6504 = 1;
    }
    if (IS_ATOM_INT(_new_space_11608)) {
        _6505 = _new_space_11608 - _6504;
    }
    else {
        _6505 = NewDouble(DBL_PTR(_new_space_11608)->dbl - (double)_6504);
    }
    _6504 = NOVALUE;
    _6506 = Repeat(0, _6505);
    DeRef(_6505);
    _6505 = NOVALUE;
    DeRefi(_s_inlined_putn_at_278_11662);
    _s_inlined_putn_at_278_11662 = _6506;
    _6506 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_278_11662); // DJP 

    /** end procedure*/
    goto L4; // [293] 296
L4: 
    DeRefi(_s_inlined_putn_at_278_11662);
    _s_inlined_putn_at_278_11662 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_11606);
    DeRef(_seek_1__tmp_at301_11665);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _free_list_11606;
    _seek_1__tmp_at301_11665 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_301_11664 = machine(19, _seek_1__tmp_at301_11665);
    DeRef(_seek_1__tmp_at301_11665);
    _seek_1__tmp_at301_11665 = NOVALUE;
    goto L5; // [315] 324
L1: 

    /** 		new_space = 0*/
    DeRef(_new_space_11608);
    _new_space_11608 = 0;
L5: 

    /** 	i = 1*/
    DeRef(_i_11603);
    _i_11603 = 1;

    /** 	prev_addr = 0*/
    DeRef(_prev_addr_11610);
    _prev_addr_11610 = 0;

    /** 	prev_size = 0*/
    DeRef(_prev_size_11611);
    _prev_size_11611 = 0;

    /** 	while i <= free_count do*/
L6: 
    if (binary_op_a(GREATER, _i_11603, _free_count_11612)){
        goto L7; // [344] 390
    }

    /** 		addr = get4()*/
    _0 = _addr_11605;
    _addr_11605 = _2get4();
    DeRef(_0);

    /** 		size = get4()*/
    _0 = _size_11604;
    _size_11604 = _2get4();
    DeRef(_0);

    /** 		if p < addr then*/
    if (binary_op_a(GREATEREQ, _p_11601, _addr_11605)){
        goto L8; // [360] 369
    }

    /** 			exit*/
    goto L7; // [366] 390
L8: 

    /** 		prev_addr = addr*/
    Ref(_addr_11605);
    DeRef(_prev_addr_11610);
    _prev_addr_11610 = _addr_11605;

    /** 		prev_size = size*/
    Ref(_size_11604);
    DeRef(_prev_size_11611);
    _prev_size_11611 = _size_11604;

    /** 		i += 1*/
    _0 = _i_11603;
    if (IS_ATOM_INT(_i_11603)) {
        _i_11603 = _i_11603 + 1;
        if (_i_11603 > MAXINT){
            _i_11603 = NewDouble((double)_i_11603);
        }
    }
    else
    _i_11603 = binary_op(PLUS, 1, _i_11603);
    DeRef(_0);

    /** 	end while*/
    goto L6; // [387] 344
L7: 

    /** 	if i > 1 and prev_addr + prev_size = p then*/
    if (IS_ATOM_INT(_i_11603)) {
        _6512 = (_i_11603 > 1);
    }
    else {
        _6512 = (DBL_PTR(_i_11603)->dbl > (double)1);
    }
    if (_6512 == 0) {
        goto L9; // [396] 701
    }
    if (IS_ATOM_INT(_prev_addr_11610) && IS_ATOM_INT(_prev_size_11611)) {
        _6514 = _prev_addr_11610 + _prev_size_11611;
        if ((long)((unsigned long)_6514 + (unsigned long)HIGH_BITS) >= 0) 
        _6514 = NewDouble((double)_6514);
    }
    else {
        if (IS_ATOM_INT(_prev_addr_11610)) {
            _6514 = NewDouble((double)_prev_addr_11610 + DBL_PTR(_prev_size_11611)->dbl);
        }
        else {
            if (IS_ATOM_INT(_prev_size_11611)) {
                _6514 = NewDouble(DBL_PTR(_prev_addr_11610)->dbl + (double)_prev_size_11611);
            }
            else
            _6514 = NewDouble(DBL_PTR(_prev_addr_11610)->dbl + DBL_PTR(_prev_size_11611)->dbl);
        }
    }
    if (IS_ATOM_INT(_6514) && IS_ATOM_INT(_p_11601)) {
        _6515 = (_6514 == _p_11601);
    }
    else {
        if (IS_ATOM_INT(_6514)) {
            _6515 = ((double)_6514 == DBL_PTR(_p_11601)->dbl);
        }
        else {
            if (IS_ATOM_INT(_p_11601)) {
                _6515 = (DBL_PTR(_6514)->dbl == (double)_p_11601);
            }
            else
            _6515 = (DBL_PTR(_6514)->dbl == DBL_PTR(_p_11601)->dbl);
        }
    }
    DeRef(_6514);
    _6514 = NOVALUE;
    if (_6515 == 0)
    {
        DeRef(_6515);
        _6515 = NOVALUE;
        goto L9; // [409] 701
    }
    else{
        DeRef(_6515);
        _6515 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-2)*8+4)*/
    if (IS_ATOM_INT(_i_11603)) {
        _6516 = _i_11603 - 2;
        if ((long)((unsigned long)_6516 +(unsigned long) HIGH_BITS) >= 0){
            _6516 = NewDouble((double)_6516);
        }
    }
    else {
        _6516 = NewDouble(DBL_PTR(_i_11603)->dbl - (double)2);
    }
    if (IS_ATOM_INT(_6516)) {
        if (_6516 == (short)_6516)
        _6517 = _6516 * 8;
        else
        _6517 = NewDouble(_6516 * (double)8);
    }
    else {
        _6517 = NewDouble(DBL_PTR(_6516)->dbl * (double)8);
    }
    DeRef(_6516);
    _6516 = NOVALUE;
    if (IS_ATOM_INT(_free_list_11606) && IS_ATOM_INT(_6517)) {
        _6518 = _free_list_11606 + _6517;
        if ((long)((unsigned long)_6518 + (unsigned long)HIGH_BITS) >= 0) 
        _6518 = NewDouble((double)_6518);
    }
    else {
        if (IS_ATOM_INT(_free_list_11606)) {
            _6518 = NewDouble((double)_free_list_11606 + DBL_PTR(_6517)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6517)) {
                _6518 = NewDouble(DBL_PTR(_free_list_11606)->dbl + (double)_6517);
            }
            else
            _6518 = NewDouble(DBL_PTR(_free_list_11606)->dbl + DBL_PTR(_6517)->dbl);
        }
    }
    DeRef(_6517);
    _6517 = NOVALUE;
    if (IS_ATOM_INT(_6518)) {
        _6519 = _6518 + 4;
        if ((long)((unsigned long)_6519 + (unsigned long)HIGH_BITS) >= 0) 
        _6519 = NewDouble((double)_6519);
    }
    else {
        _6519 = NewDouble(DBL_PTR(_6518)->dbl + (double)4);
    }
    DeRef(_6518);
    _6518 = NOVALUE;
    DeRef(_pos_inlined_seek_at_431_11684);
    _pos_inlined_seek_at_431_11684 = _6519;
    _6519 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_431_11684);
    DeRef(_seek_1__tmp_at434_11686);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_431_11684;
    _seek_1__tmp_at434_11686 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_434_11685 = machine(19, _seek_1__tmp_at434_11686);
    DeRef(_pos_inlined_seek_at_431_11684);
    _pos_inlined_seek_at_431_11684 = NOVALUE;
    DeRef(_seek_1__tmp_at434_11686);
    _seek_1__tmp_at434_11686 = NOVALUE;

    /** 		if i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_11603)) {
        _6520 = (_i_11603 < _free_count_11612);
    }
    else {
        _6520 = (DBL_PTR(_i_11603)->dbl < (double)_free_count_11612);
    }
    if (_6520 == 0) {
        goto LA; // [454] 662
    }
    if (IS_ATOM_INT(_p_11601) && IS_ATOM_INT(_psize_11602)) {
        _6522 = _p_11601 + _psize_11602;
        if ((long)((unsigned long)_6522 + (unsigned long)HIGH_BITS) >= 0) 
        _6522 = NewDouble((double)_6522);
    }
    else {
        if (IS_ATOM_INT(_p_11601)) {
            _6522 = NewDouble((double)_p_11601 + DBL_PTR(_psize_11602)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_11602)) {
                _6522 = NewDouble(DBL_PTR(_p_11601)->dbl + (double)_psize_11602);
            }
            else
            _6522 = NewDouble(DBL_PTR(_p_11601)->dbl + DBL_PTR(_psize_11602)->dbl);
        }
    }
    if (IS_ATOM_INT(_6522) && IS_ATOM_INT(_addr_11605)) {
        _6523 = (_6522 == _addr_11605);
    }
    else {
        if (IS_ATOM_INT(_6522)) {
            _6523 = ((double)_6522 == DBL_PTR(_addr_11605)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_11605)) {
                _6523 = (DBL_PTR(_6522)->dbl == (double)_addr_11605);
            }
            else
            _6523 = (DBL_PTR(_6522)->dbl == DBL_PTR(_addr_11605)->dbl);
        }
    }
    DeRef(_6522);
    _6522 = NOVALUE;
    if (_6523 == 0)
    {
        DeRef(_6523);
        _6523 = NOVALUE;
        goto LA; // [469] 662
    }
    else{
        DeRef(_6523);
        _6523 = NOVALUE;
    }

    /** 			put4(prev_size+psize+size) -- update size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_11611) && IS_ATOM_INT(_psize_11602)) {
        _6524 = _prev_size_11611 + _psize_11602;
        if ((long)((unsigned long)_6524 + (unsigned long)HIGH_BITS) >= 0) 
        _6524 = NewDouble((double)_6524);
    }
    else {
        if (IS_ATOM_INT(_prev_size_11611)) {
            _6524 = NewDouble((double)_prev_size_11611 + DBL_PTR(_psize_11602)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_11602)) {
                _6524 = NewDouble(DBL_PTR(_prev_size_11611)->dbl + (double)_psize_11602);
            }
            else
            _6524 = NewDouble(DBL_PTR(_prev_size_11611)->dbl + DBL_PTR(_psize_11602)->dbl);
        }
    }
    if (IS_ATOM_INT(_6524) && IS_ATOM_INT(_size_11604)) {
        _6525 = _6524 + _size_11604;
        if ((long)((unsigned long)_6525 + (unsigned long)HIGH_BITS) >= 0) 
        _6525 = NewDouble((double)_6525);
    }
    else {
        if (IS_ATOM_INT(_6524)) {
            _6525 = NewDouble((double)_6524 + DBL_PTR(_size_11604)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_11604)) {
                _6525 = NewDouble(DBL_PTR(_6524)->dbl + (double)_size_11604);
            }
            else
            _6525 = NewDouble(DBL_PTR(_6524)->dbl + DBL_PTR(_size_11604)->dbl);
        }
    }
    DeRef(_6524);
    _6524 = NOVALUE;
    DeRef(_x_inlined_put4_at_483_11695);
    _x_inlined_put4_at_483_11695 = _6525;
    _6525 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_483_11695)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_483_11695;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_483_11695)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at486_11696);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at486_11696 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at486_11696); // DJP 

    /** end procedure*/
    goto LB; // [508] 511
LB: 
    DeRef(_x_inlined_put4_at_483_11695);
    _x_inlined_put4_at_483_11695 = NOVALUE;
    DeRefi(_put4_1__tmp_at486_11696);
    _put4_1__tmp_at486_11696 = NOVALUE;

    /** 			io:seek(current_db, free_list+i*8)*/
    if (IS_ATOM_INT(_i_11603)) {
        if (_i_11603 == (short)_i_11603)
        _6526 = _i_11603 * 8;
        else
        _6526 = NewDouble(_i_11603 * (double)8);
    }
    else {
        _6526 = NewDouble(DBL_PTR(_i_11603)->dbl * (double)8);
    }
    if (IS_ATOM_INT(_free_list_11606) && IS_ATOM_INT(_6526)) {
        _6527 = _free_list_11606 + _6526;
        if ((long)((unsigned long)_6527 + (unsigned long)HIGH_BITS) >= 0) 
        _6527 = NewDouble((double)_6527);
    }
    else {
        if (IS_ATOM_INT(_free_list_11606)) {
            _6527 = NewDouble((double)_free_list_11606 + DBL_PTR(_6526)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6526)) {
                _6527 = NewDouble(DBL_PTR(_free_list_11606)->dbl + (double)_6526);
            }
            else
            _6527 = NewDouble(DBL_PTR(_free_list_11606)->dbl + DBL_PTR(_6526)->dbl);
        }
    }
    DeRef(_6526);
    _6526 = NOVALUE;
    DeRef(_pos_inlined_seek_at_524_11700);
    _pos_inlined_seek_at_524_11700 = _6527;
    _6527 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_524_11700);
    DeRef(_seek_1__tmp_at527_11702);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_524_11700;
    _seek_1__tmp_at527_11702 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_527_11701 = machine(19, _seek_1__tmp_at527_11702);
    DeRef(_pos_inlined_seek_at_524_11700);
    _pos_inlined_seek_at_524_11700 = NOVALUE;
    DeRef(_seek_1__tmp_at527_11702);
    _seek_1__tmp_at527_11702 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, (free_count-i)*8)*/
    if (IS_ATOM_INT(_i_11603)) {
        _6528 = _free_count_11612 - _i_11603;
        if ((long)((unsigned long)_6528 +(unsigned long) HIGH_BITS) >= 0){
            _6528 = NewDouble((double)_6528);
        }
    }
    else {
        _6528 = NewDouble((double)_free_count_11612 - DBL_PTR(_i_11603)->dbl);
    }
    if (IS_ATOM_INT(_6528)) {
        if (_6528 == (short)_6528)
        _6529 = _6528 * 8;
        else
        _6529 = NewDouble(_6528 * (double)8);
    }
    else {
        _6529 = NewDouble(DBL_PTR(_6528)->dbl * (double)8);
    }
    DeRef(_6528);
    _6528 = NOVALUE;
    _0 = _remaining_11613;
    _remaining_11613 = _15get_bytes(_2current_db_10880, _6529);
    DeRef(_0);
    _6529 = NOVALUE;

    /** 			io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_11603)) {
        _6531 = _i_11603 - 1;
        if ((long)((unsigned long)_6531 +(unsigned long) HIGH_BITS) >= 0){
            _6531 = NewDouble((double)_6531);
        }
    }
    else {
        _6531 = NewDouble(DBL_PTR(_i_11603)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6531)) {
        if (_6531 == (short)_6531)
        _6532 = _6531 * 8;
        else
        _6532 = NewDouble(_6531 * (double)8);
    }
    else {
        _6532 = NewDouble(DBL_PTR(_6531)->dbl * (double)8);
    }
    DeRef(_6531);
    _6531 = NOVALUE;
    if (IS_ATOM_INT(_free_list_11606) && IS_ATOM_INT(_6532)) {
        _6533 = _free_list_11606 + _6532;
        if ((long)((unsigned long)_6533 + (unsigned long)HIGH_BITS) >= 0) 
        _6533 = NewDouble((double)_6533);
    }
    else {
        if (IS_ATOM_INT(_free_list_11606)) {
            _6533 = NewDouble((double)_free_list_11606 + DBL_PTR(_6532)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6532)) {
                _6533 = NewDouble(DBL_PTR(_free_list_11606)->dbl + (double)_6532);
            }
            else
            _6533 = NewDouble(DBL_PTR(_free_list_11606)->dbl + DBL_PTR(_6532)->dbl);
        }
    }
    DeRef(_6532);
    _6532 = NOVALUE;
    DeRef(_pos_inlined_seek_at_575_11710);
    _pos_inlined_seek_at_575_11710 = _6533;
    _6533 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_575_11710);
    DeRef(_seek_1__tmp_at578_11712);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_575_11710;
    _seek_1__tmp_at578_11712 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_578_11711 = machine(19, _seek_1__tmp_at578_11712);
    DeRef(_pos_inlined_seek_at_575_11710);
    _pos_inlined_seek_at_575_11710 = NOVALUE;
    DeRef(_seek_1__tmp_at578_11712);
    _seek_1__tmp_at578_11712 = NOVALUE;

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_11613); // DJP 

    /** end procedure*/
    goto LC; // [603] 606
LC: 

    /** 			free_count -= 1*/
    _free_count_11612 = _free_count_11612 - 1;

    /** 			io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at617_11717);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at617_11717 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_617_11716 = machine(19, _seek_1__tmp_at617_11717);
    DeRefi(_seek_1__tmp_at617_11717);
    _seek_1__tmp_at617_11717 = NOVALUE;

    /** 			put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_11612;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at632_11719);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at632_11719 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at632_11719); // DJP 

    /** end procedure*/
    goto LD; // [654] 657
LD: 
    DeRefi(_put4_1__tmp_at632_11719);
    _put4_1__tmp_at632_11719 = NOVALUE;
    goto LE; // [659] 1036
LA: 

    /** 			put4(prev_size+psize) -- increase previous size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_11611) && IS_ATOM_INT(_psize_11602)) {
        _6535 = _prev_size_11611 + _psize_11602;
        if ((long)((unsigned long)_6535 + (unsigned long)HIGH_BITS) >= 0) 
        _6535 = NewDouble((double)_6535);
    }
    else {
        if (IS_ATOM_INT(_prev_size_11611)) {
            _6535 = NewDouble((double)_prev_size_11611 + DBL_PTR(_psize_11602)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_11602)) {
                _6535 = NewDouble(DBL_PTR(_prev_size_11611)->dbl + (double)_psize_11602);
            }
            else
            _6535 = NewDouble(DBL_PTR(_prev_size_11611)->dbl + DBL_PTR(_psize_11602)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_667_11723);
    _x_inlined_put4_at_667_11723 = _6535;
    _6535 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_667_11723)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_667_11723;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_667_11723)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at670_11724);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at670_11724 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at670_11724); // DJP 

    /** end procedure*/
    goto LF; // [692] 695
LF: 
    DeRef(_x_inlined_put4_at_667_11723);
    _x_inlined_put4_at_667_11723 = NOVALUE;
    DeRefi(_put4_1__tmp_at670_11724);
    _put4_1__tmp_at670_11724 = NOVALUE;
    goto LE; // [698] 1036
L9: 

    /** 	elsif i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_11603)) {
        _6536 = (_i_11603 < _free_count_11612);
    }
    else {
        _6536 = (DBL_PTR(_i_11603)->dbl < (double)_free_count_11612);
    }
    if (_6536 == 0) {
        goto L10; // [707] 825
    }
    if (IS_ATOM_INT(_p_11601) && IS_ATOM_INT(_psize_11602)) {
        _6538 = _p_11601 + _psize_11602;
        if ((long)((unsigned long)_6538 + (unsigned long)HIGH_BITS) >= 0) 
        _6538 = NewDouble((double)_6538);
    }
    else {
        if (IS_ATOM_INT(_p_11601)) {
            _6538 = NewDouble((double)_p_11601 + DBL_PTR(_psize_11602)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_11602)) {
                _6538 = NewDouble(DBL_PTR(_p_11601)->dbl + (double)_psize_11602);
            }
            else
            _6538 = NewDouble(DBL_PTR(_p_11601)->dbl + DBL_PTR(_psize_11602)->dbl);
        }
    }
    if (IS_ATOM_INT(_6538) && IS_ATOM_INT(_addr_11605)) {
        _6539 = (_6538 == _addr_11605);
    }
    else {
        if (IS_ATOM_INT(_6538)) {
            _6539 = ((double)_6538 == DBL_PTR(_addr_11605)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_11605)) {
                _6539 = (DBL_PTR(_6538)->dbl == (double)_addr_11605);
            }
            else
            _6539 = (DBL_PTR(_6538)->dbl == DBL_PTR(_addr_11605)->dbl);
        }
    }
    DeRef(_6538);
    _6538 = NOVALUE;
    if (_6539 == 0)
    {
        DeRef(_6539);
        _6539 = NOVALUE;
        goto L10; // [722] 825
    }
    else{
        DeRef(_6539);
        _6539 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_11603)) {
        _6540 = _i_11603 - 1;
        if ((long)((unsigned long)_6540 +(unsigned long) HIGH_BITS) >= 0){
            _6540 = NewDouble((double)_6540);
        }
    }
    else {
        _6540 = NewDouble(DBL_PTR(_i_11603)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6540)) {
        if (_6540 == (short)_6540)
        _6541 = _6540 * 8;
        else
        _6541 = NewDouble(_6540 * (double)8);
    }
    else {
        _6541 = NewDouble(DBL_PTR(_6540)->dbl * (double)8);
    }
    DeRef(_6540);
    _6540 = NOVALUE;
    if (IS_ATOM_INT(_free_list_11606) && IS_ATOM_INT(_6541)) {
        _6542 = _free_list_11606 + _6541;
        if ((long)((unsigned long)_6542 + (unsigned long)HIGH_BITS) >= 0) 
        _6542 = NewDouble((double)_6542);
    }
    else {
        if (IS_ATOM_INT(_free_list_11606)) {
            _6542 = NewDouble((double)_free_list_11606 + DBL_PTR(_6541)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6541)) {
                _6542 = NewDouble(DBL_PTR(_free_list_11606)->dbl + (double)_6541);
            }
            else
            _6542 = NewDouble(DBL_PTR(_free_list_11606)->dbl + DBL_PTR(_6541)->dbl);
        }
    }
    DeRef(_6541);
    _6541 = NOVALUE;
    DeRef(_pos_inlined_seek_at_740_11734);
    _pos_inlined_seek_at_740_11734 = _6542;
    _6542 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_740_11734);
    DeRef(_seek_1__tmp_at743_11736);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_740_11734;
    _seek_1__tmp_at743_11736 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_743_11735 = machine(19, _seek_1__tmp_at743_11736);
    DeRef(_pos_inlined_seek_at_740_11734);
    _pos_inlined_seek_at_740_11734 = NOVALUE;
    DeRef(_seek_1__tmp_at743_11736);
    _seek_1__tmp_at743_11736 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_p_11601)) {
        *poke4_addr = (unsigned long)_p_11601;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_11601)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at758_11738);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at758_11738 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at758_11738); // DJP 

    /** end procedure*/
    goto L11; // [780] 783
L11: 
    DeRefi(_put4_1__tmp_at758_11738);
    _put4_1__tmp_at758_11738 = NOVALUE;

    /** 		put4(psize+size)*/
    if (IS_ATOM_INT(_psize_11602) && IS_ATOM_INT(_size_11604)) {
        _6543 = _psize_11602 + _size_11604;
        if ((long)((unsigned long)_6543 + (unsigned long)HIGH_BITS) >= 0) 
        _6543 = NewDouble((double)_6543);
    }
    else {
        if (IS_ATOM_INT(_psize_11602)) {
            _6543 = NewDouble((double)_psize_11602 + DBL_PTR(_size_11604)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_11604)) {
                _6543 = NewDouble(DBL_PTR(_psize_11602)->dbl + (double)_size_11604);
            }
            else
            _6543 = NewDouble(DBL_PTR(_psize_11602)->dbl + DBL_PTR(_size_11604)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_792_11741);
    _x_inlined_put4_at_792_11741 = _6543;
    _6543 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_792_11741)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_792_11741;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_792_11741)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at795_11742);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at795_11742 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at795_11742); // DJP 

    /** end procedure*/
    goto L12; // [817] 820
L12: 
    DeRef(_x_inlined_put4_at_792_11741);
    _x_inlined_put4_at_792_11741 = NOVALUE;
    DeRefi(_put4_1__tmp_at795_11742);
    _put4_1__tmp_at795_11742 = NOVALUE;
    goto LE; // [822] 1036
L10: 

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_11603)) {
        _6544 = _i_11603 - 1;
        if ((long)((unsigned long)_6544 +(unsigned long) HIGH_BITS) >= 0){
            _6544 = NewDouble((double)_6544);
        }
    }
    else {
        _6544 = NewDouble(DBL_PTR(_i_11603)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6544)) {
        if (_6544 == (short)_6544)
        _6545 = _6544 * 8;
        else
        _6545 = NewDouble(_6544 * (double)8);
    }
    else {
        _6545 = NewDouble(DBL_PTR(_6544)->dbl * (double)8);
    }
    DeRef(_6544);
    _6544 = NOVALUE;
    if (IS_ATOM_INT(_free_list_11606) && IS_ATOM_INT(_6545)) {
        _6546 = _free_list_11606 + _6545;
        if ((long)((unsigned long)_6546 + (unsigned long)HIGH_BITS) >= 0) 
        _6546 = NewDouble((double)_6546);
    }
    else {
        if (IS_ATOM_INT(_free_list_11606)) {
            _6546 = NewDouble((double)_free_list_11606 + DBL_PTR(_6545)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6545)) {
                _6546 = NewDouble(DBL_PTR(_free_list_11606)->dbl + (double)_6545);
            }
            else
            _6546 = NewDouble(DBL_PTR(_free_list_11606)->dbl + DBL_PTR(_6545)->dbl);
        }
    }
    DeRef(_6545);
    _6545 = NOVALUE;
    DeRef(_pos_inlined_seek_at_840_11748);
    _pos_inlined_seek_at_840_11748 = _6546;
    _6546 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_840_11748);
    DeRef(_seek_1__tmp_at843_11750);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_840_11748;
    _seek_1__tmp_at843_11750 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_843_11749 = machine(19, _seek_1__tmp_at843_11750);
    DeRef(_pos_inlined_seek_at_840_11748);
    _pos_inlined_seek_at_840_11748 = NOVALUE;
    DeRef(_seek_1__tmp_at843_11750);
    _seek_1__tmp_at843_11750 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (free_count-i+1)*8)*/
    if (IS_ATOM_INT(_i_11603)) {
        _6547 = _free_count_11612 - _i_11603;
        if ((long)((unsigned long)_6547 +(unsigned long) HIGH_BITS) >= 0){
            _6547 = NewDouble((double)_6547);
        }
    }
    else {
        _6547 = NewDouble((double)_free_count_11612 - DBL_PTR(_i_11603)->dbl);
    }
    if (IS_ATOM_INT(_6547)) {
        _6548 = _6547 + 1;
        if (_6548 > MAXINT){
            _6548 = NewDouble((double)_6548);
        }
    }
    else
    _6548 = binary_op(PLUS, 1, _6547);
    DeRef(_6547);
    _6547 = NOVALUE;
    if (IS_ATOM_INT(_6548)) {
        if (_6548 == (short)_6548)
        _6549 = _6548 * 8;
        else
        _6549 = NewDouble(_6548 * (double)8);
    }
    else {
        _6549 = NewDouble(DBL_PTR(_6548)->dbl * (double)8);
    }
    DeRef(_6548);
    _6548 = NOVALUE;
    _0 = _remaining_11613;
    _remaining_11613 = _15get_bytes(_2current_db_10880, _6549);
    DeRef(_0);
    _6549 = NOVALUE;

    /** 		free_count += 1*/
    _free_count_11612 = _free_count_11612 + 1;

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at891_11758);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at891_11758 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_891_11757 = machine(19, _seek_1__tmp_at891_11758);
    DeRefi(_seek_1__tmp_at891_11758);
    _seek_1__tmp_at891_11758 = NOVALUE;

    /** 		put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_11612;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at906_11760);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at906_11760 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at906_11760); // DJP 

    /** end procedure*/
    goto L13; // [928] 931
L13: 
    DeRefi(_put4_1__tmp_at906_11760);
    _put4_1__tmp_at906_11760 = NOVALUE;

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_11603)) {
        _6552 = _i_11603 - 1;
        if ((long)((unsigned long)_6552 +(unsigned long) HIGH_BITS) >= 0){
            _6552 = NewDouble((double)_6552);
        }
    }
    else {
        _6552 = NewDouble(DBL_PTR(_i_11603)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6552)) {
        if (_6552 == (short)_6552)
        _6553 = _6552 * 8;
        else
        _6553 = NewDouble(_6552 * (double)8);
    }
    else {
        _6553 = NewDouble(DBL_PTR(_6552)->dbl * (double)8);
    }
    DeRef(_6552);
    _6552 = NOVALUE;
    if (IS_ATOM_INT(_free_list_11606) && IS_ATOM_INT(_6553)) {
        _6554 = _free_list_11606 + _6553;
        if ((long)((unsigned long)_6554 + (unsigned long)HIGH_BITS) >= 0) 
        _6554 = NewDouble((double)_6554);
    }
    else {
        if (IS_ATOM_INT(_free_list_11606)) {
            _6554 = NewDouble((double)_free_list_11606 + DBL_PTR(_6553)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6553)) {
                _6554 = NewDouble(DBL_PTR(_free_list_11606)->dbl + (double)_6553);
            }
            else
            _6554 = NewDouble(DBL_PTR(_free_list_11606)->dbl + DBL_PTR(_6553)->dbl);
        }
    }
    DeRef(_6553);
    _6553 = NOVALUE;
    DeRef(_pos_inlined_seek_at_948_11765);
    _pos_inlined_seek_at_948_11765 = _6554;
    _6554 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_948_11765);
    DeRef(_seek_1__tmp_at951_11767);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_948_11765;
    _seek_1__tmp_at951_11767 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_951_11766 = machine(19, _seek_1__tmp_at951_11767);
    DeRef(_pos_inlined_seek_at_948_11765);
    _pos_inlined_seek_at_948_11765 = NOVALUE;
    DeRef(_seek_1__tmp_at951_11767);
    _seek_1__tmp_at951_11767 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_p_11601)) {
        *poke4_addr = (unsigned long)_p_11601;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_11601)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at966_11769);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at966_11769 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at966_11769); // DJP 

    /** end procedure*/
    goto L14; // [988] 991
L14: 
    DeRefi(_put4_1__tmp_at966_11769);
    _put4_1__tmp_at966_11769 = NOVALUE;

    /** 		put4(psize)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_psize_11602)) {
        *poke4_addr = (unsigned long)_psize_11602;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_psize_11602)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at994_11771);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at994_11771 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at994_11771); // DJP 

    /** end procedure*/
    goto L15; // [1016] 1019
L15: 
    DeRefi(_put4_1__tmp_at994_11771);
    _put4_1__tmp_at994_11771 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_11613); // DJP 

    /** end procedure*/
    goto L16; // [1032] 1035
L16: 
LE: 

    /** 	if new_space then*/
    if (_new_space_11608 == 0) {
        goto L17; // [1040] 1054
    }
    else {
        if (!IS_ATOM_INT(_new_space_11608) && DBL_PTR(_new_space_11608)->dbl == 0.0){
            goto L17; // [1040] 1054
        }
    }

    /** 		db_free(to_be_freed) -- free the old space*/
    Ref(_to_be_freed_11609);
    DeRef(_6555);
    _6555 = _to_be_freed_11609;
    _2db_free(_6555);
    _6555 = NOVALUE;
L17: 

    /** end procedure*/
    DeRef(_p_11601);
    DeRef(_psize_11602);
    DeRef(_i_11603);
    DeRef(_size_11604);
    DeRef(_addr_11605);
    DeRef(_free_list_11606);
    DeRef(_free_list_space_11607);
    DeRef(_new_space_11608);
    DeRef(_to_be_freed_11609);
    DeRef(_prev_addr_11610);
    DeRef(_prev_size_11611);
    DeRef(_remaining_11613);
    DeRef(_6512);
    _6512 = NOVALUE;
    DeRef(_6520);
    _6520 = NOVALUE;
    DeRef(_6536);
    _6536 = NOVALUE;
    return;
    ;
}


void _2save_keys()
{
    int _k_11777 = NOVALUE;
    int _6562 = NOVALUE;
    int _6558 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if caching_option = 1 then*/
    if (_2caching_option_10890 != 1)
    goto L1; // [5] 84

    /** 		if current_table_pos > 0 then*/
    if (binary_op_a(LESSEQ, _2current_table_pos_10881, 0)){
        goto L2; // [13] 83
    }

    /** 			k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_2current_table_pos_10881);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _2current_table_pos_10881;
    _6558 = MAKE_SEQ(_1);
    _k_11777 = find_from(_6558, _2cache_index_10889, 1);
    DeRefDS(_6558);
    _6558 = NOVALUE;

    /** 			if k != 0 then*/
    if (_k_11777 == 0)
    goto L3; // [38] 55

    /** 				key_cache[k] = key_pointers*/
    RefDS(_2key_pointers_10887);
    _2 = (int)SEQ_PTR(_2key_cache_10888);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _2key_cache_10888 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_11777);
    _1 = *(int *)_2;
    *(int *)_2 = _2key_pointers_10887;
    DeRef(_1);
    goto L4; // [52] 82
L3: 

    /** 				key_cache = append(key_cache, key_pointers)*/
    RefDS(_2key_pointers_10887);
    Append(&_2key_cache_10888, _2key_cache_10888, _2key_pointers_10887);

    /** 				cache_index = append(cache_index, {current_db, current_table_pos})*/
    Ref(_2current_table_pos_10881);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _2current_table_pos_10881;
    _6562 = MAKE_SEQ(_1);
    RefDS(_6562);
    Append(&_2cache_index_10889, _2cache_index_10889, _6562);
    DeRefDS(_6562);
    _6562 = NOVALUE;
L4: 
L2: 
L1: 

    /** end procedure*/
    return;
    ;
}


int _2db_connect(int _dbalias_11792, int _path_11793, int _dboptions_11794)
{
    int _lPos_11795 = NOVALUE;
    int _lOptions_11796 = NOVALUE;
    int _6619 = NOVALUE;
    int _6618 = NOVALUE;
    int _6617 = NOVALUE;
    int _6614 = NOVALUE;
    int _6611 = NOVALUE;
    int _6610 = NOVALUE;
    int _6609 = NOVALUE;
    int _6608 = NOVALUE;
    int _6606 = NOVALUE;
    int _6605 = NOVALUE;
    int _6604 = NOVALUE;
    int _6603 = NOVALUE;
    int _6602 = NOVALUE;
    int _6601 = NOVALUE;
    int _6600 = NOVALUE;
    int _6597 = NOVALUE;
    int _6596 = NOVALUE;
    int _6595 = NOVALUE;
    int _6593 = NOVALUE;
    int _6592 = NOVALUE;
    int _6591 = NOVALUE;
    int _6589 = NOVALUE;
    int _6588 = NOVALUE;
    int _6587 = NOVALUE;
    int _6586 = NOVALUE;
    int _6585 = NOVALUE;
    int _6583 = NOVALUE;
    int _6581 = NOVALUE;
    int _6580 = NOVALUE;
    int _6578 = NOVALUE;
    int _6577 = NOVALUE;
    int _6576 = NOVALUE;
    int _6575 = NOVALUE;
    int _6574 = NOVALUE;
    int _6573 = NOVALUE;
    int _6572 = NOVALUE;
    int _6570 = NOVALUE;
    int _6567 = NOVALUE;
    int _6565 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	lPos = find(dbalias, Known_Aliases)*/
    _lPos_11795 = find_from(_dbalias_11792, _2Known_Aliases_10901, 1);

    /** 	if lPos then*/
    if (_lPos_11795 == 0)
    {
        goto L1; // [20] 114
    }
    else{
    }

    /** 		if equal(dboptions, DISCONNECT) or find(DISCONNECT, dboptions) then*/
    if (_dboptions_11794 == _2DISCONNECT_10891)
    _6565 = 1;
    else if (IS_ATOM_INT(_dboptions_11794) && IS_ATOM_INT(_2DISCONNECT_10891))
    _6565 = 0;
    else
    _6565 = (compare(_dboptions_11794, _2DISCONNECT_10891) == 0);
    if (_6565 != 0) {
        goto L2; // [29] 43
    }
    _6567 = find_from(_2DISCONNECT_10891, _dboptions_11794, 1);
    if (_6567 == 0)
    {
        _6567 = NOVALUE;
        goto L3; // [39] 70
    }
    else{
        _6567 = NOVALUE;
    }
L2: 

    /** 			Known_Aliases = remove(Known_Aliases, lPos)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2Known_Aliases_10901);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPos_11795)) ? _lPos_11795 : (long)(DBL_PTR(_lPos_11795)->dbl);
        int stop = (IS_ATOM_INT(_lPos_11795)) ? _lPos_11795 : (long)(DBL_PTR(_lPos_11795)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2Known_Aliases_10901), start, &_2Known_Aliases_10901 );
            }
            else Tail(SEQ_PTR(_2Known_Aliases_10901), stop+1, &_2Known_Aliases_10901);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2Known_Aliases_10901), start, &_2Known_Aliases_10901);
        }
        else {
            assign_slice_seq = &assign_space;
            _2Known_Aliases_10901 = Remove_elements(start, stop, (SEQ_PTR(_2Known_Aliases_10901)->ref == 1));
        }
    }

    /** 			Alias_Details = remove(Alias_Details, lPos)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2Alias_Details_10902);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPos_11795)) ? _lPos_11795 : (long)(DBL_PTR(_lPos_11795)->dbl);
        int stop = (IS_ATOM_INT(_lPos_11795)) ? _lPos_11795 : (long)(DBL_PTR(_lPos_11795)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2Alias_Details_10902), start, &_2Alias_Details_10902 );
            }
            else Tail(SEQ_PTR(_2Alias_Details_10902), stop+1, &_2Alias_Details_10902);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2Alias_Details_10902), start, &_2Alias_Details_10902);
        }
        else {
            assign_slice_seq = &assign_space;
            _2Alias_Details_10902 = Remove_elements(start, stop, (SEQ_PTR(_2Alias_Details_10902)->ref == 1));
        }
    }

    /** 			return DB_OK*/
    DeRefDS(_dbalias_11792);
    DeRefDS(_path_11793);
    DeRefDS(_dboptions_11794);
    DeRef(_lOptions_11796);
    return 0;
L3: 

    /** 		if equal(dboptions, CONNECTION) or find(CONNECTION, dboptions) then*/
    if (_dboptions_11794 == _2CONNECTION_10899)
    _6570 = 1;
    else if (IS_ATOM_INT(_dboptions_11794) && IS_ATOM_INT(_2CONNECTION_10899))
    _6570 = 0;
    else
    _6570 = (compare(_dboptions_11794, _2CONNECTION_10899) == 0);
    if (_6570 != 0) {
        goto L4; // [76] 90
    }
    _6572 = find_from(_2CONNECTION_10899, _dboptions_11794, 1);
    if (_6572 == 0)
    {
        _6572 = NOVALUE;
        goto L5; // [86] 103
    }
    else{
        _6572 = NOVALUE;
    }
L4: 

    /** 			return Alias_Details[lPos]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6573 = (int)*(((s1_ptr)_2)->base + _lPos_11795);
    Ref(_6573);
    DeRefDS(_dbalias_11792);
    DeRefDS(_path_11793);
    DeRefDS(_dboptions_11794);
    DeRef(_lOptions_11796);
    return _6573;
L5: 

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_dbalias_11792);
    DeRefDS(_path_11793);
    DeRefDS(_dboptions_11794);
    DeRef(_lOptions_11796);
    _6573 = NOVALUE;
    return -1;
    goto L6; // [111] 169
L1: 

    /** 		if equal(dboptions, DISCONNECT) or find(DISCONNECT, dboptions) or*/
    if (_dboptions_11794 == _2DISCONNECT_10891)
    _6574 = 1;
    else if (IS_ATOM_INT(_dboptions_11794) && IS_ATOM_INT(_2DISCONNECT_10891))
    _6574 = 0;
    else
    _6574 = (compare(_dboptions_11794, _2DISCONNECT_10891) == 0);
    if (_6574 != 0) {
        _6575 = 1;
        goto L7; // [120] 133
    }
    _6576 = find_from(_2DISCONNECT_10891, _dboptions_11794, 1);
    _6575 = (_6576 != 0);
L7: 
    if (_6575 != 0) {
        _6577 = 1;
        goto L8; // [133] 145
    }
    if (_dboptions_11794 == _2CONNECTION_10899)
    _6578 = 1;
    else if (IS_ATOM_INT(_dboptions_11794) && IS_ATOM_INT(_2CONNECTION_10899))
    _6578 = 0;
    else
    _6578 = (compare(_dboptions_11794, _2CONNECTION_10899) == 0);
    _6577 = (_6578 != 0);
L8: 
    if (_6577 != 0) {
        goto L9; // [145] 159
    }
    _6580 = find_from(_2CONNECTION_10899, _dboptions_11794, 1);
    if (_6580 == 0)
    {
        _6580 = NOVALUE;
        goto LA; // [155] 168
    }
    else{
        _6580 = NOVALUE;
    }
L9: 

    /** 			return DB_OPEN_FAIL*/
    DeRefDS(_dbalias_11792);
    DeRefDS(_path_11793);
    DeRefDS(_dboptions_11794);
    DeRef(_lOptions_11796);
    _6573 = NOVALUE;
    return -1;
LA: 
L6: 

    /** 	if length(path) = 0 then*/
    if (IS_SEQUENCE(_path_11793)){
            _6581 = SEQ_PTR(_path_11793)->length;
    }
    else {
        _6581 = 1;
    }
    if (_6581 != 0)
    goto LB; // [174] 187

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_dbalias_11792);
    DeRefDS(_path_11793);
    DeRefDS(_dboptions_11794);
    DeRef(_lOptions_11796);
    _6573 = NOVALUE;
    return -1;
LB: 

    /** 	if types:string(dboptions) then*/
    RefDS(_dboptions_11794);
    _6583 = _3string(_dboptions_11794);
    if (_6583 == 0) {
        DeRef(_6583);
        _6583 = NOVALUE;
        goto LC; // [193] 271
    }
    else {
        if (!IS_ATOM_INT(_6583) && DBL_PTR(_6583)->dbl == 0.0){
            DeRef(_6583);
            _6583 = NOVALUE;
            goto LC; // [193] 271
        }
        DeRef(_6583);
        _6583 = NOVALUE;
    }
    DeRef(_6583);
    _6583 = NOVALUE;

    /** 		dboptions = text:keyvalues(dboptions)*/
    RefDS(_dboptions_11794);
    RefDS(_5112);
    RefDS(_5113);
    RefDS(_5114);
    RefDS(_100);
    _0 = _dboptions_11794;
    _dboptions_11794 = _6keyvalues(_dboptions_11794, _5112, _5113, _5114, _100, 1);
    DeRefDS(_0);

    /** 		for i = 1 to length(dboptions) do*/
    if (IS_SEQUENCE(_dboptions_11794)){
            _6585 = SEQ_PTR(_dboptions_11794)->length;
    }
    else {
        _6585 = 1;
    }
    {
        int _i_11826;
        _i_11826 = 1;
LD: 
        if (_i_11826 > _6585){
            goto LE; // [214] 270
        }

        /** 			if types:string(dboptions[i][2]) then*/
        _2 = (int)SEQ_PTR(_dboptions_11794);
        _6586 = (int)*(((s1_ptr)_2)->base + _i_11826);
        _2 = (int)SEQ_PTR(_6586);
        _6587 = (int)*(((s1_ptr)_2)->base + 2);
        _6586 = NOVALUE;
        Ref(_6587);
        _6588 = _3string(_6587);
        _6587 = NOVALUE;
        if (_6588 == 0) {
            DeRef(_6588);
            _6588 = NOVALUE;
            goto LF; // [235] 263
        }
        else {
            if (!IS_ATOM_INT(_6588) && DBL_PTR(_6588)->dbl == 0.0){
                DeRef(_6588);
                _6588 = NOVALUE;
                goto LF; // [235] 263
            }
            DeRef(_6588);
            _6588 = NOVALUE;
        }
        DeRef(_6588);
        _6588 = NOVALUE;

        /** 				dboptions[i][2] = convert:to_number(dboptions[i][2])*/
        _2 = (int)SEQ_PTR(_dboptions_11794);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _dboptions_11794 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_11826 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_dboptions_11794);
        _6591 = (int)*(((s1_ptr)_2)->base + _i_11826);
        _2 = (int)SEQ_PTR(_6591);
        _6592 = (int)*(((s1_ptr)_2)->base + 2);
        _6591 = NOVALUE;
        Ref(_6592);
        _6593 = _4to_number(_6592, 0);
        _6592 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _6593;
        if( _1 != _6593 ){
            DeRef(_1);
        }
        _6593 = NOVALUE;
        _6589 = NOVALUE;
LF: 

        /** 		end for*/
        _i_11826 = _i_11826 + 1;
        goto LD; // [265] 221
LE: 
        ;
    }
LC: 

    /** 	lOptions = {DB_LOCK_NO, DEF_INIT_TABLES, DEF_INIT_TABLES}*/
    _0 = _lOptions_11796;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 5;
    *((int *)(_2+12)) = 5;
    _lOptions_11796 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	for i = 1 to length(dboptions) do*/
    if (IS_SEQUENCE(_dboptions_11794)){
            _6595 = SEQ_PTR(_dboptions_11794)->length;
    }
    else {
        _6595 = 1;
    }
    {
        int _i_11839;
        _i_11839 = 1;
L10: 
        if (_i_11839 > _6595){
            goto L11; // [286] 394
        }

        /** 		switch dboptions[i][1] do*/
        _2 = (int)SEQ_PTR(_dboptions_11794);
        _6596 = (int)*(((s1_ptr)_2)->base + _i_11839);
        _2 = (int)SEQ_PTR(_6596);
        _6597 = (int)*(((s1_ptr)_2)->base + 1);
        _6596 = NOVALUE;
        _1 = find(_6597, _6598);
        _6597 = NOVALUE;
        switch ( _1 ){ 

            /** 			case LOCK_METHOD then*/
            case 1:

            /** 				lOptions[CONNECT_LOCK] = dboptions[i][2]*/
            _2 = (int)SEQ_PTR(_dboptions_11794);
            _6600 = (int)*(((s1_ptr)_2)->base + _i_11839);
            _2 = (int)SEQ_PTR(_6600);
            _6601 = (int)*(((s1_ptr)_2)->base + 2);
            _6600 = NOVALUE;
            Ref(_6601);
            _2 = (int)SEQ_PTR(_lOptions_11796);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _lOptions_11796 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 1);
            _1 = *(int *)_2;
            *(int *)_2 = _6601;
            if( _1 != _6601 ){
                DeRef(_1);
            }
            _6601 = NOVALUE;
            goto L12; // [328] 387

            /** 			case INIT_TABLES then*/
            case 2:

            /** 				lOptions[CONNECT_TABLES] = dboptions[i][2]*/
            _2 = (int)SEQ_PTR(_dboptions_11794);
            _6602 = (int)*(((s1_ptr)_2)->base + _i_11839);
            _2 = (int)SEQ_PTR(_6602);
            _6603 = (int)*(((s1_ptr)_2)->base + 2);
            _6602 = NOVALUE;
            Ref(_6603);
            _2 = (int)SEQ_PTR(_lOptions_11796);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _lOptions_11796 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _6603;
            if( _1 != _6603 ){
                DeRef(_1);
            }
            _6603 = NOVALUE;
            goto L12; // [350] 387

            /** 			case INIT_FREE then*/
            case 3:

            /** 				lOptions[CONNECT_FREE] = dboptions[i][2]*/
            _2 = (int)SEQ_PTR(_dboptions_11794);
            _6604 = (int)*(((s1_ptr)_2)->base + _i_11839);
            _2 = (int)SEQ_PTR(_6604);
            _6605 = (int)*(((s1_ptr)_2)->base + 2);
            _6604 = NOVALUE;
            Ref(_6605);
            _2 = (int)SEQ_PTR(_lOptions_11796);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _lOptions_11796 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 3);
            _1 = *(int *)_2;
            *(int *)_2 = _6605;
            if( _1 != _6605 ){
                DeRef(_1);
            }
            _6605 = NOVALUE;
            goto L12; // [372] 387

            /** 			case else				*/
            case 0:

            /** 				return DB_OPEN_FAIL*/
            DeRefDS(_dbalias_11792);
            DeRefDS(_path_11793);
            DeRefDS(_dboptions_11794);
            DeRef(_lOptions_11796);
            _6573 = NOVALUE;
            return -1;
        ;}L12: 

        /** 	end for*/
        _i_11839 = _i_11839 + 1;
        goto L10; // [389] 293
L11: 
        ;
    }

    /** 	if lOptions[CONNECT_TABLES] < 1 then*/
    _2 = (int)SEQ_PTR(_lOptions_11796);
    _6606 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(GREATEREQ, _6606, 1)){
        _6606 = NOVALUE;
        goto L13; // [402] 415
    }
    _6606 = NOVALUE;

    /** 		lOptions[CONNECT_TABLES] = DEF_INIT_TABLES*/
    _2 = (int)SEQ_PTR(_lOptions_11796);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lOptions_11796 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 5;
    DeRef(_1);
L13: 

    /** 	lOptions[CONNECT_FREE] = math:min({lOptions[CONNECT_TABLES], MAX_INDEX})*/
    _2 = (int)SEQ_PTR(_lOptions_11796);
    _6608 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_6608);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6608;
    ((int *)_2)[2] = 10;
    _6609 = MAKE_SEQ(_1);
    _6608 = NOVALUE;
    _6610 = _17min(_6609);
    _6609 = NOVALUE;
    _2 = (int)SEQ_PTR(_lOptions_11796);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lOptions_11796 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _6610;
    if( _1 != _6610 ){
        DeRef(_1);
    }
    _6610 = NOVALUE;

    /** 	if lOptions[CONNECT_FREE] < 1 then*/
    _2 = (int)SEQ_PTR(_lOptions_11796);
    _6611 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(GREATEREQ, _6611, 1)){
        _6611 = NOVALUE;
        goto L14; // [445] 462
    }
    _6611 = NOVALUE;

    /** 		lOptions[CONNECT_FREE] = math:min({DEF_INIT_TABLES, MAX_INDEX})*/
    RefDS(_6613);
    _6614 = _17min(_6613);
    _2 = (int)SEQ_PTR(_lOptions_11796);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lOptions_11796 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _6614;
    if( _1 != _6614 ){
        DeRef(_1);
    }
    _6614 = NOVALUE;
L14: 

    /** 	Known_Aliases = append(Known_Aliases, dbalias)*/
    RefDS(_dbalias_11792);
    Append(&_2Known_Aliases_10901, _2Known_Aliases_10901, _dbalias_11792);

    /** 	Alias_Details = append(Alias_Details, { filesys:canonical_path( filesys:defaultext(path, "edb") ) , lOptions})*/
    RefDS(_path_11793);
    RefDS(_6616);
    _6617 = _8defaultext(_path_11793, _6616);
    _6618 = _8canonical_path(_6617, 0, 0);
    _6617 = NOVALUE;
    RefDS(_lOptions_11796);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6618;
    ((int *)_2)[2] = _lOptions_11796;
    _6619 = MAKE_SEQ(_1);
    _6618 = NOVALUE;
    RefDS(_6619);
    Append(&_2Alias_Details_10902, _2Alias_Details_10902, _6619);
    DeRefDS(_6619);
    _6619 = NOVALUE;

    /** 	return DB_OK*/
    DeRefDS(_dbalias_11792);
    DeRefDS(_path_11793);
    DeRefDS(_dboptions_11794);
    DeRefDS(_lOptions_11796);
    _6573 = NOVALUE;
    return 0;
    ;
}


int _2db_create(int _path_11874, int _lock_method_11875, int _init_tables_11876, int _init_free_11877)
{
    int _db_11878 = NOVALUE;
    int _lock_file_1__tmp_at272_11917 = NOVALUE;
    int _lock_file_inlined_lock_file_at_272_11916 = NOVALUE;
    int _put4_1__tmp_at398_11926 = NOVALUE;
    int _put4_1__tmp_at426_11928 = NOVALUE;
    int _put4_1__tmp_at469_11934 = NOVALUE;
    int _x_inlined_put4_at_466_11933 = NOVALUE;
    int _put4_1__tmp_at508_11939 = NOVALUE;
    int _x_inlined_put4_at_505_11938 = NOVALUE;
    int _put4_1__tmp_at536_11941 = NOVALUE;
    int _s_inlined_putn_at_572_11945 = NOVALUE;
    int _put4_1__tmp_at604_11950 = NOVALUE;
    int _x_inlined_put4_at_601_11949 = NOVALUE;
    int _s_inlined_putn_at_640_11954 = NOVALUE;
    int _6660 = NOVALUE;
    int _6659 = NOVALUE;
    int _6658 = NOVALUE;
    int _6657 = NOVALUE;
    int _6656 = NOVALUE;
    int _6655 = NOVALUE;
    int _6654 = NOVALUE;
    int _6653 = NOVALUE;
    int _6652 = NOVALUE;
    int _6651 = NOVALUE;
    int _6650 = NOVALUE;
    int _6633 = NOVALUE;
    int _6631 = NOVALUE;
    int _6630 = NOVALUE;
    int _6628 = NOVALUE;
    int _6627 = NOVALUE;
    int _6625 = NOVALUE;
    int _6624 = NOVALUE;
    int _6622 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lock_method_11875)) {
        _1 = (long)(DBL_PTR(_lock_method_11875)->dbl);
        if (UNIQUE(DBL_PTR(_lock_method_11875)) && (DBL_PTR(_lock_method_11875)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lock_method_11875);
        _lock_method_11875 = _1;
    }
    if (!IS_ATOM_INT(_init_tables_11876)) {
        _1 = (long)(DBL_PTR(_init_tables_11876)->dbl);
        if (UNIQUE(DBL_PTR(_init_tables_11876)) && (DBL_PTR(_init_tables_11876)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_tables_11876);
        _init_tables_11876 = _1;
    }
    if (!IS_ATOM_INT(_init_free_11877)) {
        _1 = (long)(DBL_PTR(_init_free_11877)->dbl);
        if (UNIQUE(DBL_PTR(_init_free_11877)) && (DBL_PTR(_init_free_11877)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_free_11877);
        _init_free_11877 = _1;
    }

    /** 	db = find(path, Known_Aliases)*/
    _db_11878 = find_from(_path_11874, _2Known_Aliases_10901, 1);

    /** 	if db then*/
    if (_db_11878 == 0)
    {
        goto L1; // [28] 114
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6622 = (int)*(((s1_ptr)_2)->base + _db_11878);
    DeRefDS(_path_11874);
    _2 = (int)SEQ_PTR(_6622);
    _path_11874 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_11874);
    _6622 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6624 = (int)*(((s1_ptr)_2)->base + _db_11878);
    _2 = (int)SEQ_PTR(_6624);
    _6625 = (int)*(((s1_ptr)_2)->base + 2);
    _6624 = NOVALUE;
    _2 = (int)SEQ_PTR(_6625);
    _lock_method_11875 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_11875)){
        _lock_method_11875 = (long)DBL_PTR(_lock_method_11875)->dbl;
    }
    _6625 = NOVALUE;

    /** 		init_tables = Alias_Details[db][2][CONNECT_TABLES]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6627 = (int)*(((s1_ptr)_2)->base + _db_11878);
    _2 = (int)SEQ_PTR(_6627);
    _6628 = (int)*(((s1_ptr)_2)->base + 2);
    _6627 = NOVALUE;
    _2 = (int)SEQ_PTR(_6628);
    _init_tables_11876 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_init_tables_11876)){
        _init_tables_11876 = (long)DBL_PTR(_init_tables_11876)->dbl;
    }
    _6628 = NOVALUE;

    /** 		init_free = Alias_Details[db][2][CONNECT_FREE]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6630 = (int)*(((s1_ptr)_2)->base + _db_11878);
    _2 = (int)SEQ_PTR(_6630);
    _6631 = (int)*(((s1_ptr)_2)->base + 2);
    _6630 = NOVALUE;
    _2 = (int)SEQ_PTR(_6631);
    _init_free_11877 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_init_free_11877)){
        _init_free_11877 = (long)DBL_PTR(_init_free_11877)->dbl;
    }
    _6631 = NOVALUE;
    goto L2; // [111] 160
L1: 

    /** 		path = filesys:canonical_path( defaultext(path, "edb") )*/
    RefDS(_path_11874);
    RefDS(_6616);
    _6633 = _8defaultext(_path_11874, _6616);
    _0 = _path_11874;
    _path_11874 = _8canonical_path(_6633, 0, 0);
    DeRefDS(_0);
    _6633 = NOVALUE;

    /** 		if init_tables < 1 then*/
    if (_init_tables_11876 >= 1)
    goto L3; // [133] 145

    /** 			init_tables = 1*/
    _init_tables_11876 = 1;
L3: 

    /** 		if init_free < 0 then*/
    if (_init_free_11877 >= 0)
    goto L4; // [147] 159

    /** 			init_free = 0*/
    _init_free_11877 = 0;
L4: 
L2: 

    /** 	db = open(path, "rb")*/
    _db_11878 = EOpen(_path_11874, _904, 0);

    /** 	if db != -1 then*/
    if (_db_11878 == -1)
    goto L5; // [171] 188

    /** 		close(db)*/
    EClose(_db_11878);

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_path_11874);
    return -2;
L5: 

    /** 	db = open(path, "wb")*/
    _db_11878 = EOpen(_path_11874, _949, 0);

    /** 	if db = -1 then*/
    if (_db_11878 != -1)
    goto L6; // [199] 212

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_11874);
    return -1;
L6: 

    /** 	close(db)*/
    EClose(_db_11878);

    /** 	db = open(path, "ub")*/
    _db_11878 = EOpen(_path_11874, _6641, 0);

    /** 	if db = -1 then*/
    if (_db_11878 != -1)
    goto L7; // [227] 240

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_11874);
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_11875 != 1)
    goto L8; // [244] 260

    /** 		lock_method = DB_LOCK_NO*/
    _lock_method_11875 = 0;
L8: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_11875 != 2)
    goto L9; // [264] 300

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at272_11917;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_11878;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at272_11917 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_272_11916 = machine(61, _lock_file_1__tmp_at272_11917);
    DeRef(_lock_file_1__tmp_at272_11917);
    _lock_file_1__tmp_at272_11917 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_272_11916 != 0)
    goto LA; // [287] 299

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_11874);
    return -3;
LA: 
L9: 

    /** 	save_keys()*/
    _2save_keys();

    /** 	current_db = db*/
    _2current_db_10880 = _db_11878;

    /** 	current_lock = lock_method*/
    _2current_lock_10886 = _lock_method_11875;

    /** 	current_table_pos = -1*/
    DeRef(_2current_table_pos_10881);
    _2current_table_pos_10881 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_2current_table_name_10882);
    _2current_table_name_10882 = _5;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_11874);
    Append(&_2db_names_10883, _2db_names_10883, _path_11874);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_2db_lock_methods_10885, _2db_lock_methods_10885, _lock_method_11875);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_2db_file_nums_10884, _2db_file_nums_10884, _db_11878);

    /** 	put1(DB_MAGIC) -- so we know what type of file it is*/

    /** 	puts(current_db, x)*/
    EPuts(_2current_db_10880, 77); // DJP 

    /** end procedure*/
    goto LB; // [365] 368
LB: 

    /** 	put1(DB_MAJOR) -- major version*/

    /** 	puts(current_db, x)*/
    EPuts(_2current_db_10880, 4); // DJP 

    /** end procedure*/
    goto LC; // [379] 382
LC: 

    /** 	put1(DB_MINOR) -- minor version*/

    /** 	puts(current_db, x)*/
    EPuts(_2current_db_10880, 0); // DJP 

    /** end procedure*/
    goto LD; // [393] 396
LD: 

    /** 	put4(19)  -- pointer to tables*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)19;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at398_11926);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at398_11926 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at398_11926); // DJP 

    /** end procedure*/
    goto LE; // [419] 422
LE: 
    DeRefi(_put4_1__tmp_at398_11926);
    _put4_1__tmp_at398_11926 = NOVALUE;

    /** 	put4(0)   -- number of free blocks*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at426_11928);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at426_11928 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at426_11928); // DJP 

    /** end procedure*/
    goto LF; // [447] 450
LF: 
    DeRefi(_put4_1__tmp_at426_11928);
    _put4_1__tmp_at426_11928 = NOVALUE;

    /** 	put4(23 + init_tables * SIZEOF_TABLE_HEADER + 4)   -- pointer to free list*/
    if (_init_tables_11876 == (short)_init_tables_11876)
    _6650 = _init_tables_11876 * 16;
    else
    _6650 = NewDouble(_init_tables_11876 * (double)16);
    if (IS_ATOM_INT(_6650)) {
        _6651 = 23 + _6650;
        if ((long)((unsigned long)_6651 + (unsigned long)HIGH_BITS) >= 0) 
        _6651 = NewDouble((double)_6651);
    }
    else {
        _6651 = NewDouble((double)23 + DBL_PTR(_6650)->dbl);
    }
    DeRef(_6650);
    _6650 = NOVALUE;
    if (IS_ATOM_INT(_6651)) {
        _6652 = _6651 + 4;
        if ((long)((unsigned long)_6652 + (unsigned long)HIGH_BITS) >= 0) 
        _6652 = NewDouble((double)_6652);
    }
    else {
        _6652 = NewDouble(DBL_PTR(_6651)->dbl + (double)4);
    }
    DeRef(_6651);
    _6651 = NOVALUE;
    DeRef(_x_inlined_put4_at_466_11933);
    _x_inlined_put4_at_466_11933 = _6652;
    _6652 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_466_11933)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_466_11933;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_466_11933)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at469_11934);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at469_11934 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at469_11934); // DJP 

    /** end procedure*/
    goto L10; // [490] 493
L10: 
    DeRef(_x_inlined_put4_at_466_11933);
    _x_inlined_put4_at_466_11933 = NOVALUE;
    DeRefi(_put4_1__tmp_at469_11934);
    _put4_1__tmp_at469_11934 = NOVALUE;

    /** 	put4( 8 + init_tables * SIZEOF_TABLE_HEADER)  -- allocated size*/
    if (_init_tables_11876 == (short)_init_tables_11876)
    _6653 = _init_tables_11876 * 16;
    else
    _6653 = NewDouble(_init_tables_11876 * (double)16);
    if (IS_ATOM_INT(_6653)) {
        _6654 = 8 + _6653;
        if ((long)((unsigned long)_6654 + (unsigned long)HIGH_BITS) >= 0) 
        _6654 = NewDouble((double)_6654);
    }
    else {
        _6654 = NewDouble((double)8 + DBL_PTR(_6653)->dbl);
    }
    DeRef(_6653);
    _6653 = NOVALUE;
    DeRef(_x_inlined_put4_at_505_11938);
    _x_inlined_put4_at_505_11938 = _6654;
    _6654 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_505_11938)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_505_11938;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_505_11938)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at508_11939);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at508_11939 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at508_11939); // DJP 

    /** end procedure*/
    goto L11; // [529] 532
L11: 
    DeRef(_x_inlined_put4_at_505_11938);
    _x_inlined_put4_at_505_11938 = NOVALUE;
    DeRefi(_put4_1__tmp_at508_11939);
    _put4_1__tmp_at508_11939 = NOVALUE;

    /** 	put4(0)   -- number of tables that currently exist*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at536_11941);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at536_11941 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at536_11941); // DJP 

    /** end procedure*/
    goto L12; // [557] 560
L12: 
    DeRefi(_put4_1__tmp_at536_11941);
    _put4_1__tmp_at536_11941 = NOVALUE;

    /** 	putn(repeat(0, init_tables * SIZEOF_TABLE_HEADER))*/
    _6655 = _init_tables_11876 * 16;
    _6656 = Repeat(0, _6655);
    _6655 = NOVALUE;
    DeRefi(_s_inlined_putn_at_572_11945);
    _s_inlined_putn_at_572_11945 = _6656;
    _6656 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_572_11945); // DJP 

    /** end procedure*/
    goto L13; // [586] 589
L13: 
    DeRefi(_s_inlined_putn_at_572_11945);
    _s_inlined_putn_at_572_11945 = NOVALUE;

    /** 	put4(4+init_free*8)   -- allocated size*/
    if (_init_free_11877 == (short)_init_free_11877)
    _6657 = _init_free_11877 * 8;
    else
    _6657 = NewDouble(_init_free_11877 * (double)8);
    if (IS_ATOM_INT(_6657)) {
        _6658 = 4 + _6657;
        if ((long)((unsigned long)_6658 + (unsigned long)HIGH_BITS) >= 0) 
        _6658 = NewDouble((double)_6658);
    }
    else {
        _6658 = NewDouble((double)4 + DBL_PTR(_6657)->dbl);
    }
    DeRef(_6657);
    _6657 = NOVALUE;
    DeRef(_x_inlined_put4_at_601_11949);
    _x_inlined_put4_at_601_11949 = _6658;
    _6658 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_601_11949)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_601_11949;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_601_11949)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at604_11950);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at604_11950 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at604_11950); // DJP 

    /** end procedure*/
    goto L14; // [625] 628
L14: 
    DeRef(_x_inlined_put4_at_601_11949);
    _x_inlined_put4_at_601_11949 = NOVALUE;
    DeRefi(_put4_1__tmp_at604_11950);
    _put4_1__tmp_at604_11950 = NOVALUE;

    /** 	putn(repeat(0, init_free * 8))*/
    _6659 = _init_free_11877 * 8;
    _6660 = Repeat(0, _6659);
    _6659 = NOVALUE;
    DeRefi(_s_inlined_putn_at_640_11954);
    _s_inlined_putn_at_640_11954 = _6660;
    _6660 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_640_11954); // DJP 

    /** end procedure*/
    goto L15; // [654] 657
L15: 
    DeRefi(_s_inlined_putn_at_640_11954);
    _s_inlined_putn_at_640_11954 = NOVALUE;

    /** 	return DB_OK*/
    DeRefDS(_path_11874);
    return 0;
    ;
}


int _2db_open(int _path_11957, int _lock_method_11958)
{
    int _db_11959 = NOVALUE;
    int _magic_11960 = NOVALUE;
    int _lock_file_1__tmp_at173_11987 = NOVALUE;
    int _lock_file_inlined_lock_file_at_173_11986 = NOVALUE;
    int _lock_file_1__tmp_at219_11994 = NOVALUE;
    int _lock_file_inlined_lock_file_at_219_11993 = NOVALUE;
    int _6671 = NOVALUE;
    int _6669 = NOVALUE;
    int _6667 = NOVALUE;
    int _6665 = NOVALUE;
    int _6664 = NOVALUE;
    int _6662 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lock_method_11958)) {
        _1 = (long)(DBL_PTR(_lock_method_11958)->dbl);
        if (UNIQUE(DBL_PTR(_lock_method_11958)) && (DBL_PTR(_lock_method_11958)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lock_method_11958);
        _lock_method_11958 = _1;
    }

    /** 	db = find(path, Known_Aliases)*/
    _db_11959 = find_from(_path_11957, _2Known_Aliases_10901, 1);

    /** 	if db then*/
    if (_db_11959 == 0)
    {
        goto L1; // [20] 62
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6662 = (int)*(((s1_ptr)_2)->base + _db_11959);
    DeRefDS(_path_11957);
    _2 = (int)SEQ_PTR(_6662);
    _path_11957 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_11957);
    _6662 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6664 = (int)*(((s1_ptr)_2)->base + _db_11959);
    _2 = (int)SEQ_PTR(_6664);
    _6665 = (int)*(((s1_ptr)_2)->base + 2);
    _6664 = NOVALUE;
    _2 = (int)SEQ_PTR(_6665);
    _lock_method_11958 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_11958)){
        _lock_method_11958 = (long)DBL_PTR(_lock_method_11958)->dbl;
    }
    _6665 = NOVALUE;
    goto L2; // [59] 80
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_11957);
    RefDS(_6616);
    _6667 = _8defaultext(_path_11957, _6616);
    _0 = _path_11957;
    _path_11957 = _8canonical_path(_6667, 0, 0);
    DeRefDS(_0);
    _6667 = NOVALUE;
L2: 

    /** 	if lock_method = DB_LOCK_NO or*/
    _6669 = (_lock_method_11958 == 0);
    if (_6669 != 0) {
        goto L3; // [88] 103
    }
    _6671 = (_lock_method_11958 == 2);
    if (_6671 == 0)
    {
        DeRef(_6671);
        _6671 = NOVALUE;
        goto L4; // [99] 115
    }
    else{
        DeRef(_6671);
        _6671 = NOVALUE;
    }
L3: 

    /** 		db = open(path, "ub")*/
    _db_11959 = EOpen(_path_11957, _6641, 0);
    goto L5; // [112] 125
L4: 

    /** 		db = open(path, "rb")*/
    _db_11959 = EOpen(_path_11957, _904, 0);
L5: 

    /** ifdef WINDOWS then*/

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_11958 != 1)
    goto L6; // [131] 147

    /** 		lock_method = DB_LOCK_EXCLUSIVE*/
    _lock_method_11958 = 2;
L6: 

    /** 	if db = -1 then*/
    if (_db_11959 != -1)
    goto L7; // [149] 162

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_11957);
    DeRef(_6669);
    _6669 = NOVALUE;
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_11958 != 2)
    goto L8; // [166] 208

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at173_11987;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_11959;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at173_11987 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_173_11986 = machine(61, _lock_file_1__tmp_at173_11987);
    DeRef(_lock_file_1__tmp_at173_11987);
    _lock_file_1__tmp_at173_11987 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_173_11986 != 0)
    goto L9; // [189] 253

    /** 			close(db)*/
    EClose(_db_11959);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_11957);
    DeRef(_6669);
    _6669 = NOVALUE;
    return -3;
    goto L9; // [205] 253
L8: 

    /** 	elsif lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_11958 != 1)
    goto LA; // [212] 252

    /** 		if not io:lock_file(db, io:LOCK_SHARED, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at219_11994;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_11959;
    *((int *)(_2+8)) = 1;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at219_11994 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_219_11993 = machine(61, _lock_file_1__tmp_at219_11994);
    DeRef(_lock_file_1__tmp_at219_11994);
    _lock_file_1__tmp_at219_11994 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_219_11993 != 0)
    goto LB; // [235] 251

    /** 			close(db)*/
    EClose(_db_11959);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_11957);
    DeRef(_6669);
    _6669 = NOVALUE;
    return -3;
LB: 
LA: 
L9: 

    /** 	magic = getc(db)*/
    if (_db_11959 != last_r_file_no) {
        last_r_file_ptr = which_file(_db_11959, EF_READ);
        last_r_file_no = _db_11959;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_11960 = getKBchar();
        }
        else
        _magic_11960 = getc(last_r_file_ptr);
    }
    else
    _magic_11960 = getc(last_r_file_ptr);

    /** 	if magic != DB_MAGIC then*/
    if (_magic_11960 == 77)
    goto LC; // [262] 279

    /** 		close(db)*/
    EClose(_db_11959);

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_11957);
    DeRef(_6669);
    _6669 = NOVALUE;
    return -1;
LC: 

    /** 	save_keys()*/
    _2save_keys();

    /** 	current_db = db */
    _2current_db_10880 = _db_11959;

    /** 	current_table_pos = -1*/
    DeRef(_2current_table_pos_10881);
    _2current_table_pos_10881 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_2current_table_name_10882);
    _2current_table_name_10882 = _5;

    /** 	current_lock = lock_method*/
    _2current_lock_10886 = _lock_method_11958;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_11957);
    Append(&_2db_names_10883, _2db_names_10883, _path_11957);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_2db_lock_methods_10885, _2db_lock_methods_10885, _lock_method_11958);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_2db_file_nums_10884, _2db_file_nums_10884, _db_11959);

    /** 	return DB_OK*/
    DeRefDS(_path_11957);
    DeRef(_6669);
    _6669 = NOVALUE;
    return 0;
    ;
}


int _2db_select(int _path_12004, int _lock_method_12005)
{
    int _index_12006 = NOVALUE;
    int _6691 = NOVALUE;
    int _6689 = NOVALUE;
    int _6688 = NOVALUE;
    int _6686 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lock_method_12005)) {
        _1 = (long)(DBL_PTR(_lock_method_12005)->dbl);
        if (UNIQUE(DBL_PTR(_lock_method_12005)) && (DBL_PTR(_lock_method_12005)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lock_method_12005);
        _lock_method_12005 = _1;
    }

    /** 	index = find(path, Known_Aliases)*/
    _index_12006 = find_from(_path_12004, _2Known_Aliases_10901, 1);

    /** 	if index then*/
    if (_index_12006 == 0)
    {
        goto L1; // [20] 62
    }
    else{
    }

    /** 		path = Alias_Details[index][1]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6686 = (int)*(((s1_ptr)_2)->base + _index_12006);
    DeRefDS(_path_12004);
    _2 = (int)SEQ_PTR(_6686);
    _path_12004 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_12004);
    _6686 = NOVALUE;

    /** 		lock_method = Alias_Details[index][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_2Alias_Details_10902);
    _6688 = (int)*(((s1_ptr)_2)->base + _index_12006);
    _2 = (int)SEQ_PTR(_6688);
    _6689 = (int)*(((s1_ptr)_2)->base + 2);
    _6688 = NOVALUE;
    _2 = (int)SEQ_PTR(_6689);
    _lock_method_12005 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_12005)){
        _lock_method_12005 = (long)DBL_PTR(_lock_method_12005)->dbl;
    }
    _6689 = NOVALUE;
    goto L2; // [59] 80
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_12004);
    RefDS(_6616);
    _6691 = _8defaultext(_path_12004, _6616);
    _0 = _path_12004;
    _path_12004 = _8canonical_path(_6691, 0, 0);
    DeRefDS(_0);
    _6691 = NOVALUE;
L2: 

    /** 	index = eu:find(path, db_names)*/
    _index_12006 = find_from(_path_12004, _2db_names_10883, 1);

    /** 	if index = 0 then*/
    if (_index_12006 != 0)
    goto L3; // [93] 150

    /** 		if lock_method = -1 then*/
    if (_lock_method_12005 != -1)
    goto L4; // [99] 112

    /** 			return DB_OPEN_FAIL*/
    DeRefDS(_path_12004);
    return -1;
L4: 

    /** 		index = db_open(path, lock_method)*/
    RefDS(_path_12004);
    _index_12006 = _2db_open(_path_12004, _lock_method_12005);
    if (!IS_ATOM_INT(_index_12006)) {
        _1 = (long)(DBL_PTR(_index_12006)->dbl);
        if (UNIQUE(DBL_PTR(_index_12006)) && (DBL_PTR(_index_12006)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_12006);
        _index_12006 = _1;
    }

    /** 		if index != DB_OK then*/
    if (_index_12006 == 0)
    goto L5; // [127] 138

    /** 			return index*/
    DeRefDS(_path_12004);
    return _index_12006;
L5: 

    /** 		index = eu:find(path, db_names)*/
    _index_12006 = find_from(_path_12004, _2db_names_10883, 1);
L3: 

    /** 	save_keys()*/
    _2save_keys();

    /** 	current_db = db_file_nums[index]*/
    _2 = (int)SEQ_PTR(_2db_file_nums_10884);
    _2current_db_10880 = (int)*(((s1_ptr)_2)->base + _index_12006);
    if (!IS_ATOM_INT(_2current_db_10880))
    _2current_db_10880 = (long)DBL_PTR(_2current_db_10880)->dbl;

    /** 	current_lock = db_lock_methods[index]*/
    _2 = (int)SEQ_PTR(_2db_lock_methods_10885);
    _2current_lock_10886 = (int)*(((s1_ptr)_2)->base + _index_12006);
    if (!IS_ATOM_INT(_2current_lock_10886))
    _2current_lock_10886 = (long)DBL_PTR(_2current_lock_10886)->dbl;

    /** 	current_table_pos = -1*/
    DeRef(_2current_table_pos_10881);
    _2current_table_pos_10881 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_2current_table_name_10882);
    _2current_table_name_10882 = _5;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_2key_pointers_10887);
    _2key_pointers_10887 = _5;

    /** 	return DB_OK*/
    DeRefDS(_path_12004);
    return 0;
    ;
}


void _2db_close()
{
    int _unlock_file_1__tmp_at25_12035 = NOVALUE;
    int _index_12030 = NOVALUE;
    int _6708 = NOVALUE;
    int _6707 = NOVALUE;
    int _6706 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_2current_db_10880 != -1)
    goto L1; // [5] 15

    /** 		return*/
    return;
L1: 

    /** 	if current_lock then*/
    if (_2current_lock_10886 == 0)
    {
        goto L2; // [19] 43
    }
    else{
    }

    /** 		io:unlock_file(current_db, {})*/

    /** 	machine_proc(M_UNLOCK_FILE, {fn, r})*/
    RefDS(_5);
    DeRef(_unlock_file_1__tmp_at25_12035);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _5;
    _unlock_file_1__tmp_at25_12035 = MAKE_SEQ(_1);
    machine(62, _unlock_file_1__tmp_at25_12035);

    /** end procedure*/
    goto L3; // [37] 40
L3: 
    DeRef(_unlock_file_1__tmp_at25_12035);
    _unlock_file_1__tmp_at25_12035 = NOVALUE;
L2: 

    /** 	close(current_db)*/
    EClose(_2current_db_10880);

    /** 	index = eu:find(current_db, db_file_nums)*/
    _index_12030 = find_from(_2current_db_10880, _2db_file_nums_10884, 1);

    /** 	db_names = remove(db_names, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2db_names_10883);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_12030)) ? _index_12030 : (long)(DBL_PTR(_index_12030)->dbl);
        int stop = (IS_ATOM_INT(_index_12030)) ? _index_12030 : (long)(DBL_PTR(_index_12030)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2db_names_10883), start, &_2db_names_10883 );
            }
            else Tail(SEQ_PTR(_2db_names_10883), stop+1, &_2db_names_10883);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2db_names_10883), start, &_2db_names_10883);
        }
        else {
            assign_slice_seq = &assign_space;
            _2db_names_10883 = Remove_elements(start, stop, (SEQ_PTR(_2db_names_10883)->ref == 1));
        }
    }

    /** 	db_file_nums = remove(db_file_nums, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2db_file_nums_10884);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_12030)) ? _index_12030 : (long)(DBL_PTR(_index_12030)->dbl);
        int stop = (IS_ATOM_INT(_index_12030)) ? _index_12030 : (long)(DBL_PTR(_index_12030)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2db_file_nums_10884), start, &_2db_file_nums_10884 );
            }
            else Tail(SEQ_PTR(_2db_file_nums_10884), stop+1, &_2db_file_nums_10884);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2db_file_nums_10884), start, &_2db_file_nums_10884);
        }
        else {
            assign_slice_seq = &assign_space;
            _2db_file_nums_10884 = Remove_elements(start, stop, (SEQ_PTR(_2db_file_nums_10884)->ref == 1));
        }
    }

    /** 	db_lock_methods = remove(db_lock_methods, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2db_lock_methods_10885);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_12030)) ? _index_12030 : (long)(DBL_PTR(_index_12030)->dbl);
        int stop = (IS_ATOM_INT(_index_12030)) ? _index_12030 : (long)(DBL_PTR(_index_12030)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2db_lock_methods_10885), start, &_2db_lock_methods_10885 );
            }
            else Tail(SEQ_PTR(_2db_lock_methods_10885), stop+1, &_2db_lock_methods_10885);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2db_lock_methods_10885), start, &_2db_lock_methods_10885);
        }
        else {
            assign_slice_seq = &assign_space;
            _2db_lock_methods_10885 = Remove_elements(start, stop, (SEQ_PTR(_2db_lock_methods_10885)->ref == 1));
        }
    }

    /** 	for i = length(cache_index) to 1 by -1 do*/
    if (IS_SEQUENCE(_2cache_index_10889)){
            _6706 = SEQ_PTR(_2cache_index_10889)->length;
    }
    else {
        _6706 = 1;
    }
    {
        int _i_12041;
        _i_12041 = _6706;
L4: 
        if (_i_12041 < 1){
            goto L5; // [96] 147
        }

        /** 		if cache_index[i][1] = current_db then*/
        _2 = (int)SEQ_PTR(_2cache_index_10889);
        _6707 = (int)*(((s1_ptr)_2)->base + _i_12041);
        _2 = (int)SEQ_PTR(_6707);
        _6708 = (int)*(((s1_ptr)_2)->base + 1);
        _6707 = NOVALUE;
        if (binary_op_a(NOTEQ, _6708, _2current_db_10880)){
            _6708 = NOVALUE;
            goto L6; // [117] 140
        }
        _6708 = NOVALUE;

        /** 			cache_index = remove(cache_index, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_2cache_index_10889);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_12041)) ? _i_12041 : (long)(DBL_PTR(_i_12041)->dbl);
            int stop = (IS_ATOM_INT(_i_12041)) ? _i_12041 : (long)(DBL_PTR(_i_12041)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_2cache_index_10889), start, &_2cache_index_10889 );
                }
                else Tail(SEQ_PTR(_2cache_index_10889), stop+1, &_2cache_index_10889);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_2cache_index_10889), start, &_2cache_index_10889);
            }
            else {
                assign_slice_seq = &assign_space;
                _2cache_index_10889 = Remove_elements(start, stop, (SEQ_PTR(_2cache_index_10889)->ref == 1));
            }
        }

        /** 			key_cache = remove(key_cache, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_2key_cache_10888);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_12041)) ? _i_12041 : (long)(DBL_PTR(_i_12041)->dbl);
            int stop = (IS_ATOM_INT(_i_12041)) ? _i_12041 : (long)(DBL_PTR(_i_12041)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_2key_cache_10888), start, &_2key_cache_10888 );
                }
                else Tail(SEQ_PTR(_2key_cache_10888), stop+1, &_2key_cache_10888);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_2key_cache_10888), start, &_2key_cache_10888);
            }
            else {
                assign_slice_seq = &assign_space;
                _2key_cache_10888 = Remove_elements(start, stop, (SEQ_PTR(_2key_cache_10888)->ref == 1));
            }
        }
L6: 

        /** 	end for*/
        _i_12041 = _i_12041 + -1;
        goto L4; // [142] 103
L5: 
        ;
    }

    /** 	current_table_pos = -1*/
    DeRef(_2current_table_pos_10881);
    _2current_table_pos_10881 = -1;

    /** 	current_table_name = ""	*/
    RefDS(_5);
    DeRef(_2current_table_name_10882);
    _2current_table_name_10882 = _5;

    /** 	current_db = -1*/
    _2current_db_10880 = -1;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_2key_pointers_10887);
    _2key_pointers_10887 = _5;

    /** end procedure*/
    return;
    ;
}


int _2table_find(int _name_12051)
{
    int _tables_12052 = NOVALUE;
    int _nt_12053 = NOVALUE;
    int _t_header_12054 = NOVALUE;
    int _name_ptr_12055 = NOVALUE;
    int _seek_1__tmp_at6_12058 = NOVALUE;
    int _seek_inlined_seek_at_6_12057 = NOVALUE;
    int _seek_1__tmp_at44_12065 = NOVALUE;
    int _seek_inlined_seek_at_44_12064 = NOVALUE;
    int _seek_1__tmp_at84_12073 = NOVALUE;
    int _seek_inlined_seek_at_84_12072 = NOVALUE;
    int _seek_1__tmp_at106_12077 = NOVALUE;
    int _seek_inlined_seek_at_106_12076 = NOVALUE;
    int _6719 = NOVALUE;
    int _6717 = NOVALUE;
    int _6712 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at6_12058);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at6_12058 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_6_12057 = machine(19, _seek_1__tmp_at6_12058);
    DeRefi(_seek_1__tmp_at6_12058);
    _seek_1__tmp_at6_12058 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_2vLastErrors_10904)){
            _6712 = SEQ_PTR(_2vLastErrors_10904)->length;
    }
    else {
        _6712 = 1;
    }
    if (_6712 <= 0)
    goto L1; // [27] 36
    DeRefDS(_name_12051);
    DeRef(_tables_12052);
    DeRef(_nt_12053);
    DeRef(_t_header_12054);
    DeRef(_name_ptr_12055);
    return -1;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_12052;
    _tables_12052 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_12052);
    DeRef(_seek_1__tmp_at44_12065);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _tables_12052;
    _seek_1__tmp_at44_12065 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_44_12064 = machine(19, _seek_1__tmp_at44_12065);
    DeRef(_seek_1__tmp_at44_12065);
    _seek_1__tmp_at44_12065 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_12053;
    _nt_12053 = _2get4();
    DeRef(_0);

    /** 	t_header = tables+4*/
    DeRef(_t_header_12054);
    if (IS_ATOM_INT(_tables_12052)) {
        _t_header_12054 = _tables_12052 + 4;
        if ((long)((unsigned long)_t_header_12054 + (unsigned long)HIGH_BITS) >= 0) 
        _t_header_12054 = NewDouble((double)_t_header_12054);
    }
    else {
        _t_header_12054 = NewDouble(DBL_PTR(_tables_12052)->dbl + (double)4);
    }

    /** 	for i = 1 to nt do*/
    Ref(_nt_12053);
    DeRef(_6717);
    _6717 = _nt_12053;
    {
        int _i_12069;
        _i_12069 = 1;
L2: 
        if (binary_op_a(GREATER, _i_12069, _6717)){
            goto L3; // [74] 150
        }

        /** 		io:seek(current_db, t_header)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_t_header_12054);
        DeRef(_seek_1__tmp_at84_12073);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _t_header_12054;
        _seek_1__tmp_at84_12073 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_84_12072 = machine(19, _seek_1__tmp_at84_12073);
        DeRef(_seek_1__tmp_at84_12073);
        _seek_1__tmp_at84_12073 = NOVALUE;

        /** 		name_ptr = get4()*/
        _0 = _name_ptr_12055;
        _name_ptr_12055 = _2get4();
        DeRef(_0);

        /** 		io:seek(current_db, name_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_ptr_12055);
        DeRef(_seek_1__tmp_at106_12077);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _name_ptr_12055;
        _seek_1__tmp_at106_12077 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_106_12076 = machine(19, _seek_1__tmp_at106_12077);
        DeRef(_seek_1__tmp_at106_12077);
        _seek_1__tmp_at106_12077 = NOVALUE;

        /** 		if equal_string(name) > 0 then*/
        RefDS(_name_12051);
        _6719 = _2equal_string(_name_12051);
        if (binary_op_a(LESSEQ, _6719, 0)){
            DeRef(_6719);
            _6719 = NOVALUE;
            goto L4; // [126] 137
        }
        DeRef(_6719);
        _6719 = NOVALUE;

        /** 			return t_header*/
        DeRef(_i_12069);
        DeRefDS(_name_12051);
        DeRef(_tables_12052);
        DeRef(_nt_12053);
        DeRef(_name_ptr_12055);
        return _t_header_12054;
L4: 

        /** 		t_header += SIZEOF_TABLE_HEADER*/
        _0 = _t_header_12054;
        if (IS_ATOM_INT(_t_header_12054)) {
            _t_header_12054 = _t_header_12054 + 16;
            if ((long)((unsigned long)_t_header_12054 + (unsigned long)HIGH_BITS) >= 0) 
            _t_header_12054 = NewDouble((double)_t_header_12054);
        }
        else {
            _t_header_12054 = NewDouble(DBL_PTR(_t_header_12054)->dbl + (double)16);
        }
        DeRef(_0);

        /** 	end for*/
        _0 = _i_12069;
        if (IS_ATOM_INT(_i_12069)) {
            _i_12069 = _i_12069 + 1;
            if ((long)((unsigned long)_i_12069 +(unsigned long) HIGH_BITS) >= 0){
                _i_12069 = NewDouble((double)_i_12069);
            }
        }
        else {
            _i_12069 = binary_op_a(PLUS, _i_12069, 1);
        }
        DeRef(_0);
        goto L2; // [145] 81
L3: 
        ;
        DeRef(_i_12069);
    }

    /** 	return -1*/
    DeRefDS(_name_12051);
    DeRef(_tables_12052);
    DeRef(_nt_12053);
    DeRef(_t_header_12054);
    DeRef(_name_ptr_12055);
    return -1;
    ;
}


int _2db_select_table(int _name_12084)
{
    int _table_12085 = NOVALUE;
    int _nkeys_12086 = NOVALUE;
    int _index_12087 = NOVALUE;
    int _block_ptr_12088 = NOVALUE;
    int _block_size_12089 = NOVALUE;
    int _blocks_12090 = NOVALUE;
    int _k_12091 = NOVALUE;
    int _seek_1__tmp_at128_12110 = NOVALUE;
    int _seek_inlined_seek_at_128_12109 = NOVALUE;
    int _pos_inlined_seek_at_125_12108 = NOVALUE;
    int _seek_1__tmp_at190_12120 = NOVALUE;
    int _seek_inlined_seek_at_190_12119 = NOVALUE;
    int _seek_1__tmp_at217_12125 = NOVALUE;
    int _seek_inlined_seek_at_217_12124 = NOVALUE;
    int _6740 = NOVALUE;
    int _6739 = NOVALUE;
    int _6736 = NOVALUE;
    int _6731 = NOVALUE;
    int _6726 = NOVALUE;
    int _6722 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(current_table_name, name) then*/
    if (_2current_table_name_10882 == _name_12084)
    _6722 = 1;
    else if (IS_ATOM_INT(_2current_table_name_10882) && IS_ATOM_INT(_name_12084))
    _6722 = 0;
    else
    _6722 = (compare(_2current_table_name_10882, _name_12084) == 0);
    if (_6722 == 0)
    {
        _6722 = NOVALUE;
        goto L1; // [11] 23
    }
    else{
        _6722 = NOVALUE;
    }

    /** 		return DB_OK*/
    DeRefDS(_name_12084);
    DeRef(_table_12085);
    DeRef(_nkeys_12086);
    DeRef(_index_12087);
    DeRef(_block_ptr_12088);
    DeRef(_block_size_12089);
    return 0;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_12084);
    _0 = _table_12085;
    _table_12085 = _2table_find(_name_12084);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_12085, -1)){
        goto L2; // [31] 44
    }

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_name_12084);
    DeRef(_table_12085);
    DeRef(_nkeys_12086);
    DeRef(_index_12087);
    DeRef(_block_ptr_12088);
    DeRef(_block_size_12089);
    return -1;
L2: 

    /** 	save_keys()*/
    _2save_keys();

    /** 	current_table_pos = table*/
    Ref(_table_12085);
    DeRef(_2current_table_pos_10881);
    _2current_table_pos_10881 = _table_12085;

    /** 	current_table_name = name*/
    RefDS(_name_12084);
    DeRef(_2current_table_name_10882);
    _2current_table_name_10882 = _name_12084;

    /** 	k = 0*/
    _k_12091 = 0;

    /** 	if caching_option = 1 then*/
    if (_2caching_option_10890 != 1)
    goto L3; // [71] 112

    /** 		k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_2current_table_pos_10881);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _2current_table_pos_10881;
    _6726 = MAKE_SEQ(_1);
    _k_12091 = find_from(_6726, _2cache_index_10889, 1);
    DeRefDS(_6726);
    _6726 = NOVALUE;

    /** 		if k != 0 then*/
    if (_k_12091 == 0)
    goto L4; // [96] 111

    /** 			key_pointers = key_cache[k]*/
    DeRef(_2key_pointers_10887);
    _2 = (int)SEQ_PTR(_2key_cache_10888);
    _2key_pointers_10887 = (int)*(((s1_ptr)_2)->base + _k_12091);
    Ref(_2key_pointers_10887);
L4: 
L3: 

    /** 	if k = 0 then*/
    if (_k_12091 != 0)
    goto L5; // [114] 283

    /** 		io:seek(current_db, table+4)*/
    if (IS_ATOM_INT(_table_12085)) {
        _6731 = _table_12085 + 4;
        if ((long)((unsigned long)_6731 + (unsigned long)HIGH_BITS) >= 0) 
        _6731 = NewDouble((double)_6731);
    }
    else {
        _6731 = NewDouble(DBL_PTR(_table_12085)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_125_12108);
    _pos_inlined_seek_at_125_12108 = _6731;
    _6731 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_125_12108);
    DeRef(_seek_1__tmp_at128_12110);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_125_12108;
    _seek_1__tmp_at128_12110 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_128_12109 = machine(19, _seek_1__tmp_at128_12110);
    DeRef(_pos_inlined_seek_at_125_12108);
    _pos_inlined_seek_at_125_12108 = NOVALUE;
    DeRef(_seek_1__tmp_at128_12110);
    _seek_1__tmp_at128_12110 = NOVALUE;

    /** 		nkeys = get4()*/
    _0 = _nkeys_12086;
    _nkeys_12086 = _2get4();
    DeRef(_0);

    /** 		blocks = get4()*/
    _blocks_12090 = _2get4();
    if (!IS_ATOM_INT(_blocks_12090)) {
        _1 = (long)(DBL_PTR(_blocks_12090)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_12090)) && (DBL_PTR(_blocks_12090)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_12090);
        _blocks_12090 = _1;
    }

    /** 		index = get4()*/
    _0 = _index_12087;
    _index_12087 = _2get4();
    DeRef(_0);

    /** 		key_pointers = repeat(0, nkeys)*/
    DeRef(_2key_pointers_10887);
    _2key_pointers_10887 = Repeat(0, _nkeys_12086);

    /** 		k = 1*/
    _k_12091 = 1;

    /** 		for b = 0 to blocks-1 do*/
    _6736 = _blocks_12090 - 1;
    if ((long)((unsigned long)_6736 +(unsigned long) HIGH_BITS) >= 0){
        _6736 = NewDouble((double)_6736);
    }
    {
        int _b_12116;
        _b_12116 = 0;
L6: 
        if (binary_op_a(GREATER, _b_12116, _6736)){
            goto L7; // [180] 282
        }

        /** 			io:seek(current_db, index)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_index_12087);
        DeRef(_seek_1__tmp_at190_12120);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _index_12087;
        _seek_1__tmp_at190_12120 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_190_12119 = machine(19, _seek_1__tmp_at190_12120);
        DeRef(_seek_1__tmp_at190_12120);
        _seek_1__tmp_at190_12120 = NOVALUE;

        /** 			block_size = get4()*/
        _0 = _block_size_12089;
        _block_size_12089 = _2get4();
        DeRef(_0);

        /** 			block_ptr = get4()*/
        _0 = _block_ptr_12088;
        _block_ptr_12088 = _2get4();
        DeRef(_0);

        /** 			io:seek(current_db, block_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_block_ptr_12088);
        DeRef(_seek_1__tmp_at217_12125);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _block_ptr_12088;
        _seek_1__tmp_at217_12125 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_217_12124 = machine(19, _seek_1__tmp_at217_12125);
        DeRef(_seek_1__tmp_at217_12125);
        _seek_1__tmp_at217_12125 = NOVALUE;

        /** 			for j = 1 to block_size do*/
        Ref(_block_size_12089);
        DeRef(_6739);
        _6739 = _block_size_12089;
        {
            int _j_12127;
            _j_12127 = 1;
L8: 
            if (binary_op_a(GREATER, _j_12127, _6739)){
                goto L9; // [236] 269
            }

            /** 				key_pointers[k] = get4()*/
            _6740 = _2get4();
            _2 = (int)SEQ_PTR(_2key_pointers_10887);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _2key_pointers_10887 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _k_12091);
            _1 = *(int *)_2;
            *(int *)_2 = _6740;
            if( _1 != _6740 ){
                DeRef(_1);
            }
            _6740 = NOVALUE;

            /** 				k += 1*/
            _k_12091 = _k_12091 + 1;

            /** 			end for*/
            _0 = _j_12127;
            if (IS_ATOM_INT(_j_12127)) {
                _j_12127 = _j_12127 + 1;
                if ((long)((unsigned long)_j_12127 +(unsigned long) HIGH_BITS) >= 0){
                    _j_12127 = NewDouble((double)_j_12127);
                }
            }
            else {
                _j_12127 = binary_op_a(PLUS, _j_12127, 1);
            }
            DeRef(_0);
            goto L8; // [264] 243
L9: 
            ;
            DeRef(_j_12127);
        }

        /** 			index += 8*/
        _0 = _index_12087;
        if (IS_ATOM_INT(_index_12087)) {
            _index_12087 = _index_12087 + 8;
            if ((long)((unsigned long)_index_12087 + (unsigned long)HIGH_BITS) >= 0) 
            _index_12087 = NewDouble((double)_index_12087);
        }
        else {
            _index_12087 = NewDouble(DBL_PTR(_index_12087)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _0 = _b_12116;
        if (IS_ATOM_INT(_b_12116)) {
            _b_12116 = _b_12116 + 1;
            if ((long)((unsigned long)_b_12116 +(unsigned long) HIGH_BITS) >= 0){
                _b_12116 = NewDouble((double)_b_12116);
            }
        }
        else {
            _b_12116 = binary_op_a(PLUS, _b_12116, 1);
        }
        DeRef(_0);
        goto L6; // [277] 187
L7: 
        ;
        DeRef(_b_12116);
    }
L5: 

    /** 	return DB_OK*/
    DeRefDS(_name_12084);
    DeRef(_table_12085);
    DeRef(_nkeys_12086);
    DeRef(_index_12087);
    DeRef(_block_ptr_12088);
    DeRef(_block_size_12089);
    DeRef(_6736);
    _6736 = NOVALUE;
    return 0;
    ;
}


int _2db_current_table()
{
    int _0, _1, _2;
    

    /** 	return current_table_name*/
    RefDS(_2current_table_name_10882);
    return _2current_table_name_10882;
    ;
}


int _2db_create_table(int _name_12136, int _init_records_12137)
{
    int _name_ptr_12138 = NOVALUE;
    int _nt_12139 = NOVALUE;
    int _tables_12140 = NOVALUE;
    int _newtables_12141 = NOVALUE;
    int _table_12142 = NOVALUE;
    int _records_ptr_12143 = NOVALUE;
    int _size_12144 = NOVALUE;
    int _newsize_12145 = NOVALUE;
    int _index_ptr_12146 = NOVALUE;
    int _remaining_12147 = NOVALUE;
    int _init_index_12148 = NOVALUE;
    int _seek_1__tmp_at78_12162 = NOVALUE;
    int _seek_inlined_seek_at_78_12161 = NOVALUE;
    int _seek_1__tmp_at107_12168 = NOVALUE;
    int _seek_inlined_seek_at_107_12167 = NOVALUE;
    int _pos_inlined_seek_at_104_12166 = NOVALUE;
    int _put4_1__tmp_at169_12181 = NOVALUE;
    int _seek_1__tmp_at206_12186 = NOVALUE;
    int _seek_inlined_seek_at_206_12185 = NOVALUE;
    int _pos_inlined_seek_at_203_12184 = NOVALUE;
    int _seek_1__tmp_at249_12194 = NOVALUE;
    int _seek_inlined_seek_at_249_12193 = NOVALUE;
    int _pos_inlined_seek_at_246_12192 = NOVALUE;
    int _s_inlined_putn_at_298_12202 = NOVALUE;
    int _seek_1__tmp_at326_12205 = NOVALUE;
    int _seek_inlined_seek_at_326_12204 = NOVALUE;
    int _put4_1__tmp_at341_12207 = NOVALUE;
    int _seek_1__tmp_at379_12211 = NOVALUE;
    int _seek_inlined_seek_at_379_12210 = NOVALUE;
    int _put4_1__tmp_at394_12213 = NOVALUE;
    int _s_inlined_putn_at_441_12219 = NOVALUE;
    int _put4_1__tmp_at472_12223 = NOVALUE;
    int _put4_1__tmp_at500_12225 = NOVALUE;
    int _s_inlined_putn_at_540_12230 = NOVALUE;
    int _s_inlined_putn_at_578_12236 = NOVALUE;
    int _seek_1__tmp_at620_12244 = NOVALUE;
    int _seek_inlined_seek_at_620_12243 = NOVALUE;
    int _pos_inlined_seek_at_617_12242 = NOVALUE;
    int _put4_1__tmp_at635_12246 = NOVALUE;
    int _put4_1__tmp_at663_12248 = NOVALUE;
    int _put4_1__tmp_at691_12250 = NOVALUE;
    int _put4_1__tmp_at719_12252 = NOVALUE;
    int _6789 = NOVALUE;
    int _6788 = NOVALUE;
    int _6787 = NOVALUE;
    int _6786 = NOVALUE;
    int _6785 = NOVALUE;
    int _6784 = NOVALUE;
    int _6782 = NOVALUE;
    int _6781 = NOVALUE;
    int _6780 = NOVALUE;
    int _6779 = NOVALUE;
    int _6778 = NOVALUE;
    int _6776 = NOVALUE;
    int _6775 = NOVALUE;
    int _6774 = NOVALUE;
    int _6772 = NOVALUE;
    int _6771 = NOVALUE;
    int _6770 = NOVALUE;
    int _6769 = NOVALUE;
    int _6768 = NOVALUE;
    int _6767 = NOVALUE;
    int _6766 = NOVALUE;
    int _6764 = NOVALUE;
    int _6763 = NOVALUE;
    int _6762 = NOVALUE;
    int _6759 = NOVALUE;
    int _6758 = NOVALUE;
    int _6756 = NOVALUE;
    int _6755 = NOVALUE;
    int _6753 = NOVALUE;
    int _6751 = NOVALUE;
    int _6748 = NOVALUE;
    int _6743 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_init_records_12137)) {
        _1 = (long)(DBL_PTR(_init_records_12137)->dbl);
        if (UNIQUE(DBL_PTR(_init_records_12137)) && (DBL_PTR(_init_records_12137)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_records_12137);
        _init_records_12137 = _1;
    }

    /** 	if not cstring(name) then*/
    RefDS(_name_12136);
    _6743 = _3cstring(_name_12136);
    if (IS_ATOM_INT(_6743)) {
        if (_6743 != 0){
            DeRef(_6743);
            _6743 = NOVALUE;
            goto L1; // [13] 25
        }
    }
    else {
        if (DBL_PTR(_6743)->dbl != 0.0){
            DeRef(_6743);
            _6743 = NOVALUE;
            goto L1; // [13] 25
        }
    }
    DeRef(_6743);
    _6743 = NOVALUE;

    /** 		return DB_BAD_NAME*/
    DeRefDS(_name_12136);
    DeRef(_name_ptr_12138);
    DeRef(_nt_12139);
    DeRef(_tables_12140);
    DeRef(_newtables_12141);
    DeRef(_table_12142);
    DeRef(_records_ptr_12143);
    DeRef(_size_12144);
    DeRef(_newsize_12145);
    DeRef(_index_ptr_12146);
    DeRef(_remaining_12147);
    return -4;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_12136);
    _0 = _table_12142;
    _table_12142 = _2table_find(_name_12136);
    DeRef(_0);

    /** 	if table != -1 then*/
    if (binary_op_a(EQUALS, _table_12142, -1)){
        goto L2; // [33] 46
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_name_12136);
    DeRef(_name_ptr_12138);
    DeRef(_nt_12139);
    DeRef(_tables_12140);
    DeRef(_newtables_12141);
    DeRef(_table_12142);
    DeRef(_records_ptr_12143);
    DeRef(_size_12144);
    DeRef(_newsize_12145);
    DeRef(_index_ptr_12146);
    DeRef(_remaining_12147);
    return -2;
L2: 

    /** 	if init_records < 1 then*/
    if (_init_records_12137 >= 1)
    goto L3; // [48] 60

    /** 		init_records = 1*/
    _init_records_12137 = 1;
L3: 

    /** 	init_index = math:min({init_records, MAX_INDEX})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _init_records_12137;
    ((int *)_2)[2] = 10;
    _6748 = MAKE_SEQ(_1);
    _init_index_12148 = _17min(_6748);
    _6748 = NOVALUE;
    if (!IS_ATOM_INT(_init_index_12148)) {
        _1 = (long)(DBL_PTR(_init_index_12148)->dbl);
        if (UNIQUE(DBL_PTR(_init_index_12148)) && (DBL_PTR(_init_index_12148)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_index_12148);
        _init_index_12148 = _1;
    }

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at78_12162);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at78_12162 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_78_12161 = machine(19, _seek_1__tmp_at78_12162);
    DeRefi(_seek_1__tmp_at78_12162);
    _seek_1__tmp_at78_12162 = NOVALUE;

    /** 	tables = get4()*/
    _0 = _tables_12140;
    _tables_12140 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables-4)*/
    if (IS_ATOM_INT(_tables_12140)) {
        _6751 = _tables_12140 - 4;
        if ((long)((unsigned long)_6751 +(unsigned long) HIGH_BITS) >= 0){
            _6751 = NewDouble((double)_6751);
        }
    }
    else {
        _6751 = NewDouble(DBL_PTR(_tables_12140)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_104_12166);
    _pos_inlined_seek_at_104_12166 = _6751;
    _6751 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_104_12166);
    DeRef(_seek_1__tmp_at107_12168);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_104_12166;
    _seek_1__tmp_at107_12168 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_107_12167 = machine(19, _seek_1__tmp_at107_12168);
    DeRef(_pos_inlined_seek_at_104_12166);
    _pos_inlined_seek_at_104_12166 = NOVALUE;
    DeRef(_seek_1__tmp_at107_12168);
    _seek_1__tmp_at107_12168 = NOVALUE;

    /** 	size = get4()*/
    _0 = _size_12144;
    _size_12144 = _2get4();
    DeRef(_0);

    /** 	nt = get4()+1*/
    _6753 = _2get4();
    DeRef(_nt_12139);
    if (IS_ATOM_INT(_6753)) {
        _nt_12139 = _6753 + 1;
        if (_nt_12139 > MAXINT){
            _nt_12139 = NewDouble((double)_nt_12139);
        }
    }
    else
    _nt_12139 = binary_op(PLUS, 1, _6753);
    DeRef(_6753);
    _6753 = NOVALUE;

    /** 	if nt*SIZEOF_TABLE_HEADER + 8 > size then*/
    if (IS_ATOM_INT(_nt_12139)) {
        if (_nt_12139 == (short)_nt_12139)
        _6755 = _nt_12139 * 16;
        else
        _6755 = NewDouble(_nt_12139 * (double)16);
    }
    else {
        _6755 = NewDouble(DBL_PTR(_nt_12139)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_6755)) {
        _6756 = _6755 + 8;
        if ((long)((unsigned long)_6756 + (unsigned long)HIGH_BITS) >= 0) 
        _6756 = NewDouble((double)_6756);
    }
    else {
        _6756 = NewDouble(DBL_PTR(_6755)->dbl + (double)8);
    }
    DeRef(_6755);
    _6755 = NOVALUE;
    if (binary_op_a(LESSEQ, _6756, _size_12144)){
        DeRef(_6756);
        _6756 = NOVALUE;
        goto L4; // [144] 375
    }
    DeRef(_6756);
    _6756 = NOVALUE;

    /** 		newsize = floor(size + size / 2)*/
    if (IS_ATOM_INT(_size_12144)) {
        if (_size_12144 & 1) {
            _6758 = NewDouble((_size_12144 >> 1) + 0.5);
        }
        else
        _6758 = _size_12144 >> 1;
    }
    else {
        _6758 = binary_op(DIVIDE, _size_12144, 2);
    }
    if (IS_ATOM_INT(_size_12144) && IS_ATOM_INT(_6758)) {
        _6759 = _size_12144 + _6758;
        if ((long)((unsigned long)_6759 + (unsigned long)HIGH_BITS) >= 0) 
        _6759 = NewDouble((double)_6759);
    }
    else {
        if (IS_ATOM_INT(_size_12144)) {
            _6759 = NewDouble((double)_size_12144 + DBL_PTR(_6758)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6758)) {
                _6759 = NewDouble(DBL_PTR(_size_12144)->dbl + (double)_6758);
            }
            else
            _6759 = NewDouble(DBL_PTR(_size_12144)->dbl + DBL_PTR(_6758)->dbl);
        }
    }
    DeRef(_6758);
    _6758 = NOVALUE;
    DeRef(_newsize_12145);
    if (IS_ATOM_INT(_6759))
    _newsize_12145 = e_floor(_6759);
    else
    _newsize_12145 = unary_op(FLOOR, _6759);
    DeRef(_6759);
    _6759 = NOVALUE;

    /** 		newtables = db_allocate(newsize)*/
    Ref(_newsize_12145);
    _0 = _newtables_12141;
    _newtables_12141 = _2db_allocate(_newsize_12145);
    DeRef(_0);

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_nt_12139)) {
        *poke4_addr = (unsigned long)_nt_12139;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_12139)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at169_12181);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at169_12181 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at169_12181); // DJP 

    /** end procedure*/
    goto L5; // [190] 193
L5: 
    DeRefi(_put4_1__tmp_at169_12181);
    _put4_1__tmp_at169_12181 = NOVALUE;

    /** 		io:seek(current_db, tables+4)*/
    if (IS_ATOM_INT(_tables_12140)) {
        _6762 = _tables_12140 + 4;
        if ((long)((unsigned long)_6762 + (unsigned long)HIGH_BITS) >= 0) 
        _6762 = NewDouble((double)_6762);
    }
    else {
        _6762 = NewDouble(DBL_PTR(_tables_12140)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_203_12184);
    _pos_inlined_seek_at_203_12184 = _6762;
    _6762 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_203_12184);
    DeRef(_seek_1__tmp_at206_12186);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_203_12184;
    _seek_1__tmp_at206_12186 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_206_12185 = machine(19, _seek_1__tmp_at206_12186);
    DeRef(_pos_inlined_seek_at_203_12184);
    _pos_inlined_seek_at_203_12184 = NOVALUE;
    DeRef(_seek_1__tmp_at206_12186);
    _seek_1__tmp_at206_12186 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_nt_12139)) {
        _6763 = _nt_12139 - 1;
        if ((long)((unsigned long)_6763 +(unsigned long) HIGH_BITS) >= 0){
            _6763 = NewDouble((double)_6763);
        }
    }
    else {
        _6763 = NewDouble(DBL_PTR(_nt_12139)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6763)) {
        if (_6763 == (short)_6763)
        _6764 = _6763 * 16;
        else
        _6764 = NewDouble(_6763 * (double)16);
    }
    else {
        _6764 = NewDouble(DBL_PTR(_6763)->dbl * (double)16);
    }
    DeRef(_6763);
    _6763 = NOVALUE;
    _0 = _remaining_12147;
    _remaining_12147 = _15get_bytes(_2current_db_10880, _6764);
    DeRef(_0);
    _6764 = NOVALUE;

    /** 		io:seek(current_db, newtables+4)*/
    if (IS_ATOM_INT(_newtables_12141)) {
        _6766 = _newtables_12141 + 4;
        if ((long)((unsigned long)_6766 + (unsigned long)HIGH_BITS) >= 0) 
        _6766 = NewDouble((double)_6766);
    }
    else {
        _6766 = NewDouble(DBL_PTR(_newtables_12141)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_246_12192);
    _pos_inlined_seek_at_246_12192 = _6766;
    _6766 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_246_12192);
    DeRef(_seek_1__tmp_at249_12194);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_246_12192;
    _seek_1__tmp_at249_12194 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_249_12193 = machine(19, _seek_1__tmp_at249_12194);
    DeRef(_pos_inlined_seek_at_246_12192);
    _pos_inlined_seek_at_246_12192 = NOVALUE;
    DeRef(_seek_1__tmp_at249_12194);
    _seek_1__tmp_at249_12194 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_12147); // DJP 

    /** end procedure*/
    goto L6; // [273] 276
L6: 

    /** 		putn(repeat(0, newsize - 4 - (nt-1)*SIZEOF_TABLE_HEADER))*/
    if (IS_ATOM_INT(_newsize_12145)) {
        _6767 = _newsize_12145 - 4;
        if ((long)((unsigned long)_6767 +(unsigned long) HIGH_BITS) >= 0){
            _6767 = NewDouble((double)_6767);
        }
    }
    else {
        _6767 = NewDouble(DBL_PTR(_newsize_12145)->dbl - (double)4);
    }
    if (IS_ATOM_INT(_nt_12139)) {
        _6768 = _nt_12139 - 1;
        if ((long)((unsigned long)_6768 +(unsigned long) HIGH_BITS) >= 0){
            _6768 = NewDouble((double)_6768);
        }
    }
    else {
        _6768 = NewDouble(DBL_PTR(_nt_12139)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6768)) {
        if (_6768 == (short)_6768)
        _6769 = _6768 * 16;
        else
        _6769 = NewDouble(_6768 * (double)16);
    }
    else {
        _6769 = NewDouble(DBL_PTR(_6768)->dbl * (double)16);
    }
    DeRef(_6768);
    _6768 = NOVALUE;
    if (IS_ATOM_INT(_6767) && IS_ATOM_INT(_6769)) {
        _6770 = _6767 - _6769;
    }
    else {
        if (IS_ATOM_INT(_6767)) {
            _6770 = NewDouble((double)_6767 - DBL_PTR(_6769)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6769)) {
                _6770 = NewDouble(DBL_PTR(_6767)->dbl - (double)_6769);
            }
            else
            _6770 = NewDouble(DBL_PTR(_6767)->dbl - DBL_PTR(_6769)->dbl);
        }
    }
    DeRef(_6767);
    _6767 = NOVALUE;
    DeRef(_6769);
    _6769 = NOVALUE;
    _6771 = Repeat(0, _6770);
    DeRef(_6770);
    _6770 = NOVALUE;
    DeRefi(_s_inlined_putn_at_298_12202);
    _s_inlined_putn_at_298_12202 = _6771;
    _6771 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_298_12202); // DJP 

    /** end procedure*/
    goto L7; // [312] 315
L7: 
    DeRefi(_s_inlined_putn_at_298_12202);
    _s_inlined_putn_at_298_12202 = NOVALUE;

    /** 		db_free(tables)*/
    Ref(_tables_12140);
    _2db_free(_tables_12140);

    /** 		io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at326_12205);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at326_12205 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_326_12204 = machine(19, _seek_1__tmp_at326_12205);
    DeRefi(_seek_1__tmp_at326_12205);
    _seek_1__tmp_at326_12205 = NOVALUE;

    /** 		put4(newtables)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_newtables_12141)) {
        *poke4_addr = (unsigned long)_newtables_12141;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_newtables_12141)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at341_12207);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at341_12207 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at341_12207); // DJP 

    /** end procedure*/
    goto L8; // [362] 365
L8: 
    DeRefi(_put4_1__tmp_at341_12207);
    _put4_1__tmp_at341_12207 = NOVALUE;

    /** 		tables = newtables*/
    Ref(_newtables_12141);
    DeRef(_tables_12140);
    _tables_12140 = _newtables_12141;
    goto L9; // [372] 421
L4: 

    /** 		io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_12140);
    DeRef(_seek_1__tmp_at379_12211);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _tables_12140;
    _seek_1__tmp_at379_12211 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_379_12210 = machine(19, _seek_1__tmp_at379_12211);
    DeRef(_seek_1__tmp_at379_12211);
    _seek_1__tmp_at379_12211 = NOVALUE;

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_nt_12139)) {
        *poke4_addr = (unsigned long)_nt_12139;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_12139)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at394_12213);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at394_12213 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at394_12213); // DJP 

    /** end procedure*/
    goto LA; // [415] 418
LA: 
    DeRefi(_put4_1__tmp_at394_12213);
    _put4_1__tmp_at394_12213 = NOVALUE;
L9: 

    /** 	records_ptr = db_allocate(init_records * 4)*/
    if (_init_records_12137 == (short)_init_records_12137)
    _6772 = _init_records_12137 * 4;
    else
    _6772 = NewDouble(_init_records_12137 * (double)4);
    _0 = _records_ptr_12143;
    _records_ptr_12143 = _2db_allocate(_6772);
    DeRef(_0);
    _6772 = NOVALUE;

    /** 	putn(repeat(0, init_records * 4))*/
    _6774 = _init_records_12137 * 4;
    _6775 = Repeat(0, _6774);
    _6774 = NOVALUE;
    DeRefi(_s_inlined_putn_at_441_12219);
    _s_inlined_putn_at_441_12219 = _6775;
    _6775 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_441_12219); // DJP 

    /** end procedure*/
    goto LB; // [455] 458
LB: 
    DeRefi(_s_inlined_putn_at_441_12219);
    _s_inlined_putn_at_441_12219 = NOVALUE;

    /** 	index_ptr = db_allocate(init_index * 8)*/
    if (_init_index_12148 == (short)_init_index_12148)
    _6776 = _init_index_12148 * 8;
    else
    _6776 = NewDouble(_init_index_12148 * (double)8);
    _0 = _index_ptr_12146;
    _index_ptr_12146 = _2db_allocate(_6776);
    DeRef(_0);
    _6776 = NOVALUE;

    /** 	put4(0)  -- 0 records*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at472_12223);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at472_12223 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at472_12223); // DJP 

    /** end procedure*/
    goto LC; // [493] 496
LC: 
    DeRefi(_put4_1__tmp_at472_12223);
    _put4_1__tmp_at472_12223 = NOVALUE;

    /** 	put4(records_ptr) -- point to 1st block*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_records_ptr_12143)) {
        *poke4_addr = (unsigned long)_records_ptr_12143;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_records_ptr_12143)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at500_12225);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at500_12225 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at500_12225); // DJP 

    /** end procedure*/
    goto LD; // [521] 524
LD: 
    DeRefi(_put4_1__tmp_at500_12225);
    _put4_1__tmp_at500_12225 = NOVALUE;

    /** 	putn(repeat(0, (init_index-1) * 8))*/
    _6778 = _init_index_12148 - 1;
    if ((long)((unsigned long)_6778 +(unsigned long) HIGH_BITS) >= 0){
        _6778 = NewDouble((double)_6778);
    }
    if (IS_ATOM_INT(_6778)) {
        _6779 = _6778 * 8;
    }
    else {
        _6779 = NewDouble(DBL_PTR(_6778)->dbl * (double)8);
    }
    DeRef(_6778);
    _6778 = NOVALUE;
    _6780 = Repeat(0, _6779);
    DeRef(_6779);
    _6779 = NOVALUE;
    DeRefi(_s_inlined_putn_at_540_12230);
    _s_inlined_putn_at_540_12230 = _6780;
    _6780 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_540_12230); // DJP 

    /** end procedure*/
    goto LE; // [554] 557
LE: 
    DeRefi(_s_inlined_putn_at_540_12230);
    _s_inlined_putn_at_540_12230 = NOVALUE;

    /** 	name_ptr = db_allocate(length(name)+1)*/
    if (IS_SEQUENCE(_name_12136)){
            _6781 = SEQ_PTR(_name_12136)->length;
    }
    else {
        _6781 = 1;
    }
    _6782 = _6781 + 1;
    _6781 = NOVALUE;
    _0 = _name_ptr_12138;
    _name_ptr_12138 = _2db_allocate(_6782);
    DeRef(_0);
    _6782 = NOVALUE;

    /** 	putn(name & 0)*/
    Append(&_6784, _name_12136, 0);
    DeRef(_s_inlined_putn_at_578_12236);
    _s_inlined_putn_at_578_12236 = _6784;
    _6784 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_578_12236); // DJP 

    /** end procedure*/
    goto LF; // [592] 595
LF: 
    DeRef(_s_inlined_putn_at_578_12236);
    _s_inlined_putn_at_578_12236 = NOVALUE;

    /** 	io:seek(current_db, tables+4+(nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_tables_12140)) {
        _6785 = _tables_12140 + 4;
        if ((long)((unsigned long)_6785 + (unsigned long)HIGH_BITS) >= 0) 
        _6785 = NewDouble((double)_6785);
    }
    else {
        _6785 = NewDouble(DBL_PTR(_tables_12140)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_nt_12139)) {
        _6786 = _nt_12139 - 1;
        if ((long)((unsigned long)_6786 +(unsigned long) HIGH_BITS) >= 0){
            _6786 = NewDouble((double)_6786);
        }
    }
    else {
        _6786 = NewDouble(DBL_PTR(_nt_12139)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6786)) {
        if (_6786 == (short)_6786)
        _6787 = _6786 * 16;
        else
        _6787 = NewDouble(_6786 * (double)16);
    }
    else {
        _6787 = NewDouble(DBL_PTR(_6786)->dbl * (double)16);
    }
    DeRef(_6786);
    _6786 = NOVALUE;
    if (IS_ATOM_INT(_6785) && IS_ATOM_INT(_6787)) {
        _6788 = _6785 + _6787;
        if ((long)((unsigned long)_6788 + (unsigned long)HIGH_BITS) >= 0) 
        _6788 = NewDouble((double)_6788);
    }
    else {
        if (IS_ATOM_INT(_6785)) {
            _6788 = NewDouble((double)_6785 + DBL_PTR(_6787)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6787)) {
                _6788 = NewDouble(DBL_PTR(_6785)->dbl + (double)_6787);
            }
            else
            _6788 = NewDouble(DBL_PTR(_6785)->dbl + DBL_PTR(_6787)->dbl);
        }
    }
    DeRef(_6785);
    _6785 = NOVALUE;
    DeRef(_6787);
    _6787 = NOVALUE;
    DeRef(_pos_inlined_seek_at_617_12242);
    _pos_inlined_seek_at_617_12242 = _6788;
    _6788 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_617_12242);
    DeRef(_seek_1__tmp_at620_12244);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_617_12242;
    _seek_1__tmp_at620_12244 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_620_12243 = machine(19, _seek_1__tmp_at620_12244);
    DeRef(_pos_inlined_seek_at_617_12242);
    _pos_inlined_seek_at_617_12242 = NOVALUE;
    DeRef(_seek_1__tmp_at620_12244);
    _seek_1__tmp_at620_12244 = NOVALUE;

    /** 	put4(name_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_name_ptr_12138)) {
        *poke4_addr = (unsigned long)_name_ptr_12138;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_name_ptr_12138)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at635_12246);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at635_12246 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at635_12246); // DJP 

    /** end procedure*/
    goto L10; // [656] 659
L10: 
    DeRefi(_put4_1__tmp_at635_12246);
    _put4_1__tmp_at635_12246 = NOVALUE;

    /** 	put4(0)  -- start with 0 records total*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at663_12248);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at663_12248 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at663_12248); // DJP 

    /** end procedure*/
    goto L11; // [684] 687
L11: 
    DeRefi(_put4_1__tmp_at663_12248);
    _put4_1__tmp_at663_12248 = NOVALUE;

    /** 	put4(1)  -- start with 1 block of records in index*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)1;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at691_12250);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at691_12250 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at691_12250); // DJP 

    /** end procedure*/
    goto L12; // [712] 715
L12: 
    DeRefi(_put4_1__tmp_at691_12250);
    _put4_1__tmp_at691_12250 = NOVALUE;

    /** 	put4(index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_index_ptr_12146)) {
        *poke4_addr = (unsigned long)_index_ptr_12146;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_index_ptr_12146)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at719_12252);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at719_12252 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at719_12252); // DJP 

    /** end procedure*/
    goto L13; // [740] 743
L13: 
    DeRefi(_put4_1__tmp_at719_12252);
    _put4_1__tmp_at719_12252 = NOVALUE;

    /** 	if db_select_table(name) then*/
    RefDS(_name_12136);
    _6789 = _2db_select_table(_name_12136);
    if (_6789 == 0) {
        DeRef(_6789);
        _6789 = NOVALUE;
        goto L14; // [751] 755
    }
    else {
        if (!IS_ATOM_INT(_6789) && DBL_PTR(_6789)->dbl == 0.0){
            DeRef(_6789);
            _6789 = NOVALUE;
            goto L14; // [751] 755
        }
        DeRef(_6789);
        _6789 = NOVALUE;
    }
    DeRef(_6789);
    _6789 = NOVALUE;
L14: 

    /** 	return DB_OK*/
    DeRefDS(_name_12136);
    DeRef(_name_ptr_12138);
    DeRef(_nt_12139);
    DeRef(_tables_12140);
    DeRef(_newtables_12141);
    DeRef(_table_12142);
    DeRef(_records_ptr_12143);
    DeRef(_size_12144);
    DeRef(_newsize_12145);
    DeRef(_index_ptr_12146);
    DeRef(_remaining_12147);
    return 0;
    ;
}


void _2db_delete_table(int _name_12257)
{
    int _table_12258 = NOVALUE;
    int _tables_12259 = NOVALUE;
    int _nt_12260 = NOVALUE;
    int _nrecs_12261 = NOVALUE;
    int _records_ptr_12262 = NOVALUE;
    int _blocks_12263 = NOVALUE;
    int _p_12264 = NOVALUE;
    int _data_ptr_12265 = NOVALUE;
    int _index_12266 = NOVALUE;
    int _remaining_12267 = NOVALUE;
    int _k_12268 = NOVALUE;
    int _seek_1__tmp_at24_12274 = NOVALUE;
    int _seek_inlined_seek_at_24_12273 = NOVALUE;
    int _seek_1__tmp_at56_12280 = NOVALUE;
    int _seek_inlined_seek_at_56_12279 = NOVALUE;
    int _pos_inlined_seek_at_53_12278 = NOVALUE;
    int _seek_1__tmp_at112_12292 = NOVALUE;
    int _seek_inlined_seek_at_112_12291 = NOVALUE;
    int _pos_inlined_seek_at_109_12290 = NOVALUE;
    int _seek_1__tmp_at163_12303 = NOVALUE;
    int _seek_inlined_seek_at_163_12302 = NOVALUE;
    int _pos_inlined_seek_at_160_12301 = NOVALUE;
    int _seek_1__tmp_at185_12307 = NOVALUE;
    int _seek_inlined_seek_at_185_12306 = NOVALUE;
    int _seek_1__tmp_at241_12311 = NOVALUE;
    int _seek_inlined_seek_at_241_12310 = NOVALUE;
    int _seek_1__tmp_at263_12315 = NOVALUE;
    int _seek_inlined_seek_at_263_12314 = NOVALUE;
    int _seek_1__tmp_at292_12321 = NOVALUE;
    int _seek_inlined_seek_at_292_12320 = NOVALUE;
    int _pos_inlined_seek_at_289_12319 = NOVALUE;
    int _seek_1__tmp_at340_12330 = NOVALUE;
    int _seek_inlined_seek_at_340_12329 = NOVALUE;
    int _seek_1__tmp_at377_12335 = NOVALUE;
    int _seek_inlined_seek_at_377_12334 = NOVALUE;
    int _put4_1__tmp_at392_12337 = NOVALUE;
    int _seek_1__tmp_at507_12351 = NOVALUE;
    int _seek_inlined_seek_at_507_12350 = NOVALUE;
    int _seek_1__tmp_at529_12355 = NOVALUE;
    int _seek_inlined_seek_at_529_12354 = NOVALUE;
    int _6817 = NOVALUE;
    int _6814 = NOVALUE;
    int _6813 = NOVALUE;
    int _6812 = NOVALUE;
    int _6811 = NOVALUE;
    int _6810 = NOVALUE;
    int _6809 = NOVALUE;
    int _6804 = NOVALUE;
    int _6803 = NOVALUE;
    int _6802 = NOVALUE;
    int _6799 = NOVALUE;
    int _6798 = NOVALUE;
    int _6797 = NOVALUE;
    int _6793 = NOVALUE;
    int _6792 = NOVALUE;
    int _0, _1, _2;
    

    /** 	table = table_find(name)*/
    RefDS(_name_12257);
    _0 = _table_12258;
    _table_12258 = _2table_find(_name_12257);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_12258, -1)){
        goto L1; // [11] 21
    }

    /** 		return*/
    DeRefDS(_name_12257);
    DeRef(_table_12258);
    DeRef(_tables_12259);
    DeRef(_nt_12260);
    DeRef(_nrecs_12261);
    DeRef(_records_ptr_12262);
    DeRef(_blocks_12263);
    DeRef(_p_12264);
    DeRef(_data_ptr_12265);
    DeRef(_index_12266);
    DeRef(_remaining_12267);
    return;
L1: 

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_12258);
    DeRef(_seek_1__tmp_at24_12274);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _table_12258;
    _seek_1__tmp_at24_12274 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_24_12273 = machine(19, _seek_1__tmp_at24_12274);
    DeRef(_seek_1__tmp_at24_12274);
    _seek_1__tmp_at24_12274 = NOVALUE;

    /** 	db_free(get4())*/
    _6792 = _2get4();
    _2db_free(_6792);
    _6792 = NOVALUE;

    /** 	io:seek(current_db, table+4)*/
    if (IS_ATOM_INT(_table_12258)) {
        _6793 = _table_12258 + 4;
        if ((long)((unsigned long)_6793 + (unsigned long)HIGH_BITS) >= 0) 
        _6793 = NewDouble((double)_6793);
    }
    else {
        _6793 = NewDouble(DBL_PTR(_table_12258)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_53_12278);
    _pos_inlined_seek_at_53_12278 = _6793;
    _6793 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_53_12278);
    DeRef(_seek_1__tmp_at56_12280);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_53_12278;
    _seek_1__tmp_at56_12280 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_56_12279 = machine(19, _seek_1__tmp_at56_12280);
    DeRef(_pos_inlined_seek_at_53_12278);
    _pos_inlined_seek_at_53_12278 = NOVALUE;
    DeRef(_seek_1__tmp_at56_12280);
    _seek_1__tmp_at56_12280 = NOVALUE;

    /** 	nrecs = get4()*/
    _0 = _nrecs_12261;
    _nrecs_12261 = _2get4();
    DeRef(_0);

    /** 	blocks = get4()*/
    _0 = _blocks_12263;
    _blocks_12263 = _2get4();
    DeRef(_0);

    /** 	index = get4()*/
    _0 = _index_12266;
    _index_12266 = _2get4();
    DeRef(_0);

    /** 	for b = 0 to blocks-1 do*/
    if (IS_ATOM_INT(_blocks_12263)) {
        _6797 = _blocks_12263 - 1;
        if ((long)((unsigned long)_6797 +(unsigned long) HIGH_BITS) >= 0){
            _6797 = NewDouble((double)_6797);
        }
    }
    else {
        _6797 = NewDouble(DBL_PTR(_blocks_12263)->dbl - (double)1);
    }
    {
        int _b_12285;
        _b_12285 = 0;
L2: 
        if (binary_op_a(GREATER, _b_12285, _6797)){
            goto L3; // [91] 233
        }

        /** 		io:seek(current_db, index+b*8)*/
        if (IS_ATOM_INT(_b_12285)) {
            if (_b_12285 == (short)_b_12285)
            _6798 = _b_12285 * 8;
            else
            _6798 = NewDouble(_b_12285 * (double)8);
        }
        else {
            _6798 = NewDouble(DBL_PTR(_b_12285)->dbl * (double)8);
        }
        if (IS_ATOM_INT(_index_12266) && IS_ATOM_INT(_6798)) {
            _6799 = _index_12266 + _6798;
            if ((long)((unsigned long)_6799 + (unsigned long)HIGH_BITS) >= 0) 
            _6799 = NewDouble((double)_6799);
        }
        else {
            if (IS_ATOM_INT(_index_12266)) {
                _6799 = NewDouble((double)_index_12266 + DBL_PTR(_6798)->dbl);
            }
            else {
                if (IS_ATOM_INT(_6798)) {
                    _6799 = NewDouble(DBL_PTR(_index_12266)->dbl + (double)_6798);
                }
                else
                _6799 = NewDouble(DBL_PTR(_index_12266)->dbl + DBL_PTR(_6798)->dbl);
            }
        }
        DeRef(_6798);
        _6798 = NOVALUE;
        DeRef(_pos_inlined_seek_at_109_12290);
        _pos_inlined_seek_at_109_12290 = _6799;
        _6799 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_109_12290);
        DeRef(_seek_1__tmp_at112_12292);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _pos_inlined_seek_at_109_12290;
        _seek_1__tmp_at112_12292 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_112_12291 = machine(19, _seek_1__tmp_at112_12292);
        DeRef(_pos_inlined_seek_at_109_12290);
        _pos_inlined_seek_at_109_12290 = NOVALUE;
        DeRef(_seek_1__tmp_at112_12292);
        _seek_1__tmp_at112_12292 = NOVALUE;

        /** 		nrecs = get4()*/
        _0 = _nrecs_12261;
        _nrecs_12261 = _2get4();
        DeRef(_0);

        /** 		records_ptr = get4()*/
        _0 = _records_ptr_12262;
        _records_ptr_12262 = _2get4();
        DeRef(_0);

        /** 		for r = 0 to nrecs-1 do*/
        if (IS_ATOM_INT(_nrecs_12261)) {
            _6802 = _nrecs_12261 - 1;
            if ((long)((unsigned long)_6802 +(unsigned long) HIGH_BITS) >= 0){
                _6802 = NewDouble((double)_6802);
            }
        }
        else {
            _6802 = NewDouble(DBL_PTR(_nrecs_12261)->dbl - (double)1);
        }
        {
            int _r_12296;
            _r_12296 = 0;
L4: 
            if (binary_op_a(GREATER, _r_12296, _6802)){
                goto L5; // [142] 221
            }

            /** 			io:seek(current_db, records_ptr + r*4)*/
            if (IS_ATOM_INT(_r_12296)) {
                if (_r_12296 == (short)_r_12296)
                _6803 = _r_12296 * 4;
                else
                _6803 = NewDouble(_r_12296 * (double)4);
            }
            else {
                _6803 = NewDouble(DBL_PTR(_r_12296)->dbl * (double)4);
            }
            if (IS_ATOM_INT(_records_ptr_12262) && IS_ATOM_INT(_6803)) {
                _6804 = _records_ptr_12262 + _6803;
                if ((long)((unsigned long)_6804 + (unsigned long)HIGH_BITS) >= 0) 
                _6804 = NewDouble((double)_6804);
            }
            else {
                if (IS_ATOM_INT(_records_ptr_12262)) {
                    _6804 = NewDouble((double)_records_ptr_12262 + DBL_PTR(_6803)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_6803)) {
                        _6804 = NewDouble(DBL_PTR(_records_ptr_12262)->dbl + (double)_6803);
                    }
                    else
                    _6804 = NewDouble(DBL_PTR(_records_ptr_12262)->dbl + DBL_PTR(_6803)->dbl);
                }
            }
            DeRef(_6803);
            _6803 = NOVALUE;
            DeRef(_pos_inlined_seek_at_160_12301);
            _pos_inlined_seek_at_160_12301 = _6804;
            _6804 = NOVALUE;

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_pos_inlined_seek_at_160_12301);
            DeRef(_seek_1__tmp_at163_12303);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _2current_db_10880;
            ((int *)_2)[2] = _pos_inlined_seek_at_160_12301;
            _seek_1__tmp_at163_12303 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_163_12302 = machine(19, _seek_1__tmp_at163_12303);
            DeRef(_pos_inlined_seek_at_160_12301);
            _pos_inlined_seek_at_160_12301 = NOVALUE;
            DeRef(_seek_1__tmp_at163_12303);
            _seek_1__tmp_at163_12303 = NOVALUE;

            /** 			p = get4()*/
            _0 = _p_12264;
            _p_12264 = _2get4();
            DeRef(_0);

            /** 			io:seek(current_db, p)*/

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_p_12264);
            DeRef(_seek_1__tmp_at185_12307);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _2current_db_10880;
            ((int *)_2)[2] = _p_12264;
            _seek_1__tmp_at185_12307 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_185_12306 = machine(19, _seek_1__tmp_at185_12307);
            DeRef(_seek_1__tmp_at185_12307);
            _seek_1__tmp_at185_12307 = NOVALUE;

            /** 			data_ptr = get4()*/
            _0 = _data_ptr_12265;
            _data_ptr_12265 = _2get4();
            DeRef(_0);

            /** 			db_free(data_ptr)*/
            Ref(_data_ptr_12265);
            _2db_free(_data_ptr_12265);

            /** 			db_free(p)*/
            Ref(_p_12264);
            _2db_free(_p_12264);

            /** 		end for*/
            _0 = _r_12296;
            if (IS_ATOM_INT(_r_12296)) {
                _r_12296 = _r_12296 + 1;
                if ((long)((unsigned long)_r_12296 +(unsigned long) HIGH_BITS) >= 0){
                    _r_12296 = NewDouble((double)_r_12296);
                }
            }
            else {
                _r_12296 = binary_op_a(PLUS, _r_12296, 1);
            }
            DeRef(_0);
            goto L4; // [216] 149
L5: 
            ;
            DeRef(_r_12296);
        }

        /** 		db_free(records_ptr)*/
        Ref(_records_ptr_12262);
        _2db_free(_records_ptr_12262);

        /** 	end for*/
        _0 = _b_12285;
        if (IS_ATOM_INT(_b_12285)) {
            _b_12285 = _b_12285 + 1;
            if ((long)((unsigned long)_b_12285 +(unsigned long) HIGH_BITS) >= 0){
                _b_12285 = NewDouble((double)_b_12285);
            }
        }
        else {
            _b_12285 = binary_op_a(PLUS, _b_12285, 1);
        }
        DeRef(_0);
        goto L2; // [228] 98
L3: 
        ;
        DeRef(_b_12285);
    }

    /** 	db_free(index)*/
    Ref(_index_12266);
    _2db_free(_index_12266);

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at241_12311);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at241_12311 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_241_12310 = machine(19, _seek_1__tmp_at241_12311);
    DeRefi(_seek_1__tmp_at241_12311);
    _seek_1__tmp_at241_12311 = NOVALUE;

    /** 	tables = get4()*/
    _0 = _tables_12259;
    _tables_12259 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_12259);
    DeRef(_seek_1__tmp_at263_12315);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _tables_12259;
    _seek_1__tmp_at263_12315 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_263_12314 = machine(19, _seek_1__tmp_at263_12315);
    DeRef(_seek_1__tmp_at263_12315);
    _seek_1__tmp_at263_12315 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_12260;
    _nt_12260 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, table+SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_table_12258)) {
        _6809 = _table_12258 + 16;
        if ((long)((unsigned long)_6809 + (unsigned long)HIGH_BITS) >= 0) 
        _6809 = NewDouble((double)_6809);
    }
    else {
        _6809 = NewDouble(DBL_PTR(_table_12258)->dbl + (double)16);
    }
    DeRef(_pos_inlined_seek_at_289_12319);
    _pos_inlined_seek_at_289_12319 = _6809;
    _6809 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_289_12319);
    DeRef(_seek_1__tmp_at292_12321);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_289_12319;
    _seek_1__tmp_at292_12321 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_292_12320 = machine(19, _seek_1__tmp_at292_12321);
    DeRef(_pos_inlined_seek_at_289_12319);
    _pos_inlined_seek_at_289_12319 = NOVALUE;
    DeRef(_seek_1__tmp_at292_12321);
    _seek_1__tmp_at292_12321 = NOVALUE;

    /** 	remaining = io:get_bytes(current_db,*/
    if (IS_ATOM_INT(_tables_12259)) {
        _6810 = _tables_12259 + 4;
        if ((long)((unsigned long)_6810 + (unsigned long)HIGH_BITS) >= 0) 
        _6810 = NewDouble((double)_6810);
    }
    else {
        _6810 = NewDouble(DBL_PTR(_tables_12259)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_nt_12260)) {
        if (_nt_12260 == (short)_nt_12260)
        _6811 = _nt_12260 * 16;
        else
        _6811 = NewDouble(_nt_12260 * (double)16);
    }
    else {
        _6811 = NewDouble(DBL_PTR(_nt_12260)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_6810) && IS_ATOM_INT(_6811)) {
        _6812 = _6810 + _6811;
        if ((long)((unsigned long)_6812 + (unsigned long)HIGH_BITS) >= 0) 
        _6812 = NewDouble((double)_6812);
    }
    else {
        if (IS_ATOM_INT(_6810)) {
            _6812 = NewDouble((double)_6810 + DBL_PTR(_6811)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6811)) {
                _6812 = NewDouble(DBL_PTR(_6810)->dbl + (double)_6811);
            }
            else
            _6812 = NewDouble(DBL_PTR(_6810)->dbl + DBL_PTR(_6811)->dbl);
        }
    }
    DeRef(_6810);
    _6810 = NOVALUE;
    DeRef(_6811);
    _6811 = NOVALUE;
    if (IS_ATOM_INT(_table_12258)) {
        _6813 = _table_12258 + 16;
        if ((long)((unsigned long)_6813 + (unsigned long)HIGH_BITS) >= 0) 
        _6813 = NewDouble((double)_6813);
    }
    else {
        _6813 = NewDouble(DBL_PTR(_table_12258)->dbl + (double)16);
    }
    if (IS_ATOM_INT(_6812) && IS_ATOM_INT(_6813)) {
        _6814 = _6812 - _6813;
        if ((long)((unsigned long)_6814 +(unsigned long) HIGH_BITS) >= 0){
            _6814 = NewDouble((double)_6814);
        }
    }
    else {
        if (IS_ATOM_INT(_6812)) {
            _6814 = NewDouble((double)_6812 - DBL_PTR(_6813)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6813)) {
                _6814 = NewDouble(DBL_PTR(_6812)->dbl - (double)_6813);
            }
            else
            _6814 = NewDouble(DBL_PTR(_6812)->dbl - DBL_PTR(_6813)->dbl);
        }
    }
    DeRef(_6812);
    _6812 = NOVALUE;
    DeRef(_6813);
    _6813 = NOVALUE;
    _0 = _remaining_12267;
    _remaining_12267 = _15get_bytes(_2current_db_10880, _6814);
    DeRef(_0);
    _6814 = NOVALUE;

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_12258);
    DeRef(_seek_1__tmp_at340_12330);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _table_12258;
    _seek_1__tmp_at340_12330 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_340_12329 = machine(19, _seek_1__tmp_at340_12330);
    DeRef(_seek_1__tmp_at340_12330);
    _seek_1__tmp_at340_12330 = NOVALUE;

    /** 	putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_12267); // DJP 

    /** end procedure*/
    goto L6; // [365] 368
L6: 

    /** 	nt -= 1*/
    _0 = _nt_12260;
    if (IS_ATOM_INT(_nt_12260)) {
        _nt_12260 = _nt_12260 - 1;
        if ((long)((unsigned long)_nt_12260 +(unsigned long) HIGH_BITS) >= 0){
            _nt_12260 = NewDouble((double)_nt_12260);
        }
    }
    else {
        _nt_12260 = NewDouble(DBL_PTR(_nt_12260)->dbl - (double)1);
    }
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_12259);
    DeRef(_seek_1__tmp_at377_12335);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _tables_12259;
    _seek_1__tmp_at377_12335 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_377_12334 = machine(19, _seek_1__tmp_at377_12335);
    DeRef(_seek_1__tmp_at377_12335);
    _seek_1__tmp_at377_12335 = NOVALUE;

    /** 	put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_nt_12260)) {
        *poke4_addr = (unsigned long)_nt_12260;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_12260)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at392_12337);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at392_12337 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at392_12337); // DJP 

    /** end procedure*/
    goto L7; // [414] 417
L7: 
    DeRefi(_put4_1__tmp_at392_12337);
    _put4_1__tmp_at392_12337 = NOVALUE;

    /** 	k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_2current_table_pos_10881);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _2current_table_pos_10881;
    _6817 = MAKE_SEQ(_1);
    _k_12268 = find_from(_6817, _2cache_index_10889, 1);
    DeRefDS(_6817);
    _6817 = NOVALUE;

    /** 	if k != 0 then*/
    if (_k_12268 == 0)
    goto L8; // [440] 463

    /** 		cache_index = remove(cache_index, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2cache_index_10889);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_12268)) ? _k_12268 : (long)(DBL_PTR(_k_12268)->dbl);
        int stop = (IS_ATOM_INT(_k_12268)) ? _k_12268 : (long)(DBL_PTR(_k_12268)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2cache_index_10889), start, &_2cache_index_10889 );
            }
            else Tail(SEQ_PTR(_2cache_index_10889), stop+1, &_2cache_index_10889);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2cache_index_10889), start, &_2cache_index_10889);
        }
        else {
            assign_slice_seq = &assign_space;
            _2cache_index_10889 = Remove_elements(start, stop, (SEQ_PTR(_2cache_index_10889)->ref == 1));
        }
    }

    /** 		key_cache = remove(key_cache, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2key_cache_10888);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_12268)) ? _k_12268 : (long)(DBL_PTR(_k_12268)->dbl);
        int stop = (IS_ATOM_INT(_k_12268)) ? _k_12268 : (long)(DBL_PTR(_k_12268)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2key_cache_10888), start, &_2key_cache_10888 );
            }
            else Tail(SEQ_PTR(_2key_cache_10888), stop+1, &_2key_cache_10888);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2key_cache_10888), start, &_2key_cache_10888);
        }
        else {
            assign_slice_seq = &assign_space;
            _2key_cache_10888 = Remove_elements(start, stop, (SEQ_PTR(_2key_cache_10888)->ref == 1));
        }
    }
L8: 

    /** 	if table = current_table_pos then*/
    if (binary_op_a(NOTEQ, _table_12258, _2current_table_pos_10881)){
        goto L9; // [467] 486
    }

    /** 		current_table_pos = -1*/
    DeRef(_2current_table_pos_10881);
    _2current_table_pos_10881 = -1;

    /** 		current_table_name = ""*/
    RefDS(_5);
    DeRef(_2current_table_name_10882);
    _2current_table_name_10882 = _5;
    goto LA; // [483] 552
L9: 

    /** 	elsif table < current_table_pos then*/
    if (binary_op_a(GREATEREQ, _table_12258, _2current_table_pos_10881)){
        goto LB; // [490] 551
    }

    /** 		current_table_pos -= SIZEOF_TABLE_HEADER*/
    _0 = _2current_table_pos_10881;
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _2current_table_pos_10881 = _2current_table_pos_10881 - 16;
        if ((long)((unsigned long)_2current_table_pos_10881 +(unsigned long) HIGH_BITS) >= 0){
            _2current_table_pos_10881 = NewDouble((double)_2current_table_pos_10881);
        }
    }
    else {
        _2current_table_pos_10881 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl - (double)16);
    }
    DeRef(_0);

    /** 		io:seek(current_db, current_table_pos)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_2current_table_pos_10881);
    DeRef(_seek_1__tmp_at507_12351);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _2current_table_pos_10881;
    _seek_1__tmp_at507_12351 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_507_12350 = machine(19, _seek_1__tmp_at507_12351);
    DeRef(_seek_1__tmp_at507_12351);
    _seek_1__tmp_at507_12351 = NOVALUE;

    /** 		data_ptr = get4()*/
    _0 = _data_ptr_12265;
    _data_ptr_12265 = _2get4();
    DeRef(_0);

    /** 		io:seek(current_db, data_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_data_ptr_12265);
    DeRef(_seek_1__tmp_at529_12355);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _data_ptr_12265;
    _seek_1__tmp_at529_12355 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_529_12354 = machine(19, _seek_1__tmp_at529_12355);
    DeRef(_seek_1__tmp_at529_12355);
    _seek_1__tmp_at529_12355 = NOVALUE;

    /** 		current_table_name = get_string()*/
    _0 = _2get_string();
    DeRef(_2current_table_name_10882);
    _2current_table_name_10882 = _0;
LB: 
LA: 

    /** end procedure*/
    DeRefDS(_name_12257);
    DeRef(_table_12258);
    DeRef(_tables_12259);
    DeRef(_nt_12260);
    DeRef(_nrecs_12261);
    DeRef(_records_ptr_12262);
    DeRef(_blocks_12263);
    DeRef(_p_12264);
    DeRef(_data_ptr_12265);
    DeRef(_index_12266);
    DeRef(_remaining_12267);
    DeRef(_6797);
    _6797 = NOVALUE;
    DeRef(_6802);
    _6802 = NOVALUE;
    return;
    ;
}


void _2db_clear_table(int _name_12359, int _init_records_12360)
{
    int _table_12361 = NOVALUE;
    int _nrecs_12362 = NOVALUE;
    int _records_ptr_12363 = NOVALUE;
    int _blocks_12364 = NOVALUE;
    int _p_12365 = NOVALUE;
    int _data_ptr_12366 = NOVALUE;
    int _index_ptr_12367 = NOVALUE;
    int _k_12368 = NOVALUE;
    int _init_index_12369 = NOVALUE;
    int _seek_1__tmp_at63_12381 = NOVALUE;
    int _seek_inlined_seek_at_63_12380 = NOVALUE;
    int _pos_inlined_seek_at_60_12379 = NOVALUE;
    int _seek_1__tmp_at119_12393 = NOVALUE;
    int _seek_inlined_seek_at_119_12392 = NOVALUE;
    int _pos_inlined_seek_at_116_12391 = NOVALUE;
    int _seek_1__tmp_at170_12404 = NOVALUE;
    int _seek_inlined_seek_at_170_12403 = NOVALUE;
    int _pos_inlined_seek_at_167_12402 = NOVALUE;
    int _seek_1__tmp_at192_12408 = NOVALUE;
    int _seek_inlined_seek_at_192_12407 = NOVALUE;
    int _s_inlined_putn_at_264_12415 = NOVALUE;
    int _put4_1__tmp_at295_12419 = NOVALUE;
    int _put4_1__tmp_at323_12421 = NOVALUE;
    int _s_inlined_putn_at_363_12426 = NOVALUE;
    int _seek_1__tmp_at393_12431 = NOVALUE;
    int _seek_inlined_seek_at_393_12430 = NOVALUE;
    int _pos_inlined_seek_at_390_12429 = NOVALUE;
    int _put4_1__tmp_at408_12433 = NOVALUE;
    int _put4_1__tmp_at436_12435 = NOVALUE;
    int _put4_1__tmp_at464_12437 = NOVALUE;
    int _6856 = NOVALUE;
    int _6855 = NOVALUE;
    int _6854 = NOVALUE;
    int _6853 = NOVALUE;
    int _6852 = NOVALUE;
    int _6850 = NOVALUE;
    int _6849 = NOVALUE;
    int _6848 = NOVALUE;
    int _6846 = NOVALUE;
    int _6843 = NOVALUE;
    int _6842 = NOVALUE;
    int _6841 = NOVALUE;
    int _6838 = NOVALUE;
    int _6837 = NOVALUE;
    int _6836 = NOVALUE;
    int _6832 = NOVALUE;
    int _6830 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_init_records_12360)) {
        _1 = (long)(DBL_PTR(_init_records_12360)->dbl);
        if (UNIQUE(DBL_PTR(_init_records_12360)) && (DBL_PTR(_init_records_12360)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_records_12360);
        _init_records_12360 = _1;
    }

    /** 	table = table_find(name)*/
    RefDS(_name_12359);
    _0 = _table_12361;
    _table_12361 = _2table_find(_name_12359);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_12361, -1)){
        goto L1; // [15] 25
    }

    /** 		return*/
    DeRefDS(_name_12359);
    DeRef(_table_12361);
    DeRef(_nrecs_12362);
    DeRef(_records_ptr_12363);
    DeRef(_blocks_12364);
    DeRef(_p_12365);
    DeRef(_data_ptr_12366);
    DeRef(_index_ptr_12367);
    return;
L1: 

    /** 	if init_records < 1 then*/
    if (_init_records_12360 >= 1)
    goto L2; // [27] 39

    /** 		init_records = 1*/
    _init_records_12360 = 1;
L2: 

    /** 	init_index = math:min({init_records, MAX_INDEX})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _init_records_12360;
    ((int *)_2)[2] = 10;
    _6830 = MAKE_SEQ(_1);
    _init_index_12369 = _17min(_6830);
    _6830 = NOVALUE;
    if (!IS_ATOM_INT(_init_index_12369)) {
        _1 = (long)(DBL_PTR(_init_index_12369)->dbl);
        if (UNIQUE(DBL_PTR(_init_index_12369)) && (DBL_PTR(_init_index_12369)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_index_12369);
        _init_index_12369 = _1;
    }

    /** 	io:seek(current_db, table + 4)*/
    if (IS_ATOM_INT(_table_12361)) {
        _6832 = _table_12361 + 4;
        if ((long)((unsigned long)_6832 + (unsigned long)HIGH_BITS) >= 0) 
        _6832 = NewDouble((double)_6832);
    }
    else {
        _6832 = NewDouble(DBL_PTR(_table_12361)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_60_12379);
    _pos_inlined_seek_at_60_12379 = _6832;
    _6832 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_60_12379);
    DeRef(_seek_1__tmp_at63_12381);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_60_12379;
    _seek_1__tmp_at63_12381 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_63_12380 = machine(19, _seek_1__tmp_at63_12381);
    DeRef(_pos_inlined_seek_at_60_12379);
    _pos_inlined_seek_at_60_12379 = NOVALUE;
    DeRef(_seek_1__tmp_at63_12381);
    _seek_1__tmp_at63_12381 = NOVALUE;

    /** 	nrecs = get4()*/
    _0 = _nrecs_12362;
    _nrecs_12362 = _2get4();
    DeRef(_0);

    /** 	blocks = get4()*/
    _0 = _blocks_12364;
    _blocks_12364 = _2get4();
    DeRef(_0);

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_12367;
    _index_ptr_12367 = _2get4();
    DeRef(_0);

    /** 	for b = 0 to blocks-1 do*/
    if (IS_ATOM_INT(_blocks_12364)) {
        _6836 = _blocks_12364 - 1;
        if ((long)((unsigned long)_6836 +(unsigned long) HIGH_BITS) >= 0){
            _6836 = NewDouble((double)_6836);
        }
    }
    else {
        _6836 = NewDouble(DBL_PTR(_blocks_12364)->dbl - (double)1);
    }
    {
        int _b_12386;
        _b_12386 = 0;
L3: 
        if (binary_op_a(GREATER, _b_12386, _6836)){
            goto L4; // [98] 240
        }

        /** 		io:seek(current_db, index_ptr + b*8)*/
        if (IS_ATOM_INT(_b_12386)) {
            if (_b_12386 == (short)_b_12386)
            _6837 = _b_12386 * 8;
            else
            _6837 = NewDouble(_b_12386 * (double)8);
        }
        else {
            _6837 = NewDouble(DBL_PTR(_b_12386)->dbl * (double)8);
        }
        if (IS_ATOM_INT(_index_ptr_12367) && IS_ATOM_INT(_6837)) {
            _6838 = _index_ptr_12367 + _6837;
            if ((long)((unsigned long)_6838 + (unsigned long)HIGH_BITS) >= 0) 
            _6838 = NewDouble((double)_6838);
        }
        else {
            if (IS_ATOM_INT(_index_ptr_12367)) {
                _6838 = NewDouble((double)_index_ptr_12367 + DBL_PTR(_6837)->dbl);
            }
            else {
                if (IS_ATOM_INT(_6837)) {
                    _6838 = NewDouble(DBL_PTR(_index_ptr_12367)->dbl + (double)_6837);
                }
                else
                _6838 = NewDouble(DBL_PTR(_index_ptr_12367)->dbl + DBL_PTR(_6837)->dbl);
            }
        }
        DeRef(_6837);
        _6837 = NOVALUE;
        DeRef(_pos_inlined_seek_at_116_12391);
        _pos_inlined_seek_at_116_12391 = _6838;
        _6838 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_116_12391);
        DeRef(_seek_1__tmp_at119_12393);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _pos_inlined_seek_at_116_12391;
        _seek_1__tmp_at119_12393 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_119_12392 = machine(19, _seek_1__tmp_at119_12393);
        DeRef(_pos_inlined_seek_at_116_12391);
        _pos_inlined_seek_at_116_12391 = NOVALUE;
        DeRef(_seek_1__tmp_at119_12393);
        _seek_1__tmp_at119_12393 = NOVALUE;

        /** 		nrecs = get4()*/
        _0 = _nrecs_12362;
        _nrecs_12362 = _2get4();
        DeRef(_0);

        /** 		records_ptr = get4()*/
        _0 = _records_ptr_12363;
        _records_ptr_12363 = _2get4();
        DeRef(_0);

        /** 		for r = 0 to nrecs-1 do*/
        if (IS_ATOM_INT(_nrecs_12362)) {
            _6841 = _nrecs_12362 - 1;
            if ((long)((unsigned long)_6841 +(unsigned long) HIGH_BITS) >= 0){
                _6841 = NewDouble((double)_6841);
            }
        }
        else {
            _6841 = NewDouble(DBL_PTR(_nrecs_12362)->dbl - (double)1);
        }
        {
            int _r_12397;
            _r_12397 = 0;
L5: 
            if (binary_op_a(GREATER, _r_12397, _6841)){
                goto L6; // [149] 228
            }

            /** 			io:seek(current_db, records_ptr + r*4)*/
            if (IS_ATOM_INT(_r_12397)) {
                if (_r_12397 == (short)_r_12397)
                _6842 = _r_12397 * 4;
                else
                _6842 = NewDouble(_r_12397 * (double)4);
            }
            else {
                _6842 = NewDouble(DBL_PTR(_r_12397)->dbl * (double)4);
            }
            if (IS_ATOM_INT(_records_ptr_12363) && IS_ATOM_INT(_6842)) {
                _6843 = _records_ptr_12363 + _6842;
                if ((long)((unsigned long)_6843 + (unsigned long)HIGH_BITS) >= 0) 
                _6843 = NewDouble((double)_6843);
            }
            else {
                if (IS_ATOM_INT(_records_ptr_12363)) {
                    _6843 = NewDouble((double)_records_ptr_12363 + DBL_PTR(_6842)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_6842)) {
                        _6843 = NewDouble(DBL_PTR(_records_ptr_12363)->dbl + (double)_6842);
                    }
                    else
                    _6843 = NewDouble(DBL_PTR(_records_ptr_12363)->dbl + DBL_PTR(_6842)->dbl);
                }
            }
            DeRef(_6842);
            _6842 = NOVALUE;
            DeRef(_pos_inlined_seek_at_167_12402);
            _pos_inlined_seek_at_167_12402 = _6843;
            _6843 = NOVALUE;

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_pos_inlined_seek_at_167_12402);
            DeRef(_seek_1__tmp_at170_12404);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _2current_db_10880;
            ((int *)_2)[2] = _pos_inlined_seek_at_167_12402;
            _seek_1__tmp_at170_12404 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_170_12403 = machine(19, _seek_1__tmp_at170_12404);
            DeRef(_pos_inlined_seek_at_167_12402);
            _pos_inlined_seek_at_167_12402 = NOVALUE;
            DeRef(_seek_1__tmp_at170_12404);
            _seek_1__tmp_at170_12404 = NOVALUE;

            /** 			p = get4()*/
            _0 = _p_12365;
            _p_12365 = _2get4();
            DeRef(_0);

            /** 			io:seek(current_db, p)*/

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_p_12365);
            DeRef(_seek_1__tmp_at192_12408);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _2current_db_10880;
            ((int *)_2)[2] = _p_12365;
            _seek_1__tmp_at192_12408 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_192_12407 = machine(19, _seek_1__tmp_at192_12408);
            DeRef(_seek_1__tmp_at192_12408);
            _seek_1__tmp_at192_12408 = NOVALUE;

            /** 			data_ptr = get4()*/
            _0 = _data_ptr_12366;
            _data_ptr_12366 = _2get4();
            DeRef(_0);

            /** 			db_free(data_ptr)*/
            Ref(_data_ptr_12366);
            _2db_free(_data_ptr_12366);

            /** 			db_free(p)*/
            Ref(_p_12365);
            _2db_free(_p_12365);

            /** 		end for*/
            _0 = _r_12397;
            if (IS_ATOM_INT(_r_12397)) {
                _r_12397 = _r_12397 + 1;
                if ((long)((unsigned long)_r_12397 +(unsigned long) HIGH_BITS) >= 0){
                    _r_12397 = NewDouble((double)_r_12397);
                }
            }
            else {
                _r_12397 = binary_op_a(PLUS, _r_12397, 1);
            }
            DeRef(_0);
            goto L5; // [223] 156
L6: 
            ;
            DeRef(_r_12397);
        }

        /** 		db_free(records_ptr)*/
        Ref(_records_ptr_12363);
        _2db_free(_records_ptr_12363);

        /** 	end for*/
        _0 = _b_12386;
        if (IS_ATOM_INT(_b_12386)) {
            _b_12386 = _b_12386 + 1;
            if ((long)((unsigned long)_b_12386 +(unsigned long) HIGH_BITS) >= 0){
                _b_12386 = NewDouble((double)_b_12386);
            }
        }
        else {
            _b_12386 = binary_op_a(PLUS, _b_12386, 1);
        }
        DeRef(_0);
        goto L3; // [235] 105
L4: 
        ;
        DeRef(_b_12386);
    }

    /** 	db_free(index_ptr)*/
    Ref(_index_ptr_12367);
    _2db_free(_index_ptr_12367);

    /** 	data_ptr = db_allocate(init_records * 4)*/
    if (_init_records_12360 == (short)_init_records_12360)
    _6846 = _init_records_12360 * 4;
    else
    _6846 = NewDouble(_init_records_12360 * (double)4);
    _0 = _data_ptr_12366;
    _data_ptr_12366 = _2db_allocate(_6846);
    DeRef(_0);
    _6846 = NOVALUE;

    /** 	putn(repeat(0, init_records * 4))*/
    _6848 = _init_records_12360 * 4;
    _6849 = Repeat(0, _6848);
    _6848 = NOVALUE;
    DeRefi(_s_inlined_putn_at_264_12415);
    _s_inlined_putn_at_264_12415 = _6849;
    _6849 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_264_12415); // DJP 

    /** end procedure*/
    goto L7; // [279] 282
L7: 
    DeRefi(_s_inlined_putn_at_264_12415);
    _s_inlined_putn_at_264_12415 = NOVALUE;

    /** 	index_ptr = db_allocate(init_index * 8)*/
    if (_init_index_12369 == (short)_init_index_12369)
    _6850 = _init_index_12369 * 8;
    else
    _6850 = NewDouble(_init_index_12369 * (double)8);
    _0 = _index_ptr_12367;
    _index_ptr_12367 = _2db_allocate(_6850);
    DeRef(_0);
    _6850 = NOVALUE;

    /** 	put4(0)  -- 0 records*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at295_12419);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at295_12419 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at295_12419); // DJP 

    /** end procedure*/
    goto L8; // [317] 320
L8: 
    DeRefi(_put4_1__tmp_at295_12419);
    _put4_1__tmp_at295_12419 = NOVALUE;

    /** 	put4(data_ptr) -- point to 1st block*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_12366)) {
        *poke4_addr = (unsigned long)_data_ptr_12366;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_12366)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at323_12421);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at323_12421 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at323_12421); // DJP 

    /** end procedure*/
    goto L9; // [345] 348
L9: 
    DeRefi(_put4_1__tmp_at323_12421);
    _put4_1__tmp_at323_12421 = NOVALUE;

    /** 	putn(repeat(0, (init_index-1) * 8))*/
    _6852 = _init_index_12369 - 1;
    if ((long)((unsigned long)_6852 +(unsigned long) HIGH_BITS) >= 0){
        _6852 = NewDouble((double)_6852);
    }
    if (IS_ATOM_INT(_6852)) {
        _6853 = _6852 * 8;
    }
    else {
        _6853 = NewDouble(DBL_PTR(_6852)->dbl * (double)8);
    }
    DeRef(_6852);
    _6852 = NOVALUE;
    _6854 = Repeat(0, _6853);
    DeRef(_6853);
    _6853 = NOVALUE;
    DeRefi(_s_inlined_putn_at_363_12426);
    _s_inlined_putn_at_363_12426 = _6854;
    _6854 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_363_12426); // DJP 

    /** end procedure*/
    goto LA; // [378] 381
LA: 
    DeRefi(_s_inlined_putn_at_363_12426);
    _s_inlined_putn_at_363_12426 = NOVALUE;

    /** 	io:seek(current_db, table + 4)*/
    if (IS_ATOM_INT(_table_12361)) {
        _6855 = _table_12361 + 4;
        if ((long)((unsigned long)_6855 + (unsigned long)HIGH_BITS) >= 0) 
        _6855 = NewDouble((double)_6855);
    }
    else {
        _6855 = NewDouble(DBL_PTR(_table_12361)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_390_12429);
    _pos_inlined_seek_at_390_12429 = _6855;
    _6855 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_390_12429);
    DeRef(_seek_1__tmp_at393_12431);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_390_12429;
    _seek_1__tmp_at393_12431 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_393_12430 = machine(19, _seek_1__tmp_at393_12431);
    DeRef(_pos_inlined_seek_at_390_12429);
    _pos_inlined_seek_at_390_12429 = NOVALUE;
    DeRef(_seek_1__tmp_at393_12431);
    _seek_1__tmp_at393_12431 = NOVALUE;

    /** 	put4(0)  -- start with 0 records total*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at408_12433);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at408_12433 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at408_12433); // DJP 

    /** end procedure*/
    goto LB; // [430] 433
LB: 
    DeRefi(_put4_1__tmp_at408_12433);
    _put4_1__tmp_at408_12433 = NOVALUE;

    /** 	put4(1)  -- start with 1 block of records in index*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)1;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at436_12435);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at436_12435 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at436_12435); // DJP 

    /** end procedure*/
    goto LC; // [458] 461
LC: 
    DeRefi(_put4_1__tmp_at436_12435);
    _put4_1__tmp_at436_12435 = NOVALUE;

    /** 	put4(index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_index_ptr_12367)) {
        *poke4_addr = (unsigned long)_index_ptr_12367;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_index_ptr_12367)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at464_12437);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at464_12437 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at464_12437); // DJP 

    /** end procedure*/
    goto LD; // [486] 489
LD: 
    DeRefi(_put4_1__tmp_at464_12437);
    _put4_1__tmp_at464_12437 = NOVALUE;

    /** 	k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_2current_table_pos_10881);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _2current_table_pos_10881;
    _6856 = MAKE_SEQ(_1);
    _k_12368 = find_from(_6856, _2cache_index_10889, 1);
    DeRefDS(_6856);
    _6856 = NOVALUE;

    /** 	if k != 0 then*/
    if (_k_12368 == 0)
    goto LE; // [512] 535

    /** 		cache_index = remove(cache_index, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2cache_index_10889);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_12368)) ? _k_12368 : (long)(DBL_PTR(_k_12368)->dbl);
        int stop = (IS_ATOM_INT(_k_12368)) ? _k_12368 : (long)(DBL_PTR(_k_12368)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2cache_index_10889), start, &_2cache_index_10889 );
            }
            else Tail(SEQ_PTR(_2cache_index_10889), stop+1, &_2cache_index_10889);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2cache_index_10889), start, &_2cache_index_10889);
        }
        else {
            assign_slice_seq = &assign_space;
            _2cache_index_10889 = Remove_elements(start, stop, (SEQ_PTR(_2cache_index_10889)->ref == 1));
        }
    }

    /** 		key_cache = remove(key_cache, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_2key_cache_10888);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_12368)) ? _k_12368 : (long)(DBL_PTR(_k_12368)->dbl);
        int stop = (IS_ATOM_INT(_k_12368)) ? _k_12368 : (long)(DBL_PTR(_k_12368)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_2key_cache_10888), start, &_2key_cache_10888 );
            }
            else Tail(SEQ_PTR(_2key_cache_10888), stop+1, &_2key_cache_10888);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_2key_cache_10888), start, &_2key_cache_10888);
        }
        else {
            assign_slice_seq = &assign_space;
            _2key_cache_10888 = Remove_elements(start, stop, (SEQ_PTR(_2key_cache_10888)->ref == 1));
        }
    }
LE: 

    /** 	if table = current_table_pos then*/
    if (binary_op_a(NOTEQ, _table_12361, _2current_table_pos_10881)){
        goto LF; // [539] 551
    }

    /** 		key_pointers = {}*/
    RefDS(_5);
    DeRef(_2key_pointers_10887);
    _2key_pointers_10887 = _5;
LF: 

    /** end procedure*/
    DeRefDS(_name_12359);
    DeRef(_table_12361);
    DeRef(_nrecs_12362);
    DeRef(_records_ptr_12363);
    DeRef(_blocks_12364);
    DeRef(_p_12365);
    DeRef(_data_ptr_12366);
    DeRef(_index_ptr_12367);
    DeRef(_6836);
    _6836 = NOVALUE;
    DeRef(_6841);
    _6841 = NOVALUE;
    return;
    ;
}


void _2db_rename_table(int _name_12448, int _new_name_12449)
{
    int _table_12450 = NOVALUE;
    int _table_ptr_12451 = NOVALUE;
    int _seek_1__tmp_at70_12465 = NOVALUE;
    int _seek_inlined_seek_at_70_12464 = NOVALUE;
    int _s_inlined_putn_at_110_12472 = NOVALUE;
    int _seek_1__tmp_at133_12475 = NOVALUE;
    int _seek_inlined_seek_at_133_12474 = NOVALUE;
    int _put4_1__tmp_at148_12477 = NOVALUE;
    int _6876 = NOVALUE;
    int _6875 = NOVALUE;
    int _6873 = NOVALUE;
    int _6872 = NOVALUE;
    int _6871 = NOVALUE;
    int _6870 = NOVALUE;
    int _6867 = NOVALUE;
    int _6866 = NOVALUE;
    int _0, _1, _2;
    

    /** 	table = table_find(name)*/
    RefDS(_name_12448);
    _0 = _table_12450;
    _table_12450 = _2table_find(_name_12448);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_12450, -1)){
        goto L1; // [13] 37
    }

    /** 		fatal(NO_TABLE, "source table doesn't exist", "db_rename_table", {name, new_name})*/
    RefDS(_new_name_12449);
    RefDS(_name_12448);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _name_12448;
    ((int *)_2)[2] = _new_name_12449;
    _6866 = MAKE_SEQ(_1);
    RefDS(_6864);
    RefDS(_6865);
    _2fatal(903, _6864, _6865, _6866);
    _6866 = NOVALUE;

    /** 		return*/
    DeRefDS(_name_12448);
    DeRefDS(_new_name_12449);
    DeRef(_table_12450);
    DeRef(_table_ptr_12451);
    return;
L1: 

    /** 	if table_find(new_name) != -1 then*/
    RefDS(_new_name_12449);
    _6867 = _2table_find(_new_name_12449);
    if (binary_op_a(EQUALS, _6867, -1)){
        DeRef(_6867);
        _6867 = NOVALUE;
        goto L2; // [43] 67
    }
    DeRef(_6867);
    _6867 = NOVALUE;

    /** 		fatal(DUP_TABLE, "target table name already exists", "db_rename_table", {name, new_name})*/
    RefDS(_new_name_12449);
    RefDS(_name_12448);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _name_12448;
    ((int *)_2)[2] = _new_name_12449;
    _6870 = MAKE_SEQ(_1);
    RefDS(_6869);
    RefDS(_6865);
    _2fatal(904, _6869, _6865, _6870);
    _6870 = NOVALUE;

    /** 		return*/
    DeRefDS(_name_12448);
    DeRefDS(_new_name_12449);
    DeRef(_table_12450);
    DeRef(_table_ptr_12451);
    return;
L2: 

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_12450);
    DeRef(_seek_1__tmp_at70_12465);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _table_12450;
    _seek_1__tmp_at70_12465 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_70_12464 = machine(19, _seek_1__tmp_at70_12465);
    DeRef(_seek_1__tmp_at70_12465);
    _seek_1__tmp_at70_12465 = NOVALUE;

    /** 	db_free(get4())*/
    _6871 = _2get4();
    _2db_free(_6871);
    _6871 = NOVALUE;

    /** 	table_ptr = db_allocate(length(new_name)+1)*/
    if (IS_SEQUENCE(_new_name_12449)){
            _6872 = SEQ_PTR(_new_name_12449)->length;
    }
    else {
        _6872 = 1;
    }
    _6873 = _6872 + 1;
    _6872 = NOVALUE;
    _0 = _table_ptr_12451;
    _table_ptr_12451 = _2db_allocate(_6873);
    DeRef(_0);
    _6873 = NOVALUE;

    /** 	putn(new_name & 0)*/
    Append(&_6875, _new_name_12449, 0);
    DeRef(_s_inlined_putn_at_110_12472);
    _s_inlined_putn_at_110_12472 = _6875;
    _6875 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_110_12472); // DJP 

    /** end procedure*/
    goto L3; // [125] 128
L3: 
    DeRef(_s_inlined_putn_at_110_12472);
    _s_inlined_putn_at_110_12472 = NOVALUE;

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_12450);
    DeRef(_seek_1__tmp_at133_12475);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _table_12450;
    _seek_1__tmp_at133_12475 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_133_12474 = machine(19, _seek_1__tmp_at133_12475);
    DeRef(_seek_1__tmp_at133_12475);
    _seek_1__tmp_at133_12475 = NOVALUE;

    /** 	put4(table_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_table_ptr_12451)) {
        *poke4_addr = (unsigned long)_table_ptr_12451;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_table_ptr_12451)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at148_12477);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at148_12477 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at148_12477); // DJP 

    /** end procedure*/
    goto L4; // [170] 173
L4: 
    DeRefi(_put4_1__tmp_at148_12477);
    _put4_1__tmp_at148_12477 = NOVALUE;

    /** 	if equal(current_table_name, name) then*/
    if (_2current_table_name_10882 == _name_12448)
    _6876 = 1;
    else if (IS_ATOM_INT(_2current_table_name_10882) && IS_ATOM_INT(_name_12448))
    _6876 = 0;
    else
    _6876 = (compare(_2current_table_name_10882, _name_12448) == 0);
    if (_6876 == 0)
    {
        _6876 = NOVALUE;
        goto L5; // [183] 194
    }
    else{
        _6876 = NOVALUE;
    }

    /** 		current_table_name = new_name*/
    RefDS(_new_name_12449);
    DeRefDS(_2current_table_name_10882);
    _2current_table_name_10882 = _new_name_12449;
L5: 

    /** end procedure*/
    DeRefDS(_name_12448);
    DeRefDS(_new_name_12449);
    DeRef(_table_12450);
    DeRef(_table_ptr_12451);
    return;
    ;
}


int _2db_table_list()
{
    int _seek_1__tmp_at120_12511 = NOVALUE;
    int _seek_inlined_seek_at_120_12510 = NOVALUE;
    int _seek_1__tmp_at98_12507 = NOVALUE;
    int _seek_inlined_seek_at_98_12506 = NOVALUE;
    int _pos_inlined_seek_at_95_12505 = NOVALUE;
    int _seek_1__tmp_at42_12495 = NOVALUE;
    int _seek_inlined_seek_at_42_12494 = NOVALUE;
    int _seek_1__tmp_at4_12488 = NOVALUE;
    int _seek_inlined_seek_at_4_12487 = NOVALUE;
    int _table_names_12482 = NOVALUE;
    int _tables_12483 = NOVALUE;
    int _nt_12484 = NOVALUE;
    int _name_12485 = NOVALUE;
    int _6888 = NOVALUE;
    int _6887 = NOVALUE;
    int _6885 = NOVALUE;
    int _6884 = NOVALUE;
    int _6883 = NOVALUE;
    int _6882 = NOVALUE;
    int _6877 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_12488);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at4_12488 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_12487 = machine(19, _seek_1__tmp_at4_12488);
    DeRefi(_seek_1__tmp_at4_12488);
    _seek_1__tmp_at4_12488 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return {} end if*/
    if (IS_SEQUENCE(_2vLastErrors_10904)){
            _6877 = SEQ_PTR(_2vLastErrors_10904)->length;
    }
    else {
        _6877 = 1;
    }
    if (_6877 <= 0)
    goto L1; // [25] 34
    RefDS(_5);
    DeRef(_table_names_12482);
    DeRef(_tables_12483);
    DeRef(_nt_12484);
    DeRef(_name_12485);
    return _5;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_12483;
    _tables_12483 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_12483);
    DeRef(_seek_1__tmp_at42_12495);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _tables_12483;
    _seek_1__tmp_at42_12495 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_42_12494 = machine(19, _seek_1__tmp_at42_12495);
    DeRef(_seek_1__tmp_at42_12495);
    _seek_1__tmp_at42_12495 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_12484;
    _nt_12484 = _2get4();
    DeRef(_0);

    /** 	table_names = repeat(0, nt)*/
    DeRef(_table_names_12482);
    _table_names_12482 = Repeat(0, _nt_12484);

    /** 	for i = 0 to nt-1 do*/
    if (IS_ATOM_INT(_nt_12484)) {
        _6882 = _nt_12484 - 1;
        if ((long)((unsigned long)_6882 +(unsigned long) HIGH_BITS) >= 0){
            _6882 = NewDouble((double)_6882);
        }
    }
    else {
        _6882 = NewDouble(DBL_PTR(_nt_12484)->dbl - (double)1);
    }
    {
        int _i_12499;
        _i_12499 = 0;
L2: 
        if (binary_op_a(GREATER, _i_12499, _6882)){
            goto L3; // [73] 154
        }

        /** 		io:seek(current_db, tables + 4 + i*SIZEOF_TABLE_HEADER)*/
        if (IS_ATOM_INT(_tables_12483)) {
            _6883 = _tables_12483 + 4;
            if ((long)((unsigned long)_6883 + (unsigned long)HIGH_BITS) >= 0) 
            _6883 = NewDouble((double)_6883);
        }
        else {
            _6883 = NewDouble(DBL_PTR(_tables_12483)->dbl + (double)4);
        }
        if (IS_ATOM_INT(_i_12499)) {
            if (_i_12499 == (short)_i_12499)
            _6884 = _i_12499 * 16;
            else
            _6884 = NewDouble(_i_12499 * (double)16);
        }
        else {
            _6884 = NewDouble(DBL_PTR(_i_12499)->dbl * (double)16);
        }
        if (IS_ATOM_INT(_6883) && IS_ATOM_INT(_6884)) {
            _6885 = _6883 + _6884;
            if ((long)((unsigned long)_6885 + (unsigned long)HIGH_BITS) >= 0) 
            _6885 = NewDouble((double)_6885);
        }
        else {
            if (IS_ATOM_INT(_6883)) {
                _6885 = NewDouble((double)_6883 + DBL_PTR(_6884)->dbl);
            }
            else {
                if (IS_ATOM_INT(_6884)) {
                    _6885 = NewDouble(DBL_PTR(_6883)->dbl + (double)_6884);
                }
                else
                _6885 = NewDouble(DBL_PTR(_6883)->dbl + DBL_PTR(_6884)->dbl);
            }
        }
        DeRef(_6883);
        _6883 = NOVALUE;
        DeRef(_6884);
        _6884 = NOVALUE;
        DeRef(_pos_inlined_seek_at_95_12505);
        _pos_inlined_seek_at_95_12505 = _6885;
        _6885 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_95_12505);
        DeRef(_seek_1__tmp_at98_12507);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _pos_inlined_seek_at_95_12505;
        _seek_1__tmp_at98_12507 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_98_12506 = machine(19, _seek_1__tmp_at98_12507);
        DeRef(_pos_inlined_seek_at_95_12505);
        _pos_inlined_seek_at_95_12505 = NOVALUE;
        DeRef(_seek_1__tmp_at98_12507);
        _seek_1__tmp_at98_12507 = NOVALUE;

        /** 		name = get4()*/
        _0 = _name_12485;
        _name_12485 = _2get4();
        DeRef(_0);

        /** 		io:seek(current_db, name)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_12485);
        DeRef(_seek_1__tmp_at120_12511);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2current_db_10880;
        ((int *)_2)[2] = _name_12485;
        _seek_1__tmp_at120_12511 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_120_12510 = machine(19, _seek_1__tmp_at120_12511);
        DeRef(_seek_1__tmp_at120_12511);
        _seek_1__tmp_at120_12511 = NOVALUE;

        /** 		table_names[i+1] = get_string()*/
        if (IS_ATOM_INT(_i_12499)) {
            _6887 = _i_12499 + 1;
            if (_6887 > MAXINT){
                _6887 = NewDouble((double)_6887);
            }
        }
        else
        _6887 = binary_op(PLUS, 1, _i_12499);
        _6888 = _2get_string();
        _2 = (int)SEQ_PTR(_table_names_12482);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _table_names_12482 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_6887))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_6887)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _6887);
        _1 = *(int *)_2;
        *(int *)_2 = _6888;
        if( _1 != _6888 ){
            DeRef(_1);
        }
        _6888 = NOVALUE;

        /** 	end for*/
        _0 = _i_12499;
        if (IS_ATOM_INT(_i_12499)) {
            _i_12499 = _i_12499 + 1;
            if ((long)((unsigned long)_i_12499 +(unsigned long) HIGH_BITS) >= 0){
                _i_12499 = NewDouble((double)_i_12499);
            }
        }
        else {
            _i_12499 = binary_op_a(PLUS, _i_12499, 1);
        }
        DeRef(_0);
        goto L2; // [149] 80
L3: 
        ;
        DeRef(_i_12499);
    }

    /** 	return table_names*/
    DeRef(_tables_12483);
    DeRef(_nt_12484);
    DeRef(_name_12485);
    DeRef(_6882);
    _6882 = NOVALUE;
    DeRef(_6887);
    _6887 = NOVALUE;
    return _table_names_12482;
    ;
}


int _2key_value(int _ptr_12516)
{
    int _seek_1__tmp_at11_12521 = NOVALUE;
    int _seek_inlined_seek_at_11_12520 = NOVALUE;
    int _pos_inlined_seek_at_8_12519 = NOVALUE;
    int _6890 = NOVALUE;
    int _6889 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, ptr+4) -- skip ptr to data*/
    if (IS_ATOM_INT(_ptr_12516)) {
        _6889 = _ptr_12516 + 4;
        if ((long)((unsigned long)_6889 + (unsigned long)HIGH_BITS) >= 0) 
        _6889 = NewDouble((double)_6889);
    }
    else {
        _6889 = NewDouble(DBL_PTR(_ptr_12516)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_12519);
    _pos_inlined_seek_at_8_12519 = _6889;
    _6889 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_12519);
    DeRef(_seek_1__tmp_at11_12521);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_12519;
    _seek_1__tmp_at11_12521 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_12520 = machine(19, _seek_1__tmp_at11_12521);
    DeRef(_pos_inlined_seek_at_8_12519);
    _pos_inlined_seek_at_8_12519 = NOVALUE;
    DeRef(_seek_1__tmp_at11_12521);
    _seek_1__tmp_at11_12521 = NOVALUE;

    /** 	return decompress(0)*/
    _6890 = _2decompress(0);
    DeRef(_ptr_12516);
    return _6890;
    ;
}


int _2db_find_key(int _key_12525, int _table_name_12526)
{
    int _lo_12527 = NOVALUE;
    int _hi_12528 = NOVALUE;
    int _mid_12529 = NOVALUE;
    int _c_12530 = NOVALUE;
    int _6914 = NOVALUE;
    int _6906 = NOVALUE;
    int _6905 = NOVALUE;
    int _6903 = NOVALUE;
    int _6900 = NOVALUE;
    int _6897 = NOVALUE;
    int _6893 = NOVALUE;
    int _6891 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_12526 == _2current_table_name_10882)
    _6891 = 1;
    else if (IS_ATOM_INT(_table_name_12526) && IS_ATOM_INT(_2current_table_name_10882))
    _6891 = 0;
    else
    _6891 = (compare(_table_name_12526, _2current_table_name_10882) == 0);
    if (_6891 != 0)
    goto L1; // [9] 46
    _6891 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_12526);
    _6893 = _2db_select_table(_table_name_12526);
    if (binary_op_a(EQUALS, _6893, 0)){
        DeRef(_6893);
        _6893 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_6893);
    _6893 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_find_key", {key, table_name})*/
    Ref(_table_name_12526);
    Ref(_key_12525);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_12525;
    ((int *)_2)[2] = _table_name_12526;
    _6897 = MAKE_SEQ(_1);
    RefDS(_6895);
    RefDS(_6896);
    _2fatal(903, _6895, _6896, _6897);
    _6897 = NOVALUE;

    /** 			return 0*/
    DeRef(_key_12525);
    DeRef(_table_name_12526);
    return 0;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _2current_table_pos_10881, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_find_key", {key, table_name})*/
    Ref(_table_name_12526);
    Ref(_key_12525);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_12525;
    ((int *)_2)[2] = _table_name_12526;
    _6900 = MAKE_SEQ(_1);
    RefDS(_6899);
    RefDS(_6896);
    _2fatal(903, _6899, _6896, _6900);
    _6900 = NOVALUE;

    /** 		return 0*/
    DeRef(_key_12525);
    DeRef(_table_name_12526);
    return 0;
L3: 

    /** 	lo = 1*/
    _lo_12527 = 1;

    /** 	hi = length(key_pointers)*/
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _hi_12528 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _hi_12528 = 1;
    }

    /** 	mid = 1*/
    _mid_12529 = 1;

    /** 	c = 0*/
    _c_12530 = 0;

    /** 	while lo <= hi do*/
L4: 
    if (_lo_12527 > _hi_12528)
    goto L5; // [110] 192

    /** 		mid = floor((lo + hi) / 2)*/
    _6903 = _lo_12527 + _hi_12528;
    if ((long)((unsigned long)_6903 + (unsigned long)HIGH_BITS) >= 0) 
    _6903 = NewDouble((double)_6903);
    if (IS_ATOM_INT(_6903)) {
        _mid_12529 = _6903 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _6903, 2);
        _mid_12529 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_6903);
    _6903 = NOVALUE;
    if (!IS_ATOM_INT(_mid_12529)) {
        _1 = (long)(DBL_PTR(_mid_12529)->dbl);
        if (UNIQUE(DBL_PTR(_mid_12529)) && (DBL_PTR(_mid_12529)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_12529);
        _mid_12529 = _1;
    }

    /** 		c = eu:compare(key, key_value(key_pointers[mid]))*/
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    _6905 = (int)*(((s1_ptr)_2)->base + _mid_12529);
    Ref(_6905);
    _6906 = _2key_value(_6905);
    _6905 = NOVALUE;
    if (IS_ATOM_INT(_key_12525) && IS_ATOM_INT(_6906)){
        _c_12530 = (_key_12525 < _6906) ? -1 : (_key_12525 > _6906);
    }
    else{
        _c_12530 = compare(_key_12525, _6906);
    }
    DeRef(_6906);
    _6906 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_12530 >= 0)
    goto L6; // [148] 163

    /** 			hi = mid - 1*/
    _hi_12528 = _mid_12529 - 1;
    goto L4; // [160] 110
L6: 

    /** 		elsif c > 0 then*/
    if (_c_12530 <= 0)
    goto L7; // [165] 180

    /** 			lo = mid + 1*/
    _lo_12527 = _mid_12529 + 1;
    goto L4; // [177] 110
L7: 

    /** 			return mid*/
    DeRef(_key_12525);
    DeRef(_table_name_12526);
    return _mid_12529;

    /** 	end while*/
    goto L4; // [189] 110
L5: 

    /** 	if c > 0 then*/
    if (_c_12530 <= 0)
    goto L8; // [194] 207

    /** 		mid += 1*/
    _mid_12529 = _mid_12529 + 1;
L8: 

    /** 	return -mid*/
    if ((unsigned long)_mid_12529 == 0xC0000000)
    _6914 = (int)NewDouble((double)-0xC0000000);
    else
    _6914 = - _mid_12529;
    DeRef(_key_12525);
    DeRef(_table_name_12526);
    return _6914;
    ;
}


int _2db_insert(int _key_12565, int _data_12566, int _table_name_12567)
{
    int _key_string_12568 = NOVALUE;
    int _data_string_12569 = NOVALUE;
    int _last_part_12570 = NOVALUE;
    int _remaining_12571 = NOVALUE;
    int _key_ptr_12572 = NOVALUE;
    int _data_ptr_12573 = NOVALUE;
    int _records_ptr_12574 = NOVALUE;
    int _nrecs_12575 = NOVALUE;
    int _current_block_12576 = NOVALUE;
    int _size_12577 = NOVALUE;
    int _new_size_12578 = NOVALUE;
    int _key_location_12579 = NOVALUE;
    int _new_block_12580 = NOVALUE;
    int _index_ptr_12581 = NOVALUE;
    int _new_index_ptr_12582 = NOVALUE;
    int _total_recs_12583 = NOVALUE;
    int _r_12584 = NOVALUE;
    int _blocks_12585 = NOVALUE;
    int _new_recs_12586 = NOVALUE;
    int _n_12587 = NOVALUE;
    int _put4_1__tmp_at81_12601 = NOVALUE;
    int _seek_1__tmp_at134_12607 = NOVALUE;
    int _seek_inlined_seek_at_134_12606 = NOVALUE;
    int _pos_inlined_seek_at_131_12605 = NOVALUE;
    int _seek_1__tmp_at178_12615 = NOVALUE;
    int _seek_inlined_seek_at_178_12614 = NOVALUE;
    int _pos_inlined_seek_at_175_12613 = NOVALUE;
    int _put4_1__tmp_at193_12617 = NOVALUE;
    int _seek_1__tmp_at323_12634 = NOVALUE;
    int _seek_inlined_seek_at_323_12633 = NOVALUE;
    int _pos_inlined_seek_at_320_12632 = NOVALUE;
    int _seek_1__tmp_at345_12638 = NOVALUE;
    int _seek_inlined_seek_at_345_12637 = NOVALUE;
    int _where_inlined_where_at_414_12647 = NOVALUE;
    int _seek_1__tmp_at458_12657 = NOVALUE;
    int _seek_inlined_seek_at_458_12656 = NOVALUE;
    int _pos_inlined_seek_at_455_12655 = NOVALUE;
    int _put4_1__tmp_at503_12666 = NOVALUE;
    int _x_inlined_put4_at_500_12665 = NOVALUE;
    int _seek_1__tmp_at540_12669 = NOVALUE;
    int _seek_inlined_seek_at_540_12668 = NOVALUE;
    int _put4_1__tmp_at561_12672 = NOVALUE;
    int _seek_1__tmp_at598_12677 = NOVALUE;
    int _seek_inlined_seek_at_598_12676 = NOVALUE;
    int _pos_inlined_seek_at_595_12675 = NOVALUE;
    int _seek_1__tmp_at704_12700 = NOVALUE;
    int _seek_inlined_seek_at_704_12699 = NOVALUE;
    int _pos_inlined_seek_at_701_12698 = NOVALUE;
    int _s_inlined_putn_at_765_12709 = NOVALUE;
    int _seek_1__tmp_at788_12712 = NOVALUE;
    int _seek_inlined_seek_at_788_12711 = NOVALUE;
    int _put4_1__tmp_at810_12716 = NOVALUE;
    int _x_inlined_put4_at_807_12715 = NOVALUE;
    int _seek_1__tmp_at847_12721 = NOVALUE;
    int _seek_inlined_seek_at_847_12720 = NOVALUE;
    int _pos_inlined_seek_at_844_12719 = NOVALUE;
    int _seek_1__tmp_at898_12731 = NOVALUE;
    int _seek_inlined_seek_at_898_12730 = NOVALUE;
    int _pos_inlined_seek_at_895_12729 = NOVALUE;
    int _put4_1__tmp_at913_12733 = NOVALUE;
    int _put4_1__tmp_at941_12735 = NOVALUE;
    int _seek_1__tmp_at994_12741 = NOVALUE;
    int _seek_inlined_seek_at_994_12740 = NOVALUE;
    int _pos_inlined_seek_at_991_12739 = NOVALUE;
    int _put4_1__tmp_at1017_12744 = NOVALUE;
    int _seek_1__tmp_at1054_12749 = NOVALUE;
    int _seek_inlined_seek_at_1054_12748 = NOVALUE;
    int _pos_inlined_seek_at_1051_12747 = NOVALUE;
    int _s_inlined_putn_at_1152_12767 = NOVALUE;
    int _seek_1__tmp_at1189_12772 = NOVALUE;
    int _seek_inlined_seek_at_1189_12771 = NOVALUE;
    int _pos_inlined_seek_at_1186_12770 = NOVALUE;
    int _put4_1__tmp_at1204_12774 = NOVALUE;
    int _7007 = NOVALUE;
    int _7006 = NOVALUE;
    int _7005 = NOVALUE;
    int _7004 = NOVALUE;
    int _7001 = NOVALUE;
    int _7000 = NOVALUE;
    int _6998 = NOVALUE;
    int _6996 = NOVALUE;
    int _6995 = NOVALUE;
    int _6993 = NOVALUE;
    int _6992 = NOVALUE;
    int _6990 = NOVALUE;
    int _6989 = NOVALUE;
    int _6987 = NOVALUE;
    int _6986 = NOVALUE;
    int _6985 = NOVALUE;
    int _6984 = NOVALUE;
    int _6983 = NOVALUE;
    int _6982 = NOVALUE;
    int _6981 = NOVALUE;
    int _6980 = NOVALUE;
    int _6979 = NOVALUE;
    int _6976 = NOVALUE;
    int _6975 = NOVALUE;
    int _6974 = NOVALUE;
    int _6973 = NOVALUE;
    int _6970 = NOVALUE;
    int _6967 = NOVALUE;
    int _6966 = NOVALUE;
    int _6965 = NOVALUE;
    int _6964 = NOVALUE;
    int _6962 = NOVALUE;
    int _6961 = NOVALUE;
    int _6959 = NOVALUE;
    int _6958 = NOVALUE;
    int _6956 = NOVALUE;
    int _6955 = NOVALUE;
    int _6954 = NOVALUE;
    int _6953 = NOVALUE;
    int _6952 = NOVALUE;
    int _6951 = NOVALUE;
    int _6950 = NOVALUE;
    int _6948 = NOVALUE;
    int _6945 = NOVALUE;
    int _6940 = NOVALUE;
    int _6939 = NOVALUE;
    int _6938 = NOVALUE;
    int _6936 = NOVALUE;
    int _6935 = NOVALUE;
    int _6934 = NOVALUE;
    int _6931 = NOVALUE;
    int _6929 = NOVALUE;
    int _6926 = NOVALUE;
    int _6925 = NOVALUE;
    int _6923 = NOVALUE;
    int _6922 = NOVALUE;
    int _6920 = NOVALUE;
    int _0, _1, _2;
    

    /** 	key_location = db_find_key(key, table_name) -- Let it set the current table if necessary*/
    Ref(_key_12565);
    Ref(_table_name_12567);
    _0 = _key_location_12579;
    _key_location_12579 = _2db_find_key(_key_12565, _table_name_12567);
    DeRef(_0);

    /** 	if key_location > 0 then*/
    if (binary_op_a(LESSEQ, _key_location_12579, 0)){
        goto L1; // [10] 23
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRef(_key_12565);
    DeRef(_data_12566);
    DeRef(_table_name_12567);
    DeRef(_key_string_12568);
    DeRef(_data_string_12569);
    DeRef(_last_part_12570);
    DeRef(_remaining_12571);
    DeRef(_key_ptr_12572);
    DeRef(_data_ptr_12573);
    DeRef(_records_ptr_12574);
    DeRef(_nrecs_12575);
    DeRef(_current_block_12576);
    DeRef(_size_12577);
    DeRef(_new_size_12578);
    DeRef(_key_location_12579);
    DeRef(_new_block_12580);
    DeRef(_index_ptr_12581);
    DeRef(_new_index_ptr_12582);
    DeRef(_total_recs_12583);
    return -2;
L1: 

    /** 	key_location = -key_location*/
    _0 = _key_location_12579;
    if (IS_ATOM_INT(_key_location_12579)) {
        if ((unsigned long)_key_location_12579 == 0xC0000000)
        _key_location_12579 = (int)NewDouble((double)-0xC0000000);
        else
        _key_location_12579 = - _key_location_12579;
    }
    else {
        _key_location_12579 = unary_op(UMINUS, _key_location_12579);
    }
    DeRef(_0);

    /** 	data_string = compress(data)*/
    Ref(_data_12566);
    _0 = _data_string_12569;
    _data_string_12569 = _2compress(_data_12566);
    DeRef(_0);

    /** 	key_string  = compress(key)*/
    Ref(_key_12565);
    _0 = _key_string_12568;
    _key_string_12568 = _2compress(_key_12565);
    DeRef(_0);

    /** 	data_ptr = db_allocate(length(data_string))*/
    if (IS_SEQUENCE(_data_string_12569)){
            _6920 = SEQ_PTR(_data_string_12569)->length;
    }
    else {
        _6920 = 1;
    }
    _0 = _data_ptr_12573;
    _data_ptr_12573 = _2db_allocate(_6920);
    DeRef(_0);
    _6920 = NOVALUE;

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _data_string_12569); // DJP 

    /** end procedure*/
    goto L2; // [64] 67
L2: 

    /** 	key_ptr = db_allocate(4+length(key_string))*/
    if (IS_SEQUENCE(_key_string_12568)){
            _6922 = SEQ_PTR(_key_string_12568)->length;
    }
    else {
        _6922 = 1;
    }
    _6923 = 4 + _6922;
    _6922 = NOVALUE;
    _0 = _key_ptr_12572;
    _key_ptr_12572 = _2db_allocate(_6923);
    DeRef(_0);
    _6923 = NOVALUE;

    /** 	put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_12573)) {
        *poke4_addr = (unsigned long)_data_ptr_12573;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_12573)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at81_12601);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at81_12601 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at81_12601); // DJP 

    /** end procedure*/
    goto L3; // [103] 106
L3: 
    DeRefi(_put4_1__tmp_at81_12601);
    _put4_1__tmp_at81_12601 = NOVALUE;

    /** 	putn(key_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _key_string_12568); // DJP 

    /** end procedure*/
    goto L4; // [119] 122
L4: 

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _6925 = _2current_table_pos_10881 + 4;
        if ((long)((unsigned long)_6925 + (unsigned long)HIGH_BITS) >= 0) 
        _6925 = NewDouble((double)_6925);
    }
    else {
        _6925 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_131_12605);
    _pos_inlined_seek_at_131_12605 = _6925;
    _6925 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_131_12605);
    DeRef(_seek_1__tmp_at134_12607);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_131_12605;
    _seek_1__tmp_at134_12607 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_134_12606 = machine(19, _seek_1__tmp_at134_12607);
    DeRef(_pos_inlined_seek_at_131_12605);
    _pos_inlined_seek_at_131_12605 = NOVALUE;
    DeRef(_seek_1__tmp_at134_12607);
    _seek_1__tmp_at134_12607 = NOVALUE;

    /** 	total_recs = get4()+1*/
    _6926 = _2get4();
    DeRef(_total_recs_12583);
    if (IS_ATOM_INT(_6926)) {
        _total_recs_12583 = _6926 + 1;
        if (_total_recs_12583 > MAXINT){
            _total_recs_12583 = NewDouble((double)_total_recs_12583);
        }
    }
    else
    _total_recs_12583 = binary_op(PLUS, 1, _6926);
    DeRef(_6926);
    _6926 = NOVALUE;

    /** 	blocks = get4()*/
    _blocks_12585 = _2get4();
    if (!IS_ATOM_INT(_blocks_12585)) {
        _1 = (long)(DBL_PTR(_blocks_12585)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_12585)) && (DBL_PTR(_blocks_12585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_12585);
        _blocks_12585 = _1;
    }

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _6929 = _2current_table_pos_10881 + 4;
        if ((long)((unsigned long)_6929 + (unsigned long)HIGH_BITS) >= 0) 
        _6929 = NewDouble((double)_6929);
    }
    else {
        _6929 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_175_12613);
    _pos_inlined_seek_at_175_12613 = _6929;
    _6929 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_175_12613);
    DeRef(_seek_1__tmp_at178_12615);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_175_12613;
    _seek_1__tmp_at178_12615 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_178_12614 = machine(19, _seek_1__tmp_at178_12615);
    DeRef(_pos_inlined_seek_at_175_12613);
    _pos_inlined_seek_at_175_12613 = NOVALUE;
    DeRef(_seek_1__tmp_at178_12615);
    _seek_1__tmp_at178_12615 = NOVALUE;

    /** 	put4(total_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_total_recs_12583)) {
        *poke4_addr = (unsigned long)_total_recs_12583;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_total_recs_12583)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at193_12617);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at193_12617 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at193_12617); // DJP 

    /** end procedure*/
    goto L5; // [215] 218
L5: 
    DeRefi(_put4_1__tmp_at193_12617);
    _put4_1__tmp_at193_12617 = NOVALUE;

    /** 	n = length(key_pointers)*/
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _n_12587 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _n_12587 = 1;
    }

    /** 	if key_location >= floor(n/2) then*/
    _6931 = _n_12587 >> 1;
    if (binary_op_a(LESS, _key_location_12579, _6931)){
        _6931 = NOVALUE;
        goto L6; // [235] 274
    }
    DeRef(_6931);
    _6931 = NOVALUE;

    /** 		key_pointers = append(key_pointers, 0)*/
    Append(&_2key_pointers_10887, _2key_pointers_10887, 0);

    /** 		key_pointers[key_location+1..n+1] = key_pointers[key_location..n]*/
    if (IS_ATOM_INT(_key_location_12579)) {
        _6934 = _key_location_12579 + 1;
        if (_6934 > MAXINT){
            _6934 = NewDouble((double)_6934);
        }
    }
    else
    _6934 = binary_op(PLUS, 1, _key_location_12579);
    _6935 = _n_12587 + 1;
    rhs_slice_target = (object_ptr)&_6936;
    RHS_Slice(_2key_pointers_10887, _key_location_12579, _n_12587);
    assign_slice_seq = (s1_ptr *)&_2key_pointers_10887;
    AssignSlice(_6934, _6935, _6936);
    DeRef(_6934);
    _6934 = NOVALUE;
    _6935 = NOVALUE;
    DeRefDS(_6936);
    _6936 = NOVALUE;
    goto L7; // [271] 303
L6: 

    /** 		key_pointers = prepend(key_pointers, 0)*/
    Prepend(&_2key_pointers_10887, _2key_pointers_10887, 0);

    /** 		key_pointers[1..key_location-1] = key_pointers[2..key_location]*/
    if (IS_ATOM_INT(_key_location_12579)) {
        _6938 = _key_location_12579 - 1;
        if ((long)((unsigned long)_6938 +(unsigned long) HIGH_BITS) >= 0){
            _6938 = NewDouble((double)_6938);
        }
    }
    else {
        _6938 = NewDouble(DBL_PTR(_key_location_12579)->dbl - (double)1);
    }
    rhs_slice_target = (object_ptr)&_6939;
    RHS_Slice(_2key_pointers_10887, 2, _key_location_12579);
    assign_slice_seq = (s1_ptr *)&_2key_pointers_10887;
    AssignSlice(1, _6938, _6939);
    DeRef(_6938);
    _6938 = NOVALUE;
    DeRefDS(_6939);
    _6939 = NOVALUE;
L7: 

    /** 	key_pointers[key_location] = key_ptr*/
    Ref(_key_ptr_12572);
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _2key_pointers_10887 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_key_location_12579))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_key_location_12579)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _key_location_12579);
    _1 = *(int *)_2;
    *(int *)_2 = _key_ptr_12572;
    DeRef(_1);

    /** 	io:seek(current_db, current_table_pos+12) -- get after put - seek is necessary*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _6940 = _2current_table_pos_10881 + 12;
        if ((long)((unsigned long)_6940 + (unsigned long)HIGH_BITS) >= 0) 
        _6940 = NewDouble((double)_6940);
    }
    else {
        _6940 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_320_12632);
    _pos_inlined_seek_at_320_12632 = _6940;
    _6940 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_320_12632);
    DeRef(_seek_1__tmp_at323_12634);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_320_12632;
    _seek_1__tmp_at323_12634 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_323_12633 = machine(19, _seek_1__tmp_at323_12634);
    DeRef(_pos_inlined_seek_at_320_12632);
    _pos_inlined_seek_at_320_12632 = NOVALUE;
    DeRef(_seek_1__tmp_at323_12634);
    _seek_1__tmp_at323_12634 = NOVALUE;

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_12581;
    _index_ptr_12581 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, index_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_index_ptr_12581);
    DeRef(_seek_1__tmp_at345_12638);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _index_ptr_12581;
    _seek_1__tmp_at345_12638 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_345_12637 = machine(19, _seek_1__tmp_at345_12638);
    DeRef(_seek_1__tmp_at345_12638);
    _seek_1__tmp_at345_12638 = NOVALUE;

    /** 	r = 0*/
    _r_12584 = 0;

    /** 	while TRUE do*/
L8: 

    /** 		nrecs = get4()*/
    _0 = _nrecs_12575;
    _nrecs_12575 = _2get4();
    DeRef(_0);

    /** 		records_ptr = get4()*/
    _0 = _records_ptr_12574;
    _records_ptr_12574 = _2get4();
    DeRef(_0);

    /** 		r += nrecs*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        _r_12584 = _r_12584 + _nrecs_12575;
    }
    else {
        _r_12584 = NewDouble((double)_r_12584 + DBL_PTR(_nrecs_12575)->dbl);
    }
    if (!IS_ATOM_INT(_r_12584)) {
        _1 = (long)(DBL_PTR(_r_12584)->dbl);
        if (UNIQUE(DBL_PTR(_r_12584)) && (DBL_PTR(_r_12584)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_12584);
        _r_12584 = _1;
    }

    /** 		if r + 1 >= key_location then*/
    _6945 = _r_12584 + 1;
    if (_6945 > MAXINT){
        _6945 = NewDouble((double)_6945);
    }
    if (binary_op_a(LESS, _6945, _key_location_12579)){
        DeRef(_6945);
        _6945 = NOVALUE;
        goto L8; // [397] 371
    }
    DeRef(_6945);
    _6945 = NOVALUE;

    /** 			exit*/
    goto L9; // [403] 411

    /** 	end while*/
    goto L8; // [408] 371
L9: 

    /** 	current_block = io:where(current_db)-8*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_414_12647);
    _where_inlined_where_at_414_12647 = machine(20, _2current_db_10880);
    DeRef(_current_block_12576);
    if (IS_ATOM_INT(_where_inlined_where_at_414_12647)) {
        _current_block_12576 = _where_inlined_where_at_414_12647 - 8;
        if ((long)((unsigned long)_current_block_12576 +(unsigned long) HIGH_BITS) >= 0){
            _current_block_12576 = NewDouble((double)_current_block_12576);
        }
    }
    else {
        _current_block_12576 = NewDouble(DBL_PTR(_where_inlined_where_at_414_12647)->dbl - (double)8);
    }

    /** 	key_location -= (r-nrecs)*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        _6948 = _r_12584 - _nrecs_12575;
        if ((long)((unsigned long)_6948 +(unsigned long) HIGH_BITS) >= 0){
            _6948 = NewDouble((double)_6948);
        }
    }
    else {
        _6948 = NewDouble((double)_r_12584 - DBL_PTR(_nrecs_12575)->dbl);
    }
    _0 = _key_location_12579;
    if (IS_ATOM_INT(_key_location_12579) && IS_ATOM_INT(_6948)) {
        _key_location_12579 = _key_location_12579 - _6948;
        if ((long)((unsigned long)_key_location_12579 +(unsigned long) HIGH_BITS) >= 0){
            _key_location_12579 = NewDouble((double)_key_location_12579);
        }
    }
    else {
        if (IS_ATOM_INT(_key_location_12579)) {
            _key_location_12579 = NewDouble((double)_key_location_12579 - DBL_PTR(_6948)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6948)) {
                _key_location_12579 = NewDouble(DBL_PTR(_key_location_12579)->dbl - (double)_6948);
            }
            else
            _key_location_12579 = NewDouble(DBL_PTR(_key_location_12579)->dbl - DBL_PTR(_6948)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_6948);
    _6948 = NOVALUE;

    /** 	io:seek(current_db, records_ptr+4*(key_location-1))*/
    if (IS_ATOM_INT(_key_location_12579)) {
        _6950 = _key_location_12579 - 1;
        if ((long)((unsigned long)_6950 +(unsigned long) HIGH_BITS) >= 0){
            _6950 = NewDouble((double)_6950);
        }
    }
    else {
        _6950 = NewDouble(DBL_PTR(_key_location_12579)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_6950)) {
        if (_6950 <= INT15 && _6950 >= -INT15)
        _6951 = 4 * _6950;
        else
        _6951 = NewDouble(4 * (double)_6950);
    }
    else {
        _6951 = NewDouble((double)4 * DBL_PTR(_6950)->dbl);
    }
    DeRef(_6950);
    _6950 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_12574) && IS_ATOM_INT(_6951)) {
        _6952 = _records_ptr_12574 + _6951;
        if ((long)((unsigned long)_6952 + (unsigned long)HIGH_BITS) >= 0) 
        _6952 = NewDouble((double)_6952);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_12574)) {
            _6952 = NewDouble((double)_records_ptr_12574 + DBL_PTR(_6951)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6951)) {
                _6952 = NewDouble(DBL_PTR(_records_ptr_12574)->dbl + (double)_6951);
            }
            else
            _6952 = NewDouble(DBL_PTR(_records_ptr_12574)->dbl + DBL_PTR(_6951)->dbl);
        }
    }
    DeRef(_6951);
    _6951 = NOVALUE;
    DeRef(_pos_inlined_seek_at_455_12655);
    _pos_inlined_seek_at_455_12655 = _6952;
    _6952 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_455_12655);
    DeRef(_seek_1__tmp_at458_12657);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_455_12655;
    _seek_1__tmp_at458_12657 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_458_12656 = machine(19, _seek_1__tmp_at458_12657);
    DeRef(_pos_inlined_seek_at_455_12655);
    _pos_inlined_seek_at_455_12655 = NOVALUE;
    DeRef(_seek_1__tmp_at458_12657);
    _seek_1__tmp_at458_12657 = NOVALUE;

    /** 	for i = key_location to nrecs+1 do*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        _6953 = _nrecs_12575 + 1;
        if (_6953 > MAXINT){
            _6953 = NewDouble((double)_6953);
        }
    }
    else
    _6953 = binary_op(PLUS, 1, _nrecs_12575);
    {
        int _i_12659;
        Ref(_key_location_12579);
        _i_12659 = _key_location_12579;
LA: 
        if (binary_op_a(GREATER, _i_12659, _6953)){
            goto LB; // [478] 537
        }

        /** 		put4(key_pointers[i+r-nrecs])*/
        if (IS_ATOM_INT(_i_12659)) {
            _6954 = _i_12659 + _r_12584;
            if ((long)((unsigned long)_6954 + (unsigned long)HIGH_BITS) >= 0) 
            _6954 = NewDouble((double)_6954);
        }
        else {
            _6954 = NewDouble(DBL_PTR(_i_12659)->dbl + (double)_r_12584);
        }
        if (IS_ATOM_INT(_6954) && IS_ATOM_INT(_nrecs_12575)) {
            _6955 = _6954 - _nrecs_12575;
        }
        else {
            if (IS_ATOM_INT(_6954)) {
                _6955 = NewDouble((double)_6954 - DBL_PTR(_nrecs_12575)->dbl);
            }
            else {
                if (IS_ATOM_INT(_nrecs_12575)) {
                    _6955 = NewDouble(DBL_PTR(_6954)->dbl - (double)_nrecs_12575);
                }
                else
                _6955 = NewDouble(DBL_PTR(_6954)->dbl - DBL_PTR(_nrecs_12575)->dbl);
            }
        }
        DeRef(_6954);
        _6954 = NOVALUE;
        _2 = (int)SEQ_PTR(_2key_pointers_10887);
        if (!IS_ATOM_INT(_6955)){
            _6956 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_6955)->dbl));
        }
        else{
            _6956 = (int)*(((s1_ptr)_2)->base + _6955);
        }
        Ref(_6956);
        DeRef(_x_inlined_put4_at_500_12665);
        _x_inlined_put4_at_500_12665 = _6956;
        _6956 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_2mem0_10922)){
            poke4_addr = (unsigned long *)_2mem0_10922;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_500_12665)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_500_12665;
        }
        else if (IS_ATOM(_x_inlined_put4_at_500_12665)) {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_500_12665)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_x_inlined_put4_at_500_12665);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at503_12666);
        _1 = (int)SEQ_PTR(_2memseq_11142);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at503_12666 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_2current_db_10880, _put4_1__tmp_at503_12666); // DJP 

        /** end procedure*/
        goto LC; // [525] 528
LC: 
        DeRef(_x_inlined_put4_at_500_12665);
        _x_inlined_put4_at_500_12665 = NOVALUE;
        DeRefi(_put4_1__tmp_at503_12666);
        _put4_1__tmp_at503_12666 = NOVALUE;

        /** 	end for*/
        _0 = _i_12659;
        if (IS_ATOM_INT(_i_12659)) {
            _i_12659 = _i_12659 + 1;
            if ((long)((unsigned long)_i_12659 +(unsigned long) HIGH_BITS) >= 0){
                _i_12659 = NewDouble((double)_i_12659);
            }
        }
        else {
            _i_12659 = binary_op_a(PLUS, _i_12659, 1);
        }
        DeRef(_0);
        goto LA; // [532] 485
LB: 
        ;
        DeRef(_i_12659);
    }

    /** 	io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_12576);
    DeRef(_seek_1__tmp_at540_12669);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _current_block_12576;
    _seek_1__tmp_at540_12669 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_540_12668 = machine(19, _seek_1__tmp_at540_12669);
    DeRef(_seek_1__tmp_at540_12669);
    _seek_1__tmp_at540_12669 = NOVALUE;

    /** 	nrecs += 1*/
    _0 = _nrecs_12575;
    if (IS_ATOM_INT(_nrecs_12575)) {
        _nrecs_12575 = _nrecs_12575 + 1;
        if (_nrecs_12575 > MAXINT){
            _nrecs_12575 = NewDouble((double)_nrecs_12575);
        }
    }
    else
    _nrecs_12575 = binary_op(PLUS, 1, _nrecs_12575);
    DeRef(_0);

    /** 	put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_12575)) {
        *poke4_addr = (unsigned long)_nrecs_12575;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_12575)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at561_12672);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at561_12672 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at561_12672); // DJP 

    /** end procedure*/
    goto LD; // [583] 586
LD: 
    DeRefi(_put4_1__tmp_at561_12672);
    _put4_1__tmp_at561_12672 = NOVALUE;

    /** 	io:seek(current_db, records_ptr - 4)*/
    if (IS_ATOM_INT(_records_ptr_12574)) {
        _6958 = _records_ptr_12574 - 4;
        if ((long)((unsigned long)_6958 +(unsigned long) HIGH_BITS) >= 0){
            _6958 = NewDouble((double)_6958);
        }
    }
    else {
        _6958 = NewDouble(DBL_PTR(_records_ptr_12574)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_595_12675);
    _pos_inlined_seek_at_595_12675 = _6958;
    _6958 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_595_12675);
    DeRef(_seek_1__tmp_at598_12677);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_595_12675;
    _seek_1__tmp_at598_12677 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_598_12676 = machine(19, _seek_1__tmp_at598_12677);
    DeRef(_pos_inlined_seek_at_595_12675);
    _pos_inlined_seek_at_595_12675 = NOVALUE;
    DeRef(_seek_1__tmp_at598_12677);
    _seek_1__tmp_at598_12677 = NOVALUE;

    /** 	size = get4() - 4*/
    _6959 = _2get4();
    DeRef(_size_12577);
    if (IS_ATOM_INT(_6959)) {
        _size_12577 = _6959 - 4;
        if ((long)((unsigned long)_size_12577 +(unsigned long) HIGH_BITS) >= 0){
            _size_12577 = NewDouble((double)_size_12577);
        }
    }
    else {
        _size_12577 = binary_op(MINUS, _6959, 4);
    }
    DeRef(_6959);
    _6959 = NOVALUE;

    /** 	if nrecs*4 > size-4 then*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        if (_nrecs_12575 == (short)_nrecs_12575)
        _6961 = _nrecs_12575 * 4;
        else
        _6961 = NewDouble(_nrecs_12575 * (double)4);
    }
    else {
        _6961 = NewDouble(DBL_PTR(_nrecs_12575)->dbl * (double)4);
    }
    if (IS_ATOM_INT(_size_12577)) {
        _6962 = _size_12577 - 4;
        if ((long)((unsigned long)_6962 +(unsigned long) HIGH_BITS) >= 0){
            _6962 = NewDouble((double)_6962);
        }
    }
    else {
        _6962 = NewDouble(DBL_PTR(_size_12577)->dbl - (double)4);
    }
    if (binary_op_a(LESSEQ, _6961, _6962)){
        DeRef(_6961);
        _6961 = NOVALUE;
        DeRef(_6962);
        _6962 = NOVALUE;
        goto LE; // [631] 1233
    }
    DeRef(_6961);
    _6961 = NOVALUE;
    DeRef(_6962);
    _6962 = NOVALUE;

    /** 		new_size = 8 * (20 + floor(sqrt(1.5 * total_recs)))*/
    if (IS_ATOM_INT(_total_recs_12583)) {
        _6964 = NewDouble(DBL_PTR(_3393)->dbl * (double)_total_recs_12583);
    }
    else
    _6964 = NewDouble(DBL_PTR(_3393)->dbl * DBL_PTR(_total_recs_12583)->dbl);
    _6965 = unary_op(SQRT, _6964);
    DeRefDS(_6964);
    _6964 = NOVALUE;
    _6966 = unary_op(FLOOR, _6965);
    DeRefDS(_6965);
    _6965 = NOVALUE;
    if (IS_ATOM_INT(_6966)) {
        _6967 = 20 + _6966;
        if ((long)((unsigned long)_6967 + (unsigned long)HIGH_BITS) >= 0) 
        _6967 = NewDouble((double)_6967);
    }
    else {
        _6967 = binary_op(PLUS, 20, _6966);
    }
    DeRef(_6966);
    _6966 = NOVALUE;
    DeRef(_new_size_12578);
    if (IS_ATOM_INT(_6967)) {
        if (_6967 <= INT15 && _6967 >= -INT15)
        _new_size_12578 = 8 * _6967;
        else
        _new_size_12578 = NewDouble(8 * (double)_6967);
    }
    else {
        _new_size_12578 = binary_op(MULTIPLY, 8, _6967);
    }
    DeRef(_6967);
    _6967 = NOVALUE;

    /** 		new_recs = floor(new_size/8)*/
    if (IS_ATOM_INT(_new_size_12578)) {
        if (8 > 0 && _new_size_12578 >= 0) {
            _new_recs_12586 = _new_size_12578 / 8;
        }
        else {
            temp_dbl = floor((double)_new_size_12578 / (double)8);
            _new_recs_12586 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _new_size_12578, 8);
        _new_recs_12586 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (!IS_ATOM_INT(_new_recs_12586)) {
        _1 = (long)(DBL_PTR(_new_recs_12586)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_12586)) && (DBL_PTR(_new_recs_12586)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_12586);
        _new_recs_12586 = _1;
    }

    /** 		if new_recs > floor(nrecs/2) then*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        _6970 = _nrecs_12575 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_12575, 2);
        _6970 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (binary_op_a(LESSEQ, _new_recs_12586, _6970)){
        DeRef(_6970);
        _6970 = NOVALUE;
        goto LF; // [671] 686
    }
    DeRef(_6970);
    _6970 = NOVALUE;

    /** 			new_recs = floor(nrecs/2)*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        _new_recs_12586 = _nrecs_12575 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_12575, 2);
        _new_recs_12586 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (!IS_ATOM_INT(_new_recs_12586)) {
        _1 = (long)(DBL_PTR(_new_recs_12586)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_12586)) && (DBL_PTR(_new_recs_12586)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_12586);
        _new_recs_12586 = _1;
    }
LF: 

    /** 		io:seek(current_db, records_ptr + (nrecs-new_recs)*4)*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        _6973 = _nrecs_12575 - _new_recs_12586;
        if ((long)((unsigned long)_6973 +(unsigned long) HIGH_BITS) >= 0){
            _6973 = NewDouble((double)_6973);
        }
    }
    else {
        _6973 = NewDouble(DBL_PTR(_nrecs_12575)->dbl - (double)_new_recs_12586);
    }
    if (IS_ATOM_INT(_6973)) {
        if (_6973 == (short)_6973)
        _6974 = _6973 * 4;
        else
        _6974 = NewDouble(_6973 * (double)4);
    }
    else {
        _6974 = NewDouble(DBL_PTR(_6973)->dbl * (double)4);
    }
    DeRef(_6973);
    _6973 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_12574) && IS_ATOM_INT(_6974)) {
        _6975 = _records_ptr_12574 + _6974;
        if ((long)((unsigned long)_6975 + (unsigned long)HIGH_BITS) >= 0) 
        _6975 = NewDouble((double)_6975);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_12574)) {
            _6975 = NewDouble((double)_records_ptr_12574 + DBL_PTR(_6974)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6974)) {
                _6975 = NewDouble(DBL_PTR(_records_ptr_12574)->dbl + (double)_6974);
            }
            else
            _6975 = NewDouble(DBL_PTR(_records_ptr_12574)->dbl + DBL_PTR(_6974)->dbl);
        }
    }
    DeRef(_6974);
    _6974 = NOVALUE;
    DeRef(_pos_inlined_seek_at_701_12698);
    _pos_inlined_seek_at_701_12698 = _6975;
    _6975 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_701_12698);
    DeRef(_seek_1__tmp_at704_12700);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_701_12698;
    _seek_1__tmp_at704_12700 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_704_12699 = machine(19, _seek_1__tmp_at704_12700);
    DeRef(_pos_inlined_seek_at_701_12698);
    _pos_inlined_seek_at_701_12698 = NOVALUE;
    DeRef(_seek_1__tmp_at704_12700);
    _seek_1__tmp_at704_12700 = NOVALUE;

    /** 		last_part = io:get_bytes(current_db, new_recs*4)*/
    if (_new_recs_12586 == (short)_new_recs_12586)
    _6976 = _new_recs_12586 * 4;
    else
    _6976 = NewDouble(_new_recs_12586 * (double)4);
    _0 = _last_part_12570;
    _last_part_12570 = _15get_bytes(_2current_db_10880, _6976);
    DeRef(_0);
    _6976 = NOVALUE;

    /** 		new_block = db_allocate(new_size)*/
    Ref(_new_size_12578);
    _0 = _new_block_12580;
    _new_block_12580 = _2db_allocate(_new_size_12578);
    DeRef(_0);

    /** 		putn(last_part)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _last_part_12570); // DJP 

    /** end procedure*/
    goto L10; // [750] 753
L10: 

    /** 		putn(repeat(0, new_size-length(last_part)))*/
    if (IS_SEQUENCE(_last_part_12570)){
            _6979 = SEQ_PTR(_last_part_12570)->length;
    }
    else {
        _6979 = 1;
    }
    if (IS_ATOM_INT(_new_size_12578)) {
        _6980 = _new_size_12578 - _6979;
    }
    else {
        _6980 = NewDouble(DBL_PTR(_new_size_12578)->dbl - (double)_6979);
    }
    _6979 = NOVALUE;
    _6981 = Repeat(0, _6980);
    DeRef(_6980);
    _6980 = NOVALUE;
    DeRefi(_s_inlined_putn_at_765_12709);
    _s_inlined_putn_at_765_12709 = _6981;
    _6981 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_765_12709); // DJP 

    /** end procedure*/
    goto L11; // [780] 783
L11: 
    DeRefi(_s_inlined_putn_at_765_12709);
    _s_inlined_putn_at_765_12709 = NOVALUE;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_12576);
    DeRef(_seek_1__tmp_at788_12712);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _current_block_12576;
    _seek_1__tmp_at788_12712 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_788_12711 = machine(19, _seek_1__tmp_at788_12712);
    DeRef(_seek_1__tmp_at788_12712);
    _seek_1__tmp_at788_12712 = NOVALUE;

    /** 		put4(nrecs-new_recs)*/
    if (IS_ATOM_INT(_nrecs_12575)) {
        _6982 = _nrecs_12575 - _new_recs_12586;
        if ((long)((unsigned long)_6982 +(unsigned long) HIGH_BITS) >= 0){
            _6982 = NewDouble((double)_6982);
        }
    }
    else {
        _6982 = NewDouble(DBL_PTR(_nrecs_12575)->dbl - (double)_new_recs_12586);
    }
    DeRef(_x_inlined_put4_at_807_12715);
    _x_inlined_put4_at_807_12715 = _6982;
    _6982 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_807_12715)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_807_12715;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_807_12715)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at810_12716);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at810_12716 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at810_12716); // DJP 

    /** end procedure*/
    goto L12; // [832] 835
L12: 
    DeRef(_x_inlined_put4_at_807_12715);
    _x_inlined_put4_at_807_12715 = NOVALUE;
    DeRefi(_put4_1__tmp_at810_12716);
    _put4_1__tmp_at810_12716 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_12576)) {
        _6983 = _current_block_12576 + 8;
        if ((long)((unsigned long)_6983 + (unsigned long)HIGH_BITS) >= 0) 
        _6983 = NewDouble((double)_6983);
    }
    else {
        _6983 = NewDouble(DBL_PTR(_current_block_12576)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_844_12719);
    _pos_inlined_seek_at_844_12719 = _6983;
    _6983 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_844_12719);
    DeRef(_seek_1__tmp_at847_12721);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_844_12719;
    _seek_1__tmp_at847_12721 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_847_12720 = machine(19, _seek_1__tmp_at847_12721);
    DeRef(_pos_inlined_seek_at_844_12719);
    _pos_inlined_seek_at_844_12719 = NOVALUE;
    DeRef(_seek_1__tmp_at847_12721);
    _seek_1__tmp_at847_12721 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, index_ptr+blocks*8-(current_block+8))*/
    if (_blocks_12585 == (short)_blocks_12585)
    _6984 = _blocks_12585 * 8;
    else
    _6984 = NewDouble(_blocks_12585 * (double)8);
    if (IS_ATOM_INT(_index_ptr_12581) && IS_ATOM_INT(_6984)) {
        _6985 = _index_ptr_12581 + _6984;
        if ((long)((unsigned long)_6985 + (unsigned long)HIGH_BITS) >= 0) 
        _6985 = NewDouble((double)_6985);
    }
    else {
        if (IS_ATOM_INT(_index_ptr_12581)) {
            _6985 = NewDouble((double)_index_ptr_12581 + DBL_PTR(_6984)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6984)) {
                _6985 = NewDouble(DBL_PTR(_index_ptr_12581)->dbl + (double)_6984);
            }
            else
            _6985 = NewDouble(DBL_PTR(_index_ptr_12581)->dbl + DBL_PTR(_6984)->dbl);
        }
    }
    DeRef(_6984);
    _6984 = NOVALUE;
    if (IS_ATOM_INT(_current_block_12576)) {
        _6986 = _current_block_12576 + 8;
        if ((long)((unsigned long)_6986 + (unsigned long)HIGH_BITS) >= 0) 
        _6986 = NewDouble((double)_6986);
    }
    else {
        _6986 = NewDouble(DBL_PTR(_current_block_12576)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_6985) && IS_ATOM_INT(_6986)) {
        _6987 = _6985 - _6986;
        if ((long)((unsigned long)_6987 +(unsigned long) HIGH_BITS) >= 0){
            _6987 = NewDouble((double)_6987);
        }
    }
    else {
        if (IS_ATOM_INT(_6985)) {
            _6987 = NewDouble((double)_6985 - DBL_PTR(_6986)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6986)) {
                _6987 = NewDouble(DBL_PTR(_6985)->dbl - (double)_6986);
            }
            else
            _6987 = NewDouble(DBL_PTR(_6985)->dbl - DBL_PTR(_6986)->dbl);
        }
    }
    DeRef(_6985);
    _6985 = NOVALUE;
    DeRef(_6986);
    _6986 = NOVALUE;
    _0 = _remaining_12571;
    _remaining_12571 = _15get_bytes(_2current_db_10880, _6987);
    DeRef(_0);
    _6987 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_12576)) {
        _6989 = _current_block_12576 + 8;
        if ((long)((unsigned long)_6989 + (unsigned long)HIGH_BITS) >= 0) 
        _6989 = NewDouble((double)_6989);
    }
    else {
        _6989 = NewDouble(DBL_PTR(_current_block_12576)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_895_12729);
    _pos_inlined_seek_at_895_12729 = _6989;
    _6989 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_895_12729);
    DeRef(_seek_1__tmp_at898_12731);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_895_12729;
    _seek_1__tmp_at898_12731 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_898_12730 = machine(19, _seek_1__tmp_at898_12731);
    DeRef(_pos_inlined_seek_at_895_12729);
    _pos_inlined_seek_at_895_12729 = NOVALUE;
    DeRef(_seek_1__tmp_at898_12731);
    _seek_1__tmp_at898_12731 = NOVALUE;

    /** 		put4(new_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)_new_recs_12586;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at913_12733);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at913_12733 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at913_12733); // DJP 

    /** end procedure*/
    goto L13; // [935] 938
L13: 
    DeRefi(_put4_1__tmp_at913_12733);
    _put4_1__tmp_at913_12733 = NOVALUE;

    /** 		put4(new_block)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_new_block_12580)) {
        *poke4_addr = (unsigned long)_new_block_12580;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_block_12580)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at941_12735);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at941_12735 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at941_12735); // DJP 

    /** end procedure*/
    goto L14; // [963] 966
L14: 
    DeRefi(_put4_1__tmp_at941_12735);
    _put4_1__tmp_at941_12735 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_12571); // DJP 

    /** end procedure*/
    goto L15; // [979] 982
L15: 

    /** 		io:seek(current_db, current_table_pos+8)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _6990 = _2current_table_pos_10881 + 8;
        if ((long)((unsigned long)_6990 + (unsigned long)HIGH_BITS) >= 0) 
        _6990 = NewDouble((double)_6990);
    }
    else {
        _6990 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_991_12739);
    _pos_inlined_seek_at_991_12739 = _6990;
    _6990 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_991_12739);
    DeRef(_seek_1__tmp_at994_12741);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_991_12739;
    _seek_1__tmp_at994_12741 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_994_12740 = machine(19, _seek_1__tmp_at994_12741);
    DeRef(_pos_inlined_seek_at_991_12739);
    _pos_inlined_seek_at_991_12739 = NOVALUE;
    DeRef(_seek_1__tmp_at994_12741);
    _seek_1__tmp_at994_12741 = NOVALUE;

    /** 		blocks += 1*/
    _blocks_12585 = _blocks_12585 + 1;

    /** 		put4(blocks)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    *poke4_addr = (unsigned long)_blocks_12585;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1017_12744);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1017_12744 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at1017_12744); // DJP 

    /** end procedure*/
    goto L16; // [1039] 1042
L16: 
    DeRefi(_put4_1__tmp_at1017_12744);
    _put4_1__tmp_at1017_12744 = NOVALUE;

    /** 		io:seek(current_db, index_ptr-4)*/
    if (IS_ATOM_INT(_index_ptr_12581)) {
        _6992 = _index_ptr_12581 - 4;
        if ((long)((unsigned long)_6992 +(unsigned long) HIGH_BITS) >= 0){
            _6992 = NewDouble((double)_6992);
        }
    }
    else {
        _6992 = NewDouble(DBL_PTR(_index_ptr_12581)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_1051_12747);
    _pos_inlined_seek_at_1051_12747 = _6992;
    _6992 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1051_12747);
    DeRef(_seek_1__tmp_at1054_12749);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_1051_12747;
    _seek_1__tmp_at1054_12749 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1054_12748 = machine(19, _seek_1__tmp_at1054_12749);
    DeRef(_pos_inlined_seek_at_1051_12747);
    _pos_inlined_seek_at_1051_12747 = NOVALUE;
    DeRef(_seek_1__tmp_at1054_12749);
    _seek_1__tmp_at1054_12749 = NOVALUE;

    /** 		size = get4() - 4*/
    _6993 = _2get4();
    DeRef(_size_12577);
    if (IS_ATOM_INT(_6993)) {
        _size_12577 = _6993 - 4;
        if ((long)((unsigned long)_size_12577 +(unsigned long) HIGH_BITS) >= 0){
            _size_12577 = NewDouble((double)_size_12577);
        }
    }
    else {
        _size_12577 = binary_op(MINUS, _6993, 4);
    }
    DeRef(_6993);
    _6993 = NOVALUE;

    /** 		if blocks*8 > size-8 then*/
    if (_blocks_12585 == (short)_blocks_12585)
    _6995 = _blocks_12585 * 8;
    else
    _6995 = NewDouble(_blocks_12585 * (double)8);
    if (IS_ATOM_INT(_size_12577)) {
        _6996 = _size_12577 - 8;
        if ((long)((unsigned long)_6996 +(unsigned long) HIGH_BITS) >= 0){
            _6996 = NewDouble((double)_6996);
        }
    }
    else {
        _6996 = NewDouble(DBL_PTR(_size_12577)->dbl - (double)8);
    }
    if (binary_op_a(LESSEQ, _6995, _6996)){
        DeRef(_6995);
        _6995 = NOVALUE;
        DeRef(_6996);
        _6996 = NOVALUE;
        goto L17; // [1087] 1232
    }
    DeRef(_6995);
    _6995 = NOVALUE;
    DeRef(_6996);
    _6996 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, blocks*8)*/
    if (_blocks_12585 == (short)_blocks_12585)
    _6998 = _blocks_12585 * 8;
    else
    _6998 = NewDouble(_blocks_12585 * (double)8);
    _0 = _remaining_12571;
    _remaining_12571 = _15get_bytes(_2current_db_10880, _6998);
    DeRef(_0);
    _6998 = NOVALUE;

    /** 			new_size = floor(size + size/2)*/
    if (IS_ATOM_INT(_size_12577)) {
        if (_size_12577 & 1) {
            _7000 = NewDouble((_size_12577 >> 1) + 0.5);
        }
        else
        _7000 = _size_12577 >> 1;
    }
    else {
        _7000 = binary_op(DIVIDE, _size_12577, 2);
    }
    if (IS_ATOM_INT(_size_12577) && IS_ATOM_INT(_7000)) {
        _7001 = _size_12577 + _7000;
        if ((long)((unsigned long)_7001 + (unsigned long)HIGH_BITS) >= 0) 
        _7001 = NewDouble((double)_7001);
    }
    else {
        if (IS_ATOM_INT(_size_12577)) {
            _7001 = NewDouble((double)_size_12577 + DBL_PTR(_7000)->dbl);
        }
        else {
            if (IS_ATOM_INT(_7000)) {
                _7001 = NewDouble(DBL_PTR(_size_12577)->dbl + (double)_7000);
            }
            else
            _7001 = NewDouble(DBL_PTR(_size_12577)->dbl + DBL_PTR(_7000)->dbl);
        }
    }
    DeRef(_7000);
    _7000 = NOVALUE;
    DeRef(_new_size_12578);
    if (IS_ATOM_INT(_7001))
    _new_size_12578 = e_floor(_7001);
    else
    _new_size_12578 = unary_op(FLOOR, _7001);
    DeRef(_7001);
    _7001 = NOVALUE;

    /** 			new_index_ptr = db_allocate(new_size)*/
    Ref(_new_size_12578);
    _0 = _new_index_ptr_12582;
    _new_index_ptr_12582 = _2db_allocate(_new_size_12578);
    DeRef(_0);

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_12571); // DJP 

    /** end procedure*/
    goto L18; // [1136] 1139
L18: 

    /** 			putn(repeat(0, new_size-blocks*8))*/
    if (_blocks_12585 == (short)_blocks_12585)
    _7004 = _blocks_12585 * 8;
    else
    _7004 = NewDouble(_blocks_12585 * (double)8);
    if (IS_ATOM_INT(_new_size_12578) && IS_ATOM_INT(_7004)) {
        _7005 = _new_size_12578 - _7004;
    }
    else {
        if (IS_ATOM_INT(_new_size_12578)) {
            _7005 = NewDouble((double)_new_size_12578 - DBL_PTR(_7004)->dbl);
        }
        else {
            if (IS_ATOM_INT(_7004)) {
                _7005 = NewDouble(DBL_PTR(_new_size_12578)->dbl - (double)_7004);
            }
            else
            _7005 = NewDouble(DBL_PTR(_new_size_12578)->dbl - DBL_PTR(_7004)->dbl);
        }
    }
    DeRef(_7004);
    _7004 = NOVALUE;
    _7006 = Repeat(0, _7005);
    DeRef(_7005);
    _7005 = NOVALUE;
    DeRefi(_s_inlined_putn_at_1152_12767);
    _s_inlined_putn_at_1152_12767 = _7006;
    _7006 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _s_inlined_putn_at_1152_12767); // DJP 

    /** end procedure*/
    goto L19; // [1167] 1170
L19: 
    DeRefi(_s_inlined_putn_at_1152_12767);
    _s_inlined_putn_at_1152_12767 = NOVALUE;

    /** 			db_free(index_ptr)*/
    Ref(_index_ptr_12581);
    _2db_free(_index_ptr_12581);

    /** 			io:seek(current_db, current_table_pos+12)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _7007 = _2current_table_pos_10881 + 12;
        if ((long)((unsigned long)_7007 + (unsigned long)HIGH_BITS) >= 0) 
        _7007 = NewDouble((double)_7007);
    }
    else {
        _7007 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_1186_12770);
    _pos_inlined_seek_at_1186_12770 = _7007;
    _7007 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1186_12770);
    DeRef(_seek_1__tmp_at1189_12772);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_1186_12770;
    _seek_1__tmp_at1189_12772 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1189_12771 = machine(19, _seek_1__tmp_at1189_12772);
    DeRef(_pos_inlined_seek_at_1186_12770);
    _pos_inlined_seek_at_1186_12770 = NOVALUE;
    DeRef(_seek_1__tmp_at1189_12772);
    _seek_1__tmp_at1189_12772 = NOVALUE;

    /** 			put4(new_index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_new_index_ptr_12582)) {
        *poke4_addr = (unsigned long)_new_index_ptr_12582;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_index_ptr_12582)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1204_12774);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1204_12774 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at1204_12774); // DJP 

    /** end procedure*/
    goto L1A; // [1226] 1229
L1A: 
    DeRefi(_put4_1__tmp_at1204_12774);
    _put4_1__tmp_at1204_12774 = NOVALUE;
L17: 
LE: 

    /** 	return DB_OK*/
    DeRef(_key_12565);
    DeRef(_data_12566);
    DeRef(_table_name_12567);
    DeRef(_key_string_12568);
    DeRef(_data_string_12569);
    DeRef(_last_part_12570);
    DeRef(_remaining_12571);
    DeRef(_key_ptr_12572);
    DeRef(_data_ptr_12573);
    DeRef(_records_ptr_12574);
    DeRef(_nrecs_12575);
    DeRef(_current_block_12576);
    DeRef(_size_12577);
    DeRef(_new_size_12578);
    DeRef(_key_location_12579);
    DeRef(_new_block_12580);
    DeRef(_index_ptr_12581);
    DeRef(_new_index_ptr_12582);
    DeRef(_total_recs_12583);
    DeRef(_6953);
    _6953 = NOVALUE;
    DeRef(_6955);
    _6955 = NOVALUE;
    return 0;
    ;
}


void _2db_delete_record(int _key_location_12777, int _table_name_12778)
{
    int _key_ptr_12779 = NOVALUE;
    int _nrecs_12780 = NOVALUE;
    int _records_ptr_12781 = NOVALUE;
    int _data_ptr_12782 = NOVALUE;
    int _index_ptr_12783 = NOVALUE;
    int _current_block_12784 = NOVALUE;
    int _r_12785 = NOVALUE;
    int _blocks_12786 = NOVALUE;
    int _n_12787 = NOVALUE;
    int _remaining_12788 = NOVALUE;
    int _seek_1__tmp_at132_12810 = NOVALUE;
    int _seek_inlined_seek_at_132_12809 = NOVALUE;
    int _seek_1__tmp_at277_12832 = NOVALUE;
    int _seek_inlined_seek_at_277_12831 = NOVALUE;
    int _pos_inlined_seek_at_274_12830 = NOVALUE;
    int _seek_1__tmp_at321_12840 = NOVALUE;
    int _seek_inlined_seek_at_321_12839 = NOVALUE;
    int _pos_inlined_seek_at_318_12838 = NOVALUE;
    int _put4_1__tmp_at336_12842 = NOVALUE;
    int _seek_1__tmp_at375_12847 = NOVALUE;
    int _seek_inlined_seek_at_375_12846 = NOVALUE;
    int _pos_inlined_seek_at_372_12845 = NOVALUE;
    int _seek_1__tmp_at397_12851 = NOVALUE;
    int _seek_inlined_seek_at_397_12850 = NOVALUE;
    int _where_inlined_where_at_472_12860 = NOVALUE;
    int _seek_1__tmp_at538_12874 = NOVALUE;
    int _seek_inlined_seek_at_538_12873 = NOVALUE;
    int _seek_1__tmp_at578_12880 = NOVALUE;
    int _seek_inlined_seek_at_578_12879 = NOVALUE;
    int _pos_inlined_seek_at_575_12878 = NOVALUE;
    int _put4_1__tmp_at600_12884 = NOVALUE;
    int _x_inlined_put4_at_597_12883 = NOVALUE;
    int _seek_1__tmp_at648_12889 = NOVALUE;
    int _seek_inlined_seek_at_648_12888 = NOVALUE;
    int _put4_1__tmp_at663_12891 = NOVALUE;
    int _seek_1__tmp_at710_12898 = NOVALUE;
    int _seek_inlined_seek_at_710_12897 = NOVALUE;
    int _pos_inlined_seek_at_707_12896 = NOVALUE;
    int _put4_1__tmp_at750_12906 = NOVALUE;
    int _x_inlined_put4_at_747_12905 = NOVALUE;
    int _7067 = NOVALUE;
    int _7066 = NOVALUE;
    int _7065 = NOVALUE;
    int _7064 = NOVALUE;
    int _7063 = NOVALUE;
    int _7062 = NOVALUE;
    int _7060 = NOVALUE;
    int _7059 = NOVALUE;
    int _7057 = NOVALUE;
    int _7056 = NOVALUE;
    int _7055 = NOVALUE;
    int _7054 = NOVALUE;
    int _7053 = NOVALUE;
    int _7052 = NOVALUE;
    int _7051 = NOVALUE;
    int _7042 = NOVALUE;
    int _7041 = NOVALUE;
    int _7038 = NOVALUE;
    int _7037 = NOVALUE;
    int _7035 = NOVALUE;
    int _7034 = NOVALUE;
    int _7032 = NOVALUE;
    int _7031 = NOVALUE;
    int _7030 = NOVALUE;
    int _7029 = NOVALUE;
    int _7027 = NOVALUE;
    int _7023 = NOVALUE;
    int _7021 = NOVALUE;
    int _7019 = NOVALUE;
    int _7018 = NOVALUE;
    int _7016 = NOVALUE;
    int _7015 = NOVALUE;
    int _7013 = NOVALUE;
    int _7010 = NOVALUE;
    int _7008 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_12777)) {
        _1 = (long)(DBL_PTR(_key_location_12777)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_12777)) && (DBL_PTR(_key_location_12777)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_12777);
        _key_location_12777 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_12778 == _2current_table_name_10882)
    _7008 = 1;
    else if (IS_ATOM_INT(_table_name_12778) && IS_ATOM_INT(_2current_table_name_10882))
    _7008 = 0;
    else
    _7008 = (compare(_table_name_12778, _2current_table_name_10882) == 0);
    if (_7008 != 0)
    goto L1; // [13] 49
    _7008 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_12778);
    _7010 = _2db_select_table(_table_name_12778);
    if (binary_op_a(EQUALS, _7010, 0)){
        DeRef(_7010);
        _7010 = NOVALUE;
        goto L2; // [24] 48
    }
    DeRef(_7010);
    _7010 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_delete_record", {key_location, table_name})*/
    Ref(_table_name_12778);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12777;
    ((int *)_2)[2] = _table_name_12778;
    _7013 = MAKE_SEQ(_1);
    RefDS(_6895);
    RefDS(_7012);
    _2fatal(903, _6895, _7012, _7013);
    _7013 = NOVALUE;

    /** 			return*/
    DeRef(_table_name_12778);
    DeRef(_key_ptr_12779);
    DeRef(_nrecs_12780);
    DeRef(_records_ptr_12781);
    DeRef(_data_ptr_12782);
    DeRef(_index_ptr_12783);
    DeRef(_current_block_12784);
    DeRef(_remaining_12788);
    return;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _2current_table_pos_10881, -1)){
        goto L3; // [53] 77
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_delete_record", {key_location, table_name})*/
    Ref(_table_name_12778);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12777;
    ((int *)_2)[2] = _table_name_12778;
    _7015 = MAKE_SEQ(_1);
    RefDS(_6899);
    RefDS(_7012);
    _2fatal(903, _6899, _7012, _7015);
    _7015 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_12778);
    DeRef(_key_ptr_12779);
    DeRef(_nrecs_12780);
    DeRef(_records_ptr_12781);
    DeRef(_data_ptr_12782);
    DeRef(_index_ptr_12783);
    DeRef(_current_block_12784);
    DeRef(_remaining_12788);
    return;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _7016 = (_key_location_12777 < 1);
    if (_7016 != 0) {
        goto L4; // [83] 101
    }
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _7018 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _7018 = 1;
    }
    _7019 = (_key_location_12777 > _7018);
    _7018 = NOVALUE;
    if (_7019 == 0)
    {
        DeRef(_7019);
        _7019 = NOVALUE;
        goto L5; // [97] 121
    }
    else{
        DeRef(_7019);
        _7019 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_delete_record", {key_location, table_name})*/
    Ref(_table_name_12778);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12777;
    ((int *)_2)[2] = _table_name_12778;
    _7021 = MAKE_SEQ(_1);
    RefDS(_7020);
    RefDS(_7012);
    _2fatal(905, _7020, _7012, _7021);
    _7021 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_12778);
    DeRef(_key_ptr_12779);
    DeRef(_nrecs_12780);
    DeRef(_records_ptr_12781);
    DeRef(_data_ptr_12782);
    DeRef(_index_ptr_12783);
    DeRef(_current_block_12784);
    DeRef(_remaining_12788);
    DeRef(_7016);
    _7016 = NOVALUE;
    return;
L5: 

    /** 	key_ptr = key_pointers[key_location]*/
    DeRef(_key_ptr_12779);
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    _key_ptr_12779 = (int)*(((s1_ptr)_2)->base + _key_location_12777);
    Ref(_key_ptr_12779);

    /** 	io:seek(current_db, key_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_key_ptr_12779);
    DeRef(_seek_1__tmp_at132_12810);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _key_ptr_12779;
    _seek_1__tmp_at132_12810 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_132_12809 = machine(19, _seek_1__tmp_at132_12810);
    DeRef(_seek_1__tmp_at132_12810);
    _seek_1__tmp_at132_12810 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return end if*/
    if (IS_SEQUENCE(_2vLastErrors_10904)){
            _7023 = SEQ_PTR(_2vLastErrors_10904)->length;
    }
    else {
        _7023 = 1;
    }
    if (_7023 <= 0)
    goto L6; // [153] 161
    DeRef(_table_name_12778);
    DeRef(_key_ptr_12779);
    DeRef(_nrecs_12780);
    DeRef(_records_ptr_12781);
    DeRef(_data_ptr_12782);
    DeRef(_index_ptr_12783);
    DeRef(_current_block_12784);
    DeRef(_remaining_12788);
    DeRef(_7016);
    _7016 = NOVALUE;
    return;
L6: 

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_12782;
    _data_ptr_12782 = _2get4();
    DeRef(_0);

    /** 	db_free(key_ptr)*/
    Ref(_key_ptr_12779);
    _2db_free(_key_ptr_12779);

    /** 	db_free(data_ptr)*/
    Ref(_data_ptr_12782);
    _2db_free(_data_ptr_12782);

    /** 	n = length(key_pointers)*/
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _n_12787 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _n_12787 = 1;
    }

    /** 	if key_location >= floor(n/2) then*/
    _7027 = _n_12787 >> 1;
    if (_key_location_12777 < _7027)
    goto L7; // [191] 235

    /** 		key_pointers[key_location..n-1] = key_pointers[key_location+1..n]*/
    _7029 = _n_12787 - 1;
    _7030 = _key_location_12777 + 1;
    rhs_slice_target = (object_ptr)&_7031;
    RHS_Slice(_2key_pointers_10887, _7030, _n_12787);
    assign_slice_seq = (s1_ptr *)&_2key_pointers_10887;
    AssignSlice(_key_location_12777, _7029, _7031);
    _7029 = NOVALUE;
    DeRefDS(_7031);
    _7031 = NOVALUE;

    /** 		key_pointers = key_pointers[1..n-1]*/
    _7032 = _n_12787 - 1;
    rhs_slice_target = (object_ptr)&_2key_pointers_10887;
    RHS_Slice(_2key_pointers_10887, 1, _7032);
    goto L8; // [232] 265
L7: 

    /** 		key_pointers[2..key_location] = key_pointers[1..key_location-1]*/
    _7034 = _key_location_12777 - 1;
    rhs_slice_target = (object_ptr)&_7035;
    RHS_Slice(_2key_pointers_10887, 1, _7034);
    assign_slice_seq = (s1_ptr *)&_2key_pointers_10887;
    AssignSlice(2, _key_location_12777, _7035);
    DeRefDS(_7035);
    _7035 = NOVALUE;

    /** 		key_pointers = key_pointers[2..n]*/
    rhs_slice_target = (object_ptr)&_2key_pointers_10887;
    RHS_Slice(_2key_pointers_10887, 2, _n_12787);
L8: 

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _7037 = _2current_table_pos_10881 + 4;
        if ((long)((unsigned long)_7037 + (unsigned long)HIGH_BITS) >= 0) 
        _7037 = NewDouble((double)_7037);
    }
    else {
        _7037 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_274_12830);
    _pos_inlined_seek_at_274_12830 = _7037;
    _7037 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_274_12830);
    DeRef(_seek_1__tmp_at277_12832);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_274_12830;
    _seek_1__tmp_at277_12832 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_277_12831 = machine(19, _seek_1__tmp_at277_12832);
    DeRef(_pos_inlined_seek_at_274_12830);
    _pos_inlined_seek_at_274_12830 = NOVALUE;
    DeRef(_seek_1__tmp_at277_12832);
    _seek_1__tmp_at277_12832 = NOVALUE;

    /** 	nrecs = get4()-1*/
    _7038 = _2get4();
    DeRef(_nrecs_12780);
    if (IS_ATOM_INT(_7038)) {
        _nrecs_12780 = _7038 - 1;
        if ((long)((unsigned long)_nrecs_12780 +(unsigned long) HIGH_BITS) >= 0){
            _nrecs_12780 = NewDouble((double)_nrecs_12780);
        }
    }
    else {
        _nrecs_12780 = binary_op(MINUS, _7038, 1);
    }
    DeRef(_7038);
    _7038 = NOVALUE;

    /** 	blocks = get4()*/
    _blocks_12786 = _2get4();
    if (!IS_ATOM_INT(_blocks_12786)) {
        _1 = (long)(DBL_PTR(_blocks_12786)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_12786)) && (DBL_PTR(_blocks_12786)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_12786);
        _blocks_12786 = _1;
    }

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _7041 = _2current_table_pos_10881 + 4;
        if ((long)((unsigned long)_7041 + (unsigned long)HIGH_BITS) >= 0) 
        _7041 = NewDouble((double)_7041);
    }
    else {
        _7041 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_318_12838);
    _pos_inlined_seek_at_318_12838 = _7041;
    _7041 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_318_12838);
    DeRef(_seek_1__tmp_at321_12840);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_318_12838;
    _seek_1__tmp_at321_12840 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_321_12839 = machine(19, _seek_1__tmp_at321_12840);
    DeRef(_pos_inlined_seek_at_318_12838);
    _pos_inlined_seek_at_318_12838 = NOVALUE;
    DeRef(_seek_1__tmp_at321_12840);
    _seek_1__tmp_at321_12840 = NOVALUE;

    /** 	put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_12780)) {
        *poke4_addr = (unsigned long)_nrecs_12780;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_12780)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at336_12842);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at336_12842 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at336_12842); // DJP 

    /** end procedure*/
    goto L9; // [358] 361
L9: 
    DeRefi(_put4_1__tmp_at336_12842);
    _put4_1__tmp_at336_12842 = NOVALUE;

    /** 	io:seek(current_db, current_table_pos+12)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _7042 = _2current_table_pos_10881 + 12;
        if ((long)((unsigned long)_7042 + (unsigned long)HIGH_BITS) >= 0) 
        _7042 = NewDouble((double)_7042);
    }
    else {
        _7042 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_372_12845);
    _pos_inlined_seek_at_372_12845 = _7042;
    _7042 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_372_12845);
    DeRef(_seek_1__tmp_at375_12847);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_372_12845;
    _seek_1__tmp_at375_12847 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_375_12846 = machine(19, _seek_1__tmp_at375_12847);
    DeRef(_pos_inlined_seek_at_372_12845);
    _pos_inlined_seek_at_372_12845 = NOVALUE;
    DeRef(_seek_1__tmp_at375_12847);
    _seek_1__tmp_at375_12847 = NOVALUE;

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_12783;
    _index_ptr_12783 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, index_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_index_ptr_12783);
    DeRef(_seek_1__tmp_at397_12851);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _index_ptr_12783;
    _seek_1__tmp_at397_12851 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_397_12850 = machine(19, _seek_1__tmp_at397_12851);
    DeRef(_seek_1__tmp_at397_12851);
    _seek_1__tmp_at397_12851 = NOVALUE;

    /** 	r = 0*/
    _r_12785 = 0;

    /** 	while TRUE do*/
LA: 

    /** 		nrecs = get4()*/
    _0 = _nrecs_12780;
    _nrecs_12780 = _2get4();
    DeRef(_0);

    /** 		records_ptr = get4()*/
    _0 = _records_ptr_12781;
    _records_ptr_12781 = _2get4();
    DeRef(_0);

    /** 		r += nrecs*/
    if (IS_ATOM_INT(_nrecs_12780)) {
        _r_12785 = _r_12785 + _nrecs_12780;
    }
    else {
        _r_12785 = NewDouble((double)_r_12785 + DBL_PTR(_nrecs_12780)->dbl);
    }
    if (!IS_ATOM_INT(_r_12785)) {
        _1 = (long)(DBL_PTR(_r_12785)->dbl);
        if (UNIQUE(DBL_PTR(_r_12785)) && (DBL_PTR(_r_12785)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_12785);
        _r_12785 = _1;
    }

    /** 		if r >= key_location then*/
    if (_r_12785 < _key_location_12777)
    goto LA; // [445] 423

    /** 			exit*/
    goto LB; // [451] 459

    /** 	end while*/
    goto LA; // [456] 423
LB: 

    /** 	r -= nrecs*/
    if (IS_ATOM_INT(_nrecs_12780)) {
        _r_12785 = _r_12785 - _nrecs_12780;
    }
    else {
        _r_12785 = NewDouble((double)_r_12785 - DBL_PTR(_nrecs_12780)->dbl);
    }
    if (!IS_ATOM_INT(_r_12785)) {
        _1 = (long)(DBL_PTR(_r_12785)->dbl);
        if (UNIQUE(DBL_PTR(_r_12785)) && (DBL_PTR(_r_12785)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_12785);
        _r_12785 = _1;
    }

    /** 	current_block = io:where(current_db)-8*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_472_12860);
    _where_inlined_where_at_472_12860 = machine(20, _2current_db_10880);
    DeRef(_current_block_12784);
    if (IS_ATOM_INT(_where_inlined_where_at_472_12860)) {
        _current_block_12784 = _where_inlined_where_at_472_12860 - 8;
        if ((long)((unsigned long)_current_block_12784 +(unsigned long) HIGH_BITS) >= 0){
            _current_block_12784 = NewDouble((double)_current_block_12784);
        }
    }
    else {
        _current_block_12784 = NewDouble(DBL_PTR(_where_inlined_where_at_472_12860)->dbl - (double)8);
    }

    /** 	nrecs -= 1*/
    _0 = _nrecs_12780;
    if (IS_ATOM_INT(_nrecs_12780)) {
        _nrecs_12780 = _nrecs_12780 - 1;
        if ((long)((unsigned long)_nrecs_12780 +(unsigned long) HIGH_BITS) >= 0){
            _nrecs_12780 = NewDouble((double)_nrecs_12780);
        }
    }
    else {
        _nrecs_12780 = NewDouble(DBL_PTR(_nrecs_12780)->dbl - (double)1);
    }
    DeRef(_0);

    /** 	if nrecs = 0 and blocks > 1 then*/
    if (IS_ATOM_INT(_nrecs_12780)) {
        _7051 = (_nrecs_12780 == 0);
    }
    else {
        _7051 = (DBL_PTR(_nrecs_12780)->dbl == (double)0);
    }
    if (_7051 == 0) {
        goto LC; // [496] 637
    }
    _7053 = (_blocks_12786 > 1);
    if (_7053 == 0)
    {
        DeRef(_7053);
        _7053 = NOVALUE;
        goto LC; // [505] 637
    }
    else{
        DeRef(_7053);
        _7053 = NOVALUE;
    }

    /** 		remaining = io:get_bytes(current_db, index_ptr+blocks*8-(current_block+8))*/
    if (_blocks_12786 == (short)_blocks_12786)
    _7054 = _blocks_12786 * 8;
    else
    _7054 = NewDouble(_blocks_12786 * (double)8);
    if (IS_ATOM_INT(_index_ptr_12783) && IS_ATOM_INT(_7054)) {
        _7055 = _index_ptr_12783 + _7054;
        if ((long)((unsigned long)_7055 + (unsigned long)HIGH_BITS) >= 0) 
        _7055 = NewDouble((double)_7055);
    }
    else {
        if (IS_ATOM_INT(_index_ptr_12783)) {
            _7055 = NewDouble((double)_index_ptr_12783 + DBL_PTR(_7054)->dbl);
        }
        else {
            if (IS_ATOM_INT(_7054)) {
                _7055 = NewDouble(DBL_PTR(_index_ptr_12783)->dbl + (double)_7054);
            }
            else
            _7055 = NewDouble(DBL_PTR(_index_ptr_12783)->dbl + DBL_PTR(_7054)->dbl);
        }
    }
    DeRef(_7054);
    _7054 = NOVALUE;
    if (IS_ATOM_INT(_current_block_12784)) {
        _7056 = _current_block_12784 + 8;
        if ((long)((unsigned long)_7056 + (unsigned long)HIGH_BITS) >= 0) 
        _7056 = NewDouble((double)_7056);
    }
    else {
        _7056 = NewDouble(DBL_PTR(_current_block_12784)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_7055) && IS_ATOM_INT(_7056)) {
        _7057 = _7055 - _7056;
        if ((long)((unsigned long)_7057 +(unsigned long) HIGH_BITS) >= 0){
            _7057 = NewDouble((double)_7057);
        }
    }
    else {
        if (IS_ATOM_INT(_7055)) {
            _7057 = NewDouble((double)_7055 - DBL_PTR(_7056)->dbl);
        }
        else {
            if (IS_ATOM_INT(_7056)) {
                _7057 = NewDouble(DBL_PTR(_7055)->dbl - (double)_7056);
            }
            else
            _7057 = NewDouble(DBL_PTR(_7055)->dbl - DBL_PTR(_7056)->dbl);
        }
    }
    DeRef(_7055);
    _7055 = NOVALUE;
    DeRef(_7056);
    _7056 = NOVALUE;
    _0 = _remaining_12788;
    _remaining_12788 = _15get_bytes(_2current_db_10880, _7057);
    DeRef(_0);
    _7057 = NOVALUE;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_12784);
    DeRef(_seek_1__tmp_at538_12874);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _current_block_12784;
    _seek_1__tmp_at538_12874 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_538_12873 = machine(19, _seek_1__tmp_at538_12874);
    DeRef(_seek_1__tmp_at538_12874);
    _seek_1__tmp_at538_12874 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _remaining_12788); // DJP 

    /** end procedure*/
    goto LD; // [563] 566
LD: 

    /** 		io:seek(current_db, current_table_pos+8)*/
    if (IS_ATOM_INT(_2current_table_pos_10881)) {
        _7059 = _2current_table_pos_10881 + 8;
        if ((long)((unsigned long)_7059 + (unsigned long)HIGH_BITS) >= 0) 
        _7059 = NewDouble((double)_7059);
    }
    else {
        _7059 = NewDouble(DBL_PTR(_2current_table_pos_10881)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_575_12878);
    _pos_inlined_seek_at_575_12878 = _7059;
    _7059 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_575_12878);
    DeRef(_seek_1__tmp_at578_12880);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_575_12878;
    _seek_1__tmp_at578_12880 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_578_12879 = machine(19, _seek_1__tmp_at578_12880);
    DeRef(_pos_inlined_seek_at_575_12878);
    _pos_inlined_seek_at_575_12878 = NOVALUE;
    DeRef(_seek_1__tmp_at578_12880);
    _seek_1__tmp_at578_12880 = NOVALUE;

    /** 		put4(blocks-1)*/
    _7060 = _blocks_12786 - 1;
    if ((long)((unsigned long)_7060 +(unsigned long) HIGH_BITS) >= 0){
        _7060 = NewDouble((double)_7060);
    }
    DeRef(_x_inlined_put4_at_597_12883);
    _x_inlined_put4_at_597_12883 = _7060;
    _7060 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_597_12883)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_597_12883;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_597_12883)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at600_12884);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at600_12884 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at600_12884); // DJP 

    /** end procedure*/
    goto LE; // [622] 625
LE: 
    DeRef(_x_inlined_put4_at_597_12883);
    _x_inlined_put4_at_597_12883 = NOVALUE;
    DeRefi(_put4_1__tmp_at600_12884);
    _put4_1__tmp_at600_12884 = NOVALUE;

    /** 		db_free(records_ptr)*/
    Ref(_records_ptr_12781);
    _2db_free(_records_ptr_12781);
    goto LF; // [634] 785
LC: 

    /** 		key_location -= r*/
    _key_location_12777 = _key_location_12777 - _r_12785;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_12784);
    DeRef(_seek_1__tmp_at648_12889);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _current_block_12784;
    _seek_1__tmp_at648_12889 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_648_12888 = machine(19, _seek_1__tmp_at648_12889);
    DeRef(_seek_1__tmp_at648_12889);
    _seek_1__tmp_at648_12889 = NOVALUE;

    /** 		put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_12780)) {
        *poke4_addr = (unsigned long)_nrecs_12780;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_12780)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at663_12891);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at663_12891 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at663_12891); // DJP 

    /** end procedure*/
    goto L10; // [685] 688
L10: 
    DeRefi(_put4_1__tmp_at663_12891);
    _put4_1__tmp_at663_12891 = NOVALUE;

    /** 		io:seek(current_db, records_ptr+4*(key_location-1))*/
    _7062 = _key_location_12777 - 1;
    if ((long)((unsigned long)_7062 +(unsigned long) HIGH_BITS) >= 0){
        _7062 = NewDouble((double)_7062);
    }
    if (IS_ATOM_INT(_7062)) {
        if (_7062 <= INT15 && _7062 >= -INT15)
        _7063 = 4 * _7062;
        else
        _7063 = NewDouble(4 * (double)_7062);
    }
    else {
        _7063 = NewDouble((double)4 * DBL_PTR(_7062)->dbl);
    }
    DeRef(_7062);
    _7062 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_12781) && IS_ATOM_INT(_7063)) {
        _7064 = _records_ptr_12781 + _7063;
        if ((long)((unsigned long)_7064 + (unsigned long)HIGH_BITS) >= 0) 
        _7064 = NewDouble((double)_7064);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_12781)) {
            _7064 = NewDouble((double)_records_ptr_12781 + DBL_PTR(_7063)->dbl);
        }
        else {
            if (IS_ATOM_INT(_7063)) {
                _7064 = NewDouble(DBL_PTR(_records_ptr_12781)->dbl + (double)_7063);
            }
            else
            _7064 = NewDouble(DBL_PTR(_records_ptr_12781)->dbl + DBL_PTR(_7063)->dbl);
        }
    }
    DeRef(_7063);
    _7063 = NOVALUE;
    DeRef(_pos_inlined_seek_at_707_12896);
    _pos_inlined_seek_at_707_12896 = _7064;
    _7064 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_707_12896);
    DeRef(_seek_1__tmp_at710_12898);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_707_12896;
    _seek_1__tmp_at710_12898 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_710_12897 = machine(19, _seek_1__tmp_at710_12898);
    DeRef(_pos_inlined_seek_at_707_12896);
    _pos_inlined_seek_at_707_12896 = NOVALUE;
    DeRef(_seek_1__tmp_at710_12898);
    _seek_1__tmp_at710_12898 = NOVALUE;

    /** 		for i = key_location to nrecs do*/
    Ref(_nrecs_12780);
    DeRef(_7065);
    _7065 = _nrecs_12780;
    {
        int _i_12900;
        _i_12900 = _key_location_12777;
L11: 
        if (binary_op_a(GREATER, _i_12900, _7065)){
            goto L12; // [729] 784
        }

        /** 			put4(key_pointers[i+r])*/
        if (IS_ATOM_INT(_i_12900)) {
            _7066 = _i_12900 + _r_12785;
        }
        else {
            _7066 = NewDouble(DBL_PTR(_i_12900)->dbl + (double)_r_12785);
        }
        _2 = (int)SEQ_PTR(_2key_pointers_10887);
        if (!IS_ATOM_INT(_7066)){
            _7067 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_7066)->dbl));
        }
        else{
            _7067 = (int)*(((s1_ptr)_2)->base + _7066);
        }
        Ref(_7067);
        DeRef(_x_inlined_put4_at_747_12905);
        _x_inlined_put4_at_747_12905 = _7067;
        _7067 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_2mem0_10922)){
            poke4_addr = (unsigned long *)_2mem0_10922;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_747_12905)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_747_12905;
        }
        else if (IS_ATOM(_x_inlined_put4_at_747_12905)) {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_747_12905)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_x_inlined_put4_at_747_12905);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at750_12906);
        _1 = (int)SEQ_PTR(_2memseq_11142);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at750_12906 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_2current_db_10880, _put4_1__tmp_at750_12906); // DJP 

        /** end procedure*/
        goto L13; // [772] 775
L13: 
        DeRef(_x_inlined_put4_at_747_12905);
        _x_inlined_put4_at_747_12905 = NOVALUE;
        DeRefi(_put4_1__tmp_at750_12906);
        _put4_1__tmp_at750_12906 = NOVALUE;

        /** 		end for*/
        _0 = _i_12900;
        if (IS_ATOM_INT(_i_12900)) {
            _i_12900 = _i_12900 + 1;
            if ((long)((unsigned long)_i_12900 +(unsigned long) HIGH_BITS) >= 0){
                _i_12900 = NewDouble((double)_i_12900);
            }
        }
        else {
            _i_12900 = binary_op_a(PLUS, _i_12900, 1);
        }
        DeRef(_0);
        goto L11; // [779] 736
L12: 
        ;
        DeRef(_i_12900);
    }
LF: 

    /** end procedure*/
    DeRef(_table_name_12778);
    DeRef(_key_ptr_12779);
    DeRef(_nrecs_12780);
    DeRef(_records_ptr_12781);
    DeRef(_data_ptr_12782);
    DeRef(_index_ptr_12783);
    DeRef(_current_block_12784);
    DeRef(_remaining_12788);
    DeRef(_7016);
    _7016 = NOVALUE;
    DeRef(_7027);
    _7027 = NOVALUE;
    DeRef(_7032);
    _7032 = NOVALUE;
    DeRef(_7030);
    _7030 = NOVALUE;
    DeRef(_7034);
    _7034 = NOVALUE;
    DeRef(_7051);
    _7051 = NOVALUE;
    DeRef(_7066);
    _7066 = NOVALUE;
    return;
    ;
}


void _2db_replace_data(int _key_location_12909, int _data_12910, int _table_name_12911)
{
    int _7081 = NOVALUE;
    int _7080 = NOVALUE;
    int _7079 = NOVALUE;
    int _7078 = NOVALUE;
    int _7076 = NOVALUE;
    int _7075 = NOVALUE;
    int _7073 = NOVALUE;
    int _7070 = NOVALUE;
    int _7068 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_12909)) {
        _1 = (long)(DBL_PTR(_key_location_12909)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_12909)) && (DBL_PTR(_key_location_12909)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_12909);
        _key_location_12909 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_12911 == _2current_table_name_10882)
    _7068 = 1;
    else if (IS_ATOM_INT(_table_name_12911) && IS_ATOM_INT(_2current_table_name_10882))
    _7068 = 0;
    else
    _7068 = (compare(_table_name_12911, _2current_table_name_10882) == 0);
    if (_7068 != 0)
    goto L1; // [13] 51
    _7068 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_12911);
    _7070 = _2db_select_table(_table_name_12911);
    if (binary_op_a(EQUALS, _7070, 0)){
        DeRef(_7070);
        _7070 = NOVALUE;
        goto L2; // [24] 50
    }
    DeRef(_7070);
    _7070 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_12909;
    Ref(_data_12910);
    *((int *)(_2+8)) = _data_12910;
    Ref(_table_name_12911);
    *((int *)(_2+12)) = _table_name_12911;
    _7073 = MAKE_SEQ(_1);
    RefDS(_6895);
    RefDS(_7072);
    _2fatal(903, _6895, _7072, _7073);
    _7073 = NOVALUE;

    /** 			return*/
    DeRef(_data_12910);
    DeRef(_table_name_12911);
    return;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _2current_table_pos_10881, -1)){
        goto L3; // [55] 81
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_12909;
    Ref(_data_12910);
    *((int *)(_2+8)) = _data_12910;
    Ref(_table_name_12911);
    *((int *)(_2+12)) = _table_name_12911;
    _7075 = MAKE_SEQ(_1);
    RefDS(_6899);
    RefDS(_7072);
    _2fatal(903, _6899, _7072, _7075);
    _7075 = NOVALUE;

    /** 		return*/
    DeRef(_data_12910);
    DeRef(_table_name_12911);
    return;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _7076 = (_key_location_12909 < 1);
    if (_7076 != 0) {
        goto L4; // [87] 105
    }
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _7078 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _7078 = 1;
    }
    _7079 = (_key_location_12909 > _7078);
    _7078 = NOVALUE;
    if (_7079 == 0)
    {
        DeRef(_7079);
        _7079 = NOVALUE;
        goto L5; // [101] 127
    }
    else{
        DeRef(_7079);
        _7079 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_12909;
    Ref(_data_12910);
    *((int *)(_2+8)) = _data_12910;
    Ref(_table_name_12911);
    *((int *)(_2+12)) = _table_name_12911;
    _7080 = MAKE_SEQ(_1);
    RefDS(_7020);
    RefDS(_7072);
    _2fatal(905, _7020, _7072, _7080);
    _7080 = NOVALUE;

    /** 		return*/
    DeRef(_data_12910);
    DeRef(_table_name_12911);
    DeRef(_7076);
    _7076 = NOVALUE;
    return;
L5: 

    /** 	db_replace_recid(key_pointers[key_location], data)*/
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    _7081 = (int)*(((s1_ptr)_2)->base + _key_location_12909);
    Ref(_7081);
    Ref(_data_12910);
    _2db_replace_recid(_7081, _data_12910);
    _7081 = NOVALUE;

    /** end procedure*/
    DeRef(_data_12910);
    DeRef(_table_name_12911);
    DeRef(_7076);
    _7076 = NOVALUE;
    return;
    ;
}


int _2db_table_size(int _table_name_12933)
{
    int _7090 = NOVALUE;
    int _7089 = NOVALUE;
    int _7087 = NOVALUE;
    int _7084 = NOVALUE;
    int _7082 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_12933 == _2current_table_name_10882)
    _7082 = 1;
    else if (IS_ATOM_INT(_table_name_12933) && IS_ATOM_INT(_2current_table_name_10882))
    _7082 = 0;
    else
    _7082 = (compare(_table_name_12933, _2current_table_name_10882) == 0);
    if (_7082 != 0)
    goto L1; // [9] 46
    _7082 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_12933);
    _7084 = _2db_select_table(_table_name_12933);
    if (binary_op_a(EQUALS, _7084, 0)){
        DeRef(_7084);
        _7084 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_7084);
    _7084 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_table_name_12933);
    *((int *)(_2+4)) = _table_name_12933;
    _7087 = MAKE_SEQ(_1);
    RefDS(_6895);
    RefDS(_7086);
    _2fatal(903, _6895, _7086, _7087);
    _7087 = NOVALUE;

    /** 			return -1*/
    DeRef(_table_name_12933);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _2current_table_pos_10881, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_table_name_12933);
    *((int *)(_2+4)) = _table_name_12933;
    _7089 = MAKE_SEQ(_1);
    RefDS(_6899);
    RefDS(_7086);
    _2fatal(903, _6899, _7086, _7089);
    _7089 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_12933);
    return -1;
L3: 

    /** 	return length(key_pointers)*/
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _7090 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _7090 = 1;
    }
    DeRef(_table_name_12933);
    return _7090;
    ;
}


int _2db_record_data(int _key_location_12948, int _table_name_12949)
{
    int _data_ptr_12950 = NOVALUE;
    int _data_value_12951 = NOVALUE;
    int _seek_1__tmp_at136_12973 = NOVALUE;
    int _seek_inlined_seek_at_136_12972 = NOVALUE;
    int _pos_inlined_seek_at_133_12971 = NOVALUE;
    int _seek_1__tmp_at174_12980 = NOVALUE;
    int _seek_inlined_seek_at_174_12979 = NOVALUE;
    int _7105 = NOVALUE;
    int _7104 = NOVALUE;
    int _7103 = NOVALUE;
    int _7102 = NOVALUE;
    int _7101 = NOVALUE;
    int _7099 = NOVALUE;
    int _7098 = NOVALUE;
    int _7096 = NOVALUE;
    int _7093 = NOVALUE;
    int _7091 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_12948)) {
        _1 = (long)(DBL_PTR(_key_location_12948)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_12948)) && (DBL_PTR(_key_location_12948)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_12948);
        _key_location_12948 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_12949 == _2current_table_name_10882)
    _7091 = 1;
    else if (IS_ATOM_INT(_table_name_12949) && IS_ATOM_INT(_2current_table_name_10882))
    _7091 = 0;
    else
    _7091 = (compare(_table_name_12949, _2current_table_name_10882) == 0);
    if (_7091 != 0)
    goto L1; // [13] 50
    _7091 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_12949);
    _7093 = _2db_select_table(_table_name_12949);
    if (binary_op_a(EQUALS, _7093, 0)){
        DeRef(_7093);
        _7093 = NOVALUE;
        goto L2; // [24] 49
    }
    DeRef(_7093);
    _7093 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_12949);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12948;
    ((int *)_2)[2] = _table_name_12949;
    _7096 = MAKE_SEQ(_1);
    RefDS(_6895);
    RefDS(_7095);
    _2fatal(903, _6895, _7095, _7096);
    _7096 = NOVALUE;

    /** 			return -1*/
    DeRef(_table_name_12949);
    DeRef(_data_ptr_12950);
    DeRef(_data_value_12951);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _2current_table_pos_10881, -1)){
        goto L3; // [54] 79
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_12949);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12948;
    ((int *)_2)[2] = _table_name_12949;
    _7098 = MAKE_SEQ(_1);
    RefDS(_6899);
    RefDS(_7095);
    _2fatal(903, _6899, _7095, _7098);
    _7098 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_12949);
    DeRef(_data_ptr_12950);
    DeRef(_data_value_12951);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _7099 = (_key_location_12948 < 1);
    if (_7099 != 0) {
        goto L4; // [85] 103
    }
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _7101 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _7101 = 1;
    }
    _7102 = (_key_location_12948 > _7101);
    _7101 = NOVALUE;
    if (_7102 == 0)
    {
        DeRef(_7102);
        _7102 = NOVALUE;
        goto L5; // [99] 124
    }
    else{
        DeRef(_7102);
        _7102 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_12949);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12948;
    ((int *)_2)[2] = _table_name_12949;
    _7103 = MAKE_SEQ(_1);
    RefDS(_7020);
    RefDS(_7095);
    _2fatal(905, _7020, _7095, _7103);
    _7103 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_12949);
    DeRef(_data_ptr_12950);
    DeRef(_data_value_12951);
    DeRef(_7099);
    _7099 = NOVALUE;
    return -1;
L5: 

    /** 	io:seek(current_db, key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    _7104 = (int)*(((s1_ptr)_2)->base + _key_location_12948);
    Ref(_7104);
    DeRef(_pos_inlined_seek_at_133_12971);
    _pos_inlined_seek_at_133_12971 = _7104;
    _7104 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_133_12971);
    DeRef(_seek_1__tmp_at136_12973);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _pos_inlined_seek_at_133_12971;
    _seek_1__tmp_at136_12973 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_136_12972 = machine(19, _seek_1__tmp_at136_12973);
    DeRef(_pos_inlined_seek_at_133_12971);
    _pos_inlined_seek_at_133_12971 = NOVALUE;
    DeRef(_seek_1__tmp_at136_12973);
    _seek_1__tmp_at136_12973 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_2vLastErrors_10904)){
            _7105 = SEQ_PTR(_2vLastErrors_10904)->length;
    }
    else {
        _7105 = 1;
    }
    if (_7105 <= 0)
    goto L6; // [157] 166
    DeRef(_table_name_12949);
    DeRef(_data_ptr_12950);
    DeRef(_data_value_12951);
    DeRef(_7099);
    _7099 = NOVALUE;
    return -1;
L6: 

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_12950;
    _data_ptr_12950 = _2get4();
    DeRef(_0);

    /** 	io:seek(current_db, data_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_data_ptr_12950);
    DeRef(_seek_1__tmp_at174_12980);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2current_db_10880;
    ((int *)_2)[2] = _data_ptr_12950;
    _seek_1__tmp_at174_12980 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_174_12979 = machine(19, _seek_1__tmp_at174_12980);
    DeRef(_seek_1__tmp_at174_12980);
    _seek_1__tmp_at174_12980 = NOVALUE;

    /** 	data_value = decompress(0)*/
    _0 = _data_value_12951;
    _data_value_12951 = _2decompress(0);
    DeRef(_0);

    /** 	return data_value*/
    DeRef(_table_name_12949);
    DeRef(_data_ptr_12950);
    DeRef(_7099);
    _7099 = NOVALUE;
    return _data_value_12951;
    ;
}


int _2db_fetch_record(int _key_12984, int _table_name_12985)
{
    int _pos_12986 = NOVALUE;
    int _7111 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pos = db_find_key(key, table_name)*/
    Ref(_key_12984);
    Ref(_table_name_12985);
    _pos_12986 = _2db_find_key(_key_12984, _table_name_12985);
    if (!IS_ATOM_INT(_pos_12986)) {
        _1 = (long)(DBL_PTR(_pos_12986)->dbl);
        if (UNIQUE(DBL_PTR(_pos_12986)) && (DBL_PTR(_pos_12986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_12986);
        _pos_12986 = _1;
    }

    /** 	if pos > 0 then*/
    if (_pos_12986 <= 0)
    goto L1; // [14] 32

    /** 		return db_record_data(pos, table_name)*/
    Ref(_table_name_12985);
    _7111 = _2db_record_data(_pos_12986, _table_name_12985);
    DeRef(_key_12984);
    DeRef(_table_name_12985);
    return _7111;
    goto L2; // [29] 39
L1: 

    /** 		return pos*/
    DeRef(_key_12984);
    DeRef(_table_name_12985);
    DeRef(_7111);
    _7111 = NOVALUE;
    return _pos_12986;
L2: 
    ;
}


int _2db_record_key(int _key_location_12994, int _table_name_12995)
{
    int _7126 = NOVALUE;
    int _7125 = NOVALUE;
    int _7124 = NOVALUE;
    int _7123 = NOVALUE;
    int _7122 = NOVALUE;
    int _7120 = NOVALUE;
    int _7119 = NOVALUE;
    int _7117 = NOVALUE;
    int _7114 = NOVALUE;
    int _7112 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_12994)) {
        _1 = (long)(DBL_PTR(_key_location_12994)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_12994)) && (DBL_PTR(_key_location_12994)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_12994);
        _key_location_12994 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_12995 == _2current_table_name_10882)
    _7112 = 1;
    else if (IS_ATOM_INT(_table_name_12995) && IS_ATOM_INT(_2current_table_name_10882))
    _7112 = 0;
    else
    _7112 = (compare(_table_name_12995, _2current_table_name_10882) == 0);
    if (_7112 != 0)
    goto L1; // [13] 50
    _7112 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_12995);
    _7114 = _2db_select_table(_table_name_12995);
    if (binary_op_a(EQUALS, _7114, 0)){
        DeRef(_7114);
        _7114 = NOVALUE;
        goto L2; // [24] 49
    }
    DeRef(_7114);
    _7114 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_12995);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12994;
    ((int *)_2)[2] = _table_name_12995;
    _7117 = MAKE_SEQ(_1);
    RefDS(_6895);
    RefDS(_7116);
    _2fatal(903, _6895, _7116, _7117);
    _7117 = NOVALUE;

    /** 			return -1*/
    DeRef(_table_name_12995);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _2current_table_pos_10881, -1)){
        goto L3; // [54] 79
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_12995);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12994;
    ((int *)_2)[2] = _table_name_12995;
    _7119 = MAKE_SEQ(_1);
    RefDS(_6899);
    RefDS(_7116);
    _2fatal(903, _6899, _7116, _7119);
    _7119 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_12995);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _7120 = (_key_location_12994 < 1);
    if (_7120 != 0) {
        goto L4; // [85] 103
    }
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _7122 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _7122 = 1;
    }
    _7123 = (_key_location_12994 > _7122);
    _7122 = NOVALUE;
    if (_7123 == 0)
    {
        DeRef(_7123);
        _7123 = NOVALUE;
        goto L5; // [99] 124
    }
    else{
        DeRef(_7123);
        _7123 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_12995);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_12994;
    ((int *)_2)[2] = _table_name_12995;
    _7124 = MAKE_SEQ(_1);
    RefDS(_7020);
    RefDS(_7116);
    _2fatal(905, _7020, _7116, _7124);
    _7124 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_12995);
    DeRef(_7120);
    _7120 = NOVALUE;
    return -1;
L5: 

    /** 	return key_value(key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    _7125 = (int)*(((s1_ptr)_2)->base + _key_location_12994);
    Ref(_7125);
    _7126 = _2key_value(_7125);
    _7125 = NOVALUE;
    DeRef(_table_name_12995);
    DeRef(_7120);
    _7120 = NOVALUE;
    return _7126;
    ;
}


int _2db_compress()
{
    int _index_13017 = NOVALUE;
    int _chunk_size_13018 = NOVALUE;
    int _nrecs_13019 = NOVALUE;
    int _r_13020 = NOVALUE;
    int _fn_13021 = NOVALUE;
    int _new_path_13022 = NOVALUE;
    int _table_list_13023 = NOVALUE;
    int _record_13024 = NOVALUE;
    int _chunk_13025 = NOVALUE;
    int _temp_path_13033 = NOVALUE;
    int _7172 = NOVALUE;
    int _7168 = NOVALUE;
    int _7167 = NOVALUE;
    int _7166 = NOVALUE;
    int _7165 = NOVALUE;
    int _7164 = NOVALUE;
    int _7163 = NOVALUE;
    int _7161 = NOVALUE;
    int _7156 = NOVALUE;
    int _7155 = NOVALUE;
    int _7154 = NOVALUE;
    int _7151 = NOVALUE;
    int _7147 = NOVALUE;
    int _7144 = NOVALUE;
    int _7142 = NOVALUE;
    int _7139 = NOVALUE;
    int _7136 = NOVALUE;
    int _7131 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_2current_db_10880 != -1)
    goto L1; // [5] 26

    /** 		fatal(NO_DATABASE, "no current database", "db_compress", {})*/
    RefDS(_7128);
    RefDS(_7129);
    RefDS(_5);
    _2fatal(901, _7128, _7129, _5);

    /** 		return -1*/
    DeRef(_new_path_13022);
    DeRef(_table_list_13023);
    DeRef(_record_13024);
    DeRef(_chunk_13025);
    DeRef(_temp_path_13033);
    return -1;
L1: 

    /** 	index = eu:find(current_db, db_file_nums)*/
    _index_13017 = find_from(_2current_db_10880, _2db_file_nums_10884, 1);

    /** 	new_path = text:trim(db_names[index])*/
    _2 = (int)SEQ_PTR(_2db_names_10883);
    _7131 = (int)*(((s1_ptr)_2)->base + _index_13017);
    Ref(_7131);
    RefDS(_4282);
    _0 = _new_path_13022;
    _new_path_13022 = _6trim(_7131, _4282, 0);
    DeRef(_0);
    _7131 = NOVALUE;

    /** 	db_close()*/
    _2db_close();

    /** 	fn = -1*/
    _fn_13021 = -1;

    /** 	sequence temp_path = filesys:temp_file()*/
    RefDS(_5);
    RefDS(_5);
    RefDS(_4363);
    _0 = _temp_path_13033;
    _temp_path_13033 = _8temp_file(_5, _5, _4363, 0);
    DeRef(_0);

    /** 	fn = open( temp_path, "r" )*/
    _fn_13021 = EOpen(_temp_path_13033, _846, 0);

    /** 	if fn != -1 then*/
    if (_fn_13021 == -1)
    goto L2; // [88] 101

    /** 		return DB_EXISTS_ALREADY -- you better delete some temp files*/
    DeRefDS(_new_path_13022);
    DeRef(_table_list_13023);
    DeRef(_record_13024);
    DeRef(_chunk_13025);
    DeRefDS(_temp_path_13033);
    return -2;
L2: 

    /** 	filesys:move_file( new_path, temp_path )*/
    RefDS(_new_path_13022);
    RefDS(_temp_path_13033);
    _7136 = _8move_file(_new_path_13022, _temp_path_13033, 0);

    /** 	index = db_create(new_path, DB_LOCK_NO)*/
    RefDS(_new_path_13022);
    _index_13017 = _2db_create(_new_path_13022, 0, 5, 5);
    if (!IS_ATOM_INT(_index_13017)) {
        _1 = (long)(DBL_PTR(_index_13017)->dbl);
        if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_13017);
        _index_13017 = _1;
    }

    /** 	if index != DB_OK then*/
    if (_index_13017 == 0)
    goto L3; // [128] 147

    /** 		filesys:move_file( temp_path, new_path )*/
    RefDS(_temp_path_13033);
    RefDS(_new_path_13022);
    _7139 = _8move_file(_temp_path_13033, _new_path_13022, 0);

    /** 		return index*/
    DeRefDS(_new_path_13022);
    DeRef(_table_list_13023);
    DeRef(_record_13024);
    DeRef(_chunk_13025);
    DeRefDS(_temp_path_13033);
    DeRef(_7136);
    _7136 = NOVALUE;
    DeRef(_7139);
    _7139 = NOVALUE;
    return _index_13017;
L3: 

    /** 	index = db_open(temp_path, DB_LOCK_NO)*/
    RefDS(_temp_path_13033);
    _index_13017 = _2db_open(_temp_path_13033, 0);
    if (!IS_ATOM_INT(_index_13017)) {
        _1 = (long)(DBL_PTR(_index_13017)->dbl);
        if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_13017);
        _index_13017 = _1;
    }

    /** 	table_list = db_table_list()*/
    _0 = _table_list_13023;
    _table_list_13023 = _2db_table_list();
    DeRef(_0);

    /** 	for i = 1 to length(table_list) do*/
    if (IS_SEQUENCE(_table_list_13023)){
            _7142 = SEQ_PTR(_table_list_13023)->length;
    }
    else {
        _7142 = 1;
    }
    {
        int _i_13046;
        _i_13046 = 1;
L4: 
        if (_i_13046 > _7142){
            goto L5; // [172] 476
        }

        /** 		index = db_select(new_path)*/
        RefDS(_new_path_13022);
        _index_13017 = _2db_select(_new_path_13022, -1);
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 		index = db_create_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_13023);
        _7144 = (int)*(((s1_ptr)_2)->base + _i_13046);
        Ref(_7144);
        _index_13017 = _2db_create_table(_7144, 50);
        _7144 = NOVALUE;
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 		index = db_select(temp_path)*/
        RefDS(_temp_path_13033);
        _index_13017 = _2db_select(_temp_path_13033, -1);
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 		index = db_select_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_13023);
        _7147 = (int)*(((s1_ptr)_2)->base + _i_13046);
        Ref(_7147);
        _index_13017 = _2db_select_table(_7147);
        _7147 = NOVALUE;
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 		nrecs = db_table_size()*/
        RefDS(_2current_table_name_10882);
        _nrecs_13019 = _2db_table_size(_2current_table_name_10882);
        if (!IS_ATOM_INT(_nrecs_13019)) {
            _1 = (long)(DBL_PTR(_nrecs_13019)->dbl);
            if (UNIQUE(DBL_PTR(_nrecs_13019)) && (DBL_PTR(_nrecs_13019)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_nrecs_13019);
            _nrecs_13019 = _1;
        }

        /** 		r = 1*/
        _r_13020 = 1;

        /** 		while r <= nrecs do*/
L6: 
        if (_r_13020 > _nrecs_13019)
        goto L7; // [254] 469

        /** 			chunk_size = nrecs - r + 1*/
        _7151 = _nrecs_13019 - _r_13020;
        if ((long)((unsigned long)_7151 +(unsigned long) HIGH_BITS) >= 0){
            _7151 = NewDouble((double)_7151);
        }
        if (IS_ATOM_INT(_7151)) {
            _chunk_size_13018 = _7151 + 1;
        }
        else
        { // coercing _chunk_size_13018 to an integer 1
            _chunk_size_13018 = 1+(long)(DBL_PTR(_7151)->dbl);
            if( !IS_ATOM_INT(_chunk_size_13018) ){
                _chunk_size_13018 = (object)DBL_PTR(_chunk_size_13018)->dbl;
            }
        }
        DeRef(_7151);
        _7151 = NOVALUE;

        /** 			if chunk_size > 20 then*/
        if (_chunk_size_13018 <= 20)
        goto L8; // [272] 284

        /** 				chunk_size = 20  -- copy up to 20 records at a time*/
        _chunk_size_13018 = 20;
L8: 

        /** 			chunk = {}*/
        RefDS(_5);
        DeRef(_chunk_13025);
        _chunk_13025 = _5;

        /** 			for j = 1 to chunk_size do*/
        _7154 = _chunk_size_13018;
        {
            int _j_13062;
            _j_13062 = 1;
L9: 
            if (_j_13062 > _7154){
                goto LA; // [296] 344
            }

            /** 				record = {db_record_key(r), db_record_data(r)}*/
            RefDS(_2current_table_name_10882);
            _7155 = _2db_record_key(_r_13020, _2current_table_name_10882);
            RefDS(_2current_table_name_10882);
            _7156 = _2db_record_data(_r_13020, _2current_table_name_10882);
            DeRef(_record_13024);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _7155;
            ((int *)_2)[2] = _7156;
            _record_13024 = MAKE_SEQ(_1);
            _7156 = NOVALUE;
            _7155 = NOVALUE;

            /** 				r += 1*/
            _r_13020 = _r_13020 + 1;

            /** 				chunk = append(chunk, record)*/
            RefDS(_record_13024);
            Append(&_chunk_13025, _chunk_13025, _record_13024);

            /** 			end for*/
            _j_13062 = _j_13062 + 1;
            goto L9; // [339] 303
LA: 
            ;
        }

        /** 			index = db_select(new_path)*/
        RefDS(_new_path_13022);
        _index_13017 = _2db_select(_new_path_13022, -1);
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 			index = db_select_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_13023);
        _7161 = (int)*(((s1_ptr)_2)->base + _i_13046);
        Ref(_7161);
        _index_13017 = _2db_select_table(_7161);
        _7161 = NOVALUE;
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 			for j = 1 to chunk_size do*/
        _7163 = _chunk_size_13018;
        {
            int _j_13073;
            _j_13073 = 1;
LB: 
            if (_j_13073 > _7163){
                goto LC; // [374] 439
            }

            /** 				if db_insert(chunk[j][1], chunk[j][2]) != DB_OK then*/
            _2 = (int)SEQ_PTR(_chunk_13025);
            _7164 = (int)*(((s1_ptr)_2)->base + _j_13073);
            _2 = (int)SEQ_PTR(_7164);
            _7165 = (int)*(((s1_ptr)_2)->base + 1);
            _7164 = NOVALUE;
            _2 = (int)SEQ_PTR(_chunk_13025);
            _7166 = (int)*(((s1_ptr)_2)->base + _j_13073);
            _2 = (int)SEQ_PTR(_7166);
            _7167 = (int)*(((s1_ptr)_2)->base + 2);
            _7166 = NOVALUE;
            Ref(_7165);
            Ref(_7167);
            RefDS(_2current_table_name_10882);
            _7168 = _2db_insert(_7165, _7167, _2current_table_name_10882);
            _7165 = NOVALUE;
            _7167 = NOVALUE;
            if (binary_op_a(EQUALS, _7168, 0)){
                DeRef(_7168);
                _7168 = NOVALUE;
                goto LD; // [409] 432
            }
            DeRef(_7168);
            _7168 = NOVALUE;

            /** 					fatal(INSERT_FAILED, "couldn't insert into new database", "db_compress", {})*/
            RefDS(_7170);
            RefDS(_7129);
            RefDS(_5);
            _2fatal(906, _7170, _7129, _5);

            /** 					return DB_FATAL_FAIL*/
            DeRef(_new_path_13022);
            DeRef(_table_list_13023);
            DeRef(_record_13024);
            DeRefDS(_chunk_13025);
            DeRef(_temp_path_13033);
            DeRef(_7136);
            _7136 = NOVALUE;
            DeRef(_7139);
            _7139 = NOVALUE;
            return -404;
LD: 

            /** 			end for*/
            _j_13073 = _j_13073 + 1;
            goto LB; // [434] 381
LC: 
            ;
        }

        /** 			index = db_select(temp_path)*/
        RefDS(_temp_path_13033);
        _index_13017 = _2db_select(_temp_path_13033, -1);
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 			index = db_select_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_13023);
        _7172 = (int)*(((s1_ptr)_2)->base + _i_13046);
        Ref(_7172);
        _index_13017 = _2db_select_table(_7172);
        _7172 = NOVALUE;
        if (!IS_ATOM_INT(_index_13017)) {
            _1 = (long)(DBL_PTR(_index_13017)->dbl);
            if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_13017);
            _index_13017 = _1;
        }

        /** 		end while*/
        goto L6; // [466] 254
L7: 

        /** 	end for*/
        _i_13046 = _i_13046 + 1;
        goto L4; // [471] 179
L5: 
        ;
    }

    /** 	db_close()*/
    _2db_close();

    /** 	index = db_select(new_path)*/
    RefDS(_new_path_13022);
    _index_13017 = _2db_select(_new_path_13022, -1);
    if (!IS_ATOM_INT(_index_13017)) {
        _1 = (long)(DBL_PTR(_index_13017)->dbl);
        if (UNIQUE(DBL_PTR(_index_13017)) && (DBL_PTR(_index_13017)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_13017);
        _index_13017 = _1;
    }

    /** 	return DB_OK*/
    DeRefDS(_new_path_13022);
    DeRef(_table_list_13023);
    DeRef(_record_13024);
    DeRef(_chunk_13025);
    DeRef(_temp_path_13033);
    DeRef(_7136);
    _7136 = NOVALUE;
    DeRef(_7139);
    _7139 = NOVALUE;
    return 0;
    ;
}


int _2db_current()
{
    int _index_13089 = NOVALUE;
    int _7177 = NOVALUE;
    int _0, _1, _2;
    

    /** 	index = find (current_db, db_file_nums)*/
    _index_13089 = find_from(_2current_db_10880, _2db_file_nums_10884, 1);

    /** 	if index != 0 then*/
    if (_index_13089 == 0)
    goto L1; // [16] 35

    /** 		return db_names [index]*/
    _2 = (int)SEQ_PTR(_2db_names_10883);
    _7177 = (int)*(((s1_ptr)_2)->base + _index_13089);
    Ref(_7177);
    return _7177;
    goto L2; // [32] 42
L1: 

    /** 		return ""*/
    RefDS(_5);
    _7177 = NOVALUE;
    return _5;
L2: 
    ;
}


void _2db_cache_clear()
{
    int _0, _1, _2;
    

    /** 	cache_index = {}*/
    RefDS(_5);
    DeRef(_2cache_index_10889);
    _2cache_index_10889 = _5;

    /** 	key_cache = {}*/
    RefDS(_5);
    DeRef(_2key_cache_10888);
    _2key_cache_10888 = _5;

    /** end procedure*/
    return;
    ;
}


int _2db_set_caching(int _new_setting_13099)
{
    int _lOldVal_13100 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lOldVal = caching_option*/
    _lOldVal_13100 = _2caching_option_10890;

    /** 	caching_option = (new_setting != 0)*/
    if (IS_ATOM_INT(_new_setting_13099)) {
        _2caching_option_10890 = (_new_setting_13099 != 0);
    }
    else {
        _2caching_option_10890 = (DBL_PTR(_new_setting_13099)->dbl != (double)0);
    }

    /** 	if caching_option = 0 then*/
    if (_2caching_option_10890 != 0)
    goto L1; // [24] 50

    /** 		db_cache_clear()*/

    /** 	cache_index = {}*/
    RefDS(_5);
    DeRef(_2cache_index_10889);
    _2cache_index_10889 = _5;

    /** 	key_cache = {}*/
    RefDS(_5);
    DeRef(_2key_cache_10888);
    _2key_cache_10888 = _5;

    /** end procedure*/
    goto L2; // [46] 49
L2: 
L1: 

    /** 	return lOldVal*/
    DeRef(_new_setting_13099);
    return _lOldVal_13100;
    ;
}


void _2db_replace_recid(int _recid_13107, int _data_13108)
{
    int _old_size_13109 = NOVALUE;
    int _new_size_13110 = NOVALUE;
    int _data_ptr_13111 = NOVALUE;
    int _data_string_13112 = NOVALUE;
    int _put4_1__tmp_at113_13132 = NOVALUE;
    int _7224 = NOVALUE;
    int _7223 = NOVALUE;
    int _7222 = NOVALUE;
    int _7221 = NOVALUE;
    int _7220 = NOVALUE;
    int _7192 = NOVALUE;
    int _7190 = NOVALUE;
    int _7189 = NOVALUE;
    int _7188 = NOVALUE;
    int _7187 = NOVALUE;
    int _7186 = NOVALUE;
    int _7182 = NOVALUE;
    int _7181 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recid_13107)) {
        _1 = (long)(DBL_PTR(_recid_13107)->dbl);
        if (UNIQUE(DBL_PTR(_recid_13107)) && (DBL_PTR(_recid_13107)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recid_13107);
        _recid_13107 = _1;
    }

    /** 	seek(current_db, recid)*/
    _7224 = _15seek(_2current_db_10880, _recid_13107);
    DeRef(_7224);
    _7224 = NOVALUE;

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_13111;
    _data_ptr_13111 = _2get4();
    DeRef(_0);

    /** 	seek(current_db, data_ptr-4)*/
    if (IS_ATOM_INT(_data_ptr_13111)) {
        _7181 = _data_ptr_13111 - 4;
        if ((long)((unsigned long)_7181 +(unsigned long) HIGH_BITS) >= 0){
            _7181 = NewDouble((double)_7181);
        }
    }
    else {
        _7181 = NewDouble(DBL_PTR(_data_ptr_13111)->dbl - (double)4);
    }
    _7223 = _15seek(_2current_db_10880, _7181);
    _7181 = NOVALUE;
    DeRef(_7223);
    _7223 = NOVALUE;

    /** 	old_size = get4()-4*/
    _7182 = _2get4();
    DeRef(_old_size_13109);
    if (IS_ATOM_INT(_7182)) {
        _old_size_13109 = _7182 - 4;
        if ((long)((unsigned long)_old_size_13109 +(unsigned long) HIGH_BITS) >= 0){
            _old_size_13109 = NewDouble((double)_old_size_13109);
        }
    }
    else {
        _old_size_13109 = binary_op(MINUS, _7182, 4);
    }
    DeRef(_7182);
    _7182 = NOVALUE;

    /** 	data_string = compress(data)*/
    Ref(_data_13108);
    _0 = _data_string_13112;
    _data_string_13112 = _2compress(_data_13108);
    DeRef(_0);

    /** 	new_size = length(data_string)*/
    if (IS_SEQUENCE(_data_string_13112)){
            _new_size_13110 = SEQ_PTR(_data_string_13112)->length;
    }
    else {
        _new_size_13110 = 1;
    }

    /** 	if new_size <= old_size and*/
    if (IS_ATOM_INT(_old_size_13109)) {
        _7186 = (_new_size_13110 <= _old_size_13109);
    }
    else {
        _7186 = ((double)_new_size_13110 <= DBL_PTR(_old_size_13109)->dbl);
    }
    if (_7186 == 0) {
        goto L1; // [64] 94
    }
    if (IS_ATOM_INT(_old_size_13109)) {
        _7188 = _old_size_13109 - 16;
        if ((long)((unsigned long)_7188 +(unsigned long) HIGH_BITS) >= 0){
            _7188 = NewDouble((double)_7188);
        }
    }
    else {
        _7188 = NewDouble(DBL_PTR(_old_size_13109)->dbl - (double)16);
    }
    if (IS_ATOM_INT(_7188)) {
        _7189 = (_new_size_13110 >= _7188);
    }
    else {
        _7189 = ((double)_new_size_13110 >= DBL_PTR(_7188)->dbl);
    }
    DeRef(_7188);
    _7188 = NOVALUE;
    if (_7189 == 0)
    {
        DeRef(_7189);
        _7189 = NOVALUE;
        goto L1; // [77] 94
    }
    else{
        DeRef(_7189);
        _7189 = NOVALUE;
    }

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_13111);
    _7222 = _15seek(_2current_db_10880, _data_ptr_13111);
    DeRef(_7222);
    _7222 = NOVALUE;
    goto L2; // [91] 170
L1: 

    /** 		db_free(data_ptr)*/
    Ref(_data_ptr_13111);
    _2db_free(_data_ptr_13111);

    /** 		data_ptr = db_allocate(new_size + 8)*/
    _7190 = _new_size_13110 + 8;
    if ((long)((unsigned long)_7190 + (unsigned long)HIGH_BITS) >= 0) 
    _7190 = NewDouble((double)_7190);
    _0 = _data_ptr_13111;
    _data_ptr_13111 = _2db_allocate(_7190);
    DeRef(_0);
    _7190 = NOVALUE;

    /** 		seek(current_db, recid)*/
    _7221 = _15seek(_2current_db_10880, _recid_13107);
    DeRef(_7221);
    _7221 = NOVALUE;

    /** 		put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_2mem0_10922)){
        poke4_addr = (unsigned long *)_2mem0_10922;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_2mem0_10922)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_13111)) {
        *poke4_addr = (unsigned long)_data_ptr_13111;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_13111)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at113_13132);
    _1 = (int)SEQ_PTR(_2memseq_11142);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at113_13132 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_2current_db_10880, _put4_1__tmp_at113_13132); // DJP 

    /** end procedure*/
    goto L3; // [143] 146
L3: 
    DeRefi(_put4_1__tmp_at113_13132);
    _put4_1__tmp_at113_13132 = NOVALUE;

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_13111);
    _7220 = _15seek(_2current_db_10880, _data_ptr_13111);
    DeRef(_7220);
    _7220 = NOVALUE;

    /** 		data_string &= repeat( 0, 8 )*/
    _7192 = Repeat(0, 8);
    Concat((object_ptr)&_data_string_13112, _data_string_13112, _7192);
    DeRefDS(_7192);
    _7192 = NOVALUE;
L2: 

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_2current_db_10880, _data_string_13112); // DJP 

    /** end procedure*/
    goto L4; // [181] 184
L4: 

    /** end procedure*/
    DeRef(_data_13108);
    DeRef(_old_size_13109);
    DeRef(_data_ptr_13111);
    DeRef(_data_string_13112);
    DeRef(_7186);
    _7186 = NOVALUE;
    return;
    ;
}


int _2db_record_recid(int _recid_13139)
{
    int _data_ptr_13140 = NOVALUE;
    int _data_value_13141 = NOVALUE;
    int _key_value_13142 = NOVALUE;
    int _7219 = NOVALUE;
    int _7218 = NOVALUE;
    int _7197 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recid_13139)) {
        _1 = (long)(DBL_PTR(_recid_13139)->dbl);
        if (UNIQUE(DBL_PTR(_recid_13139)) && (DBL_PTR(_recid_13139)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recid_13139);
        _recid_13139 = _1;
    }

    /** 	seek(current_db, recid)*/
    _7219 = _15seek(_2current_db_10880, _recid_13139);
    DeRef(_7219);
    _7219 = NOVALUE;

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_13140;
    _data_ptr_13140 = _2get4();
    DeRef(_0);

    /** 	key_value = decompress(0)*/
    _0 = _key_value_13142;
    _key_value_13142 = _2decompress(0);
    DeRef(_0);

    /** 	seek(current_db, data_ptr)*/
    Ref(_data_ptr_13140);
    _7218 = _15seek(_2current_db_10880, _data_ptr_13140);
    DeRef(_7218);
    _7218 = NOVALUE;

    /** 	data_value = decompress(0)*/
    _0 = _data_value_13141;
    _data_value_13141 = _2decompress(0);
    DeRef(_0);

    /** 	return {key_value, data_value}*/
    Ref(_data_value_13141);
    Ref(_key_value_13142);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_value_13142;
    ((int *)_2)[2] = _data_value_13141;
    _7197 = MAKE_SEQ(_1);
    DeRef(_data_ptr_13140);
    DeRef(_data_value_13141);
    DeRef(_key_value_13142);
    return _7197;
    ;
}


int _2db_get_recid(int _key_13151, int _table_name_13152)
{
    int _lo_13153 = NOVALUE;
    int _hi_13154 = NOVALUE;
    int _mid_13155 = NOVALUE;
    int _c_13156 = NOVALUE;
    int _7217 = NOVALUE;
    int _7211 = NOVALUE;
    int _7210 = NOVALUE;
    int _7208 = NOVALUE;
    int _7205 = NOVALUE;
    int _7203 = NOVALUE;
    int _7200 = NOVALUE;
    int _7198 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_13152 == _2current_table_name_10882)
    _7198 = 1;
    else if (IS_ATOM_INT(_table_name_13152) && IS_ATOM_INT(_2current_table_name_10882))
    _7198 = 0;
    else
    _7198 = (compare(_table_name_13152, _2current_table_name_10882) == 0);
    if (_7198 != 0)
    goto L1; // [9] 46
    _7198 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_13152);
    _7200 = _2db_select_table(_table_name_13152);
    if (binary_op_a(EQUALS, _7200, 0)){
        DeRef(_7200);
        _7200 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_7200);
    _7200 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_get_recid", {key, table_name})*/
    Ref(_table_name_13152);
    Ref(_key_13151);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_13151;
    ((int *)_2)[2] = _table_name_13152;
    _7203 = MAKE_SEQ(_1);
    RefDS(_6895);
    RefDS(_7202);
    _2fatal(903, _6895, _7202, _7203);
    _7203 = NOVALUE;

    /** 			return 0*/
    DeRef(_key_13151);
    DeRef(_table_name_13152);
    return 0;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _2current_table_pos_10881, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_get_recid", {key, table_name})*/
    Ref(_table_name_13152);
    Ref(_key_13151);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_13151;
    ((int *)_2)[2] = _table_name_13152;
    _7205 = MAKE_SEQ(_1);
    RefDS(_6899);
    RefDS(_7202);
    _2fatal(903, _6899, _7202, _7205);
    _7205 = NOVALUE;

    /** 		return 0*/
    DeRef(_key_13151);
    DeRef(_table_name_13152);
    return 0;
L3: 

    /** 	lo = 1*/
    _lo_13153 = 1;

    /** 	hi = length(key_pointers)*/
    if (IS_SEQUENCE(_2key_pointers_10887)){
            _hi_13154 = SEQ_PTR(_2key_pointers_10887)->length;
    }
    else {
        _hi_13154 = 1;
    }

    /** 	mid = 1*/
    _mid_13155 = 1;

    /** 	c = 0*/
    _c_13156 = 0;

    /** 	while lo <= hi do*/
L4: 
    if (_lo_13153 > _hi_13154)
    goto L5; // [110] 198

    /** 		mid = floor((lo + hi) / 2)*/
    _7208 = _lo_13153 + _hi_13154;
    if ((long)((unsigned long)_7208 + (unsigned long)HIGH_BITS) >= 0) 
    _7208 = NewDouble((double)_7208);
    if (IS_ATOM_INT(_7208)) {
        _mid_13155 = _7208 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _7208, 2);
        _mid_13155 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_7208);
    _7208 = NOVALUE;
    if (!IS_ATOM_INT(_mid_13155)) {
        _1 = (long)(DBL_PTR(_mid_13155)->dbl);
        if (UNIQUE(DBL_PTR(_mid_13155)) && (DBL_PTR(_mid_13155)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_13155);
        _mid_13155 = _1;
    }

    /** 		c = eu:compare(key, key_value(key_pointers[mid]))*/
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    _7210 = (int)*(((s1_ptr)_2)->base + _mid_13155);
    Ref(_7210);
    _7211 = _2key_value(_7210);
    _7210 = NOVALUE;
    if (IS_ATOM_INT(_key_13151) && IS_ATOM_INT(_7211)){
        _c_13156 = (_key_13151 < _7211) ? -1 : (_key_13151 > _7211);
    }
    else{
        _c_13156 = compare(_key_13151, _7211);
    }
    DeRef(_7211);
    _7211 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_13156 >= 0)
    goto L6; // [148] 163

    /** 			hi = mid - 1*/
    _hi_13154 = _mid_13155 - 1;
    goto L4; // [160] 110
L6: 

    /** 		elsif c > 0 then*/
    if (_c_13156 <= 0)
    goto L7; // [165] 180

    /** 			lo = mid + 1*/
    _lo_13153 = _mid_13155 + 1;
    goto L4; // [177] 110
L7: 

    /** 			return key_pointers[mid]*/
    _2 = (int)SEQ_PTR(_2key_pointers_10887);
    _7217 = (int)*(((s1_ptr)_2)->base + _mid_13155);
    Ref(_7217);
    DeRef(_key_13151);
    DeRef(_table_name_13152);
    return _7217;

    /** 	end while*/
    goto L4; // [195] 110
L5: 

    /** 	return -1*/
    DeRef(_key_13151);
    DeRef(_table_name_13152);
    _7217 = NOVALUE;
    return -1;
    ;
}



// 0x98A4FFBD
