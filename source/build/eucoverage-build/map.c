// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _27map(int _obj_p_14116)
{
    int _m__14120 = NOVALUE;
    int _7771 = NOVALUE;
    int _7770 = NOVALUE;
    int _7769 = NOVALUE;
    int _7768 = NOVALUE;
    int _7767 = NOVALUE;
    int _7766 = NOVALUE;
    int _7765 = NOVALUE;
    int _7764 = NOVALUE;
    int _7763 = NOVALUE;
    int _7762 = NOVALUE;
    int _7760 = NOVALUE;
    int _7759 = NOVALUE;
    int _7758 = NOVALUE;
    int _7757 = NOVALUE;
    int _7755 = NOVALUE;
    int _7754 = NOVALUE;
    int _7753 = NOVALUE;
    int _7752 = NOVALUE;
    int _7750 = NOVALUE;
    int _7749 = NOVALUE;
    int _7748 = NOVALUE;
    int _7747 = NOVALUE;
    int _7746 = NOVALUE;
    int _7745 = NOVALUE;
    int _7744 = NOVALUE;
    int _7743 = NOVALUE;
    int _7742 = NOVALUE;
    int _7741 = NOVALUE;
    int _7739 = NOVALUE;
    int _7737 = NOVALUE;
    int _7736 = NOVALUE;
    int _7734 = NOVALUE;
    int _7732 = NOVALUE;
    int _7731 = NOVALUE;
    int _7729 = NOVALUE;
    int _7728 = NOVALUE;
    int _7726 = NOVALUE;
    int _7724 = NOVALUE;
    int _7722 = NOVALUE;
    int _7719 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not eumem:valid(obj_p, "") then return 0 end if*/
    Ref(_obj_p_14116);
    RefDS(_5);
    _7719 = _28valid(_obj_p_14116, _5);
    if (IS_ATOM_INT(_7719)) {
        if (_7719 != 0){
            DeRef(_7719);
            _7719 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    else {
        if (DBL_PTR(_7719)->dbl != 0.0){
            DeRef(_7719);
            _7719 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    DeRef(_7719);
    _7719 = NOVALUE;
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L1: 

    /** 	object m_*/

    /** 	m_ = eumem:ram_space[obj_p]*/
    DeRef(_m__14120);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_obj_p_14116)){
        _m__14120 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_obj_p_14116)->dbl));
    }
    else{
        _m__14120 = (int)*(((s1_ptr)_2)->base + _obj_p_14116);
    }
    Ref(_m__14120);

    /** 	if not sequence(m_) then return 0 end if*/
    _7722 = IS_SEQUENCE(_m__14120);
    if (_7722 != 0)
    goto L2; // [31] 39
    _7722 = NOVALUE;
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L2: 

    /** 	if length(m_) < 6 then return 0 end if*/
    if (IS_SEQUENCE(_m__14120)){
            _7724 = SEQ_PTR(_m__14120)->length;
    }
    else {
        _7724 = 1;
    }
    if (_7724 >= 6)
    goto L3; // [44] 53
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L3: 

    /** 	if length(m_) > 7 then return 0 end if*/
    if (IS_SEQUENCE(_m__14120)){
            _7726 = SEQ_PTR(_m__14120)->length;
    }
    else {
        _7726 = 1;
    }
    if (_7726 <= 7)
    goto L4; // [58] 67
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L4: 

    /** 	if not equal(m_[TYPE_TAG], type_is_map) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7728 = (int)*(((s1_ptr)_2)->base + 1);
    if (_7728 == _27type_is_map_14088)
    _7729 = 1;
    else if (IS_ATOM_INT(_7728) && IS_ATOM_INT(_27type_is_map_14088))
    _7729 = 0;
    else
    _7729 = (compare(_7728, _27type_is_map_14088) == 0);
    _7728 = NOVALUE;
    if (_7729 != 0)
    goto L5; // [79] 87
    _7729 = NOVALUE;
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L5: 

    /** 	if not integer(m_[ELEMENT_COUNT]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7731 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7731))
    _7732 = 1;
    else if (IS_ATOM_DBL(_7731))
    _7732 = IS_ATOM_INT(DoubleToInt(_7731));
    else
    _7732 = 0;
    _7731 = NOVALUE;
    if (_7732 != 0)
    goto L6; // [98] 106
    _7732 = NOVALUE;
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L6: 

    /** 	if m_[ELEMENT_COUNT] < 0 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7734 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(GREATEREQ, _7734, 0)){
        _7734 = NOVALUE;
        goto L7; // [114] 123
    }
    _7734 = NOVALUE;
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L7: 

    /** 	if not integer(m_[IN_USE]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7736 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7736))
    _7737 = 1;
    else if (IS_ATOM_DBL(_7736))
    _7737 = IS_ATOM_INT(DoubleToInt(_7736));
    else
    _7737 = 0;
    _7736 = NOVALUE;
    if (_7737 != 0)
    goto L8; // [134] 142
    _7737 = NOVALUE;
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L8: 

    /** 	if m_[IN_USE] < 0		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7739 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(GREATEREQ, _7739, 0)){
        _7739 = NOVALUE;
        goto L9; // [150] 159
    }
    _7739 = NOVALUE;
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
L9: 

    /** 	if equal(m_[MAP_TYPE],SMALLMAP) then*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7741 = (int)*(((s1_ptr)_2)->base + 4);
    if (_7741 == 115)
    _7742 = 1;
    else if (IS_ATOM_INT(_7741) && IS_ATOM_INT(115))
    _7742 = 0;
    else
    _7742 = (compare(_7741, 115) == 0);
    _7741 = NOVALUE;
    if (_7742 == 0)
    {
        _7742 = NOVALUE;
        goto LA; // [171] 312
    }
    else{
        _7742 = NOVALUE;
    }

    /** 		if atom(m_[KEY_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7743 = (int)*(((s1_ptr)_2)->base + 5);
    _7744 = IS_ATOM(_7743);
    _7743 = NOVALUE;
    if (_7744 == 0)
    {
        _7744 = NOVALUE;
        goto LB; // [185] 193
    }
    else{
        _7744 = NOVALUE;
    }
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
LB: 

    /** 		if atom(m_[VALUE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7745 = (int)*(((s1_ptr)_2)->base + 6);
    _7746 = IS_ATOM(_7745);
    _7745 = NOVALUE;
    if (_7746 == 0)
    {
        _7746 = NOVALUE;
        goto LC; // [204] 212
    }
    else{
        _7746 = NOVALUE;
    }
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
LC: 

    /** 		if atom(m_[FREE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7747 = (int)*(((s1_ptr)_2)->base + 7);
    _7748 = IS_ATOM(_7747);
    _7747 = NOVALUE;
    if (_7748 == 0)
    {
        _7748 = NOVALUE;
        goto LD; // [223] 231
    }
    else{
        _7748 = NOVALUE;
    }
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    return 0;
LD: 

    /** 		if length(m_[KEY_LIST]) = 0  then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7749 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7749)){
            _7750 = SEQ_PTR(_7749)->length;
    }
    else {
        _7750 = 1;
    }
    _7749 = NOVALUE;
    if (_7750 != 0)
    goto LE; // [242] 251
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    return 0;
LE: 

    /** 		if length(m_[KEY_LIST]) != length(m_[VALUE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7752 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7752)){
            _7753 = SEQ_PTR(_7752)->length;
    }
    else {
        _7753 = 1;
    }
    _7752 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__14120);
    _7754 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7754)){
            _7755 = SEQ_PTR(_7754)->length;
    }
    else {
        _7755 = 1;
    }
    _7754 = NOVALUE;
    if (_7753 == _7755)
    goto LF; // [271] 280
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    _7752 = NOVALUE;
    _7754 = NOVALUE;
    return 0;
LF: 

    /** 		if length(m_[KEY_LIST]) != length(m_[FREE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7757 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7757)){
            _7758 = SEQ_PTR(_7757)->length;
    }
    else {
        _7758 = 1;
    }
    _7757 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__14120);
    _7759 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7759)){
            _7760 = SEQ_PTR(_7759)->length;
    }
    else {
        _7760 = 1;
    }
    _7759 = NOVALUE;
    if (_7758 == _7760)
    goto L10; // [300] 404
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    _7752 = NOVALUE;
    _7754 = NOVALUE;
    _7757 = NOVALUE;
    _7759 = NOVALUE;
    return 0;
    goto L10; // [309] 404
LA: 

    /** 	elsif  equal(m_[MAP_TYPE],LARGEMAP) then*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7762 = (int)*(((s1_ptr)_2)->base + 4);
    if (_7762 == 76)
    _7763 = 1;
    else if (IS_ATOM_INT(_7762) && IS_ATOM_INT(76))
    _7763 = 0;
    else
    _7763 = (compare(_7762, 76) == 0);
    _7762 = NOVALUE;
    if (_7763 == 0)
    {
        _7763 = NOVALUE;
        goto L11; // [324] 397
    }
    else{
        _7763 = NOVALUE;
    }

    /** 		if atom(m_[KEY_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7764 = (int)*(((s1_ptr)_2)->base + 5);
    _7765 = IS_ATOM(_7764);
    _7764 = NOVALUE;
    if (_7765 == 0)
    {
        _7765 = NOVALUE;
        goto L12; // [338] 346
    }
    else{
        _7765 = NOVALUE;
    }
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    _7752 = NOVALUE;
    _7754 = NOVALUE;
    _7757 = NOVALUE;
    _7759 = NOVALUE;
    return 0;
L12: 

    /** 		if atom(m_[VALUE_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7766 = (int)*(((s1_ptr)_2)->base + 6);
    _7767 = IS_ATOM(_7766);
    _7766 = NOVALUE;
    if (_7767 == 0)
    {
        _7767 = NOVALUE;
        goto L13; // [357] 365
    }
    else{
        _7767 = NOVALUE;
    }
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    _7752 = NOVALUE;
    _7754 = NOVALUE;
    _7757 = NOVALUE;
    _7759 = NOVALUE;
    return 0;
L13: 

    /** 		if length(m_[KEY_BUCKETS]) != length(m_[VALUE_BUCKETS])	then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__14120);
    _7768 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7768)){
            _7769 = SEQ_PTR(_7768)->length;
    }
    else {
        _7769 = 1;
    }
    _7768 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__14120);
    _7770 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7770)){
            _7771 = SEQ_PTR(_7770)->length;
    }
    else {
        _7771 = 1;
    }
    _7770 = NOVALUE;
    if (_7769 == _7771)
    goto L10; // [385] 404
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    _7752 = NOVALUE;
    _7754 = NOVALUE;
    _7757 = NOVALUE;
    _7759 = NOVALUE;
    _7768 = NOVALUE;
    _7770 = NOVALUE;
    return 0;
    goto L10; // [394] 404
L11: 

    /** 		return 0*/
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    _7752 = NOVALUE;
    _7754 = NOVALUE;
    _7757 = NOVALUE;
    _7759 = NOVALUE;
    _7768 = NOVALUE;
    _7770 = NOVALUE;
    return 0;
L10: 

    /** 	return 1*/
    DeRef(_obj_p_14116);
    DeRef(_m__14120);
    _7749 = NOVALUE;
    _7752 = NOVALUE;
    _7754 = NOVALUE;
    _7757 = NOVALUE;
    _7759 = NOVALUE;
    _7768 = NOVALUE;
    _7770 = NOVALUE;
    return 1;
    ;
}


int _27calc_hash(int _key_p_14197, int _max_hash_p_14198)
{
    int _ret__14199 = NOVALUE;
    int _7776 = NOVALUE;
    int _7775 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_hash_p_14198)) {
        _1 = (long)(DBL_PTR(_max_hash_p_14198)->dbl);
        if (UNIQUE(DBL_PTR(_max_hash_p_14198)) && (DBL_PTR(_max_hash_p_14198)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_hash_p_14198);
        _max_hash_p_14198 = _1;
    }

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    _ret__14199 = calc_hash(_key_p_14197, -6);
    if (!IS_ATOM_INT(_ret__14199)) {
        _1 = (long)(DBL_PTR(_ret__14199)->dbl);
        if (UNIQUE(DBL_PTR(_ret__14199)) && (DBL_PTR(_ret__14199)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__14199);
        _ret__14199 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _7775 = (_ret__14199 % _max_hash_p_14198);
    _7776 = _7775 + 1;
    if (_7776 > MAXINT){
        _7776 = NewDouble((double)_7776);
    }
    _7775 = NOVALUE;
    DeRef(_key_p_14197);
    return _7776;
    ;
}


int _27threshold(int _new_value_p_14205)
{
    int _old_value__14208 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_new_value_p_14205)) {
        _1 = (long)(DBL_PTR(_new_value_p_14205)->dbl);
        if (UNIQUE(DBL_PTR(_new_value_p_14205)) && (DBL_PTR(_new_value_p_14205)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_value_p_14205);
        _new_value_p_14205 = _1;
    }

    /** 	if new_value_p < 1 then*/
    if (_new_value_p_14205 >= 1)
    goto L1; // [7] 20

    /** 		return threshold_size*/
    return _27threshold_size_14110;
L1: 

    /** 	integer old_value_ = threshold_size*/
    _old_value__14208 = _27threshold_size_14110;

    /** 	threshold_size = new_value_p*/
    _27threshold_size_14110 = _new_value_p_14205;

    /** 	return old_value_*/
    return _old_value__14208;
    ;
}


int _27type_of(int _the_map_p_14211)
{
    int _7779 = NOVALUE;
    int _7778 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][MAP_TYPE]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_14211)){
        _7778 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14211)->dbl));
    }
    else{
        _7778 = (int)*(((s1_ptr)_2)->base + _the_map_p_14211);
    }
    _2 = (int)SEQ_PTR(_7778);
    _7779 = (int)*(((s1_ptr)_2)->base + 4);
    _7778 = NOVALUE;
    Ref(_7779);
    DeRef(_the_map_p_14211);
    return _7779;
    ;
}


void _27rehash(int _the_map_p_14216, int _requested_bucket_size_p_14217)
{
    int _size__14218 = NOVALUE;
    int _index_2__14219 = NOVALUE;
    int _old_key_buckets__14220 = NOVALUE;
    int _old_val_buckets__14221 = NOVALUE;
    int _new_key_buckets__14222 = NOVALUE;
    int _new_val_buckets__14223 = NOVALUE;
    int _key__14224 = NOVALUE;
    int _value__14225 = NOVALUE;
    int _pos_14226 = NOVALUE;
    int _new_keys_14227 = NOVALUE;
    int _in_use_14228 = NOVALUE;
    int _elem_count_14229 = NOVALUE;
    int _7840 = NOVALUE;
    int _7839 = NOVALUE;
    int _7838 = NOVALUE;
    int _7837 = NOVALUE;
    int _7836 = NOVALUE;
    int _7835 = NOVALUE;
    int _7834 = NOVALUE;
    int _7833 = NOVALUE;
    int _7832 = NOVALUE;
    int _7830 = NOVALUE;
    int _7829 = NOVALUE;
    int _7828 = NOVALUE;
    int _7825 = NOVALUE;
    int _7824 = NOVALUE;
    int _7822 = NOVALUE;
    int _7821 = NOVALUE;
    int _7820 = NOVALUE;
    int _7819 = NOVALUE;
    int _7817 = NOVALUE;
    int _7815 = NOVALUE;
    int _7813 = NOVALUE;
    int _7809 = NOVALUE;
    int _7807 = NOVALUE;
    int _7806 = NOVALUE;
    int _7805 = NOVALUE;
    int _7804 = NOVALUE;
    int _7802 = NOVALUE;
    int _7800 = NOVALUE;
    int _7798 = NOVALUE;
    int _7797 = NOVALUE;
    int _7795 = NOVALUE;
    int _7793 = NOVALUE;
    int _7790 = NOVALUE;
    int _7788 = NOVALUE;
    int _7787 = NOVALUE;
    int _7786 = NOVALUE;
    int _7785 = NOVALUE;
    int _7784 = NOVALUE;
    int _7781 = NOVALUE;
    int _7780 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_14216)) {
        _1 = (long)(DBL_PTR(_the_map_p_14216)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_14216)) && (DBL_PTR(_the_map_p_14216)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_14216);
        _the_map_p_14216 = _1;
    }
    if (!IS_ATOM_INT(_requested_bucket_size_p_14217)) {
        _1 = (long)(DBL_PTR(_requested_bucket_size_p_14217)->dbl);
        if (UNIQUE(DBL_PTR(_requested_bucket_size_p_14217)) && (DBL_PTR(_requested_bucket_size_p_14217)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_requested_bucket_size_p_14217);
        _requested_bucket_size_p_14217 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = SMALLMAP then*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7780 = (int)*(((s1_ptr)_2)->base + _the_map_p_14216);
    _2 = (int)SEQ_PTR(_7780);
    _7781 = (int)*(((s1_ptr)_2)->base + 4);
    _7780 = NOVALUE;
    if (binary_op_a(NOTEQ, _7781, 115)){
        _7781 = NOVALUE;
        goto L1; // [23] 33
    }
    _7781 = NOVALUE;

    /** 		return -- small maps are not hashed.*/
    DeRef(_old_key_buckets__14220);
    DeRef(_old_val_buckets__14221);
    DeRef(_new_key_buckets__14222);
    DeRef(_new_val_buckets__14223);
    DeRef(_key__14224);
    DeRef(_value__14225);
    DeRef(_new_keys_14227);
    return;
L1: 

    /** 	if requested_bucket_size_p <= 0 then*/
    if (_requested_bucket_size_p_14217 > 0)
    goto L2; // [35] 72

    /** 		size_ = floor(length(eumem:ram_space[the_map_p][KEY_BUCKETS]) * 3.5) + 1*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7784 = (int)*(((s1_ptr)_2)->base + _the_map_p_14216);
    _2 = (int)SEQ_PTR(_7784);
    _7785 = (int)*(((s1_ptr)_2)->base + 5);
    _7784 = NOVALUE;
    if (IS_SEQUENCE(_7785)){
            _7786 = SEQ_PTR(_7785)->length;
    }
    else {
        _7786 = 1;
    }
    _7785 = NOVALUE;
    _7787 = NewDouble((double)_7786 * DBL_PTR(_7271)->dbl);
    _7786 = NOVALUE;
    _7788 = unary_op(FLOOR, _7787);
    DeRefDS(_7787);
    _7787 = NOVALUE;
    if (IS_ATOM_INT(_7788)) {
        _size__14218 = _7788 + 1;
    }
    else
    { // coercing _size__14218 to an integer 1
        _size__14218 = 1+(long)(DBL_PTR(_7788)->dbl);
        if( !IS_ATOM_INT(_size__14218) ){
            _size__14218 = (object)DBL_PTR(_size__14218)->dbl;
        }
    }
    DeRef(_7788);
    _7788 = NOVALUE;
    goto L3; // [69] 80
L2: 

    /** 		size_ = requested_bucket_size_p*/
    _size__14218 = _requested_bucket_size_p_14217;
L3: 

    /** 	size_ = primes:next_prime(size_, -size_, 2)	-- Allow up to 2 seconds to calc next prime.*/
    if ((unsigned long)_size__14218 == 0xC0000000)
    _7790 = (int)NewDouble((double)-0xC0000000);
    else
    _7790 = - _size__14218;
    _size__14218 = _29next_prime(_size__14218, _7790, 2);
    _7790 = NOVALUE;
    if (!IS_ATOM_INT(_size__14218)) {
        _1 = (long)(DBL_PTR(_size__14218)->dbl);
        if (UNIQUE(DBL_PTR(_size__14218)) && (DBL_PTR(_size__14218)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size__14218);
        _size__14218 = _1;
    }

    /** 	if size_ < 0 then*/
    if (_size__14218 >= 0)
    goto L4; // [99] 109

    /** 		return  -- don't do anything. New size would take too long.*/
    DeRef(_old_key_buckets__14220);
    DeRef(_old_val_buckets__14221);
    DeRef(_new_key_buckets__14222);
    DeRef(_new_val_buckets__14223);
    DeRef(_key__14224);
    DeRef(_value__14225);
    DeRef(_new_keys_14227);
    _7785 = NOVALUE;
    return;
L4: 

    /** 	old_key_buckets_ = eumem:ram_space[the_map_p][KEY_BUCKETS]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7793 = (int)*(((s1_ptr)_2)->base + _the_map_p_14216);
    DeRef(_old_key_buckets__14220);
    _2 = (int)SEQ_PTR(_7793);
    _old_key_buckets__14220 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_old_key_buckets__14220);
    _7793 = NOVALUE;

    /** 	old_val_buckets_ = eumem:ram_space[the_map_p][VALUE_BUCKETS]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7795 = (int)*(((s1_ptr)_2)->base + _the_map_p_14216);
    DeRef(_old_val_buckets__14221);
    _2 = (int)SEQ_PTR(_7795);
    _old_val_buckets__14221 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_old_val_buckets__14221);
    _7795 = NOVALUE;

    /** 	new_key_buckets_ = repeat(repeat(1, threshold_size + 1), size_)*/
    _7797 = _27threshold_size_14110 + 1;
    _7798 = Repeat(1, _7797);
    _7797 = NOVALUE;
    DeRef(_new_key_buckets__14222);
    _new_key_buckets__14222 = Repeat(_7798, _size__14218);
    DeRefDS(_7798);
    _7798 = NOVALUE;

    /** 	new_val_buckets_ = repeat(repeat(0, threshold_size), size_)*/
    _7800 = Repeat(0, _27threshold_size_14110);
    DeRef(_new_val_buckets__14223);
    _new_val_buckets__14223 = Repeat(_7800, _size__14218);
    DeRefDS(_7800);
    _7800 = NOVALUE;

    /** 	elem_count = eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7802 = (int)*(((s1_ptr)_2)->base + _the_map_p_14216);
    _2 = (int)SEQ_PTR(_7802);
    _elem_count_14229 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_elem_count_14229)){
        _elem_count_14229 = (long)DBL_PTR(_elem_count_14229)->dbl;
    }
    _7802 = NOVALUE;

    /** 	in_use = 0*/
    _in_use_14228 = 0;

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14216);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	for index = 1 to length(old_key_buckets_) do*/
    if (IS_SEQUENCE(_old_key_buckets__14220)){
            _7804 = SEQ_PTR(_old_key_buckets__14220)->length;
    }
    else {
        _7804 = 1;
    }
    {
        int _index_14259;
        _index_14259 = 1;
L5: 
        if (_index_14259 > _7804){
            goto L6; // [207] 387
        }

        /** 		for entry_idx = 1 to length(old_key_buckets_[index]) do*/
        _2 = (int)SEQ_PTR(_old_key_buckets__14220);
        _7805 = (int)*(((s1_ptr)_2)->base + _index_14259);
        if (IS_SEQUENCE(_7805)){
                _7806 = SEQ_PTR(_7805)->length;
        }
        else {
            _7806 = 1;
        }
        _7805 = NOVALUE;
        {
            int _entry_idx_14262;
            _entry_idx_14262 = 1;
L7: 
            if (_entry_idx_14262 > _7806){
                goto L8; // [223] 380
            }

            /** 			key_ = old_key_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_key_buckets__14220);
            _7807 = (int)*(((s1_ptr)_2)->base + _index_14259);
            DeRef(_key__14224);
            _2 = (int)SEQ_PTR(_7807);
            _key__14224 = (int)*(((s1_ptr)_2)->base + _entry_idx_14262);
            Ref(_key__14224);
            _7807 = NOVALUE;

            /** 			value_ = old_val_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_val_buckets__14221);
            _7809 = (int)*(((s1_ptr)_2)->base + _index_14259);
            DeRef(_value__14225);
            _2 = (int)SEQ_PTR(_7809);
            _value__14225 = (int)*(((s1_ptr)_2)->base + _entry_idx_14262);
            Ref(_value__14225);
            _7809 = NOVALUE;

            /** 			index_2_ = calc_hash(key_, size_)*/
            Ref(_key__14224);
            _index_2__14219 = _27calc_hash(_key__14224, _size__14218);
            if (!IS_ATOM_INT(_index_2__14219)) {
                _1 = (long)(DBL_PTR(_index_2__14219)->dbl);
                if (UNIQUE(DBL_PTR(_index_2__14219)) && (DBL_PTR(_index_2__14219)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_index_2__14219);
                _index_2__14219 = _1;
            }

            /** 			new_keys = new_key_buckets_[index_2_]*/
            DeRef(_new_keys_14227);
            _2 = (int)SEQ_PTR(_new_key_buckets__14222);
            _new_keys_14227 = (int)*(((s1_ptr)_2)->base + _index_2__14219);
            RefDS(_new_keys_14227);

            /** 			pos = new_keys[$]*/
            if (IS_SEQUENCE(_new_keys_14227)){
                    _7813 = SEQ_PTR(_new_keys_14227)->length;
            }
            else {
                _7813 = 1;
            }
            _2 = (int)SEQ_PTR(_new_keys_14227);
            _pos_14226 = (int)*(((s1_ptr)_2)->base + _7813);
            if (!IS_ATOM_INT(_pos_14226))
            _pos_14226 = (long)DBL_PTR(_pos_14226)->dbl;

            /** 			if length(new_keys) = pos then*/
            if (IS_SEQUENCE(_new_keys_14227)){
                    _7815 = SEQ_PTR(_new_keys_14227)->length;
            }
            else {
                _7815 = 1;
            }
            if (_7815 != _pos_14226)
            goto L9; // [285] 322

            /** 				new_keys &= repeat(pos, threshold_size)*/
            _7817 = Repeat(_pos_14226, _27threshold_size_14110);
            Concat((object_ptr)&_new_keys_14227, _new_keys_14227, _7817);
            DeRefDS(_7817);
            _7817 = NOVALUE;

            /** 				new_val_buckets_[index_2_] &= repeat(0, threshold_size)*/
            _7819 = Repeat(0, _27threshold_size_14110);
            _2 = (int)SEQ_PTR(_new_val_buckets__14223);
            _7820 = (int)*(((s1_ptr)_2)->base + _index_2__14219);
            if (IS_SEQUENCE(_7820) && IS_ATOM(_7819)) {
            }
            else if (IS_ATOM(_7820) && IS_SEQUENCE(_7819)) {
                Ref(_7820);
                Prepend(&_7821, _7819, _7820);
            }
            else {
                Concat((object_ptr)&_7821, _7820, _7819);
                _7820 = NOVALUE;
            }
            _7820 = NOVALUE;
            DeRefDS(_7819);
            _7819 = NOVALUE;
            _2 = (int)SEQ_PTR(_new_val_buckets__14223);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__14223 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__14219);
            _1 = *(int *)_2;
            *(int *)_2 = _7821;
            if( _1 != _7821 ){
                DeRef(_1);
            }
            _7821 = NOVALUE;
L9: 

            /** 			new_keys[pos] = key_*/
            Ref(_key__14224);
            _2 = (int)SEQ_PTR(_new_keys_14227);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_14227 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_14226);
            _1 = *(int *)_2;
            *(int *)_2 = _key__14224;
            DeRef(_1);

            /** 			new_val_buckets_[index_2_][pos] = value_*/
            _2 = (int)SEQ_PTR(_new_val_buckets__14223);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__14223 = MAKE_SEQ(_2);
            }
            _3 = (int)(_index_2__14219 + ((s1_ptr)_2)->base);
            Ref(_value__14225);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_14226);
            _1 = *(int *)_2;
            *(int *)_2 = _value__14225;
            DeRef(_1);
            _7822 = NOVALUE;

            /** 			new_keys[$] = pos + 1*/
            if (IS_SEQUENCE(_new_keys_14227)){
                    _7824 = SEQ_PTR(_new_keys_14227)->length;
            }
            else {
                _7824 = 1;
            }
            _7825 = _pos_14226 + 1;
            if (_7825 > MAXINT){
                _7825 = NewDouble((double)_7825);
            }
            _2 = (int)SEQ_PTR(_new_keys_14227);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_14227 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _7824);
            _1 = *(int *)_2;
            *(int *)_2 = _7825;
            if( _1 != _7825 ){
                DeRef(_1);
            }
            _7825 = NOVALUE;

            /** 			new_key_buckets_[index_2_] = new_keys*/
            RefDS(_new_keys_14227);
            _2 = (int)SEQ_PTR(_new_key_buckets__14222);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_key_buckets__14222 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__14219);
            _1 = *(int *)_2;
            *(int *)_2 = _new_keys_14227;
            DeRefDS(_1);

            /** 			if pos = 1 then*/
            if (_pos_14226 != 1)
            goto LA; // [360] 373

            /** 				in_use += 1*/
            _in_use_14228 = _in_use_14228 + 1;
LA: 

            /** 		end for*/
            _entry_idx_14262 = _entry_idx_14262 + 1;
            goto L7; // [375] 230
L8: 
            ;
        }

        /** 	end for*/
        _index_14259 = _index_14259 + 1;
        goto L5; // [382] 214
L6: 
        ;
    }

    /** 	for index = 1 to length(new_key_buckets_) do*/
    if (IS_SEQUENCE(_new_key_buckets__14222)){
            _7828 = SEQ_PTR(_new_key_buckets__14222)->length;
    }
    else {
        _7828 = 1;
    }
    {
        int _index_14289;
        _index_14289 = 1;
LB: 
        if (_index_14289 > _7828){
            goto LC; // [392] 467
        }

        /** 		pos = new_key_buckets_[index][$]*/
        _2 = (int)SEQ_PTR(_new_key_buckets__14222);
        _7829 = (int)*(((s1_ptr)_2)->base + _index_14289);
        if (IS_SEQUENCE(_7829)){
                _7830 = SEQ_PTR(_7829)->length;
        }
        else {
            _7830 = 1;
        }
        _2 = (int)SEQ_PTR(_7829);
        _pos_14226 = (int)*(((s1_ptr)_2)->base + _7830);
        if (!IS_ATOM_INT(_pos_14226)){
            _pos_14226 = (long)DBL_PTR(_pos_14226)->dbl;
        }
        _7829 = NOVALUE;

        /** 		new_key_buckets_[index] = remove(new_key_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_key_buckets__14222);
        _7832 = (int)*(((s1_ptr)_2)->base + _index_14289);
        _2 = (int)SEQ_PTR(_new_key_buckets__14222);
        _7833 = (int)*(((s1_ptr)_2)->base + _index_14289);
        if (IS_SEQUENCE(_7833)){
                _7834 = SEQ_PTR(_7833)->length;
        }
        else {
            _7834 = 1;
        }
        _7833 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_7832);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_14226)) ? _pos_14226 : (long)(DBL_PTR(_pos_14226)->dbl);
            int stop = (IS_ATOM_INT(_7834)) ? _7834 : (long)(DBL_PTR(_7834)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_7832);
                DeRef(_7835);
                _7835 = _7832;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_7832), start, &_7835 );
                }
                else Tail(SEQ_PTR(_7832), stop+1, &_7835);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_7832), start, &_7835);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_7835);
                _7835 = _1;
            }
        }
        _7832 = NOVALUE;
        _7834 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_key_buckets__14222);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_key_buckets__14222 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_14289);
        _1 = *(int *)_2;
        *(int *)_2 = _7835;
        if( _1 != _7835 ){
            DeRefDS(_1);
        }
        _7835 = NOVALUE;

        /** 		new_val_buckets_[index] = remove(new_val_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_val_buckets__14223);
        _7836 = (int)*(((s1_ptr)_2)->base + _index_14289);
        _2 = (int)SEQ_PTR(_new_val_buckets__14223);
        _7837 = (int)*(((s1_ptr)_2)->base + _index_14289);
        if (IS_SEQUENCE(_7837)){
                _7838 = SEQ_PTR(_7837)->length;
        }
        else {
            _7838 = 1;
        }
        _7837 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_7836);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_14226)) ? _pos_14226 : (long)(DBL_PTR(_pos_14226)->dbl);
            int stop = (IS_ATOM_INT(_7838)) ? _7838 : (long)(DBL_PTR(_7838)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_7836);
                DeRef(_7839);
                _7839 = _7836;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_7836), start, &_7839 );
                }
                else Tail(SEQ_PTR(_7836), stop+1, &_7839);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_7836), start, &_7839);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_7839);
                _7839 = _1;
            }
        }
        _7836 = NOVALUE;
        _7838 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_val_buckets__14223);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_val_buckets__14223 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_14289);
        _1 = *(int *)_2;
        *(int *)_2 = _7839;
        if( _1 != _7839 ){
            DeRef(_1);
        }
        _7839 = NOVALUE;

        /** 	end for*/
        _index_14289 = _index_14289 + 1;
        goto LB; // [462] 399
LC: 
        ;
    }

    /** 	eumem:ram_space[the_map_p] = { */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_27type_is_map_14088);
    *((int *)(_2+4)) = _27type_is_map_14088;
    *((int *)(_2+8)) = _elem_count_14229;
    *((int *)(_2+12)) = _in_use_14228;
    *((int *)(_2+16)) = 76;
    RefDS(_new_key_buckets__14222);
    *((int *)(_2+20)) = _new_key_buckets__14222;
    RefDS(_new_val_buckets__14223);
    *((int *)(_2+24)) = _new_val_buckets__14223;
    _7840 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14216);
    _1 = *(int *)_2;
    *(int *)_2 = _7840;
    if( _1 != _7840 ){
        DeRef(_1);
    }
    _7840 = NOVALUE;

    /** end procedure*/
    DeRef(_old_key_buckets__14220);
    DeRef(_old_val_buckets__14221);
    DeRefDS(_new_key_buckets__14222);
    DeRefDS(_new_val_buckets__14223);
    DeRef(_key__14224);
    DeRef(_value__14225);
    DeRef(_new_keys_14227);
    _7785 = NOVALUE;
    _7805 = NOVALUE;
    _7833 = NOVALUE;
    _7837 = NOVALUE;
    return;
    ;
}


int _27new(int _initial_size_p_14305)
{
    int _buckets__14307 = NOVALUE;
    int _new_map__14308 = NOVALUE;
    int _temp_map__14309 = NOVALUE;
    int _7853 = NOVALUE;
    int _7852 = NOVALUE;
    int _7851 = NOVALUE;
    int _7849 = NOVALUE;
    int _7848 = NOVALUE;
    int _7845 = NOVALUE;
    int _7844 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_14305)) {
        _1 = (long)(DBL_PTR(_initial_size_p_14305)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_14305)) && (DBL_PTR(_initial_size_p_14305)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_14305);
        _initial_size_p_14305 = _1;
    }

    /** 	if initial_size_p < 3 then*/
    if (_initial_size_p_14305 >= 3)
    goto L1; // [7] 19

    /** 		initial_size_p = 3*/
    _initial_size_p_14305 = 3;
L1: 

    /** 	if initial_size_p > threshold_size then*/
    if (_initial_size_p_14305 <= _27threshold_size_14110)
    goto L2; // [23] 83

    /** 		buckets_ = floor((initial_size_p + threshold_size - 1) / threshold_size)*/
    _7844 = _initial_size_p_14305 + _27threshold_size_14110;
    if ((long)((unsigned long)_7844 + (unsigned long)HIGH_BITS) >= 0) 
    _7844 = NewDouble((double)_7844);
    if (IS_ATOM_INT(_7844)) {
        _7845 = _7844 - 1;
        if ((long)((unsigned long)_7845 +(unsigned long) HIGH_BITS) >= 0){
            _7845 = NewDouble((double)_7845);
        }
    }
    else {
        _7845 = NewDouble(DBL_PTR(_7844)->dbl - (double)1);
    }
    DeRef(_7844);
    _7844 = NOVALUE;
    if (IS_ATOM_INT(_7845)) {
        if (_27threshold_size_14110 > 0 && _7845 >= 0) {
            _buckets__14307 = _7845 / _27threshold_size_14110;
        }
        else {
            temp_dbl = floor((double)_7845 / (double)_27threshold_size_14110);
            _buckets__14307 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7845, _27threshold_size_14110);
        _buckets__14307 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_7845);
    _7845 = NOVALUE;
    if (!IS_ATOM_INT(_buckets__14307)) {
        _1 = (long)(DBL_PTR(_buckets__14307)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__14307)) && (DBL_PTR(_buckets__14307)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__14307);
        _buckets__14307 = _1;
    }

    /** 		buckets_ = primes:next_prime(buckets_)*/
    _buckets__14307 = _29next_prime(_buckets__14307, -1, 1);
    if (!IS_ATOM_INT(_buckets__14307)) {
        _1 = (long)(DBL_PTR(_buckets__14307)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__14307)) && (DBL_PTR(_buckets__14307)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__14307);
        _buckets__14307 = _1;
    }

    /** 		new_map_ = { type_is_map, 0, 0, LARGEMAP, repeat({}, buckets_), repeat({}, buckets_) }*/
    _7848 = Repeat(_5, _buckets__14307);
    _7849 = Repeat(_5, _buckets__14307);
    _0 = _new_map__14308;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_27type_is_map_14088);
    *((int *)(_2+4)) = _27type_is_map_14088;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 76;
    *((int *)(_2+20)) = _7848;
    *((int *)(_2+24)) = _7849;
    _new_map__14308 = MAKE_SEQ(_1);
    DeRef(_0);
    _7849 = NOVALUE;
    _7848 = NOVALUE;
    goto L3; // [80] 108
L2: 

    /** 		new_map_ = {*/
    _7851 = Repeat(_27init_small_map_key_14111, _initial_size_p_14305);
    _7852 = Repeat(0, _initial_size_p_14305);
    _7853 = Repeat(0, _initial_size_p_14305);
    _0 = _new_map__14308;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_27type_is_map_14088);
    *((int *)(_2+4)) = _27type_is_map_14088;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _7851;
    *((int *)(_2+24)) = _7852;
    *((int *)(_2+28)) = _7853;
    _new_map__14308 = MAKE_SEQ(_1);
    DeRef(_0);
    _7853 = NOVALUE;
    _7852 = NOVALUE;
    _7851 = NOVALUE;
L3: 

    /** 	temp_map_ = eumem:malloc()*/
    _0 = _temp_map__14309;
    _temp_map__14309 = _28malloc(1, 1);
    DeRef(_0);

    /** 	eumem:ram_space[temp_map_] = new_map_*/
    RefDS(_new_map__14308);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map__14309))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map__14309)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map__14309);
    _1 = *(int *)_2;
    *(int *)_2 = _new_map__14308;
    DeRef(_1);

    /** 	return temp_map_*/
    DeRefDS(_new_map__14308);
    return _temp_map__14309;
    ;
}


int _27new_extra(int _the_map_p_14329, int _initial_size_p_14330)
{
    int _7857 = NOVALUE;
    int _7856 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_14330)) {
        _1 = (long)(DBL_PTR(_initial_size_p_14330)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_14330)) && (DBL_PTR(_initial_size_p_14330)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_14330);
        _initial_size_p_14330 = _1;
    }

    /** 	if map(the_map_p) then*/
    Ref(_the_map_p_14329);
    _7856 = _27map(_the_map_p_14329);
    if (_7856 == 0) {
        DeRef(_7856);
        _7856 = NOVALUE;
        goto L1; // [11] 23
    }
    else {
        if (!IS_ATOM_INT(_7856) && DBL_PTR(_7856)->dbl == 0.0){
            DeRef(_7856);
            _7856 = NOVALUE;
            goto L1; // [11] 23
        }
        DeRef(_7856);
        _7856 = NOVALUE;
    }
    DeRef(_7856);
    _7856 = NOVALUE;

    /** 		return the_map_p*/
    return _the_map_p_14329;
    goto L2; // [20] 34
L1: 

    /** 		return new(initial_size_p)*/
    _7857 = _27new(_initial_size_p_14330);
    DeRef(_the_map_p_14329);
    return _7857;
L2: 
    ;
}


int _27compare(int _map_1_p_14337, int _map_2_p_14338, int _scope_p_14339)
{
    int _data_set_1__14340 = NOVALUE;
    int _data_set_2__14341 = NOVALUE;
    int _7871 = NOVALUE;
    int _7865 = NOVALUE;
    int _7863 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scope_p_14339)) {
        _1 = (long)(DBL_PTR(_scope_p_14339)->dbl);
        if (UNIQUE(DBL_PTR(_scope_p_14339)) && (DBL_PTR(_scope_p_14339)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_p_14339);
        _scope_p_14339 = _1;
    }

    /** 	if map_1_p = map_2_p then*/
    if (binary_op_a(NOTEQ, _map_1_p_14337, _map_2_p_14338)){
        goto L1; // [7] 18
    }

    /** 		return 0*/
    DeRef(_map_1_p_14337);
    DeRef(_map_2_p_14338);
    DeRef(_data_set_1__14340);
    DeRef(_data_set_2__14341);
    return 0;
L1: 

    /** 	switch scope_p do*/
    _0 = _scope_p_14339;
    switch ( _0 ){ 

        /** 		case 'v', 'V' then*/
        case 118:
        case 86:

        /** 			data_set_1_ = stdsort:sort(values(map_1_p))*/
        Ref(_map_1_p_14337);
        _7863 = _27values(_map_1_p_14337, 0, 0);
        _0 = _data_set_1__14340;
        _data_set_1__14340 = _21sort(_7863, 1);
        DeRef(_0);
        _7863 = NOVALUE;

        /** 			data_set_2_ = stdsort:sort(values(map_2_p))*/
        Ref(_map_2_p_14338);
        _7865 = _27values(_map_2_p_14338, 0, 0);
        _0 = _data_set_2__14341;
        _data_set_2__14341 = _21sort(_7865, 1);
        DeRef(_0);
        _7865 = NOVALUE;
        goto L2; // [61] 112

        /** 		case 'k', 'K' then*/
        case 107:
        case 75:

        /** 			data_set_1_ = keys(map_1_p, 1)*/
        Ref(_map_1_p_14337);
        _0 = _data_set_1__14340;
        _data_set_1__14340 = _27keys(_map_1_p_14337, 1);
        DeRef(_0);

        /** 			data_set_2_ = keys(map_2_p, 1)*/
        Ref(_map_2_p_14338);
        _0 = _data_set_2__14341;
        _data_set_2__14341 = _27keys(_map_2_p_14338, 1);
        DeRef(_0);
        goto L2; // [87] 112

        /** 		case else*/
        default:

        /** 			data_set_1_ = pairs(map_1_p, 1)*/
        Ref(_map_1_p_14337);
        _0 = _data_set_1__14340;
        _data_set_1__14340 = _27pairs(_map_1_p_14337, 1);
        DeRef(_0);

        /** 			data_set_2_ = pairs(map_2_p, 1)*/
        Ref(_map_2_p_14338);
        _0 = _data_set_2__14341;
        _data_set_2__14341 = _27pairs(_map_2_p_14338, 1);
        DeRef(_0);
    ;}L2: 

    /** 	if equal(data_set_1_, data_set_2_) then*/
    if (_data_set_1__14340 == _data_set_2__14341)
    _7871 = 1;
    else if (IS_ATOM_INT(_data_set_1__14340) && IS_ATOM_INT(_data_set_2__14341))
    _7871 = 0;
    else
    _7871 = (compare(_data_set_1__14340, _data_set_2__14341) == 0);
    if (_7871 == 0)
    {
        _7871 = NOVALUE;
        goto L3; // [122] 132
    }
    else{
        _7871 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_map_1_p_14337);
    DeRef(_map_2_p_14338);
    DeRefDS(_data_set_1__14340);
    DeRefDS(_data_set_2__14341);
    return 1;
L3: 

    /** 	return -1*/
    DeRef(_map_1_p_14337);
    DeRef(_map_2_p_14338);
    DeRef(_data_set_1__14340);
    DeRef(_data_set_2__14341);
    return -1;
    ;
}


int _27has(int _the_map_p_14369, int _the_key_p_14370)
{
    int _index__14371 = NOVALUE;
    int _pos__14372 = NOVALUE;
    int _from__14373 = NOVALUE;
    int _7896 = NOVALUE;
    int _7894 = NOVALUE;
    int _7893 = NOVALUE;
    int _7890 = NOVALUE;
    int _7889 = NOVALUE;
    int _7888 = NOVALUE;
    int _7886 = NOVALUE;
    int _7885 = NOVALUE;
    int _7883 = NOVALUE;
    int _7881 = NOVALUE;
    int _7880 = NOVALUE;
    int _7879 = NOVALUE;
    int _7877 = NOVALUE;
    int _7876 = NOVALUE;
    int _7875 = NOVALUE;
    int _7873 = NOVALUE;
    int _7872 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_14369)) {
        _1 = (long)(DBL_PTR(_the_map_p_14369)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_14369)) && (DBL_PTR(_the_map_p_14369)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_14369);
        _the_map_p_14369 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7872 = (int)*(((s1_ptr)_2)->base + _the_map_p_14369);
    _2 = (int)SEQ_PTR(_7872);
    _7873 = (int)*(((s1_ptr)_2)->base + 4);
    _7872 = NOVALUE;
    if (binary_op_a(NOTEQ, _7873, 76)){
        _7873 = NOVALUE;
        goto L1; // [19] 77
    }
    _7873 = NOVALUE;

    /** 		index_ = calc_hash(the_key_p, length(eumem:ram_space[the_map_p][KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7875 = (int)*(((s1_ptr)_2)->base + _the_map_p_14369);
    _2 = (int)SEQ_PTR(_7875);
    _7876 = (int)*(((s1_ptr)_2)->base + 5);
    _7875 = NOVALUE;
    if (IS_SEQUENCE(_7876)){
            _7877 = SEQ_PTR(_7876)->length;
    }
    else {
        _7877 = 1;
    }
    _7876 = NOVALUE;
    Ref(_the_key_p_14370);
    _index__14371 = _27calc_hash(_the_key_p_14370, _7877);
    _7877 = NOVALUE;
    if (!IS_ATOM_INT(_index__14371)) {
        _1 = (long)(DBL_PTR(_index__14371)->dbl);
        if (UNIQUE(DBL_PTR(_index__14371)) && (DBL_PTR(_index__14371)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index__14371);
        _index__14371 = _1;
    }

    /** 		pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_BUCKETS][index_])*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7879 = (int)*(((s1_ptr)_2)->base + _the_map_p_14369);
    _2 = (int)SEQ_PTR(_7879);
    _7880 = (int)*(((s1_ptr)_2)->base + 5);
    _7879 = NOVALUE;
    _2 = (int)SEQ_PTR(_7880);
    _7881 = (int)*(((s1_ptr)_2)->base + _index__14371);
    _7880 = NOVALUE;
    _pos__14372 = find_from(_the_key_p_14370, _7881, 1);
    _7881 = NOVALUE;
    goto L2; // [74] 206
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_14370 == _27init_small_map_key_14111)
    _7883 = 1;
    else if (IS_ATOM_INT(_the_key_p_14370) && IS_ATOM_INT(_27init_small_map_key_14111))
    _7883 = 0;
    else
    _7883 = (compare(_the_key_p_14370, _27init_small_map_key_14111) == 0);
    if (_7883 == 0)
    {
        _7883 = NOVALUE;
        goto L3; // [83] 183
    }
    else{
        _7883 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__14373 = 1;

    /** 			while from_ > 0 do*/
L4: 
    if (_from__14373 <= 0)
    goto L5; // [98] 205

    /** 				pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7885 = (int)*(((s1_ptr)_2)->base + _the_map_p_14369);
    _2 = (int)SEQ_PTR(_7885);
    _7886 = (int)*(((s1_ptr)_2)->base + 5);
    _7885 = NOVALUE;
    _pos__14372 = find_from(_the_key_p_14370, _7886, _from__14373);
    _7886 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__14372 == 0)
    {
        goto L6; // [125] 160
    }
    else{
    }

    /** 					if eumem:ram_space[the_map_p][FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7888 = (int)*(((s1_ptr)_2)->base + _the_map_p_14369);
    _2 = (int)SEQ_PTR(_7888);
    _7889 = (int)*(((s1_ptr)_2)->base + 7);
    _7888 = NOVALUE;
    _2 = (int)SEQ_PTR(_7889);
    _7890 = (int)*(((s1_ptr)_2)->base + _pos__14372);
    _7889 = NOVALUE;
    if (binary_op_a(NOTEQ, _7890, 1)){
        _7890 = NOVALUE;
        goto L7; // [146] 167
    }
    _7890 = NOVALUE;

    /** 						return 1*/
    DeRef(_the_key_p_14370);
    _7876 = NOVALUE;
    return 1;
    goto L7; // [157] 167
L6: 

    /** 					return 0*/
    DeRef(_the_key_p_14370);
    _7876 = NOVALUE;
    return 0;
L7: 

    /** 				from_ = pos_ + 1*/
    _from__14373 = _pos__14372 + 1;

    /** 			end while*/
    goto L4; // [177] 98
    goto L5; // [180] 205
L3: 

    /** 			pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST])*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _7893 = (int)*(((s1_ptr)_2)->base + _the_map_p_14369);
    _2 = (int)SEQ_PTR(_7893);
    _7894 = (int)*(((s1_ptr)_2)->base + 5);
    _7893 = NOVALUE;
    _pos__14372 = find_from(_the_key_p_14370, _7894, 1);
    _7894 = NOVALUE;
L5: 
L2: 

    /** 	return (pos_  != 0)	*/
    _7896 = (_pos__14372 != 0);
    DeRef(_the_key_p_14370);
    _7876 = NOVALUE;
    return _7896;
    ;
}


int _27get(int _the_map_p_14409, int _the_key_p_14410, int _default_value_p_14411)
{
    int _bucket__14412 = NOVALUE;
    int _pos__14413 = NOVALUE;
    int _from__14414 = NOVALUE;
    int _themap_14415 = NOVALUE;
    int _thekeys_14420 = NOVALUE;
    int _7922 = NOVALUE;
    int _7921 = NOVALUE;
    int _7919 = NOVALUE;
    int _7917 = NOVALUE;
    int _7916 = NOVALUE;
    int _7914 = NOVALUE;
    int _7913 = NOVALUE;
    int _7911 = NOVALUE;
    int _7909 = NOVALUE;
    int _7908 = NOVALUE;
    int _7907 = NOVALUE;
    int _7906 = NOVALUE;
    int _7903 = NOVALUE;
    int _7901 = NOVALUE;
    int _7898 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_14409)) {
        _1 = (long)(DBL_PTR(_the_map_p_14409)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_14409)) && (DBL_PTR(_the_map_p_14409)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_14409);
        _the_map_p_14409 = _1;
    }

    /** 	themap = eumem:ram_space[the_map_p]*/
    DeRef(_themap_14415);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _themap_14415 = (int)*(((s1_ptr)_2)->base + _the_map_p_14409);
    Ref(_themap_14415);

    /** 	if themap[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_themap_14415);
    _7898 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7898, 76)){
        _7898 = NOVALUE;
        goto L1; // [23] 104
    }
    _7898 = NOVALUE;

    /** 		sequence thekeys*/

    /** 		thekeys = themap[KEY_BUCKETS]*/
    DeRef(_thekeys_14420);
    _2 = (int)SEQ_PTR(_themap_14415);
    _thekeys_14420 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_thekeys_14420);

    /** 		bucket_ = calc_hash(the_key_p, length(thekeys))*/
    if (IS_SEQUENCE(_thekeys_14420)){
            _7901 = SEQ_PTR(_thekeys_14420)->length;
    }
    else {
        _7901 = 1;
    }
    Ref(_the_key_p_14410);
    _bucket__14412 = _27calc_hash(_the_key_p_14410, _7901);
    _7901 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__14412)) {
        _1 = (long)(DBL_PTR(_bucket__14412)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__14412)) && (DBL_PTR(_bucket__14412)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__14412);
        _bucket__14412 = _1;
    }

    /** 		pos_ = find(the_key_p, thekeys[bucket_])*/
    _2 = (int)SEQ_PTR(_thekeys_14420);
    _7903 = (int)*(((s1_ptr)_2)->base + _bucket__14412);
    _pos__14413 = find_from(_the_key_p_14410, _7903, 1);
    _7903 = NOVALUE;

    /** 		if pos_ > 0 then*/
    if (_pos__14413 <= 0)
    goto L2; // [68] 93

    /** 			return themap[VALUE_BUCKETS][bucket_][pos_]*/
    _2 = (int)SEQ_PTR(_themap_14415);
    _7906 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7906);
    _7907 = (int)*(((s1_ptr)_2)->base + _bucket__14412);
    _7906 = NOVALUE;
    _2 = (int)SEQ_PTR(_7907);
    _7908 = (int)*(((s1_ptr)_2)->base + _pos__14413);
    _7907 = NOVALUE;
    Ref(_7908);
    DeRefDS(_thekeys_14420);
    DeRef(_the_key_p_14410);
    DeRef(_default_value_p_14411);
    DeRefDS(_themap_14415);
    return _7908;
L2: 

    /** 		return default_value_p*/
    DeRef(_thekeys_14420);
    DeRef(_the_key_p_14410);
    DeRef(_themap_14415);
    _7908 = NOVALUE;
    return _default_value_p_14411;
    goto L3; // [101] 247
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_14410 == _27init_small_map_key_14111)
    _7909 = 1;
    else if (IS_ATOM_INT(_the_key_p_14410) && IS_ATOM_INT(_27init_small_map_key_14111))
    _7909 = 0;
    else
    _7909 = (compare(_the_key_p_14410, _27init_small_map_key_14111) == 0);
    if (_7909 == 0)
    {
        _7909 = NOVALUE;
        goto L4; // [110] 208
    }
    else{
        _7909 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__14414 = 1;

    /** 			while from_ > 0 do*/
L5: 
    if (_from__14414 <= 0)
    goto L6; // [125] 246

    /** 				pos_ = find(the_key_p, themap[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_themap_14415);
    _7911 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__14413 = find_from(_the_key_p_14410, _7911, _from__14414);
    _7911 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__14413 == 0)
    {
        goto L7; // [146] 185
    }
    else{
    }

    /** 					if themap[FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_themap_14415);
    _7913 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7913);
    _7914 = (int)*(((s1_ptr)_2)->base + _pos__14413);
    _7913 = NOVALUE;
    if (binary_op_a(NOTEQ, _7914, 1)){
        _7914 = NOVALUE;
        goto L8; // [161] 192
    }
    _7914 = NOVALUE;

    /** 						return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_14415);
    _7916 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7916);
    _7917 = (int)*(((s1_ptr)_2)->base + _pos__14413);
    _7916 = NOVALUE;
    Ref(_7917);
    DeRef(_the_key_p_14410);
    DeRef(_default_value_p_14411);
    DeRefDS(_themap_14415);
    _7908 = NOVALUE;
    return _7917;
    goto L8; // [182] 192
L7: 

    /** 					return default_value_p*/
    DeRef(_the_key_p_14410);
    DeRef(_themap_14415);
    _7908 = NOVALUE;
    _7917 = NOVALUE;
    return _default_value_p_14411;
L8: 

    /** 				from_ = pos_ + 1*/
    _from__14414 = _pos__14413 + 1;

    /** 			end while*/
    goto L5; // [202] 125
    goto L6; // [205] 246
L4: 

    /** 			pos_ = find(the_key_p, themap[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_themap_14415);
    _7919 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__14413 = find_from(_the_key_p_14410, _7919, 1);
    _7919 = NOVALUE;

    /** 			if pos_  then*/
    if (_pos__14413 == 0)
    {
        goto L9; // [225] 245
    }
    else{
    }

    /** 				return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_14415);
    _7921 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7921);
    _7922 = (int)*(((s1_ptr)_2)->base + _pos__14413);
    _7921 = NOVALUE;
    Ref(_7922);
    DeRef(_the_key_p_14410);
    DeRef(_default_value_p_14411);
    DeRefDS(_themap_14415);
    _7908 = NOVALUE;
    _7917 = NOVALUE;
    return _7922;
L9: 
L6: 
L3: 

    /** 	return default_value_p*/
    DeRef(_the_key_p_14410);
    DeRef(_themap_14415);
    _7908 = NOVALUE;
    _7917 = NOVALUE;
    _7922 = NOVALUE;
    return _default_value_p_14411;
    ;
}


int _27nested_get(int _the_map_p_14455, int _the_keys_p_14456, int _default_value_p_14457)
{
    int _val__14462 = NOVALUE;
    int _7932 = NOVALUE;
    int _7931 = NOVALUE;
    int _7929 = NOVALUE;
    int _7927 = NOVALUE;
    int _7925 = NOVALUE;
    int _7924 = NOVALUE;
    int _7923 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( the_keys_p ) - 1 do*/
    if (IS_SEQUENCE(_the_keys_p_14456)){
            _7923 = SEQ_PTR(_the_keys_p_14456)->length;
    }
    else {
        _7923 = 1;
    }
    _7924 = _7923 - 1;
    _7923 = NOVALUE;
    {
        int _i_14459;
        _i_14459 = 1;
L1: 
        if (_i_14459 > _7924){
            goto L2; // [12] 74
        }

        /** 		object val_ = get( the_map_p, the_keys_p[1], 0 )*/
        _2 = (int)SEQ_PTR(_the_keys_p_14456);
        _7925 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_the_map_p_14455);
        Ref(_7925);
        _0 = _val__14462;
        _val__14462 = _27get(_the_map_p_14455, _7925, 0);
        DeRef(_0);
        _7925 = NOVALUE;

        /** 		if not map( val_ ) then*/
        Ref(_val__14462);
        _7927 = _27map(_val__14462);
        if (IS_ATOM_INT(_7927)) {
            if (_7927 != 0){
                DeRef(_7927);
                _7927 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        else {
            if (DBL_PTR(_7927)->dbl != 0.0){
                DeRef(_7927);
                _7927 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        DeRef(_7927);
        _7927 = NOVALUE;

        /** 			return default_value_p*/
        DeRef(_val__14462);
        DeRef(_the_map_p_14455);
        DeRefDS(_the_keys_p_14456);
        DeRef(_7924);
        _7924 = NOVALUE;
        return _default_value_p_14457;
        goto L4; // [46] 65
L3: 

        /** 			the_map_p = val_*/
        Ref(_val__14462);
        DeRef(_the_map_p_14455);
        _the_map_p_14455 = _val__14462;

        /** 			the_keys_p = the_keys_p[2..$]*/
        if (IS_SEQUENCE(_the_keys_p_14456)){
                _7929 = SEQ_PTR(_the_keys_p_14456)->length;
        }
        else {
            _7929 = 1;
        }
        rhs_slice_target = (object_ptr)&_the_keys_p_14456;
        RHS_Slice(_the_keys_p_14456, 2, _7929);
L4: 
        DeRef(_val__14462);
        _val__14462 = NOVALUE;

        /** 	end for*/
        _i_14459 = _i_14459 + 1;
        goto L1; // [69] 19
L2: 
        ;
    }

    /** 	return get( the_map_p, the_keys_p[1], default_value_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_14456);
    _7931 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_14455);
    Ref(_7931);
    Ref(_default_value_p_14457);
    _7932 = _27get(_the_map_p_14455, _7931, _default_value_p_14457);
    _7931 = NOVALUE;
    DeRef(_the_map_p_14455);
    DeRefDS(_the_keys_p_14456);
    DeRef(_default_value_p_14457);
    DeRef(_7924);
    _7924 = NOVALUE;
    return _7932;
    ;
}


void _27put(int _the_map_p_14475, int _the_key_p_14476, int _the_value_p_14477, int _operation_p_14478, int _trigger_p_14479)
{
    int _index__14480 = NOVALUE;
    int _bucket__14481 = NOVALUE;
    int _average_length__14482 = NOVALUE;
    int _from__14483 = NOVALUE;
    int _map_data_14484 = NOVALUE;
    int _data_14523 = NOVALUE;
    int _msg_inlined_crash_at_347_14539 = NOVALUE;
    int _msg_inlined_crash_at_394_14545 = NOVALUE;
    int _tmp_seqk_14559 = NOVALUE;
    int _tmp_seqv_14567 = NOVALUE;
    int _msg_inlined_crash_at_778_14603 = NOVALUE;
    int _msg_inlined_crash_at_1176_14666 = NOVALUE;
    int _8067 = NOVALUE;
    int _8066 = NOVALUE;
    int _8064 = NOVALUE;
    int _8063 = NOVALUE;
    int _8062 = NOVALUE;
    int _8061 = NOVALUE;
    int _8059 = NOVALUE;
    int _8058 = NOVALUE;
    int _8057 = NOVALUE;
    int _8055 = NOVALUE;
    int _8054 = NOVALUE;
    int _8053 = NOVALUE;
    int _8051 = NOVALUE;
    int _8050 = NOVALUE;
    int _8049 = NOVALUE;
    int _8047 = NOVALUE;
    int _8046 = NOVALUE;
    int _8045 = NOVALUE;
    int _8043 = NOVALUE;
    int _8041 = NOVALUE;
    int _8035 = NOVALUE;
    int _8034 = NOVALUE;
    int _8033 = NOVALUE;
    int _8032 = NOVALUE;
    int _8030 = NOVALUE;
    int _8028 = NOVALUE;
    int _8027 = NOVALUE;
    int _8026 = NOVALUE;
    int _8025 = NOVALUE;
    int _8024 = NOVALUE;
    int _8023 = NOVALUE;
    int _8020 = NOVALUE;
    int _8018 = NOVALUE;
    int _8015 = NOVALUE;
    int _8013 = NOVALUE;
    int _8010 = NOVALUE;
    int _8009 = NOVALUE;
    int _8007 = NOVALUE;
    int _8004 = NOVALUE;
    int _8003 = NOVALUE;
    int _8000 = NOVALUE;
    int _7997 = NOVALUE;
    int _7995 = NOVALUE;
    int _7993 = NOVALUE;
    int _7990 = NOVALUE;
    int _7988 = NOVALUE;
    int _7987 = NOVALUE;
    int _7986 = NOVALUE;
    int _7985 = NOVALUE;
    int _7984 = NOVALUE;
    int _7983 = NOVALUE;
    int _7982 = NOVALUE;
    int _7981 = NOVALUE;
    int _7980 = NOVALUE;
    int _7974 = NOVALUE;
    int _7972 = NOVALUE;
    int _7971 = NOVALUE;
    int _7969 = NOVALUE;
    int _7967 = NOVALUE;
    int _7964 = NOVALUE;
    int _7963 = NOVALUE;
    int _7962 = NOVALUE;
    int _7961 = NOVALUE;
    int _7959 = NOVALUE;
    int _7958 = NOVALUE;
    int _7957 = NOVALUE;
    int _7955 = NOVALUE;
    int _7954 = NOVALUE;
    int _7953 = NOVALUE;
    int _7951 = NOVALUE;
    int _7950 = NOVALUE;
    int _7949 = NOVALUE;
    int _7947 = NOVALUE;
    int _7945 = NOVALUE;
    int _7940 = NOVALUE;
    int _7939 = NOVALUE;
    int _7937 = NOVALUE;
    int _7936 = NOVALUE;
    int _7934 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_14475)) {
        _1 = (long)(DBL_PTR(_the_map_p_14475)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_14475)) && (DBL_PTR(_the_map_p_14475)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_14475);
        _the_map_p_14475 = _1;
    }
    if (!IS_ATOM_INT(_operation_p_14478)) {
        _1 = (long)(DBL_PTR(_operation_p_14478)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_14478)) && (DBL_PTR(_operation_p_14478)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_14478);
        _operation_p_14478 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_14479)) {
        _1 = (long)(DBL_PTR(_trigger_p_14479)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_14479)) && (DBL_PTR(_trigger_p_14479)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_14479);
        _trigger_p_14479 = _1;
    }

    /** 	sequence map_data = eumem:ram_space[the_map_p]*/
    DeRef(_map_data_14484);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _map_data_14484 = (int)*(((s1_ptr)_2)->base + _the_map_p_14475);
    Ref(_map_data_14484);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14475);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if map_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7934 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7934, 76)){
        _7934 = NOVALUE;
        goto L1; // [39] 659
    }
    _7934 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p,  length(map_data[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7936 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7936)){
            _7937 = SEQ_PTR(_7936)->length;
    }
    else {
        _7937 = 1;
    }
    _7936 = NOVALUE;
    Ref(_the_key_p_14476);
    _bucket__14481 = _27calc_hash(_the_key_p_14476, _7937);
    _7937 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__14481)) {
        _1 = (long)(DBL_PTR(_bucket__14481)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__14481)) && (DBL_PTR(_bucket__14481)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__14481);
        _bucket__14481 = _1;
    }

    /** 		index_ = find(the_key_p, map_data[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7939 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7939);
    _7940 = (int)*(((s1_ptr)_2)->base + _bucket__14481);
    _7939 = NOVALUE;
    _index__14480 = find_from(_the_key_p_14476, _7940, 1);
    _7940 = NOVALUE;

    /** 		if index_ > 0 then*/
    if (_index__14480 <= 0)
    goto L2; // [84] 381

    /** 			switch operation_p do*/
    _0 = _operation_p_14478;
    switch ( _0 ){ 

        /** 				case PUT then*/
        case 1:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__14481 + ((s1_ptr)_2)->base);
        _7945 = NOVALUE;
        Ref(_the_value_p_14477);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_14477;
        DeRef(_1);
        _7945 = NOVALUE;
        goto L3; // [117] 367

        /** 				case ADD then*/
        case 2:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__14481 + ((s1_ptr)_2)->base);
        _7947 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7949 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _7947 = NOVALUE;
        if (IS_ATOM_INT(_7949) && IS_ATOM_INT(_the_value_p_14477)) {
            _7950 = _7949 + _the_value_p_14477;
            if ((long)((unsigned long)_7950 + (unsigned long)HIGH_BITS) >= 0) 
            _7950 = NewDouble((double)_7950);
        }
        else {
            _7950 = binary_op(PLUS, _7949, _the_value_p_14477);
        }
        _7949 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _7950;
        if( _1 != _7950 ){
            DeRef(_1);
        }
        _7950 = NOVALUE;
        _7947 = NOVALUE;
        goto L3; // [149] 367

        /** 				case SUBTRACT then*/
        case 3:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__14481 + ((s1_ptr)_2)->base);
        _7951 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7953 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _7951 = NOVALUE;
        if (IS_ATOM_INT(_7953) && IS_ATOM_INT(_the_value_p_14477)) {
            _7954 = _7953 - _the_value_p_14477;
            if ((long)((unsigned long)_7954 +(unsigned long) HIGH_BITS) >= 0){
                _7954 = NewDouble((double)_7954);
            }
        }
        else {
            _7954 = binary_op(MINUS, _7953, _the_value_p_14477);
        }
        _7953 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _7954;
        if( _1 != _7954 ){
            DeRef(_1);
        }
        _7954 = NOVALUE;
        _7951 = NOVALUE;
        goto L3; // [181] 367

        /** 				case MULTIPLY then*/
        case 4:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__14481 + ((s1_ptr)_2)->base);
        _7955 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7957 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _7955 = NOVALUE;
        if (IS_ATOM_INT(_7957) && IS_ATOM_INT(_the_value_p_14477)) {
            if (_7957 == (short)_7957 && _the_value_p_14477 <= INT15 && _the_value_p_14477 >= -INT15)
            _7958 = _7957 * _the_value_p_14477;
            else
            _7958 = NewDouble(_7957 * (double)_the_value_p_14477);
        }
        else {
            _7958 = binary_op(MULTIPLY, _7957, _the_value_p_14477);
        }
        _7957 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _7958;
        if( _1 != _7958 ){
            DeRef(_1);
        }
        _7958 = NOVALUE;
        _7955 = NOVALUE;
        goto L3; // [213] 367

        /** 				case DIVIDE then*/
        case 5:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__14481 + ((s1_ptr)_2)->base);
        _7959 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7961 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _7959 = NOVALUE;
        if (IS_ATOM_INT(_7961) && IS_ATOM_INT(_the_value_p_14477)) {
            _7962 = (_7961 % _the_value_p_14477) ? NewDouble((double)_7961 / _the_value_p_14477) : (_7961 / _the_value_p_14477);
        }
        else {
            _7962 = binary_op(DIVIDE, _7961, _the_value_p_14477);
        }
        _7961 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _7962;
        if( _1 != _7962 ){
            DeRef(_1);
        }
        _7962 = NOVALUE;
        _7959 = NOVALUE;
        goto L3; // [245] 367

        /** 				case APPEND then*/
        case 6:

        /** 					sequence data = map_data[VALUE_BUCKETS][bucket_][index_]*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        _7963 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7963);
        _7964 = (int)*(((s1_ptr)_2)->base + _bucket__14481);
        _7963 = NOVALUE;
        DeRef(_data_14523);
        _2 = (int)SEQ_PTR(_7964);
        _data_14523 = (int)*(((s1_ptr)_2)->base + _index__14480);
        Ref(_data_14523);
        _7964 = NOVALUE;

        /** 					data = append( data, the_value_p )*/
        Ref(_the_value_p_14477);
        Append(&_data_14523, _data_14523, _the_value_p_14477);

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = data*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__14481 + ((s1_ptr)_2)->base);
        _7967 = NOVALUE;
        RefDS(_data_14523);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _data_14523;
        DeRef(_1);
        _7967 = NOVALUE;
        DeRefDS(_data_14523);
        _data_14523 = NOVALUE;
        goto L3; // [295] 367

        /** 				case CONCAT then*/
        case 7:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__14481 + ((s1_ptr)_2)->base);
        _7969 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7971 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _7969 = NOVALUE;
        if (IS_SEQUENCE(_7971) && IS_ATOM(_the_value_p_14477)) {
            Ref(_the_value_p_14477);
            Append(&_7972, _7971, _the_value_p_14477);
        }
        else if (IS_ATOM(_7971) && IS_SEQUENCE(_the_value_p_14477)) {
            Ref(_7971);
            Prepend(&_7972, _the_value_p_14477, _7971);
        }
        else {
            Concat((object_ptr)&_7972, _7971, _the_value_p_14477);
            _7971 = NOVALUE;
        }
        _7971 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _7972;
        if( _1 != _7972 ){
            DeRef(_1);
        }
        _7972 = NOVALUE;
        _7969 = NOVALUE;
        goto L3; // [327] 367

        /** 				case LEAVE then*/
        case 8:

        /** 					operation_p = operation_p*/
        _operation_p_14478 = _operation_p_14478;
        goto L3; // [340] 367

        /** 				case else*/
        default:

        /** 					error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_347_14539);
        _msg_inlined_crash_at_347_14539 = EPrintf(-9999999, _7973, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_347_14539);

        /** end procedure*/
        goto L4; // [361] 364
L4: 
        DeRefi(_msg_inlined_crash_at_347_14539);
        _msg_inlined_crash_at_347_14539 = NOVALUE;
    ;}L3: 

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_14484);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14475);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_14484;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_14559);
    DeRef(_tmp_seqv_14567);
    DeRef(_the_key_p_14476);
    DeRef(_the_value_p_14477);
    DeRef(_average_length__14482);
    DeRefDS(_map_data_14484);
    _7936 = NOVALUE;
    return;
L2: 

    /** 		if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _7974 = find_from(_operation_p_14478, _27INIT_OPERATIONS_14105, 1);
    if (_7974 != 0)
    goto L5; // [390] 414
    _7974 = NOVALUE;

    /** 				error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_394_14545);
    _msg_inlined_crash_at_394_14545 = EPrintf(-9999999, _7976, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_394_14545);

    /** end procedure*/
    goto L6; // [408] 411
L6: 
    DeRefi(_msg_inlined_crash_at_394_14545);
    _msg_inlined_crash_at_394_14545 = NOVALUE;
L5: 

    /** 		if operation_p = LEAVE then*/
    if (_operation_p_14478 != 8)
    goto L7; // [418] 436

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_14484);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14475);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_14484;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_14559);
    DeRef(_tmp_seqv_14567);
    DeRef(_the_key_p_14476);
    DeRef(_the_value_p_14477);
    DeRef(_average_length__14482);
    DeRefDS(_map_data_14484);
    _7936 = NOVALUE;
    return;
L7: 

    /** 		if operation_p = APPEND then*/
    if (_operation_p_14478 != 6)
    goto L8; // [440] 451

    /** 			the_value_p = { the_value_p }*/
    _0 = _the_value_p_14477;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_14477);
    *((int *)(_2+4)) = _the_value_p_14477;
    _the_value_p_14477 = MAKE_SEQ(_1);
    DeRef(_0);
L8: 

    /** 		map_data[IN_USE] += (length(map_data[KEY_BUCKETS][bucket_]) = 0)*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7980 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7980);
    _7981 = (int)*(((s1_ptr)_2)->base + _bucket__14481);
    _7980 = NOVALUE;
    if (IS_SEQUENCE(_7981)){
            _7982 = SEQ_PTR(_7981)->length;
    }
    else {
        _7982 = 1;
    }
    _7981 = NOVALUE;
    _7983 = (_7982 == 0);
    _7982 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7984 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7984)) {
        _7985 = _7984 + _7983;
        if ((long)((unsigned long)_7985 + (unsigned long)HIGH_BITS) >= 0) 
        _7985 = NewDouble((double)_7985);
    }
    else {
        _7985 = binary_op(PLUS, _7984, _7983);
    }
    _7984 = NOVALUE;
    _7983 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7985;
    if( _1 != _7985 ){
        DeRef(_1);
    }
    _7985 = NOVALUE;

    /** 		map_data[ELEMENT_COUNT] += 1 -- elementCount		*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7986 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7986)) {
        _7987 = _7986 + 1;
        if (_7987 > MAXINT){
            _7987 = NewDouble((double)_7987);
        }
    }
    else
    _7987 = binary_op(PLUS, 1, _7986);
    _7986 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7987;
    if( _1 != _7987 ){
        DeRef(_1);
    }
    _7987 = NOVALUE;

    /** 		sequence tmp_seqk*/

    /** 		tmp_seqk = map_data[KEY_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7988 = (int)*(((s1_ptr)_2)->base + 5);
    DeRef(_tmp_seqk_14559);
    _2 = (int)SEQ_PTR(_7988);
    _tmp_seqk_14559 = (int)*(((s1_ptr)_2)->base + _bucket__14481);
    Ref(_tmp_seqk_14559);
    _7988 = NOVALUE;

    /** 		map_data[KEY_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14481);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7990 = NOVALUE;

    /** 		tmp_seqk = append( tmp_seqk, the_key_p)*/
    Ref(_the_key_p_14476);
    Append(&_tmp_seqk_14559, _tmp_seqk_14559, _the_key_p_14476);

    /** 		map_data[KEY_BUCKETS][bucket_] = tmp_seqk*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqk_14559);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14481);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqk_14559;
    DeRef(_1);
    _7993 = NOVALUE;

    /** 		sequence tmp_seqv*/

    /** 		tmp_seqv = map_data[VALUE_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _7995 = (int)*(((s1_ptr)_2)->base + 6);
    DeRef(_tmp_seqv_14567);
    _2 = (int)SEQ_PTR(_7995);
    _tmp_seqv_14567 = (int)*(((s1_ptr)_2)->base + _bucket__14481);
    Ref(_tmp_seqv_14567);
    _7995 = NOVALUE;

    /** 		map_data[VALUE_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14481);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7997 = NOVALUE;

    /** 		tmp_seqv = append( tmp_seqv, the_value_p)*/
    Ref(_the_value_p_14477);
    Append(&_tmp_seqv_14567, _tmp_seqv_14567, _the_value_p_14477);

    /** 		map_data[VALUE_BUCKETS][bucket_] = tmp_seqv*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqv_14567);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14481);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqv_14567;
    DeRef(_1);
    _8000 = NOVALUE;

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_14484);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14475);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_14484;
    DeRef(_1);

    /** 		if trigger_p > 0 then*/
    if (_trigger_p_14479 <= 0)
    goto L9; // [606] 649

    /** 			average_length_ = map_data[ELEMENT_COUNT] / map_data[IN_USE]*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8003 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8004 = (int)*(((s1_ptr)_2)->base + 3);
    DeRef(_average_length__14482);
    if (IS_ATOM_INT(_8003) && IS_ATOM_INT(_8004)) {
        _average_length__14482 = (_8003 % _8004) ? NewDouble((double)_8003 / _8004) : (_8003 / _8004);
    }
    else {
        _average_length__14482 = binary_op(DIVIDE, _8003, _8004);
    }
    _8003 = NOVALUE;
    _8004 = NOVALUE;

    /** 			if (average_length_ >= trigger_p) then*/
    if (binary_op_a(LESS, _average_length__14482, _trigger_p_14479)){
        goto LA; // [630] 648
    }

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_14484);
    _map_data_14484 = _5;

    /** 				rehash(the_map_p)*/
    _27rehash(_the_map_p_14475, 0);
LA: 
L9: 

    /** 		return*/
    DeRef(_tmp_seqk_14559);
    DeRef(_tmp_seqv_14567);
    DeRef(_the_key_p_14476);
    DeRef(_the_value_p_14477);
    DeRef(_average_length__14482);
    DeRef(_map_data_14484);
    _7936 = NOVALUE;
    _7981 = NOVALUE;
    return;
    goto LB; // [656] 1209
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_14476 == _27init_small_map_key_14111)
    _8007 = 1;
    else if (IS_ATOM_INT(_the_key_p_14476) && IS_ATOM_INT(_27init_small_map_key_14111))
    _8007 = 0;
    else
    _8007 = (compare(_the_key_p_14476, _27init_small_map_key_14111) == 0);
    if (_8007 == 0)
    {
        _8007 = NOVALUE;
        goto LC; // [665] 741
    }
    else{
        _8007 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__14483 = 1;

    /** 			while index_ > 0 with entry do*/
    goto LD; // [677] 718
LE: 
    if (_index__14480 <= 0)
    goto LF; // [682] 757

    /** 				if map_data[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8009 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_8009);
    _8010 = (int)*(((s1_ptr)_2)->base + _index__14480);
    _8009 = NOVALUE;
    if (binary_op_a(NOTEQ, _8010, 1)){
        _8010 = NOVALUE;
        goto L10; // [698] 707
    }
    _8010 = NOVALUE;

    /** 					exit*/
    goto LF; // [704] 757
L10: 

    /** 				from_ = index_ + 1*/
    _from__14483 = _index__14480 + 1;

    /** 			  entry*/
LD: 

    /** 				index_ = find(the_key_p, map_data[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8013 = (int)*(((s1_ptr)_2)->base + 5);
    _index__14480 = find_from(_the_key_p_14476, _8013, _from__14483);
    _8013 = NOVALUE;

    /** 			end while*/
    goto LE; // [735] 680
    goto LF; // [738] 757
LC: 

    /** 			index_ = find(the_key_p, map_data[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8015 = (int)*(((s1_ptr)_2)->base + 5);
    _index__14480 = find_from(_the_key_p_14476, _8015, 1);
    _8015 = NOVALUE;
LF: 

    /** 		if index_ = 0 then*/
    if (_index__14480 != 0)
    goto L11; // [761] 963

    /** 			if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _8018 = find_from(_operation_p_14478, _27INIT_OPERATIONS_14105, 1);
    if (_8018 != 0)
    goto L12; // [774] 798
    _8018 = NOVALUE;

    /** 					error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_778_14603);
    _msg_inlined_crash_at_778_14603 = EPrintf(-9999999, _7976, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_778_14603);

    /** end procedure*/
    goto L13; // [792] 795
L13: 
    DeRefi(_msg_inlined_crash_at_778_14603);
    _msg_inlined_crash_at_778_14603 = NOVALUE;
L12: 

    /** 			index_ = find(0, map_data[FREE_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8020 = (int)*(((s1_ptr)_2)->base + 7);
    _index__14480 = find_from(0, _8020, 1);
    _8020 = NOVALUE;

    /** 			if index_ = 0 then*/
    if (_index__14480 != 0)
    goto L14; // [815] 869

    /** 				eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_14484);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14475);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_14484;
    DeRef(_1);

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_14484);
    _map_data_14484 = _5;

    /** 				convert_to_large_map(the_map_p)*/
    _27convert_to_large_map(_the_map_p_14475);

    /** 				put(the_map_p, the_key_p, the_value_p, operation_p, trigger_p)*/
    DeRef(_8023);
    _8023 = _the_map_p_14475;
    Ref(_the_key_p_14476);
    DeRef(_8024);
    _8024 = _the_key_p_14476;
    Ref(_the_value_p_14477);
    DeRef(_8025);
    _8025 = _the_value_p_14477;
    DeRef(_8026);
    _8026 = _operation_p_14478;
    DeRef(_8027);
    _8027 = _trigger_p_14479;
    _27put(_8023, _8024, _8025, _8026, _8027);
    _8023 = NOVALUE;
    _8024 = NOVALUE;
    _8025 = NOVALUE;
    _8026 = NOVALUE;
    _8027 = NOVALUE;

    /** 				return*/
    DeRef(_the_key_p_14476);
    DeRef(_the_value_p_14477);
    DeRef(_average_length__14482);
    DeRefDS(_map_data_14484);
    _7936 = NOVALUE;
    _7981 = NOVALUE;
    return;
L14: 

    /** 			map_data[KEY_LIST][index_] = the_key_p*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    Ref(_the_key_p_14476);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__14480);
    _1 = *(int *)_2;
    *(int *)_2 = _the_key_p_14476;
    DeRef(_1);
    _8028 = NOVALUE;

    /** 			map_data[FREE_LIST][index_] = 1*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__14480);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _8030 = NOVALUE;

    /** 			map_data[IN_USE] += 1*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8032 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_8032)) {
        _8033 = _8032 + 1;
        if (_8033 > MAXINT){
            _8033 = NewDouble((double)_8033);
        }
    }
    else
    _8033 = binary_op(PLUS, 1, _8032);
    _8032 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _8033;
    if( _1 != _8033 ){
        DeRef(_1);
    }
    _8033 = NOVALUE;

    /** 			map_data[ELEMENT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_map_data_14484);
    _8034 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_8034)) {
        _8035 = _8034 + 1;
        if (_8035 > MAXINT){
            _8035 = NewDouble((double)_8035);
        }
    }
    else
    _8035 = binary_op(PLUS, 1, _8034);
    _8034 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_14484);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_14484 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _8035;
    if( _1 != _8035 ){
        DeRef(_1);
    }
    _8035 = NOVALUE;

    /** 			if operation_p = APPEND then*/
    if (_operation_p_14478 != 6)
    goto L15; // [931] 942

    /** 				the_value_p = { the_value_p }*/
    _0 = _the_value_p_14477;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_14477);
    *((int *)(_2+4)) = _the_value_p_14477;
    _the_value_p_14477 = MAKE_SEQ(_1);
    DeRef(_0);
L15: 

    /** 			if operation_p != LEAVE then*/
    if (_operation_p_14478 == 8)
    goto L16; // [946] 962

    /** 				operation_p = PUT	-- Initially, nearly everything is a PUT.*/
    _operation_p_14478 = 1;
L16: 
L11: 

    /** 		switch operation_p do*/
    _0 = _operation_p_14478;
    switch ( _0 ){ 

        /** 			case PUT then*/
        case 1:

        /** 				map_data[VALUE_LIST][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        Ref(_the_value_p_14477);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_14477;
        DeRef(_1);
        _8041 = NOVALUE;
        goto L17; // [987] 1195

        /** 			case ADD then*/
        case 2:

        /** 				map_data[VALUE_LIST][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _8045 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _8043 = NOVALUE;
        if (IS_ATOM_INT(_8045) && IS_ATOM_INT(_the_value_p_14477)) {
            _8046 = _8045 + _the_value_p_14477;
            if ((long)((unsigned long)_8046 + (unsigned long)HIGH_BITS) >= 0) 
            _8046 = NewDouble((double)_8046);
        }
        else {
            _8046 = binary_op(PLUS, _8045, _the_value_p_14477);
        }
        _8045 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _8046;
        if( _1 != _8046 ){
            DeRef(_1);
        }
        _8046 = NOVALUE;
        _8043 = NOVALUE;
        goto L17; // [1014] 1195

        /** 			case SUBTRACT then*/
        case 3:

        /** 				map_data[VALUE_LIST][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _8049 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _8047 = NOVALUE;
        if (IS_ATOM_INT(_8049) && IS_ATOM_INT(_the_value_p_14477)) {
            _8050 = _8049 - _the_value_p_14477;
            if ((long)((unsigned long)_8050 +(unsigned long) HIGH_BITS) >= 0){
                _8050 = NewDouble((double)_8050);
            }
        }
        else {
            _8050 = binary_op(MINUS, _8049, _the_value_p_14477);
        }
        _8049 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _8050;
        if( _1 != _8050 ){
            DeRef(_1);
        }
        _8050 = NOVALUE;
        _8047 = NOVALUE;
        goto L17; // [1041] 1195

        /** 			case MULTIPLY then*/
        case 4:

        /** 				map_data[VALUE_LIST][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _8053 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _8051 = NOVALUE;
        if (IS_ATOM_INT(_8053) && IS_ATOM_INT(_the_value_p_14477)) {
            if (_8053 == (short)_8053 && _the_value_p_14477 <= INT15 && _the_value_p_14477 >= -INT15)
            _8054 = _8053 * _the_value_p_14477;
            else
            _8054 = NewDouble(_8053 * (double)_the_value_p_14477);
        }
        else {
            _8054 = binary_op(MULTIPLY, _8053, _the_value_p_14477);
        }
        _8053 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _8054;
        if( _1 != _8054 ){
            DeRef(_1);
        }
        _8054 = NOVALUE;
        _8051 = NOVALUE;
        goto L17; // [1068] 1195

        /** 			case DIVIDE then*/
        case 5:

        /** 				map_data[VALUE_LIST][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _8057 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _8055 = NOVALUE;
        if (IS_ATOM_INT(_8057) && IS_ATOM_INT(_the_value_p_14477)) {
            _8058 = (_8057 % _the_value_p_14477) ? NewDouble((double)_8057 / _the_value_p_14477) : (_8057 / _the_value_p_14477);
        }
        else {
            _8058 = binary_op(DIVIDE, _8057, _the_value_p_14477);
        }
        _8057 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _8058;
        if( _1 != _8058 ){
            DeRef(_1);
        }
        _8058 = NOVALUE;
        _8055 = NOVALUE;
        goto L17; // [1095] 1195

        /** 			case APPEND then*/
        case 6:

        /** 				map_data[VALUE_LIST][index_] = append( map_data[VALUE_LIST][index_], the_value_p )*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_map_data_14484);
        _8061 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_8061);
        _8062 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _8061 = NOVALUE;
        Ref(_the_value_p_14477);
        Append(&_8063, _8062, _the_value_p_14477);
        _8062 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _8063;
        if( _1 != _8063 ){
            DeRef(_1);
        }
        _8063 = NOVALUE;
        _8059 = NOVALUE;
        goto L17; // [1128] 1195

        /** 			case CONCAT then*/
        case 7:

        /** 				map_data[VALUE_LIST][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_14484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_14484 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _8066 = (int)*(((s1_ptr)_2)->base + _index__14480);
        _8064 = NOVALUE;
        if (IS_SEQUENCE(_8066) && IS_ATOM(_the_value_p_14477)) {
            Ref(_the_value_p_14477);
            Append(&_8067, _8066, _the_value_p_14477);
        }
        else if (IS_ATOM(_8066) && IS_SEQUENCE(_the_value_p_14477)) {
            Ref(_8066);
            Prepend(&_8067, _the_value_p_14477, _8066);
        }
        else {
            Concat((object_ptr)&_8067, _8066, _the_value_p_14477);
            _8066 = NOVALUE;
        }
        _8066 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__14480);
        _1 = *(int *)_2;
        *(int *)_2 = _8067;
        if( _1 != _8067 ){
            DeRef(_1);
        }
        _8067 = NOVALUE;
        _8064 = NOVALUE;
        goto L17; // [1155] 1195

        /** 			case LEAVE then*/
        case 8:

        /** 				operation_p = operation_p*/
        _operation_p_14478 = _operation_p_14478;
        goto L17; // [1168] 1195

        /** 			case else*/
        default:

        /** 				error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_1176_14666);
        _msg_inlined_crash_at_1176_14666 = EPrintf(-9999999, _7973, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_1176_14666);

        /** end procedure*/
        goto L18; // [1189] 1192
L18: 
        DeRefi(_msg_inlined_crash_at_1176_14666);
        _msg_inlined_crash_at_1176_14666 = NOVALUE;
    ;}L17: 

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_14484);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14475);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_14484;
    DeRef(_1);

    /** 		return*/
    DeRef(_the_key_p_14476);
    DeRef(_the_value_p_14477);
    DeRef(_average_length__14482);
    DeRefDS(_map_data_14484);
    _7936 = NOVALUE;
    _7981 = NOVALUE;
    return;
LB: 

    /** end procedure*/
    DeRef(_the_key_p_14476);
    DeRef(_the_value_p_14477);
    DeRef(_average_length__14482);
    DeRef(_map_data_14484);
    _7936 = NOVALUE;
    _7981 = NOVALUE;
    return;
    ;
}


void _27nested_put(int _the_map_p_14669, int _the_keys_p_14670, int _the_value_p_14671, int _operation_p_14672, int _trigger_p_14673)
{
    int _temp_map__14674 = NOVALUE;
    int _8080 = NOVALUE;
    int _8079 = NOVALUE;
    int _8078 = NOVALUE;
    int _8077 = NOVALUE;
    int _8076 = NOVALUE;
    int _8075 = NOVALUE;
    int _8074 = NOVALUE;
    int _8072 = NOVALUE;
    int _8071 = NOVALUE;
    int _8070 = NOVALUE;
    int _8068 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_operation_p_14672)) {
        _1 = (long)(DBL_PTR(_operation_p_14672)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_14672)) && (DBL_PTR(_operation_p_14672)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_14672);
        _operation_p_14672 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_14673)) {
        _1 = (long)(DBL_PTR(_trigger_p_14673)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_14673)) && (DBL_PTR(_trigger_p_14673)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_14673);
        _trigger_p_14673 = _1;
    }

    /** 	if length( the_keys_p ) = 1 then*/
    if (IS_SEQUENCE(_the_keys_p_14670)){
            _8068 = SEQ_PTR(_the_keys_p_14670)->length;
    }
    else {
        _8068 = 1;
    }
    if (_8068 != 1)
    goto L1; // [16] 36

    /** 		put( the_map_p, the_keys_p[1], the_value_p, operation_p, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_14670);
    _8070 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_14669);
    Ref(_8070);
    Ref(_the_value_p_14671);
    _27put(_the_map_p_14669, _8070, _the_value_p_14671, _operation_p_14672, _trigger_p_14673);
    _8070 = NOVALUE;
    goto L2; // [33] 98
L1: 

    /** 		temp_map_ = new_extra( get( the_map_p, the_keys_p[1] ) )*/
    _2 = (int)SEQ_PTR(_the_keys_p_14670);
    _8071 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_14669);
    Ref(_8071);
    _8072 = _27get(_the_map_p_14669, _8071, 0);
    _8071 = NOVALUE;
    _0 = _temp_map__14674;
    _temp_map__14674 = _27new_extra(_8072, 690);
    DeRef(_0);
    _8072 = NOVALUE;

    /** 		nested_put( temp_map_, the_keys_p[2..$], the_value_p, operation_p, trigger_p )*/
    if (IS_SEQUENCE(_the_keys_p_14670)){
            _8074 = SEQ_PTR(_the_keys_p_14670)->length;
    }
    else {
        _8074 = 1;
    }
    rhs_slice_target = (object_ptr)&_8075;
    RHS_Slice(_the_keys_p_14670, 2, _8074);
    Ref(_temp_map__14674);
    DeRef(_8076);
    _8076 = _temp_map__14674;
    Ref(_the_value_p_14671);
    DeRef(_8077);
    _8077 = _the_value_p_14671;
    DeRef(_8078);
    _8078 = _operation_p_14672;
    DeRef(_8079);
    _8079 = _trigger_p_14673;
    _27nested_put(_8076, _8075, _8077, _8078, _8079);
    _8076 = NOVALUE;
    _8075 = NOVALUE;
    _8077 = NOVALUE;
    _8078 = NOVALUE;
    _8079 = NOVALUE;

    /** 		put( the_map_p, the_keys_p[1], temp_map_, PUT, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_14670);
    _8080 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_14669);
    Ref(_8080);
    Ref(_temp_map__14674);
    _27put(_the_map_p_14669, _8080, _temp_map__14674, 1, _trigger_p_14673);
    _8080 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_the_map_p_14669);
    DeRefDS(_the_keys_p_14670);
    DeRef(_the_value_p_14671);
    DeRef(_temp_map__14674);
    return;
    ;
}


void _27remove(int _the_map_p_14692, int _the_key_p_14693)
{
    int _index__14694 = NOVALUE;
    int _bucket__14695 = NOVALUE;
    int _temp_map__14696 = NOVALUE;
    int _from__14697 = NOVALUE;
    int _8147 = NOVALUE;
    int _8146 = NOVALUE;
    int _8145 = NOVALUE;
    int _8144 = NOVALUE;
    int _8142 = NOVALUE;
    int _8140 = NOVALUE;
    int _8138 = NOVALUE;
    int _8136 = NOVALUE;
    int _8135 = NOVALUE;
    int _8133 = NOVALUE;
    int _8130 = NOVALUE;
    int _8129 = NOVALUE;
    int _8128 = NOVALUE;
    int _8127 = NOVALUE;
    int _8126 = NOVALUE;
    int _8125 = NOVALUE;
    int _8124 = NOVALUE;
    int _8123 = NOVALUE;
    int _8122 = NOVALUE;
    int _8121 = NOVALUE;
    int _8120 = NOVALUE;
    int _8119 = NOVALUE;
    int _8118 = NOVALUE;
    int _8116 = NOVALUE;
    int _8115 = NOVALUE;
    int _8114 = NOVALUE;
    int _8113 = NOVALUE;
    int _8112 = NOVALUE;
    int _8111 = NOVALUE;
    int _8110 = NOVALUE;
    int _8109 = NOVALUE;
    int _8108 = NOVALUE;
    int _8107 = NOVALUE;
    int _8106 = NOVALUE;
    int _8104 = NOVALUE;
    int _8102 = NOVALUE;
    int _8100 = NOVALUE;
    int _8099 = NOVALUE;
    int _8098 = NOVALUE;
    int _8096 = NOVALUE;
    int _8095 = NOVALUE;
    int _8094 = NOVALUE;
    int _8093 = NOVALUE;
    int _8092 = NOVALUE;
    int _8089 = NOVALUE;
    int _8088 = NOVALUE;
    int _8085 = NOVALUE;
    int _8084 = NOVALUE;
    int _8082 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14696);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_14692)){
        _temp_map__14696 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14692)->dbl));
    }
    else{
        _temp_map__14696 = (int)*(((s1_ptr)_2)->base + _the_map_p_14692);
    }
    Ref(_temp_map__14696);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_14692))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14692)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14692);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8082 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8082, 76)){
        _8082 = NOVALUE;
        goto L1; // [27] 318
    }
    _8082 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8084 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8084)){
            _8085 = SEQ_PTR(_8084)->length;
    }
    else {
        _8085 = 1;
    }
    _8084 = NOVALUE;
    Ref(_the_key_p_14693);
    _bucket__14695 = _27calc_hash(_the_key_p_14693, _8085);
    _8085 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__14695)) {
        _1 = (long)(DBL_PTR(_bucket__14695)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__14695)) && (DBL_PTR(_bucket__14695)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__14695);
        _bucket__14695 = _1;
    }

    /** 		index_ = find(the_key_p, temp_map_[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8088 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_8088);
    _8089 = (int)*(((s1_ptr)_2)->base + _bucket__14695);
    _8088 = NOVALUE;
    _index__14694 = find_from(_the_key_p_14693, _8089, 1);
    _8089 = NOVALUE;

    /** 		if index_ != 0 then*/
    if (_index__14694 == 0)
    goto L2; // [72] 464

    /** 			temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8092 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_8092)) {
        _8093 = _8092 - 1;
        if ((long)((unsigned long)_8093 +(unsigned long) HIGH_BITS) >= 0){
            _8093 = NewDouble((double)_8093);
        }
    }
    else {
        _8093 = binary_op(MINUS, _8092, 1);
    }
    _8092 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _8093;
    if( _1 != _8093 ){
        DeRef(_1);
    }
    _8093 = NOVALUE;

    /** 			if length(temp_map_[KEY_BUCKETS][bucket_]) = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8094 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_8094);
    _8095 = (int)*(((s1_ptr)_2)->base + _bucket__14695);
    _8094 = NOVALUE;
    if (IS_SEQUENCE(_8095)){
            _8096 = SEQ_PTR(_8095)->length;
    }
    else {
        _8096 = 1;
    }
    _8095 = NOVALUE;
    if (_8096 != 1)
    goto L3; // [107] 156

    /** 				temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8098 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_8098)) {
        _8099 = _8098 - 1;
        if ((long)((unsigned long)_8099 +(unsigned long) HIGH_BITS) >= 0){
            _8099 = NewDouble((double)_8099);
        }
    }
    else {
        _8099 = binary_op(MINUS, _8098, 1);
    }
    _8098 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _8099;
    if( _1 != _8099 ){
        DeRef(_1);
    }
    _8099 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14695);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _8100 = NOVALUE;

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14695);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _8102 = NOVALUE;
    goto L4; // [153] 273
L3: 

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = temp_map_[VALUE_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8106 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_8106);
    _8107 = (int)*(((s1_ptr)_2)->base + _bucket__14695);
    _8106 = NOVALUE;
    _8108 = _index__14694 - 1;
    rhs_slice_target = (object_ptr)&_8109;
    RHS_Slice(_8107, 1, _8108);
    _8107 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8110 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_8110);
    _8111 = (int)*(((s1_ptr)_2)->base + _bucket__14695);
    _8110 = NOVALUE;
    _8112 = _index__14694 + 1;
    if (_8112 > MAXINT){
        _8112 = NewDouble((double)_8112);
    }
    if (IS_SEQUENCE(_8111)){
            _8113 = SEQ_PTR(_8111)->length;
    }
    else {
        _8113 = 1;
    }
    rhs_slice_target = (object_ptr)&_8114;
    RHS_Slice(_8111, _8112, _8113);
    _8111 = NOVALUE;
    Concat((object_ptr)&_8115, _8109, _8114);
    DeRefDS(_8109);
    _8109 = NOVALUE;
    DeRef(_8109);
    _8109 = NOVALUE;
    DeRefDS(_8114);
    _8114 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14695);
    _1 = *(int *)_2;
    *(int *)_2 = _8115;
    if( _1 != _8115 ){
        DeRef(_1);
    }
    _8115 = NOVALUE;
    _8104 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = temp_map_[KEY_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8118 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_8118);
    _8119 = (int)*(((s1_ptr)_2)->base + _bucket__14695);
    _8118 = NOVALUE;
    _8120 = _index__14694 - 1;
    rhs_slice_target = (object_ptr)&_8121;
    RHS_Slice(_8119, 1, _8120);
    _8119 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8122 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_8122);
    _8123 = (int)*(((s1_ptr)_2)->base + _bucket__14695);
    _8122 = NOVALUE;
    _8124 = _index__14694 + 1;
    if (_8124 > MAXINT){
        _8124 = NewDouble((double)_8124);
    }
    if (IS_SEQUENCE(_8123)){
            _8125 = SEQ_PTR(_8123)->length;
    }
    else {
        _8125 = 1;
    }
    rhs_slice_target = (object_ptr)&_8126;
    RHS_Slice(_8123, _8124, _8125);
    _8123 = NOVALUE;
    Concat((object_ptr)&_8127, _8121, _8126);
    DeRefDS(_8121);
    _8121 = NOVALUE;
    DeRef(_8121);
    _8121 = NOVALUE;
    DeRefDS(_8126);
    _8126 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__14695);
    _1 = *(int *)_2;
    *(int *)_2 = _8127;
    if( _1 != _8127 ){
        DeRef(_1);
    }
    _8127 = NOVALUE;
    _8116 = NOVALUE;
L4: 

    /** 			if temp_map_[ELEMENT_COUNT] < floor(51 * threshold_size / 100) then*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8128 = (int)*(((s1_ptr)_2)->base + 2);
    if (_27threshold_size_14110 <= INT15 && _27threshold_size_14110 >= -INT15)
    _8129 = 51 * _27threshold_size_14110;
    else
    _8129 = NewDouble(51 * (double)_27threshold_size_14110);
    if (IS_ATOM_INT(_8129)) {
        if (100 > 0 && _8129 >= 0) {
            _8130 = _8129 / 100;
        }
        else {
            temp_dbl = floor((double)_8129 / (double)100);
            if (_8129 != MININT)
            _8130 = (long)temp_dbl;
            else
            _8130 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _8129, 100);
        _8130 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_8129);
    _8129 = NOVALUE;
    if (binary_op_a(GREATEREQ, _8128, _8130)){
        _8128 = NOVALUE;
        DeRef(_8130);
        _8130 = NOVALUE;
        goto L2; // [291] 464
    }
    _8128 = NOVALUE;
    DeRef(_8130);
    _8130 = NOVALUE;

    /** 				eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__14696);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_14692))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14692)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14692);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__14696;
    DeRef(_1);

    /** 				convert_to_small_map(the_map_p)*/
    Ref(_the_map_p_14692);
    _27convert_to_small_map(_the_map_p_14692);

    /** 				return*/
    DeRef(_the_map_p_14692);
    DeRef(_the_key_p_14693);
    DeRefDS(_temp_map__14696);
    _8084 = NOVALUE;
    _8095 = NOVALUE;
    DeRef(_8108);
    _8108 = NOVALUE;
    DeRef(_8120);
    _8120 = NOVALUE;
    DeRef(_8112);
    _8112 = NOVALUE;
    DeRef(_8124);
    _8124 = NOVALUE;
    return;
    goto L2; // [315] 464
L1: 

    /** 		from_ = 1*/
    _from__14697 = 1;

    /** 		while from_ > 0 do*/
L5: 
    if (_from__14697 <= 0)
    goto L6; // [330] 463

    /** 			index_ = find(the_key_p, temp_map_[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8133 = (int)*(((s1_ptr)_2)->base + 5);
    _index__14694 = find_from(_the_key_p_14693, _8133, _from__14697);
    _8133 = NOVALUE;

    /** 			if index_ then*/
    if (_index__14694 == 0)
    {
        goto L6; // [351] 463
    }
    else{
    }

    /** 				if temp_map_[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8135 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_8135);
    _8136 = (int)*(((s1_ptr)_2)->base + _index__14694);
    _8135 = NOVALUE;
    if (binary_op_a(NOTEQ, _8136, 1)){
        _8136 = NOVALUE;
        goto L7; // [366] 450
    }
    _8136 = NOVALUE;

    /** 					temp_map_[FREE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__14694);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _8138 = NOVALUE;

    /** 					temp_map_[KEY_LIST][index_] = init_small_map_key*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_27init_small_map_key_14111);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__14694);
    _1 = *(int *)_2;
    *(int *)_2 = _27init_small_map_key_14111;
    DeRef(_1);
    _8140 = NOVALUE;

    /** 					temp_map_[VALUE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__14694);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _8142 = NOVALUE;

    /** 					temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8144 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_8144)) {
        _8145 = _8144 - 1;
        if ((long)((unsigned long)_8145 +(unsigned long) HIGH_BITS) >= 0){
            _8145 = NewDouble((double)_8145);
        }
    }
    else {
        _8145 = binary_op(MINUS, _8144, 1);
    }
    _8144 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _8145;
    if( _1 != _8145 ){
        DeRef(_1);
    }
    _8145 = NOVALUE;

    /** 					temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__14696);
    _8146 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_8146)) {
        _8147 = _8146 - 1;
        if ((long)((unsigned long)_8147 +(unsigned long) HIGH_BITS) >= 0){
            _8147 = NewDouble((double)_8147);
        }
    }
    else {
        _8147 = binary_op(MINUS, _8146, 1);
    }
    _8146 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14696);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14696 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _8147;
    if( _1 != _8147 ){
        DeRef(_1);
    }
    _8147 = NOVALUE;
    goto L7; // [442] 450

    /** 				exit*/
    goto L6; // [447] 463
L7: 

    /** 			from_ = index_ + 1*/
    _from__14697 = _index__14694 + 1;

    /** 		end while*/
    goto L5; // [460] 330
L6: 
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__14696);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_14692))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14692)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14692);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__14696;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_14692);
    DeRef(_the_key_p_14693);
    DeRefDS(_temp_map__14696);
    _8084 = NOVALUE;
    _8095 = NOVALUE;
    DeRef(_8108);
    _8108 = NOVALUE;
    DeRef(_8120);
    _8120 = NOVALUE;
    DeRef(_8112);
    _8112 = NOVALUE;
    DeRef(_8124);
    _8124 = NOVALUE;
    return;
    ;
}


void _27clear(int _the_map_p_14778)
{
    int _temp_map__14779 = NOVALUE;
    int _8166 = NOVALUE;
    int _8165 = NOVALUE;
    int _8164 = NOVALUE;
    int _8163 = NOVALUE;
    int _8162 = NOVALUE;
    int _8161 = NOVALUE;
    int _8160 = NOVALUE;
    int _8159 = NOVALUE;
    int _8158 = NOVALUE;
    int _8157 = NOVALUE;
    int _8156 = NOVALUE;
    int _8155 = NOVALUE;
    int _8154 = NOVALUE;
    int _8153 = NOVALUE;
    int _8152 = NOVALUE;
    int _8150 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14779);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_14778)){
        _temp_map__14779 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14778)->dbl));
    }
    else{
        _temp_map__14779 = (int)*(((s1_ptr)_2)->base + _the_map_p_14778);
    }
    Ref(_temp_map__14779);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    _8150 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8150, 76)){
        _8150 = NOVALUE;
        goto L1; // [19] 84
    }
    _8150 = NOVALUE;

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_BUCKETS] = repeat({}, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    _8152 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8152)){
            _8153 = SEQ_PTR(_8152)->length;
    }
    else {
        _8153 = 1;
    }
    _8152 = NOVALUE;
    _8154 = Repeat(_5, _8153);
    _8153 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _8154;
    if( _1 != _8154 ){
        DeRef(_1);
    }
    _8154 = NOVALUE;

    /** 		temp_map_[VALUE_BUCKETS] = repeat({}, length(temp_map_[VALUE_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    _8155 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_8155)){
            _8156 = SEQ_PTR(_8155)->length;
    }
    else {
        _8156 = 1;
    }
    _8155 = NOVALUE;
    _8157 = Repeat(_5, _8156);
    _8156 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _8157;
    if( _1 != _8157 ){
        DeRef(_1);
    }
    _8157 = NOVALUE;
    goto L2; // [81] 164
L1: 

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_LIST] = repeat(init_small_map_key, length(temp_map_[KEY_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    _8158 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8158)){
            _8159 = SEQ_PTR(_8158)->length;
    }
    else {
        _8159 = 1;
    }
    _8158 = NOVALUE;
    _8160 = Repeat(_27init_small_map_key_14111, _8159);
    _8159 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _8160;
    if( _1 != _8160 ){
        DeRef(_1);
    }
    _8160 = NOVALUE;

    /** 		temp_map_[VALUE_LIST] = repeat(0, length(temp_map_[VALUE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    _8161 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_8161)){
            _8162 = SEQ_PTR(_8161)->length;
    }
    else {
        _8162 = 1;
    }
    _8161 = NOVALUE;
    _8163 = Repeat(0, _8162);
    _8162 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _8163;
    if( _1 != _8163 ){
        DeRef(_1);
    }
    _8163 = NOVALUE;

    /** 		temp_map_[FREE_LIST] = repeat(0, length(temp_map_[FREE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__14779);
    _8164 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8164)){
            _8165 = SEQ_PTR(_8164)->length;
    }
    else {
        _8165 = 1;
    }
    _8164 = NOVALUE;
    _8166 = Repeat(0, _8165);
    _8165 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14779);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__14779 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _8166;
    if( _1 != _8166 ){
        DeRef(_1);
    }
    _8166 = NOVALUE;
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__14779);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_14778))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14778)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_14778);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__14779;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_14778);
    DeRefDS(_temp_map__14779);
    _8152 = NOVALUE;
    _8155 = NOVALUE;
    _8158 = NOVALUE;
    _8161 = NOVALUE;
    _8164 = NOVALUE;
    return;
    ;
}


int _27size(int _the_map_p_14802)
{
    int _8168 = NOVALUE;
    int _8167 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_14802)){
        _8167 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14802)->dbl));
    }
    else{
        _8167 = (int)*(((s1_ptr)_2)->base + _the_map_p_14802);
    }
    _2 = (int)SEQ_PTR(_8167);
    _8168 = (int)*(((s1_ptr)_2)->base + 2);
    _8167 = NOVALUE;
    Ref(_8168);
    DeRef(_the_map_p_14802);
    return _8168;
    ;
}


int _27statistics(int _the_map_p_14820)
{
    int _statistic_set__14821 = NOVALUE;
    int _lengths__14822 = NOVALUE;
    int _length__14823 = NOVALUE;
    int _temp_map__14824 = NOVALUE;
    int _8205 = NOVALUE;
    int _8204 = NOVALUE;
    int _8203 = NOVALUE;
    int _8202 = NOVALUE;
    int _8201 = NOVALUE;
    int _8200 = NOVALUE;
    int _8199 = NOVALUE;
    int _8198 = NOVALUE;
    int _8197 = NOVALUE;
    int _8196 = NOVALUE;
    int _8195 = NOVALUE;
    int _8194 = NOVALUE;
    int _8191 = NOVALUE;
    int _8189 = NOVALUE;
    int _8186 = NOVALUE;
    int _8185 = NOVALUE;
    int _8184 = NOVALUE;
    int _8183 = NOVALUE;
    int _8181 = NOVALUE;
    int _8180 = NOVALUE;
    int _8179 = NOVALUE;
    int _8178 = NOVALUE;
    int _8176 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14824);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_14820)){
        _temp_map__14824 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14820)->dbl));
    }
    else{
        _temp_map__14824 = (int)*(((s1_ptr)_2)->base + _the_map_p_14820);
    }
    Ref(_temp_map__14824);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8176 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8176, 76)){
        _8176 = NOVALUE;
        goto L1; // [19] 196
    }
    _8176 = NOVALUE;

    /** 		statistic_set_ = { */
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8178 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8179 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8180 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8180)){
            _8181 = SEQ_PTR(_8180)->length;
    }
    else {
        _8181 = 1;
    }
    _8180 = NOVALUE;
    _0 = _statistic_set__14821;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_8178);
    *((int *)(_2+4)) = _8178;
    Ref(_8179);
    *((int *)(_2+8)) = _8179;
    *((int *)(_2+12)) = _8181;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 1073741823;
    *((int *)(_2+24)) = 0;
    *((int *)(_2+28)) = 0;
    _statistic_set__14821 = MAKE_SEQ(_1);
    DeRef(_0);
    _8181 = NOVALUE;
    _8179 = NOVALUE;
    _8178 = NOVALUE;

    /** 		lengths_ = {}*/
    RefDS(_5);
    DeRefi(_lengths__14822);
    _lengths__14822 = _5;

    /** 		for i = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8183 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8183)){
            _8184 = SEQ_PTR(_8183)->length;
    }
    else {
        _8184 = 1;
    }
    _8183 = NOVALUE;
    {
        int _i_14835;
        _i_14835 = 1;
L2: 
        if (_i_14835 > _8184){
            goto L3; // [74] 160
        }

        /** 			length_ = length(temp_map_[KEY_BUCKETS][i])*/
        _2 = (int)SEQ_PTR(_temp_map__14824);
        _8185 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_8185);
        _8186 = (int)*(((s1_ptr)_2)->base + _i_14835);
        _8185 = NOVALUE;
        if (IS_SEQUENCE(_8186)){
                _length__14823 = SEQ_PTR(_8186)->length;
        }
        else {
            _length__14823 = 1;
        }
        _8186 = NOVALUE;

        /** 			if length_ > 0 then*/
        if (_length__14823 <= 0)
        goto L4; // [100] 153

        /** 				if length_ > statistic_set_[LARGEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__14821);
        _8189 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(LESSEQ, _length__14823, _8189)){
            _8189 = NOVALUE;
            goto L5; // [112] 125
        }
        _8189 = NOVALUE;

        /** 					statistic_set_[LARGEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__14821);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__14821 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 4);
        _1 = *(int *)_2;
        *(int *)_2 = _length__14823;
        DeRef(_1);
L5: 

        /** 				if length_ < statistic_set_[SMALLEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__14821);
        _8191 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(GREATEREQ, _length__14823, _8191)){
            _8191 = NOVALUE;
            goto L6; // [133] 146
        }
        _8191 = NOVALUE;

        /** 					statistic_set_[SMALLEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__14821);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__14821 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _length__14823;
        DeRef(_1);
L6: 

        /** 				lengths_ &= length_*/
        Append(&_lengths__14822, _lengths__14822, _length__14823);
L4: 

        /** 		end for*/
        _i_14835 = _i_14835 + 1;
        goto L2; // [155] 81
L3: 
        ;
    }

    /** 		statistic_set_[AVERAGE_BUCKET] = stats:average(lengths_)*/
    RefDS(_lengths__14822);
    _8194 = _30average(_lengths__14822, 1);
    _2 = (int)SEQ_PTR(_statistic_set__14821);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__14821 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _8194;
    if( _1 != _8194 ){
        DeRef(_1);
    }
    _8194 = NOVALUE;

    /** 		statistic_set_[STDEV_BUCKET] = stats:stdev(lengths_)*/
    RefDS(_lengths__14822);
    _8195 = _30stdev(_lengths__14822, 1, 2);
    _2 = (int)SEQ_PTR(_statistic_set__14821);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__14821 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _8195;
    if( _1 != _8195 ){
        DeRef(_1);
    }
    _8195 = NOVALUE;
    goto L7; // [193] 257
L1: 

    /** 		statistic_set_ = {*/
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8196 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8197 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8198 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8198)){
            _8199 = SEQ_PTR(_8198)->length;
    }
    else {
        _8199 = 1;
    }
    _8198 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8200 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8200)){
            _8201 = SEQ_PTR(_8200)->length;
    }
    else {
        _8201 = 1;
    }
    _8200 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8202 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8202)){
            _8203 = SEQ_PTR(_8202)->length;
    }
    else {
        _8203 = 1;
    }
    _8202 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__14824);
    _8204 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8204)){
            _8205 = SEQ_PTR(_8204)->length;
    }
    else {
        _8205 = 1;
    }
    _8204 = NOVALUE;
    _0 = _statistic_set__14821;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_8196);
    *((int *)(_2+4)) = _8196;
    Ref(_8197);
    *((int *)(_2+8)) = _8197;
    *((int *)(_2+12)) = _8199;
    *((int *)(_2+16)) = _8201;
    *((int *)(_2+20)) = _8203;
    *((int *)(_2+24)) = _8205;
    *((int *)(_2+28)) = 0;
    _statistic_set__14821 = MAKE_SEQ(_1);
    DeRef(_0);
    _8205 = NOVALUE;
    _8203 = NOVALUE;
    _8201 = NOVALUE;
    _8199 = NOVALUE;
    _8197 = NOVALUE;
    _8196 = NOVALUE;
L7: 

    /** 	return statistic_set_*/
    DeRef(_the_map_p_14820);
    DeRefi(_lengths__14822);
    DeRef(_temp_map__14824);
    _8183 = NOVALUE;
    _8186 = NOVALUE;
    _8180 = NOVALUE;
    _8198 = NOVALUE;
    _8200 = NOVALUE;
    _8202 = NOVALUE;
    _8204 = NOVALUE;
    return _statistic_set__14821;
    ;
}


int _27keys(int _the_map_p_14866, int _sorted_result_14867)
{
    int _buckets__14868 = NOVALUE;
    int _current_bucket__14869 = NOVALUE;
    int _results__14870 = NOVALUE;
    int _pos__14871 = NOVALUE;
    int _temp_map__14872 = NOVALUE;
    int _8230 = NOVALUE;
    int _8228 = NOVALUE;
    int _8227 = NOVALUE;
    int _8225 = NOVALUE;
    int _8224 = NOVALUE;
    int _8223 = NOVALUE;
    int _8222 = NOVALUE;
    int _8220 = NOVALUE;
    int _8219 = NOVALUE;
    int _8218 = NOVALUE;
    int _8217 = NOVALUE;
    int _8215 = NOVALUE;
    int _8213 = NOVALUE;
    int _8210 = NOVALUE;
    int _8208 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sorted_result_14867)) {
        _1 = (long)(DBL_PTR(_sorted_result_14867)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_14867)) && (DBL_PTR(_sorted_result_14867)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_14867);
        _sorted_result_14867 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14872);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_14866)){
        _temp_map__14872 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14866)->dbl));
    }
    else{
        _temp_map__14872 = (int)*(((s1_ptr)_2)->base + _the_map_p_14866);
    }
    Ref(_temp_map__14872);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__14872);
    _8208 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__14870);
    _results__14870 = Repeat(0, _8208);
    _8208 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__14871 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14872);
    _8210 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8210, 76)){
        _8210 = NOVALUE;
        goto L1; // [42] 125
    }
    _8210 = NOVALUE;

    /** 		buckets_ = temp_map_[KEY_BUCKETS]*/
    DeRef(_buckets__14868);
    _2 = (int)SEQ_PTR(_temp_map__14872);
    _buckets__14868 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_buckets__14868);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__14868)){
            _8213 = SEQ_PTR(_buckets__14868)->length;
    }
    else {
        _8213 = 1;
    }
    {
        int _index_14881;
        _index_14881 = 1;
L2: 
        if (_index_14881 > _8213){
            goto L3; // [61] 122
        }

        /** 			current_bucket_ = buckets_[index]*/
        DeRef(_current_bucket__14869);
        _2 = (int)SEQ_PTR(_buckets__14868);
        _current_bucket__14869 = (int)*(((s1_ptr)_2)->base + _index_14881);
        Ref(_current_bucket__14869);

        /** 			if length(current_bucket_) > 0 then*/
        if (IS_SEQUENCE(_current_bucket__14869)){
                _8215 = SEQ_PTR(_current_bucket__14869)->length;
        }
        else {
            _8215 = 1;
        }
        if (_8215 <= 0)
        goto L4; // [81] 115

        /** 				results_[pos_ .. pos_ + length(current_bucket_) - 1] = current_bucket_*/
        if (IS_SEQUENCE(_current_bucket__14869)){
                _8217 = SEQ_PTR(_current_bucket__14869)->length;
        }
        else {
            _8217 = 1;
        }
        _8218 = _pos__14871 + _8217;
        if ((long)((unsigned long)_8218 + (unsigned long)HIGH_BITS) >= 0) 
        _8218 = NewDouble((double)_8218);
        _8217 = NOVALUE;
        if (IS_ATOM_INT(_8218)) {
            _8219 = _8218 - 1;
        }
        else {
            _8219 = NewDouble(DBL_PTR(_8218)->dbl - (double)1);
        }
        DeRef(_8218);
        _8218 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__14870;
        AssignSlice(_pos__14871, _8219, _current_bucket__14869);
        DeRef(_8219);
        _8219 = NOVALUE;

        /** 				pos_ += length(current_bucket_)*/
        if (IS_SEQUENCE(_current_bucket__14869)){
                _8220 = SEQ_PTR(_current_bucket__14869)->length;
        }
        else {
            _8220 = 1;
        }
        _pos__14871 = _pos__14871 + _8220;
        _8220 = NOVALUE;
L4: 

        /** 		end for*/
        _index_14881 = _index_14881 + 1;
        goto L2; // [117] 68
L3: 
        ;
    }
    goto L5; // [122] 192
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14872);
    _8222 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8222)){
            _8223 = SEQ_PTR(_8222)->length;
    }
    else {
        _8223 = 1;
    }
    _8222 = NOVALUE;
    {
        int _index_14894;
        _index_14894 = 1;
L6: 
        if (_index_14894 > _8223){
            goto L7; // [136] 191
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__14872);
        _8224 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_8224);
        _8225 = (int)*(((s1_ptr)_2)->base + _index_14894);
        _8224 = NOVALUE;
        if (binary_op_a(EQUALS, _8225, 0)){
            _8225 = NOVALUE;
            goto L8; // [155] 184
        }
        _8225 = NOVALUE;

        /** 				results_[pos_] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14872);
        _8227 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_8227);
        _8228 = (int)*(((s1_ptr)_2)->base + _index_14894);
        _8227 = NOVALUE;
        Ref(_8228);
        _2 = (int)SEQ_PTR(_results__14870);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14870 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__14871);
        _1 = *(int *)_2;
        *(int *)_2 = _8228;
        if( _1 != _8228 ){
            DeRef(_1);
        }
        _8228 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__14871 = _pos__14871 + 1;
L8: 

        /** 		end for*/
        _index_14894 = _index_14894 + 1;
        goto L6; // [186] 143
L7: 
        ;
    }
L5: 

    /** 	if sorted_result then*/
    if (_sorted_result_14867 == 0)
    {
        goto L9; // [194] 211
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__14870);
    _8230 = _21sort(_results__14870, 1);
    DeRef(_the_map_p_14866);
    DeRef(_buckets__14868);
    DeRef(_current_bucket__14869);
    DeRefDS(_results__14870);
    DeRef(_temp_map__14872);
    _8222 = NOVALUE;
    return _8230;
    goto LA; // [208] 218
L9: 

    /** 		return results_*/
    DeRef(_the_map_p_14866);
    DeRef(_buckets__14868);
    DeRef(_current_bucket__14869);
    DeRef(_temp_map__14872);
    _8222 = NOVALUE;
    DeRef(_8230);
    _8230 = NOVALUE;
    return _results__14870;
LA: 
    ;
}


int _27values(int _the_map_14909, int _keys_14910, int _default_values_14911)
{
    int _buckets__14935 = NOVALUE;
    int _bucket__14936 = NOVALUE;
    int _results__14937 = NOVALUE;
    int _pos__14938 = NOVALUE;
    int _temp_map__14939 = NOVALUE;
    int _8270 = NOVALUE;
    int _8269 = NOVALUE;
    int _8267 = NOVALUE;
    int _8266 = NOVALUE;
    int _8265 = NOVALUE;
    int _8264 = NOVALUE;
    int _8262 = NOVALUE;
    int _8261 = NOVALUE;
    int _8260 = NOVALUE;
    int _8259 = NOVALUE;
    int _8257 = NOVALUE;
    int _8255 = NOVALUE;
    int _8252 = NOVALUE;
    int _8250 = NOVALUE;
    int _8248 = NOVALUE;
    int _8247 = NOVALUE;
    int _8246 = NOVALUE;
    int _8245 = NOVALUE;
    int _8243 = NOVALUE;
    int _8242 = NOVALUE;
    int _8241 = NOVALUE;
    int _8240 = NOVALUE;
    int _8239 = NOVALUE;
    int _8238 = NOVALUE;
    int _8236 = NOVALUE;
    int _8235 = NOVALUE;
    int _8233 = NOVALUE;
    int _8232 = NOVALUE;
    int _8231 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(keys) then*/
    _8231 = IS_SEQUENCE(_keys_14910);
    if (_8231 == 0)
    {
        _8231 = NOVALUE;
        goto L1; // [6] 116
    }
    else{
        _8231 = NOVALUE;
    }

    /** 		if atom(default_values) then*/
    _8232 = IS_ATOM(_default_values_14911);
    if (_8232 == 0)
    {
        _8232 = NOVALUE;
        goto L2; // [14] 29
    }
    else{
        _8232 = NOVALUE;
    }

    /** 			default_values = repeat(default_values, length(keys))*/
    if (IS_SEQUENCE(_keys_14910)){
            _8233 = SEQ_PTR(_keys_14910)->length;
    }
    else {
        _8233 = 1;
    }
    _0 = _default_values_14911;
    _default_values_14911 = Repeat(_default_values_14911, _8233);
    DeRef(_0);
    _8233 = NOVALUE;
    goto L3; // [26] 70
L2: 

    /** 		elsif length(default_values) < length(keys) then*/
    if (IS_SEQUENCE(_default_values_14911)){
            _8235 = SEQ_PTR(_default_values_14911)->length;
    }
    else {
        _8235 = 1;
    }
    if (IS_SEQUENCE(_keys_14910)){
            _8236 = SEQ_PTR(_keys_14910)->length;
    }
    else {
        _8236 = 1;
    }
    if (_8235 >= _8236)
    goto L4; // [37] 69

    /** 			default_values &= repeat(default_values[$], length(keys) - length(default_values))*/
    if (IS_SEQUENCE(_default_values_14911)){
            _8238 = SEQ_PTR(_default_values_14911)->length;
    }
    else {
        _8238 = 1;
    }
    _2 = (int)SEQ_PTR(_default_values_14911);
    _8239 = (int)*(((s1_ptr)_2)->base + _8238);
    if (IS_SEQUENCE(_keys_14910)){
            _8240 = SEQ_PTR(_keys_14910)->length;
    }
    else {
        _8240 = 1;
    }
    if (IS_SEQUENCE(_default_values_14911)){
            _8241 = SEQ_PTR(_default_values_14911)->length;
    }
    else {
        _8241 = 1;
    }
    _8242 = _8240 - _8241;
    _8240 = NOVALUE;
    _8241 = NOVALUE;
    _8243 = Repeat(_8239, _8242);
    _8239 = NOVALUE;
    _8242 = NOVALUE;
    if (IS_SEQUENCE(_default_values_14911) && IS_ATOM(_8243)) {
    }
    else if (IS_ATOM(_default_values_14911) && IS_SEQUENCE(_8243)) {
        Ref(_default_values_14911);
        Prepend(&_default_values_14911, _8243, _default_values_14911);
    }
    else {
        Concat((object_ptr)&_default_values_14911, _default_values_14911, _8243);
    }
    DeRefDS(_8243);
    _8243 = NOVALUE;
L4: 
L3: 

    /** 		for i = 1 to length(keys) do*/
    if (IS_SEQUENCE(_keys_14910)){
            _8245 = SEQ_PTR(_keys_14910)->length;
    }
    else {
        _8245 = 1;
    }
    {
        int _i_14930;
        _i_14930 = 1;
L5: 
        if (_i_14930 > _8245){
            goto L6; // [75] 109
        }

        /** 			keys[i] = get(the_map, keys[i], default_values[i])*/
        _2 = (int)SEQ_PTR(_keys_14910);
        _8246 = (int)*(((s1_ptr)_2)->base + _i_14930);
        _2 = (int)SEQ_PTR(_default_values_14911);
        _8247 = (int)*(((s1_ptr)_2)->base + _i_14930);
        Ref(_the_map_14909);
        Ref(_8246);
        Ref(_8247);
        _8248 = _27get(_the_map_14909, _8246, _8247);
        _8246 = NOVALUE;
        _8247 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys_14910);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys_14910 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_14930);
        _1 = *(int *)_2;
        *(int *)_2 = _8248;
        if( _1 != _8248 ){
            DeRef(_1);
        }
        _8248 = NOVALUE;

        /** 		end for*/
        _i_14930 = _i_14930 + 1;
        goto L5; // [104] 82
L6: 
        ;
    }

    /** 		return keys*/
    DeRef(_the_map_14909);
    DeRef(_default_values_14911);
    DeRef(_buckets__14935);
    DeRef(_bucket__14936);
    DeRef(_results__14937);
    DeRef(_temp_map__14939);
    return _keys_14910;
L1: 

    /** 	sequence buckets_*/

    /** 	sequence bucket_*/

    /** 	sequence results_*/

    /** 	integer pos_*/

    /** 	sequence temp_map_*/

    /** 	temp_map_ = eumem:ram_space[the_map]*/
    DeRef(_temp_map__14939);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_14909)){
        _temp_map__14939 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_14909)->dbl));
    }
    else{
        _temp_map__14939 = (int)*(((s1_ptr)_2)->base + _the_map_14909);
    }
    Ref(_temp_map__14939);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__14939);
    _8250 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__14937);
    _results__14937 = Repeat(0, _8250);
    _8250 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__14938 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14939);
    _8252 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8252, 76)){
        _8252 = NOVALUE;
        goto L7; // [163] 246
    }
    _8252 = NOVALUE;

    /** 		buckets_ = temp_map_[VALUE_BUCKETS]*/
    DeRef(_buckets__14935);
    _2 = (int)SEQ_PTR(_temp_map__14939);
    _buckets__14935 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_buckets__14935);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__14935)){
            _8255 = SEQ_PTR(_buckets__14935)->length;
    }
    else {
        _8255 = 1;
    }
    {
        int _index_14948;
        _index_14948 = 1;
L8: 
        if (_index_14948 > _8255){
            goto L9; // [182] 243
        }

        /** 			bucket_ = buckets_[index]*/
        DeRef(_bucket__14936);
        _2 = (int)SEQ_PTR(_buckets__14935);
        _bucket__14936 = (int)*(((s1_ptr)_2)->base + _index_14948);
        Ref(_bucket__14936);

        /** 			if length(bucket_) > 0 then*/
        if (IS_SEQUENCE(_bucket__14936)){
                _8257 = SEQ_PTR(_bucket__14936)->length;
        }
        else {
            _8257 = 1;
        }
        if (_8257 <= 0)
        goto LA; // [202] 236

        /** 				results_[pos_ .. pos_ + length(bucket_) - 1] = bucket_*/
        if (IS_SEQUENCE(_bucket__14936)){
                _8259 = SEQ_PTR(_bucket__14936)->length;
        }
        else {
            _8259 = 1;
        }
        _8260 = _pos__14938 + _8259;
        if ((long)((unsigned long)_8260 + (unsigned long)HIGH_BITS) >= 0) 
        _8260 = NewDouble((double)_8260);
        _8259 = NOVALUE;
        if (IS_ATOM_INT(_8260)) {
            _8261 = _8260 - 1;
        }
        else {
            _8261 = NewDouble(DBL_PTR(_8260)->dbl - (double)1);
        }
        DeRef(_8260);
        _8260 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__14937;
        AssignSlice(_pos__14938, _8261, _bucket__14936);
        DeRef(_8261);
        _8261 = NOVALUE;

        /** 				pos_ += length(bucket_)*/
        if (IS_SEQUENCE(_bucket__14936)){
                _8262 = SEQ_PTR(_bucket__14936)->length;
        }
        else {
            _8262 = 1;
        }
        _pos__14938 = _pos__14938 + _8262;
        _8262 = NOVALUE;
LA: 

        /** 		end for*/
        _index_14948 = _index_14948 + 1;
        goto L8; // [238] 189
L9: 
        ;
    }
    goto LB; // [243] 313
L7: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14939);
    _8264 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8264)){
            _8265 = SEQ_PTR(_8264)->length;
    }
    else {
        _8265 = 1;
    }
    _8264 = NOVALUE;
    {
        int _index_14961;
        _index_14961 = 1;
LC: 
        if (_index_14961 > _8265){
            goto LD; // [257] 312
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__14939);
        _8266 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_8266);
        _8267 = (int)*(((s1_ptr)_2)->base + _index_14961);
        _8266 = NOVALUE;
        if (binary_op_a(EQUALS, _8267, 0)){
            _8267 = NOVALUE;
            goto LE; // [276] 305
        }
        _8267 = NOVALUE;

        /** 				results_[pos_] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14939);
        _8269 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_8269);
        _8270 = (int)*(((s1_ptr)_2)->base + _index_14961);
        _8269 = NOVALUE;
        Ref(_8270);
        _2 = (int)SEQ_PTR(_results__14937);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14937 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__14938);
        _1 = *(int *)_2;
        *(int *)_2 = _8270;
        if( _1 != _8270 ){
            DeRef(_1);
        }
        _8270 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__14938 = _pos__14938 + 1;
LE: 

        /** 		end for*/
        _index_14961 = _index_14961 + 1;
        goto LC; // [307] 264
LD: 
        ;
    }
LB: 

    /** 	return results_*/
    DeRef(_the_map_14909);
    DeRef(_keys_14910);
    DeRef(_default_values_14911);
    DeRef(_buckets__14935);
    DeRef(_bucket__14936);
    DeRef(_temp_map__14939);
    _8264 = NOVALUE;
    return _results__14937;
    ;
}


int _27pairs(int _the_map_p_14973, int _sorted_result_14974)
{
    int _key_bucket__14975 = NOVALUE;
    int _value_bucket__14976 = NOVALUE;
    int _results__14977 = NOVALUE;
    int _pos__14978 = NOVALUE;
    int _temp_map__14979 = NOVALUE;
    int _8306 = NOVALUE;
    int _8304 = NOVALUE;
    int _8303 = NOVALUE;
    int _8301 = NOVALUE;
    int _8300 = NOVALUE;
    int _8299 = NOVALUE;
    int _8297 = NOVALUE;
    int _8295 = NOVALUE;
    int _8294 = NOVALUE;
    int _8293 = NOVALUE;
    int _8292 = NOVALUE;
    int _8290 = NOVALUE;
    int _8288 = NOVALUE;
    int _8287 = NOVALUE;
    int _8285 = NOVALUE;
    int _8284 = NOVALUE;
    int _8282 = NOVALUE;
    int _8280 = NOVALUE;
    int _8279 = NOVALUE;
    int _8278 = NOVALUE;
    int _8276 = NOVALUE;
    int _8274 = NOVALUE;
    int _8273 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sorted_result_14974)) {
        _1 = (long)(DBL_PTR(_sorted_result_14974)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_14974)) && (DBL_PTR(_sorted_result_14974)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_14974);
        _sorted_result_14974 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__14979);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_14973)){
        _temp_map__14979 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_14973)->dbl));
    }
    else{
        _temp_map__14979 = (int)*(((s1_ptr)_2)->base + _the_map_p_14973);
    }
    Ref(_temp_map__14979);

    /** 	results_ = repeat({ 0, 0 }, temp_map_[ELEMENT_COUNT])*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _8273 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_temp_map__14979);
    _8274 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__14977);
    _results__14977 = Repeat(_8273, _8274);
    DeRefDS(_8273);
    _8273 = NOVALUE;
    _8274 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__14978 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__14979);
    _8276 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8276, 76)){
        _8276 = NOVALUE;
        goto L1; // [46] 163
    }
    _8276 = NOVALUE;

    /** 		for index = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14979);
    _8278 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8278)){
            _8279 = SEQ_PTR(_8278)->length;
    }
    else {
        _8279 = 1;
    }
    _8278 = NOVALUE;
    {
        int _index_14988;
        _index_14988 = 1;
L2: 
        if (_index_14988 > _8279){
            goto L3; // [61] 160
        }

        /** 			key_bucket_ = temp_map_[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14979);
        _8280 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_key_bucket__14975);
        _2 = (int)SEQ_PTR(_8280);
        _key_bucket__14975 = (int)*(((s1_ptr)_2)->base + _index_14988);
        Ref(_key_bucket__14975);
        _8280 = NOVALUE;

        /** 			value_bucket_ = temp_map_[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__14979);
        _8282 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_bucket__14976);
        _2 = (int)SEQ_PTR(_8282);
        _value_bucket__14976 = (int)*(((s1_ptr)_2)->base + _index_14988);
        Ref(_value_bucket__14976);
        _8282 = NOVALUE;

        /** 			for j = 1 to length(key_bucket_) do*/
        if (IS_SEQUENCE(_key_bucket__14975)){
                _8284 = SEQ_PTR(_key_bucket__14975)->length;
        }
        else {
            _8284 = 1;
        }
        {
            int _j_14996;
            _j_14996 = 1;
L4: 
            if (_j_14996 > _8284){
                goto L5; // [101] 153
            }

            /** 				results_[pos_][1] = key_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__14977);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__14977 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__14978 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_key_bucket__14975);
            _8287 = (int)*(((s1_ptr)_2)->base + _j_14996);
            Ref(_8287);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 1);
            _1 = *(int *)_2;
            *(int *)_2 = _8287;
            if( _1 != _8287 ){
                DeRef(_1);
            }
            _8287 = NOVALUE;
            _8285 = NOVALUE;

            /** 				results_[pos_][2] = value_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__14977);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__14977 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__14978 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_value_bucket__14976);
            _8290 = (int)*(((s1_ptr)_2)->base + _j_14996);
            Ref(_8290);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _8290;
            if( _1 != _8290 ){
                DeRef(_1);
            }
            _8290 = NOVALUE;
            _8288 = NOVALUE;

            /** 				pos_ += 1*/
            _pos__14978 = _pos__14978 + 1;

            /** 			end for*/
            _j_14996 = _j_14996 + 1;
            goto L4; // [148] 108
L5: 
            ;
        }

        /** 		end for*/
        _index_14988 = _index_14988 + 1;
        goto L2; // [155] 68
L3: 
        ;
    }
    goto L6; // [160] 256
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__14979);
    _8292 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8292)){
            _8293 = SEQ_PTR(_8292)->length;
    }
    else {
        _8293 = 1;
    }
    _8292 = NOVALUE;
    {
        int _index_15007;
        _index_15007 = 1;
L7: 
        if (_index_15007 > _8293){
            goto L8; // [174] 255
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__14979);
        _8294 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_8294);
        _8295 = (int)*(((s1_ptr)_2)->base + _index_15007);
        _8294 = NOVALUE;
        if (binary_op_a(EQUALS, _8295, 0)){
            _8295 = NOVALUE;
            goto L9; // [193] 248
        }
        _8295 = NOVALUE;

        /** 				results_[pos_][1] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__14977);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14977 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__14978 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__14979);
        _8299 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_8299);
        _8300 = (int)*(((s1_ptr)_2)->base + _index_15007);
        _8299 = NOVALUE;
        Ref(_8300);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _8300;
        if( _1 != _8300 ){
            DeRef(_1);
        }
        _8300 = NOVALUE;
        _8297 = NOVALUE;

        /** 				results_[pos_][2] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__14977);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__14977 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__14978 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__14979);
        _8303 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_8303);
        _8304 = (int)*(((s1_ptr)_2)->base + _index_15007);
        _8303 = NOVALUE;
        Ref(_8304);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _8304;
        if( _1 != _8304 ){
            DeRef(_1);
        }
        _8304 = NOVALUE;
        _8301 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__14978 = _pos__14978 + 1;
L9: 

        /** 		end for	*/
        _index_15007 = _index_15007 + 1;
        goto L7; // [250] 181
L8: 
        ;
    }
L6: 

    /** 	if sorted_result then*/
    if (_sorted_result_14974 == 0)
    {
        goto LA; // [258] 275
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__14977);
    _8306 = _21sort(_results__14977, 1);
    DeRef(_the_map_p_14973);
    DeRef(_key_bucket__14975);
    DeRef(_value_bucket__14976);
    DeRefDS(_results__14977);
    DeRef(_temp_map__14979);
    _8278 = NOVALUE;
    _8292 = NOVALUE;
    return _8306;
    goto LB; // [272] 282
LA: 

    /** 		return results_*/
    DeRef(_the_map_p_14973);
    DeRef(_key_bucket__14975);
    DeRef(_value_bucket__14976);
    DeRef(_temp_map__14979);
    _8278 = NOVALUE;
    _8292 = NOVALUE;
    DeRef(_8306);
    _8306 = NOVALUE;
    return _results__14977;
LB: 
    ;
}


void _27optimize(int _the_map_p_15028, int _max_p_15029, int _grow_p_15030)
{
    int _stats__15032 = NOVALUE;
    int _next_guess__15033 = NOVALUE;
    int _prev_guess_15034 = NOVALUE;
    int _8327 = NOVALUE;
    int _8326 = NOVALUE;
    int _8324 = NOVALUE;
    int _8323 = NOVALUE;
    int _8322 = NOVALUE;
    int _8321 = NOVALUE;
    int _8320 = NOVALUE;
    int _8318 = NOVALUE;
    int _8316 = NOVALUE;
    int _8315 = NOVALUE;
    int _8314 = NOVALUE;
    int _8313 = NOVALUE;
    int _8309 = NOVALUE;
    int _8308 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_p_15029)) {
        _1 = (long)(DBL_PTR(_max_p_15029)->dbl);
        if (UNIQUE(DBL_PTR(_max_p_15029)) && (DBL_PTR(_max_p_15029)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_p_15029);
        _max_p_15029 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_15028)){
        _8308 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_15028)->dbl));
    }
    else{
        _8308 = (int)*(((s1_ptr)_2)->base + _the_map_p_15028);
    }
    _2 = (int)SEQ_PTR(_8308);
    _8309 = (int)*(((s1_ptr)_2)->base + 4);
    _8308 = NOVALUE;
    if (binary_op_a(NOTEQ, _8309, 76)){
        _8309 = NOVALUE;
        goto L1; // [19] 204
    }
    _8309 = NOVALUE;

    /** 		if grow_p < 1 then*/
    if (binary_op_a(GREATEREQ, _grow_p_15030, 1)){
        goto L2; // [25] 35
    }

    /** 			grow_p = 1.333*/
    RefDS(_8307);
    DeRef(_grow_p_15030);
    _grow_p_15030 = _8307;
L2: 

    /** 		if max_p < 3 then*/
    if (_max_p_15029 >= 3)
    goto L3; // [37] 49

    /** 			max_p = 3*/
    _max_p_15029 = 3;
L3: 

    /** 		next_guess_ = math:max({1, floor(eumem:ram_space[the_map_p][ELEMENT_COUNT] / max_p)})*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_the_map_p_15028)){
        _8313 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_15028)->dbl));
    }
    else{
        _8313 = (int)*(((s1_ptr)_2)->base + _the_map_p_15028);
    }
    _2 = (int)SEQ_PTR(_8313);
    _8314 = (int)*(((s1_ptr)_2)->base + 2);
    _8313 = NOVALUE;
    if (IS_ATOM_INT(_8314)) {
        if (_max_p_15029 > 0 && _8314 >= 0) {
            _8315 = _8314 / _max_p_15029;
        }
        else {
            temp_dbl = floor((double)_8314 / (double)_max_p_15029);
            if (_8314 != MININT)
            _8315 = (long)temp_dbl;
            else
            _8315 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _8314, _max_p_15029);
        _8315 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _8314 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = _8315;
    _8316 = MAKE_SEQ(_1);
    _8315 = NOVALUE;
    _next_guess__15033 = _17max(_8316);
    _8316 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__15033)) {
        _1 = (long)(DBL_PTR(_next_guess__15033)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__15033)) && (DBL_PTR(_next_guess__15033)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__15033);
        _next_guess__15033 = _1;
    }

    /** 		while 1 with entry do*/
    goto L4; // [81] 184
L5: 

    /** 			if stats_[LARGEST_BUCKET] <= max_p then*/
    _2 = (int)SEQ_PTR(_stats__15032);
    _8318 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(GREATER, _8318, _max_p_15029)){
        _8318 = NOVALUE;
        goto L6; // [94] 103
    }
    _8318 = NOVALUE;

    /** 				exit -- Largest is now smaller than the maximum I wanted.*/
    goto L7; // [100] 203
L6: 

    /** 			if stats_[LARGEST_BUCKET] <= (stats_[STDEV_BUCKET]*3 + stats_[AVERAGE_BUCKET]) then*/
    _2 = (int)SEQ_PTR(_stats__15032);
    _8320 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_stats__15032);
    _8321 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_ATOM_INT(_8321)) {
        if (_8321 == (short)_8321)
        _8322 = _8321 * 3;
        else
        _8322 = NewDouble(_8321 * (double)3);
    }
    else {
        _8322 = binary_op(MULTIPLY, _8321, 3);
    }
    _8321 = NOVALUE;
    _2 = (int)SEQ_PTR(_stats__15032);
    _8323 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_8322) && IS_ATOM_INT(_8323)) {
        _8324 = _8322 + _8323;
        if ((long)((unsigned long)_8324 + (unsigned long)HIGH_BITS) >= 0) 
        _8324 = NewDouble((double)_8324);
    }
    else {
        _8324 = binary_op(PLUS, _8322, _8323);
    }
    DeRef(_8322);
    _8322 = NOVALUE;
    _8323 = NOVALUE;
    if (binary_op_a(GREATER, _8320, _8324)){
        _8320 = NOVALUE;
        DeRef(_8324);
        _8324 = NOVALUE;
        goto L8; // [131] 140
    }
    _8320 = NOVALUE;
    DeRef(_8324);
    _8324 = NOVALUE;

    /** 				exit -- Largest is smaller than is statistically expected.*/
    goto L7; // [137] 203
L8: 

    /** 			prev_guess = next_guess_*/
    _prev_guess_15034 = _next_guess__15033;

    /** 			next_guess_ = floor(stats_[NUM_BUCKETS] * grow_p)*/
    _2 = (int)SEQ_PTR(_stats__15032);
    _8326 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_8326) && IS_ATOM_INT(_grow_p_15030)) {
        if (_8326 == (short)_8326 && _grow_p_15030 <= INT15 && _grow_p_15030 >= -INT15)
        _8327 = _8326 * _grow_p_15030;
        else
        _8327 = NewDouble(_8326 * (double)_grow_p_15030);
    }
    else {
        _8327 = binary_op(MULTIPLY, _8326, _grow_p_15030);
    }
    _8326 = NOVALUE;
    if (IS_ATOM_INT(_8327))
    _next_guess__15033 = e_floor(_8327);
    else
    _next_guess__15033 = unary_op(FLOOR, _8327);
    DeRef(_8327);
    _8327 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__15033)) {
        _1 = (long)(DBL_PTR(_next_guess__15033)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__15033)) && (DBL_PTR(_next_guess__15033)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__15033);
        _next_guess__15033 = _1;
    }

    /** 			if prev_guess = next_guess_ then*/
    if (_prev_guess_15034 != _next_guess__15033)
    goto L9; // [168] 181

    /** 				next_guess_ += 1*/
    _next_guess__15033 = _next_guess__15033 + 1;
L9: 

    /** 		entry*/
L4: 

    /** 			rehash(the_map_p, next_guess_)*/
    Ref(_the_map_p_15028);
    _27rehash(_the_map_p_15028, _next_guess__15033);

    /** 			stats_ = statistics(the_map_p)*/
    Ref(_the_map_p_15028);
    _0 = _stats__15032;
    _stats__15032 = _27statistics(_the_map_p_15028);
    DeRef(_0);

    /** 		end while*/
    goto L5; // [200] 84
L7: 
L1: 

    /** end procedure*/
    DeRef(_the_map_p_15028);
    DeRef(_grow_p_15030);
    DeRef(_stats__15032);
    return;
    ;
}


int _27load_map(int _input_file_name_15068)
{
    int _file_handle_15069 = NOVALUE;
    int _line_in_15070 = NOVALUE;
    int _logical_line_15071 = NOVALUE;
    int _has_comment_15072 = NOVALUE;
    int _delim_pos_15073 = NOVALUE;
    int _data_value_15074 = NOVALUE;
    int _data_key_15075 = NOVALUE;
    int _conv_res_15076 = NOVALUE;
    int _new_map_15077 = NOVALUE;
    int _line_conts_15078 = NOVALUE;
    int _value_inlined_value_at_339_15146 = NOVALUE;
    int _value_inlined_value_at_510_15176 = NOVALUE;
    int _in_quote_15211 = NOVALUE;
    int _last_in_15212 = NOVALUE;
    int _cur_in_15213 = NOVALUE;
    int _seek_1__tmp_at925_15241 = NOVALUE;
    int _seek_inlined_seek_at_925_15240 = NOVALUE;
    int _8459 = NOVALUE;
    int _8458 = NOVALUE;
    int _8457 = NOVALUE;
    int _8456 = NOVALUE;
    int _8451 = NOVALUE;
    int _8449 = NOVALUE;
    int _8448 = NOVALUE;
    int _8446 = NOVALUE;
    int _8445 = NOVALUE;
    int _8444 = NOVALUE;
    int _8443 = NOVALUE;
    int _8435 = NOVALUE;
    int _8433 = NOVALUE;
    int _8426 = NOVALUE;
    int _8425 = NOVALUE;
    int _8424 = NOVALUE;
    int _8423 = NOVALUE;
    int _8419 = NOVALUE;
    int _8418 = NOVALUE;
    int _8414 = NOVALUE;
    int _8413 = NOVALUE;
    int _8410 = NOVALUE;
    int _8409 = NOVALUE;
    int _8407 = NOVALUE;
    int _8399 = NOVALUE;
    int _8398 = NOVALUE;
    int _8397 = NOVALUE;
    int _8396 = NOVALUE;
    int _8395 = NOVALUE;
    int _8394 = NOVALUE;
    int _8393 = NOVALUE;
    int _8392 = NOVALUE;
    int _8390 = NOVALUE;
    int _8389 = NOVALUE;
    int _8388 = NOVALUE;
    int _8385 = NOVALUE;
    int _8384 = NOVALUE;
    int _8382 = NOVALUE;
    int _8380 = NOVALUE;
    int _8379 = NOVALUE;
    int _8367 = NOVALUE;
    int _8365 = NOVALUE;
    int _8364 = NOVALUE;
    int _8359 = NOVALUE;
    int _8356 = NOVALUE;
    int _8352 = NOVALUE;
    int _8349 = NOVALUE;
    int _8346 = NOVALUE;
    int _8345 = NOVALUE;
    int _8341 = NOVALUE;
    int _8339 = NOVALUE;
    int _8333 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence line_conts =   ",${"*/
    RefDS(_8332);
    DeRefi(_line_conts_15078);
    _line_conts_15078 = _8332;

    /** 	if sequence(input_file_name) then*/
    _8333 = IS_SEQUENCE(_input_file_name_15068);
    if (_8333 == 0)
    {
        _8333 = NOVALUE;
        goto L1; // [13] 28
    }
    else{
        _8333 = NOVALUE;
    }

    /** 		file_handle = open(input_file_name, "rb")*/
    _file_handle_15069 = EOpen(_input_file_name_15068, _904, 0);
    goto L2; // [25] 38
L1: 

    /** 		file_handle = input_file_name*/
    Ref(_input_file_name_15068);
    _file_handle_15069 = _input_file_name_15068;
    if (!IS_ATOM_INT(_file_handle_15069)) {
        _1 = (long)(DBL_PTR(_file_handle_15069)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle_15069)) && (DBL_PTR(_file_handle_15069)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle_15069);
        _file_handle_15069 = _1;
    }
L2: 

    /** 	if file_handle = -1 then*/
    if (_file_handle_15069 != -1)
    goto L3; // [42] 53

    /** 		return -1*/
    DeRef(_input_file_name_15068);
    DeRef(_line_in_15070);
    DeRef(_logical_line_15071);
    DeRef(_data_value_15074);
    DeRef(_data_key_15075);
    DeRef(_conv_res_15076);
    DeRef(_new_map_15077);
    DeRefi(_line_conts_15078);
    return -1;
L3: 

    /** 	new_map = new(threshold_size) -- Assume a small map initially.*/
    _0 = _new_map_15077;
    _new_map_15077 = _27new(_27threshold_size_14110);
    DeRef(_0);

    /** 	for i = 1 to 10 do*/
    {
        int _i_15088;
        _i_15088 = 1;
L4: 
        if (_i_15088 > 10){
            goto L5; // [63] 126
        }

        /** 		delim_pos = getc(file_handle)*/
        if (_file_handle_15069 != last_r_file_no) {
            last_r_file_ptr = which_file(_file_handle_15069, EF_READ);
            last_r_file_no = _file_handle_15069;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _delim_pos_15073 = getKBchar();
            }
            else
            _delim_pos_15073 = getc(last_r_file_ptr);
        }
        else
        _delim_pos_15073 = getc(last_r_file_ptr);

        /** 		if delim_pos = -1 then */
        if (_delim_pos_15073 != -1)
        goto L6; // [79] 88

        /** 			exit*/
        goto L5; // [85] 126
L6: 

        /** 		if not t_print(delim_pos) then */
        _8339 = _3t_print(_delim_pos_15073);
        if (IS_ATOM_INT(_8339)) {
            if (_8339 != 0){
                DeRef(_8339);
                _8339 = NOVALUE;
                goto L7; // [94] 112
            }
        }
        else {
            if (DBL_PTR(_8339)->dbl != 0.0){
                DeRef(_8339);
                _8339 = NOVALUE;
                goto L7; // [94] 112
            }
        }
        DeRef(_8339);
        _8339 = NOVALUE;

        /** 	    	if not t_space(delim_pos) then*/
        _8341 = _3t_space(_delim_pos_15073);
        if (IS_ATOM_INT(_8341)) {
            if (_8341 != 0){
                DeRef(_8341);
                _8341 = NOVALUE;
                goto L8; // [103] 111
            }
        }
        else {
            if (DBL_PTR(_8341)->dbl != 0.0){
                DeRef(_8341);
                _8341 = NOVALUE;
                goto L8; // [103] 111
            }
        }
        DeRef(_8341);
        _8341 = NOVALUE;

        /** 	    		exit*/
        goto L5; // [108] 126
L8: 
L7: 

        /** 	    delim_pos = -1*/
        _delim_pos_15073 = -1;

        /** 	end for*/
        _i_15088 = _i_15088 + 1;
        goto L4; // [121] 70
L5: 
        ;
    }

    /** 	if delim_pos = -1 then*/
    if (_delim_pos_15073 != -1)
    goto L9; // [130] 921

    /** 		close(file_handle)*/
    EClose(_file_handle_15069);

    /** 		file_handle = open(input_file_name, "r")*/
    _file_handle_15069 = EOpen(_input_file_name_15068, _846, 0);

    /** 		while sequence(logical_line) with entry do*/
    goto LA; // [149] 574
LB: 
    _8345 = IS_SEQUENCE(_logical_line_15071);
    if (_8345 == 0)
    {
        _8345 = NOVALUE;
        goto LC; // [157] 1062
    }
    else{
        _8345 = NOVALUE;
    }

    /** 			logical_line = search:match_replace({-2}, logical_line, "\\#3D")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    _8346 = MAKE_SEQ(_1);
    Ref(_logical_line_15071);
    RefDS(_8347);
    _0 = _logical_line_15071;
    _logical_line_15071 = _5match_replace(_8346, _logical_line_15071, _8347, 0);
    DeRef(_0);
    _8346 = NOVALUE;

    /** 			logical_line = search:match_replace({-3}, logical_line, "\\#23")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -3;
    _8349 = MAKE_SEQ(_1);
    Ref(_logical_line_15071);
    RefDS(_8350);
    _0 = _logical_line_15071;
    _logical_line_15071 = _5match_replace(_8349, _logical_line_15071, _8350, 0);
    DeRef(_0);
    _8349 = NOVALUE;

    /** 			logical_line = search:match_replace({-4}, logical_line, "\\#24")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -4;
    _8352 = MAKE_SEQ(_1);
    Ref(_logical_line_15071);
    RefDS(_8353);
    _0 = _logical_line_15071;
    _logical_line_15071 = _5match_replace(_8352, _logical_line_15071, _8353, 0);
    DeRef(_0);
    _8352 = NOVALUE;

    /** 			logical_line = search:match_replace({-5}, logical_line, "\\#2C")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -5;
    _8356 = MAKE_SEQ(_1);
    Ref(_logical_line_15071);
    RefDS(_8357);
    _0 = _logical_line_15071;
    _logical_line_15071 = _5match_replace(_8356, _logical_line_15071, _8357, 0);
    DeRef(_0);
    _8356 = NOVALUE;

    /** 			logical_line = search:match_replace({-6}, logical_line, "\\-")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -6;
    _8359 = MAKE_SEQ(_1);
    Ref(_logical_line_15071);
    RefDS(_8360);
    _0 = _logical_line_15071;
    _logical_line_15071 = _5match_replace(_8359, _logical_line_15071, _8360, 0);
    DeRef(_0);
    _8359 = NOVALUE;

    /** 			delim_pos = find('=', logical_line)*/
    _delim_pos_15073 = find_from(61, _logical_line_15071, 1);

    /** 			if delim_pos > 0 then*/
    if (_delim_pos_15073 <= 0)
    goto LD; // [236] 571

    /** 				data_key = text:trim(logical_line[1..delim_pos-1])*/
    _8364 = _delim_pos_15073 - 1;
    rhs_slice_target = (object_ptr)&_8365;
    RHS_Slice(_logical_line_15071, 1, _8364);
    RefDS(_4282);
    _0 = _data_key_15075;
    _data_key_15075 = _6trim(_8365, _4282, 0);
    DeRef(_0);
    _8365 = NOVALUE;

    /** 				if length(data_key) > 0 then*/
    if (IS_SEQUENCE(_data_key_15075)){
            _8367 = SEQ_PTR(_data_key_15075)->length;
    }
    else {
        _8367 = 1;
    }
    if (_8367 <= 0)
    goto LE; // [262] 570

    /** 					data_key = search:match_replace("\\#2C", data_key, ",")*/
    RefDS(_8357);
    Ref(_data_key_15075);
    RefDS(_8369);
    _0 = _data_key_15075;
    _data_key_15075 = _5match_replace(_8357, _data_key_15075, _8369, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#24", data_key, "$")*/
    RefDS(_8353);
    Ref(_data_key_15075);
    RefDS(_8371);
    _0 = _data_key_15075;
    _data_key_15075 = _5match_replace(_8353, _data_key_15075, _8371, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#22", data_key, "\"")*/
    RefDS(_8373);
    Ref(_data_key_15075);
    RefDS(_5289);
    _0 = _data_key_15075;
    _data_key_15075 = _5match_replace(_8373, _data_key_15075, _5289, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#3D", data_key, "=")*/
    RefDS(_8347);
    Ref(_data_key_15075);
    RefDS(_2792);
    _0 = _data_key_15075;
    _data_key_15075 = _5match_replace(_8347, _data_key_15075, _2792, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#23", data_key, "#")*/
    RefDS(_8350);
    Ref(_data_key_15075);
    RefDS(_270);
    _0 = _data_key_15075;
    _data_key_15075 = _5match_replace(_8350, _data_key_15075, _270, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\-", data_key, "-")*/
    RefDS(_8360);
    Ref(_data_key_15075);
    RefDS(_8377);
    _0 = _data_key_15075;
    _data_key_15075 = _5match_replace(_8360, _data_key_15075, _8377, 0);
    DeRef(_0);

    /** 					if not t_alpha(data_key[1]) then*/
    _2 = (int)SEQ_PTR(_data_key_15075);
    _8379 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_8379);
    _8380 = _3t_alpha(_8379);
    _8379 = NOVALUE;
    if (IS_ATOM_INT(_8380)) {
        if (_8380 != 0){
            DeRef(_8380);
            _8380 = NOVALUE;
            goto LF; // [330] 386
        }
    }
    else {
        if (DBL_PTR(_8380)->dbl != 0.0){
            DeRef(_8380);
            _8380 = NOVALUE;
            goto LF; // [330] 386
        }
    }
    DeRef(_8380);
    _8380 = NOVALUE;

    /** 						conv_res = stdget:value(data_key,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_2714)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_2714)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_2714)) && (DBL_PTR(_14GET_LONG_ANSWER_2714)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_2714);
        _14GET_LONG_ANSWER_2714 = _1;
    }
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_2714)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_2714)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_2714)) && (DBL_PTR(_14GET_LONG_ANSWER_2714)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_2714);
        _14GET_LONG_ANSWER_2714 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_key_15075);
    _0 = _conv_res_15076;
    _conv_res_15076 = _14get_value(_data_key_15075, 1, _14GET_LONG_ANSWER_2714);
    DeRef(_0);

    /** 						if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_15076);
    _8382 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _8382, 0)){
        _8382 = NOVALUE;
        goto L10; // [360] 385
    }
    _8382 = NOVALUE;

    /** 							if conv_res[3] = length(data_key) then*/
    _2 = (int)SEQ_PTR(_conv_res_15076);
    _8384 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_key_15075)){
            _8385 = SEQ_PTR(_data_key_15075)->length;
    }
    else {
        _8385 = 1;
    }
    if (binary_op_a(NOTEQ, _8384, _8385)){
        _8384 = NOVALUE;
        _8385 = NOVALUE;
        goto L11; // [373] 384
    }
    _8384 = NOVALUE;
    _8385 = NOVALUE;

    /** 								data_key = conv_res[2]*/
    DeRef(_data_key_15075);
    _2 = (int)SEQ_PTR(_conv_res_15076);
    _data_key_15075 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_key_15075);
L11: 
L10: 
LF: 

    /** 					data_value = text:trim(logical_line[delim_pos+1..$])*/
    _8388 = _delim_pos_15073 + 1;
    if (_8388 > MAXINT){
        _8388 = NewDouble((double)_8388);
    }
    if (IS_SEQUENCE(_logical_line_15071)){
            _8389 = SEQ_PTR(_logical_line_15071)->length;
    }
    else {
        _8389 = 1;
    }
    rhs_slice_target = (object_ptr)&_8390;
    RHS_Slice(_logical_line_15071, _8388, _8389);
    RefDS(_4282);
    _0 = _data_value_15074;
    _data_value_15074 = _6trim(_8390, _4282, 0);
    DeRef(_0);
    _8390 = NOVALUE;

    /** 					if data_value[1] = '\"' and data_value[$] = '\"' then*/
    _2 = (int)SEQ_PTR(_data_value_15074);
    _8392 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_8392)) {
        _8393 = (_8392 == 34);
    }
    else {
        _8393 = binary_op(EQUALS, _8392, 34);
    }
    _8392 = NOVALUE;
    if (IS_ATOM_INT(_8393)) {
        if (_8393 == 0) {
            goto L12; // [416] 450
        }
    }
    else {
        if (DBL_PTR(_8393)->dbl == 0.0) {
            goto L12; // [416] 450
        }
    }
    if (IS_SEQUENCE(_data_value_15074)){
            _8395 = SEQ_PTR(_data_value_15074)->length;
    }
    else {
        _8395 = 1;
    }
    _2 = (int)SEQ_PTR(_data_value_15074);
    _8396 = (int)*(((s1_ptr)_2)->base + _8395);
    if (IS_ATOM_INT(_8396)) {
        _8397 = (_8396 == 34);
    }
    else {
        _8397 = binary_op(EQUALS, _8396, 34);
    }
    _8396 = NOVALUE;
    if (_8397 == 0) {
        DeRef(_8397);
        _8397 = NOVALUE;
        goto L12; // [432] 450
    }
    else {
        if (!IS_ATOM_INT(_8397) && DBL_PTR(_8397)->dbl == 0.0){
            DeRef(_8397);
            _8397 = NOVALUE;
            goto L12; // [432] 450
        }
        DeRef(_8397);
        _8397 = NOVALUE;
    }
    DeRef(_8397);
    _8397 = NOVALUE;

    /** 					    data_value = data_value[2..$-1]*/
    if (IS_SEQUENCE(_data_value_15074)){
            _8398 = SEQ_PTR(_data_value_15074)->length;
    }
    else {
        _8398 = 1;
    }
    _8399 = _8398 - 1;
    _8398 = NOVALUE;
    rhs_slice_target = (object_ptr)&_data_value_15074;
    RHS_Slice(_data_value_15074, 2, _8399);
L12: 

    /** 					data_value = search:match_replace("\\#2C", data_value, ",")*/
    RefDS(_8357);
    Ref(_data_value_15074);
    RefDS(_8369);
    _0 = _data_value_15074;
    _data_value_15074 = _5match_replace(_8357, _data_value_15074, _8369, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#24", data_value, "$")*/
    RefDS(_8353);
    Ref(_data_value_15074);
    RefDS(_8371);
    _0 = _data_value_15074;
    _data_value_15074 = _5match_replace(_8353, _data_value_15074, _8371, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#22", data_value, "\"")*/
    RefDS(_8373);
    Ref(_data_value_15074);
    RefDS(_5289);
    _0 = _data_value_15074;
    _data_value_15074 = _5match_replace(_8373, _data_value_15074, _5289, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#3D", data_value, "=")*/
    RefDS(_8347);
    Ref(_data_value_15074);
    RefDS(_2792);
    _0 = _data_value_15074;
    _data_value_15074 = _5match_replace(_8347, _data_value_15074, _2792, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#23", data_value, "#")*/
    RefDS(_8350);
    Ref(_data_value_15074);
    RefDS(_270);
    _0 = _data_value_15074;
    _data_value_15074 = _5match_replace(_8350, _data_value_15074, _270, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\-", data_value, "-")*/
    RefDS(_8360);
    Ref(_data_value_15074);
    RefDS(_8377);
    _0 = _data_value_15074;
    _data_value_15074 = _5match_replace(_8360, _data_value_15074, _8377, 0);
    DeRef(_0);

    /** 					conv_res = stdget:value(data_value,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_2714)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_2714)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_2714)) && (DBL_PTR(_14GET_LONG_ANSWER_2714)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_2714);
        _14GET_LONG_ANSWER_2714 = _1;
    }
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_2714)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_2714)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_2714)) && (DBL_PTR(_14GET_LONG_ANSWER_2714)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_2714);
        _14GET_LONG_ANSWER_2714 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_value_15074);
    _0 = _conv_res_15076;
    _conv_res_15076 = _14get_value(_data_value_15074, 1, _14GET_LONG_ANSWER_2714);
    DeRef(_0);

    /** 					if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_15076);
    _8407 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _8407, 0)){
        _8407 = NOVALUE;
        goto L13; // [531] 556
    }
    _8407 = NOVALUE;

    /** 						if conv_res[3] = length(data_value) then*/
    _2 = (int)SEQ_PTR(_conv_res_15076);
    _8409 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_value_15074)){
            _8410 = SEQ_PTR(_data_value_15074)->length;
    }
    else {
        _8410 = 1;
    }
    if (binary_op_a(NOTEQ, _8409, _8410)){
        _8409 = NOVALUE;
        _8410 = NOVALUE;
        goto L14; // [544] 555
    }
    _8409 = NOVALUE;
    _8410 = NOVALUE;

    /** 							data_value = conv_res[2]*/
    DeRef(_data_value_15074);
    _2 = (int)SEQ_PTR(_conv_res_15076);
    _data_value_15074 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_value_15074);
L14: 
L13: 

    /** 					put(new_map, data_key, data_value)*/
    Ref(_new_map_15077);
    Ref(_data_key_15075);
    Ref(_data_value_15074);
    _27put(_new_map_15077, _data_key_15075, _data_value_15074, 1, _27threshold_size_14110);
LE: 
LD: 

    /** 		entry*/
LA: 

    /** 			logical_line = -1*/
    DeRef(_logical_line_15071);
    _logical_line_15071 = -1;

    /** 			while sequence(line_in) with entry do*/
    goto L15; // [581] 708
L16: 
    _8413 = IS_SEQUENCE(_line_in_15070);
    if (_8413 == 0)
    {
        _8413 = NOVALUE;
        goto LB; // [589] 152
    }
    else{
        _8413 = NOVALUE;
    }

    /** 				if atom(logical_line) then*/
    _8414 = IS_ATOM(_logical_line_15071);
    if (_8414 == 0)
    {
        _8414 = NOVALUE;
        goto L17; // [597] 606
    }
    else{
        _8414 = NOVALUE;
    }

    /** 					logical_line = ""*/
    RefDS(_5);
    DeRef(_logical_line_15071);
    _logical_line_15071 = _5;
L17: 

    /** 				has_comment = match("--", line_in)*/
    _has_comment_15072 = e_match_from(_8415, _line_in_15070, 1);

    /** 				if has_comment != 0 then*/
    if (_has_comment_15072 == 0)
    goto L18; // [617] 641

    /** 					line_in = text:trim(line_in[1..has_comment-1])*/
    _8418 = _has_comment_15072 - 1;
    rhs_slice_target = (object_ptr)&_8419;
    RHS_Slice(_line_in_15070, 1, _8418);
    RefDS(_4282);
    _0 = _line_in_15070;
    _line_in_15070 = _6trim(_8419, _4282, 0);
    DeRef(_0);
    _8419 = NOVALUE;
    goto L19; // [638] 650
L18: 

    /** 					line_in = text:trim(line_in)*/
    Ref(_line_in_15070);
    RefDS(_4282);
    _0 = _line_in_15070;
    _line_in_15070 = _6trim(_line_in_15070, _4282, 0);
    DeRef(_0);
L19: 

    /** 				logical_line &= line_in*/
    if (IS_SEQUENCE(_logical_line_15071) && IS_ATOM(_line_in_15070)) {
        Ref(_line_in_15070);
        Append(&_logical_line_15071, _logical_line_15071, _line_in_15070);
    }
    else if (IS_ATOM(_logical_line_15071) && IS_SEQUENCE(_line_in_15070)) {
        Ref(_logical_line_15071);
        Prepend(&_logical_line_15071, _line_in_15070, _logical_line_15071);
    }
    else {
        Concat((object_ptr)&_logical_line_15071, _logical_line_15071, _line_in_15070);
    }

    /** 				if length(line_in) then*/
    if (IS_SEQUENCE(_line_in_15070)){
            _8423 = SEQ_PTR(_line_in_15070)->length;
    }
    else {
        _8423 = 1;
    }
    if (_8423 == 0)
    {
        _8423 = NOVALUE;
        goto L1A; // [661] 705
    }
    else{
        _8423 = NOVALUE;
    }

    /** 					if not find(line_in[$], line_conts) then*/
    if (IS_SEQUENCE(_line_in_15070)){
            _8424 = SEQ_PTR(_line_in_15070)->length;
    }
    else {
        _8424 = 1;
    }
    _2 = (int)SEQ_PTR(_line_in_15070);
    _8425 = (int)*(((s1_ptr)_2)->base + _8424);
    _8426 = find_from(_8425, _line_conts_15078, 1);
    _8425 = NOVALUE;
    if (_8426 != 0)
    goto L1B; // [678] 704
    _8426 = NOVALUE;

    /** 						logical_line = search:match_replace(`",$"`, logical_line, "")*/
    RefDS(_8428);
    RefDS(_logical_line_15071);
    RefDS(_5);
    _0 = _logical_line_15071;
    _logical_line_15071 = _5match_replace(_8428, _logical_line_15071, _5, 0);
    DeRefDS(_0);

    /** 						logical_line = search:match_replace(`,$`, logical_line, "")*/
    RefDS(_8430);
    Ref(_logical_line_15071);
    RefDS(_5);
    _0 = _logical_line_15071;
    _logical_line_15071 = _5match_replace(_8430, _logical_line_15071, _5, 0);
    DeRef(_0);

    /** 						exit*/
    goto LB; // [701] 152
L1B: 
L1A: 

    /** 			entry*/
L15: 

    /** 				line_in = gets(file_handle)*/
    DeRef(_line_in_15070);
    _line_in_15070 = EGets(_file_handle_15069);

    /**                 integer in_quote = 0, last_in = -1, cur_in = -1*/
    _in_quote_15211 = 0;
    _last_in_15212 = -1;
    _cur_in_15213 = -1;

    /**                 if not equal(line_in, -1) then*/
    if (_line_in_15070 == -1)
    _8433 = 1;
    else if (IS_ATOM_INT(_line_in_15070) && IS_ATOM_INT(-1))
    _8433 = 0;
    else
    _8433 = (compare(_line_in_15070, -1) == 0);
    if (_8433 != 0)
    goto L1C; // [736] 906
    _8433 = NOVALUE;

    /**                     for i = 1 to length(line_in) do*/
    if (IS_SEQUENCE(_line_in_15070)){
            _8435 = SEQ_PTR(_line_in_15070)->length;
    }
    else {
        _8435 = 1;
    }
    {
        int _i_15218;
        _i_15218 = 1;
L1D: 
        if (_i_15218 > _8435){
            goto L1E; // [744] 905
        }

        /**                         cur_in = line_in[i]*/
        _2 = (int)SEQ_PTR(_line_in_15070);
        _cur_in_15213 = (int)*(((s1_ptr)_2)->base + _i_15218);
        if (!IS_ATOM_INT(_cur_in_15213)){
            _cur_in_15213 = (long)DBL_PTR(_cur_in_15213)->dbl;
        }

        /**                         if cur_in = '"' then*/
        if (_cur_in_15213 != 34)
        goto L1F; // [763] 777

        /**                             in_quote = not in_quote*/
        _in_quote_15211 = (_in_quote_15211 == 0);
        goto L20; // [774] 885
L1F: 

        /**                         elsif in_quote then*/
        if (_in_quote_15211 == 0)
        {
            goto L21; // [779] 884
        }
        else{
        }

        /**                             if cur_in = '=' then*/
        if (_cur_in_15213 != 61)
        goto L22; // [784] 798

        /**                                 cur_in = -2 */
        _cur_in_15213 = -2;
        goto L23; // [795] 883
L22: 

        /**                             elsif cur_in = '#' then*/
        if (_cur_in_15213 != 35)
        goto L24; // [800] 814

        /**                                 cur_in = -3 */
        _cur_in_15213 = -3;
        goto L23; // [811] 883
L24: 

        /**                             elsif cur_in = '$' then*/
        if (_cur_in_15213 != 36)
        goto L25; // [816] 830

        /**                                 cur_in = -4*/
        _cur_in_15213 = -4;
        goto L23; // [827] 883
L25: 

        /**                             elsif cur_in = ',' then*/
        if (_cur_in_15213 != 44)
        goto L26; // [832] 846

        /**                                 cur_in = -5*/
        _cur_in_15213 = -5;
        goto L23; // [843] 883
L26: 

        /**                             elsif cur_in = '-' and last_in = '-' then*/
        _8443 = (_cur_in_15213 == 45);
        if (_8443 == 0) {
            goto L27; // [852] 882
        }
        _8445 = (_last_in_15212 == 45);
        if (_8445 == 0)
        {
            DeRef(_8445);
            _8445 = NOVALUE;
            goto L27; // [861] 882
        }
        else{
            DeRef(_8445);
            _8445 = NOVALUE;
        }

        /**                                 cur_in = -6*/
        _cur_in_15213 = -6;

        /**                                 line_in[i-1] = -6*/
        _8446 = _i_15218 - 1;
        _2 = (int)SEQ_PTR(_line_in_15070);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_15070 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _8446);
        _1 = *(int *)_2;
        *(int *)_2 = -6;
        DeRef(_1);
L27: 
L23: 
L21: 
L20: 

        /**                         line_in[i] = cur_in*/
        _2 = (int)SEQ_PTR(_line_in_15070);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_15070 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15218);
        _1 = *(int *)_2;
        *(int *)_2 = _cur_in_15213;
        DeRef(_1);

        /**                         last_in = cur_in*/
        _last_in_15212 = _cur_in_15213;

        /**                     end for*/
        _i_15218 = _i_15218 + 1;
        goto L1D; // [900] 751
L1E: 
        ;
    }
L1C: 

    /** 			end while*/
    goto L16; // [910] 584

    /** 		end while*/
    goto LB; // [915] 152
    goto LC; // [918] 1062
L9: 

    /** 		io:seek(file_handle, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at925_15241);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_handle_15069;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at925_15241 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_925_15240 = machine(19, _seek_1__tmp_at925_15241);
    DeRefi(_seek_1__tmp_at925_15241);
    _seek_1__tmp_at925_15241 = NOVALUE;

    /** 		line_in  = serialize:deserialize(file_handle)*/
    _0 = _line_in_15070;
    _line_in_15070 = _24deserialize(_file_handle_15069, 1);
    DeRef(_0);

    /** 		if atom(line_in) then*/
    _8448 = IS_ATOM(_line_in_15070);
    if (_8448 == 0)
    {
        _8448 = NOVALUE;
        goto L28; // [948] 958
    }
    else{
        _8448 = NOVALUE;
    }

    /** 			return -2*/
    DeRef(_input_file_name_15068);
    DeRef(_line_in_15070);
    DeRef(_logical_line_15071);
    DeRef(_data_value_15074);
    DeRef(_data_key_15075);
    DeRef(_conv_res_15076);
    DeRef(_new_map_15077);
    DeRefi(_line_conts_15078);
    DeRef(_8364);
    _8364 = NOVALUE;
    DeRef(_8388);
    _8388 = NOVALUE;
    DeRef(_8399);
    _8399 = NOVALUE;
    DeRef(_8393);
    _8393 = NOVALUE;
    DeRef(_8418);
    _8418 = NOVALUE;
    DeRef(_8443);
    _8443 = NOVALUE;
    DeRef(_8446);
    _8446 = NOVALUE;
    return -2;
L28: 

    /** 		if length(line_in) > 1 then*/
    if (IS_SEQUENCE(_line_in_15070)){
            _8449 = SEQ_PTR(_line_in_15070)->length;
    }
    else {
        _8449 = 1;
    }
    if (_8449 <= 1)
    goto L29; // [963] 1054

    /** 			switch line_in[1] do*/
    _2 = (int)SEQ_PTR(_line_in_15070);
    _8451 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_8451) ){
        goto L2A; // [973] 1042
    }
    if(!IS_ATOM_INT(_8451)){
        if( (DBL_PTR(_8451)->dbl != (double) ((int) DBL_PTR(_8451)->dbl) ) ){
            goto L2A; // [973] 1042
        }
        _0 = (int) DBL_PTR(_8451)->dbl;
    }
    else {
        _0 = _8451;
    };
    _8451 = NOVALUE;
    switch ( _0 ){ 

        /** 				case 1, 2 then*/
        case 1:
        case 2:

        /** 					data_key   = serialize:deserialize(file_handle)*/
        _0 = _data_key_15075;
        _data_key_15075 = _24deserialize(_file_handle_15069, 1);
        DeRef(_0);

        /** 					data_value =  serialize:deserialize(file_handle)*/
        _0 = _data_value_15074;
        _data_value_15074 = _24deserialize(_file_handle_15069, 1);
        DeRef(_0);

        /** 					for i = 1 to length(data_key) do*/
        if (IS_SEQUENCE(_data_key_15075)){
                _8456 = SEQ_PTR(_data_key_15075)->length;
        }
        else {
            _8456 = 1;
        }
        {
            int _i_15255;
            _i_15255 = 1;
L2B: 
            if (_i_15255 > _8456){
                goto L2C; // [1003] 1038
            }

            /** 						put(new_map, data_key[i], data_value[i])*/
            _2 = (int)SEQ_PTR(_data_key_15075);
            _8457 = (int)*(((s1_ptr)_2)->base + _i_15255);
            _2 = (int)SEQ_PTR(_data_value_15074);
            _8458 = (int)*(((s1_ptr)_2)->base + _i_15255);
            Ref(_new_map_15077);
            Ref(_8457);
            Ref(_8458);
            _27put(_new_map_15077, _8457, _8458, 1, _27threshold_size_14110);
            _8457 = NOVALUE;
            _8458 = NOVALUE;

            /** 					end for*/
            _i_15255 = _i_15255 + 1;
            goto L2B; // [1033] 1010
L2C: 
            ;
        }
        goto L2D; // [1038] 1061

        /** 				case else*/
        default:
L2A: 

        /** 					return -2*/
        DeRef(_input_file_name_15068);
        DeRef(_line_in_15070);
        DeRef(_logical_line_15071);
        DeRef(_data_value_15074);
        DeRef(_data_key_15075);
        DeRef(_conv_res_15076);
        DeRef(_new_map_15077);
        DeRefi(_line_conts_15078);
        DeRef(_8364);
        _8364 = NOVALUE;
        DeRef(_8388);
        _8388 = NOVALUE;
        DeRef(_8399);
        _8399 = NOVALUE;
        DeRef(_8393);
        _8393 = NOVALUE;
        DeRef(_8418);
        _8418 = NOVALUE;
        DeRef(_8443);
        _8443 = NOVALUE;
        DeRef(_8446);
        _8446 = NOVALUE;
        return -2;
    ;}    goto L2D; // [1051] 1061
L29: 

    /** 			return -2*/
    DeRef(_input_file_name_15068);
    DeRef(_line_in_15070);
    DeRef(_logical_line_15071);
    DeRef(_data_value_15074);
    DeRef(_data_key_15075);
    DeRef(_conv_res_15076);
    DeRef(_new_map_15077);
    DeRefi(_line_conts_15078);
    DeRef(_8364);
    _8364 = NOVALUE;
    DeRef(_8388);
    _8388 = NOVALUE;
    DeRef(_8399);
    _8399 = NOVALUE;
    DeRef(_8393);
    _8393 = NOVALUE;
    DeRef(_8418);
    _8418 = NOVALUE;
    DeRef(_8443);
    _8443 = NOVALUE;
    DeRef(_8446);
    _8446 = NOVALUE;
    return -2;
L2D: 
LC: 

    /** 	if sequence(input_file_name) then*/
    _8459 = IS_SEQUENCE(_input_file_name_15068);
    if (_8459 == 0)
    {
        _8459 = NOVALUE;
        goto L2E; // [1067] 1075
    }
    else{
        _8459 = NOVALUE;
    }

    /** 		close(file_handle)*/
    EClose(_file_handle_15069);
L2E: 

    /** 	optimize(new_map)*/
    Ref(_new_map_15077);
    RefDS(_8307);
    _27optimize(_new_map_15077, _27threshold_size_14110, _8307);

    /** 	return new_map*/
    DeRef(_input_file_name_15068);
    DeRef(_line_in_15070);
    DeRef(_logical_line_15071);
    DeRef(_data_value_15074);
    DeRef(_data_key_15075);
    DeRef(_conv_res_15076);
    DeRefi(_line_conts_15078);
    DeRef(_8364);
    _8364 = NOVALUE;
    DeRef(_8388);
    _8388 = NOVALUE;
    DeRef(_8399);
    _8399 = NOVALUE;
    DeRef(_8393);
    _8393 = NOVALUE;
    DeRef(_8418);
    _8418 = NOVALUE;
    DeRef(_8443);
    _8443 = NOVALUE;
    DeRef(_8446);
    _8446 = NOVALUE;
    return _new_map_15077;
    ;
}


int _27save_map(int _the_map__15268, int _file_name_p_15269, int _type__15270)
{
    int _file_handle__15271 = NOVALUE;
    int _keys__15272 = NOVALUE;
    int _values__15273 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_188_15306 = NOVALUE;
    int _options_inlined_pretty_sprint_at_185_15305 = NOVALUE;
    int _x_inlined_pretty_sprint_at_182_15304 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_344_15324 = NOVALUE;
    int _options_inlined_pretty_sprint_at_341_15323 = NOVALUE;
    int _x_inlined_pretty_sprint_at_338_15322 = NOVALUE;
    int _8511 = NOVALUE;
    int _8510 = NOVALUE;
    int _8509 = NOVALUE;
    int _8508 = NOVALUE;
    int _8507 = NOVALUE;
    int _8505 = NOVALUE;
    int _8504 = NOVALUE;
    int _8503 = NOVALUE;
    int _8502 = NOVALUE;
    int _8501 = NOVALUE;
    int _8500 = NOVALUE;
    int _8499 = NOVALUE;
    int _8498 = NOVALUE;
    int _8497 = NOVALUE;
    int _8496 = NOVALUE;
    int _8495 = NOVALUE;
    int _8494 = NOVALUE;
    int _8493 = NOVALUE;
    int _8492 = NOVALUE;
    int _8491 = NOVALUE;
    int _8490 = NOVALUE;
    int _8489 = NOVALUE;
    int _8488 = NOVALUE;
    int _8487 = NOVALUE;
    int _8486 = NOVALUE;
    int _8485 = NOVALUE;
    int _8484 = NOVALUE;
    int _8483 = NOVALUE;
    int _8482 = NOVALUE;
    int _8481 = NOVALUE;
    int _8480 = NOVALUE;
    int _8479 = NOVALUE;
    int _8478 = NOVALUE;
    int _8477 = NOVALUE;
    int _8476 = NOVALUE;
    int _8475 = NOVALUE;
    int _8474 = NOVALUE;
    int _8473 = NOVALUE;
    int _8472 = NOVALUE;
    int _8471 = NOVALUE;
    int _8469 = NOVALUE;
    int _8461 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_type__15270)) {
        _1 = (long)(DBL_PTR(_type__15270)->dbl);
        if (UNIQUE(DBL_PTR(_type__15270)) && (DBL_PTR(_type__15270)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type__15270);
        _type__15270 = _1;
    }

    /** 	integer file_handle_ = -2*/
    _file_handle__15271 = -2;

    /** 	if sequence(file_name_p) then*/
    _8461 = IS_SEQUENCE(_file_name_p_15269);
    if (_8461 == 0)
    {
        _8461 = NOVALUE;
        goto L1; // [17] 53
    }
    else{
        _8461 = NOVALUE;
    }

    /** 		if type_ = SM_TEXT then*/
    if (_type__15270 != 1)
    goto L2; // [24] 40

    /** 			file_handle_ = open(file_name_p, "w")*/
    _file_handle__15271 = EOpen(_file_name_p_15269, _889, 0);
    goto L3; // [37] 63
L2: 

    /** 			file_handle_ = open(file_name_p, "wb")*/
    _file_handle__15271 = EOpen(_file_name_p_15269, _949, 0);
    goto L3; // [50] 63
L1: 

    /** 		file_handle_ = file_name_p*/
    Ref(_file_name_p_15269);
    _file_handle__15271 = _file_name_p_15269;
    if (!IS_ATOM_INT(_file_handle__15271)) {
        _1 = (long)(DBL_PTR(_file_handle__15271)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle__15271)) && (DBL_PTR(_file_handle__15271)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle__15271);
        _file_handle__15271 = _1;
    }
L3: 

    /** 	if file_handle_ < 0 then*/
    if (_file_handle__15271 >= 0)
    goto L4; // [65] 76

    /** 		return -1*/
    DeRef(_the_map__15268);
    DeRef(_file_name_p_15269);
    DeRef(_keys__15272);
    DeRef(_values__15273);
    return -1;
L4: 

    /** 	keys_ = keys(the_map_)*/
    Ref(_the_map__15268);
    _0 = _keys__15272;
    _keys__15272 = _27keys(_the_map__15268, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    Ref(_the_map__15268);
    _0 = _values__15273;
    _values__15273 = _27values(_the_map__15268, 0, 0);
    DeRef(_0);

    /** 	if type_ = SM_RAW then*/
    if (_type__15270 != 2)
    goto L5; // [99] 151

    /** 		puts(file_handle_, serialize:serialize({*/
    _8469 = _9now_gmt();
    RefDS(_8470);
    _8471 = _9format(_8469, _8470);
    _8469 = NOVALUE;
    _8472 = _31version_string(0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = _8471;
    *((int *)(_2+12)) = _8472;
    _8473 = MAKE_SEQ(_1);
    _8472 = NOVALUE;
    _8471 = NOVALUE;
    _8474 = _24serialize(_8473);
    _8473 = NOVALUE;
    EPuts(_file_handle__15271, _8474); // DJP 
    DeRef(_8474);
    _8474 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(keys_))*/
    RefDS(_keys__15272);
    _8475 = _24serialize(_keys__15272);
    EPuts(_file_handle__15271, _8475); // DJP 
    DeRef(_8475);
    _8475 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(values_))*/
    RefDS(_values__15273);
    _8476 = _24serialize(_values__15273);
    EPuts(_file_handle__15271, _8476); // DJP 
    DeRef(_8476);
    _8476 = NOVALUE;
    goto L6; // [148] 501
L5: 

    /** 		for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__15272)){
            _8477 = SEQ_PTR(_keys__15272)->length;
    }
    else {
        _8477 = 1;
    }
    {
        int _i_15299;
        _i_15299 = 1;
L7: 
        if (_i_15299 > _8477){
            goto L8; // [156] 500
        }

        /** 			keys_[i] = pretty:pretty_sprint(keys_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8478 = (int)*(((s1_ptr)_2)->base + _i_15299);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_1681);
        *((int *)(_2+20)) = _1681;
        RefDS(_5773);
        *((int *)(_2+24)) = _5773;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _8479 = MAKE_SEQ(_1);
        Ref(_8478);
        DeRef(_x_inlined_pretty_sprint_at_182_15304);
        _x_inlined_pretty_sprint_at_182_15304 = _8478;
        _8478 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_185_15305);
        _options_inlined_pretty_sprint_at_185_15305 = _8479;
        _8479 = NOVALUE;

        /** 	pretty_printing = 0*/
        _23pretty_printing_8147 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_182_15304);
        RefDS(_options_inlined_pretty_sprint_at_185_15305);
        _23pretty(_x_inlined_pretty_sprint_at_182_15304, _options_inlined_pretty_sprint_at_185_15305);

        /** 	return pretty_line*/
        RefDS(_23pretty_line_8150);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_188_15306);
        _pretty_sprint_inlined_pretty_sprint_at_188_15306 = _23pretty_line_8150;
        DeRef(_x_inlined_pretty_sprint_at_182_15304);
        _x_inlined_pretty_sprint_at_182_15304 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_185_15305);
        _options_inlined_pretty_sprint_at_185_15305 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_188_15306);
        _2 = (int)SEQ_PTR(_keys__15272);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__15272 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_188_15306;
        DeRef(_1);

        /** 			keys_[i] = search:match_replace("#", keys_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8480 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_270);
        Ref(_8480);
        RefDS(_8350);
        _8481 = _5match_replace(_270, _8480, _8350, 0);
        _8480 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__15272);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__15272 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8481;
        if( _1 != _8481 ){
            DeRef(_1);
        }
        _8481 = NOVALUE;

        /** 			keys_[i] = search:match_replace("\"", keys_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8482 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_5289);
        Ref(_8482);
        RefDS(_8373);
        _8483 = _5match_replace(_5289, _8482, _8373, 0);
        _8482 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__15272);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__15272 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8483;
        if( _1 != _8483 ){
            DeRef(_1);
        }
        _8483 = NOVALUE;

        /** 			keys_[i] = search:match_replace("$", keys_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8484 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_8371);
        Ref(_8484);
        RefDS(_8353);
        _8485 = _5match_replace(_8371, _8484, _8353, 0);
        _8484 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__15272);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__15272 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8485;
        if( _1 != _8485 ){
            DeRef(_1);
        }
        _8485 = NOVALUE;

        /** 			keys_[i] = search:match_replace(",", keys_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8486 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_8369);
        Ref(_8486);
        RefDS(_8357);
        _8487 = _5match_replace(_8369, _8486, _8357, 0);
        _8486 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__15272);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__15272 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8487;
        if( _1 != _8487 ){
            DeRef(_1);
        }
        _8487 = NOVALUE;

        /** 			keys_[i] = search:match_replace("=", keys_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8488 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_2792);
        Ref(_8488);
        RefDS(_8347);
        _8489 = _5match_replace(_2792, _8488, _8347, 0);
        _8488 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__15272);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__15272 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8489;
        if( _1 != _8489 ){
            DeRef(_1);
        }
        _8489 = NOVALUE;

        /** 			keys_[i] = search:match_replace("-", keys_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8490 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_8377);
        Ref(_8490);
        RefDS(_8360);
        _8491 = _5match_replace(_8377, _8490, _8360, 0);
        _8490 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__15272);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__15272 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8491;
        if( _1 != _8491 ){
            DeRef(_1);
        }
        _8491 = NOVALUE;

        /** 			values_[i] = pretty:pretty_sprint(values_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_values__15273);
        _8492 = (int)*(((s1_ptr)_2)->base + _i_15299);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_1681);
        *((int *)(_2+20)) = _1681;
        RefDS(_5773);
        *((int *)(_2+24)) = _5773;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _8493 = MAKE_SEQ(_1);
        Ref(_8492);
        DeRef(_x_inlined_pretty_sprint_at_338_15322);
        _x_inlined_pretty_sprint_at_338_15322 = _8492;
        _8492 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_341_15323);
        _options_inlined_pretty_sprint_at_341_15323 = _8493;
        _8493 = NOVALUE;

        /** 	pretty_printing = 0*/
        _23pretty_printing_8147 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_338_15322);
        RefDS(_options_inlined_pretty_sprint_at_341_15323);
        _23pretty(_x_inlined_pretty_sprint_at_338_15322, _options_inlined_pretty_sprint_at_341_15323);

        /** 	return pretty_line*/
        RefDS(_23pretty_line_8150);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_344_15324);
        _pretty_sprint_inlined_pretty_sprint_at_344_15324 = _23pretty_line_8150;
        DeRef(_x_inlined_pretty_sprint_at_338_15322);
        _x_inlined_pretty_sprint_at_338_15322 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_341_15323);
        _options_inlined_pretty_sprint_at_341_15323 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_344_15324);
        _2 = (int)SEQ_PTR(_values__15273);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__15273 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_344_15324;
        DeRef(_1);

        /** 			values_[i] = search:match_replace("#", values_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_values__15273);
        _8494 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_270);
        Ref(_8494);
        RefDS(_8350);
        _8495 = _5match_replace(_270, _8494, _8350, 0);
        _8494 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__15273);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__15273 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8495;
        if( _1 != _8495 ){
            DeRef(_1);
        }
        _8495 = NOVALUE;

        /** 			values_[i] = search:match_replace("\"", values_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_values__15273);
        _8496 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_5289);
        Ref(_8496);
        RefDS(_8373);
        _8497 = _5match_replace(_5289, _8496, _8373, 0);
        _8496 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__15273);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__15273 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8497;
        if( _1 != _8497 ){
            DeRef(_1);
        }
        _8497 = NOVALUE;

        /** 			values_[i] = search:match_replace("$", values_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_values__15273);
        _8498 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_8371);
        Ref(_8498);
        RefDS(_8353);
        _8499 = _5match_replace(_8371, _8498, _8353, 0);
        _8498 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__15273);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__15273 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8499;
        if( _1 != _8499 ){
            DeRef(_1);
        }
        _8499 = NOVALUE;

        /** 			values_[i] = search:match_replace(",", values_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_values__15273);
        _8500 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_8369);
        Ref(_8500);
        RefDS(_8357);
        _8501 = _5match_replace(_8369, _8500, _8357, 0);
        _8500 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__15273);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__15273 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8501;
        if( _1 != _8501 ){
            DeRef(_1);
        }
        _8501 = NOVALUE;

        /** 			values_[i] = search:match_replace("=", values_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_values__15273);
        _8502 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_2792);
        Ref(_8502);
        RefDS(_8347);
        _8503 = _5match_replace(_2792, _8502, _8347, 0);
        _8502 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__15273);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__15273 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8503;
        if( _1 != _8503 ){
            DeRef(_1);
        }
        _8503 = NOVALUE;

        /** 			values_[i] = search:match_replace("-", values_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_values__15273);
        _8504 = (int)*(((s1_ptr)_2)->base + _i_15299);
        RefDS(_8377);
        Ref(_8504);
        RefDS(_8360);
        _8505 = _5match_replace(_8377, _8504, _8360, 0);
        _8504 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__15273);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__15273 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15299);
        _1 = *(int *)_2;
        *(int *)_2 = _8505;
        if( _1 != _8505 ){
            DeRef(_1);
        }
        _8505 = NOVALUE;

        /** 			printf(file_handle_, "%s = %s\n", {keys_[i], values_[i]})*/
        _2 = (int)SEQ_PTR(_keys__15272);
        _8507 = (int)*(((s1_ptr)_2)->base + _i_15299);
        _2 = (int)SEQ_PTR(_values__15273);
        _8508 = (int)*(((s1_ptr)_2)->base + _i_15299);
        Ref(_8508);
        Ref(_8507);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _8507;
        ((int *)_2)[2] = _8508;
        _8509 = MAKE_SEQ(_1);
        _8508 = NOVALUE;
        _8507 = NOVALUE;
        EPrintf(_file_handle__15271, _8506, _8509);
        DeRefDS(_8509);
        _8509 = NOVALUE;

        /** 		end for*/
        _i_15299 = _i_15299 + 1;
        goto L7; // [495] 163
L8: 
        ;
    }
L6: 

    /** 	if sequence(file_name_p) then*/
    _8510 = IS_SEQUENCE(_file_name_p_15269);
    if (_8510 == 0)
    {
        _8510 = NOVALUE;
        goto L9; // [506] 514
    }
    else{
        _8510 = NOVALUE;
    }

    /** 		close(file_handle_)*/
    EClose(_file_handle__15271);
L9: 

    /** 	return length(keys_)*/
    if (IS_SEQUENCE(_keys__15272)){
            _8511 = SEQ_PTR(_keys__15272)->length;
    }
    else {
        _8511 = 1;
    }
    DeRef(_the_map__15268);
    DeRef(_file_name_p_15269);
    DeRefDS(_keys__15272);
    DeRef(_values__15273);
    return _8511;
    ;
}


int _27copy(int _source_map_15346, int _dest_map_15347, int _put_operation_15348)
{
    int _keys_set_15351 = NOVALUE;
    int _value_set_15352 = NOVALUE;
    int _source_data_15353 = NOVALUE;
    int _temp_map_15385 = NOVALUE;
    int _8535 = NOVALUE;
    int _8533 = NOVALUE;
    int _8532 = NOVALUE;
    int _8531 = NOVALUE;
    int _8530 = NOVALUE;
    int _8528 = NOVALUE;
    int _8527 = NOVALUE;
    int _8526 = NOVALUE;
    int _8525 = NOVALUE;
    int _8524 = NOVALUE;
    int _8523 = NOVALUE;
    int _8522 = NOVALUE;
    int _8520 = NOVALUE;
    int _8518 = NOVALUE;
    int _8517 = NOVALUE;
    int _8516 = NOVALUE;
    int _8514 = NOVALUE;
    int _8512 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_put_operation_15348)) {
        _1 = (long)(DBL_PTR(_put_operation_15348)->dbl);
        if (UNIQUE(DBL_PTR(_put_operation_15348)) && (DBL_PTR(_put_operation_15348)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_put_operation_15348);
        _put_operation_15348 = _1;
    }

    /** 	if map(dest_map) then*/
    Ref(_dest_map_15347);
    _8512 = _27map(_dest_map_15347);
    if (_8512 == 0) {
        DeRef(_8512);
        _8512 = NOVALUE;
        goto L1; // [11] 221
    }
    else {
        if (!IS_ATOM_INT(_8512) && DBL_PTR(_8512)->dbl == 0.0){
            DeRef(_8512);
            _8512 = NOVALUE;
            goto L1; // [11] 221
        }
        DeRef(_8512);
        _8512 = NOVALUE;
    }
    DeRef(_8512);
    _8512 = NOVALUE;

    /** 		sequence keys_set*/

    /** 		sequence value_set		*/

    /** 		sequence source_data*/

    /** 		source_data = eumem:ram_space[source_map]	*/
    DeRef(_source_data_15353);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_source_map_15346)){
        _source_data_15353 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_15346)->dbl));
    }
    else{
        _source_data_15353 = (int)*(((s1_ptr)_2)->base + _source_map_15346);
    }
    Ref(_source_data_15353);

    /** 		if source_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_source_data_15353);
    _8514 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _8514, 76)){
        _8514 = NOVALUE;
        goto L2; // [38] 136
    }
    _8514 = NOVALUE;

    /** 			for index = 1 to length(source_data[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_source_data_15353);
    _8516 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_8516)){
            _8517 = SEQ_PTR(_8516)->length;
    }
    else {
        _8517 = 1;
    }
    _8516 = NOVALUE;
    {
        int _index_15359;
        _index_15359 = 1;
L3: 
        if (_index_15359 > _8517){
            goto L4; // [53] 133
        }

        /** 				keys_set = source_data[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_15353);
        _8518 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_keys_set_15351);
        _2 = (int)SEQ_PTR(_8518);
        _keys_set_15351 = (int)*(((s1_ptr)_2)->base + _index_15359);
        Ref(_keys_set_15351);
        _8518 = NOVALUE;

        /** 				value_set = source_data[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_15353);
        _8520 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_set_15352);
        _2 = (int)SEQ_PTR(_8520);
        _value_set_15352 = (int)*(((s1_ptr)_2)->base + _index_15359);
        Ref(_value_set_15352);
        _8520 = NOVALUE;

        /** 				for j = 1 to length(keys_set) do*/
        if (IS_SEQUENCE(_keys_set_15351)){
                _8522 = SEQ_PTR(_keys_set_15351)->length;
        }
        else {
            _8522 = 1;
        }
        {
            int _j_15367;
            _j_15367 = 1;
L5: 
            if (_j_15367 > _8522){
                goto L6; // [93] 126
            }

            /** 					put(dest_map, keys_set[j], value_set[j], put_operation)*/
            _2 = (int)SEQ_PTR(_keys_set_15351);
            _8523 = (int)*(((s1_ptr)_2)->base + _j_15367);
            _2 = (int)SEQ_PTR(_value_set_15352);
            _8524 = (int)*(((s1_ptr)_2)->base + _j_15367);
            Ref(_dest_map_15347);
            Ref(_8523);
            Ref(_8524);
            _27put(_dest_map_15347, _8523, _8524, _put_operation_15348, _27threshold_size_14110);
            _8523 = NOVALUE;
            _8524 = NOVALUE;

            /** 				end for*/
            _j_15367 = _j_15367 + 1;
            goto L5; // [121] 100
L6: 
            ;
        }

        /** 			end for*/
        _index_15359 = _index_15359 + 1;
        goto L3; // [128] 60
L4: 
        ;
    }
    goto L7; // [133] 210
L2: 

    /** 			for index = 1 to length(source_data[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_source_data_15353);
    _8525 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8525)){
            _8526 = SEQ_PTR(_8525)->length;
    }
    else {
        _8526 = 1;
    }
    _8525 = NOVALUE;
    {
        int _index_15373;
        _index_15373 = 1;
L8: 
        if (_index_15373 > _8526){
            goto L9; // [147] 209
        }

        /** 				if source_data[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_source_data_15353);
        _8527 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_8527);
        _8528 = (int)*(((s1_ptr)_2)->base + _index_15373);
        _8527 = NOVALUE;
        if (binary_op_a(EQUALS, _8528, 0)){
            _8528 = NOVALUE;
            goto LA; // [166] 202
        }
        _8528 = NOVALUE;

        /** 					put(dest_map, source_data[KEY_LIST][index], */
        _2 = (int)SEQ_PTR(_source_data_15353);
        _8530 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_8530);
        _8531 = (int)*(((s1_ptr)_2)->base + _index_15373);
        _8530 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_data_15353);
        _8532 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_8532);
        _8533 = (int)*(((s1_ptr)_2)->base + _index_15373);
        _8532 = NOVALUE;
        Ref(_dest_map_15347);
        Ref(_8531);
        Ref(_8533);
        _27put(_dest_map_15347, _8531, _8533, _put_operation_15348, _27threshold_size_14110);
        _8531 = NOVALUE;
        _8533 = NOVALUE;
LA: 

        /** 			end for*/
        _index_15373 = _index_15373 + 1;
        goto L8; // [204] 154
L9: 
        ;
    }
L7: 

    /** 		return dest_map*/
    DeRef(_keys_set_15351);
    DeRef(_value_set_15352);
    DeRef(_source_data_15353);
    DeRef(_source_map_15346);
    _8516 = NOVALUE;
    _8525 = NOVALUE;
    return _dest_map_15347;
    goto LB; // [218] 251
L1: 

    /** 		atom temp_map = eumem:malloc()*/
    _0 = _temp_map_15385;
    _temp_map_15385 = _28malloc(1, 1);
    DeRef(_0);

    /** 	 	eumem:ram_space[temp_map] = eumem:ram_space[source_map]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_source_map_15346)){
        _8535 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_15346)->dbl));
    }
    else{
        _8535 = (int)*(((s1_ptr)_2)->base + _source_map_15346);
    }
    Ref(_8535);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map_15385))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map_15385)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map_15385);
    _1 = *(int *)_2;
    *(int *)_2 = _8535;
    if( _1 != _8535 ){
        DeRef(_1);
    }
    _8535 = NOVALUE;

    /** 		return temp_map*/
    DeRef(_source_map_15346);
    DeRef(_dest_map_15347);
    _8516 = NOVALUE;
    _8525 = NOVALUE;
    return _temp_map_15385;
    DeRef(_temp_map_15385);
    _temp_map_15385 = NOVALUE;
LB: 
    ;
}


int _27new_from_kvpairs(int _kv_pairs_15390)
{
    int _new_map_15391 = NOVALUE;
    int _8547 = NOVALUE;
    int _8546 = NOVALUE;
    int _8545 = NOVALUE;
    int _8544 = NOVALUE;
    int _8542 = NOVALUE;
    int _8541 = NOVALUE;
    int _8540 = NOVALUE;
    int _8538 = NOVALUE;
    int _8537 = NOVALUE;
    int _8536 = NOVALUE;
    int _0, _1, _2;
    

    /** 	new_map = new( floor(7 * length(kv_pairs) / 2) )*/
    if (IS_SEQUENCE(_kv_pairs_15390)){
            _8536 = SEQ_PTR(_kv_pairs_15390)->length;
    }
    else {
        _8536 = 1;
    }
    if (_8536 <= INT15)
    _8537 = 7 * _8536;
    else
    _8537 = NewDouble(7 * (double)_8536);
    _8536 = NOVALUE;
    if (IS_ATOM_INT(_8537)) {
        _8538 = _8537 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _8537, 2);
        _8538 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_8537);
    _8537 = NOVALUE;
    _0 = _new_map_15391;
    _new_map_15391 = _27new(_8538);
    DeRef(_0);
    _8538 = NOVALUE;

    /** 	for i = 1 to length(kv_pairs) do*/
    if (IS_SEQUENCE(_kv_pairs_15390)){
            _8540 = SEQ_PTR(_kv_pairs_15390)->length;
    }
    else {
        _8540 = 1;
    }
    {
        int _i_15397;
        _i_15397 = 1;
L1: 
        if (_i_15397 > _8540){
            goto L2; // [25] 82
        }

        /** 		if length(kv_pairs[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_kv_pairs_15390);
        _8541 = (int)*(((s1_ptr)_2)->base + _i_15397);
        if (IS_SEQUENCE(_8541)){
                _8542 = SEQ_PTR(_8541)->length;
        }
        else {
            _8542 = 1;
        }
        _8541 = NOVALUE;
        if (_8542 != 2)
        goto L3; // [41] 75

        /** 			put(new_map, kv_pairs[i][1], kv_pairs[i][2])*/
        _2 = (int)SEQ_PTR(_kv_pairs_15390);
        _8544 = (int)*(((s1_ptr)_2)->base + _i_15397);
        _2 = (int)SEQ_PTR(_8544);
        _8545 = (int)*(((s1_ptr)_2)->base + 1);
        _8544 = NOVALUE;
        _2 = (int)SEQ_PTR(_kv_pairs_15390);
        _8546 = (int)*(((s1_ptr)_2)->base + _i_15397);
        _2 = (int)SEQ_PTR(_8546);
        _8547 = (int)*(((s1_ptr)_2)->base + 2);
        _8546 = NOVALUE;
        Ref(_new_map_15391);
        Ref(_8545);
        Ref(_8547);
        _27put(_new_map_15391, _8545, _8547, 1, _27threshold_size_14110);
        _8545 = NOVALUE;
        _8547 = NOVALUE;
L3: 

        /** 	end for*/
        _i_15397 = _i_15397 + 1;
        goto L1; // [77] 32
L2: 
        ;
    }

    /** 	return new_map	*/
    DeRefDS(_kv_pairs_15390);
    _8541 = NOVALUE;
    return _new_map_15391;
    ;
}


int _27new_from_string(int _kv_string_15409)
{
    int _8549 = NOVALUE;
    int _8548 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return new_from_kvpairs( text:keyvalues (kv_string) )*/
    RefDS(_kv_string_15409);
    RefDS(_5112);
    RefDS(_5113);
    RefDS(_5114);
    RefDS(_100);
    _8548 = _6keyvalues(_kv_string_15409, _5112, _5113, _5114, _100, 1);
    _8549 = _27new_from_kvpairs(_8548);
    _8548 = NOVALUE;
    DeRefDS(_kv_string_15409);
    return _8549;
    ;
}


int _27for_each(int _source_map_15414, int _user_rid_15415, int _user_data_15416, int _in_sorted_order_15417, int _signal_boundary_15418)
{
    int _lKV_15419 = NOVALUE;
    int _lRes_15420 = NOVALUE;
    int _progress_code_15421 = NOVALUE;
    int _8568 = NOVALUE;
    int _8566 = NOVALUE;
    int _8565 = NOVALUE;
    int _8564 = NOVALUE;
    int _8563 = NOVALUE;
    int _8562 = NOVALUE;
    int _8560 = NOVALUE;
    int _8559 = NOVALUE;
    int _8558 = NOVALUE;
    int _8557 = NOVALUE;
    int _8556 = NOVALUE;
    int _8555 = NOVALUE;
    int _8554 = NOVALUE;
    int _8553 = NOVALUE;
    int _8552 = NOVALUE;
    int _8551 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_user_rid_15415)) {
        _1 = (long)(DBL_PTR(_user_rid_15415)->dbl);
        if (UNIQUE(DBL_PTR(_user_rid_15415)) && (DBL_PTR(_user_rid_15415)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_user_rid_15415);
        _user_rid_15415 = _1;
    }
    if (!IS_ATOM_INT(_in_sorted_order_15417)) {
        _1 = (long)(DBL_PTR(_in_sorted_order_15417)->dbl);
        if (UNIQUE(DBL_PTR(_in_sorted_order_15417)) && (DBL_PTR(_in_sorted_order_15417)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_in_sorted_order_15417);
        _in_sorted_order_15417 = _1;
    }
    if (!IS_ATOM_INT(_signal_boundary_15418)) {
        _1 = (long)(DBL_PTR(_signal_boundary_15418)->dbl);
        if (UNIQUE(DBL_PTR(_signal_boundary_15418)) && (DBL_PTR(_signal_boundary_15418)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_signal_boundary_15418);
        _signal_boundary_15418 = _1;
    }

    /** 	lKV = pairs(source_map, in_sorted_order)	*/
    Ref(_source_map_15414);
    _0 = _lKV_15419;
    _lKV_15419 = _27pairs(_source_map_15414, _in_sorted_order_15417);
    DeRef(_0);

    /** 	if length(lKV) = 0 and signal_boundary != 0 then*/
    if (IS_SEQUENCE(_lKV_15419)){
            _8551 = SEQ_PTR(_lKV_15419)->length;
    }
    else {
        _8551 = 1;
    }
    _8552 = (_8551 == 0);
    _8551 = NOVALUE;
    if (_8552 == 0) {
        goto L1; // [31] 61
    }
    _8554 = (_signal_boundary_15418 != 0);
    if (_8554 == 0)
    {
        DeRef(_8554);
        _8554 = NOVALUE;
        goto L1; // [40] 61
    }
    else{
        DeRef(_8554);
        _8554 = NOVALUE;
    }

    /** 		return call_func(user_rid, {0,0,user_data,0} )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    Ref(_user_data_15416);
    *((int *)(_2+12)) = _user_data_15416;
    *((int *)(_2+16)) = 0;
    _8555 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_8555);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_user_rid_15415].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    Ref(*(int *)(_2+16));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12), 
                        *(int *)(_2+16)
                         );
    DeRef(_8556);
    _8556 = _1;
    DeRefDS(_8555);
    _8555 = NOVALUE;
    DeRef(_source_map_15414);
    DeRef(_user_data_15416);
    DeRef(_lKV_15419);
    DeRef(_lRes_15420);
    DeRef(_8552);
    _8552 = NOVALUE;
    return _8556;
L1: 

    /** 	for i = 1 to length(lKV) do*/
    if (IS_SEQUENCE(_lKV_15419)){
            _8557 = SEQ_PTR(_lKV_15419)->length;
    }
    else {
        _8557 = 1;
    }
    {
        int _i_15431;
        _i_15431 = 1;
L2: 
        if (_i_15431 > _8557){
            goto L3; // [66] 164
        }

        /** 		if i = length(lKV) and signal_boundary then*/
        if (IS_SEQUENCE(_lKV_15419)){
                _8558 = SEQ_PTR(_lKV_15419)->length;
        }
        else {
            _8558 = 1;
        }
        _8559 = (_i_15431 == _8558);
        _8558 = NOVALUE;
        if (_8559 == 0) {
            goto L4; // [82] 102
        }
        if (_signal_boundary_15418 == 0)
        {
            goto L4; // [87] 102
        }
        else{
        }

        /** 			progress_code = -i*/
        _progress_code_15421 = - _i_15431;
        goto L5; // [99] 110
L4: 

        /** 			progress_code = i*/
        _progress_code_15421 = _i_15431;
L5: 

        /** 		lRes = call_func(user_rid, {lKV[i][1], lKV[i][2], user_data, progress_code})*/
        _2 = (int)SEQ_PTR(_lKV_15419);
        _8562 = (int)*(((s1_ptr)_2)->base + _i_15431);
        _2 = (int)SEQ_PTR(_8562);
        _8563 = (int)*(((s1_ptr)_2)->base + 1);
        _8562 = NOVALUE;
        _2 = (int)SEQ_PTR(_lKV_15419);
        _8564 = (int)*(((s1_ptr)_2)->base + _i_15431);
        _2 = (int)SEQ_PTR(_8564);
        _8565 = (int)*(((s1_ptr)_2)->base + 2);
        _8564 = NOVALUE;
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_8563);
        *((int *)(_2+4)) = _8563;
        Ref(_8565);
        *((int *)(_2+8)) = _8565;
        Ref(_user_data_15416);
        *((int *)(_2+12)) = _user_data_15416;
        *((int *)(_2+16)) = _progress_code_15421;
        _8566 = MAKE_SEQ(_1);
        _8565 = NOVALUE;
        _8563 = NOVALUE;
        _1 = (int)SEQ_PTR(_8566);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_user_rid_15415].addr;
        Ref(*(int *)(_2+4));
        Ref(*(int *)(_2+8));
        Ref(*(int *)(_2+12));
        Ref(*(int *)(_2+16));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4), 
                            *(int *)(_2+8), 
                            *(int *)(_2+12), 
                            *(int *)(_2+16)
                             );
        DeRef(_lRes_15420);
        _lRes_15420 = _1;
        DeRefDS(_8566);
        _8566 = NOVALUE;

        /** 		if not equal(lRes, 0) then*/
        if (_lRes_15420 == 0)
        _8568 = 1;
        else if (IS_ATOM_INT(_lRes_15420) && IS_ATOM_INT(0))
        _8568 = 0;
        else
        _8568 = (compare(_lRes_15420, 0) == 0);
        if (_8568 != 0)
        goto L6; // [147] 157
        _8568 = NOVALUE;

        /** 			return lRes*/
        DeRef(_source_map_15414);
        DeRef(_user_data_15416);
        DeRefDS(_lKV_15419);
        DeRef(_8552);
        _8552 = NOVALUE;
        DeRef(_8559);
        _8559 = NOVALUE;
        DeRef(_8556);
        _8556 = NOVALUE;
        return _lRes_15420;
L6: 

        /** 	end for*/
        _i_15431 = _i_15431 + 1;
        goto L2; // [159] 73
L3: 
        ;
    }

    /** 	return 0*/
    DeRef(_source_map_15414);
    DeRef(_user_data_15416);
    DeRef(_lKV_15419);
    DeRef(_lRes_15420);
    DeRef(_8552);
    _8552 = NOVALUE;
    DeRef(_8559);
    _8559 = NOVALUE;
    DeRef(_8556);
    _8556 = NOVALUE;
    return 0;
    ;
}


void _27convert_to_large_map(int _the_map__15450)
{
    int _temp_map__15451 = NOVALUE;
    int _map_handle__15452 = NOVALUE;
    int _8581 = NOVALUE;
    int _8580 = NOVALUE;
    int _8579 = NOVALUE;
    int _8578 = NOVALUE;
    int _8577 = NOVALUE;
    int _8575 = NOVALUE;
    int _8574 = NOVALUE;
    int _8573 = NOVALUE;
    int _8572 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_]*/
    DeRef(_temp_map__15451);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    _temp_map__15451 = (int)*(((s1_ptr)_2)->base + _the_map__15450);
    Ref(_temp_map__15451);

    /** 	map_handle_ = new()*/
    _0 = _map_handle__15452;
    _map_handle__15452 = _27new(690);
    DeRef(_0);

    /** 	for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__15451);
    _8572 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_8572)){
            _8573 = SEQ_PTR(_8572)->length;
    }
    else {
        _8573 = 1;
    }
    _8572 = NOVALUE;
    {
        int _index_15456;
        _index_15456 = 1;
L1: 
        if (_index_15456 > _8573){
            goto L2; // [32] 96
        }

        /** 		if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__15451);
        _8574 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_8574);
        _8575 = (int)*(((s1_ptr)_2)->base + _index_15456);
        _8574 = NOVALUE;
        if (binary_op_a(EQUALS, _8575, 0)){
            _8575 = NOVALUE;
            goto L3; // [51] 89
        }
        _8575 = NOVALUE;

        /** 			put(map_handle_, temp_map_[KEY_LIST][index], temp_map_[VALUE_LIST][index])*/
        _2 = (int)SEQ_PTR(_temp_map__15451);
        _8577 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_8577);
        _8578 = (int)*(((s1_ptr)_2)->base + _index_15456);
        _8577 = NOVALUE;
        _2 = (int)SEQ_PTR(_temp_map__15451);
        _8579 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_8579);
        _8580 = (int)*(((s1_ptr)_2)->base + _index_15456);
        _8579 = NOVALUE;
        Ref(_map_handle__15452);
        Ref(_8578);
        Ref(_8580);
        _27put(_map_handle__15452, _8578, _8580, 1, _27threshold_size_14110);
        _8578 = NOVALUE;
        _8580 = NOVALUE;
L3: 

        /** 	end for*/
        _index_15456 = _index_15456 + 1;
        goto L1; // [91] 39
L2: 
        ;
    }

    /** 	eumem:ram_space[the_map_] = eumem:ram_space[map_handle_]*/
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_map_handle__15452)){
        _8581 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_map_handle__15452)->dbl));
    }
    else{
        _8581 = (int)*(((s1_ptr)_2)->base + _map_handle__15452);
    }
    Ref(_8581);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__15450);
    _1 = *(int *)_2;
    *(int *)_2 = _8581;
    if( _1 != _8581 ){
        DeRef(_1);
    }
    _8581 = NOVALUE;

    /** end procedure*/
    DeRef(_temp_map__15451);
    DeRef(_map_handle__15452);
    _8572 = NOVALUE;
    return;
    ;
}


void _27convert_to_small_map(int _the_map__15470)
{
    int _keys__15471 = NOVALUE;
    int _values__15472 = NOVALUE;
    int _8590 = NOVALUE;
    int _8589 = NOVALUE;
    int _8588 = NOVALUE;
    int _8587 = NOVALUE;
    int _8586 = NOVALUE;
    int _8585 = NOVALUE;
    int _8584 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map__15470)) {
        _1 = (long)(DBL_PTR(_the_map__15470)->dbl);
        if (UNIQUE(DBL_PTR(_the_map__15470)) && (DBL_PTR(_the_map__15470)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map__15470);
        _the_map__15470 = _1;
    }

    /** 	keys_ = keys(the_map_)*/
    _0 = _keys__15471;
    _keys__15471 = _27keys(_the_map__15470, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    _0 = _values__15472;
    _values__15472 = _27values(_the_map__15470, 0, 0);
    DeRef(_0);

    /** 	eumem:ram_space[the_map_] = {*/
    _8584 = Repeat(_27init_small_map_key_14111, _27threshold_size_14110);
    _8585 = Repeat(0, _27threshold_size_14110);
    _8586 = Repeat(0, _27threshold_size_14110);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_27type_is_map_14088);
    *((int *)(_2+4)) = _27type_is_map_14088;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _8584;
    *((int *)(_2+24)) = _8585;
    *((int *)(_2+28)) = _8586;
    _8587 = MAKE_SEQ(_1);
    _8586 = NOVALUE;
    _8585 = NOVALUE;
    _8584 = NOVALUE;
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _28ram_space_13199 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__15470);
    _1 = *(int *)_2;
    *(int *)_2 = _8587;
    if( _1 != _8587 ){
        DeRef(_1);
    }
    _8587 = NOVALUE;

    /** 	for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__15471)){
            _8588 = SEQ_PTR(_keys__15471)->length;
    }
    else {
        _8588 = 1;
    }
    {
        int _i_15480;
        _i_15480 = 1;
L1: 
        if (_i_15480 > _8588){
            goto L2; // [65] 98
        }

        /** 		put(the_map_, keys_[i], values_[i], PUT, 0)*/
        _2 = (int)SEQ_PTR(_keys__15471);
        _8589 = (int)*(((s1_ptr)_2)->base + _i_15480);
        _2 = (int)SEQ_PTR(_values__15472);
        _8590 = (int)*(((s1_ptr)_2)->base + _i_15480);
        Ref(_8589);
        Ref(_8590);
        _27put(_the_map__15470, _8589, _8590, 1, 0);
        _8589 = NOVALUE;
        _8590 = NOVALUE;

        /** 	end for*/
        _i_15480 = _i_15480 + 1;
        goto L1; // [93] 72
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_keys__15471);
    DeRef(_values__15472);
    return;
    ;
}



// 0x844C1732
