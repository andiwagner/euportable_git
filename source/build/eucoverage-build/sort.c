// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _21sort(int _x_4509, int _order_4510)
{
    int _gap_4511 = NOVALUE;
    int _j_4512 = NOVALUE;
    int _first_4513 = NOVALUE;
    int _last_4514 = NOVALUE;
    int _tempi_4515 = NOVALUE;
    int _tempj_4516 = NOVALUE;
    int _2296 = NOVALUE;
    int _2292 = NOVALUE;
    int _2289 = NOVALUE;
    int _2285 = NOVALUE;
    int _2282 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_order_4510)) {
        _1 = (long)(DBL_PTR(_order_4510)->dbl);
        if (UNIQUE(DBL_PTR(_order_4510)) && (DBL_PTR(_order_4510)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_order_4510);
        _order_4510 = _1;
    }

    /** 	if order >= 0 then*/
    if (_order_4510 < 0)
    goto L1; // [9] 23

    /** 		order = -1*/
    _order_4510 = -1;
    goto L2; // [20] 31
L1: 

    /** 		order = 1*/
    _order_4510 = 1;
L2: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_4509)){
            _last_4514 = SEQ_PTR(_x_4509)->length;
    }
    else {
        _last_4514 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_4514 >= 0) {
        _2282 = _last_4514 / 10;
    }
    else {
        temp_dbl = floor((double)_last_4514 / (double)10);
        _2282 = (long)temp_dbl;
    }
    _gap_4511 = _2282 + 1;
    _2282 = NOVALUE;

    /** 	while 1 do*/
L3: 

    /** 		first = gap + 1*/
    _first_4513 = _gap_4511 + 1;

    /** 		for i = first to last do*/
    _2285 = _last_4514;
    {
        int _i_4526;
        _i_4526 = _first_4513;
L4: 
        if (_i_4526 > _2285){
            goto L5; // [68] 170
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_4515);
        _2 = (int)SEQ_PTR(_x_4509);
        _tempi_4515 = (int)*(((s1_ptr)_2)->base + _i_4526);
        Ref(_tempi_4515);

        /** 			j = i - gap*/
        _j_4512 = _i_4526 - _gap_4511;

        /** 			while 1 do*/
L6: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_4516);
        _2 = (int)SEQ_PTR(_x_4509);
        _tempj_4516 = (int)*(((s1_ptr)_2)->base + _j_4512);
        Ref(_tempj_4516);

        /** 				if eu:compare(tempi, tempj) != order then*/
        if (IS_ATOM_INT(_tempi_4515) && IS_ATOM_INT(_tempj_4516)){
            _2289 = (_tempi_4515 < _tempj_4516) ? -1 : (_tempi_4515 > _tempj_4516);
        }
        else{
            _2289 = compare(_tempi_4515, _tempj_4516);
        }
        if (_2289 == _order_4510)
        goto L7; // [106] 123

        /** 					j += gap*/
        _j_4512 = _j_4512 + _gap_4511;

        /** 					exit*/
        goto L8; // [120] 157
L7: 

        /** 				x[j+gap] = tempj*/
        _2292 = _j_4512 + _gap_4511;
        Ref(_tempj_4516);
        _2 = (int)SEQ_PTR(_x_4509);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_4509 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _2292);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_4516;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_4512 > _gap_4511)
        goto L9; // [135] 144

        /** 					exit*/
        goto L8; // [141] 157
L9: 

        /** 				j -= gap*/
        _j_4512 = _j_4512 - _gap_4511;

        /** 			end while*/
        goto L6; // [154] 94
L8: 

        /** 			x[j] = tempi*/
        Ref(_tempi_4515);
        _2 = (int)SEQ_PTR(_x_4509);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_4509 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_4512);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_4515;
        DeRef(_1);

        /** 		end for*/
        _i_4526 = _i_4526 + 1;
        goto L4; // [165] 75
L5: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_4511 != 1)
    goto LA; // [172] 185

    /** 			return x*/
    DeRef(_tempi_4515);
    DeRef(_tempj_4516);
    DeRef(_2292);
    _2292 = NOVALUE;
    return _x_4509;
    goto L3; // [182] 55
LA: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_4511 >= 0) {
        _2296 = _gap_4511 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_4511 / (double)7);
        _2296 = (long)temp_dbl;
    }
    _gap_4511 = _2296 + 1;
    _2296 = NOVALUE;

    /** 	end while*/
    goto L3; // [200] 55
    ;
}


int _21custom_sort(int _custom_compare_4547, int _x_4548, int _data_4549, int _order_4550)
{
    int _gap_4551 = NOVALUE;
    int _j_4552 = NOVALUE;
    int _first_4553 = NOVALUE;
    int _last_4554 = NOVALUE;
    int _tempi_4555 = NOVALUE;
    int _tempj_4556 = NOVALUE;
    int _result_4557 = NOVALUE;
    int _args_4558 = NOVALUE;
    int _2324 = NOVALUE;
    int _2320 = NOVALUE;
    int _2317 = NOVALUE;
    int _2315 = NOVALUE;
    int _2314 = NOVALUE;
    int _2309 = NOVALUE;
    int _2306 = NOVALUE;
    int _2303 = NOVALUE;
    int _2302 = NOVALUE;
    int _2300 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_custom_compare_4547)) {
        _1 = (long)(DBL_PTR(_custom_compare_4547)->dbl);
        if (UNIQUE(DBL_PTR(_custom_compare_4547)) && (DBL_PTR(_custom_compare_4547)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_custom_compare_4547);
        _custom_compare_4547 = _1;
    }
    if (!IS_ATOM_INT(_order_4550)) {
        _1 = (long)(DBL_PTR(_order_4550)->dbl);
        if (UNIQUE(DBL_PTR(_order_4550)) && (DBL_PTR(_order_4550)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_order_4550);
        _order_4550 = _1;
    }

    /** 	sequence args = {0, 0}*/
    DeRef(_args_4558);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _args_4558 = MAKE_SEQ(_1);

    /** 	if order >= 0 then*/
    if (_order_4550 < 0)
    goto L1; // [19] 33

    /** 		order = -1*/
    _order_4550 = -1;
    goto L2; // [30] 41
L1: 

    /** 		order = 1*/
    _order_4550 = 1;
L2: 

    /** 	if atom(data) then*/
    _2300 = IS_ATOM(_data_4549);
    if (_2300 == 0)
    {
        _2300 = NOVALUE;
        goto L3; // [46] 58
    }
    else{
        _2300 = NOVALUE;
    }

    /** 		args &= data*/
    if (IS_SEQUENCE(_args_4558) && IS_ATOM(_data_4549)) {
        Ref(_data_4549);
        Append(&_args_4558, _args_4558, _data_4549);
    }
    else if (IS_ATOM(_args_4558) && IS_SEQUENCE(_data_4549)) {
    }
    else {
        Concat((object_ptr)&_args_4558, _args_4558, _data_4549);
    }
    goto L4; // [55] 78
L3: 

    /** 	elsif length(data) then*/
    if (IS_SEQUENCE(_data_4549)){
            _2302 = SEQ_PTR(_data_4549)->length;
    }
    else {
        _2302 = 1;
    }
    if (_2302 == 0)
    {
        _2302 = NOVALUE;
        goto L5; // [63] 77
    }
    else{
        _2302 = NOVALUE;
    }

    /** 		args = append(args, data[1])*/
    _2 = (int)SEQ_PTR(_data_4549);
    _2303 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_2303);
    Append(&_args_4558, _args_4558, _2303);
    _2303 = NOVALUE;
L5: 
L4: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_4548)){
            _last_4554 = SEQ_PTR(_x_4548)->length;
    }
    else {
        _last_4554 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_4554 >= 0) {
        _2306 = _last_4554 / 10;
    }
    else {
        temp_dbl = floor((double)_last_4554 / (double)10);
        _2306 = (long)temp_dbl;
    }
    _gap_4551 = _2306 + 1;
    _2306 = NOVALUE;

    /** 	while 1 do*/
L6: 

    /** 		first = gap + 1*/
    _first_4553 = _gap_4551 + 1;

    /** 		for i = first to last do*/
    _2309 = _last_4554;
    {
        int _i_4576;
        _i_4576 = _first_4553;
L7: 
        if (_i_4576 > _2309){
            goto L8; // [115] 260
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_4555);
        _2 = (int)SEQ_PTR(_x_4548);
        _tempi_4555 = (int)*(((s1_ptr)_2)->base + _i_4576);
        Ref(_tempi_4555);

        /** 			args[1] = tempi*/
        Ref(_tempi_4555);
        _2 = (int)SEQ_PTR(_args_4558);
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_4555;
        DeRef(_1);

        /** 			j = i - gap*/
        _j_4552 = _i_4576 - _gap_4551;

        /** 			while 1 do*/
L9: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_4556);
        _2 = (int)SEQ_PTR(_x_4548);
        _tempj_4556 = (int)*(((s1_ptr)_2)->base + _j_4552);
        Ref(_tempj_4556);

        /** 				args[2] = tempj*/
        Ref(_tempj_4556);
        _2 = (int)SEQ_PTR(_args_4558);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_4556;
        DeRef(_1);

        /** 				result = call_func(custom_compare, args)*/
        _1 = (int)SEQ_PTR(_args_4558);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_custom_compare_4547].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
        }
        DeRef(_result_4557);
        _result_4557 = _1;

        /** 				if sequence(result) then*/
        _2314 = IS_SEQUENCE(_result_4557);
        if (_2314 == 0)
        {
            _2314 = NOVALUE;
            goto LA; // [170] 190
        }
        else{
            _2314 = NOVALUE;
        }

        /** 					args[3] = result[2]*/
        _2 = (int)SEQ_PTR(_result_4557);
        _2315 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_2315);
        _2 = (int)SEQ_PTR(_args_4558);
        _2 = (int)(((s1_ptr)_2)->base + 3);
        _1 = *(int *)_2;
        *(int *)_2 = _2315;
        if( _1 != _2315 ){
            DeRef(_1);
        }
        _2315 = NOVALUE;

        /** 					result = result[1]*/
        _0 = _result_4557;
        _2 = (int)SEQ_PTR(_result_4557);
        _result_4557 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_result_4557);
        DeRef(_0);
LA: 

        /** 				if eu:compare(result, 0) != order then*/
        if (IS_ATOM_INT(_result_4557) && IS_ATOM_INT(0)){
            _2317 = (_result_4557 < 0) ? -1 : (_result_4557 > 0);
        }
        else{
            _2317 = compare(_result_4557, 0);
        }
        if (_2317 == _order_4550)
        goto LB; // [196] 213

        /** 					j += gap*/
        _j_4552 = _j_4552 + _gap_4551;

        /** 					exit*/
        goto LC; // [210] 247
LB: 

        /** 				x[j+gap] = tempj*/
        _2320 = _j_4552 + _gap_4551;
        Ref(_tempj_4556);
        _2 = (int)SEQ_PTR(_x_4548);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_4548 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _2320);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_4556;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_4552 > _gap_4551)
        goto LD; // [225] 234

        /** 					exit*/
        goto LC; // [231] 247
LD: 

        /** 				j -= gap*/
        _j_4552 = _j_4552 - _gap_4551;

        /** 			end while*/
        goto L9; // [244] 147
LC: 

        /** 			x[j] = tempi*/
        Ref(_tempi_4555);
        _2 = (int)SEQ_PTR(_x_4548);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_4548 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_4552);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_4555;
        DeRef(_1);

        /** 		end for*/
        _i_4576 = _i_4576 + 1;
        goto L7; // [255] 122
L8: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_4551 != 1)
    goto LE; // [262] 275

    /** 			return x*/
    DeRef(_data_4549);
    DeRef(_tempi_4555);
    DeRef(_tempj_4556);
    DeRef(_result_4557);
    DeRef(_args_4558);
    DeRef(_2320);
    _2320 = NOVALUE;
    return _x_4548;
    goto L6; // [272] 102
LE: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_4551 >= 0) {
        _2324 = _gap_4551 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_4551 / (double)7);
        _2324 = (long)temp_dbl;
    }
    _gap_4551 = _2324 + 1;
    _2324 = NOVALUE;

    /** 	end while*/
    goto L6; // [290] 102
    ;
}


int _21column_compare(int _a_4602, int _b_4603, int _cols_4604)
{
    int _sign_4605 = NOVALUE;
    int _column_4606 = NOVALUE;
    int _2347 = NOVALUE;
    int _2345 = NOVALUE;
    int _2344 = NOVALUE;
    int _2343 = NOVALUE;
    int _2342 = NOVALUE;
    int _2341 = NOVALUE;
    int _2340 = NOVALUE;
    int _2338 = NOVALUE;
    int _2337 = NOVALUE;
    int _2336 = NOVALUE;
    int _2334 = NOVALUE;
    int _2332 = NOVALUE;
    int _2329 = NOVALUE;
    int _2327 = NOVALUE;
    int _2326 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(cols) do*/
    if (IS_SEQUENCE(_cols_4604)){
            _2326 = SEQ_PTR(_cols_4604)->length;
    }
    else {
        _2326 = 1;
    }
    {
        int _i_4608;
        _i_4608 = 1;
L1: 
        if (_i_4608 > _2326){
            goto L2; // [6] 184
        }

        /** 		if cols[i] < 0 then*/
        _2 = (int)SEQ_PTR(_cols_4604);
        _2327 = (int)*(((s1_ptr)_2)->base + _i_4608);
        if (binary_op_a(GREATEREQ, _2327, 0)){
            _2327 = NOVALUE;
            goto L3; // [19] 46
        }
        _2327 = NOVALUE;

        /** 			sign = -1*/
        _sign_4605 = -1;

        /** 			column = -cols[i]*/
        _2 = (int)SEQ_PTR(_cols_4604);
        _2329 = (int)*(((s1_ptr)_2)->base + _i_4608);
        if (IS_ATOM_INT(_2329)) {
            if ((unsigned long)_2329 == 0xC0000000)
            _column_4606 = (int)NewDouble((double)-0xC0000000);
            else
            _column_4606 = - _2329;
        }
        else {
            _column_4606 = unary_op(UMINUS, _2329);
        }
        _2329 = NOVALUE;
        if (!IS_ATOM_INT(_column_4606)) {
            _1 = (long)(DBL_PTR(_column_4606)->dbl);
            if (UNIQUE(DBL_PTR(_column_4606)) && (DBL_PTR(_column_4606)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_column_4606);
            _column_4606 = _1;
        }
        goto L4; // [43] 64
L3: 

        /** 			sign = 1*/
        _sign_4605 = 1;

        /** 			column = cols[i]*/
        _2 = (int)SEQ_PTR(_cols_4604);
        _column_4606 = (int)*(((s1_ptr)_2)->base + _i_4608);
        if (!IS_ATOM_INT(_column_4606)){
            _column_4606 = (long)DBL_PTR(_column_4606)->dbl;
        }
L4: 

        /** 		if column <= length(a) then*/
        if (IS_SEQUENCE(_a_4602)){
                _2332 = SEQ_PTR(_a_4602)->length;
        }
        else {
            _2332 = 1;
        }
        if (_column_4606 > _2332)
        goto L5; // [71] 145

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_4603)){
                _2334 = SEQ_PTR(_b_4603)->length;
        }
        else {
            _2334 = 1;
        }
        if (_column_4606 > _2334)
        goto L6; // [80] 129

        /** 				if not equal(a[column], b[column]) then*/
        _2 = (int)SEQ_PTR(_a_4602);
        _2336 = (int)*(((s1_ptr)_2)->base + _column_4606);
        _2 = (int)SEQ_PTR(_b_4603);
        _2337 = (int)*(((s1_ptr)_2)->base + _column_4606);
        if (_2336 == _2337)
        _2338 = 1;
        else if (IS_ATOM_INT(_2336) && IS_ATOM_INT(_2337))
        _2338 = 0;
        else
        _2338 = (compare(_2336, _2337) == 0);
        _2336 = NOVALUE;
        _2337 = NOVALUE;
        if (_2338 != 0)
        goto L7; // [98] 177
        _2338 = NOVALUE;

        /** 					return sign * eu:compare(a[column], b[column])*/
        _2 = (int)SEQ_PTR(_a_4602);
        _2340 = (int)*(((s1_ptr)_2)->base + _column_4606);
        _2 = (int)SEQ_PTR(_b_4603);
        _2341 = (int)*(((s1_ptr)_2)->base + _column_4606);
        if (IS_ATOM_INT(_2340) && IS_ATOM_INT(_2341)){
            _2342 = (_2340 < _2341) ? -1 : (_2340 > _2341);
        }
        else{
            _2342 = compare(_2340, _2341);
        }
        _2340 = NOVALUE;
        _2341 = NOVALUE;
        if (_sign_4605 == (short)_sign_4605)
        _2343 = _sign_4605 * _2342;
        else
        _2343 = NewDouble(_sign_4605 * (double)_2342);
        _2342 = NOVALUE;
        DeRef(_a_4602);
        DeRef(_b_4603);
        DeRef(_cols_4604);
        return _2343;
        goto L7; // [126] 177
L6: 

        /** 				return sign * -1*/
        if (_sign_4605 == (short)_sign_4605)
        _2344 = _sign_4605 * -1;
        else
        _2344 = NewDouble(_sign_4605 * (double)-1);
        DeRef(_a_4602);
        DeRef(_b_4603);
        DeRef(_cols_4604);
        DeRef(_2343);
        _2343 = NOVALUE;
        return _2344;
        goto L7; // [142] 177
L5: 

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_4603)){
                _2345 = SEQ_PTR(_b_4603)->length;
        }
        else {
            _2345 = 1;
        }
        if (_column_4606 > _2345)
        goto L8; // [150] 169

        /** 				return sign * 1*/
        _2347 = _sign_4605 * 1;
        DeRef(_a_4602);
        DeRef(_b_4603);
        DeRef(_cols_4604);
        DeRef(_2343);
        _2343 = NOVALUE;
        DeRef(_2344);
        _2344 = NOVALUE;
        return _2347;
        goto L9; // [166] 176
L8: 

        /** 				return 0*/
        DeRef(_a_4602);
        DeRef(_b_4603);
        DeRef(_cols_4604);
        DeRef(_2343);
        _2343 = NOVALUE;
        DeRef(_2344);
        _2344 = NOVALUE;
        DeRef(_2347);
        _2347 = NOVALUE;
        return 0;
L9: 
L7: 

        /** 	end for*/
        _i_4608 = _i_4608 + 1;
        goto L1; // [179] 13
L2: 
        ;
    }

    /** 	return 0*/
    DeRef(_a_4602);
    DeRef(_b_4603);
    DeRef(_cols_4604);
    DeRef(_2343);
    _2343 = NOVALUE;
    DeRef(_2344);
    _2344 = NOVALUE;
    DeRef(_2347);
    _2347 = NOVALUE;
    return 0;
    ;
}


int _21sort_columns(int _x_4642, int _column_list_4643)
{
    int _2351 = NOVALUE;
    int _2350 = NOVALUE;
    int _2349 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return custom_sort(routine_id("column_compare"), x, {column_list})*/
    _2349 = CRoutineId(241, 21, _2348);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_column_list_4643);
    *((int *)(_2+4)) = _column_list_4643;
    _2350 = MAKE_SEQ(_1);
    RefDS(_x_4642);
    _2351 = _21custom_sort(_2349, _x_4642, _2350, 1);
    _2349 = NOVALUE;
    _2350 = NOVALUE;
    DeRefDS(_x_4642);
    DeRefDS(_column_list_4643);
    return _2351;
    ;
}


int _21merge(int _a_4650, int _b_4651, int _compfunc_4652, int _userdata_4653)
{
    int _al_4654 = NOVALUE;
    int _bl_4655 = NOVALUE;
    int _n_4656 = NOVALUE;
    int _r_4657 = NOVALUE;
    int _s_4658 = NOVALUE;
    int _2395 = NOVALUE;
    int _2394 = NOVALUE;
    int _2393 = NOVALUE;
    int _2391 = NOVALUE;
    int _2390 = NOVALUE;
    int _2389 = NOVALUE;
    int _2388 = NOVALUE;
    int _2386 = NOVALUE;
    int _2383 = NOVALUE;
    int _2381 = NOVALUE;
    int _2378 = NOVALUE;
    int _2377 = NOVALUE;
    int _2376 = NOVALUE;
    int _2375 = NOVALUE;
    int _2374 = NOVALUE;
    int _2373 = NOVALUE;
    int _2372 = NOVALUE;
    int _2369 = NOVALUE;
    int _2367 = NOVALUE;
    int _2364 = NOVALUE;
    int _2363 = NOVALUE;
    int _2362 = NOVALUE;
    int _2361 = NOVALUE;
    int _2360 = NOVALUE;
    int _2359 = NOVALUE;
    int _2358 = NOVALUE;
    int _2357 = NOVALUE;
    int _2354 = NOVALUE;
    int _2353 = NOVALUE;
    int _2352 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_compfunc_4652)) {
        _1 = (long)(DBL_PTR(_compfunc_4652)->dbl);
        if (UNIQUE(DBL_PTR(_compfunc_4652)) && (DBL_PTR(_compfunc_4652)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_compfunc_4652);
        _compfunc_4652 = _1;
    }

    /** 	al = 1*/
    _al_4654 = 1;

    /** 	bl = 1*/
    _bl_4655 = 1;

    /** 	n = 1*/
    _n_4656 = 1;

    /** 	s = repeat(0, length(a) + length(b))*/
    if (IS_SEQUENCE(_a_4650)){
            _2352 = SEQ_PTR(_a_4650)->length;
    }
    else {
        _2352 = 1;
    }
    if (IS_SEQUENCE(_b_4651)){
            _2353 = SEQ_PTR(_b_4651)->length;
    }
    else {
        _2353 = 1;
    }
    _2354 = _2352 + _2353;
    _2352 = NOVALUE;
    _2353 = NOVALUE;
    DeRef(_s_4658);
    _s_4658 = Repeat(0, _2354);
    _2354 = NOVALUE;

    /** 	if compfunc >= 0 then*/
    if (_compfunc_4652 < 0)
    goto L1; // [48] 165

    /** 		while al <= length(a) and bl <= length(b) do*/
L2: 
    if (IS_SEQUENCE(_a_4650)){
            _2357 = SEQ_PTR(_a_4650)->length;
    }
    else {
        _2357 = 1;
    }
    _2358 = (_al_4654 <= _2357);
    _2357 = NOVALUE;
    if (_2358 == 0) {
        goto L3; // [64] 268
    }
    if (IS_SEQUENCE(_b_4651)){
            _2360 = SEQ_PTR(_b_4651)->length;
    }
    else {
        _2360 = 1;
    }
    _2361 = (_bl_4655 <= _2360);
    _2360 = NOVALUE;
    if (_2361 == 0)
    {
        DeRef(_2361);
        _2361 = NOVALUE;
        goto L3; // [76] 268
    }
    else{
        DeRef(_2361);
        _2361 = NOVALUE;
    }

    /** 			r = call_func(compfunc,{a[al], b[bl], userdata})*/
    _2 = (int)SEQ_PTR(_a_4650);
    _2362 = (int)*(((s1_ptr)_2)->base + _al_4654);
    _2 = (int)SEQ_PTR(_b_4651);
    _2363 = (int)*(((s1_ptr)_2)->base + _bl_4655);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_2362);
    *((int *)(_2+4)) = _2362;
    Ref(_2363);
    *((int *)(_2+8)) = _2363;
    Ref(_userdata_4653);
    *((int *)(_2+12)) = _userdata_4653;
    _2364 = MAKE_SEQ(_1);
    _2363 = NOVALUE;
    _2362 = NOVALUE;
    _1 = (int)SEQ_PTR(_2364);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_compfunc_4652].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12)
                         );
    _r_4657 = _1;
    DeRefDS(_2364);
    _2364 = NOVALUE;
    if (!IS_ATOM_INT(_r_4657)) {
        _1 = (long)(DBL_PTR(_r_4657)->dbl);
        if (UNIQUE(DBL_PTR(_r_4657)) && (DBL_PTR(_r_4657)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_4657);
        _r_4657 = _1;
    }

    /** 			if r <= 0 then*/
    if (_r_4657 > 0)
    goto L4; // [105] 130

    /** 				s[n] = a[al]*/
    _2 = (int)SEQ_PTR(_a_4650);
    _2367 = (int)*(((s1_ptr)_2)->base + _al_4654);
    Ref(_2367);
    _2 = (int)SEQ_PTR(_s_4658);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_4658 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_4656);
    _1 = *(int *)_2;
    *(int *)_2 = _2367;
    if( _1 != _2367 ){
        DeRef(_1);
    }
    _2367 = NOVALUE;

    /** 				al += 1*/
    _al_4654 = _al_4654 + 1;
    goto L5; // [127] 149
L4: 

    /** 				s[n] = b[bl]*/
    _2 = (int)SEQ_PTR(_b_4651);
    _2369 = (int)*(((s1_ptr)_2)->base + _bl_4655);
    Ref(_2369);
    _2 = (int)SEQ_PTR(_s_4658);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_4658 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_4656);
    _1 = *(int *)_2;
    *(int *)_2 = _2369;
    if( _1 != _2369 ){
        DeRef(_1);
    }
    _2369 = NOVALUE;

    /** 				bl += 1*/
    _bl_4655 = _bl_4655 + 1;
L5: 

    /** 			n += 1*/
    _n_4656 = _n_4656 + 1;

    /** 		end while*/
    goto L2; // [159] 57
    goto L3; // [162] 268
L1: 

    /** 		while al <= length(a) and bl <= length(b) do*/
L6: 
    if (IS_SEQUENCE(_a_4650)){
            _2372 = SEQ_PTR(_a_4650)->length;
    }
    else {
        _2372 = 1;
    }
    _2373 = (_al_4654 <= _2372);
    _2372 = NOVALUE;
    if (_2373 == 0) {
        goto L7; // [177] 267
    }
    if (IS_SEQUENCE(_b_4651)){
            _2375 = SEQ_PTR(_b_4651)->length;
    }
    else {
        _2375 = 1;
    }
    _2376 = (_bl_4655 <= _2375);
    _2375 = NOVALUE;
    if (_2376 == 0)
    {
        DeRef(_2376);
        _2376 = NOVALUE;
        goto L7; // [189] 267
    }
    else{
        DeRef(_2376);
        _2376 = NOVALUE;
    }

    /** 			r = compare(a[al], b[bl])*/
    _2 = (int)SEQ_PTR(_a_4650);
    _2377 = (int)*(((s1_ptr)_2)->base + _al_4654);
    _2 = (int)SEQ_PTR(_b_4651);
    _2378 = (int)*(((s1_ptr)_2)->base + _bl_4655);
    if (IS_ATOM_INT(_2377) && IS_ATOM_INT(_2378)){
        _r_4657 = (_2377 < _2378) ? -1 : (_2377 > _2378);
    }
    else{
        _r_4657 = compare(_2377, _2378);
    }
    _2377 = NOVALUE;
    _2378 = NOVALUE;

    /** 			if r <= 0 then*/
    if (_r_4657 > 0)
    goto L8; // [210] 235

    /** 				s[n] = a[al]*/
    _2 = (int)SEQ_PTR(_a_4650);
    _2381 = (int)*(((s1_ptr)_2)->base + _al_4654);
    Ref(_2381);
    _2 = (int)SEQ_PTR(_s_4658);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_4658 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_4656);
    _1 = *(int *)_2;
    *(int *)_2 = _2381;
    if( _1 != _2381 ){
        DeRef(_1);
    }
    _2381 = NOVALUE;

    /** 				al += 1*/
    _al_4654 = _al_4654 + 1;
    goto L9; // [232] 254
L8: 

    /** 				s[n] = b[bl]*/
    _2 = (int)SEQ_PTR(_b_4651);
    _2383 = (int)*(((s1_ptr)_2)->base + _bl_4655);
    Ref(_2383);
    _2 = (int)SEQ_PTR(_s_4658);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_4658 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_4656);
    _1 = *(int *)_2;
    *(int *)_2 = _2383;
    if( _1 != _2383 ){
        DeRef(_1);
    }
    _2383 = NOVALUE;

    /** 				bl += 1*/
    _bl_4655 = _bl_4655 + 1;
L9: 

    /** 			n += 1*/
    _n_4656 = _n_4656 + 1;

    /** 		end while*/
    goto L6; // [264] 170
L7: 
L3: 

    /** 	if al > length(a) then*/
    if (IS_SEQUENCE(_a_4650)){
            _2386 = SEQ_PTR(_a_4650)->length;
    }
    else {
        _2386 = 1;
    }
    if (_al_4654 <= _2386)
    goto LA; // [273] 298

    /** 		s[n .. $] = b[bl .. $]*/
    if (IS_SEQUENCE(_s_4658)){
            _2388 = SEQ_PTR(_s_4658)->length;
    }
    else {
        _2388 = 1;
    }
    if (IS_SEQUENCE(_b_4651)){
            _2389 = SEQ_PTR(_b_4651)->length;
    }
    else {
        _2389 = 1;
    }
    rhs_slice_target = (object_ptr)&_2390;
    RHS_Slice(_b_4651, _bl_4655, _2389);
    assign_slice_seq = (s1_ptr *)&_s_4658;
    AssignSlice(_n_4656, _2388, _2390);
    _2388 = NOVALUE;
    DeRefDS(_2390);
    _2390 = NOVALUE;
    goto LB; // [295] 327
LA: 

    /** 	elsif bl > length(b) then*/
    if (IS_SEQUENCE(_b_4651)){
            _2391 = SEQ_PTR(_b_4651)->length;
    }
    else {
        _2391 = 1;
    }
    if (_bl_4655 <= _2391)
    goto LC; // [303] 326

    /** 		s[n .. $] = a[al .. $]*/
    if (IS_SEQUENCE(_s_4658)){
            _2393 = SEQ_PTR(_s_4658)->length;
    }
    else {
        _2393 = 1;
    }
    if (IS_SEQUENCE(_a_4650)){
            _2394 = SEQ_PTR(_a_4650)->length;
    }
    else {
        _2394 = 1;
    }
    rhs_slice_target = (object_ptr)&_2395;
    RHS_Slice(_a_4650, _al_4654, _2394);
    assign_slice_seq = (s1_ptr *)&_s_4658;
    AssignSlice(_n_4656, _2393, _2395);
    _2393 = NOVALUE;
    DeRefDS(_2395);
    _2395 = NOVALUE;
LC: 
LB: 

    /** 	return s*/
    DeRefDS(_a_4650);
    DeRefDS(_b_4651);
    DeRef(_userdata_4653);
    DeRef(_2358);
    _2358 = NOVALUE;
    DeRef(_2373);
    _2373 = NOVALUE;
    return _s_4658;
    ;
}


int _21insertion_sort(int _s_4715, int _e_4716, int _compfunc_4717, int _userdata_4718)
{
    int _key_4719 = NOVALUE;
    int _a_4720 = NOVALUE;
    int _2439 = NOVALUE;
    int _2438 = NOVALUE;
    int _2437 = NOVALUE;
    int _2436 = NOVALUE;
    int _2435 = NOVALUE;
    int _2434 = NOVALUE;
    int _2430 = NOVALUE;
    int _2429 = NOVALUE;
    int _2428 = NOVALUE;
    int _2427 = NOVALUE;
    int _2425 = NOVALUE;
    int _2424 = NOVALUE;
    int _2423 = NOVALUE;
    int _2422 = NOVALUE;
    int _2421 = NOVALUE;
    int _2420 = NOVALUE;
    int _2419 = NOVALUE;
    int _2415 = NOVALUE;
    int _2414 = NOVALUE;
    int _2413 = NOVALUE;
    int _2410 = NOVALUE;
    int _2408 = NOVALUE;
    int _2407 = NOVALUE;
    int _2406 = NOVALUE;
    int _2405 = NOVALUE;
    int _2404 = NOVALUE;
    int _2403 = NOVALUE;
    int _2402 = NOVALUE;
    int _2401 = NOVALUE;
    int _2400 = NOVALUE;
    int _2398 = NOVALUE;
    int _2396 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_compfunc_4717)) {
        _1 = (long)(DBL_PTR(_compfunc_4717)->dbl);
        if (UNIQUE(DBL_PTR(_compfunc_4717)) && (DBL_PTR(_compfunc_4717)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_compfunc_4717);
        _compfunc_4717 = _1;
    }

    /** 	if atom(e) then*/
    _2396 = IS_ATOM(_e_4716);
    if (_2396 == 0)
    {
        _2396 = NOVALUE;
        goto L1; // [12] 24
    }
    else{
        _2396 = NOVALUE;
    }

    /** 		s &= e*/
    if (IS_SEQUENCE(_s_4715) && IS_ATOM(_e_4716)) {
        Ref(_e_4716);
        Append(&_s_4715, _s_4715, _e_4716);
    }
    else if (IS_ATOM(_s_4715) && IS_SEQUENCE(_e_4716)) {
    }
    else {
        Concat((object_ptr)&_s_4715, _s_4715, _e_4716);
    }
    goto L2; // [21] 80
L1: 

    /** 	elsif length(e) > 1 then*/
    if (IS_SEQUENCE(_e_4716)){
            _2398 = SEQ_PTR(_e_4716)->length;
    }
    else {
        _2398 = 1;
    }
    if (_2398 <= 1)
    goto L3; // [29] 79

    /** 		return merge(insertion_sort(s,,compfunc, userdata), insertion_sort(e,,compfunc, userdata), compfunc, userdata)*/
    RefDS(_s_4715);
    DeRef(_2400);
    _2400 = _s_4715;
    DeRef(_2401);
    _2401 = _compfunc_4717;
    Ref(_userdata_4718);
    DeRef(_2402);
    _2402 = _userdata_4718;
    RefDS(_5);
    _2403 = _21insertion_sort(_2400, _5, _2401, _2402);
    _2400 = NOVALUE;
    _2401 = NOVALUE;
    _2402 = NOVALUE;
    Ref(_e_4716);
    DeRef(_2404);
    _2404 = _e_4716;
    DeRef(_2405);
    _2405 = _compfunc_4717;
    Ref(_userdata_4718);
    DeRef(_2406);
    _2406 = _userdata_4718;
    RefDS(_5);
    _2407 = _21insertion_sort(_2404, _5, _2405, _2406);
    _2404 = NOVALUE;
    _2405 = NOVALUE;
    _2406 = NOVALUE;
    Ref(_userdata_4718);
    _2408 = _21merge(_2403, _2407, _compfunc_4717, _userdata_4718);
    _2403 = NOVALUE;
    _2407 = NOVALUE;
    DeRefDS(_s_4715);
    DeRef(_e_4716);
    DeRef(_userdata_4718);
    DeRef(_key_4719);
    return _2408;
L3: 
L2: 

    /** 	if compfunc = -1 then*/
    if (_compfunc_4717 != -1)
    goto L4; // [82] 231

    /** 		for j = 2 to length(s) label "outer" do*/
    if (IS_SEQUENCE(_s_4715)){
            _2410 = SEQ_PTR(_s_4715)->length;
    }
    else {
        _2410 = 1;
    }
    {
        int _j_4739;
        _j_4739 = 2;
L5: 
        if (_j_4739 > _2410){
            goto L6; // [91] 228
        }

        /** 			key = s[j]*/
        DeRef(_key_4719);
        _2 = (int)SEQ_PTR(_s_4715);
        _key_4719 = (int)*(((s1_ptr)_2)->base + _j_4739);
        Ref(_key_4719);

        /** 			for i = j - 1 to 1 by -1 do*/
        _2413 = _j_4739 - 1;
        {
            int _i_4744;
            _i_4744 = _2413;
L7: 
            if (_i_4744 < 1){
                goto L8; // [110] 193
            }

            /** 				if compare(s[i], key) <= 0 then*/
            _2 = (int)SEQ_PTR(_s_4715);
            _2414 = (int)*(((s1_ptr)_2)->base + _i_4744);
            if (IS_ATOM_INT(_2414) && IS_ATOM_INT(_key_4719)){
                _2415 = (_2414 < _key_4719) ? -1 : (_2414 > _key_4719);
            }
            else{
                _2415 = compare(_2414, _key_4719);
            }
            _2414 = NOVALUE;
            if (_2415 > 0)
            goto L9; // [127] 177

            /** 					a = i+1*/
            _a_4720 = _i_4744 + 1;

            /** 					if a != j then*/
            if (_a_4720 == _j_4739)
            goto LA; // [141] 223

            /** 						s[a+1 .. j] = s[a .. j-1]*/
            _2419 = _a_4720 + 1;
            _2420 = _j_4739 - 1;
            rhs_slice_target = (object_ptr)&_2421;
            RHS_Slice(_s_4715, _a_4720, _2420);
            assign_slice_seq = (s1_ptr *)&_s_4715;
            AssignSlice(_2419, _j_4739, _2421);
            _2419 = NOVALUE;
            DeRefDS(_2421);
            _2421 = NOVALUE;

            /** 						s[a] = key*/
            Ref(_key_4719);
            _2 = (int)SEQ_PTR(_s_4715);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_4715 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _a_4720);
            _1 = *(int *)_2;
            *(int *)_2 = _key_4719;
            DeRef(_1);

            /** 					continue "outer"*/
            goto LA; // [174] 223
L9: 

            /** 				a = i*/
            _a_4720 = _i_4744;

            /** 			end for*/
            _i_4744 = _i_4744 + -1;
            goto L7; // [188] 117
L8: 
            ;
        }

        /** 			s[a+1 .. j] = s[a .. j-1]*/
        _2422 = _a_4720 + 1;
        if (_2422 > MAXINT){
            _2422 = NewDouble((double)_2422);
        }
        _2423 = _j_4739 - 1;
        rhs_slice_target = (object_ptr)&_2424;
        RHS_Slice(_s_4715, _a_4720, _2423);
        assign_slice_seq = (s1_ptr *)&_s_4715;
        AssignSlice(_2422, _j_4739, _2424);
        DeRef(_2422);
        _2422 = NOVALUE;
        DeRefDS(_2424);
        _2424 = NOVALUE;

        /** 			s[a] = key*/
        Ref(_key_4719);
        _2 = (int)SEQ_PTR(_s_4715);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4715 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_4720);
        _1 = *(int *)_2;
        *(int *)_2 = _key_4719;
        DeRef(_1);

        /** 		end for*/
LA: 
        _j_4739 = _j_4739 + 1;
        goto L5; // [223] 98
L6: 
        ;
    }
    goto LB; // [228] 380
L4: 

    /** 		for j = 2 to length(s) label "outer" do*/
    if (IS_SEQUENCE(_s_4715)){
            _2425 = SEQ_PTR(_s_4715)->length;
    }
    else {
        _2425 = 1;
    }
    {
        int _j_4761;
        _j_4761 = 2;
LC: 
        if (_j_4761 > _2425){
            goto LD; // [236] 379
        }

        /** 			key = s[j]*/
        DeRef(_key_4719);
        _2 = (int)SEQ_PTR(_s_4715);
        _key_4719 = (int)*(((s1_ptr)_2)->base + _j_4761);
        Ref(_key_4719);

        /** 			for i = j - 1 to 1 by -1 do*/
        _2427 = _j_4761 - 1;
        {
            int _i_4765;
            _i_4765 = _2427;
LE: 
            if (_i_4765 < 1){
                goto LF; // [255] 344
            }

            /** 				if call_func(compfunc,{s[i], key, userdata}) <= 0 then*/
            _2 = (int)SEQ_PTR(_s_4715);
            _2428 = (int)*(((s1_ptr)_2)->base + _i_4765);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_2428);
            *((int *)(_2+4)) = _2428;
            Ref(_key_4719);
            *((int *)(_2+8)) = _key_4719;
            Ref(_userdata_4718);
            *((int *)(_2+12)) = _userdata_4718;
            _2429 = MAKE_SEQ(_1);
            _2428 = NOVALUE;
            _1 = (int)SEQ_PTR(_2429);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_compfunc_4717].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            Ref(*(int *)(_2+12));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8), 
                                *(int *)(_2+12)
                                 );
            DeRef(_2430);
            _2430 = _1;
            DeRefDS(_2429);
            _2429 = NOVALUE;
            if (binary_op_a(GREATER, _2430, 0)){
                DeRef(_2430);
                _2430 = NOVALUE;
                goto L10; // [278] 328
            }
            DeRef(_2430);
            _2430 = NOVALUE;

            /** 					a = i+1*/
            _a_4720 = _i_4765 + 1;

            /** 					if a != j then*/
            if (_a_4720 == _j_4761)
            goto L11; // [292] 374

            /** 						s[a+1 .. j] = s[a .. j-1]*/
            _2434 = _a_4720 + 1;
            _2435 = _j_4761 - 1;
            rhs_slice_target = (object_ptr)&_2436;
            RHS_Slice(_s_4715, _a_4720, _2435);
            assign_slice_seq = (s1_ptr *)&_s_4715;
            AssignSlice(_2434, _j_4761, _2436);
            _2434 = NOVALUE;
            DeRefDS(_2436);
            _2436 = NOVALUE;

            /** 						s[a] = key*/
            Ref(_key_4719);
            _2 = (int)SEQ_PTR(_s_4715);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_4715 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _a_4720);
            _1 = *(int *)_2;
            *(int *)_2 = _key_4719;
            DeRef(_1);

            /** 					continue "outer"*/
            goto L11; // [325] 374
L10: 

            /** 				a = i*/
            _a_4720 = _i_4765;

            /** 			end for*/
            _i_4765 = _i_4765 + -1;
            goto LE; // [339] 262
LF: 
            ;
        }

        /** 			s[a+1 .. j] = s[a .. j-1]*/
        _2437 = _a_4720 + 1;
        if (_2437 > MAXINT){
            _2437 = NewDouble((double)_2437);
        }
        _2438 = _j_4761 - 1;
        rhs_slice_target = (object_ptr)&_2439;
        RHS_Slice(_s_4715, _a_4720, _2438);
        assign_slice_seq = (s1_ptr *)&_s_4715;
        AssignSlice(_2437, _j_4761, _2439);
        DeRef(_2437);
        _2437 = NOVALUE;
        DeRefDS(_2439);
        _2439 = NOVALUE;

        /** 			s[a] = key*/
        Ref(_key_4719);
        _2 = (int)SEQ_PTR(_s_4715);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4715 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_4720);
        _1 = *(int *)_2;
        *(int *)_2 = _key_4719;
        DeRef(_1);

        /** 		end for*/
L11: 
        _j_4761 = _j_4761 + 1;
        goto LC; // [374] 243
LD: 
        ;
    }
LB: 

    /** 	return s*/
    DeRef(_e_4716);
    DeRef(_userdata_4718);
    DeRef(_key_4719);
    DeRef(_2413);
    _2413 = NOVALUE;
    DeRef(_2427);
    _2427 = NOVALUE;
    DeRef(_2408);
    _2408 = NOVALUE;
    DeRef(_2420);
    _2420 = NOVALUE;
    DeRef(_2423);
    _2423 = NOVALUE;
    DeRef(_2435);
    _2435 = NOVALUE;
    DeRef(_2438);
    _2438 = NOVALUE;
    return _s_4715;
    ;
}



// 0xDF080B89
