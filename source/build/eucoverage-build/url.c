// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _34parse_querystring(int _query_string_17870)
{
    int _i_17871 = NOVALUE;
    int _char_17872 = NOVALUE;
    int _tmp_17873 = NOVALUE;
    int _charbuf_17874 = NOVALUE;
    int _fname_17875 = NOVALUE;
    int _the_map_17876 = NOVALUE;
    int _value_inlined_value_at_104_17894 = NOVALUE;
    int _st_inlined_value_at_101_17893 = NOVALUE;
    int _10035 = NOVALUE;
    int _10029 = NOVALUE;
    int _10028 = NOVALUE;
    int _10027 = NOVALUE;
    int _10026 = NOVALUE;
    int _10025 = NOVALUE;
    int _10024 = NOVALUE;
    int _10019 = NOVALUE;
    int _10018 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence charbuf, fname=""*/
    RefDS(_5);
    DeRef(_fname_17875);
    _fname_17875 = _5;

    /** 	map:map the_map = map:new()*/
    _0 = _the_map_17876;
    _the_map_17876 = _27new(690);
    DeRef(_0);

    /** 	if atom(query_string) then*/
    _10018 = IS_ATOM(_query_string_17870);
    if (_10018 == 0)
    {
        _10018 = NOVALUE;
        goto L1; // [19] 29
    }
    else{
        _10018 = NOVALUE;
    }

    /** 		return the_map*/
    DeRef(_query_string_17870);
    DeRef(_tmp_17873);
    DeRef(_charbuf_17874);
    DeRefDS(_fname_17875);
    return _the_map_17876;
L1: 

    /** 	charbuf = {}  */
    RefDS(_5);
    DeRef(_charbuf_17874);
    _charbuf_17874 = _5;

    /** 	i = 1*/
    _i_17871 = 1;

    /** 	while i <= length(query_string) do*/
L2: 
    if (IS_SEQUENCE(_query_string_17870)){
            _10019 = SEQ_PTR(_query_string_17870)->length;
    }
    else {
        _10019 = 1;
    }
    if (_i_17871 > _10019)
    goto L3; // [51] 233

    /** 		char = query_string[i]  -- character we're working on*/
    _2 = (int)SEQ_PTR(_query_string_17870);
    _char_17872 = (int)*(((s1_ptr)_2)->base + _i_17871);
    if (!IS_ATOM_INT(_char_17872)){
        _char_17872 = (long)DBL_PTR(_char_17872)->dbl;
    }

    /** 		switch char do*/
    _0 = _char_17872;
    switch ( _0 ){ 

        /** 			case HEX_SIG then*/
        case 37:

        /** 				tmp = stdget:value("#" & query_string[i+1] & query_string[i+2])*/
        _10024 = _i_17871 + 1;
        _2 = (int)SEQ_PTR(_query_string_17870);
        _10025 = (int)*(((s1_ptr)_2)->base + _10024);
        _10026 = _i_17871 + 2;
        _2 = (int)SEQ_PTR(_query_string_17870);
        _10027 = (int)*(((s1_ptr)_2)->base + _10026);
        {
            int concat_list[3];

            concat_list[0] = _10027;
            concat_list[1] = _10025;
            concat_list[2] = _270;
            Concat_N((object_ptr)&_10028, concat_list, 3);
        }
        _10027 = NOVALUE;
        _10025 = NOVALUE;
        DeRef(_st_inlined_value_at_101_17893);
        _st_inlined_value_at_101_17893 = _10028;
        _10028 = NOVALUE;
        if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_2711)) {
            _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_2711)->dbl);
            if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_2711)) && (DBL_PTR(_14GET_SHORT_ANSWER_2711)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_14GET_SHORT_ANSWER_2711);
            _14GET_SHORT_ANSWER_2711 = _1;
        }
        if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_2711)) {
            _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_2711)->dbl);
            if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_2711)) && (DBL_PTR(_14GET_SHORT_ANSWER_2711)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_14GET_SHORT_ANSWER_2711);
            _14GET_SHORT_ANSWER_2711 = _1;
        }

        /** 	return get_value(st, start_point, answer)*/
        RefDS(_st_inlined_value_at_101_17893);
        _0 = _tmp_17873;
        _tmp_17873 = _14get_value(_st_inlined_value_at_101_17893, 1, _14GET_SHORT_ANSWER_2711);
        DeRef(_0);
        DeRef(_st_inlined_value_at_101_17893);
        _st_inlined_value_at_101_17893 = NOVALUE;

        /** 				charbuf &= tmp[2]*/
        _2 = (int)SEQ_PTR(_tmp_17873);
        _10029 = (int)*(((s1_ptr)_2)->base + 2);
        if (IS_SEQUENCE(_charbuf_17874) && IS_ATOM(_10029)) {
            Ref(_10029);
            Append(&_charbuf_17874, _charbuf_17874, _10029);
        }
        else if (IS_ATOM(_charbuf_17874) && IS_SEQUENCE(_10029)) {
        }
        else {
            Concat((object_ptr)&_charbuf_17874, _charbuf_17874, _10029);
        }
        _10029 = NOVALUE;

        /** 				i += 2 -- skip over hex digits*/
        _i_17871 = _i_17871 + 2;
        goto L4; // [140] 220

        /** 			case WHITESPACE then*/
        case 43:

        /** 				charbuf &= " "*/
        Concat((object_ptr)&_charbuf_17874, _charbuf_17874, _3039);
        goto L4; // [152] 220

        /** 			case VALUE_SEP then*/
        case 61:

        /** 				fname = charbuf*/
        RefDS(_charbuf_17874);
        DeRef(_fname_17875);
        _fname_17875 = _charbuf_17874;

        /** 				charbuf = {}*/
        RefDS(_5);
        DeRefDS(_charbuf_17874);
        _charbuf_17874 = _5;
        goto L4; // [172] 220

        /** 			case PAIR_SEP_A, PAIR_SEP_B then*/
        case 38:
        case 59:

        /** 				map:put(the_map, fname, charbuf)*/
        Ref(_the_map_17876);
        RefDS(_fname_17875);
        RefDS(_charbuf_17874);
        _27put(_the_map_17876, _fname_17875, _charbuf_17874, 1, _27threshold_size_14110);

        /** 				fname = {}*/
        RefDS(_5);
        DeRefDS(_fname_17875);
        _fname_17875 = _5;

        /** 				charbuf = {}*/
        RefDS(_5);
        DeRefDS(_charbuf_17874);
        _charbuf_17874 = _5;
        goto L4; // [207] 220

        /** 			case else*/
        default:

        /** 				charbuf &= char*/
        Append(&_charbuf_17874, _charbuf_17874, _char_17872);
    ;}L4: 

    /** 		i += 1*/
    _i_17871 = _i_17871 + 1;

    /** 	end while*/
    goto L2; // [230] 48
L3: 

    /** 	if length(fname) then*/
    if (IS_SEQUENCE(_fname_17875)){
            _10035 = SEQ_PTR(_fname_17875)->length;
    }
    else {
        _10035 = 1;
    }
    if (_10035 == 0)
    {
        _10035 = NOVALUE;
        goto L5; // [238] 255
    }
    else{
        _10035 = NOVALUE;
    }

    /** 		map:put(the_map, fname, charbuf)*/
    Ref(_the_map_17876);
    RefDS(_fname_17875);
    RefDS(_charbuf_17874);
    _27put(_the_map_17876, _fname_17875, _charbuf_17874, 1, _27threshold_size_14110);
L5: 

    /** 	return the_map*/
    DeRef(_query_string_17870);
    DeRef(_tmp_17873);
    DeRef(_charbuf_17874);
    DeRef(_fname_17875);
    DeRef(_10024);
    _10024 = NOVALUE;
    DeRef(_10026);
    _10026 = NOVALUE;
    return _the_map_17876;
    ;
}


int _34parse(int _url_17922, int _querystring_also_17923)
{
    int _protocol_17924 = NOVALUE;
    int _host_name_17925 = NOVALUE;
    int _path_17926 = NOVALUE;
    int _user_name_17927 = NOVALUE;
    int _password_17928 = NOVALUE;
    int _query_string_17929 = NOVALUE;
    int _port_17930 = NOVALUE;
    int _pos_17931 = NOVALUE;
    int _at_17950 = NOVALUE;
    int _password_colon_17955 = NOVALUE;
    int _qs_start_17970 = NOVALUE;
    int _first_slash_17972 = NOVALUE;
    int _port_colon_17974 = NOVALUE;
    int _port_end_17993 = NOVALUE;
    int _10104 = NOVALUE;
    int _10102 = NOVALUE;
    int _10101 = NOVALUE;
    int _10099 = NOVALUE;
    int _10098 = NOVALUE;
    int _10095 = NOVALUE;
    int _10092 = NOVALUE;
    int _10088 = NOVALUE;
    int _10087 = NOVALUE;
    int _10082 = NOVALUE;
    int _10080 = NOVALUE;
    int _10078 = NOVALUE;
    int _10074 = NOVALUE;
    int _10068 = NOVALUE;
    int _10066 = NOVALUE;
    int _10065 = NOVALUE;
    int _10063 = NOVALUE;
    int _10062 = NOVALUE;
    int _10061 = NOVALUE;
    int _10060 = NOVALUE;
    int _10053 = NOVALUE;
    int _10050 = NOVALUE;
    int _10047 = NOVALUE;
    int _10044 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_querystring_also_17923)) {
        _1 = (long)(DBL_PTR(_querystring_also_17923)->dbl);
        if (UNIQUE(DBL_PTR(_querystring_also_17923)) && (DBL_PTR(_querystring_also_17923)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_querystring_also_17923);
        _querystring_also_17923 = _1;
    }

    /**     sequence protocol = ""*/
    RefDS(_5);
    DeRef(_protocol_17924);
    _protocol_17924 = _5;

    /**     host_name = 0*/
    DeRef(_host_name_17925);
    _host_name_17925 = 0;

    /**     port = 0*/
    _port_17930 = 0;

    /**     path = 0*/
    DeRef(_path_17926);
    _path_17926 = 0;

    /**     user_name = 0*/
    DeRef(_user_name_17927);
    _user_name_17927 = 0;

    /**     password  = 0*/
    DeRef(_password_17928);
    _password_17928 = 0;

    /**     query_string = 0*/
    DeRef(_query_string_17929);
    _query_string_17929 = 0;

    /** 	integer pos = find(':', url)*/
    _pos_17931 = find_from(58, _url_17922, 1);

    /**     if not pos then*/
    if (_pos_17931 != 0)
    goto L1; // [57] 67

    /**         return 0*/
    DeRefDS(_url_17922);
    DeRefDS(_protocol_17924);
    return 0;
L1: 

    /** 	protocol = url[1..pos - 1]*/
    _10044 = _pos_17931 - 1;
    rhs_slice_target = (object_ptr)&_protocol_17924;
    RHS_Slice(_url_17922, 1, _10044);

    /**     pos += 1*/
    _pos_17931 = _pos_17931 + 1;

    /**     if url[pos] = '/' then*/
    _2 = (int)SEQ_PTR(_url_17922);
    _10047 = (int)*(((s1_ptr)_2)->base + _pos_17931);
    if (binary_op_a(NOTEQ, _10047, 47)){
        _10047 = NOVALUE;
        goto L2; // [92] 105
    }
    _10047 = NOVALUE;

    /**         pos += 1*/
    _pos_17931 = _pos_17931 + 1;
L2: 

    /**     if url[pos] = '/' then*/
    _2 = (int)SEQ_PTR(_url_17922);
    _10050 = (int)*(((s1_ptr)_2)->base + _pos_17931);
    if (binary_op_a(NOTEQ, _10050, 47)){
        _10050 = NOVALUE;
        goto L3; // [111] 124
    }
    _10050 = NOVALUE;

    /**         pos += 1*/
    _pos_17931 = _pos_17931 + 1;
L3: 

    /** 	if url[pos] = '/' then*/
    _2 = (int)SEQ_PTR(_url_17922);
    _10053 = (int)*(((s1_ptr)_2)->base + _pos_17931);
    if (binary_op_a(NOTEQ, _10053, 47)){
        _10053 = NOVALUE;
        goto L4; // [130] 141
    }
    _10053 = NOVALUE;

    /**         goto "parse_path"*/
    goto G5;
L4: 

    /** 	integer at = find('@', url)*/
    _at_17950 = find_from(64, _url_17922, 1);

    /**     if not at then*/
    if (_at_17950 != 0)
    goto L6; // [152] 162

    /**         goto "parse_domain"*/
    goto G7;
L6: 

    /**     integer password_colon = find(':', url, pos)*/
    _password_colon_17955 = find_from(58, _url_17922, _pos_17931);

    /**     if password_colon > 0 and password_colon < at then*/
    _10060 = (_password_colon_17955 > 0);
    if (_10060 == 0) {
        goto L8; // [177] 218
    }
    _10062 = (_password_colon_17955 < _at_17950);
    if (_10062 == 0)
    {
        DeRef(_10062);
        _10062 = NOVALUE;
        goto L8; // [186] 218
    }
    else{
        DeRef(_10062);
        _10062 = NOVALUE;
    }

    /**         user_name = url[pos..password_colon-1]*/
    _10063 = _password_colon_17955 - 1;
    rhs_slice_target = (object_ptr)&_user_name_17927;
    RHS_Slice(_url_17922, _pos_17931, _10063);

    /**         password = url[password_colon+1..at-1]*/
    _10065 = _password_colon_17955 + 1;
    if (_10065 > MAXINT){
        _10065 = NewDouble((double)_10065);
    }
    _10066 = _at_17950 - 1;
    rhs_slice_target = (object_ptr)&_password_17928;
    RHS_Slice(_url_17922, _10065, _10066);
    goto L9; // [215] 230
L8: 

    /**     	user_name = url[pos..at-1]*/
    _10068 = _at_17950 - 1;
    rhs_slice_target = (object_ptr)&_user_name_17927;
    RHS_Slice(_url_17922, _pos_17931, _10068);
L9: 

    /**     pos = at + 1*/
    _pos_17931 = _at_17950 + 1;

    /** label "parse_domain"*/
G7:

    /**     integer qs_start = find('?', url, pos)*/
    _qs_start_17970 = find_from(63, _url_17922, _pos_17931);

    /** 	integer first_slash = find('/', url, pos)*/
    _first_slash_17972 = find_from(47, _url_17922, _pos_17931);

    /**     integer port_colon = find(':', url, pos)*/
    _port_colon_17974 = find_from(58, _url_17922, _pos_17931);

    /**     if port_colon then*/
    if (_port_colon_17974 == 0)
    {
        goto LA; // [271] 288
    }
    else{
    }

    /**         host_name = url[pos..port_colon-1]*/
    _10074 = _port_colon_17974 - 1;
    rhs_slice_target = (object_ptr)&_host_name_17925;
    RHS_Slice(_url_17922, _pos_17931, _10074);
    goto LB; // [285] 339
LA: 

    /**         if not first_slash then*/
    if (_first_slash_17972 != 0)
    goto LC; // [290] 326

    /**             if not qs_start then*/
    if (_qs_start_17970 != 0)
    goto LD; // [295] 311

    /**                 host_name = url[pos..$]*/
    if (IS_SEQUENCE(_url_17922)){
            _10078 = SEQ_PTR(_url_17922)->length;
    }
    else {
        _10078 = 1;
    }
    rhs_slice_target = (object_ptr)&_host_name_17925;
    RHS_Slice(_url_17922, _pos_17931, _10078);
    goto LE; // [308] 338
LD: 

    /**             	host_name = url[pos..qs_start-1]*/
    _10080 = _qs_start_17970 - 1;
    rhs_slice_target = (object_ptr)&_host_name_17925;
    RHS_Slice(_url_17922, _pos_17931, _10080);
    goto LE; // [323] 338
LC: 

    /**         	host_name = url[pos..first_slash-1]*/
    _10082 = _first_slash_17972 - 1;
    rhs_slice_target = (object_ptr)&_host_name_17925;
    RHS_Slice(_url_17922, _pos_17931, _10082);
LE: 
LB: 

    /**     if port_colon then*/
    if (_port_colon_17974 == 0)
    {
        goto LF; // [341] 413
    }
    else{
    }

    /**         integer port_end = 0*/
    _port_end_17993 = 0;

    /** 		if first_slash then*/
    if (_first_slash_17972 == 0)
    {
        goto L10; // [353] 367
    }
    else{
    }

    /**             port_end = first_slash - 1*/
    _port_end_17993 = _first_slash_17972 - 1;
    goto L11; // [364] 391
L10: 

    /**         elsif qs_start then*/
    if (_qs_start_17970 == 0)
    {
        goto L12; // [369] 383
    }
    else{
    }

    /**             port_end = qs_start - 1*/
    _port_end_17993 = _qs_start_17970 - 1;
    goto L11; // [380] 391
L12: 

    /**         	port_end = length(url)*/
    if (IS_SEQUENCE(_url_17922)){
            _port_end_17993 = SEQ_PTR(_url_17922)->length;
    }
    else {
        _port_end_17993 = 1;
    }
L11: 

    /** 		port = stdget:defaulted_value(url[port_colon+1..port_end], 0)*/
    _10087 = _port_colon_17974 + 1;
    rhs_slice_target = (object_ptr)&_10088;
    RHS_Slice(_url_17922, _10087, _port_end_17993);
    _port_17930 = _14defaulted_value(_10088, 0, 1);
    _10088 = NOVALUE;
    if (!IS_ATOM_INT(_port_17930)) {
        _1 = (long)(DBL_PTR(_port_17930)->dbl);
        if (UNIQUE(DBL_PTR(_port_17930)) && (DBL_PTR(_port_17930)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_port_17930);
        _port_17930 = _1;
    }
LF: 

    /**     if first_slash then*/
    if (_first_slash_17972 == 0)
    {
        goto L13; // [417] 430
    }
    else{
    }

    /**         pos = first_slash*/
    _pos_17931 = _first_slash_17972;
    goto L14; // [427] 452
L13: 

    /**     elsif qs_start then*/
    if (_qs_start_17970 == 0)
    {
        goto L15; // [432] 445
    }
    else{
    }

    /**         pos = qs_start*/
    _pos_17931 = _qs_start_17970;
    goto L14; // [442] 452
L15: 

    /**         goto "parse_done"*/
    goto G16;
L14: 

    /** label "parse_path"*/
G5:

    /**     if not qs_start then*/
    if (_qs_start_17970 != 0)
    goto L17; // [458] 478

    /**         path = url[pos..$]*/
    if (IS_SEQUENCE(_url_17922)){
            _10092 = SEQ_PTR(_url_17922)->length;
    }
    else {
        _10092 = 1;
    }
    rhs_slice_target = (object_ptr)&_path_17926;
    RHS_Slice(_url_17922, _pos_17931, _10092);

    /**         goto "parse_done"*/
    goto G16;
L17: 

    /**     if pos != qs_start then*/
    if (_pos_17931 == _qs_start_17970)
    goto L18; // [480] 496

    /**         path = url[pos..qs_start - 1]*/
    _10095 = _qs_start_17970 - 1;
    rhs_slice_target = (object_ptr)&_path_17926;
    RHS_Slice(_url_17922, _pos_17931, _10095);
L18: 

    /**     pos = qs_start*/
    _pos_17931 = _qs_start_17970;

    /** label "parse_query_string"*/
G19:

    /** 	query_string = url[qs_start + 1..$]*/
    _10098 = _qs_start_17970 + 1;
    if (_10098 > MAXINT){
        _10098 = NewDouble((double)_10098);
    }
    if (IS_SEQUENCE(_url_17922)){
            _10099 = SEQ_PTR(_url_17922)->length;
    }
    else {
        _10099 = 1;
    }
    rhs_slice_target = (object_ptr)&_query_string_17929;
    RHS_Slice(_url_17922, _10098, _10099);

    /**     if querystring_also and length(query_string) then*/
    if (_querystring_also_17923 == 0) {
        goto L1A; // [523] 541
    }
    if (IS_SEQUENCE(_query_string_17929)){
            _10102 = SEQ_PTR(_query_string_17929)->length;
    }
    else {
        _10102 = 1;
    }
    if (_10102 == 0)
    {
        _10102 = NOVALUE;
        goto L1A; // [531] 541
    }
    else{
        _10102 = NOVALUE;
    }

    /**         query_string = parse_querystring(query_string)*/
    Ref(_query_string_17929);
    _0 = _query_string_17929;
    _query_string_17929 = _34parse_querystring(_query_string_17929);
    DeRef(_0);
L1A: 

    /** label "parse_done"*/
G16:

    /**     return { protocol, host_name, port, path, user_name, password, query_string }*/
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_protocol_17924);
    *((int *)(_2+4)) = _protocol_17924;
    Ref(_host_name_17925);
    *((int *)(_2+8)) = _host_name_17925;
    *((int *)(_2+12)) = _port_17930;
    Ref(_path_17926);
    *((int *)(_2+16)) = _path_17926;
    Ref(_user_name_17927);
    *((int *)(_2+20)) = _user_name_17927;
    Ref(_password_17928);
    *((int *)(_2+24)) = _password_17928;
    Ref(_query_string_17929);
    *((int *)(_2+28)) = _query_string_17929;
    _10104 = MAKE_SEQ(_1);
    DeRefDS(_url_17922);
    DeRefDS(_protocol_17924);
    DeRef(_host_name_17925);
    DeRef(_path_17926);
    DeRef(_user_name_17927);
    DeRef(_password_17928);
    DeRef(_query_string_17929);
    DeRef(_10044);
    _10044 = NOVALUE;
    DeRef(_10060);
    _10060 = NOVALUE;
    DeRef(_10063);
    _10063 = NOVALUE;
    DeRef(_10065);
    _10065 = NOVALUE;
    DeRef(_10066);
    _10066 = NOVALUE;
    DeRef(_10068);
    _10068 = NOVALUE;
    DeRef(_10074);
    _10074 = NOVALUE;
    DeRef(_10080);
    _10080 = NOVALUE;
    DeRef(_10082);
    _10082 = NOVALUE;
    DeRef(_10087);
    _10087 = NOVALUE;
    DeRef(_10095);
    _10095 = NOVALUE;
    DeRef(_10098);
    _10098 = NOVALUE;
    return _10104;
    ;
}


int _34encode(int _what_18030, int _spacecode_18031)
{
    int _encoded_18032 = NOVALUE;
    int _junk_18033 = NOVALUE;
    int _junk1_18034 = NOVALUE;
    int _junk2_18035 = NOVALUE;
    int _10125 = NOVALUE;
    int _10124 = NOVALUE;
    int _10123 = NOVALUE;
    int _10122 = NOVALUE;
    int _10121 = NOVALUE;
    int _10118 = NOVALUE;
    int _10117 = NOVALUE;
    int _10113 = NOVALUE;
    int _10112 = NOVALUE;
    int _10110 = NOVALUE;
    int _10109 = NOVALUE;
    int _10108 = NOVALUE;
    int _10107 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence encoded = ""*/
    RefDS(_5);
    DeRef(_encoded_18032);
    _encoded_18032 = _5;

    /** 	object junk = "", junk1, junk2*/
    RefDS(_5);
    DeRef(_junk_18033);
    _junk_18033 = _5;

    /** 	for idx = 1 to length(what) do*/
    if (IS_SEQUENCE(_what_18030)){
            _10107 = SEQ_PTR(_what_18030)->length;
    }
    else {
        _10107 = 1;
    }
    {
        int _idx_18037;
        _idx_18037 = 1;
L1: 
        if (_idx_18037 > _10107){
            goto L2; // [22] 145
        }

        /** 		if find(what[idx],alphanum) then*/
        _2 = (int)SEQ_PTR(_what_18030);
        _10108 = (int)*(((s1_ptr)_2)->base + _idx_18037);
        _10109 = find_from(_10108, _34alphanum_18024, 1);
        _10108 = NOVALUE;
        if (_10109 == 0)
        {
            _10109 = NOVALUE;
            goto L3; // [40] 56
        }
        else{
            _10109 = NOVALUE;
        }

        /** 			encoded &= what[idx]*/
        _2 = (int)SEQ_PTR(_what_18030);
        _10110 = (int)*(((s1_ptr)_2)->base + _idx_18037);
        if (IS_SEQUENCE(_encoded_18032) && IS_ATOM(_10110)) {
            Ref(_10110);
            Append(&_encoded_18032, _encoded_18032, _10110);
        }
        else if (IS_ATOM(_encoded_18032) && IS_SEQUENCE(_10110)) {
        }
        else {
            Concat((object_ptr)&_encoded_18032, _encoded_18032, _10110);
        }
        _10110 = NOVALUE;
        goto L4; // [53] 138
L3: 

        /** 		elsif equal(what[idx],' ') then*/
        _2 = (int)SEQ_PTR(_what_18030);
        _10112 = (int)*(((s1_ptr)_2)->base + _idx_18037);
        if (_10112 == 32)
        _10113 = 1;
        else if (IS_ATOM_INT(_10112) && IS_ATOM_INT(32))
        _10113 = 0;
        else
        _10113 = (compare(_10112, 32) == 0);
        _10112 = NOVALUE;
        if (_10113 == 0)
        {
            _10113 = NOVALUE;
            goto L5; // [66] 78
        }
        else{
            _10113 = NOVALUE;
        }

        /** 			encoded &= spacecode*/
        Concat((object_ptr)&_encoded_18032, _encoded_18032, _spacecode_18031);
        goto L4; // [75] 138
L5: 

        /** 		elsif 1 then*/

        /** 			junk = what[idx]*/
        DeRef(_junk_18033);
        _2 = (int)SEQ_PTR(_what_18030);
        _junk_18033 = (int)*(((s1_ptr)_2)->base + _idx_18037);
        Ref(_junk_18033);

        /** 			junk1 = floor(junk / 16)*/
        DeRef(_junk1_18034);
        if (IS_ATOM_INT(_junk_18033)) {
            if (16 > 0 && _junk_18033 >= 0) {
                _junk1_18034 = _junk_18033 / 16;
            }
            else {
                temp_dbl = floor((double)_junk_18033 / (double)16);
                if (_junk_18033 != MININT)
                _junk1_18034 = (long)temp_dbl;
                else
                _junk1_18034 = NewDouble(temp_dbl);
            }
        }
        else {
            _2 = binary_op(DIVIDE, _junk_18033, 16);
            _junk1_18034 = unary_op(FLOOR, _2);
            DeRef(_2);
        }

        /** 			junk2 = floor(junk - (junk1 * 16))*/
        if (IS_ATOM_INT(_junk1_18034)) {
            if (_junk1_18034 == (short)_junk1_18034)
            _10117 = _junk1_18034 * 16;
            else
            _10117 = NewDouble(_junk1_18034 * (double)16);
        }
        else {
            _10117 = binary_op(MULTIPLY, _junk1_18034, 16);
        }
        if (IS_ATOM_INT(_junk_18033) && IS_ATOM_INT(_10117)) {
            _10118 = _junk_18033 - _10117;
            if ((long)((unsigned long)_10118 +(unsigned long) HIGH_BITS) >= 0){
                _10118 = NewDouble((double)_10118);
            }
        }
        else {
            _10118 = binary_op(MINUS, _junk_18033, _10117);
        }
        DeRef(_10117);
        _10117 = NOVALUE;
        DeRef(_junk2_18035);
        if (IS_ATOM_INT(_10118))
        _junk2_18035 = e_floor(_10118);
        else
        _junk2_18035 = unary_op(FLOOR, _10118);
        DeRef(_10118);
        _10118 = NOVALUE;

        /** 			encoded &= "%" & hexnums[junk1+1] & hexnums[junk2+1]*/
        if (IS_ATOM_INT(_junk1_18034)) {
            _10121 = _junk1_18034 + 1;
        }
        else
        _10121 = binary_op(PLUS, 1, _junk1_18034);
        _2 = (int)SEQ_PTR(_34hexnums_18026);
        if (!IS_ATOM_INT(_10121)){
            _10122 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_10121)->dbl));
        }
        else{
            _10122 = (int)*(((s1_ptr)_2)->base + _10121);
        }
        if (IS_ATOM_INT(_junk2_18035)) {
            _10123 = _junk2_18035 + 1;
        }
        else
        _10123 = binary_op(PLUS, 1, _junk2_18035);
        _2 = (int)SEQ_PTR(_34hexnums_18026);
        if (!IS_ATOM_INT(_10123)){
            _10124 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_10123)->dbl));
        }
        else{
            _10124 = (int)*(((s1_ptr)_2)->base + _10123);
        }
        {
            int concat_list[3];

            concat_list[0] = _10124;
            concat_list[1] = _10122;
            concat_list[2] = _10120;
            Concat_N((object_ptr)&_10125, concat_list, 3);
        }
        _10124 = NOVALUE;
        _10122 = NOVALUE;
        Concat((object_ptr)&_encoded_18032, _encoded_18032, _10125);
        DeRefDS(_10125);
        _10125 = NOVALUE;
L4: 

        /** 	end for*/
        _idx_18037 = _idx_18037 + 1;
        goto L1; // [140] 29
L2: 
        ;
    }

    /** 	return encoded*/
    DeRefDS(_what_18030);
    DeRefDS(_spacecode_18031);
    DeRef(_junk_18033);
    DeRef(_junk1_18034);
    DeRef(_junk2_18035);
    DeRef(_10121);
    _10121 = NOVALUE;
    DeRef(_10123);
    _10123 = NOVALUE;
    return _encoded_18032;
    ;
}


int _34decode(int _what_18063)
{
    int _k_18064 = NOVALUE;
    int _value_inlined_value_at_126_18095 = NOVALUE;
    int _st_inlined_value_at_123_18094 = NOVALUE;
    int _value_inlined_value_at_216_18112 = NOVALUE;
    int _st_inlined_value_at_213_18111 = NOVALUE;
    int _10166 = NOVALUE;
    int _10165 = NOVALUE;
    int _10164 = NOVALUE;
    int _10163 = NOVALUE;
    int _10162 = NOVALUE;
    int _10161 = NOVALUE;
    int _10160 = NOVALUE;
    int _10159 = NOVALUE;
    int _10158 = NOVALUE;
    int _10157 = NOVALUE;
    int _10156 = NOVALUE;
    int _10154 = NOVALUE;
    int _10153 = NOVALUE;
    int _10152 = NOVALUE;
    int _10151 = NOVALUE;
    int _10150 = NOVALUE;
    int _10149 = NOVALUE;
    int _10148 = NOVALUE;
    int _10147 = NOVALUE;
    int _10146 = NOVALUE;
    int _10145 = NOVALUE;
    int _10142 = NOVALUE;
    int _10141 = NOVALUE;
    int _10139 = NOVALUE;
    int _10138 = NOVALUE;
    int _10137 = NOVALUE;
    int _10136 = NOVALUE;
    int _10135 = NOVALUE;
    int _10133 = NOVALUE;
    int _10131 = NOVALUE;
    int _10129 = NOVALUE;
    int _10127 = NOVALUE;
    int _0, _1, _2;
    

    /**   integer k = 1*/
    _k_18064 = 1;

    /**   while k <= length(what) do*/
L1: 
    if (IS_SEQUENCE(_what_18063)){
            _10127 = SEQ_PTR(_what_18063)->length;
    }
    else {
        _10127 = 1;
    }
    if (_k_18064 > _10127)
    goto L2; // [18] 291

    /**     if what[k] = '+' then*/
    _2 = (int)SEQ_PTR(_what_18063);
    _10129 = (int)*(((s1_ptr)_2)->base + _k_18064);
    if (binary_op_a(NOTEQ, _10129, 43)){
        _10129 = NOVALUE;
        goto L3; // [28] 41
    }
    _10129 = NOVALUE;

    /**       what[k] = ' ' -- space is a special case, converts into +*/
    _2 = (int)SEQ_PTR(_what_18063);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _what_18063 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_18064);
    _1 = *(int *)_2;
    *(int *)_2 = 32;
    DeRef(_1);
    goto L4; // [38] 278
L3: 

    /**     elsif what[k] = '%' then*/
    _2 = (int)SEQ_PTR(_what_18063);
    _10131 = (int)*(((s1_ptr)_2)->base + _k_18064);
    if (binary_op_a(NOTEQ, _10131, 37)){
        _10131 = NOVALUE;
        goto L5; // [47] 277
    }
    _10131 = NOVALUE;

    /**       if k = length(what) then*/
    if (IS_SEQUENCE(_what_18063)){
            _10133 = SEQ_PTR(_what_18063)->length;
    }
    else {
        _10133 = 1;
    }
    if (_k_18064 != _10133)
    goto L6; // [56] 90

    /**         what = what[1..k-1] & what[k+1 .. $]*/
    _10135 = _k_18064 - 1;
    rhs_slice_target = (object_ptr)&_10136;
    RHS_Slice(_what_18063, 1, _10135);
    _10137 = _k_18064 + 1;
    if (_10137 > MAXINT){
        _10137 = NewDouble((double)_10137);
    }
    if (IS_SEQUENCE(_what_18063)){
            _10138 = SEQ_PTR(_what_18063)->length;
    }
    else {
        _10138 = 1;
    }
    rhs_slice_target = (object_ptr)&_10139;
    RHS_Slice(_what_18063, _10137, _10138);
    Concat((object_ptr)&_what_18063, _10136, _10139);
    DeRefDS(_10136);
    _10136 = NOVALUE;
    DeRef(_10136);
    _10136 = NOVALUE;
    DeRefDS(_10139);
    _10139 = NOVALUE;
    goto L4; // [87] 278
L6: 

    /**       elsif k+1 = length(what) then*/
    _10141 = _k_18064 + 1;
    if (_10141 > MAXINT){
        _10141 = NewDouble((double)_10141);
    }
    if (IS_SEQUENCE(_what_18063)){
            _10142 = SEQ_PTR(_what_18063)->length;
    }
    else {
        _10142 = 1;
    }
    if (binary_op_a(NOTEQ, _10141, _10142)){
        DeRef(_10141);
        _10141 = NOVALUE;
        _10142 = NOVALUE;
        goto L7; // [99] 187
    }
    DeRef(_10141);
    _10141 = NOVALUE;
    _10142 = NOVALUE;

    /**         what[k] = stdget:value("#0" & decode_upper(what[k+1]))*/
    _10145 = _k_18064 + 1;
    _2 = (int)SEQ_PTR(_what_18063);
    _10146 = (int)*(((s1_ptr)_2)->base + _10145);
    Ref(_10146);
    _10147 = _34decode_upper(_10146);
    _10146 = NOVALUE;
    if (IS_SEQUENCE(_10144) && IS_ATOM(_10147)) {
        Ref(_10147);
        Append(&_10148, _10144, _10147);
    }
    else if (IS_ATOM(_10144) && IS_SEQUENCE(_10147)) {
    }
    else {
        Concat((object_ptr)&_10148, _10144, _10147);
    }
    DeRef(_10147);
    _10147 = NOVALUE;
    DeRef(_st_inlined_value_at_123_18094);
    _st_inlined_value_at_123_18094 = _10148;
    _10148 = NOVALUE;
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_2711)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_2711)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_2711)) && (DBL_PTR(_14GET_SHORT_ANSWER_2711)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_2711);
        _14GET_SHORT_ANSWER_2711 = _1;
    }
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_2711)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_2711)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_2711)) && (DBL_PTR(_14GET_SHORT_ANSWER_2711)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_2711);
        _14GET_SHORT_ANSWER_2711 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    RefDS(_st_inlined_value_at_123_18094);
    _0 = _value_inlined_value_at_126_18095;
    _value_inlined_value_at_126_18095 = _14get_value(_st_inlined_value_at_123_18094, 1, _14GET_SHORT_ANSWER_2711);
    DeRef(_0);
    DeRef(_st_inlined_value_at_123_18094);
    _st_inlined_value_at_123_18094 = NOVALUE;
    Ref(_value_inlined_value_at_126_18095);
    _2 = (int)SEQ_PTR(_what_18063);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _what_18063 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_18064);
    _1 = *(int *)_2;
    *(int *)_2 = _value_inlined_value_at_126_18095;
    DeRef(_1);

    /**         what[k] = what[k][2]*/
    _2 = (int)SEQ_PTR(_what_18063);
    _10149 = (int)*(((s1_ptr)_2)->base + _k_18064);
    _2 = (int)SEQ_PTR(_10149);
    _10150 = (int)*(((s1_ptr)_2)->base + 2);
    _10149 = NOVALUE;
    Ref(_10150);
    _2 = (int)SEQ_PTR(_what_18063);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _what_18063 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_18064);
    _1 = *(int *)_2;
    *(int *)_2 = _10150;
    if( _1 != _10150 ){
        DeRef(_1);
    }
    _10150 = NOVALUE;

    /**         what = what[1..k] & what[k+2 .. $]*/
    rhs_slice_target = (object_ptr)&_10151;
    RHS_Slice(_what_18063, 1, _k_18064);
    _10152 = _k_18064 + 2;
    if ((long)((unsigned long)_10152 + (unsigned long)HIGH_BITS) >= 0) 
    _10152 = NewDouble((double)_10152);
    if (IS_SEQUENCE(_what_18063)){
            _10153 = SEQ_PTR(_what_18063)->length;
    }
    else {
        _10153 = 1;
    }
    rhs_slice_target = (object_ptr)&_10154;
    RHS_Slice(_what_18063, _10152, _10153);
    Concat((object_ptr)&_what_18063, _10151, _10154);
    DeRefDS(_10151);
    _10151 = NOVALUE;
    DeRef(_10151);
    _10151 = NOVALUE;
    DeRefDS(_10154);
    _10154 = NOVALUE;
    goto L4; // [184] 278
L7: 

    /**         what[k] = stdget:value("#" & decode_upper(what[k+1..k+2]))*/
    _10156 = _k_18064 + 1;
    if (_10156 > MAXINT){
        _10156 = NewDouble((double)_10156);
    }
    _10157 = _k_18064 + 2;
    rhs_slice_target = (object_ptr)&_10158;
    RHS_Slice(_what_18063, _10156, _10157);
    _10159 = _34decode_upper(_10158);
    _10158 = NOVALUE;
    if (IS_SEQUENCE(_270) && IS_ATOM(_10159)) {
        Ref(_10159);
        Append(&_10160, _270, _10159);
    }
    else if (IS_ATOM(_270) && IS_SEQUENCE(_10159)) {
    }
    else {
        Concat((object_ptr)&_10160, _270, _10159);
    }
    DeRef(_10159);
    _10159 = NOVALUE;
    DeRef(_st_inlined_value_at_213_18111);
    _st_inlined_value_at_213_18111 = _10160;
    _10160 = NOVALUE;
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_2711)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_2711)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_2711)) && (DBL_PTR(_14GET_SHORT_ANSWER_2711)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_2711);
        _14GET_SHORT_ANSWER_2711 = _1;
    }
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_2711)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_2711)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_2711)) && (DBL_PTR(_14GET_SHORT_ANSWER_2711)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_2711);
        _14GET_SHORT_ANSWER_2711 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    RefDS(_st_inlined_value_at_213_18111);
    _0 = _value_inlined_value_at_216_18112;
    _value_inlined_value_at_216_18112 = _14get_value(_st_inlined_value_at_213_18111, 1, _14GET_SHORT_ANSWER_2711);
    DeRef(_0);
    DeRef(_st_inlined_value_at_213_18111);
    _st_inlined_value_at_213_18111 = NOVALUE;
    Ref(_value_inlined_value_at_216_18112);
    _2 = (int)SEQ_PTR(_what_18063);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _what_18063 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_18064);
    _1 = *(int *)_2;
    *(int *)_2 = _value_inlined_value_at_216_18112;
    DeRef(_1);

    /**         what[k] = what[k][2]*/
    _2 = (int)SEQ_PTR(_what_18063);
    _10161 = (int)*(((s1_ptr)_2)->base + _k_18064);
    _2 = (int)SEQ_PTR(_10161);
    _10162 = (int)*(((s1_ptr)_2)->base + 2);
    _10161 = NOVALUE;
    Ref(_10162);
    _2 = (int)SEQ_PTR(_what_18063);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _what_18063 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_18064);
    _1 = *(int *)_2;
    *(int *)_2 = _10162;
    if( _1 != _10162 ){
        DeRef(_1);
    }
    _10162 = NOVALUE;

    /**         what = what[1..k] & what[k+3 .. $]*/
    rhs_slice_target = (object_ptr)&_10163;
    RHS_Slice(_what_18063, 1, _k_18064);
    _10164 = _k_18064 + 3;
    if ((long)((unsigned long)_10164 + (unsigned long)HIGH_BITS) >= 0) 
    _10164 = NewDouble((double)_10164);
    if (IS_SEQUENCE(_what_18063)){
            _10165 = SEQ_PTR(_what_18063)->length;
    }
    else {
        _10165 = 1;
    }
    rhs_slice_target = (object_ptr)&_10166;
    RHS_Slice(_what_18063, _10164, _10165);
    Concat((object_ptr)&_what_18063, _10163, _10166);
    DeRefDS(_10163);
    _10163 = NOVALUE;
    DeRef(_10163);
    _10163 = NOVALUE;
    DeRefDS(_10166);
    _10166 = NOVALUE;
    goto L4; // [274] 278
L5: 
L4: 

    /**     k += 1*/
    _k_18064 = _k_18064 + 1;

    /**   end while*/
    goto L1; // [288] 15
L2: 

    /**   return what*/
    DeRef(_10135);
    _10135 = NOVALUE;
    DeRef(_10145);
    _10145 = NOVALUE;
    DeRef(_10137);
    _10137 = NOVALUE;
    DeRef(_10156);
    _10156 = NOVALUE;
    DeRef(_10152);
    _10152 = NOVALUE;
    DeRef(_10157);
    _10157 = NOVALUE;
    DeRef(_10164);
    _10164 = NOVALUE;
    return _what_18063;
    ;
}


int _34decode_upper(int _x_18124)
{
    int _10174 = NOVALUE;
    int _10173 = NOVALUE;
    int _10172 = NOVALUE;
    int _10171 = NOVALUE;
    int _10170 = NOVALUE;
    int _10169 = NOVALUE;
    int _0, _1, _2;
    

    /**   return x - (x >= 'a' and x <= 'z') * ('a' - 'A')*/
    if (IS_ATOM_INT(_x_18124)) {
        _10169 = (_x_18124 >= 97);
    }
    else {
        _10169 = binary_op(GREATEREQ, _x_18124, 97);
    }
    if (IS_ATOM_INT(_x_18124)) {
        _10170 = (_x_18124 <= 122);
    }
    else {
        _10170 = binary_op(LESSEQ, _x_18124, 122);
    }
    if (IS_ATOM_INT(_10169) && IS_ATOM_INT(_10170)) {
        _10171 = (_10169 != 0 && _10170 != 0);
    }
    else {
        _10171 = binary_op(AND, _10169, _10170);
    }
    DeRef(_10169);
    _10169 = NOVALUE;
    DeRef(_10170);
    _10170 = NOVALUE;
    _10172 = 32;
    if (IS_ATOM_INT(_10171)) {
        if (_10171 == (short)_10171)
        _10173 = _10171 * 32;
        else
        _10173 = NewDouble(_10171 * (double)32);
    }
    else {
        _10173 = binary_op(MULTIPLY, _10171, 32);
    }
    DeRef(_10171);
    _10171 = NOVALUE;
    _10172 = NOVALUE;
    if (IS_ATOM_INT(_x_18124) && IS_ATOM_INT(_10173)) {
        _10174 = _x_18124 - _10173;
        if ((long)((unsigned long)_10174 +(unsigned long) HIGH_BITS) >= 0){
            _10174 = NewDouble((double)_10174);
        }
    }
    else {
        _10174 = binary_op(MINUS, _x_18124, _10173);
    }
    DeRef(_10173);
    _10173 = NOVALUE;
    DeRef(_x_18124);
    return _10174;
    ;
}



// 0x44FA2291
