// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _31version_node(int _full_13982)
{
    int _7654 = NOVALUE;
    int _7653 = NOVALUE;
    int _7652 = NOVALUE;
    int _7651 = NOVALUE;
    int _7650 = NOVALUE;
    int _7649 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or length(version_info[NODE]) < 12 then*/
    if (0 != 0) {
        goto L1; // [7] 31
    }
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7649 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7649)){
            _7650 = SEQ_PTR(_7649)->length;
    }
    else {
        _7650 = 1;
    }
    _7649 = NOVALUE;
    _7651 = (_7650 < 12);
    _7650 = NOVALUE;
    if (_7651 == 0)
    {
        DeRef(_7651);
        _7651 = NOVALUE;
        goto L2; // [27] 46
    }
    else{
        DeRef(_7651);
        _7651 = NOVALUE;
    }
L1: 

    /** 		return version_info[NODE]*/
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7652 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_7652);
    _7649 = NOVALUE;
    return _7652;
L2: 

    /** 	return version_info[NODE][1..12]*/
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7653 = (int)*(((s1_ptr)_2)->base + 5);
    rhs_slice_target = (object_ptr)&_7654;
    RHS_Slice(_7653, 1, 12);
    _7653 = NOVALUE;
    _7649 = NOVALUE;
    _7652 = NOVALUE;
    return _7654;
    ;
}


int _31version_date(int _full_13996)
{
    int _7663 = NOVALUE;
    int _7662 = NOVALUE;
    int _7661 = NOVALUE;
    int _7660 = NOVALUE;
    int _7659 = NOVALUE;
    int _7658 = NOVALUE;
    int _7656 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental or length(version_info[REVISION_DATE]) < 10 then*/
    if (_full_13996 != 0) {
        _7656 = 1;
        goto L1; // [7] 17
    }
    _7656 = (_31is_developmental_13946 != 0);
L1: 
    if (_7656 != 0) {
        goto L2; // [17] 41
    }
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7658 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7658)){
            _7659 = SEQ_PTR(_7658)->length;
    }
    else {
        _7659 = 1;
    }
    _7658 = NOVALUE;
    _7660 = (_7659 < 10);
    _7659 = NOVALUE;
    if (_7660 == 0)
    {
        DeRef(_7660);
        _7660 = NOVALUE;
        goto L3; // [37] 56
    }
    else{
        DeRef(_7660);
        _7660 = NOVALUE;
    }
L2: 

    /** 		return version_info[REVISION_DATE]*/
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7661 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_7661);
    _7658 = NOVALUE;
    return _7661;
L3: 

    /** 	return version_info[REVISION_DATE][1..10]*/
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7662 = (int)*(((s1_ptr)_2)->base + 7);
    rhs_slice_target = (object_ptr)&_7663;
    RHS_Slice(_7662, 1, 10);
    _7662 = NOVALUE;
    _7658 = NOVALUE;
    _7661 = NOVALUE;
    return _7663;
    ;
}


int _31version_string(int _full_14011)
{
    int _version_revision_inlined_version_revision_at_51_14020 = NOVALUE;
    int _7683 = NOVALUE;
    int _7682 = NOVALUE;
    int _7681 = NOVALUE;
    int _7680 = NOVALUE;
    int _7679 = NOVALUE;
    int _7678 = NOVALUE;
    int _7677 = NOVALUE;
    int _7676 = NOVALUE;
    int _7674 = NOVALUE;
    int _7673 = NOVALUE;
    int _7672 = NOVALUE;
    int _7671 = NOVALUE;
    int _7670 = NOVALUE;
    int _7669 = NOVALUE;
    int _7668 = NOVALUE;
    int _7667 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental then*/
    if (0 != 0) {
        goto L1; // [7] 18
    }
    if (_31is_developmental_13946 == 0)
    {
        goto L2; // [14] 92
    }
    else{
    }
L1: 

    /** 		return sprintf("%d.%d.%d %s (%d:%s, %s)", {*/
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7667 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7668 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7669 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7670 = (int)*(((s1_ptr)_2)->base + 4);

    /** 	return version_info[REVISION]*/
    DeRef(_version_revision_inlined_version_revision_at_51_14020);
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _version_revision_inlined_version_revision_at_51_14020 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_version_revision_inlined_version_revision_at_51_14020);
    _7671 = _31version_node(0);
    _7672 = _31version_date(_full_14011);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7667);
    *((int *)(_2+4)) = _7667;
    Ref(_7668);
    *((int *)(_2+8)) = _7668;
    Ref(_7669);
    *((int *)(_2+12)) = _7669;
    Ref(_7670);
    *((int *)(_2+16)) = _7670;
    Ref(_version_revision_inlined_version_revision_at_51_14020);
    *((int *)(_2+20)) = _version_revision_inlined_version_revision_at_51_14020;
    *((int *)(_2+24)) = _7671;
    *((int *)(_2+28)) = _7672;
    _7673 = MAKE_SEQ(_1);
    _7672 = NOVALUE;
    _7671 = NOVALUE;
    _7670 = NOVALUE;
    _7669 = NOVALUE;
    _7668 = NOVALUE;
    _7667 = NOVALUE;
    _7674 = EPrintf(-9999999, _7666, _7673);
    DeRefDS(_7673);
    _7673 = NOVALUE;
    return _7674;
    goto L3; // [89] 152
L2: 

    /** 		return sprintf("%d.%d.%d %s (%s, %s)", {*/
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7676 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7677 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7678 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_31version_info_13944);
    _7679 = (int)*(((s1_ptr)_2)->base + 4);
    _7680 = _31version_node(0);
    _7681 = _31version_date(_full_14011);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7676);
    *((int *)(_2+4)) = _7676;
    Ref(_7677);
    *((int *)(_2+8)) = _7677;
    Ref(_7678);
    *((int *)(_2+12)) = _7678;
    Ref(_7679);
    *((int *)(_2+16)) = _7679;
    *((int *)(_2+20)) = _7680;
    *((int *)(_2+24)) = _7681;
    _7682 = MAKE_SEQ(_1);
    _7681 = NOVALUE;
    _7680 = NOVALUE;
    _7679 = NOVALUE;
    _7678 = NOVALUE;
    _7677 = NOVALUE;
    _7676 = NOVALUE;
    _7683 = EPrintf(-9999999, _7675, _7682);
    DeRefDS(_7682);
    _7682 = NOVALUE;
    DeRef(_7674);
    _7674 = NOVALUE;
    return _7683;
L3: 
    ;
}



// 0x35512850
