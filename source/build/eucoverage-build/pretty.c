// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _23pretty_out(int _text_8153)
{
    int _4470 = NOVALUE;
    int _4468 = NOVALUE;
    int _4466 = NOVALUE;
    int _4465 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pretty_line &= text*/
    if (IS_SEQUENCE(_23pretty_line_8150) && IS_ATOM(_text_8153)) {
        Ref(_text_8153);
        Append(&_23pretty_line_8150, _23pretty_line_8150, _text_8153);
    }
    else if (IS_ATOM(_23pretty_line_8150) && IS_SEQUENCE(_text_8153)) {
    }
    else {
        Concat((object_ptr)&_23pretty_line_8150, _23pretty_line_8150, _text_8153);
    }

    /** 	if equal(text, '\n') and pretty_printing then*/
    if (_text_8153 == 10)
    _4465 = 1;
    else if (IS_ATOM_INT(_text_8153) && IS_ATOM_INT(10))
    _4465 = 0;
    else
    _4465 = (compare(_text_8153, 10) == 0);
    if (_4465 == 0) {
        goto L1; // [15] 52
    }
    if (_23pretty_printing_8147 == 0)
    {
        goto L1; // [22] 52
    }
    else{
    }

    /** 		puts(pretty_file, pretty_line)*/
    EPuts(_23pretty_file_8138, _23pretty_line_8150); // DJP 

    /** 		pretty_line = ""*/
    RefDS(_5);
    DeRefDS(_23pretty_line_8150);
    _23pretty_line_8150 = _5;

    /** 		pretty_line_count += 1*/
    _23pretty_line_count_8143 = _23pretty_line_count_8143 + 1;
L1: 

    /** 	if atom(text) then*/
    _4468 = IS_ATOM(_text_8153);
    if (_4468 == 0)
    {
        _4468 = NOVALUE;
        goto L2; // [57] 73
    }
    else{
        _4468 = NOVALUE;
    }

    /** 		pretty_chars += 1*/
    _23pretty_chars_8135 = _23pretty_chars_8135 + 1;
    goto L3; // [70] 87
L2: 

    /** 		pretty_chars += length(text)*/
    if (IS_SEQUENCE(_text_8153)){
            _4470 = SEQ_PTR(_text_8153)->length;
    }
    else {
        _4470 = 1;
    }
    _23pretty_chars_8135 = _23pretty_chars_8135 + _4470;
    _4470 = NOVALUE;
L3: 

    /** end procedure*/
    DeRef(_text_8153);
    return;
    ;
}


void _23cut_line(int _n_8167)
{
    int _4473 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not pretty_line_breaks then	*/
    if (_23pretty_line_breaks_8146 != 0)
    goto L1; // [9] 25

    /** 		pretty_chars = 0*/
    _23pretty_chars_8135 = 0;

    /** 		return*/
    return;
L1: 

    /** 	if pretty_chars + n > pretty_end_col then*/
    _4473 = _23pretty_chars_8135 + _n_8167;
    if ((long)((unsigned long)_4473 + (unsigned long)HIGH_BITS) >= 0) 
    _4473 = NewDouble((double)_4473);
    if (binary_op_a(LESSEQ, _4473, _23pretty_end_col_8134)){
        DeRef(_4473);
        _4473 = NOVALUE;
        goto L2; // [35] 52
    }
    DeRef(_4473);
    _4473 = NOVALUE;

    /** 		pretty_out('\n')*/
    _23pretty_out(10);

    /** 		pretty_chars = 0*/
    _23pretty_chars_8135 = 0;
L2: 

    /** end procedure*/
    return;
    ;
}


void _23indent()
{
    int _4481 = NOVALUE;
    int _4480 = NOVALUE;
    int _4479 = NOVALUE;
    int _4478 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if pretty_line_breaks = 0 then	*/
    if (_23pretty_line_breaks_8146 != 0)
    goto L1; // [5] 24

    /** 		pretty_chars = 0*/
    _23pretty_chars_8135 = 0;

    /** 		return*/
    return;
    goto L2; // [21] 89
L1: 

    /** 	elsif pretty_line_breaks = -1 then*/
    if (_23pretty_line_breaks_8146 != -1)
    goto L3; // [28] 40

    /** 		cut_line( 0 )*/
    _23cut_line(0);
    goto L2; // [37] 89
L3: 

    /** 		if pretty_chars > 0 then*/
    if (_23pretty_chars_8135 <= 0)
    goto L4; // [44] 61

    /** 			pretty_out('\n')*/
    _23pretty_out(10);

    /** 			pretty_chars = 0*/
    _23pretty_chars_8135 = 0;
L4: 

    /** 		pretty_out(repeat(' ', (pretty_start_col-1) + */
    _4478 = _23pretty_start_col_8136 - 1;
    if ((long)((unsigned long)_4478 +(unsigned long) HIGH_BITS) >= 0){
        _4478 = NewDouble((double)_4478);
    }
    if (_23pretty_level_8137 == (short)_23pretty_level_8137 && _23pretty_indent_8140 <= INT15 && _23pretty_indent_8140 >= -INT15)
    _4479 = _23pretty_level_8137 * _23pretty_indent_8140;
    else
    _4479 = NewDouble(_23pretty_level_8137 * (double)_23pretty_indent_8140);
    if (IS_ATOM_INT(_4478) && IS_ATOM_INT(_4479)) {
        _4480 = _4478 + _4479;
    }
    else {
        if (IS_ATOM_INT(_4478)) {
            _4480 = NewDouble((double)_4478 + DBL_PTR(_4479)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4479)) {
                _4480 = NewDouble(DBL_PTR(_4478)->dbl + (double)_4479);
            }
            else
            _4480 = NewDouble(DBL_PTR(_4478)->dbl + DBL_PTR(_4479)->dbl);
        }
    }
    DeRef(_4478);
    _4478 = NOVALUE;
    DeRef(_4479);
    _4479 = NOVALUE;
    _4481 = Repeat(32, _4480);
    DeRef(_4480);
    _4480 = NOVALUE;
    _23pretty_out(_4481);
    _4481 = NOVALUE;
L2: 

    /** end procedure*/
    return;
    ;
}


int _23esc_char(int _a_8188)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_8188)) {
        _1 = (long)(DBL_PTR(_a_8188)->dbl);
        if (UNIQUE(DBL_PTR(_a_8188)) && (DBL_PTR(_a_8188)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_8188);
        _a_8188 = _1;
    }

    /** 	switch a do*/
    _0 = _a_8188;
    switch ( _0 ){ 

        /** 		case'\t' then*/
        case 9:

        /** 			return `\t`*/
        RefDS(_4484);
        return _4484;
        goto L1; // [22] 83

        /** 		case'\n' then*/
        case 10:

        /** 			return `\n`*/
        RefDS(_4485);
        return _4485;
        goto L1; // [34] 83

        /** 		case'\r' then*/
        case 13:

        /** 			return `\r`*/
        RefDS(_4486);
        return _4486;
        goto L1; // [46] 83

        /** 		case'\\' then*/
        case 92:

        /** 			return `\\`*/
        RefDS(_4487);
        return _4487;
        goto L1; // [58] 83

        /** 		case'"' then*/
        case 34:

        /** 			return `\"`*/
        RefDS(_4488);
        return _4488;
        goto L1; // [70] 83

        /** 		case else*/
        default:

        /** 			return a*/
        return _a_8188;
    ;}L1: 
    ;
}


void _23rPrint(int _a_8204)
{
    int _sbuff_8205 = NOVALUE;
    int _multi_line_8206 = NOVALUE;
    int _all_ascii_8207 = NOVALUE;
    int _4544 = NOVALUE;
    int _4543 = NOVALUE;
    int _4542 = NOVALUE;
    int _4541 = NOVALUE;
    int _4537 = NOVALUE;
    int _4536 = NOVALUE;
    int _4535 = NOVALUE;
    int _4534 = NOVALUE;
    int _4532 = NOVALUE;
    int _4531 = NOVALUE;
    int _4529 = NOVALUE;
    int _4528 = NOVALUE;
    int _4526 = NOVALUE;
    int _4525 = NOVALUE;
    int _4524 = NOVALUE;
    int _4523 = NOVALUE;
    int _4522 = NOVALUE;
    int _4521 = NOVALUE;
    int _4520 = NOVALUE;
    int _4519 = NOVALUE;
    int _4518 = NOVALUE;
    int _4517 = NOVALUE;
    int _4516 = NOVALUE;
    int _4515 = NOVALUE;
    int _4514 = NOVALUE;
    int _4513 = NOVALUE;
    int _4512 = NOVALUE;
    int _4511 = NOVALUE;
    int _4510 = NOVALUE;
    int _4506 = NOVALUE;
    int _4505 = NOVALUE;
    int _4504 = NOVALUE;
    int _4503 = NOVALUE;
    int _4502 = NOVALUE;
    int _4501 = NOVALUE;
    int _4499 = NOVALUE;
    int _4498 = NOVALUE;
    int _4495 = NOVALUE;
    int _4494 = NOVALUE;
    int _4493 = NOVALUE;
    int _4490 = NOVALUE;
    int _4489 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _4489 = IS_ATOM(_a_8204);
    if (_4489 == 0)
    {
        _4489 = NOVALUE;
        goto L1; // [6] 176
    }
    else{
        _4489 = NOVALUE;
    }

    /** 		if integer(a) then*/
    if (IS_ATOM_INT(_a_8204))
    _4490 = 1;
    else if (IS_ATOM_DBL(_a_8204))
    _4490 = IS_ATOM_INT(DoubleToInt(_a_8204));
    else
    _4490 = 0;
    if (_4490 == 0)
    {
        _4490 = NOVALUE;
        goto L2; // [14] 157
    }
    else{
        _4490 = NOVALUE;
    }

    /** 			sbuff = sprintf(pretty_int_format, a)*/
    DeRef(_sbuff_8205);
    _sbuff_8205 = EPrintf(-9999999, _23pretty_int_format_8149, _a_8204);

    /** 			if pretty_ascii then */
    if (_23pretty_ascii_8139 == 0)
    {
        goto L3; // [29] 166
    }
    else{
    }

    /** 				if pretty_ascii >= 3 then */
    if (_23pretty_ascii_8139 < 3)
    goto L4; // [36] 103

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) then*/
    if (IS_ATOM_INT(_a_8204)) {
        _4493 = (_a_8204 >= _23pretty_ascii_min_8141);
    }
    else {
        _4493 = binary_op(GREATEREQ, _a_8204, _23pretty_ascii_min_8141);
    }
    if (IS_ATOM_INT(_4493)) {
        if (_4493 == 0) {
            _4494 = 0;
            goto L5; // [48] 62
        }
    }
    else {
        if (DBL_PTR(_4493)->dbl == 0.0) {
            _4494 = 0;
            goto L5; // [48] 62
        }
    }
    if (IS_ATOM_INT(_a_8204)) {
        _4495 = (_a_8204 <= _23pretty_ascii_max_8142);
    }
    else {
        _4495 = binary_op(LESSEQ, _a_8204, _23pretty_ascii_max_8142);
    }
    DeRef(_4494);
    if (IS_ATOM_INT(_4495))
    _4494 = (_4495 != 0);
    else
    _4494 = DBL_PTR(_4495)->dbl != 0.0;
L5: 
    if (_4494 == 0)
    {
        _4494 = NOVALUE;
        goto L6; // [62] 76
    }
    else{
        _4494 = NOVALUE;
    }

    /** 						sbuff = '\'' & a & '\''  -- display char only*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_8204;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_8205, concat_list, 3);
    }
    goto L3; // [73] 166
L6: 

    /** 					elsif find(a, "\t\n\r\\") then*/
    _4498 = find_from(_a_8204, _4497, 1);
    if (_4498 == 0)
    {
        _4498 = NOVALUE;
        goto L3; // [83] 166
    }
    else{
        _4498 = NOVALUE;
    }

    /** 						sbuff = '\'' & esc_char(a) & '\''  -- display char only*/
    Ref(_a_8204);
    _4499 = _23esc_char(_a_8204);
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _4499;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_8205, concat_list, 3);
    }
    DeRef(_4499);
    _4499 = NOVALUE;
    goto L3; // [100] 166
L4: 

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) and pretty_ascii < 2 then*/
    if (IS_ATOM_INT(_a_8204)) {
        _4501 = (_a_8204 >= _23pretty_ascii_min_8141);
    }
    else {
        _4501 = binary_op(GREATEREQ, _a_8204, _23pretty_ascii_min_8141);
    }
    if (IS_ATOM_INT(_4501)) {
        if (_4501 == 0) {
            DeRef(_4502);
            _4502 = 0;
            goto L7; // [111] 125
        }
    }
    else {
        if (DBL_PTR(_4501)->dbl == 0.0) {
            DeRef(_4502);
            _4502 = 0;
            goto L7; // [111] 125
        }
    }
    if (IS_ATOM_INT(_a_8204)) {
        _4503 = (_a_8204 <= _23pretty_ascii_max_8142);
    }
    else {
        _4503 = binary_op(LESSEQ, _a_8204, _23pretty_ascii_max_8142);
    }
    DeRef(_4502);
    if (IS_ATOM_INT(_4503))
    _4502 = (_4503 != 0);
    else
    _4502 = DBL_PTR(_4503)->dbl != 0.0;
L7: 
    if (_4502 == 0) {
        goto L3; // [125] 166
    }
    _4505 = (_23pretty_ascii_8139 < 2);
    if (_4505 == 0)
    {
        DeRef(_4505);
        _4505 = NOVALUE;
        goto L3; // [136] 166
    }
    else{
        DeRef(_4505);
        _4505 = NOVALUE;
    }

    /** 						sbuff &= '\'' & a & '\'' -- add to numeric display*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_8204;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_4506, concat_list, 3);
    }
    Concat((object_ptr)&_sbuff_8205, _sbuff_8205, _4506);
    DeRefDS(_4506);
    _4506 = NOVALUE;
    goto L3; // [154] 166
L2: 

    /** 			sbuff = sprintf(pretty_fp_format, a)*/
    DeRef(_sbuff_8205);
    _sbuff_8205 = EPrintf(-9999999, _23pretty_fp_format_8148, _a_8204);
L3: 

    /** 		pretty_out(sbuff)*/
    RefDS(_sbuff_8205);
    _23pretty_out(_sbuff_8205);
    goto L8; // [173] 551
L1: 

    /** 		cut_line(1)*/
    _23cut_line(1);

    /** 		multi_line = 0*/
    _multi_line_8206 = 0;

    /** 		all_ascii = pretty_ascii > 1*/
    _all_ascii_8207 = (_23pretty_ascii_8139 > 1);

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_8204)){
            _4510 = SEQ_PTR(_a_8204)->length;
    }
    else {
        _4510 = 1;
    }
    {
        int _i_8240;
        _i_8240 = 1;
L9: 
        if (_i_8240 > _4510){
            goto LA; // [203] 355
        }

        /** 			if sequence(a[i]) and length(a[i]) > 0 then*/
        _2 = (int)SEQ_PTR(_a_8204);
        _4511 = (int)*(((s1_ptr)_2)->base + _i_8240);
        _4512 = IS_SEQUENCE(_4511);
        _4511 = NOVALUE;
        if (_4512 == 0) {
            goto LB; // [219] 257
        }
        _2 = (int)SEQ_PTR(_a_8204);
        _4514 = (int)*(((s1_ptr)_2)->base + _i_8240);
        if (IS_SEQUENCE(_4514)){
                _4515 = SEQ_PTR(_4514)->length;
        }
        else {
            _4515 = 1;
        }
        _4514 = NOVALUE;
        _4516 = (_4515 > 0);
        _4515 = NOVALUE;
        if (_4516 == 0)
        {
            DeRef(_4516);
            _4516 = NOVALUE;
            goto LB; // [235] 257
        }
        else{
            DeRef(_4516);
            _4516 = NOVALUE;
        }

        /** 				multi_line = 1*/
        _multi_line_8206 = 1;

        /** 				all_ascii = 0*/
        _all_ascii_8207 = 0;

        /** 				exit*/
        goto LA; // [254] 355
LB: 

        /** 			if not integer(a[i]) or*/
        _2 = (int)SEQ_PTR(_a_8204);
        _4517 = (int)*(((s1_ptr)_2)->base + _i_8240);
        if (IS_ATOM_INT(_4517))
        _4518 = 1;
        else if (IS_ATOM_DBL(_4517))
        _4518 = IS_ATOM_INT(DoubleToInt(_4517));
        else
        _4518 = 0;
        _4517 = NOVALUE;
        _4519 = (_4518 == 0);
        _4518 = NOVALUE;
        if (_4519 != 0) {
            _4520 = 1;
            goto LC; // [269] 321
        }
        _2 = (int)SEQ_PTR(_a_8204);
        _4521 = (int)*(((s1_ptr)_2)->base + _i_8240);
        if (IS_ATOM_INT(_4521)) {
            _4522 = (_4521 < _23pretty_ascii_min_8141);
        }
        else {
            _4522 = binary_op(LESS, _4521, _23pretty_ascii_min_8141);
        }
        _4521 = NOVALUE;
        if (IS_ATOM_INT(_4522)) {
            if (_4522 == 0) {
                DeRef(_4523);
                _4523 = 0;
                goto LD; // [283] 317
            }
        }
        else {
            if (DBL_PTR(_4522)->dbl == 0.0) {
                DeRef(_4523);
                _4523 = 0;
                goto LD; // [283] 317
            }
        }
        _4524 = (_23pretty_ascii_8139 < 2);
        if (_4524 != 0) {
            _4525 = 1;
            goto LE; // [293] 313
        }
        _2 = (int)SEQ_PTR(_a_8204);
        _4526 = (int)*(((s1_ptr)_2)->base + _i_8240);
        _4528 = find_from(_4526, _4527, 1);
        _4526 = NOVALUE;
        _4529 = (_4528 == 0);
        _4528 = NOVALUE;
        _4525 = (_4529 != 0);
LE: 
        DeRef(_4523);
        _4523 = (_4525 != 0);
LD: 
        _4520 = (_4523 != 0);
LC: 
        if (_4520 != 0) {
            goto LF; // [321] 340
        }
        _2 = (int)SEQ_PTR(_a_8204);
        _4531 = (int)*(((s1_ptr)_2)->base + _i_8240);
        if (IS_ATOM_INT(_4531)) {
            _4532 = (_4531 > _23pretty_ascii_max_8142);
        }
        else {
            _4532 = binary_op(GREATER, _4531, _23pretty_ascii_max_8142);
        }
        _4531 = NOVALUE;
        if (_4532 == 0) {
            DeRef(_4532);
            _4532 = NOVALUE;
            goto L10; // [336] 348
        }
        else {
            if (!IS_ATOM_INT(_4532) && DBL_PTR(_4532)->dbl == 0.0){
                DeRef(_4532);
                _4532 = NOVALUE;
                goto L10; // [336] 348
            }
            DeRef(_4532);
            _4532 = NOVALUE;
        }
        DeRef(_4532);
        _4532 = NOVALUE;
LF: 

        /** 				all_ascii = 0*/
        _all_ascii_8207 = 0;
L10: 

        /** 		end for*/
        _i_8240 = _i_8240 + 1;
        goto L9; // [350] 210
LA: 
        ;
    }

    /** 		if all_ascii then*/
    if (_all_ascii_8207 == 0)
    {
        goto L11; // [357] 368
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _23pretty_out(34);
    goto L12; // [365] 374
L11: 

    /** 			pretty_out('{')*/
    _23pretty_out(123);
L12: 

    /** 		pretty_level += 1*/
    _23pretty_level_8137 = _23pretty_level_8137 + 1;

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_8204)){
            _4534 = SEQ_PTR(_a_8204)->length;
    }
    else {
        _4534 = 1;
    }
    {
        int _i_8270;
        _i_8270 = 1;
L13: 
        if (_i_8270 > _4534){
            goto L14; // [389] 511
        }

        /** 			if multi_line then*/
        if (_multi_line_8206 == 0)
        {
            goto L15; // [398] 406
        }
        else{
        }

        /** 				indent()*/
        _23indent();
L15: 

        /** 			if all_ascii then*/
        if (_all_ascii_8207 == 0)
        {
            goto L16; // [408] 427
        }
        else{
        }

        /** 				pretty_out(esc_char(a[i]))*/
        _2 = (int)SEQ_PTR(_a_8204);
        _4535 = (int)*(((s1_ptr)_2)->base + _i_8270);
        Ref(_4535);
        _4536 = _23esc_char(_4535);
        _4535 = NOVALUE;
        _23pretty_out(_4536);
        _4536 = NOVALUE;
        goto L17; // [424] 437
L16: 

        /** 				rPrint(a[i])*/
        _2 = (int)SEQ_PTR(_a_8204);
        _4537 = (int)*(((s1_ptr)_2)->base + _i_8270);
        Ref(_4537);
        _23rPrint(_4537);
        _4537 = NOVALUE;
L17: 

        /** 			if pretty_line_count >= pretty_line_max then*/
        if (_23pretty_line_count_8143 < _23pretty_line_max_8144)
        goto L18; // [443] 473

        /** 				if not pretty_dots then*/
        if (_23pretty_dots_8145 != 0)
        goto L19; // [451] 460

        /** 					pretty_out(" ...")*/
        RefDS(_4540);
        _23pretty_out(_4540);
L19: 

        /** 				pretty_dots = 1*/
        _23pretty_dots_8145 = 1;

        /** 				return*/
        DeRef(_a_8204);
        DeRef(_sbuff_8205);
        DeRef(_4493);
        _4493 = NOVALUE;
        DeRef(_4495);
        _4495 = NOVALUE;
        DeRef(_4501);
        _4501 = NOVALUE;
        DeRef(_4503);
        _4503 = NOVALUE;
        _4514 = NOVALUE;
        DeRef(_4519);
        _4519 = NOVALUE;
        DeRef(_4524);
        _4524 = NOVALUE;
        DeRef(_4522);
        _4522 = NOVALUE;
        DeRef(_4529);
        _4529 = NOVALUE;
        return;
L18: 

        /** 			if i != length(a) and not all_ascii then*/
        if (IS_SEQUENCE(_a_8204)){
                _4541 = SEQ_PTR(_a_8204)->length;
        }
        else {
            _4541 = 1;
        }
        _4542 = (_i_8270 != _4541);
        _4541 = NOVALUE;
        if (_4542 == 0) {
            goto L1A; // [482] 504
        }
        _4544 = (_all_ascii_8207 == 0);
        if (_4544 == 0)
        {
            DeRef(_4544);
            _4544 = NOVALUE;
            goto L1A; // [490] 504
        }
        else{
            DeRef(_4544);
            _4544 = NOVALUE;
        }

        /** 				pretty_out(',')*/
        _23pretty_out(44);

        /** 				cut_line(6)*/
        _23cut_line(6);
L1A: 

        /** 		end for*/
        _i_8270 = _i_8270 + 1;
        goto L13; // [506] 396
L14: 
        ;
    }

    /** 		pretty_level -= 1*/
    _23pretty_level_8137 = _23pretty_level_8137 - 1;

    /** 		if multi_line then*/
    if (_multi_line_8206 == 0)
    {
        goto L1B; // [523] 531
    }
    else{
    }

    /** 			indent()*/
    _23indent();
L1B: 

    /** 		if all_ascii then*/
    if (_all_ascii_8207 == 0)
    {
        goto L1C; // [533] 544
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _23pretty_out(34);
    goto L1D; // [541] 550
L1C: 

    /** 			pretty_out('}')*/
    _23pretty_out(125);
L1D: 
L8: 

    /** end procedure*/
    DeRef(_a_8204);
    DeRef(_sbuff_8205);
    DeRef(_4493);
    _4493 = NOVALUE;
    DeRef(_4495);
    _4495 = NOVALUE;
    DeRef(_4501);
    _4501 = NOVALUE;
    DeRef(_4503);
    _4503 = NOVALUE;
    _4514 = NOVALUE;
    DeRef(_4519);
    _4519 = NOVALUE;
    DeRef(_4524);
    _4524 = NOVALUE;
    DeRef(_4522);
    _4522 = NOVALUE;
    DeRef(_4529);
    _4529 = NOVALUE;
    DeRef(_4542);
    _4542 = NOVALUE;
    return;
    ;
}


void _23pretty(int _x_8318, int _options_8319)
{
    int _4565 = NOVALUE;
    int _4564 = NOVALUE;
    int _4563 = NOVALUE;
    int _4562 = NOVALUE;
    int _4560 = NOVALUE;
    int _4559 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(options) < length( PRETTY_DEFAULT ) then*/
    if (IS_SEQUENCE(_options_8319)){
            _4559 = SEQ_PTR(_options_8319)->length;
    }
    else {
        _4559 = 1;
    }
    _4560 = 10;
    if (_4559 >= 10)
    goto L1; // [13] 41

    /** 		options &= PRETTY_DEFAULT[length(options)+1..$]*/
    if (IS_SEQUENCE(_options_8319)){
            _4562 = SEQ_PTR(_options_8319)->length;
    }
    else {
        _4562 = 1;
    }
    _4563 = _4562 + 1;
    _4562 = NOVALUE;
    _4564 = 10;
    rhs_slice_target = (object_ptr)&_4565;
    RHS_Slice(_23PRETTY_DEFAULT_8295, _4563, 10);
    Concat((object_ptr)&_options_8319, _options_8319, _4565);
    DeRefDS(_4565);
    _4565 = NOVALUE;
L1: 

    /** 	pretty_ascii = options[DISPLAY_ASCII] */
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_ascii_8139 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_23pretty_ascii_8139))
    _23pretty_ascii_8139 = (long)DBL_PTR(_23pretty_ascii_8139)->dbl;

    /** 	pretty_indent = options[INDENT]*/
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_indent_8140 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_23pretty_indent_8140))
    _23pretty_indent_8140 = (long)DBL_PTR(_23pretty_indent_8140)->dbl;

    /** 	pretty_start_col = options[START_COLUMN]*/
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_start_col_8136 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_23pretty_start_col_8136))
    _23pretty_start_col_8136 = (long)DBL_PTR(_23pretty_start_col_8136)->dbl;

    /** 	pretty_end_col = options[WRAP]*/
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_end_col_8134 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_23pretty_end_col_8134))
    _23pretty_end_col_8134 = (long)DBL_PTR(_23pretty_end_col_8134)->dbl;

    /** 	pretty_int_format = options[INT_FORMAT]*/
    DeRef(_23pretty_int_format_8149);
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_int_format_8149 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_23pretty_int_format_8149);

    /** 	pretty_fp_format = options[FP_FORMAT]*/
    DeRef(_23pretty_fp_format_8148);
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_fp_format_8148 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_23pretty_fp_format_8148);

    /** 	pretty_ascii_min = options[MIN_ASCII]*/
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_ascii_min_8141 = (int)*(((s1_ptr)_2)->base + 7);
    if (!IS_ATOM_INT(_23pretty_ascii_min_8141))
    _23pretty_ascii_min_8141 = (long)DBL_PTR(_23pretty_ascii_min_8141)->dbl;

    /** 	pretty_ascii_max = options[MAX_ASCII]*/
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_ascii_max_8142 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_23pretty_ascii_max_8142))
    _23pretty_ascii_max_8142 = (long)DBL_PTR(_23pretty_ascii_max_8142)->dbl;

    /** 	pretty_line_max = options[MAX_LINES]*/
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_line_max_8144 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_23pretty_line_max_8144))
    _23pretty_line_max_8144 = (long)DBL_PTR(_23pretty_line_max_8144)->dbl;

    /** 	pretty_line_breaks = options[LINE_BREAKS]*/
    _2 = (int)SEQ_PTR(_options_8319);
    _23pretty_line_breaks_8146 = (int)*(((s1_ptr)_2)->base + 10);
    if (!IS_ATOM_INT(_23pretty_line_breaks_8146))
    _23pretty_line_breaks_8146 = (long)DBL_PTR(_23pretty_line_breaks_8146)->dbl;

    /** 	pretty_chars = pretty_start_col*/
    _23pretty_chars_8135 = _23pretty_start_col_8136;

    /** 	pretty_level = 0 */
    _23pretty_level_8137 = 0;

    /** 	pretty_line = ""*/
    RefDS(_5);
    DeRef(_23pretty_line_8150);
    _23pretty_line_8150 = _5;

    /** 	pretty_line_count = 0*/
    _23pretty_line_count_8143 = 0;

    /** 	pretty_dots = 0*/
    _23pretty_dots_8145 = 0;

    /** 	rPrint(x)*/
    Ref(_x_8318);
    _23rPrint(_x_8318);

    /** end procedure*/
    DeRef(_x_8318);
    DeRefDS(_options_8319);
    DeRef(_4563);
    _4563 = NOVALUE;
    return;
    ;
}


void _23pretty_print(int _fn_8341, int _x_8342, int _options_8343)
{
    int _0, _1, _2;
    

    /** 	pretty_printing = 1*/
    _23pretty_printing_8147 = 1;

    /** 	pretty_file = fn*/
    _23pretty_file_8138 = _fn_8341;

    /** 	pretty( x, options )*/
    Ref(_x_8342);
    RefDS(_options_8343);
    _23pretty(_x_8342, _options_8343);

    /** 	puts(pretty_file, pretty_line)*/
    EPuts(_23pretty_file_8138, _23pretty_line_8150); // DJP 

    /** end procedure*/
    DeRef(_x_8342);
    DeRefDSi(_options_8343);
    return;
    ;
}



// 0x88FC3433
