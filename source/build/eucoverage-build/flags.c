// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _36flags_to_string(int _flag_bits_18163, int _flag_names_18164, int _expand_flags_18165)
{
    int _s_18166 = NOVALUE;
    int _has_one_bit_18183 = NOVALUE;
    int _which_bit_inlined_which_bit_at_102_18185 = NOVALUE;
    int _current_flag_18189 = NOVALUE;
    int _which_bit_inlined_which_bit_at_247_18215 = NOVALUE;
    int _10224 = NOVALUE;
    int _10222 = NOVALUE;
    int _10221 = NOVALUE;
    int _10219 = NOVALUE;
    int _10217 = NOVALUE;
    int _10216 = NOVALUE;
    int _10212 = NOVALUE;
    int _10211 = NOVALUE;
    int _10208 = NOVALUE;
    int _10207 = NOVALUE;
    int _10203 = NOVALUE;
    int _10202 = NOVALUE;
    int _10201 = NOVALUE;
    int _10200 = NOVALUE;
    int _10199 = NOVALUE;
    int _10198 = NOVALUE;
    int _10197 = NOVALUE;
    int _10195 = NOVALUE;
    int _10194 = NOVALUE;
    int _10193 = NOVALUE;
    int _10192 = NOVALUE;
    int _0, _1, _2;
    

    /**     sequence s = {}*/
    RefDS(_5);
    DeRef(_s_18166);
    _s_18166 = _5;

    /**     if sequence(flag_bits) then*/
    _10192 = IS_SEQUENCE(_flag_bits_18163);
    if (_10192 == 0)
    {
        _10192 = NOVALUE;
        goto L1; // [19] 99
    }
    else{
        _10192 = NOVALUE;
    }

    /** 		for i = 1 to length(flag_bits) do*/
    if (IS_SEQUENCE(_flag_bits_18163)){
            _10193 = SEQ_PTR(_flag_bits_18163)->length;
    }
    else {
        _10193 = 1;
    }
    {
        int _i_18170;
        _i_18170 = 1;
L2: 
        if (_i_18170 > _10193){
            goto L3; // [27] 89
        }

        /** 			if not sequence(flag_bits[i]) then*/
        _2 = (int)SEQ_PTR(_flag_bits_18163);
        _10194 = (int)*(((s1_ptr)_2)->base + _i_18170);
        _10195 = IS_SEQUENCE(_10194);
        _10194 = NOVALUE;
        if (_10195 != 0)
        goto L4; // [43] 71
        _10195 = NOVALUE;

        /** 				flag_bits[i] = flags_to_string(flag_bits[i], flag_names, expand_flags)*/
        _2 = (int)SEQ_PTR(_flag_bits_18163);
        _10197 = (int)*(((s1_ptr)_2)->base + _i_18170);
        RefDS(_flag_names_18164);
        DeRef(_10198);
        _10198 = _flag_names_18164;
        DeRef(_10199);
        _10199 = _expand_flags_18165;
        Ref(_10197);
        _10200 = _36flags_to_string(_10197, _10198, _10199);
        _10197 = NOVALUE;
        _10198 = NOVALUE;
        _10199 = NOVALUE;
        _2 = (int)SEQ_PTR(_flag_bits_18163);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _flag_bits_18163 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18170);
        _1 = *(int *)_2;
        *(int *)_2 = _10200;
        if( _1 != _10200 ){
            DeRef(_1);
        }
        _10200 = NOVALUE;
        goto L5; // [68] 82
L4: 

        /** 				flag_bits[i] = {"?"}*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_9383);
        *((int *)(_2+4)) = _9383;
        _10201 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_flag_bits_18163);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _flag_bits_18163 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18170);
        _1 = *(int *)_2;
        *(int *)_2 = _10201;
        if( _1 != _10201 ){
            DeRef(_1);
        }
        _10201 = NOVALUE;
L5: 

        /** 		end for*/
        _i_18170 = _i_18170 + 1;
        goto L2; // [84] 34
L3: 
        ;
    }

    /** 		s = flag_bits*/
    Ref(_flag_bits_18163);
    DeRef(_s_18166);
    _s_18166 = _flag_bits_18163;
    goto L6; // [96] 301
L1: 

    /** 		integer has_one_bit*/

    /** 		has_one_bit = which_bit(flag_bits)*/

    /** 	return map:get(one_bit_numbers, theValue, 0)*/
    Ref(_36one_bit_numbers_18140);
    Ref(_flag_bits_18163);
    _has_one_bit_18183 = _27get(_36one_bit_numbers_18140, _flag_bits_18163, 0);
    if (!IS_ATOM_INT(_has_one_bit_18183)) {
        _1 = (long)(DBL_PTR(_has_one_bit_18183)->dbl);
        if (UNIQUE(DBL_PTR(_has_one_bit_18183)) && (DBL_PTR(_has_one_bit_18183)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_has_one_bit_18183);
        _has_one_bit_18183 = _1;
    }

    /** 		for i = 1 to length(flag_names) do*/
    if (IS_SEQUENCE(_flag_names_18164)){
            _10202 = SEQ_PTR(_flag_names_18164)->length;
    }
    else {
        _10202 = 1;
    }
    {
        int _i_18187;
        _i_18187 = 1;
L7: 
        if (_i_18187 > _10202){
            goto L8; // [123] 298
        }

        /** 			atom current_flag = flag_names[i][1]*/
        _2 = (int)SEQ_PTR(_flag_names_18164);
        _10203 = (int)*(((s1_ptr)_2)->base + _i_18187);
        DeRef(_current_flag_18189);
        _2 = (int)SEQ_PTR(_10203);
        _current_flag_18189 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_current_flag_18189);
        _10203 = NOVALUE;

        /** 			if flag_bits = 0 then*/
        if (binary_op_a(NOTEQ, _flag_bits_18163, 0)){
            goto L9; // [142] 176
        }

        /** 				if current_flag = 0 then*/
        if (binary_op_a(NOTEQ, _current_flag_18189, 0)){
            goto LA; // [148] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_18164);
        _10207 = (int)*(((s1_ptr)_2)->base + _i_18187);
        _2 = (int)SEQ_PTR(_10207);
        _10208 = (int)*(((s1_ptr)_2)->base + 2);
        _10207 = NOVALUE;
        Ref(_10208);
        Append(&_s_18166, _s_18166, _10208);
        _10208 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_18189);
        _current_flag_18189 = NOVALUE;
        goto L8; // [170] 298
        goto LA; // [173] 289
L9: 

        /** 			elsif has_one_bit then*/
        if (_has_one_bit_18183 == 0)
        {
            goto LB; // [178] 211
        }
        else{
        }

        /** 				if current_flag = flag_bits then*/
        if (binary_op_a(NOTEQ, _current_flag_18189, _flag_bits_18163)){
            goto LA; // [183] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_18164);
        _10211 = (int)*(((s1_ptr)_2)->base + _i_18187);
        _2 = (int)SEQ_PTR(_10211);
        _10212 = (int)*(((s1_ptr)_2)->base + 2);
        _10211 = NOVALUE;
        Ref(_10212);
        Append(&_s_18166, _s_18166, _10212);
        _10212 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_18189);
        _current_flag_18189 = NOVALUE;
        goto L8; // [205] 298
        goto LA; // [208] 289
LB: 

        /** 			elsif not expand_flags then*/
        if (_expand_flags_18165 != 0)
        goto LC; // [213] 246

        /** 				if current_flag = flag_bits then*/
        if (binary_op_a(NOTEQ, _current_flag_18189, _flag_bits_18163)){
            goto LA; // [218] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_18164);
        _10216 = (int)*(((s1_ptr)_2)->base + _i_18187);
        _2 = (int)SEQ_PTR(_10216);
        _10217 = (int)*(((s1_ptr)_2)->base + 2);
        _10216 = NOVALUE;
        Ref(_10217);
        Append(&_s_18166, _s_18166, _10217);
        _10217 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_18189);
        _current_flag_18189 = NOVALUE;
        goto L8; // [240] 298
        goto LA; // [243] 289
LC: 

        /** 				if which_bit(current_flag) then*/

        /** 	return map:get(one_bit_numbers, theValue, 0)*/
        Ref(_36one_bit_numbers_18140);
        Ref(_current_flag_18189);
        _0 = _which_bit_inlined_which_bit_at_247_18215;
        _which_bit_inlined_which_bit_at_247_18215 = _27get(_36one_bit_numbers_18140, _current_flag_18189, 0);
        DeRef(_0);
        if (_which_bit_inlined_which_bit_at_247_18215 == 0) {
            goto LD; // [259] 288
        }
        else {
            if (!IS_ATOM_INT(_which_bit_inlined_which_bit_at_247_18215) && DBL_PTR(_which_bit_inlined_which_bit_at_247_18215)->dbl == 0.0){
                goto LD; // [259] 288
            }
        }

        /** 					if and_bits( current_flag, flag_bits ) = current_flag then*/
        if (IS_ATOM_INT(_current_flag_18189) && IS_ATOM_INT(_flag_bits_18163)) {
            {unsigned long tu;
                 tu = (unsigned long)_current_flag_18189 & (unsigned long)_flag_bits_18163;
                 _10219 = MAKE_UINT(tu);
            }
        }
        else {
            _10219 = binary_op(AND_BITS, _current_flag_18189, _flag_bits_18163);
        }
        if (binary_op_a(NOTEQ, _10219, _current_flag_18189)){
            DeRef(_10219);
            _10219 = NOVALUE;
            goto LE; // [268] 287
        }
        DeRef(_10219);
        _10219 = NOVALUE;

        /** 						s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_18164);
        _10221 = (int)*(((s1_ptr)_2)->base + _i_18187);
        _2 = (int)SEQ_PTR(_10221);
        _10222 = (int)*(((s1_ptr)_2)->base + 2);
        _10221 = NOVALUE;
        Ref(_10222);
        Append(&_s_18166, _s_18166, _10222);
        _10222 = NOVALUE;
LE: 
LD: 
LA: 
        DeRef(_current_flag_18189);
        _current_flag_18189 = NOVALUE;

        /** 		end for*/
        _i_18187 = _i_18187 + 1;
        goto L7; // [293] 130
L8: 
        ;
    }
L6: 

    /**     if length(s) = 0 then*/
    if (IS_SEQUENCE(_s_18166)){
            _10224 = SEQ_PTR(_s_18166)->length;
    }
    else {
        _10224 = 1;
    }
    if (_10224 != 0)
    goto LF; // [306] 317

    /** 		s = append(s,"?")*/
    RefDS(_9383);
    Append(&_s_18166, _s_18166, _9383);
LF: 

    /**     return s*/
    DeRef(_flag_bits_18163);
    DeRefDS(_flag_names_18164);
    return _s_18166;
    ;
}



// 0x4EDEF16A
