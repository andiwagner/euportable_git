// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _24init_float80()
{
    int _data_inlined_allocate_code_at_55_19379 = NOVALUE;
    int _code_8404 = NOVALUE;
    int _4619 = NOVALUE;
    int _4618 = NOVALUE;
    int _4616 = NOVALUE;
    int _0, _1, _2;
    

    /** 	f80 = allocate( 10 )*/
    _0 = _11allocate(10, 0);
    DeRef(_24f80_8393);
    _24f80_8393 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _24f64_8394, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _11allocate(8, 0);
    DeRef(_24f64_8394);
    _24f64_8394 = _0;
L1: 

    /** 	atom code*/

    /** 	code = machine:allocate_code(*/
    _1 = NewS1(25);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 12;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 85;
    *((int *)(_2+48)) = 8;
    *((int *)(_2+52)) = 219;
    *((int *)(_2+56)) = 42;
    *((int *)(_2+60)) = 221;
    *((int *)(_2+64)) = 93;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 221;
    *((int *)(_2+76)) = 69;
    *((int *)(_2+80)) = 248;
    *((int *)(_2+84)) = 221;
    *((int *)(_2+88)) = 24;
    *((int *)(_2+92)) = 201;
    *((int *)(_2+96)) = 195;
    *((int *)(_2+100)) = 0;
    _4616 = MAKE_SEQ(_1);
    DeRefi(_data_inlined_allocate_code_at_55_19379);
    _data_inlined_allocate_code_at_55_19379 = _4616;
    _4616 = NOVALUE;

    /** 	return allocate_protect( data, wordsize, PAGE_EXECUTE )*/
    RefDS(_data_inlined_allocate_code_at_55_19379);
    _0 = _code_8404;
    _code_8404 = _11allocate_protect(_data_inlined_allocate_code_at_55_19379, 1, 16);
    DeRef(_0);
    DeRefi(_data_inlined_allocate_code_at_55_19379);
    _data_inlined_allocate_code_at_55_19379 = NOVALUE;

    /** 	F80_TO_ATOM = dll:define_c_proc( "", {'+', code}, { dll:C_POINTER, dll:C_POINTER } )*/
    Ref(_code_8404);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_8404;
    _4618 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _4619 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _10define_c_proc(_5, _4618, _4619);
    _24F80_TO_ATOM_8395 = _0;
    _4618 = NOVALUE;
    _4619 = NOVALUE;
    if (!IS_ATOM_INT(_24F80_TO_ATOM_8395)) {
        _1 = (long)(DBL_PTR(_24F80_TO_ATOM_8395)->dbl);
        if (UNIQUE(DBL_PTR(_24F80_TO_ATOM_8395)) && (DBL_PTR(_24F80_TO_ATOM_8395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_24F80_TO_ATOM_8395);
        _24F80_TO_ATOM_8395 = _1;
    }

    /** end procedure*/
    DeRef(_code_8404);
    return;
    ;
}


int _24float80_to_atom(int _f_8425)
{
    int _4625 = NOVALUE;
    int _4624 = NOVALUE;
    int _4623 = NOVALUE;
    int _4622 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not f80 then*/
    if (IS_ATOM_INT(_24f80_8393)) {
        if (_24f80_8393 != 0){
            goto L1; // [7] 15
        }
    }
    else {
        if (DBL_PTR(_24f80_8393)->dbl != 0.0){
            goto L1; // [7] 15
        }
    }

    /** 		init_float80()*/
    _24init_float80();
L1: 

    /** 	poke( f80, f )*/
    if (IS_ATOM_INT(_24f80_8393)){
        poke_addr = (unsigned char *)_24f80_8393;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24f80_8393)->dbl);
    }
    _1 = (int)SEQ_PTR(_f_8425);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( F80_TO_ATOM, { f80, f64 } )*/
    Ref(_24f64_8394);
    Ref(_24f80_8393);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24f80_8393;
    ((int *)_2)[2] = _24f64_8394;
    _4622 = MAKE_SEQ(_1);
    call_c(0, _24F80_TO_ATOM_8395, _4622);
    DeRefDS(_4622);
    _4622 = NOVALUE;

    /** 	return float64_to_atom( peek( { f64, 8 } ) )*/
    Ref(_24f64_8394);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24f64_8394;
    ((int *)_2)[2] = 8;
    _4623 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_4623);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4624 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4623);
    _4623 = NOVALUE;
    _4625 = _4float64_to_atom(_4624);
    _4624 = NOVALUE;
    DeRefDS(_f_8425);
    return _4625;
    ;
}


void _24init_peek8s()
{
    int _data_inlined_allocate_code_at_58_19381 = NOVALUE;
    int _code_8443 = NOVALUE;
    int _4633 = NOVALUE;
    int _4632 = NOVALUE;
    int _4630 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i64 = allocate( 8 )*/
    _0 = _11allocate(8, 0);
    DeRef(_24i64_8433);
    _24i64_8433 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _24f64_8394, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _11allocate(8, 0);
    DeRef(_24f64_8394);
    _24f64_8394 = _0;
L1: 

    /** 	atom code = machine:allocate_code(	{*/
    _1 = NewS1(30);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 8;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 80;
    *((int *)(_2+48)) = 4;
    *((int *)(_2+52)) = 139;
    *((int *)(_2+56)) = 0;
    *((int *)(_2+60)) = 137;
    *((int *)(_2+64)) = 69;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 137;
    *((int *)(_2+76)) = 85;
    *((int *)(_2+80)) = 252;
    *((int *)(_2+84)) = 223;
    *((int *)(_2+88)) = 109;
    *((int *)(_2+92)) = 248;
    *((int *)(_2+96)) = 139;
    *((int *)(_2+100)) = 69;
    *((int *)(_2+104)) = 12;
    *((int *)(_2+108)) = 221;
    *((int *)(_2+112)) = 24;
    *((int *)(_2+116)) = 201;
    *((int *)(_2+120)) = 195;
    _4630 = MAKE_SEQ(_1);
    DeRefi(_data_inlined_allocate_code_at_58_19381);
    _data_inlined_allocate_code_at_58_19381 = _4630;
    _4630 = NOVALUE;

    /** 	return allocate_protect( data, wordsize, PAGE_EXECUTE )*/
    RefDS(_data_inlined_allocate_code_at_58_19381);
    _0 = _code_8443;
    _code_8443 = _11allocate_protect(_data_inlined_allocate_code_at_58_19381, 1, 16);
    DeRef(_0);
    DeRefi(_data_inlined_allocate_code_at_58_19381);
    _data_inlined_allocate_code_at_58_19381 = NOVALUE;

    /** 	PEEK8S = define_c_proc( {}, {'+', code}, { C_POINTER, C_POINTER } )*/
    Ref(_code_8443);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_8443;
    _4632 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _4633 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _10define_c_proc(_5, _4632, _4633);
    _24PEEK8S_8434 = _0;
    _4632 = NOVALUE;
    _4633 = NOVALUE;
    if (!IS_ATOM_INT(_24PEEK8S_8434)) {
        _1 = (long)(DBL_PTR(_24PEEK8S_8434)->dbl);
        if (UNIQUE(DBL_PTR(_24PEEK8S_8434)) && (DBL_PTR(_24PEEK8S_8434)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_24PEEK8S_8434);
        _24PEEK8S_8434 = _1;
    }

    /** end procedure*/
    DeRef(_code_8443);
    return;
    ;
}


int _24int64_to_atom(int _s_8455)
{
    int _4639 = NOVALUE;
    int _4638 = NOVALUE;
    int _4637 = NOVALUE;
    int _4636 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if i64 = 0 then*/
    if (binary_op_a(NOTEQ, _24i64_8433, 0)){
        goto L1; // [7] 16
    }

    /** 		init_peek8s()*/
    _24init_peek8s();
L1: 

    /** 	poke( i64, s )*/
    if (IS_ATOM_INT(_24i64_8433)){
        poke_addr = (unsigned char *)_24i64_8433;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24i64_8433)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_8455);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( PEEK8S, { i64, f64 } )*/
    Ref(_24f64_8394);
    Ref(_24i64_8433);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _24i64_8433;
    ((int *)_2)[2] = _24f64_8394;
    _4636 = MAKE_SEQ(_1);
    call_c(0, _24PEEK8S_8434, _4636);
    DeRefDS(_4636);
    _4636 = NOVALUE;

    /** 	return float64_to_atom( peek( f64 & 8 ) )*/
    Concat((object_ptr)&_4637, _24f64_8394, 8);
    _1 = (int)SEQ_PTR(_4637);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4638 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4637);
    _4637 = NOVALUE;
    _4639 = _4float64_to_atom(_4638);
    _4638 = NOVALUE;
    DeRefDS(_s_8455);
    return _4639;
    ;
}


int _24get4(int _fh_8465)
{
    int _4644 = NOVALUE;
    int _4643 = NOVALUE;
    int _4642 = NOVALUE;
    int _4641 = NOVALUE;
    int _4640 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_8465 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8465, EF_READ);
        last_r_file_no = _fh_8465;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4640 = getKBchar();
        }
        else
        _4640 = getc(last_r_file_ptr);
    }
    else
    _4640 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem0_8385)){
        poke_addr = (unsigned char *)_24mem0_8385;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem0_8385)->dbl);
    }
    *poke_addr = (unsigned char)_4640;
    _4640 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_8465 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8465, EF_READ);
        last_r_file_no = _fh_8465;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4641 = getKBchar();
        }
        else
        _4641 = getc(last_r_file_ptr);
    }
    else
    _4641 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem1_8386)){
        poke_addr = (unsigned char *)_24mem1_8386;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem1_8386)->dbl);
    }
    *poke_addr = (unsigned char)_4641;
    _4641 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_8465 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8465, EF_READ);
        last_r_file_no = _fh_8465;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4642 = getKBchar();
        }
        else
        _4642 = getc(last_r_file_ptr);
    }
    else
    _4642 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem2_8387)){
        poke_addr = (unsigned char *)_24mem2_8387;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem2_8387)->dbl);
    }
    *poke_addr = (unsigned char)_4642;
    _4642 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_8465 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8465, EF_READ);
        last_r_file_no = _fh_8465;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4643 = getKBchar();
        }
        else
        _4643 = getc(last_r_file_ptr);
    }
    else
    _4643 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_24mem3_8388)){
        poke_addr = (unsigned char *)_24mem3_8388;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem3_8388)->dbl);
    }
    *poke_addr = (unsigned char)_4643;
    _4643 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_24mem0_8385)) {
        _4644 = *(unsigned long *)_24mem0_8385;
        if ((unsigned)_4644 > (unsigned)MAXINT)
        _4644 = NewDouble((double)(unsigned long)_4644);
    }
    else {
        _4644 = *(unsigned long *)(unsigned long)(DBL_PTR(_24mem0_8385)->dbl);
        if ((unsigned)_4644 > (unsigned)MAXINT)
        _4644 = NewDouble((double)(unsigned long)_4644);
    }
    return _4644;
    ;
}


int _24deserialize_file(int _fh_8473, int _c_8474)
{
    int _s_8475 = NOVALUE;
    int _len_8476 = NOVALUE;
    int _msg_inlined_crash_at_412_8568 = NOVALUE;
    int _4725 = NOVALUE;
    int _4724 = NOVALUE;
    int _4723 = NOVALUE;
    int _4722 = NOVALUE;
    int _4719 = NOVALUE;
    int _4716 = NOVALUE;
    int _4715 = NOVALUE;
    int _4714 = NOVALUE;
    int _4713 = NOVALUE;
    int _4712 = NOVALUE;
    int _4711 = NOVALUE;
    int _4710 = NOVALUE;
    int _4709 = NOVALUE;
    int _4708 = NOVALUE;
    int _4707 = NOVALUE;
    int _4706 = NOVALUE;
    int _4705 = NOVALUE;
    int _4703 = NOVALUE;
    int _4702 = NOVALUE;
    int _4701 = NOVALUE;
    int _4700 = NOVALUE;
    int _4699 = NOVALUE;
    int _4698 = NOVALUE;
    int _4697 = NOVALUE;
    int _4696 = NOVALUE;
    int _4695 = NOVALUE;
    int _4694 = NOVALUE;
    int _4692 = NOVALUE;
    int _4691 = NOVALUE;
    int _4690 = NOVALUE;
    int _4689 = NOVALUE;
    int _4688 = NOVALUE;
    int _4686 = NOVALUE;
    int _4682 = NOVALUE;
    int _4681 = NOVALUE;
    int _4680 = NOVALUE;
    int _4679 = NOVALUE;
    int _4678 = NOVALUE;
    int _4677 = NOVALUE;
    int _4676 = NOVALUE;
    int _4675 = NOVALUE;
    int _4674 = NOVALUE;
    int _4673 = NOVALUE;
    int _4672 = NOVALUE;
    int _4671 = NOVALUE;
    int _4670 = NOVALUE;
    int _4669 = NOVALUE;
    int _4668 = NOVALUE;
    int _4667 = NOVALUE;
    int _4666 = NOVALUE;
    int _4665 = NOVALUE;
    int _4664 = NOVALUE;
    int _4663 = NOVALUE;
    int _4662 = NOVALUE;
    int _4661 = NOVALUE;
    int _4659 = NOVALUE;
    int _4658 = NOVALUE;
    int _4657 = NOVALUE;
    int _4656 = NOVALUE;
    int _4655 = NOVALUE;
    int _4654 = NOVALUE;
    int _4653 = NOVALUE;
    int _4652 = NOVALUE;
    int _4651 = NOVALUE;
    int _4648 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_8474 != 0)
    goto L1; // [11] 40

    /** 		c = getc(fh)*/
    if (_fh_8473 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_8473, EF_READ);
        last_r_file_no = _fh_8473;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_8474 = getKBchar();
        }
        else
        _c_8474 = getc(last_r_file_ptr);
    }
    else
    _c_8474 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_8474 >= 249)
    goto L2; // [24] 39

    /** 			return c + MIN1B*/
    _4648 = _c_8474 + -9;
    DeRef(_s_8475);
    DeRef(_len_8476);
    return _4648;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_8474;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(fh) +*/
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4651 = getKBchar();
            }
            else
            _4651 = getc(last_r_file_ptr);
        }
        else
        _4651 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4652 = getKBchar();
            }
            else
            _4652 = getc(last_r_file_ptr);
        }
        else
        _4652 = getc(last_r_file_ptr);
        _4653 = 256 * _4652;
        _4652 = NOVALUE;
        _4654 = _4651 + _4653;
        _4651 = NOVALUE;
        _4653 = NOVALUE;
        _4655 = _4654 + _24MIN2B_8369;
        if ((long)((unsigned long)_4655 + (unsigned long)HIGH_BITS) >= 0) 
        _4655 = NewDouble((double)_4655);
        _4654 = NOVALUE;
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        return _4655;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(fh) +*/
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4656 = getKBchar();
            }
            else
            _4656 = getc(last_r_file_ptr);
        }
        else
        _4656 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4657 = getKBchar();
            }
            else
            _4657 = getc(last_r_file_ptr);
        }
        else
        _4657 = getc(last_r_file_ptr);
        _4658 = 256 * _4657;
        _4657 = NOVALUE;
        _4659 = _4656 + _4658;
        _4656 = NOVALUE;
        _4658 = NOVALUE;
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4661 = getKBchar();
            }
            else
            _4661 = getc(last_r_file_ptr);
        }
        else
        _4661 = getc(last_r_file_ptr);
        _4662 = 65536 * _4661;
        _4661 = NOVALUE;
        _4663 = _4659 + _4662;
        _4659 = NOVALUE;
        _4662 = NOVALUE;
        _4664 = _4663 + _24MIN3B_8376;
        if ((long)((unsigned long)_4664 + (unsigned long)HIGH_BITS) >= 0) 
        _4664 = NewDouble((double)_4664);
        _4663 = NOVALUE;
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        return _4664;

        /** 		case I4B then*/
        case 251:

        /** 			return get4(fh) + MIN4B*/
        _4665 = _24get4(_fh_8473);
        if (IS_ATOM_INT(_4665) && IS_ATOM_INT(_24MIN4B_8382)) {
            _4666 = _4665 + _24MIN4B_8382;
            if ((long)((unsigned long)_4666 + (unsigned long)HIGH_BITS) >= 0) 
            _4666 = NewDouble((double)_4666);
        }
        else {
            _4666 = binary_op(PLUS, _4665, _24MIN4B_8382);
        }
        DeRef(_4665);
        _4665 = NOVALUE;
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4664);
        _4664 = NOVALUE;
        return _4666;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(fh), getc(fh),*/
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4667 = getKBchar();
            }
            else
            _4667 = getc(last_r_file_ptr);
        }
        else
        _4667 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4668 = getKBchar();
            }
            else
            _4668 = getc(last_r_file_ptr);
        }
        else
        _4668 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4669 = getKBchar();
            }
            else
            _4669 = getc(last_r_file_ptr);
        }
        else
        _4669 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4670 = getKBchar();
            }
            else
            _4670 = getc(last_r_file_ptr);
        }
        else
        _4670 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4667;
        *((int *)(_2+8)) = _4668;
        *((int *)(_2+12)) = _4669;
        *((int *)(_2+16)) = _4670;
        _4671 = MAKE_SEQ(_1);
        _4670 = NOVALUE;
        _4669 = NOVALUE;
        _4668 = NOVALUE;
        _4667 = NOVALUE;
        _4672 = _4float32_to_atom(_4671);
        _4671 = NOVALUE;
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4664);
        _4664 = NOVALUE;
        DeRef(_4666);
        _4666 = NOVALUE;
        return _4672;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(fh), getc(fh),*/
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4673 = getKBchar();
            }
            else
            _4673 = getc(last_r_file_ptr);
        }
        else
        _4673 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4674 = getKBchar();
            }
            else
            _4674 = getc(last_r_file_ptr);
        }
        else
        _4674 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4675 = getKBchar();
            }
            else
            _4675 = getc(last_r_file_ptr);
        }
        else
        _4675 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4676 = getKBchar();
            }
            else
            _4676 = getc(last_r_file_ptr);
        }
        else
        _4676 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4677 = getKBchar();
            }
            else
            _4677 = getc(last_r_file_ptr);
        }
        else
        _4677 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4678 = getKBchar();
            }
            else
            _4678 = getc(last_r_file_ptr);
        }
        else
        _4678 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4679 = getKBchar();
            }
            else
            _4679 = getc(last_r_file_ptr);
        }
        else
        _4679 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4680 = getKBchar();
            }
            else
            _4680 = getc(last_r_file_ptr);
        }
        else
        _4680 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4673;
        *((int *)(_2+8)) = _4674;
        *((int *)(_2+12)) = _4675;
        *((int *)(_2+16)) = _4676;
        *((int *)(_2+20)) = _4677;
        *((int *)(_2+24)) = _4678;
        *((int *)(_2+28)) = _4679;
        *((int *)(_2+32)) = _4680;
        _4681 = MAKE_SEQ(_1);
        _4680 = NOVALUE;
        _4679 = NOVALUE;
        _4678 = NOVALUE;
        _4677 = NOVALUE;
        _4676 = NOVALUE;
        _4675 = NOVALUE;
        _4674 = NOVALUE;
        _4673 = NOVALUE;
        _4682 = _4float64_to_atom(_4681);
        _4681 = NOVALUE;
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4664);
        _4664 = NOVALUE;
        DeRef(_4666);
        _4666 = NOVALUE;
        DeRef(_4672);
        _4672 = NOVALUE;
        return _4682;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_8474 != 254)
        goto L3; // [226] 238

        /** 				len = getc(fh)*/
        DeRef(_len_8476);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_8476 = getKBchar();
            }
            else
            _len_8476 = getc(last_r_file_ptr);
        }
        else
        _len_8476 = getc(last_r_file_ptr);
        goto L4; // [235] 245
L3: 

        /** 				len = get4(fh)*/
        _0 = _len_8476;
        _len_8476 = _24get4(_fh_8473);
        DeRef(_0);
L4: 

        /** 			if len < 0  or not integer(len) then*/
        if (IS_ATOM_INT(_len_8476)) {
            _4686 = (_len_8476 < 0);
        }
        else {
            _4686 = (DBL_PTR(_len_8476)->dbl < (double)0);
        }
        if (_4686 != 0) {
            goto L5; // [253] 268
        }
        if (IS_ATOM_INT(_len_8476))
        _4688 = 1;
        else if (IS_ATOM_DBL(_len_8476))
        _4688 = IS_ATOM_INT(DoubleToInt(_len_8476));
        else
        _4688 = 0;
        _4689 = (_4688 == 0);
        _4688 = NOVALUE;
        if (_4689 == 0)
        {
            DeRef(_4689);
            _4689 = NOVALUE;
            goto L6; // [264] 275
        }
        else{
            DeRef(_4689);
            _4689 = NOVALUE;
        }
L5: 

        /** 				return 0*/
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4664);
        _4664 = NOVALUE;
        DeRef(_4686);
        _4686 = NOVALUE;
        DeRef(_4666);
        _4666 = NOVALUE;
        DeRef(_4672);
        _4672 = NOVALUE;
        DeRef(_4682);
        _4682 = NOVALUE;
        return 0;
L6: 

        /** 			if c = S4B and len < 256 then*/
        _4690 = (_c_8474 == 255);
        if (_4690 == 0) {
            goto L7; // [281] 433
        }
        if (IS_ATOM_INT(_len_8476)) {
            _4692 = (_len_8476 < 256);
        }
        else {
            _4692 = (DBL_PTR(_len_8476)->dbl < (double)256);
        }
        if (_4692 == 0)
        {
            DeRef(_4692);
            _4692 = NOVALUE;
            goto L7; // [290] 433
        }
        else{
            DeRef(_4692);
            _4692 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (binary_op_a(NOTEQ, _len_8476, 0)){
            goto L8; // [295] 347
        }

        /** 					return int64_to_atom( {*/
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4694 = getKBchar();
            }
            else
            _4694 = getc(last_r_file_ptr);
        }
        else
        _4694 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4695 = getKBchar();
            }
            else
            _4695 = getc(last_r_file_ptr);
        }
        else
        _4695 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4696 = getKBchar();
            }
            else
            _4696 = getc(last_r_file_ptr);
        }
        else
        _4696 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4697 = getKBchar();
            }
            else
            _4697 = getc(last_r_file_ptr);
        }
        else
        _4697 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4698 = getKBchar();
            }
            else
            _4698 = getc(last_r_file_ptr);
        }
        else
        _4698 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4699 = getKBchar();
            }
            else
            _4699 = getc(last_r_file_ptr);
        }
        else
        _4699 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4700 = getKBchar();
            }
            else
            _4700 = getc(last_r_file_ptr);
        }
        else
        _4700 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4701 = getKBchar();
            }
            else
            _4701 = getc(last_r_file_ptr);
        }
        else
        _4701 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4694;
        *((int *)(_2+8)) = _4695;
        *((int *)(_2+12)) = _4696;
        *((int *)(_2+16)) = _4697;
        *((int *)(_2+20)) = _4698;
        *((int *)(_2+24)) = _4699;
        *((int *)(_2+28)) = _4700;
        *((int *)(_2+32)) = _4701;
        _4702 = MAKE_SEQ(_1);
        _4701 = NOVALUE;
        _4700 = NOVALUE;
        _4699 = NOVALUE;
        _4698 = NOVALUE;
        _4697 = NOVALUE;
        _4696 = NOVALUE;
        _4695 = NOVALUE;
        _4694 = NOVALUE;
        _4703 = _24int64_to_atom(_4702);
        _4702 = NOVALUE;
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4664);
        _4664 = NOVALUE;
        DeRef(_4686);
        _4686 = NOVALUE;
        DeRef(_4666);
        _4666 = NOVALUE;
        DeRef(_4672);
        _4672 = NOVALUE;
        DeRef(_4682);
        _4682 = NOVALUE;
        DeRef(_4690);
        _4690 = NOVALUE;
        return _4703;
        goto L9; // [344] 509
L8: 

        /** 				elsif len = F10B then*/
        if (binary_op_a(NOTEQ, _len_8476, 1)){
            goto LA; // [349] 409
        }

        /** 					return float80_to_atom(*/
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4705 = getKBchar();
            }
            else
            _4705 = getc(last_r_file_ptr);
        }
        else
        _4705 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4706 = getKBchar();
            }
            else
            _4706 = getc(last_r_file_ptr);
        }
        else
        _4706 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4707 = getKBchar();
            }
            else
            _4707 = getc(last_r_file_ptr);
        }
        else
        _4707 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4708 = getKBchar();
            }
            else
            _4708 = getc(last_r_file_ptr);
        }
        else
        _4708 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4709 = getKBchar();
            }
            else
            _4709 = getc(last_r_file_ptr);
        }
        else
        _4709 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4710 = getKBchar();
            }
            else
            _4710 = getc(last_r_file_ptr);
        }
        else
        _4710 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4711 = getKBchar();
            }
            else
            _4711 = getc(last_r_file_ptr);
        }
        else
        _4711 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4712 = getKBchar();
            }
            else
            _4712 = getc(last_r_file_ptr);
        }
        else
        _4712 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4713 = getKBchar();
            }
            else
            _4713 = getc(last_r_file_ptr);
        }
        else
        _4713 = getc(last_r_file_ptr);
        if (_fh_8473 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_8473, EF_READ);
            last_r_file_no = _fh_8473;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4714 = getKBchar();
            }
            else
            _4714 = getc(last_r_file_ptr);
        }
        else
        _4714 = getc(last_r_file_ptr);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4705;
        *((int *)(_2+8)) = _4706;
        *((int *)(_2+12)) = _4707;
        *((int *)(_2+16)) = _4708;
        *((int *)(_2+20)) = _4709;
        *((int *)(_2+24)) = _4710;
        *((int *)(_2+28)) = _4711;
        *((int *)(_2+32)) = _4712;
        *((int *)(_2+36)) = _4713;
        *((int *)(_2+40)) = _4714;
        _4715 = MAKE_SEQ(_1);
        _4714 = NOVALUE;
        _4713 = NOVALUE;
        _4712 = NOVALUE;
        _4711 = NOVALUE;
        _4710 = NOVALUE;
        _4709 = NOVALUE;
        _4708 = NOVALUE;
        _4707 = NOVALUE;
        _4706 = NOVALUE;
        _4705 = NOVALUE;
        _4716 = _24float80_to_atom(_4715);
        _4715 = NOVALUE;
        DeRef(_s_8475);
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4664);
        _4664 = NOVALUE;
        DeRef(_4686);
        _4686 = NOVALUE;
        DeRef(_4666);
        _4666 = NOVALUE;
        DeRef(_4672);
        _4672 = NOVALUE;
        DeRef(_4682);
        _4682 = NOVALUE;
        DeRef(_4690);
        _4690 = NOVALUE;
        DeRef(_4703);
        _4703 = NOVALUE;
        return _4716;
        goto L9; // [406] 509
LA: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_412_8568);
        _msg_inlined_crash_at_412_8568 = EPrintf(-9999999, _4717, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_412_8568);

        /** end procedure*/
        goto LB; // [424] 427
LB: 
        DeRefi(_msg_inlined_crash_at_412_8568);
        _msg_inlined_crash_at_412_8568 = NOVALUE;
        goto L9; // [430] 509
L7: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_8475);
        _s_8475 = Repeat(0, _len_8476);

        /** 				for i = 1 to len do*/
        Ref(_len_8476);
        DeRef(_4719);
        _4719 = _len_8476;
        {
            int _i_8572;
            _i_8572 = 1;
LC: 
            if (binary_op_a(GREATER, _i_8572, _4719)){
                goto LD; // [444] 502
            }

            /** 					c = getc(fh)*/
            if (_fh_8473 != last_r_file_no) {
                last_r_file_ptr = which_file(_fh_8473, EF_READ);
                last_r_file_no = _fh_8473;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_8474 = getKBchar();
                }
                else
                _c_8474 = getc(last_r_file_ptr);
            }
            else
            _c_8474 = getc(last_r_file_ptr);

            /** 					if c < I2B then*/
            if (_c_8474 >= 249)
            goto LE; // [460] 477

            /** 						s[i] = c + MIN1B*/
            _4722 = _c_8474 + -9;
            _2 = (int)SEQ_PTR(_s_8475);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8475 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_8572))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_8572)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_8572);
            _1 = *(int *)_2;
            *(int *)_2 = _4722;
            if( _1 != _4722 ){
                DeRef(_1);
            }
            _4722 = NOVALUE;
            goto LF; // [474] 495
LE: 

            /** 						s[i] = deserialize_file(fh, c)*/
            DeRef(_4723);
            _4723 = _fh_8473;
            DeRef(_4724);
            _4724 = _c_8474;
            _4725 = _24deserialize_file(_4723, _4724);
            _4723 = NOVALUE;
            _4724 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_8475);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8475 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_8572))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_8572)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_8572);
            _1 = *(int *)_2;
            *(int *)_2 = _4725;
            if( _1 != _4725 ){
                DeRef(_1);
            }
            _4725 = NOVALUE;
LF: 

            /** 				end for*/
            _0 = _i_8572;
            if (IS_ATOM_INT(_i_8572)) {
                _i_8572 = _i_8572 + 1;
                if ((long)((unsigned long)_i_8572 +(unsigned long) HIGH_BITS) >= 0){
                    _i_8572 = NewDouble((double)_i_8572);
                }
            }
            else {
                _i_8572 = binary_op_a(PLUS, _i_8572, 1);
            }
            DeRef(_0);
            goto LC; // [497] 451
LD: 
            ;
            DeRef(_i_8572);
        }

        /** 				return s*/
        DeRef(_len_8476);
        DeRef(_4648);
        _4648 = NOVALUE;
        DeRef(_4655);
        _4655 = NOVALUE;
        DeRef(_4664);
        _4664 = NOVALUE;
        DeRef(_4686);
        _4686 = NOVALUE;
        DeRef(_4666);
        _4666 = NOVALUE;
        DeRef(_4672);
        _4672 = NOVALUE;
        DeRef(_4682);
        _4682 = NOVALUE;
        DeRef(_4690);
        _4690 = NOVALUE;
        DeRef(_4703);
        _4703 = NOVALUE;
        DeRef(_4716);
        _4716 = NOVALUE;
        return _s_8475;
L9: 
    ;}    ;
}


int _24getp4(int _sdata_8584, int _pos_8585)
{
    int _4734 = NOVALUE;
    int _4733 = NOVALUE;
    int _4732 = NOVALUE;
    int _4731 = NOVALUE;
    int _4730 = NOVALUE;
    int _4729 = NOVALUE;
    int _4728 = NOVALUE;
    int _4727 = NOVALUE;
    int _4726 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, sdata[pos+0])*/
    _4726 = _pos_8585 + 0;
    _2 = (int)SEQ_PTR(_sdata_8584);
    _4727 = (int)*(((s1_ptr)_2)->base + _4726);
    if (IS_ATOM_INT(_24mem0_8385)){
        poke_addr = (unsigned char *)_24mem0_8385;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem0_8385)->dbl);
    }
    if (IS_ATOM_INT(_4727)) {
        *poke_addr = (unsigned char)_4727;
    }
    else if (IS_ATOM(_4727)) {
        _1 = (signed char)DBL_PTR(_4727)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4727);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4727 = NOVALUE;

    /** 	poke(mem1, sdata[pos+1])*/
    _4728 = _pos_8585 + 1;
    _2 = (int)SEQ_PTR(_sdata_8584);
    _4729 = (int)*(((s1_ptr)_2)->base + _4728);
    if (IS_ATOM_INT(_24mem1_8386)){
        poke_addr = (unsigned char *)_24mem1_8386;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem1_8386)->dbl);
    }
    if (IS_ATOM_INT(_4729)) {
        *poke_addr = (unsigned char)_4729;
    }
    else if (IS_ATOM(_4729)) {
        _1 = (signed char)DBL_PTR(_4729)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4729);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4729 = NOVALUE;

    /** 	poke(mem2, sdata[pos+2])*/
    _4730 = _pos_8585 + 2;
    _2 = (int)SEQ_PTR(_sdata_8584);
    _4731 = (int)*(((s1_ptr)_2)->base + _4730);
    if (IS_ATOM_INT(_24mem2_8387)){
        poke_addr = (unsigned char *)_24mem2_8387;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem2_8387)->dbl);
    }
    if (IS_ATOM_INT(_4731)) {
        *poke_addr = (unsigned char)_4731;
    }
    else if (IS_ATOM(_4731)) {
        _1 = (signed char)DBL_PTR(_4731)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4731);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4731 = NOVALUE;

    /** 	poke(mem3, sdata[pos+3])*/
    _4732 = _pos_8585 + 3;
    _2 = (int)SEQ_PTR(_sdata_8584);
    _4733 = (int)*(((s1_ptr)_2)->base + _4732);
    if (IS_ATOM_INT(_24mem3_8388)){
        poke_addr = (unsigned char *)_24mem3_8388;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_24mem3_8388)->dbl);
    }
    if (IS_ATOM_INT(_4733)) {
        *poke_addr = (unsigned char)_4733;
    }
    else if (IS_ATOM(_4733)) {
        _1 = (signed char)DBL_PTR(_4733)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4733);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4733 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_24mem0_8385)) {
        _4734 = *(unsigned long *)_24mem0_8385;
        if ((unsigned)_4734 > (unsigned)MAXINT)
        _4734 = NewDouble((double)(unsigned long)_4734);
    }
    else {
        _4734 = *(unsigned long *)(unsigned long)(DBL_PTR(_24mem0_8385)->dbl);
        if ((unsigned)_4734 > (unsigned)MAXINT)
        _4734 = NewDouble((double)(unsigned long)_4734);
    }
    DeRefDS(_sdata_8584);
    _4726 = NOVALUE;
    _4728 = NOVALUE;
    _4730 = NOVALUE;
    _4732 = NOVALUE;
    return _4734;
    ;
}


int _24deserialize_object(int _sdata_8597, int _pos_8598, int _c_8599)
{
    int _s_8600 = NOVALUE;
    int _len_8601 = NOVALUE;
    int _msg_inlined_crash_at_491_8701 = NOVALUE;
    int _temp_8713 = NOVALUE;
    int _4829 = NOVALUE;
    int _4827 = NOVALUE;
    int _4825 = NOVALUE;
    int _4824 = NOVALUE;
    int _4823 = NOVALUE;
    int _4822 = NOVALUE;
    int _4818 = NOVALUE;
    int _4816 = NOVALUE;
    int _4815 = NOVALUE;
    int _4814 = NOVALUE;
    int _4813 = NOVALUE;
    int _4812 = NOVALUE;
    int _4810 = NOVALUE;
    int _4809 = NOVALUE;
    int _4808 = NOVALUE;
    int _4807 = NOVALUE;
    int _4806 = NOVALUE;
    int _4804 = NOVALUE;
    int _4803 = NOVALUE;
    int _4802 = NOVALUE;
    int _4796 = NOVALUE;
    int _4795 = NOVALUE;
    int _4794 = NOVALUE;
    int _4793 = NOVALUE;
    int _4792 = NOVALUE;
    int _4791 = NOVALUE;
    int _4790 = NOVALUE;
    int _4789 = NOVALUE;
    int _4788 = NOVALUE;
    int _4787 = NOVALUE;
    int _4786 = NOVALUE;
    int _4785 = NOVALUE;
    int _4784 = NOVALUE;
    int _4783 = NOVALUE;
    int _4782 = NOVALUE;
    int _4781 = NOVALUE;
    int _4780 = NOVALUE;
    int _4779 = NOVALUE;
    int _4778 = NOVALUE;
    int _4777 = NOVALUE;
    int _4776 = NOVALUE;
    int _4775 = NOVALUE;
    int _4774 = NOVALUE;
    int _4773 = NOVALUE;
    int _4772 = NOVALUE;
    int _4771 = NOVALUE;
    int _4770 = NOVALUE;
    int _4769 = NOVALUE;
    int _4768 = NOVALUE;
    int _4767 = NOVALUE;
    int _4766 = NOVALUE;
    int _4765 = NOVALUE;
    int _4764 = NOVALUE;
    int _4763 = NOVALUE;
    int _4762 = NOVALUE;
    int _4761 = NOVALUE;
    int _4760 = NOVALUE;
    int _4759 = NOVALUE;
    int _4758 = NOVALUE;
    int _4757 = NOVALUE;
    int _4756 = NOVALUE;
    int _4755 = NOVALUE;
    int _4754 = NOVALUE;
    int _4753 = NOVALUE;
    int _4752 = NOVALUE;
    int _4751 = NOVALUE;
    int _4750 = NOVALUE;
    int _4749 = NOVALUE;
    int _4748 = NOVALUE;
    int _4747 = NOVALUE;
    int _4746 = NOVALUE;
    int _4745 = NOVALUE;
    int _4744 = NOVALUE;
    int _4743 = NOVALUE;
    int _4740 = NOVALUE;
    int _4739 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_8599 != 0)
    goto L1; // [13] 55

    /** 		c = sdata[pos]*/
    _2 = (int)SEQ_PTR(_sdata_8597);
    _c_8599 = (int)*(((s1_ptr)_2)->base + _pos_8598);
    if (!IS_ATOM_INT(_c_8599))
    _c_8599 = (long)DBL_PTR(_c_8599)->dbl;

    /** 		pos += 1*/
    _pos_8598 = _pos_8598 + 1;

    /** 		if c < I2B then*/
    if (_c_8599 >= 249)
    goto L2; // [35] 54

    /** 			return {c + MIN1B, pos}*/
    _4739 = _c_8599 + -9;
    if ((long)((unsigned long)_4739 + (unsigned long)HIGH_BITS) >= 0) 
    _4739 = NewDouble((double)_4739);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4739;
    ((int *)_2)[2] = _pos_8598;
    _4740 = MAKE_SEQ(_1);
    _4739 = NOVALUE;
    DeRefDS(_sdata_8597);
    DeRef(_s_8600);
    return _4740;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_8599;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4743 = (int)*(((s1_ptr)_2)->base + _pos_8598);
        _4744 = _pos_8598 + 1;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4745 = (int)*(((s1_ptr)_2)->base + _4744);
        if (IS_ATOM_INT(_4745)) {
            if (_4745 <= INT15 && _4745 >= -INT15)
            _4746 = 256 * _4745;
            else
            _4746 = NewDouble(256 * (double)_4745);
        }
        else {
            _4746 = binary_op(MULTIPLY, 256, _4745);
        }
        _4745 = NOVALUE;
        if (IS_ATOM_INT(_4743) && IS_ATOM_INT(_4746)) {
            _4747 = _4743 + _4746;
            if ((long)((unsigned long)_4747 + (unsigned long)HIGH_BITS) >= 0) 
            _4747 = NewDouble((double)_4747);
        }
        else {
            _4747 = binary_op(PLUS, _4743, _4746);
        }
        _4743 = NOVALUE;
        DeRef(_4746);
        _4746 = NOVALUE;
        if (IS_ATOM_INT(_4747)) {
            _4748 = _4747 + _24MIN2B_8369;
            if ((long)((unsigned long)_4748 + (unsigned long)HIGH_BITS) >= 0) 
            _4748 = NewDouble((double)_4748);
        }
        else {
            _4748 = binary_op(PLUS, _4747, _24MIN2B_8369);
        }
        DeRef(_4747);
        _4747 = NOVALUE;
        _4749 = _pos_8598 + 2;
        if ((long)((unsigned long)_4749 + (unsigned long)HIGH_BITS) >= 0) 
        _4749 = NewDouble((double)_4749);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4748;
        ((int *)_2)[2] = _4749;
        _4750 = MAKE_SEQ(_1);
        _4749 = NOVALUE;
        _4748 = NOVALUE;
        DeRefDS(_sdata_8597);
        DeRef(_s_8600);
        DeRef(_4740);
        _4740 = NOVALUE;
        _4744 = NOVALUE;
        return _4750;

        /** 		case I3B then*/
        case 250:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4751 = (int)*(((s1_ptr)_2)->base + _pos_8598);
        _4752 = _pos_8598 + 1;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4753 = (int)*(((s1_ptr)_2)->base + _4752);
        if (IS_ATOM_INT(_4753)) {
            if (_4753 <= INT15 && _4753 >= -INT15)
            _4754 = 256 * _4753;
            else
            _4754 = NewDouble(256 * (double)_4753);
        }
        else {
            _4754 = binary_op(MULTIPLY, 256, _4753);
        }
        _4753 = NOVALUE;
        if (IS_ATOM_INT(_4751) && IS_ATOM_INT(_4754)) {
            _4755 = _4751 + _4754;
            if ((long)((unsigned long)_4755 + (unsigned long)HIGH_BITS) >= 0) 
            _4755 = NewDouble((double)_4755);
        }
        else {
            _4755 = binary_op(PLUS, _4751, _4754);
        }
        _4751 = NOVALUE;
        DeRef(_4754);
        _4754 = NOVALUE;
        _4756 = _pos_8598 + 2;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4757 = (int)*(((s1_ptr)_2)->base + _4756);
        if (IS_ATOM_INT(_4757)) {
            _4758 = NewDouble(65536 * (double)_4757);
        }
        else {
            _4758 = binary_op(MULTIPLY, 65536, _4757);
        }
        _4757 = NOVALUE;
        if (IS_ATOM_INT(_4755) && IS_ATOM_INT(_4758)) {
            _4759 = _4755 + _4758;
            if ((long)((unsigned long)_4759 + (unsigned long)HIGH_BITS) >= 0) 
            _4759 = NewDouble((double)_4759);
        }
        else {
            _4759 = binary_op(PLUS, _4755, _4758);
        }
        DeRef(_4755);
        _4755 = NOVALUE;
        DeRef(_4758);
        _4758 = NOVALUE;
        if (IS_ATOM_INT(_4759)) {
            _4760 = _4759 + _24MIN3B_8376;
            if ((long)((unsigned long)_4760 + (unsigned long)HIGH_BITS) >= 0) 
            _4760 = NewDouble((double)_4760);
        }
        else {
            _4760 = binary_op(PLUS, _4759, _24MIN3B_8376);
        }
        DeRef(_4759);
        _4759 = NOVALUE;
        _4761 = _pos_8598 + 3;
        if ((long)((unsigned long)_4761 + (unsigned long)HIGH_BITS) >= 0) 
        _4761 = NewDouble((double)_4761);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4760;
        ((int *)_2)[2] = _4761;
        _4762 = MAKE_SEQ(_1);
        _4761 = NOVALUE;
        _4760 = NOVALUE;
        DeRefDS(_sdata_8597);
        DeRef(_s_8600);
        DeRef(_4740);
        _4740 = NOVALUE;
        DeRef(_4750);
        _4750 = NOVALUE;
        DeRef(_4744);
        _4744 = NOVALUE;
        _4756 = NOVALUE;
        _4752 = NOVALUE;
        return _4762;

        /** 		case I4B then*/
        case 251:

        /** 			return {getp4(sdata, pos) + MIN4B, pos + 4}*/
        RefDS(_sdata_8597);
        _4763 = _24getp4(_sdata_8597, _pos_8598);
        if (IS_ATOM_INT(_4763) && IS_ATOM_INT(_24MIN4B_8382)) {
            _4764 = _4763 + _24MIN4B_8382;
            if ((long)((unsigned long)_4764 + (unsigned long)HIGH_BITS) >= 0) 
            _4764 = NewDouble((double)_4764);
        }
        else {
            _4764 = binary_op(PLUS, _4763, _24MIN4B_8382);
        }
        DeRef(_4763);
        _4763 = NOVALUE;
        _4765 = _pos_8598 + 4;
        if ((long)((unsigned long)_4765 + (unsigned long)HIGH_BITS) >= 0) 
        _4765 = NewDouble((double)_4765);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4764;
        ((int *)_2)[2] = _4765;
        _4766 = MAKE_SEQ(_1);
        _4765 = NOVALUE;
        _4764 = NOVALUE;
        DeRefDS(_sdata_8597);
        DeRef(_s_8600);
        DeRef(_4740);
        _4740 = NOVALUE;
        DeRef(_4750);
        _4750 = NOVALUE;
        DeRef(_4744);
        _4744 = NOVALUE;
        DeRef(_4756);
        _4756 = NOVALUE;
        DeRef(_4752);
        _4752 = NOVALUE;
        DeRef(_4762);
        _4762 = NOVALUE;
        return _4766;

        /** 		case F4B then*/
        case 252:

        /** 			return {convert:float32_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4767 = (int)*(((s1_ptr)_2)->base + _pos_8598);
        _4768 = _pos_8598 + 1;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4769 = (int)*(((s1_ptr)_2)->base + _4768);
        _4770 = _pos_8598 + 2;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4771 = (int)*(((s1_ptr)_2)->base + _4770);
        _4772 = _pos_8598 + 3;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4773 = (int)*(((s1_ptr)_2)->base + _4772);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_4767);
        *((int *)(_2+4)) = _4767;
        Ref(_4769);
        *((int *)(_2+8)) = _4769;
        Ref(_4771);
        *((int *)(_2+12)) = _4771;
        Ref(_4773);
        *((int *)(_2+16)) = _4773;
        _4774 = MAKE_SEQ(_1);
        _4773 = NOVALUE;
        _4771 = NOVALUE;
        _4769 = NOVALUE;
        _4767 = NOVALUE;
        _4775 = _4float32_to_atom(_4774);
        _4774 = NOVALUE;
        _4776 = _pos_8598 + 4;
        if ((long)((unsigned long)_4776 + (unsigned long)HIGH_BITS) >= 0) 
        _4776 = NewDouble((double)_4776);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4775;
        ((int *)_2)[2] = _4776;
        _4777 = MAKE_SEQ(_1);
        _4776 = NOVALUE;
        _4775 = NOVALUE;
        DeRefDS(_sdata_8597);
        DeRef(_s_8600);
        DeRef(_4740);
        _4740 = NOVALUE;
        DeRef(_4750);
        _4750 = NOVALUE;
        DeRef(_4744);
        _4744 = NOVALUE;
        DeRef(_4756);
        _4756 = NOVALUE;
        DeRef(_4752);
        _4752 = NOVALUE;
        DeRef(_4762);
        _4762 = NOVALUE;
        DeRef(_4766);
        _4766 = NOVALUE;
        _4768 = NOVALUE;
        _4770 = NOVALUE;
        _4772 = NOVALUE;
        return _4777;

        /** 		case F8B then*/
        case 253:

        /** 			return {convert:float64_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4778 = (int)*(((s1_ptr)_2)->base + _pos_8598);
        _4779 = _pos_8598 + 1;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4780 = (int)*(((s1_ptr)_2)->base + _4779);
        _4781 = _pos_8598 + 2;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4782 = (int)*(((s1_ptr)_2)->base + _4781);
        _4783 = _pos_8598 + 3;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4784 = (int)*(((s1_ptr)_2)->base + _4783);
        _4785 = _pos_8598 + 4;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4786 = (int)*(((s1_ptr)_2)->base + _4785);
        _4787 = _pos_8598 + 5;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4788 = (int)*(((s1_ptr)_2)->base + _4787);
        _4789 = _pos_8598 + 6;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4790 = (int)*(((s1_ptr)_2)->base + _4789);
        _4791 = _pos_8598 + 7;
        _2 = (int)SEQ_PTR(_sdata_8597);
        _4792 = (int)*(((s1_ptr)_2)->base + _4791);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_4778);
        *((int *)(_2+4)) = _4778;
        Ref(_4780);
        *((int *)(_2+8)) = _4780;
        Ref(_4782);
        *((int *)(_2+12)) = _4782;
        Ref(_4784);
        *((int *)(_2+16)) = _4784;
        Ref(_4786);
        *((int *)(_2+20)) = _4786;
        Ref(_4788);
        *((int *)(_2+24)) = _4788;
        Ref(_4790);
        *((int *)(_2+28)) = _4790;
        Ref(_4792);
        *((int *)(_2+32)) = _4792;
        _4793 = MAKE_SEQ(_1);
        _4792 = NOVALUE;
        _4790 = NOVALUE;
        _4788 = NOVALUE;
        _4786 = NOVALUE;
        _4784 = NOVALUE;
        _4782 = NOVALUE;
        _4780 = NOVALUE;
        _4778 = NOVALUE;
        _4794 = _4float64_to_atom(_4793);
        _4793 = NOVALUE;
        _4795 = _pos_8598 + 8;
        if ((long)((unsigned long)_4795 + (unsigned long)HIGH_BITS) >= 0) 
        _4795 = NewDouble((double)_4795);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4794;
        ((int *)_2)[2] = _4795;
        _4796 = MAKE_SEQ(_1);
        _4795 = NOVALUE;
        _4794 = NOVALUE;
        DeRefDS(_sdata_8597);
        DeRef(_s_8600);
        DeRef(_4770);
        _4770 = NOVALUE;
        DeRef(_4752);
        _4752 = NOVALUE;
        DeRef(_4777);
        _4777 = NOVALUE;
        _4789 = NOVALUE;
        _4779 = NOVALUE;
        DeRef(_4744);
        _4744 = NOVALUE;
        DeRef(_4740);
        _4740 = NOVALUE;
        DeRef(_4750);
        _4750 = NOVALUE;
        DeRef(_4772);
        _4772 = NOVALUE;
        _4785 = NOVALUE;
        DeRef(_4762);
        _4762 = NOVALUE;
        _4781 = NOVALUE;
        _4791 = NOVALUE;
        _4787 = NOVALUE;
        DeRef(_4766);
        _4766 = NOVALUE;
        DeRef(_4768);
        _4768 = NOVALUE;
        DeRef(_4756);
        _4756 = NOVALUE;
        _4783 = NOVALUE;
        return _4796;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_8599 != 254)
        goto L3; // [351] 374

        /** 				len = sdata[pos]*/
        _2 = (int)SEQ_PTR(_sdata_8597);
        _len_8601 = (int)*(((s1_ptr)_2)->base + _pos_8598);
        if (!IS_ATOM_INT(_len_8601))
        _len_8601 = (long)DBL_PTR(_len_8601)->dbl;

        /** 				pos += 1*/
        _pos_8598 = _pos_8598 + 1;
        goto L4; // [371] 394
L3: 

        /** 				len = getp4(sdata, pos)*/
        RefDS(_sdata_8597);
        _len_8601 = _24getp4(_sdata_8597, _pos_8598);
        if (!IS_ATOM_INT(_len_8601)) {
            _1 = (long)(DBL_PTR(_len_8601)->dbl);
            if (UNIQUE(DBL_PTR(_len_8601)) && (DBL_PTR(_len_8601)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_8601);
            _len_8601 = _1;
        }

        /** 				pos += 4*/
        _pos_8598 = _pos_8598 + 4;
L4: 

        /** 			if c = S4B and len < 256 then*/
        _4802 = (_c_8599 == 255);
        if (_4802 == 0) {
            goto L5; // [400] 512
        }
        _4804 = (_len_8601 < 256);
        if (_4804 == 0)
        {
            DeRef(_4804);
            _4804 = NOVALUE;
            goto L5; // [411] 512
        }
        else{
            DeRef(_4804);
            _4804 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (_len_8601 != 0)
        goto L6; // [418] 452

        /** 					return {int64_to_atom( sdata[pos..pos+7] ), pos + 8 }*/
        _4806 = _pos_8598 + 7;
        rhs_slice_target = (object_ptr)&_4807;
        RHS_Slice(_sdata_8597, _pos_8598, _4806);
        _4808 = _24int64_to_atom(_4807);
        _4807 = NOVALUE;
        _4809 = _pos_8598 + 8;
        if ((long)((unsigned long)_4809 + (unsigned long)HIGH_BITS) >= 0) 
        _4809 = NewDouble((double)_4809);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4808;
        ((int *)_2)[2] = _4809;
        _4810 = MAKE_SEQ(_1);
        _4809 = NOVALUE;
        _4808 = NOVALUE;
        DeRefDS(_sdata_8597);
        DeRef(_s_8600);
        DeRef(_4770);
        _4770 = NOVALUE;
        DeRef(_4802);
        _4802 = NOVALUE;
        DeRef(_4752);
        _4752 = NOVALUE;
        DeRef(_4777);
        _4777 = NOVALUE;
        DeRef(_4789);
        _4789 = NOVALUE;
        DeRef(_4779);
        _4779 = NOVALUE;
        _4806 = NOVALUE;
        DeRef(_4744);
        _4744 = NOVALUE;
        DeRef(_4740);
        _4740 = NOVALUE;
        DeRef(_4750);
        _4750 = NOVALUE;
        DeRef(_4772);
        _4772 = NOVALUE;
        DeRef(_4785);
        _4785 = NOVALUE;
        DeRef(_4762);
        _4762 = NOVALUE;
        DeRef(_4781);
        _4781 = NOVALUE;
        DeRef(_4791);
        _4791 = NOVALUE;
        DeRef(_4796);
        _4796 = NOVALUE;
        DeRef(_4787);
        _4787 = NOVALUE;
        DeRef(_4766);
        _4766 = NOVALUE;
        DeRef(_4768);
        _4768 = NOVALUE;
        DeRef(_4756);
        _4756 = NOVALUE;
        DeRef(_4783);
        _4783 = NOVALUE;
        return _4810;
        goto L7; // [449] 625
L6: 

        /** 				elsif len = F10B then*/
        if (_len_8601 != 1)
        goto L8; // [454] 488

        /** 					return { float80_to_atom( sdata[pos..pos+9] ), pos + 10 }*/
        _4812 = _pos_8598 + 9;
        rhs_slice_target = (object_ptr)&_4813;
        RHS_Slice(_sdata_8597, _pos_8598, _4812);
        _4814 = _24float80_to_atom(_4813);
        _4813 = NOVALUE;
        _4815 = _pos_8598 + 10;
        if ((long)((unsigned long)_4815 + (unsigned long)HIGH_BITS) >= 0) 
        _4815 = NewDouble((double)_4815);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4814;
        ((int *)_2)[2] = _4815;
        _4816 = MAKE_SEQ(_1);
        _4815 = NOVALUE;
        _4814 = NOVALUE;
        DeRefDS(_sdata_8597);
        DeRef(_s_8600);
        DeRef(_4770);
        _4770 = NOVALUE;
        DeRef(_4802);
        _4802 = NOVALUE;
        DeRef(_4752);
        _4752 = NOVALUE;
        DeRef(_4777);
        _4777 = NOVALUE;
        DeRef(_4789);
        _4789 = NOVALUE;
        DeRef(_4779);
        _4779 = NOVALUE;
        DeRef(_4806);
        _4806 = NOVALUE;
        DeRef(_4744);
        _4744 = NOVALUE;
        DeRef(_4740);
        _4740 = NOVALUE;
        DeRef(_4810);
        _4810 = NOVALUE;
        DeRef(_4750);
        _4750 = NOVALUE;
        DeRef(_4772);
        _4772 = NOVALUE;
        DeRef(_4785);
        _4785 = NOVALUE;
        DeRef(_4762);
        _4762 = NOVALUE;
        DeRef(_4781);
        _4781 = NOVALUE;
        DeRef(_4791);
        _4791 = NOVALUE;
        DeRef(_4796);
        _4796 = NOVALUE;
        DeRef(_4787);
        _4787 = NOVALUE;
        DeRef(_4766);
        _4766 = NOVALUE;
        DeRef(_4768);
        _4768 = NOVALUE;
        _4812 = NOVALUE;
        DeRef(_4756);
        _4756 = NOVALUE;
        DeRef(_4783);
        _4783 = NOVALUE;
        return _4816;
        goto L7; // [485] 625
L8: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_491_8701);
        _msg_inlined_crash_at_491_8701 = EPrintf(-9999999, _4717, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_491_8701);

        /** end procedure*/
        goto L9; // [503] 506
L9: 
        DeRefi(_msg_inlined_crash_at_491_8701);
        _msg_inlined_crash_at_491_8701 = NOVALUE;
        goto L7; // [509] 625
L5: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_8600);
        _s_8600 = Repeat(0, _len_8601);

        /** 				for i = 1 to len do*/
        _4818 = _len_8601;
        {
            int _i_8705;
            _i_8705 = 1;
LA: 
            if (_i_8705 > _4818){
                goto LB; // [525] 614
            }

            /** 					c = sdata[pos]*/
            _2 = (int)SEQ_PTR(_sdata_8597);
            _c_8599 = (int)*(((s1_ptr)_2)->base + _pos_8598);
            if (!IS_ATOM_INT(_c_8599))
            _c_8599 = (long)DBL_PTR(_c_8599)->dbl;

            /** 					pos += 1*/
            _pos_8598 = _pos_8598 + 1;

            /** 					if c < I2B then*/
            if (_c_8599 >= 249)
            goto LC; // [550] 567

            /** 						s[i] = c + MIN1B*/
            _4822 = _c_8599 + -9;
            if ((long)((unsigned long)_4822 + (unsigned long)HIGH_BITS) >= 0) 
            _4822 = NewDouble((double)_4822);
            _2 = (int)SEQ_PTR(_s_8600);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8600 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_8705);
            _1 = *(int *)_2;
            *(int *)_2 = _4822;
            if( _1 != _4822 ){
                DeRef(_1);
            }
            _4822 = NOVALUE;
            goto LD; // [564] 607
LC: 

            /** 						sequence temp = deserialize_object(sdata, pos, c)*/
            RefDS(_sdata_8597);
            DeRef(_4823);
            _4823 = _sdata_8597;
            DeRef(_4824);
            _4824 = _pos_8598;
            DeRef(_4825);
            _4825 = _c_8599;
            _0 = _temp_8713;
            _temp_8713 = _24deserialize_object(_4823, _4824, _4825);
            DeRef(_0);
            _4823 = NOVALUE;
            _4824 = NOVALUE;
            _4825 = NOVALUE;

            /** 						s[i] = temp[1]*/
            _2 = (int)SEQ_PTR(_temp_8713);
            _4827 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_4827);
            _2 = (int)SEQ_PTR(_s_8600);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8600 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_8705);
            _1 = *(int *)_2;
            *(int *)_2 = _4827;
            if( _1 != _4827 ){
                DeRef(_1);
            }
            _4827 = NOVALUE;

            /** 						pos = temp[2]*/
            _2 = (int)SEQ_PTR(_temp_8713);
            _pos_8598 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_pos_8598))
            _pos_8598 = (long)DBL_PTR(_pos_8598)->dbl;
            DeRefDS(_temp_8713);
            _temp_8713 = NOVALUE;
LD: 

            /** 				end for*/
            _i_8705 = _i_8705 + 1;
            goto LA; // [609] 532
LB: 
            ;
        }

        /** 				return {s, pos}*/
        RefDS(_s_8600);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _s_8600;
        ((int *)_2)[2] = _pos_8598;
        _4829 = MAKE_SEQ(_1);
        DeRefDS(_sdata_8597);
        DeRefDS(_s_8600);
        DeRef(_4770);
        _4770 = NOVALUE;
        DeRef(_4802);
        _4802 = NOVALUE;
        DeRef(_4752);
        _4752 = NOVALUE;
        DeRef(_4777);
        _4777 = NOVALUE;
        DeRef(_4789);
        _4789 = NOVALUE;
        DeRef(_4816);
        _4816 = NOVALUE;
        DeRef(_4779);
        _4779 = NOVALUE;
        DeRef(_4806);
        _4806 = NOVALUE;
        DeRef(_4744);
        _4744 = NOVALUE;
        DeRef(_4740);
        _4740 = NOVALUE;
        DeRef(_4810);
        _4810 = NOVALUE;
        DeRef(_4750);
        _4750 = NOVALUE;
        DeRef(_4772);
        _4772 = NOVALUE;
        DeRef(_4785);
        _4785 = NOVALUE;
        DeRef(_4762);
        _4762 = NOVALUE;
        DeRef(_4781);
        _4781 = NOVALUE;
        DeRef(_4791);
        _4791 = NOVALUE;
        DeRef(_4796);
        _4796 = NOVALUE;
        DeRef(_4787);
        _4787 = NOVALUE;
        DeRef(_4766);
        _4766 = NOVALUE;
        DeRef(_4768);
        _4768 = NOVALUE;
        DeRef(_4812);
        _4812 = NOVALUE;
        DeRef(_4756);
        _4756 = NOVALUE;
        DeRef(_4783);
        _4783 = NOVALUE;
        return _4829;
L7: 
    ;}    ;
}


int _24deserialize(int _sdata_8723, int _pos_8724)
{
    int _4833 = NOVALUE;
    int _4832 = NOVALUE;
    int _4831 = NOVALUE;
    int _4830 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(sdata) then*/
    _4830 = 1;
    if (_4830 == 0)
    {
        _4830 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _4830 = NOVALUE;
    }

    /** 		return deserialize_file(sdata, 0)*/
    _4831 = _24deserialize_file(_sdata_8723, 0);
    return _4831;
L1: 

    /** 	if atom(sdata) then*/
    _4832 = 1;
    if (_4832 == 0)
    {
        _4832 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _4832 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_4831);
    _4831 = NOVALUE;
    return 0;
L2: 

    /** 	return deserialize_object(sdata, pos, 0)*/
    _4833 = _24deserialize_object(_sdata_8723, _pos_8724, 0);
    DeRef(_4831);
    _4831 = NOVALUE;
    return _4833;
    ;
}


int _24serialize(int _x_8733)
{
    int _x4_8734 = NOVALUE;
    int _s_8735 = NOVALUE;
    int _4875 = NOVALUE;
    int _4874 = NOVALUE;
    int _4873 = NOVALUE;
    int _4871 = NOVALUE;
    int _4870 = NOVALUE;
    int _4868 = NOVALUE;
    int _4866 = NOVALUE;
    int _4865 = NOVALUE;
    int _4864 = NOVALUE;
    int _4863 = NOVALUE;
    int _4861 = NOVALUE;
    int _4859 = NOVALUE;
    int _4858 = NOVALUE;
    int _4857 = NOVALUE;
    int _4856 = NOVALUE;
    int _4855 = NOVALUE;
    int _4854 = NOVALUE;
    int _4853 = NOVALUE;
    int _4852 = NOVALUE;
    int _4851 = NOVALUE;
    int _4849 = NOVALUE;
    int _4848 = NOVALUE;
    int _4847 = NOVALUE;
    int _4846 = NOVALUE;
    int _4845 = NOVALUE;
    int _4844 = NOVALUE;
    int _4842 = NOVALUE;
    int _4841 = NOVALUE;
    int _4840 = NOVALUE;
    int _4839 = NOVALUE;
    int _4838 = NOVALUE;
    int _4837 = NOVALUE;
    int _4836 = NOVALUE;
    int _4835 = NOVALUE;
    int _4834 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_8733))
    _4834 = 1;
    else if (IS_ATOM_DBL(_x_8733))
    _4834 = IS_ATOM_INT(DoubleToInt(_x_8733));
    else
    _4834 = 0;
    if (_4834 == 0)
    {
        _4834 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _4834 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_8733)) {
        _4835 = (_x_8733 >= -9);
    }
    else {
        _4835 = binary_op(GREATEREQ, _x_8733, -9);
    }
    if (IS_ATOM_INT(_4835)) {
        if (_4835 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_4835)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_8733)) {
        _4837 = (_x_8733 <= 239);
    }
    else {
        _4837 = binary_op(LESSEQ, _x_8733, 239);
    }
    if (_4837 == 0) {
        DeRef(_4837);
        _4837 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_4837) && DBL_PTR(_4837)->dbl == 0.0){
            DeRef(_4837);
            _4837 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_4837);
        _4837 = NOVALUE;
    }
    DeRef(_4837);
    _4837 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_8733)) {
        _4838 = _x_8733 - -9;
        if ((long)((unsigned long)_4838 +(unsigned long) HIGH_BITS) >= 0){
            _4838 = NewDouble((double)_4838);
        }
    }
    else {
        _4838 = binary_op(MINUS, _x_8733, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4838;
    _4839 = MAKE_SEQ(_1);
    _4838 = NOVALUE;
    DeRef(_x_8733);
    DeRef(_x4_8734);
    DeRef(_s_8735);
    DeRef(_4835);
    _4835 = NOVALUE;
    return _4839;
    goto L3; // [41] 319
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_8733)) {
        _4840 = (_x_8733 >= _24MIN2B_8369);
    }
    else {
        _4840 = binary_op(GREATEREQ, _x_8733, _24MIN2B_8369);
    }
    if (IS_ATOM_INT(_4840)) {
        if (_4840 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_4840)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_8733)) {
        _4842 = (_x_8733 <= 32767);
    }
    else {
        _4842 = binary_op(LESSEQ, _x_8733, 32767);
    }
    if (_4842 == 0) {
        DeRef(_4842);
        _4842 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_4842) && DBL_PTR(_4842)->dbl == 0.0){
            DeRef(_4842);
            _4842 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_4842);
        _4842 = NOVALUE;
    }
    DeRef(_4842);
    _4842 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_8733;
    if (IS_ATOM_INT(_x_8733)) {
        _x_8733 = _x_8733 - _24MIN2B_8369;
        if ((long)((unsigned long)_x_8733 +(unsigned long) HIGH_BITS) >= 0){
            _x_8733 = NewDouble((double)_x_8733);
        }
    }
    else {
        _x_8733 = binary_op(MINUS, _x_8733, _24MIN2B_8369);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_8733)) {
        {unsigned long tu;
             tu = (unsigned long)_x_8733 & (unsigned long)255;
             _4844 = MAKE_UINT(tu);
        }
    }
    else {
        _4844 = binary_op(AND_BITS, _x_8733, 255);
    }
    if (IS_ATOM_INT(_x_8733)) {
        if (256 > 0 && _x_8733 >= 0) {
            _4845 = _x_8733 / 256;
        }
        else {
            temp_dbl = floor((double)_x_8733 / (double)256);
            if (_x_8733 != MININT)
            _4845 = (long)temp_dbl;
            else
            _4845 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_8733, 256);
        _4845 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _4844;
    *((int *)(_2+12)) = _4845;
    _4846 = MAKE_SEQ(_1);
    _4845 = NOVALUE;
    _4844 = NOVALUE;
    DeRef(_x_8733);
    DeRef(_x4_8734);
    DeRef(_s_8735);
    DeRef(_4835);
    _4835 = NOVALUE;
    DeRef(_4839);
    _4839 = NOVALUE;
    DeRef(_4840);
    _4840 = NOVALUE;
    return _4846;
    goto L3; // [94] 319
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_8733)) {
        _4847 = (_x_8733 >= _24MIN3B_8376);
    }
    else {
        _4847 = binary_op(GREATEREQ, _x_8733, _24MIN3B_8376);
    }
    if (IS_ATOM_INT(_4847)) {
        if (_4847 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_4847)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_8733)) {
        _4849 = (_x_8733 <= 8388607);
    }
    else {
        _4849 = binary_op(LESSEQ, _x_8733, 8388607);
    }
    if (_4849 == 0) {
        DeRef(_4849);
        _4849 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_4849) && DBL_PTR(_4849)->dbl == 0.0){
            DeRef(_4849);
            _4849 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_4849);
        _4849 = NOVALUE;
    }
    DeRef(_4849);
    _4849 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_8733;
    if (IS_ATOM_INT(_x_8733)) {
        _x_8733 = _x_8733 - _24MIN3B_8376;
        if ((long)((unsigned long)_x_8733 +(unsigned long) HIGH_BITS) >= 0){
            _x_8733 = NewDouble((double)_x_8733);
        }
    }
    else {
        _x_8733 = binary_op(MINUS, _x_8733, _24MIN3B_8376);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_8733)) {
        {unsigned long tu;
             tu = (unsigned long)_x_8733 & (unsigned long)255;
             _4851 = MAKE_UINT(tu);
        }
    }
    else {
        _4851 = binary_op(AND_BITS, _x_8733, 255);
    }
    if (IS_ATOM_INT(_x_8733)) {
        if (256 > 0 && _x_8733 >= 0) {
            _4852 = _x_8733 / 256;
        }
        else {
            temp_dbl = floor((double)_x_8733 / (double)256);
            if (_x_8733 != MININT)
            _4852 = (long)temp_dbl;
            else
            _4852 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_8733, 256);
        _4852 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_4852)) {
        {unsigned long tu;
             tu = (unsigned long)_4852 & (unsigned long)255;
             _4853 = MAKE_UINT(tu);
        }
    }
    else {
        _4853 = binary_op(AND_BITS, _4852, 255);
    }
    DeRef(_4852);
    _4852 = NOVALUE;
    if (IS_ATOM_INT(_x_8733)) {
        if (65536 > 0 && _x_8733 >= 0) {
            _4854 = _x_8733 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_8733 / (double)65536);
            if (_x_8733 != MININT)
            _4854 = (long)temp_dbl;
            else
            _4854 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_8733, 65536);
        _4854 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _4851;
    *((int *)(_2+12)) = _4853;
    *((int *)(_2+16)) = _4854;
    _4855 = MAKE_SEQ(_1);
    _4854 = NOVALUE;
    _4853 = NOVALUE;
    _4851 = NOVALUE;
    DeRef(_x_8733);
    DeRef(_x4_8734);
    DeRef(_s_8735);
    DeRef(_4835);
    _4835 = NOVALUE;
    DeRef(_4839);
    _4839 = NOVALUE;
    DeRef(_4840);
    _4840 = NOVALUE;
    DeRef(_4846);
    _4846 = NOVALUE;
    DeRef(_4847);
    _4847 = NOVALUE;
    return _4855;
    goto L3; // [156] 319
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_8733) && IS_ATOM_INT(_24MIN4B_8382)) {
        _4856 = _x_8733 - _24MIN4B_8382;
        if ((long)((unsigned long)_4856 +(unsigned long) HIGH_BITS) >= 0){
            _4856 = NewDouble((double)_4856);
        }
    }
    else {
        _4856 = binary_op(MINUS, _x_8733, _24MIN4B_8382);
    }
    _4857 = _4int_to_bytes(_4856);
    _4856 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_4857)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_4857)) {
        Prepend(&_4858, _4857, 251);
    }
    else {
        Concat((object_ptr)&_4858, 251, _4857);
    }
    DeRef(_4857);
    _4857 = NOVALUE;
    DeRef(_x_8733);
    DeRef(_x4_8734);
    DeRef(_s_8735);
    DeRef(_4835);
    _4835 = NOVALUE;
    DeRef(_4839);
    _4839 = NOVALUE;
    DeRef(_4840);
    _4840 = NOVALUE;
    DeRef(_4846);
    _4846 = NOVALUE;
    DeRef(_4847);
    _4847 = NOVALUE;
    DeRef(_4855);
    _4855 = NOVALUE;
    return _4858;
    goto L3; // [180] 319
L1: 

    /** 	elsif atom(x) then*/
    _4859 = IS_ATOM(_x_8733);
    if (_4859 == 0)
    {
        _4859 = NOVALUE;
        goto L6; // [188] 240
    }
    else{
        _4859 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/
    Ref(_x_8733);
    _0 = _x4_8734;
    _x4_8734 = _4atom_to_float32(_x_8733);
    DeRef(_0);

    /** 		if x = convert:float32_to_atom(x4) then*/
    RefDS(_x4_8734);
    _4861 = _4float32_to_atom(_x4_8734);
    if (binary_op_a(NOTEQ, _x_8733, _4861)){
        DeRef(_4861);
        _4861 = NOVALUE;
        goto L7; // [205] 222
    }
    DeRef(_4861);
    _4861 = NOVALUE;

    /** 			return F4B & x4*/
    Prepend(&_4863, _x4_8734, 252);
    DeRef(_x_8733);
    DeRefDS(_x4_8734);
    DeRef(_s_8735);
    DeRef(_4835);
    _4835 = NOVALUE;
    DeRef(_4839);
    _4839 = NOVALUE;
    DeRef(_4840);
    _4840 = NOVALUE;
    DeRef(_4846);
    _4846 = NOVALUE;
    DeRef(_4847);
    _4847 = NOVALUE;
    DeRef(_4855);
    _4855 = NOVALUE;
    DeRef(_4858);
    _4858 = NOVALUE;
    return _4863;
    goto L3; // [219] 319
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/
    Ref(_x_8733);
    _4864 = _4atom_to_float64(_x_8733);
    if (IS_SEQUENCE(253) && IS_ATOM(_4864)) {
    }
    else if (IS_ATOM(253) && IS_SEQUENCE(_4864)) {
        Prepend(&_4865, _4864, 253);
    }
    else {
        Concat((object_ptr)&_4865, 253, _4864);
    }
    DeRef(_4864);
    _4864 = NOVALUE;
    DeRef(_x_8733);
    DeRef(_x4_8734);
    DeRef(_s_8735);
    DeRef(_4835);
    _4835 = NOVALUE;
    DeRef(_4839);
    _4839 = NOVALUE;
    DeRef(_4840);
    _4840 = NOVALUE;
    DeRef(_4846);
    _4846 = NOVALUE;
    DeRef(_4847);
    _4847 = NOVALUE;
    DeRef(_4855);
    _4855 = NOVALUE;
    DeRef(_4858);
    _4858 = NOVALUE;
    DeRef(_4863);
    _4863 = NOVALUE;
    return _4865;
    goto L3; // [237] 319
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_8733)){
            _4866 = SEQ_PTR(_x_8733)->length;
    }
    else {
        _4866 = 1;
    }
    if (_4866 > 255)
    goto L8; // [245] 261

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_8733)){
            _4868 = SEQ_PTR(_x_8733)->length;
    }
    else {
        _4868 = 1;
    }
    DeRef(_s_8735);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _4868;
    _s_8735 = MAKE_SEQ(_1);
    _4868 = NOVALUE;
    goto L9; // [258] 275
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_8733)){
            _4870 = SEQ_PTR(_x_8733)->length;
    }
    else {
        _4870 = 1;
    }
    _4871 = _4int_to_bytes(_4870);
    _4870 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_4871)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_4871)) {
        Prepend(&_s_8735, _4871, 255);
    }
    else {
        Concat((object_ptr)&_s_8735, 255, _4871);
    }
    DeRef(_4871);
    _4871 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_8733)){
            _4873 = SEQ_PTR(_x_8733)->length;
    }
    else {
        _4873 = 1;
    }
    {
        int _i_8792;
        _i_8792 = 1;
LA: 
        if (_i_8792 > _4873){
            goto LB; // [280] 310
        }

        /** 			s &= serialize(x[i])*/
        _2 = (int)SEQ_PTR(_x_8733);
        _4874 = (int)*(((s1_ptr)_2)->base + _i_8792);
        Ref(_4874);
        _4875 = _24serialize(_4874);
        _4874 = NOVALUE;
        if (IS_SEQUENCE(_s_8735) && IS_ATOM(_4875)) {
            Ref(_4875);
            Append(&_s_8735, _s_8735, _4875);
        }
        else if (IS_ATOM(_s_8735) && IS_SEQUENCE(_4875)) {
        }
        else {
            Concat((object_ptr)&_s_8735, _s_8735, _4875);
        }
        DeRef(_4875);
        _4875 = NOVALUE;

        /** 		end for*/
        _i_8792 = _i_8792 + 1;
        goto LA; // [305] 287
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_8733);
    DeRef(_x4_8734);
    DeRef(_4835);
    _4835 = NOVALUE;
    DeRef(_4839);
    _4839 = NOVALUE;
    DeRef(_4840);
    _4840 = NOVALUE;
    DeRef(_4846);
    _4846 = NOVALUE;
    DeRef(_4847);
    _4847 = NOVALUE;
    DeRef(_4855);
    _4855 = NOVALUE;
    DeRef(_4858);
    _4858 = NOVALUE;
    DeRef(_4863);
    _4863 = NOVALUE;
    DeRef(_4865);
    _4865 = NOVALUE;
    return _s_8735;
L3: 
    ;
}



// 0x7B3432D7
