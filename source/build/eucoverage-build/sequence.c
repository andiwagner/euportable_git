// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _20reverse(int _target_5033, int _pFrom_5034, int _pTo_5035)
{
    int _uppr_5036 = NOVALUE;
    int _n_5037 = NOVALUE;
    int _lLimit_5038 = NOVALUE;
    int _t_5039 = NOVALUE;
    int _2593 = NOVALUE;
    int _2592 = NOVALUE;
    int _2591 = NOVALUE;
    int _2589 = NOVALUE;
    int _2588 = NOVALUE;
    int _2586 = NOVALUE;
    int _2584 = NOVALUE;
    int _0, _1, _2;
    

    /** 	n = length(target)*/
    if (IS_SEQUENCE(_target_5033)){
            _n_5037 = SEQ_PTR(_target_5033)->length;
    }
    else {
        _n_5037 = 1;
    }

    /** 	if n < 2 then*/
    if (_n_5037 >= 2)
    goto L1; // [18] 29

    /** 		return target*/
    DeRef(_t_5039);
    return _target_5033;
L1: 

    /** 	if pFrom < 1 then*/
    if (_pFrom_5034 >= 1)
    goto L2; // [31] 43

    /** 		pFrom = 1*/
    _pFrom_5034 = 1;
L2: 

    /** 	if pTo < 1 then*/
    if (_pTo_5035 >= 1)
    goto L3; // [45] 58

    /** 		pTo = n + pTo*/
    _pTo_5035 = _n_5037 + _pTo_5035;
L3: 

    /** 	if pTo < pFrom or pFrom >= n then*/
    _2584 = (_pTo_5035 < _pFrom_5034);
    if (_2584 != 0) {
        goto L4; // [64] 77
    }
    _2586 = (_pFrom_5034 >= _n_5037);
    if (_2586 == 0)
    {
        DeRef(_2586);
        _2586 = NOVALUE;
        goto L5; // [73] 84
    }
    else{
        DeRef(_2586);
        _2586 = NOVALUE;
    }
L4: 

    /** 		return target*/
    DeRef(_t_5039);
    DeRef(_2584);
    _2584 = NOVALUE;
    return _target_5033;
L5: 

    /** 	if pTo > n then*/
    if (_pTo_5035 <= _n_5037)
    goto L6; // [86] 98

    /** 		pTo = n*/
    _pTo_5035 = _n_5037;
L6: 

    /** 	lLimit = floor((pFrom+pTo-1)/2)*/
    _2588 = _pFrom_5034 + _pTo_5035;
    if ((long)((unsigned long)_2588 + (unsigned long)HIGH_BITS) >= 0) 
    _2588 = NewDouble((double)_2588);
    if (IS_ATOM_INT(_2588)) {
        _2589 = _2588 - 1;
        if ((long)((unsigned long)_2589 +(unsigned long) HIGH_BITS) >= 0){
            _2589 = NewDouble((double)_2589);
        }
    }
    else {
        _2589 = NewDouble(DBL_PTR(_2588)->dbl - (double)1);
    }
    DeRef(_2588);
    _2588 = NOVALUE;
    if (IS_ATOM_INT(_2589)) {
        _lLimit_5038 = _2589 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _2589, 2);
        _lLimit_5038 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_2589);
    _2589 = NOVALUE;
    if (!IS_ATOM_INT(_lLimit_5038)) {
        _1 = (long)(DBL_PTR(_lLimit_5038)->dbl);
        if (UNIQUE(DBL_PTR(_lLimit_5038)) && (DBL_PTR(_lLimit_5038)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lLimit_5038);
        _lLimit_5038 = _1;
    }

    /** 	t = target*/
    Ref(_target_5033);
    DeRef(_t_5039);
    _t_5039 = _target_5033;

    /** 	uppr = pTo*/
    _uppr_5036 = _pTo_5035;

    /** 	for lowr = pFrom to lLimit do*/
    _2591 = _lLimit_5038;
    {
        int _lowr_5058;
        _lowr_5058 = _pFrom_5034;
L7: 
        if (_lowr_5058 > _2591){
            goto L8; // [135] 177
        }

        /** 		t[uppr] = target[lowr]*/
        _2 = (int)SEQ_PTR(_target_5033);
        _2592 = (int)*(((s1_ptr)_2)->base + _lowr_5058);
        Ref(_2592);
        _2 = (int)SEQ_PTR(_t_5039);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_5039 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _uppr_5036);
        _1 = *(int *)_2;
        *(int *)_2 = _2592;
        if( _1 != _2592 ){
            DeRef(_1);
        }
        _2592 = NOVALUE;

        /** 		t[lowr] = target[uppr]*/
        _2 = (int)SEQ_PTR(_target_5033);
        _2593 = (int)*(((s1_ptr)_2)->base + _uppr_5036);
        Ref(_2593);
        _2 = (int)SEQ_PTR(_t_5039);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_5039 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lowr_5058);
        _1 = *(int *)_2;
        *(int *)_2 = _2593;
        if( _1 != _2593 ){
            DeRef(_1);
        }
        _2593 = NOVALUE;

        /** 		uppr -= 1*/
        _uppr_5036 = _uppr_5036 - 1;

        /** 	end for*/
        _lowr_5058 = _lowr_5058 + 1;
        goto L7; // [172] 142
L8: 
        ;
    }

    /** 	return t*/
    DeRef(_target_5033);
    DeRef(_2584);
    _2584 = NOVALUE;
    return _t_5039;
    ;
}


int _20pad_tail(int _target_5134, int _size_5135, int _ch_5136)
{
    int _2630 = NOVALUE;
    int _2629 = NOVALUE;
    int _2628 = NOVALUE;
    int _2627 = NOVALUE;
    int _2625 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if size <= length(target) then*/
    if (IS_SEQUENCE(_target_5134)){
            _2625 = SEQ_PTR(_target_5134)->length;
    }
    else {
        _2625 = 1;
    }
    if (_size_5135 > _2625)
    goto L1; // [10] 21

    /** 		return target*/
    return _target_5134;
L1: 

    /** 	return target & repeat(ch, size - length(target))*/
    if (IS_SEQUENCE(_target_5134)){
            _2627 = SEQ_PTR(_target_5134)->length;
    }
    else {
        _2627 = 1;
    }
    _2628 = _size_5135 - _2627;
    _2627 = NOVALUE;
    _2629 = Repeat(_ch_5136, _2628);
    _2628 = NOVALUE;
    if (IS_SEQUENCE(_target_5134) && IS_ATOM(_2629)) {
    }
    else if (IS_ATOM(_target_5134) && IS_SEQUENCE(_2629)) {
        Ref(_target_5134);
        Prepend(&_2630, _2629, _target_5134);
    }
    else {
        Concat((object_ptr)&_2630, _target_5134, _2629);
    }
    DeRefDS(_2629);
    _2629 = NOVALUE;
    DeRef(_target_5134);
    return _2630;
    ;
}


int _20filter_alpha(int _elem_5653, int _ud_5654)
{
    int _2945 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return t_alpha(elem)*/
    Ref(_elem_5653);
    _2945 = _3t_alpha(_elem_5653);
    DeRef(_elem_5653);
    return _2945;
    ;
}


int _20split(int _st_5698, int _delim_5699, int _no_empty_5700, int _limit_5701)
{
    int _ret_5702 = NOVALUE;
    int _start_5703 = NOVALUE;
    int _pos_5704 = NOVALUE;
    int _k_5756 = NOVALUE;
    int _3013 = NOVALUE;
    int _3011 = NOVALUE;
    int _3010 = NOVALUE;
    int _3006 = NOVALUE;
    int _3005 = NOVALUE;
    int _3004 = NOVALUE;
    int _3001 = NOVALUE;
    int _3000 = NOVALUE;
    int _2995 = NOVALUE;
    int _2994 = NOVALUE;
    int _2990 = NOVALUE;
    int _2986 = NOVALUE;
    int _2984 = NOVALUE;
    int _2983 = NOVALUE;
    int _2979 = NOVALUE;
    int _2977 = NOVALUE;
    int _2976 = NOVALUE;
    int _2975 = NOVALUE;
    int _2974 = NOVALUE;
    int _2971 = NOVALUE;
    int _2970 = NOVALUE;
    int _2969 = NOVALUE;
    int _2968 = NOVALUE;
    int _2967 = NOVALUE;
    int _2965 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence ret = {}*/
    RefDS(_5);
    DeRef(_ret_5702);
    _ret_5702 = _5;

    /** 	if length(st) = 0 then*/
    if (IS_SEQUENCE(_st_5698)){
            _2965 = SEQ_PTR(_st_5698)->length;
    }
    else {
        _2965 = 1;
    }
    if (_2965 != 0)
    goto L1; // [23] 34

    /** 		return ret*/
    DeRefDS(_st_5698);
    return _ret_5702;
L1: 

    /** 	if sequence(delim) then*/
    _2967 = 0;
    if (_2967 == 0)
    {
        _2967 = NOVALUE;
        goto L2; // [39] 225
    }
    else{
        _2967 = NOVALUE;
    }

    /** 		if equal(delim, "") then*/
    if (_delim_5699 == _5)
    _2968 = 1;
    else if (IS_ATOM_INT(_delim_5699) && IS_ATOM_INT(_5))
    _2968 = 0;
    else
    _2968 = (compare(_delim_5699, _5) == 0);
    if (_2968 == 0)
    {
        _2968 = NOVALUE;
        goto L3; // [48] 133
    }
    else{
        _2968 = NOVALUE;
    }

    /** 			for i = 1 to length(st) do*/
    if (IS_SEQUENCE(_st_5698)){
            _2969 = SEQ_PTR(_st_5698)->length;
    }
    else {
        _2969 = 1;
    }
    {
        int _i_5713;
        _i_5713 = 1;
L4: 
        if (_i_5713 > _2969){
            goto L5; // [56] 126
        }

        /** 				st[i] = {st[i]}*/
        _2 = (int)SEQ_PTR(_st_5698);
        _2970 = (int)*(((s1_ptr)_2)->base + _i_5713);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_2970);
        *((int *)(_2+4)) = _2970;
        _2971 = MAKE_SEQ(_1);
        _2970 = NOVALUE;
        _2 = (int)SEQ_PTR(_st_5698);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _st_5698 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5713);
        _1 = *(int *)_2;
        *(int *)_2 = _2971;
        if( _1 != _2971 ){
            DeRef(_1);
        }
        _2971 = NOVALUE;

        /** 				limit -= 1*/
        _limit_5701 = _limit_5701 - 1;

        /** 				if limit = 0 then*/
        if (_limit_5701 != 0)
        goto L6; // [87] 119

        /** 					st = append(st[1 .. i],st[i+1 .. $])*/
        rhs_slice_target = (object_ptr)&_2974;
        RHS_Slice(_st_5698, 1, _i_5713);
        _2975 = _i_5713 + 1;
        if (IS_SEQUENCE(_st_5698)){
                _2976 = SEQ_PTR(_st_5698)->length;
        }
        else {
            _2976 = 1;
        }
        rhs_slice_target = (object_ptr)&_2977;
        RHS_Slice(_st_5698, _2975, _2976);
        RefDS(_2977);
        Append(&_st_5698, _2974, _2977);
        DeRefDS(_2974);
        _2974 = NOVALUE;
        DeRefDS(_2977);
        _2977 = NOVALUE;

        /** 					exit*/
        goto L5; // [116] 126
L6: 

        /** 			end for*/
        _i_5713 = _i_5713 + 1;
        goto L4; // [121] 63
L5: 
        ;
    }

    /** 			return st*/
    DeRef(_ret_5702);
    DeRef(_2975);
    _2975 = NOVALUE;
    return _st_5698;
L3: 

    /** 		start = 1*/
    _start_5703 = 1;

    /** 		while start <= length(st) do*/
L7: 
    if (IS_SEQUENCE(_st_5698)){
            _2979 = SEQ_PTR(_st_5698)->length;
    }
    else {
        _2979 = 1;
    }
    if (_start_5703 > _2979)
    goto L8; // [148] 312

    /** 			pos = match(delim, st, start)*/
    _pos_5704 = e_match_from(_delim_5699, _st_5698, _start_5703);

    /** 			if pos = 0 then*/
    if (_pos_5704 != 0)
    goto L9; // [163] 172

    /** 				exit*/
    goto L8; // [169] 312
L9: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _2983 = _pos_5704 - 1;
    rhs_slice_target = (object_ptr)&_2984;
    RHS_Slice(_st_5698, _start_5703, _2983);
    RefDS(_2984);
    Append(&_ret_5702, _ret_5702, _2984);
    DeRefDS(_2984);
    _2984 = NOVALUE;

    /** 			start = pos+length(delim)*/
    _2986 = 1;
    _start_5703 = _pos_5704 + 1;
    _2986 = NOVALUE;

    /** 			limit -= 1*/
    _limit_5701 = _limit_5701 - 1;

    /** 			if limit = 0 then*/
    if (_limit_5701 != 0)
    goto L7; // [208] 145

    /** 				exit*/
    goto L8; // [214] 312

    /** 		end while*/
    goto L7; // [219] 145
    goto L8; // [222] 312
L2: 

    /** 		start = 1*/
    _start_5703 = 1;

    /** 		while start <= length(st) do*/
LA: 
    if (IS_SEQUENCE(_st_5698)){
            _2990 = SEQ_PTR(_st_5698)->length;
    }
    else {
        _2990 = 1;
    }
    if (_start_5703 > _2990)
    goto LB; // [240] 311

    /** 			pos = find(delim, st, start)*/
    _pos_5704 = find_from(_delim_5699, _st_5698, _start_5703);

    /** 			if pos = 0 then*/
    if (_pos_5704 != 0)
    goto LC; // [255] 264

    /** 				exit*/
    goto LB; // [261] 311
LC: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _2994 = _pos_5704 - 1;
    rhs_slice_target = (object_ptr)&_2995;
    RHS_Slice(_st_5698, _start_5703, _2994);
    RefDS(_2995);
    Append(&_ret_5702, _ret_5702, _2995);
    DeRefDS(_2995);
    _2995 = NOVALUE;

    /** 			start = pos + 1*/
    _start_5703 = _pos_5704 + 1;

    /** 			limit -= 1*/
    _limit_5701 = _limit_5701 - 1;

    /** 			if limit = 0 then*/
    if (_limit_5701 != 0)
    goto LA; // [297] 237

    /** 				exit*/
    goto LB; // [303] 311

    /** 		end while*/
    goto LA; // [308] 237
LB: 
L8: 

    /** 	ret = append(ret, st[start..$])*/
    if (IS_SEQUENCE(_st_5698)){
            _3000 = SEQ_PTR(_st_5698)->length;
    }
    else {
        _3000 = 1;
    }
    rhs_slice_target = (object_ptr)&_3001;
    RHS_Slice(_st_5698, _start_5703, _3000);
    RefDS(_3001);
    Append(&_ret_5702, _ret_5702, _3001);
    DeRefDS(_3001);
    _3001 = NOVALUE;

    /** 	integer k = length(ret)*/
    if (IS_SEQUENCE(_ret_5702)){
            _k_5756 = SEQ_PTR(_ret_5702)->length;
    }
    else {
        _k_5756 = 1;
    }

    /** 	if no_empty then*/
    if (_no_empty_5700 == 0)
    {
        goto LD; // [337] 406
    }
    else{
    }

    /** 		k = 0*/
    _k_5756 = 0;

    /** 		for i = 1 to length(ret) do*/
    if (IS_SEQUENCE(_ret_5702)){
            _3004 = SEQ_PTR(_ret_5702)->length;
    }
    else {
        _3004 = 1;
    }
    {
        int _i_5760;
        _i_5760 = 1;
LE: 
        if (_i_5760 > _3004){
            goto LF; // [352] 405
        }

        /** 			if length(ret[i]) != 0 then*/
        _2 = (int)SEQ_PTR(_ret_5702);
        _3005 = (int)*(((s1_ptr)_2)->base + _i_5760);
        if (IS_SEQUENCE(_3005)){
                _3006 = SEQ_PTR(_3005)->length;
        }
        else {
            _3006 = 1;
        }
        _3005 = NOVALUE;
        if (_3006 == 0)
        goto L10; // [368] 398

        /** 				k += 1*/
        _k_5756 = _k_5756 + 1;

        /** 				if k != i then*/
        if (_k_5756 == _i_5760)
        goto L11; // [382] 397

        /** 					ret[k] = ret[i]*/
        _2 = (int)SEQ_PTR(_ret_5702);
        _3010 = (int)*(((s1_ptr)_2)->base + _i_5760);
        Ref(_3010);
        _2 = (int)SEQ_PTR(_ret_5702);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_5702 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _k_5756);
        _1 = *(int *)_2;
        *(int *)_2 = _3010;
        if( _1 != _3010 ){
            DeRef(_1);
        }
        _3010 = NOVALUE;
L11: 
L10: 

        /** 		end for*/
        _i_5760 = _i_5760 + 1;
        goto LE; // [400] 359
LF: 
        ;
    }
LD: 

    /** 	if k < length(ret) then*/
    if (IS_SEQUENCE(_ret_5702)){
            _3011 = SEQ_PTR(_ret_5702)->length;
    }
    else {
        _3011 = 1;
    }
    if (_k_5756 >= _3011)
    goto L12; // [411] 429

    /** 		return ret[1 .. k]*/
    rhs_slice_target = (object_ptr)&_3013;
    RHS_Slice(_ret_5702, 1, _k_5756);
    DeRefDS(_st_5698);
    DeRefDS(_ret_5702);
    DeRef(_2983);
    _2983 = NOVALUE;
    DeRef(_2975);
    _2975 = NOVALUE;
    DeRef(_2994);
    _2994 = NOVALUE;
    _3005 = NOVALUE;
    return _3013;
    goto L13; // [426] 436
L12: 

    /** 		return ret*/
    DeRefDS(_st_5698);
    DeRef(_2983);
    _2983 = NOVALUE;
    DeRef(_2975);
    _2975 = NOVALUE;
    DeRef(_2994);
    _2994 = NOVALUE;
    _3005 = NOVALUE;
    DeRef(_3013);
    _3013 = NOVALUE;
    return _ret_5702;
L13: 
    ;
}


int _20join(int _items_5825, int _delim_5826)
{
    int _ret_5828 = NOVALUE;
    int _3048 = NOVALUE;
    int _3047 = NOVALUE;
    int _3045 = NOVALUE;
    int _3044 = NOVALUE;
    int _3043 = NOVALUE;
    int _3042 = NOVALUE;
    int _3040 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(items) then return {} end if*/
    if (IS_SEQUENCE(_items_5825)){
            _3040 = SEQ_PTR(_items_5825)->length;
    }
    else {
        _3040 = 1;
    }
    if (_3040 != 0)
    goto L1; // [8] 16
    _3040 = NOVALUE;
    RefDS(_5);
    DeRefDS(_items_5825);
    DeRef(_ret_5828);
    return _5;
L1: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_5828);
    _ret_5828 = _5;

    /** 	for i=1 to length(items)-1 do*/
    if (IS_SEQUENCE(_items_5825)){
            _3042 = SEQ_PTR(_items_5825)->length;
    }
    else {
        _3042 = 1;
    }
    _3043 = _3042 - 1;
    _3042 = NOVALUE;
    {
        int _i_5833;
        _i_5833 = 1;
L2: 
        if (_i_5833 > _3043){
            goto L3; // [30] 58
        }

        /** 		ret &= items[i] & delim*/
        _2 = (int)SEQ_PTR(_items_5825);
        _3044 = (int)*(((s1_ptr)_2)->base + _i_5833);
        if (IS_SEQUENCE(_3044) && IS_ATOM(_delim_5826)) {
            Append(&_3045, _3044, _delim_5826);
        }
        else if (IS_ATOM(_3044) && IS_SEQUENCE(_delim_5826)) {
        }
        else {
            Concat((object_ptr)&_3045, _3044, _delim_5826);
            _3044 = NOVALUE;
        }
        _3044 = NOVALUE;
        if (IS_SEQUENCE(_ret_5828) && IS_ATOM(_3045)) {
        }
        else if (IS_ATOM(_ret_5828) && IS_SEQUENCE(_3045)) {
            Ref(_ret_5828);
            Prepend(&_ret_5828, _3045, _ret_5828);
        }
        else {
            Concat((object_ptr)&_ret_5828, _ret_5828, _3045);
        }
        DeRefDS(_3045);
        _3045 = NOVALUE;

        /** 	end for*/
        _i_5833 = _i_5833 + 1;
        goto L2; // [53] 37
L3: 
        ;
    }

    /** 	ret &= items[$]*/
    if (IS_SEQUENCE(_items_5825)){
            _3047 = SEQ_PTR(_items_5825)->length;
    }
    else {
        _3047 = 1;
    }
    _2 = (int)SEQ_PTR(_items_5825);
    _3048 = (int)*(((s1_ptr)_2)->base + _3047);
    if (IS_SEQUENCE(_ret_5828) && IS_ATOM(_3048)) {
        Ref(_3048);
        Append(&_ret_5828, _ret_5828, _3048);
    }
    else if (IS_ATOM(_ret_5828) && IS_SEQUENCE(_3048)) {
        Ref(_ret_5828);
        Prepend(&_ret_5828, _3048, _ret_5828);
    }
    else {
        Concat((object_ptr)&_ret_5828, _ret_5828, _3048);
    }
    _3048 = NOVALUE;

    /** 	return ret*/
    DeRefDS(_items_5825);
    DeRef(_3043);
    _3043 = NOVALUE;
    return _ret_5828;
    ;
}


int _20flatten(int _s_5936, int _delim_5937)
{
    int _ret_5938 = NOVALUE;
    int _x_5939 = NOVALUE;
    int _len_5940 = NOVALUE;
    int _pos_5941 = NOVALUE;
    int _temp_5960 = NOVALUE;
    int _3137 = NOVALUE;
    int _3136 = NOVALUE;
    int _3135 = NOVALUE;
    int _3133 = NOVALUE;
    int _3132 = NOVALUE;
    int _3131 = NOVALUE;
    int _3129 = NOVALUE;
    int _3127 = NOVALUE;
    int _3126 = NOVALUE;
    int _3125 = NOVALUE;
    int _3124 = NOVALUE;
    int _3122 = NOVALUE;
    int _3121 = NOVALUE;
    int _3120 = NOVALUE;
    int _3119 = NOVALUE;
    int _3118 = NOVALUE;
    int _3117 = NOVALUE;
    int _3116 = NOVALUE;
    int _3114 = NOVALUE;
    int _3113 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ret = s*/
    RefDS(_s_5936);
    DeRef(_ret_5938);
    _ret_5938 = _s_5936;

    /** 	pos = 1*/
    _pos_5941 = 1;

    /** 	len = length(ret)*/
    if (IS_SEQUENCE(_ret_5938)){
            _len_5940 = SEQ_PTR(_ret_5938)->length;
    }
    else {
        _len_5940 = 1;
    }

    /** 	while pos <= len do*/
L1: 
    if (_pos_5941 > _len_5940)
    goto L2; // [29] 197

    /** 		x = ret[pos]*/
    DeRef(_x_5939);
    _2 = (int)SEQ_PTR(_ret_5938);
    _x_5939 = (int)*(((s1_ptr)_2)->base + _pos_5941);
    Ref(_x_5939);

    /** 		if sequence(x) then*/
    _3113 = IS_SEQUENCE(_x_5939);
    if (_3113 == 0)
    {
        _3113 = NOVALUE;
        goto L3; // [44] 183
    }
    else{
        _3113 = NOVALUE;
    }

    /** 			if length(delim) = 0 then*/
    if (IS_SEQUENCE(_delim_5937)){
            _3114 = SEQ_PTR(_delim_5937)->length;
    }
    else {
        _3114 = 1;
    }
    if (_3114 != 0)
    goto L4; // [52] 96

    /** 				ret = ret[1..pos-1] & flatten(x) & ret[pos+1 .. $]*/
    _3116 = _pos_5941 - 1;
    rhs_slice_target = (object_ptr)&_3117;
    RHS_Slice(_ret_5938, 1, _3116);
    Ref(_x_5939);
    DeRef(_3118);
    _3118 = _x_5939;
    RefDS(_5);
    _3119 = _20flatten(_3118, _5);
    _3118 = NOVALUE;
    _3120 = _pos_5941 + 1;
    if (_3120 > MAXINT){
        _3120 = NewDouble((double)_3120);
    }
    if (IS_SEQUENCE(_ret_5938)){
            _3121 = SEQ_PTR(_ret_5938)->length;
    }
    else {
        _3121 = 1;
    }
    rhs_slice_target = (object_ptr)&_3122;
    RHS_Slice(_ret_5938, _3120, _3121);
    {
        int concat_list[3];

        concat_list[0] = _3122;
        concat_list[1] = _3119;
        concat_list[2] = _3117;
        Concat_N((object_ptr)&_ret_5938, concat_list, 3);
    }
    DeRefDS(_3122);
    _3122 = NOVALUE;
    DeRef(_3119);
    _3119 = NOVALUE;
    DeRefDS(_3117);
    _3117 = NOVALUE;
    goto L5; // [93] 173
L4: 

    /** 				sequence temp = ret[1..pos-1] & flatten(x)*/
    _3124 = _pos_5941 - 1;
    rhs_slice_target = (object_ptr)&_3125;
    RHS_Slice(_ret_5938, 1, _3124);
    Ref(_x_5939);
    DeRef(_3126);
    _3126 = _x_5939;
    RefDS(_5);
    _3127 = _20flatten(_3126, _5);
    _3126 = NOVALUE;
    if (IS_SEQUENCE(_3125) && IS_ATOM(_3127)) {
        Ref(_3127);
        Append(&_temp_5960, _3125, _3127);
    }
    else if (IS_ATOM(_3125) && IS_SEQUENCE(_3127)) {
    }
    else {
        Concat((object_ptr)&_temp_5960, _3125, _3127);
        DeRefDS(_3125);
        _3125 = NOVALUE;
    }
    DeRef(_3125);
    _3125 = NOVALUE;
    DeRef(_3127);
    _3127 = NOVALUE;

    /** 				if pos != length(ret) then*/
    if (IS_SEQUENCE(_ret_5938)){
            _3129 = SEQ_PTR(_ret_5938)->length;
    }
    else {
        _3129 = 1;
    }
    if (_pos_5941 == _3129)
    goto L6; // [124] 151

    /** 					ret = temp &  delim & ret[pos+1 .. $]*/
    _3131 = _pos_5941 + 1;
    if (_3131 > MAXINT){
        _3131 = NewDouble((double)_3131);
    }
    if (IS_SEQUENCE(_ret_5938)){
            _3132 = SEQ_PTR(_ret_5938)->length;
    }
    else {
        _3132 = 1;
    }
    rhs_slice_target = (object_ptr)&_3133;
    RHS_Slice(_ret_5938, _3131, _3132);
    {
        int concat_list[3];

        concat_list[0] = _3133;
        concat_list[1] = _delim_5937;
        concat_list[2] = _temp_5960;
        Concat_N((object_ptr)&_ret_5938, concat_list, 3);
    }
    DeRefDS(_3133);
    _3133 = NOVALUE;
    goto L7; // [148] 170
L6: 

    /** 					ret = temp & ret[pos+1 .. $]*/
    _3135 = _pos_5941 + 1;
    if (_3135 > MAXINT){
        _3135 = NewDouble((double)_3135);
    }
    if (IS_SEQUENCE(_ret_5938)){
            _3136 = SEQ_PTR(_ret_5938)->length;
    }
    else {
        _3136 = 1;
    }
    rhs_slice_target = (object_ptr)&_3137;
    RHS_Slice(_ret_5938, _3135, _3136);
    Concat((object_ptr)&_ret_5938, _temp_5960, _3137);
    DeRefDS(_3137);
    _3137 = NOVALUE;
L7: 
    DeRef(_temp_5960);
    _temp_5960 = NOVALUE;
L5: 

    /** 			len = length(ret)*/
    if (IS_SEQUENCE(_ret_5938)){
            _len_5940 = SEQ_PTR(_ret_5938)->length;
    }
    else {
        _len_5940 = 1;
    }
    goto L1; // [180] 29
L3: 

    /** 			pos += 1*/
    _pos_5941 = _pos_5941 + 1;

    /** 	end while*/
    goto L1; // [194] 29
L2: 

    /** 	return ret*/
    DeRefDS(_s_5936);
    DeRefi(_delim_5937);
    DeRef(_x_5939);
    DeRef(_3116);
    _3116 = NOVALUE;
    DeRef(_3124);
    _3124 = NOVALUE;
    DeRef(_3131);
    _3131 = NOVALUE;
    DeRef(_3120);
    _3120 = NOVALUE;
    DeRef(_3135);
    _3135 = NOVALUE;
    return _ret_5938;
    ;
}


int _20remove_subseq(int _source_list_6277, int _alt_value_6278)
{
    int _lResult_6279 = NOVALUE;
    int _lCOW_6280 = NOVALUE;
    int _3345 = NOVALUE;
    int _3344 = NOVALUE;
    int _3340 = NOVALUE;
    int _3337 = NOVALUE;
    int _3334 = NOVALUE;
    int _3333 = NOVALUE;
    int _3332 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lCOW = 0*/
    _lCOW_6280 = 0;

    /** 	for i = 1 to length(source_list) do*/
    if (IS_SEQUENCE(_source_list_6277)){
            _3332 = SEQ_PTR(_source_list_6277)->length;
    }
    else {
        _3332 = 1;
    }
    {
        int _i_6282;
        _i_6282 = 1;
L1: 
        if (_i_6282 > _3332){
            goto L2; // [15] 129
        }

        /** 		if atom(source_list[i]) then*/
        _2 = (int)SEQ_PTR(_source_list_6277);
        _3333 = (int)*(((s1_ptr)_2)->base + _i_6282);
        _3334 = IS_ATOM(_3333);
        _3333 = NOVALUE;
        if (_3334 == 0)
        {
            _3334 = NOVALUE;
            goto L3; // [31] 73
        }
        else{
            _3334 = NOVALUE;
        }

        /** 			if lCOW != 0 then*/
        if (_lCOW_6280 == 0)
        goto L4; // [36] 124

        /** 				if lCOW != i then*/
        if (_lCOW_6280 == _i_6282)
        goto L5; // [42] 59

        /** 					lResult[lCOW] = source_list[i]*/
        _2 = (int)SEQ_PTR(_source_list_6277);
        _3337 = (int)*(((s1_ptr)_2)->base + _i_6282);
        Ref(_3337);
        _2 = (int)SEQ_PTR(_lResult_6279);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_6279 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_6280);
        _1 = *(int *)_2;
        *(int *)_2 = _3337;
        if( _1 != _3337 ){
            DeRef(_1);
        }
        _3337 = NOVALUE;
L5: 

        /** 				lCOW += 1*/
        _lCOW_6280 = _lCOW_6280 + 1;

        /** 			continue*/
        goto L4; // [70] 124
L3: 

        /** 		if lCOW = 0 then*/
        if (_lCOW_6280 != 0)
        goto L6; // [75] 94

        /** 			lResult = source_list*/
        RefDS(_source_list_6277);
        DeRef(_lResult_6279);
        _lResult_6279 = _source_list_6277;

        /** 			lCOW = i*/
        _lCOW_6280 = _i_6282;
L6: 

        /** 		if not equal(alt_value, SEQ_NOALT) then*/
        if (_alt_value_6278 == _20SEQ_NOALT_6271)
        _3340 = 1;
        else if (IS_ATOM_INT(_alt_value_6278) && IS_ATOM_INT(_20SEQ_NOALT_6271))
        _3340 = 0;
        else
        _3340 = (compare(_alt_value_6278, _20SEQ_NOALT_6271) == 0);
        if (_3340 != 0)
        goto L7; // [102] 122
        _3340 = NOVALUE;

        /** 			lResult[lCOW] = alt_value*/
        Ref(_alt_value_6278);
        _2 = (int)SEQ_PTR(_lResult_6279);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_6279 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_6280);
        _1 = *(int *)_2;
        *(int *)_2 = _alt_value_6278;
        DeRef(_1);

        /** 			lCOW += 1*/
        _lCOW_6280 = _lCOW_6280 + 1;
L7: 

        /** 	end for*/
L4: 
        _i_6282 = _i_6282 + 1;
        goto L1; // [124] 22
L2: 
        ;
    }

    /** 	if lCOW = 0 then*/
    if (_lCOW_6280 != 0)
    goto L8; // [131] 142

    /** 		return source_list*/
    DeRef(_alt_value_6278);
    DeRef(_lResult_6279);
    return _source_list_6277;
L8: 

    /** 	return lResult[1.. lCOW - 1]*/
    _3344 = _lCOW_6280 - 1;
    rhs_slice_target = (object_ptr)&_3345;
    RHS_Slice(_lResult_6279, 1, _3344);
    DeRefDS(_source_list_6277);
    DeRef(_alt_value_6278);
    DeRefDS(_lResult_6279);
    _3344 = NOVALUE;
    return _3345;
    ;
}



// 0x442BF3E6
