// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _17abs(int _a_3883)
{
    int _t_3884 = NOVALUE;
    int _1973 = NOVALUE;
    int _1972 = NOVALUE;
    int _1971 = NOVALUE;
    int _1969 = NOVALUE;
    int _1967 = NOVALUE;
    int _1966 = NOVALUE;
    int _1964 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _1964 = IS_ATOM(_a_3883);
    if (_1964 == 0)
    {
        _1964 = NOVALUE;
        goto L1; // [6] 35
    }
    else{
        _1964 = NOVALUE;
    }

    /** 		if a >= 0 then*/
    if (binary_op_a(LESS, _a_3883, 0)){
        goto L2; // [11] 24
    }

    /** 			return a*/
    DeRef(_t_3884);
    return _a_3883;
    goto L3; // [21] 34
L2: 

    /** 			return - a*/
    if (IS_ATOM_INT(_a_3883)) {
        if ((unsigned long)_a_3883 == 0xC0000000)
        _1966 = (int)NewDouble((double)-0xC0000000);
        else
        _1966 = - _a_3883;
    }
    else {
        _1966 = unary_op(UMINUS, _a_3883);
    }
    DeRef(_a_3883);
    DeRef(_t_3884);
    return _1966;
L3: 
L1: 

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_3883)){
            _1967 = SEQ_PTR(_a_3883)->length;
    }
    else {
        _1967 = 1;
    }
    {
        int _i_3892;
        _i_3892 = 1;
L4: 
        if (_i_3892 > _1967){
            goto L5; // [40] 101
        }

        /** 		t = a[i]*/
        DeRef(_t_3884);
        _2 = (int)SEQ_PTR(_a_3883);
        _t_3884 = (int)*(((s1_ptr)_2)->base + _i_3892);
        Ref(_t_3884);

        /** 		if atom(t) then*/
        _1969 = IS_ATOM(_t_3884);
        if (_1969 == 0)
        {
            _1969 = NOVALUE;
            goto L6; // [58] 80
        }
        else{
            _1969 = NOVALUE;
        }

        /** 			if t < 0 then*/
        if (binary_op_a(GREATEREQ, _t_3884, 0)){
            goto L7; // [63] 94
        }

        /** 				a[i] = - t*/
        if (IS_ATOM_INT(_t_3884)) {
            if ((unsigned long)_t_3884 == 0xC0000000)
            _1971 = (int)NewDouble((double)-0xC0000000);
            else
            _1971 = - _t_3884;
        }
        else {
            _1971 = unary_op(UMINUS, _t_3884);
        }
        _2 = (int)SEQ_PTR(_a_3883);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_3883 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_3892);
        _1 = *(int *)_2;
        *(int *)_2 = _1971;
        if( _1 != _1971 ){
            DeRef(_1);
        }
        _1971 = NOVALUE;
        goto L7; // [77] 94
L6: 

        /** 			a[i] = abs(t)*/
        Ref(_t_3884);
        DeRef(_1972);
        _1972 = _t_3884;
        _1973 = _17abs(_1972);
        _1972 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_3883);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_3883 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_3892);
        _1 = *(int *)_2;
        *(int *)_2 = _1973;
        if( _1 != _1973 ){
            DeRef(_1);
        }
        _1973 = NOVALUE;
L7: 

        /** 	end for*/
        _i_3892 = _i_3892 + 1;
        goto L4; // [96] 47
L5: 
        ;
    }

    /** 	return a*/
    DeRef(_t_3884);
    DeRef(_1966);
    _1966 = NOVALUE;
    return _a_3883;
    ;
}


int _17max(int _a_3927)
{
    int _b_3928 = NOVALUE;
    int _c_3929 = NOVALUE;
    int _1983 = NOVALUE;
    int _1982 = NOVALUE;
    int _1981 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _1981 = IS_ATOM(_a_3927);
    if (_1981 == 0)
    {
        _1981 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _1981 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_3928);
    DeRef(_c_3929);
    return _a_3927;
L1: 

    /** 	b = mathcons:MINF*/
    RefDS(_19MINF_3861);
    DeRef(_b_3928);
    _b_3928 = _19MINF_3861;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_3927)){
            _1982 = SEQ_PTR(_a_3927)->length;
    }
    else {
        _1982 = 1;
    }
    {
        int _i_3933;
        _i_3933 = 1;
L2: 
        if (_i_3933 > _1982){
            goto L3; // [28] 64
        }

        /** 		c = max(a[i])*/
        _2 = (int)SEQ_PTR(_a_3927);
        _1983 = (int)*(((s1_ptr)_2)->base + _i_3933);
        Ref(_1983);
        _0 = _c_3929;
        _c_3929 = _17max(_1983);
        DeRef(_0);
        _1983 = NOVALUE;

        /** 		if c > b then*/
        if (binary_op_a(LESSEQ, _c_3929, _b_3928)){
            goto L4; // [47] 57
        }

        /** 			b = c*/
        Ref(_c_3929);
        DeRef(_b_3928);
        _b_3928 = _c_3929;
L4: 

        /** 	end for*/
        _i_3933 = _i_3933 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_3927);
    DeRef(_c_3929);
    return _b_3928;
    ;
}


int _17min(int _a_3941)
{
    int _b_3942 = NOVALUE;
    int _c_3943 = NOVALUE;
    int _1988 = NOVALUE;
    int _1987 = NOVALUE;
    int _1986 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _1986 = IS_ATOM(_a_3941);
    if (_1986 == 0)
    {
        _1986 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _1986 = NOVALUE;
    }

    /** 			return a*/
    DeRef(_b_3942);
    DeRef(_c_3943);
    return _a_3941;
L1: 

    /** 	b = mathcons:PINF*/
    RefDS(_19PINF_3858);
    DeRef(_b_3942);
    _b_3942 = _19PINF_3858;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_3941)){
            _1987 = SEQ_PTR(_a_3941)->length;
    }
    else {
        _1987 = 1;
    }
    {
        int _i_3947;
        _i_3947 = 1;
L2: 
        if (_i_3947 > _1987){
            goto L3; // [28] 64
        }

        /** 		c = min(a[i])*/
        _2 = (int)SEQ_PTR(_a_3941);
        _1988 = (int)*(((s1_ptr)_2)->base + _i_3947);
        Ref(_1988);
        _0 = _c_3943;
        _c_3943 = _17min(_1988);
        DeRef(_0);
        _1988 = NOVALUE;

        /** 			if c < b then*/
        if (binary_op_a(GREATEREQ, _c_3943, _b_3942)){
            goto L4; // [47] 57
        }

        /** 				b = c*/
        Ref(_c_3943);
        DeRef(_b_3942);
        _b_3942 = _c_3943;
L4: 

        /** 	end for*/
        _i_3947 = _i_3947 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_3941);
    DeRef(_c_3943);
    return _b_3942;
    ;
}


int _17or_all(int _a_4321)
{
    int _b_4322 = NOVALUE;
    int _2188 = NOVALUE;
    int _2187 = NOVALUE;
    int _2185 = NOVALUE;
    int _2184 = NOVALUE;
    int _2183 = NOVALUE;
    int _2182 = NOVALUE;
    int _2181 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2181 = IS_ATOM(_a_4321);
    if (_2181 == 0)
    {
        _2181 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2181 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4322);
    return _a_4321;
L1: 

    /** 	b = 0*/
    DeRef(_b_4322);
    _b_4322 = 0;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4321)){
            _2182 = SEQ_PTR(_a_4321)->length;
    }
    else {
        _2182 = 1;
    }
    {
        int _i_4326;
        _i_4326 = 1;
L2: 
        if (_i_4326 > _2182){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4321);
        _2183 = (int)*(((s1_ptr)_2)->base + _i_4326);
        _2184 = IS_ATOM(_2183);
        _2183 = NOVALUE;
        if (_2184 == 0)
        {
            _2184 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2184 = NOVALUE;
        }

        /** 			b = or_bits(b, a[i])*/
        _2 = (int)SEQ_PTR(_a_4321);
        _2185 = (int)*(((s1_ptr)_2)->base + _i_4326);
        _0 = _b_4322;
        if (IS_ATOM_INT(_b_4322) && IS_ATOM_INT(_2185)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4322 | (unsigned long)_2185;
                 _b_4322 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4322 = binary_op(OR_BITS, _b_4322, _2185);
        }
        DeRef(_0);
        _2185 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b = or_bits(b, or_all(a[i]))*/
        _2 = (int)SEQ_PTR(_a_4321);
        _2187 = (int)*(((s1_ptr)_2)->base + _i_4326);
        Ref(_2187);
        _2188 = _17or_all(_2187);
        _2187 = NOVALUE;
        _0 = _b_4322;
        if (IS_ATOM_INT(_b_4322) && IS_ATOM_INT(_2188)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4322 | (unsigned long)_2188;
                 _b_4322 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4322 = binary_op(OR_BITS, _b_4322, _2188);
        }
        DeRef(_0);
        DeRef(_2188);
        _2188 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4326 = _i_4326 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4321);
    return _b_4322;
    ;
}



// 0x8E28B094
