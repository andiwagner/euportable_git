// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _9time()
{
    int _ptra_2803 = NOVALUE;
    int _valhi_2804 = NOVALUE;
    int _vallow_2805 = NOVALUE;
    int _deltahi_2806 = NOVALUE;
    int _deltalow_2807 = NOVALUE;
    int _1331 = NOVALUE;
    int _1329 = NOVALUE;
    int _1328 = NOVALUE;
    int _1327 = NOVALUE;
    int _1324 = NOVALUE;
    int _1319 = NOVALUE;
    int _1317 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		atom ptra, valhi, vallow, deltahi, deltalow*/

    /** 		deltahi = 27111902*/
    _deltahi_2806 = 27111902;

    /** 		deltalow = 3577643008*/
    RefDS(_1315);
    DeRef(_deltalow_2807);
    _deltalow_2807 = _1315;

    /** 		ptra = machine:allocate(8)*/
    _0 = _ptra_2803;
    _ptra_2803 = _11allocate(8, 0);
    DeRef(_0);

    /** 		c_proc(time_, {ptra})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ptra_2803);
    *((int *)(_2+4)) = _ptra_2803;
    _1317 = MAKE_SEQ(_1);
    call_c(0, _9time__2785, _1317);
    DeRefDS(_1317);
    _1317 = NOVALUE;

    /** 		vallow = peek4u(ptra)*/
    DeRef(_vallow_2805);
    if (IS_ATOM_INT(_ptra_2803)) {
        _vallow_2805 = *(unsigned long *)_ptra_2803;
        if ((unsigned)_vallow_2805 > (unsigned)MAXINT)
        _vallow_2805 = NewDouble((double)(unsigned long)_vallow_2805);
    }
    else {
        _vallow_2805 = *(unsigned long *)(unsigned long)(DBL_PTR(_ptra_2803)->dbl);
        if ((unsigned)_vallow_2805 > (unsigned)MAXINT)
        _vallow_2805 = NewDouble((double)(unsigned long)_vallow_2805);
    }

    /** 		valhi = peek4u(ptra+4)*/
    if (IS_ATOM_INT(_ptra_2803)) {
        _1319 = _ptra_2803 + 4;
        if ((long)((unsigned long)_1319 + (unsigned long)HIGH_BITS) >= 0) 
        _1319 = NewDouble((double)_1319);
    }
    else {
        _1319 = NewDouble(DBL_PTR(_ptra_2803)->dbl + (double)4);
    }
    DeRef(_valhi_2804);
    if (IS_ATOM_INT(_1319)) {
        _valhi_2804 = *(unsigned long *)_1319;
        if ((unsigned)_valhi_2804 > (unsigned)MAXINT)
        _valhi_2804 = NewDouble((double)(unsigned long)_valhi_2804);
    }
    else {
        _valhi_2804 = *(unsigned long *)(unsigned long)(DBL_PTR(_1319)->dbl);
        if ((unsigned)_valhi_2804 > (unsigned)MAXINT)
        _valhi_2804 = NewDouble((double)(unsigned long)_valhi_2804);
    }
    DeRef(_1319);
    _1319 = NOVALUE;

    /** 		machine:free(ptra)*/
    Ref(_ptra_2803);
    _11free(_ptra_2803);

    /** 		vallow -= deltalow*/
    _0 = _vallow_2805;
    if (IS_ATOM_INT(_vallow_2805)) {
        _vallow_2805 = NewDouble((double)_vallow_2805 - DBL_PTR(_deltalow_2807)->dbl);
    }
    else {
        _vallow_2805 = NewDouble(DBL_PTR(_vallow_2805)->dbl - DBL_PTR(_deltalow_2807)->dbl);
    }
    DeRef(_0);

    /** 		valhi -= deltahi*/
    _0 = _valhi_2804;
    if (IS_ATOM_INT(_valhi_2804)) {
        _valhi_2804 = _valhi_2804 - 27111902;
        if ((long)((unsigned long)_valhi_2804 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_2804 = NewDouble((double)_valhi_2804);
        }
    }
    else {
        _valhi_2804 = NewDouble(DBL_PTR(_valhi_2804)->dbl - (double)27111902);
    }
    DeRef(_0);

    /** 		if vallow < 0 then*/
    if (binary_op_a(GREATEREQ, _vallow_2805, 0)){
        goto L1; // [67] 88
    }

    /** 			vallow += power(2, 32)*/
    _1324 = power(2, 32);
    _0 = _vallow_2805;
    if (IS_ATOM_INT(_1324)) {
        _vallow_2805 = NewDouble(DBL_PTR(_vallow_2805)->dbl + (double)_1324);
    }
    else
    _vallow_2805 = NewDouble(DBL_PTR(_vallow_2805)->dbl + DBL_PTR(_1324)->dbl);
    DeRefDS(_0);
    DeRef(_1324);
    _1324 = NOVALUE;

    /** 			valhi -= 1*/
    _0 = _valhi_2804;
    if (IS_ATOM_INT(_valhi_2804)) {
        _valhi_2804 = _valhi_2804 - 1;
        if ((long)((unsigned long)_valhi_2804 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_2804 = NewDouble((double)_valhi_2804);
        }
    }
    else {
        _valhi_2804 = NewDouble(DBL_PTR(_valhi_2804)->dbl - (double)1);
    }
    DeRef(_0);
L1: 

    /** 		return floor(((valhi * power(2,32)) + vallow) / 10000000)*/
    _1327 = power(2, 32);
    if (IS_ATOM_INT(_valhi_2804) && IS_ATOM_INT(_1327)) {
        if (_valhi_2804 == (short)_valhi_2804 && _1327 <= INT15 && _1327 >= -INT15)
        _1328 = _valhi_2804 * _1327;
        else
        _1328 = NewDouble(_valhi_2804 * (double)_1327);
    }
    else {
        if (IS_ATOM_INT(_valhi_2804)) {
            _1328 = NewDouble((double)_valhi_2804 * DBL_PTR(_1327)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1327)) {
                _1328 = NewDouble(DBL_PTR(_valhi_2804)->dbl * (double)_1327);
            }
            else
            _1328 = NewDouble(DBL_PTR(_valhi_2804)->dbl * DBL_PTR(_1327)->dbl);
        }
    }
    DeRef(_1327);
    _1327 = NOVALUE;
    if (IS_ATOM_INT(_1328) && IS_ATOM_INT(_vallow_2805)) {
        _1329 = _1328 + _vallow_2805;
        if ((long)((unsigned long)_1329 + (unsigned long)HIGH_BITS) >= 0) 
        _1329 = NewDouble((double)_1329);
    }
    else {
        if (IS_ATOM_INT(_1328)) {
            _1329 = NewDouble((double)_1328 + DBL_PTR(_vallow_2805)->dbl);
        }
        else {
            if (IS_ATOM_INT(_vallow_2805)) {
                _1329 = NewDouble(DBL_PTR(_1328)->dbl + (double)_vallow_2805);
            }
            else
            _1329 = NewDouble(DBL_PTR(_1328)->dbl + DBL_PTR(_vallow_2805)->dbl);
        }
    }
    DeRef(_1328);
    _1328 = NOVALUE;
    if (IS_ATOM_INT(_1329)) {
        if (10000000 > 0 && _1329 >= 0) {
            _1331 = _1329 / 10000000;
        }
        else {
            temp_dbl = floor((double)_1329 / (double)10000000);
            if (_1329 != MININT)
            _1331 = (long)temp_dbl;
            else
            _1331 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _1329, 10000000);
        _1331 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_1329);
    _1329 = NOVALUE;
    DeRef(_ptra_2803);
    DeRef(_valhi_2804);
    DeRef(_vallow_2805);
    DeRef(_deltalow_2807);
    return _1331;
    ;
}


int _9gmtime(int _time_2829)
{
    int _ret_2830 = NOVALUE;
    int _timep_2831 = NOVALUE;
    int _tm_p_2832 = NOVALUE;
    int _n_2833 = NOVALUE;
    int _1337 = NOVALUE;
    int _1336 = NOVALUE;
    int _1333 = NOVALUE;
    int _0, _1, _2;
    

    /** 	timep = machine:allocate(4)*/
    _0 = _timep_2831;
    _timep_2831 = _11allocate(4, 0);
    DeRef(_0);

    /** 	poke4(timep, time)*/
    if (IS_ATOM_INT(_timep_2831)){
        poke4_addr = (unsigned long *)_timep_2831;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_timep_2831)->dbl);
    }
    if (IS_ATOM_INT(_time_2829)) {
        *poke4_addr = (unsigned long)_time_2829;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_time_2829)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	tm_p = c_func(gmtime_, {timep})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_timep_2831);
    *((int *)(_2+4)) = _timep_2831;
    _1333 = MAKE_SEQ(_1);
    DeRef(_tm_p_2832);
    _tm_p_2832 = call_c(1, _9gmtime__2779, _1333);
    DeRefDS(_1333);
    _1333 = NOVALUE;

    /** 	machine:free(timep)*/
    Ref(_timep_2831);
    _11free(_timep_2831);

    /** 	ret = repeat(0, 9)*/
    DeRef(_ret_2830);
    _ret_2830 = Repeat(0, 9);

    /** 	n = 0*/
    _n_2833 = 0;

    /** 	for i = 1 to 9 do*/
    {
        int _i_2839;
        _i_2839 = 1;
L1: 
        if (_i_2839 > 9){
            goto L2; // [46] 81
        }

        /** 		ret[i] = peek4s(tm_p+n)*/
        if (IS_ATOM_INT(_tm_p_2832)) {
            _1336 = _tm_p_2832 + _n_2833;
            if ((long)((unsigned long)_1336 + (unsigned long)HIGH_BITS) >= 0) 
            _1336 = NewDouble((double)_1336);
        }
        else {
            _1336 = NewDouble(DBL_PTR(_tm_p_2832)->dbl + (double)_n_2833);
        }
        if (IS_ATOM_INT(_1336)) {
            _1337 = *(unsigned long *)_1336;
            if (_1337 < MININT || _1337 > MAXINT)
            _1337 = NewDouble((double)(long)_1337);
        }
        else {
            _1337 = *(unsigned long *)(unsigned long)(DBL_PTR(_1336)->dbl);
            if (_1337 < MININT || _1337 > MAXINT)
            _1337 = NewDouble((double)(long)_1337);
        }
        DeRef(_1336);
        _1336 = NOVALUE;
        _2 = (int)SEQ_PTR(_ret_2830);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_2830 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2839);
        _1 = *(int *)_2;
        *(int *)_2 = _1337;
        if( _1 != _1337 ){
            DeRef(_1);
        }
        _1337 = NOVALUE;

        /** 		n = n + 4*/
        _n_2833 = _n_2833 + 4;

        /** 	end for*/
        _i_2839 = _i_2839 + 1;
        goto L1; // [76] 53
L2: 
        ;
    }

    /** 	return ret*/
    DeRef(_time_2829);
    DeRef(_timep_2831);
    DeRef(_tm_p_2832);
    return _ret_2830;
    ;
}


int _9tolower(int _x_2856)
{
    int _1350 = NOVALUE;
    int _1349 = NOVALUE;
    int _1348 = NOVALUE;
    int _1347 = NOVALUE;
    int _1346 = NOVALUE;
    int _1345 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return x + (x >= 'A' and x <= 'Z') * ('a' - 'A')*/
    _1345 = binary_op(GREATEREQ, _x_2856, 65);
    _1346 = binary_op(LESSEQ, _x_2856, 90);
    _1347 = binary_op(AND, _1345, _1346);
    DeRefDS(_1345);
    _1345 = NOVALUE;
    DeRefDS(_1346);
    _1346 = NOVALUE;
    _1348 = 32;
    _1349 = binary_op(MULTIPLY, _1347, 32);
    DeRefDS(_1347);
    _1347 = NOVALUE;
    _1348 = NOVALUE;
    _1350 = binary_op(PLUS, _x_2856, _1349);
    DeRefDS(_1349);
    _1349 = NOVALUE;
    DeRefDS(_x_2856);
    return _1350;
    ;
}


int _9isLeap(int _year_2865)
{
    int _ly_2866 = NOVALUE;
    int _1369 = NOVALUE;
    int _1368 = NOVALUE;
    int _1366 = NOVALUE;
    int _1365 = NOVALUE;
    int _1364 = NOVALUE;
    int _1363 = NOVALUE;
    int _1362 = NOVALUE;
    int _1361 = NOVALUE;
    int _1360 = NOVALUE;
    int _1357 = NOVALUE;
    int _1355 = NOVALUE;
    int _1354 = NOVALUE;
    int _0, _1, _2;
    

    /** 		ly = (remainder(year, {4, 100, 400, 3200, 80000})=0)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 4;
    *((int *)(_2+8)) = 100;
    *((int *)(_2+12)) = 400;
    *((int *)(_2+16)) = 3200;
    *((int *)(_2+20)) = 80000;
    _1354 = MAKE_SEQ(_1);
    _1355 = binary_op(REMAINDER, _year_2865, _1354);
    DeRefDS(_1354);
    _1354 = NOVALUE;
    DeRefi(_ly_2866);
    _ly_2866 = binary_op(EQUALS, _1355, 0);
    DeRefDS(_1355);
    _1355 = NOVALUE;

    /** 		if not ly[1] then return 0 end if*/
    _2 = (int)SEQ_PTR(_ly_2866);
    _1357 = (int)*(((s1_ptr)_2)->base + 1);
    if (_1357 != 0)
    goto L1; // [31] 39
    _1357 = NOVALUE;
    DeRefDSi(_ly_2866);
    return 0;
L1: 

    /** 		if year <= Gregorian_Reformation then*/
    if (_year_2865 > 1752)
    goto L2; // [41] 54

    /** 				return 1 -- ly[1] can't possibly be 0 here so set shortcut as '1'.*/
    DeRefi(_ly_2866);
    return 1;
    goto L3; // [51] 97
L2: 

    /** 				return ly[1] - ly[2] + ly[3] - ly[4] + ly[5]*/
    _2 = (int)SEQ_PTR(_ly_2866);
    _1360 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_ly_2866);
    _1361 = (int)*(((s1_ptr)_2)->base + 2);
    _1362 = _1360 - _1361;
    if ((long)((unsigned long)_1362 +(unsigned long) HIGH_BITS) >= 0){
        _1362 = NewDouble((double)_1362);
    }
    _1360 = NOVALUE;
    _1361 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_2866);
    _1363 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_1362)) {
        _1364 = _1362 + _1363;
        if ((long)((unsigned long)_1364 + (unsigned long)HIGH_BITS) >= 0) 
        _1364 = NewDouble((double)_1364);
    }
    else {
        _1364 = NewDouble(DBL_PTR(_1362)->dbl + (double)_1363);
    }
    DeRef(_1362);
    _1362 = NOVALUE;
    _1363 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_2866);
    _1365 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1364)) {
        _1366 = _1364 - _1365;
        if ((long)((unsigned long)_1366 +(unsigned long) HIGH_BITS) >= 0){
            _1366 = NewDouble((double)_1366);
        }
    }
    else {
        _1366 = NewDouble(DBL_PTR(_1364)->dbl - (double)_1365);
    }
    DeRef(_1364);
    _1364 = NOVALUE;
    _1365 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_2866);
    _1368 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1366)) {
        _1369 = _1366 + _1368;
        if ((long)((unsigned long)_1369 + (unsigned long)HIGH_BITS) >= 0) 
        _1369 = NewDouble((double)_1369);
    }
    else {
        _1369 = NewDouble(DBL_PTR(_1366)->dbl + (double)_1368);
    }
    DeRef(_1366);
    _1366 = NOVALUE;
    _1368 = NOVALUE;
    DeRefDSi(_ly_2866);
    return _1369;
L3: 
    ;
}


int _9daysInMonth(int _year_2891, int _month_2892)
{
    int _1377 = NOVALUE;
    int _1376 = NOVALUE;
    int _1375 = NOVALUE;
    int _1374 = NOVALUE;
    int _1372 = NOVALUE;
    int _1371 = NOVALUE;
    int _1370 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_month_2892)) {
        _1 = (long)(DBL_PTR(_month_2892)->dbl);
        if (UNIQUE(DBL_PTR(_month_2892)) && (DBL_PTR(_month_2892)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_2892);
        _month_2892 = _1;
    }

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1370 = (_year_2891 == 1752);
    if (_1370 == 0) {
        goto L1; // [15] 36
    }
    _1372 = (_month_2892 == 9);
    if (_1372 == 0)
    {
        DeRef(_1372);
        _1372 = NOVALUE;
        goto L1; // [24] 36
    }
    else{
        DeRef(_1372);
        _1372 = NOVALUE;
    }

    /** 		return 19*/
    DeRef(_1370);
    _1370 = NOVALUE;
    return 19;
    goto L2; // [33] 74
L1: 

    /** 	elsif month != 2 then*/
    if (_month_2892 == 2)
    goto L3; // [38] 55

    /** 		return DaysPerMonth[month]*/
    _2 = (int)SEQ_PTR(_9DaysPerMonth_2847);
    _1374 = (int)*(((s1_ptr)_2)->base + _month_2892);
    Ref(_1374);
    DeRef(_1370);
    _1370 = NOVALUE;
    return _1374;
    goto L2; // [52] 74
L3: 

    /** 		return DaysPerMonth[month] + isLeap(year)*/
    _2 = (int)SEQ_PTR(_9DaysPerMonth_2847);
    _1375 = (int)*(((s1_ptr)_2)->base + _month_2892);
    _1376 = _9isLeap(_year_2891);
    if (IS_ATOM_INT(_1375) && IS_ATOM_INT(_1376)) {
        _1377 = _1375 + _1376;
        if ((long)((unsigned long)_1377 + (unsigned long)HIGH_BITS) >= 0) 
        _1377 = NewDouble((double)_1377);
    }
    else {
        _1377 = binary_op(PLUS, _1375, _1376);
    }
    _1375 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    DeRef(_1370);
    _1370 = NOVALUE;
    _1374 = NOVALUE;
    return _1377;
L2: 
    ;
}


int _9julianDayOfYear(int _ymd_2915)
{
    int _year_2916 = NOVALUE;
    int _month_2917 = NOVALUE;
    int _day_2918 = NOVALUE;
    int _d_2919 = NOVALUE;
    int _1393 = NOVALUE;
    int _1392 = NOVALUE;
    int _1391 = NOVALUE;
    int _1388 = NOVALUE;
    int _1387 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_2915);
    _year_2916 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_2916)){
        _year_2916 = (long)DBL_PTR(_year_2916)->dbl;
    }

    /** 	month = ymd[2]*/
    _2 = (int)SEQ_PTR(_ymd_2915);
    _month_2917 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_month_2917)){
        _month_2917 = (long)DBL_PTR(_month_2917)->dbl;
    }

    /** 	day = ymd[3]*/
    _2 = (int)SEQ_PTR(_ymd_2915);
    _day_2918 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_day_2918)){
        _day_2918 = (long)DBL_PTR(_day_2918)->dbl;
    }

    /** 	if month = 1 then return day end if*/
    if (_month_2917 != 1)
    goto L1; // [33] 42
    DeRef(_ymd_2915);
    return _day_2918;
L1: 

    /** 	d = 0*/
    _d_2919 = 0;

    /** 	for i = 1 to month - 1 do*/
    _1387 = _month_2917 - 1;
    if ((long)((unsigned long)_1387 +(unsigned long) HIGH_BITS) >= 0){
        _1387 = NewDouble((double)_1387);
    }
    {
        int _i_2926;
        _i_2926 = 1;
L2: 
        if (binary_op_a(GREATER, _i_2926, _1387)){
            goto L3; // [55] 84
        }

        /** 		d += daysInMonth(year, i)*/
        Ref(_i_2926);
        _1388 = _9daysInMonth(_year_2916, _i_2926);
        if (IS_ATOM_INT(_1388)) {
            _d_2919 = _d_2919 + _1388;
        }
        else {
            _d_2919 = binary_op(PLUS, _d_2919, _1388);
        }
        DeRef(_1388);
        _1388 = NOVALUE;
        if (!IS_ATOM_INT(_d_2919)) {
            _1 = (long)(DBL_PTR(_d_2919)->dbl);
            if (UNIQUE(DBL_PTR(_d_2919)) && (DBL_PTR(_d_2919)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_2919);
            _d_2919 = _1;
        }

        /** 	end for*/
        _0 = _i_2926;
        if (IS_ATOM_INT(_i_2926)) {
            _i_2926 = _i_2926 + 1;
            if ((long)((unsigned long)_i_2926 +(unsigned long) HIGH_BITS) >= 0){
                _i_2926 = NewDouble((double)_i_2926);
            }
        }
        else {
            _i_2926 = binary_op_a(PLUS, _i_2926, 1);
        }
        DeRef(_0);
        goto L2; // [79] 62
L3: 
        ;
        DeRef(_i_2926);
    }

    /** 	d += day*/
    _d_2919 = _d_2919 + _day_2918;

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1391 = (_year_2916 == 1752);
    if (_1391 == 0) {
        goto L4; // [98] 142
    }
    _1393 = (_month_2917 == 9);
    if (_1393 == 0)
    {
        DeRef(_1393);
        _1393 = NOVALUE;
        goto L4; // [107] 142
    }
    else{
        DeRef(_1393);
        _1393 = NOVALUE;
    }

    /** 		if day > 13 then*/
    if (_day_2918 <= 13)
    goto L5; // [112] 127

    /** 			d -= 11*/
    _d_2919 = _d_2919 - 11;
    goto L6; // [124] 141
L5: 

    /** 		elsif day > 2 then*/
    if (_day_2918 <= 2)
    goto L7; // [129] 140

    /** 			return 0*/
    DeRef(_ymd_2915);
    DeRef(_1387);
    _1387 = NOVALUE;
    DeRef(_1391);
    _1391 = NOVALUE;
    return 0;
L7: 
L6: 
L4: 

    /** 	return d*/
    DeRef(_ymd_2915);
    DeRef(_1387);
    _1387 = NOVALUE;
    DeRef(_1391);
    _1391 = NOVALUE;
    return _d_2919;
    ;
}


int _9julianDay(int _ymd_2942)
{
    int _year_2943 = NOVALUE;
    int _j_2944 = NOVALUE;
    int _greg00_2945 = NOVALUE;
    int _1422 = NOVALUE;
    int _1419 = NOVALUE;
    int _1416 = NOVALUE;
    int _1415 = NOVALUE;
    int _1414 = NOVALUE;
    int _1413 = NOVALUE;
    int _1412 = NOVALUE;
    int _1411 = NOVALUE;
    int _1410 = NOVALUE;
    int _1409 = NOVALUE;
    int _1407 = NOVALUE;
    int _1406 = NOVALUE;
    int _1405 = NOVALUE;
    int _1404 = NOVALUE;
    int _1403 = NOVALUE;
    int _1402 = NOVALUE;
    int _1401 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_2942);
    _year_2943 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_2943)){
        _year_2943 = (long)DBL_PTR(_year_2943)->dbl;
    }

    /** 	j = julianDayOfYear(ymd)*/
    Ref(_ymd_2942);
    _j_2944 = _9julianDayOfYear(_ymd_2942);
    if (!IS_ATOM_INT(_j_2944)) {
        _1 = (long)(DBL_PTR(_j_2944)->dbl);
        if (UNIQUE(DBL_PTR(_j_2944)) && (DBL_PTR(_j_2944)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_2944);
        _j_2944 = _1;
    }

    /** 	year  -= 1*/
    _year_2943 = _year_2943 - 1;

    /** 	greg00 = year - Gregorian_Reformation00*/
    _greg00_2945 = _year_2943 - 1700;

    /** 	j += (*/
    if (_year_2943 <= INT15 && _year_2943 >= -INT15)
    _1401 = 365 * _year_2943;
    else
    _1401 = NewDouble(365 * (double)_year_2943);
    if (4 > 0 && _year_2943 >= 0) {
        _1402 = _year_2943 / 4;
    }
    else {
        temp_dbl = floor((double)_year_2943 / (double)4);
        _1402 = (long)temp_dbl;
    }
    if (IS_ATOM_INT(_1401)) {
        _1403 = _1401 + _1402;
        if ((long)((unsigned long)_1403 + (unsigned long)HIGH_BITS) >= 0) 
        _1403 = NewDouble((double)_1403);
    }
    else {
        _1403 = NewDouble(DBL_PTR(_1401)->dbl + (double)_1402);
    }
    DeRef(_1401);
    _1401 = NOVALUE;
    _1402 = NOVALUE;
    _1404 = (_greg00_2945 > 0);
    if (100 > 0 && _greg00_2945 >= 0) {
        _1405 = _greg00_2945 / 100;
    }
    else {
        temp_dbl = floor((double)_greg00_2945 / (double)100);
        _1405 = (long)temp_dbl;
    }
    _1406 = - _1405;
    _1407 = (_greg00_2945 % 400) ? NewDouble((double)_greg00_2945 / 400) : (_greg00_2945 / 400);
    if (IS_ATOM_INT(_1407)) {
        _1409 = NewDouble((double)_1407 + DBL_PTR(_1408)->dbl);
    }
    else {
        _1409 = NewDouble(DBL_PTR(_1407)->dbl + DBL_PTR(_1408)->dbl);
    }
    DeRef(_1407);
    _1407 = NOVALUE;
    _1410 = unary_op(FLOOR, _1409);
    DeRefDS(_1409);
    _1409 = NOVALUE;
    if (IS_ATOM_INT(_1410)) {
        _1411 = _1406 + _1410;
        if ((long)((unsigned long)_1411 + (unsigned long)HIGH_BITS) >= 0) 
        _1411 = NewDouble((double)_1411);
    }
    else {
        _1411 = binary_op(PLUS, _1406, _1410);
    }
    _1406 = NOVALUE;
    DeRef(_1410);
    _1410 = NOVALUE;
    if (IS_ATOM_INT(_1411)) {
        if (_1411 <= INT15 && _1411 >= -INT15)
        _1412 = _1404 * _1411;
        else
        _1412 = NewDouble(_1404 * (double)_1411);
    }
    else {
        _1412 = binary_op(MULTIPLY, _1404, _1411);
    }
    _1404 = NOVALUE;
    DeRef(_1411);
    _1411 = NOVALUE;
    if (IS_ATOM_INT(_1403) && IS_ATOM_INT(_1412)) {
        _1413 = _1403 + _1412;
        if ((long)((unsigned long)_1413 + (unsigned long)HIGH_BITS) >= 0) 
        _1413 = NewDouble((double)_1413);
    }
    else {
        _1413 = binary_op(PLUS, _1403, _1412);
    }
    DeRef(_1403);
    _1403 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    _1414 = (_year_2943 >= 1752);
    _1415 = 11 * _1414;
    _1414 = NOVALUE;
    if (IS_ATOM_INT(_1413)) {
        _1416 = _1413 - _1415;
        if ((long)((unsigned long)_1416 +(unsigned long) HIGH_BITS) >= 0){
            _1416 = NewDouble((double)_1416);
        }
    }
    else {
        _1416 = binary_op(MINUS, _1413, _1415);
    }
    DeRef(_1413);
    _1413 = NOVALUE;
    _1415 = NOVALUE;
    if (IS_ATOM_INT(_1416)) {
        _j_2944 = _j_2944 + _1416;
    }
    else {
        _j_2944 = binary_op(PLUS, _j_2944, _1416);
    }
    DeRef(_1416);
    _1416 = NOVALUE;
    if (!IS_ATOM_INT(_j_2944)) {
        _1 = (long)(DBL_PTR(_j_2944)->dbl);
        if (UNIQUE(DBL_PTR(_j_2944)) && (DBL_PTR(_j_2944)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_2944);
        _j_2944 = _1;
    }

    /** 	if year >= 3200 then*/
    if (_year_2943 < 3200)
    goto L1; // [107] 147

    /** 		j -= floor(year/ 3200)*/
    if (3200 > 0 && _year_2943 >= 0) {
        _1419 = _year_2943 / 3200;
    }
    else {
        temp_dbl = floor((double)_year_2943 / (double)3200);
        _1419 = (long)temp_dbl;
    }
    _j_2944 = _j_2944 - _1419;
    _1419 = NOVALUE;

    /** 		if year >= 80000 then*/
    if (_year_2943 < 80000)
    goto L2; // [127] 146

    /** 			j += floor(year/80000)*/
    if (80000 > 0 && _year_2943 >= 0) {
        _1422 = _year_2943 / 80000;
    }
    else {
        temp_dbl = floor((double)_year_2943 / (double)80000);
        _1422 = (long)temp_dbl;
    }
    _j_2944 = _j_2944 + _1422;
    _1422 = NOVALUE;
L2: 
L1: 

    /** 	return j*/
    DeRef(_ymd_2942);
    DeRef(_1405);
    _1405 = NOVALUE;
    return _j_2944;
    ;
}


int _9datetimeToSeconds(int _dt_3026)
{
    int _1470 = NOVALUE;
    int _1469 = NOVALUE;
    int _1467 = NOVALUE;
    int _1466 = NOVALUE;
    int _1465 = NOVALUE;
    int _1464 = NOVALUE;
    int _1463 = NOVALUE;
    int _1462 = NOVALUE;
    int _1461 = NOVALUE;
    int _1460 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return julianDay(dt) * DayLengthInSeconds + (dt[4] * 60 + dt[5]) * 60 + dt[6]*/
    Ref(_dt_3026);
    _1460 = _9julianDay(_dt_3026);
    if (IS_ATOM_INT(_1460)) {
        _1461 = NewDouble(_1460 * (double)86400);
    }
    else {
        _1461 = binary_op(MULTIPLY, _1460, 86400);
    }
    DeRef(_1460);
    _1460 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3026);
    _1462 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1462)) {
        if (_1462 == (short)_1462)
        _1463 = _1462 * 60;
        else
        _1463 = NewDouble(_1462 * (double)60);
    }
    else {
        _1463 = binary_op(MULTIPLY, _1462, 60);
    }
    _1462 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3026);
    _1464 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1463) && IS_ATOM_INT(_1464)) {
        _1465 = _1463 + _1464;
        if ((long)((unsigned long)_1465 + (unsigned long)HIGH_BITS) >= 0) 
        _1465 = NewDouble((double)_1465);
    }
    else {
        _1465 = binary_op(PLUS, _1463, _1464);
    }
    DeRef(_1463);
    _1463 = NOVALUE;
    _1464 = NOVALUE;
    if (IS_ATOM_INT(_1465)) {
        if (_1465 == (short)_1465)
        _1466 = _1465 * 60;
        else
        _1466 = NewDouble(_1465 * (double)60);
    }
    else {
        _1466 = binary_op(MULTIPLY, _1465, 60);
    }
    DeRef(_1465);
    _1465 = NOVALUE;
    if (IS_ATOM_INT(_1461) && IS_ATOM_INT(_1466)) {
        _1467 = _1461 + _1466;
        if ((long)((unsigned long)_1467 + (unsigned long)HIGH_BITS) >= 0) 
        _1467 = NewDouble((double)_1467);
    }
    else {
        _1467 = binary_op(PLUS, _1461, _1466);
    }
    DeRef(_1461);
    _1461 = NOVALUE;
    DeRef(_1466);
    _1466 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3026);
    _1469 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_1467) && IS_ATOM_INT(_1469)) {
        _1470 = _1467 + _1469;
        if ((long)((unsigned long)_1470 + (unsigned long)HIGH_BITS) >= 0) 
        _1470 = NewDouble((double)_1470);
    }
    else {
        _1470 = binary_op(PLUS, _1467, _1469);
    }
    DeRef(_1467);
    _1467 = NOVALUE;
    _1469 = NOVALUE;
    DeRef(_dt_3026);
    return _1470;
    ;
}


int _9from_date(int _src_3205)
{
    int _1597 = NOVALUE;
    int _1596 = NOVALUE;
    int _1595 = NOVALUE;
    int _1594 = NOVALUE;
    int _1593 = NOVALUE;
    int _1592 = NOVALUE;
    int _1591 = NOVALUE;
    int _1589 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {src[YEAR]+1900, src[MONTH], src[DAY], src[HOUR], src[MINUTE], src[SECOND]}*/
    _2 = (int)SEQ_PTR(_src_3205);
    _1589 = (int)*(((s1_ptr)_2)->base + 1);
    _1591 = _1589 + 1900;
    if ((long)((unsigned long)_1591 + (unsigned long)HIGH_BITS) >= 0) 
    _1591 = NewDouble((double)_1591);
    _1589 = NOVALUE;
    _2 = (int)SEQ_PTR(_src_3205);
    _1592 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_src_3205);
    _1593 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_src_3205);
    _1594 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_src_3205);
    _1595 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_src_3205);
    _1596 = (int)*(((s1_ptr)_2)->base + 6);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1591;
    *((int *)(_2+8)) = _1592;
    *((int *)(_2+12)) = _1593;
    *((int *)(_2+16)) = _1594;
    *((int *)(_2+20)) = _1595;
    *((int *)(_2+24)) = _1596;
    _1597 = MAKE_SEQ(_1);
    _1596 = NOVALUE;
    _1595 = NOVALUE;
    _1594 = NOVALUE;
    _1593 = NOVALUE;
    _1592 = NOVALUE;
    _1591 = NOVALUE;
    DeRefDSi(_src_3205);
    return _1597;
    ;
}


int _9now_gmt()
{
    int _t1_3221 = NOVALUE;
    int _1610 = NOVALUE;
    int _1609 = NOVALUE;
    int _1608 = NOVALUE;
    int _1607 = NOVALUE;
    int _1606 = NOVALUE;
    int _1605 = NOVALUE;
    int _1604 = NOVALUE;
    int _1603 = NOVALUE;
    int _1602 = NOVALUE;
    int _1600 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence t1 = gmtime(time())*/
    _1600 = _9time();
    _0 = _t1_3221;
    _t1_3221 = _9gmtime(_1600);
    DeRef(_0);
    _1600 = NOVALUE;

    /** 	return { */
    _2 = (int)SEQ_PTR(_t1_3221);
    _1602 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_1602)) {
        _1603 = _1602 + 1900;
        if ((long)((unsigned long)_1603 + (unsigned long)HIGH_BITS) >= 0) 
        _1603 = NewDouble((double)_1603);
    }
    else {
        _1603 = binary_op(PLUS, _1602, 1900);
    }
    _1602 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_3221);
    _1604 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1604)) {
        _1605 = _1604 + 1;
        if (_1605 > MAXINT){
            _1605 = NewDouble((double)_1605);
        }
    }
    else
    _1605 = binary_op(PLUS, 1, _1604);
    _1604 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_3221);
    _1606 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_t1_3221);
    _1607 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_t1_3221);
    _1608 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_t1_3221);
    _1609 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1603;
    *((int *)(_2+8)) = _1605;
    Ref(_1606);
    *((int *)(_2+12)) = _1606;
    Ref(_1607);
    *((int *)(_2+16)) = _1607;
    Ref(_1608);
    *((int *)(_2+20)) = _1608;
    Ref(_1609);
    *((int *)(_2+24)) = _1609;
    _1610 = MAKE_SEQ(_1);
    _1609 = NOVALUE;
    _1608 = NOVALUE;
    _1607 = NOVALUE;
    _1606 = NOVALUE;
    _1605 = NOVALUE;
    _1603 = NOVALUE;
    DeRefDS(_t1_3221);
    return _1610;
    ;
}


int _9new(int _year_3235, int _month_3236, int _day_3237, int _hour_3238, int _minute_3239, int _second_3240)
{
    int _d_3241 = NOVALUE;
    int _now_1__tmp_at51_3248 = NOVALUE;
    int _now_inlined_now_at_51_3247 = NOVALUE;
    int _1613 = NOVALUE;
    int _1612 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_year_3235)) {
        _1 = (long)(DBL_PTR(_year_3235)->dbl);
        if (UNIQUE(DBL_PTR(_year_3235)) && (DBL_PTR(_year_3235)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_year_3235);
        _year_3235 = _1;
    }
    if (!IS_ATOM_INT(_month_3236)) {
        _1 = (long)(DBL_PTR(_month_3236)->dbl);
        if (UNIQUE(DBL_PTR(_month_3236)) && (DBL_PTR(_month_3236)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_3236);
        _month_3236 = _1;
    }
    if (!IS_ATOM_INT(_day_3237)) {
        _1 = (long)(DBL_PTR(_day_3237)->dbl);
        if (UNIQUE(DBL_PTR(_day_3237)) && (DBL_PTR(_day_3237)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_day_3237);
        _day_3237 = _1;
    }
    if (!IS_ATOM_INT(_hour_3238)) {
        _1 = (long)(DBL_PTR(_hour_3238)->dbl);
        if (UNIQUE(DBL_PTR(_hour_3238)) && (DBL_PTR(_hour_3238)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hour_3238);
        _hour_3238 = _1;
    }
    if (!IS_ATOM_INT(_minute_3239)) {
        _1 = (long)(DBL_PTR(_minute_3239)->dbl);
        if (UNIQUE(DBL_PTR(_minute_3239)) && (DBL_PTR(_minute_3239)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minute_3239);
        _minute_3239 = _1;
    }

    /** 	d = {year, month, day, hour, minute, second}*/
    _0 = _d_3241;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _year_3235;
    *((int *)(_2+8)) = _month_3236;
    *((int *)(_2+12)) = _day_3237;
    *((int *)(_2+16)) = _hour_3238;
    *((int *)(_2+20)) = _minute_3239;
    Ref(_second_3240);
    *((int *)(_2+24)) = _second_3240;
    _d_3241 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	if equal(d, {0,0,0,0,0,0}) then*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    *((int *)(_2+24)) = 0;
    _1612 = MAKE_SEQ(_1);
    if (_d_3241 == _1612)
    _1613 = 1;
    else if (IS_ATOM_INT(_d_3241) && IS_ATOM_INT(_1612))
    _1613 = 0;
    else
    _1613 = (compare(_d_3241, _1612) == 0);
    DeRefDS(_1612);
    _1612 = NOVALUE;
    if (_1613 == 0)
    {
        _1613 = NOVALUE;
        goto L1; // [47] 70
    }
    else{
        _1613 = NOVALUE;
    }

    /** 		return now()*/

    /** 	return from_date(date())*/
    DeRefi(_now_1__tmp_at51_3248);
    _now_1__tmp_at51_3248 = Date();
    RefDS(_now_1__tmp_at51_3248);
    _0 = _now_inlined_now_at_51_3247;
    _now_inlined_now_at_51_3247 = _9from_date(_now_1__tmp_at51_3248);
    DeRef(_0);
    DeRefi(_now_1__tmp_at51_3248);
    _now_1__tmp_at51_3248 = NOVALUE;
    DeRef(_second_3240);
    DeRef(_d_3241);
    return _now_inlined_now_at_51_3247;
    goto L2; // [67] 77
L1: 

    /** 		return d*/
    DeRef(_second_3240);
    return _d_3241;
L2: 
    ;
}


int _9format(int _d_3301, int _pattern_3302)
{
    int _in_fmt_3304 = NOVALUE;
    int _ch_3305 = NOVALUE;
    int _tmp_3306 = NOVALUE;
    int _res_3307 = NOVALUE;
    int _weeks_day_4__tmp_at73_3323 = NOVALUE;
    int _weeks_day_3__tmp_at73_3322 = NOVALUE;
    int _weeks_day_2__tmp_at73_3321 = NOVALUE;
    int _weeks_day_1__tmp_at73_3320 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_73_3319 = NOVALUE;
    int _weeks_day_4__tmp_at119_3333 = NOVALUE;
    int _weeks_day_3__tmp_at119_3332 = NOVALUE;
    int _weeks_day_2__tmp_at119_3331 = NOVALUE;
    int _weeks_day_1__tmp_at119_3330 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_119_3329 = NOVALUE;
    int _to_unix_1__tmp_at626_3440 = NOVALUE;
    int _to_unix_inlined_to_unix_at_626_3439 = NOVALUE;
    int _weeks_day_4__tmp_at683_3457 = NOVALUE;
    int _weeks_day_3__tmp_at683_3456 = NOVALUE;
    int _weeks_day_2__tmp_at683_3455 = NOVALUE;
    int _weeks_day_1__tmp_at683_3454 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_683_3453 = NOVALUE;
    int _weeks_day_4__tmp_at729_3468 = NOVALUE;
    int _weeks_day_3__tmp_at729_3467 = NOVALUE;
    int _weeks_day_2__tmp_at729_3466 = NOVALUE;
    int _weeks_day_1__tmp_at729_3465 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_729_3464 = NOVALUE;
    int _weeks_day_4__tmp_at778_3480 = NOVALUE;
    int _weeks_day_3__tmp_at778_3479 = NOVALUE;
    int _weeks_day_2__tmp_at778_3478 = NOVALUE;
    int _weeks_day_1__tmp_at778_3477 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_778_3476 = NOVALUE;
    int _1761 = NOVALUE;
    int _1760 = NOVALUE;
    int _1755 = NOVALUE;
    int _1754 = NOVALUE;
    int _1753 = NOVALUE;
    int _1752 = NOVALUE;
    int _1750 = NOVALUE;
    int _1746 = NOVALUE;
    int _1745 = NOVALUE;
    int _1741 = NOVALUE;
    int _1740 = NOVALUE;
    int _1733 = NOVALUE;
    int _1732 = NOVALUE;
    int _1728 = NOVALUE;
    int _1724 = NOVALUE;
    int _1723 = NOVALUE;
    int _1721 = NOVALUE;
    int _1720 = NOVALUE;
    int _1718 = NOVALUE;
    int _1714 = NOVALUE;
    int _1712 = NOVALUE;
    int _1710 = NOVALUE;
    int _1706 = NOVALUE;
    int _1705 = NOVALUE;
    int _1701 = NOVALUE;
    int _1700 = NOVALUE;
    int _1696 = NOVALUE;
    int _1688 = NOVALUE;
    int _1687 = NOVALUE;
    int _1683 = NOVALUE;
    int _1682 = NOVALUE;
    int _1677 = NOVALUE;
    int _1669 = NOVALUE;
    int _1668 = NOVALUE;
    int _1665 = NOVALUE;
    int _1664 = NOVALUE;
    int _1661 = NOVALUE;
    int _1660 = NOVALUE;
    int _1659 = NOVALUE;
    int _1655 = NOVALUE;
    int _1654 = NOVALUE;
    int _1651 = NOVALUE;
    int _1650 = NOVALUE;
    int _1646 = NOVALUE;
    int _1643 = NOVALUE;
    int _1638 = NOVALUE;
    int _0, _1, _2;
    

    /** 	in_fmt = 0*/
    _in_fmt_3304 = 0;

    /** 	res = ""*/
    RefDS(_5);
    DeRef(_res_3307);
    _res_3307 = _5;

    /** 	for i = 1 to length(pattern) do*/
    if (IS_SEQUENCE(_pattern_3302)){
            _1638 = SEQ_PTR(_pattern_3302)->length;
    }
    else {
        _1638 = 1;
    }
    {
        int _i_3309;
        _i_3309 = 1;
L1: 
        if (_i_3309 > _1638){
            goto L2; // [22] 927
        }

        /** 		ch = pattern[i]*/
        _2 = (int)SEQ_PTR(_pattern_3302);
        _ch_3305 = (int)*(((s1_ptr)_2)->base + _i_3309);

        /** 		if in_fmt then*/
        if (_in_fmt_3304 == 0)
        {
            goto L3; // [39] 897
        }
        else{
        }

        /** 			in_fmt = 0*/
        _in_fmt_3304 = 0;

        /** 			if ch = '%' then*/
        if (_ch_3305 != 37)
        goto L4; // [51] 64

        /** 				res &= '%'*/
        Append(&_res_3307, _res_3307, 37);
        goto L5; // [61] 920
L4: 

        /** 			elsif ch = 'a' then*/
        if (_ch_3305 != 97)
        goto L6; // [66] 110

        /** 				res &= day_abbrs[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3301);
        _0 = _weeks_day_1__tmp_at73_3320;
        _weeks_day_1__tmp_at73_3320 = _9julianDay(_d_3301);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at73_3321);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at73_3320)) {
            _weeks_day_2__tmp_at73_3321 = _weeks_day_1__tmp_at73_3320 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at73_3321 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at73_3321 = NewDouble((double)_weeks_day_2__tmp_at73_3321);
            }
        }
        else {
            _weeks_day_2__tmp_at73_3321 = binary_op(MINUS, _weeks_day_1__tmp_at73_3320, 1);
        }
        DeRef(_weeks_day_3__tmp_at73_3322);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at73_3321)) {
            _weeks_day_3__tmp_at73_3322 = _weeks_day_2__tmp_at73_3321 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at73_3322 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at73_3322 = NewDouble((double)_weeks_day_3__tmp_at73_3322);
        }
        else {
            _weeks_day_3__tmp_at73_3322 = binary_op(PLUS, _weeks_day_2__tmp_at73_3321, 4094);
        }
        DeRef(_weeks_day_4__tmp_at73_3323);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at73_3322)) {
            _weeks_day_4__tmp_at73_3323 = (_weeks_day_3__tmp_at73_3322 % 7);
        }
        else {
            _weeks_day_4__tmp_at73_3323 = binary_op(REMAINDER, _weeks_day_3__tmp_at73_3322, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_73_3319);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at73_3323)) {
            _weeks_day_inlined_weeks_day_at_73_3319 = _weeks_day_4__tmp_at73_3323 + 1;
            if (_weeks_day_inlined_weeks_day_at_73_3319 > MAXINT){
                _weeks_day_inlined_weeks_day_at_73_3319 = NewDouble((double)_weeks_day_inlined_weeks_day_at_73_3319);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_73_3319 = binary_op(PLUS, 1, _weeks_day_4__tmp_at73_3323);
        DeRef(_weeks_day_1__tmp_at73_3320);
        _weeks_day_1__tmp_at73_3320 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at73_3321);
        _weeks_day_2__tmp_at73_3321 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at73_3322);
        _weeks_day_3__tmp_at73_3322 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at73_3323);
        _weeks_day_4__tmp_at73_3323 = NOVALUE;
        _2 = (int)SEQ_PTR(_9day_abbrs_3092);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_73_3319)){
            _1643 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_73_3319)->dbl));
        }
        else{
            _1643 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_73_3319);
        }
        Concat((object_ptr)&_res_3307, _res_3307, _1643);
        _1643 = NOVALUE;
        goto L5; // [107] 920
L6: 

        /** 			elsif ch = 'A' then*/
        if (_ch_3305 != 65)
        goto L7; // [112] 156

        /** 				res &= day_names[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3301);
        _0 = _weeks_day_1__tmp_at119_3330;
        _weeks_day_1__tmp_at119_3330 = _9julianDay(_d_3301);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at119_3331);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at119_3330)) {
            _weeks_day_2__tmp_at119_3331 = _weeks_day_1__tmp_at119_3330 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at119_3331 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at119_3331 = NewDouble((double)_weeks_day_2__tmp_at119_3331);
            }
        }
        else {
            _weeks_day_2__tmp_at119_3331 = binary_op(MINUS, _weeks_day_1__tmp_at119_3330, 1);
        }
        DeRef(_weeks_day_3__tmp_at119_3332);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at119_3331)) {
            _weeks_day_3__tmp_at119_3332 = _weeks_day_2__tmp_at119_3331 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at119_3332 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at119_3332 = NewDouble((double)_weeks_day_3__tmp_at119_3332);
        }
        else {
            _weeks_day_3__tmp_at119_3332 = binary_op(PLUS, _weeks_day_2__tmp_at119_3331, 4094);
        }
        DeRef(_weeks_day_4__tmp_at119_3333);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at119_3332)) {
            _weeks_day_4__tmp_at119_3333 = (_weeks_day_3__tmp_at119_3332 % 7);
        }
        else {
            _weeks_day_4__tmp_at119_3333 = binary_op(REMAINDER, _weeks_day_3__tmp_at119_3332, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_119_3329);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at119_3333)) {
            _weeks_day_inlined_weeks_day_at_119_3329 = _weeks_day_4__tmp_at119_3333 + 1;
            if (_weeks_day_inlined_weeks_day_at_119_3329 > MAXINT){
                _weeks_day_inlined_weeks_day_at_119_3329 = NewDouble((double)_weeks_day_inlined_weeks_day_at_119_3329);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_119_3329 = binary_op(PLUS, 1, _weeks_day_4__tmp_at119_3333);
        DeRef(_weeks_day_1__tmp_at119_3330);
        _weeks_day_1__tmp_at119_3330 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at119_3331);
        _weeks_day_2__tmp_at119_3331 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at119_3332);
        _weeks_day_3__tmp_at119_3332 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at119_3333);
        _weeks_day_4__tmp_at119_3333 = NOVALUE;
        _2 = (int)SEQ_PTR(_9day_names_3083);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_119_3329)){
            _1646 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_119_3329)->dbl));
        }
        else{
            _1646 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_119_3329);
        }
        Concat((object_ptr)&_res_3307, _res_3307, _1646);
        _1646 = NOVALUE;
        goto L5; // [153] 920
L7: 

        /** 			elsif ch = 'b' then*/
        if (_ch_3305 != 98)
        goto L8; // [158] 183

        /** 				res &= month_abbrs[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1650 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_9month_abbrs_3070);
        if (!IS_ATOM_INT(_1650)){
            _1651 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_1650)->dbl));
        }
        else{
            _1651 = (int)*(((s1_ptr)_2)->base + _1650);
        }
        Concat((object_ptr)&_res_3307, _res_3307, _1651);
        _1651 = NOVALUE;
        goto L5; // [180] 920
L8: 

        /** 			elsif ch = 'B' then*/
        if (_ch_3305 != 66)
        goto L9; // [185] 210

        /** 				res &= month_names[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1654 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_9month_names_3056);
        if (!IS_ATOM_INT(_1654)){
            _1655 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_1654)->dbl));
        }
        else{
            _1655 = (int)*(((s1_ptr)_2)->base + _1654);
        }
        Concat((object_ptr)&_res_3307, _res_3307, _1655);
        _1655 = NOVALUE;
        goto L5; // [207] 920
L9: 

        /** 			elsif ch = 'C' then*/
        if (_ch_3305 != 67)
        goto LA; // [212] 239

        /** 				res &= sprintf("%02d", d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1659 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_1659)) {
            _1660 = (_1659 % 100) ? NewDouble((double)_1659 / 100) : (_1659 / 100);
        }
        else {
            _1660 = binary_op(DIVIDE, _1659, 100);
        }
        _1659 = NOVALUE;
        _1661 = EPrintf(-9999999, _1658, _1660);
        DeRef(_1660);
        _1660 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1661);
        DeRefDS(_1661);
        _1661 = NOVALUE;
        goto L5; // [236] 920
LA: 

        /** 			elsif ch = 'd' then*/
        if (_ch_3305 != 100)
        goto LB; // [241] 264

        /** 				res &= sprintf("%02d", d[DAY])*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1664 = (int)*(((s1_ptr)_2)->base + 3);
        _1665 = EPrintf(-9999999, _1658, _1664);
        _1664 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1665);
        DeRefDS(_1665);
        _1665 = NOVALUE;
        goto L5; // [261] 920
LB: 

        /** 			elsif ch = 'H' then*/
        if (_ch_3305 != 72)
        goto LC; // [266] 289

        /** 				res &= sprintf("%02d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1668 = (int)*(((s1_ptr)_2)->base + 4);
        _1669 = EPrintf(-9999999, _1658, _1668);
        _1668 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1669);
        DeRefDS(_1669);
        _1669 = NOVALUE;
        goto L5; // [286] 920
LC: 

        /** 			elsif ch = 'I' then*/
        if (_ch_3305 != 73)
        goto LD; // [291] 352

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_3301);
        _tmp_3306 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_3306)){
            _tmp_3306 = (long)DBL_PTR(_tmp_3306)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_3306 <= 12)
        goto LE; // [309] 324

        /** 					tmp -= 12*/
        _tmp_3306 = _tmp_3306 - 12;
        goto LF; // [321] 339
LE: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_3306 != 0)
        goto L10; // [326] 338

        /** 					tmp = 12*/
        _tmp_3306 = 12;
L10: 
LF: 

        /** 				res &= sprintf("%02d", tmp)*/
        _1677 = EPrintf(-9999999, _1658, _tmp_3306);
        Concat((object_ptr)&_res_3307, _res_3307, _1677);
        DeRefDS(_1677);
        _1677 = NOVALUE;
        goto L5; // [349] 920
LD: 

        /** 			elsif ch = 'j' then*/
        if (_ch_3305 != 106)
        goto L11; // [354] 375

        /** 				res &= sprintf("%d", julianDayOfYear(d))*/
        Ref(_d_3301);
        _1682 = _9julianDayOfYear(_d_3301);
        _1683 = EPrintf(-9999999, _1681, _1682);
        DeRef(_1682);
        _1682 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1683);
        DeRefDS(_1683);
        _1683 = NOVALUE;
        goto L5; // [372] 920
L11: 

        /** 			elsif ch = 'k' then*/
        if (_ch_3305 != 107)
        goto L12; // [377] 400

        /** 				res &= sprintf("%d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1687 = (int)*(((s1_ptr)_2)->base + 4);
        _1688 = EPrintf(-9999999, _1681, _1687);
        _1687 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1688);
        DeRefDS(_1688);
        _1688 = NOVALUE;
        goto L5; // [397] 920
L12: 

        /** 			elsif ch = 'l' then*/
        if (_ch_3305 != 108)
        goto L13; // [402] 463

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_3301);
        _tmp_3306 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_3306)){
            _tmp_3306 = (long)DBL_PTR(_tmp_3306)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_3306 <= 12)
        goto L14; // [420] 435

        /** 					tmp -= 12*/
        _tmp_3306 = _tmp_3306 - 12;
        goto L15; // [432] 450
L14: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_3306 != 0)
        goto L16; // [437] 449

        /** 					tmp = 12*/
        _tmp_3306 = 12;
L16: 
L15: 

        /** 				res &= sprintf("%d", tmp)*/
        _1696 = EPrintf(-9999999, _1681, _tmp_3306);
        Concat((object_ptr)&_res_3307, _res_3307, _1696);
        DeRefDS(_1696);
        _1696 = NOVALUE;
        goto L5; // [460] 920
L13: 

        /** 			elsif ch = 'm' then*/
        if (_ch_3305 != 109)
        goto L17; // [465] 488

        /** 				res &= sprintf("%02d", d[MONTH])*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1700 = (int)*(((s1_ptr)_2)->base + 2);
        _1701 = EPrintf(-9999999, _1658, _1700);
        _1700 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1701);
        DeRefDS(_1701);
        _1701 = NOVALUE;
        goto L5; // [485] 920
L17: 

        /** 			elsif ch = 'M' then*/
        if (_ch_3305 != 77)
        goto L18; // [490] 513

        /** 				res &= sprintf("%02d", d[MINUTE])*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1705 = (int)*(((s1_ptr)_2)->base + 5);
        _1706 = EPrintf(-9999999, _1658, _1705);
        _1705 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1706);
        DeRefDS(_1706);
        _1706 = NOVALUE;
        goto L5; // [510] 920
L18: 

        /** 			elsif ch = 'p' then*/
        if (_ch_3305 != 112)
        goto L19; // [515] 562

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1710 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _1710, 12)){
            _1710 = NOVALUE;
            goto L1A; // [527] 546
        }
        _1710 = NOVALUE;

        /** 					res &= ampm[1]*/
        _2 = (int)SEQ_PTR(_9ampm_3101);
        _1712 = (int)*(((s1_ptr)_2)->base + 1);
        Concat((object_ptr)&_res_3307, _res_3307, _1712);
        _1712 = NOVALUE;
        goto L5; // [543] 920
L1A: 

        /** 					res &= ampm[2]*/
        _2 = (int)SEQ_PTR(_9ampm_3101);
        _1714 = (int)*(((s1_ptr)_2)->base + 2);
        Concat((object_ptr)&_res_3307, _res_3307, _1714);
        _1714 = NOVALUE;
        goto L5; // [559] 920
L19: 

        /** 			elsif ch = 'P' then*/
        if (_ch_3305 != 80)
        goto L1B; // [564] 619

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1718 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _1718, 12)){
            _1718 = NOVALUE;
            goto L1C; // [576] 599
        }
        _1718 = NOVALUE;

        /** 					res &= tolower(ampm[1])*/
        _2 = (int)SEQ_PTR(_9ampm_3101);
        _1720 = (int)*(((s1_ptr)_2)->base + 1);
        RefDS(_1720);
        _1721 = _9tolower(_1720);
        _1720 = NOVALUE;
        if (IS_SEQUENCE(_res_3307) && IS_ATOM(_1721)) {
            Ref(_1721);
            Append(&_res_3307, _res_3307, _1721);
        }
        else if (IS_ATOM(_res_3307) && IS_SEQUENCE(_1721)) {
        }
        else {
            Concat((object_ptr)&_res_3307, _res_3307, _1721);
        }
        DeRef(_1721);
        _1721 = NOVALUE;
        goto L5; // [596] 920
L1C: 

        /** 					res &= tolower(ampm[2])*/
        _2 = (int)SEQ_PTR(_9ampm_3101);
        _1723 = (int)*(((s1_ptr)_2)->base + 2);
        RefDS(_1723);
        _1724 = _9tolower(_1723);
        _1723 = NOVALUE;
        if (IS_SEQUENCE(_res_3307) && IS_ATOM(_1724)) {
            Ref(_1724);
            Append(&_res_3307, _res_3307, _1724);
        }
        else if (IS_ATOM(_res_3307) && IS_SEQUENCE(_1724)) {
        }
        else {
            Concat((object_ptr)&_res_3307, _res_3307, _1724);
        }
        DeRef(_1724);
        _1724 = NOVALUE;
        goto L5; // [616] 920
L1B: 

        /** 			elsif ch = 's' then*/
        if (_ch_3305 != 115)
        goto L1D; // [621] 651

        /** 				res &= sprintf("%d", to_unix(d))*/

        /** 	return datetimeToSeconds(dt) - EPOCH_1970*/
        Ref(_d_3301);
        _0 = _to_unix_1__tmp_at626_3440;
        _to_unix_1__tmp_at626_3440 = _9datetimeToSeconds(_d_3301);
        DeRef(_0);
        DeRef(_to_unix_inlined_to_unix_at_626_3439);
        _to_unix_inlined_to_unix_at_626_3439 = binary_op(MINUS, _to_unix_1__tmp_at626_3440, _9EPOCH_1970_2850);
        DeRef(_to_unix_1__tmp_at626_3440);
        _to_unix_1__tmp_at626_3440 = NOVALUE;
        _1728 = EPrintf(-9999999, _1681, _to_unix_inlined_to_unix_at_626_3439);
        Concat((object_ptr)&_res_3307, _res_3307, _1728);
        DeRefDS(_1728);
        _1728 = NOVALUE;
        goto L5; // [648] 920
L1D: 

        /** 			elsif ch = 'S' then*/
        if (_ch_3305 != 83)
        goto L1E; // [653] 676

        /** 				res &= sprintf("%02d", d[SECOND])*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1732 = (int)*(((s1_ptr)_2)->base + 6);
        _1733 = EPrintf(-9999999, _1658, _1732);
        _1732 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1733);
        DeRefDS(_1733);
        _1733 = NOVALUE;
        goto L5; // [673] 920
L1E: 

        /** 			elsif ch = 'u' then*/
        if (_ch_3305 != 117)
        goto L1F; // [678] 771

        /** 				tmp = weeks_day(d)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3301);
        _0 = _weeks_day_1__tmp_at683_3454;
        _weeks_day_1__tmp_at683_3454 = _9julianDay(_d_3301);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at683_3455);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at683_3454)) {
            _weeks_day_2__tmp_at683_3455 = _weeks_day_1__tmp_at683_3454 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at683_3455 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at683_3455 = NewDouble((double)_weeks_day_2__tmp_at683_3455);
            }
        }
        else {
            _weeks_day_2__tmp_at683_3455 = binary_op(MINUS, _weeks_day_1__tmp_at683_3454, 1);
        }
        DeRef(_weeks_day_3__tmp_at683_3456);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at683_3455)) {
            _weeks_day_3__tmp_at683_3456 = _weeks_day_2__tmp_at683_3455 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at683_3456 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at683_3456 = NewDouble((double)_weeks_day_3__tmp_at683_3456);
        }
        else {
            _weeks_day_3__tmp_at683_3456 = binary_op(PLUS, _weeks_day_2__tmp_at683_3455, 4094);
        }
        DeRef(_weeks_day_4__tmp_at683_3457);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at683_3456)) {
            _weeks_day_4__tmp_at683_3457 = (_weeks_day_3__tmp_at683_3456 % 7);
        }
        else {
            _weeks_day_4__tmp_at683_3457 = binary_op(REMAINDER, _weeks_day_3__tmp_at683_3456, 7);
        }
        if (IS_ATOM_INT(_weeks_day_4__tmp_at683_3457)) {
            _tmp_3306 = _weeks_day_4__tmp_at683_3457 + 1;
        }
        else
        { // coercing _tmp_3306 to an integer 1
            _tmp_3306 = binary_op(PLUS, 1, _weeks_day_4__tmp_at683_3457);
            if( !IS_ATOM_INT(_tmp_3306) ){
                _tmp_3306 = (object)DBL_PTR(_tmp_3306)->dbl;
            }
        }
        DeRef(_weeks_day_1__tmp_at683_3454);
        _weeks_day_1__tmp_at683_3454 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at683_3455);
        _weeks_day_2__tmp_at683_3455 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at683_3456);
        _weeks_day_3__tmp_at683_3456 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at683_3457);
        _weeks_day_4__tmp_at683_3457 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_3306)) {
            _1 = (long)(DBL_PTR(_tmp_3306)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_3306)) && (DBL_PTR(_tmp_3306)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_3306);
            _tmp_3306 = _1;
        }

        /** 				if tmp = 1 then*/
        if (_tmp_3306 != 1)
        goto L20; // [715] 728

        /** 					res &= "7" -- Sunday*/
        Concat((object_ptr)&_res_3307, _res_3307, _1738);
        goto L5; // [725] 920
L20: 

        /** 					res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3301);
        _0 = _weeks_day_1__tmp_at729_3465;
        _weeks_day_1__tmp_at729_3465 = _9julianDay(_d_3301);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at729_3466);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at729_3465)) {
            _weeks_day_2__tmp_at729_3466 = _weeks_day_1__tmp_at729_3465 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at729_3466 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at729_3466 = NewDouble((double)_weeks_day_2__tmp_at729_3466);
            }
        }
        else {
            _weeks_day_2__tmp_at729_3466 = binary_op(MINUS, _weeks_day_1__tmp_at729_3465, 1);
        }
        DeRef(_weeks_day_3__tmp_at729_3467);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at729_3466)) {
            _weeks_day_3__tmp_at729_3467 = _weeks_day_2__tmp_at729_3466 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at729_3467 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at729_3467 = NewDouble((double)_weeks_day_3__tmp_at729_3467);
        }
        else {
            _weeks_day_3__tmp_at729_3467 = binary_op(PLUS, _weeks_day_2__tmp_at729_3466, 4094);
        }
        DeRef(_weeks_day_4__tmp_at729_3468);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at729_3467)) {
            _weeks_day_4__tmp_at729_3468 = (_weeks_day_3__tmp_at729_3467 % 7);
        }
        else {
            _weeks_day_4__tmp_at729_3468 = binary_op(REMAINDER, _weeks_day_3__tmp_at729_3467, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_729_3464);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at729_3468)) {
            _weeks_day_inlined_weeks_day_at_729_3464 = _weeks_day_4__tmp_at729_3468 + 1;
            if (_weeks_day_inlined_weeks_day_at_729_3464 > MAXINT){
                _weeks_day_inlined_weeks_day_at_729_3464 = NewDouble((double)_weeks_day_inlined_weeks_day_at_729_3464);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_729_3464 = binary_op(PLUS, 1, _weeks_day_4__tmp_at729_3468);
        DeRef(_weeks_day_1__tmp_at729_3465);
        _weeks_day_1__tmp_at729_3465 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at729_3466);
        _weeks_day_2__tmp_at729_3466 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at729_3467);
        _weeks_day_3__tmp_at729_3467 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at729_3468);
        _weeks_day_4__tmp_at729_3468 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_729_3464)) {
            _1740 = _weeks_day_inlined_weeks_day_at_729_3464 - 1;
            if ((long)((unsigned long)_1740 +(unsigned long) HIGH_BITS) >= 0){
                _1740 = NewDouble((double)_1740);
            }
        }
        else {
            _1740 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_729_3464, 1);
        }
        _1741 = EPrintf(-9999999, _1681, _1740);
        DeRef(_1740);
        _1740 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1741);
        DeRefDS(_1741);
        _1741 = NOVALUE;
        goto L5; // [768] 920
L1F: 

        /** 			elsif ch = 'w' then*/
        if (_ch_3305 != 119)
        goto L21; // [773] 819

        /** 				res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_3301);
        _0 = _weeks_day_1__tmp_at778_3477;
        _weeks_day_1__tmp_at778_3477 = _9julianDay(_d_3301);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at778_3478);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at778_3477)) {
            _weeks_day_2__tmp_at778_3478 = _weeks_day_1__tmp_at778_3477 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at778_3478 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at778_3478 = NewDouble((double)_weeks_day_2__tmp_at778_3478);
            }
        }
        else {
            _weeks_day_2__tmp_at778_3478 = binary_op(MINUS, _weeks_day_1__tmp_at778_3477, 1);
        }
        DeRef(_weeks_day_3__tmp_at778_3479);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at778_3478)) {
            _weeks_day_3__tmp_at778_3479 = _weeks_day_2__tmp_at778_3478 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at778_3479 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at778_3479 = NewDouble((double)_weeks_day_3__tmp_at778_3479);
        }
        else {
            _weeks_day_3__tmp_at778_3479 = binary_op(PLUS, _weeks_day_2__tmp_at778_3478, 4094);
        }
        DeRef(_weeks_day_4__tmp_at778_3480);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at778_3479)) {
            _weeks_day_4__tmp_at778_3480 = (_weeks_day_3__tmp_at778_3479 % 7);
        }
        else {
            _weeks_day_4__tmp_at778_3480 = binary_op(REMAINDER, _weeks_day_3__tmp_at778_3479, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_778_3476);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at778_3480)) {
            _weeks_day_inlined_weeks_day_at_778_3476 = _weeks_day_4__tmp_at778_3480 + 1;
            if (_weeks_day_inlined_weeks_day_at_778_3476 > MAXINT){
                _weeks_day_inlined_weeks_day_at_778_3476 = NewDouble((double)_weeks_day_inlined_weeks_day_at_778_3476);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_778_3476 = binary_op(PLUS, 1, _weeks_day_4__tmp_at778_3480);
        DeRef(_weeks_day_1__tmp_at778_3477);
        _weeks_day_1__tmp_at778_3477 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at778_3478);
        _weeks_day_2__tmp_at778_3478 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at778_3479);
        _weeks_day_3__tmp_at778_3479 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at778_3480);
        _weeks_day_4__tmp_at778_3480 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_778_3476)) {
            _1745 = _weeks_day_inlined_weeks_day_at_778_3476 - 1;
            if ((long)((unsigned long)_1745 +(unsigned long) HIGH_BITS) >= 0){
                _1745 = NewDouble((double)_1745);
            }
        }
        else {
            _1745 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_778_3476, 1);
        }
        _1746 = EPrintf(-9999999, _1681, _1745);
        DeRef(_1745);
        _1745 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1746);
        DeRefDS(_1746);
        _1746 = NOVALUE;
        goto L5; // [816] 920
L21: 

        /** 			elsif ch = 'y' then*/
        if (_ch_3305 != 121)
        goto L22; // [821] 868

        /** 			   tmp = floor(d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1750 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_1750)) {
            if (100 > 0 && _1750 >= 0) {
                _tmp_3306 = _1750 / 100;
            }
            else {
                temp_dbl = floor((double)_1750 / (double)100);
                _tmp_3306 = (long)temp_dbl;
            }
        }
        else {
            _2 = binary_op(DIVIDE, _1750, 100);
            _tmp_3306 = unary_op(FLOOR, _2);
            DeRef(_2);
        }
        _1750 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_3306)) {
            _1 = (long)(DBL_PTR(_tmp_3306)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_3306)) && (DBL_PTR(_tmp_3306)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_3306);
            _tmp_3306 = _1;
        }

        /** 			   res &= sprintf("%02d", d[YEAR] - (tmp * 100))*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1752 = (int)*(((s1_ptr)_2)->base + 1);
        if (_tmp_3306 == (short)_tmp_3306)
        _1753 = _tmp_3306 * 100;
        else
        _1753 = NewDouble(_tmp_3306 * (double)100);
        if (IS_ATOM_INT(_1752) && IS_ATOM_INT(_1753)) {
            _1754 = _1752 - _1753;
            if ((long)((unsigned long)_1754 +(unsigned long) HIGH_BITS) >= 0){
                _1754 = NewDouble((double)_1754);
            }
        }
        else {
            _1754 = binary_op(MINUS, _1752, _1753);
        }
        _1752 = NOVALUE;
        DeRef(_1753);
        _1753 = NOVALUE;
        _1755 = EPrintf(-9999999, _1658, _1754);
        DeRef(_1754);
        _1754 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1755);
        DeRefDS(_1755);
        _1755 = NOVALUE;
        goto L5; // [865] 920
L22: 

        /** 			elsif ch = 'Y' then*/
        if (_ch_3305 != 89)
        goto L5; // [870] 920

        /** 				res &= sprintf("%04d", d[YEAR])*/
        _2 = (int)SEQ_PTR(_d_3301);
        _1760 = (int)*(((s1_ptr)_2)->base + 1);
        _1761 = EPrintf(-9999999, _1759, _1760);
        _1760 = NOVALUE;
        Concat((object_ptr)&_res_3307, _res_3307, _1761);
        DeRefDS(_1761);
        _1761 = NOVALUE;
        goto L5; // [890] 920
        goto L5; // [894] 920
L3: 

        /** 		elsif ch = '%' then*/
        if (_ch_3305 != 37)
        goto L23; // [899] 913

        /** 			in_fmt = 1*/
        _in_fmt_3304 = 1;
        goto L5; // [910] 920
L23: 

        /** 			res &= ch*/
        Append(&_res_3307, _res_3307, _ch_3305);
L5: 

        /** 	end for*/
        _i_3309 = _i_3309 + 1;
        goto L1; // [922] 29
L2: 
        ;
    }

    /** 	return res*/
    DeRef(_d_3301);
    DeRefDSi(_pattern_3302);
    _1650 = NOVALUE;
    _1654 = NOVALUE;
    return _res_3307;
    ;
}



// 0xC482139F
