CC     = gcc
CFLAGS =  -DEWINDOWS -fomit-frame-pointer -c -w -fsigned-char -O2 -m32 -Ic:/develop/offEu/euphoria -ffast-math
LINKER = gcc
LFLAGS = c:/develop/offEu/euphoria/source/build/eu.a -m32 
EUCOVERAGE_SOURCES = init-.c eucoverage.c main-.c eds.c types.c convert.c search.c text.c filesys.c datetime.c dll.c machine.c memconst.c memory.c get.c io.c math.c rand.c sequence.c sort.c pretty.c serialize.c cmdline.c console.c map.c eumem.c primes.c stats.c info.c url.c regex.c flags.c
EUCOVERAGE_OBJECTS = init-.o eucoverage.o main-.o eds.o types.o convert.o search.o text.o filesys.o datetime.o dll.o machine.o memconst.o memory.o get.o io.o math.o rand.o sequence.o sort.o pretty.o serialize.o cmdline.o console.o map.o eumem.o primes.o stats.o info.o url.o regex.o flags.o
EUCOVERAGE_GENERATED_FILES =  init-.c init-.o main-.h eucoverage.c eucoverage.o main-.c main-.o eds.c eds.o types.c types.o convert.c convert.o search.c search.o text.c text.o filesys.c filesys.o datetime.c datetime.o dll.c dll.o machine.c machine.o memconst.c memconst.o memory.c memory.o get.c get.o io.c io.o math.c math.o rand.c rand.o sequence.c sequence.o sort.c sort.o pretty.c pretty.o serialize.c serialize.o cmdline.c cmdline.o console.c console.o map.c map.o eumem.c eumem.o primes.c primes.o stats.c stats.o info.c info.o url.c url.o regex.c regex.o flags.c flags.o

c:/develop/offEu/euphoria/source/build/eucoverage: $(EUCOVERAGE_OBJECTS) c:/develop/offEu/euphoria/source/build/eu.a 
	$(LINKER) -o c:/develop/offEu/euphoria/source/build/eucoverage $(EUCOVERAGE_OBJECTS)  $(LFLAGS)

.PHONY: eucoverage-clean eucoverage-clean-all

eucoverage-clean:
	rm -rf $(EUCOVERAGE_OBJECTS) 

eucoverage-clean-all: eucoverage-clean
	rm -rf $(EUCOVERAGE_SOURCES)  c:/develop/offEu/euphoria/source/build/eucoverage

%.o: %.c
	$(CC) $(CFLAGS) $*.c -o $*.o

