// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _15get_bytes(int _fn_1796, int _n_1797)
{
    int _s_1798 = NOVALUE;
    int _c_1799 = NOVALUE;
    int _first_1800 = NOVALUE;
    int _last_1801 = NOVALUE;
    int _778 = NOVALUE;
    int _775 = NOVALUE;
    int _773 = NOVALUE;
    int _772 = NOVALUE;
    int _771 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_n_1797)) {
        _1 = (long)(DBL_PTR(_n_1797)->dbl);
        if (UNIQUE(DBL_PTR(_n_1797)) && (DBL_PTR(_n_1797)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_1797);
        _n_1797 = _1;
    }

    /** 	if n = 0 then*/
    if (_n_1797 != 0)
    goto L1; // [11] 22

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_1798);
    return _5;
L1: 

    /** 	c = getc(fn)*/
    if (_fn_1796 != last_r_file_no) {
        last_r_file_ptr = which_file(_fn_1796, EF_READ);
        last_r_file_no = _fn_1796;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_1799 = getKBchar();
        }
        else
        _c_1799 = getc(last_r_file_ptr);
    }
    else
    _c_1799 = getc(last_r_file_ptr);

    /** 	if c = EOF then*/
    if (_c_1799 != -1)
    goto L2; // [31] 42

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_1798);
    return _5;
L2: 

    /** 	s = repeat(c, n)*/
    DeRefi(_s_1798);
    _s_1798 = Repeat(_c_1799, _n_1797);

    /** 	last = 1*/
    _last_1801 = 1;

    /** 	while last < n do*/
L3: 
    if (_last_1801 >= _n_1797)
    goto L4; // [60] 175

    /** 		first = last+1*/
    _first_1800 = _last_1801 + 1;

    /** 		last  = last+CHUNK*/
    _last_1801 = _last_1801 + 100;

    /** 		if last > n then*/
    if (_last_1801 <= _n_1797)
    goto L5; // [82] 94

    /** 			last = n*/
    _last_1801 = _n_1797;
L5: 

    /** 		for i = first to last do*/
    _771 = _last_1801;
    {
        int _i_1815;
        _i_1815 = _first_1800;
L6: 
        if (_i_1815 > _771){
            goto L7; // [99] 122
        }

        /** 			s[i] = getc(fn)*/
        if (_fn_1796 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_1796, EF_READ);
            last_r_file_no = _fn_1796;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _772 = getKBchar();
            }
            else
            _772 = getc(last_r_file_ptr);
        }
        else
        _772 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_s_1798);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_1798 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1815);
        *(int *)_2 = _772;
        if( _1 != _772 ){
        }
        _772 = NOVALUE;

        /** 		end for*/
        _i_1815 = _i_1815 + 1;
        goto L6; // [117] 106
L7: 
        ;
    }

    /** 		if s[last] = EOF then*/
    _2 = (int)SEQ_PTR(_s_1798);
    _773 = (int)*(((s1_ptr)_2)->base + _last_1801);
    if (_773 != -1)
    goto L3; // [128] 60

    /** 			while s[last] = EOF do*/
L8: 
    _2 = (int)SEQ_PTR(_s_1798);
    _775 = (int)*(((s1_ptr)_2)->base + _last_1801);
    if (_775 != -1)
    goto L9; // [141] 158

    /** 				last -= 1*/
    _last_1801 = _last_1801 - 1;

    /** 			end while*/
    goto L8; // [155] 137
L9: 

    /** 			return s[1..last]*/
    rhs_slice_target = (object_ptr)&_778;
    RHS_Slice(_s_1798, 1, _last_1801);
    DeRefDSi(_s_1798);
    _773 = NOVALUE;
    _775 = NOVALUE;
    return _778;

    /** 	end while*/
    goto L3; // [172] 60
L4: 

    /** 	return s*/
    _773 = NOVALUE;
    _775 = NOVALUE;
    DeRef(_778);
    _778 = NOVALUE;
    return _s_1798;
    ;
}


int _15seek(int _fn_1939, int _pos_1940)
{
    int _838 = NOVALUE;
    int _837 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_1940);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_1939;
    ((int *)_2)[2] = _pos_1940;
    _837 = MAKE_SEQ(_1);
    _838 = machine(19, _837);
    DeRefDS(_837);
    _837 = NOVALUE;
    DeRef(_pos_1940);
    return _838;
    ;
}


int _15read_lines(int _file_1964)
{
    int _fn_1965 = NOVALUE;
    int _ret_1966 = NOVALUE;
    int _y_1967 = NOVALUE;
    int _864 = NOVALUE;
    int _863 = NOVALUE;
    int _862 = NOVALUE;
    int _861 = NOVALUE;
    int _855 = NOVALUE;
    int _854 = NOVALUE;
    int _851 = NOVALUE;
    int _850 = NOVALUE;
    int _849 = NOVALUE;
    int _844 = NOVALUE;
    int _843 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _843 = 1;
    if (_843 == 0)
    {
        _843 = NOVALUE;
        goto L1; // [6] 37
    }
    else{
        _843 = NOVALUE;
    }

    /** 		if length(file) = 0 then*/
    if (IS_SEQUENCE(_file_1964)){
            _844 = SEQ_PTR(_file_1964)->length;
    }
    else {
        _844 = 1;
    }
    if (_844 != 0)
    goto L2; // [14] 26

    /** 			fn = 0*/
    DeRef(_fn_1965);
    _fn_1965 = 0;
    goto L3; // [23] 43
L2: 

    /** 			fn = open(file, "r")*/
    DeRef(_fn_1965);
    _fn_1965 = EOpen(_file_1964, _846, 0);
    goto L3; // [34] 43
L1: 

    /** 		fn = file*/
    Ref(_file_1964);
    DeRef(_fn_1965);
    _fn_1965 = _file_1964;
L3: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_1965, 0)){
        goto L4; // [47] 56
    }
    DeRef(_file_1964);
    DeRef(_fn_1965);
    DeRef(_ret_1966);
    DeRefi(_y_1967);
    return -1;
L4: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_1966);
    _ret_1966 = _5;

    /** 	while sequence(y) with entry do*/
    goto L5; // [63] 125
L6: 
    _849 = IS_SEQUENCE(_y_1967);
    if (_849 == 0)
    {
        _849 = NOVALUE;
        goto L7; // [71] 135
    }
    else{
        _849 = NOVALUE;
    }

    /** 		if y[$] = '\n' then*/
    if (IS_SEQUENCE(_y_1967)){
            _850 = SEQ_PTR(_y_1967)->length;
    }
    else {
        _850 = 1;
    }
    _2 = (int)SEQ_PTR(_y_1967);
    _851 = (int)*(((s1_ptr)_2)->base + _850);
    if (_851 != 10)
    goto L8; // [83] 104

    /** 			y = y[1..$-1]*/
    if (IS_SEQUENCE(_y_1967)){
            _854 = SEQ_PTR(_y_1967)->length;
    }
    else {
        _854 = 1;
    }
    _855 = _854 - 1;
    _854 = NOVALUE;
    rhs_slice_target = (object_ptr)&_y_1967;
    RHS_Slice(_y_1967, 1, _855);

    /** 			ifdef UNIX then*/
L8: 

    /** 		ret = append(ret, y)*/
    Ref(_y_1967);
    Append(&_ret_1966, _ret_1966, _y_1967);

    /** 		if fn = 0 then*/
    if (binary_op_a(NOTEQ, _fn_1965, 0)){
        goto L9; // [112] 122
    }

    /** 			puts(2, '\n')*/
    EPuts(2, 10); // DJP 
L9: 

    /** 	entry*/
L5: 

    /** 		y = gets(fn)*/
    DeRefi(_y_1967);
    _y_1967 = EGets(_fn_1965);

    /** 	end while*/
    goto L6; // [132] 66
L7: 

    /** 	if sequence(file) and length(file) != 0 then*/
    _861 = IS_SEQUENCE(_file_1964);
    if (_861 == 0) {
        goto LA; // [140] 160
    }
    if (IS_SEQUENCE(_file_1964)){
            _863 = SEQ_PTR(_file_1964)->length;
    }
    else {
        _863 = 1;
    }
    _864 = (_863 != 0);
    _863 = NOVALUE;
    if (_864 == 0)
    {
        DeRef(_864);
        _864 = NOVALUE;
        goto LA; // [152] 160
    }
    else{
        DeRef(_864);
        _864 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_1965))
    EClose(_fn_1965);
    else
    EClose((int)DBL_PTR(_fn_1965)->dbl);
LA: 

    /** 	return ret*/
    DeRef(_file_1964);
    DeRef(_fn_1965);
    DeRefi(_y_1967);
    _851 = NOVALUE;
    DeRef(_855);
    _855 = NOVALUE;
    return _ret_1966;
    ;
}


int _15write_file(int _file_2137, int _data_2138, int _as_text_2139)
{
    int _fn_2140 = NOVALUE;
    int _952 = NOVALUE;
    int _946 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if as_text != BINARY_MODE then*/

    /** 	if sequence(file) then*/
    _946 = 1;
    if (_946 == 0)
    {
        _946 = NOVALUE;
        goto L1; // [163] 199
    }
    else{
        _946 = NOVALUE;
    }

    /** 		if as_text = TEXT_MODE then*/

    /** 			fn = open(file, "wb")*/
    _fn_2140 = EOpen(_file_2137, _949, 0);
    goto L2; // [196] 209
L1: 

    /** 		fn = file*/
    Ref(_file_2137);
    _fn_2140 = _file_2137;
    if (!IS_ATOM_INT(_fn_2140)) {
        _1 = (long)(DBL_PTR(_fn_2140)->dbl);
        if (UNIQUE(DBL_PTR(_fn_2140)) && (DBL_PTR(_fn_2140)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_2140);
        _fn_2140 = _1;
    }
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_2140 >= 0)
    goto L3; // [213] 222
    DeRefi(_file_2137);
    DeRefDS(_data_2138);
    return -1;
L3: 

    /** 	puts(fn, data)*/
    EPuts(_fn_2140, _data_2138); // DJP 

    /** 	if sequence(file) then*/
    _952 = IS_SEQUENCE(_file_2137);
    if (_952 == 0)
    {
        _952 = NOVALUE;
        goto L4; // [232] 240
    }
    else{
        _952 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_2140);
L4: 

    /** 	return 1*/
    DeRefi(_file_2137);
    DeRefDS(_data_2138);
    return 1;
    ;
}



// 0xC2D6DBA3
