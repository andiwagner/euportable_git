// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _35regex(int _o_18418)
{
    int _10356 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sequence(o)*/
    _10356 = IS_SEQUENCE(_o_18418);
    DeRef(_o_18418);
    return _10356;
    ;
}


int _35option_spec(int _o_18422)
{
    int _10363 = NOVALUE;
    int _10362 = NOVALUE;
    int _10360 = NOVALUE;
    int _10358 = NOVALUE;
    int _10357 = NOVALUE;
    int _0, _1, _2;
    
L1: 

    /** 	if atom(o) then*/
    _10357 = IS_ATOM(_o_18422);
    if (_10357 == 0)
    {
        _10357 = NOVALUE;
        goto L2; // [6] 60
    }
    else{
        _10357 = NOVALUE;
    }

    /** 		if not integer(o) then*/
    if (IS_ATOM_INT(_o_18422))
    _10358 = 1;
    else if (IS_ATOM_DBL(_o_18422))
    _10358 = IS_ATOM_INT(DoubleToInt(_o_18422));
    else
    _10358 = 0;
    if (_10358 != 0)
    goto L3; // [14] 26
    _10358 = NOVALUE;

    /** 			return 0*/
    DeRef(_o_18422);
    return 0;
    goto L4; // [23] 89
L3: 

    /** 			if (or_bits(o,all_options) != all_options) then*/
    if (IS_ATOM_INT(_o_18422) && IS_ATOM_INT(_35all_options_18413)) {
        {unsigned long tu;
             tu = (unsigned long)_o_18422 | (unsigned long)_35all_options_18413;
             _10360 = MAKE_UINT(tu);
        }
    }
    else {
        _10360 = binary_op(OR_BITS, _o_18422, _35all_options_18413);
    }
    if (binary_op_a(EQUALS, _10360, _35all_options_18413)){
        DeRef(_10360);
        _10360 = NOVALUE;
        goto L5; // [36] 49
    }
    DeRef(_10360);
    _10360 = NOVALUE;

    /** 				return 0*/
    DeRef(_o_18422);
    return 0;
    goto L4; // [46] 89
L5: 

    /** 				return 1*/
    DeRef(_o_18422);
    return 1;
    goto L4; // [57] 89
L2: 

    /** 	elsif integer_array(o) then*/
    Ref(_o_18422);
    _10362 = _3integer_array(_o_18422);
    if (_10362 == 0) {
        DeRef(_10362);
        _10362 = NOVALUE;
        goto L6; // [66] 82
    }
    else {
        if (!IS_ATOM_INT(_10362) && DBL_PTR(_10362)->dbl == 0.0){
            DeRef(_10362);
            _10362 = NOVALUE;
            goto L6; // [66] 82
        }
        DeRef(_10362);
        _10362 = NOVALUE;
    }
    DeRef(_10362);
    _10362 = NOVALUE;

    /** 		return option_spec( math:or_all(o) )*/
    Ref(_o_18422);
    _10363 = _17or_all(_o_18422);
    _0 = _o_18422;
    _o_18422 = _10363;
    Ref(_o_18422);
    DeRef(_0);
    DeRef(_10363);
    _10363 = NOVALUE;
    goto L1; // [75] 1
    goto L4; // [79] 89
L6: 

    /** 		return 0*/
    DeRef(_o_18422);
    return 0;
L4: 
    ;
}


int _35option_spec_to_string(int _o_18441)
{
    int _10365 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return flags:flags_to_string(o, option_names)*/
    Ref(_o_18441);
    RefDS(_35option_names_18264);
    _10365 = _36flags_to_string(_o_18441, _35option_names_18264, 0);
    DeRef(_o_18441);
    return _10365;
    ;
}


int _35error_to_string(int _i_18445)
{
    int _10372 = NOVALUE;
    int _10370 = NOVALUE;
    int _10369 = NOVALUE;
    int _10368 = NOVALUE;
    int _10366 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_i_18445)) {
        _1 = (long)(DBL_PTR(_i_18445)->dbl);
        if (UNIQUE(DBL_PTR(_i_18445)) && (DBL_PTR(_i_18445)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_18445);
        _i_18445 = _1;
    }

    /** 	if i >= 0 or i < -23 then*/
    _10366 = (_i_18445 >= 0);
    if (_10366 != 0) {
        goto L1; // [11] 24
    }
    _10368 = (_i_18445 < -23);
    if (_10368 == 0)
    {
        DeRef(_10368);
        _10368 = NOVALUE;
        goto L2; // [20] 41
    }
    else{
        DeRef(_10368);
        _10368 = NOVALUE;
    }
L1: 

    /** 		return sprintf("%d",{i})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _i_18445;
    _10369 = MAKE_SEQ(_1);
    _10370 = EPrintf(-9999999, _1681, _10369);
    DeRefDS(_10369);
    _10369 = NOVALUE;
    DeRef(_10366);
    _10366 = NOVALUE;
    return _10370;
    goto L3; // [38] 58
L2: 

    /** 		return search:vlookup(i, error_names, 1, 2, "Unknown Error")*/
    RefDS(_35error_names_18364);
    RefDS(_10371);
    _10372 = _5vlookup(_i_18445, _35error_names_18364, 1, 2, _10371);
    DeRef(_10366);
    _10366 = NOVALUE;
    DeRef(_10370);
    _10370 = NOVALUE;
    return _10372;
L3: 
    ;
}


int _35new(int _pattern_18458, int _options_18459)
{
    int _10376 = NOVALUE;
    int _10375 = NOVALUE;
    int _10373 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _10373 = IS_SEQUENCE(_options_18459);
    if (_10373 == 0)
    {
        _10373 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _10373 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18459);
    _0 = _options_18459;
    _options_18459 = _17or_all(_options_18459);
    DeRef(_0);
L1: 

    /** 	return machine_func(M_PCRE_COMPILE, { pattern, options })*/
    Ref(_options_18459);
    Ref(_pattern_18458);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pattern_18458;
    ((int *)_2)[2] = _options_18459;
    _10375 = MAKE_SEQ(_1);
    _10376 = machine(68, _10375);
    DeRefDS(_10375);
    _10375 = NOVALUE;
    DeRef(_pattern_18458);
    DeRef(_options_18459);
    return _10376;
    ;
}


int _35error_message(int _re_18467)
{
    int _10378 = NOVALUE;
    int _10377 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_PCRE_ERROR_MESSAGE, { re })*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_18467);
    *((int *)(_2+4)) = _re_18467;
    _10377 = MAKE_SEQ(_1);
    _10378 = machine(95, _10377);
    DeRefDS(_10377);
    _10377 = NOVALUE;
    DeRef(_re_18467);
    return _10378;
    ;
}


int _35escape(int _s_18473)
{
    int _10380 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return text:escape(s, ".\\+*?[^]$(){}=!<>|:-")*/
    Ref(_s_18473);
    RefDS(_10379);
    _10380 = _6escape(_s_18473, _10379);
    DeRef(_s_18473);
    return _10380;
    ;
}


int _35get_ovector_size(int _ex_18478, int _maxsize_18479)
{
    int _m_18480 = NOVALUE;
    int _10384 = NOVALUE;
    int _10381 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_maxsize_18479)) {
        _1 = (long)(DBL_PTR(_maxsize_18479)->dbl);
        if (UNIQUE(DBL_PTR(_maxsize_18479)) && (DBL_PTR(_maxsize_18479)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxsize_18479);
        _maxsize_18479 = _1;
    }

    /** 	integer m = machine_func(M_PCRE_GET_OVECTOR_SIZE, {ex})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_18478);
    *((int *)(_2+4)) = _ex_18478;
    _10381 = MAKE_SEQ(_1);
    _m_18480 = machine(97, _10381);
    DeRefDS(_10381);
    _10381 = NOVALUE;
    if (!IS_ATOM_INT(_m_18480)) {
        _1 = (long)(DBL_PTR(_m_18480)->dbl);
        if (UNIQUE(DBL_PTR(_m_18480)) && (DBL_PTR(_m_18480)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_m_18480);
        _m_18480 = _1;
    }

    /** 	if (m > maxsize) then*/
    if (_m_18480 <= _maxsize_18479)
    goto L1; // [23] 34

    /** 		return maxsize*/
    DeRef(_ex_18478);
    return _maxsize_18479;
L1: 

    /** 	return m+1*/
    _10384 = _m_18480 + 1;
    if (_10384 > MAXINT){
        _10384 = NewDouble((double)_10384);
    }
    DeRef(_ex_18478);
    return _10384;
    ;
}


int _35find(int _re_18488, int _haystack_18490, int _from_18491, int _options_18492, int _size_18493)
{
    int _10391 = NOVALUE;
    int _10390 = NOVALUE;
    int _10389 = NOVALUE;
    int _10386 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18491)) {
        _1 = (long)(DBL_PTR(_from_18491)->dbl);
        if (UNIQUE(DBL_PTR(_from_18491)) && (DBL_PTR(_from_18491)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18491);
        _from_18491 = _1;
    }
    if (!IS_ATOM_INT(_size_18493)) {
        _1 = (long)(DBL_PTR(_size_18493)->dbl);
        if (UNIQUE(DBL_PTR(_size_18493)) && (DBL_PTR(_size_18493)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_18493);
        _size_18493 = _1;
    }

    /** 	if sequence(options) then */
    _10386 = IS_SEQUENCE(_options_18492);
    if (_10386 == 0)
    {
        _10386 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _10386 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18492);
    _0 = _options_18492;
    _options_18492 = _17or_all(_options_18492);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_18493 >= 0)
    goto L2; // [26] 38

    /** 		size = 0*/
    _size_18493 = 0;
L2: 

    /** 	return machine_func(M_PCRE_EXEC, { re, haystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_18490)){
            _10389 = SEQ_PTR(_haystack_18490)->length;
    }
    else {
        _10389 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_18488);
    *((int *)(_2+4)) = _re_18488;
    Ref(_haystack_18490);
    *((int *)(_2+8)) = _haystack_18490;
    *((int *)(_2+12)) = _10389;
    Ref(_options_18492);
    *((int *)(_2+16)) = _options_18492;
    *((int *)(_2+20)) = _from_18491;
    *((int *)(_2+24)) = _size_18493;
    _10390 = MAKE_SEQ(_1);
    _10389 = NOVALUE;
    _10391 = machine(70, _10390);
    DeRefDS(_10390);
    _10390 = NOVALUE;
    DeRef(_re_18488);
    DeRef(_haystack_18490);
    DeRef(_options_18492);
    return _10391;
    ;
}


int _35find_all(int _re_18505, int _haystack_18507, int _from_18508, int _options_18509, int _size_18510)
{
    int _result_18517 = NOVALUE;
    int _results_18518 = NOVALUE;
    int _pHaystack_18519 = NOVALUE;
    int _10404 = NOVALUE;
    int _10403 = NOVALUE;
    int _10401 = NOVALUE;
    int _10399 = NOVALUE;
    int _10397 = NOVALUE;
    int _10393 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18508)) {
        _1 = (long)(DBL_PTR(_from_18508)->dbl);
        if (UNIQUE(DBL_PTR(_from_18508)) && (DBL_PTR(_from_18508)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18508);
        _from_18508 = _1;
    }
    if (!IS_ATOM_INT(_size_18510)) {
        _1 = (long)(DBL_PTR(_size_18510)->dbl);
        if (UNIQUE(DBL_PTR(_size_18510)) && (DBL_PTR(_size_18510)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_18510);
        _size_18510 = _1;
    }

    /** 	if sequence(options) then */
    _10393 = IS_SEQUENCE(_options_18509);
    if (_10393 == 0)
    {
        _10393 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _10393 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18509);
    _0 = _options_18509;
    _options_18509 = _17or_all(_options_18509);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_18510 >= 0)
    goto L2; // [26] 38

    /** 		size = 0*/
    _size_18510 = 0;
L2: 

    /** 	object result*/

    /** 	sequence results = {}*/
    RefDS(_5);
    DeRef(_results_18518);
    _results_18518 = _5;

    /** 	atom pHaystack = machine:allocate_string(haystack)*/
    Ref(_haystack_18507);
    _0 = _pHaystack_18519;
    _pHaystack_18519 = _11allocate_string(_haystack_18507, 0);
    DeRef(_0);

    /** 	while sequence(result) with entry do*/
    goto L3; // [56] 102
L4: 
    _10397 = IS_SEQUENCE(_result_18517);
    if (_10397 == 0)
    {
        _10397 = NOVALUE;
        goto L5; // [64] 127
    }
    else{
        _10397 = NOVALUE;
    }

    /** 		results = append(results, result)*/
    Ref(_result_18517);
    Append(&_results_18518, _results_18518, _result_18517);

    /** 		from = math:max(result) + 1*/
    Ref(_result_18517);
    _10399 = _17max(_result_18517);
    if (IS_ATOM_INT(_10399)) {
        _from_18508 = _10399 + 1;
    }
    else
    { // coercing _from_18508 to an integer 1
        _from_18508 = 1+(long)(DBL_PTR(_10399)->dbl);
        if( !IS_ATOM_INT(_from_18508) ){
            _from_18508 = (object)DBL_PTR(_from_18508)->dbl;
        }
    }
    DeRef(_10399);
    _10399 = NOVALUE;

    /** 		if from > length(haystack) then*/
    if (IS_SEQUENCE(_haystack_18507)){
            _10401 = SEQ_PTR(_haystack_18507)->length;
    }
    else {
        _10401 = 1;
    }
    if (_from_18508 <= _10401)
    goto L6; // [90] 99

    /** 			exit*/
    goto L5; // [96] 127
L6: 

    /** 	entry*/
L3: 

    /** 		result = machine_func(M_PCRE_EXEC, { re, pHaystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_18507)){
            _10403 = SEQ_PTR(_haystack_18507)->length;
    }
    else {
        _10403 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_18505);
    *((int *)(_2+4)) = _re_18505;
    Ref(_pHaystack_18519);
    *((int *)(_2+8)) = _pHaystack_18519;
    *((int *)(_2+12)) = _10403;
    Ref(_options_18509);
    *((int *)(_2+16)) = _options_18509;
    *((int *)(_2+20)) = _from_18508;
    *((int *)(_2+24)) = _size_18510;
    _10404 = MAKE_SEQ(_1);
    _10403 = NOVALUE;
    DeRef(_result_18517);
    _result_18517 = machine(70, _10404);
    DeRefDS(_10404);
    _10404 = NOVALUE;

    /** 	end while*/
    goto L4; // [124] 59
L5: 

    /** 	machine:free(pHaystack)*/
    Ref(_pHaystack_18519);
    _11free(_pHaystack_18519);

    /** 	return results*/
    DeRef(_re_18505);
    DeRef(_haystack_18507);
    DeRef(_options_18509);
    DeRef(_result_18517);
    DeRef(_pHaystack_18519);
    return _results_18518;
    ;
}


int _35has_match(int _re_18534, int _haystack_18536, int _from_18537, int _options_18538)
{
    int _10408 = NOVALUE;
    int _10407 = NOVALUE;
    int _10406 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18537)) {
        _1 = (long)(DBL_PTR(_from_18537)->dbl);
        if (UNIQUE(DBL_PTR(_from_18537)) && (DBL_PTR(_from_18537)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18537);
        _from_18537 = _1;
    }

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_re_18534);
    _10406 = _35get_ovector_size(_re_18534, 30);
    Ref(_re_18534);
    Ref(_haystack_18536);
    Ref(_options_18538);
    _10407 = _35find(_re_18534, _haystack_18536, _from_18537, _options_18538, _10406);
    _10406 = NOVALUE;
    _10408 = IS_SEQUENCE(_10407);
    DeRef(_10407);
    _10407 = NOVALUE;
    DeRef(_re_18534);
    DeRef(_haystack_18536);
    DeRef(_options_18538);
    return _10408;
    ;
}


int _35is_match(int _re_18544, int _haystack_18546, int _from_18547, int _options_18548)
{
    int _m_18549 = NOVALUE;
    int _10423 = NOVALUE;
    int _10422 = NOVALUE;
    int _10421 = NOVALUE;
    int _10420 = NOVALUE;
    int _10419 = NOVALUE;
    int _10418 = NOVALUE;
    int _10417 = NOVALUE;
    int _10416 = NOVALUE;
    int _10415 = NOVALUE;
    int _10414 = NOVALUE;
    int _10413 = NOVALUE;
    int _10412 = NOVALUE;
    int _10411 = NOVALUE;
    int _10409 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18547)) {
        _1 = (long)(DBL_PTR(_from_18547)->dbl);
        if (UNIQUE(DBL_PTR(_from_18547)) && (DBL_PTR(_from_18547)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18547);
        _from_18547 = _1;
    }

    /** 	object m = find(re, haystack, from, options)*/
    Ref(_re_18544);
    _10409 = _35get_ovector_size(_re_18544, 30);
    Ref(_re_18544);
    Ref(_haystack_18546);
    Ref(_options_18548);
    _0 = _m_18549;
    _m_18549 = _35find(_re_18544, _haystack_18546, _from_18547, _options_18548, _10409);
    DeRef(_0);
    _10409 = NOVALUE;

    /** 	if sequence(m) and length(m) > 0 and m[1][1] = from and m[1][2] = length(haystack) then*/
    _10411 = IS_SEQUENCE(_m_18549);
    if (_10411 == 0) {
        _10412 = 0;
        goto L1; // [25] 40
    }
    if (IS_SEQUENCE(_m_18549)){
            _10413 = SEQ_PTR(_m_18549)->length;
    }
    else {
        _10413 = 1;
    }
    _10414 = (_10413 > 0);
    _10413 = NOVALUE;
    _10412 = (_10414 != 0);
L1: 
    if (_10412 == 0) {
        _10415 = 0;
        goto L2; // [40] 60
    }
    _2 = (int)SEQ_PTR(_m_18549);
    _10416 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_10416);
    _10417 = (int)*(((s1_ptr)_2)->base + 1);
    _10416 = NOVALUE;
    if (IS_ATOM_INT(_10417)) {
        _10418 = (_10417 == _from_18547);
    }
    else {
        _10418 = binary_op(EQUALS, _10417, _from_18547);
    }
    _10417 = NOVALUE;
    if (IS_ATOM_INT(_10418))
    _10415 = (_10418 != 0);
    else
    _10415 = DBL_PTR(_10418)->dbl != 0.0;
L2: 
    if (_10415 == 0) {
        goto L3; // [60] 90
    }
    _2 = (int)SEQ_PTR(_m_18549);
    _10420 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_10420);
    _10421 = (int)*(((s1_ptr)_2)->base + 2);
    _10420 = NOVALUE;
    if (IS_SEQUENCE(_haystack_18546)){
            _10422 = SEQ_PTR(_haystack_18546)->length;
    }
    else {
        _10422 = 1;
    }
    if (IS_ATOM_INT(_10421)) {
        _10423 = (_10421 == _10422);
    }
    else {
        _10423 = binary_op(EQUALS, _10421, _10422);
    }
    _10421 = NOVALUE;
    _10422 = NOVALUE;
    if (_10423 == 0) {
        DeRef(_10423);
        _10423 = NOVALUE;
        goto L3; // [80] 90
    }
    else {
        if (!IS_ATOM_INT(_10423) && DBL_PTR(_10423)->dbl == 0.0){
            DeRef(_10423);
            _10423 = NOVALUE;
            goto L3; // [80] 90
        }
        DeRef(_10423);
        _10423 = NOVALUE;
    }
    DeRef(_10423);
    _10423 = NOVALUE;

    /** 		return 1*/
    DeRef(_re_18544);
    DeRef(_haystack_18546);
    DeRef(_options_18548);
    DeRef(_m_18549);
    DeRef(_10414);
    _10414 = NOVALUE;
    DeRef(_10418);
    _10418 = NOVALUE;
    return 1;
L3: 

    /** 	return 0*/
    DeRef(_re_18544);
    DeRef(_haystack_18546);
    DeRef(_options_18548);
    DeRef(_m_18549);
    DeRef(_10414);
    _10414 = NOVALUE;
    DeRef(_10418);
    _10418 = NOVALUE;
    return 0;
    ;
}


int _35matches(int _re_18568, int _haystack_18570, int _from_18571, int _options_18572)
{
    int _str_offsets_18576 = NOVALUE;
    int _match_data_18578 = NOVALUE;
    int _tmp_18588 = NOVALUE;
    int _10445 = NOVALUE;
    int _10444 = NOVALUE;
    int _10443 = NOVALUE;
    int _10442 = NOVALUE;
    int _10441 = NOVALUE;
    int _10439 = NOVALUE;
    int _10438 = NOVALUE;
    int _10437 = NOVALUE;
    int _10436 = NOVALUE;
    int _10434 = NOVALUE;
    int _10433 = NOVALUE;
    int _10432 = NOVALUE;
    int _10431 = NOVALUE;
    int _10429 = NOVALUE;
    int _10428 = NOVALUE;
    int _10427 = NOVALUE;
    int _10424 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18571)) {
        _1 = (long)(DBL_PTR(_from_18571)->dbl);
        if (UNIQUE(DBL_PTR(_from_18571)) && (DBL_PTR(_from_18571)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18571);
        _from_18571 = _1;
    }

    /** 	if sequence(options) then */
    _10424 = IS_SEQUENCE(_options_18572);
    if (_10424 == 0)
    {
        _10424 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _10424 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18572);
    _0 = _options_18572;
    _options_18572 = _17or_all(_options_18572);
    DeRef(_0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_18572)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_18572;
             _str_offsets_18576 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_18576 = binary_op(AND_BITS, 201326592, _options_18572);
    }
    if (!IS_ATOM_INT(_str_offsets_18576)) {
        _1 = (long)(DBL_PTR(_str_offsets_18576)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_18576)) && (DBL_PTR(_str_offsets_18576)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_18576);
        _str_offsets_18576 = _1;
    }

    /** 	object match_data = find(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _10427 = not_bits(201326592);
    if (IS_ATOM_INT(_options_18572) && IS_ATOM_INT(_10427)) {
        {unsigned long tu;
             tu = (unsigned long)_options_18572 & (unsigned long)_10427;
             _10428 = MAKE_UINT(tu);
        }
    }
    else {
        _10428 = binary_op(AND_BITS, _options_18572, _10427);
    }
    DeRef(_10427);
    _10427 = NOVALUE;
    Ref(_re_18568);
    _10429 = _35get_ovector_size(_re_18568, 30);
    Ref(_re_18568);
    Ref(_haystack_18570);
    _0 = _match_data_18578;
    _match_data_18578 = _35find(_re_18568, _haystack_18570, _from_18571, _10428, _10429);
    DeRef(_0);
    _10428 = NOVALUE;
    _10429 = NOVALUE;

    /** 	if atom(match_data) then */
    _10431 = IS_ATOM(_match_data_18578);
    if (_10431 == 0)
    {
        _10431 = NOVALUE;
        goto L2; // [57] 67
    }
    else{
        _10431 = NOVALUE;
    }

    /** 		return ERROR_NOMATCH */
    DeRef(_re_18568);
    DeRef(_haystack_18570);
    DeRef(_options_18572);
    DeRef(_match_data_18578);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_18578)){
            _10432 = SEQ_PTR(_match_data_18578)->length;
    }
    else {
        _10432 = 1;
    }
    {
        int _i_18586;
        _i_18586 = 1;
L3: 
        if (_i_18586 > _10432){
            goto L4; // [72] 185
        }

        /** 		sequence tmp*/

        /** 		if match_data[i][1] = 0 then*/
        _2 = (int)SEQ_PTR(_match_data_18578);
        _10433 = (int)*(((s1_ptr)_2)->base + _i_18586);
        _2 = (int)SEQ_PTR(_10433);
        _10434 = (int)*(((s1_ptr)_2)->base + 1);
        _10433 = NOVALUE;
        if (binary_op_a(NOTEQ, _10434, 0)){
            _10434 = NOVALUE;
            goto L5; // [91] 105
        }
        _10434 = NOVALUE;

        /** 			tmp = ""*/
        RefDS(_5);
        DeRef(_tmp_18588);
        _tmp_18588 = _5;
        goto L6; // [102] 129
L5: 

        /** 			tmp = haystack[match_data[i][1]..match_data[i][2]]*/
        _2 = (int)SEQ_PTR(_match_data_18578);
        _10436 = (int)*(((s1_ptr)_2)->base + _i_18586);
        _2 = (int)SEQ_PTR(_10436);
        _10437 = (int)*(((s1_ptr)_2)->base + 1);
        _10436 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18578);
        _10438 = (int)*(((s1_ptr)_2)->base + _i_18586);
        _2 = (int)SEQ_PTR(_10438);
        _10439 = (int)*(((s1_ptr)_2)->base + 2);
        _10438 = NOVALUE;
        rhs_slice_target = (object_ptr)&_tmp_18588;
        RHS_Slice(_haystack_18570, _10437, _10439);
L6: 

        /** 		if str_offsets then*/
        if (_str_offsets_18576 == 0)
        {
            goto L7; // [131] 167
        }
        else{
        }

        /** 			match_data[i] = { tmp, match_data[i][1], match_data[i][2] }*/
        _2 = (int)SEQ_PTR(_match_data_18578);
        _10441 = (int)*(((s1_ptr)_2)->base + _i_18586);
        _2 = (int)SEQ_PTR(_10441);
        _10442 = (int)*(((s1_ptr)_2)->base + 1);
        _10441 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18578);
        _10443 = (int)*(((s1_ptr)_2)->base + _i_18586);
        _2 = (int)SEQ_PTR(_10443);
        _10444 = (int)*(((s1_ptr)_2)->base + 2);
        _10443 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_tmp_18588);
        *((int *)(_2+4)) = _tmp_18588;
        Ref(_10442);
        *((int *)(_2+8)) = _10442;
        Ref(_10444);
        *((int *)(_2+12)) = _10444;
        _10445 = MAKE_SEQ(_1);
        _10444 = NOVALUE;
        _10442 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18578);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_18578 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18586);
        _1 = *(int *)_2;
        *(int *)_2 = _10445;
        if( _1 != _10445 ){
            DeRef(_1);
        }
        _10445 = NOVALUE;
        goto L8; // [164] 176
L7: 

        /** 			match_data[i] = tmp*/
        RefDS(_tmp_18588);
        _2 = (int)SEQ_PTR(_match_data_18578);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_18578 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18586);
        _1 = *(int *)_2;
        *(int *)_2 = _tmp_18588;
        DeRef(_1);
L8: 
        DeRef(_tmp_18588);
        _tmp_18588 = NOVALUE;

        /** 	end for*/
        _i_18586 = _i_18586 + 1;
        goto L3; // [180] 79
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_18568);
    DeRef(_haystack_18570);
    DeRef(_options_18572);
    _10437 = NOVALUE;
    _10439 = NOVALUE;
    return _match_data_18578;
    ;
}


int _35all_matches(int _re_18608, int _haystack_18610, int _from_18611, int _options_18612)
{
    int _str_offsets_18616 = NOVALUE;
    int _match_data_18618 = NOVALUE;
    int _tmp_18633 = NOVALUE;
    int _a_18634 = NOVALUE;
    int _b_18635 = NOVALUE;
    int _10469 = NOVALUE;
    int _10468 = NOVALUE;
    int _10466 = NOVALUE;
    int _10463 = NOVALUE;
    int _10462 = NOVALUE;
    int _10459 = NOVALUE;
    int _10458 = NOVALUE;
    int _10457 = NOVALUE;
    int _10456 = NOVALUE;
    int _10455 = NOVALUE;
    int _10453 = NOVALUE;
    int _10451 = NOVALUE;
    int _10450 = NOVALUE;
    int _10449 = NOVALUE;
    int _10446 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_from_18611)) {
        _1 = (long)(DBL_PTR(_from_18611)->dbl);
        if (UNIQUE(DBL_PTR(_from_18611)) && (DBL_PTR(_from_18611)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18611);
        _from_18611 = _1;
    }

    /** 	if sequence(options) then */
    _10446 = IS_SEQUENCE(_options_18612);
    if (_10446 == 0)
    {
        _10446 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _10446 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18612);
    _0 = _options_18612;
    _options_18612 = _17or_all(_options_18612);
    DeRef(_0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_18612)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_18612;
             _str_offsets_18616 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_18616 = binary_op(AND_BITS, 201326592, _options_18612);
    }
    if (!IS_ATOM_INT(_str_offsets_18616)) {
        _1 = (long)(DBL_PTR(_str_offsets_18616)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_18616)) && (DBL_PTR(_str_offsets_18616)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_18616);
        _str_offsets_18616 = _1;
    }

    /** 	object match_data = find_all(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _10449 = not_bits(201326592);
    if (IS_ATOM_INT(_options_18612) && IS_ATOM_INT(_10449)) {
        {unsigned long tu;
             tu = (unsigned long)_options_18612 & (unsigned long)_10449;
             _10450 = MAKE_UINT(tu);
        }
    }
    else {
        _10450 = binary_op(AND_BITS, _options_18612, _10449);
    }
    DeRef(_10449);
    _10449 = NOVALUE;
    Ref(_re_18608);
    _10451 = _35get_ovector_size(_re_18608, 30);
    Ref(_re_18608);
    Ref(_haystack_18610);
    _0 = _match_data_18618;
    _match_data_18618 = _35find_all(_re_18608, _haystack_18610, _from_18611, _10450, _10451);
    DeRef(_0);
    _10450 = NOVALUE;
    _10451 = NOVALUE;

    /** 	if length(match_data) = 0 then */
    if (IS_SEQUENCE(_match_data_18618)){
            _10453 = SEQ_PTR(_match_data_18618)->length;
    }
    else {
        _10453 = 1;
    }
    if (_10453 != 0)
    goto L2; // [57] 68

    /** 		return ERROR_NOMATCH */
    DeRef(_re_18608);
    DeRef(_haystack_18610);
    DeRef(_options_18612);
    DeRef(_match_data_18618);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_18618)){
            _10455 = SEQ_PTR(_match_data_18618)->length;
    }
    else {
        _10455 = 1;
    }
    {
        int _i_18627;
        _i_18627 = 1;
L3: 
        if (_i_18627 > _10455){
            goto L4; // [73] 219
        }

        /** 		for j = 1 to length(match_data[i]) do*/
        _2 = (int)SEQ_PTR(_match_data_18618);
        _10456 = (int)*(((s1_ptr)_2)->base + _i_18627);
        if (IS_SEQUENCE(_10456)){
                _10457 = SEQ_PTR(_10456)->length;
        }
        else {
            _10457 = 1;
        }
        _10456 = NOVALUE;
        {
            int _j_18630;
            _j_18630 = 1;
L5: 
            if (_j_18630 > _10457){
                goto L6; // [89] 212
            }

            /** 			sequence tmp*/

            /** 			integer a,b*/

            /** 			a = match_data[i][j][1]*/
            _2 = (int)SEQ_PTR(_match_data_18618);
            _10458 = (int)*(((s1_ptr)_2)->base + _i_18627);
            _2 = (int)SEQ_PTR(_10458);
            _10459 = (int)*(((s1_ptr)_2)->base + _j_18630);
            _10458 = NOVALUE;
            _2 = (int)SEQ_PTR(_10459);
            _a_18634 = (int)*(((s1_ptr)_2)->base + 1);
            if (!IS_ATOM_INT(_a_18634)){
                _a_18634 = (long)DBL_PTR(_a_18634)->dbl;
            }
            _10459 = NOVALUE;

            /** 			if a = 0 then*/
            if (_a_18634 != 0)
            goto L7; // [120] 134

            /** 				tmp = ""*/
            RefDS(_5);
            DeRef(_tmp_18633);
            _tmp_18633 = _5;
            goto L8; // [131] 160
L7: 

            /** 				b = match_data[i][j][2]*/
            _2 = (int)SEQ_PTR(_match_data_18618);
            _10462 = (int)*(((s1_ptr)_2)->base + _i_18627);
            _2 = (int)SEQ_PTR(_10462);
            _10463 = (int)*(((s1_ptr)_2)->base + _j_18630);
            _10462 = NOVALUE;
            _2 = (int)SEQ_PTR(_10463);
            _b_18635 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_b_18635)){
                _b_18635 = (long)DBL_PTR(_b_18635)->dbl;
            }
            _10463 = NOVALUE;

            /** 				tmp = haystack[a..b]*/
            rhs_slice_target = (object_ptr)&_tmp_18633;
            RHS_Slice(_haystack_18610, _a_18634, _b_18635);
L8: 

            /** 			if str_offsets then*/
            if (_str_offsets_18616 == 0)
            {
                goto L9; // [162] 189
            }
            else{
            }

            /** 				match_data[i][j] = { tmp, a, b }*/
            _2 = (int)SEQ_PTR(_match_data_18618);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _match_data_18618 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_18627 + ((s1_ptr)_2)->base);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            RefDS(_tmp_18633);
            *((int *)(_2+4)) = _tmp_18633;
            *((int *)(_2+8)) = _a_18634;
            *((int *)(_2+12)) = _b_18635;
            _10468 = MAKE_SEQ(_1);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18630);
            _1 = *(int *)_2;
            *(int *)_2 = _10468;
            if( _1 != _10468 ){
                DeRef(_1);
            }
            _10468 = NOVALUE;
            _10466 = NOVALUE;
            goto LA; // [186] 203
L9: 

            /** 				match_data[i][j] = tmp*/
            _2 = (int)SEQ_PTR(_match_data_18618);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _match_data_18618 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_18627 + ((s1_ptr)_2)->base);
            RefDS(_tmp_18633);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18630);
            _1 = *(int *)_2;
            *(int *)_2 = _tmp_18633;
            DeRef(_1);
            _10469 = NOVALUE;
LA: 
            DeRef(_tmp_18633);
            _tmp_18633 = NOVALUE;

            /** 		end for*/
            _j_18630 = _j_18630 + 1;
            goto L5; // [207] 96
L6: 
            ;
        }

        /** 	end for*/
        _i_18627 = _i_18627 + 1;
        goto L3; // [214] 80
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_18608);
    DeRef(_haystack_18610);
    DeRef(_options_18612);
    _10456 = NOVALUE;
    return _match_data_18618;
    ;
}


int _35split(int _re_18655, int _text_18657, int _from_18658, int _options_18659)
{
    int _10471 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18658)) {
        _1 = (long)(DBL_PTR(_from_18658)->dbl);
        if (UNIQUE(DBL_PTR(_from_18658)) && (DBL_PTR(_from_18658)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18658);
        _from_18658 = _1;
    }

    /** 	return split_limit(re, text, 0, from, options)*/
    Ref(_re_18655);
    Ref(_text_18657);
    Ref(_options_18659);
    _10471 = _35split_limit(_re_18655, _text_18657, 0, _from_18658, _options_18659);
    DeRef(_re_18655);
    DeRef(_text_18657);
    DeRef(_options_18659);
    return _10471;
    ;
}


int _35split_limit(int _re_18664, int _text_18666, int _limit_18667, int _from_18668, int _options_18669)
{
    int _match_data_18673 = NOVALUE;
    int _result_18676 = NOVALUE;
    int _last_18677 = NOVALUE;
    int _a_18688 = NOVALUE;
    int _10497 = NOVALUE;
    int _10496 = NOVALUE;
    int _10495 = NOVALUE;
    int _10493 = NOVALUE;
    int _10491 = NOVALUE;
    int _10490 = NOVALUE;
    int _10489 = NOVALUE;
    int _10488 = NOVALUE;
    int _10487 = NOVALUE;
    int _10484 = NOVALUE;
    int _10483 = NOVALUE;
    int _10482 = NOVALUE;
    int _10479 = NOVALUE;
    int _10478 = NOVALUE;
    int _10476 = NOVALUE;
    int _10474 = NOVALUE;
    int _10472 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_18667)) {
        _1 = (long)(DBL_PTR(_limit_18667)->dbl);
        if (UNIQUE(DBL_PTR(_limit_18667)) && (DBL_PTR(_limit_18667)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_18667);
        _limit_18667 = _1;
    }
    if (!IS_ATOM_INT(_from_18668)) {
        _1 = (long)(DBL_PTR(_from_18668)->dbl);
        if (UNIQUE(DBL_PTR(_from_18668)) && (DBL_PTR(_from_18668)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18668);
        _from_18668 = _1;
    }

    /** 	if sequence(options) then */
    _10472 = IS_SEQUENCE(_options_18669);
    if (_10472 == 0)
    {
        _10472 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _10472 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18669);
    _0 = _options_18669;
    _options_18669 = _17or_all(_options_18669);
    DeRef(_0);
L1: 

    /** 	sequence match_data = find_all(re, text, from, options), result*/
    Ref(_re_18664);
    _10474 = _35get_ovector_size(_re_18664, 30);
    Ref(_re_18664);
    Ref(_text_18666);
    Ref(_options_18669);
    _0 = _match_data_18673;
    _match_data_18673 = _35find_all(_re_18664, _text_18666, _from_18668, _options_18669, _10474);
    DeRef(_0);
    _10474 = NOVALUE;

    /** 	integer last = 1*/
    _last_18677 = 1;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _10476 = (_limit_18667 == 0);
    if (_10476 != 0) {
        goto L2; // [54] 70
    }
    if (IS_SEQUENCE(_match_data_18673)){
            _10478 = SEQ_PTR(_match_data_18673)->length;
    }
    else {
        _10478 = 1;
    }
    _10479 = (_limit_18667 > _10478);
    _10478 = NOVALUE;
    if (_10479 == 0)
    {
        DeRef(_10479);
        _10479 = NOVALUE;
        goto L3; // [66] 78
    }
    else{
        DeRef(_10479);
        _10479 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_18673)){
            _limit_18667 = SEQ_PTR(_match_data_18673)->length;
    }
    else {
        _limit_18667 = 1;
    }
L3: 

    /** 	result = repeat(0, limit)*/
    DeRef(_result_18676);
    _result_18676 = Repeat(0, _limit_18667);

    /** 	for i = 1 to limit do*/
    _10482 = _limit_18667;
    {
        int _i_18686;
        _i_18686 = 1;
L4: 
        if (_i_18686 > _10482){
            goto L5; // [89] 176
        }

        /** 		integer a*/

        /** 		a = match_data[i][1][1]*/
        _2 = (int)SEQ_PTR(_match_data_18673);
        _10483 = (int)*(((s1_ptr)_2)->base + _i_18686);
        _2 = (int)SEQ_PTR(_10483);
        _10484 = (int)*(((s1_ptr)_2)->base + 1);
        _10483 = NOVALUE;
        _2 = (int)SEQ_PTR(_10484);
        _a_18688 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_a_18688)){
            _a_18688 = (long)DBL_PTR(_a_18688)->dbl;
        }
        _10484 = NOVALUE;

        /** 		if a = 0 then*/
        if (_a_18688 != 0)
        goto L6; // [118] 131

        /** 			result[i] = ""*/
        RefDS(_5);
        _2 = (int)SEQ_PTR(_result_18676);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_18676 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18686);
        _1 = *(int *)_2;
        *(int *)_2 = _5;
        DeRef(_1);
        goto L7; // [128] 167
L6: 

        /** 			result[i] = text[last..a - 1]*/
        _10487 = _a_18688 - 1;
        rhs_slice_target = (object_ptr)&_10488;
        RHS_Slice(_text_18666, _last_18677, _10487);
        _2 = (int)SEQ_PTR(_result_18676);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_18676 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18686);
        _1 = *(int *)_2;
        *(int *)_2 = _10488;
        if( _1 != _10488 ){
            DeRef(_1);
        }
        _10488 = NOVALUE;

        /** 			last = match_data[i][1][2] + 1*/
        _2 = (int)SEQ_PTR(_match_data_18673);
        _10489 = (int)*(((s1_ptr)_2)->base + _i_18686);
        _2 = (int)SEQ_PTR(_10489);
        _10490 = (int)*(((s1_ptr)_2)->base + 1);
        _10489 = NOVALUE;
        _2 = (int)SEQ_PTR(_10490);
        _10491 = (int)*(((s1_ptr)_2)->base + 2);
        _10490 = NOVALUE;
        if (IS_ATOM_INT(_10491)) {
            _last_18677 = _10491 + 1;
        }
        else
        { // coercing _last_18677 to an integer 1
            _last_18677 = 1+(long)(DBL_PTR(_10491)->dbl);
            if( !IS_ATOM_INT(_last_18677) ){
                _last_18677 = (object)DBL_PTR(_last_18677)->dbl;
            }
        }
        _10491 = NOVALUE;
L7: 

        /** 	end for*/
        _i_18686 = _i_18686 + 1;
        goto L4; // [171] 96
L5: 
        ;
    }

    /** 	if last < length(text) then*/
    if (IS_SEQUENCE(_text_18666)){
            _10493 = SEQ_PTR(_text_18666)->length;
    }
    else {
        _10493 = 1;
    }
    if (_last_18677 >= _10493)
    goto L8; // [181] 204

    /** 		result &= { text[last..$] }*/
    if (IS_SEQUENCE(_text_18666)){
            _10495 = SEQ_PTR(_text_18666)->length;
    }
    else {
        _10495 = 1;
    }
    rhs_slice_target = (object_ptr)&_10496;
    RHS_Slice(_text_18666, _last_18677, _10495);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _10496;
    _10497 = MAKE_SEQ(_1);
    _10496 = NOVALUE;
    Concat((object_ptr)&_result_18676, _result_18676, _10497);
    DeRefDS(_10497);
    _10497 = NOVALUE;
L8: 

    /** 	return result*/
    DeRef(_re_18664);
    DeRef(_text_18666);
    DeRef(_options_18669);
    DeRef(_match_data_18673);
    DeRef(_10476);
    _10476 = NOVALUE;
    DeRef(_10487);
    _10487 = NOVALUE;
    return _result_18676;
    ;
}


int _35find_replace(int _ex_18710, int _text_18712, int _replacement_18713, int _from_18714, int _options_18715)
{
    int _10499 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18714)) {
        _1 = (long)(DBL_PTR(_from_18714)->dbl);
        if (UNIQUE(DBL_PTR(_from_18714)) && (DBL_PTR(_from_18714)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18714);
        _from_18714 = _1;
    }

    /** 	return find_replace_limit(ex, text, replacement, -1, from, options)*/
    Ref(_ex_18710);
    Ref(_text_18712);
    RefDS(_replacement_18713);
    Ref(_options_18715);
    _10499 = _35find_replace_limit(_ex_18710, _text_18712, _replacement_18713, -1, _from_18714, _options_18715);
    DeRef(_ex_18710);
    DeRef(_text_18712);
    DeRefDS(_replacement_18713);
    DeRef(_options_18715);
    return _10499;
    ;
}


int _35find_replace_limit(int _ex_18720, int _text_18722, int _replacement_18723, int _limit_18724, int _from_18725, int _options_18726)
{
    int _10503 = NOVALUE;
    int _10502 = NOVALUE;
    int _10500 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_18724)) {
        _1 = (long)(DBL_PTR(_limit_18724)->dbl);
        if (UNIQUE(DBL_PTR(_limit_18724)) && (DBL_PTR(_limit_18724)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_18724);
        _limit_18724 = _1;
    }
    if (!IS_ATOM_INT(_from_18725)) {
        _1 = (long)(DBL_PTR(_from_18725)->dbl);
        if (UNIQUE(DBL_PTR(_from_18725)) && (DBL_PTR(_from_18725)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18725);
        _from_18725 = _1;
    }

    /** 	if sequence(options) then */
    _10500 = IS_SEQUENCE(_options_18726);
    if (_10500 == 0)
    {
        _10500 = NOVALUE;
        goto L1; // [16] 26
    }
    else{
        _10500 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18726);
    _0 = _options_18726;
    _options_18726 = _17or_all(_options_18726);
    DeRef(_0);
L1: 

    /**     return machine_func(M_PCRE_REPLACE, { ex, text, replacement, options, */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_18720);
    *((int *)(_2+4)) = _ex_18720;
    Ref(_text_18722);
    *((int *)(_2+8)) = _text_18722;
    RefDS(_replacement_18723);
    *((int *)(_2+12)) = _replacement_18723;
    Ref(_options_18726);
    *((int *)(_2+16)) = _options_18726;
    *((int *)(_2+20)) = _from_18725;
    *((int *)(_2+24)) = _limit_18724;
    _10502 = MAKE_SEQ(_1);
    _10503 = machine(71, _10502);
    DeRefDS(_10502);
    _10502 = NOVALUE;
    DeRef(_ex_18720);
    DeRef(_text_18722);
    DeRefDS(_replacement_18723);
    DeRef(_options_18726);
    return _10503;
    ;
}


int _35find_replace_callback(int _ex_18734, int _text_18736, int _rid_18737, int _limit_18738, int _from_18739, int _options_18740)
{
    int _match_data_18744 = NOVALUE;
    int _replace_data_18747 = NOVALUE;
    int _params_18758 = NOVALUE;
    int _10539 = NOVALUE;
    int _10538 = NOVALUE;
    int _10537 = NOVALUE;
    int _10536 = NOVALUE;
    int _10535 = NOVALUE;
    int _10534 = NOVALUE;
    int _10533 = NOVALUE;
    int _10532 = NOVALUE;
    int _10531 = NOVALUE;
    int _10530 = NOVALUE;
    int _10529 = NOVALUE;
    int _10528 = NOVALUE;
    int _10527 = NOVALUE;
    int _10526 = NOVALUE;
    int _10525 = NOVALUE;
    int _10524 = NOVALUE;
    int _10523 = NOVALUE;
    int _10522 = NOVALUE;
    int _10521 = NOVALUE;
    int _10520 = NOVALUE;
    int _10519 = NOVALUE;
    int _10518 = NOVALUE;
    int _10516 = NOVALUE;
    int _10515 = NOVALUE;
    int _10514 = NOVALUE;
    int _10511 = NOVALUE;
    int _10510 = NOVALUE;
    int _10508 = NOVALUE;
    int _10506 = NOVALUE;
    int _10504 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_18737)) {
        _1 = (long)(DBL_PTR(_rid_18737)->dbl);
        if (UNIQUE(DBL_PTR(_rid_18737)) && (DBL_PTR(_rid_18737)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_18737);
        _rid_18737 = _1;
    }
    if (!IS_ATOM_INT(_limit_18738)) {
        _1 = (long)(DBL_PTR(_limit_18738)->dbl);
        if (UNIQUE(DBL_PTR(_limit_18738)) && (DBL_PTR(_limit_18738)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_18738);
        _limit_18738 = _1;
    }
    if (!IS_ATOM_INT(_from_18739)) {
        _1 = (long)(DBL_PTR(_from_18739)->dbl);
        if (UNIQUE(DBL_PTR(_from_18739)) && (DBL_PTR(_from_18739)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18739);
        _from_18739 = _1;
    }

    /** 	if sequence(options) then */
    _10504 = IS_SEQUENCE(_options_18740);
    if (_10504 == 0)
    {
        _10504 = NOVALUE;
        goto L1; // [18] 28
    }
    else{
        _10504 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18740);
    _0 = _options_18740;
    _options_18740 = _17or_all(_options_18740);
    DeRef(_0);
L1: 

    /** 	sequence match_data = find_all(ex, text, from, options), replace_data*/
    Ref(_ex_18734);
    _10506 = _35get_ovector_size(_ex_18734, 30);
    Ref(_ex_18734);
    Ref(_text_18736);
    Ref(_options_18740);
    _0 = _match_data_18744;
    _match_data_18744 = _35find_all(_ex_18734, _text_18736, _from_18739, _options_18740, _10506);
    DeRef(_0);
    _10506 = NOVALUE;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _10508 = (_limit_18738 == 0);
    if (_10508 != 0) {
        goto L2; // [51] 67
    }
    if (IS_SEQUENCE(_match_data_18744)){
            _10510 = SEQ_PTR(_match_data_18744)->length;
    }
    else {
        _10510 = 1;
    }
    _10511 = (_limit_18738 > _10510);
    _10510 = NOVALUE;
    if (_10511 == 0)
    {
        DeRef(_10511);
        _10511 = NOVALUE;
        goto L3; // [63] 75
    }
    else{
        DeRef(_10511);
        _10511 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_18744)){
            _limit_18738 = SEQ_PTR(_match_data_18744)->length;
    }
    else {
        _limit_18738 = 1;
    }
L3: 

    /** 	replace_data = repeat(0, limit)*/
    DeRef(_replace_data_18747);
    _replace_data_18747 = Repeat(0, _limit_18738);

    /** 	for i = 1 to limit do*/
    _10514 = _limit_18738;
    {
        int _i_18756;
        _i_18756 = 1;
L4: 
        if (_i_18756 > _10514){
            goto L5; // [86] 218
        }

        /** 		sequence params = repeat(0, length(match_data[i]))*/
        _2 = (int)SEQ_PTR(_match_data_18744);
        _10515 = (int)*(((s1_ptr)_2)->base + _i_18756);
        if (IS_SEQUENCE(_10515)){
                _10516 = SEQ_PTR(_10515)->length;
        }
        else {
            _10516 = 1;
        }
        _10515 = NOVALUE;
        DeRef(_params_18758);
        _params_18758 = Repeat(0, _10516);
        _10516 = NOVALUE;

        /** 		for j = 1 to length(match_data[i]) do*/
        _2 = (int)SEQ_PTR(_match_data_18744);
        _10518 = (int)*(((s1_ptr)_2)->base + _i_18756);
        if (IS_SEQUENCE(_10518)){
                _10519 = SEQ_PTR(_10518)->length;
        }
        else {
            _10519 = 1;
        }
        _10518 = NOVALUE;
        {
            int _j_18763;
            _j_18763 = 1;
L6: 
            if (_j_18763 > _10519){
                goto L7; // [115] 195
            }

            /** 			if equal(match_data[i][j],{0,0}) then*/
            _2 = (int)SEQ_PTR(_match_data_18744);
            _10520 = (int)*(((s1_ptr)_2)->base + _i_18756);
            _2 = (int)SEQ_PTR(_10520);
            _10521 = (int)*(((s1_ptr)_2)->base + _j_18763);
            _10520 = NOVALUE;
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = 0;
            ((int *)_2)[2] = 0;
            _10522 = MAKE_SEQ(_1);
            if (_10521 == _10522)
            _10523 = 1;
            else if (IS_ATOM_INT(_10521) && IS_ATOM_INT(_10522))
            _10523 = 0;
            else
            _10523 = (compare(_10521, _10522) == 0);
            _10521 = NOVALUE;
            DeRefDS(_10522);
            _10522 = NOVALUE;
            if (_10523 == 0)
            {
                _10523 = NOVALUE;
                goto L8; // [140] 152
            }
            else{
                _10523 = NOVALUE;
            }

            /** 				params[j] = 0*/
            _2 = (int)SEQ_PTR(_params_18758);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _params_18758 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18763);
            _1 = *(int *)_2;
            *(int *)_2 = 0;
            DeRef(_1);
            goto L9; // [149] 188
L8: 

            /** 				params[j] = text[match_data[i][j][1]..match_data[i][j][2]]*/
            _2 = (int)SEQ_PTR(_match_data_18744);
            _10524 = (int)*(((s1_ptr)_2)->base + _i_18756);
            _2 = (int)SEQ_PTR(_10524);
            _10525 = (int)*(((s1_ptr)_2)->base + _j_18763);
            _10524 = NOVALUE;
            _2 = (int)SEQ_PTR(_10525);
            _10526 = (int)*(((s1_ptr)_2)->base + 1);
            _10525 = NOVALUE;
            _2 = (int)SEQ_PTR(_match_data_18744);
            _10527 = (int)*(((s1_ptr)_2)->base + _i_18756);
            _2 = (int)SEQ_PTR(_10527);
            _10528 = (int)*(((s1_ptr)_2)->base + _j_18763);
            _10527 = NOVALUE;
            _2 = (int)SEQ_PTR(_10528);
            _10529 = (int)*(((s1_ptr)_2)->base + 2);
            _10528 = NOVALUE;
            rhs_slice_target = (object_ptr)&_10530;
            RHS_Slice(_text_18736, _10526, _10529);
            _2 = (int)SEQ_PTR(_params_18758);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _params_18758 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18763);
            _1 = *(int *)_2;
            *(int *)_2 = _10530;
            if( _1 != _10530 ){
                DeRef(_1);
            }
            _10530 = NOVALUE;
L9: 

            /** 		end for*/
            _j_18763 = _j_18763 + 1;
            goto L6; // [190] 122
L7: 
            ;
        }

        /** 		replace_data[i] = call_func(rid, { params })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_params_18758);
        *((int *)(_2+4)) = _params_18758;
        _10531 = MAKE_SEQ(_1);
        _1 = (int)SEQ_PTR(_10531);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_rid_18737].addr;
        Ref(*(int *)(_2+4));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRef(_10532);
        _10532 = _1;
        DeRefDS(_10531);
        _10531 = NOVALUE;
        _2 = (int)SEQ_PTR(_replace_data_18747);
        _2 = (int)(((s1_ptr)_2)->base + _i_18756);
        _1 = *(int *)_2;
        *(int *)_2 = _10532;
        if( _1 != _10532 ){
            DeRef(_1);
        }
        _10532 = NOVALUE;
        DeRefDS(_params_18758);
        _params_18758 = NOVALUE;

        /** 	end for*/
        _i_18756 = _i_18756 + 1;
        goto L4; // [213] 93
L5: 
        ;
    }

    /** 	for i = limit to 1 by -1 do*/
    {
        int _i_18782;
        _i_18782 = _limit_18738;
LA: 
        if (_i_18782 < 1){
            goto LB; // [220] 270
        }

        /** 		text = replace(text, replace_data[i], match_data[i][1][1], match_data[i][1][2])*/
        _2 = (int)SEQ_PTR(_replace_data_18747);
        _10533 = (int)*(((s1_ptr)_2)->base + _i_18782);
        _2 = (int)SEQ_PTR(_match_data_18744);
        _10534 = (int)*(((s1_ptr)_2)->base + _i_18782);
        _2 = (int)SEQ_PTR(_10534);
        _10535 = (int)*(((s1_ptr)_2)->base + 1);
        _10534 = NOVALUE;
        _2 = (int)SEQ_PTR(_10535);
        _10536 = (int)*(((s1_ptr)_2)->base + 1);
        _10535 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18744);
        _10537 = (int)*(((s1_ptr)_2)->base + _i_18782);
        _2 = (int)SEQ_PTR(_10537);
        _10538 = (int)*(((s1_ptr)_2)->base + 1);
        _10537 = NOVALUE;
        _2 = (int)SEQ_PTR(_10538);
        _10539 = (int)*(((s1_ptr)_2)->base + 2);
        _10538 = NOVALUE;
        {
            int p1 = _text_18736;
            int p2 = _10533;
            int p3 = _10536;
            int p4 = _10539;
            struct replace_block replace_params;
            replace_params.copy_to   = &p1;
            replace_params.copy_from = &p2;
            replace_params.start     = &p3;
            replace_params.stop      = &p4;
            replace_params.target    = &_text_18736;
            Replace( &replace_params );
        }
        _10533 = NOVALUE;
        _10536 = NOVALUE;
        _10539 = NOVALUE;

        /** 	end for*/
        _i_18782 = _i_18782 + -1;
        goto LA; // [265] 227
LB: 
        ;
    }

    /** 	return text*/
    DeRef(_ex_18734);
    DeRef(_options_18740);
    DeRef(_match_data_18744);
    DeRef(_replace_data_18747);
    DeRef(_10508);
    _10508 = NOVALUE;
    _10515 = NOVALUE;
    _10518 = NOVALUE;
    _10526 = NOVALUE;
    _10529 = NOVALUE;
    return _text_18736;
    ;
}



// 0xBDC383F8
