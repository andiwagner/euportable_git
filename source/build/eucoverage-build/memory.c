// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _13positive_int(int _x_1198)
{
    int _541 = NOVALUE;
    int _539 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(x) then*/
    if (IS_ATOM_INT(_x_1198))
    _539 = 1;
    else if (IS_ATOM_DBL(_x_1198))
    _539 = IS_ATOM_INT(DoubleToInt(_x_1198));
    else
    _539 = 0;
    if (_539 != 0)
    goto L1; // [6] 16
    _539 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_1198);
    return 0;
L1: 

    /**     return x >= 1*/
    if (IS_ATOM_INT(_x_1198)) {
        _541 = (_x_1198 >= 1);
    }
    else {
        _541 = binary_op(GREATEREQ, _x_1198, 1);
    }
    DeRef(_x_1198);
    return _541;
    ;
}


int _13machine_addr(int _a_1205)
{
    int _550 = NOVALUE;
    int _549 = NOVALUE;
    int _548 = NOVALUE;
    int _546 = NOVALUE;
    int _544 = NOVALUE;
    int _542 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(a) then*/
    _542 = IS_ATOM(_a_1205);
    if (_542 != 0)
    goto L1; // [6] 16
    _542 = NOVALUE;

    /** 		return 0*/
    DeRef(_a_1205);
    return 0;
L1: 

    /** 	if not integer(a)then*/
    if (IS_ATOM_INT(_a_1205))
    _544 = 1;
    else if (IS_ATOM_DBL(_a_1205))
    _544 = IS_ATOM_INT(DoubleToInt(_a_1205));
    else
    _544 = 0;
    if (_544 != 0)
    goto L2; // [21] 41
    _544 = NOVALUE;

    /** 		if floor(a) != a then*/
    if (IS_ATOM_INT(_a_1205))
    _546 = e_floor(_a_1205);
    else
    _546 = unary_op(FLOOR, _a_1205);
    if (binary_op_a(EQUALS, _546, _a_1205)){
        DeRef(_546);
        _546 = NOVALUE;
        goto L3; // [29] 40
    }
    DeRef(_546);
    _546 = NOVALUE;

    /** 			return 0*/
    DeRef(_a_1205);
    return 0;
L3: 
L2: 

    /** 	return a > 0 and a <= MAX_ADDR*/
    if (IS_ATOM_INT(_a_1205)) {
        _548 = (_a_1205 > 0);
    }
    else {
        _548 = binary_op(GREATER, _a_1205, 0);
    }
    if (IS_ATOM_INT(_a_1205) && IS_ATOM_INT(_13MAX_ADDR_1191)) {
        _549 = (_a_1205 <= _13MAX_ADDR_1191);
    }
    else {
        _549 = binary_op(LESSEQ, _a_1205, _13MAX_ADDR_1191);
    }
    if (IS_ATOM_INT(_548) && IS_ATOM_INT(_549)) {
        _550 = (_548 != 0 && _549 != 0);
    }
    else {
        _550 = binary_op(AND, _548, _549);
    }
    DeRef(_548);
    _548 = NOVALUE;
    DeRef(_549);
    _549 = NOVALUE;
    DeRef(_a_1205);
    return _550;
    ;
}


void _13deallocate(int _addr_1220)
{
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_1220);

    /** end procedure*/
    DeRef(_addr_1220);
    return;
    ;
}


void _13register_block(int _block_addr_1227, int _block_len_1228, int _protection_1229)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_protection_1229)) {
        _1 = (long)(DBL_PTR(_protection_1229)->dbl);
        if (UNIQUE(DBL_PTR(_protection_1229)) && (DBL_PTR(_protection_1229)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_1229);
        _protection_1229 = _1;
    }

    /** end procedure*/
    return;
    ;
}


void _13unregister_block(int _block_addr_1233)
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _13safe_address(int _start_1237, int _len_1238, int _action_1239)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_len_1238)) {
        _1 = (long)(DBL_PTR(_len_1238)->dbl);
        if (UNIQUE(DBL_PTR(_len_1238)) && (DBL_PTR(_len_1238)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_1238);
        _len_1238 = _1;
    }

    /** 	return 1*/
    return 1;
    ;
}


void _13check_all_blocks()
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _13prepare_block(int _addr_1246, int _a_1247, int _protection_1248)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_1247)) {
        _1 = (long)(DBL_PTR(_a_1247)->dbl);
        if (UNIQUE(DBL_PTR(_a_1247)) && (DBL_PTR(_a_1247)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_1247);
        _a_1247 = _1;
    }
    if (!IS_ATOM_INT(_protection_1248)) {
        _1 = (long)(DBL_PTR(_protection_1248)->dbl);
        if (UNIQUE(DBL_PTR(_protection_1248)) && (DBL_PTR(_protection_1248)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_1248);
        _protection_1248 = _1;
    }

    /** 	return addr*/
    return _addr_1246;
    ;
}


int _13bordered_address(int _addr_1256)
{
    int _555 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(addr) then*/
    _555 = IS_ATOM(_addr_1256);
    if (_555 != 0)
    goto L1; // [6] 16
    _555 = NOVALUE;

    /** 		return 0*/
    DeRef(_addr_1256);
    return 0;
L1: 

    /** 	return 1*/
    DeRef(_addr_1256);
    return 1;
    ;
}


int _13dep_works()
{
    int _557 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return (DEP_really_works and use_DEP)*/
    _557 = (_12DEP_really_works_1163 != 0 && 1 != 0);
    return _557;

    /** 	return 1*/
    _557 = NOVALUE;
    return 1;
    ;
}


void _13free_code(int _addr_1268, int _size_1269, int _wordsize_1271)
{
    int _dep_works_inlined_dep_works_at_6_19377 = NOVALUE;
    int _561 = NOVALUE;
    int _560 = NOVALUE;
    int _559 = NOVALUE;
    int _558 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_1269)) {
        _1 = (long)(DBL_PTR(_size_1269)->dbl);
        if (UNIQUE(DBL_PTR(_size_1269)) && (DBL_PTR(_size_1269)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_1269);
        _size_1269 = _1;
    }

    /** 		if dep_works() then*/

    /** 	ifdef WINDOWS then*/

    /** 		return (DEP_really_works and use_DEP)*/
    _dep_works_inlined_dep_works_at_6_19377 = (_12DEP_really_works_1163 != 0 && 1 != 0);
    goto L1; // [19] 27

    /** 	return 1*/
    _dep_works_inlined_dep_works_at_6_19377 = 1;
L1: 
    DeRef(_558);
    _558 = _dep_works_inlined_dep_works_at_6_19377;
    if (_558 == 0)
    {
        _558 = NOVALUE;
        goto L2; // [30] 57
    }
    else{
        _558 = NOVALUE;
    }

    /** 			c_func(VirtualFree_rid, { addr, size*wordsize, MEM_RELEASE })*/
    if (IS_ATOM_INT(_wordsize_1271)) {
        if (_size_1269 == (short)_size_1269 && _wordsize_1271 <= INT15 && _wordsize_1271 >= -INT15)
        _559 = _size_1269 * _wordsize_1271;
        else
        _559 = NewDouble(_size_1269 * (double)_wordsize_1271);
    }
    else {
        _559 = binary_op(MULTIPLY, _size_1269, _wordsize_1271);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_1268);
    *((int *)(_2+4)) = _addr_1268;
    *((int *)(_2+8)) = _559;
    *((int *)(_2+12)) = 32768;
    _560 = MAKE_SEQ(_1);
    _559 = NOVALUE;
    _561 = call_c(1, _13VirtualFree_rid_1265, _560);
    DeRefDS(_560);
    _560 = NOVALUE;
    goto L3; // [54] 63
L2: 

    /** 			machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_1268);
L3: 

    /** end procedure*/
    DeRef(_addr_1268);
    DeRef(_wordsize_1271);
    DeRef(_561);
    _561 = NOVALUE;
    return;
    ;
}



// 0x184D024C
