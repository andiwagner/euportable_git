// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _10open_dll(int _file_name_1713)
{
    int _fh_1723 = NOVALUE;
    int _740 = NOVALUE;
    int _738 = NOVALUE;
    int _737 = NOVALUE;
    int _736 = NOVALUE;
    int _735 = NOVALUE;
    int _734 = NOVALUE;
    int _733 = NOVALUE;
    int _732 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(file_name) > 0 and types:string(file_name) then*/
    if (IS_SEQUENCE(_file_name_1713)){
            _732 = SEQ_PTR(_file_name_1713)->length;
    }
    else {
        _732 = 1;
    }
    _733 = (_732 > 0);
    _732 = NOVALUE;
    if (_733 == 0) {
        goto L1; // [12] 35
    }
    RefDS(_file_name_1713);
    _735 = _3string(_file_name_1713);
    if (_735 == 0) {
        DeRef(_735);
        _735 = NOVALUE;
        goto L1; // [21] 35
    }
    else {
        if (!IS_ATOM_INT(_735) && DBL_PTR(_735)->dbl == 0.0){
            DeRef(_735);
            _735 = NOVALUE;
            goto L1; // [21] 35
        }
        DeRef(_735);
        _735 = NOVALUE;
    }
    DeRef(_735);
    _735 = NOVALUE;

    /** 		return machine_func(M_OPEN_DLL, file_name)*/
    _736 = machine(50, _file_name_1713);
    DeRefDSi(_file_name_1713);
    DeRef(_733);
    _733 = NOVALUE;
    return _736;
L1: 

    /** 	for idx = 1 to length(file_name) do*/
    if (IS_SEQUENCE(_file_name_1713)){
            _737 = SEQ_PTR(_file_name_1713)->length;
    }
    else {
        _737 = 1;
    }
    {
        int _idx_1721;
        _idx_1721 = 1;
L2: 
        if (_idx_1721 > _737){
            goto L3; // [40] 82
        }

        /** 		atom fh = machine_func(M_OPEN_DLL, file_name[idx])*/
        _2 = (int)SEQ_PTR(_file_name_1713);
        _738 = (int)*(((s1_ptr)_2)->base + _idx_1721);
        DeRef(_fh_1723);
        _fh_1723 = machine(50, _738);
        _738 = NOVALUE;

        /** 		if not fh = 0 then*/
        if (IS_ATOM_INT(_fh_1723)) {
            _740 = (_fh_1723 == 0);
        }
        else {
            _740 = unary_op(NOT, _fh_1723);
        }
        if (_740 != 0)
        goto L4; // [62] 73

        /** 			return fh*/
        DeRefDSi(_file_name_1713);
        DeRef(_733);
        _733 = NOVALUE;
        DeRef(_736);
        _736 = NOVALUE;
        _740 = NOVALUE;
        return _fh_1723;
L4: 
        DeRef(_fh_1723);
        _fh_1723 = NOVALUE;

        /** 	end for*/
        _idx_1721 = _idx_1721 + 1;
        goto L2; // [77] 47
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDSi(_file_name_1713);
    DeRef(_733);
    _733 = NOVALUE;
    DeRef(_736);
    _736 = NOVALUE;
    DeRef(_740);
    _740 = NOVALUE;
    return 0;
    ;
}


int _10define_c_proc(int _lib_1737, int _routine_name_1738, int _arg_types_1739)
{
    int _safe_address_inlined_safe_address_at_13_1744 = NOVALUE;
    int _msg_inlined_crash_at_28_1748 = NOVALUE;
    int _749 = NOVALUE;
    int _748 = NOVALUE;
    int _746 = NOVALUE;
    int _745 = NOVALUE;
    int _744 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _744 = 0;
    if (_744 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_1744 = 1;
    _746 = (1 == 0);
    if (_746 == 0)
    {
        DeRef(_746);
        _746 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_746);
        _746 = NOVALUE;
    }

    /**         error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_1748);
    _msg_inlined_crash_at_28_1748 = EPrintf(-9999999, _747, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_1748);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_1748);
    _msg_inlined_crash_at_28_1748 = NOVALUE;
L1: 

    /** 	return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, 0})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_1737);
    *((int *)(_2+4)) = _lib_1737;
    Ref(_routine_name_1738);
    *((int *)(_2+8)) = _routine_name_1738;
    RefDS(_arg_types_1739);
    *((int *)(_2+12)) = _arg_types_1739;
    *((int *)(_2+16)) = 0;
    _748 = MAKE_SEQ(_1);
    _749 = machine(51, _748);
    DeRefDS(_748);
    _748 = NOVALUE;
    DeRef(_lib_1737);
    DeRef(_routine_name_1738);
    DeRefDSi(_arg_types_1739);
    return _749;
    ;
}


int _10define_c_func(int _lib_1753, int _routine_name_1754, int _arg_types_1755, int _return_type_1756)
{
    int _safe_address_inlined_safe_address_at_13_1761 = NOVALUE;
    int _msg_inlined_crash_at_28_1764 = NOVALUE;
    int _754 = NOVALUE;
    int _753 = NOVALUE;
    int _752 = NOVALUE;
    int _751 = NOVALUE;
    int _750 = NOVALUE;
    int _0, _1, _2;
    

    /** 	  if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _750 = 0;
    if (_750 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_1761 = 1;
    _752 = (1 == 0);
    if (_752 == 0)
    {
        DeRef(_752);
        _752 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_752);
        _752 = NOVALUE;
    }

    /** 	      error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_1764);
    _msg_inlined_crash_at_28_1764 = EPrintf(-9999999, _747, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_1764);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_1764);
    _msg_inlined_crash_at_28_1764 = NOVALUE;
L1: 

    /** 	  return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, return_type})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_1753);
    *((int *)(_2+4)) = _lib_1753;
    Ref(_routine_name_1754);
    *((int *)(_2+8)) = _routine_name_1754;
    RefDS(_arg_types_1755);
    *((int *)(_2+12)) = _arg_types_1755;
    *((int *)(_2+16)) = _return_type_1756;
    _753 = MAKE_SEQ(_1);
    _754 = machine(51, _753);
    DeRefDS(_753);
    _753 = NOVALUE;
    DeRef(_lib_1753);
    DeRefi(_routine_name_1754);
    DeRefDSi(_arg_types_1755);
    return _754;
    ;
}



// 0x99A6D8CF
