// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _29calc_primes(int _approx_limit_13261, int _time_limit_p_13262)
{
    int _result__13263 = NOVALUE;
    int _candidate__13264 = NOVALUE;
    int _pos__13265 = NOVALUE;
    int _time_out__13266 = NOVALUE;
    int _maxp__13267 = NOVALUE;
    int _maxf__13268 = NOVALUE;
    int _maxf_idx_13269 = NOVALUE;
    int _next_trigger_13270 = NOVALUE;
    int _growth_13271 = NOVALUE;
    int _7306 = NOVALUE;
    int _7303 = NOVALUE;
    int _7301 = NOVALUE;
    int _7298 = NOVALUE;
    int _7295 = NOVALUE;
    int _7292 = NOVALUE;
    int _7285 = NOVALUE;
    int _7283 = NOVALUE;
    int _7280 = NOVALUE;
    int _7277 = NOVALUE;
    int _7273 = NOVALUE;
    int _7272 = NOVALUE;
    int _7268 = NOVALUE;
    int _7262 = NOVALUE;
    int _7260 = NOVALUE;
    int _7258 = NOVALUE;
    int _7253 = NOVALUE;
    int _7252 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if approx_limit <= list_of_primes[$] then*/
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _7252 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _7252 = 1;
    }
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _7253 = (int)*(((s1_ptr)_2)->base + _7252);
    if (binary_op_a(GREATER, _approx_limit_13261, _7253)){
        _7253 = NOVALUE;
        goto L1; // [16] 65
    }
    _7253 = NOVALUE;

    /** 		pos_ = search:binary_search(approx_limit, list_of_primes)*/
    RefDS(_29list_of_primes_13257);
    _pos__13265 = _5binary_search(_approx_limit_13261, _29list_of_primes_13257, 1, 0);
    if (!IS_ATOM_INT(_pos__13265)) {
        _1 = (long)(DBL_PTR(_pos__13265)->dbl);
        if (UNIQUE(DBL_PTR(_pos__13265)) && (DBL_PTR(_pos__13265)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos__13265);
        _pos__13265 = _1;
    }

    /** 		if pos_ < 0 then*/
    if (_pos__13265 >= 0)
    goto L2; // [37] 51

    /** 			pos_ = (-pos_)*/
    _pos__13265 = - _pos__13265;
L2: 

    /** 		return list_of_primes[1..pos_]*/
    rhs_slice_target = (object_ptr)&_7258;
    RHS_Slice(_29list_of_primes_13257, 1, _pos__13265);
    DeRef(_result__13263);
    DeRef(_time_out__13266);
    return _7258;
L1: 

    /** 	pos_ = length(list_of_primes)*/
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _pos__13265 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _pos__13265 = 1;
    }

    /** 	candidate_ = list_of_primes[$]*/
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _7260 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _7260 = 1;
    }
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _candidate__13264 = (int)*(((s1_ptr)_2)->base + _7260);
    if (!IS_ATOM_INT(_candidate__13264))
    _candidate__13264 = (long)DBL_PTR(_candidate__13264)->dbl;

    /** 	maxf_ = floor(power(candidate_, 0.5))*/
    temp_d.dbl = (double)_candidate__13264;
    _7262 = Dpower(&temp_d, DBL_PTR(_2036));
    _maxf__13268 = unary_op(FLOOR, _7262);
    DeRefDS(_7262);
    _7262 = NOVALUE;
    if (!IS_ATOM_INT(_maxf__13268)) {
        _1 = (long)(DBL_PTR(_maxf__13268)->dbl);
        if (UNIQUE(DBL_PTR(_maxf__13268)) && (DBL_PTR(_maxf__13268)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf__13268);
        _maxf__13268 = _1;
    }

    /** 	maxf_idx = search:binary_search(maxf_, list_of_primes)*/
    RefDS(_29list_of_primes_13257);
    _maxf_idx_13269 = _5binary_search(_maxf__13268, _29list_of_primes_13257, 1, 0);
    if (!IS_ATOM_INT(_maxf_idx_13269)) {
        _1 = (long)(DBL_PTR(_maxf_idx_13269)->dbl);
        if (UNIQUE(DBL_PTR(_maxf_idx_13269)) && (DBL_PTR(_maxf_idx_13269)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf_idx_13269);
        _maxf_idx_13269 = _1;
    }

    /** 	if maxf_idx < 0 then*/
    if (_maxf_idx_13269 >= 0)
    goto L3; // [117] 141

    /** 		maxf_idx = (-maxf_idx)*/
    _maxf_idx_13269 = - _maxf_idx_13269;

    /** 		maxf_ = list_of_primes[maxf_idx]*/
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _maxf__13268 = (int)*(((s1_ptr)_2)->base + _maxf_idx_13269);
    if (!IS_ATOM_INT(_maxf__13268))
    _maxf__13268 = (long)DBL_PTR(_maxf__13268)->dbl;
L3: 

    /** 	next_trigger = list_of_primes[maxf_idx+1]*/
    _7268 = _maxf_idx_13269 + 1;
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _next_trigger_13270 = (int)*(((s1_ptr)_2)->base + _7268);
    if (!IS_ATOM_INT(_next_trigger_13270))
    _next_trigger_13270 = (long)DBL_PTR(_next_trigger_13270)->dbl;

    /** 	next_trigger *= next_trigger*/
    _next_trigger_13270 = _next_trigger_13270 * _next_trigger_13270;

    /** 	growth = floor(approx_limit  / 3.5) - length(list_of_primes)*/
    _2 = binary_op(DIVIDE, _approx_limit_13261, _7271);
    _7272 = unary_op(FLOOR, _2);
    DeRef(_2);
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _7273 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _7273 = 1;
    }
    if (IS_ATOM_INT(_7272)) {
        _growth_13271 = _7272 - _7273;
    }
    else {
        _growth_13271 = NewDouble(DBL_PTR(_7272)->dbl - (double)_7273);
    }
    DeRef(_7272);
    _7272 = NOVALUE;
    _7273 = NOVALUE;
    if (!IS_ATOM_INT(_growth_13271)) {
        _1 = (long)(DBL_PTR(_growth_13271)->dbl);
        if (UNIQUE(DBL_PTR(_growth_13271)) && (DBL_PTR(_growth_13271)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_growth_13271);
        _growth_13271 = _1;
    }

    /** 	if growth <= 0 then*/
    if (_growth_13271 > 0)
    goto L4; // [186] 200

    /** 		growth = length(list_of_primes)*/
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _growth_13271 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _growth_13271 = 1;
    }
L4: 

    /** 	result_ = list_of_primes & repeat(0, growth)*/
    _7277 = Repeat(0, _growth_13271);
    Concat((object_ptr)&_result__13263, _29list_of_primes_13257, _7277);
    DeRefDS(_7277);
    _7277 = NOVALUE;

    /** 	if time_limit_p < 0 then*/
    if (_time_limit_p_13262 >= 0)
    goto L5; // [214] 229

    /** 		time_out_ = time() + 100_000_000*/
    DeRef(_7280);
    _7280 = NewDouble(current_time());
    DeRef(_time_out__13266);
    _time_out__13266 = NewDouble(DBL_PTR(_7280)->dbl + (double)100000000);
    DeRefDS(_7280);
    _7280 = NOVALUE;
    goto L6; // [226] 238
L5: 

    /** 		time_out_ = time() + time_limit_p*/
    DeRef(_7283);
    _7283 = NewDouble(current_time());
    DeRef(_time_out__13266);
    _time_out__13266 = NewDouble(DBL_PTR(_7283)->dbl + (double)_time_limit_p_13262);
    DeRefDS(_7283);
    _7283 = NOVALUE;
L6: 

    /** 	while time_out_ >= time()  label "MW" do*/
L7: 
    DeRef(_7285);
    _7285 = NewDouble(current_time());
    if (binary_op_a(LESS, _time_out__13266, _7285)){
        DeRefDS(_7285);
        _7285 = NOVALUE;
        goto L8; // [247] 410
    }
    DeRef(_7285);
    _7285 = NOVALUE;

    /** 		task_yield()*/
    task_yield();

    /** 		candidate_ += 2*/
    _candidate__13264 = _candidate__13264 + 2;

    /** 		if candidate_ >= next_trigger then*/
    if (_candidate__13264 < _next_trigger_13270)
    goto L9; // [264] 307

    /** 			maxf_idx += 1*/
    _maxf_idx_13269 = _maxf_idx_13269 + 1;

    /** 			maxf_ = result_[maxf_idx]*/
    _2 = (int)SEQ_PTR(_result__13263);
    _maxf__13268 = (int)*(((s1_ptr)_2)->base + _maxf_idx_13269);
    if (!IS_ATOM_INT(_maxf__13268))
    _maxf__13268 = (long)DBL_PTR(_maxf__13268)->dbl;

    /** 			next_trigger = result_[maxf_idx+1]*/
    _7292 = _maxf_idx_13269 + 1;
    _2 = (int)SEQ_PTR(_result__13263);
    _next_trigger_13270 = (int)*(((s1_ptr)_2)->base + _7292);
    if (!IS_ATOM_INT(_next_trigger_13270))
    _next_trigger_13270 = (long)DBL_PTR(_next_trigger_13270)->dbl;

    /** 			next_trigger *= next_trigger*/
    _next_trigger_13270 = _next_trigger_13270 * _next_trigger_13270;
L9: 

    /** 		for i = 2 to pos_ do*/
    _7295 = _pos__13265;
    {
        int _i_13324;
        _i_13324 = 2;
LA: 
        if (_i_13324 > _7295){
            goto LB; // [312] 360
        }

        /** 			maxp_ = result_[i]*/
        _2 = (int)SEQ_PTR(_result__13263);
        _maxp__13267 = (int)*(((s1_ptr)_2)->base + _i_13324);
        if (!IS_ATOM_INT(_maxp__13267))
        _maxp__13267 = (long)DBL_PTR(_maxp__13267)->dbl;

        /** 			if maxp_ > maxf_ then*/
        if (_maxp__13267 <= _maxf__13268)
        goto LC; // [329] 338

        /** 				exit*/
        goto LB; // [335] 360
LC: 

        /** 			if remainder(candidate_, maxp_) = 0 then*/
        _7298 = (_candidate__13264 % _maxp__13267);
        if (_7298 != 0)
        goto LD; // [344] 353

        /** 				continue "MW"*/
        goto L7; // [350] 243
LD: 

        /** 		end for*/
        _i_13324 = _i_13324 + 1;
        goto LA; // [355] 319
LB: 
        ;
    }

    /** 		pos_ += 1*/
    _pos__13265 = _pos__13265 + 1;

    /** 		if pos_ >= length(result_) then*/
    if (IS_SEQUENCE(_result__13263)){
            _7301 = SEQ_PTR(_result__13263)->length;
    }
    else {
        _7301 = 1;
    }
    if (_pos__13265 < _7301)
    goto LE; // [373] 388

    /** 			result_ &= repeat(0, 1000)*/
    _7303 = Repeat(0, 1000);
    Concat((object_ptr)&_result__13263, _result__13263, _7303);
    DeRefDS(_7303);
    _7303 = NOVALUE;
LE: 

    /** 		result_[pos_] = candidate_*/
    _2 = (int)SEQ_PTR(_result__13263);
    _2 = (int)(((s1_ptr)_2)->base + _pos__13265);
    _1 = *(int *)_2;
    *(int *)_2 = _candidate__13264;
    DeRef(_1);

    /** 		if candidate_ >= approx_limit then*/
    if (_candidate__13264 < _approx_limit_13261)
    goto L7; // [396] 243

    /** 			exit*/
    goto L8; // [402] 410

    /** 	end while*/
    goto L7; // [407] 243
L8: 

    /** 	return result_[1..pos_]*/
    rhs_slice_target = (object_ptr)&_7306;
    RHS_Slice(_result__13263, 1, _pos__13265);
    DeRefDS(_result__13263);
    DeRef(_time_out__13266);
    DeRef(_7258);
    _7258 = NOVALUE;
    DeRef(_7268);
    _7268 = NOVALUE;
    DeRef(_7292);
    _7292 = NOVALUE;
    DeRef(_7298);
    _7298 = NOVALUE;
    return _7306;
    ;
}


int _29next_prime(int _n_13343, int _fail_signal_p_13344, int _time_out_p_13345)
{
    int _i_13346 = NOVALUE;
    int _7326 = NOVALUE;
    int _7320 = NOVALUE;
    int _7319 = NOVALUE;
    int _7318 = NOVALUE;
    int _7317 = NOVALUE;
    int _7316 = NOVALUE;
    int _7313 = NOVALUE;
    int _7312 = NOVALUE;
    int _7309 = NOVALUE;
    int _7308 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if n < 0 then*/
    if (_n_13343 >= 0)
    goto L1; // [7] 18

    /** 		return fail_signal_p*/
    return _fail_signal_p_13344;
L1: 

    /** 	if list_of_primes[$] < n then*/
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _7308 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _7308 = 1;
    }
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _7309 = (int)*(((s1_ptr)_2)->base + _7308);
    if (binary_op_a(GREATEREQ, _7309, _n_13343)){
        _7309 = NOVALUE;
        goto L2; // [29] 43
    }
    _7309 = NOVALUE;

    /** 		list_of_primes = calc_primes(n,time_out_p)*/
    _0 = _29calc_primes(_n_13343, _time_out_p_13345);
    DeRefDS(_29list_of_primes_13257);
    _29list_of_primes_13257 = _0;
L2: 

    /** 	if n > list_of_primes[$] then*/
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _7312 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _7312 = 1;
    }
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _7313 = (int)*(((s1_ptr)_2)->base + _7312);
    if (binary_op_a(LESSEQ, _n_13343, _7313)){
        _7313 = NOVALUE;
        goto L3; // [54] 65
    }
    _7313 = NOVALUE;

    /** 		return fail_signal_p*/
    return _fail_signal_p_13344;
L3: 

    /** 	if n < 1009 and 1009 <= list_of_primes[$] then*/
    _7316 = (_n_13343 < 1009);
    if (_7316 == 0) {
        goto L4; // [71] 110
    }
    if (IS_SEQUENCE(_29list_of_primes_13257)){
            _7318 = SEQ_PTR(_29list_of_primes_13257)->length;
    }
    else {
        _7318 = 1;
    }
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _7319 = (int)*(((s1_ptr)_2)->base + _7318);
    if (IS_ATOM_INT(_7319)) {
        _7320 = (1009 <= _7319);
    }
    else {
        _7320 = binary_op(LESSEQ, 1009, _7319);
    }
    _7319 = NOVALUE;
    if (_7320 == 0) {
        DeRef(_7320);
        _7320 = NOVALUE;
        goto L4; // [89] 110
    }
    else {
        if (!IS_ATOM_INT(_7320) && DBL_PTR(_7320)->dbl == 0.0){
            DeRef(_7320);
            _7320 = NOVALUE;
            goto L4; // [89] 110
        }
        DeRef(_7320);
        _7320 = NOVALUE;
    }
    DeRef(_7320);
    _7320 = NOVALUE;

    /** 		i = search:binary_search(n, list_of_primes, ,169)*/
    RefDS(_29list_of_primes_13257);
    _i_13346 = _5binary_search(_n_13343, _29list_of_primes_13257, 1, 169);
    if (!IS_ATOM_INT(_i_13346)) {
        _1 = (long)(DBL_PTR(_i_13346)->dbl);
        if (UNIQUE(DBL_PTR(_i_13346)) && (DBL_PTR(_i_13346)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_13346);
        _i_13346 = _1;
    }
    goto L5; // [107] 126
L4: 

    /** 		i = search:binary_search(n, list_of_primes)*/
    RefDS(_29list_of_primes_13257);
    _i_13346 = _5binary_search(_n_13343, _29list_of_primes_13257, 1, 0);
    if (!IS_ATOM_INT(_i_13346)) {
        _1 = (long)(DBL_PTR(_i_13346)->dbl);
        if (UNIQUE(DBL_PTR(_i_13346)) && (DBL_PTR(_i_13346)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_13346);
        _i_13346 = _1;
    }
L5: 

    /** 	if i < 0 then*/
    if (_i_13346 >= 0)
    goto L6; // [130] 144

    /** 		i = (-i)*/
    _i_13346 = - _i_13346;
L6: 

    /** 	return list_of_primes[i]*/
    _2 = (int)SEQ_PTR(_29list_of_primes_13257);
    _7326 = (int)*(((s1_ptr)_2)->base + _i_13346);
    Ref(_7326);
    DeRef(_fail_signal_p_13344);
    DeRef(_7316);
    _7316 = NOVALUE;
    return _7326;
    ;
}



// 0x22556F93
