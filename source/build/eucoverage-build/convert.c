// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _4int_to_bytes(int _x_10450)
{
    int _a_10451 = NOVALUE;
    int _b_10452 = NOVALUE;
    int _c_10453 = NOVALUE;
    int _d_10454 = NOVALUE;
    int _5949 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_10450)) {
        _a_10451 = (_x_10450 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _a_10451 = Dremainder(DBL_PTR(_x_10450), &temp_d);
    }
    if (!IS_ATOM_INT(_a_10451)) {
        _1 = (long)(DBL_PTR(_a_10451)->dbl);
        if (UNIQUE(DBL_PTR(_a_10451)) && (DBL_PTR(_a_10451)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_10451);
        _a_10451 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_10450;
    if (IS_ATOM_INT(_x_10450)) {
        if (256 > 0 && _x_10450 >= 0) {
            _x_10450 = _x_10450 / 256;
        }
        else {
            temp_dbl = floor((double)_x_10450 / (double)256);
            if (_x_10450 != MININT)
            _x_10450 = (long)temp_dbl;
            else
            _x_10450 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_10450, 256);
        _x_10450 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	b = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_10450)) {
        _b_10452 = (_x_10450 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _b_10452 = Dremainder(DBL_PTR(_x_10450), &temp_d);
    }
    if (!IS_ATOM_INT(_b_10452)) {
        _1 = (long)(DBL_PTR(_b_10452)->dbl);
        if (UNIQUE(DBL_PTR(_b_10452)) && (DBL_PTR(_b_10452)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_10452);
        _b_10452 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_10450;
    if (IS_ATOM_INT(_x_10450)) {
        if (256 > 0 && _x_10450 >= 0) {
            _x_10450 = _x_10450 / 256;
        }
        else {
            temp_dbl = floor((double)_x_10450 / (double)256);
            if (_x_10450 != MININT)
            _x_10450 = (long)temp_dbl;
            else
            _x_10450 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_10450, 256);
        _x_10450 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	c = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_10450)) {
        _c_10453 = (_x_10450 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _c_10453 = Dremainder(DBL_PTR(_x_10450), &temp_d);
    }
    if (!IS_ATOM_INT(_c_10453)) {
        _1 = (long)(DBL_PTR(_c_10453)->dbl);
        if (UNIQUE(DBL_PTR(_c_10453)) && (DBL_PTR(_c_10453)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_10453);
        _c_10453 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_10450;
    if (IS_ATOM_INT(_x_10450)) {
        if (256 > 0 && _x_10450 >= 0) {
            _x_10450 = _x_10450 / 256;
        }
        else {
            temp_dbl = floor((double)_x_10450 / (double)256);
            if (_x_10450 != MININT)
            _x_10450 = (long)temp_dbl;
            else
            _x_10450 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_10450, 256);
        _x_10450 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	d = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_10450)) {
        _d_10454 = (_x_10450 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _d_10454 = Dremainder(DBL_PTR(_x_10450), &temp_d);
    }
    if (!IS_ATOM_INT(_d_10454)) {
        _1 = (long)(DBL_PTR(_d_10454)->dbl);
        if (UNIQUE(DBL_PTR(_d_10454)) && (DBL_PTR(_d_10454)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_d_10454);
        _d_10454 = _1;
    }

    /** 	return {a,b,c,d}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _a_10451;
    *((int *)(_2+8)) = _b_10452;
    *((int *)(_2+12)) = _c_10453;
    *((int *)(_2+16)) = _d_10454;
    _5949 = MAKE_SEQ(_1);
    DeRef(_x_10450);
    return _5949;
    ;
}


int _4int_to_bits(int _x_10491, int _nbits_10492)
{
    int _bits_10493 = NOVALUE;
    int _mask_10494 = NOVALUE;
    int _5975 = NOVALUE;
    int _5974 = NOVALUE;
    int _5972 = NOVALUE;
    int _5969 = NOVALUE;
    int _5968 = NOVALUE;
    int _5967 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if nbits < 1 then*/

    /** 	bits = repeat(0, nbits)*/
    DeRef(_bits_10493);
    _bits_10493 = Repeat(0, 32);

    /** 	if nbits <= 32 then*/

    /** 		mask = 1*/
    DeRef(_mask_10494);
    _mask_10494 = 1;

    /** 		for i = 1 to nbits do*/
    _5967 = 32;
    {
        int _i_10501;
        _i_10501 = 1;
L1: 
        if (_i_10501 > 32){
            goto L2; // [40] 74
        }

        /** 			bits[i] = and_bits(x, mask) and 1*/
        if (IS_ATOM_INT(_x_10491) && IS_ATOM_INT(_mask_10494)) {
            {unsigned long tu;
                 tu = (unsigned long)_x_10491 & (unsigned long)_mask_10494;
                 _5968 = MAKE_UINT(tu);
            }
        }
        else {
            if (IS_ATOM_INT(_x_10491)) {
                temp_d.dbl = (double)_x_10491;
                _5968 = Dand_bits(&temp_d, DBL_PTR(_mask_10494));
            }
            else {
                if (IS_ATOM_INT(_mask_10494)) {
                    temp_d.dbl = (double)_mask_10494;
                    _5968 = Dand_bits(DBL_PTR(_x_10491), &temp_d);
                }
                else
                _5968 = Dand_bits(DBL_PTR(_x_10491), DBL_PTR(_mask_10494));
            }
        }
        if (IS_ATOM_INT(_5968)) {
            _5969 = (_5968 != 0 && 1 != 0);
        }
        else {
            temp_d.dbl = (double)1;
            _5969 = Dand(DBL_PTR(_5968), &temp_d);
        }
        DeRef(_5968);
        _5968 = NOVALUE;
        _2 = (int)SEQ_PTR(_bits_10493);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_10493 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_10501);
        _1 = *(int *)_2;
        *(int *)_2 = _5969;
        if( _1 != _5969 ){
            DeRef(_1);
        }
        _5969 = NOVALUE;

        /** 			mask *= 2*/
        _0 = _mask_10494;
        if (IS_ATOM_INT(_mask_10494) && IS_ATOM_INT(_mask_10494)) {
            _mask_10494 = _mask_10494 + _mask_10494;
            if ((long)((unsigned long)_mask_10494 + (unsigned long)HIGH_BITS) >= 0) 
            _mask_10494 = NewDouble((double)_mask_10494);
        }
        else {
            if (IS_ATOM_INT(_mask_10494)) {
                _mask_10494 = NewDouble((double)_mask_10494 + DBL_PTR(_mask_10494)->dbl);
            }
            else {
                if (IS_ATOM_INT(_mask_10494)) {
                    _mask_10494 = NewDouble(DBL_PTR(_mask_10494)->dbl + (double)_mask_10494);
                }
                else
                _mask_10494 = NewDouble(DBL_PTR(_mask_10494)->dbl + DBL_PTR(_mask_10494)->dbl);
            }
        }
        DeRef(_0);

        /** 		end for*/
        _i_10501 = _i_10501 + 1;
        goto L1; // [69] 47
L2: 
        ;
    }
    goto L3; // [74] 130

    /** 		if x < 0 then*/
    if (binary_op_a(GREATEREQ, _x_10491, 0)){
        goto L4; // [79] 94
    }

    /** 			x += power(2, nbits) -- for 2's complement bit pattern*/
    _5972 = power(2, _nbits_10492);
    _0 = _x_10491;
    if (IS_ATOM_INT(_x_10491) && IS_ATOM_INT(_5972)) {
        _x_10491 = _x_10491 + _5972;
        if ((long)((unsigned long)_x_10491 + (unsigned long)HIGH_BITS) >= 0) 
        _x_10491 = NewDouble((double)_x_10491);
    }
    else {
        if (IS_ATOM_INT(_x_10491)) {
            _x_10491 = NewDouble((double)_x_10491 + DBL_PTR(_5972)->dbl);
        }
        else {
            if (IS_ATOM_INT(_5972)) {
                _x_10491 = NewDouble(DBL_PTR(_x_10491)->dbl + (double)_5972);
            }
            else
            _x_10491 = NewDouble(DBL_PTR(_x_10491)->dbl + DBL_PTR(_5972)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_5972);
    _5972 = NOVALUE;
L4: 

    /** 		for i = 1 to nbits do*/
    _5974 = _nbits_10492;
    {
        int _i_10512;
        _i_10512 = 1;
L5: 
        if (_i_10512 > _5974){
            goto L6; // [99] 129
        }

        /** 			bits[i] = remainder(x, 2)*/
        if (IS_ATOM_INT(_x_10491)) {
            _5975 = (_x_10491 % 2);
        }
        else {
            temp_d.dbl = (double)2;
            _5975 = Dremainder(DBL_PTR(_x_10491), &temp_d);
        }
        _2 = (int)SEQ_PTR(_bits_10493);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_10493 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_10512);
        _1 = *(int *)_2;
        *(int *)_2 = _5975;
        if( _1 != _5975 ){
            DeRef(_1);
        }
        _5975 = NOVALUE;

        /** 			x = floor(x / 2)*/
        _0 = _x_10491;
        if (IS_ATOM_INT(_x_10491)) {
            _x_10491 = _x_10491 >> 1;
        }
        else {
            _1 = binary_op(DIVIDE, _x_10491, 2);
            _x_10491 = unary_op(FLOOR, _1);
            DeRef(_1);
        }
        DeRef(_0);

        /** 		end for*/
        _i_10512 = _i_10512 + 1;
        goto L5; // [124] 106
L6: 
        ;
    }
L3: 

    /** 	return bits*/
    DeRef(_x_10491);
    DeRef(_mask_10494);
    return _bits_10493;
    ;
}


int _4atom_to_float64(int _a_10530)
{
    int _5981 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_A_TO_F64, a)*/
    _5981 = machine(46, _a_10530);
    DeRef(_a_10530);
    return _5981;
    ;
}


int _4atom_to_float32(int _a_10534)
{
    int _5982 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_A_TO_F32, a)*/
    _5982 = machine(48, _a_10534);
    DeRef(_a_10534);
    return _5982;
    ;
}


int _4float64_to_atom(int _ieee64_10538)
{
    int _5983 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F64_TO_A, ieee64)*/
    _5983 = machine(47, _ieee64_10538);
    DeRefDS(_ieee64_10538);
    return _5983;
    ;
}


int _4float32_to_atom(int _ieee32_10542)
{
    int _5984 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    _5984 = machine(49, _ieee32_10542);
    DeRefDS(_ieee32_10542);
    return _5984;
    ;
}


int _4to_number(int _text_in_10618, int _return_bad_pos_10619)
{
    int _lDotFound_10620 = NOVALUE;
    int _lSignFound_10621 = NOVALUE;
    int _lCharValue_10622 = NOVALUE;
    int _lBadPos_10623 = NOVALUE;
    int _lLeftSize_10624 = NOVALUE;
    int _lRightSize_10625 = NOVALUE;
    int _lLeftValue_10626 = NOVALUE;
    int _lRightValue_10627 = NOVALUE;
    int _lBase_10628 = NOVALUE;
    int _lPercent_10629 = NOVALUE;
    int _lResult_10630 = NOVALUE;
    int _lDigitCount_10631 = NOVALUE;
    int _lCurrencyFound_10632 = NOVALUE;
    int _lLastDigit_10633 = NOVALUE;
    int _lChar_10634 = NOVALUE;
    int _6095 = NOVALUE;
    int _6094 = NOVALUE;
    int _6087 = NOVALUE;
    int _6085 = NOVALUE;
    int _6084 = NOVALUE;
    int _6079 = NOVALUE;
    int _6078 = NOVALUE;
    int _6077 = NOVALUE;
    int _6076 = NOVALUE;
    int _6075 = NOVALUE;
    int _6074 = NOVALUE;
    int _6070 = NOVALUE;
    int _6066 = NOVALUE;
    int _6058 = NOVALUE;
    int _6053 = NOVALUE;
    int _6052 = NOVALUE;
    int _6047 = NOVALUE;
    int _6045 = NOVALUE;
    int _6039 = NOVALUE;
    int _6038 = NOVALUE;
    int _6037 = NOVALUE;
    int _6036 = NOVALUE;
    int _6035 = NOVALUE;
    int _6034 = NOVALUE;
    int _6033 = NOVALUE;
    int _6032 = NOVALUE;
    int _6031 = NOVALUE;
    int _6024 = NOVALUE;
    int _6023 = NOVALUE;
    int _6022 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lDotFound = 0*/
    _lDotFound_10620 = 0;

    /** 	integer lSignFound = 2*/
    _lSignFound_10621 = 2;

    /** 	integer lBadPos = 0*/
    _lBadPos_10623 = 0;

    /** 	atom    lLeftSize = 0*/
    DeRef(_lLeftSize_10624);
    _lLeftSize_10624 = 0;

    /** 	atom    lRightSize = 1*/
    DeRef(_lRightSize_10625);
    _lRightSize_10625 = 1;

    /** 	atom    lLeftValue = 0*/
    DeRef(_lLeftValue_10626);
    _lLeftValue_10626 = 0;

    /** 	atom    lRightValue = 0*/
    DeRef(_lRightValue_10627);
    _lRightValue_10627 = 0;

    /** 	integer lBase = 10*/
    _lBase_10628 = 10;

    /** 	integer lPercent = 1*/
    _lPercent_10629 = 1;

    /** 	integer lDigitCount = 0*/
    _lDigitCount_10631 = 0;

    /** 	integer lCurrencyFound = 0*/
    _lCurrencyFound_10632 = 0;

    /** 	integer lLastDigit = 0*/
    _lLastDigit_10633 = 0;

    /** 	for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_10618)){
            _6022 = SEQ_PTR(_text_in_10618)->length;
    }
    else {
        _6022 = 1;
    }
    {
        int _i_10636;
        _i_10636 = 1;
L1: 
        if (_i_10636 > _6022){
            goto L2; // [88] 754
        }

        /** 		if not integer(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_10618);
        _6023 = (int)*(((s1_ptr)_2)->base + _i_10636);
        if (IS_ATOM_INT(_6023))
        _6024 = 1;
        else if (IS_ATOM_DBL(_6023))
        _6024 = IS_ATOM_INT(DoubleToInt(_6023));
        else
        _6024 = 0;
        _6023 = NOVALUE;
        if (_6024 != 0)
        goto L3; // [104] 112
        _6024 = NOVALUE;

        /** 			exit*/
        goto L2; // [109] 754
L3: 

        /** 		lChar = text_in[i]*/
        _2 = (int)SEQ_PTR(_text_in_10618);
        _lChar_10634 = (int)*(((s1_ptr)_2)->base + _i_10636);
        if (!IS_ATOM_INT(_lChar_10634))
        _lChar_10634 = (long)DBL_PTR(_lChar_10634)->dbl;

        /** 		switch lChar do*/
        _0 = _lChar_10634;
        switch ( _0 ){ 

            /** 			case '-' then*/
            case 45:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_10621 != 2)
            goto L4; // [133] 154

            /** 					lSignFound = -1*/
            _lSignFound_10621 = -1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_10633 = _lDigitCount_10631;
            goto L5; // [151] 736
L4: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [162] 736

            /** 			case '+' then*/
            case 43:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_10621 != 2)
            goto L6; // [170] 191

            /** 					lSignFound = 1*/
            _lSignFound_10621 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_10633 = _lDigitCount_10631;
            goto L5; // [188] 736
L6: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [199] 736

            /** 			case '#' then*/
            case 35:

            /** 				if lDigitCount = 0 and lBase = 10 then*/
            _6031 = (_lDigitCount_10631 == 0);
            if (_6031 == 0) {
                goto L7; // [211] 233
            }
            _6033 = (_lBase_10628 == 10);
            if (_6033 == 0)
            {
                DeRef(_6033);
                _6033 = NOVALUE;
                goto L7; // [220] 233
            }
            else{
                DeRef(_6033);
                _6033 = NOVALUE;
            }

            /** 					lBase = 16*/
            _lBase_10628 = 16;
            goto L5; // [230] 736
L7: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [241] 736

            /** 			case '@' then*/
            case 64:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _6034 = (_lDigitCount_10631 == 0);
            if (_6034 == 0) {
                goto L8; // [253] 275
            }
            _6036 = (_lBase_10628 == 10);
            if (_6036 == 0)
            {
                DeRef(_6036);
                _6036 = NOVALUE;
                goto L8; // [262] 275
            }
            else{
                DeRef(_6036);
                _6036 = NOVALUE;
            }

            /** 					lBase = 8*/
            _lBase_10628 = 8;
            goto L5; // [272] 736
L8: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [283] 736

            /** 			case '!' then*/
            case 33:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _6037 = (_lDigitCount_10631 == 0);
            if (_6037 == 0) {
                goto L9; // [295] 317
            }
            _6039 = (_lBase_10628 == 10);
            if (_6039 == 0)
            {
                DeRef(_6039);
                _6039 = NOVALUE;
                goto L9; // [304] 317
            }
            else{
                DeRef(_6039);
                _6039 = NOVALUE;
            }

            /** 					lBase = 2*/
            _lBase_10628 = 2;
            goto L5; // [314] 736
L9: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [325] 736

            /** 			case '$', '�', '�', '�', '�' then*/
            case 36:
            case 163:
            case 164:
            case 165:
            case 128:

            /** 				if lCurrencyFound = 0 then*/
            if (_lCurrencyFound_10632 != 0)
            goto LA; // [341] 362

            /** 					lCurrencyFound = 1*/
            _lCurrencyFound_10632 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_10633 = _lDigitCount_10631;
            goto L5; // [359] 736
LA: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [370] 736

            /** 			case '_' then -- grouping character*/
            case 95:

            /** 				if lDigitCount = 0 or lLastDigit != 0 then*/
            _6045 = (_lDigitCount_10631 == 0);
            if (_6045 != 0) {
                goto LB; // [382] 395
            }
            _6047 = (_lLastDigit_10633 != 0);
            if (_6047 == 0)
            {
                DeRef(_6047);
                _6047 = NOVALUE;
                goto L5; // [391] 736
            }
            else{
                DeRef(_6047);
                _6047 = NOVALUE;
            }
LB: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [403] 736

            /** 			case '.', ',' then*/
            case 46:
            case 44:

            /** 				if lLastDigit = 0 then*/
            if (_lLastDigit_10633 != 0)
            goto LC; // [413] 456

            /** 					if decimal_mark = lChar then*/
            if (46 != _lChar_10634)
            goto L5; // [421] 736

            /** 						if lDotFound = 0 then*/
            if (_lDotFound_10620 != 0)
            goto LD; // [427] 441

            /** 							lDotFound = 1*/
            _lDotFound_10620 = 1;
            goto L5; // [438] 736
LD: 

            /** 							lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [449] 736
            goto L5; // [453] 736
LC: 

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [464] 736

            /** 			case '%' then*/
            case 37:

            /** 				lLastDigit = lDigitCount*/
            _lLastDigit_10633 = _lDigitCount_10631;

            /** 				if lPercent = 1 then*/
            if (_lPercent_10629 != 1)
            goto LE; // [479] 493

            /** 					lPercent = 100*/
            _lPercent_10629 = 100;
            goto L5; // [490] 736
LE: 

            /** 					if text_in[i-1] = '%' then*/
            _6052 = _i_10636 - 1;
            _2 = (int)SEQ_PTR(_text_in_10618);
            _6053 = (int)*(((s1_ptr)_2)->base + _6052);
            if (binary_op_a(NOTEQ, _6053, 37)){
                _6053 = NOVALUE;
                goto LF; // [503] 520
            }
            _6053 = NOVALUE;

            /** 						lPercent *= 10 -- Yes ten not one hundred.*/
            _lPercent_10629 = _lPercent_10629 * 10;
            goto L5; // [517] 736
LF: 

            /** 						lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [529] 736

            /** 			case '\t', ' ', #A0 then*/
            case 9:
            case 32:
            case 160:

            /** 				if lDigitCount = 0 then*/
            if (_lDigitCount_10631 != 0)
            goto L10; // [541] 548
            goto L5; // [545] 736
L10: 

            /** 					lLastDigit = i*/
            _lLastDigit_10633 = _i_10636;
            goto L5; // [556] 736

            /** 			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',*/
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:

            /** 	            lCharValue = find(lChar, vDigits) - 1*/
            _6058 = find_from(_lChar_10634, _4vDigits_10605, 1);
            _lCharValue_10622 = _6058 - 1;
            _6058 = NOVALUE;

            /** 	            if lCharValue > 15 then*/
            if (_lCharValue_10622 <= 15)
            goto L11; // [619] 632

            /** 	            	lCharValue -= 6*/
            _lCharValue_10622 = _lCharValue_10622 - 6;
L11: 

            /** 	            if lCharValue >= lBase then*/
            if (_lCharValue_10622 < _lBase_10628)
            goto L12; // [634] 648

            /** 	                lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [645] 736
L12: 

            /** 	            elsif lLastDigit != 0 then  -- shouldn't be any more digits*/
            if (_lLastDigit_10633 == 0)
            goto L13; // [650] 664

            /** 					lBadPos = i*/
            _lBadPos_10623 = _i_10636;
            goto L5; // [661] 736
L13: 

            /** 				elsif lDotFound = 1 then*/
            if (_lDotFound_10620 != 1)
            goto L14; // [666] 697

            /** 					lRightSize *= lBase*/
            _0 = _lRightSize_10625;
            if (IS_ATOM_INT(_lRightSize_10625)) {
                if (_lRightSize_10625 == (short)_lRightSize_10625 && _lBase_10628 <= INT15 && _lBase_10628 >= -INT15)
                _lRightSize_10625 = _lRightSize_10625 * _lBase_10628;
                else
                _lRightSize_10625 = NewDouble(_lRightSize_10625 * (double)_lBase_10628);
            }
            else {
                _lRightSize_10625 = NewDouble(DBL_PTR(_lRightSize_10625)->dbl * (double)_lBase_10628);
            }
            DeRef(_0);

            /** 					lRightValue = (lRightValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lRightValue_10627)) {
                if (_lRightValue_10627 == (short)_lRightValue_10627 && _lBase_10628 <= INT15 && _lBase_10628 >= -INT15)
                _6066 = _lRightValue_10627 * _lBase_10628;
                else
                _6066 = NewDouble(_lRightValue_10627 * (double)_lBase_10628);
            }
            else {
                _6066 = NewDouble(DBL_PTR(_lRightValue_10627)->dbl * (double)_lBase_10628);
            }
            DeRef(_lRightValue_10627);
            if (IS_ATOM_INT(_6066)) {
                _lRightValue_10627 = _6066 + _lCharValue_10622;
                if ((long)((unsigned long)_lRightValue_10627 + (unsigned long)HIGH_BITS) >= 0) 
                _lRightValue_10627 = NewDouble((double)_lRightValue_10627);
            }
            else {
                _lRightValue_10627 = NewDouble(DBL_PTR(_6066)->dbl + (double)_lCharValue_10622);
            }
            DeRef(_6066);
            _6066 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_10631 = _lDigitCount_10631 + 1;
            goto L5; // [694] 736
L14: 

            /** 					lLeftSize += 1*/
            _0 = _lLeftSize_10624;
            if (IS_ATOM_INT(_lLeftSize_10624)) {
                _lLeftSize_10624 = _lLeftSize_10624 + 1;
                if (_lLeftSize_10624 > MAXINT){
                    _lLeftSize_10624 = NewDouble((double)_lLeftSize_10624);
                }
            }
            else
            _lLeftSize_10624 = binary_op(PLUS, 1, _lLeftSize_10624);
            DeRef(_0);

            /** 					lLeftValue = (lLeftValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lLeftValue_10626)) {
                if (_lLeftValue_10626 == (short)_lLeftValue_10626 && _lBase_10628 <= INT15 && _lBase_10628 >= -INT15)
                _6070 = _lLeftValue_10626 * _lBase_10628;
                else
                _6070 = NewDouble(_lLeftValue_10626 * (double)_lBase_10628);
            }
            else {
                _6070 = NewDouble(DBL_PTR(_lLeftValue_10626)->dbl * (double)_lBase_10628);
            }
            DeRef(_lLeftValue_10626);
            if (IS_ATOM_INT(_6070)) {
                _lLeftValue_10626 = _6070 + _lCharValue_10622;
                if ((long)((unsigned long)_lLeftValue_10626 + (unsigned long)HIGH_BITS) >= 0) 
                _lLeftValue_10626 = NewDouble((double)_lLeftValue_10626);
            }
            else {
                _lLeftValue_10626 = NewDouble(DBL_PTR(_6070)->dbl + (double)_lCharValue_10622);
            }
            DeRef(_6070);
            _6070 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_10631 = _lDigitCount_10631 + 1;
            goto L5; // [722] 736

            /** 			case else*/
            default:

            /** 				lBadPos = i*/
            _lBadPos_10623 = _i_10636;
        ;}L5: 

        /** 		if lBadPos != 0 then*/
        if (_lBadPos_10623 == 0)
        goto L15; // [738] 747

        /** 			exit*/
        goto L2; // [744] 754
L15: 

        /** 	end for*/
        _i_10636 = _i_10636 + 1;
        goto L1; // [749] 95
L2: 
        ;
    }

    /** 	if lBadPos = 0 and lDigitCount = 0 then*/
    _6074 = (_lBadPos_10623 == 0);
    if (_6074 == 0) {
        goto L16; // [760] 780
    }
    _6076 = (_lDigitCount_10631 == 0);
    if (_6076 == 0)
    {
        DeRef(_6076);
        _6076 = NOVALUE;
        goto L16; // [769] 780
    }
    else{
        DeRef(_6076);
        _6076 = NOVALUE;
    }

    /** 		lBadPos = 1*/
    _lBadPos_10623 = 1;
L16: 

    /** 	if return_bad_pos = 0 and lBadPos != 0 then*/
    _6077 = (_return_bad_pos_10619 == 0);
    if (_6077 == 0) {
        goto L17; // [786] 805
    }
    _6079 = (_lBadPos_10623 != 0);
    if (_6079 == 0)
    {
        DeRef(_6079);
        _6079 = NOVALUE;
        goto L17; // [795] 805
    }
    else{
        DeRef(_6079);
        _6079 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_text_in_10618);
    DeRef(_lLeftSize_10624);
    DeRef(_lRightSize_10625);
    DeRef(_lLeftValue_10626);
    DeRef(_lRightValue_10627);
    DeRef(_lResult_10630);
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6034);
    _6034 = NOVALUE;
    DeRef(_6037);
    _6037 = NOVALUE;
    DeRef(_6045);
    _6045 = NOVALUE;
    DeRef(_6052);
    _6052 = NOVALUE;
    DeRef(_6074);
    _6074 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    return 0;
L17: 

    /** 	if lRightValue = 0 then*/
    if (binary_op_a(NOTEQ, _lRightValue_10627, 0)){
        goto L18; // [807] 835
    }

    /** 	    if lPercent != 1 then*/
    if (_lPercent_10629 == 1)
    goto L19; // [813] 826

    /** 			lResult = (lLeftValue / lPercent)*/
    DeRef(_lResult_10630);
    if (IS_ATOM_INT(_lLeftValue_10626)) {
        _lResult_10630 = (_lLeftValue_10626 % _lPercent_10629) ? NewDouble((double)_lLeftValue_10626 / _lPercent_10629) : (_lLeftValue_10626 / _lPercent_10629);
    }
    else {
        _lResult_10630 = NewDouble(DBL_PTR(_lLeftValue_10626)->dbl / (double)_lPercent_10629);
    }
    goto L1A; // [823] 870
L19: 

    /** 	        lResult = lLeftValue*/
    Ref(_lLeftValue_10626);
    DeRef(_lResult_10630);
    _lResult_10630 = _lLeftValue_10626;
    goto L1A; // [832] 870
L18: 

    /** 	    if lPercent != 1 then*/
    if (_lPercent_10629 == 1)
    goto L1B; // [837] 858

    /** 	        lResult = (lLeftValue  + (lRightValue / (lRightSize))) / lPercent*/
    if (IS_ATOM_INT(_lRightValue_10627) && IS_ATOM_INT(_lRightSize_10625)) {
        _6084 = (_lRightValue_10627 % _lRightSize_10625) ? NewDouble((double)_lRightValue_10627 / _lRightSize_10625) : (_lRightValue_10627 / _lRightSize_10625);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_10627)) {
            _6084 = NewDouble((double)_lRightValue_10627 / DBL_PTR(_lRightSize_10625)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_10625)) {
                _6084 = NewDouble(DBL_PTR(_lRightValue_10627)->dbl / (double)_lRightSize_10625);
            }
            else
            _6084 = NewDouble(DBL_PTR(_lRightValue_10627)->dbl / DBL_PTR(_lRightSize_10625)->dbl);
        }
    }
    if (IS_ATOM_INT(_lLeftValue_10626) && IS_ATOM_INT(_6084)) {
        _6085 = _lLeftValue_10626 + _6084;
        if ((long)((unsigned long)_6085 + (unsigned long)HIGH_BITS) >= 0) 
        _6085 = NewDouble((double)_6085);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_10626)) {
            _6085 = NewDouble((double)_lLeftValue_10626 + DBL_PTR(_6084)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6084)) {
                _6085 = NewDouble(DBL_PTR(_lLeftValue_10626)->dbl + (double)_6084);
            }
            else
            _6085 = NewDouble(DBL_PTR(_lLeftValue_10626)->dbl + DBL_PTR(_6084)->dbl);
        }
    }
    DeRef(_6084);
    _6084 = NOVALUE;
    DeRef(_lResult_10630);
    if (IS_ATOM_INT(_6085)) {
        _lResult_10630 = (_6085 % _lPercent_10629) ? NewDouble((double)_6085 / _lPercent_10629) : (_6085 / _lPercent_10629);
    }
    else {
        _lResult_10630 = NewDouble(DBL_PTR(_6085)->dbl / (double)_lPercent_10629);
    }
    DeRef(_6085);
    _6085 = NOVALUE;
    goto L1C; // [855] 869
L1B: 

    /** 	        lResult = lLeftValue + (lRightValue / lRightSize)*/
    if (IS_ATOM_INT(_lRightValue_10627) && IS_ATOM_INT(_lRightSize_10625)) {
        _6087 = (_lRightValue_10627 % _lRightSize_10625) ? NewDouble((double)_lRightValue_10627 / _lRightSize_10625) : (_lRightValue_10627 / _lRightSize_10625);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_10627)) {
            _6087 = NewDouble((double)_lRightValue_10627 / DBL_PTR(_lRightSize_10625)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_10625)) {
                _6087 = NewDouble(DBL_PTR(_lRightValue_10627)->dbl / (double)_lRightSize_10625);
            }
            else
            _6087 = NewDouble(DBL_PTR(_lRightValue_10627)->dbl / DBL_PTR(_lRightSize_10625)->dbl);
        }
    }
    DeRef(_lResult_10630);
    if (IS_ATOM_INT(_lLeftValue_10626) && IS_ATOM_INT(_6087)) {
        _lResult_10630 = _lLeftValue_10626 + _6087;
        if ((long)((unsigned long)_lResult_10630 + (unsigned long)HIGH_BITS) >= 0) 
        _lResult_10630 = NewDouble((double)_lResult_10630);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_10626)) {
            _lResult_10630 = NewDouble((double)_lLeftValue_10626 + DBL_PTR(_6087)->dbl);
        }
        else {
            if (IS_ATOM_INT(_6087)) {
                _lResult_10630 = NewDouble(DBL_PTR(_lLeftValue_10626)->dbl + (double)_6087);
            }
            else
            _lResult_10630 = NewDouble(DBL_PTR(_lLeftValue_10626)->dbl + DBL_PTR(_6087)->dbl);
        }
    }
    DeRef(_6087);
    _6087 = NOVALUE;
L1C: 
L1A: 

    /** 	if lSignFound < 0 then*/
    if (_lSignFound_10621 >= 0)
    goto L1D; // [872] 884

    /** 		lResult = -lResult*/
    _0 = _lResult_10630;
    if (IS_ATOM_INT(_lResult_10630)) {
        if ((unsigned long)_lResult_10630 == 0xC0000000)
        _lResult_10630 = (int)NewDouble((double)-0xC0000000);
        else
        _lResult_10630 = - _lResult_10630;
    }
    else {
        _lResult_10630 = unary_op(UMINUS, _lResult_10630);
    }
    DeRef(_0);
L1D: 

    /** 	if return_bad_pos = 0 then*/
    if (_return_bad_pos_10619 != 0)
    goto L1E; // [886] 899

    /** 		return lResult*/
    DeRefDS(_text_in_10618);
    DeRef(_lLeftSize_10624);
    DeRef(_lRightSize_10625);
    DeRef(_lLeftValue_10626);
    DeRef(_lRightValue_10627);
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6034);
    _6034 = NOVALUE;
    DeRef(_6037);
    _6037 = NOVALUE;
    DeRef(_6045);
    _6045 = NOVALUE;
    DeRef(_6052);
    _6052 = NOVALUE;
    DeRef(_6074);
    _6074 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    return _lResult_10630;
L1E: 

    /** 	if return_bad_pos = -1 then*/
    if (_return_bad_pos_10619 != -1)
    goto L1F; // [901] 934

    /** 		if lBadPos = 0 then*/
    if (_lBadPos_10623 != 0)
    goto L20; // [907] 922

    /** 			return lResult*/
    DeRefDS(_text_in_10618);
    DeRef(_lLeftSize_10624);
    DeRef(_lRightSize_10625);
    DeRef(_lLeftValue_10626);
    DeRef(_lRightValue_10627);
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6034);
    _6034 = NOVALUE;
    DeRef(_6037);
    _6037 = NOVALUE;
    DeRef(_6045);
    _6045 = NOVALUE;
    DeRef(_6052);
    _6052 = NOVALUE;
    DeRef(_6074);
    _6074 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    return _lResult_10630;
    goto L21; // [919] 933
L20: 

    /** 			return {lBadPos}	*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _lBadPos_10623;
    _6094 = MAKE_SEQ(_1);
    DeRefDS(_text_in_10618);
    DeRef(_lLeftSize_10624);
    DeRef(_lRightSize_10625);
    DeRef(_lLeftValue_10626);
    DeRef(_lRightValue_10627);
    DeRef(_lResult_10630);
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6034);
    _6034 = NOVALUE;
    DeRef(_6037);
    _6037 = NOVALUE;
    DeRef(_6045);
    _6045 = NOVALUE;
    DeRef(_6052);
    _6052 = NOVALUE;
    DeRef(_6074);
    _6074 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    return _6094;
L21: 
L1F: 

    /** 	return {lResult, lBadPos}*/
    Ref(_lResult_10630);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_10630;
    ((int *)_2)[2] = _lBadPos_10623;
    _6095 = MAKE_SEQ(_1);
    DeRefDS(_text_in_10618);
    DeRef(_lLeftSize_10624);
    DeRef(_lRightSize_10625);
    DeRef(_lLeftValue_10626);
    DeRef(_lRightValue_10627);
    DeRef(_lResult_10630);
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6034);
    _6034 = NOVALUE;
    DeRef(_6037);
    _6037 = NOVALUE;
    DeRef(_6045);
    _6045 = NOVALUE;
    DeRef(_6052);
    _6052 = NOVALUE;
    DeRef(_6074);
    _6074 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    DeRef(_6094);
    _6094 = NOVALUE;
    return _6095;
    ;
}



// 0x19E0FDCE
