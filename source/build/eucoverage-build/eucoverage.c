// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _1process_cmd_line()
{
    int _cmd_18848 = NOVALUE;
    int _keys_18851 = NOVALUE;
    int _val_18856 = NOVALUE;
    int _extras_18870 = NOVALUE;
    int _10850 = NOVALUE;
    int _10582 = NOVALUE;
    int _10579 = NOVALUE;
    int _10576 = NOVALUE;
    int _10574 = NOVALUE;
    int _10571 = NOVALUE;
    int _10569 = NOVALUE;
    int _10568 = NOVALUE;
    int _0, _1, _2;
    

    /** 	map cmd = cmd_parse( opts )*/
    _10850 = Command_Line();
    RefDS(_1opts_18811);
    RefDS(_5);
    _0 = _cmd_18848;
    _cmd_18848 = _25cmd_parse(_1opts_18811, _5, _10850);
    DeRef(_0);
    _10850 = NOVALUE;

    /** 	sequence keys = map:keys( cmd )*/
    Ref(_cmd_18848);
    _0 = _keys_18851;
    _keys_18851 = _27keys(_cmd_18848, 0);
    DeRef(_0);

    /** 	for i = 1 to length( keys ) do*/
    if (IS_SEQUENCE(_keys_18851)){
            _10568 = SEQ_PTR(_keys_18851)->length;
    }
    else {
        _10568 = 1;
    }
    {
        int _i_18854;
        _i_18854 = 1;
L1: 
        if (_i_18854 > _10568){
            goto L2; // [27] 107
        }

        /** 		object val = map:get( cmd, keys[i] )*/
        _2 = (int)SEQ_PTR(_keys_18851);
        _10569 = (int)*(((s1_ptr)_2)->base + _i_18854);
        Ref(_cmd_18848);
        Ref(_10569);
        _0 = _val_18856;
        _val_18856 = _27get(_cmd_18848, _10569, 0);
        DeRef(_0);
        _10569 = NOVALUE;

        /** 		switch keys[i] do*/
        _2 = (int)SEQ_PTR(_keys_18851);
        _10571 = (int)*(((s1_ptr)_2)->base + _i_18854);
        _1 = find(_10571, _10572);
        _10571 = NOVALUE;
        switch ( _1 ){ 

            /** 			case "o" then*/
            case 1:

            /** 				output_directory = val*/
            Ref(_val_18856);
            DeRef(_1output_directory_18833);
            _1output_directory_18833 = _val_18856;
            goto L3; // [68] 98

            /** 			case "v" then*/
            case 2:

            /** 				verbose = 1*/
            _1verbose_18832 = 1;
            goto L3; // [81] 98

            /** 			case "s" then*/
            case 3:

            /** 				stylesheet = sprintf(USER_CSS, { val })*/
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_val_18856);
            *((int *)(_2+4)) = _val_18856;
            _10574 = MAKE_SEQ(_1);
            DeRef(_1stylesheet_18835);
            _1stylesheet_18835 = EPrintf(-9999999, _1USER_CSS_18797, _10574);
            DeRefDS(_10574);
            _10574 = NOVALUE;
        ;}L3: 
        DeRef(_val_18856);
        _val_18856 = NOVALUE;

        /** 	end for*/
        _i_18854 = _i_18854 + 1;
        goto L1; // [102] 34
L2: 
        ;
    }

    /** 	if length(stylesheet) = 0 then*/
    if (IS_SEQUENCE(_1stylesheet_18835)){
            _10576 = SEQ_PTR(_1stylesheet_18835)->length;
    }
    else {
        _10576 = 1;
    }
    if (_10576 != 0)
    goto L4; // [114] 124

    /** 		stylesheet = DEFAULT_CSS*/
    RefDS(_1DEFAULT_CSS_18795);
    DeRefDS(_1stylesheet_18835);
    _1stylesheet_18835 = _1DEFAULT_CSS_18795;
L4: 

    /** 	sequence extras = map:get( cmd, cmdline:EXTRAS )*/
    Ref(_cmd_18848);
    RefDS(_25EXTRAS_16749);
    _0 = _extras_18870;
    _extras_18870 = _27get(_cmd_18848, _25EXTRAS_16749, 0);
    DeRef(_0);

    /** 	if length( extras ) != 1 then*/
    if (IS_SEQUENCE(_extras_18870)){
            _10579 = SEQ_PTR(_extras_18870)->length;
    }
    else {
        _10579 = 1;
    }
    if (_10579 == 1)
    goto L5; // [141] 155

    /** 		puts( 2, "Expected a single input coverage database\n" )*/
    EPuts(2, _10581); // DJP 

    /** 		abort( 1 )*/
    UserCleanup(1);
L5: 

    /** 	coverage_db_name = canonical_path( extras[1] )*/
    _2 = (int)SEQ_PTR(_extras_18870);
    _10582 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_10582);
    _0 = _8canonical_path(_10582, 0, 0);
    DeRef(_1coverage_db_name_18834);
    _1coverage_db_name_18834 = _0;
    _10582 = NOVALUE;

    /** end procedure*/
    DeRef(_cmd_18848);
    DeRef(_keys_18851);
    DeRefDS(_extras_18870);
    return;
    ;
}


void _1read_table(int _table_name_18881)
{
    int _file_name_18889 = NOVALUE;
    int _tx_18892 = NOVALUE;
    int _coverage_18904 = NOVALUE;
    int _records_18911 = NOVALUE;
    int _10608 = NOVALUE;
    int _10606 = NOVALUE;
    int _10605 = NOVALUE;
    int _10604 = NOVALUE;
    int _10599 = NOVALUE;
    int _10597 = NOVALUE;
    int _10595 = NOVALUE;
    int _10588 = NOVALUE;
    int _10587 = NOVALUE;
    int _10584 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if db_select_table( table_name ) != DB_OK then*/
    RefDS(_table_name_18881);
    _10584 = _2db_select_table(_table_name_18881);
    if (binary_op_a(EQUALS, _10584, 0)){
        DeRef(_10584);
        _10584 = NOVALUE;
        goto L1; // [11] 30
    }
    DeRef(_10584);
    _10584 = NOVALUE;

    /** 		printf( 2, "Error reading table %s\n", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_table_name_18881);
    *((int *)(_2+4)) = _table_name_18881;
    _10587 = MAKE_SEQ(_1);
    EPrintf(2, _10586, _10587);
    DeRefDS(_10587);
    _10587 = NOVALUE;

    /** 		abort( 1 )*/
    UserCleanup(1);
L1: 

    /** 	sequence file_name = table_name[2..$]*/
    if (IS_SEQUENCE(_table_name_18881)){
            _10588 = SEQ_PTR(_table_name_18881)->length;
    }
    else {
        _10588 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_name_18889;
    RHS_Slice(_table_name_18881, 2, _10588);

    /** 	integer tx = find( file_name, files )*/
    _tx_18892 = find_from(_file_name_18889, _1files_18838, 1);

    /** 	if not tx then*/
    if (_tx_18892 != 0)
    goto L2; // [53] 106

    /** 		files = append( files, file_name )*/
    RefDS(_file_name_18889);
    Append(&_1files_18838, _1files_18838, _file_name_18889);

    /** 		file_coverage &= 0*/
    Append(&_1file_coverage_18839, _1file_coverage_18839, 0);

    /** 		tx = length( files )*/
    if (IS_SEQUENCE(_1files_18838)){
            _tx_18892 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _tx_18892 = 1;
    }

    /** 		routine_map &= map:new()*/
    _10595 = _27new(690);
    if (IS_SEQUENCE(_1routine_map_18837) && IS_ATOM(_10595)) {
        Ref(_10595);
        Append(&_1routine_map_18837, _1routine_map_18837, _10595);
    }
    else if (IS_ATOM(_1routine_map_18837) && IS_SEQUENCE(_10595)) {
    }
    else {
        Concat((object_ptr)&_1routine_map_18837, _1routine_map_18837, _10595);
    }
    DeRef(_10595);
    _10595 = NOVALUE;

    /** 		line_map    &= map:new()*/
    _10597 = _27new(690);
    if (IS_SEQUENCE(_1line_map_18836) && IS_ATOM(_10597)) {
        Ref(_10597);
        Append(&_1line_map_18836, _1line_map_18836, _10597);
    }
    else if (IS_ATOM(_1line_map_18836) && IS_SEQUENCE(_10597)) {
    }
    else {
        Concat((object_ptr)&_1line_map_18836, _1line_map_18836, _10597);
    }
    DeRef(_10597);
    _10597 = NOVALUE;
L2: 

    /** 	if table_name[1] = 'r' then*/
    _2 = (int)SEQ_PTR(_table_name_18881);
    _10599 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _10599, 114)){
        _10599 = NOVALUE;
        goto L3; // [112] 127
    }
    _10599 = NOVALUE;

    /** 		coverage = routine_map[tx]*/
    DeRef(_coverage_18904);
    _2 = (int)SEQ_PTR(_1routine_map_18837);
    _coverage_18904 = (int)*(((s1_ptr)_2)->base + _tx_18892);
    Ref(_coverage_18904);
    goto L4; // [124] 136
L3: 

    /** 		coverage = line_map[tx]*/
    DeRef(_coverage_18904);
    _2 = (int)SEQ_PTR(_1line_map_18836);
    _coverage_18904 = (int)*(((s1_ptr)_2)->base + _tx_18892);
    Ref(_coverage_18904);
L4: 

    /** 	integer records = db_table_size()*/
    RefDS(_2current_table_name_10882);
    _records_18911 = _2db_table_size(_2current_table_name_10882);
    if (!IS_ATOM_INT(_records_18911)) {
        _1 = (long)(DBL_PTR(_records_18911)->dbl);
        if (UNIQUE(DBL_PTR(_records_18911)) && (DBL_PTR(_records_18911)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_records_18911);
        _records_18911 = _1;
    }

    /** 	for i = 1 to records do*/
    _10604 = _records_18911;
    {
        int _i_18915;
        _i_18915 = 1;
L5: 
        if (_i_18915 > _10604){
            goto L6; // [153] 196
        }

        /** 		map:put( coverage, db_record_key( i ), db_record_data( i ) )*/
        RefDS(_2current_table_name_10882);
        _10605 = _2db_record_key(_i_18915, _2current_table_name_10882);
        RefDS(_2current_table_name_10882);
        _10606 = _2db_record_data(_i_18915, _2current_table_name_10882);
        Ref(_coverage_18904);
        _27put(_coverage_18904, _10605, _10606, 1, _27threshold_size_14110);
        _10605 = NOVALUE;
        _10606 = NOVALUE;

        /** 	end for*/
        _i_18915 = _i_18915 + 1;
        goto L5; // [191] 160
L6: 
        ;
    }

    /** 	if verbose then*/
    if (_1verbose_18832 == 0)
    {
        goto L7; // [200] 214
    }
    else{
    }

    /** 		printf( 1, "%d records in table %s\n", { records, table_name } )*/
    RefDS(_table_name_18881);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _records_18911;
    ((int *)_2)[2] = _table_name_18881;
    _10608 = MAKE_SEQ(_1);
    EPrintf(1, _10607, _10608);
    DeRefDS(_10608);
    _10608 = NOVALUE;
L7: 

    /** end procedure*/
    DeRefDS(_table_name_18881);
    DeRef(_file_name_18889);
    DeRef(_coverage_18904);
    return;
    ;
}


void _1read_db()
{
    int _table_list_18933 = NOVALUE;
    int _10615 = NOVALUE;
    int _10614 = NOVALUE;
    int _10612 = NOVALUE;
    int _10609 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if db_open( coverage_db_name ) != DB_OK then*/
    RefDS(_1coverage_db_name_18834);
    _10609 = _2db_open(_1coverage_db_name_18834, 0);
    if (binary_op_a(EQUALS, _10609, 0)){
        DeRef(_10609);
        _10609 = NOVALUE;
        goto L1; // [14] 35
    }
    DeRef(_10609);
    _10609 = NOVALUE;

    /** 		printf( 2, "Could not open coverage DB %s\n", { coverage_db_name } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_1coverage_db_name_18834);
    *((int *)(_2+4)) = _1coverage_db_name_18834;
    _10612 = MAKE_SEQ(_1);
    EPrintf(2, _10611, _10612);
    DeRefDS(_10612);
    _10612 = NOVALUE;

    /** 		abort( 1 )*/
    UserCleanup(1);
L1: 

    /** 	sequence table_list = db_table_list()*/
    _0 = _table_list_18933;
    _table_list_18933 = _2db_table_list();
    DeRef(_0);

    /** 	for i = 1 to length( table_list ) do*/
    if (IS_SEQUENCE(_table_list_18933)){
            _10614 = SEQ_PTR(_table_list_18933)->length;
    }
    else {
        _10614 = 1;
    }
    {
        int _i_18937;
        _i_18937 = 1;
L2: 
        if (_i_18937 > _10614){
            goto L3; // [47] 70
        }

        /** 		read_table( table_list[i] )*/
        _2 = (int)SEQ_PTR(_table_list_18933);
        _10615 = (int)*(((s1_ptr)_2)->base + _i_18937);
        Ref(_10615);
        _1read_table(_10615);
        _10615 = NOVALUE;

        /** 	end for*/
        _i_18937 = _i_18937 + 1;
        goto L2; // [65] 54
L3: 
        ;
    }

    /** end procedure*/
    DeRef(_table_list_18933);
    return;
    ;
}


int _1sum_coverage(int _coverage_18951)
{
    int _keys_18952 = NOVALUE;
    int _covered_18954 = NOVALUE;
    int _10623 = NOVALUE;
    int _10622 = NOVALUE;
    int _10621 = NOVALUE;
    int _10620 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence keys = map:keys( coverage )*/
    Ref(_coverage_18951);
    _0 = _keys_18952;
    _keys_18952 = _27keys(_coverage_18951, 0);
    DeRef(_0);

    /** 	integer covered = 0*/
    _covered_18954 = 0;

    /** 	for i = 1 to length( keys ) do*/
    if (IS_SEQUENCE(_keys_18952)){
            _10620 = SEQ_PTR(_keys_18952)->length;
    }
    else {
        _10620 = 1;
    }
    {
        int _i_18956;
        _i_18956 = 1;
L1: 
        if (_i_18956 > _10620){
            goto L2; // [22] 60
        }

        /** 		covered += 0 != map:get( coverage, keys[i] )*/
        _2 = (int)SEQ_PTR(_keys_18952);
        _10621 = (int)*(((s1_ptr)_2)->base + _i_18956);
        Ref(_coverage_18951);
        Ref(_10621);
        _10622 = _27get(_coverage_18951, _10621, 0);
        _10621 = NOVALUE;
        if (IS_ATOM_INT(_10622)) {
            _10623 = (0 != _10622);
        }
        else {
            _10623 = binary_op(NOTEQ, 0, _10622);
        }
        DeRef(_10622);
        _10622 = NOVALUE;
        if (IS_ATOM_INT(_10623)) {
            _covered_18954 = _covered_18954 + _10623;
        }
        else {
            _covered_18954 = binary_op(PLUS, _covered_18954, _10623);
        }
        DeRef(_10623);
        _10623 = NOVALUE;
        if (!IS_ATOM_INT(_covered_18954)) {
            _1 = (long)(DBL_PTR(_covered_18954)->dbl);
            if (UNIQUE(DBL_PTR(_covered_18954)) && (DBL_PTR(_covered_18954)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_covered_18954);
            _covered_18954 = _1;
        }

        /** 	end for*/
        _i_18956 = _i_18956 + 1;
        goto L1; // [55] 29
L2: 
        ;
    }

    /** 	return covered*/
    DeRef(_coverage_18951);
    DeRef(_keys_18952);
    return _covered_18954;
    ;
}


void _1analyze_coverage()
{
    int _size_1__tmp_at124_18988 = NOVALUE;
    int _size_inlined_size_at_124_18987 = NOVALUE;
    int _the_map_p_inlined_size_at_121_18986 = NOVALUE;
    int _size_1__tmp_at72_18981 = NOVALUE;
    int _size_inlined_size_at_72_18980 = NOVALUE;
    int _the_map_p_inlined_size_at_69_18979 = NOVALUE;
    int _file_name_18969 = NOVALUE;
    int _path_18971 = NOVALUE;
    int _coverage_18974 = NOVALUE;
    int _op_18976 = NOVALUE;
    int _10639 = NOVALUE;
    int _10638 = NOVALUE;
    int _10636 = NOVALUE;
    int _10635 = NOVALUE;
    int _10634 = NOVALUE;
    int _10633 = NOVALUE;
    int _10632 = NOVALUE;
    int _10631 = NOVALUE;
    int _10627 = NOVALUE;
    int _0, _1, _2;
    

    /** 	dir_map = map:new()*/
    _0 = _27new(690);
    DeRef(_1dir_map_18844);
    _1dir_map_18844 = _0;

    /** 	dir_coverage = map:new()*/
    _0 = _27new(690);
    DeRef(_1dir_coverage_18842);
    _1dir_coverage_18842 = _0;

    /** 	for i = 1 to length( files ) do*/
    if (IS_SEQUENCE(_1files_18838)){
            _10627 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _10627 = 1;
    }
    {
        int _i_18967;
        _i_18967 = 1;
L1: 
        if (_i_18967 > _10627){
            goto L2; // [20] 229
        }

        /** 		sequence file_name = files[i]*/
        DeRef(_file_name_18969);
        _2 = (int)SEQ_PTR(_1files_18838);
        _file_name_18969 = (int)*(((s1_ptr)_2)->base + _i_18967);
        RefDS(_file_name_18969);

        /** 		sequence path = dirname( file_name )*/
        RefDS(_file_name_18969);
        _0 = _path_18971;
        _path_18971 = _8dirname(_file_name_18969, 0);
        DeRef(_0);

        /** 		sequence coverage = repeat( 0, COV_SIZE )*/
        DeRef(_coverage_18974);
        _coverage_18974 = Repeat(0, 4);

        /** 		integer op*/

        /** 		coverage[COV_FUNCS] = map:size( routine_map[i] )*/
        _2 = (int)SEQ_PTR(_1routine_map_18837);
        _10631 = (int)*(((s1_ptr)_2)->base + _i_18967);
        Ref(_10631);
        DeRef(_the_map_p_inlined_size_at_69_18979);
        _the_map_p_inlined_size_at_69_18979 = _10631;
        _10631 = NOVALUE;

        /** 	return eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
        DeRef(_size_1__tmp_at72_18981);
        _2 = (int)SEQ_PTR(_28ram_space_13199);
        if (!IS_ATOM_INT(_the_map_p_inlined_size_at_69_18979)){
            _size_1__tmp_at72_18981 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_inlined_size_at_69_18979)->dbl));
        }
        else{
            _size_1__tmp_at72_18981 = (int)*(((s1_ptr)_2)->base + _the_map_p_inlined_size_at_69_18979);
        }
        Ref(_size_1__tmp_at72_18981);
        DeRef(_size_inlined_size_at_72_18980);
        _2 = (int)SEQ_PTR(_size_1__tmp_at72_18981);
        _size_inlined_size_at_72_18980 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_size_inlined_size_at_72_18980);
        DeRef(_the_map_p_inlined_size_at_69_18979);
        _the_map_p_inlined_size_at_69_18979 = NOVALUE;
        DeRef(_size_1__tmp_at72_18981);
        _size_1__tmp_at72_18981 = NOVALUE;
        Ref(_size_inlined_size_at_72_18980);
        _2 = (int)SEQ_PTR(_coverage_18974);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _coverage_18974 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _size_inlined_size_at_72_18980;
        DeRef(_1);

        /** 		coverage[COV_FUNCS_TESTED] = sum_coverage( routine_map[i] )*/
        _2 = (int)SEQ_PTR(_1routine_map_18837);
        _10632 = (int)*(((s1_ptr)_2)->base + _i_18967);
        Ref(_10632);
        _10633 = _1sum_coverage(_10632);
        _10632 = NOVALUE;
        _2 = (int)SEQ_PTR(_coverage_18974);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _coverage_18974 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _10633;
        if( _1 != _10633 ){
            DeRef(_1);
        }
        _10633 = NOVALUE;

        /** 		coverage[COV_LINES] = map:size( line_map[i] )*/
        _2 = (int)SEQ_PTR(_1line_map_18836);
        _10634 = (int)*(((s1_ptr)_2)->base + _i_18967);
        Ref(_10634);
        DeRef(_the_map_p_inlined_size_at_121_18986);
        _the_map_p_inlined_size_at_121_18986 = _10634;
        _10634 = NOVALUE;

        /** 	return eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
        DeRef(_size_1__tmp_at124_18988);
        _2 = (int)SEQ_PTR(_28ram_space_13199);
        if (!IS_ATOM_INT(_the_map_p_inlined_size_at_121_18986)){
            _size_1__tmp_at124_18988 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_inlined_size_at_121_18986)->dbl));
        }
        else{
            _size_1__tmp_at124_18988 = (int)*(((s1_ptr)_2)->base + _the_map_p_inlined_size_at_121_18986);
        }
        Ref(_size_1__tmp_at124_18988);
        DeRef(_size_inlined_size_at_124_18987);
        _2 = (int)SEQ_PTR(_size_1__tmp_at124_18988);
        _size_inlined_size_at_124_18987 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_size_inlined_size_at_124_18987);
        DeRef(_the_map_p_inlined_size_at_121_18986);
        _the_map_p_inlined_size_at_121_18986 = NOVALUE;
        DeRef(_size_1__tmp_at124_18988);
        _size_1__tmp_at124_18988 = NOVALUE;
        Ref(_size_inlined_size_at_124_18987);
        _2 = (int)SEQ_PTR(_coverage_18974);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _coverage_18974 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 4);
        _1 = *(int *)_2;
        *(int *)_2 = _size_inlined_size_at_124_18987;
        DeRef(_1);

        /** 		coverage[COV_LINES_TESTED] = sum_coverage( line_map[i] )*/
        _2 = (int)SEQ_PTR(_1line_map_18836);
        _10635 = (int)*(((s1_ptr)_2)->base + _i_18967);
        Ref(_10635);
        _10636 = _1sum_coverage(_10635);
        _10635 = NOVALUE;
        _2 = (int)SEQ_PTR(_coverage_18974);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _coverage_18974 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 3);
        _1 = *(int *)_2;
        *(int *)_2 = _10636;
        if( _1 != _10636 ){
            DeRef(_1);
        }
        _10636 = NOVALUE;

        /** 		file_coverage[i] = coverage*/
        RefDS(_coverage_18974);
        _2 = (int)SEQ_PTR(_1file_coverage_18839);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _1file_coverage_18839 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18967);
        _1 = *(int *)_2;
        *(int *)_2 = _coverage_18974;
        DeRef(_1);

        /** 		if verbose then*/
        if (_1verbose_18832 == 0)
        {
            goto L3; // [170] 190
        }
        else{
        }

        /** 			printf( 1, "file coverage: %s routines [%d / %d]  lines [%d / %d]\n",*/
        _2 = (int)SEQ_PTR(_1file_coverage_18839);
        _10638 = (int)*(((s1_ptr)_2)->base + _i_18967);
        RefDS(_file_name_18969);
        Prepend(&_10639, _10638, _file_name_18969);
        _10638 = NOVALUE;
        EPrintf(1, _10637, _10639);
        DeRefDS(_10639);
        _10639 = NOVALUE;
L3: 

        /** 		map:put( dir_map, path, i, map:APPEND )*/
        Ref(_1dir_map_18844);
        RefDS(_path_18971);
        _27put(_1dir_map_18844, _path_18971, _i_18967, 6, _27threshold_size_14110);

        /** 		map:put( dir_coverage, path, coverage, map:ADD )*/
        Ref(_1dir_coverage_18842);
        RefDS(_path_18971);
        RefDS(_coverage_18974);
        _27put(_1dir_coverage_18842, _path_18971, _coverage_18974, 2, _27threshold_size_14110);
        DeRef(_file_name_18969);
        _file_name_18969 = NOVALUE;
        DeRefDS(_path_18971);
        _path_18971 = NOVALUE;
        DeRefDS(_coverage_18974);
        _coverage_18974 = NOVALUE;

        /** 	end for*/
        _i_18967 = _i_18967 + 1;
        goto L1; // [224] 27
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}


void _1output_dir(int _name_18997)
{
    int _10645 = NOVALUE;
    int _10642 = NOVALUE;
    int _10640 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not file_exists( name ) then*/
    RefDS(_name_18997);
    _10640 = _8file_exists(_name_18997);
    if (IS_ATOM_INT(_10640)) {
        if (_10640 != 0){
            DeRef(_10640);
            _10640 = NOVALUE;
            goto L1; // [9] 39
        }
    }
    else {
        if (DBL_PTR(_10640)->dbl != 0.0){
            DeRef(_10640);
            _10640 = NOVALUE;
            goto L1; // [9] 39
        }
    }
    DeRef(_10640);
    _10640 = NOVALUE;

    /** 		if not create_directory( name ) then*/
    RefDS(_name_18997);
    _10642 = _8create_directory(_name_18997, 448, 1);
    if (IS_ATOM_INT(_10642)) {
        if (_10642 != 0){
            DeRef(_10642);
            _10642 = NOVALUE;
            goto L2; // [20] 38
        }
    }
    else {
        if (DBL_PTR(_10642)->dbl != 0.0){
            DeRef(_10642);
            _10642 = NOVALUE;
            goto L2; // [20] 38
        }
    }
    DeRef(_10642);
    _10642 = NOVALUE;

    /** 			printf( 2, "Could not create output directory: %s\n", {name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_name_18997);
    *((int *)(_2+4)) = _name_18997;
    _10645 = MAKE_SEQ(_1);
    EPrintf(2, _10644, _10645);
    DeRefDS(_10645);
    _10645 = NOVALUE;

    /** 			abort(1)*/
    UserCleanup(1);
L2: 
L1: 

    /** end procedure*/
    DeRefDS(_name_18997);
    return;
    ;
}


int _1get_style(int _executed_19010)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_executed_19010)) {
        _1 = (long)(DBL_PTR(_executed_19010)->dbl);
        if (UNIQUE(DBL_PTR(_executed_19010)) && (DBL_PTR(_executed_19010)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_executed_19010);
        _executed_19010 = _1;
    }

    /** 	if executed then*/
    if (_executed_19010 == 0)
    {
        goto L1; // [7] 19
    }
    else{
    }

    /** 		return "exec"*/
    RefDS(_10646);
    return _10646;
    goto L2; // [16] 26
L1: 

    /** 		return "noexec"*/
    RefDS(_10647);
    return _10647;
L2: 
    ;
}


int _1calc_percent(int _numerator_19017, int _denominator_19018)
{
    int _10649 = NOVALUE;
    int _10648 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if denominator then*/
    if (_denominator_19018 == 0) {
        goto L1; // [3] 23
    }
    else {
        if (!IS_ATOM_INT(_denominator_19018) && DBL_PTR(_denominator_19018)->dbl == 0.0){
            goto L1; // [3] 23
        }
    }

    /** 		return 100 * numerator / denominator*/
    if (IS_ATOM_INT(_numerator_19017)) {
        if (_numerator_19017 <= INT15 && _numerator_19017 >= -INT15)
        _10648 = 100 * _numerator_19017;
        else
        _10648 = NewDouble(100 * (double)_numerator_19017);
    }
    else {
        _10648 = NewDouble((double)100 * DBL_PTR(_numerator_19017)->dbl);
    }
    if (IS_ATOM_INT(_10648) && IS_ATOM_INT(_denominator_19018)) {
        _10649 = (_10648 % _denominator_19018) ? NewDouble((double)_10648 / _denominator_19018) : (_10648 / _denominator_19018);
    }
    else {
        if (IS_ATOM_INT(_10648)) {
            _10649 = NewDouble((double)_10648 / DBL_PTR(_denominator_19018)->dbl);
        }
        else {
            if (IS_ATOM_INT(_denominator_19018)) {
                _10649 = NewDouble(DBL_PTR(_10648)->dbl / (double)_denominator_19018);
            }
            else
            _10649 = NewDouble(DBL_PTR(_10648)->dbl / DBL_PTR(_denominator_19018)->dbl);
        }
    }
    DeRef(_10648);
    _10648 = NOVALUE;
    DeRef(_numerator_19017);
    DeRef(_denominator_19018);
    return _10649;
    goto L2; // [20] 30
L1: 

    /** 		return 100*/
    DeRef(_numerator_19017);
    DeRef(_denominator_19018);
    DeRef(_10649);
    _10649 = NOVALUE;
    return 100;
L2: 
    ;
}


void _1write_file_html(int _output_dir_19033, int _fx_19034)
{
    int _in_19035 = NOVALUE;
    int _html_name_19040 = NOVALUE;
    int _out_19046 = NOVALUE;
    int _source_lines_19048 = NOVALUE;
    int _line_number_19049 = NOVALUE;
    int _lines_19051 = NOVALUE;
    int _routines_19054 = NOVALUE;
    int _line_19056 = NOVALUE;
    int _routine_lines_19058 = NOVALUE;
    int _routine_line_count_19060 = NOVALUE;
    int _routines_executed_19061 = NOVALUE;
    int _routine_executed_count_19062 = NOVALUE;
    int _total_routines_19063 = NOVALUE;
    int _in_routine_19064 = NOVALUE;
    int _routine_name_19065 = NOVALUE;
    int _total_lines_19066 = NOVALUE;
    int _total_executed_19067 = NOVALUE;
    int _out_line_19071 = NOVALUE;
    int _executed_19074 = NOVALUE;
    int _get_style_inlined_get_style_at_228_19080 = NOVALUE;
    int _routine_matches_19093 = NOVALUE;
    int _r_executed_19098 = NOVALUE;
    int _get_style_inlined_get_style_at_448_19108 = NOVALUE;
    int _percent_19128 = NOVALUE;
    int _size_1__tmp_at694_19138 = NOVALUE;
    int _size_inlined_size_at_694_19137 = NOVALUE;
    int _calc_percent_1__tmp_at711_19141 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_711_19140 = NOVALUE;
    int _calc_percent_1__tmp_at742_19144 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_742_19143 = NOVALUE;
    int _keys_19148 = NOVALUE;
    int _routine_coverage_19150 = NOVALUE;
    int _coverage_19156 = NOVALUE;
    int _calc_percent_1__tmp_at860_19163 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_860_19162 = NOVALUE;
    int _row_class_19176 = NOVALUE;
    int _has_match_2__tmp_at283_19390 = NOVALUE;
    int _has_match_1__tmp_at283_19389 = NOVALUE;
    int _has_match_inlined_has_match_at_283_19388 = NOVALUE;
    int _has_match_2__tmp_at359_19394 = NOVALUE;
    int _has_match_1__tmp_at359_19393 = NOVALUE;
    int _has_match_inlined_has_match_at_359_19392 = NOVALUE;
    int _has_match_2__tmp_at559_19398 = NOVALUE;
    int _has_match_1__tmp_at559_19397 = NOVALUE;
    int _has_match_inlined_has_match_at_559_19396 = NOVALUE;
    int _10744 = NOVALUE;
    int _10743 = NOVALUE;
    int _10740 = NOVALUE;
    int _10739 = NOVALUE;
    int _10738 = NOVALUE;
    int _10737 = NOVALUE;
    int _10736 = NOVALUE;
    int _10734 = NOVALUE;
    int _10733 = NOVALUE;
    int _10729 = NOVALUE;
    int _10728 = NOVALUE;
    int _10727 = NOVALUE;
    int _10726 = NOVALUE;
    int _10725 = NOVALUE;
    int _10721 = NOVALUE;
    int _10720 = NOVALUE;
    int _10718 = NOVALUE;
    int _10715 = NOVALUE;
    int _10714 = NOVALUE;
    int _10713 = NOVALUE;
    int _10712 = NOVALUE;
    int _10706 = NOVALUE;
    int _10705 = NOVALUE;
    int _10703 = NOVALUE;
    int _10701 = NOVALUE;
    int _10697 = NOVALUE;
    int _10696 = NOVALUE;
    int _10695 = NOVALUE;
    int _10693 = NOVALUE;
    int _10690 = NOVALUE;
    int _10687 = NOVALUE;
    int _10685 = NOVALUE;
    int _10684 = NOVALUE;
    int _10681 = NOVALUE;
    int _10679 = NOVALUE;
    int _10678 = NOVALUE;
    int _10677 = NOVALUE;
    int _10675 = NOVALUE;
    int _10672 = NOVALUE;
    int _10670 = NOVALUE;
    int _10667 = NOVALUE;
    int _10666 = NOVALUE;
    int _10665 = NOVALUE;
    int _10658 = NOVALUE;
    int _10657 = NOVALUE;
    int _10654 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_fx_19034)) {
        _1 = (long)(DBL_PTR(_fx_19034)->dbl);
        if (UNIQUE(DBL_PTR(_fx_19034)) && (DBL_PTR(_fx_19034)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fx_19034);
        _fx_19034 = _1;
    }

    /** 	atom in  = open( files[fx], "r", 1 )*/
    _2 = (int)SEQ_PTR(_1files_18838);
    _10654 = (int)*(((s1_ptr)_2)->base + _fx_19034);
    DeRef(_in_19035);
    _in_19035 = EOpen(_10654, _846, 1);
    _10654 = NOVALUE;

    /** 	if in = -1 then*/
    if (binary_op_a(NOTEQ, _in_19035, -1)){
        goto L1; // [22] 32
    }

    /** 		return*/
    DeRefDS(_output_dir_19033);
    DeRef(_in_19035);
    DeRef(_html_name_19040);
    DeRef(_out_19046);
    DeRef(_source_lines_19048);
    DeRef(_lines_19051);
    DeRef(_routines_19054);
    DeRefi(_line_19056);
    DeRef(_routine_lines_19058);
    DeRef(_routine_name_19065);
    DeRef(_percent_19128);
    DeRef(_keys_19148);
    DeRef(_routine_coverage_19150);
    return;
L1: 

    /** 	sequence html_name = output_dir & encode( short_names[fx] ) & ".html"*/
    _2 = (int)SEQ_PTR(_1short_names_18840);
    _10657 = (int)*(((s1_ptr)_2)->base + _fx_19034);
    RefDS(_10657);
    RefDS(_9690);
    _10658 = _34encode(_10657, _9690);
    _10657 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _10659;
        concat_list[1] = _10658;
        concat_list[2] = _output_dir_19033;
        Concat_N((object_ptr)&_html_name_19040, concat_list, 3);
    }
    DeRef(_10658);
    _10658 = NOVALUE;

    /** 	atom out = open( html_name, "w", 1 )*/
    DeRef(_out_19046);
    _out_19046 = EOpen(_html_name_19040, _889, 1);

    /** 	sequence source_lines = {}*/
    RefDS(_5);
    DeRef(_source_lines_19048);
    _source_lines_19048 = _5;

    /** 	integer line_number = 0*/
    _line_number_19049 = 0;

    /** 	map lines    = line_map[fx]*/
    DeRef(_lines_19051);
    _2 = (int)SEQ_PTR(_1line_map_18836);
    _lines_19051 = (int)*(((s1_ptr)_2)->base + _fx_19034);
    Ref(_lines_19051);

    /** 	map routines = routine_map[fx]*/
    DeRef(_routines_19054);
    _2 = (int)SEQ_PTR(_1routine_map_18837);
    _routines_19054 = (int)*(((s1_ptr)_2)->base + _fx_19034);
    Ref(_routines_19054);

    /** 	object line*/

    /** 	map routine_lines = map:new()*/
    _0 = _routine_lines_19058;
    _routine_lines_19058 = _27new(690);
    DeRef(_0);

    /** 	integer routine_line_count = 0*/
    _routine_line_count_19060 = 0;

    /** 	integer routines_executed = 0*/
    _routines_executed_19061 = 0;

    /** 	integer routine_executed_count = 0*/
    _routine_executed_count_19062 = 0;

    /** 	integer total_routines = 0*/
    _total_routines_19063 = 0;

    /** 	integer in_routine = 0*/
    _in_routine_19064 = 0;

    /** 	sequence routine_name = ""*/
    RefDS(_5);
    DeRef(_routine_name_19065);
    _routine_name_19065 = _5;

    /** 	integer total_lines    = 0*/
    _total_lines_19066 = 0;

    /** 	integer total_executed = 0*/
    _total_executed_19067 = 0;

    /** 	while sequence( line ) with entry do*/
    goto L2; // [154] 639
L3: 
    _10665 = IS_SEQUENCE(_line_19056);
    if (_10665 == 0)
    {
        _10665 = NOVALUE;
        goto L4; // [162] 659
    }
    else{
        _10665 = NOVALUE;
    }

    /** 		line[$] = ' ' -- remove the newline char*/
    if (IS_SEQUENCE(_line_19056)){
            _10666 = SEQ_PTR(_line_19056)->length;
    }
    else {
        _10666 = 1;
    }
    _2 = (int)SEQ_PTR(_line_19056);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _line_19056 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _10666);
    *(int *)_2 = 32;

    /** 		sequence out_line*/

    /** 		if map:has( lines, line_number ) then*/
    Ref(_lines_19051);
    _10667 = _27has(_lines_19051, _line_number_19049);
    if (_10667 == 0) {
        DeRef(_10667);
        _10667 = NOVALUE;
        goto L5; // [183] 542
    }
    else {
        if (!IS_ATOM_INT(_10667) && DBL_PTR(_10667)->dbl == 0.0){
            DeRef(_10667);
            _10667 = NOVALUE;
            goto L5; // [183] 542
        }
        DeRef(_10667);
        _10667 = NOVALUE;
    }
    DeRef(_10667);
    _10667 = NOVALUE;

    /** 			integer executed = map:get( lines, line_number )*/
    Ref(_lines_19051);
    _executed_19074 = _27get(_lines_19051, _line_number_19049, 0);
    if (!IS_ATOM_INT(_executed_19074)) {
        _1 = (long)(DBL_PTR(_executed_19074)->dbl);
        if (UNIQUE(DBL_PTR(_executed_19074)) && (DBL_PTR(_executed_19074)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_executed_19074);
        _executed_19074 = _1;
    }

    /** 			total_lines += 1*/
    _total_lines_19066 = _total_lines_19066 + 1;

    /** 			total_executed += 0 != executed*/
    _10670 = (0 != _executed_19074);
    _total_executed_19067 = _total_executed_19067 + _10670;
    _10670 = NOVALUE;

    /** 			out_line = sprintf( SOURCE_LINE, {line_number, executed, get_style( executed ), line})*/

    /** 	if executed then*/
    if (_executed_19074 == 0)
    {
        goto L6; // [222] 235
    }
    else{
    }

    /** 		return "exec"*/
    RefDS(_10646);
    DeRefi(_get_style_inlined_get_style_at_228_19080);
    _get_style_inlined_get_style_at_228_19080 = _10646;
    goto L7; // [230] 244
    goto L8; // [232] 243
L6: 

    /** 		return "noexec"*/
    RefDS(_10647);
    DeRefi(_get_style_inlined_get_style_at_228_19080);
    _get_style_inlined_get_style_at_228_19080 = _10647;
    goto L7; // [240] 244
L8: 
L7: 
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _line_number_19049;
    *((int *)(_2+8)) = _executed_19074;
    RefDS(_get_style_inlined_get_style_at_228_19080);
    *((int *)(_2+12)) = _get_style_inlined_get_style_at_228_19080;
    Ref(_line_19056);
    *((int *)(_2+16)) = _line_19056;
    _10672 = MAKE_SEQ(_1);
    DeRefi(_out_line_19071);
    _out_line_19071 = EPrintf(-9999999, _1SOURCE_LINE_18799, _10672);
    DeRefDS(_10672);
    _10672 = NOVALUE;

    /** 			if in_routine then*/
    if (_in_routine_19064 == 0)
    {
        goto L9; // [257] 356
    }
    else{
    }

    /** 				routine_line_count     += 1*/
    _routine_line_count_19060 = _routine_line_count_19060 + 1;

    /** 				routine_executed_count += 0 != executed*/
    _10675 = (0 != _executed_19074);
    _routine_executed_count_19062 = _routine_executed_count_19062 + _10675;
    _10675 = NOVALUE;

    /** 				if regex:has_match( match_end_routine, line ) then*/

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_1match_end_routine_19028);
    _0 = _has_match_1__tmp_at283_19389;
    _has_match_1__tmp_at283_19389 = _35get_ovector_size(_1match_end_routine_19028, 30);
    DeRef(_0);
    Ref(_1match_end_routine_19028);
    Ref(_line_19056);
    Ref(_has_match_1__tmp_at283_19389);
    _0 = _has_match_2__tmp_at283_19390;
    _has_match_2__tmp_at283_19390 = _35find(_1match_end_routine_19028, _line_19056, 1, 0, _has_match_1__tmp_at283_19389);
    DeRef(_0);
    _has_match_inlined_has_match_at_283_19388 = IS_SEQUENCE(_has_match_2__tmp_at283_19390);
    DeRef(_has_match_1__tmp_at283_19389);
    _has_match_1__tmp_at283_19389 = NOVALUE;
    DeRef(_has_match_2__tmp_at283_19390);
    _has_match_2__tmp_at283_19390 = NOVALUE;
    DeRef(_10677);
    _10677 = _has_match_inlined_has_match_at_283_19388;
    if (_10677 == 0)
    {
        _10677 = NOVALUE;
        goto LA; // [308] 537
    }
    else{
        _10677 = NOVALUE;
    }

    /** 					in_routine = 0*/
    _in_routine_19064 = 0;

    /** 					map:put( routine_lines, routine_name, { routine_executed_count, routine_line_count } )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _routine_executed_count_19062;
    ((int *)_2)[2] = _routine_line_count_19060;
    _10678 = MAKE_SEQ(_1);
    Ref(_routine_lines_19058);
    RefDS(_routine_name_19065);
    _27put(_routine_lines_19058, _routine_name_19065, _10678, 1, _27threshold_size_14110);
    _10678 = NOVALUE;

    /** 					routine_executed_count = 0*/
    _routine_executed_count_19062 = 0;

    /** 					routine_line_count = 0*/
    _routine_line_count_19060 = 0;
    goto LA; // [349] 537
    goto LA; // [353] 537
L9: 

    /** 			elsif regex:has_match( match_routine, line ) then*/

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_1match_routine_19024);
    _0 = _has_match_1__tmp_at359_19393;
    _has_match_1__tmp_at359_19393 = _35get_ovector_size(_1match_routine_19024, 30);
    DeRef(_0);
    Ref(_1match_routine_19024);
    Ref(_line_19056);
    Ref(_has_match_1__tmp_at359_19393);
    _0 = _has_match_2__tmp_at359_19394;
    _has_match_2__tmp_at359_19394 = _35find(_1match_routine_19024, _line_19056, 1, 0, _has_match_1__tmp_at359_19393);
    DeRef(_0);
    _has_match_inlined_has_match_at_359_19392 = IS_SEQUENCE(_has_match_2__tmp_at359_19394);
    DeRef(_has_match_1__tmp_at359_19393);
    _has_match_1__tmp_at359_19393 = NOVALUE;
    DeRef(_has_match_2__tmp_at359_19394);
    _has_match_2__tmp_at359_19394 = NOVALUE;
    DeRef(_10679);
    _10679 = _has_match_inlined_has_match_at_359_19392;
    if (_10679 == 0)
    {
        _10679 = NOVALUE;
        goto LB; // [384] 534
    }
    else{
        _10679 = NOVALUE;
    }

    /** 				sequence routine_matches = all_matches( match_routine, line )*/
    Ref(_1match_routine_19024);
    Ref(_line_19056);
    _0 = _routine_matches_19093;
    _routine_matches_19093 = _35all_matches(_1match_routine_19024, _line_19056, 1, 0);
    DeRef(_0);

    /** 				routine_name = routine_matches[1][2]*/
    _2 = (int)SEQ_PTR(_routine_matches_19093);
    _10681 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_routine_name_19065);
    _2 = (int)SEQ_PTR(_10681);
    _routine_name_19065 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_routine_name_19065);
    _10681 = NOVALUE;

    /** 				integer r_executed = map:get( routines, routine_name)*/
    Ref(_routines_19054);
    RefDS(_routine_name_19065);
    _r_executed_19098 = _27get(_routines_19054, _routine_name_19065, 0);
    if (!IS_ATOM_INT(_r_executed_19098)) {
        _1 = (long)(DBL_PTR(_r_executed_19098)->dbl);
        if (UNIQUE(DBL_PTR(_r_executed_19098)) && (DBL_PTR(_r_executed_19098)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_executed_19098);
        _r_executed_19098 = _1;
    }

    /** 				if r_executed and not executed then*/
    if (_r_executed_19098 == 0) {
        goto LC; // [426] 446
    }
    _10685 = (_executed_19074 == 0);
    if (_10685 == 0)
    {
        DeRef(_10685);
        _10685 = NOVALUE;
        goto LC; // [434] 446
    }
    else{
        DeRef(_10685);
        _10685 = NOVALUE;
    }

    /** 					total_executed += 1*/
    _total_executed_19067 = _total_executed_19067 + 1;
LC: 

    /** 				executed = r_executed*/
    _executed_19074 = _r_executed_19098;

    /** 				routines_executed += 0 != executed*/
    _10687 = (0 != _executed_19074);
    _routines_executed_19061 = _routines_executed_19061 + _10687;
    _10687 = NOVALUE;

    /** 				total_routines += 1*/
    _total_routines_19063 = _total_routines_19063 + 1;

    /** 				out_line = sprintf( ROUTINE_LINE, { line_number, executed, get_style( executed ), routine_name, line })*/

    /** 	if executed then*/
    if (_executed_19074 == 0)
    {
        goto LD; // [477] 490
    }
    else{
    }

    /** 		return "exec"*/
    RefDS(_10646);
    DeRefi(_get_style_inlined_get_style_at_448_19108);
    _get_style_inlined_get_style_at_448_19108 = _10646;
    goto LE; // [485] 499
    goto LF; // [487] 498
LD: 

    /** 		return "noexec"*/
    RefDS(_10647);
    DeRefi(_get_style_inlined_get_style_at_448_19108);
    _get_style_inlined_get_style_at_448_19108 = _10647;
    goto LE; // [495] 499
LF: 
LE: 
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _line_number_19049;
    *((int *)(_2+8)) = _executed_19074;
    RefDS(_get_style_inlined_get_style_at_448_19108);
    *((int *)(_2+12)) = _get_style_inlined_get_style_at_448_19108;
    RefDS(_routine_name_19065);
    *((int *)(_2+16)) = _routine_name_19065;
    Ref(_line_19056);
    *((int *)(_2+20)) = _line_19056;
    _10690 = MAKE_SEQ(_1);
    DeRefi(_out_line_19071);
    _out_line_19071 = EPrintf(-9999999, _1ROUTINE_LINE_18803, _10690);
    DeRefDS(_10690);
    _10690 = NOVALUE;

    /** 				in_routine = 1*/
    _in_routine_19064 = 1;

    /** 				routine_executed_count = 0 != executed*/
    _routine_executed_count_19062 = (0 != _executed_19074);

    /** 				routine_line_count = 1*/
    _routine_line_count_19060 = 1;
LB: 
    DeRef(_routine_matches_19093);
    _routine_matches_19093 = NOVALUE;
LA: 
    goto L10; // [539] 628
L5: 

    /** 			out_line = sprintf( EMPTY_LINE, { line_number, line } )*/
    Ref(_line_19056);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _line_number_19049;
    ((int *)_2)[2] = _line_19056;
    _10693 = MAKE_SEQ(_1);
    DeRefi(_out_line_19071);
    _out_line_19071 = EPrintf(-9999999, _1EMPTY_LINE_18801, _10693);
    DeRefDS(_10693);
    _10693 = NOVALUE;

    /** 			if in_routine and regex:has_match( match_end_routine, line ) then*/
    if (_in_routine_19064 == 0) {
        goto L11; // [554] 627
    }

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_1match_end_routine_19028);
    _0 = _has_match_1__tmp_at559_19397;
    _has_match_1__tmp_at559_19397 = _35get_ovector_size(_1match_end_routine_19028, 30);
    DeRef(_0);
    Ref(_1match_end_routine_19028);
    Ref(_line_19056);
    Ref(_has_match_1__tmp_at559_19397);
    _0 = _has_match_2__tmp_at559_19398;
    _has_match_2__tmp_at559_19398 = _35find(_1match_end_routine_19028, _line_19056, 1, 0, _has_match_1__tmp_at559_19397);
    DeRef(_0);
    _has_match_inlined_has_match_at_559_19396 = IS_SEQUENCE(_has_match_2__tmp_at559_19398);
    DeRef(_has_match_1__tmp_at559_19397);
    _has_match_1__tmp_at559_19397 = NOVALUE;
    DeRef(_has_match_2__tmp_at559_19398);
    _has_match_2__tmp_at559_19398 = NOVALUE;
    DeRef(_10696);
    _10696 = _has_match_inlined_has_match_at_559_19396;
    if (_10696 == 0)
    {
        _10696 = NOVALUE;
        goto L11; // [585] 627
    }
    else{
        _10696 = NOVALUE;
    }

    /** 				in_routine = 0*/
    _in_routine_19064 = 0;

    /** 				map:put( routine_lines, routine_name, { routine_executed_count, routine_line_count } )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _routine_executed_count_19062;
    ((int *)_2)[2] = _routine_line_count_19060;
    _10697 = MAKE_SEQ(_1);
    Ref(_routine_lines_19058);
    RefDS(_routine_name_19065);
    _27put(_routine_lines_19058, _routine_name_19065, _10697, 1, _27threshold_size_14110);
    _10697 = NOVALUE;

    /** 				routine_executed_count = 0*/
    _routine_executed_count_19062 = 0;

    /** 				routine_line_count = 0*/
    _routine_line_count_19060 = 0;
L11: 
L10: 

    /** 		source_lines = append( source_lines, out_line )*/
    RefDS(_out_line_19071);
    Append(&_source_lines_19048, _source_lines_19048, _out_line_19071);

    /** 	entry*/
L2: 

    /** 		line = gets( in )*/
    DeRefi(_line_19056);
    _line_19056 = EGets(_in_19035);

    /** 		line_number += 1*/
    _line_number_19049 = _line_number_19049 + 1;
    DeRefi(_out_line_19071);
    _out_line_19071 = NOVALUE;

    /** 	end while*/
    goto L3; // [656] 157
L4: 

    /** 	file_coverage[fx][COV_FUNCS] = total_routines*/
    _2 = (int)SEQ_PTR(_1file_coverage_18839);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _1file_coverage_18839 = MAKE_SEQ(_2);
    }
    _3 = (int)(_fx_19034 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _total_routines_19063;
    DeRef(_1);
    _10701 = NOVALUE;

    /** 	file_coverage[fx][COV_LINES_TESTED] = total_executed*/
    _2 = (int)SEQ_PTR(_1file_coverage_18839);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _1file_coverage_18839 = MAKE_SEQ(_2);
    }
    _3 = (int)(_fx_19034 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _total_executed_19067;
    DeRef(_1);
    _10703 = NOVALUE;

    /** 	printf( out, HEADER, { short_names[fx], stylesheet } )*/
    _2 = (int)SEQ_PTR(_1short_names_18840);
    _10705 = (int)*(((s1_ptr)_2)->base + _fx_19034);
    RefDS(_1stylesheet_18835);
    RefDS(_10705);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _10705;
    ((int *)_2)[2] = _1stylesheet_18835;
    _10706 = MAKE_SEQ(_1);
    _10705 = NOVALUE;
    EPrintf(_out_19046, _1HEADER_18791, _10706);
    DeRefDS(_10706);
    _10706 = NOVALUE;

    /** 	atom percent*/

    /** 	puts( out, "<div class='summary_header'><a href='../index.html'>COVERAGE SUMMARY</a></div>\n" )*/
    EPuts(_out_19046, _10707); // DJP 

    /** 	puts( out, "<div class='summary_header'>FILE SUMMARY</div>\n" )*/
    EPuts(_out_19046, _10708); // DJP 

    /** 	puts( out, "<table class='overview'>" &*/
    {
        int concat_list[3];

        concat_list[0] = _10711;
        concat_list[1] = _10710;
        concat_list[2] = _10709;
        Concat_N((object_ptr)&_10712, concat_list, 3);
    }
    EPuts(_out_19046, _10712); // DJP 
    DeRefDS(_10712);
    _10712 = NOVALUE;

    /** 	printf( out, FILE_SUMMARY_ROW_NO_LINK, { "", short_names[fx],*/
    _2 = (int)SEQ_PTR(_1short_names_18840);
    _10713 = (int)*(((s1_ptr)_2)->base + _fx_19034);

    /** 	return eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    DeRef(_size_1__tmp_at694_19138);
    _2 = (int)SEQ_PTR(_28ram_space_13199);
    if (!IS_ATOM_INT(_routine_lines_19058)){
        _size_1__tmp_at694_19138 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_routine_lines_19058)->dbl));
    }
    else{
        _size_1__tmp_at694_19138 = (int)*(((s1_ptr)_2)->base + _routine_lines_19058);
    }
    Ref(_size_1__tmp_at694_19138);
    DeRef(_size_inlined_size_at_694_19137);
    _2 = (int)SEQ_PTR(_size_1__tmp_at694_19138);
    _size_inlined_size_at_694_19137 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_size_inlined_size_at_694_19137);
    DeRef(_size_1__tmp_at694_19138);
    _size_1__tmp_at694_19138 = NOVALUE;

    /** 	if denominator then*/
    if (_total_routines_19063 == 0)
    {
        goto L12; // [757] 775
    }
    else{
    }

    /** 		return 100 * numerator / denominator*/
    DeRef(_calc_percent_1__tmp_at711_19141);
    if (_routines_executed_19061 <= INT15 && _routines_executed_19061 >= -INT15)
    _calc_percent_1__tmp_at711_19141 = 100 * _routines_executed_19061;
    else
    _calc_percent_1__tmp_at711_19141 = NewDouble(100 * (double)_routines_executed_19061);
    DeRef(_calc_percent_inlined_calc_percent_at_711_19140);
    if (IS_ATOM_INT(_calc_percent_1__tmp_at711_19141)) {
        _calc_percent_inlined_calc_percent_at_711_19140 = (_calc_percent_1__tmp_at711_19141 % _total_routines_19063) ? NewDouble((double)_calc_percent_1__tmp_at711_19141 / _total_routines_19063) : (_calc_percent_1__tmp_at711_19141 / _total_routines_19063);
    }
    else {
        _calc_percent_inlined_calc_percent_at_711_19140 = NewDouble(DBL_PTR(_calc_percent_1__tmp_at711_19141)->dbl / (double)_total_routines_19063);
    }
    goto L13; // [770] 784
    goto L14; // [772] 783
L12: 

    /** 		return 100*/
    DeRef(_calc_percent_inlined_calc_percent_at_711_19140);
    _calc_percent_inlined_calc_percent_at_711_19140 = 100;
    goto L13; // [780] 784
L14: 
L13: 
    DeRef(_calc_percent_1__tmp_at711_19141);
    _calc_percent_1__tmp_at711_19141 = NOVALUE;

    /** 	if denominator then*/
    if (_total_lines_19066 == 0)
    {
        goto L15; // [788] 806
    }
    else{
    }

    /** 		return 100 * numerator / denominator*/
    DeRef(_calc_percent_1__tmp_at742_19144);
    if (_total_executed_19067 <= INT15 && _total_executed_19067 >= -INT15)
    _calc_percent_1__tmp_at742_19144 = 100 * _total_executed_19067;
    else
    _calc_percent_1__tmp_at742_19144 = NewDouble(100 * (double)_total_executed_19067);
    DeRef(_calc_percent_inlined_calc_percent_at_742_19143);
    if (IS_ATOM_INT(_calc_percent_1__tmp_at742_19144)) {
        _calc_percent_inlined_calc_percent_at_742_19143 = (_calc_percent_1__tmp_at742_19144 % _total_lines_19066) ? NewDouble((double)_calc_percent_1__tmp_at742_19144 / _total_lines_19066) : (_calc_percent_1__tmp_at742_19144 / _total_lines_19066);
    }
    else {
        _calc_percent_inlined_calc_percent_at_742_19143 = NewDouble(DBL_PTR(_calc_percent_1__tmp_at742_19144)->dbl / (double)_total_lines_19066);
    }
    goto L16; // [801] 815
    goto L17; // [803] 814
L15: 

    /** 		return 100*/
    DeRef(_calc_percent_inlined_calc_percent_at_742_19143);
    _calc_percent_inlined_calc_percent_at_742_19143 = 100;
    goto L16; // [811] 815
L17: 
L16: 
    DeRef(_calc_percent_1__tmp_at742_19144);
    _calc_percent_1__tmp_at742_19144 = NOVALUE;
    _10714 = _total_lines_19066 - _total_executed_19067;
    if ((long)((unsigned long)_10714 +(unsigned long) HIGH_BITS) >= 0){
        _10714 = NewDouble((double)_10714);
    }
    _1 = NewS1(9);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_5);
    *((int *)(_2+4)) = _5;
    RefDS(_10713);
    *((int *)(_2+8)) = _10713;
    *((int *)(_2+12)) = _routines_executed_19061;
    Ref(_size_inlined_size_at_694_19137);
    *((int *)(_2+16)) = _size_inlined_size_at_694_19137;
    Ref(_calc_percent_inlined_calc_percent_at_711_19140);
    *((int *)(_2+20)) = _calc_percent_inlined_calc_percent_at_711_19140;
    *((int *)(_2+24)) = _total_executed_19067;
    *((int *)(_2+28)) = _total_lines_19066;
    Ref(_calc_percent_inlined_calc_percent_at_742_19143);
    *((int *)(_2+32)) = _calc_percent_inlined_calc_percent_at_742_19143;
    *((int *)(_2+36)) = _10714;
    _10715 = MAKE_SEQ(_1);
    _10714 = NOVALUE;
    _10713 = NOVALUE;
    EPrintf(_out_19046, _1FILE_SUMMARY_ROW_NO_LINK_18807, _10715);
    DeRefDS(_10715);
    _10715 = NOVALUE;

    /** 	puts( out, "</table>\n" )*/
    EPuts(_out_19046, _10716); // DJP 

    /** 	sequence keys = map:keys( routine_lines )*/
    Ref(_routine_lines_19058);
    _0 = _keys_19148;
    _keys_19148 = _27keys(_routine_lines_19058, 0);
    DeRef(_0);

    /** 	sequence routine_coverage = repeat( 0, length( keys ) )*/
    if (IS_SEQUENCE(_keys_19148)){
            _10718 = SEQ_PTR(_keys_19148)->length;
    }
    else {
        _10718 = 1;
    }
    DeRef(_routine_coverage_19150);
    _routine_coverage_19150 = Repeat(0, _10718);
    _10718 = NOVALUE;

    /** 	for i = 1 to length( keys ) do*/
    if (IS_SEQUENCE(_keys_19148)){
            _10720 = SEQ_PTR(_keys_19148)->length;
    }
    else {
        _10720 = 1;
    }
    {
        int _i_19154;
        _i_19154 = 1;
L18: 
        if (_i_19154 > _10720){
            goto L19; // [865] 976
        }

        /** 		sequence coverage = map:get( routine_lines, keys[i] )*/
        _2 = (int)SEQ_PTR(_keys_19148);
        _10721 = (int)*(((s1_ptr)_2)->base + _i_19154);
        Ref(_routine_lines_19058);
        Ref(_10721);
        _0 = _coverage_19156;
        _coverage_19156 = _27get(_routine_lines_19058, _10721, 0);
        DeRef(_0);
        _10721 = NOVALUE;

        /** 		total_executed = coverage[1]*/
        _2 = (int)SEQ_PTR(_coverage_19156);
        _total_executed_19067 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_total_executed_19067))
        _total_executed_19067 = (long)DBL_PTR(_total_executed_19067)->dbl;

        /** 		total_lines    = coverage[2]*/
        _2 = (int)SEQ_PTR(_coverage_19156);
        _total_lines_19066 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_total_lines_19066))
        _total_lines_19066 = (long)DBL_PTR(_total_lines_19066)->dbl;

        /** 		percent = calc_percent( total_executed, total_lines )*/

        /** 	if denominator then*/
        if (_total_lines_19066 == 0)
        {
            goto L1A; // [906] 924
        }
        else{
        }

        /** 		return 100 * numerator / denominator*/
        DeRef(_calc_percent_1__tmp_at860_19163);
        if (_total_executed_19067 <= INT15 && _total_executed_19067 >= -INT15)
        _calc_percent_1__tmp_at860_19163 = 100 * _total_executed_19067;
        else
        _calc_percent_1__tmp_at860_19163 = NewDouble(100 * (double)_total_executed_19067);
        DeRef(_percent_19128);
        if (IS_ATOM_INT(_calc_percent_1__tmp_at860_19163)) {
            _percent_19128 = (_calc_percent_1__tmp_at860_19163 % _total_lines_19066) ? NewDouble((double)_calc_percent_1__tmp_at860_19163 / _total_lines_19066) : (_calc_percent_1__tmp_at860_19163 / _total_lines_19066);
        }
        else {
            _percent_19128 = NewDouble(DBL_PTR(_calc_percent_1__tmp_at860_19163)->dbl / (double)_total_lines_19066);
        }
        goto L1B; // [919] 933
        goto L1C; // [921] 932
L1A: 

        /** 		return 100*/
        DeRef(_percent_19128);
        _percent_19128 = 100;
        goto L1B; // [929] 933
L1C: 
L1B: 
        DeRef(_calc_percent_1__tmp_at860_19163);
        _calc_percent_1__tmp_at860_19163 = NOVALUE;

        /** 		routine_coverage[i] = { total_executed - total_lines, keys[i], keys[i],*/
        _10725 = _total_executed_19067 - _total_lines_19066;
        if ((long)((unsigned long)_10725 +(unsigned long) HIGH_BITS) >= 0){
            _10725 = NewDouble((double)_10725);
        }
        _2 = (int)SEQ_PTR(_keys_19148);
        _10726 = (int)*(((s1_ptr)_2)->base + _i_19154);
        _2 = (int)SEQ_PTR(_keys_19148);
        _10727 = (int)*(((s1_ptr)_2)->base + _i_19154);
        _10728 = _total_lines_19066 - _total_executed_19067;
        if ((long)((unsigned long)_10728 +(unsigned long) HIGH_BITS) >= 0){
            _10728 = NewDouble((double)_10728);
        }
        _1 = NewS1(7);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _10725;
        Ref(_10726);
        *((int *)(_2+8)) = _10726;
        Ref(_10727);
        *((int *)(_2+12)) = _10727;
        *((int *)(_2+16)) = _total_executed_19067;
        *((int *)(_2+20)) = _total_lines_19066;
        Ref(_percent_19128);
        *((int *)(_2+24)) = _percent_19128;
        *((int *)(_2+28)) = _10728;
        _10729 = MAKE_SEQ(_1);
        _10728 = NOVALUE;
        _10727 = NOVALUE;
        _10726 = NOVALUE;
        _10725 = NOVALUE;
        _2 = (int)SEQ_PTR(_routine_coverage_19150);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _routine_coverage_19150 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_19154);
        _1 = *(int *)_2;
        *(int *)_2 = _10729;
        if( _1 != _10729 ){
            DeRef(_1);
        }
        _10729 = NOVALUE;
        DeRef(_coverage_19156);
        _coverage_19156 = NOVALUE;

        /** 	end for*/
        _i_19154 = _i_19154 + 1;
        goto L18; // [971] 872
L19: 
        ;
    }

    /** 	routine_coverage = sort( routine_coverage )*/
    RefDS(_routine_coverage_19150);
    _0 = _routine_coverage_19150;
    _routine_coverage_19150 = _21sort(_routine_coverage_19150, 1);
    DeRefDS(_0);

    /** 	puts( out, "<div class='summary_header'>ROUTINE SUMMARY</div>\n" )*/
    EPuts(_out_19046, _10731); // DJP 

    /** 	puts( out, "<table class='overview'><tr><th>Routine</th><th>Executed</th><th>Lines</th><th></th><th>Unexecuted</th></tr>\n" )*/
    EPuts(_out_19046, _10732); // DJP 

    /** 	for i = 1 to length( routine_coverage ) do*/
    if (IS_SEQUENCE(_routine_coverage_19150)){
            _10733 = SEQ_PTR(_routine_coverage_19150)->length;
    }
    else {
        _10733 = 1;
    }
    {
        int _i_19174;
        _i_19174 = 1;
L1D: 
        if (_i_19174 > _10733){
            goto L1E; // [1000] 1066
        }

        /** 		sequence row_class = ""*/
        RefDS(_5);
        DeRefi(_row_class_19176);
        _row_class_19176 = _5;

        /** 		if and_bits( i, 1 ) then*/
        {unsigned long tu;
             tu = (unsigned long)_i_19174 & (unsigned long)1;
             _10734 = MAKE_UINT(tu);
        }
        if (_10734 == 0) {
            DeRef(_10734);
            _10734 = NOVALUE;
            goto L1F; // [1020] 1031
        }
        else {
            if (!IS_ATOM_INT(_10734) && DBL_PTR(_10734)->dbl == 0.0){
                DeRef(_10734);
                _10734 = NOVALUE;
                goto L1F; // [1020] 1031
            }
            DeRef(_10734);
            _10734 = NOVALUE;
        }
        DeRef(_10734);
        _10734 = NOVALUE;

        /** 			row_class = "shade-row"*/
        RefDS(_10735);
        DeRefDSi(_row_class_19176);
        _row_class_19176 = _10735;
L1F: 

        /** 		printf( out, ROUTINE_SUMMARY_ROW, { row_class } & routine_coverage[i][2..$] )*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_row_class_19176);
        *((int *)(_2+4)) = _row_class_19176;
        _10736 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_routine_coverage_19150);
        _10737 = (int)*(((s1_ptr)_2)->base + _i_19174);
        if (IS_SEQUENCE(_10737)){
                _10738 = SEQ_PTR(_10737)->length;
        }
        else {
            _10738 = 1;
        }
        rhs_slice_target = (object_ptr)&_10739;
        RHS_Slice(_10737, 2, _10738);
        _10737 = NOVALUE;
        Concat((object_ptr)&_10740, _10736, _10739);
        DeRefDS(_10736);
        _10736 = NOVALUE;
        DeRef(_10736);
        _10736 = NOVALUE;
        DeRefDS(_10739);
        _10739 = NOVALUE;
        EPrintf(_out_19046, _1ROUTINE_SUMMARY_ROW_18809, _10740);
        DeRefDS(_10740);
        _10740 = NOVALUE;
        DeRefDSi(_row_class_19176);
        _row_class_19176 = NOVALUE;

        /** 	end for*/
        _i_19174 = _i_19174 + 1;
        goto L1D; // [1061] 1007
L1E: 
        ;
    }

    /** 	puts( out, "</table>\n" )*/
    EPuts(_out_19046, _10716); // DJP 

    /** 	puts( out, "<div class='summary_header'>LINE COVERAGE DETAIL</div>\n" )*/
    EPuts(_out_19046, _10741); // DJP 

    /** 	puts( out, "<table class='code'><tr><th class='num'>#</th><th><span style='margin-left: 1em;'>Executed</span></th><th></th></tr>\n" )*/
    EPuts(_out_19046, _10742); // DJP 

    /** 	for i = 1 to length( source_lines ) do*/
    if (IS_SEQUENCE(_source_lines_19048)){
            _10743 = SEQ_PTR(_source_lines_19048)->length;
    }
    else {
        _10743 = 1;
    }
    {
        int _i_19188;
        _i_19188 = 1;
L20: 
        if (_i_19188 > _10743){
            goto L21; // [1086] 1109
        }

        /** 		puts( out, source_lines[i] )*/
        _2 = (int)SEQ_PTR(_source_lines_19048);
        _10744 = (int)*(((s1_ptr)_2)->base + _i_19188);
        EPuts(_out_19046, _10744); // DJP 
        _10744 = NOVALUE;

        /** 	end for*/
        _i_19188 = _i_19188 + 1;
        goto L20; // [1104] 1093
L21: 
        ;
    }

    /** 	puts( out, "</table>\n" )*/
    EPuts(_out_19046, _10716); // DJP 

    /** 	puts( out, FOOTER )*/
    EPuts(_out_19046, _1FOOTER_18793); // DJP 

    /** end procedure*/
    DeRefDS(_output_dir_19033);
    DeRef(_in_19035);
    DeRef(_html_name_19040);
    DeRef(_out_19046);
    DeRef(_source_lines_19048);
    DeRef(_lines_19051);
    DeRef(_routines_19054);
    DeRefi(_line_19056);
    DeRef(_routine_lines_19058);
    DeRef(_routine_name_19065);
    DeRef(_percent_19128);
    DeRef(_keys_19148);
    DeRef(_routine_coverage_19150);
    return;
    ;
}


void _1write_summary(int _output_directory_19193)
{
    int _out_19194 = NOVALUE;
    int _total_lines_19199 = NOVALUE;
    int _total_lines_executed_19200 = NOVALUE;
    int _total_routines_19201 = NOVALUE;
    int _total_routines_executed_19202 = NOVALUE;
    int _total_files_executed_19203 = NOVALUE;
    int _file_data_19204 = NOVALUE;
    int _coverage_19210 = NOVALUE;
    int _routine_percent_19212 = NOVALUE;
    int _calc_percent_1__tmp_at107_19219 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_107_19218 = NOVALUE;
    int _denominator_inlined_calc_percent_at_104_19217 = NOVALUE;
    int _numerator_inlined_calc_percent_at_101_19216 = NOVALUE;
    int _line_percent_19220 = NOVALUE;
    int _calc_percent_1__tmp_at158_19227 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_158_19226 = NOVALUE;
    int _denominator_inlined_calc_percent_at_155_19225 = NOVALUE;
    int _numerator_inlined_calc_percent_at_152_19224 = NOVALUE;
    int _html_name_19239 = NOVALUE;
    int _calc_percent_1__tmp_at449_19276 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_449_19275 = NOVALUE;
    int _denominator_inlined_calc_percent_at_446_19274 = NOVALUE;
    int _calc_percent_1__tmp_at480_19279 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_480_19278 = NOVALUE;
    int _calc_percent_1__tmp_at511_19282 = NOVALUE;
    int _calc_percent_inlined_calc_percent_at_511_19281 = NOVALUE;
    int _row_class_19292 = NOVALUE;
    int _10809 = NOVALUE;
    int _10808 = NOVALUE;
    int _10807 = NOVALUE;
    int _10806 = NOVALUE;
    int _10805 = NOVALUE;
    int _10804 = NOVALUE;
    int _10803 = NOVALUE;
    int _10802 = NOVALUE;
    int _10798 = NOVALUE;
    int _10797 = NOVALUE;
    int _10796 = NOVALUE;
    int _10795 = NOVALUE;
    int _10794 = NOVALUE;
    int _10787 = NOVALUE;
    int _10784 = NOVALUE;
    int _10783 = NOVALUE;
    int _10782 = NOVALUE;
    int _10781 = NOVALUE;
    int _10780 = NOVALUE;
    int _10779 = NOVALUE;
    int _10778 = NOVALUE;
    int _10777 = NOVALUE;
    int _10776 = NOVALUE;
    int _10775 = NOVALUE;
    int _10774 = NOVALUE;
    int _10773 = NOVALUE;
    int _10772 = NOVALUE;
    int _10770 = NOVALUE;
    int _10769 = NOVALUE;
    int _10768 = NOVALUE;
    int _10765 = NOVALUE;
    int _10764 = NOVALUE;
    int _10762 = NOVALUE;
    int _10760 = NOVALUE;
    int _10758 = NOVALUE;
    int _10756 = NOVALUE;
    int _10755 = NOVALUE;
    int _10754 = NOVALUE;
    int _10753 = NOVALUE;
    int _10752 = NOVALUE;
    int _10750 = NOVALUE;
    int _10748 = NOVALUE;
    int _10746 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom out = open( output_directory & SLASH & "index.html", "w", 1 )*/
    {
        int concat_list[3];

        concat_list[0] = _10745;
        concat_list[1] = 92;
        concat_list[2] = _output_directory_19193;
        Concat_N((object_ptr)&_10746, concat_list, 3);
    }
    DeRef(_out_19194);
    _out_19194 = EOpen(_10746, _889, 1);
    DeRefDS(_10746);
    _10746 = NOVALUE;

    /** 	integer total_lines             = 0*/
    _total_lines_19199 = 0;

    /** 	integer total_lines_executed    = 0*/
    _total_lines_executed_19200 = 0;

    /** 	integer total_routines          = 0*/
    _total_routines_19201 = 0;

    /** 	integer total_routines_executed = 0*/
    _total_routines_executed_19202 = 0;

    /** 	integer total_files_executed    = 0*/
    _total_files_executed_19203 = 0;

    /** 	sequence file_data = repeat( 0, length( file_coverage ) )*/
    if (IS_SEQUENCE(_1file_coverage_18839)){
            _10748 = SEQ_PTR(_1file_coverage_18839)->length;
    }
    else {
        _10748 = 1;
    }
    DeRef(_file_data_19204);
    _file_data_19204 = Repeat(0, _10748);
    _10748 = NOVALUE;

    /** 	for i = 1 to length( file_coverage ) do*/
    if (IS_SEQUENCE(_1file_coverage_18839)){
            _10750 = SEQ_PTR(_1file_coverage_18839)->length;
    }
    else {
        _10750 = 1;
    }
    {
        int _i_19208;
        _i_19208 = 1;
L1: 
        if (_i_19208 > _10750){
            goto L2; // [71] 395
        }

        /** 		sequence coverage = file_coverage[i]*/
        DeRef(_coverage_19210);
        _2 = (int)SEQ_PTR(_1file_coverage_18839);
        _coverage_19210 = (int)*(((s1_ptr)_2)->base + _i_19208);
        Ref(_coverage_19210);

        /** 		atom routine_percent = calc_percent( coverage[COV_FUNCS_TESTED], coverage[COV_FUNCS] )*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10752 = (int)*(((s1_ptr)_2)->base + 1);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10753 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_10752);
        DeRef(_numerator_inlined_calc_percent_at_101_19216);
        _numerator_inlined_calc_percent_at_101_19216 = _10752;
        _10752 = NOVALUE;
        Ref(_10753);
        DeRef(_denominator_inlined_calc_percent_at_104_19217);
        _denominator_inlined_calc_percent_at_104_19217 = _10753;
        _10753 = NOVALUE;

        /** 	if denominator then*/
        if (_denominator_inlined_calc_percent_at_104_19217 == 0) {
            goto L3; // [110] 128
        }
        else {
            if (!IS_ATOM_INT(_denominator_inlined_calc_percent_at_104_19217) && DBL_PTR(_denominator_inlined_calc_percent_at_104_19217)->dbl == 0.0){
                goto L3; // [110] 128
            }
        }

        /** 		return 100 * numerator / denominator*/
        DeRef(_calc_percent_1__tmp_at107_19219);
        if (IS_ATOM_INT(_numerator_inlined_calc_percent_at_101_19216)) {
            if (_numerator_inlined_calc_percent_at_101_19216 <= INT15 && _numerator_inlined_calc_percent_at_101_19216 >= -INT15)
            _calc_percent_1__tmp_at107_19219 = 100 * _numerator_inlined_calc_percent_at_101_19216;
            else
            _calc_percent_1__tmp_at107_19219 = NewDouble(100 * (double)_numerator_inlined_calc_percent_at_101_19216);
        }
        else {
            _calc_percent_1__tmp_at107_19219 = binary_op(MULTIPLY, 100, _numerator_inlined_calc_percent_at_101_19216);
        }
        DeRef(_routine_percent_19212);
        if (IS_ATOM_INT(_calc_percent_1__tmp_at107_19219) && IS_ATOM_INT(_denominator_inlined_calc_percent_at_104_19217)) {
            _routine_percent_19212 = (_calc_percent_1__tmp_at107_19219 % _denominator_inlined_calc_percent_at_104_19217) ? NewDouble((double)_calc_percent_1__tmp_at107_19219 / _denominator_inlined_calc_percent_at_104_19217) : (_calc_percent_1__tmp_at107_19219 / _denominator_inlined_calc_percent_at_104_19217);
        }
        else {
            _routine_percent_19212 = binary_op(DIVIDE, _calc_percent_1__tmp_at107_19219, _denominator_inlined_calc_percent_at_104_19217);
        }
        goto L4; // [123] 137
        goto L5; // [125] 136
L3: 

        /** 		return 100*/
        DeRef(_routine_percent_19212);
        _routine_percent_19212 = 100;
        goto L4; // [133] 137
L5: 
L4: 
        DeRef(_numerator_inlined_calc_percent_at_101_19216);
        _numerator_inlined_calc_percent_at_101_19216 = NOVALUE;
        DeRef(_denominator_inlined_calc_percent_at_104_19217);
        _denominator_inlined_calc_percent_at_104_19217 = NOVALUE;
        DeRef(_calc_percent_1__tmp_at107_19219);
        _calc_percent_1__tmp_at107_19219 = NOVALUE;

        /** 		atom line_percent    = calc_percent( coverage[COV_LINES_TESTED], coverage[COV_LINES] )*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10754 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10755 = (int)*(((s1_ptr)_2)->base + 4);
        Ref(_10754);
        DeRef(_numerator_inlined_calc_percent_at_152_19224);
        _numerator_inlined_calc_percent_at_152_19224 = _10754;
        _10754 = NOVALUE;
        Ref(_10755);
        DeRef(_denominator_inlined_calc_percent_at_155_19225);
        _denominator_inlined_calc_percent_at_155_19225 = _10755;
        _10755 = NOVALUE;

        /** 	if denominator then*/
        if (_denominator_inlined_calc_percent_at_155_19225 == 0) {
            goto L6; // [161] 179
        }
        else {
            if (!IS_ATOM_INT(_denominator_inlined_calc_percent_at_155_19225) && DBL_PTR(_denominator_inlined_calc_percent_at_155_19225)->dbl == 0.0){
                goto L6; // [161] 179
            }
        }

        /** 		return 100 * numerator / denominator*/
        DeRef(_calc_percent_1__tmp_at158_19227);
        if (IS_ATOM_INT(_numerator_inlined_calc_percent_at_152_19224)) {
            if (_numerator_inlined_calc_percent_at_152_19224 <= INT15 && _numerator_inlined_calc_percent_at_152_19224 >= -INT15)
            _calc_percent_1__tmp_at158_19227 = 100 * _numerator_inlined_calc_percent_at_152_19224;
            else
            _calc_percent_1__tmp_at158_19227 = NewDouble(100 * (double)_numerator_inlined_calc_percent_at_152_19224);
        }
        else {
            _calc_percent_1__tmp_at158_19227 = binary_op(MULTIPLY, 100, _numerator_inlined_calc_percent_at_152_19224);
        }
        DeRef(_line_percent_19220);
        if (IS_ATOM_INT(_calc_percent_1__tmp_at158_19227) && IS_ATOM_INT(_denominator_inlined_calc_percent_at_155_19225)) {
            _line_percent_19220 = (_calc_percent_1__tmp_at158_19227 % _denominator_inlined_calc_percent_at_155_19225) ? NewDouble((double)_calc_percent_1__tmp_at158_19227 / _denominator_inlined_calc_percent_at_155_19225) : (_calc_percent_1__tmp_at158_19227 / _denominator_inlined_calc_percent_at_155_19225);
        }
        else {
            _line_percent_19220 = binary_op(DIVIDE, _calc_percent_1__tmp_at158_19227, _denominator_inlined_calc_percent_at_155_19225);
        }
        goto L7; // [174] 188
        goto L8; // [176] 187
L6: 

        /** 		return 100*/
        DeRef(_line_percent_19220);
        _line_percent_19220 = 100;
        goto L7; // [184] 188
L8: 
L7: 
        DeRef(_numerator_inlined_calc_percent_at_152_19224);
        _numerator_inlined_calc_percent_at_152_19224 = NOVALUE;
        DeRef(_denominator_inlined_calc_percent_at_155_19225);
        _denominator_inlined_calc_percent_at_155_19225 = NOVALUE;
        DeRef(_calc_percent_1__tmp_at158_19227);
        _calc_percent_1__tmp_at158_19227 = NOVALUE;

        /** 		total_lines             += coverage[COV_LINES]*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10756 = (int)*(((s1_ptr)_2)->base + 4);
        if (IS_ATOM_INT(_10756)) {
            _total_lines_19199 = _total_lines_19199 + _10756;
        }
        else {
            _total_lines_19199 = binary_op(PLUS, _total_lines_19199, _10756);
        }
        _10756 = NOVALUE;
        if (!IS_ATOM_INT(_total_lines_19199)) {
            _1 = (long)(DBL_PTR(_total_lines_19199)->dbl);
            if (UNIQUE(DBL_PTR(_total_lines_19199)) && (DBL_PTR(_total_lines_19199)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_total_lines_19199);
            _total_lines_19199 = _1;
        }

        /** 		total_lines_executed    += coverage[COV_LINES_TESTED]*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10758 = (int)*(((s1_ptr)_2)->base + 3);
        if (IS_ATOM_INT(_10758)) {
            _total_lines_executed_19200 = _total_lines_executed_19200 + _10758;
        }
        else {
            _total_lines_executed_19200 = binary_op(PLUS, _total_lines_executed_19200, _10758);
        }
        _10758 = NOVALUE;
        if (!IS_ATOM_INT(_total_lines_executed_19200)) {
            _1 = (long)(DBL_PTR(_total_lines_executed_19200)->dbl);
            if (UNIQUE(DBL_PTR(_total_lines_executed_19200)) && (DBL_PTR(_total_lines_executed_19200)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_total_lines_executed_19200);
            _total_lines_executed_19200 = _1;
        }

        /** 		total_routines          += coverage[COV_FUNCS]*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10760 = (int)*(((s1_ptr)_2)->base + 2);
        if (IS_ATOM_INT(_10760)) {
            _total_routines_19201 = _total_routines_19201 + _10760;
        }
        else {
            _total_routines_19201 = binary_op(PLUS, _total_routines_19201, _10760);
        }
        _10760 = NOVALUE;
        if (!IS_ATOM_INT(_total_routines_19201)) {
            _1 = (long)(DBL_PTR(_total_routines_19201)->dbl);
            if (UNIQUE(DBL_PTR(_total_routines_19201)) && (DBL_PTR(_total_routines_19201)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_total_routines_19201);
            _total_routines_19201 = _1;
        }

        /** 		total_routines_executed += coverage[COV_FUNCS_TESTED]*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10762 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_10762)) {
            _total_routines_executed_19202 = _total_routines_executed_19202 + _10762;
        }
        else {
            _total_routines_executed_19202 = binary_op(PLUS, _total_routines_executed_19202, _10762);
        }
        _10762 = NOVALUE;
        if (!IS_ATOM_INT(_total_routines_executed_19202)) {
            _1 = (long)(DBL_PTR(_total_routines_executed_19202)->dbl);
            if (UNIQUE(DBL_PTR(_total_routines_executed_19202)) && (DBL_PTR(_total_routines_executed_19202)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_total_routines_executed_19202);
            _total_routines_executed_19202 = _1;
        }

        /** 		total_files_executed    += 0 != coverage[COV_LINES_TESTED]*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10764 = (int)*(((s1_ptr)_2)->base + 3);
        if (IS_ATOM_INT(_10764)) {
            _10765 = (0 != _10764);
        }
        else {
            _10765 = binary_op(NOTEQ, 0, _10764);
        }
        _10764 = NOVALUE;
        if (IS_ATOM_INT(_10765)) {
            _total_files_executed_19203 = _total_files_executed_19203 + _10765;
        }
        else {
            _total_files_executed_19203 = binary_op(PLUS, _total_files_executed_19203, _10765);
        }
        DeRef(_10765);
        _10765 = NOVALUE;
        if (!IS_ATOM_INT(_total_files_executed_19203)) {
            _1 = (long)(DBL_PTR(_total_files_executed_19203)->dbl);
            if (UNIQUE(DBL_PTR(_total_files_executed_19203)) && (DBL_PTR(_total_files_executed_19203)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_total_files_executed_19203);
            _total_files_executed_19203 = _1;
        }

        /** 		sequence html_name = "files/" & encode( encode( short_names[i] ) ) & ".html"*/
        _2 = (int)SEQ_PTR(_1short_names_18840);
        _10768 = (int)*(((s1_ptr)_2)->base + _i_19208);
        RefDS(_10768);
        RefDS(_9690);
        _10769 = _34encode(_10768, _9690);
        _10768 = NOVALUE;
        RefDS(_9690);
        _10770 = _34encode(_10769, _9690);
        _10769 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _10659;
            concat_list[1] = _10770;
            concat_list[2] = _10767;
            Concat_N((object_ptr)&_html_name_19239, concat_list, 3);
        }
        DeRef(_10770);
        _10770 = NOVALUE;

        /** 		file_data[i] = { coverage[COV_LINES_TESTED] - coverage[COV_LINES], short_names[i], html_name,*/
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10772 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10773 = (int)*(((s1_ptr)_2)->base + 4);
        if (IS_ATOM_INT(_10772) && IS_ATOM_INT(_10773)) {
            _10774 = _10772 - _10773;
            if ((long)((unsigned long)_10774 +(unsigned long) HIGH_BITS) >= 0){
                _10774 = NewDouble((double)_10774);
            }
        }
        else {
            _10774 = binary_op(MINUS, _10772, _10773);
        }
        _10772 = NOVALUE;
        _10773 = NOVALUE;
        _2 = (int)SEQ_PTR(_1short_names_18840);
        _10775 = (int)*(((s1_ptr)_2)->base + _i_19208);
        _2 = (int)SEQ_PTR(_1short_names_18840);
        _10776 = (int)*(((s1_ptr)_2)->base + _i_19208);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10777 = (int)*(((s1_ptr)_2)->base + 1);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10778 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10779 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10780 = (int)*(((s1_ptr)_2)->base + 4);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10781 = (int)*(((s1_ptr)_2)->base + 4);
        _2 = (int)SEQ_PTR(_coverage_19210);
        _10782 = (int)*(((s1_ptr)_2)->base + 3);
        if (IS_ATOM_INT(_10781) && IS_ATOM_INT(_10782)) {
            _10783 = _10781 - _10782;
            if ((long)((unsigned long)_10783 +(unsigned long) HIGH_BITS) >= 0){
                _10783 = NewDouble((double)_10783);
            }
        }
        else {
            _10783 = binary_op(MINUS, _10781, _10782);
        }
        _10781 = NOVALUE;
        _10782 = NOVALUE;
        _1 = NewS1(11);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _10774;
        RefDS(_10775);
        *((int *)(_2+8)) = _10775;
        RefDS(_html_name_19239);
        *((int *)(_2+12)) = _html_name_19239;
        RefDS(_10776);
        *((int *)(_2+16)) = _10776;
        Ref(_10777);
        *((int *)(_2+20)) = _10777;
        Ref(_10778);
        *((int *)(_2+24)) = _10778;
        Ref(_routine_percent_19212);
        *((int *)(_2+28)) = _routine_percent_19212;
        Ref(_10779);
        *((int *)(_2+32)) = _10779;
        Ref(_10780);
        *((int *)(_2+36)) = _10780;
        Ref(_line_percent_19220);
        *((int *)(_2+40)) = _line_percent_19220;
        *((int *)(_2+44)) = _10783;
        _10784 = MAKE_SEQ(_1);
        _10783 = NOVALUE;
        _10780 = NOVALUE;
        _10779 = NOVALUE;
        _10778 = NOVALUE;
        _10777 = NOVALUE;
        _10776 = NOVALUE;
        _10775 = NOVALUE;
        _10774 = NOVALUE;
        _2 = (int)SEQ_PTR(_file_data_19204);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _file_data_19204 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_19208);
        _1 = *(int *)_2;
        *(int *)_2 = _10784;
        if( _1 != _10784 ){
            DeRef(_1);
        }
        _10784 = NOVALUE;
        DeRefDS(_coverage_19210);
        _coverage_19210 = NOVALUE;
        DeRef(_routine_percent_19212);
        _routine_percent_19212 = NOVALUE;
        DeRef(_line_percent_19220);
        _line_percent_19220 = NOVALUE;
        DeRefDS(_html_name_19239);
        _html_name_19239 = NOVALUE;

        /** 	end for*/
        _i_19208 = _i_19208 + 1;
        goto L1; // [390] 78
L2: 
        ;
    }

    /** 	file_data = sort( file_data )*/
    RefDS(_file_data_19204);
    _0 = _file_data_19204;
    _file_data_19204 = _21sort(_file_data_19204, 1);
    DeRefDS(_0);

    /** 	printf( out, HEADER, { "Coverage Summary", stylesheet } )*/
    RefDS(_1stylesheet_18835);
    RefDS(_10786);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _10786;
    ((int *)_2)[2] = _1stylesheet_18835;
    _10787 = MAKE_SEQ(_1);
    EPrintf(_out_19194, _1HEADER_18791, _10787);
    DeRefDS(_10787);
    _10787 = NOVALUE;

    /** 	puts( out, "<div class='summary_header'>COVERAGE SUMMARY</div>\n" )*/
    EPuts(_out_19194, _10788); // DJP 

    /** 	puts( out, "<table class='summary'><tr><th>Files</th><th>Routines</th><th>Lines</th><th>Unexecuted</th></tr>\n" )*/
    EPuts(_out_19194, _10789); // DJP 

    /** 	printf( out,*/
    {
        int concat_list[6];

        concat_list[0] = _10793;
        concat_list[1] = _10792;
        concat_list[2] = _10791;
        concat_list[3] = _10791;
        concat_list[4] = _10791;
        concat_list[5] = _10790;
        Concat_N((object_ptr)&_10794, concat_list, 6);
    }
    if (IS_SEQUENCE(_1files_18838)){
            _10795 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _10795 = 1;
    }
    if (IS_SEQUENCE(_1files_18838)){
            _10796 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _10796 = 1;
    }
    _denominator_inlined_calc_percent_at_446_19274 = _10796;
    _10796 = NOVALUE;

    /** 	if denominator then*/
    if (_denominator_inlined_calc_percent_at_446_19274 == 0)
    {
        goto L9; // [452] 470
    }
    else{
    }

    /** 		return 100 * numerator / denominator*/
    DeRef(_calc_percent_1__tmp_at449_19276);
    if (_total_files_executed_19203 <= INT15 && _total_files_executed_19203 >= -INT15)
    _calc_percent_1__tmp_at449_19276 = 100 * _total_files_executed_19203;
    else
    _calc_percent_1__tmp_at449_19276 = NewDouble(100 * (double)_total_files_executed_19203);
    DeRef(_calc_percent_inlined_calc_percent_at_449_19275);
    if (IS_ATOM_INT(_calc_percent_1__tmp_at449_19276)) {
        _calc_percent_inlined_calc_percent_at_449_19275 = (_calc_percent_1__tmp_at449_19276 % _denominator_inlined_calc_percent_at_446_19274) ? NewDouble((double)_calc_percent_1__tmp_at449_19276 / _denominator_inlined_calc_percent_at_446_19274) : (_calc_percent_1__tmp_at449_19276 / _denominator_inlined_calc_percent_at_446_19274);
    }
    else {
        _calc_percent_inlined_calc_percent_at_449_19275 = NewDouble(DBL_PTR(_calc_percent_1__tmp_at449_19276)->dbl / (double)_denominator_inlined_calc_percent_at_446_19274);
    }
    goto LA; // [465] 479
    goto LB; // [467] 478
L9: 

    /** 		return 100*/
    DeRef(_calc_percent_inlined_calc_percent_at_449_19275);
    _calc_percent_inlined_calc_percent_at_449_19275 = 100;
    goto LA; // [475] 479
LB: 
LA: 
    DeRef(_calc_percent_1__tmp_at449_19276);
    _calc_percent_1__tmp_at449_19276 = NOVALUE;

    /** 	if denominator then*/
    if (_total_routines_19201 == 0)
    {
        goto LC; // [483] 501
    }
    else{
    }

    /** 		return 100 * numerator / denominator*/
    DeRef(_calc_percent_1__tmp_at480_19279);
    if (_total_routines_executed_19202 <= INT15 && _total_routines_executed_19202 >= -INT15)
    _calc_percent_1__tmp_at480_19279 = 100 * _total_routines_executed_19202;
    else
    _calc_percent_1__tmp_at480_19279 = NewDouble(100 * (double)_total_routines_executed_19202);
    DeRef(_calc_percent_inlined_calc_percent_at_480_19278);
    if (IS_ATOM_INT(_calc_percent_1__tmp_at480_19279)) {
        _calc_percent_inlined_calc_percent_at_480_19278 = (_calc_percent_1__tmp_at480_19279 % _total_routines_19201) ? NewDouble((double)_calc_percent_1__tmp_at480_19279 / _total_routines_19201) : (_calc_percent_1__tmp_at480_19279 / _total_routines_19201);
    }
    else {
        _calc_percent_inlined_calc_percent_at_480_19278 = NewDouble(DBL_PTR(_calc_percent_1__tmp_at480_19279)->dbl / (double)_total_routines_19201);
    }
    goto LD; // [496] 510
    goto LE; // [498] 509
LC: 

    /** 		return 100*/
    DeRef(_calc_percent_inlined_calc_percent_at_480_19278);
    _calc_percent_inlined_calc_percent_at_480_19278 = 100;
    goto LD; // [506] 510
LE: 
LD: 
    DeRef(_calc_percent_1__tmp_at480_19279);
    _calc_percent_1__tmp_at480_19279 = NOVALUE;

    /** 	if denominator then*/
    if (_total_lines_19199 == 0)
    {
        goto LF; // [514] 532
    }
    else{
    }

    /** 		return 100 * numerator / denominator*/
    DeRef(_calc_percent_1__tmp_at511_19282);
    if (_total_lines_executed_19200 <= INT15 && _total_lines_executed_19200 >= -INT15)
    _calc_percent_1__tmp_at511_19282 = 100 * _total_lines_executed_19200;
    else
    _calc_percent_1__tmp_at511_19282 = NewDouble(100 * (double)_total_lines_executed_19200);
    DeRef(_calc_percent_inlined_calc_percent_at_511_19281);
    if (IS_ATOM_INT(_calc_percent_1__tmp_at511_19282)) {
        _calc_percent_inlined_calc_percent_at_511_19281 = (_calc_percent_1__tmp_at511_19282 % _total_lines_19199) ? NewDouble((double)_calc_percent_1__tmp_at511_19282 / _total_lines_19199) : (_calc_percent_1__tmp_at511_19282 / _total_lines_19199);
    }
    else {
        _calc_percent_inlined_calc_percent_at_511_19281 = NewDouble(DBL_PTR(_calc_percent_1__tmp_at511_19282)->dbl / (double)_total_lines_19199);
    }
    goto L10; // [527] 541
    goto L11; // [529] 540
LF: 

    /** 		return 100*/
    DeRef(_calc_percent_inlined_calc_percent_at_511_19281);
    _calc_percent_inlined_calc_percent_at_511_19281 = 100;
    goto L10; // [537] 541
L11: 
L10: 
    DeRef(_calc_percent_1__tmp_at511_19282);
    _calc_percent_1__tmp_at511_19282 = NOVALUE;
    _10797 = _total_lines_19199 - _total_lines_executed_19200;
    if ((long)((unsigned long)_10797 +(unsigned long) HIGH_BITS) >= 0){
        _10797 = NewDouble((double)_10797);
    }
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _total_files_executed_19203;
    *((int *)(_2+8)) = _10795;
    Ref(_calc_percent_inlined_calc_percent_at_449_19275);
    *((int *)(_2+12)) = _calc_percent_inlined_calc_percent_at_449_19275;
    *((int *)(_2+16)) = _total_routines_executed_19202;
    *((int *)(_2+20)) = _total_routines_19201;
    Ref(_calc_percent_inlined_calc_percent_at_480_19278);
    *((int *)(_2+24)) = _calc_percent_inlined_calc_percent_at_480_19278;
    *((int *)(_2+28)) = _total_lines_executed_19200;
    *((int *)(_2+32)) = _total_lines_19199;
    Ref(_calc_percent_inlined_calc_percent_at_511_19281);
    *((int *)(_2+36)) = _calc_percent_inlined_calc_percent_at_511_19281;
    *((int *)(_2+40)) = _10797;
    _10798 = MAKE_SEQ(_1);
    _10797 = NOVALUE;
    _10795 = NOVALUE;
    EPrintf(_out_19194, _10794, _10798);
    DeRef(_10794);
    _10794 = NOVALUE;
    DeRefDS(_10798);
    _10798 = NOVALUE;

    /** 	puts( out, "</table>\n" )*/
    EPuts(_out_19194, _10716); // DJP 

    /** 	puts( out, "<div class='summary_header'>FILES COVERAGE SUMMARY</div>\n" )*/
    EPuts(_out_19194, _10799); // DJP 

    /** 	puts( out, "<table class='summary'><tr><th>Name</th><th>Executed</th><th>Routines</th><th><span style='margin-left:3em;'>%</span></th>" &*/
    Concat((object_ptr)&_10802, _10800, _10801);
    EPuts(_out_19194, _10802); // DJP 
    DeRefDS(_10802);
    _10802 = NOVALUE;

    /** 	for i = 1 to length( file_data ) do*/
    if (IS_SEQUENCE(_file_data_19204)){
            _10803 = SEQ_PTR(_file_data_19204)->length;
    }
    else {
        _10803 = 1;
    }
    {
        int _i_19290;
        _i_19290 = 1;
L12: 
        if (_i_19290 > _10803){
            goto L13; // [588] 654
        }

        /** 		sequence row_class = ""*/
        RefDS(_5);
        DeRefi(_row_class_19292);
        _row_class_19292 = _5;

        /** 		if and_bits( i, 1 ) then*/
        {unsigned long tu;
             tu = (unsigned long)_i_19290 & (unsigned long)1;
             _10804 = MAKE_UINT(tu);
        }
        if (_10804 == 0) {
            DeRef(_10804);
            _10804 = NOVALUE;
            goto L14; // [608] 619
        }
        else {
            if (!IS_ATOM_INT(_10804) && DBL_PTR(_10804)->dbl == 0.0){
                DeRef(_10804);
                _10804 = NOVALUE;
                goto L14; // [608] 619
            }
            DeRef(_10804);
            _10804 = NOVALUE;
        }
        DeRef(_10804);
        _10804 = NOVALUE;

        /** 			row_class = "shade-row"*/
        RefDS(_10735);
        DeRefDSi(_row_class_19292);
        _row_class_19292 = _10735;
L14: 

        /** 		printf( out, FILE_SUMMARY_ROW, {row_class} & file_data[i][3..$] )*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_row_class_19292);
        *((int *)(_2+4)) = _row_class_19292;
        _10805 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_file_data_19204);
        _10806 = (int)*(((s1_ptr)_2)->base + _i_19290);
        if (IS_SEQUENCE(_10806)){
                _10807 = SEQ_PTR(_10806)->length;
        }
        else {
            _10807 = 1;
        }
        rhs_slice_target = (object_ptr)&_10808;
        RHS_Slice(_10806, 3, _10807);
        _10806 = NOVALUE;
        Concat((object_ptr)&_10809, _10805, _10808);
        DeRefDS(_10805);
        _10805 = NOVALUE;
        DeRef(_10805);
        _10805 = NOVALUE;
        DeRefDS(_10808);
        _10808 = NOVALUE;
        EPrintf(_out_19194, _1FILE_SUMMARY_ROW_18805, _10809);
        DeRefDS(_10809);
        _10809 = NOVALUE;
        DeRefDSi(_row_class_19292);
        _row_class_19292 = NOVALUE;

        /** 	end for*/
        _i_19290 = _i_19290 + 1;
        goto L12; // [649] 595
L13: 
        ;
    }

    /** 	puts( out, "</table>\n" )*/
    EPuts(_out_19194, _10716); // DJP 

    /** 	puts( out, FOOTER )*/
    EPuts(_out_19194, _1FOOTER_18793); // DJP 

    /** end procedure*/
    DeRefDS(_output_directory_19193);
    DeRef(_out_19194);
    DeRef(_file_data_19204);
    return;
    ;
}


int _1get_common(int _a_19302, int _b_19303)
{
    int _len_19304 = NOVALUE;
    int _10820 = NOVALUE;
    int _10819 = NOVALUE;
    int _10816 = NOVALUE;
    int _10815 = NOVALUE;
    int _10814 = NOVALUE;
    int _10813 = NOVALUE;
    int _10812 = NOVALUE;
    int _10811 = NOVALUE;
    int _10810 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer len = 1*/
    _len_19304 = 1;

    /** 	while len <= length(a) and len <= length(b) do*/
L1: 
    if (IS_SEQUENCE(_a_19302)){
            _10810 = SEQ_PTR(_a_19302)->length;
    }
    else {
        _10810 = 1;
    }
    _10811 = (_len_19304 <= _10810);
    _10810 = NOVALUE;
    if (_10811 == 0) {
        goto L2; // [24] 71
    }
    if (IS_SEQUENCE(_b_19303)){
            _10813 = SEQ_PTR(_b_19303)->length;
    }
    else {
        _10813 = 1;
    }
    _10814 = (_len_19304 <= _10813);
    _10813 = NOVALUE;
    if (_10814 == 0)
    {
        DeRef(_10814);
        _10814 = NOVALUE;
        goto L2; // [36] 71
    }
    else{
        DeRef(_10814);
        _10814 = NOVALUE;
    }

    /** 		if a[len] != b[len] then*/
    _2 = (int)SEQ_PTR(_a_19302);
    _10815 = (int)*(((s1_ptr)_2)->base + _len_19304);
    _2 = (int)SEQ_PTR(_b_19303);
    _10816 = (int)*(((s1_ptr)_2)->base + _len_19304);
    if (binary_op_a(EQUALS, _10815, _10816)){
        _10815 = NOVALUE;
        _10816 = NOVALUE;
        goto L3; // [49] 58
    }
    _10815 = NOVALUE;
    _10816 = NOVALUE;

    /** 			exit*/
    goto L2; // [55] 71
L3: 

    /** 		len += 1*/
    _len_19304 = _len_19304 + 1;

    /** 	end while*/
    goto L1; // [68] 17
L2: 

    /** 	return a[1..len-1]*/
    _10819 = _len_19304 - 1;
    rhs_slice_target = (object_ptr)&_10820;
    RHS_Slice(_a_19302, 1, _10819);
    DeRefDS(_a_19302);
    DeRefDS(_b_19303);
    DeRef(_10811);
    _10811 = NOVALUE;
    _10819 = NOVALUE;
    return _10820;
    ;
}


void _1shorten_names()
{
    int _common_19323 = NOVALUE;
    int _size_19330 = NOVALUE;
    int _10839 = NOVALUE;
    int _10838 = NOVALUE;
    int _10837 = NOVALUE;
    int _10835 = NOVALUE;
    int _10834 = NOVALUE;
    int _10833 = NOVALUE;
    int _10832 = NOVALUE;
    int _10831 = NOVALUE;
    int _10830 = NOVALUE;
    int _10829 = NOVALUE;
    int _10828 = NOVALUE;
    int _10825 = NOVALUE;
    int _10824 = NOVALUE;
    int _10821 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(files) then*/
    if (IS_SEQUENCE(_1files_18838)){
            _10821 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _10821 = 1;
    }
    if (_10821 != 0)
    goto L1; // [8] 17
    _10821 = NOVALUE;

    /** 		return*/
    DeRef(_common_19323);
    return;
L1: 

    /** 	sequence common = files[1]*/
    DeRef(_common_19323);
    _2 = (int)SEQ_PTR(_1files_18838);
    _common_19323 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_common_19323);

    /** 	for i = 2 to length(files) do*/
    if (IS_SEQUENCE(_1files_18838)){
            _10824 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _10824 = 1;
    }
    {
        int _i_19326;
        _i_19326 = 2;
L2: 
        if (_i_19326 > _10824){
            goto L3; // [34] 63
        }

        /** 		common = get_common( common, files[i] )*/
        _2 = (int)SEQ_PTR(_1files_18838);
        _10825 = (int)*(((s1_ptr)_2)->base + _i_19326);
        RefDS(_common_19323);
        RefDS(_10825);
        _0 = _common_19323;
        _common_19323 = _1get_common(_common_19323, _10825);
        DeRefDS(_0);
        _10825 = NOVALUE;

        /** 	end for*/
        _i_19326 = _i_19326 + 1;
        goto L2; // [58] 41
L3: 
        ;
    }

    /** 	integer size = length( common )*/
    if (IS_SEQUENCE(_common_19323)){
            _size_19330 = SEQ_PTR(_common_19323)->length;
    }
    else {
        _size_19330 = 1;
    }

    /** 	if length(common) and common[$] != SLASH then*/
    if (IS_SEQUENCE(_common_19323)){
            _10828 = SEQ_PTR(_common_19323)->length;
    }
    else {
        _10828 = 1;
    }
    if (_10828 == 0) {
        goto L4; // [75] 133
    }
    if (IS_SEQUENCE(_common_19323)){
            _10830 = SEQ_PTR(_common_19323)->length;
    }
    else {
        _10830 = 1;
    }
    _2 = (int)SEQ_PTR(_common_19323);
    _10831 = (int)*(((s1_ptr)_2)->base + _10830);
    if (IS_ATOM_INT(_10831)) {
        _10832 = (_10831 != 92);
    }
    else {
        _10832 = binary_op(NOTEQ, _10831, 92);
    }
    _10831 = NOVALUE;
    if (_10832 == 0) {
        DeRef(_10832);
        _10832 = NOVALUE;
        goto L4; // [93] 133
    }
    else {
        if (!IS_ATOM_INT(_10832) && DBL_PTR(_10832)->dbl == 0.0){
            DeRef(_10832);
            _10832 = NOVALUE;
            goto L4; // [93] 133
        }
        DeRef(_10832);
        _10832 = NOVALUE;
    }
    DeRef(_10832);
    _10832 = NOVALUE;

    /** 		while size and common[size] != SLASH do*/
L5: 
    if (_size_19330 == 0) {
        goto L6; // [101] 132
    }
    _2 = (int)SEQ_PTR(_common_19323);
    _10834 = (int)*(((s1_ptr)_2)->base + _size_19330);
    if (IS_ATOM_INT(_10834)) {
        _10835 = (_10834 != 92);
    }
    else {
        _10835 = binary_op(NOTEQ, _10834, 92);
    }
    _10834 = NOVALUE;
    if (_10835 <= 0) {
        if (_10835 == 0) {
            DeRef(_10835);
            _10835 = NOVALUE;
            goto L6; // [116] 132
        }
        else {
            if (!IS_ATOM_INT(_10835) && DBL_PTR(_10835)->dbl == 0.0){
                DeRef(_10835);
                _10835 = NOVALUE;
                goto L6; // [116] 132
            }
            DeRef(_10835);
            _10835 = NOVALUE;
        }
    }
    DeRef(_10835);
    _10835 = NOVALUE;

    /** 			size -= 1*/
    _size_19330 = _size_19330 - 1;

    /** 		end while*/
    goto L5; // [129] 101
L6: 
L4: 

    /** 	for i = 1 to length( files ) do*/
    if (IS_SEQUENCE(_1files_18838)){
            _10837 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _10837 = 1;
    }
    {
        int _i_19346;
        _i_19346 = 1;
L7: 
        if (_i_19346 > _10837){
            goto L8; // [140] 173
        }

        /** 		short_names = append( short_names, remove( files[i], 1, size ) )*/
        _2 = (int)SEQ_PTR(_1files_18838);
        _10838 = (int)*(((s1_ptr)_2)->base + _i_19346);
        {
            s1_ptr assign_space = SEQ_PTR(_10838);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(1)) ? 1 : (long)(DBL_PTR(1)->dbl);
            int stop = (IS_ATOM_INT(_size_19330)) ? _size_19330 : (long)(DBL_PTR(_size_19330)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_10838);
                DeRef(_10839);
                _10839 = _10838;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_10838), start, &_10839 );
                }
                else Tail(SEQ_PTR(_10838), stop+1, &_10839);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_10838), start, &_10839);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_10839);
                _10839 = _1;
            }
        }
        _10838 = NOVALUE;
        RefDS(_10839);
        Append(&_1short_names_18840, _1short_names_18840, _10839);
        DeRefDS(_10839);
        _10839 = NOVALUE;

        /** 	end for*/
        _i_19346 = _i_19346 + 1;
        goto L7; // [168] 147
L8: 
        ;
    }

    /** end procedure*/
    DeRef(_common_19323);
    return;
    ;
}


void _1write_html()
{
    int _files_dir_19361 = NOVALUE;
    int _10849 = NOVALUE;
    int _10843 = NOVALUE;
    int _10842 = NOVALUE;
    int _10841 = NOVALUE;
    int _0, _1, _2;
    

    /** 	shorten_names()*/
    _1shorten_names();

    /** 	if equal( output_directory, "" ) then*/
    if (_1output_directory_18833 == _5)
    _10841 = 1;
    else if (IS_ATOM_INT(_1output_directory_18833) && IS_ATOM_INT(_5))
    _10841 = 0;
    else
    _10841 = (compare(_1output_directory_18833, _5) == 0);
    if (_10841 == 0)
    {
        _10841 = NOVALUE;
        goto L1; // [13] 40
    }
    else{
        _10841 = NOVALUE;
    }

    /** 		output_directory = dirname( coverage_db_name ) & SLASH & filebase( coverage_db_name )*/
    RefDS(_1coverage_db_name_18834);
    _10842 = _8dirname(_1coverage_db_name_18834, 0);
    RefDS(_1coverage_db_name_18834);
    _10843 = _8filebase(_1coverage_db_name_18834);
    {
        int concat_list[3];

        concat_list[0] = _10843;
        concat_list[1] = 92;
        concat_list[2] = _10842;
        Concat_N((object_ptr)&_1output_directory_18833, concat_list, 3);
    }
    DeRef(_10843);
    _10843 = NOVALUE;
    DeRef(_10842);
    _10842 = NOVALUE;
L1: 

    /** 	output_dir( output_directory )*/
    RefDS(_1output_directory_18833);
    _1output_dir(_1output_directory_18833);

    /** 	sequence files_dir = output_directory & SLASH & "files"*/
    {
        int concat_list[3];

        concat_list[0] = _10845;
        concat_list[1] = 92;
        concat_list[2] = _1output_directory_18833;
        Concat_N((object_ptr)&_files_dir_19361, concat_list, 3);
    }

    /** 	output_dir( files_dir )*/
    RefDS(_files_dir_19361);
    _1output_dir(_files_dir_19361);

    /** 	output_directory &= SLASH*/
    Append(&_1output_directory_18833, _1output_directory_18833, 92);

    /** 	files_dir &= SLASH*/
    Append(&_files_dir_19361, _files_dir_19361, 92);

    /** 	for i = 1 to length( files ) do*/
    if (IS_SEQUENCE(_1files_18838)){
            _10849 = SEQ_PTR(_1files_18838)->length;
    }
    else {
        _10849 = 1;
    }
    {
        int _i_19370;
        _i_19370 = 1;
L2: 
        if (_i_19370 > _10849){
            goto L3; // [89] 109
        }

        /** 		write_file_html( files_dir, i )*/
        RefDS(_files_dir_19361);
        _1write_file_html(_files_dir_19361, _i_19370);

        /** 	end for*/
        _i_19370 = _i_19370 + 1;
        goto L2; // [104] 96
L3: 
        ;
    }

    /** 	write_summary( output_directory )*/
    RefDS(_1output_directory_18833);
    _1write_summary(_1output_directory_18833);

    /** end procedure*/
    DeRef(_files_dir_19361);
    return;
    ;
}


void _1main()
{
    int _0, _1, _2;
    

    /** 	process_cmd_line()*/
    _1process_cmd_line();

    /** 	read_db()*/
    _1read_db();

    /** 	analyze_coverage()*/
    _1analyze_coverage();

    /** 	write_html()*/
    _1write_html();

    /** end procedure*/
    return;
    ;
}



// 0x8ECC7112
