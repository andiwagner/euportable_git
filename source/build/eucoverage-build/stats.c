// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _30massage(int _data_set_13497, int _subseq_opt_13498)
{
    int _7387 = NOVALUE;
    int _7386 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch subseq_opt do*/
    _0 = _subseq_opt_13498;
    switch ( _0 ){ 

        /** 		case ST_IGNSTR then*/
        case 2:

        /** 			return stdseq:remove_subseq(data_set, stdseq:SEQ_NOALT)*/
        RefDS(_data_set_13497);
        RefDS(_20SEQ_NOALT_6271);
        _7386 = _20remove_subseq(_data_set_13497, _20SEQ_NOALT_6271);
        DeRefDS(_data_set_13497);
        return _7386;
        goto L1; // [27] 57

        /** 		case ST_ZEROSTR then*/
        case 3:

        /** 			return stdseq:remove_subseq(data_set, 0)*/
        RefDS(_data_set_13497);
        _7387 = _20remove_subseq(_data_set_13497, 0);
        DeRefDS(_data_set_13497);
        DeRef(_7386);
        _7386 = NOVALUE;
        return _7387;
        goto L1; // [44] 57

        /** 		case else*/
        default:

        /** 			return data_set*/
        DeRef(_7386);
        _7386 = NOVALUE;
        DeRef(_7387);
        _7387 = NOVALUE;
        return _data_set_13497;
    ;}L1: 
    ;
}


int _30stdev(int _data_set_13508, int _subseq_opt_13509, int _population_type_13510)
{
    int _lSum_13511 = NOVALUE;
    int _lMean_13512 = NOVALUE;
    int _lCnt_13513 = NOVALUE;
    int _7404 = NOVALUE;
    int _7403 = NOVALUE;
    int _7399 = NOVALUE;
    int _7398 = NOVALUE;
    int _7397 = NOVALUE;
    int _7396 = NOVALUE;
    int _7393 = NOVALUE;
    int _7392 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data_set = massage(data_set, subseq_opt)*/
    RefDS(_data_set_13508);
    _0 = _data_set_13508;
    _data_set_13508 = _30massage(_data_set_13508, 1);
    DeRefDSi(_0);

    /** 	lCnt = length(data_set)*/
    if (IS_SEQUENCE(_data_set_13508)){
            _lCnt_13513 = SEQ_PTR(_data_set_13508)->length;
    }
    else {
        _lCnt_13513 = 1;
    }

    /** 	if lCnt = 0 then*/
    if (_lCnt_13513 != 0)
    goto L1; // [25] 36

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_data_set_13508);
    DeRef(_lSum_13511);
    DeRef(_lMean_13512);
    return _5;
L1: 

    /** 	if lCnt = 1 then*/
    if (_lCnt_13513 != 1)
    goto L2; // [38] 49

    /** 		return 0*/
    DeRefDS(_data_set_13508);
    DeRef(_lSum_13511);
    DeRef(_lMean_13512);
    return 0;
L2: 

    /** 	lSum = 0*/
    DeRef(_lSum_13511);
    _lSum_13511 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_13508)){
            _7392 = SEQ_PTR(_data_set_13508)->length;
    }
    else {
        _7392 = 1;
    }
    {
        int _i_13521;
        _i_13521 = 1;
L3: 
        if (_i_13521 > _7392){
            goto L4; // [59] 83
        }

        /** 		lSum += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_13508);
        _7393 = (int)*(((s1_ptr)_2)->base + _i_13521);
        _0 = _lSum_13511;
        if (IS_ATOM_INT(_lSum_13511) && IS_ATOM_INT(_7393)) {
            _lSum_13511 = _lSum_13511 + _7393;
            if ((long)((unsigned long)_lSum_13511 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_13511 = NewDouble((double)_lSum_13511);
        }
        else {
            _lSum_13511 = binary_op(PLUS, _lSum_13511, _7393);
        }
        DeRef(_0);
        _7393 = NOVALUE;

        /** 	end for*/
        _i_13521 = _i_13521 + 1;
        goto L3; // [78] 66
L4: 
        ;
    }

    /** 	lMean = lSum / lCnt*/
    DeRef(_lMean_13512);
    if (IS_ATOM_INT(_lSum_13511)) {
        _lMean_13512 = (_lSum_13511 % _lCnt_13513) ? NewDouble((double)_lSum_13511 / _lCnt_13513) : (_lSum_13511 / _lCnt_13513);
    }
    else {
        _lMean_13512 = NewDouble(DBL_PTR(_lSum_13511)->dbl / (double)_lCnt_13513);
    }

    /** 	lSum = 0*/
    DeRef(_lSum_13511);
    _lSum_13511 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_13508)){
            _7396 = SEQ_PTR(_data_set_13508)->length;
    }
    else {
        _7396 = 1;
    }
    {
        int _i_13527;
        _i_13527 = 1;
L5: 
        if (_i_13527 > _7396){
            goto L6; // [99] 131
        }

        /** 		lSum += power(data_set[i] - lMean, 2)*/
        _2 = (int)SEQ_PTR(_data_set_13508);
        _7397 = (int)*(((s1_ptr)_2)->base + _i_13527);
        if (IS_ATOM_INT(_7397) && IS_ATOM_INT(_lMean_13512)) {
            _7398 = _7397 - _lMean_13512;
            if ((long)((unsigned long)_7398 +(unsigned long) HIGH_BITS) >= 0){
                _7398 = NewDouble((double)_7398);
            }
        }
        else {
            _7398 = binary_op(MINUS, _7397, _lMean_13512);
        }
        _7397 = NOVALUE;
        if (IS_ATOM_INT(_7398) && IS_ATOM_INT(_7398)) {
            if (_7398 == (short)_7398 && _7398 <= INT15 && _7398 >= -INT15)
            _7399 = _7398 * _7398;
            else
            _7399 = NewDouble(_7398 * (double)_7398);
        }
        else {
            _7399 = binary_op(MULTIPLY, _7398, _7398);
        }
        DeRef(_7398);
        _7398 = NOVALUE;
        _7398 = NOVALUE;
        _0 = _lSum_13511;
        if (IS_ATOM_INT(_lSum_13511) && IS_ATOM_INT(_7399)) {
            _lSum_13511 = _lSum_13511 + _7399;
            if ((long)((unsigned long)_lSum_13511 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_13511 = NewDouble((double)_lSum_13511);
        }
        else {
            _lSum_13511 = binary_op(PLUS, _lSum_13511, _7399);
        }
        DeRef(_0);
        DeRef(_7399);
        _7399 = NOVALUE;

        /** 	end for*/
        _i_13527 = _i_13527 + 1;
        goto L5; // [126] 106
L6: 
        ;
    }

    /** 	if population_type = ST_SAMPLE then*/
    if (_population_type_13510 != 2)
    goto L7; // [135] 148

    /** 		lCnt -= 1*/
    _lCnt_13513 = _lCnt_13513 - 1;
L7: 

    /** 	return power(lSum / lCnt, 0.5)*/
    if (IS_ATOM_INT(_lSum_13511)) {
        _7403 = (_lSum_13511 % _lCnt_13513) ? NewDouble((double)_lSum_13511 / _lCnt_13513) : (_lSum_13511 / _lCnt_13513);
    }
    else {
        _7403 = NewDouble(DBL_PTR(_lSum_13511)->dbl / (double)_lCnt_13513);
    }
    if (IS_ATOM_INT(_7403)) {
        temp_d.dbl = (double)_7403;
        _7404 = Dpower(&temp_d, DBL_PTR(_2036));
    }
    else {
        _7404 = Dpower(DBL_PTR(_7403), DBL_PTR(_2036));
    }
    DeRef(_7403);
    _7403 = NOVALUE;
    DeRefDS(_data_set_13508);
    DeRef(_lSum_13511);
    DeRef(_lMean_13512);
    return _7404;
    ;
}


int _30sum(int _data_set_13577, int _subseq_opt_13578)
{
    int _result__13579 = NOVALUE;
    int _7428 = NOVALUE;
    int _7427 = NOVALUE;
    int _7425 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _7425 = IS_ATOM(_data_set_13577);
    if (_7425 == 0)
    {
        _7425 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _7425 = NOVALUE;
    }

    /** 		return data_set*/
    DeRef(_result__13579);
    return _data_set_13577;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_13577);
    _0 = _data_set_13577;
    _data_set_13577 = _30massage(_data_set_13577, _subseq_opt_13578);
    DeRef(_0);

    /** 	result_ = 0*/
    DeRef(_result__13579);
    _result__13579 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_13577)){
            _7427 = SEQ_PTR(_data_set_13577)->length;
    }
    else {
        _7427 = 1;
    }
    {
        int _i_13584;
        _i_13584 = 1;
L2: 
        if (_i_13584 > _7427){
            goto L3; // [33] 57
        }

        /** 		result_ += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_13577);
        _7428 = (int)*(((s1_ptr)_2)->base + _i_13584);
        _0 = _result__13579;
        if (IS_ATOM_INT(_result__13579) && IS_ATOM_INT(_7428)) {
            _result__13579 = _result__13579 + _7428;
            if ((long)((unsigned long)_result__13579 + (unsigned long)HIGH_BITS) >= 0) 
            _result__13579 = NewDouble((double)_result__13579);
        }
        else {
            _result__13579 = binary_op(PLUS, _result__13579, _7428);
        }
        DeRef(_0);
        _7428 = NOVALUE;

        /** 	end for*/
        _i_13584 = _i_13584 + 1;
        goto L2; // [52] 40
L3: 
        ;
    }

    /** 	return result_*/
    DeRef(_data_set_13577);
    return _result__13579;
    ;
}


int _30average(int _data_set_13598, int _subseq_opt_13599)
{
    int _7439 = NOVALUE;
    int _7438 = NOVALUE;
    int _7437 = NOVALUE;
    int _7435 = NOVALUE;
    int _7433 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _7433 = 0;
    if (_7433 == 0)
    {
        _7433 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _7433 = NOVALUE;
    }

    /** 		return data_set*/
    return _data_set_13598;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_13598);
    _0 = _data_set_13598;
    _data_set_13598 = _30massage(_data_set_13598, _subseq_opt_13599);
    DeRef(_0);

    /** 	if length(data_set) = 0 then*/
    if (IS_SEQUENCE(_data_set_13598)){
            _7435 = SEQ_PTR(_data_set_13598)->length;
    }
    else {
        _7435 = 1;
    }
    if (_7435 != 0)
    goto L2; // [28] 39

    /** 		return {}*/
    RefDS(_5);
    DeRef(_data_set_13598);
    return _5;
L2: 

    /** 	return sum(data_set) / length(data_set)*/
    Ref(_data_set_13598);
    _7437 = _30sum(_data_set_13598, 1);
    if (IS_SEQUENCE(_data_set_13598)){
            _7438 = SEQ_PTR(_data_set_13598)->length;
    }
    else {
        _7438 = 1;
    }
    if (IS_ATOM_INT(_7437)) {
        _7439 = (_7437 % _7438) ? NewDouble((double)_7437 / _7438) : (_7437 / _7438);
    }
    else {
        _7439 = binary_op(DIVIDE, _7437, _7438);
    }
    DeRef(_7437);
    _7437 = NOVALUE;
    _7438 = NOVALUE;
    DeRef(_data_set_13598);
    return _7439;
    ;
}



// 0x47E6D32B
