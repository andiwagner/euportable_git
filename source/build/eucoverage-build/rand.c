// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _18rand_range(int _lo_3711, int _hi_3712)
{
    int _temp_3715 = NOVALUE;
    int _1894 = NOVALUE;
    int _1892 = NOVALUE;
    int _1889 = NOVALUE;
    int _1888 = NOVALUE;
    int _1887 = NOVALUE;
    int _1886 = NOVALUE;
    int _1884 = NOVALUE;
    int _1883 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if lo > hi then*/
    if (binary_op_a(LESSEQ, _lo_3711, _hi_3712)){
        goto L1; // [3] 23
    }

    /** 		atom temp = hi*/
    Ref(_hi_3712);
    DeRef(_temp_3715);
    _temp_3715 = _hi_3712;

    /** 		hi = lo*/
    Ref(_lo_3711);
    DeRef(_hi_3712);
    _hi_3712 = _lo_3711;

    /** 		lo = temp*/
    Ref(_temp_3715);
    DeRef(_lo_3711);
    _lo_3711 = _temp_3715;
L1: 
    DeRef(_temp_3715);
    _temp_3715 = NOVALUE;

    /** 	if not integer(lo) or not integer(hi) then*/
    if (IS_ATOM_INT(_lo_3711))
    _1883 = 1;
    else if (IS_ATOM_DBL(_lo_3711))
    _1883 = IS_ATOM_INT(DoubleToInt(_lo_3711));
    else
    _1883 = 0;
    _1884 = (_1883 == 0);
    _1883 = NOVALUE;
    if (_1884 != 0) {
        goto L2; // [33] 48
    }
    if (IS_ATOM_INT(_hi_3712))
    _1886 = 1;
    else if (IS_ATOM_DBL(_hi_3712))
    _1886 = IS_ATOM_INT(DoubleToInt(_hi_3712));
    else
    _1886 = 0;
    _1887 = (_1886 == 0);
    _1886 = NOVALUE;
    if (_1887 == 0)
    {
        DeRef(_1887);
        _1887 = NOVALUE;
        goto L3; // [44] 64
    }
    else{
        DeRef(_1887);
        _1887 = NOVALUE;
    }
L2: 

    /**    		hi = rnd() * (hi - lo)*/
    _1888 = _18rnd();
    if (IS_ATOM_INT(_hi_3712) && IS_ATOM_INT(_lo_3711)) {
        _1889 = _hi_3712 - _lo_3711;
        if ((long)((unsigned long)_1889 +(unsigned long) HIGH_BITS) >= 0){
            _1889 = NewDouble((double)_1889);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_3712)) {
            _1889 = NewDouble((double)_hi_3712 - DBL_PTR(_lo_3711)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_3711)) {
                _1889 = NewDouble(DBL_PTR(_hi_3712)->dbl - (double)_lo_3711);
            }
            else
            _1889 = NewDouble(DBL_PTR(_hi_3712)->dbl - DBL_PTR(_lo_3711)->dbl);
        }
    }
    DeRef(_hi_3712);
    if (IS_ATOM_INT(_1888) && IS_ATOM_INT(_1889)) {
        if (_1888 == (short)_1888 && _1889 <= INT15 && _1889 >= -INT15)
        _hi_3712 = _1888 * _1889;
        else
        _hi_3712 = NewDouble(_1888 * (double)_1889);
    }
    else {
        _hi_3712 = binary_op(MULTIPLY, _1888, _1889);
    }
    DeRef(_1888);
    _1888 = NOVALUE;
    DeRef(_1889);
    _1889 = NOVALUE;
    goto L4; // [61] 80
L3: 

    /** 		lo -= 1*/
    _0 = _lo_3711;
    if (IS_ATOM_INT(_lo_3711)) {
        _lo_3711 = _lo_3711 - 1;
        if ((long)((unsigned long)_lo_3711 +(unsigned long) HIGH_BITS) >= 0){
            _lo_3711 = NewDouble((double)_lo_3711);
        }
    }
    else {
        _lo_3711 = NewDouble(DBL_PTR(_lo_3711)->dbl - (double)1);
    }
    DeRef(_0);

    /**    		hi = rand(hi - lo)*/
    if (IS_ATOM_INT(_hi_3712) && IS_ATOM_INT(_lo_3711)) {
        _1892 = _hi_3712 - _lo_3711;
        if ((long)((unsigned long)_1892 +(unsigned long) HIGH_BITS) >= 0){
            _1892 = NewDouble((double)_1892);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_3712)) {
            _1892 = NewDouble((double)_hi_3712 - DBL_PTR(_lo_3711)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_3711)) {
                _1892 = NewDouble(DBL_PTR(_hi_3712)->dbl - (double)_lo_3711);
            }
            else
            _1892 = NewDouble(DBL_PTR(_hi_3712)->dbl - DBL_PTR(_lo_3711)->dbl);
        }
    }
    DeRef(_hi_3712);
    if (IS_ATOM_INT(_1892)) {
        _hi_3712 = good_rand() % ((unsigned)_1892) + 1;
    }
    else {
        _hi_3712 = unary_op(RAND, _1892);
    }
    DeRef(_1892);
    _1892 = NOVALUE;
L4: 

    /**    	return lo + hi*/
    if (IS_ATOM_INT(_lo_3711) && IS_ATOM_INT(_hi_3712)) {
        _1894 = _lo_3711 + _hi_3712;
        if ((long)((unsigned long)_1894 + (unsigned long)HIGH_BITS) >= 0) 
        _1894 = NewDouble((double)_1894);
    }
    else {
        if (IS_ATOM_INT(_lo_3711)) {
            _1894 = NewDouble((double)_lo_3711 + DBL_PTR(_hi_3712)->dbl);
        }
        else {
            if (IS_ATOM_INT(_hi_3712)) {
                _1894 = NewDouble(DBL_PTR(_lo_3711)->dbl + (double)_hi_3712);
            }
            else
            _1894 = NewDouble(DBL_PTR(_lo_3711)->dbl + DBL_PTR(_hi_3712)->dbl);
        }
    }
    DeRef(_lo_3711);
    DeRef(_hi_3712);
    DeRef(_1884);
    _1884 = NOVALUE;
    return _1894;
    ;
}


int _18rnd()
{
    int _a_3735 = NOVALUE;
    int _b_3736 = NOVALUE;
    int _r_3737 = NOVALUE;
    int _0, _1, _2;
    

    /** 	 a = rand(#FFFFFFFF)*/
    DeRef(_a_3735);
    _a_3735 = unary_op(RAND, _1895);

    /** 	 if a = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _a_3735, 1)){
        goto L1; // [8] 17
    }
    DeRef(_a_3735);
    DeRef(_b_3736);
    DeRef(_r_3737);
    return 0;
L1: 

    /** 	 b = rand(#FFFFFFFF)*/
    DeRef(_b_3736);
    _b_3736 = unary_op(RAND, _1895);

    /** 	 if b = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _b_3736, 1)){
        goto L2; // [24] 33
    }
    DeRef(_a_3735);
    DeRef(_b_3736);
    DeRef(_r_3737);
    return 0;
L2: 

    /** 	 if a > b then*/
    if (binary_op_a(LESSEQ, _a_3735, _b_3736)){
        goto L3; // [35] 48
    }

    /** 	 	r = b / a*/
    DeRef(_r_3737);
    if (IS_ATOM_INT(_b_3736) && IS_ATOM_INT(_a_3735)) {
        _r_3737 = (_b_3736 % _a_3735) ? NewDouble((double)_b_3736 / _a_3735) : (_b_3736 / _a_3735);
    }
    else {
        if (IS_ATOM_INT(_b_3736)) {
            _r_3737 = NewDouble((double)_b_3736 / DBL_PTR(_a_3735)->dbl);
        }
        else {
            if (IS_ATOM_INT(_a_3735)) {
                _r_3737 = NewDouble(DBL_PTR(_b_3736)->dbl / (double)_a_3735);
            }
            else
            _r_3737 = NewDouble(DBL_PTR(_b_3736)->dbl / DBL_PTR(_a_3735)->dbl);
        }
    }
    goto L4; // [45] 55
L3: 

    /** 	 	r = a / b*/
    DeRef(_r_3737);
    if (IS_ATOM_INT(_a_3735) && IS_ATOM_INT(_b_3736)) {
        _r_3737 = (_a_3735 % _b_3736) ? NewDouble((double)_a_3735 / _b_3736) : (_a_3735 / _b_3736);
    }
    else {
        if (IS_ATOM_INT(_a_3735)) {
            _r_3737 = NewDouble((double)_a_3735 / DBL_PTR(_b_3736)->dbl);
        }
        else {
            if (IS_ATOM_INT(_b_3736)) {
                _r_3737 = NewDouble(DBL_PTR(_a_3735)->dbl / (double)_b_3736);
            }
            else
            _r_3737 = NewDouble(DBL_PTR(_a_3735)->dbl / DBL_PTR(_b_3736)->dbl);
        }
    }
L4: 

    /** 	 return r*/
    DeRef(_a_3735);
    DeRef(_b_3736);
    return _r_3737;
    ;
}


int _18rnd_1()
{
    int _r_3752 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while r >= 1.0 with entry do*/
    goto L1; // [3] 15
L2: 
    if (binary_op_a(LESS, _r_3752, _1903)){
        goto L3; // [8] 25
    }

    /** 	entry*/
L1: 

    /** 		r = rnd()*/
    _0 = _r_3752;
    _r_3752 = _18rnd();
    DeRef(_0);

    /** 	end while	 */
    goto L2; // [22] 6
L3: 

    /** 	return r*/
    return _r_3752;
    ;
}


void _18set_rand(int _seed_3759)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_SET_RAND, seed)*/
    machine(35, _seed_3759);

    /** end procedure*/
    DeRef(_seed_3759);
    return;
    ;
}


int _18get_rand()
{
    int _1906 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_GET_RAND, {})*/
    _1906 = machine(98, _5);
    return _1906;
    ;
}


int _18chance(int _my_limit_3765, int _top_limit_3766)
{
    int _1909 = NOVALUE;
    int _1908 = NOVALUE;
    int _1907 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return (rnd_1() * top_limit) <= my_limit*/
    _1907 = _18rnd_1();
    if (IS_ATOM_INT(_1907) && IS_ATOM_INT(_top_limit_3766)) {
        if (_1907 == (short)_1907 && _top_limit_3766 <= INT15 && _top_limit_3766 >= -INT15)
        _1908 = _1907 * _top_limit_3766;
        else
        _1908 = NewDouble(_1907 * (double)_top_limit_3766);
    }
    else {
        _1908 = binary_op(MULTIPLY, _1907, _top_limit_3766);
    }
    DeRef(_1907);
    _1907 = NOVALUE;
    if (IS_ATOM_INT(_1908) && IS_ATOM_INT(_my_limit_3765)) {
        _1909 = (_1908 <= _my_limit_3765);
    }
    else {
        _1909 = binary_op(LESSEQ, _1908, _my_limit_3765);
    }
    DeRef(_1908);
    _1908 = NOVALUE;
    DeRef(_my_limit_3765);
    DeRef(_top_limit_3766);
    return _1909;
    ;
}


int _18roll(int _desired_3772, int _sides_3773)
{
    int _rolled_3774 = NOVALUE;
    int _1914 = NOVALUE;
    int _1911 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sides_3773)) {
        _1 = (long)(DBL_PTR(_sides_3773)->dbl);
        if (UNIQUE(DBL_PTR(_sides_3773)) && (DBL_PTR(_sides_3773)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sides_3773);
        _sides_3773 = _1;
    }

    /** 	if sides < 2 then*/
    if (_sides_3773 >= 2)
    goto L1; // [7] 18

    /** 		return 0*/
    DeRef(_desired_3772);
    return 0;
L1: 

    /** 	if atom(desired) then*/
    _1911 = IS_ATOM(_desired_3772);
    if (_1911 == 0)
    {
        _1911 = NOVALUE;
        goto L2; // [23] 33
    }
    else{
        _1911 = NOVALUE;
    }

    /** 		desired = {desired}*/
    _0 = _desired_3772;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_desired_3772);
    *((int *)(_2+4)) = _desired_3772;
    _desired_3772 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	rolled =  rand(sides)*/
    _rolled_3774 = good_rand() % ((unsigned)_sides_3773) + 1;

    /** 	if find(rolled, desired) then*/
    _1914 = find_from(_rolled_3774, _desired_3772, 1);
    if (_1914 == 0)
    {
        _1914 = NOVALUE;
        goto L3; // [47] 59
    }
    else{
        _1914 = NOVALUE;
    }

    /** 		return rolled*/
    DeRef(_desired_3772);
    return _rolled_3774;
    goto L4; // [56] 66
L3: 

    /** 		return 0*/
    DeRef(_desired_3772);
    return 0;
L4: 
    ;
}


int _18sample(int _population_3786, int _sample_size_3787, int _sampling_method_3788)
{
    int _lResult_3789 = NOVALUE;
    int _lIdx_3790 = NOVALUE;
    int _lChoice_3791 = NOVALUE;
    int _1932 = NOVALUE;
    int _1928 = NOVALUE;
    int _1925 = NOVALUE;
    int _1921 = NOVALUE;
    int _1920 = NOVALUE;
    int _1919 = NOVALUE;
    int _1918 = NOVALUE;
    int _1917 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sample_size_3787)) {
        _1 = (long)(DBL_PTR(_sample_size_3787)->dbl);
        if (UNIQUE(DBL_PTR(_sample_size_3787)) && (DBL_PTR(_sample_size_3787)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sample_size_3787);
        _sample_size_3787 = _1;
    }
    if (!IS_ATOM_INT(_sampling_method_3788)) {
        _1 = (long)(DBL_PTR(_sampling_method_3788)->dbl);
        if (UNIQUE(DBL_PTR(_sampling_method_3788)) && (DBL_PTR(_sampling_method_3788)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sampling_method_3788);
        _sampling_method_3788 = _1;
    }

    /** 	if sample_size < 1 then*/
    if (_sample_size_3787 >= 1)
    goto L1; // [13] 44

    /** 		if sampling_method > 0 then*/
    if (_sampling_method_3788 <= 0)
    goto L2; // [19] 36

    /** 			return {{}, population}	*/
    RefDS(_population_3786);
    RefDS(_5);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5;
    ((int *)_2)[2] = _population_3786;
    _1917 = MAKE_SEQ(_1);
    DeRefDS(_population_3786);
    DeRef(_lResult_3789);
    return _1917;
    goto L3; // [33] 43
L2: 

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_population_3786);
    DeRef(_lResult_3789);
    DeRef(_1917);
    _1917 = NOVALUE;
    return _5;
L3: 
L1: 

    /** 	if sampling_method >= 0 and sample_size >= length(population) then*/
    _1918 = (_sampling_method_3788 >= 0);
    if (_1918 == 0) {
        goto L4; // [50] 73
    }
    if (IS_SEQUENCE(_population_3786)){
            _1920 = SEQ_PTR(_population_3786)->length;
    }
    else {
        _1920 = 1;
    }
    _1921 = (_sample_size_3787 >= _1920);
    _1920 = NOVALUE;
    if (_1921 == 0)
    {
        DeRef(_1921);
        _1921 = NOVALUE;
        goto L4; // [62] 73
    }
    else{
        DeRef(_1921);
        _1921 = NOVALUE;
    }

    /** 		sample_size = length(population)*/
    if (IS_SEQUENCE(_population_3786)){
            _sample_size_3787 = SEQ_PTR(_population_3786)->length;
    }
    else {
        _sample_size_3787 = 1;
    }
L4: 

    /** 	lResult = repeat(0, sample_size)*/
    DeRef(_lResult_3789);
    _lResult_3789 = Repeat(0, _sample_size_3787);

    /** 	lIdx = 0*/
    _lIdx_3790 = 0;

    /** 	while lIdx < sample_size do*/
L5: 
    if (_lIdx_3790 >= _sample_size_3787)
    goto L6; // [91] 142

    /** 		lChoice = rand(length(population))*/
    if (IS_SEQUENCE(_population_3786)){
            _1925 = SEQ_PTR(_population_3786)->length;
    }
    else {
        _1925 = 1;
    }
    _lChoice_3791 = good_rand() % ((unsigned)_1925) + 1;
    _1925 = NOVALUE;

    /** 		lIdx += 1*/
    _lIdx_3790 = _lIdx_3790 + 1;

    /** 		lResult[lIdx] = population[lChoice]*/
    _2 = (int)SEQ_PTR(_population_3786);
    _1928 = (int)*(((s1_ptr)_2)->base + _lChoice_3791);
    Ref(_1928);
    _2 = (int)SEQ_PTR(_lResult_3789);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lResult_3789 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _lIdx_3790);
    _1 = *(int *)_2;
    *(int *)_2 = _1928;
    if( _1 != _1928 ){
        DeRef(_1);
    }
    _1928 = NOVALUE;

    /** 		if sampling_method >= 0 then*/
    if (_sampling_method_3788 < 0)
    goto L5; // [125] 91

    /** 			population = remove(population, lChoice)*/
    {
        s1_ptr assign_space = SEQ_PTR(_population_3786);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lChoice_3791)) ? _lChoice_3791 : (long)(DBL_PTR(_lChoice_3791)->dbl);
        int stop = (IS_ATOM_INT(_lChoice_3791)) ? _lChoice_3791 : (long)(DBL_PTR(_lChoice_3791)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_population_3786), start, &_population_3786 );
            }
            else Tail(SEQ_PTR(_population_3786), stop+1, &_population_3786);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_population_3786), start, &_population_3786);
        }
        else {
            assign_slice_seq = &assign_space;
            _population_3786 = Remove_elements(start, stop, (SEQ_PTR(_population_3786)->ref == 1));
        }
    }

    /** 	end while*/
    goto L5; // [139] 91
L6: 

    /** 	if sampling_method > 0 then*/
    if (_sampling_method_3788 <= 0)
    goto L7; // [144] 161

    /** 		return {lResult, population}	*/
    RefDS(_population_3786);
    RefDS(_lResult_3789);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_3789;
    ((int *)_2)[2] = _population_3786;
    _1932 = MAKE_SEQ(_1);
    DeRefDS(_population_3786);
    DeRefDS(_lResult_3789);
    DeRef(_1917);
    _1917 = NOVALUE;
    DeRef(_1918);
    _1918 = NOVALUE;
    return _1932;
    goto L8; // [158] 168
L7: 

    /** 		return lResult*/
    DeRefDS(_population_3786);
    DeRef(_1917);
    _1917 = NOVALUE;
    DeRef(_1918);
    _1918 = NOVALUE;
    DeRef(_1932);
    _1932 = NOVALUE;
    return _lResult_3789;
L8: 
    ;
}



// 0x2E7B74BC
