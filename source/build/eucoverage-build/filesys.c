// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _8find_first_wildcard(int _name_6576, int _from_6577)
{
    int _asterisk_at_6578 = NOVALUE;
    int _question_at_6580 = NOVALUE;
    int _first_wildcard_at_6582 = NOVALUE;
    int _3510 = NOVALUE;
    int _3509 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer asterisk_at = eu:find('*', name, from)*/
    _asterisk_at_6578 = find_from(42, _name_6576, 1);

    /** 	integer question_at = eu:find('?', name, from)*/
    _question_at_6580 = find_from(63, _name_6576, 1);

    /** 	integer first_wildcard_at = asterisk_at*/
    _first_wildcard_at_6582 = _asterisk_at_6578;

    /** 	if asterisk_at or question_at then*/
    if (_asterisk_at_6578 != 0) {
        goto L1; // [34] 43
    }
    if (_question_at_6580 == 0)
    {
        goto L2; // [39] 66
    }
    else{
    }
L1: 

    /** 		if question_at and question_at < asterisk_at then*/
    if (_question_at_6580 == 0) {
        goto L3; // [45] 65
    }
    _3510 = (_question_at_6580 < _asterisk_at_6578);
    if (_3510 == 0)
    {
        DeRef(_3510);
        _3510 = NOVALUE;
        goto L3; // [54] 65
    }
    else{
        DeRef(_3510);
        _3510 = NOVALUE;
    }

    /** 			first_wildcard_at = question_at*/
    _first_wildcard_at_6582 = _question_at_6580;
L3: 
L2: 

    /** 	return first_wildcard_at*/
    DeRefDS(_name_6576);
    return _first_wildcard_at_6582;
    ;
}


int _8dir(int _name_6590)
{
    int _3511 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    _3511 = machine(22, _name_6590);
    DeRefDS(_name_6590);
    return _3511;
    ;
}


int _8current_dir()
{
    int _3513 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    _3513 = machine(23, 0);
    return _3513;
    ;
}


int _8chdir(int _newdir_6598)
{
    int _3514 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CHDIR, newdir)*/
    _3514 = machine(63, _newdir_6598);
    DeRefDS(_newdir_6598);
    return _3514;
    ;
}


int _8walk_dir(int _path_name_6614, int _your_function_6615, int _scan_subdirs_6616, int _dir_source_6617)
{
    int _d_6618 = NOVALUE;
    int _abort_now_6619 = NOVALUE;
    int _orig_func_6620 = NOVALUE;
    int _user_data_6621 = NOVALUE;
    int _source_orig_func_6623 = NOVALUE;
    int _source_user_data_6624 = NOVALUE;
    int _3564 = NOVALUE;
    int _3563 = NOVALUE;
    int _3562 = NOVALUE;
    int _3561 = NOVALUE;
    int _3560 = NOVALUE;
    int _3558 = NOVALUE;
    int _3557 = NOVALUE;
    int _3556 = NOVALUE;
    int _3555 = NOVALUE;
    int _3554 = NOVALUE;
    int _3553 = NOVALUE;
    int _3552 = NOVALUE;
    int _3551 = NOVALUE;
    int _3550 = NOVALUE;
    int _3548 = NOVALUE;
    int _3546 = NOVALUE;
    int _3545 = NOVALUE;
    int _3544 = NOVALUE;
    int _3542 = NOVALUE;
    int _3541 = NOVALUE;
    int _3540 = NOVALUE;
    int _3536 = NOVALUE;
    int _3534 = NOVALUE;
    int _3530 = NOVALUE;
    int _3528 = NOVALUE;
    int _3527 = NOVALUE;
    int _3525 = NOVALUE;
    int _3522 = NOVALUE;
    int _3519 = NOVALUE;
    int _3518 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scan_subdirs_6616)) {
        _1 = (long)(DBL_PTR(_scan_subdirs_6616)->dbl);
        if (UNIQUE(DBL_PTR(_scan_subdirs_6616)) && (DBL_PTR(_scan_subdirs_6616)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scan_subdirs_6616);
        _scan_subdirs_6616 = _1;
    }

    /** 	sequence user_data = {path_name, 0}*/
    RefDS(_path_name_6614);
    DeRef(_user_data_6621);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_6614;
    ((int *)_2)[2] = 0;
    _user_data_6621 = MAKE_SEQ(_1);

    /** 	object source_user_data = ""*/
    RefDS(_5);
    DeRef(_source_user_data_6624);
    _source_user_data_6624 = _5;

    /** 	orig_func = your_function*/
    Ref(_your_function_6615);
    DeRef(_orig_func_6620);
    _orig_func_6620 = _your_function_6615;

    /** 	if sequence(your_function) then*/
    _3518 = IS_SEQUENCE(_your_function_6615);
    if (_3518 == 0)
    {
        _3518 = NOVALUE;
        goto L1; // [28] 48
    }
    else{
        _3518 = NOVALUE;
    }

    /** 		user_data = append(user_data, your_function[2])*/
    _2 = (int)SEQ_PTR(_your_function_6615);
    _3519 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_3519);
    Append(&_user_data_6621, _user_data_6621, _3519);
    _3519 = NOVALUE;

    /** 		your_function = your_function[1]*/
    _0 = _your_function_6615;
    _2 = (int)SEQ_PTR(_your_function_6615);
    _your_function_6615 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_your_function_6615);
    DeRef(_0);
L1: 

    /** 	source_orig_func = dir_source*/
    Ref(_dir_source_6617);
    DeRef(_source_orig_func_6623);
    _source_orig_func_6623 = _dir_source_6617;

    /** 	if sequence(dir_source) then*/
    _3522 = IS_SEQUENCE(_dir_source_6617);
    if (_3522 == 0)
    {
        _3522 = NOVALUE;
        goto L2; // [58] 74
    }
    else{
        _3522 = NOVALUE;
    }

    /** 		source_user_data = dir_source[2]*/
    DeRef(_source_user_data_6624);
    _2 = (int)SEQ_PTR(_dir_source_6617);
    _source_user_data_6624 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_source_user_data_6624);

    /** 		dir_source = dir_source[1]*/
    _0 = _dir_source_6617;
    _2 = (int)SEQ_PTR(_dir_source_6617);
    _dir_source_6617 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_dir_source_6617);
    DeRef(_0);
L2: 

    /** 	if not equal(dir_source, types:NO_ROUTINE_ID) then*/
    if (_dir_source_6617 == -99999)
    _3525 = 1;
    else if (IS_ATOM_INT(_dir_source_6617) && IS_ATOM_INT(-99999))
    _3525 = 0;
    else
    _3525 = (compare(_dir_source_6617, -99999) == 0);
    if (_3525 != 0)
    goto L3; // [80] 118
    _3525 = NOVALUE;

    /** 		if atom(source_orig_func) then*/
    _3527 = IS_ATOM(_source_orig_func_6623);
    if (_3527 == 0)
    {
        _3527 = NOVALUE;
        goto L4; // [88] 104
    }
    else{
        _3527 = NOVALUE;
    }

    /** 			d = call_func(dir_source, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_6614);
    *((int *)(_2+4)) = _path_name_6614;
    _3528 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3528);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_6617].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_6618);
    _d_6618 = _1;
    DeRefDS(_3528);
    _3528 = NOVALUE;
    goto L5; // [101] 148
L4: 

    /** 			d = call_func(dir_source, {path_name, source_user_data})*/
    Ref(_source_user_data_6624);
    RefDS(_path_name_6614);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_6614;
    ((int *)_2)[2] = _source_user_data_6624;
    _3530 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3530);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_6617].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8)
                         );
    DeRef(_d_6618);
    _d_6618 = _1;
    DeRefDS(_3530);
    _3530 = NOVALUE;
    goto L5; // [115] 148
L3: 

    /** 	elsif my_dir = DEFAULT_DIR_SOURCE then*/

    /** 		d = machine_func(M_DIR, path_name)*/
    DeRef(_d_6618);
    _d_6618 = machine(22, _path_name_6614);
    goto L5; // [132] 148

    /** 		d = call_func(my_dir, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_6614);
    *((int *)(_2+4)) = _path_name_6614;
    _3534 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3534);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[-2].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_6618);
    _d_6618 = _1;
    DeRefDS(_3534);
    _3534 = NOVALUE;
L5: 

    /** 	if atom(d) then*/
    _3536 = IS_ATOM(_d_6618);
    if (_3536 == 0)
    {
        _3536 = NOVALUE;
        goto L6; // [155] 165
    }
    else{
        _3536 = NOVALUE;
    }

    /** 		return W_BAD_PATH*/
    DeRefDS(_path_name_6614);
    DeRef(_your_function_6615);
    DeRef(_dir_source_6617);
    DeRef(_d_6618);
    DeRef(_abort_now_6619);
    DeRef(_orig_func_6620);
    DeRef(_user_data_6621);
    DeRef(_source_orig_func_6623);
    DeRef(_source_user_data_6624);
    return -1;
L6: 

    /** 	ifdef not UNIX then*/

    /** 		path_name = match_replace('/', path_name, '\\')*/
    RefDS(_path_name_6614);
    _0 = _path_name_6614;
    _path_name_6614 = _5match_replace(47, _path_name_6614, 92, 0);
    DeRefDS(_0);

    /** 	path_name = text:trim_tail(path_name, {' ', SLASH, '\n'})*/
    RefDS(_path_name_6614);
    RefDS(_3538);
    _0 = _path_name_6614;
    _path_name_6614 = _6trim_tail(_path_name_6614, _3538, 0);
    DeRefDS(_0);

    /** 	user_data[1] = path_name*/
    RefDS(_path_name_6614);
    _2 = (int)SEQ_PTR(_user_data_6621);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _path_name_6614;
    DeRef(_1);

    /** 	for i = 1 to length(d) do*/
    if (IS_SEQUENCE(_d_6618)){
            _3540 = SEQ_PTR(_d_6618)->length;
    }
    else {
        _3540 = 1;
    }
    {
        int _i_6658;
        _i_6658 = 1;
L7: 
        if (_i_6658 > _3540){
            goto L8; // [199] 366
        }

        /** 		if eu:find(d[i][D_NAME], {".", ".."}) then*/
        _2 = (int)SEQ_PTR(_d_6618);
        _3541 = (int)*(((s1_ptr)_2)->base + _i_6658);
        _2 = (int)SEQ_PTR(_3541);
        _3542 = (int)*(((s1_ptr)_2)->base + 1);
        _3541 = NOVALUE;
        RefDS(_3543);
        RefDS(_3512);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _3512;
        ((int *)_2)[2] = _3543;
        _3544 = MAKE_SEQ(_1);
        _3545 = find_from(_3542, _3544, 1);
        _3542 = NOVALUE;
        DeRefDS(_3544);
        _3544 = NOVALUE;
        if (_3545 == 0)
        {
            _3545 = NOVALUE;
            goto L9; // [227] 235
        }
        else{
            _3545 = NOVALUE;
        }

        /** 			continue*/
        goto LA; // [232] 361
L9: 

        /** 		user_data[2] = d[i]*/
        _2 = (int)SEQ_PTR(_d_6618);
        _3546 = (int)*(((s1_ptr)_2)->base + _i_6658);
        Ref(_3546);
        _2 = (int)SEQ_PTR(_user_data_6621);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _3546;
        if( _1 != _3546 ){
            DeRef(_1);
        }
        _3546 = NOVALUE;

        /** 		abort_now = call_func(your_function, user_data)*/
        _1 = (int)SEQ_PTR(_user_data_6621);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_your_function_6615].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
        }
        DeRef(_abort_now_6619);
        _abort_now_6619 = _1;

        /** 		if not equal(abort_now, 0) then*/
        if (_abort_now_6619 == 0)
        _3548 = 1;
        else if (IS_ATOM_INT(_abort_now_6619) && IS_ATOM_INT(0))
        _3548 = 0;
        else
        _3548 = (compare(_abort_now_6619, 0) == 0);
        if (_3548 != 0)
        goto LB; // [257] 267
        _3548 = NOVALUE;

        /** 			return abort_now*/
        DeRefDS(_path_name_6614);
        DeRef(_your_function_6615);
        DeRef(_dir_source_6617);
        DeRef(_d_6618);
        DeRef(_orig_func_6620);
        DeRefDS(_user_data_6621);
        DeRef(_source_orig_func_6623);
        DeRef(_source_user_data_6624);
        return _abort_now_6619;
LB: 

        /** 		if eu:find('d', d[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_d_6618);
        _3550 = (int)*(((s1_ptr)_2)->base + _i_6658);
        _2 = (int)SEQ_PTR(_3550);
        _3551 = (int)*(((s1_ptr)_2)->base + 2);
        _3550 = NOVALUE;
        _3552 = find_from(100, _3551, 1);
        _3551 = NOVALUE;
        if (_3552 == 0)
        {
            _3552 = NOVALUE;
            goto LC; // [284] 359
        }
        else{
            _3552 = NOVALUE;
        }

        /** 			if scan_subdirs then*/
        if (_scan_subdirs_6616 == 0)
        {
            goto LD; // [289] 358
        }
        else{
        }

        /** 				abort_now = walk_dir(path_name & SLASH & d[i][D_NAME],*/
        _2 = (int)SEQ_PTR(_d_6618);
        _3553 = (int)*(((s1_ptr)_2)->base + _i_6658);
        _2 = (int)SEQ_PTR(_3553);
        _3554 = (int)*(((s1_ptr)_2)->base + 1);
        _3553 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _3554;
            concat_list[1] = 92;
            concat_list[2] = _path_name_6614;
            Concat_N((object_ptr)&_3555, concat_list, 3);
        }
        _3554 = NOVALUE;
        Ref(_orig_func_6620);
        DeRef(_3556);
        _3556 = _orig_func_6620;
        DeRef(_3557);
        _3557 = _scan_subdirs_6616;
        Ref(_source_orig_func_6623);
        DeRef(_3558);
        _3558 = _source_orig_func_6623;
        _0 = _abort_now_6619;
        _abort_now_6619 = _8walk_dir(_3555, _3556, _3557, _3558);
        DeRef(_0);
        _3555 = NOVALUE;
        _3556 = NOVALUE;
        _3557 = NOVALUE;
        _3558 = NOVALUE;

        /** 				if not equal(abort_now, 0) and */
        if (_abort_now_6619 == 0)
        _3560 = 1;
        else if (IS_ATOM_INT(_abort_now_6619) && IS_ATOM_INT(0))
        _3560 = 0;
        else
        _3560 = (compare(_abort_now_6619, 0) == 0);
        _3561 = (_3560 == 0);
        _3560 = NOVALUE;
        if (_3561 == 0) {
            goto LE; // [335] 357
        }
        if (_abort_now_6619 == -1)
        _3563 = 1;
        else if (IS_ATOM_INT(_abort_now_6619) && IS_ATOM_INT(-1))
        _3563 = 0;
        else
        _3563 = (compare(_abort_now_6619, -1) == 0);
        _3564 = (_3563 == 0);
        _3563 = NOVALUE;
        if (_3564 == 0)
        {
            DeRef(_3564);
            _3564 = NOVALUE;
            goto LE; // [347] 357
        }
        else{
            DeRef(_3564);
            _3564 = NOVALUE;
        }

        /** 					return abort_now*/
        DeRefDS(_path_name_6614);
        DeRef(_your_function_6615);
        DeRef(_dir_source_6617);
        DeRef(_d_6618);
        DeRef(_orig_func_6620);
        DeRef(_user_data_6621);
        DeRef(_source_orig_func_6623);
        DeRef(_source_user_data_6624);
        DeRef(_3561);
        _3561 = NOVALUE;
        return _abort_now_6619;
LE: 
LD: 
LC: 

        /** 	end for*/
LA: 
        _i_6658 = _i_6658 + 1;
        goto L7; // [361] 206
L8: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_path_name_6614);
    DeRef(_your_function_6615);
    DeRef(_dir_source_6617);
    DeRef(_d_6618);
    DeRef(_abort_now_6619);
    DeRef(_orig_func_6620);
    DeRef(_user_data_6621);
    DeRef(_source_orig_func_6623);
    DeRef(_source_user_data_6624);
    DeRef(_3561);
    _3561 = NOVALUE;
    return 0;
    ;
}


int _8create_directory(int _name_6691, int _mode_6692, int _mkparent_6694)
{
    int _pname_6695 = NOVALUE;
    int _ret_6696 = NOVALUE;
    int _pos_6697 = NOVALUE;
    int _3584 = NOVALUE;
    int _3581 = NOVALUE;
    int _3580 = NOVALUE;
    int _3579 = NOVALUE;
    int _3578 = NOVALUE;
    int _3575 = NOVALUE;
    int _3572 = NOVALUE;
    int _3571 = NOVALUE;
    int _3569 = NOVALUE;
    int _3568 = NOVALUE;
    int _3566 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_mode_6692)) {
        _1 = (long)(DBL_PTR(_mode_6692)->dbl);
        if (UNIQUE(DBL_PTR(_mode_6692)) && (DBL_PTR(_mode_6692)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mode_6692);
        _mode_6692 = _1;
    }
    if (!IS_ATOM_INT(_mkparent_6694)) {
        _1 = (long)(DBL_PTR(_mkparent_6694)->dbl);
        if (UNIQUE(DBL_PTR(_mkparent_6694)) && (DBL_PTR(_mkparent_6694)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mkparent_6694);
        _mkparent_6694 = _1;
    }

    /** 	if length(name) = 0 then*/
    if (IS_SEQUENCE(_name_6691)){
            _3566 = SEQ_PTR(_name_6691)->length;
    }
    else {
        _3566 = 1;
    }
    if (_3566 != 0)
    goto L1; // [16] 27

    /** 		return 0 -- failed*/
    DeRefDS(_name_6691);
    DeRef(_pname_6695);
    DeRef(_ret_6696);
    return 0;
L1: 

    /** 	if name[$] = SLASH then*/
    if (IS_SEQUENCE(_name_6691)){
            _3568 = SEQ_PTR(_name_6691)->length;
    }
    else {
        _3568 = 1;
    }
    _2 = (int)SEQ_PTR(_name_6691);
    _3569 = (int)*(((s1_ptr)_2)->base + _3568);
    if (binary_op_a(NOTEQ, _3569, 92)){
        _3569 = NOVALUE;
        goto L2; // [36] 55
    }
    _3569 = NOVALUE;

    /** 		name = name[1 .. $-1]*/
    if (IS_SEQUENCE(_name_6691)){
            _3571 = SEQ_PTR(_name_6691)->length;
    }
    else {
        _3571 = 1;
    }
    _3572 = _3571 - 1;
    _3571 = NOVALUE;
    rhs_slice_target = (object_ptr)&_name_6691;
    RHS_Slice(_name_6691, 1, _3572);
L2: 

    /** 	if mkparent != 0 then*/
    if (_mkparent_6694 == 0)
    goto L3; // [57] 107

    /** 		pos = search:rfind(SLASH, name)*/
    if (IS_SEQUENCE(_name_6691)){
            _3575 = SEQ_PTR(_name_6691)->length;
    }
    else {
        _3575 = 1;
    }
    RefDS(_name_6691);
    _pos_6697 = _5rfind(92, _name_6691, _3575);
    _3575 = NOVALUE;
    if (!IS_ATOM_INT(_pos_6697)) {
        _1 = (long)(DBL_PTR(_pos_6697)->dbl);
        if (UNIQUE(DBL_PTR(_pos_6697)) && (DBL_PTR(_pos_6697)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_6697);
        _pos_6697 = _1;
    }

    /** 		if pos != 0 then*/
    if (_pos_6697 == 0)
    goto L4; // [78] 106

    /** 			ret = create_directory(name[1.. pos-1], mode, mkparent)*/
    _3578 = _pos_6697 - 1;
    rhs_slice_target = (object_ptr)&_3579;
    RHS_Slice(_name_6691, 1, _3578);
    DeRef(_3580);
    _3580 = _mode_6692;
    DeRef(_3581);
    _3581 = _mkparent_6694;
    _0 = _ret_6696;
    _ret_6696 = _8create_directory(_3579, _3580, _3581);
    DeRef(_0);
    _3579 = NOVALUE;
    _3580 = NOVALUE;
    _3581 = NOVALUE;
L4: 
L3: 

    /** 	pname = machine:allocate_string(name)*/
    RefDS(_name_6691);
    _0 = _pname_6695;
    _pname_6695 = _11allocate_string(_name_6691, 0);
    DeRef(_0);

    /** 	ifdef UNIX then*/

    /** 		ret = c_func(xCreateDirectory, {pname, 0})*/
    Ref(_pname_6695);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pname_6695;
    ((int *)_2)[2] = 0;
    _3584 = MAKE_SEQ(_1);
    DeRef(_ret_6696);
    _ret_6696 = call_c(1, _8xCreateDirectory_6519, _3584);
    DeRefDS(_3584);
    _3584 = NOVALUE;

    /** 		mode = mode -- get rid of not used warning*/
    _mode_6692 = _mode_6692;

    /** 	return ret*/
    DeRefDS(_name_6691);
    DeRef(_pname_6695);
    DeRef(_3572);
    _3572 = NOVALUE;
    DeRef(_3578);
    _3578 = NOVALUE;
    return _ret_6696;
    ;
}


int _8create_file(int _name_6724)
{
    int _fh_6725 = NOVALUE;
    int _ret_6727 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fh = open(name, "wb")*/
    _fh_6725 = EOpen(_name_6724, _949, 0);

    /** 	integer ret = (fh != -1)*/
    _ret_6727 = (_fh_6725 != -1);

    /** 	if ret then*/
    if (_ret_6727 == 0)
    {
        goto L1; // [22] 30
    }
    else{
    }

    /** 		close(fh)*/
    EClose(_fh_6725);
L1: 

    /** 	return ret*/
    DeRefDS(_name_6724);
    return _ret_6727;
    ;
}


int _8delete_file(int _name_6732)
{
    int _pfilename_6733 = NOVALUE;
    int _success_6735 = NOVALUE;
    int _3589 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom pfilename = machine:allocate_string(name)*/
    RefDS(_name_6732);
    _0 = _pfilename_6733;
    _pfilename_6733 = _11allocate_string(_name_6732, 0);
    DeRef(_0);

    /** 	integer success = c_func(xDeleteFile, {pfilename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pfilename_6733);
    *((int *)(_2+4)) = _pfilename_6733;
    _3589 = MAKE_SEQ(_1);
    _success_6735 = call_c(1, _8xDeleteFile_6515, _3589);
    DeRefDS(_3589);
    _3589 = NOVALUE;
    if (!IS_ATOM_INT(_success_6735)) {
        _1 = (long)(DBL_PTR(_success_6735)->dbl);
        if (UNIQUE(DBL_PTR(_success_6735)) && (DBL_PTR(_success_6735)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_6735);
        _success_6735 = _1;
    }

    /** 	ifdef UNIX then*/

    /** 	machine:free(pfilename)*/
    Ref(_pfilename_6733);
    _11free(_pfilename_6733);

    /** 	return success*/
    DeRefDS(_name_6732);
    DeRef(_pfilename_6733);
    return _success_6735;
    ;
}


int _8curdir(int _drive_id_6740)
{
    int _lCurDir_6741 = NOVALUE;
    int _lOrigDir_6742 = NOVALUE;
    int _lDrive_6743 = NOVALUE;
    int _current_dir_inlined_current_dir_at_27_6748 = NOVALUE;
    int _chdir_inlined_chdir_at_57_6751 = NOVALUE;
    int _current_dir_inlined_current_dir_at_79_6754 = NOVALUE;
    int _chdir_inlined_chdir_at_111_6761 = NOVALUE;
    int _newdir_inlined_chdir_at_108_6760 = NOVALUE;
    int _3597 = NOVALUE;
    int _3596 = NOVALUE;
    int _3595 = NOVALUE;
    int _3593 = NOVALUE;
    int _3591 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_drive_id_6740)) {
        _1 = (long)(DBL_PTR(_drive_id_6740)->dbl);
        if (UNIQUE(DBL_PTR(_drive_id_6740)) && (DBL_PTR(_drive_id_6740)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_drive_id_6740);
        _drive_id_6740 = _1;
    }

    /** 	ifdef not LINUX then*/

    /** 	    sequence lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_6742);
    _lOrigDir_6742 = _5;

    /** 	    sequence lDrive*/

    /** 	    if t_alpha(drive_id) then*/
    _3591 = _3t_alpha(_drive_id_6740);
    if (_3591 == 0) {
        DeRef(_3591);
        _3591 = NOVALUE;
        goto L1; // [22] 77
    }
    else {
        if (!IS_ATOM_INT(_3591) && DBL_PTR(_3591)->dbl == 0.0){
            DeRef(_3591);
            _3591 = NOVALUE;
            goto L1; // [22] 77
        }
        DeRef(_3591);
        _3591 = NOVALUE;
    }
    DeRef(_3591);
    _3591 = NOVALUE;

    /** 		    lOrigDir =  current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefDSi(_lOrigDir_6742);
    _lOrigDir_6742 = machine(23, 0);

    /** 		    lDrive = "  "*/
    RefDS(_91);
    DeRefi(_lDrive_6743);
    _lDrive_6743 = _91;

    /** 		    lDrive[1] = drive_id*/
    _2 = (int)SEQ_PTR(_lDrive_6743);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_6743 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _drive_id_6740;

    /** 		    lDrive[2] = ':'*/
    _2 = (int)SEQ_PTR(_lDrive_6743);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_6743 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    *(int *)_2 = 58;

    /** 		    if chdir(lDrive) = 0 then*/

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_57_6751 = machine(63, _lDrive_6743);
    if (_chdir_inlined_chdir_at_57_6751 != 0)
    goto L2; // [64] 76

    /** 		    	lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_6742);
    _lOrigDir_6742 = _5;
L2: 
L1: 

    /**     lCurDir = current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_lCurDir_6741);
    _lCurDir_6741 = machine(23, 0);

    /** 	ifdef not LINUX then*/

    /** 		if length(lOrigDir) > 0 then*/
    if (IS_SEQUENCE(_lOrigDir_6742)){
            _3593 = SEQ_PTR(_lOrigDir_6742)->length;
    }
    else {
        _3593 = 1;
    }
    if (_3593 <= 0)
    goto L3; // [97] 123

    /** 	    	chdir(lOrigDir[1..2])*/
    rhs_slice_target = (object_ptr)&_3595;
    RHS_Slice(_lOrigDir_6742, 1, 2);
    DeRefi(_newdir_inlined_chdir_at_108_6760);
    _newdir_inlined_chdir_at_108_6760 = _3595;
    _3595 = NOVALUE;

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_111_6761 = machine(63, _newdir_inlined_chdir_at_108_6760);
    DeRefi(_newdir_inlined_chdir_at_108_6760);
    _newdir_inlined_chdir_at_108_6760 = NOVALUE;
L3: 

    /** 	if (lCurDir[$] != SLASH) then*/
    if (IS_SEQUENCE(_lCurDir_6741)){
            _3596 = SEQ_PTR(_lCurDir_6741)->length;
    }
    else {
        _3596 = 1;
    }
    _2 = (int)SEQ_PTR(_lCurDir_6741);
    _3597 = (int)*(((s1_ptr)_2)->base + _3596);
    if (_3597 == 92)
    goto L4; // [132] 143

    /** 		lCurDir &= SLASH*/
    Append(&_lCurDir_6741, _lCurDir_6741, 92);
L4: 

    /** 	return lCurDir*/
    DeRefi(_lOrigDir_6742);
    DeRefi(_lDrive_6743);
    _3597 = NOVALUE;
    return _lCurDir_6741;
    ;
}


int _8init_curdir()
{
    int _0, _1, _2;
    

    /** 	return InitCurDir*/
    RefDS(_8InitCurDir_6767);
    return _8InitCurDir_6767;
    ;
}


int _8clear_directory(int _path_6773, int _recurse_6774)
{
    int _files_6775 = NOVALUE;
    int _ret_6776 = NOVALUE;
    int _dir_inlined_dir_at_91_6797 = NOVALUE;
    int _cnt_6822 = NOVALUE;
    int _3641 = NOVALUE;
    int _3640 = NOVALUE;
    int _3639 = NOVALUE;
    int _3638 = NOVALUE;
    int _3634 = NOVALUE;
    int _3633 = NOVALUE;
    int _3632 = NOVALUE;
    int _3631 = NOVALUE;
    int _3630 = NOVALUE;
    int _3629 = NOVALUE;
    int _3628 = NOVALUE;
    int _3627 = NOVALUE;
    int _3624 = NOVALUE;
    int _3623 = NOVALUE;
    int _3622 = NOVALUE;
    int _3620 = NOVALUE;
    int _3619 = NOVALUE;
    int _3618 = NOVALUE;
    int _3616 = NOVALUE;
    int _3615 = NOVALUE;
    int _3613 = NOVALUE;
    int _3611 = NOVALUE;
    int _3609 = NOVALUE;
    int _3607 = NOVALUE;
    int _3606 = NOVALUE;
    int _3604 = NOVALUE;
    int _3603 = NOVALUE;
    int _3601 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recurse_6774)) {
        _1 = (long)(DBL_PTR(_recurse_6774)->dbl);
        if (UNIQUE(DBL_PTR(_recurse_6774)) && (DBL_PTR(_recurse_6774)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recurse_6774);
        _recurse_6774 = _1;
    }

    /** 	if length(path) > 0 then*/
    if (IS_SEQUENCE(_path_6773)){
            _3601 = SEQ_PTR(_path_6773)->length;
    }
    else {
        _3601 = 1;
    }
    if (_3601 <= 0)
    goto L1; // [12] 45

    /** 		if path[$] = SLASH then*/
    if (IS_SEQUENCE(_path_6773)){
            _3603 = SEQ_PTR(_path_6773)->length;
    }
    else {
        _3603 = 1;
    }
    _2 = (int)SEQ_PTR(_path_6773);
    _3604 = (int)*(((s1_ptr)_2)->base + _3603);
    if (binary_op_a(NOTEQ, _3604, 92)){
        _3604 = NOVALUE;
        goto L2; // [25] 44
    }
    _3604 = NOVALUE;

    /** 			path = path[1 .. $-1]*/
    if (IS_SEQUENCE(_path_6773)){
            _3606 = SEQ_PTR(_path_6773)->length;
    }
    else {
        _3606 = 1;
    }
    _3607 = _3606 - 1;
    _3606 = NOVALUE;
    rhs_slice_target = (object_ptr)&_path_6773;
    RHS_Slice(_path_6773, 1, _3607);
L2: 
L1: 

    /** 	if length(path) = 0 then*/
    if (IS_SEQUENCE(_path_6773)){
            _3609 = SEQ_PTR(_path_6773)->length;
    }
    else {
        _3609 = 1;
    }
    if (_3609 != 0)
    goto L3; // [50] 61

    /** 		return 0 -- Nothing specified to clear. Not safe to assume anything.*/
    DeRefDS(_path_6773);
    DeRef(_files_6775);
    DeRef(_3607);
    _3607 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(path) = 2 then*/
    if (IS_SEQUENCE(_path_6773)){
            _3611 = SEQ_PTR(_path_6773)->length;
    }
    else {
        _3611 = 1;
    }
    if (_3611 != 2)
    goto L4; // [68] 90

    /** 			if path[2] = ':' then*/
    _2 = (int)SEQ_PTR(_path_6773);
    _3613 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3613, 58)){
        _3613 = NOVALUE;
        goto L5; // [78] 89
    }
    _3613 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_path_6773);
    DeRef(_files_6775);
    DeRef(_3607);
    _3607 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(path)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_6775);
    _files_6775 = machine(22, _path_6773);

    /** 	if atom(files) then*/
    _3615 = IS_ATOM(_files_6775);
    if (_3615 == 0)
    {
        _3615 = NOVALUE;
        goto L6; // [106] 116
    }
    else{
        _3615 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_path_6773);
    DeRef(_files_6775);
    DeRef(_3607);
    _3607 = NOVALUE;
    return 0;
L6: 

    /** 	ifdef WINDOWS then*/

    /** 		if length( files ) < 3 then*/
    if (IS_SEQUENCE(_files_6775)){
            _3616 = SEQ_PTR(_files_6775)->length;
    }
    else {
        _3616 = 1;
    }
    if (_3616 >= 3)
    goto L7; // [123] 134

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_6773);
    DeRef(_files_6775);
    DeRef(_3607);
    _3607 = NOVALUE;
    return 0;
L7: 

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_6775);
    _3618 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3618);
    _3619 = (int)*(((s1_ptr)_2)->base + 1);
    _3618 = NOVALUE;
    if (_3619 == _3512)
    _3620 = 1;
    else if (IS_ATOM_INT(_3619) && IS_ATOM_INT(_3512))
    _3620 = 0;
    else
    _3620 = (compare(_3619, _3512) == 0);
    _3619 = NOVALUE;
    if (_3620 != 0)
    goto L8; // [150] 160
    _3620 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_6773);
    DeRef(_files_6775);
    DeRef(_3607);
    _3607 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_6775);
    _3622 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3622);
    _3623 = (int)*(((s1_ptr)_2)->base + 2);
    _3622 = NOVALUE;
    _3624 = find_from(100, _3623, 1);
    _3623 = NOVALUE;
    if (_3624 != 0)
    goto L9; // [177] 187
    _3624 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_6773);
    DeRef(_files_6775);
    DeRef(_3607);
    _3607 = NOVALUE;
    return 0;
L9: 

    /** 	ret = 1*/
    _ret_6776 = 1;

    /** 	path &= SLASH*/
    Append(&_path_6773, _path_6773, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_6775)){
            _3627 = SEQ_PTR(_files_6775)->length;
    }
    else {
        _3627 = 1;
    }
    {
        int _i_6815;
        _i_6815 = 3;
LA: 
        if (_i_6815 > _3627){
            goto LB; // [207] 348
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_6775);
        _3628 = (int)*(((s1_ptr)_2)->base + _i_6815);
        _2 = (int)SEQ_PTR(_3628);
        _3629 = (int)*(((s1_ptr)_2)->base + 2);
        _3628 = NOVALUE;
        _3630 = find_from(100, _3629, 1);
        _3629 = NOVALUE;
        if (_3630 == 0)
        {
            _3630 = NOVALUE;
            goto LC; // [231] 301
        }
        else{
            _3630 = NOVALUE;
        }

        /** 				if recurse then*/
        if (_recurse_6774 == 0)
        {
            goto LD; // [236] 343
        }
        else{
        }

        /** 					integer cnt = clear_directory(path & files[i][D_NAME], recurse)*/
        _2 = (int)SEQ_PTR(_files_6775);
        _3631 = (int)*(((s1_ptr)_2)->base + _i_6815);
        _2 = (int)SEQ_PTR(_3631);
        _3632 = (int)*(((s1_ptr)_2)->base + 1);
        _3631 = NOVALUE;
        if (IS_SEQUENCE(_path_6773) && IS_ATOM(_3632)) {
            Ref(_3632);
            Append(&_3633, _path_6773, _3632);
        }
        else if (IS_ATOM(_path_6773) && IS_SEQUENCE(_3632)) {
        }
        else {
            Concat((object_ptr)&_3633, _path_6773, _3632);
        }
        _3632 = NOVALUE;
        DeRef(_3634);
        _3634 = _recurse_6774;
        _cnt_6822 = _8clear_directory(_3633, _3634);
        _3633 = NOVALUE;
        _3634 = NOVALUE;
        if (!IS_ATOM_INT(_cnt_6822)) {
            _1 = (long)(DBL_PTR(_cnt_6822)->dbl);
            if (UNIQUE(DBL_PTR(_cnt_6822)) && (DBL_PTR(_cnt_6822)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_cnt_6822);
            _cnt_6822 = _1;
        }

        /** 					if cnt = 0 then*/
        if (_cnt_6822 != 0)
        goto LE; // [269] 280

        /** 						return 0*/
        DeRefDS(_path_6773);
        DeRef(_files_6775);
        DeRef(_3607);
        _3607 = NOVALUE;
        return 0;
LE: 

        /** 					ret += cnt*/
        _ret_6776 = _ret_6776 + _cnt_6822;
        goto LF; // [290] 341

        /** 					continue*/
        goto LD; // [295] 343
        goto LF; // [298] 341
LC: 

        /** 				if delete_file(path & files[i][D_NAME]) = 0 then*/
        _2 = (int)SEQ_PTR(_files_6775);
        _3638 = (int)*(((s1_ptr)_2)->base + _i_6815);
        _2 = (int)SEQ_PTR(_3638);
        _3639 = (int)*(((s1_ptr)_2)->base + 1);
        _3638 = NOVALUE;
        if (IS_SEQUENCE(_path_6773) && IS_ATOM(_3639)) {
            Ref(_3639);
            Append(&_3640, _path_6773, _3639);
        }
        else if (IS_ATOM(_path_6773) && IS_SEQUENCE(_3639)) {
        }
        else {
            Concat((object_ptr)&_3640, _path_6773, _3639);
        }
        _3639 = NOVALUE;
        _3641 = _8delete_file(_3640);
        _3640 = NOVALUE;
        if (binary_op_a(NOTEQ, _3641, 0)){
            DeRef(_3641);
            _3641 = NOVALUE;
            goto L10; // [321] 332
        }
        DeRef(_3641);
        _3641 = NOVALUE;

        /** 					return 0*/
        DeRefDS(_path_6773);
        DeRef(_files_6775);
        DeRef(_3607);
        _3607 = NOVALUE;
        return 0;
L10: 

        /** 				ret += 1*/
        _ret_6776 = _ret_6776 + 1;
LF: 

        /** 		end for*/
LD: 
        _i_6815 = _i_6815 + 1;
        goto LA; // [343] 214
LB: 
        ;
    }

    /** 	return ret*/
    DeRefDS(_path_6773);
    DeRef(_files_6775);
    DeRef(_3607);
    _3607 = NOVALUE;
    return _ret_6776;
    ;
}


int _8remove_directory(int _dir_name_6842, int _force_6843)
{
    int _pname_6844 = NOVALUE;
    int _ret_6845 = NOVALUE;
    int _files_6846 = NOVALUE;
    int _D_NAME_6847 = NOVALUE;
    int _D_ATTRIBUTES_6848 = NOVALUE;
    int _dir_inlined_dir_at_103_6869 = NOVALUE;
    int _3688 = NOVALUE;
    int _3684 = NOVALUE;
    int _3683 = NOVALUE;
    int _3682 = NOVALUE;
    int _3680 = NOVALUE;
    int _3679 = NOVALUE;
    int _3678 = NOVALUE;
    int _3677 = NOVALUE;
    int _3676 = NOVALUE;
    int _3675 = NOVALUE;
    int _3674 = NOVALUE;
    int _3673 = NOVALUE;
    int _3669 = NOVALUE;
    int _3667 = NOVALUE;
    int _3666 = NOVALUE;
    int _3665 = NOVALUE;
    int _3663 = NOVALUE;
    int _3662 = NOVALUE;
    int _3661 = NOVALUE;
    int _3659 = NOVALUE;
    int _3658 = NOVALUE;
    int _3656 = NOVALUE;
    int _3654 = NOVALUE;
    int _3652 = NOVALUE;
    int _3650 = NOVALUE;
    int _3649 = NOVALUE;
    int _3647 = NOVALUE;
    int _3646 = NOVALUE;
    int _3644 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_force_6843)) {
        _1 = (long)(DBL_PTR(_force_6843)->dbl);
        if (UNIQUE(DBL_PTR(_force_6843)) && (DBL_PTR(_force_6843)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_force_6843);
        _force_6843 = _1;
    }

    /** 	integer D_NAME = 1, D_ATTRIBUTES = 2*/
    _D_NAME_6847 = 1;
    _D_ATTRIBUTES_6848 = 2;

    /**  	if length(dir_name) > 0 then*/
    if (IS_SEQUENCE(_dir_name_6842)){
            _3644 = SEQ_PTR(_dir_name_6842)->length;
    }
    else {
        _3644 = 1;
    }
    if (_3644 <= 0)
    goto L1; // [24] 57

    /** 		if dir_name[$] = SLASH then*/
    if (IS_SEQUENCE(_dir_name_6842)){
            _3646 = SEQ_PTR(_dir_name_6842)->length;
    }
    else {
        _3646 = 1;
    }
    _2 = (int)SEQ_PTR(_dir_name_6842);
    _3647 = (int)*(((s1_ptr)_2)->base + _3646);
    if (binary_op_a(NOTEQ, _3647, 92)){
        _3647 = NOVALUE;
        goto L2; // [37] 56
    }
    _3647 = NOVALUE;

    /** 			dir_name = dir_name[1 .. $-1]*/
    if (IS_SEQUENCE(_dir_name_6842)){
            _3649 = SEQ_PTR(_dir_name_6842)->length;
    }
    else {
        _3649 = 1;
    }
    _3650 = _3649 - 1;
    _3649 = NOVALUE;
    rhs_slice_target = (object_ptr)&_dir_name_6842;
    RHS_Slice(_dir_name_6842, 1, _3650);
L2: 
L1: 

    /** 	if length(dir_name) = 0 then*/
    if (IS_SEQUENCE(_dir_name_6842)){
            _3652 = SEQ_PTR(_dir_name_6842)->length;
    }
    else {
        _3652 = 1;
    }
    if (_3652 != 0)
    goto L3; // [62] 73

    /** 		return 0	-- nothing specified to delete.*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_ret_6845);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(dir_name) = 2 then*/
    if (IS_SEQUENCE(_dir_name_6842)){
            _3654 = SEQ_PTR(_dir_name_6842)->length;
    }
    else {
        _3654 = 1;
    }
    if (_3654 != 2)
    goto L4; // [80] 102

    /** 			if dir_name[2] = ':' then*/
    _2 = (int)SEQ_PTR(_dir_name_6842);
    _3656 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3656, 58)){
        _3656 = NOVALUE;
        goto L5; // [90] 101
    }
    _3656 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_ret_6845);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(dir_name)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_6846);
    _files_6846 = machine(22, _dir_name_6842);

    /** 	if atom(files) then*/
    _3658 = IS_ATOM(_files_6846);
    if (_3658 == 0)
    {
        _3658 = NOVALUE;
        goto L6; // [118] 128
    }
    else{
        _3658 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_ret_6845);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return 0;
L6: 

    /** 	if length( files ) < 2 then*/
    if (IS_SEQUENCE(_files_6846)){
            _3659 = SEQ_PTR(_files_6846)->length;
    }
    else {
        _3659 = 1;
    }
    if (_3659 >= 2)
    goto L7; // [133] 144

    /** 		return 0	-- Supplied dir_name was not a directory*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_ret_6845);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return 0;
L7: 

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_6846);
    _3661 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3661);
    _3662 = (int)*(((s1_ptr)_2)->base + _D_NAME_6847);
    _3661 = NOVALUE;
    if (_3662 == _3512)
    _3663 = 1;
    else if (IS_ATOM_INT(_3662) && IS_ATOM_INT(_3512))
    _3663 = 0;
    else
    _3663 = (compare(_3662, _3512) == 0);
    _3662 = NOVALUE;
    if (_3663 != 0)
    goto L8; // [160] 170
    _3663 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_ret_6845);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_6846);
    _3665 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3665);
    _3666 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_6848);
    _3665 = NOVALUE;
    _3667 = find_from(100, _3666, 1);
    _3666 = NOVALUE;
    if (_3667 != 0)
    goto L9; // [185] 195
    _3667 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_ret_6845);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return 0;
L9: 

    /** 		if length(files) > 2 then*/
    if (IS_SEQUENCE(_files_6846)){
            _3669 = SEQ_PTR(_files_6846)->length;
    }
    else {
        _3669 = 1;
    }
    if (_3669 <= 2)
    goto LA; // [200] 217

    /** 			if not force then*/
    if (_force_6843 != 0)
    goto LB; // [206] 216

    /** 				return 0 -- Directory is not already emptied.*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_ret_6845);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return 0;
LB: 
LA: 

    /** 	dir_name &= SLASH*/
    Append(&_dir_name_6842, _dir_name_6842, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_6846)){
            _3673 = SEQ_PTR(_files_6846)->length;
    }
    else {
        _3673 = 1;
    }
    {
        int _i_6892;
        _i_6892 = 3;
LC: 
        if (_i_6892 > _3673){
            goto LD; // [230] 322
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_6846);
        _3674 = (int)*(((s1_ptr)_2)->base + _i_6892);
        _2 = (int)SEQ_PTR(_3674);
        _3675 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_6848);
        _3674 = NOVALUE;
        _3676 = find_from(100, _3675, 1);
        _3675 = NOVALUE;
        if (_3676 == 0)
        {
            _3676 = NOVALUE;
            goto LE; // [252] 282
        }
        else{
            _3676 = NOVALUE;
        }

        /** 				ret = remove_directory(dir_name & files[i][D_NAME] & SLASH, force)*/
        _2 = (int)SEQ_PTR(_files_6846);
        _3677 = (int)*(((s1_ptr)_2)->base + _i_6892);
        _2 = (int)SEQ_PTR(_3677);
        _3678 = (int)*(((s1_ptr)_2)->base + _D_NAME_6847);
        _3677 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = 92;
            concat_list[1] = _3678;
            concat_list[2] = _dir_name_6842;
            Concat_N((object_ptr)&_3679, concat_list, 3);
        }
        _3678 = NOVALUE;
        DeRef(_3680);
        _3680 = _force_6843;
        _0 = _ret_6845;
        _ret_6845 = _8remove_directory(_3679, _3680);
        DeRef(_0);
        _3679 = NOVALUE;
        _3680 = NOVALUE;
        goto LF; // [279] 301
LE: 

        /** 				ret = delete_file(dir_name & files[i][D_NAME])*/
        _2 = (int)SEQ_PTR(_files_6846);
        _3682 = (int)*(((s1_ptr)_2)->base + _i_6892);
        _2 = (int)SEQ_PTR(_3682);
        _3683 = (int)*(((s1_ptr)_2)->base + _D_NAME_6847);
        _3682 = NOVALUE;
        if (IS_SEQUENCE(_dir_name_6842) && IS_ATOM(_3683)) {
            Ref(_3683);
            Append(&_3684, _dir_name_6842, _3683);
        }
        else if (IS_ATOM(_dir_name_6842) && IS_SEQUENCE(_3683)) {
        }
        else {
            Concat((object_ptr)&_3684, _dir_name_6842, _3683);
        }
        _3683 = NOVALUE;
        _0 = _ret_6845;
        _ret_6845 = _8delete_file(_3684);
        DeRef(_0);
        _3684 = NOVALUE;
LF: 

        /** 			if not ret then*/
        if (IS_ATOM_INT(_ret_6845)) {
            if (_ret_6845 != 0){
                goto L10; // [305] 315
            }
        }
        else {
            if (DBL_PTR(_ret_6845)->dbl != 0.0){
                goto L10; // [305] 315
            }
        }

        /** 				return 0*/
        DeRefDS(_dir_name_6842);
        DeRef(_pname_6844);
        DeRef(_ret_6845);
        DeRef(_files_6846);
        DeRef(_3650);
        _3650 = NOVALUE;
        return 0;
L10: 

        /** 		end for*/
        _i_6892 = _i_6892 + 1;
        goto LC; // [317] 237
LD: 
        ;
    }

    /** 	pname = machine:allocate_string(dir_name)*/
    RefDS(_dir_name_6842);
    _0 = _pname_6844;
    _pname_6844 = _11allocate_string(_dir_name_6842, 0);
    DeRef(_0);

    /** 	ret = c_func(xRemoveDirectory, {pname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pname_6844);
    *((int *)(_2+4)) = _pname_6844;
    _3688 = MAKE_SEQ(_1);
    DeRef(_ret_6845);
    _ret_6845 = call_c(1, _8xRemoveDirectory_6526, _3688);
    DeRefDS(_3688);
    _3688 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free(pname)*/
    Ref(_pname_6844);
    _11free(_pname_6844);

    /** 	return ret*/
    DeRefDS(_dir_name_6842);
    DeRef(_pname_6844);
    DeRef(_files_6846);
    DeRef(_3650);
    _3650 = NOVALUE;
    return _ret_6845;
    ;
}


int _8pathinfo(int _path_6924, int _std_slash_6925)
{
    int _slash_6926 = NOVALUE;
    int _period_6927 = NOVALUE;
    int _ch_6928 = NOVALUE;
    int _dir_name_6929 = NOVALUE;
    int _file_name_6930 = NOVALUE;
    int _file_ext_6931 = NOVALUE;
    int _file_full_6932 = NOVALUE;
    int _drive_id_6933 = NOVALUE;
    int _from_slash_6974 = NOVALUE;
    int _3727 = NOVALUE;
    int _3719 = NOVALUE;
    int _3718 = NOVALUE;
    int _3715 = NOVALUE;
    int _3714 = NOVALUE;
    int _3712 = NOVALUE;
    int _3711 = NOVALUE;
    int _3708 = NOVALUE;
    int _3707 = NOVALUE;
    int _3705 = NOVALUE;
    int _3701 = NOVALUE;
    int _3699 = NOVALUE;
    int _3698 = NOVALUE;
    int _3697 = NOVALUE;
    int _3696 = NOVALUE;
    int _3694 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_std_slash_6925)) {
        _1 = (long)(DBL_PTR(_std_slash_6925)->dbl);
        if (UNIQUE(DBL_PTR(_std_slash_6925)) && (DBL_PTR(_std_slash_6925)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_std_slash_6925);
        _std_slash_6925 = _1;
    }

    /** 	dir_name  = ""*/
    RefDS(_5);
    DeRef(_dir_name_6929);
    _dir_name_6929 = _5;

    /** 	file_name = ""*/
    RefDS(_5);
    DeRef(_file_name_6930);
    _file_name_6930 = _5;

    /** 	file_ext  = ""*/
    RefDS(_5);
    DeRef(_file_ext_6931);
    _file_ext_6931 = _5;

    /** 	file_full = ""*/
    RefDS(_5);
    DeRef(_file_full_6932);
    _file_full_6932 = _5;

    /** 	drive_id  = ""*/
    RefDS(_5);
    DeRef(_drive_id_6933);
    _drive_id_6933 = _5;

    /** 	slash = 0*/
    _slash_6926 = 0;

    /** 	period = 0*/
    _period_6927 = 0;

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_6924)){
            _3694 = SEQ_PTR(_path_6924)->length;
    }
    else {
        _3694 = 1;
    }
    {
        int _i_6935;
        _i_6935 = _3694;
L1: 
        if (_i_6935 < 1){
            goto L2; // [61] 134
        }

        /** 		ch = path[i]*/
        _2 = (int)SEQ_PTR(_path_6924);
        _ch_6928 = (int)*(((s1_ptr)_2)->base + _i_6935);
        if (!IS_ATOM_INT(_ch_6928))
        _ch_6928 = (long)DBL_PTR(_ch_6928)->dbl;

        /** 		if period = 0 and ch = '.' then*/
        _3696 = (_period_6927 == 0);
        if (_3696 == 0) {
            goto L3; // [82] 104
        }
        _3698 = (_ch_6928 == 46);
        if (_3698 == 0)
        {
            DeRef(_3698);
            _3698 = NOVALUE;
            goto L3; // [91] 104
        }
        else{
            DeRef(_3698);
            _3698 = NOVALUE;
        }

        /** 			period = i*/
        _period_6927 = _i_6935;
        goto L4; // [101] 127
L3: 

        /** 		elsif eu:find(ch, SLASHES) then*/
        _3699 = find_from(_ch_6928, _8SLASHES_6543, 1);
        if (_3699 == 0)
        {
            _3699 = NOVALUE;
            goto L5; // [111] 126
        }
        else{
            _3699 = NOVALUE;
        }

        /** 			slash = i*/
        _slash_6926 = _i_6935;

        /** 			exit*/
        goto L2; // [123] 134
L5: 
L4: 

        /** 	end for*/
        _i_6935 = _i_6935 + -1;
        goto L1; // [129] 68
L2: 
        ;
    }

    /** 	if slash > 0 then*/
    if (_slash_6926 <= 0)
    goto L6; // [136] 195

    /** 		dir_name = path[1..slash-1]*/
    _3701 = _slash_6926 - 1;
    rhs_slice_target = (object_ptr)&_dir_name_6929;
    RHS_Slice(_path_6924, 1, _3701);

    /** 		ifdef not UNIX then*/

    /** 			ch = eu:find(':', dir_name)*/
    _ch_6928 = find_from(58, _dir_name_6929, 1);

    /** 			if ch != 0 then*/
    if (_ch_6928 == 0)
    goto L7; // [164] 194

    /** 				drive_id = dir_name[1..ch-1]*/
    _3705 = _ch_6928 - 1;
    rhs_slice_target = (object_ptr)&_drive_id_6933;
    RHS_Slice(_dir_name_6929, 1, _3705);

    /** 				dir_name = dir_name[ch+1..$]*/
    _3707 = _ch_6928 + 1;
    if (IS_SEQUENCE(_dir_name_6929)){
            _3708 = SEQ_PTR(_dir_name_6929)->length;
    }
    else {
        _3708 = 1;
    }
    rhs_slice_target = (object_ptr)&_dir_name_6929;
    RHS_Slice(_dir_name_6929, _3707, _3708);
L7: 
L6: 

    /** 	if period > 0 then*/
    if (_period_6927 <= 0)
    goto L8; // [197] 241

    /** 		file_name = path[slash+1..period-1]*/
    _3711 = _slash_6926 + 1;
    if (_3711 > MAXINT){
        _3711 = NewDouble((double)_3711);
    }
    _3712 = _period_6927 - 1;
    rhs_slice_target = (object_ptr)&_file_name_6930;
    RHS_Slice(_path_6924, _3711, _3712);

    /** 		file_ext = path[period+1..$]*/
    _3714 = _period_6927 + 1;
    if (_3714 > MAXINT){
        _3714 = NewDouble((double)_3714);
    }
    if (IS_SEQUENCE(_path_6924)){
            _3715 = SEQ_PTR(_path_6924)->length;
    }
    else {
        _3715 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_ext_6931;
    RHS_Slice(_path_6924, _3714, _3715);

    /** 		file_full = file_name & '.' & file_ext*/
    {
        int concat_list[3];

        concat_list[0] = _file_ext_6931;
        concat_list[1] = 46;
        concat_list[2] = _file_name_6930;
        Concat_N((object_ptr)&_file_full_6932, concat_list, 3);
    }
    goto L9; // [238] 263
L8: 

    /** 		file_name = path[slash+1..$]*/
    _3718 = _slash_6926 + 1;
    if (_3718 > MAXINT){
        _3718 = NewDouble((double)_3718);
    }
    if (IS_SEQUENCE(_path_6924)){
            _3719 = SEQ_PTR(_path_6924)->length;
    }
    else {
        _3719 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_name_6930;
    RHS_Slice(_path_6924, _3718, _3719);

    /** 		file_full = file_name*/
    RefDS(_file_name_6930);
    DeRef(_file_full_6932);
    _file_full_6932 = _file_name_6930;
L9: 

    /** 	if std_slash != 0 then*/
    if (_std_slash_6925 == 0)
    goto LA; // [265] 333

    /** 		if std_slash < 0 then*/
    if (_std_slash_6925 >= 0)
    goto LB; // [271] 309

    /** 			std_slash = SLASH*/
    _std_slash_6925 = 92;

    /** 			ifdef UNIX then*/

    /** 			sequence from_slash = "/"*/
    RefDS(_3489);
    DeRefi(_from_slash_6974);
    _from_slash_6974 = _3489;

    /** 			dir_name = search:match_replace(from_slash, dir_name, std_slash)*/
    RefDS(_from_slash_6974);
    RefDS(_dir_name_6929);
    _0 = _dir_name_6929;
    _dir_name_6929 = _5match_replace(_from_slash_6974, _dir_name_6929, 92, 0);
    DeRefDS(_0);
    DeRefDSi(_from_slash_6974);
    _from_slash_6974 = NOVALUE;
    goto LC; // [306] 332
LB: 

    /** 			dir_name = search:match_replace("\\", dir_name, std_slash)*/
    RefDS(_3723);
    RefDS(_dir_name_6929);
    _0 = _dir_name_6929;
    _dir_name_6929 = _5match_replace(_3723, _dir_name_6929, _std_slash_6925, 0);
    DeRefDS(_0);

    /** 			dir_name = search:match_replace("/", dir_name, std_slash)*/
    RefDS(_3489);
    RefDS(_dir_name_6929);
    _0 = _dir_name_6929;
    _dir_name_6929 = _5match_replace(_3489, _dir_name_6929, _std_slash_6925, 0);
    DeRefDS(_0);
LC: 
LA: 

    /** 	return {dir_name, file_full, file_name, file_ext, drive_id}*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_dir_name_6929);
    *((int *)(_2+4)) = _dir_name_6929;
    RefDS(_file_full_6932);
    *((int *)(_2+8)) = _file_full_6932;
    RefDS(_file_name_6930);
    *((int *)(_2+12)) = _file_name_6930;
    RefDS(_file_ext_6931);
    *((int *)(_2+16)) = _file_ext_6931;
    RefDS(_drive_id_6933);
    *((int *)(_2+20)) = _drive_id_6933;
    _3727 = MAKE_SEQ(_1);
    DeRefDS(_path_6924);
    DeRefDS(_dir_name_6929);
    DeRefDS(_file_name_6930);
    DeRefDS(_file_ext_6931);
    DeRefDS(_file_full_6932);
    DeRefDS(_drive_id_6933);
    DeRef(_3696);
    _3696 = NOVALUE;
    DeRef(_3701);
    _3701 = NOVALUE;
    DeRef(_3705);
    _3705 = NOVALUE;
    DeRef(_3707);
    _3707 = NOVALUE;
    DeRef(_3711);
    _3711 = NOVALUE;
    DeRef(_3712);
    _3712 = NOVALUE;
    DeRef(_3714);
    _3714 = NOVALUE;
    DeRef(_3718);
    _3718 = NOVALUE;
    return _3727;
    ;
}


int _8dirname(int _path_6982, int _pcd_6983)
{
    int _data_6984 = NOVALUE;
    int _3732 = NOVALUE;
    int _3730 = NOVALUE;
    int _3729 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pcd_6983)) {
        _1 = (long)(DBL_PTR(_pcd_6983)->dbl);
        if (UNIQUE(DBL_PTR(_pcd_6983)) && (DBL_PTR(_pcd_6983)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pcd_6983);
        _pcd_6983 = _1;
    }

    /** 	data = pathinfo(path)*/
    RefDS(_path_6982);
    _0 = _data_6984;
    _data_6984 = _8pathinfo(_path_6982, 0);
    DeRef(_0);

    /** 	if pcd then*/
    if (_pcd_6983 == 0)
    {
        goto L1; // [18] 42
    }
    else{
    }

    /** 		if length(data[1]) = 0 then*/
    _2 = (int)SEQ_PTR(_data_6984);
    _3729 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_3729)){
            _3730 = SEQ_PTR(_3729)->length;
    }
    else {
        _3730 = 1;
    }
    _3729 = NOVALUE;
    if (_3730 != 0)
    goto L2; // [30] 41

    /** 			return "."*/
    RefDS(_3512);
    DeRefDS(_path_6982);
    DeRefDS(_data_6984);
    _3729 = NOVALUE;
    return _3512;
L2: 
L1: 

    /** 	return data[1]*/
    _2 = (int)SEQ_PTR(_data_6984);
    _3732 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3732);
    DeRefDS(_path_6982);
    DeRefDS(_data_6984);
    _3729 = NOVALUE;
    return _3732;
    ;
}


int _8pathname(int _path_6994)
{
    int _data_6995 = NOVALUE;
    int _stop_6996 = NOVALUE;
    int _3737 = NOVALUE;
    int _3736 = NOVALUE;
    int _3734 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = canonical_path(path)*/
    RefDS(_path_6994);
    _0 = _data_6995;
    _data_6995 = _8canonical_path(_path_6994, 0, 0);
    DeRef(_0);

    /** 	stop = search:rfind(SLASH, data)*/
    if (IS_SEQUENCE(_data_6995)){
            _3734 = SEQ_PTR(_data_6995)->length;
    }
    else {
        _3734 = 1;
    }
    RefDS(_data_6995);
    _stop_6996 = _5rfind(92, _data_6995, _3734);
    _3734 = NOVALUE;
    if (!IS_ATOM_INT(_stop_6996)) {
        _1 = (long)(DBL_PTR(_stop_6996)->dbl);
        if (UNIQUE(DBL_PTR(_stop_6996)) && (DBL_PTR(_stop_6996)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stop_6996);
        _stop_6996 = _1;
    }

    /** 	return data[1 .. stop - 1]*/
    _3736 = _stop_6996 - 1;
    rhs_slice_target = (object_ptr)&_3737;
    RHS_Slice(_data_6995, 1, _3736);
    DeRefDS(_path_6994);
    DeRefDS(_data_6995);
    _3736 = NOVALUE;
    return _3737;
    ;
}


int _8filename(int _path_7005)
{
    int _data_7006 = NOVALUE;
    int _3739 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7005);
    _0 = _data_7006;
    _data_7006 = _8pathinfo(_path_7005, 0);
    DeRef(_0);

    /** 	return data[2]*/
    _2 = (int)SEQ_PTR(_data_7006);
    _3739 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_3739);
    DeRefDS(_path_7005);
    DeRefDS(_data_7006);
    return _3739;
    ;
}


int _8filebase(int _path_7011)
{
    int _data_7012 = NOVALUE;
    int _3741 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7011);
    _0 = _data_7012;
    _data_7012 = _8pathinfo(_path_7011, 0);
    DeRef(_0);

    /** 	return data[3]*/
    _2 = (int)SEQ_PTR(_data_7012);
    _3741 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_3741);
    DeRefDS(_path_7011);
    DeRefDS(_data_7012);
    return _3741;
    ;
}


int _8fileext(int _path_7017)
{
    int _data_7018 = NOVALUE;
    int _3743 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7017);
    _0 = _data_7018;
    _data_7018 = _8pathinfo(_path_7017, 0);
    DeRef(_0);

    /** 	return data[4]*/
    _2 = (int)SEQ_PTR(_data_7018);
    _3743 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_3743);
    DeRefDS(_path_7017);
    DeRefDS(_data_7018);
    return _3743;
    ;
}


int _8driveid(int _path_7023)
{
    int _data_7024 = NOVALUE;
    int _3745 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_7023);
    _0 = _data_7024;
    _data_7024 = _8pathinfo(_path_7023, 0);
    DeRef(_0);

    /** 	return data[5]*/
    _2 = (int)SEQ_PTR(_data_7024);
    _3745 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_3745);
    DeRefDS(_path_7023);
    DeRefDS(_data_7024);
    return _3745;
    ;
}


int _8defaultext(int _path_7029, int _defext_7030)
{
    int _3758 = NOVALUE;
    int _3755 = NOVALUE;
    int _3753 = NOVALUE;
    int _3752 = NOVALUE;
    int _3751 = NOVALUE;
    int _3749 = NOVALUE;
    int _3748 = NOVALUE;
    int _3746 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(defext) = 0 then*/
    if (IS_SEQUENCE(_defext_7030)){
            _3746 = SEQ_PTR(_defext_7030)->length;
    }
    else {
        _3746 = 1;
    }
    if (_3746 != 0)
    goto L1; // [10] 21

    /** 		return path*/
    DeRefDS(_defext_7030);
    return _path_7029;
L1: 

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_7029)){
            _3748 = SEQ_PTR(_path_7029)->length;
    }
    else {
        _3748 = 1;
    }
    {
        int _i_7035;
        _i_7035 = _3748;
L2: 
        if (_i_7035 < 1){
            goto L3; // [26] 95
        }

        /** 		if path[i] = '.' then*/
        _2 = (int)SEQ_PTR(_path_7029);
        _3749 = (int)*(((s1_ptr)_2)->base + _i_7035);
        if (binary_op_a(NOTEQ, _3749, 46)){
            _3749 = NOVALUE;
            goto L4; // [39] 50
        }
        _3749 = NOVALUE;

        /** 			return path*/
        DeRefDS(_defext_7030);
        return _path_7029;
L4: 

        /** 		if find(path[i], SLASHES) then*/
        _2 = (int)SEQ_PTR(_path_7029);
        _3751 = (int)*(((s1_ptr)_2)->base + _i_7035);
        _3752 = find_from(_3751, _8SLASHES_6543, 1);
        _3751 = NOVALUE;
        if (_3752 == 0)
        {
            _3752 = NOVALUE;
            goto L5; // [61] 88
        }
        else{
            _3752 = NOVALUE;
        }

        /** 			if i = length(path) then*/
        if (IS_SEQUENCE(_path_7029)){
                _3753 = SEQ_PTR(_path_7029)->length;
        }
        else {
            _3753 = 1;
        }
        if (_i_7035 != _3753)
        goto L3; // [69] 95

        /** 				return path*/
        DeRefDS(_defext_7030);
        return _path_7029;
        goto L6; // [79] 87

        /** 				exit*/
        goto L3; // [84] 95
L6: 
L5: 

        /** 	end for*/
        _i_7035 = _i_7035 + -1;
        goto L2; // [90] 33
L3: 
        ;
    }

    /** 	if defext[1] != '.' then*/
    _2 = (int)SEQ_PTR(_defext_7030);
    _3755 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _3755, 46)){
        _3755 = NOVALUE;
        goto L7; // [101] 112
    }
    _3755 = NOVALUE;

    /** 		path &= '.'*/
    Append(&_path_7029, _path_7029, 46);
L7: 

    /** 	return path & defext*/
    Concat((object_ptr)&_3758, _path_7029, _defext_7030);
    DeRefDS(_path_7029);
    DeRefDS(_defext_7030);
    return _3758;
    ;
}


int _8absolute_path(int _filename_7054)
{
    int _3770 = NOVALUE;
    int _3769 = NOVALUE;
    int _3767 = NOVALUE;
    int _3765 = NOVALUE;
    int _3763 = NOVALUE;
    int _3762 = NOVALUE;
    int _3761 = NOVALUE;
    int _3759 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(filename) = 0 then*/
    if (IS_SEQUENCE(_filename_7054)){
            _3759 = SEQ_PTR(_filename_7054)->length;
    }
    else {
        _3759 = 1;
    }
    if (_3759 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRefDS(_filename_7054);
    return 0;
L1: 

    /** 	if eu:find(filename[1], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_7054);
    _3761 = (int)*(((s1_ptr)_2)->base + 1);
    _3762 = find_from(_3761, _8SLASHES_6543, 1);
    _3761 = NOVALUE;
    if (_3762 == 0)
    {
        _3762 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _3762 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_filename_7054);
    return 1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 1 then*/
    if (IS_SEQUENCE(_filename_7054)){
            _3763 = SEQ_PTR(_filename_7054)->length;
    }
    else {
        _3763 = 1;
    }
    if (_3763 != 1)
    goto L3; // [47] 58

    /** 			return 0*/
    DeRefDS(_filename_7054);
    return 0;
L3: 

    /** 		if filename[2] != ':' then*/
    _2 = (int)SEQ_PTR(_filename_7054);
    _3765 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(EQUALS, _3765, 58)){
        _3765 = NOVALUE;
        goto L4; // [64] 75
    }
    _3765 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_filename_7054);
    return 0;
L4: 

    /** 		if length(filename) < 3 then*/
    if (IS_SEQUENCE(_filename_7054)){
            _3767 = SEQ_PTR(_filename_7054)->length;
    }
    else {
        _3767 = 1;
    }
    if (_3767 >= 3)
    goto L5; // [80] 91

    /** 			return 0*/
    DeRefDS(_filename_7054);
    return 0;
L5: 

    /** 		if eu:find(filename[3], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_7054);
    _3769 = (int)*(((s1_ptr)_2)->base + 3);
    _3770 = find_from(_3769, _8SLASHES_6543, 1);
    _3769 = NOVALUE;
    if (_3770 == 0)
    {
        _3770 = NOVALUE;
        goto L6; // [102] 112
    }
    else{
        _3770 = NOVALUE;
    }

    /** 			return 1*/
    DeRefDS(_filename_7054);
    return 1;
L6: 

    /** 	return 0*/
    DeRefDS(_filename_7054);
    return 0;
    ;
}


int _8case_flagset_type(int _x_7086)
{
    int _3779 = NOVALUE;
    int _3778 = NOVALUE;
    int _3777 = NOVALUE;
    int _3776 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_7086)) {
        _1 = (long)(DBL_PTR(_x_7086)->dbl);
        if (UNIQUE(DBL_PTR(_x_7086)) && (DBL_PTR(_x_7086)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_7086);
        _x_7086 = _1;
    }

    /** 	return x >= AS_IS and x < 2*TO_SHORT*/
    _3776 = (_x_7086 >= 0);
    _3777 = 8;
    _3778 = (_x_7086 < 8);
    _3777 = NOVALUE;
    _3779 = (_3776 != 0 && _3778 != 0);
    _3776 = NOVALUE;
    _3778 = NOVALUE;
    return _3779;
    ;
}


int _8canonical_path(int _path_in_7093, int _directory_given_7094, int _case_flags_7095)
{
    int _lPath_7096 = NOVALUE;
    int _lPosA_7097 = NOVALUE;
    int _lPosB_7098 = NOVALUE;
    int _lLevel_7099 = NOVALUE;
    int _lHome_7100 = NOVALUE;
    int _lDrive_7101 = NOVALUE;
    int _current_dir_inlined_current_dir_at_306_7161 = NOVALUE;
    int _driveid_inlined_driveid_at_313_7164 = NOVALUE;
    int _data_inlined_driveid_at_313_7163 = NOVALUE;
    int _wildcard_suffix_7166 = NOVALUE;
    int _first_wildcard_at_7167 = NOVALUE;
    int _last_slash_7170 = NOVALUE;
    int _sl_7242 = NOVALUE;
    int _short_name_7245 = NOVALUE;
    int _correct_name_7248 = NOVALUE;
    int _lower_name_7251 = NOVALUE;
    int _part_7267 = NOVALUE;
    int _list_7272 = NOVALUE;
    int _dir_inlined_dir_at_929_7276 = NOVALUE;
    int _name_inlined_dir_at_926_7275 = NOVALUE;
    int _supplied_name_7277 = NOVALUE;
    int _read_name_7296 = NOVALUE;
    int _read_name_7321 = NOVALUE;
    int _3992 = NOVALUE;
    int _3989 = NOVALUE;
    int _3985 = NOVALUE;
    int _3984 = NOVALUE;
    int _3982 = NOVALUE;
    int _3981 = NOVALUE;
    int _3980 = NOVALUE;
    int _3979 = NOVALUE;
    int _3978 = NOVALUE;
    int _3976 = NOVALUE;
    int _3975 = NOVALUE;
    int _3974 = NOVALUE;
    int _3973 = NOVALUE;
    int _3972 = NOVALUE;
    int _3971 = NOVALUE;
    int _3970 = NOVALUE;
    int _3969 = NOVALUE;
    int _3967 = NOVALUE;
    int _3966 = NOVALUE;
    int _3965 = NOVALUE;
    int _3964 = NOVALUE;
    int _3963 = NOVALUE;
    int _3962 = NOVALUE;
    int _3961 = NOVALUE;
    int _3960 = NOVALUE;
    int _3959 = NOVALUE;
    int _3957 = NOVALUE;
    int _3956 = NOVALUE;
    int _3955 = NOVALUE;
    int _3954 = NOVALUE;
    int _3953 = NOVALUE;
    int _3952 = NOVALUE;
    int _3951 = NOVALUE;
    int _3950 = NOVALUE;
    int _3949 = NOVALUE;
    int _3948 = NOVALUE;
    int _3947 = NOVALUE;
    int _3946 = NOVALUE;
    int _3945 = NOVALUE;
    int _3944 = NOVALUE;
    int _3943 = NOVALUE;
    int _3941 = NOVALUE;
    int _3940 = NOVALUE;
    int _3939 = NOVALUE;
    int _3938 = NOVALUE;
    int _3937 = NOVALUE;
    int _3935 = NOVALUE;
    int _3934 = NOVALUE;
    int _3933 = NOVALUE;
    int _3932 = NOVALUE;
    int _3931 = NOVALUE;
    int _3930 = NOVALUE;
    int _3929 = NOVALUE;
    int _3928 = NOVALUE;
    int _3927 = NOVALUE;
    int _3926 = NOVALUE;
    int _3925 = NOVALUE;
    int _3924 = NOVALUE;
    int _3923 = NOVALUE;
    int _3921 = NOVALUE;
    int _3920 = NOVALUE;
    int _3918 = NOVALUE;
    int _3917 = NOVALUE;
    int _3916 = NOVALUE;
    int _3915 = NOVALUE;
    int _3914 = NOVALUE;
    int _3912 = NOVALUE;
    int _3911 = NOVALUE;
    int _3910 = NOVALUE;
    int _3909 = NOVALUE;
    int _3908 = NOVALUE;
    int _3907 = NOVALUE;
    int _3905 = NOVALUE;
    int _3904 = NOVALUE;
    int _3903 = NOVALUE;
    int _3901 = NOVALUE;
    int _3900 = NOVALUE;
    int _3898 = NOVALUE;
    int _3897 = NOVALUE;
    int _3896 = NOVALUE;
    int _3894 = NOVALUE;
    int _3893 = NOVALUE;
    int _3891 = NOVALUE;
    int _3889 = NOVALUE;
    int _3887 = NOVALUE;
    int _3880 = NOVALUE;
    int _3877 = NOVALUE;
    int _3876 = NOVALUE;
    int _3875 = NOVALUE;
    int _3874 = NOVALUE;
    int _3868 = NOVALUE;
    int _3864 = NOVALUE;
    int _3863 = NOVALUE;
    int _3862 = NOVALUE;
    int _3861 = NOVALUE;
    int _3860 = NOVALUE;
    int _3858 = NOVALUE;
    int _3855 = NOVALUE;
    int _3854 = NOVALUE;
    int _3853 = NOVALUE;
    int _3852 = NOVALUE;
    int _3851 = NOVALUE;
    int _3850 = NOVALUE;
    int _3848 = NOVALUE;
    int _3847 = NOVALUE;
    int _3845 = NOVALUE;
    int _3843 = NOVALUE;
    int _3842 = NOVALUE;
    int _3841 = NOVALUE;
    int _3839 = NOVALUE;
    int _3838 = NOVALUE;
    int _3837 = NOVALUE;
    int _3836 = NOVALUE;
    int _3834 = NOVALUE;
    int _3832 = NOVALUE;
    int _3827 = NOVALUE;
    int _3825 = NOVALUE;
    int _3824 = NOVALUE;
    int _3823 = NOVALUE;
    int _3822 = NOVALUE;
    int _3821 = NOVALUE;
    int _3819 = NOVALUE;
    int _3818 = NOVALUE;
    int _3816 = NOVALUE;
    int _3815 = NOVALUE;
    int _3814 = NOVALUE;
    int _3813 = NOVALUE;
    int _3812 = NOVALUE;
    int _3811 = NOVALUE;
    int _3810 = NOVALUE;
    int _3807 = NOVALUE;
    int _3806 = NOVALUE;
    int _3804 = NOVALUE;
    int _3802 = NOVALUE;
    int _3800 = NOVALUE;
    int _3797 = NOVALUE;
    int _3796 = NOVALUE;
    int _3795 = NOVALUE;
    int _3794 = NOVALUE;
    int _3793 = NOVALUE;
    int _3791 = NOVALUE;
    int _3790 = NOVALUE;
    int _3789 = NOVALUE;
    int _3788 = NOVALUE;
    int _3787 = NOVALUE;
    int _3786 = NOVALUE;
    int _3785 = NOVALUE;
    int _3784 = NOVALUE;
    int _3783 = NOVALUE;
    int _3782 = NOVALUE;
    int _3781 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_directory_given_7094)) {
        _1 = (long)(DBL_PTR(_directory_given_7094)->dbl);
        if (UNIQUE(DBL_PTR(_directory_given_7094)) && (DBL_PTR(_directory_given_7094)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_directory_given_7094);
        _directory_given_7094 = _1;
    }
    if (!IS_ATOM_INT(_case_flags_7095)) {
        _1 = (long)(DBL_PTR(_case_flags_7095)->dbl);
        if (UNIQUE(DBL_PTR(_case_flags_7095)) && (DBL_PTR(_case_flags_7095)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_case_flags_7095);
        _case_flags_7095 = _1;
    }

    /**     sequence lPath = ""*/
    RefDS(_5);
    DeRef(_lPath_7096);
    _lPath_7096 = _5;

    /**     integer lPosA = -1*/
    _lPosA_7097 = -1;

    /**     integer lPosB = -1*/
    _lPosB_7098 = -1;

    /**     sequence lLevel = ""*/
    RefDS(_5);
    DeRefi(_lLevel_7099);
    _lLevel_7099 = _5;

    /**     path_in = path_in*/
    RefDS(_path_in_7093);
    DeRefDS(_path_in_7093);
    _path_in_7093 = _path_in_7093;

    /** 	ifdef UNIX then*/

    /** 	    sequence lDrive*/

    /** 	    lPath = match_replace("/", path_in, SLASH)*/
    RefDS(_3489);
    RefDS(_path_in_7093);
    _0 = _lPath_7096;
    _lPath_7096 = _5match_replace(_3489, _path_in_7093, 92, 0);
    DeRefDS(_0);

    /**     if (length(lPath) > 2 and lPath[1] = '"' and lPath[$] = '"') then*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3781 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3781 = 1;
    }
    _3782 = (_3781 > 2);
    _3781 = NOVALUE;
    if (_3782 == 0) {
        _3783 = 0;
        goto L1; // [68] 84
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3784 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3784)) {
        _3785 = (_3784 == 34);
    }
    else {
        _3785 = binary_op(EQUALS, _3784, 34);
    }
    _3784 = NOVALUE;
    if (IS_ATOM_INT(_3785))
    _3783 = (_3785 != 0);
    else
    _3783 = DBL_PTR(_3785)->dbl != 0.0;
L1: 
    if (_3783 == 0) {
        DeRef(_3786);
        _3786 = 0;
        goto L2; // [84] 103
    }
    if (IS_SEQUENCE(_lPath_7096)){
            _3787 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3787 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3788 = (int)*(((s1_ptr)_2)->base + _3787);
    if (IS_ATOM_INT(_3788)) {
        _3789 = (_3788 == 34);
    }
    else {
        _3789 = binary_op(EQUALS, _3788, 34);
    }
    _3788 = NOVALUE;
    if (IS_ATOM_INT(_3789))
    _3786 = (_3789 != 0);
    else
    _3786 = DBL_PTR(_3789)->dbl != 0.0;
L2: 
    if (_3786 == 0)
    {
        _3786 = NOVALUE;
        goto L3; // [103] 121
    }
    else{
        _3786 = NOVALUE;
    }

    /**         lPath = lPath[2..$-1]*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3790 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3790 = 1;
    }
    _3791 = _3790 - 1;
    _3790 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lPath_7096;
    RHS_Slice(_lPath_7096, 2, _3791);
L3: 

    /**     if (length(lPath) > 0 and lPath[1] = '~') then*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3793 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3793 = 1;
    }
    _3794 = (_3793 > 0);
    _3793 = NOVALUE;
    if (_3794 == 0) {
        DeRef(_3795);
        _3795 = 0;
        goto L4; // [130] 146
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3796 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3796)) {
        _3797 = (_3796 == 126);
    }
    else {
        _3797 = binary_op(EQUALS, _3796, 126);
    }
    _3796 = NOVALUE;
    if (IS_ATOM_INT(_3797))
    _3795 = (_3797 != 0);
    else
    _3795 = DBL_PTR(_3797)->dbl != 0.0;
L4: 
    if (_3795 == 0)
    {
        _3795 = NOVALUE;
        goto L5; // [146] 255
    }
    else{
        _3795 = NOVALUE;
    }

    /** 		lHome = getenv("HOME")*/
    DeRef(_lHome_7100);
    _lHome_7100 = EGetEnv(_3798);

    /** 		ifdef WINDOWS then*/

    /** 			if atom(lHome) then*/
    _3800 = IS_ATOM(_lHome_7100);
    if (_3800 == 0)
    {
        _3800 = NOVALUE;
        goto L6; // [161] 177
    }
    else{
        _3800 = NOVALUE;
    }

    /** 				lHome = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _3802 = EGetEnv(_3801);
    _3804 = EGetEnv(_3803);
    if (IS_SEQUENCE(_3802) && IS_ATOM(_3804)) {
        Ref(_3804);
        Append(&_lHome_7100, _3802, _3804);
    }
    else if (IS_ATOM(_3802) && IS_SEQUENCE(_3804)) {
        Ref(_3802);
        Prepend(&_lHome_7100, _3804, _3802);
    }
    else {
        Concat((object_ptr)&_lHome_7100, _3802, _3804);
        DeRef(_3802);
        _3802 = NOVALUE;
    }
    DeRef(_3802);
    _3802 = NOVALUE;
    DeRef(_3804);
    _3804 = NOVALUE;
L6: 

    /** 		if lHome[$] != SLASH then*/
    if (IS_SEQUENCE(_lHome_7100)){
            _3806 = SEQ_PTR(_lHome_7100)->length;
    }
    else {
        _3806 = 1;
    }
    _2 = (int)SEQ_PTR(_lHome_7100);
    _3807 = (int)*(((s1_ptr)_2)->base + _3806);
    if (binary_op_a(EQUALS, _3807, 92)){
        _3807 = NOVALUE;
        goto L7; // [186] 197
    }
    _3807 = NOVALUE;

    /** 			lHome &= SLASH*/
    if (IS_SEQUENCE(_lHome_7100) && IS_ATOM(92)) {
        Append(&_lHome_7100, _lHome_7100, 92);
    }
    else if (IS_ATOM(_lHome_7100) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_lHome_7100, _lHome_7100, 92);
    }
L7: 

    /** 		if length(lPath) > 1 and lPath[2] = SLASH then*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3810 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3810 = 1;
    }
    _3811 = (_3810 > 1);
    _3810 = NOVALUE;
    if (_3811 == 0) {
        goto L8; // [206] 239
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3813 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3813)) {
        _3814 = (_3813 == 92);
    }
    else {
        _3814 = binary_op(EQUALS, _3813, 92);
    }
    _3813 = NOVALUE;
    if (_3814 == 0) {
        DeRef(_3814);
        _3814 = NOVALUE;
        goto L8; // [219] 239
    }
    else {
        if (!IS_ATOM_INT(_3814) && DBL_PTR(_3814)->dbl == 0.0){
            DeRef(_3814);
            _3814 = NOVALUE;
            goto L8; // [219] 239
        }
        DeRef(_3814);
        _3814 = NOVALUE;
    }
    DeRef(_3814);
    _3814 = NOVALUE;

    /** 			lPath = lHome & lPath[3 .. $]*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3815 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3815 = 1;
    }
    rhs_slice_target = (object_ptr)&_3816;
    RHS_Slice(_lPath_7096, 3, _3815);
    if (IS_SEQUENCE(_lHome_7100) && IS_ATOM(_3816)) {
    }
    else if (IS_ATOM(_lHome_7100) && IS_SEQUENCE(_3816)) {
        Ref(_lHome_7100);
        Prepend(&_lPath_7096, _3816, _lHome_7100);
    }
    else {
        Concat((object_ptr)&_lPath_7096, _lHome_7100, _3816);
    }
    DeRefDS(_3816);
    _3816 = NOVALUE;
    goto L9; // [236] 254
L8: 

    /** 			lPath = lHome & lPath[2 .. $]*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3818 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3818 = 1;
    }
    rhs_slice_target = (object_ptr)&_3819;
    RHS_Slice(_lPath_7096, 2, _3818);
    if (IS_SEQUENCE(_lHome_7100) && IS_ATOM(_3819)) {
    }
    else if (IS_ATOM(_lHome_7100) && IS_SEQUENCE(_3819)) {
        Ref(_lHome_7100);
        Prepend(&_lPath_7096, _3819, _lHome_7100);
    }
    else {
        Concat((object_ptr)&_lPath_7096, _lHome_7100, _3819);
    }
    DeRefDS(_3819);
    _3819 = NOVALUE;
L9: 
L5: 

    /** 	ifdef WINDOWS then*/

    /** 	    if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3821 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3821 = 1;
    }
    _3822 = (_3821 > 1);
    _3821 = NOVALUE;
    if (_3822 == 0) {
        DeRef(_3823);
        _3823 = 0;
        goto LA; // [266] 282
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3824 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3824)) {
        _3825 = (_3824 == 58);
    }
    else {
        _3825 = binary_op(EQUALS, _3824, 58);
    }
    _3824 = NOVALUE;
    if (IS_ATOM_INT(_3825))
    _3823 = (_3825 != 0);
    else
    _3823 = DBL_PTR(_3825)->dbl != 0.0;
LA: 
    if (_3823 == 0)
    {
        _3823 = NOVALUE;
        goto LB; // [282] 305
    }
    else{
        _3823 = NOVALUE;
    }

    /** 			lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_7101;
    RHS_Slice(_lPath_7096, 1, 2);

    /** 			lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3827 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3827 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_7096;
    RHS_Slice(_lPath_7096, 3, _3827);
    goto LC; // [302] 339
LB: 

    /** 			lDrive = driveid(current_dir()) & ':'*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_current_dir_inlined_current_dir_at_306_7161);
    _current_dir_inlined_current_dir_at_306_7161 = machine(23, 0);

    /** 	data = pathinfo(path)*/
    RefDS(_current_dir_inlined_current_dir_at_306_7161);
    _0 = _data_inlined_driveid_at_313_7163;
    _data_inlined_driveid_at_313_7163 = _8pathinfo(_current_dir_inlined_current_dir_at_306_7161, 0);
    DeRef(_0);

    /** 	return data[5]*/
    DeRef(_driveid_inlined_driveid_at_313_7164);
    _2 = (int)SEQ_PTR(_data_inlined_driveid_at_313_7163);
    _driveid_inlined_driveid_at_313_7164 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_driveid_inlined_driveid_at_313_7164);
    DeRef(_data_inlined_driveid_at_313_7163);
    _data_inlined_driveid_at_313_7163 = NOVALUE;
    if (IS_SEQUENCE(_driveid_inlined_driveid_at_313_7164) && IS_ATOM(58)) {
        Append(&_lDrive_7101, _driveid_inlined_driveid_at_313_7164, 58);
    }
    else if (IS_ATOM(_driveid_inlined_driveid_at_313_7164) && IS_SEQUENCE(58)) {
    }
    else {
        Concat((object_ptr)&_lDrive_7101, _driveid_inlined_driveid_at_313_7164, 58);
    }
LC: 

    /** 	sequence wildcard_suffix*/

    /** 	integer first_wildcard_at = find_first_wildcard( lPath )*/
    RefDS(_lPath_7096);
    _first_wildcard_at_7167 = _8find_first_wildcard(_lPath_7096, 1);
    if (!IS_ATOM_INT(_first_wildcard_at_7167)) {
        _1 = (long)(DBL_PTR(_first_wildcard_at_7167)->dbl);
        if (UNIQUE(DBL_PTR(_first_wildcard_at_7167)) && (DBL_PTR(_first_wildcard_at_7167)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_first_wildcard_at_7167);
        _first_wildcard_at_7167 = _1;
    }

    /** 	if first_wildcard_at then*/
    if (_first_wildcard_at_7167 == 0)
    {
        goto LD; // [354] 417
    }
    else{
    }

    /** 		integer last_slash = search:rfind( SLASH, lPath, first_wildcard_at )*/
    RefDS(_lPath_7096);
    _last_slash_7170 = _5rfind(92, _lPath_7096, _first_wildcard_at_7167);
    if (!IS_ATOM_INT(_last_slash_7170)) {
        _1 = (long)(DBL_PTR(_last_slash_7170)->dbl);
        if (UNIQUE(DBL_PTR(_last_slash_7170)) && (DBL_PTR(_last_slash_7170)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_slash_7170);
        _last_slash_7170 = _1;
    }

    /** 		if last_slash then*/
    if (_last_slash_7170 == 0)
    {
        goto LE; // [371] 397
    }
    else{
    }

    /** 			wildcard_suffix = lPath[last_slash..$]*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3832 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3832 = 1;
    }
    rhs_slice_target = (object_ptr)&_wildcard_suffix_7166;
    RHS_Slice(_lPath_7096, _last_slash_7170, _3832);

    /** 			lPath = remove( lPath, last_slash, length( lPath ) )*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3834 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3834 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7096);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_last_slash_7170)) ? _last_slash_7170 : (long)(DBL_PTR(_last_slash_7170)->dbl);
        int stop = (IS_ATOM_INT(_3834)) ? _3834 : (long)(DBL_PTR(_3834)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7096), start, &_lPath_7096 );
            }
            else Tail(SEQ_PTR(_lPath_7096), stop+1, &_lPath_7096);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7096), start, &_lPath_7096);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7096 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7096)->ref == 1));
        }
    }
    _3834 = NOVALUE;
    goto LF; // [394] 412
LE: 

    /** 			wildcard_suffix = lPath*/
    RefDS(_lPath_7096);
    DeRef(_wildcard_suffix_7166);
    _wildcard_suffix_7166 = _lPath_7096;

    /** 			lPath = ""*/
    RefDS(_5);
    DeRefDS(_lPath_7096);
    _lPath_7096 = _5;
LF: 
    goto L10; // [414] 425
LD: 

    /** 		wildcard_suffix = ""*/
    RefDS(_5);
    DeRef(_wildcard_suffix_7166);
    _wildcard_suffix_7166 = _5;
L10: 

    /** 	if ((length(lPath) = 0) or not find(lPath[1], "/\\")) then*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3836 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3836 = 1;
    }
    _3837 = (_3836 == 0);
    _3836 = NOVALUE;
    if (_3837 != 0) {
        DeRef(_3838);
        _3838 = 1;
        goto L11; // [434] 454
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3839 = (int)*(((s1_ptr)_2)->base + 1);
    _3841 = find_from(_3839, _3840, 1);
    _3839 = NOVALUE;
    _3842 = (_3841 == 0);
    _3841 = NOVALUE;
    _3838 = (_3842 != 0);
L11: 
    if (_3838 == 0)
    {
        _3838 = NOVALUE;
        goto L12; // [454] 555
    }
    else{
        _3838 = NOVALUE;
    }

    /** 		ifdef UNIX then*/

    /** 			if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_7101)){
            _3843 = SEQ_PTR(_lDrive_7101)->length;
    }
    else {
        _3843 = 1;
    }
    if (_3843 != 0)
    goto L13; // [466] 483

    /** 				lPath = curdir() & lPath*/
    _3845 = _8curdir(0);
    if (IS_SEQUENCE(_3845) && IS_ATOM(_lPath_7096)) {
    }
    else if (IS_ATOM(_3845) && IS_SEQUENCE(_lPath_7096)) {
        Ref(_3845);
        Prepend(&_lPath_7096, _lPath_7096, _3845);
    }
    else {
        Concat((object_ptr)&_lPath_7096, _3845, _lPath_7096);
        DeRef(_3845);
        _3845 = NOVALUE;
    }
    DeRef(_3845);
    _3845 = NOVALUE;
    goto L14; // [480] 498
L13: 

    /** 				lPath = curdir(lDrive[1]) & lPath*/
    _2 = (int)SEQ_PTR(_lDrive_7101);
    _3847 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3847);
    _3848 = _8curdir(_3847);
    _3847 = NOVALUE;
    if (IS_SEQUENCE(_3848) && IS_ATOM(_lPath_7096)) {
    }
    else if (IS_ATOM(_3848) && IS_SEQUENCE(_lPath_7096)) {
        Ref(_3848);
        Prepend(&_lPath_7096, _lPath_7096, _3848);
    }
    else {
        Concat((object_ptr)&_lPath_7096, _3848, _lPath_7096);
        DeRef(_3848);
        _3848 = NOVALUE;
    }
    DeRef(_3848);
    _3848 = NOVALUE;
L14: 

    /** 			if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3850 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3850 = 1;
    }
    _3851 = (_3850 > 1);
    _3850 = NOVALUE;
    if (_3851 == 0) {
        DeRef(_3852);
        _3852 = 0;
        goto L15; // [507] 523
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3853 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3853)) {
        _3854 = (_3853 == 58);
    }
    else {
        _3854 = binary_op(EQUALS, _3853, 58);
    }
    _3853 = NOVALUE;
    if (IS_ATOM_INT(_3854))
    _3852 = (_3854 != 0);
    else
    _3852 = DBL_PTR(_3854)->dbl != 0.0;
L15: 
    if (_3852 == 0)
    {
        _3852 = NOVALUE;
        goto L16; // [523] 554
    }
    else{
        _3852 = NOVALUE;
    }

    /** 				if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_7101)){
            _3855 = SEQ_PTR(_lDrive_7101)->length;
    }
    else {
        _3855 = 1;
    }
    if (_3855 != 0)
    goto L17; // [531] 543

    /** 					lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_7101;
    RHS_Slice(_lPath_7096, 1, 2);
L17: 

    /** 				lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3858 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3858 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_7096;
    RHS_Slice(_lPath_7096, 3, _3858);
L16: 
L12: 

    /** 	if ((directory_given != 0) and (lPath[$] != SLASH) ) then*/
    _3860 = (_directory_given_7094 != 0);
    if (_3860 == 0) {
        DeRef(_3861);
        _3861 = 0;
        goto L18; // [561] 580
    }
    if (IS_SEQUENCE(_lPath_7096)){
            _3862 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3862 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3863 = (int)*(((s1_ptr)_2)->base + _3862);
    if (IS_ATOM_INT(_3863)) {
        _3864 = (_3863 != 92);
    }
    else {
        _3864 = binary_op(NOTEQ, _3863, 92);
    }
    _3863 = NOVALUE;
    if (IS_ATOM_INT(_3864))
    _3861 = (_3864 != 0);
    else
    _3861 = DBL_PTR(_3864)->dbl != 0.0;
L18: 
    if (_3861 == 0)
    {
        _3861 = NOVALUE;
        goto L19; // [580] 590
    }
    else{
        _3861 = NOVALUE;
    }

    /** 		lPath &= SLASH*/
    Append(&_lPath_7096, _lPath_7096, 92);
L19: 

    /** 	lLevel = SLASH & '.' & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = 46;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_7099, concat_list, 3);
    }

    /** 	lPosA = 1*/
    _lPosA_7097 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1A; // [607] 628
L1B: 
    if (_lPosA_7097 == 0)
    goto L1C; // [610] 642

    /** 		lPath = eu:remove(lPath, lPosA, lPosA + 1)*/
    _3868 = _lPosA_7097 + 1;
    if (_3868 > MAXINT){
        _3868 = NewDouble((double)_3868);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7096);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosA_7097)) ? _lPosA_7097 : (long)(DBL_PTR(_lPosA_7097)->dbl);
        int stop = (IS_ATOM_INT(_3868)) ? _3868 : (long)(DBL_PTR(_3868)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7096), start, &_lPath_7096 );
            }
            else Tail(SEQ_PTR(_lPath_7096), stop+1, &_lPath_7096);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7096), start, &_lPath_7096);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7096 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7096)->ref == 1));
        }
    }
    DeRef(_3868);
    _3868 = NOVALUE;

    /** 	  entry*/
L1A: 

    /** 		lPosA = match(lLevel, lPath, lPosA )*/
    _lPosA_7097 = e_match_from(_lLevel_7099, _lPath_7096, _lPosA_7097);

    /** 	end while*/
    goto L1B; // [639] 610
L1C: 

    /** 	lLevel = SLASH & ".." & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = _3543;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_7099, concat_list, 3);
    }

    /** 	lPosB = 1*/
    _lPosB_7098 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1D; // [659] 743
L1E: 
    if (_lPosA_7097 == 0)
    goto L1F; // [662] 757

    /** 		lPosB = lPosA-1*/
    _lPosB_7098 = _lPosA_7097 - 1;

    /** 		while((lPosB > 0) and (lPath[lPosB] != SLASH)) do*/
L20: 
    _3874 = (_lPosB_7098 > 0);
    if (_3874 == 0) {
        DeRef(_3875);
        _3875 = 0;
        goto L21; // [683] 699
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3876 = (int)*(((s1_ptr)_2)->base + _lPosB_7098);
    if (IS_ATOM_INT(_3876)) {
        _3877 = (_3876 != 92);
    }
    else {
        _3877 = binary_op(NOTEQ, _3876, 92);
    }
    _3876 = NOVALUE;
    if (IS_ATOM_INT(_3877))
    _3875 = (_3877 != 0);
    else
    _3875 = DBL_PTR(_3877)->dbl != 0.0;
L21: 
    if (_3875 == 0)
    {
        _3875 = NOVALUE;
        goto L22; // [699] 715
    }
    else{
        _3875 = NOVALUE;
    }

    /** 			lPosB -= 1*/
    _lPosB_7098 = _lPosB_7098 - 1;

    /** 		end while*/
    goto L20; // [712] 679
L22: 

    /** 		if (lPosB <= 0) then*/
    if (_lPosB_7098 > 0)
    goto L23; // [717] 729

    /** 			lPosB = 1*/
    _lPosB_7098 = 1;
L23: 

    /** 		lPath = eu:remove(lPath, lPosB, lPosA + 2)*/
    _3880 = _lPosA_7097 + 2;
    if ((long)((unsigned long)_3880 + (unsigned long)HIGH_BITS) >= 0) 
    _3880 = NewDouble((double)_3880);
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_7096);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosB_7098)) ? _lPosB_7098 : (long)(DBL_PTR(_lPosB_7098)->dbl);
        int stop = (IS_ATOM_INT(_3880)) ? _3880 : (long)(DBL_PTR(_3880)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_7096), start, &_lPath_7096 );
            }
            else Tail(SEQ_PTR(_lPath_7096), stop+1, &_lPath_7096);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_7096), start, &_lPath_7096);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_7096 = Remove_elements(start, stop, (SEQ_PTR(_lPath_7096)->ref == 1));
        }
    }
    DeRef(_3880);
    _3880 = NOVALUE;

    /** 	  entry*/
L1D: 

    /** 		lPosA = match(lLevel, lPath, lPosB )*/
    _lPosA_7097 = e_match_from(_lLevel_7099, _lPath_7096, _lPosB_7098);

    /** 	end while*/
    goto L1E; // [754] 662
L1F: 

    /** 	if case_flags = TO_LOWER then*/
    if (_case_flags_7095 != 1)
    goto L24; // [761] 776

    /** 		lPath = lower( lPath )*/
    RefDS(_lPath_7096);
    _0 = _lPath_7096;
    _lPath_7096 = _6lower(_lPath_7096);
    DeRefDS(_0);
    goto L25; // [773] 1437
L24: 

    /** 	elsif case_flags != AS_IS then*/
    if (_case_flags_7095 == 0)
    goto L26; // [780] 1434

    /** 		sequence sl = find_all(SLASH,lPath) -- split apart lPath*/
    RefDS(_lPath_7096);
    _0 = _sl_7242;
    _sl_7242 = _5find_all(92, _lPath_7096, 1);
    DeRef(_0);

    /** 		integer short_name = and_bits(TO_SHORT,case_flags)=TO_SHORT*/
    {unsigned long tu;
         tu = (unsigned long)4 & (unsigned long)_case_flags_7095;
         _3887 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3887)) {
        _short_name_7245 = (_3887 == 4);
    }
    else {
        _short_name_7245 = (DBL_PTR(_3887)->dbl == (double)4);
    }
    DeRef(_3887);
    _3887 = NOVALUE;

    /** 		integer correct_name = and_bits(case_flags,CORRECT)=CORRECT*/
    {unsigned long tu;
         tu = (unsigned long)_case_flags_7095 & (unsigned long)2;
         _3889 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3889)) {
        _correct_name_7248 = (_3889 == 2);
    }
    else {
        _correct_name_7248 = (DBL_PTR(_3889)->dbl == (double)2);
    }
    DeRef(_3889);
    _3889 = NOVALUE;

    /** 		integer lower_name = and_bits(TO_LOWER,case_flags)=TO_LOWER*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_7095;
         _3891 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3891)) {
        _lower_name_7251 = (_3891 == 1);
    }
    else {
        _lower_name_7251 = (DBL_PTR(_3891)->dbl == (double)1);
    }
    DeRef(_3891);
    _3891 = NOVALUE;

    /** 		if lPath[$] != SLASH then*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3893 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3893 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_7096);
    _3894 = (int)*(((s1_ptr)_2)->base + _3893);
    if (binary_op_a(EQUALS, _3894, 92)){
        _3894 = NOVALUE;
        goto L27; // [857] 879
    }
    _3894 = NOVALUE;

    /** 			sl = sl & {length(lPath)+1}*/
    if (IS_SEQUENCE(_lPath_7096)){
            _3896 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3896 = 1;
    }
    _3897 = _3896 + 1;
    _3896 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3897;
    _3898 = MAKE_SEQ(_1);
    _3897 = NOVALUE;
    Concat((object_ptr)&_sl_7242, _sl_7242, _3898);
    DeRefDS(_3898);
    _3898 = NOVALUE;
L27: 

    /** 		for i = length(sl)-1 to 1 by -1 label "partloop" do*/
    if (IS_SEQUENCE(_sl_7242)){
            _3900 = SEQ_PTR(_sl_7242)->length;
    }
    else {
        _3900 = 1;
    }
    _3901 = _3900 - 1;
    _3900 = NOVALUE;
    {
        int _i_7263;
        _i_7263 = _3901;
L28: 
        if (_i_7263 < 1){
            goto L29; // [888] 1393
        }

        /** 			ifdef WINDOWS then*/

        /** 				sequence part = lDrive & lPath[1..sl[i]-1]*/
        _2 = (int)SEQ_PTR(_sl_7242);
        _3903 = (int)*(((s1_ptr)_2)->base + _i_7263);
        if (IS_ATOM_INT(_3903)) {
            _3904 = _3903 - 1;
        }
        else {
            _3904 = binary_op(MINUS, _3903, 1);
        }
        _3903 = NOVALUE;
        rhs_slice_target = (object_ptr)&_3905;
        RHS_Slice(_lPath_7096, 1, _3904);
        Concat((object_ptr)&_part_7267, _lDrive_7101, _3905);
        DeRefDS(_3905);
        _3905 = NOVALUE;

        /** 			object list = dir( part & SLASH )*/
        Append(&_3907, _part_7267, 92);
        DeRef(_name_inlined_dir_at_926_7275);
        _name_inlined_dir_at_926_7275 = _3907;
        _3907 = NOVALUE;

        /** 	ifdef WINDOWS then*/

        /** 		return machine_func(M_DIR, name)*/
        DeRef(_list_7272);
        _list_7272 = machine(22, _name_inlined_dir_at_926_7275);
        DeRef(_name_inlined_dir_at_926_7275);
        _name_inlined_dir_at_926_7275 = NOVALUE;

        /** 			sequence supplied_name = lPath[sl[i]+1..sl[i+1]-1]*/
        _2 = (int)SEQ_PTR(_sl_7242);
        _3908 = (int)*(((s1_ptr)_2)->base + _i_7263);
        if (IS_ATOM_INT(_3908)) {
            _3909 = _3908 + 1;
            if (_3909 > MAXINT){
                _3909 = NewDouble((double)_3909);
            }
        }
        else
        _3909 = binary_op(PLUS, 1, _3908);
        _3908 = NOVALUE;
        _3910 = _i_7263 + 1;
        _2 = (int)SEQ_PTR(_sl_7242);
        _3911 = (int)*(((s1_ptr)_2)->base + _3910);
        if (IS_ATOM_INT(_3911)) {
            _3912 = _3911 - 1;
        }
        else {
            _3912 = binary_op(MINUS, _3911, 1);
        }
        _3911 = NOVALUE;
        rhs_slice_target = (object_ptr)&_supplied_name_7277;
        RHS_Slice(_lPath_7096, _3909, _3912);

        /** 			if atom(list) then*/
        _3914 = IS_ATOM(_list_7272);
        if (_3914 == 0)
        {
            _3914 = NOVALUE;
            goto L2A; // [974] 1012
        }
        else{
            _3914 = NOVALUE;
        }

        /** 				if lower_name then*/
        if (_lower_name_7251 == 0)
        {
            goto L2B; // [979] 1005
        }
        else{
        }

        /** 					lPath = part & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_7242);
        _3915 = (int)*(((s1_ptr)_2)->base + _i_7263);
        if (IS_SEQUENCE(_lPath_7096)){
                _3916 = SEQ_PTR(_lPath_7096)->length;
        }
        else {
            _3916 = 1;
        }
        rhs_slice_target = (object_ptr)&_3917;
        RHS_Slice(_lPath_7096, _3915, _3916);
        _3918 = _6lower(_3917);
        _3917 = NOVALUE;
        if (IS_SEQUENCE(_part_7267) && IS_ATOM(_3918)) {
            Ref(_3918);
            Append(&_lPath_7096, _part_7267, _3918);
        }
        else if (IS_ATOM(_part_7267) && IS_SEQUENCE(_3918)) {
        }
        else {
            Concat((object_ptr)&_lPath_7096, _part_7267, _3918);
        }
        DeRef(_3918);
        _3918 = NOVALUE;
L2B: 

        /** 				continue*/
        DeRef(_part_7267);
        _part_7267 = NOVALUE;
        DeRef(_list_7272);
        _list_7272 = NOVALUE;
        DeRef(_supplied_name_7277);
        _supplied_name_7277 = NOVALUE;
        goto L2C; // [1009] 1388
L2A: 

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_7272)){
                _3920 = SEQ_PTR(_list_7272)->length;
        }
        else {
            _3920 = 1;
        }
        {
            int _j_7294;
            _j_7294 = 1;
L2D: 
            if (_j_7294 > _3920){
                goto L2E; // [1017] 1148
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_7272);
            _3921 = (int)*(((s1_ptr)_2)->base + _j_7294);
            DeRef(_read_name_7296);
            _2 = (int)SEQ_PTR(_3921);
            _read_name_7296 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_7296);
            _3921 = NOVALUE;

            /** 				if equal(read_name, supplied_name) then*/
            if (_read_name_7296 == _supplied_name_7277)
            _3923 = 1;
            else if (IS_ATOM_INT(_read_name_7296) && IS_ATOM_INT(_supplied_name_7277))
            _3923 = 0;
            else
            _3923 = (compare(_read_name_7296, _supplied_name_7277) == 0);
            if (_3923 == 0)
            {
                _3923 = NOVALUE;
                goto L2F; // [1044] 1139
            }
            else{
                _3923 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_7245 == 0) {
                goto L30; // [1049] 1130
            }
            _2 = (int)SEQ_PTR(_list_7272);
            _3925 = (int)*(((s1_ptr)_2)->base + _j_7294);
            _2 = (int)SEQ_PTR(_3925);
            _3926 = (int)*(((s1_ptr)_2)->base + 11);
            _3925 = NOVALUE;
            _3927 = IS_SEQUENCE(_3926);
            _3926 = NOVALUE;
            if (_3927 == 0)
            {
                _3927 = NOVALUE;
                goto L30; // [1067] 1130
            }
            else{
                _3927 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7242);
            _3928 = (int)*(((s1_ptr)_2)->base + _i_7263);
            rhs_slice_target = (object_ptr)&_3929;
            RHS_Slice(_lPath_7096, 1, _3928);
            _2 = (int)SEQ_PTR(_list_7272);
            _3930 = (int)*(((s1_ptr)_2)->base + _j_7294);
            _2 = (int)SEQ_PTR(_3930);
            _3931 = (int)*(((s1_ptr)_2)->base + 11);
            _3930 = NOVALUE;
            _3932 = _i_7263 + 1;
            _2 = (int)SEQ_PTR(_sl_7242);
            _3933 = (int)*(((s1_ptr)_2)->base + _3932);
            if (IS_SEQUENCE(_lPath_7096)){
                    _3934 = SEQ_PTR(_lPath_7096)->length;
            }
            else {
                _3934 = 1;
            }
            rhs_slice_target = (object_ptr)&_3935;
            RHS_Slice(_lPath_7096, _3933, _3934);
            {
                int concat_list[3];

                concat_list[0] = _3935;
                concat_list[1] = _3931;
                concat_list[2] = _3929;
                Concat_N((object_ptr)&_lPath_7096, concat_list, 3);
            }
            DeRefDS(_3935);
            _3935 = NOVALUE;
            _3931 = NOVALUE;
            DeRefDS(_3929);
            _3929 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_7242)){
                    _3937 = SEQ_PTR(_sl_7242)->length;
            }
            else {
                _3937 = 1;
            }
            if (IS_SEQUENCE(_lPath_7096)){
                    _3938 = SEQ_PTR(_lPath_7096)->length;
            }
            else {
                _3938 = 1;
            }
            _3939 = _3938 + 1;
            _3938 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_7242);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_7242 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _3937);
            _1 = *(int *)_2;
            *(int *)_2 = _3939;
            if( _1 != _3939 ){
                DeRef(_1);
            }
            _3939 = NOVALUE;
L30: 

            /** 					continue "partloop"*/
            DeRef(_read_name_7296);
            _read_name_7296 = NOVALUE;
            DeRef(_part_7267);
            _part_7267 = NOVALUE;
            DeRef(_list_7272);
            _list_7272 = NOVALUE;
            DeRef(_supplied_name_7277);
            _supplied_name_7277 = NOVALUE;
            goto L2C; // [1136] 1388
L2F: 
            DeRef(_read_name_7296);
            _read_name_7296 = NOVALUE;

            /** 			end for*/
            _j_7294 = _j_7294 + 1;
            goto L2D; // [1143] 1024
L2E: 
            ;
        }

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_7272)){
                _3940 = SEQ_PTR(_list_7272)->length;
        }
        else {
            _3940 = 1;
        }
        {
            int _j_7319;
            _j_7319 = 1;
L31: 
            if (_j_7319 > _3940){
                goto L32; // [1153] 1331
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_7272);
            _3941 = (int)*(((s1_ptr)_2)->base + _j_7319);
            DeRef(_read_name_7321);
            _2 = (int)SEQ_PTR(_3941);
            _read_name_7321 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_7321);
            _3941 = NOVALUE;

            /** 				if equal(lower(read_name), lower(supplied_name)) then*/
            RefDS(_read_name_7321);
            _3943 = _6lower(_read_name_7321);
            RefDS(_supplied_name_7277);
            _3944 = _6lower(_supplied_name_7277);
            if (_3943 == _3944)
            _3945 = 1;
            else if (IS_ATOM_INT(_3943) && IS_ATOM_INT(_3944))
            _3945 = 0;
            else
            _3945 = (compare(_3943, _3944) == 0);
            DeRef(_3943);
            _3943 = NOVALUE;
            DeRef(_3944);
            _3944 = NOVALUE;
            if (_3945 == 0)
            {
                _3945 = NOVALUE;
                goto L33; // [1188] 1322
            }
            else{
                _3945 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_7245 == 0) {
                goto L34; // [1193] 1274
            }
            _2 = (int)SEQ_PTR(_list_7272);
            _3947 = (int)*(((s1_ptr)_2)->base + _j_7319);
            _2 = (int)SEQ_PTR(_3947);
            _3948 = (int)*(((s1_ptr)_2)->base + 11);
            _3947 = NOVALUE;
            _3949 = IS_SEQUENCE(_3948);
            _3948 = NOVALUE;
            if (_3949 == 0)
            {
                _3949 = NOVALUE;
                goto L34; // [1211] 1274
            }
            else{
                _3949 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7242);
            _3950 = (int)*(((s1_ptr)_2)->base + _i_7263);
            rhs_slice_target = (object_ptr)&_3951;
            RHS_Slice(_lPath_7096, 1, _3950);
            _2 = (int)SEQ_PTR(_list_7272);
            _3952 = (int)*(((s1_ptr)_2)->base + _j_7319);
            _2 = (int)SEQ_PTR(_3952);
            _3953 = (int)*(((s1_ptr)_2)->base + 11);
            _3952 = NOVALUE;
            _3954 = _i_7263 + 1;
            _2 = (int)SEQ_PTR(_sl_7242);
            _3955 = (int)*(((s1_ptr)_2)->base + _3954);
            if (IS_SEQUENCE(_lPath_7096)){
                    _3956 = SEQ_PTR(_lPath_7096)->length;
            }
            else {
                _3956 = 1;
            }
            rhs_slice_target = (object_ptr)&_3957;
            RHS_Slice(_lPath_7096, _3955, _3956);
            {
                int concat_list[3];

                concat_list[0] = _3957;
                concat_list[1] = _3953;
                concat_list[2] = _3951;
                Concat_N((object_ptr)&_lPath_7096, concat_list, 3);
            }
            DeRefDS(_3957);
            _3957 = NOVALUE;
            _3953 = NOVALUE;
            DeRefDS(_3951);
            _3951 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_7242)){
                    _3959 = SEQ_PTR(_sl_7242)->length;
            }
            else {
                _3959 = 1;
            }
            if (IS_SEQUENCE(_lPath_7096)){
                    _3960 = SEQ_PTR(_lPath_7096)->length;
            }
            else {
                _3960 = 1;
            }
            _3961 = _3960 + 1;
            _3960 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_7242);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_7242 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _3959);
            _1 = *(int *)_2;
            *(int *)_2 = _3961;
            if( _1 != _3961 ){
                DeRef(_1);
            }
            _3961 = NOVALUE;
L34: 

            /** 					if correct_name then*/
            if (_correct_name_7248 == 0)
            {
                goto L35; // [1276] 1313
            }
            else{
            }

            /** 						lPath = lPath[1..sl[i]] & read_name & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_7242);
            _3962 = (int)*(((s1_ptr)_2)->base + _i_7263);
            rhs_slice_target = (object_ptr)&_3963;
            RHS_Slice(_lPath_7096, 1, _3962);
            _3964 = _i_7263 + 1;
            _2 = (int)SEQ_PTR(_sl_7242);
            _3965 = (int)*(((s1_ptr)_2)->base + _3964);
            if (IS_SEQUENCE(_lPath_7096)){
                    _3966 = SEQ_PTR(_lPath_7096)->length;
            }
            else {
                _3966 = 1;
            }
            rhs_slice_target = (object_ptr)&_3967;
            RHS_Slice(_lPath_7096, _3965, _3966);
            {
                int concat_list[3];

                concat_list[0] = _3967;
                concat_list[1] = _read_name_7321;
                concat_list[2] = _3963;
                Concat_N((object_ptr)&_lPath_7096, concat_list, 3);
            }
            DeRefDS(_3967);
            _3967 = NOVALUE;
            DeRefDS(_3963);
            _3963 = NOVALUE;
L35: 

            /** 					continue "partloop"*/
            DeRef(_read_name_7321);
            _read_name_7321 = NOVALUE;
            DeRef(_part_7267);
            _part_7267 = NOVALUE;
            DeRef(_list_7272);
            _list_7272 = NOVALUE;
            DeRef(_supplied_name_7277);
            _supplied_name_7277 = NOVALUE;
            goto L2C; // [1319] 1388
L33: 
            DeRef(_read_name_7321);
            _read_name_7321 = NOVALUE;

            /** 			end for*/
            _j_7319 = _j_7319 + 1;
            goto L31; // [1326] 1160
L32: 
            ;
        }

        /** 			if and_bits(TO_LOWER,case_flags) then*/
        {unsigned long tu;
             tu = (unsigned long)1 & (unsigned long)_case_flags_7095;
             _3969 = MAKE_UINT(tu);
        }
        if (_3969 == 0) {
            DeRef(_3969);
            _3969 = NOVALUE;
            goto L36; // [1339] 1378
        }
        else {
            if (!IS_ATOM_INT(_3969) && DBL_PTR(_3969)->dbl == 0.0){
                DeRef(_3969);
                _3969 = NOVALUE;
                goto L36; // [1339] 1378
            }
            DeRef(_3969);
            _3969 = NOVALUE;
        }
        DeRef(_3969);
        _3969 = NOVALUE;

        /** 				lPath = lPath[1..sl[i]-1] & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_7242);
        _3970 = (int)*(((s1_ptr)_2)->base + _i_7263);
        if (IS_ATOM_INT(_3970)) {
            _3971 = _3970 - 1;
        }
        else {
            _3971 = binary_op(MINUS, _3970, 1);
        }
        _3970 = NOVALUE;
        rhs_slice_target = (object_ptr)&_3972;
        RHS_Slice(_lPath_7096, 1, _3971);
        _2 = (int)SEQ_PTR(_sl_7242);
        _3973 = (int)*(((s1_ptr)_2)->base + _i_7263);
        if (IS_SEQUENCE(_lPath_7096)){
                _3974 = SEQ_PTR(_lPath_7096)->length;
        }
        else {
            _3974 = 1;
        }
        rhs_slice_target = (object_ptr)&_3975;
        RHS_Slice(_lPath_7096, _3973, _3974);
        _3976 = _6lower(_3975);
        _3975 = NOVALUE;
        if (IS_SEQUENCE(_3972) && IS_ATOM(_3976)) {
            Ref(_3976);
            Append(&_lPath_7096, _3972, _3976);
        }
        else if (IS_ATOM(_3972) && IS_SEQUENCE(_3976)) {
        }
        else {
            Concat((object_ptr)&_lPath_7096, _3972, _3976);
            DeRefDS(_3972);
            _3972 = NOVALUE;
        }
        DeRef(_3972);
        _3972 = NOVALUE;
        DeRef(_3976);
        _3976 = NOVALUE;
L36: 

        /** 			exit*/
        DeRef(_part_7267);
        _part_7267 = NOVALUE;
        DeRef(_list_7272);
        _list_7272 = NOVALUE;
        DeRef(_supplied_name_7277);
        _supplied_name_7277 = NOVALUE;
        goto L29; // [1382] 1393

        /** 		end for*/
L2C: 
        _i_7263 = _i_7263 + -1;
        goto L28; // [1388] 895
L29: 
        ;
    }

    /** 		if and_bits(case_flags,or_bits(CORRECT,TO_LOWER))=TO_LOWER and length(lPath) then*/
    {unsigned long tu;
         tu = (unsigned long)2 | (unsigned long)1;
         _3978 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3978)) {
        {unsigned long tu;
             tu = (unsigned long)_case_flags_7095 & (unsigned long)_3978;
             _3979 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_case_flags_7095;
        _3979 = Dand_bits(&temp_d, DBL_PTR(_3978));
    }
    DeRef(_3978);
    _3978 = NOVALUE;
    if (IS_ATOM_INT(_3979)) {
        _3980 = (_3979 == 1);
    }
    else {
        _3980 = (DBL_PTR(_3979)->dbl == (double)1);
    }
    DeRef(_3979);
    _3979 = NOVALUE;
    if (_3980 == 0) {
        goto L37; // [1413] 1433
    }
    if (IS_SEQUENCE(_lPath_7096)){
            _3982 = SEQ_PTR(_lPath_7096)->length;
    }
    else {
        _3982 = 1;
    }
    if (_3982 == 0)
    {
        _3982 = NOVALUE;
        goto L37; // [1421] 1433
    }
    else{
        _3982 = NOVALUE;
    }

    /** 			lPath = lower(lPath)*/
    RefDS(_lPath_7096);
    _0 = _lPath_7096;
    _lPath_7096 = _6lower(_lPath_7096);
    DeRefDS(_0);
L37: 
L26: 
    DeRef(_sl_7242);
    _sl_7242 = NOVALUE;
L25: 

    /** 	ifdef WINDOWS then*/

    /** 		if and_bits(CORRECT,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)2 & (unsigned long)_case_flags_7095;
         _3984 = MAKE_UINT(tu);
    }
    if (_3984 == 0) {
        DeRef(_3984);
        _3984 = NOVALUE;
        goto L38; // [1447] 1489
    }
    else {
        if (!IS_ATOM_INT(_3984) && DBL_PTR(_3984)->dbl == 0.0){
            DeRef(_3984);
            _3984 = NOVALUE;
            goto L38; // [1447] 1489
        }
        DeRef(_3984);
        _3984 = NOVALUE;
    }
    DeRef(_3984);
    _3984 = NOVALUE;

    /** 			if or_bits(system_drive_case,'A') = 'a' then*/
    if (IS_ATOM_INT(_8system_drive_case_7079)) {
        {unsigned long tu;
             tu = (unsigned long)_8system_drive_case_7079 | (unsigned long)65;
             _3985 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)65;
        _3985 = Dor_bits(DBL_PTR(_8system_drive_case_7079), &temp_d);
    }
    if (binary_op_a(NOTEQ, _3985, 97)){
        DeRef(_3985);
        _3985 = NOVALUE;
        goto L39; // [1458] 1475
    }
    DeRef(_3985);
    _3985 = NOVALUE;

    /** 				lDrive = lower(lDrive)*/
    RefDS(_lDrive_7101);
    _0 = _lDrive_7101;
    _lDrive_7101 = _6lower(_lDrive_7101);
    DeRefDS(_0);
    goto L3A; // [1472] 1512
L39: 

    /** 				lDrive = upper(lDrive)*/
    RefDS(_lDrive_7101);
    _0 = _lDrive_7101;
    _lDrive_7101 = _6upper(_lDrive_7101);
    DeRefDS(_0);
    goto L3A; // [1486] 1512
L38: 

    /** 		elsif and_bits(TO_LOWER,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_7095;
         _3989 = MAKE_UINT(tu);
    }
    if (_3989 == 0) {
        DeRef(_3989);
        _3989 = NOVALUE;
        goto L3B; // [1497] 1511
    }
    else {
        if (!IS_ATOM_INT(_3989) && DBL_PTR(_3989)->dbl == 0.0){
            DeRef(_3989);
            _3989 = NOVALUE;
            goto L3B; // [1497] 1511
        }
        DeRef(_3989);
        _3989 = NOVALUE;
    }
    DeRef(_3989);
    _3989 = NOVALUE;

    /** 			lDrive = lower(lDrive)*/
    RefDS(_lDrive_7101);
    _0 = _lDrive_7101;
    _lDrive_7101 = _6lower(_lDrive_7101);
    DeRefDS(_0);
L3B: 
L3A: 

    /** 		lPath = lDrive & lPath*/
    Concat((object_ptr)&_lPath_7096, _lDrive_7101, _lPath_7096);

    /** 	return lPath & wildcard_suffix*/
    Concat((object_ptr)&_3992, _lPath_7096, _wildcard_suffix_7166);
    DeRefDS(_path_in_7093);
    DeRefDS(_lPath_7096);
    DeRefi(_lLevel_7099);
    DeRef(_lHome_7100);
    DeRefDS(_lDrive_7101);
    DeRefDS(_wildcard_suffix_7166);
    DeRef(_3785);
    _3785 = NOVALUE;
    _3915 = NOVALUE;
    _3933 = NOVALUE;
    DeRef(_3811);
    _3811 = NOVALUE;
    DeRef(_3822);
    _3822 = NOVALUE;
    DeRef(_3842);
    _3842 = NOVALUE;
    DeRef(_3825);
    _3825 = NOVALUE;
    DeRef(_3909);
    _3909 = NOVALUE;
    DeRef(_3954);
    _3954 = NOVALUE;
    DeRef(_3877);
    _3877 = NOVALUE;
    DeRef(_3964);
    _3964 = NOVALUE;
    DeRef(_3860);
    _3860 = NOVALUE;
    DeRef(_3794);
    _3794 = NOVALUE;
    DeRef(_3932);
    _3932 = NOVALUE;
    _3955 = NOVALUE;
    DeRef(_3797);
    _3797 = NOVALUE;
    DeRef(_3910);
    _3910 = NOVALUE;
    DeRef(_3789);
    _3789 = NOVALUE;
    _3928 = NOVALUE;
    DeRef(_3782);
    _3782 = NOVALUE;
    DeRef(_3854);
    _3854 = NOVALUE;
    _3965 = NOVALUE;
    DeRef(_3971);
    _3971 = NOVALUE;
    DeRef(_3912);
    _3912 = NOVALUE;
    DeRef(_3874);
    _3874 = NOVALUE;
    _3973 = NOVALUE;
    DeRef(_3791);
    _3791 = NOVALUE;
    _3950 = NOVALUE;
    DeRef(_3837);
    _3837 = NOVALUE;
    DeRef(_3980);
    _3980 = NOVALUE;
    DeRef(_3901);
    _3901 = NOVALUE;
    DeRef(_3851);
    _3851 = NOVALUE;
    DeRef(_3904);
    _3904 = NOVALUE;
    _3962 = NOVALUE;
    DeRef(_3864);
    _3864 = NOVALUE;
    return _3992;
    ;
}


int _8abbreviate_path(int _orig_path_7397, int _base_paths_7398)
{
    int _expanded_path_7399 = NOVALUE;
    int _lowered_expanded_path_7409 = NOVALUE;
    int _fs_case_inlined_fs_case_at_253_19386 = NOVALUE;
    int _s_inlined_fs_case_at_250_19385 = NOVALUE;
    int _4038 = NOVALUE;
    int _4037 = NOVALUE;
    int _4034 = NOVALUE;
    int _4033 = NOVALUE;
    int _4032 = NOVALUE;
    int _4031 = NOVALUE;
    int _4030 = NOVALUE;
    int _4028 = NOVALUE;
    int _4027 = NOVALUE;
    int _4026 = NOVALUE;
    int _4025 = NOVALUE;
    int _4024 = NOVALUE;
    int _4023 = NOVALUE;
    int _4022 = NOVALUE;
    int _4021 = NOVALUE;
    int _4020 = NOVALUE;
    int _4017 = NOVALUE;
    int _4016 = NOVALUE;
    int _4014 = NOVALUE;
    int _4013 = NOVALUE;
    int _4012 = NOVALUE;
    int _4011 = NOVALUE;
    int _4010 = NOVALUE;
    int _4009 = NOVALUE;
    int _4008 = NOVALUE;
    int _4007 = NOVALUE;
    int _4006 = NOVALUE;
    int _4005 = NOVALUE;
    int _4004 = NOVALUE;
    int _4003 = NOVALUE;
    int _4002 = NOVALUE;
    int _3999 = NOVALUE;
    int _3998 = NOVALUE;
    int _3997 = NOVALUE;
    int _3995 = NOVALUE;
    int _0, _1, _2;
    

    /** 	expanded_path = canonical_path(orig_path)*/
    RefDS(_orig_path_7397);
    _0 = _expanded_path_7399;
    _expanded_path_7399 = _8canonical_path(_orig_path_7397, 0, 0);
    DeRef(_0);

    /** 	base_paths = append(base_paths, curdir())*/
    _3995 = _8curdir(0);
    Ref(_3995);
    Append(&_base_paths_7398, _base_paths_7398, _3995);
    DeRef(_3995);
    _3995 = NOVALUE;

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_7398)){
            _3997 = SEQ_PTR(_base_paths_7398)->length;
    }
    else {
        _3997 = 1;
    }
    {
        int _i_7404;
        _i_7404 = 1;
L1: 
        if (_i_7404 > _3997){
            goto L2; // [32] 64
        }

        /** 		base_paths[i] = canonical_path(base_paths[i], 1) -- assume each base path is meant to be a directory.*/
        _2 = (int)SEQ_PTR(_base_paths_7398);
        _3998 = (int)*(((s1_ptr)_2)->base + _i_7404);
        Ref(_3998);
        _3999 = _8canonical_path(_3998, 1, 0);
        _3998 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_7398);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _base_paths_7398 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_7404);
        _1 = *(int *)_2;
        *(int *)_2 = _3999;
        if( _1 != _3999 ){
            DeRef(_1);
        }
        _3999 = NOVALUE;

        /** 	end for*/
        _i_7404 = _i_7404 + 1;
        goto L1; // [59] 39
L2: 
        ;
    }

    /** 	base_paths = fs_case(base_paths)*/

    /** 	ifdef WINDOWS then*/

    /** 		return lower(s)*/
    RefDS(_base_paths_7398);
    _0 = _base_paths_7398;
    _base_paths_7398 = _6lower(_base_paths_7398);
    DeRefDS(_0);

    /** 	sequence lowered_expanded_path = fs_case(expanded_path)*/

    /** 	ifdef WINDOWS then*/

    /** 		return lower(s)*/
    RefDS(_expanded_path_7399);
    _0 = _lowered_expanded_path_7409;
    _lowered_expanded_path_7409 = _6lower(_expanded_path_7399);
    DeRef(_0);

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_7398)){
            _4002 = SEQ_PTR(_base_paths_7398)->length;
    }
    else {
        _4002 = 1;
    }
    {
        int _i_7412;
        _i_7412 = 1;
L3: 
        if (_i_7412 > _4002){
            goto L4; // [95] 149
        }

        /** 		if search:begins(base_paths[i], lowered_expanded_path) then*/
        _2 = (int)SEQ_PTR(_base_paths_7398);
        _4003 = (int)*(((s1_ptr)_2)->base + _i_7412);
        Ref(_4003);
        RefDS(_lowered_expanded_path_7409);
        _4004 = _5begins(_4003, _lowered_expanded_path_7409);
        _4003 = NOVALUE;
        if (_4004 == 0) {
            DeRef(_4004);
            _4004 = NOVALUE;
            goto L5; // [113] 142
        }
        else {
            if (!IS_ATOM_INT(_4004) && DBL_PTR(_4004)->dbl == 0.0){
                DeRef(_4004);
                _4004 = NOVALUE;
                goto L5; // [113] 142
            }
            DeRef(_4004);
            _4004 = NOVALUE;
        }
        DeRef(_4004);
        _4004 = NOVALUE;

        /** 			return expanded_path[length(base_paths[i]) + 1 .. $]*/
        _2 = (int)SEQ_PTR(_base_paths_7398);
        _4005 = (int)*(((s1_ptr)_2)->base + _i_7412);
        if (IS_SEQUENCE(_4005)){
                _4006 = SEQ_PTR(_4005)->length;
        }
        else {
            _4006 = 1;
        }
        _4005 = NOVALUE;
        _4007 = _4006 + 1;
        _4006 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_7399)){
                _4008 = SEQ_PTR(_expanded_path_7399)->length;
        }
        else {
            _4008 = 1;
        }
        rhs_slice_target = (object_ptr)&_4009;
        RHS_Slice(_expanded_path_7399, _4007, _4008);
        DeRefDS(_orig_path_7397);
        DeRefDS(_base_paths_7398);
        DeRefDS(_expanded_path_7399);
        DeRefDS(_lowered_expanded_path_7409);
        _4005 = NOVALUE;
        _4007 = NOVALUE;
        return _4009;
L5: 

        /** 	end for*/
        _i_7412 = _i_7412 + 1;
        goto L3; // [144] 102
L4: 
        ;
    }

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(base_paths[$][1], lowered_expanded_path[1]) then*/
    if (IS_SEQUENCE(_base_paths_7398)){
            _4010 = SEQ_PTR(_base_paths_7398)->length;
    }
    else {
        _4010 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_7398);
    _4011 = (int)*(((s1_ptr)_2)->base + _4010);
    _2 = (int)SEQ_PTR(_4011);
    _4012 = (int)*(((s1_ptr)_2)->base + 1);
    _4011 = NOVALUE;
    _2 = (int)SEQ_PTR(_lowered_expanded_path_7409);
    _4013 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4012 == _4013)
    _4014 = 1;
    else if (IS_ATOM_INT(_4012) && IS_ATOM_INT(_4013))
    _4014 = 0;
    else
    _4014 = (compare(_4012, _4013) == 0);
    _4012 = NOVALUE;
    _4013 = NOVALUE;
    if (_4014 != 0)
    goto L6; // [172] 182
    _4014 = NOVALUE;

    /** 			return orig_path*/
    DeRefDS(_base_paths_7398);
    DeRef(_expanded_path_7399);
    DeRefDS(_lowered_expanded_path_7409);
    _4005 = NOVALUE;
    DeRef(_4007);
    _4007 = NOVALUE;
    DeRef(_4009);
    _4009 = NOVALUE;
    return _orig_path_7397;
L6: 

    /** 	base_paths = stdseq:split(base_paths[$], SLASH)*/
    if (IS_SEQUENCE(_base_paths_7398)){
            _4016 = SEQ_PTR(_base_paths_7398)->length;
    }
    else {
        _4016 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_7398);
    _4017 = (int)*(((s1_ptr)_2)->base + _4016);
    Ref(_4017);
    _0 = _base_paths_7398;
    _base_paths_7398 = _20split(_4017, 92, 0, 0);
    DeRefDS(_0);
    _4017 = NOVALUE;

    /** 	expanded_path = stdseq:split(expanded_path, SLASH)*/
    RefDS(_expanded_path_7399);
    _0 = _expanded_path_7399;
    _expanded_path_7399 = _20split(_expanded_path_7399, 92, 0, 0);
    DeRefDS(_0);

    /** 	lowered_expanded_path = ""*/
    RefDS(_5);
    DeRef(_lowered_expanded_path_7409);
    _lowered_expanded_path_7409 = _5;

    /** 	for i = 1 to math:min({length(expanded_path), length(base_paths) - 1}) do*/
    if (IS_SEQUENCE(_expanded_path_7399)){
            _4020 = SEQ_PTR(_expanded_path_7399)->length;
    }
    else {
        _4020 = 1;
    }
    if (IS_SEQUENCE(_base_paths_7398)){
            _4021 = SEQ_PTR(_base_paths_7398)->length;
    }
    else {
        _4021 = 1;
    }
    _4022 = _4021 - 1;
    _4021 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4020;
    ((int *)_2)[2] = _4022;
    _4023 = MAKE_SEQ(_1);
    _4022 = NOVALUE;
    _4020 = NOVALUE;
    _4024 = _17min(_4023);
    _4023 = NOVALUE;
    {
        int _i_7434;
        _i_7434 = 1;
L7: 
        if (binary_op_a(GREATER, _i_7434, _4024)){
            goto L8; // [238] 346
        }

        /** 		if not equal(fs_case(expanded_path[i]), base_paths[i]) then*/
        _2 = (int)SEQ_PTR(_expanded_path_7399);
        if (!IS_ATOM_INT(_i_7434)){
            _4025 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7434)->dbl));
        }
        else{
            _4025 = (int)*(((s1_ptr)_2)->base + _i_7434);
        }
        Ref(_4025);
        DeRef(_s_inlined_fs_case_at_250_19385);
        _s_inlined_fs_case_at_250_19385 = _4025;
        _4025 = NOVALUE;

        /** 	ifdef WINDOWS then*/

        /** 		return lower(s)*/
        Ref(_s_inlined_fs_case_at_250_19385);
        _0 = _fs_case_inlined_fs_case_at_253_19386;
        _fs_case_inlined_fs_case_at_253_19386 = _6lower(_s_inlined_fs_case_at_250_19385);
        DeRef(_0);
        DeRef(_s_inlined_fs_case_at_250_19385);
        _s_inlined_fs_case_at_250_19385 = NOVALUE;
        Ref(_fs_case_inlined_fs_case_at_253_19386);
        DeRef(_4026);
        _4026 = _fs_case_inlined_fs_case_at_253_19386;
        _2 = (int)SEQ_PTR(_base_paths_7398);
        if (!IS_ATOM_INT(_i_7434)){
            _4027 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7434)->dbl));
        }
        else{
            _4027 = (int)*(((s1_ptr)_2)->base + _i_7434);
        }
        if (_4026 == _4027)
        _4028 = 1;
        else if (IS_ATOM_INT(_4026) && IS_ATOM_INT(_4027))
        _4028 = 0;
        else
        _4028 = (compare(_4026, _4027) == 0);
        _4026 = NOVALUE;
        _4027 = NOVALUE;
        if (_4028 != 0)
        goto L9; // [278] 339
        _4028 = NOVALUE;

        /** 			expanded_path = repeat("..", length(base_paths) - i) & expanded_path[i .. $]*/
        if (IS_SEQUENCE(_base_paths_7398)){
                _4030 = SEQ_PTR(_base_paths_7398)->length;
        }
        else {
            _4030 = 1;
        }
        if (IS_ATOM_INT(_i_7434)) {
            _4031 = _4030 - _i_7434;
        }
        else {
            _4031 = NewDouble((double)_4030 - DBL_PTR(_i_7434)->dbl);
        }
        _4030 = NOVALUE;
        _4032 = Repeat(_3543, _4031);
        DeRef(_4031);
        _4031 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_7399)){
                _4033 = SEQ_PTR(_expanded_path_7399)->length;
        }
        else {
            _4033 = 1;
        }
        rhs_slice_target = (object_ptr)&_4034;
        RHS_Slice(_expanded_path_7399, _i_7434, _4033);
        Concat((object_ptr)&_expanded_path_7399, _4032, _4034);
        DeRefDS(_4032);
        _4032 = NOVALUE;
        DeRef(_4032);
        _4032 = NOVALUE;
        DeRefDS(_4034);
        _4034 = NOVALUE;

        /** 			expanded_path = stdseq:join(expanded_path, SLASH)*/
        RefDS(_expanded_path_7399);
        _0 = _expanded_path_7399;
        _expanded_path_7399 = _20join(_expanded_path_7399, 92);
        DeRefDS(_0);

        /** 			if length(expanded_path) < length(orig_path) then*/
        if (IS_SEQUENCE(_expanded_path_7399)){
                _4037 = SEQ_PTR(_expanded_path_7399)->length;
        }
        else {
            _4037 = 1;
        }
        if (IS_SEQUENCE(_orig_path_7397)){
                _4038 = SEQ_PTR(_orig_path_7397)->length;
        }
        else {
            _4038 = 1;
        }
        if (_4037 >= _4038)
        goto L8; // [323] 346

        /** 		  		return expanded_path*/
        DeRef(_i_7434);
        DeRefDS(_orig_path_7397);
        DeRefDS(_base_paths_7398);
        DeRef(_lowered_expanded_path_7409);
        _4005 = NOVALUE;
        DeRef(_4007);
        _4007 = NOVALUE;
        DeRef(_4009);
        _4009 = NOVALUE;
        DeRef(_4024);
        _4024 = NOVALUE;
        return _expanded_path_7399;

        /** 			exit*/
        goto L8; // [336] 346
L9: 

        /** 	end for*/
        _0 = _i_7434;
        if (IS_ATOM_INT(_i_7434)) {
            _i_7434 = _i_7434 + 1;
            if ((long)((unsigned long)_i_7434 +(unsigned long) HIGH_BITS) >= 0){
                _i_7434 = NewDouble((double)_i_7434);
            }
        }
        else {
            _i_7434 = binary_op_a(PLUS, _i_7434, 1);
        }
        DeRef(_0);
        goto L7; // [341] 245
L8: 
        ;
        DeRef(_i_7434);
    }

    /** 	return orig_path*/
    DeRefDS(_base_paths_7398);
    DeRef(_expanded_path_7399);
    DeRef(_lowered_expanded_path_7409);
    _4005 = NOVALUE;
    DeRef(_4007);
    _4007 = NOVALUE;
    DeRef(_4009);
    _4009 = NOVALUE;
    DeRef(_4024);
    _4024 = NOVALUE;
    return _orig_path_7397;
    ;
}


int _8split_path(int _fname_7459)
{
    int _4040 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return stdseq:split(fname, SLASH, 1)*/
    RefDS(_fname_7459);
    _4040 = _20split(_fname_7459, 92, 1, 0);
    DeRefDS(_fname_7459);
    return _4040;
    ;
}


int _8join_path(int _path_elements_7463)
{
    int _fname_7464 = NOVALUE;
    int _elem_7468 = NOVALUE;
    int _4054 = NOVALUE;
    int _4053 = NOVALUE;
    int _4052 = NOVALUE;
    int _4051 = NOVALUE;
    int _4050 = NOVALUE;
    int _4049 = NOVALUE;
    int _4047 = NOVALUE;
    int _4046 = NOVALUE;
    int _4044 = NOVALUE;
    int _4043 = NOVALUE;
    int _4041 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fname = ""*/
    RefDS(_5);
    DeRef(_fname_7464);
    _fname_7464 = _5;

    /** 	for i = 1 to length(path_elements) do*/
    if (IS_SEQUENCE(_path_elements_7463)){
            _4041 = SEQ_PTR(_path_elements_7463)->length;
    }
    else {
        _4041 = 1;
    }
    {
        int _i_7466;
        _i_7466 = 1;
L1: 
        if (_i_7466 > _4041){
            goto L2; // [15] 117
        }

        /** 		sequence elem = path_elements[i]*/
        DeRef(_elem_7468);
        _2 = (int)SEQ_PTR(_path_elements_7463);
        _elem_7468 = (int)*(((s1_ptr)_2)->base + _i_7466);
        Ref(_elem_7468);

        /** 		if elem[$] = SLASH then*/
        if (IS_SEQUENCE(_elem_7468)){
                _4043 = SEQ_PTR(_elem_7468)->length;
        }
        else {
            _4043 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_7468);
        _4044 = (int)*(((s1_ptr)_2)->base + _4043);
        if (binary_op_a(NOTEQ, _4044, 92)){
            _4044 = NOVALUE;
            goto L3; // [39] 58
        }
        _4044 = NOVALUE;

        /** 			elem = elem[1..$ - 1]*/
        if (IS_SEQUENCE(_elem_7468)){
                _4046 = SEQ_PTR(_elem_7468)->length;
        }
        else {
            _4046 = 1;
        }
        _4047 = _4046 - 1;
        _4046 = NOVALUE;
        rhs_slice_target = (object_ptr)&_elem_7468;
        RHS_Slice(_elem_7468, 1, _4047);
L3: 

        /** 		if length(elem) and elem[1] != SLASH then*/
        if (IS_SEQUENCE(_elem_7468)){
                _4049 = SEQ_PTR(_elem_7468)->length;
        }
        else {
            _4049 = 1;
        }
        if (_4049 == 0) {
            goto L4; // [63] 102
        }
        _2 = (int)SEQ_PTR(_elem_7468);
        _4051 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_4051)) {
            _4052 = (_4051 != 92);
        }
        else {
            _4052 = binary_op(NOTEQ, _4051, 92);
        }
        _4051 = NOVALUE;
        if (_4052 == 0) {
            DeRef(_4052);
            _4052 = NOVALUE;
            goto L4; // [76] 102
        }
        else {
            if (!IS_ATOM_INT(_4052) && DBL_PTR(_4052)->dbl == 0.0){
                DeRef(_4052);
                _4052 = NOVALUE;
                goto L4; // [76] 102
            }
            DeRef(_4052);
            _4052 = NOVALUE;
        }
        DeRef(_4052);
        _4052 = NOVALUE;

        /** 			ifdef WINDOWS then*/

        /** 				if elem[$] != ':' then*/
        if (IS_SEQUENCE(_elem_7468)){
                _4053 = SEQ_PTR(_elem_7468)->length;
        }
        else {
            _4053 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_7468);
        _4054 = (int)*(((s1_ptr)_2)->base + _4053);
        if (binary_op_a(EQUALS, _4054, 58)){
            _4054 = NOVALUE;
            goto L5; // [90] 101
        }
        _4054 = NOVALUE;

        /** 					elem = SLASH & elem*/
        Prepend(&_elem_7468, _elem_7468, 92);
L5: 
L4: 

        /** 		fname &= elem*/
        Concat((object_ptr)&_fname_7464, _fname_7464, _elem_7468);
        DeRefDS(_elem_7468);
        _elem_7468 = NOVALUE;

        /** 	end for*/
        _i_7466 = _i_7466 + 1;
        goto L1; // [112] 22
L2: 
        ;
    }

    /** 	return fname*/
    DeRefDS(_path_elements_7463);
    DeRef(_4047);
    _4047 = NOVALUE;
    return _fname_7464;
    ;
}


int _8file_type(int _filename_7497)
{
    int _dirfil_7498 = NOVALUE;
    int _dir_inlined_dir_at_66_7511 = NOVALUE;
    int _4082 = NOVALUE;
    int _4081 = NOVALUE;
    int _4080 = NOVALUE;
    int _4079 = NOVALUE;
    int _4078 = NOVALUE;
    int _4076 = NOVALUE;
    int _4075 = NOVALUE;
    int _4074 = NOVALUE;
    int _4073 = NOVALUE;
    int _4072 = NOVALUE;
    int _4071 = NOVALUE;
    int _4070 = NOVALUE;
    int _4068 = NOVALUE;
    int _4067 = NOVALUE;
    int _4066 = NOVALUE;
    int _4065 = NOVALUE;
    int _4064 = NOVALUE;
    int _4063 = NOVALUE;
    int _4061 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if eu:find('*', filename) or eu:find('?', filename) then return FILETYPE_UNDEFINED end if*/
    _4061 = find_from(42, _filename_7497, 1);
    if (_4061 != 0) {
        goto L1; // [10] 24
    }
    _4063 = find_from(63, _filename_7497, 1);
    if (_4063 == 0)
    {
        _4063 = NOVALUE;
        goto L2; // [20] 31
    }
    else{
        _4063 = NOVALUE;
    }
L1: 
    DeRefDS(_filename_7497);
    DeRef(_dirfil_7498);
    return -1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 2 and filename[2] = ':' then*/
    if (IS_SEQUENCE(_filename_7497)){
            _4064 = SEQ_PTR(_filename_7497)->length;
    }
    else {
        _4064 = 1;
    }
    _4065 = (_4064 == 2);
    _4064 = NOVALUE;
    if (_4065 == 0) {
        goto L3; // [42] 65
    }
    _2 = (int)SEQ_PTR(_filename_7497);
    _4067 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4067)) {
        _4068 = (_4067 == 58);
    }
    else {
        _4068 = binary_op(EQUALS, _4067, 58);
    }
    _4067 = NOVALUE;
    if (_4068 == 0) {
        DeRef(_4068);
        _4068 = NOVALUE;
        goto L3; // [55] 65
    }
    else {
        if (!IS_ATOM_INT(_4068) && DBL_PTR(_4068)->dbl == 0.0){
            DeRef(_4068);
            _4068 = NOVALUE;
            goto L3; // [55] 65
        }
        DeRef(_4068);
        _4068 = NOVALUE;
    }
    DeRef(_4068);
    _4068 = NOVALUE;

    /** 			filename &= "\\"*/
    Concat((object_ptr)&_filename_7497, _filename_7497, _3723);
L3: 

    /** 	dirfil = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_dirfil_7498);
    _dirfil_7498 = machine(22, _filename_7497);

    /** 	if sequence(dirfil) then*/
    _4070 = IS_SEQUENCE(_dirfil_7498);
    if (_4070 == 0)
    {
        _4070 = NOVALUE;
        goto L4; // [81] 169
    }
    else{
        _4070 = NOVALUE;
    }

    /** 		if length( dirfil ) > 1 or eu:find('d', dirfil[1][2]) or (length(filename)=3 and filename[2]=':') then*/
    if (IS_SEQUENCE(_dirfil_7498)){
            _4071 = SEQ_PTR(_dirfil_7498)->length;
    }
    else {
        _4071 = 1;
    }
    _4072 = (_4071 > 1);
    _4071 = NOVALUE;
    if (_4072 != 0) {
        _4073 = 1;
        goto L5; // [93] 114
    }
    _2 = (int)SEQ_PTR(_dirfil_7498);
    _4074 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4074);
    _4075 = (int)*(((s1_ptr)_2)->base + 2);
    _4074 = NOVALUE;
    _4076 = find_from(100, _4075, 1);
    _4075 = NOVALUE;
    _4073 = (_4076 != 0);
L5: 
    if (_4073 != 0) {
        goto L6; // [114] 146
    }
    if (IS_SEQUENCE(_filename_7497)){
            _4078 = SEQ_PTR(_filename_7497)->length;
    }
    else {
        _4078 = 1;
    }
    _4079 = (_4078 == 3);
    _4078 = NOVALUE;
    if (_4079 == 0) {
        DeRef(_4080);
        _4080 = 0;
        goto L7; // [125] 141
    }
    _2 = (int)SEQ_PTR(_filename_7497);
    _4081 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4081)) {
        _4082 = (_4081 == 58);
    }
    else {
        _4082 = binary_op(EQUALS, _4081, 58);
    }
    _4081 = NOVALUE;
    if (IS_ATOM_INT(_4082))
    _4080 = (_4082 != 0);
    else
    _4080 = DBL_PTR(_4082)->dbl != 0.0;
L7: 
    if (_4080 == 0)
    {
        _4080 = NOVALUE;
        goto L8; // [142] 157
    }
    else{
        _4080 = NOVALUE;
    }
L6: 

    /** 			return FILETYPE_DIRECTORY*/
    DeRefDS(_filename_7497);
    DeRef(_dirfil_7498);
    DeRef(_4065);
    _4065 = NOVALUE;
    DeRef(_4072);
    _4072 = NOVALUE;
    DeRef(_4079);
    _4079 = NOVALUE;
    DeRef(_4082);
    _4082 = NOVALUE;
    return 2;
    goto L9; // [154] 178
L8: 

    /** 			return FILETYPE_FILE*/
    DeRefDS(_filename_7497);
    DeRef(_dirfil_7498);
    DeRef(_4065);
    _4065 = NOVALUE;
    DeRef(_4072);
    _4072 = NOVALUE;
    DeRef(_4079);
    _4079 = NOVALUE;
    DeRef(_4082);
    _4082 = NOVALUE;
    return 1;
    goto L9; // [166] 178
L4: 

    /** 		return FILETYPE_NOT_FOUND*/
    DeRefDS(_filename_7497);
    DeRef(_dirfil_7498);
    DeRef(_4065);
    _4065 = NOVALUE;
    DeRef(_4072);
    _4072 = NOVALUE;
    DeRef(_4079);
    _4079 = NOVALUE;
    DeRef(_4082);
    _4082 = NOVALUE;
    return 0;
L9: 
    ;
}


int _8file_exists(int _name_7555)
{
    int _pName_7558 = NOVALUE;
    int _r_7561 = NOVALUE;
    int _4097 = NOVALUE;
    int _4095 = NOVALUE;
    int _4093 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(name) then*/
    _4093 = IS_ATOM(_name_7555);
    if (_4093 == 0)
    {
        _4093 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _4093 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_name_7555);
    DeRef(_pName_7558);
    DeRef(_r_7561);
    return 0;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom pName = allocate_string(name)*/
    Ref(_name_7555);
    _0 = _pName_7558;
    _pName_7558 = _11allocate_string(_name_7555, 0);
    DeRef(_0);

    /** 		atom r = c_func(xGetFileAttributes, {pName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pName_7558);
    *((int *)(_2+4)) = _pName_7558;
    _4095 = MAKE_SEQ(_1);
    DeRef(_r_7561);
    _r_7561 = call_c(1, _8xGetFileAttributes_6530, _4095);
    DeRefDS(_4095);
    _4095 = NOVALUE;

    /** 		free(pName)*/
    Ref(_pName_7558);
    _11free(_pName_7558);

    /** 		return r > 0*/
    if (IS_ATOM_INT(_r_7561)) {
        _4097 = (_r_7561 > 0);
    }
    else {
        _4097 = (DBL_PTR(_r_7561)->dbl > (double)0);
    }
    DeRef(_name_7555);
    DeRef(_pName_7558);
    DeRef(_r_7561);
    return _4097;
    ;
}


int _8file_timestamp(int _fname_7568)
{
    int _d_7569 = NOVALUE;
    int _dir_inlined_dir_at_4_7571 = NOVALUE;
    int _4111 = NOVALUE;
    int _4110 = NOVALUE;
    int _4109 = NOVALUE;
    int _4108 = NOVALUE;
    int _4107 = NOVALUE;
    int _4106 = NOVALUE;
    int _4105 = NOVALUE;
    int _4104 = NOVALUE;
    int _4103 = NOVALUE;
    int _4102 = NOVALUE;
    int _4101 = NOVALUE;
    int _4100 = NOVALUE;
    int _4099 = NOVALUE;
    int _4098 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d = dir(fname)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_d_7569);
    _d_7569 = machine(22, _fname_7568);

    /** 	if atom(d) then return -1 end if*/
    _4098 = IS_ATOM(_d_7569);
    if (_4098 == 0)
    {
        _4098 = NOVALUE;
        goto L1; // [19] 27
    }
    else{
        _4098 = NOVALUE;
    }
    DeRefDS(_fname_7568);
    DeRef(_d_7569);
    return -1;
L1: 

    /** 	return datetime:new(d[1][D_YEAR], d[1][D_MONTH], d[1][D_DAY],*/
    _2 = (int)SEQ_PTR(_d_7569);
    _4099 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4099);
    _4100 = (int)*(((s1_ptr)_2)->base + 4);
    _4099 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7569);
    _4101 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4101);
    _4102 = (int)*(((s1_ptr)_2)->base + 5);
    _4101 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7569);
    _4103 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4103);
    _4104 = (int)*(((s1_ptr)_2)->base + 6);
    _4103 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7569);
    _4105 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4105);
    _4106 = (int)*(((s1_ptr)_2)->base + 7);
    _4105 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7569);
    _4107 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4107);
    _4108 = (int)*(((s1_ptr)_2)->base + 8);
    _4107 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_7569);
    _4109 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4109);
    _4110 = (int)*(((s1_ptr)_2)->base + 9);
    _4109 = NOVALUE;
    Ref(_4100);
    Ref(_4102);
    Ref(_4104);
    Ref(_4106);
    Ref(_4108);
    Ref(_4110);
    _4111 = _9new(_4100, _4102, _4104, _4106, _4108, _4110);
    _4100 = NOVALUE;
    _4102 = NOVALUE;
    _4104 = NOVALUE;
    _4106 = NOVALUE;
    _4108 = NOVALUE;
    _4110 = NOVALUE;
    DeRefDS(_fname_7568);
    DeRef(_d_7569);
    return _4111;
    ;
}


int _8copy_file(int _src_7589, int _dest_7590, int _overwrite_7591)
{
    int _info_7602 = NOVALUE;
    int _psrc_7606 = NOVALUE;
    int _pdest_7609 = NOVALUE;
    int _success_7612 = NOVALUE;
    int _4127 = NOVALUE;
    int _4125 = NOVALUE;
    int _4124 = NOVALUE;
    int _4120 = NOVALUE;
    int _4116 = NOVALUE;
    int _4115 = NOVALUE;
    int _4113 = NOVALUE;
    int _4112 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_7591)) {
        _1 = (long)(DBL_PTR(_overwrite_7591)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_7591)) && (DBL_PTR(_overwrite_7591)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_7591);
        _overwrite_7591 = _1;
    }

    /** 	if length(dest) then*/
    if (IS_SEQUENCE(_dest_7590)){
            _4112 = SEQ_PTR(_dest_7590)->length;
    }
    else {
        _4112 = 1;
    }
    if (_4112 == 0)
    {
        _4112 = NOVALUE;
        goto L1; // [14] 74
    }
    else{
        _4112 = NOVALUE;
    }

    /** 		if file_type( dest ) = FILETYPE_DIRECTORY then*/
    RefDS(_dest_7590);
    _4113 = _8file_type(_dest_7590);
    if (binary_op_a(NOTEQ, _4113, 2)){
        DeRef(_4113);
        _4113 = NOVALUE;
        goto L2; // [25] 71
    }
    DeRef(_4113);
    _4113 = NOVALUE;

    /** 			if dest[$] != SLASH then*/
    if (IS_SEQUENCE(_dest_7590)){
            _4115 = SEQ_PTR(_dest_7590)->length;
    }
    else {
        _4115 = 1;
    }
    _2 = (int)SEQ_PTR(_dest_7590);
    _4116 = (int)*(((s1_ptr)_2)->base + _4115);
    if (binary_op_a(EQUALS, _4116, 92)){
        _4116 = NOVALUE;
        goto L3; // [38] 49
    }
    _4116 = NOVALUE;

    /** 				dest &= SLASH*/
    Append(&_dest_7590, _dest_7590, 92);
L3: 

    /** 			sequence info = pathinfo( src )*/
    RefDS(_src_7589);
    _0 = _info_7602;
    _info_7602 = _8pathinfo(_src_7589, 0);
    DeRef(_0);

    /** 			dest &= info[PATH_FILENAME]*/
    _2 = (int)SEQ_PTR(_info_7602);
    _4120 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_dest_7590) && IS_ATOM(_4120)) {
        Ref(_4120);
        Append(&_dest_7590, _dest_7590, _4120);
    }
    else if (IS_ATOM(_dest_7590) && IS_SEQUENCE(_4120)) {
    }
    else {
        Concat((object_ptr)&_dest_7590, _dest_7590, _4120);
    }
    _4120 = NOVALUE;
L2: 
    DeRef(_info_7602);
    _info_7602 = NOVALUE;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom psrc = allocate_string(src)*/
    RefDS(_src_7589);
    _0 = _psrc_7606;
    _psrc_7606 = _11allocate_string(_src_7589, 0);
    DeRef(_0);

    /** 		atom pdest = allocate_string(dest)*/
    RefDS(_dest_7590);
    _0 = _pdest_7609;
    _pdest_7609 = _11allocate_string(_dest_7590, 0);
    DeRef(_0);

    /** 		integer success = c_func(xCopyFile, {psrc, pdest, not overwrite})*/
    _4124 = (_overwrite_7591 == 0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_psrc_7606);
    *((int *)(_2+4)) = _psrc_7606;
    Ref(_pdest_7609);
    *((int *)(_2+8)) = _pdest_7609;
    *((int *)(_2+12)) = _4124;
    _4125 = MAKE_SEQ(_1);
    _4124 = NOVALUE;
    _success_7612 = call_c(1, _8xCopyFile_6506, _4125);
    DeRefDS(_4125);
    _4125 = NOVALUE;
    if (!IS_ATOM_INT(_success_7612)) {
        _1 = (long)(DBL_PTR(_success_7612)->dbl);
        if (UNIQUE(DBL_PTR(_success_7612)) && (DBL_PTR(_success_7612)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_7612);
        _success_7612 = _1;
    }

    /** 		free({pdest, psrc})*/
    Ref(_psrc_7606);
    Ref(_pdest_7609);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_7609;
    ((int *)_2)[2] = _psrc_7606;
    _4127 = MAKE_SEQ(_1);
    _11free(_4127);
    _4127 = NOVALUE;

    /** 	return success*/
    DeRefDS(_src_7589);
    DeRefDS(_dest_7590);
    DeRef(_psrc_7606);
    DeRef(_pdest_7609);
    return _success_7612;
    ;
}


int _8rename_file(int _old_name_7620, int _new_name_7621, int _overwrite_7622)
{
    int _psrc_7623 = NOVALUE;
    int _pdest_7624 = NOVALUE;
    int _ret_7625 = NOVALUE;
    int _tempfile_7626 = NOVALUE;
    int _4142 = NOVALUE;
    int _4139 = NOVALUE;
    int _4137 = NOVALUE;
    int _4135 = NOVALUE;
    int _4130 = NOVALUE;
    int _4129 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_7622)) {
        _1 = (long)(DBL_PTR(_overwrite_7622)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_7622)) && (DBL_PTR(_overwrite_7622)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_7622);
        _overwrite_7622 = _1;
    }

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_7626);
    _tempfile_7626 = _5;

    /** 	if not overwrite then*/
    if (_overwrite_7622 != 0)
    goto L1; // [18] 40

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_7621);
    _4129 = _8file_exists(_new_name_7621);
    if (_4129 == 0) {
        DeRef(_4129);
        _4129 = NOVALUE;
        goto L2; // [27] 70
    }
    else {
        if (!IS_ATOM_INT(_4129) && DBL_PTR(_4129)->dbl == 0.0){
            DeRef(_4129);
            _4129 = NOVALUE;
            goto L2; // [27] 70
        }
        DeRef(_4129);
        _4129 = NOVALUE;
    }
    DeRef(_4129);
    _4129 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_old_name_7620);
    DeRefDS(_new_name_7621);
    DeRef(_psrc_7623);
    DeRef(_pdest_7624);
    DeRef(_ret_7625);
    DeRefDS(_tempfile_7626);
    return 0;
    goto L2; // [37] 70
L1: 

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_7621);
    _4130 = _8file_exists(_new_name_7621);
    if (_4130 == 0) {
        DeRef(_4130);
        _4130 = NOVALUE;
        goto L3; // [46] 69
    }
    else {
        if (!IS_ATOM_INT(_4130) && DBL_PTR(_4130)->dbl == 0.0){
            DeRef(_4130);
            _4130 = NOVALUE;
            goto L3; // [46] 69
        }
        DeRef(_4130);
        _4130 = NOVALUE;
    }
    DeRef(_4130);
    _4130 = NOVALUE;

    /** 			tempfile = temp_file(new_name)*/
    RefDS(_new_name_7621);
    RefDS(_5);
    RefDS(_4363);
    _0 = _tempfile_7626;
    _tempfile_7626 = _8temp_file(_new_name_7621, _5, _4363, 0);
    DeRef(_0);

    /** 			ret = move_file(new_name, tempfile)*/
    RefDS(_new_name_7621);
    RefDS(_tempfile_7626);
    _0 = _ret_7625;
    _ret_7625 = _8move_file(_new_name_7621, _tempfile_7626, 0);
    DeRef(_0);
L3: 
L2: 

    /** 	psrc = machine:allocate_string(old_name)*/
    RefDS(_old_name_7620);
    _0 = _psrc_7623;
    _psrc_7623 = _11allocate_string(_old_name_7620, 0);
    DeRef(_0);

    /** 	pdest = machine:allocate_string(new_name)*/
    RefDS(_new_name_7621);
    _0 = _pdest_7624;
    _pdest_7624 = _11allocate_string(_new_name_7621, 0);
    DeRef(_0);

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_7624);
    Ref(_psrc_7623);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_7623;
    ((int *)_2)[2] = _pdest_7624;
    _4135 = MAKE_SEQ(_1);
    DeRef(_ret_7625);
    _ret_7625 = call_c(1, _8xMoveFile_6511, _4135);
    DeRefDS(_4135);
    _4135 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_7623);
    Ref(_pdest_7624);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_7624;
    ((int *)_2)[2] = _psrc_7623;
    _4137 = MAKE_SEQ(_1);
    _11free(_4137);
    _4137 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_7622 == 0)
    {
        goto L4; // [110] 144
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_7625)) {
        if (_ret_7625 != 0){
            goto L5; // [115] 137
        }
    }
    else {
        if (DBL_PTR(_ret_7625)->dbl != 0.0){
            goto L5; // [115] 137
        }
    }

    /** 			if length(tempfile) > 0 then*/
    if (IS_SEQUENCE(_tempfile_7626)){
            _4139 = SEQ_PTR(_tempfile_7626)->length;
    }
    else {
        _4139 = 1;
    }
    if (_4139 <= 0)
    goto L6; // [123] 136

    /** 				ret = move_file(tempfile, new_name)*/
    RefDS(_tempfile_7626);
    RefDS(_new_name_7621);
    _0 = _ret_7625;
    _ret_7625 = _8move_file(_tempfile_7626, _new_name_7621, 0);
    DeRef(_0);
L6: 
L5: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_7626);
    _4142 = _8delete_file(_tempfile_7626);
L4: 

    /** 	return ret*/
    DeRefDS(_old_name_7620);
    DeRefDS(_new_name_7621);
    DeRef(_psrc_7623);
    DeRef(_pdest_7624);
    DeRef(_tempfile_7626);
    DeRef(_4142);
    _4142 = NOVALUE;
    return _ret_7625;
    ;
}


int _8move_file(int _src_7654, int _dest_7655, int _overwrite_7656)
{
    int _psrc_7657 = NOVALUE;
    int _pdest_7658 = NOVALUE;
    int _ret_7659 = NOVALUE;
    int _tempfile_7660 = NOVALUE;
    int _4161 = NOVALUE;
    int _4160 = NOVALUE;
    int _4159 = NOVALUE;
    int _4158 = NOVALUE;
    int _4156 = NOVALUE;
    int _4154 = NOVALUE;
    int _4153 = NOVALUE;
    int _4152 = NOVALUE;
    int _4151 = NOVALUE;
    int _4146 = NOVALUE;
    int _4143 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_7656)) {
        _1 = (long)(DBL_PTR(_overwrite_7656)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_7656)) && (DBL_PTR(_overwrite_7656)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_7656);
        _overwrite_7656 = _1;
    }

    /** 	atom psrc = 0, pdest = 0, ret*/
    DeRef(_psrc_7657);
    _psrc_7657 = 0;
    DeRef(_pdest_7658);
    _pdest_7658 = 0;

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_7660);
    _tempfile_7660 = _5;

    /** 	if not file_exists(src) then*/
    RefDS(_src_7654);
    _4143 = _8file_exists(_src_7654);
    if (IS_ATOM_INT(_4143)) {
        if (_4143 != 0){
            DeRef(_4143);
            _4143 = NOVALUE;
            goto L1; // [30] 40
        }
    }
    else {
        if (DBL_PTR(_4143)->dbl != 0.0){
            DeRef(_4143);
            _4143 = NOVALUE;
            goto L1; // [30] 40
        }
    }
    DeRef(_4143);
    _4143 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_src_7654);
    DeRefDS(_dest_7655);
    DeRef(_ret_7659);
    DeRefDS(_tempfile_7660);
    return 0;
L1: 

    /** 	if not overwrite then*/
    if (_overwrite_7656 != 0)
    goto L2; // [42] 62

    /** 		if file_exists( dest ) then*/
    RefDS(_dest_7655);
    _4146 = _8file_exists(_dest_7655);
    if (_4146 == 0) {
        DeRef(_4146);
        _4146 = NOVALUE;
        goto L3; // [51] 61
    }
    else {
        if (!IS_ATOM_INT(_4146) && DBL_PTR(_4146)->dbl == 0.0){
            DeRef(_4146);
            _4146 = NOVALUE;
            goto L3; // [51] 61
        }
        DeRef(_4146);
        _4146 = NOVALUE;
    }
    DeRef(_4146);
    _4146 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_src_7654);
    DeRefDS(_dest_7655);
    DeRef(_psrc_7657);
    DeRef(_pdest_7658);
    DeRef(_ret_7659);
    DeRef(_tempfile_7660);
    return 0;
L3: 
L2: 

    /** 	ifdef UNIX then*/

    /** 	ifdef LINUX then*/

    /** 	ifdef UNIX then*/

    /** 		psrc  = machine:allocate_string(src)*/
    RefDS(_src_7654);
    _0 = _psrc_7657;
    _psrc_7657 = _11allocate_string(_src_7654, 0);
    DeRef(_0);

    /** 		pdest = machine:allocate_string(dest)*/
    RefDS(_dest_7655);
    _0 = _pdest_7658;
    _pdest_7658 = _11allocate_string(_dest_7655, 0);
    DeRef(_0);

    /** 	if overwrite then*/
    if (_overwrite_7656 == 0)
    {
        goto L4; // [84] 113
    }
    else{
    }

    /** 		tempfile = temp_file(dest)*/
    RefDS(_dest_7655);
    RefDS(_5);
    RefDS(_4363);
    _0 = _tempfile_7660;
    _tempfile_7660 = _8temp_file(_dest_7655, _5, _4363, 0);
    DeRef(_0);

    /** 		move_file(dest, tempfile)*/
    RefDS(_dest_7655);
    DeRef(_4151);
    _4151 = _dest_7655;
    RefDS(_tempfile_7660);
    DeRef(_4152);
    _4152 = _tempfile_7660;
    _4153 = _8move_file(_4151, _4152, 0);
    _4151 = NOVALUE;
    _4152 = NOVALUE;
L4: 

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_7658);
    Ref(_psrc_7657);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_7657;
    ((int *)_2)[2] = _pdest_7658;
    _4154 = MAKE_SEQ(_1);
    DeRef(_ret_7659);
    _ret_7659 = call_c(1, _8xMoveFile_6511, _4154);
    DeRefDS(_4154);
    _4154 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_7657);
    Ref(_pdest_7658);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_7658;
    ((int *)_2)[2] = _psrc_7657;
    _4156 = MAKE_SEQ(_1);
    _11free(_4156);
    _4156 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_7656 == 0)
    {
        goto L5; // [139] 169
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_7659)) {
        if (_ret_7659 != 0){
            goto L6; // [144] 162
        }
    }
    else {
        if (DBL_PTR(_ret_7659)->dbl != 0.0){
            goto L6; // [144] 162
        }
    }

    /** 			move_file(tempfile, dest)*/
    RefDS(_tempfile_7660);
    DeRef(_4158);
    _4158 = _tempfile_7660;
    RefDS(_dest_7655);
    DeRef(_4159);
    _4159 = _dest_7655;
    _4160 = _8move_file(_4158, _4159, 0);
    _4158 = NOVALUE;
    _4159 = NOVALUE;
L6: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_7660);
    _4161 = _8delete_file(_tempfile_7660);
L5: 

    /** 	return ret*/
    DeRefDS(_src_7654);
    DeRefDS(_dest_7655);
    DeRef(_psrc_7657);
    DeRef(_pdest_7658);
    DeRef(_tempfile_7660);
    DeRef(_4153);
    _4153 = NOVALUE;
    DeRef(_4160);
    _4160 = NOVALUE;
    DeRef(_4161);
    _4161 = NOVALUE;
    return _ret_7659;
    ;
}


int _8file_length(int _filename_7689)
{
    int _list_7690 = NOVALUE;
    int _dir_inlined_dir_at_4_7692 = NOVALUE;
    int _4167 = NOVALUE;
    int _4166 = NOVALUE;
    int _4165 = NOVALUE;
    int _4164 = NOVALUE;
    int _4162 = NOVALUE;
    int _0, _1, _2;
    

    /** 	list = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_list_7690);
    _list_7690 = machine(22, _filename_7689);

    /** 	if atom(list) or length(list) = 0 then*/
    _4162 = IS_ATOM(_list_7690);
    if (_4162 != 0) {
        goto L1; // [19] 35
    }
    if (IS_SEQUENCE(_list_7690)){
            _4164 = SEQ_PTR(_list_7690)->length;
    }
    else {
        _4164 = 1;
    }
    _4165 = (_4164 == 0);
    _4164 = NOVALUE;
    if (_4165 == 0)
    {
        DeRef(_4165);
        _4165 = NOVALUE;
        goto L2; // [31] 42
    }
    else{
        DeRef(_4165);
        _4165 = NOVALUE;
    }
L1: 

    /** 		return -1*/
    DeRefDS(_filename_7689);
    DeRef(_list_7690);
    return -1;
L2: 

    /** 	return list[1][D_SIZE]*/
    _2 = (int)SEQ_PTR(_list_7690);
    _4166 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4166);
    _4167 = (int)*(((s1_ptr)_2)->base + 3);
    _4166 = NOVALUE;
    Ref(_4167);
    DeRefDS(_filename_7689);
    DeRef(_list_7690);
    return _4167;
    ;
}


int _8locate_file(int _filename_7702, int _search_list_7703, int _subdir_7704)
{
    int _extra_paths_7705 = NOVALUE;
    int _this_path_7706 = NOVALUE;
    int _4251 = NOVALUE;
    int _4250 = NOVALUE;
    int _4248 = NOVALUE;
    int _4246 = NOVALUE;
    int _4244 = NOVALUE;
    int _4243 = NOVALUE;
    int _4242 = NOVALUE;
    int _4240 = NOVALUE;
    int _4239 = NOVALUE;
    int _4238 = NOVALUE;
    int _4236 = NOVALUE;
    int _4235 = NOVALUE;
    int _4234 = NOVALUE;
    int _4231 = NOVALUE;
    int _4230 = NOVALUE;
    int _4228 = NOVALUE;
    int _4226 = NOVALUE;
    int _4225 = NOVALUE;
    int _4222 = NOVALUE;
    int _4217 = NOVALUE;
    int _4213 = NOVALUE;
    int _4204 = NOVALUE;
    int _4201 = NOVALUE;
    int _4198 = NOVALUE;
    int _4197 = NOVALUE;
    int _4193 = NOVALUE;
    int _4190 = NOVALUE;
    int _4188 = NOVALUE;
    int _4184 = NOVALUE;
    int _4182 = NOVALUE;
    int _4181 = NOVALUE;
    int _4179 = NOVALUE;
    int _4178 = NOVALUE;
    int _4175 = NOVALUE;
    int _4174 = NOVALUE;
    int _4171 = NOVALUE;
    int _4169 = NOVALUE;
    int _4168 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path(filename) then*/
    RefDS(_filename_7702);
    _4168 = _8absolute_path(_filename_7702);
    if (_4168 == 0) {
        DeRef(_4168);
        _4168 = NOVALUE;
        goto L1; // [13] 23
    }
    else {
        if (!IS_ATOM_INT(_4168) && DBL_PTR(_4168)->dbl == 0.0){
            DeRef(_4168);
            _4168 = NOVALUE;
            goto L1; // [13] 23
        }
        DeRef(_4168);
        _4168 = NOVALUE;
    }
    DeRef(_4168);
    _4168 = NOVALUE;

    /** 		return filename*/
    DeRefDS(_search_list_7703);
    DeRefDS(_subdir_7704);
    DeRef(_extra_paths_7705);
    DeRef(_this_path_7706);
    return _filename_7702;
L1: 

    /** 	if length(search_list) = 0 then*/
    if (IS_SEQUENCE(_search_list_7703)){
            _4169 = SEQ_PTR(_search_list_7703)->length;
    }
    else {
        _4169 = 1;
    }
    if (_4169 != 0)
    goto L2; // [28] 281

    /** 		search_list = append(search_list, "." & SLASH)*/
    Append(&_4171, _3512, 92);
    RefDS(_4171);
    Append(&_search_list_7703, _search_list_7703, _4171);
    DeRefDS(_4171);
    _4171 = NOVALUE;

    /** 		extra_paths = command_line()*/
    DeRef(_extra_paths_7705);
    _extra_paths_7705 = Command_Line();

    /** 		extra_paths = canonical_path(dirname(extra_paths[2]), 1)*/
    _2 = (int)SEQ_PTR(_extra_paths_7705);
    _4174 = (int)*(((s1_ptr)_2)->base + 2);
    RefDS(_4174);
    _4175 = _8dirname(_4174, 0);
    _4174 = NOVALUE;
    _0 = _extra_paths_7705;
    _extra_paths_7705 = _8canonical_path(_4175, 1, 0);
    DeRefDS(_0);
    _4175 = NOVALUE;

    /** 		search_list = append(search_list, extra_paths)*/
    Ref(_extra_paths_7705);
    Append(&_search_list_7703, _search_list_7703, _extra_paths_7705);

    /** 		ifdef UNIX then*/

    /** 			extra_paths = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _4178 = EGetEnv(_3801);
    _4179 = EGetEnv(_3803);
    if (IS_SEQUENCE(_4178) && IS_ATOM(_4179)) {
        Ref(_4179);
        Append(&_extra_paths_7705, _4178, _4179);
    }
    else if (IS_ATOM(_4178) && IS_SEQUENCE(_4179)) {
        Ref(_4178);
        Prepend(&_extra_paths_7705, _4179, _4178);
    }
    else {
        Concat((object_ptr)&_extra_paths_7705, _4178, _4179);
        DeRef(_4178);
        _4178 = NOVALUE;
    }
    DeRef(_4178);
    _4178 = NOVALUE;
    DeRef(_4179);
    _4179 = NOVALUE;

    /** 		if sequence(extra_paths) then*/
    _4181 = 1;
    if (_4181 == 0)
    {
        _4181 = NOVALUE;
        goto L3; // [90] 104
    }
    else{
        _4181 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    Append(&_4182, _extra_paths_7705, 92);
    RefDS(_4182);
    Append(&_search_list_7703, _search_list_7703, _4182);
    DeRefDS(_4182);
    _4182 = NOVALUE;
L3: 

    /** 		search_list = append(search_list, ".." & SLASH)*/
    Append(&_4184, _3543, 92);
    RefDS(_4184);
    Append(&_search_list_7703, _search_list_7703, _4184);
    DeRefDS(_4184);
    _4184 = NOVALUE;

    /** 		extra_paths = getenv("EUDIR")*/
    DeRef(_extra_paths_7705);
    _extra_paths_7705 = EGetEnv(_4186);

    /** 		if sequence(extra_paths) then*/
    _4188 = IS_SEQUENCE(_extra_paths_7705);
    if (_4188 == 0)
    {
        _4188 = NOVALUE;
        goto L4; // [124] 154
    }
    else{
        _4188 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH & "bin" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4189;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7705;
        Concat_N((object_ptr)&_4190, concat_list, 4);
    }
    RefDS(_4190);
    Append(&_search_list_7703, _search_list_7703, _4190);
    DeRefDS(_4190);
    _4190 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "docs" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4192;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7705;
        Concat_N((object_ptr)&_4193, concat_list, 4);
    }
    RefDS(_4193);
    Append(&_search_list_7703, _search_list_7703, _4193);
    DeRefDS(_4193);
    _4193 = NOVALUE;
L4: 

    /** 		extra_paths = getenv("EUDIST")*/
    DeRef(_extra_paths_7705);
    _extra_paths_7705 = EGetEnv(_4195);

    /** 		if sequence(extra_paths) then*/
    _4197 = IS_SEQUENCE(_extra_paths_7705);
    if (_4197 == 0)
    {
        _4197 = NOVALUE;
        goto L5; // [164] 204
    }
    else{
        _4197 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    if (IS_SEQUENCE(_extra_paths_7705) && IS_ATOM(92)) {
        Append(&_4198, _extra_paths_7705, 92);
    }
    else if (IS_ATOM(_extra_paths_7705) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_4198, _extra_paths_7705, 92);
    }
    RefDS(_4198);
    Append(&_search_list_7703, _search_list_7703, _4198);
    DeRefDS(_4198);
    _4198 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "etc" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4200;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7705;
        Concat_N((object_ptr)&_4201, concat_list, 4);
    }
    RefDS(_4201);
    Append(&_search_list_7703, _search_list_7703, _4201);
    DeRefDS(_4201);
    _4201 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "data" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _4203;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7705;
        Concat_N((object_ptr)&_4204, concat_list, 4);
    }
    RefDS(_4204);
    Append(&_search_list_7703, _search_list_7703, _4204);
    DeRefDS(_4204);
    _4204 = NOVALUE;
L5: 

    /** 		ifdef UNIX then*/

    /** 		search_list &= include_paths(1)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_4212);
    *((int *)(_2+4)) = _4212;
    RefDS(_4211);
    *((int *)(_2+8)) = _4211;
    RefDS(_4210);
    *((int *)(_2+12)) = _4210;
    RefDS(_4209);
    *((int *)(_2+16)) = _4209;
    RefDS(_4208);
    *((int *)(_2+20)) = _4208;
    _4213 = MAKE_SEQ(_1);
    Concat((object_ptr)&_search_list_7703, _search_list_7703, _4213);
    DeRefDS(_4213);
    _4213 = NOVALUE;

    /** 		extra_paths = getenv("USERPATH")*/
    DeRef(_extra_paths_7705);
    _extra_paths_7705 = EGetEnv(_4215);

    /** 		if sequence(extra_paths) then*/
    _4217 = IS_SEQUENCE(_extra_paths_7705);
    if (_4217 == 0)
    {
        _4217 = NOVALUE;
        goto L6; // [230] 249
    }
    else{
        _4217 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_7705);
    _0 = _extra_paths_7705;
    _extra_paths_7705 = _20split(_extra_paths_7705, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_7703) && IS_ATOM(_extra_paths_7705)) {
        Ref(_extra_paths_7705);
        Append(&_search_list_7703, _search_list_7703, _extra_paths_7705);
    }
    else if (IS_ATOM(_search_list_7703) && IS_SEQUENCE(_extra_paths_7705)) {
    }
    else {
        Concat((object_ptr)&_search_list_7703, _search_list_7703, _extra_paths_7705);
    }
L6: 

    /** 		extra_paths = getenv("PATH")*/
    DeRef(_extra_paths_7705);
    _extra_paths_7705 = EGetEnv(_4220);

    /** 		if sequence(extra_paths) then*/
    _4222 = IS_SEQUENCE(_extra_paths_7705);
    if (_4222 == 0)
    {
        _4222 = NOVALUE;
        goto L7; // [259] 306
    }
    else{
        _4222 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_7705);
    _0 = _extra_paths_7705;
    _extra_paths_7705 = _20split(_extra_paths_7705, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_7703) && IS_ATOM(_extra_paths_7705)) {
        Ref(_extra_paths_7705);
        Append(&_search_list_7703, _search_list_7703, _extra_paths_7705);
    }
    else if (IS_ATOM(_search_list_7703) && IS_SEQUENCE(_extra_paths_7705)) {
    }
    else {
        Concat((object_ptr)&_search_list_7703, _search_list_7703, _extra_paths_7705);
    }
    goto L7; // [278] 306
L2: 

    /** 		if integer(search_list[1]) then*/
    _2 = (int)SEQ_PTR(_search_list_7703);
    _4225 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4225))
    _4226 = 1;
    else if (IS_ATOM_DBL(_4225))
    _4226 = IS_ATOM_INT(DoubleToInt(_4225));
    else
    _4226 = 0;
    _4225 = NOVALUE;
    if (_4226 == 0)
    {
        _4226 = NOVALUE;
        goto L8; // [290] 305
    }
    else{
        _4226 = NOVALUE;
    }

    /** 			search_list = stdseq:split(search_list, PATHSEP)*/
    RefDS(_search_list_7703);
    _0 = _search_list_7703;
    _search_list_7703 = _20split(_search_list_7703, 59, 0, 0);
    DeRefDS(_0);
L8: 
L7: 

    /** 	if length(subdir) > 0 then*/
    if (IS_SEQUENCE(_subdir_7704)){
            _4228 = SEQ_PTR(_subdir_7704)->length;
    }
    else {
        _4228 = 1;
    }
    if (_4228 <= 0)
    goto L9; // [311] 336

    /** 		if subdir[$] != SLASH then*/
    if (IS_SEQUENCE(_subdir_7704)){
            _4230 = SEQ_PTR(_subdir_7704)->length;
    }
    else {
        _4230 = 1;
    }
    _2 = (int)SEQ_PTR(_subdir_7704);
    _4231 = (int)*(((s1_ptr)_2)->base + _4230);
    if (binary_op_a(EQUALS, _4231, 92)){
        _4231 = NOVALUE;
        goto LA; // [324] 335
    }
    _4231 = NOVALUE;

    /** 			subdir &= SLASH*/
    Append(&_subdir_7704, _subdir_7704, 92);
LA: 
L9: 

    /** 	for i = 1 to length(search_list) do*/
    if (IS_SEQUENCE(_search_list_7703)){
            _4234 = SEQ_PTR(_search_list_7703)->length;
    }
    else {
        _4234 = 1;
    }
    {
        int _i_7785;
        _i_7785 = 1;
LB: 
        if (_i_7785 > _4234){
            goto LC; // [341] 466
        }

        /** 		if length(search_list[i]) = 0 then*/
        _2 = (int)SEQ_PTR(_search_list_7703);
        _4235 = (int)*(((s1_ptr)_2)->base + _i_7785);
        if (IS_SEQUENCE(_4235)){
                _4236 = SEQ_PTR(_4235)->length;
        }
        else {
            _4236 = 1;
        }
        _4235 = NOVALUE;
        if (_4236 != 0)
        goto LD; // [357] 366

        /** 			continue*/
        goto LE; // [363] 461
LD: 

        /** 		if search_list[i][$] != SLASH then*/
        _2 = (int)SEQ_PTR(_search_list_7703);
        _4238 = (int)*(((s1_ptr)_2)->base + _i_7785);
        if (IS_SEQUENCE(_4238)){
                _4239 = SEQ_PTR(_4238)->length;
        }
        else {
            _4239 = 1;
        }
        _2 = (int)SEQ_PTR(_4238);
        _4240 = (int)*(((s1_ptr)_2)->base + _4239);
        _4238 = NOVALUE;
        if (binary_op_a(EQUALS, _4240, 92)){
            _4240 = NOVALUE;
            goto LF; // [379] 398
        }
        _4240 = NOVALUE;

        /** 			search_list[i] &= SLASH*/
        _2 = (int)SEQ_PTR(_search_list_7703);
        _4242 = (int)*(((s1_ptr)_2)->base + _i_7785);
        if (IS_SEQUENCE(_4242) && IS_ATOM(92)) {
            Append(&_4243, _4242, 92);
        }
        else if (IS_ATOM(_4242) && IS_SEQUENCE(92)) {
        }
        else {
            Concat((object_ptr)&_4243, _4242, 92);
            _4242 = NOVALUE;
        }
        _4242 = NOVALUE;
        _2 = (int)SEQ_PTR(_search_list_7703);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _search_list_7703 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_7785);
        _1 = *(int *)_2;
        *(int *)_2 = _4243;
        if( _1 != _4243 ){
            DeRef(_1);
        }
        _4243 = NOVALUE;
LF: 

        /** 		if length(subdir) > 0 then*/
        if (IS_SEQUENCE(_subdir_7704)){
                _4244 = SEQ_PTR(_subdir_7704)->length;
        }
        else {
            _4244 = 1;
        }
        if (_4244 <= 0)
        goto L10; // [403] 422

        /** 			this_path = search_list[i] & subdir & filename*/
        _2 = (int)SEQ_PTR(_search_list_7703);
        _4246 = (int)*(((s1_ptr)_2)->base + _i_7785);
        {
            int concat_list[3];

            concat_list[0] = _filename_7702;
            concat_list[1] = _subdir_7704;
            concat_list[2] = _4246;
            Concat_N((object_ptr)&_this_path_7706, concat_list, 3);
        }
        _4246 = NOVALUE;
        goto L11; // [419] 433
L10: 

        /** 			this_path = search_list[i] & filename*/
        _2 = (int)SEQ_PTR(_search_list_7703);
        _4248 = (int)*(((s1_ptr)_2)->base + _i_7785);
        if (IS_SEQUENCE(_4248) && IS_ATOM(_filename_7702)) {
        }
        else if (IS_ATOM(_4248) && IS_SEQUENCE(_filename_7702)) {
            Ref(_4248);
            Prepend(&_this_path_7706, _filename_7702, _4248);
        }
        else {
            Concat((object_ptr)&_this_path_7706, _4248, _filename_7702);
            _4248 = NOVALUE;
        }
        _4248 = NOVALUE;
L11: 

        /** 		if file_exists(this_path) then*/
        RefDS(_this_path_7706);
        _4250 = _8file_exists(_this_path_7706);
        if (_4250 == 0) {
            DeRef(_4250);
            _4250 = NOVALUE;
            goto L12; // [441] 459
        }
        else {
            if (!IS_ATOM_INT(_4250) && DBL_PTR(_4250)->dbl == 0.0){
                DeRef(_4250);
                _4250 = NOVALUE;
                goto L12; // [441] 459
            }
            DeRef(_4250);
            _4250 = NOVALUE;
        }
        DeRef(_4250);
        _4250 = NOVALUE;

        /** 			return canonical_path(this_path)*/
        RefDS(_this_path_7706);
        _4251 = _8canonical_path(_this_path_7706, 0, 0);
        DeRefDS(_filename_7702);
        DeRefDS(_search_list_7703);
        DeRefDS(_subdir_7704);
        DeRef(_extra_paths_7705);
        DeRefDS(_this_path_7706);
        _4235 = NOVALUE;
        return _4251;
L12: 

        /** 	end for*/
LE: 
        _i_7785 = _i_7785 + 1;
        goto LB; // [461] 348
LC: 
        ;
    }

    /** 	return filename*/
    DeRefDS(_search_list_7703);
    DeRefDS(_subdir_7704);
    DeRef(_extra_paths_7705);
    DeRef(_this_path_7706);
    _4235 = NOVALUE;
    DeRef(_4251);
    _4251 = NOVALUE;
    return _filename_7702;
    ;
}


int _8disk_metrics(int _disk_path_7811)
{
    int _result_7812 = NOVALUE;
    int _path_addr_7814 = NOVALUE;
    int _metric_addr_7815 = NOVALUE;
    int _4264 = NOVALUE;
    int _4262 = NOVALUE;
    int _4261 = NOVALUE;
    int _4260 = NOVALUE;
    int _4259 = NOVALUE;
    int _4258 = NOVALUE;
    int _4257 = NOVALUE;
    int _4256 = NOVALUE;
    int _4253 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence result = {0, 0, 0, 0} */
    _0 = _result_7812;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    _result_7812 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	atom path_addr = 0*/
    DeRef(_path_addr_7814);
    _path_addr_7814 = 0;

    /** 	atom metric_addr = 0*/
    DeRef(_metric_addr_7815);
    _metric_addr_7815 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		if sequence(disk_path) then */
    _4253 = IS_SEQUENCE(_disk_path_7811);
    if (_4253 == 0)
    {
        _4253 = NOVALUE;
        goto L1; // [27] 40
    }
    else{
        _4253 = NOVALUE;
    }

    /** 			path_addr = allocate_string(disk_path) */
    Ref(_disk_path_7811);
    _path_addr_7814 = _11allocate_string(_disk_path_7811, 0);
    goto L2; // [37] 46
L1: 

    /** 			path_addr = 0 */
    DeRef(_path_addr_7814);
    _path_addr_7814 = 0;
L2: 

    /** 		metric_addr = allocate(16) */
    _0 = _metric_addr_7815;
    _metric_addr_7815 = _11allocate(16, 0);
    DeRef(_0);

    /** 		if c_func(xGetDiskFreeSpace, {path_addr, */
    if (IS_ATOM_INT(_metric_addr_7815)) {
        _4256 = _metric_addr_7815 + 0;
        if ((long)((unsigned long)_4256 + (unsigned long)HIGH_BITS) >= 0) 
        _4256 = NewDouble((double)_4256);
    }
    else {
        _4256 = NewDouble(DBL_PTR(_metric_addr_7815)->dbl + (double)0);
    }
    if (IS_ATOM_INT(_metric_addr_7815)) {
        _4257 = _metric_addr_7815 + 4;
        if ((long)((unsigned long)_4257 + (unsigned long)HIGH_BITS) >= 0) 
        _4257 = NewDouble((double)_4257);
    }
    else {
        _4257 = NewDouble(DBL_PTR(_metric_addr_7815)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_metric_addr_7815)) {
        _4258 = _metric_addr_7815 + 8;
        if ((long)((unsigned long)_4258 + (unsigned long)HIGH_BITS) >= 0) 
        _4258 = NewDouble((double)_4258);
    }
    else {
        _4258 = NewDouble(DBL_PTR(_metric_addr_7815)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_metric_addr_7815)) {
        _4259 = _metric_addr_7815 + 12;
        if ((long)((unsigned long)_4259 + (unsigned long)HIGH_BITS) >= 0) 
        _4259 = NewDouble((double)_4259);
    }
    else {
        _4259 = NewDouble(DBL_PTR(_metric_addr_7815)->dbl + (double)12);
    }
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_path_addr_7814);
    *((int *)(_2+4)) = _path_addr_7814;
    *((int *)(_2+8)) = _4256;
    *((int *)(_2+12)) = _4257;
    *((int *)(_2+16)) = _4258;
    *((int *)(_2+20)) = _4259;
    _4260 = MAKE_SEQ(_1);
    _4259 = NOVALUE;
    _4258 = NOVALUE;
    _4257 = NOVALUE;
    _4256 = NOVALUE;
    _4261 = call_c(1, _8xGetDiskFreeSpace_6534, _4260);
    DeRefDS(_4260);
    _4260 = NOVALUE;
    if (_4261 == 0) {
        DeRef(_4261);
        _4261 = NOVALUE;
        goto L3; // [86] 101
    }
    else {
        if (!IS_ATOM_INT(_4261) && DBL_PTR(_4261)->dbl == 0.0){
            DeRef(_4261);
            _4261 = NOVALUE;
            goto L3; // [86] 101
        }
        DeRef(_4261);
        _4261 = NOVALUE;
    }
    DeRef(_4261);
    _4261 = NOVALUE;

    /** 			result = peek4s({metric_addr, 4}) */
    Ref(_metric_addr_7815);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _metric_addr_7815;
    ((int *)_2)[2] = 4;
    _4262 = MAKE_SEQ(_1);
    DeRef(_result_7812);
    _1 = (int)SEQ_PTR(_4262);
    peek4_addr = (unsigned long *)get_pos_int("peek4s/peek4u", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _result_7812 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)*peek4_addr++;
        if (_1 < MININT || _1 > MAXINT)
        _1 = NewDouble((double)(long)_1);
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4262);
    _4262 = NOVALUE;
L3: 

    /** 		free({path_addr, metric_addr}) */
    Ref(_metric_addr_7815);
    Ref(_path_addr_7814);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_addr_7814;
    ((int *)_2)[2] = _metric_addr_7815;
    _4264 = MAKE_SEQ(_1);
    _11free(_4264);
    _4264 = NOVALUE;

    /** 	return result */
    DeRef(_disk_path_7811);
    DeRef(_path_addr_7814);
    DeRef(_metric_addr_7815);
    return _result_7812;
    ;
}


int _8disk_size(int _disk_path_7837)
{
    int _disk_size_7838 = NOVALUE;
    int _result_7840 = NOVALUE;
    int _bytes_per_cluster_7841 = NOVALUE;
    int _4277 = NOVALUE;
    int _4276 = NOVALUE;
    int _4275 = NOVALUE;
    int _4274 = NOVALUE;
    int _4273 = NOVALUE;
    int _4272 = NOVALUE;
    int _4271 = NOVALUE;
    int _4269 = NOVALUE;
    int _4268 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence disk_size = {0,0,0, disk_path}*/
    _0 = _disk_size_7838;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    Ref(_disk_path_7837);
    *((int *)(_2+16)) = _disk_path_7837;
    _disk_size_7838 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	ifdef WINDOWS then*/

    /** 		sequence result */

    /** 		atom bytes_per_cluster*/

    /** 		result = disk_metrics(disk_path) */
    Ref(_disk_path_7837);
    _0 = _result_7840;
    _result_7840 = _8disk_metrics(_disk_path_7837);
    DeRef(_0);

    /** 		bytes_per_cluster = result[BYTES_PER_SECTOR] * result[SECTORS_PER_CLUSTER]*/
    _2 = (int)SEQ_PTR(_result_7840);
    _4268 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_result_7840);
    _4269 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_bytes_per_cluster_7841);
    if (IS_ATOM_INT(_4268) && IS_ATOM_INT(_4269)) {
        if (_4268 == (short)_4268 && _4269 <= INT15 && _4269 >= -INT15)
        _bytes_per_cluster_7841 = _4268 * _4269;
        else
        _bytes_per_cluster_7841 = NewDouble(_4268 * (double)_4269);
    }
    else {
        _bytes_per_cluster_7841 = binary_op(MULTIPLY, _4268, _4269);
    }
    _4268 = NOVALUE;
    _4269 = NOVALUE;

    /** 		disk_size[TOTAL_BYTES] = bytes_per_cluster * result[TOTAL_NUMBER_OF_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_7840);
    _4271 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_bytes_per_cluster_7841) && IS_ATOM_INT(_4271)) {
        if (_bytes_per_cluster_7841 == (short)_bytes_per_cluster_7841 && _4271 <= INT15 && _4271 >= -INT15)
        _4272 = _bytes_per_cluster_7841 * _4271;
        else
        _4272 = NewDouble(_bytes_per_cluster_7841 * (double)_4271);
    }
    else {
        _4272 = binary_op(MULTIPLY, _bytes_per_cluster_7841, _4271);
    }
    _4271 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_7838);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_7838 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _4272;
    if( _1 != _4272 ){
        DeRef(_1);
    }
    _4272 = NOVALUE;

    /** 		disk_size[FREE_BYTES]  = bytes_per_cluster * result[NUMBER_OF_FREE_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_7840);
    _4273 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_bytes_per_cluster_7841) && IS_ATOM_INT(_4273)) {
        if (_bytes_per_cluster_7841 == (short)_bytes_per_cluster_7841 && _4273 <= INT15 && _4273 >= -INT15)
        _4274 = _bytes_per_cluster_7841 * _4273;
        else
        _4274 = NewDouble(_bytes_per_cluster_7841 * (double)_4273);
    }
    else {
        _4274 = binary_op(MULTIPLY, _bytes_per_cluster_7841, _4273);
    }
    _4273 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_7838);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_7838 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4274;
    if( _1 != _4274 ){
        DeRef(_1);
    }
    _4274 = NOVALUE;

    /** 		disk_size[USED_BYTES]  = disk_size[TOTAL_BYTES] - disk_size[FREE_BYTES] */
    _2 = (int)SEQ_PTR(_disk_size_7838);
    _4275 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_disk_size_7838);
    _4276 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4275) && IS_ATOM_INT(_4276)) {
        _4277 = _4275 - _4276;
        if ((long)((unsigned long)_4277 +(unsigned long) HIGH_BITS) >= 0){
            _4277 = NewDouble((double)_4277);
        }
    }
    else {
        _4277 = binary_op(MINUS, _4275, _4276);
    }
    _4275 = NOVALUE;
    _4276 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_7838);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_7838 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4277;
    if( _1 != _4277 ){
        DeRef(_1);
    }
    _4277 = NOVALUE;

    /** 	return disk_size */
    DeRef(_disk_path_7837);
    DeRefDS(_result_7840);
    DeRef(_bytes_per_cluster_7841);
    return _disk_size_7838;
    ;
}


int _8count_files(int _orig_path_7862, int _dir_info_7863, int _inst_7864)
{
    int _pos_7865 = NOVALUE;
    int _ext_7866 = NOVALUE;
    int _fileext_inlined_fileext_at_245_7909 = NOVALUE;
    int _data_inlined_fileext_at_245_7908 = NOVALUE;
    int _path_inlined_fileext_at_242_7907 = NOVALUE;
    int _4347 = NOVALUE;
    int _4346 = NOVALUE;
    int _4345 = NOVALUE;
    int _4343 = NOVALUE;
    int _4342 = NOVALUE;
    int _4341 = NOVALUE;
    int _4340 = NOVALUE;
    int _4338 = NOVALUE;
    int _4337 = NOVALUE;
    int _4335 = NOVALUE;
    int _4334 = NOVALUE;
    int _4333 = NOVALUE;
    int _4332 = NOVALUE;
    int _4331 = NOVALUE;
    int _4330 = NOVALUE;
    int _4329 = NOVALUE;
    int _4327 = NOVALUE;
    int _4326 = NOVALUE;
    int _4324 = NOVALUE;
    int _4323 = NOVALUE;
    int _4322 = NOVALUE;
    int _4321 = NOVALUE;
    int _4320 = NOVALUE;
    int _4319 = NOVALUE;
    int _4318 = NOVALUE;
    int _4317 = NOVALUE;
    int _4316 = NOVALUE;
    int _4315 = NOVALUE;
    int _4314 = NOVALUE;
    int _4313 = NOVALUE;
    int _4312 = NOVALUE;
    int _4311 = NOVALUE;
    int _4309 = NOVALUE;
    int _4308 = NOVALUE;
    int _4307 = NOVALUE;
    int _4306 = NOVALUE;
    int _4304 = NOVALUE;
    int _4303 = NOVALUE;
    int _4302 = NOVALUE;
    int _4301 = NOVALUE;
    int _4300 = NOVALUE;
    int _4299 = NOVALUE;
    int _4298 = NOVALUE;
    int _4296 = NOVALUE;
    int _4295 = NOVALUE;
    int _4294 = NOVALUE;
    int _4293 = NOVALUE;
    int _4292 = NOVALUE;
    int _4291 = NOVALUE;
    int _4288 = NOVALUE;
    int _4287 = NOVALUE;
    int _4286 = NOVALUE;
    int _4285 = NOVALUE;
    int _4284 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer pos = 0*/
    _pos_7865 = 0;

    /** 	orig_path = orig_path*/
    RefDS(_orig_path_7862);
    DeRefDS(_orig_path_7862);
    _orig_path_7862 = _orig_path_7862;

    /** 	if equal(dir_info[D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4284 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4284 == _3512)
    _4285 = 1;
    else if (IS_ATOM_INT(_4284) && IS_ATOM_INT(_3512))
    _4285 = 0;
    else
    _4285 = (compare(_4284, _3512) == 0);
    _4284 = NOVALUE;
    if (_4285 == 0)
    {
        _4285 = NOVALUE;
        goto L1; // [33] 43
    }
    else{
        _4285 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_7862);
    DeRefDS(_dir_info_7863);
    DeRefDS(_inst_7864);
    DeRef(_ext_7866);
    return 0;
L1: 

    /** 	if equal(dir_info[D_NAME], "..") then*/
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4286 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4286 == _3543)
    _4287 = 1;
    else if (IS_ATOM_INT(_4286) && IS_ATOM_INT(_3543))
    _4287 = 0;
    else
    _4287 = (compare(_4286, _3543) == 0);
    _4286 = NOVALUE;
    if (_4287 == 0)
    {
        _4287 = NOVALUE;
        goto L2; // [55] 65
    }
    else{
        _4287 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_7862);
    DeRefDS(_dir_info_7863);
    DeRefDS(_inst_7864);
    DeRef(_ext_7866);
    return 0;
L2: 

    /** 	if inst[1] = 0 then -- count all is false*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4288 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _4288, 0)){
        _4288 = NOVALUE;
        goto L3; // [71] 122
    }
    _4288 = NOVALUE;

    /** 		if find('h', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4291 = (int)*(((s1_ptr)_2)->base + 2);
    _4292 = find_from(104, _4291, 1);
    _4291 = NOVALUE;
    if (_4292 == 0)
    {
        _4292 = NOVALUE;
        goto L4; // [88] 98
    }
    else{
        _4292 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_7862);
    DeRefDS(_dir_info_7863);
    DeRefDS(_inst_7864);
    DeRef(_ext_7866);
    return 0;
L4: 

    /** 		if find('s', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4293 = (int)*(((s1_ptr)_2)->base + 2);
    _4294 = find_from(115, _4293, 1);
    _4293 = NOVALUE;
    if (_4294 == 0)
    {
        _4294 = NOVALUE;
        goto L5; // [111] 121
    }
    else{
        _4294 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_7862);
    DeRefDS(_dir_info_7863);
    DeRefDS(_inst_7864);
    DeRef(_ext_7866);
    return 0;
L5: 
L3: 

    /** 	file_counters[inst[2]][COUNT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4295 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_7859 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4295))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4295)->dbl));
    else
    _3 = (int)(_4295 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4298 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4299 = (int)*(((s1_ptr)_2)->base + 3);
    _4296 = NOVALUE;
    if (IS_ATOM_INT(_4299) && IS_ATOM_INT(_4298)) {
        _4300 = _4299 + _4298;
        if ((long)((unsigned long)_4300 + (unsigned long)HIGH_BITS) >= 0) 
        _4300 = NewDouble((double)_4300);
    }
    else {
        _4300 = binary_op(PLUS, _4299, _4298);
    }
    _4299 = NOVALUE;
    _4298 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4300;
    if( _1 != _4300 ){
        DeRef(_1);
    }
    _4300 = NOVALUE;
    _4296 = NOVALUE;

    /** 	if find('d', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4301 = (int)*(((s1_ptr)_2)->base + 2);
    _4302 = find_from(100, _4301, 1);
    _4301 = NOVALUE;
    if (_4302 == 0)
    {
        _4302 = NOVALUE;
        goto L6; // [168] 201
    }
    else{
        _4302 = NOVALUE;
    }

    /** 		file_counters[inst[2]][COUNT_DIRS] += 1*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4303 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_7859 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4303))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4303)->dbl));
    else
    _3 = (int)(_4303 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4306 = (int)*(((s1_ptr)_2)->base + 1);
    _4304 = NOVALUE;
    if (IS_ATOM_INT(_4306)) {
        _4307 = _4306 + 1;
        if (_4307 > MAXINT){
            _4307 = NewDouble((double)_4307);
        }
    }
    else
    _4307 = binary_op(PLUS, 1, _4306);
    _4306 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _4307;
    if( _1 != _4307 ){
        DeRef(_1);
    }
    _4307 = NOVALUE;
    _4304 = NOVALUE;
    goto L7; // [198] 512
L6: 

    /** 		file_counters[inst[2]][COUNT_FILES] += 1*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4308 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_7859 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4308))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4308)->dbl));
    else
    _3 = (int)(_4308 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4311 = (int)*(((s1_ptr)_2)->base + 2);
    _4309 = NOVALUE;
    if (IS_ATOM_INT(_4311)) {
        _4312 = _4311 + 1;
        if (_4312 > MAXINT){
            _4312 = NewDouble((double)_4312);
        }
    }
    else
    _4312 = binary_op(PLUS, 1, _4311);
    _4311 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4312;
    if( _1 != _4312 ){
        DeRef(_1);
    }
    _4312 = NOVALUE;
    _4309 = NOVALUE;

    /** 		ifdef not UNIX then*/

    /** 			ext = fileext(lower(dir_info[D_NAME]))*/
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4313 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_4313);
    _4314 = _6lower(_4313);
    _4313 = NOVALUE;
    DeRef(_path_inlined_fileext_at_242_7907);
    _path_inlined_fileext_at_242_7907 = _4314;
    _4314 = NOVALUE;

    /** 	data = pathinfo(path)*/
    Ref(_path_inlined_fileext_at_242_7907);
    _0 = _data_inlined_fileext_at_245_7908;
    _data_inlined_fileext_at_245_7908 = _8pathinfo(_path_inlined_fileext_at_242_7907, 0);
    DeRef(_0);

    /** 	return data[4]*/
    DeRef(_ext_7866);
    _2 = (int)SEQ_PTR(_data_inlined_fileext_at_245_7908);
    _ext_7866 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_ext_7866);
    DeRef(_path_inlined_fileext_at_242_7907);
    _path_inlined_fileext_at_242_7907 = NOVALUE;
    DeRef(_data_inlined_fileext_at_245_7908);
    _data_inlined_fileext_at_245_7908 = NOVALUE;

    /** 		pos = 0*/
    _pos_7865 = 0;

    /** 		for i = 1 to length(file_counters[inst[2]][COUNT_TYPES]) do*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4315 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!IS_ATOM_INT(_4315)){
        _4316 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4315)->dbl));
    }
    else{
        _4316 = (int)*(((s1_ptr)_2)->base + _4315);
    }
    _2 = (int)SEQ_PTR(_4316);
    _4317 = (int)*(((s1_ptr)_2)->base + 4);
    _4316 = NOVALUE;
    if (IS_SEQUENCE(_4317)){
            _4318 = SEQ_PTR(_4317)->length;
    }
    else {
        _4318 = 1;
    }
    _4317 = NOVALUE;
    {
        int _i_7911;
        _i_7911 = 1;
L8: 
        if (_i_7911 > _4318){
            goto L9; // [295] 358
        }

        /** 			if equal(file_counters[inst[2]][COUNT_TYPES][i][EXT_NAME], ext) then*/
        _2 = (int)SEQ_PTR(_inst_7864);
        _4319 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_8file_counters_7859);
        if (!IS_ATOM_INT(_4319)){
            _4320 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4319)->dbl));
        }
        else{
            _4320 = (int)*(((s1_ptr)_2)->base + _4319);
        }
        _2 = (int)SEQ_PTR(_4320);
        _4321 = (int)*(((s1_ptr)_2)->base + 4);
        _4320 = NOVALUE;
        _2 = (int)SEQ_PTR(_4321);
        _4322 = (int)*(((s1_ptr)_2)->base + _i_7911);
        _4321 = NOVALUE;
        _2 = (int)SEQ_PTR(_4322);
        _4323 = (int)*(((s1_ptr)_2)->base + 1);
        _4322 = NOVALUE;
        if (_4323 == _ext_7866)
        _4324 = 1;
        else if (IS_ATOM_INT(_4323) && IS_ATOM_INT(_ext_7866))
        _4324 = 0;
        else
        _4324 = (compare(_4323, _ext_7866) == 0);
        _4323 = NOVALUE;
        if (_4324 == 0)
        {
            _4324 = NOVALUE;
            goto LA; // [336] 351
        }
        else{
            _4324 = NOVALUE;
        }

        /** 				pos = i*/
        _pos_7865 = _i_7911;

        /** 				exit*/
        goto L9; // [348] 358
LA: 

        /** 		end for*/
        _i_7911 = _i_7911 + 1;
        goto L8; // [353] 302
L9: 
        ;
    }

    /** 		if pos = 0 then*/
    if (_pos_7865 != 0)
    goto LB; // [360] 427

    /** 			file_counters[inst[2]][COUNT_TYPES] &= {{ext, 0, 0}}*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4326 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_7859 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4326))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4326)->dbl));
    else
    _3 = (int)(_4326 + ((s1_ptr)_2)->base);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ext_7866);
    *((int *)(_2+4)) = _ext_7866;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    _4329 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4329;
    _4330 = MAKE_SEQ(_1);
    _4329 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4331 = (int)*(((s1_ptr)_2)->base + 4);
    _4327 = NOVALUE;
    if (IS_SEQUENCE(_4331) && IS_ATOM(_4330)) {
    }
    else if (IS_ATOM(_4331) && IS_SEQUENCE(_4330)) {
        Ref(_4331);
        Prepend(&_4332, _4330, _4331);
    }
    else {
        Concat((object_ptr)&_4332, _4331, _4330);
        _4331 = NOVALUE;
    }
    _4331 = NOVALUE;
    DeRefDS(_4330);
    _4330 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _4332;
    if( _1 != _4332 ){
        DeRef(_1);
    }
    _4332 = NOVALUE;
    _4327 = NOVALUE;

    /** 			pos = length(file_counters[inst[2]][COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4333 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!IS_ATOM_INT(_4333)){
        _4334 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4333)->dbl));
    }
    else{
        _4334 = (int)*(((s1_ptr)_2)->base + _4333);
    }
    _2 = (int)SEQ_PTR(_4334);
    _4335 = (int)*(((s1_ptr)_2)->base + 4);
    _4334 = NOVALUE;
    if (IS_SEQUENCE(_4335)){
            _pos_7865 = SEQ_PTR(_4335)->length;
    }
    else {
        _pos_7865 = 1;
    }
    _4335 = NOVALUE;
LB: 

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4337 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_7859 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4337))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4337)->dbl));
    else
    _3 = (int)(_4337 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4338 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_7865 + ((s1_ptr)_2)->base);
    _4338 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4340 = (int)*(((s1_ptr)_2)->base + 2);
    _4338 = NOVALUE;
    if (IS_ATOM_INT(_4340)) {
        _4341 = _4340 + 1;
        if (_4341 > MAXINT){
            _4341 = NewDouble((double)_4341);
        }
    }
    else
    _4341 = binary_op(PLUS, 1, _4340);
    _4340 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4341;
    if( _1 != _4341 ){
        DeRef(_1);
    }
    _4341 = NOVALUE;
    _4338 = NOVALUE;

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_7864);
    _4342 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_7859 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4342))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4342)->dbl));
    else
    _3 = (int)(_4342 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4343 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_7865 + ((s1_ptr)_2)->base);
    _4343 = NOVALUE;
    _2 = (int)SEQ_PTR(_dir_info_7863);
    _4345 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4346 = (int)*(((s1_ptr)_2)->base + 3);
    _4343 = NOVALUE;
    if (IS_ATOM_INT(_4346) && IS_ATOM_INT(_4345)) {
        _4347 = _4346 + _4345;
        if ((long)((unsigned long)_4347 + (unsigned long)HIGH_BITS) >= 0) 
        _4347 = NewDouble((double)_4347);
    }
    else {
        _4347 = binary_op(PLUS, _4346, _4345);
    }
    _4346 = NOVALUE;
    _4345 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4347;
    if( _1 != _4347 ){
        DeRef(_1);
    }
    _4347 = NOVALUE;
    _4343 = NOVALUE;
L7: 

    /** 	return 0*/
    DeRefDS(_orig_path_7862);
    DeRefDS(_dir_info_7863);
    DeRefDS(_inst_7864);
    DeRef(_ext_7866);
    _4295 = NOVALUE;
    _4303 = NOVALUE;
    _4308 = NOVALUE;
    _4315 = NOVALUE;
    _4317 = NOVALUE;
    _4319 = NOVALUE;
    _4326 = NOVALUE;
    _4333 = NOVALUE;
    _4335 = NOVALUE;
    _4337 = NOVALUE;
    _4342 = NOVALUE;
    return 0;
    ;
}


int _8dir_size(int _dir_path_7949, int _count_all_7950)
{
    int _fc_7951 = NOVALUE;
    int _4362 = NOVALUE;
    int _4361 = NOVALUE;
    int _4359 = NOVALUE;
    int _4358 = NOVALUE;
    int _4356 = NOVALUE;
    int _4355 = NOVALUE;
    int _4354 = NOVALUE;
    int _4353 = NOVALUE;
    int _4352 = NOVALUE;
    int _4351 = NOVALUE;
    int _4348 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_all_7950)) {
        _1 = (long)(DBL_PTR(_count_all_7950)->dbl);
        if (UNIQUE(DBL_PTR(_count_all_7950)) && (DBL_PTR(_count_all_7950)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_all_7950);
        _count_all_7950 = _1;
    }

    /** 	file_counters = append(file_counters, {0,0,0,{}})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    RefDS(_5);
    *((int *)(_2+16)) = _5;
    _4348 = MAKE_SEQ(_1);
    RefDS(_4348);
    Append(&_8file_counters_7859, _8file_counters_7859, _4348);
    DeRefDS(_4348);
    _4348 = NOVALUE;

    /** 	walk_dir(dir_path, {routine_id("count_files"), {count_all, length(file_counters)}}, 0)*/
    _4351 = CRoutineId(326, 8, _4350);
    if (IS_SEQUENCE(_8file_counters_7859)){
            _4352 = SEQ_PTR(_8file_counters_7859)->length;
    }
    else {
        _4352 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _count_all_7950;
    ((int *)_2)[2] = _4352;
    _4353 = MAKE_SEQ(_1);
    _4352 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4351;
    ((int *)_2)[2] = _4353;
    _4354 = MAKE_SEQ(_1);
    _4353 = NOVALUE;
    _4351 = NOVALUE;
    RefDS(_dir_path_7949);
    _4355 = _8walk_dir(_dir_path_7949, _4354, 0, -99999);
    _4354 = NOVALUE;

    /** 	fc = file_counters[$]*/
    if (IS_SEQUENCE(_8file_counters_7859)){
            _4356 = SEQ_PTR(_8file_counters_7859)->length;
    }
    else {
        _4356 = 1;
    }
    DeRef(_fc_7951);
    _2 = (int)SEQ_PTR(_8file_counters_7859);
    _fc_7951 = (int)*(((s1_ptr)_2)->base + _4356);
    RefDS(_fc_7951);

    /** 	file_counters = file_counters[1 .. $-1]*/
    if (IS_SEQUENCE(_8file_counters_7859)){
            _4358 = SEQ_PTR(_8file_counters_7859)->length;
    }
    else {
        _4358 = 1;
    }
    _4359 = _4358 - 1;
    _4358 = NOVALUE;
    rhs_slice_target = (object_ptr)&_8file_counters_7859;
    RHS_Slice(_8file_counters_7859, 1, _4359);

    /** 	fc[COUNT_TYPES] = stdsort:sort(fc[COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_fc_7951);
    _4361 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_4361);
    _4362 = _21sort(_4361, 1);
    _4361 = NOVALUE;
    _2 = (int)SEQ_PTR(_fc_7951);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _fc_7951 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _4362;
    if( _1 != _4362 ){
        DeRef(_1);
    }
    _4362 = NOVALUE;

    /** 	return fc*/
    DeRefDS(_dir_path_7949);
    _4359 = NOVALUE;
    DeRef(_4355);
    _4355 = NOVALUE;
    return _fc_7951;
    ;
}


int _8temp_file(int _temp_location_7969, int _temp_prefix_7970, int _temp_extn_7971, int _reserve_temp_7973)
{
    int _randname_7974 = NOVALUE;
    int _envtmp_7978 = NOVALUE;
    int _tdir_7997 = NOVALUE;
    int _4401 = NOVALUE;
    int _4399 = NOVALUE;
    int _4397 = NOVALUE;
    int _4395 = NOVALUE;
    int _4393 = NOVALUE;
    int _4392 = NOVALUE;
    int _4391 = NOVALUE;
    int _4387 = NOVALUE;
    int _4386 = NOVALUE;
    int _4385 = NOVALUE;
    int _4384 = NOVALUE;
    int _4381 = NOVALUE;
    int _4380 = NOVALUE;
    int _4379 = NOVALUE;
    int _4374 = NOVALUE;
    int _4371 = NOVALUE;
    int _4368 = NOVALUE;
    int _4364 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_reserve_temp_7973)) {
        _1 = (long)(DBL_PTR(_reserve_temp_7973)->dbl);
        if (UNIQUE(DBL_PTR(_reserve_temp_7973)) && (DBL_PTR(_reserve_temp_7973)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_reserve_temp_7973);
        _reserve_temp_7973 = _1;
    }

    /** 	if length(temp_location) = 0 then*/
    if (IS_SEQUENCE(_temp_location_7969)){
            _4364 = SEQ_PTR(_temp_location_7969)->length;
    }
    else {
        _4364 = 1;
    }
    if (_4364 != 0)
    goto L1; // [16] 69

    /** 		object envtmp*/

    /** 		envtmp = getenv("TEMP")*/
    DeRefi(_envtmp_7978);
    _envtmp_7978 = EGetEnv(_4366);

    /** 		if atom(envtmp) then*/
    _4368 = IS_ATOM(_envtmp_7978);
    if (_4368 == 0)
    {
        _4368 = NOVALUE;
        goto L2; // [32] 41
    }
    else{
        _4368 = NOVALUE;
    }

    /** 			envtmp = getenv("TMP")*/
    DeRefi(_envtmp_7978);
    _envtmp_7978 = EGetEnv(_4369);
L2: 

    /** 		ifdef WINDOWS then			*/

    /** 			if atom(envtmp) then*/
    _4371 = IS_ATOM(_envtmp_7978);
    if (_4371 == 0)
    {
        _4371 = NOVALUE;
        goto L3; // [48] 57
    }
    else{
        _4371 = NOVALUE;
    }

    /** 				envtmp = "C:\\temp\\"*/
    RefDS(_4372);
    DeRefi(_envtmp_7978);
    _envtmp_7978 = _4372;
L3: 

    /** 		temp_location = envtmp*/
    Ref(_envtmp_7978);
    DeRefDS(_temp_location_7969);
    _temp_location_7969 = _envtmp_7978;
    DeRefi(_envtmp_7978);
    _envtmp_7978 = NOVALUE;
    goto L4; // [66] 163
L1: 

    /** 		switch file_type(temp_location) do*/
    RefDS(_temp_location_7969);
    _4374 = _8file_type(_temp_location_7969);
    if (IS_SEQUENCE(_4374) ){
        goto L5; // [75] 152
    }
    if(!IS_ATOM_INT(_4374)){
        if( (DBL_PTR(_4374)->dbl != (double) ((int) DBL_PTR(_4374)->dbl) ) ){
            goto L5; // [75] 152
        }
        _0 = (int) DBL_PTR(_4374)->dbl;
    }
    else {
        _0 = _4374;
    };
    DeRef(_4374);
    _4374 = NOVALUE;
    switch ( _0 ){ 

        /** 			case FILETYPE_FILE then*/
        case 1:

        /** 				temp_location = dirname(temp_location, 1)*/
        RefDS(_temp_location_7969);
        _0 = _temp_location_7969;
        _temp_location_7969 = _8dirname(_temp_location_7969, 1);
        DeRefDS(_0);
        goto L6; // [93] 162

        /** 			case FILETYPE_DIRECTORY then*/
        case 2:

        /** 				temp_location = temp_location*/
        RefDS(_temp_location_7969);
        DeRefDS(_temp_location_7969);
        _temp_location_7969 = _temp_location_7969;
        goto L6; // [106] 162

        /** 			case FILETYPE_NOT_FOUND then*/
        case 0:

        /** 				object tdir = dirname(temp_location, 1)*/
        RefDS(_temp_location_7969);
        _0 = _tdir_7997;
        _tdir_7997 = _8dirname(_temp_location_7969, 1);
        DeRef(_0);

        /** 				if file_exists(tdir) then*/
        Ref(_tdir_7997);
        _4379 = _8file_exists(_tdir_7997);
        if (_4379 == 0) {
            DeRef(_4379);
            _4379 = NOVALUE;
            goto L7; // [125] 138
        }
        else {
            if (!IS_ATOM_INT(_4379) && DBL_PTR(_4379)->dbl == 0.0){
                DeRef(_4379);
                _4379 = NOVALUE;
                goto L7; // [125] 138
            }
            DeRef(_4379);
            _4379 = NOVALUE;
        }
        DeRef(_4379);
        _4379 = NOVALUE;

        /** 					temp_location = tdir*/
        Ref(_tdir_7997);
        DeRefDS(_temp_location_7969);
        _temp_location_7969 = _tdir_7997;
        goto L8; // [135] 146
L7: 

        /** 					temp_location = "."*/
        RefDS(_3512);
        DeRefDS(_temp_location_7969);
        _temp_location_7969 = _3512;
L8: 
        DeRef(_tdir_7997);
        _tdir_7997 = NOVALUE;
        goto L6; // [148] 162

        /** 			case else*/
        default:
L5: 

        /** 				temp_location = "."*/
        RefDS(_3512);
        DeRefDS(_temp_location_7969);
        _temp_location_7969 = _3512;
    ;}L6: 
L4: 

    /** 	if temp_location[$] != SLASH then*/
    if (IS_SEQUENCE(_temp_location_7969)){
            _4380 = SEQ_PTR(_temp_location_7969)->length;
    }
    else {
        _4380 = 1;
    }
    _2 = (int)SEQ_PTR(_temp_location_7969);
    _4381 = (int)*(((s1_ptr)_2)->base + _4380);
    if (binary_op_a(EQUALS, _4381, 92)){
        _4381 = NOVALUE;
        goto L9; // [172] 183
    }
    _4381 = NOVALUE;

    /** 		temp_location &= SLASH*/
    Append(&_temp_location_7969, _temp_location_7969, 92);
L9: 

    /** 	if length(temp_extn) and temp_extn[1] != '.' then*/
    if (IS_SEQUENCE(_temp_extn_7971)){
            _4384 = SEQ_PTR(_temp_extn_7971)->length;
    }
    else {
        _4384 = 1;
    }
    if (_4384 == 0) {
        goto LA; // [188] 211
    }
    _2 = (int)SEQ_PTR(_temp_extn_7971);
    _4386 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4386)) {
        _4387 = (_4386 != 46);
    }
    else {
        _4387 = binary_op(NOTEQ, _4386, 46);
    }
    _4386 = NOVALUE;
    if (_4387 == 0) {
        DeRef(_4387);
        _4387 = NOVALUE;
        goto LA; // [201] 211
    }
    else {
        if (!IS_ATOM_INT(_4387) && DBL_PTR(_4387)->dbl == 0.0){
            DeRef(_4387);
            _4387 = NOVALUE;
            goto LA; // [201] 211
        }
        DeRef(_4387);
        _4387 = NOVALUE;
    }
    DeRef(_4387);
    _4387 = NOVALUE;

    /** 		temp_extn = '.' & temp_extn*/
    Prepend(&_temp_extn_7971, _temp_extn_7971, 46);
LA: 

    /** 	while 1 do*/
LB: 

    /** 		randname = sprintf("%s%s%06d%s", {temp_location, temp_prefix, rand(1_000_000) - 1, temp_extn})*/
    _4391 = good_rand() % ((unsigned)1000000) + 1;
    _4392 = _4391 - 1;
    _4391 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_temp_location_7969);
    *((int *)(_2+4)) = _temp_location_7969;
    RefDS(_temp_prefix_7970);
    *((int *)(_2+8)) = _temp_prefix_7970;
    *((int *)(_2+12)) = _4392;
    RefDS(_temp_extn_7971);
    *((int *)(_2+16)) = _temp_extn_7971;
    _4393 = MAKE_SEQ(_1);
    _4392 = NOVALUE;
    DeRefi(_randname_7974);
    _randname_7974 = EPrintf(-9999999, _4389, _4393);
    DeRefDS(_4393);
    _4393 = NOVALUE;

    /** 		if not file_exists( randname ) then*/
    RefDS(_randname_7974);
    _4395 = _8file_exists(_randname_7974);
    if (IS_ATOM_INT(_4395)) {
        if (_4395 != 0){
            DeRef(_4395);
            _4395 = NOVALUE;
            goto LB; // [242] 216
        }
    }
    else {
        if (DBL_PTR(_4395)->dbl != 0.0){
            DeRef(_4395);
            _4395 = NOVALUE;
            goto LB; // [242] 216
        }
    }
    DeRef(_4395);
    _4395 = NOVALUE;

    /** 			exit*/
    goto LC; // [247] 255

    /** 	end while*/
    goto LB; // [252] 216
LC: 

    /** 	if reserve_temp then*/
    if (_reserve_temp_7973 == 0)
    {
        goto LD; // [257] 302
    }
    else{
    }

    /** 		if not file_exists(temp_location) then*/
    RefDS(_temp_location_7969);
    _4397 = _8file_exists(_temp_location_7969);
    if (IS_ATOM_INT(_4397)) {
        if (_4397 != 0){
            DeRef(_4397);
            _4397 = NOVALUE;
            goto LE; // [266] 289
        }
    }
    else {
        if (DBL_PTR(_4397)->dbl != 0.0){
            DeRef(_4397);
            _4397 = NOVALUE;
            goto LE; // [266] 289
        }
    }
    DeRef(_4397);
    _4397 = NOVALUE;

    /** 			if create_directory(temp_location) = 0 then*/
    RefDS(_temp_location_7969);
    _4399 = _8create_directory(_temp_location_7969, 448, 1);
    if (binary_op_a(NOTEQ, _4399, 0)){
        DeRef(_4399);
        _4399 = NOVALUE;
        goto LF; // [277] 288
    }
    DeRef(_4399);
    _4399 = NOVALUE;

    /** 				return ""*/
    RefDS(_5);
    DeRefDS(_temp_location_7969);
    DeRefDS(_temp_prefix_7970);
    DeRefDS(_temp_extn_7971);
    DeRefi(_randname_7974);
    return _5;
LF: 
LE: 

    /** 		io:write_file(randname, "")*/
    RefDS(_randname_7974);
    RefDS(_5);
    _4401 = _15write_file(_randname_7974, _5, 1);
LD: 

    /** 	return randname*/
    DeRefDS(_temp_location_7969);
    DeRefDS(_temp_prefix_7970);
    DeRefDS(_temp_extn_7971);
    DeRef(_4401);
    _4401 = NOVALUE;
    return _randname_7974;
    ;
}


int _8checksum(int _filename_8034, int _size_8035, int _usename_8036, int _return_text_8037)
{
    int _fn_8038 = NOVALUE;
    int _cs_8039 = NOVALUE;
    int _hits_8040 = NOVALUE;
    int _ix_8041 = NOVALUE;
    int _jx_8042 = NOVALUE;
    int _fx_8043 = NOVALUE;
    int _data_8044 = NOVALUE;
    int _nhit_8045 = NOVALUE;
    int _nmiss_8046 = NOVALUE;
    int _cs_text_8118 = NOVALUE;
    int _4461 = NOVALUE;
    int _4459 = NOVALUE;
    int _4458 = NOVALUE;
    int _4456 = NOVALUE;
    int _4454 = NOVALUE;
    int _4453 = NOVALUE;
    int _4452 = NOVALUE;
    int _4451 = NOVALUE;
    int _4450 = NOVALUE;
    int _4448 = NOVALUE;
    int _4446 = NOVALUE;
    int _4444 = NOVALUE;
    int _4443 = NOVALUE;
    int _4442 = NOVALUE;
    int _4441 = NOVALUE;
    int _4440 = NOVALUE;
    int _4439 = NOVALUE;
    int _4438 = NOVALUE;
    int _4436 = NOVALUE;
    int _4433 = NOVALUE;
    int _4431 = NOVALUE;
    int _4430 = NOVALUE;
    int _4429 = NOVALUE;
    int _4428 = NOVALUE;
    int _4427 = NOVALUE;
    int _4424 = NOVALUE;
    int _4423 = NOVALUE;
    int _4422 = NOVALUE;
    int _4421 = NOVALUE;
    int _4419 = NOVALUE;
    int _4416 = NOVALUE;
    int _4414 = NOVALUE;
    int _4410 = NOVALUE;
    int _4409 = NOVALUE;
    int _4408 = NOVALUE;
    int _4407 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_8035)) {
        _1 = (long)(DBL_PTR(_size_8035)->dbl);
        if (UNIQUE(DBL_PTR(_size_8035)) && (DBL_PTR(_size_8035)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_8035);
        _size_8035 = _1;
    }
    if (!IS_ATOM_INT(_usename_8036)) {
        _1 = (long)(DBL_PTR(_usename_8036)->dbl);
        if (UNIQUE(DBL_PTR(_usename_8036)) && (DBL_PTR(_usename_8036)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_usename_8036);
        _usename_8036 = _1;
    }
    if (!IS_ATOM_INT(_return_text_8037)) {
        _1 = (long)(DBL_PTR(_return_text_8037)->dbl);
        if (UNIQUE(DBL_PTR(_return_text_8037)) && (DBL_PTR(_return_text_8037)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_return_text_8037);
        _return_text_8037 = _1;
    }

    /** 	if size <= 0 then*/
    if (_size_8035 > 0)
    goto L1; // [17] 28

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_8034);
    DeRef(_cs_8039);
    DeRef(_hits_8040);
    DeRef(_jx_8042);
    DeRef(_fx_8043);
    DeRefi(_data_8044);
    return _5;
L1: 

    /** 	fn = open(filename, "rb")*/
    _fn_8038 = EOpen(_filename_8034, _904, 0);

    /** 	if fn = -1 then*/
    if (_fn_8038 != -1)
    goto L2; // [39] 50

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_8034);
    DeRef(_cs_8039);
    DeRef(_hits_8040);
    DeRef(_jx_8042);
    DeRef(_fx_8043);
    DeRefi(_data_8044);
    return _5;
L2: 

    /** 	jx = file_length(filename)*/
    RefDS(_filename_8034);
    _0 = _jx_8042;
    _jx_8042 = _8file_length(_filename_8034);
    DeRef(_0);

    /** 	cs = repeat(jx, size)*/
    DeRef(_cs_8039);
    _cs_8039 = Repeat(_jx_8042, _size_8035);

    /** 	for i = 1 to size do*/
    _4407 = _size_8035;
    {
        int _i_8055;
        _i_8055 = 1;
L3: 
        if (_i_8055 > _4407){
            goto L4; // [67] 99
        }

        /** 		cs[i] = hash(i + size, cs[i])*/
        _4408 = _i_8055 + _size_8035;
        if ((long)((unsigned long)_4408 + (unsigned long)HIGH_BITS) >= 0) 
        _4408 = NewDouble((double)_4408);
        _2 = (int)SEQ_PTR(_cs_8039);
        _4409 = (int)*(((s1_ptr)_2)->base + _i_8055);
        _4410 = calc_hash(_4408, _4409);
        DeRef(_4408);
        _4408 = NOVALUE;
        _4409 = NOVALUE;
        _2 = (int)SEQ_PTR(_cs_8039);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _cs_8039 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8055);
        _1 = *(int *)_2;
        *(int *)_2 = _4410;
        if( _1 != _4410 ){
            DeRef(_1);
        }
        _4410 = NOVALUE;

        /** 	end for*/
        _i_8055 = _i_8055 + 1;
        goto L3; // [94] 74
L4: 
        ;
    }

    /** 	if usename != 0 then*/
    if (_usename_8036 == 0)
    goto L5; // [101] 236

    /** 		nhit = 0*/
    _nhit_8045 = 0;

    /** 		nmiss = 0*/
    _nmiss_8046 = 0;

    /** 		hits = {0,0}*/
    DeRef(_hits_8040);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _hits_8040 = MAKE_SEQ(_1);

    /** 		fx = hash(filename, stdhash:HSIEH32) -- Get a hash value for the whole name.*/
    DeRef(_fx_8043);
    _fx_8043 = calc_hash(_filename_8034, -5);

    /** 		while find(0, hits) do*/
L6: 
    _4414 = find_from(0, _hits_8040, 1);
    if (_4414 == 0)
    {
        _4414 = NOVALUE;
        goto L7; // [143] 235
    }
    else{
        _4414 = NOVALUE;
    }

    /** 			nhit += 1*/
    _nhit_8045 = _nhit_8045 + 1;

    /** 			if nhit > length(filename) then*/
    if (IS_SEQUENCE(_filename_8034)){
            _4416 = SEQ_PTR(_filename_8034)->length;
    }
    else {
        _4416 = 1;
    }
    if (_nhit_8045 <= _4416)
    goto L8; // [159] 177

    /** 				nhit = 1*/
    _nhit_8045 = 1;

    /** 				hits[1] = 1*/
    _2 = (int)SEQ_PTR(_hits_8040);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L8: 

    /** 			nmiss += 1*/
    _nmiss_8046 = _nmiss_8046 + 1;

    /** 			if nmiss > length(cs) then*/
    if (IS_SEQUENCE(_cs_8039)){
            _4419 = SEQ_PTR(_cs_8039)->length;
    }
    else {
        _4419 = 1;
    }
    if (_nmiss_8046 <= _4419)
    goto L9; // [190] 208

    /** 				nmiss = 1*/
    _nmiss_8046 = 1;

    /** 				hits[2] = 1*/
    _2 = (int)SEQ_PTR(_hits_8040);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L9: 

    /** 			cs[nmiss] = hash(filename[nhit], xor_bits(fx, cs[nmiss]))*/
    _2 = (int)SEQ_PTR(_filename_8034);
    _4421 = (int)*(((s1_ptr)_2)->base + _nhit_8045);
    _2 = (int)SEQ_PTR(_cs_8039);
    _4422 = (int)*(((s1_ptr)_2)->base + _nmiss_8046);
    if (IS_ATOM_INT(_fx_8043) && IS_ATOM_INT(_4422)) {
        {unsigned long tu;
             tu = (unsigned long)_fx_8043 ^ (unsigned long)_4422;
             _4423 = MAKE_UINT(tu);
        }
    }
    else {
        _4423 = binary_op(XOR_BITS, _fx_8043, _4422);
    }
    _4422 = NOVALUE;
    _4424 = calc_hash(_4421, _4423);
    _4421 = NOVALUE;
    DeRef(_4423);
    _4423 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_8039);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_8039 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_8046);
    _1 = *(int *)_2;
    *(int *)_2 = _4424;
    if( _1 != _4424 ){
        DeRef(_1);
    }
    _4424 = NOVALUE;

    /** 		end while -- repeat until every bucket and every character has been used.*/
    goto L6; // [232] 138
L7: 
L5: 

    /** 	hits = repeat(0, size)*/
    DeRef(_hits_8040);
    _hits_8040 = Repeat(0, _size_8035);

    /** 	if jx != 0 then*/
    if (binary_op_a(EQUALS, _jx_8042, 0)){
        goto LA; // [244] 494
    }

    /** 		data = repeat(0, remainder( hash(jx * jx / size , stdhash:HSIEH32), 8) + 7)*/
    if (IS_ATOM_INT(_jx_8042) && IS_ATOM_INT(_jx_8042)) {
        if (_jx_8042 == (short)_jx_8042 && _jx_8042 <= INT15 && _jx_8042 >= -INT15)
        _4427 = _jx_8042 * _jx_8042;
        else
        _4427 = NewDouble(_jx_8042 * (double)_jx_8042);
    }
    else {
        if (IS_ATOM_INT(_jx_8042)) {
            _4427 = NewDouble((double)_jx_8042 * DBL_PTR(_jx_8042)->dbl);
        }
        else {
            if (IS_ATOM_INT(_jx_8042)) {
                _4427 = NewDouble(DBL_PTR(_jx_8042)->dbl * (double)_jx_8042);
            }
            else
            _4427 = NewDouble(DBL_PTR(_jx_8042)->dbl * DBL_PTR(_jx_8042)->dbl);
        }
    }
    if (IS_ATOM_INT(_4427)) {
        _4428 = (_4427 % _size_8035) ? NewDouble((double)_4427 / _size_8035) : (_4427 / _size_8035);
    }
    else {
        _4428 = NewDouble(DBL_PTR(_4427)->dbl / (double)_size_8035);
    }
    DeRef(_4427);
    _4427 = NOVALUE;
    _4429 = calc_hash(_4428, -5);
    DeRef(_4428);
    _4428 = NOVALUE;
    if (IS_ATOM_INT(_4429)) {
        _4430 = (_4429 % 8);
    }
    else {
        temp_d.dbl = (double)8;
        _4430 = Dremainder(DBL_PTR(_4429), &temp_d);
    }
    DeRef(_4429);
    _4429 = NOVALUE;
    if (IS_ATOM_INT(_4430)) {
        _4431 = _4430 + 7;
    }
    else {
        _4431 = NewDouble(DBL_PTR(_4430)->dbl + (double)7);
    }
    DeRef(_4430);
    _4430 = NOVALUE;
    DeRefi(_data_8044);
    _data_8044 = Repeat(0, _4431);
    DeRef(_4431);
    _4431 = NOVALUE;

    /** 		while data[1] != -1 with entry do*/
    goto LB; // [278] 344
LC: 
    _2 = (int)SEQ_PTR(_data_8044);
    _4433 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4433 == -1)
    goto LD; // [285] 377

    /** 			jx = hash(jx, data)*/
    _0 = _jx_8042;
    _jx_8042 = calc_hash(_jx_8042, _data_8044);
    DeRef(_0);

    /** 			ix = remainder(jx, size) + 1*/
    if (IS_ATOM_INT(_jx_8042)) {
        _4436 = (_jx_8042 % _size_8035);
    }
    else {
        temp_d.dbl = (double)_size_8035;
        _4436 = Dremainder(DBL_PTR(_jx_8042), &temp_d);
    }
    if (IS_ATOM_INT(_4436)) {
        _ix_8041 = _4436 + 1;
    }
    else
    { // coercing _ix_8041 to an integer 1
        _ix_8041 = 1+(long)(DBL_PTR(_4436)->dbl);
        if( !IS_ATOM_INT(_ix_8041) ){
            _ix_8041 = (object)DBL_PTR(_ix_8041)->dbl;
        }
    }
    DeRef(_4436);
    _4436 = NOVALUE;

    /** 			cs[ix] = xor_bits(cs[ix], hash(data, stdhash:HSIEH32))*/
    _2 = (int)SEQ_PTR(_cs_8039);
    _4438 = (int)*(((s1_ptr)_2)->base + _ix_8041);
    _4439 = calc_hash(_data_8044, -5);
    if (IS_ATOM_INT(_4438) && IS_ATOM_INT(_4439)) {
        {unsigned long tu;
             tu = (unsigned long)_4438 ^ (unsigned long)_4439;
             _4440 = MAKE_UINT(tu);
        }
    }
    else {
        _4440 = binary_op(XOR_BITS, _4438, _4439);
    }
    _4438 = NOVALUE;
    DeRef(_4439);
    _4439 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_8039);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_8039 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ix_8041);
    _1 = *(int *)_2;
    *(int *)_2 = _4440;
    if( _1 != _4440 ){
        DeRef(_1);
    }
    _4440 = NOVALUE;

    /** 			hits[ix] += 1*/
    _2 = (int)SEQ_PTR(_hits_8040);
    _4441 = (int)*(((s1_ptr)_2)->base + _ix_8041);
    if (IS_ATOM_INT(_4441)) {
        _4442 = _4441 + 1;
        if (_4442 > MAXINT){
            _4442 = NewDouble((double)_4442);
        }
    }
    else
    _4442 = binary_op(PLUS, 1, _4441);
    _4441 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_8040);
    _2 = (int)(((s1_ptr)_2)->base + _ix_8041);
    _1 = *(int *)_2;
    *(int *)_2 = _4442;
    if( _1 != _4442 ){
        DeRef(_1);
    }
    _4442 = NOVALUE;

    /** 		entry*/
LB: 

    /** 			for i = 1 to length(data) do*/
    if (IS_SEQUENCE(_data_8044)){
            _4443 = SEQ_PTR(_data_8044)->length;
    }
    else {
        _4443 = 1;
    }
    {
        int _i_8099;
        _i_8099 = 1;
LE: 
        if (_i_8099 > _4443){
            goto LF; // [349] 372
        }

        /** 				data[i] = getc(fn)*/
        if (_fn_8038 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_8038, EF_READ);
            last_r_file_no = _fn_8038;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4444 = getKBchar();
            }
            else
            _4444 = getc(last_r_file_ptr);
        }
        else
        _4444 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_data_8044);
        _2 = (int)(((s1_ptr)_2)->base + _i_8099);
        *(int *)_2 = _4444;
        if( _1 != _4444 ){
        }
        _4444 = NOVALUE;

        /** 			end for*/
        _i_8099 = _i_8099 + 1;
        goto LE; // [367] 356
LF: 
        ;
    }

    /** 		end while*/
    goto LC; // [374] 281
LD: 

    /** 		nhit = 0*/
    _nhit_8045 = 0;

    /** 		while nmiss with entry do*/
    goto L10; // [386] 479
L11: 
    if (_nmiss_8046 == 0)
    {
        goto L12; // [391] 493
    }
    else{
    }

    /** 			while 1 do*/
L13: 

    /** 				nhit += 1*/
    _nhit_8045 = _nhit_8045 + 1;

    /** 				if nhit > length(hits) then*/
    if (IS_SEQUENCE(_hits_8040)){
            _4446 = SEQ_PTR(_hits_8040)->length;
    }
    else {
        _4446 = 1;
    }
    if (_nhit_8045 <= _4446)
    goto L14; // [412] 424

    /** 					nhit = 1*/
    _nhit_8045 = 1;
L14: 

    /** 				if hits[nhit] != 0 then*/
    _2 = (int)SEQ_PTR(_hits_8040);
    _4448 = (int)*(((s1_ptr)_2)->base + _nhit_8045);
    if (binary_op_a(EQUALS, _4448, 0)){
        _4448 = NOVALUE;
        goto L13; // [430] 399
    }
    _4448 = NOVALUE;

    /** 					exit*/
    goto L15; // [436] 444

    /** 			end while*/
    goto L13; // [441] 399
L15: 

    /** 			cs[nmiss] = hash(cs[nmiss], cs[nhit])*/
    _2 = (int)SEQ_PTR(_cs_8039);
    _4450 = (int)*(((s1_ptr)_2)->base + _nmiss_8046);
    _2 = (int)SEQ_PTR(_cs_8039);
    _4451 = (int)*(((s1_ptr)_2)->base + _nhit_8045);
    _4452 = calc_hash(_4450, _4451);
    _4450 = NOVALUE;
    _4451 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_8039);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_8039 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_8046);
    _1 = *(int *)_2;
    *(int *)_2 = _4452;
    if( _1 != _4452 ){
        DeRef(_1);
    }
    _4452 = NOVALUE;

    /** 			hits[nmiss] += 1*/
    _2 = (int)SEQ_PTR(_hits_8040);
    _4453 = (int)*(((s1_ptr)_2)->base + _nmiss_8046);
    if (IS_ATOM_INT(_4453)) {
        _4454 = _4453 + 1;
        if (_4454 > MAXINT){
            _4454 = NewDouble((double)_4454);
        }
    }
    else
    _4454 = binary_op(PLUS, 1, _4453);
    _4453 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_8040);
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_8046);
    _1 = *(int *)_2;
    *(int *)_2 = _4454;
    if( _1 != _4454 ){
        DeRef(_1);
    }
    _4454 = NOVALUE;

    /** 		entry*/
L10: 

    /** 			nmiss = find(0, hits)	*/
    _nmiss_8046 = find_from(0, _hits_8040, 1);

    /** 		end while*/
    goto L11; // [490] 389
L12: 
LA: 

    /** 	close(fn)*/
    EClose(_fn_8038);

    /** 	if return_text then*/
    if (_return_text_8037 == 0)
    {
        goto L16; // [500] 571
    }
    else{
    }

    /** 		sequence cs_text = ""*/
    RefDS(_5);
    DeRef(_cs_text_8118);
    _cs_text_8118 = _5;

    /** 		for i = 1 to length(cs) do*/
    if (IS_SEQUENCE(_cs_8039)){
            _4456 = SEQ_PTR(_cs_8039)->length;
    }
    else {
        _4456 = 1;
    }
    {
        int _i_8120;
        _i_8120 = 1;
L17: 
        if (_i_8120 > _4456){
            goto L18; // [515] 560
        }

        /** 			cs_text &= text:format("[:08X]", cs[i])*/
        _2 = (int)SEQ_PTR(_cs_8039);
        _4458 = (int)*(((s1_ptr)_2)->base + _i_8120);
        RefDS(_4457);
        Ref(_4458);
        _4459 = _6format(_4457, _4458);
        _4458 = NOVALUE;
        if (IS_SEQUENCE(_cs_text_8118) && IS_ATOM(_4459)) {
            Ref(_4459);
            Append(&_cs_text_8118, _cs_text_8118, _4459);
        }
        else if (IS_ATOM(_cs_text_8118) && IS_SEQUENCE(_4459)) {
        }
        else {
            Concat((object_ptr)&_cs_text_8118, _cs_text_8118, _4459);
        }
        DeRef(_4459);
        _4459 = NOVALUE;

        /** 			if i != length(cs) then*/
        if (IS_SEQUENCE(_cs_8039)){
                _4461 = SEQ_PTR(_cs_8039)->length;
        }
        else {
            _4461 = 1;
        }
        if (_i_8120 == _4461)
        goto L19; // [542] 553

        /** 				cs_text &= ' '*/
        Append(&_cs_text_8118, _cs_text_8118, 32);
L19: 

        /** 		end for*/
        _i_8120 = _i_8120 + 1;
        goto L17; // [555] 522
L18: 
        ;
    }

    /** 		return cs_text*/
    DeRefDS(_filename_8034);
    DeRef(_cs_8039);
    DeRef(_hits_8040);
    DeRef(_jx_8042);
    DeRef(_fx_8043);
    DeRefi(_data_8044);
    _4433 = NOVALUE;
    return _cs_text_8118;
    DeRefDS(_cs_text_8118);
    _cs_text_8118 = NOVALUE;
    goto L1A; // [568] 578
L16: 

    /** 		return cs*/
    DeRefDS(_filename_8034);
    DeRef(_hits_8040);
    DeRef(_jx_8042);
    DeRef(_fx_8043);
    DeRefi(_data_8044);
    _4433 = NOVALUE;
    return _cs_8039;
L1A: 
    ;
}



// 0x22F0C9A0
