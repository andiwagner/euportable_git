// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _3char_test(int _test_data_197, int _char_set_198)
{
    int _lChr_199 = NOVALUE;
    int _66 = NOVALUE;
    int _65 = NOVALUE;
    int _64 = NOVALUE;
    int _63 = NOVALUE;
    int _62 = NOVALUE;
    int _61 = NOVALUE;
    int _60 = NOVALUE;
    int _59 = NOVALUE;
    int _58 = NOVALUE;
    int _57 = NOVALUE;
    int _56 = NOVALUE;
    int _53 = NOVALUE;
    int _52 = NOVALUE;
    int _51 = NOVALUE;
    int _50 = NOVALUE;
    int _48 = NOVALUE;
    int _46 = NOVALUE;
    int _45 = NOVALUE;
    int _44 = NOVALUE;
    int _43 = NOVALUE;
    int _42 = NOVALUE;
    int _41 = NOVALUE;
    int _40 = NOVALUE;
    int _39 = NOVALUE;
    int _38 = NOVALUE;
    int _37 = NOVALUE;
    int _36 = NOVALUE;
    int _35 = NOVALUE;
    int _34 = NOVALUE;
    int _33 = NOVALUE;
    int _32 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(test_data) then*/
    if (IS_ATOM_INT(_test_data_197))
    _32 = 1;
    else if (IS_ATOM_DBL(_test_data_197))
    _32 = IS_ATOM_INT(DoubleToInt(_test_data_197));
    else
    _32 = 0;
    if (_32 == 0)
    {
        _32 = NOVALUE;
        goto L1; // [8] 115
    }
    else{
        _32 = NOVALUE;
    }

    /** 		if sequence(char_set[1]) then*/
    _2 = (int)SEQ_PTR(_char_set_198);
    _33 = (int)*(((s1_ptr)_2)->base + 1);
    _34 = IS_SEQUENCE(_33);
    _33 = NOVALUE;
    if (_34 == 0)
    {
        _34 = NOVALUE;
        goto L2; // [20] 96
    }
    else{
        _34 = NOVALUE;
    }

    /** 			for j = 1 to length(char_set) do*/
    if (IS_SEQUENCE(_char_set_198)){
            _35 = SEQ_PTR(_char_set_198)->length;
    }
    else {
        _35 = 1;
    }
    {
        int _j_206;
        _j_206 = 1;
L3: 
        if (_j_206 > _35){
            goto L4; // [28] 85
        }

        /** 				if test_data >= char_set[j][1] and test_data <= char_set[j][2] then */
        _2 = (int)SEQ_PTR(_char_set_198);
        _36 = (int)*(((s1_ptr)_2)->base + _j_206);
        _2 = (int)SEQ_PTR(_36);
        _37 = (int)*(((s1_ptr)_2)->base + 1);
        _36 = NOVALUE;
        if (IS_ATOM_INT(_test_data_197) && IS_ATOM_INT(_37)) {
            _38 = (_test_data_197 >= _37);
        }
        else {
            _38 = binary_op(GREATEREQ, _test_data_197, _37);
        }
        _37 = NOVALUE;
        if (IS_ATOM_INT(_38)) {
            if (_38 == 0) {
                goto L5; // [49] 78
            }
        }
        else {
            if (DBL_PTR(_38)->dbl == 0.0) {
                goto L5; // [49] 78
            }
        }
        _2 = (int)SEQ_PTR(_char_set_198);
        _40 = (int)*(((s1_ptr)_2)->base + _j_206);
        _2 = (int)SEQ_PTR(_40);
        _41 = (int)*(((s1_ptr)_2)->base + 2);
        _40 = NOVALUE;
        if (IS_ATOM_INT(_test_data_197) && IS_ATOM_INT(_41)) {
            _42 = (_test_data_197 <= _41);
        }
        else {
            _42 = binary_op(LESSEQ, _test_data_197, _41);
        }
        _41 = NOVALUE;
        if (_42 == 0) {
            DeRef(_42);
            _42 = NOVALUE;
            goto L5; // [66] 78
        }
        else {
            if (!IS_ATOM_INT(_42) && DBL_PTR(_42)->dbl == 0.0){
                DeRef(_42);
                _42 = NOVALUE;
                goto L5; // [66] 78
            }
            DeRef(_42);
            _42 = NOVALUE;
        }
        DeRef(_42);
        _42 = NOVALUE;

        /** 					return TRUE */
        DeRef(_test_data_197);
        DeRefDS(_char_set_198);
        DeRef(_38);
        _38 = NOVALUE;
        return _3TRUE_152;
L5: 

        /** 			end for*/
        _j_206 = _j_206 + 1;
        goto L3; // [80] 35
L4: 
        ;
    }

    /** 			return FALSE*/
    DeRef(_test_data_197);
    DeRefDS(_char_set_198);
    DeRef(_38);
    _38 = NOVALUE;
    return _3FALSE_150;
    goto L6; // [93] 330
L2: 

    /** 			return find(test_data, char_set) > 0*/
    _43 = find_from(_test_data_197, _char_set_198, 1);
    _44 = (_43 > 0);
    _43 = NOVALUE;
    DeRef(_test_data_197);
    DeRefDS(_char_set_198);
    DeRef(_38);
    _38 = NOVALUE;
    return _44;
    goto L6; // [112] 330
L1: 

    /** 	elsif sequence(test_data) then*/
    _45 = IS_SEQUENCE(_test_data_197);
    if (_45 == 0)
    {
        _45 = NOVALUE;
        goto L7; // [120] 321
    }
    else{
        _45 = NOVALUE;
    }

    /** 		if length(test_data) = 0 then */
    if (IS_SEQUENCE(_test_data_197)){
            _46 = SEQ_PTR(_test_data_197)->length;
    }
    else {
        _46 = 1;
    }
    if (_46 != 0)
    goto L8; // [128] 141

    /** 			return FALSE */
    DeRef(_test_data_197);
    DeRefDS(_char_set_198);
    DeRef(_44);
    _44 = NOVALUE;
    DeRef(_38);
    _38 = NOVALUE;
    return _3FALSE_150;
L8: 

    /** 		for i = 1 to length(test_data) label "NXTCHR" do*/
    if (IS_SEQUENCE(_test_data_197)){
            _48 = SEQ_PTR(_test_data_197)->length;
    }
    else {
        _48 = 1;
    }
    {
        int _i_225;
        _i_225 = 1;
L9: 
        if (_i_225 > _48){
            goto LA; // [146] 310
        }

        /** 			if sequence(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_197);
        _50 = (int)*(((s1_ptr)_2)->base + _i_225);
        _51 = IS_SEQUENCE(_50);
        _50 = NOVALUE;
        if (_51 == 0)
        {
            _51 = NOVALUE;
            goto LB; // [162] 174
        }
        else{
            _51 = NOVALUE;
        }

        /** 				return FALSE*/
        DeRef(_test_data_197);
        DeRefDS(_char_set_198);
        DeRef(_44);
        _44 = NOVALUE;
        DeRef(_38);
        _38 = NOVALUE;
        return _3FALSE_150;
LB: 

        /** 			if not integer(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_197);
        _52 = (int)*(((s1_ptr)_2)->base + _i_225);
        if (IS_ATOM_INT(_52))
        _53 = 1;
        else if (IS_ATOM_DBL(_52))
        _53 = IS_ATOM_INT(DoubleToInt(_52));
        else
        _53 = 0;
        _52 = NOVALUE;
        if (_53 != 0)
        goto LC; // [183] 195
        _53 = NOVALUE;

        /** 				return FALSE*/
        DeRef(_test_data_197);
        DeRefDS(_char_set_198);
        DeRef(_44);
        _44 = NOVALUE;
        DeRef(_38);
        _38 = NOVALUE;
        return _3FALSE_150;
LC: 

        /** 			lChr = test_data[i]*/
        _2 = (int)SEQ_PTR(_test_data_197);
        _lChr_199 = (int)*(((s1_ptr)_2)->base + _i_225);
        if (!IS_ATOM_INT(_lChr_199)){
            _lChr_199 = (long)DBL_PTR(_lChr_199)->dbl;
        }

        /** 			if sequence(char_set[1]) then*/
        _2 = (int)SEQ_PTR(_char_set_198);
        _56 = (int)*(((s1_ptr)_2)->base + 1);
        _57 = IS_SEQUENCE(_56);
        _56 = NOVALUE;
        if (_57 == 0)
        {
            _57 = NOVALUE;
            goto LD; // [214] 278
        }
        else{
            _57 = NOVALUE;
        }

        /** 				for j = 1 to length(char_set) do*/
        if (IS_SEQUENCE(_char_set_198)){
                _58 = SEQ_PTR(_char_set_198)->length;
        }
        else {
            _58 = 1;
        }
        {
            int _j_240;
            _j_240 = 1;
LE: 
            if (_j_240 > _58){
                goto LF; // [222] 275
            }

            /** 					if lChr >= char_set[j][1] and lChr <= char_set[j][2] then*/
            _2 = (int)SEQ_PTR(_char_set_198);
            _59 = (int)*(((s1_ptr)_2)->base + _j_240);
            _2 = (int)SEQ_PTR(_59);
            _60 = (int)*(((s1_ptr)_2)->base + 1);
            _59 = NOVALUE;
            if (IS_ATOM_INT(_60)) {
                _61 = (_lChr_199 >= _60);
            }
            else {
                _61 = binary_op(GREATEREQ, _lChr_199, _60);
            }
            _60 = NOVALUE;
            if (IS_ATOM_INT(_61)) {
                if (_61 == 0) {
                    goto L10; // [243] 268
                }
            }
            else {
                if (DBL_PTR(_61)->dbl == 0.0) {
                    goto L10; // [243] 268
                }
            }
            _2 = (int)SEQ_PTR(_char_set_198);
            _63 = (int)*(((s1_ptr)_2)->base + _j_240);
            _2 = (int)SEQ_PTR(_63);
            _64 = (int)*(((s1_ptr)_2)->base + 2);
            _63 = NOVALUE;
            if (IS_ATOM_INT(_64)) {
                _65 = (_lChr_199 <= _64);
            }
            else {
                _65 = binary_op(LESSEQ, _lChr_199, _64);
            }
            _64 = NOVALUE;
            if (_65 == 0) {
                DeRef(_65);
                _65 = NOVALUE;
                goto L10; // [260] 268
            }
            else {
                if (!IS_ATOM_INT(_65) && DBL_PTR(_65)->dbl == 0.0){
                    DeRef(_65);
                    _65 = NOVALUE;
                    goto L10; // [260] 268
                }
                DeRef(_65);
                _65 = NOVALUE;
            }
            DeRef(_65);
            _65 = NOVALUE;

            /** 						continue "NXTCHR" */
            goto L11; // [265] 305
L10: 

            /** 				end for*/
            _j_240 = _j_240 + 1;
            goto LE; // [270] 229
LF: 
            ;
        }
        goto L12; // [275] 295
LD: 

        /** 				if find(lChr, char_set) > 0 then*/
        _66 = find_from(_lChr_199, _char_set_198, 1);
        if (_66 <= 0)
        goto L13; // [285] 294

        /** 					continue "NXTCHR"*/
        goto L11; // [291] 305
L13: 
L12: 

        /** 			return FALSE*/
        DeRef(_test_data_197);
        DeRefDS(_char_set_198);
        DeRef(_44);
        _44 = NOVALUE;
        DeRef(_38);
        _38 = NOVALUE;
        DeRef(_61);
        _61 = NOVALUE;
        return _3FALSE_150;

        /** 		end for*/
L11: 
        _i_225 = _i_225 + 1;
        goto L9; // [305] 153
LA: 
        ;
    }

    /** 		return TRUE*/
    DeRef(_test_data_197);
    DeRefDS(_char_set_198);
    DeRef(_44);
    _44 = NOVALUE;
    DeRef(_38);
    _38 = NOVALUE;
    DeRef(_61);
    _61 = NOVALUE;
    return _3TRUE_152;
    goto L6; // [318] 330
L7: 

    /** 		return FALSE*/
    DeRef(_test_data_197);
    DeRefDS(_char_set_198);
    DeRef(_44);
    _44 = NOVALUE;
    DeRef(_38);
    _38 = NOVALUE;
    DeRef(_61);
    _61 = NOVALUE;
    return _3FALSE_150;
L6: 
    ;
}


void _3set_default_charsets()
{
    int _137 = NOVALUE;
    int _135 = NOVALUE;
    int _134 = NOVALUE;
    int _132 = NOVALUE;
    int _129 = NOVALUE;
    int _128 = NOVALUE;
    int _127 = NOVALUE;
    int _126 = NOVALUE;
    int _123 = NOVALUE;
    int _121 = NOVALUE;
    int _110 = NOVALUE;
    int _103 = NOVALUE;
    int _99 = NOVALUE;
    int _90 = NOVALUE;
    int _86 = NOVALUE;
    int _85 = NOVALUE;
    int _84 = NOVALUE;
    int _81 = NOVALUE;
    int _77 = NOVALUE;
    int _69 = NOVALUE;
    int _68 = NOVALUE;
    int _0, _1, _2;
    

    /** 	Defined_Sets = repeat(0, CS_LAST - CS_FIRST - 1)*/
    _68 = 20;
    _69 = 19;
    _68 = NOVALUE;
    DeRef(_3Defined_Sets_255);
    _3Defined_Sets_255 = Repeat(0, 19);
    _69 = NOVALUE;

    /** 	Defined_Sets[CS_Alphabetic	] = {{'a', 'z'}, {'A', 'Z'}}*/
    RefDS(_76);
    RefDS(_73);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _73;
    ((int *)_2)[2] = _76;
    _77 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 12);
    *(int *)_2 = _77;
    if( _1 != _77 ){
    }
    _77 = NOVALUE;

    /** 	Defined_Sets[CS_Alphanumeric] = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_80);
    *((int *)(_2+4)) = _80;
    RefDS(_73);
    *((int *)(_2+8)) = _73;
    RefDS(_76);
    *((int *)(_2+12)) = _76;
    _81 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = _81;
    if( _1 != _81 ){
        DeRef(_1);
    }
    _81 = NOVALUE;

    /** 	Defined_Sets[CS_Identifier]   = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}, {'_', '_'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_80);
    *((int *)(_2+4)) = _80;
    RefDS(_73);
    *((int *)(_2+8)) = _73;
    RefDS(_76);
    *((int *)(_2+12)) = _76;
    RefDS(_83);
    *((int *)(_2+16)) = _83;
    _84 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _84;
    if( _1 != _84 ){
        DeRef(_1);
    }
    _84 = NOVALUE;

    /** 	Defined_Sets[CS_Uppercase 	] = {{'A', 'Z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_76);
    *((int *)(_2+4)) = _76;
    _85 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _85;
    if( _1 != _85 ){
        DeRef(_1);
    }
    _85 = NOVALUE;

    /** 	Defined_Sets[CS_Lowercase 	] = {{'a', 'z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_73);
    *((int *)(_2+4)) = _73;
    _86 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _86;
    if( _1 != _86 ){
        DeRef(_1);
    }
    _86 = NOVALUE;

    /** 	Defined_Sets[CS_Printable 	] = {{' ', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_89);
    *((int *)(_2+4)) = _89;
    _90 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _90;
    if( _1 != _90 ){
        DeRef(_1);
    }
    _90 = NOVALUE;

    /** 	Defined_Sets[CS_Displayable ] = {{' ', '~'}, "  ", "\t\t", "\n\n", "\r\r", {8,8}, {7,7} }*/
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_89);
    *((int *)(_2+4)) = _89;
    RefDS(_91);
    *((int *)(_2+8)) = _91;
    RefDS(_92);
    *((int *)(_2+12)) = _92;
    RefDS(_93);
    *((int *)(_2+16)) = _93;
    RefDS(_94);
    *((int *)(_2+20)) = _94;
    RefDS(_96);
    *((int *)(_2+24)) = _96;
    RefDS(_98);
    *((int *)(_2+28)) = _98;
    _99 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _99;
    if( _1 != _99 ){
        DeRef(_1);
    }
    _99 = NOVALUE;

    /** 	Defined_Sets[CS_Whitespace 	] = " \t\n\r" & 11 & 160*/
    {
        int concat_list[3];

        concat_list[0] = 160;
        concat_list[1] = 11;
        concat_list[2] = _100;
        Concat_N((object_ptr)&_103, concat_list, 3);
    }
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _103;
    if( _1 != _103 ){
        DeRef(_1);
    }
    _103 = NOVALUE;

    /** 	Defined_Sets[CS_Consonant 	] = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"*/
    RefDS(_104);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _104;
    DeRef(_1);

    /** 	Defined_Sets[CS_Vowel 		] = "aeiouAEIOU"*/
    RefDS(_105);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _105;
    DeRef(_1);

    /** 	Defined_Sets[CS_Hexadecimal ] = {{'0', '9'}, {'A', 'F'},{'a', 'f'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_80);
    *((int *)(_2+4)) = _80;
    RefDS(_107);
    *((int *)(_2+8)) = _107;
    RefDS(_109);
    *((int *)(_2+12)) = _109;
    _110 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _110;
    if( _1 != _110 ){
        DeRef(_1);
    }
    _110 = NOVALUE;

    /** 	Defined_Sets[CS_Punctuation ] = {{' ', '/'}, {':', '?'}, {'[', '`'}, {'{', '~'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_112);
    *((int *)(_2+4)) = _112;
    RefDS(_115);
    *((int *)(_2+8)) = _115;
    RefDS(_118);
    *((int *)(_2+12)) = _118;
    RefDS(_120);
    *((int *)(_2+16)) = _120;
    _121 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _121;
    if( _1 != _121 ){
        DeRef(_1);
    }
    _121 = NOVALUE;

    /** 	Defined_Sets[CS_Control 	] = {{0, 31}, {127, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 31;
    _123 = MAKE_SEQ(_1);
    RefDS(_125);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _123;
    ((int *)_2)[2] = _125;
    _126 = MAKE_SEQ(_1);
    _123 = NOVALUE;
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = _126;
    if( _1 != _126 ){
        DeRef(_1);
    }
    _126 = NOVALUE;

    /** 	Defined_Sets[CS_ASCII 		] = {{0, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 127;
    _127 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _127;
    _128 = MAKE_SEQ(_1);
    _127 = NOVALUE;
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 13);
    _1 = *(int *)_2;
    *(int *)_2 = _128;
    if( _1 != _128 ){
        DeRef(_1);
    }
    _128 = NOVALUE;

    /** 	Defined_Sets[CS_Digit 		] = {{'0', '9'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_80);
    *((int *)(_2+4)) = _80;
    _129 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _129;
    if( _1 != _129 ){
        DeRef(_1);
    }
    _129 = NOVALUE;

    /** 	Defined_Sets[CS_Graphic 	] = {{'!', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_131);
    *((int *)(_2+4)) = _131;
    _132 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 16);
    _1 = *(int *)_2;
    *(int *)_2 = _132;
    if( _1 != _132 ){
        DeRef(_1);
    }
    _132 = NOVALUE;

    /** 	Defined_Sets[CS_Bytes	 	] = {{0, 255}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 255;
    _134 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _134;
    _135 = MAKE_SEQ(_1);
    _134 = NOVALUE;
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 17);
    _1 = *(int *)_2;
    *(int *)_2 = _135;
    if( _1 != _135 ){
        DeRef(_1);
    }
    _135 = NOVALUE;

    /** 	Defined_Sets[CS_SpecWord 	] = "_"*/
    RefDS(_136);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 18);
    _1 = *(int *)_2;
    *(int *)_2 = _136;
    DeRef(_1);

    /** 	Defined_Sets[CS_Boolean     ] = {TRUE,FALSE}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3TRUE_152;
    ((int *)_2)[2] = _3FALSE_150;
    _137 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _2 = (int)(((s1_ptr)_2)->base + 19);
    _1 = *(int *)_2;
    *(int *)_2 = _137;
    if( _1 != _137 ){
        DeRef(_1);
    }
    _137 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _3get_charsets()
{
    int _result__330 = NOVALUE;
    int _141 = NOVALUE;
    int _140 = NOVALUE;
    int _139 = NOVALUE;
    int _138 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_ = {}*/
    RefDS(_5);
    DeRef(_result__330);
    _result__330 = _5;

    /** 	for i = CS_FIRST + 1 to CS_LAST - 1 do*/
    _138 = 1;
    _139 = 19;
    {
        int _i_332;
        _i_332 = 1;
L1: 
        if (_i_332 > 19){
            goto L2; // [22] 52
        }

        /** 		result_ = append(result_, {i, Defined_Sets[i]} )*/
        _2 = (int)SEQ_PTR(_3Defined_Sets_255);
        _140 = (int)*(((s1_ptr)_2)->base + _i_332);
        Ref(_140);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_332;
        ((int *)_2)[2] = _140;
        _141 = MAKE_SEQ(_1);
        _140 = NOVALUE;
        RefDS(_141);
        Append(&_result__330, _result__330, _141);
        DeRefDS(_141);
        _141 = NOVALUE;

        /** 	end for*/
        _i_332 = _i_332 + 1;
        goto L1; // [47] 29
L2: 
        ;
    }

    /** 	return result_*/
    DeRef(_138);
    _138 = NOVALUE;
    DeRef(_139);
    _139 = NOVALUE;
    return _result__330;
    ;
}


void _3set_charsets(int _charset_list_340)
{
    int _164 = NOVALUE;
    int _163 = NOVALUE;
    int _162 = NOVALUE;
    int _161 = NOVALUE;
    int _160 = NOVALUE;
    int _159 = NOVALUE;
    int _158 = NOVALUE;
    int _157 = NOVALUE;
    int _156 = NOVALUE;
    int _155 = NOVALUE;
    int _154 = NOVALUE;
    int _153 = NOVALUE;
    int _152 = NOVALUE;
    int _151 = NOVALUE;
    int _150 = NOVALUE;
    int _149 = NOVALUE;
    int _148 = NOVALUE;
    int _147 = NOVALUE;
    int _146 = NOVALUE;
    int _145 = NOVALUE;
    int _144 = NOVALUE;
    int _143 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(charset_list) do*/
    if (IS_SEQUENCE(_charset_list_340)){
            _143 = SEQ_PTR(_charset_list_340)->length;
    }
    else {
        _143 = 1;
    }
    {
        int _i_342;
        _i_342 = 1;
L1: 
        if (_i_342 > _143){
            goto L2; // [8] 133
        }

        /** 		if sequence(charset_list[i]) and length(charset_list[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_charset_list_340);
        _144 = (int)*(((s1_ptr)_2)->base + _i_342);
        _145 = IS_SEQUENCE(_144);
        _144 = NOVALUE;
        if (_145 == 0) {
            goto L3; // [24] 126
        }
        _2 = (int)SEQ_PTR(_charset_list_340);
        _147 = (int)*(((s1_ptr)_2)->base + _i_342);
        if (IS_SEQUENCE(_147)){
                _148 = SEQ_PTR(_147)->length;
        }
        else {
            _148 = 1;
        }
        _147 = NOVALUE;
        _149 = (_148 == 2);
        _148 = NOVALUE;
        if (_149 == 0)
        {
            DeRef(_149);
            _149 = NOVALUE;
            goto L3; // [40] 126
        }
        else{
            DeRef(_149);
            _149 = NOVALUE;
        }

        /** 			if integer(charset_list[i][1]) and charset_list[i][1] > CS_FIRST and charset_list[i][1] < CS_LAST then*/
        _2 = (int)SEQ_PTR(_charset_list_340);
        _150 = (int)*(((s1_ptr)_2)->base + _i_342);
        _2 = (int)SEQ_PTR(_150);
        _151 = (int)*(((s1_ptr)_2)->base + 1);
        _150 = NOVALUE;
        if (IS_ATOM_INT(_151))
        _152 = 1;
        else if (IS_ATOM_DBL(_151))
        _152 = IS_ATOM_INT(DoubleToInt(_151));
        else
        _152 = 0;
        _151 = NOVALUE;
        if (_152 == 0) {
            _153 = 0;
            goto L4; // [56] 78
        }
        _2 = (int)SEQ_PTR(_charset_list_340);
        _154 = (int)*(((s1_ptr)_2)->base + _i_342);
        _2 = (int)SEQ_PTR(_154);
        _155 = (int)*(((s1_ptr)_2)->base + 1);
        _154 = NOVALUE;
        if (IS_ATOM_INT(_155)) {
            _156 = (_155 > 0);
        }
        else {
            _156 = binary_op(GREATER, _155, 0);
        }
        _155 = NOVALUE;
        if (IS_ATOM_INT(_156))
        _153 = (_156 != 0);
        else
        _153 = DBL_PTR(_156)->dbl != 0.0;
L4: 
        if (_153 == 0) {
            goto L5; // [78] 125
        }
        _2 = (int)SEQ_PTR(_charset_list_340);
        _158 = (int)*(((s1_ptr)_2)->base + _i_342);
        _2 = (int)SEQ_PTR(_158);
        _159 = (int)*(((s1_ptr)_2)->base + 1);
        _158 = NOVALUE;
        if (IS_ATOM_INT(_159)) {
            _160 = (_159 < 20);
        }
        else {
            _160 = binary_op(LESS, _159, 20);
        }
        _159 = NOVALUE;
        if (_160 == 0) {
            DeRef(_160);
            _160 = NOVALUE;
            goto L5; // [97] 125
        }
        else {
            if (!IS_ATOM_INT(_160) && DBL_PTR(_160)->dbl == 0.0){
                DeRef(_160);
                _160 = NOVALUE;
                goto L5; // [97] 125
            }
            DeRef(_160);
            _160 = NOVALUE;
        }
        DeRef(_160);
        _160 = NOVALUE;

        /** 				Defined_Sets[charset_list[i][1]] = charset_list[i][2]*/
        _2 = (int)SEQ_PTR(_charset_list_340);
        _161 = (int)*(((s1_ptr)_2)->base + _i_342);
        _2 = (int)SEQ_PTR(_161);
        _162 = (int)*(((s1_ptr)_2)->base + 1);
        _161 = NOVALUE;
        _2 = (int)SEQ_PTR(_charset_list_340);
        _163 = (int)*(((s1_ptr)_2)->base + _i_342);
        _2 = (int)SEQ_PTR(_163);
        _164 = (int)*(((s1_ptr)_2)->base + 2);
        _163 = NOVALUE;
        Ref(_164);
        _2 = (int)SEQ_PTR(_3Defined_Sets_255);
        if (!IS_ATOM_INT(_162))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_162)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _162);
        _1 = *(int *)_2;
        *(int *)_2 = _164;
        if( _1 != _164 ){
            DeRef(_1);
        }
        _164 = NOVALUE;
L5: 
L3: 

        /** 	end for*/
        _i_342 = _i_342 + 1;
        goto L1; // [128] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_charset_list_340);
    _147 = NOVALUE;
    _162 = NOVALUE;
    DeRef(_156);
    _156 = NOVALUE;
    return;
    ;
}


int _3boolean(int _test_data_369)
{
    int _167 = NOVALUE;
    int _166 = NOVALUE;
    int _165 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return find(test_data,{1,0}) != 0*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _165 = MAKE_SEQ(_1);
    _166 = find_from(_test_data_369, _165, 1);
    DeRefDS(_165);
    _165 = NOVALUE;
    _167 = (_166 != 0);
    _166 = NOVALUE;
    DeRef(_test_data_369);
    return _167;
    ;
}


int _3t_boolean(int _test_data_375)
{
    int _169 = NOVALUE;
    int _168 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Boolean])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _168 = (int)*(((s1_ptr)_2)->base + 19);
    Ref(_test_data_375);
    Ref(_168);
    _169 = _3char_test(_test_data_375, _168);
    _168 = NOVALUE;
    DeRef(_test_data_375);
    return _169;
    ;
}


int _3t_alnum(int _test_data_380)
{
    int _171 = NOVALUE;
    int _170 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphanumeric])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _170 = (int)*(((s1_ptr)_2)->base + 10);
    Ref(_test_data_380);
    Ref(_170);
    _171 = _3char_test(_test_data_380, _170);
    _170 = NOVALUE;
    DeRef(_test_data_380);
    return _171;
    ;
}


int _3t_identifier(int _test_data_385)
{
    int _181 = NOVALUE;
    int _180 = NOVALUE;
    int _179 = NOVALUE;
    int _178 = NOVALUE;
    int _177 = NOVALUE;
    int _176 = NOVALUE;
    int _175 = NOVALUE;
    int _174 = NOVALUE;
    int _173 = NOVALUE;
    int _172 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if t_digit(test_data) then*/
    Ref(_test_data_385);
    _172 = _3t_digit(_test_data_385);
    if (_172 == 0) {
        DeRef(_172);
        _172 = NOVALUE;
        goto L1; // [7] 19
    }
    else {
        if (!IS_ATOM_INT(_172) && DBL_PTR(_172)->dbl == 0.0){
            DeRef(_172);
            _172 = NOVALUE;
            goto L1; // [7] 19
        }
        DeRef(_172);
        _172 = NOVALUE;
    }
    DeRef(_172);
    _172 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_385);
    return 0;
    goto L2; // [16] 63
L1: 

    /** 	elsif sequence(test_data) and length(test_data) > 0 and t_digit(test_data[1]) then*/
    _173 = IS_SEQUENCE(_test_data_385);
    if (_173 == 0) {
        _174 = 0;
        goto L3; // [24] 39
    }
    if (IS_SEQUENCE(_test_data_385)){
            _175 = SEQ_PTR(_test_data_385)->length;
    }
    else {
        _175 = 1;
    }
    _176 = (_175 > 0);
    _175 = NOVALUE;
    _174 = (_176 != 0);
L3: 
    if (_174 == 0) {
        goto L4; // [39] 62
    }
    _2 = (int)SEQ_PTR(_test_data_385);
    _178 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_178);
    _179 = _3t_digit(_178);
    _178 = NOVALUE;
    if (_179 == 0) {
        DeRef(_179);
        _179 = NOVALUE;
        goto L4; // [52] 62
    }
    else {
        if (!IS_ATOM_INT(_179) && DBL_PTR(_179)->dbl == 0.0){
            DeRef(_179);
            _179 = NOVALUE;
            goto L4; // [52] 62
        }
        DeRef(_179);
        _179 = NOVALUE;
    }
    DeRef(_179);
    _179 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_385);
    DeRef(_176);
    _176 = NOVALUE;
    return 0;
L4: 
L2: 

    /** 	return char_test(test_data, Defined_Sets[CS_Identifier])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _180 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_test_data_385);
    Ref(_180);
    _181 = _3char_test(_test_data_385, _180);
    _180 = NOVALUE;
    DeRef(_test_data_385);
    DeRef(_176);
    _176 = NOVALUE;
    return _181;
    ;
}


int _3t_alpha(int _test_data_402)
{
    int _183 = NOVALUE;
    int _182 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphabetic])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _182 = (int)*(((s1_ptr)_2)->base + 12);
    Ref(_test_data_402);
    Ref(_182);
    _183 = _3char_test(_test_data_402, _182);
    _182 = NOVALUE;
    DeRef(_test_data_402);
    return _183;
    ;
}


int _3t_ascii(int _test_data_407)
{
    int _185 = NOVALUE;
    int _184 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_ASCII])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _184 = (int)*(((s1_ptr)_2)->base + 13);
    Ref(_test_data_407);
    Ref(_184);
    _185 = _3char_test(_test_data_407, _184);
    _184 = NOVALUE;
    DeRef(_test_data_407);
    return _185;
    ;
}


int _3t_cntrl(int _test_data_412)
{
    int _187 = NOVALUE;
    int _186 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Control])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _186 = (int)*(((s1_ptr)_2)->base + 14);
    Ref(_test_data_412);
    Ref(_186);
    _187 = _3char_test(_test_data_412, _186);
    _186 = NOVALUE;
    DeRef(_test_data_412);
    return _187;
    ;
}


int _3t_digit(int _test_data_417)
{
    int _189 = NOVALUE;
    int _188 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Digit])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _188 = (int)*(((s1_ptr)_2)->base + 15);
    Ref(_test_data_417);
    Ref(_188);
    _189 = _3char_test(_test_data_417, _188);
    _188 = NOVALUE;
    DeRef(_test_data_417);
    return _189;
    ;
}


int _3t_graph(int _test_data_422)
{
    int _191 = NOVALUE;
    int _190 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Graphic])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _190 = (int)*(((s1_ptr)_2)->base + 16);
    Ref(_test_data_422);
    Ref(_190);
    _191 = _3char_test(_test_data_422, _190);
    _190 = NOVALUE;
    DeRef(_test_data_422);
    return _191;
    ;
}


int _3t_specword(int _test_data_427)
{
    int _193 = NOVALUE;
    int _192 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_SpecWord])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _192 = (int)*(((s1_ptr)_2)->base + 18);
    Ref(_test_data_427);
    Ref(_192);
    _193 = _3char_test(_test_data_427, _192);
    _192 = NOVALUE;
    DeRef(_test_data_427);
    return _193;
    ;
}


int _3t_bytearray(int _test_data_432)
{
    int _195 = NOVALUE;
    int _194 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Bytes])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _194 = (int)*(((s1_ptr)_2)->base + 17);
    Ref(_test_data_432);
    Ref(_194);
    _195 = _3char_test(_test_data_432, _194);
    _194 = NOVALUE;
    DeRef(_test_data_432);
    return _195;
    ;
}


int _3t_lower(int _test_data_437)
{
    int _197 = NOVALUE;
    int _196 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Lowercase])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _196 = (int)*(((s1_ptr)_2)->base + 8);
    Ref(_test_data_437);
    Ref(_196);
    _197 = _3char_test(_test_data_437, _196);
    _196 = NOVALUE;
    DeRef(_test_data_437);
    return _197;
    ;
}


int _3t_print(int _test_data_442)
{
    int _199 = NOVALUE;
    int _198 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Printable])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _198 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_test_data_442);
    Ref(_198);
    _199 = _3char_test(_test_data_442, _198);
    _198 = NOVALUE;
    DeRef(_test_data_442);
    return _199;
    ;
}


int _3t_display(int _test_data_447)
{
    int _201 = NOVALUE;
    int _200 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Displayable])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _200 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_test_data_447);
    Ref(_200);
    _201 = _3char_test(_test_data_447, _200);
    _200 = NOVALUE;
    DeRef(_test_data_447);
    return _201;
    ;
}


int _3t_punct(int _test_data_452)
{
    int _203 = NOVALUE;
    int _202 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Punctuation])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _202 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_test_data_452);
    Ref(_202);
    _203 = _3char_test(_test_data_452, _202);
    _202 = NOVALUE;
    DeRef(_test_data_452);
    return _203;
    ;
}


int _3t_space(int _test_data_457)
{
    int _205 = NOVALUE;
    int _204 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Whitespace])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _204 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_test_data_457);
    Ref(_204);
    _205 = _3char_test(_test_data_457, _204);
    _204 = NOVALUE;
    DeRef(_test_data_457);
    return _205;
    ;
}


int _3t_upper(int _test_data_462)
{
    int _207 = NOVALUE;
    int _206 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Uppercase])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _206 = (int)*(((s1_ptr)_2)->base + 9);
    Ref(_test_data_462);
    Ref(_206);
    _207 = _3char_test(_test_data_462, _206);
    _206 = NOVALUE;
    DeRef(_test_data_462);
    return _207;
    ;
}


int _3t_xdigit(int _test_data_467)
{
    int _209 = NOVALUE;
    int _208 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Hexadecimal])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _208 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_test_data_467);
    Ref(_208);
    _209 = _3char_test(_test_data_467, _208);
    _208 = NOVALUE;
    DeRef(_test_data_467);
    return _209;
    ;
}


int _3t_vowel(int _test_data_472)
{
    int _211 = NOVALUE;
    int _210 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Vowel])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _210 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_test_data_472);
    Ref(_210);
    _211 = _3char_test(_test_data_472, _210);
    _210 = NOVALUE;
    DeRef(_test_data_472);
    return _211;
    ;
}


int _3t_consonant(int _test_data_477)
{
    int _213 = NOVALUE;
    int _212 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Consonant])*/
    _2 = (int)SEQ_PTR(_3Defined_Sets_255);
    _212 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_test_data_477);
    Ref(_212);
    _213 = _3char_test(_test_data_477, _212);
    _212 = NOVALUE;
    DeRef(_test_data_477);
    return _213;
    ;
}


int _3integer_array(int _x_482)
{
    int _218 = NOVALUE;
    int _217 = NOVALUE;
    int _216 = NOVALUE;
    int _214 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _214 = IS_SEQUENCE(_x_482);
    if (_214 != 0)
    goto L1; // [6] 16
    _214 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_482);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_482)){
            _216 = SEQ_PTR(_x_482)->length;
    }
    else {
        _216 = 1;
    }
    {
        int _i_487;
        _i_487 = 1;
L2: 
        if (_i_487 > _216){
            goto L3; // [21] 54
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_482);
        _217 = (int)*(((s1_ptr)_2)->base + _i_487);
        if (IS_ATOM_INT(_217))
        _218 = 1;
        else if (IS_ATOM_DBL(_217))
        _218 = IS_ATOM_INT(DoubleToInt(_217));
        else
        _218 = 0;
        _217 = NOVALUE;
        if (_218 != 0)
        goto L4; // [37] 47
        _218 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_482);
        return 0;
L4: 

        /** 	end for*/
        _i_487 = _i_487 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_482);
    return 1;
    ;
}


int _3t_text(int _x_495)
{
    int _226 = NOVALUE;
    int _224 = NOVALUE;
    int _223 = NOVALUE;
    int _222 = NOVALUE;
    int _220 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _220 = IS_SEQUENCE(_x_495);
    if (_220 != 0)
    goto L1; // [6] 16
    _220 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_495);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_495)){
            _222 = SEQ_PTR(_x_495)->length;
    }
    else {
        _222 = 1;
    }
    {
        int _i_500;
        _i_500 = 1;
L2: 
        if (_i_500 > _222){
            goto L3; // [21] 71
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_495);
        _223 = (int)*(((s1_ptr)_2)->base + _i_500);
        if (IS_ATOM_INT(_223))
        _224 = 1;
        else if (IS_ATOM_DBL(_223))
        _224 = IS_ATOM_INT(DoubleToInt(_223));
        else
        _224 = 0;
        _223 = NOVALUE;
        if (_224 != 0)
        goto L4; // [37] 47
        _224 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_495);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_495);
        _226 = (int)*(((s1_ptr)_2)->base + _i_500);
        if (binary_op_a(GREATEREQ, _226, 0)){
            _226 = NOVALUE;
            goto L5; // [53] 64
        }
        _226 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_495);
        return 0;
L5: 

        /** 	end for*/
        _i_500 = _i_500 + 1;
        goto L2; // [66] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_495);
    return 1;
    ;
}


int _3number_array(int _x_511)
{
    int _232 = NOVALUE;
    int _231 = NOVALUE;
    int _230 = NOVALUE;
    int _228 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _228 = IS_SEQUENCE(_x_511);
    if (_228 != 0)
    goto L1; // [6] 16
    _228 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_511);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_511)){
            _230 = SEQ_PTR(_x_511)->length;
    }
    else {
        _230 = 1;
    }
    {
        int _i_516;
        _i_516 = 1;
L2: 
        if (_i_516 > _230){
            goto L3; // [21] 54
        }

        /** 		if not atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_511);
        _231 = (int)*(((s1_ptr)_2)->base + _i_516);
        _232 = IS_ATOM(_231);
        _231 = NOVALUE;
        if (_232 != 0)
        goto L4; // [37] 47
        _232 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_511);
        return 0;
L4: 

        /** 	end for*/
        _i_516 = _i_516 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_511);
    return 1;
    ;
}


int _3sequence_array(int _x_524)
{
    int _238 = NOVALUE;
    int _237 = NOVALUE;
    int _236 = NOVALUE;
    int _234 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _234 = IS_SEQUENCE(_x_524);
    if (_234 != 0)
    goto L1; // [6] 16
    _234 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_524);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_524)){
            _236 = SEQ_PTR(_x_524)->length;
    }
    else {
        _236 = 1;
    }
    {
        int _i_529;
        _i_529 = 1;
L2: 
        if (_i_529 > _236){
            goto L3; // [21] 54
        }

        /** 		if not sequence(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_524);
        _237 = (int)*(((s1_ptr)_2)->base + _i_529);
        _238 = IS_SEQUENCE(_237);
        _237 = NOVALUE;
        if (_238 != 0)
        goto L4; // [37] 47
        _238 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_524);
        return 0;
L4: 

        /** 	end for*/
        _i_529 = _i_529 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_524);
    return 1;
    ;
}


int _3ascii_string(int _x_537)
{
    int _248 = NOVALUE;
    int _246 = NOVALUE;
    int _244 = NOVALUE;
    int _243 = NOVALUE;
    int _242 = NOVALUE;
    int _240 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _240 = IS_SEQUENCE(_x_537);
    if (_240 != 0)
    goto L1; // [6] 16
    _240 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_537);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_537)){
            _242 = SEQ_PTR(_x_537)->length;
    }
    else {
        _242 = 1;
    }
    {
        int _i_542;
        _i_542 = 1;
L2: 
        if (_i_542 > _242){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_537);
        _243 = (int)*(((s1_ptr)_2)->base + _i_542);
        if (IS_ATOM_INT(_243))
        _244 = 1;
        else if (IS_ATOM_DBL(_243))
        _244 = IS_ATOM_INT(DoubleToInt(_243));
        else
        _244 = 0;
        _243 = NOVALUE;
        if (_244 != 0)
        goto L4; // [37] 47
        _244 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_537);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_537);
        _246 = (int)*(((s1_ptr)_2)->base + _i_542);
        if (binary_op_a(GREATEREQ, _246, 0)){
            _246 = NOVALUE;
            goto L5; // [53] 64
        }
        _246 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_537);
        return 0;
L5: 

        /** 		if x[i] > 127 then*/
        _2 = (int)SEQ_PTR(_x_537);
        _248 = (int)*(((s1_ptr)_2)->base + _i_542);
        if (binary_op_a(LESSEQ, _248, 127)){
            _248 = NOVALUE;
            goto L6; // [70] 81
        }
        _248 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_537);
        return 0;
L6: 

        /** 	end for*/
        _i_542 = _i_542 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_537);
    return 1;
    ;
}


int _3string(int _x_556)
{
    int _258 = NOVALUE;
    int _256 = NOVALUE;
    int _254 = NOVALUE;
    int _253 = NOVALUE;
    int _252 = NOVALUE;
    int _250 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _250 = IS_SEQUENCE(_x_556);
    if (_250 != 0)
    goto L1; // [6] 16
    _250 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_556);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_556)){
            _252 = SEQ_PTR(_x_556)->length;
    }
    else {
        _252 = 1;
    }
    {
        int _i_561;
        _i_561 = 1;
L2: 
        if (_i_561 > _252){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_556);
        _253 = (int)*(((s1_ptr)_2)->base + _i_561);
        if (IS_ATOM_INT(_253))
        _254 = 1;
        else if (IS_ATOM_DBL(_253))
        _254 = IS_ATOM_INT(DoubleToInt(_253));
        else
        _254 = 0;
        _253 = NOVALUE;
        if (_254 != 0)
        goto L4; // [37] 47
        _254 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_556);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_556);
        _256 = (int)*(((s1_ptr)_2)->base + _i_561);
        if (binary_op_a(GREATEREQ, _256, 0)){
            _256 = NOVALUE;
            goto L5; // [53] 64
        }
        _256 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_556);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_556);
        _258 = (int)*(((s1_ptr)_2)->base + _i_561);
        if (binary_op_a(LESSEQ, _258, 255)){
            _258 = NOVALUE;
            goto L6; // [70] 81
        }
        _258 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_556);
        return 0;
L6: 

        /** 	end for*/
        _i_561 = _i_561 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_556);
    return 1;
    ;
}


int _3cstring(int _x_575)
{
    int _268 = NOVALUE;
    int _266 = NOVALUE;
    int _264 = NOVALUE;
    int _263 = NOVALUE;
    int _262 = NOVALUE;
    int _260 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _260 = IS_SEQUENCE(_x_575);
    if (_260 != 0)
    goto L1; // [6] 16
    _260 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_575);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_575)){
            _262 = SEQ_PTR(_x_575)->length;
    }
    else {
        _262 = 1;
    }
    {
        int _i_580;
        _i_580 = 1;
L2: 
        if (_i_580 > _262){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_575);
        _263 = (int)*(((s1_ptr)_2)->base + _i_580);
        if (IS_ATOM_INT(_263))
        _264 = 1;
        else if (IS_ATOM_DBL(_263))
        _264 = IS_ATOM_INT(DoubleToInt(_263));
        else
        _264 = 0;
        _263 = NOVALUE;
        if (_264 != 0)
        goto L4; // [37] 47
        _264 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_575);
        return 0;
L4: 

        /** 		if x[i] <= 0 then*/
        _2 = (int)SEQ_PTR(_x_575);
        _266 = (int)*(((s1_ptr)_2)->base + _i_580);
        if (binary_op_a(GREATER, _266, 0)){
            _266 = NOVALUE;
            goto L5; // [53] 64
        }
        _266 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_575);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_575);
        _268 = (int)*(((s1_ptr)_2)->base + _i_580);
        if (binary_op_a(LESSEQ, _268, 255)){
            _268 = NOVALUE;
            goto L6; // [70] 81
        }
        _268 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_575);
        return 0;
L6: 

        /** 	end for*/
        _i_580 = _i_580 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_575);
    return 1;
    ;
}



// 0x4CF6EFCB
