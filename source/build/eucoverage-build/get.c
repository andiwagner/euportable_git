// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _14get_ch()
{
    int _987 = NOVALUE;
    int _986 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(input_string) then*/
    _986 = IS_SEQUENCE(_14input_string_2261);
    if (_986 == 0)
    {
        _986 = NOVALUE;
        goto L1; // [8] 56
    }
    else{
        _986 = NOVALUE;
    }

    /** 		if string_next <= length(input_string) then*/
    if (IS_SEQUENCE(_14input_string_2261)){
            _987 = SEQ_PTR(_14input_string_2261)->length;
    }
    else {
        _987 = 1;
    }
    if (_14string_next_2262 > _987)
    goto L2; // [20] 47

    /** 			ch = input_string[string_next]*/
    _2 = (int)SEQ_PTR(_14input_string_2261);
    _14ch_2263 = (int)*(((s1_ptr)_2)->base + _14string_next_2262);
    if (!IS_ATOM_INT(_14ch_2263)){
        _14ch_2263 = (long)DBL_PTR(_14ch_2263)->dbl;
    }

    /** 			string_next += 1*/
    _14string_next_2262 = _14string_next_2262 + 1;
    goto L3; // [44] 81
L2: 

    /** 			ch = GET_EOF*/
    _14ch_2263 = -1;
    goto L3; // [53] 81
L1: 

    /** 		ch = getc(input_file)*/
    if (_14input_file_2260 != last_r_file_no) {
        last_r_file_ptr = which_file(_14input_file_2260, EF_READ);
        last_r_file_no = _14input_file_2260;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _14ch_2263 = getKBchar();
        }
        else
        _14ch_2263 = getc(last_r_file_ptr);
    }
    else
    _14ch_2263 = getc(last_r_file_ptr);

    /** 		if ch = GET_EOF then*/
    if (_14ch_2263 != -1)
    goto L4; // [67] 80

    /** 			string_next += 1*/
    _14string_next_2262 = _14string_next_2262 + 1;
L4: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _14escape_char(int _c_2290)
{
    int _i_2291 = NOVALUE;
    int _999 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = find(c, ESCAPE_CHARS)*/
    _i_2291 = find_from(_c_2290, _14ESCAPE_CHARS_2284, 1);

    /** 	if i = 0 then*/
    if (_i_2291 != 0)
    goto L1; // [12] 25

    /** 		return GET_FAIL*/
    return 1;
    goto L2; // [22] 36
L1: 

    /** 		return ESCAPED_CHARS[i]*/
    _2 = (int)SEQ_PTR(_14ESCAPED_CHARS_2286);
    _999 = (int)*(((s1_ptr)_2)->base + _i_2291);
    Ref(_999);
    return _999;
L2: 
    ;
}


int _14get_qchar()
{
    int _c_2299 = NOVALUE;
    int _1010 = NOVALUE;
    int _1009 = NOVALUE;
    int _1007 = NOVALUE;
    int _1004 = NOVALUE;
    int _0, _1, _2;
    

    /** 	get_ch()*/
    _14get_ch();

    /** 	c = ch*/
    _c_2299 = _14ch_2263;

    /** 	if ch = '\\' then*/
    if (_14ch_2263 != 92)
    goto L1; // [16] 54

    /** 		get_ch()*/
    _14get_ch();

    /** 		c = escape_char(ch)*/
    _c_2299 = _14escape_char(_14ch_2263);
    if (!IS_ATOM_INT(_c_2299)) {
        _1 = (long)(DBL_PTR(_c_2299)->dbl);
        if (UNIQUE(DBL_PTR(_c_2299)) && (DBL_PTR(_c_2299)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_2299);
        _c_2299 = _1;
    }

    /** 		if c = GET_FAIL then*/
    if (_c_2299 != 1)
    goto L2; // [36] 74

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1004 = MAKE_SEQ(_1);
    return _1004;
    goto L2; // [51] 74
L1: 

    /** 	elsif ch = '\'' then*/
    if (_14ch_2263 != 39)
    goto L3; // [58] 73

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1007 = MAKE_SEQ(_1);
    DeRef(_1004);
    _1004 = NOVALUE;
    return _1007;
L3: 
L2: 

    /** 	get_ch()*/
    _14get_ch();

    /** 	if ch != '\'' then*/
    if (_14ch_2263 == 39)
    goto L4; // [82] 99

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1009 = MAKE_SEQ(_1);
    DeRef(_1004);
    _1004 = NOVALUE;
    DeRef(_1007);
    _1007 = NOVALUE;
    return _1009;
    goto L5; // [96] 114
L4: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		return {GET_SUCCESS, c}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _c_2299;
    _1010 = MAKE_SEQ(_1);
    DeRef(_1004);
    _1004 = NOVALUE;
    DeRef(_1007);
    _1007 = NOVALUE;
    DeRef(_1009);
    _1009 = NOVALUE;
    return _1010;
L5: 
    ;
}


int _14get_heredoc(int _terminator_2318)
{
    int _text_2319 = NOVALUE;
    int _ends_at_2320 = NOVALUE;
    int _1025 = NOVALUE;
    int _1024 = NOVALUE;
    int _1023 = NOVALUE;
    int _1022 = NOVALUE;
    int _1021 = NOVALUE;
    int _1018 = NOVALUE;
    int _1016 = NOVALUE;
    int _1015 = NOVALUE;
    int _1014 = NOVALUE;
    int _1013 = NOVALUE;
    int _1011 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence text = ""*/
    RefDS(_5);
    DeRefi(_text_2319);
    _text_2319 = _5;

    /** 	integer ends_at = 1 - length( terminator )*/
    if (IS_SEQUENCE(_terminator_2318)){
            _1011 = SEQ_PTR(_terminator_2318)->length;
    }
    else {
        _1011 = 1;
    }
    _ends_at_2320 = 1 - _1011;
    _1011 = NOVALUE;

    /** 	while ends_at < 1 or not match( terminator, text, ends_at ) with entry do*/
    goto L1; // [23] 71
L2: 
    _1013 = (_ends_at_2320 < 1);
    if (_1013 != 0) {
        DeRef(_1014);
        _1014 = 1;
        goto L3; // [30] 46
    }
    _1015 = e_match_from(_terminator_2318, _text_2319, _ends_at_2320);
    _1016 = (_1015 == 0);
    _1015 = NOVALUE;
    _1014 = (_1016 != 0);
L3: 
    if (_1014 == 0)
    {
        _1014 = NOVALUE;
        goto L4; // [46] 96
    }
    else{
        _1014 = NOVALUE;
    }

    /** 		if ch = GET_EOF then*/
    if (_14ch_2263 != -1)
    goto L5; // [53] 68

    /** 			return { GET_FAIL, 0 }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1018 = MAKE_SEQ(_1);
    DeRefDSi(_terminator_2318);
    DeRefi(_text_2319);
    DeRef(_1013);
    _1013 = NOVALUE;
    DeRef(_1016);
    _1016 = NOVALUE;
    return _1018;
L5: 

    /** 	entry*/
L1: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		text &= ch*/
    Append(&_text_2319, _text_2319, _14ch_2263);

    /** 		ends_at += 1*/
    _ends_at_2320 = _ends_at_2320 + 1;

    /** 	end while*/
    goto L2; // [93] 26
L4: 

    /** 	return { GET_SUCCESS, head( text, length( text ) - length( terminator ) ) }*/
    if (IS_SEQUENCE(_text_2319)){
            _1021 = SEQ_PTR(_text_2319)->length;
    }
    else {
        _1021 = 1;
    }
    if (IS_SEQUENCE(_terminator_2318)){
            _1022 = SEQ_PTR(_terminator_2318)->length;
    }
    else {
        _1022 = 1;
    }
    _1023 = _1021 - _1022;
    _1021 = NOVALUE;
    _1022 = NOVALUE;
    {
        int len = SEQ_PTR(_text_2319)->length;
        int size = (IS_ATOM_INT(_1023)) ? _1023 : (object)(DBL_PTR(_1023)->dbl);
        if (size <= 0){
            DeRef( _1024 );
            _1024 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_text_2319);
            DeRef(_1024);
            _1024 = _text_2319;
        }
        else{
            Head(SEQ_PTR(_text_2319),size+1,&_1024);
        }
    }
    _1023 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _1024;
    _1025 = MAKE_SEQ(_1);
    _1024 = NOVALUE;
    DeRefDSi(_terminator_2318);
    DeRefDSi(_text_2319);
    DeRef(_1013);
    _1013 = NOVALUE;
    DeRef(_1016);
    _1016 = NOVALUE;
    DeRef(_1018);
    _1018 = NOVALUE;
    return _1025;
    ;
}


int _14get_string()
{
    int _text_2340 = NOVALUE;
    int _1043 = NOVALUE;
    int _1039 = NOVALUE;
    int _1038 = NOVALUE;
    int _1035 = NOVALUE;
    int _1034 = NOVALUE;
    int _1033 = NOVALUE;
    int _1032 = NOVALUE;
    int _1029 = NOVALUE;
    int _1028 = NOVALUE;
    int _1026 = NOVALUE;
    int _0, _1, _2;
    

    /** 	text = ""*/
    RefDS(_5);
    DeRefi(_text_2340);
    _text_2340 = _5;

    /** 	while TRUE do*/
L1: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch = GET_EOF or ch = '\n' then*/
    _1026 = (_14ch_2263 == -1);
    if (_1026 != 0) {
        goto L2; // [25] 40
    }
    _1028 = (_14ch_2263 == 10);
    if (_1028 == 0)
    {
        DeRef(_1028);
        _1028 = NOVALUE;
        goto L3; // [36] 53
    }
    else{
        DeRef(_1028);
        _1028 = NOVALUE;
    }
L2: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1029 = MAKE_SEQ(_1);
    DeRefi(_text_2340);
    DeRef(_1026);
    _1026 = NOVALUE;
    return _1029;
    goto L4; // [50] 164
L3: 

    /** 		elsif ch = '"' then*/
    if (_14ch_2263 != 34)
    goto L5; // [57] 121

    /** 			get_ch()*/
    _14get_ch();

    /** 			if length( text ) = 0 and ch = '"' then*/
    if (IS_SEQUENCE(_text_2340)){
            _1032 = SEQ_PTR(_text_2340)->length;
    }
    else {
        _1032 = 1;
    }
    _1033 = (_1032 == 0);
    _1032 = NOVALUE;
    if (_1033 == 0) {
        goto L6; // [74] 108
    }
    _1035 = (_14ch_2263 == 34);
    if (_1035 == 0)
    {
        DeRef(_1035);
        _1035 = NOVALUE;
        goto L6; // [85] 108
    }
    else{
        DeRef(_1035);
        _1035 = NOVALUE;
    }

    /** 				if ch = '"' then*/
    if (_14ch_2263 != 34)
    goto L7; // [92] 107

    /** 					return get_heredoc( `"""` )*/
    RefDS(_1037);
    _1038 = _14get_heredoc(_1037);
    DeRefi(_text_2340);
    DeRef(_1026);
    _1026 = NOVALUE;
    DeRef(_1029);
    _1029 = NOVALUE;
    DeRef(_1033);
    _1033 = NOVALUE;
    return _1038;
L7: 
L6: 

    /** 			return {GET_SUCCESS, text}*/
    RefDS(_text_2340);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _text_2340;
    _1039 = MAKE_SEQ(_1);
    DeRefDSi(_text_2340);
    DeRef(_1026);
    _1026 = NOVALUE;
    DeRef(_1029);
    _1029 = NOVALUE;
    DeRef(_1033);
    _1033 = NOVALUE;
    DeRef(_1038);
    _1038 = NOVALUE;
    return _1039;
    goto L4; // [118] 164
L5: 

    /** 		elsif ch = '\\' then*/
    if (_14ch_2263 != 92)
    goto L8; // [125] 163

    /** 			get_ch()*/
    _14get_ch();

    /** 			ch = escape_char(ch)*/
    _0 = _14escape_char(_14ch_2263);
    _14ch_2263 = _0;
    if (!IS_ATOM_INT(_14ch_2263)) {
        _1 = (long)(DBL_PTR(_14ch_2263)->dbl);
        if (UNIQUE(DBL_PTR(_14ch_2263)) && (DBL_PTR(_14ch_2263)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14ch_2263);
        _14ch_2263 = _1;
    }

    /** 			if ch = GET_FAIL then*/
    if (_14ch_2263 != 1)
    goto L9; // [147] 162

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1043 = MAKE_SEQ(_1);
    DeRefi(_text_2340);
    DeRef(_1026);
    _1026 = NOVALUE;
    DeRef(_1029);
    _1029 = NOVALUE;
    DeRef(_1033);
    _1033 = NOVALUE;
    DeRef(_1038);
    _1038 = NOVALUE;
    DeRef(_1039);
    _1039 = NOVALUE;
    return _1043;
L9: 
L8: 
L4: 

    /** 		text = text & ch*/
    Append(&_text_2340, _text_2340, _14ch_2263);

    /** 	end while*/
    goto L1; // [174] 13
    ;
}


int _14read_comment()
{
    int _1064 = NOVALUE;
    int _1063 = NOVALUE;
    int _1061 = NOVALUE;
    int _1059 = NOVALUE;
    int _1057 = NOVALUE;
    int _1056 = NOVALUE;
    int _1055 = NOVALUE;
    int _1053 = NOVALUE;
    int _1052 = NOVALUE;
    int _1051 = NOVALUE;
    int _1050 = NOVALUE;
    int _1049 = NOVALUE;
    int _1048 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(input_string) then*/
    _1048 = IS_ATOM(_14input_string_2261);
    if (_1048 == 0)
    {
        _1048 = NOVALUE;
        goto L1; // [8] 98
    }
    else{
        _1048 = NOVALUE;
    }

    /** 		while ch!='\n' and ch!='\r' and ch!=-1 do*/
L2: 
    _1049 = (_14ch_2263 != 10);
    if (_1049 == 0) {
        _1050 = 0;
        goto L3; // [22] 36
    }
    _1051 = (_14ch_2263 != 13);
    _1050 = (_1051 != 0);
L3: 
    if (_1050 == 0) {
        goto L4; // [36] 59
    }
    _1053 = (_14ch_2263 != -1);
    if (_1053 == 0)
    {
        DeRef(_1053);
        _1053 = NOVALUE;
        goto L4; // [47] 59
    }
    else{
        DeRef(_1053);
        _1053 = NOVALUE;
    }

    /** 			get_ch()*/
    _14get_ch();

    /** 		end while*/
    goto L2; // [56] 16
L4: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch=-1 then*/
    if (_14ch_2263 != -1)
    goto L5; // [67] 84

    /** 			return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1055 = MAKE_SEQ(_1);
    DeRef(_1049);
    _1049 = NOVALUE;
    DeRef(_1051);
    _1051 = NOVALUE;
    return _1055;
    goto L6; // [81] 182
L5: 

    /** 			return {GET_IGNORE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _1056 = MAKE_SEQ(_1);
    DeRef(_1049);
    _1049 = NOVALUE;
    DeRef(_1051);
    _1051 = NOVALUE;
    DeRef(_1055);
    _1055 = NOVALUE;
    return _1056;
    goto L6; // [95] 182
L1: 

    /** 		for i=string_next to length(input_string) do*/
    if (IS_SEQUENCE(_14input_string_2261)){
            _1057 = SEQ_PTR(_14input_string_2261)->length;
    }
    else {
        _1057 = 1;
    }
    {
        int _i_2391;
        _i_2391 = _14string_next_2262;
L7: 
        if (_i_2391 > _1057){
            goto L8; // [107] 171
        }

        /** 			ch=input_string[i]*/
        _2 = (int)SEQ_PTR(_14input_string_2261);
        _14ch_2263 = (int)*(((s1_ptr)_2)->base + _i_2391);
        if (!IS_ATOM_INT(_14ch_2263)){
            _14ch_2263 = (long)DBL_PTR(_14ch_2263)->dbl;
        }

        /** 			if ch='\n' or ch='\r' then*/
        _1059 = (_14ch_2263 == 10);
        if (_1059 != 0) {
            goto L9; // [132] 147
        }
        _1061 = (_14ch_2263 == 13);
        if (_1061 == 0)
        {
            DeRef(_1061);
            _1061 = NOVALUE;
            goto LA; // [143] 164
        }
        else{
            DeRef(_1061);
            _1061 = NOVALUE;
        }
L9: 

        /** 				string_next=i+1*/
        _14string_next_2262 = _i_2391 + 1;

        /** 				return {GET_IGNORE, 0}*/
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = 0;
        _1063 = MAKE_SEQ(_1);
        DeRef(_1049);
        _1049 = NOVALUE;
        DeRef(_1051);
        _1051 = NOVALUE;
        DeRef(_1055);
        _1055 = NOVALUE;
        DeRef(_1056);
        _1056 = NOVALUE;
        DeRef(_1059);
        _1059 = NOVALUE;
        return _1063;
LA: 

        /** 		end for*/
        _i_2391 = _i_2391 + 1;
        goto L7; // [166] 114
L8: 
        ;
    }

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1064 = MAKE_SEQ(_1);
    DeRef(_1049);
    _1049 = NOVALUE;
    DeRef(_1051);
    _1051 = NOVALUE;
    DeRef(_1055);
    _1055 = NOVALUE;
    DeRef(_1056);
    _1056 = NOVALUE;
    DeRef(_1059);
    _1059 = NOVALUE;
    DeRef(_1063);
    _1063 = NOVALUE;
    return _1064;
L6: 
    ;
}


int _14get_number()
{
    int _sign_2403 = NOVALUE;
    int _e_sign_2404 = NOVALUE;
    int _ndigits_2405 = NOVALUE;
    int _hex_digit_2406 = NOVALUE;
    int _mantissa_2407 = NOVALUE;
    int _dec_2408 = NOVALUE;
    int _e_mag_2409 = NOVALUE;
    int _1132 = NOVALUE;
    int _1130 = NOVALUE;
    int _1128 = NOVALUE;
    int _1124 = NOVALUE;
    int _1120 = NOVALUE;
    int _1118 = NOVALUE;
    int _1117 = NOVALUE;
    int _1116 = NOVALUE;
    int _1115 = NOVALUE;
    int _1114 = NOVALUE;
    int _1112 = NOVALUE;
    int _1111 = NOVALUE;
    int _1110 = NOVALUE;
    int _1107 = NOVALUE;
    int _1104 = NOVALUE;
    int _1101 = NOVALUE;
    int _1097 = NOVALUE;
    int _1096 = NOVALUE;
    int _1094 = NOVALUE;
    int _1093 = NOVALUE;
    int _1092 = NOVALUE;
    int _1088 = NOVALUE;
    int _1087 = NOVALUE;
    int _1085 = NOVALUE;
    int _1084 = NOVALUE;
    int _1083 = NOVALUE;
    int _1082 = NOVALUE;
    int _1081 = NOVALUE;
    int _1080 = NOVALUE;
    int _1077 = NOVALUE;
    int _1073 = NOVALUE;
    int _1068 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sign = +1*/
    _sign_2403 = 1;

    /** 	mantissa = 0*/
    DeRef(_mantissa_2407);
    _mantissa_2407 = 0;

    /** 	ndigits = 0*/
    _ndigits_2405 = 0;

    /** 	if ch = '-' then*/
    if (_14ch_2263 != 45)
    goto L1; // [20] 54

    /** 		sign = -1*/
    _sign_2403 = -1;

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch='-' then*/
    if (_14ch_2263 != 45)
    goto L2; // [37] 68

    /** 			return read_comment()*/
    _1068 = _14read_comment();
    DeRef(_dec_2408);
    DeRef(_e_mag_2409);
    return _1068;
    goto L2; // [51] 68
L1: 

    /** 	elsif ch = '+' then*/
    if (_14ch_2263 != 43)
    goto L3; // [58] 67

    /** 		get_ch()*/
    _14get_ch();
L3: 
L2: 

    /** 	if ch = '#' then*/
    if (_14ch_2263 != 35)
    goto L4; // [72] 172

    /** 		get_ch()*/
    _14get_ch();

    /** 		while TRUE do*/
L5: 

    /** 			hex_digit = find(ch, HEX_DIGITS)-1*/
    _1073 = find_from(_14ch_2263, _14HEX_DIGITS_2243, 1);
    _hex_digit_2406 = _1073 - 1;
    _1073 = NOVALUE;

    /** 			if hex_digit >= 0 then*/
    if (_hex_digit_2406 < 0)
    goto L6; // [104] 131

    /** 				ndigits += 1*/
    _ndigits_2405 = _ndigits_2405 + 1;

    /** 				mantissa = mantissa * 16 + hex_digit*/
    if (IS_ATOM_INT(_mantissa_2407)) {
        if (_mantissa_2407 == (short)_mantissa_2407)
        _1077 = _mantissa_2407 * 16;
        else
        _1077 = NewDouble(_mantissa_2407 * (double)16);
    }
    else {
        _1077 = NewDouble(DBL_PTR(_mantissa_2407)->dbl * (double)16);
    }
    DeRef(_mantissa_2407);
    if (IS_ATOM_INT(_1077)) {
        _mantissa_2407 = _1077 + _hex_digit_2406;
        if ((long)((unsigned long)_mantissa_2407 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2407 = NewDouble((double)_mantissa_2407);
    }
    else {
        _mantissa_2407 = NewDouble(DBL_PTR(_1077)->dbl + (double)_hex_digit_2406);
    }
    DeRef(_1077);
    _1077 = NOVALUE;

    /** 				get_ch()*/
    _14get_ch();
    goto L5; // [128] 85
L6: 

    /** 				if ndigits > 0 then*/
    if (_ndigits_2405 <= 0)
    goto L7; // [133] 154

    /** 					return {GET_SUCCESS, sign * mantissa}*/
    if (IS_ATOM_INT(_mantissa_2407)) {
        if (_sign_2403 == (short)_sign_2403 && _mantissa_2407 <= INT15 && _mantissa_2407 >= -INT15)
        _1080 = _sign_2403 * _mantissa_2407;
        else
        _1080 = NewDouble(_sign_2403 * (double)_mantissa_2407);
    }
    else {
        _1080 = NewDouble((double)_sign_2403 * DBL_PTR(_mantissa_2407)->dbl);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _1080;
    _1081 = MAKE_SEQ(_1);
    _1080 = NOVALUE;
    DeRef(_mantissa_2407);
    DeRef(_dec_2408);
    DeRef(_e_mag_2409);
    DeRef(_1068);
    _1068 = NOVALUE;
    return _1081;
    goto L5; // [151] 85
L7: 

    /** 					return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1082 = MAKE_SEQ(_1);
    DeRef(_mantissa_2407);
    DeRef(_dec_2408);
    DeRef(_e_mag_2409);
    DeRef(_1068);
    _1068 = NOVALUE;
    DeRef(_1081);
    _1081 = NOVALUE;
    return _1082;

    /** 		end while*/
    goto L5; // [168] 85
L4: 

    /** 	while ch >= '0' and ch <= '9' do*/
L8: 
    _1083 = (_14ch_2263 >= 48);
    if (_1083 == 0) {
        goto L9; // [183] 228
    }
    _1085 = (_14ch_2263 <= 57);
    if (_1085 == 0)
    {
        DeRef(_1085);
        _1085 = NOVALUE;
        goto L9; // [194] 228
    }
    else{
        DeRef(_1085);
        _1085 = NOVALUE;
    }

    /** 		ndigits += 1*/
    _ndigits_2405 = _ndigits_2405 + 1;

    /** 		mantissa = mantissa * 10 + (ch - '0')*/
    if (IS_ATOM_INT(_mantissa_2407)) {
        if (_mantissa_2407 == (short)_mantissa_2407)
        _1087 = _mantissa_2407 * 10;
        else
        _1087 = NewDouble(_mantissa_2407 * (double)10);
    }
    else {
        _1087 = NewDouble(DBL_PTR(_mantissa_2407)->dbl * (double)10);
    }
    _1088 = _14ch_2263 - 48;
    if ((long)((unsigned long)_1088 +(unsigned long) HIGH_BITS) >= 0){
        _1088 = NewDouble((double)_1088);
    }
    DeRef(_mantissa_2407);
    if (IS_ATOM_INT(_1087) && IS_ATOM_INT(_1088)) {
        _mantissa_2407 = _1087 + _1088;
        if ((long)((unsigned long)_mantissa_2407 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2407 = NewDouble((double)_mantissa_2407);
    }
    else {
        if (IS_ATOM_INT(_1087)) {
            _mantissa_2407 = NewDouble((double)_1087 + DBL_PTR(_1088)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1088)) {
                _mantissa_2407 = NewDouble(DBL_PTR(_1087)->dbl + (double)_1088);
            }
            else
            _mantissa_2407 = NewDouble(DBL_PTR(_1087)->dbl + DBL_PTR(_1088)->dbl);
        }
    }
    DeRef(_1087);
    _1087 = NOVALUE;
    DeRef(_1088);
    _1088 = NOVALUE;

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L8; // [225] 177
L9: 

    /** 	if ch = '.' then*/
    if (_14ch_2263 != 46)
    goto LA; // [232] 308

    /** 		get_ch()*/
    _14get_ch();

    /** 		dec = 10*/
    DeRef(_dec_2408);
    _dec_2408 = 10;

    /** 		while ch >= '0' and ch <= '9' do*/
LB: 
    _1092 = (_14ch_2263 >= 48);
    if (_1092 == 0) {
        goto LC; // [256] 307
    }
    _1094 = (_14ch_2263 <= 57);
    if (_1094 == 0)
    {
        DeRef(_1094);
        _1094 = NOVALUE;
        goto LC; // [267] 307
    }
    else{
        DeRef(_1094);
        _1094 = NOVALUE;
    }

    /** 			ndigits += 1*/
    _ndigits_2405 = _ndigits_2405 + 1;

    /** 			mantissa += (ch - '0') / dec*/
    _1096 = _14ch_2263 - 48;
    if ((long)((unsigned long)_1096 +(unsigned long) HIGH_BITS) >= 0){
        _1096 = NewDouble((double)_1096);
    }
    if (IS_ATOM_INT(_1096) && IS_ATOM_INT(_dec_2408)) {
        _1097 = (_1096 % _dec_2408) ? NewDouble((double)_1096 / _dec_2408) : (_1096 / _dec_2408);
    }
    else {
        if (IS_ATOM_INT(_1096)) {
            _1097 = NewDouble((double)_1096 / DBL_PTR(_dec_2408)->dbl);
        }
        else {
            if (IS_ATOM_INT(_dec_2408)) {
                _1097 = NewDouble(DBL_PTR(_1096)->dbl / (double)_dec_2408);
            }
            else
            _1097 = NewDouble(DBL_PTR(_1096)->dbl / DBL_PTR(_dec_2408)->dbl);
        }
    }
    DeRef(_1096);
    _1096 = NOVALUE;
    _0 = _mantissa_2407;
    if (IS_ATOM_INT(_mantissa_2407) && IS_ATOM_INT(_1097)) {
        _mantissa_2407 = _mantissa_2407 + _1097;
        if ((long)((unsigned long)_mantissa_2407 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2407 = NewDouble((double)_mantissa_2407);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2407)) {
            _mantissa_2407 = NewDouble((double)_mantissa_2407 + DBL_PTR(_1097)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1097)) {
                _mantissa_2407 = NewDouble(DBL_PTR(_mantissa_2407)->dbl + (double)_1097);
            }
            else
            _mantissa_2407 = NewDouble(DBL_PTR(_mantissa_2407)->dbl + DBL_PTR(_1097)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1097);
    _1097 = NOVALUE;

    /** 			dec *= 10*/
    _0 = _dec_2408;
    if (IS_ATOM_INT(_dec_2408)) {
        if (_dec_2408 == (short)_dec_2408)
        _dec_2408 = _dec_2408 * 10;
        else
        _dec_2408 = NewDouble(_dec_2408 * (double)10);
    }
    else {
        _dec_2408 = NewDouble(DBL_PTR(_dec_2408)->dbl * (double)10);
    }
    DeRef(_0);

    /** 			get_ch()*/
    _14get_ch();

    /** 		end while*/
    goto LB; // [304] 250
LC: 
LA: 

    /** 	if ndigits = 0 then*/
    if (_ndigits_2405 != 0)
    goto LD; // [310] 325

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1101 = MAKE_SEQ(_1);
    DeRef(_mantissa_2407);
    DeRef(_dec_2408);
    DeRef(_e_mag_2409);
    DeRef(_1068);
    _1068 = NOVALUE;
    DeRef(_1081);
    _1081 = NOVALUE;
    DeRef(_1082);
    _1082 = NOVALUE;
    DeRef(_1083);
    _1083 = NOVALUE;
    DeRef(_1092);
    _1092 = NOVALUE;
    return _1101;
LD: 

    /** 	mantissa = sign * mantissa*/
    _0 = _mantissa_2407;
    if (IS_ATOM_INT(_mantissa_2407)) {
        if (_sign_2403 == (short)_sign_2403 && _mantissa_2407 <= INT15 && _mantissa_2407 >= -INT15)
        _mantissa_2407 = _sign_2403 * _mantissa_2407;
        else
        _mantissa_2407 = NewDouble(_sign_2403 * (double)_mantissa_2407);
    }
    else {
        _mantissa_2407 = NewDouble((double)_sign_2403 * DBL_PTR(_mantissa_2407)->dbl);
    }
    DeRef(_0);

    /** 	if ch = 'e' or ch = 'E' then*/
    _1104 = (_14ch_2263 == 101);
    if (_1104 != 0) {
        goto LE; // [339] 354
    }
    _1107 = (_14ch_2263 == 69);
    if (_1107 == 0)
    {
        DeRef(_1107);
        _1107 = NOVALUE;
        goto LF; // [350] 575
    }
    else{
        DeRef(_1107);
        _1107 = NOVALUE;
    }
LE: 

    /** 		e_sign = +1*/
    _e_sign_2404 = 1;

    /** 		e_mag = 0*/
    DeRef(_e_mag_2409);
    _e_mag_2409 = 0;

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch = '-' then*/
    if (_14ch_2263 != 45)
    goto L10; // [372] 388

    /** 			e_sign = -1*/
    _e_sign_2404 = -1;

    /** 			get_ch()*/
    _14get_ch();
    goto L11; // [385] 402
L10: 

    /** 		elsif ch = '+' then*/
    if (_14ch_2263 != 43)
    goto L12; // [392] 401

    /** 			get_ch()*/
    _14get_ch();
L12: 
L11: 

    /** 		if ch >= '0' and ch <= '9' then*/
    _1110 = (_14ch_2263 >= 48);
    if (_1110 == 0) {
        goto L13; // [410] 489
    }
    _1112 = (_14ch_2263 <= 57);
    if (_1112 == 0)
    {
        DeRef(_1112);
        _1112 = NOVALUE;
        goto L13; // [421] 489
    }
    else{
        DeRef(_1112);
        _1112 = NOVALUE;
    }

    /** 			e_mag = ch - '0'*/
    DeRef(_e_mag_2409);
    _e_mag_2409 = _14ch_2263 - 48;
    if ((long)((unsigned long)_e_mag_2409 +(unsigned long) HIGH_BITS) >= 0){
        _e_mag_2409 = NewDouble((double)_e_mag_2409);
    }

    /** 			get_ch()*/
    _14get_ch();

    /** 			while ch >= '0' and ch <= '9' do*/
L14: 
    _1114 = (_14ch_2263 >= 48);
    if (_1114 == 0) {
        goto L15; // [447] 500
    }
    _1116 = (_14ch_2263 <= 57);
    if (_1116 == 0)
    {
        DeRef(_1116);
        _1116 = NOVALUE;
        goto L15; // [458] 500
    }
    else{
        DeRef(_1116);
        _1116 = NOVALUE;
    }

    /** 				e_mag = e_mag * 10 + ch - '0'*/
    if (IS_ATOM_INT(_e_mag_2409)) {
        if (_e_mag_2409 == (short)_e_mag_2409)
        _1117 = _e_mag_2409 * 10;
        else
        _1117 = NewDouble(_e_mag_2409 * (double)10);
    }
    else {
        _1117 = NewDouble(DBL_PTR(_e_mag_2409)->dbl * (double)10);
    }
    if (IS_ATOM_INT(_1117)) {
        _1118 = _1117 + _14ch_2263;
        if ((long)((unsigned long)_1118 + (unsigned long)HIGH_BITS) >= 0) 
        _1118 = NewDouble((double)_1118);
    }
    else {
        _1118 = NewDouble(DBL_PTR(_1117)->dbl + (double)_14ch_2263);
    }
    DeRef(_1117);
    _1117 = NOVALUE;
    DeRef(_e_mag_2409);
    if (IS_ATOM_INT(_1118)) {
        _e_mag_2409 = _1118 - 48;
        if ((long)((unsigned long)_e_mag_2409 +(unsigned long) HIGH_BITS) >= 0){
            _e_mag_2409 = NewDouble((double)_e_mag_2409);
        }
    }
    else {
        _e_mag_2409 = NewDouble(DBL_PTR(_1118)->dbl - (double)48);
    }
    DeRef(_1118);
    _1118 = NOVALUE;

    /** 				get_ch()*/
    _14get_ch();

    /** 			end while*/
    goto L14; // [483] 441
    goto L15; // [486] 500
L13: 

    /** 			return {GET_FAIL, 0} -- no exponent*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1120 = MAKE_SEQ(_1);
    DeRef(_mantissa_2407);
    DeRef(_dec_2408);
    DeRef(_e_mag_2409);
    DeRef(_1068);
    _1068 = NOVALUE;
    DeRef(_1081);
    _1081 = NOVALUE;
    DeRef(_1082);
    _1082 = NOVALUE;
    DeRef(_1083);
    _1083 = NOVALUE;
    DeRef(_1092);
    _1092 = NOVALUE;
    DeRef(_1101);
    _1101 = NOVALUE;
    DeRef(_1104);
    _1104 = NOVALUE;
    DeRef(_1110);
    _1110 = NOVALUE;
    DeRef(_1114);
    _1114 = NOVALUE;
    return _1120;
L15: 

    /** 		e_mag *= e_sign*/
    _0 = _e_mag_2409;
    if (IS_ATOM_INT(_e_mag_2409)) {
        if (_e_mag_2409 == (short)_e_mag_2409 && _e_sign_2404 <= INT15 && _e_sign_2404 >= -INT15)
        _e_mag_2409 = _e_mag_2409 * _e_sign_2404;
        else
        _e_mag_2409 = NewDouble(_e_mag_2409 * (double)_e_sign_2404);
    }
    else {
        _e_mag_2409 = NewDouble(DBL_PTR(_e_mag_2409)->dbl * (double)_e_sign_2404);
    }
    DeRef(_0);

    /** 		if e_mag > 308 then*/
    if (binary_op_a(LESSEQ, _e_mag_2409, 308)){
        goto L16; // [508] 563
    }

    /** 			mantissa *= power(10, 308)*/
    _1124 = power(10, 308);
    _0 = _mantissa_2407;
    if (IS_ATOM_INT(_mantissa_2407) && IS_ATOM_INT(_1124)) {
        if (_mantissa_2407 == (short)_mantissa_2407 && _1124 <= INT15 && _1124 >= -INT15)
        _mantissa_2407 = _mantissa_2407 * _1124;
        else
        _mantissa_2407 = NewDouble(_mantissa_2407 * (double)_1124);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2407)) {
            _mantissa_2407 = NewDouble((double)_mantissa_2407 * DBL_PTR(_1124)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1124)) {
                _mantissa_2407 = NewDouble(DBL_PTR(_mantissa_2407)->dbl * (double)_1124);
            }
            else
            _mantissa_2407 = NewDouble(DBL_PTR(_mantissa_2407)->dbl * DBL_PTR(_1124)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1124);
    _1124 = NOVALUE;

    /** 			if e_mag > 1000 then*/
    if (binary_op_a(LESSEQ, _e_mag_2409, 1000)){
        goto L17; // [524] 534
    }

    /** 				e_mag = 1000*/
    DeRef(_e_mag_2409);
    _e_mag_2409 = 1000;
L17: 

    /** 			for i = 1 to e_mag - 308 do*/
    if (IS_ATOM_INT(_e_mag_2409)) {
        _1128 = _e_mag_2409 - 308;
        if ((long)((unsigned long)_1128 +(unsigned long) HIGH_BITS) >= 0){
            _1128 = NewDouble((double)_1128);
        }
    }
    else {
        _1128 = NewDouble(DBL_PTR(_e_mag_2409)->dbl - (double)308);
    }
    {
        int _i_2495;
        _i_2495 = 1;
L18: 
        if (binary_op_a(GREATER, _i_2495, _1128)){
            goto L19; // [540] 560
        }

        /** 				mantissa *= 10*/
        _0 = _mantissa_2407;
        if (IS_ATOM_INT(_mantissa_2407)) {
            if (_mantissa_2407 == (short)_mantissa_2407)
            _mantissa_2407 = _mantissa_2407 * 10;
            else
            _mantissa_2407 = NewDouble(_mantissa_2407 * (double)10);
        }
        else {
            _mantissa_2407 = NewDouble(DBL_PTR(_mantissa_2407)->dbl * (double)10);
        }
        DeRef(_0);

        /** 			end for*/
        _0 = _i_2495;
        if (IS_ATOM_INT(_i_2495)) {
            _i_2495 = _i_2495 + 1;
            if ((long)((unsigned long)_i_2495 +(unsigned long) HIGH_BITS) >= 0){
                _i_2495 = NewDouble((double)_i_2495);
            }
        }
        else {
            _i_2495 = binary_op_a(PLUS, _i_2495, 1);
        }
        DeRef(_0);
        goto L18; // [555] 547
L19: 
        ;
        DeRef(_i_2495);
    }
    goto L1A; // [560] 574
L16: 

    /** 			mantissa *= power(10, e_mag)*/
    if (IS_ATOM_INT(_e_mag_2409)) {
        _1130 = power(10, _e_mag_2409);
    }
    else {
        temp_d.dbl = (double)10;
        _1130 = Dpower(&temp_d, DBL_PTR(_e_mag_2409));
    }
    _0 = _mantissa_2407;
    if (IS_ATOM_INT(_mantissa_2407) && IS_ATOM_INT(_1130)) {
        if (_mantissa_2407 == (short)_mantissa_2407 && _1130 <= INT15 && _1130 >= -INT15)
        _mantissa_2407 = _mantissa_2407 * _1130;
        else
        _mantissa_2407 = NewDouble(_mantissa_2407 * (double)_1130);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2407)) {
            _mantissa_2407 = NewDouble((double)_mantissa_2407 * DBL_PTR(_1130)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1130)) {
                _mantissa_2407 = NewDouble(DBL_PTR(_mantissa_2407)->dbl * (double)_1130);
            }
            else
            _mantissa_2407 = NewDouble(DBL_PTR(_mantissa_2407)->dbl * DBL_PTR(_1130)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1130);
    _1130 = NOVALUE;
L1A: 
LF: 

    /** 	return {GET_SUCCESS, mantissa}*/
    Ref(_mantissa_2407);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _mantissa_2407;
    _1132 = MAKE_SEQ(_1);
    DeRef(_mantissa_2407);
    DeRef(_dec_2408);
    DeRef(_e_mag_2409);
    DeRef(_1068);
    _1068 = NOVALUE;
    DeRef(_1081);
    _1081 = NOVALUE;
    DeRef(_1082);
    _1082 = NOVALUE;
    DeRef(_1083);
    _1083 = NOVALUE;
    DeRef(_1092);
    _1092 = NOVALUE;
    DeRef(_1101);
    _1101 = NOVALUE;
    DeRef(_1104);
    _1104 = NOVALUE;
    DeRef(_1110);
    _1110 = NOVALUE;
    DeRef(_1114);
    _1114 = NOVALUE;
    DeRef(_1120);
    _1120 = NOVALUE;
    DeRef(_1128);
    _1128 = NOVALUE;
    return _1132;
    ;
}


int _14Get()
{
    int _skip_blanks_1__tmp_at330_2549 = NOVALUE;
    int _skip_blanks_1__tmp_at177_2530 = NOVALUE;
    int _skip_blanks_1__tmp_at88_2520 = NOVALUE;
    int _s_2504 = NOVALUE;
    int _e_2505 = NOVALUE;
    int _e1_2506 = NOVALUE;
    int _1173 = NOVALUE;
    int _1172 = NOVALUE;
    int _1170 = NOVALUE;
    int _1167 = NOVALUE;
    int _1165 = NOVALUE;
    int _1162 = NOVALUE;
    int _1160 = NOVALUE;
    int _1157 = NOVALUE;
    int _1155 = NOVALUE;
    int _1151 = NOVALUE;
    int _1147 = NOVALUE;
    int _1144 = NOVALUE;
    int _1143 = NOVALUE;
    int _1140 = NOVALUE;
    int _1138 = NOVALUE;
    int _1136 = NOVALUE;
    int _1135 = NOVALUE;
    int _1133 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while find(ch, white_space) do*/
L1: 
    _1133 = find_from(_14ch_2263, _14white_space_2279, 1);
    if (_1133 == 0)
    {
        _1133 = NOVALUE;
        goto L2; // [13] 25
    }
    else{
        _1133 = NOVALUE;
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L1; // [22] 6
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_14ch_2263 != -1)
    goto L3; // [29] 44

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1135 = MAKE_SEQ(_1);
    DeRef(_s_2504);
    DeRef(_e_2505);
    return _1135;
L3: 

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _1136 = find_from(_14ch_2263, _14START_NUMERIC_2246, 1);
    if (_1136 == 0)
    {
        _1136 = NOVALUE;
        goto L5; // [60] 157
    }
    else{
        _1136 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_2505;
    _e_2505 = _14get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_2505);
    _1138 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1138, -2)){
        _1138 = NOVALUE;
        goto L6; // [76] 87
    }
    _1138 = NOVALUE;

    /** 				return e*/
    DeRef(_s_2504);
    DeRef(_1135);
    _1135 = NOVALUE;
    return _e_2505;
L6: 

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L7: 
    _skip_blanks_1__tmp_at88_2520 = find_from(_14ch_2263, _14white_space_2279, 1);
    if (_skip_blanks_1__tmp_at88_2520 == 0)
    {
        goto L8; // [101] 118
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L7; // [110] 94

    /** end procedure*/
    goto L8; // [115] 118
L8: 

    /** 			if ch=-1 or ch='}' then -- '}' is expected only in the "{--\n}" case*/
    _1140 = (_14ch_2263 == -1);
    if (_1140 != 0) {
        goto L9; // [128] 143
    }
    _1143 = (_14ch_2263 == 125);
    if (_1143 == 0)
    {
        DeRef(_1143);
        _1143 = NOVALUE;
        goto L4; // [139] 49
    }
    else{
        DeRef(_1143);
        _1143 = NOVALUE;
    }
L9: 

    /** 				return {GET_NOTHING, 0} -- just a comment*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _1144 = MAKE_SEQ(_1);
    DeRef(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    return _1144;
    goto L4; // [154] 49
L5: 

    /** 		elsif ch = '{' then*/
    if (_14ch_2263 != 123)
    goto LA; // [161] 467

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_2504);
    _s_2504 = _5;

    /** 			get_ch()*/
    _14get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
LB: 
    _skip_blanks_1__tmp_at177_2530 = find_from(_14ch_2263, _14white_space_2279, 1);
    if (_skip_blanks_1__tmp_at177_2530 == 0)
    {
        goto LC; // [190] 207
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto LB; // [199] 183

    /** end procedure*/
    goto LC; // [204] 207
LC: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_14ch_2263 != 125)
    goto LD; // [213] 232

    /** 				get_ch()*/
    _14get_ch();

    /** 				return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_2504);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2504;
    _1147 = MAKE_SEQ(_1);
    DeRefDS(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    return _1147;
LD: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LE: 

    /** 				while 1 do -- read zero or more comments and an element*/
LF: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_2505;
    _e_2505 = _14Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_2505);
    _e1_2506 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_2506))
    _e1_2506 = (long)DBL_PTR(_e1_2506)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_2506 != 0)
    goto L10; // [259] 280

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_2505);
    _1151 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_1151);
    Append(&_s_2504, _s_2504, _1151);
    _1151 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto L11; // [275] 324
    goto LF; // [277] 242
L10: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_2506 == -2)
    goto L12; // [282] 295

    /** 						return e*/
    DeRef(_s_2504);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    return _e_2505;
    goto LF; // [292] 242
L12: 

    /** 					elsif ch='}' then*/
    if (_14ch_2263 != 125)
    goto LF; // [299] 242

    /** 						get_ch()*/
    _14get_ch();

    /** 						return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_2504);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2504;
    _1155 = MAKE_SEQ(_1);
    DeRefDS(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    return _1155;

    /** 				end while*/
    goto LF; // [321] 242
L11: 

    /** 				while 1 do -- now read zero or more post element comments*/
L13: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L14: 
    _skip_blanks_1__tmp_at330_2549 = find_from(_14ch_2263, _14white_space_2279, 1);
    if (_skip_blanks_1__tmp_at330_2549 == 0)
    {
        goto L15; // [343] 360
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L14; // [352] 336

    /** end procedure*/
    goto L15; // [357] 360
L15: 

    /** 					if ch = '}' then*/
    if (_14ch_2263 != 125)
    goto L16; // [366] 387

    /** 						get_ch()*/
    _14get_ch();

    /** 					return {GET_SUCCESS, s}*/
    RefDS(_s_2504);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2504;
    _1157 = MAKE_SEQ(_1);
    DeRefDS(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    DeRef(_1155);
    _1155 = NOVALUE;
    return _1157;
    goto L13; // [384] 329
L16: 

    /** 					elsif ch!='-' then*/
    if (_14ch_2263 == 45)
    goto L17; // [391] 402

    /** 						exit*/
    goto L18; // [397] 436
    goto L13; // [399] 329
L17: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_2505;
    _e_2505 = _14get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it wasn't a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_2505);
    _1160 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1160, -2)){
        _1160 = NOVALUE;
        goto L13; // [415] 329
    }
    _1160 = NOVALUE;

    /** 							return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1162 = MAKE_SEQ(_1);
    DeRef(_s_2504);
    DeRefDS(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    DeRef(_1155);
    _1155 = NOVALUE;
    DeRef(_1157);
    _1157 = NOVALUE;
    return _1162;

    /** 			end while*/
    goto L13; // [433] 329
L18: 

    /** 				if ch != ',' then*/
    if (_14ch_2263 == 44)
    goto L19; // [440] 455

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1165 = MAKE_SEQ(_1);
    DeRef(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    DeRef(_1155);
    _1155 = NOVALUE;
    DeRef(_1157);
    _1157 = NOVALUE;
    DeRef(_1162);
    _1162 = NOVALUE;
    return _1165;
L19: 

    /** 			get_ch() -- skip comma*/
    _14get_ch();

    /** 			end while*/
    goto LE; // [461] 237
    goto L4; // [464] 49
LA: 

    /** 		elsif ch = '\"' then*/
    if (_14ch_2263 != 34)
    goto L1A; // [471] 487

    /** 			return get_string()*/
    _1167 = _14get_string();
    DeRef(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    DeRef(_1155);
    _1155 = NOVALUE;
    DeRef(_1157);
    _1157 = NOVALUE;
    DeRef(_1162);
    _1162 = NOVALUE;
    DeRef(_1165);
    _1165 = NOVALUE;
    return _1167;
    goto L4; // [484] 49
L1A: 

    /** 		elsif ch = '`' then*/
    if (_14ch_2263 != 96)
    goto L1B; // [491] 508

    /** 			return get_heredoc("`")*/
    RefDS(_1169);
    _1170 = _14get_heredoc(_1169);
    DeRef(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    DeRef(_1155);
    _1155 = NOVALUE;
    DeRef(_1157);
    _1157 = NOVALUE;
    DeRef(_1162);
    _1162 = NOVALUE;
    DeRef(_1165);
    _1165 = NOVALUE;
    DeRef(_1167);
    _1167 = NOVALUE;
    return _1170;
    goto L4; // [505] 49
L1B: 

    /** 		elsif ch = '\'' then*/
    if (_14ch_2263 != 39)
    goto L1C; // [512] 528

    /** 			return get_qchar()*/
    _1172 = _14get_qchar();
    DeRef(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    DeRef(_1155);
    _1155 = NOVALUE;
    DeRef(_1157);
    _1157 = NOVALUE;
    DeRef(_1162);
    _1162 = NOVALUE;
    DeRef(_1165);
    _1165 = NOVALUE;
    DeRef(_1167);
    _1167 = NOVALUE;
    DeRef(_1170);
    _1170 = NOVALUE;
    return _1172;
    goto L4; // [525] 49
L1C: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1173 = MAKE_SEQ(_1);
    DeRef(_s_2504);
    DeRef(_e_2505);
    DeRef(_1135);
    _1135 = NOVALUE;
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1144);
    _1144 = NOVALUE;
    DeRef(_1147);
    _1147 = NOVALUE;
    DeRef(_1155);
    _1155 = NOVALUE;
    DeRef(_1157);
    _1157 = NOVALUE;
    DeRef(_1162);
    _1162 = NOVALUE;
    DeRef(_1165);
    _1165 = NOVALUE;
    DeRef(_1167);
    _1167 = NOVALUE;
    DeRef(_1170);
    _1170 = NOVALUE;
    DeRef(_1172);
    _1172 = NOVALUE;
    return _1173;

    /** 	end while*/
    goto L4; // [541] 49
    ;
}


int _14Get2()
{
    int _skip_blanks_1__tmp_at468_2651 = NOVALUE;
    int _skip_blanks_1__tmp_at235_2618 = NOVALUE;
    int _s_2580 = NOVALUE;
    int _e_2581 = NOVALUE;
    int _e1_2582 = NOVALUE;
    int _offset_2583 = NOVALUE;
    int _1273 = NOVALUE;
    int _1272 = NOVALUE;
    int _1271 = NOVALUE;
    int _1270 = NOVALUE;
    int _1269 = NOVALUE;
    int _1268 = NOVALUE;
    int _1267 = NOVALUE;
    int _1266 = NOVALUE;
    int _1265 = NOVALUE;
    int _1264 = NOVALUE;
    int _1263 = NOVALUE;
    int _1260 = NOVALUE;
    int _1259 = NOVALUE;
    int _1258 = NOVALUE;
    int _1257 = NOVALUE;
    int _1256 = NOVALUE;
    int _1255 = NOVALUE;
    int _1252 = NOVALUE;
    int _1251 = NOVALUE;
    int _1250 = NOVALUE;
    int _1249 = NOVALUE;
    int _1248 = NOVALUE;
    int _1247 = NOVALUE;
    int _1244 = NOVALUE;
    int _1243 = NOVALUE;
    int _1242 = NOVALUE;
    int _1241 = NOVALUE;
    int _1240 = NOVALUE;
    int _1238 = NOVALUE;
    int _1237 = NOVALUE;
    int _1236 = NOVALUE;
    int _1235 = NOVALUE;
    int _1234 = NOVALUE;
    int _1232 = NOVALUE;
    int _1229 = NOVALUE;
    int _1228 = NOVALUE;
    int _1227 = NOVALUE;
    int _1226 = NOVALUE;
    int _1225 = NOVALUE;
    int _1223 = NOVALUE;
    int _1222 = NOVALUE;
    int _1221 = NOVALUE;
    int _1220 = NOVALUE;
    int _1219 = NOVALUE;
    int _1217 = NOVALUE;
    int _1216 = NOVALUE;
    int _1215 = NOVALUE;
    int _1214 = NOVALUE;
    int _1213 = NOVALUE;
    int _1212 = NOVALUE;
    int _1209 = NOVALUE;
    int _1205 = NOVALUE;
    int _1204 = NOVALUE;
    int _1203 = NOVALUE;
    int _1202 = NOVALUE;
    int _1201 = NOVALUE;
    int _1198 = NOVALUE;
    int _1197 = NOVALUE;
    int _1196 = NOVALUE;
    int _1195 = NOVALUE;
    int _1194 = NOVALUE;
    int _1192 = NOVALUE;
    int _1191 = NOVALUE;
    int _1190 = NOVALUE;
    int _1189 = NOVALUE;
    int _1188 = NOVALUE;
    int _1187 = NOVALUE;
    int _1185 = NOVALUE;
    int _1183 = NOVALUE;
    int _1181 = NOVALUE;
    int _1180 = NOVALUE;
    int _1179 = NOVALUE;
    int _1178 = NOVALUE;
    int _1177 = NOVALUE;
    int _1175 = NOVALUE;
    int _0, _1, _2;
    

    /** 	offset = string_next-1*/
    _offset_2583 = _14string_next_2262 - 1;

    /** 	get_ch()*/
    _14get_ch();

    /** 	while find(ch, white_space) do*/
L1: 
    _1175 = find_from(_14ch_2263, _14white_space_2279, 1);
    if (_1175 == 0)
    {
        _1175 = NOVALUE;
        goto L2; // [25] 37
    }
    else{
        _1175 = NOVALUE;
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L1; // [34] 18
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_14ch_2263 != -1)
    goto L3; // [41] 75

    /** 		return {GET_EOF, 0, string_next-1-offset ,string_next-1}*/
    _1177 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1177 +(unsigned long) HIGH_BITS) >= 0){
        _1177 = NewDouble((double)_1177);
    }
    if (IS_ATOM_INT(_1177)) {
        _1178 = _1177 - _offset_2583;
        if ((long)((unsigned long)_1178 +(unsigned long) HIGH_BITS) >= 0){
            _1178 = NewDouble((double)_1178);
        }
    }
    else {
        _1178 = NewDouble(DBL_PTR(_1177)->dbl - (double)_offset_2583);
    }
    DeRef(_1177);
    _1177 = NOVALUE;
    _1179 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1179 +(unsigned long) HIGH_BITS) >= 0){
        _1179 = NewDouble((double)_1179);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1178;
    *((int *)(_2+16)) = _1179;
    _1180 = MAKE_SEQ(_1);
    _1179 = NOVALUE;
    _1178 = NOVALUE;
    DeRef(_s_2580);
    DeRef(_e_2581);
    return _1180;
L3: 

    /** 	leading_whitespace = string_next-2-offset -- index of the last whitespace: string_next points past the first non whitespace*/
    _1181 = _14string_next_2262 - 2;
    if ((long)((unsigned long)_1181 +(unsigned long) HIGH_BITS) >= 0){
        _1181 = NewDouble((double)_1181);
    }
    if (IS_ATOM_INT(_1181)) {
        _14leading_whitespace_2577 = _1181 - _offset_2583;
    }
    else {
        _14leading_whitespace_2577 = NewDouble(DBL_PTR(_1181)->dbl - (double)_offset_2583);
    }
    DeRef(_1181);
    _1181 = NOVALUE;
    if (!IS_ATOM_INT(_14leading_whitespace_2577)) {
        _1 = (long)(DBL_PTR(_14leading_whitespace_2577)->dbl);
        if (UNIQUE(DBL_PTR(_14leading_whitespace_2577)) && (DBL_PTR(_14leading_whitespace_2577)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14leading_whitespace_2577);
        _14leading_whitespace_2577 = _1;
    }

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _1183 = find_from(_14ch_2263, _14START_NUMERIC_2246, 1);
    if (_1183 == 0)
    {
        _1183 = NOVALUE;
        goto L5; // [107] 215
    }
    else{
        _1183 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_2581;
    _e_2581 = _14get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_2581);
    _1185 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1185, -2)){
        _1185 = NOVALUE;
        goto L6; // [123] 164
    }
    _1185 = NOVALUE;

    /** 				return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1187 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1187 +(unsigned long) HIGH_BITS) >= 0){
        _1187 = NewDouble((double)_1187);
    }
    if (IS_ATOM_INT(_1187)) {
        _1188 = _1187 - _offset_2583;
        if ((long)((unsigned long)_1188 +(unsigned long) HIGH_BITS) >= 0){
            _1188 = NewDouble((double)_1188);
        }
    }
    else {
        _1188 = NewDouble(DBL_PTR(_1187)->dbl - (double)_offset_2583);
    }
    DeRef(_1187);
    _1187 = NOVALUE;
    _1189 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1188)) {
        _1190 = _1188 - _1189;
        if ((long)((unsigned long)_1190 +(unsigned long) HIGH_BITS) >= 0){
            _1190 = NewDouble((double)_1190);
        }
    }
    else {
        _1190 = NewDouble(DBL_PTR(_1188)->dbl - (double)_1189);
    }
    DeRef(_1188);
    _1188 = NOVALUE;
    _1189 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1190;
    ((int *)_2)[2] = _14leading_whitespace_2577;
    _1191 = MAKE_SEQ(_1);
    _1190 = NOVALUE;
    Concat((object_ptr)&_1192, _e_2581, _1191);
    DeRefDS(_1191);
    _1191 = NOVALUE;
    DeRef(_s_2580);
    DeRefDS(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    return _1192;
L6: 

    /** 			get_ch()*/
    _14get_ch();

    /** 			if ch=-1 then*/
    if (_14ch_2263 != -1)
    goto L4; // [172] 96

    /** 				return {GET_NOTHING, 0, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _1194 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1194 +(unsigned long) HIGH_BITS) >= 0){
        _1194 = NewDouble((double)_1194);
    }
    if (IS_ATOM_INT(_1194)) {
        _1195 = _1194 - _offset_2583;
        if ((long)((unsigned long)_1195 +(unsigned long) HIGH_BITS) >= 0){
            _1195 = NewDouble((double)_1195);
        }
    }
    else {
        _1195 = NewDouble(DBL_PTR(_1194)->dbl - (double)_offset_2583);
    }
    DeRef(_1194);
    _1194 = NOVALUE;
    _1196 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1195)) {
        _1197 = _1195 - _1196;
        if ((long)((unsigned long)_1197 +(unsigned long) HIGH_BITS) >= 0){
            _1197 = NewDouble((double)_1197);
        }
    }
    else {
        _1197 = NewDouble(DBL_PTR(_1195)->dbl - (double)_1196);
    }
    DeRef(_1195);
    _1195 = NOVALUE;
    _1196 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1197;
    *((int *)(_2+16)) = _14leading_whitespace_2577;
    _1198 = MAKE_SEQ(_1);
    _1197 = NOVALUE;
    DeRef(_s_2580);
    DeRef(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    return _1198;
    goto L4; // [212] 96
L5: 

    /** 		elsif ch = '{' then*/
    if (_14ch_2263 != 123)
    goto L7; // [219] 680

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_2580);
    _s_2580 = _5;

    /** 			get_ch()*/
    _14get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L8: 
    _skip_blanks_1__tmp_at235_2618 = find_from(_14ch_2263, _14white_space_2279, 1);
    if (_skip_blanks_1__tmp_at235_2618 == 0)
    {
        goto L9; // [248] 265
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L8; // [257] 241

    /** end procedure*/
    goto L9; // [262] 265
L9: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_14ch_2263 != 125)
    goto LA; // [271] 315

    /** 				get_ch()*/
    _14get_ch();

    /** 				return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _1201 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1201 +(unsigned long) HIGH_BITS) >= 0){
        _1201 = NewDouble((double)_1201);
    }
    if (IS_ATOM_INT(_1201)) {
        _1202 = _1201 - _offset_2583;
        if ((long)((unsigned long)_1202 +(unsigned long) HIGH_BITS) >= 0){
            _1202 = NewDouble((double)_1202);
        }
    }
    else {
        _1202 = NewDouble(DBL_PTR(_1201)->dbl - (double)_offset_2583);
    }
    DeRef(_1201);
    _1201 = NOVALUE;
    _1203 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1202)) {
        _1204 = _1202 - _1203;
        if ((long)((unsigned long)_1204 +(unsigned long) HIGH_BITS) >= 0){
            _1204 = NewDouble((double)_1204);
        }
    }
    else {
        _1204 = NewDouble(DBL_PTR(_1202)->dbl - (double)_1203);
    }
    DeRef(_1202);
    _1202 = NOVALUE;
    _1203 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_2580);
    *((int *)(_2+8)) = _s_2580;
    *((int *)(_2+12)) = _1204;
    *((int *)(_2+16)) = _14leading_whitespace_2577;
    _1205 = MAKE_SEQ(_1);
    _1204 = NOVALUE;
    DeRefDS(_s_2580);
    DeRef(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    return _1205;
LA: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LB: 

    /** 				while 1 do -- read zero or more comments and an element*/
LC: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_2581;
    _e_2581 = _14Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_2581);
    _e1_2582 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_2582))
    _e1_2582 = (long)DBL_PTR(_e1_2582)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_2582 != 0)
    goto LD; // [342] 363

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_2581);
    _1209 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_1209);
    Append(&_s_2580, _s_2580, _1209);
    _1209 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto LE; // [358] 462
    goto LC; // [360] 325
LD: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_2582 == -2)
    goto LF; // [365] 408

    /** 						return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1212 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1212 +(unsigned long) HIGH_BITS) >= 0){
        _1212 = NewDouble((double)_1212);
    }
    if (IS_ATOM_INT(_1212)) {
        _1213 = _1212 - _offset_2583;
        if ((long)((unsigned long)_1213 +(unsigned long) HIGH_BITS) >= 0){
            _1213 = NewDouble((double)_1213);
        }
    }
    else {
        _1213 = NewDouble(DBL_PTR(_1212)->dbl - (double)_offset_2583);
    }
    DeRef(_1212);
    _1212 = NOVALUE;
    _1214 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1213)) {
        _1215 = _1213 - _1214;
        if ((long)((unsigned long)_1215 +(unsigned long) HIGH_BITS) >= 0){
            _1215 = NewDouble((double)_1215);
        }
    }
    else {
        _1215 = NewDouble(DBL_PTR(_1213)->dbl - (double)_1214);
    }
    DeRef(_1213);
    _1213 = NOVALUE;
    _1214 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1215;
    ((int *)_2)[2] = _14leading_whitespace_2577;
    _1216 = MAKE_SEQ(_1);
    _1215 = NOVALUE;
    Concat((object_ptr)&_1217, _e_2581, _1216);
    DeRefDS(_1216);
    _1216 = NOVALUE;
    DeRef(_s_2580);
    DeRefDS(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    return _1217;
    goto LC; // [405] 325
LF: 

    /** 					elsif ch='}' then*/
    if (_14ch_2263 != 125)
    goto LC; // [412] 325

    /** 						get_ch()*/
    _14get_ch();

    /** 						return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1),leading_whitespace} -- empty sequence*/
    _1219 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1219 +(unsigned long) HIGH_BITS) >= 0){
        _1219 = NewDouble((double)_1219);
    }
    if (IS_ATOM_INT(_1219)) {
        _1220 = _1219 - _offset_2583;
        if ((long)((unsigned long)_1220 +(unsigned long) HIGH_BITS) >= 0){
            _1220 = NewDouble((double)_1220);
        }
    }
    else {
        _1220 = NewDouble(DBL_PTR(_1219)->dbl - (double)_offset_2583);
    }
    DeRef(_1219);
    _1219 = NOVALUE;
    _1221 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1220)) {
        _1222 = _1220 - _1221;
        if ((long)((unsigned long)_1222 +(unsigned long) HIGH_BITS) >= 0){
            _1222 = NewDouble((double)_1222);
        }
    }
    else {
        _1222 = NewDouble(DBL_PTR(_1220)->dbl - (double)_1221);
    }
    DeRef(_1220);
    _1220 = NOVALUE;
    _1221 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_2580);
    *((int *)(_2+8)) = _s_2580;
    *((int *)(_2+12)) = _1222;
    *((int *)(_2+16)) = _14leading_whitespace_2577;
    _1223 = MAKE_SEQ(_1);
    _1222 = NOVALUE;
    DeRefDS(_s_2580);
    DeRef(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    return _1223;

    /** 				end while*/
    goto LC; // [459] 325
LE: 

    /** 				while 1 do -- now read zero or more post element comments*/
L10: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L11: 
    _skip_blanks_1__tmp_at468_2651 = find_from(_14ch_2263, _14white_space_2279, 1);
    if (_skip_blanks_1__tmp_at468_2651 == 0)
    {
        goto L12; // [481] 498
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L11; // [490] 474

    /** end procedure*/
    goto L12; // [495] 498
L12: 

    /** 					if ch = '}' then*/
    if (_14ch_2263 != 125)
    goto L13; // [504] 550

    /** 						get_ch()*/
    _14get_ch();

    /** 					return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1225 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1225 +(unsigned long) HIGH_BITS) >= 0){
        _1225 = NewDouble((double)_1225);
    }
    if (IS_ATOM_INT(_1225)) {
        _1226 = _1225 - _offset_2583;
        if ((long)((unsigned long)_1226 +(unsigned long) HIGH_BITS) >= 0){
            _1226 = NewDouble((double)_1226);
        }
    }
    else {
        _1226 = NewDouble(DBL_PTR(_1225)->dbl - (double)_offset_2583);
    }
    DeRef(_1225);
    _1225 = NOVALUE;
    _1227 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1226)) {
        _1228 = _1226 - _1227;
        if ((long)((unsigned long)_1228 +(unsigned long) HIGH_BITS) >= 0){
            _1228 = NewDouble((double)_1228);
        }
    }
    else {
        _1228 = NewDouble(DBL_PTR(_1226)->dbl - (double)_1227);
    }
    DeRef(_1226);
    _1226 = NOVALUE;
    _1227 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_2580);
    *((int *)(_2+8)) = _s_2580;
    *((int *)(_2+12)) = _1228;
    *((int *)(_2+16)) = _14leading_whitespace_2577;
    _1229 = MAKE_SEQ(_1);
    _1228 = NOVALUE;
    DeRefDS(_s_2580);
    DeRef(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    DeRef(_1223);
    _1223 = NOVALUE;
    return _1229;
    goto L10; // [547] 467
L13: 

    /** 					elsif ch!='-' then*/
    if (_14ch_2263 == 45)
    goto L14; // [554] 565

    /** 						exit*/
    goto L15; // [560] 624
    goto L10; // [562] 467
L14: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_2581;
    _e_2581 = _14get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it was not a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_2581);
    _1232 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1232, -2)){
        _1232 = NOVALUE;
        goto L10; // [578] 467
    }
    _1232 = NOVALUE;

    /** 							return {GET_FAIL, 0, string_next-1-offset-(ch!=-1),leading_whitespace}*/
    _1234 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1234 +(unsigned long) HIGH_BITS) >= 0){
        _1234 = NewDouble((double)_1234);
    }
    if (IS_ATOM_INT(_1234)) {
        _1235 = _1234 - _offset_2583;
        if ((long)((unsigned long)_1235 +(unsigned long) HIGH_BITS) >= 0){
            _1235 = NewDouble((double)_1235);
        }
    }
    else {
        _1235 = NewDouble(DBL_PTR(_1234)->dbl - (double)_offset_2583);
    }
    DeRef(_1234);
    _1234 = NOVALUE;
    _1236 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1235)) {
        _1237 = _1235 - _1236;
        if ((long)((unsigned long)_1237 +(unsigned long) HIGH_BITS) >= 0){
            _1237 = NewDouble((double)_1237);
        }
    }
    else {
        _1237 = NewDouble(DBL_PTR(_1235)->dbl - (double)_1236);
    }
    DeRef(_1235);
    _1235 = NOVALUE;
    _1236 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1237;
    *((int *)(_2+16)) = _14leading_whitespace_2577;
    _1238 = MAKE_SEQ(_1);
    _1237 = NOVALUE;
    DeRef(_s_2580);
    DeRefDS(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    DeRef(_1223);
    _1223 = NOVALUE;
    DeRef(_1229);
    _1229 = NOVALUE;
    return _1238;

    /** 			end while*/
    goto L10; // [621] 467
L15: 

    /** 				if ch != ',' then*/
    if (_14ch_2263 == 44)
    goto L16; // [628] 668

    /** 				return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1240 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1240 +(unsigned long) HIGH_BITS) >= 0){
        _1240 = NewDouble((double)_1240);
    }
    if (IS_ATOM_INT(_1240)) {
        _1241 = _1240 - _offset_2583;
        if ((long)((unsigned long)_1241 +(unsigned long) HIGH_BITS) >= 0){
            _1241 = NewDouble((double)_1241);
        }
    }
    else {
        _1241 = NewDouble(DBL_PTR(_1240)->dbl - (double)_offset_2583);
    }
    DeRef(_1240);
    _1240 = NOVALUE;
    _1242 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1241)) {
        _1243 = _1241 - _1242;
        if ((long)((unsigned long)_1243 +(unsigned long) HIGH_BITS) >= 0){
            _1243 = NewDouble((double)_1243);
        }
    }
    else {
        _1243 = NewDouble(DBL_PTR(_1241)->dbl - (double)_1242);
    }
    DeRef(_1241);
    _1241 = NOVALUE;
    _1242 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1243;
    *((int *)(_2+16)) = _14leading_whitespace_2577;
    _1244 = MAKE_SEQ(_1);
    _1243 = NOVALUE;
    DeRef(_s_2580);
    DeRef(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    DeRef(_1223);
    _1223 = NOVALUE;
    DeRef(_1229);
    _1229 = NOVALUE;
    DeRef(_1238);
    _1238 = NOVALUE;
    return _1244;
L16: 

    /** 			get_ch() -- skip comma*/
    _14get_ch();

    /** 			end while*/
    goto LB; // [674] 320
    goto L4; // [677] 96
L7: 

    /** 		elsif ch = '\"' then*/
    if (_14ch_2263 != 34)
    goto L17; // [684] 734

    /** 			e = get_string()*/
    _0 = _e_2581;
    _e_2581 = _14get_string();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1247 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1247 +(unsigned long) HIGH_BITS) >= 0){
        _1247 = NewDouble((double)_1247);
    }
    if (IS_ATOM_INT(_1247)) {
        _1248 = _1247 - _offset_2583;
        if ((long)((unsigned long)_1248 +(unsigned long) HIGH_BITS) >= 0){
            _1248 = NewDouble((double)_1248);
        }
    }
    else {
        _1248 = NewDouble(DBL_PTR(_1247)->dbl - (double)_offset_2583);
    }
    DeRef(_1247);
    _1247 = NOVALUE;
    _1249 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1248)) {
        _1250 = _1248 - _1249;
        if ((long)((unsigned long)_1250 +(unsigned long) HIGH_BITS) >= 0){
            _1250 = NewDouble((double)_1250);
        }
    }
    else {
        _1250 = NewDouble(DBL_PTR(_1248)->dbl - (double)_1249);
    }
    DeRef(_1248);
    _1248 = NOVALUE;
    _1249 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1250;
    ((int *)_2)[2] = _14leading_whitespace_2577;
    _1251 = MAKE_SEQ(_1);
    _1250 = NOVALUE;
    Concat((object_ptr)&_1252, _e_2581, _1251);
    DeRefDS(_1251);
    _1251 = NOVALUE;
    DeRef(_s_2580);
    DeRefDS(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    DeRef(_1223);
    _1223 = NOVALUE;
    DeRef(_1229);
    _1229 = NOVALUE;
    DeRef(_1238);
    _1238 = NOVALUE;
    DeRef(_1244);
    _1244 = NOVALUE;
    return _1252;
    goto L4; // [731] 96
L17: 

    /** 		elsif ch = '`' then*/
    if (_14ch_2263 != 96)
    goto L18; // [738] 789

    /** 			e = get_heredoc("`")*/
    RefDS(_1169);
    _0 = _e_2581;
    _e_2581 = _14get_heredoc(_1169);
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1255 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1255 +(unsigned long) HIGH_BITS) >= 0){
        _1255 = NewDouble((double)_1255);
    }
    if (IS_ATOM_INT(_1255)) {
        _1256 = _1255 - _offset_2583;
        if ((long)((unsigned long)_1256 +(unsigned long) HIGH_BITS) >= 0){
            _1256 = NewDouble((double)_1256);
        }
    }
    else {
        _1256 = NewDouble(DBL_PTR(_1255)->dbl - (double)_offset_2583);
    }
    DeRef(_1255);
    _1255 = NOVALUE;
    _1257 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1256)) {
        _1258 = _1256 - _1257;
        if ((long)((unsigned long)_1258 +(unsigned long) HIGH_BITS) >= 0){
            _1258 = NewDouble((double)_1258);
        }
    }
    else {
        _1258 = NewDouble(DBL_PTR(_1256)->dbl - (double)_1257);
    }
    DeRef(_1256);
    _1256 = NOVALUE;
    _1257 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1258;
    ((int *)_2)[2] = _14leading_whitespace_2577;
    _1259 = MAKE_SEQ(_1);
    _1258 = NOVALUE;
    Concat((object_ptr)&_1260, _e_2581, _1259);
    DeRefDS(_1259);
    _1259 = NOVALUE;
    DeRef(_s_2580);
    DeRefDS(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    DeRef(_1223);
    _1223 = NOVALUE;
    DeRef(_1229);
    _1229 = NOVALUE;
    DeRef(_1238);
    _1238 = NOVALUE;
    DeRef(_1244);
    _1244 = NOVALUE;
    DeRef(_1252);
    _1252 = NOVALUE;
    return _1260;
    goto L4; // [786] 96
L18: 

    /** 		elsif ch = '\'' then*/
    if (_14ch_2263 != 39)
    goto L19; // [793] 843

    /** 			e = get_qchar()*/
    _0 = _e_2581;
    _e_2581 = _14get_qchar();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1263 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1263 +(unsigned long) HIGH_BITS) >= 0){
        _1263 = NewDouble((double)_1263);
    }
    if (IS_ATOM_INT(_1263)) {
        _1264 = _1263 - _offset_2583;
        if ((long)((unsigned long)_1264 +(unsigned long) HIGH_BITS) >= 0){
            _1264 = NewDouble((double)_1264);
        }
    }
    else {
        _1264 = NewDouble(DBL_PTR(_1263)->dbl - (double)_offset_2583);
    }
    DeRef(_1263);
    _1263 = NOVALUE;
    _1265 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1264)) {
        _1266 = _1264 - _1265;
        if ((long)((unsigned long)_1266 +(unsigned long) HIGH_BITS) >= 0){
            _1266 = NewDouble((double)_1266);
        }
    }
    else {
        _1266 = NewDouble(DBL_PTR(_1264)->dbl - (double)_1265);
    }
    DeRef(_1264);
    _1264 = NOVALUE;
    _1265 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1266;
    ((int *)_2)[2] = _14leading_whitespace_2577;
    _1267 = MAKE_SEQ(_1);
    _1266 = NOVALUE;
    Concat((object_ptr)&_1268, _e_2581, _1267);
    DeRefDS(_1267);
    _1267 = NOVALUE;
    DeRef(_s_2580);
    DeRefDS(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    DeRef(_1223);
    _1223 = NOVALUE;
    DeRef(_1229);
    _1229 = NOVALUE;
    DeRef(_1238);
    _1238 = NOVALUE;
    DeRef(_1244);
    _1244 = NOVALUE;
    DeRef(_1252);
    _1252 = NOVALUE;
    DeRef(_1260);
    _1260 = NOVALUE;
    return _1268;
    goto L4; // [840] 96
L19: 

    /** 			return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1269 = _14string_next_2262 - 1;
    if ((long)((unsigned long)_1269 +(unsigned long) HIGH_BITS) >= 0){
        _1269 = NewDouble((double)_1269);
    }
    if (IS_ATOM_INT(_1269)) {
        _1270 = _1269 - _offset_2583;
        if ((long)((unsigned long)_1270 +(unsigned long) HIGH_BITS) >= 0){
            _1270 = NewDouble((double)_1270);
        }
    }
    else {
        _1270 = NewDouble(DBL_PTR(_1269)->dbl - (double)_offset_2583);
    }
    DeRef(_1269);
    _1269 = NOVALUE;
    _1271 = (_14ch_2263 != -1);
    if (IS_ATOM_INT(_1270)) {
        _1272 = _1270 - _1271;
        if ((long)((unsigned long)_1272 +(unsigned long) HIGH_BITS) >= 0){
            _1272 = NewDouble((double)_1272);
        }
    }
    else {
        _1272 = NewDouble(DBL_PTR(_1270)->dbl - (double)_1271);
    }
    DeRef(_1270);
    _1270 = NOVALUE;
    _1271 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1272;
    *((int *)(_2+16)) = _14leading_whitespace_2577;
    _1273 = MAKE_SEQ(_1);
    _1272 = NOVALUE;
    DeRef(_s_2580);
    DeRef(_e_2581);
    DeRef(_1180);
    _1180 = NOVALUE;
    DeRef(_1192);
    _1192 = NOVALUE;
    DeRef(_1198);
    _1198 = NOVALUE;
    DeRef(_1205);
    _1205 = NOVALUE;
    DeRef(_1217);
    _1217 = NOVALUE;
    DeRef(_1223);
    _1223 = NOVALUE;
    DeRef(_1229);
    _1229 = NOVALUE;
    DeRef(_1238);
    _1238 = NOVALUE;
    DeRef(_1244);
    _1244 = NOVALUE;
    DeRef(_1252);
    _1252 = NOVALUE;
    DeRef(_1260);
    _1260 = NOVALUE;
    DeRef(_1268);
    _1268 = NOVALUE;
    return _1273;

    /** 	end while*/
    goto L4; // [881] 96
    ;
}


int _14get_value(int _target_2719, int _start_point_2720, int _answer_type_2721)
{
    int _msg_inlined_crash_at_39_2732 = NOVALUE;
    int _data_inlined_crash_at_36_2731 = NOVALUE;
    int _where_inlined_where_at_80_2738 = NOVALUE;
    int _seek_1__tmp_at94_2743 = NOVALUE;
    int _seek_inlined_seek_at_94_2742 = NOVALUE;
    int _pos_inlined_seek_at_91_2741 = NOVALUE;
    int _msg_inlined_crash_at_112_2746 = NOVALUE;
    int _1289 = NOVALUE;
    int _1286 = NOVALUE;
    int _1285 = NOVALUE;
    int _1284 = NOVALUE;
    int _1280 = NOVALUE;
    int _1279 = NOVALUE;
    int _1278 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if answer_type != GET_SHORT_ANSWER and answer_type != GET_LONG_ANSWER then*/
    _1278 = (_answer_type_2721 != _14GET_SHORT_ANSWER_2711);
    if (_1278 == 0) {
        goto L1; // [17] 59
    }
    _1280 = (_answer_type_2721 != _14GET_LONG_ANSWER_2714);
    if (_1280 == 0)
    {
        DeRef(_1280);
        _1280 = NOVALUE;
        goto L1; // [28] 59
    }
    else{
        DeRef(_1280);
        _1280 = NOVALUE;
    }

    /** 		error:crash("Invalid type of answer, please only use %s (the default) or %s.", {"GET_SHORT_ANSWER", "GET_LONG_ANSWER"})*/
    RefDS(_1283);
    RefDS(_1282);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1282;
    ((int *)_2)[2] = _1283;
    _1284 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_36_2731);
    _data_inlined_crash_at_36_2731 = _1284;
    _1284 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_39_2732);
    _msg_inlined_crash_at_39_2732 = EPrintf(-9999999, _1281, _data_inlined_crash_at_36_2731);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_39_2732);

    /** end procedure*/
    goto L2; // [53] 56
L2: 
    DeRef(_data_inlined_crash_at_36_2731);
    _data_inlined_crash_at_36_2731 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_39_2732);
    _msg_inlined_crash_at_39_2732 = NOVALUE;
L1: 

    /** 	if atom(target) then -- get()*/
    _1285 = IS_ATOM(_target_2719);
    if (_1285 == 0)
    {
        _1285 = NOVALUE;
        goto L3; // [64] 146
    }
    else{
        _1285 = NOVALUE;
    }

    /** 		input_file = target*/
    Ref(_target_2719);
    _14input_file_2260 = _target_2719;
    if (!IS_ATOM_INT(_14input_file_2260)) {
        _1 = (long)(DBL_PTR(_14input_file_2260)->dbl);
        if (UNIQUE(DBL_PTR(_14input_file_2260)) && (DBL_PTR(_14input_file_2260)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14input_file_2260);
        _14input_file_2260 = _1;
    }

    /** 		if start_point then*/
    if (_start_point_2720 == 0)
    {
        goto L4; // [76] 133
    }
    else{
    }

    /** 			if io:seek(target, io:where(target)+start_point) then*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_80_2738);
    _where_inlined_where_at_80_2738 = machine(20, _target_2719);
    if (IS_ATOM_INT(_where_inlined_where_at_80_2738)) {
        _1286 = _where_inlined_where_at_80_2738 + _start_point_2720;
        if ((long)((unsigned long)_1286 + (unsigned long)HIGH_BITS) >= 0) 
        _1286 = NewDouble((double)_1286);
    }
    else {
        _1286 = NewDouble(DBL_PTR(_where_inlined_where_at_80_2738)->dbl + (double)_start_point_2720);
    }
    DeRef(_pos_inlined_seek_at_91_2741);
    _pos_inlined_seek_at_91_2741 = _1286;
    _1286 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_91_2741);
    Ref(_target_2719);
    DeRef(_seek_1__tmp_at94_2743);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _target_2719;
    ((int *)_2)[2] = _pos_inlined_seek_at_91_2741;
    _seek_1__tmp_at94_2743 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_94_2742 = machine(19, _seek_1__tmp_at94_2743);
    DeRef(_pos_inlined_seek_at_91_2741);
    _pos_inlined_seek_at_91_2741 = NOVALUE;
    DeRef(_seek_1__tmp_at94_2743);
    _seek_1__tmp_at94_2743 = NOVALUE;
    if (_seek_inlined_seek_at_94_2742 == 0)
    {
        goto L5; // [108] 132
    }
    else{
    }

    /** 				error:crash("Initial seek() for get() failed!")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_112_2746);
    _msg_inlined_crash_at_112_2746 = EPrintf(-9999999, _1287, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_112_2746);

    /** end procedure*/
    goto L6; // [126] 129
L6: 
    DeRefi(_msg_inlined_crash_at_112_2746);
    _msg_inlined_crash_at_112_2746 = NOVALUE;
L5: 
L4: 

    /** 		string_next = 1*/
    _14string_next_2262 = 1;

    /** 		input_string = 0*/
    DeRef(_14input_string_2261);
    _14input_string_2261 = 0;
    goto L7; // [143] 157
L3: 

    /** 		input_string = target*/
    Ref(_target_2719);
    DeRef(_14input_string_2261);
    _14input_string_2261 = _target_2719;

    /** 		string_next = start_point*/
    _14string_next_2262 = _start_point_2720;
L7: 

    /** 	if answer_type = GET_SHORT_ANSWER then*/
    if (_answer_type_2721 != _14GET_SHORT_ANSWER_2711)
    goto L8; // [161] 170

    /** 		get_ch()*/
    _14get_ch();
L8: 

    /** 	return call_func(answer_type, {})*/
    _0 = (int)_00[_answer_type_2721].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_1289);
    _1289 = _1;
    DeRef(_target_2719);
    DeRef(_1278);
    _1278 = NOVALUE;
    return _1289;
    ;
}


int _14defaulted_value(int _st_2765, int _def_2766, int _start_point_2767)
{
    int _result_2770 = NOVALUE;
    int _1296 = NOVALUE;
    int _1294 = NOVALUE;
    int _1292 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(st) then*/
    _1292 = 0;
    if (_1292 == 0)
    {
        _1292 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _1292 = NOVALUE;
    }

    /** 		return def*/
    DeRefDS(_st_2765);
    DeRef(_result_2770);
    return 0;
L1: 

    /** 	object result = get_value(st,start_point, GET_SHORT_ANSWER)*/
    Ref(_st_2765);
    _0 = _result_2770;
    _result_2770 = _14get_value(_st_2765, _start_point_2767, _14GET_SHORT_ANSWER_2711);
    DeRef(_0);

    /** 	if result[1] = GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_result_2770);
    _1294 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _1294, 0)){
        _1294 = NOVALUE;
        goto L2; // [36] 51
    }
    _1294 = NOVALUE;

    /** 		return result[2]*/
    _2 = (int)SEQ_PTR(_result_2770);
    _1296 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_1296);
    DeRef(_st_2765);
    DeRef(_result_2770);
    return _1296;
L2: 

    /** 	return def*/
    DeRef(_st_2765);
    DeRef(_result_2770);
    _1296 = NOVALUE;
    return _def_2766;
    ;
}



// 0x5713FA58
