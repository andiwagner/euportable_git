// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _6trim_tail(int _source_8872, int _what_8873, int _ret_index_8874)
{
    int _rpos_8875 = NOVALUE;
    int _4922 = NOVALUE;
    int _4919 = NOVALUE;
    int _4918 = NOVALUE;
    int _4914 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(what) then*/
    _4914 = 0;
    if (_4914 == 0)
    {
        _4914 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _4914 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_8873;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_what_8873);
    *((int *)(_2+4)) = _what_8873;
    _what_8873 = MAKE_SEQ(_1);
    DeRefDSi(_0);
L1: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_8872)){
            _rpos_8875 = SEQ_PTR(_source_8872)->length;
    }
    else {
        _rpos_8875 = 1;
    }

    /** 	while rpos > 0 do*/
L2: 
    if (_rpos_8875 <= 0)
    goto L3; // [34] 70

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_8872);
    _4918 = (int)*(((s1_ptr)_2)->base + _rpos_8875);
    _4919 = find_from(_4918, _what_8873, 1);
    _4918 = NOVALUE;
    if (_4919 != 0)
    goto L4; // [49] 57
    _4919 = NOVALUE;

    /** 			exit*/
    goto L3; // [54] 70
L4: 

    /** 		rpos -= 1*/
    _rpos_8875 = _rpos_8875 - 1;

    /** 	end while*/
    goto L2; // [67] 34
L3: 

    /** 	if ret_index then*/
    if (_ret_index_8874 == 0)
    {
        goto L5; // [72] 84
    }
    else{
    }

    /** 		return rpos*/
    DeRefDS(_source_8872);
    DeRef(_what_8873);
    return _rpos_8875;
    goto L6; // [81] 96
L5: 

    /** 		return source[1..rpos]*/
    rhs_slice_target = (object_ptr)&_4922;
    RHS_Slice(_source_8872, 1, _rpos_8875);
    DeRefDS(_source_8872);
    DeRef(_what_8873);
    return _4922;
L6: 
    ;
}


int _6trim(int _source_8892, int _what_8893, int _ret_index_8894)
{
    int _rpos_8895 = NOVALUE;
    int _lpos_8896 = NOVALUE;
    int _4943 = NOVALUE;
    int _4941 = NOVALUE;
    int _4939 = NOVALUE;
    int _4937 = NOVALUE;
    int _4934 = NOVALUE;
    int _4933 = NOVALUE;
    int _4928 = NOVALUE;
    int _4927 = NOVALUE;
    int _4925 = NOVALUE;
    int _4923 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(what) then*/
    _4923 = 0;
    if (_4923 == 0)
    {
        _4923 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _4923 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_8893;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_what_8893);
    *((int *)(_2+4)) = _what_8893;
    _what_8893 = MAKE_SEQ(_1);
    DeRefDSi(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_8896 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_8892)){
            _4925 = SEQ_PTR(_source_8892)->length;
    }
    else {
        _4925 = 1;
    }
    if (_lpos_8896 > _4925)
    goto L3; // [37] 73

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_8892);
    _4927 = (int)*(((s1_ptr)_2)->base + _lpos_8896);
    _4928 = find_from(_4927, _what_8893, 1);
    _4927 = NOVALUE;
    if (_4928 != 0)
    goto L4; // [52] 60
    _4928 = NOVALUE;

    /** 			exit*/
    goto L3; // [57] 73
L4: 

    /** 		lpos += 1*/
    _lpos_8896 = _lpos_8896 + 1;

    /** 	end while*/
    goto L2; // [70] 34
L3: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_8892)){
            _rpos_8895 = SEQ_PTR(_source_8892)->length;
    }
    else {
        _rpos_8895 = 1;
    }

    /** 	while rpos > lpos do*/
L5: 
    if (_rpos_8895 <= _lpos_8896)
    goto L6; // [85] 121

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_8892);
    _4933 = (int)*(((s1_ptr)_2)->base + _rpos_8895);
    _4934 = find_from(_4933, _what_8893, 1);
    _4933 = NOVALUE;
    if (_4934 != 0)
    goto L7; // [100] 108
    _4934 = NOVALUE;

    /** 			exit*/
    goto L6; // [105] 121
L7: 

    /** 		rpos -= 1*/
    _rpos_8895 = _rpos_8895 - 1;

    /** 	end while*/
    goto L5; // [118] 85
L6: 

    /** 	if ret_index then*/
    if (_ret_index_8894 == 0)
    {
        goto L8; // [123] 139
    }
    else{
    }

    /** 		return {lpos, rpos}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lpos_8896;
    ((int *)_2)[2] = _rpos_8895;
    _4937 = MAKE_SEQ(_1);
    DeRefDS(_source_8892);
    DeRef(_what_8893);
    return _4937;
    goto L9; // [136] 190
L8: 

    /** 		if lpos = 1 then*/
    if (_lpos_8896 != 1)
    goto LA; // [141] 162

    /** 			if rpos = length(source) then*/
    if (IS_SEQUENCE(_source_8892)){
            _4939 = SEQ_PTR(_source_8892)->length;
    }
    else {
        _4939 = 1;
    }
    if (_rpos_8895 != _4939)
    goto LB; // [150] 161

    /** 				return source*/
    DeRef(_what_8893);
    DeRef(_4937);
    _4937 = NOVALUE;
    return _source_8892;
LB: 
LA: 

    /** 		if lpos > length(source) then*/
    if (IS_SEQUENCE(_source_8892)){
            _4941 = SEQ_PTR(_source_8892)->length;
    }
    else {
        _4941 = 1;
    }
    if (_lpos_8896 <= _4941)
    goto LC; // [167] 178

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_source_8892);
    DeRef(_what_8893);
    DeRef(_4937);
    _4937 = NOVALUE;
    return _5;
LC: 

    /** 		return source[lpos..rpos]*/
    rhs_slice_target = (object_ptr)&_4943;
    RHS_Slice(_source_8892, _lpos_8896, _rpos_8895);
    DeRefDS(_source_8892);
    DeRef(_what_8893);
    DeRef(_4937);
    _4937 = NOVALUE;
    return _4943;
L9: 
    ;
}


int _6change_case(int _x_9109, int _api_9110)
{
    int _changed_text_9111 = NOVALUE;
    int _single_char_9112 = NOVALUE;
    int _len_9113 = NOVALUE;
    int _5074 = NOVALUE;
    int _5072 = NOVALUE;
    int _5070 = NOVALUE;
    int _5069 = NOVALUE;
    int _5066 = NOVALUE;
    int _5064 = NOVALUE;
    int _5062 = NOVALUE;
    int _5061 = NOVALUE;
    int _5060 = NOVALUE;
    int _5059 = NOVALUE;
    int _5058 = NOVALUE;
    int _5055 = NOVALUE;
    int _5053 = NOVALUE;
    int _0, _1, _2;
    

    /** 		integer single_char = 0*/
    _single_char_9112 = 0;

    /** 		if not string(x) then*/
    Ref(_x_9109);
    _5053 = _3string(_x_9109);
    if (IS_ATOM_INT(_5053)) {
        if (_5053 != 0){
            DeRef(_5053);
            _5053 = NOVALUE;
            goto L1; // [14] 99
        }
    }
    else {
        if (DBL_PTR(_5053)->dbl != 0.0){
            DeRef(_5053);
            _5053 = NOVALUE;
            goto L1; // [14] 99
        }
    }
    DeRef(_5053);
    _5053 = NOVALUE;

    /** 			if atom(x) then*/
    _5055 = IS_ATOM(_x_9109);
    if (_5055 == 0)
    {
        _5055 = NOVALUE;
        goto L2; // [22] 54
    }
    else{
        _5055 = NOVALUE;
    }

    /** 				if x = 0 then*/
    if (binary_op_a(NOTEQ, _x_9109, 0)){
        goto L3; // [27] 38
    }

    /** 					return 0*/
    DeRef(_x_9109);
    DeRef(_api_9110);
    DeRefi(_changed_text_9111);
    return 0;
L3: 

    /** 				x = {x}*/
    _0 = _x_9109;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_x_9109);
    *((int *)(_2+4)) = _x_9109;
    _x_9109 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 				single_char = 1*/
    _single_char_9112 = 1;
    goto L4; // [51] 98
L2: 

    /** 				for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_9109)){
            _5058 = SEQ_PTR(_x_9109)->length;
    }
    else {
        _5058 = 1;
    }
    {
        int _i_9125;
        _i_9125 = 1;
L5: 
        if (_i_9125 > _5058){
            goto L6; // [59] 91
        }

        /** 					x[i] = change_case(x[i], api)*/
        _2 = (int)SEQ_PTR(_x_9109);
        _5059 = (int)*(((s1_ptr)_2)->base + _i_9125);
        Ref(_api_9110);
        DeRef(_5060);
        _5060 = _api_9110;
        Ref(_5059);
        _5061 = _6change_case(_5059, _5060);
        _5059 = NOVALUE;
        _5060 = NOVALUE;
        _2 = (int)SEQ_PTR(_x_9109);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_9109 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9125);
        _1 = *(int *)_2;
        *(int *)_2 = _5061;
        if( _1 != _5061 ){
            DeRef(_1);
        }
        _5061 = NOVALUE;

        /** 				end for*/
        _i_9125 = _i_9125 + 1;
        goto L5; // [86] 66
L6: 
        ;
    }

    /** 				return x*/
    DeRef(_api_9110);
    DeRefi(_changed_text_9111);
    return _x_9109;
L4: 
L1: 

    /** 		if length(x) = 0 then*/
    if (IS_SEQUENCE(_x_9109)){
            _5062 = SEQ_PTR(_x_9109)->length;
    }
    else {
        _5062 = 1;
    }
    if (_5062 != 0)
    goto L7; // [104] 115

    /** 			return x*/
    DeRef(_api_9110);
    DeRefi(_changed_text_9111);
    return _x_9109;
L7: 

    /** 		if length(x) >= tm_size then*/
    if (IS_SEQUENCE(_x_9109)){
            _5064 = SEQ_PTR(_x_9109)->length;
    }
    else {
        _5064 = 1;
    }
    if (_5064 < _6tm_size_9103)
    goto L8; // [122] 152

    /** 			tm_size = length(x) + 1*/
    if (IS_SEQUENCE(_x_9109)){
            _5066 = SEQ_PTR(_x_9109)->length;
    }
    else {
        _5066 = 1;
    }
    _6tm_size_9103 = _5066 + 1;
    _5066 = NOVALUE;

    /** 			free(temp_mem)*/
    Ref(_6temp_mem_9104);
    _11free(_6temp_mem_9104);

    /** 			temp_mem = allocate(tm_size)*/
    _0 = _11allocate(_6tm_size_9103, 0);
    DeRef(_6temp_mem_9104);
    _6temp_mem_9104 = _0;
L8: 

    /** 		poke(temp_mem, x)*/
    if (IS_ATOM_INT(_6temp_mem_9104)){
        poke_addr = (unsigned char *)_6temp_mem_9104;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_6temp_mem_9104)->dbl);
    }
    if (IS_ATOM_INT(_x_9109)) {
        *poke_addr = (unsigned char)_x_9109;
    }
    else if (IS_ATOM(_x_9109)) {
        _1 = (signed char)DBL_PTR(_x_9109)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_x_9109);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }

    /** 		len = c_func(api, {temp_mem, length(x)} )*/
    if (IS_SEQUENCE(_x_9109)){
            _5069 = SEQ_PTR(_x_9109)->length;
    }
    else {
        _5069 = 1;
    }
    Ref(_6temp_mem_9104);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6temp_mem_9104;
    ((int *)_2)[2] = _5069;
    _5070 = MAKE_SEQ(_1);
    _5069 = NOVALUE;
    _len_9113 = call_c(1, _api_9110, _5070);
    DeRefDS(_5070);
    _5070 = NOVALUE;
    if (!IS_ATOM_INT(_len_9113)) {
        _1 = (long)(DBL_PTR(_len_9113)->dbl);
        if (UNIQUE(DBL_PTR(_len_9113)) && (DBL_PTR(_len_9113)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_9113);
        _len_9113 = _1;
    }

    /** 		changed_text = peek({temp_mem, len})*/
    Ref(_6temp_mem_9104);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6temp_mem_9104;
    ((int *)_2)[2] = _len_9113;
    _5072 = MAKE_SEQ(_1);
    DeRefi(_changed_text_9111);
    _1 = (int)SEQ_PTR(_5072);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _changed_text_9111 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_5072);
    _5072 = NOVALUE;

    /** 		if single_char then*/
    if (_single_char_9112 == 0)
    {
        goto L9; // [194] 210
    }
    else{
    }

    /** 			return changed_text[1]*/
    _2 = (int)SEQ_PTR(_changed_text_9111);
    _5074 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_x_9109);
    DeRef(_api_9110);
    DeRefDSi(_changed_text_9111);
    return _5074;
    goto LA; // [207] 217
L9: 

    /** 			return changed_text*/
    DeRef(_x_9109);
    DeRef(_api_9110);
    _5074 = NOVALUE;
    return _changed_text_9111;
LA: 
    ;
}


int _6lower(int _x_9151)
{
    int _5078 = NOVALUE;
    int _5075 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(lower_case_SET) != 0 then*/
    _5075 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharLowerBuff)*/
    Ref(_x_9151);
    Ref(_6api_CharLowerBuff_9087);
    _5078 = _6change_case(_x_9151, _6api_CharLowerBuff_9087);
    DeRef(_x_9151);
    return _5078;
    ;
}


int _6upper(int _x_9159)
{
    int _5082 = NOVALUE;
    int _5079 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(upper_case_SET) != 0 then*/
    _5079 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharUpperBuff)*/
    Ref(_x_9159);
    Ref(_6api_CharUpperBuff_9095);
    _5082 = _6change_case(_x_9159, _6api_CharUpperBuff_9095);
    DeRef(_x_9159);
    return _5082;
    ;
}


int _6proper(int _x_9167)
{
    int _pos_9168 = NOVALUE;
    int _inword_9169 = NOVALUE;
    int _convert_9170 = NOVALUE;
    int _res_9171 = NOVALUE;
    int _5111 = NOVALUE;
    int _5110 = NOVALUE;
    int _5109 = NOVALUE;
    int _5108 = NOVALUE;
    int _5107 = NOVALUE;
    int _5106 = NOVALUE;
    int _5105 = NOVALUE;
    int _5104 = NOVALUE;
    int _5103 = NOVALUE;
    int _5102 = NOVALUE;
    int _5100 = NOVALUE;
    int _5099 = NOVALUE;
    int _5095 = NOVALUE;
    int _5092 = NOVALUE;
    int _5089 = NOVALUE;
    int _5086 = NOVALUE;
    int _5085 = NOVALUE;
    int _5084 = NOVALUE;
    int _5083 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inword = 0	-- Initially not in a word*/
    _inword_9169 = 0;

    /** 	convert = 1	-- Initially convert text*/
    _convert_9170 = 1;

    /** 	res = x		-- Work on a copy of the original, in case we need to restore.*/
    RefDS(_x_9167);
    DeRef(_res_9171);
    _res_9171 = _x_9167;

    /** 	for i = 1 to length(res) do*/
    if (IS_SEQUENCE(_res_9171)){
            _5083 = SEQ_PTR(_res_9171)->length;
    }
    else {
        _5083 = 1;
    }
    {
        int _i_9173;
        _i_9173 = 1;
L1: 
        if (_i_9173 > _5083){
            goto L2; // [29] 320
        }

        /** 		if integer(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5084 = (int)*(((s1_ptr)_2)->base + _i_9173);
        if (IS_ATOM_INT(_5084))
        _5085 = 1;
        else if (IS_ATOM_DBL(_5084))
        _5085 = IS_ATOM_INT(DoubleToInt(_5084));
        else
        _5085 = 0;
        _5084 = NOVALUE;
        if (_5085 == 0)
        {
            _5085 = NOVALUE;
            goto L3; // [45] 229
        }
        else{
            _5085 = NOVALUE;
        }

        /** 			if convert then*/
        if (_convert_9170 == 0)
        {
            goto L4; // [50] 313
        }
        else{
        }

        /** 				pos = types:t_upper(res[i])*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5086 = (int)*(((s1_ptr)_2)->base + _i_9173);
        Ref(_5086);
        _pos_9168 = _3t_upper(_5086);
        _5086 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9168)) {
            _1 = (long)(DBL_PTR(_pos_9168)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9168)) && (DBL_PTR(_pos_9168)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9168);
            _pos_9168 = _1;
        }

        /** 				if pos = 0 then*/
        if (_pos_9168 != 0)
        goto L5; // [69] 193

        /** 					pos = types:t_lower(res[i])*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5089 = (int)*(((s1_ptr)_2)->base + _i_9173);
        Ref(_5089);
        _pos_9168 = _3t_lower(_5089);
        _5089 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9168)) {
            _1 = (long)(DBL_PTR(_pos_9168)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9168)) && (DBL_PTR(_pos_9168)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9168);
            _pos_9168 = _1;
        }

        /** 					if pos = 0 then*/
        if (_pos_9168 != 0)
        goto L6; // [89] 154

        /** 						pos = t_digit(res[i])*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5092 = (int)*(((s1_ptr)_2)->base + _i_9173);
        Ref(_5092);
        _pos_9168 = _3t_digit(_5092);
        _5092 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9168)) {
            _1 = (long)(DBL_PTR(_pos_9168)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9168)) && (DBL_PTR(_pos_9168)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9168);
            _pos_9168 = _1;
        }

        /** 						if pos = 0 then*/
        if (_pos_9168 != 0)
        goto L4; // [109] 313

        /** 							pos = t_specword(res[i])*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5095 = (int)*(((s1_ptr)_2)->base + _i_9173);
        Ref(_5095);
        _pos_9168 = _3t_specword(_5095);
        _5095 = NOVALUE;
        if (!IS_ATOM_INT(_pos_9168)) {
            _1 = (long)(DBL_PTR(_pos_9168)->dbl);
            if (UNIQUE(DBL_PTR(_pos_9168)) && (DBL_PTR(_pos_9168)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_9168);
            _pos_9168 = _1;
        }

        /** 							if pos then*/
        if (_pos_9168 == 0)
        {
            goto L7; // [129] 142
        }
        else{
        }

        /** 								inword = 1*/
        _inword_9169 = 1;
        goto L4; // [139] 313
L7: 

        /** 								inword = 0*/
        _inword_9169 = 0;
        goto L4; // [151] 313
L6: 

        /** 						if inword = 0 then*/
        if (_inword_9169 != 0)
        goto L4; // [156] 313

        /** 							if pos <= 26 then*/
        if (_pos_9168 > 26)
        goto L8; // [162] 181

        /** 								res[i] = upper(res[i]) -- Convert to uppercase*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5099 = (int)*(((s1_ptr)_2)->base + _i_9173);
        Ref(_5099);
        _5100 = _6upper(_5099);
        _5099 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9171);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9171 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9173);
        _1 = *(int *)_2;
        *(int *)_2 = _5100;
        if( _1 != _5100 ){
            DeRef(_1);
        }
        _5100 = NOVALUE;
L8: 

        /** 							inword = 1	-- now we are in a word*/
        _inword_9169 = 1;
        goto L4; // [190] 313
L5: 

        /** 					if inword = 1 then*/
        if (_inword_9169 != 1)
        goto L9; // [195] 216

        /** 						res[i] = lower(res[i]) -- Convert to lowercase*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5102 = (int)*(((s1_ptr)_2)->base + _i_9173);
        Ref(_5102);
        _5103 = _6lower(_5102);
        _5102 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9171);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9171 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9173);
        _1 = *(int *)_2;
        *(int *)_2 = _5103;
        if( _1 != _5103 ){
            DeRef(_1);
        }
        _5103 = NOVALUE;
        goto L4; // [213] 313
L9: 

        /** 						inword = 1	-- now we are in a word*/
        _inword_9169 = 1;
        goto L4; // [226] 313
L3: 

        /** 			if convert then*/
        if (_convert_9170 == 0)
        {
            goto LA; // [231] 285
        }
        else{
        }

        /** 				for j = 1 to i-1 do*/
        _5104 = _i_9173 - 1;
        {
            int _j_9213;
            _j_9213 = 1;
LB: 
            if (_j_9213 > _5104){
                goto LC; // [240] 277
            }

            /** 					if atom(x[j]) then*/
            _2 = (int)SEQ_PTR(_x_9167);
            _5105 = (int)*(((s1_ptr)_2)->base + _j_9213);
            _5106 = IS_ATOM(_5105);
            _5105 = NOVALUE;
            if (_5106 == 0)
            {
                _5106 = NOVALUE;
                goto LD; // [256] 270
            }
            else{
                _5106 = NOVALUE;
            }

            /** 						res[j] = x[j]*/
            _2 = (int)SEQ_PTR(_x_9167);
            _5107 = (int)*(((s1_ptr)_2)->base + _j_9213);
            Ref(_5107);
            _2 = (int)SEQ_PTR(_res_9171);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _res_9171 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_9213);
            _1 = *(int *)_2;
            *(int *)_2 = _5107;
            if( _1 != _5107 ){
                DeRef(_1);
            }
            _5107 = NOVALUE;
LD: 

            /** 				end for*/
            _j_9213 = _j_9213 + 1;
            goto LB; // [272] 247
LC: 
            ;
        }

        /** 				convert = 0*/
        _convert_9170 = 0;
LA: 

        /** 			if sequence(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5108 = (int)*(((s1_ptr)_2)->base + _i_9173);
        _5109 = IS_SEQUENCE(_5108);
        _5108 = NOVALUE;
        if (_5109 == 0)
        {
            _5109 = NOVALUE;
            goto LE; // [294] 312
        }
        else{
            _5109 = NOVALUE;
        }

        /** 				res[i] = proper(res[i])	-- recursive conversion*/
        _2 = (int)SEQ_PTR(_res_9171);
        _5110 = (int)*(((s1_ptr)_2)->base + _i_9173);
        Ref(_5110);
        _5111 = _6proper(_5110);
        _5110 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_9171);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_9171 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9173);
        _1 = *(int *)_2;
        *(int *)_2 = _5111;
        if( _1 != _5111 ){
            DeRef(_1);
        }
        _5111 = NOVALUE;
LE: 
L4: 

        /** 	end for*/
        _i_9173 = _i_9173 + 1;
        goto L1; // [315] 36
L2: 
        ;
    }

    /** 	return res*/
    DeRefDS(_x_9167);
    DeRef(_5104);
    _5104 = NOVALUE;
    return _res_9171;
    ;
}


int _6keyvalues(int _source_9226, int _pair_delim_9227, int _kv_delim_9229, int _quotes_9231, int _whitespace_9233, int _haskeys_9234)
{
    int _lKeyValues_9235 = NOVALUE;
    int _value__9236 = NOVALUE;
    int _key__9237 = NOVALUE;
    int _lAllDelim_9238 = NOVALUE;
    int _lWhitePair_9239 = NOVALUE;
    int _lStartBracket_9240 = NOVALUE;
    int _lEndBracket_9241 = NOVALUE;
    int _lBracketed_9242 = NOVALUE;
    int _lQuote_9243 = NOVALUE;
    int _pos__9244 = NOVALUE;
    int _lChar_9245 = NOVALUE;
    int _lBPos_9246 = NOVALUE;
    int _lWasKV_9247 = NOVALUE;
    int _5287 = NOVALUE;
    int _5284 = NOVALUE;
    int _5280 = NOVALUE;
    int _5277 = NOVALUE;
    int _5276 = NOVALUE;
    int _5275 = NOVALUE;
    int _5274 = NOVALUE;
    int _5273 = NOVALUE;
    int _5272 = NOVALUE;
    int _5271 = NOVALUE;
    int _5269 = NOVALUE;
    int _5268 = NOVALUE;
    int _5267 = NOVALUE;
    int _5266 = NOVALUE;
    int _5265 = NOVALUE;
    int _5264 = NOVALUE;
    int _5263 = NOVALUE;
    int _5262 = NOVALUE;
    int _5259 = NOVALUE;
    int _5258 = NOVALUE;
    int _5257 = NOVALUE;
    int _5256 = NOVALUE;
    int _5255 = NOVALUE;
    int _5254 = NOVALUE;
    int _5250 = NOVALUE;
    int _5248 = NOVALUE;
    int _5247 = NOVALUE;
    int _5244 = NOVALUE;
    int _5240 = NOVALUE;
    int _5238 = NOVALUE;
    int _5235 = NOVALUE;
    int _5232 = NOVALUE;
    int _5229 = NOVALUE;
    int _5226 = NOVALUE;
    int _5223 = NOVALUE;
    int _5220 = NOVALUE;
    int _5216 = NOVALUE;
    int _5215 = NOVALUE;
    int _5214 = NOVALUE;
    int _5213 = NOVALUE;
    int _5212 = NOVALUE;
    int _5211 = NOVALUE;
    int _5210 = NOVALUE;
    int _5208 = NOVALUE;
    int _5207 = NOVALUE;
    int _5206 = NOVALUE;
    int _5205 = NOVALUE;
    int _5204 = NOVALUE;
    int _5203 = NOVALUE;
    int _5202 = NOVALUE;
    int _5201 = NOVALUE;
    int _5199 = NOVALUE;
    int _5196 = NOVALUE;
    int _5194 = NOVALUE;
    int _5192 = NOVALUE;
    int _5191 = NOVALUE;
    int _5190 = NOVALUE;
    int _5189 = NOVALUE;
    int _5188 = NOVALUE;
    int _5187 = NOVALUE;
    int _5186 = NOVALUE;
    int _5185 = NOVALUE;
    int _5182 = NOVALUE;
    int _5181 = NOVALUE;
    int _5180 = NOVALUE;
    int _5179 = NOVALUE;
    int _5178 = NOVALUE;
    int _5175 = NOVALUE;
    int _5172 = NOVALUE;
    int _5169 = NOVALUE;
    int _5166 = NOVALUE;
    int _5165 = NOVALUE;
    int _5163 = NOVALUE;
    int _5162 = NOVALUE;
    int _5158 = NOVALUE;
    int _5155 = NOVALUE;
    int _5152 = NOVALUE;
    int _5148 = NOVALUE;
    int _5147 = NOVALUE;
    int _5146 = NOVALUE;
    int _5145 = NOVALUE;
    int _5141 = NOVALUE;
    int _5138 = NOVALUE;
    int _5135 = NOVALUE;
    int _5134 = NOVALUE;
    int _5132 = NOVALUE;
    int _5130 = NOVALUE;
    int _5124 = NOVALUE;
    int _5122 = NOVALUE;
    int _5120 = NOVALUE;
    int _5118 = NOVALUE;
    int _5116 = NOVALUE;
    int _0, _1, _2;
    

    /** 	source = trim(source)*/
    RefDS(_source_9226);
    RefDS(_4282);
    _0 = _source_9226;
    _source_9226 = _6trim(_source_9226, _4282, 0);
    DeRefDS(_0);

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_9226)){
            _5116 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5116 = 1;
    }
    if (_5116 != 0)
    goto L1; // [22] 33

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_source_9226);
    DeRef(_pair_delim_9227);
    DeRef(_kv_delim_9229);
    DeRef(_quotes_9231);
    DeRef(_whitespace_9233);
    DeRef(_lKeyValues_9235);
    DeRef(_value__9236);
    DeRef(_key__9237);
    DeRef(_lAllDelim_9238);
    DeRef(_lWhitePair_9239);
    DeRefi(_lStartBracket_9240);
    DeRefi(_lEndBracket_9241);
    DeRefi(_lBracketed_9242);
    return _5;
L1: 

    /** 	if atom(pair_delim) then*/
    _5118 = IS_ATOM(_pair_delim_9227);
    if (_5118 == 0)
    {
        _5118 = NOVALUE;
        goto L2; // [38] 48
    }
    else{
        _5118 = NOVALUE;
    }

    /** 		pair_delim = {pair_delim}*/
    _0 = _pair_delim_9227;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pair_delim_9227);
    *((int *)(_2+4)) = _pair_delim_9227;
    _pair_delim_9227 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	if atom(kv_delim) then*/
    _5120 = IS_ATOM(_kv_delim_9229);
    if (_5120 == 0)
    {
        _5120 = NOVALUE;
        goto L3; // [53] 63
    }
    else{
        _5120 = NOVALUE;
    }

    /** 		kv_delim = {kv_delim}*/
    _0 = _kv_delim_9229;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_kv_delim_9229);
    *((int *)(_2+4)) = _kv_delim_9229;
    _kv_delim_9229 = MAKE_SEQ(_1);
    DeRef(_0);
L3: 

    /** 	if atom(quotes) then*/
    _5122 = IS_ATOM(_quotes_9231);
    if (_5122 == 0)
    {
        _5122 = NOVALUE;
        goto L4; // [68] 78
    }
    else{
        _5122 = NOVALUE;
    }

    /** 		quotes = {quotes}*/
    _0 = _quotes_9231;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quotes_9231);
    *((int *)(_2+4)) = _quotes_9231;
    _quotes_9231 = MAKE_SEQ(_1);
    DeRef(_0);
L4: 

    /** 	if atom(whitespace) then*/
    _5124 = IS_ATOM(_whitespace_9233);
    if (_5124 == 0)
    {
        _5124 = NOVALUE;
        goto L5; // [83] 93
    }
    else{
        _5124 = NOVALUE;
    }

    /** 		whitespace = {whitespace}*/
    _0 = _whitespace_9233;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_whitespace_9233);
    *((int *)(_2+4)) = _whitespace_9233;
    _whitespace_9233 = MAKE_SEQ(_1);
    DeRef(_0);
L5: 

    /** 	lAllDelim = whitespace & pair_delim & kv_delim*/
    {
        int concat_list[3];

        concat_list[0] = _kv_delim_9229;
        concat_list[1] = _pair_delim_9227;
        concat_list[2] = _whitespace_9233;
        Concat_N((object_ptr)&_lAllDelim_9238, concat_list, 3);
    }

    /** 	lWhitePair = whitespace & pair_delim*/
    if (IS_SEQUENCE(_whitespace_9233) && IS_ATOM(_pair_delim_9227)) {
        Ref(_pair_delim_9227);
        Append(&_lWhitePair_9239, _whitespace_9233, _pair_delim_9227);
    }
    else if (IS_ATOM(_whitespace_9233) && IS_SEQUENCE(_pair_delim_9227)) {
        Ref(_whitespace_9233);
        Prepend(&_lWhitePair_9239, _pair_delim_9227, _whitespace_9233);
    }
    else {
        Concat((object_ptr)&_lWhitePair_9239, _whitespace_9233, _pair_delim_9227);
    }

    /** 	lStartBracket = "{[("*/
    RefDS(_5128);
    DeRefi(_lStartBracket_9240);
    _lStartBracket_9240 = _5128;

    /** 	lEndBracket   = "}])"*/
    RefDS(_5129);
    DeRefi(_lEndBracket_9241);
    _lEndBracket_9241 = _5129;

    /** 	lKeyValues = {}*/
    RefDS(_5);
    DeRef(_lKeyValues_9235);
    _lKeyValues_9235 = _5;

    /** 	pos_ = 1*/
    _pos__9244 = 1;

    /** 	while pos_ <= length(source) do*/
L6: 
    if (IS_SEQUENCE(_source_9226)){
            _5130 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5130 = 1;
    }
    if (_pos__9244 > _5130)
    goto L7; // [143] 1298

    /** 		while pos_ < length(source) do*/
L8: 
    if (IS_SEQUENCE(_source_9226)){
            _5132 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5132 = 1;
    }
    if (_pos__9244 >= _5132)
    goto L9; // [155] 192

    /** 			if find(source[pos_], whitespace) = 0 then*/
    _2 = (int)SEQ_PTR(_source_9226);
    _5134 = (int)*(((s1_ptr)_2)->base + _pos__9244);
    _5135 = find_from(_5134, _whitespace_9233, 1);
    _5134 = NOVALUE;
    if (_5135 != 0)
    goto LA; // [170] 179

    /** 				exit*/
    goto L9; // [176] 192
LA: 

    /** 			pos_ +=1*/
    _pos__9244 = _pos__9244 + 1;

    /** 		end while*/
    goto L8; // [189] 152
L9: 

    /** 		key_ = ""*/
    RefDS(_5);
    DeRef(_key__9237);
    _key__9237 = _5;

    /** 		lQuote = 0*/
    _lQuote_9243 = 0;

    /** 		lChar = 0*/
    _lChar_9245 = 0;

    /** 		lWasKV = 0*/
    _lWasKV_9247 = 0;

    /** 		if haskeys then*/
    if (_haskeys_9234 == 0)
    {
        goto LB; // [222] 431
    }
    else{
    }

    /** 			while pos_ <= length(source) do*/
LC: 
    if (IS_SEQUENCE(_source_9226)){
            _5138 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5138 = 1;
    }
    if (_pos__9244 > _5138)
    goto LD; // [233] 359

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9226);
    _lChar_9245 = (int)*(((s1_ptr)_2)->base + _pos__9244);
    if (!IS_ATOM_INT(_lChar_9245))
    _lChar_9245 = (long)DBL_PTR(_lChar_9245)->dbl;

    /** 				if find(lChar, quotes) != 0 then*/
    _5141 = find_from(_lChar_9245, _quotes_9231, 1);
    if (_5141 == 0)
    goto LE; // [252] 304

    /** 					if lChar = lQuote then*/
    if (_lChar_9245 != _lQuote_9243)
    goto LF; // [258] 279

    /** 						lQuote = 0*/
    _lQuote_9243 = 0;

    /** 						lChar = -1*/
    _lChar_9245 = -1;
    goto L10; // [276] 333
LF: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_9243 != 0)
    goto L10; // [281] 333

    /** 						lQuote = lChar*/
    _lQuote_9243 = _lChar_9245;

    /** 						lChar = -1*/
    _lChar_9245 = -1;
    goto L10; // [301] 333
LE: 

    /** 				elsif lQuote = 0 and find(lChar, lAllDelim) != 0 then*/
    _5145 = (_lQuote_9243 == 0);
    if (_5145 == 0) {
        goto L11; // [310] 332
    }
    _5147 = find_from(_lChar_9245, _lAllDelim_9238, 1);
    _5148 = (_5147 != 0);
    _5147 = NOVALUE;
    if (_5148 == 0)
    {
        DeRef(_5148);
        _5148 = NOVALUE;
        goto L11; // [324] 332
    }
    else{
        DeRef(_5148);
        _5148 = NOVALUE;
    }

    /** 					exit*/
    goto LD; // [329] 359
L11: 
L10: 

    /** 				if lChar > 0 then*/
    if (_lChar_9245 <= 0)
    goto L12; // [335] 346

    /** 					key_ &= lChar*/
    Append(&_key__9237, _key__9237, _lChar_9245);
L12: 

    /** 				pos_ += 1*/
    _pos__9244 = _pos__9244 + 1;

    /** 			end while*/
    goto LC; // [356] 230
LD: 

    /** 			if find(lChar, whitespace) != 0 then*/
    _5152 = find_from(_lChar_9245, _whitespace_9233, 1);
    if (_5152 == 0)
    goto L13; // [366] 440

    /** 				pos_ += 1*/
    _pos__9244 = _pos__9244 + 1;

    /** 				while pos_ <= length(source) do*/
L14: 
    if (IS_SEQUENCE(_source_9226)){
            _5155 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5155 = 1;
    }
    if (_pos__9244 > _5155)
    goto L13; // [386] 440

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9226);
    _lChar_9245 = (int)*(((s1_ptr)_2)->base + _pos__9244);
    if (!IS_ATOM_INT(_lChar_9245))
    _lChar_9245 = (long)DBL_PTR(_lChar_9245)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _5158 = find_from(_lChar_9245, _whitespace_9233, 1);
    if (_5158 != 0)
    goto L15; // [405] 414

    /** 						exit*/
    goto L13; // [411] 440
L15: 

    /** 					pos_ +=1*/
    _pos__9244 = _pos__9244 + 1;

    /** 				end while*/
    goto L14; // [424] 383
    goto L13; // [428] 440
LB: 

    /** 			pos_ -= 1	-- Put back the last char.*/
    _pos__9244 = _pos__9244 - 1;
L13: 

    /** 		value_ = ""*/
    RefDS(_5);
    DeRef(_value__9236);
    _value__9236 = _5;

    /** 		if find(lChar, kv_delim) != 0  or not haskeys then*/
    _5162 = find_from(_lChar_9245, _kv_delim_9229, 1);
    _5163 = (_5162 != 0);
    _5162 = NOVALUE;
    if (_5163 != 0) {
        goto L16; // [458] 470
    }
    _5165 = (_haskeys_9234 == 0);
    if (_5165 == 0)
    {
        DeRef(_5165);
        _5165 = NOVALUE;
        goto L17; // [466] 981
    }
    else{
        DeRef(_5165);
        _5165 = NOVALUE;
    }
L16: 

    /** 			if find(lChar, kv_delim) != 0 then*/
    _5166 = find_from(_lChar_9245, _kv_delim_9229, 1);
    if (_5166 == 0)
    goto L18; // [477] 489

    /** 				lWasKV = 1*/
    _lWasKV_9247 = 1;
L18: 

    /** 			pos_ += 1*/
    _pos__9244 = _pos__9244 + 1;

    /** 			while pos_ <= length(source) do*/
L19: 
    if (IS_SEQUENCE(_source_9226)){
            _5169 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5169 = 1;
    }
    if (_pos__9244 > _5169)
    goto L1A; // [505] 546

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9226);
    _lChar_9245 = (int)*(((s1_ptr)_2)->base + _pos__9244);
    if (!IS_ATOM_INT(_lChar_9245))
    _lChar_9245 = (long)DBL_PTR(_lChar_9245)->dbl;

    /** 				if find(lChar, whitespace) = 0 then*/
    _5172 = find_from(_lChar_9245, _whitespace_9233, 1);
    if (_5172 != 0)
    goto L1B; // [524] 533

    /** 					exit*/
    goto L1A; // [530] 546
L1B: 

    /** 				pos_ +=1*/
    _pos__9244 = _pos__9244 + 1;

    /** 			end while*/
    goto L19; // [543] 502
L1A: 

    /** 			lQuote = 0*/
    _lQuote_9243 = 0;

    /** 			lChar = 0*/
    _lChar_9245 = 0;

    /** 			lBracketed = {}*/
    RefDS(_5);
    DeRefi(_lBracketed_9242);
    _lBracketed_9242 = _5;

    /** 			while pos_ <= length(source) do*/
L1C: 
    if (IS_SEQUENCE(_source_9226)){
            _5175 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5175 = 1;
    }
    if (_pos__9244 > _5175)
    goto L1D; // [575] 873

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9226);
    _lChar_9245 = (int)*(((s1_ptr)_2)->base + _pos__9244);
    if (!IS_ATOM_INT(_lChar_9245))
    _lChar_9245 = (long)DBL_PTR(_lChar_9245)->dbl;

    /** 				if length(lBracketed) = 0 and find(lChar, quotes) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_9242)){
            _5178 = SEQ_PTR(_lBracketed_9242)->length;
    }
    else {
        _5178 = 1;
    }
    _5179 = (_5178 == 0);
    _5178 = NOVALUE;
    if (_5179 == 0) {
        goto L1E; // [596] 661
    }
    _5181 = find_from(_lChar_9245, _quotes_9231, 1);
    _5182 = (_5181 != 0);
    _5181 = NOVALUE;
    if (_5182 == 0)
    {
        DeRef(_5182);
        _5182 = NOVALUE;
        goto L1E; // [610] 661
    }
    else{
        DeRef(_5182);
        _5182 = NOVALUE;
    }

    /** 					if lChar = lQuote then*/
    if (_lChar_9245 != _lQuote_9243)
    goto L1F; // [615] 636

    /** 						lQuote = 0*/
    _lQuote_9243 = 0;

    /** 						lChar = -1*/
    _lChar_9245 = -1;
    goto L20; // [633] 847
L1F: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_9243 != 0)
    goto L20; // [638] 847

    /** 						lQuote = lChar*/
    _lQuote_9243 = _lChar_9245;

    /** 						lChar = -1*/
    _lChar_9245 = -1;
    goto L20; // [658] 847
L1E: 

    /** 				elsif length(value_) = 1 and value_[1] = '~' and find(lChar, lStartBracket) > 0 then*/
    if (IS_SEQUENCE(_value__9236)){
            _5185 = SEQ_PTR(_value__9236)->length;
    }
    else {
        _5185 = 1;
    }
    _5186 = (_5185 == 1);
    _5185 = NOVALUE;
    if (_5186 == 0) {
        _5187 = 0;
        goto L21; // [670] 686
    }
    _2 = (int)SEQ_PTR(_value__9236);
    _5188 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5188)) {
        _5189 = (_5188 == 126);
    }
    else {
        _5189 = binary_op(EQUALS, _5188, 126);
    }
    _5188 = NOVALUE;
    if (IS_ATOM_INT(_5189))
    _5187 = (_5189 != 0);
    else
    _5187 = DBL_PTR(_5189)->dbl != 0.0;
L21: 
    if (_5187 == 0) {
        goto L22; // [686] 725
    }
    _5191 = find_from(_lChar_9245, _lStartBracket_9240, 1);
    _5192 = (_5191 > 0);
    _5191 = NOVALUE;
    if (_5192 == 0)
    {
        DeRef(_5192);
        _5192 = NOVALUE;
        goto L22; // [700] 725
    }
    else{
        DeRef(_5192);
        _5192 = NOVALUE;
    }

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_9246 = find_from(_lChar_9245, _lStartBracket_9240, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_9241);
    _5194 = (int)*(((s1_ptr)_2)->base + _lBPos_9246);
    Append(&_lBracketed_9242, _lBracketed_9242, _5194);
    _5194 = NOVALUE;
    goto L20; // [722] 847
L22: 

    /** 				elsif find(lChar, lStartBracket) > 0 then*/
    _5196 = find_from(_lChar_9245, _lStartBracket_9240, 1);
    if (_5196 <= 0)
    goto L23; // [732] 758

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_9246 = find_from(_lChar_9245, _lStartBracket_9240, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_9241);
    _5199 = (int)*(((s1_ptr)_2)->base + _lBPos_9246);
    Append(&_lBracketed_9242, _lBracketed_9242, _5199);
    _5199 = NOVALUE;
    goto L20; // [755] 847
L23: 

    /** 				elsif length(lBracketed) != 0 and lChar = lBracketed[$] then*/
    if (IS_SEQUENCE(_lBracketed_9242)){
            _5201 = SEQ_PTR(_lBracketed_9242)->length;
    }
    else {
        _5201 = 1;
    }
    _5202 = (_5201 != 0);
    _5201 = NOVALUE;
    if (_5202 == 0) {
        goto L24; // [767] 803
    }
    if (IS_SEQUENCE(_lBracketed_9242)){
            _5204 = SEQ_PTR(_lBracketed_9242)->length;
    }
    else {
        _5204 = 1;
    }
    _2 = (int)SEQ_PTR(_lBracketed_9242);
    _5205 = (int)*(((s1_ptr)_2)->base + _5204);
    _5206 = (_lChar_9245 == _5205);
    _5205 = NOVALUE;
    if (_5206 == 0)
    {
        DeRef(_5206);
        _5206 = NOVALUE;
        goto L24; // [783] 803
    }
    else{
        DeRef(_5206);
        _5206 = NOVALUE;
    }

    /** 					lBracketed = lBracketed[1..$-1]*/
    if (IS_SEQUENCE(_lBracketed_9242)){
            _5207 = SEQ_PTR(_lBracketed_9242)->length;
    }
    else {
        _5207 = 1;
    }
    _5208 = _5207 - 1;
    _5207 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lBracketed_9242;
    RHS_Slice(_lBracketed_9242, 1, _5208);
    goto L20; // [800] 847
L24: 

    /** 				elsif length(lBracketed) = 0 and lQuote = 0 and find(lChar, lWhitePair) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_9242)){
            _5210 = SEQ_PTR(_lBracketed_9242)->length;
    }
    else {
        _5210 = 1;
    }
    _5211 = (_5210 == 0);
    _5210 = NOVALUE;
    if (_5211 == 0) {
        _5212 = 0;
        goto L25; // [812] 824
    }
    _5213 = (_lQuote_9243 == 0);
    _5212 = (_5213 != 0);
L25: 
    if (_5212 == 0) {
        goto L26; // [824] 846
    }
    _5215 = find_from(_lChar_9245, _lWhitePair_9239, 1);
    _5216 = (_5215 != 0);
    _5215 = NOVALUE;
    if (_5216 == 0)
    {
        DeRef(_5216);
        _5216 = NOVALUE;
        goto L26; // [838] 846
    }
    else{
        DeRef(_5216);
        _5216 = NOVALUE;
    }

    /** 					exit*/
    goto L1D; // [843] 873
L26: 
L20: 

    /** 				if lChar > 0 then*/
    if (_lChar_9245 <= 0)
    goto L27; // [849] 860

    /** 					value_ &= lChar*/
    Append(&_value__9236, _value__9236, _lChar_9245);
L27: 

    /** 				pos_ += 1*/
    _pos__9244 = _pos__9244 + 1;

    /** 			end while*/
    goto L1C; // [870] 572
L1D: 

    /** 			if find(lChar, whitespace) != 0  then*/
    _5220 = find_from(_lChar_9245, _whitespace_9233, 1);
    if (_5220 == 0)
    goto L28; // [880] 942

    /** 				pos_ += 1*/
    _pos__9244 = _pos__9244 + 1;

    /** 				while pos_ <= length(source) do*/
L29: 
    if (IS_SEQUENCE(_source_9226)){
            _5223 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5223 = 1;
    }
    if (_pos__9244 > _5223)
    goto L2A; // [900] 941

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9226);
    _lChar_9245 = (int)*(((s1_ptr)_2)->base + _pos__9244);
    if (!IS_ATOM_INT(_lChar_9245))
    _lChar_9245 = (long)DBL_PTR(_lChar_9245)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _5226 = find_from(_lChar_9245, _whitespace_9233, 1);
    if (_5226 != 0)
    goto L2B; // [919] 928

    /** 						exit*/
    goto L2A; // [925] 941
L2B: 

    /** 					pos_ +=1*/
    _pos__9244 = _pos__9244 + 1;

    /** 				end while*/
    goto L29; // [938] 897
L2A: 
L28: 

    /** 			if find(lChar, pair_delim) != 0  then*/
    _5229 = find_from(_lChar_9245, _pair_delim_9227, 1);
    if (_5229 == 0)
    goto L2C; // [949] 980

    /** 				pos_ += 1*/
    _pos__9244 = _pos__9244 + 1;

    /** 				if pos_ <= length(source) then*/
    if (IS_SEQUENCE(_source_9226)){
            _5232 = SEQ_PTR(_source_9226)->length;
    }
    else {
        _5232 = 1;
    }
    if (_pos__9244 > _5232)
    goto L2D; // [966] 979

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_9226);
    _lChar_9245 = (int)*(((s1_ptr)_2)->base + _pos__9244);
    if (!IS_ATOM_INT(_lChar_9245))
    _lChar_9245 = (long)DBL_PTR(_lChar_9245)->dbl;
L2D: 
L2C: 
L17: 

    /** 		if find(lChar, pair_delim) != 0  then*/
    _5235 = find_from(_lChar_9245, _pair_delim_9227, 1);
    if (_5235 == 0)
    goto L2E; // [988] 1001

    /** 			pos_ += 1*/
    _pos__9244 = _pos__9244 + 1;
L2E: 

    /** 		if length(value_) = 0 then*/
    if (IS_SEQUENCE(_value__9236)){
            _5238 = SEQ_PTR(_value__9236)->length;
    }
    else {
        _5238 = 1;
    }
    if (_5238 != 0)
    goto L2F; // [1006] 1051

    /** 			if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__9237)){
            _5240 = SEQ_PTR(_key__9237)->length;
    }
    else {
        _5240 = 1;
    }
    if (_5240 != 0)
    goto L30; // [1015] 1030

    /** 				lKeyValues = append(lKeyValues, {})*/
    RefDS(_5);
    Append(&_lKeyValues_9235, _lKeyValues_9235, _5);

    /** 				continue*/
    goto L6; // [1027] 140
L30: 

    /** 			if not lWasKV then*/
    if (_lWasKV_9247 != 0)
    goto L31; // [1032] 1050

    /** 				value_ = key_*/
    RefDS(_key__9237);
    DeRef(_value__9236);
    _value__9236 = _key__9237;

    /** 				key_ = ""*/
    RefDS(_5);
    DeRefDS(_key__9237);
    _key__9237 = _5;
L31: 
L2F: 

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__9237)){
            _5244 = SEQ_PTR(_key__9237)->length;
    }
    else {
        _5244 = 1;
    }
    if (_5244 != 0)
    goto L32; // [1056] 1080

    /** 			if haskeys then*/
    if (_haskeys_9234 == 0)
    {
        goto L33; // [1062] 1079
    }
    else{
    }

    /** 				key_ =  sprintf("p[%d]", length(lKeyValues) + 1)*/
    if (IS_SEQUENCE(_lKeyValues_9235)){
            _5247 = SEQ_PTR(_lKeyValues_9235)->length;
    }
    else {
        _5247 = 1;
    }
    _5248 = _5247 + 1;
    _5247 = NOVALUE;
    DeRefDS(_key__9237);
    _key__9237 = EPrintf(-9999999, _5246, _5248);
    _5248 = NOVALUE;
L33: 
L32: 

    /** 		if length(value_) > 0 then*/
    if (IS_SEQUENCE(_value__9236)){
            _5250 = SEQ_PTR(_value__9236)->length;
    }
    else {
        _5250 = 1;
    }
    if (_5250 <= 0)
    goto L34; // [1085] 1244

    /** 			lChar = value_[1]*/
    _2 = (int)SEQ_PTR(_value__9236);
    _lChar_9245 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lChar_9245))
    _lChar_9245 = (long)DBL_PTR(_lChar_9245)->dbl;

    /** 			lBPos = find(lChar, lStartBracket)*/
    _lBPos_9246 = find_from(_lChar_9245, _lStartBracket_9240, 1);

    /** 			if lBPos > 0 and value_[$] = lEndBracket[lBPos] then*/
    _5254 = (_lBPos_9246 > 0);
    if (_5254 == 0) {
        goto L35; // [1112] 1225
    }
    if (IS_SEQUENCE(_value__9236)){
            _5256 = SEQ_PTR(_value__9236)->length;
    }
    else {
        _5256 = 1;
    }
    _2 = (int)SEQ_PTR(_value__9236);
    _5257 = (int)*(((s1_ptr)_2)->base + _5256);
    _2 = (int)SEQ_PTR(_lEndBracket_9241);
    _5258 = (int)*(((s1_ptr)_2)->base + _lBPos_9246);
    if (IS_ATOM_INT(_5257)) {
        _5259 = (_5257 == _5258);
    }
    else {
        _5259 = binary_op(EQUALS, _5257, _5258);
    }
    _5257 = NOVALUE;
    _5258 = NOVALUE;
    if (_5259 == 0) {
        DeRef(_5259);
        _5259 = NOVALUE;
        goto L35; // [1132] 1225
    }
    else {
        if (!IS_ATOM_INT(_5259) && DBL_PTR(_5259)->dbl == 0.0){
            DeRef(_5259);
            _5259 = NOVALUE;
            goto L35; // [1132] 1225
        }
        DeRef(_5259);
        _5259 = NOVALUE;
    }
    DeRef(_5259);
    _5259 = NOVALUE;

    /** 				if lChar = '(' then*/
    if (_lChar_9245 != 40)
    goto L36; // [1137] 1184

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, haskeys)*/
    if (IS_SEQUENCE(_value__9236)){
            _5262 = SEQ_PTR(_value__9236)->length;
    }
    else {
        _5262 = 1;
    }
    _5263 = _5262 - 1;
    _5262 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5264;
    RHS_Slice(_value__9236, 2, _5263);
    Ref(_pair_delim_9227);
    DeRef(_5265);
    _5265 = _pair_delim_9227;
    Ref(_kv_delim_9229);
    DeRef(_5266);
    _5266 = _kv_delim_9229;
    Ref(_quotes_9231);
    DeRef(_5267);
    _5267 = _quotes_9231;
    Ref(_whitespace_9233);
    DeRef(_5268);
    _5268 = _whitespace_9233;
    DeRef(_5269);
    _5269 = _haskeys_9234;
    _0 = _value__9236;
    _value__9236 = _6keyvalues(_5264, _5265, _5266, _5267, _5268, _5269);
    DeRefDS(_0);
    _5264 = NOVALUE;
    _5265 = NOVALUE;
    _5266 = NOVALUE;
    _5267 = NOVALUE;
    _5268 = NOVALUE;
    _5269 = NOVALUE;
    goto L37; // [1181] 1243
L36: 

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, 0)*/
    if (IS_SEQUENCE(_value__9236)){
            _5271 = SEQ_PTR(_value__9236)->length;
    }
    else {
        _5271 = 1;
    }
    _5272 = _5271 - 1;
    _5271 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5273;
    RHS_Slice(_value__9236, 2, _5272);
    Ref(_pair_delim_9227);
    DeRef(_5274);
    _5274 = _pair_delim_9227;
    Ref(_kv_delim_9229);
    DeRef(_5275);
    _5275 = _kv_delim_9229;
    Ref(_quotes_9231);
    DeRef(_5276);
    _5276 = _quotes_9231;
    Ref(_whitespace_9233);
    DeRef(_5277);
    _5277 = _whitespace_9233;
    _0 = _value__9236;
    _value__9236 = _6keyvalues(_5273, _5274, _5275, _5276, _5277, 0);
    DeRefDS(_0);
    _5273 = NOVALUE;
    _5274 = NOVALUE;
    _5275 = NOVALUE;
    _5276 = NOVALUE;
    _5277 = NOVALUE;
    goto L37; // [1222] 1243
L35: 

    /** 			elsif lChar = '~' then*/
    if (_lChar_9245 != 126)
    goto L38; // [1227] 1242

    /** 				value_ = value_[2 .. $]*/
    if (IS_SEQUENCE(_value__9236)){
            _5280 = SEQ_PTR(_value__9236)->length;
    }
    else {
        _5280 = 1;
    }
    rhs_slice_target = (object_ptr)&_value__9236;
    RHS_Slice(_value__9236, 2, _5280);
L38: 
L37: 
L34: 

    /** 		key_ = trim(key_)*/
    RefDS(_key__9237);
    RefDS(_4282);
    _0 = _key__9237;
    _key__9237 = _6trim(_key__9237, _4282, 0);
    DeRefDS(_0);

    /** 		value_ = trim(value_)*/
    RefDS(_value__9236);
    RefDS(_4282);
    _0 = _value__9236;
    _value__9236 = _6trim(_value__9236, _4282, 0);
    DeRefDS(_0);

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__9237)){
            _5284 = SEQ_PTR(_key__9237)->length;
    }
    else {
        _5284 = 1;
    }
    if (_5284 != 0)
    goto L39; // [1269] 1282

    /** 			lKeyValues = append(lKeyValues, value_)*/
    RefDS(_value__9236);
    Append(&_lKeyValues_9235, _lKeyValues_9235, _value__9236);
    goto L6; // [1279] 140
L39: 

    /** 			lKeyValues = append(lKeyValues, {key_, value_})*/
    RefDS(_value__9236);
    RefDS(_key__9237);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key__9237;
    ((int *)_2)[2] = _value__9236;
    _5287 = MAKE_SEQ(_1);
    RefDS(_5287);
    Append(&_lKeyValues_9235, _lKeyValues_9235, _5287);
    DeRefDS(_5287);
    _5287 = NOVALUE;

    /** 	end while*/
    goto L6; // [1295] 140
L7: 

    /** 	return lKeyValues*/
    DeRefDS(_source_9226);
    DeRef(_pair_delim_9227);
    DeRef(_kv_delim_9229);
    DeRef(_quotes_9231);
    DeRef(_whitespace_9233);
    DeRef(_value__9236);
    DeRef(_key__9237);
    DeRef(_lAllDelim_9238);
    DeRef(_lWhitePair_9239);
    DeRefi(_lStartBracket_9240);
    DeRefi(_lEndBracket_9241);
    DeRefi(_lBracketed_9242);
    DeRef(_5145);
    _5145 = NOVALUE;
    DeRef(_5163);
    _5163 = NOVALUE;
    DeRef(_5179);
    _5179 = NOVALUE;
    DeRef(_5186);
    _5186 = NOVALUE;
    DeRef(_5202);
    _5202 = NOVALUE;
    DeRef(_5189);
    _5189 = NOVALUE;
    DeRef(_5208);
    _5208 = NOVALUE;
    DeRef(_5211);
    _5211 = NOVALUE;
    DeRef(_5213);
    _5213 = NOVALUE;
    DeRef(_5254);
    _5254 = NOVALUE;
    DeRef(_5263);
    _5263 = NOVALUE;
    DeRef(_5272);
    _5272 = NOVALUE;
    return _lKeyValues_9235;
    ;
}


int _6escape(int _s_9474, int _what_9475)
{
    int _r_9477 = NOVALUE;
    int _5294 = NOVALUE;
    int _5292 = NOVALUE;
    int _5291 = NOVALUE;
    int _5290 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence r = ""*/
    RefDS(_5);
    DeRef(_r_9477);
    _r_9477 = _5;

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_9474)){
            _5290 = SEQ_PTR(_s_9474)->length;
    }
    else {
        _5290 = 1;
    }
    {
        int _i_9479;
        _i_9479 = 1;
L1: 
        if (_i_9479 > _5290){
            goto L2; // [17] 62
        }

        /** 		if find(s[i], what) then*/
        _2 = (int)SEQ_PTR(_s_9474);
        _5291 = (int)*(((s1_ptr)_2)->base + _i_9479);
        _5292 = find_from(_5291, _what_9475, 1);
        _5291 = NOVALUE;
        if (_5292 == 0)
        {
            _5292 = NOVALUE;
            goto L3; // [35] 45
        }
        else{
            _5292 = NOVALUE;
        }

        /** 			r &= "\\"*/
        Concat((object_ptr)&_r_9477, _r_9477, _3723);
L3: 

        /** 		r &= s[i]*/
        _2 = (int)SEQ_PTR(_s_9474);
        _5294 = (int)*(((s1_ptr)_2)->base + _i_9479);
        if (IS_SEQUENCE(_r_9477) && IS_ATOM(_5294)) {
            Ref(_5294);
            Append(&_r_9477, _r_9477, _5294);
        }
        else if (IS_ATOM(_r_9477) && IS_SEQUENCE(_5294)) {
        }
        else {
            Concat((object_ptr)&_r_9477, _r_9477, _5294);
        }
        _5294 = NOVALUE;

        /** 	end for*/
        _i_9479 = _i_9479 + 1;
        goto L1; // [57] 24
L2: 
        ;
    }

    /** 	return r*/
    DeRefDS(_s_9474);
    DeRefDSi(_what_9475);
    return _r_9477;
    ;
}


int _6quote(int _text_in_9489, int _quote_pair_9490, int _esc_9492, int _sp_9494)
{
    int _5382 = NOVALUE;
    int _5381 = NOVALUE;
    int _5380 = NOVALUE;
    int _5378 = NOVALUE;
    int _5377 = NOVALUE;
    int _5376 = NOVALUE;
    int _5374 = NOVALUE;
    int _5373 = NOVALUE;
    int _5372 = NOVALUE;
    int _5371 = NOVALUE;
    int _5370 = NOVALUE;
    int _5369 = NOVALUE;
    int _5368 = NOVALUE;
    int _5367 = NOVALUE;
    int _5366 = NOVALUE;
    int _5364 = NOVALUE;
    int _5363 = NOVALUE;
    int _5362 = NOVALUE;
    int _5360 = NOVALUE;
    int _5359 = NOVALUE;
    int _5358 = NOVALUE;
    int _5357 = NOVALUE;
    int _5356 = NOVALUE;
    int _5355 = NOVALUE;
    int _5354 = NOVALUE;
    int _5353 = NOVALUE;
    int _5352 = NOVALUE;
    int _5350 = NOVALUE;
    int _5349 = NOVALUE;
    int _5347 = NOVALUE;
    int _5346 = NOVALUE;
    int _5345 = NOVALUE;
    int _5343 = NOVALUE;
    int _5342 = NOVALUE;
    int _5341 = NOVALUE;
    int _5340 = NOVALUE;
    int _5339 = NOVALUE;
    int _5338 = NOVALUE;
    int _5337 = NOVALUE;
    int _5336 = NOVALUE;
    int _5335 = NOVALUE;
    int _5334 = NOVALUE;
    int _5333 = NOVALUE;
    int _5332 = NOVALUE;
    int _5331 = NOVALUE;
    int _5330 = NOVALUE;
    int _5329 = NOVALUE;
    int _5328 = NOVALUE;
    int _5327 = NOVALUE;
    int _5324 = NOVALUE;
    int _5323 = NOVALUE;
    int _5322 = NOVALUE;
    int _5321 = NOVALUE;
    int _5320 = NOVALUE;
    int _5319 = NOVALUE;
    int _5318 = NOVALUE;
    int _5317 = NOVALUE;
    int _5316 = NOVALUE;
    int _5315 = NOVALUE;
    int _5314 = NOVALUE;
    int _5313 = NOVALUE;
    int _5312 = NOVALUE;
    int _5311 = NOVALUE;
    int _5308 = NOVALUE;
    int _5306 = NOVALUE;
    int _5305 = NOVALUE;
    int _5303 = NOVALUE;
    int _5301 = NOVALUE;
    int _5300 = NOVALUE;
    int _5299 = NOVALUE;
    int _5297 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_9489)){
            _5297 = SEQ_PTR(_text_in_9489)->length;
    }
    else {
        _5297 = 1;
    }
    if (_5297 != 0)
    goto L1; // [12] 23

    /** 		return text_in*/
    DeRef(_quote_pair_9490);
    DeRef(_sp_9494);
    return _text_in_9489;
L1: 

    /** 	if atom(quote_pair) then*/
    _5299 = IS_ATOM(_quote_pair_9490);
    if (_5299 == 0)
    {
        _5299 = NOVALUE;
        goto L2; // [28] 48
    }
    else{
        _5299 = NOVALUE;
    }

    /** 		quote_pair = {{quote_pair}, {quote_pair}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_9490);
    *((int *)(_2+4)) = _quote_pair_9490;
    _5300 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_9490);
    *((int *)(_2+4)) = _quote_pair_9490;
    _5301 = MAKE_SEQ(_1);
    DeRef(_quote_pair_9490);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5300;
    ((int *)_2)[2] = _5301;
    _quote_pair_9490 = MAKE_SEQ(_1);
    _5301 = NOVALUE;
    _5300 = NOVALUE;
    goto L3; // [45] 91
L2: 

    /** 	elsif length(quote_pair) = 1 then*/
    if (IS_SEQUENCE(_quote_pair_9490)){
            _5303 = SEQ_PTR(_quote_pair_9490)->length;
    }
    else {
        _5303 = 1;
    }
    if (_5303 != 1)
    goto L4; // [53] 74

    /** 		quote_pair = {quote_pair[1], quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5305 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5306 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5306);
    Ref(_5305);
    DeRef(_quote_pair_9490);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5305;
    ((int *)_2)[2] = _5306;
    _quote_pair_9490 = MAKE_SEQ(_1);
    _5306 = NOVALUE;
    _5305 = NOVALUE;
    goto L3; // [71] 91
L4: 

    /** 	elsif length(quote_pair) = 0 then*/
    if (IS_SEQUENCE(_quote_pair_9490)){
            _5308 = SEQ_PTR(_quote_pair_9490)->length;
    }
    else {
        _5308 = 1;
    }
    if (_5308 != 0)
    goto L5; // [79] 90

    /** 		quote_pair = {"\"", "\""}*/
    RefDS(_5289);
    RefDS(_5289);
    DeRef(_quote_pair_9490);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5289;
    ((int *)_2)[2] = _5289;
    _quote_pair_9490 = MAKE_SEQ(_1);
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_9489);
    _5311 = (int)*(((s1_ptr)_2)->base + 1);
    _5312 = IS_SEQUENCE(_5311);
    _5311 = NOVALUE;
    if (_5312 == 0)
    {
        _5312 = NOVALUE;
        goto L6; // [100] 168
    }
    else{
        _5312 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_9489)){
            _5313 = SEQ_PTR(_text_in_9489)->length;
    }
    else {
        _5313 = 1;
    }
    {
        int _i_9517;
        _i_9517 = 1;
L7: 
        if (_i_9517 > _5313){
            goto L8; // [108] 161
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_9489);
        _5314 = (int)*(((s1_ptr)_2)->base + _i_9517);
        _5315 = IS_SEQUENCE(_5314);
        _5314 = NOVALUE;
        if (_5315 == 0)
        {
            _5315 = NOVALUE;
            goto L9; // [124] 154
        }
        else{
            _5315 = NOVALUE;
        }

        /** 				text_in[i] = quote(text_in[i], quote_pair, esc, sp)*/
        _2 = (int)SEQ_PTR(_text_in_9489);
        _5316 = (int)*(((s1_ptr)_2)->base + _i_9517);
        Ref(_quote_pair_9490);
        DeRef(_5317);
        _5317 = _quote_pair_9490;
        DeRef(_5318);
        _5318 = _esc_9492;
        Ref(_sp_9494);
        DeRef(_5319);
        _5319 = _sp_9494;
        Ref(_5316);
        _5320 = _6quote(_5316, _5317, _5318, _5319);
        _5316 = NOVALUE;
        _5317 = NOVALUE;
        _5318 = NOVALUE;
        _5319 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_9489);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_9489 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9517);
        _1 = *(int *)_2;
        *(int *)_2 = _5320;
        if( _1 != _5320 ){
            DeRef(_1);
        }
        _5320 = NOVALUE;
L9: 

        /** 		end for*/
        _i_9517 = _i_9517 + 1;
        goto L7; // [156] 115
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pair_9490);
    DeRef(_sp_9494);
    return _text_in_9489;
L6: 

    /** 	for i = 1 to length(sp) do*/
    if (IS_SEQUENCE(_sp_9494)){
            _5321 = SEQ_PTR(_sp_9494)->length;
    }
    else {
        _5321 = 1;
    }
    {
        int _i_9528;
        _i_9528 = 1;
LA: 
        if (_i_9528 > _5321){
            goto LB; // [173] 222
        }

        /** 		if find(sp[i], text_in) then*/
        _2 = (int)SEQ_PTR(_sp_9494);
        _5322 = (int)*(((s1_ptr)_2)->base + _i_9528);
        _5323 = find_from(_5322, _text_in_9489, 1);
        _5322 = NOVALUE;
        if (_5323 == 0)
        {
            _5323 = NOVALUE;
            goto LC; // [191] 199
        }
        else{
            _5323 = NOVALUE;
        }

        /** 			exit*/
        goto LB; // [196] 222
LC: 

        /** 		if i = length(sp) then*/
        if (IS_SEQUENCE(_sp_9494)){
                _5324 = SEQ_PTR(_sp_9494)->length;
        }
        else {
            _5324 = 1;
        }
        if (_i_9528 != _5324)
        goto LD; // [204] 215

        /** 			return text_in*/
        DeRef(_quote_pair_9490);
        DeRef(_sp_9494);
        return _text_in_9489;
LD: 

        /** 	end for*/
        _i_9528 = _i_9528 + 1;
        goto LA; // [217] 180
LB: 
        ;
    }

    /** 	if esc >= 0  then*/
    if (_esc_9492 < 0)
    goto LE; // [224] 563

    /** 		if atom(quote_pair[1]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5327 = (int)*(((s1_ptr)_2)->base + 1);
    _5328 = IS_ATOM(_5327);
    _5327 = NOVALUE;
    if (_5328 == 0)
    {
        _5328 = NOVALUE;
        goto LF; // [237] 255
    }
    else{
        _5328 = NOVALUE;
    }

    /** 			quote_pair[1] = {quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5329 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5329);
    *((int *)(_2+4)) = _5329;
    _5330 = MAKE_SEQ(_1);
    _5329 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_9490 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _5330;
    if( _1 != _5330 ){
        DeRef(_1);
    }
    _5330 = NOVALUE;
LF: 

    /** 		if atom(quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5331 = (int)*(((s1_ptr)_2)->base + 2);
    _5332 = IS_ATOM(_5331);
    _5331 = NOVALUE;
    if (_5332 == 0)
    {
        _5332 = NOVALUE;
        goto L10; // [264] 282
    }
    else{
        _5332 = NOVALUE;
    }

    /** 			quote_pair[2] = {quote_pair[2]}*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5333 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5333);
    *((int *)(_2+4)) = _5333;
    _5334 = MAKE_SEQ(_1);
    _5333 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_9490 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _5334;
    if( _1 != _5334 ){
        DeRef(_1);
    }
    _5334 = NOVALUE;
L10: 

    /** 		if equal(quote_pair[1], quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5335 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5336 = (int)*(((s1_ptr)_2)->base + 2);
    if (_5335 == _5336)
    _5337 = 1;
    else if (IS_ATOM_INT(_5335) && IS_ATOM_INT(_5336))
    _5337 = 0;
    else
    _5337 = (compare(_5335, _5336) == 0);
    _5335 = NOVALUE;
    _5336 = NOVALUE;
    if (_5337 == 0)
    {
        _5337 = NOVALUE;
        goto L11; // [296] 374
    }
    else{
        _5337 = NOVALUE;
    }

    /** 			if match(quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5338 = (int)*(((s1_ptr)_2)->base + 1);
    _5339 = e_match_from(_5338, _text_in_9489, 1);
    _5338 = NOVALUE;
    if (_5339 == 0)
    {
        _5339 = NOVALUE;
        goto L12; // [310] 562
    }
    else{
        _5339 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5340 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5340)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5340)) {
        Prepend(&_5341, _5340, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5341, _esc_9492, _5340);
    }
    _5340 = NOVALUE;
    _5342 = e_match_from(_5341, _text_in_9489, 1);
    DeRefDS(_5341);
    _5341 = NOVALUE;
    if (_5342 == 0)
    {
        _5342 = NOVALUE;
        goto L13; // [328] 347
    }
    else{
        _5342 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc, text_in, esc & esc)*/
    Concat((object_ptr)&_5343, _esc_9492, _esc_9492);
    RefDS(_text_in_9489);
    _0 = _text_in_9489;
    _text_in_9489 = _5match_replace(_esc_9492, _text_in_9489, _5343, 0);
    DeRefDS(_0);
    _5343 = NOVALUE;
L13: 

    /** 				text_in = search:match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5345 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5346 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5346)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5346)) {
        Prepend(&_5347, _5346, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5347, _esc_9492, _5346);
    }
    _5346 = NOVALUE;
    Ref(_5345);
    RefDS(_text_in_9489);
    _0 = _text_in_9489;
    _text_in_9489 = _5match_replace(_5345, _text_in_9489, _5347, 0);
    DeRefDS(_0);
    _5345 = NOVALUE;
    _5347 = NOVALUE;
    goto L12; // [371] 562
L11: 

    /** 			if match(quote_pair[1], text_in) or*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5349 = (int)*(((s1_ptr)_2)->base + 1);
    _5350 = e_match_from(_5349, _text_in_9489, 1);
    _5349 = NOVALUE;
    if (_5350 != 0) {
        goto L14; // [385] 403
    }
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5352 = (int)*(((s1_ptr)_2)->base + 2);
    _5353 = e_match_from(_5352, _text_in_9489, 1);
    _5352 = NOVALUE;
    if (_5353 == 0)
    {
        _5353 = NOVALUE;
        goto L15; // [399] 475
    }
    else{
        _5353 = NOVALUE;
    }
L14: 

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5354 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5354)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5354)) {
        Prepend(&_5355, _5354, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5355, _esc_9492, _5354);
    }
    _5354 = NOVALUE;
    _5356 = e_match_from(_5355, _text_in_9489, 1);
    DeRefDS(_5355);
    _5355 = NOVALUE;
    if (_5356 == 0)
    {
        _5356 = NOVALUE;
        goto L16; // [418] 451
    }
    else{
        _5356 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[1], text_in, esc & esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5357 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5357)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5357)) {
        Prepend(&_5358, _5357, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5358, _esc_9492, _5357);
    }
    _5357 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5359 = (int)*(((s1_ptr)_2)->base + 1);
    {
        int concat_list[3];

        concat_list[0] = _5359;
        concat_list[1] = _esc_9492;
        concat_list[2] = _esc_9492;
        Concat_N((object_ptr)&_5360, concat_list, 3);
    }
    _5359 = NOVALUE;
    RefDS(_text_in_9489);
    _0 = _text_in_9489;
    _text_in_9489 = _5match_replace(_5358, _text_in_9489, _5360, 0);
    DeRefDS(_0);
    _5358 = NOVALUE;
    _5360 = NOVALUE;
L16: 

    /** 				text_in = match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5362 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5363 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5363)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5363)) {
        Prepend(&_5364, _5363, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5364, _esc_9492, _5363);
    }
    _5363 = NOVALUE;
    Ref(_5362);
    RefDS(_text_in_9489);
    _0 = _text_in_9489;
    _text_in_9489 = _5match_replace(_5362, _text_in_9489, _5364, 0);
    DeRefDS(_0);
    _5362 = NOVALUE;
    _5364 = NOVALUE;
L15: 

    /** 			if match(quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5366 = (int)*(((s1_ptr)_2)->base + 2);
    _5367 = e_match_from(_5366, _text_in_9489, 1);
    _5366 = NOVALUE;
    if (_5367 == 0)
    {
        _5367 = NOVALUE;
        goto L17; // [486] 561
    }
    else{
        _5367 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5368 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5368)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5368)) {
        Prepend(&_5369, _5368, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5369, _esc_9492, _5368);
    }
    _5368 = NOVALUE;
    _5370 = e_match_from(_5369, _text_in_9489, 1);
    DeRefDS(_5369);
    _5369 = NOVALUE;
    if (_5370 == 0)
    {
        _5370 = NOVALUE;
        goto L18; // [504] 537
    }
    else{
        _5370 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[2], text_in, esc & esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5371 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5371)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5371)) {
        Prepend(&_5372, _5371, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5372, _esc_9492, _5371);
    }
    _5371 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5373 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5373;
        concat_list[1] = _esc_9492;
        concat_list[2] = _esc_9492;
        Concat_N((object_ptr)&_5374, concat_list, 3);
    }
    _5373 = NOVALUE;
    RefDS(_text_in_9489);
    _0 = _text_in_9489;
    _text_in_9489 = _5match_replace(_5372, _text_in_9489, _5374, 0);
    DeRefDS(_0);
    _5372 = NOVALUE;
    _5374 = NOVALUE;
L18: 

    /** 				text_in = search:match_replace(quote_pair[2], text_in, esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5376 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5377 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_9492) && IS_ATOM(_5377)) {
    }
    else if (IS_ATOM(_esc_9492) && IS_SEQUENCE(_5377)) {
        Prepend(&_5378, _5377, _esc_9492);
    }
    else {
        Concat((object_ptr)&_5378, _esc_9492, _5377);
    }
    _5377 = NOVALUE;
    Ref(_5376);
    RefDS(_text_in_9489);
    _0 = _text_in_9489;
    _text_in_9489 = _5match_replace(_5376, _text_in_9489, _5378, 0);
    DeRefDS(_0);
    _5376 = NOVALUE;
    _5378 = NOVALUE;
L17: 
L12: 
LE: 

    /** 	return quote_pair[1] & text_in & quote_pair[2]*/
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5380 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_9490);
    _5381 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5381;
        concat_list[1] = _text_in_9489;
        concat_list[2] = _5380;
        Concat_N((object_ptr)&_5382, concat_list, 3);
    }
    _5381 = NOVALUE;
    _5380 = NOVALUE;
    DeRefDS(_text_in_9489);
    DeRef(_quote_pair_9490);
    DeRef(_sp_9494);
    return _5382;
    ;
}


int _6format(int _format_pattern_9710, int _arg_list_9711)
{
    int _result_9712 = NOVALUE;
    int _in_token_9713 = NOVALUE;
    int _tch_9714 = NOVALUE;
    int _i_9715 = NOVALUE;
    int _tend_9716 = NOVALUE;
    int _cap_9717 = NOVALUE;
    int _align_9718 = NOVALUE;
    int _psign_9719 = NOVALUE;
    int _msign_9720 = NOVALUE;
    int _zfill_9721 = NOVALUE;
    int _bwz_9722 = NOVALUE;
    int _spacer_9723 = NOVALUE;
    int _alt_9724 = NOVALUE;
    int _width_9725 = NOVALUE;
    int _decs_9726 = NOVALUE;
    int _pos_9727 = NOVALUE;
    int _argn_9728 = NOVALUE;
    int _argl_9729 = NOVALUE;
    int _trimming_9730 = NOVALUE;
    int _hexout_9731 = NOVALUE;
    int _binout_9732 = NOVALUE;
    int _tsep_9733 = NOVALUE;
    int _istext_9734 = NOVALUE;
    int _prevargv_9735 = NOVALUE;
    int _currargv_9736 = NOVALUE;
    int _idname_9737 = NOVALUE;
    int _envsym_9738 = NOVALUE;
    int _envvar_9739 = NOVALUE;
    int _ep_9740 = NOVALUE;
    int _pflag_9741 = NOVALUE;
    int _count_9742 = NOVALUE;
    int _sp_9814 = NOVALUE;
    int _sp_9849 = NOVALUE;
    int _argtext_9900 = NOVALUE;
    int _tempv_10137 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2649_10192 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2646_10191 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2707_10199 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2704_10198 = NOVALUE;
    int _x_inlined_pretty_sprint_at_2701_10197 = NOVALUE;
    int _msg_inlined_crash_at_2861_10221 = NOVALUE;
    int _dpos_10283 = NOVALUE;
    int _dist_10284 = NOVALUE;
    int _bracketed_10285 = NOVALUE;
    int _5909 = NOVALUE;
    int _5908 = NOVALUE;
    int _5907 = NOVALUE;
    int _5905 = NOVALUE;
    int _5904 = NOVALUE;
    int _5903 = NOVALUE;
    int _5900 = NOVALUE;
    int _5899 = NOVALUE;
    int _5896 = NOVALUE;
    int _5894 = NOVALUE;
    int _5891 = NOVALUE;
    int _5890 = NOVALUE;
    int _5889 = NOVALUE;
    int _5886 = NOVALUE;
    int _5883 = NOVALUE;
    int _5882 = NOVALUE;
    int _5881 = NOVALUE;
    int _5880 = NOVALUE;
    int _5877 = NOVALUE;
    int _5876 = NOVALUE;
    int _5875 = NOVALUE;
    int _5872 = NOVALUE;
    int _5870 = NOVALUE;
    int _5867 = NOVALUE;
    int _5866 = NOVALUE;
    int _5865 = NOVALUE;
    int _5864 = NOVALUE;
    int _5861 = NOVALUE;
    int _5856 = NOVALUE;
    int _5855 = NOVALUE;
    int _5854 = NOVALUE;
    int _5853 = NOVALUE;
    int _5851 = NOVALUE;
    int _5850 = NOVALUE;
    int _5849 = NOVALUE;
    int _5848 = NOVALUE;
    int _5847 = NOVALUE;
    int _5846 = NOVALUE;
    int _5845 = NOVALUE;
    int _5840 = NOVALUE;
    int _5836 = NOVALUE;
    int _5835 = NOVALUE;
    int _5833 = NOVALUE;
    int _5831 = NOVALUE;
    int _5830 = NOVALUE;
    int _5829 = NOVALUE;
    int _5828 = NOVALUE;
    int _5827 = NOVALUE;
    int _5824 = NOVALUE;
    int _5822 = NOVALUE;
    int _5821 = NOVALUE;
    int _5820 = NOVALUE;
    int _5819 = NOVALUE;
    int _5816 = NOVALUE;
    int _5815 = NOVALUE;
    int _5813 = NOVALUE;
    int _5812 = NOVALUE;
    int _5811 = NOVALUE;
    int _5810 = NOVALUE;
    int _5809 = NOVALUE;
    int _5806 = NOVALUE;
    int _5805 = NOVALUE;
    int _5804 = NOVALUE;
    int _5801 = NOVALUE;
    int _5800 = NOVALUE;
    int _5798 = NOVALUE;
    int _5794 = NOVALUE;
    int _5793 = NOVALUE;
    int _5791 = NOVALUE;
    int _5790 = NOVALUE;
    int _5782 = NOVALUE;
    int _5778 = NOVALUE;
    int _5776 = NOVALUE;
    int _5775 = NOVALUE;
    int _5774 = NOVALUE;
    int _5771 = NOVALUE;
    int _5769 = NOVALUE;
    int _5768 = NOVALUE;
    int _5767 = NOVALUE;
    int _5765 = NOVALUE;
    int _5764 = NOVALUE;
    int _5763 = NOVALUE;
    int _5762 = NOVALUE;
    int _5759 = NOVALUE;
    int _5758 = NOVALUE;
    int _5757 = NOVALUE;
    int _5755 = NOVALUE;
    int _5754 = NOVALUE;
    int _5753 = NOVALUE;
    int _5752 = NOVALUE;
    int _5750 = NOVALUE;
    int _5748 = NOVALUE;
    int _5746 = NOVALUE;
    int _5744 = NOVALUE;
    int _5742 = NOVALUE;
    int _5740 = NOVALUE;
    int _5739 = NOVALUE;
    int _5738 = NOVALUE;
    int _5737 = NOVALUE;
    int _5736 = NOVALUE;
    int _5735 = NOVALUE;
    int _5733 = NOVALUE;
    int _5732 = NOVALUE;
    int _5730 = NOVALUE;
    int _5729 = NOVALUE;
    int _5727 = NOVALUE;
    int _5725 = NOVALUE;
    int _5724 = NOVALUE;
    int _5721 = NOVALUE;
    int _5719 = NOVALUE;
    int _5715 = NOVALUE;
    int _5713 = NOVALUE;
    int _5712 = NOVALUE;
    int _5711 = NOVALUE;
    int _5709 = NOVALUE;
    int _5708 = NOVALUE;
    int _5707 = NOVALUE;
    int _5706 = NOVALUE;
    int _5705 = NOVALUE;
    int _5703 = NOVALUE;
    int _5701 = NOVALUE;
    int _5700 = NOVALUE;
    int _5699 = NOVALUE;
    int _5698 = NOVALUE;
    int _5694 = NOVALUE;
    int _5691 = NOVALUE;
    int _5690 = NOVALUE;
    int _5687 = NOVALUE;
    int _5686 = NOVALUE;
    int _5685 = NOVALUE;
    int _5683 = NOVALUE;
    int _5682 = NOVALUE;
    int _5681 = NOVALUE;
    int _5680 = NOVALUE;
    int _5678 = NOVALUE;
    int _5676 = NOVALUE;
    int _5675 = NOVALUE;
    int _5674 = NOVALUE;
    int _5673 = NOVALUE;
    int _5672 = NOVALUE;
    int _5671 = NOVALUE;
    int _5669 = NOVALUE;
    int _5668 = NOVALUE;
    int _5667 = NOVALUE;
    int _5665 = NOVALUE;
    int _5664 = NOVALUE;
    int _5663 = NOVALUE;
    int _5662 = NOVALUE;
    int _5660 = NOVALUE;
    int _5657 = NOVALUE;
    int _5656 = NOVALUE;
    int _5654 = NOVALUE;
    int _5653 = NOVALUE;
    int _5651 = NOVALUE;
    int _5648 = NOVALUE;
    int _5647 = NOVALUE;
    int _5644 = NOVALUE;
    int _5642 = NOVALUE;
    int _5638 = NOVALUE;
    int _5636 = NOVALUE;
    int _5635 = NOVALUE;
    int _5634 = NOVALUE;
    int _5632 = NOVALUE;
    int _5630 = NOVALUE;
    int _5629 = NOVALUE;
    int _5628 = NOVALUE;
    int _5627 = NOVALUE;
    int _5626 = NOVALUE;
    int _5624 = NOVALUE;
    int _5622 = NOVALUE;
    int _5621 = NOVALUE;
    int _5620 = NOVALUE;
    int _5619 = NOVALUE;
    int _5617 = NOVALUE;
    int _5614 = NOVALUE;
    int _5612 = NOVALUE;
    int _5611 = NOVALUE;
    int _5610 = NOVALUE;
    int _5609 = NOVALUE;
    int _5608 = NOVALUE;
    int _5606 = NOVALUE;
    int _5605 = NOVALUE;
    int _5604 = NOVALUE;
    int _5602 = NOVALUE;
    int _5601 = NOVALUE;
    int _5600 = NOVALUE;
    int _5599 = NOVALUE;
    int _5597 = NOVALUE;
    int _5596 = NOVALUE;
    int _5595 = NOVALUE;
    int _5592 = NOVALUE;
    int _5591 = NOVALUE;
    int _5590 = NOVALUE;
    int _5589 = NOVALUE;
    int _5587 = NOVALUE;
    int _5586 = NOVALUE;
    int _5585 = NOVALUE;
    int _5584 = NOVALUE;
    int _5583 = NOVALUE;
    int _5580 = NOVALUE;
    int _5579 = NOVALUE;
    int _5578 = NOVALUE;
    int _5577 = NOVALUE;
    int _5575 = NOVALUE;
    int _5574 = NOVALUE;
    int _5573 = NOVALUE;
    int _5571 = NOVALUE;
    int _5570 = NOVALUE;
    int _5569 = NOVALUE;
    int _5567 = NOVALUE;
    int _5560 = NOVALUE;
    int _5558 = NOVALUE;
    int _5557 = NOVALUE;
    int _5550 = NOVALUE;
    int _5543 = NOVALUE;
    int _5539 = NOVALUE;
    int _5537 = NOVALUE;
    int _5536 = NOVALUE;
    int _5533 = NOVALUE;
    int _5531 = NOVALUE;
    int _5529 = NOVALUE;
    int _5526 = NOVALUE;
    int _5524 = NOVALUE;
    int _5523 = NOVALUE;
    int _5522 = NOVALUE;
    int _5521 = NOVALUE;
    int _5520 = NOVALUE;
    int _5517 = NOVALUE;
    int _5515 = NOVALUE;
    int _5514 = NOVALUE;
    int _5513 = NOVALUE;
    int _5510 = NOVALUE;
    int _5508 = NOVALUE;
    int _5506 = NOVALUE;
    int _5503 = NOVALUE;
    int _5502 = NOVALUE;
    int _5495 = NOVALUE;
    int _5492 = NOVALUE;
    int _5491 = NOVALUE;
    int _5484 = NOVALUE;
    int _5479 = NOVALUE;
    int _5476 = NOVALUE;
    int _5466 = NOVALUE;
    int _5464 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(arg_list) then*/
    _5464 = IS_ATOM(_arg_list_9711);
    if (_5464 == 0)
    {
        _5464 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _5464 = NOVALUE;
    }

    /** 		arg_list = {arg_list}*/
    _0 = _arg_list_9711;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_arg_list_9711);
    *((int *)(_2+4)) = _arg_list_9711;
    _arg_list_9711 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	result = ""*/
    RefDS(_5);
    DeRef(_result_9712);
    _result_9712 = _5;

    /** 	in_token = 0*/
    _in_token_9713 = 0;

    /** 	i = 0*/
    _i_9715 = 0;

    /** 	tend = 0*/
    _tend_9716 = 0;

    /** 	argl = 0*/
    _argl_9729 = 0;

    /** 	spacer = 0*/
    _spacer_9723 = 0;

    /** 	prevargv = 0*/
    DeRef(_prevargv_9735);
    _prevargv_9735 = 0;

    /**     while i < length(format_pattern) do*/
L2: 
    _5466 = 6;
    if (_i_9715 >= 6)
    goto L3; // [73] 3791

    /**     	i += 1*/
    _i_9715 = _i_9715 + 1;

    /**     	tch = format_pattern[i]*/
    _2 = (int)SEQ_PTR(_format_pattern_9710);
    _tch_9714 = (int)*(((s1_ptr)_2)->base + _i_9715);

    /**     	if not in_token then*/
    if (_in_token_9713 != 0)
    goto L4; // [95] 260

    /**     		if tch = '[' then*/
    if (_tch_9714 != 91)
    goto L5; // [100] 250

    /**     			in_token = 1*/
    _in_token_9713 = 1;

    /**     			tend = 0*/
    _tend_9716 = 0;

    /** 				cap = 0*/
    _cap_9717 = 0;

    /** 				align = 0*/
    _align_9718 = 0;

    /** 				psign = 0*/
    _psign_9719 = 0;

    /** 				msign = 0*/
    _msign_9720 = 0;

    /** 				zfill = 0*/
    _zfill_9721 = 0;

    /** 				bwz = 0*/
    _bwz_9722 = 0;

    /** 				spacer = 0*/
    _spacer_9723 = 0;

    /** 				alt = 0*/
    _alt_9724 = 0;

    /**     			width = 0*/
    _width_9725 = 0;

    /**     			decs = -1*/
    _decs_9726 = -1;

    /**     			argn = 0*/
    _argn_9728 = 0;

    /**     			hexout = 0*/
    _hexout_9731 = 0;

    /**     			binout = 0*/
    _binout_9732 = 0;

    /**     			trimming = 0*/
    _trimming_9730 = 0;

    /**     			tsep = 0*/
    _tsep_9733 = 0;

    /**     			istext = 0*/
    _istext_9734 = 0;

    /**     			idname = ""*/
    RefDS(_5);
    DeRef(_idname_9737);
    _idname_9737 = _5;

    /**     			envvar = ""*/
    RefDS(_5);
    DeRefi(_envvar_9739);
    _envvar_9739 = _5;

    /**     			envsym = ""*/
    RefDS(_5);
    DeRef(_envsym_9738);
    _envsym_9738 = _5;
    goto L2; // [247] 70
L5: 

    /**     			result &= tch*/
    Append(&_result_9712, _result_9712, _tch_9714);
    goto L2; // [257] 70
L4: 

    /** 			switch tch do*/
    _0 = _tch_9714;
    switch ( _0 ){ 

        /**     			case ']' then*/
        case 93:

        /**     				in_token = 0*/
        _in_token_9713 = 0;

        /**     				tend = i*/
        _tend_9716 = _i_9715;
        goto L6; // [285] 1218

        /**     			case '[' then*/
        case 91:

        /** 	    			result &= tch*/
        Append(&_result_9712, _result_9712, _tch_9714);

        /** 	    			while i < length(format_pattern) do*/
L7: 
        _5476 = 6;
        if (_i_9715 >= 6)
        goto L6; // [305] 1218

        /** 	    				i += 1*/
        _i_9715 = _i_9715 + 1;

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _5479 = (int)*(((s1_ptr)_2)->base + _i_9715);
        if (_5479 != 93)
        goto L7; // [323] 302

        /** 	    					in_token = 0*/
        _in_token_9713 = 0;

        /** 	    					tend = 0*/
        _tend_9716 = 0;

        /** 	    					exit*/
        goto L6; // [343] 1218

        /** 	    			end while*/
        goto L7; // [348] 302
        goto L6; // [351] 1218

        /** 	    		case 'w', 'u', 'l' then*/
        case 119:
        case 117:
        case 108:

        /** 	    			cap = tch*/
        _cap_9717 = _tch_9714;
        goto L6; // [368] 1218

        /** 	    		case 'b' then*/
        case 98:

        /** 	    			bwz = 1*/
        _bwz_9722 = 1;
        goto L6; // [381] 1218

        /** 	    		case 's' then*/
        case 115:

        /** 	    			spacer = 1*/
        _spacer_9723 = 1;
        goto L6; // [394] 1218

        /** 	    		case 't' then*/
        case 116:

        /** 	    			trimming = 1*/
        _trimming_9730 = 1;
        goto L6; // [407] 1218

        /** 	    		case 'z' then*/
        case 122:

        /** 	    			zfill = 1*/
        _zfill_9721 = 1;
        goto L6; // [420] 1218

        /** 	    		case 'X' then*/
        case 88:

        /** 	    			hexout = 1*/
        _hexout_9731 = 1;
        goto L6; // [433] 1218

        /** 	    		case 'B' then*/
        case 66:

        /** 	    			binout = 1*/
        _binout_9732 = 1;
        goto L6; // [446] 1218

        /** 	    		case 'c', '<', '>' then*/
        case 99:
        case 60:
        case 62:

        /** 	    			align = tch*/
        _align_9718 = _tch_9714;
        goto L6; // [463] 1218

        /** 	    		case '+' then*/
        case 43:

        /** 	    			psign = 1*/
        _psign_9719 = 1;
        goto L6; // [476] 1218

        /** 	    		case '(' then*/
        case 40:

        /** 	    			msign = 1*/
        _msign_9720 = 1;
        goto L6; // [489] 1218

        /** 	    		case '?' then*/
        case 63:

        /** 	    			alt = 1*/
        _alt_9724 = 1;
        goto L6; // [502] 1218

        /** 	    		case 'T' then*/
        case 84:

        /** 	    			istext = 1*/
        _istext_9734 = 1;
        goto L6; // [515] 1218

        /** 	    		case ':' then*/
        case 58:

        /** 	    			while i < length(format_pattern) do*/
L8: 
        _5484 = 6;
        if (_i_9715 >= 6)
        goto L6; // [529] 1218

        /** 	    				i += 1*/
        _i_9715 = _i_9715 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _tch_9714 = (int)*(((s1_ptr)_2)->base + _i_9715);

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_9727 = find_from(_tch_9714, _977, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_9727 != 0)
        goto L9; // [560] 577

        /** 	    					i -= 1*/
        _i_9715 = _i_9715 - 1;

        /** 	    					exit*/
        goto L6; // [574] 1218
L9: 

        /** 	    				width = width * 10 + pos - 1*/
        if (_width_9725 == (short)_width_9725)
        _5491 = _width_9725 * 10;
        else
        _5491 = NewDouble(_width_9725 * (double)10);
        if (IS_ATOM_INT(_5491)) {
            _5492 = _5491 + _pos_9727;
            if ((long)((unsigned long)_5492 + (unsigned long)HIGH_BITS) >= 0) 
            _5492 = NewDouble((double)_5492);
        }
        else {
            _5492 = NewDouble(DBL_PTR(_5491)->dbl + (double)_pos_9727);
        }
        DeRef(_5491);
        _5491 = NOVALUE;
        if (IS_ATOM_INT(_5492)) {
            _width_9725 = _5492 - 1;
        }
        else {
            _width_9725 = NewDouble(DBL_PTR(_5492)->dbl - (double)1);
        }
        DeRef(_5492);
        _5492 = NOVALUE;
        if (!IS_ATOM_INT(_width_9725)) {
            _1 = (long)(DBL_PTR(_width_9725)->dbl);
            if (UNIQUE(DBL_PTR(_width_9725)) && (DBL_PTR(_width_9725)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_width_9725);
            _width_9725 = _1;
        }

        /** 	    				if width = 0 then*/
        if (_width_9725 != 0)
        goto L8; // [599] 526

        /** 	    					zfill = '0'*/
        _zfill_9721 = 48;

        /** 	    			end while*/
        goto L8; // [613] 526
        goto L6; // [616] 1218

        /** 	    		case '.' then*/
        case 46:

        /** 	    			decs = 0*/
        _decs_9726 = 0;

        /** 	    			while i < length(format_pattern) do*/
LA: 
        _5495 = 6;
        if (_i_9715 >= 6)
        goto L6; // [637] 1218

        /** 	    				i += 1*/
        _i_9715 = _i_9715 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _tch_9714 = (int)*(((s1_ptr)_2)->base + _i_9715);

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_9727 = find_from(_tch_9714, _977, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_9727 != 0)
        goto LB; // [668] 685

        /** 	    					i -= 1*/
        _i_9715 = _i_9715 - 1;

        /** 	    					exit*/
        goto L6; // [682] 1218
LB: 

        /** 	    				decs = decs * 10 + pos - 1*/
        if (_decs_9726 == (short)_decs_9726)
        _5502 = _decs_9726 * 10;
        else
        _5502 = NewDouble(_decs_9726 * (double)10);
        if (IS_ATOM_INT(_5502)) {
            _5503 = _5502 + _pos_9727;
            if ((long)((unsigned long)_5503 + (unsigned long)HIGH_BITS) >= 0) 
            _5503 = NewDouble((double)_5503);
        }
        else {
            _5503 = NewDouble(DBL_PTR(_5502)->dbl + (double)_pos_9727);
        }
        DeRef(_5502);
        _5502 = NOVALUE;
        if (IS_ATOM_INT(_5503)) {
            _decs_9726 = _5503 - 1;
        }
        else {
            _decs_9726 = NewDouble(DBL_PTR(_5503)->dbl - (double)1);
        }
        DeRef(_5503);
        _5503 = NOVALUE;
        if (!IS_ATOM_INT(_decs_9726)) {
            _1 = (long)(DBL_PTR(_decs_9726)->dbl);
            if (UNIQUE(DBL_PTR(_decs_9726)) && (DBL_PTR(_decs_9726)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_decs_9726);
            _decs_9726 = _1;
        }

        /** 	    			end while*/
        goto LA; // [705] 634
        goto L6; // [708] 1218

        /** 	    		case '{' then*/
        case 123:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_9814 = _i_9715 + 1;

        /** 	    			i = sp*/
        _i_9715 = _sp_9814;

        /** 	    			while i < length(format_pattern) do*/
LC: 
        _5506 = 6;
        if (_i_9715 >= 6)
        goto LD; // [739] 786

        /** 	    				if format_pattern[i] = '}' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _5508 = (int)*(((s1_ptr)_2)->base + _i_9715);
        if (_5508 != 125)
        goto LE; // [749] 758

        /** 	    					exit*/
        goto LD; // [755] 786
LE: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _5510 = (int)*(((s1_ptr)_2)->base + _i_9715);
        if (_5510 != 93)
        goto LF; // [764] 773

        /** 	    					exit*/
        goto LD; // [770] 786
LF: 

        /** 	    				i += 1*/
        _i_9715 = _i_9715 + 1;

        /** 	    			end while*/
        goto LC; // [783] 736
LD: 

        /** 	    			idname = trim(format_pattern[sp .. i-1]) & '='*/
        _5513 = _i_9715 - 1;
        rhs_slice_target = (object_ptr)&_5514;
        RHS_Slice(_format_pattern_9710, _sp_9814, _5513);
        RefDS(_4282);
        _5515 = _6trim(_5514, _4282, 0);
        _5514 = NOVALUE;
        if (IS_SEQUENCE(_5515) && IS_ATOM(61)) {
            Append(&_idname_9737, _5515, 61);
        }
        else if (IS_ATOM(_5515) && IS_SEQUENCE(61)) {
        }
        else {
            Concat((object_ptr)&_idname_9737, _5515, 61);
            DeRef(_5515);
            _5515 = NOVALUE;
        }
        DeRef(_5515);
        _5515 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _5517 = (int)*(((s1_ptr)_2)->base + _i_9715);
        if (_5517 != 93)
        goto L10; // [813] 826

        /**     					i -= 1*/
        _i_9715 = _i_9715 - 1;
L10: 

        /**     				for j = 1 to length(arg_list) do*/
        if (IS_SEQUENCE(_arg_list_9711)){
                _5520 = SEQ_PTR(_arg_list_9711)->length;
        }
        else {
            _5520 = 1;
        }
        {
            int _j_9835;
            _j_9835 = 1;
L11: 
            if (_j_9835 > _5520){
                goto L12; // [831] 917
            }

            /**     					if sequence(arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_9711);
            _5521 = (int)*(((s1_ptr)_2)->base + _j_9835);
            _5522 = IS_SEQUENCE(_5521);
            _5521 = NOVALUE;
            if (_5522 == 0)
            {
                _5522 = NOVALUE;
                goto L13; // [847] 886
            }
            else{
                _5522 = NOVALUE;
            }

            /**     						if search:begins(idname, arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_9711);
            _5523 = (int)*(((s1_ptr)_2)->base + _j_9835);
            RefDS(_idname_9737);
            Ref(_5523);
            _5524 = _5begins(_idname_9737, _5523);
            _5523 = NOVALUE;
            if (_5524 == 0) {
                DeRef(_5524);
                _5524 = NOVALUE;
                goto L14; // [861] 885
            }
            else {
                if (!IS_ATOM_INT(_5524) && DBL_PTR(_5524)->dbl == 0.0){
                    DeRef(_5524);
                    _5524 = NOVALUE;
                    goto L14; // [861] 885
                }
                DeRef(_5524);
                _5524 = NOVALUE;
            }
            DeRef(_5524);
            _5524 = NOVALUE;

            /**     							if argn = 0 then*/
            if (_argn_9728 != 0)
            goto L15; // [868] 884

            /**     								argn = j*/
            _argn_9728 = _j_9835;

            /**     								exit*/
            goto L12; // [881] 917
L15: 
L14: 
L13: 

            /**     					if j = length(arg_list) then*/
            if (IS_SEQUENCE(_arg_list_9711)){
                    _5526 = SEQ_PTR(_arg_list_9711)->length;
            }
            else {
                _5526 = 1;
            }
            if (_j_9835 != _5526)
            goto L16; // [891] 910

            /**     						idname = ""*/
            RefDS(_5);
            DeRef(_idname_9737);
            _idname_9737 = _5;

            /**     						argn = -1*/
            _argn_9728 = -1;
L16: 

            /**     				end for*/
            _j_9835 = _j_9835 + 1;
            goto L11; // [912] 838
L12: 
            ;
        }
        goto L6; // [919] 1218

        /** 	    		case '%' then*/
        case 37:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_9849 = _i_9715 + 1;

        /** 	    			i = sp*/
        _i_9715 = _sp_9849;

        /** 	    			while i < length(format_pattern) do*/
L17: 
        _5529 = 6;
        if (_i_9715 >= 6)
        goto L18; // [950] 997

        /** 	    				if format_pattern[i] = '%' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _5531 = (int)*(((s1_ptr)_2)->base + _i_9715);
        if (_5531 != 37)
        goto L19; // [960] 969

        /** 	    					exit*/
        goto L18; // [966] 997
L19: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _5533 = (int)*(((s1_ptr)_2)->base + _i_9715);
        if (_5533 != 93)
        goto L1A; // [975] 984

        /** 	    					exit*/
        goto L18; // [981] 997
L1A: 

        /** 	    				i += 1*/
        _i_9715 = _i_9715 + 1;

        /** 	    			end while*/
        goto L17; // [994] 947
L18: 

        /** 	    			envsym = trim(format_pattern[sp .. i-1])*/
        _5536 = _i_9715 - 1;
        rhs_slice_target = (object_ptr)&_5537;
        RHS_Slice(_format_pattern_9710, _sp_9849, _5536);
        RefDS(_4282);
        _0 = _envsym_9738;
        _envsym_9738 = _6trim(_5537, _4282, 0);
        DeRef(_0);
        _5537 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _5539 = (int)*(((s1_ptr)_2)->base + _i_9715);
        if (_5539 != 93)
        goto L1B; // [1020] 1033

        /**     					i -= 1*/
        _i_9715 = _i_9715 - 1;
L1B: 

        /**     				envvar = getenv(envsym)*/
        DeRefi(_envvar_9739);
        _envvar_9739 = EGetEnv(_envsym_9738);

        /**     				argn = -1*/
        _argn_9728 = -1;

        /**     				if atom(envvar) then*/
        _5543 = IS_ATOM(_envvar_9739);
        if (_5543 == 0)
        {
            _5543 = NOVALUE;
            goto L1C; // [1050] 1059
        }
        else{
            _5543 = NOVALUE;
        }

        /**     					envvar = ""*/
        RefDS(_5);
        DeRefi(_envvar_9739);
        _envvar_9739 = _5;
L1C: 
        goto L6; // [1061] 1218

        /** 	    		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' then*/
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:

        /** 	    			if argn = 0 then*/
        if (_argn_9728 != 0)
        goto L6; // [1087] 1218

        /** 		    			i -= 1*/
        _i_9715 = _i_9715 - 1;

        /** 		    			while i < length(format_pattern) do*/
L1D: 
        _5550 = 6;
        if (_i_9715 >= 6)
        goto L6; // [1107] 1218

        /** 		    				i += 1*/
        _i_9715 = _i_9715 + 1;

        /** 		    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _tch_9714 = (int)*(((s1_ptr)_2)->base + _i_9715);

        /** 		    				pos = find(tch, "0123456789")*/
        _pos_9727 = find_from(_tch_9714, _977, 1);

        /** 		    				if pos = 0 then*/
        if (_pos_9727 != 0)
        goto L1E; // [1138] 1155

        /** 		    					i -= 1*/
        _i_9715 = _i_9715 - 1;

        /** 		    					exit*/
        goto L6; // [1152] 1218
L1E: 

        /** 		    				argn = argn * 10 + pos - 1*/
        if (_argn_9728 == (short)_argn_9728)
        _5557 = _argn_9728 * 10;
        else
        _5557 = NewDouble(_argn_9728 * (double)10);
        if (IS_ATOM_INT(_5557)) {
            _5558 = _5557 + _pos_9727;
            if ((long)((unsigned long)_5558 + (unsigned long)HIGH_BITS) >= 0) 
            _5558 = NewDouble((double)_5558);
        }
        else {
            _5558 = NewDouble(DBL_PTR(_5557)->dbl + (double)_pos_9727);
        }
        DeRef(_5557);
        _5557 = NOVALUE;
        if (IS_ATOM_INT(_5558)) {
            _argn_9728 = _5558 - 1;
        }
        else {
            _argn_9728 = NewDouble(DBL_PTR(_5558)->dbl - (double)1);
        }
        DeRef(_5558);
        _5558 = NOVALUE;
        if (!IS_ATOM_INT(_argn_9728)) {
            _1 = (long)(DBL_PTR(_argn_9728)->dbl);
            if (UNIQUE(DBL_PTR(_argn_9728)) && (DBL_PTR(_argn_9728)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_argn_9728);
            _argn_9728 = _1;
        }

        /** 		    			end while*/
        goto L1D; // [1175] 1104
        goto L6; // [1179] 1218

        /** 	    		case ',' then*/
        case 44:

        /** 	    			if i < length(format_pattern) then*/
        _5560 = 6;
        if (_i_9715 >= 6)
        goto L6; // [1190] 1218

        /** 	    				i +=1*/
        _i_9715 = _i_9715 + 1;

        /** 	    				tsep = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9710);
        _tsep_9733 = (int)*(((s1_ptr)_2)->base + _i_9715);
        goto L6; // [1211] 1218

        /** 	    		case else*/
        default:
    ;}L6: 

    /**     		if tend > 0 then*/
    if (_tend_9716 <= 0)
    goto L1F; // [1220] 3783

    /**     			sequence argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_9900);
    _argtext_9900 = _5;

    /**     			if argn = 0 then*/
    if (_argn_9728 != 0)
    goto L20; // [1235] 1248

    /**     				argn = argl + 1*/
    _argn_9728 = _argl_9729 + 1;
L20: 

    /**     			argl = argn*/
    _argl_9729 = _argn_9728;

    /**     			if argn < 1 or argn > length(arg_list) then*/
    _5567 = (_argn_9728 < 1);
    if (_5567 != 0) {
        goto L21; // [1261] 1277
    }
    if (IS_SEQUENCE(_arg_list_9711)){
            _5569 = SEQ_PTR(_arg_list_9711)->length;
    }
    else {
        _5569 = 1;
    }
    _5570 = (_argn_9728 > _5569);
    _5569 = NOVALUE;
    if (_5570 == 0)
    {
        DeRef(_5570);
        _5570 = NOVALUE;
        goto L22; // [1273] 1319
    }
    else{
        DeRef(_5570);
        _5570 = NOVALUE;
    }
L21: 

    /**     				if length(envvar) > 0 then*/
    if (IS_SEQUENCE(_envvar_9739)){
            _5571 = SEQ_PTR(_envvar_9739)->length;
    }
    else {
        _5571 = 1;
    }
    if (_5571 <= 0)
    goto L23; // [1284] 1303

    /**     					argtext = envvar*/
    Ref(_envvar_9739);
    DeRef(_argtext_9900);
    _argtext_9900 = _envvar_9739;

    /** 	    				currargv = envvar*/
    Ref(_envvar_9739);
    DeRef(_currargv_9736);
    _currargv_9736 = _envvar_9739;
    goto L24; // [1300] 2780
L23: 

    /**     					argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_9900);
    _argtext_9900 = _5;

    /** 	    				currargv =""*/
    RefDS(_5);
    DeRef(_currargv_9736);
    _currargv_9736 = _5;
    goto L24; // [1316] 2780
L22: 

    /** 					if string(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5573 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    Ref(_5573);
    _5574 = _3string(_5573);
    _5573 = NOVALUE;
    if (_5574 == 0) {
        DeRef(_5574);
        _5574 = NOVALUE;
        goto L25; // [1329] 1379
    }
    else {
        if (!IS_ATOM_INT(_5574) && DBL_PTR(_5574)->dbl == 0.0){
            DeRef(_5574);
            _5574 = NOVALUE;
            goto L25; // [1329] 1379
        }
        DeRef(_5574);
        _5574 = NOVALUE;
    }
    DeRef(_5574);
    _5574 = NOVALUE;

    /** 						if length(idname) > 0 then*/
    if (IS_SEQUENCE(_idname_9737)){
            _5575 = SEQ_PTR(_idname_9737)->length;
    }
    else {
        _5575 = 1;
    }
    if (_5575 <= 0)
    goto L26; // [1339] 1367

    /** 							argtext = arg_list[argn][length(idname) + 1 .. $]*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5577 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (IS_SEQUENCE(_idname_9737)){
            _5578 = SEQ_PTR(_idname_9737)->length;
    }
    else {
        _5578 = 1;
    }
    _5579 = _5578 + 1;
    _5578 = NOVALUE;
    if (IS_SEQUENCE(_5577)){
            _5580 = SEQ_PTR(_5577)->length;
    }
    else {
        _5580 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_5577, _5579, _5580);
    _5577 = NOVALUE;
    goto L27; // [1364] 2773
L26: 

    /** 							argtext = arg_list[argn]*/
    DeRef(_argtext_9900);
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _argtext_9900 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    Ref(_argtext_9900);
    goto L27; // [1376] 2773
L25: 

    /** 					elsif integer(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5583 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (IS_ATOM_INT(_5583))
    _5584 = 1;
    else if (IS_ATOM_DBL(_5583))
    _5584 = IS_ATOM_INT(DoubleToInt(_5583));
    else
    _5584 = 0;
    _5583 = NOVALUE;
    if (_5584 == 0)
    {
        _5584 = NOVALUE;
        goto L28; // [1388] 1937
    }
    else{
        _5584 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_9734 == 0)
    {
        goto L29; // [1395] 1419
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(arg_list[argn]))}*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5585 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    Ref(_5585);
    _5586 = _17abs(_5585);
    _5585 = NOVALUE;
    _5587 = binary_op(AND_BITS, _1895, _5586);
    DeRef(_5586);
    _5586 = NOVALUE;
    _0 = _argtext_9900;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5587;
    _argtext_9900 = MAKE_SEQ(_1);
    DeRef(_0);
    _5587 = NOVALUE;
    goto L27; // [1416] 2773
L29: 

    /** 						elsif bwz != 0 and arg_list[argn] = 0 then*/
    _5589 = (_bwz_9722 != 0);
    if (_5589 == 0) {
        goto L2A; // [1427] 1454
    }
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5591 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (IS_ATOM_INT(_5591)) {
        _5592 = (_5591 == 0);
    }
    else {
        _5592 = binary_op(EQUALS, _5591, 0);
    }
    _5591 = NOVALUE;
    if (_5592 == 0) {
        DeRef(_5592);
        _5592 = NOVALUE;
        goto L2A; // [1440] 1454
    }
    else {
        if (!IS_ATOM_INT(_5592) && DBL_PTR(_5592)->dbl == 0.0){
            DeRef(_5592);
            _5592 = NOVALUE;
            goto L2A; // [1440] 1454
        }
        DeRef(_5592);
        _5592 = NOVALUE;
    }
    DeRef(_5592);
    _5592 = NOVALUE;

    /** 							argtext = repeat(' ', width)*/
    DeRef(_argtext_9900);
    _argtext_9900 = Repeat(32, _width_9725);
    goto L27; // [1451] 2773
L2A: 

    /** 						elsif binout = 1 then*/
    if (_binout_9732 != 1)
    goto L2B; // [1458] 1601

    /** 							argtext = stdseq:reverse( convert:int_to_bits(arg_list[argn], 32)) + '0'*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5595 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    Ref(_5595);
    _5596 = _4int_to_bits(_5595, 32);
    _5595 = NOVALUE;
    _5597 = _20reverse(_5596, 1, 0);
    _5596 = NOVALUE;
    DeRef(_argtext_9900);
    if (IS_ATOM_INT(_5597)) {
        _argtext_9900 = _5597 + 48;
        if ((long)((unsigned long)_argtext_9900 + (unsigned long)HIGH_BITS) >= 0) 
        _argtext_9900 = NewDouble((double)_argtext_9900);
    }
    else {
        _argtext_9900 = binary_op(PLUS, _5597, 48);
    }
    DeRef(_5597);
    _5597 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5599 = (_zfill_9721 != 0);
    if (_5599 == 0) {
        goto L2C; // [1493] 1539
    }
    _5601 = (_width_9725 > 0);
    if (_5601 == 0)
    {
        DeRef(_5601);
        _5601 = NOVALUE;
        goto L2C; // [1504] 1539
    }
    else{
        DeRef(_5601);
        _5601 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5602 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5602 = 1;
    }
    if (_width_9725 <= _5602)
    goto L27; // [1514] 2773

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5604 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5604 = 1;
    }
    _5605 = _width_9725 - _5604;
    _5604 = NOVALUE;
    _5606 = Repeat(48, _5605);
    _5605 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _5606, _argtext_9900);
    DeRefDS(_5606);
    _5606 = NOVALUE;
    DeRef(_5606);
    _5606 = NOVALUE;
    goto L27; // [1536] 2773
L2C: 

    /** 								count = 1*/
    _count_9742 = 1;

    /** 								while count < length(argtext) and argtext[count] = '0' do*/
L2D: 
    if (IS_SEQUENCE(_argtext_9900)){
            _5608 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5608 = 1;
    }
    _5609 = (_count_9742 < _5608);
    _5608 = NOVALUE;
    if (_5609 == 0) {
        goto L2E; // [1558] 1587
    }
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5611 = (int)*(((s1_ptr)_2)->base + _count_9742);
    if (IS_ATOM_INT(_5611)) {
        _5612 = (_5611 == 48);
    }
    else {
        _5612 = binary_op(EQUALS, _5611, 48);
    }
    _5611 = NOVALUE;
    if (_5612 <= 0) {
        if (_5612 == 0) {
            DeRef(_5612);
            _5612 = NOVALUE;
            goto L2E; // [1571] 1587
        }
        else {
            if (!IS_ATOM_INT(_5612) && DBL_PTR(_5612)->dbl == 0.0){
                DeRef(_5612);
                _5612 = NOVALUE;
                goto L2E; // [1571] 1587
            }
            DeRef(_5612);
            _5612 = NOVALUE;
        }
    }
    DeRef(_5612);
    _5612 = NOVALUE;

    /** 									count += 1*/
    _count_9742 = _count_9742 + 1;

    /** 								end while*/
    goto L2D; // [1584] 1551
L2E: 

    /** 								argtext = argtext[count .. $]*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5614 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5614 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, _count_9742, _5614);
    goto L27; // [1598] 2773
L2B: 

    /** 						elsif hexout = 0 then*/
    if (_hexout_9731 != 0)
    goto L2F; // [1605] 1871

    /** 							argtext = sprintf("%d", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5617 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    DeRef(_argtext_9900);
    _argtext_9900 = EPrintf(-9999999, _1681, _5617);
    _5617 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5619 = (_zfill_9721 != 0);
    if (_5619 == 0) {
        goto L30; // [1627] 1724
    }
    _5621 = (_width_9725 > 0);
    if (_5621 == 0)
    {
        DeRef(_5621);
        _5621 = NOVALUE;
        goto L30; // [1638] 1724
    }
    else{
        DeRef(_5621);
        _5621 = NOVALUE;
    }

    /** 								if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5622 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5622, 45)){
        _5622 = NOVALUE;
        goto L31; // [1647] 1693
    }
    _5622 = NOVALUE;

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5624 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5624 = 1;
    }
    if (_width_9725 <= _5624)
    goto L32; // [1658] 1723

    /** 										argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5626 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5626 = 1;
    }
    _5627 = _width_9725 - _5626;
    _5626 = NOVALUE;
    _5628 = Repeat(48, _5627);
    _5627 = NOVALUE;
    if (IS_SEQUENCE(_argtext_9900)){
            _5629 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5629 = 1;
    }
    rhs_slice_target = (object_ptr)&_5630;
    RHS_Slice(_argtext_9900, 2, _5629);
    {
        int concat_list[3];

        concat_list[0] = _5630;
        concat_list[1] = _5628;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5630);
    _5630 = NOVALUE;
    DeRefDS(_5628);
    _5628 = NOVALUE;
    goto L32; // [1690] 1723
L31: 

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5632 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5632 = 1;
    }
    if (_width_9725 <= _5632)
    goto L33; // [1700] 1722

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5634 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5634 = 1;
    }
    _5635 = _width_9725 - _5634;
    _5634 = NOVALUE;
    _5636 = Repeat(48, _5635);
    _5635 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _5636, _argtext_9900);
    DeRefDS(_5636);
    _5636 = NOVALUE;
    DeRef(_5636);
    _5636 = NOVALUE;
L33: 
L32: 
L30: 

    /** 							if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5638 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (binary_op_a(LESSEQ, _5638, 0)){
        _5638 = NOVALUE;
        goto L34; // [1730] 1778
    }
    _5638 = NOVALUE;

    /** 								if psign then*/
    if (_psign_9719 == 0)
    {
        goto L27; // [1738] 2773
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_9721 != 0)
    goto L35; // [1743] 1756

    /** 										argtext = '+' & argtext*/
    Prepend(&_argtext_9900, _argtext_9900, 43);
    goto L27; // [1753] 2773
L35: 

    /** 									elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5642 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5642, 48)){
        _5642 = NOVALUE;
        goto L27; // [1762] 2773
    }
    _5642 = NOVALUE;

    /** 										argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_9900 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [1775] 2773
L34: 

    /** 							elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5644 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (binary_op_a(GREATEREQ, _5644, 0)){
        _5644 = NOVALUE;
        goto L27; // [1784] 2773
    }
    _5644 = NOVALUE;

    /** 								if msign then*/
    if (_msign_9720 == 0)
    {
        goto L27; // [1792] 2773
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_9721 != 0)
    goto L36; // [1797] 1820

    /** 										argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5647 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5647 = 1;
    }
    rhs_slice_target = (object_ptr)&_5648;
    RHS_Slice(_argtext_9900, 2, _5647);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5648;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5648);
    _5648 = NOVALUE;
    goto L27; // [1817] 2773
L36: 

    /** 										if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5651 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5651, 48)){
        _5651 = NOVALUE;
        goto L37; // [1826] 1849
    }
    _5651 = NOVALUE;

    /** 											argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5653 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5653 = 1;
    }
    rhs_slice_target = (object_ptr)&_5654;
    RHS_Slice(_argtext_9900, 3, _5653);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5654;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5654);
    _5654 = NOVALUE;
    goto L27; // [1846] 2773
L37: 

    /** 											argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5656 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5656 = 1;
    }
    rhs_slice_target = (object_ptr)&_5657;
    RHS_Slice(_argtext_9900, 2, _5656);
    Append(&_argtext_9900, _5657, 41);
    DeRefDS(_5657);
    _5657 = NOVALUE;
    goto L27; // [1868] 2773
L2F: 

    /** 							argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5660 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    DeRef(_argtext_9900);
    _argtext_9900 = EPrintf(-9999999, _5659, _5660);
    _5660 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5662 = (_zfill_9721 != 0);
    if (_5662 == 0) {
        goto L27; // [1889] 2773
    }
    _5664 = (_width_9725 > 0);
    if (_5664 == 0)
    {
        DeRef(_5664);
        _5664 = NOVALUE;
        goto L27; // [1900] 2773
    }
    else{
        DeRef(_5664);
        _5664 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5665 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5665 = 1;
    }
    if (_width_9725 <= _5665)
    goto L27; // [1910] 2773

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5667 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5667 = 1;
    }
    _5668 = _width_9725 - _5667;
    _5667 = NOVALUE;
    _5669 = Repeat(48, _5668);
    _5668 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _5669, _argtext_9900);
    DeRefDS(_5669);
    _5669 = NOVALUE;
    DeRef(_5669);
    _5669 = NOVALUE;
    goto L27; // [1934] 2773
L28: 

    /** 					elsif atom(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5671 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    _5672 = IS_ATOM(_5671);
    _5671 = NOVALUE;
    if (_5672 == 0)
    {
        _5672 = NOVALUE;
        goto L38; // [1946] 2351
    }
    else{
        _5672 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_9734 == 0)
    {
        goto L39; // [1953] 1980
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(arg_list[argn])))}*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5673 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (IS_ATOM_INT(_5673))
    _5674 = e_floor(_5673);
    else
    _5674 = unary_op(FLOOR, _5673);
    _5673 = NOVALUE;
    _5675 = _17abs(_5674);
    _5674 = NOVALUE;
    _5676 = binary_op(AND_BITS, _1895, _5675);
    DeRef(_5675);
    _5675 = NOVALUE;
    _0 = _argtext_9900;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5676;
    _argtext_9900 = MAKE_SEQ(_1);
    DeRef(_0);
    _5676 = NOVALUE;
    goto L27; // [1977] 2773
L39: 

    /** 							if hexout then*/
    if (_hexout_9731 == 0)
    {
        goto L3A; // [1984] 2052
    }
    else{
    }

    /** 								argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5678 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    DeRef(_argtext_9900);
    _argtext_9900 = EPrintf(-9999999, _5659, _5678);
    _5678 = NOVALUE;

    /** 								if zfill != 0 and width > 0 then*/
    _5680 = (_zfill_9721 != 0);
    if (_5680 == 0) {
        goto L27; // [2005] 2773
    }
    _5682 = (_width_9725 > 0);
    if (_5682 == 0)
    {
        DeRef(_5682);
        _5682 = NOVALUE;
        goto L27; // [2016] 2773
    }
    else{
        DeRef(_5682);
        _5682 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5683 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5683 = 1;
    }
    if (_width_9725 <= _5683)
    goto L27; // [2026] 2773

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5685 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5685 = 1;
    }
    _5686 = _width_9725 - _5685;
    _5685 = NOVALUE;
    _5687 = Repeat(48, _5686);
    _5686 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _5687, _argtext_9900);
    DeRefDS(_5687);
    _5687 = NOVALUE;
    DeRef(_5687);
    _5687 = NOVALUE;
    goto L27; // [2049] 2773
L3A: 

    /** 								argtext = trim(sprintf("%15.15g", arg_list[argn]))*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5690 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    _5691 = EPrintf(-9999999, _5689, _5690);
    _5690 = NOVALUE;
    RefDS(_4282);
    _0 = _argtext_9900;
    _argtext_9900 = _6trim(_5691, _4282, 0);
    DeRef(_0);
    _5691 = NOVALUE;

    /** 								while ep != 0 with entry do*/
    goto L3B; // [2072] 2095
L3C: 
    if (_ep_9740 == 0)
    goto L3D; // [2077] 2109

    /** 									argtext = remove(argtext, ep+2)*/
    _5694 = _ep_9740 + 2;
    if ((long)((unsigned long)_5694 + (unsigned long)HIGH_BITS) >= 0) 
    _5694 = NewDouble((double)_5694);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_9900);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5694)) ? _5694 : (long)(DBL_PTR(_5694)->dbl);
        int stop = (IS_ATOM_INT(_5694)) ? _5694 : (long)(DBL_PTR(_5694)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_9900), start, &_argtext_9900 );
            }
            else Tail(SEQ_PTR(_argtext_9900), stop+1, &_argtext_9900);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_9900), start, &_argtext_9900);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_9900 = Remove_elements(start, stop, (SEQ_PTR(_argtext_9900)->ref == 1));
        }
    }
    DeRef(_5694);
    _5694 = NOVALUE;
    _5694 = NOVALUE;

    /** 								entry*/
L3B: 

    /** 									ep = match("e+0", argtext)*/
    _ep_9740 = e_match_from(_5696, _argtext_9900, 1);

    /** 								end while*/
    goto L3C; // [2106] 2075
L3D: 

    /** 								if zfill != 0 and width > 0 then*/
    _5698 = (_zfill_9721 != 0);
    if (_5698 == 0) {
        goto L3E; // [2117] 2202
    }
    _5700 = (_width_9725 > 0);
    if (_5700 == 0)
    {
        DeRef(_5700);
        _5700 = NOVALUE;
        goto L3E; // [2128] 2202
    }
    else{
        DeRef(_5700);
        _5700 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5701 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5701 = 1;
    }
    if (_width_9725 <= _5701)
    goto L3F; // [2138] 2201

    /** 										if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5703 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5703, 45)){
        _5703 = NOVALUE;
        goto L40; // [2148] 2182
    }
    _5703 = NOVALUE;

    /** 											argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5705 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5705 = 1;
    }
    _5706 = _width_9725 - _5705;
    _5705 = NOVALUE;
    _5707 = Repeat(48, _5706);
    _5706 = NOVALUE;
    if (IS_SEQUENCE(_argtext_9900)){
            _5708 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5708 = 1;
    }
    rhs_slice_target = (object_ptr)&_5709;
    RHS_Slice(_argtext_9900, 2, _5708);
    {
        int concat_list[3];

        concat_list[0] = _5709;
        concat_list[1] = _5707;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5709);
    _5709 = NOVALUE;
    DeRefDS(_5707);
    _5707 = NOVALUE;
    goto L41; // [2179] 2200
L40: 

    /** 											argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5711 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5711 = 1;
    }
    _5712 = _width_9725 - _5711;
    _5711 = NOVALUE;
    _5713 = Repeat(48, _5712);
    _5712 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _5713, _argtext_9900);
    DeRefDS(_5713);
    _5713 = NOVALUE;
    DeRef(_5713);
    _5713 = NOVALUE;
L41: 
L3F: 
L3E: 

    /** 								if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5715 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (binary_op_a(LESSEQ, _5715, 0)){
        _5715 = NOVALUE;
        goto L42; // [2208] 2256
    }
    _5715 = NOVALUE;

    /** 									if psign  then*/
    if (_psign_9719 == 0)
    {
        goto L27; // [2216] 2773
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_9721 != 0)
    goto L43; // [2221] 2234

    /** 											argtext = '+' & argtext*/
    Prepend(&_argtext_9900, _argtext_9900, 43);
    goto L27; // [2231] 2773
L43: 

    /** 										elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5719 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5719, 48)){
        _5719 = NOVALUE;
        goto L27; // [2240] 2773
    }
    _5719 = NOVALUE;

    /** 											argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_9900 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [2253] 2773
L42: 

    /** 								elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5721 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (binary_op_a(GREATEREQ, _5721, 0)){
        _5721 = NOVALUE;
        goto L27; // [2262] 2773
    }
    _5721 = NOVALUE;

    /** 									if msign then*/
    if (_msign_9720 == 0)
    {
        goto L27; // [2270] 2773
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_9721 != 0)
    goto L44; // [2275] 2298

    /** 											argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5724 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5724 = 1;
    }
    rhs_slice_target = (object_ptr)&_5725;
    RHS_Slice(_argtext_9900, 2, _5724);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5725;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5725);
    _5725 = NOVALUE;
    goto L27; // [2295] 2773
L44: 

    /** 											if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5727 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5727, 48)){
        _5727 = NOVALUE;
        goto L45; // [2304] 2327
    }
    _5727 = NOVALUE;

    /** 												argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5729 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5729 = 1;
    }
    rhs_slice_target = (object_ptr)&_5730;
    RHS_Slice(_argtext_9900, 3, _5729);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5730;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5730);
    _5730 = NOVALUE;
    goto L27; // [2324] 2773
L45: 

    /** 												argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5732 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5732 = 1;
    }
    rhs_slice_target = (object_ptr)&_5733;
    RHS_Slice(_argtext_9900, 2, _5732);
    Append(&_argtext_9900, _5733, 41);
    DeRefDS(_5733);
    _5733 = NOVALUE;
    goto L27; // [2348] 2773
L38: 

    /** 						if alt != 0 and length(arg_list[argn]) = 2 then*/
    _5735 = (_alt_9724 != 0);
    if (_5735 == 0) {
        goto L46; // [2359] 2680
    }
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5737 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    if (IS_SEQUENCE(_5737)){
            _5738 = SEQ_PTR(_5737)->length;
    }
    else {
        _5738 = 1;
    }
    _5737 = NOVALUE;
    _5739 = (_5738 == 2);
    _5738 = NOVALUE;
    if (_5739 == 0)
    {
        DeRef(_5739);
        _5739 = NOVALUE;
        goto L46; // [2375] 2680
    }
    else{
        DeRef(_5739);
        _5739 = NOVALUE;
    }

    /** 							object tempv*/

    /** 							if atom(prevargv) then*/
    _5740 = IS_ATOM(_prevargv_9735);
    if (_5740 == 0)
    {
        _5740 = NOVALUE;
        goto L47; // [2385] 2421
    }
    else{
        _5740 = NOVALUE;
    }

    /** 								if prevargv != 1 then*/
    if (binary_op_a(EQUALS, _prevargv_9735, 1)){
        goto L48; // [2390] 2407
    }

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5742 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    DeRef(_tempv_10137);
    _2 = (int)SEQ_PTR(_5742);
    _tempv_10137 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_10137);
    _5742 = NOVALUE;
    goto L49; // [2404] 2455
L48: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5744 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    DeRef(_tempv_10137);
    _2 = (int)SEQ_PTR(_5744);
    _tempv_10137 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_10137);
    _5744 = NOVALUE;
    goto L49; // [2418] 2455
L47: 

    /** 								if length(prevargv) = 0 then*/
    if (IS_SEQUENCE(_prevargv_9735)){
            _5746 = SEQ_PTR(_prevargv_9735)->length;
    }
    else {
        _5746 = 1;
    }
    if (_5746 != 0)
    goto L4A; // [2426] 2443

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5748 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    DeRef(_tempv_10137);
    _2 = (int)SEQ_PTR(_5748);
    _tempv_10137 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_10137);
    _5748 = NOVALUE;
    goto L4B; // [2440] 2454
L4A: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5750 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    DeRef(_tempv_10137);
    _2 = (int)SEQ_PTR(_5750);
    _tempv_10137 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_10137);
    _5750 = NOVALUE;
L4B: 
L49: 

    /** 							if string(tempv) then*/
    Ref(_tempv_10137);
    _5752 = _3string(_tempv_10137);
    if (_5752 == 0) {
        DeRef(_5752);
        _5752 = NOVALUE;
        goto L4C; // [2463] 2476
    }
    else {
        if (!IS_ATOM_INT(_5752) && DBL_PTR(_5752)->dbl == 0.0){
            DeRef(_5752);
            _5752 = NOVALUE;
            goto L4C; // [2463] 2476
        }
        DeRef(_5752);
        _5752 = NOVALUE;
    }
    DeRef(_5752);
    _5752 = NOVALUE;

    /** 								argtext = tempv*/
    Ref(_tempv_10137);
    DeRef(_argtext_9900);
    _argtext_9900 = _tempv_10137;
    goto L4D; // [2473] 2675
L4C: 

    /** 							elsif integer(tempv) then*/
    if (IS_ATOM_INT(_tempv_10137))
    _5753 = 1;
    else if (IS_ATOM_DBL(_tempv_10137))
    _5753 = IS_ATOM_INT(DoubleToInt(_tempv_10137));
    else
    _5753 = 0;
    if (_5753 == 0)
    {
        _5753 = NOVALUE;
        goto L4E; // [2481] 2547
    }
    else{
        _5753 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_9734 == 0)
    {
        goto L4F; // [2486] 2506
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(tempv))}*/
    Ref(_tempv_10137);
    _5754 = _17abs(_tempv_10137);
    _5755 = binary_op(AND_BITS, _1895, _5754);
    DeRef(_5754);
    _5754 = NOVALUE;
    _0 = _argtext_9900;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5755;
    _argtext_9900 = MAKE_SEQ(_1);
    DeRef(_0);
    _5755 = NOVALUE;
    goto L4D; // [2503] 2675
L4F: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5757 = (_bwz_9722 != 0);
    if (_5757 == 0) {
        goto L50; // [2514] 2537
    }
    if (IS_ATOM_INT(_tempv_10137)) {
        _5759 = (_tempv_10137 == 0);
    }
    else {
        _5759 = binary_op(EQUALS, _tempv_10137, 0);
    }
    if (_5759 == 0) {
        DeRef(_5759);
        _5759 = NOVALUE;
        goto L50; // [2523] 2537
    }
    else {
        if (!IS_ATOM_INT(_5759) && DBL_PTR(_5759)->dbl == 0.0){
            DeRef(_5759);
            _5759 = NOVALUE;
            goto L50; // [2523] 2537
        }
        DeRef(_5759);
        _5759 = NOVALUE;
    }
    DeRef(_5759);
    _5759 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_9900);
    _argtext_9900 = Repeat(32, _width_9725);
    goto L4D; // [2534] 2675
L50: 

    /** 									argtext = sprintf("%d", tempv)*/
    DeRef(_argtext_9900);
    _argtext_9900 = EPrintf(-9999999, _1681, _tempv_10137);
    goto L4D; // [2544] 2675
L4E: 

    /** 							elsif atom(tempv) then*/
    _5762 = IS_ATOM(_tempv_10137);
    if (_5762 == 0)
    {
        _5762 = NOVALUE;
        goto L51; // [2552] 2629
    }
    else{
        _5762 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_9734 == 0)
    {
        goto L52; // [2557] 2580
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(tempv)))}*/
    if (IS_ATOM_INT(_tempv_10137))
    _5763 = e_floor(_tempv_10137);
    else
    _5763 = unary_op(FLOOR, _tempv_10137);
    _5764 = _17abs(_5763);
    _5763 = NOVALUE;
    _5765 = binary_op(AND_BITS, _1895, _5764);
    DeRef(_5764);
    _5764 = NOVALUE;
    _0 = _argtext_9900;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5765;
    _argtext_9900 = MAKE_SEQ(_1);
    DeRef(_0);
    _5765 = NOVALUE;
    goto L4D; // [2577] 2675
L52: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5767 = (_bwz_9722 != 0);
    if (_5767 == 0) {
        goto L53; // [2588] 2611
    }
    if (IS_ATOM_INT(_tempv_10137)) {
        _5769 = (_tempv_10137 == 0);
    }
    else {
        _5769 = binary_op(EQUALS, _tempv_10137, 0);
    }
    if (_5769 == 0) {
        DeRef(_5769);
        _5769 = NOVALUE;
        goto L53; // [2597] 2611
    }
    else {
        if (!IS_ATOM_INT(_5769) && DBL_PTR(_5769)->dbl == 0.0){
            DeRef(_5769);
            _5769 = NOVALUE;
            goto L53; // [2597] 2611
        }
        DeRef(_5769);
        _5769 = NOVALUE;
    }
    DeRef(_5769);
    _5769 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_9900);
    _argtext_9900 = Repeat(32, _width_9725);
    goto L4D; // [2608] 2675
L53: 

    /** 									argtext = trim(sprintf("%15.15g", tempv))*/
    _5771 = EPrintf(-9999999, _5689, _tempv_10137);
    RefDS(_4282);
    _0 = _argtext_9900;
    _argtext_9900 = _6trim(_5771, _4282, 0);
    DeRef(_0);
    _5771 = NOVALUE;
    goto L4D; // [2626] 2675
L51: 

    /** 								argtext = pretty:pretty_sprint( tempv,*/
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_1681);
    *((int *)(_2+20)) = _1681;
    RefDS(_5773);
    *((int *)(_2+24)) = _5773;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5774 = MAKE_SEQ(_1);
    DeRef(_options_inlined_pretty_sprint_at_2646_10191);
    _options_inlined_pretty_sprint_at_2646_10191 = _5774;
    _5774 = NOVALUE;

    /** 	pretty_printing = 0*/
    _23pretty_printing_8147 = 0;

    /** 	pretty( x, options )*/
    Ref(_tempv_10137);
    RefDS(_options_inlined_pretty_sprint_at_2646_10191);
    _23pretty(_tempv_10137, _options_inlined_pretty_sprint_at_2646_10191);

    /** 	return pretty_line*/
    RefDS(_23pretty_line_8150);
    DeRef(_argtext_9900);
    _argtext_9900 = _23pretty_line_8150;
    DeRef(_options_inlined_pretty_sprint_at_2646_10191);
    _options_inlined_pretty_sprint_at_2646_10191 = NOVALUE;
L4D: 
    DeRef(_tempv_10137);
    _tempv_10137 = NOVALUE;
    goto L54; // [2677] 2758
L46: 

    /** 							argtext = pretty:pretty_sprint( arg_list[argn],*/
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _5775 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_1681);
    *((int *)(_2+20)) = _1681;
    RefDS(_5773);
    *((int *)(_2+24)) = _5773;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5776 = MAKE_SEQ(_1);
    Ref(_5775);
    DeRef(_x_inlined_pretty_sprint_at_2701_10197);
    _x_inlined_pretty_sprint_at_2701_10197 = _5775;
    _5775 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2704_10198);
    _options_inlined_pretty_sprint_at_2704_10198 = _5776;
    _5776 = NOVALUE;

    /** 	pretty_printing = 0*/
    _23pretty_printing_8147 = 0;

    /** 	pretty( x, options )*/
    Ref(_x_inlined_pretty_sprint_at_2701_10197);
    RefDS(_options_inlined_pretty_sprint_at_2704_10198);
    _23pretty(_x_inlined_pretty_sprint_at_2701_10197, _options_inlined_pretty_sprint_at_2704_10198);

    /** 	return pretty_line*/
    RefDS(_23pretty_line_8150);
    DeRef(_argtext_9900);
    _argtext_9900 = _23pretty_line_8150;
    DeRef(_x_inlined_pretty_sprint_at_2701_10197);
    _x_inlined_pretty_sprint_at_2701_10197 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2704_10198);
    _options_inlined_pretty_sprint_at_2704_10198 = NOVALUE;

    /** 						while ep != 0 with entry do*/
    goto L54; // [2735] 2758
L55: 
    if (_ep_9740 == 0)
    goto L56; // [2740] 2772

    /** 							argtext = remove(argtext, ep+2)*/
    _5778 = _ep_9740 + 2;
    if ((long)((unsigned long)_5778 + (unsigned long)HIGH_BITS) >= 0) 
    _5778 = NewDouble((double)_5778);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_9900);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5778)) ? _5778 : (long)(DBL_PTR(_5778)->dbl);
        int stop = (IS_ATOM_INT(_5778)) ? _5778 : (long)(DBL_PTR(_5778)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_9900), start, &_argtext_9900 );
            }
            else Tail(SEQ_PTR(_argtext_9900), stop+1, &_argtext_9900);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_9900), start, &_argtext_9900);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_9900 = Remove_elements(start, stop, (SEQ_PTR(_argtext_9900)->ref == 1));
        }
    }
    DeRef(_5778);
    _5778 = NOVALUE;
    _5778 = NOVALUE;

    /** 						entry*/
L54: 

    /** 							ep = match("e+0", argtext)*/
    _ep_9740 = e_match_from(_5696, _argtext_9900, 1);

    /** 						end while*/
    goto L55; // [2769] 2738
L56: 
L27: 

    /** 	    			currargv = arg_list[argn]*/
    DeRef(_currargv_9736);
    _2 = (int)SEQ_PTR(_arg_list_9711);
    _currargv_9736 = (int)*(((s1_ptr)_2)->base + _argn_9728);
    Ref(_currargv_9736);
L24: 

    /**     			if length(argtext) > 0 then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5782 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5782 = 1;
    }
    if (_5782 <= 0)
    goto L57; // [2785] 3737

    /**     				switch cap do*/
    _0 = _cap_9717;
    switch ( _0 ){ 

        /**     					case 'u' then*/
        case 117:

        /**     						argtext = upper(argtext)*/
        RefDS(_argtext_9900);
        _0 = _argtext_9900;
        _argtext_9900 = _6upper(_argtext_9900);
        DeRefDS(_0);
        goto L58; // [2810] 2878

        /**     					case 'l' then*/
        case 108:

        /**     						argtext = lower(argtext)*/
        RefDS(_argtext_9900);
        _0 = _argtext_9900;
        _argtext_9900 = _6lower(_argtext_9900);
        DeRefDS(_0);
        goto L58; // [2824] 2878

        /**     					case 'w' then*/
        case 119:

        /**     						argtext = proper(argtext)*/
        RefDS(_argtext_9900);
        _0 = _argtext_9900;
        _argtext_9900 = _6proper(_argtext_9900);
        DeRefDS(_0);
        goto L58; // [2838] 2878

        /**     					case 0 then*/
        case 0:

        /** 							cap = cap*/
        _cap_9717 = _cap_9717;
        goto L58; // [2851] 2878

        /**     					case else*/
        default:

        /**     						error:crash("logic error: 'cap' mode in format.")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_2861_10221);
        _msg_inlined_crash_at_2861_10221 = EPrintf(-9999999, _5789, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_2861_10221);

        /** end procedure*/
        goto L59; // [2872] 2875
L59: 
        DeRefi(_msg_inlined_crash_at_2861_10221);
        _msg_inlined_crash_at_2861_10221 = NOVALUE;
    ;}L58: 

    /** 					if atom(currargv) then*/
    _5790 = IS_ATOM(_currargv_9736);
    if (_5790 == 0)
    {
        _5790 = NOVALUE;
        goto L5A; // [2885] 3136
    }
    else{
        _5790 = NOVALUE;
    }

    /** 						if find('e', argtext) = 0 then*/
    _5791 = find_from(101, _argtext_9900, 1);
    if (_5791 != 0)
    goto L5B; // [2895] 3135

    /** 							pflag = 0*/
    _pflag_9741 = 0;

    /** 							if msign and currargv < 0 then*/
    if (_msign_9720 == 0) {
        goto L5C; // [2910] 2930
    }
    if (IS_ATOM_INT(_currargv_9736)) {
        _5794 = (_currargv_9736 < 0);
    }
    else {
        _5794 = binary_op(LESS, _currargv_9736, 0);
    }
    if (_5794 == 0) {
        DeRef(_5794);
        _5794 = NOVALUE;
        goto L5C; // [2919] 2930
    }
    else {
        if (!IS_ATOM_INT(_5794) && DBL_PTR(_5794)->dbl == 0.0){
            DeRef(_5794);
            _5794 = NOVALUE;
            goto L5C; // [2919] 2930
        }
        DeRef(_5794);
        _5794 = NOVALUE;
    }
    DeRef(_5794);
    _5794 = NOVALUE;

    /** 								pflag = 1*/
    _pflag_9741 = 1;
L5C: 

    /** 							if decs != -1 then*/
    if (_decs_9726 == -1)
    goto L5D; // [2934] 3134

    /** 								pos = find('.', argtext)*/
    _pos_9727 = find_from(46, _argtext_9900, 1);

    /** 								if pos then*/
    if (_pos_9727 == 0)
    {
        goto L5E; // [2949] 3079
    }
    else{
    }

    /** 									if decs = 0 then*/
    if (_decs_9726 != 0)
    goto L5F; // [2954] 2972

    /** 										argtext = argtext [1 .. pos-1 ]*/
    _5798 = _pos_9727 - 1;
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, 1, _5798);
    goto L60; // [2969] 3133
L5F: 

    /** 										pos = length(argtext) - pos - pflag*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5800 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5800 = 1;
    }
    _5801 = _5800 - _pos_9727;
    if ((long)((unsigned long)_5801 +(unsigned long) HIGH_BITS) >= 0){
        _5801 = NewDouble((double)_5801);
    }
    _5800 = NOVALUE;
    if (IS_ATOM_INT(_5801)) {
        _pos_9727 = _5801 - _pflag_9741;
    }
    else {
        _pos_9727 = NewDouble(DBL_PTR(_5801)->dbl - (double)_pflag_9741);
    }
    DeRef(_5801);
    _5801 = NOVALUE;
    if (!IS_ATOM_INT(_pos_9727)) {
        _1 = (long)(DBL_PTR(_pos_9727)->dbl);
        if (UNIQUE(DBL_PTR(_pos_9727)) && (DBL_PTR(_pos_9727)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_9727);
        _pos_9727 = _1;
    }

    /** 										if pos > decs then*/
    if (_pos_9727 <= _decs_9726)
    goto L61; // [2991] 3016

    /** 											argtext = argtext[ 1 .. $ - pos + decs ]*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5804 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5804 = 1;
    }
    _5805 = _5804 - _pos_9727;
    if ((long)((unsigned long)_5805 +(unsigned long) HIGH_BITS) >= 0){
        _5805 = NewDouble((double)_5805);
    }
    _5804 = NOVALUE;
    if (IS_ATOM_INT(_5805)) {
        _5806 = _5805 + _decs_9726;
    }
    else {
        _5806 = NewDouble(DBL_PTR(_5805)->dbl + (double)_decs_9726);
    }
    DeRef(_5805);
    _5805 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, 1, _5806);
    goto L60; // [3013] 3133
L61: 

    /** 										elsif pos < decs then*/
    if (_pos_9727 >= _decs_9726)
    goto L60; // [3018] 3133

    /** 											if pflag then*/
    if (_pflag_9741 == 0)
    {
        goto L62; // [3024] 3058
    }
    else{
    }

    /** 												argtext = argtext[ 1 .. $ - 1 ] & repeat('0', decs - pos) & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5809 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5809 = 1;
    }
    _5810 = _5809 - 1;
    _5809 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5811;
    RHS_Slice(_argtext_9900, 1, _5810);
    _5812 = _decs_9726 - _pos_9727;
    _5813 = Repeat(48, _5812);
    _5812 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5813;
        concat_list[2] = _5811;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5813);
    _5813 = NOVALUE;
    DeRefDS(_5811);
    _5811 = NOVALUE;
    goto L60; // [3055] 3133
L62: 

    /** 												argtext = argtext & repeat('0', decs - pos)*/
    _5815 = _decs_9726 - _pos_9727;
    _5816 = Repeat(48, _5815);
    _5815 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _argtext_9900, _5816);
    DeRefDS(_5816);
    _5816 = NOVALUE;
    goto L60; // [3076] 3133
L5E: 

    /** 								elsif decs > 0 then*/
    if (_decs_9726 <= 0)
    goto L63; // [3081] 3132

    /** 									if pflag then*/
    if (_pflag_9741 == 0)
    {
        goto L64; // [3087] 3118
    }
    else{
    }

    /** 										argtext = argtext[1 .. $ - 1] & '.' & repeat('0', decs) & ')'*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5819 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5819 = 1;
    }
    _5820 = _5819 - 1;
    _5819 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5821;
    RHS_Slice(_argtext_9900, 1, _5820);
    _5822 = Repeat(48, _decs_9726);
    {
        int concat_list[4];

        concat_list[0] = 41;
        concat_list[1] = _5822;
        concat_list[2] = 46;
        concat_list[3] = _5821;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 4);
    }
    DeRefDS(_5822);
    _5822 = NOVALUE;
    DeRefDS(_5821);
    _5821 = NOVALUE;
    goto L65; // [3115] 3131
L64: 

    /** 										argtext = argtext & '.' & repeat('0', decs)*/
    _5824 = Repeat(48, _decs_9726);
    {
        int concat_list[3];

        concat_list[0] = _5824;
        concat_list[1] = 46;
        concat_list[2] = _argtext_9900;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5824);
    _5824 = NOVALUE;
L65: 
L63: 
L60: 
L5D: 
L5B: 
L5A: 

    /**     				if align = 0 then*/
    if (_align_9718 != 0)
    goto L66; // [3140] 3171

    /**     					if atom(currargv) then*/
    _5827 = IS_ATOM(_currargv_9736);
    if (_5827 == 0)
    {
        _5827 = NOVALUE;
        goto L67; // [3149] 3162
    }
    else{
        _5827 = NOVALUE;
    }

    /**     						align = '>'*/
    _align_9718 = 62;
    goto L68; // [3159] 3170
L67: 

    /**     						align = '<'*/
    _align_9718 = 60;
L68: 
L66: 

    /**     				if atom(currargv) then*/
    _5828 = IS_ATOM(_currargv_9736);
    if (_5828 == 0)
    {
        _5828 = NOVALUE;
        goto L69; // [3176] 3427
    }
    else{
        _5828 = NOVALUE;
    }

    /** 	    				if tsep != 0 and zfill = 0 then*/
    _5829 = (_tsep_9733 != 0);
    if (_5829 == 0) {
        goto L6A; // [3187] 3424
    }
    _5831 = (_zfill_9721 == 0);
    if (_5831 == 0)
    {
        DeRef(_5831);
        _5831 = NOVALUE;
        goto L6A; // [3198] 3424
    }
    else{
        DeRef(_5831);
        _5831 = NOVALUE;
    }

    /** 	    					integer dpos*/

    /** 	    					integer dist*/

    /** 	    					integer bracketed*/

    /** 	    					if binout or hexout then*/
    if (_binout_9732 != 0) {
        goto L6B; // [3211] 3220
    }
    if (_hexout_9731 == 0)
    {
        goto L6C; // [3216] 3237
    }
    else{
    }
L6B: 

    /** 	    						dist = 4*/
    _dist_10284 = 4;

    /** 							psign = 0*/
    _psign_9719 = 0;
    goto L6D; // [3234] 3245
L6C: 

    /** 	    						dist = 3*/
    _dist_10284 = 3;
L6D: 

    /** 	    					bracketed = (argtext[1] = '(')*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    _5833 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5833)) {
        _bracketed_10285 = (_5833 == 40);
    }
    else {
        _bracketed_10285 = binary_op(EQUALS, _5833, 40);
    }
    _5833 = NOVALUE;
    if (!IS_ATOM_INT(_bracketed_10285)) {
        _1 = (long)(DBL_PTR(_bracketed_10285)->dbl);
        if (UNIQUE(DBL_PTR(_bracketed_10285)) && (DBL_PTR(_bracketed_10285)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bracketed_10285);
        _bracketed_10285 = _1;
    }

    /** 	    					if bracketed then*/
    if (_bracketed_10285 == 0)
    {
        goto L6E; // [3261] 3279
    }
    else{
    }

    /** 	    						argtext = argtext[2 .. $-1]*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5835 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5835 = 1;
    }
    _5836 = _5835 - 1;
    _5835 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, 2, _5836);
L6E: 

    /** 	    					dpos = find('.', argtext)*/
    _dpos_10283 = find_from(46, _argtext_9900, 1);

    /** 	    					if dpos = 0 then*/
    if (_dpos_10283 != 0)
    goto L6F; // [3290] 3308

    /** 	    						dpos = length(argtext) + 1*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5840 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5840 = 1;
    }
    _dpos_10283 = _5840 + 1;
    _5840 = NOVALUE;
    goto L70; // [3305] 3322
L6F: 

    /** 	    						if tsep = '.' then*/
    if (_tsep_9733 != 46)
    goto L71; // [3310] 3321

    /** 	    							argtext[dpos] = ','*/
    _2 = (int)SEQ_PTR(_argtext_9900);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_9900 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _dpos_10283);
    _1 = *(int *)_2;
    *(int *)_2 = 44;
    DeRef(_1);
L71: 
L70: 

    /** 	    					while dpos > dist do*/
L72: 
    if (_dpos_10283 <= _dist_10284)
    goto L73; // [3329] 3409

    /** 	    						dpos -= dist*/
    _dpos_10283 = _dpos_10283 - _dist_10284;

    /** 	    						if dpos > 1 + (currargv < 0) * not msign + (currargv > 0) * psign then*/
    if (IS_ATOM_INT(_currargv_9736)) {
        _5845 = (_currargv_9736 < 0);
    }
    else {
        _5845 = binary_op(LESS, _currargv_9736, 0);
    }
    _5846 = (_msign_9720 == 0);
    if (IS_ATOM_INT(_5845)) {
        if (_5845 == (short)_5845 && _5846 <= INT15 && _5846 >= -INT15)
        _5847 = _5845 * _5846;
        else
        _5847 = NewDouble(_5845 * (double)_5846);
    }
    else {
        _5847 = binary_op(MULTIPLY, _5845, _5846);
    }
    DeRef(_5845);
    _5845 = NOVALUE;
    _5846 = NOVALUE;
    if (IS_ATOM_INT(_5847)) {
        _5848 = _5847 + 1;
        if (_5848 > MAXINT){
            _5848 = NewDouble((double)_5848);
        }
    }
    else
    _5848 = binary_op(PLUS, 1, _5847);
    DeRef(_5847);
    _5847 = NOVALUE;
    if (IS_ATOM_INT(_currargv_9736)) {
        _5849 = (_currargv_9736 > 0);
    }
    else {
        _5849 = binary_op(GREATER, _currargv_9736, 0);
    }
    if (IS_ATOM_INT(_5849)) {
        if (_5849 == (short)_5849 && _psign_9719 <= INT15 && _psign_9719 >= -INT15)
        _5850 = _5849 * _psign_9719;
        else
        _5850 = NewDouble(_5849 * (double)_psign_9719);
    }
    else {
        _5850 = binary_op(MULTIPLY, _5849, _psign_9719);
    }
    DeRef(_5849);
    _5849 = NOVALUE;
    if (IS_ATOM_INT(_5848) && IS_ATOM_INT(_5850)) {
        _5851 = _5848 + _5850;
        if ((long)((unsigned long)_5851 + (unsigned long)HIGH_BITS) >= 0) 
        _5851 = NewDouble((double)_5851);
    }
    else {
        _5851 = binary_op(PLUS, _5848, _5850);
    }
    DeRef(_5848);
    _5848 = NOVALUE;
    DeRef(_5850);
    _5850 = NOVALUE;
    if (binary_op_a(LESSEQ, _dpos_10283, _5851)){
        DeRef(_5851);
        _5851 = NOVALUE;
        goto L72; // [3374] 3327
    }
    DeRef(_5851);
    _5851 = NOVALUE;

    /** 	    							argtext = argtext[1.. dpos - 1] & tsep & argtext[dpos .. $]*/
    _5853 = _dpos_10283 - 1;
    rhs_slice_target = (object_ptr)&_5854;
    RHS_Slice(_argtext_9900, 1, _5853);
    if (IS_SEQUENCE(_argtext_9900)){
            _5855 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5855 = 1;
    }
    rhs_slice_target = (object_ptr)&_5856;
    RHS_Slice(_argtext_9900, _dpos_10283, _5855);
    {
        int concat_list[3];

        concat_list[0] = _5856;
        concat_list[1] = _tsep_9733;
        concat_list[2] = _5854;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5856);
    _5856 = NOVALUE;
    DeRefDS(_5854);
    _5854 = NOVALUE;

    /** 	    					end while*/
    goto L72; // [3406] 3327
L73: 

    /** 	    					if bracketed then*/
    if (_bracketed_10285 == 0)
    {
        goto L74; // [3411] 3423
    }
    else{
    }

    /** 	    						argtext = '(' & argtext & ')'*/
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _argtext_9900;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
L74: 
L6A: 
L69: 

    /**     				if width <= 0 then*/
    if (_width_9725 > 0)
    goto L75; // [3431] 3443

    /**     					width = length(argtext)*/
    if (IS_SEQUENCE(_argtext_9900)){
            _width_9725 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _width_9725 = 1;
    }
L75: 

    /**     				if width < length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5861 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5861 = 1;
    }
    if (_width_9725 >= _5861)
    goto L76; // [3448] 3585

    /**     					if align = '>' then*/
    if (_align_9718 != 62)
    goto L77; // [3454] 3482

    /**     						argtext = argtext[ $ - width + 1 .. $]*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5864 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5864 = 1;
    }
    _5865 = _5864 - _width_9725;
    if ((long)((unsigned long)_5865 +(unsigned long) HIGH_BITS) >= 0){
        _5865 = NewDouble((double)_5865);
    }
    _5864 = NOVALUE;
    if (IS_ATOM_INT(_5865)) {
        _5866 = _5865 + 1;
        if (_5866 > MAXINT){
            _5866 = NewDouble((double)_5866);
        }
    }
    else
    _5866 = binary_op(PLUS, 1, _5865);
    DeRef(_5865);
    _5865 = NOVALUE;
    if (IS_SEQUENCE(_argtext_9900)){
            _5867 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5867 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, _5866, _5867);
    goto L78; // [3479] 3728
L77: 

    /**     					elsif align = 'c' then*/
    if (_align_9718 != 99)
    goto L79; // [3484] 3574

    /**     						pos = length(argtext) - width*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5870 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5870 = 1;
    }
    _pos_9727 = _5870 - _width_9725;
    _5870 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _5872 = (_pos_9727 % 2);
    if (_5872 != 0)
    goto L7A; // [3505] 3540

    /**     							pos = pos / 2*/
    if (_pos_9727 & 1) {
        _pos_9727 = NewDouble((_pos_9727 >> 1) + 0.5);
    }
    else
    _pos_9727 = _pos_9727 >> 1;
    if (!IS_ATOM_INT(_pos_9727)) {
        _1 = (long)(DBL_PTR(_pos_9727)->dbl);
        if (UNIQUE(DBL_PTR(_pos_9727)) && (DBL_PTR(_pos_9727)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_9727);
        _pos_9727 = _1;
    }

    /**     							argtext = argtext[ pos + 1 .. $ - pos ]*/
    _5875 = _pos_9727 + 1;
    if (_5875 > MAXINT){
        _5875 = NewDouble((double)_5875);
    }
    if (IS_SEQUENCE(_argtext_9900)){
            _5876 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5876 = 1;
    }
    _5877 = _5876 - _pos_9727;
    _5876 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, _5875, _5877);
    goto L78; // [3537] 3728
L7A: 

    /**     							pos = floor(pos / 2)*/
    _pos_9727 = _pos_9727 >> 1;

    /**     							argtext = argtext[ pos + 1 .. $ - pos - 1]*/
    _5880 = _pos_9727 + 1;
    if (_5880 > MAXINT){
        _5880 = NewDouble((double)_5880);
    }
    if (IS_SEQUENCE(_argtext_9900)){
            _5881 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5881 = 1;
    }
    _5882 = _5881 - _pos_9727;
    if ((long)((unsigned long)_5882 +(unsigned long) HIGH_BITS) >= 0){
        _5882 = NewDouble((double)_5882);
    }
    _5881 = NOVALUE;
    if (IS_ATOM_INT(_5882)) {
        _5883 = _5882 - 1;
    }
    else {
        _5883 = NewDouble(DBL_PTR(_5882)->dbl - (double)1);
    }
    DeRef(_5882);
    _5882 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, _5880, _5883);
    goto L78; // [3571] 3728
L79: 

    /**     						argtext = argtext[ 1 .. width]*/
    rhs_slice_target = (object_ptr)&_argtext_9900;
    RHS_Slice(_argtext_9900, 1, _width_9725);
    goto L78; // [3582] 3728
L76: 

    /**     				elsif width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5886 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5886 = 1;
    }
    if (_width_9725 <= _5886)
    goto L7B; // [3590] 3727

    /** 						if align = '>' then*/
    if (_align_9718 != 62)
    goto L7C; // [3596] 3620

    /** 							argtext = repeat(' ', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5889 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5889 = 1;
    }
    _5890 = _width_9725 - _5889;
    _5889 = NOVALUE;
    _5891 = Repeat(32, _5890);
    _5890 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _5891, _argtext_9900);
    DeRefDS(_5891);
    _5891 = NOVALUE;
    DeRef(_5891);
    _5891 = NOVALUE;
    goto L7D; // [3617] 3726
L7C: 

    /**     					elsif align = 'c' then*/
    if (_align_9718 != 99)
    goto L7E; // [3622] 3708

    /**     						pos = width - length(argtext)*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5894 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5894 = 1;
    }
    _pos_9727 = _width_9725 - _5894;
    _5894 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _5896 = (_pos_9727 % 2);
    if (_5896 != 0)
    goto L7F; // [3643] 3676

    /**     							pos = pos / 2*/
    if (_pos_9727 & 1) {
        _pos_9727 = NewDouble((_pos_9727 >> 1) + 0.5);
    }
    else
    _pos_9727 = _pos_9727 >> 1;
    if (!IS_ATOM_INT(_pos_9727)) {
        _1 = (long)(DBL_PTR(_pos_9727)->dbl);
        if (UNIQUE(DBL_PTR(_pos_9727)) && (DBL_PTR(_pos_9727)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_9727);
        _pos_9727 = _1;
    }

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos)*/
    _5899 = Repeat(32, _pos_9727);
    _5900 = Repeat(32, _pos_9727);
    {
        int concat_list[3];

        concat_list[0] = _5900;
        concat_list[1] = _argtext_9900;
        concat_list[2] = _5899;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5900);
    _5900 = NOVALUE;
    DeRefDS(_5899);
    _5899 = NOVALUE;
    goto L7D; // [3673] 3726
L7F: 

    /**     							pos = floor(pos / 2)*/
    _pos_9727 = _pos_9727 >> 1;

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos + 1)*/
    _5903 = Repeat(32, _pos_9727);
    _5904 = _pos_9727 + 1;
    _5905 = Repeat(32, _5904);
    _5904 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _5905;
        concat_list[1] = _argtext_9900;
        concat_list[2] = _5903;
        Concat_N((object_ptr)&_argtext_9900, concat_list, 3);
    }
    DeRefDS(_5905);
    _5905 = NOVALUE;
    DeRefDS(_5903);
    _5903 = NOVALUE;
    goto L7D; // [3705] 3726
L7E: 

    /** 							argtext = argtext & repeat(' ', width - length(argtext))*/
    if (IS_SEQUENCE(_argtext_9900)){
            _5907 = SEQ_PTR(_argtext_9900)->length;
    }
    else {
        _5907 = 1;
    }
    _5908 = _width_9725 - _5907;
    _5907 = NOVALUE;
    _5909 = Repeat(32, _5908);
    _5908 = NOVALUE;
    Concat((object_ptr)&_argtext_9900, _argtext_9900, _5909);
    DeRefDS(_5909);
    _5909 = NOVALUE;
L7D: 
L7B: 
L78: 

    /**     				result &= argtext*/
    Concat((object_ptr)&_result_9712, _result_9712, _argtext_9900);
    goto L80; // [3734] 3750
L57: 

    /**     				if spacer then*/
    if (_spacer_9723 == 0)
    {
        goto L81; // [3739] 3749
    }
    else{
    }

    /**     					result &= ' '*/
    Append(&_result_9712, _result_9712, 32);
L81: 
L80: 

    /**    				if trimming then*/
    if (_trimming_9730 == 0)
    {
        goto L82; // [3754] 3768
    }
    else{
    }

    /**    					result = trim(result)*/
    RefDS(_result_9712);
    RefDS(_4282);
    _0 = _result_9712;
    _result_9712 = _6trim(_result_9712, _4282, 0);
    DeRefDS(_0);
L82: 

    /**     			tend = 0*/
    _tend_9716 = 0;

    /** 		    	prevargv = currargv*/
    Ref(_currargv_9736);
    DeRef(_prevargv_9735);
    _prevargv_9735 = _currargv_9736;
L1F: 
    DeRef(_argtext_9900);
    _argtext_9900 = NOVALUE;

    /**     end while*/
    goto L2; // [3788] 70
L3: 

    /** 	return result*/
    DeRefDSi(_format_pattern_9710);
    DeRef(_arg_list_9711);
    DeRef(_prevargv_9735);
    DeRef(_currargv_9736);
    DeRef(_idname_9737);
    DeRef(_envsym_9738);
    DeRefi(_envvar_9739);
    DeRef(_5579);
    _5579 = NOVALUE;
    DeRef(_5798);
    _5798 = NOVALUE;
    _5479 = NOVALUE;
    DeRef(_5810);
    _5810 = NOVALUE;
    _5539 = NOVALUE;
    DeRef(_5513);
    _5513 = NOVALUE;
    DeRef(_5599);
    _5599 = NOVALUE;
    DeRef(_5662);
    _5662 = NOVALUE;
    DeRef(_5829);
    _5829 = NOVALUE;
    DeRef(_5877);
    _5877 = NOVALUE;
    DeRef(_5883);
    _5883 = NOVALUE;
    DeRef(_5609);
    _5609 = NOVALUE;
    DeRef(_5896);
    _5896 = NOVALUE;
    DeRef(_5853);
    _5853 = NOVALUE;
    _5533 = NOVALUE;
    DeRef(_5806);
    _5806 = NOVALUE;
    DeRef(_5872);
    _5872 = NOVALUE;
    _5517 = NOVALUE;
    _5531 = NOVALUE;
    DeRef(_5680);
    _5680 = NOVALUE;
    DeRef(_5875);
    _5875 = NOVALUE;
    DeRef(_5589);
    _5589 = NOVALUE;
    DeRef(_5619);
    _5619 = NOVALUE;
    DeRef(_5820);
    _5820 = NOVALUE;
    DeRef(_5866);
    _5866 = NOVALUE;
    DeRef(_5880);
    _5880 = NOVALUE;
    DeRef(_5735);
    _5735 = NOVALUE;
    DeRef(_5567);
    _5567 = NOVALUE;
    _5510 = NOVALUE;
    _5737 = NOVALUE;
    DeRef(_5767);
    _5767 = NOVALUE;
    _5508 = NOVALUE;
    DeRef(_5536);
    _5536 = NOVALUE;
    DeRef(_5836);
    _5836 = NOVALUE;
    DeRef(_5698);
    _5698 = NOVALUE;
    DeRef(_5757);
    _5757 = NOVALUE;
    return _result_9712;
    ;
}



// 0x0B3196F3
