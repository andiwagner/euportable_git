// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _26malloc(int _mem_struct_p_11046, int _cleanup_p_11047)
{
    int _temp__11048 = NOVALUE;
    int _6271 = NOVALUE;
    int _6269 = NOVALUE;
    int _6268 = NOVALUE;
    int _6267 = NOVALUE;
    int _6263 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_cleanup_p_11047)) {
        _1 = (long)(DBL_PTR(_cleanup_p_11047)->dbl);
        if (UNIQUE(DBL_PTR(_cleanup_p_11047)) && (DBL_PTR(_cleanup_p_11047)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_cleanup_p_11047);
        _cleanup_p_11047 = _1;
    }

    /** 	if atom(mem_struct_p) then*/
    _6263 = IS_ATOM(_mem_struct_p_11046);
    if (_6263 == 0)
    {
        _6263 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _6263 = NOVALUE;
    }

    /** 		mem_struct_p = repeat(0, mem_struct_p)*/
    _0 = _mem_struct_p_11046;
    _mem_struct_p_11046 = Repeat(0, _mem_struct_p_11046);
    DeRef(_0);
L1: 

    /** 	if ram_free_list = 0 then*/
    if (_26ram_free_list_11042 != 0)
    goto L2; // [24] 74

    /** 		ram_space = append(ram_space, mem_struct_p)*/
    Ref(_mem_struct_p_11046);
    Append(&_26ram_space_11041, _26ram_space_11041, _mem_struct_p_11046);

    /** 		if cleanup_p then*/
    if (_cleanup_p_11047 == 0)
    {
        goto L3; // [38] 61
    }
    else{
    }

    /** 			return delete_routine( length(ram_space), free_rid )*/
    if (IS_SEQUENCE(_26ram_space_11041)){
            _6267 = SEQ_PTR(_26ram_space_11041)->length;
    }
    else {
        _6267 = 1;
    }
    _6268 = NewDouble( (double) _6267 );
    _1 = (int) _00[_26free_rid_11043].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_26free_rid_11043].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _26free_rid_11043;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6268)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6268)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6268)) ){
        DeRefDS(_6268);
        _6268 = NewDouble( DBL_PTR(_6268)->dbl );
    }
    DBL_PTR(_6268)->cleanup = (cleanup_ptr)_1;
    _6267 = NOVALUE;
    DeRef(_mem_struct_p_11046);
    return _6268;
    goto L4; // [58] 73
L3: 

    /** 			return length(ram_space)*/
    if (IS_SEQUENCE(_26ram_space_11041)){
            _6269 = SEQ_PTR(_26ram_space_11041)->length;
    }
    else {
        _6269 = 1;
    }
    DeRef(_mem_struct_p_11046);
    DeRef(_6268);
    _6268 = NOVALUE;
    return _6269;
L4: 
L2: 

    /** 	temp_ = ram_free_list*/
    _temp__11048 = _26ram_free_list_11042;

    /** 	ram_free_list = ram_space[temp_]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _26ram_free_list_11042 = (int)*(((s1_ptr)_2)->base + _temp__11048);
    if (!IS_ATOM_INT(_26ram_free_list_11042))
    _26ram_free_list_11042 = (long)DBL_PTR(_26ram_free_list_11042)->dbl;

    /** 	ram_space[temp_] = mem_struct_p*/
    Ref(_mem_struct_p_11046);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp__11048);
    _1 = *(int *)_2;
    *(int *)_2 = _mem_struct_p_11046;
    DeRef(_1);

    /** 	if cleanup_p then*/
    if (_cleanup_p_11047 == 0)
    {
        goto L5; // [103] 121
    }
    else{
    }

    /** 		return delete_routine( temp_, free_rid )*/
    _6271 = NewDouble( (double) _temp__11048 );
    _1 = (int) _00[_26free_rid_11043].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_26free_rid_11043].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _26free_rid_11043;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6271)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6271)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6271)) ){
        DeRefDS(_6271);
        _6271 = NewDouble( DBL_PTR(_6271)->dbl );
    }
    DBL_PTR(_6271)->cleanup = (cleanup_ptr)_1;
    DeRef(_mem_struct_p_11046);
    DeRef(_6268);
    _6268 = NOVALUE;
    return _6271;
    goto L6; // [118] 128
L5: 

    /** 		return temp_*/
    DeRef(_mem_struct_p_11046);
    DeRef(_6268);
    _6268 = NOVALUE;
    DeRef(_6271);
    _6271 = NOVALUE;
    return _temp__11048;
L6: 
    ;
}


void _26free(int _mem_p_11066)
{
    int _6273 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if mem_p < 1 then return end if*/
    if (binary_op_a(GREATEREQ, _mem_p_11066, 1)){
        goto L1; // [3] 11
    }
    DeRef(_mem_p_11066);
    return;
L1: 

    /** 	if mem_p > length(ram_space) then return end if*/
    if (IS_SEQUENCE(_26ram_space_11041)){
            _6273 = SEQ_PTR(_26ram_space_11041)->length;
    }
    else {
        _6273 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_11066, _6273)){
        _6273 = NOVALUE;
        goto L2; // [18] 26
    }
    _6273 = NOVALUE;
    DeRef(_mem_p_11066);
    return;
L2: 

    /** 	ram_space[mem_p] = ram_free_list*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_mem_p_11066))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_11066)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _mem_p_11066);
    _1 = *(int *)_2;
    *(int *)_2 = _26ram_free_list_11042;
    DeRef(_1);

    /** 	ram_free_list = floor(mem_p)*/
    if (IS_ATOM_INT(_mem_p_11066))
    _26ram_free_list_11042 = e_floor(_mem_p_11066);
    else
    _26ram_free_list_11042 = unary_op(FLOOR, _mem_p_11066);
    if (!IS_ATOM_INT(_26ram_free_list_11042)) {
        _1 = (long)(DBL_PTR(_26ram_free_list_11042)->dbl);
        if (UNIQUE(DBL_PTR(_26ram_free_list_11042)) && (DBL_PTR(_26ram_free_list_11042)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_26ram_free_list_11042);
        _26ram_free_list_11042 = _1;
    }

    /** end procedure*/
    DeRef(_mem_p_11066);
    return;
    ;
}


int _26valid(int _mem_p_11076, int _mem_struct_p_11077)
{
    int _6287 = NOVALUE;
    int _6286 = NOVALUE;
    int _6284 = NOVALUE;
    int _6283 = NOVALUE;
    int _6282 = NOVALUE;
    int _6280 = NOVALUE;
    int _6277 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(mem_p) then return 0 end if*/
    if (IS_ATOM_INT(_mem_p_11076))
    _6277 = 1;
    else if (IS_ATOM_DBL(_mem_p_11076))
    _6277 = IS_ATOM_INT(DoubleToInt(_mem_p_11076));
    else
    _6277 = 0;
    if (_6277 != 0)
    goto L1; // [6] 14
    _6277 = NOVALUE;
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    return 0;
L1: 

    /** 	if mem_p < 1 then return 0 end if*/
    if (binary_op_a(GREATEREQ, _mem_p_11076, 1)){
        goto L2; // [16] 25
    }
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    return 0;
L2: 

    /** 	if mem_p > length(ram_space) then return 0 end if*/
    if (IS_SEQUENCE(_26ram_space_11041)){
            _6280 = SEQ_PTR(_26ram_space_11041)->length;
    }
    else {
        _6280 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_11076, _6280)){
        _6280 = NOVALUE;
        goto L3; // [32] 41
    }
    _6280 = NOVALUE;
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    return 0;
L3: 

    /** 	if sequence(mem_struct_p) then return 1 end if*/
    _6282 = IS_SEQUENCE(_mem_struct_p_11077);
    if (_6282 == 0)
    {
        _6282 = NOVALUE;
        goto L4; // [46] 54
    }
    else{
        _6282 = NOVALUE;
    }
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    return 1;
L4: 

    /** 	if atom(ram_space[mem_p]) then */
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_mem_p_11076)){
        _6283 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_11076)->dbl));
    }
    else{
        _6283 = (int)*(((s1_ptr)_2)->base + _mem_p_11076);
    }
    _6284 = IS_ATOM(_6283);
    _6283 = NOVALUE;
    if (_6284 == 0)
    {
        _6284 = NOVALUE;
        goto L5; // [65] 88
    }
    else{
        _6284 = NOVALUE;
    }

    /** 		if mem_struct_p >= 0 then*/
    if (binary_op_a(LESS, _mem_struct_p_11077, 0)){
        goto L6; // [70] 81
    }

    /** 			return 0*/
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    return 0;
L6: 

    /** 		return 1*/
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    return 1;
L5: 

    /** 	if length(ram_space[mem_p]) != mem_struct_p then*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_mem_p_11076)){
        _6286 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_11076)->dbl));
    }
    else{
        _6286 = (int)*(((s1_ptr)_2)->base + _mem_p_11076);
    }
    if (IS_SEQUENCE(_6286)){
            _6287 = SEQ_PTR(_6286)->length;
    }
    else {
        _6287 = 1;
    }
    _6286 = NOVALUE;
    if (binary_op_a(EQUALS, _6287, _mem_struct_p_11077)){
        _6287 = NOVALUE;
        goto L7; // [99] 110
    }
    _6287 = NOVALUE;

    /** 		return 0*/
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    _6286 = NOVALUE;
    return 0;
L7: 

    /** 	return 1*/
    DeRef(_mem_p_11076);
    DeRef(_mem_struct_p_11077);
    _6286 = NOVALUE;
    return 1;
    ;
}



// 0xADFFC10A
