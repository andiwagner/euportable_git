// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _2pretty_out(int _text_163)
{
    int _16 = NOVALUE;
    int _14 = NOVALUE;
    int _12 = NOVALUE;
    int _11 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pretty_line &= text*/
    if (IS_SEQUENCE(_2pretty_line_160) && IS_ATOM(_text_163)) {
        Ref(_text_163);
        Append(&_2pretty_line_160, _2pretty_line_160, _text_163);
    }
    else if (IS_ATOM(_2pretty_line_160) && IS_SEQUENCE(_text_163)) {
    }
    else {
        Concat((object_ptr)&_2pretty_line_160, _2pretty_line_160, _text_163);
    }

    /** 	if equal(text, '\n') and pretty_printing then*/
    if (_text_163 == 10)
    _11 = 1;
    else if (IS_ATOM_INT(_text_163) && IS_ATOM_INT(10))
    _11 = 0;
    else
    _11 = (compare(_text_163, 10) == 0);
    if (_11 == 0) {
        goto L1; // [15] 52
    }
    if (_2pretty_printing_157 == 0)
    {
        goto L1; // [22] 52
    }
    else{
    }

    /** 		puts(pretty_file, pretty_line)*/
    EPuts(_2pretty_file_148, _2pretty_line_160); // DJP 

    /** 		pretty_line = ""*/
    RefDS(_5);
    DeRefDS(_2pretty_line_160);
    _2pretty_line_160 = _5;

    /** 		pretty_line_count += 1*/
    _2pretty_line_count_153 = _2pretty_line_count_153 + 1;
L1: 

    /** 	if atom(text) then*/
    _14 = IS_ATOM(_text_163);
    if (_14 == 0)
    {
        _14 = NOVALUE;
        goto L2; // [57] 73
    }
    else{
        _14 = NOVALUE;
    }

    /** 		pretty_chars += 1*/
    _2pretty_chars_145 = _2pretty_chars_145 + 1;
    goto L3; // [70] 87
L2: 

    /** 		pretty_chars += length(text)*/
    if (IS_SEQUENCE(_text_163)){
            _16 = SEQ_PTR(_text_163)->length;
    }
    else {
        _16 = 1;
    }
    _2pretty_chars_145 = _2pretty_chars_145 + _16;
    _16 = NOVALUE;
L3: 

    /** end procedure*/
    DeRef(_text_163);
    return;
    ;
}


void _2cut_line(int _n_178)
{
    int _19 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not pretty_line_breaks then	*/
    if (_2pretty_line_breaks_156 != 0)
    goto L1; // [9] 25

    /** 		pretty_chars = 0*/
    _2pretty_chars_145 = 0;

    /** 		return*/
    return;
L1: 

    /** 	if pretty_chars + n > pretty_end_col then*/
    _19 = _2pretty_chars_145 + _n_178;
    if ((long)((unsigned long)_19 + (unsigned long)HIGH_BITS) >= 0) 
    _19 = NewDouble((double)_19);
    if (binary_op_a(LESSEQ, _19, _2pretty_end_col_144)){
        DeRef(_19);
        _19 = NOVALUE;
        goto L2; // [35] 52
    }
    DeRef(_19);
    _19 = NOVALUE;

    /** 		pretty_out('\n')*/
    _2pretty_out(10);

    /** 		pretty_chars = 0*/
    _2pretty_chars_145 = 0;
L2: 

    /** end procedure*/
    return;
    ;
}


void _2indent()
{
    int _29 = NOVALUE;
    int _28 = NOVALUE;
    int _27 = NOVALUE;
    int _26 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if pretty_line_breaks = 0 then	*/
    if (_2pretty_line_breaks_156 != 0)
    goto L1; // [5] 24

    /** 		pretty_chars = 0*/
    _2pretty_chars_145 = 0;

    /** 		return*/
    return;
    goto L2; // [21] 89
L1: 

    /** 	elsif pretty_line_breaks = -1 then*/
    if (_2pretty_line_breaks_156 != -1)
    goto L3; // [28] 40

    /** 		cut_line( 0 )*/
    _2cut_line(0);
    goto L2; // [37] 89
L3: 

    /** 		if pretty_chars > 0 then*/
    if (_2pretty_chars_145 <= 0)
    goto L4; // [44] 61

    /** 			pretty_out('\n')*/
    _2pretty_out(10);

    /** 			pretty_chars = 0*/
    _2pretty_chars_145 = 0;
L4: 

    /** 		pretty_out(repeat(' ', (pretty_start_col-1) + */
    _26 = _2pretty_start_col_146 - 1;
    if ((long)((unsigned long)_26 +(unsigned long) HIGH_BITS) >= 0){
        _26 = NewDouble((double)_26);
    }
    if (_2pretty_level_147 == (short)_2pretty_level_147 && _2pretty_indent_150 <= INT15 && _2pretty_indent_150 >= -INT15)
    _27 = _2pretty_level_147 * _2pretty_indent_150;
    else
    _27 = NewDouble(_2pretty_level_147 * (double)_2pretty_indent_150);
    if (IS_ATOM_INT(_26) && IS_ATOM_INT(_27)) {
        _28 = _26 + _27;
    }
    else {
        if (IS_ATOM_INT(_26)) {
            _28 = NewDouble((double)_26 + DBL_PTR(_27)->dbl);
        }
        else {
            if (IS_ATOM_INT(_27)) {
                _28 = NewDouble(DBL_PTR(_26)->dbl + (double)_27);
            }
            else
            _28 = NewDouble(DBL_PTR(_26)->dbl + DBL_PTR(_27)->dbl);
        }
    }
    DeRef(_26);
    _26 = NOVALUE;
    DeRef(_27);
    _27 = NOVALUE;
    _29 = Repeat(32, _28);
    DeRef(_28);
    _28 = NOVALUE;
    _2pretty_out(_29);
    _29 = NOVALUE;
L2: 

    /** end procedure*/
    return;
    ;
}


int _2esc_char(int _a_201)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_201)) {
        _1 = (long)(DBL_PTR(_a_201)->dbl);
        if (UNIQUE(DBL_PTR(_a_201)) && (DBL_PTR(_a_201)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_201);
        _a_201 = _1;
    }

    /** 	switch a do*/
    _0 = _a_201;
    switch ( _0 ){ 

        /** 		case'\t' then*/
        case 9:

        /** 			return `\t`*/
        RefDS(_33);
        return _33;
        goto L1; // [22] 83

        /** 		case'\n' then*/
        case 10:

        /** 			return `\n`*/
        RefDS(_34);
        return _34;
        goto L1; // [34] 83

        /** 		case'\r' then*/
        case 13:

        /** 			return `\r`*/
        RefDS(_36);
        return _36;
        goto L1; // [46] 83

        /** 		case'\\' then*/
        case 92:

        /** 			return `\\`*/
        RefDS(_38);
        return _38;
        goto L1; // [58] 83

        /** 		case'"' then*/
        case 34:

        /** 			return `\"`*/
        RefDS(_40);
        return _40;
        goto L1; // [70] 83

        /** 		case else*/
        default:

        /** 			return a*/
        return _a_201;
    ;}L1: 
    ;
}


void _2rPrint(int _a_221)
{
    int _sbuff_222 = NOVALUE;
    int _multi_line_223 = NOVALUE;
    int _all_ascii_224 = NOVALUE;
    int _100 = NOVALUE;
    int _99 = NOVALUE;
    int _98 = NOVALUE;
    int _97 = NOVALUE;
    int _93 = NOVALUE;
    int _92 = NOVALUE;
    int _91 = NOVALUE;
    int _90 = NOVALUE;
    int _87 = NOVALUE;
    int _86 = NOVALUE;
    int _84 = NOVALUE;
    int _83 = NOVALUE;
    int _81 = NOVALUE;
    int _80 = NOVALUE;
    int _79 = NOVALUE;
    int _78 = NOVALUE;
    int _77 = NOVALUE;
    int _76 = NOVALUE;
    int _75 = NOVALUE;
    int _74 = NOVALUE;
    int _73 = NOVALUE;
    int _72 = NOVALUE;
    int _71 = NOVALUE;
    int _70 = NOVALUE;
    int _69 = NOVALUE;
    int _68 = NOVALUE;
    int _67 = NOVALUE;
    int _66 = NOVALUE;
    int _65 = NOVALUE;
    int _61 = NOVALUE;
    int _60 = NOVALUE;
    int _58 = NOVALUE;
    int _57 = NOVALUE;
    int _56 = NOVALUE;
    int _55 = NOVALUE;
    int _53 = NOVALUE;
    int _52 = NOVALUE;
    int _48 = NOVALUE;
    int _47 = NOVALUE;
    int _46 = NOVALUE;
    int _42 = NOVALUE;
    int _41 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _41 = IS_ATOM(_a_221);
    if (_41 == 0)
    {
        _41 = NOVALUE;
        goto L1; // [6] 176
    }
    else{
        _41 = NOVALUE;
    }

    /** 		if integer(a) then*/
    if (IS_ATOM_INT(_a_221))
    _42 = 1;
    else if (IS_ATOM_DBL(_a_221))
    _42 = IS_ATOM_INT(DoubleToInt(_a_221));
    else
    _42 = 0;
    if (_42 == 0)
    {
        _42 = NOVALUE;
        goto L2; // [14] 157
    }
    else{
        _42 = NOVALUE;
    }

    /** 			sbuff = sprintf(pretty_int_format, a)*/
    DeRef(_sbuff_222);
    _sbuff_222 = EPrintf(-9999999, _2pretty_int_format_159, _a_221);

    /** 			if pretty_ascii then */
    if (_2pretty_ascii_149 == 0)
    {
        goto L3; // [29] 166
    }
    else{
    }

    /** 				if pretty_ascii >= 3 then */
    if (_2pretty_ascii_149 < 3)
    goto L4; // [36] 103

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) then*/
    if (IS_ATOM_INT(_a_221)) {
        _46 = (_a_221 >= _2pretty_ascii_min_151);
    }
    else {
        _46 = binary_op(GREATEREQ, _a_221, _2pretty_ascii_min_151);
    }
    if (IS_ATOM_INT(_46)) {
        if (_46 == 0) {
            _47 = 0;
            goto L5; // [48] 62
        }
    }
    else {
        if (DBL_PTR(_46)->dbl == 0.0) {
            _47 = 0;
            goto L5; // [48] 62
        }
    }
    if (IS_ATOM_INT(_a_221)) {
        _48 = (_a_221 <= _2pretty_ascii_max_152);
    }
    else {
        _48 = binary_op(LESSEQ, _a_221, _2pretty_ascii_max_152);
    }
    DeRef(_47);
    if (IS_ATOM_INT(_48))
    _47 = (_48 != 0);
    else
    _47 = DBL_PTR(_48)->dbl != 0.0;
L5: 
    if (_47 == 0)
    {
        _47 = NOVALUE;
        goto L6; // [62] 76
    }
    else{
        _47 = NOVALUE;
    }

    /** 						sbuff = '\'' & a & '\''  -- display char only*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_221;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_222, concat_list, 3);
    }
    goto L3; // [73] 166
L6: 

    /** 					elsif find(a, "\t\n\r\\") then*/
    _52 = find_from(_a_221, _51, 1);
    if (_52 == 0)
    {
        _52 = NOVALUE;
        goto L3; // [83] 166
    }
    else{
        _52 = NOVALUE;
    }

    /** 						sbuff = '\'' & esc_char(a) & '\''  -- display char only*/
    Ref(_a_221);
    _53 = _2esc_char(_a_221);
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _53;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_222, concat_list, 3);
    }
    DeRef(_53);
    _53 = NOVALUE;
    goto L3; // [100] 166
L4: 

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) and pretty_ascii < 2 then*/
    if (IS_ATOM_INT(_a_221)) {
        _55 = (_a_221 >= _2pretty_ascii_min_151);
    }
    else {
        _55 = binary_op(GREATEREQ, _a_221, _2pretty_ascii_min_151);
    }
    if (IS_ATOM_INT(_55)) {
        if (_55 == 0) {
            DeRef(_56);
            _56 = 0;
            goto L7; // [111] 125
        }
    }
    else {
        if (DBL_PTR(_55)->dbl == 0.0) {
            DeRef(_56);
            _56 = 0;
            goto L7; // [111] 125
        }
    }
    if (IS_ATOM_INT(_a_221)) {
        _57 = (_a_221 <= _2pretty_ascii_max_152);
    }
    else {
        _57 = binary_op(LESSEQ, _a_221, _2pretty_ascii_max_152);
    }
    DeRef(_56);
    if (IS_ATOM_INT(_57))
    _56 = (_57 != 0);
    else
    _56 = DBL_PTR(_57)->dbl != 0.0;
L7: 
    if (_56 == 0) {
        goto L3; // [125] 166
    }
    _60 = (_2pretty_ascii_149 < 2);
    if (_60 == 0)
    {
        DeRef(_60);
        _60 = NOVALUE;
        goto L3; // [136] 166
    }
    else{
        DeRef(_60);
        _60 = NOVALUE;
    }

    /** 						sbuff &= '\'' & a & '\'' -- add to numeric display*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_221;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_61, concat_list, 3);
    }
    Concat((object_ptr)&_sbuff_222, _sbuff_222, _61);
    DeRefDS(_61);
    _61 = NOVALUE;
    goto L3; // [154] 166
L2: 

    /** 			sbuff = sprintf(pretty_fp_format, a)*/
    DeRef(_sbuff_222);
    _sbuff_222 = EPrintf(-9999999, _2pretty_fp_format_158, _a_221);
L3: 

    /** 		pretty_out(sbuff)*/
    RefDS(_sbuff_222);
    _2pretty_out(_sbuff_222);
    goto L8; // [173] 551
L1: 

    /** 		cut_line(1)*/
    _2cut_line(1);

    /** 		multi_line = 0*/
    _multi_line_223 = 0;

    /** 		all_ascii = pretty_ascii > 1*/
    _all_ascii_224 = (_2pretty_ascii_149 > 1);

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_221)){
            _65 = SEQ_PTR(_a_221)->length;
    }
    else {
        _65 = 1;
    }
    {
        int _i_260;
        _i_260 = 1;
L9: 
        if (_i_260 > _65){
            goto LA; // [203] 355
        }

        /** 			if sequence(a[i]) and length(a[i]) > 0 then*/
        _2 = (int)SEQ_PTR(_a_221);
        _66 = (int)*(((s1_ptr)_2)->base + _i_260);
        _67 = IS_SEQUENCE(_66);
        _66 = NOVALUE;
        if (_67 == 0) {
            goto LB; // [219] 257
        }
        _2 = (int)SEQ_PTR(_a_221);
        _69 = (int)*(((s1_ptr)_2)->base + _i_260);
        if (IS_SEQUENCE(_69)){
                _70 = SEQ_PTR(_69)->length;
        }
        else {
            _70 = 1;
        }
        _69 = NOVALUE;
        _71 = (_70 > 0);
        _70 = NOVALUE;
        if (_71 == 0)
        {
            DeRef(_71);
            _71 = NOVALUE;
            goto LB; // [235] 257
        }
        else{
            DeRef(_71);
            _71 = NOVALUE;
        }

        /** 				multi_line = 1*/
        _multi_line_223 = 1;

        /** 				all_ascii = 0*/
        _all_ascii_224 = 0;

        /** 				exit*/
        goto LA; // [254] 355
LB: 

        /** 			if not integer(a[i]) or*/
        _2 = (int)SEQ_PTR(_a_221);
        _72 = (int)*(((s1_ptr)_2)->base + _i_260);
        if (IS_ATOM_INT(_72))
        _73 = 1;
        else if (IS_ATOM_DBL(_72))
        _73 = IS_ATOM_INT(DoubleToInt(_72));
        else
        _73 = 0;
        _72 = NOVALUE;
        _74 = (_73 == 0);
        _73 = NOVALUE;
        if (_74 != 0) {
            _75 = 1;
            goto LC; // [269] 321
        }
        _2 = (int)SEQ_PTR(_a_221);
        _76 = (int)*(((s1_ptr)_2)->base + _i_260);
        if (IS_ATOM_INT(_76)) {
            _77 = (_76 < _2pretty_ascii_min_151);
        }
        else {
            _77 = binary_op(LESS, _76, _2pretty_ascii_min_151);
        }
        _76 = NOVALUE;
        if (IS_ATOM_INT(_77)) {
            if (_77 == 0) {
                DeRef(_78);
                _78 = 0;
                goto LD; // [283] 317
            }
        }
        else {
            if (DBL_PTR(_77)->dbl == 0.0) {
                DeRef(_78);
                _78 = 0;
                goto LD; // [283] 317
            }
        }
        _79 = (_2pretty_ascii_149 < 2);
        if (_79 != 0) {
            _80 = 1;
            goto LE; // [293] 313
        }
        _2 = (int)SEQ_PTR(_a_221);
        _81 = (int)*(((s1_ptr)_2)->base + _i_260);
        _83 = find_from(_81, _82, 1);
        _81 = NOVALUE;
        _84 = (_83 == 0);
        _83 = NOVALUE;
        _80 = (_84 != 0);
LE: 
        DeRef(_78);
        _78 = (_80 != 0);
LD: 
        _75 = (_78 != 0);
LC: 
        if (_75 != 0) {
            goto LF; // [321] 340
        }
        _2 = (int)SEQ_PTR(_a_221);
        _86 = (int)*(((s1_ptr)_2)->base + _i_260);
        if (IS_ATOM_INT(_86)) {
            _87 = (_86 > _2pretty_ascii_max_152);
        }
        else {
            _87 = binary_op(GREATER, _86, _2pretty_ascii_max_152);
        }
        _86 = NOVALUE;
        if (_87 == 0) {
            DeRef(_87);
            _87 = NOVALUE;
            goto L10; // [336] 348
        }
        else {
            if (!IS_ATOM_INT(_87) && DBL_PTR(_87)->dbl == 0.0){
                DeRef(_87);
                _87 = NOVALUE;
                goto L10; // [336] 348
            }
            DeRef(_87);
            _87 = NOVALUE;
        }
        DeRef(_87);
        _87 = NOVALUE;
LF: 

        /** 				all_ascii = 0*/
        _all_ascii_224 = 0;
L10: 

        /** 		end for*/
        _i_260 = _i_260 + 1;
        goto L9; // [350] 210
LA: 
        ;
    }

    /** 		if all_ascii then*/
    if (_all_ascii_224 == 0)
    {
        goto L11; // [357] 368
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _2pretty_out(34);
    goto L12; // [365] 374
L11: 

    /** 			pretty_out('{')*/
    _2pretty_out(123);
L12: 

    /** 		pretty_level += 1*/
    _2pretty_level_147 = _2pretty_level_147 + 1;

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_221)){
            _90 = SEQ_PTR(_a_221)->length;
    }
    else {
        _90 = 1;
    }
    {
        int _i_291;
        _i_291 = 1;
L13: 
        if (_i_291 > _90){
            goto L14; // [389] 511
        }

        /** 			if multi_line then*/
        if (_multi_line_223 == 0)
        {
            goto L15; // [398] 406
        }
        else{
        }

        /** 				indent()*/
        _2indent();
L15: 

        /** 			if all_ascii then*/
        if (_all_ascii_224 == 0)
        {
            goto L16; // [408] 427
        }
        else{
        }

        /** 				pretty_out(esc_char(a[i]))*/
        _2 = (int)SEQ_PTR(_a_221);
        _91 = (int)*(((s1_ptr)_2)->base + _i_291);
        Ref(_91);
        _92 = _2esc_char(_91);
        _91 = NOVALUE;
        _2pretty_out(_92);
        _92 = NOVALUE;
        goto L17; // [424] 437
L16: 

        /** 				rPrint(a[i])*/
        _2 = (int)SEQ_PTR(_a_221);
        _93 = (int)*(((s1_ptr)_2)->base + _i_291);
        Ref(_93);
        _2rPrint(_93);
        _93 = NOVALUE;
L17: 

        /** 			if pretty_line_count >= pretty_line_max then*/
        if (_2pretty_line_count_153 < _2pretty_line_max_154)
        goto L18; // [443] 473

        /** 				if not pretty_dots then*/
        if (_2pretty_dots_155 != 0)
        goto L19; // [451] 460

        /** 					pretty_out(" ...")*/
        RefDS(_96);
        _2pretty_out(_96);
L19: 

        /** 				pretty_dots = 1*/
        _2pretty_dots_155 = 1;

        /** 				return*/
        DeRef(_a_221);
        DeRef(_sbuff_222);
        DeRef(_46);
        _46 = NOVALUE;
        DeRef(_48);
        _48 = NOVALUE;
        DeRef(_55);
        _55 = NOVALUE;
        DeRef(_57);
        _57 = NOVALUE;
        _69 = NOVALUE;
        DeRef(_74);
        _74 = NOVALUE;
        DeRef(_79);
        _79 = NOVALUE;
        DeRef(_77);
        _77 = NOVALUE;
        DeRef(_84);
        _84 = NOVALUE;
        return;
L18: 

        /** 			if i != length(a) and not all_ascii then*/
        if (IS_SEQUENCE(_a_221)){
                _97 = SEQ_PTR(_a_221)->length;
        }
        else {
            _97 = 1;
        }
        _98 = (_i_291 != _97);
        _97 = NOVALUE;
        if (_98 == 0) {
            goto L1A; // [482] 504
        }
        _100 = (_all_ascii_224 == 0);
        if (_100 == 0)
        {
            DeRef(_100);
            _100 = NOVALUE;
            goto L1A; // [490] 504
        }
        else{
            DeRef(_100);
            _100 = NOVALUE;
        }

        /** 				pretty_out(',')*/
        _2pretty_out(44);

        /** 				cut_line(6)*/
        _2cut_line(6);
L1A: 

        /** 		end for*/
        _i_291 = _i_291 + 1;
        goto L13; // [506] 396
L14: 
        ;
    }

    /** 		pretty_level -= 1*/
    _2pretty_level_147 = _2pretty_level_147 - 1;

    /** 		if multi_line then*/
    if (_multi_line_223 == 0)
    {
        goto L1B; // [523] 531
    }
    else{
    }

    /** 			indent()*/
    _2indent();
L1B: 

    /** 		if all_ascii then*/
    if (_all_ascii_224 == 0)
    {
        goto L1C; // [533] 544
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _2pretty_out(34);
    goto L1D; // [541] 550
L1C: 

    /** 			pretty_out('}')*/
    _2pretty_out(125);
L1D: 
L8: 

    /** end procedure*/
    DeRef(_a_221);
    DeRef(_sbuff_222);
    DeRef(_46);
    _46 = NOVALUE;
    DeRef(_48);
    _48 = NOVALUE;
    DeRef(_55);
    _55 = NOVALUE;
    DeRef(_57);
    _57 = NOVALUE;
    _69 = NOVALUE;
    DeRef(_74);
    _74 = NOVALUE;
    DeRef(_79);
    _79 = NOVALUE;
    DeRef(_77);
    _77 = NOVALUE;
    DeRef(_84);
    _84 = NOVALUE;
    DeRef(_98);
    _98 = NOVALUE;
    return;
    ;
}


void _2pretty(int _x_345, int _options_346)
{
    int _127 = NOVALUE;
    int _126 = NOVALUE;
    int _125 = NOVALUE;
    int _124 = NOVALUE;
    int _122 = NOVALUE;
    int _121 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(options) < length( PRETTY_DEFAULT ) then*/
    if (IS_SEQUENCE(_options_346)){
            _121 = SEQ_PTR(_options_346)->length;
    }
    else {
        _121 = 1;
    }
    _122 = 10;
    if (_121 >= 10)
    goto L1; // [13] 41

    /** 		options &= PRETTY_DEFAULT[length(options)+1..$]*/
    if (IS_SEQUENCE(_options_346)){
            _124 = SEQ_PTR(_options_346)->length;
    }
    else {
        _124 = 1;
    }
    _125 = _124 + 1;
    _124 = NOVALUE;
    _126 = 10;
    rhs_slice_target = (object_ptr)&_127;
    RHS_Slice(_2PRETTY_DEFAULT_321, _125, 10);
    Concat((object_ptr)&_options_346, _options_346, _127);
    DeRefDS(_127);
    _127 = NOVALUE;
L1: 

    /** 	pretty_ascii = options[DISPLAY_ASCII] */
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_ascii_149 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_2pretty_ascii_149))
    _2pretty_ascii_149 = (long)DBL_PTR(_2pretty_ascii_149)->dbl;

    /** 	pretty_indent = options[INDENT]*/
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_indent_150 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_2pretty_indent_150))
    _2pretty_indent_150 = (long)DBL_PTR(_2pretty_indent_150)->dbl;

    /** 	pretty_start_col = options[START_COLUMN]*/
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_start_col_146 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_2pretty_start_col_146))
    _2pretty_start_col_146 = (long)DBL_PTR(_2pretty_start_col_146)->dbl;

    /** 	pretty_end_col = options[WRAP]*/
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_end_col_144 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_2pretty_end_col_144))
    _2pretty_end_col_144 = (long)DBL_PTR(_2pretty_end_col_144)->dbl;

    /** 	pretty_int_format = options[INT_FORMAT]*/
    DeRef(_2pretty_int_format_159);
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_int_format_159 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_2pretty_int_format_159);

    /** 	pretty_fp_format = options[FP_FORMAT]*/
    DeRef(_2pretty_fp_format_158);
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_fp_format_158 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_2pretty_fp_format_158);

    /** 	pretty_ascii_min = options[MIN_ASCII]*/
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_ascii_min_151 = (int)*(((s1_ptr)_2)->base + 7);
    if (!IS_ATOM_INT(_2pretty_ascii_min_151))
    _2pretty_ascii_min_151 = (long)DBL_PTR(_2pretty_ascii_min_151)->dbl;

    /** 	pretty_ascii_max = options[MAX_ASCII]*/
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_ascii_max_152 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_2pretty_ascii_max_152))
    _2pretty_ascii_max_152 = (long)DBL_PTR(_2pretty_ascii_max_152)->dbl;

    /** 	pretty_line_max = options[MAX_LINES]*/
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_line_max_154 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_2pretty_line_max_154))
    _2pretty_line_max_154 = (long)DBL_PTR(_2pretty_line_max_154)->dbl;

    /** 	pretty_line_breaks = options[LINE_BREAKS]*/
    _2 = (int)SEQ_PTR(_options_346);
    _2pretty_line_breaks_156 = (int)*(((s1_ptr)_2)->base + 10);
    if (!IS_ATOM_INT(_2pretty_line_breaks_156))
    _2pretty_line_breaks_156 = (long)DBL_PTR(_2pretty_line_breaks_156)->dbl;

    /** 	pretty_chars = pretty_start_col*/
    _2pretty_chars_145 = _2pretty_start_col_146;

    /** 	pretty_level = 0 */
    _2pretty_level_147 = 0;

    /** 	pretty_line = ""*/
    RefDS(_5);
    DeRef(_2pretty_line_160);
    _2pretty_line_160 = _5;

    /** 	pretty_line_count = 0*/
    _2pretty_line_count_153 = 0;

    /** 	pretty_dots = 0*/
    _2pretty_dots_155 = 0;

    /** 	rPrint(x)*/
    Ref(_x_345);
    _2rPrint(_x_345);

    /** end procedure*/
    DeRef(_x_345);
    DeRefDS(_options_346);
    DeRef(_125);
    _125 = NOVALUE;
    return;
    ;
}


void _2pretty_print(int _fn_368, int _x_369, int _options_370)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fn_368)) {
        _1 = (long)(DBL_PTR(_fn_368)->dbl);
        if (UNIQUE(DBL_PTR(_fn_368)) && (DBL_PTR(_fn_368)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_368);
        _fn_368 = _1;
    }

    /** 	pretty_printing = 1*/
    _2pretty_printing_157 = 1;

    /** 	pretty_file = fn*/
    _2pretty_file_148 = _fn_368;

    /** 	pretty( x, options )*/
    Ref(_x_369);
    RefDS(_options_370);
    _2pretty(_x_369, _options_370);

    /** 	puts(pretty_file, pretty_line)*/
    EPuts(_2pretty_file_148, _2pretty_line_160); // DJP 

    /** end procedure*/
    DeRef(_x_369);
    DeRefDS(_options_370);
    return;
    ;
}


int _2pretty_sprint(int _x_373, int _options_374)
{
    int _0, _1, _2;
    

    /** 	pretty_printing = 0*/
    _2pretty_printing_157 = 0;

    /** 	pretty( x, options )*/
    Ref(_x_373);
    RefDS(_options_374);
    _2pretty(_x_373, _options_374);

    /** 	return pretty_line*/
    RefDS(_2pretty_line_160);
    DeRef(_x_373);
    DeRefDS(_options_374);
    return _2pretty_line_160;
    ;
}



// 0x2D83FCE0
