// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _35flags_to_string(int _flag_bits_17895, int _flag_names_17896, int _expand_flags_17897)
{
    int _s_17898 = NOVALUE;
    int _has_one_bit_17915 = NOVALUE;
    int _which_bit_inlined_which_bit_at_102_17917 = NOVALUE;
    int _current_flag_17921 = NOVALUE;
    int _which_bit_inlined_which_bit_at_247_17947 = NOVALUE;
    int _10063 = NOVALUE;
    int _10061 = NOVALUE;
    int _10060 = NOVALUE;
    int _10058 = NOVALUE;
    int _10056 = NOVALUE;
    int _10055 = NOVALUE;
    int _10051 = NOVALUE;
    int _10050 = NOVALUE;
    int _10047 = NOVALUE;
    int _10046 = NOVALUE;
    int _10042 = NOVALUE;
    int _10041 = NOVALUE;
    int _10040 = NOVALUE;
    int _10039 = NOVALUE;
    int _10038 = NOVALUE;
    int _10037 = NOVALUE;
    int _10036 = NOVALUE;
    int _10034 = NOVALUE;
    int _10033 = NOVALUE;
    int _10032 = NOVALUE;
    int _10031 = NOVALUE;
    int _0, _1, _2;
    

    /**     sequence s = {}*/
    RefDS(_5);
    DeRef(_s_17898);
    _s_17898 = _5;

    /**     if sequence(flag_bits) then*/
    _10031 = IS_SEQUENCE(_flag_bits_17895);
    if (_10031 == 0)
    {
        _10031 = NOVALUE;
        goto L1; // [19] 99
    }
    else{
        _10031 = NOVALUE;
    }

    /** 		for i = 1 to length(flag_bits) do*/
    if (IS_SEQUENCE(_flag_bits_17895)){
            _10032 = SEQ_PTR(_flag_bits_17895)->length;
    }
    else {
        _10032 = 1;
    }
    {
        int _i_17902;
        _i_17902 = 1;
L2: 
        if (_i_17902 > _10032){
            goto L3; // [27] 89
        }

        /** 			if not sequence(flag_bits[i]) then*/
        _2 = (int)SEQ_PTR(_flag_bits_17895);
        _10033 = (int)*(((s1_ptr)_2)->base + _i_17902);
        _10034 = IS_SEQUENCE(_10033);
        _10033 = NOVALUE;
        if (_10034 != 0)
        goto L4; // [43] 71
        _10034 = NOVALUE;

        /** 				flag_bits[i] = flags_to_string(flag_bits[i], flag_names, expand_flags)*/
        _2 = (int)SEQ_PTR(_flag_bits_17895);
        _10036 = (int)*(((s1_ptr)_2)->base + _i_17902);
        RefDS(_flag_names_17896);
        DeRef(_10037);
        _10037 = _flag_names_17896;
        DeRef(_10038);
        _10038 = _expand_flags_17897;
        Ref(_10036);
        _10039 = _35flags_to_string(_10036, _10037, _10038);
        _10036 = NOVALUE;
        _10037 = NOVALUE;
        _10038 = NOVALUE;
        _2 = (int)SEQ_PTR(_flag_bits_17895);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _flag_bits_17895 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_17902);
        _1 = *(int *)_2;
        *(int *)_2 = _10039;
        if( _1 != _10039 ){
            DeRef(_1);
        }
        _10039 = NOVALUE;
        goto L5; // [68] 82
L4: 

        /** 				flag_bits[i] = {"?"}*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_8289);
        *((int *)(_2+4)) = _8289;
        _10040 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_flag_bits_17895);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _flag_bits_17895 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_17902);
        _1 = *(int *)_2;
        *(int *)_2 = _10040;
        if( _1 != _10040 ){
            DeRef(_1);
        }
        _10040 = NOVALUE;
L5: 

        /** 		end for*/
        _i_17902 = _i_17902 + 1;
        goto L2; // [84] 34
L3: 
        ;
    }

    /** 		s = flag_bits*/
    Ref(_flag_bits_17895);
    DeRef(_s_17898);
    _s_17898 = _flag_bits_17895;
    goto L6; // [96] 301
L1: 

    /** 		integer has_one_bit*/

    /** 		has_one_bit = which_bit(flag_bits)*/

    /** 	return map:get(one_bit_numbers, theValue, 0)*/
    Ref(_35one_bit_numbers_17872);
    Ref(_flag_bits_17895);
    _has_one_bit_17915 = _25get(_35one_bit_numbers_17872, _flag_bits_17895, 0);
    if (!IS_ATOM_INT(_has_one_bit_17915)) {
        _1 = (long)(DBL_PTR(_has_one_bit_17915)->dbl);
        if (UNIQUE(DBL_PTR(_has_one_bit_17915)) && (DBL_PTR(_has_one_bit_17915)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_has_one_bit_17915);
        _has_one_bit_17915 = _1;
    }

    /** 		for i = 1 to length(flag_names) do*/
    if (IS_SEQUENCE(_flag_names_17896)){
            _10041 = SEQ_PTR(_flag_names_17896)->length;
    }
    else {
        _10041 = 1;
    }
    {
        int _i_17919;
        _i_17919 = 1;
L7: 
        if (_i_17919 > _10041){
            goto L8; // [123] 298
        }

        /** 			atom current_flag = flag_names[i][1]*/
        _2 = (int)SEQ_PTR(_flag_names_17896);
        _10042 = (int)*(((s1_ptr)_2)->base + _i_17919);
        DeRef(_current_flag_17921);
        _2 = (int)SEQ_PTR(_10042);
        _current_flag_17921 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_current_flag_17921);
        _10042 = NOVALUE;

        /** 			if flag_bits = 0 then*/
        if (binary_op_a(NOTEQ, _flag_bits_17895, 0)){
            goto L9; // [142] 176
        }

        /** 				if current_flag = 0 then*/
        if (binary_op_a(NOTEQ, _current_flag_17921, 0)){
            goto LA; // [148] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_17896);
        _10046 = (int)*(((s1_ptr)_2)->base + _i_17919);
        _2 = (int)SEQ_PTR(_10046);
        _10047 = (int)*(((s1_ptr)_2)->base + 2);
        _10046 = NOVALUE;
        Ref(_10047);
        Append(&_s_17898, _s_17898, _10047);
        _10047 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_17921);
        _current_flag_17921 = NOVALUE;
        goto L8; // [170] 298
        goto LA; // [173] 289
L9: 

        /** 			elsif has_one_bit then*/
        if (_has_one_bit_17915 == 0)
        {
            goto LB; // [178] 211
        }
        else{
        }

        /** 				if current_flag = flag_bits then*/
        if (binary_op_a(NOTEQ, _current_flag_17921, _flag_bits_17895)){
            goto LA; // [183] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_17896);
        _10050 = (int)*(((s1_ptr)_2)->base + _i_17919);
        _2 = (int)SEQ_PTR(_10050);
        _10051 = (int)*(((s1_ptr)_2)->base + 2);
        _10050 = NOVALUE;
        Ref(_10051);
        Append(&_s_17898, _s_17898, _10051);
        _10051 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_17921);
        _current_flag_17921 = NOVALUE;
        goto L8; // [205] 298
        goto LA; // [208] 289
LB: 

        /** 			elsif not expand_flags then*/
        if (_expand_flags_17897 != 0)
        goto LC; // [213] 246

        /** 				if current_flag = flag_bits then*/
        if (binary_op_a(NOTEQ, _current_flag_17921, _flag_bits_17895)){
            goto LA; // [218] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_17896);
        _10055 = (int)*(((s1_ptr)_2)->base + _i_17919);
        _2 = (int)SEQ_PTR(_10055);
        _10056 = (int)*(((s1_ptr)_2)->base + 2);
        _10055 = NOVALUE;
        Ref(_10056);
        Append(&_s_17898, _s_17898, _10056);
        _10056 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_17921);
        _current_flag_17921 = NOVALUE;
        goto L8; // [240] 298
        goto LA; // [243] 289
LC: 

        /** 				if which_bit(current_flag) then*/

        /** 	return map:get(one_bit_numbers, theValue, 0)*/
        Ref(_35one_bit_numbers_17872);
        Ref(_current_flag_17921);
        _0 = _which_bit_inlined_which_bit_at_247_17947;
        _which_bit_inlined_which_bit_at_247_17947 = _25get(_35one_bit_numbers_17872, _current_flag_17921, 0);
        DeRef(_0);
        if (_which_bit_inlined_which_bit_at_247_17947 == 0) {
            goto LD; // [259] 288
        }
        else {
            if (!IS_ATOM_INT(_which_bit_inlined_which_bit_at_247_17947) && DBL_PTR(_which_bit_inlined_which_bit_at_247_17947)->dbl == 0.0){
                goto LD; // [259] 288
            }
        }

        /** 					if and_bits( current_flag, flag_bits ) = current_flag then*/
        if (IS_ATOM_INT(_current_flag_17921) && IS_ATOM_INT(_flag_bits_17895)) {
            {unsigned long tu;
                 tu = (unsigned long)_current_flag_17921 & (unsigned long)_flag_bits_17895;
                 _10058 = MAKE_UINT(tu);
            }
        }
        else {
            _10058 = binary_op(AND_BITS, _current_flag_17921, _flag_bits_17895);
        }
        if (binary_op_a(NOTEQ, _10058, _current_flag_17921)){
            DeRef(_10058);
            _10058 = NOVALUE;
            goto LE; // [268] 287
        }
        DeRef(_10058);
        _10058 = NOVALUE;

        /** 						s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_17896);
        _10060 = (int)*(((s1_ptr)_2)->base + _i_17919);
        _2 = (int)SEQ_PTR(_10060);
        _10061 = (int)*(((s1_ptr)_2)->base + 2);
        _10060 = NOVALUE;
        Ref(_10061);
        Append(&_s_17898, _s_17898, _10061);
        _10061 = NOVALUE;
LE: 
LD: 
LA: 
        DeRef(_current_flag_17921);
        _current_flag_17921 = NOVALUE;

        /** 		end for*/
        _i_17919 = _i_17919 + 1;
        goto L7; // [293] 130
L8: 
        ;
    }
L6: 

    /**     if length(s) = 0 then*/
    if (IS_SEQUENCE(_s_17898)){
            _10063 = SEQ_PTR(_s_17898)->length;
    }
    else {
        _10063 = 1;
    }
    if (_10063 != 0)
    goto LF; // [306] 317

    /** 		s = append(s,"?")*/
    RefDS(_8289);
    Append(&_s_17898, _s_17898, _8289);
LF: 

    /**     return s*/
    DeRef(_flag_bits_17895);
    DeRefDS(_flag_names_17896);
    return _s_17898;
    ;
}



// 0xA80F9869
