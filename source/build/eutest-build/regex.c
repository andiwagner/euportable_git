// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _34regex(int _o_18150)
{
    int _10195 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sequence(o)*/
    _10195 = IS_SEQUENCE(_o_18150);
    DeRef(_o_18150);
    return _10195;
    ;
}


int _34option_spec(int _o_18154)
{
    int _10202 = NOVALUE;
    int _10201 = NOVALUE;
    int _10199 = NOVALUE;
    int _10197 = NOVALUE;
    int _10196 = NOVALUE;
    int _0, _1, _2;
    
L1: 

    /** 	if atom(o) then*/
    _10196 = IS_ATOM(_o_18154);
    if (_10196 == 0)
    {
        _10196 = NOVALUE;
        goto L2; // [6] 60
    }
    else{
        _10196 = NOVALUE;
    }

    /** 		if not integer(o) then*/
    if (IS_ATOM_INT(_o_18154))
    _10197 = 1;
    else if (IS_ATOM_DBL(_o_18154))
    _10197 = IS_ATOM_INT(DoubleToInt(_o_18154));
    else
    _10197 = 0;
    if (_10197 != 0)
    goto L3; // [14] 26
    _10197 = NOVALUE;

    /** 			return 0*/
    DeRef(_o_18154);
    return 0;
    goto L4; // [23] 89
L3: 

    /** 			if (or_bits(o,all_options) != all_options) then*/
    if (IS_ATOM_INT(_o_18154) && IS_ATOM_INT(_34all_options_18145)) {
        {unsigned long tu;
             tu = (unsigned long)_o_18154 | (unsigned long)_34all_options_18145;
             _10199 = MAKE_UINT(tu);
        }
    }
    else {
        _10199 = binary_op(OR_BITS, _o_18154, _34all_options_18145);
    }
    if (binary_op_a(EQUALS, _10199, _34all_options_18145)){
        DeRef(_10199);
        _10199 = NOVALUE;
        goto L5; // [36] 49
    }
    DeRef(_10199);
    _10199 = NOVALUE;

    /** 				return 0*/
    DeRef(_o_18154);
    return 0;
    goto L4; // [46] 89
L5: 

    /** 				return 1*/
    DeRef(_o_18154);
    return 1;
    goto L4; // [57] 89
L2: 

    /** 	elsif integer_array(o) then*/
    Ref(_o_18154);
    _10201 = _5integer_array(_o_18154);
    if (_10201 == 0) {
        DeRef(_10201);
        _10201 = NOVALUE;
        goto L6; // [66] 82
    }
    else {
        if (!IS_ATOM_INT(_10201) && DBL_PTR(_10201)->dbl == 0.0){
            DeRef(_10201);
            _10201 = NOVALUE;
            goto L6; // [66] 82
        }
        DeRef(_10201);
        _10201 = NOVALUE;
    }
    DeRef(_10201);
    _10201 = NOVALUE;

    /** 		return option_spec( math:or_all(o) )*/
    Ref(_o_18154);
    _10202 = _18or_all(_o_18154);
    _0 = _o_18154;
    _o_18154 = _10202;
    Ref(_o_18154);
    DeRef(_0);
    DeRef(_10202);
    _10202 = NOVALUE;
    goto L1; // [75] 1
    goto L4; // [79] 89
L6: 

    /** 		return 0*/
    DeRef(_o_18154);
    return 0;
L4: 
    ;
}


int _34option_spec_to_string(int _o_18173)
{
    int _10204 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return flags:flags_to_string(o, option_names)*/
    Ref(_o_18173);
    RefDS(_34option_names_17996);
    _10204 = _35flags_to_string(_o_18173, _34option_names_17996, 0);
    DeRef(_o_18173);
    return _10204;
    ;
}


int _34error_to_string(int _i_18177)
{
    int _10211 = NOVALUE;
    int _10209 = NOVALUE;
    int _10208 = NOVALUE;
    int _10207 = NOVALUE;
    int _10205 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_i_18177)) {
        _1 = (long)(DBL_PTR(_i_18177)->dbl);
        if (UNIQUE(DBL_PTR(_i_18177)) && (DBL_PTR(_i_18177)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_18177);
        _i_18177 = _1;
    }

    /** 	if i >= 0 or i < -23 then*/
    _10205 = (_i_18177 >= 0);
    if (_10205 != 0) {
        goto L1; // [11] 24
    }
    _10207 = (_i_18177 < -23);
    if (_10207 == 0)
    {
        DeRef(_10207);
        _10207 = NOVALUE;
        goto L2; // [20] 41
    }
    else{
        DeRef(_10207);
        _10207 = NOVALUE;
    }
L1: 

    /** 		return sprintf("%d",{i})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _i_18177;
    _10208 = MAKE_SEQ(_1);
    _10209 = EPrintf(-9999999, _106, _10208);
    DeRefDS(_10208);
    _10208 = NOVALUE;
    DeRef(_10205);
    _10205 = NOVALUE;
    return _10209;
    goto L3; // [38] 58
L2: 

    /** 		return search:vlookup(i, error_names, 1, 2, "Unknown Error")*/
    RefDS(_34error_names_18096);
    RefDS(_10210);
    _10211 = _6vlookup(_i_18177, _34error_names_18096, 1, 2, _10210);
    DeRef(_10205);
    _10205 = NOVALUE;
    DeRef(_10209);
    _10209 = NOVALUE;
    return _10211;
L3: 
    ;
}


int _34new(int _pattern_18190, int _options_18191)
{
    int _10215 = NOVALUE;
    int _10214 = NOVALUE;
    int _10212 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _10212 = IS_SEQUENCE(_options_18191);
    if (_10212 == 0)
    {
        _10212 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _10212 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18191);
    _0 = _options_18191;
    _options_18191 = _18or_all(_options_18191);
    DeRef(_0);
L1: 

    /** 	return machine_func(M_PCRE_COMPILE, { pattern, options })*/
    Ref(_options_18191);
    Ref(_pattern_18190);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pattern_18190;
    ((int *)_2)[2] = _options_18191;
    _10214 = MAKE_SEQ(_1);
    _10215 = machine(68, _10214);
    DeRefDS(_10214);
    _10214 = NOVALUE;
    DeRef(_pattern_18190);
    DeRef(_options_18191);
    return _10215;
    ;
}


int _34error_message(int _re_18199)
{
    int _10217 = NOVALUE;
    int _10216 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_PCRE_ERROR_MESSAGE, { re })*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_18199);
    *((int *)(_2+4)) = _re_18199;
    _10216 = MAKE_SEQ(_1);
    _10217 = machine(95, _10216);
    DeRefDS(_10216);
    _10216 = NOVALUE;
    DeRef(_re_18199);
    return _10217;
    ;
}


int _34escape(int _s_18205)
{
    int _10219 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return text:escape(s, ".\\+*?[^]$(){}=!<>|:-")*/
    Ref(_s_18205);
    RefDS(_10218);
    _10219 = _16escape(_s_18205, _10218);
    DeRef(_s_18205);
    return _10219;
    ;
}


int _34get_ovector_size(int _ex_18210, int _maxsize_18211)
{
    int _m_18212 = NOVALUE;
    int _10223 = NOVALUE;
    int _10220 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_maxsize_18211)) {
        _1 = (long)(DBL_PTR(_maxsize_18211)->dbl);
        if (UNIQUE(DBL_PTR(_maxsize_18211)) && (DBL_PTR(_maxsize_18211)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxsize_18211);
        _maxsize_18211 = _1;
    }

    /** 	integer m = machine_func(M_PCRE_GET_OVECTOR_SIZE, {ex})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_18210);
    *((int *)(_2+4)) = _ex_18210;
    _10220 = MAKE_SEQ(_1);
    _m_18212 = machine(97, _10220);
    DeRefDS(_10220);
    _10220 = NOVALUE;
    if (!IS_ATOM_INT(_m_18212)) {
        _1 = (long)(DBL_PTR(_m_18212)->dbl);
        if (UNIQUE(DBL_PTR(_m_18212)) && (DBL_PTR(_m_18212)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_m_18212);
        _m_18212 = _1;
    }

    /** 	if (m > maxsize) then*/
    if (_m_18212 <= _maxsize_18211)
    goto L1; // [23] 34

    /** 		return maxsize*/
    DeRef(_ex_18210);
    return _maxsize_18211;
L1: 

    /** 	return m+1*/
    _10223 = _m_18212 + 1;
    if (_10223 > MAXINT){
        _10223 = NewDouble((double)_10223);
    }
    DeRef(_ex_18210);
    return _10223;
    ;
}


int _34find(int _re_18220, int _haystack_18222, int _from_18223, int _options_18224, int _size_18225)
{
    int _10230 = NOVALUE;
    int _10229 = NOVALUE;
    int _10228 = NOVALUE;
    int _10225 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18223)) {
        _1 = (long)(DBL_PTR(_from_18223)->dbl);
        if (UNIQUE(DBL_PTR(_from_18223)) && (DBL_PTR(_from_18223)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18223);
        _from_18223 = _1;
    }
    if (!IS_ATOM_INT(_size_18225)) {
        _1 = (long)(DBL_PTR(_size_18225)->dbl);
        if (UNIQUE(DBL_PTR(_size_18225)) && (DBL_PTR(_size_18225)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_18225);
        _size_18225 = _1;
    }

    /** 	if sequence(options) then */
    _10225 = IS_SEQUENCE(_options_18224);
    if (_10225 == 0)
    {
        _10225 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _10225 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18224);
    _0 = _options_18224;
    _options_18224 = _18or_all(_options_18224);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_18225 >= 0)
    goto L2; // [26] 38

    /** 		size = 0*/
    _size_18225 = 0;
L2: 

    /** 	return machine_func(M_PCRE_EXEC, { re, haystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_18222)){
            _10228 = SEQ_PTR(_haystack_18222)->length;
    }
    else {
        _10228 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_18220);
    *((int *)(_2+4)) = _re_18220;
    Ref(_haystack_18222);
    *((int *)(_2+8)) = _haystack_18222;
    *((int *)(_2+12)) = _10228;
    Ref(_options_18224);
    *((int *)(_2+16)) = _options_18224;
    *((int *)(_2+20)) = _from_18223;
    *((int *)(_2+24)) = _size_18225;
    _10229 = MAKE_SEQ(_1);
    _10228 = NOVALUE;
    _10230 = machine(70, _10229);
    DeRefDS(_10229);
    _10229 = NOVALUE;
    DeRef(_re_18220);
    DeRef(_haystack_18222);
    DeRef(_options_18224);
    return _10230;
    ;
}


int _34find_all(int _re_18237, int _haystack_18239, int _from_18240, int _options_18241, int _size_18242)
{
    int _result_18249 = NOVALUE;
    int _results_18250 = NOVALUE;
    int _pHaystack_18251 = NOVALUE;
    int _10243 = NOVALUE;
    int _10242 = NOVALUE;
    int _10240 = NOVALUE;
    int _10238 = NOVALUE;
    int _10236 = NOVALUE;
    int _10232 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18240)) {
        _1 = (long)(DBL_PTR(_from_18240)->dbl);
        if (UNIQUE(DBL_PTR(_from_18240)) && (DBL_PTR(_from_18240)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18240);
        _from_18240 = _1;
    }
    if (!IS_ATOM_INT(_size_18242)) {
        _1 = (long)(DBL_PTR(_size_18242)->dbl);
        if (UNIQUE(DBL_PTR(_size_18242)) && (DBL_PTR(_size_18242)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_18242);
        _size_18242 = _1;
    }

    /** 	if sequence(options) then */
    _10232 = IS_SEQUENCE(_options_18241);
    if (_10232 == 0)
    {
        _10232 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _10232 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18241);
    _0 = _options_18241;
    _options_18241 = _18or_all(_options_18241);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_18242 >= 0)
    goto L2; // [26] 38

    /** 		size = 0*/
    _size_18242 = 0;
L2: 

    /** 	object result*/

    /** 	sequence results = {}*/
    RefDS(_5);
    DeRef(_results_18250);
    _results_18250 = _5;

    /** 	atom pHaystack = machine:allocate_string(haystack)*/
    Ref(_haystack_18239);
    _0 = _pHaystack_18251;
    _pHaystack_18251 = _11allocate_string(_haystack_18239, 0);
    DeRef(_0);

    /** 	while sequence(result) with entry do*/
    goto L3; // [56] 102
L4: 
    _10236 = IS_SEQUENCE(_result_18249);
    if (_10236 == 0)
    {
        _10236 = NOVALUE;
        goto L5; // [64] 127
    }
    else{
        _10236 = NOVALUE;
    }

    /** 		results = append(results, result)*/
    Ref(_result_18249);
    Append(&_results_18250, _results_18250, _result_18249);

    /** 		from = math:max(result) + 1*/
    Ref(_result_18249);
    _10238 = _18max(_result_18249);
    if (IS_ATOM_INT(_10238)) {
        _from_18240 = _10238 + 1;
    }
    else
    { // coercing _from_18240 to an integer 1
        _from_18240 = 1+(long)(DBL_PTR(_10238)->dbl);
        if( !IS_ATOM_INT(_from_18240) ){
            _from_18240 = (object)DBL_PTR(_from_18240)->dbl;
        }
    }
    DeRef(_10238);
    _10238 = NOVALUE;

    /** 		if from > length(haystack) then*/
    if (IS_SEQUENCE(_haystack_18239)){
            _10240 = SEQ_PTR(_haystack_18239)->length;
    }
    else {
        _10240 = 1;
    }
    if (_from_18240 <= _10240)
    goto L6; // [90] 99

    /** 			exit*/
    goto L5; // [96] 127
L6: 

    /** 	entry*/
L3: 

    /** 		result = machine_func(M_PCRE_EXEC, { re, pHaystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_18239)){
            _10242 = SEQ_PTR(_haystack_18239)->length;
    }
    else {
        _10242 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_18237);
    *((int *)(_2+4)) = _re_18237;
    Ref(_pHaystack_18251);
    *((int *)(_2+8)) = _pHaystack_18251;
    *((int *)(_2+12)) = _10242;
    Ref(_options_18241);
    *((int *)(_2+16)) = _options_18241;
    *((int *)(_2+20)) = _from_18240;
    *((int *)(_2+24)) = _size_18242;
    _10243 = MAKE_SEQ(_1);
    _10242 = NOVALUE;
    DeRef(_result_18249);
    _result_18249 = machine(70, _10243);
    DeRefDS(_10243);
    _10243 = NOVALUE;

    /** 	end while*/
    goto L4; // [124] 59
L5: 

    /** 	machine:free(pHaystack)*/
    Ref(_pHaystack_18251);
    _11free(_pHaystack_18251);

    /** 	return results*/
    DeRef(_re_18237);
    DeRef(_haystack_18239);
    DeRef(_options_18241);
    DeRef(_result_18249);
    DeRef(_pHaystack_18251);
    return _results_18250;
    ;
}


int _34has_match(int _re_18266, int _haystack_18268, int _from_18269, int _options_18270)
{
    int _10247 = NOVALUE;
    int _10246 = NOVALUE;
    int _10245 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18269)) {
        _1 = (long)(DBL_PTR(_from_18269)->dbl);
        if (UNIQUE(DBL_PTR(_from_18269)) && (DBL_PTR(_from_18269)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18269);
        _from_18269 = _1;
    }

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_re_18266);
    _10245 = _34get_ovector_size(_re_18266, 30);
    Ref(_re_18266);
    Ref(_haystack_18268);
    Ref(_options_18270);
    _10246 = _34find(_re_18266, _haystack_18268, _from_18269, _options_18270, _10245);
    _10245 = NOVALUE;
    _10247 = IS_SEQUENCE(_10246);
    DeRef(_10246);
    _10246 = NOVALUE;
    DeRef(_re_18266);
    DeRef(_haystack_18268);
    DeRef(_options_18270);
    return _10247;
    ;
}


int _34is_match(int _re_18276, int _haystack_18278, int _from_18279, int _options_18280)
{
    int _m_18281 = NOVALUE;
    int _10262 = NOVALUE;
    int _10261 = NOVALUE;
    int _10260 = NOVALUE;
    int _10259 = NOVALUE;
    int _10258 = NOVALUE;
    int _10257 = NOVALUE;
    int _10256 = NOVALUE;
    int _10255 = NOVALUE;
    int _10254 = NOVALUE;
    int _10253 = NOVALUE;
    int _10252 = NOVALUE;
    int _10251 = NOVALUE;
    int _10250 = NOVALUE;
    int _10248 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18279)) {
        _1 = (long)(DBL_PTR(_from_18279)->dbl);
        if (UNIQUE(DBL_PTR(_from_18279)) && (DBL_PTR(_from_18279)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18279);
        _from_18279 = _1;
    }

    /** 	object m = find(re, haystack, from, options)*/
    Ref(_re_18276);
    _10248 = _34get_ovector_size(_re_18276, 30);
    Ref(_re_18276);
    Ref(_haystack_18278);
    Ref(_options_18280);
    _0 = _m_18281;
    _m_18281 = _34find(_re_18276, _haystack_18278, _from_18279, _options_18280, _10248);
    DeRef(_0);
    _10248 = NOVALUE;

    /** 	if sequence(m) and length(m) > 0 and m[1][1] = from and m[1][2] = length(haystack) then*/
    _10250 = IS_SEQUENCE(_m_18281);
    if (_10250 == 0) {
        _10251 = 0;
        goto L1; // [25] 40
    }
    if (IS_SEQUENCE(_m_18281)){
            _10252 = SEQ_PTR(_m_18281)->length;
    }
    else {
        _10252 = 1;
    }
    _10253 = (_10252 > 0);
    _10252 = NOVALUE;
    _10251 = (_10253 != 0);
L1: 
    if (_10251 == 0) {
        _10254 = 0;
        goto L2; // [40] 60
    }
    _2 = (int)SEQ_PTR(_m_18281);
    _10255 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_10255);
    _10256 = (int)*(((s1_ptr)_2)->base + 1);
    _10255 = NOVALUE;
    if (IS_ATOM_INT(_10256)) {
        _10257 = (_10256 == _from_18279);
    }
    else {
        _10257 = binary_op(EQUALS, _10256, _from_18279);
    }
    _10256 = NOVALUE;
    if (IS_ATOM_INT(_10257))
    _10254 = (_10257 != 0);
    else
    _10254 = DBL_PTR(_10257)->dbl != 0.0;
L2: 
    if (_10254 == 0) {
        goto L3; // [60] 90
    }
    _2 = (int)SEQ_PTR(_m_18281);
    _10259 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_10259);
    _10260 = (int)*(((s1_ptr)_2)->base + 2);
    _10259 = NOVALUE;
    if (IS_SEQUENCE(_haystack_18278)){
            _10261 = SEQ_PTR(_haystack_18278)->length;
    }
    else {
        _10261 = 1;
    }
    if (IS_ATOM_INT(_10260)) {
        _10262 = (_10260 == _10261);
    }
    else {
        _10262 = binary_op(EQUALS, _10260, _10261);
    }
    _10260 = NOVALUE;
    _10261 = NOVALUE;
    if (_10262 == 0) {
        DeRef(_10262);
        _10262 = NOVALUE;
        goto L3; // [80] 90
    }
    else {
        if (!IS_ATOM_INT(_10262) && DBL_PTR(_10262)->dbl == 0.0){
            DeRef(_10262);
            _10262 = NOVALUE;
            goto L3; // [80] 90
        }
        DeRef(_10262);
        _10262 = NOVALUE;
    }
    DeRef(_10262);
    _10262 = NOVALUE;

    /** 		return 1*/
    DeRef(_re_18276);
    DeRef(_haystack_18278);
    DeRef(_options_18280);
    DeRef(_m_18281);
    DeRef(_10253);
    _10253 = NOVALUE;
    DeRef(_10257);
    _10257 = NOVALUE;
    return 1;
L3: 

    /** 	return 0*/
    DeRef(_re_18276);
    DeRef(_haystack_18278);
    DeRef(_options_18280);
    DeRef(_m_18281);
    DeRef(_10253);
    _10253 = NOVALUE;
    DeRef(_10257);
    _10257 = NOVALUE;
    return 0;
    ;
}


int _34matches(int _re_18300, int _haystack_18302, int _from_18303, int _options_18304)
{
    int _str_offsets_18308 = NOVALUE;
    int _match_data_18310 = NOVALUE;
    int _tmp_18320 = NOVALUE;
    int _10284 = NOVALUE;
    int _10283 = NOVALUE;
    int _10282 = NOVALUE;
    int _10281 = NOVALUE;
    int _10280 = NOVALUE;
    int _10278 = NOVALUE;
    int _10277 = NOVALUE;
    int _10276 = NOVALUE;
    int _10275 = NOVALUE;
    int _10273 = NOVALUE;
    int _10272 = NOVALUE;
    int _10271 = NOVALUE;
    int _10270 = NOVALUE;
    int _10268 = NOVALUE;
    int _10267 = NOVALUE;
    int _10266 = NOVALUE;
    int _10263 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18303)) {
        _1 = (long)(DBL_PTR(_from_18303)->dbl);
        if (UNIQUE(DBL_PTR(_from_18303)) && (DBL_PTR(_from_18303)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18303);
        _from_18303 = _1;
    }

    /** 	if sequence(options) then */
    _10263 = IS_SEQUENCE(_options_18304);
    if (_10263 == 0)
    {
        _10263 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _10263 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18304);
    _0 = _options_18304;
    _options_18304 = _18or_all(_options_18304);
    DeRef(_0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_18304)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_18304;
             _str_offsets_18308 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_18308 = binary_op(AND_BITS, 201326592, _options_18304);
    }
    if (!IS_ATOM_INT(_str_offsets_18308)) {
        _1 = (long)(DBL_PTR(_str_offsets_18308)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_18308)) && (DBL_PTR(_str_offsets_18308)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_18308);
        _str_offsets_18308 = _1;
    }

    /** 	object match_data = find(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _10266 = not_bits(201326592);
    if (IS_ATOM_INT(_options_18304) && IS_ATOM_INT(_10266)) {
        {unsigned long tu;
             tu = (unsigned long)_options_18304 & (unsigned long)_10266;
             _10267 = MAKE_UINT(tu);
        }
    }
    else {
        _10267 = binary_op(AND_BITS, _options_18304, _10266);
    }
    DeRef(_10266);
    _10266 = NOVALUE;
    Ref(_re_18300);
    _10268 = _34get_ovector_size(_re_18300, 30);
    Ref(_re_18300);
    Ref(_haystack_18302);
    _0 = _match_data_18310;
    _match_data_18310 = _34find(_re_18300, _haystack_18302, _from_18303, _10267, _10268);
    DeRef(_0);
    _10267 = NOVALUE;
    _10268 = NOVALUE;

    /** 	if atom(match_data) then */
    _10270 = IS_ATOM(_match_data_18310);
    if (_10270 == 0)
    {
        _10270 = NOVALUE;
        goto L2; // [57] 67
    }
    else{
        _10270 = NOVALUE;
    }

    /** 		return ERROR_NOMATCH */
    DeRef(_re_18300);
    DeRef(_haystack_18302);
    DeRef(_options_18304);
    DeRef(_match_data_18310);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_18310)){
            _10271 = SEQ_PTR(_match_data_18310)->length;
    }
    else {
        _10271 = 1;
    }
    {
        int _i_18318;
        _i_18318 = 1;
L3: 
        if (_i_18318 > _10271){
            goto L4; // [72] 185
        }

        /** 		sequence tmp*/

        /** 		if match_data[i][1] = 0 then*/
        _2 = (int)SEQ_PTR(_match_data_18310);
        _10272 = (int)*(((s1_ptr)_2)->base + _i_18318);
        _2 = (int)SEQ_PTR(_10272);
        _10273 = (int)*(((s1_ptr)_2)->base + 1);
        _10272 = NOVALUE;
        if (binary_op_a(NOTEQ, _10273, 0)){
            _10273 = NOVALUE;
            goto L5; // [91] 105
        }
        _10273 = NOVALUE;

        /** 			tmp = ""*/
        RefDS(_5);
        DeRef(_tmp_18320);
        _tmp_18320 = _5;
        goto L6; // [102] 129
L5: 

        /** 			tmp = haystack[match_data[i][1]..match_data[i][2]]*/
        _2 = (int)SEQ_PTR(_match_data_18310);
        _10275 = (int)*(((s1_ptr)_2)->base + _i_18318);
        _2 = (int)SEQ_PTR(_10275);
        _10276 = (int)*(((s1_ptr)_2)->base + 1);
        _10275 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18310);
        _10277 = (int)*(((s1_ptr)_2)->base + _i_18318);
        _2 = (int)SEQ_PTR(_10277);
        _10278 = (int)*(((s1_ptr)_2)->base + 2);
        _10277 = NOVALUE;
        rhs_slice_target = (object_ptr)&_tmp_18320;
        RHS_Slice(_haystack_18302, _10276, _10278);
L6: 

        /** 		if str_offsets then*/
        if (_str_offsets_18308 == 0)
        {
            goto L7; // [131] 167
        }
        else{
        }

        /** 			match_data[i] = { tmp, match_data[i][1], match_data[i][2] }*/
        _2 = (int)SEQ_PTR(_match_data_18310);
        _10280 = (int)*(((s1_ptr)_2)->base + _i_18318);
        _2 = (int)SEQ_PTR(_10280);
        _10281 = (int)*(((s1_ptr)_2)->base + 1);
        _10280 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18310);
        _10282 = (int)*(((s1_ptr)_2)->base + _i_18318);
        _2 = (int)SEQ_PTR(_10282);
        _10283 = (int)*(((s1_ptr)_2)->base + 2);
        _10282 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_tmp_18320);
        *((int *)(_2+4)) = _tmp_18320;
        Ref(_10281);
        *((int *)(_2+8)) = _10281;
        Ref(_10283);
        *((int *)(_2+12)) = _10283;
        _10284 = MAKE_SEQ(_1);
        _10283 = NOVALUE;
        _10281 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18310);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_18310 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18318);
        _1 = *(int *)_2;
        *(int *)_2 = _10284;
        if( _1 != _10284 ){
            DeRef(_1);
        }
        _10284 = NOVALUE;
        goto L8; // [164] 176
L7: 

        /** 			match_data[i] = tmp*/
        RefDS(_tmp_18320);
        _2 = (int)SEQ_PTR(_match_data_18310);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_18310 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18318);
        _1 = *(int *)_2;
        *(int *)_2 = _tmp_18320;
        DeRef(_1);
L8: 
        DeRef(_tmp_18320);
        _tmp_18320 = NOVALUE;

        /** 	end for*/
        _i_18318 = _i_18318 + 1;
        goto L3; // [180] 79
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_18300);
    DeRef(_haystack_18302);
    DeRef(_options_18304);
    _10276 = NOVALUE;
    _10278 = NOVALUE;
    return _match_data_18310;
    ;
}


int _34all_matches(int _re_18340, int _haystack_18342, int _from_18343, int _options_18344)
{
    int _str_offsets_18348 = NOVALUE;
    int _match_data_18350 = NOVALUE;
    int _tmp_18365 = NOVALUE;
    int _a_18366 = NOVALUE;
    int _b_18367 = NOVALUE;
    int _10308 = NOVALUE;
    int _10307 = NOVALUE;
    int _10305 = NOVALUE;
    int _10302 = NOVALUE;
    int _10301 = NOVALUE;
    int _10298 = NOVALUE;
    int _10297 = NOVALUE;
    int _10296 = NOVALUE;
    int _10295 = NOVALUE;
    int _10294 = NOVALUE;
    int _10292 = NOVALUE;
    int _10290 = NOVALUE;
    int _10289 = NOVALUE;
    int _10288 = NOVALUE;
    int _10285 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_from_18343)) {
        _1 = (long)(DBL_PTR(_from_18343)->dbl);
        if (UNIQUE(DBL_PTR(_from_18343)) && (DBL_PTR(_from_18343)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18343);
        _from_18343 = _1;
    }

    /** 	if sequence(options) then */
    _10285 = IS_SEQUENCE(_options_18344);
    if (_10285 == 0)
    {
        _10285 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _10285 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18344);
    _0 = _options_18344;
    _options_18344 = _18or_all(_options_18344);
    DeRef(_0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_18344)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_18344;
             _str_offsets_18348 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_18348 = binary_op(AND_BITS, 201326592, _options_18344);
    }
    if (!IS_ATOM_INT(_str_offsets_18348)) {
        _1 = (long)(DBL_PTR(_str_offsets_18348)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_18348)) && (DBL_PTR(_str_offsets_18348)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_18348);
        _str_offsets_18348 = _1;
    }

    /** 	object match_data = find_all(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _10288 = not_bits(201326592);
    if (IS_ATOM_INT(_options_18344) && IS_ATOM_INT(_10288)) {
        {unsigned long tu;
             tu = (unsigned long)_options_18344 & (unsigned long)_10288;
             _10289 = MAKE_UINT(tu);
        }
    }
    else {
        _10289 = binary_op(AND_BITS, _options_18344, _10288);
    }
    DeRef(_10288);
    _10288 = NOVALUE;
    Ref(_re_18340);
    _10290 = _34get_ovector_size(_re_18340, 30);
    Ref(_re_18340);
    Ref(_haystack_18342);
    _0 = _match_data_18350;
    _match_data_18350 = _34find_all(_re_18340, _haystack_18342, _from_18343, _10289, _10290);
    DeRef(_0);
    _10289 = NOVALUE;
    _10290 = NOVALUE;

    /** 	if length(match_data) = 0 then */
    if (IS_SEQUENCE(_match_data_18350)){
            _10292 = SEQ_PTR(_match_data_18350)->length;
    }
    else {
        _10292 = 1;
    }
    if (_10292 != 0)
    goto L2; // [57] 68

    /** 		return ERROR_NOMATCH */
    DeRef(_re_18340);
    DeRef(_haystack_18342);
    DeRef(_options_18344);
    DeRef(_match_data_18350);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_18350)){
            _10294 = SEQ_PTR(_match_data_18350)->length;
    }
    else {
        _10294 = 1;
    }
    {
        int _i_18359;
        _i_18359 = 1;
L3: 
        if (_i_18359 > _10294){
            goto L4; // [73] 219
        }

        /** 		for j = 1 to length(match_data[i]) do*/
        _2 = (int)SEQ_PTR(_match_data_18350);
        _10295 = (int)*(((s1_ptr)_2)->base + _i_18359);
        if (IS_SEQUENCE(_10295)){
                _10296 = SEQ_PTR(_10295)->length;
        }
        else {
            _10296 = 1;
        }
        _10295 = NOVALUE;
        {
            int _j_18362;
            _j_18362 = 1;
L5: 
            if (_j_18362 > _10296){
                goto L6; // [89] 212
            }

            /** 			sequence tmp*/

            /** 			integer a,b*/

            /** 			a = match_data[i][j][1]*/
            _2 = (int)SEQ_PTR(_match_data_18350);
            _10297 = (int)*(((s1_ptr)_2)->base + _i_18359);
            _2 = (int)SEQ_PTR(_10297);
            _10298 = (int)*(((s1_ptr)_2)->base + _j_18362);
            _10297 = NOVALUE;
            _2 = (int)SEQ_PTR(_10298);
            _a_18366 = (int)*(((s1_ptr)_2)->base + 1);
            if (!IS_ATOM_INT(_a_18366)){
                _a_18366 = (long)DBL_PTR(_a_18366)->dbl;
            }
            _10298 = NOVALUE;

            /** 			if a = 0 then*/
            if (_a_18366 != 0)
            goto L7; // [120] 134

            /** 				tmp = ""*/
            RefDS(_5);
            DeRef(_tmp_18365);
            _tmp_18365 = _5;
            goto L8; // [131] 160
L7: 

            /** 				b = match_data[i][j][2]*/
            _2 = (int)SEQ_PTR(_match_data_18350);
            _10301 = (int)*(((s1_ptr)_2)->base + _i_18359);
            _2 = (int)SEQ_PTR(_10301);
            _10302 = (int)*(((s1_ptr)_2)->base + _j_18362);
            _10301 = NOVALUE;
            _2 = (int)SEQ_PTR(_10302);
            _b_18367 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_b_18367)){
                _b_18367 = (long)DBL_PTR(_b_18367)->dbl;
            }
            _10302 = NOVALUE;

            /** 				tmp = haystack[a..b]*/
            rhs_slice_target = (object_ptr)&_tmp_18365;
            RHS_Slice(_haystack_18342, _a_18366, _b_18367);
L8: 

            /** 			if str_offsets then*/
            if (_str_offsets_18348 == 0)
            {
                goto L9; // [162] 189
            }
            else{
            }

            /** 				match_data[i][j] = { tmp, a, b }*/
            _2 = (int)SEQ_PTR(_match_data_18350);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _match_data_18350 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_18359 + ((s1_ptr)_2)->base);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            RefDS(_tmp_18365);
            *((int *)(_2+4)) = _tmp_18365;
            *((int *)(_2+8)) = _a_18366;
            *((int *)(_2+12)) = _b_18367;
            _10307 = MAKE_SEQ(_1);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18362);
            _1 = *(int *)_2;
            *(int *)_2 = _10307;
            if( _1 != _10307 ){
                DeRef(_1);
            }
            _10307 = NOVALUE;
            _10305 = NOVALUE;
            goto LA; // [186] 203
L9: 

            /** 				match_data[i][j] = tmp*/
            _2 = (int)SEQ_PTR(_match_data_18350);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _match_data_18350 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_18359 + ((s1_ptr)_2)->base);
            RefDS(_tmp_18365);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18362);
            _1 = *(int *)_2;
            *(int *)_2 = _tmp_18365;
            DeRef(_1);
            _10308 = NOVALUE;
LA: 
            DeRef(_tmp_18365);
            _tmp_18365 = NOVALUE;

            /** 		end for*/
            _j_18362 = _j_18362 + 1;
            goto L5; // [207] 96
L6: 
            ;
        }

        /** 	end for*/
        _i_18359 = _i_18359 + 1;
        goto L3; // [214] 80
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_18340);
    DeRef(_haystack_18342);
    DeRef(_options_18344);
    _10295 = NOVALUE;
    return _match_data_18350;
    ;
}


int _34split(int _re_18387, int _text_18389, int _from_18390, int _options_18391)
{
    int _10310 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18390)) {
        _1 = (long)(DBL_PTR(_from_18390)->dbl);
        if (UNIQUE(DBL_PTR(_from_18390)) && (DBL_PTR(_from_18390)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18390);
        _from_18390 = _1;
    }

    /** 	return split_limit(re, text, 0, from, options)*/
    Ref(_re_18387);
    Ref(_text_18389);
    Ref(_options_18391);
    _10310 = _34split_limit(_re_18387, _text_18389, 0, _from_18390, _options_18391);
    DeRef(_re_18387);
    DeRef(_text_18389);
    DeRef(_options_18391);
    return _10310;
    ;
}


int _34split_limit(int _re_18396, int _text_18398, int _limit_18399, int _from_18400, int _options_18401)
{
    int _match_data_18405 = NOVALUE;
    int _result_18408 = NOVALUE;
    int _last_18409 = NOVALUE;
    int _a_18420 = NOVALUE;
    int _10336 = NOVALUE;
    int _10335 = NOVALUE;
    int _10334 = NOVALUE;
    int _10332 = NOVALUE;
    int _10330 = NOVALUE;
    int _10329 = NOVALUE;
    int _10328 = NOVALUE;
    int _10327 = NOVALUE;
    int _10326 = NOVALUE;
    int _10323 = NOVALUE;
    int _10322 = NOVALUE;
    int _10321 = NOVALUE;
    int _10318 = NOVALUE;
    int _10317 = NOVALUE;
    int _10315 = NOVALUE;
    int _10313 = NOVALUE;
    int _10311 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_18399)) {
        _1 = (long)(DBL_PTR(_limit_18399)->dbl);
        if (UNIQUE(DBL_PTR(_limit_18399)) && (DBL_PTR(_limit_18399)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_18399);
        _limit_18399 = _1;
    }
    if (!IS_ATOM_INT(_from_18400)) {
        _1 = (long)(DBL_PTR(_from_18400)->dbl);
        if (UNIQUE(DBL_PTR(_from_18400)) && (DBL_PTR(_from_18400)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18400);
        _from_18400 = _1;
    }

    /** 	if sequence(options) then */
    _10311 = IS_SEQUENCE(_options_18401);
    if (_10311 == 0)
    {
        _10311 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _10311 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18401);
    _0 = _options_18401;
    _options_18401 = _18or_all(_options_18401);
    DeRef(_0);
L1: 

    /** 	sequence match_data = find_all(re, text, from, options), result*/
    Ref(_re_18396);
    _10313 = _34get_ovector_size(_re_18396, 30);
    Ref(_re_18396);
    Ref(_text_18398);
    Ref(_options_18401);
    _0 = _match_data_18405;
    _match_data_18405 = _34find_all(_re_18396, _text_18398, _from_18400, _options_18401, _10313);
    DeRef(_0);
    _10313 = NOVALUE;

    /** 	integer last = 1*/
    _last_18409 = 1;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _10315 = (_limit_18399 == 0);
    if (_10315 != 0) {
        goto L2; // [54] 70
    }
    if (IS_SEQUENCE(_match_data_18405)){
            _10317 = SEQ_PTR(_match_data_18405)->length;
    }
    else {
        _10317 = 1;
    }
    _10318 = (_limit_18399 > _10317);
    _10317 = NOVALUE;
    if (_10318 == 0)
    {
        DeRef(_10318);
        _10318 = NOVALUE;
        goto L3; // [66] 78
    }
    else{
        DeRef(_10318);
        _10318 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_18405)){
            _limit_18399 = SEQ_PTR(_match_data_18405)->length;
    }
    else {
        _limit_18399 = 1;
    }
L3: 

    /** 	result = repeat(0, limit)*/
    DeRef(_result_18408);
    _result_18408 = Repeat(0, _limit_18399);

    /** 	for i = 1 to limit do*/
    _10321 = _limit_18399;
    {
        int _i_18418;
        _i_18418 = 1;
L4: 
        if (_i_18418 > _10321){
            goto L5; // [89] 176
        }

        /** 		integer a*/

        /** 		a = match_data[i][1][1]*/
        _2 = (int)SEQ_PTR(_match_data_18405);
        _10322 = (int)*(((s1_ptr)_2)->base + _i_18418);
        _2 = (int)SEQ_PTR(_10322);
        _10323 = (int)*(((s1_ptr)_2)->base + 1);
        _10322 = NOVALUE;
        _2 = (int)SEQ_PTR(_10323);
        _a_18420 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_a_18420)){
            _a_18420 = (long)DBL_PTR(_a_18420)->dbl;
        }
        _10323 = NOVALUE;

        /** 		if a = 0 then*/
        if (_a_18420 != 0)
        goto L6; // [118] 131

        /** 			result[i] = ""*/
        RefDS(_5);
        _2 = (int)SEQ_PTR(_result_18408);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_18408 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18418);
        _1 = *(int *)_2;
        *(int *)_2 = _5;
        DeRef(_1);
        goto L7; // [128] 167
L6: 

        /** 			result[i] = text[last..a - 1]*/
        _10326 = _a_18420 - 1;
        rhs_slice_target = (object_ptr)&_10327;
        RHS_Slice(_text_18398, _last_18409, _10326);
        _2 = (int)SEQ_PTR(_result_18408);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_18408 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18418);
        _1 = *(int *)_2;
        *(int *)_2 = _10327;
        if( _1 != _10327 ){
            DeRef(_1);
        }
        _10327 = NOVALUE;

        /** 			last = match_data[i][1][2] + 1*/
        _2 = (int)SEQ_PTR(_match_data_18405);
        _10328 = (int)*(((s1_ptr)_2)->base + _i_18418);
        _2 = (int)SEQ_PTR(_10328);
        _10329 = (int)*(((s1_ptr)_2)->base + 1);
        _10328 = NOVALUE;
        _2 = (int)SEQ_PTR(_10329);
        _10330 = (int)*(((s1_ptr)_2)->base + 2);
        _10329 = NOVALUE;
        if (IS_ATOM_INT(_10330)) {
            _last_18409 = _10330 + 1;
        }
        else
        { // coercing _last_18409 to an integer 1
            _last_18409 = 1+(long)(DBL_PTR(_10330)->dbl);
            if( !IS_ATOM_INT(_last_18409) ){
                _last_18409 = (object)DBL_PTR(_last_18409)->dbl;
            }
        }
        _10330 = NOVALUE;
L7: 

        /** 	end for*/
        _i_18418 = _i_18418 + 1;
        goto L4; // [171] 96
L5: 
        ;
    }

    /** 	if last < length(text) then*/
    if (IS_SEQUENCE(_text_18398)){
            _10332 = SEQ_PTR(_text_18398)->length;
    }
    else {
        _10332 = 1;
    }
    if (_last_18409 >= _10332)
    goto L8; // [181] 204

    /** 		result &= { text[last..$] }*/
    if (IS_SEQUENCE(_text_18398)){
            _10334 = SEQ_PTR(_text_18398)->length;
    }
    else {
        _10334 = 1;
    }
    rhs_slice_target = (object_ptr)&_10335;
    RHS_Slice(_text_18398, _last_18409, _10334);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _10335;
    _10336 = MAKE_SEQ(_1);
    _10335 = NOVALUE;
    Concat((object_ptr)&_result_18408, _result_18408, _10336);
    DeRefDS(_10336);
    _10336 = NOVALUE;
L8: 

    /** 	return result*/
    DeRef(_re_18396);
    DeRef(_text_18398);
    DeRef(_options_18401);
    DeRef(_match_data_18405);
    DeRef(_10315);
    _10315 = NOVALUE;
    DeRef(_10326);
    _10326 = NOVALUE;
    return _result_18408;
    ;
}


int _34find_replace(int _ex_18442, int _text_18444, int _replacement_18445, int _from_18446, int _options_18447)
{
    int _10338 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_18446)) {
        _1 = (long)(DBL_PTR(_from_18446)->dbl);
        if (UNIQUE(DBL_PTR(_from_18446)) && (DBL_PTR(_from_18446)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18446);
        _from_18446 = _1;
    }

    /** 	return find_replace_limit(ex, text, replacement, -1, from, options)*/
    Ref(_ex_18442);
    Ref(_text_18444);
    RefDS(_replacement_18445);
    Ref(_options_18447);
    _10338 = _34find_replace_limit(_ex_18442, _text_18444, _replacement_18445, -1, _from_18446, _options_18447);
    DeRef(_ex_18442);
    DeRef(_text_18444);
    DeRefDS(_replacement_18445);
    DeRef(_options_18447);
    return _10338;
    ;
}


int _34find_replace_limit(int _ex_18452, int _text_18454, int _replacement_18455, int _limit_18456, int _from_18457, int _options_18458)
{
    int _10342 = NOVALUE;
    int _10341 = NOVALUE;
    int _10339 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_18456)) {
        _1 = (long)(DBL_PTR(_limit_18456)->dbl);
        if (UNIQUE(DBL_PTR(_limit_18456)) && (DBL_PTR(_limit_18456)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_18456);
        _limit_18456 = _1;
    }
    if (!IS_ATOM_INT(_from_18457)) {
        _1 = (long)(DBL_PTR(_from_18457)->dbl);
        if (UNIQUE(DBL_PTR(_from_18457)) && (DBL_PTR(_from_18457)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18457);
        _from_18457 = _1;
    }

    /** 	if sequence(options) then */
    _10339 = IS_SEQUENCE(_options_18458);
    if (_10339 == 0)
    {
        _10339 = NOVALUE;
        goto L1; // [16] 26
    }
    else{
        _10339 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18458);
    _0 = _options_18458;
    _options_18458 = _18or_all(_options_18458);
    DeRef(_0);
L1: 

    /**     return machine_func(M_PCRE_REPLACE, { ex, text, replacement, options, */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_18452);
    *((int *)(_2+4)) = _ex_18452;
    Ref(_text_18454);
    *((int *)(_2+8)) = _text_18454;
    RefDS(_replacement_18455);
    *((int *)(_2+12)) = _replacement_18455;
    Ref(_options_18458);
    *((int *)(_2+16)) = _options_18458;
    *((int *)(_2+20)) = _from_18457;
    *((int *)(_2+24)) = _limit_18456;
    _10341 = MAKE_SEQ(_1);
    _10342 = machine(71, _10341);
    DeRefDS(_10341);
    _10341 = NOVALUE;
    DeRef(_ex_18452);
    DeRef(_text_18454);
    DeRefDS(_replacement_18455);
    DeRef(_options_18458);
    return _10342;
    ;
}


int _34find_replace_callback(int _ex_18466, int _text_18468, int _rid_18469, int _limit_18470, int _from_18471, int _options_18472)
{
    int _match_data_18476 = NOVALUE;
    int _replace_data_18479 = NOVALUE;
    int _params_18490 = NOVALUE;
    int _10378 = NOVALUE;
    int _10377 = NOVALUE;
    int _10376 = NOVALUE;
    int _10375 = NOVALUE;
    int _10374 = NOVALUE;
    int _10373 = NOVALUE;
    int _10372 = NOVALUE;
    int _10371 = NOVALUE;
    int _10370 = NOVALUE;
    int _10369 = NOVALUE;
    int _10368 = NOVALUE;
    int _10367 = NOVALUE;
    int _10366 = NOVALUE;
    int _10365 = NOVALUE;
    int _10364 = NOVALUE;
    int _10363 = NOVALUE;
    int _10362 = NOVALUE;
    int _10361 = NOVALUE;
    int _10360 = NOVALUE;
    int _10359 = NOVALUE;
    int _10358 = NOVALUE;
    int _10357 = NOVALUE;
    int _10355 = NOVALUE;
    int _10354 = NOVALUE;
    int _10353 = NOVALUE;
    int _10350 = NOVALUE;
    int _10349 = NOVALUE;
    int _10347 = NOVALUE;
    int _10345 = NOVALUE;
    int _10343 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_18469)) {
        _1 = (long)(DBL_PTR(_rid_18469)->dbl);
        if (UNIQUE(DBL_PTR(_rid_18469)) && (DBL_PTR(_rid_18469)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_18469);
        _rid_18469 = _1;
    }
    if (!IS_ATOM_INT(_limit_18470)) {
        _1 = (long)(DBL_PTR(_limit_18470)->dbl);
        if (UNIQUE(DBL_PTR(_limit_18470)) && (DBL_PTR(_limit_18470)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_18470);
        _limit_18470 = _1;
    }
    if (!IS_ATOM_INT(_from_18471)) {
        _1 = (long)(DBL_PTR(_from_18471)->dbl);
        if (UNIQUE(DBL_PTR(_from_18471)) && (DBL_PTR(_from_18471)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_18471);
        _from_18471 = _1;
    }

    /** 	if sequence(options) then */
    _10343 = IS_SEQUENCE(_options_18472);
    if (_10343 == 0)
    {
        _10343 = NOVALUE;
        goto L1; // [18] 28
    }
    else{
        _10343 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_18472);
    _0 = _options_18472;
    _options_18472 = _18or_all(_options_18472);
    DeRef(_0);
L1: 

    /** 	sequence match_data = find_all(ex, text, from, options), replace_data*/
    Ref(_ex_18466);
    _10345 = _34get_ovector_size(_ex_18466, 30);
    Ref(_ex_18466);
    Ref(_text_18468);
    Ref(_options_18472);
    _0 = _match_data_18476;
    _match_data_18476 = _34find_all(_ex_18466, _text_18468, _from_18471, _options_18472, _10345);
    DeRef(_0);
    _10345 = NOVALUE;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _10347 = (_limit_18470 == 0);
    if (_10347 != 0) {
        goto L2; // [51] 67
    }
    if (IS_SEQUENCE(_match_data_18476)){
            _10349 = SEQ_PTR(_match_data_18476)->length;
    }
    else {
        _10349 = 1;
    }
    _10350 = (_limit_18470 > _10349);
    _10349 = NOVALUE;
    if (_10350 == 0)
    {
        DeRef(_10350);
        _10350 = NOVALUE;
        goto L3; // [63] 75
    }
    else{
        DeRef(_10350);
        _10350 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_18476)){
            _limit_18470 = SEQ_PTR(_match_data_18476)->length;
    }
    else {
        _limit_18470 = 1;
    }
L3: 

    /** 	replace_data = repeat(0, limit)*/
    DeRef(_replace_data_18479);
    _replace_data_18479 = Repeat(0, _limit_18470);

    /** 	for i = 1 to limit do*/
    _10353 = _limit_18470;
    {
        int _i_18488;
        _i_18488 = 1;
L4: 
        if (_i_18488 > _10353){
            goto L5; // [86] 218
        }

        /** 		sequence params = repeat(0, length(match_data[i]))*/
        _2 = (int)SEQ_PTR(_match_data_18476);
        _10354 = (int)*(((s1_ptr)_2)->base + _i_18488);
        if (IS_SEQUENCE(_10354)){
                _10355 = SEQ_PTR(_10354)->length;
        }
        else {
            _10355 = 1;
        }
        _10354 = NOVALUE;
        DeRef(_params_18490);
        _params_18490 = Repeat(0, _10355);
        _10355 = NOVALUE;

        /** 		for j = 1 to length(match_data[i]) do*/
        _2 = (int)SEQ_PTR(_match_data_18476);
        _10357 = (int)*(((s1_ptr)_2)->base + _i_18488);
        if (IS_SEQUENCE(_10357)){
                _10358 = SEQ_PTR(_10357)->length;
        }
        else {
            _10358 = 1;
        }
        _10357 = NOVALUE;
        {
            int _j_18495;
            _j_18495 = 1;
L6: 
            if (_j_18495 > _10358){
                goto L7; // [115] 195
            }

            /** 			if equal(match_data[i][j],{0,0}) then*/
            _2 = (int)SEQ_PTR(_match_data_18476);
            _10359 = (int)*(((s1_ptr)_2)->base + _i_18488);
            _2 = (int)SEQ_PTR(_10359);
            _10360 = (int)*(((s1_ptr)_2)->base + _j_18495);
            _10359 = NOVALUE;
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = 0;
            ((int *)_2)[2] = 0;
            _10361 = MAKE_SEQ(_1);
            if (_10360 == _10361)
            _10362 = 1;
            else if (IS_ATOM_INT(_10360) && IS_ATOM_INT(_10361))
            _10362 = 0;
            else
            _10362 = (compare(_10360, _10361) == 0);
            _10360 = NOVALUE;
            DeRefDS(_10361);
            _10361 = NOVALUE;
            if (_10362 == 0)
            {
                _10362 = NOVALUE;
                goto L8; // [140] 152
            }
            else{
                _10362 = NOVALUE;
            }

            /** 				params[j] = 0*/
            _2 = (int)SEQ_PTR(_params_18490);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _params_18490 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18495);
            _1 = *(int *)_2;
            *(int *)_2 = 0;
            DeRef(_1);
            goto L9; // [149] 188
L8: 

            /** 				params[j] = text[match_data[i][j][1]..match_data[i][j][2]]*/
            _2 = (int)SEQ_PTR(_match_data_18476);
            _10363 = (int)*(((s1_ptr)_2)->base + _i_18488);
            _2 = (int)SEQ_PTR(_10363);
            _10364 = (int)*(((s1_ptr)_2)->base + _j_18495);
            _10363 = NOVALUE;
            _2 = (int)SEQ_PTR(_10364);
            _10365 = (int)*(((s1_ptr)_2)->base + 1);
            _10364 = NOVALUE;
            _2 = (int)SEQ_PTR(_match_data_18476);
            _10366 = (int)*(((s1_ptr)_2)->base + _i_18488);
            _2 = (int)SEQ_PTR(_10366);
            _10367 = (int)*(((s1_ptr)_2)->base + _j_18495);
            _10366 = NOVALUE;
            _2 = (int)SEQ_PTR(_10367);
            _10368 = (int)*(((s1_ptr)_2)->base + 2);
            _10367 = NOVALUE;
            rhs_slice_target = (object_ptr)&_10369;
            RHS_Slice(_text_18468, _10365, _10368);
            _2 = (int)SEQ_PTR(_params_18490);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _params_18490 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_18495);
            _1 = *(int *)_2;
            *(int *)_2 = _10369;
            if( _1 != _10369 ){
                DeRef(_1);
            }
            _10369 = NOVALUE;
L9: 

            /** 		end for*/
            _j_18495 = _j_18495 + 1;
            goto L6; // [190] 122
L7: 
            ;
        }

        /** 		replace_data[i] = call_func(rid, { params })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_params_18490);
        *((int *)(_2+4)) = _params_18490;
        _10370 = MAKE_SEQ(_1);
        _1 = (int)SEQ_PTR(_10370);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_rid_18469].addr;
        Ref(*(int *)(_2+4));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRef(_10371);
        _10371 = _1;
        DeRefDS(_10370);
        _10370 = NOVALUE;
        _2 = (int)SEQ_PTR(_replace_data_18479);
        _2 = (int)(((s1_ptr)_2)->base + _i_18488);
        _1 = *(int *)_2;
        *(int *)_2 = _10371;
        if( _1 != _10371 ){
            DeRef(_1);
        }
        _10371 = NOVALUE;
        DeRefDS(_params_18490);
        _params_18490 = NOVALUE;

        /** 	end for*/
        _i_18488 = _i_18488 + 1;
        goto L4; // [213] 93
L5: 
        ;
    }

    /** 	for i = limit to 1 by -1 do*/
    {
        int _i_18514;
        _i_18514 = _limit_18470;
LA: 
        if (_i_18514 < 1){
            goto LB; // [220] 270
        }

        /** 		text = replace(text, replace_data[i], match_data[i][1][1], match_data[i][1][2])*/
        _2 = (int)SEQ_PTR(_replace_data_18479);
        _10372 = (int)*(((s1_ptr)_2)->base + _i_18514);
        _2 = (int)SEQ_PTR(_match_data_18476);
        _10373 = (int)*(((s1_ptr)_2)->base + _i_18514);
        _2 = (int)SEQ_PTR(_10373);
        _10374 = (int)*(((s1_ptr)_2)->base + 1);
        _10373 = NOVALUE;
        _2 = (int)SEQ_PTR(_10374);
        _10375 = (int)*(((s1_ptr)_2)->base + 1);
        _10374 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_18476);
        _10376 = (int)*(((s1_ptr)_2)->base + _i_18514);
        _2 = (int)SEQ_PTR(_10376);
        _10377 = (int)*(((s1_ptr)_2)->base + 1);
        _10376 = NOVALUE;
        _2 = (int)SEQ_PTR(_10377);
        _10378 = (int)*(((s1_ptr)_2)->base + 2);
        _10377 = NOVALUE;
        {
            int p1 = _text_18468;
            int p2 = _10372;
            int p3 = _10375;
            int p4 = _10378;
            struct replace_block replace_params;
            replace_params.copy_to   = &p1;
            replace_params.copy_from = &p2;
            replace_params.start     = &p3;
            replace_params.stop      = &p4;
            replace_params.target    = &_text_18468;
            Replace( &replace_params );
        }
        _10372 = NOVALUE;
        _10375 = NOVALUE;
        _10378 = NOVALUE;

        /** 	end for*/
        _i_18514 = _i_18514 + -1;
        goto LA; // [265] 227
LB: 
        ;
    }

    /** 	return text*/
    DeRef(_ex_18466);
    DeRef(_options_18472);
    DeRef(_match_data_18476);
    DeRef(_replace_data_18479);
    DeRef(_10347);
    _10347 = NOVALUE;
    _10354 = NOVALUE;
    _10357 = NOVALUE;
    _10365 = NOVALUE;
    _10368 = NOVALUE;
    return _text_18468;
    ;
}



// 0xA7105198
