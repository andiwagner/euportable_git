// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _7sort(int _x_1341, int _order_1342)
{
    int _gap_1343 = NOVALUE;
    int _j_1344 = NOVALUE;
    int _first_1345 = NOVALUE;
    int _last_1346 = NOVALUE;
    int _tempi_1347 = NOVALUE;
    int _tempj_1348 = NOVALUE;
    int _659 = NOVALUE;
    int _655 = NOVALUE;
    int _652 = NOVALUE;
    int _648 = NOVALUE;
    int _645 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_order_1342)) {
        _1 = (long)(DBL_PTR(_order_1342)->dbl);
        if (UNIQUE(DBL_PTR(_order_1342)) && (DBL_PTR(_order_1342)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_order_1342);
        _order_1342 = _1;
    }

    /** 	if order >= 0 then*/
    if (_order_1342 < 0)
    goto L1; // [9] 23

    /** 		order = -1*/
    _order_1342 = -1;
    goto L2; // [20] 31
L1: 

    /** 		order = 1*/
    _order_1342 = 1;
L2: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_1341)){
            _last_1346 = SEQ_PTR(_x_1341)->length;
    }
    else {
        _last_1346 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_1346 >= 0) {
        _645 = _last_1346 / 10;
    }
    else {
        temp_dbl = floor((double)_last_1346 / (double)10);
        _645 = (long)temp_dbl;
    }
    _gap_1343 = _645 + 1;
    _645 = NOVALUE;

    /** 	while 1 do*/
L3: 

    /** 		first = gap + 1*/
    _first_1345 = _gap_1343 + 1;

    /** 		for i = first to last do*/
    _648 = _last_1346;
    {
        int _i_1358;
        _i_1358 = _first_1345;
L4: 
        if (_i_1358 > _648){
            goto L5; // [68] 170
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_1347);
        _2 = (int)SEQ_PTR(_x_1341);
        _tempi_1347 = (int)*(((s1_ptr)_2)->base + _i_1358);
        Ref(_tempi_1347);

        /** 			j = i - gap*/
        _j_1344 = _i_1358 - _gap_1343;

        /** 			while 1 do*/
L6: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_1348);
        _2 = (int)SEQ_PTR(_x_1341);
        _tempj_1348 = (int)*(((s1_ptr)_2)->base + _j_1344);
        Ref(_tempj_1348);

        /** 				if eu:compare(tempi, tempj) != order then*/
        if (IS_ATOM_INT(_tempi_1347) && IS_ATOM_INT(_tempj_1348)){
            _652 = (_tempi_1347 < _tempj_1348) ? -1 : (_tempi_1347 > _tempj_1348);
        }
        else{
            _652 = compare(_tempi_1347, _tempj_1348);
        }
        if (_652 == _order_1342)
        goto L7; // [106] 123

        /** 					j += gap*/
        _j_1344 = _j_1344 + _gap_1343;

        /** 					exit*/
        goto L8; // [120] 157
L7: 

        /** 				x[j+gap] = tempj*/
        _655 = _j_1344 + _gap_1343;
        Ref(_tempj_1348);
        _2 = (int)SEQ_PTR(_x_1341);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_1341 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _655);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_1348;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_1344 > _gap_1343)
        goto L9; // [135] 144

        /** 					exit*/
        goto L8; // [141] 157
L9: 

        /** 				j -= gap*/
        _j_1344 = _j_1344 - _gap_1343;

        /** 			end while*/
        goto L6; // [154] 94
L8: 

        /** 			x[j] = tempi*/
        Ref(_tempi_1347);
        _2 = (int)SEQ_PTR(_x_1341);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_1341 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_1344);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_1347;
        DeRef(_1);

        /** 		end for*/
        _i_1358 = _i_1358 + 1;
        goto L4; // [165] 75
L5: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_1343 != 1)
    goto LA; // [172] 185

    /** 			return x*/
    DeRef(_tempi_1347);
    DeRef(_tempj_1348);
    DeRef(_655);
    _655 = NOVALUE;
    return _x_1341;
    goto L3; // [182] 55
LA: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_1343 >= 0) {
        _659 = _gap_1343 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_1343 / (double)7);
        _659 = (long)temp_dbl;
    }
    _gap_1343 = _659 + 1;
    _659 = NOVALUE;

    /** 	end while*/
    goto L3; // [200] 55
    ;
}


int _7custom_sort(int _custom_compare_1379, int _x_1380, int _data_1381, int _order_1382)
{
    int _gap_1383 = NOVALUE;
    int _j_1384 = NOVALUE;
    int _first_1385 = NOVALUE;
    int _last_1386 = NOVALUE;
    int _tempi_1387 = NOVALUE;
    int _tempj_1388 = NOVALUE;
    int _result_1389 = NOVALUE;
    int _args_1390 = NOVALUE;
    int _687 = NOVALUE;
    int _683 = NOVALUE;
    int _680 = NOVALUE;
    int _678 = NOVALUE;
    int _677 = NOVALUE;
    int _672 = NOVALUE;
    int _669 = NOVALUE;
    int _666 = NOVALUE;
    int _665 = NOVALUE;
    int _663 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_custom_compare_1379)) {
        _1 = (long)(DBL_PTR(_custom_compare_1379)->dbl);
        if (UNIQUE(DBL_PTR(_custom_compare_1379)) && (DBL_PTR(_custom_compare_1379)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_custom_compare_1379);
        _custom_compare_1379 = _1;
    }
    if (!IS_ATOM_INT(_order_1382)) {
        _1 = (long)(DBL_PTR(_order_1382)->dbl);
        if (UNIQUE(DBL_PTR(_order_1382)) && (DBL_PTR(_order_1382)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_order_1382);
        _order_1382 = _1;
    }

    /** 	sequence args = {0, 0}*/
    DeRef(_args_1390);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _args_1390 = MAKE_SEQ(_1);

    /** 	if order >= 0 then*/
    if (_order_1382 < 0)
    goto L1; // [19] 33

    /** 		order = -1*/
    _order_1382 = -1;
    goto L2; // [30] 41
L1: 

    /** 		order = 1*/
    _order_1382 = 1;
L2: 

    /** 	if atom(data) then*/
    _663 = IS_ATOM(_data_1381);
    if (_663 == 0)
    {
        _663 = NOVALUE;
        goto L3; // [46] 58
    }
    else{
        _663 = NOVALUE;
    }

    /** 		args &= data*/
    if (IS_SEQUENCE(_args_1390) && IS_ATOM(_data_1381)) {
        Ref(_data_1381);
        Append(&_args_1390, _args_1390, _data_1381);
    }
    else if (IS_ATOM(_args_1390) && IS_SEQUENCE(_data_1381)) {
    }
    else {
        Concat((object_ptr)&_args_1390, _args_1390, _data_1381);
    }
    goto L4; // [55] 78
L3: 

    /** 	elsif length(data) then*/
    if (IS_SEQUENCE(_data_1381)){
            _665 = SEQ_PTR(_data_1381)->length;
    }
    else {
        _665 = 1;
    }
    if (_665 == 0)
    {
        _665 = NOVALUE;
        goto L5; // [63] 77
    }
    else{
        _665 = NOVALUE;
    }

    /** 		args = append(args, data[1])*/
    _2 = (int)SEQ_PTR(_data_1381);
    _666 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_666);
    Append(&_args_1390, _args_1390, _666);
    _666 = NOVALUE;
L5: 
L4: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_1380)){
            _last_1386 = SEQ_PTR(_x_1380)->length;
    }
    else {
        _last_1386 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_1386 >= 0) {
        _669 = _last_1386 / 10;
    }
    else {
        temp_dbl = floor((double)_last_1386 / (double)10);
        _669 = (long)temp_dbl;
    }
    _gap_1383 = _669 + 1;
    _669 = NOVALUE;

    /** 	while 1 do*/
L6: 

    /** 		first = gap + 1*/
    _first_1385 = _gap_1383 + 1;

    /** 		for i = first to last do*/
    _672 = _last_1386;
    {
        int _i_1408;
        _i_1408 = _first_1385;
L7: 
        if (_i_1408 > _672){
            goto L8; // [115] 260
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_1387);
        _2 = (int)SEQ_PTR(_x_1380);
        _tempi_1387 = (int)*(((s1_ptr)_2)->base + _i_1408);
        Ref(_tempi_1387);

        /** 			args[1] = tempi*/
        Ref(_tempi_1387);
        _2 = (int)SEQ_PTR(_args_1390);
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_1387;
        DeRef(_1);

        /** 			j = i - gap*/
        _j_1384 = _i_1408 - _gap_1383;

        /** 			while 1 do*/
L9: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_1388);
        _2 = (int)SEQ_PTR(_x_1380);
        _tempj_1388 = (int)*(((s1_ptr)_2)->base + _j_1384);
        Ref(_tempj_1388);

        /** 				args[2] = tempj*/
        Ref(_tempj_1388);
        _2 = (int)SEQ_PTR(_args_1390);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_1388;
        DeRef(_1);

        /** 				result = call_func(custom_compare, args)*/
        _1 = (int)SEQ_PTR(_args_1390);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_custom_compare_1379].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
        }
        DeRef(_result_1389);
        _result_1389 = _1;

        /** 				if sequence(result) then*/
        _677 = IS_SEQUENCE(_result_1389);
        if (_677 == 0)
        {
            _677 = NOVALUE;
            goto LA; // [170] 190
        }
        else{
            _677 = NOVALUE;
        }

        /** 					args[3] = result[2]*/
        _2 = (int)SEQ_PTR(_result_1389);
        _678 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_678);
        _2 = (int)SEQ_PTR(_args_1390);
        _2 = (int)(((s1_ptr)_2)->base + 3);
        _1 = *(int *)_2;
        *(int *)_2 = _678;
        if( _1 != _678 ){
            DeRef(_1);
        }
        _678 = NOVALUE;

        /** 					result = result[1]*/
        _0 = _result_1389;
        _2 = (int)SEQ_PTR(_result_1389);
        _result_1389 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_result_1389);
        DeRef(_0);
LA: 

        /** 				if eu:compare(result, 0) != order then*/
        if (IS_ATOM_INT(_result_1389) && IS_ATOM_INT(0)){
            _680 = (_result_1389 < 0) ? -1 : (_result_1389 > 0);
        }
        else{
            _680 = compare(_result_1389, 0);
        }
        if (_680 == _order_1382)
        goto LB; // [196] 213

        /** 					j += gap*/
        _j_1384 = _j_1384 + _gap_1383;

        /** 					exit*/
        goto LC; // [210] 247
LB: 

        /** 				x[j+gap] = tempj*/
        _683 = _j_1384 + _gap_1383;
        Ref(_tempj_1388);
        _2 = (int)SEQ_PTR(_x_1380);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_1380 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _683);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_1388;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_1384 > _gap_1383)
        goto LD; // [225] 234

        /** 					exit*/
        goto LC; // [231] 247
LD: 

        /** 				j -= gap*/
        _j_1384 = _j_1384 - _gap_1383;

        /** 			end while*/
        goto L9; // [244] 147
LC: 

        /** 			x[j] = tempi*/
        Ref(_tempi_1387);
        _2 = (int)SEQ_PTR(_x_1380);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_1380 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_1384);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_1387;
        DeRef(_1);

        /** 		end for*/
        _i_1408 = _i_1408 + 1;
        goto L7; // [255] 122
L8: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_1383 != 1)
    goto LE; // [262] 275

    /** 			return x*/
    DeRef(_data_1381);
    DeRef(_tempi_1387);
    DeRef(_tempj_1388);
    DeRef(_result_1389);
    DeRef(_args_1390);
    DeRef(_683);
    _683 = NOVALUE;
    return _x_1380;
    goto L6; // [272] 102
LE: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_1383 >= 0) {
        _687 = _gap_1383 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_1383 / (double)7);
        _687 = (long)temp_dbl;
    }
    _gap_1383 = _687 + 1;
    _687 = NOVALUE;

    /** 	end while*/
    goto L6; // [290] 102
    ;
}


int _7column_compare(int _a_1434, int _b_1435, int _cols_1436)
{
    int _sign_1437 = NOVALUE;
    int _column_1438 = NOVALUE;
    int _710 = NOVALUE;
    int _708 = NOVALUE;
    int _707 = NOVALUE;
    int _706 = NOVALUE;
    int _705 = NOVALUE;
    int _704 = NOVALUE;
    int _703 = NOVALUE;
    int _701 = NOVALUE;
    int _700 = NOVALUE;
    int _699 = NOVALUE;
    int _697 = NOVALUE;
    int _695 = NOVALUE;
    int _692 = NOVALUE;
    int _690 = NOVALUE;
    int _689 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(cols) do*/
    if (IS_SEQUENCE(_cols_1436)){
            _689 = SEQ_PTR(_cols_1436)->length;
    }
    else {
        _689 = 1;
    }
    {
        int _i_1440;
        _i_1440 = 1;
L1: 
        if (_i_1440 > _689){
            goto L2; // [6] 184
        }

        /** 		if cols[i] < 0 then*/
        _2 = (int)SEQ_PTR(_cols_1436);
        _690 = (int)*(((s1_ptr)_2)->base + _i_1440);
        if (binary_op_a(GREATEREQ, _690, 0)){
            _690 = NOVALUE;
            goto L3; // [19] 46
        }
        _690 = NOVALUE;

        /** 			sign = -1*/
        _sign_1437 = -1;

        /** 			column = -cols[i]*/
        _2 = (int)SEQ_PTR(_cols_1436);
        _692 = (int)*(((s1_ptr)_2)->base + _i_1440);
        if (IS_ATOM_INT(_692)) {
            if ((unsigned long)_692 == 0xC0000000)
            _column_1438 = (int)NewDouble((double)-0xC0000000);
            else
            _column_1438 = - _692;
        }
        else {
            _column_1438 = unary_op(UMINUS, _692);
        }
        _692 = NOVALUE;
        if (!IS_ATOM_INT(_column_1438)) {
            _1 = (long)(DBL_PTR(_column_1438)->dbl);
            if (UNIQUE(DBL_PTR(_column_1438)) && (DBL_PTR(_column_1438)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_column_1438);
            _column_1438 = _1;
        }
        goto L4; // [43] 64
L3: 

        /** 			sign = 1*/
        _sign_1437 = 1;

        /** 			column = cols[i]*/
        _2 = (int)SEQ_PTR(_cols_1436);
        _column_1438 = (int)*(((s1_ptr)_2)->base + _i_1440);
        if (!IS_ATOM_INT(_column_1438)){
            _column_1438 = (long)DBL_PTR(_column_1438)->dbl;
        }
L4: 

        /** 		if column <= length(a) then*/
        if (IS_SEQUENCE(_a_1434)){
                _695 = SEQ_PTR(_a_1434)->length;
        }
        else {
            _695 = 1;
        }
        if (_column_1438 > _695)
        goto L5; // [71] 145

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_1435)){
                _697 = SEQ_PTR(_b_1435)->length;
        }
        else {
            _697 = 1;
        }
        if (_column_1438 > _697)
        goto L6; // [80] 129

        /** 				if not equal(a[column], b[column]) then*/
        _2 = (int)SEQ_PTR(_a_1434);
        _699 = (int)*(((s1_ptr)_2)->base + _column_1438);
        _2 = (int)SEQ_PTR(_b_1435);
        _700 = (int)*(((s1_ptr)_2)->base + _column_1438);
        if (_699 == _700)
        _701 = 1;
        else if (IS_ATOM_INT(_699) && IS_ATOM_INT(_700))
        _701 = 0;
        else
        _701 = (compare(_699, _700) == 0);
        _699 = NOVALUE;
        _700 = NOVALUE;
        if (_701 != 0)
        goto L7; // [98] 177
        _701 = NOVALUE;

        /** 					return sign * eu:compare(a[column], b[column])*/
        _2 = (int)SEQ_PTR(_a_1434);
        _703 = (int)*(((s1_ptr)_2)->base + _column_1438);
        _2 = (int)SEQ_PTR(_b_1435);
        _704 = (int)*(((s1_ptr)_2)->base + _column_1438);
        if (IS_ATOM_INT(_703) && IS_ATOM_INT(_704)){
            _705 = (_703 < _704) ? -1 : (_703 > _704);
        }
        else{
            _705 = compare(_703, _704);
        }
        _703 = NOVALUE;
        _704 = NOVALUE;
        if (_sign_1437 == (short)_sign_1437)
        _706 = _sign_1437 * _705;
        else
        _706 = NewDouble(_sign_1437 * (double)_705);
        _705 = NOVALUE;
        DeRef(_a_1434);
        DeRef(_b_1435);
        DeRef(_cols_1436);
        return _706;
        goto L7; // [126] 177
L6: 

        /** 				return sign * -1*/
        if (_sign_1437 == (short)_sign_1437)
        _707 = _sign_1437 * -1;
        else
        _707 = NewDouble(_sign_1437 * (double)-1);
        DeRef(_a_1434);
        DeRef(_b_1435);
        DeRef(_cols_1436);
        DeRef(_706);
        _706 = NOVALUE;
        return _707;
        goto L7; // [142] 177
L5: 

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_1435)){
                _708 = SEQ_PTR(_b_1435)->length;
        }
        else {
            _708 = 1;
        }
        if (_column_1438 > _708)
        goto L8; // [150] 169

        /** 				return sign * 1*/
        _710 = _sign_1437 * 1;
        DeRef(_a_1434);
        DeRef(_b_1435);
        DeRef(_cols_1436);
        DeRef(_706);
        _706 = NOVALUE;
        DeRef(_707);
        _707 = NOVALUE;
        return _710;
        goto L9; // [166] 176
L8: 

        /** 				return 0*/
        DeRef(_a_1434);
        DeRef(_b_1435);
        DeRef(_cols_1436);
        DeRef(_706);
        _706 = NOVALUE;
        DeRef(_707);
        _707 = NOVALUE;
        DeRef(_710);
        _710 = NOVALUE;
        return 0;
L9: 
L7: 

        /** 	end for*/
        _i_1440 = _i_1440 + 1;
        goto L1; // [179] 13
L2: 
        ;
    }

    /** 	return 0*/
    DeRef(_a_1434);
    DeRef(_b_1435);
    DeRef(_cols_1436);
    DeRef(_706);
    _706 = NOVALUE;
    DeRef(_707);
    _707 = NOVALUE;
    DeRef(_710);
    _710 = NOVALUE;
    return 0;
    ;
}


int _7sort_columns(int _x_1474, int _column_list_1475)
{
    int _714 = NOVALUE;
    int _713 = NOVALUE;
    int _712 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return custom_sort(routine_id("column_compare"), x, {column_list})*/
    _712 = CRoutineId(72, 7, _711);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_column_list_1475);
    *((int *)(_2+4)) = _column_list_1475;
    _713 = MAKE_SEQ(_1);
    RefDS(_x_1474);
    _714 = _7custom_sort(_712, _x_1474, _713, 1);
    _712 = NOVALUE;
    _713 = NOVALUE;
    DeRefDS(_x_1474);
    DeRefDS(_column_list_1475);
    return _714;
    ;
}


int _7merge(int _a_1482, int _b_1483, int _compfunc_1484, int _userdata_1485)
{
    int _al_1486 = NOVALUE;
    int _bl_1487 = NOVALUE;
    int _n_1488 = NOVALUE;
    int _r_1489 = NOVALUE;
    int _s_1490 = NOVALUE;
    int _758 = NOVALUE;
    int _757 = NOVALUE;
    int _756 = NOVALUE;
    int _754 = NOVALUE;
    int _753 = NOVALUE;
    int _752 = NOVALUE;
    int _751 = NOVALUE;
    int _749 = NOVALUE;
    int _746 = NOVALUE;
    int _744 = NOVALUE;
    int _741 = NOVALUE;
    int _740 = NOVALUE;
    int _739 = NOVALUE;
    int _738 = NOVALUE;
    int _737 = NOVALUE;
    int _736 = NOVALUE;
    int _735 = NOVALUE;
    int _732 = NOVALUE;
    int _730 = NOVALUE;
    int _727 = NOVALUE;
    int _726 = NOVALUE;
    int _725 = NOVALUE;
    int _724 = NOVALUE;
    int _723 = NOVALUE;
    int _722 = NOVALUE;
    int _721 = NOVALUE;
    int _720 = NOVALUE;
    int _717 = NOVALUE;
    int _716 = NOVALUE;
    int _715 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_compfunc_1484)) {
        _1 = (long)(DBL_PTR(_compfunc_1484)->dbl);
        if (UNIQUE(DBL_PTR(_compfunc_1484)) && (DBL_PTR(_compfunc_1484)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_compfunc_1484);
        _compfunc_1484 = _1;
    }

    /** 	al = 1*/
    _al_1486 = 1;

    /** 	bl = 1*/
    _bl_1487 = 1;

    /** 	n = 1*/
    _n_1488 = 1;

    /** 	s = repeat(0, length(a) + length(b))*/
    if (IS_SEQUENCE(_a_1482)){
            _715 = SEQ_PTR(_a_1482)->length;
    }
    else {
        _715 = 1;
    }
    if (IS_SEQUENCE(_b_1483)){
            _716 = SEQ_PTR(_b_1483)->length;
    }
    else {
        _716 = 1;
    }
    _717 = _715 + _716;
    _715 = NOVALUE;
    _716 = NOVALUE;
    DeRef(_s_1490);
    _s_1490 = Repeat(0, _717);
    _717 = NOVALUE;

    /** 	if compfunc >= 0 then*/
    if (_compfunc_1484 < 0)
    goto L1; // [48] 165

    /** 		while al <= length(a) and bl <= length(b) do*/
L2: 
    if (IS_SEQUENCE(_a_1482)){
            _720 = SEQ_PTR(_a_1482)->length;
    }
    else {
        _720 = 1;
    }
    _721 = (_al_1486 <= _720);
    _720 = NOVALUE;
    if (_721 == 0) {
        goto L3; // [64] 268
    }
    if (IS_SEQUENCE(_b_1483)){
            _723 = SEQ_PTR(_b_1483)->length;
    }
    else {
        _723 = 1;
    }
    _724 = (_bl_1487 <= _723);
    _723 = NOVALUE;
    if (_724 == 0)
    {
        DeRef(_724);
        _724 = NOVALUE;
        goto L3; // [76] 268
    }
    else{
        DeRef(_724);
        _724 = NOVALUE;
    }

    /** 			r = call_func(compfunc,{a[al], b[bl], userdata})*/
    _2 = (int)SEQ_PTR(_a_1482);
    _725 = (int)*(((s1_ptr)_2)->base + _al_1486);
    _2 = (int)SEQ_PTR(_b_1483);
    _726 = (int)*(((s1_ptr)_2)->base + _bl_1487);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_725);
    *((int *)(_2+4)) = _725;
    Ref(_726);
    *((int *)(_2+8)) = _726;
    Ref(_userdata_1485);
    *((int *)(_2+12)) = _userdata_1485;
    _727 = MAKE_SEQ(_1);
    _726 = NOVALUE;
    _725 = NOVALUE;
    _1 = (int)SEQ_PTR(_727);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_compfunc_1484].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12)
                         );
    _r_1489 = _1;
    DeRefDS(_727);
    _727 = NOVALUE;
    if (!IS_ATOM_INT(_r_1489)) {
        _1 = (long)(DBL_PTR(_r_1489)->dbl);
        if (UNIQUE(DBL_PTR(_r_1489)) && (DBL_PTR(_r_1489)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_1489);
        _r_1489 = _1;
    }

    /** 			if r <= 0 then*/
    if (_r_1489 > 0)
    goto L4; // [105] 130

    /** 				s[n] = a[al]*/
    _2 = (int)SEQ_PTR(_a_1482);
    _730 = (int)*(((s1_ptr)_2)->base + _al_1486);
    Ref(_730);
    _2 = (int)SEQ_PTR(_s_1490);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_1490 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_1488);
    _1 = *(int *)_2;
    *(int *)_2 = _730;
    if( _1 != _730 ){
        DeRef(_1);
    }
    _730 = NOVALUE;

    /** 				al += 1*/
    _al_1486 = _al_1486 + 1;
    goto L5; // [127] 149
L4: 

    /** 				s[n] = b[bl]*/
    _2 = (int)SEQ_PTR(_b_1483);
    _732 = (int)*(((s1_ptr)_2)->base + _bl_1487);
    Ref(_732);
    _2 = (int)SEQ_PTR(_s_1490);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_1490 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_1488);
    _1 = *(int *)_2;
    *(int *)_2 = _732;
    if( _1 != _732 ){
        DeRef(_1);
    }
    _732 = NOVALUE;

    /** 				bl += 1*/
    _bl_1487 = _bl_1487 + 1;
L5: 

    /** 			n += 1*/
    _n_1488 = _n_1488 + 1;

    /** 		end while*/
    goto L2; // [159] 57
    goto L3; // [162] 268
L1: 

    /** 		while al <= length(a) and bl <= length(b) do*/
L6: 
    if (IS_SEQUENCE(_a_1482)){
            _735 = SEQ_PTR(_a_1482)->length;
    }
    else {
        _735 = 1;
    }
    _736 = (_al_1486 <= _735);
    _735 = NOVALUE;
    if (_736 == 0) {
        goto L7; // [177] 267
    }
    if (IS_SEQUENCE(_b_1483)){
            _738 = SEQ_PTR(_b_1483)->length;
    }
    else {
        _738 = 1;
    }
    _739 = (_bl_1487 <= _738);
    _738 = NOVALUE;
    if (_739 == 0)
    {
        DeRef(_739);
        _739 = NOVALUE;
        goto L7; // [189] 267
    }
    else{
        DeRef(_739);
        _739 = NOVALUE;
    }

    /** 			r = compare(a[al], b[bl])*/
    _2 = (int)SEQ_PTR(_a_1482);
    _740 = (int)*(((s1_ptr)_2)->base + _al_1486);
    _2 = (int)SEQ_PTR(_b_1483);
    _741 = (int)*(((s1_ptr)_2)->base + _bl_1487);
    if (IS_ATOM_INT(_740) && IS_ATOM_INT(_741)){
        _r_1489 = (_740 < _741) ? -1 : (_740 > _741);
    }
    else{
        _r_1489 = compare(_740, _741);
    }
    _740 = NOVALUE;
    _741 = NOVALUE;

    /** 			if r <= 0 then*/
    if (_r_1489 > 0)
    goto L8; // [210] 235

    /** 				s[n] = a[al]*/
    _2 = (int)SEQ_PTR(_a_1482);
    _744 = (int)*(((s1_ptr)_2)->base + _al_1486);
    Ref(_744);
    _2 = (int)SEQ_PTR(_s_1490);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_1490 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_1488);
    _1 = *(int *)_2;
    *(int *)_2 = _744;
    if( _1 != _744 ){
        DeRef(_1);
    }
    _744 = NOVALUE;

    /** 				al += 1*/
    _al_1486 = _al_1486 + 1;
    goto L9; // [232] 254
L8: 

    /** 				s[n] = b[bl]*/
    _2 = (int)SEQ_PTR(_b_1483);
    _746 = (int)*(((s1_ptr)_2)->base + _bl_1487);
    Ref(_746);
    _2 = (int)SEQ_PTR(_s_1490);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_1490 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_1488);
    _1 = *(int *)_2;
    *(int *)_2 = _746;
    if( _1 != _746 ){
        DeRef(_1);
    }
    _746 = NOVALUE;

    /** 				bl += 1*/
    _bl_1487 = _bl_1487 + 1;
L9: 

    /** 			n += 1*/
    _n_1488 = _n_1488 + 1;

    /** 		end while*/
    goto L6; // [264] 170
L7: 
L3: 

    /** 	if al > length(a) then*/
    if (IS_SEQUENCE(_a_1482)){
            _749 = SEQ_PTR(_a_1482)->length;
    }
    else {
        _749 = 1;
    }
    if (_al_1486 <= _749)
    goto LA; // [273] 298

    /** 		s[n .. $] = b[bl .. $]*/
    if (IS_SEQUENCE(_s_1490)){
            _751 = SEQ_PTR(_s_1490)->length;
    }
    else {
        _751 = 1;
    }
    if (IS_SEQUENCE(_b_1483)){
            _752 = SEQ_PTR(_b_1483)->length;
    }
    else {
        _752 = 1;
    }
    rhs_slice_target = (object_ptr)&_753;
    RHS_Slice(_b_1483, _bl_1487, _752);
    assign_slice_seq = (s1_ptr *)&_s_1490;
    AssignSlice(_n_1488, _751, _753);
    _751 = NOVALUE;
    DeRefDS(_753);
    _753 = NOVALUE;
    goto LB; // [295] 327
LA: 

    /** 	elsif bl > length(b) then*/
    if (IS_SEQUENCE(_b_1483)){
            _754 = SEQ_PTR(_b_1483)->length;
    }
    else {
        _754 = 1;
    }
    if (_bl_1487 <= _754)
    goto LC; // [303] 326

    /** 		s[n .. $] = a[al .. $]*/
    if (IS_SEQUENCE(_s_1490)){
            _756 = SEQ_PTR(_s_1490)->length;
    }
    else {
        _756 = 1;
    }
    if (IS_SEQUENCE(_a_1482)){
            _757 = SEQ_PTR(_a_1482)->length;
    }
    else {
        _757 = 1;
    }
    rhs_slice_target = (object_ptr)&_758;
    RHS_Slice(_a_1482, _al_1486, _757);
    assign_slice_seq = (s1_ptr *)&_s_1490;
    AssignSlice(_n_1488, _756, _758);
    _756 = NOVALUE;
    DeRefDS(_758);
    _758 = NOVALUE;
LC: 
LB: 

    /** 	return s*/
    DeRefDS(_a_1482);
    DeRefDS(_b_1483);
    DeRef(_userdata_1485);
    DeRef(_721);
    _721 = NOVALUE;
    DeRef(_736);
    _736 = NOVALUE;
    return _s_1490;
    ;
}


int _7insertion_sort(int _s_1547, int _e_1548, int _compfunc_1549, int _userdata_1550)
{
    int _key_1551 = NOVALUE;
    int _a_1552 = NOVALUE;
    int _802 = NOVALUE;
    int _801 = NOVALUE;
    int _800 = NOVALUE;
    int _799 = NOVALUE;
    int _798 = NOVALUE;
    int _797 = NOVALUE;
    int _793 = NOVALUE;
    int _792 = NOVALUE;
    int _791 = NOVALUE;
    int _790 = NOVALUE;
    int _788 = NOVALUE;
    int _787 = NOVALUE;
    int _786 = NOVALUE;
    int _785 = NOVALUE;
    int _784 = NOVALUE;
    int _783 = NOVALUE;
    int _782 = NOVALUE;
    int _778 = NOVALUE;
    int _777 = NOVALUE;
    int _776 = NOVALUE;
    int _773 = NOVALUE;
    int _771 = NOVALUE;
    int _770 = NOVALUE;
    int _769 = NOVALUE;
    int _768 = NOVALUE;
    int _767 = NOVALUE;
    int _766 = NOVALUE;
    int _765 = NOVALUE;
    int _764 = NOVALUE;
    int _763 = NOVALUE;
    int _761 = NOVALUE;
    int _759 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_compfunc_1549)) {
        _1 = (long)(DBL_PTR(_compfunc_1549)->dbl);
        if (UNIQUE(DBL_PTR(_compfunc_1549)) && (DBL_PTR(_compfunc_1549)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_compfunc_1549);
        _compfunc_1549 = _1;
    }

    /** 	if atom(e) then*/
    _759 = IS_ATOM(_e_1548);
    if (_759 == 0)
    {
        _759 = NOVALUE;
        goto L1; // [12] 24
    }
    else{
        _759 = NOVALUE;
    }

    /** 		s &= e*/
    if (IS_SEQUENCE(_s_1547) && IS_ATOM(_e_1548)) {
        Ref(_e_1548);
        Append(&_s_1547, _s_1547, _e_1548);
    }
    else if (IS_ATOM(_s_1547) && IS_SEQUENCE(_e_1548)) {
    }
    else {
        Concat((object_ptr)&_s_1547, _s_1547, _e_1548);
    }
    goto L2; // [21] 80
L1: 

    /** 	elsif length(e) > 1 then*/
    if (IS_SEQUENCE(_e_1548)){
            _761 = SEQ_PTR(_e_1548)->length;
    }
    else {
        _761 = 1;
    }
    if (_761 <= 1)
    goto L3; // [29] 79

    /** 		return merge(insertion_sort(s,,compfunc, userdata), insertion_sort(e,,compfunc, userdata), compfunc, userdata)*/
    RefDS(_s_1547);
    DeRef(_763);
    _763 = _s_1547;
    DeRef(_764);
    _764 = _compfunc_1549;
    Ref(_userdata_1550);
    DeRef(_765);
    _765 = _userdata_1550;
    RefDS(_5);
    _766 = _7insertion_sort(_763, _5, _764, _765);
    _763 = NOVALUE;
    _764 = NOVALUE;
    _765 = NOVALUE;
    Ref(_e_1548);
    DeRef(_767);
    _767 = _e_1548;
    DeRef(_768);
    _768 = _compfunc_1549;
    Ref(_userdata_1550);
    DeRef(_769);
    _769 = _userdata_1550;
    RefDS(_5);
    _770 = _7insertion_sort(_767, _5, _768, _769);
    _767 = NOVALUE;
    _768 = NOVALUE;
    _769 = NOVALUE;
    Ref(_userdata_1550);
    _771 = _7merge(_766, _770, _compfunc_1549, _userdata_1550);
    _766 = NOVALUE;
    _770 = NOVALUE;
    DeRefDS(_s_1547);
    DeRef(_e_1548);
    DeRef(_userdata_1550);
    DeRef(_key_1551);
    return _771;
L3: 
L2: 

    /** 	if compfunc = -1 then*/
    if (_compfunc_1549 != -1)
    goto L4; // [82] 231

    /** 		for j = 2 to length(s) label "outer" do*/
    if (IS_SEQUENCE(_s_1547)){
            _773 = SEQ_PTR(_s_1547)->length;
    }
    else {
        _773 = 1;
    }
    {
        int _j_1571;
        _j_1571 = 2;
L5: 
        if (_j_1571 > _773){
            goto L6; // [91] 228
        }

        /** 			key = s[j]*/
        DeRef(_key_1551);
        _2 = (int)SEQ_PTR(_s_1547);
        _key_1551 = (int)*(((s1_ptr)_2)->base + _j_1571);
        Ref(_key_1551);

        /** 			for i = j - 1 to 1 by -1 do*/
        _776 = _j_1571 - 1;
        {
            int _i_1576;
            _i_1576 = _776;
L7: 
            if (_i_1576 < 1){
                goto L8; // [110] 193
            }

            /** 				if compare(s[i], key) <= 0 then*/
            _2 = (int)SEQ_PTR(_s_1547);
            _777 = (int)*(((s1_ptr)_2)->base + _i_1576);
            if (IS_ATOM_INT(_777) && IS_ATOM_INT(_key_1551)){
                _778 = (_777 < _key_1551) ? -1 : (_777 > _key_1551);
            }
            else{
                _778 = compare(_777, _key_1551);
            }
            _777 = NOVALUE;
            if (_778 > 0)
            goto L9; // [127] 177

            /** 					a = i+1*/
            _a_1552 = _i_1576 + 1;

            /** 					if a != j then*/
            if (_a_1552 == _j_1571)
            goto LA; // [141] 223

            /** 						s[a+1 .. j] = s[a .. j-1]*/
            _782 = _a_1552 + 1;
            _783 = _j_1571 - 1;
            rhs_slice_target = (object_ptr)&_784;
            RHS_Slice(_s_1547, _a_1552, _783);
            assign_slice_seq = (s1_ptr *)&_s_1547;
            AssignSlice(_782, _j_1571, _784);
            _782 = NOVALUE;
            DeRefDS(_784);
            _784 = NOVALUE;

            /** 						s[a] = key*/
            Ref(_key_1551);
            _2 = (int)SEQ_PTR(_s_1547);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_1547 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _a_1552);
            _1 = *(int *)_2;
            *(int *)_2 = _key_1551;
            DeRef(_1);

            /** 					continue "outer"*/
            goto LA; // [174] 223
L9: 

            /** 				a = i*/
            _a_1552 = _i_1576;

            /** 			end for*/
            _i_1576 = _i_1576 + -1;
            goto L7; // [188] 117
L8: 
            ;
        }

        /** 			s[a+1 .. j] = s[a .. j-1]*/
        _785 = _a_1552 + 1;
        if (_785 > MAXINT){
            _785 = NewDouble((double)_785);
        }
        _786 = _j_1571 - 1;
        rhs_slice_target = (object_ptr)&_787;
        RHS_Slice(_s_1547, _a_1552, _786);
        assign_slice_seq = (s1_ptr *)&_s_1547;
        AssignSlice(_785, _j_1571, _787);
        DeRef(_785);
        _785 = NOVALUE;
        DeRefDS(_787);
        _787 = NOVALUE;

        /** 			s[a] = key*/
        Ref(_key_1551);
        _2 = (int)SEQ_PTR(_s_1547);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_1547 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_1552);
        _1 = *(int *)_2;
        *(int *)_2 = _key_1551;
        DeRef(_1);

        /** 		end for*/
LA: 
        _j_1571 = _j_1571 + 1;
        goto L5; // [223] 98
L6: 
        ;
    }
    goto LB; // [228] 380
L4: 

    /** 		for j = 2 to length(s) label "outer" do*/
    if (IS_SEQUENCE(_s_1547)){
            _788 = SEQ_PTR(_s_1547)->length;
    }
    else {
        _788 = 1;
    }
    {
        int _j_1593;
        _j_1593 = 2;
LC: 
        if (_j_1593 > _788){
            goto LD; // [236] 379
        }

        /** 			key = s[j]*/
        DeRef(_key_1551);
        _2 = (int)SEQ_PTR(_s_1547);
        _key_1551 = (int)*(((s1_ptr)_2)->base + _j_1593);
        Ref(_key_1551);

        /** 			for i = j - 1 to 1 by -1 do*/
        _790 = _j_1593 - 1;
        {
            int _i_1597;
            _i_1597 = _790;
LE: 
            if (_i_1597 < 1){
                goto LF; // [255] 344
            }

            /** 				if call_func(compfunc,{s[i], key, userdata}) <= 0 then*/
            _2 = (int)SEQ_PTR(_s_1547);
            _791 = (int)*(((s1_ptr)_2)->base + _i_1597);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_791);
            *((int *)(_2+4)) = _791;
            Ref(_key_1551);
            *((int *)(_2+8)) = _key_1551;
            Ref(_userdata_1550);
            *((int *)(_2+12)) = _userdata_1550;
            _792 = MAKE_SEQ(_1);
            _791 = NOVALUE;
            _1 = (int)SEQ_PTR(_792);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_compfunc_1549].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            Ref(*(int *)(_2+12));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8), 
                                *(int *)(_2+12)
                                 );
            DeRef(_793);
            _793 = _1;
            DeRefDS(_792);
            _792 = NOVALUE;
            if (binary_op_a(GREATER, _793, 0)){
                DeRef(_793);
                _793 = NOVALUE;
                goto L10; // [278] 328
            }
            DeRef(_793);
            _793 = NOVALUE;

            /** 					a = i+1*/
            _a_1552 = _i_1597 + 1;

            /** 					if a != j then*/
            if (_a_1552 == _j_1593)
            goto L11; // [292] 374

            /** 						s[a+1 .. j] = s[a .. j-1]*/
            _797 = _a_1552 + 1;
            _798 = _j_1593 - 1;
            rhs_slice_target = (object_ptr)&_799;
            RHS_Slice(_s_1547, _a_1552, _798);
            assign_slice_seq = (s1_ptr *)&_s_1547;
            AssignSlice(_797, _j_1593, _799);
            _797 = NOVALUE;
            DeRefDS(_799);
            _799 = NOVALUE;

            /** 						s[a] = key*/
            Ref(_key_1551);
            _2 = (int)SEQ_PTR(_s_1547);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_1547 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _a_1552);
            _1 = *(int *)_2;
            *(int *)_2 = _key_1551;
            DeRef(_1);

            /** 					continue "outer"*/
            goto L11; // [325] 374
L10: 

            /** 				a = i*/
            _a_1552 = _i_1597;

            /** 			end for*/
            _i_1597 = _i_1597 + -1;
            goto LE; // [339] 262
LF: 
            ;
        }

        /** 			s[a+1 .. j] = s[a .. j-1]*/
        _800 = _a_1552 + 1;
        if (_800 > MAXINT){
            _800 = NewDouble((double)_800);
        }
        _801 = _j_1593 - 1;
        rhs_slice_target = (object_ptr)&_802;
        RHS_Slice(_s_1547, _a_1552, _801);
        assign_slice_seq = (s1_ptr *)&_s_1547;
        AssignSlice(_800, _j_1593, _802);
        DeRef(_800);
        _800 = NOVALUE;
        DeRefDS(_802);
        _802 = NOVALUE;

        /** 			s[a] = key*/
        Ref(_key_1551);
        _2 = (int)SEQ_PTR(_s_1547);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_1547 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_1552);
        _1 = *(int *)_2;
        *(int *)_2 = _key_1551;
        DeRef(_1);

        /** 		end for*/
L11: 
        _j_1593 = _j_1593 + 1;
        goto LC; // [374] 243
LD: 
        ;
    }
LB: 

    /** 	return s*/
    DeRef(_e_1548);
    DeRef(_userdata_1550);
    DeRef(_key_1551);
    DeRef(_776);
    _776 = NOVALUE;
    DeRef(_790);
    _790 = NOVALUE;
    DeRef(_771);
    _771 = NOVALUE;
    DeRef(_783);
    _783 = NOVALUE;
    DeRef(_786);
    _786 = NOVALUE;
    DeRef(_798);
    _798 = NOVALUE;
    DeRef(_801);
    _801 = NOVALUE;
    return _s_1547;
    ;
}



// 0xD18ABE45
