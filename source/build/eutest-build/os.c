// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _24instance()
{
    int _6137 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_INSTANCE, 0)*/
    _6137 = machine(55, 0);
    return _6137;
    ;
}


int _24get_pid()
{
    int _6139 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef UNIX then*/

    /** 		if cur_pid = -1 then*/
    if (binary_op_a(NOTEQ, _24cur_pid_10854, -1)){
        goto L1; // [7] 43
    }

    /** 			cur_pid = dll:define_c_func( dll:open_dll("kernel32.dll"), "GetCurrentProcessId", {}, dll:C_DWORD)*/
    RefDS(_1860);
    _6139 = _10open_dll(_1860);
    RefDS(_6140);
    RefDS(_5);
    _0 = _10define_c_func(_6139, _6140, _5, 33554436);
    DeRef(_24cur_pid_10854);
    _24cur_pid_10854 = _0;
    _6139 = NOVALUE;

    /** 			if cur_pid >= 0 then*/
    if (binary_op_a(LESS, _24cur_pid_10854, 0)){
        goto L2; // [28] 42
    }

    /** 				cur_pid = c_func(cur_pid, {})*/
    _0 = _24cur_pid_10854;
    _24cur_pid_10854 = call_c(1, _24cur_pid_10854, _5);
    DeRef(_0);
L2: 
L1: 

    /** 		return cur_pid*/
    Ref(_24cur_pid_10854);
    return _24cur_pid_10854;
    ;
}


int _24uname()
{
    int _buf_10873 = NOVALUE;
    int _sbuf_10874 = NOVALUE;
    int _maj_10875 = NOVALUE;
    int _mine_10876 = NOVALUE;
    int _build_10877 = NOVALUE;
    int _plat_10878 = NOVALUE;
    int _6252 = NOVALUE;
    int _6250 = NOVALUE;
    int _6249 = NOVALUE;
    int _6247 = NOVALUE;
    int _6246 = NOVALUE;
    int _6241 = NOVALUE;
    int _6240 = NOVALUE;
    int _6234 = NOVALUE;
    int _6233 = NOVALUE;
    int _6229 = NOVALUE;
    int _6228 = NOVALUE;
    int _6227 = NOVALUE;
    int _6224 = NOVALUE;
    int _6223 = NOVALUE;
    int _6222 = NOVALUE;
    int _6219 = NOVALUE;
    int _6218 = NOVALUE;
    int _6217 = NOVALUE;
    int _6214 = NOVALUE;
    int _6213 = NOVALUE;
    int _6212 = NOVALUE;
    int _6210 = NOVALUE;
    int _6209 = NOVALUE;
    int _6197 = NOVALUE;
    int _6196 = NOVALUE;
    int _6181 = NOVALUE;
    int _6180 = NOVALUE;
    int _6166 = NOVALUE;
    int _6165 = NOVALUE;
    int _6159 = NOVALUE;
    int _6157 = NOVALUE;
    int _6155 = NOVALUE;
    int _6153 = NOVALUE;
    int _6152 = NOVALUE;
    int _6151 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		atom buf*/

    /** 		sequence sbuf*/

    /** 		integer maj, mine, build, plat*/

    /** 		buf = machine:allocate(148)*/
    _0 = _buf_10873;
    _buf_10873 = _11allocate(148, 0);
    DeRef(_0);

    /** 		poke4(buf, 148)*/
    if (IS_ATOM_INT(_buf_10873)){
        poke4_addr = (unsigned long *)_buf_10873;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_buf_10873)->dbl);
    }
    *poke4_addr = (unsigned long)148;

    /** 		if c_func(M_UNAME, {buf}) then*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_buf_10873);
    *((int *)(_2+4)) = _buf_10873;
    _6151 = MAKE_SEQ(_1);
    _6152 = call_c(1, _24M_UNAME_10865, _6151);
    DeRefDS(_6151);
    _6151 = NOVALUE;
    if (_6152 == 0) {
        DeRef(_6152);
        _6152 = NOVALUE;
        goto L1; // [34] 566
    }
    else {
        if (!IS_ATOM_INT(_6152) && DBL_PTR(_6152)->dbl == 0.0){
            DeRef(_6152);
            _6152 = NOVALUE;
            goto L1; // [34] 566
        }
        DeRef(_6152);
        _6152 = NOVALUE;
    }
    DeRef(_6152);
    _6152 = NOVALUE;

    /** 			maj = peek4u(buf+4)*/
    if (IS_ATOM_INT(_buf_10873)) {
        _6153 = _buf_10873 + 4;
        if ((long)((unsigned long)_6153 + (unsigned long)HIGH_BITS) >= 0) 
        _6153 = NewDouble((double)_6153);
    }
    else {
        _6153 = NewDouble(DBL_PTR(_buf_10873)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_6153)) {
        _maj_10875 = *(unsigned long *)_6153;
        if ((unsigned)_maj_10875 > (unsigned)MAXINT)
        _maj_10875 = NewDouble((double)(unsigned long)_maj_10875);
    }
    else {
        _maj_10875 = *(unsigned long *)(unsigned long)(DBL_PTR(_6153)->dbl);
        if ((unsigned)_maj_10875 > (unsigned)MAXINT)
        _maj_10875 = NewDouble((double)(unsigned long)_maj_10875);
    }
    DeRef(_6153);
    _6153 = NOVALUE;
    if (!IS_ATOM_INT(_maj_10875)) {
        _1 = (long)(DBL_PTR(_maj_10875)->dbl);
        if (UNIQUE(DBL_PTR(_maj_10875)) && (DBL_PTR(_maj_10875)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maj_10875);
        _maj_10875 = _1;
    }

    /** 			mine = peek4u(buf+8)*/
    if (IS_ATOM_INT(_buf_10873)) {
        _6155 = _buf_10873 + 8;
        if ((long)((unsigned long)_6155 + (unsigned long)HIGH_BITS) >= 0) 
        _6155 = NewDouble((double)_6155);
    }
    else {
        _6155 = NewDouble(DBL_PTR(_buf_10873)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_6155)) {
        _mine_10876 = *(unsigned long *)_6155;
        if ((unsigned)_mine_10876 > (unsigned)MAXINT)
        _mine_10876 = NewDouble((double)(unsigned long)_mine_10876);
    }
    else {
        _mine_10876 = *(unsigned long *)(unsigned long)(DBL_PTR(_6155)->dbl);
        if ((unsigned)_mine_10876 > (unsigned)MAXINT)
        _mine_10876 = NewDouble((double)(unsigned long)_mine_10876);
    }
    DeRef(_6155);
    _6155 = NOVALUE;
    if (!IS_ATOM_INT(_mine_10876)) {
        _1 = (long)(DBL_PTR(_mine_10876)->dbl);
        if (UNIQUE(DBL_PTR(_mine_10876)) && (DBL_PTR(_mine_10876)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mine_10876);
        _mine_10876 = _1;
    }

    /** 			build = peek4u(buf+12)*/
    if (IS_ATOM_INT(_buf_10873)) {
        _6157 = _buf_10873 + 12;
        if ((long)((unsigned long)_6157 + (unsigned long)HIGH_BITS) >= 0) 
        _6157 = NewDouble((double)_6157);
    }
    else {
        _6157 = NewDouble(DBL_PTR(_buf_10873)->dbl + (double)12);
    }
    if (IS_ATOM_INT(_6157)) {
        _build_10877 = *(unsigned long *)_6157;
        if ((unsigned)_build_10877 > (unsigned)MAXINT)
        _build_10877 = NewDouble((double)(unsigned long)_build_10877);
    }
    else {
        _build_10877 = *(unsigned long *)(unsigned long)(DBL_PTR(_6157)->dbl);
        if ((unsigned)_build_10877 > (unsigned)MAXINT)
        _build_10877 = NewDouble((double)(unsigned long)_build_10877);
    }
    DeRef(_6157);
    _6157 = NOVALUE;
    if (!IS_ATOM_INT(_build_10877)) {
        _1 = (long)(DBL_PTR(_build_10877)->dbl);
        if (UNIQUE(DBL_PTR(_build_10877)) && (DBL_PTR(_build_10877)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_build_10877);
        _build_10877 = _1;
    }

    /** 			plat = peek4u(buf+16)*/
    if (IS_ATOM_INT(_buf_10873)) {
        _6159 = _buf_10873 + 16;
        if ((long)((unsigned long)_6159 + (unsigned long)HIGH_BITS) >= 0) 
        _6159 = NewDouble((double)_6159);
    }
    else {
        _6159 = NewDouble(DBL_PTR(_buf_10873)->dbl + (double)16);
    }
    if (IS_ATOM_INT(_6159)) {
        _plat_10878 = *(unsigned long *)_6159;
        if ((unsigned)_plat_10878 > (unsigned)MAXINT)
        _plat_10878 = NewDouble((double)(unsigned long)_plat_10878);
    }
    else {
        _plat_10878 = *(unsigned long *)(unsigned long)(DBL_PTR(_6159)->dbl);
        if ((unsigned)_plat_10878 > (unsigned)MAXINT)
        _plat_10878 = NewDouble((double)(unsigned long)_plat_10878);
    }
    DeRef(_6159);
    _6159 = NOVALUE;
    if (!IS_ATOM_INT(_plat_10878)) {
        _1 = (long)(DBL_PTR(_plat_10878)->dbl);
        if (UNIQUE(DBL_PTR(_plat_10878)) && (DBL_PTR(_plat_10878)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_plat_10878);
        _plat_10878 = _1;
    }

    /** 			sbuf = {}*/
    RefDS(_5);
    DeRef(_sbuf_10874);
    _sbuf_10874 = _5;

    /** 			if plat = 0 then*/
    if (_plat_10878 != 0)
    goto L2; // [98] 125

    /** 				sbuf = append(sbuf, "Win32s")*/
    RefDS(_6162);
    Append(&_sbuf_10874, _sbuf_10874, _6162);

    /** 				sbuf = append(sbuf, sprintf("Windows %d.%d", {maj,mine}))*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _maj_10875;
    ((int *)_2)[2] = _mine_10876;
    _6165 = MAKE_SEQ(_1);
    _6166 = EPrintf(-9999999, _6164, _6165);
    DeRefDS(_6165);
    _6165 = NOVALUE;
    RefDS(_6166);
    Append(&_sbuf_10874, _sbuf_10874, _6166);
    DeRefDS(_6166);
    _6166 = NOVALUE;
    goto L3; // [122] 526
L2: 

    /** 			elsif plat = 1 then*/
    if (_plat_10878 != 1)
    goto L4; // [127] 200

    /** 				sbuf = append(sbuf, "Win9x")*/
    RefDS(_6169);
    Append(&_sbuf_10874, _sbuf_10874, _6169);

    /** 				if mine = 0 then*/
    if (_mine_10876 != 0)
    goto L5; // [139] 152

    /** 					sbuf = append(sbuf, "Win95")*/
    RefDS(_6172);
    Append(&_sbuf_10874, _sbuf_10874, _6172);
    goto L3; // [149] 526
L5: 

    /** 				elsif mine = 10 then*/
    if (_mine_10876 != 10)
    goto L6; // [154] 167

    /** 					sbuf = append(sbuf, "Win98")*/
    RefDS(_6175);
    Append(&_sbuf_10874, _sbuf_10874, _6175);
    goto L3; // [164] 526
L6: 

    /** 				elsif mine = 90 then*/
    if (_mine_10876 != 90)
    goto L7; // [169] 182

    /** 					sbuf = append(sbuf, "WinME")*/
    RefDS(_6178);
    Append(&_sbuf_10874, _sbuf_10874, _6178);
    goto L3; // [179] 526
L7: 

    /** 					sbuf = append(sbuf, sprintf("Windows %d.%d", {maj,mine}))*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _maj_10875;
    ((int *)_2)[2] = _mine_10876;
    _6180 = MAKE_SEQ(_1);
    _6181 = EPrintf(-9999999, _6164, _6180);
    DeRefDS(_6180);
    _6180 = NOVALUE;
    RefDS(_6181);
    Append(&_sbuf_10874, _sbuf_10874, _6181);
    DeRefDS(_6181);
    _6181 = NOVALUE;
    goto L3; // [197] 526
L4: 

    /** 			elsif plat = 2 then*/
    if (_plat_10878 != 2)
    goto L8; // [202] 476

    /** 				sbuf = append(sbuf, "WinNT")*/
    RefDS(_6184);
    Append(&_sbuf_10874, _sbuf_10874, _6184);

    /** 				if maj = 6 then*/
    if (_maj_10875 != 6)
    goto L9; // [214] 281

    /** 					if mine = 0 then*/
    if (_mine_10876 != 0)
    goto LA; // [220] 233

    /** 						sbuf = append(sbuf, "Vista") -- or Win2008*/
    RefDS(_6188);
    Append(&_sbuf_10874, _sbuf_10874, _6188);
    goto L3; // [230] 526
LA: 

    /** 					elsif mine = 1 then*/
    if (_mine_10876 != 1)
    goto LB; // [235] 248

    /** 						sbuf = append(sbuf, "Windows7") -- or Win2008 R2*/
    RefDS(_6191);
    Append(&_sbuf_10874, _sbuf_10874, _6191);
    goto L3; // [245] 526
LB: 

    /** 					elsif mine = 2 then*/
    if (_mine_10876 != 2)
    goto LC; // [250] 263

    /** 						sbuf = append(sbuf, "Windows8") -- or Win2012*/
    RefDS(_6194);
    Append(&_sbuf_10874, _sbuf_10874, _6194);
    goto L3; // [260] 526
LC: 

    /** 						sbuf = append(sbuf, sprintf("Windows %d.%d", {maj,mine}))*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _maj_10875;
    ((int *)_2)[2] = _mine_10876;
    _6196 = MAKE_SEQ(_1);
    _6197 = EPrintf(-9999999, _6164, _6196);
    DeRefDS(_6196);
    _6196 = NOVALUE;
    RefDS(_6197);
    Append(&_sbuf_10874, _sbuf_10874, _6197);
    DeRefDS(_6197);
    _6197 = NOVALUE;
    goto L3; // [278] 526
L9: 

    /** 				elsif maj = 5 then*/
    if (_maj_10875 != 5)
    goto LD; // [283] 350

    /** 					if mine = 0 then*/
    if (_mine_10876 != 0)
    goto LE; // [289] 302

    /** 						sbuf = append(sbuf, "Win2K")*/
    RefDS(_6201);
    Append(&_sbuf_10874, _sbuf_10874, _6201);
    goto L3; // [299] 526
LE: 

    /** 					elsif mine = 1 then*/
    if (_mine_10876 != 1)
    goto LF; // [304] 317

    /** 						sbuf = append(sbuf, "WinXP")*/
    RefDS(_6204);
    Append(&_sbuf_10874, _sbuf_10874, _6204);
    goto L3; // [314] 526
LF: 

    /** 					elsif mine = 2 then*/
    if (_mine_10876 != 2)
    goto L10; // [319] 332

    /** 						sbuf = append(sbuf, "Win2003") -- according to http://msdn.microsoft.com/en-us/library/windows/desktop/ms724834%28v=vs.85%29.aspx*/
    RefDS(_6207);
    Append(&_sbuf_10874, _sbuf_10874, _6207);
    goto L3; // [329] 526
L10: 

    /** 						sbuf = append(sbuf, sprintf("Windows %d.%d", {maj,mine}))*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _maj_10875;
    ((int *)_2)[2] = _mine_10876;
    _6209 = MAKE_SEQ(_1);
    _6210 = EPrintf(-9999999, _6164, _6209);
    DeRefDS(_6209);
    _6209 = NOVALUE;
    RefDS(_6210);
    Append(&_sbuf_10874, _sbuf_10874, _6210);
    DeRefDS(_6210);
    _6210 = NOVALUE;
    goto L3; // [347] 526
LD: 

    /** 				elsif maj = 4 and mine = 0 then*/
    _6212 = (_maj_10875 == 4);
    if (_6212 == 0) {
        goto L11; // [356] 377
    }
    _6214 = (_mine_10876 == 0);
    if (_6214 == 0)
    {
        DeRef(_6214);
        _6214 = NOVALUE;
        goto L11; // [365] 377
    }
    else{
        DeRef(_6214);
        _6214 = NOVALUE;
    }

    /** 					sbuf = append(sbuf, "WinNT 4.0")*/
    RefDS(_6215);
    Append(&_sbuf_10874, _sbuf_10874, _6215);
    goto L3; // [374] 526
L11: 

    /** 				elsif maj = 3 and mine = 51 then*/
    _6217 = (_maj_10875 == 3);
    if (_6217 == 0) {
        goto L12; // [383] 404
    }
    _6219 = (_mine_10876 == 51);
    if (_6219 == 0)
    {
        DeRef(_6219);
        _6219 = NOVALUE;
        goto L12; // [392] 404
    }
    else{
        DeRef(_6219);
        _6219 = NOVALUE;
    }

    /** 					sbuf = append(sbuf, "WinNT 3.51")*/
    RefDS(_6220);
    Append(&_sbuf_10874, _sbuf_10874, _6220);
    goto L3; // [401] 526
L12: 

    /** 				elsif maj = 3 and mine = 50 then --is it 50 or 5?*/
    _6222 = (_maj_10875 == 3);
    if (_6222 == 0) {
        goto L13; // [410] 431
    }
    _6224 = (_mine_10876 == 50);
    if (_6224 == 0)
    {
        DeRef(_6224);
        _6224 = NOVALUE;
        goto L13; // [419] 431
    }
    else{
        DeRef(_6224);
        _6224 = NOVALUE;
    }

    /** 					sbuf = append(sbuf, "WinNT 3.5")*/
    RefDS(_6225);
    Append(&_sbuf_10874, _sbuf_10874, _6225);
    goto L3; // [428] 526
L13: 

    /** 				elsif maj = 3 and mine = 1 then*/
    _6227 = (_maj_10875 == 3);
    if (_6227 == 0) {
        goto L14; // [437] 458
    }
    _6229 = (_mine_10876 == 1);
    if (_6229 == 0)
    {
        DeRef(_6229);
        _6229 = NOVALUE;
        goto L14; // [446] 458
    }
    else{
        DeRef(_6229);
        _6229 = NOVALUE;
    }

    /** 					sbuf = append(sbuf, "WinNT 3.1")*/
    RefDS(_6230);
    Append(&_sbuf_10874, _sbuf_10874, _6230);
    goto L3; // [455] 526
L14: 

    /** 					sbuf = append(sbuf, sprintf("WinNT %d.%d", {maj,mine}))*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _maj_10875;
    ((int *)_2)[2] = _mine_10876;
    _6233 = MAKE_SEQ(_1);
    _6234 = EPrintf(-9999999, _6232, _6233);
    DeRefDS(_6233);
    _6233 = NOVALUE;
    RefDS(_6234);
    Append(&_sbuf_10874, _sbuf_10874, _6234);
    DeRefDS(_6234);
    _6234 = NOVALUE;
    goto L3; // [473] 526
L8: 

    /** 			elsif plat = 3 then*/
    if (_plat_10878 != 3)
    goto L15; // [478] 505

    /** 				sbuf = append(sbuf, "WinCE")*/
    RefDS(_6237);
    Append(&_sbuf_10874, _sbuf_10874, _6237);

    /** 				sbuf = append(sbuf, sprintf("WinCE %d.%d", {maj,mine}))*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _maj_10875;
    ((int *)_2)[2] = _mine_10876;
    _6240 = MAKE_SEQ(_1);
    _6241 = EPrintf(-9999999, _6239, _6240);
    DeRefDS(_6240);
    _6240 = NOVALUE;
    RefDS(_6241);
    Append(&_sbuf_10874, _sbuf_10874, _6241);
    DeRefDS(_6241);
    _6241 = NOVALUE;
    goto L3; // [502] 526
L15: 

    /** 				sbuf = append(sbuf, "Unknown Windows")*/
    RefDS(_6243);
    Append(&_sbuf_10874, _sbuf_10874, _6243);

    /** 				sbuf = append(sbuf, sprintf("Version %d.%d", {maj,mine}))*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _maj_10875;
    ((int *)_2)[2] = _mine_10876;
    _6246 = MAKE_SEQ(_1);
    _6247 = EPrintf(-9999999, _6245, _6246);
    DeRefDS(_6246);
    _6246 = NOVALUE;
    RefDS(_6247);
    Append(&_sbuf_10874, _sbuf_10874, _6247);
    DeRefDS(_6247);
    _6247 = NOVALUE;
L3: 

    /** 			sbuf = append(sbuf, peek_string(buf+20))*/
    if (IS_ATOM_INT(_buf_10873)) {
        _6249 = _buf_10873 + 20;
        if ((long)((unsigned long)_6249 + (unsigned long)HIGH_BITS) >= 0) 
        _6249 = NewDouble((double)_6249);
    }
    else {
        _6249 = NewDouble(DBL_PTR(_buf_10873)->dbl + (double)20);
    }
    if (IS_ATOM_INT(_6249)) {
        _6250 =  NewString((char *)_6249);
    }
    else {
        _6250 = NewString((char *)(unsigned long)(DBL_PTR(_6249)->dbl));
    }
    DeRef(_6249);
    _6249 = NOVALUE;
    RefDS(_6250);
    Append(&_sbuf_10874, _sbuf_10874, _6250);
    DeRefDS(_6250);
    _6250 = NOVALUE;

    /** 			sbuf &= {plat, build, mine, maj}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _plat_10878;
    *((int *)(_2+8)) = _build_10877;
    *((int *)(_2+12)) = _mine_10876;
    *((int *)(_2+16)) = _maj_10875;
    _6252 = MAKE_SEQ(_1);
    Concat((object_ptr)&_sbuf_10874, _sbuf_10874, _6252);
    DeRefDS(_6252);
    _6252 = NOVALUE;

    /** 			machine:free( buf )*/
    Ref(_buf_10873);
    _11free(_buf_10873);

    /** 			return sbuf*/
    DeRef(_buf_10873);
    DeRef(_6212);
    _6212 = NOVALUE;
    DeRef(_6217);
    _6217 = NOVALUE;
    DeRef(_6222);
    _6222 = NOVALUE;
    DeRef(_6227);
    _6227 = NOVALUE;
    return _sbuf_10874;
    goto L16; // [563] 578
L1: 

    /** 			machine:free( buf )*/
    Ref(_buf_10873);
    _11free(_buf_10873);

    /** 			return {}*/
    RefDS(_5);
    DeRef(_buf_10873);
    DeRef(_sbuf_10874);
    DeRef(_6212);
    _6212 = NOVALUE;
    DeRef(_6217);
    _6217 = NOVALUE;
    DeRef(_6222);
    _6222 = NOVALUE;
    DeRef(_6227);
    _6227 = NOVALUE;
    return _5;
L16: 
    ;
}


int _24is_win_nt()
{
    int _s_11013 = NOVALUE;
    int _6257 = NOVALUE;
    int _6256 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		sequence s*/

    /** 		s = uname()*/
    _0 = _s_11013;
    _s_11013 = _24uname();
    DeRef(_0);

    /** 		return equal(s[1], "WinNT")*/
    _2 = (int)SEQ_PTR(_s_11013);
    _6256 = (int)*(((s1_ptr)_2)->base + 1);
    if (_6256 == _6184)
    _6257 = 1;
    else if (IS_ATOM_INT(_6256) && IS_ATOM_INT(_6184))
    _6257 = 0;
    else
    _6257 = (compare(_6256, _6184) == 0);
    _6256 = NOVALUE;
    DeRefDS(_s_11013);
    return _6257;
    ;
}


int _24setenv(int _name_11019, int _val_11020, int _overwrite_11021)
{
    int _6259 = NOVALUE;
    int _6258 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_11021)) {
        _1 = (long)(DBL_PTR(_overwrite_11021)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_11021)) && (DBL_PTR(_overwrite_11021)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_11021);
        _overwrite_11021 = _1;
    }

    /** 	return machine_func(M_SET_ENV, {name, val, overwrite})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_name_11019);
    *((int *)(_2+4)) = _name_11019;
    RefDS(_val_11020);
    *((int *)(_2+8)) = _val_11020;
    *((int *)(_2+12)) = _overwrite_11021;
    _6258 = MAKE_SEQ(_1);
    _6259 = machine(73, _6258);
    DeRefDS(_6258);
    _6258 = NOVALUE;
    DeRefDS(_name_11019);
    DeRefDS(_val_11020);
    return _6259;
    ;
}


int _24unsetenv(int _env_11026)
{
    int _6261 = NOVALUE;
    int _6260 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_UNSET_ENV, {env})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_env_11026);
    *((int *)(_2+4)) = _env_11026;
    _6260 = MAKE_SEQ(_1);
    _6261 = machine(74, _6260);
    DeRefDS(_6260);
    _6260 = NOVALUE;
    DeRefDS(_env_11026);
    return _6261;
    ;
}


void _24sleep(int _t_11031)
{
    int _0, _1, _2;
    

    /** 	if t >= 0 then*/
    if (binary_op_a(LESS, _t_11031, 0)){
        goto L1; // [3] 13
    }

    /** 		machine_proc(M_SLEEP, t)*/
    machine(64, _t_11031);
L1: 

    /** end procedure*/
    DeRef(_t_11031);
    return;
    ;
}



// 0x06C1CF6C
