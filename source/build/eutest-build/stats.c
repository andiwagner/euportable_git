// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _28massage(int _data_set_11339, int _subseq_opt_11340)
{
    int _6425 = NOVALUE;
    int _6424 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch subseq_opt do*/
    _0 = _subseq_opt_11340;
    switch ( _0 ){ 

        /** 		case ST_IGNSTR then*/
        case 2:

        /** 			return stdseq:remove_subseq(data_set, stdseq:SEQ_NOALT)*/
        RefDS(_data_set_11339);
        RefDS(_3SEQ_NOALT_3105);
        _6424 = _3remove_subseq(_data_set_11339, _3SEQ_NOALT_3105);
        DeRefDS(_data_set_11339);
        return _6424;
        goto L1; // [27] 57

        /** 		case ST_ZEROSTR then*/
        case 3:

        /** 			return stdseq:remove_subseq(data_set, 0)*/
        RefDS(_data_set_11339);
        _6425 = _3remove_subseq(_data_set_11339, 0);
        DeRefDS(_data_set_11339);
        DeRef(_6424);
        _6424 = NOVALUE;
        return _6425;
        goto L1; // [44] 57

        /** 		case else*/
        default:

        /** 			return data_set*/
        DeRef(_6424);
        _6424 = NOVALUE;
        DeRef(_6425);
        _6425 = NOVALUE;
        return _data_set_11339;
    ;}L1: 
    ;
}


int _28stdev(int _data_set_11350, int _subseq_opt_11351, int _population_type_11352)
{
    int _lSum_11353 = NOVALUE;
    int _lMean_11354 = NOVALUE;
    int _lCnt_11355 = NOVALUE;
    int _6442 = NOVALUE;
    int _6441 = NOVALUE;
    int _6437 = NOVALUE;
    int _6436 = NOVALUE;
    int _6435 = NOVALUE;
    int _6434 = NOVALUE;
    int _6431 = NOVALUE;
    int _6430 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data_set = massage(data_set, subseq_opt)*/
    RefDS(_data_set_11350);
    _0 = _data_set_11350;
    _data_set_11350 = _28massage(_data_set_11350, 1);
    DeRefDSi(_0);

    /** 	lCnt = length(data_set)*/
    if (IS_SEQUENCE(_data_set_11350)){
            _lCnt_11355 = SEQ_PTR(_data_set_11350)->length;
    }
    else {
        _lCnt_11355 = 1;
    }

    /** 	if lCnt = 0 then*/
    if (_lCnt_11355 != 0)
    goto L1; // [25] 36

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_data_set_11350);
    DeRef(_lSum_11353);
    DeRef(_lMean_11354);
    return _5;
L1: 

    /** 	if lCnt = 1 then*/
    if (_lCnt_11355 != 1)
    goto L2; // [38] 49

    /** 		return 0*/
    DeRefDS(_data_set_11350);
    DeRef(_lSum_11353);
    DeRef(_lMean_11354);
    return 0;
L2: 

    /** 	lSum = 0*/
    DeRef(_lSum_11353);
    _lSum_11353 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_11350)){
            _6430 = SEQ_PTR(_data_set_11350)->length;
    }
    else {
        _6430 = 1;
    }
    {
        int _i_11363;
        _i_11363 = 1;
L3: 
        if (_i_11363 > _6430){
            goto L4; // [59] 83
        }

        /** 		lSum += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_11350);
        _6431 = (int)*(((s1_ptr)_2)->base + _i_11363);
        _0 = _lSum_11353;
        if (IS_ATOM_INT(_lSum_11353) && IS_ATOM_INT(_6431)) {
            _lSum_11353 = _lSum_11353 + _6431;
            if ((long)((unsigned long)_lSum_11353 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_11353 = NewDouble((double)_lSum_11353);
        }
        else {
            _lSum_11353 = binary_op(PLUS, _lSum_11353, _6431);
        }
        DeRef(_0);
        _6431 = NOVALUE;

        /** 	end for*/
        _i_11363 = _i_11363 + 1;
        goto L3; // [78] 66
L4: 
        ;
    }

    /** 	lMean = lSum / lCnt*/
    DeRef(_lMean_11354);
    if (IS_ATOM_INT(_lSum_11353)) {
        _lMean_11354 = (_lSum_11353 % _lCnt_11355) ? NewDouble((double)_lSum_11353 / _lCnt_11355) : (_lSum_11353 / _lCnt_11355);
    }
    else {
        _lMean_11354 = NewDouble(DBL_PTR(_lSum_11353)->dbl / (double)_lCnt_11355);
    }

    /** 	lSum = 0*/
    DeRef(_lSum_11353);
    _lSum_11353 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_11350)){
            _6434 = SEQ_PTR(_data_set_11350)->length;
    }
    else {
        _6434 = 1;
    }
    {
        int _i_11369;
        _i_11369 = 1;
L5: 
        if (_i_11369 > _6434){
            goto L6; // [99] 131
        }

        /** 		lSum += power(data_set[i] - lMean, 2)*/
        _2 = (int)SEQ_PTR(_data_set_11350);
        _6435 = (int)*(((s1_ptr)_2)->base + _i_11369);
        if (IS_ATOM_INT(_6435) && IS_ATOM_INT(_lMean_11354)) {
            _6436 = _6435 - _lMean_11354;
            if ((long)((unsigned long)_6436 +(unsigned long) HIGH_BITS) >= 0){
                _6436 = NewDouble((double)_6436);
            }
        }
        else {
            _6436 = binary_op(MINUS, _6435, _lMean_11354);
        }
        _6435 = NOVALUE;
        if (IS_ATOM_INT(_6436) && IS_ATOM_INT(_6436)) {
            if (_6436 == (short)_6436 && _6436 <= INT15 && _6436 >= -INT15)
            _6437 = _6436 * _6436;
            else
            _6437 = NewDouble(_6436 * (double)_6436);
        }
        else {
            _6437 = binary_op(MULTIPLY, _6436, _6436);
        }
        DeRef(_6436);
        _6436 = NOVALUE;
        _6436 = NOVALUE;
        _0 = _lSum_11353;
        if (IS_ATOM_INT(_lSum_11353) && IS_ATOM_INT(_6437)) {
            _lSum_11353 = _lSum_11353 + _6437;
            if ((long)((unsigned long)_lSum_11353 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_11353 = NewDouble((double)_lSum_11353);
        }
        else {
            _lSum_11353 = binary_op(PLUS, _lSum_11353, _6437);
        }
        DeRef(_0);
        DeRef(_6437);
        _6437 = NOVALUE;

        /** 	end for*/
        _i_11369 = _i_11369 + 1;
        goto L5; // [126] 106
L6: 
        ;
    }

    /** 	if population_type = ST_SAMPLE then*/
    if (_population_type_11352 != 2)
    goto L7; // [135] 148

    /** 		lCnt -= 1*/
    _lCnt_11355 = _lCnt_11355 - 1;
L7: 

    /** 	return power(lSum / lCnt, 0.5)*/
    if (IS_ATOM_INT(_lSum_11353)) {
        _6441 = (_lSum_11353 % _lCnt_11355) ? NewDouble((double)_lSum_11353 / _lCnt_11355) : (_lSum_11353 / _lCnt_11355);
    }
    else {
        _6441 = NewDouble(DBL_PTR(_lSum_11353)->dbl / (double)_lCnt_11355);
    }
    if (IS_ATOM_INT(_6441)) {
        temp_d.dbl = (double)_6441;
        _6442 = Dpower(&temp_d, DBL_PTR(_1692));
    }
    else {
        _6442 = Dpower(DBL_PTR(_6441), DBL_PTR(_1692));
    }
    DeRef(_6441);
    _6441 = NOVALUE;
    DeRefDS(_data_set_11350);
    DeRef(_lSum_11353);
    DeRef(_lMean_11354);
    return _6442;
    ;
}


int _28sum(int _data_set_11419, int _subseq_opt_11420)
{
    int _result__11421 = NOVALUE;
    int _6466 = NOVALUE;
    int _6465 = NOVALUE;
    int _6463 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _6463 = IS_ATOM(_data_set_11419);
    if (_6463 == 0)
    {
        _6463 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _6463 = NOVALUE;
    }

    /** 		return data_set*/
    DeRef(_result__11421);
    return _data_set_11419;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_11419);
    _0 = _data_set_11419;
    _data_set_11419 = _28massage(_data_set_11419, _subseq_opt_11420);
    DeRef(_0);

    /** 	result_ = 0*/
    DeRef(_result__11421);
    _result__11421 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_11419)){
            _6465 = SEQ_PTR(_data_set_11419)->length;
    }
    else {
        _6465 = 1;
    }
    {
        int _i_11426;
        _i_11426 = 1;
L2: 
        if (_i_11426 > _6465){
            goto L3; // [33] 57
        }

        /** 		result_ += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_11419);
        _6466 = (int)*(((s1_ptr)_2)->base + _i_11426);
        _0 = _result__11421;
        if (IS_ATOM_INT(_result__11421) && IS_ATOM_INT(_6466)) {
            _result__11421 = _result__11421 + _6466;
            if ((long)((unsigned long)_result__11421 + (unsigned long)HIGH_BITS) >= 0) 
            _result__11421 = NewDouble((double)_result__11421);
        }
        else {
            _result__11421 = binary_op(PLUS, _result__11421, _6466);
        }
        DeRef(_0);
        _6466 = NOVALUE;

        /** 	end for*/
        _i_11426 = _i_11426 + 1;
        goto L2; // [52] 40
L3: 
        ;
    }

    /** 	return result_*/
    DeRef(_data_set_11419);
    return _result__11421;
    ;
}


int _28average(int _data_set_11440, int _subseq_opt_11441)
{
    int _6477 = NOVALUE;
    int _6476 = NOVALUE;
    int _6475 = NOVALUE;
    int _6473 = NOVALUE;
    int _6471 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _6471 = 0;
    if (_6471 == 0)
    {
        _6471 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _6471 = NOVALUE;
    }

    /** 		return data_set*/
    return _data_set_11440;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_11440);
    _0 = _data_set_11440;
    _data_set_11440 = _28massage(_data_set_11440, _subseq_opt_11441);
    DeRef(_0);

    /** 	if length(data_set) = 0 then*/
    if (IS_SEQUENCE(_data_set_11440)){
            _6473 = SEQ_PTR(_data_set_11440)->length;
    }
    else {
        _6473 = 1;
    }
    if (_6473 != 0)
    goto L2; // [28] 39

    /** 		return {}*/
    RefDS(_5);
    DeRef(_data_set_11440);
    return _5;
L2: 

    /** 	return sum(data_set) / length(data_set)*/
    Ref(_data_set_11440);
    _6475 = _28sum(_data_set_11440, 1);
    if (IS_SEQUENCE(_data_set_11440)){
            _6476 = SEQ_PTR(_data_set_11440)->length;
    }
    else {
        _6476 = 1;
    }
    if (IS_ATOM_INT(_6475)) {
        _6477 = (_6475 % _6476) ? NewDouble((double)_6475 / _6476) : (_6475 / _6476);
    }
    else {
        _6477 = binary_op(DIVIDE, _6475, _6476);
    }
    DeRef(_6475);
    _6475 = NOVALUE;
    _6476 = NOVALUE;
    DeRef(_data_set_11440);
    return _6477;
    ;
}



// 0x2F9A77B3
