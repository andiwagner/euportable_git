// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _1error_class(int _i_18700)
{
    int _10514 = NOVALUE;
    int _10513 = NOVALUE;
    int _10512 = NOVALUE;
    int _10511 = NOVALUE;
    int _10510 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return integer(i) and i >= 1 and i <= E_EUTEST*/
    if (IS_ATOM_INT(_i_18700))
    _10510 = 1;
    else if (IS_ATOM_DBL(_i_18700))
    _10510 = IS_ATOM_INT(DoubleToInt(_i_18700));
    else
    _10510 = 0;
    if (IS_ATOM_INT(_i_18700)) {
        _10511 = (_i_18700 >= 1);
    }
    else {
        _10511 = binary_op(GREATEREQ, _i_18700, 1);
    }
    if (IS_ATOM_INT(_10511)) {
        _10512 = (_10510 != 0 && _10511 != 0);
    }
    else {
        _10512 = binary_op(AND, _10510, _10511);
    }
    _10510 = NOVALUE;
    DeRef(_10511);
    _10511 = NOVALUE;
    if (IS_ATOM_INT(_i_18700)) {
        _10513 = (_i_18700 <= 8);
    }
    else {
        _10513 = binary_op(LESSEQ, _i_18700, 8);
    }
    if (IS_ATOM_INT(_10512) && IS_ATOM_INT(_10513)) {
        _10514 = (_10512 != 0 && _10513 != 0);
    }
    else {
        _10514 = binary_op(AND, _10512, _10513);
    }
    DeRef(_10512);
    _10512 = NOVALUE;
    DeRef(_10513);
    _10513 = NOVALUE;
    DeRef(_i_18700);
    return _10514;
    ;
}


void _1error(int _file_18708, int _e_18709, int _message_18710, int _vals_18711, int _error_file_18712)
{
    int _lines_18713 = NOVALUE;
    int _bad_string_flag_18714 = NOVALUE;
    int _10536 = NOVALUE;
    int _10535 = NOVALUE;
    int _10534 = NOVALUE;
    int _10533 = NOVALUE;
    int _10532 = NOVALUE;
    int _10531 = NOVALUE;
    int _10530 = NOVALUE;
    int _10529 = NOVALUE;
    int _10528 = NOVALUE;
    int _10527 = NOVALUE;
    int _10526 = NOVALUE;
    int _10525 = NOVALUE;
    int _10524 = NOVALUE;
    int _10523 = NOVALUE;
    int _10522 = NOVALUE;
    int _10521 = NOVALUE;
    int _10520 = NOVALUE;
    int _10519 = NOVALUE;
    int _10518 = NOVALUE;
    int _10517 = NOVALUE;
    int _10515 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer bad_string_flag = 0*/
    _bad_string_flag_18714 = 0;

    /** 	if sequence(error_file) then*/
    _10515 = IS_SEQUENCE(_error_file_18712);
    if (_10515 == 0)
    {
        _10515 = NOVALUE;
        goto L1; // [19] 31
    }
    else{
        _10515 = NOVALUE;
    }

    /** 		lines = read_lines(error_file)*/
    Ref(_error_file_18712);
    _0 = _lines_18713;
    _lines_18713 = _15read_lines(_error_file_18712);
    DeRef(_0);
    goto L2; // [28] 37
L1: 

    /** 		lines = 0*/
    DeRef(_lines_18713);
    _lines_18713 = 0;
L2: 

    /** 	for i = 1 to length(vals) do*/
    if (IS_SEQUENCE(_vals_18711)){
            _10517 = SEQ_PTR(_vals_18711)->length;
    }
    else {
        _10517 = 1;
    }
    {
        int _i_18721;
        _i_18721 = 1;
L3: 
        if (_i_18721 > _10517){
            goto L4; // [42] 95
        }

        /** 		if not atom(vals[i]) and not ascii_string(vals[i]) then*/
        _2 = (int)SEQ_PTR(_vals_18711);
        _10518 = (int)*(((s1_ptr)_2)->base + _i_18721);
        _10519 = IS_ATOM(_10518);
        _10518 = NOVALUE;
        _10520 = (_10519 == 0);
        _10519 = NOVALUE;
        if (_10520 == 0) {
            goto L5; // [61] 88
        }
        _2 = (int)SEQ_PTR(_vals_18711);
        _10522 = (int)*(((s1_ptr)_2)->base + _i_18721);
        Ref(_10522);
        _10523 = _5ascii_string(_10522);
        _10522 = NOVALUE;
        if (IS_ATOM_INT(_10523)) {
            _10524 = (_10523 == 0);
        }
        else {
            _10524 = unary_op(NOT, _10523);
        }
        DeRef(_10523);
        _10523 = NOVALUE;
        if (_10524 == 0) {
            DeRef(_10524);
            _10524 = NOVALUE;
            goto L5; // [77] 88
        }
        else {
            if (!IS_ATOM_INT(_10524) && DBL_PTR(_10524)->dbl == 0.0){
                DeRef(_10524);
                _10524 = NOVALUE;
                goto L5; // [77] 88
            }
            DeRef(_10524);
            _10524 = NOVALUE;
        }
        DeRef(_10524);
        _10524 = NOVALUE;

        /** 			bad_string_flag = 1*/
        _bad_string_flag_18714 = 1;
L5: 

        /** 	end for*/
        _i_18721 = _i_18721 + 1;
        goto L3; // [90] 49
L4: 
        ;
    }

    /** 	if bad_string_flag then*/
    if (_bad_string_flag_18714 == 0)
    {
        goto L6; // [97] 135
    }
    else{
    }

    /** 	    for i = 1 to length(vals) do*/
    if (IS_SEQUENCE(_vals_18711)){
            _10525 = SEQ_PTR(_vals_18711)->length;
    }
    else {
        _10525 = 1;
    }
    {
        int _i_18734;
        _i_18734 = 1;
L7: 
        if (_i_18734 > _10525){
            goto L8; // [105] 134
        }

        /** 	    	vals[i] = pretty_sprint(vals[i], {2} )*/
        _2 = (int)SEQ_PTR(_vals_18711);
        _10526 = (int)*(((s1_ptr)_2)->base + _i_18734);
        Ref(_10526);
        RefDS(_8147);
        _10527 = _2pretty_sprint(_10526, _8147);
        _10526 = NOVALUE;
        _2 = (int)SEQ_PTR(_vals_18711);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _vals_18711 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_18734);
        _1 = *(int *)_2;
        *(int *)_2 = _10527;
        if( _1 != _10527 ){
            DeRef(_1);
        }
        _10527 = NOVALUE;

        /** 	    end for*/
        _i_18734 = _i_18734 + 1;
        goto L7; // [129] 112
L8: 
        ;
    }
L6: 

    /** 	error_list = {*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10528 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_file_18708);
    Append(&_10529, _10528, _file_18708);
    _10528 = NOVALUE;
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10530 = (int)*(((s1_ptr)_2)->base + 2);
    _10531 = EPrintf(-9999999, _message_18710, _vals_18711);
    RefDS(_10531);
    Append(&_10532, _10530, _10531);
    _10530 = NOVALUE;
    DeRefDS(_10531);
    _10531 = NOVALUE;
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10533 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_e_18709);
    Append(&_10534, _10533, _e_18709);
    _10533 = NOVALUE;
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10535 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_lines_18713);
    Append(&_10536, _10535, _lines_18713);
    _10535 = NOVALUE;
    _0 = _1error_list_18672;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _10529;
    *((int *)(_2+8)) = _10532;
    *((int *)(_2+12)) = _10534;
    *((int *)(_2+16)) = _10536;
    _1error_list_18672 = MAKE_SEQ(_1);
    DeRefDS(_0);
    _10536 = NOVALUE;
    _10534 = NOVALUE;
    _10532 = NOVALUE;
    _10529 = NOVALUE;

    /** end procedure*/
    DeRefDS(_file_18708);
    DeRef(_e_18709);
    DeRefDS(_message_18710);
    DeRefDS(_vals_18711);
    DeRef(_error_file_18712);
    DeRef(_lines_18713);
    DeRef(_10520);
    _10520 = NOVALUE;
    return;
    ;
}


void _1verbose_printf(int _fh_18751, int _fmt_18752, int _data_18753)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_18751)) {
        _1 = (long)(DBL_PTR(_fh_18751)->dbl);
        if (UNIQUE(DBL_PTR(_fh_18751)) && (DBL_PTR(_fh_18751)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_18751);
        _fh_18751 = _1;
    }

    /** 	if verbose_switch then*/
    if (_1verbose_switch_18669 == 0)
    {
        goto L1; // [13] 23
    }
    else{
    }

    /** 		printf(fh, fmt, data)*/
    EPrintf(_fh_18751, _fmt_18752, _data_18753);
L1: 

    /** end procedure*/
    DeRefDS(_fmt_18752);
    DeRefDS(_data_18753);
    return;
    ;
}


int _1invoke(int _cmd_18757, int _filename_18758, int _err_18759)
{
    int _status_18760 = NOVALUE;
    int _11352 = NOVALUE;
    int _11351 = NOVALUE;
    int _11350 = NOVALUE;
    int _11349 = NOVALUE;
    int _10553 = NOVALUE;
    int _10550 = NOVALUE;
    int _10548 = NOVALUE;
    int _10546 = NOVALUE;
    int _10544 = NOVALUE;
    int _10541 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_err_18759)) {
        _1 = (long)(DBL_PTR(_err_18759)->dbl);
        if (UNIQUE(DBL_PTR(_err_18759)) && (DBL_PTR(_err_18759)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_err_18759);
        _err_18759 = _1;
    }

    /** 	delete_file("cw.err")*/
    RefDS(_10538);
    _11352 = _8delete_file(_10538);
    DeRef(_11352);
    _11352 = NOVALUE;

    /** 	delete_file("ex.err")*/
    RefDS(_10539);
    _11351 = _8delete_file(_10539);
    DeRef(_11351);
    _11351 = NOVALUE;

    /** 	status = system_exec(cmd, 2)*/
    _status_18760 = system_exec_call(_cmd_18757, 2);

    /** 	if find(status, USER_BREAK_EXIT_CODES) > 0 then*/
    _10541 = find_from(_status_18760, _1USER_BREAK_EXIT_CODES_18665, 1);
    if (_10541 <= 0)
    goto L1; // [42] 51

    /** 		abort(status)*/
    UserCleanup(_status_18760);
L1: 

    /** 	sleep(0.1)*/
    RefDS(_10543);
    _24sleep(_10543);

    /** 	if file_exists("cw.err") then*/
    RefDS(_10538);
    _10544 = _8file_exists(_10538);
    if (_10544 == 0) {
        DeRef(_10544);
        _10544 = NOVALUE;
        goto L2; // [62] 104
    }
    else {
        if (!IS_ATOM_INT(_10544) && DBL_PTR(_10544)->dbl == 0.0){
            DeRef(_10544);
            _10544 = NOVALUE;
            goto L2; // [62] 104
        }
        DeRef(_10544);
        _10544 = NOVALUE;
    }
    DeRef(_10544);
    _10544 = NOVALUE;

    /** 		error(filename, err, "Causeway error with status %d", {status}, "cw.err")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _status_18760;
    _10546 = MAKE_SEQ(_1);
    RefDS(_filename_18758);
    RefDS(_10545);
    RefDS(_10538);
    _1error(_filename_18758, _err_18759, _10545, _10546, _10538);
    _10546 = NOVALUE;

    /** 		if not status then*/
    if (_status_18760 != 0)
    goto L3; // [80] 91

    /** 			status = 1*/
    _status_18760 = 1;
L3: 

    /** 		ifdef not REC then*/

    /** 			delete_file("cw.err")*/
    RefDS(_10538);
    _11350 = _8delete_file(_10538);
    DeRef(_11350);
    _11350 = NOVALUE;
    goto L4; // [101] 172
L2: 

    /** 	elsif file_exists("ex.err") then*/
    RefDS(_10539);
    _10548 = _8file_exists(_10539);
    if (_10548 == 0) {
        DeRef(_10548);
        _10548 = NOVALUE;
        goto L5; // [110] 152
    }
    else {
        if (!IS_ATOM_INT(_10548) && DBL_PTR(_10548)->dbl == 0.0){
            DeRef(_10548);
            _10548 = NOVALUE;
            goto L5; // [110] 152
        }
        DeRef(_10548);
        _10548 = NOVALUE;
    }
    DeRef(_10548);
    _10548 = NOVALUE;

    /** 		error(filename, err, "EUPHORIA error with status %d", {status}, "ex.err")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _status_18760;
    _10550 = MAKE_SEQ(_1);
    RefDS(_filename_18758);
    RefDS(_10549);
    RefDS(_10539);
    _1error(_filename_18758, _err_18759, _10549, _10550, _10539);
    _10550 = NOVALUE;

    /** 		if not status then*/
    if (_status_18760 != 0)
    goto L6; // [128] 139

    /** 			status = 1*/
    _status_18760 = 1;
L6: 

    /** 		ifdef not REC then*/

    /** 			delete_file("ex.err")*/
    RefDS(_10539);
    _11349 = _8delete_file(_10539);
    DeRef(_11349);
    _11349 = NOVALUE;
    goto L4; // [149] 172
L5: 

    /** 	elsif status then*/
    if (_status_18760 == 0)
    {
        goto L7; // [154] 171
    }
    else{
    }

    /** 		error(filename, err, "program died with status %d", {status})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _status_18760;
    _10553 = MAKE_SEQ(_1);
    RefDS(_filename_18758);
    RefDS(_10552);
    _1error(_filename_18758, _err_18759, _10552, _10553, 0);
    _10553 = NOVALUE;
L7: 
L4: 

    /** 	return status*/
    DeRefDS(_cmd_18757);
    DeRefDS(_filename_18758);
    return _status_18760;
    ;
}


int _1check_log(int _log_where_18792)
{
    int _log_fd_18793 = NOVALUE;
    int _pos_18797 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_log_where_18792)) {
        _1 = (long)(DBL_PTR(_log_where_18792)->dbl);
        if (UNIQUE(DBL_PTR(_log_where_18792)) && (DBL_PTR(_log_where_18792)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_log_where_18792);
        _log_where_18792 = _1;
    }

    /** 	integer log_fd = open("unittest.log", "a")*/
    _log_fd_18793 = EOpen(_10554, _10555, 0);

    /** 	if log_fd = -1  then*/
    if (_log_fd_18793 != -1)
    goto L1; // [16] 29

    /** 		return "couldn't generate unittest.log"*/
    RefDS(_10558);
    DeRef(_pos_18797);
    return _10558;
    goto L2; // [26] 53
L1: 

    /** 		pos = where(log_fd)*/
    _0 = _pos_18797;
    _pos_18797 = _15where(_log_fd_18793);
    DeRef(_0);

    /** 		if log_where = pos then*/
    if (binary_op_a(NOTEQ, _log_where_18792, _pos_18797)){
        goto L3; // [37] 52
    }

    /** 			close(log_fd)*/
    EClose(_log_fd_18793);

    /** 			return  "couldn't add to unittest.log"*/
    RefDS(_10561);
    DeRef(_pos_18797);
    return _10561;
L3: 
L2: 

    /** 	log_where = where(log_fd)*/
    _log_where_18792 = _15where(_log_fd_18793);
    if (!IS_ATOM_INT(_log_where_18792)) {
        _1 = (long)(DBL_PTR(_log_where_18792)->dbl);
        if (UNIQUE(DBL_PTR(_log_where_18792)) && (DBL_PTR(_log_where_18792)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_log_where_18792);
        _log_where_18792 = _1;
    }

    /** 	close(log_fd)*/
    EClose(_log_fd_18793);

    /** 	return log_where*/
    DeRef(_pos_18797);
    return _log_where_18792;
    ;
}


void _1report_last_error(int _filename_18811)
{
    int _data_inlined_verbose_printf_at_72_18829 = NOVALUE;
    int _10582 = NOVALUE;
    int _10581 = NOVALUE;
    int _10580 = NOVALUE;
    int _10579 = NOVALUE;
    int _10578 = NOVALUE;
    int _10576 = NOVALUE;
    int _10575 = NOVALUE;
    int _10574 = NOVALUE;
    int _10573 = NOVALUE;
    int _10572 = NOVALUE;
    int _10569 = NOVALUE;
    int _10568 = NOVALUE;
    int _10567 = NOVALUE;
    int _10566 = NOVALUE;
    int _10565 = NOVALUE;
    int _10564 = NOVALUE;
    int _10563 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(error_list) and length(error_list[3])  then*/
    if (IS_SEQUENCE(_1error_list_18672)){
            _10563 = SEQ_PTR(_1error_list_18672)->length;
    }
    else {
        _10563 = 1;
    }
    if (_10563 == 0) {
        goto L1; // [10] 133
    }
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10565 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_10565)){
            _10566 = SEQ_PTR(_10565)->length;
    }
    else {
        _10566 = 1;
    }
    _10565 = NOVALUE;
    if (_10566 == 0)
    {
        _10566 = NOVALUE;
        goto L1; // [24] 133
    }
    else{
        _10566 = NOVALUE;
    }

    /** 		if error_list[3][$] = E_NOERROR then*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10567 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_10567)){
            _10568 = SEQ_PTR(_10567)->length;
    }
    else {
        _10568 = 1;
    }
    _2 = (int)SEQ_PTR(_10567);
    _10569 = (int)*(((s1_ptr)_2)->base + _10568);
    _10567 = NOVALUE;
    if (binary_op_a(NOTEQ, _10569, 1)){
        _10569 = NOVALUE;
        goto L2; // [44] 102
    }
    _10569 = NOVALUE;

    /** 			verbose_printf(1, "SUCCESS: %s %s\n", {filename, error_list[2][length(error_list[1])]})*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10572 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10573 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_10573)){
            _10574 = SEQ_PTR(_10573)->length;
    }
    else {
        _10574 = 1;
    }
    _10573 = NOVALUE;
    _2 = (int)SEQ_PTR(_10572);
    _10575 = (int)*(((s1_ptr)_2)->base + _10574);
    _10572 = NOVALUE;
    Ref(_10575);
    RefDS(_filename_18811);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _filename_18811;
    ((int *)_2)[2] = _10575;
    _10576 = MAKE_SEQ(_1);
    _10575 = NOVALUE;
    DeRef(_data_inlined_verbose_printf_at_72_18829);
    _data_inlined_verbose_printf_at_72_18829 = _10576;
    _10576 = NOVALUE;

    /** 	if verbose_switch then*/
    if (_1verbose_switch_18669 == 0)
    {
        goto L3; // [82] 97
    }
    else{
    }

    /** 		printf(fh, fmt, data)*/
    EPrintf(1, _10571, _data_inlined_verbose_printf_at_72_18829);

    /** end procedure*/
    goto L3; // [94] 97
L3: 
    DeRef(_data_inlined_verbose_printf_at_72_18829);
    _data_inlined_verbose_printf_at_72_18829 = NOVALUE;
    goto L4; // [99] 132
L2: 

    /** 			printf(1, "FAILURE: %s %s\n", {filename, error_list[2][length(error_list[1])]})*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10578 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _10579 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_10579)){
            _10580 = SEQ_PTR(_10579)->length;
    }
    else {
        _10580 = 1;
    }
    _10579 = NOVALUE;
    _2 = (int)SEQ_PTR(_10578);
    _10581 = (int)*(((s1_ptr)_2)->base + _10580);
    _10578 = NOVALUE;
    Ref(_10581);
    RefDS(_filename_18811);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _filename_18811;
    ((int *)_2)[2] = _10581;
    _10582 = MAKE_SEQ(_1);
    _10581 = NOVALUE;
    EPrintf(1, _10577, _10582);
    DeRefDS(_10582);
    _10582 = NOVALUE;
L4: 
L1: 

    /** end procedure*/
    DeRefDS(_filename_18811);
    _10565 = NOVALUE;
    _10573 = NOVALUE;
    _10579 = NOVALUE;
    return;
    ;
}


int _1prepare_error_file(int _file_name_18839)
{
    int _pos_18840 = NOVALUE;
    int _file_data_18841 = NOVALUE;
    int _path_18879 = NOVALUE;
    int _11348 = NOVALUE;
    int _11347 = NOVALUE;
    int _10620 = NOVALUE;
    int _10619 = NOVALUE;
    int _10618 = NOVALUE;
    int _10617 = NOVALUE;
    int _10616 = NOVALUE;
    int _10615 = NOVALUE;
    int _10613 = NOVALUE;
    int _10610 = NOVALUE;
    int _10608 = NOVALUE;
    int _10607 = NOVALUE;
    int _10605 = NOVALUE;
    int _10603 = NOVALUE;
    int _10602 = NOVALUE;
    int _10600 = NOVALUE;
    int _10598 = NOVALUE;
    int _10597 = NOVALUE;
    int _10595 = NOVALUE;
    int _10594 = NOVALUE;
    int _10592 = NOVALUE;
    int _10591 = NOVALUE;
    int _10589 = NOVALUE;
    int _10588 = NOVALUE;
    int _10585 = NOVALUE;
    int _10584 = NOVALUE;
    int _0, _1, _2;
    

    /** 	file_data = read_lines(file_name)*/
    Ref(_file_name_18839);
    _0 = _file_data_18841;
    _file_data_18841 = _15read_lines(_file_name_18839);
    DeRef(_0);

    /** 	if atom(file_data) then*/
    _10584 = IS_ATOM(_file_data_18841);
    if (_10584 == 0)
    {
        _10584 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _10584 = NOVALUE;
    }

    /** 		return file_data*/
    DeRef(_file_name_18839);
    DeRef(_path_18879);
    return _file_data_18841;
L1: 

    /** 	if length(file_data) > 4 then*/
    if (IS_SEQUENCE(_file_data_18841)){
            _10585 = SEQ_PTR(_file_data_18841)->length;
    }
    else {
        _10585 = 1;
    }
    if (_10585 <= 4)
    goto L2; // [27] 39

    /** 		file_data = file_data[1..4]*/
    rhs_slice_target = (object_ptr)&_file_data_18841;
    RHS_Slice(_file_data_18841, 1, 4);
L2: 

    /** 	for i = 1 to length(file_data) do*/
    if (IS_SEQUENCE(_file_data_18841)){
            _10588 = SEQ_PTR(_file_data_18841)->length;
    }
    else {
        _10588 = 1;
    }
    {
        int _i_18851;
        _i_18851 = 1;
L3: 
        if (_i_18851 > _10588){
            goto L4; // [44] 183
        }

        /** 		pos = find('=', file_data[i])*/
        _2 = (int)SEQ_PTR(_file_data_18841);
        _10589 = (int)*(((s1_ptr)_2)->base + _i_18851);
        _pos_18840 = find_from(61, _10589, 1);
        _10589 = NOVALUE;

        /** 		if pos then*/
        if (_pos_18840 == 0)
        {
            goto L5; // [66] 118
        }
        else{
        }

        /** 			if length(file_data[i]) >= 6 then*/
        _2 = (int)SEQ_PTR(_file_data_18841);
        _10591 = (int)*(((s1_ptr)_2)->base + _i_18851);
        if (IS_SEQUENCE(_10591)){
                _10592 = SEQ_PTR(_10591)->length;
        }
        else {
            _10592 = 1;
        }
        _10591 = NOVALUE;
        if (_10592 < 6)
        goto L6; // [78] 117

        /** 				if equal(file_data[i][1..4], "    ") then*/
        _2 = (int)SEQ_PTR(_file_data_18841);
        _10594 = (int)*(((s1_ptr)_2)->base + _i_18851);
        rhs_slice_target = (object_ptr)&_10595;
        RHS_Slice(_10594, 1, 4);
        _10594 = NOVALUE;
        if (_10595 == _10596)
        _10597 = 1;
        else if (IS_ATOM_INT(_10595) && IS_ATOM_INT(_10596))
        _10597 = 0;
        else
        _10597 = (compare(_10595, _10596) == 0);
        DeRefDS(_10595);
        _10595 = NOVALUE;
        if (_10597 == 0)
        {
            _10597 = NOVALUE;
            goto L7; // [97] 116
        }
        else{
            _10597 = NOVALUE;
        }

        /** 					file_data = file_data[1 .. i-1]*/
        _10598 = _i_18851 - 1;
        rhs_slice_target = (object_ptr)&_file_data_18841;
        RHS_Slice(_file_data_18841, 1, _10598);

        /** 					exit*/
        goto L4; // [113] 183
L7: 
L6: 
L5: 

        /** 		if equal(file_data[i], "Global & Local Variables") then*/
        _2 = (int)SEQ_PTR(_file_data_18841);
        _10600 = (int)*(((s1_ptr)_2)->base + _i_18851);
        if (_10600 == _10601)
        _10602 = 1;
        else if (IS_ATOM_INT(_10600) && IS_ATOM_INT(_10601))
        _10602 = 0;
        else
        _10602 = (compare(_10600, _10601) == 0);
        _10600 = NOVALUE;
        if (_10602 == 0)
        {
            _10602 = NOVALUE;
            goto L8; // [128] 147
        }
        else{
            _10602 = NOVALUE;
        }

        /** 			file_data = file_data[1 .. i-1]*/
        _10603 = _i_18851 - 1;
        rhs_slice_target = (object_ptr)&_file_data_18841;
        RHS_Slice(_file_data_18841, 1, _10603);

        /** 			exit*/
        goto L4; // [144] 183
L8: 

        /** 		if equal(file_data[i], "--- Defined Words ---") then*/
        _2 = (int)SEQ_PTR(_file_data_18841);
        _10605 = (int)*(((s1_ptr)_2)->base + _i_18851);
        if (_10605 == _10606)
        _10607 = 1;
        else if (IS_ATOM_INT(_10605) && IS_ATOM_INT(_10606))
        _10607 = 0;
        else
        _10607 = (compare(_10605, _10606) == 0);
        _10605 = NOVALUE;
        if (_10607 == 0)
        {
            _10607 = NOVALUE;
            goto L9; // [157] 176
        }
        else{
            _10607 = NOVALUE;
        }

        /** 			file_data = file_data[1 .. i-1]*/
        _10608 = _i_18851 - 1;
        rhs_slice_target = (object_ptr)&_file_data_18841;
        RHS_Slice(_file_data_18841, 1, _10608);

        /** 			exit*/
        goto L4; // [173] 183
L9: 

        /** 	end for*/
        _i_18851 = _i_18851 + 1;
        goto L3; // [178] 51
L4: 
        ;
    }

    /**     sequence path*/

    /**     if length(file_data) >= 2 then*/
    if (IS_SEQUENCE(_file_data_18841)){
            _10610 = SEQ_PTR(_file_data_18841)->length;
    }
    else {
        _10610 = 1;
    }
    if (_10610 < 2)
    goto LA; // [190] 255

    /**     	path = "\\/" & file_data[1]*/
    _2 = (int)SEQ_PTR(_file_data_18841);
    _10613 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_10612) && IS_ATOM(_10613)) {
        Ref(_10613);
        Append(&_path_18879, _10612, _10613);
    }
    else if (IS_ATOM(_10612) && IS_SEQUENCE(_10613)) {
    }
    else {
        Concat((object_ptr)&_path_18879, _10612, _10613);
    }
    _10613 = NOVALUE;

    /**     	path = path[max(rfind('/', path) & rfind('\\', path))+1..$]*/
    if (IS_SEQUENCE(_path_18879)){
            _11348 = SEQ_PTR(_path_18879)->length;
    }
    else {
        _11348 = 1;
    }
    RefDS(_path_18879);
    _10615 = _6rfind(47, _path_18879, _11348);
    _11348 = NOVALUE;
    if (IS_SEQUENCE(_path_18879)){
            _11347 = SEQ_PTR(_path_18879)->length;
    }
    else {
        _11347 = 1;
    }
    RefDS(_path_18879);
    _10616 = _6rfind(92, _path_18879, _11347);
    _11347 = NOVALUE;
    if (IS_SEQUENCE(_10615) && IS_ATOM(_10616)) {
        Ref(_10616);
        Append(&_10617, _10615, _10616);
    }
    else if (IS_ATOM(_10615) && IS_SEQUENCE(_10616)) {
        Ref(_10615);
        Prepend(&_10617, _10616, _10615);
    }
    else {
        Concat((object_ptr)&_10617, _10615, _10616);
        DeRef(_10615);
        _10615 = NOVALUE;
    }
    DeRef(_10615);
    _10615 = NOVALUE;
    DeRef(_10616);
    _10616 = NOVALUE;
    _10618 = _18max(_10617);
    _10617 = NOVALUE;
    if (IS_ATOM_INT(_10618)) {
        _10619 = _10618 + 1;
        if (_10619 > MAXINT){
            _10619 = NewDouble((double)_10619);
        }
    }
    else
    _10619 = binary_op(PLUS, 1, _10618);
    DeRef(_10618);
    _10618 = NOVALUE;
    if (IS_SEQUENCE(_path_18879)){
            _10620 = SEQ_PTR(_path_18879)->length;
    }
    else {
        _10620 = 1;
    }
    rhs_slice_target = (object_ptr)&_path_18879;
    RHS_Slice(_path_18879, _10619, _10620);

    /**     	file_data[1] = path*/
    RefDS(_path_18879);
    _2 = (int)SEQ_PTR(_file_data_18841);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _file_data_18841 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _path_18879;
    DeRef(_1);
    goto LB; // [252] 261
LA: 

    /**     	file_data = 0*/
    DeRef(_file_data_18841);
    _file_data_18841 = 0;
LB: 

    /**     return file_data*/
    DeRef(_file_name_18839);
    DeRef(_path_18879);
    _10591 = NOVALUE;
    DeRef(_10598);
    _10598 = NOVALUE;
    DeRef(_10603);
    _10603 = NOVALUE;
    DeRef(_10608);
    _10608 = NOVALUE;
    DeRef(_10619);
    _10619 = NOVALUE;
    return _file_data_18841;
    ;
}


int _1check_errors(int _filename_18899, int _fail_list_18900)
{
    int _expected_err_18901 = NOVALUE;
    int _actual_err_18902 = NOVALUE;
    int _some_error_18903 = NOVALUE;
    int _10641 = NOVALUE;
    int _10640 = NOVALUE;
    int _10639 = NOVALUE;
    int _10637 = NOVALUE;
    int _10636 = NOVALUE;
    int _10635 = NOVALUE;
    int _10634 = NOVALUE;
    int _10633 = NOVALUE;
    int _10630 = NOVALUE;
    int _10629 = NOVALUE;
    int _10625 = NOVALUE;
    int _10624 = NOVALUE;
    int _10622 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer some_error = 0*/
    _some_error_18903 = 0;

    /** 	expected_err = prepare_error_file( find_error_file( filename ) )*/
    RefDS(_filename_18899);
    _10622 = _1find_error_file(_filename_18899);
    _0 = _expected_err_18901;
    _expected_err_18901 = _1prepare_error_file(_10622);
    DeRef(_0);
    _10622 = NOVALUE;

    /** 	if atom(expected_err) then*/
    _10624 = IS_ATOM(_expected_err_18901);
    if (_10624 == 0)
    {
        _10624 = NOVALUE;
        goto L1; // [27] 53
    }
    else{
        _10624 = NOVALUE;
    }

    /** 		error(filename, E_INTERPRET, No_valid_control_file_was_supplied, {})*/
    RefDS(_filename_18899);
    RefDS(_1No_valid_control_file_was_supplied_19500);
    RefDS(_5);
    _1error(_filename_18899, 2, _1No_valid_control_file_was_supplied_19500, _5, 0);

    /** 		some_error = 1*/
    _some_error_18903 = 1;
    goto L2; // [50] 84
L1: 

    /** 	elsif length(expected_err) = 0 then*/
    if (IS_SEQUENCE(_expected_err_18901)){
            _10625 = SEQ_PTR(_expected_err_18901)->length;
    }
    else {
        _10625 = 1;
    }
    if (_10625 != 0)
    goto L3; // [58] 83

    /** 		error(filename, E_INTERPRET, Unexpected_empty_control_file, {})*/
    RefDS(_filename_18899);
    RefDS(_1Unexpected_empty_control_file_19502);
    RefDS(_5);
    _1error(_filename_18899, 2, _1Unexpected_empty_control_file_19502, _5, 0);

    /** 		some_error = 1*/
    _some_error_18903 = 1;
L3: 
L2: 

    /** 	if not some_error then*/
    if (_some_error_18903 != 0)
    goto L4; // [86] 224

    /** 		actual_err = prepare_error_file("ex.err")*/
    RefDS(_10539);
    _0 = _actual_err_18902;
    _actual_err_18902 = _1prepare_error_file(_10539);
    DeRef(_0);

    /** 		if atom(actual_err) then*/
    _10629 = IS_ATOM(_actual_err_18902);
    if (_10629 == 0)
    {
        _10629 = NOVALUE;
        goto L5; // [100] 126
    }
    else{
        _10629 = NOVALUE;
    }

    /** 			error(filename, E_INTERPRET, No_valid_exerr_has_been_generated, {})*/
    RefDS(_filename_18899);
    RefDS(_1No_valid_exerr_has_been_generated_19504);
    RefDS(_5);
    _1error(_filename_18899, 2, _1No_valid_exerr_has_been_generated_19504, _5, 0);

    /** 			some_error = 1*/
    _some_error_18903 = 1;
    goto L6; // [123] 157
L5: 

    /** 		elsif length(actual_err) = 0 then*/
    if (IS_SEQUENCE(_actual_err_18902)){
            _10630 = SEQ_PTR(_actual_err_18902)->length;
    }
    else {
        _10630 = 1;
    }
    if (_10630 != 0)
    goto L7; // [131] 156

    /** 			error(filename, E_INTERPRET, Unexpected_empty_exerr, {})*/
    RefDS(_filename_18899);
    RefDS(_1Unexpected_empty_exerr_19506);
    RefDS(_5);
    _1error(_filename_18899, 2, _1Unexpected_empty_exerr_19506, _5, 0);

    /** 			some_error = 1*/
    _some_error_18903 = 1;
L7: 
L6: 

    /** 		if not some_error then*/
    if (_some_error_18903 != 0)
    goto L8; // [159] 223

    /** 			if not equal(actual_err[2..$], expected_err[2..$]) then*/
    if (IS_SEQUENCE(_actual_err_18902)){
            _10633 = SEQ_PTR(_actual_err_18902)->length;
    }
    else {
        _10633 = 1;
    }
    rhs_slice_target = (object_ptr)&_10634;
    RHS_Slice(_actual_err_18902, 2, _10633);
    if (IS_SEQUENCE(_expected_err_18901)){
            _10635 = SEQ_PTR(_expected_err_18901)->length;
    }
    else {
        _10635 = 1;
    }
    rhs_slice_target = (object_ptr)&_10636;
    RHS_Slice(_expected_err_18901, 2, _10635);
    if (_10634 == _10636)
    _10637 = 1;
    else if (IS_ATOM_INT(_10634) && IS_ATOM_INT(_10636))
    _10637 = 0;
    else
    _10637 = (compare(_10634, _10636) == 0);
    DeRefDS(_10634);
    _10634 = NOVALUE;
    DeRefDS(_10636);
    _10636 = NOVALUE;
    if (_10637 != 0)
    goto L9; // [184] 222
    _10637 = NOVALUE;

    /** 				error(filename, E_INTERPRET, differing_ex_err_format,*/
    Ref(_expected_err_18901);
    RefDS(_3957);
    _10639 = _3join(_expected_err_18901, _3957);
    Ref(_actual_err_18902);
    RefDS(_3957);
    _10640 = _3join(_actual_err_18902, _3957);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _10639;
    ((int *)_2)[2] = _10640;
    _10641 = MAKE_SEQ(_1);
    _10640 = NOVALUE;
    _10639 = NOVALUE;
    RefDS(_filename_18899);
    RefDS(_1differing_ex_err_format_19508);
    RefDS(_10539);
    _1error(_filename_18899, 2, _1differing_ex_err_format_19508, _10641, _10539);
    _10641 = NOVALUE;

    /** 				some_error = 1*/
    _some_error_18903 = 1;
L9: 
L8: 
L4: 

    /** 	if some_error then	*/
    if (_some_error_18903 == 0)
    {
        goto LA; // [226] 246
    }
    else{
    }

    /** 		failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 		fail_list = append(fail_list, filename)*/
    RefDS(_filename_18899);
    Append(&_fail_list_18900, _fail_list_18900, _filename_18899);
LA: 

    /** 	return fail_list*/
    DeRefDS(_filename_18899);
    DeRef(_expected_err_18901);
    DeRef(_actual_err_18902);
    return _fail_list_18900;
    ;
}


int _1find_error_file(int _filename_18944)
{
    int _control_error_file_18945 = NOVALUE;
    int _base_filename_18946 = NOVALUE;
    int _10657 = NOVALUE;
    int _10655 = NOVALUE;
    int _10653 = NOVALUE;
    int _10647 = NOVALUE;
    int _10646 = NOVALUE;
    int _10645 = NOVALUE;
    int _10644 = NOVALUE;
    int _0, _1, _2;
    

    /** 	base_filename = filename[1 .. find('.', filename & '.') - 1] & ".d" & SLASH*/
    Append(&_10644, _filename_18944, 46);
    _10645 = find_from(46, _10644, 1);
    DeRefDS(_10644);
    _10644 = NOVALUE;
    _10646 = _10645 - 1;
    _10645 = NOVALUE;
    rhs_slice_target = (object_ptr)&_10647;
    RHS_Slice(_filename_18944, 1, _10646);
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = _10648;
        concat_list[2] = _10647;
        Concat_N((object_ptr)&_base_filename_18946, concat_list, 3);
    }
    DeRefDS(_10647);
    _10647 = NOVALUE;

    /** 	control_error_file =  base_filename & "interpreter" & SLASH & interpreter_os_name & SLASH & "control.err"*/
    {
        int concat_list[6];

        concat_list[0] = _10651;
        concat_list[1] = 92;
        concat_list[2] = _1interpreter_os_name_19193;
        concat_list[3] = 92;
        concat_list[4] = _10650;
        concat_list[5] = _base_filename_18946;
        Concat_N((object_ptr)&_control_error_file_18945, concat_list, 6);
    }

    /** 	if file_exists(control_error_file) then*/
    RefDS(_control_error_file_18945);
    _10653 = _8file_exists(_control_error_file_18945);
    if (_10653 == 0) {
        DeRef(_10653);
        _10653 = NOVALUE;
        goto L1; // [54] 64
    }
    else {
        if (!IS_ATOM_INT(_10653) && DBL_PTR(_10653)->dbl == 0.0){
            DeRef(_10653);
            _10653 = NOVALUE;
            goto L1; // [54] 64
        }
        DeRef(_10653);
        _10653 = NOVALUE;
    }
    DeRef(_10653);
    _10653 = NOVALUE;

    /** 		return control_error_file*/
    DeRefDS(_filename_18944);
    DeRefDS(_base_filename_18946);
    _10646 = NOVALUE;
    return _control_error_file_18945;
L1: 

    /** 	control_error_file =  base_filename & interpreter_os_name & SLASH & "control.err"*/
    {
        int concat_list[4];

        concat_list[0] = _10651;
        concat_list[1] = 92;
        concat_list[2] = _1interpreter_os_name_19193;
        concat_list[3] = _base_filename_18946;
        Concat_N((object_ptr)&_control_error_file_18945, concat_list, 4);
    }

    /** 	if file_exists(control_error_file) then*/
    RefDS(_control_error_file_18945);
    _10655 = _8file_exists(_control_error_file_18945);
    if (_10655 == 0) {
        DeRef(_10655);
        _10655 = NOVALUE;
        goto L2; // [83] 93
    }
    else {
        if (!IS_ATOM_INT(_10655) && DBL_PTR(_10655)->dbl == 0.0){
            DeRef(_10655);
            _10655 = NOVALUE;
            goto L2; // [83] 93
        }
        DeRef(_10655);
        _10655 = NOVALUE;
    }
    DeRef(_10655);
    _10655 = NOVALUE;

    /** 		return control_error_file*/
    DeRefDS(_filename_18944);
    DeRefDS(_base_filename_18946);
    DeRef(_10646);
    _10646 = NOVALUE;
    return _control_error_file_18945;
L2: 

    /** 	control_error_file = base_filename & "control.err"*/
    Concat((object_ptr)&_control_error_file_18945, _base_filename_18946, _10651);

    /** 	if file_exists(control_error_file) then*/
    RefDS(_control_error_file_18945);
    _10657 = _8file_exists(_control_error_file_18945);
    if (_10657 == 0) {
        DeRef(_10657);
        _10657 = NOVALUE;
        goto L3; // [105] 115
    }
    else {
        if (!IS_ATOM_INT(_10657) && DBL_PTR(_10657)->dbl == 0.0){
            DeRef(_10657);
            _10657 = NOVALUE;
            goto L3; // [105] 115
        }
        DeRef(_10657);
        _10657 = NOVALUE;
    }
    DeRef(_10657);
    _10657 = NOVALUE;

    /** 		return control_error_file*/
    DeRefDS(_filename_18944);
    DeRefDS(_base_filename_18946);
    DeRef(_10646);
    _10646 = NOVALUE;
    return _control_error_file_18945;
L3: 

    /** 	return -1*/
    DeRefDS(_filename_18944);
    DeRef(_control_error_file_18945);
    DeRef(_base_filename_18946);
    DeRef(_10646);
    _10646 = NOVALUE;
    return -1;
    ;
}


int _1interpret_fail(int _cmd_18975, int _filename_18976, int _fail_list_18977)
{
    int _status_18978 = NOVALUE;
    int _old_length_18980 = NOVALUE;
    int _10670 = NOVALUE;
    int _10669 = NOVALUE;
    int _10661 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer status = system_exec(cmd, 2)*/
    _status_18978 = system_exec_call(_cmd_18975, 2);

    /** 	old_length = length( fail_list )*/
    if (IS_SEQUENCE(_fail_list_18977)){
            _old_length_18980 = SEQ_PTR(_fail_list_18977)->length;
    }
    else {
        _old_length_18980 = 1;
    }

    /** 	fail_list = check_errors( filename, fail_list )*/
    RefDS(_filename_18976);
    RefDS(_fail_list_18977);
    _0 = _fail_list_18977;
    _fail_list_18977 = _1check_errors(_filename_18976, _fail_list_18977);
    DeRefDS(_0);

    /** 	if old_length = length( fail_list ) then*/
    if (IS_SEQUENCE(_fail_list_18977)){
            _10661 = SEQ_PTR(_fail_list_18977)->length;
    }
    else {
        _10661 = 1;
    }
    if (_old_length_18980 != _10661)
    goto L1; // [36] 97

    /** 		if status = 0 then*/
    if (_status_18978 != 0)
    goto L2; // [42] 76

    /** 			failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 			fail_list = append(fail_list, filename)*/
    RefDS(_filename_18976);
    Append(&_fail_list_18977, _fail_list_18977, _filename_18976);

    /** 			error(filename, E_INTERPRET,*/
    RefDS(_filename_18976);
    RefDS(_10666);
    RefDS(_5);
    _1error(_filename_18976, 2, _10666, _5, 0);
    goto L3; // [73] 96
L2: 

    /** 			error(filename, E_NOERROR, "The unit test crashed in the expected manner. " &*/
    Concat((object_ptr)&_10669, _10667, _10668);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _status_18978;
    _10670 = MAKE_SEQ(_1);
    RefDS(_filename_18976);
    _1error(_filename_18976, 1, _10669, _10670, 0);
    _10669 = NOVALUE;
    _10670 = NOVALUE;
L3: 
L1: 

    /** 	return fail_list*/
    DeRefDS(_cmd_18975);
    DeRefDS(_filename_18976);
    return _fail_list_18977;
    ;
}


int _1translate(int _filename_18998, int _fail_list_18999)
{
    int _exename_19003 = NOVALUE;
    int _cmd_19008 = NOVALUE;
    int _data_inlined_verbose_printf_at_68_19019 = NOVALUE;
    int _status_19020 = NOVALUE;
    int _log_where_19024 = NOVALUE;
    int _data_inlined_verbose_printf_at_137_19032 = NOVALUE;
    int _token_19044 = NOVALUE;
    int _10708 = NOVALUE;
    int _10705 = NOVALUE;
    int _10699 = NOVALUE;
    int _10697 = NOVALUE;
    int _10694 = NOVALUE;
    int _10689 = NOVALUE;
    int _10687 = NOVALUE;
    int _10681 = NOVALUE;
    int _10678 = NOVALUE;
    int _10674 = NOVALUE;
    int _10672 = NOVALUE;
    int _0, _1, _2;
    

    /** 	printf(1, "\ntranslating %s:", {filename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_filename_18998);
    *((int *)(_2+4)) = _filename_18998;
    _10672 = MAKE_SEQ(_1);
    EPrintf(1, _10671, _10672);
    DeRefDS(_10672);
    _10672 = NOVALUE;

    /** 	total += 1*/
    _1total_18679 = _1total_18679 + 1;

    /** 	sequence exename = filebase(filename) & "-translated" & dexe*/
    RefDS(_filename_18998);
    _10674 = _8filebase(_filename_18998);
    {
        int concat_list[3];

        concat_list[0] = _1dexe_18680;
        concat_list[1] = _10675;
        concat_list[2] = _10674;
        Concat_N((object_ptr)&_exename_19003, concat_list, 3);
    }
    DeRef(_10674);
    _10674 = NOVALUE;

    /** 	sequence cmd = sprintf("%s %s %s %s -d UNITTEST -d EC -batch %s -o %s",*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_1translator_19190);
    *((int *)(_2+4)) = _1translator_19190;
    RefDS(_1library_19191);
    *((int *)(_2+8)) = _1library_19191;
    RefDS(_1compiler_19192);
    *((int *)(_2+12)) = _1compiler_19192;
    RefDS(_1translator_options_19188);
    *((int *)(_2+16)) = _1translator_options_19188;
    RefDS(_filename_18998);
    *((int *)(_2+20)) = _filename_18998;
    RefDS(_exename_19003);
    *((int *)(_2+24)) = _exename_19003;
    _10678 = MAKE_SEQ(_1);
    DeRefi(_cmd_19008);
    _cmd_19008 = EPrintf(-9999999, _10677, _10678);
    DeRefDS(_10678);
    _10678 = NOVALUE;

    /** 	verbose_printf(1, "CMD '%s'\n", {cmd})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_cmd_19008);
    *((int *)(_2+4)) = _cmd_19008;
    _10681 = MAKE_SEQ(_1);
    DeRef(_data_inlined_verbose_printf_at_68_19019);
    _data_inlined_verbose_printf_at_68_19019 = _10681;
    _10681 = NOVALUE;

    /** 	if verbose_switch then*/
    if (_1verbose_switch_18669 == 0)
    {
        goto L1; // [77] 92
    }
    else{
    }

    /** 		printf(fh, fmt, data)*/
    EPrintf(1, _10680, _data_inlined_verbose_printf_at_68_19019);

    /** end procedure*/
    goto L1; // [89] 92
L1: 
    DeRef(_data_inlined_verbose_printf_at_68_19019);
    _data_inlined_verbose_printf_at_68_19019 = NOVALUE;

    /** 	integer status = system_exec(cmd, 0)*/
    _status_19020 = system_exec_call(_cmd_19008, 0);

    /** 	filename = filebase(filename)*/
    RefDS(_filename_18998);
    _0 = _filename_18998;
    _filename_18998 = _8filebase(_filename_18998);
    DeRefDS(_0);

    /** 	integer log_where = 0*/
    _log_where_19024 = 0;

    /** 	if status = 0 then*/
    if (_status_19020 != 0)
    goto L2; // [119] 325

    /** 		void = delete_file("cw.err")*/
    RefDS(_10538);
    _0 = _8delete_file(_10538);
    DeRef(_1void_18670);
    _1void_18670 = _0;

    /** 		verbose_printf(1, "executing %s:\n", {exename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_exename_19003);
    *((int *)(_2+4)) = _exename_19003;
    _10687 = MAKE_SEQ(_1);
    DeRef(_data_inlined_verbose_printf_at_137_19032);
    _data_inlined_verbose_printf_at_137_19032 = _10687;
    _10687 = NOVALUE;

    /** 	if verbose_switch then*/
    if (_1verbose_switch_18669 == 0)
    {
        goto L3; // [144] 159
    }
    else{
    }

    /** 		printf(fh, fmt, data)*/
    EPrintf(1, _10686, _data_inlined_verbose_printf_at_137_19032);

    /** end procedure*/
    goto L3; // [156] 159
L3: 
    DeRef(_data_inlined_verbose_printf_at_137_19032);
    _data_inlined_verbose_printf_at_137_19032 = NOVALUE;

    /** 		cmd = sprintf("./%s %s", {exename, test_options})*/
    RefDS(_1test_options_19189);
    RefDS(_exename_19003);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _exename_19003;
    ((int *)_2)[2] = _1test_options_19189;
    _10689 = MAKE_SEQ(_1);
    DeRefi(_cmd_19008);
    _cmd_19008 = EPrintf(-9999999, _10688, _10689);
    DeRefDS(_10689);
    _10689 = NOVALUE;

    /** 		status = invoke(cmd, exename,  E_EXECUTE) */
    RefDS(_cmd_19008);
    RefDS(_exename_19003);
    _status_19020 = _1invoke(_cmd_19008, _exename_19003, 5);
    if (!IS_ATOM_INT(_status_19020)) {
        _1 = (long)(DBL_PTR(_status_19020)->dbl);
        if (UNIQUE(DBL_PTR(_status_19020)) && (DBL_PTR(_status_19020)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_status_19020);
        _status_19020 = _1;
    }

    /** 		if status then*/
    if (_status_19020 == 0)
    {
        goto L4; // [189] 217
    }
    else{
    }

    /** 			failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 			fail_list = append(fail_list, "translated" & " " & exename)*/
    {
        int concat_list[3];

        concat_list[0] = _exename_19003;
        concat_list[1] = _1403;
        concat_list[2] = _10693;
        Concat_N((object_ptr)&_10694, concat_list, 3);
    }
    RefDS(_10694);
    Append(&_fail_list_18999, _fail_list_18999, _10694);
    DeRefDS(_10694);
    _10694 = NOVALUE;
    goto L5; // [214] 316
L4: 

    /** 			object token*/

    /** 			if logging_activated then*/
    if (_1logging_activated_18677 == 0)
    {
        goto L6; // [223] 235
    }
    else{
    }

    /** 				token = check_log(log_where)*/
    _0 = _token_19044;
    _token_19044 = _1check_log(_log_where_19024);
    DeRef(_0);
    goto L7; // [232] 241
L6: 

    /** 				token = 0*/
    DeRef(_token_19044);
    _token_19044 = 0;
L7: 

    /** 			if sequence(token) then*/
    _10697 = IS_SEQUENCE(_token_19044);
    if (_10697 == 0)
    {
        _10697 = NOVALUE;
        goto L8; // [248] 287
    }
    else{
        _10697 = NOVALUE;
    }

    /** 				failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 				fail_list = append(fail_list, "translated" & " " & exename)				*/
    {
        int concat_list[3];

        concat_list[0] = _exename_19003;
        concat_list[1] = _1403;
        concat_list[2] = _10693;
        Concat_N((object_ptr)&_10699, concat_list, 3);
    }
    RefDS(_10699);
    Append(&_fail_list_18999, _fail_list_18999, _10699);
    DeRefDS(_10699);
    _10699 = NOVALUE;

    /** 				error(exename, E_EXECUTE, token, {}, "ex.err")*/
    RefDS(_exename_19003);
    Ref(_token_19044);
    RefDS(_5);
    RefDS(_10539);
    _1error(_exename_19003, 5, _token_19044, _5, _10539);
    goto L9; // [284] 308
L8: 

    /** 				log_where = token*/
    Ref(_token_19044);
    _log_where_19024 = _token_19044;
    if (!IS_ATOM_INT(_log_where_19024)) {
        _1 = (long)(DBL_PTR(_log_where_19024)->dbl);
        if (UNIQUE(DBL_PTR(_log_where_19024)) && (DBL_PTR(_log_where_19024)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_log_where_19024);
        _log_where_19024 = _1;
    }

    /** 				error(exename, E_NOERROR, "all tests successful", {})*/
    RefDS(_exename_19003);
    RefDS(_10701);
    RefDS(_5);
    _1error(_exename_19003, 1, _10701, _5, 0);
L9: 

    /** 			puts(1, "\n")*/
    EPuts(1, _3957); // DJP 
    DeRef(_token_19044);
    _token_19044 = NOVALUE;
L5: 

    /** 		void = delete_file(exename)*/
    RefDS(_exename_19003);
    _0 = _8delete_file(_exename_19003);
    DeRef(_1void_18670);
    _1void_18670 = _0;
    goto LA; // [322] 361
L2: 

    /** 		failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 		fail_list = append(fail_list, "translating " & filename)*/
    Concat((object_ptr)&_10705, _10704, _filename_18998);
    RefDS(_10705);
    Append(&_fail_list_18999, _fail_list_18999, _10705);
    DeRefDS(_10705);
    _10705 = NOVALUE;

    /** 		error(filename, E_TRANSLATE, "program translation terminated with a bad status %d", {status})                               */
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _status_19020;
    _10708 = MAKE_SEQ(_1);
    RefDS(_filename_18998);
    RefDS(_10707);
    _1error(_filename_18998, 3, _10707, _10708, 0);
    _10708 = NOVALUE;
LA: 

    /** 	report_last_error(filename)*/
    RefDS(_filename_18998);
    _1report_last_error(_filename_18998);

    /** 	return fail_list*/
    DeRefDS(_filename_18998);
    DeRef(_exename_19003);
    DeRefi(_cmd_19008);
    return _fail_list_18999;
    ;
}


int _1bind(int _filename_19066, int _fail_list_19067)
{
    int _exename_19070 = NOVALUE;
    int _filebase_inlined_filebase_at_16_19073 = NOVALUE;
    int _data_inlined_filebase_at_16_19072 = NOVALUE;
    int _cmd_19076 = NOVALUE;
    int _data_inlined_verbose_printf_at_78_19085 = NOVALUE;
    int _status_19086 = NOVALUE;
    int _log_where_19090 = NOVALUE;
    int _data_inlined_verbose_printf_at_140_19095 = NOVALUE;
    int _token_19106 = NOVALUE;
    int _10740 = NOVALUE;
    int _10737 = NOVALUE;
    int _10732 = NOVALUE;
    int _10730 = NOVALUE;
    int _10727 = NOVALUE;
    int _10722 = NOVALUE;
    int _10721 = NOVALUE;
    int _10717 = NOVALUE;
    int _10714 = NOVALUE;
    int _10710 = NOVALUE;
    int _0, _1, _2;
    

    /** 	printf(1, "\nbinding %s:\n", {filename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_filename_19066);
    *((int *)(_2+4)) = _filename_19066;
    _10710 = MAKE_SEQ(_1);
    EPrintf(1, _10709, _10710);
    DeRefDS(_10710);
    _10710 = NOVALUE;

    /** 	sequence exename = fs:filebase(filename) & "-bound" & dexe*/

    /** 	data = pathinfo(path)*/
    RefDS(_filename_19066);
    _0 = _data_inlined_filebase_at_16_19072;
    _data_inlined_filebase_at_16_19072 = _8pathinfo(_filename_19066, 0);
    DeRef(_0);

    /** 	return data[3]*/
    DeRef(_filebase_inlined_filebase_at_16_19073);
    _2 = (int)SEQ_PTR(_data_inlined_filebase_at_16_19072);
    _filebase_inlined_filebase_at_16_19073 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_filebase_inlined_filebase_at_16_19073);
    DeRef(_data_inlined_filebase_at_16_19072);
    _data_inlined_filebase_at_16_19072 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _1dexe_18680;
        concat_list[1] = _10711;
        concat_list[2] = _filebase_inlined_filebase_at_16_19073;
        Concat_N((object_ptr)&_exename_19070, concat_list, 3);
    }

    /** 	sequence cmd = sprintf("\"%s\" %s %s -batch -d UNITTEST %s -out %s",*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_1binder_19194);
    *((int *)(_2+4)) = _1binder_19194;
    RefDS(_1eub_path_18674);
    *((int *)(_2+8)) = _1eub_path_18674;
    RefDS(_1interpreter_options_19187);
    *((int *)(_2+12)) = _1interpreter_options_19187;
    RefDS(_filename_19066);
    *((int *)(_2+16)) = _filename_19066;
    RefDS(_exename_19070);
    *((int *)(_2+20)) = _exename_19070;
    _10714 = MAKE_SEQ(_1);
    DeRefi(_cmd_19076);
    _cmd_19076 = EPrintf(-9999999, _10713, _10714);
    DeRefDS(_10714);
    _10714 = NOVALUE;

    /** 	total += 1*/
    _1total_18679 = _1total_18679 + 1;

    /** 	verbose_printf(1, "CMD '%s'\n", {cmd})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_cmd_19076);
    *((int *)(_2+4)) = _cmd_19076;
    _10717 = MAKE_SEQ(_1);
    DeRef(_data_inlined_verbose_printf_at_78_19085);
    _data_inlined_verbose_printf_at_78_19085 = _10717;
    _10717 = NOVALUE;

    /** 	if verbose_switch then*/
    if (_1verbose_switch_18669 == 0)
    {
        goto L1; // [88] 103
    }
    else{
    }

    /** 		printf(fh, fmt, data)*/
    EPrintf(1, _10680, _data_inlined_verbose_printf_at_78_19085);

    /** end procedure*/
    goto L1; // [100] 103
L1: 
    DeRef(_data_inlined_verbose_printf_at_78_19085);
    _data_inlined_verbose_printf_at_78_19085 = NOVALUE;

    /** 	integer status = system_exec(cmd, 0)*/
    _status_19086 = system_exec_call(_cmd_19076, 0);

    /** 	filename = filebase(filename)*/
    RefDS(_filename_19066);
    _0 = _filename_19066;
    _filename_19066 = _8filebase(_filename_19066);
    DeRefDS(_0);

    /** 	integer log_where = 0*/
    _log_where_19090 = 0;

    /** 	if status = 0 then*/
    if (_status_19086 != 0)
    goto L2; // [130] 325

    /** 		verbose_printf(1, "executing %s:\n", {exename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_exename_19070);
    *((int *)(_2+4)) = _exename_19070;
    _10721 = MAKE_SEQ(_1);
    DeRef(_data_inlined_verbose_printf_at_140_19095);
    _data_inlined_verbose_printf_at_140_19095 = _10721;
    _10721 = NOVALUE;

    /** 	if verbose_switch then*/
    if (_1verbose_switch_18669 == 0)
    {
        goto L3; // [149] 164
    }
    else{
    }

    /** 		printf(fh, fmt, data)*/
    EPrintf(1, _10686, _data_inlined_verbose_printf_at_140_19095);

    /** end procedure*/
    goto L3; // [161] 164
L3: 
    DeRef(_data_inlined_verbose_printf_at_140_19095);
    _data_inlined_verbose_printf_at_140_19095 = NOVALUE;

    /** 		cmd = sprintf("./%s %s", {exename, test_options})*/
    RefDS(_1test_options_19189);
    RefDS(_exename_19070);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _exename_19070;
    ((int *)_2)[2] = _1test_options_19189;
    _10722 = MAKE_SEQ(_1);
    DeRefi(_cmd_19076);
    _cmd_19076 = EPrintf(-9999999, _10688, _10722);
    DeRefDS(_10722);
    _10722 = NOVALUE;

    /** 		status = invoke(cmd, exename,  E_EXECUTE) */
    RefDS(_cmd_19076);
    RefDS(_exename_19070);
    _status_19086 = _1invoke(_cmd_19076, _exename_19070, 5);
    if (!IS_ATOM_INT(_status_19086)) {
        _1 = (long)(DBL_PTR(_status_19086)->dbl);
        if (UNIQUE(DBL_PTR(_status_19086)) && (DBL_PTR(_status_19086)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_status_19086);
        _status_19086 = _1;
    }

    /** 		if status then*/
    if (_status_19086 == 0)
    {
        goto L4; // [194] 222
    }
    else{
    }

    /** 			failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 			fail_list = append(fail_list, "bound" & " " & exename)*/
    {
        int concat_list[3];

        concat_list[0] = _exename_19070;
        concat_list[1] = _1403;
        concat_list[2] = _10726;
        Concat_N((object_ptr)&_10727, concat_list, 3);
    }
    RefDS(_10727);
    Append(&_fail_list_19067, _fail_list_19067, _10727);
    DeRefDS(_10727);
    _10727 = NOVALUE;
    goto L5; // [219] 316
L4: 

    /** 			object token*/

    /** 			if logging_activated then*/
    if (_1logging_activated_18677 == 0)
    {
        goto L6; // [228] 240
    }
    else{
    }

    /** 				token = check_log(log_where)*/
    _0 = _token_19106;
    _token_19106 = _1check_log(_log_where_19090);
    DeRef(_0);
    goto L7; // [237] 246
L6: 

    /** 				token = 0*/
    DeRef(_token_19106);
    _token_19106 = 0;
L7: 

    /** 			if sequence(token) then*/
    _10730 = IS_SEQUENCE(_token_19106);
    if (_10730 == 0)
    {
        _10730 = NOVALUE;
        goto L8; // [253] 292
    }
    else{
        _10730 = NOVALUE;
    }

    /** 				failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 				fail_list = append(fail_list, "bound" & " " & exename)					*/
    {
        int concat_list[3];

        concat_list[0] = _exename_19070;
        concat_list[1] = _1403;
        concat_list[2] = _10726;
        Concat_N((object_ptr)&_10732, concat_list, 3);
    }
    RefDS(_10732);
    Append(&_fail_list_19067, _fail_list_19067, _10732);
    DeRefDS(_10732);
    _10732 = NOVALUE;

    /** 				error(exename, E_BOUND, token, {}, "ex.err")*/
    RefDS(_exename_19070);
    Ref(_token_19106);
    RefDS(_5);
    RefDS(_10539);
    _1error(_exename_19070, 7, _token_19106, _5, _10539);
    goto L9; // [289] 313
L8: 

    /** 				log_where = token*/
    Ref(_token_19106);
    _log_where_19090 = _token_19106;
    if (!IS_ATOM_INT(_log_where_19090)) {
        _1 = (long)(DBL_PTR(_log_where_19090)->dbl);
        if (UNIQUE(DBL_PTR(_log_where_19090)) && (DBL_PTR(_log_where_19090)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_log_where_19090);
        _log_where_19090 = _1;
    }

    /** 				error(exename, E_NOERROR, "all tests successful", {})*/
    RefDS(_exename_19070);
    RefDS(_10701);
    RefDS(_5);
    _1error(_exename_19070, 1, _10701, _5, 0);
L9: 
    DeRef(_token_19106);
    _token_19106 = NOVALUE;
L5: 

    /** 		void = delete_file(exename)*/
    RefDS(_exename_19070);
    _0 = _8delete_file(_exename_19070);
    DeRef(_1void_18670);
    _1void_18670 = _0;
    goto LA; // [322] 361
L2: 

    /** 		failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 		fail_list = append(fail_list, "binding " & filename)*/
    Concat((object_ptr)&_10737, _10736, _filename_19066);
    RefDS(_10737);
    Append(&_fail_list_19067, _fail_list_19067, _10737);
    DeRefDS(_10737);
    _10737 = NOVALUE;

    /** 		error(filename, E_BIND, "program binding terminated with a bad status %d", {status})                               */
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _status_19086;
    _10740 = MAKE_SEQ(_1);
    RefDS(_filename_19066);
    RefDS(_10739);
    _1error(_filename_19066, 6, _10739, _10740, 0);
    _10740 = NOVALUE;
LA: 

    /** 	report_last_error(filename)*/
    RefDS(_filename_19066);
    _1report_last_error(_filename_19066);

    /** 	return fail_list*/
    DeRefDS(_filename_19066);
    DeRef(_exename_19070);
    DeRefi(_cmd_19076);
    return _fail_list_19067;
    ;
}


int _1test_file(int _filename_19127, int _fail_list_19128)
{
    int _log_where_19129 = NOVALUE;
    int _status_19130 = NOVALUE;
    int _crash_option_19135 = NOVALUE;
    int _cmd_19143 = NOVALUE;
    int _data_inlined_verbose_printf_at_90_19152 = NOVALUE;
    int _expected_status_19153 = NOVALUE;
    int _old_fail_list_19157 = NOVALUE;
    int _token_19166 = NOVALUE;
    int _10770 = NOVALUE;
    int _10769 = NOVALUE;
    int _10768 = NOVALUE;
    int _10766 = NOVALUE;
    int _10765 = NOVALUE;
    int _10764 = NOVALUE;
    int _10761 = NOVALUE;
    int _10753 = NOVALUE;
    int _10752 = NOVALUE;
    int _10750 = NOVALUE;
    int _10748 = NOVALUE;
    int _10744 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer log_where = 0 -- keep track of unittest.log*/
    _log_where_19129 = 0;

    /** 	void = delete_file("ex.err")*/
    RefDS(_10539);
    _0 = _8delete_file(_10539);
    DeRef(_1void_18670);
    _1void_18670 = _0;

    /** 	void = delete_file("cw.err")*/
    RefDS(_10538);
    _0 = _8delete_file(_10538);
    DeRef(_1void_18670);
    _1void_18670 = _0;

    /** 	sequence crash_option = ""*/
    RefDS(_5);
    DeRefi(_crash_option_19135);
    _crash_option_19135 = _5;

    /** 	if match("t_c_", filename) = 1 then*/
    _10744 = e_match_from(_10743, _filename_19127, 1);
    if (_10744 != 1)
    goto L1; // [38] 50

    /** 		crash_option = " -d CRASH "*/
    RefDS(_10746);
    DeRefDSi(_crash_option_19135);
    _crash_option_19135 = _10746;
L1: 

    /** 	printf(1, "\ninterpreting %s:\n", {filename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_filename_19127);
    *((int *)(_2+4)) = _filename_19127;
    _10748 = MAKE_SEQ(_1);
    EPrintf(1, _10747, _10748);
    DeRefDS(_10748);
    _10748 = NOVALUE;

    /** 	sequence cmd = sprintf("%s %s %s -d UNITTEST -batch %s%s %s",*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_1executable_18681);
    *((int *)(_2+4)) = _1executable_18681;
    RefDS(_1interpreter_options_19187);
    *((int *)(_2+8)) = _1interpreter_options_19187;
    RefDS(_1coverage_erase_19197);
    *((int *)(_2+12)) = _1coverage_erase_19197;
    RefDS(_crash_option_19135);
    *((int *)(_2+16)) = _crash_option_19135;
    RefDS(_filename_19127);
    *((int *)(_2+20)) = _filename_19127;
    RefDS(_1test_options_19189);
    *((int *)(_2+24)) = _1test_options_19189;
    _10750 = MAKE_SEQ(_1);
    DeRefi(_cmd_19143);
    _cmd_19143 = EPrintf(-9999999, _10749, _10750);
    DeRefDS(_10750);
    _10750 = NOVALUE;

    /** 	verbose_printf(1, "CMD '%s'\n", {cmd})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_cmd_19143);
    *((int *)(_2+4)) = _cmd_19143;
    _10752 = MAKE_SEQ(_1);
    DeRef(_data_inlined_verbose_printf_at_90_19152);
    _data_inlined_verbose_printf_at_90_19152 = _10752;
    _10752 = NOVALUE;

    /** 	if verbose_switch then*/
    if (_1verbose_switch_18669 == 0)
    {
        goto L2; // [98] 113
    }
    else{
    }

    /** 		printf(fh, fmt, data)*/
    EPrintf(1, _10680, _data_inlined_verbose_printf_at_90_19152);

    /** end procedure*/
    goto L2; // [110] 113
L2: 
    DeRef(_data_inlined_verbose_printf_at_90_19152);
    _data_inlined_verbose_printf_at_90_19152 = NOVALUE;

    /** 	integer expected_status*/

    /** 	if match("t_c_", filename) = 1 then*/
    _10753 = e_match_from(_10743, _filename_19127, 1);
    if (_10753 != 1)
    goto L3; // [124] 159

    /** 		expected_status = 1*/
    _expected_status_19153 = 1;

    /** 		sequence old_fail_list = fail_list*/
    RefDS(_fail_list_19128);
    DeRef(_old_fail_list_19157);
    _old_fail_list_19157 = _fail_list_19128;

    /** 		fail_list = interpret_fail( cmd, filename, fail_list )*/
    RefDS(_cmd_19143);
    RefDS(_filename_19127);
    RefDS(_fail_list_19128);
    _0 = _fail_list_19128;
    _fail_list_19128 = _1interpret_fail(_cmd_19143, _filename_19127, _fail_list_19128);
    DeRefDS(_0);

    /** 		ifdef REC then*/
    DeRefDS(_old_fail_list_19157);
    _old_fail_list_19157 = NOVALUE;
    goto L4; // [156] 293
L3: 

    /** 		expected_status = 0*/
    _expected_status_19153 = 0;

    /** 		status = invoke(cmd, filename, E_INTERPRET) -- error() called if status != 0*/
    RefDS(_cmd_19143);
    RefDS(_filename_19127);
    _status_19130 = _1invoke(_cmd_19143, _filename_19127, 2);
    if (!IS_ATOM_INT(_status_19130)) {
        _1 = (long)(DBL_PTR(_status_19130)->dbl);
        if (UNIQUE(DBL_PTR(_status_19130)) && (DBL_PTR(_status_19130)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_status_19130);
        _status_19130 = _1;
    }

    /** 		if status then*/
    if (_status_19130 == 0)
    {
        goto L5; // [182] 204
    }
    else{
    }

    /** 			failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 			fail_list = append(fail_list, filename)*/
    RefDS(_filename_19127);
    Append(&_fail_list_19128, _fail_list_19128, _filename_19127);
    goto L6; // [201] 292
L5: 

    /** 			object token*/

    /** 			if logging_activated then*/
    if (_1logging_activated_18677 == 0)
    {
        goto L7; // [210] 222
    }
    else{
    }

    /** 				token = check_log(log_where)*/
    _0 = _token_19166;
    _token_19166 = _1check_log(_log_where_19129);
    DeRef(_0);
    goto L8; // [219] 228
L7: 

    /** 				token = 0*/
    DeRef(_token_19166);
    _token_19166 = 0;
L8: 

    /** 			if sequence(token) then*/
    _10761 = IS_SEQUENCE(_token_19166);
    if (_10761 == 0)
    {
        _10761 = NOVALUE;
        goto L9; // [235] 268
    }
    else{
        _10761 = NOVALUE;
    }

    /** 				failed += 1*/
    _1failed_18678 = _1failed_18678 + 1;

    /** 				fail_list = append(fail_list, filename)					*/
    RefDS(_filename_19127);
    Append(&_fail_list_19128, _fail_list_19128, _filename_19127);

    /** 				error(filename, E_INTERPRET, token, {}, "ex.err")*/
    RefDS(_filename_19127);
    Ref(_token_19166);
    RefDS(_5);
    RefDS(_10539);
    _1error(_filename_19127, 2, _token_19166, _5, _10539);
    goto LA; // [265] 289
L9: 

    /** 				log_where = token*/
    Ref(_token_19166);
    _log_where_19129 = _token_19166;
    if (!IS_ATOM_INT(_log_where_19129)) {
        _1 = (long)(DBL_PTR(_log_where_19129)->dbl);
        if (UNIQUE(DBL_PTR(_log_where_19129)) && (DBL_PTR(_log_where_19129)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_log_where_19129);
        _log_where_19129 = _1;
    }

    /** 				error(filename, E_NOERROR, "all tests successful", {})*/
    RefDS(_filename_19127);
    RefDS(_10701);
    RefDS(_5);
    _1error(_filename_19127, 1, _10701, _5, 0);
LA: 
    DeRef(_token_19166);
    _token_19166 = NOVALUE;
L6: 
L4: 

    /** 	report_last_error(filename)*/
    RefDS(_filename_19127);
    _1report_last_error(_filename_19127);

    /** 	if length(binder) and expected_status = 0 then*/
    if (IS_SEQUENCE(_1binder_19194)){
            _10764 = SEQ_PTR(_1binder_19194)->length;
    }
    else {
        _10764 = 1;
    }
    if (_10764 == 0) {
        goto LB; // [305] 329
    }
    _10766 = (_expected_status_19153 == 0);
    if (_10766 == 0)
    {
        DeRef(_10766);
        _10766 = NOVALUE;
        goto LB; // [316] 329
    }
    else{
        DeRef(_10766);
        _10766 = NOVALUE;
    }

    /** 		fail_list = bind( filename, fail_list )*/
    RefDS(_filename_19127);
    RefDS(_fail_list_19128);
    _0 = _fail_list_19128;
    _fail_list_19128 = _1bind(_filename_19127, _fail_list_19128);
    DeRefDS(_0);
LB: 

    /** 	if length(translator) and expected_status = 0 then*/
    if (IS_SEQUENCE(_1translator_19190)){
            _10768 = SEQ_PTR(_1translator_19190)->length;
    }
    else {
        _10768 = 1;
    }
    if (_10768 == 0) {
        goto LC; // [336] 360
    }
    _10770 = (_expected_status_19153 == 0);
    if (_10770 == 0)
    {
        DeRef(_10770);
        _10770 = NOVALUE;
        goto LC; // [347] 360
    }
    else{
        DeRef(_10770);
        _10770 = NOVALUE;
    }

    /** 		fail_list = translate( filename, fail_list )*/
    RefDS(_filename_19127);
    RefDS(_fail_list_19128);
    _0 = _fail_list_19128;
    _fail_list_19128 = _1translate(_filename_19127, _fail_list_19128);
    DeRefDS(_0);
LC: 

    /** 	return fail_list*/
    DeRefDS(_filename_19127);
    DeRefi(_crash_option_19135);
    DeRefi(_cmd_19143);
    return _fail_list_19128;
    ;
}


void _1ensure_coverage()
{
    int _tables_19207 = NOVALUE;
    int _ix_19210 = NOVALUE;
    int _table_name_19214 = NOVALUE;
    int _table_name_19232 = NOVALUE;
    int _10797 = NOVALUE;
    int _10796 = NOVALUE;
    int _10795 = NOVALUE;
    int _10793 = NOVALUE;
    int _10792 = NOVALUE;
    int _10791 = NOVALUE;
    int _10789 = NOVALUE;
    int _10784 = NOVALUE;
    int _10783 = NOVALUE;
    int _10782 = NOVALUE;
    int _10781 = NOVALUE;
    int _10780 = NOVALUE;
    int _10777 = NOVALUE;
    int _10775 = NOVALUE;
    int _10772 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if DB_OK != db_open( coverage_db ) then*/
    RefDS(_1coverage_db_19195);
    _10772 = _33db_open(_1coverage_db_19195, 0);
    if (binary_op_a(EQUALS, 0, _10772)){
        DeRef(_10772);
        _10772 = NOVALUE;
        goto L1; // [14] 36
    }
    DeRef(_10772);
    _10772 = NOVALUE;

    /** 		printf( 2, "Error reading coverage database: %s\n", {coverage_db} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_1coverage_db_19195);
    *((int *)(_2+4)) = _1coverage_db_19195;
    _10775 = MAKE_SEQ(_1);
    EPrintf(2, _10774, _10775);
    DeRefDS(_10775);
    _10775 = NOVALUE;

    /** 		return*/
    DeRef(_tables_19207);
    return;
L1: 

    /** 	sequence tables = db_table_list()*/
    _0 = _tables_19207;
    _tables_19207 = _33db_table_list();
    DeRef(_0);

    /** 	integer ix = 1*/
    _ix_19210 = 1;

    /** 	while ix <= length(tables) do*/
L2: 
    if (IS_SEQUENCE(_tables_19207)){
            _10777 = SEQ_PTR(_tables_19207)->length;
    }
    else {
        _10777 = 1;
    }
    if (_ix_19210 > _10777)
    goto L3; // [58] 122

    /** 		sequence table_name = tables[ix]*/
    DeRef(_table_name_19214);
    _2 = (int)SEQ_PTR(_tables_19207);
    _table_name_19214 = (int)*(((s1_ptr)_2)->base + _ix_19210);
    Ref(_table_name_19214);

    /** 		if table_name[1] = 'l' */
    _2 = (int)SEQ_PTR(_table_name_19214);
    _10780 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_10780)) {
        _10781 = (_10780 == 108);
    }
    else {
        _10781 = binary_op(EQUALS, _10780, 108);
    }
    _10780 = NOVALUE;
    if (IS_ATOM_INT(_10781)) {
        if (_10781 == 0) {
            goto L4; // [80] 107
        }
    }
    else {
        if (DBL_PTR(_10781)->dbl == 0.0) {
            goto L4; // [80] 107
        }
    }
    RefDS(_table_name_19214);
    _10783 = _33db_table_size(_table_name_19214);
    if (IS_ATOM_INT(_10783)) {
        _10784 = (_10783 == 0);
    }
    else {
        _10784 = binary_op(EQUALS, _10783, 0);
    }
    DeRef(_10783);
    _10783 = NOVALUE;
    if (_10784 == 0) {
        DeRef(_10784);
        _10784 = NOVALUE;
        goto L4; // [93] 107
    }
    else {
        if (!IS_ATOM_INT(_10784) && DBL_PTR(_10784)->dbl == 0.0){
            DeRef(_10784);
            _10784 = NOVALUE;
            goto L4; // [93] 107
        }
        DeRef(_10784);
        _10784 = NOVALUE;
    }
    DeRef(_10784);
    _10784 = NOVALUE;

    /** 			ix += 1*/
    _ix_19210 = _ix_19210 + 1;
    goto L5; // [104] 115
L4: 

    /** 			tables = remove( tables, ix )*/
    {
        s1_ptr assign_space = SEQ_PTR(_tables_19207);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_ix_19210)) ? _ix_19210 : (long)(DBL_PTR(_ix_19210)->dbl);
        int stop = (IS_ATOM_INT(_ix_19210)) ? _ix_19210 : (long)(DBL_PTR(_ix_19210)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_tables_19207), start, &_tables_19207 );
            }
            else Tail(SEQ_PTR(_tables_19207), stop+1, &_tables_19207);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_tables_19207), start, &_tables_19207);
        }
        else {
            assign_slice_seq = &assign_space;
            _tables_19207 = Remove_elements(start, stop, (SEQ_PTR(_tables_19207)->ref == 1));
        }
    }
L5: 
    DeRef(_table_name_19214);
    _table_name_19214 = NOVALUE;

    /** 	end while*/
    goto L2; // [119] 55
L3: 

    /** 	db_close()*/
    _33db_close();

    /** 	interpreter_options &= " -test"*/
    Concat((object_ptr)&_1interpreter_options_19187, _1interpreter_options_19187, _10787);

    /** 	for tx = 1 to length( tables ) do*/
    if (IS_SEQUENCE(_tables_19207)){
            _10789 = SEQ_PTR(_tables_19207)->length;
    }
    else {
        _10789 = 1;
    }
    {
        int _tx_19230;
        _tx_19230 = 1;
L6: 
        if (_tx_19230 > _10789){
            goto L7; // [139] 196
        }

        /** 		sequence table_name = tables[tx]*/
        DeRef(_table_name_19232);
        _2 = (int)SEQ_PTR(_tables_19207);
        _table_name_19232 = (int)*(((s1_ptr)_2)->base + _tx_19230);
        Ref(_table_name_19232);

        /** 		if not is_excluded( table_name[2..$] ) then*/
        if (IS_SEQUENCE(_table_name_19232)){
                _10791 = SEQ_PTR(_table_name_19232)->length;
        }
        else {
            _10791 = 1;
        }
        rhs_slice_target = (object_ptr)&_10792;
        RHS_Slice(_table_name_19232, 2, _10791);
        _10793 = _1is_excluded(_10792);
        _10792 = NOVALUE;
        if (IS_ATOM_INT(_10793)) {
            if (_10793 != 0){
                DeRef(_10793);
                _10793 = NOVALUE;
                goto L8; // [168] 187
            }
        }
        else {
            if (DBL_PTR(_10793)->dbl != 0.0){
                DeRef(_10793);
                _10793 = NOVALUE;
                goto L8; // [168] 187
            }
        }
        DeRef(_10793);
        _10793 = NOVALUE;

        /** 			test_file( table_name[2..$], {} )*/
        if (IS_SEQUENCE(_table_name_19232)){
                _10795 = SEQ_PTR(_table_name_19232)->length;
        }
        else {
            _10795 = 1;
        }
        rhs_slice_target = (object_ptr)&_10796;
        RHS_Slice(_table_name_19232, 2, _10795);
        RefDS(_5);
        _10797 = _1test_file(_10796, _5);
        _10796 = NOVALUE;
L8: 
        DeRef(_table_name_19232);
        _table_name_19232 = NOVALUE;

        /** 	end for*/
        _tx_19230 = _tx_19230 + 1;
        goto L6; // [191] 146
L7: 
        ;
    }

    /** end procedure*/
    DeRef(_tables_19207);
    DeRef(_10781);
    _10781 = NOVALUE;
    DeRef(_10797);
    _10797 = NOVALUE;
    return;
    ;
}


int _1is_excluded(int _file_19245)
{
    int _10800 = NOVALUE;
    int _10799 = NOVALUE;
    int _10798 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( exclude_patterns ) do*/
    if (IS_SEQUENCE(_1exclude_patterns_18675)){
            _10798 = SEQ_PTR(_1exclude_patterns_18675)->length;
    }
    else {
        _10798 = 1;
    }
    {
        int _i_19247;
        _i_19247 = 1;
L1: 
        if (_i_19247 > _10798){
            goto L2; // [10] 49
        }

        /** 		if regex:has_match( exclude_patterns[i], file ) then*/
        _2 = (int)SEQ_PTR(_1exclude_patterns_18675);
        _10799 = (int)*(((s1_ptr)_2)->base + _i_19247);
        Ref(_10799);
        RefDS(_file_19245);
        _10800 = _34has_match(_10799, _file_19245, 1, 0);
        _10799 = NOVALUE;
        if (_10800 == 0) {
            DeRef(_10800);
            _10800 = NOVALUE;
            goto L3; // [32] 42
        }
        else {
            if (!IS_ATOM_INT(_10800) && DBL_PTR(_10800)->dbl == 0.0){
                DeRef(_10800);
                _10800 = NOVALUE;
                goto L3; // [32] 42
            }
            DeRef(_10800);
            _10800 = NOVALUE;
        }
        DeRef(_10800);
        _10800 = NOVALUE;

        /** 			return 1*/
        DeRefDS(_file_19245);
        return 1;
L3: 

        /** 	end for*/
        _i_19247 = _i_19247 + 1;
        goto L1; // [44] 17
L2: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_file_19245);
    return 0;
    ;
}


void _1process_coverage()
{
    int _10811 = NOVALUE;
    int _10808 = NOVALUE;
    int _10807 = NOVALUE;
    int _10806 = NOVALUE;
    int _10803 = NOVALUE;
    int _10801 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length( coverage_db ) then*/
    if (IS_SEQUENCE(_1coverage_db_19195)){
            _10801 = SEQ_PTR(_1coverage_db_19195)->length;
    }
    else {
        _10801 = 1;
    }
    if (_10801 != 0)
    goto L1; // [8] 17
    _10801 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	ensure_coverage()*/
    _1ensure_coverage();

    /** 	if not length( coverage_pp ) then*/
    if (IS_SEQUENCE(_1coverage_pp_19196)){
            _10803 = SEQ_PTR(_1coverage_pp_19196)->length;
    }
    else {
        _10803 = 1;
    }
    if (_10803 != 0)
    goto L2; // [28] 37
    _10803 = NOVALUE;

    /** 		return*/
    return;
L2: 

    /** 	if system_exec( sprintf(`%s "%s"`, { coverage_pp, coverage_db }), 2 ) then*/
    RefDS(_1coverage_db_19195);
    RefDS(_1coverage_pp_19196);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1coverage_pp_19196;
    ((int *)_2)[2] = _1coverage_db_19195;
    _10806 = MAKE_SEQ(_1);
    _10807 = EPrintf(-9999999, _10805, _10806);
    DeRefDS(_10806);
    _10806 = NOVALUE;
    _10808 = system_exec_call(_10807, 2);
    DeRefDS(_10807);
    _10807 = NOVALUE;
    if (_10808 == 0)
    {
        _10808 = NOVALUE;
        goto L3; // [55] 78
    }
    else{
        _10808 = NOVALUE;
    }

    /** 		puts( 2, "Error running coverage postprocessor\n" )*/
    EPuts(2, _10809); // DJP 

    /** 		printf(2,`CMD: %s "%s"`, { coverage_pp, coverage_db })*/
    RefDS(_1coverage_db_19195);
    RefDS(_1coverage_pp_19196);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1coverage_pp_19196;
    ((int *)_2)[2] = _1coverage_db_19195;
    _10811 = MAKE_SEQ(_1);
    EPrintf(2, _10810, _10811);
    DeRefDS(_10811);
    _10811 = NOVALUE;
L3: 

    /** end procedure*/
    return;
    ;
}


void _1do_test(int _files_19270)
{
    int _score_19271 = NOVALUE;
    int _first_counter_19272 = NOVALUE;
    int _log_fd_19275 = NOVALUE;
    int _respc_19276 = NOVALUE;
    int _fail_list_19282 = NOVALUE;
    int _temps_19330 = NOVALUE;
    int _ln_19331 = NOVALUE;
    int _10872 = NOVALUE;
    int _10870 = NOVALUE;
    int _10868 = NOVALUE;
    int _10864 = NOVALUE;
    int _10863 = NOVALUE;
    int _10860 = NOVALUE;
    int _10859 = NOVALUE;
    int _10857 = NOVALUE;
    int _10856 = NOVALUE;
    int _10854 = NOVALUE;
    int _10852 = NOVALUE;
    int _10851 = NOVALUE;
    int _10850 = NOVALUE;
    int _10847 = NOVALUE;
    int _10844 = NOVALUE;
    int _10839 = NOVALUE;
    int _10838 = NOVALUE;
    int _10836 = NOVALUE;
    int _10833 = NOVALUE;
    int _10832 = NOVALUE;
    int _10829 = NOVALUE;
    int _10828 = NOVALUE;
    int _10827 = NOVALUE;
    int _10825 = NOVALUE;
    int _10824 = NOVALUE;
    int _10812 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer first_counter = length(files)+1*/
    if (IS_SEQUENCE(_files_19270)){
            _10812 = SEQ_PTR(_files_19270)->length;
    }
    else {
        _10812 = 1;
    }
    _first_counter_19272 = _10812 + 1;
    _10812 = NOVALUE;

    /** 	integer log_fd = 0*/
    _log_fd_19275 = 0;

    /** 	total = length(files)*/
    if (IS_SEQUENCE(_files_19270)){
            _1total_18679 = SEQ_PTR(_files_19270)->length;
    }
    else {
        _1total_18679 = 1;
    }

    /** 	if verbose_switch <= 0 then*/
    if (_1verbose_switch_18669 > 0)
    goto L1; // [32] 45

    /** 		translator_options &= " -silent"*/
    Concat((object_ptr)&_1translator_options_19188, _1translator_options_19188, _10816);
L1: 

    /** 	sequence fail_list = {}*/
    RefDS(_5);
    DeRef(_fail_list_19282);
    _fail_list_19282 = _5;

    /** 	if logging_activated then*/
    if (_1logging_activated_18677 == 0)
    {
        goto L2; // [56] 87
    }
    else{
    }

    /** 		void = delete_file("unittest.dat")*/
    RefDS(_10818);
    _0 = _8delete_file(_10818);
    DeRef(_1void_18670);
    _1void_18670 = _0;

    /** 		void = delete_file("unittest.log")*/
    RefDS(_10554);
    _0 = _8delete_file(_10554);
    DeRef(_1void_18670);
    _1void_18670 = _0;

    /** 		void = delete_file("ctc.log")*/
    RefDS(_10821);
    _0 = _8delete_file(_10821);
    DeRef(_1void_18670);
    _1void_18670 = _0;

    /** 		ctcfh = open("ctc.log", "w")*/
    _1ctcfh_18671 = EOpen(_10821, _4110, 0);
L2: 

    /** 	for i = 1 to length(files) do*/
    if (IS_SEQUENCE(_files_19270)){
            _10824 = SEQ_PTR(_files_19270)->length;
    }
    else {
        _10824 = 1;
    }
    {
        int _i_19294;
        _i_19294 = 1;
L3: 
        if (_i_19294 > _10824){
            goto L4; // [92] 126
        }

        /** 		fail_list = test_file( files[i], fail_list )*/
        _2 = (int)SEQ_PTR(_files_19270);
        _10825 = (int)*(((s1_ptr)_2)->base + _i_19294);
        Ref(_10825);
        RefDS(_fail_list_19282);
        _0 = _fail_list_19282;
        _fail_list_19282 = _1test_file(_10825, _fail_list_19282);
        DeRefDS(_0);
        _10825 = NOVALUE;

        /** 		coverage_erase = ""*/
        RefDS(_5);
        DeRefi(_1coverage_erase_19197);
        _1coverage_erase_19197 = _5;

        /** 	end for*/
        _i_19294 = _i_19294 + 1;
        goto L3; // [121] 99
L4: 
        ;
    }

    /** 	if logging_activated and ctcfh != -1 then*/
    if (_1logging_activated_18677 == 0) {
        goto L5; // [130] 198
    }
    _10828 = (_1ctcfh_18671 != -1);
    if (_10828 == 0)
    {
        DeRef(_10828);
        _10828 = NOVALUE;
        goto L5; // [141] 198
    }
    else{
        DeRef(_10828);
        _10828 = NOVALUE;
    }

    /** 		print(ctcfh, error_list)*/
    StdPrint(_1ctcfh_18671, _1error_list_18672, 0);

    /** 		puts(ctcfh, 10)*/
    EPuts(_1ctcfh_18671, 10); // DJP 

    /** 		if length(translator) > 0 then*/
    if (IS_SEQUENCE(_1translator_19190)){
            _10829 = SEQ_PTR(_1translator_19190)->length;
    }
    else {
        _10829 = 1;
    }
    if (_10829 <= 0)
    goto L6; // [167] 183

    /** 			print(ctcfh, total)*/
    StdPrint(_1ctcfh_18671, _1total_18679, 0);
    goto L7; // [180] 191
L6: 

    /** 			print(ctcfh, 0)*/
    StdPrint(_1ctcfh_18671, 0, 0);
L7: 

    /** 		close(ctcfh)*/
    EClose(_1ctcfh_18671);
L5: 

    /** 	ctcfh = 0*/
    _1ctcfh_18671 = 0;

    /** 	if total = 0 then*/
    if (_1total_18679 != 0)
    goto L8; // [209] 221

    /** 		score = 100*/
    DeRef(_score_19271);
    _score_19271 = 100;
    goto L9; // [218] 242
L8: 

    /** 		score = ((total - failed) / total) * 100*/
    _10832 = _1total_18679 - _1failed_18678;
    if ((long)((unsigned long)_10832 +(unsigned long) HIGH_BITS) >= 0){
        _10832 = NewDouble((double)_10832);
    }
    if (IS_ATOM_INT(_10832)) {
        _10833 = (_10832 % _1total_18679) ? NewDouble((double)_10832 / _1total_18679) : (_10832 / _1total_18679);
    }
    else {
        _10833 = NewDouble(DBL_PTR(_10832)->dbl / (double)_1total_18679);
    }
    DeRef(_10832);
    _10832 = NOVALUE;
    DeRef(_score_19271);
    if (IS_ATOM_INT(_10833)) {
        if (_10833 == (short)_10833)
        _score_19271 = _10833 * 100;
        else
        _score_19271 = NewDouble(_10833 * (double)100);
    }
    else {
        _score_19271 = NewDouble(DBL_PTR(_10833)->dbl * (double)100);
    }
    DeRef(_10833);
    _10833 = NOVALUE;
L9: 

    /** 	puts(1, "\nTest results summary:\n")*/
    EPuts(1, _10835); // DJP 

    /** 	for i = 1 to length(fail_list) do*/
    if (IS_SEQUENCE(_fail_list_19282)){
            _10836 = SEQ_PTR(_fail_list_19282)->length;
    }
    else {
        _10836 = 1;
    }
    {
        int _i_19313;
        _i_19313 = 1;
LA: 
        if (_i_19313 > _10836){
            goto LB; // [252] 280
        }

        /** 		printf(1, "    FAIL: %s\n", {fail_list[i]})*/
        _2 = (int)SEQ_PTR(_fail_list_19282);
        _10838 = (int)*(((s1_ptr)_2)->base + _i_19313);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_10838);
        *((int *)(_2+4)) = _10838;
        _10839 = MAKE_SEQ(_1);
        _10838 = NOVALUE;
        EPrintf(1, _10837, _10839);
        DeRefDS(_10839);
        _10839 = NOVALUE;

        /** 	end for*/
        _i_19313 = _i_19313 + 1;
        goto LA; // [275] 259
LB: 
        ;
    }

    /** 	if failed != 0 then*/
    if (_1failed_18678 == 0)
    goto LC; // [284] 332

    /** 		respc = sprintf("%1.f%%", score)*/
    DeRefi(_respc_19276);
    _respc_19276 = EPrintf(-9999999, _10841, _score_19271);

    /** 		if equal(respc, "100%") then*/
    if (_respc_19276 == _10843)
    _10844 = 1;
    else if (IS_ATOM_INT(_respc_19276) && IS_ATOM_INT(_10843))
    _10844 = 0;
    else
    _10844 = (compare(_respc_19276, _10843) == 0);
    if (_10844 == 0)
    {
        _10844 = NOVALUE;
        goto LD; // [302] 313
    }
    else{
        _10844 = NOVALUE;
    }

    /** 			respc = "99.99%"*/
    RefDS(_10845);
    DeRefDSi(_respc_19276);
    _respc_19276 = _10845;
LD: 

    /** 		printf(1, "Files (run: %d) (failed: %d) (%s success)\n", {total, failed, respc})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1total_18679;
    *((int *)(_2+8)) = _1failed_18678;
    RefDS(_respc_19276);
    *((int *)(_2+12)) = _respc_19276;
    _10847 = MAKE_SEQ(_1);
    EPrintf(1, _10846, _10847);
    DeRefDS(_10847);
    _10847 = NOVALUE;
    goto LE; // [329] 341
LC: 

    /** 		printf(1, "Files (run: %d) (failed: 0) (100%% success)\n", total)*/
    EPrintf(1, _10848, _1total_18679);
LE: 

    /** 	object temps, ln = read_lines("unittest.dat")*/
    RefDS(_10818);
    _0 = _ln_19331;
    _ln_19331 = _15read_lines(_10818);
    DeRef(_0);

    /** 	if sequence(ln) then*/
    _10850 = IS_SEQUENCE(_ln_19331);
    if (_10850 == 0)
    {
        _10850 = NOVALUE;
        goto LF; // [352] 554
    }
    else{
        _10850 = NOVALUE;
    }

    /** 		total = 0*/
    _1total_18679 = 0;

    /** 		failed = 0*/
    _1failed_18678 = 0;

    /** 		for i = 1 to length(ln) do*/
    if (IS_SEQUENCE(_ln_19331)){
            _10851 = SEQ_PTR(_ln_19331)->length;
    }
    else {
        _10851 = 1;
    }
    {
        int _i_19337;
        _i_19337 = 1;
L10: 
        if (_i_19337 > _10851){
            goto L11; // [374] 455
        }

        /** 			temps = value(ln[i])*/
        _2 = (int)SEQ_PTR(_ln_19331);
        _10852 = (int)*(((s1_ptr)_2)->base + _i_19337);
        Ref(_10852);
        _0 = _temps_19330;
        _temps_19330 = _14value(_10852, 1, _14GET_SHORT_ANSWER_8123);
        DeRef(_0);
        _10852 = NOVALUE;

        /** 			if temps[1] = GET_SUCCESS then*/
        _2 = (int)SEQ_PTR(_temps_19330);
        _10854 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _10854, 0)){
            _10854 = NOVALUE;
            goto L12; // [403] 448
        }
        _10854 = NOVALUE;

        /** 				total += temps[2][1]*/
        _2 = (int)SEQ_PTR(_temps_19330);
        _10856 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_10856);
        _10857 = (int)*(((s1_ptr)_2)->base + 1);
        _10856 = NOVALUE;
        if (IS_ATOM_INT(_10857)) {
            _1total_18679 = _1total_18679 + _10857;
        }
        else {
            _1total_18679 = binary_op(PLUS, _1total_18679, _10857);
        }
        _10857 = NOVALUE;
        if (!IS_ATOM_INT(_1total_18679)) {
            _1 = (long)(DBL_PTR(_1total_18679)->dbl);
            if (UNIQUE(DBL_PTR(_1total_18679)) && (DBL_PTR(_1total_18679)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_1total_18679);
            _1total_18679 = _1;
        }

        /** 				failed += temps[2][2]*/
        _2 = (int)SEQ_PTR(_temps_19330);
        _10859 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_10859);
        _10860 = (int)*(((s1_ptr)_2)->base + 2);
        _10859 = NOVALUE;
        if (IS_ATOM_INT(_10860)) {
            _1failed_18678 = _1failed_18678 + _10860;
        }
        else {
            _1failed_18678 = binary_op(PLUS, _1failed_18678, _10860);
        }
        _10860 = NOVALUE;
        if (!IS_ATOM_INT(_1failed_18678)) {
            _1 = (long)(DBL_PTR(_1failed_18678)->dbl);
            if (UNIQUE(DBL_PTR(_1failed_18678)) && (DBL_PTR(_1failed_18678)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_1failed_18678);
            _1failed_18678 = _1;
        }
L12: 

        /** 		end for*/
        _i_19337 = _i_19337 + 1;
        goto L10; // [450] 381
L11: 
        ;
    }

    /** 		if total = 0 then*/
    if (_1total_18679 != 0)
    goto L13; // [459] 471

    /** 			score = 100*/
    DeRef(_score_19271);
    _score_19271 = 100;
    goto L14; // [468] 492
L13: 

    /** 			score = ((total - failed) / total) * 100*/
    _10863 = _1total_18679 - _1failed_18678;
    if ((long)((unsigned long)_10863 +(unsigned long) HIGH_BITS) >= 0){
        _10863 = NewDouble((double)_10863);
    }
    if (IS_ATOM_INT(_10863)) {
        _10864 = (_10863 % _1total_18679) ? NewDouble((double)_10863 / _1total_18679) : (_10863 / _1total_18679);
    }
    else {
        _10864 = NewDouble(DBL_PTR(_10863)->dbl / (double)_1total_18679);
    }
    DeRef(_10863);
    _10863 = NOVALUE;
    DeRef(_score_19271);
    if (IS_ATOM_INT(_10864)) {
        if (_10864 == (short)_10864)
        _score_19271 = _10864 * 100;
        else
        _score_19271 = NewDouble(_10864 * (double)100);
    }
    else {
        _score_19271 = NewDouble(DBL_PTR(_10864)->dbl * (double)100);
    }
    DeRef(_10864);
    _10864 = NOVALUE;
L14: 

    /** 		if failed != 0 then*/
    if (_1failed_18678 == 0)
    goto L15; // [496] 544

    /** 			respc = sprintf("%1.f%%", score)*/
    DeRefi(_respc_19276);
    _respc_19276 = EPrintf(-9999999, _10841, _score_19271);

    /** 			if equal(respc, "100%") then*/
    if (_respc_19276 == _10843)
    _10868 = 1;
    else if (IS_ATOM_INT(_respc_19276) && IS_ATOM_INT(_10843))
    _10868 = 0;
    else
    _10868 = (compare(_respc_19276, _10843) == 0);
    if (_10868 == 0)
    {
        _10868 = NOVALUE;
        goto L16; // [514] 525
    }
    else{
        _10868 = NOVALUE;
    }

    /** 				respc = "99.99%"*/
    RefDS(_10845);
    DeRefDSi(_respc_19276);
    _respc_19276 = _10845;
L16: 

    /** 			printf(1, "Tests (run: %d) (failed: %d) (%s success)\n", {total, failed, respc})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1total_18679;
    *((int *)(_2+8)) = _1failed_18678;
    RefDS(_respc_19276);
    *((int *)(_2+12)) = _respc_19276;
    _10870 = MAKE_SEQ(_1);
    EPrintf(1, _10869, _10870);
    DeRefDS(_10870);
    _10870 = NOVALUE;
    goto L17; // [541] 553
L15: 

    /** 			printf(1, "Tests (run: %d) (failed: 0) (100%% success)\n", total)*/
    EPrintf(1, _10871, _1total_18679);
L17: 
LF: 

    /** 	process_coverage()*/
    _1process_coverage();

    /** 	abort(failed > 0)*/
    _10872 = (_1failed_18678 > 0);
    UserCleanup(_10872);
    _10872 = NOVALUE;

    /** end procedure*/
    DeRefDS(_files_19270);
    DeRef(_score_19271);
    DeRefi(_respc_19276);
    DeRef(_fail_list_19282);
    DeRef(_temps_19330);
    DeRef(_ln_19331);
    return;
    ;
}


void _1ascii_out(int _data_19371)
{
    int _anum_19406 = NOVALUE;
    int _status_19426 = NOVALUE;
    int _10924 = NOVALUE;
    int _10923 = NOVALUE;
    int _10922 = NOVALUE;
    int _10921 = NOVALUE;
    int _10920 = NOVALUE;
    int _10916 = NOVALUE;
    int _10914 = NOVALUE;
    int _10913 = NOVALUE;
    int _10912 = NOVALUE;
    int _10911 = NOVALUE;
    int _10910 = NOVALUE;
    int _10908 = NOVALUE;
    int _10907 = NOVALUE;
    int _10904 = NOVALUE;
    int _10901 = NOVALUE;
    int _10899 = NOVALUE;
    int _10898 = NOVALUE;
    int _10896 = NOVALUE;
    int _10895 = NOVALUE;
    int _10894 = NOVALUE;
    int _10893 = NOVALUE;
    int _10892 = NOVALUE;
    int _10890 = NOVALUE;
    int _10889 = NOVALUE;
    int _10888 = NOVALUE;
    int _10887 = NOVALUE;
    int _10886 = NOVALUE;
    int _10885 = NOVALUE;
    int _10884 = NOVALUE;
    int _10883 = NOVALUE;
    int _10882 = NOVALUE;
    int _10881 = NOVALUE;
    int _10880 = NOVALUE;
    int _10879 = NOVALUE;
    int _10878 = NOVALUE;
    int _10876 = NOVALUE;
    int _10873 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch data[1] do*/
    _2 = (int)SEQ_PTR(_data_19371);
    _10873 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = find(_10873, _10874);
    _10873 = NOVALUE;
    switch ( _1 ){ 

        /** 		case "file" then*/
        case 1:

        /** 			unsummarized_files = append(unsummarized_files, data[2])*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10876 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_10876);
        Append(&_1unsummarized_files_19368, _1unsummarized_files_19368, _10876);
        _10876 = NOVALUE;

        /** 			printf(1, "%s\n", { data[2] })*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10878 = (int)*(((s1_ptr)_2)->base + 2);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_10878);
        *((int *)(_2+4)) = _10878;
        _10879 = MAKE_SEQ(_1);
        _10878 = NOVALUE;
        EPrintf(1, _8888, _10879);
        DeRefDS(_10879);
        _10879 = NOVALUE;

        /** 			puts(1, repeat('=', 76) & "\n")*/
        _10880 = Repeat(61, 76);
        Concat((object_ptr)&_10881, _10880, _3957);
        DeRefDS(_10880);
        _10880 = NOVALUE;
        DeRef(_10880);
        _10880 = NOVALUE;
        EPuts(1, _10881); // DJP 
        DeRefDS(_10881);
        _10881 = NOVALUE;

        /** 			if find(data[2], error_list[1]) then*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10882 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _10883 = (int)*(((s1_ptr)_2)->base + 1);
        _10884 = find_from(_10882, _10883, 1);
        _10882 = NOVALUE;
        _10883 = NOVALUE;
        if (_10884 == 0)
        {
            _10884 = NOVALUE;
            goto L1; // [74] 353
        }
        else{
            _10884 = NOVALUE;
        }

        /** 				printf(1,"%s\n",*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _10885 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19371);
        _10886 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _10887 = (int)*(((s1_ptr)_2)->base + 1);
        _10888 = find_from(_10886, _10887, 1);
        _10886 = NOVALUE;
        _10887 = NOVALUE;
        _2 = (int)SEQ_PTR(_10885);
        _10889 = (int)*(((s1_ptr)_2)->base + _10888);
        _10885 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_10889);
        *((int *)(_2+4)) = _10889;
        _10890 = MAKE_SEQ(_1);
        _10889 = NOVALUE;
        EPrintf(1, _8888, _10890);
        DeRefDS(_10890);
        _10890 = NOVALUE;
        goto L1; // [113] 353

        /** 		case "failed" then*/
        case 2:

        /** 			printf(1, "  Failed: %s (%f) expected ", { data[2], data[5] })*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10892 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19371);
        _10893 = (int)*(((s1_ptr)_2)->base + 5);
        Ref(_10893);
        Ref(_10892);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _10892;
        ((int *)_2)[2] = _10893;
        _10894 = MAKE_SEQ(_1);
        _10893 = NOVALUE;
        _10892 = NOVALUE;
        EPrintf(1, _10891, _10894);
        DeRefDS(_10894);
        _10894 = NOVALUE;

        /** 			pretty_print(1, data[3], {2, 2, 12, 78, "%d", "%.15g"})*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10895 = (int)*(((s1_ptr)_2)->base + 3);
        _1 = NewS1(6);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 2;
        *((int *)(_2+12)) = 12;
        *((int *)(_2+16)) = 78;
        RefDS(_106);
        *((int *)(_2+20)) = _106;
        RefDS(_3816);
        *((int *)(_2+24)) = _3816;
        _10896 = MAKE_SEQ(_1);
        Ref(_10895);
        _2pretty_print(1, _10895, _10896);
        _10895 = NOVALUE;
        _10896 = NOVALUE;

        /** 			puts(1, " got ")*/
        EPuts(1, _10897); // DJP 

        /** 			pretty_print(1, data[4], {2, 2, 12, 78, "%d", "%.15g"})*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10898 = (int)*(((s1_ptr)_2)->base + 4);
        _1 = NewS1(6);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 2;
        *((int *)(_2+12)) = 12;
        *((int *)(_2+16)) = 78;
        RefDS(_106);
        *((int *)(_2+20)) = _106;
        RefDS(_3816);
        *((int *)(_2+24)) = _3816;
        _10899 = MAKE_SEQ(_1);
        Ref(_10898);
        _2pretty_print(1, _10898, _10899);
        _10898 = NOVALUE;
        _10899 = NOVALUE;

        /** 			puts(1, "\n")*/
        EPuts(1, _3957); // DJP 
        goto L1; // [187] 353

        /** 		case "passed" then*/
        case 3:

        /** 			sequence anum*/

        /** 			if data[3] = 0 then*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10901 = (int)*(((s1_ptr)_2)->base + 3);
        if (binary_op_a(NOTEQ, _10901, 0)){
            _10901 = NOVALUE;
            goto L2; // [201] 215
        }
        _10901 = NOVALUE;

        /** 				anum = "0"*/
        RefDS(_8600);
        DeRefi(_anum_19406);
        _anum_19406 = _8600;
        goto L3; // [212] 226
L2: 

        /** 				anum = sprintf("%f", data[3])*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10904 = (int)*(((s1_ptr)_2)->base + 3);
        DeRefi(_anum_19406);
        _anum_19406 = EPrintf(-9999999, _10903, _10904);
        _10904 = NOVALUE;
L3: 

        /** 			printf(1, "  Passed: %s (%s)\n", { data[2], anum })*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10907 = (int)*(((s1_ptr)_2)->base + 2);
        RefDS(_anum_19406);
        Ref(_10907);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _10907;
        ((int *)_2)[2] = _anum_19406;
        _10908 = MAKE_SEQ(_1);
        _10907 = NOVALUE;
        EPrintf(1, _10906, _10908);
        DeRefDS(_10908);
        _10908 = NOVALUE;
        DeRefDSi(_anum_19406);
        _anum_19406 = NOVALUE;
        goto L1; // [244] 353

        /** 		case "summary" then*/
        case 4:

        /** 			puts(1, repeat('-', 76) & "\n")*/
        _10910 = Repeat(45, 76);
        Concat((object_ptr)&_10911, _10910, _3957);
        DeRefDS(_10910);
        _10910 = NOVALUE;
        DeRef(_10910);
        _10910 = NOVALUE;
        EPuts(1, _10911); // DJP 
        DeRefDS(_10911);
        _10911 = NOVALUE;

        /** 			if length(unsummarized_files) then*/
        if (IS_SEQUENCE(_1unsummarized_files_19368)){
                _10912 = SEQ_PTR(_1unsummarized_files_19368)->length;
        }
        else {
            _10912 = 1;
        }
        if (_10912 == 0)
        {
            _10912 = NOVALUE;
            goto L4; // [270] 290
        }
        else{
            _10912 = NOVALUE;
        }

        /** 				unsummarized_files = unsummarized_files[1..$-1]*/
        if (IS_SEQUENCE(_1unsummarized_files_19368)){
                _10913 = SEQ_PTR(_1unsummarized_files_19368)->length;
        }
        else {
            _10913 = 1;
        }
        _10914 = _10913 - 1;
        _10913 = NOVALUE;
        rhs_slice_target = (object_ptr)&_1unsummarized_files_19368;
        RHS_Slice(_1unsummarized_files_19368, 1, _10914);
L4: 

        /** 			sequence status*/

        /** 			if data[3] > 0 then*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10916 = (int)*(((s1_ptr)_2)->base + 3);
        if (binary_op_a(LESSEQ, _10916, 0)){
            _10916 = NOVALUE;
            goto L5; // [298] 312
        }
        _10916 = NOVALUE;

        /** 				status = "bad"*/
        RefDS(_10918);
        DeRefi(_status_19426);
        _status_19426 = _10918;
        goto L6; // [309] 320
L5: 

        /** 				status = "ok"*/
        RefDS(_8595);
        DeRefi(_status_19426);
        _status_19426 = _8595;
L6: 

        /** 			printf(1, "  (Tests: %04d) (Status: %3s) (Failed: %04d) (Passed: %04d) (Time: %f)\n\n", {*/
        _2 = (int)SEQ_PTR(_data_19371);
        _10920 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19371);
        _10921 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_data_19371);
        _10922 = (int)*(((s1_ptr)_2)->base + 4);
        _2 = (int)SEQ_PTR(_data_19371);
        _10923 = (int)*(((s1_ptr)_2)->base + 5);
        _1 = NewS1(5);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_10920);
        *((int *)(_2+4)) = _10920;
        RefDS(_status_19426);
        *((int *)(_2+8)) = _status_19426;
        Ref(_10921);
        *((int *)(_2+12)) = _10921;
        Ref(_10922);
        *((int *)(_2+16)) = _10922;
        Ref(_10923);
        *((int *)(_2+20)) = _10923;
        _10924 = MAKE_SEQ(_1);
        _10923 = NOVALUE;
        _10922 = NOVALUE;
        _10921 = NOVALUE;
        _10920 = NOVALUE;
        EPrintf(1, _10919, _10924);
        DeRefDS(_10924);
        _10924 = NOVALUE;
    ;}L1: 
    DeRefi(_status_19426);
    _status_19426 = NOVALUE;

    /** end procedure*/
    DeRefDS(_data_19371);
    DeRef(_10914);
    _10914 = NOVALUE;
    return;
    ;
}


int _1text2html(int _t_19440)
{
    int _f_19441 = NOVALUE;
    int _10962 = NOVALUE;
    int _10961 = NOVALUE;
    int _10960 = NOVALUE;
    int _10958 = NOVALUE;
    int _10957 = NOVALUE;
    int _10955 = NOVALUE;
    int _10953 = NOVALUE;
    int _10952 = NOVALUE;
    int _10951 = NOVALUE;
    int _10950 = NOVALUE;
    int _10949 = NOVALUE;
    int _10947 = NOVALUE;
    int _10946 = NOVALUE;
    int _10944 = NOVALUE;
    int _10942 = NOVALUE;
    int _10941 = NOVALUE;
    int _10940 = NOVALUE;
    int _10938 = NOVALUE;
    int _10937 = NOVALUE;
    int _10935 = NOVALUE;
    int _10933 = NOVALUE;
    int _10932 = NOVALUE;
    int _10931 = NOVALUE;
    int _10929 = NOVALUE;
    int _10928 = NOVALUE;
    int _10926 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer f = length(t)*/
    if (IS_SEQUENCE(_t_19440)){
            _f_19441 = SEQ_PTR(_t_19440)->length;
    }
    else {
        _f_19441 = 1;
    }

    /** 	while f do*/
L1: 
    if (_f_19441 == 0)
    {
        goto L2; // [15] 207
    }
    else{
    }

    /** 		if t[f] = '<' then*/
    _2 = (int)SEQ_PTR(_t_19440);
    _10926 = (int)*(((s1_ptr)_2)->base + _f_19441);
    if (binary_op_a(NOTEQ, _10926, 60)){
        _10926 = NOVALUE;
        goto L3; // [24] 60
    }
    _10926 = NOVALUE;

    /** 			t = t[1..f-1] & "&lt;" & t[f+1..$]*/
    _10928 = _f_19441 - 1;
    rhs_slice_target = (object_ptr)&_10929;
    RHS_Slice(_t_19440, 1, _10928);
    _10931 = _f_19441 + 1;
    if (_10931 > MAXINT){
        _10931 = NewDouble((double)_10931);
    }
    if (IS_SEQUENCE(_t_19440)){
            _10932 = SEQ_PTR(_t_19440)->length;
    }
    else {
        _10932 = 1;
    }
    rhs_slice_target = (object_ptr)&_10933;
    RHS_Slice(_t_19440, _10931, _10932);
    {
        int concat_list[3];

        concat_list[0] = _10933;
        concat_list[1] = _10930;
        concat_list[2] = _10929;
        Concat_N((object_ptr)&_t_19440, concat_list, 3);
    }
    DeRefDS(_10933);
    _10933 = NOVALUE;
    DeRefDS(_10929);
    _10929 = NOVALUE;
    goto L4; // [57] 194
L3: 

    /** 		elsif t[f] = '>' then*/
    _2 = (int)SEQ_PTR(_t_19440);
    _10935 = (int)*(((s1_ptr)_2)->base + _f_19441);
    if (binary_op_a(NOTEQ, _10935, 62)){
        _10935 = NOVALUE;
        goto L5; // [66] 102
    }
    _10935 = NOVALUE;

    /** 			t = t[1..f-1] & "&gt;" & t[f+1..$]*/
    _10937 = _f_19441 - 1;
    rhs_slice_target = (object_ptr)&_10938;
    RHS_Slice(_t_19440, 1, _10937);
    _10940 = _f_19441 + 1;
    if (_10940 > MAXINT){
        _10940 = NewDouble((double)_10940);
    }
    if (IS_SEQUENCE(_t_19440)){
            _10941 = SEQ_PTR(_t_19440)->length;
    }
    else {
        _10941 = 1;
    }
    rhs_slice_target = (object_ptr)&_10942;
    RHS_Slice(_t_19440, _10940, _10941);
    {
        int concat_list[3];

        concat_list[0] = _10942;
        concat_list[1] = _10939;
        concat_list[2] = _10938;
        Concat_N((object_ptr)&_t_19440, concat_list, 3);
    }
    DeRefDS(_10942);
    _10942 = NOVALUE;
    DeRefDS(_10938);
    _10938 = NOVALUE;
    goto L4; // [99] 194
L5: 

    /** 		elsif t[f] > 127 then*/
    _2 = (int)SEQ_PTR(_t_19440);
    _10944 = (int)*(((s1_ptr)_2)->base + _f_19441);
    if (binary_op_a(LESSEQ, _10944, 127)){
        _10944 = NOVALUE;
        goto L6; // [108] 153
    }
    _10944 = NOVALUE;

    /** 			t = t[1..f-1] & sprintf("&#%x;", t[f..f]) & t[f+1..$]*/
    _10946 = _f_19441 - 1;
    rhs_slice_target = (object_ptr)&_10947;
    RHS_Slice(_t_19440, 1, _10946);
    rhs_slice_target = (object_ptr)&_10949;
    RHS_Slice(_t_19440, _f_19441, _f_19441);
    _10950 = EPrintf(-9999999, _10948, _10949);
    DeRefDS(_10949);
    _10949 = NOVALUE;
    _10951 = _f_19441 + 1;
    if (_10951 > MAXINT){
        _10951 = NewDouble((double)_10951);
    }
    if (IS_SEQUENCE(_t_19440)){
            _10952 = SEQ_PTR(_t_19440)->length;
    }
    else {
        _10952 = 1;
    }
    rhs_slice_target = (object_ptr)&_10953;
    RHS_Slice(_t_19440, _10951, _10952);
    {
        int concat_list[3];

        concat_list[0] = _10953;
        concat_list[1] = _10950;
        concat_list[2] = _10947;
        Concat_N((object_ptr)&_t_19440, concat_list, 3);
    }
    DeRefDS(_10953);
    _10953 = NOVALUE;
    DeRefDS(_10950);
    _10950 = NOVALUE;
    DeRefDS(_10947);
    _10947 = NOVALUE;
    goto L4; // [150] 194
L6: 

    /** 		elsif t[f] = 10 then*/
    _2 = (int)SEQ_PTR(_t_19440);
    _10955 = (int)*(((s1_ptr)_2)->base + _f_19441);
    if (binary_op_a(NOTEQ, _10955, 10)){
        _10955 = NOVALUE;
        goto L7; // [159] 193
    }
    _10955 = NOVALUE;

    /** 			t = t[1..f-1] & "<br>" & t[f+1..$]*/
    _10957 = _f_19441 - 1;
    rhs_slice_target = (object_ptr)&_10958;
    RHS_Slice(_t_19440, 1, _10957);
    _10960 = _f_19441 + 1;
    if (_10960 > MAXINT){
        _10960 = NewDouble((double)_10960);
    }
    if (IS_SEQUENCE(_t_19440)){
            _10961 = SEQ_PTR(_t_19440)->length;
    }
    else {
        _10961 = 1;
    }
    rhs_slice_target = (object_ptr)&_10962;
    RHS_Slice(_t_19440, _10960, _10961);
    {
        int concat_list[3];

        concat_list[0] = _10962;
        concat_list[1] = _10959;
        concat_list[2] = _10958;
        Concat_N((object_ptr)&_t_19440, concat_list, 3);
    }
    DeRefDS(_10962);
    _10962 = NOVALUE;
    DeRefDS(_10958);
    _10958 = NOVALUE;
L7: 
L4: 

    /** 		f = f - 1*/
    _f_19441 = _f_19441 - 1;

    /** 	end while*/
    goto L1; // [204] 15
L2: 

    /** 	return t*/
    DeRef(_10928);
    _10928 = NOVALUE;
    DeRef(_10937);
    _10937 = NOVALUE;
    DeRef(_10931);
    _10931 = NOVALUE;
    DeRef(_10946);
    _10946 = NOVALUE;
    DeRef(_10940);
    _10940 = NOVALUE;
    DeRef(_10957);
    _10957 = NOVALUE;
    DeRef(_10951);
    _10951 = NOVALUE;
    DeRef(_10960);
    _10960 = NOVALUE;
    return _t_19440;
    ;
}


void _1html_out(int _data_19542)
{
    int _err_19547 = NOVALUE;
    int _color_19552 = NOVALUE;
    int _ex_err_19558 = NOVALUE;
    int _anum_19623 = NOVALUE;
    int _11071 = NOVALUE;
    int _11070 = NOVALUE;
    int _11069 = NOVALUE;
    int _11068 = NOVALUE;
    int _11067 = NOVALUE;
    int _11065 = NOVALUE;
    int _11064 = NOVALUE;
    int _11063 = NOVALUE;
    int _11062 = NOVALUE;
    int _11061 = NOVALUE;
    int _11059 = NOVALUE;
    int _11057 = NOVALUE;
    int _11056 = NOVALUE;
    int _11055 = NOVALUE;
    int _11054 = NOVALUE;
    int _11053 = NOVALUE;
    int _11052 = NOVALUE;
    int _11051 = NOVALUE;
    int _11050 = NOVALUE;
    int _11049 = NOVALUE;
    int _11047 = NOVALUE;
    int _11046 = NOVALUE;
    int _11045 = NOVALUE;
    int _11044 = NOVALUE;
    int _11042 = NOVALUE;
    int _11041 = NOVALUE;
    int _11040 = NOVALUE;
    int _11039 = NOVALUE;
    int _11038 = NOVALUE;
    int _11037 = NOVALUE;
    int _11036 = NOVALUE;
    int _11035 = NOVALUE;
    int _11034 = NOVALUE;
    int _11033 = NOVALUE;
    int _11032 = NOVALUE;
    int _11031 = NOVALUE;
    int _11030 = NOVALUE;
    int _11029 = NOVALUE;
    int _11028 = NOVALUE;
    int _11027 = NOVALUE;
    int _11026 = NOVALUE;
    int _11025 = NOVALUE;
    int _11023 = NOVALUE;
    int _11022 = NOVALUE;
    int _11021 = NOVALUE;
    int _11020 = NOVALUE;
    int _11019 = NOVALUE;
    int _11018 = NOVALUE;
    int _11017 = NOVALUE;
    int _11016 = NOVALUE;
    int _11015 = NOVALUE;
    int _11014 = NOVALUE;
    int _11013 = NOVALUE;
    int _11012 = NOVALUE;
    int _11011 = NOVALUE;
    int _11010 = NOVALUE;
    int _11009 = NOVALUE;
    int _11008 = NOVALUE;
    int _11006 = NOVALUE;
    int _11005 = NOVALUE;
    int _11003 = NOVALUE;
    int _11002 = NOVALUE;
    int _11000 = NOVALUE;
    int _10999 = NOVALUE;
    int _10996 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch data[1] do*/
    _2 = (int)SEQ_PTR(_data_19542);
    _10996 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = find(_10996, _10997);
    _10996 = NOVALUE;
    switch ( _1 ){ 

        /** 		case "file" then*/
        case 1:

        /** 			integer err = find(data[2], error_list[1])*/
        _2 = (int)SEQ_PTR(_data_19542);
        _10999 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11000 = (int)*(((s1_ptr)_2)->base + 1);
        _err_19547 = find_from(_10999, _11000, 1);
        _10999 = NOVALUE;
        _11000 = NOVALUE;

        /** 			if err then*/
        if (_err_19547 == 0)
        {
            goto L1; // [39] 420
        }
        else{
        }

        /** 				sequence color*/

        /** 				if error_list[3][err] = E_NOERROR then*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11002 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_11002);
        _11003 = (int)*(((s1_ptr)_2)->base + _err_19547);
        _11002 = NOVALUE;
        if (binary_op_a(NOTEQ, _11003, 1)){
            _11003 = NOVALUE;
            goto L2; // [58] 70
        }
        _11003 = NOVALUE;

        /** 					color = no_error_color*/
        RefDS(_1no_error_color_19487);
        DeRef(_color_19552);
        _color_19552 = _1no_error_color_19487;
        goto L3; // [67] 76
L2: 

        /** 					color = error_color*/
        RefDS(_1error_color_19489);
        DeRef(_color_19552);
        _color_19552 = _1error_color_19489;
L3: 

        /** 				object ex_err = regex:matches(differing_ex_err_pattern, error_list[2][err])*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11005 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_11005);
        _11006 = (int)*(((s1_ptr)_2)->base + _err_19547);
        _11005 = NOVALUE;
        Ref(_1differing_ex_err_pattern_19510);
        Ref(_11006);
        _0 = _ex_err_19558;
        _ex_err_19558 = _34matches(_1differing_ex_err_pattern_19510, _11006, 1, 0);
        DeRef(_0);
        _11006 = NOVALUE;

        /** 				if sequence(ex_err) then*/
        _11008 = IS_SEQUENCE(_ex_err_19558);
        if (_11008 == 0)
        {
            _11008 = NOVALUE;
            goto L4; // [102] 153
        }
        else{
            _11008 = NOVALUE;
        }

        /** 					printf(html_fn, html_unexpected_exerr_table_begin, {data[2], data[2]} )*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11009 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19542);
        _11010 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_11010);
        Ref(_11009);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _11009;
        ((int *)_2)[2] = _11010;
        _11011 = MAKE_SEQ(_1);
        _11010 = NOVALUE;
        _11009 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_unexpected_exerr_table_begin_19515, _11011);
        DeRefDS(_11011);
        _11011 = NOVALUE;

        /** 					printf(html_fn, html_unexpected_exerr_row_format, ex_err[2..3])*/
        rhs_slice_target = (object_ptr)&_11012;
        RHS_Slice(_ex_err_19558, 2, 3);
        EPrintf(_1html_fn_18682, _1html_unexpected_exerr_row_format_19519, _11012);
        DeRefDS(_11012);
        _11012 = NOVALUE;

        /** 					printf(html_fn, html_unexpected_exerr_table_end, {} )			*/
        EPrintf(_1html_fn_18682, _1html_unexpected_exerr_table_end_19523, _5);
        goto L5; // [150] 415
L4: 

        /** 				elsif find(error_list[2][err], {No_valid_control_file_was_supplied, Unexpected_empty_control_file, No_valid_exerr_has_been_generated, Unexpected_empty_exerr}) then*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11013 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_11013);
        _11014 = (int)*(((s1_ptr)_2)->base + _err_19547);
        _11013 = NOVALUE;
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_1No_valid_control_file_was_supplied_19500);
        *((int *)(_2+4)) = _1No_valid_control_file_was_supplied_19500;
        RefDS(_1Unexpected_empty_control_file_19502);
        *((int *)(_2+8)) = _1Unexpected_empty_control_file_19502;
        RefDS(_1No_valid_exerr_has_been_generated_19504);
        *((int *)(_2+12)) = _1No_valid_exerr_has_been_generated_19504;
        RefDS(_1Unexpected_empty_exerr_19506);
        *((int *)(_2+16)) = _1Unexpected_empty_exerr_19506;
        _11015 = MAKE_SEQ(_1);
        _11016 = find_from(_11014, _11015, 1);
        _11014 = NOVALUE;
        DeRefDS(_11015);
        _11015 = NOVALUE;
        if (_11016 == 0)
        {
            _11016 = NOVALUE;
            goto L6; // [177] 251
        }
        else{
            _11016 = NOVALUE;
        }

        /** 					printf(html_fn, html_error_table_begin, {data[2], data[2]} )*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11017 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19542);
        _11018 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_11018);
        Ref(_11017);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _11017;
        ((int *)_2)[2] = _11018;
        _11019 = MAKE_SEQ(_1);
        _11018 = NOVALUE;
        _11017 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_error_table_begin_19495, _11019);
        DeRefDS(_11019);
        _11019 = NOVALUE;

        /** 					printf(html_fn, html_table_error_row, {color, "", error_list[2][err]} )*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11020 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_11020);
        _11021 = (int)*(((s1_ptr)_2)->base + _err_19547);
        _11020 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_color_19552);
        *((int *)(_2+4)) = _color_19552;
        RefDS(_5);
        *((int *)(_2+8)) = _5;
        Ref(_11021);
        *((int *)(_2+12)) = _11021;
        _11022 = MAKE_SEQ(_1);
        _11021 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_table_error_row_19526, _11022);
        DeRefDS(_11022);
        _11022 = NOVALUE;

        /** 					printf(html_fn, html_error_table_end, {} )*/
        EPrintf(_1html_fn_18682, _1html_error_table_end_19525, _5);

        /** 					unsummarized_files = append(unsummarized_files, data[2])*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11023 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_11023);
        Append(&_1unsummarized_files_19368, _1unsummarized_files_19368, _11023);
        _11023 = NOVALUE;
        goto L5; // [248] 415
L6: 

        /** 					printf(html_fn, html_table_head, { data[2], data[2] })*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11025 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19542);
        _11026 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_11026);
        Ref(_11025);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _11025;
        ((int *)_2)[2] = _11026;
        _11027 = MAKE_SEQ(_1);
        _11026 = NOVALUE;
        _11025 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_table_head_19491, _11027);
        DeRefDS(_11027);
        _11027 = NOVALUE;

        /** 					printf(html_fn, html_table_error_row, { color, "", error_list[2][err] })*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11028 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_11028);
        _11029 = (int)*(((s1_ptr)_2)->base + _err_19547);
        _11028 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_color_19552);
        *((int *)(_2+4)) = _color_19552;
        RefDS(_5);
        *((int *)(_2+8)) = _5;
        Ref(_11029);
        *((int *)(_2+12)) = _11029;
        _11030 = MAKE_SEQ(_1);
        _11029 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_table_error_row_19526, _11030);
        DeRefDS(_11030);
        _11030 = NOVALUE;

        /** 					puts(html_fn, html_table_headers )*/
        EPuts(_1html_fn_18682, _1html_table_headers_19493); // DJP 

        /** 					if sequence(error_list[4][err]) then*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11031 = (int)*(((s1_ptr)_2)->base + 4);
        _2 = (int)SEQ_PTR(_11031);
        _11032 = (int)*(((s1_ptr)_2)->base + _err_19547);
        _11031 = NOVALUE;
        _11033 = IS_SEQUENCE(_11032);
        _11032 = NOVALUE;
        if (_11033 == 0)
        {
            _11033 = NOVALUE;
            goto L7; // [321] 402
        }
        else{
            _11033 = NOVALUE;
        }

        /** 						puts(html_fn, html_table_error_content_begin)*/
        EPuts(_1html_fn_18682, _1html_table_error_content_begin_19528); // DJP 

        /** 						for i = 1 to length(error_list[4][err]) do*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11034 = (int)*(((s1_ptr)_2)->base + 4);
        _2 = (int)SEQ_PTR(_11034);
        _11035 = (int)*(((s1_ptr)_2)->base + _err_19547);
        _11034 = NOVALUE;
        if (IS_SEQUENCE(_11035)){
                _11036 = SEQ_PTR(_11035)->length;
        }
        else {
            _11036 = 1;
        }
        _11035 = NOVALUE;
        {
            int _i_19593;
            _i_19593 = 1;
L8: 
            if (_i_19593 > _11036){
                goto L9; // [348] 392
            }

            /** 							printf(html_fn,"%s\n", { text2html(error_list[4][err][i]) })*/
            _2 = (int)SEQ_PTR(_1error_list_18672);
            _11037 = (int)*(((s1_ptr)_2)->base + 4);
            _2 = (int)SEQ_PTR(_11037);
            _11038 = (int)*(((s1_ptr)_2)->base + _err_19547);
            _11037 = NOVALUE;
            _2 = (int)SEQ_PTR(_11038);
            _11039 = (int)*(((s1_ptr)_2)->base + _i_19593);
            _11038 = NOVALUE;
            Ref(_11039);
            _11040 = _1text2html(_11039);
            _11039 = NOVALUE;
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = _11040;
            _11041 = MAKE_SEQ(_1);
            _11040 = NOVALUE;
            EPrintf(_1html_fn_18682, _8888, _11041);
            DeRefDS(_11041);
            _11041 = NOVALUE;

            /** 						end for*/
            _i_19593 = _i_19593 + 1;
            goto L8; // [387] 355
L9: 
            ;
        }

        /** 						puts(html_fn, html_table_error_content_end)*/
        EPuts(_1html_fn_18682, _1html_table_error_content_end_19530); // DJP 
L7: 

        /** 					unsummarized_files = append(unsummarized_files, data[2])*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11042 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_11042);
        Append(&_1unsummarized_files_19368, _1unsummarized_files_19368, _11042);
        _11042 = NOVALUE;
L5: 
        DeRef(_color_19552);
        _color_19552 = NOVALUE;
        DeRef(_ex_err_19558);
        _ex_err_19558 = NOVALUE;
        goto LA; // [417] 455
L1: 

        /** 				printf(html_fn, html_table_head, { data[2], data[2] })*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11044 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19542);
        _11045 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_11045);
        Ref(_11044);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _11044;
        ((int *)_2)[2] = _11045;
        _11046 = MAKE_SEQ(_1);
        _11045 = NOVALUE;
        _11044 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_table_head_19491, _11046);
        DeRefDS(_11046);
        _11046 = NOVALUE;

        /** 				unsummarized_files = append(unsummarized_files, data[2])*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11047 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_11047);
        Append(&_1unsummarized_files_19368, _1unsummarized_files_19368, _11047);
        _11047 = NOVALUE;
LA: 
        goto LB; // [457] 636

        /** 		case "failed" then*/
        case 2:

        /** 			printf(html_fn, html_table_failed_row, {*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11049 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19542);
        _11050 = (int)*(((s1_ptr)_2)->base + 5);
        Ref(_11050);
        _11051 = _16sprint(_11050);
        _11050 = NOVALUE;
        _2 = (int)SEQ_PTR(_data_19542);
        _11052 = (int)*(((s1_ptr)_2)->base + 3);
        Ref(_11052);
        _11053 = _16sprint(_11052);
        _11052 = NOVALUE;
        _2 = (int)SEQ_PTR(_data_19542);
        _11054 = (int)*(((s1_ptr)_2)->base + 4);
        Ref(_11054);
        _11055 = _16sprint(_11054);
        _11054 = NOVALUE;
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_11049);
        *((int *)(_2+4)) = _11049;
        *((int *)(_2+8)) = _11051;
        *((int *)(_2+12)) = _11053;
        *((int *)(_2+16)) = _11055;
        _11056 = MAKE_SEQ(_1);
        _11055 = NOVALUE;
        _11053 = NOVALUE;
        _11051 = NOVALUE;
        _11049 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_table_failed_row_19532, _11056);
        DeRefDS(_11056);
        _11056 = NOVALUE;
        goto LB; // [508] 636

        /** 		case "passed" then*/
        case 3:

        /** 			sequence anum*/

        /** 			if data[3] = 0 then*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11057 = (int)*(((s1_ptr)_2)->base + 3);
        if (binary_op_a(NOTEQ, _11057, 0)){
            _11057 = NOVALUE;
            goto LC; // [522] 536
        }
        _11057 = NOVALUE;

        /** 				anum = "0"*/
        RefDS(_8600);
        DeRefi(_anum_19623);
        _anum_19623 = _8600;
        goto LD; // [533] 547
LC: 

        /** 				anum = sprintf("%f", data[3])*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11059 = (int)*(((s1_ptr)_2)->base + 3);
        DeRefi(_anum_19623);
        _anum_19623 = EPrintf(-9999999, _10903, _11059);
        _11059 = NOVALUE;
LD: 

        /** 			printf(html_fn, html_table_passed_row, { data[2], anum })*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11061 = (int)*(((s1_ptr)_2)->base + 2);
        RefDS(_anum_19623);
        Ref(_11061);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _11061;
        ((int *)_2)[2] = _anum_19623;
        _11062 = MAKE_SEQ(_1);
        _11061 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_table_passed_row_19534, _11062);
        DeRefDS(_11062);
        _11062 = NOVALUE;
        DeRefDSi(_anum_19623);
        _anum_19623 = NOVALUE;
        goto LB; // [569] 636

        /** 		case "summary" then*/
        case 4:

        /** 			if length(unsummarized_files) then*/
        if (IS_SEQUENCE(_1unsummarized_files_19368)){
                _11063 = SEQ_PTR(_1unsummarized_files_19368)->length;
        }
        else {
            _11063 = 1;
        }
        if (_11063 == 0)
        {
            _11063 = NOVALUE;
            goto LE; // [582] 602
        }
        else{
            _11063 = NOVALUE;
        }

        /** 				unsummarized_files = unsummarized_files[1..$-1] */
        if (IS_SEQUENCE(_1unsummarized_files_19368)){
                _11064 = SEQ_PTR(_1unsummarized_files_19368)->length;
        }
        else {
            _11064 = 1;
        }
        _11065 = _11064 - 1;
        _11064 = NOVALUE;
        rhs_slice_target = (object_ptr)&_1unsummarized_files_19368;
        RHS_Slice(_1unsummarized_files_19368, 1, _11065);
LE: 

        /** 			printf(html_fn, html_table_summary, {*/
        _2 = (int)SEQ_PTR(_data_19542);
        _11067 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_data_19542);
        _11068 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_data_19542);
        _11069 = (int)*(((s1_ptr)_2)->base + 4);
        _2 = (int)SEQ_PTR(_data_19542);
        _11070 = (int)*(((s1_ptr)_2)->base + 5);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_11067);
        *((int *)(_2+4)) = _11067;
        Ref(_11068);
        *((int *)(_2+8)) = _11068;
        Ref(_11069);
        *((int *)(_2+12)) = _11069;
        Ref(_11070);
        *((int *)(_2+16)) = _11070;
        _11071 = MAKE_SEQ(_1);
        _11070 = NOVALUE;
        _11069 = NOVALUE;
        _11068 = NOVALUE;
        _11067 = NOVALUE;
        EPrintf(_1html_fn_18682, _1html_table_summary_19536, _11071);
        DeRefDS(_11071);
        _11071 = NOVALUE;
    ;}LB: 

    /** end procedure*/
    DeRefDS(_data_19542);
    _11035 = NOVALUE;
    DeRef(_11065);
    _11065 = NOVALUE;
    return;
    ;
}


void _1summarize_error(int _message_19645, int _e_19646, int _html_19647)
{
    int _11098 = NOVALUE;
    int _11097 = NOVALUE;
    int _11096 = NOVALUE;
    int _11094 = NOVALUE;
    int _11093 = NOVALUE;
    int _11092 = NOVALUE;
    int _11089 = NOVALUE;
    int _11088 = NOVALUE;
    int _11087 = NOVALUE;
    int _11086 = NOVALUE;
    int _11085 = NOVALUE;
    int _11084 = NOVALUE;
    int _11083 = NOVALUE;
    int _11082 = NOVALUE;
    int _11081 = NOVALUE;
    int _11079 = NOVALUE;
    int _11078 = NOVALUE;
    int _11077 = NOVALUE;
    int _11076 = NOVALUE;
    int _11075 = NOVALUE;
    int _11073 = NOVALUE;
    int _11072 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_html_19647)) {
        _1 = (long)(DBL_PTR(_html_19647)->dbl);
        if (UNIQUE(DBL_PTR(_html_19647)) && (DBL_PTR(_html_19647)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_html_19647);
        _html_19647 = _1;
    }

    /** 	if find(e, error_list[3]) then*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _11072 = (int)*(((s1_ptr)_2)->base + 3);
    _11073 = find_from(_e_19646, _11072, 1);
    _11072 = NOVALUE;
    if (_11073 == 0)
    {
        _11073 = NOVALUE;
        goto L1; // [20] 202
    }
    else{
        _11073 = NOVALUE;
    }

    /** 		if html then*/
    if (_html_19647 == 0)
    {
        goto L2; // [25] 61
    }
    else{
    }

    /** 			printf(html_fn,message & "<br>\nThese were:\n", {sum(error_list[3] = e)})*/
    Concat((object_ptr)&_11075, _message_19645, _11074);
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _11076 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_11076) && IS_ATOM_INT(_e_19646)) {
        _11077 = (_11076 == _e_19646);
    }
    else {
        _11077 = binary_op(EQUALS, _11076, _e_19646);
    }
    _11076 = NOVALUE;
    _11078 = _18sum(_11077);
    _11077 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _11078;
    _11079 = MAKE_SEQ(_1);
    _11078 = NOVALUE;
    EPrintf(_1html_fn_18682, _11075, _11079);
    DeRefDS(_11075);
    _11075 = NOVALUE;
    DeRefDS(_11079);
    _11079 = NOVALUE;
    goto L3; // [58] 90
L2: 

    /** 			printf(1,message & "\nThese were:\n", {sum(error_list[3] = e)})*/
    Concat((object_ptr)&_11081, _message_19645, _11080);
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _11082 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_11082) && IS_ATOM_INT(_e_19646)) {
        _11083 = (_11082 == _e_19646);
    }
    else {
        _11083 = binary_op(EQUALS, _11082, _e_19646);
    }
    _11082 = NOVALUE;
    _11084 = _18sum(_11083);
    _11083 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _11084;
    _11085 = MAKE_SEQ(_1);
    _11084 = NOVALUE;
    EPrintf(1, _11081, _11085);
    DeRefDS(_11081);
    _11081 = NOVALUE;
    DeRefDS(_11085);
    _11085 = NOVALUE;
L3: 

    /** 		for i = 1 to length(error_list[1]) do*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _11086 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_11086)){
            _11087 = SEQ_PTR(_11086)->length;
    }
    else {
        _11087 = 1;
    }
    _11086 = NOVALUE;
    {
        int _i_19668;
        _i_19668 = 1;
L4: 
        if (_i_19668 > _11087){
            goto L5; // [101] 183
        }

        /** 			if error_list[3][i] = e then*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11088 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_11088);
        _11089 = (int)*(((s1_ptr)_2)->base + _i_19668);
        _11088 = NOVALUE;
        if (binary_op_a(NOTEQ, _11089, _e_19646)){
            _11089 = NOVALUE;
            goto L6; // [120] 176
        }
        _11089 = NOVALUE;

        /** 				if html then*/
        if (_html_19647 == 0)
        {
            goto L7; // [126] 154
        }
        else{
        }

        /** 					printf(html_fn, "<a href='#%s'>%s</a>, ", repeat(error_list[1][i],2))*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11092 = (int)*(((s1_ptr)_2)->base + 1);
        _2 = (int)SEQ_PTR(_11092);
        _11093 = (int)*(((s1_ptr)_2)->base + _i_19668);
        _11092 = NOVALUE;
        _11094 = Repeat(_11093, 2);
        _11093 = NOVALUE;
        EPrintf(_1html_fn_18682, _11091, _11094);
        DeRefDS(_11094);
        _11094 = NOVALUE;
        goto L8; // [151] 175
L7: 

        /** 					printf(1, "%s, ", repeat(error_list[1][i],1))*/
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11096 = (int)*(((s1_ptr)_2)->base + 1);
        _2 = (int)SEQ_PTR(_11096);
        _11097 = (int)*(((s1_ptr)_2)->base + _i_19668);
        _11096 = NOVALUE;
        _11098 = Repeat(_11097, 1);
        _11097 = NOVALUE;
        EPrintf(1, _11095, _11098);
        DeRefDS(_11098);
        _11098 = NOVALUE;
L8: 
L6: 

        /** 		end for*/
        _i_19668 = _i_19668 + 1;
        goto L4; // [178] 108
L5: 
        ;
    }

    /** 		if html then*/
    if (_html_19647 == 0)
    {
        goto L9; // [185] 196
    }
    else{
    }

    /** 			puts(html_fn, "<p>")*/
    EPuts(_1html_fn_18682, _11099); // DJP 
L9: 

    /** 		puts(1, "\n")*/
    EPuts(1, _3957); // DJP 
L1: 

    /** end procedure*/
    DeRefDS(_message_19645);
    DeRef(_e_19646);
    _11086 = NOVALUE;
    return;
    ;
}


void _1do_process_log(int _cmds_19689, int _html_19690)
{
    int _other_files_19691 = NOVALUE;
    int _total_failed_19692 = NOVALUE;
    int _total_passed_19693 = NOVALUE;
    int _out_r_19694 = NOVALUE;
    int _total_time_19695 = NOVALUE;
    int _ctc_19696 = NOVALUE;
    int _messages_19697 = NOVALUE;
    int _get_inlined_get_at_98_19711 = NOVALUE;
    int _get_inlined_get_at_142_19718 = NOVALUE;
    int _content_19728 = NOVALUE;
    int _data_19746 = NOVALUE;
    int _ofi_19772 = NOVALUE;
    int _11199 = NOVALUE;
    int _11198 = NOVALUE;
    int _11197 = NOVALUE;
    int _11196 = NOVALUE;
    int _11194 = NOVALUE;
    int _11193 = NOVALUE;
    int _11191 = NOVALUE;
    int _11190 = NOVALUE;
    int _11189 = NOVALUE;
    int _11188 = NOVALUE;
    int _11187 = NOVALUE;
    int _11186 = NOVALUE;
    int _11185 = NOVALUE;
    int _11183 = NOVALUE;
    int _11182 = NOVALUE;
    int _11181 = NOVALUE;
    int _11173 = NOVALUE;
    int _11172 = NOVALUE;
    int _11171 = NOVALUE;
    int _11170 = NOVALUE;
    int _11169 = NOVALUE;
    int _11168 = NOVALUE;
    int _11167 = NOVALUE;
    int _11166 = NOVALUE;
    int _11165 = NOVALUE;
    int _11164 = NOVALUE;
    int _11163 = NOVALUE;
    int _11162 = NOVALUE;
    int _11161 = NOVALUE;
    int _11160 = NOVALUE;
    int _11159 = NOVALUE;
    int _11158 = NOVALUE;
    int _11157 = NOVALUE;
    int _11156 = NOVALUE;
    int _11155 = NOVALUE;
    int _11154 = NOVALUE;
    int _11153 = NOVALUE;
    int _11152 = NOVALUE;
    int _11150 = NOVALUE;
    int _11149 = NOVALUE;
    int _11148 = NOVALUE;
    int _11147 = NOVALUE;
    int _11146 = NOVALUE;
    int _11143 = NOVALUE;
    int _11141 = NOVALUE;
    int _11138 = NOVALUE;
    int _11134 = NOVALUE;
    int _11132 = NOVALUE;
    int _11128 = NOVALUE;
    int _11126 = NOVALUE;
    int _11125 = NOVALUE;
    int _11124 = NOVALUE;
    int _11123 = NOVALUE;
    int _11122 = NOVALUE;
    int _11121 = NOVALUE;
    int _11120 = NOVALUE;
    int _11116 = NOVALUE;
    int _11111 = NOVALUE;
    int _11108 = NOVALUE;
    int _11102 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_html_19690)) {
        _1 = (long)(DBL_PTR(_html_19690)->dbl);
        if (UNIQUE(DBL_PTR(_html_19690)) && (DBL_PTR(_html_19690)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_html_19690);
        _html_19690 = _1;
    }

    /** 	object other_files = {}*/
    RefDS(_5);
    DeRef(_other_files_19691);
    _other_files_19691 = _5;

    /** 	integer total_failed=0, total_passed=0*/
    _total_failed_19692 = 0;
    _total_passed_19693 = 0;

    /** 	atom total_time = 0*/
    DeRef(_total_time_19695);
    _total_time_19695 = 0;

    /** 	if html then*/
    if (_html_19690 == 0)
    {
        goto L1; // [31] 66
    }
    else{
    }

    /** 		out_r = routine_id("html_out")*/
    _out_r_19694 = CRoutineId(602, 1, _11100);

    /** 		if sequence( html_fn ) then*/
    _11102 = IS_SEQUENCE(_1html_fn_18682);
    if (_11102 == 0)
    {
        _11102 = NOVALUE;
        goto L2; // [50] 76
    }
    else{
        _11102 = NOVALUE;
    }

    /** 			html_fn = open( html_fn, "w" )*/
    _0 = _1html_fn_18682;
    _1html_fn_18682 = EOpen(_1html_fn_18682, _4110, 0);
    DeRef(_0);
    goto L2; // [63] 76
L1: 

    /** 		out_r = routine_id("ascii_out")*/
    _out_r_19694 = CRoutineId(602, 1, _11104);
L2: 

    /** 	ctcfh = open("ctc.log","r")*/
    _1ctcfh_18671 = EOpen(_10821, _4069, 0);

    /** 	if ctcfh != -1 then*/
    if (_1ctcfh_18671 == -1)
    goto L3; // [89] 192

    /** 		ctc = stdget:get(ctcfh)*/
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_8123)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_8123)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_8123)) && (DBL_PTR(_14GET_SHORT_ANSWER_8123)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_8123);
        _14GET_SHORT_ANSWER_8123 = _1;
    }
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_8123)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_8123)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_8123)) && (DBL_PTR(_14GET_SHORT_ANSWER_8123)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_8123);
        _14GET_SHORT_ANSWER_8123 = _1;
    }

    /** 	return get_value(file, offset, answer)*/
    _0 = _ctc_19696;
    _ctc_19696 = _14get_value(_1ctcfh_18671, 0, _14GET_SHORT_ANSWER_8123);
    DeRef(_0);

    /** 		if ctc[1] = GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_ctc_19696);
    _11108 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _11108, 0)){
        _11108 = NOVALUE;
        goto L4; // [120] 177
    }
    _11108 = NOVALUE;

    /** 			ctc = ctc[2]*/
    _0 = _ctc_19696;
    _2 = (int)SEQ_PTR(_ctc_19696);
    _ctc_19696 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_ctc_19696);
    DeRef(_0);

    /** 			error_list = ctc*/
    Ref(_ctc_19696);
    DeRef(_1error_list_18672);
    _1error_list_18672 = _ctc_19696;

    /** 			ctc = stdget:get(ctcfh)*/
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_8123)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_8123)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_8123)) && (DBL_PTR(_14GET_SHORT_ANSWER_8123)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_8123);
        _14GET_SHORT_ANSWER_8123 = _1;
    }
    if (!IS_ATOM_INT(_14GET_SHORT_ANSWER_8123)) {
        _1 = (long)(DBL_PTR(_14GET_SHORT_ANSWER_8123)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_SHORT_ANSWER_8123)) && (DBL_PTR(_14GET_SHORT_ANSWER_8123)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_SHORT_ANSWER_8123);
        _14GET_SHORT_ANSWER_8123 = _1;
    }

    /** 	return get_value(file, offset, answer)*/
    _0 = _ctc_19696;
    _ctc_19696 = _14get_value(_1ctcfh_18671, 0, _14GET_SHORT_ANSWER_8123);
    DeRef(_0);

    /** 			if ctc[1] != GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_ctc_19696);
    _11111 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _11111, 0)){
        _11111 = NOVALUE;
        goto L5; // [164] 183
    }
    _11111 = NOVALUE;

    /** 				ctc = 0*/
    DeRef(_ctc_19696);
    _ctc_19696 = 0;
    goto L5; // [174] 183
L4: 

    /** 			ctc = 0*/
    DeRef(_ctc_19696);
    _ctc_19696 = 0;
L5: 

    /** 		close(ctcfh)*/
    EClose(_1ctcfh_18671);
    goto L6; // [189] 205
L3: 

    /** 		ctc = 0*/
    DeRef(_ctc_19696);
    _ctc_19696 = 0;

    /** 		ctcfh = 0*/
    _1ctcfh_18671 = 0;
L6: 

    /** 	other_files = error_list[1]*/
    DeRef(_other_files_19691);
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _other_files_19691 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_other_files_19691);

    /** 	if html then*/
    if (_html_19690 == 0)
    {
        goto L7; // [215] 226
    }
    else{
    }

    /** 		puts(html_fn, "<html><body>\n")*/
    EPuts(_1html_fn_18682, _11114); // DJP 
L7: 

    /** 	object content = read_file("unittest.log")*/
    RefDS(_10554);
    _0 = _content_19728;
    _content_19728 = _15read_file(_10554, 1);
    DeRef(_0);

    /** 	if atom(content) then*/
    _11116 = IS_ATOM(_content_19728);
    if (_11116 == 0)
    {
        _11116 = NOVALUE;
        goto L8; // [240] 251
    }
    else{
        _11116 = NOVALUE;
    }

    /** 		puts(1, "unittest.log could not be read\n")*/
    EPuts(1, _11117); // DJP 
    goto L9; // [248] 562
L8: 

    /** 		messages = stdseq:split( content,"entry = ")*/
    Ref(_content_19728);
    RefDS(_11118);
    _0 = _messages_19697;
    _messages_19697 = _3split(_content_19728, _11118, 0, 0);
    DeRef(_0);

    /** 		for a = 1 to length(messages) do*/
    if (IS_SEQUENCE(_messages_19697)){
            _11120 = SEQ_PTR(_messages_19697)->length;
    }
    else {
        _11120 = 1;
    }
    {
        int _a_19738;
        _a_19738 = 1;
LA: 
        if (_a_19738 > _11120){
            goto LB; // [267] 561
        }

        /** 			if sequence(messages[a]) and equal(messages[a], "") then*/
        _2 = (int)SEQ_PTR(_messages_19697);
        _11121 = (int)*(((s1_ptr)_2)->base + _a_19738);
        _11122 = IS_SEQUENCE(_11121);
        _11121 = NOVALUE;
        if (_11122 == 0) {
            goto LC; // [283] 304
        }
        _2 = (int)SEQ_PTR(_messages_19697);
        _11124 = (int)*(((s1_ptr)_2)->base + _a_19738);
        if (_11124 == _5)
        _11125 = 1;
        else if (IS_ATOM_INT(_11124) && IS_ATOM_INT(_5))
        _11125 = 0;
        else
        _11125 = (compare(_11124, _5) == 0);
        _11124 = NOVALUE;
        if (_11125 == 0)
        {
            _11125 = NOVALUE;
            goto LC; // [296] 304
        }
        else{
            _11125 = NOVALUE;
        }

        /** 				continue*/
        goto LD; // [301] 556
LC: 

        /** 			sequence data = value(messages[a])*/
        _2 = (int)SEQ_PTR(_messages_19697);
        _11126 = (int)*(((s1_ptr)_2)->base + _a_19738);
        Ref(_11126);
        _0 = _data_19746;
        _data_19746 = _14value(_11126, 1, _14GET_SHORT_ANSWER_8123);
        DeRef(_0);
        _11126 = NOVALUE;

        /** 			if data[1] = GET_SUCCESS then*/
        _2 = (int)SEQ_PTR(_data_19746);
        _11128 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(NOTEQ, _11128, 0)){
            _11128 = NOVALUE;
            goto LE; // [328] 343
        }
        _11128 = NOVALUE;

        /** 				data = data[2]*/
        _0 = _data_19746;
        _2 = (int)SEQ_PTR(_data_19746);
        _data_19746 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_data_19746);
        DeRefDS(_0);
        goto LF; // [340] 364
LE: 

        /** 				puts(1, "unittest.log could not parse:\n")*/
        EPuts(1, _11131); // DJP 

        /** 				pretty_print(1, messages[a], {3})*/
        _2 = (int)SEQ_PTR(_messages_19697);
        _11132 = (int)*(((s1_ptr)_2)->base + _a_19738);
        Ref(_11132);
        RefDS(_11133);
        _2pretty_print(1, _11132, _11133);
        _11132 = NOVALUE;

        /** 				abort(1)*/
        UserCleanup(1);
LF: 

        /** 			switch data[1] do*/
        _2 = (int)SEQ_PTR(_data_19746);
        _11134 = (int)*(((s1_ptr)_2)->base + 1);
        _1 = find(_11134, _11135);
        _11134 = NOVALUE;
        switch ( _1 ){ 

            /** 				case "failed" then*/
            case 1:

            /** 					total_failed += 1*/
            _total_failed_19692 = _total_failed_19692 + 1;

            /** 					total_time += data[5]*/
            _2 = (int)SEQ_PTR(_data_19746);
            _11138 = (int)*(((s1_ptr)_2)->base + 5);
            _0 = _total_time_19695;
            if (IS_ATOM_INT(_total_time_19695) && IS_ATOM_INT(_11138)) {
                _total_time_19695 = _total_time_19695 + _11138;
                if ((long)((unsigned long)_total_time_19695 + (unsigned long)HIGH_BITS) >= 0) 
                _total_time_19695 = NewDouble((double)_total_time_19695);
            }
            else {
                _total_time_19695 = binary_op(PLUS, _total_time_19695, _11138);
            }
            DeRef(_0);
            _11138 = NOVALUE;
            goto L10; // [397] 539

            /** 				case "passed" then*/
            case 2:

            /** 					total_passed += 1*/
            _total_passed_19693 = _total_passed_19693 + 1;

            /** 					total_time += data[3]*/
            _2 = (int)SEQ_PTR(_data_19746);
            _11141 = (int)*(((s1_ptr)_2)->base + 3);
            _0 = _total_time_19695;
            if (IS_ATOM_INT(_total_time_19695) && IS_ATOM_INT(_11141)) {
                _total_time_19695 = _total_time_19695 + _11141;
                if ((long)((unsigned long)_total_time_19695 + (unsigned long)HIGH_BITS) >= 0) 
                _total_time_19695 = NewDouble((double)_total_time_19695);
            }
            else {
                _total_time_19695 = binary_op(PLUS, _total_time_19695, _11141);
            }
            DeRef(_0);
            _11141 = NOVALUE;
            goto L10; // [421] 539

            /** 				case "file" then*/
            case 3:

            /** 					integer ofi = find(data[2], other_files)*/
            _2 = (int)SEQ_PTR(_data_19746);
            _11143 = (int)*(((s1_ptr)_2)->base + 2);
            _ofi_19772 = find_from(_11143, _other_files_19691, 1);
            _11143 = NOVALUE;

            /** 					if ofi != 0 then*/
            if (_ofi_19772 == 0)
            goto L11; // [442] 474

            /** 						other_files = other_files[1..ofi-1] & other_files[ofi+1..$]*/
            _11146 = _ofi_19772 - 1;
            rhs_slice_target = (object_ptr)&_11147;
            RHS_Slice(_other_files_19691, 1, _11146);
            _11148 = _ofi_19772 + 1;
            if (IS_SEQUENCE(_other_files_19691)){
                    _11149 = SEQ_PTR(_other_files_19691)->length;
            }
            else {
                _11149 = 1;
            }
            rhs_slice_target = (object_ptr)&_11150;
            RHS_Slice(_other_files_19691, _11148, _11149);
            Concat((object_ptr)&_other_files_19691, _11147, _11150);
            DeRefDS(_11147);
            _11147 = NOVALUE;
            DeRef(_11147);
            _11147 = NOVALUE;
            DeRefDS(_11150);
            _11150 = NOVALUE;
L11: 

            /** 					while length(unsummarized_files)>=1 and compare(data[2],unsummarized_files[1])!=0 do*/
L12: 
            if (IS_SEQUENCE(_1unsummarized_files_19368)){
                    _11152 = SEQ_PTR(_1unsummarized_files_19368)->length;
            }
            else {
                _11152 = 1;
            }
            _11153 = (_11152 >= 1);
            _11152 = NOVALUE;
            if (_11153 == 0) {
                goto L13; // [488] 538
            }
            _2 = (int)SEQ_PTR(_data_19746);
            _11155 = (int)*(((s1_ptr)_2)->base + 2);
            _2 = (int)SEQ_PTR(_1unsummarized_files_19368);
            _11156 = (int)*(((s1_ptr)_2)->base + 1);
            if (IS_ATOM_INT(_11155) && IS_ATOM_INT(_11156)){
                _11157 = (_11155 < _11156) ? -1 : (_11155 > _11156);
            }
            else{
                _11157 = compare(_11155, _11156);
            }
            _11155 = NOVALUE;
            _11156 = NOVALUE;
            _11158 = (_11157 != 0);
            _11157 = NOVALUE;
            if (_11158 == 0)
            {
                DeRef(_11158);
                _11158 = NOVALUE;
                goto L13; // [511] 538
            }
            else{
                DeRef(_11158);
                _11158 = NOVALUE;
            }

            /** 						call_proc(out_r, {{"summary",0,0,0,0}})*/
            _1 = NewS1(5);
            _2 = (int)((s1_ptr)_1)->base;
            RefDS(_10909);
            *((int *)(_2+4)) = _10909;
            *((int *)(_2+8)) = 0;
            *((int *)(_2+12)) = 0;
            *((int *)(_2+16)) = 0;
            *((int *)(_2+20)) = 0;
            _11159 = MAKE_SEQ(_1);
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = _11159;
            _11160 = MAKE_SEQ(_1);
            _11159 = NOVALUE;
            _1 = (int)SEQ_PTR(_11160);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_out_r_19694].addr;
            Ref(*(int *)(_2+4));
            (*(int (*)())_0)(
                                *(int *)(_2+4)
                                 );
            DeRefDS(_11160);
            _11160 = NOVALUE;

            /** 					end while*/
            goto L12; // [535] 479
L13: 
        ;}L10: 

        /** 			call_proc(out_r, {data})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_data_19746);
        *((int *)(_2+4)) = _data_19746;
        _11161 = MAKE_SEQ(_1);
        _1 = (int)SEQ_PTR(_11161);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_out_r_19694].addr;
        Ref(*(int *)(_2+4));
        (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRefDS(_11161);
        _11161 = NOVALUE;
        DeRefDS(_data_19746);
        _data_19746 = NOVALUE;

        /** 		end for*/
LD: 
        _a_19738 = _a_19738 + 1;
        goto LA; // [556] 274
LB: 
        ;
    }
L9: 

    /** 	while length(unsummarized_files) do*/
L14: 
    if (IS_SEQUENCE(_1unsummarized_files_19368)){
            _11162 = SEQ_PTR(_1unsummarized_files_19368)->length;
    }
    else {
        _11162 = 1;
    }
    if (_11162 == 0)
    {
        _11162 = NOVALUE;
        goto L15; // [572] 599
    }
    else{
        _11162 = NOVALUE;
    }

    /** 		call_proc(out_r, {{"summary",0,0,0,0}})*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_10909);
    *((int *)(_2+4)) = _10909;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    _11163 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _11163;
    _11164 = MAKE_SEQ(_1);
    _11163 = NOVALUE;
    _1 = (int)SEQ_PTR(_11164);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_out_r_19694].addr;
    Ref(*(int *)(_2+4));
    (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRefDS(_11164);
    _11164 = NOVALUE;

    /** 	end while*/
    goto L14; // [596] 567
L15: 

    /** 	for i = 1 to length(other_files) do*/
    if (IS_SEQUENCE(_other_files_19691)){
            _11165 = SEQ_PTR(_other_files_19691)->length;
    }
    else {
        _11165 = 1;
    }
    {
        int _i_19799;
        _i_19799 = 1;
L16: 
        if (_i_19799 > _11165){
            goto L17; // [604] 675
        }

        /** 		if find(other_files[i], error_list[1]) then*/
        _2 = (int)SEQ_PTR(_other_files_19691);
        _11166 = (int)*(((s1_ptr)_2)->base + _i_19799);
        _2 = (int)SEQ_PTR(_1error_list_18672);
        _11167 = (int)*(((s1_ptr)_2)->base + 1);
        _11168 = find_from(_11166, _11167, 1);
        _11166 = NOVALUE;
        _11167 = NOVALUE;
        if (_11168 == 0)
        {
            _11168 = NOVALUE;
            goto L18; // [628] 668
        }
        else{
            _11168 = NOVALUE;
        }

        /** 			call_proc(out_r, {{"file",other_files[i]}})*/
        _2 = (int)SEQ_PTR(_other_files_19691);
        _11169 = (int)*(((s1_ptr)_2)->base + _i_19799);
        Ref(_11169);
        RefDS(_10473);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _10473;
        ((int *)_2)[2] = _11169;
        _11170 = MAKE_SEQ(_1);
        _11169 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _11170;
        _11171 = MAKE_SEQ(_1);
        _11170 = NOVALUE;
        _1 = (int)SEQ_PTR(_11171);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_out_r_19694].addr;
        Ref(*(int *)(_2+4));
        (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRefDS(_11171);
        _11171 = NOVALUE;

        /** 			call_proc(out_r, {{"summary",0,0,0,0}})*/
        _1 = NewS1(5);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_10909);
        *((int *)(_2+4)) = _10909;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 0;
        *((int *)(_2+16)) = 0;
        *((int *)(_2+20)) = 0;
        _11172 = MAKE_SEQ(_1);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _11172;
        _11173 = MAKE_SEQ(_1);
        _11172 = NOVALUE;
        _1 = (int)SEQ_PTR(_11173);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_out_r_19694].addr;
        Ref(*(int *)(_2+4));
        (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRefDS(_11173);
        _11173 = NOVALUE;
L18: 

        /** 	end for*/
        _i_19799 = _i_19799 + 1;
        goto L16; // [670] 611
L17: 
        ;
    }

    /** 	summarize_error("Interpreted test files failed unexpectedly.: %d", E_INTERPRET, html)*/
    RefDS(_11174);
    _1summarize_error(_11174, 2, _html_19690);

    /** 	summarize_error("Test files could not be translated.........: %d", E_TRANSLATE, html)*/
    RefDS(_11175);
    _1summarize_error(_11175, 3, _html_19690);

    /** 	summarize_error("Translated test files could not be compiled: %d", E_COMPILE, html)*/
    RefDS(_11176);
    _1summarize_error(_11176, 4, _html_19690);

    /** 	summarize_error("Compiled test files failed unexpectedly....: %d", E_EXECUTE, html)*/
    RefDS(_11177);
    _1summarize_error(_11177, 5, _html_19690);

    /** 	summarize_error("Test files could not be bound..............: %d", E_BIND, html )*/
    RefDS(_11178);
    _1summarize_error(_11178, 6, _html_19690);

    /** 	summarize_error("Bound test files failed unexpectedly.......: %d", E_BOUND, html )*/
    RefDS(_11179);
    _1summarize_error(_11179, 7, _html_19690);

    /** 	summarize_error("Test files run successfully................: %d", E_NOERROR, html)*/
    RefDS(_11180);
    _1summarize_error(_11180, 1, _html_19690);

    /** 	if html then*/
    if (_html_19690 == 0)
    {
        goto L19; // [740] 838
    }
    else{
    }

    /** 		if find(1, error_list[3] = E_EUTEST) then*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _11181 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_11181)) {
        _11182 = (_11181 == 8);
    }
    else {
        _11182 = binary_op(EQUALS, _11181, 8);
    }
    _11181 = NOVALUE;
    _11183 = find_from(1, _11182, 1);
    DeRef(_11182);
    _11182 = NOVALUE;
    if (_11183 == 0)
    {
        _11183 = NOVALUE;
        goto L1A; // [762] 799
    }
    else{
        _11183 = NOVALUE;
    }

    /** 			printf(1, "There was an internal error to the testing system involving %s<br>",*/
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _11185 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_1error_list_18672);
    _11186 = (int)*(((s1_ptr)_2)->base + 3);
    _11187 = find_from(8, _11186, 1);
    _11186 = NOVALUE;
    _2 = (int)SEQ_PTR(_11185);
    _11188 = (int)*(((s1_ptr)_2)->base + _11187);
    _11185 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_11188);
    *((int *)(_2+4)) = _11188;
    _11189 = MAKE_SEQ(_1);
    _11188 = NOVALUE;
    EPrintf(1, _11184, _11189);
    DeRefDS(_11189);
    _11189 = NOVALUE;
L1A: 

    /** 		printf(html_fn, html_table_final_summary, {*/
    _11190 = _total_passed_19693 + _total_failed_19692;
    if ((long)((unsigned long)_11190 + (unsigned long)HIGH_BITS) >= 0) 
    _11190 = NewDouble((double)_11190);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _11190;
    *((int *)(_2+8)) = _total_failed_19692;
    *((int *)(_2+12)) = _total_passed_19693;
    Ref(_total_time_19695);
    *((int *)(_2+16)) = _total_time_19695;
    _11191 = MAKE_SEQ(_1);
    _11190 = NOVALUE;
    EPrintf(_1html_fn_18682, _1html_table_final_summary_19538, _11191);
    DeRefDS(_11191);
    _11191 = NOVALUE;

    /** 		if html_fn != 1 then*/
    if (binary_op_a(EQUALS, _1html_fn_18682, 1)){
        goto L1B; // [824] 882
    }

    /** 			close( html_fn )*/
    if (IS_ATOM_INT(_1html_fn_18682))
    EClose(_1html_fn_18682);
    else
    EClose((int)DBL_PTR(_1html_fn_18682)->dbl);
    goto L1B; // [835] 882
L19: 

    /** 		puts(1, repeat('*', 76) & "\n\n")*/
    _11193 = Repeat(42, 76);
    Concat((object_ptr)&_11194, _11193, _9148);
    DeRefDS(_11193);
    _11193 = NOVALUE;
    DeRef(_11193);
    _11193 = NOVALUE;
    EPuts(1, _11194); // DJP 
    DeRefDS(_11194);
    _11194 = NOVALUE;

    /** 		printf(1, "Overall: Total Tests: %04d  Failed: %04d  Passed: %04d Time: %f\n\n", {*/
    _11196 = _total_passed_19693 + _total_failed_19692;
    if ((long)((unsigned long)_11196 + (unsigned long)HIGH_BITS) >= 0) 
    _11196 = NewDouble((double)_11196);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _11196;
    *((int *)(_2+8)) = _total_failed_19692;
    *((int *)(_2+12)) = _total_passed_19693;
    Ref(_total_time_19695);
    *((int *)(_2+16)) = _total_time_19695;
    _11197 = MAKE_SEQ(_1);
    _11196 = NOVALUE;
    EPrintf(1, _11195, _11197);
    DeRefDS(_11197);
    _11197 = NOVALUE;

    /** 		puts(1, repeat('*', 76) & "\n\n")*/
    _11198 = Repeat(42, 76);
    Concat((object_ptr)&_11199, _11198, _9148);
    DeRefDS(_11198);
    _11198 = NOVALUE;
    DeRef(_11198);
    _11198 = NOVALUE;
    EPuts(1, _11199); // DJP 
    DeRefDS(_11199);
    _11199 = NOVALUE;
L1B: 

    /** end procedure*/
    DeRef(_other_files_19691);
    DeRef(_total_time_19695);
    DeRef(_ctc_19696);
    DeRef(_messages_19697);
    DeRef(_content_19728);
    DeRef(_11146);
    _11146 = NOVALUE;
    DeRef(_11153);
    _11153 = NOVALUE;
    DeRef(_11148);
    _11148 = NOVALUE;
    return;
    ;
}


void _1platform_init()
{
    int _11205 = NOVALUE;
    int _11202 = NOVALUE;
    int _11200 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef UNIX then*/

    /** 		if equal( executable, "" ) then*/
    if (_1executable_18681 == _5)
    _11200 = 1;
    else if (IS_ATOM_INT(_1executable_18681) && IS_ATOM_INT(_5))
    _11200 = 0;
    else
    _11200 = (compare(_1executable_18681, _5) == 0);
    if (_11200 == 0)
    {
        _11200 = NOVALUE;
        goto L1; // [11] 22
    }
    else{
        _11200 = NOVALUE;
    }

    /** 			executable = "eui"*/
    RefDS(_10408);
    DeRefDS(_1executable_18681);
    _1executable_18681 = _10408;
L1: 

    /** 		dexe = ".exe"		*/
    RefDS(_11201);
    DeRefi(_1dexe_18680);
    _1dexe_18680 = _11201;

    /** 	if equal(translator, "-") then*/
    if (_1translator_19190 == _6130)
    _11202 = 1;
    else if (IS_ATOM_INT(_1translator_19190) && IS_ATOM_INT(_6130))
    _11202 = 0;
    else
    _11202 = (compare(_1translator_19190, _6130) == 0);
    if (_11202 == 0)
    {
        _11202 = NOVALUE;
        goto L2; // [37] 50
    }
    else{
        _11202 = NOVALUE;
    }

    /** 		ifdef UNIX then*/

    /** 			translator = "euc.exe"*/
    RefDS(_11203);
    DeRefDS(_1translator_19190);
    _1translator_19190 = _11203;
L2: 

    /** 	ifdef UNIX then*/

    /** 		if length(translator) > 0 then*/
    if (IS_SEQUENCE(_1translator_19190)){
            _11205 = SEQ_PTR(_1translator_19190)->length;
    }
    else {
        _11205 = 1;
    }
    if (_11205 <= 0)
    goto L3; // [59] 72

    /** 			translator_options &= " -CON"*/
    Concat((object_ptr)&_1translator_options_19188, _1translator_options_19188, _11207);
L3: 

    /** 		interpreter_os_name = "WINDOWS"*/
    RefDS(_11209);
    DeRefi(_1interpreter_os_name_19193);
    _1interpreter_os_name_19193 = _11209;

    /** end procedure*/
    return;
    ;
}


int _1build_file_list(int _list_19858)
{
    int _files_19859 = NOVALUE;
    int _dirlist_19869 = NOVALUE;
    int _basedir_19875 = NOVALUE;
    int _11233 = NOVALUE;
    int _11232 = NOVALUE;
    int _11231 = NOVALUE;
    int _11230 = NOVALUE;
    int _11227 = NOVALUE;
    int _11226 = NOVALUE;
    int _11225 = NOVALUE;
    int _11224 = NOVALUE;
    int _11221 = NOVALUE;
    int _11220 = NOVALUE;
    int _11218 = NOVALUE;
    int _11217 = NOVALUE;
    int _11215 = NOVALUE;
    int _11213 = NOVALUE;
    int _11212 = NOVALUE;
    int _11211 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence files = {}*/
    RefDS(_5);
    DeRef(_files_19859);
    _files_19859 = _5;

    /** 	for j = 1 to length( list ) do*/
    if (IS_SEQUENCE(_list_19858)){
            _11211 = SEQ_PTR(_list_19858)->length;
    }
    else {
        _11211 = 1;
    }
    {
        int _j_19861;
        _j_19861 = 1;
L1: 
        if (_j_19861 > _11211){
            goto L2; // [15] 178
        }

        /** 		if file_type( list[j] ) = FILETYPE_DIRECTORY then*/
        _2 = (int)SEQ_PTR(_list_19858);
        _11212 = (int)*(((s1_ptr)_2)->base + _j_19861);
        Ref(_11212);
        _11213 = _8file_type(_11212);
        _11212 = NOVALUE;
        if (binary_op_a(NOTEQ, _11213, 2)){
            DeRef(_11213);
            _11213 = NOVALUE;
            goto L3; // [34] 160
        }
        DeRef(_11213);
        _11213 = NOVALUE;

        /** 			object dirlist = dir( list[j] )*/
        _2 = (int)SEQ_PTR(_list_19858);
        _11215 = (int)*(((s1_ptr)_2)->base + _j_19861);
        Ref(_11215);
        _0 = _dirlist_19869;
        _dirlist_19869 = _8dir(_11215);
        DeRef(_0);
        _11215 = NOVALUE;

        /** 			if sequence( dirlist ) then*/
        _11217 = IS_SEQUENCE(_dirlist_19869);
        if (_11217 == 0)
        {
            _11217 = NOVALUE;
            goto L4; // [53] 153
        }
        else{
            _11217 = NOVALUE;
        }

        /** 				sequence basedir = canonical_path( list[j] )*/
        _2 = (int)SEQ_PTR(_list_19858);
        _11218 = (int)*(((s1_ptr)_2)->base + _j_19861);
        Ref(_11218);
        _0 = _basedir_19875;
        _basedir_19875 = _8canonical_path(_11218, 0, 0);
        DeRef(_0);
        _11218 = NOVALUE;

        /** 				if basedir[$] != SLASH then*/
        if (IS_SEQUENCE(_basedir_19875)){
                _11220 = SEQ_PTR(_basedir_19875)->length;
        }
        else {
            _11220 = 1;
        }
        _2 = (int)SEQ_PTR(_basedir_19875);
        _11221 = (int)*(((s1_ptr)_2)->base + _11220);
        if (binary_op_a(EQUALS, _11221, 92)){
            _11221 = NOVALUE;
            goto L5; // [83] 96
        }
        _11221 = NOVALUE;

        /** 					basedir &= SLASH*/
        Append(&_basedir_19875, _basedir_19875, 92);
L5: 

        /** 				for k = 1 to length( dirlist ) do*/
        if (IS_SEQUENCE(_dirlist_19869)){
                _11224 = SEQ_PTR(_dirlist_19869)->length;
        }
        else {
            _11224 = 1;
        }
        {
            int _k_19887;
            _k_19887 = 1;
L6: 
            if (_k_19887 > _11224){
                goto L7; // [101] 152
            }

            /** 					files = append( files, basedir & dirlist[k][D_NAME] )*/
            _2 = (int)SEQ_PTR(_dirlist_19869);
            _11225 = (int)*(((s1_ptr)_2)->base + _k_19887);
            _2 = (int)SEQ_PTR(_11225);
            _11226 = (int)*(((s1_ptr)_2)->base + 1);
            _11225 = NOVALUE;
            if (IS_SEQUENCE(_basedir_19875) && IS_ATOM(_11226)) {
                Ref(_11226);
                Append(&_11227, _basedir_19875, _11226);
            }
            else if (IS_ATOM(_basedir_19875) && IS_SEQUENCE(_11226)) {
            }
            else {
                Concat((object_ptr)&_11227, _basedir_19875, _11226);
            }
            _11226 = NOVALUE;
            RefDS(_11227);
            Append(&_files_19859, _files_19859, _11227);
            DeRefDS(_11227);
            _11227 = NOVALUE;

            /** 					printf( 1, "adding test file: %s\n", { files[$] })*/
            if (IS_SEQUENCE(_files_19859)){
                    _11230 = SEQ_PTR(_files_19859)->length;
            }
            else {
                _11230 = 1;
            }
            _2 = (int)SEQ_PTR(_files_19859);
            _11231 = (int)*(((s1_ptr)_2)->base + _11230);
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            Ref(_11231);
            *((int *)(_2+4)) = _11231;
            _11232 = MAKE_SEQ(_1);
            _11231 = NOVALUE;
            EPrintf(1, _11229, _11232);
            DeRefDS(_11232);
            _11232 = NOVALUE;

            /** 				end for*/
            _k_19887 = _k_19887 + 1;
            goto L6; // [147] 108
L7: 
            ;
        }
L4: 
        DeRef(_basedir_19875);
        _basedir_19875 = NOVALUE;
        DeRef(_dirlist_19869);
        _dirlist_19869 = NOVALUE;
        goto L8; // [157] 171
L3: 

        /** 			files = append( files, list[j] )*/
        _2 = (int)SEQ_PTR(_list_19858);
        _11233 = (int)*(((s1_ptr)_2)->base + _j_19861);
        Ref(_11233);
        Append(&_files_19859, _files_19859, _11233);
        _11233 = NOVALUE;
L8: 

        /** 	end for*/
        _j_19861 = _j_19861 + 1;
        goto L1; // [173] 22
L2: 
        ;
    }

    /** 	return files*/
    DeRefDS(_list_19858);
    return _files_19859;
    ;
}


void _1main()
{
    int _files_19903 = NOVALUE;
    int _opts_19905 = NOVALUE;
    int _keys_19908 = NOVALUE;
    int _param_19916 = NOVALUE;
    int _val_19918 = NOVALUE;
    int _tmp_19951 = NOVALUE;
    int _tmp_19980 = NOVALUE;
    int _option_19996 = NOVALUE;
    int _pattern_20031 = NOVALUE;
    int _first_counter_20066 = NOVALUE;
    int _last_counter_20067 = NOVALUE;
    int _11346 = NOVALUE;
    int _11345 = NOVALUE;
    int _11344 = NOVALUE;
    int _11342 = NOVALUE;
    int _11340 = NOVALUE;
    int _11339 = NOVALUE;
    int _11336 = NOVALUE;
    int _11335 = NOVALUE;
    int _11334 = NOVALUE;
    int _11332 = NOVALUE;
    int _11331 = NOVALUE;
    int _11330 = NOVALUE;
    int _11327 = NOVALUE;
    int _11325 = NOVALUE;
    int _11324 = NOVALUE;
    int _11323 = NOVALUE;
    int _11322 = NOVALUE;
    int _11318 = NOVALUE;
    int _11316 = NOVALUE;
    int _11315 = NOVALUE;
    int _11314 = NOVALUE;
    int _11311 = NOVALUE;
    int _11310 = NOVALUE;
    int _11309 = NOVALUE;
    int _11307 = NOVALUE;
    int _11305 = NOVALUE;
    int _11304 = NOVALUE;
    int _11303 = NOVALUE;
    int _11301 = NOVALUE;
    int _11298 = NOVALUE;
    int _11295 = NOVALUE;
    int _11293 = NOVALUE;
    int _11292 = NOVALUE;
    int _11291 = NOVALUE;
    int _11289 = NOVALUE;
    int _11285 = NOVALUE;
    int _11284 = NOVALUE;
    int _11282 = NOVALUE;
    int _11276 = NOVALUE;
    int _11272 = NOVALUE;
    int _11268 = NOVALUE;
    int _11261 = NOVALUE;
    int _11256 = NOVALUE;
    int _11251 = NOVALUE;
    int _11246 = NOVALUE;
    int _11240 = NOVALUE;
    int _11238 = NOVALUE;
    int _11237 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object files = {}*/
    RefDS(_5);
    DeRef(_files_19903);
    _files_19903 = _5;

    /** 	map opts = cmd_parse( cmdopts )*/
    _11346 = Command_Line();
    RefDS(_1cmdopts_18523);
    RefDS(_5);
    _0 = _opts_19905;
    _opts_19905 = _30cmd_parse(_1cmdopts_18523, _5, _11346);
    DeRef(_0);
    _11346 = NOVALUE;

    /** 	sequence keys = map:keys( opts )*/
    Ref(_opts_19905);
    _0 = _keys_19908;
    _keys_19908 = _25keys(_opts_19905, 0);
    DeRef(_0);

    /** 	no_check = map:has( opts, "n") or map:has( opts, "nocheck" )*/
    Ref(_opts_19905);
    RefDS(_8603);
    _11237 = _25has(_opts_19905, _8603);
    Ref(_opts_19905);
    RefDS(_10495);
    _11238 = _25has(_opts_19905, _10495);
    if (IS_ATOM_INT(_11237) && IS_ATOM_INT(_11238)) {
        _1no_check_18676 = (_11237 != 0 || _11238 != 0);
    }
    else {
        _1no_check_18676 = binary_op(OR, _11237, _11238);
    }
    DeRef(_11237);
    _11237 = NOVALUE;
    DeRef(_11238);
    _11238 = NOVALUE;
    if (!IS_ATOM_INT(_1no_check_18676)) {
        _1 = (long)(DBL_PTR(_1no_check_18676)->dbl);
        if (UNIQUE(DBL_PTR(_1no_check_18676)) && (DBL_PTR(_1no_check_18676)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_1no_check_18676);
        _1no_check_18676 = _1;
    }

    /** 	for i = 1 to length( keys ) do*/
    if (IS_SEQUENCE(_keys_19908)){
            _11240 = SEQ_PTR(_keys_19908)->length;
    }
    else {
        _11240 = 1;
    }
    {
        int _i_19914;
        _i_19914 = 1;
L1: 
        if (_i_19914 > _11240){
            goto L2; // [52] 982
        }

        /** 		sequence param = keys[i]*/
        DeRef(_param_19916);
        _2 = (int)SEQ_PTR(_keys_19908);
        _param_19916 = (int)*(((s1_ptr)_2)->base + _i_19914);
        Ref(_param_19916);

        /** 		object val = map:get(opts, param)*/
        Ref(_opts_19905);
        RefDS(_param_19916);
        _0 = _val_19918;
        _val_19918 = _25get(_opts_19905, _param_19916, 0);
        DeRef(_0);

        /** 		switch param do*/
        if( _1_19920_cases == 0 ){
            _1_19920_cases = 1;
            SEQ_PTR( _11243 )->base[1] = _10383;
            SEQ_PTR( _11243 )->base[2] = _10386;
            SEQ_PTR( _11243 )->base[3] = _10389;
            SEQ_PTR( _11243 )->base[4] = _10392;
            SEQ_PTR( _11243 )->base[5] = _10460;
            SEQ_PTR( _11243 )->base[6] = _10380;
            SEQ_PTR( _11243 )->base[7] = _10408;
            SEQ_PTR( _11243 )->base[8] = _10413;
            SEQ_PTR( _11243 )->base[9] = _10417;
            SEQ_PTR( _11243 )->base[10] = _10421;
            SEQ_PTR( _11243 )->base[11] = _10425;
            SEQ_PTR( _11243 )->base[12] = _10429;
            SEQ_PTR( _11243 )->base[13] = _10433;
            SEQ_PTR( _11243 )->base[14] = _10437;
            SEQ_PTR( _11243 )->base[15] = _10440;
            SEQ_PTR( _11243 )->base[16] = _10445;
            SEQ_PTR( _11243 )->base[17] = _10450;
            SEQ_PTR( _11243 )->base[18] = _10455;
            SEQ_PTR( _11243 )->base[19] = _10466;
            SEQ_PTR( _11243 )->base[20] = _10471;
            SEQ_PTR( _11243 )->base[21] = _10476;
            SEQ_PTR( _11243 )->base[22] = _10479;
            SEQ_PTR( _11243 )->base[23] = _10483;
            SEQ_PTR( _11243 )->base[24] = _10403;
            SEQ_PTR( _11243 )->base[25] = _10398;
            SEQ_PTR( _11243 )->base[26] = _30EXTRAS_14392;
            SEQ_PTR( _11243 )->base[27] = _8603;
            SEQ_PTR( _11243 )->base[28] = _10495;
        }
        _1 = find(_param_19916, _11243);
        switch ( _1 ){ 

            /** 			case "all","failed","wait","accumulate" then*/
            case 1:
            case 2:
            case 3:
            case 4:

            /** 				test_options &= " -" & param		*/
            Concat((object_ptr)&_11246, _11245, _param_19916);
            Concat((object_ptr)&_1test_options_19189, _1test_options_19189, _11246);
            DeRefDS(_11246);
            _11246 = NOVALUE;
            goto L3; // [104] 973

            /** 			case "log" then*/
            case 5:

            /** 				logging_activated = 1*/
            _1logging_activated_18677 = 1;

            /** 				test_options &= " -log "*/
            Concat((object_ptr)&_1test_options_19189, _1test_options_19189, _11248);
            goto L3; // [125] 973

            /** 			case "verbose" then*/
            case 6:

            /** 				verbose_switch = 1*/
            _1verbose_switch_18669 = 1;
            goto L3; // [138] 973

            /** 			case "eui", "exe" then*/
            case 7:
            case 8:

            /** 				executable = canonical_path(val)*/
            Ref(_val_19918);
            _0 = _8canonical_path(_val_19918, 0, 0);
            DeRef(_1executable_18681);
            _1executable_18681 = _0;

            /** 				if not file_exists(executable) then*/
            RefDS(_1executable_18681);
            _11251 = _8file_exists(_1executable_18681);
            if (IS_ATOM_INT(_11251)) {
                if (_11251 != 0){
                    DeRef(_11251);
                    _11251 = NOVALUE;
                    goto L3; // [166] 973
                }
            }
            else {
                if (DBL_PTR(_11251)->dbl != 0.0){
                    DeRef(_11251);
                    _11251 = NOVALUE;
                    goto L3; // [166] 973
                }
            }
            DeRef(_11251);
            _11251 = NOVALUE;

            /** 					printf(1, "Specified interpreter via -eui parameter was not found\n")*/
            EPrintf(1, _11253, _5);

            /** 					if not no_check then*/
            if (_1no_check_18676 != 0)
            goto L3; // [179] 973

            /** 						abort(1)*/
            UserCleanup(1);
            goto L3; // [188] 973

            /** 			case "eubind", "bind" then*/
            case 9:
            case 10:

            /** 				binder = canonical_path(val)*/
            Ref(_val_19918);
            _0 = _8canonical_path(_val_19918, 0, 0);
            DeRef(_1binder_19194);
            _1binder_19194 = _0;

            /** 				if not file_exists(binder) then*/
            RefDS(_1binder_19194);
            _11256 = _8file_exists(_1binder_19194);
            if (IS_ATOM_INT(_11256)) {
                if (_11256 != 0){
                    DeRef(_11256);
                    _11256 = NOVALUE;
                    goto L3; // [216] 973
                }
            }
            else {
                if (DBL_PTR(_11256)->dbl != 0.0){
                    DeRef(_11256);
                    _11256 = NOVALUE;
                    goto L3; // [216] 973
                }
            }
            DeRef(_11256);
            _11256 = NOVALUE;

            /** 					printf(1, "Specified binder via -eubind parameter was not found\n")*/
            EPrintf(1, _11258, _5);

            /** 					if not no_check then*/
            if (_1no_check_18676 != 0)
            goto L3; // [229] 973

            /** 						abort(1)*/
            UserCleanup(1);
            goto L3; // [238] 973

            /** 			case "eub" then*/
            case 11:

            /** 				sequence tmp = canonical_path(val)*/
            Ref(_val_19918);
            _0 = _tmp_19951;
            _tmp_19951 = _8canonical_path(_val_19918, 0, 0);
            DeRef(_0);

            /** 				if not file_exists(tmp) then*/
            RefDS(_tmp_19951);
            _11261 = _8file_exists(_tmp_19951);
            if (IS_ATOM_INT(_11261)) {
                if (_11261 != 0){
                    DeRef(_11261);
                    _11261 = NOVALUE;
                    goto L4; // [262] 284
                }
            }
            else {
                if (DBL_PTR(_11261)->dbl != 0.0){
                    DeRef(_11261);
                    _11261 = NOVALUE;
                    goto L4; // [262] 284
                }
            }
            DeRef(_11261);
            _11261 = NOVALUE;

            /** 					printf(1, "Specified backend via -eub parameter was not found\n")*/
            EPrintf(1, _11263, _5);

            /** 					if not no_check then*/
            if (_1no_check_18676 != 0)
            goto L5; // [275] 283

            /** 						abort(1)*/
            UserCleanup(1);
L5: 
L4: 

            /** 				eub_path = "-eub " & tmp*/
            Concat((object_ptr)&_1eub_path_18674, _11265, _tmp_19951);
            DeRefDS(_tmp_19951);
            _tmp_19951 = NOVALUE;
            goto L3; // [292] 973

            /** 			case "euc", "ec" then*/
            case 12:
            case 13:

            /** 				translator = canonical_path(val)*/
            Ref(_val_19918);
            _0 = _8canonical_path(_val_19918, 0, 0);
            DeRef(_1translator_19190);
            _1translator_19190 = _0;

            /** 				if not file_exists(translator) then*/
            RefDS(_1translator_19190);
            _11268 = _8file_exists(_1translator_19190);
            if (IS_ATOM_INT(_11268)) {
                if (_11268 != 0){
                    DeRef(_11268);
                    _11268 = NOVALUE;
                    goto L3; // [320] 973
                }
            }
            else {
                if (DBL_PTR(_11268)->dbl != 0.0){
                    DeRef(_11268);
                    _11268 = NOVALUE;
                    goto L3; // [320] 973
                }
            }
            DeRef(_11268);
            _11268 = NOVALUE;

            /** 					printf(1, "Specified translator via -euc parameter was not found\n")*/
            EPrintf(1, _11270, _5);

            /** 					if not no_check then*/
            if (_1no_check_18676 != 0)
            goto L3; // [333] 973

            /** 						abort(1)*/
            UserCleanup(1);
            goto L3; // [342] 973

            /** 			case "trans" then*/
            case 14:

            /** 				if not length( translator ) then*/
            if (IS_SEQUENCE(_1translator_19190)){
                    _11272 = SEQ_PTR(_1translator_19190)->length;
            }
            else {
                _11272 = 1;
            }
            if (_11272 != 0)
            goto L3; // [355] 973
            _11272 = NOVALUE;

            /** 					translator = "-"*/
            RefDS(_6130);
            DeRefDS(_1translator_19190);
            _1translator_19190 = _6130;
            goto L3; // [366] 973

            /** 			case "cc" then*/
            case 15:

            /** 				compiler = "-" & val*/
            if (IS_SEQUENCE(_6130) && IS_ATOM(_val_19918)) {
                Ref(_val_19918);
                Append(&_1compiler_19192, _6130, _val_19918);
            }
            else if (IS_ATOM(_6130) && IS_SEQUENCE(_val_19918)) {
            }
            else {
                Concat((object_ptr)&_1compiler_19192, _6130, _val_19918);
            }
            goto L3; // [378] 973

            /** 			case "lib" then*/
            case 16:

            /** 				sequence tmp = canonical_path(val)*/
            Ref(_val_19918);
            _0 = _tmp_19980;
            _tmp_19980 = _8canonical_path(_val_19918, 0, 0);
            DeRef(_0);

            /** 				if not file_exists(tmp) then*/
            RefDS(_tmp_19980);
            _11276 = _8file_exists(_tmp_19980);
            if (IS_ATOM_INT(_11276)) {
                if (_11276 != 0){
                    DeRef(_11276);
                    _11276 = NOVALUE;
                    goto L6; // [402] 424
                }
            }
            else {
                if (DBL_PTR(_11276)->dbl != 0.0){
                    DeRef(_11276);
                    _11276 = NOVALUE;
                    goto L6; // [402] 424
                }
            }
            DeRef(_11276);
            _11276 = NOVALUE;

            /** 					printf(1, "Specified library via -lib parameter was not found\n")*/
            EPrintf(1, _11278, _5);

            /** 					if not no_check then*/
            if (_1no_check_18676 != 0)
            goto L7; // [415] 423

            /** 						abort(1)*/
            UserCleanup(1);
L7: 
L6: 

            /** 				library = "-lib " & tmp*/
            Concat((object_ptr)&_1library_19191, _11280, _tmp_19980);
            DeRefDS(_tmp_19980);
            _tmp_19980 = NOVALUE;
            goto L3; // [432] 973

            /** 			case "i", "d" then*/
            case 17:
            case 18:

            /** 				for j = 1 to length(val) do*/
            if (IS_SEQUENCE(_val_19918)){
                    _11282 = SEQ_PTR(_val_19918)->length;
            }
            else {
                _11282 = 1;
            }
            {
                int _j_19994;
                _j_19994 = 1;
L8: 
                if (_j_19994 > _11282){
                    goto L9; // [445] 491
                }

                /** 					sequence option = sprintf( " -%s %s", {param, val[j] })*/
                _2 = (int)SEQ_PTR(_val_19918);
                _11284 = (int)*(((s1_ptr)_2)->base + _j_19994);
                Ref(_11284);
                RefDS(_param_19916);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _param_19916;
                ((int *)_2)[2] = _11284;
                _11285 = MAKE_SEQ(_1);
                _11284 = NOVALUE;
                DeRefi(_option_19996);
                _option_19996 = EPrintf(-9999999, _11283, _11285);
                DeRefDS(_11285);
                _11285 = NOVALUE;

                /** 					interpreter_options &= option*/
                Concat((object_ptr)&_1interpreter_options_19187, _1interpreter_options_19187, _option_19996);

                /** 					translator_options &= option*/
                Concat((object_ptr)&_1translator_options_19188, _1translator_options_19188, _option_19996);
                DeRefDSi(_option_19996);
                _option_19996 = NOVALUE;

                /** 				end for*/
                _j_19994 = _j_19994 + 1;
                goto L8; // [486] 452
L9: 
                ;
            }
            goto L3; // [491] 973

            /** 			case "coverage" then*/
            case 19:

            /** 				for j = 1 to length( val ) do*/
            if (IS_SEQUENCE(_val_19918)){
                    _11289 = SEQ_PTR(_val_19918)->length;
            }
            else {
                _11289 = 1;
            }
            {
                int _j_20005;
                _j_20005 = 1;
LA: 
                if (_j_20005 > _11289){
                    goto LB; // [502] 536
                }

                /** 					interpreter_options &= sprintf( " -coverage %s", {val[j]} )*/
                _2 = (int)SEQ_PTR(_val_19918);
                _11291 = (int)*(((s1_ptr)_2)->base + _j_20005);
                _1 = NewS1(1);
                _2 = (int)((s1_ptr)_1)->base;
                Ref(_11291);
                *((int *)(_2+4)) = _11291;
                _11292 = MAKE_SEQ(_1);
                _11291 = NOVALUE;
                _11293 = EPrintf(-9999999, _11290, _11292);
                DeRefDS(_11292);
                _11292 = NOVALUE;
                Concat((object_ptr)&_1interpreter_options_19187, _1interpreter_options_19187, _11293);
                DeRefDS(_11293);
                _11293 = NOVALUE;

                /** 				end for*/
                _j_20005 = _j_20005 + 1;
                goto LA; // [531] 509
LB: 
                ;
            }

            /** 				if not length( coverage_db ) then*/
            if (IS_SEQUENCE(_1coverage_db_19195)){
                    _11295 = SEQ_PTR(_1coverage_db_19195)->length;
            }
            else {
                _11295 = 1;
            }
            if (_11295 != 0)
            goto L3; // [543] 973
            _11295 = NOVALUE;

            /** 					coverage_db = "-"*/
            RefDS(_6130);
            DeRefDS(_1coverage_db_19195);
            _1coverage_db_19195 = _6130;
            goto L3; // [554] 973

            /** 			case "coverage-db" then*/
            case 20:

            /** 				coverage_db = val*/
            Ref(_val_19918);
            DeRef(_1coverage_db_19195);
            _1coverage_db_19195 = _val_19918;

            /** 				interpreter_options &= " -coverage-db " & val*/
            if (IS_SEQUENCE(_11297) && IS_ATOM(_val_19918)) {
                Ref(_val_19918);
                Append(&_11298, _11297, _val_19918);
            }
            else if (IS_ATOM(_11297) && IS_SEQUENCE(_val_19918)) {
            }
            else {
                Concat((object_ptr)&_11298, _11297, _val_19918);
            }
            Concat((object_ptr)&_1interpreter_options_19187, _1interpreter_options_19187, _11298);
            DeRefDS(_11298);
            _11298 = NOVALUE;
            goto L3; // [579] 973

            /** 			case "coverage-erase" then*/
            case 21:

            /** 				coverage_erase = "-coverage-erase"*/
            RefDS(_11300);
            DeRefi(_1coverage_erase_19197);
            _1coverage_erase_19197 = _11300;
            goto L3; // [592] 973

            /** 			case "coverage-pp" then*/
            case 22:

            /** 				coverage_pp = val*/
            Ref(_val_19918);
            DeRef(_1coverage_pp_19196);
            _1coverage_pp_19196 = _val_19918;
            goto L3; // [605] 973

            /** 			case "coverage-exclude" then*/
            case 23:

            /** 				for j = 1 to length( val ) do*/
            if (IS_SEQUENCE(_val_19918)){
                    _11301 = SEQ_PTR(_val_19918)->length;
            }
            else {
                _11301 = 1;
            }
            {
                int _j_20024;
                _j_20024 = 1;
LC: 
                if (_j_20024 > _11301){
                    goto LD; // [616] 707
                }

                /** 					interpreter_options &= sprintf(` -coverage-exclude "%s"`, {val[j]} )*/
                _2 = (int)SEQ_PTR(_val_19918);
                _11303 = (int)*(((s1_ptr)_2)->base + _j_20024);
                _1 = NewS1(1);
                _2 = (int)((s1_ptr)_1)->base;
                Ref(_11303);
                *((int *)(_2+4)) = _11303;
                _11304 = MAKE_SEQ(_1);
                _11303 = NOVALUE;
                _11305 = EPrintf(-9999999, _11302, _11304);
                DeRefDS(_11304);
                _11304 = NOVALUE;
                Concat((object_ptr)&_1interpreter_options_19187, _1interpreter_options_19187, _11305);
                DeRefDS(_11305);
                _11305 = NOVALUE;

                /** 					object pattern = regex:new( val[j] )*/
                _2 = (int)SEQ_PTR(_val_19918);
                _11307 = (int)*(((s1_ptr)_2)->base + _j_20024);
                Ref(_11307);
                _0 = _pattern_20031;
                _pattern_20031 = _34new(_11307, 0);
                DeRef(_0);
                _11307 = NOVALUE;

                /** 					if regex( pattern ) then*/
                Ref(_pattern_20031);
                _11309 = _34regex(_pattern_20031);
                if (_11309 == 0) {
                    DeRef(_11309);
                    _11309 = NOVALUE;
                    goto LE; // [660] 683
                }
                else {
                    if (!IS_ATOM_INT(_11309) && DBL_PTR(_11309)->dbl == 0.0){
                        DeRef(_11309);
                        _11309 = NOVALUE;
                        goto LE; // [660] 683
                    }
                    DeRef(_11309);
                    _11309 = NOVALUE;
                }
                DeRef(_11309);
                _11309 = NOVALUE;

                /** 						exclude_patterns = append( exclude_patterns, regex:new( val[j] ) )*/
                _2 = (int)SEQ_PTR(_val_19918);
                _11310 = (int)*(((s1_ptr)_2)->base + _j_20024);
                Ref(_11310);
                _11311 = _34new(_11310, 0);
                _11310 = NOVALUE;
                Ref(_11311);
                Append(&_1exclude_patterns_18675, _1exclude_patterns_18675, _11311);
                DeRef(_11311);
                _11311 = NOVALUE;
                goto LF; // [680] 698
LE: 

                /** 						printf(2, "invalid exclude pattern: [%s]\n", { val[j] } )*/
                _2 = (int)SEQ_PTR(_val_19918);
                _11314 = (int)*(((s1_ptr)_2)->base + _j_20024);
                _1 = NewS1(1);
                _2 = (int)((s1_ptr)_1)->base;
                Ref(_11314);
                *((int *)(_2+4)) = _11314;
                _11315 = MAKE_SEQ(_1);
                _11314 = NOVALUE;
                EPrintf(2, _11313, _11315);
                DeRefDS(_11315);
                _11315 = NOVALUE;
LF: 
                DeRef(_pattern_20031);
                _pattern_20031 = NOVALUE;

                /** 				end for*/
                _j_20024 = _j_20024 + 1;
                goto LC; // [702] 623
LD: 
                ;
            }
            goto L3; // [707] 973

            /** 			case "testopt" then*/
            case 24:

            /** 				test_options &= " -" & val & " "*/
            {
                int concat_list[3];

                concat_list[0] = _1403;
                concat_list[1] = _val_19918;
                concat_list[2] = _11245;
                Concat_N((object_ptr)&_11316, concat_list, 3);
            }
            Concat((object_ptr)&_1test_options_19189, _1test_options_19189, _11316);
            DeRefDS(_11316);
            _11316 = NOVALUE;
            goto L3; // [727] 973

            /** 			case "html-file" then*/
            case 25:

            /** 				html_fn = val*/
            Ref(_val_19918);
            DeRef(_1html_fn_18682);
            _1html_fn_18682 = _val_19918;
            goto L3; // [738] 973

            /** 			case cmdline:EXTRAS then*/
            case 26:

            /** 				if length( val ) then*/
            if (IS_SEQUENCE(_val_19918)){
                    _11318 = SEQ_PTR(_val_19918)->length;
            }
            else {
                _11318 = 1;
            }
            if (_11318 == 0)
            {
                _11318 = NOVALUE;
                goto L10; // [749] 761
            }
            else{
                _11318 = NOVALUE;
            }

            /** 					files = build_file_list( val )*/
            Ref(_val_19918);
            _0 = _files_19903;
            _files_19903 = _1build_file_list(_val_19918);
            DeRef(_0);
            goto L3; // [758] 973
L10: 

            /** 					files = dir("t_*.e" )*/
            RefDS(_11320);
            _0 = _files_19903;
            _files_19903 = _8dir(_11320);
            DeRef(_0);

            /** 					if atom(files) then*/
            _11322 = IS_ATOM(_files_19903);
            if (_11322 == 0)
            {
                _11322 = NOVALUE;
                goto L11; // [772] 781
            }
            else{
                _11322 = NOVALUE;
            }

            /** 						files = {}*/
            RefDS(_5);
            DeRef(_files_19903);
            _files_19903 = _5;
L11: 

            /** 					for f = 1 to length( files ) do*/
            if (IS_SEQUENCE(_files_19903)){
                    _11323 = SEQ_PTR(_files_19903)->length;
            }
            else {
                _11323 = 1;
            }
            {
                int _f_20059;
                _f_20059 = 1;
L12: 
                if (_f_20059 > _11323){
                    goto L13; // [786] 816
                }

                /** 						files[f] = files[f][D_NAME]*/
                _2 = (int)SEQ_PTR(_files_19903);
                _11324 = (int)*(((s1_ptr)_2)->base + _f_20059);
                _2 = (int)SEQ_PTR(_11324);
                _11325 = (int)*(((s1_ptr)_2)->base + 1);
                _11324 = NOVALUE;
                Ref(_11325);
                _2 = (int)SEQ_PTR(_files_19903);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _files_19903 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + _f_20059);
                _1 = *(int *)_2;
                *(int *)_2 = _11325;
                if( _1 != _11325 ){
                    DeRef(_1);
                }
                _11325 = NOVALUE;

                /** 					end for*/
                _f_20059 = _f_20059 + 1;
                goto L12; // [811] 793
L13: 
                ;
            }

            /** 					files = sort( files )*/
            Ref(_files_19903);
            _0 = _files_19903;
            _files_19903 = _7sort(_files_19903, 1);
            DeRef(_0);

            /** 					integer first_counter*/

            /** 					integer last_counter*/

            /** 					first_counter = length(files)+1*/
            if (IS_SEQUENCE(_files_19903)){
                    _11327 = SEQ_PTR(_files_19903)->length;
            }
            else {
                _11327 = 1;
            }
            _first_counter_20066 = _11327 + 1;
            _11327 = NOVALUE;

            /** 					last_counter = length(files)*/
            if (IS_SEQUENCE(_files_19903)){
                    _last_counter_20067 = SEQ_PTR(_files_19903)->length;
            }
            else {
                _last_counter_20067 = 1;
            }

            /** 					for f = 1 to length(files) do*/
            if (IS_SEQUENCE(_files_19903)){
                    _11330 = SEQ_PTR(_files_19903)->length;
            }
            else {
                _11330 = 1;
            }
            {
                int _f_20072;
                _f_20072 = 1;
L14: 
                if (_f_20072 > _11330){
                    goto L15; // [850] 891
                }

                /** 						if match("t_c_", files[f])=1 then*/
                _2 = (int)SEQ_PTR(_files_19903);
                _11331 = (int)*(((s1_ptr)_2)->base + _f_20072);
                _11332 = e_match_from(_10743, _11331, 1);
                _11331 = NOVALUE;
                if (_11332 != 1)
                goto L16; // [868] 884

                /** 							first_counter = f*/
                _first_counter_20066 = _f_20072;

                /** 							exit*/
                goto L15; // [881] 891
L16: 

                /** 					end for*/
                _f_20072 = _f_20072 + 1;
                goto L14; // [886] 857
L15: 
                ;
            }

            /** 					for f = first_counter to length(files) do*/
            if (IS_SEQUENCE(_files_19903)){
                    _11334 = SEQ_PTR(_files_19903)->length;
            }
            else {
                _11334 = 1;
            }
            {
                int _f_20079;
                _f_20079 = _first_counter_20066;
L17: 
                if (_f_20079 > _11334){
                    goto L18; // [896] 938
                }

                /** 						if match("t_c_", files[f])!=1 then*/
                _2 = (int)SEQ_PTR(_files_19903);
                _11335 = (int)*(((s1_ptr)_2)->base + _f_20079);
                _11336 = e_match_from(_10743, _11335, 1);
                _11335 = NOVALUE;
                if (_11336 == 1)
                goto L19; // [914] 931

                /** 							last_counter = f-1*/
                _last_counter_20067 = _f_20079 - 1;

                /** 							exit*/
                goto L18; // [928] 938
L19: 

                /** 					end for*/
                _f_20079 = _f_20079 + 1;
                goto L17; // [933] 903
L18: 
                ;
            }

            /** 					files = remove(files,first_counter,last_counter) */
            {
                s1_ptr assign_space = SEQ_PTR(_files_19903);
                int len = assign_space->length;
                int start = (IS_ATOM_INT(_first_counter_20066)) ? _first_counter_20066 : (long)(DBL_PTR(_first_counter_20066)->dbl);
                int stop = (IS_ATOM_INT(_last_counter_20067)) ? _last_counter_20067 : (long)(DBL_PTR(_last_counter_20067)->dbl);
                if (stop > len){
                    stop = len;
                }
                if (start > len || start > stop || stop<1) {
                    RefDS(_files_19903);
                    DeRef(_11339);
                    _11339 = _files_19903;
                }
                else if (start < 2) {
                    if (stop >= len) {
                        Head( SEQ_PTR(_files_19903), start, &_11339 );
                    }
                    else Tail(SEQ_PTR(_files_19903), stop+1, &_11339);
                }
                else if (stop >= len){
                    Head(SEQ_PTR(_files_19903), start, &_11339);
                }
                else {
                    assign_slice_seq = &assign_space;
                    _1 = Remove_elements(start, stop, 0);
                    DeRef(_11339);
                    _11339 = _1;
                }
            }
            rhs_slice_target = (object_ptr)&_11340;
            RHS_Slice(_files_19903, _first_counter_20066, _last_counter_20067);
            Concat((object_ptr)&_files_19903, _11339, _11340);
            DeRefDS(_11339);
            _11339 = NOVALUE;
            DeRef(_11339);
            _11339 = NOVALUE;
            DeRefDS(_11340);
            _11340 = NOVALUE;
            goto L3; // [957] 973

            /** 			case "n", "nocheck" then*/
            case 27:
            case 28:

            /** 				no_check = 1*/
            _1no_check_18676 = 1;
        ;}L3: 
        DeRef(_param_19916);
        _param_19916 = NOVALUE;
        DeRef(_val_19918);
        _val_19918 = NOVALUE;

        /** 	end for*/
        _i_19914 = _i_19914 + 1;
        goto L1; // [977] 59
L2: 
        ;
    }

    /** 	if equal( coverage_db, "-" ) then*/
    if (_1coverage_db_19195 == _6130)
    _11342 = 1;
    else if (IS_ATOM_INT(_1coverage_db_19195) && IS_ATOM_INT(_6130))
    _11342 = 0;
    else
    _11342 = (compare(_1coverage_db_19195, _6130) == 0);
    if (_11342 == 0)
    {
        _11342 = NOVALUE;
        goto L1A; // [990] 1001
    }
    else{
        _11342 = NOVALUE;
    }

    /** 		coverage_db = "eutest-cvg.edb"*/
    RefDS(_11343);
    DeRefDS(_1coverage_db_19195);
    _1coverage_db_19195 = _11343;
L1A: 

    /** 	if map:has( opts, "process-log") then*/
    Ref(_opts_19905);
    RefDS(_10463);
    _11344 = _25has(_opts_19905, _10463);
    if (_11344 == 0) {
        DeRef(_11344);
        _11344 = NOVALUE;
        goto L1B; // [1008] 1025
    }
    else {
        if (!IS_ATOM_INT(_11344) && DBL_PTR(_11344)->dbl == 0.0){
            DeRef(_11344);
            _11344 = NOVALUE;
            goto L1B; // [1008] 1025
        }
        DeRef(_11344);
        _11344 = NOVALUE;
    }
    DeRef(_11344);
    _11344 = NOVALUE;

    /** 		do_process_log( files, map:has( opts, "html" ) )*/
    Ref(_opts_19905);
    RefDS(_10395);
    _11345 = _25has(_opts_19905, _10395);
    Ref(_files_19903);
    _1do_process_log(_files_19903, _11345);
    _11345 = NOVALUE;
    goto L1C; // [1022] 1035
L1B: 

    /** 		platform_init()*/
    _1platform_init();

    /** 		do_test( files )*/
    Ref(_files_19903);
    _1do_test(_files_19903);
L1C: 

    /** end procedure*/
    DeRef(_files_19903);
    DeRef(_opts_19905);
    DeRef(_keys_19908);
    return;
    ;
}



// 0x122B16BC
