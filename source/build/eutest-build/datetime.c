// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _9time()
{
    int _ptra_8215 = NOVALUE;
    int _valhi_8216 = NOVALUE;
    int _vallow_8217 = NOVALUE;
    int _deltahi_8218 = NOVALUE;
    int _deltalow_8219 = NOVALUE;
    int _4534 = NOVALUE;
    int _4532 = NOVALUE;
    int _4531 = NOVALUE;
    int _4530 = NOVALUE;
    int _4527 = NOVALUE;
    int _4522 = NOVALUE;
    int _4520 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		atom ptra, valhi, vallow, deltahi, deltalow*/

    /** 		deltahi = 27111902*/
    _deltahi_8218 = 27111902;

    /** 		deltalow = 3577643008*/
    RefDS(_4518);
    DeRef(_deltalow_8219);
    _deltalow_8219 = _4518;

    /** 		ptra = machine:allocate(8)*/
    _0 = _ptra_8215;
    _ptra_8215 = _11allocate(8, 0);
    DeRef(_0);

    /** 		c_proc(time_, {ptra})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ptra_8215);
    *((int *)(_2+4)) = _ptra_8215;
    _4520 = MAKE_SEQ(_1);
    call_c(0, _9time__8197, _4520);
    DeRefDS(_4520);
    _4520 = NOVALUE;

    /** 		vallow = peek4u(ptra)*/
    DeRef(_vallow_8217);
    if (IS_ATOM_INT(_ptra_8215)) {
        _vallow_8217 = *(unsigned long *)_ptra_8215;
        if ((unsigned)_vallow_8217 > (unsigned)MAXINT)
        _vallow_8217 = NewDouble((double)(unsigned long)_vallow_8217);
    }
    else {
        _vallow_8217 = *(unsigned long *)(unsigned long)(DBL_PTR(_ptra_8215)->dbl);
        if ((unsigned)_vallow_8217 > (unsigned)MAXINT)
        _vallow_8217 = NewDouble((double)(unsigned long)_vallow_8217);
    }

    /** 		valhi = peek4u(ptra+4)*/
    if (IS_ATOM_INT(_ptra_8215)) {
        _4522 = _ptra_8215 + 4;
        if ((long)((unsigned long)_4522 + (unsigned long)HIGH_BITS) >= 0) 
        _4522 = NewDouble((double)_4522);
    }
    else {
        _4522 = NewDouble(DBL_PTR(_ptra_8215)->dbl + (double)4);
    }
    DeRef(_valhi_8216);
    if (IS_ATOM_INT(_4522)) {
        _valhi_8216 = *(unsigned long *)_4522;
        if ((unsigned)_valhi_8216 > (unsigned)MAXINT)
        _valhi_8216 = NewDouble((double)(unsigned long)_valhi_8216);
    }
    else {
        _valhi_8216 = *(unsigned long *)(unsigned long)(DBL_PTR(_4522)->dbl);
        if ((unsigned)_valhi_8216 > (unsigned)MAXINT)
        _valhi_8216 = NewDouble((double)(unsigned long)_valhi_8216);
    }
    DeRef(_4522);
    _4522 = NOVALUE;

    /** 		machine:free(ptra)*/
    Ref(_ptra_8215);
    _11free(_ptra_8215);

    /** 		vallow -= deltalow*/
    _0 = _vallow_8217;
    if (IS_ATOM_INT(_vallow_8217)) {
        _vallow_8217 = NewDouble((double)_vallow_8217 - DBL_PTR(_deltalow_8219)->dbl);
    }
    else {
        _vallow_8217 = NewDouble(DBL_PTR(_vallow_8217)->dbl - DBL_PTR(_deltalow_8219)->dbl);
    }
    DeRef(_0);

    /** 		valhi -= deltahi*/
    _0 = _valhi_8216;
    if (IS_ATOM_INT(_valhi_8216)) {
        _valhi_8216 = _valhi_8216 - 27111902;
        if ((long)((unsigned long)_valhi_8216 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_8216 = NewDouble((double)_valhi_8216);
        }
    }
    else {
        _valhi_8216 = NewDouble(DBL_PTR(_valhi_8216)->dbl - (double)27111902);
    }
    DeRef(_0);

    /** 		if vallow < 0 then*/
    if (binary_op_a(GREATEREQ, _vallow_8217, 0)){
        goto L1; // [67] 88
    }

    /** 			vallow += power(2, 32)*/
    _4527 = power(2, 32);
    _0 = _vallow_8217;
    if (IS_ATOM_INT(_4527)) {
        _vallow_8217 = NewDouble(DBL_PTR(_vallow_8217)->dbl + (double)_4527);
    }
    else
    _vallow_8217 = NewDouble(DBL_PTR(_vallow_8217)->dbl + DBL_PTR(_4527)->dbl);
    DeRefDS(_0);
    DeRef(_4527);
    _4527 = NOVALUE;

    /** 			valhi -= 1*/
    _0 = _valhi_8216;
    if (IS_ATOM_INT(_valhi_8216)) {
        _valhi_8216 = _valhi_8216 - 1;
        if ((long)((unsigned long)_valhi_8216 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_8216 = NewDouble((double)_valhi_8216);
        }
    }
    else {
        _valhi_8216 = NewDouble(DBL_PTR(_valhi_8216)->dbl - (double)1);
    }
    DeRef(_0);
L1: 

    /** 		return floor(((valhi * power(2,32)) + vallow) / 10000000)*/
    _4530 = power(2, 32);
    if (IS_ATOM_INT(_valhi_8216) && IS_ATOM_INT(_4530)) {
        if (_valhi_8216 == (short)_valhi_8216 && _4530 <= INT15 && _4530 >= -INT15)
        _4531 = _valhi_8216 * _4530;
        else
        _4531 = NewDouble(_valhi_8216 * (double)_4530);
    }
    else {
        if (IS_ATOM_INT(_valhi_8216)) {
            _4531 = NewDouble((double)_valhi_8216 * DBL_PTR(_4530)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4530)) {
                _4531 = NewDouble(DBL_PTR(_valhi_8216)->dbl * (double)_4530);
            }
            else
            _4531 = NewDouble(DBL_PTR(_valhi_8216)->dbl * DBL_PTR(_4530)->dbl);
        }
    }
    DeRef(_4530);
    _4530 = NOVALUE;
    if (IS_ATOM_INT(_4531) && IS_ATOM_INT(_vallow_8217)) {
        _4532 = _4531 + _vallow_8217;
        if ((long)((unsigned long)_4532 + (unsigned long)HIGH_BITS) >= 0) 
        _4532 = NewDouble((double)_4532);
    }
    else {
        if (IS_ATOM_INT(_4531)) {
            _4532 = NewDouble((double)_4531 + DBL_PTR(_vallow_8217)->dbl);
        }
        else {
            if (IS_ATOM_INT(_vallow_8217)) {
                _4532 = NewDouble(DBL_PTR(_4531)->dbl + (double)_vallow_8217);
            }
            else
            _4532 = NewDouble(DBL_PTR(_4531)->dbl + DBL_PTR(_vallow_8217)->dbl);
        }
    }
    DeRef(_4531);
    _4531 = NOVALUE;
    if (IS_ATOM_INT(_4532)) {
        if (10000000 > 0 && _4532 >= 0) {
            _4534 = _4532 / 10000000;
        }
        else {
            temp_dbl = floor((double)_4532 / (double)10000000);
            if (_4532 != MININT)
            _4534 = (long)temp_dbl;
            else
            _4534 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _4532, 10000000);
        _4534 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_4532);
    _4532 = NOVALUE;
    DeRef(_ptra_8215);
    DeRef(_valhi_8216);
    DeRef(_vallow_8217);
    DeRef(_deltalow_8219);
    return _4534;
    ;
}


int _9gmtime(int _time_8241)
{
    int _ret_8242 = NOVALUE;
    int _timep_8243 = NOVALUE;
    int _tm_p_8244 = NOVALUE;
    int _n_8245 = NOVALUE;
    int _4540 = NOVALUE;
    int _4539 = NOVALUE;
    int _4536 = NOVALUE;
    int _0, _1, _2;
    

    /** 	timep = machine:allocate(4)*/
    _0 = _timep_8243;
    _timep_8243 = _11allocate(4, 0);
    DeRef(_0);

    /** 	poke4(timep, time)*/
    if (IS_ATOM_INT(_timep_8243)){
        poke4_addr = (unsigned long *)_timep_8243;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_timep_8243)->dbl);
    }
    if (IS_ATOM_INT(_time_8241)) {
        *poke4_addr = (unsigned long)_time_8241;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_time_8241)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	tm_p = c_func(gmtime_, {timep})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_timep_8243);
    *((int *)(_2+4)) = _timep_8243;
    _4536 = MAKE_SEQ(_1);
    DeRef(_tm_p_8244);
    _tm_p_8244 = call_c(1, _9gmtime__8191, _4536);
    DeRefDS(_4536);
    _4536 = NOVALUE;

    /** 	machine:free(timep)*/
    Ref(_timep_8243);
    _11free(_timep_8243);

    /** 	ret = repeat(0, 9)*/
    DeRef(_ret_8242);
    _ret_8242 = Repeat(0, 9);

    /** 	n = 0*/
    _n_8245 = 0;

    /** 	for i = 1 to 9 do*/
    {
        int _i_8251;
        _i_8251 = 1;
L1: 
        if (_i_8251 > 9){
            goto L2; // [46] 81
        }

        /** 		ret[i] = peek4s(tm_p+n)*/
        if (IS_ATOM_INT(_tm_p_8244)) {
            _4539 = _tm_p_8244 + _n_8245;
            if ((long)((unsigned long)_4539 + (unsigned long)HIGH_BITS) >= 0) 
            _4539 = NewDouble((double)_4539);
        }
        else {
            _4539 = NewDouble(DBL_PTR(_tm_p_8244)->dbl + (double)_n_8245);
        }
        if (IS_ATOM_INT(_4539)) {
            _4540 = *(unsigned long *)_4539;
            if (_4540 < MININT || _4540 > MAXINT)
            _4540 = NewDouble((double)(long)_4540);
        }
        else {
            _4540 = *(unsigned long *)(unsigned long)(DBL_PTR(_4539)->dbl);
            if (_4540 < MININT || _4540 > MAXINT)
            _4540 = NewDouble((double)(long)_4540);
        }
        DeRef(_4539);
        _4539 = NOVALUE;
        _2 = (int)SEQ_PTR(_ret_8242);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_8242 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8251);
        _1 = *(int *)_2;
        *(int *)_2 = _4540;
        if( _1 != _4540 ){
            DeRef(_1);
        }
        _4540 = NOVALUE;

        /** 		n = n + 4*/
        _n_8245 = _n_8245 + 4;

        /** 	end for*/
        _i_8251 = _i_8251 + 1;
        goto L1; // [76] 53
L2: 
        ;
    }

    /** 	return ret*/
    DeRef(_time_8241);
    DeRef(_timep_8243);
    DeRef(_tm_p_8244);
    return _ret_8242;
    ;
}


int _9tolower(int _x_8268)
{
    int _4553 = NOVALUE;
    int _4552 = NOVALUE;
    int _4551 = NOVALUE;
    int _4550 = NOVALUE;
    int _4549 = NOVALUE;
    int _4548 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return x + (x >= 'A' and x <= 'Z') * ('a' - 'A')*/
    _4548 = binary_op(GREATEREQ, _x_8268, 65);
    _4549 = binary_op(LESSEQ, _x_8268, 90);
    _4550 = binary_op(AND, _4548, _4549);
    DeRefDS(_4548);
    _4548 = NOVALUE;
    DeRefDS(_4549);
    _4549 = NOVALUE;
    _4551 = 32;
    _4552 = binary_op(MULTIPLY, _4550, 32);
    DeRefDS(_4550);
    _4550 = NOVALUE;
    _4551 = NOVALUE;
    _4553 = binary_op(PLUS, _x_8268, _4552);
    DeRefDS(_4552);
    _4552 = NOVALUE;
    DeRefDS(_x_8268);
    return _4553;
    ;
}


int _9isLeap(int _year_8277)
{
    int _ly_8278 = NOVALUE;
    int _4571 = NOVALUE;
    int _4570 = NOVALUE;
    int _4569 = NOVALUE;
    int _4568 = NOVALUE;
    int _4567 = NOVALUE;
    int _4566 = NOVALUE;
    int _4565 = NOVALUE;
    int _4564 = NOVALUE;
    int _4563 = NOVALUE;
    int _4560 = NOVALUE;
    int _4558 = NOVALUE;
    int _4557 = NOVALUE;
    int _0, _1, _2;
    

    /** 		ly = (remainder(year, {4, 100, 400, 3200, 80000})=0)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 4;
    *((int *)(_2+8)) = 100;
    *((int *)(_2+12)) = 400;
    *((int *)(_2+16)) = 3200;
    *((int *)(_2+20)) = 80000;
    _4557 = MAKE_SEQ(_1);
    _4558 = binary_op(REMAINDER, _year_8277, _4557);
    DeRefDS(_4557);
    _4557 = NOVALUE;
    DeRefi(_ly_8278);
    _ly_8278 = binary_op(EQUALS, _4558, 0);
    DeRefDS(_4558);
    _4558 = NOVALUE;

    /** 		if not ly[1] then return 0 end if*/
    _2 = (int)SEQ_PTR(_ly_8278);
    _4560 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4560 != 0)
    goto L1; // [31] 39
    _4560 = NOVALUE;
    DeRefDSi(_ly_8278);
    return 0;
L1: 

    /** 		if year <= Gregorian_Reformation then*/
    if (_year_8277 > 1752)
    goto L2; // [41] 54

    /** 				return 1 -- ly[1] can't possibly be 0 here so set shortcut as '1'.*/
    DeRefi(_ly_8278);
    return 1;
    goto L3; // [51] 97
L2: 

    /** 				return ly[1] - ly[2] + ly[3] - ly[4] + ly[5]*/
    _2 = (int)SEQ_PTR(_ly_8278);
    _4563 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_ly_8278);
    _4564 = (int)*(((s1_ptr)_2)->base + 2);
    _4565 = _4563 - _4564;
    if ((long)((unsigned long)_4565 +(unsigned long) HIGH_BITS) >= 0){
        _4565 = NewDouble((double)_4565);
    }
    _4563 = NOVALUE;
    _4564 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_8278);
    _4566 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_4565)) {
        _4567 = _4565 + _4566;
        if ((long)((unsigned long)_4567 + (unsigned long)HIGH_BITS) >= 0) 
        _4567 = NewDouble((double)_4567);
    }
    else {
        _4567 = NewDouble(DBL_PTR(_4565)->dbl + (double)_4566);
    }
    DeRef(_4565);
    _4565 = NOVALUE;
    _4566 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_8278);
    _4568 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_4567)) {
        _4569 = _4567 - _4568;
        if ((long)((unsigned long)_4569 +(unsigned long) HIGH_BITS) >= 0){
            _4569 = NewDouble((double)_4569);
        }
    }
    else {
        _4569 = NewDouble(DBL_PTR(_4567)->dbl - (double)_4568);
    }
    DeRef(_4567);
    _4567 = NOVALUE;
    _4568 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_8278);
    _4570 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_4569)) {
        _4571 = _4569 + _4570;
        if ((long)((unsigned long)_4571 + (unsigned long)HIGH_BITS) >= 0) 
        _4571 = NewDouble((double)_4571);
    }
    else {
        _4571 = NewDouble(DBL_PTR(_4569)->dbl + (double)_4570);
    }
    DeRef(_4569);
    _4569 = NOVALUE;
    _4570 = NOVALUE;
    DeRefDSi(_ly_8278);
    return _4571;
L3: 
    ;
}


int _9daysInMonth(int _year_8302, int _month_8303)
{
    int _4579 = NOVALUE;
    int _4578 = NOVALUE;
    int _4577 = NOVALUE;
    int _4576 = NOVALUE;
    int _4574 = NOVALUE;
    int _4573 = NOVALUE;
    int _4572 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_month_8303)) {
        _1 = (long)(DBL_PTR(_month_8303)->dbl);
        if (UNIQUE(DBL_PTR(_month_8303)) && (DBL_PTR(_month_8303)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_8303);
        _month_8303 = _1;
    }

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _4572 = (_year_8302 == 1752);
    if (_4572 == 0) {
        goto L1; // [15] 36
    }
    _4574 = (_month_8303 == 9);
    if (_4574 == 0)
    {
        DeRef(_4574);
        _4574 = NOVALUE;
        goto L1; // [24] 36
    }
    else{
        DeRef(_4574);
        _4574 = NOVALUE;
    }

    /** 		return 19*/
    DeRef(_4572);
    _4572 = NOVALUE;
    return 19;
    goto L2; // [33] 74
L1: 

    /** 	elsif month != 2 then*/
    if (_month_8303 == 2)
    goto L3; // [38] 55

    /** 		return DaysPerMonth[month]*/
    _2 = (int)SEQ_PTR(_9DaysPerMonth_8259);
    _4576 = (int)*(((s1_ptr)_2)->base + _month_8303);
    Ref(_4576);
    DeRef(_4572);
    _4572 = NOVALUE;
    return _4576;
    goto L2; // [52] 74
L3: 

    /** 		return DaysPerMonth[month] + isLeap(year)*/
    _2 = (int)SEQ_PTR(_9DaysPerMonth_8259);
    _4577 = (int)*(((s1_ptr)_2)->base + _month_8303);
    _4578 = _9isLeap(_year_8302);
    if (IS_ATOM_INT(_4577) && IS_ATOM_INT(_4578)) {
        _4579 = _4577 + _4578;
        if ((long)((unsigned long)_4579 + (unsigned long)HIGH_BITS) >= 0) 
        _4579 = NewDouble((double)_4579);
    }
    else {
        _4579 = binary_op(PLUS, _4577, _4578);
    }
    _4577 = NOVALUE;
    DeRef(_4578);
    _4578 = NOVALUE;
    DeRef(_4572);
    _4572 = NOVALUE;
    _4576 = NOVALUE;
    return _4579;
L2: 
    ;
}


int _9julianDayOfYear(int _ymd_8326)
{
    int _year_8327 = NOVALUE;
    int _month_8328 = NOVALUE;
    int _day_8329 = NOVALUE;
    int _d_8330 = NOVALUE;
    int _4595 = NOVALUE;
    int _4594 = NOVALUE;
    int _4593 = NOVALUE;
    int _4590 = NOVALUE;
    int _4589 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_8326);
    _year_8327 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_8327)){
        _year_8327 = (long)DBL_PTR(_year_8327)->dbl;
    }

    /** 	month = ymd[2]*/
    _2 = (int)SEQ_PTR(_ymd_8326);
    _month_8328 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_month_8328)){
        _month_8328 = (long)DBL_PTR(_month_8328)->dbl;
    }

    /** 	day = ymd[3]*/
    _2 = (int)SEQ_PTR(_ymd_8326);
    _day_8329 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_day_8329)){
        _day_8329 = (long)DBL_PTR(_day_8329)->dbl;
    }

    /** 	if month = 1 then return day end if*/
    if (_month_8328 != 1)
    goto L1; // [33] 42
    DeRef(_ymd_8326);
    return _day_8329;
L1: 

    /** 	d = 0*/
    _d_8330 = 0;

    /** 	for i = 1 to month - 1 do*/
    _4589 = _month_8328 - 1;
    if ((long)((unsigned long)_4589 +(unsigned long) HIGH_BITS) >= 0){
        _4589 = NewDouble((double)_4589);
    }
    {
        int _i_8337;
        _i_8337 = 1;
L2: 
        if (binary_op_a(GREATER, _i_8337, _4589)){
            goto L3; // [55] 84
        }

        /** 		d += daysInMonth(year, i)*/
        Ref(_i_8337);
        _4590 = _9daysInMonth(_year_8327, _i_8337);
        if (IS_ATOM_INT(_4590)) {
            _d_8330 = _d_8330 + _4590;
        }
        else {
            _d_8330 = binary_op(PLUS, _d_8330, _4590);
        }
        DeRef(_4590);
        _4590 = NOVALUE;
        if (!IS_ATOM_INT(_d_8330)) {
            _1 = (long)(DBL_PTR(_d_8330)->dbl);
            if (UNIQUE(DBL_PTR(_d_8330)) && (DBL_PTR(_d_8330)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_8330);
            _d_8330 = _1;
        }

        /** 	end for*/
        _0 = _i_8337;
        if (IS_ATOM_INT(_i_8337)) {
            _i_8337 = _i_8337 + 1;
            if ((long)((unsigned long)_i_8337 +(unsigned long) HIGH_BITS) >= 0){
                _i_8337 = NewDouble((double)_i_8337);
            }
        }
        else {
            _i_8337 = binary_op_a(PLUS, _i_8337, 1);
        }
        DeRef(_0);
        goto L2; // [79] 62
L3: 
        ;
        DeRef(_i_8337);
    }

    /** 	d += day*/
    _d_8330 = _d_8330 + _day_8329;

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _4593 = (_year_8327 == 1752);
    if (_4593 == 0) {
        goto L4; // [98] 142
    }
    _4595 = (_month_8328 == 9);
    if (_4595 == 0)
    {
        DeRef(_4595);
        _4595 = NOVALUE;
        goto L4; // [107] 142
    }
    else{
        DeRef(_4595);
        _4595 = NOVALUE;
    }

    /** 		if day > 13 then*/
    if (_day_8329 <= 13)
    goto L5; // [112] 127

    /** 			d -= 11*/
    _d_8330 = _d_8330 - 11;
    goto L6; // [124] 141
L5: 

    /** 		elsif day > 2 then*/
    if (_day_8329 <= 2)
    goto L7; // [129] 140

    /** 			return 0*/
    DeRef(_ymd_8326);
    DeRef(_4589);
    _4589 = NOVALUE;
    DeRef(_4593);
    _4593 = NOVALUE;
    return 0;
L7: 
L6: 
L4: 

    /** 	return d*/
    DeRef(_ymd_8326);
    DeRef(_4589);
    _4589 = NOVALUE;
    DeRef(_4593);
    _4593 = NOVALUE;
    return _d_8330;
    ;
}


int _9julianDay(int _ymd_8353)
{
    int _year_8354 = NOVALUE;
    int _j_8355 = NOVALUE;
    int _greg00_8356 = NOVALUE;
    int _4624 = NOVALUE;
    int _4621 = NOVALUE;
    int _4618 = NOVALUE;
    int _4617 = NOVALUE;
    int _4616 = NOVALUE;
    int _4615 = NOVALUE;
    int _4614 = NOVALUE;
    int _4613 = NOVALUE;
    int _4612 = NOVALUE;
    int _4611 = NOVALUE;
    int _4609 = NOVALUE;
    int _4608 = NOVALUE;
    int _4607 = NOVALUE;
    int _4606 = NOVALUE;
    int _4605 = NOVALUE;
    int _4604 = NOVALUE;
    int _4603 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_8353);
    _year_8354 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_8354)){
        _year_8354 = (long)DBL_PTR(_year_8354)->dbl;
    }

    /** 	j = julianDayOfYear(ymd)*/
    Ref(_ymd_8353);
    _j_8355 = _9julianDayOfYear(_ymd_8353);
    if (!IS_ATOM_INT(_j_8355)) {
        _1 = (long)(DBL_PTR(_j_8355)->dbl);
        if (UNIQUE(DBL_PTR(_j_8355)) && (DBL_PTR(_j_8355)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_8355);
        _j_8355 = _1;
    }

    /** 	year  -= 1*/
    _year_8354 = _year_8354 - 1;

    /** 	greg00 = year - Gregorian_Reformation00*/
    _greg00_8356 = _year_8354 - 1700;

    /** 	j += (*/
    if (_year_8354 <= INT15 && _year_8354 >= -INT15)
    _4603 = 365 * _year_8354;
    else
    _4603 = NewDouble(365 * (double)_year_8354);
    if (4 > 0 && _year_8354 >= 0) {
        _4604 = _year_8354 / 4;
    }
    else {
        temp_dbl = floor((double)_year_8354 / (double)4);
        _4604 = (long)temp_dbl;
    }
    if (IS_ATOM_INT(_4603)) {
        _4605 = _4603 + _4604;
        if ((long)((unsigned long)_4605 + (unsigned long)HIGH_BITS) >= 0) 
        _4605 = NewDouble((double)_4605);
    }
    else {
        _4605 = NewDouble(DBL_PTR(_4603)->dbl + (double)_4604);
    }
    DeRef(_4603);
    _4603 = NOVALUE;
    _4604 = NOVALUE;
    _4606 = (_greg00_8356 > 0);
    if (100 > 0 && _greg00_8356 >= 0) {
        _4607 = _greg00_8356 / 100;
    }
    else {
        temp_dbl = floor((double)_greg00_8356 / (double)100);
        _4607 = (long)temp_dbl;
    }
    _4608 = - _4607;
    _4609 = (_greg00_8356 % 400) ? NewDouble((double)_greg00_8356 / 400) : (_greg00_8356 / 400);
    if (IS_ATOM_INT(_4609)) {
        _4611 = NewDouble((double)_4609 + DBL_PTR(_4610)->dbl);
    }
    else {
        _4611 = NewDouble(DBL_PTR(_4609)->dbl + DBL_PTR(_4610)->dbl);
    }
    DeRef(_4609);
    _4609 = NOVALUE;
    _4612 = unary_op(FLOOR, _4611);
    DeRefDS(_4611);
    _4611 = NOVALUE;
    if (IS_ATOM_INT(_4612)) {
        _4613 = _4608 + _4612;
        if ((long)((unsigned long)_4613 + (unsigned long)HIGH_BITS) >= 0) 
        _4613 = NewDouble((double)_4613);
    }
    else {
        _4613 = binary_op(PLUS, _4608, _4612);
    }
    _4608 = NOVALUE;
    DeRef(_4612);
    _4612 = NOVALUE;
    if (IS_ATOM_INT(_4613)) {
        if (_4613 <= INT15 && _4613 >= -INT15)
        _4614 = _4606 * _4613;
        else
        _4614 = NewDouble(_4606 * (double)_4613);
    }
    else {
        _4614 = binary_op(MULTIPLY, _4606, _4613);
    }
    _4606 = NOVALUE;
    DeRef(_4613);
    _4613 = NOVALUE;
    if (IS_ATOM_INT(_4605) && IS_ATOM_INT(_4614)) {
        _4615 = _4605 + _4614;
        if ((long)((unsigned long)_4615 + (unsigned long)HIGH_BITS) >= 0) 
        _4615 = NewDouble((double)_4615);
    }
    else {
        _4615 = binary_op(PLUS, _4605, _4614);
    }
    DeRef(_4605);
    _4605 = NOVALUE;
    DeRef(_4614);
    _4614 = NOVALUE;
    _4616 = (_year_8354 >= 1752);
    _4617 = 11 * _4616;
    _4616 = NOVALUE;
    if (IS_ATOM_INT(_4615)) {
        _4618 = _4615 - _4617;
        if ((long)((unsigned long)_4618 +(unsigned long) HIGH_BITS) >= 0){
            _4618 = NewDouble((double)_4618);
        }
    }
    else {
        _4618 = binary_op(MINUS, _4615, _4617);
    }
    DeRef(_4615);
    _4615 = NOVALUE;
    _4617 = NOVALUE;
    if (IS_ATOM_INT(_4618)) {
        _j_8355 = _j_8355 + _4618;
    }
    else {
        _j_8355 = binary_op(PLUS, _j_8355, _4618);
    }
    DeRef(_4618);
    _4618 = NOVALUE;
    if (!IS_ATOM_INT(_j_8355)) {
        _1 = (long)(DBL_PTR(_j_8355)->dbl);
        if (UNIQUE(DBL_PTR(_j_8355)) && (DBL_PTR(_j_8355)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_8355);
        _j_8355 = _1;
    }

    /** 	if year >= 3200 then*/
    if (_year_8354 < 3200)
    goto L1; // [107] 147

    /** 		j -= floor(year/ 3200)*/
    if (3200 > 0 && _year_8354 >= 0) {
        _4621 = _year_8354 / 3200;
    }
    else {
        temp_dbl = floor((double)_year_8354 / (double)3200);
        _4621 = (long)temp_dbl;
    }
    _j_8355 = _j_8355 - _4621;
    _4621 = NOVALUE;

    /** 		if year >= 80000 then*/
    if (_year_8354 < 80000)
    goto L2; // [127] 146

    /** 			j += floor(year/80000)*/
    if (80000 > 0 && _year_8354 >= 0) {
        _4624 = _year_8354 / 80000;
    }
    else {
        temp_dbl = floor((double)_year_8354 / (double)80000);
        _4624 = (long)temp_dbl;
    }
    _j_8355 = _j_8355 + _4624;
    _4624 = NOVALUE;
L2: 
L1: 

    /** 	return j*/
    DeRef(_ymd_8353);
    DeRef(_4607);
    _4607 = NOVALUE;
    return _j_8355;
    ;
}


int _9datetimeToSeconds(int _dt_8436)
{
    int _4670 = NOVALUE;
    int _4669 = NOVALUE;
    int _4668 = NOVALUE;
    int _4667 = NOVALUE;
    int _4666 = NOVALUE;
    int _4665 = NOVALUE;
    int _4664 = NOVALUE;
    int _4663 = NOVALUE;
    int _4662 = NOVALUE;
    int _4661 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return julianDay(dt) * DayLengthInSeconds + (dt[4] * 60 + dt[5]) * 60 + dt[6]*/
    Ref(_dt_8436);
    _4661 = _9julianDay(_dt_8436);
    if (IS_ATOM_INT(_4661)) {
        _4662 = NewDouble(_4661 * (double)86400);
    }
    else {
        _4662 = binary_op(MULTIPLY, _4661, 86400);
    }
    DeRef(_4661);
    _4661 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_8436);
    _4663 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_4663)) {
        if (_4663 == (short)_4663)
        _4664 = _4663 * 60;
        else
        _4664 = NewDouble(_4663 * (double)60);
    }
    else {
        _4664 = binary_op(MULTIPLY, _4663, 60);
    }
    _4663 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_8436);
    _4665 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_4664) && IS_ATOM_INT(_4665)) {
        _4666 = _4664 + _4665;
        if ((long)((unsigned long)_4666 + (unsigned long)HIGH_BITS) >= 0) 
        _4666 = NewDouble((double)_4666);
    }
    else {
        _4666 = binary_op(PLUS, _4664, _4665);
    }
    DeRef(_4664);
    _4664 = NOVALUE;
    _4665 = NOVALUE;
    if (IS_ATOM_INT(_4666)) {
        if (_4666 == (short)_4666)
        _4667 = _4666 * 60;
        else
        _4667 = NewDouble(_4666 * (double)60);
    }
    else {
        _4667 = binary_op(MULTIPLY, _4666, 60);
    }
    DeRef(_4666);
    _4666 = NOVALUE;
    if (IS_ATOM_INT(_4662) && IS_ATOM_INT(_4667)) {
        _4668 = _4662 + _4667;
        if ((long)((unsigned long)_4668 + (unsigned long)HIGH_BITS) >= 0) 
        _4668 = NewDouble((double)_4668);
    }
    else {
        _4668 = binary_op(PLUS, _4662, _4667);
    }
    DeRef(_4662);
    _4662 = NOVALUE;
    DeRef(_4667);
    _4667 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_8436);
    _4669 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_4668) && IS_ATOM_INT(_4669)) {
        _4670 = _4668 + _4669;
        if ((long)((unsigned long)_4670 + (unsigned long)HIGH_BITS) >= 0) 
        _4670 = NewDouble((double)_4670);
    }
    else {
        _4670 = binary_op(PLUS, _4668, _4669);
    }
    DeRef(_4668);
    _4668 = NOVALUE;
    _4669 = NOVALUE;
    DeRef(_dt_8436);
    return _4670;
    ;
}


int _9from_date(int _src_8612)
{
    int _4795 = NOVALUE;
    int _4794 = NOVALUE;
    int _4793 = NOVALUE;
    int _4792 = NOVALUE;
    int _4791 = NOVALUE;
    int _4790 = NOVALUE;
    int _4789 = NOVALUE;
    int _4787 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {src[YEAR]+1900, src[MONTH], src[DAY], src[HOUR], src[MINUTE], src[SECOND]}*/
    _2 = (int)SEQ_PTR(_src_8612);
    _4787 = (int)*(((s1_ptr)_2)->base + 1);
    _4789 = _4787 + 1900;
    if ((long)((unsigned long)_4789 + (unsigned long)HIGH_BITS) >= 0) 
    _4789 = NewDouble((double)_4789);
    _4787 = NOVALUE;
    _2 = (int)SEQ_PTR(_src_8612);
    _4790 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_src_8612);
    _4791 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_src_8612);
    _4792 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_src_8612);
    _4793 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_src_8612);
    _4794 = (int)*(((s1_ptr)_2)->base + 6);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4789;
    *((int *)(_2+8)) = _4790;
    *((int *)(_2+12)) = _4791;
    *((int *)(_2+16)) = _4792;
    *((int *)(_2+20)) = _4793;
    *((int *)(_2+24)) = _4794;
    _4795 = MAKE_SEQ(_1);
    _4794 = NOVALUE;
    _4793 = NOVALUE;
    _4792 = NOVALUE;
    _4791 = NOVALUE;
    _4790 = NOVALUE;
    _4789 = NOVALUE;
    DeRefDSi(_src_8612);
    return _4795;
    ;
}


int _9now_gmt()
{
    int _t1_8628 = NOVALUE;
    int _4808 = NOVALUE;
    int _4807 = NOVALUE;
    int _4806 = NOVALUE;
    int _4805 = NOVALUE;
    int _4804 = NOVALUE;
    int _4803 = NOVALUE;
    int _4802 = NOVALUE;
    int _4801 = NOVALUE;
    int _4800 = NOVALUE;
    int _4798 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence t1 = gmtime(time())*/
    _4798 = _9time();
    _0 = _t1_8628;
    _t1_8628 = _9gmtime(_4798);
    DeRef(_0);
    _4798 = NOVALUE;

    /** 	return { */
    _2 = (int)SEQ_PTR(_t1_8628);
    _4800 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_4800)) {
        _4801 = _4800 + 1900;
        if ((long)((unsigned long)_4801 + (unsigned long)HIGH_BITS) >= 0) 
        _4801 = NewDouble((double)_4801);
    }
    else {
        _4801 = binary_op(PLUS, _4800, 1900);
    }
    _4800 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_8628);
    _4802 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_4802)) {
        _4803 = _4802 + 1;
        if (_4803 > MAXINT){
            _4803 = NewDouble((double)_4803);
        }
    }
    else
    _4803 = binary_op(PLUS, 1, _4802);
    _4802 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_8628);
    _4804 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_t1_8628);
    _4805 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_t1_8628);
    _4806 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_t1_8628);
    _4807 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4801;
    *((int *)(_2+8)) = _4803;
    Ref(_4804);
    *((int *)(_2+12)) = _4804;
    Ref(_4805);
    *((int *)(_2+16)) = _4805;
    Ref(_4806);
    *((int *)(_2+20)) = _4806;
    Ref(_4807);
    *((int *)(_2+24)) = _4807;
    _4808 = MAKE_SEQ(_1);
    _4807 = NOVALUE;
    _4806 = NOVALUE;
    _4805 = NOVALUE;
    _4804 = NOVALUE;
    _4803 = NOVALUE;
    _4801 = NOVALUE;
    DeRefDS(_t1_8628);
    return _4808;
    ;
}


int _9new(int _year_8642, int _month_8643, int _day_8644, int _hour_8645, int _minute_8646, int _second_8647)
{
    int _d_8648 = NOVALUE;
    int _now_1__tmp_at51_8655 = NOVALUE;
    int _now_inlined_now_at_51_8654 = NOVALUE;
    int _4811 = NOVALUE;
    int _4810 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_year_8642)) {
        _1 = (long)(DBL_PTR(_year_8642)->dbl);
        if (UNIQUE(DBL_PTR(_year_8642)) && (DBL_PTR(_year_8642)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_year_8642);
        _year_8642 = _1;
    }
    if (!IS_ATOM_INT(_month_8643)) {
        _1 = (long)(DBL_PTR(_month_8643)->dbl);
        if (UNIQUE(DBL_PTR(_month_8643)) && (DBL_PTR(_month_8643)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_8643);
        _month_8643 = _1;
    }
    if (!IS_ATOM_INT(_day_8644)) {
        _1 = (long)(DBL_PTR(_day_8644)->dbl);
        if (UNIQUE(DBL_PTR(_day_8644)) && (DBL_PTR(_day_8644)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_day_8644);
        _day_8644 = _1;
    }
    if (!IS_ATOM_INT(_hour_8645)) {
        _1 = (long)(DBL_PTR(_hour_8645)->dbl);
        if (UNIQUE(DBL_PTR(_hour_8645)) && (DBL_PTR(_hour_8645)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hour_8645);
        _hour_8645 = _1;
    }
    if (!IS_ATOM_INT(_minute_8646)) {
        _1 = (long)(DBL_PTR(_minute_8646)->dbl);
        if (UNIQUE(DBL_PTR(_minute_8646)) && (DBL_PTR(_minute_8646)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minute_8646);
        _minute_8646 = _1;
    }

    /** 	d = {year, month, day, hour, minute, second}*/
    _0 = _d_8648;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _year_8642;
    *((int *)(_2+8)) = _month_8643;
    *((int *)(_2+12)) = _day_8644;
    *((int *)(_2+16)) = _hour_8645;
    *((int *)(_2+20)) = _minute_8646;
    Ref(_second_8647);
    *((int *)(_2+24)) = _second_8647;
    _d_8648 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	if equal(d, {0,0,0,0,0,0}) then*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    *((int *)(_2+24)) = 0;
    _4810 = MAKE_SEQ(_1);
    if (_d_8648 == _4810)
    _4811 = 1;
    else if (IS_ATOM_INT(_d_8648) && IS_ATOM_INT(_4810))
    _4811 = 0;
    else
    _4811 = (compare(_d_8648, _4810) == 0);
    DeRefDS(_4810);
    _4810 = NOVALUE;
    if (_4811 == 0)
    {
        _4811 = NOVALUE;
        goto L1; // [47] 70
    }
    else{
        _4811 = NOVALUE;
    }

    /** 		return now()*/

    /** 	return from_date(date())*/
    DeRefi(_now_1__tmp_at51_8655);
    _now_1__tmp_at51_8655 = Date();
    RefDS(_now_1__tmp_at51_8655);
    _0 = _now_inlined_now_at_51_8654;
    _now_inlined_now_at_51_8654 = _9from_date(_now_1__tmp_at51_8655);
    DeRef(_0);
    DeRefi(_now_1__tmp_at51_8655);
    _now_1__tmp_at51_8655 = NOVALUE;
    DeRef(_second_8647);
    DeRef(_d_8648);
    return _now_inlined_now_at_51_8654;
    goto L2; // [67] 77
L1: 

    /** 		return d*/
    DeRef(_second_8647);
    return _d_8648;
L2: 
    ;
}


int _9format(int _d_8708, int _pattern_8709)
{
    int _in_fmt_8711 = NOVALUE;
    int _ch_8712 = NOVALUE;
    int _tmp_8713 = NOVALUE;
    int _res_8714 = NOVALUE;
    int _weeks_day_4__tmp_at73_8730 = NOVALUE;
    int _weeks_day_3__tmp_at73_8729 = NOVALUE;
    int _weeks_day_2__tmp_at73_8728 = NOVALUE;
    int _weeks_day_1__tmp_at73_8727 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_73_8726 = NOVALUE;
    int _weeks_day_4__tmp_at119_8740 = NOVALUE;
    int _weeks_day_3__tmp_at119_8739 = NOVALUE;
    int _weeks_day_2__tmp_at119_8738 = NOVALUE;
    int _weeks_day_1__tmp_at119_8737 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_119_8736 = NOVALUE;
    int _to_unix_1__tmp_at626_8841 = NOVALUE;
    int _to_unix_inlined_to_unix_at_626_8840 = NOVALUE;
    int _weeks_day_4__tmp_at683_8857 = NOVALUE;
    int _weeks_day_3__tmp_at683_8856 = NOVALUE;
    int _weeks_day_2__tmp_at683_8855 = NOVALUE;
    int _weeks_day_1__tmp_at683_8854 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_683_8853 = NOVALUE;
    int _weeks_day_4__tmp_at729_8868 = NOVALUE;
    int _weeks_day_3__tmp_at729_8867 = NOVALUE;
    int _weeks_day_2__tmp_at729_8866 = NOVALUE;
    int _weeks_day_1__tmp_at729_8865 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_729_8864 = NOVALUE;
    int _weeks_day_4__tmp_at778_8879 = NOVALUE;
    int _weeks_day_3__tmp_at778_8878 = NOVALUE;
    int _weeks_day_2__tmp_at778_8877 = NOVALUE;
    int _weeks_day_1__tmp_at778_8876 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_778_8875 = NOVALUE;
    int _4951 = NOVALUE;
    int _4950 = NOVALUE;
    int _4945 = NOVALUE;
    int _4944 = NOVALUE;
    int _4943 = NOVALUE;
    int _4942 = NOVALUE;
    int _4940 = NOVALUE;
    int _4936 = NOVALUE;
    int _4935 = NOVALUE;
    int _4932 = NOVALUE;
    int _4931 = NOVALUE;
    int _4925 = NOVALUE;
    int _4924 = NOVALUE;
    int _4920 = NOVALUE;
    int _4917 = NOVALUE;
    int _4916 = NOVALUE;
    int _4914 = NOVALUE;
    int _4913 = NOVALUE;
    int _4911 = NOVALUE;
    int _4908 = NOVALUE;
    int _4906 = NOVALUE;
    int _4904 = NOVALUE;
    int _4900 = NOVALUE;
    int _4899 = NOVALUE;
    int _4895 = NOVALUE;
    int _4894 = NOVALUE;
    int _4891 = NOVALUE;
    int _4884 = NOVALUE;
    int _4883 = NOVALUE;
    int _4879 = NOVALUE;
    int _4878 = NOVALUE;
    int _4874 = NOVALUE;
    int _4866 = NOVALUE;
    int _4865 = NOVALUE;
    int _4862 = NOVALUE;
    int _4861 = NOVALUE;
    int _4858 = NOVALUE;
    int _4857 = NOVALUE;
    int _4856 = NOVALUE;
    int _4852 = NOVALUE;
    int _4851 = NOVALUE;
    int _4848 = NOVALUE;
    int _4847 = NOVALUE;
    int _4844 = NOVALUE;
    int _4841 = NOVALUE;
    int _4836 = NOVALUE;
    int _0, _1, _2;
    

    /** 	in_fmt = 0*/
    _in_fmt_8711 = 0;

    /** 	res = ""*/
    RefDS(_5);
    DeRef(_res_8714);
    _res_8714 = _5;

    /** 	for i = 1 to length(pattern) do*/
    if (IS_SEQUENCE(_pattern_8709)){
            _4836 = SEQ_PTR(_pattern_8709)->length;
    }
    else {
        _4836 = 1;
    }
    {
        int _i_8716;
        _i_8716 = 1;
L1: 
        if (_i_8716 > _4836){
            goto L2; // [22] 927
        }

        /** 		ch = pattern[i]*/
        _2 = (int)SEQ_PTR(_pattern_8709);
        _ch_8712 = (int)*(((s1_ptr)_2)->base + _i_8716);

        /** 		if in_fmt then*/
        if (_in_fmt_8711 == 0)
        {
            goto L3; // [39] 897
        }
        else{
        }

        /** 			in_fmt = 0*/
        _in_fmt_8711 = 0;

        /** 			if ch = '%' then*/
        if (_ch_8712 != 37)
        goto L4; // [51] 64

        /** 				res &= '%'*/
        Append(&_res_8714, _res_8714, 37);
        goto L5; // [61] 920
L4: 

        /** 			elsif ch = 'a' then*/
        if (_ch_8712 != 97)
        goto L6; // [66] 110

        /** 				res &= day_abbrs[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_8708);
        _0 = _weeks_day_1__tmp_at73_8727;
        _weeks_day_1__tmp_at73_8727 = _9julianDay(_d_8708);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at73_8728);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at73_8727)) {
            _weeks_day_2__tmp_at73_8728 = _weeks_day_1__tmp_at73_8727 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at73_8728 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at73_8728 = NewDouble((double)_weeks_day_2__tmp_at73_8728);
            }
        }
        else {
            _weeks_day_2__tmp_at73_8728 = binary_op(MINUS, _weeks_day_1__tmp_at73_8727, 1);
        }
        DeRef(_weeks_day_3__tmp_at73_8729);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at73_8728)) {
            _weeks_day_3__tmp_at73_8729 = _weeks_day_2__tmp_at73_8728 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at73_8729 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at73_8729 = NewDouble((double)_weeks_day_3__tmp_at73_8729);
        }
        else {
            _weeks_day_3__tmp_at73_8729 = binary_op(PLUS, _weeks_day_2__tmp_at73_8728, 4094);
        }
        DeRef(_weeks_day_4__tmp_at73_8730);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at73_8729)) {
            _weeks_day_4__tmp_at73_8730 = (_weeks_day_3__tmp_at73_8729 % 7);
        }
        else {
            _weeks_day_4__tmp_at73_8730 = binary_op(REMAINDER, _weeks_day_3__tmp_at73_8729, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_73_8726);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at73_8730)) {
            _weeks_day_inlined_weeks_day_at_73_8726 = _weeks_day_4__tmp_at73_8730 + 1;
            if (_weeks_day_inlined_weeks_day_at_73_8726 > MAXINT){
                _weeks_day_inlined_weeks_day_at_73_8726 = NewDouble((double)_weeks_day_inlined_weeks_day_at_73_8726);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_73_8726 = binary_op(PLUS, 1, _weeks_day_4__tmp_at73_8730);
        DeRef(_weeks_day_1__tmp_at73_8727);
        _weeks_day_1__tmp_at73_8727 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at73_8728);
        _weeks_day_2__tmp_at73_8728 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at73_8729);
        _weeks_day_3__tmp_at73_8729 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at73_8730);
        _weeks_day_4__tmp_at73_8730 = NOVALUE;
        _2 = (int)SEQ_PTR(_9day_abbrs_8501);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_73_8726)){
            _4841 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_73_8726)->dbl));
        }
        else{
            _4841 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_73_8726);
        }
        Concat((object_ptr)&_res_8714, _res_8714, _4841);
        _4841 = NOVALUE;
        goto L5; // [107] 920
L6: 

        /** 			elsif ch = 'A' then*/
        if (_ch_8712 != 65)
        goto L7; // [112] 156

        /** 				res &= day_names[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_8708);
        _0 = _weeks_day_1__tmp_at119_8737;
        _weeks_day_1__tmp_at119_8737 = _9julianDay(_d_8708);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at119_8738);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at119_8737)) {
            _weeks_day_2__tmp_at119_8738 = _weeks_day_1__tmp_at119_8737 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at119_8738 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at119_8738 = NewDouble((double)_weeks_day_2__tmp_at119_8738);
            }
        }
        else {
            _weeks_day_2__tmp_at119_8738 = binary_op(MINUS, _weeks_day_1__tmp_at119_8737, 1);
        }
        DeRef(_weeks_day_3__tmp_at119_8739);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at119_8738)) {
            _weeks_day_3__tmp_at119_8739 = _weeks_day_2__tmp_at119_8738 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at119_8739 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at119_8739 = NewDouble((double)_weeks_day_3__tmp_at119_8739);
        }
        else {
            _weeks_day_3__tmp_at119_8739 = binary_op(PLUS, _weeks_day_2__tmp_at119_8738, 4094);
        }
        DeRef(_weeks_day_4__tmp_at119_8740);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at119_8739)) {
            _weeks_day_4__tmp_at119_8740 = (_weeks_day_3__tmp_at119_8739 % 7);
        }
        else {
            _weeks_day_4__tmp_at119_8740 = binary_op(REMAINDER, _weeks_day_3__tmp_at119_8739, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_119_8736);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at119_8740)) {
            _weeks_day_inlined_weeks_day_at_119_8736 = _weeks_day_4__tmp_at119_8740 + 1;
            if (_weeks_day_inlined_weeks_day_at_119_8736 > MAXINT){
                _weeks_day_inlined_weeks_day_at_119_8736 = NewDouble((double)_weeks_day_inlined_weeks_day_at_119_8736);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_119_8736 = binary_op(PLUS, 1, _weeks_day_4__tmp_at119_8740);
        DeRef(_weeks_day_1__tmp_at119_8737);
        _weeks_day_1__tmp_at119_8737 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at119_8738);
        _weeks_day_2__tmp_at119_8738 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at119_8739);
        _weeks_day_3__tmp_at119_8739 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at119_8740);
        _weeks_day_4__tmp_at119_8740 = NOVALUE;
        _2 = (int)SEQ_PTR(_9day_names_8492);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_119_8736)){
            _4844 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_119_8736)->dbl));
        }
        else{
            _4844 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_119_8736);
        }
        Concat((object_ptr)&_res_8714, _res_8714, _4844);
        _4844 = NOVALUE;
        goto L5; // [153] 920
L7: 

        /** 			elsif ch = 'b' then*/
        if (_ch_8712 != 98)
        goto L8; // [158] 183

        /** 				res &= month_abbrs[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4847 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_9month_abbrs_8479);
        if (!IS_ATOM_INT(_4847)){
            _4848 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4847)->dbl));
        }
        else{
            _4848 = (int)*(((s1_ptr)_2)->base + _4847);
        }
        Concat((object_ptr)&_res_8714, _res_8714, _4848);
        _4848 = NOVALUE;
        goto L5; // [180] 920
L8: 

        /** 			elsif ch = 'B' then*/
        if (_ch_8712 != 66)
        goto L9; // [185] 210

        /** 				res &= month_names[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4851 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_9month_names_8465);
        if (!IS_ATOM_INT(_4851)){
            _4852 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4851)->dbl));
        }
        else{
            _4852 = (int)*(((s1_ptr)_2)->base + _4851);
        }
        Concat((object_ptr)&_res_8714, _res_8714, _4852);
        _4852 = NOVALUE;
        goto L5; // [207] 920
L9: 

        /** 			elsif ch = 'C' then*/
        if (_ch_8712 != 67)
        goto LA; // [212] 239

        /** 				res &= sprintf("%02d", d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4856 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_4856)) {
            _4857 = (_4856 % 100) ? NewDouble((double)_4856 / 100) : (_4856 / 100);
        }
        else {
            _4857 = binary_op(DIVIDE, _4856, 100);
        }
        _4856 = NOVALUE;
        _4858 = EPrintf(-9999999, _4855, _4857);
        DeRef(_4857);
        _4857 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4858);
        DeRefDS(_4858);
        _4858 = NOVALUE;
        goto L5; // [236] 920
LA: 

        /** 			elsif ch = 'd' then*/
        if (_ch_8712 != 100)
        goto LB; // [241] 264

        /** 				res &= sprintf("%02d", d[DAY])*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4861 = (int)*(((s1_ptr)_2)->base + 3);
        _4862 = EPrintf(-9999999, _4855, _4861);
        _4861 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4862);
        DeRefDS(_4862);
        _4862 = NOVALUE;
        goto L5; // [261] 920
LB: 

        /** 			elsif ch = 'H' then*/
        if (_ch_8712 != 72)
        goto LC; // [266] 289

        /** 				res &= sprintf("%02d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4865 = (int)*(((s1_ptr)_2)->base + 4);
        _4866 = EPrintf(-9999999, _4855, _4865);
        _4865 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4866);
        DeRefDS(_4866);
        _4866 = NOVALUE;
        goto L5; // [286] 920
LC: 

        /** 			elsif ch = 'I' then*/
        if (_ch_8712 != 73)
        goto LD; // [291] 352

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_8708);
        _tmp_8713 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_8713)){
            _tmp_8713 = (long)DBL_PTR(_tmp_8713)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_8713 <= 12)
        goto LE; // [309] 324

        /** 					tmp -= 12*/
        _tmp_8713 = _tmp_8713 - 12;
        goto LF; // [321] 339
LE: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_8713 != 0)
        goto L10; // [326] 338

        /** 					tmp = 12*/
        _tmp_8713 = 12;
L10: 
LF: 

        /** 				res &= sprintf("%02d", tmp)*/
        _4874 = EPrintf(-9999999, _4855, _tmp_8713);
        Concat((object_ptr)&_res_8714, _res_8714, _4874);
        DeRefDS(_4874);
        _4874 = NOVALUE;
        goto L5; // [349] 920
LD: 

        /** 			elsif ch = 'j' then*/
        if (_ch_8712 != 106)
        goto L11; // [354] 375

        /** 				res &= sprintf("%d", julianDayOfYear(d))*/
        Ref(_d_8708);
        _4878 = _9julianDayOfYear(_d_8708);
        _4879 = EPrintf(-9999999, _106, _4878);
        DeRef(_4878);
        _4878 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4879);
        DeRefDS(_4879);
        _4879 = NOVALUE;
        goto L5; // [372] 920
L11: 

        /** 			elsif ch = 'k' then*/
        if (_ch_8712 != 107)
        goto L12; // [377] 400

        /** 				res &= sprintf("%d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4883 = (int)*(((s1_ptr)_2)->base + 4);
        _4884 = EPrintf(-9999999, _106, _4883);
        _4883 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4884);
        DeRefDS(_4884);
        _4884 = NOVALUE;
        goto L5; // [397] 920
L12: 

        /** 			elsif ch = 'l' then*/
        if (_ch_8712 != 108)
        goto L13; // [402] 463

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_8708);
        _tmp_8713 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_8713)){
            _tmp_8713 = (long)DBL_PTR(_tmp_8713)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_8713 <= 12)
        goto L14; // [420] 435

        /** 					tmp -= 12*/
        _tmp_8713 = _tmp_8713 - 12;
        goto L15; // [432] 450
L14: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_8713 != 0)
        goto L16; // [437] 449

        /** 					tmp = 12*/
        _tmp_8713 = 12;
L16: 
L15: 

        /** 				res &= sprintf("%d", tmp)*/
        _4891 = EPrintf(-9999999, _106, _tmp_8713);
        Concat((object_ptr)&_res_8714, _res_8714, _4891);
        DeRefDS(_4891);
        _4891 = NOVALUE;
        goto L5; // [460] 920
L13: 

        /** 			elsif ch = 'm' then*/
        if (_ch_8712 != 109)
        goto L17; // [465] 488

        /** 				res &= sprintf("%02d", d[MONTH])*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4894 = (int)*(((s1_ptr)_2)->base + 2);
        _4895 = EPrintf(-9999999, _4855, _4894);
        _4894 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4895);
        DeRefDS(_4895);
        _4895 = NOVALUE;
        goto L5; // [485] 920
L17: 

        /** 			elsif ch = 'M' then*/
        if (_ch_8712 != 77)
        goto L18; // [490] 513

        /** 				res &= sprintf("%02d", d[MINUTE])*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4899 = (int)*(((s1_ptr)_2)->base + 5);
        _4900 = EPrintf(-9999999, _4855, _4899);
        _4899 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4900);
        DeRefDS(_4900);
        _4900 = NOVALUE;
        goto L5; // [510] 920
L18: 

        /** 			elsif ch = 'p' then*/
        if (_ch_8712 != 112)
        goto L19; // [515] 562

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4904 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _4904, 12)){
            _4904 = NOVALUE;
            goto L1A; // [527] 546
        }
        _4904 = NOVALUE;

        /** 					res &= ampm[1]*/
        _2 = (int)SEQ_PTR(_9ampm_8510);
        _4906 = (int)*(((s1_ptr)_2)->base + 1);
        Concat((object_ptr)&_res_8714, _res_8714, _4906);
        _4906 = NOVALUE;
        goto L5; // [543] 920
L1A: 

        /** 					res &= ampm[2]*/
        _2 = (int)SEQ_PTR(_9ampm_8510);
        _4908 = (int)*(((s1_ptr)_2)->base + 2);
        Concat((object_ptr)&_res_8714, _res_8714, _4908);
        _4908 = NOVALUE;
        goto L5; // [559] 920
L19: 

        /** 			elsif ch = 'P' then*/
        if (_ch_8712 != 80)
        goto L1B; // [564] 619

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4911 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _4911, 12)){
            _4911 = NOVALUE;
            goto L1C; // [576] 599
        }
        _4911 = NOVALUE;

        /** 					res &= tolower(ampm[1])*/
        _2 = (int)SEQ_PTR(_9ampm_8510);
        _4913 = (int)*(((s1_ptr)_2)->base + 1);
        RefDS(_4913);
        _4914 = _9tolower(_4913);
        _4913 = NOVALUE;
        if (IS_SEQUENCE(_res_8714) && IS_ATOM(_4914)) {
            Ref(_4914);
            Append(&_res_8714, _res_8714, _4914);
        }
        else if (IS_ATOM(_res_8714) && IS_SEQUENCE(_4914)) {
        }
        else {
            Concat((object_ptr)&_res_8714, _res_8714, _4914);
        }
        DeRef(_4914);
        _4914 = NOVALUE;
        goto L5; // [596] 920
L1C: 

        /** 					res &= tolower(ampm[2])*/
        _2 = (int)SEQ_PTR(_9ampm_8510);
        _4916 = (int)*(((s1_ptr)_2)->base + 2);
        RefDS(_4916);
        _4917 = _9tolower(_4916);
        _4916 = NOVALUE;
        if (IS_SEQUENCE(_res_8714) && IS_ATOM(_4917)) {
            Ref(_4917);
            Append(&_res_8714, _res_8714, _4917);
        }
        else if (IS_ATOM(_res_8714) && IS_SEQUENCE(_4917)) {
        }
        else {
            Concat((object_ptr)&_res_8714, _res_8714, _4917);
        }
        DeRef(_4917);
        _4917 = NOVALUE;
        goto L5; // [616] 920
L1B: 

        /** 			elsif ch = 's' then*/
        if (_ch_8712 != 115)
        goto L1D; // [621] 651

        /** 				res &= sprintf("%d", to_unix(d))*/

        /** 	return datetimeToSeconds(dt) - EPOCH_1970*/
        Ref(_d_8708);
        _0 = _to_unix_1__tmp_at626_8841;
        _to_unix_1__tmp_at626_8841 = _9datetimeToSeconds(_d_8708);
        DeRef(_0);
        DeRef(_to_unix_inlined_to_unix_at_626_8840);
        _to_unix_inlined_to_unix_at_626_8840 = binary_op(MINUS, _to_unix_1__tmp_at626_8841, _9EPOCH_1970_8262);
        DeRef(_to_unix_1__tmp_at626_8841);
        _to_unix_1__tmp_at626_8841 = NOVALUE;
        _4920 = EPrintf(-9999999, _106, _to_unix_inlined_to_unix_at_626_8840);
        Concat((object_ptr)&_res_8714, _res_8714, _4920);
        DeRefDS(_4920);
        _4920 = NOVALUE;
        goto L5; // [648] 920
L1D: 

        /** 			elsif ch = 'S' then*/
        if (_ch_8712 != 83)
        goto L1E; // [653] 676

        /** 				res &= sprintf("%02d", d[SECOND])*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4924 = (int)*(((s1_ptr)_2)->base + 6);
        _4925 = EPrintf(-9999999, _4855, _4924);
        _4924 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4925);
        DeRefDS(_4925);
        _4925 = NOVALUE;
        goto L5; // [673] 920
L1E: 

        /** 			elsif ch = 'u' then*/
        if (_ch_8712 != 117)
        goto L1F; // [678] 771

        /** 				tmp = weeks_day(d)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_8708);
        _0 = _weeks_day_1__tmp_at683_8854;
        _weeks_day_1__tmp_at683_8854 = _9julianDay(_d_8708);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at683_8855);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at683_8854)) {
            _weeks_day_2__tmp_at683_8855 = _weeks_day_1__tmp_at683_8854 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at683_8855 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at683_8855 = NewDouble((double)_weeks_day_2__tmp_at683_8855);
            }
        }
        else {
            _weeks_day_2__tmp_at683_8855 = binary_op(MINUS, _weeks_day_1__tmp_at683_8854, 1);
        }
        DeRef(_weeks_day_3__tmp_at683_8856);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at683_8855)) {
            _weeks_day_3__tmp_at683_8856 = _weeks_day_2__tmp_at683_8855 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at683_8856 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at683_8856 = NewDouble((double)_weeks_day_3__tmp_at683_8856);
        }
        else {
            _weeks_day_3__tmp_at683_8856 = binary_op(PLUS, _weeks_day_2__tmp_at683_8855, 4094);
        }
        DeRef(_weeks_day_4__tmp_at683_8857);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at683_8856)) {
            _weeks_day_4__tmp_at683_8857 = (_weeks_day_3__tmp_at683_8856 % 7);
        }
        else {
            _weeks_day_4__tmp_at683_8857 = binary_op(REMAINDER, _weeks_day_3__tmp_at683_8856, 7);
        }
        if (IS_ATOM_INT(_weeks_day_4__tmp_at683_8857)) {
            _tmp_8713 = _weeks_day_4__tmp_at683_8857 + 1;
        }
        else
        { // coercing _tmp_8713 to an integer 1
            _tmp_8713 = binary_op(PLUS, 1, _weeks_day_4__tmp_at683_8857);
            if( !IS_ATOM_INT(_tmp_8713) ){
                _tmp_8713 = (object)DBL_PTR(_tmp_8713)->dbl;
            }
        }
        DeRef(_weeks_day_1__tmp_at683_8854);
        _weeks_day_1__tmp_at683_8854 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at683_8855);
        _weeks_day_2__tmp_at683_8855 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at683_8856);
        _weeks_day_3__tmp_at683_8856 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at683_8857);
        _weeks_day_4__tmp_at683_8857 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_8713)) {
            _1 = (long)(DBL_PTR(_tmp_8713)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_8713)) && (DBL_PTR(_tmp_8713)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_8713);
            _tmp_8713 = _1;
        }

        /** 				if tmp = 1 then*/
        if (_tmp_8713 != 1)
        goto L20; // [715] 728

        /** 					res &= "7" -- Sunday*/
        Concat((object_ptr)&_res_8714, _res_8714, _4929);
        goto L5; // [725] 920
L20: 

        /** 					res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_8708);
        _0 = _weeks_day_1__tmp_at729_8865;
        _weeks_day_1__tmp_at729_8865 = _9julianDay(_d_8708);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at729_8866);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at729_8865)) {
            _weeks_day_2__tmp_at729_8866 = _weeks_day_1__tmp_at729_8865 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at729_8866 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at729_8866 = NewDouble((double)_weeks_day_2__tmp_at729_8866);
            }
        }
        else {
            _weeks_day_2__tmp_at729_8866 = binary_op(MINUS, _weeks_day_1__tmp_at729_8865, 1);
        }
        DeRef(_weeks_day_3__tmp_at729_8867);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at729_8866)) {
            _weeks_day_3__tmp_at729_8867 = _weeks_day_2__tmp_at729_8866 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at729_8867 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at729_8867 = NewDouble((double)_weeks_day_3__tmp_at729_8867);
        }
        else {
            _weeks_day_3__tmp_at729_8867 = binary_op(PLUS, _weeks_day_2__tmp_at729_8866, 4094);
        }
        DeRef(_weeks_day_4__tmp_at729_8868);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at729_8867)) {
            _weeks_day_4__tmp_at729_8868 = (_weeks_day_3__tmp_at729_8867 % 7);
        }
        else {
            _weeks_day_4__tmp_at729_8868 = binary_op(REMAINDER, _weeks_day_3__tmp_at729_8867, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_729_8864);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at729_8868)) {
            _weeks_day_inlined_weeks_day_at_729_8864 = _weeks_day_4__tmp_at729_8868 + 1;
            if (_weeks_day_inlined_weeks_day_at_729_8864 > MAXINT){
                _weeks_day_inlined_weeks_day_at_729_8864 = NewDouble((double)_weeks_day_inlined_weeks_day_at_729_8864);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_729_8864 = binary_op(PLUS, 1, _weeks_day_4__tmp_at729_8868);
        DeRef(_weeks_day_1__tmp_at729_8865);
        _weeks_day_1__tmp_at729_8865 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at729_8866);
        _weeks_day_2__tmp_at729_8866 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at729_8867);
        _weeks_day_3__tmp_at729_8867 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at729_8868);
        _weeks_day_4__tmp_at729_8868 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_729_8864)) {
            _4931 = _weeks_day_inlined_weeks_day_at_729_8864 - 1;
            if ((long)((unsigned long)_4931 +(unsigned long) HIGH_BITS) >= 0){
                _4931 = NewDouble((double)_4931);
            }
        }
        else {
            _4931 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_729_8864, 1);
        }
        _4932 = EPrintf(-9999999, _106, _4931);
        DeRef(_4931);
        _4931 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4932);
        DeRefDS(_4932);
        _4932 = NOVALUE;
        goto L5; // [768] 920
L1F: 

        /** 			elsif ch = 'w' then*/
        if (_ch_8712 != 119)
        goto L21; // [773] 819

        /** 				res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_8708);
        _0 = _weeks_day_1__tmp_at778_8876;
        _weeks_day_1__tmp_at778_8876 = _9julianDay(_d_8708);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at778_8877);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at778_8876)) {
            _weeks_day_2__tmp_at778_8877 = _weeks_day_1__tmp_at778_8876 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at778_8877 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at778_8877 = NewDouble((double)_weeks_day_2__tmp_at778_8877);
            }
        }
        else {
            _weeks_day_2__tmp_at778_8877 = binary_op(MINUS, _weeks_day_1__tmp_at778_8876, 1);
        }
        DeRef(_weeks_day_3__tmp_at778_8878);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at778_8877)) {
            _weeks_day_3__tmp_at778_8878 = _weeks_day_2__tmp_at778_8877 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at778_8878 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at778_8878 = NewDouble((double)_weeks_day_3__tmp_at778_8878);
        }
        else {
            _weeks_day_3__tmp_at778_8878 = binary_op(PLUS, _weeks_day_2__tmp_at778_8877, 4094);
        }
        DeRef(_weeks_day_4__tmp_at778_8879);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at778_8878)) {
            _weeks_day_4__tmp_at778_8879 = (_weeks_day_3__tmp_at778_8878 % 7);
        }
        else {
            _weeks_day_4__tmp_at778_8879 = binary_op(REMAINDER, _weeks_day_3__tmp_at778_8878, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_778_8875);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at778_8879)) {
            _weeks_day_inlined_weeks_day_at_778_8875 = _weeks_day_4__tmp_at778_8879 + 1;
            if (_weeks_day_inlined_weeks_day_at_778_8875 > MAXINT){
                _weeks_day_inlined_weeks_day_at_778_8875 = NewDouble((double)_weeks_day_inlined_weeks_day_at_778_8875);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_778_8875 = binary_op(PLUS, 1, _weeks_day_4__tmp_at778_8879);
        DeRef(_weeks_day_1__tmp_at778_8876);
        _weeks_day_1__tmp_at778_8876 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at778_8877);
        _weeks_day_2__tmp_at778_8877 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at778_8878);
        _weeks_day_3__tmp_at778_8878 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at778_8879);
        _weeks_day_4__tmp_at778_8879 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_778_8875)) {
            _4935 = _weeks_day_inlined_weeks_day_at_778_8875 - 1;
            if ((long)((unsigned long)_4935 +(unsigned long) HIGH_BITS) >= 0){
                _4935 = NewDouble((double)_4935);
            }
        }
        else {
            _4935 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_778_8875, 1);
        }
        _4936 = EPrintf(-9999999, _106, _4935);
        DeRef(_4935);
        _4935 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4936);
        DeRefDS(_4936);
        _4936 = NOVALUE;
        goto L5; // [816] 920
L21: 

        /** 			elsif ch = 'y' then*/
        if (_ch_8712 != 121)
        goto L22; // [821] 868

        /** 			   tmp = floor(d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4940 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_4940)) {
            if (100 > 0 && _4940 >= 0) {
                _tmp_8713 = _4940 / 100;
            }
            else {
                temp_dbl = floor((double)_4940 / (double)100);
                _tmp_8713 = (long)temp_dbl;
            }
        }
        else {
            _2 = binary_op(DIVIDE, _4940, 100);
            _tmp_8713 = unary_op(FLOOR, _2);
            DeRef(_2);
        }
        _4940 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_8713)) {
            _1 = (long)(DBL_PTR(_tmp_8713)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_8713)) && (DBL_PTR(_tmp_8713)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_8713);
            _tmp_8713 = _1;
        }

        /** 			   res &= sprintf("%02d", d[YEAR] - (tmp * 100))*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4942 = (int)*(((s1_ptr)_2)->base + 1);
        if (_tmp_8713 == (short)_tmp_8713)
        _4943 = _tmp_8713 * 100;
        else
        _4943 = NewDouble(_tmp_8713 * (double)100);
        if (IS_ATOM_INT(_4942) && IS_ATOM_INT(_4943)) {
            _4944 = _4942 - _4943;
            if ((long)((unsigned long)_4944 +(unsigned long) HIGH_BITS) >= 0){
                _4944 = NewDouble((double)_4944);
            }
        }
        else {
            _4944 = binary_op(MINUS, _4942, _4943);
        }
        _4942 = NOVALUE;
        DeRef(_4943);
        _4943 = NOVALUE;
        _4945 = EPrintf(-9999999, _4855, _4944);
        DeRef(_4944);
        _4944 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4945);
        DeRefDS(_4945);
        _4945 = NOVALUE;
        goto L5; // [865] 920
L22: 

        /** 			elsif ch = 'Y' then*/
        if (_ch_8712 != 89)
        goto L5; // [870] 920

        /** 				res &= sprintf("%04d", d[YEAR])*/
        _2 = (int)SEQ_PTR(_d_8708);
        _4950 = (int)*(((s1_ptr)_2)->base + 1);
        _4951 = EPrintf(-9999999, _4949, _4950);
        _4950 = NOVALUE;
        Concat((object_ptr)&_res_8714, _res_8714, _4951);
        DeRefDS(_4951);
        _4951 = NOVALUE;
        goto L5; // [890] 920
        goto L5; // [894] 920
L3: 

        /** 		elsif ch = '%' then*/
        if (_ch_8712 != 37)
        goto L23; // [899] 913

        /** 			in_fmt = 1*/
        _in_fmt_8711 = 1;
        goto L5; // [910] 920
L23: 

        /** 			res &= ch*/
        Append(&_res_8714, _res_8714, _ch_8712);
L5: 

        /** 	end for*/
        _i_8716 = _i_8716 + 1;
        goto L1; // [922] 29
L2: 
        ;
    }

    /** 	return res*/
    DeRef(_d_8708);
    DeRefDSi(_pattern_8709);
    _4847 = NOVALUE;
    _4851 = NOVALUE;
    return _res_8714;
    ;
}



// 0x7EAA91AE
