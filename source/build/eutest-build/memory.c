// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _13positive_int(int _x_3325)
{
    int _1789 = NOVALUE;
    int _1787 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(x) then*/
    if (IS_ATOM_INT(_x_3325))
    _1787 = 1;
    else if (IS_ATOM_DBL(_x_3325))
    _1787 = IS_ATOM_INT(DoubleToInt(_x_3325));
    else
    _1787 = 0;
    if (_1787 != 0)
    goto L1; // [6] 16
    _1787 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_3325);
    return 0;
L1: 

    /**     return x >= 1*/
    if (IS_ATOM_INT(_x_3325)) {
        _1789 = (_x_3325 >= 1);
    }
    else {
        _1789 = binary_op(GREATEREQ, _x_3325, 1);
    }
    DeRef(_x_3325);
    return _1789;
    ;
}


int _13machine_addr(int _a_3332)
{
    int _1798 = NOVALUE;
    int _1797 = NOVALUE;
    int _1796 = NOVALUE;
    int _1794 = NOVALUE;
    int _1792 = NOVALUE;
    int _1790 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(a) then*/
    _1790 = IS_ATOM(_a_3332);
    if (_1790 != 0)
    goto L1; // [6] 16
    _1790 = NOVALUE;

    /** 		return 0*/
    DeRef(_a_3332);
    return 0;
L1: 

    /** 	if not integer(a)then*/
    if (IS_ATOM_INT(_a_3332))
    _1792 = 1;
    else if (IS_ATOM_DBL(_a_3332))
    _1792 = IS_ATOM_INT(DoubleToInt(_a_3332));
    else
    _1792 = 0;
    if (_1792 != 0)
    goto L2; // [21] 41
    _1792 = NOVALUE;

    /** 		if floor(a) != a then*/
    if (IS_ATOM_INT(_a_3332))
    _1794 = e_floor(_a_3332);
    else
    _1794 = unary_op(FLOOR, _a_3332);
    if (binary_op_a(EQUALS, _1794, _a_3332)){
        DeRef(_1794);
        _1794 = NOVALUE;
        goto L3; // [29] 40
    }
    DeRef(_1794);
    _1794 = NOVALUE;

    /** 			return 0*/
    DeRef(_a_3332);
    return 0;
L3: 
L2: 

    /** 	return a > 0 and a <= MAX_ADDR*/
    if (IS_ATOM_INT(_a_3332)) {
        _1796 = (_a_3332 > 0);
    }
    else {
        _1796 = binary_op(GREATER, _a_3332, 0);
    }
    if (IS_ATOM_INT(_a_3332) && IS_ATOM_INT(_13MAX_ADDR_3318)) {
        _1797 = (_a_3332 <= _13MAX_ADDR_3318);
    }
    else {
        _1797 = binary_op(LESSEQ, _a_3332, _13MAX_ADDR_3318);
    }
    if (IS_ATOM_INT(_1796) && IS_ATOM_INT(_1797)) {
        _1798 = (_1796 != 0 && _1797 != 0);
    }
    else {
        _1798 = binary_op(AND, _1796, _1797);
    }
    DeRef(_1796);
    _1796 = NOVALUE;
    DeRef(_1797);
    _1797 = NOVALUE;
    DeRef(_a_3332);
    return _1798;
    ;
}


void _13deallocate(int _addr_3347)
{
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_3347);

    /** end procedure*/
    DeRef(_addr_3347);
    return;
    ;
}


void _13register_block(int _block_addr_3354, int _block_len_3355, int _protection_3356)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_protection_3356)) {
        _1 = (long)(DBL_PTR(_protection_3356)->dbl);
        if (UNIQUE(DBL_PTR(_protection_3356)) && (DBL_PTR(_protection_3356)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_3356);
        _protection_3356 = _1;
    }

    /** end procedure*/
    return;
    ;
}


void _13unregister_block(int _block_addr_3360)
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _13safe_address(int _start_3364, int _len_3365, int _action_3366)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_len_3365)) {
        _1 = (long)(DBL_PTR(_len_3365)->dbl);
        if (UNIQUE(DBL_PTR(_len_3365)) && (DBL_PTR(_len_3365)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_3365);
        _len_3365 = _1;
    }

    /** 	return 1*/
    return 1;
    ;
}


void _13check_all_blocks()
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _13prepare_block(int _addr_3373, int _a_3374, int _protection_3375)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_3374)) {
        _1 = (long)(DBL_PTR(_a_3374)->dbl);
        if (UNIQUE(DBL_PTR(_a_3374)) && (DBL_PTR(_a_3374)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_3374);
        _a_3374 = _1;
    }
    if (!IS_ATOM_INT(_protection_3375)) {
        _1 = (long)(DBL_PTR(_protection_3375)->dbl);
        if (UNIQUE(DBL_PTR(_protection_3375)) && (DBL_PTR(_protection_3375)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_3375);
        _protection_3375 = _1;
    }

    /** 	return addr*/
    return _addr_3373;
    ;
}


int _13bordered_address(int _addr_3383)
{
    int _1803 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(addr) then*/
    _1803 = IS_ATOM(_addr_3383);
    if (_1803 != 0)
    goto L1; // [6] 16
    _1803 = NOVALUE;

    /** 		return 0*/
    DeRef(_addr_3383);
    return 0;
L1: 

    /** 	return 1*/
    DeRef(_addr_3383);
    return 1;
    ;
}


int _13dep_works()
{
    int _1805 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return (DEP_really_works and use_DEP)*/
    _1805 = (_12DEP_really_works_3290 != 0 && 1 != 0);
    return _1805;

    /** 	return 1*/
    _1805 = NOVALUE;
    return 1;
    ;
}


void _13free_code(int _addr_3395, int _size_3396, int _wordsize_3398)
{
    int _1809 = NOVALUE;
    int _1808 = NOVALUE;
    int _1807 = NOVALUE;
    int _1806 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_3396)) {
        _1 = (long)(DBL_PTR(_size_3396)->dbl);
        if (UNIQUE(DBL_PTR(_size_3396)) && (DBL_PTR(_size_3396)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_3396);
        _size_3396 = _1;
    }

    /** 		if dep_works() then*/
    _1806 = _13dep_works();
    if (_1806 == 0) {
        DeRef(_1806);
        _1806 = NOVALUE;
        goto L1; // [10] 37
    }
    else {
        if (!IS_ATOM_INT(_1806) && DBL_PTR(_1806)->dbl == 0.0){
            DeRef(_1806);
            _1806 = NOVALUE;
            goto L1; // [10] 37
        }
        DeRef(_1806);
        _1806 = NOVALUE;
    }
    DeRef(_1806);
    _1806 = NOVALUE;

    /** 			c_func(VirtualFree_rid, { addr, size*wordsize, MEM_RELEASE })*/
    if (IS_ATOM_INT(_wordsize_3398)) {
        if (_size_3396 == (short)_size_3396 && _wordsize_3398 <= INT15 && _wordsize_3398 >= -INT15)
        _1807 = _size_3396 * _wordsize_3398;
        else
        _1807 = NewDouble(_size_3396 * (double)_wordsize_3398);
    }
    else {
        _1807 = binary_op(MULTIPLY, _size_3396, _wordsize_3398);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_3395);
    *((int *)(_2+4)) = _addr_3395;
    *((int *)(_2+8)) = _1807;
    *((int *)(_2+12)) = 32768;
    _1808 = MAKE_SEQ(_1);
    _1807 = NOVALUE;
    _1809 = call_c(1, _13VirtualFree_rid_3392, _1808);
    DeRefDS(_1808);
    _1808 = NOVALUE;
    goto L2; // [34] 43
L1: 

    /** 			machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_3395);
L2: 

    /** end procedure*/
    DeRef(_addr_3395);
    DeRef(_wordsize_3398);
    DeRef(_1809);
    _1809 = NOVALUE;
    return;
    ;
}



// 0x3083F7DD
