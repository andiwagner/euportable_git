// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _19rand_range(int _lo_4319, int _hi_4320)
{
    int _temp_4323 = NOVALUE;
    int _2225 = NOVALUE;
    int _2223 = NOVALUE;
    int _2220 = NOVALUE;
    int _2219 = NOVALUE;
    int _2218 = NOVALUE;
    int _2217 = NOVALUE;
    int _2215 = NOVALUE;
    int _2214 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if lo > hi then*/
    if (binary_op_a(LESSEQ, _lo_4319, _hi_4320)){
        goto L1; // [3] 23
    }

    /** 		atom temp = hi*/
    Ref(_hi_4320);
    DeRef(_temp_4323);
    _temp_4323 = _hi_4320;

    /** 		hi = lo*/
    Ref(_lo_4319);
    DeRef(_hi_4320);
    _hi_4320 = _lo_4319;

    /** 		lo = temp*/
    Ref(_temp_4323);
    DeRef(_lo_4319);
    _lo_4319 = _temp_4323;
L1: 
    DeRef(_temp_4323);
    _temp_4323 = NOVALUE;

    /** 	if not integer(lo) or not integer(hi) then*/
    if (IS_ATOM_INT(_lo_4319))
    _2214 = 1;
    else if (IS_ATOM_DBL(_lo_4319))
    _2214 = IS_ATOM_INT(DoubleToInt(_lo_4319));
    else
    _2214 = 0;
    _2215 = (_2214 == 0);
    _2214 = NOVALUE;
    if (_2215 != 0) {
        goto L2; // [33] 48
    }
    if (IS_ATOM_INT(_hi_4320))
    _2217 = 1;
    else if (IS_ATOM_DBL(_hi_4320))
    _2217 = IS_ATOM_INT(DoubleToInt(_hi_4320));
    else
    _2217 = 0;
    _2218 = (_2217 == 0);
    _2217 = NOVALUE;
    if (_2218 == 0)
    {
        DeRef(_2218);
        _2218 = NOVALUE;
        goto L3; // [44] 64
    }
    else{
        DeRef(_2218);
        _2218 = NOVALUE;
    }
L2: 

    /**    		hi = rnd() * (hi - lo)*/
    _2219 = _19rnd();
    if (IS_ATOM_INT(_hi_4320) && IS_ATOM_INT(_lo_4319)) {
        _2220 = _hi_4320 - _lo_4319;
        if ((long)((unsigned long)_2220 +(unsigned long) HIGH_BITS) >= 0){
            _2220 = NewDouble((double)_2220);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_4320)) {
            _2220 = NewDouble((double)_hi_4320 - DBL_PTR(_lo_4319)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_4319)) {
                _2220 = NewDouble(DBL_PTR(_hi_4320)->dbl - (double)_lo_4319);
            }
            else
            _2220 = NewDouble(DBL_PTR(_hi_4320)->dbl - DBL_PTR(_lo_4319)->dbl);
        }
    }
    DeRef(_hi_4320);
    if (IS_ATOM_INT(_2219) && IS_ATOM_INT(_2220)) {
        if (_2219 == (short)_2219 && _2220 <= INT15 && _2220 >= -INT15)
        _hi_4320 = _2219 * _2220;
        else
        _hi_4320 = NewDouble(_2219 * (double)_2220);
    }
    else {
        _hi_4320 = binary_op(MULTIPLY, _2219, _2220);
    }
    DeRef(_2219);
    _2219 = NOVALUE;
    DeRef(_2220);
    _2220 = NOVALUE;
    goto L4; // [61] 80
L3: 

    /** 		lo -= 1*/
    _0 = _lo_4319;
    if (IS_ATOM_INT(_lo_4319)) {
        _lo_4319 = _lo_4319 - 1;
        if ((long)((unsigned long)_lo_4319 +(unsigned long) HIGH_BITS) >= 0){
            _lo_4319 = NewDouble((double)_lo_4319);
        }
    }
    else {
        _lo_4319 = NewDouble(DBL_PTR(_lo_4319)->dbl - (double)1);
    }
    DeRef(_0);

    /**    		hi = rand(hi - lo)*/
    if (IS_ATOM_INT(_hi_4320) && IS_ATOM_INT(_lo_4319)) {
        _2223 = _hi_4320 - _lo_4319;
        if ((long)((unsigned long)_2223 +(unsigned long) HIGH_BITS) >= 0){
            _2223 = NewDouble((double)_2223);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_4320)) {
            _2223 = NewDouble((double)_hi_4320 - DBL_PTR(_lo_4319)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_4319)) {
                _2223 = NewDouble(DBL_PTR(_hi_4320)->dbl - (double)_lo_4319);
            }
            else
            _2223 = NewDouble(DBL_PTR(_hi_4320)->dbl - DBL_PTR(_lo_4319)->dbl);
        }
    }
    DeRef(_hi_4320);
    if (IS_ATOM_INT(_2223)) {
        _hi_4320 = good_rand() % ((unsigned)_2223) + 1;
    }
    else {
        _hi_4320 = unary_op(RAND, _2223);
    }
    DeRef(_2223);
    _2223 = NOVALUE;
L4: 

    /**    	return lo + hi*/
    if (IS_ATOM_INT(_lo_4319) && IS_ATOM_INT(_hi_4320)) {
        _2225 = _lo_4319 + _hi_4320;
        if ((long)((unsigned long)_2225 + (unsigned long)HIGH_BITS) >= 0) 
        _2225 = NewDouble((double)_2225);
    }
    else {
        if (IS_ATOM_INT(_lo_4319)) {
            _2225 = NewDouble((double)_lo_4319 + DBL_PTR(_hi_4320)->dbl);
        }
        else {
            if (IS_ATOM_INT(_hi_4320)) {
                _2225 = NewDouble(DBL_PTR(_lo_4319)->dbl + (double)_hi_4320);
            }
            else
            _2225 = NewDouble(DBL_PTR(_lo_4319)->dbl + DBL_PTR(_hi_4320)->dbl);
        }
    }
    DeRef(_lo_4319);
    DeRef(_hi_4320);
    DeRef(_2215);
    _2215 = NOVALUE;
    return _2225;
    ;
}


int _19rnd()
{
    int _a_4343 = NOVALUE;
    int _b_4344 = NOVALUE;
    int _r_4345 = NOVALUE;
    int _0, _1, _2;
    

    /** 	 a = rand(#FFFFFFFF)*/
    DeRef(_a_4343);
    _a_4343 = unary_op(RAND, _2226);

    /** 	 if a = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _a_4343, 1)){
        goto L1; // [8] 17
    }
    DeRef(_a_4343);
    DeRef(_b_4344);
    DeRef(_r_4345);
    return 0;
L1: 

    /** 	 b = rand(#FFFFFFFF)*/
    DeRef(_b_4344);
    _b_4344 = unary_op(RAND, _2226);

    /** 	 if b = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _b_4344, 1)){
        goto L2; // [24] 33
    }
    DeRef(_a_4343);
    DeRef(_b_4344);
    DeRef(_r_4345);
    return 0;
L2: 

    /** 	 if a > b then*/
    if (binary_op_a(LESSEQ, _a_4343, _b_4344)){
        goto L3; // [35] 48
    }

    /** 	 	r = b / a*/
    DeRef(_r_4345);
    if (IS_ATOM_INT(_b_4344) && IS_ATOM_INT(_a_4343)) {
        _r_4345 = (_b_4344 % _a_4343) ? NewDouble((double)_b_4344 / _a_4343) : (_b_4344 / _a_4343);
    }
    else {
        if (IS_ATOM_INT(_b_4344)) {
            _r_4345 = NewDouble((double)_b_4344 / DBL_PTR(_a_4343)->dbl);
        }
        else {
            if (IS_ATOM_INT(_a_4343)) {
                _r_4345 = NewDouble(DBL_PTR(_b_4344)->dbl / (double)_a_4343);
            }
            else
            _r_4345 = NewDouble(DBL_PTR(_b_4344)->dbl / DBL_PTR(_a_4343)->dbl);
        }
    }
    goto L4; // [45] 55
L3: 

    /** 	 	r = a / b*/
    DeRef(_r_4345);
    if (IS_ATOM_INT(_a_4343) && IS_ATOM_INT(_b_4344)) {
        _r_4345 = (_a_4343 % _b_4344) ? NewDouble((double)_a_4343 / _b_4344) : (_a_4343 / _b_4344);
    }
    else {
        if (IS_ATOM_INT(_a_4343)) {
            _r_4345 = NewDouble((double)_a_4343 / DBL_PTR(_b_4344)->dbl);
        }
        else {
            if (IS_ATOM_INT(_b_4344)) {
                _r_4345 = NewDouble(DBL_PTR(_a_4343)->dbl / (double)_b_4344);
            }
            else
            _r_4345 = NewDouble(DBL_PTR(_a_4343)->dbl / DBL_PTR(_b_4344)->dbl);
        }
    }
L4: 

    /** 	 return r*/
    DeRef(_a_4343);
    DeRef(_b_4344);
    return _r_4345;
    ;
}


int _19rnd_1()
{
    int _r_4360 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while r >= 1.0 with entry do*/
    goto L1; // [3] 15
L2: 
    if (binary_op_a(LESS, _r_4360, _2234)){
        goto L3; // [8] 25
    }

    /** 	entry*/
L1: 

    /** 		r = rnd()*/
    _0 = _r_4360;
    _r_4360 = _19rnd();
    DeRef(_0);

    /** 	end while	 */
    goto L2; // [22] 6
L3: 

    /** 	return r*/
    return _r_4360;
    ;
}


void _19set_rand(int _seed_4367)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_SET_RAND, seed)*/
    machine(35, _seed_4367);

    /** end procedure*/
    DeRef(_seed_4367);
    return;
    ;
}


int _19get_rand()
{
    int _2237 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_GET_RAND, {})*/
    _2237 = machine(98, _5);
    return _2237;
    ;
}


int _19chance(int _my_limit_4373, int _top_limit_4374)
{
    int _2240 = NOVALUE;
    int _2239 = NOVALUE;
    int _2238 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return (rnd_1() * top_limit) <= my_limit*/
    _2238 = _19rnd_1();
    if (IS_ATOM_INT(_2238) && IS_ATOM_INT(_top_limit_4374)) {
        if (_2238 == (short)_2238 && _top_limit_4374 <= INT15 && _top_limit_4374 >= -INT15)
        _2239 = _2238 * _top_limit_4374;
        else
        _2239 = NewDouble(_2238 * (double)_top_limit_4374);
    }
    else {
        _2239 = binary_op(MULTIPLY, _2238, _top_limit_4374);
    }
    DeRef(_2238);
    _2238 = NOVALUE;
    if (IS_ATOM_INT(_2239) && IS_ATOM_INT(_my_limit_4373)) {
        _2240 = (_2239 <= _my_limit_4373);
    }
    else {
        _2240 = binary_op(LESSEQ, _2239, _my_limit_4373);
    }
    DeRef(_2239);
    _2239 = NOVALUE;
    DeRef(_my_limit_4373);
    DeRef(_top_limit_4374);
    return _2240;
    ;
}


int _19roll(int _desired_4380, int _sides_4381)
{
    int _rolled_4382 = NOVALUE;
    int _2245 = NOVALUE;
    int _2242 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sides_4381)) {
        _1 = (long)(DBL_PTR(_sides_4381)->dbl);
        if (UNIQUE(DBL_PTR(_sides_4381)) && (DBL_PTR(_sides_4381)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sides_4381);
        _sides_4381 = _1;
    }

    /** 	if sides < 2 then*/
    if (_sides_4381 >= 2)
    goto L1; // [7] 18

    /** 		return 0*/
    DeRef(_desired_4380);
    return 0;
L1: 

    /** 	if atom(desired) then*/
    _2242 = IS_ATOM(_desired_4380);
    if (_2242 == 0)
    {
        _2242 = NOVALUE;
        goto L2; // [23] 33
    }
    else{
        _2242 = NOVALUE;
    }

    /** 		desired = {desired}*/
    _0 = _desired_4380;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_desired_4380);
    *((int *)(_2+4)) = _desired_4380;
    _desired_4380 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	rolled =  rand(sides)*/
    _rolled_4382 = good_rand() % ((unsigned)_sides_4381) + 1;

    /** 	if find(rolled, desired) then*/
    _2245 = find_from(_rolled_4382, _desired_4380, 1);
    if (_2245 == 0)
    {
        _2245 = NOVALUE;
        goto L3; // [47] 59
    }
    else{
        _2245 = NOVALUE;
    }

    /** 		return rolled*/
    DeRef(_desired_4380);
    return _rolled_4382;
    goto L4; // [56] 66
L3: 

    /** 		return 0*/
    DeRef(_desired_4380);
    return 0;
L4: 
    ;
}


int _19sample(int _population_4394, int _sample_size_4395, int _sampling_method_4396)
{
    int _lResult_4397 = NOVALUE;
    int _lIdx_4398 = NOVALUE;
    int _lChoice_4399 = NOVALUE;
    int _2263 = NOVALUE;
    int _2259 = NOVALUE;
    int _2256 = NOVALUE;
    int _2252 = NOVALUE;
    int _2251 = NOVALUE;
    int _2250 = NOVALUE;
    int _2249 = NOVALUE;
    int _2248 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sample_size_4395)) {
        _1 = (long)(DBL_PTR(_sample_size_4395)->dbl);
        if (UNIQUE(DBL_PTR(_sample_size_4395)) && (DBL_PTR(_sample_size_4395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sample_size_4395);
        _sample_size_4395 = _1;
    }
    if (!IS_ATOM_INT(_sampling_method_4396)) {
        _1 = (long)(DBL_PTR(_sampling_method_4396)->dbl);
        if (UNIQUE(DBL_PTR(_sampling_method_4396)) && (DBL_PTR(_sampling_method_4396)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sampling_method_4396);
        _sampling_method_4396 = _1;
    }

    /** 	if sample_size < 1 then*/
    if (_sample_size_4395 >= 1)
    goto L1; // [13] 44

    /** 		if sampling_method > 0 then*/
    if (_sampling_method_4396 <= 0)
    goto L2; // [19] 36

    /** 			return {{}, population}	*/
    RefDS(_population_4394);
    RefDS(_5);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5;
    ((int *)_2)[2] = _population_4394;
    _2248 = MAKE_SEQ(_1);
    DeRefDS(_population_4394);
    DeRef(_lResult_4397);
    return _2248;
    goto L3; // [33] 43
L2: 

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_population_4394);
    DeRef(_lResult_4397);
    DeRef(_2248);
    _2248 = NOVALUE;
    return _5;
L3: 
L1: 

    /** 	if sampling_method >= 0 and sample_size >= length(population) then*/
    _2249 = (_sampling_method_4396 >= 0);
    if (_2249 == 0) {
        goto L4; // [50] 73
    }
    if (IS_SEQUENCE(_population_4394)){
            _2251 = SEQ_PTR(_population_4394)->length;
    }
    else {
        _2251 = 1;
    }
    _2252 = (_sample_size_4395 >= _2251);
    _2251 = NOVALUE;
    if (_2252 == 0)
    {
        DeRef(_2252);
        _2252 = NOVALUE;
        goto L4; // [62] 73
    }
    else{
        DeRef(_2252);
        _2252 = NOVALUE;
    }

    /** 		sample_size = length(population)*/
    if (IS_SEQUENCE(_population_4394)){
            _sample_size_4395 = SEQ_PTR(_population_4394)->length;
    }
    else {
        _sample_size_4395 = 1;
    }
L4: 

    /** 	lResult = repeat(0, sample_size)*/
    DeRef(_lResult_4397);
    _lResult_4397 = Repeat(0, _sample_size_4395);

    /** 	lIdx = 0*/
    _lIdx_4398 = 0;

    /** 	while lIdx < sample_size do*/
L5: 
    if (_lIdx_4398 >= _sample_size_4395)
    goto L6; // [91] 142

    /** 		lChoice = rand(length(population))*/
    if (IS_SEQUENCE(_population_4394)){
            _2256 = SEQ_PTR(_population_4394)->length;
    }
    else {
        _2256 = 1;
    }
    _lChoice_4399 = good_rand() % ((unsigned)_2256) + 1;
    _2256 = NOVALUE;

    /** 		lIdx += 1*/
    _lIdx_4398 = _lIdx_4398 + 1;

    /** 		lResult[lIdx] = population[lChoice]*/
    _2 = (int)SEQ_PTR(_population_4394);
    _2259 = (int)*(((s1_ptr)_2)->base + _lChoice_4399);
    Ref(_2259);
    _2 = (int)SEQ_PTR(_lResult_4397);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lResult_4397 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _lIdx_4398);
    _1 = *(int *)_2;
    *(int *)_2 = _2259;
    if( _1 != _2259 ){
        DeRef(_1);
    }
    _2259 = NOVALUE;

    /** 		if sampling_method >= 0 then*/
    if (_sampling_method_4396 < 0)
    goto L5; // [125] 91

    /** 			population = remove(population, lChoice)*/
    {
        s1_ptr assign_space = SEQ_PTR(_population_4394);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lChoice_4399)) ? _lChoice_4399 : (long)(DBL_PTR(_lChoice_4399)->dbl);
        int stop = (IS_ATOM_INT(_lChoice_4399)) ? _lChoice_4399 : (long)(DBL_PTR(_lChoice_4399)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_population_4394), start, &_population_4394 );
            }
            else Tail(SEQ_PTR(_population_4394), stop+1, &_population_4394);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_population_4394), start, &_population_4394);
        }
        else {
            assign_slice_seq = &assign_space;
            _population_4394 = Remove_elements(start, stop, (SEQ_PTR(_population_4394)->ref == 1));
        }
    }

    /** 	end while*/
    goto L5; // [139] 91
L6: 

    /** 	if sampling_method > 0 then*/
    if (_sampling_method_4396 <= 0)
    goto L7; // [144] 161

    /** 		return {lResult, population}	*/
    RefDS(_population_4394);
    RefDS(_lResult_4397);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_4397;
    ((int *)_2)[2] = _population_4394;
    _2263 = MAKE_SEQ(_1);
    DeRefDS(_population_4394);
    DeRefDS(_lResult_4397);
    DeRef(_2248);
    _2248 = NOVALUE;
    DeRef(_2249);
    _2249 = NOVALUE;
    return _2263;
    goto L8; // [158] 168
L7: 

    /** 		return lResult*/
    DeRefDS(_population_4394);
    DeRef(_2248);
    _2248 = NOVALUE;
    DeRef(_2249);
    _2249 = NOVALUE;
    DeRef(_2263);
    _2263 = NOVALUE;
    return _lResult_4397;
L8: 
    ;
}



// 0x6B6CC2B2
