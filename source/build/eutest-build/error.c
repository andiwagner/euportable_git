// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _4crash(int _fmt_391, int _data_392)
{
    int _msg_393 = NOVALUE;
    int _0, _1, _2;
    

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_393);
    _msg_393 = EPrintf(-9999999, _fmt_391, _data_392);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_393);

    /** end procedure*/
    DeRefDS(_fmt_391);
    DeRef(_data_392);
    DeRefDSi(_msg_393);
    return;
    ;
}


void _4crash_message(int _msg_397)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_CRASH_MESSAGE, msg)*/
    machine(37, _msg_397);

    /** end procedure*/
    DeRefDS(_msg_397);
    return;
    ;
}


void _4crash_file(int _file_path_400)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_CRASH_FILE, file_path)*/
    machine(57, _file_path_400);

    /** end procedure*/
    DeRefDS(_file_path_400);
    return;
    ;
}


void _4warning_file(int _file_path_403)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_WARNING_FILE, file_path)*/
    machine(72, _file_path_403);

    /** end procedure*/
    DeRef(_file_path_403);
    return;
    ;
}


void _4crash_routine(int _func_406)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_func_406)) {
        _1 = (long)(DBL_PTR(_func_406)->dbl);
        if (UNIQUE(DBL_PTR(_func_406)) && (DBL_PTR(_func_406)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_func_406);
        _func_406 = _1;
    }

    /** 	machine_proc(M_CRASH_ROUTINE, func)*/
    machine(66, _func_406);

    /** end procedure*/
    return;
    ;
}



// 0x40FA8EB6
