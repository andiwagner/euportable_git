// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _16sprint(int _x_5588)
{
    int _s_5589 = NOVALUE;
    int _2934 = NOVALUE;
    int _2932 = NOVALUE;
    int _2931 = NOVALUE;
    int _2928 = NOVALUE;
    int _2927 = NOVALUE;
    int _2925 = NOVALUE;
    int _2924 = NOVALUE;
    int _2923 = NOVALUE;
    int _2922 = NOVALUE;
    int _2921 = NOVALUE;
    int _2920 = NOVALUE;
    int _2919 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(x) then*/
    _2919 = IS_ATOM(_x_5588);
    if (_2919 == 0)
    {
        _2919 = NOVALUE;
        goto L1; // [6] 22
    }
    else{
        _2919 = NOVALUE;
    }

    /** 		return sprintf("%.10g", x)*/
    _2920 = EPrintf(-9999999, _107, _x_5588);
    DeRef(_x_5588);
    DeRef(_s_5589);
    return _2920;
    goto L2; // [19] 137
L1: 

    /** 		s = "{"*/
    RefDS(_2202);
    DeRef(_s_5589);
    _s_5589 = _2202;

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_5588)){
            _2921 = SEQ_PTR(_x_5588)->length;
    }
    else {
        _2921 = 1;
    }
    {
        int _i_5595;
        _i_5595 = 1;
L3: 
        if (_i_5595 > _2921){
            goto L4; // [34] 98
        }

        /** 			if atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_5588);
        _2922 = (int)*(((s1_ptr)_2)->base + _i_5595);
        _2923 = IS_ATOM(_2922);
        _2922 = NOVALUE;
        if (_2923 == 0)
        {
            _2923 = NOVALUE;
            goto L5; // [50] 70
        }
        else{
            _2923 = NOVALUE;
        }

        /** 				s &= sprintf("%.10g", x[i])*/
        _2 = (int)SEQ_PTR(_x_5588);
        _2924 = (int)*(((s1_ptr)_2)->base + _i_5595);
        _2925 = EPrintf(-9999999, _107, _2924);
        _2924 = NOVALUE;
        Concat((object_ptr)&_s_5589, _s_5589, _2925);
        DeRefDS(_2925);
        _2925 = NOVALUE;
        goto L6; // [67] 85
L5: 

        /** 				s &= sprint(x[i])*/
        _2 = (int)SEQ_PTR(_x_5588);
        _2927 = (int)*(((s1_ptr)_2)->base + _i_5595);
        Ref(_2927);
        _2928 = _16sprint(_2927);
        _2927 = NOVALUE;
        if (IS_SEQUENCE(_s_5589) && IS_ATOM(_2928)) {
            Ref(_2928);
            Append(&_s_5589, _s_5589, _2928);
        }
        else if (IS_ATOM(_s_5589) && IS_SEQUENCE(_2928)) {
        }
        else {
            Concat((object_ptr)&_s_5589, _s_5589, _2928);
        }
        DeRef(_2928);
        _2928 = NOVALUE;
L6: 

        /** 			s &= ','*/
        Append(&_s_5589, _s_5589, 44);

        /** 		end for*/
        _i_5595 = _i_5595 + 1;
        goto L3; // [93] 41
L4: 
        ;
    }

    /** 		if s[$] = ',' then*/
    if (IS_SEQUENCE(_s_5589)){
            _2931 = SEQ_PTR(_s_5589)->length;
    }
    else {
        _2931 = 1;
    }
    _2 = (int)SEQ_PTR(_s_5589);
    _2932 = (int)*(((s1_ptr)_2)->base + _2931);
    if (binary_op_a(NOTEQ, _2932, 44)){
        _2932 = NOVALUE;
        goto L7; // [107] 123
    }
    _2932 = NOVALUE;

    /** 			s[$] = '}'*/
    if (IS_SEQUENCE(_s_5589)){
            _2934 = SEQ_PTR(_s_5589)->length;
    }
    else {
        _2934 = 1;
    }
    _2 = (int)SEQ_PTR(_s_5589);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_5589 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _2934);
    _1 = *(int *)_2;
    *(int *)_2 = 125;
    DeRef(_1);
    goto L8; // [120] 130
L7: 

    /** 			s &= '}'*/
    Append(&_s_5589, _s_5589, 125);
L8: 

    /** 		return s*/
    DeRef(_x_5588);
    DeRef(_2920);
    _2920 = NOVALUE;
    return _s_5589;
L2: 
    ;
}


int _16trim_head(int _source_5617, int _what_5618, int _ret_index_5620)
{
    int _lpos_5621 = NOVALUE;
    int _2946 = NOVALUE;
    int _2945 = NOVALUE;
    int _2942 = NOVALUE;
    int _2941 = NOVALUE;
    int _2939 = NOVALUE;
    int _2937 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_5620)) {
        _1 = (long)(DBL_PTR(_ret_index_5620)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_5620)) && (DBL_PTR(_ret_index_5620)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_5620);
        _ret_index_5620 = _1;
    }

    /** 	if atom(what) then*/
    _2937 = IS_ATOM(_what_5618);
    if (_2937 == 0)
    {
        _2937 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _2937 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_5618;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_5618);
    *((int *)(_2+4)) = _what_5618;
    _what_5618 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_5621 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_5617)){
            _2939 = SEQ_PTR(_source_5617)->length;
    }
    else {
        _2939 = 1;
    }
    if (_lpos_5621 > _2939)
    goto L3; // [37] 73

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_5617);
    _2941 = (int)*(((s1_ptr)_2)->base + _lpos_5621);
    _2942 = find_from(_2941, _what_5618, 1);
    _2941 = NOVALUE;
    if (_2942 != 0)
    goto L4; // [52] 60
    _2942 = NOVALUE;

    /** 			exit*/
    goto L3; // [57] 73
L4: 

    /** 		lpos += 1*/
    _lpos_5621 = _lpos_5621 + 1;

    /** 	end while*/
    goto L2; // [70] 34
L3: 

    /** 	if ret_index then*/
    if (_ret_index_5620 == 0)
    {
        goto L5; // [75] 87
    }
    else{
    }

    /** 		return lpos*/
    DeRefDS(_source_5617);
    DeRef(_what_5618);
    return _lpos_5621;
    goto L6; // [84] 102
L5: 

    /** 		return source[lpos .. $]*/
    if (IS_SEQUENCE(_source_5617)){
            _2945 = SEQ_PTR(_source_5617)->length;
    }
    else {
        _2945 = 1;
    }
    rhs_slice_target = (object_ptr)&_2946;
    RHS_Slice(_source_5617, _lpos_5621, _2945);
    DeRefDS(_source_5617);
    DeRef(_what_5618);
    return _2946;
L6: 
    ;
}


int _16trim_tail(int _source_5639, int _what_5640, int _ret_index_5641)
{
    int _rpos_5642 = NOVALUE;
    int _2955 = NOVALUE;
    int _2952 = NOVALUE;
    int _2951 = NOVALUE;
    int _2947 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_5641)) {
        _1 = (long)(DBL_PTR(_ret_index_5641)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_5641)) && (DBL_PTR(_ret_index_5641)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_5641);
        _ret_index_5641 = _1;
    }

    /** 	if atom(what) then*/
    _2947 = IS_ATOM(_what_5640);
    if (_2947 == 0)
    {
        _2947 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _2947 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_5640;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_5640);
    *((int *)(_2+4)) = _what_5640;
    _what_5640 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_5639)){
            _rpos_5642 = SEQ_PTR(_source_5639)->length;
    }
    else {
        _rpos_5642 = 1;
    }

    /** 	while rpos > 0 do*/
L2: 
    if (_rpos_5642 <= 0)
    goto L3; // [34] 70

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_5639);
    _2951 = (int)*(((s1_ptr)_2)->base + _rpos_5642);
    _2952 = find_from(_2951, _what_5640, 1);
    _2951 = NOVALUE;
    if (_2952 != 0)
    goto L4; // [49] 57
    _2952 = NOVALUE;

    /** 			exit*/
    goto L3; // [54] 70
L4: 

    /** 		rpos -= 1*/
    _rpos_5642 = _rpos_5642 - 1;

    /** 	end while*/
    goto L2; // [67] 34
L3: 

    /** 	if ret_index then*/
    if (_ret_index_5641 == 0)
    {
        goto L5; // [72] 84
    }
    else{
    }

    /** 		return rpos*/
    DeRefDS(_source_5639);
    DeRef(_what_5640);
    return _rpos_5642;
    goto L6; // [81] 96
L5: 

    /** 		return source[1..rpos]*/
    rhs_slice_target = (object_ptr)&_2955;
    RHS_Slice(_source_5639, 1, _rpos_5642);
    DeRefDS(_source_5639);
    DeRef(_what_5640);
    return _2955;
L6: 
    ;
}


int _16trim(int _source_5659, int _what_5660, int _ret_index_5661)
{
    int _rpos_5662 = NOVALUE;
    int _lpos_5663 = NOVALUE;
    int _2976 = NOVALUE;
    int _2974 = NOVALUE;
    int _2972 = NOVALUE;
    int _2970 = NOVALUE;
    int _2967 = NOVALUE;
    int _2966 = NOVALUE;
    int _2961 = NOVALUE;
    int _2960 = NOVALUE;
    int _2958 = NOVALUE;
    int _2956 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_5661)) {
        _1 = (long)(DBL_PTR(_ret_index_5661)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_5661)) && (DBL_PTR(_ret_index_5661)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_5661);
        _ret_index_5661 = _1;
    }

    /** 	if atom(what) then*/
    _2956 = IS_ATOM(_what_5660);
    if (_2956 == 0)
    {
        _2956 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _2956 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_5660;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_5660);
    *((int *)(_2+4)) = _what_5660;
    _what_5660 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_5663 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_5659)){
            _2958 = SEQ_PTR(_source_5659)->length;
    }
    else {
        _2958 = 1;
    }
    if (_lpos_5663 > _2958)
    goto L3; // [37] 73

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_5659);
    _2960 = (int)*(((s1_ptr)_2)->base + _lpos_5663);
    _2961 = find_from(_2960, _what_5660, 1);
    _2960 = NOVALUE;
    if (_2961 != 0)
    goto L4; // [52] 60
    _2961 = NOVALUE;

    /** 			exit*/
    goto L3; // [57] 73
L4: 

    /** 		lpos += 1*/
    _lpos_5663 = _lpos_5663 + 1;

    /** 	end while*/
    goto L2; // [70] 34
L3: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_5659)){
            _rpos_5662 = SEQ_PTR(_source_5659)->length;
    }
    else {
        _rpos_5662 = 1;
    }

    /** 	while rpos > lpos do*/
L5: 
    if (_rpos_5662 <= _lpos_5663)
    goto L6; // [85] 121

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_5659);
    _2966 = (int)*(((s1_ptr)_2)->base + _rpos_5662);
    _2967 = find_from(_2966, _what_5660, 1);
    _2966 = NOVALUE;
    if (_2967 != 0)
    goto L7; // [100] 108
    _2967 = NOVALUE;

    /** 			exit*/
    goto L6; // [105] 121
L7: 

    /** 		rpos -= 1*/
    _rpos_5662 = _rpos_5662 - 1;

    /** 	end while*/
    goto L5; // [118] 85
L6: 

    /** 	if ret_index then*/
    if (_ret_index_5661 == 0)
    {
        goto L8; // [123] 139
    }
    else{
    }

    /** 		return {lpos, rpos}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lpos_5663;
    ((int *)_2)[2] = _rpos_5662;
    _2970 = MAKE_SEQ(_1);
    DeRefDS(_source_5659);
    DeRef(_what_5660);
    return _2970;
    goto L9; // [136] 190
L8: 

    /** 		if lpos = 1 then*/
    if (_lpos_5663 != 1)
    goto LA; // [141] 162

    /** 			if rpos = length(source) then*/
    if (IS_SEQUENCE(_source_5659)){
            _2972 = SEQ_PTR(_source_5659)->length;
    }
    else {
        _2972 = 1;
    }
    if (_rpos_5662 != _2972)
    goto LB; // [150] 161

    /** 				return source*/
    DeRef(_what_5660);
    DeRef(_2970);
    _2970 = NOVALUE;
    return _source_5659;
LB: 
LA: 

    /** 		if lpos > length(source) then*/
    if (IS_SEQUENCE(_source_5659)){
            _2974 = SEQ_PTR(_source_5659)->length;
    }
    else {
        _2974 = 1;
    }
    if (_lpos_5663 <= _2974)
    goto LC; // [167] 178

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_source_5659);
    DeRef(_what_5660);
    DeRef(_2970);
    _2970 = NOVALUE;
    return _5;
LC: 

    /** 		return source[lpos..rpos]*/
    rhs_slice_target = (object_ptr)&_2976;
    RHS_Slice(_source_5659, _lpos_5663, _rpos_5662);
    DeRefDS(_source_5659);
    DeRef(_what_5660);
    DeRef(_2970);
    _2970 = NOVALUE;
    return _2976;
L9: 
    ;
}


int _16load_code_page(int _cpname_5701)
{
    int _cpdata_5702 = NOVALUE;
    int _pos_5703 = NOVALUE;
    int _kv_5704 = NOVALUE;
    int _cp_source_5705 = NOVALUE;
    int _cp_db_5706 = NOVALUE;
    int _fh_5784 = NOVALUE;
    int _idx_5788 = NOVALUE;
    int _vers_5789 = NOVALUE;
    int _3054 = NOVALUE;
    int _3053 = NOVALUE;
    int _3050 = NOVALUE;
    int _3044 = NOVALUE;
    int _3042 = NOVALUE;
    int _3041 = NOVALUE;
    int _3039 = NOVALUE;
    int _3032 = NOVALUE;
    int _3031 = NOVALUE;
    int _3030 = NOVALUE;
    int _3028 = NOVALUE;
    int _3027 = NOVALUE;
    int _3026 = NOVALUE;
    int _3024 = NOVALUE;
    int _3022 = NOVALUE;
    int _3021 = NOVALUE;
    int _3019 = NOVALUE;
    int _3018 = NOVALUE;
    int _3016 = NOVALUE;
    int _3015 = NOVALUE;
    int _3014 = NOVALUE;
    int _3013 = NOVALUE;
    int _3010 = NOVALUE;
    int _3007 = NOVALUE;
    int _3005 = NOVALUE;
    int _3004 = NOVALUE;
    int _3002 = NOVALUE;
    int _3001 = NOVALUE;
    int _3000 = NOVALUE;
    int _2998 = NOVALUE;
    int _2997 = NOVALUE;
    int _2996 = NOVALUE;
    int _2992 = NOVALUE;
    int _2991 = NOVALUE;
    int _2990 = NOVALUE;
    int _2989 = NOVALUE;
    int _2987 = NOVALUE;
    int _2986 = NOVALUE;
    int _2983 = NOVALUE;
    int _2982 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cp_source = filesys:defaultext(cpname, ".ecp")*/
    RefDS(_cpname_5701);
    RefDS(_2978);
    _0 = _cp_source_5705;
    _cp_source_5705 = _8defaultext(_cpname_5701, _2978);
    DeRef(_0);

    /** 	cp_source = filesys:locate_file(cp_source)*/
    RefDS(_cp_source_5705);
    RefDS(_5);
    RefDS(_5);
    _0 = _cp_source_5705;
    _cp_source_5705 = _8locate_file(_cp_source_5705, _5, _5);
    DeRefDS(_0);

    /** 	cpdata = io:read_lines(cp_source)*/
    RefDS(_cp_source_5705);
    _0 = _cpdata_5702;
    _cpdata_5702 = _15read_lines(_cp_source_5705);
    DeRef(_0);

    /** 	if sequence(cpdata) then*/
    _2982 = IS_SEQUENCE(_cpdata_5702);
    if (_2982 == 0)
    {
        _2982 = NOVALUE;
        goto L1; // [33] 379
    }
    else{
        _2982 = NOVALUE;
    }

    /** 		pos = 0*/
    _pos_5703 = 0;

    /** 		while pos < length(cpdata) do*/
L2: 
    if (IS_SEQUENCE(_cpdata_5702)){
            _2983 = SEQ_PTR(_cpdata_5702)->length;
    }
    else {
        _2983 = 1;
    }
    if (_pos_5703 >= _2983)
    goto L3; // [51] 192

    /** 			pos += 1*/
    _pos_5703 = _pos_5703 + 1;

    /** 			cpdata[pos]  = trim(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _2986 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    Ref(_2986);
    RefDS(_2936);
    _2987 = _16trim(_2986, _2936, 0);
    _2986 = NOVALUE;
    _2 = (int)SEQ_PTR(_cpdata_5702);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cpdata_5702 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pos_5703);
    _1 = *(int *)_2;
    *(int *)_2 = _2987;
    if( _1 != _2987 ){
        DeRef(_1);
    }
    _2987 = NOVALUE;

    /** 			if search:begins("--HEAD--", cpdata[pos]) then*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _2989 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    RefDS(_2988);
    Ref(_2989);
    _2990 = _6begins(_2988, _2989);
    _2989 = NOVALUE;
    if (_2990 == 0) {
        DeRef(_2990);
        _2990 = NOVALUE;
        goto L4; // [90] 98
    }
    else {
        if (!IS_ATOM_INT(_2990) && DBL_PTR(_2990)->dbl == 0.0){
            DeRef(_2990);
            _2990 = NOVALUE;
            goto L4; // [90] 98
        }
        DeRef(_2990);
        _2990 = NOVALUE;
    }
    DeRef(_2990);
    _2990 = NOVALUE;

    /** 				continue*/
    goto L2; // [95] 48
L4: 

    /** 			if cpdata[pos][1] = ';' then*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _2991 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    _2 = (int)SEQ_PTR(_2991);
    _2992 = (int)*(((s1_ptr)_2)->base + 1);
    _2991 = NOVALUE;
    if (binary_op_a(NOTEQ, _2992, 59)){
        _2992 = NOVALUE;
        goto L5; // [108] 117
    }
    _2992 = NOVALUE;

    /** 				continue	-- A comment line*/
    goto L2; // [114] 48
L5: 

    /** 			if search:begins("--CASE--", cpdata[pos]) then*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _2996 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    RefDS(_2995);
    Ref(_2996);
    _2997 = _6begins(_2995, _2996);
    _2996 = NOVALUE;
    if (_2997 == 0) {
        DeRef(_2997);
        _2997 = NOVALUE;
        goto L6; // [128] 136
    }
    else {
        if (!IS_ATOM_INT(_2997) && DBL_PTR(_2997)->dbl == 0.0){
            DeRef(_2997);
            _2997 = NOVALUE;
            goto L6; // [128] 136
        }
        DeRef(_2997);
        _2997 = NOVALUE;
    }
    DeRef(_2997);
    _2997 = NOVALUE;

    /** 				exit*/
    goto L3; // [133] 192
L6: 

    /** 			kv = keyvalues(cpdata[pos],,,,"")*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _2998 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    Ref(_2998);
    RefDS(_3151);
    RefDS(_3152);
    RefDS(_3153);
    RefDS(_5);
    _0 = _kv_5704;
    _kv_5704 = _16keyvalues(_2998, _3151, _3152, _3153, _5, 1);
    DeRef(_0);
    _2998 = NOVALUE;

    /** 			if equal(lower(kv[1][1]), "title") then*/
    _2 = (int)SEQ_PTR(_kv_5704);
    _3000 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3000);
    _3001 = (int)*(((s1_ptr)_2)->base + 1);
    _3000 = NOVALUE;
    Ref(_3001);
    _3002 = _16lower(_3001);
    _3001 = NOVALUE;
    if (_3002 == _3003)
    _3004 = 1;
    else if (IS_ATOM_INT(_3002) && IS_ATOM_INT(_3003))
    _3004 = 0;
    else
    _3004 = (compare(_3002, _3003) == 0);
    DeRef(_3002);
    _3002 = NOVALUE;
    if (_3004 == 0)
    {
        _3004 = NOVALUE;
        goto L2; // [171] 48
    }
    else{
        _3004 = NOVALUE;
    }

    /** 				encoding_NAME = kv[1][2]*/
    _2 = (int)SEQ_PTR(_kv_5704);
    _3005 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_16encoding_NAME_5697);
    _2 = (int)SEQ_PTR(_3005);
    _16encoding_NAME_5697 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_16encoding_NAME_5697);
    _3005 = NOVALUE;

    /** 		end while*/
    goto L2; // [189] 48
L3: 

    /** 		if pos > length(cpdata) then*/
    if (IS_SEQUENCE(_cpdata_5702)){
            _3007 = SEQ_PTR(_cpdata_5702)->length;
    }
    else {
        _3007 = 1;
    }
    if (_pos_5703 <= _3007)
    goto L7; // [197] 208

    /** 			return -2 -- No Case Conversion table found.*/
    DeRefDS(_cpname_5701);
    DeRef(_cpdata_5702);
    DeRef(_kv_5704);
    DeRef(_cp_source_5705);
    DeRef(_cp_db_5706);
    return -2;
L7: 

    /** 		upper_case_SET = ""*/
    RefDS(_5);
    DeRef(_16upper_case_SET_5696);
    _16upper_case_SET_5696 = _5;

    /** 		lower_case_SET = ""*/
    RefDS(_5);
    DeRef(_16lower_case_SET_5695);
    _16lower_case_SET_5695 = _5;

    /** 		while pos < length(cpdata) do*/
L8: 
    if (IS_SEQUENCE(_cpdata_5702)){
            _3010 = SEQ_PTR(_cpdata_5702)->length;
    }
    else {
        _3010 = 1;
    }
    if (_pos_5703 >= _3010)
    goto L9; // [230] 578

    /** 			pos += 1*/
    _pos_5703 = _pos_5703 + 1;

    /** 			cpdata[pos]  = trim(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _3013 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    Ref(_3013);
    RefDS(_2936);
    _3014 = _16trim(_3013, _2936, 0);
    _3013 = NOVALUE;
    _2 = (int)SEQ_PTR(_cpdata_5702);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cpdata_5702 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pos_5703);
    _1 = *(int *)_2;
    *(int *)_2 = _3014;
    if( _1 != _3014 ){
        DeRef(_1);
    }
    _3014 = NOVALUE;

    /** 			if length(cpdata[pos]) < 3 then*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _3015 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    if (IS_SEQUENCE(_3015)){
            _3016 = SEQ_PTR(_3015)->length;
    }
    else {
        _3016 = 1;
    }
    _3015 = NOVALUE;
    if (_3016 >= 3)
    goto LA; // [267] 276

    /** 				continue*/
    goto L8; // [273] 227
LA: 

    /** 			if cpdata[pos][1] = ';' then*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _3018 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    _2 = (int)SEQ_PTR(_3018);
    _3019 = (int)*(((s1_ptr)_2)->base + 1);
    _3018 = NOVALUE;
    if (binary_op_a(NOTEQ, _3019, 59)){
        _3019 = NOVALUE;
        goto LB; // [286] 295
    }
    _3019 = NOVALUE;

    /** 				continue	-- A comment line*/
    goto L8; // [292] 227
LB: 

    /** 			if cpdata[pos][1] = '-' then*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _3021 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    _2 = (int)SEQ_PTR(_3021);
    _3022 = (int)*(((s1_ptr)_2)->base + 1);
    _3021 = NOVALUE;
    if (binary_op_a(NOTEQ, _3022, 45)){
        _3022 = NOVALUE;
        goto LC; // [305] 314
    }
    _3022 = NOVALUE;

    /** 				exit*/
    goto L9; // [311] 578
LC: 

    /** 			kv = keyvalues(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_5702);
    _3024 = (int)*(((s1_ptr)_2)->base + _pos_5703);
    Ref(_3024);
    RefDS(_3151);
    RefDS(_3152);
    RefDS(_3153);
    RefDS(_232);
    _0 = _kv_5704;
    _kv_5704 = _16keyvalues(_3024, _3151, _3152, _3153, _232, 1);
    DeRef(_0);
    _3024 = NOVALUE;

    /** 			upper_case_SET &= convert:hex_text(kv[1][1])*/
    _2 = (int)SEQ_PTR(_kv_5704);
    _3026 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3026);
    _3027 = (int)*(((s1_ptr)_2)->base + 1);
    _3026 = NOVALUE;
    Ref(_3027);
    _3028 = _17hex_text(_3027);
    _3027 = NOVALUE;
    if (IS_SEQUENCE(_16upper_case_SET_5696) && IS_ATOM(_3028)) {
        Ref(_3028);
        Append(&_16upper_case_SET_5696, _16upper_case_SET_5696, _3028);
    }
    else if (IS_ATOM(_16upper_case_SET_5696) && IS_SEQUENCE(_3028)) {
    }
    else {
        Concat((object_ptr)&_16upper_case_SET_5696, _16upper_case_SET_5696, _3028);
    }
    DeRef(_3028);
    _3028 = NOVALUE;

    /** 			lower_case_SET &= convert:hex_text(kv[1][2])*/
    _2 = (int)SEQ_PTR(_kv_5704);
    _3030 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3030);
    _3031 = (int)*(((s1_ptr)_2)->base + 2);
    _3030 = NOVALUE;
    Ref(_3031);
    _3032 = _17hex_text(_3031);
    _3031 = NOVALUE;
    if (IS_SEQUENCE(_16lower_case_SET_5695) && IS_ATOM(_3032)) {
        Ref(_3032);
        Append(&_16lower_case_SET_5695, _16lower_case_SET_5695, _3032);
    }
    else if (IS_ATOM(_16lower_case_SET_5695) && IS_SEQUENCE(_3032)) {
    }
    else {
        Concat((object_ptr)&_16lower_case_SET_5695, _16lower_case_SET_5695, _3032);
    }
    DeRef(_3032);
    _3032 = NOVALUE;

    /** 		end while*/
    goto L8; // [373] 227
    goto L9; // [376] 578
L1: 

    /** 		cp_db = filesys:locate_file("ecp.dat")*/
    RefDS(_3034);
    RefDS(_5);
    RefDS(_5);
    _0 = _cp_db_5706;
    _cp_db_5706 = _8locate_file(_3034, _5, _5);
    DeRef(_0);

    /** 		integer fh = open(cp_db, "rb")*/
    _fh_5784 = EOpen(_cp_db_5706, _2913, 0);

    /** 		if fh = -1 then*/
    if (_fh_5784 != -1)
    goto LD; // [400] 411

    /** 			return -2 -- Couldn't open DB*/
    DeRef(_idx_5788);
    DeRef(_vers_5789);
    DeRefDS(_cpname_5701);
    DeRef(_cpdata_5702);
    DeRef(_kv_5704);
    DeRef(_cp_source_5705);
    DeRefDS(_cp_db_5706);
    _3015 = NOVALUE;
    return -2;
LD: 

    /** 		object idx*/

    /** 		object vers*/

    /** 		vers = serialize:deserialize(fh)  -- get the database version*/
    _0 = _vers_5789;
    _vers_5789 = _21deserialize(_fh_5784, 1);
    DeRef(_0);

    /** 		if atom(vers) or length(vers) = 0 then*/
    _3039 = IS_ATOM(_vers_5789);
    if (_3039 != 0) {
        goto LE; // [427] 443
    }
    if (IS_SEQUENCE(_vers_5789)){
            _3041 = SEQ_PTR(_vers_5789)->length;
    }
    else {
        _3041 = 1;
    }
    _3042 = (_3041 == 0);
    _3041 = NOVALUE;
    if (_3042 == 0)
    {
        DeRef(_3042);
        _3042 = NOVALUE;
        goto LF; // [439] 450
    }
    else{
        DeRef(_3042);
        _3042 = NOVALUE;
    }
LE: 

    /** 			return -3 -- DB is wrong or corrupted.*/
    DeRef(_idx_5788);
    DeRef(_vers_5789);
    DeRefDS(_cpname_5701);
    DeRef(_cpdata_5702);
    DeRef(_kv_5704);
    DeRef(_cp_source_5705);
    DeRef(_cp_db_5706);
    _3015 = NOVALUE;
    return -3;
LF: 

    /** 		switch vers[1] do*/
    _2 = (int)SEQ_PTR(_vers_5789);
    _3044 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_3044) ){
        goto L10; // [456] 562
    }
    if(!IS_ATOM_INT(_3044)){
        if( (DBL_PTR(_3044)->dbl != (double) ((int) DBL_PTR(_3044)->dbl) ) ){
            goto L10; // [456] 562
        }
        _0 = (int) DBL_PTR(_3044)->dbl;
    }
    else {
        _0 = _3044;
    };
    _3044 = NOVALUE;
    switch ( _0 ){ 

        /** 			case 1, 2 then*/
        case 1:
        case 2:

        /** 				idx = serialize:deserialize(fh)  -- get Code Page index offset*/
        _0 = _idx_5788;
        _idx_5788 = _21deserialize(_fh_5784, 1);
        DeRef(_0);

        /** 				pos = io:seek(fh, idx)*/
        Ref(_idx_5788);
        _pos_5703 = _15seek(_fh_5784, _idx_5788);
        if (!IS_ATOM_INT(_pos_5703)) {
            _1 = (long)(DBL_PTR(_pos_5703)->dbl);
            if (UNIQUE(DBL_PTR(_pos_5703)) && (DBL_PTR(_pos_5703)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_5703);
            _pos_5703 = _1;
        }

        /** 				idx = serialize:deserialize(fh)	-- get the Code Page Index*/
        _0 = _idx_5788;
        _idx_5788 = _21deserialize(_fh_5784, 1);
        DeRef(_0);

        /** 				pos = find(cpname, idx[1])*/
        _2 = (int)SEQ_PTR(_idx_5788);
        _3050 = (int)*(((s1_ptr)_2)->base + 1);
        _pos_5703 = find_from(_cpname_5701, _3050, 1);
        _3050 = NOVALUE;

        /** 				if pos != 0 then*/
        if (_pos_5703 == 0)
        goto L11; // [507] 571

        /** 					pos = io:seek(fh, idx[2][pos])*/
        _2 = (int)SEQ_PTR(_idx_5788);
        _3053 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_3053);
        _3054 = (int)*(((s1_ptr)_2)->base + _pos_5703);
        _3053 = NOVALUE;
        Ref(_3054);
        _pos_5703 = _15seek(_fh_5784, _3054);
        _3054 = NOVALUE;
        if (!IS_ATOM_INT(_pos_5703)) {
            _1 = (long)(DBL_PTR(_pos_5703)->dbl);
            if (UNIQUE(DBL_PTR(_pos_5703)) && (DBL_PTR(_pos_5703)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_5703);
            _pos_5703 = _1;
        }

        /** 					upper_case_SET = serialize:deserialize(fh) -- "uppercase"*/
        _0 = _21deserialize(_fh_5784, 1);
        DeRef(_16upper_case_SET_5696);
        _16upper_case_SET_5696 = _0;

        /** 					lower_case_SET = serialize:deserialize(fh) -- "lowercase"*/
        _0 = _21deserialize(_fh_5784, 1);
        DeRef(_16lower_case_SET_5695);
        _16lower_case_SET_5695 = _0;

        /** 					encoding_NAME = serialize:deserialize(fh) -- "title"*/
        _0 = _21deserialize(_fh_5784, 1);
        DeRef(_16encoding_NAME_5697);
        _16encoding_NAME_5697 = _0;
        goto L11; // [558] 571

        /** 			case else*/
        default:
L10: 

        /** 				return -4 -- Unhandled ecp database version.*/
        DeRef(_idx_5788);
        DeRef(_vers_5789);
        DeRefDS(_cpname_5701);
        DeRef(_cpdata_5702);
        DeRef(_kv_5704);
        DeRef(_cp_source_5705);
        DeRef(_cp_db_5706);
        _3015 = NOVALUE;
        return -4;
    ;}L11: 

    /** 		close(fh)*/
    EClose(_fh_5784);
    DeRef(_idx_5788);
    _idx_5788 = NOVALUE;
    DeRef(_vers_5789);
    _vers_5789 = NOVALUE;
L9: 

    /** 	return 0*/
    DeRefDS(_cpname_5701);
    DeRef(_cpdata_5702);
    DeRef(_kv_5704);
    DeRef(_cp_source_5705);
    DeRef(_cp_db_5706);
    _3015 = NOVALUE;
    return 0;
    ;
}


void _16set_encoding_properties(int _en_5820, int _lc_5821, int _uc_5822)
{
    int _res_5823 = NOVALUE;
    int _3079 = NOVALUE;
    int _3078 = NOVALUE;
    int _3077 = NOVALUE;
    int _3076 = NOVALUE;
    int _3075 = NOVALUE;
    int _3073 = NOVALUE;
    int _3072 = NOVALUE;
    int _3071 = NOVALUE;
    int _3067 = NOVALUE;
    int _3066 = NOVALUE;
    int _3065 = NOVALUE;
    int _3064 = NOVALUE;
    int _3063 = NOVALUE;
    int _3062 = NOVALUE;
    int _3061 = NOVALUE;
    int _3060 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(en) > 0 and length(lc) = 0 and length(uc) = 0 then*/
    if (IS_SEQUENCE(_en_5820)){
            _3060 = SEQ_PTR(_en_5820)->length;
    }
    else {
        _3060 = 1;
    }
    _3061 = (_3060 > 0);
    _3060 = NOVALUE;
    if (_3061 == 0) {
        _3062 = 0;
        goto L1; // [16] 31
    }
    if (IS_SEQUENCE(_lc_5821)){
            _3063 = SEQ_PTR(_lc_5821)->length;
    }
    else {
        _3063 = 1;
    }
    _3064 = (_3063 == 0);
    _3063 = NOVALUE;
    _3062 = (_3064 != 0);
L1: 
    if (_3062 == 0) {
        goto L2; // [31] 79
    }
    if (IS_SEQUENCE(_uc_5822)){
            _3066 = SEQ_PTR(_uc_5822)->length;
    }
    else {
        _3066 = 1;
    }
    _3067 = (_3066 == 0);
    _3066 = NOVALUE;
    if (_3067 == 0)
    {
        DeRef(_3067);
        _3067 = NOVALUE;
        goto L2; // [43] 79
    }
    else{
        DeRef(_3067);
        _3067 = NOVALUE;
    }

    /** 		res = load_code_page(en)*/
    RefDS(_en_5820);
    _res_5823 = _16load_code_page(_en_5820);
    if (!IS_ATOM_INT(_res_5823)) {
        _1 = (long)(DBL_PTR(_res_5823)->dbl);
        if (UNIQUE(DBL_PTR(_res_5823)) && (DBL_PTR(_res_5823)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_res_5823);
        _res_5823 = _1;
    }

    /** 		if res != 0 then*/
    if (_res_5823 == 0)
    goto L3; // [58] 73

    /** 			printf(2, "Failed to load code page '%s'. Error # %d\n", {en, res})*/
    RefDS(_en_5820);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _en_5820;
    ((int *)_2)[2] = _res_5823;
    _3071 = MAKE_SEQ(_1);
    EPrintf(2, _3070, _3071);
    DeRefDS(_3071);
    _3071 = NOVALUE;
L3: 

    /** 		return*/
    DeRefDS(_en_5820);
    DeRefDS(_lc_5821);
    DeRefDS(_uc_5822);
    DeRef(_3061);
    _3061 = NOVALUE;
    DeRef(_3064);
    _3064 = NOVALUE;
    return;
L2: 

    /** 	if length(lc) = length(uc) then*/
    if (IS_SEQUENCE(_lc_5821)){
            _3072 = SEQ_PTR(_lc_5821)->length;
    }
    else {
        _3072 = 1;
    }
    if (IS_SEQUENCE(_uc_5822)){
            _3073 = SEQ_PTR(_uc_5822)->length;
    }
    else {
        _3073 = 1;
    }
    if (_3072 != _3073)
    goto L4; // [87] 145

    /** 		if length(lc) = 0 and length(en) = 0 then*/
    if (IS_SEQUENCE(_lc_5821)){
            _3075 = SEQ_PTR(_lc_5821)->length;
    }
    else {
        _3075 = 1;
    }
    _3076 = (_3075 == 0);
    _3075 = NOVALUE;
    if (_3076 == 0) {
        goto L5; // [100] 123
    }
    if (IS_SEQUENCE(_en_5820)){
            _3078 = SEQ_PTR(_en_5820)->length;
    }
    else {
        _3078 = 1;
    }
    _3079 = (_3078 == 0);
    _3078 = NOVALUE;
    if (_3079 == 0)
    {
        DeRef(_3079);
        _3079 = NOVALUE;
        goto L5; // [112] 123
    }
    else{
        DeRef(_3079);
        _3079 = NOVALUE;
    }

    /** 			en = "ASCII"*/
    RefDS(_2977);
    DeRefDS(_en_5820);
    _en_5820 = _2977;
L5: 

    /** 		lower_case_SET = lc*/
    RefDS(_lc_5821);
    DeRef(_16lower_case_SET_5695);
    _16lower_case_SET_5695 = _lc_5821;

    /** 		upper_case_SET = uc*/
    RefDS(_uc_5822);
    DeRef(_16upper_case_SET_5696);
    _16upper_case_SET_5696 = _uc_5822;

    /** 		encoding_NAME = en*/
    RefDS(_en_5820);
    DeRef(_16encoding_NAME_5697);
    _16encoding_NAME_5697 = _en_5820;
L4: 

    /** end procedure*/
    DeRefDS(_en_5820);
    DeRefDS(_lc_5821);
    DeRefDS(_uc_5822);
    DeRef(_3061);
    _3061 = NOVALUE;
    DeRef(_3064);
    _3064 = NOVALUE;
    DeRef(_3076);
    _3076 = NOVALUE;
    return;
    ;
}


int _16get_encoding_properties()
{
    int _3080 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {encoding_NAME, lower_case_SET, upper_case_SET}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_16encoding_NAME_5697);
    *((int *)(_2+4)) = _16encoding_NAME_5697;
    RefDS(_16lower_case_SET_5695);
    *((int *)(_2+8)) = _16lower_case_SET_5695;
    RefDS(_16upper_case_SET_5696);
    *((int *)(_2+12)) = _16upper_case_SET_5696;
    _3080 = MAKE_SEQ(_1);
    return _3080;
    ;
}


int _16change_case(int _x_5878, int _api_5879)
{
    int _changed_text_5880 = NOVALUE;
    int _single_char_5881 = NOVALUE;
    int _len_5882 = NOVALUE;
    int _3112 = NOVALUE;
    int _3110 = NOVALUE;
    int _3108 = NOVALUE;
    int _3107 = NOVALUE;
    int _3104 = NOVALUE;
    int _3102 = NOVALUE;
    int _3100 = NOVALUE;
    int _3099 = NOVALUE;
    int _3098 = NOVALUE;
    int _3097 = NOVALUE;
    int _3096 = NOVALUE;
    int _3093 = NOVALUE;
    int _3091 = NOVALUE;
    int _0, _1, _2;
    

    /** 		integer single_char = 0*/
    _single_char_5881 = 0;

    /** 		if not string(x) then*/
    Ref(_x_5878);
    _3091 = _5string(_x_5878);
    if (IS_ATOM_INT(_3091)) {
        if (_3091 != 0){
            DeRef(_3091);
            _3091 = NOVALUE;
            goto L1; // [14] 99
        }
    }
    else {
        if (DBL_PTR(_3091)->dbl != 0.0){
            DeRef(_3091);
            _3091 = NOVALUE;
            goto L1; // [14] 99
        }
    }
    DeRef(_3091);
    _3091 = NOVALUE;

    /** 			if atom(x) then*/
    _3093 = IS_ATOM(_x_5878);
    if (_3093 == 0)
    {
        _3093 = NOVALUE;
        goto L2; // [22] 54
    }
    else{
        _3093 = NOVALUE;
    }

    /** 				if x = 0 then*/
    if (binary_op_a(NOTEQ, _x_5878, 0)){
        goto L3; // [27] 38
    }

    /** 					return 0*/
    DeRef(_x_5878);
    DeRef(_api_5879);
    DeRefi(_changed_text_5880);
    return 0;
L3: 

    /** 				x = {x}*/
    _0 = _x_5878;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_x_5878);
    *((int *)(_2+4)) = _x_5878;
    _x_5878 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 				single_char = 1*/
    _single_char_5881 = 1;
    goto L4; // [51] 98
L2: 

    /** 				for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_5878)){
            _3096 = SEQ_PTR(_x_5878)->length;
    }
    else {
        _3096 = 1;
    }
    {
        int _i_5894;
        _i_5894 = 1;
L5: 
        if (_i_5894 > _3096){
            goto L6; // [59] 91
        }

        /** 					x[i] = change_case(x[i], api)*/
        _2 = (int)SEQ_PTR(_x_5878);
        _3097 = (int)*(((s1_ptr)_2)->base + _i_5894);
        Ref(_api_5879);
        DeRef(_3098);
        _3098 = _api_5879;
        Ref(_3097);
        _3099 = _16change_case(_3097, _3098);
        _3097 = NOVALUE;
        _3098 = NOVALUE;
        _2 = (int)SEQ_PTR(_x_5878);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_5878 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5894);
        _1 = *(int *)_2;
        *(int *)_2 = _3099;
        if( _1 != _3099 ){
            DeRef(_1);
        }
        _3099 = NOVALUE;

        /** 				end for*/
        _i_5894 = _i_5894 + 1;
        goto L5; // [86] 66
L6: 
        ;
    }

    /** 				return x*/
    DeRef(_api_5879);
    DeRefi(_changed_text_5880);
    return _x_5878;
L4: 
L1: 

    /** 		if length(x) = 0 then*/
    if (IS_SEQUENCE(_x_5878)){
            _3100 = SEQ_PTR(_x_5878)->length;
    }
    else {
        _3100 = 1;
    }
    if (_3100 != 0)
    goto L7; // [104] 115

    /** 			return x*/
    DeRef(_api_5879);
    DeRefi(_changed_text_5880);
    return _x_5878;
L7: 

    /** 		if length(x) >= tm_size then*/
    if (IS_SEQUENCE(_x_5878)){
            _3102 = SEQ_PTR(_x_5878)->length;
    }
    else {
        _3102 = 1;
    }
    if (_3102 < _16tm_size_5871)
    goto L8; // [122] 152

    /** 			tm_size = length(x) + 1*/
    if (IS_SEQUENCE(_x_5878)){
            _3104 = SEQ_PTR(_x_5878)->length;
    }
    else {
        _3104 = 1;
    }
    _16tm_size_5871 = _3104 + 1;
    _3104 = NOVALUE;

    /** 			free(temp_mem)*/
    Ref(_16temp_mem_5873);
    _11free(_16temp_mem_5873);

    /** 			temp_mem = allocate(tm_size)*/
    _0 = _11allocate(_16tm_size_5871, 0);
    DeRef(_16temp_mem_5873);
    _16temp_mem_5873 = _0;
L8: 

    /** 		poke(temp_mem, x)*/
    if (IS_ATOM_INT(_16temp_mem_5873)){
        poke_addr = (unsigned char *)_16temp_mem_5873;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_16temp_mem_5873)->dbl);
    }
    if (IS_ATOM_INT(_x_5878)) {
        *poke_addr = (unsigned char)_x_5878;
    }
    else if (IS_ATOM(_x_5878)) {
        _1 = (signed char)DBL_PTR(_x_5878)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_x_5878);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }

    /** 		len = c_func(api, {temp_mem, length(x)} )*/
    if (IS_SEQUENCE(_x_5878)){
            _3107 = SEQ_PTR(_x_5878)->length;
    }
    else {
        _3107 = 1;
    }
    Ref(_16temp_mem_5873);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _16temp_mem_5873;
    ((int *)_2)[2] = _3107;
    _3108 = MAKE_SEQ(_1);
    _3107 = NOVALUE;
    _len_5882 = call_c(1, _api_5879, _3108);
    DeRefDS(_3108);
    _3108 = NOVALUE;
    if (!IS_ATOM_INT(_len_5882)) {
        _1 = (long)(DBL_PTR(_len_5882)->dbl);
        if (UNIQUE(DBL_PTR(_len_5882)) && (DBL_PTR(_len_5882)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_5882);
        _len_5882 = _1;
    }

    /** 		changed_text = peek({temp_mem, len})*/
    Ref(_16temp_mem_5873);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _16temp_mem_5873;
    ((int *)_2)[2] = _len_5882;
    _3110 = MAKE_SEQ(_1);
    DeRefi(_changed_text_5880);
    _1 = (int)SEQ_PTR(_3110);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _changed_text_5880 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_3110);
    _3110 = NOVALUE;

    /** 		if single_char then*/
    if (_single_char_5881 == 0)
    {
        goto L9; // [194] 210
    }
    else{
    }

    /** 			return changed_text[1]*/
    _2 = (int)SEQ_PTR(_changed_text_5880);
    _3112 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_x_5878);
    DeRef(_api_5879);
    DeRefDSi(_changed_text_5880);
    return _3112;
    goto LA; // [207] 217
L9: 

    /** 			return changed_text*/
    DeRef(_x_5878);
    DeRef(_api_5879);
    _3112 = NOVALUE;
    return _changed_text_5880;
LA: 
    ;
}


int _16lower(int _x_5920)
{
    int _3116 = NOVALUE;
    int _3115 = NOVALUE;
    int _3113 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(lower_case_SET) != 0 then*/
    if (IS_SEQUENCE(_16lower_case_SET_5695)){
            _3113 = SEQ_PTR(_16lower_case_SET_5695)->length;
    }
    else {
        _3113 = 1;
    }
    if (_3113 == 0)
    goto L1; // [8] 30

    /** 		return stdseq:mapping(x, upper_case_SET, lower_case_SET)*/
    Ref(_x_5920);
    RefDS(_16upper_case_SET_5696);
    RefDS(_16lower_case_SET_5695);
    _3115 = _3mapping(_x_5920, _16upper_case_SET_5696, _16lower_case_SET_5695, 0);
    DeRef(_x_5920);
    return _3115;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharLowerBuff)*/
    Ref(_x_5920);
    Ref(_16api_CharLowerBuff_5855);
    _3116 = _16change_case(_x_5920, _16api_CharLowerBuff_5855);
    DeRef(_x_5920);
    DeRef(_3115);
    _3115 = NOVALUE;
    return _3116;
    ;
}


int _16upper(int _x_5928)
{
    int _3120 = NOVALUE;
    int _3119 = NOVALUE;
    int _3117 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(upper_case_SET) != 0 then*/
    if (IS_SEQUENCE(_16upper_case_SET_5696)){
            _3117 = SEQ_PTR(_16upper_case_SET_5696)->length;
    }
    else {
        _3117 = 1;
    }
    if (_3117 == 0)
    goto L1; // [8] 30

    /** 		return stdseq:mapping(x, lower_case_SET, upper_case_SET)*/
    Ref(_x_5928);
    RefDS(_16lower_case_SET_5695);
    RefDS(_16upper_case_SET_5696);
    _3119 = _3mapping(_x_5928, _16lower_case_SET_5695, _16upper_case_SET_5696, 0);
    DeRef(_x_5928);
    return _3119;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharUpperBuff)*/
    Ref(_x_5928);
    Ref(_16api_CharUpperBuff_5863);
    _3120 = _16change_case(_x_5928, _16api_CharUpperBuff_5863);
    DeRef(_x_5928);
    DeRef(_3119);
    _3119 = NOVALUE;
    return _3120;
    ;
}


int _16proper(int _x_5936)
{
    int _pos_5937 = NOVALUE;
    int _inword_5938 = NOVALUE;
    int _convert_5939 = NOVALUE;
    int _res_5940 = NOVALUE;
    int _3150 = NOVALUE;
    int _3149 = NOVALUE;
    int _3148 = NOVALUE;
    int _3147 = NOVALUE;
    int _3146 = NOVALUE;
    int _3145 = NOVALUE;
    int _3144 = NOVALUE;
    int _3143 = NOVALUE;
    int _3142 = NOVALUE;
    int _3141 = NOVALUE;
    int _3139 = NOVALUE;
    int _3138 = NOVALUE;
    int _3133 = NOVALUE;
    int _3130 = NOVALUE;
    int _3127 = NOVALUE;
    int _3124 = NOVALUE;
    int _3123 = NOVALUE;
    int _3122 = NOVALUE;
    int _3121 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inword = 0	-- Initially not in a word*/
    _inword_5938 = 0;

    /** 	convert = 1	-- Initially convert text*/
    _convert_5939 = 1;

    /** 	res = x		-- Work on a copy of the original, in case we need to restore.*/
    RefDS(_x_5936);
    DeRef(_res_5940);
    _res_5940 = _x_5936;

    /** 	for i = 1 to length(res) do*/
    if (IS_SEQUENCE(_res_5940)){
            _3121 = SEQ_PTR(_res_5940)->length;
    }
    else {
        _3121 = 1;
    }
    {
        int _i_5942;
        _i_5942 = 1;
L1: 
        if (_i_5942 > _3121){
            goto L2; // [29] 320
        }

        /** 		if integer(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3122 = (int)*(((s1_ptr)_2)->base + _i_5942);
        if (IS_ATOM_INT(_3122))
        _3123 = 1;
        else if (IS_ATOM_DBL(_3122))
        _3123 = IS_ATOM_INT(DoubleToInt(_3122));
        else
        _3123 = 0;
        _3122 = NOVALUE;
        if (_3123 == 0)
        {
            _3123 = NOVALUE;
            goto L3; // [45] 229
        }
        else{
            _3123 = NOVALUE;
        }

        /** 			if convert then*/
        if (_convert_5939 == 0)
        {
            goto L4; // [50] 313
        }
        else{
        }

        /** 				pos = types:t_upper(res[i])*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3124 = (int)*(((s1_ptr)_2)->base + _i_5942);
        Ref(_3124);
        _pos_5937 = _5t_upper(_3124);
        _3124 = NOVALUE;
        if (!IS_ATOM_INT(_pos_5937)) {
            _1 = (long)(DBL_PTR(_pos_5937)->dbl);
            if (UNIQUE(DBL_PTR(_pos_5937)) && (DBL_PTR(_pos_5937)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_5937);
            _pos_5937 = _1;
        }

        /** 				if pos = 0 then*/
        if (_pos_5937 != 0)
        goto L5; // [69] 193

        /** 					pos = types:t_lower(res[i])*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3127 = (int)*(((s1_ptr)_2)->base + _i_5942);
        Ref(_3127);
        _pos_5937 = _5t_lower(_3127);
        _3127 = NOVALUE;
        if (!IS_ATOM_INT(_pos_5937)) {
            _1 = (long)(DBL_PTR(_pos_5937)->dbl);
            if (UNIQUE(DBL_PTR(_pos_5937)) && (DBL_PTR(_pos_5937)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_5937);
            _pos_5937 = _1;
        }

        /** 					if pos = 0 then*/
        if (_pos_5937 != 0)
        goto L6; // [89] 154

        /** 						pos = t_digit(res[i])*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3130 = (int)*(((s1_ptr)_2)->base + _i_5942);
        Ref(_3130);
        _pos_5937 = _5t_digit(_3130);
        _3130 = NOVALUE;
        if (!IS_ATOM_INT(_pos_5937)) {
            _1 = (long)(DBL_PTR(_pos_5937)->dbl);
            if (UNIQUE(DBL_PTR(_pos_5937)) && (DBL_PTR(_pos_5937)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_5937);
            _pos_5937 = _1;
        }

        /** 						if pos = 0 then*/
        if (_pos_5937 != 0)
        goto L4; // [109] 313

        /** 							pos = t_specword(res[i])*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3133 = (int)*(((s1_ptr)_2)->base + _i_5942);
        Ref(_3133);
        _pos_5937 = _5t_specword(_3133);
        _3133 = NOVALUE;
        if (!IS_ATOM_INT(_pos_5937)) {
            _1 = (long)(DBL_PTR(_pos_5937)->dbl);
            if (UNIQUE(DBL_PTR(_pos_5937)) && (DBL_PTR(_pos_5937)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_5937);
            _pos_5937 = _1;
        }

        /** 							if pos then*/
        if (_pos_5937 == 0)
        {
            goto L7; // [129] 142
        }
        else{
        }

        /** 								inword = 1*/
        _inword_5938 = 1;
        goto L4; // [139] 313
L7: 

        /** 								inword = 0*/
        _inword_5938 = 0;
        goto L4; // [151] 313
L6: 

        /** 						if inword = 0 then*/
        if (_inword_5938 != 0)
        goto L4; // [156] 313

        /** 							if pos <= 26 then*/
        if (_pos_5937 > 26)
        goto L8; // [162] 181

        /** 								res[i] = upper(res[i]) -- Convert to uppercase*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3138 = (int)*(((s1_ptr)_2)->base + _i_5942);
        Ref(_3138);
        _3139 = _16upper(_3138);
        _3138 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_5940);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_5940 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5942);
        _1 = *(int *)_2;
        *(int *)_2 = _3139;
        if( _1 != _3139 ){
            DeRef(_1);
        }
        _3139 = NOVALUE;
L8: 

        /** 							inword = 1	-- now we are in a word*/
        _inword_5938 = 1;
        goto L4; // [190] 313
L5: 

        /** 					if inword = 1 then*/
        if (_inword_5938 != 1)
        goto L9; // [195] 216

        /** 						res[i] = lower(res[i]) -- Convert to lowercase*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3141 = (int)*(((s1_ptr)_2)->base + _i_5942);
        Ref(_3141);
        _3142 = _16lower(_3141);
        _3141 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_5940);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_5940 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5942);
        _1 = *(int *)_2;
        *(int *)_2 = _3142;
        if( _1 != _3142 ){
            DeRef(_1);
        }
        _3142 = NOVALUE;
        goto L4; // [213] 313
L9: 

        /** 						inword = 1	-- now we are in a word*/
        _inword_5938 = 1;
        goto L4; // [226] 313
L3: 

        /** 			if convert then*/
        if (_convert_5939 == 0)
        {
            goto LA; // [231] 285
        }
        else{
        }

        /** 				for j = 1 to i-1 do*/
        _3143 = _i_5942 - 1;
        {
            int _j_5983;
            _j_5983 = 1;
LB: 
            if (_j_5983 > _3143){
                goto LC; // [240] 277
            }

            /** 					if atom(x[j]) then*/
            _2 = (int)SEQ_PTR(_x_5936);
            _3144 = (int)*(((s1_ptr)_2)->base + _j_5983);
            _3145 = IS_ATOM(_3144);
            _3144 = NOVALUE;
            if (_3145 == 0)
            {
                _3145 = NOVALUE;
                goto LD; // [256] 270
            }
            else{
                _3145 = NOVALUE;
            }

            /** 						res[j] = x[j]*/
            _2 = (int)SEQ_PTR(_x_5936);
            _3146 = (int)*(((s1_ptr)_2)->base + _j_5983);
            Ref(_3146);
            _2 = (int)SEQ_PTR(_res_5940);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _res_5940 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_5983);
            _1 = *(int *)_2;
            *(int *)_2 = _3146;
            if( _1 != _3146 ){
                DeRef(_1);
            }
            _3146 = NOVALUE;
LD: 

            /** 				end for*/
            _j_5983 = _j_5983 + 1;
            goto LB; // [272] 247
LC: 
            ;
        }

        /** 				convert = 0*/
        _convert_5939 = 0;
LA: 

        /** 			if sequence(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3147 = (int)*(((s1_ptr)_2)->base + _i_5942);
        _3148 = IS_SEQUENCE(_3147);
        _3147 = NOVALUE;
        if (_3148 == 0)
        {
            _3148 = NOVALUE;
            goto LE; // [294] 312
        }
        else{
            _3148 = NOVALUE;
        }

        /** 				res[i] = proper(res[i])	-- recursive conversion*/
        _2 = (int)SEQ_PTR(_res_5940);
        _3149 = (int)*(((s1_ptr)_2)->base + _i_5942);
        Ref(_3149);
        _3150 = _16proper(_3149);
        _3149 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_5940);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_5940 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5942);
        _1 = *(int *)_2;
        *(int *)_2 = _3150;
        if( _1 != _3150 ){
            DeRef(_1);
        }
        _3150 = NOVALUE;
LE: 
L4: 

        /** 	end for*/
        _i_5942 = _i_5942 + 1;
        goto L1; // [315] 36
L2: 
        ;
    }

    /** 	return res*/
    DeRefDS(_x_5936);
    DeRef(_3143);
    _3143 = NOVALUE;
    return _res_5940;
    ;
}


int _16keyvalues(int _source_5996, int _pair_delim_5997, int _kv_delim_5999, int _quotes_6001, int _whitespace_6003, int _haskeys_6004)
{
    int _lKeyValues_6005 = NOVALUE;
    int _value__6006 = NOVALUE;
    int _key__6007 = NOVALUE;
    int _lAllDelim_6008 = NOVALUE;
    int _lWhitePair_6009 = NOVALUE;
    int _lStartBracket_6010 = NOVALUE;
    int _lEndBracket_6011 = NOVALUE;
    int _lBracketed_6012 = NOVALUE;
    int _lQuote_6013 = NOVALUE;
    int _pos__6014 = NOVALUE;
    int _lChar_6015 = NOVALUE;
    int _lBPos_6016 = NOVALUE;
    int _lWasKV_6017 = NOVALUE;
    int _3326 = NOVALUE;
    int _3323 = NOVALUE;
    int _3319 = NOVALUE;
    int _3316 = NOVALUE;
    int _3315 = NOVALUE;
    int _3314 = NOVALUE;
    int _3313 = NOVALUE;
    int _3312 = NOVALUE;
    int _3311 = NOVALUE;
    int _3310 = NOVALUE;
    int _3308 = NOVALUE;
    int _3307 = NOVALUE;
    int _3306 = NOVALUE;
    int _3305 = NOVALUE;
    int _3304 = NOVALUE;
    int _3303 = NOVALUE;
    int _3302 = NOVALUE;
    int _3301 = NOVALUE;
    int _3298 = NOVALUE;
    int _3297 = NOVALUE;
    int _3296 = NOVALUE;
    int _3295 = NOVALUE;
    int _3294 = NOVALUE;
    int _3293 = NOVALUE;
    int _3289 = NOVALUE;
    int _3287 = NOVALUE;
    int _3286 = NOVALUE;
    int _3283 = NOVALUE;
    int _3279 = NOVALUE;
    int _3277 = NOVALUE;
    int _3274 = NOVALUE;
    int _3271 = NOVALUE;
    int _3268 = NOVALUE;
    int _3265 = NOVALUE;
    int _3262 = NOVALUE;
    int _3259 = NOVALUE;
    int _3255 = NOVALUE;
    int _3254 = NOVALUE;
    int _3253 = NOVALUE;
    int _3252 = NOVALUE;
    int _3251 = NOVALUE;
    int _3250 = NOVALUE;
    int _3249 = NOVALUE;
    int _3247 = NOVALUE;
    int _3246 = NOVALUE;
    int _3245 = NOVALUE;
    int _3244 = NOVALUE;
    int _3243 = NOVALUE;
    int _3242 = NOVALUE;
    int _3241 = NOVALUE;
    int _3240 = NOVALUE;
    int _3238 = NOVALUE;
    int _3235 = NOVALUE;
    int _3233 = NOVALUE;
    int _3231 = NOVALUE;
    int _3230 = NOVALUE;
    int _3229 = NOVALUE;
    int _3228 = NOVALUE;
    int _3227 = NOVALUE;
    int _3226 = NOVALUE;
    int _3225 = NOVALUE;
    int _3224 = NOVALUE;
    int _3221 = NOVALUE;
    int _3220 = NOVALUE;
    int _3219 = NOVALUE;
    int _3218 = NOVALUE;
    int _3217 = NOVALUE;
    int _3214 = NOVALUE;
    int _3211 = NOVALUE;
    int _3208 = NOVALUE;
    int _3205 = NOVALUE;
    int _3204 = NOVALUE;
    int _3202 = NOVALUE;
    int _3201 = NOVALUE;
    int _3197 = NOVALUE;
    int _3194 = NOVALUE;
    int _3191 = NOVALUE;
    int _3187 = NOVALUE;
    int _3186 = NOVALUE;
    int _3185 = NOVALUE;
    int _3184 = NOVALUE;
    int _3180 = NOVALUE;
    int _3177 = NOVALUE;
    int _3174 = NOVALUE;
    int _3173 = NOVALUE;
    int _3171 = NOVALUE;
    int _3169 = NOVALUE;
    int _3163 = NOVALUE;
    int _3161 = NOVALUE;
    int _3159 = NOVALUE;
    int _3157 = NOVALUE;
    int _3155 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_haskeys_6004)) {
        _1 = (long)(DBL_PTR(_haskeys_6004)->dbl);
        if (UNIQUE(DBL_PTR(_haskeys_6004)) && (DBL_PTR(_haskeys_6004)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_haskeys_6004);
        _haskeys_6004 = _1;
    }

    /** 	source = trim(source)*/
    RefDS(_source_5996);
    RefDS(_2936);
    _0 = _source_5996;
    _source_5996 = _16trim(_source_5996, _2936, 0);
    DeRefDS(_0);

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_5996)){
            _3155 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3155 = 1;
    }
    if (_3155 != 0)
    goto L1; // [22] 33

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_source_5996);
    DeRef(_pair_delim_5997);
    DeRef(_kv_delim_5999);
    DeRef(_quotes_6001);
    DeRef(_whitespace_6003);
    DeRef(_lKeyValues_6005);
    DeRef(_value__6006);
    DeRef(_key__6007);
    DeRef(_lAllDelim_6008);
    DeRef(_lWhitePair_6009);
    DeRefi(_lStartBracket_6010);
    DeRefi(_lEndBracket_6011);
    DeRefi(_lBracketed_6012);
    return _5;
L1: 

    /** 	if atom(pair_delim) then*/
    _3157 = IS_ATOM(_pair_delim_5997);
    if (_3157 == 0)
    {
        _3157 = NOVALUE;
        goto L2; // [38] 48
    }
    else{
        _3157 = NOVALUE;
    }

    /** 		pair_delim = {pair_delim}*/
    _0 = _pair_delim_5997;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pair_delim_5997);
    *((int *)(_2+4)) = _pair_delim_5997;
    _pair_delim_5997 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	if atom(kv_delim) then*/
    _3159 = IS_ATOM(_kv_delim_5999);
    if (_3159 == 0)
    {
        _3159 = NOVALUE;
        goto L3; // [53] 63
    }
    else{
        _3159 = NOVALUE;
    }

    /** 		kv_delim = {kv_delim}*/
    _0 = _kv_delim_5999;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_kv_delim_5999);
    *((int *)(_2+4)) = _kv_delim_5999;
    _kv_delim_5999 = MAKE_SEQ(_1);
    DeRef(_0);
L3: 

    /** 	if atom(quotes) then*/
    _3161 = IS_ATOM(_quotes_6001);
    if (_3161 == 0)
    {
        _3161 = NOVALUE;
        goto L4; // [68] 78
    }
    else{
        _3161 = NOVALUE;
    }

    /** 		quotes = {quotes}*/
    _0 = _quotes_6001;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quotes_6001);
    *((int *)(_2+4)) = _quotes_6001;
    _quotes_6001 = MAKE_SEQ(_1);
    DeRef(_0);
L4: 

    /** 	if atom(whitespace) then*/
    _3163 = IS_ATOM(_whitespace_6003);
    if (_3163 == 0)
    {
        _3163 = NOVALUE;
        goto L5; // [83] 93
    }
    else{
        _3163 = NOVALUE;
    }

    /** 		whitespace = {whitespace}*/
    _0 = _whitespace_6003;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_whitespace_6003);
    *((int *)(_2+4)) = _whitespace_6003;
    _whitespace_6003 = MAKE_SEQ(_1);
    DeRef(_0);
L5: 

    /** 	lAllDelim = whitespace & pair_delim & kv_delim*/
    {
        int concat_list[3];

        concat_list[0] = _kv_delim_5999;
        concat_list[1] = _pair_delim_5997;
        concat_list[2] = _whitespace_6003;
        Concat_N((object_ptr)&_lAllDelim_6008, concat_list, 3);
    }

    /** 	lWhitePair = whitespace & pair_delim*/
    if (IS_SEQUENCE(_whitespace_6003) && IS_ATOM(_pair_delim_5997)) {
        Ref(_pair_delim_5997);
        Append(&_lWhitePair_6009, _whitespace_6003, _pair_delim_5997);
    }
    else if (IS_ATOM(_whitespace_6003) && IS_SEQUENCE(_pair_delim_5997)) {
        Ref(_whitespace_6003);
        Prepend(&_lWhitePair_6009, _pair_delim_5997, _whitespace_6003);
    }
    else {
        Concat((object_ptr)&_lWhitePair_6009, _whitespace_6003, _pair_delim_5997);
    }

    /** 	lStartBracket = "{[("*/
    RefDS(_3167);
    DeRefi(_lStartBracket_6010);
    _lStartBracket_6010 = _3167;

    /** 	lEndBracket   = "}])"*/
    RefDS(_3168);
    DeRefi(_lEndBracket_6011);
    _lEndBracket_6011 = _3168;

    /** 	lKeyValues = {}*/
    RefDS(_5);
    DeRef(_lKeyValues_6005);
    _lKeyValues_6005 = _5;

    /** 	pos_ = 1*/
    _pos__6014 = 1;

    /** 	while pos_ <= length(source) do*/
L6: 
    if (IS_SEQUENCE(_source_5996)){
            _3169 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3169 = 1;
    }
    if (_pos__6014 > _3169)
    goto L7; // [143] 1298

    /** 		while pos_ < length(source) do*/
L8: 
    if (IS_SEQUENCE(_source_5996)){
            _3171 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3171 = 1;
    }
    if (_pos__6014 >= _3171)
    goto L9; // [155] 192

    /** 			if find(source[pos_], whitespace) = 0 then*/
    _2 = (int)SEQ_PTR(_source_5996);
    _3173 = (int)*(((s1_ptr)_2)->base + _pos__6014);
    _3174 = find_from(_3173, _whitespace_6003, 1);
    _3173 = NOVALUE;
    if (_3174 != 0)
    goto LA; // [170] 179

    /** 				exit*/
    goto L9; // [176] 192
LA: 

    /** 			pos_ +=1*/
    _pos__6014 = _pos__6014 + 1;

    /** 		end while*/
    goto L8; // [189] 152
L9: 

    /** 		key_ = ""*/
    RefDS(_5);
    DeRef(_key__6007);
    _key__6007 = _5;

    /** 		lQuote = 0*/
    _lQuote_6013 = 0;

    /** 		lChar = 0*/
    _lChar_6015 = 0;

    /** 		lWasKV = 0*/
    _lWasKV_6017 = 0;

    /** 		if haskeys then*/
    if (_haskeys_6004 == 0)
    {
        goto LB; // [222] 431
    }
    else{
    }

    /** 			while pos_ <= length(source) do*/
LC: 
    if (IS_SEQUENCE(_source_5996)){
            _3177 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3177 = 1;
    }
    if (_pos__6014 > _3177)
    goto LD; // [233] 359

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_5996);
    _lChar_6015 = (int)*(((s1_ptr)_2)->base + _pos__6014);
    if (!IS_ATOM_INT(_lChar_6015))
    _lChar_6015 = (long)DBL_PTR(_lChar_6015)->dbl;

    /** 				if find(lChar, quotes) != 0 then*/
    _3180 = find_from(_lChar_6015, _quotes_6001, 1);
    if (_3180 == 0)
    goto LE; // [252] 304

    /** 					if lChar = lQuote then*/
    if (_lChar_6015 != _lQuote_6013)
    goto LF; // [258] 279

    /** 						lQuote = 0*/
    _lQuote_6013 = 0;

    /** 						lChar = -1*/
    _lChar_6015 = -1;
    goto L10; // [276] 333
LF: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_6013 != 0)
    goto L10; // [281] 333

    /** 						lQuote = lChar*/
    _lQuote_6013 = _lChar_6015;

    /** 						lChar = -1*/
    _lChar_6015 = -1;
    goto L10; // [301] 333
LE: 

    /** 				elsif lQuote = 0 and find(lChar, lAllDelim) != 0 then*/
    _3184 = (_lQuote_6013 == 0);
    if (_3184 == 0) {
        goto L11; // [310] 332
    }
    _3186 = find_from(_lChar_6015, _lAllDelim_6008, 1);
    _3187 = (_3186 != 0);
    _3186 = NOVALUE;
    if (_3187 == 0)
    {
        DeRef(_3187);
        _3187 = NOVALUE;
        goto L11; // [324] 332
    }
    else{
        DeRef(_3187);
        _3187 = NOVALUE;
    }

    /** 					exit*/
    goto LD; // [329] 359
L11: 
L10: 

    /** 				if lChar > 0 then*/
    if (_lChar_6015 <= 0)
    goto L12; // [335] 346

    /** 					key_ &= lChar*/
    Append(&_key__6007, _key__6007, _lChar_6015);
L12: 

    /** 				pos_ += 1*/
    _pos__6014 = _pos__6014 + 1;

    /** 			end while*/
    goto LC; // [356] 230
LD: 

    /** 			if find(lChar, whitespace) != 0 then*/
    _3191 = find_from(_lChar_6015, _whitespace_6003, 1);
    if (_3191 == 0)
    goto L13; // [366] 440

    /** 				pos_ += 1*/
    _pos__6014 = _pos__6014 + 1;

    /** 				while pos_ <= length(source) do*/
L14: 
    if (IS_SEQUENCE(_source_5996)){
            _3194 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3194 = 1;
    }
    if (_pos__6014 > _3194)
    goto L13; // [386] 440

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_5996);
    _lChar_6015 = (int)*(((s1_ptr)_2)->base + _pos__6014);
    if (!IS_ATOM_INT(_lChar_6015))
    _lChar_6015 = (long)DBL_PTR(_lChar_6015)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _3197 = find_from(_lChar_6015, _whitespace_6003, 1);
    if (_3197 != 0)
    goto L15; // [405] 414

    /** 						exit*/
    goto L13; // [411] 440
L15: 

    /** 					pos_ +=1*/
    _pos__6014 = _pos__6014 + 1;

    /** 				end while*/
    goto L14; // [424] 383
    goto L13; // [428] 440
LB: 

    /** 			pos_ -= 1	-- Put back the last char.*/
    _pos__6014 = _pos__6014 - 1;
L13: 

    /** 		value_ = ""*/
    RefDS(_5);
    DeRef(_value__6006);
    _value__6006 = _5;

    /** 		if find(lChar, kv_delim) != 0  or not haskeys then*/
    _3201 = find_from(_lChar_6015, _kv_delim_5999, 1);
    _3202 = (_3201 != 0);
    _3201 = NOVALUE;
    if (_3202 != 0) {
        goto L16; // [458] 470
    }
    _3204 = (_haskeys_6004 == 0);
    if (_3204 == 0)
    {
        DeRef(_3204);
        _3204 = NOVALUE;
        goto L17; // [466] 981
    }
    else{
        DeRef(_3204);
        _3204 = NOVALUE;
    }
L16: 

    /** 			if find(lChar, kv_delim) != 0 then*/
    _3205 = find_from(_lChar_6015, _kv_delim_5999, 1);
    if (_3205 == 0)
    goto L18; // [477] 489

    /** 				lWasKV = 1*/
    _lWasKV_6017 = 1;
L18: 

    /** 			pos_ += 1*/
    _pos__6014 = _pos__6014 + 1;

    /** 			while pos_ <= length(source) do*/
L19: 
    if (IS_SEQUENCE(_source_5996)){
            _3208 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3208 = 1;
    }
    if (_pos__6014 > _3208)
    goto L1A; // [505] 546

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_5996);
    _lChar_6015 = (int)*(((s1_ptr)_2)->base + _pos__6014);
    if (!IS_ATOM_INT(_lChar_6015))
    _lChar_6015 = (long)DBL_PTR(_lChar_6015)->dbl;

    /** 				if find(lChar, whitespace) = 0 then*/
    _3211 = find_from(_lChar_6015, _whitespace_6003, 1);
    if (_3211 != 0)
    goto L1B; // [524] 533

    /** 					exit*/
    goto L1A; // [530] 546
L1B: 

    /** 				pos_ +=1*/
    _pos__6014 = _pos__6014 + 1;

    /** 			end while*/
    goto L19; // [543] 502
L1A: 

    /** 			lQuote = 0*/
    _lQuote_6013 = 0;

    /** 			lChar = 0*/
    _lChar_6015 = 0;

    /** 			lBracketed = {}*/
    RefDS(_5);
    DeRefi(_lBracketed_6012);
    _lBracketed_6012 = _5;

    /** 			while pos_ <= length(source) do*/
L1C: 
    if (IS_SEQUENCE(_source_5996)){
            _3214 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3214 = 1;
    }
    if (_pos__6014 > _3214)
    goto L1D; // [575] 873

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_5996);
    _lChar_6015 = (int)*(((s1_ptr)_2)->base + _pos__6014);
    if (!IS_ATOM_INT(_lChar_6015))
    _lChar_6015 = (long)DBL_PTR(_lChar_6015)->dbl;

    /** 				if length(lBracketed) = 0 and find(lChar, quotes) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_6012)){
            _3217 = SEQ_PTR(_lBracketed_6012)->length;
    }
    else {
        _3217 = 1;
    }
    _3218 = (_3217 == 0);
    _3217 = NOVALUE;
    if (_3218 == 0) {
        goto L1E; // [596] 661
    }
    _3220 = find_from(_lChar_6015, _quotes_6001, 1);
    _3221 = (_3220 != 0);
    _3220 = NOVALUE;
    if (_3221 == 0)
    {
        DeRef(_3221);
        _3221 = NOVALUE;
        goto L1E; // [610] 661
    }
    else{
        DeRef(_3221);
        _3221 = NOVALUE;
    }

    /** 					if lChar = lQuote then*/
    if (_lChar_6015 != _lQuote_6013)
    goto L1F; // [615] 636

    /** 						lQuote = 0*/
    _lQuote_6013 = 0;

    /** 						lChar = -1*/
    _lChar_6015 = -1;
    goto L20; // [633] 847
L1F: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_6013 != 0)
    goto L20; // [638] 847

    /** 						lQuote = lChar*/
    _lQuote_6013 = _lChar_6015;

    /** 						lChar = -1*/
    _lChar_6015 = -1;
    goto L20; // [658] 847
L1E: 

    /** 				elsif length(value_) = 1 and value_[1] = '~' and find(lChar, lStartBracket) > 0 then*/
    if (IS_SEQUENCE(_value__6006)){
            _3224 = SEQ_PTR(_value__6006)->length;
    }
    else {
        _3224 = 1;
    }
    _3225 = (_3224 == 1);
    _3224 = NOVALUE;
    if (_3225 == 0) {
        _3226 = 0;
        goto L21; // [670] 686
    }
    _2 = (int)SEQ_PTR(_value__6006);
    _3227 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3227)) {
        _3228 = (_3227 == 126);
    }
    else {
        _3228 = binary_op(EQUALS, _3227, 126);
    }
    _3227 = NOVALUE;
    if (IS_ATOM_INT(_3228))
    _3226 = (_3228 != 0);
    else
    _3226 = DBL_PTR(_3228)->dbl != 0.0;
L21: 
    if (_3226 == 0) {
        goto L22; // [686] 725
    }
    _3230 = find_from(_lChar_6015, _lStartBracket_6010, 1);
    _3231 = (_3230 > 0);
    _3230 = NOVALUE;
    if (_3231 == 0)
    {
        DeRef(_3231);
        _3231 = NOVALUE;
        goto L22; // [700] 725
    }
    else{
        DeRef(_3231);
        _3231 = NOVALUE;
    }

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_6016 = find_from(_lChar_6015, _lStartBracket_6010, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_6011);
    _3233 = (int)*(((s1_ptr)_2)->base + _lBPos_6016);
    Append(&_lBracketed_6012, _lBracketed_6012, _3233);
    _3233 = NOVALUE;
    goto L20; // [722] 847
L22: 

    /** 				elsif find(lChar, lStartBracket) > 0 then*/
    _3235 = find_from(_lChar_6015, _lStartBracket_6010, 1);
    if (_3235 <= 0)
    goto L23; // [732] 758

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_6016 = find_from(_lChar_6015, _lStartBracket_6010, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_6011);
    _3238 = (int)*(((s1_ptr)_2)->base + _lBPos_6016);
    Append(&_lBracketed_6012, _lBracketed_6012, _3238);
    _3238 = NOVALUE;
    goto L20; // [755] 847
L23: 

    /** 				elsif length(lBracketed) != 0 and lChar = lBracketed[$] then*/
    if (IS_SEQUENCE(_lBracketed_6012)){
            _3240 = SEQ_PTR(_lBracketed_6012)->length;
    }
    else {
        _3240 = 1;
    }
    _3241 = (_3240 != 0);
    _3240 = NOVALUE;
    if (_3241 == 0) {
        goto L24; // [767] 803
    }
    if (IS_SEQUENCE(_lBracketed_6012)){
            _3243 = SEQ_PTR(_lBracketed_6012)->length;
    }
    else {
        _3243 = 1;
    }
    _2 = (int)SEQ_PTR(_lBracketed_6012);
    _3244 = (int)*(((s1_ptr)_2)->base + _3243);
    _3245 = (_lChar_6015 == _3244);
    _3244 = NOVALUE;
    if (_3245 == 0)
    {
        DeRef(_3245);
        _3245 = NOVALUE;
        goto L24; // [783] 803
    }
    else{
        DeRef(_3245);
        _3245 = NOVALUE;
    }

    /** 					lBracketed = lBracketed[1..$-1]*/
    if (IS_SEQUENCE(_lBracketed_6012)){
            _3246 = SEQ_PTR(_lBracketed_6012)->length;
    }
    else {
        _3246 = 1;
    }
    _3247 = _3246 - 1;
    _3246 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lBracketed_6012;
    RHS_Slice(_lBracketed_6012, 1, _3247);
    goto L20; // [800] 847
L24: 

    /** 				elsif length(lBracketed) = 0 and lQuote = 0 and find(lChar, lWhitePair) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_6012)){
            _3249 = SEQ_PTR(_lBracketed_6012)->length;
    }
    else {
        _3249 = 1;
    }
    _3250 = (_3249 == 0);
    _3249 = NOVALUE;
    if (_3250 == 0) {
        _3251 = 0;
        goto L25; // [812] 824
    }
    _3252 = (_lQuote_6013 == 0);
    _3251 = (_3252 != 0);
L25: 
    if (_3251 == 0) {
        goto L26; // [824] 846
    }
    _3254 = find_from(_lChar_6015, _lWhitePair_6009, 1);
    _3255 = (_3254 != 0);
    _3254 = NOVALUE;
    if (_3255 == 0)
    {
        DeRef(_3255);
        _3255 = NOVALUE;
        goto L26; // [838] 846
    }
    else{
        DeRef(_3255);
        _3255 = NOVALUE;
    }

    /** 					exit*/
    goto L1D; // [843] 873
L26: 
L20: 

    /** 				if lChar > 0 then*/
    if (_lChar_6015 <= 0)
    goto L27; // [849] 860

    /** 					value_ &= lChar*/
    Append(&_value__6006, _value__6006, _lChar_6015);
L27: 

    /** 				pos_ += 1*/
    _pos__6014 = _pos__6014 + 1;

    /** 			end while*/
    goto L1C; // [870] 572
L1D: 

    /** 			if find(lChar, whitespace) != 0  then*/
    _3259 = find_from(_lChar_6015, _whitespace_6003, 1);
    if (_3259 == 0)
    goto L28; // [880] 942

    /** 				pos_ += 1*/
    _pos__6014 = _pos__6014 + 1;

    /** 				while pos_ <= length(source) do*/
L29: 
    if (IS_SEQUENCE(_source_5996)){
            _3262 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3262 = 1;
    }
    if (_pos__6014 > _3262)
    goto L2A; // [900] 941

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_5996);
    _lChar_6015 = (int)*(((s1_ptr)_2)->base + _pos__6014);
    if (!IS_ATOM_INT(_lChar_6015))
    _lChar_6015 = (long)DBL_PTR(_lChar_6015)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _3265 = find_from(_lChar_6015, _whitespace_6003, 1);
    if (_3265 != 0)
    goto L2B; // [919] 928

    /** 						exit*/
    goto L2A; // [925] 941
L2B: 

    /** 					pos_ +=1*/
    _pos__6014 = _pos__6014 + 1;

    /** 				end while*/
    goto L29; // [938] 897
L2A: 
L28: 

    /** 			if find(lChar, pair_delim) != 0  then*/
    _3268 = find_from(_lChar_6015, _pair_delim_5997, 1);
    if (_3268 == 0)
    goto L2C; // [949] 980

    /** 				pos_ += 1*/
    _pos__6014 = _pos__6014 + 1;

    /** 				if pos_ <= length(source) then*/
    if (IS_SEQUENCE(_source_5996)){
            _3271 = SEQ_PTR(_source_5996)->length;
    }
    else {
        _3271 = 1;
    }
    if (_pos__6014 > _3271)
    goto L2D; // [966] 979

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_5996);
    _lChar_6015 = (int)*(((s1_ptr)_2)->base + _pos__6014);
    if (!IS_ATOM_INT(_lChar_6015))
    _lChar_6015 = (long)DBL_PTR(_lChar_6015)->dbl;
L2D: 
L2C: 
L17: 

    /** 		if find(lChar, pair_delim) != 0  then*/
    _3274 = find_from(_lChar_6015, _pair_delim_5997, 1);
    if (_3274 == 0)
    goto L2E; // [988] 1001

    /** 			pos_ += 1*/
    _pos__6014 = _pos__6014 + 1;
L2E: 

    /** 		if length(value_) = 0 then*/
    if (IS_SEQUENCE(_value__6006)){
            _3277 = SEQ_PTR(_value__6006)->length;
    }
    else {
        _3277 = 1;
    }
    if (_3277 != 0)
    goto L2F; // [1006] 1051

    /** 			if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__6007)){
            _3279 = SEQ_PTR(_key__6007)->length;
    }
    else {
        _3279 = 1;
    }
    if (_3279 != 0)
    goto L30; // [1015] 1030

    /** 				lKeyValues = append(lKeyValues, {})*/
    RefDS(_5);
    Append(&_lKeyValues_6005, _lKeyValues_6005, _5);

    /** 				continue*/
    goto L6; // [1027] 140
L30: 

    /** 			if not lWasKV then*/
    if (_lWasKV_6017 != 0)
    goto L31; // [1032] 1050

    /** 				value_ = key_*/
    RefDS(_key__6007);
    DeRef(_value__6006);
    _value__6006 = _key__6007;

    /** 				key_ = ""*/
    RefDS(_5);
    DeRefDS(_key__6007);
    _key__6007 = _5;
L31: 
L2F: 

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__6007)){
            _3283 = SEQ_PTR(_key__6007)->length;
    }
    else {
        _3283 = 1;
    }
    if (_3283 != 0)
    goto L32; // [1056] 1080

    /** 			if haskeys then*/
    if (_haskeys_6004 == 0)
    {
        goto L33; // [1062] 1079
    }
    else{
    }

    /** 				key_ =  sprintf("p[%d]", length(lKeyValues) + 1)*/
    if (IS_SEQUENCE(_lKeyValues_6005)){
            _3286 = SEQ_PTR(_lKeyValues_6005)->length;
    }
    else {
        _3286 = 1;
    }
    _3287 = _3286 + 1;
    _3286 = NOVALUE;
    DeRefDS(_key__6007);
    _key__6007 = EPrintf(-9999999, _3285, _3287);
    _3287 = NOVALUE;
L33: 
L32: 

    /** 		if length(value_) > 0 then*/
    if (IS_SEQUENCE(_value__6006)){
            _3289 = SEQ_PTR(_value__6006)->length;
    }
    else {
        _3289 = 1;
    }
    if (_3289 <= 0)
    goto L34; // [1085] 1244

    /** 			lChar = value_[1]*/
    _2 = (int)SEQ_PTR(_value__6006);
    _lChar_6015 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lChar_6015))
    _lChar_6015 = (long)DBL_PTR(_lChar_6015)->dbl;

    /** 			lBPos = find(lChar, lStartBracket)*/
    _lBPos_6016 = find_from(_lChar_6015, _lStartBracket_6010, 1);

    /** 			if lBPos > 0 and value_[$] = lEndBracket[lBPos] then*/
    _3293 = (_lBPos_6016 > 0);
    if (_3293 == 0) {
        goto L35; // [1112] 1225
    }
    if (IS_SEQUENCE(_value__6006)){
            _3295 = SEQ_PTR(_value__6006)->length;
    }
    else {
        _3295 = 1;
    }
    _2 = (int)SEQ_PTR(_value__6006);
    _3296 = (int)*(((s1_ptr)_2)->base + _3295);
    _2 = (int)SEQ_PTR(_lEndBracket_6011);
    _3297 = (int)*(((s1_ptr)_2)->base + _lBPos_6016);
    if (IS_ATOM_INT(_3296)) {
        _3298 = (_3296 == _3297);
    }
    else {
        _3298 = binary_op(EQUALS, _3296, _3297);
    }
    _3296 = NOVALUE;
    _3297 = NOVALUE;
    if (_3298 == 0) {
        DeRef(_3298);
        _3298 = NOVALUE;
        goto L35; // [1132] 1225
    }
    else {
        if (!IS_ATOM_INT(_3298) && DBL_PTR(_3298)->dbl == 0.0){
            DeRef(_3298);
            _3298 = NOVALUE;
            goto L35; // [1132] 1225
        }
        DeRef(_3298);
        _3298 = NOVALUE;
    }
    DeRef(_3298);
    _3298 = NOVALUE;

    /** 				if lChar = '(' then*/
    if (_lChar_6015 != 40)
    goto L36; // [1137] 1184

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, haskeys)*/
    if (IS_SEQUENCE(_value__6006)){
            _3301 = SEQ_PTR(_value__6006)->length;
    }
    else {
        _3301 = 1;
    }
    _3302 = _3301 - 1;
    _3301 = NOVALUE;
    rhs_slice_target = (object_ptr)&_3303;
    RHS_Slice(_value__6006, 2, _3302);
    Ref(_pair_delim_5997);
    DeRef(_3304);
    _3304 = _pair_delim_5997;
    Ref(_kv_delim_5999);
    DeRef(_3305);
    _3305 = _kv_delim_5999;
    Ref(_quotes_6001);
    DeRef(_3306);
    _3306 = _quotes_6001;
    Ref(_whitespace_6003);
    DeRef(_3307);
    _3307 = _whitespace_6003;
    DeRef(_3308);
    _3308 = _haskeys_6004;
    _0 = _value__6006;
    _value__6006 = _16keyvalues(_3303, _3304, _3305, _3306, _3307, _3308);
    DeRefDS(_0);
    _3303 = NOVALUE;
    _3304 = NOVALUE;
    _3305 = NOVALUE;
    _3306 = NOVALUE;
    _3307 = NOVALUE;
    _3308 = NOVALUE;
    goto L37; // [1181] 1243
L36: 

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, 0)*/
    if (IS_SEQUENCE(_value__6006)){
            _3310 = SEQ_PTR(_value__6006)->length;
    }
    else {
        _3310 = 1;
    }
    _3311 = _3310 - 1;
    _3310 = NOVALUE;
    rhs_slice_target = (object_ptr)&_3312;
    RHS_Slice(_value__6006, 2, _3311);
    Ref(_pair_delim_5997);
    DeRef(_3313);
    _3313 = _pair_delim_5997;
    Ref(_kv_delim_5999);
    DeRef(_3314);
    _3314 = _kv_delim_5999;
    Ref(_quotes_6001);
    DeRef(_3315);
    _3315 = _quotes_6001;
    Ref(_whitespace_6003);
    DeRef(_3316);
    _3316 = _whitespace_6003;
    _0 = _value__6006;
    _value__6006 = _16keyvalues(_3312, _3313, _3314, _3315, _3316, 0);
    DeRefDS(_0);
    _3312 = NOVALUE;
    _3313 = NOVALUE;
    _3314 = NOVALUE;
    _3315 = NOVALUE;
    _3316 = NOVALUE;
    goto L37; // [1222] 1243
L35: 

    /** 			elsif lChar = '~' then*/
    if (_lChar_6015 != 126)
    goto L38; // [1227] 1242

    /** 				value_ = value_[2 .. $]*/
    if (IS_SEQUENCE(_value__6006)){
            _3319 = SEQ_PTR(_value__6006)->length;
    }
    else {
        _3319 = 1;
    }
    rhs_slice_target = (object_ptr)&_value__6006;
    RHS_Slice(_value__6006, 2, _3319);
L38: 
L37: 
L34: 

    /** 		key_ = trim(key_)*/
    RefDS(_key__6007);
    RefDS(_2936);
    _0 = _key__6007;
    _key__6007 = _16trim(_key__6007, _2936, 0);
    DeRefDS(_0);

    /** 		value_ = trim(value_)*/
    RefDS(_value__6006);
    RefDS(_2936);
    _0 = _value__6006;
    _value__6006 = _16trim(_value__6006, _2936, 0);
    DeRefDS(_0);

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__6007)){
            _3323 = SEQ_PTR(_key__6007)->length;
    }
    else {
        _3323 = 1;
    }
    if (_3323 != 0)
    goto L39; // [1269] 1282

    /** 			lKeyValues = append(lKeyValues, value_)*/
    RefDS(_value__6006);
    Append(&_lKeyValues_6005, _lKeyValues_6005, _value__6006);
    goto L6; // [1279] 140
L39: 

    /** 			lKeyValues = append(lKeyValues, {key_, value_})*/
    RefDS(_value__6006);
    RefDS(_key__6007);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key__6007;
    ((int *)_2)[2] = _value__6006;
    _3326 = MAKE_SEQ(_1);
    RefDS(_3326);
    Append(&_lKeyValues_6005, _lKeyValues_6005, _3326);
    DeRefDS(_3326);
    _3326 = NOVALUE;

    /** 	end while*/
    goto L6; // [1295] 140
L7: 

    /** 	return lKeyValues*/
    DeRefDS(_source_5996);
    DeRef(_pair_delim_5997);
    DeRef(_kv_delim_5999);
    DeRef(_quotes_6001);
    DeRef(_whitespace_6003);
    DeRef(_value__6006);
    DeRef(_key__6007);
    DeRef(_lAllDelim_6008);
    DeRef(_lWhitePair_6009);
    DeRefi(_lStartBracket_6010);
    DeRefi(_lEndBracket_6011);
    DeRefi(_lBracketed_6012);
    DeRef(_3184);
    _3184 = NOVALUE;
    DeRef(_3202);
    _3202 = NOVALUE;
    DeRef(_3218);
    _3218 = NOVALUE;
    DeRef(_3225);
    _3225 = NOVALUE;
    DeRef(_3241);
    _3241 = NOVALUE;
    DeRef(_3228);
    _3228 = NOVALUE;
    DeRef(_3247);
    _3247 = NOVALUE;
    DeRef(_3250);
    _3250 = NOVALUE;
    DeRef(_3252);
    _3252 = NOVALUE;
    DeRef(_3293);
    _3293 = NOVALUE;
    DeRef(_3302);
    _3302 = NOVALUE;
    DeRef(_3311);
    _3311 = NOVALUE;
    return _lKeyValues_6005;
    ;
}


int _16escape(int _s_6244, int _what_6245)
{
    int _r_6247 = NOVALUE;
    int _3333 = NOVALUE;
    int _3331 = NOVALUE;
    int _3330 = NOVALUE;
    int _3329 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence r = ""*/
    RefDS(_5);
    DeRef(_r_6247);
    _r_6247 = _5;

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_6244)){
            _3329 = SEQ_PTR(_s_6244)->length;
    }
    else {
        _3329 = 1;
    }
    {
        int _i_6249;
        _i_6249 = 1;
L1: 
        if (_i_6249 > _3329){
            goto L2; // [17] 62
        }

        /** 		if find(s[i], what) then*/
        _2 = (int)SEQ_PTR(_s_6244);
        _3330 = (int)*(((s1_ptr)_2)->base + _i_6249);
        _3331 = find_from(_3330, _what_6245, 1);
        _3330 = NOVALUE;
        if (_3331 == 0)
        {
            _3331 = NOVALUE;
            goto L3; // [35] 45
        }
        else{
            _3331 = NOVALUE;
        }

        /** 			r &= "\\"*/
        Concat((object_ptr)&_r_6247, _r_6247, _2185);
L3: 

        /** 		r &= s[i]*/
        _2 = (int)SEQ_PTR(_s_6244);
        _3333 = (int)*(((s1_ptr)_2)->base + _i_6249);
        if (IS_SEQUENCE(_r_6247) && IS_ATOM(_3333)) {
            Ref(_3333);
            Append(&_r_6247, _r_6247, _3333);
        }
        else if (IS_ATOM(_r_6247) && IS_SEQUENCE(_3333)) {
        }
        else {
            Concat((object_ptr)&_r_6247, _r_6247, _3333);
        }
        _3333 = NOVALUE;

        /** 	end for*/
        _i_6249 = _i_6249 + 1;
        goto L1; // [57] 24
L2: 
        ;
    }

    /** 	return r*/
    DeRefDS(_s_6244);
    DeRefDS(_what_6245);
    return _r_6247;
    ;
}


int _16quote(int _text_in_6259, int _quote_pair_6260, int _esc_6262, int _sp_6264)
{
    int _3421 = NOVALUE;
    int _3420 = NOVALUE;
    int _3419 = NOVALUE;
    int _3417 = NOVALUE;
    int _3416 = NOVALUE;
    int _3415 = NOVALUE;
    int _3413 = NOVALUE;
    int _3412 = NOVALUE;
    int _3411 = NOVALUE;
    int _3410 = NOVALUE;
    int _3409 = NOVALUE;
    int _3408 = NOVALUE;
    int _3407 = NOVALUE;
    int _3406 = NOVALUE;
    int _3405 = NOVALUE;
    int _3403 = NOVALUE;
    int _3402 = NOVALUE;
    int _3401 = NOVALUE;
    int _3399 = NOVALUE;
    int _3398 = NOVALUE;
    int _3397 = NOVALUE;
    int _3396 = NOVALUE;
    int _3395 = NOVALUE;
    int _3394 = NOVALUE;
    int _3393 = NOVALUE;
    int _3392 = NOVALUE;
    int _3391 = NOVALUE;
    int _3389 = NOVALUE;
    int _3388 = NOVALUE;
    int _3386 = NOVALUE;
    int _3385 = NOVALUE;
    int _3384 = NOVALUE;
    int _3382 = NOVALUE;
    int _3381 = NOVALUE;
    int _3380 = NOVALUE;
    int _3379 = NOVALUE;
    int _3378 = NOVALUE;
    int _3377 = NOVALUE;
    int _3376 = NOVALUE;
    int _3375 = NOVALUE;
    int _3374 = NOVALUE;
    int _3373 = NOVALUE;
    int _3372 = NOVALUE;
    int _3371 = NOVALUE;
    int _3370 = NOVALUE;
    int _3369 = NOVALUE;
    int _3368 = NOVALUE;
    int _3367 = NOVALUE;
    int _3366 = NOVALUE;
    int _3363 = NOVALUE;
    int _3362 = NOVALUE;
    int _3361 = NOVALUE;
    int _3360 = NOVALUE;
    int _3359 = NOVALUE;
    int _3358 = NOVALUE;
    int _3357 = NOVALUE;
    int _3356 = NOVALUE;
    int _3355 = NOVALUE;
    int _3354 = NOVALUE;
    int _3353 = NOVALUE;
    int _3352 = NOVALUE;
    int _3351 = NOVALUE;
    int _3350 = NOVALUE;
    int _3347 = NOVALUE;
    int _3345 = NOVALUE;
    int _3344 = NOVALUE;
    int _3342 = NOVALUE;
    int _3340 = NOVALUE;
    int _3339 = NOVALUE;
    int _3338 = NOVALUE;
    int _3336 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_esc_6262)) {
        _1 = (long)(DBL_PTR(_esc_6262)->dbl);
        if (UNIQUE(DBL_PTR(_esc_6262)) && (DBL_PTR(_esc_6262)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_esc_6262);
        _esc_6262 = _1;
    }

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_6259)){
            _3336 = SEQ_PTR(_text_in_6259)->length;
    }
    else {
        _3336 = 1;
    }
    if (_3336 != 0)
    goto L1; // [12] 23

    /** 		return text_in*/
    DeRef(_quote_pair_6260);
    DeRef(_sp_6264);
    return _text_in_6259;
L1: 

    /** 	if atom(quote_pair) then*/
    _3338 = IS_ATOM(_quote_pair_6260);
    if (_3338 == 0)
    {
        _3338 = NOVALUE;
        goto L2; // [28] 48
    }
    else{
        _3338 = NOVALUE;
    }

    /** 		quote_pair = {{quote_pair}, {quote_pair}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_6260);
    *((int *)(_2+4)) = _quote_pair_6260;
    _3339 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_6260);
    *((int *)(_2+4)) = _quote_pair_6260;
    _3340 = MAKE_SEQ(_1);
    DeRef(_quote_pair_6260);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3339;
    ((int *)_2)[2] = _3340;
    _quote_pair_6260 = MAKE_SEQ(_1);
    _3340 = NOVALUE;
    _3339 = NOVALUE;
    goto L3; // [45] 91
L2: 

    /** 	elsif length(quote_pair) = 1 then*/
    if (IS_SEQUENCE(_quote_pair_6260)){
            _3342 = SEQ_PTR(_quote_pair_6260)->length;
    }
    else {
        _3342 = 1;
    }
    if (_3342 != 1)
    goto L4; // [53] 74

    /** 		quote_pair = {quote_pair[1], quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3344 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3345 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3345);
    Ref(_3344);
    DeRef(_quote_pair_6260);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3344;
    ((int *)_2)[2] = _3345;
    _quote_pair_6260 = MAKE_SEQ(_1);
    _3345 = NOVALUE;
    _3344 = NOVALUE;
    goto L3; // [71] 91
L4: 

    /** 	elsif length(quote_pair) = 0 then*/
    if (IS_SEQUENCE(_quote_pair_6260)){
            _3347 = SEQ_PTR(_quote_pair_6260)->length;
    }
    else {
        _3347 = 1;
    }
    if (_3347 != 0)
    goto L5; // [79] 90

    /** 		quote_pair = {"\"", "\""}*/
    RefDS(_3328);
    RefDS(_3328);
    DeRef(_quote_pair_6260);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3328;
    ((int *)_2)[2] = _3328;
    _quote_pair_6260 = MAKE_SEQ(_1);
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_6259);
    _3350 = (int)*(((s1_ptr)_2)->base + 1);
    _3351 = IS_SEQUENCE(_3350);
    _3350 = NOVALUE;
    if (_3351 == 0)
    {
        _3351 = NOVALUE;
        goto L6; // [100] 168
    }
    else{
        _3351 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_6259)){
            _3352 = SEQ_PTR(_text_in_6259)->length;
    }
    else {
        _3352 = 1;
    }
    {
        int _i_6287;
        _i_6287 = 1;
L7: 
        if (_i_6287 > _3352){
            goto L8; // [108] 161
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_6259);
        _3353 = (int)*(((s1_ptr)_2)->base + _i_6287);
        _3354 = IS_SEQUENCE(_3353);
        _3353 = NOVALUE;
        if (_3354 == 0)
        {
            _3354 = NOVALUE;
            goto L9; // [124] 154
        }
        else{
            _3354 = NOVALUE;
        }

        /** 				text_in[i] = quote(text_in[i], quote_pair, esc, sp)*/
        _2 = (int)SEQ_PTR(_text_in_6259);
        _3355 = (int)*(((s1_ptr)_2)->base + _i_6287);
        Ref(_quote_pair_6260);
        DeRef(_3356);
        _3356 = _quote_pair_6260;
        DeRef(_3357);
        _3357 = _esc_6262;
        Ref(_sp_6264);
        DeRef(_3358);
        _3358 = _sp_6264;
        Ref(_3355);
        _3359 = _16quote(_3355, _3356, _3357, _3358);
        _3355 = NOVALUE;
        _3356 = NOVALUE;
        _3357 = NOVALUE;
        _3358 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_6259);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_6259 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_6287);
        _1 = *(int *)_2;
        *(int *)_2 = _3359;
        if( _1 != _3359 ){
            DeRef(_1);
        }
        _3359 = NOVALUE;
L9: 

        /** 		end for*/
        _i_6287 = _i_6287 + 1;
        goto L7; // [156] 115
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pair_6260);
    DeRef(_sp_6264);
    return _text_in_6259;
L6: 

    /** 	for i = 1 to length(sp) do*/
    if (IS_SEQUENCE(_sp_6264)){
            _3360 = SEQ_PTR(_sp_6264)->length;
    }
    else {
        _3360 = 1;
    }
    {
        int _i_6298;
        _i_6298 = 1;
LA: 
        if (_i_6298 > _3360){
            goto LB; // [173] 222
        }

        /** 		if find(sp[i], text_in) then*/
        _2 = (int)SEQ_PTR(_sp_6264);
        _3361 = (int)*(((s1_ptr)_2)->base + _i_6298);
        _3362 = find_from(_3361, _text_in_6259, 1);
        _3361 = NOVALUE;
        if (_3362 == 0)
        {
            _3362 = NOVALUE;
            goto LC; // [191] 199
        }
        else{
            _3362 = NOVALUE;
        }

        /** 			exit*/
        goto LB; // [196] 222
LC: 

        /** 		if i = length(sp) then*/
        if (IS_SEQUENCE(_sp_6264)){
                _3363 = SEQ_PTR(_sp_6264)->length;
        }
        else {
            _3363 = 1;
        }
        if (_i_6298 != _3363)
        goto LD; // [204] 215

        /** 			return text_in*/
        DeRef(_quote_pair_6260);
        DeRef(_sp_6264);
        return _text_in_6259;
LD: 

        /** 	end for*/
        _i_6298 = _i_6298 + 1;
        goto LA; // [217] 180
LB: 
        ;
    }

    /** 	if esc >= 0  then*/
    if (_esc_6262 < 0)
    goto LE; // [224] 563

    /** 		if atom(quote_pair[1]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3366 = (int)*(((s1_ptr)_2)->base + 1);
    _3367 = IS_ATOM(_3366);
    _3366 = NOVALUE;
    if (_3367 == 0)
    {
        _3367 = NOVALUE;
        goto LF; // [237] 255
    }
    else{
        _3367 = NOVALUE;
    }

    /** 			quote_pair[1] = {quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3368 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_3368);
    *((int *)(_2+4)) = _3368;
    _3369 = MAKE_SEQ(_1);
    _3368 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_6260 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _3369;
    if( _1 != _3369 ){
        DeRef(_1);
    }
    _3369 = NOVALUE;
LF: 

    /** 		if atom(quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3370 = (int)*(((s1_ptr)_2)->base + 2);
    _3371 = IS_ATOM(_3370);
    _3370 = NOVALUE;
    if (_3371 == 0)
    {
        _3371 = NOVALUE;
        goto L10; // [264] 282
    }
    else{
        _3371 = NOVALUE;
    }

    /** 			quote_pair[2] = {quote_pair[2]}*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3372 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_3372);
    *((int *)(_2+4)) = _3372;
    _3373 = MAKE_SEQ(_1);
    _3372 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_6260 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _3373;
    if( _1 != _3373 ){
        DeRef(_1);
    }
    _3373 = NOVALUE;
L10: 

    /** 		if equal(quote_pair[1], quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3374 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3375 = (int)*(((s1_ptr)_2)->base + 2);
    if (_3374 == _3375)
    _3376 = 1;
    else if (IS_ATOM_INT(_3374) && IS_ATOM_INT(_3375))
    _3376 = 0;
    else
    _3376 = (compare(_3374, _3375) == 0);
    _3374 = NOVALUE;
    _3375 = NOVALUE;
    if (_3376 == 0)
    {
        _3376 = NOVALUE;
        goto L11; // [296] 374
    }
    else{
        _3376 = NOVALUE;
    }

    /** 			if match(quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3377 = (int)*(((s1_ptr)_2)->base + 1);
    _3378 = e_match_from(_3377, _text_in_6259, 1);
    _3377 = NOVALUE;
    if (_3378 == 0)
    {
        _3378 = NOVALUE;
        goto L12; // [310] 562
    }
    else{
        _3378 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3379 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3379)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3379)) {
        Prepend(&_3380, _3379, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3380, _esc_6262, _3379);
    }
    _3379 = NOVALUE;
    _3381 = e_match_from(_3380, _text_in_6259, 1);
    DeRefDS(_3380);
    _3380 = NOVALUE;
    if (_3381 == 0)
    {
        _3381 = NOVALUE;
        goto L13; // [328] 347
    }
    else{
        _3381 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc, text_in, esc & esc)*/
    Concat((object_ptr)&_3382, _esc_6262, _esc_6262);
    RefDS(_text_in_6259);
    _0 = _text_in_6259;
    _text_in_6259 = _6match_replace(_esc_6262, _text_in_6259, _3382, 0);
    DeRefDS(_0);
    _3382 = NOVALUE;
L13: 

    /** 				text_in = search:match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3384 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3385 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3385)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3385)) {
        Prepend(&_3386, _3385, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3386, _esc_6262, _3385);
    }
    _3385 = NOVALUE;
    Ref(_3384);
    RefDS(_text_in_6259);
    _0 = _text_in_6259;
    _text_in_6259 = _6match_replace(_3384, _text_in_6259, _3386, 0);
    DeRefDS(_0);
    _3384 = NOVALUE;
    _3386 = NOVALUE;
    goto L12; // [371] 562
L11: 

    /** 			if match(quote_pair[1], text_in) or*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3388 = (int)*(((s1_ptr)_2)->base + 1);
    _3389 = e_match_from(_3388, _text_in_6259, 1);
    _3388 = NOVALUE;
    if (_3389 != 0) {
        goto L14; // [385] 403
    }
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3391 = (int)*(((s1_ptr)_2)->base + 2);
    _3392 = e_match_from(_3391, _text_in_6259, 1);
    _3391 = NOVALUE;
    if (_3392 == 0)
    {
        _3392 = NOVALUE;
        goto L15; // [399] 475
    }
    else{
        _3392 = NOVALUE;
    }
L14: 

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3393 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3393)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3393)) {
        Prepend(&_3394, _3393, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3394, _esc_6262, _3393);
    }
    _3393 = NOVALUE;
    _3395 = e_match_from(_3394, _text_in_6259, 1);
    DeRefDS(_3394);
    _3394 = NOVALUE;
    if (_3395 == 0)
    {
        _3395 = NOVALUE;
        goto L16; // [418] 451
    }
    else{
        _3395 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[1], text_in, esc & esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3396 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3396)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3396)) {
        Prepend(&_3397, _3396, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3397, _esc_6262, _3396);
    }
    _3396 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3398 = (int)*(((s1_ptr)_2)->base + 1);
    {
        int concat_list[3];

        concat_list[0] = _3398;
        concat_list[1] = _esc_6262;
        concat_list[2] = _esc_6262;
        Concat_N((object_ptr)&_3399, concat_list, 3);
    }
    _3398 = NOVALUE;
    RefDS(_text_in_6259);
    _0 = _text_in_6259;
    _text_in_6259 = _6match_replace(_3397, _text_in_6259, _3399, 0);
    DeRefDS(_0);
    _3397 = NOVALUE;
    _3399 = NOVALUE;
L16: 

    /** 				text_in = match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3401 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3402 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3402)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3402)) {
        Prepend(&_3403, _3402, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3403, _esc_6262, _3402);
    }
    _3402 = NOVALUE;
    Ref(_3401);
    RefDS(_text_in_6259);
    _0 = _text_in_6259;
    _text_in_6259 = _6match_replace(_3401, _text_in_6259, _3403, 0);
    DeRefDS(_0);
    _3401 = NOVALUE;
    _3403 = NOVALUE;
L15: 

    /** 			if match(quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3405 = (int)*(((s1_ptr)_2)->base + 2);
    _3406 = e_match_from(_3405, _text_in_6259, 1);
    _3405 = NOVALUE;
    if (_3406 == 0)
    {
        _3406 = NOVALUE;
        goto L17; // [486] 561
    }
    else{
        _3406 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3407 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3407)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3407)) {
        Prepend(&_3408, _3407, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3408, _esc_6262, _3407);
    }
    _3407 = NOVALUE;
    _3409 = e_match_from(_3408, _text_in_6259, 1);
    DeRefDS(_3408);
    _3408 = NOVALUE;
    if (_3409 == 0)
    {
        _3409 = NOVALUE;
        goto L18; // [504] 537
    }
    else{
        _3409 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[2], text_in, esc & esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3410 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3410)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3410)) {
        Prepend(&_3411, _3410, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3411, _esc_6262, _3410);
    }
    _3410 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3412 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _3412;
        concat_list[1] = _esc_6262;
        concat_list[2] = _esc_6262;
        Concat_N((object_ptr)&_3413, concat_list, 3);
    }
    _3412 = NOVALUE;
    RefDS(_text_in_6259);
    _0 = _text_in_6259;
    _text_in_6259 = _6match_replace(_3411, _text_in_6259, _3413, 0);
    DeRefDS(_0);
    _3411 = NOVALUE;
    _3413 = NOVALUE;
L18: 

    /** 				text_in = search:match_replace(quote_pair[2], text_in, esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3415 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3416 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_6262) && IS_ATOM(_3416)) {
    }
    else if (IS_ATOM(_esc_6262) && IS_SEQUENCE(_3416)) {
        Prepend(&_3417, _3416, _esc_6262);
    }
    else {
        Concat((object_ptr)&_3417, _esc_6262, _3416);
    }
    _3416 = NOVALUE;
    Ref(_3415);
    RefDS(_text_in_6259);
    _0 = _text_in_6259;
    _text_in_6259 = _6match_replace(_3415, _text_in_6259, _3417, 0);
    DeRefDS(_0);
    _3415 = NOVALUE;
    _3417 = NOVALUE;
L17: 
L12: 
LE: 

    /** 	return quote_pair[1] & text_in & quote_pair[2]*/
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3419 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_6260);
    _3420 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _3420;
        concat_list[1] = _text_in_6259;
        concat_list[2] = _3419;
        Concat_N((object_ptr)&_3421, concat_list, 3);
    }
    _3420 = NOVALUE;
    _3419 = NOVALUE;
    DeRefDS(_text_in_6259);
    DeRef(_quote_pair_6260);
    DeRef(_sp_6264);
    return _3421;
    ;
}


int _16dequote(int _text_in_6377, int _quote_pairs_6378, int _esc_6381)
{
    int _pos_6446 = NOVALUE;
    int _3499 = NOVALUE;
    int _3498 = NOVALUE;
    int _3497 = NOVALUE;
    int _3496 = NOVALUE;
    int _3495 = NOVALUE;
    int _3494 = NOVALUE;
    int _3493 = NOVALUE;
    int _3492 = NOVALUE;
    int _3491 = NOVALUE;
    int _3490 = NOVALUE;
    int _3489 = NOVALUE;
    int _3487 = NOVALUE;
    int _3486 = NOVALUE;
    int _3485 = NOVALUE;
    int _3484 = NOVALUE;
    int _3483 = NOVALUE;
    int _3482 = NOVALUE;
    int _3481 = NOVALUE;
    int _3480 = NOVALUE;
    int _3479 = NOVALUE;
    int _3478 = NOVALUE;
    int _3477 = NOVALUE;
    int _3474 = NOVALUE;
    int _3473 = NOVALUE;
    int _3472 = NOVALUE;
    int _3471 = NOVALUE;
    int _3470 = NOVALUE;
    int _3469 = NOVALUE;
    int _3468 = NOVALUE;
    int _3467 = NOVALUE;
    int _3466 = NOVALUE;
    int _3465 = NOVALUE;
    int _3464 = NOVALUE;
    int _3463 = NOVALUE;
    int _3462 = NOVALUE;
    int _3461 = NOVALUE;
    int _3460 = NOVALUE;
    int _3459 = NOVALUE;
    int _3457 = NOVALUE;
    int _3456 = NOVALUE;
    int _3455 = NOVALUE;
    int _3454 = NOVALUE;
    int _3453 = NOVALUE;
    int _3452 = NOVALUE;
    int _3451 = NOVALUE;
    int _3450 = NOVALUE;
    int _3449 = NOVALUE;
    int _3448 = NOVALUE;
    int _3447 = NOVALUE;
    int _3446 = NOVALUE;
    int _3445 = NOVALUE;
    int _3444 = NOVALUE;
    int _3443 = NOVALUE;
    int _3442 = NOVALUE;
    int _3441 = NOVALUE;
    int _3440 = NOVALUE;
    int _3438 = NOVALUE;
    int _3436 = NOVALUE;
    int _3434 = NOVALUE;
    int _3433 = NOVALUE;
    int _3431 = NOVALUE;
    int _3429 = NOVALUE;
    int _3428 = NOVALUE;
    int _3427 = NOVALUE;
    int _3426 = NOVALUE;
    int _3424 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_esc_6381)) {
        _1 = (long)(DBL_PTR(_esc_6381)->dbl);
        if (UNIQUE(DBL_PTR(_esc_6381)) && (DBL_PTR(_esc_6381)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_esc_6381);
        _esc_6381 = _1;
    }

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_6377)){
            _3424 = SEQ_PTR(_text_in_6377)->length;
    }
    else {
        _3424 = 1;
    }
    if (_3424 != 0)
    goto L1; // [12] 23

    /** 		return text_in*/
    DeRef(_quote_pairs_6378);
    return _text_in_6377;
L1: 

    /** 	if atom(quote_pairs) then*/
    _3426 = IS_ATOM(_quote_pairs_6378);
    if (_3426 == 0)
    {
        _3426 = NOVALUE;
        goto L2; // [28] 52
    }
    else{
        _3426 = NOVALUE;
    }

    /** 		quote_pairs = {{{quote_pairs}, {quote_pairs}}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pairs_6378);
    *((int *)(_2+4)) = _quote_pairs_6378;
    _3427 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pairs_6378);
    *((int *)(_2+4)) = _quote_pairs_6378;
    _3428 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3427;
    ((int *)_2)[2] = _3428;
    _3429 = MAKE_SEQ(_1);
    _3428 = NOVALUE;
    _3427 = NOVALUE;
    _0 = _quote_pairs_6378;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3429;
    _quote_pairs_6378 = MAKE_SEQ(_1);
    DeRef(_0);
    _3429 = NOVALUE;
    goto L3; // [49] 99
L2: 

    /** 	elsif length(quote_pairs) = 1 then*/
    if (IS_SEQUENCE(_quote_pairs_6378)){
            _3431 = SEQ_PTR(_quote_pairs_6378)->length;
    }
    else {
        _3431 = 1;
    }
    if (_3431 != 1)
    goto L4; // [57] 78

    /** 		quote_pairs = {quote_pairs[1], quote_pairs[1]}*/
    _2 = (int)SEQ_PTR(_quote_pairs_6378);
    _3433 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pairs_6378);
    _3434 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3434);
    Ref(_3433);
    DeRef(_quote_pairs_6378);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3433;
    ((int *)_2)[2] = _3434;
    _quote_pairs_6378 = MAKE_SEQ(_1);
    _3434 = NOVALUE;
    _3433 = NOVALUE;
    goto L3; // [75] 99
L4: 

    /** 	elsif length(quote_pairs) = 0 then*/
    if (IS_SEQUENCE(_quote_pairs_6378)){
            _3436 = SEQ_PTR(_quote_pairs_6378)->length;
    }
    else {
        _3436 = 1;
    }
    if (_3436 != 0)
    goto L5; // [83] 98

    /** 		quote_pairs = {{"\"", "\""}}*/
    RefDS(_3328);
    RefDS(_3328);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3328;
    ((int *)_2)[2] = _3328;
    _3438 = MAKE_SEQ(_1);
    _0 = _quote_pairs_6378;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3438;
    _quote_pairs_6378 = MAKE_SEQ(_1);
    DeRef(_0);
    _3438 = NOVALUE;
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_6377);
    _3440 = (int)*(((s1_ptr)_2)->base + 1);
    _3441 = IS_SEQUENCE(_3440);
    _3440 = NOVALUE;
    if (_3441 == 0)
    {
        _3441 = NOVALUE;
        goto L6; // [108] 172
    }
    else{
        _3441 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_6377)){
            _3442 = SEQ_PTR(_text_in_6377)->length;
    }
    else {
        _3442 = 1;
    }
    {
        int _i_6406;
        _i_6406 = 1;
L7: 
        if (_i_6406 > _3442){
            goto L8; // [116] 165
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_6377);
        _3443 = (int)*(((s1_ptr)_2)->base + _i_6406);
        _3444 = IS_SEQUENCE(_3443);
        _3443 = NOVALUE;
        if (_3444 == 0)
        {
            _3444 = NOVALUE;
            goto L9; // [132] 158
        }
        else{
            _3444 = NOVALUE;
        }

        /** 				text_in[i] = dequote(text_in[i], quote_pairs, esc)*/
        _2 = (int)SEQ_PTR(_text_in_6377);
        _3445 = (int)*(((s1_ptr)_2)->base + _i_6406);
        Ref(_quote_pairs_6378);
        DeRef(_3446);
        _3446 = _quote_pairs_6378;
        DeRef(_3447);
        _3447 = _esc_6381;
        Ref(_3445);
        _3448 = _16dequote(_3445, _3446, _3447);
        _3445 = NOVALUE;
        _3446 = NOVALUE;
        _3447 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_6377);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_6377 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_6406);
        _1 = *(int *)_2;
        *(int *)_2 = _3448;
        if( _1 != _3448 ){
            DeRef(_1);
        }
        _3448 = NOVALUE;
L9: 

        /** 		end for*/
        _i_6406 = _i_6406 + 1;
        goto L7; // [160] 123
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pairs_6378);
    return _text_in_6377;
L6: 

    /** 	for i = 1 to length(quote_pairs) do*/
    if (IS_SEQUENCE(_quote_pairs_6378)){
            _3449 = SEQ_PTR(_quote_pairs_6378)->length;
    }
    else {
        _3449 = 1;
    }
    {
        int _i_6416;
        _i_6416 = 1;
LA: 
        if (_i_6416 > _3449){
            goto LB; // [177] 474
        }

        /** 		if length(text_in) >= length(quote_pairs[i][1]) + length(quote_pairs[i][2]) then*/
        if (IS_SEQUENCE(_text_in_6377)){
                _3450 = SEQ_PTR(_text_in_6377)->length;
        }
        else {
            _3450 = 1;
        }
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3451 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3451);
        _3452 = (int)*(((s1_ptr)_2)->base + 1);
        _3451 = NOVALUE;
        if (IS_SEQUENCE(_3452)){
                _3453 = SEQ_PTR(_3452)->length;
        }
        else {
            _3453 = 1;
        }
        _3452 = NOVALUE;
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3454 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3454);
        _3455 = (int)*(((s1_ptr)_2)->base + 2);
        _3454 = NOVALUE;
        if (IS_SEQUENCE(_3455)){
                _3456 = SEQ_PTR(_3455)->length;
        }
        else {
            _3456 = 1;
        }
        _3455 = NOVALUE;
        _3457 = _3453 + _3456;
        if ((long)((unsigned long)_3457 + (unsigned long)HIGH_BITS) >= 0) 
        _3457 = NewDouble((double)_3457);
        _3453 = NOVALUE;
        _3456 = NOVALUE;
        if (binary_op_a(LESS, _3450, _3457)){
            _3450 = NOVALUE;
            DeRef(_3457);
            _3457 = NOVALUE;
            goto LC; // [215] 467
        }
        _3450 = NOVALUE;
        DeRef(_3457);
        _3457 = NOVALUE;

        /** 			if search:begins(quote_pairs[i][1], text_in) and search:ends(quote_pairs[i][2], text_in) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3459 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3459);
        _3460 = (int)*(((s1_ptr)_2)->base + 1);
        _3459 = NOVALUE;
        Ref(_3460);
        RefDS(_text_in_6377);
        _3461 = _6begins(_3460, _text_in_6377);
        _3460 = NOVALUE;
        if (IS_ATOM_INT(_3461)) {
            if (_3461 == 0) {
                goto LD; // [234] 464
            }
        }
        else {
            if (DBL_PTR(_3461)->dbl == 0.0) {
                goto LD; // [234] 464
            }
        }
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3463 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3463);
        _3464 = (int)*(((s1_ptr)_2)->base + 2);
        _3463 = NOVALUE;
        Ref(_3464);
        RefDS(_text_in_6377);
        _3465 = _6ends(_3464, _text_in_6377);
        _3464 = NOVALUE;
        if (_3465 == 0) {
            DeRef(_3465);
            _3465 = NOVALUE;
            goto LD; // [252] 464
        }
        else {
            if (!IS_ATOM_INT(_3465) && DBL_PTR(_3465)->dbl == 0.0){
                DeRef(_3465);
                _3465 = NOVALUE;
                goto LD; // [252] 464
            }
            DeRef(_3465);
            _3465 = NOVALUE;
        }
        DeRef(_3465);
        _3465 = NOVALUE;

        /** 				text_in = text_in[1 + length(quote_pairs[i][1]) .. $ - length(quote_pairs[i][2])]*/
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3466 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3466);
        _3467 = (int)*(((s1_ptr)_2)->base + 1);
        _3466 = NOVALUE;
        if (IS_SEQUENCE(_3467)){
                _3468 = SEQ_PTR(_3467)->length;
        }
        else {
            _3468 = 1;
        }
        _3467 = NOVALUE;
        _3469 = _3468 + 1;
        _3468 = NOVALUE;
        if (IS_SEQUENCE(_text_in_6377)){
                _3470 = SEQ_PTR(_text_in_6377)->length;
        }
        else {
            _3470 = 1;
        }
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3471 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3471);
        _3472 = (int)*(((s1_ptr)_2)->base + 2);
        _3471 = NOVALUE;
        if (IS_SEQUENCE(_3472)){
                _3473 = SEQ_PTR(_3472)->length;
        }
        else {
            _3473 = 1;
        }
        _3472 = NOVALUE;
        _3474 = _3470 - _3473;
        _3470 = NOVALUE;
        _3473 = NOVALUE;
        rhs_slice_target = (object_ptr)&_text_in_6377;
        RHS_Slice(_text_in_6377, _3469, _3474);

        /** 				integer pos = 1*/
        _pos_6446 = 1;

        /** 				while pos > 0 with entry do*/
        goto LE; // [304] 443
LF: 
        if (_pos_6446 <= 0)
        goto L10; // [307] 457

        /** 					if search:begins(quote_pairs[i][1], text_in[pos+1 .. $]) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3477 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3477);
        _3478 = (int)*(((s1_ptr)_2)->base + 1);
        _3477 = NOVALUE;
        _3479 = _pos_6446 + 1;
        if (_3479 > MAXINT){
            _3479 = NewDouble((double)_3479);
        }
        if (IS_SEQUENCE(_text_in_6377)){
                _3480 = SEQ_PTR(_text_in_6377)->length;
        }
        else {
            _3480 = 1;
        }
        rhs_slice_target = (object_ptr)&_3481;
        RHS_Slice(_text_in_6377, _3479, _3480);
        Ref(_3478);
        _3482 = _6begins(_3478, _3481);
        _3478 = NOVALUE;
        _3481 = NOVALUE;
        if (_3482 == 0) {
            DeRef(_3482);
            _3482 = NOVALUE;
            goto L11; // [338] 371
        }
        else {
            if (!IS_ATOM_INT(_3482) && DBL_PTR(_3482)->dbl == 0.0){
                DeRef(_3482);
                _3482 = NOVALUE;
                goto L11; // [338] 371
            }
            DeRef(_3482);
            _3482 = NOVALUE;
        }
        DeRef(_3482);
        _3482 = NOVALUE;

        /** 						text_in = text_in[1 .. pos-1] & text_in[pos + 1 .. $]*/
        _3483 = _pos_6446 - 1;
        rhs_slice_target = (object_ptr)&_3484;
        RHS_Slice(_text_in_6377, 1, _3483);
        _3485 = _pos_6446 + 1;
        if (_3485 > MAXINT){
            _3485 = NewDouble((double)_3485);
        }
        if (IS_SEQUENCE(_text_in_6377)){
                _3486 = SEQ_PTR(_text_in_6377)->length;
        }
        else {
            _3486 = 1;
        }
        rhs_slice_target = (object_ptr)&_3487;
        RHS_Slice(_text_in_6377, _3485, _3486);
        Concat((object_ptr)&_text_in_6377, _3484, _3487);
        DeRefDS(_3484);
        _3484 = NOVALUE;
        DeRef(_3484);
        _3484 = NOVALUE;
        DeRefDS(_3487);
        _3487 = NOVALUE;
        goto L12; // [368] 440
L11: 

        /** 					elsif search:begins(quote_pairs[i][2], text_in[pos+1 .. $]) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_6378);
        _3489 = (int)*(((s1_ptr)_2)->base + _i_6416);
        _2 = (int)SEQ_PTR(_3489);
        _3490 = (int)*(((s1_ptr)_2)->base + 2);
        _3489 = NOVALUE;
        _3491 = _pos_6446 + 1;
        if (_3491 > MAXINT){
            _3491 = NewDouble((double)_3491);
        }
        if (IS_SEQUENCE(_text_in_6377)){
                _3492 = SEQ_PTR(_text_in_6377)->length;
        }
        else {
            _3492 = 1;
        }
        rhs_slice_target = (object_ptr)&_3493;
        RHS_Slice(_text_in_6377, _3491, _3492);
        Ref(_3490);
        _3494 = _6begins(_3490, _3493);
        _3490 = NOVALUE;
        _3493 = NOVALUE;
        if (_3494 == 0) {
            DeRef(_3494);
            _3494 = NOVALUE;
            goto L13; // [398] 431
        }
        else {
            if (!IS_ATOM_INT(_3494) && DBL_PTR(_3494)->dbl == 0.0){
                DeRef(_3494);
                _3494 = NOVALUE;
                goto L13; // [398] 431
            }
            DeRef(_3494);
            _3494 = NOVALUE;
        }
        DeRef(_3494);
        _3494 = NOVALUE;

        /** 						text_in = text_in[1 .. pos-1] & text_in[pos + 1 .. $]*/
        _3495 = _pos_6446 - 1;
        rhs_slice_target = (object_ptr)&_3496;
        RHS_Slice(_text_in_6377, 1, _3495);
        _3497 = _pos_6446 + 1;
        if (_3497 > MAXINT){
            _3497 = NewDouble((double)_3497);
        }
        if (IS_SEQUENCE(_text_in_6377)){
                _3498 = SEQ_PTR(_text_in_6377)->length;
        }
        else {
            _3498 = 1;
        }
        rhs_slice_target = (object_ptr)&_3499;
        RHS_Slice(_text_in_6377, _3497, _3498);
        Concat((object_ptr)&_text_in_6377, _3496, _3499);
        DeRefDS(_3496);
        _3496 = NOVALUE;
        DeRef(_3496);
        _3496 = NOVALUE;
        DeRefDS(_3499);
        _3499 = NOVALUE;
        goto L12; // [428] 440
L13: 

        /** 						pos += 1*/
        _pos_6446 = _pos_6446 + 1;
L12: 

        /** 				entry*/
LE: 

        /** 					pos = find(esc, text_in, pos)*/
        _pos_6446 = find_from(_esc_6381, _text_in_6377, _pos_6446);

        /** 				end while*/
        goto LF; // [454] 307
L10: 

        /** 				exit*/
        goto LB; // [461] 474
LD: 
LC: 

        /** 	end for*/
        _i_6416 = _i_6416 + 1;
        goto LA; // [469] 184
LB: 
        ;
    }

    /** 	return text_in*/
    DeRef(_quote_pairs_6378);
    _3452 = NOVALUE;
    _3455 = NOVALUE;
    _3467 = NOVALUE;
    DeRef(_3461);
    _3461 = NOVALUE;
    DeRef(_3469);
    _3469 = NOVALUE;
    _3472 = NOVALUE;
    DeRef(_3474);
    _3474 = NOVALUE;
    DeRef(_3483);
    _3483 = NOVALUE;
    DeRef(_3479);
    _3479 = NOVALUE;
    DeRef(_3495);
    _3495 = NOVALUE;
    DeRef(_3485);
    _3485 = NOVALUE;
    DeRef(_3491);
    _3491 = NOVALUE;
    DeRef(_3497);
    _3497 = NOVALUE;
    return _text_in_6377;
    ;
}


int _16format(int _format_pattern_6480, int _arg_list_6481)
{
    int _result_6482 = NOVALUE;
    int _in_token_6483 = NOVALUE;
    int _tch_6484 = NOVALUE;
    int _i_6485 = NOVALUE;
    int _tend_6486 = NOVALUE;
    int _cap_6487 = NOVALUE;
    int _align_6488 = NOVALUE;
    int _psign_6489 = NOVALUE;
    int _msign_6490 = NOVALUE;
    int _zfill_6491 = NOVALUE;
    int _bwz_6492 = NOVALUE;
    int _spacer_6493 = NOVALUE;
    int _alt_6494 = NOVALUE;
    int _width_6495 = NOVALUE;
    int _decs_6496 = NOVALUE;
    int _pos_6497 = NOVALUE;
    int _argn_6498 = NOVALUE;
    int _argl_6499 = NOVALUE;
    int _trimming_6500 = NOVALUE;
    int _hexout_6501 = NOVALUE;
    int _binout_6502 = NOVALUE;
    int _tsep_6503 = NOVALUE;
    int _istext_6504 = NOVALUE;
    int _prevargv_6505 = NOVALUE;
    int _currargv_6506 = NOVALUE;
    int _idname_6507 = NOVALUE;
    int _envsym_6508 = NOVALUE;
    int _envvar_6509 = NOVALUE;
    int _ep_6510 = NOVALUE;
    int _pflag_6511 = NOVALUE;
    int _count_6512 = NOVALUE;
    int _sp_6591 = NOVALUE;
    int _sp_6627 = NOVALUE;
    int _argtext_6674 = NOVALUE;
    int _tempv_6910 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2648_6965 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2645_6964 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2706_6972 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2703_6971 = NOVALUE;
    int _x_inlined_pretty_sprint_at_2700_6970 = NOVALUE;
    int _msg_inlined_crash_at_2860_6994 = NOVALUE;
    int _dpos_7056 = NOVALUE;
    int _dist_7057 = NOVALUE;
    int _bracketed_7058 = NOVALUE;
    int _3952 = NOVALUE;
    int _3951 = NOVALUE;
    int _3950 = NOVALUE;
    int _3948 = NOVALUE;
    int _3947 = NOVALUE;
    int _3946 = NOVALUE;
    int _3943 = NOVALUE;
    int _3942 = NOVALUE;
    int _3939 = NOVALUE;
    int _3937 = NOVALUE;
    int _3934 = NOVALUE;
    int _3933 = NOVALUE;
    int _3932 = NOVALUE;
    int _3929 = NOVALUE;
    int _3926 = NOVALUE;
    int _3925 = NOVALUE;
    int _3924 = NOVALUE;
    int _3923 = NOVALUE;
    int _3920 = NOVALUE;
    int _3919 = NOVALUE;
    int _3918 = NOVALUE;
    int _3915 = NOVALUE;
    int _3913 = NOVALUE;
    int _3910 = NOVALUE;
    int _3909 = NOVALUE;
    int _3908 = NOVALUE;
    int _3907 = NOVALUE;
    int _3904 = NOVALUE;
    int _3899 = NOVALUE;
    int _3898 = NOVALUE;
    int _3897 = NOVALUE;
    int _3896 = NOVALUE;
    int _3894 = NOVALUE;
    int _3893 = NOVALUE;
    int _3892 = NOVALUE;
    int _3891 = NOVALUE;
    int _3890 = NOVALUE;
    int _3889 = NOVALUE;
    int _3888 = NOVALUE;
    int _3883 = NOVALUE;
    int _3879 = NOVALUE;
    int _3878 = NOVALUE;
    int _3876 = NOVALUE;
    int _3874 = NOVALUE;
    int _3873 = NOVALUE;
    int _3872 = NOVALUE;
    int _3871 = NOVALUE;
    int _3870 = NOVALUE;
    int _3867 = NOVALUE;
    int _3865 = NOVALUE;
    int _3864 = NOVALUE;
    int _3863 = NOVALUE;
    int _3862 = NOVALUE;
    int _3859 = NOVALUE;
    int _3858 = NOVALUE;
    int _3856 = NOVALUE;
    int _3855 = NOVALUE;
    int _3854 = NOVALUE;
    int _3853 = NOVALUE;
    int _3852 = NOVALUE;
    int _3849 = NOVALUE;
    int _3848 = NOVALUE;
    int _3847 = NOVALUE;
    int _3844 = NOVALUE;
    int _3843 = NOVALUE;
    int _3841 = NOVALUE;
    int _3837 = NOVALUE;
    int _3836 = NOVALUE;
    int _3834 = NOVALUE;
    int _3833 = NOVALUE;
    int _3825 = NOVALUE;
    int _3821 = NOVALUE;
    int _3819 = NOVALUE;
    int _3818 = NOVALUE;
    int _3817 = NOVALUE;
    int _3814 = NOVALUE;
    int _3812 = NOVALUE;
    int _3811 = NOVALUE;
    int _3810 = NOVALUE;
    int _3808 = NOVALUE;
    int _3807 = NOVALUE;
    int _3806 = NOVALUE;
    int _3805 = NOVALUE;
    int _3802 = NOVALUE;
    int _3801 = NOVALUE;
    int _3800 = NOVALUE;
    int _3798 = NOVALUE;
    int _3797 = NOVALUE;
    int _3796 = NOVALUE;
    int _3795 = NOVALUE;
    int _3793 = NOVALUE;
    int _3791 = NOVALUE;
    int _3789 = NOVALUE;
    int _3787 = NOVALUE;
    int _3785 = NOVALUE;
    int _3783 = NOVALUE;
    int _3782 = NOVALUE;
    int _3781 = NOVALUE;
    int _3780 = NOVALUE;
    int _3779 = NOVALUE;
    int _3778 = NOVALUE;
    int _3776 = NOVALUE;
    int _3775 = NOVALUE;
    int _3773 = NOVALUE;
    int _3772 = NOVALUE;
    int _3770 = NOVALUE;
    int _3768 = NOVALUE;
    int _3767 = NOVALUE;
    int _3764 = NOVALUE;
    int _3762 = NOVALUE;
    int _3758 = NOVALUE;
    int _3756 = NOVALUE;
    int _3755 = NOVALUE;
    int _3754 = NOVALUE;
    int _3752 = NOVALUE;
    int _3751 = NOVALUE;
    int _3750 = NOVALUE;
    int _3749 = NOVALUE;
    int _3748 = NOVALUE;
    int _3746 = NOVALUE;
    int _3744 = NOVALUE;
    int _3743 = NOVALUE;
    int _3742 = NOVALUE;
    int _3741 = NOVALUE;
    int _3737 = NOVALUE;
    int _3734 = NOVALUE;
    int _3733 = NOVALUE;
    int _3730 = NOVALUE;
    int _3729 = NOVALUE;
    int _3728 = NOVALUE;
    int _3726 = NOVALUE;
    int _3725 = NOVALUE;
    int _3724 = NOVALUE;
    int _3723 = NOVALUE;
    int _3721 = NOVALUE;
    int _3719 = NOVALUE;
    int _3718 = NOVALUE;
    int _3717 = NOVALUE;
    int _3716 = NOVALUE;
    int _3715 = NOVALUE;
    int _3714 = NOVALUE;
    int _3712 = NOVALUE;
    int _3711 = NOVALUE;
    int _3710 = NOVALUE;
    int _3708 = NOVALUE;
    int _3707 = NOVALUE;
    int _3706 = NOVALUE;
    int _3705 = NOVALUE;
    int _3703 = NOVALUE;
    int _3700 = NOVALUE;
    int _3699 = NOVALUE;
    int _3697 = NOVALUE;
    int _3696 = NOVALUE;
    int _3694 = NOVALUE;
    int _3691 = NOVALUE;
    int _3690 = NOVALUE;
    int _3687 = NOVALUE;
    int _3685 = NOVALUE;
    int _3681 = NOVALUE;
    int _3679 = NOVALUE;
    int _3678 = NOVALUE;
    int _3677 = NOVALUE;
    int _3675 = NOVALUE;
    int _3673 = NOVALUE;
    int _3672 = NOVALUE;
    int _3671 = NOVALUE;
    int _3670 = NOVALUE;
    int _3669 = NOVALUE;
    int _3667 = NOVALUE;
    int _3665 = NOVALUE;
    int _3664 = NOVALUE;
    int _3663 = NOVALUE;
    int _3662 = NOVALUE;
    int _3660 = NOVALUE;
    int _3657 = NOVALUE;
    int _3655 = NOVALUE;
    int _3654 = NOVALUE;
    int _3653 = NOVALUE;
    int _3652 = NOVALUE;
    int _3651 = NOVALUE;
    int _3649 = NOVALUE;
    int _3648 = NOVALUE;
    int _3647 = NOVALUE;
    int _3645 = NOVALUE;
    int _3644 = NOVALUE;
    int _3643 = NOVALUE;
    int _3642 = NOVALUE;
    int _3640 = NOVALUE;
    int _3639 = NOVALUE;
    int _3638 = NOVALUE;
    int _3635 = NOVALUE;
    int _3634 = NOVALUE;
    int _3633 = NOVALUE;
    int _3632 = NOVALUE;
    int _3630 = NOVALUE;
    int _3629 = NOVALUE;
    int _3628 = NOVALUE;
    int _3627 = NOVALUE;
    int _3626 = NOVALUE;
    int _3623 = NOVALUE;
    int _3622 = NOVALUE;
    int _3621 = NOVALUE;
    int _3620 = NOVALUE;
    int _3618 = NOVALUE;
    int _3617 = NOVALUE;
    int _3616 = NOVALUE;
    int _3614 = NOVALUE;
    int _3613 = NOVALUE;
    int _3612 = NOVALUE;
    int _3610 = NOVALUE;
    int _3603 = NOVALUE;
    int _3601 = NOVALUE;
    int _3600 = NOVALUE;
    int _3593 = NOVALUE;
    int _3590 = NOVALUE;
    int _3586 = NOVALUE;
    int _3584 = NOVALUE;
    int _3583 = NOVALUE;
    int _3580 = NOVALUE;
    int _3578 = NOVALUE;
    int _3576 = NOVALUE;
    int _3573 = NOVALUE;
    int _3571 = NOVALUE;
    int _3570 = NOVALUE;
    int _3569 = NOVALUE;
    int _3568 = NOVALUE;
    int _3567 = NOVALUE;
    int _3564 = NOVALUE;
    int _3561 = NOVALUE;
    int _3560 = NOVALUE;
    int _3559 = NOVALUE;
    int _3556 = NOVALUE;
    int _3554 = NOVALUE;
    int _3552 = NOVALUE;
    int _3549 = NOVALUE;
    int _3548 = NOVALUE;
    int _3541 = NOVALUE;
    int _3538 = NOVALUE;
    int _3537 = NOVALUE;
    int _3529 = NOVALUE;
    int _3518 = NOVALUE;
    int _3515 = NOVALUE;
    int _3505 = NOVALUE;
    int _3503 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(arg_list) then*/
    _3503 = IS_ATOM(_arg_list_6481);
    if (_3503 == 0)
    {
        _3503 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _3503 = NOVALUE;
    }

    /** 		arg_list = {arg_list}*/
    _0 = _arg_list_6481;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_arg_list_6481);
    *((int *)(_2+4)) = _arg_list_6481;
    _arg_list_6481 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	result = ""*/
    RefDS(_5);
    DeRef(_result_6482);
    _result_6482 = _5;

    /** 	in_token = 0*/
    _in_token_6483 = 0;

    /** 	i = 0*/
    _i_6485 = 0;

    /** 	tend = 0*/
    _tend_6486 = 0;

    /** 	argl = 0*/
    _argl_6499 = 0;

    /** 	spacer = 0*/
    _spacer_6493 = 0;

    /** 	prevargv = 0*/
    DeRef(_prevargv_6505);
    _prevargv_6505 = 0;

    /**     while i < length(format_pattern) do*/
L2: 
    if (IS_SEQUENCE(_format_pattern_6480)){
            _3505 = SEQ_PTR(_format_pattern_6480)->length;
    }
    else {
        _3505 = 1;
    }
    if (_i_6485 >= _3505)
    goto L3; // [73] 3791

    /**     	i += 1*/
    _i_6485 = _i_6485 + 1;

    /**     	tch = format_pattern[i]*/
    _2 = (int)SEQ_PTR(_format_pattern_6480);
    _tch_6484 = (int)*(((s1_ptr)_2)->base + _i_6485);
    if (!IS_ATOM_INT(_tch_6484))
    _tch_6484 = (long)DBL_PTR(_tch_6484)->dbl;

    /**     	if not in_token then*/
    if (_in_token_6483 != 0)
    goto L4; // [95] 260

    /**     		if tch = '[' then*/
    if (_tch_6484 != 91)
    goto L5; // [100] 250

    /**     			in_token = 1*/
    _in_token_6483 = 1;

    /**     			tend = 0*/
    _tend_6486 = 0;

    /** 				cap = 0*/
    _cap_6487 = 0;

    /** 				align = 0*/
    _align_6488 = 0;

    /** 				psign = 0*/
    _psign_6489 = 0;

    /** 				msign = 0*/
    _msign_6490 = 0;

    /** 				zfill = 0*/
    _zfill_6491 = 0;

    /** 				bwz = 0*/
    _bwz_6492 = 0;

    /** 				spacer = 0*/
    _spacer_6493 = 0;

    /** 				alt = 0*/
    _alt_6494 = 0;

    /**     			width = 0*/
    _width_6495 = 0;

    /**     			decs = -1*/
    _decs_6496 = -1;

    /**     			argn = 0*/
    _argn_6498 = 0;

    /**     			hexout = 0*/
    _hexout_6501 = 0;

    /**     			binout = 0*/
    _binout_6502 = 0;

    /**     			trimming = 0*/
    _trimming_6500 = 0;

    /**     			tsep = 0*/
    _tsep_6503 = 0;

    /**     			istext = 0*/
    _istext_6504 = 0;

    /**     			idname = ""*/
    RefDS(_5);
    DeRef(_idname_6507);
    _idname_6507 = _5;

    /**     			envvar = ""*/
    RefDS(_5);
    DeRefi(_envvar_6509);
    _envvar_6509 = _5;

    /**     			envsym = ""*/
    RefDS(_5);
    DeRef(_envsym_6508);
    _envsym_6508 = _5;
    goto L2; // [247] 70
L5: 

    /**     			result &= tch*/
    Append(&_result_6482, _result_6482, _tch_6484);
    goto L2; // [257] 70
L4: 

    /** 			switch tch do*/
    _0 = _tch_6484;
    switch ( _0 ){ 

        /**     			case ']' then*/
        case 93:

        /**     				in_token = 0*/
        _in_token_6483 = 0;

        /**     				tend = i*/
        _tend_6486 = _i_6485;
        goto L6; // [285] 1218

        /**     			case '[' then*/
        case 91:

        /** 	    			result &= tch*/
        Append(&_result_6482, _result_6482, _tch_6484);

        /** 	    			while i < length(format_pattern) do*/
L7: 
        if (IS_SEQUENCE(_format_pattern_6480)){
                _3515 = SEQ_PTR(_format_pattern_6480)->length;
        }
        else {
            _3515 = 1;
        }
        if (_i_6485 >= _3515)
        goto L6; // [305] 1218

        /** 	    				i += 1*/
        _i_6485 = _i_6485 + 1;

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _3518 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (binary_op_a(NOTEQ, _3518, 93)){
            _3518 = NOVALUE;
            goto L7; // [323] 302
        }
        _3518 = NOVALUE;

        /** 	    					in_token = 0*/
        _in_token_6483 = 0;

        /** 	    					tend = 0*/
        _tend_6486 = 0;

        /** 	    					exit*/
        goto L6; // [343] 1218

        /** 	    			end while*/
        goto L7; // [348] 302
        goto L6; // [351] 1218

        /** 	    		case 'w', 'u', 'l' then*/
        case 119:
        case 117:
        case 108:

        /** 	    			cap = tch*/
        _cap_6487 = _tch_6484;
        goto L6; // [368] 1218

        /** 	    		case 'b' then*/
        case 98:

        /** 	    			bwz = 1*/
        _bwz_6492 = 1;
        goto L6; // [381] 1218

        /** 	    		case 's' then*/
        case 115:

        /** 	    			spacer = 1*/
        _spacer_6493 = 1;
        goto L6; // [394] 1218

        /** 	    		case 't' then*/
        case 116:

        /** 	    			trimming = 1*/
        _trimming_6500 = 1;
        goto L6; // [407] 1218

        /** 	    		case 'z' then*/
        case 122:

        /** 	    			zfill = 1*/
        _zfill_6491 = 1;
        goto L6; // [420] 1218

        /** 	    		case 'X' then*/
        case 88:

        /** 	    			hexout = 1*/
        _hexout_6501 = 1;
        goto L6; // [433] 1218

        /** 	    		case 'B' then*/
        case 66:

        /** 	    			binout = 1*/
        _binout_6502 = 1;
        goto L6; // [446] 1218

        /** 	    		case 'c', '<', '>' then*/
        case 99:
        case 60:
        case 62:

        /** 	    			align = tch*/
        _align_6488 = _tch_6484;
        goto L6; // [463] 1218

        /** 	    		case '+' then*/
        case 43:

        /** 	    			psign = 1*/
        _psign_6489 = 1;
        goto L6; // [476] 1218

        /** 	    		case '(' then*/
        case 40:

        /** 	    			msign = 1*/
        _msign_6490 = 1;
        goto L6; // [489] 1218

        /** 	    		case '?' then*/
        case 63:

        /** 	    			alt = 1*/
        _alt_6494 = 1;
        goto L6; // [502] 1218

        /** 	    		case 'T' then*/
        case 84:

        /** 	    			istext = 1*/
        _istext_6504 = 1;
        goto L6; // [515] 1218

        /** 	    		case ':' then*/
        case 58:

        /** 	    			while i < length(format_pattern) do*/
L8: 
        if (IS_SEQUENCE(_format_pattern_6480)){
                _3529 = SEQ_PTR(_format_pattern_6480)->length;
        }
        else {
            _3529 = 1;
        }
        if (_i_6485 >= _3529)
        goto L6; // [529] 1218

        /** 	    				i += 1*/
        _i_6485 = _i_6485 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _tch_6484 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (!IS_ATOM_INT(_tch_6484))
        _tch_6484 = (long)DBL_PTR(_tch_6484)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_6497 = find_from(_tch_6484, _3533, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_6497 != 0)
        goto L9; // [560] 577

        /** 	    					i -= 1*/
        _i_6485 = _i_6485 - 1;

        /** 	    					exit*/
        goto L6; // [574] 1218
L9: 

        /** 	    				width = width * 10 + pos - 1*/
        if (_width_6495 == (short)_width_6495)
        _3537 = _width_6495 * 10;
        else
        _3537 = NewDouble(_width_6495 * (double)10);
        if (IS_ATOM_INT(_3537)) {
            _3538 = _3537 + _pos_6497;
            if ((long)((unsigned long)_3538 + (unsigned long)HIGH_BITS) >= 0) 
            _3538 = NewDouble((double)_3538);
        }
        else {
            _3538 = NewDouble(DBL_PTR(_3537)->dbl + (double)_pos_6497);
        }
        DeRef(_3537);
        _3537 = NOVALUE;
        if (IS_ATOM_INT(_3538)) {
            _width_6495 = _3538 - 1;
        }
        else {
            _width_6495 = NewDouble(DBL_PTR(_3538)->dbl - (double)1);
        }
        DeRef(_3538);
        _3538 = NOVALUE;
        if (!IS_ATOM_INT(_width_6495)) {
            _1 = (long)(DBL_PTR(_width_6495)->dbl);
            if (UNIQUE(DBL_PTR(_width_6495)) && (DBL_PTR(_width_6495)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_width_6495);
            _width_6495 = _1;
        }

        /** 	    				if width = 0 then*/
        if (_width_6495 != 0)
        goto L8; // [599] 526

        /** 	    					zfill = '0'*/
        _zfill_6491 = 48;

        /** 	    			end while*/
        goto L8; // [613] 526
        goto L6; // [616] 1218

        /** 	    		case '.' then*/
        case 46:

        /** 	    			decs = 0*/
        _decs_6496 = 0;

        /** 	    			while i < length(format_pattern) do*/
LA: 
        if (IS_SEQUENCE(_format_pattern_6480)){
                _3541 = SEQ_PTR(_format_pattern_6480)->length;
        }
        else {
            _3541 = 1;
        }
        if (_i_6485 >= _3541)
        goto L6; // [637] 1218

        /** 	    				i += 1*/
        _i_6485 = _i_6485 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _tch_6484 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (!IS_ATOM_INT(_tch_6484))
        _tch_6484 = (long)DBL_PTR(_tch_6484)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_6497 = find_from(_tch_6484, _3533, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_6497 != 0)
        goto LB; // [668] 685

        /** 	    					i -= 1*/
        _i_6485 = _i_6485 - 1;

        /** 	    					exit*/
        goto L6; // [682] 1218
LB: 

        /** 	    				decs = decs * 10 + pos - 1*/
        if (_decs_6496 == (short)_decs_6496)
        _3548 = _decs_6496 * 10;
        else
        _3548 = NewDouble(_decs_6496 * (double)10);
        if (IS_ATOM_INT(_3548)) {
            _3549 = _3548 + _pos_6497;
            if ((long)((unsigned long)_3549 + (unsigned long)HIGH_BITS) >= 0) 
            _3549 = NewDouble((double)_3549);
        }
        else {
            _3549 = NewDouble(DBL_PTR(_3548)->dbl + (double)_pos_6497);
        }
        DeRef(_3548);
        _3548 = NOVALUE;
        if (IS_ATOM_INT(_3549)) {
            _decs_6496 = _3549 - 1;
        }
        else {
            _decs_6496 = NewDouble(DBL_PTR(_3549)->dbl - (double)1);
        }
        DeRef(_3549);
        _3549 = NOVALUE;
        if (!IS_ATOM_INT(_decs_6496)) {
            _1 = (long)(DBL_PTR(_decs_6496)->dbl);
            if (UNIQUE(DBL_PTR(_decs_6496)) && (DBL_PTR(_decs_6496)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_decs_6496);
            _decs_6496 = _1;
        }

        /** 	    			end while*/
        goto LA; // [705] 634
        goto L6; // [708] 1218

        /** 	    		case '{' then*/
        case 123:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_6591 = _i_6485 + 1;

        /** 	    			i = sp*/
        _i_6485 = _sp_6591;

        /** 	    			while i < length(format_pattern) do*/
LC: 
        if (IS_SEQUENCE(_format_pattern_6480)){
                _3552 = SEQ_PTR(_format_pattern_6480)->length;
        }
        else {
            _3552 = 1;
        }
        if (_i_6485 >= _3552)
        goto LD; // [739] 786

        /** 	    				if format_pattern[i] = '}' then*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _3554 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (binary_op_a(NOTEQ, _3554, 125)){
            _3554 = NOVALUE;
            goto LE; // [749] 758
        }
        _3554 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [755] 786
LE: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _3556 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (binary_op_a(NOTEQ, _3556, 93)){
            _3556 = NOVALUE;
            goto LF; // [764] 773
        }
        _3556 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [770] 786
LF: 

        /** 	    				i += 1*/
        _i_6485 = _i_6485 + 1;

        /** 	    			end while*/
        goto LC; // [783] 736
LD: 

        /** 	    			idname = trim(format_pattern[sp .. i-1]) & '='*/
        _3559 = _i_6485 - 1;
        rhs_slice_target = (object_ptr)&_3560;
        RHS_Slice(_format_pattern_6480, _sp_6591, _3559);
        RefDS(_2936);
        _3561 = _16trim(_3560, _2936, 0);
        _3560 = NOVALUE;
        if (IS_SEQUENCE(_3561) && IS_ATOM(61)) {
            Append(&_idname_6507, _3561, 61);
        }
        else if (IS_ATOM(_3561) && IS_SEQUENCE(61)) {
        }
        else {
            Concat((object_ptr)&_idname_6507, _3561, 61);
            DeRef(_3561);
            _3561 = NOVALUE;
        }
        DeRef(_3561);
        _3561 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _3564 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (binary_op_a(NOTEQ, _3564, 93)){
            _3564 = NOVALUE;
            goto L10; // [813] 826
        }
        _3564 = NOVALUE;

        /**     					i -= 1*/
        _i_6485 = _i_6485 - 1;
L10: 

        /**     				for j = 1 to length(arg_list) do*/
        if (IS_SEQUENCE(_arg_list_6481)){
                _3567 = SEQ_PTR(_arg_list_6481)->length;
        }
        else {
            _3567 = 1;
        }
        {
            int _j_6613;
            _j_6613 = 1;
L11: 
            if (_j_6613 > _3567){
                goto L12; // [831] 917
            }

            /**     					if sequence(arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_6481);
            _3568 = (int)*(((s1_ptr)_2)->base + _j_6613);
            _3569 = IS_SEQUENCE(_3568);
            _3568 = NOVALUE;
            if (_3569 == 0)
            {
                _3569 = NOVALUE;
                goto L13; // [847] 886
            }
            else{
                _3569 = NOVALUE;
            }

            /**     						if search:begins(idname, arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_6481);
            _3570 = (int)*(((s1_ptr)_2)->base + _j_6613);
            RefDS(_idname_6507);
            Ref(_3570);
            _3571 = _6begins(_idname_6507, _3570);
            _3570 = NOVALUE;
            if (_3571 == 0) {
                DeRef(_3571);
                _3571 = NOVALUE;
                goto L14; // [861] 885
            }
            else {
                if (!IS_ATOM_INT(_3571) && DBL_PTR(_3571)->dbl == 0.0){
                    DeRef(_3571);
                    _3571 = NOVALUE;
                    goto L14; // [861] 885
                }
                DeRef(_3571);
                _3571 = NOVALUE;
            }
            DeRef(_3571);
            _3571 = NOVALUE;

            /**     							if argn = 0 then*/
            if (_argn_6498 != 0)
            goto L15; // [868] 884

            /**     								argn = j*/
            _argn_6498 = _j_6613;

            /**     								exit*/
            goto L12; // [881] 917
L15: 
L14: 
L13: 

            /**     					if j = length(arg_list) then*/
            if (IS_SEQUENCE(_arg_list_6481)){
                    _3573 = SEQ_PTR(_arg_list_6481)->length;
            }
            else {
                _3573 = 1;
            }
            if (_j_6613 != _3573)
            goto L16; // [891] 910

            /**     						idname = ""*/
            RefDS(_5);
            DeRef(_idname_6507);
            _idname_6507 = _5;

            /**     						argn = -1*/
            _argn_6498 = -1;
L16: 

            /**     				end for*/
            _j_6613 = _j_6613 + 1;
            goto L11; // [912] 838
L12: 
            ;
        }
        goto L6; // [919] 1218

        /** 	    		case '%' then*/
        case 37:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_6627 = _i_6485 + 1;

        /** 	    			i = sp*/
        _i_6485 = _sp_6627;

        /** 	    			while i < length(format_pattern) do*/
L17: 
        if (IS_SEQUENCE(_format_pattern_6480)){
                _3576 = SEQ_PTR(_format_pattern_6480)->length;
        }
        else {
            _3576 = 1;
        }
        if (_i_6485 >= _3576)
        goto L18; // [950] 997

        /** 	    				if format_pattern[i] = '%' then*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _3578 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (binary_op_a(NOTEQ, _3578, 37)){
            _3578 = NOVALUE;
            goto L19; // [960] 969
        }
        _3578 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [966] 997
L19: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _3580 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (binary_op_a(NOTEQ, _3580, 93)){
            _3580 = NOVALUE;
            goto L1A; // [975] 984
        }
        _3580 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [981] 997
L1A: 

        /** 	    				i += 1*/
        _i_6485 = _i_6485 + 1;

        /** 	    			end while*/
        goto L17; // [994] 947
L18: 

        /** 	    			envsym = trim(format_pattern[sp .. i-1])*/
        _3583 = _i_6485 - 1;
        rhs_slice_target = (object_ptr)&_3584;
        RHS_Slice(_format_pattern_6480, _sp_6627, _3583);
        RefDS(_2936);
        _0 = _envsym_6508;
        _envsym_6508 = _16trim(_3584, _2936, 0);
        DeRef(_0);
        _3584 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _3586 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (binary_op_a(NOTEQ, _3586, 93)){
            _3586 = NOVALUE;
            goto L1B; // [1020] 1033
        }
        _3586 = NOVALUE;

        /**     					i -= 1*/
        _i_6485 = _i_6485 - 1;
L1B: 

        /**     				envvar = getenv(envsym)*/
        DeRefi(_envvar_6509);
        _envvar_6509 = EGetEnv(_envsym_6508);

        /**     				argn = -1*/
        _argn_6498 = -1;

        /**     				if atom(envvar) then*/
        _3590 = IS_ATOM(_envvar_6509);
        if (_3590 == 0)
        {
            _3590 = NOVALUE;
            goto L1C; // [1050] 1059
        }
        else{
            _3590 = NOVALUE;
        }

        /**     					envvar = ""*/
        RefDS(_5);
        DeRefi(_envvar_6509);
        _envvar_6509 = _5;
L1C: 
        goto L6; // [1061] 1218

        /** 	    		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' then*/
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:

        /** 	    			if argn = 0 then*/
        if (_argn_6498 != 0)
        goto L6; // [1087] 1218

        /** 		    			i -= 1*/
        _i_6485 = _i_6485 - 1;

        /** 		    			while i < length(format_pattern) do*/
L1D: 
        if (IS_SEQUENCE(_format_pattern_6480)){
                _3593 = SEQ_PTR(_format_pattern_6480)->length;
        }
        else {
            _3593 = 1;
        }
        if (_i_6485 >= _3593)
        goto L6; // [1107] 1218

        /** 		    				i += 1*/
        _i_6485 = _i_6485 + 1;

        /** 		    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _tch_6484 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (!IS_ATOM_INT(_tch_6484))
        _tch_6484 = (long)DBL_PTR(_tch_6484)->dbl;

        /** 		    				pos = find(tch, "0123456789")*/
        _pos_6497 = find_from(_tch_6484, _3533, 1);

        /** 		    				if pos = 0 then*/
        if (_pos_6497 != 0)
        goto L1E; // [1138] 1155

        /** 		    					i -= 1*/
        _i_6485 = _i_6485 - 1;

        /** 		    					exit*/
        goto L6; // [1152] 1218
L1E: 

        /** 		    				argn = argn * 10 + pos - 1*/
        if (_argn_6498 == (short)_argn_6498)
        _3600 = _argn_6498 * 10;
        else
        _3600 = NewDouble(_argn_6498 * (double)10);
        if (IS_ATOM_INT(_3600)) {
            _3601 = _3600 + _pos_6497;
            if ((long)((unsigned long)_3601 + (unsigned long)HIGH_BITS) >= 0) 
            _3601 = NewDouble((double)_3601);
        }
        else {
            _3601 = NewDouble(DBL_PTR(_3600)->dbl + (double)_pos_6497);
        }
        DeRef(_3600);
        _3600 = NOVALUE;
        if (IS_ATOM_INT(_3601)) {
            _argn_6498 = _3601 - 1;
        }
        else {
            _argn_6498 = NewDouble(DBL_PTR(_3601)->dbl - (double)1);
        }
        DeRef(_3601);
        _3601 = NOVALUE;
        if (!IS_ATOM_INT(_argn_6498)) {
            _1 = (long)(DBL_PTR(_argn_6498)->dbl);
            if (UNIQUE(DBL_PTR(_argn_6498)) && (DBL_PTR(_argn_6498)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_argn_6498);
            _argn_6498 = _1;
        }

        /** 		    			end while*/
        goto L1D; // [1175] 1104
        goto L6; // [1179] 1218

        /** 	    		case ',' then*/
        case 44:

        /** 	    			if i < length(format_pattern) then*/
        if (IS_SEQUENCE(_format_pattern_6480)){
                _3603 = SEQ_PTR(_format_pattern_6480)->length;
        }
        else {
            _3603 = 1;
        }
        if (_i_6485 >= _3603)
        goto L6; // [1190] 1218

        /** 	    				i +=1*/
        _i_6485 = _i_6485 + 1;

        /** 	    				tsep = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_6480);
        _tsep_6503 = (int)*(((s1_ptr)_2)->base + _i_6485);
        if (!IS_ATOM_INT(_tsep_6503))
        _tsep_6503 = (long)DBL_PTR(_tsep_6503)->dbl;
        goto L6; // [1211] 1218

        /** 	    		case else*/
        default:
    ;}L6: 

    /**     		if tend > 0 then*/
    if (_tend_6486 <= 0)
    goto L1F; // [1220] 3783

    /**     			sequence argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_6674);
    _argtext_6674 = _5;

    /**     			if argn = 0 then*/
    if (_argn_6498 != 0)
    goto L20; // [1235] 1248

    /**     				argn = argl + 1*/
    _argn_6498 = _argl_6499 + 1;
L20: 

    /**     			argl = argn*/
    _argl_6499 = _argn_6498;

    /**     			if argn < 1 or argn > length(arg_list) then*/
    _3610 = (_argn_6498 < 1);
    if (_3610 != 0) {
        goto L21; // [1261] 1277
    }
    if (IS_SEQUENCE(_arg_list_6481)){
            _3612 = SEQ_PTR(_arg_list_6481)->length;
    }
    else {
        _3612 = 1;
    }
    _3613 = (_argn_6498 > _3612);
    _3612 = NOVALUE;
    if (_3613 == 0)
    {
        DeRef(_3613);
        _3613 = NOVALUE;
        goto L22; // [1273] 1319
    }
    else{
        DeRef(_3613);
        _3613 = NOVALUE;
    }
L21: 

    /**     				if length(envvar) > 0 then*/
    if (IS_SEQUENCE(_envvar_6509)){
            _3614 = SEQ_PTR(_envvar_6509)->length;
    }
    else {
        _3614 = 1;
    }
    if (_3614 <= 0)
    goto L23; // [1284] 1303

    /**     					argtext = envvar*/
    Ref(_envvar_6509);
    DeRef(_argtext_6674);
    _argtext_6674 = _envvar_6509;

    /** 	    				currargv = envvar*/
    Ref(_envvar_6509);
    DeRef(_currargv_6506);
    _currargv_6506 = _envvar_6509;
    goto L24; // [1300] 2780
L23: 

    /**     					argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_6674);
    _argtext_6674 = _5;

    /** 	    				currargv =""*/
    RefDS(_5);
    DeRef(_currargv_6506);
    _currargv_6506 = _5;
    goto L24; // [1316] 2780
L22: 

    /** 					if string(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3616 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    Ref(_3616);
    _3617 = _5string(_3616);
    _3616 = NOVALUE;
    if (_3617 == 0) {
        DeRef(_3617);
        _3617 = NOVALUE;
        goto L25; // [1329] 1379
    }
    else {
        if (!IS_ATOM_INT(_3617) && DBL_PTR(_3617)->dbl == 0.0){
            DeRef(_3617);
            _3617 = NOVALUE;
            goto L25; // [1329] 1379
        }
        DeRef(_3617);
        _3617 = NOVALUE;
    }
    DeRef(_3617);
    _3617 = NOVALUE;

    /** 						if length(idname) > 0 then*/
    if (IS_SEQUENCE(_idname_6507)){
            _3618 = SEQ_PTR(_idname_6507)->length;
    }
    else {
        _3618 = 1;
    }
    if (_3618 <= 0)
    goto L26; // [1339] 1367

    /** 							argtext = arg_list[argn][length(idname) + 1 .. $]*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3620 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (IS_SEQUENCE(_idname_6507)){
            _3621 = SEQ_PTR(_idname_6507)->length;
    }
    else {
        _3621 = 1;
    }
    _3622 = _3621 + 1;
    _3621 = NOVALUE;
    if (IS_SEQUENCE(_3620)){
            _3623 = SEQ_PTR(_3620)->length;
    }
    else {
        _3623 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_3620, _3622, _3623);
    _3620 = NOVALUE;
    goto L27; // [1364] 2773
L26: 

    /** 							argtext = arg_list[argn]*/
    DeRef(_argtext_6674);
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _argtext_6674 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    Ref(_argtext_6674);
    goto L27; // [1376] 2773
L25: 

    /** 					elsif integer(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3626 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (IS_ATOM_INT(_3626))
    _3627 = 1;
    else if (IS_ATOM_DBL(_3626))
    _3627 = IS_ATOM_INT(DoubleToInt(_3626));
    else
    _3627 = 0;
    _3626 = NOVALUE;
    if (_3627 == 0)
    {
        _3627 = NOVALUE;
        goto L28; // [1388] 1937
    }
    else{
        _3627 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_6504 == 0)
    {
        goto L29; // [1395] 1419
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(arg_list[argn]))}*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3628 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    Ref(_3628);
    _3629 = _18abs(_3628);
    _3628 = NOVALUE;
    _3630 = binary_op(AND_BITS, _2226, _3629);
    DeRef(_3629);
    _3629 = NOVALUE;
    _0 = _argtext_6674;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3630;
    _argtext_6674 = MAKE_SEQ(_1);
    DeRef(_0);
    _3630 = NOVALUE;
    goto L27; // [1416] 2773
L29: 

    /** 						elsif bwz != 0 and arg_list[argn] = 0 then*/
    _3632 = (_bwz_6492 != 0);
    if (_3632 == 0) {
        goto L2A; // [1427] 1454
    }
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3634 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (IS_ATOM_INT(_3634)) {
        _3635 = (_3634 == 0);
    }
    else {
        _3635 = binary_op(EQUALS, _3634, 0);
    }
    _3634 = NOVALUE;
    if (_3635 == 0) {
        DeRef(_3635);
        _3635 = NOVALUE;
        goto L2A; // [1440] 1454
    }
    else {
        if (!IS_ATOM_INT(_3635) && DBL_PTR(_3635)->dbl == 0.0){
            DeRef(_3635);
            _3635 = NOVALUE;
            goto L2A; // [1440] 1454
        }
        DeRef(_3635);
        _3635 = NOVALUE;
    }
    DeRef(_3635);
    _3635 = NOVALUE;

    /** 							argtext = repeat(' ', width)*/
    DeRef(_argtext_6674);
    _argtext_6674 = Repeat(32, _width_6495);
    goto L27; // [1451] 2773
L2A: 

    /** 						elsif binout = 1 then*/
    if (_binout_6502 != 1)
    goto L2B; // [1458] 1601

    /** 							argtext = stdseq:reverse( convert:int_to_bits(arg_list[argn], 32)) + '0'*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3638 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    Ref(_3638);
    _3639 = _17int_to_bits(_3638, 32);
    _3638 = NOVALUE;
    _3640 = _3reverse(_3639, 1, 0);
    _3639 = NOVALUE;
    DeRef(_argtext_6674);
    if (IS_ATOM_INT(_3640)) {
        _argtext_6674 = _3640 + 48;
        if ((long)((unsigned long)_argtext_6674 + (unsigned long)HIGH_BITS) >= 0) 
        _argtext_6674 = NewDouble((double)_argtext_6674);
    }
    else {
        _argtext_6674 = binary_op(PLUS, _3640, 48);
    }
    DeRef(_3640);
    _3640 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _3642 = (_zfill_6491 != 0);
    if (_3642 == 0) {
        goto L2C; // [1493] 1539
    }
    _3644 = (_width_6495 > 0);
    if (_3644 == 0)
    {
        DeRef(_3644);
        _3644 = NOVALUE;
        goto L2C; // [1504] 1539
    }
    else{
        DeRef(_3644);
        _3644 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3645 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3645 = 1;
    }
    if (_width_6495 <= _3645)
    goto L27; // [1514] 2773

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3647 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3647 = 1;
    }
    _3648 = _width_6495 - _3647;
    _3647 = NOVALUE;
    _3649 = Repeat(48, _3648);
    _3648 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _3649, _argtext_6674);
    DeRefDS(_3649);
    _3649 = NOVALUE;
    DeRef(_3649);
    _3649 = NOVALUE;
    goto L27; // [1536] 2773
L2C: 

    /** 								count = 1*/
    _count_6512 = 1;

    /** 								while count < length(argtext) and argtext[count] = '0' do*/
L2D: 
    if (IS_SEQUENCE(_argtext_6674)){
            _3651 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3651 = 1;
    }
    _3652 = (_count_6512 < _3651);
    _3651 = NOVALUE;
    if (_3652 == 0) {
        goto L2E; // [1558] 1587
    }
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3654 = (int)*(((s1_ptr)_2)->base + _count_6512);
    if (IS_ATOM_INT(_3654)) {
        _3655 = (_3654 == 48);
    }
    else {
        _3655 = binary_op(EQUALS, _3654, 48);
    }
    _3654 = NOVALUE;
    if (_3655 <= 0) {
        if (_3655 == 0) {
            DeRef(_3655);
            _3655 = NOVALUE;
            goto L2E; // [1571] 1587
        }
        else {
            if (!IS_ATOM_INT(_3655) && DBL_PTR(_3655)->dbl == 0.0){
                DeRef(_3655);
                _3655 = NOVALUE;
                goto L2E; // [1571] 1587
            }
            DeRef(_3655);
            _3655 = NOVALUE;
        }
    }
    DeRef(_3655);
    _3655 = NOVALUE;

    /** 									count += 1*/
    _count_6512 = _count_6512 + 1;

    /** 								end while*/
    goto L2D; // [1584] 1551
L2E: 

    /** 								argtext = argtext[count .. $]*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3657 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3657 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, _count_6512, _3657);
    goto L27; // [1598] 2773
L2B: 

    /** 						elsif hexout = 0 then*/
    if (_hexout_6501 != 0)
    goto L2F; // [1605] 1871

    /** 							argtext = sprintf("%d", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3660 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    DeRef(_argtext_6674);
    _argtext_6674 = EPrintf(-9999999, _106, _3660);
    _3660 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _3662 = (_zfill_6491 != 0);
    if (_3662 == 0) {
        goto L30; // [1627] 1724
    }
    _3664 = (_width_6495 > 0);
    if (_3664 == 0)
    {
        DeRef(_3664);
        _3664 = NOVALUE;
        goto L30; // [1638] 1724
    }
    else{
        DeRef(_3664);
        _3664 = NOVALUE;
    }

    /** 								if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3665 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _3665, 45)){
        _3665 = NOVALUE;
        goto L31; // [1647] 1693
    }
    _3665 = NOVALUE;

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3667 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3667 = 1;
    }
    if (_width_6495 <= _3667)
    goto L32; // [1658] 1723

    /** 										argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3669 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3669 = 1;
    }
    _3670 = _width_6495 - _3669;
    _3669 = NOVALUE;
    _3671 = Repeat(48, _3670);
    _3670 = NOVALUE;
    if (IS_SEQUENCE(_argtext_6674)){
            _3672 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3672 = 1;
    }
    rhs_slice_target = (object_ptr)&_3673;
    RHS_Slice(_argtext_6674, 2, _3672);
    {
        int concat_list[3];

        concat_list[0] = _3673;
        concat_list[1] = _3671;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3673);
    _3673 = NOVALUE;
    DeRefDS(_3671);
    _3671 = NOVALUE;
    goto L32; // [1690] 1723
L31: 

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3675 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3675 = 1;
    }
    if (_width_6495 <= _3675)
    goto L33; // [1700] 1722

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3677 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3677 = 1;
    }
    _3678 = _width_6495 - _3677;
    _3677 = NOVALUE;
    _3679 = Repeat(48, _3678);
    _3678 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _3679, _argtext_6674);
    DeRefDS(_3679);
    _3679 = NOVALUE;
    DeRef(_3679);
    _3679 = NOVALUE;
L33: 
L32: 
L30: 

    /** 							if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3681 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (binary_op_a(LESSEQ, _3681, 0)){
        _3681 = NOVALUE;
        goto L34; // [1730] 1778
    }
    _3681 = NOVALUE;

    /** 								if psign then*/
    if (_psign_6489 == 0)
    {
        goto L27; // [1738] 2773
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_6491 != 0)
    goto L35; // [1743] 1756

    /** 										argtext = '+' & argtext*/
    Prepend(&_argtext_6674, _argtext_6674, 43);
    goto L27; // [1753] 2773
L35: 

    /** 									elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3685 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _3685, 48)){
        _3685 = NOVALUE;
        goto L27; // [1762] 2773
    }
    _3685 = NOVALUE;

    /** 										argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_6674 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [1775] 2773
L34: 

    /** 							elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3687 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (binary_op_a(GREATEREQ, _3687, 0)){
        _3687 = NOVALUE;
        goto L27; // [1784] 2773
    }
    _3687 = NOVALUE;

    /** 								if msign then*/
    if (_msign_6490 == 0)
    {
        goto L27; // [1792] 2773
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_6491 != 0)
    goto L36; // [1797] 1820

    /** 										argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3690 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3690 = 1;
    }
    rhs_slice_target = (object_ptr)&_3691;
    RHS_Slice(_argtext_6674, 2, _3690);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _3691;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3691);
    _3691 = NOVALUE;
    goto L27; // [1817] 2773
L36: 

    /** 										if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3694 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3694, 48)){
        _3694 = NOVALUE;
        goto L37; // [1826] 1849
    }
    _3694 = NOVALUE;

    /** 											argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3696 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3696 = 1;
    }
    rhs_slice_target = (object_ptr)&_3697;
    RHS_Slice(_argtext_6674, 3, _3696);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _3697;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3697);
    _3697 = NOVALUE;
    goto L27; // [1846] 2773
L37: 

    /** 											argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3699 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3699 = 1;
    }
    rhs_slice_target = (object_ptr)&_3700;
    RHS_Slice(_argtext_6674, 2, _3699);
    Append(&_argtext_6674, _3700, 41);
    DeRefDS(_3700);
    _3700 = NOVALUE;
    goto L27; // [1868] 2773
L2F: 

    /** 							argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3703 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    DeRef(_argtext_6674);
    _argtext_6674 = EPrintf(-9999999, _3702, _3703);
    _3703 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _3705 = (_zfill_6491 != 0);
    if (_3705 == 0) {
        goto L27; // [1889] 2773
    }
    _3707 = (_width_6495 > 0);
    if (_3707 == 0)
    {
        DeRef(_3707);
        _3707 = NOVALUE;
        goto L27; // [1900] 2773
    }
    else{
        DeRef(_3707);
        _3707 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3708 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3708 = 1;
    }
    if (_width_6495 <= _3708)
    goto L27; // [1910] 2773

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3710 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3710 = 1;
    }
    _3711 = _width_6495 - _3710;
    _3710 = NOVALUE;
    _3712 = Repeat(48, _3711);
    _3711 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _3712, _argtext_6674);
    DeRefDS(_3712);
    _3712 = NOVALUE;
    DeRef(_3712);
    _3712 = NOVALUE;
    goto L27; // [1934] 2773
L28: 

    /** 					elsif atom(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3714 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    _3715 = IS_ATOM(_3714);
    _3714 = NOVALUE;
    if (_3715 == 0)
    {
        _3715 = NOVALUE;
        goto L38; // [1946] 2351
    }
    else{
        _3715 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_6504 == 0)
    {
        goto L39; // [1953] 1980
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(arg_list[argn])))}*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3716 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (IS_ATOM_INT(_3716))
    _3717 = e_floor(_3716);
    else
    _3717 = unary_op(FLOOR, _3716);
    _3716 = NOVALUE;
    _3718 = _18abs(_3717);
    _3717 = NOVALUE;
    _3719 = binary_op(AND_BITS, _2226, _3718);
    DeRef(_3718);
    _3718 = NOVALUE;
    _0 = _argtext_6674;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3719;
    _argtext_6674 = MAKE_SEQ(_1);
    DeRef(_0);
    _3719 = NOVALUE;
    goto L27; // [1977] 2773
L39: 

    /** 							if hexout then*/
    if (_hexout_6501 == 0)
    {
        goto L3A; // [1984] 2052
    }
    else{
    }

    /** 								argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3721 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    DeRef(_argtext_6674);
    _argtext_6674 = EPrintf(-9999999, _3702, _3721);
    _3721 = NOVALUE;

    /** 								if zfill != 0 and width > 0 then*/
    _3723 = (_zfill_6491 != 0);
    if (_3723 == 0) {
        goto L27; // [2005] 2773
    }
    _3725 = (_width_6495 > 0);
    if (_3725 == 0)
    {
        DeRef(_3725);
        _3725 = NOVALUE;
        goto L27; // [2016] 2773
    }
    else{
        DeRef(_3725);
        _3725 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3726 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3726 = 1;
    }
    if (_width_6495 <= _3726)
    goto L27; // [2026] 2773

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3728 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3728 = 1;
    }
    _3729 = _width_6495 - _3728;
    _3728 = NOVALUE;
    _3730 = Repeat(48, _3729);
    _3729 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _3730, _argtext_6674);
    DeRefDS(_3730);
    _3730 = NOVALUE;
    DeRef(_3730);
    _3730 = NOVALUE;
    goto L27; // [2049] 2773
L3A: 

    /** 								argtext = trim(sprintf("%15.15g", arg_list[argn]))*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3733 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    _3734 = EPrintf(-9999999, _3732, _3733);
    _3733 = NOVALUE;
    RefDS(_2936);
    _0 = _argtext_6674;
    _argtext_6674 = _16trim(_3734, _2936, 0);
    DeRef(_0);
    _3734 = NOVALUE;

    /** 								while ep != 0 with entry do*/
    goto L3B; // [2072] 2095
L3C: 
    if (_ep_6510 == 0)
    goto L3D; // [2077] 2109

    /** 									argtext = remove(argtext, ep+2)*/
    _3737 = _ep_6510 + 2;
    if ((long)((unsigned long)_3737 + (unsigned long)HIGH_BITS) >= 0) 
    _3737 = NewDouble((double)_3737);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_6674);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_3737)) ? _3737 : (long)(DBL_PTR(_3737)->dbl);
        int stop = (IS_ATOM_INT(_3737)) ? _3737 : (long)(DBL_PTR(_3737)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_6674), start, &_argtext_6674 );
            }
            else Tail(SEQ_PTR(_argtext_6674), stop+1, &_argtext_6674);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_6674), start, &_argtext_6674);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_6674 = Remove_elements(start, stop, (SEQ_PTR(_argtext_6674)->ref == 1));
        }
    }
    DeRef(_3737);
    _3737 = NOVALUE;
    _3737 = NOVALUE;

    /** 								entry*/
L3B: 

    /** 									ep = match("e+0", argtext)*/
    _ep_6510 = e_match_from(_3739, _argtext_6674, 1);

    /** 								end while*/
    goto L3C; // [2106] 2075
L3D: 

    /** 								if zfill != 0 and width > 0 then*/
    _3741 = (_zfill_6491 != 0);
    if (_3741 == 0) {
        goto L3E; // [2117] 2202
    }
    _3743 = (_width_6495 > 0);
    if (_3743 == 0)
    {
        DeRef(_3743);
        _3743 = NOVALUE;
        goto L3E; // [2128] 2202
    }
    else{
        DeRef(_3743);
        _3743 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3744 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3744 = 1;
    }
    if (_width_6495 <= _3744)
    goto L3F; // [2138] 2201

    /** 										if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3746 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _3746, 45)){
        _3746 = NOVALUE;
        goto L40; // [2148] 2182
    }
    _3746 = NOVALUE;

    /** 											argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3748 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3748 = 1;
    }
    _3749 = _width_6495 - _3748;
    _3748 = NOVALUE;
    _3750 = Repeat(48, _3749);
    _3749 = NOVALUE;
    if (IS_SEQUENCE(_argtext_6674)){
            _3751 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3751 = 1;
    }
    rhs_slice_target = (object_ptr)&_3752;
    RHS_Slice(_argtext_6674, 2, _3751);
    {
        int concat_list[3];

        concat_list[0] = _3752;
        concat_list[1] = _3750;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3752);
    _3752 = NOVALUE;
    DeRefDS(_3750);
    _3750 = NOVALUE;
    goto L41; // [2179] 2200
L40: 

    /** 											argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3754 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3754 = 1;
    }
    _3755 = _width_6495 - _3754;
    _3754 = NOVALUE;
    _3756 = Repeat(48, _3755);
    _3755 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _3756, _argtext_6674);
    DeRefDS(_3756);
    _3756 = NOVALUE;
    DeRef(_3756);
    _3756 = NOVALUE;
L41: 
L3F: 
L3E: 

    /** 								if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3758 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (binary_op_a(LESSEQ, _3758, 0)){
        _3758 = NOVALUE;
        goto L42; // [2208] 2256
    }
    _3758 = NOVALUE;

    /** 									if psign  then*/
    if (_psign_6489 == 0)
    {
        goto L27; // [2216] 2773
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_6491 != 0)
    goto L43; // [2221] 2234

    /** 											argtext = '+' & argtext*/
    Prepend(&_argtext_6674, _argtext_6674, 43);
    goto L27; // [2231] 2773
L43: 

    /** 										elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3762 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _3762, 48)){
        _3762 = NOVALUE;
        goto L27; // [2240] 2773
    }
    _3762 = NOVALUE;

    /** 											argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_6674 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [2253] 2773
L42: 

    /** 								elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3764 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (binary_op_a(GREATEREQ, _3764, 0)){
        _3764 = NOVALUE;
        goto L27; // [2262] 2773
    }
    _3764 = NOVALUE;

    /** 									if msign then*/
    if (_msign_6490 == 0)
    {
        goto L27; // [2270] 2773
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_6491 != 0)
    goto L44; // [2275] 2298

    /** 											argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3767 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3767 = 1;
    }
    rhs_slice_target = (object_ptr)&_3768;
    RHS_Slice(_argtext_6674, 2, _3767);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _3768;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3768);
    _3768 = NOVALUE;
    goto L27; // [2295] 2773
L44: 

    /** 											if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3770 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3770, 48)){
        _3770 = NOVALUE;
        goto L45; // [2304] 2327
    }
    _3770 = NOVALUE;

    /** 												argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3772 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3772 = 1;
    }
    rhs_slice_target = (object_ptr)&_3773;
    RHS_Slice(_argtext_6674, 3, _3772);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _3773;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3773);
    _3773 = NOVALUE;
    goto L27; // [2324] 2773
L45: 

    /** 												argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3775 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3775 = 1;
    }
    rhs_slice_target = (object_ptr)&_3776;
    RHS_Slice(_argtext_6674, 2, _3775);
    Append(&_argtext_6674, _3776, 41);
    DeRefDS(_3776);
    _3776 = NOVALUE;
    goto L27; // [2348] 2773
L38: 

    /** 						if alt != 0 and length(arg_list[argn]) = 2 then*/
    _3778 = (_alt_6494 != 0);
    if (_3778 == 0) {
        goto L46; // [2359] 2680
    }
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3780 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    if (IS_SEQUENCE(_3780)){
            _3781 = SEQ_PTR(_3780)->length;
    }
    else {
        _3781 = 1;
    }
    _3780 = NOVALUE;
    _3782 = (_3781 == 2);
    _3781 = NOVALUE;
    if (_3782 == 0)
    {
        DeRef(_3782);
        _3782 = NOVALUE;
        goto L46; // [2375] 2680
    }
    else{
        DeRef(_3782);
        _3782 = NOVALUE;
    }

    /** 							object tempv*/

    /** 							if atom(prevargv) then*/
    _3783 = IS_ATOM(_prevargv_6505);
    if (_3783 == 0)
    {
        _3783 = NOVALUE;
        goto L47; // [2385] 2421
    }
    else{
        _3783 = NOVALUE;
    }

    /** 								if prevargv != 1 then*/
    if (binary_op_a(EQUALS, _prevargv_6505, 1)){
        goto L48; // [2390] 2407
    }

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3785 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    DeRef(_tempv_6910);
    _2 = (int)SEQ_PTR(_3785);
    _tempv_6910 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_6910);
    _3785 = NOVALUE;
    goto L49; // [2404] 2455
L48: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3787 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    DeRef(_tempv_6910);
    _2 = (int)SEQ_PTR(_3787);
    _tempv_6910 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_6910);
    _3787 = NOVALUE;
    goto L49; // [2418] 2455
L47: 

    /** 								if length(prevargv) = 0 then*/
    if (IS_SEQUENCE(_prevargv_6505)){
            _3789 = SEQ_PTR(_prevargv_6505)->length;
    }
    else {
        _3789 = 1;
    }
    if (_3789 != 0)
    goto L4A; // [2426] 2443

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3791 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    DeRef(_tempv_6910);
    _2 = (int)SEQ_PTR(_3791);
    _tempv_6910 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_6910);
    _3791 = NOVALUE;
    goto L4B; // [2440] 2454
L4A: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3793 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    DeRef(_tempv_6910);
    _2 = (int)SEQ_PTR(_3793);
    _tempv_6910 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_6910);
    _3793 = NOVALUE;
L4B: 
L49: 

    /** 							if string(tempv) then*/
    Ref(_tempv_6910);
    _3795 = _5string(_tempv_6910);
    if (_3795 == 0) {
        DeRef(_3795);
        _3795 = NOVALUE;
        goto L4C; // [2463] 2476
    }
    else {
        if (!IS_ATOM_INT(_3795) && DBL_PTR(_3795)->dbl == 0.0){
            DeRef(_3795);
            _3795 = NOVALUE;
            goto L4C; // [2463] 2476
        }
        DeRef(_3795);
        _3795 = NOVALUE;
    }
    DeRef(_3795);
    _3795 = NOVALUE;

    /** 								argtext = tempv*/
    Ref(_tempv_6910);
    DeRef(_argtext_6674);
    _argtext_6674 = _tempv_6910;
    goto L4D; // [2473] 2675
L4C: 

    /** 							elsif integer(tempv) then*/
    if (IS_ATOM_INT(_tempv_6910))
    _3796 = 1;
    else if (IS_ATOM_DBL(_tempv_6910))
    _3796 = IS_ATOM_INT(DoubleToInt(_tempv_6910));
    else
    _3796 = 0;
    if (_3796 == 0)
    {
        _3796 = NOVALUE;
        goto L4E; // [2481] 2547
    }
    else{
        _3796 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_6504 == 0)
    {
        goto L4F; // [2486] 2506
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(tempv))}*/
    Ref(_tempv_6910);
    _3797 = _18abs(_tempv_6910);
    _3798 = binary_op(AND_BITS, _2226, _3797);
    DeRef(_3797);
    _3797 = NOVALUE;
    _0 = _argtext_6674;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3798;
    _argtext_6674 = MAKE_SEQ(_1);
    DeRef(_0);
    _3798 = NOVALUE;
    goto L4D; // [2503] 2675
L4F: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _3800 = (_bwz_6492 != 0);
    if (_3800 == 0) {
        goto L50; // [2514] 2537
    }
    if (IS_ATOM_INT(_tempv_6910)) {
        _3802 = (_tempv_6910 == 0);
    }
    else {
        _3802 = binary_op(EQUALS, _tempv_6910, 0);
    }
    if (_3802 == 0) {
        DeRef(_3802);
        _3802 = NOVALUE;
        goto L50; // [2523] 2537
    }
    else {
        if (!IS_ATOM_INT(_3802) && DBL_PTR(_3802)->dbl == 0.0){
            DeRef(_3802);
            _3802 = NOVALUE;
            goto L50; // [2523] 2537
        }
        DeRef(_3802);
        _3802 = NOVALUE;
    }
    DeRef(_3802);
    _3802 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_6674);
    _argtext_6674 = Repeat(32, _width_6495);
    goto L4D; // [2534] 2675
L50: 

    /** 									argtext = sprintf("%d", tempv)*/
    DeRef(_argtext_6674);
    _argtext_6674 = EPrintf(-9999999, _106, _tempv_6910);
    goto L4D; // [2544] 2675
L4E: 

    /** 							elsif atom(tempv) then*/
    _3805 = IS_ATOM(_tempv_6910);
    if (_3805 == 0)
    {
        _3805 = NOVALUE;
        goto L51; // [2552] 2629
    }
    else{
        _3805 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_6504 == 0)
    {
        goto L52; // [2557] 2580
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(tempv)))}*/
    if (IS_ATOM_INT(_tempv_6910))
    _3806 = e_floor(_tempv_6910);
    else
    _3806 = unary_op(FLOOR, _tempv_6910);
    _3807 = _18abs(_3806);
    _3806 = NOVALUE;
    _3808 = binary_op(AND_BITS, _2226, _3807);
    DeRef(_3807);
    _3807 = NOVALUE;
    _0 = _argtext_6674;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3808;
    _argtext_6674 = MAKE_SEQ(_1);
    DeRef(_0);
    _3808 = NOVALUE;
    goto L4D; // [2577] 2675
L52: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _3810 = (_bwz_6492 != 0);
    if (_3810 == 0) {
        goto L53; // [2588] 2611
    }
    if (IS_ATOM_INT(_tempv_6910)) {
        _3812 = (_tempv_6910 == 0);
    }
    else {
        _3812 = binary_op(EQUALS, _tempv_6910, 0);
    }
    if (_3812 == 0) {
        DeRef(_3812);
        _3812 = NOVALUE;
        goto L53; // [2597] 2611
    }
    else {
        if (!IS_ATOM_INT(_3812) && DBL_PTR(_3812)->dbl == 0.0){
            DeRef(_3812);
            _3812 = NOVALUE;
            goto L53; // [2597] 2611
        }
        DeRef(_3812);
        _3812 = NOVALUE;
    }
    DeRef(_3812);
    _3812 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_6674);
    _argtext_6674 = Repeat(32, _width_6495);
    goto L4D; // [2608] 2675
L53: 

    /** 									argtext = trim(sprintf("%15.15g", tempv))*/
    _3814 = EPrintf(-9999999, _3732, _tempv_6910);
    RefDS(_2936);
    _0 = _argtext_6674;
    _argtext_6674 = _16trim(_3814, _2936, 0);
    DeRef(_0);
    _3814 = NOVALUE;
    goto L4D; // [2626] 2675
L51: 

    /** 								argtext = pretty:pretty_sprint( tempv,*/
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_106);
    *((int *)(_2+20)) = _106;
    RefDS(_3816);
    *((int *)(_2+24)) = _3816;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _3817 = MAKE_SEQ(_1);
    DeRef(_options_inlined_pretty_sprint_at_2645_6964);
    _options_inlined_pretty_sprint_at_2645_6964 = _3817;
    _3817 = NOVALUE;

    /** 	pretty_printing = 0*/
    _2pretty_printing_157 = 0;

    /** 	pretty( x, options )*/
    Ref(_tempv_6910);
    RefDS(_options_inlined_pretty_sprint_at_2645_6964);
    _2pretty(_tempv_6910, _options_inlined_pretty_sprint_at_2645_6964);

    /** 	return pretty_line*/
    RefDS(_2pretty_line_160);
    DeRef(_argtext_6674);
    _argtext_6674 = _2pretty_line_160;
    DeRef(_options_inlined_pretty_sprint_at_2645_6964);
    _options_inlined_pretty_sprint_at_2645_6964 = NOVALUE;
L4D: 
    DeRef(_tempv_6910);
    _tempv_6910 = NOVALUE;
    goto L54; // [2677] 2758
L46: 

    /** 							argtext = pretty:pretty_sprint( arg_list[argn],*/
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _3818 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_106);
    *((int *)(_2+20)) = _106;
    RefDS(_3816);
    *((int *)(_2+24)) = _3816;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _3819 = MAKE_SEQ(_1);
    Ref(_3818);
    DeRef(_x_inlined_pretty_sprint_at_2700_6970);
    _x_inlined_pretty_sprint_at_2700_6970 = _3818;
    _3818 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2703_6971);
    _options_inlined_pretty_sprint_at_2703_6971 = _3819;
    _3819 = NOVALUE;

    /** 	pretty_printing = 0*/
    _2pretty_printing_157 = 0;

    /** 	pretty( x, options )*/
    Ref(_x_inlined_pretty_sprint_at_2700_6970);
    RefDS(_options_inlined_pretty_sprint_at_2703_6971);
    _2pretty(_x_inlined_pretty_sprint_at_2700_6970, _options_inlined_pretty_sprint_at_2703_6971);

    /** 	return pretty_line*/
    RefDS(_2pretty_line_160);
    DeRef(_argtext_6674);
    _argtext_6674 = _2pretty_line_160;
    DeRef(_x_inlined_pretty_sprint_at_2700_6970);
    _x_inlined_pretty_sprint_at_2700_6970 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2703_6971);
    _options_inlined_pretty_sprint_at_2703_6971 = NOVALUE;

    /** 						while ep != 0 with entry do*/
    goto L54; // [2735] 2758
L55: 
    if (_ep_6510 == 0)
    goto L56; // [2740] 2772

    /** 							argtext = remove(argtext, ep+2)*/
    _3821 = _ep_6510 + 2;
    if ((long)((unsigned long)_3821 + (unsigned long)HIGH_BITS) >= 0) 
    _3821 = NewDouble((double)_3821);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_6674);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_3821)) ? _3821 : (long)(DBL_PTR(_3821)->dbl);
        int stop = (IS_ATOM_INT(_3821)) ? _3821 : (long)(DBL_PTR(_3821)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_6674), start, &_argtext_6674 );
            }
            else Tail(SEQ_PTR(_argtext_6674), stop+1, &_argtext_6674);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_6674), start, &_argtext_6674);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_6674 = Remove_elements(start, stop, (SEQ_PTR(_argtext_6674)->ref == 1));
        }
    }
    DeRef(_3821);
    _3821 = NOVALUE;
    _3821 = NOVALUE;

    /** 						entry*/
L54: 

    /** 							ep = match("e+0", argtext)*/
    _ep_6510 = e_match_from(_3739, _argtext_6674, 1);

    /** 						end while*/
    goto L55; // [2769] 2738
L56: 
L27: 

    /** 	    			currargv = arg_list[argn]*/
    DeRef(_currargv_6506);
    _2 = (int)SEQ_PTR(_arg_list_6481);
    _currargv_6506 = (int)*(((s1_ptr)_2)->base + _argn_6498);
    Ref(_currargv_6506);
L24: 

    /**     			if length(argtext) > 0 then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3825 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3825 = 1;
    }
    if (_3825 <= 0)
    goto L57; // [2785] 3737

    /**     				switch cap do*/
    _0 = _cap_6487;
    switch ( _0 ){ 

        /**     					case 'u' then*/
        case 117:

        /**     						argtext = upper(argtext)*/
        RefDS(_argtext_6674);
        _0 = _argtext_6674;
        _argtext_6674 = _16upper(_argtext_6674);
        DeRefDS(_0);
        goto L58; // [2810] 2878

        /**     					case 'l' then*/
        case 108:

        /**     						argtext = lower(argtext)*/
        RefDS(_argtext_6674);
        _0 = _argtext_6674;
        _argtext_6674 = _16lower(_argtext_6674);
        DeRefDS(_0);
        goto L58; // [2824] 2878

        /**     					case 'w' then*/
        case 119:

        /**     						argtext = proper(argtext)*/
        RefDS(_argtext_6674);
        _0 = _argtext_6674;
        _argtext_6674 = _16proper(_argtext_6674);
        DeRefDS(_0);
        goto L58; // [2838] 2878

        /**     					case 0 then*/
        case 0:

        /** 							cap = cap*/
        _cap_6487 = _cap_6487;
        goto L58; // [2851] 2878

        /**     					case else*/
        default:

        /**     						error:crash("logic error: 'cap' mode in format.")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_2860_6994);
        _msg_inlined_crash_at_2860_6994 = EPrintf(-9999999, _3832, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_2860_6994);

        /** end procedure*/
        goto L59; // [2872] 2875
L59: 
        DeRefi(_msg_inlined_crash_at_2860_6994);
        _msg_inlined_crash_at_2860_6994 = NOVALUE;
    ;}L58: 

    /** 					if atom(currargv) then*/
    _3833 = IS_ATOM(_currargv_6506);
    if (_3833 == 0)
    {
        _3833 = NOVALUE;
        goto L5A; // [2885] 3136
    }
    else{
        _3833 = NOVALUE;
    }

    /** 						if find('e', argtext) = 0 then*/
    _3834 = find_from(101, _argtext_6674, 1);
    if (_3834 != 0)
    goto L5B; // [2895] 3135

    /** 							pflag = 0*/
    _pflag_6511 = 0;

    /** 							if msign and currargv < 0 then*/
    if (_msign_6490 == 0) {
        goto L5C; // [2910] 2930
    }
    if (IS_ATOM_INT(_currargv_6506)) {
        _3837 = (_currargv_6506 < 0);
    }
    else {
        _3837 = binary_op(LESS, _currargv_6506, 0);
    }
    if (_3837 == 0) {
        DeRef(_3837);
        _3837 = NOVALUE;
        goto L5C; // [2919] 2930
    }
    else {
        if (!IS_ATOM_INT(_3837) && DBL_PTR(_3837)->dbl == 0.0){
            DeRef(_3837);
            _3837 = NOVALUE;
            goto L5C; // [2919] 2930
        }
        DeRef(_3837);
        _3837 = NOVALUE;
    }
    DeRef(_3837);
    _3837 = NOVALUE;

    /** 								pflag = 1*/
    _pflag_6511 = 1;
L5C: 

    /** 							if decs != -1 then*/
    if (_decs_6496 == -1)
    goto L5D; // [2934] 3134

    /** 								pos = find('.', argtext)*/
    _pos_6497 = find_from(46, _argtext_6674, 1);

    /** 								if pos then*/
    if (_pos_6497 == 0)
    {
        goto L5E; // [2949] 3079
    }
    else{
    }

    /** 									if decs = 0 then*/
    if (_decs_6496 != 0)
    goto L5F; // [2954] 2972

    /** 										argtext = argtext [1 .. pos-1 ]*/
    _3841 = _pos_6497 - 1;
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, 1, _3841);
    goto L60; // [2969] 3133
L5F: 

    /** 										pos = length(argtext) - pos - pflag*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3843 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3843 = 1;
    }
    _3844 = _3843 - _pos_6497;
    if ((long)((unsigned long)_3844 +(unsigned long) HIGH_BITS) >= 0){
        _3844 = NewDouble((double)_3844);
    }
    _3843 = NOVALUE;
    if (IS_ATOM_INT(_3844)) {
        _pos_6497 = _3844 - _pflag_6511;
    }
    else {
        _pos_6497 = NewDouble(DBL_PTR(_3844)->dbl - (double)_pflag_6511);
    }
    DeRef(_3844);
    _3844 = NOVALUE;
    if (!IS_ATOM_INT(_pos_6497)) {
        _1 = (long)(DBL_PTR(_pos_6497)->dbl);
        if (UNIQUE(DBL_PTR(_pos_6497)) && (DBL_PTR(_pos_6497)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_6497);
        _pos_6497 = _1;
    }

    /** 										if pos > decs then*/
    if (_pos_6497 <= _decs_6496)
    goto L61; // [2991] 3016

    /** 											argtext = argtext[ 1 .. $ - pos + decs ]*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3847 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3847 = 1;
    }
    _3848 = _3847 - _pos_6497;
    if ((long)((unsigned long)_3848 +(unsigned long) HIGH_BITS) >= 0){
        _3848 = NewDouble((double)_3848);
    }
    _3847 = NOVALUE;
    if (IS_ATOM_INT(_3848)) {
        _3849 = _3848 + _decs_6496;
    }
    else {
        _3849 = NewDouble(DBL_PTR(_3848)->dbl + (double)_decs_6496);
    }
    DeRef(_3848);
    _3848 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, 1, _3849);
    goto L60; // [3013] 3133
L61: 

    /** 										elsif pos < decs then*/
    if (_pos_6497 >= _decs_6496)
    goto L60; // [3018] 3133

    /** 											if pflag then*/
    if (_pflag_6511 == 0)
    {
        goto L62; // [3024] 3058
    }
    else{
    }

    /** 												argtext = argtext[ 1 .. $ - 1 ] & repeat('0', decs - pos) & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3852 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3852 = 1;
    }
    _3853 = _3852 - 1;
    _3852 = NOVALUE;
    rhs_slice_target = (object_ptr)&_3854;
    RHS_Slice(_argtext_6674, 1, _3853);
    _3855 = _decs_6496 - _pos_6497;
    _3856 = Repeat(48, _3855);
    _3855 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _3856;
        concat_list[2] = _3854;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3856);
    _3856 = NOVALUE;
    DeRefDS(_3854);
    _3854 = NOVALUE;
    goto L60; // [3055] 3133
L62: 

    /** 												argtext = argtext & repeat('0', decs - pos)*/
    _3858 = _decs_6496 - _pos_6497;
    _3859 = Repeat(48, _3858);
    _3858 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _argtext_6674, _3859);
    DeRefDS(_3859);
    _3859 = NOVALUE;
    goto L60; // [3076] 3133
L5E: 

    /** 								elsif decs > 0 then*/
    if (_decs_6496 <= 0)
    goto L63; // [3081] 3132

    /** 									if pflag then*/
    if (_pflag_6511 == 0)
    {
        goto L64; // [3087] 3118
    }
    else{
    }

    /** 										argtext = argtext[1 .. $ - 1] & '.' & repeat('0', decs) & ')'*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3862 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3862 = 1;
    }
    _3863 = _3862 - 1;
    _3862 = NOVALUE;
    rhs_slice_target = (object_ptr)&_3864;
    RHS_Slice(_argtext_6674, 1, _3863);
    _3865 = Repeat(48, _decs_6496);
    {
        int concat_list[4];

        concat_list[0] = 41;
        concat_list[1] = _3865;
        concat_list[2] = 46;
        concat_list[3] = _3864;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 4);
    }
    DeRefDS(_3865);
    _3865 = NOVALUE;
    DeRefDS(_3864);
    _3864 = NOVALUE;
    goto L65; // [3115] 3131
L64: 

    /** 										argtext = argtext & '.' & repeat('0', decs)*/
    _3867 = Repeat(48, _decs_6496);
    {
        int concat_list[3];

        concat_list[0] = _3867;
        concat_list[1] = 46;
        concat_list[2] = _argtext_6674;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3867);
    _3867 = NOVALUE;
L65: 
L63: 
L60: 
L5D: 
L5B: 
L5A: 

    /**     				if align = 0 then*/
    if (_align_6488 != 0)
    goto L66; // [3140] 3171

    /**     					if atom(currargv) then*/
    _3870 = IS_ATOM(_currargv_6506);
    if (_3870 == 0)
    {
        _3870 = NOVALUE;
        goto L67; // [3149] 3162
    }
    else{
        _3870 = NOVALUE;
    }

    /**     						align = '>'*/
    _align_6488 = 62;
    goto L68; // [3159] 3170
L67: 

    /**     						align = '<'*/
    _align_6488 = 60;
L68: 
L66: 

    /**     				if atom(currargv) then*/
    _3871 = IS_ATOM(_currargv_6506);
    if (_3871 == 0)
    {
        _3871 = NOVALUE;
        goto L69; // [3176] 3427
    }
    else{
        _3871 = NOVALUE;
    }

    /** 	    				if tsep != 0 and zfill = 0 then*/
    _3872 = (_tsep_6503 != 0);
    if (_3872 == 0) {
        goto L6A; // [3187] 3424
    }
    _3874 = (_zfill_6491 == 0);
    if (_3874 == 0)
    {
        DeRef(_3874);
        _3874 = NOVALUE;
        goto L6A; // [3198] 3424
    }
    else{
        DeRef(_3874);
        _3874 = NOVALUE;
    }

    /** 	    					integer dpos*/

    /** 	    					integer dist*/

    /** 	    					integer bracketed*/

    /** 	    					if binout or hexout then*/
    if (_binout_6502 != 0) {
        goto L6B; // [3211] 3220
    }
    if (_hexout_6501 == 0)
    {
        goto L6C; // [3216] 3237
    }
    else{
    }
L6B: 

    /** 	    						dist = 4*/
    _dist_7057 = 4;

    /** 							psign = 0*/
    _psign_6489 = 0;
    goto L6D; // [3234] 3245
L6C: 

    /** 	    						dist = 3*/
    _dist_7057 = 3;
L6D: 

    /** 	    					bracketed = (argtext[1] = '(')*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    _3876 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3876)) {
        _bracketed_7058 = (_3876 == 40);
    }
    else {
        _bracketed_7058 = binary_op(EQUALS, _3876, 40);
    }
    _3876 = NOVALUE;
    if (!IS_ATOM_INT(_bracketed_7058)) {
        _1 = (long)(DBL_PTR(_bracketed_7058)->dbl);
        if (UNIQUE(DBL_PTR(_bracketed_7058)) && (DBL_PTR(_bracketed_7058)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bracketed_7058);
        _bracketed_7058 = _1;
    }

    /** 	    					if bracketed then*/
    if (_bracketed_7058 == 0)
    {
        goto L6E; // [3261] 3279
    }
    else{
    }

    /** 	    						argtext = argtext[2 .. $-1]*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3878 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3878 = 1;
    }
    _3879 = _3878 - 1;
    _3878 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, 2, _3879);
L6E: 

    /** 	    					dpos = find('.', argtext)*/
    _dpos_7056 = find_from(46, _argtext_6674, 1);

    /** 	    					if dpos = 0 then*/
    if (_dpos_7056 != 0)
    goto L6F; // [3290] 3308

    /** 	    						dpos = length(argtext) + 1*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3883 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3883 = 1;
    }
    _dpos_7056 = _3883 + 1;
    _3883 = NOVALUE;
    goto L70; // [3305] 3322
L6F: 

    /** 	    						if tsep = '.' then*/
    if (_tsep_6503 != 46)
    goto L71; // [3310] 3321

    /** 	    							argtext[dpos] = ','*/
    _2 = (int)SEQ_PTR(_argtext_6674);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_6674 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _dpos_7056);
    _1 = *(int *)_2;
    *(int *)_2 = 44;
    DeRef(_1);
L71: 
L70: 

    /** 	    					while dpos > dist do*/
L72: 
    if (_dpos_7056 <= _dist_7057)
    goto L73; // [3329] 3409

    /** 	    						dpos -= dist*/
    _dpos_7056 = _dpos_7056 - _dist_7057;

    /** 	    						if dpos > 1 + (currargv < 0) * not msign + (currargv > 0) * psign then*/
    if (IS_ATOM_INT(_currargv_6506)) {
        _3888 = (_currargv_6506 < 0);
    }
    else {
        _3888 = binary_op(LESS, _currargv_6506, 0);
    }
    _3889 = (_msign_6490 == 0);
    if (IS_ATOM_INT(_3888)) {
        if (_3888 == (short)_3888 && _3889 <= INT15 && _3889 >= -INT15)
        _3890 = _3888 * _3889;
        else
        _3890 = NewDouble(_3888 * (double)_3889);
    }
    else {
        _3890 = binary_op(MULTIPLY, _3888, _3889);
    }
    DeRef(_3888);
    _3888 = NOVALUE;
    _3889 = NOVALUE;
    if (IS_ATOM_INT(_3890)) {
        _3891 = _3890 + 1;
        if (_3891 > MAXINT){
            _3891 = NewDouble((double)_3891);
        }
    }
    else
    _3891 = binary_op(PLUS, 1, _3890);
    DeRef(_3890);
    _3890 = NOVALUE;
    if (IS_ATOM_INT(_currargv_6506)) {
        _3892 = (_currargv_6506 > 0);
    }
    else {
        _3892 = binary_op(GREATER, _currargv_6506, 0);
    }
    if (IS_ATOM_INT(_3892)) {
        if (_3892 == (short)_3892 && _psign_6489 <= INT15 && _psign_6489 >= -INT15)
        _3893 = _3892 * _psign_6489;
        else
        _3893 = NewDouble(_3892 * (double)_psign_6489);
    }
    else {
        _3893 = binary_op(MULTIPLY, _3892, _psign_6489);
    }
    DeRef(_3892);
    _3892 = NOVALUE;
    if (IS_ATOM_INT(_3891) && IS_ATOM_INT(_3893)) {
        _3894 = _3891 + _3893;
        if ((long)((unsigned long)_3894 + (unsigned long)HIGH_BITS) >= 0) 
        _3894 = NewDouble((double)_3894);
    }
    else {
        _3894 = binary_op(PLUS, _3891, _3893);
    }
    DeRef(_3891);
    _3891 = NOVALUE;
    DeRef(_3893);
    _3893 = NOVALUE;
    if (binary_op_a(LESSEQ, _dpos_7056, _3894)){
        DeRef(_3894);
        _3894 = NOVALUE;
        goto L72; // [3374] 3327
    }
    DeRef(_3894);
    _3894 = NOVALUE;

    /** 	    							argtext = argtext[1.. dpos - 1] & tsep & argtext[dpos .. $]*/
    _3896 = _dpos_7056 - 1;
    rhs_slice_target = (object_ptr)&_3897;
    RHS_Slice(_argtext_6674, 1, _3896);
    if (IS_SEQUENCE(_argtext_6674)){
            _3898 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3898 = 1;
    }
    rhs_slice_target = (object_ptr)&_3899;
    RHS_Slice(_argtext_6674, _dpos_7056, _3898);
    {
        int concat_list[3];

        concat_list[0] = _3899;
        concat_list[1] = _tsep_6503;
        concat_list[2] = _3897;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3899);
    _3899 = NOVALUE;
    DeRefDS(_3897);
    _3897 = NOVALUE;

    /** 	    					end while*/
    goto L72; // [3406] 3327
L73: 

    /** 	    					if bracketed then*/
    if (_bracketed_7058 == 0)
    {
        goto L74; // [3411] 3423
    }
    else{
    }

    /** 	    						argtext = '(' & argtext & ')'*/
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _argtext_6674;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
L74: 
L6A: 
L69: 

    /**     				if width <= 0 then*/
    if (_width_6495 > 0)
    goto L75; // [3431] 3443

    /**     					width = length(argtext)*/
    if (IS_SEQUENCE(_argtext_6674)){
            _width_6495 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _width_6495 = 1;
    }
L75: 

    /**     				if width < length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3904 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3904 = 1;
    }
    if (_width_6495 >= _3904)
    goto L76; // [3448] 3585

    /**     					if align = '>' then*/
    if (_align_6488 != 62)
    goto L77; // [3454] 3482

    /**     						argtext = argtext[ $ - width + 1 .. $]*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3907 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3907 = 1;
    }
    _3908 = _3907 - _width_6495;
    if ((long)((unsigned long)_3908 +(unsigned long) HIGH_BITS) >= 0){
        _3908 = NewDouble((double)_3908);
    }
    _3907 = NOVALUE;
    if (IS_ATOM_INT(_3908)) {
        _3909 = _3908 + 1;
        if (_3909 > MAXINT){
            _3909 = NewDouble((double)_3909);
        }
    }
    else
    _3909 = binary_op(PLUS, 1, _3908);
    DeRef(_3908);
    _3908 = NOVALUE;
    if (IS_SEQUENCE(_argtext_6674)){
            _3910 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3910 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, _3909, _3910);
    goto L78; // [3479] 3728
L77: 

    /**     					elsif align = 'c' then*/
    if (_align_6488 != 99)
    goto L79; // [3484] 3574

    /**     						pos = length(argtext) - width*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3913 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3913 = 1;
    }
    _pos_6497 = _3913 - _width_6495;
    _3913 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _3915 = (_pos_6497 % 2);
    if (_3915 != 0)
    goto L7A; // [3505] 3540

    /**     							pos = pos / 2*/
    if (_pos_6497 & 1) {
        _pos_6497 = NewDouble((_pos_6497 >> 1) + 0.5);
    }
    else
    _pos_6497 = _pos_6497 >> 1;
    if (!IS_ATOM_INT(_pos_6497)) {
        _1 = (long)(DBL_PTR(_pos_6497)->dbl);
        if (UNIQUE(DBL_PTR(_pos_6497)) && (DBL_PTR(_pos_6497)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_6497);
        _pos_6497 = _1;
    }

    /**     							argtext = argtext[ pos + 1 .. $ - pos ]*/
    _3918 = _pos_6497 + 1;
    if (_3918 > MAXINT){
        _3918 = NewDouble((double)_3918);
    }
    if (IS_SEQUENCE(_argtext_6674)){
            _3919 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3919 = 1;
    }
    _3920 = _3919 - _pos_6497;
    _3919 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, _3918, _3920);
    goto L78; // [3537] 3728
L7A: 

    /**     							pos = floor(pos / 2)*/
    _pos_6497 = _pos_6497 >> 1;

    /**     							argtext = argtext[ pos + 1 .. $ - pos - 1]*/
    _3923 = _pos_6497 + 1;
    if (_3923 > MAXINT){
        _3923 = NewDouble((double)_3923);
    }
    if (IS_SEQUENCE(_argtext_6674)){
            _3924 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3924 = 1;
    }
    _3925 = _3924 - _pos_6497;
    if ((long)((unsigned long)_3925 +(unsigned long) HIGH_BITS) >= 0){
        _3925 = NewDouble((double)_3925);
    }
    _3924 = NOVALUE;
    if (IS_ATOM_INT(_3925)) {
        _3926 = _3925 - 1;
    }
    else {
        _3926 = NewDouble(DBL_PTR(_3925)->dbl - (double)1);
    }
    DeRef(_3925);
    _3925 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, _3923, _3926);
    goto L78; // [3571] 3728
L79: 

    /**     						argtext = argtext[ 1 .. width]*/
    rhs_slice_target = (object_ptr)&_argtext_6674;
    RHS_Slice(_argtext_6674, 1, _width_6495);
    goto L78; // [3582] 3728
L76: 

    /**     				elsif width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3929 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3929 = 1;
    }
    if (_width_6495 <= _3929)
    goto L7B; // [3590] 3727

    /** 						if align = '>' then*/
    if (_align_6488 != 62)
    goto L7C; // [3596] 3620

    /** 							argtext = repeat(' ', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3932 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3932 = 1;
    }
    _3933 = _width_6495 - _3932;
    _3932 = NOVALUE;
    _3934 = Repeat(32, _3933);
    _3933 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _3934, _argtext_6674);
    DeRefDS(_3934);
    _3934 = NOVALUE;
    DeRef(_3934);
    _3934 = NOVALUE;
    goto L7D; // [3617] 3726
L7C: 

    /**     					elsif align = 'c' then*/
    if (_align_6488 != 99)
    goto L7E; // [3622] 3708

    /**     						pos = width - length(argtext)*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3937 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3937 = 1;
    }
    _pos_6497 = _width_6495 - _3937;
    _3937 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _3939 = (_pos_6497 % 2);
    if (_3939 != 0)
    goto L7F; // [3643] 3676

    /**     							pos = pos / 2*/
    if (_pos_6497 & 1) {
        _pos_6497 = NewDouble((_pos_6497 >> 1) + 0.5);
    }
    else
    _pos_6497 = _pos_6497 >> 1;
    if (!IS_ATOM_INT(_pos_6497)) {
        _1 = (long)(DBL_PTR(_pos_6497)->dbl);
        if (UNIQUE(DBL_PTR(_pos_6497)) && (DBL_PTR(_pos_6497)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_6497);
        _pos_6497 = _1;
    }

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos)*/
    _3942 = Repeat(32, _pos_6497);
    _3943 = Repeat(32, _pos_6497);
    {
        int concat_list[3];

        concat_list[0] = _3943;
        concat_list[1] = _argtext_6674;
        concat_list[2] = _3942;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3943);
    _3943 = NOVALUE;
    DeRefDS(_3942);
    _3942 = NOVALUE;
    goto L7D; // [3673] 3726
L7F: 

    /**     							pos = floor(pos / 2)*/
    _pos_6497 = _pos_6497 >> 1;

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos + 1)*/
    _3946 = Repeat(32, _pos_6497);
    _3947 = _pos_6497 + 1;
    _3948 = Repeat(32, _3947);
    _3947 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _3948;
        concat_list[1] = _argtext_6674;
        concat_list[2] = _3946;
        Concat_N((object_ptr)&_argtext_6674, concat_list, 3);
    }
    DeRefDS(_3948);
    _3948 = NOVALUE;
    DeRefDS(_3946);
    _3946 = NOVALUE;
    goto L7D; // [3705] 3726
L7E: 

    /** 							argtext = argtext & repeat(' ', width - length(argtext))*/
    if (IS_SEQUENCE(_argtext_6674)){
            _3950 = SEQ_PTR(_argtext_6674)->length;
    }
    else {
        _3950 = 1;
    }
    _3951 = _width_6495 - _3950;
    _3950 = NOVALUE;
    _3952 = Repeat(32, _3951);
    _3951 = NOVALUE;
    Concat((object_ptr)&_argtext_6674, _argtext_6674, _3952);
    DeRefDS(_3952);
    _3952 = NOVALUE;
L7D: 
L7B: 
L78: 

    /**     				result &= argtext*/
    Concat((object_ptr)&_result_6482, _result_6482, _argtext_6674);
    goto L80; // [3734] 3750
L57: 

    /**     				if spacer then*/
    if (_spacer_6493 == 0)
    {
        goto L81; // [3739] 3749
    }
    else{
    }

    /**     					result &= ' '*/
    Append(&_result_6482, _result_6482, 32);
L81: 
L80: 

    /**    				if trimming then*/
    if (_trimming_6500 == 0)
    {
        goto L82; // [3754] 3768
    }
    else{
    }

    /**    					result = trim(result)*/
    RefDS(_result_6482);
    RefDS(_2936);
    _0 = _result_6482;
    _result_6482 = _16trim(_result_6482, _2936, 0);
    DeRefDS(_0);
L82: 

    /**     			tend = 0*/
    _tend_6486 = 0;

    /** 		    	prevargv = currargv*/
    Ref(_currargv_6506);
    DeRef(_prevargv_6505);
    _prevargv_6505 = _currargv_6506;
L1F: 
    DeRef(_argtext_6674);
    _argtext_6674 = NOVALUE;

    /**     end while*/
    goto L2; // [3788] 70
L3: 

    /** 	return result*/
    DeRefDS(_format_pattern_6480);
    DeRef(_arg_list_6481);
    DeRef(_prevargv_6505);
    DeRef(_currargv_6506);
    DeRef(_idname_6507);
    DeRef(_envsym_6508);
    DeRefi(_envvar_6509);
    DeRef(_3610);
    _3610 = NOVALUE;
    DeRef(_3705);
    _3705 = NOVALUE;
    DeRef(_3723);
    _3723 = NOVALUE;
    DeRef(_3741);
    _3741 = NOVALUE;
    DeRef(_3918);
    _3918 = NOVALUE;
    DeRef(_3583);
    _3583 = NOVALUE;
    DeRef(_3939);
    _3939 = NOVALUE;
    DeRef(_3920);
    _3920 = NOVALUE;
    DeRef(_3926);
    _3926 = NOVALUE;
    DeRef(_3778);
    _3778 = NOVALUE;
    DeRef(_3800);
    _3800 = NOVALUE;
    DeRef(_3652);
    _3652 = NOVALUE;
    DeRef(_3642);
    _3642 = NOVALUE;
    DeRef(_3872);
    _3872 = NOVALUE;
    DeRef(_3923);
    _3923 = NOVALUE;
    _3780 = NOVALUE;
    DeRef(_3559);
    _3559 = NOVALUE;
    DeRef(_3915);
    _3915 = NOVALUE;
    DeRef(_3810);
    _3810 = NOVALUE;
    DeRef(_3632);
    _3632 = NOVALUE;
    DeRef(_3879);
    _3879 = NOVALUE;
    DeRef(_3849);
    _3849 = NOVALUE;
    DeRef(_3896);
    _3896 = NOVALUE;
    DeRef(_3853);
    _3853 = NOVALUE;
    DeRef(_3909);
    _3909 = NOVALUE;
    DeRef(_3841);
    _3841 = NOVALUE;
    DeRef(_3863);
    _3863 = NOVALUE;
    DeRef(_3622);
    _3622 = NOVALUE;
    DeRef(_3662);
    _3662 = NOVALUE;
    return _result_6482;
    ;
}


int _16wrap(int _content_7168, int _width_7169, int _wrap_with_7170, int _wrap_at_7172)
{
    int _result_7177 = NOVALUE;
    int _split_at_7180 = NOVALUE;
    int _3983 = NOVALUE;
    int _3981 = NOVALUE;
    int _3979 = NOVALUE;
    int _3978 = NOVALUE;
    int _3977 = NOVALUE;
    int _3975 = NOVALUE;
    int _3974 = NOVALUE;
    int _3972 = NOVALUE;
    int _3969 = NOVALUE;
    int _3967 = NOVALUE;
    int _3966 = NOVALUE;
    int _3965 = NOVALUE;
    int _3963 = NOVALUE;
    int _3962 = NOVALUE;
    int _3961 = NOVALUE;
    int _3959 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_width_7169)) {
        _1 = (long)(DBL_PTR(_width_7169)->dbl);
        if (UNIQUE(DBL_PTR(_width_7169)) && (DBL_PTR(_width_7169)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_width_7169);
        _width_7169 = _1;
    }

    /** 	if length(content) < width then*/
    if (IS_SEQUENCE(_content_7168)){
            _3959 = SEQ_PTR(_content_7168)->length;
    }
    else {
        _3959 = 1;
    }
    if (_3959 >= _width_7169)
    goto L1; // [16] 27

    /** 		return content*/
    DeRefDS(_wrap_with_7170);
    DeRefDS(_wrap_at_7172);
    DeRef(_result_7177);
    return _content_7168;
L1: 

    /** 	sequence result = ""*/
    RefDS(_5);
    DeRef(_result_7177);
    _result_7177 = _5;

    /** 	while length(content) do*/
L2: 
    if (IS_SEQUENCE(_content_7168)){
            _3961 = SEQ_PTR(_content_7168)->length;
    }
    else {
        _3961 = 1;
    }
    if (_3961 == 0)
    {
        _3961 = NOVALUE;
        goto L3; // [42] 267
    }
    else{
        _3961 = NOVALUE;
    }

    /** 		integer split_at = 0*/
    _split_at_7180 = 0;

    /** 		for i = width to 1 by -1 do*/
    {
        int _i_7182;
        _i_7182 = _width_7169;
L4: 
        if (_i_7182 < 1){
            goto L5; // [54] 94
        }

        /** 			if find(content[i], wrap_at) then*/
        _2 = (int)SEQ_PTR(_content_7168);
        _3962 = (int)*(((s1_ptr)_2)->base + _i_7182);
        _3963 = find_from(_3962, _wrap_at_7172, 1);
        _3962 = NOVALUE;
        if (_3963 == 0)
        {
            _3963 = NOVALUE;
            goto L6; // [72] 87
        }
        else{
            _3963 = NOVALUE;
        }

        /** 				split_at = i*/
        _split_at_7180 = _i_7182;

        /** 				exit*/
        goto L5; // [84] 94
L6: 

        /** 		end for*/
        _i_7182 = _i_7182 + -1;
        goto L4; // [89] 61
L5: 
        ;
    }

    /** 		if split_at = 0 then*/
    if (_split_at_7180 != 0)
    goto L7; // [96] 180

    /** 			for i = width to length(content) do*/
    if (IS_SEQUENCE(_content_7168)){
            _3965 = SEQ_PTR(_content_7168)->length;
    }
    else {
        _3965 = 1;
    }
    {
        int _i_7189;
        _i_7189 = _width_7169;
L8: 
        if (_i_7189 > _3965){
            goto L9; // [105] 145
        }

        /** 				if find(content[i], wrap_at) then*/
        _2 = (int)SEQ_PTR(_content_7168);
        _3966 = (int)*(((s1_ptr)_2)->base + _i_7189);
        _3967 = find_from(_3966, _wrap_at_7172, 1);
        _3966 = NOVALUE;
        if (_3967 == 0)
        {
            _3967 = NOVALUE;
            goto LA; // [123] 138
        }
        else{
            _3967 = NOVALUE;
        }

        /** 					split_at = i*/
        _split_at_7180 = _i_7189;

        /** 					exit*/
        goto L9; // [135] 145
LA: 

        /** 			end for*/
        _i_7189 = _i_7189 + 1;
        goto L8; // [140] 112
L9: 
        ;
    }

    /** 			if split_at = 0 then*/
    if (_split_at_7180 != 0)
    goto LB; // [147] 179

    /** 				if length(result) then*/
    if (IS_SEQUENCE(_result_7177)){
            _3969 = SEQ_PTR(_result_7177)->length;
    }
    else {
        _3969 = 1;
    }
    if (_3969 == 0)
    {
        _3969 = NOVALUE;
        goto LC; // [156] 166
    }
    else{
        _3969 = NOVALUE;
    }

    /** 					result &= wrap_with*/
    Concat((object_ptr)&_result_7177, _result_7177, _wrap_with_7170);
LC: 

    /** 				result &= content*/
    Concat((object_ptr)&_result_7177, _result_7177, _content_7168);

    /** 				exit*/
    goto L3; // [176] 267
LB: 
L7: 

    /** 		if length(result) then*/
    if (IS_SEQUENCE(_result_7177)){
            _3972 = SEQ_PTR(_result_7177)->length;
    }
    else {
        _3972 = 1;
    }
    if (_3972 == 0)
    {
        _3972 = NOVALUE;
        goto LD; // [185] 195
    }
    else{
        _3972 = NOVALUE;
    }

    /** 			result &= wrap_with*/
    Concat((object_ptr)&_result_7177, _result_7177, _wrap_with_7170);
LD: 

    /** 		result &= trim(content[1..split_at])*/
    rhs_slice_target = (object_ptr)&_3974;
    RHS_Slice(_content_7168, 1, _split_at_7180);
    RefDS(_2936);
    _3975 = _16trim(_3974, _2936, 0);
    _3974 = NOVALUE;
    if (IS_SEQUENCE(_result_7177) && IS_ATOM(_3975)) {
        Ref(_3975);
        Append(&_result_7177, _result_7177, _3975);
    }
    else if (IS_ATOM(_result_7177) && IS_SEQUENCE(_3975)) {
    }
    else {
        Concat((object_ptr)&_result_7177, _result_7177, _3975);
    }
    DeRef(_3975);
    _3975 = NOVALUE;

    /** 		content = trim(content[split_at + 1..$])*/
    _3977 = _split_at_7180 + 1;
    if (_3977 > MAXINT){
        _3977 = NewDouble((double)_3977);
    }
    if (IS_SEQUENCE(_content_7168)){
            _3978 = SEQ_PTR(_content_7168)->length;
    }
    else {
        _3978 = 1;
    }
    rhs_slice_target = (object_ptr)&_3979;
    RHS_Slice(_content_7168, _3977, _3978);
    RefDS(_2936);
    _0 = _content_7168;
    _content_7168 = _16trim(_3979, _2936, 0);
    DeRefDS(_0);
    _3979 = NOVALUE;

    /** 		if length(content) < width then*/
    if (IS_SEQUENCE(_content_7168)){
            _3981 = SEQ_PTR(_content_7168)->length;
    }
    else {
        _3981 = 1;
    }
    if (_3981 >= _width_7169)
    goto LE; // [239] 260

    /** 			result &= wrap_with & content*/
    Concat((object_ptr)&_3983, _wrap_with_7170, _content_7168);
    Concat((object_ptr)&_result_7177, _result_7177, _3983);
    DeRefDS(_3983);
    _3983 = NOVALUE;

    /** 			exit*/
    goto L3; // [257] 267
LE: 

    /** 	end while*/
    goto L2; // [264] 39
L3: 

    /** 	return result*/
    DeRefDS(_content_7168);
    DeRefDS(_wrap_with_7170);
    DeRefDS(_wrap_at_7172);
    DeRef(_3977);
    _3977 = NOVALUE;
    return _result_7177;
    ;
}



// 0x13207C2B
