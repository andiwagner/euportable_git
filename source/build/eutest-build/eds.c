// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _33fatal(int _errcode_15584, int _msg_15585, int _routine_name_15586, int _parms_15587)
{
    int _8945 = NOVALUE;
    int _0, _1, _2;
    

    /** 	vLastErrors = append(vLastErrors, {errcode, msg, routine_name, parms})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _errcode_15584;
    RefDS(_msg_15585);
    *((int *)(_2+8)) = _msg_15585;
    RefDS(_routine_name_15586);
    *((int *)(_2+12)) = _routine_name_15586;
    RefDS(_parms_15587);
    *((int *)(_2+16)) = _parms_15587;
    _8945 = MAKE_SEQ(_1);
    RefDS(_8945);
    Append(&_33vLastErrors_15581, _33vLastErrors_15581, _8945);
    DeRefDS(_8945);
    _8945 = NOVALUE;

    /** 	if db_fatal_id >= 0 then*/

    /** end procedure*/
    DeRefDSi(_msg_15585);
    DeRefDSi(_routine_name_15586);
    DeRefDS(_parms_15587);
    return;
    ;
}


int _33get4()
{
    int _8961 = NOVALUE;
    int _8960 = NOVALUE;
    int _8959 = NOVALUE;
    int _8958 = NOVALUE;
    int _8957 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(current_db))*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _8957 = getKBchar();
        }
        else
        _8957 = getc(last_r_file_ptr);
    }
    else
    _8957 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_33mem0_15599)){
        poke_addr = (unsigned char *)_33mem0_15599;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke_addr = (unsigned char)_8957;
    _8957 = NOVALUE;

    /** 	poke(mem1, getc(current_db))*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _8958 = getKBchar();
        }
        else
        _8958 = getc(last_r_file_ptr);
    }
    else
    _8958 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_33mem1_15600)){
        poke_addr = (unsigned char *)_33mem1_15600;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_33mem1_15600)->dbl);
    }
    *poke_addr = (unsigned char)_8958;
    _8958 = NOVALUE;

    /** 	poke(mem2, getc(current_db))*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _8959 = getKBchar();
        }
        else
        _8959 = getc(last_r_file_ptr);
    }
    else
    _8959 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_33mem2_15601)){
        poke_addr = (unsigned char *)_33mem2_15601;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_33mem2_15601)->dbl);
    }
    *poke_addr = (unsigned char)_8959;
    _8959 = NOVALUE;

    /** 	poke(mem3, getc(current_db))*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _8960 = getKBchar();
        }
        else
        _8960 = getc(last_r_file_ptr);
    }
    else
    _8960 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_33mem3_15602)){
        poke_addr = (unsigned char *)_33mem3_15602;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_33mem3_15602)->dbl);
    }
    *poke_addr = (unsigned char)_8960;
    _8960 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_33mem0_15599)) {
        _8961 = *(unsigned long *)_33mem0_15599;
        if ((unsigned)_8961 > (unsigned)MAXINT)
        _8961 = NewDouble((double)(unsigned long)_8961);
    }
    else {
        _8961 = *(unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        if ((unsigned)_8961 > (unsigned)MAXINT)
        _8961 = NewDouble((double)(unsigned long)_8961);
    }
    return _8961;
    ;
}


int _33get_string()
{
    int _where_inlined_where_at_35_15626 = NOVALUE;
    int _s_15616 = NOVALUE;
    int _c_15617 = NOVALUE;
    int _i_15618 = NOVALUE;
    int _8973 = NOVALUE;
    int _8970 = NOVALUE;
    int _8968 = NOVALUE;
    int _8966 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = repeat(0, 256)*/
    DeRefi(_s_15616);
    _s_15616 = Repeat(0, 256);

    /** 	i = 0*/
    _i_15618 = 0;

    /** 	while c with entry do*/
    goto L1; // [16] 95
L2: 
    if (_c_15617 == 0)
    {
        goto L3; // [21] 109
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_15617 != -1)
    goto L4; // [26] 58

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "get_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_35_15626);
    _where_inlined_where_at_35_15626 = machine(20, _33current_db_15557);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_35_15626);
    *((int *)(_2+4)) = _where_inlined_where_at_35_15626;
    _8966 = MAKE_SEQ(_1);
    RefDS(_8964);
    RefDS(_8965);
    _33fatal(900, _8964, _8965, _8966);
    _8966 = NOVALUE;

    /** 			exit*/
    goto L3; // [55] 109
L4: 

    /** 		i += 1*/
    _i_15618 = _i_15618 + 1;

    /** 		if i > length(s) then*/
    if (IS_SEQUENCE(_s_15616)){
            _8968 = SEQ_PTR(_s_15616)->length;
    }
    else {
        _8968 = 1;
    }
    if (_i_15618 <= _8968)
    goto L5; // [71] 86

    /** 			s &= repeat(0, 256)*/
    _8970 = Repeat(0, 256);
    Concat((object_ptr)&_s_15616, _s_15616, _8970);
    DeRefDS(_8970);
    _8970 = NOVALUE;
L5: 

    /** 		s[i] = c*/
    _2 = (int)SEQ_PTR(_s_15616);
    _2 = (int)(((s1_ptr)_2)->base + _i_15618);
    *(int *)_2 = _c_15617;

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_15617 = getKBchar();
        }
        else
        _c_15617 = getc(last_r_file_ptr);
    }
    else
    _c_15617 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [106] 19
L3: 

    /** 	return s[1..i]*/
    rhs_slice_target = (object_ptr)&_8973;
    RHS_Slice(_s_15616, 1, _i_15618);
    DeRefDSi(_s_15616);
    return _8973;
    ;
}


int _33equal_string(int _target_15638)
{
    int _c_15639 = NOVALUE;
    int _i_15640 = NOVALUE;
    int _where_inlined_where_at_31_15646 = NOVALUE;
    int _8984 = NOVALUE;
    int _8983 = NOVALUE;
    int _8980 = NOVALUE;
    int _8978 = NOVALUE;
    int _8976 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = 0*/
    _i_15640 = 0;

    /** 	while c with entry do*/
    goto L1; // [12] 102
L2: 
    if (_c_15639 == 0)
    {
        goto L3; // [17] 116
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_15639 != -1)
    goto L4; // [22] 58

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "equal_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_31_15646);
    _where_inlined_where_at_31_15646 = machine(20, _33current_db_15557);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_31_15646);
    *((int *)(_2+4)) = _where_inlined_where_at_31_15646;
    _8976 = MAKE_SEQ(_1);
    RefDS(_8964);
    RefDS(_8975);
    _33fatal(900, _8964, _8975, _8976);
    _8976 = NOVALUE;

    /** 			return DB_FATAL_FAIL*/
    DeRefDS(_target_15638);
    return -404;
L4: 

    /** 		i += 1*/
    _i_15640 = _i_15640 + 1;

    /** 		if i > length(target) then*/
    if (IS_SEQUENCE(_target_15638)){
            _8978 = SEQ_PTR(_target_15638)->length;
    }
    else {
        _8978 = 1;
    }
    if (_i_15640 <= _8978)
    goto L5; // [71] 82

    /** 			return 0*/
    DeRefDS(_target_15638);
    return 0;
L5: 

    /** 		if target[i] != c then*/
    _2 = (int)SEQ_PTR(_target_15638);
    _8980 = (int)*(((s1_ptr)_2)->base + _i_15640);
    if (binary_op_a(EQUALS, _8980, _c_15639)){
        _8980 = NOVALUE;
        goto L6; // [88] 99
    }
    _8980 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_target_15638);
    return 0;
L6: 

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_15639 = getKBchar();
        }
        else
        _c_15639 = getc(last_r_file_ptr);
    }
    else
    _c_15639 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [113] 15
L3: 

    /** 	return (i = length(target))*/
    if (IS_SEQUENCE(_target_15638)){
            _8983 = SEQ_PTR(_target_15638)->length;
    }
    else {
        _8983 = 1;
    }
    _8984 = (_i_15640 == _8983);
    _8983 = NOVALUE;
    DeRefDS(_target_15638);
    return _8984;
    ;
}


int _33decompress(int _c_15684)
{
    int _s_15685 = NOVALUE;
    int _len_15686 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_180_15721 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_177_15720 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_255_15734 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_252_15733 = NOVALUE;
    int _9039 = NOVALUE;
    int _9038 = NOVALUE;
    int _9037 = NOVALUE;
    int _9034 = NOVALUE;
    int _9029 = NOVALUE;
    int _9028 = NOVALUE;
    int _9027 = NOVALUE;
    int _9026 = NOVALUE;
    int _9025 = NOVALUE;
    int _9024 = NOVALUE;
    int _9023 = NOVALUE;
    int _9022 = NOVALUE;
    int _9021 = NOVALUE;
    int _9020 = NOVALUE;
    int _9019 = NOVALUE;
    int _9018 = NOVALUE;
    int _9017 = NOVALUE;
    int _9016 = NOVALUE;
    int _9015 = NOVALUE;
    int _9014 = NOVALUE;
    int _9013 = NOVALUE;
    int _9012 = NOVALUE;
    int _9011 = NOVALUE;
    int _9010 = NOVALUE;
    int _9009 = NOVALUE;
    int _9008 = NOVALUE;
    int _9007 = NOVALUE;
    int _9006 = NOVALUE;
    int _9005 = NOVALUE;
    int _9004 = NOVALUE;
    int _9003 = NOVALUE;
    int _9002 = NOVALUE;
    int _9001 = NOVALUE;
    int _8998 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_15684 != 0)
    goto L1; // [7] 38

    /** 		c = getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_15684 = getKBchar();
        }
        else
        _c_15684 = getc(last_r_file_ptr);
    }
    else
    _c_15684 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_15684 >= 249)
    goto L2; // [22] 37

    /** 			return c + MIN1B*/
    _8998 = _c_15684 + -9;
    DeRef(_s_15685);
    return _8998;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_15684;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(current_db) +*/
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9001 = getKBchar();
            }
            else
            _9001 = getc(last_r_file_ptr);
        }
        else
        _9001 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9002 = getKBchar();
            }
            else
            _9002 = getc(last_r_file_ptr);
        }
        else
        _9002 = getc(last_r_file_ptr);
        _9003 = 256 * _9002;
        _9002 = NOVALUE;
        _9004 = _9001 + _9003;
        _9001 = NOVALUE;
        _9003 = NOVALUE;
        _9005 = _9004 + _33MIN2B_15667;
        if ((long)((unsigned long)_9005 + (unsigned long)HIGH_BITS) >= 0) 
        _9005 = NewDouble((double)_9005);
        _9004 = NOVALUE;
        DeRef(_s_15685);
        DeRef(_8998);
        _8998 = NOVALUE;
        return _9005;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(current_db) +*/
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9006 = getKBchar();
            }
            else
            _9006 = getc(last_r_file_ptr);
        }
        else
        _9006 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9007 = getKBchar();
            }
            else
            _9007 = getc(last_r_file_ptr);
        }
        else
        _9007 = getc(last_r_file_ptr);
        _9008 = 256 * _9007;
        _9007 = NOVALUE;
        _9009 = _9006 + _9008;
        _9006 = NOVALUE;
        _9008 = NOVALUE;
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9010 = getKBchar();
            }
            else
            _9010 = getc(last_r_file_ptr);
        }
        else
        _9010 = getc(last_r_file_ptr);
        _9011 = 65536 * _9010;
        _9010 = NOVALUE;
        _9012 = _9009 + _9011;
        _9009 = NOVALUE;
        _9011 = NOVALUE;
        _9013 = _9012 + _33MIN3B_15673;
        if ((long)((unsigned long)_9013 + (unsigned long)HIGH_BITS) >= 0) 
        _9013 = NewDouble((double)_9013);
        _9012 = NOVALUE;
        DeRef(_s_15685);
        DeRef(_8998);
        _8998 = NOVALUE;
        DeRef(_9005);
        _9005 = NOVALUE;
        return _9013;

        /** 		case I4B then*/
        case 251:

        /** 			return get4() + MIN4B*/
        _9014 = _33get4();
        if (IS_ATOM_INT(_9014) && IS_ATOM_INT(_33MIN4B_15679)) {
            _9015 = _9014 + _33MIN4B_15679;
            if ((long)((unsigned long)_9015 + (unsigned long)HIGH_BITS) >= 0) 
            _9015 = NewDouble((double)_9015);
        }
        else {
            _9015 = binary_op(PLUS, _9014, _33MIN4B_15679);
        }
        DeRef(_9014);
        _9014 = NOVALUE;
        DeRef(_s_15685);
        DeRef(_8998);
        _8998 = NOVALUE;
        DeRef(_9005);
        _9005 = NOVALUE;
        DeRef(_9013);
        _9013 = NOVALUE;
        return _9015;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(current_db), getc(current_db),*/
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9016 = getKBchar();
            }
            else
            _9016 = getc(last_r_file_ptr);
        }
        else
        _9016 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9017 = getKBchar();
            }
            else
            _9017 = getc(last_r_file_ptr);
        }
        else
        _9017 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9018 = getKBchar();
            }
            else
            _9018 = getc(last_r_file_ptr);
        }
        else
        _9018 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9019 = getKBchar();
            }
            else
            _9019 = getc(last_r_file_ptr);
        }
        else
        _9019 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _9016;
        *((int *)(_2+8)) = _9017;
        *((int *)(_2+12)) = _9018;
        *((int *)(_2+16)) = _9019;
        _9020 = MAKE_SEQ(_1);
        _9019 = NOVALUE;
        _9018 = NOVALUE;
        _9017 = NOVALUE;
        _9016 = NOVALUE;
        DeRefi(_ieee32_inlined_float32_to_atom_at_177_15720);
        _ieee32_inlined_float32_to_atom_at_177_15720 = _9020;
        _9020 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_180_15721);
        _float32_to_atom_inlined_float32_to_atom_at_180_15721 = machine(49, _ieee32_inlined_float32_to_atom_at_177_15720);
        DeRefi(_ieee32_inlined_float32_to_atom_at_177_15720);
        _ieee32_inlined_float32_to_atom_at_177_15720 = NOVALUE;
        DeRef(_s_15685);
        DeRef(_8998);
        _8998 = NOVALUE;
        DeRef(_9005);
        _9005 = NOVALUE;
        DeRef(_9013);
        _9013 = NOVALUE;
        DeRef(_9015);
        _9015 = NOVALUE;
        return _float32_to_atom_inlined_float32_to_atom_at_180_15721;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(current_db), getc(current_db),*/
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9021 = getKBchar();
            }
            else
            _9021 = getc(last_r_file_ptr);
        }
        else
        _9021 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9022 = getKBchar();
            }
            else
            _9022 = getc(last_r_file_ptr);
        }
        else
        _9022 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9023 = getKBchar();
            }
            else
            _9023 = getc(last_r_file_ptr);
        }
        else
        _9023 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9024 = getKBchar();
            }
            else
            _9024 = getc(last_r_file_ptr);
        }
        else
        _9024 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9025 = getKBchar();
            }
            else
            _9025 = getc(last_r_file_ptr);
        }
        else
        _9025 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9026 = getKBchar();
            }
            else
            _9026 = getc(last_r_file_ptr);
        }
        else
        _9026 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9027 = getKBchar();
            }
            else
            _9027 = getc(last_r_file_ptr);
        }
        else
        _9027 = getc(last_r_file_ptr);
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _9028 = getKBchar();
            }
            else
            _9028 = getc(last_r_file_ptr);
        }
        else
        _9028 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _9021;
        *((int *)(_2+8)) = _9022;
        *((int *)(_2+12)) = _9023;
        *((int *)(_2+16)) = _9024;
        *((int *)(_2+20)) = _9025;
        *((int *)(_2+24)) = _9026;
        *((int *)(_2+28)) = _9027;
        *((int *)(_2+32)) = _9028;
        _9029 = MAKE_SEQ(_1);
        _9028 = NOVALUE;
        _9027 = NOVALUE;
        _9026 = NOVALUE;
        _9025 = NOVALUE;
        _9024 = NOVALUE;
        _9023 = NOVALUE;
        _9022 = NOVALUE;
        _9021 = NOVALUE;
        DeRefi(_ieee64_inlined_float64_to_atom_at_252_15733);
        _ieee64_inlined_float64_to_atom_at_252_15733 = _9029;
        _9029 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_255_15734);
        _float64_to_atom_inlined_float64_to_atom_at_255_15734 = machine(47, _ieee64_inlined_float64_to_atom_at_252_15733);
        DeRefi(_ieee64_inlined_float64_to_atom_at_252_15733);
        _ieee64_inlined_float64_to_atom_at_252_15733 = NOVALUE;
        DeRef(_s_15685);
        DeRef(_8998);
        _8998 = NOVALUE;
        DeRef(_9005);
        _9005 = NOVALUE;
        DeRef(_9013);
        _9013 = NOVALUE;
        DeRef(_9015);
        _9015 = NOVALUE;
        return _float64_to_atom_inlined_float64_to_atom_at_255_15734;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_15684 != 254)
        goto L3; // [277] 293

        /** 				len = getc(current_db)*/
        if (_33current_db_15557 != last_r_file_no) {
            last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
            last_r_file_no = _33current_db_15557;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_15686 = getKBchar();
            }
            else
            _len_15686 = getc(last_r_file_ptr);
        }
        else
        _len_15686 = getc(last_r_file_ptr);
        goto L4; // [290] 303
L3: 

        /** 				len = get4()*/
        _len_15686 = _33get4();
        if (!IS_ATOM_INT(_len_15686)) {
            _1 = (long)(DBL_PTR(_len_15686)->dbl);
            if (UNIQUE(DBL_PTR(_len_15686)) && (DBL_PTR(_len_15686)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_15686);
            _len_15686 = _1;
        }
L4: 

        /** 			s = repeat(0, len)*/
        DeRef(_s_15685);
        _s_15685 = Repeat(0, _len_15686);

        /** 			for i = 1 to len do*/
        _9034 = _len_15686;
        {
            int _i_15743;
            _i_15743 = 1;
L5: 
            if (_i_15743 > _9034){
                goto L6; // [316] 372
            }

            /** 				c = getc(current_db)*/
            if (_33current_db_15557 != last_r_file_no) {
                last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
                last_r_file_no = _33current_db_15557;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_15684 = getKBchar();
                }
                else
                _c_15684 = getc(last_r_file_ptr);
            }
            else
            _c_15684 = getc(last_r_file_ptr);

            /** 				if c < I2B then*/
            if (_c_15684 >= 249)
            goto L7; // [334] 351

            /** 					s[i] = c + MIN1B*/
            _9037 = _c_15684 + -9;
            _2 = (int)SEQ_PTR(_s_15685);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_15685 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_15743);
            _1 = *(int *)_2;
            *(int *)_2 = _9037;
            if( _1 != _9037 ){
                DeRef(_1);
            }
            _9037 = NOVALUE;
            goto L8; // [348] 365
L7: 

            /** 					s[i] = decompress(c)*/
            DeRef(_9038);
            _9038 = _c_15684;
            _9039 = _33decompress(_9038);
            _9038 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_15685);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_15685 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_15743);
            _1 = *(int *)_2;
            *(int *)_2 = _9039;
            if( _1 != _9039 ){
                DeRef(_1);
            }
            _9039 = NOVALUE;
L8: 

            /** 			end for*/
            _i_15743 = _i_15743 + 1;
            goto L5; // [367] 323
L6: 
            ;
        }

        /** 			return s*/
        DeRef(_8998);
        _8998 = NOVALUE;
        DeRef(_9005);
        _9005 = NOVALUE;
        DeRef(_9013);
        _9013 = NOVALUE;
        DeRef(_9015);
        _9015 = NOVALUE;
        return _s_15685;
    ;}    ;
}


int _33compress(int _x_15754)
{
    int _x4_15755 = NOVALUE;
    int _s_15756 = NOVALUE;
    int _atom_to_float32_inlined_atom_to_float32_at_192_15790 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_203_15793 = NOVALUE;
    int _atom_to_float64_inlined_atom_to_float64_at_229_15798 = NOVALUE;
    int _9078 = NOVALUE;
    int _9077 = NOVALUE;
    int _9076 = NOVALUE;
    int _9074 = NOVALUE;
    int _9073 = NOVALUE;
    int _9071 = NOVALUE;
    int _9069 = NOVALUE;
    int _9068 = NOVALUE;
    int _9067 = NOVALUE;
    int _9065 = NOVALUE;
    int _9064 = NOVALUE;
    int _9063 = NOVALUE;
    int _9062 = NOVALUE;
    int _9061 = NOVALUE;
    int _9060 = NOVALUE;
    int _9059 = NOVALUE;
    int _9058 = NOVALUE;
    int _9057 = NOVALUE;
    int _9055 = NOVALUE;
    int _9054 = NOVALUE;
    int _9053 = NOVALUE;
    int _9052 = NOVALUE;
    int _9051 = NOVALUE;
    int _9050 = NOVALUE;
    int _9048 = NOVALUE;
    int _9047 = NOVALUE;
    int _9046 = NOVALUE;
    int _9045 = NOVALUE;
    int _9044 = NOVALUE;
    int _9043 = NOVALUE;
    int _9042 = NOVALUE;
    int _9041 = NOVALUE;
    int _9040 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_15754))
    _9040 = 1;
    else if (IS_ATOM_DBL(_x_15754))
    _9040 = IS_ATOM_INT(DoubleToInt(_x_15754));
    else
    _9040 = 0;
    if (_9040 == 0)
    {
        _9040 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _9040 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_15754)) {
        _9041 = (_x_15754 >= -9);
    }
    else {
        _9041 = binary_op(GREATEREQ, _x_15754, -9);
    }
    if (IS_ATOM_INT(_9041)) {
        if (_9041 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_9041)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_15754)) {
        _9043 = (_x_15754 <= 239);
    }
    else {
        _9043 = binary_op(LESSEQ, _x_15754, 239);
    }
    if (_9043 == 0) {
        DeRef(_9043);
        _9043 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_9043) && DBL_PTR(_9043)->dbl == 0.0){
            DeRef(_9043);
            _9043 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_9043);
        _9043 = NOVALUE;
    }
    DeRef(_9043);
    _9043 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_15754)) {
        _9044 = _x_15754 - -9;
        if ((long)((unsigned long)_9044 +(unsigned long) HIGH_BITS) >= 0){
            _9044 = NewDouble((double)_9044);
        }
    }
    else {
        _9044 = binary_op(MINUS, _x_15754, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9044;
    _9045 = MAKE_SEQ(_1);
    _9044 = NOVALUE;
    DeRef(_x_15754);
    DeRefi(_x4_15755);
    DeRef(_s_15756);
    DeRef(_9041);
    _9041 = NOVALUE;
    return _9045;
    goto L3; // [41] 328
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_15754)) {
        _9046 = (_x_15754 >= _33MIN2B_15667);
    }
    else {
        _9046 = binary_op(GREATEREQ, _x_15754, _33MIN2B_15667);
    }
    if (IS_ATOM_INT(_9046)) {
        if (_9046 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_9046)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_15754)) {
        _9048 = (_x_15754 <= 32767);
    }
    else {
        _9048 = binary_op(LESSEQ, _x_15754, 32767);
    }
    if (_9048 == 0) {
        DeRef(_9048);
        _9048 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_9048) && DBL_PTR(_9048)->dbl == 0.0){
            DeRef(_9048);
            _9048 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_9048);
        _9048 = NOVALUE;
    }
    DeRef(_9048);
    _9048 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_15754;
    if (IS_ATOM_INT(_x_15754)) {
        _x_15754 = _x_15754 - _33MIN2B_15667;
        if ((long)((unsigned long)_x_15754 +(unsigned long) HIGH_BITS) >= 0){
            _x_15754 = NewDouble((double)_x_15754);
        }
    }
    else {
        _x_15754 = binary_op(MINUS, _x_15754, _33MIN2B_15667);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_15754)) {
        {unsigned long tu;
             tu = (unsigned long)_x_15754 & (unsigned long)255;
             _9050 = MAKE_UINT(tu);
        }
    }
    else {
        _9050 = binary_op(AND_BITS, _x_15754, 255);
    }
    if (IS_ATOM_INT(_x_15754)) {
        if (256 > 0 && _x_15754 >= 0) {
            _9051 = _x_15754 / 256;
        }
        else {
            temp_dbl = floor((double)_x_15754 / (double)256);
            if (_x_15754 != MININT)
            _9051 = (long)temp_dbl;
            else
            _9051 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15754, 256);
        _9051 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _9050;
    *((int *)(_2+12)) = _9051;
    _9052 = MAKE_SEQ(_1);
    _9051 = NOVALUE;
    _9050 = NOVALUE;
    DeRef(_x_15754);
    DeRefi(_x4_15755);
    DeRef(_s_15756);
    DeRef(_9041);
    _9041 = NOVALUE;
    DeRef(_9045);
    _9045 = NOVALUE;
    DeRef(_9046);
    _9046 = NOVALUE;
    return _9052;
    goto L3; // [94] 328
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_15754)) {
        _9053 = (_x_15754 >= _33MIN3B_15673);
    }
    else {
        _9053 = binary_op(GREATEREQ, _x_15754, _33MIN3B_15673);
    }
    if (IS_ATOM_INT(_9053)) {
        if (_9053 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_9053)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_15754)) {
        _9055 = (_x_15754 <= 8388607);
    }
    else {
        _9055 = binary_op(LESSEQ, _x_15754, 8388607);
    }
    if (_9055 == 0) {
        DeRef(_9055);
        _9055 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_9055) && DBL_PTR(_9055)->dbl == 0.0){
            DeRef(_9055);
            _9055 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_9055);
        _9055 = NOVALUE;
    }
    DeRef(_9055);
    _9055 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_15754;
    if (IS_ATOM_INT(_x_15754)) {
        _x_15754 = _x_15754 - _33MIN3B_15673;
        if ((long)((unsigned long)_x_15754 +(unsigned long) HIGH_BITS) >= 0){
            _x_15754 = NewDouble((double)_x_15754);
        }
    }
    else {
        _x_15754 = binary_op(MINUS, _x_15754, _33MIN3B_15673);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_15754)) {
        {unsigned long tu;
             tu = (unsigned long)_x_15754 & (unsigned long)255;
             _9057 = MAKE_UINT(tu);
        }
    }
    else {
        _9057 = binary_op(AND_BITS, _x_15754, 255);
    }
    if (IS_ATOM_INT(_x_15754)) {
        if (256 > 0 && _x_15754 >= 0) {
            _9058 = _x_15754 / 256;
        }
        else {
            temp_dbl = floor((double)_x_15754 / (double)256);
            if (_x_15754 != MININT)
            _9058 = (long)temp_dbl;
            else
            _9058 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15754, 256);
        _9058 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_9058)) {
        {unsigned long tu;
             tu = (unsigned long)_9058 & (unsigned long)255;
             _9059 = MAKE_UINT(tu);
        }
    }
    else {
        _9059 = binary_op(AND_BITS, _9058, 255);
    }
    DeRef(_9058);
    _9058 = NOVALUE;
    if (IS_ATOM_INT(_x_15754)) {
        if (65536 > 0 && _x_15754 >= 0) {
            _9060 = _x_15754 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_15754 / (double)65536);
            if (_x_15754 != MININT)
            _9060 = (long)temp_dbl;
            else
            _9060 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_15754, 65536);
        _9060 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _9057;
    *((int *)(_2+12)) = _9059;
    *((int *)(_2+16)) = _9060;
    _9061 = MAKE_SEQ(_1);
    _9060 = NOVALUE;
    _9059 = NOVALUE;
    _9057 = NOVALUE;
    DeRef(_x_15754);
    DeRefi(_x4_15755);
    DeRef(_s_15756);
    DeRef(_9041);
    _9041 = NOVALUE;
    DeRef(_9045);
    _9045 = NOVALUE;
    DeRef(_9046);
    _9046 = NOVALUE;
    DeRef(_9052);
    _9052 = NOVALUE;
    DeRef(_9053);
    _9053 = NOVALUE;
    return _9061;
    goto L3; // [156] 328
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_15754) && IS_ATOM_INT(_33MIN4B_15679)) {
        _9062 = _x_15754 - _33MIN4B_15679;
        if ((long)((unsigned long)_9062 +(unsigned long) HIGH_BITS) >= 0){
            _9062 = NewDouble((double)_9062);
        }
    }
    else {
        _9062 = binary_op(MINUS, _x_15754, _33MIN4B_15679);
    }
    _9063 = _17int_to_bytes(_9062);
    _9062 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_9063)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_9063)) {
        Prepend(&_9064, _9063, 251);
    }
    else {
        Concat((object_ptr)&_9064, 251, _9063);
    }
    DeRef(_9063);
    _9063 = NOVALUE;
    DeRef(_x_15754);
    DeRefi(_x4_15755);
    DeRef(_s_15756);
    DeRef(_9041);
    _9041 = NOVALUE;
    DeRef(_9045);
    _9045 = NOVALUE;
    DeRef(_9046);
    _9046 = NOVALUE;
    DeRef(_9052);
    _9052 = NOVALUE;
    DeRef(_9053);
    _9053 = NOVALUE;
    DeRef(_9061);
    _9061 = NOVALUE;
    return _9064;
    goto L3; // [180] 328
L1: 

    /** 	elsif atom(x) then*/
    _9065 = IS_ATOM(_x_15754);
    if (_9065 == 0)
    {
        _9065 = NOVALUE;
        goto L6; // [188] 249
    }
    else{
        _9065 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/

    /** 	return machine_func(M_A_TO_F32, a)*/
    DeRefi(_x4_15755);
    _x4_15755 = machine(48, _x_15754);

    /** 		if x = convert:float32_to_atom(x4) then*/

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    DeRef(_float32_to_atom_inlined_float32_to_atom_at_203_15793);
    _float32_to_atom_inlined_float32_to_atom_at_203_15793 = machine(49, _x4_15755);
    if (binary_op_a(NOTEQ, _x_15754, _float32_to_atom_inlined_float32_to_atom_at_203_15793)){
        goto L7; // [211] 228
    }

    /** 			return F4B & x4*/
    Prepend(&_9067, _x4_15755, 252);
    DeRef(_x_15754);
    DeRefDSi(_x4_15755);
    DeRef(_s_15756);
    DeRef(_9041);
    _9041 = NOVALUE;
    DeRef(_9045);
    _9045 = NOVALUE;
    DeRef(_9046);
    _9046 = NOVALUE;
    DeRef(_9052);
    _9052 = NOVALUE;
    DeRef(_9053);
    _9053 = NOVALUE;
    DeRef(_9061);
    _9061 = NOVALUE;
    DeRef(_9064);
    _9064 = NOVALUE;
    return _9067;
    goto L3; // [225] 328
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/

    /** 	return machine_func(M_A_TO_F64, a)*/
    DeRefi(_atom_to_float64_inlined_atom_to_float64_at_229_15798);
    _atom_to_float64_inlined_atom_to_float64_at_229_15798 = machine(46, _x_15754);
    Prepend(&_9068, _atom_to_float64_inlined_atom_to_float64_at_229_15798, 253);
    DeRef(_x_15754);
    DeRefi(_x4_15755);
    DeRef(_s_15756);
    DeRef(_9041);
    _9041 = NOVALUE;
    DeRef(_9045);
    _9045 = NOVALUE;
    DeRef(_9046);
    _9046 = NOVALUE;
    DeRef(_9052);
    _9052 = NOVALUE;
    DeRef(_9053);
    _9053 = NOVALUE;
    DeRef(_9061);
    _9061 = NOVALUE;
    DeRef(_9064);
    _9064 = NOVALUE;
    DeRef(_9067);
    _9067 = NOVALUE;
    return _9068;
    goto L3; // [246] 328
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_15754)){
            _9069 = SEQ_PTR(_x_15754)->length;
    }
    else {
        _9069 = 1;
    }
    if (_9069 > 255)
    goto L8; // [254] 270

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_15754)){
            _9071 = SEQ_PTR(_x_15754)->length;
    }
    else {
        _9071 = 1;
    }
    DeRef(_s_15756);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _9071;
    _s_15756 = MAKE_SEQ(_1);
    _9071 = NOVALUE;
    goto L9; // [267] 284
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_15754)){
            _9073 = SEQ_PTR(_x_15754)->length;
    }
    else {
        _9073 = 1;
    }
    _9074 = _17int_to_bytes(_9073);
    _9073 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_9074)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_9074)) {
        Prepend(&_s_15756, _9074, 255);
    }
    else {
        Concat((object_ptr)&_s_15756, 255, _9074);
    }
    DeRef(_9074);
    _9074 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_15754)){
            _9076 = SEQ_PTR(_x_15754)->length;
    }
    else {
        _9076 = 1;
    }
    {
        int _i_15811;
        _i_15811 = 1;
LA: 
        if (_i_15811 > _9076){
            goto LB; // [289] 319
        }

        /** 			s &= compress(x[i])*/
        _2 = (int)SEQ_PTR(_x_15754);
        _9077 = (int)*(((s1_ptr)_2)->base + _i_15811);
        Ref(_9077);
        _9078 = _33compress(_9077);
        _9077 = NOVALUE;
        if (IS_SEQUENCE(_s_15756) && IS_ATOM(_9078)) {
            Ref(_9078);
            Append(&_s_15756, _s_15756, _9078);
        }
        else if (IS_ATOM(_s_15756) && IS_SEQUENCE(_9078)) {
        }
        else {
            Concat((object_ptr)&_s_15756, _s_15756, _9078);
        }
        DeRef(_9078);
        _9078 = NOVALUE;

        /** 		end for*/
        _i_15811 = _i_15811 + 1;
        goto LA; // [314] 296
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_15754);
    DeRefi(_x4_15755);
    DeRef(_9041);
    _9041 = NOVALUE;
    DeRef(_9045);
    _9045 = NOVALUE;
    DeRef(_9046);
    _9046 = NOVALUE;
    DeRef(_9052);
    _9052 = NOVALUE;
    DeRef(_9053);
    _9053 = NOVALUE;
    DeRef(_9061);
    _9061 = NOVALUE;
    DeRef(_9064);
    _9064 = NOVALUE;
    DeRef(_9067);
    _9067 = NOVALUE;
    DeRef(_9068);
    _9068 = NOVALUE;
    return _s_15756;
L3: 
    ;
}


void _33safe_seek(int _pos_15830, int _msg_15831)
{
    int _eofpos_15832 = NOVALUE;
    int _seek_1__tmp_at34_15840 = NOVALUE;
    int _seek_inlined_seek_at_34_15839 = NOVALUE;
    int _where_inlined_where_at_51_15842 = NOVALUE;
    int _seek_1__tmp_at88_15850 = NOVALUE;
    int _seek_inlined_seek_at_88_15849 = NOVALUE;
    int _where_inlined_where_at_135_15858 = NOVALUE;
    int _9095 = NOVALUE;
    int _9091 = NOVALUE;
    int _9088 = NOVALUE;
    int _9085 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_33current_db_15557 != -1)
    goto L1; // [7] 31

    /** 		fatal(NO_DATABASE, "no current database defined", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_15830);
    *((int *)(_2+4)) = _pos_15830;
    _9085 = MAKE_SEQ(_1);
    RefDS(_9083);
    RefDS(_9084);
    _33fatal(901, _9083, _9084, _9085);
    _9085 = NOVALUE;

    /** 		return*/
    DeRef(_pos_15830);
    DeRef(_eofpos_15832);
    return;
L1: 

    /** 	io:seek(current_db, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at34_15840);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at34_15840 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_34_15839 = machine(19, _seek_1__tmp_at34_15840);
    DeRefi(_seek_1__tmp_at34_15840);
    _seek_1__tmp_at34_15840 = NOVALUE;

    /** 	eofpos = io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_eofpos_15832);
    _eofpos_15832 = machine(20, _33current_db_15557);

    /** 	if pos > eofpos then*/
    if (binary_op_a(LESSEQ, _pos_15830, _eofpos_15832)){
        goto L2; // [61] 85
    }

    /** 		fatal(BAD_SEEK, "io:seeking past EOF", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_15830);
    *((int *)(_2+4)) = _pos_15830;
    _9088 = MAKE_SEQ(_1);
    RefDS(_9087);
    RefDS(_9084);
    _33fatal(902, _9087, _9084, _9088);
    _9088 = NOVALUE;

    /** 		return*/
    DeRef(_pos_15830);
    DeRef(_eofpos_15832);
    return;
L2: 

    /** 	if io:seek(current_db, pos) != 0 then*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_15830);
    DeRef(_seek_1__tmp_at88_15850);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_15830;
    _seek_1__tmp_at88_15850 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_88_15849 = machine(19, _seek_1__tmp_at88_15850);
    DeRef(_seek_1__tmp_at88_15850);
    _seek_1__tmp_at88_15850 = NOVALUE;
    if (_seek_inlined_seek_at_88_15849 == 0)
    goto L3; // [102] 126

    /** 		fatal(BAD_SEEK, "io:seek to position failed", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_15830);
    *((int *)(_2+4)) = _pos_15830;
    _9091 = MAKE_SEQ(_1);
    RefDS(_9090);
    RefDS(_9084);
    _33fatal(902, _9090, _9084, _9091);
    _9091 = NOVALUE;

    /** 		return*/
    DeRef(_pos_15830);
    DeRef(_eofpos_15832);
    return;
L3: 

    /** 	if pos != -1 then*/
    if (binary_op_a(EQUALS, _pos_15830, -1)){
        goto L4; // [128] 168
    }

    /** 		if io:where(current_db) != pos then*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_135_15858);
    _where_inlined_where_at_135_15858 = machine(20, _33current_db_15557);
    if (binary_op_a(EQUALS, _where_inlined_where_at_135_15858, _pos_15830)){
        goto L5; // [143] 167
    }

    /** 			fatal(BAD_SEEK, "io:seek not in position", "safe_seek", {pos})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pos_15830);
    *((int *)(_2+4)) = _pos_15830;
    _9095 = MAKE_SEQ(_1);
    RefDS(_9094);
    RefDS(_9084);
    _33fatal(902, _9094, _9084, _9095);
    _9095 = NOVALUE;

    /** 			return*/
    DeRef(_pos_15830);
    DeRef(_eofpos_15832);
    return;
L5: 
L4: 

    /** end procedure*/
    DeRef(_pos_15830);
    DeRef(_eofpos_15832);
    return;
    ;
}


int _33db_get_errors(int _clearing_15864)
{
    int _lErrors_15865 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_clearing_15864)) {
        _1 = (long)(DBL_PTR(_clearing_15864)->dbl);
        if (UNIQUE(DBL_PTR(_clearing_15864)) && (DBL_PTR(_clearing_15864)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_clearing_15864);
        _clearing_15864 = _1;
    }

    /** 	lErrors = vLastErrors*/
    RefDS(_33vLastErrors_15581);
    DeRef(_lErrors_15865);
    _lErrors_15865 = _33vLastErrors_15581;

    /** 	if clearing then*/
    if (_clearing_15864 == 0)
    {
        goto L1; // [16] 27
    }
    else{
    }

    /** 		vLastErrors = {}*/
    RefDS(_5);
    DeRefDS(_33vLastErrors_15581);
    _33vLastErrors_15581 = _5;
L1: 

    /** 	return lErrors*/
    return _lErrors_15865;
    ;
}


void _33db_dump(int _file_id_15869, int _low_level_too_15870)
{
    int _magic_15871 = NOVALUE;
    int _minor_15872 = NOVALUE;
    int _major_15873 = NOVALUE;
    int _fn_15874 = NOVALUE;
    int _tables_15875 = NOVALUE;
    int _ntables_15876 = NOVALUE;
    int _tname_15877 = NOVALUE;
    int _trecords_15878 = NOVALUE;
    int _t_header_15879 = NOVALUE;
    int _tnrecs_15880 = NOVALUE;
    int _key_ptr_15881 = NOVALUE;
    int _data_ptr_15882 = NOVALUE;
    int _size_15883 = NOVALUE;
    int _addr_15884 = NOVALUE;
    int _tindex_15885 = NOVALUE;
    int _fbp_15886 = NOVALUE;
    int _key_15887 = NOVALUE;
    int _data_15888 = NOVALUE;
    int _c_15889 = NOVALUE;
    int _n_15890 = NOVALUE;
    int _tblocks_15891 = NOVALUE;
    int _a_15892 = NOVALUE;
    int _ll_line_15893 = NOVALUE;
    int _hi_15894 = NOVALUE;
    int _ci_15895 = NOVALUE;
    int _now_1__tmp_at87_15910 = NOVALUE;
    int _now_inlined_now_at_87_15909 = NOVALUE;
    int _seek_1__tmp_at115_15915 = NOVALUE;
    int _seek_inlined_seek_at_115_15914 = NOVALUE;
    int _get1_inlined_get1_at_145_15920 = NOVALUE;
    int _get1_inlined_get1_at_173_15926 = NOVALUE;
    int _get1_inlined_get1_at_187_15928 = NOVALUE;
    int _seek_1__tmp_at218_15934 = NOVALUE;
    int _seek_inlined_seek_at_218_15933 = NOVALUE;
    int _seek_1__tmp_at304_15950 = NOVALUE;
    int _seek_inlined_seek_at_304_15949 = NOVALUE;
    int _seek_1__tmp_at592_15989 = NOVALUE;
    int _seek_inlined_seek_at_592_15988 = NOVALUE;
    int _get1_inlined_get1_at_607_15991 = NOVALUE;
    int _get1_inlined_get1_at_646_15997 = NOVALUE;
    int _get1_inlined_get1_at_660_15999 = NOVALUE;
    int _seek_1__tmp_at691_16005 = NOVALUE;
    int _seek_inlined_seek_at_691_16004 = NOVALUE;
    int _where_inlined_where_at_713_16008 = NOVALUE;
    int _seek_1__tmp_at780_16022 = NOVALUE;
    int _seek_inlined_seek_at_780_16021 = NOVALUE;
    int _seek_1__tmp_at871_16043 = NOVALUE;
    int _seek_inlined_seek_at_871_16042 = NOVALUE;
    int _pos_inlined_seek_at_868_16041 = NOVALUE;
    int _seek_1__tmp_at967_16065 = NOVALUE;
    int _seek_inlined_seek_at_967_16064 = NOVALUE;
    int _pos_inlined_seek_at_964_16063 = NOVALUE;
    int _seek_1__tmp_at1003_16072 = NOVALUE;
    int _seek_inlined_seek_at_1003_16071 = NOVALUE;
    int _seek_1__tmp_at1062_16082 = NOVALUE;
    int _seek_inlined_seek_at_1062_16081 = NOVALUE;
    int _seek_1__tmp_at1133_16091 = NOVALUE;
    int _seek_inlined_seek_at_1133_16090 = NOVALUE;
    int _seek_1__tmp_at1167_16096 = NOVALUE;
    int _seek_inlined_seek_at_1167_16095 = NOVALUE;
    int _seek_1__tmp_at1230_16106 = NOVALUE;
    int _seek_inlined_seek_at_1230_16105 = NOVALUE;
    int _9215 = NOVALUE;
    int _9213 = NOVALUE;
    int _9209 = NOVALUE;
    int _9197 = NOVALUE;
    int _9191 = NOVALUE;
    int _9188 = NOVALUE;
    int _9187 = NOVALUE;
    int _9186 = NOVALUE;
    int _9185 = NOVALUE;
    int _9184 = NOVALUE;
    int _9183 = NOVALUE;
    int _9182 = NOVALUE;
    int _9180 = NOVALUE;
    int _9179 = NOVALUE;
    int _9174 = NOVALUE;
    int _9173 = NOVALUE;
    int _9172 = NOVALUE;
    int _9171 = NOVALUE;
    int _9170 = NOVALUE;
    int _9169 = NOVALUE;
    int _9168 = NOVALUE;
    int _9166 = NOVALUE;
    int _9164 = NOVALUE;
    int _9163 = NOVALUE;
    int _9155 = NOVALUE;
    int _9151 = NOVALUE;
    int _9149 = NOVALUE;
    int _9147 = NOVALUE;
    int _9146 = NOVALUE;
    int _9142 = NOVALUE;
    int _9141 = NOVALUE;
    int _9140 = NOVALUE;
    int _9137 = NOVALUE;
    int _9134 = NOVALUE;
    int _9132 = NOVALUE;
    int _9131 = NOVALUE;
    int _9128 = NOVALUE;
    int _9125 = NOVALUE;
    int _9122 = NOVALUE;
    int _9121 = NOVALUE;
    int _9117 = NOVALUE;
    int _9116 = NOVALUE;
    int _9115 = NOVALUE;
    int _9111 = NOVALUE;
    int _9106 = NOVALUE;
    int _9105 = NOVALUE;
    int _9104 = NOVALUE;
    int _9102 = NOVALUE;
    int _9096 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_low_level_too_15870)) {
        _1 = (long)(DBL_PTR(_low_level_too_15870)->dbl);
        if (UNIQUE(DBL_PTR(_low_level_too_15870)) && (DBL_PTR(_low_level_too_15870)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_low_level_too_15870);
        _low_level_too_15870 = _1;
    }

    /** 	if sequence(file_id) then*/
    _9096 = IS_SEQUENCE(_file_id_15869);
    if (_9096 == 0)
    {
        _9096 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _9096 = NOVALUE;
    }

    /** 		fn = open(file_id, "w")*/
    _fn_15874 = EOpen(_file_id_15869, _4110, 0);
    goto L2; // [22] 58
L1: 

    /** 	elsif file_id > 0 then*/
    if (binary_op_a(LESSEQ, _file_id_15869, 0)){
        goto L3; // [27] 48
    }

    /** 		fn = file_id*/
    Ref(_file_id_15869);
    _fn_15874 = _file_id_15869;
    if (!IS_ATOM_INT(_fn_15874)) {
        _1 = (long)(DBL_PTR(_fn_15874)->dbl);
        if (UNIQUE(DBL_PTR(_fn_15874)) && (DBL_PTR(_fn_15874)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_15874);
        _fn_15874 = _1;
    }

    /** 		puts(fn, '\n')*/
    EPuts(_fn_15874, 10); // DJP 
    goto L2; // [45] 58
L3: 

    /** 		fn = file_id*/
    Ref(_file_id_15869);
    _fn_15874 = _file_id_15869;
    if (!IS_ATOM_INT(_fn_15874)) {
        _1 = (long)(DBL_PTR(_fn_15874)->dbl);
        if (UNIQUE(DBL_PTR(_fn_15874)) && (DBL_PTR(_fn_15874)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_15874);
        _fn_15874 = _1;
    }
L2: 

    /** 	if fn <= 0 then*/
    if (_fn_15874 > 0)
    goto L4; // [62] 86

    /** 		fatal( BAD_FILE, "bad file", "db_dump", {file_id, low_level_too})*/
    Ref(_file_id_15869);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_id_15869;
    ((int *)_2)[2] = _low_level_too_15870;
    _9102 = MAKE_SEQ(_1);
    RefDS(_9100);
    RefDS(_9101);
    _33fatal(908, _9100, _9101, _9102);
    _9102 = NOVALUE;

    /** 		return*/
    DeRef(_file_id_15869);
    DeRef(_tables_15875);
    DeRef(_ntables_15876);
    DeRef(_tname_15877);
    DeRef(_trecords_15878);
    DeRef(_t_header_15879);
    DeRef(_tnrecs_15880);
    DeRef(_key_ptr_15881);
    DeRef(_data_ptr_15882);
    DeRef(_size_15883);
    DeRef(_addr_15884);
    DeRef(_tindex_15885);
    DeRef(_fbp_15886);
    DeRef(_key_15887);
    DeRef(_data_15888);
    DeRef(_a_15892);
    DeRef(_ll_line_15893);
    return;
L4: 

    /** 	printf(fn, "Database dump as at %s\n", {datetime:format( datetime:now(), "%Y-%m-%d %H:%M:%S")})*/

    /** 	return from_date(date())*/
    DeRefi(_now_1__tmp_at87_15910);
    _now_1__tmp_at87_15910 = Date();
    RefDS(_now_1__tmp_at87_15910);
    _0 = _now_inlined_now_at_87_15909;
    _now_inlined_now_at_87_15909 = _9from_date(_now_1__tmp_at87_15910);
    DeRef(_0);
    DeRefi(_now_1__tmp_at87_15910);
    _now_1__tmp_at87_15910 = NOVALUE;
    Ref(_now_inlined_now_at_87_15909);
    RefDS(_4835);
    _9104 = _9format(_now_inlined_now_at_87_15909, _4835);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9104;
    _9105 = MAKE_SEQ(_1);
    _9104 = NOVALUE;
    EPrintf(_fn_15874, _9103, _9105);
    DeRefDS(_9105);
    _9105 = NOVALUE;

    /** 	io:seek(current_db, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at115_15915);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at115_15915 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_115_15914 = machine(19, _seek_1__tmp_at115_15915);
    DeRefi(_seek_1__tmp_at115_15915);
    _seek_1__tmp_at115_15915 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return end if*/
    if (IS_SEQUENCE(_33vLastErrors_15581)){
            _9106 = SEQ_PTR(_33vLastErrors_15581)->length;
    }
    else {
        _9106 = 1;
    }
    if (_9106 <= 0)
    goto L5; // [136] 144
    DeRef(_file_id_15869);
    DeRef(_tables_15875);
    DeRef(_ntables_15876);
    DeRef(_tname_15877);
    DeRef(_trecords_15878);
    DeRef(_t_header_15879);
    DeRef(_tnrecs_15880);
    DeRef(_key_ptr_15881);
    DeRef(_data_ptr_15882);
    DeRef(_size_15883);
    DeRef(_addr_15884);
    DeRef(_tindex_15885);
    DeRef(_fbp_15886);
    DeRef(_key_15887);
    DeRef(_data_15888);
    DeRef(_a_15892);
    DeRef(_ll_line_15893);
    return;
L5: 

    /** 	magic = get1()*/

    /** 	return getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_15871 = getKBchar();
        }
        else
        _magic_15871 = getc(last_r_file_ptr);
    }
    else
    _magic_15871 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_magic_15871)) {
        _1 = (long)(DBL_PTR(_magic_15871)->dbl);
        if (UNIQUE(DBL_PTR(_magic_15871)) && (DBL_PTR(_magic_15871)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_magic_15871);
        _magic_15871 = _1;
    }

    /** 	if magic != DB_MAGIC then*/
    if (_magic_15871 == 77)
    goto L6; // [160] 172

    /** 		puts(fn, "This is not a Euphoria Database file.\n")*/
    EPuts(_fn_15874, _9109); // DJP 
    goto L7; // [169] 283
L6: 

    /** 		major = get1()*/

    /** 	return getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _major_15873 = getKBchar();
        }
        else
        _major_15873 = getc(last_r_file_ptr);
    }
    else
    _major_15873 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_major_15873)) {
        _1 = (long)(DBL_PTR(_major_15873)->dbl);
        if (UNIQUE(DBL_PTR(_major_15873)) && (DBL_PTR(_major_15873)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_major_15873);
        _major_15873 = _1;
    }

    /** 		minor = get1()*/

    /** 	return getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _minor_15872 = getKBchar();
        }
        else
        _minor_15872 = getc(last_r_file_ptr);
    }
    else
    _minor_15872 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_minor_15872)) {
        _1 = (long)(DBL_PTR(_minor_15872)->dbl);
        if (UNIQUE(DBL_PTR(_minor_15872)) && (DBL_PTR(_minor_15872)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minor_15872);
        _minor_15872 = _1;
    }

    /** 		printf(fn, "Euphoria Database System Version %d.%d\n\n", {major, minor})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _major_15873;
    ((int *)_2)[2] = _minor_15872;
    _9111 = MAKE_SEQ(_1);
    EPrintf(_fn_15874, _9110, _9111);
    DeRefDS(_9111);
    _9111 = NOVALUE;

    /** 		tables = get4()*/
    _0 = _tables_15875;
    _tables_15875 = _33get4();
    DeRef(_0);

    /** 		io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_15875);
    DeRef(_seek_1__tmp_at218_15934);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _tables_15875;
    _seek_1__tmp_at218_15934 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_218_15933 = machine(19, _seek_1__tmp_at218_15934);
    DeRef(_seek_1__tmp_at218_15934);
    _seek_1__tmp_at218_15934 = NOVALUE;

    /** 		ntables = get4()*/
    _0 = _ntables_15876;
    _ntables_15876 = _33get4();
    DeRef(_0);

    /** 		printf(fn, "The \"%s\" database has %d table",*/
    _9115 = find_from(_33current_db_15557, _33db_file_nums_15561, 1);
    _2 = (int)SEQ_PTR(_33db_names_15560);
    _9116 = (int)*(((s1_ptr)_2)->base + _9115);
    Ref(_ntables_15876);
    Ref(_9116);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _9116;
    ((int *)_2)[2] = _ntables_15876;
    _9117 = MAKE_SEQ(_1);
    _9116 = NOVALUE;
    EPrintf(_fn_15874, _9114, _9117);
    DeRefDS(_9117);
    _9117 = NOVALUE;

    /** 		if ntables = 1 then*/
    if (binary_op_a(NOTEQ, _ntables_15876, 1)){
        goto L8; // [264] 276
    }

    /** 			puts(fn, "\n")*/
    EPuts(_fn_15874, _3957); // DJP 
    goto L9; // [273] 282
L8: 

    /** 			puts(fn, "s\n")*/
    EPuts(_fn_15874, _9119); // DJP 
L9: 
L7: 

    /** 	if low_level_too then*/
    if (_low_level_too_15870 == 0)
    {
        goto LA; // [285] 589
    }
    else{
    }

    /** 		puts(fn, "            Disk Dump\nDiskAddr " & repeat('-', 58))*/
    _9121 = Repeat(45, 58);
    Concat((object_ptr)&_9122, _9120, _9121);
    DeRefDS(_9121);
    _9121 = NOVALUE;
    EPuts(_fn_15874, _9122); // DJP 
    DeRefDS(_9122);
    _9122 = NOVALUE;

    /** 		io:seek(current_db, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at304_15950);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at304_15950 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_304_15949 = machine(19, _seek_1__tmp_at304_15950);
    DeRefi(_seek_1__tmp_at304_15950);
    _seek_1__tmp_at304_15950 = NOVALUE;

    /** 		a = 0*/
    DeRef(_a_15892);
    _a_15892 = 0;

    /** 		while c >= 0 with entry do*/
    goto LB; // [325] 549
LC: 
    if (_c_15889 < 0)
    goto LD; // [330] 563

    /** 			if c = -1 then*/
    if (_c_15889 != -1)
    goto LE; // [336] 345

    /** 				exit*/
    goto LD; // [342] 563
LE: 

    /** 			if remainder(a, 16) = 0 then*/
    if (IS_ATOM_INT(_a_15892)) {
        _9125 = (_a_15892 % 16);
    }
    else {
        temp_d.dbl = (double)16;
        _9125 = Dremainder(DBL_PTR(_a_15892), &temp_d);
    }
    if (binary_op_a(NOTEQ, _9125, 0)){
        DeRef(_9125);
        _9125 = NOVALUE;
        goto LF; // [351] 432
    }
    DeRef(_9125);
    _9125 = NOVALUE;

    /** 				if a > 0 then*/
    if (binary_op_a(LESSEQ, _a_15892, 0)){
        goto L10; // [357] 376
    }

    /** 					printf(fn, "%s\n", {ll_line})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ll_line_15893);
    *((int *)(_2+4)) = _ll_line_15893;
    _9128 = MAKE_SEQ(_1);
    EPrintf(_fn_15874, _8888, _9128);
    DeRefDS(_9128);
    _9128 = NOVALUE;
    goto L11; // [373] 382
L10: 

    /** 					puts(fn, '\n')*/
    EPuts(_fn_15874, 10); // DJP 
L11: 

    /** 				ll_line = repeat(' ', 67)*/
    DeRef(_ll_line_15893);
    _ll_line_15893 = Repeat(32, 67);

    /** 				ll_line[9] = ':'*/
    _2 = (int)SEQ_PTR(_ll_line_15893);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_15893 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 9);
    *(int *)_2 = 58;

    /** 				ll_line[48] = '|'*/
    _2 = (int)SEQ_PTR(_ll_line_15893);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_15893 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 48);
    *(int *)_2 = 124;

    /** 				ll_line[67] = '|'*/
    _2 = (int)SEQ_PTR(_ll_line_15893);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_15893 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 67);
    *(int *)_2 = 124;

    /** 				hi = 11*/
    _hi_15894 = 11;

    /** 				ci = 50*/
    _ci_15895 = 50;

    /** 				ll_line[1..8] = sprintf("%08x", a)*/
    _9131 = EPrintf(-9999999, _9130, _a_15892);
    assign_slice_seq = (s1_ptr *)&_ll_line_15893;
    AssignSlice(1, 8, _9131);
    DeRefDS(_9131);
    _9131 = NOVALUE;
LF: 

    /** 			ll_line[hi .. hi + 1] = sprintf("%02x", c)*/
    _9132 = _hi_15894 + 1;
    if (_9132 > MAXINT){
        _9132 = NewDouble((double)_9132);
    }
    _9134 = EPrintf(-9999999, _9133, _c_15889);
    assign_slice_seq = (s1_ptr *)&_ll_line_15893;
    AssignSlice(_hi_15894, _9132, _9134);
    DeRef(_9132);
    _9132 = NOVALUE;
    DeRefDS(_9134);
    _9134 = NOVALUE;

    /** 			hi += 2*/
    _hi_15894 = _hi_15894 + 2;

    /** 			if eu:find(hi, {19, 28, 38}) then*/
    _9137 = find_from(_hi_15894, _9136, 1);
    if (_9137 == 0)
    {
        _9137 = NOVALUE;
        goto L12; // [466] 492
    }
    else{
        _9137 = NOVALUE;
    }

    /** 				hi += 1*/
    _hi_15894 = _hi_15894 + 1;

    /** 				if hi = 29 then*/
    if (_hi_15894 != 29)
    goto L13; // [479] 491

    /** 					hi = 30*/
    _hi_15894 = 30;
L13: 
L12: 

    /** 			if c > ' ' and c < '~' then*/
    _9140 = (_c_15889 > 32);
    if (_9140 == 0) {
        goto L14; // [498] 521
    }
    _9142 = (_c_15889 < 126);
    if (_9142 == 0)
    {
        DeRef(_9142);
        _9142 = NOVALUE;
        goto L14; // [507] 521
    }
    else{
        DeRef(_9142);
        _9142 = NOVALUE;
    }

    /** 				ll_line[ci] = c*/
    _2 = (int)SEQ_PTR(_ll_line_15893);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_15893 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ci_15895);
    _1 = *(int *)_2;
    *(int *)_2 = _c_15889;
    DeRef(_1);
    goto L15; // [518] 530
L14: 

    /** 				ll_line[ci] = '.'*/
    _2 = (int)SEQ_PTR(_ll_line_15893);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _ll_line_15893 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ci_15895);
    _1 = *(int *)_2;
    *(int *)_2 = 46;
    DeRef(_1);
L15: 

    /** 			ci += 1*/
    _ci_15895 = _ci_15895 + 1;

    /** 			a += 1*/
    _0 = _a_15892;
    if (IS_ATOM_INT(_a_15892)) {
        _a_15892 = _a_15892 + 1;
        if (_a_15892 > MAXINT){
            _a_15892 = NewDouble((double)_a_15892);
        }
    }
    else
    _a_15892 = binary_op(PLUS, 1, _a_15892);
    DeRef(_0);

    /** 		  entry*/
LB: 

    /** 			c = getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_15889 = getKBchar();
        }
        else
        _c_15889 = getc(last_r_file_ptr);
    }
    else
    _c_15889 = getc(last_r_file_ptr);

    /** 		end while*/
    goto LC; // [560] 328
LD: 

    /** 		printf(fn, "%s\n", {ll_line})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ll_line_15893);
    *((int *)(_2+4)) = _ll_line_15893;
    _9146 = MAKE_SEQ(_1);
    EPrintf(_fn_15874, _8888, _9146);
    DeRefDS(_9146);
    _9146 = NOVALUE;

    /** 		puts(fn, repeat('-', 67) & "\n\n")*/
    _9147 = Repeat(45, 67);
    Concat((object_ptr)&_9149, _9147, _9148);
    DeRefDS(_9147);
    _9147 = NOVALUE;
    DeRef(_9147);
    _9147 = NOVALUE;
    EPuts(_fn_15874, _9149); // DJP 
    DeRefDS(_9149);
    _9149 = NOVALUE;
LA: 

    /** 	io:seek(current_db, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at592_15989);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at592_15989 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_592_15988 = machine(19, _seek_1__tmp_at592_15989);
    DeRefi(_seek_1__tmp_at592_15989);
    _seek_1__tmp_at592_15989 = NOVALUE;

    /** 	magic = get1()*/

    /** 	return getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_15871 = getKBchar();
        }
        else
        _magic_15871 = getc(last_r_file_ptr);
    }
    else
    _magic_15871 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_magic_15871)) {
        _1 = (long)(DBL_PTR(_magic_15871)->dbl);
        if (UNIQUE(DBL_PTR(_magic_15871)) && (DBL_PTR(_magic_15871)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_magic_15871);
        _magic_15871 = _1;
    }

    /** 	if magic != DB_MAGIC then*/
    if (_magic_15871 == 77)
    goto L16; // [622] 645

    /** 		if sequence(file_id) then*/
    _9151 = IS_SEQUENCE(_file_id_15869);
    if (_9151 == 0)
    {
        _9151 = NOVALUE;
        goto L17; // [631] 639
    }
    else{
        _9151 = NOVALUE;
    }

    /** 			close(fn)*/
    EClose(_fn_15874);
L17: 

    /** 		return*/
    DeRef(_file_id_15869);
    DeRef(_tables_15875);
    DeRef(_ntables_15876);
    DeRef(_tname_15877);
    DeRef(_trecords_15878);
    DeRef(_t_header_15879);
    DeRef(_tnrecs_15880);
    DeRef(_key_ptr_15881);
    DeRef(_data_ptr_15882);
    DeRef(_size_15883);
    DeRef(_addr_15884);
    DeRef(_tindex_15885);
    DeRef(_fbp_15886);
    DeRef(_key_15887);
    DeRef(_data_15888);
    DeRef(_a_15892);
    DeRef(_ll_line_15893);
    DeRef(_9140);
    _9140 = NOVALUE;
    return;
L16: 

    /** 	major = get1()*/

    /** 	return getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _major_15873 = getKBchar();
        }
        else
        _major_15873 = getc(last_r_file_ptr);
    }
    else
    _major_15873 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_major_15873)) {
        _1 = (long)(DBL_PTR(_major_15873)->dbl);
        if (UNIQUE(DBL_PTR(_major_15873)) && (DBL_PTR(_major_15873)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_major_15873);
        _major_15873 = _1;
    }

    /** 	minor = get1()*/

    /** 	return getc(current_db)*/
    if (_33current_db_15557 != last_r_file_no) {
        last_r_file_ptr = which_file(_33current_db_15557, EF_READ);
        last_r_file_no = _33current_db_15557;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _minor_15872 = getKBchar();
        }
        else
        _minor_15872 = getc(last_r_file_ptr);
    }
    else
    _minor_15872 = getc(last_r_file_ptr);
    if (!IS_ATOM_INT(_minor_15872)) {
        _1 = (long)(DBL_PTR(_minor_15872)->dbl);
        if (UNIQUE(DBL_PTR(_minor_15872)) && (DBL_PTR(_minor_15872)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minor_15872);
        _minor_15872 = _1;
    }

    /** 	tables = get4()*/
    _0 = _tables_15875;
    _tables_15875 = _33get4();
    DeRef(_0);

    /** 	if low_level_too then printf(fn, "[tables:#%08x]\n", tables) end if*/
    if (_low_level_too_15870 == 0)
    {
        goto L18; // [680] 688
    }
    else{
    }
    EPrintf(_fn_15874, _9153, _tables_15875);
L18: 

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_15875);
    DeRef(_seek_1__tmp_at691_16005);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _tables_15875;
    _seek_1__tmp_at691_16005 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_691_16004 = machine(19, _seek_1__tmp_at691_16005);
    DeRef(_seek_1__tmp_at691_16005);
    _seek_1__tmp_at691_16005 = NOVALUE;

    /** 	ntables = get4()*/
    _0 = _ntables_15876;
    _ntables_15876 = _33get4();
    DeRef(_0);

    /** 	t_header = io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_t_header_15879);
    _t_header_15879 = machine(20, _33current_db_15557);

    /** 	for t = 1 to ntables do*/
    Ref(_ntables_15876);
    DeRef(_9155);
    _9155 = _ntables_15876;
    {
        int _t_16010;
        _t_16010 = 1;
L19: 
        if (binary_op_a(GREATER, _t_16010, _9155)){
            goto L1A; // [726] 1154
        }

        /** 		if low_level_too then printf(fn, "\n---------------\n[table header:#%08x]\n", t_header) end if*/
        if (_low_level_too_15870 == 0)
        {
            goto L1B; // [735] 743
        }
        else{
        }
        EPrintf(_fn_15874, _9156, _t_header_15879);
L1B: 

        /** 		tname = get4()*/
        _0 = _tname_15877;
        _tname_15877 = _33get4();
        DeRef(_0);

        /** 		tnrecs = get4()*/
        _0 = _tnrecs_15880;
        _tnrecs_15880 = _33get4();
        DeRef(_0);

        /** 		tblocks = get4()*/
        _tblocks_15891 = _33get4();
        if (!IS_ATOM_INT(_tblocks_15891)) {
            _1 = (long)(DBL_PTR(_tblocks_15891)->dbl);
            if (UNIQUE(DBL_PTR(_tblocks_15891)) && (DBL_PTR(_tblocks_15891)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tblocks_15891);
            _tblocks_15891 = _1;
        }

        /** 		tindex = get4()*/
        _0 = _tindex_15885;
        _tindex_15885 = _33get4();
        DeRef(_0);

        /** 		if low_level_too then printf(fn, "[table name:#%08x]\n", tname) end if*/
        if (_low_level_too_15870 == 0)
        {
            goto L1C; // [769] 777
        }
        else{
        }
        EPrintf(_fn_15874, _9161, _tname_15877);
L1C: 

        /** 		io:seek(current_db, tname)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_tname_15877);
        DeRef(_seek_1__tmp_at780_16022);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _tname_15877;
        _seek_1__tmp_at780_16022 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_780_16021 = machine(19, _seek_1__tmp_at780_16022);
        DeRef(_seek_1__tmp_at780_16022);
        _seek_1__tmp_at780_16022 = NOVALUE;

        /** 		printf(fn, "\ntable \"%s\", records:%d    indexblks: %d\n\n\n", {get_string(), tnrecs, tblocks})*/
        _9163 = _33get_string();
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _9163;
        Ref(_tnrecs_15880);
        *((int *)(_2+8)) = _tnrecs_15880;
        *((int *)(_2+12)) = _tblocks_15891;
        _9164 = MAKE_SEQ(_1);
        _9163 = NOVALUE;
        EPrintf(_fn_15874, _9162, _9164);
        DeRefDS(_9164);
        _9164 = NOVALUE;

        /** 		if tnrecs > 0 then*/
        if (binary_op_a(LESSEQ, _tnrecs_15880, 0)){
            goto L1D; // [811] 1124
        }

        /** 			for b = 1 to tblocks do*/
        _9166 = _tblocks_15891;
        {
            int _b_16029;
            _b_16029 = 1;
L1E: 
            if (_b_16029 > _9166){
                goto L1F; // [820] 1123
            }

            /** 				if low_level_too then printf(fn, "[table block %d:#%08x]\n", {b, tindex+(b-1)*8}) end if*/
            if (_low_level_too_15870 == 0)
            {
                goto L20; // [829] 853
            }
            else{
            }
            _9168 = _b_16029 - 1;
            if (_9168 == (short)_9168)
            _9169 = _9168 * 8;
            else
            _9169 = NewDouble(_9168 * (double)8);
            _9168 = NOVALUE;
            if (IS_ATOM_INT(_tindex_15885) && IS_ATOM_INT(_9169)) {
                _9170 = _tindex_15885 + _9169;
                if ((long)((unsigned long)_9170 + (unsigned long)HIGH_BITS) >= 0) 
                _9170 = NewDouble((double)_9170);
            }
            else {
                if (IS_ATOM_INT(_tindex_15885)) {
                    _9170 = NewDouble((double)_tindex_15885 + DBL_PTR(_9169)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_9169)) {
                        _9170 = NewDouble(DBL_PTR(_tindex_15885)->dbl + (double)_9169);
                    }
                    else
                    _9170 = NewDouble(DBL_PTR(_tindex_15885)->dbl + DBL_PTR(_9169)->dbl);
                }
            }
            DeRef(_9169);
            _9169 = NOVALUE;
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _b_16029;
            ((int *)_2)[2] = _9170;
            _9171 = MAKE_SEQ(_1);
            _9170 = NOVALUE;
            EPrintf(_fn_15874, _9167, _9171);
            DeRefDS(_9171);
            _9171 = NOVALUE;
L20: 

            /** 				io:seek(current_db, tindex+(b-1)*8)*/
            _9172 = _b_16029 - 1;
            if (_9172 == (short)_9172)
            _9173 = _9172 * 8;
            else
            _9173 = NewDouble(_9172 * (double)8);
            _9172 = NOVALUE;
            if (IS_ATOM_INT(_tindex_15885) && IS_ATOM_INT(_9173)) {
                _9174 = _tindex_15885 + _9173;
                if ((long)((unsigned long)_9174 + (unsigned long)HIGH_BITS) >= 0) 
                _9174 = NewDouble((double)_9174);
            }
            else {
                if (IS_ATOM_INT(_tindex_15885)) {
                    _9174 = NewDouble((double)_tindex_15885 + DBL_PTR(_9173)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_9173)) {
                        _9174 = NewDouble(DBL_PTR(_tindex_15885)->dbl + (double)_9173);
                    }
                    else
                    _9174 = NewDouble(DBL_PTR(_tindex_15885)->dbl + DBL_PTR(_9173)->dbl);
                }
            }
            DeRef(_9173);
            _9173 = NOVALUE;
            DeRef(_pos_inlined_seek_at_868_16041);
            _pos_inlined_seek_at_868_16041 = _9174;
            _9174 = NOVALUE;

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_pos_inlined_seek_at_868_16041);
            DeRef(_seek_1__tmp_at871_16043);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _33current_db_15557;
            ((int *)_2)[2] = _pos_inlined_seek_at_868_16041;
            _seek_1__tmp_at871_16043 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_871_16042 = machine(19, _seek_1__tmp_at871_16043);
            DeRef(_pos_inlined_seek_at_868_16041);
            _pos_inlined_seek_at_868_16041 = NOVALUE;
            DeRef(_seek_1__tmp_at871_16043);
            _seek_1__tmp_at871_16043 = NOVALUE;

            /** 				tnrecs = get4()*/
            _0 = _tnrecs_15880;
            _tnrecs_15880 = _33get4();
            DeRef(_0);

            /** 				trecords = get4()*/
            _0 = _trecords_15878;
            _trecords_15878 = _33get4();
            DeRef(_0);

            /** 				if tnrecs > 0 then*/
            if (binary_op_a(LESSEQ, _tnrecs_15880, 0)){
                goto L21; // [897] 1109
            }

            /** 					printf(fn, "\n--------------------------\nblock #%d, ptrs:%d\n--------------------------\n", {b, tnrecs})*/
            Ref(_tnrecs_15880);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _b_16029;
            ((int *)_2)[2] = _tnrecs_15880;
            _9179 = MAKE_SEQ(_1);
            EPrintf(_fn_15874, _9178, _9179);
            DeRefDS(_9179);
            _9179 = NOVALUE;

            /** 					for r = 1 to tnrecs do*/
            Ref(_tnrecs_15880);
            DeRef(_9180);
            _9180 = _tnrecs_15880;
            {
                int _r_16051;
                _r_16051 = 1;
L22: 
                if (binary_op_a(GREATER, _r_16051, _9180)){
                    goto L23; // [916] 1106
                }

                /** 						if low_level_too then printf(fn, "[record %d:#%08x]\n", {r, trecords+(r-1)*4}) end if*/
                if (_low_level_too_15870 == 0)
                {
                    goto L24; // [925] 949
                }
                else{
                }
                if (IS_ATOM_INT(_r_16051)) {
                    _9182 = _r_16051 - 1;
                    if ((long)((unsigned long)_9182 +(unsigned long) HIGH_BITS) >= 0){
                        _9182 = NewDouble((double)_9182);
                    }
                }
                else {
                    _9182 = NewDouble(DBL_PTR(_r_16051)->dbl - (double)1);
                }
                if (IS_ATOM_INT(_9182)) {
                    if (_9182 == (short)_9182)
                    _9183 = _9182 * 4;
                    else
                    _9183 = NewDouble(_9182 * (double)4);
                }
                else {
                    _9183 = NewDouble(DBL_PTR(_9182)->dbl * (double)4);
                }
                DeRef(_9182);
                _9182 = NOVALUE;
                if (IS_ATOM_INT(_trecords_15878) && IS_ATOM_INT(_9183)) {
                    _9184 = _trecords_15878 + _9183;
                    if ((long)((unsigned long)_9184 + (unsigned long)HIGH_BITS) >= 0) 
                    _9184 = NewDouble((double)_9184);
                }
                else {
                    if (IS_ATOM_INT(_trecords_15878)) {
                        _9184 = NewDouble((double)_trecords_15878 + DBL_PTR(_9183)->dbl);
                    }
                    else {
                        if (IS_ATOM_INT(_9183)) {
                            _9184 = NewDouble(DBL_PTR(_trecords_15878)->dbl + (double)_9183);
                        }
                        else
                        _9184 = NewDouble(DBL_PTR(_trecords_15878)->dbl + DBL_PTR(_9183)->dbl);
                    }
                }
                DeRef(_9183);
                _9183 = NOVALUE;
                Ref(_r_16051);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _r_16051;
                ((int *)_2)[2] = _9184;
                _9185 = MAKE_SEQ(_1);
                _9184 = NOVALUE;
                EPrintf(_fn_15874, _9181, _9185);
                DeRefDS(_9185);
                _9185 = NOVALUE;
L24: 

                /** 						io:seek(current_db, trecords+(r-1)*4)*/
                if (IS_ATOM_INT(_r_16051)) {
                    _9186 = _r_16051 - 1;
                    if ((long)((unsigned long)_9186 +(unsigned long) HIGH_BITS) >= 0){
                        _9186 = NewDouble((double)_9186);
                    }
                }
                else {
                    _9186 = NewDouble(DBL_PTR(_r_16051)->dbl - (double)1);
                }
                if (IS_ATOM_INT(_9186)) {
                    if (_9186 == (short)_9186)
                    _9187 = _9186 * 4;
                    else
                    _9187 = NewDouble(_9186 * (double)4);
                }
                else {
                    _9187 = NewDouble(DBL_PTR(_9186)->dbl * (double)4);
                }
                DeRef(_9186);
                _9186 = NOVALUE;
                if (IS_ATOM_INT(_trecords_15878) && IS_ATOM_INT(_9187)) {
                    _9188 = _trecords_15878 + _9187;
                    if ((long)((unsigned long)_9188 + (unsigned long)HIGH_BITS) >= 0) 
                    _9188 = NewDouble((double)_9188);
                }
                else {
                    if (IS_ATOM_INT(_trecords_15878)) {
                        _9188 = NewDouble((double)_trecords_15878 + DBL_PTR(_9187)->dbl);
                    }
                    else {
                        if (IS_ATOM_INT(_9187)) {
                            _9188 = NewDouble(DBL_PTR(_trecords_15878)->dbl + (double)_9187);
                        }
                        else
                        _9188 = NewDouble(DBL_PTR(_trecords_15878)->dbl + DBL_PTR(_9187)->dbl);
                    }
                }
                DeRef(_9187);
                _9187 = NOVALUE;
                DeRef(_pos_inlined_seek_at_964_16063);
                _pos_inlined_seek_at_964_16063 = _9188;
                _9188 = NOVALUE;

                /** 	return machine_func(M_SEEK, {fn, pos})*/
                Ref(_pos_inlined_seek_at_964_16063);
                DeRef(_seek_1__tmp_at967_16065);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _33current_db_15557;
                ((int *)_2)[2] = _pos_inlined_seek_at_964_16063;
                _seek_1__tmp_at967_16065 = MAKE_SEQ(_1);
                _seek_inlined_seek_at_967_16064 = machine(19, _seek_1__tmp_at967_16065);
                DeRef(_pos_inlined_seek_at_964_16063);
                _pos_inlined_seek_at_964_16063 = NOVALUE;
                DeRef(_seek_1__tmp_at967_16065);
                _seek_1__tmp_at967_16065 = NOVALUE;

                /** 						key_ptr = get4()*/
                _0 = _key_ptr_15881;
                _key_ptr_15881 = _33get4();
                DeRef(_0);

                /** 						if low_level_too then printf(fn, "[key %d:#%08x]\n", {r, key_ptr}) end if*/
                if (_low_level_too_15870 == 0)
                {
                    goto L25; // [988] 1000
                }
                else{
                }
                Ref(_key_ptr_15881);
                Ref(_r_16051);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _r_16051;
                ((int *)_2)[2] = _key_ptr_15881;
                _9191 = MAKE_SEQ(_1);
                EPrintf(_fn_15874, _9190, _9191);
                DeRefDS(_9191);
                _9191 = NOVALUE;
L25: 

                /** 						io:seek(current_db, key_ptr)*/

                /** 	return machine_func(M_SEEK, {fn, pos})*/
                Ref(_key_ptr_15881);
                DeRef(_seek_1__tmp_at1003_16072);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _33current_db_15557;
                ((int *)_2)[2] = _key_ptr_15881;
                _seek_1__tmp_at1003_16072 = MAKE_SEQ(_1);
                _seek_inlined_seek_at_1003_16071 = machine(19, _seek_1__tmp_at1003_16072);
                DeRef(_seek_1__tmp_at1003_16072);
                _seek_1__tmp_at1003_16072 = NOVALUE;

                /** 						data_ptr = get4()*/
                _0 = _data_ptr_15882;
                _data_ptr_15882 = _33get4();
                DeRef(_0);

                /** 						key = decompress(0)*/
                _0 = _key_15887;
                _key_15887 = _33decompress(0);
                DeRef(_0);

                /** 						puts(fn, "  key: ")*/
                EPuts(_fn_15874, _9194); // DJP 

                /** 						pretty:pretty_print(fn, key, {2, 2, 8})*/
                Ref(_key_15887);
                RefDS(_9195);
                _2pretty_print(_fn_15874, _key_15887, _9195);

                /** 						puts(fn, '\n')*/
                EPuts(_fn_15874, 10); // DJP 

                /** 						if low_level_too then printf(fn, "[data %d:#%08x]\n", {r, data_ptr}) end if*/
                if (_low_level_too_15870 == 0)
                {
                    goto L26; // [1047] 1059
                }
                else{
                }
                Ref(_data_ptr_15882);
                Ref(_r_16051);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _r_16051;
                ((int *)_2)[2] = _data_ptr_15882;
                _9197 = MAKE_SEQ(_1);
                EPrintf(_fn_15874, _9196, _9197);
                DeRefDS(_9197);
                _9197 = NOVALUE;
L26: 

                /** 						io:seek(current_db, data_ptr)*/

                /** 	return machine_func(M_SEEK, {fn, pos})*/
                Ref(_data_ptr_15882);
                DeRef(_seek_1__tmp_at1062_16082);
                _1 = NewS1(2);
                _2 = (int)((s1_ptr)_1)->base;
                ((int *)_2)[1] = _33current_db_15557;
                ((int *)_2)[2] = _data_ptr_15882;
                _seek_1__tmp_at1062_16082 = MAKE_SEQ(_1);
                _seek_inlined_seek_at_1062_16081 = machine(19, _seek_1__tmp_at1062_16082);
                DeRef(_seek_1__tmp_at1062_16082);
                _seek_1__tmp_at1062_16082 = NOVALUE;

                /** 						data = decompress(0)*/
                _0 = _data_15888;
                _data_15888 = _33decompress(0);
                DeRef(_0);

                /** 						puts(fn, "  data: ")*/
                EPuts(_fn_15874, _9199); // DJP 

                /** 						pretty:pretty_print(fn, data, {2, 2, 9})*/
                Ref(_data_15888);
                RefDS(_9200);
                _2pretty_print(_fn_15874, _data_15888, _9200);

                /** 						puts(fn, "\n\n")*/
                EPuts(_fn_15874, _9148); // DJP 

                /** 					end for*/
                _0 = _r_16051;
                if (IS_ATOM_INT(_r_16051)) {
                    _r_16051 = _r_16051 + 1;
                    if ((long)((unsigned long)_r_16051 +(unsigned long) HIGH_BITS) >= 0){
                        _r_16051 = NewDouble((double)_r_16051);
                    }
                }
                else {
                    _r_16051 = binary_op_a(PLUS, _r_16051, 1);
                }
                DeRef(_0);
                goto L22; // [1101] 923
L23: 
                ;
                DeRef(_r_16051);
            }
            goto L27; // [1106] 1116
L21: 

            /** 					printf(fn, "\nblock #%d (empty)\n\n", b)*/
            EPrintf(_fn_15874, _9201, _b_16029);
L27: 

            /** 			end for*/
            _b_16029 = _b_16029 + 1;
            goto L1E; // [1118] 827
L1F: 
            ;
        }
L1D: 

        /** 		t_header += SIZEOF_TABLE_HEADER*/
        _0 = _t_header_15879;
        if (IS_ATOM_INT(_t_header_15879)) {
            _t_header_15879 = _t_header_15879 + 16;
            if ((long)((unsigned long)_t_header_15879 + (unsigned long)HIGH_BITS) >= 0) 
            _t_header_15879 = NewDouble((double)_t_header_15879);
        }
        else {
            _t_header_15879 = NewDouble(DBL_PTR(_t_header_15879)->dbl + (double)16);
        }
        DeRef(_0);

        /** 		io:seek(current_db, t_header)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_t_header_15879);
        DeRef(_seek_1__tmp_at1133_16091);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _t_header_15879;
        _seek_1__tmp_at1133_16091 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_1133_16090 = machine(19, _seek_1__tmp_at1133_16091);
        DeRef(_seek_1__tmp_at1133_16091);
        _seek_1__tmp_at1133_16091 = NOVALUE;

        /** 	end for*/
        _0 = _t_16010;
        if (IS_ATOM_INT(_t_16010)) {
            _t_16010 = _t_16010 + 1;
            if ((long)((unsigned long)_t_16010 +(unsigned long) HIGH_BITS) >= 0){
                _t_16010 = NewDouble((double)_t_16010);
            }
        }
        else {
            _t_16010 = binary_op_a(PLUS, _t_16010, 1);
        }
        DeRef(_0);
        goto L19; // [1149] 733
L1A: 
        ;
        DeRef(_t_16010);
    }

    /** 	if low_level_too then printf(fn, "[free blocks:#%08x]\n", FREE_COUNT) end if*/
    if (_low_level_too_15870 == 0)
    {
        goto L28; // [1156] 1164
    }
    else{
    }
    EPrintf(_fn_15874, _9203, 7);
L28: 

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at1167_16096);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at1167_16096 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1167_16095 = machine(19, _seek_1__tmp_at1167_16096);
    DeRefi(_seek_1__tmp_at1167_16096);
    _seek_1__tmp_at1167_16096 = NOVALUE;

    /** 	n = get4()*/
    _n_15890 = _33get4();
    if (!IS_ATOM_INT(_n_15890)) {
        _1 = (long)(DBL_PTR(_n_15890)->dbl);
        if (UNIQUE(DBL_PTR(_n_15890)) && (DBL_PTR(_n_15890)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_15890);
        _n_15890 = _1;
    }

    /** 	puts(fn, '\n')*/
    EPuts(_fn_15874, 10); // DJP 

    /** 	if n > 0 then*/
    if (_n_15890 <= 0)
    goto L29; // [1197] 1286

    /** 		fbp = get4()*/
    _0 = _fbp_15886;
    _fbp_15886 = _33get4();
    DeRef(_0);

    /** 		printf(fn, "Number of Free blocks: %d ", n)*/
    EPrintf(_fn_15874, _9207, _n_15890);

    /** 		if low_level_too then printf(fn, " [#%08x]:", fbp) end if*/
    if (_low_level_too_15870 == 0)
    {
        goto L2A; // [1214] 1222
    }
    else{
    }
    EPrintf(_fn_15874, _9208, _fbp_15886);
L2A: 

    /** 		puts(fn, '\n')*/
    EPuts(_fn_15874, 10); // DJP 

    /** 		io:seek(current_db, fbp)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_fbp_15886);
    DeRef(_seek_1__tmp_at1230_16106);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _fbp_15886;
    _seek_1__tmp_at1230_16106 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1230_16105 = machine(19, _seek_1__tmp_at1230_16106);
    DeRef(_seek_1__tmp_at1230_16106);
    _seek_1__tmp_at1230_16106 = NOVALUE;

    /** 		for i = 1 to n do*/
    _9209 = _n_15890;
    {
        int _i_16108;
        _i_16108 = 1;
L2B: 
        if (_i_16108 > _9209){
            goto L2C; // [1249] 1283
        }

        /** 			addr = get4()*/
        _0 = _addr_15884;
        _addr_15884 = _33get4();
        DeRef(_0);

        /** 			size = get4()*/
        _0 = _size_15883;
        _size_15883 = _33get4();
        DeRef(_0);

        /** 			printf(fn, "%08x: %6d bytes\n", {addr, size})*/
        Ref(_size_15883);
        Ref(_addr_15884);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _addr_15884;
        ((int *)_2)[2] = _size_15883;
        _9213 = MAKE_SEQ(_1);
        EPrintf(_fn_15874, _9212, _9213);
        DeRefDS(_9213);
        _9213 = NOVALUE;

        /** 		end for*/
        _i_16108 = _i_16108 + 1;
        goto L2B; // [1278] 1256
L2C: 
        ;
    }
    goto L2D; // [1283] 1292
L29: 

    /** 		puts(fn, "No free blocks available.\n")*/
    EPuts(_fn_15874, _9214); // DJP 
L2D: 

    /** 	if sequence(file_id) then*/
    _9215 = IS_SEQUENCE(_file_id_15869);
    if (_9215 == 0)
    {
        _9215 = NOVALUE;
        goto L2E; // [1297] 1305
    }
    else{
        _9215 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_15874);
L2E: 

    /** end procedure*/
    DeRef(_file_id_15869);
    DeRef(_tables_15875);
    DeRef(_ntables_15876);
    DeRef(_tname_15877);
    DeRef(_trecords_15878);
    DeRef(_t_header_15879);
    DeRef(_tnrecs_15880);
    DeRef(_key_ptr_15881);
    DeRef(_data_ptr_15882);
    DeRef(_size_15883);
    DeRef(_addr_15884);
    DeRef(_tindex_15885);
    DeRef(_fbp_15886);
    DeRef(_key_15887);
    DeRef(_data_15888);
    DeRef(_a_15892);
    DeRef(_ll_line_15893);
    DeRef(_9140);
    _9140 = NOVALUE;
    return;
    ;
}


void _33check_free_list()
{
    int _msg_inlined_crash_at_273_16176 = NOVALUE;
    int _msg_inlined_crash_at_233_16169 = NOVALUE;
    int _msg_inlined_crash_at_201_16163 = NOVALUE;
    int _msg_inlined_crash_at_142_16152 = NOVALUE;
    int _msg_inlined_crash_at_87_16143 = NOVALUE;
    int _msg_inlined_crash_at_55_16137 = NOVALUE;
    int _where_inlined_where_at_25_16130 = NOVALUE;
    int _free_count_16120 = NOVALUE;
    int _free_list_16121 = NOVALUE;
    int _addr_16122 = NOVALUE;
    int _size_16123 = NOVALUE;
    int _free_list_space_16124 = NOVALUE;
    int _max_16125 = NOVALUE;
    int _9241 = NOVALUE;
    int _9240 = NOVALUE;
    int _9233 = NOVALUE;
    int _9232 = NOVALUE;
    int _9231 = NOVALUE;
    int _9229 = NOVALUE;
    int _9227 = NOVALUE;
    int _9225 = NOVALUE;
    int _9219 = NOVALUE;
    int _9216 = NOVALUE;
    int _0, _1, _2;
    

    /** 	safe_seek(-1)*/
    RefDS(_5);
    _33safe_seek(-1, _5);

    /** 	if length(vLastErrors) > 0 then return end if*/
    if (IS_SEQUENCE(_33vLastErrors_15581)){
            _9216 = SEQ_PTR(_33vLastErrors_15581)->length;
    }
    else {
        _9216 = 1;
    }
    if (_9216 <= 0)
    goto L1; // [14] 22
    DeRef(_free_count_16120);
    DeRef(_free_list_16121);
    DeRef(_addr_16122);
    DeRef(_size_16123);
    DeRef(_free_list_space_16124);
    DeRef(_max_16125);
    return;
L1: 

    /** 	max = io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_max_16125);
    _max_16125 = machine(20, _33current_db_15557);

    /** 	safe_seek( FREE_COUNT)*/
    RefDS(_5);
    _33safe_seek(7, _5);

    /** 	free_count = get4()*/
    _0 = _free_count_16120;
    _free_count_16120 = _33get4();
    DeRef(_0);

    /** 	if free_count > max/13 then*/
    if (IS_ATOM_INT(_max_16125)) {
        _9219 = (_max_16125 % 13) ? NewDouble((double)_max_16125 / 13) : (_max_16125 / 13);
    }
    else {
        _9219 = NewDouble(DBL_PTR(_max_16125)->dbl / (double)13);
    }
    if (binary_op_a(LESSEQ, _free_count_16120, _9219)){
        DeRef(_9219);
        _9219 = NOVALUE;
        goto L2; // [50] 75
    }
    DeRef(_9219);
    _9219 = NOVALUE;

    /** 		error:crash("free count is too high")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_55_16137);
    _msg_inlined_crash_at_55_16137 = EPrintf(-9999999, _9221, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_55_16137);

    /** end procedure*/
    goto L3; // [69] 72
L3: 
    DeRefi(_msg_inlined_crash_at_55_16137);
    _msg_inlined_crash_at_55_16137 = NOVALUE;
L2: 

    /** 	free_list = get4()*/
    _0 = _free_list_16121;
    _free_list_16121 = _33get4();
    DeRef(_0);

    /** 	if free_list > max then*/
    if (binary_op_a(LESSEQ, _free_list_16121, _max_16125)){
        goto L4; // [82] 107
    }

    /** 		error:crash("bad free list pointer")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_87_16143);
    _msg_inlined_crash_at_87_16143 = EPrintf(-9999999, _9224, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_87_16143);

    /** end procedure*/
    goto L5; // [101] 104
L5: 
    DeRefi(_msg_inlined_crash_at_87_16143);
    _msg_inlined_crash_at_87_16143 = NOVALUE;
L4: 

    /** 	safe_seek( free_list - 4)*/
    if (IS_ATOM_INT(_free_list_16121)) {
        _9225 = _free_list_16121 - 4;
        if ((long)((unsigned long)_9225 +(unsigned long) HIGH_BITS) >= 0){
            _9225 = NewDouble((double)_9225);
        }
    }
    else {
        _9225 = NewDouble(DBL_PTR(_free_list_16121)->dbl - (double)4);
    }
    RefDS(_5);
    _33safe_seek(_9225, _5);
    _9225 = NOVALUE;

    /** 	free_list_space = get4()*/
    _0 = _free_list_space_16124;
    _free_list_space_16124 = _33get4();
    DeRef(_0);

    /** 	if free_list_space > max or free_list_space < 0 then*/
    if (IS_ATOM_INT(_free_list_space_16124) && IS_ATOM_INT(_max_16125)) {
        _9227 = (_free_list_space_16124 > _max_16125);
    }
    else {
        if (IS_ATOM_INT(_free_list_space_16124)) {
            _9227 = ((double)_free_list_space_16124 > DBL_PTR(_max_16125)->dbl);
        }
        else {
            if (IS_ATOM_INT(_max_16125)) {
                _9227 = (DBL_PTR(_free_list_space_16124)->dbl > (double)_max_16125);
            }
            else
            _9227 = (DBL_PTR(_free_list_space_16124)->dbl > DBL_PTR(_max_16125)->dbl);
        }
    }
    if (_9227 != 0) {
        goto L6; // [128] 141
    }
    if (IS_ATOM_INT(_free_list_space_16124)) {
        _9229 = (_free_list_space_16124 < 0);
    }
    else {
        _9229 = (DBL_PTR(_free_list_space_16124)->dbl < (double)0);
    }
    if (_9229 == 0)
    {
        DeRef(_9229);
        _9229 = NOVALUE;
        goto L7; // [137] 162
    }
    else{
        DeRef(_9229);
        _9229 = NOVALUE;
    }
L6: 

    /** 		error:crash("free list space is bad")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_142_16152);
    _msg_inlined_crash_at_142_16152 = EPrintf(-9999999, _9230, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_142_16152);

    /** end procedure*/
    goto L8; // [156] 159
L8: 
    DeRefi(_msg_inlined_crash_at_142_16152);
    _msg_inlined_crash_at_142_16152 = NOVALUE;
L7: 

    /** 	for i = 0 to free_count - 1 do*/
    if (IS_ATOM_INT(_free_count_16120)) {
        _9231 = _free_count_16120 - 1;
        if ((long)((unsigned long)_9231 +(unsigned long) HIGH_BITS) >= 0){
            _9231 = NewDouble((double)_9231);
        }
    }
    else {
        _9231 = NewDouble(DBL_PTR(_free_count_16120)->dbl - (double)1);
    }
    {
        int _i_16154;
        _i_16154 = 0;
L9: 
        if (binary_op_a(GREATER, _i_16154, _9231)){
            goto LA; // [168] 300
        }

        /** 		safe_seek( free_list + i * 8)*/
        if (IS_ATOM_INT(_i_16154)) {
            if (_i_16154 == (short)_i_16154)
            _9232 = _i_16154 * 8;
            else
            _9232 = NewDouble(_i_16154 * (double)8);
        }
        else {
            _9232 = NewDouble(DBL_PTR(_i_16154)->dbl * (double)8);
        }
        if (IS_ATOM_INT(_free_list_16121) && IS_ATOM_INT(_9232)) {
            _9233 = _free_list_16121 + _9232;
            if ((long)((unsigned long)_9233 + (unsigned long)HIGH_BITS) >= 0) 
            _9233 = NewDouble((double)_9233);
        }
        else {
            if (IS_ATOM_INT(_free_list_16121)) {
                _9233 = NewDouble((double)_free_list_16121 + DBL_PTR(_9232)->dbl);
            }
            else {
                if (IS_ATOM_INT(_9232)) {
                    _9233 = NewDouble(DBL_PTR(_free_list_16121)->dbl + (double)_9232);
                }
                else
                _9233 = NewDouble(DBL_PTR(_free_list_16121)->dbl + DBL_PTR(_9232)->dbl);
            }
        }
        DeRef(_9232);
        _9232 = NOVALUE;
        RefDS(_5);
        _33safe_seek(_9233, _5);
        _9233 = NOVALUE;

        /** 		addr = get4()*/
        _0 = _addr_16122;
        _addr_16122 = _33get4();
        DeRef(_0);

        /** 		if addr > max then*/
        if (binary_op_a(LESSEQ, _addr_16122, _max_16125)){
            goto LB; // [196] 221
        }

        /** 			error:crash("bad block address")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_201_16163);
        _msg_inlined_crash_at_201_16163 = EPrintf(-9999999, _9236, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_201_16163);

        /** end procedure*/
        goto LC; // [215] 218
LC: 
        DeRefi(_msg_inlined_crash_at_201_16163);
        _msg_inlined_crash_at_201_16163 = NOVALUE;
LB: 

        /** 		size = get4()*/
        _0 = _size_16123;
        _size_16123 = _33get4();
        DeRef(_0);

        /** 		if size > max then*/
        if (binary_op_a(LESSEQ, _size_16123, _max_16125)){
            goto LD; // [228] 253
        }

        /** 			error:crash("block size too big")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_233_16169);
        _msg_inlined_crash_at_233_16169 = EPrintf(-9999999, _9239, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_233_16169);

        /** end procedure*/
        goto LE; // [247] 250
LE: 
        DeRefi(_msg_inlined_crash_at_233_16169);
        _msg_inlined_crash_at_233_16169 = NOVALUE;
LD: 

        /** 		safe_seek( addr - 4)*/
        if (IS_ATOM_INT(_addr_16122)) {
            _9240 = _addr_16122 - 4;
            if ((long)((unsigned long)_9240 +(unsigned long) HIGH_BITS) >= 0){
                _9240 = NewDouble((double)_9240);
            }
        }
        else {
            _9240 = NewDouble(DBL_PTR(_addr_16122)->dbl - (double)4);
        }
        RefDS(_5);
        _33safe_seek(_9240, _5);
        _9240 = NOVALUE;

        /** 		if get4() > size then*/
        _9241 = _33get4();
        if (binary_op_a(LESSEQ, _9241, _size_16123)){
            DeRef(_9241);
            _9241 = NOVALUE;
            goto LF; // [268] 293
        }
        DeRef(_9241);
        _9241 = NOVALUE;

        /** 			error:crash("bad size in front of free block")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_273_16176);
        _msg_inlined_crash_at_273_16176 = EPrintf(-9999999, _9243, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_273_16176);

        /** end procedure*/
        goto L10; // [287] 290
L10: 
        DeRefi(_msg_inlined_crash_at_273_16176);
        _msg_inlined_crash_at_273_16176 = NOVALUE;
LF: 

        /** 	end for*/
        _0 = _i_16154;
        if (IS_ATOM_INT(_i_16154)) {
            _i_16154 = _i_16154 + 1;
            if ((long)((unsigned long)_i_16154 +(unsigned long) HIGH_BITS) >= 0){
                _i_16154 = NewDouble((double)_i_16154);
            }
        }
        else {
            _i_16154 = binary_op_a(PLUS, _i_16154, 1);
        }
        DeRef(_0);
        goto L9; // [295] 175
LA: 
        ;
        DeRef(_i_16154);
    }

    /** end procedure*/
    DeRef(_free_count_16120);
    DeRef(_free_list_16121);
    DeRef(_addr_16122);
    DeRef(_size_16123);
    DeRef(_free_list_space_16124);
    DeRef(_max_16125);
    DeRef(_9227);
    _9227 = NOVALUE;
    DeRef(_9231);
    _9231 = NOVALUE;
    return;
    ;
}


int _33db_allocate(int _n_16179)
{
    int _free_list_16180 = NOVALUE;
    int _size_16181 = NOVALUE;
    int _size_ptr_16182 = NOVALUE;
    int _addr_16183 = NOVALUE;
    int _free_count_16184 = NOVALUE;
    int _remaining_16185 = NOVALUE;
    int _seek_1__tmp_at4_16188 = NOVALUE;
    int _seek_inlined_seek_at_4_16187 = NOVALUE;
    int _seek_1__tmp_at41_16195 = NOVALUE;
    int _seek_inlined_seek_at_41_16194 = NOVALUE;
    int _seek_1__tmp_at113_16212 = NOVALUE;
    int _seek_inlined_seek_at_113_16211 = NOVALUE;
    int _pos_inlined_seek_at_110_16210 = NOVALUE;
    int _put4_1__tmp_at139_16217 = NOVALUE;
    int _x_inlined_put4_at_136_16216 = NOVALUE;
    int _seek_1__tmp_at169_16220 = NOVALUE;
    int _seek_inlined_seek_at_169_16219 = NOVALUE;
    int _put4_1__tmp_at195_16225 = NOVALUE;
    int _x_inlined_put4_at_192_16224 = NOVALUE;
    int _seek_1__tmp_at246_16233 = NOVALUE;
    int _seek_inlined_seek_at_246_16232 = NOVALUE;
    int _pos_inlined_seek_at_243_16231 = NOVALUE;
    int _put4_1__tmp_at268_16237 = NOVALUE;
    int _x_inlined_put4_at_265_16236 = NOVALUE;
    int _seek_1__tmp_at335_16248 = NOVALUE;
    int _seek_inlined_seek_at_335_16247 = NOVALUE;
    int _pos_inlined_seek_at_332_16246 = NOVALUE;
    int _seek_1__tmp_at366_16252 = NOVALUE;
    int _seek_inlined_seek_at_366_16251 = NOVALUE;
    int _put4_1__tmp_at388_16256 = NOVALUE;
    int _x_inlined_put4_at_385_16255 = NOVALUE;
    int _seek_1__tmp_at425_16261 = NOVALUE;
    int _seek_inlined_seek_at_425_16260 = NOVALUE;
    int _pos_inlined_seek_at_422_16259 = NOVALUE;
    int _put4_1__tmp_at440_16263 = NOVALUE;
    int _seek_1__tmp_at492_16267 = NOVALUE;
    int _seek_inlined_seek_at_492_16266 = NOVALUE;
    int _put4_1__tmp_at514_16271 = NOVALUE;
    int _x_inlined_put4_at_511_16270 = NOVALUE;
    int _where_inlined_where_at_544_16273 = NOVALUE;
    int _9274 = NOVALUE;
    int _9272 = NOVALUE;
    int _9271 = NOVALUE;
    int _9270 = NOVALUE;
    int _9269 = NOVALUE;
    int _9268 = NOVALUE;
    int _9266 = NOVALUE;
    int _9265 = NOVALUE;
    int _9264 = NOVALUE;
    int _9263 = NOVALUE;
    int _9261 = NOVALUE;
    int _9260 = NOVALUE;
    int _9259 = NOVALUE;
    int _9258 = NOVALUE;
    int _9257 = NOVALUE;
    int _9256 = NOVALUE;
    int _9255 = NOVALUE;
    int _9253 = NOVALUE;
    int _9251 = NOVALUE;
    int _9248 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_16188);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at4_16188 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_16187 = machine(19, _seek_1__tmp_at4_16188);
    DeRefi(_seek_1__tmp_at4_16188);
    _seek_1__tmp_at4_16188 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_16184 = _33get4();
    if (!IS_ATOM_INT(_free_count_16184)) {
        _1 = (long)(DBL_PTR(_free_count_16184)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_16184)) && (DBL_PTR(_free_count_16184)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_16184);
        _free_count_16184 = _1;
    }

    /** 	if free_count > 0 then*/
    if (_free_count_16184 <= 0)
    goto L1; // [29] 489

    /** 		free_list = get4()*/
    _0 = _free_list_16180;
    _free_list_16180 = _33get4();
    DeRef(_0);

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_16180);
    DeRef(_seek_1__tmp_at41_16195);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _free_list_16180;
    _seek_1__tmp_at41_16195 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_41_16194 = machine(19, _seek_1__tmp_at41_16195);
    DeRef(_seek_1__tmp_at41_16195);
    _seek_1__tmp_at41_16195 = NOVALUE;

    /** 		size_ptr = free_list + 4*/
    DeRef(_size_ptr_16182);
    if (IS_ATOM_INT(_free_list_16180)) {
        _size_ptr_16182 = _free_list_16180 + 4;
        if ((long)((unsigned long)_size_ptr_16182 + (unsigned long)HIGH_BITS) >= 0) 
        _size_ptr_16182 = NewDouble((double)_size_ptr_16182);
    }
    else {
        _size_ptr_16182 = NewDouble(DBL_PTR(_free_list_16180)->dbl + (double)4);
    }

    /** 		for i = 1 to free_count do*/
    _9248 = _free_count_16184;
    {
        int _i_16198;
        _i_16198 = 1;
L2: 
        if (_i_16198 > _9248){
            goto L3; // [66] 488
        }

        /** 			addr = get4()*/
        _0 = _addr_16183;
        _addr_16183 = _33get4();
        DeRef(_0);

        /** 			size = get4()*/
        _0 = _size_16181;
        _size_16181 = _33get4();
        DeRef(_0);

        /** 			if size >= n+4 then*/
        if (IS_ATOM_INT(_n_16179)) {
            _9251 = _n_16179 + 4;
            if ((long)((unsigned long)_9251 + (unsigned long)HIGH_BITS) >= 0) 
            _9251 = NewDouble((double)_9251);
        }
        else {
            _9251 = NewDouble(DBL_PTR(_n_16179)->dbl + (double)4);
        }
        if (binary_op_a(LESS, _size_16181, _9251)){
            DeRef(_9251);
            _9251 = NOVALUE;
            goto L4; // [89] 475
        }
        DeRef(_9251);
        _9251 = NOVALUE;

        /** 				if size >= n+16 then*/
        if (IS_ATOM_INT(_n_16179)) {
            _9253 = _n_16179 + 16;
            if ((long)((unsigned long)_9253 + (unsigned long)HIGH_BITS) >= 0) 
            _9253 = NewDouble((double)_9253);
        }
        else {
            _9253 = NewDouble(DBL_PTR(_n_16179)->dbl + (double)16);
        }
        if (binary_op_a(LESS, _size_16181, _9253)){
            DeRef(_9253);
            _9253 = NOVALUE;
            goto L5; // [99] 298
        }
        DeRef(_9253);
        _9253 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_16183)) {
            _9255 = _addr_16183 - 4;
            if ((long)((unsigned long)_9255 +(unsigned long) HIGH_BITS) >= 0){
                _9255 = NewDouble((double)_9255);
            }
        }
        else {
            _9255 = NewDouble(DBL_PTR(_addr_16183)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_110_16210);
        _pos_inlined_seek_at_110_16210 = _9255;
        _9255 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_110_16210);
        DeRef(_seek_1__tmp_at113_16212);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _pos_inlined_seek_at_110_16210;
        _seek_1__tmp_at113_16212 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_113_16211 = machine(19, _seek_1__tmp_at113_16212);
        DeRef(_pos_inlined_seek_at_110_16210);
        _pos_inlined_seek_at_110_16210 = NOVALUE;
        DeRef(_seek_1__tmp_at113_16212);
        _seek_1__tmp_at113_16212 = NOVALUE;

        /** 					put4(size-n-4) -- shrink the block*/
        if (IS_ATOM_INT(_size_16181) && IS_ATOM_INT(_n_16179)) {
            _9256 = _size_16181 - _n_16179;
            if ((long)((unsigned long)_9256 +(unsigned long) HIGH_BITS) >= 0){
                _9256 = NewDouble((double)_9256);
            }
        }
        else {
            if (IS_ATOM_INT(_size_16181)) {
                _9256 = NewDouble((double)_size_16181 - DBL_PTR(_n_16179)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_16179)) {
                    _9256 = NewDouble(DBL_PTR(_size_16181)->dbl - (double)_n_16179);
                }
                else
                _9256 = NewDouble(DBL_PTR(_size_16181)->dbl - DBL_PTR(_n_16179)->dbl);
            }
        }
        if (IS_ATOM_INT(_9256)) {
            _9257 = _9256 - 4;
            if ((long)((unsigned long)_9257 +(unsigned long) HIGH_BITS) >= 0){
                _9257 = NewDouble((double)_9257);
            }
        }
        else {
            _9257 = NewDouble(DBL_PTR(_9256)->dbl - (double)4);
        }
        DeRef(_9256);
        _9256 = NOVALUE;
        DeRef(_x_inlined_put4_at_136_16216);
        _x_inlined_put4_at_136_16216 = _9257;
        _9257 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_33mem0_15599)){
            poke4_addr = (unsigned long *)_33mem0_15599;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_136_16216)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_136_16216;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_136_16216)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at139_16217);
        _1 = (int)SEQ_PTR(_33memseq_15819);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at139_16217 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_33current_db_15557, _put4_1__tmp_at139_16217); // DJP 

        /** end procedure*/
        goto L6; // [161] 164
L6: 
        DeRef(_x_inlined_put4_at_136_16216);
        _x_inlined_put4_at_136_16216 = NOVALUE;
        DeRefi(_put4_1__tmp_at139_16217);
        _put4_1__tmp_at139_16217 = NOVALUE;

        /** 					io:seek(current_db, size_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_size_ptr_16182);
        DeRef(_seek_1__tmp_at169_16220);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _size_ptr_16182;
        _seek_1__tmp_at169_16220 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_169_16219 = machine(19, _seek_1__tmp_at169_16220);
        DeRef(_seek_1__tmp_at169_16220);
        _seek_1__tmp_at169_16220 = NOVALUE;

        /** 					put4(size-n-4) -- update size on free list too*/
        if (IS_ATOM_INT(_size_16181) && IS_ATOM_INT(_n_16179)) {
            _9258 = _size_16181 - _n_16179;
            if ((long)((unsigned long)_9258 +(unsigned long) HIGH_BITS) >= 0){
                _9258 = NewDouble((double)_9258);
            }
        }
        else {
            if (IS_ATOM_INT(_size_16181)) {
                _9258 = NewDouble((double)_size_16181 - DBL_PTR(_n_16179)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_16179)) {
                    _9258 = NewDouble(DBL_PTR(_size_16181)->dbl - (double)_n_16179);
                }
                else
                _9258 = NewDouble(DBL_PTR(_size_16181)->dbl - DBL_PTR(_n_16179)->dbl);
            }
        }
        if (IS_ATOM_INT(_9258)) {
            _9259 = _9258 - 4;
            if ((long)((unsigned long)_9259 +(unsigned long) HIGH_BITS) >= 0){
                _9259 = NewDouble((double)_9259);
            }
        }
        else {
            _9259 = NewDouble(DBL_PTR(_9258)->dbl - (double)4);
        }
        DeRef(_9258);
        _9258 = NOVALUE;
        DeRef(_x_inlined_put4_at_192_16224);
        _x_inlined_put4_at_192_16224 = _9259;
        _9259 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_33mem0_15599)){
            poke4_addr = (unsigned long *)_33mem0_15599;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_192_16224)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_192_16224;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_192_16224)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at195_16225);
        _1 = (int)SEQ_PTR(_33memseq_15819);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at195_16225 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_33current_db_15557, _put4_1__tmp_at195_16225); // DJP 

        /** end procedure*/
        goto L7; // [217] 220
L7: 
        DeRef(_x_inlined_put4_at_192_16224);
        _x_inlined_put4_at_192_16224 = NOVALUE;
        DeRefi(_put4_1__tmp_at195_16225);
        _put4_1__tmp_at195_16225 = NOVALUE;

        /** 					addr += size-n-4*/
        if (IS_ATOM_INT(_size_16181) && IS_ATOM_INT(_n_16179)) {
            _9260 = _size_16181 - _n_16179;
            if ((long)((unsigned long)_9260 +(unsigned long) HIGH_BITS) >= 0){
                _9260 = NewDouble((double)_9260);
            }
        }
        else {
            if (IS_ATOM_INT(_size_16181)) {
                _9260 = NewDouble((double)_size_16181 - DBL_PTR(_n_16179)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_16179)) {
                    _9260 = NewDouble(DBL_PTR(_size_16181)->dbl - (double)_n_16179);
                }
                else
                _9260 = NewDouble(DBL_PTR(_size_16181)->dbl - DBL_PTR(_n_16179)->dbl);
            }
        }
        if (IS_ATOM_INT(_9260)) {
            _9261 = _9260 - 4;
            if ((long)((unsigned long)_9261 +(unsigned long) HIGH_BITS) >= 0){
                _9261 = NewDouble((double)_9261);
            }
        }
        else {
            _9261 = NewDouble(DBL_PTR(_9260)->dbl - (double)4);
        }
        DeRef(_9260);
        _9260 = NOVALUE;
        _0 = _addr_16183;
        if (IS_ATOM_INT(_addr_16183) && IS_ATOM_INT(_9261)) {
            _addr_16183 = _addr_16183 + _9261;
            if ((long)((unsigned long)_addr_16183 + (unsigned long)HIGH_BITS) >= 0) 
            _addr_16183 = NewDouble((double)_addr_16183);
        }
        else {
            if (IS_ATOM_INT(_addr_16183)) {
                _addr_16183 = NewDouble((double)_addr_16183 + DBL_PTR(_9261)->dbl);
            }
            else {
                if (IS_ATOM_INT(_9261)) {
                    _addr_16183 = NewDouble(DBL_PTR(_addr_16183)->dbl + (double)_9261);
                }
                else
                _addr_16183 = NewDouble(DBL_PTR(_addr_16183)->dbl + DBL_PTR(_9261)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_9261);
        _9261 = NOVALUE;

        /** 					io:seek(current_db, addr - 4) */
        if (IS_ATOM_INT(_addr_16183)) {
            _9263 = _addr_16183 - 4;
            if ((long)((unsigned long)_9263 +(unsigned long) HIGH_BITS) >= 0){
                _9263 = NewDouble((double)_9263);
            }
        }
        else {
            _9263 = NewDouble(DBL_PTR(_addr_16183)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_243_16231);
        _pos_inlined_seek_at_243_16231 = _9263;
        _9263 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_243_16231);
        DeRef(_seek_1__tmp_at246_16233);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _pos_inlined_seek_at_243_16231;
        _seek_1__tmp_at246_16233 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_246_16232 = machine(19, _seek_1__tmp_at246_16233);
        DeRef(_pos_inlined_seek_at_243_16231);
        _pos_inlined_seek_at_243_16231 = NOVALUE;
        DeRef(_seek_1__tmp_at246_16233);
        _seek_1__tmp_at246_16233 = NOVALUE;

        /** 					put4(n+4)*/
        if (IS_ATOM_INT(_n_16179)) {
            _9264 = _n_16179 + 4;
            if ((long)((unsigned long)_9264 + (unsigned long)HIGH_BITS) >= 0) 
            _9264 = NewDouble((double)_9264);
        }
        else {
            _9264 = NewDouble(DBL_PTR(_n_16179)->dbl + (double)4);
        }
        DeRef(_x_inlined_put4_at_265_16236);
        _x_inlined_put4_at_265_16236 = _9264;
        _9264 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_33mem0_15599)){
            poke4_addr = (unsigned long *)_33mem0_15599;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_265_16236)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_265_16236;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_265_16236)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at268_16237);
        _1 = (int)SEQ_PTR(_33memseq_15819);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at268_16237 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_33current_db_15557, _put4_1__tmp_at268_16237); // DJP 

        /** end procedure*/
        goto L8; // [290] 293
L8: 
        DeRef(_x_inlined_put4_at_265_16236);
        _x_inlined_put4_at_265_16236 = NOVALUE;
        DeRefi(_put4_1__tmp_at268_16237);
        _put4_1__tmp_at268_16237 = NOVALUE;
        goto L9; // [295] 468
L5: 

        /** 					remaining = io:get_bytes(current_db, (free_count-i) * 8)*/
        _9265 = _free_count_16184 - _i_16198;
        if ((long)((unsigned long)_9265 +(unsigned long) HIGH_BITS) >= 0){
            _9265 = NewDouble((double)_9265);
        }
        if (IS_ATOM_INT(_9265)) {
            if (_9265 == (short)_9265)
            _9266 = _9265 * 8;
            else
            _9266 = NewDouble(_9265 * (double)8);
        }
        else {
            _9266 = NewDouble(DBL_PTR(_9265)->dbl * (double)8);
        }
        DeRef(_9265);
        _9265 = NOVALUE;
        _0 = _remaining_16185;
        _remaining_16185 = _15get_bytes(_33current_db_15557, _9266);
        DeRef(_0);
        _9266 = NOVALUE;

        /** 					io:seek(current_db, free_list+8*(i-1))*/
        _9268 = _i_16198 - 1;
        if (_9268 <= INT15)
        _9269 = 8 * _9268;
        else
        _9269 = NewDouble(8 * (double)_9268);
        _9268 = NOVALUE;
        if (IS_ATOM_INT(_free_list_16180) && IS_ATOM_INT(_9269)) {
            _9270 = _free_list_16180 + _9269;
            if ((long)((unsigned long)_9270 + (unsigned long)HIGH_BITS) >= 0) 
            _9270 = NewDouble((double)_9270);
        }
        else {
            if (IS_ATOM_INT(_free_list_16180)) {
                _9270 = NewDouble((double)_free_list_16180 + DBL_PTR(_9269)->dbl);
            }
            else {
                if (IS_ATOM_INT(_9269)) {
                    _9270 = NewDouble(DBL_PTR(_free_list_16180)->dbl + (double)_9269);
                }
                else
                _9270 = NewDouble(DBL_PTR(_free_list_16180)->dbl + DBL_PTR(_9269)->dbl);
            }
        }
        DeRef(_9269);
        _9269 = NOVALUE;
        DeRef(_pos_inlined_seek_at_332_16246);
        _pos_inlined_seek_at_332_16246 = _9270;
        _9270 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_332_16246);
        DeRef(_seek_1__tmp_at335_16248);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _pos_inlined_seek_at_332_16246;
        _seek_1__tmp_at335_16248 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_335_16247 = machine(19, _seek_1__tmp_at335_16248);
        DeRef(_pos_inlined_seek_at_332_16246);
        _pos_inlined_seek_at_332_16246 = NOVALUE;
        DeRef(_seek_1__tmp_at335_16248);
        _seek_1__tmp_at335_16248 = NOVALUE;

        /** 					putn(remaining)*/

        /** 	puts(current_db, s)*/
        EPuts(_33current_db_15557, _remaining_16185); // DJP 

        /** end procedure*/
        goto LA; // [360] 363
LA: 

        /** 					io:seek(current_db, FREE_COUNT)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        DeRefi(_seek_1__tmp_at366_16252);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = 7;
        _seek_1__tmp_at366_16252 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_366_16251 = machine(19, _seek_1__tmp_at366_16252);
        DeRefi(_seek_1__tmp_at366_16252);
        _seek_1__tmp_at366_16252 = NOVALUE;

        /** 					put4(free_count-1)*/
        _9271 = _free_count_16184 - 1;
        if ((long)((unsigned long)_9271 +(unsigned long) HIGH_BITS) >= 0){
            _9271 = NewDouble((double)_9271);
        }
        DeRef(_x_inlined_put4_at_385_16255);
        _x_inlined_put4_at_385_16255 = _9271;
        _9271 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_33mem0_15599)){
            poke4_addr = (unsigned long *)_33mem0_15599;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_385_16255)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_385_16255;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_385_16255)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at388_16256);
        _1 = (int)SEQ_PTR(_33memseq_15819);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at388_16256 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_33current_db_15557, _put4_1__tmp_at388_16256); // DJP 

        /** end procedure*/
        goto LB; // [410] 413
LB: 
        DeRef(_x_inlined_put4_at_385_16255);
        _x_inlined_put4_at_385_16255 = NOVALUE;
        DeRefi(_put4_1__tmp_at388_16256);
        _put4_1__tmp_at388_16256 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_16183)) {
            _9272 = _addr_16183 - 4;
            if ((long)((unsigned long)_9272 +(unsigned long) HIGH_BITS) >= 0){
                _9272 = NewDouble((double)_9272);
            }
        }
        else {
            _9272 = NewDouble(DBL_PTR(_addr_16183)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_422_16259);
        _pos_inlined_seek_at_422_16259 = _9272;
        _9272 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_422_16259);
        DeRef(_seek_1__tmp_at425_16261);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _pos_inlined_seek_at_422_16259;
        _seek_1__tmp_at425_16261 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_425_16260 = machine(19, _seek_1__tmp_at425_16261);
        DeRef(_pos_inlined_seek_at_422_16259);
        _pos_inlined_seek_at_422_16259 = NOVALUE;
        DeRef(_seek_1__tmp_at425_16261);
        _seek_1__tmp_at425_16261 = NOVALUE;

        /** 					put4(size) -- in case size was not updated by db_free()*/

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_33mem0_15599)){
            poke4_addr = (unsigned long *)_33mem0_15599;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        }
        if (IS_ATOM_INT(_size_16181)) {
            *poke4_addr = (unsigned long)_size_16181;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_size_16181)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at440_16263);
        _1 = (int)SEQ_PTR(_33memseq_15819);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at440_16263 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_33current_db_15557, _put4_1__tmp_at440_16263); // DJP 

        /** end procedure*/
        goto LC; // [462] 465
LC: 
        DeRefi(_put4_1__tmp_at440_16263);
        _put4_1__tmp_at440_16263 = NOVALUE;
L9: 

        /** 				return addr*/
        DeRef(_n_16179);
        DeRef(_free_list_16180);
        DeRef(_size_16181);
        DeRef(_size_ptr_16182);
        DeRef(_remaining_16185);
        return _addr_16183;
L4: 

        /** 			size_ptr += 8*/
        _0 = _size_ptr_16182;
        if (IS_ATOM_INT(_size_ptr_16182)) {
            _size_ptr_16182 = _size_ptr_16182 + 8;
            if ((long)((unsigned long)_size_ptr_16182 + (unsigned long)HIGH_BITS) >= 0) 
            _size_ptr_16182 = NewDouble((double)_size_ptr_16182);
        }
        else {
            _size_ptr_16182 = NewDouble(DBL_PTR(_size_ptr_16182)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _i_16198 = _i_16198 + 1;
        goto L2; // [483] 73
L3: 
        ;
    }
L1: 

    /** 	io:seek(current_db, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at492_16267);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at492_16267 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_492_16266 = machine(19, _seek_1__tmp_at492_16267);
    DeRefi(_seek_1__tmp_at492_16267);
    _seek_1__tmp_at492_16267 = NOVALUE;

    /** 	put4(n+4)*/
    if (IS_ATOM_INT(_n_16179)) {
        _9274 = _n_16179 + 4;
        if ((long)((unsigned long)_9274 + (unsigned long)HIGH_BITS) >= 0) 
        _9274 = NewDouble((double)_9274);
    }
    else {
        _9274 = NewDouble(DBL_PTR(_n_16179)->dbl + (double)4);
    }
    DeRef(_x_inlined_put4_at_511_16270);
    _x_inlined_put4_at_511_16270 = _9274;
    _9274 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_511_16270)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_511_16270;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_511_16270)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at514_16271);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at514_16271 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at514_16271); // DJP 

    /** end procedure*/
    goto LD; // [536] 539
LD: 
    DeRef(_x_inlined_put4_at_511_16270);
    _x_inlined_put4_at_511_16270 = NOVALUE;
    DeRefi(_put4_1__tmp_at514_16271);
    _put4_1__tmp_at514_16271 = NOVALUE;

    /** 	return io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_544_16273);
    _where_inlined_where_at_544_16273 = machine(20, _33current_db_15557);
    DeRef(_n_16179);
    DeRef(_free_list_16180);
    DeRef(_size_16181);
    DeRef(_size_ptr_16182);
    DeRef(_addr_16183);
    DeRef(_remaining_16185);
    return _where_inlined_where_at_544_16273;
    ;
}


void _33db_free(int _p_16276)
{
    int _psize_16277 = NOVALUE;
    int _i_16278 = NOVALUE;
    int _size_16279 = NOVALUE;
    int _addr_16280 = NOVALUE;
    int _free_list_16281 = NOVALUE;
    int _free_list_space_16282 = NOVALUE;
    int _new_space_16283 = NOVALUE;
    int _to_be_freed_16284 = NOVALUE;
    int _prev_addr_16285 = NOVALUE;
    int _prev_size_16286 = NOVALUE;
    int _free_count_16287 = NOVALUE;
    int _remaining_16288 = NOVALUE;
    int _seek_1__tmp_at11_16293 = NOVALUE;
    int _seek_inlined_seek_at_11_16292 = NOVALUE;
    int _pos_inlined_seek_at_8_16291 = NOVALUE;
    int _seek_1__tmp_at33_16297 = NOVALUE;
    int _seek_inlined_seek_at_33_16296 = NOVALUE;
    int _seek_1__tmp_at71_16304 = NOVALUE;
    int _seek_inlined_seek_at_71_16303 = NOVALUE;
    int _pos_inlined_seek_at_68_16302 = NOVALUE;
    int _seek_1__tmp_at135_16317 = NOVALUE;
    int _seek_inlined_seek_at_135_16316 = NOVALUE;
    int _seek_1__tmp_at161_16321 = NOVALUE;
    int _seek_inlined_seek_at_161_16320 = NOVALUE;
    int _put4_1__tmp_at176_16323 = NOVALUE;
    int _seek_1__tmp_at206_16326 = NOVALUE;
    int _seek_inlined_seek_at_206_16325 = NOVALUE;
    int _seek_1__tmp_at238_16331 = NOVALUE;
    int _seek_inlined_seek_at_238_16330 = NOVALUE;
    int _s_inlined_putn_at_278_16337 = NOVALUE;
    int _seek_1__tmp_at301_16340 = NOVALUE;
    int _seek_inlined_seek_at_301_16339 = NOVALUE;
    int _seek_1__tmp_at434_16361 = NOVALUE;
    int _seek_inlined_seek_at_434_16360 = NOVALUE;
    int _pos_inlined_seek_at_431_16359 = NOVALUE;
    int _put4_1__tmp_at486_16371 = NOVALUE;
    int _x_inlined_put4_at_483_16370 = NOVALUE;
    int _seek_1__tmp_at527_16377 = NOVALUE;
    int _seek_inlined_seek_at_527_16376 = NOVALUE;
    int _pos_inlined_seek_at_524_16375 = NOVALUE;
    int _seek_1__tmp_at578_16387 = NOVALUE;
    int _seek_inlined_seek_at_578_16386 = NOVALUE;
    int _pos_inlined_seek_at_575_16385 = NOVALUE;
    int _seek_1__tmp_at617_16392 = NOVALUE;
    int _seek_inlined_seek_at_617_16391 = NOVALUE;
    int _put4_1__tmp_at632_16394 = NOVALUE;
    int _put4_1__tmp_at670_16399 = NOVALUE;
    int _x_inlined_put4_at_667_16398 = NOVALUE;
    int _seek_1__tmp_at743_16411 = NOVALUE;
    int _seek_inlined_seek_at_743_16410 = NOVALUE;
    int _pos_inlined_seek_at_740_16409 = NOVALUE;
    int _put4_1__tmp_at758_16413 = NOVALUE;
    int _put4_1__tmp_at795_16417 = NOVALUE;
    int _x_inlined_put4_at_792_16416 = NOVALUE;
    int _seek_1__tmp_at843_16425 = NOVALUE;
    int _seek_inlined_seek_at_843_16424 = NOVALUE;
    int _pos_inlined_seek_at_840_16423 = NOVALUE;
    int _seek_1__tmp_at891_16433 = NOVALUE;
    int _seek_inlined_seek_at_891_16432 = NOVALUE;
    int _put4_1__tmp_at906_16435 = NOVALUE;
    int _seek_1__tmp_at951_16442 = NOVALUE;
    int _seek_inlined_seek_at_951_16441 = NOVALUE;
    int _pos_inlined_seek_at_948_16440 = NOVALUE;
    int _put4_1__tmp_at966_16444 = NOVALUE;
    int _put4_1__tmp_at994_16446 = NOVALUE;
    int _9343 = NOVALUE;
    int _9342 = NOVALUE;
    int _9341 = NOVALUE;
    int _9340 = NOVALUE;
    int _9337 = NOVALUE;
    int _9336 = NOVALUE;
    int _9335 = NOVALUE;
    int _9334 = NOVALUE;
    int _9333 = NOVALUE;
    int _9332 = NOVALUE;
    int _9331 = NOVALUE;
    int _9330 = NOVALUE;
    int _9329 = NOVALUE;
    int _9328 = NOVALUE;
    int _9327 = NOVALUE;
    int _9326 = NOVALUE;
    int _9325 = NOVALUE;
    int _9324 = NOVALUE;
    int _9323 = NOVALUE;
    int _9321 = NOVALUE;
    int _9320 = NOVALUE;
    int _9319 = NOVALUE;
    int _9317 = NOVALUE;
    int _9316 = NOVALUE;
    int _9315 = NOVALUE;
    int _9314 = NOVALUE;
    int _9313 = NOVALUE;
    int _9312 = NOVALUE;
    int _9311 = NOVALUE;
    int _9310 = NOVALUE;
    int _9309 = NOVALUE;
    int _9308 = NOVALUE;
    int _9307 = NOVALUE;
    int _9306 = NOVALUE;
    int _9305 = NOVALUE;
    int _9304 = NOVALUE;
    int _9303 = NOVALUE;
    int _9302 = NOVALUE;
    int _9301 = NOVALUE;
    int _9300 = NOVALUE;
    int _9294 = NOVALUE;
    int _9293 = NOVALUE;
    int _9292 = NOVALUE;
    int _9290 = NOVALUE;
    int _9286 = NOVALUE;
    int _9285 = NOVALUE;
    int _9283 = NOVALUE;
    int _9282 = NOVALUE;
    int _9280 = NOVALUE;
    int _9279 = NOVALUE;
    int _9275 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, p-4)*/
    if (IS_ATOM_INT(_p_16276)) {
        _9275 = _p_16276 - 4;
        if ((long)((unsigned long)_9275 +(unsigned long) HIGH_BITS) >= 0){
            _9275 = NewDouble((double)_9275);
        }
    }
    else {
        _9275 = NewDouble(DBL_PTR(_p_16276)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_16291);
    _pos_inlined_seek_at_8_16291 = _9275;
    _9275 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_16291);
    DeRef(_seek_1__tmp_at11_16293);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_16291;
    _seek_1__tmp_at11_16293 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_16292 = machine(19, _seek_1__tmp_at11_16293);
    DeRef(_pos_inlined_seek_at_8_16291);
    _pos_inlined_seek_at_8_16291 = NOVALUE;
    DeRef(_seek_1__tmp_at11_16293);
    _seek_1__tmp_at11_16293 = NOVALUE;

    /** 	psize = get4()*/
    _0 = _psize_16277;
    _psize_16277 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at33_16297);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at33_16297 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_33_16296 = machine(19, _seek_1__tmp_at33_16297);
    DeRefi(_seek_1__tmp_at33_16297);
    _seek_1__tmp_at33_16297 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_16287 = _33get4();
    if (!IS_ATOM_INT(_free_count_16287)) {
        _1 = (long)(DBL_PTR(_free_count_16287)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_16287)) && (DBL_PTR(_free_count_16287)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_16287);
        _free_count_16287 = _1;
    }

    /** 	free_list = get4()*/
    _0 = _free_list_16281;
    _free_list_16281 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, free_list - 4)*/
    if (IS_ATOM_INT(_free_list_16281)) {
        _9279 = _free_list_16281 - 4;
        if ((long)((unsigned long)_9279 +(unsigned long) HIGH_BITS) >= 0){
            _9279 = NewDouble((double)_9279);
        }
    }
    else {
        _9279 = NewDouble(DBL_PTR(_free_list_16281)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_68_16302);
    _pos_inlined_seek_at_68_16302 = _9279;
    _9279 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_68_16302);
    DeRef(_seek_1__tmp_at71_16304);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_68_16302;
    _seek_1__tmp_at71_16304 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_71_16303 = machine(19, _seek_1__tmp_at71_16304);
    DeRef(_pos_inlined_seek_at_68_16302);
    _pos_inlined_seek_at_68_16302 = NOVALUE;
    DeRef(_seek_1__tmp_at71_16304);
    _seek_1__tmp_at71_16304 = NOVALUE;

    /** 	free_list_space = get4()-4*/
    _9280 = _33get4();
    DeRef(_free_list_space_16282);
    if (IS_ATOM_INT(_9280)) {
        _free_list_space_16282 = _9280 - 4;
        if ((long)((unsigned long)_free_list_space_16282 +(unsigned long) HIGH_BITS) >= 0){
            _free_list_space_16282 = NewDouble((double)_free_list_space_16282);
        }
    }
    else {
        _free_list_space_16282 = binary_op(MINUS, _9280, 4);
    }
    DeRef(_9280);
    _9280 = NOVALUE;

    /** 	if free_list_space < 8 * (free_count+1) then*/
    _9282 = _free_count_16287 + 1;
    if (_9282 > MAXINT){
        _9282 = NewDouble((double)_9282);
    }
    if (IS_ATOM_INT(_9282)) {
        if (_9282 <= INT15 && _9282 >= -INT15)
        _9283 = 8 * _9282;
        else
        _9283 = NewDouble(8 * (double)_9282);
    }
    else {
        _9283 = NewDouble((double)8 * DBL_PTR(_9282)->dbl);
    }
    DeRef(_9282);
    _9282 = NOVALUE;
    if (binary_op_a(GREATEREQ, _free_list_space_16282, _9283)){
        DeRef(_9283);
        _9283 = NOVALUE;
        goto L1; // [104] 318
    }
    DeRef(_9283);
    _9283 = NOVALUE;

    /** 		new_space = floor(free_list_space + free_list_space / 2)*/
    if (IS_ATOM_INT(_free_list_space_16282)) {
        if (_free_list_space_16282 & 1) {
            _9285 = NewDouble((_free_list_space_16282 >> 1) + 0.5);
        }
        else
        _9285 = _free_list_space_16282 >> 1;
    }
    else {
        _9285 = binary_op(DIVIDE, _free_list_space_16282, 2);
    }
    if (IS_ATOM_INT(_free_list_space_16282) && IS_ATOM_INT(_9285)) {
        _9286 = _free_list_space_16282 + _9285;
        if ((long)((unsigned long)_9286 + (unsigned long)HIGH_BITS) >= 0) 
        _9286 = NewDouble((double)_9286);
    }
    else {
        if (IS_ATOM_INT(_free_list_space_16282)) {
            _9286 = NewDouble((double)_free_list_space_16282 + DBL_PTR(_9285)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9285)) {
                _9286 = NewDouble(DBL_PTR(_free_list_space_16282)->dbl + (double)_9285);
            }
            else
            _9286 = NewDouble(DBL_PTR(_free_list_space_16282)->dbl + DBL_PTR(_9285)->dbl);
        }
    }
    DeRef(_9285);
    _9285 = NOVALUE;
    DeRef(_new_space_16283);
    if (IS_ATOM_INT(_9286))
    _new_space_16283 = e_floor(_9286);
    else
    _new_space_16283 = unary_op(FLOOR, _9286);
    DeRef(_9286);
    _9286 = NOVALUE;

    /** 		to_be_freed = free_list*/
    Ref(_free_list_16281);
    DeRef(_to_be_freed_16284);
    _to_be_freed_16284 = _free_list_16281;

    /** 		free_list = db_allocate(new_space)*/
    Ref(_new_space_16283);
    _0 = _free_list_16281;
    _free_list_16281 = _33db_allocate(_new_space_16283);
    DeRef(_0);

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at135_16317);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at135_16317 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_135_16316 = machine(19, _seek_1__tmp_at135_16317);
    DeRefi(_seek_1__tmp_at135_16317);
    _seek_1__tmp_at135_16317 = NOVALUE;

    /** 		free_count = get4() -- db_allocate may have changed it*/
    _free_count_16287 = _33get4();
    if (!IS_ATOM_INT(_free_count_16287)) {
        _1 = (long)(DBL_PTR(_free_count_16287)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_16287)) && (DBL_PTR(_free_count_16287)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_16287);
        _free_count_16287 = _1;
    }

    /** 		io:seek(current_db, FREE_LIST)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at161_16321);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 11;
    _seek_1__tmp_at161_16321 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_161_16320 = machine(19, _seek_1__tmp_at161_16321);
    DeRefi(_seek_1__tmp_at161_16321);
    _seek_1__tmp_at161_16321 = NOVALUE;

    /** 		put4(free_list)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_free_list_16281)) {
        *poke4_addr = (unsigned long)_free_list_16281;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_free_list_16281)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at176_16323);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at176_16323 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at176_16323); // DJP 

    /** end procedure*/
    goto L2; // [198] 201
L2: 
    DeRefi(_put4_1__tmp_at176_16323);
    _put4_1__tmp_at176_16323 = NOVALUE;

    /** 		io:seek(current_db, to_be_freed)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_to_be_freed_16284);
    DeRef(_seek_1__tmp_at206_16326);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _to_be_freed_16284;
    _seek_1__tmp_at206_16326 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_206_16325 = machine(19, _seek_1__tmp_at206_16326);
    DeRef(_seek_1__tmp_at206_16326);
    _seek_1__tmp_at206_16326 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, 8*free_count)*/
    if (_free_count_16287 <= INT15 && _free_count_16287 >= -INT15)
    _9290 = 8 * _free_count_16287;
    else
    _9290 = NewDouble(8 * (double)_free_count_16287);
    _0 = _remaining_16288;
    _remaining_16288 = _15get_bytes(_33current_db_15557, _9290);
    DeRef(_0);
    _9290 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_16281);
    DeRef(_seek_1__tmp_at238_16331);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _free_list_16281;
    _seek_1__tmp_at238_16331 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_238_16330 = machine(19, _seek_1__tmp_at238_16331);
    DeRef(_seek_1__tmp_at238_16331);
    _seek_1__tmp_at238_16331 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_16288); // DJP 

    /** end procedure*/
    goto L3; // [263] 266
L3: 

    /** 		putn(repeat(0, new_space-length(remaining)))*/
    if (IS_SEQUENCE(_remaining_16288)){
            _9292 = SEQ_PTR(_remaining_16288)->length;
    }
    else {
        _9292 = 1;
    }
    if (IS_ATOM_INT(_new_space_16283)) {
        _9293 = _new_space_16283 - _9292;
    }
    else {
        _9293 = NewDouble(DBL_PTR(_new_space_16283)->dbl - (double)_9292);
    }
    _9292 = NOVALUE;
    _9294 = Repeat(0, _9293);
    DeRef(_9293);
    _9293 = NOVALUE;
    DeRefi(_s_inlined_putn_at_278_16337);
    _s_inlined_putn_at_278_16337 = _9294;
    _9294 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_278_16337); // DJP 

    /** end procedure*/
    goto L4; // [293] 296
L4: 
    DeRefi(_s_inlined_putn_at_278_16337);
    _s_inlined_putn_at_278_16337 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_16281);
    DeRef(_seek_1__tmp_at301_16340);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _free_list_16281;
    _seek_1__tmp_at301_16340 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_301_16339 = machine(19, _seek_1__tmp_at301_16340);
    DeRef(_seek_1__tmp_at301_16340);
    _seek_1__tmp_at301_16340 = NOVALUE;
    goto L5; // [315] 324
L1: 

    /** 		new_space = 0*/
    DeRef(_new_space_16283);
    _new_space_16283 = 0;
L5: 

    /** 	i = 1*/
    DeRef(_i_16278);
    _i_16278 = 1;

    /** 	prev_addr = 0*/
    DeRef(_prev_addr_16285);
    _prev_addr_16285 = 0;

    /** 	prev_size = 0*/
    DeRef(_prev_size_16286);
    _prev_size_16286 = 0;

    /** 	while i <= free_count do*/
L6: 
    if (binary_op_a(GREATER, _i_16278, _free_count_16287)){
        goto L7; // [344] 390
    }

    /** 		addr = get4()*/
    _0 = _addr_16280;
    _addr_16280 = _33get4();
    DeRef(_0);

    /** 		size = get4()*/
    _0 = _size_16279;
    _size_16279 = _33get4();
    DeRef(_0);

    /** 		if p < addr then*/
    if (binary_op_a(GREATEREQ, _p_16276, _addr_16280)){
        goto L8; // [360] 369
    }

    /** 			exit*/
    goto L7; // [366] 390
L8: 

    /** 		prev_addr = addr*/
    Ref(_addr_16280);
    DeRef(_prev_addr_16285);
    _prev_addr_16285 = _addr_16280;

    /** 		prev_size = size*/
    Ref(_size_16279);
    DeRef(_prev_size_16286);
    _prev_size_16286 = _size_16279;

    /** 		i += 1*/
    _0 = _i_16278;
    if (IS_ATOM_INT(_i_16278)) {
        _i_16278 = _i_16278 + 1;
        if (_i_16278 > MAXINT){
            _i_16278 = NewDouble((double)_i_16278);
        }
    }
    else
    _i_16278 = binary_op(PLUS, 1, _i_16278);
    DeRef(_0);

    /** 	end while*/
    goto L6; // [387] 344
L7: 

    /** 	if i > 1 and prev_addr + prev_size = p then*/
    if (IS_ATOM_INT(_i_16278)) {
        _9300 = (_i_16278 > 1);
    }
    else {
        _9300 = (DBL_PTR(_i_16278)->dbl > (double)1);
    }
    if (_9300 == 0) {
        goto L9; // [396] 701
    }
    if (IS_ATOM_INT(_prev_addr_16285) && IS_ATOM_INT(_prev_size_16286)) {
        _9302 = _prev_addr_16285 + _prev_size_16286;
        if ((long)((unsigned long)_9302 + (unsigned long)HIGH_BITS) >= 0) 
        _9302 = NewDouble((double)_9302);
    }
    else {
        if (IS_ATOM_INT(_prev_addr_16285)) {
            _9302 = NewDouble((double)_prev_addr_16285 + DBL_PTR(_prev_size_16286)->dbl);
        }
        else {
            if (IS_ATOM_INT(_prev_size_16286)) {
                _9302 = NewDouble(DBL_PTR(_prev_addr_16285)->dbl + (double)_prev_size_16286);
            }
            else
            _9302 = NewDouble(DBL_PTR(_prev_addr_16285)->dbl + DBL_PTR(_prev_size_16286)->dbl);
        }
    }
    if (IS_ATOM_INT(_9302) && IS_ATOM_INT(_p_16276)) {
        _9303 = (_9302 == _p_16276);
    }
    else {
        if (IS_ATOM_INT(_9302)) {
            _9303 = ((double)_9302 == DBL_PTR(_p_16276)->dbl);
        }
        else {
            if (IS_ATOM_INT(_p_16276)) {
                _9303 = (DBL_PTR(_9302)->dbl == (double)_p_16276);
            }
            else
            _9303 = (DBL_PTR(_9302)->dbl == DBL_PTR(_p_16276)->dbl);
        }
    }
    DeRef(_9302);
    _9302 = NOVALUE;
    if (_9303 == 0)
    {
        DeRef(_9303);
        _9303 = NOVALUE;
        goto L9; // [409] 701
    }
    else{
        DeRef(_9303);
        _9303 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-2)*8+4)*/
    if (IS_ATOM_INT(_i_16278)) {
        _9304 = _i_16278 - 2;
        if ((long)((unsigned long)_9304 +(unsigned long) HIGH_BITS) >= 0){
            _9304 = NewDouble((double)_9304);
        }
    }
    else {
        _9304 = NewDouble(DBL_PTR(_i_16278)->dbl - (double)2);
    }
    if (IS_ATOM_INT(_9304)) {
        if (_9304 == (short)_9304)
        _9305 = _9304 * 8;
        else
        _9305 = NewDouble(_9304 * (double)8);
    }
    else {
        _9305 = NewDouble(DBL_PTR(_9304)->dbl * (double)8);
    }
    DeRef(_9304);
    _9304 = NOVALUE;
    if (IS_ATOM_INT(_free_list_16281) && IS_ATOM_INT(_9305)) {
        _9306 = _free_list_16281 + _9305;
        if ((long)((unsigned long)_9306 + (unsigned long)HIGH_BITS) >= 0) 
        _9306 = NewDouble((double)_9306);
    }
    else {
        if (IS_ATOM_INT(_free_list_16281)) {
            _9306 = NewDouble((double)_free_list_16281 + DBL_PTR(_9305)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9305)) {
                _9306 = NewDouble(DBL_PTR(_free_list_16281)->dbl + (double)_9305);
            }
            else
            _9306 = NewDouble(DBL_PTR(_free_list_16281)->dbl + DBL_PTR(_9305)->dbl);
        }
    }
    DeRef(_9305);
    _9305 = NOVALUE;
    if (IS_ATOM_INT(_9306)) {
        _9307 = _9306 + 4;
        if ((long)((unsigned long)_9307 + (unsigned long)HIGH_BITS) >= 0) 
        _9307 = NewDouble((double)_9307);
    }
    else {
        _9307 = NewDouble(DBL_PTR(_9306)->dbl + (double)4);
    }
    DeRef(_9306);
    _9306 = NOVALUE;
    DeRef(_pos_inlined_seek_at_431_16359);
    _pos_inlined_seek_at_431_16359 = _9307;
    _9307 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_431_16359);
    DeRef(_seek_1__tmp_at434_16361);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_431_16359;
    _seek_1__tmp_at434_16361 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_434_16360 = machine(19, _seek_1__tmp_at434_16361);
    DeRef(_pos_inlined_seek_at_431_16359);
    _pos_inlined_seek_at_431_16359 = NOVALUE;
    DeRef(_seek_1__tmp_at434_16361);
    _seek_1__tmp_at434_16361 = NOVALUE;

    /** 		if i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_16278)) {
        _9308 = (_i_16278 < _free_count_16287);
    }
    else {
        _9308 = (DBL_PTR(_i_16278)->dbl < (double)_free_count_16287);
    }
    if (_9308 == 0) {
        goto LA; // [454] 662
    }
    if (IS_ATOM_INT(_p_16276) && IS_ATOM_INT(_psize_16277)) {
        _9310 = _p_16276 + _psize_16277;
        if ((long)((unsigned long)_9310 + (unsigned long)HIGH_BITS) >= 0) 
        _9310 = NewDouble((double)_9310);
    }
    else {
        if (IS_ATOM_INT(_p_16276)) {
            _9310 = NewDouble((double)_p_16276 + DBL_PTR(_psize_16277)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_16277)) {
                _9310 = NewDouble(DBL_PTR(_p_16276)->dbl + (double)_psize_16277);
            }
            else
            _9310 = NewDouble(DBL_PTR(_p_16276)->dbl + DBL_PTR(_psize_16277)->dbl);
        }
    }
    if (IS_ATOM_INT(_9310) && IS_ATOM_INT(_addr_16280)) {
        _9311 = (_9310 == _addr_16280);
    }
    else {
        if (IS_ATOM_INT(_9310)) {
            _9311 = ((double)_9310 == DBL_PTR(_addr_16280)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_16280)) {
                _9311 = (DBL_PTR(_9310)->dbl == (double)_addr_16280);
            }
            else
            _9311 = (DBL_PTR(_9310)->dbl == DBL_PTR(_addr_16280)->dbl);
        }
    }
    DeRef(_9310);
    _9310 = NOVALUE;
    if (_9311 == 0)
    {
        DeRef(_9311);
        _9311 = NOVALUE;
        goto LA; // [469] 662
    }
    else{
        DeRef(_9311);
        _9311 = NOVALUE;
    }

    /** 			put4(prev_size+psize+size) -- update size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_16286) && IS_ATOM_INT(_psize_16277)) {
        _9312 = _prev_size_16286 + _psize_16277;
        if ((long)((unsigned long)_9312 + (unsigned long)HIGH_BITS) >= 0) 
        _9312 = NewDouble((double)_9312);
    }
    else {
        if (IS_ATOM_INT(_prev_size_16286)) {
            _9312 = NewDouble((double)_prev_size_16286 + DBL_PTR(_psize_16277)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_16277)) {
                _9312 = NewDouble(DBL_PTR(_prev_size_16286)->dbl + (double)_psize_16277);
            }
            else
            _9312 = NewDouble(DBL_PTR(_prev_size_16286)->dbl + DBL_PTR(_psize_16277)->dbl);
        }
    }
    if (IS_ATOM_INT(_9312) && IS_ATOM_INT(_size_16279)) {
        _9313 = _9312 + _size_16279;
        if ((long)((unsigned long)_9313 + (unsigned long)HIGH_BITS) >= 0) 
        _9313 = NewDouble((double)_9313);
    }
    else {
        if (IS_ATOM_INT(_9312)) {
            _9313 = NewDouble((double)_9312 + DBL_PTR(_size_16279)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_16279)) {
                _9313 = NewDouble(DBL_PTR(_9312)->dbl + (double)_size_16279);
            }
            else
            _9313 = NewDouble(DBL_PTR(_9312)->dbl + DBL_PTR(_size_16279)->dbl);
        }
    }
    DeRef(_9312);
    _9312 = NOVALUE;
    DeRef(_x_inlined_put4_at_483_16370);
    _x_inlined_put4_at_483_16370 = _9313;
    _9313 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_483_16370)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_483_16370;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_483_16370)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at486_16371);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at486_16371 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at486_16371); // DJP 

    /** end procedure*/
    goto LB; // [508] 511
LB: 
    DeRef(_x_inlined_put4_at_483_16370);
    _x_inlined_put4_at_483_16370 = NOVALUE;
    DeRefi(_put4_1__tmp_at486_16371);
    _put4_1__tmp_at486_16371 = NOVALUE;

    /** 			io:seek(current_db, free_list+i*8)*/
    if (IS_ATOM_INT(_i_16278)) {
        if (_i_16278 == (short)_i_16278)
        _9314 = _i_16278 * 8;
        else
        _9314 = NewDouble(_i_16278 * (double)8);
    }
    else {
        _9314 = NewDouble(DBL_PTR(_i_16278)->dbl * (double)8);
    }
    if (IS_ATOM_INT(_free_list_16281) && IS_ATOM_INT(_9314)) {
        _9315 = _free_list_16281 + _9314;
        if ((long)((unsigned long)_9315 + (unsigned long)HIGH_BITS) >= 0) 
        _9315 = NewDouble((double)_9315);
    }
    else {
        if (IS_ATOM_INT(_free_list_16281)) {
            _9315 = NewDouble((double)_free_list_16281 + DBL_PTR(_9314)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9314)) {
                _9315 = NewDouble(DBL_PTR(_free_list_16281)->dbl + (double)_9314);
            }
            else
            _9315 = NewDouble(DBL_PTR(_free_list_16281)->dbl + DBL_PTR(_9314)->dbl);
        }
    }
    DeRef(_9314);
    _9314 = NOVALUE;
    DeRef(_pos_inlined_seek_at_524_16375);
    _pos_inlined_seek_at_524_16375 = _9315;
    _9315 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_524_16375);
    DeRef(_seek_1__tmp_at527_16377);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_524_16375;
    _seek_1__tmp_at527_16377 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_527_16376 = machine(19, _seek_1__tmp_at527_16377);
    DeRef(_pos_inlined_seek_at_524_16375);
    _pos_inlined_seek_at_524_16375 = NOVALUE;
    DeRef(_seek_1__tmp_at527_16377);
    _seek_1__tmp_at527_16377 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, (free_count-i)*8)*/
    if (IS_ATOM_INT(_i_16278)) {
        _9316 = _free_count_16287 - _i_16278;
        if ((long)((unsigned long)_9316 +(unsigned long) HIGH_BITS) >= 0){
            _9316 = NewDouble((double)_9316);
        }
    }
    else {
        _9316 = NewDouble((double)_free_count_16287 - DBL_PTR(_i_16278)->dbl);
    }
    if (IS_ATOM_INT(_9316)) {
        if (_9316 == (short)_9316)
        _9317 = _9316 * 8;
        else
        _9317 = NewDouble(_9316 * (double)8);
    }
    else {
        _9317 = NewDouble(DBL_PTR(_9316)->dbl * (double)8);
    }
    DeRef(_9316);
    _9316 = NOVALUE;
    _0 = _remaining_16288;
    _remaining_16288 = _15get_bytes(_33current_db_15557, _9317);
    DeRef(_0);
    _9317 = NOVALUE;

    /** 			io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_16278)) {
        _9319 = _i_16278 - 1;
        if ((long)((unsigned long)_9319 +(unsigned long) HIGH_BITS) >= 0){
            _9319 = NewDouble((double)_9319);
        }
    }
    else {
        _9319 = NewDouble(DBL_PTR(_i_16278)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9319)) {
        if (_9319 == (short)_9319)
        _9320 = _9319 * 8;
        else
        _9320 = NewDouble(_9319 * (double)8);
    }
    else {
        _9320 = NewDouble(DBL_PTR(_9319)->dbl * (double)8);
    }
    DeRef(_9319);
    _9319 = NOVALUE;
    if (IS_ATOM_INT(_free_list_16281) && IS_ATOM_INT(_9320)) {
        _9321 = _free_list_16281 + _9320;
        if ((long)((unsigned long)_9321 + (unsigned long)HIGH_BITS) >= 0) 
        _9321 = NewDouble((double)_9321);
    }
    else {
        if (IS_ATOM_INT(_free_list_16281)) {
            _9321 = NewDouble((double)_free_list_16281 + DBL_PTR(_9320)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9320)) {
                _9321 = NewDouble(DBL_PTR(_free_list_16281)->dbl + (double)_9320);
            }
            else
            _9321 = NewDouble(DBL_PTR(_free_list_16281)->dbl + DBL_PTR(_9320)->dbl);
        }
    }
    DeRef(_9320);
    _9320 = NOVALUE;
    DeRef(_pos_inlined_seek_at_575_16385);
    _pos_inlined_seek_at_575_16385 = _9321;
    _9321 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_575_16385);
    DeRef(_seek_1__tmp_at578_16387);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_575_16385;
    _seek_1__tmp_at578_16387 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_578_16386 = machine(19, _seek_1__tmp_at578_16387);
    DeRef(_pos_inlined_seek_at_575_16385);
    _pos_inlined_seek_at_575_16385 = NOVALUE;
    DeRef(_seek_1__tmp_at578_16387);
    _seek_1__tmp_at578_16387 = NOVALUE;

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_16288); // DJP 

    /** end procedure*/
    goto LC; // [603] 606
LC: 

    /** 			free_count -= 1*/
    _free_count_16287 = _free_count_16287 - 1;

    /** 			io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at617_16392);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at617_16392 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_617_16391 = machine(19, _seek_1__tmp_at617_16392);
    DeRefi(_seek_1__tmp_at617_16392);
    _seek_1__tmp_at617_16392 = NOVALUE;

    /** 			put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_16287;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at632_16394);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at632_16394 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at632_16394); // DJP 

    /** end procedure*/
    goto LD; // [654] 657
LD: 
    DeRefi(_put4_1__tmp_at632_16394);
    _put4_1__tmp_at632_16394 = NOVALUE;
    goto LE; // [659] 1036
LA: 

    /** 			put4(prev_size+psize) -- increase previous size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_16286) && IS_ATOM_INT(_psize_16277)) {
        _9323 = _prev_size_16286 + _psize_16277;
        if ((long)((unsigned long)_9323 + (unsigned long)HIGH_BITS) >= 0) 
        _9323 = NewDouble((double)_9323);
    }
    else {
        if (IS_ATOM_INT(_prev_size_16286)) {
            _9323 = NewDouble((double)_prev_size_16286 + DBL_PTR(_psize_16277)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_16277)) {
                _9323 = NewDouble(DBL_PTR(_prev_size_16286)->dbl + (double)_psize_16277);
            }
            else
            _9323 = NewDouble(DBL_PTR(_prev_size_16286)->dbl + DBL_PTR(_psize_16277)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_667_16398);
    _x_inlined_put4_at_667_16398 = _9323;
    _9323 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_667_16398)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_667_16398;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_667_16398)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at670_16399);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at670_16399 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at670_16399); // DJP 

    /** end procedure*/
    goto LF; // [692] 695
LF: 
    DeRef(_x_inlined_put4_at_667_16398);
    _x_inlined_put4_at_667_16398 = NOVALUE;
    DeRefi(_put4_1__tmp_at670_16399);
    _put4_1__tmp_at670_16399 = NOVALUE;
    goto LE; // [698] 1036
L9: 

    /** 	elsif i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_16278)) {
        _9324 = (_i_16278 < _free_count_16287);
    }
    else {
        _9324 = (DBL_PTR(_i_16278)->dbl < (double)_free_count_16287);
    }
    if (_9324 == 0) {
        goto L10; // [707] 825
    }
    if (IS_ATOM_INT(_p_16276) && IS_ATOM_INT(_psize_16277)) {
        _9326 = _p_16276 + _psize_16277;
        if ((long)((unsigned long)_9326 + (unsigned long)HIGH_BITS) >= 0) 
        _9326 = NewDouble((double)_9326);
    }
    else {
        if (IS_ATOM_INT(_p_16276)) {
            _9326 = NewDouble((double)_p_16276 + DBL_PTR(_psize_16277)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_16277)) {
                _9326 = NewDouble(DBL_PTR(_p_16276)->dbl + (double)_psize_16277);
            }
            else
            _9326 = NewDouble(DBL_PTR(_p_16276)->dbl + DBL_PTR(_psize_16277)->dbl);
        }
    }
    if (IS_ATOM_INT(_9326) && IS_ATOM_INT(_addr_16280)) {
        _9327 = (_9326 == _addr_16280);
    }
    else {
        if (IS_ATOM_INT(_9326)) {
            _9327 = ((double)_9326 == DBL_PTR(_addr_16280)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_16280)) {
                _9327 = (DBL_PTR(_9326)->dbl == (double)_addr_16280);
            }
            else
            _9327 = (DBL_PTR(_9326)->dbl == DBL_PTR(_addr_16280)->dbl);
        }
    }
    DeRef(_9326);
    _9326 = NOVALUE;
    if (_9327 == 0)
    {
        DeRef(_9327);
        _9327 = NOVALUE;
        goto L10; // [722] 825
    }
    else{
        DeRef(_9327);
        _9327 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_16278)) {
        _9328 = _i_16278 - 1;
        if ((long)((unsigned long)_9328 +(unsigned long) HIGH_BITS) >= 0){
            _9328 = NewDouble((double)_9328);
        }
    }
    else {
        _9328 = NewDouble(DBL_PTR(_i_16278)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9328)) {
        if (_9328 == (short)_9328)
        _9329 = _9328 * 8;
        else
        _9329 = NewDouble(_9328 * (double)8);
    }
    else {
        _9329 = NewDouble(DBL_PTR(_9328)->dbl * (double)8);
    }
    DeRef(_9328);
    _9328 = NOVALUE;
    if (IS_ATOM_INT(_free_list_16281) && IS_ATOM_INT(_9329)) {
        _9330 = _free_list_16281 + _9329;
        if ((long)((unsigned long)_9330 + (unsigned long)HIGH_BITS) >= 0) 
        _9330 = NewDouble((double)_9330);
    }
    else {
        if (IS_ATOM_INT(_free_list_16281)) {
            _9330 = NewDouble((double)_free_list_16281 + DBL_PTR(_9329)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9329)) {
                _9330 = NewDouble(DBL_PTR(_free_list_16281)->dbl + (double)_9329);
            }
            else
            _9330 = NewDouble(DBL_PTR(_free_list_16281)->dbl + DBL_PTR(_9329)->dbl);
        }
    }
    DeRef(_9329);
    _9329 = NOVALUE;
    DeRef(_pos_inlined_seek_at_740_16409);
    _pos_inlined_seek_at_740_16409 = _9330;
    _9330 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_740_16409);
    DeRef(_seek_1__tmp_at743_16411);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_740_16409;
    _seek_1__tmp_at743_16411 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_743_16410 = machine(19, _seek_1__tmp_at743_16411);
    DeRef(_pos_inlined_seek_at_740_16409);
    _pos_inlined_seek_at_740_16409 = NOVALUE;
    DeRef(_seek_1__tmp_at743_16411);
    _seek_1__tmp_at743_16411 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_p_16276)) {
        *poke4_addr = (unsigned long)_p_16276;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_16276)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at758_16413);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at758_16413 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at758_16413); // DJP 

    /** end procedure*/
    goto L11; // [780] 783
L11: 
    DeRefi(_put4_1__tmp_at758_16413);
    _put4_1__tmp_at758_16413 = NOVALUE;

    /** 		put4(psize+size)*/
    if (IS_ATOM_INT(_psize_16277) && IS_ATOM_INT(_size_16279)) {
        _9331 = _psize_16277 + _size_16279;
        if ((long)((unsigned long)_9331 + (unsigned long)HIGH_BITS) >= 0) 
        _9331 = NewDouble((double)_9331);
    }
    else {
        if (IS_ATOM_INT(_psize_16277)) {
            _9331 = NewDouble((double)_psize_16277 + DBL_PTR(_size_16279)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_16279)) {
                _9331 = NewDouble(DBL_PTR(_psize_16277)->dbl + (double)_size_16279);
            }
            else
            _9331 = NewDouble(DBL_PTR(_psize_16277)->dbl + DBL_PTR(_size_16279)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_792_16416);
    _x_inlined_put4_at_792_16416 = _9331;
    _9331 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_792_16416)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_792_16416;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_792_16416)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at795_16417);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at795_16417 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at795_16417); // DJP 

    /** end procedure*/
    goto L12; // [817] 820
L12: 
    DeRef(_x_inlined_put4_at_792_16416);
    _x_inlined_put4_at_792_16416 = NOVALUE;
    DeRefi(_put4_1__tmp_at795_16417);
    _put4_1__tmp_at795_16417 = NOVALUE;
    goto LE; // [822] 1036
L10: 

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_16278)) {
        _9332 = _i_16278 - 1;
        if ((long)((unsigned long)_9332 +(unsigned long) HIGH_BITS) >= 0){
            _9332 = NewDouble((double)_9332);
        }
    }
    else {
        _9332 = NewDouble(DBL_PTR(_i_16278)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9332)) {
        if (_9332 == (short)_9332)
        _9333 = _9332 * 8;
        else
        _9333 = NewDouble(_9332 * (double)8);
    }
    else {
        _9333 = NewDouble(DBL_PTR(_9332)->dbl * (double)8);
    }
    DeRef(_9332);
    _9332 = NOVALUE;
    if (IS_ATOM_INT(_free_list_16281) && IS_ATOM_INT(_9333)) {
        _9334 = _free_list_16281 + _9333;
        if ((long)((unsigned long)_9334 + (unsigned long)HIGH_BITS) >= 0) 
        _9334 = NewDouble((double)_9334);
    }
    else {
        if (IS_ATOM_INT(_free_list_16281)) {
            _9334 = NewDouble((double)_free_list_16281 + DBL_PTR(_9333)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9333)) {
                _9334 = NewDouble(DBL_PTR(_free_list_16281)->dbl + (double)_9333);
            }
            else
            _9334 = NewDouble(DBL_PTR(_free_list_16281)->dbl + DBL_PTR(_9333)->dbl);
        }
    }
    DeRef(_9333);
    _9333 = NOVALUE;
    DeRef(_pos_inlined_seek_at_840_16423);
    _pos_inlined_seek_at_840_16423 = _9334;
    _9334 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_840_16423);
    DeRef(_seek_1__tmp_at843_16425);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_840_16423;
    _seek_1__tmp_at843_16425 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_843_16424 = machine(19, _seek_1__tmp_at843_16425);
    DeRef(_pos_inlined_seek_at_840_16423);
    _pos_inlined_seek_at_840_16423 = NOVALUE;
    DeRef(_seek_1__tmp_at843_16425);
    _seek_1__tmp_at843_16425 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (free_count-i+1)*8)*/
    if (IS_ATOM_INT(_i_16278)) {
        _9335 = _free_count_16287 - _i_16278;
        if ((long)((unsigned long)_9335 +(unsigned long) HIGH_BITS) >= 0){
            _9335 = NewDouble((double)_9335);
        }
    }
    else {
        _9335 = NewDouble((double)_free_count_16287 - DBL_PTR(_i_16278)->dbl);
    }
    if (IS_ATOM_INT(_9335)) {
        _9336 = _9335 + 1;
        if (_9336 > MAXINT){
            _9336 = NewDouble((double)_9336);
        }
    }
    else
    _9336 = binary_op(PLUS, 1, _9335);
    DeRef(_9335);
    _9335 = NOVALUE;
    if (IS_ATOM_INT(_9336)) {
        if (_9336 == (short)_9336)
        _9337 = _9336 * 8;
        else
        _9337 = NewDouble(_9336 * (double)8);
    }
    else {
        _9337 = NewDouble(DBL_PTR(_9336)->dbl * (double)8);
    }
    DeRef(_9336);
    _9336 = NOVALUE;
    _0 = _remaining_16288;
    _remaining_16288 = _15get_bytes(_33current_db_15557, _9337);
    DeRef(_0);
    _9337 = NOVALUE;

    /** 		free_count += 1*/
    _free_count_16287 = _free_count_16287 + 1;

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at891_16433);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at891_16433 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_891_16432 = machine(19, _seek_1__tmp_at891_16433);
    DeRefi(_seek_1__tmp_at891_16433);
    _seek_1__tmp_at891_16433 = NOVALUE;

    /** 		put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_16287;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at906_16435);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at906_16435 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at906_16435); // DJP 

    /** end procedure*/
    goto L13; // [928] 931
L13: 
    DeRefi(_put4_1__tmp_at906_16435);
    _put4_1__tmp_at906_16435 = NOVALUE;

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_16278)) {
        _9340 = _i_16278 - 1;
        if ((long)((unsigned long)_9340 +(unsigned long) HIGH_BITS) >= 0){
            _9340 = NewDouble((double)_9340);
        }
    }
    else {
        _9340 = NewDouble(DBL_PTR(_i_16278)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9340)) {
        if (_9340 == (short)_9340)
        _9341 = _9340 * 8;
        else
        _9341 = NewDouble(_9340 * (double)8);
    }
    else {
        _9341 = NewDouble(DBL_PTR(_9340)->dbl * (double)8);
    }
    DeRef(_9340);
    _9340 = NOVALUE;
    if (IS_ATOM_INT(_free_list_16281) && IS_ATOM_INT(_9341)) {
        _9342 = _free_list_16281 + _9341;
        if ((long)((unsigned long)_9342 + (unsigned long)HIGH_BITS) >= 0) 
        _9342 = NewDouble((double)_9342);
    }
    else {
        if (IS_ATOM_INT(_free_list_16281)) {
            _9342 = NewDouble((double)_free_list_16281 + DBL_PTR(_9341)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9341)) {
                _9342 = NewDouble(DBL_PTR(_free_list_16281)->dbl + (double)_9341);
            }
            else
            _9342 = NewDouble(DBL_PTR(_free_list_16281)->dbl + DBL_PTR(_9341)->dbl);
        }
    }
    DeRef(_9341);
    _9341 = NOVALUE;
    DeRef(_pos_inlined_seek_at_948_16440);
    _pos_inlined_seek_at_948_16440 = _9342;
    _9342 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_948_16440);
    DeRef(_seek_1__tmp_at951_16442);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_948_16440;
    _seek_1__tmp_at951_16442 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_951_16441 = machine(19, _seek_1__tmp_at951_16442);
    DeRef(_pos_inlined_seek_at_948_16440);
    _pos_inlined_seek_at_948_16440 = NOVALUE;
    DeRef(_seek_1__tmp_at951_16442);
    _seek_1__tmp_at951_16442 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_p_16276)) {
        *poke4_addr = (unsigned long)_p_16276;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_16276)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at966_16444);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at966_16444 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at966_16444); // DJP 

    /** end procedure*/
    goto L14; // [988] 991
L14: 
    DeRefi(_put4_1__tmp_at966_16444);
    _put4_1__tmp_at966_16444 = NOVALUE;

    /** 		put4(psize)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_psize_16277)) {
        *poke4_addr = (unsigned long)_psize_16277;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_psize_16277)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at994_16446);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at994_16446 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at994_16446); // DJP 

    /** end procedure*/
    goto L15; // [1016] 1019
L15: 
    DeRefi(_put4_1__tmp_at994_16446);
    _put4_1__tmp_at994_16446 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_16288); // DJP 

    /** end procedure*/
    goto L16; // [1032] 1035
L16: 
LE: 

    /** 	if new_space then*/
    if (_new_space_16283 == 0) {
        goto L17; // [1040] 1054
    }
    else {
        if (!IS_ATOM_INT(_new_space_16283) && DBL_PTR(_new_space_16283)->dbl == 0.0){
            goto L17; // [1040] 1054
        }
    }

    /** 		db_free(to_be_freed) -- free the old space*/
    Ref(_to_be_freed_16284);
    DeRef(_9343);
    _9343 = _to_be_freed_16284;
    _33db_free(_9343);
    _9343 = NOVALUE;
L17: 

    /** end procedure*/
    DeRef(_p_16276);
    DeRef(_psize_16277);
    DeRef(_i_16278);
    DeRef(_size_16279);
    DeRef(_addr_16280);
    DeRef(_free_list_16281);
    DeRef(_free_list_space_16282);
    DeRef(_new_space_16283);
    DeRef(_to_be_freed_16284);
    DeRef(_prev_addr_16285);
    DeRef(_prev_size_16286);
    DeRef(_remaining_16288);
    DeRef(_9300);
    _9300 = NOVALUE;
    DeRef(_9308);
    _9308 = NOVALUE;
    DeRef(_9324);
    _9324 = NOVALUE;
    return;
    ;
}


void _33save_keys()
{
    int _k_16452 = NOVALUE;
    int _9350 = NOVALUE;
    int _9346 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if caching_option = 1 then*/
    if (_33caching_option_15567 != 1)
    goto L1; // [5] 84

    /** 		if current_table_pos > 0 then*/
    if (binary_op_a(LESSEQ, _33current_table_pos_15558, 0)){
        goto L2; // [13] 83
    }

    /** 			k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_33current_table_pos_15558);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _33current_table_pos_15558;
    _9346 = MAKE_SEQ(_1);
    _k_16452 = find_from(_9346, _33cache_index_15566, 1);
    DeRefDS(_9346);
    _9346 = NOVALUE;

    /** 			if k != 0 then*/
    if (_k_16452 == 0)
    goto L3; // [38] 55

    /** 				key_cache[k] = key_pointers*/
    RefDS(_33key_pointers_15564);
    _2 = (int)SEQ_PTR(_33key_cache_15565);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33key_cache_15565 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_16452);
    _1 = *(int *)_2;
    *(int *)_2 = _33key_pointers_15564;
    DeRef(_1);
    goto L4; // [52] 82
L3: 

    /** 				key_cache = append(key_cache, key_pointers)*/
    RefDS(_33key_pointers_15564);
    Append(&_33key_cache_15565, _33key_cache_15565, _33key_pointers_15564);

    /** 				cache_index = append(cache_index, {current_db, current_table_pos})*/
    Ref(_33current_table_pos_15558);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _33current_table_pos_15558;
    _9350 = MAKE_SEQ(_1);
    RefDS(_9350);
    Append(&_33cache_index_15566, _33cache_index_15566, _9350);
    DeRefDS(_9350);
    _9350 = NOVALUE;
L4: 
L2: 
L1: 

    /** end procedure*/
    return;
    ;
}


int _33db_connect(int _dbalias_16467, int _path_16468, int _dboptions_16469)
{
    int _lPos_16470 = NOVALUE;
    int _lOptions_16471 = NOVALUE;
    int _9407 = NOVALUE;
    int _9406 = NOVALUE;
    int _9405 = NOVALUE;
    int _9402 = NOVALUE;
    int _9399 = NOVALUE;
    int _9398 = NOVALUE;
    int _9397 = NOVALUE;
    int _9396 = NOVALUE;
    int _9394 = NOVALUE;
    int _9393 = NOVALUE;
    int _9392 = NOVALUE;
    int _9391 = NOVALUE;
    int _9390 = NOVALUE;
    int _9389 = NOVALUE;
    int _9388 = NOVALUE;
    int _9385 = NOVALUE;
    int _9384 = NOVALUE;
    int _9383 = NOVALUE;
    int _9381 = NOVALUE;
    int _9380 = NOVALUE;
    int _9379 = NOVALUE;
    int _9377 = NOVALUE;
    int _9376 = NOVALUE;
    int _9375 = NOVALUE;
    int _9374 = NOVALUE;
    int _9373 = NOVALUE;
    int _9371 = NOVALUE;
    int _9369 = NOVALUE;
    int _9368 = NOVALUE;
    int _9366 = NOVALUE;
    int _9365 = NOVALUE;
    int _9364 = NOVALUE;
    int _9363 = NOVALUE;
    int _9362 = NOVALUE;
    int _9361 = NOVALUE;
    int _9360 = NOVALUE;
    int _9358 = NOVALUE;
    int _9355 = NOVALUE;
    int _9353 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	lPos = find(dbalias, Known_Aliases)*/
    _lPos_16470 = find_from(_dbalias_16467, _33Known_Aliases_15578, 1);

    /** 	if lPos then*/
    if (_lPos_16470 == 0)
    {
        goto L1; // [20] 114
    }
    else{
    }

    /** 		if equal(dboptions, DISCONNECT) or find(DISCONNECT, dboptions) then*/
    if (_dboptions_16469 == _33DISCONNECT_15568)
    _9353 = 1;
    else if (IS_ATOM_INT(_dboptions_16469) && IS_ATOM_INT(_33DISCONNECT_15568))
    _9353 = 0;
    else
    _9353 = (compare(_dboptions_16469, _33DISCONNECT_15568) == 0);
    if (_9353 != 0) {
        goto L2; // [29] 43
    }
    _9355 = find_from(_33DISCONNECT_15568, _dboptions_16469, 1);
    if (_9355 == 0)
    {
        _9355 = NOVALUE;
        goto L3; // [39] 70
    }
    else{
        _9355 = NOVALUE;
    }
L2: 

    /** 			Known_Aliases = remove(Known_Aliases, lPos)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33Known_Aliases_15578);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPos_16470)) ? _lPos_16470 : (long)(DBL_PTR(_lPos_16470)->dbl);
        int stop = (IS_ATOM_INT(_lPos_16470)) ? _lPos_16470 : (long)(DBL_PTR(_lPos_16470)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33Known_Aliases_15578), start, &_33Known_Aliases_15578 );
            }
            else Tail(SEQ_PTR(_33Known_Aliases_15578), stop+1, &_33Known_Aliases_15578);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33Known_Aliases_15578), start, &_33Known_Aliases_15578);
        }
        else {
            assign_slice_seq = &assign_space;
            _33Known_Aliases_15578 = Remove_elements(start, stop, (SEQ_PTR(_33Known_Aliases_15578)->ref == 1));
        }
    }

    /** 			Alias_Details = remove(Alias_Details, lPos)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33Alias_Details_15579);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPos_16470)) ? _lPos_16470 : (long)(DBL_PTR(_lPos_16470)->dbl);
        int stop = (IS_ATOM_INT(_lPos_16470)) ? _lPos_16470 : (long)(DBL_PTR(_lPos_16470)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33Alias_Details_15579), start, &_33Alias_Details_15579 );
            }
            else Tail(SEQ_PTR(_33Alias_Details_15579), stop+1, &_33Alias_Details_15579);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33Alias_Details_15579), start, &_33Alias_Details_15579);
        }
        else {
            assign_slice_seq = &assign_space;
            _33Alias_Details_15579 = Remove_elements(start, stop, (SEQ_PTR(_33Alias_Details_15579)->ref == 1));
        }
    }

    /** 			return DB_OK*/
    DeRefDS(_dbalias_16467);
    DeRefDS(_path_16468);
    DeRefDS(_dboptions_16469);
    DeRef(_lOptions_16471);
    return 0;
L3: 

    /** 		if equal(dboptions, CONNECTION) or find(CONNECTION, dboptions) then*/
    if (_dboptions_16469 == _33CONNECTION_15576)
    _9358 = 1;
    else if (IS_ATOM_INT(_dboptions_16469) && IS_ATOM_INT(_33CONNECTION_15576))
    _9358 = 0;
    else
    _9358 = (compare(_dboptions_16469, _33CONNECTION_15576) == 0);
    if (_9358 != 0) {
        goto L4; // [76] 90
    }
    _9360 = find_from(_33CONNECTION_15576, _dboptions_16469, 1);
    if (_9360 == 0)
    {
        _9360 = NOVALUE;
        goto L5; // [86] 103
    }
    else{
        _9360 = NOVALUE;
    }
L4: 

    /** 			return Alias_Details[lPos]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9361 = (int)*(((s1_ptr)_2)->base + _lPos_16470);
    Ref(_9361);
    DeRefDS(_dbalias_16467);
    DeRefDS(_path_16468);
    DeRefDS(_dboptions_16469);
    DeRef(_lOptions_16471);
    return _9361;
L5: 

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_dbalias_16467);
    DeRefDS(_path_16468);
    DeRefDS(_dboptions_16469);
    DeRef(_lOptions_16471);
    _9361 = NOVALUE;
    return -1;
    goto L6; // [111] 169
L1: 

    /** 		if equal(dboptions, DISCONNECT) or find(DISCONNECT, dboptions) or*/
    if (_dboptions_16469 == _33DISCONNECT_15568)
    _9362 = 1;
    else if (IS_ATOM_INT(_dboptions_16469) && IS_ATOM_INT(_33DISCONNECT_15568))
    _9362 = 0;
    else
    _9362 = (compare(_dboptions_16469, _33DISCONNECT_15568) == 0);
    if (_9362 != 0) {
        _9363 = 1;
        goto L7; // [120] 133
    }
    _9364 = find_from(_33DISCONNECT_15568, _dboptions_16469, 1);
    _9363 = (_9364 != 0);
L7: 
    if (_9363 != 0) {
        _9365 = 1;
        goto L8; // [133] 145
    }
    if (_dboptions_16469 == _33CONNECTION_15576)
    _9366 = 1;
    else if (IS_ATOM_INT(_dboptions_16469) && IS_ATOM_INT(_33CONNECTION_15576))
    _9366 = 0;
    else
    _9366 = (compare(_dboptions_16469, _33CONNECTION_15576) == 0);
    _9365 = (_9366 != 0);
L8: 
    if (_9365 != 0) {
        goto L9; // [145] 159
    }
    _9368 = find_from(_33CONNECTION_15576, _dboptions_16469, 1);
    if (_9368 == 0)
    {
        _9368 = NOVALUE;
        goto LA; // [155] 168
    }
    else{
        _9368 = NOVALUE;
    }
L9: 

    /** 			return DB_OPEN_FAIL*/
    DeRefDS(_dbalias_16467);
    DeRefDS(_path_16468);
    DeRefDS(_dboptions_16469);
    DeRef(_lOptions_16471);
    _9361 = NOVALUE;
    return -1;
LA: 
L6: 

    /** 	if length(path) = 0 then*/
    if (IS_SEQUENCE(_path_16468)){
            _9369 = SEQ_PTR(_path_16468)->length;
    }
    else {
        _9369 = 1;
    }
    if (_9369 != 0)
    goto LB; // [174] 187

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_dbalias_16467);
    DeRefDS(_path_16468);
    DeRefDS(_dboptions_16469);
    DeRef(_lOptions_16471);
    _9361 = NOVALUE;
    return -1;
LB: 

    /** 	if types:string(dboptions) then*/
    RefDS(_dboptions_16469);
    _9371 = _5string(_dboptions_16469);
    if (_9371 == 0) {
        DeRef(_9371);
        _9371 = NOVALUE;
        goto LC; // [193] 271
    }
    else {
        if (!IS_ATOM_INT(_9371) && DBL_PTR(_9371)->dbl == 0.0){
            DeRef(_9371);
            _9371 = NOVALUE;
            goto LC; // [193] 271
        }
        DeRef(_9371);
        _9371 = NOVALUE;
    }
    DeRef(_9371);
    _9371 = NOVALUE;

    /** 		dboptions = text:keyvalues(dboptions)*/
    RefDS(_dboptions_16469);
    RefDS(_3151);
    RefDS(_3152);
    RefDS(_3153);
    RefDS(_232);
    _0 = _dboptions_16469;
    _dboptions_16469 = _16keyvalues(_dboptions_16469, _3151, _3152, _3153, _232, 1);
    DeRefDS(_0);

    /** 		for i = 1 to length(dboptions) do*/
    if (IS_SEQUENCE(_dboptions_16469)){
            _9373 = SEQ_PTR(_dboptions_16469)->length;
    }
    else {
        _9373 = 1;
    }
    {
        int _i_16501;
        _i_16501 = 1;
LD: 
        if (_i_16501 > _9373){
            goto LE; // [214] 270
        }

        /** 			if types:string(dboptions[i][2]) then*/
        _2 = (int)SEQ_PTR(_dboptions_16469);
        _9374 = (int)*(((s1_ptr)_2)->base + _i_16501);
        _2 = (int)SEQ_PTR(_9374);
        _9375 = (int)*(((s1_ptr)_2)->base + 2);
        _9374 = NOVALUE;
        Ref(_9375);
        _9376 = _5string(_9375);
        _9375 = NOVALUE;
        if (_9376 == 0) {
            DeRef(_9376);
            _9376 = NOVALUE;
            goto LF; // [235] 263
        }
        else {
            if (!IS_ATOM_INT(_9376) && DBL_PTR(_9376)->dbl == 0.0){
                DeRef(_9376);
                _9376 = NOVALUE;
                goto LF; // [235] 263
            }
            DeRef(_9376);
            _9376 = NOVALUE;
        }
        DeRef(_9376);
        _9376 = NOVALUE;

        /** 				dboptions[i][2] = convert:to_number(dboptions[i][2])*/
        _2 = (int)SEQ_PTR(_dboptions_16469);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _dboptions_16469 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_16501 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_dboptions_16469);
        _9379 = (int)*(((s1_ptr)_2)->base + _i_16501);
        _2 = (int)SEQ_PTR(_9379);
        _9380 = (int)*(((s1_ptr)_2)->base + 2);
        _9379 = NOVALUE;
        Ref(_9380);
        _9381 = _17to_number(_9380, 0);
        _9380 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _9381;
        if( _1 != _9381 ){
            DeRef(_1);
        }
        _9381 = NOVALUE;
        _9377 = NOVALUE;
LF: 

        /** 		end for*/
        _i_16501 = _i_16501 + 1;
        goto LD; // [265] 221
LE: 
        ;
    }
LC: 

    /** 	lOptions = {DB_LOCK_NO, DEF_INIT_TABLES, DEF_INIT_TABLES}*/
    _0 = _lOptions_16471;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 5;
    *((int *)(_2+12)) = 5;
    _lOptions_16471 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	for i = 1 to length(dboptions) do*/
    if (IS_SEQUENCE(_dboptions_16469)){
            _9383 = SEQ_PTR(_dboptions_16469)->length;
    }
    else {
        _9383 = 1;
    }
    {
        int _i_16514;
        _i_16514 = 1;
L10: 
        if (_i_16514 > _9383){
            goto L11; // [286] 394
        }

        /** 		switch dboptions[i][1] do*/
        _2 = (int)SEQ_PTR(_dboptions_16469);
        _9384 = (int)*(((s1_ptr)_2)->base + _i_16514);
        _2 = (int)SEQ_PTR(_9384);
        _9385 = (int)*(((s1_ptr)_2)->base + 1);
        _9384 = NOVALUE;
        _1 = find(_9385, _9386);
        _9385 = NOVALUE;
        switch ( _1 ){ 

            /** 			case LOCK_METHOD then*/
            case 1:

            /** 				lOptions[CONNECT_LOCK] = dboptions[i][2]*/
            _2 = (int)SEQ_PTR(_dboptions_16469);
            _9388 = (int)*(((s1_ptr)_2)->base + _i_16514);
            _2 = (int)SEQ_PTR(_9388);
            _9389 = (int)*(((s1_ptr)_2)->base + 2);
            _9388 = NOVALUE;
            Ref(_9389);
            _2 = (int)SEQ_PTR(_lOptions_16471);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _lOptions_16471 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 1);
            _1 = *(int *)_2;
            *(int *)_2 = _9389;
            if( _1 != _9389 ){
                DeRef(_1);
            }
            _9389 = NOVALUE;
            goto L12; // [328] 387

            /** 			case INIT_TABLES then*/
            case 2:

            /** 				lOptions[CONNECT_TABLES] = dboptions[i][2]*/
            _2 = (int)SEQ_PTR(_dboptions_16469);
            _9390 = (int)*(((s1_ptr)_2)->base + _i_16514);
            _2 = (int)SEQ_PTR(_9390);
            _9391 = (int)*(((s1_ptr)_2)->base + 2);
            _9390 = NOVALUE;
            Ref(_9391);
            _2 = (int)SEQ_PTR(_lOptions_16471);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _lOptions_16471 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _9391;
            if( _1 != _9391 ){
                DeRef(_1);
            }
            _9391 = NOVALUE;
            goto L12; // [350] 387

            /** 			case INIT_FREE then*/
            case 3:

            /** 				lOptions[CONNECT_FREE] = dboptions[i][2]*/
            _2 = (int)SEQ_PTR(_dboptions_16469);
            _9392 = (int)*(((s1_ptr)_2)->base + _i_16514);
            _2 = (int)SEQ_PTR(_9392);
            _9393 = (int)*(((s1_ptr)_2)->base + 2);
            _9392 = NOVALUE;
            Ref(_9393);
            _2 = (int)SEQ_PTR(_lOptions_16471);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _lOptions_16471 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 3);
            _1 = *(int *)_2;
            *(int *)_2 = _9393;
            if( _1 != _9393 ){
                DeRef(_1);
            }
            _9393 = NOVALUE;
            goto L12; // [372] 387

            /** 			case else				*/
            case 0:

            /** 				return DB_OPEN_FAIL*/
            DeRefDS(_dbalias_16467);
            DeRefDS(_path_16468);
            DeRefDS(_dboptions_16469);
            DeRef(_lOptions_16471);
            _9361 = NOVALUE;
            return -1;
        ;}L12: 

        /** 	end for*/
        _i_16514 = _i_16514 + 1;
        goto L10; // [389] 293
L11: 
        ;
    }

    /** 	if lOptions[CONNECT_TABLES] < 1 then*/
    _2 = (int)SEQ_PTR(_lOptions_16471);
    _9394 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(GREATEREQ, _9394, 1)){
        _9394 = NOVALUE;
        goto L13; // [402] 415
    }
    _9394 = NOVALUE;

    /** 		lOptions[CONNECT_TABLES] = DEF_INIT_TABLES*/
    _2 = (int)SEQ_PTR(_lOptions_16471);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lOptions_16471 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 5;
    DeRef(_1);
L13: 

    /** 	lOptions[CONNECT_FREE] = math:min({lOptions[CONNECT_TABLES], MAX_INDEX})*/
    _2 = (int)SEQ_PTR(_lOptions_16471);
    _9396 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_9396);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _9396;
    ((int *)_2)[2] = 10;
    _9397 = MAKE_SEQ(_1);
    _9396 = NOVALUE;
    _9398 = _18min(_9397);
    _9397 = NOVALUE;
    _2 = (int)SEQ_PTR(_lOptions_16471);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lOptions_16471 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _9398;
    if( _1 != _9398 ){
        DeRef(_1);
    }
    _9398 = NOVALUE;

    /** 	if lOptions[CONNECT_FREE] < 1 then*/
    _2 = (int)SEQ_PTR(_lOptions_16471);
    _9399 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(GREATEREQ, _9399, 1)){
        _9399 = NOVALUE;
        goto L14; // [445] 462
    }
    _9399 = NOVALUE;

    /** 		lOptions[CONNECT_FREE] = math:min({DEF_INIT_TABLES, MAX_INDEX})*/
    RefDS(_9401);
    _9402 = _18min(_9401);
    _2 = (int)SEQ_PTR(_lOptions_16471);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lOptions_16471 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _9402;
    if( _1 != _9402 ){
        DeRef(_1);
    }
    _9402 = NOVALUE;
L14: 

    /** 	Known_Aliases = append(Known_Aliases, dbalias)*/
    RefDS(_dbalias_16467);
    Append(&_33Known_Aliases_15578, _33Known_Aliases_15578, _dbalias_16467);

    /** 	Alias_Details = append(Alias_Details, { filesys:canonical_path( filesys:defaultext(path, "edb") ) , lOptions})*/
    RefDS(_path_16468);
    RefDS(_9404);
    _9405 = _8defaultext(_path_16468, _9404);
    _9406 = _8canonical_path(_9405, 0, 0);
    _9405 = NOVALUE;
    RefDS(_lOptions_16471);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _9406;
    ((int *)_2)[2] = _lOptions_16471;
    _9407 = MAKE_SEQ(_1);
    _9406 = NOVALUE;
    RefDS(_9407);
    Append(&_33Alias_Details_15579, _33Alias_Details_15579, _9407);
    DeRefDS(_9407);
    _9407 = NOVALUE;

    /** 	return DB_OK*/
    DeRefDS(_dbalias_16467);
    DeRefDS(_path_16468);
    DeRefDS(_dboptions_16469);
    DeRefDS(_lOptions_16471);
    _9361 = NOVALUE;
    return 0;
    ;
}


int _33db_create(int _path_16549, int _lock_method_16550, int _init_tables_16551, int _init_free_16552)
{
    int _db_16553 = NOVALUE;
    int _lock_file_1__tmp_at272_16592 = NOVALUE;
    int _lock_file_inlined_lock_file_at_272_16591 = NOVALUE;
    int _put4_1__tmp_at398_16601 = NOVALUE;
    int _put4_1__tmp_at426_16603 = NOVALUE;
    int _put4_1__tmp_at469_16609 = NOVALUE;
    int _x_inlined_put4_at_466_16608 = NOVALUE;
    int _put4_1__tmp_at508_16614 = NOVALUE;
    int _x_inlined_put4_at_505_16613 = NOVALUE;
    int _put4_1__tmp_at536_16616 = NOVALUE;
    int _s_inlined_putn_at_572_16620 = NOVALUE;
    int _put4_1__tmp_at604_16625 = NOVALUE;
    int _x_inlined_put4_at_601_16624 = NOVALUE;
    int _s_inlined_putn_at_640_16629 = NOVALUE;
    int _9448 = NOVALUE;
    int _9447 = NOVALUE;
    int _9446 = NOVALUE;
    int _9445 = NOVALUE;
    int _9444 = NOVALUE;
    int _9443 = NOVALUE;
    int _9442 = NOVALUE;
    int _9441 = NOVALUE;
    int _9440 = NOVALUE;
    int _9439 = NOVALUE;
    int _9438 = NOVALUE;
    int _9421 = NOVALUE;
    int _9419 = NOVALUE;
    int _9418 = NOVALUE;
    int _9416 = NOVALUE;
    int _9415 = NOVALUE;
    int _9413 = NOVALUE;
    int _9412 = NOVALUE;
    int _9410 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lock_method_16550)) {
        _1 = (long)(DBL_PTR(_lock_method_16550)->dbl);
        if (UNIQUE(DBL_PTR(_lock_method_16550)) && (DBL_PTR(_lock_method_16550)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lock_method_16550);
        _lock_method_16550 = _1;
    }
    if (!IS_ATOM_INT(_init_tables_16551)) {
        _1 = (long)(DBL_PTR(_init_tables_16551)->dbl);
        if (UNIQUE(DBL_PTR(_init_tables_16551)) && (DBL_PTR(_init_tables_16551)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_tables_16551);
        _init_tables_16551 = _1;
    }
    if (!IS_ATOM_INT(_init_free_16552)) {
        _1 = (long)(DBL_PTR(_init_free_16552)->dbl);
        if (UNIQUE(DBL_PTR(_init_free_16552)) && (DBL_PTR(_init_free_16552)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_free_16552);
        _init_free_16552 = _1;
    }

    /** 	db = find(path, Known_Aliases)*/
    _db_16553 = find_from(_path_16549, _33Known_Aliases_15578, 1);

    /** 	if db then*/
    if (_db_16553 == 0)
    {
        goto L1; // [28] 114
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9410 = (int)*(((s1_ptr)_2)->base + _db_16553);
    DeRefDS(_path_16549);
    _2 = (int)SEQ_PTR(_9410);
    _path_16549 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_16549);
    _9410 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9412 = (int)*(((s1_ptr)_2)->base + _db_16553);
    _2 = (int)SEQ_PTR(_9412);
    _9413 = (int)*(((s1_ptr)_2)->base + 2);
    _9412 = NOVALUE;
    _2 = (int)SEQ_PTR(_9413);
    _lock_method_16550 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_16550)){
        _lock_method_16550 = (long)DBL_PTR(_lock_method_16550)->dbl;
    }
    _9413 = NOVALUE;

    /** 		init_tables = Alias_Details[db][2][CONNECT_TABLES]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9415 = (int)*(((s1_ptr)_2)->base + _db_16553);
    _2 = (int)SEQ_PTR(_9415);
    _9416 = (int)*(((s1_ptr)_2)->base + 2);
    _9415 = NOVALUE;
    _2 = (int)SEQ_PTR(_9416);
    _init_tables_16551 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_init_tables_16551)){
        _init_tables_16551 = (long)DBL_PTR(_init_tables_16551)->dbl;
    }
    _9416 = NOVALUE;

    /** 		init_free = Alias_Details[db][2][CONNECT_FREE]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9418 = (int)*(((s1_ptr)_2)->base + _db_16553);
    _2 = (int)SEQ_PTR(_9418);
    _9419 = (int)*(((s1_ptr)_2)->base + 2);
    _9418 = NOVALUE;
    _2 = (int)SEQ_PTR(_9419);
    _init_free_16552 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_init_free_16552)){
        _init_free_16552 = (long)DBL_PTR(_init_free_16552)->dbl;
    }
    _9419 = NOVALUE;
    goto L2; // [111] 160
L1: 

    /** 		path = filesys:canonical_path( defaultext(path, "edb") )*/
    RefDS(_path_16549);
    RefDS(_9404);
    _9421 = _8defaultext(_path_16549, _9404);
    _0 = _path_16549;
    _path_16549 = _8canonical_path(_9421, 0, 0);
    DeRefDS(_0);
    _9421 = NOVALUE;

    /** 		if init_tables < 1 then*/
    if (_init_tables_16551 >= 1)
    goto L3; // [133] 145

    /** 			init_tables = 1*/
    _init_tables_16551 = 1;
L3: 

    /** 		if init_free < 0 then*/
    if (_init_free_16552 >= 0)
    goto L4; // [147] 159

    /** 			init_free = 0*/
    _init_free_16552 = 0;
L4: 
L2: 

    /** 	db = open(path, "rb")*/
    _db_16553 = EOpen(_path_16549, _2913, 0);

    /** 	if db != -1 then*/
    if (_db_16553 == -1)
    goto L5; // [171] 188

    /** 		close(db)*/
    EClose(_db_16553);

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_path_16549);
    return -2;
L5: 

    /** 	db = open(path, "wb")*/
    _db_16553 = EOpen(_path_16549, _2908, 0);

    /** 	if db = -1 then*/
    if (_db_16553 != -1)
    goto L6; // [199] 212

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_16549);
    return -1;
L6: 

    /** 	close(db)*/
    EClose(_db_16553);

    /** 	db = open(path, "ub")*/
    _db_16553 = EOpen(_path_16549, _9429, 0);

    /** 	if db = -1 then*/
    if (_db_16553 != -1)
    goto L7; // [227] 240

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_16549);
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_16550 != 1)
    goto L8; // [244] 260

    /** 		lock_method = DB_LOCK_NO*/
    _lock_method_16550 = 0;
L8: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_16550 != 2)
    goto L9; // [264] 300

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at272_16592;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_16553;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at272_16592 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_272_16591 = machine(61, _lock_file_1__tmp_at272_16592);
    DeRef(_lock_file_1__tmp_at272_16592);
    _lock_file_1__tmp_at272_16592 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_272_16591 != 0)
    goto LA; // [287] 299

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_16549);
    return -3;
LA: 
L9: 

    /** 	save_keys()*/
    _33save_keys();

    /** 	current_db = db*/
    _33current_db_15557 = _db_16553;

    /** 	current_lock = lock_method*/
    _33current_lock_15563 = _lock_method_16550;

    /** 	current_table_pos = -1*/
    DeRef(_33current_table_pos_15558);
    _33current_table_pos_15558 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_33current_table_name_15559);
    _33current_table_name_15559 = _5;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_16549);
    Append(&_33db_names_15560, _33db_names_15560, _path_16549);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_33db_lock_methods_15562, _33db_lock_methods_15562, _lock_method_16550);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_33db_file_nums_15561, _33db_file_nums_15561, _db_16553);

    /** 	put1(DB_MAGIC) -- so we know what type of file it is*/

    /** 	puts(current_db, x)*/
    EPuts(_33current_db_15557, 77); // DJP 

    /** end procedure*/
    goto LB; // [365] 368
LB: 

    /** 	put1(DB_MAJOR) -- major version*/

    /** 	puts(current_db, x)*/
    EPuts(_33current_db_15557, 4); // DJP 

    /** end procedure*/
    goto LC; // [379] 382
LC: 

    /** 	put1(DB_MINOR) -- minor version*/

    /** 	puts(current_db, x)*/
    EPuts(_33current_db_15557, 0); // DJP 

    /** end procedure*/
    goto LD; // [393] 396
LD: 

    /** 	put4(19)  -- pointer to tables*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)19;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at398_16601);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at398_16601 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at398_16601); // DJP 

    /** end procedure*/
    goto LE; // [419] 422
LE: 
    DeRefi(_put4_1__tmp_at398_16601);
    _put4_1__tmp_at398_16601 = NOVALUE;

    /** 	put4(0)   -- number of free blocks*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at426_16603);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at426_16603 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at426_16603); // DJP 

    /** end procedure*/
    goto LF; // [447] 450
LF: 
    DeRefi(_put4_1__tmp_at426_16603);
    _put4_1__tmp_at426_16603 = NOVALUE;

    /** 	put4(23 + init_tables * SIZEOF_TABLE_HEADER + 4)   -- pointer to free list*/
    if (_init_tables_16551 == (short)_init_tables_16551)
    _9438 = _init_tables_16551 * 16;
    else
    _9438 = NewDouble(_init_tables_16551 * (double)16);
    if (IS_ATOM_INT(_9438)) {
        _9439 = 23 + _9438;
        if ((long)((unsigned long)_9439 + (unsigned long)HIGH_BITS) >= 0) 
        _9439 = NewDouble((double)_9439);
    }
    else {
        _9439 = NewDouble((double)23 + DBL_PTR(_9438)->dbl);
    }
    DeRef(_9438);
    _9438 = NOVALUE;
    if (IS_ATOM_INT(_9439)) {
        _9440 = _9439 + 4;
        if ((long)((unsigned long)_9440 + (unsigned long)HIGH_BITS) >= 0) 
        _9440 = NewDouble((double)_9440);
    }
    else {
        _9440 = NewDouble(DBL_PTR(_9439)->dbl + (double)4);
    }
    DeRef(_9439);
    _9439 = NOVALUE;
    DeRef(_x_inlined_put4_at_466_16608);
    _x_inlined_put4_at_466_16608 = _9440;
    _9440 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_466_16608)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_466_16608;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_466_16608)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at469_16609);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at469_16609 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at469_16609); // DJP 

    /** end procedure*/
    goto L10; // [490] 493
L10: 
    DeRef(_x_inlined_put4_at_466_16608);
    _x_inlined_put4_at_466_16608 = NOVALUE;
    DeRefi(_put4_1__tmp_at469_16609);
    _put4_1__tmp_at469_16609 = NOVALUE;

    /** 	put4( 8 + init_tables * SIZEOF_TABLE_HEADER)  -- allocated size*/
    if (_init_tables_16551 == (short)_init_tables_16551)
    _9441 = _init_tables_16551 * 16;
    else
    _9441 = NewDouble(_init_tables_16551 * (double)16);
    if (IS_ATOM_INT(_9441)) {
        _9442 = 8 + _9441;
        if ((long)((unsigned long)_9442 + (unsigned long)HIGH_BITS) >= 0) 
        _9442 = NewDouble((double)_9442);
    }
    else {
        _9442 = NewDouble((double)8 + DBL_PTR(_9441)->dbl);
    }
    DeRef(_9441);
    _9441 = NOVALUE;
    DeRef(_x_inlined_put4_at_505_16613);
    _x_inlined_put4_at_505_16613 = _9442;
    _9442 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_505_16613)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_505_16613;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_505_16613)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at508_16614);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at508_16614 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at508_16614); // DJP 

    /** end procedure*/
    goto L11; // [529] 532
L11: 
    DeRef(_x_inlined_put4_at_505_16613);
    _x_inlined_put4_at_505_16613 = NOVALUE;
    DeRefi(_put4_1__tmp_at508_16614);
    _put4_1__tmp_at508_16614 = NOVALUE;

    /** 	put4(0)   -- number of tables that currently exist*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at536_16616);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at536_16616 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at536_16616); // DJP 

    /** end procedure*/
    goto L12; // [557] 560
L12: 
    DeRefi(_put4_1__tmp_at536_16616);
    _put4_1__tmp_at536_16616 = NOVALUE;

    /** 	putn(repeat(0, init_tables * SIZEOF_TABLE_HEADER))*/
    _9443 = _init_tables_16551 * 16;
    _9444 = Repeat(0, _9443);
    _9443 = NOVALUE;
    DeRefi(_s_inlined_putn_at_572_16620);
    _s_inlined_putn_at_572_16620 = _9444;
    _9444 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_572_16620); // DJP 

    /** end procedure*/
    goto L13; // [586] 589
L13: 
    DeRefi(_s_inlined_putn_at_572_16620);
    _s_inlined_putn_at_572_16620 = NOVALUE;

    /** 	put4(4+init_free*8)   -- allocated size*/
    if (_init_free_16552 == (short)_init_free_16552)
    _9445 = _init_free_16552 * 8;
    else
    _9445 = NewDouble(_init_free_16552 * (double)8);
    if (IS_ATOM_INT(_9445)) {
        _9446 = 4 + _9445;
        if ((long)((unsigned long)_9446 + (unsigned long)HIGH_BITS) >= 0) 
        _9446 = NewDouble((double)_9446);
    }
    else {
        _9446 = NewDouble((double)4 + DBL_PTR(_9445)->dbl);
    }
    DeRef(_9445);
    _9445 = NOVALUE;
    DeRef(_x_inlined_put4_at_601_16624);
    _x_inlined_put4_at_601_16624 = _9446;
    _9446 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_601_16624)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_601_16624;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_601_16624)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at604_16625);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at604_16625 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at604_16625); // DJP 

    /** end procedure*/
    goto L14; // [625] 628
L14: 
    DeRef(_x_inlined_put4_at_601_16624);
    _x_inlined_put4_at_601_16624 = NOVALUE;
    DeRefi(_put4_1__tmp_at604_16625);
    _put4_1__tmp_at604_16625 = NOVALUE;

    /** 	putn(repeat(0, init_free * 8))*/
    _9447 = _init_free_16552 * 8;
    _9448 = Repeat(0, _9447);
    _9447 = NOVALUE;
    DeRefi(_s_inlined_putn_at_640_16629);
    _s_inlined_putn_at_640_16629 = _9448;
    _9448 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_640_16629); // DJP 

    /** end procedure*/
    goto L15; // [654] 657
L15: 
    DeRefi(_s_inlined_putn_at_640_16629);
    _s_inlined_putn_at_640_16629 = NOVALUE;

    /** 	return DB_OK*/
    DeRefDS(_path_16549);
    return 0;
    ;
}


int _33db_open(int _path_16632, int _lock_method_16633)
{
    int _db_16634 = NOVALUE;
    int _magic_16635 = NOVALUE;
    int _lock_file_1__tmp_at173_16662 = NOVALUE;
    int _lock_file_inlined_lock_file_at_173_16661 = NOVALUE;
    int _lock_file_1__tmp_at219_16669 = NOVALUE;
    int _lock_file_inlined_lock_file_at_219_16668 = NOVALUE;
    int _9459 = NOVALUE;
    int _9457 = NOVALUE;
    int _9455 = NOVALUE;
    int _9453 = NOVALUE;
    int _9452 = NOVALUE;
    int _9450 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lock_method_16633)) {
        _1 = (long)(DBL_PTR(_lock_method_16633)->dbl);
        if (UNIQUE(DBL_PTR(_lock_method_16633)) && (DBL_PTR(_lock_method_16633)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lock_method_16633);
        _lock_method_16633 = _1;
    }

    /** 	db = find(path, Known_Aliases)*/
    _db_16634 = find_from(_path_16632, _33Known_Aliases_15578, 1);

    /** 	if db then*/
    if (_db_16634 == 0)
    {
        goto L1; // [20] 62
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9450 = (int)*(((s1_ptr)_2)->base + _db_16634);
    DeRefDS(_path_16632);
    _2 = (int)SEQ_PTR(_9450);
    _path_16632 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_16632);
    _9450 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9452 = (int)*(((s1_ptr)_2)->base + _db_16634);
    _2 = (int)SEQ_PTR(_9452);
    _9453 = (int)*(((s1_ptr)_2)->base + 2);
    _9452 = NOVALUE;
    _2 = (int)SEQ_PTR(_9453);
    _lock_method_16633 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_16633)){
        _lock_method_16633 = (long)DBL_PTR(_lock_method_16633)->dbl;
    }
    _9453 = NOVALUE;
    goto L2; // [59] 80
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_16632);
    RefDS(_9404);
    _9455 = _8defaultext(_path_16632, _9404);
    _0 = _path_16632;
    _path_16632 = _8canonical_path(_9455, 0, 0);
    DeRefDS(_0);
    _9455 = NOVALUE;
L2: 

    /** 	if lock_method = DB_LOCK_NO or*/
    _9457 = (_lock_method_16633 == 0);
    if (_9457 != 0) {
        goto L3; // [88] 103
    }
    _9459 = (_lock_method_16633 == 2);
    if (_9459 == 0)
    {
        DeRef(_9459);
        _9459 = NOVALUE;
        goto L4; // [99] 115
    }
    else{
        DeRef(_9459);
        _9459 = NOVALUE;
    }
L3: 

    /** 		db = open(path, "ub")*/
    _db_16634 = EOpen(_path_16632, _9429, 0);
    goto L5; // [112] 125
L4: 

    /** 		db = open(path, "rb")*/
    _db_16634 = EOpen(_path_16632, _2913, 0);
L5: 

    /** ifdef WINDOWS then*/

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_16633 != 1)
    goto L6; // [131] 147

    /** 		lock_method = DB_LOCK_EXCLUSIVE*/
    _lock_method_16633 = 2;
L6: 

    /** 	if db = -1 then*/
    if (_db_16634 != -1)
    goto L7; // [149] 162

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_16632);
    DeRef(_9457);
    _9457 = NOVALUE;
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_16633 != 2)
    goto L8; // [166] 208

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at173_16662;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_16634;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at173_16662 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_173_16661 = machine(61, _lock_file_1__tmp_at173_16662);
    DeRef(_lock_file_1__tmp_at173_16662);
    _lock_file_1__tmp_at173_16662 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_173_16661 != 0)
    goto L9; // [189] 253

    /** 			close(db)*/
    EClose(_db_16634);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_16632);
    DeRef(_9457);
    _9457 = NOVALUE;
    return -3;
    goto L9; // [205] 253
L8: 

    /** 	elsif lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_16633 != 1)
    goto LA; // [212] 252

    /** 		if not io:lock_file(db, io:LOCK_SHARED, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at219_16669;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_16634;
    *((int *)(_2+8)) = 1;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at219_16669 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_219_16668 = machine(61, _lock_file_1__tmp_at219_16669);
    DeRef(_lock_file_1__tmp_at219_16669);
    _lock_file_1__tmp_at219_16669 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_219_16668 != 0)
    goto LB; // [235] 251

    /** 			close(db)*/
    EClose(_db_16634);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_16632);
    DeRef(_9457);
    _9457 = NOVALUE;
    return -3;
LB: 
LA: 
L9: 

    /** 	magic = getc(db)*/
    if (_db_16634 != last_r_file_no) {
        last_r_file_ptr = which_file(_db_16634, EF_READ);
        last_r_file_no = _db_16634;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_16635 = getKBchar();
        }
        else
        _magic_16635 = getc(last_r_file_ptr);
    }
    else
    _magic_16635 = getc(last_r_file_ptr);

    /** 	if magic != DB_MAGIC then*/
    if (_magic_16635 == 77)
    goto LC; // [262] 279

    /** 		close(db)*/
    EClose(_db_16634);

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_16632);
    DeRef(_9457);
    _9457 = NOVALUE;
    return -1;
LC: 

    /** 	save_keys()*/
    _33save_keys();

    /** 	current_db = db */
    _33current_db_15557 = _db_16634;

    /** 	current_table_pos = -1*/
    DeRef(_33current_table_pos_15558);
    _33current_table_pos_15558 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_33current_table_name_15559);
    _33current_table_name_15559 = _5;

    /** 	current_lock = lock_method*/
    _33current_lock_15563 = _lock_method_16633;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_16632);
    Append(&_33db_names_15560, _33db_names_15560, _path_16632);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_33db_lock_methods_15562, _33db_lock_methods_15562, _lock_method_16633);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_33db_file_nums_15561, _33db_file_nums_15561, _db_16634);

    /** 	return DB_OK*/
    DeRefDS(_path_16632);
    DeRef(_9457);
    _9457 = NOVALUE;
    return 0;
    ;
}


int _33db_select(int _path_16679, int _lock_method_16680)
{
    int _index_16681 = NOVALUE;
    int _9479 = NOVALUE;
    int _9477 = NOVALUE;
    int _9476 = NOVALUE;
    int _9474 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lock_method_16680)) {
        _1 = (long)(DBL_PTR(_lock_method_16680)->dbl);
        if (UNIQUE(DBL_PTR(_lock_method_16680)) && (DBL_PTR(_lock_method_16680)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lock_method_16680);
        _lock_method_16680 = _1;
    }

    /** 	index = find(path, Known_Aliases)*/
    _index_16681 = find_from(_path_16679, _33Known_Aliases_15578, 1);

    /** 	if index then*/
    if (_index_16681 == 0)
    {
        goto L1; // [20] 62
    }
    else{
    }

    /** 		path = Alias_Details[index][1]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9474 = (int)*(((s1_ptr)_2)->base + _index_16681);
    DeRefDS(_path_16679);
    _2 = (int)SEQ_PTR(_9474);
    _path_16679 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_16679);
    _9474 = NOVALUE;

    /** 		lock_method = Alias_Details[index][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_33Alias_Details_15579);
    _9476 = (int)*(((s1_ptr)_2)->base + _index_16681);
    _2 = (int)SEQ_PTR(_9476);
    _9477 = (int)*(((s1_ptr)_2)->base + 2);
    _9476 = NOVALUE;
    _2 = (int)SEQ_PTR(_9477);
    _lock_method_16680 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_16680)){
        _lock_method_16680 = (long)DBL_PTR(_lock_method_16680)->dbl;
    }
    _9477 = NOVALUE;
    goto L2; // [59] 80
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_16679);
    RefDS(_9404);
    _9479 = _8defaultext(_path_16679, _9404);
    _0 = _path_16679;
    _path_16679 = _8canonical_path(_9479, 0, 0);
    DeRefDS(_0);
    _9479 = NOVALUE;
L2: 

    /** 	index = eu:find(path, db_names)*/
    _index_16681 = find_from(_path_16679, _33db_names_15560, 1);

    /** 	if index = 0 then*/
    if (_index_16681 != 0)
    goto L3; // [93] 150

    /** 		if lock_method = -1 then*/
    if (_lock_method_16680 != -1)
    goto L4; // [99] 112

    /** 			return DB_OPEN_FAIL*/
    DeRefDS(_path_16679);
    return -1;
L4: 

    /** 		index = db_open(path, lock_method)*/
    RefDS(_path_16679);
    _index_16681 = _33db_open(_path_16679, _lock_method_16680);
    if (!IS_ATOM_INT(_index_16681)) {
        _1 = (long)(DBL_PTR(_index_16681)->dbl);
        if (UNIQUE(DBL_PTR(_index_16681)) && (DBL_PTR(_index_16681)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_16681);
        _index_16681 = _1;
    }

    /** 		if index != DB_OK then*/
    if (_index_16681 == 0)
    goto L5; // [127] 138

    /** 			return index*/
    DeRefDS(_path_16679);
    return _index_16681;
L5: 

    /** 		index = eu:find(path, db_names)*/
    _index_16681 = find_from(_path_16679, _33db_names_15560, 1);
L3: 

    /** 	save_keys()*/
    _33save_keys();

    /** 	current_db = db_file_nums[index]*/
    _2 = (int)SEQ_PTR(_33db_file_nums_15561);
    _33current_db_15557 = (int)*(((s1_ptr)_2)->base + _index_16681);
    if (!IS_ATOM_INT(_33current_db_15557))
    _33current_db_15557 = (long)DBL_PTR(_33current_db_15557)->dbl;

    /** 	current_lock = db_lock_methods[index]*/
    _2 = (int)SEQ_PTR(_33db_lock_methods_15562);
    _33current_lock_15563 = (int)*(((s1_ptr)_2)->base + _index_16681);
    if (!IS_ATOM_INT(_33current_lock_15563))
    _33current_lock_15563 = (long)DBL_PTR(_33current_lock_15563)->dbl;

    /** 	current_table_pos = -1*/
    DeRef(_33current_table_pos_15558);
    _33current_table_pos_15558 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_33current_table_name_15559);
    _33current_table_name_15559 = _5;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_33key_pointers_15564);
    _33key_pointers_15564 = _5;

    /** 	return DB_OK*/
    DeRefDS(_path_16679);
    return 0;
    ;
}


void _33db_close()
{
    int _unlock_file_1__tmp_at25_16710 = NOVALUE;
    int _index_16705 = NOVALUE;
    int _9496 = NOVALUE;
    int _9495 = NOVALUE;
    int _9494 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_33current_db_15557 != -1)
    goto L1; // [5] 15

    /** 		return*/
    return;
L1: 

    /** 	if current_lock then*/
    if (_33current_lock_15563 == 0)
    {
        goto L2; // [19] 43
    }
    else{
    }

    /** 		io:unlock_file(current_db, {})*/

    /** 	machine_proc(M_UNLOCK_FILE, {fn, r})*/
    RefDS(_5);
    DeRef(_unlock_file_1__tmp_at25_16710);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _5;
    _unlock_file_1__tmp_at25_16710 = MAKE_SEQ(_1);
    machine(62, _unlock_file_1__tmp_at25_16710);

    /** end procedure*/
    goto L3; // [37] 40
L3: 
    DeRef(_unlock_file_1__tmp_at25_16710);
    _unlock_file_1__tmp_at25_16710 = NOVALUE;
L2: 

    /** 	close(current_db)*/
    EClose(_33current_db_15557);

    /** 	index = eu:find(current_db, db_file_nums)*/
    _index_16705 = find_from(_33current_db_15557, _33db_file_nums_15561, 1);

    /** 	db_names = remove(db_names, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33db_names_15560);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_16705)) ? _index_16705 : (long)(DBL_PTR(_index_16705)->dbl);
        int stop = (IS_ATOM_INT(_index_16705)) ? _index_16705 : (long)(DBL_PTR(_index_16705)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33db_names_15560), start, &_33db_names_15560 );
            }
            else Tail(SEQ_PTR(_33db_names_15560), stop+1, &_33db_names_15560);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33db_names_15560), start, &_33db_names_15560);
        }
        else {
            assign_slice_seq = &assign_space;
            _33db_names_15560 = Remove_elements(start, stop, (SEQ_PTR(_33db_names_15560)->ref == 1));
        }
    }

    /** 	db_file_nums = remove(db_file_nums, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33db_file_nums_15561);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_16705)) ? _index_16705 : (long)(DBL_PTR(_index_16705)->dbl);
        int stop = (IS_ATOM_INT(_index_16705)) ? _index_16705 : (long)(DBL_PTR(_index_16705)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33db_file_nums_15561), start, &_33db_file_nums_15561 );
            }
            else Tail(SEQ_PTR(_33db_file_nums_15561), stop+1, &_33db_file_nums_15561);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33db_file_nums_15561), start, &_33db_file_nums_15561);
        }
        else {
            assign_slice_seq = &assign_space;
            _33db_file_nums_15561 = Remove_elements(start, stop, (SEQ_PTR(_33db_file_nums_15561)->ref == 1));
        }
    }

    /** 	db_lock_methods = remove(db_lock_methods, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33db_lock_methods_15562);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_16705)) ? _index_16705 : (long)(DBL_PTR(_index_16705)->dbl);
        int stop = (IS_ATOM_INT(_index_16705)) ? _index_16705 : (long)(DBL_PTR(_index_16705)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33db_lock_methods_15562), start, &_33db_lock_methods_15562 );
            }
            else Tail(SEQ_PTR(_33db_lock_methods_15562), stop+1, &_33db_lock_methods_15562);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33db_lock_methods_15562), start, &_33db_lock_methods_15562);
        }
        else {
            assign_slice_seq = &assign_space;
            _33db_lock_methods_15562 = Remove_elements(start, stop, (SEQ_PTR(_33db_lock_methods_15562)->ref == 1));
        }
    }

    /** 	for i = length(cache_index) to 1 by -1 do*/
    if (IS_SEQUENCE(_33cache_index_15566)){
            _9494 = SEQ_PTR(_33cache_index_15566)->length;
    }
    else {
        _9494 = 1;
    }
    {
        int _i_16716;
        _i_16716 = _9494;
L4: 
        if (_i_16716 < 1){
            goto L5; // [96] 147
        }

        /** 		if cache_index[i][1] = current_db then*/
        _2 = (int)SEQ_PTR(_33cache_index_15566);
        _9495 = (int)*(((s1_ptr)_2)->base + _i_16716);
        _2 = (int)SEQ_PTR(_9495);
        _9496 = (int)*(((s1_ptr)_2)->base + 1);
        _9495 = NOVALUE;
        if (binary_op_a(NOTEQ, _9496, _33current_db_15557)){
            _9496 = NOVALUE;
            goto L6; // [117] 140
        }
        _9496 = NOVALUE;

        /** 			cache_index = remove(cache_index, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_33cache_index_15566);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_16716)) ? _i_16716 : (long)(DBL_PTR(_i_16716)->dbl);
            int stop = (IS_ATOM_INT(_i_16716)) ? _i_16716 : (long)(DBL_PTR(_i_16716)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_33cache_index_15566), start, &_33cache_index_15566 );
                }
                else Tail(SEQ_PTR(_33cache_index_15566), stop+1, &_33cache_index_15566);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_33cache_index_15566), start, &_33cache_index_15566);
            }
            else {
                assign_slice_seq = &assign_space;
                _33cache_index_15566 = Remove_elements(start, stop, (SEQ_PTR(_33cache_index_15566)->ref == 1));
            }
        }

        /** 			key_cache = remove(key_cache, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_33key_cache_15565);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_16716)) ? _i_16716 : (long)(DBL_PTR(_i_16716)->dbl);
            int stop = (IS_ATOM_INT(_i_16716)) ? _i_16716 : (long)(DBL_PTR(_i_16716)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_33key_cache_15565), start, &_33key_cache_15565 );
                }
                else Tail(SEQ_PTR(_33key_cache_15565), stop+1, &_33key_cache_15565);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_33key_cache_15565), start, &_33key_cache_15565);
            }
            else {
                assign_slice_seq = &assign_space;
                _33key_cache_15565 = Remove_elements(start, stop, (SEQ_PTR(_33key_cache_15565)->ref == 1));
            }
        }
L6: 

        /** 	end for*/
        _i_16716 = _i_16716 + -1;
        goto L4; // [142] 103
L5: 
        ;
    }

    /** 	current_table_pos = -1*/
    DeRef(_33current_table_pos_15558);
    _33current_table_pos_15558 = -1;

    /** 	current_table_name = ""	*/
    RefDS(_5);
    DeRef(_33current_table_name_15559);
    _33current_table_name_15559 = _5;

    /** 	current_db = -1*/
    _33current_db_15557 = -1;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_33key_pointers_15564);
    _33key_pointers_15564 = _5;

    /** end procedure*/
    return;
    ;
}


int _33table_find(int _name_16726)
{
    int _tables_16727 = NOVALUE;
    int _nt_16728 = NOVALUE;
    int _t_header_16729 = NOVALUE;
    int _name_ptr_16730 = NOVALUE;
    int _seek_1__tmp_at6_16733 = NOVALUE;
    int _seek_inlined_seek_at_6_16732 = NOVALUE;
    int _seek_1__tmp_at44_16740 = NOVALUE;
    int _seek_inlined_seek_at_44_16739 = NOVALUE;
    int _seek_1__tmp_at84_16748 = NOVALUE;
    int _seek_inlined_seek_at_84_16747 = NOVALUE;
    int _seek_1__tmp_at106_16752 = NOVALUE;
    int _seek_inlined_seek_at_106_16751 = NOVALUE;
    int _9507 = NOVALUE;
    int _9505 = NOVALUE;
    int _9500 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at6_16733);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at6_16733 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_6_16732 = machine(19, _seek_1__tmp_at6_16733);
    DeRefi(_seek_1__tmp_at6_16733);
    _seek_1__tmp_at6_16733 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_33vLastErrors_15581)){
            _9500 = SEQ_PTR(_33vLastErrors_15581)->length;
    }
    else {
        _9500 = 1;
    }
    if (_9500 <= 0)
    goto L1; // [27] 36
    DeRefDS(_name_16726);
    DeRef(_tables_16727);
    DeRef(_nt_16728);
    DeRef(_t_header_16729);
    DeRef(_name_ptr_16730);
    return -1;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_16727;
    _tables_16727 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_16727);
    DeRef(_seek_1__tmp_at44_16740);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _tables_16727;
    _seek_1__tmp_at44_16740 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_44_16739 = machine(19, _seek_1__tmp_at44_16740);
    DeRef(_seek_1__tmp_at44_16740);
    _seek_1__tmp_at44_16740 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_16728;
    _nt_16728 = _33get4();
    DeRef(_0);

    /** 	t_header = tables+4*/
    DeRef(_t_header_16729);
    if (IS_ATOM_INT(_tables_16727)) {
        _t_header_16729 = _tables_16727 + 4;
        if ((long)((unsigned long)_t_header_16729 + (unsigned long)HIGH_BITS) >= 0) 
        _t_header_16729 = NewDouble((double)_t_header_16729);
    }
    else {
        _t_header_16729 = NewDouble(DBL_PTR(_tables_16727)->dbl + (double)4);
    }

    /** 	for i = 1 to nt do*/
    Ref(_nt_16728);
    DeRef(_9505);
    _9505 = _nt_16728;
    {
        int _i_16744;
        _i_16744 = 1;
L2: 
        if (binary_op_a(GREATER, _i_16744, _9505)){
            goto L3; // [74] 150
        }

        /** 		io:seek(current_db, t_header)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_t_header_16729);
        DeRef(_seek_1__tmp_at84_16748);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _t_header_16729;
        _seek_1__tmp_at84_16748 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_84_16747 = machine(19, _seek_1__tmp_at84_16748);
        DeRef(_seek_1__tmp_at84_16748);
        _seek_1__tmp_at84_16748 = NOVALUE;

        /** 		name_ptr = get4()*/
        _0 = _name_ptr_16730;
        _name_ptr_16730 = _33get4();
        DeRef(_0);

        /** 		io:seek(current_db, name_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_ptr_16730);
        DeRef(_seek_1__tmp_at106_16752);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _name_ptr_16730;
        _seek_1__tmp_at106_16752 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_106_16751 = machine(19, _seek_1__tmp_at106_16752);
        DeRef(_seek_1__tmp_at106_16752);
        _seek_1__tmp_at106_16752 = NOVALUE;

        /** 		if equal_string(name) > 0 then*/
        RefDS(_name_16726);
        _9507 = _33equal_string(_name_16726);
        if (binary_op_a(LESSEQ, _9507, 0)){
            DeRef(_9507);
            _9507 = NOVALUE;
            goto L4; // [126] 137
        }
        DeRef(_9507);
        _9507 = NOVALUE;

        /** 			return t_header*/
        DeRef(_i_16744);
        DeRefDS(_name_16726);
        DeRef(_tables_16727);
        DeRef(_nt_16728);
        DeRef(_name_ptr_16730);
        return _t_header_16729;
L4: 

        /** 		t_header += SIZEOF_TABLE_HEADER*/
        _0 = _t_header_16729;
        if (IS_ATOM_INT(_t_header_16729)) {
            _t_header_16729 = _t_header_16729 + 16;
            if ((long)((unsigned long)_t_header_16729 + (unsigned long)HIGH_BITS) >= 0) 
            _t_header_16729 = NewDouble((double)_t_header_16729);
        }
        else {
            _t_header_16729 = NewDouble(DBL_PTR(_t_header_16729)->dbl + (double)16);
        }
        DeRef(_0);

        /** 	end for*/
        _0 = _i_16744;
        if (IS_ATOM_INT(_i_16744)) {
            _i_16744 = _i_16744 + 1;
            if ((long)((unsigned long)_i_16744 +(unsigned long) HIGH_BITS) >= 0){
                _i_16744 = NewDouble((double)_i_16744);
            }
        }
        else {
            _i_16744 = binary_op_a(PLUS, _i_16744, 1);
        }
        DeRef(_0);
        goto L2; // [145] 81
L3: 
        ;
        DeRef(_i_16744);
    }

    /** 	return -1*/
    DeRefDS(_name_16726);
    DeRef(_tables_16727);
    DeRef(_nt_16728);
    DeRef(_t_header_16729);
    DeRef(_name_ptr_16730);
    return -1;
    ;
}


int _33db_select_table(int _name_16759)
{
    int _table_16760 = NOVALUE;
    int _nkeys_16761 = NOVALUE;
    int _index_16762 = NOVALUE;
    int _block_ptr_16763 = NOVALUE;
    int _block_size_16764 = NOVALUE;
    int _blocks_16765 = NOVALUE;
    int _k_16766 = NOVALUE;
    int _seek_1__tmp_at128_16785 = NOVALUE;
    int _seek_inlined_seek_at_128_16784 = NOVALUE;
    int _pos_inlined_seek_at_125_16783 = NOVALUE;
    int _seek_1__tmp_at190_16795 = NOVALUE;
    int _seek_inlined_seek_at_190_16794 = NOVALUE;
    int _seek_1__tmp_at217_16800 = NOVALUE;
    int _seek_inlined_seek_at_217_16799 = NOVALUE;
    int _9528 = NOVALUE;
    int _9527 = NOVALUE;
    int _9524 = NOVALUE;
    int _9519 = NOVALUE;
    int _9514 = NOVALUE;
    int _9510 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(current_table_name, name) then*/
    if (_33current_table_name_15559 == _name_16759)
    _9510 = 1;
    else if (IS_ATOM_INT(_33current_table_name_15559) && IS_ATOM_INT(_name_16759))
    _9510 = 0;
    else
    _9510 = (compare(_33current_table_name_15559, _name_16759) == 0);
    if (_9510 == 0)
    {
        _9510 = NOVALUE;
        goto L1; // [11] 23
    }
    else{
        _9510 = NOVALUE;
    }

    /** 		return DB_OK*/
    DeRefDS(_name_16759);
    DeRef(_table_16760);
    DeRef(_nkeys_16761);
    DeRef(_index_16762);
    DeRef(_block_ptr_16763);
    DeRef(_block_size_16764);
    return 0;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_16759);
    _0 = _table_16760;
    _table_16760 = _33table_find(_name_16759);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_16760, -1)){
        goto L2; // [31] 44
    }

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_name_16759);
    DeRef(_table_16760);
    DeRef(_nkeys_16761);
    DeRef(_index_16762);
    DeRef(_block_ptr_16763);
    DeRef(_block_size_16764);
    return -1;
L2: 

    /** 	save_keys()*/
    _33save_keys();

    /** 	current_table_pos = table*/
    Ref(_table_16760);
    DeRef(_33current_table_pos_15558);
    _33current_table_pos_15558 = _table_16760;

    /** 	current_table_name = name*/
    RefDS(_name_16759);
    DeRef(_33current_table_name_15559);
    _33current_table_name_15559 = _name_16759;

    /** 	k = 0*/
    _k_16766 = 0;

    /** 	if caching_option = 1 then*/
    if (_33caching_option_15567 != 1)
    goto L3; // [71] 112

    /** 		k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_33current_table_pos_15558);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _33current_table_pos_15558;
    _9514 = MAKE_SEQ(_1);
    _k_16766 = find_from(_9514, _33cache_index_15566, 1);
    DeRefDS(_9514);
    _9514 = NOVALUE;

    /** 		if k != 0 then*/
    if (_k_16766 == 0)
    goto L4; // [96] 111

    /** 			key_pointers = key_cache[k]*/
    DeRef(_33key_pointers_15564);
    _2 = (int)SEQ_PTR(_33key_cache_15565);
    _33key_pointers_15564 = (int)*(((s1_ptr)_2)->base + _k_16766);
    Ref(_33key_pointers_15564);
L4: 
L3: 

    /** 	if k = 0 then*/
    if (_k_16766 != 0)
    goto L5; // [114] 283

    /** 		io:seek(current_db, table+4)*/
    if (IS_ATOM_INT(_table_16760)) {
        _9519 = _table_16760 + 4;
        if ((long)((unsigned long)_9519 + (unsigned long)HIGH_BITS) >= 0) 
        _9519 = NewDouble((double)_9519);
    }
    else {
        _9519 = NewDouble(DBL_PTR(_table_16760)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_125_16783);
    _pos_inlined_seek_at_125_16783 = _9519;
    _9519 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_125_16783);
    DeRef(_seek_1__tmp_at128_16785);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_125_16783;
    _seek_1__tmp_at128_16785 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_128_16784 = machine(19, _seek_1__tmp_at128_16785);
    DeRef(_pos_inlined_seek_at_125_16783);
    _pos_inlined_seek_at_125_16783 = NOVALUE;
    DeRef(_seek_1__tmp_at128_16785);
    _seek_1__tmp_at128_16785 = NOVALUE;

    /** 		nkeys = get4()*/
    _0 = _nkeys_16761;
    _nkeys_16761 = _33get4();
    DeRef(_0);

    /** 		blocks = get4()*/
    _blocks_16765 = _33get4();
    if (!IS_ATOM_INT(_blocks_16765)) {
        _1 = (long)(DBL_PTR(_blocks_16765)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_16765)) && (DBL_PTR(_blocks_16765)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_16765);
        _blocks_16765 = _1;
    }

    /** 		index = get4()*/
    _0 = _index_16762;
    _index_16762 = _33get4();
    DeRef(_0);

    /** 		key_pointers = repeat(0, nkeys)*/
    DeRef(_33key_pointers_15564);
    _33key_pointers_15564 = Repeat(0, _nkeys_16761);

    /** 		k = 1*/
    _k_16766 = 1;

    /** 		for b = 0 to blocks-1 do*/
    _9524 = _blocks_16765 - 1;
    if ((long)((unsigned long)_9524 +(unsigned long) HIGH_BITS) >= 0){
        _9524 = NewDouble((double)_9524);
    }
    {
        int _b_16791;
        _b_16791 = 0;
L6: 
        if (binary_op_a(GREATER, _b_16791, _9524)){
            goto L7; // [180] 282
        }

        /** 			io:seek(current_db, index)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_index_16762);
        DeRef(_seek_1__tmp_at190_16795);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _index_16762;
        _seek_1__tmp_at190_16795 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_190_16794 = machine(19, _seek_1__tmp_at190_16795);
        DeRef(_seek_1__tmp_at190_16795);
        _seek_1__tmp_at190_16795 = NOVALUE;

        /** 			block_size = get4()*/
        _0 = _block_size_16764;
        _block_size_16764 = _33get4();
        DeRef(_0);

        /** 			block_ptr = get4()*/
        _0 = _block_ptr_16763;
        _block_ptr_16763 = _33get4();
        DeRef(_0);

        /** 			io:seek(current_db, block_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_block_ptr_16763);
        DeRef(_seek_1__tmp_at217_16800);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _block_ptr_16763;
        _seek_1__tmp_at217_16800 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_217_16799 = machine(19, _seek_1__tmp_at217_16800);
        DeRef(_seek_1__tmp_at217_16800);
        _seek_1__tmp_at217_16800 = NOVALUE;

        /** 			for j = 1 to block_size do*/
        Ref(_block_size_16764);
        DeRef(_9527);
        _9527 = _block_size_16764;
        {
            int _j_16802;
            _j_16802 = 1;
L8: 
            if (binary_op_a(GREATER, _j_16802, _9527)){
                goto L9; // [236] 269
            }

            /** 				key_pointers[k] = get4()*/
            _9528 = _33get4();
            _2 = (int)SEQ_PTR(_33key_pointers_15564);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _33key_pointers_15564 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _k_16766);
            _1 = *(int *)_2;
            *(int *)_2 = _9528;
            if( _1 != _9528 ){
                DeRef(_1);
            }
            _9528 = NOVALUE;

            /** 				k += 1*/
            _k_16766 = _k_16766 + 1;

            /** 			end for*/
            _0 = _j_16802;
            if (IS_ATOM_INT(_j_16802)) {
                _j_16802 = _j_16802 + 1;
                if ((long)((unsigned long)_j_16802 +(unsigned long) HIGH_BITS) >= 0){
                    _j_16802 = NewDouble((double)_j_16802);
                }
            }
            else {
                _j_16802 = binary_op_a(PLUS, _j_16802, 1);
            }
            DeRef(_0);
            goto L8; // [264] 243
L9: 
            ;
            DeRef(_j_16802);
        }

        /** 			index += 8*/
        _0 = _index_16762;
        if (IS_ATOM_INT(_index_16762)) {
            _index_16762 = _index_16762 + 8;
            if ((long)((unsigned long)_index_16762 + (unsigned long)HIGH_BITS) >= 0) 
            _index_16762 = NewDouble((double)_index_16762);
        }
        else {
            _index_16762 = NewDouble(DBL_PTR(_index_16762)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _0 = _b_16791;
        if (IS_ATOM_INT(_b_16791)) {
            _b_16791 = _b_16791 + 1;
            if ((long)((unsigned long)_b_16791 +(unsigned long) HIGH_BITS) >= 0){
                _b_16791 = NewDouble((double)_b_16791);
            }
        }
        else {
            _b_16791 = binary_op_a(PLUS, _b_16791, 1);
        }
        DeRef(_0);
        goto L6; // [277] 187
L7: 
        ;
        DeRef(_b_16791);
    }
L5: 

    /** 	return DB_OK*/
    DeRefDS(_name_16759);
    DeRef(_table_16760);
    DeRef(_nkeys_16761);
    DeRef(_index_16762);
    DeRef(_block_ptr_16763);
    DeRef(_block_size_16764);
    DeRef(_9524);
    _9524 = NOVALUE;
    return 0;
    ;
}


int _33db_current_table()
{
    int _0, _1, _2;
    

    /** 	return current_table_name*/
    RefDS(_33current_table_name_15559);
    return _33current_table_name_15559;
    ;
}


int _33db_create_table(int _name_16811, int _init_records_16812)
{
    int _name_ptr_16813 = NOVALUE;
    int _nt_16814 = NOVALUE;
    int _tables_16815 = NOVALUE;
    int _newtables_16816 = NOVALUE;
    int _table_16817 = NOVALUE;
    int _records_ptr_16818 = NOVALUE;
    int _size_16819 = NOVALUE;
    int _newsize_16820 = NOVALUE;
    int _index_ptr_16821 = NOVALUE;
    int _remaining_16822 = NOVALUE;
    int _init_index_16823 = NOVALUE;
    int _seek_1__tmp_at78_16837 = NOVALUE;
    int _seek_inlined_seek_at_78_16836 = NOVALUE;
    int _seek_1__tmp_at107_16843 = NOVALUE;
    int _seek_inlined_seek_at_107_16842 = NOVALUE;
    int _pos_inlined_seek_at_104_16841 = NOVALUE;
    int _put4_1__tmp_at169_16856 = NOVALUE;
    int _seek_1__tmp_at206_16861 = NOVALUE;
    int _seek_inlined_seek_at_206_16860 = NOVALUE;
    int _pos_inlined_seek_at_203_16859 = NOVALUE;
    int _seek_1__tmp_at249_16869 = NOVALUE;
    int _seek_inlined_seek_at_249_16868 = NOVALUE;
    int _pos_inlined_seek_at_246_16867 = NOVALUE;
    int _s_inlined_putn_at_298_16877 = NOVALUE;
    int _seek_1__tmp_at326_16880 = NOVALUE;
    int _seek_inlined_seek_at_326_16879 = NOVALUE;
    int _put4_1__tmp_at341_16882 = NOVALUE;
    int _seek_1__tmp_at379_16886 = NOVALUE;
    int _seek_inlined_seek_at_379_16885 = NOVALUE;
    int _put4_1__tmp_at394_16888 = NOVALUE;
    int _s_inlined_putn_at_441_16894 = NOVALUE;
    int _put4_1__tmp_at472_16898 = NOVALUE;
    int _put4_1__tmp_at500_16900 = NOVALUE;
    int _s_inlined_putn_at_540_16905 = NOVALUE;
    int _s_inlined_putn_at_578_16911 = NOVALUE;
    int _seek_1__tmp_at620_16919 = NOVALUE;
    int _seek_inlined_seek_at_620_16918 = NOVALUE;
    int _pos_inlined_seek_at_617_16917 = NOVALUE;
    int _put4_1__tmp_at635_16921 = NOVALUE;
    int _put4_1__tmp_at663_16923 = NOVALUE;
    int _put4_1__tmp_at691_16925 = NOVALUE;
    int _put4_1__tmp_at719_16927 = NOVALUE;
    int _9577 = NOVALUE;
    int _9576 = NOVALUE;
    int _9575 = NOVALUE;
    int _9574 = NOVALUE;
    int _9573 = NOVALUE;
    int _9572 = NOVALUE;
    int _9570 = NOVALUE;
    int _9569 = NOVALUE;
    int _9568 = NOVALUE;
    int _9567 = NOVALUE;
    int _9566 = NOVALUE;
    int _9564 = NOVALUE;
    int _9563 = NOVALUE;
    int _9562 = NOVALUE;
    int _9560 = NOVALUE;
    int _9559 = NOVALUE;
    int _9558 = NOVALUE;
    int _9557 = NOVALUE;
    int _9556 = NOVALUE;
    int _9555 = NOVALUE;
    int _9554 = NOVALUE;
    int _9552 = NOVALUE;
    int _9551 = NOVALUE;
    int _9550 = NOVALUE;
    int _9547 = NOVALUE;
    int _9546 = NOVALUE;
    int _9544 = NOVALUE;
    int _9543 = NOVALUE;
    int _9541 = NOVALUE;
    int _9539 = NOVALUE;
    int _9536 = NOVALUE;
    int _9531 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_init_records_16812)) {
        _1 = (long)(DBL_PTR(_init_records_16812)->dbl);
        if (UNIQUE(DBL_PTR(_init_records_16812)) && (DBL_PTR(_init_records_16812)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_records_16812);
        _init_records_16812 = _1;
    }

    /** 	if not cstring(name) then*/
    RefDS(_name_16811);
    _9531 = _5cstring(_name_16811);
    if (IS_ATOM_INT(_9531)) {
        if (_9531 != 0){
            DeRef(_9531);
            _9531 = NOVALUE;
            goto L1; // [13] 25
        }
    }
    else {
        if (DBL_PTR(_9531)->dbl != 0.0){
            DeRef(_9531);
            _9531 = NOVALUE;
            goto L1; // [13] 25
        }
    }
    DeRef(_9531);
    _9531 = NOVALUE;

    /** 		return DB_BAD_NAME*/
    DeRefDS(_name_16811);
    DeRef(_name_ptr_16813);
    DeRef(_nt_16814);
    DeRef(_tables_16815);
    DeRef(_newtables_16816);
    DeRef(_table_16817);
    DeRef(_records_ptr_16818);
    DeRef(_size_16819);
    DeRef(_newsize_16820);
    DeRef(_index_ptr_16821);
    DeRef(_remaining_16822);
    return -4;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_16811);
    _0 = _table_16817;
    _table_16817 = _33table_find(_name_16811);
    DeRef(_0);

    /** 	if table != -1 then*/
    if (binary_op_a(EQUALS, _table_16817, -1)){
        goto L2; // [33] 46
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_name_16811);
    DeRef(_name_ptr_16813);
    DeRef(_nt_16814);
    DeRef(_tables_16815);
    DeRef(_newtables_16816);
    DeRef(_table_16817);
    DeRef(_records_ptr_16818);
    DeRef(_size_16819);
    DeRef(_newsize_16820);
    DeRef(_index_ptr_16821);
    DeRef(_remaining_16822);
    return -2;
L2: 

    /** 	if init_records < 1 then*/
    if (_init_records_16812 >= 1)
    goto L3; // [48] 60

    /** 		init_records = 1*/
    _init_records_16812 = 1;
L3: 

    /** 	init_index = math:min({init_records, MAX_INDEX})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _init_records_16812;
    ((int *)_2)[2] = 10;
    _9536 = MAKE_SEQ(_1);
    _init_index_16823 = _18min(_9536);
    _9536 = NOVALUE;
    if (!IS_ATOM_INT(_init_index_16823)) {
        _1 = (long)(DBL_PTR(_init_index_16823)->dbl);
        if (UNIQUE(DBL_PTR(_init_index_16823)) && (DBL_PTR(_init_index_16823)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_index_16823);
        _init_index_16823 = _1;
    }

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at78_16837);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at78_16837 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_78_16836 = machine(19, _seek_1__tmp_at78_16837);
    DeRefi(_seek_1__tmp_at78_16837);
    _seek_1__tmp_at78_16837 = NOVALUE;

    /** 	tables = get4()*/
    _0 = _tables_16815;
    _tables_16815 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables-4)*/
    if (IS_ATOM_INT(_tables_16815)) {
        _9539 = _tables_16815 - 4;
        if ((long)((unsigned long)_9539 +(unsigned long) HIGH_BITS) >= 0){
            _9539 = NewDouble((double)_9539);
        }
    }
    else {
        _9539 = NewDouble(DBL_PTR(_tables_16815)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_104_16841);
    _pos_inlined_seek_at_104_16841 = _9539;
    _9539 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_104_16841);
    DeRef(_seek_1__tmp_at107_16843);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_104_16841;
    _seek_1__tmp_at107_16843 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_107_16842 = machine(19, _seek_1__tmp_at107_16843);
    DeRef(_pos_inlined_seek_at_104_16841);
    _pos_inlined_seek_at_104_16841 = NOVALUE;
    DeRef(_seek_1__tmp_at107_16843);
    _seek_1__tmp_at107_16843 = NOVALUE;

    /** 	size = get4()*/
    _0 = _size_16819;
    _size_16819 = _33get4();
    DeRef(_0);

    /** 	nt = get4()+1*/
    _9541 = _33get4();
    DeRef(_nt_16814);
    if (IS_ATOM_INT(_9541)) {
        _nt_16814 = _9541 + 1;
        if (_nt_16814 > MAXINT){
            _nt_16814 = NewDouble((double)_nt_16814);
        }
    }
    else
    _nt_16814 = binary_op(PLUS, 1, _9541);
    DeRef(_9541);
    _9541 = NOVALUE;

    /** 	if nt*SIZEOF_TABLE_HEADER + 8 > size then*/
    if (IS_ATOM_INT(_nt_16814)) {
        if (_nt_16814 == (short)_nt_16814)
        _9543 = _nt_16814 * 16;
        else
        _9543 = NewDouble(_nt_16814 * (double)16);
    }
    else {
        _9543 = NewDouble(DBL_PTR(_nt_16814)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_9543)) {
        _9544 = _9543 + 8;
        if ((long)((unsigned long)_9544 + (unsigned long)HIGH_BITS) >= 0) 
        _9544 = NewDouble((double)_9544);
    }
    else {
        _9544 = NewDouble(DBL_PTR(_9543)->dbl + (double)8);
    }
    DeRef(_9543);
    _9543 = NOVALUE;
    if (binary_op_a(LESSEQ, _9544, _size_16819)){
        DeRef(_9544);
        _9544 = NOVALUE;
        goto L4; // [144] 375
    }
    DeRef(_9544);
    _9544 = NOVALUE;

    /** 		newsize = floor(size + size / 2)*/
    if (IS_ATOM_INT(_size_16819)) {
        if (_size_16819 & 1) {
            _9546 = NewDouble((_size_16819 >> 1) + 0.5);
        }
        else
        _9546 = _size_16819 >> 1;
    }
    else {
        _9546 = binary_op(DIVIDE, _size_16819, 2);
    }
    if (IS_ATOM_INT(_size_16819) && IS_ATOM_INT(_9546)) {
        _9547 = _size_16819 + _9546;
        if ((long)((unsigned long)_9547 + (unsigned long)HIGH_BITS) >= 0) 
        _9547 = NewDouble((double)_9547);
    }
    else {
        if (IS_ATOM_INT(_size_16819)) {
            _9547 = NewDouble((double)_size_16819 + DBL_PTR(_9546)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9546)) {
                _9547 = NewDouble(DBL_PTR(_size_16819)->dbl + (double)_9546);
            }
            else
            _9547 = NewDouble(DBL_PTR(_size_16819)->dbl + DBL_PTR(_9546)->dbl);
        }
    }
    DeRef(_9546);
    _9546 = NOVALUE;
    DeRef(_newsize_16820);
    if (IS_ATOM_INT(_9547))
    _newsize_16820 = e_floor(_9547);
    else
    _newsize_16820 = unary_op(FLOOR, _9547);
    DeRef(_9547);
    _9547 = NOVALUE;

    /** 		newtables = db_allocate(newsize)*/
    Ref(_newsize_16820);
    _0 = _newtables_16816;
    _newtables_16816 = _33db_allocate(_newsize_16820);
    DeRef(_0);

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_nt_16814)) {
        *poke4_addr = (unsigned long)_nt_16814;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_16814)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at169_16856);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at169_16856 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at169_16856); // DJP 

    /** end procedure*/
    goto L5; // [190] 193
L5: 
    DeRefi(_put4_1__tmp_at169_16856);
    _put4_1__tmp_at169_16856 = NOVALUE;

    /** 		io:seek(current_db, tables+4)*/
    if (IS_ATOM_INT(_tables_16815)) {
        _9550 = _tables_16815 + 4;
        if ((long)((unsigned long)_9550 + (unsigned long)HIGH_BITS) >= 0) 
        _9550 = NewDouble((double)_9550);
    }
    else {
        _9550 = NewDouble(DBL_PTR(_tables_16815)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_203_16859);
    _pos_inlined_seek_at_203_16859 = _9550;
    _9550 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_203_16859);
    DeRef(_seek_1__tmp_at206_16861);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_203_16859;
    _seek_1__tmp_at206_16861 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_206_16860 = machine(19, _seek_1__tmp_at206_16861);
    DeRef(_pos_inlined_seek_at_203_16859);
    _pos_inlined_seek_at_203_16859 = NOVALUE;
    DeRef(_seek_1__tmp_at206_16861);
    _seek_1__tmp_at206_16861 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_nt_16814)) {
        _9551 = _nt_16814 - 1;
        if ((long)((unsigned long)_9551 +(unsigned long) HIGH_BITS) >= 0){
            _9551 = NewDouble((double)_9551);
        }
    }
    else {
        _9551 = NewDouble(DBL_PTR(_nt_16814)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9551)) {
        if (_9551 == (short)_9551)
        _9552 = _9551 * 16;
        else
        _9552 = NewDouble(_9551 * (double)16);
    }
    else {
        _9552 = NewDouble(DBL_PTR(_9551)->dbl * (double)16);
    }
    DeRef(_9551);
    _9551 = NOVALUE;
    _0 = _remaining_16822;
    _remaining_16822 = _15get_bytes(_33current_db_15557, _9552);
    DeRef(_0);
    _9552 = NOVALUE;

    /** 		io:seek(current_db, newtables+4)*/
    if (IS_ATOM_INT(_newtables_16816)) {
        _9554 = _newtables_16816 + 4;
        if ((long)((unsigned long)_9554 + (unsigned long)HIGH_BITS) >= 0) 
        _9554 = NewDouble((double)_9554);
    }
    else {
        _9554 = NewDouble(DBL_PTR(_newtables_16816)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_246_16867);
    _pos_inlined_seek_at_246_16867 = _9554;
    _9554 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_246_16867);
    DeRef(_seek_1__tmp_at249_16869);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_246_16867;
    _seek_1__tmp_at249_16869 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_249_16868 = machine(19, _seek_1__tmp_at249_16869);
    DeRef(_pos_inlined_seek_at_246_16867);
    _pos_inlined_seek_at_246_16867 = NOVALUE;
    DeRef(_seek_1__tmp_at249_16869);
    _seek_1__tmp_at249_16869 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_16822); // DJP 

    /** end procedure*/
    goto L6; // [273] 276
L6: 

    /** 		putn(repeat(0, newsize - 4 - (nt-1)*SIZEOF_TABLE_HEADER))*/
    if (IS_ATOM_INT(_newsize_16820)) {
        _9555 = _newsize_16820 - 4;
        if ((long)((unsigned long)_9555 +(unsigned long) HIGH_BITS) >= 0){
            _9555 = NewDouble((double)_9555);
        }
    }
    else {
        _9555 = NewDouble(DBL_PTR(_newsize_16820)->dbl - (double)4);
    }
    if (IS_ATOM_INT(_nt_16814)) {
        _9556 = _nt_16814 - 1;
        if ((long)((unsigned long)_9556 +(unsigned long) HIGH_BITS) >= 0){
            _9556 = NewDouble((double)_9556);
        }
    }
    else {
        _9556 = NewDouble(DBL_PTR(_nt_16814)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9556)) {
        if (_9556 == (short)_9556)
        _9557 = _9556 * 16;
        else
        _9557 = NewDouble(_9556 * (double)16);
    }
    else {
        _9557 = NewDouble(DBL_PTR(_9556)->dbl * (double)16);
    }
    DeRef(_9556);
    _9556 = NOVALUE;
    if (IS_ATOM_INT(_9555) && IS_ATOM_INT(_9557)) {
        _9558 = _9555 - _9557;
    }
    else {
        if (IS_ATOM_INT(_9555)) {
            _9558 = NewDouble((double)_9555 - DBL_PTR(_9557)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9557)) {
                _9558 = NewDouble(DBL_PTR(_9555)->dbl - (double)_9557);
            }
            else
            _9558 = NewDouble(DBL_PTR(_9555)->dbl - DBL_PTR(_9557)->dbl);
        }
    }
    DeRef(_9555);
    _9555 = NOVALUE;
    DeRef(_9557);
    _9557 = NOVALUE;
    _9559 = Repeat(0, _9558);
    DeRef(_9558);
    _9558 = NOVALUE;
    DeRefi(_s_inlined_putn_at_298_16877);
    _s_inlined_putn_at_298_16877 = _9559;
    _9559 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_298_16877); // DJP 

    /** end procedure*/
    goto L7; // [312] 315
L7: 
    DeRefi(_s_inlined_putn_at_298_16877);
    _s_inlined_putn_at_298_16877 = NOVALUE;

    /** 		db_free(tables)*/
    Ref(_tables_16815);
    _33db_free(_tables_16815);

    /** 		io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at326_16880);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at326_16880 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_326_16879 = machine(19, _seek_1__tmp_at326_16880);
    DeRefi(_seek_1__tmp_at326_16880);
    _seek_1__tmp_at326_16880 = NOVALUE;

    /** 		put4(newtables)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_newtables_16816)) {
        *poke4_addr = (unsigned long)_newtables_16816;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_newtables_16816)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at341_16882);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at341_16882 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at341_16882); // DJP 

    /** end procedure*/
    goto L8; // [362] 365
L8: 
    DeRefi(_put4_1__tmp_at341_16882);
    _put4_1__tmp_at341_16882 = NOVALUE;

    /** 		tables = newtables*/
    Ref(_newtables_16816);
    DeRef(_tables_16815);
    _tables_16815 = _newtables_16816;
    goto L9; // [372] 421
L4: 

    /** 		io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_16815);
    DeRef(_seek_1__tmp_at379_16886);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _tables_16815;
    _seek_1__tmp_at379_16886 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_379_16885 = machine(19, _seek_1__tmp_at379_16886);
    DeRef(_seek_1__tmp_at379_16886);
    _seek_1__tmp_at379_16886 = NOVALUE;

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_nt_16814)) {
        *poke4_addr = (unsigned long)_nt_16814;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_16814)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at394_16888);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at394_16888 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at394_16888); // DJP 

    /** end procedure*/
    goto LA; // [415] 418
LA: 
    DeRefi(_put4_1__tmp_at394_16888);
    _put4_1__tmp_at394_16888 = NOVALUE;
L9: 

    /** 	records_ptr = db_allocate(init_records * 4)*/
    if (_init_records_16812 == (short)_init_records_16812)
    _9560 = _init_records_16812 * 4;
    else
    _9560 = NewDouble(_init_records_16812 * (double)4);
    _0 = _records_ptr_16818;
    _records_ptr_16818 = _33db_allocate(_9560);
    DeRef(_0);
    _9560 = NOVALUE;

    /** 	putn(repeat(0, init_records * 4))*/
    _9562 = _init_records_16812 * 4;
    _9563 = Repeat(0, _9562);
    _9562 = NOVALUE;
    DeRefi(_s_inlined_putn_at_441_16894);
    _s_inlined_putn_at_441_16894 = _9563;
    _9563 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_441_16894); // DJP 

    /** end procedure*/
    goto LB; // [455] 458
LB: 
    DeRefi(_s_inlined_putn_at_441_16894);
    _s_inlined_putn_at_441_16894 = NOVALUE;

    /** 	index_ptr = db_allocate(init_index * 8)*/
    if (_init_index_16823 == (short)_init_index_16823)
    _9564 = _init_index_16823 * 8;
    else
    _9564 = NewDouble(_init_index_16823 * (double)8);
    _0 = _index_ptr_16821;
    _index_ptr_16821 = _33db_allocate(_9564);
    DeRef(_0);
    _9564 = NOVALUE;

    /** 	put4(0)  -- 0 records*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at472_16898);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at472_16898 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at472_16898); // DJP 

    /** end procedure*/
    goto LC; // [493] 496
LC: 
    DeRefi(_put4_1__tmp_at472_16898);
    _put4_1__tmp_at472_16898 = NOVALUE;

    /** 	put4(records_ptr) -- point to 1st block*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_records_ptr_16818)) {
        *poke4_addr = (unsigned long)_records_ptr_16818;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_records_ptr_16818)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at500_16900);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at500_16900 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at500_16900); // DJP 

    /** end procedure*/
    goto LD; // [521] 524
LD: 
    DeRefi(_put4_1__tmp_at500_16900);
    _put4_1__tmp_at500_16900 = NOVALUE;

    /** 	putn(repeat(0, (init_index-1) * 8))*/
    _9566 = _init_index_16823 - 1;
    if ((long)((unsigned long)_9566 +(unsigned long) HIGH_BITS) >= 0){
        _9566 = NewDouble((double)_9566);
    }
    if (IS_ATOM_INT(_9566)) {
        _9567 = _9566 * 8;
    }
    else {
        _9567 = NewDouble(DBL_PTR(_9566)->dbl * (double)8);
    }
    DeRef(_9566);
    _9566 = NOVALUE;
    _9568 = Repeat(0, _9567);
    DeRef(_9567);
    _9567 = NOVALUE;
    DeRefi(_s_inlined_putn_at_540_16905);
    _s_inlined_putn_at_540_16905 = _9568;
    _9568 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_540_16905); // DJP 

    /** end procedure*/
    goto LE; // [554] 557
LE: 
    DeRefi(_s_inlined_putn_at_540_16905);
    _s_inlined_putn_at_540_16905 = NOVALUE;

    /** 	name_ptr = db_allocate(length(name)+1)*/
    if (IS_SEQUENCE(_name_16811)){
            _9569 = SEQ_PTR(_name_16811)->length;
    }
    else {
        _9569 = 1;
    }
    _9570 = _9569 + 1;
    _9569 = NOVALUE;
    _0 = _name_ptr_16813;
    _name_ptr_16813 = _33db_allocate(_9570);
    DeRef(_0);
    _9570 = NOVALUE;

    /** 	putn(name & 0)*/
    Append(&_9572, _name_16811, 0);
    DeRef(_s_inlined_putn_at_578_16911);
    _s_inlined_putn_at_578_16911 = _9572;
    _9572 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_578_16911); // DJP 

    /** end procedure*/
    goto LF; // [592] 595
LF: 
    DeRef(_s_inlined_putn_at_578_16911);
    _s_inlined_putn_at_578_16911 = NOVALUE;

    /** 	io:seek(current_db, tables+4+(nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_tables_16815)) {
        _9573 = _tables_16815 + 4;
        if ((long)((unsigned long)_9573 + (unsigned long)HIGH_BITS) >= 0) 
        _9573 = NewDouble((double)_9573);
    }
    else {
        _9573 = NewDouble(DBL_PTR(_tables_16815)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_nt_16814)) {
        _9574 = _nt_16814 - 1;
        if ((long)((unsigned long)_9574 +(unsigned long) HIGH_BITS) >= 0){
            _9574 = NewDouble((double)_9574);
        }
    }
    else {
        _9574 = NewDouble(DBL_PTR(_nt_16814)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9574)) {
        if (_9574 == (short)_9574)
        _9575 = _9574 * 16;
        else
        _9575 = NewDouble(_9574 * (double)16);
    }
    else {
        _9575 = NewDouble(DBL_PTR(_9574)->dbl * (double)16);
    }
    DeRef(_9574);
    _9574 = NOVALUE;
    if (IS_ATOM_INT(_9573) && IS_ATOM_INT(_9575)) {
        _9576 = _9573 + _9575;
        if ((long)((unsigned long)_9576 + (unsigned long)HIGH_BITS) >= 0) 
        _9576 = NewDouble((double)_9576);
    }
    else {
        if (IS_ATOM_INT(_9573)) {
            _9576 = NewDouble((double)_9573 + DBL_PTR(_9575)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9575)) {
                _9576 = NewDouble(DBL_PTR(_9573)->dbl + (double)_9575);
            }
            else
            _9576 = NewDouble(DBL_PTR(_9573)->dbl + DBL_PTR(_9575)->dbl);
        }
    }
    DeRef(_9573);
    _9573 = NOVALUE;
    DeRef(_9575);
    _9575 = NOVALUE;
    DeRef(_pos_inlined_seek_at_617_16917);
    _pos_inlined_seek_at_617_16917 = _9576;
    _9576 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_617_16917);
    DeRef(_seek_1__tmp_at620_16919);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_617_16917;
    _seek_1__tmp_at620_16919 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_620_16918 = machine(19, _seek_1__tmp_at620_16919);
    DeRef(_pos_inlined_seek_at_617_16917);
    _pos_inlined_seek_at_617_16917 = NOVALUE;
    DeRef(_seek_1__tmp_at620_16919);
    _seek_1__tmp_at620_16919 = NOVALUE;

    /** 	put4(name_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_name_ptr_16813)) {
        *poke4_addr = (unsigned long)_name_ptr_16813;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_name_ptr_16813)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at635_16921);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at635_16921 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at635_16921); // DJP 

    /** end procedure*/
    goto L10; // [656] 659
L10: 
    DeRefi(_put4_1__tmp_at635_16921);
    _put4_1__tmp_at635_16921 = NOVALUE;

    /** 	put4(0)  -- start with 0 records total*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at663_16923);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at663_16923 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at663_16923); // DJP 

    /** end procedure*/
    goto L11; // [684] 687
L11: 
    DeRefi(_put4_1__tmp_at663_16923);
    _put4_1__tmp_at663_16923 = NOVALUE;

    /** 	put4(1)  -- start with 1 block of records in index*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)1;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at691_16925);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at691_16925 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at691_16925); // DJP 

    /** end procedure*/
    goto L12; // [712] 715
L12: 
    DeRefi(_put4_1__tmp_at691_16925);
    _put4_1__tmp_at691_16925 = NOVALUE;

    /** 	put4(index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_index_ptr_16821)) {
        *poke4_addr = (unsigned long)_index_ptr_16821;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_index_ptr_16821)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at719_16927);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at719_16927 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at719_16927); // DJP 

    /** end procedure*/
    goto L13; // [740] 743
L13: 
    DeRefi(_put4_1__tmp_at719_16927);
    _put4_1__tmp_at719_16927 = NOVALUE;

    /** 	if db_select_table(name) then*/
    RefDS(_name_16811);
    _9577 = _33db_select_table(_name_16811);
    if (_9577 == 0) {
        DeRef(_9577);
        _9577 = NOVALUE;
        goto L14; // [751] 755
    }
    else {
        if (!IS_ATOM_INT(_9577) && DBL_PTR(_9577)->dbl == 0.0){
            DeRef(_9577);
            _9577 = NOVALUE;
            goto L14; // [751] 755
        }
        DeRef(_9577);
        _9577 = NOVALUE;
    }
    DeRef(_9577);
    _9577 = NOVALUE;
L14: 

    /** 	return DB_OK*/
    DeRefDS(_name_16811);
    DeRef(_name_ptr_16813);
    DeRef(_nt_16814);
    DeRef(_tables_16815);
    DeRef(_newtables_16816);
    DeRef(_table_16817);
    DeRef(_records_ptr_16818);
    DeRef(_size_16819);
    DeRef(_newsize_16820);
    DeRef(_index_ptr_16821);
    DeRef(_remaining_16822);
    return 0;
    ;
}


void _33db_delete_table(int _name_16932)
{
    int _table_16933 = NOVALUE;
    int _tables_16934 = NOVALUE;
    int _nt_16935 = NOVALUE;
    int _nrecs_16936 = NOVALUE;
    int _records_ptr_16937 = NOVALUE;
    int _blocks_16938 = NOVALUE;
    int _p_16939 = NOVALUE;
    int _data_ptr_16940 = NOVALUE;
    int _index_16941 = NOVALUE;
    int _remaining_16942 = NOVALUE;
    int _k_16943 = NOVALUE;
    int _seek_1__tmp_at24_16949 = NOVALUE;
    int _seek_inlined_seek_at_24_16948 = NOVALUE;
    int _seek_1__tmp_at56_16955 = NOVALUE;
    int _seek_inlined_seek_at_56_16954 = NOVALUE;
    int _pos_inlined_seek_at_53_16953 = NOVALUE;
    int _seek_1__tmp_at112_16967 = NOVALUE;
    int _seek_inlined_seek_at_112_16966 = NOVALUE;
    int _pos_inlined_seek_at_109_16965 = NOVALUE;
    int _seek_1__tmp_at163_16978 = NOVALUE;
    int _seek_inlined_seek_at_163_16977 = NOVALUE;
    int _pos_inlined_seek_at_160_16976 = NOVALUE;
    int _seek_1__tmp_at185_16982 = NOVALUE;
    int _seek_inlined_seek_at_185_16981 = NOVALUE;
    int _seek_1__tmp_at241_16986 = NOVALUE;
    int _seek_inlined_seek_at_241_16985 = NOVALUE;
    int _seek_1__tmp_at263_16990 = NOVALUE;
    int _seek_inlined_seek_at_263_16989 = NOVALUE;
    int _seek_1__tmp_at292_16996 = NOVALUE;
    int _seek_inlined_seek_at_292_16995 = NOVALUE;
    int _pos_inlined_seek_at_289_16994 = NOVALUE;
    int _seek_1__tmp_at340_17005 = NOVALUE;
    int _seek_inlined_seek_at_340_17004 = NOVALUE;
    int _seek_1__tmp_at377_17010 = NOVALUE;
    int _seek_inlined_seek_at_377_17009 = NOVALUE;
    int _put4_1__tmp_at392_17012 = NOVALUE;
    int _seek_1__tmp_at507_17026 = NOVALUE;
    int _seek_inlined_seek_at_507_17025 = NOVALUE;
    int _seek_1__tmp_at529_17030 = NOVALUE;
    int _seek_inlined_seek_at_529_17029 = NOVALUE;
    int _9605 = NOVALUE;
    int _9602 = NOVALUE;
    int _9601 = NOVALUE;
    int _9600 = NOVALUE;
    int _9599 = NOVALUE;
    int _9598 = NOVALUE;
    int _9597 = NOVALUE;
    int _9592 = NOVALUE;
    int _9591 = NOVALUE;
    int _9590 = NOVALUE;
    int _9587 = NOVALUE;
    int _9586 = NOVALUE;
    int _9585 = NOVALUE;
    int _9581 = NOVALUE;
    int _9580 = NOVALUE;
    int _0, _1, _2;
    

    /** 	table = table_find(name)*/
    RefDS(_name_16932);
    _0 = _table_16933;
    _table_16933 = _33table_find(_name_16932);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_16933, -1)){
        goto L1; // [11] 21
    }

    /** 		return*/
    DeRefDS(_name_16932);
    DeRef(_table_16933);
    DeRef(_tables_16934);
    DeRef(_nt_16935);
    DeRef(_nrecs_16936);
    DeRef(_records_ptr_16937);
    DeRef(_blocks_16938);
    DeRef(_p_16939);
    DeRef(_data_ptr_16940);
    DeRef(_index_16941);
    DeRef(_remaining_16942);
    return;
L1: 

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_16933);
    DeRef(_seek_1__tmp_at24_16949);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _table_16933;
    _seek_1__tmp_at24_16949 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_24_16948 = machine(19, _seek_1__tmp_at24_16949);
    DeRef(_seek_1__tmp_at24_16949);
    _seek_1__tmp_at24_16949 = NOVALUE;

    /** 	db_free(get4())*/
    _9580 = _33get4();
    _33db_free(_9580);
    _9580 = NOVALUE;

    /** 	io:seek(current_db, table+4)*/
    if (IS_ATOM_INT(_table_16933)) {
        _9581 = _table_16933 + 4;
        if ((long)((unsigned long)_9581 + (unsigned long)HIGH_BITS) >= 0) 
        _9581 = NewDouble((double)_9581);
    }
    else {
        _9581 = NewDouble(DBL_PTR(_table_16933)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_53_16953);
    _pos_inlined_seek_at_53_16953 = _9581;
    _9581 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_53_16953);
    DeRef(_seek_1__tmp_at56_16955);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_53_16953;
    _seek_1__tmp_at56_16955 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_56_16954 = machine(19, _seek_1__tmp_at56_16955);
    DeRef(_pos_inlined_seek_at_53_16953);
    _pos_inlined_seek_at_53_16953 = NOVALUE;
    DeRef(_seek_1__tmp_at56_16955);
    _seek_1__tmp_at56_16955 = NOVALUE;

    /** 	nrecs = get4()*/
    _0 = _nrecs_16936;
    _nrecs_16936 = _33get4();
    DeRef(_0);

    /** 	blocks = get4()*/
    _0 = _blocks_16938;
    _blocks_16938 = _33get4();
    DeRef(_0);

    /** 	index = get4()*/
    _0 = _index_16941;
    _index_16941 = _33get4();
    DeRef(_0);

    /** 	for b = 0 to blocks-1 do*/
    if (IS_ATOM_INT(_blocks_16938)) {
        _9585 = _blocks_16938 - 1;
        if ((long)((unsigned long)_9585 +(unsigned long) HIGH_BITS) >= 0){
            _9585 = NewDouble((double)_9585);
        }
    }
    else {
        _9585 = NewDouble(DBL_PTR(_blocks_16938)->dbl - (double)1);
    }
    {
        int _b_16960;
        _b_16960 = 0;
L2: 
        if (binary_op_a(GREATER, _b_16960, _9585)){
            goto L3; // [91] 233
        }

        /** 		io:seek(current_db, index+b*8)*/
        if (IS_ATOM_INT(_b_16960)) {
            if (_b_16960 == (short)_b_16960)
            _9586 = _b_16960 * 8;
            else
            _9586 = NewDouble(_b_16960 * (double)8);
        }
        else {
            _9586 = NewDouble(DBL_PTR(_b_16960)->dbl * (double)8);
        }
        if (IS_ATOM_INT(_index_16941) && IS_ATOM_INT(_9586)) {
            _9587 = _index_16941 + _9586;
            if ((long)((unsigned long)_9587 + (unsigned long)HIGH_BITS) >= 0) 
            _9587 = NewDouble((double)_9587);
        }
        else {
            if (IS_ATOM_INT(_index_16941)) {
                _9587 = NewDouble((double)_index_16941 + DBL_PTR(_9586)->dbl);
            }
            else {
                if (IS_ATOM_INT(_9586)) {
                    _9587 = NewDouble(DBL_PTR(_index_16941)->dbl + (double)_9586);
                }
                else
                _9587 = NewDouble(DBL_PTR(_index_16941)->dbl + DBL_PTR(_9586)->dbl);
            }
        }
        DeRef(_9586);
        _9586 = NOVALUE;
        DeRef(_pos_inlined_seek_at_109_16965);
        _pos_inlined_seek_at_109_16965 = _9587;
        _9587 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_109_16965);
        DeRef(_seek_1__tmp_at112_16967);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _pos_inlined_seek_at_109_16965;
        _seek_1__tmp_at112_16967 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_112_16966 = machine(19, _seek_1__tmp_at112_16967);
        DeRef(_pos_inlined_seek_at_109_16965);
        _pos_inlined_seek_at_109_16965 = NOVALUE;
        DeRef(_seek_1__tmp_at112_16967);
        _seek_1__tmp_at112_16967 = NOVALUE;

        /** 		nrecs = get4()*/
        _0 = _nrecs_16936;
        _nrecs_16936 = _33get4();
        DeRef(_0);

        /** 		records_ptr = get4()*/
        _0 = _records_ptr_16937;
        _records_ptr_16937 = _33get4();
        DeRef(_0);

        /** 		for r = 0 to nrecs-1 do*/
        if (IS_ATOM_INT(_nrecs_16936)) {
            _9590 = _nrecs_16936 - 1;
            if ((long)((unsigned long)_9590 +(unsigned long) HIGH_BITS) >= 0){
                _9590 = NewDouble((double)_9590);
            }
        }
        else {
            _9590 = NewDouble(DBL_PTR(_nrecs_16936)->dbl - (double)1);
        }
        {
            int _r_16971;
            _r_16971 = 0;
L4: 
            if (binary_op_a(GREATER, _r_16971, _9590)){
                goto L5; // [142] 221
            }

            /** 			io:seek(current_db, records_ptr + r*4)*/
            if (IS_ATOM_INT(_r_16971)) {
                if (_r_16971 == (short)_r_16971)
                _9591 = _r_16971 * 4;
                else
                _9591 = NewDouble(_r_16971 * (double)4);
            }
            else {
                _9591 = NewDouble(DBL_PTR(_r_16971)->dbl * (double)4);
            }
            if (IS_ATOM_INT(_records_ptr_16937) && IS_ATOM_INT(_9591)) {
                _9592 = _records_ptr_16937 + _9591;
                if ((long)((unsigned long)_9592 + (unsigned long)HIGH_BITS) >= 0) 
                _9592 = NewDouble((double)_9592);
            }
            else {
                if (IS_ATOM_INT(_records_ptr_16937)) {
                    _9592 = NewDouble((double)_records_ptr_16937 + DBL_PTR(_9591)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_9591)) {
                        _9592 = NewDouble(DBL_PTR(_records_ptr_16937)->dbl + (double)_9591);
                    }
                    else
                    _9592 = NewDouble(DBL_PTR(_records_ptr_16937)->dbl + DBL_PTR(_9591)->dbl);
                }
            }
            DeRef(_9591);
            _9591 = NOVALUE;
            DeRef(_pos_inlined_seek_at_160_16976);
            _pos_inlined_seek_at_160_16976 = _9592;
            _9592 = NOVALUE;

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_pos_inlined_seek_at_160_16976);
            DeRef(_seek_1__tmp_at163_16978);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _33current_db_15557;
            ((int *)_2)[2] = _pos_inlined_seek_at_160_16976;
            _seek_1__tmp_at163_16978 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_163_16977 = machine(19, _seek_1__tmp_at163_16978);
            DeRef(_pos_inlined_seek_at_160_16976);
            _pos_inlined_seek_at_160_16976 = NOVALUE;
            DeRef(_seek_1__tmp_at163_16978);
            _seek_1__tmp_at163_16978 = NOVALUE;

            /** 			p = get4()*/
            _0 = _p_16939;
            _p_16939 = _33get4();
            DeRef(_0);

            /** 			io:seek(current_db, p)*/

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_p_16939);
            DeRef(_seek_1__tmp_at185_16982);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _33current_db_15557;
            ((int *)_2)[2] = _p_16939;
            _seek_1__tmp_at185_16982 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_185_16981 = machine(19, _seek_1__tmp_at185_16982);
            DeRef(_seek_1__tmp_at185_16982);
            _seek_1__tmp_at185_16982 = NOVALUE;

            /** 			data_ptr = get4()*/
            _0 = _data_ptr_16940;
            _data_ptr_16940 = _33get4();
            DeRef(_0);

            /** 			db_free(data_ptr)*/
            Ref(_data_ptr_16940);
            _33db_free(_data_ptr_16940);

            /** 			db_free(p)*/
            Ref(_p_16939);
            _33db_free(_p_16939);

            /** 		end for*/
            _0 = _r_16971;
            if (IS_ATOM_INT(_r_16971)) {
                _r_16971 = _r_16971 + 1;
                if ((long)((unsigned long)_r_16971 +(unsigned long) HIGH_BITS) >= 0){
                    _r_16971 = NewDouble((double)_r_16971);
                }
            }
            else {
                _r_16971 = binary_op_a(PLUS, _r_16971, 1);
            }
            DeRef(_0);
            goto L4; // [216] 149
L5: 
            ;
            DeRef(_r_16971);
        }

        /** 		db_free(records_ptr)*/
        Ref(_records_ptr_16937);
        _33db_free(_records_ptr_16937);

        /** 	end for*/
        _0 = _b_16960;
        if (IS_ATOM_INT(_b_16960)) {
            _b_16960 = _b_16960 + 1;
            if ((long)((unsigned long)_b_16960 +(unsigned long) HIGH_BITS) >= 0){
                _b_16960 = NewDouble((double)_b_16960);
            }
        }
        else {
            _b_16960 = binary_op_a(PLUS, _b_16960, 1);
        }
        DeRef(_0);
        goto L2; // [228] 98
L3: 
        ;
        DeRef(_b_16960);
    }

    /** 	db_free(index)*/
    Ref(_index_16941);
    _33db_free(_index_16941);

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at241_16986);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at241_16986 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_241_16985 = machine(19, _seek_1__tmp_at241_16986);
    DeRefi(_seek_1__tmp_at241_16986);
    _seek_1__tmp_at241_16986 = NOVALUE;

    /** 	tables = get4()*/
    _0 = _tables_16934;
    _tables_16934 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_16934);
    DeRef(_seek_1__tmp_at263_16990);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _tables_16934;
    _seek_1__tmp_at263_16990 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_263_16989 = machine(19, _seek_1__tmp_at263_16990);
    DeRef(_seek_1__tmp_at263_16990);
    _seek_1__tmp_at263_16990 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_16935;
    _nt_16935 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, table+SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_table_16933)) {
        _9597 = _table_16933 + 16;
        if ((long)((unsigned long)_9597 + (unsigned long)HIGH_BITS) >= 0) 
        _9597 = NewDouble((double)_9597);
    }
    else {
        _9597 = NewDouble(DBL_PTR(_table_16933)->dbl + (double)16);
    }
    DeRef(_pos_inlined_seek_at_289_16994);
    _pos_inlined_seek_at_289_16994 = _9597;
    _9597 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_289_16994);
    DeRef(_seek_1__tmp_at292_16996);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_289_16994;
    _seek_1__tmp_at292_16996 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_292_16995 = machine(19, _seek_1__tmp_at292_16996);
    DeRef(_pos_inlined_seek_at_289_16994);
    _pos_inlined_seek_at_289_16994 = NOVALUE;
    DeRef(_seek_1__tmp_at292_16996);
    _seek_1__tmp_at292_16996 = NOVALUE;

    /** 	remaining = io:get_bytes(current_db,*/
    if (IS_ATOM_INT(_tables_16934)) {
        _9598 = _tables_16934 + 4;
        if ((long)((unsigned long)_9598 + (unsigned long)HIGH_BITS) >= 0) 
        _9598 = NewDouble((double)_9598);
    }
    else {
        _9598 = NewDouble(DBL_PTR(_tables_16934)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_nt_16935)) {
        if (_nt_16935 == (short)_nt_16935)
        _9599 = _nt_16935 * 16;
        else
        _9599 = NewDouble(_nt_16935 * (double)16);
    }
    else {
        _9599 = NewDouble(DBL_PTR(_nt_16935)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_9598) && IS_ATOM_INT(_9599)) {
        _9600 = _9598 + _9599;
        if ((long)((unsigned long)_9600 + (unsigned long)HIGH_BITS) >= 0) 
        _9600 = NewDouble((double)_9600);
    }
    else {
        if (IS_ATOM_INT(_9598)) {
            _9600 = NewDouble((double)_9598 + DBL_PTR(_9599)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9599)) {
                _9600 = NewDouble(DBL_PTR(_9598)->dbl + (double)_9599);
            }
            else
            _9600 = NewDouble(DBL_PTR(_9598)->dbl + DBL_PTR(_9599)->dbl);
        }
    }
    DeRef(_9598);
    _9598 = NOVALUE;
    DeRef(_9599);
    _9599 = NOVALUE;
    if (IS_ATOM_INT(_table_16933)) {
        _9601 = _table_16933 + 16;
        if ((long)((unsigned long)_9601 + (unsigned long)HIGH_BITS) >= 0) 
        _9601 = NewDouble((double)_9601);
    }
    else {
        _9601 = NewDouble(DBL_PTR(_table_16933)->dbl + (double)16);
    }
    if (IS_ATOM_INT(_9600) && IS_ATOM_INT(_9601)) {
        _9602 = _9600 - _9601;
        if ((long)((unsigned long)_9602 +(unsigned long) HIGH_BITS) >= 0){
            _9602 = NewDouble((double)_9602);
        }
    }
    else {
        if (IS_ATOM_INT(_9600)) {
            _9602 = NewDouble((double)_9600 - DBL_PTR(_9601)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9601)) {
                _9602 = NewDouble(DBL_PTR(_9600)->dbl - (double)_9601);
            }
            else
            _9602 = NewDouble(DBL_PTR(_9600)->dbl - DBL_PTR(_9601)->dbl);
        }
    }
    DeRef(_9600);
    _9600 = NOVALUE;
    DeRef(_9601);
    _9601 = NOVALUE;
    _0 = _remaining_16942;
    _remaining_16942 = _15get_bytes(_33current_db_15557, _9602);
    DeRef(_0);
    _9602 = NOVALUE;

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_16933);
    DeRef(_seek_1__tmp_at340_17005);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _table_16933;
    _seek_1__tmp_at340_17005 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_340_17004 = machine(19, _seek_1__tmp_at340_17005);
    DeRef(_seek_1__tmp_at340_17005);
    _seek_1__tmp_at340_17005 = NOVALUE;

    /** 	putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_16942); // DJP 

    /** end procedure*/
    goto L6; // [365] 368
L6: 

    /** 	nt -= 1*/
    _0 = _nt_16935;
    if (IS_ATOM_INT(_nt_16935)) {
        _nt_16935 = _nt_16935 - 1;
        if ((long)((unsigned long)_nt_16935 +(unsigned long) HIGH_BITS) >= 0){
            _nt_16935 = NewDouble((double)_nt_16935);
        }
    }
    else {
        _nt_16935 = NewDouble(DBL_PTR(_nt_16935)->dbl - (double)1);
    }
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_16934);
    DeRef(_seek_1__tmp_at377_17010);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _tables_16934;
    _seek_1__tmp_at377_17010 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_377_17009 = machine(19, _seek_1__tmp_at377_17010);
    DeRef(_seek_1__tmp_at377_17010);
    _seek_1__tmp_at377_17010 = NOVALUE;

    /** 	put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_nt_16935)) {
        *poke4_addr = (unsigned long)_nt_16935;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_16935)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at392_17012);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at392_17012 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at392_17012); // DJP 

    /** end procedure*/
    goto L7; // [414] 417
L7: 
    DeRefi(_put4_1__tmp_at392_17012);
    _put4_1__tmp_at392_17012 = NOVALUE;

    /** 	k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_33current_table_pos_15558);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _33current_table_pos_15558;
    _9605 = MAKE_SEQ(_1);
    _k_16943 = find_from(_9605, _33cache_index_15566, 1);
    DeRefDS(_9605);
    _9605 = NOVALUE;

    /** 	if k != 0 then*/
    if (_k_16943 == 0)
    goto L8; // [440] 463

    /** 		cache_index = remove(cache_index, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33cache_index_15566);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_16943)) ? _k_16943 : (long)(DBL_PTR(_k_16943)->dbl);
        int stop = (IS_ATOM_INT(_k_16943)) ? _k_16943 : (long)(DBL_PTR(_k_16943)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33cache_index_15566), start, &_33cache_index_15566 );
            }
            else Tail(SEQ_PTR(_33cache_index_15566), stop+1, &_33cache_index_15566);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33cache_index_15566), start, &_33cache_index_15566);
        }
        else {
            assign_slice_seq = &assign_space;
            _33cache_index_15566 = Remove_elements(start, stop, (SEQ_PTR(_33cache_index_15566)->ref == 1));
        }
    }

    /** 		key_cache = remove(key_cache, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33key_cache_15565);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_16943)) ? _k_16943 : (long)(DBL_PTR(_k_16943)->dbl);
        int stop = (IS_ATOM_INT(_k_16943)) ? _k_16943 : (long)(DBL_PTR(_k_16943)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33key_cache_15565), start, &_33key_cache_15565 );
            }
            else Tail(SEQ_PTR(_33key_cache_15565), stop+1, &_33key_cache_15565);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33key_cache_15565), start, &_33key_cache_15565);
        }
        else {
            assign_slice_seq = &assign_space;
            _33key_cache_15565 = Remove_elements(start, stop, (SEQ_PTR(_33key_cache_15565)->ref == 1));
        }
    }
L8: 

    /** 	if table = current_table_pos then*/
    if (binary_op_a(NOTEQ, _table_16933, _33current_table_pos_15558)){
        goto L9; // [467] 486
    }

    /** 		current_table_pos = -1*/
    DeRef(_33current_table_pos_15558);
    _33current_table_pos_15558 = -1;

    /** 		current_table_name = ""*/
    RefDS(_5);
    DeRef(_33current_table_name_15559);
    _33current_table_name_15559 = _5;
    goto LA; // [483] 552
L9: 

    /** 	elsif table < current_table_pos then*/
    if (binary_op_a(GREATEREQ, _table_16933, _33current_table_pos_15558)){
        goto LB; // [490] 551
    }

    /** 		current_table_pos -= SIZEOF_TABLE_HEADER*/
    _0 = _33current_table_pos_15558;
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _33current_table_pos_15558 = _33current_table_pos_15558 - 16;
        if ((long)((unsigned long)_33current_table_pos_15558 +(unsigned long) HIGH_BITS) >= 0){
            _33current_table_pos_15558 = NewDouble((double)_33current_table_pos_15558);
        }
    }
    else {
        _33current_table_pos_15558 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl - (double)16);
    }
    DeRef(_0);

    /** 		io:seek(current_db, current_table_pos)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_33current_table_pos_15558);
    DeRef(_seek_1__tmp_at507_17026);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _33current_table_pos_15558;
    _seek_1__tmp_at507_17026 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_507_17025 = machine(19, _seek_1__tmp_at507_17026);
    DeRef(_seek_1__tmp_at507_17026);
    _seek_1__tmp_at507_17026 = NOVALUE;

    /** 		data_ptr = get4()*/
    _0 = _data_ptr_16940;
    _data_ptr_16940 = _33get4();
    DeRef(_0);

    /** 		io:seek(current_db, data_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_data_ptr_16940);
    DeRef(_seek_1__tmp_at529_17030);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _data_ptr_16940;
    _seek_1__tmp_at529_17030 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_529_17029 = machine(19, _seek_1__tmp_at529_17030);
    DeRef(_seek_1__tmp_at529_17030);
    _seek_1__tmp_at529_17030 = NOVALUE;

    /** 		current_table_name = get_string()*/
    _0 = _33get_string();
    DeRef(_33current_table_name_15559);
    _33current_table_name_15559 = _0;
LB: 
LA: 

    /** end procedure*/
    DeRefDS(_name_16932);
    DeRef(_table_16933);
    DeRef(_tables_16934);
    DeRef(_nt_16935);
    DeRef(_nrecs_16936);
    DeRef(_records_ptr_16937);
    DeRef(_blocks_16938);
    DeRef(_p_16939);
    DeRef(_data_ptr_16940);
    DeRef(_index_16941);
    DeRef(_remaining_16942);
    DeRef(_9585);
    _9585 = NOVALUE;
    DeRef(_9590);
    _9590 = NOVALUE;
    return;
    ;
}


void _33db_clear_table(int _name_17034, int _init_records_17035)
{
    int _table_17036 = NOVALUE;
    int _nrecs_17037 = NOVALUE;
    int _records_ptr_17038 = NOVALUE;
    int _blocks_17039 = NOVALUE;
    int _p_17040 = NOVALUE;
    int _data_ptr_17041 = NOVALUE;
    int _index_ptr_17042 = NOVALUE;
    int _k_17043 = NOVALUE;
    int _init_index_17044 = NOVALUE;
    int _seek_1__tmp_at63_17056 = NOVALUE;
    int _seek_inlined_seek_at_63_17055 = NOVALUE;
    int _pos_inlined_seek_at_60_17054 = NOVALUE;
    int _seek_1__tmp_at119_17068 = NOVALUE;
    int _seek_inlined_seek_at_119_17067 = NOVALUE;
    int _pos_inlined_seek_at_116_17066 = NOVALUE;
    int _seek_1__tmp_at170_17079 = NOVALUE;
    int _seek_inlined_seek_at_170_17078 = NOVALUE;
    int _pos_inlined_seek_at_167_17077 = NOVALUE;
    int _seek_1__tmp_at192_17083 = NOVALUE;
    int _seek_inlined_seek_at_192_17082 = NOVALUE;
    int _s_inlined_putn_at_264_17090 = NOVALUE;
    int _put4_1__tmp_at295_17094 = NOVALUE;
    int _put4_1__tmp_at323_17096 = NOVALUE;
    int _s_inlined_putn_at_363_17101 = NOVALUE;
    int _seek_1__tmp_at393_17106 = NOVALUE;
    int _seek_inlined_seek_at_393_17105 = NOVALUE;
    int _pos_inlined_seek_at_390_17104 = NOVALUE;
    int _put4_1__tmp_at408_17108 = NOVALUE;
    int _put4_1__tmp_at436_17110 = NOVALUE;
    int _put4_1__tmp_at464_17112 = NOVALUE;
    int _9644 = NOVALUE;
    int _9643 = NOVALUE;
    int _9642 = NOVALUE;
    int _9641 = NOVALUE;
    int _9640 = NOVALUE;
    int _9638 = NOVALUE;
    int _9637 = NOVALUE;
    int _9636 = NOVALUE;
    int _9634 = NOVALUE;
    int _9631 = NOVALUE;
    int _9630 = NOVALUE;
    int _9629 = NOVALUE;
    int _9626 = NOVALUE;
    int _9625 = NOVALUE;
    int _9624 = NOVALUE;
    int _9620 = NOVALUE;
    int _9618 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_init_records_17035)) {
        _1 = (long)(DBL_PTR(_init_records_17035)->dbl);
        if (UNIQUE(DBL_PTR(_init_records_17035)) && (DBL_PTR(_init_records_17035)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_records_17035);
        _init_records_17035 = _1;
    }

    /** 	table = table_find(name)*/
    RefDS(_name_17034);
    _0 = _table_17036;
    _table_17036 = _33table_find(_name_17034);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_17036, -1)){
        goto L1; // [15] 25
    }

    /** 		return*/
    DeRefDS(_name_17034);
    DeRef(_table_17036);
    DeRef(_nrecs_17037);
    DeRef(_records_ptr_17038);
    DeRef(_blocks_17039);
    DeRef(_p_17040);
    DeRef(_data_ptr_17041);
    DeRef(_index_ptr_17042);
    return;
L1: 

    /** 	if init_records < 1 then*/
    if (_init_records_17035 >= 1)
    goto L2; // [27] 39

    /** 		init_records = 1*/
    _init_records_17035 = 1;
L2: 

    /** 	init_index = math:min({init_records, MAX_INDEX})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _init_records_17035;
    ((int *)_2)[2] = 10;
    _9618 = MAKE_SEQ(_1);
    _init_index_17044 = _18min(_9618);
    _9618 = NOVALUE;
    if (!IS_ATOM_INT(_init_index_17044)) {
        _1 = (long)(DBL_PTR(_init_index_17044)->dbl);
        if (UNIQUE(DBL_PTR(_init_index_17044)) && (DBL_PTR(_init_index_17044)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_index_17044);
        _init_index_17044 = _1;
    }

    /** 	io:seek(current_db, table + 4)*/
    if (IS_ATOM_INT(_table_17036)) {
        _9620 = _table_17036 + 4;
        if ((long)((unsigned long)_9620 + (unsigned long)HIGH_BITS) >= 0) 
        _9620 = NewDouble((double)_9620);
    }
    else {
        _9620 = NewDouble(DBL_PTR(_table_17036)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_60_17054);
    _pos_inlined_seek_at_60_17054 = _9620;
    _9620 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_60_17054);
    DeRef(_seek_1__tmp_at63_17056);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_60_17054;
    _seek_1__tmp_at63_17056 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_63_17055 = machine(19, _seek_1__tmp_at63_17056);
    DeRef(_pos_inlined_seek_at_60_17054);
    _pos_inlined_seek_at_60_17054 = NOVALUE;
    DeRef(_seek_1__tmp_at63_17056);
    _seek_1__tmp_at63_17056 = NOVALUE;

    /** 	nrecs = get4()*/
    _0 = _nrecs_17037;
    _nrecs_17037 = _33get4();
    DeRef(_0);

    /** 	blocks = get4()*/
    _0 = _blocks_17039;
    _blocks_17039 = _33get4();
    DeRef(_0);

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_17042;
    _index_ptr_17042 = _33get4();
    DeRef(_0);

    /** 	for b = 0 to blocks-1 do*/
    if (IS_ATOM_INT(_blocks_17039)) {
        _9624 = _blocks_17039 - 1;
        if ((long)((unsigned long)_9624 +(unsigned long) HIGH_BITS) >= 0){
            _9624 = NewDouble((double)_9624);
        }
    }
    else {
        _9624 = NewDouble(DBL_PTR(_blocks_17039)->dbl - (double)1);
    }
    {
        int _b_17061;
        _b_17061 = 0;
L3: 
        if (binary_op_a(GREATER, _b_17061, _9624)){
            goto L4; // [98] 240
        }

        /** 		io:seek(current_db, index_ptr + b*8)*/
        if (IS_ATOM_INT(_b_17061)) {
            if (_b_17061 == (short)_b_17061)
            _9625 = _b_17061 * 8;
            else
            _9625 = NewDouble(_b_17061 * (double)8);
        }
        else {
            _9625 = NewDouble(DBL_PTR(_b_17061)->dbl * (double)8);
        }
        if (IS_ATOM_INT(_index_ptr_17042) && IS_ATOM_INT(_9625)) {
            _9626 = _index_ptr_17042 + _9625;
            if ((long)((unsigned long)_9626 + (unsigned long)HIGH_BITS) >= 0) 
            _9626 = NewDouble((double)_9626);
        }
        else {
            if (IS_ATOM_INT(_index_ptr_17042)) {
                _9626 = NewDouble((double)_index_ptr_17042 + DBL_PTR(_9625)->dbl);
            }
            else {
                if (IS_ATOM_INT(_9625)) {
                    _9626 = NewDouble(DBL_PTR(_index_ptr_17042)->dbl + (double)_9625);
                }
                else
                _9626 = NewDouble(DBL_PTR(_index_ptr_17042)->dbl + DBL_PTR(_9625)->dbl);
            }
        }
        DeRef(_9625);
        _9625 = NOVALUE;
        DeRef(_pos_inlined_seek_at_116_17066);
        _pos_inlined_seek_at_116_17066 = _9626;
        _9626 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_116_17066);
        DeRef(_seek_1__tmp_at119_17068);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _pos_inlined_seek_at_116_17066;
        _seek_1__tmp_at119_17068 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_119_17067 = machine(19, _seek_1__tmp_at119_17068);
        DeRef(_pos_inlined_seek_at_116_17066);
        _pos_inlined_seek_at_116_17066 = NOVALUE;
        DeRef(_seek_1__tmp_at119_17068);
        _seek_1__tmp_at119_17068 = NOVALUE;

        /** 		nrecs = get4()*/
        _0 = _nrecs_17037;
        _nrecs_17037 = _33get4();
        DeRef(_0);

        /** 		records_ptr = get4()*/
        _0 = _records_ptr_17038;
        _records_ptr_17038 = _33get4();
        DeRef(_0);

        /** 		for r = 0 to nrecs-1 do*/
        if (IS_ATOM_INT(_nrecs_17037)) {
            _9629 = _nrecs_17037 - 1;
            if ((long)((unsigned long)_9629 +(unsigned long) HIGH_BITS) >= 0){
                _9629 = NewDouble((double)_9629);
            }
        }
        else {
            _9629 = NewDouble(DBL_PTR(_nrecs_17037)->dbl - (double)1);
        }
        {
            int _r_17072;
            _r_17072 = 0;
L5: 
            if (binary_op_a(GREATER, _r_17072, _9629)){
                goto L6; // [149] 228
            }

            /** 			io:seek(current_db, records_ptr + r*4)*/
            if (IS_ATOM_INT(_r_17072)) {
                if (_r_17072 == (short)_r_17072)
                _9630 = _r_17072 * 4;
                else
                _9630 = NewDouble(_r_17072 * (double)4);
            }
            else {
                _9630 = NewDouble(DBL_PTR(_r_17072)->dbl * (double)4);
            }
            if (IS_ATOM_INT(_records_ptr_17038) && IS_ATOM_INT(_9630)) {
                _9631 = _records_ptr_17038 + _9630;
                if ((long)((unsigned long)_9631 + (unsigned long)HIGH_BITS) >= 0) 
                _9631 = NewDouble((double)_9631);
            }
            else {
                if (IS_ATOM_INT(_records_ptr_17038)) {
                    _9631 = NewDouble((double)_records_ptr_17038 + DBL_PTR(_9630)->dbl);
                }
                else {
                    if (IS_ATOM_INT(_9630)) {
                        _9631 = NewDouble(DBL_PTR(_records_ptr_17038)->dbl + (double)_9630);
                    }
                    else
                    _9631 = NewDouble(DBL_PTR(_records_ptr_17038)->dbl + DBL_PTR(_9630)->dbl);
                }
            }
            DeRef(_9630);
            _9630 = NOVALUE;
            DeRef(_pos_inlined_seek_at_167_17077);
            _pos_inlined_seek_at_167_17077 = _9631;
            _9631 = NOVALUE;

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_pos_inlined_seek_at_167_17077);
            DeRef(_seek_1__tmp_at170_17079);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _33current_db_15557;
            ((int *)_2)[2] = _pos_inlined_seek_at_167_17077;
            _seek_1__tmp_at170_17079 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_170_17078 = machine(19, _seek_1__tmp_at170_17079);
            DeRef(_pos_inlined_seek_at_167_17077);
            _pos_inlined_seek_at_167_17077 = NOVALUE;
            DeRef(_seek_1__tmp_at170_17079);
            _seek_1__tmp_at170_17079 = NOVALUE;

            /** 			p = get4()*/
            _0 = _p_17040;
            _p_17040 = _33get4();
            DeRef(_0);

            /** 			io:seek(current_db, p)*/

            /** 	return machine_func(M_SEEK, {fn, pos})*/
            Ref(_p_17040);
            DeRef(_seek_1__tmp_at192_17083);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _33current_db_15557;
            ((int *)_2)[2] = _p_17040;
            _seek_1__tmp_at192_17083 = MAKE_SEQ(_1);
            _seek_inlined_seek_at_192_17082 = machine(19, _seek_1__tmp_at192_17083);
            DeRef(_seek_1__tmp_at192_17083);
            _seek_1__tmp_at192_17083 = NOVALUE;

            /** 			data_ptr = get4()*/
            _0 = _data_ptr_17041;
            _data_ptr_17041 = _33get4();
            DeRef(_0);

            /** 			db_free(data_ptr)*/
            Ref(_data_ptr_17041);
            _33db_free(_data_ptr_17041);

            /** 			db_free(p)*/
            Ref(_p_17040);
            _33db_free(_p_17040);

            /** 		end for*/
            _0 = _r_17072;
            if (IS_ATOM_INT(_r_17072)) {
                _r_17072 = _r_17072 + 1;
                if ((long)((unsigned long)_r_17072 +(unsigned long) HIGH_BITS) >= 0){
                    _r_17072 = NewDouble((double)_r_17072);
                }
            }
            else {
                _r_17072 = binary_op_a(PLUS, _r_17072, 1);
            }
            DeRef(_0);
            goto L5; // [223] 156
L6: 
            ;
            DeRef(_r_17072);
        }

        /** 		db_free(records_ptr)*/
        Ref(_records_ptr_17038);
        _33db_free(_records_ptr_17038);

        /** 	end for*/
        _0 = _b_17061;
        if (IS_ATOM_INT(_b_17061)) {
            _b_17061 = _b_17061 + 1;
            if ((long)((unsigned long)_b_17061 +(unsigned long) HIGH_BITS) >= 0){
                _b_17061 = NewDouble((double)_b_17061);
            }
        }
        else {
            _b_17061 = binary_op_a(PLUS, _b_17061, 1);
        }
        DeRef(_0);
        goto L3; // [235] 105
L4: 
        ;
        DeRef(_b_17061);
    }

    /** 	db_free(index_ptr)*/
    Ref(_index_ptr_17042);
    _33db_free(_index_ptr_17042);

    /** 	data_ptr = db_allocate(init_records * 4)*/
    if (_init_records_17035 == (short)_init_records_17035)
    _9634 = _init_records_17035 * 4;
    else
    _9634 = NewDouble(_init_records_17035 * (double)4);
    _0 = _data_ptr_17041;
    _data_ptr_17041 = _33db_allocate(_9634);
    DeRef(_0);
    _9634 = NOVALUE;

    /** 	putn(repeat(0, init_records * 4))*/
    _9636 = _init_records_17035 * 4;
    _9637 = Repeat(0, _9636);
    _9636 = NOVALUE;
    DeRefi(_s_inlined_putn_at_264_17090);
    _s_inlined_putn_at_264_17090 = _9637;
    _9637 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_264_17090); // DJP 

    /** end procedure*/
    goto L7; // [279] 282
L7: 
    DeRefi(_s_inlined_putn_at_264_17090);
    _s_inlined_putn_at_264_17090 = NOVALUE;

    /** 	index_ptr = db_allocate(init_index * 8)*/
    if (_init_index_17044 == (short)_init_index_17044)
    _9638 = _init_index_17044 * 8;
    else
    _9638 = NewDouble(_init_index_17044 * (double)8);
    _0 = _index_ptr_17042;
    _index_ptr_17042 = _33db_allocate(_9638);
    DeRef(_0);
    _9638 = NOVALUE;

    /** 	put4(0)  -- 0 records*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at295_17094);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at295_17094 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at295_17094); // DJP 

    /** end procedure*/
    goto L8; // [317] 320
L8: 
    DeRefi(_put4_1__tmp_at295_17094);
    _put4_1__tmp_at295_17094 = NOVALUE;

    /** 	put4(data_ptr) -- point to 1st block*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_17041)) {
        *poke4_addr = (unsigned long)_data_ptr_17041;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_17041)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at323_17096);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at323_17096 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at323_17096); // DJP 

    /** end procedure*/
    goto L9; // [345] 348
L9: 
    DeRefi(_put4_1__tmp_at323_17096);
    _put4_1__tmp_at323_17096 = NOVALUE;

    /** 	putn(repeat(0, (init_index-1) * 8))*/
    _9640 = _init_index_17044 - 1;
    if ((long)((unsigned long)_9640 +(unsigned long) HIGH_BITS) >= 0){
        _9640 = NewDouble((double)_9640);
    }
    if (IS_ATOM_INT(_9640)) {
        _9641 = _9640 * 8;
    }
    else {
        _9641 = NewDouble(DBL_PTR(_9640)->dbl * (double)8);
    }
    DeRef(_9640);
    _9640 = NOVALUE;
    _9642 = Repeat(0, _9641);
    DeRef(_9641);
    _9641 = NOVALUE;
    DeRefi(_s_inlined_putn_at_363_17101);
    _s_inlined_putn_at_363_17101 = _9642;
    _9642 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_363_17101); // DJP 

    /** end procedure*/
    goto LA; // [378] 381
LA: 
    DeRefi(_s_inlined_putn_at_363_17101);
    _s_inlined_putn_at_363_17101 = NOVALUE;

    /** 	io:seek(current_db, table + 4)*/
    if (IS_ATOM_INT(_table_17036)) {
        _9643 = _table_17036 + 4;
        if ((long)((unsigned long)_9643 + (unsigned long)HIGH_BITS) >= 0) 
        _9643 = NewDouble((double)_9643);
    }
    else {
        _9643 = NewDouble(DBL_PTR(_table_17036)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_390_17104);
    _pos_inlined_seek_at_390_17104 = _9643;
    _9643 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_390_17104);
    DeRef(_seek_1__tmp_at393_17106);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_390_17104;
    _seek_1__tmp_at393_17106 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_393_17105 = machine(19, _seek_1__tmp_at393_17106);
    DeRef(_pos_inlined_seek_at_390_17104);
    _pos_inlined_seek_at_390_17104 = NOVALUE;
    DeRef(_seek_1__tmp_at393_17106);
    _seek_1__tmp_at393_17106 = NOVALUE;

    /** 	put4(0)  -- start with 0 records total*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at408_17108);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at408_17108 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at408_17108); // DJP 

    /** end procedure*/
    goto LB; // [430] 433
LB: 
    DeRefi(_put4_1__tmp_at408_17108);
    _put4_1__tmp_at408_17108 = NOVALUE;

    /** 	put4(1)  -- start with 1 block of records in index*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)1;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at436_17110);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at436_17110 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at436_17110); // DJP 

    /** end procedure*/
    goto LC; // [458] 461
LC: 
    DeRefi(_put4_1__tmp_at436_17110);
    _put4_1__tmp_at436_17110 = NOVALUE;

    /** 	put4(index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_index_ptr_17042)) {
        *poke4_addr = (unsigned long)_index_ptr_17042;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_index_ptr_17042)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at464_17112);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at464_17112 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at464_17112); // DJP 

    /** end procedure*/
    goto LD; // [486] 489
LD: 
    DeRefi(_put4_1__tmp_at464_17112);
    _put4_1__tmp_at464_17112 = NOVALUE;

    /** 	k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_33current_table_pos_15558);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _33current_table_pos_15558;
    _9644 = MAKE_SEQ(_1);
    _k_17043 = find_from(_9644, _33cache_index_15566, 1);
    DeRefDS(_9644);
    _9644 = NOVALUE;

    /** 	if k != 0 then*/
    if (_k_17043 == 0)
    goto LE; // [512] 535

    /** 		cache_index = remove(cache_index, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33cache_index_15566);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_17043)) ? _k_17043 : (long)(DBL_PTR(_k_17043)->dbl);
        int stop = (IS_ATOM_INT(_k_17043)) ? _k_17043 : (long)(DBL_PTR(_k_17043)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33cache_index_15566), start, &_33cache_index_15566 );
            }
            else Tail(SEQ_PTR(_33cache_index_15566), stop+1, &_33cache_index_15566);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33cache_index_15566), start, &_33cache_index_15566);
        }
        else {
            assign_slice_seq = &assign_space;
            _33cache_index_15566 = Remove_elements(start, stop, (SEQ_PTR(_33cache_index_15566)->ref == 1));
        }
    }

    /** 		key_cache = remove(key_cache, k)*/
    {
        s1_ptr assign_space = SEQ_PTR(_33key_cache_15565);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_k_17043)) ? _k_17043 : (long)(DBL_PTR(_k_17043)->dbl);
        int stop = (IS_ATOM_INT(_k_17043)) ? _k_17043 : (long)(DBL_PTR(_k_17043)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_33key_cache_15565), start, &_33key_cache_15565 );
            }
            else Tail(SEQ_PTR(_33key_cache_15565), stop+1, &_33key_cache_15565);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_33key_cache_15565), start, &_33key_cache_15565);
        }
        else {
            assign_slice_seq = &assign_space;
            _33key_cache_15565 = Remove_elements(start, stop, (SEQ_PTR(_33key_cache_15565)->ref == 1));
        }
    }
LE: 

    /** 	if table = current_table_pos then*/
    if (binary_op_a(NOTEQ, _table_17036, _33current_table_pos_15558)){
        goto LF; // [539] 551
    }

    /** 		key_pointers = {}*/
    RefDS(_5);
    DeRef(_33key_pointers_15564);
    _33key_pointers_15564 = _5;
LF: 

    /** end procedure*/
    DeRefDS(_name_17034);
    DeRef(_table_17036);
    DeRef(_nrecs_17037);
    DeRef(_records_ptr_17038);
    DeRef(_blocks_17039);
    DeRef(_p_17040);
    DeRef(_data_ptr_17041);
    DeRef(_index_ptr_17042);
    DeRef(_9624);
    _9624 = NOVALUE;
    DeRef(_9629);
    _9629 = NOVALUE;
    return;
    ;
}


void _33db_rename_table(int _name_17123, int _new_name_17124)
{
    int _table_17125 = NOVALUE;
    int _table_ptr_17126 = NOVALUE;
    int _seek_1__tmp_at70_17140 = NOVALUE;
    int _seek_inlined_seek_at_70_17139 = NOVALUE;
    int _s_inlined_putn_at_110_17147 = NOVALUE;
    int _seek_1__tmp_at133_17150 = NOVALUE;
    int _seek_inlined_seek_at_133_17149 = NOVALUE;
    int _put4_1__tmp_at148_17152 = NOVALUE;
    int _9664 = NOVALUE;
    int _9663 = NOVALUE;
    int _9661 = NOVALUE;
    int _9660 = NOVALUE;
    int _9659 = NOVALUE;
    int _9658 = NOVALUE;
    int _9655 = NOVALUE;
    int _9654 = NOVALUE;
    int _0, _1, _2;
    

    /** 	table = table_find(name)*/
    RefDS(_name_17123);
    _0 = _table_17125;
    _table_17125 = _33table_find(_name_17123);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_17125, -1)){
        goto L1; // [13] 37
    }

    /** 		fatal(NO_TABLE, "source table doesn't exist", "db_rename_table", {name, new_name})*/
    RefDS(_new_name_17124);
    RefDS(_name_17123);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _name_17123;
    ((int *)_2)[2] = _new_name_17124;
    _9654 = MAKE_SEQ(_1);
    RefDS(_9652);
    RefDS(_9653);
    _33fatal(903, _9652, _9653, _9654);
    _9654 = NOVALUE;

    /** 		return*/
    DeRefDS(_name_17123);
    DeRefDS(_new_name_17124);
    DeRef(_table_17125);
    DeRef(_table_ptr_17126);
    return;
L1: 

    /** 	if table_find(new_name) != -1 then*/
    RefDS(_new_name_17124);
    _9655 = _33table_find(_new_name_17124);
    if (binary_op_a(EQUALS, _9655, -1)){
        DeRef(_9655);
        _9655 = NOVALUE;
        goto L2; // [43] 67
    }
    DeRef(_9655);
    _9655 = NOVALUE;

    /** 		fatal(DUP_TABLE, "target table name already exists", "db_rename_table", {name, new_name})*/
    RefDS(_new_name_17124);
    RefDS(_name_17123);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _name_17123;
    ((int *)_2)[2] = _new_name_17124;
    _9658 = MAKE_SEQ(_1);
    RefDS(_9657);
    RefDS(_9653);
    _33fatal(904, _9657, _9653, _9658);
    _9658 = NOVALUE;

    /** 		return*/
    DeRefDS(_name_17123);
    DeRefDS(_new_name_17124);
    DeRef(_table_17125);
    DeRef(_table_ptr_17126);
    return;
L2: 

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_17125);
    DeRef(_seek_1__tmp_at70_17140);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _table_17125;
    _seek_1__tmp_at70_17140 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_70_17139 = machine(19, _seek_1__tmp_at70_17140);
    DeRef(_seek_1__tmp_at70_17140);
    _seek_1__tmp_at70_17140 = NOVALUE;

    /** 	db_free(get4())*/
    _9659 = _33get4();
    _33db_free(_9659);
    _9659 = NOVALUE;

    /** 	table_ptr = db_allocate(length(new_name)+1)*/
    if (IS_SEQUENCE(_new_name_17124)){
            _9660 = SEQ_PTR(_new_name_17124)->length;
    }
    else {
        _9660 = 1;
    }
    _9661 = _9660 + 1;
    _9660 = NOVALUE;
    _0 = _table_ptr_17126;
    _table_ptr_17126 = _33db_allocate(_9661);
    DeRef(_0);
    _9661 = NOVALUE;

    /** 	putn(new_name & 0)*/
    Append(&_9663, _new_name_17124, 0);
    DeRef(_s_inlined_putn_at_110_17147);
    _s_inlined_putn_at_110_17147 = _9663;
    _9663 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_110_17147); // DJP 

    /** end procedure*/
    goto L3; // [125] 128
L3: 
    DeRef(_s_inlined_putn_at_110_17147);
    _s_inlined_putn_at_110_17147 = NOVALUE;

    /** 	io:seek(current_db, table)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_table_17125);
    DeRef(_seek_1__tmp_at133_17150);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _table_17125;
    _seek_1__tmp_at133_17150 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_133_17149 = machine(19, _seek_1__tmp_at133_17150);
    DeRef(_seek_1__tmp_at133_17150);
    _seek_1__tmp_at133_17150 = NOVALUE;

    /** 	put4(table_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_table_ptr_17126)) {
        *poke4_addr = (unsigned long)_table_ptr_17126;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_table_ptr_17126)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at148_17152);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at148_17152 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at148_17152); // DJP 

    /** end procedure*/
    goto L4; // [170] 173
L4: 
    DeRefi(_put4_1__tmp_at148_17152);
    _put4_1__tmp_at148_17152 = NOVALUE;

    /** 	if equal(current_table_name, name) then*/
    if (_33current_table_name_15559 == _name_17123)
    _9664 = 1;
    else if (IS_ATOM_INT(_33current_table_name_15559) && IS_ATOM_INT(_name_17123))
    _9664 = 0;
    else
    _9664 = (compare(_33current_table_name_15559, _name_17123) == 0);
    if (_9664 == 0)
    {
        _9664 = NOVALUE;
        goto L5; // [183] 194
    }
    else{
        _9664 = NOVALUE;
    }

    /** 		current_table_name = new_name*/
    RefDS(_new_name_17124);
    DeRefDS(_33current_table_name_15559);
    _33current_table_name_15559 = _new_name_17124;
L5: 

    /** end procedure*/
    DeRefDS(_name_17123);
    DeRefDS(_new_name_17124);
    DeRef(_table_17125);
    DeRef(_table_ptr_17126);
    return;
    ;
}


int _33db_table_list()
{
    int _seek_1__tmp_at120_17186 = NOVALUE;
    int _seek_inlined_seek_at_120_17185 = NOVALUE;
    int _seek_1__tmp_at98_17182 = NOVALUE;
    int _seek_inlined_seek_at_98_17181 = NOVALUE;
    int _pos_inlined_seek_at_95_17180 = NOVALUE;
    int _seek_1__tmp_at42_17170 = NOVALUE;
    int _seek_inlined_seek_at_42_17169 = NOVALUE;
    int _seek_1__tmp_at4_17163 = NOVALUE;
    int _seek_inlined_seek_at_4_17162 = NOVALUE;
    int _table_names_17157 = NOVALUE;
    int _tables_17158 = NOVALUE;
    int _nt_17159 = NOVALUE;
    int _name_17160 = NOVALUE;
    int _9676 = NOVALUE;
    int _9675 = NOVALUE;
    int _9673 = NOVALUE;
    int _9672 = NOVALUE;
    int _9671 = NOVALUE;
    int _9670 = NOVALUE;
    int _9665 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_17163);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at4_17163 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_17162 = machine(19, _seek_1__tmp_at4_17163);
    DeRefi(_seek_1__tmp_at4_17163);
    _seek_1__tmp_at4_17163 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return {} end if*/
    if (IS_SEQUENCE(_33vLastErrors_15581)){
            _9665 = SEQ_PTR(_33vLastErrors_15581)->length;
    }
    else {
        _9665 = 1;
    }
    if (_9665 <= 0)
    goto L1; // [25] 34
    RefDS(_5);
    DeRef(_table_names_17157);
    DeRef(_tables_17158);
    DeRef(_nt_17159);
    DeRef(_name_17160);
    return _5;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_17158;
    _tables_17158 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_17158);
    DeRef(_seek_1__tmp_at42_17170);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _tables_17158;
    _seek_1__tmp_at42_17170 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_42_17169 = machine(19, _seek_1__tmp_at42_17170);
    DeRef(_seek_1__tmp_at42_17170);
    _seek_1__tmp_at42_17170 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_17159;
    _nt_17159 = _33get4();
    DeRef(_0);

    /** 	table_names = repeat(0, nt)*/
    DeRef(_table_names_17157);
    _table_names_17157 = Repeat(0, _nt_17159);

    /** 	for i = 0 to nt-1 do*/
    if (IS_ATOM_INT(_nt_17159)) {
        _9670 = _nt_17159 - 1;
        if ((long)((unsigned long)_9670 +(unsigned long) HIGH_BITS) >= 0){
            _9670 = NewDouble((double)_9670);
        }
    }
    else {
        _9670 = NewDouble(DBL_PTR(_nt_17159)->dbl - (double)1);
    }
    {
        int _i_17174;
        _i_17174 = 0;
L2: 
        if (binary_op_a(GREATER, _i_17174, _9670)){
            goto L3; // [73] 154
        }

        /** 		io:seek(current_db, tables + 4 + i*SIZEOF_TABLE_HEADER)*/
        if (IS_ATOM_INT(_tables_17158)) {
            _9671 = _tables_17158 + 4;
            if ((long)((unsigned long)_9671 + (unsigned long)HIGH_BITS) >= 0) 
            _9671 = NewDouble((double)_9671);
        }
        else {
            _9671 = NewDouble(DBL_PTR(_tables_17158)->dbl + (double)4);
        }
        if (IS_ATOM_INT(_i_17174)) {
            if (_i_17174 == (short)_i_17174)
            _9672 = _i_17174 * 16;
            else
            _9672 = NewDouble(_i_17174 * (double)16);
        }
        else {
            _9672 = NewDouble(DBL_PTR(_i_17174)->dbl * (double)16);
        }
        if (IS_ATOM_INT(_9671) && IS_ATOM_INT(_9672)) {
            _9673 = _9671 + _9672;
            if ((long)((unsigned long)_9673 + (unsigned long)HIGH_BITS) >= 0) 
            _9673 = NewDouble((double)_9673);
        }
        else {
            if (IS_ATOM_INT(_9671)) {
                _9673 = NewDouble((double)_9671 + DBL_PTR(_9672)->dbl);
            }
            else {
                if (IS_ATOM_INT(_9672)) {
                    _9673 = NewDouble(DBL_PTR(_9671)->dbl + (double)_9672);
                }
                else
                _9673 = NewDouble(DBL_PTR(_9671)->dbl + DBL_PTR(_9672)->dbl);
            }
        }
        DeRef(_9671);
        _9671 = NOVALUE;
        DeRef(_9672);
        _9672 = NOVALUE;
        DeRef(_pos_inlined_seek_at_95_17180);
        _pos_inlined_seek_at_95_17180 = _9673;
        _9673 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_95_17180);
        DeRef(_seek_1__tmp_at98_17182);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _pos_inlined_seek_at_95_17180;
        _seek_1__tmp_at98_17182 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_98_17181 = machine(19, _seek_1__tmp_at98_17182);
        DeRef(_pos_inlined_seek_at_95_17180);
        _pos_inlined_seek_at_95_17180 = NOVALUE;
        DeRef(_seek_1__tmp_at98_17182);
        _seek_1__tmp_at98_17182 = NOVALUE;

        /** 		name = get4()*/
        _0 = _name_17160;
        _name_17160 = _33get4();
        DeRef(_0);

        /** 		io:seek(current_db, name)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_17160);
        DeRef(_seek_1__tmp_at120_17186);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _33current_db_15557;
        ((int *)_2)[2] = _name_17160;
        _seek_1__tmp_at120_17186 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_120_17185 = machine(19, _seek_1__tmp_at120_17186);
        DeRef(_seek_1__tmp_at120_17186);
        _seek_1__tmp_at120_17186 = NOVALUE;

        /** 		table_names[i+1] = get_string()*/
        if (IS_ATOM_INT(_i_17174)) {
            _9675 = _i_17174 + 1;
            if (_9675 > MAXINT){
                _9675 = NewDouble((double)_9675);
            }
        }
        else
        _9675 = binary_op(PLUS, 1, _i_17174);
        _9676 = _33get_string();
        _2 = (int)SEQ_PTR(_table_names_17157);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _table_names_17157 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_9675))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_9675)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _9675);
        _1 = *(int *)_2;
        *(int *)_2 = _9676;
        if( _1 != _9676 ){
            DeRef(_1);
        }
        _9676 = NOVALUE;

        /** 	end for*/
        _0 = _i_17174;
        if (IS_ATOM_INT(_i_17174)) {
            _i_17174 = _i_17174 + 1;
            if ((long)((unsigned long)_i_17174 +(unsigned long) HIGH_BITS) >= 0){
                _i_17174 = NewDouble((double)_i_17174);
            }
        }
        else {
            _i_17174 = binary_op_a(PLUS, _i_17174, 1);
        }
        DeRef(_0);
        goto L2; // [149] 80
L3: 
        ;
        DeRef(_i_17174);
    }

    /** 	return table_names*/
    DeRef(_tables_17158);
    DeRef(_nt_17159);
    DeRef(_name_17160);
    DeRef(_9670);
    _9670 = NOVALUE;
    DeRef(_9675);
    _9675 = NOVALUE;
    return _table_names_17157;
    ;
}


int _33key_value(int _ptr_17191)
{
    int _seek_1__tmp_at11_17196 = NOVALUE;
    int _seek_inlined_seek_at_11_17195 = NOVALUE;
    int _pos_inlined_seek_at_8_17194 = NOVALUE;
    int _9678 = NOVALUE;
    int _9677 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, ptr+4) -- skip ptr to data*/
    if (IS_ATOM_INT(_ptr_17191)) {
        _9677 = _ptr_17191 + 4;
        if ((long)((unsigned long)_9677 + (unsigned long)HIGH_BITS) >= 0) 
        _9677 = NewDouble((double)_9677);
    }
    else {
        _9677 = NewDouble(DBL_PTR(_ptr_17191)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_17194);
    _pos_inlined_seek_at_8_17194 = _9677;
    _9677 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_17194);
    DeRef(_seek_1__tmp_at11_17196);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_17194;
    _seek_1__tmp_at11_17196 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_17195 = machine(19, _seek_1__tmp_at11_17196);
    DeRef(_pos_inlined_seek_at_8_17194);
    _pos_inlined_seek_at_8_17194 = NOVALUE;
    DeRef(_seek_1__tmp_at11_17196);
    _seek_1__tmp_at11_17196 = NOVALUE;

    /** 	return decompress(0)*/
    _9678 = _33decompress(0);
    DeRef(_ptr_17191);
    return _9678;
    ;
}


int _33db_find_key(int _key_17200, int _table_name_17201)
{
    int _lo_17202 = NOVALUE;
    int _hi_17203 = NOVALUE;
    int _mid_17204 = NOVALUE;
    int _c_17205 = NOVALUE;
    int _9702 = NOVALUE;
    int _9694 = NOVALUE;
    int _9693 = NOVALUE;
    int _9691 = NOVALUE;
    int _9688 = NOVALUE;
    int _9685 = NOVALUE;
    int _9681 = NOVALUE;
    int _9679 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_17201 == _33current_table_name_15559)
    _9679 = 1;
    else if (IS_ATOM_INT(_table_name_17201) && IS_ATOM_INT(_33current_table_name_15559))
    _9679 = 0;
    else
    _9679 = (compare(_table_name_17201, _33current_table_name_15559) == 0);
    if (_9679 != 0)
    goto L1; // [9] 46
    _9679 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_17201);
    _9681 = _33db_select_table(_table_name_17201);
    if (binary_op_a(EQUALS, _9681, 0)){
        DeRef(_9681);
        _9681 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_9681);
    _9681 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_find_key", {key, table_name})*/
    Ref(_table_name_17201);
    Ref(_key_17200);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_17200;
    ((int *)_2)[2] = _table_name_17201;
    _9685 = MAKE_SEQ(_1);
    RefDS(_9683);
    RefDS(_9684);
    _33fatal(903, _9683, _9684, _9685);
    _9685 = NOVALUE;

    /** 			return 0*/
    DeRef(_key_17200);
    DeRef(_table_name_17201);
    return 0;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _33current_table_pos_15558, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_find_key", {key, table_name})*/
    Ref(_table_name_17201);
    Ref(_key_17200);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_17200;
    ((int *)_2)[2] = _table_name_17201;
    _9688 = MAKE_SEQ(_1);
    RefDS(_9687);
    RefDS(_9684);
    _33fatal(903, _9687, _9684, _9688);
    _9688 = NOVALUE;

    /** 		return 0*/
    DeRef(_key_17200);
    DeRef(_table_name_17201);
    return 0;
L3: 

    /** 	lo = 1*/
    _lo_17202 = 1;

    /** 	hi = length(key_pointers)*/
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _hi_17203 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _hi_17203 = 1;
    }

    /** 	mid = 1*/
    _mid_17204 = 1;

    /** 	c = 0*/
    _c_17205 = 0;

    /** 	while lo <= hi do*/
L4: 
    if (_lo_17202 > _hi_17203)
    goto L5; // [110] 192

    /** 		mid = floor((lo + hi) / 2)*/
    _9691 = _lo_17202 + _hi_17203;
    if ((long)((unsigned long)_9691 + (unsigned long)HIGH_BITS) >= 0) 
    _9691 = NewDouble((double)_9691);
    if (IS_ATOM_INT(_9691)) {
        _mid_17204 = _9691 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _9691, 2);
        _mid_17204 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_9691);
    _9691 = NOVALUE;
    if (!IS_ATOM_INT(_mid_17204)) {
        _1 = (long)(DBL_PTR(_mid_17204)->dbl);
        if (UNIQUE(DBL_PTR(_mid_17204)) && (DBL_PTR(_mid_17204)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_17204);
        _mid_17204 = _1;
    }

    /** 		c = eu:compare(key, key_value(key_pointers[mid]))*/
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    _9693 = (int)*(((s1_ptr)_2)->base + _mid_17204);
    Ref(_9693);
    _9694 = _33key_value(_9693);
    _9693 = NOVALUE;
    if (IS_ATOM_INT(_key_17200) && IS_ATOM_INT(_9694)){
        _c_17205 = (_key_17200 < _9694) ? -1 : (_key_17200 > _9694);
    }
    else{
        _c_17205 = compare(_key_17200, _9694);
    }
    DeRef(_9694);
    _9694 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_17205 >= 0)
    goto L6; // [148] 163

    /** 			hi = mid - 1*/
    _hi_17203 = _mid_17204 - 1;
    goto L4; // [160] 110
L6: 

    /** 		elsif c > 0 then*/
    if (_c_17205 <= 0)
    goto L7; // [165] 180

    /** 			lo = mid + 1*/
    _lo_17202 = _mid_17204 + 1;
    goto L4; // [177] 110
L7: 

    /** 			return mid*/
    DeRef(_key_17200);
    DeRef(_table_name_17201);
    return _mid_17204;

    /** 	end while*/
    goto L4; // [189] 110
L5: 

    /** 	if c > 0 then*/
    if (_c_17205 <= 0)
    goto L8; // [194] 207

    /** 		mid += 1*/
    _mid_17204 = _mid_17204 + 1;
L8: 

    /** 	return -mid*/
    if ((unsigned long)_mid_17204 == 0xC0000000)
    _9702 = (int)NewDouble((double)-0xC0000000);
    else
    _9702 = - _mid_17204;
    DeRef(_key_17200);
    DeRef(_table_name_17201);
    return _9702;
    ;
}


int _33db_insert(int _key_17240, int _data_17241, int _table_name_17242)
{
    int _key_string_17243 = NOVALUE;
    int _data_string_17244 = NOVALUE;
    int _last_part_17245 = NOVALUE;
    int _remaining_17246 = NOVALUE;
    int _key_ptr_17247 = NOVALUE;
    int _data_ptr_17248 = NOVALUE;
    int _records_ptr_17249 = NOVALUE;
    int _nrecs_17250 = NOVALUE;
    int _current_block_17251 = NOVALUE;
    int _size_17252 = NOVALUE;
    int _new_size_17253 = NOVALUE;
    int _key_location_17254 = NOVALUE;
    int _new_block_17255 = NOVALUE;
    int _index_ptr_17256 = NOVALUE;
    int _new_index_ptr_17257 = NOVALUE;
    int _total_recs_17258 = NOVALUE;
    int _r_17259 = NOVALUE;
    int _blocks_17260 = NOVALUE;
    int _new_recs_17261 = NOVALUE;
    int _n_17262 = NOVALUE;
    int _put4_1__tmp_at81_17276 = NOVALUE;
    int _seek_1__tmp_at134_17282 = NOVALUE;
    int _seek_inlined_seek_at_134_17281 = NOVALUE;
    int _pos_inlined_seek_at_131_17280 = NOVALUE;
    int _seek_1__tmp_at178_17290 = NOVALUE;
    int _seek_inlined_seek_at_178_17289 = NOVALUE;
    int _pos_inlined_seek_at_175_17288 = NOVALUE;
    int _put4_1__tmp_at193_17292 = NOVALUE;
    int _seek_1__tmp_at323_17309 = NOVALUE;
    int _seek_inlined_seek_at_323_17308 = NOVALUE;
    int _pos_inlined_seek_at_320_17307 = NOVALUE;
    int _seek_1__tmp_at345_17313 = NOVALUE;
    int _seek_inlined_seek_at_345_17312 = NOVALUE;
    int _where_inlined_where_at_414_17322 = NOVALUE;
    int _seek_1__tmp_at458_17332 = NOVALUE;
    int _seek_inlined_seek_at_458_17331 = NOVALUE;
    int _pos_inlined_seek_at_455_17330 = NOVALUE;
    int _put4_1__tmp_at503_17341 = NOVALUE;
    int _x_inlined_put4_at_500_17340 = NOVALUE;
    int _seek_1__tmp_at540_17344 = NOVALUE;
    int _seek_inlined_seek_at_540_17343 = NOVALUE;
    int _put4_1__tmp_at561_17347 = NOVALUE;
    int _seek_1__tmp_at598_17352 = NOVALUE;
    int _seek_inlined_seek_at_598_17351 = NOVALUE;
    int _pos_inlined_seek_at_595_17350 = NOVALUE;
    int _seek_1__tmp_at704_17376 = NOVALUE;
    int _seek_inlined_seek_at_704_17375 = NOVALUE;
    int _pos_inlined_seek_at_701_17374 = NOVALUE;
    int _s_inlined_putn_at_765_17385 = NOVALUE;
    int _seek_1__tmp_at788_17388 = NOVALUE;
    int _seek_inlined_seek_at_788_17387 = NOVALUE;
    int _put4_1__tmp_at810_17392 = NOVALUE;
    int _x_inlined_put4_at_807_17391 = NOVALUE;
    int _seek_1__tmp_at847_17397 = NOVALUE;
    int _seek_inlined_seek_at_847_17396 = NOVALUE;
    int _pos_inlined_seek_at_844_17395 = NOVALUE;
    int _seek_1__tmp_at898_17407 = NOVALUE;
    int _seek_inlined_seek_at_898_17406 = NOVALUE;
    int _pos_inlined_seek_at_895_17405 = NOVALUE;
    int _put4_1__tmp_at913_17409 = NOVALUE;
    int _put4_1__tmp_at941_17411 = NOVALUE;
    int _seek_1__tmp_at994_17417 = NOVALUE;
    int _seek_inlined_seek_at_994_17416 = NOVALUE;
    int _pos_inlined_seek_at_991_17415 = NOVALUE;
    int _put4_1__tmp_at1017_17420 = NOVALUE;
    int _seek_1__tmp_at1054_17425 = NOVALUE;
    int _seek_inlined_seek_at_1054_17424 = NOVALUE;
    int _pos_inlined_seek_at_1051_17423 = NOVALUE;
    int _s_inlined_putn_at_1152_17443 = NOVALUE;
    int _seek_1__tmp_at1189_17448 = NOVALUE;
    int _seek_inlined_seek_at_1189_17447 = NOVALUE;
    int _pos_inlined_seek_at_1186_17446 = NOVALUE;
    int _put4_1__tmp_at1204_17450 = NOVALUE;
    int _9796 = NOVALUE;
    int _9795 = NOVALUE;
    int _9794 = NOVALUE;
    int _9793 = NOVALUE;
    int _9790 = NOVALUE;
    int _9789 = NOVALUE;
    int _9787 = NOVALUE;
    int _9785 = NOVALUE;
    int _9784 = NOVALUE;
    int _9782 = NOVALUE;
    int _9781 = NOVALUE;
    int _9779 = NOVALUE;
    int _9778 = NOVALUE;
    int _9776 = NOVALUE;
    int _9775 = NOVALUE;
    int _9774 = NOVALUE;
    int _9773 = NOVALUE;
    int _9772 = NOVALUE;
    int _9771 = NOVALUE;
    int _9770 = NOVALUE;
    int _9769 = NOVALUE;
    int _9768 = NOVALUE;
    int _9765 = NOVALUE;
    int _9764 = NOVALUE;
    int _9763 = NOVALUE;
    int _9762 = NOVALUE;
    int _9759 = NOVALUE;
    int _9756 = NOVALUE;
    int _9755 = NOVALUE;
    int _9754 = NOVALUE;
    int _9753 = NOVALUE;
    int _9750 = NOVALUE;
    int _9749 = NOVALUE;
    int _9747 = NOVALUE;
    int _9746 = NOVALUE;
    int _9744 = NOVALUE;
    int _9743 = NOVALUE;
    int _9742 = NOVALUE;
    int _9741 = NOVALUE;
    int _9740 = NOVALUE;
    int _9739 = NOVALUE;
    int _9738 = NOVALUE;
    int _9736 = NOVALUE;
    int _9733 = NOVALUE;
    int _9728 = NOVALUE;
    int _9727 = NOVALUE;
    int _9726 = NOVALUE;
    int _9724 = NOVALUE;
    int _9723 = NOVALUE;
    int _9722 = NOVALUE;
    int _9719 = NOVALUE;
    int _9717 = NOVALUE;
    int _9714 = NOVALUE;
    int _9713 = NOVALUE;
    int _9711 = NOVALUE;
    int _9710 = NOVALUE;
    int _9708 = NOVALUE;
    int _0, _1, _2;
    

    /** 	key_location = db_find_key(key, table_name) -- Let it set the current table if necessary*/
    Ref(_key_17240);
    Ref(_table_name_17242);
    _0 = _key_location_17254;
    _key_location_17254 = _33db_find_key(_key_17240, _table_name_17242);
    DeRef(_0);

    /** 	if key_location > 0 then*/
    if (binary_op_a(LESSEQ, _key_location_17254, 0)){
        goto L1; // [10] 23
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRef(_key_17240);
    DeRef(_data_17241);
    DeRef(_table_name_17242);
    DeRef(_key_string_17243);
    DeRef(_data_string_17244);
    DeRef(_last_part_17245);
    DeRef(_remaining_17246);
    DeRef(_key_ptr_17247);
    DeRef(_data_ptr_17248);
    DeRef(_records_ptr_17249);
    DeRef(_nrecs_17250);
    DeRef(_current_block_17251);
    DeRef(_size_17252);
    DeRef(_new_size_17253);
    DeRef(_key_location_17254);
    DeRef(_new_block_17255);
    DeRef(_index_ptr_17256);
    DeRef(_new_index_ptr_17257);
    DeRef(_total_recs_17258);
    return -2;
L1: 

    /** 	key_location = -key_location*/
    _0 = _key_location_17254;
    if (IS_ATOM_INT(_key_location_17254)) {
        if ((unsigned long)_key_location_17254 == 0xC0000000)
        _key_location_17254 = (int)NewDouble((double)-0xC0000000);
        else
        _key_location_17254 = - _key_location_17254;
    }
    else {
        _key_location_17254 = unary_op(UMINUS, _key_location_17254);
    }
    DeRef(_0);

    /** 	data_string = compress(data)*/
    Ref(_data_17241);
    _0 = _data_string_17244;
    _data_string_17244 = _33compress(_data_17241);
    DeRef(_0);

    /** 	key_string  = compress(key)*/
    Ref(_key_17240);
    _0 = _key_string_17243;
    _key_string_17243 = _33compress(_key_17240);
    DeRef(_0);

    /** 	data_ptr = db_allocate(length(data_string))*/
    if (IS_SEQUENCE(_data_string_17244)){
            _9708 = SEQ_PTR(_data_string_17244)->length;
    }
    else {
        _9708 = 1;
    }
    _0 = _data_ptr_17248;
    _data_ptr_17248 = _33db_allocate(_9708);
    DeRef(_0);
    _9708 = NOVALUE;

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _data_string_17244); // DJP 

    /** end procedure*/
    goto L2; // [64] 67
L2: 

    /** 	key_ptr = db_allocate(4+length(key_string))*/
    if (IS_SEQUENCE(_key_string_17243)){
            _9710 = SEQ_PTR(_key_string_17243)->length;
    }
    else {
        _9710 = 1;
    }
    _9711 = 4 + _9710;
    _9710 = NOVALUE;
    _0 = _key_ptr_17247;
    _key_ptr_17247 = _33db_allocate(_9711);
    DeRef(_0);
    _9711 = NOVALUE;

    /** 	put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_17248)) {
        *poke4_addr = (unsigned long)_data_ptr_17248;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_17248)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at81_17276);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at81_17276 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at81_17276); // DJP 

    /** end procedure*/
    goto L3; // [103] 106
L3: 
    DeRefi(_put4_1__tmp_at81_17276);
    _put4_1__tmp_at81_17276 = NOVALUE;

    /** 	putn(key_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _key_string_17243); // DJP 

    /** end procedure*/
    goto L4; // [119] 122
L4: 

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9713 = _33current_table_pos_15558 + 4;
        if ((long)((unsigned long)_9713 + (unsigned long)HIGH_BITS) >= 0) 
        _9713 = NewDouble((double)_9713);
    }
    else {
        _9713 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_131_17280);
    _pos_inlined_seek_at_131_17280 = _9713;
    _9713 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_131_17280);
    DeRef(_seek_1__tmp_at134_17282);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_131_17280;
    _seek_1__tmp_at134_17282 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_134_17281 = machine(19, _seek_1__tmp_at134_17282);
    DeRef(_pos_inlined_seek_at_131_17280);
    _pos_inlined_seek_at_131_17280 = NOVALUE;
    DeRef(_seek_1__tmp_at134_17282);
    _seek_1__tmp_at134_17282 = NOVALUE;

    /** 	total_recs = get4()+1*/
    _9714 = _33get4();
    DeRef(_total_recs_17258);
    if (IS_ATOM_INT(_9714)) {
        _total_recs_17258 = _9714 + 1;
        if (_total_recs_17258 > MAXINT){
            _total_recs_17258 = NewDouble((double)_total_recs_17258);
        }
    }
    else
    _total_recs_17258 = binary_op(PLUS, 1, _9714);
    DeRef(_9714);
    _9714 = NOVALUE;

    /** 	blocks = get4()*/
    _blocks_17260 = _33get4();
    if (!IS_ATOM_INT(_blocks_17260)) {
        _1 = (long)(DBL_PTR(_blocks_17260)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_17260)) && (DBL_PTR(_blocks_17260)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_17260);
        _blocks_17260 = _1;
    }

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9717 = _33current_table_pos_15558 + 4;
        if ((long)((unsigned long)_9717 + (unsigned long)HIGH_BITS) >= 0) 
        _9717 = NewDouble((double)_9717);
    }
    else {
        _9717 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_175_17288);
    _pos_inlined_seek_at_175_17288 = _9717;
    _9717 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_175_17288);
    DeRef(_seek_1__tmp_at178_17290);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_175_17288;
    _seek_1__tmp_at178_17290 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_178_17289 = machine(19, _seek_1__tmp_at178_17290);
    DeRef(_pos_inlined_seek_at_175_17288);
    _pos_inlined_seek_at_175_17288 = NOVALUE;
    DeRef(_seek_1__tmp_at178_17290);
    _seek_1__tmp_at178_17290 = NOVALUE;

    /** 	put4(total_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_total_recs_17258)) {
        *poke4_addr = (unsigned long)_total_recs_17258;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_total_recs_17258)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at193_17292);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at193_17292 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at193_17292); // DJP 

    /** end procedure*/
    goto L5; // [215] 218
L5: 
    DeRefi(_put4_1__tmp_at193_17292);
    _put4_1__tmp_at193_17292 = NOVALUE;

    /** 	n = length(key_pointers)*/
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _n_17262 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _n_17262 = 1;
    }

    /** 	if key_location >= floor(n/2) then*/
    _9719 = _n_17262 >> 1;
    if (binary_op_a(LESS, _key_location_17254, _9719)){
        _9719 = NOVALUE;
        goto L6; // [235] 274
    }
    DeRef(_9719);
    _9719 = NOVALUE;

    /** 		key_pointers = append(key_pointers, 0)*/
    Append(&_33key_pointers_15564, _33key_pointers_15564, 0);

    /** 		key_pointers[key_location+1..n+1] = key_pointers[key_location..n]*/
    if (IS_ATOM_INT(_key_location_17254)) {
        _9722 = _key_location_17254 + 1;
        if (_9722 > MAXINT){
            _9722 = NewDouble((double)_9722);
        }
    }
    else
    _9722 = binary_op(PLUS, 1, _key_location_17254);
    _9723 = _n_17262 + 1;
    rhs_slice_target = (object_ptr)&_9724;
    RHS_Slice(_33key_pointers_15564, _key_location_17254, _n_17262);
    assign_slice_seq = (s1_ptr *)&_33key_pointers_15564;
    AssignSlice(_9722, _9723, _9724);
    DeRef(_9722);
    _9722 = NOVALUE;
    _9723 = NOVALUE;
    DeRefDS(_9724);
    _9724 = NOVALUE;
    goto L7; // [271] 303
L6: 

    /** 		key_pointers = prepend(key_pointers, 0)*/
    Prepend(&_33key_pointers_15564, _33key_pointers_15564, 0);

    /** 		key_pointers[1..key_location-1] = key_pointers[2..key_location]*/
    if (IS_ATOM_INT(_key_location_17254)) {
        _9726 = _key_location_17254 - 1;
        if ((long)((unsigned long)_9726 +(unsigned long) HIGH_BITS) >= 0){
            _9726 = NewDouble((double)_9726);
        }
    }
    else {
        _9726 = NewDouble(DBL_PTR(_key_location_17254)->dbl - (double)1);
    }
    rhs_slice_target = (object_ptr)&_9727;
    RHS_Slice(_33key_pointers_15564, 2, _key_location_17254);
    assign_slice_seq = (s1_ptr *)&_33key_pointers_15564;
    AssignSlice(1, _9726, _9727);
    DeRef(_9726);
    _9726 = NOVALUE;
    DeRefDS(_9727);
    _9727 = NOVALUE;
L7: 

    /** 	key_pointers[key_location] = key_ptr*/
    Ref(_key_ptr_17247);
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _33key_pointers_15564 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_key_location_17254))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_key_location_17254)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _key_location_17254);
    _1 = *(int *)_2;
    *(int *)_2 = _key_ptr_17247;
    DeRef(_1);

    /** 	io:seek(current_db, current_table_pos+12) -- get after put - seek is necessary*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9728 = _33current_table_pos_15558 + 12;
        if ((long)((unsigned long)_9728 + (unsigned long)HIGH_BITS) >= 0) 
        _9728 = NewDouble((double)_9728);
    }
    else {
        _9728 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_320_17307);
    _pos_inlined_seek_at_320_17307 = _9728;
    _9728 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_320_17307);
    DeRef(_seek_1__tmp_at323_17309);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_320_17307;
    _seek_1__tmp_at323_17309 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_323_17308 = machine(19, _seek_1__tmp_at323_17309);
    DeRef(_pos_inlined_seek_at_320_17307);
    _pos_inlined_seek_at_320_17307 = NOVALUE;
    DeRef(_seek_1__tmp_at323_17309);
    _seek_1__tmp_at323_17309 = NOVALUE;

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_17256;
    _index_ptr_17256 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, index_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_index_ptr_17256);
    DeRef(_seek_1__tmp_at345_17313);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _index_ptr_17256;
    _seek_1__tmp_at345_17313 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_345_17312 = machine(19, _seek_1__tmp_at345_17313);
    DeRef(_seek_1__tmp_at345_17313);
    _seek_1__tmp_at345_17313 = NOVALUE;

    /** 	r = 0*/
    _r_17259 = 0;

    /** 	while TRUE do*/
L8: 

    /** 		nrecs = get4()*/
    _0 = _nrecs_17250;
    _nrecs_17250 = _33get4();
    DeRef(_0);

    /** 		records_ptr = get4()*/
    _0 = _records_ptr_17249;
    _records_ptr_17249 = _33get4();
    DeRef(_0);

    /** 		r += nrecs*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        _r_17259 = _r_17259 + _nrecs_17250;
    }
    else {
        _r_17259 = NewDouble((double)_r_17259 + DBL_PTR(_nrecs_17250)->dbl);
    }
    if (!IS_ATOM_INT(_r_17259)) {
        _1 = (long)(DBL_PTR(_r_17259)->dbl);
        if (UNIQUE(DBL_PTR(_r_17259)) && (DBL_PTR(_r_17259)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_17259);
        _r_17259 = _1;
    }

    /** 		if r + 1 >= key_location then*/
    _9733 = _r_17259 + 1;
    if (_9733 > MAXINT){
        _9733 = NewDouble((double)_9733);
    }
    if (binary_op_a(LESS, _9733, _key_location_17254)){
        DeRef(_9733);
        _9733 = NOVALUE;
        goto L8; // [397] 371
    }
    DeRef(_9733);
    _9733 = NOVALUE;

    /** 			exit*/
    goto L9; // [403] 411

    /** 	end while*/
    goto L8; // [408] 371
L9: 

    /** 	current_block = io:where(current_db)-8*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_414_17322);
    _where_inlined_where_at_414_17322 = machine(20, _33current_db_15557);
    DeRef(_current_block_17251);
    if (IS_ATOM_INT(_where_inlined_where_at_414_17322)) {
        _current_block_17251 = _where_inlined_where_at_414_17322 - 8;
        if ((long)((unsigned long)_current_block_17251 +(unsigned long) HIGH_BITS) >= 0){
            _current_block_17251 = NewDouble((double)_current_block_17251);
        }
    }
    else {
        _current_block_17251 = NewDouble(DBL_PTR(_where_inlined_where_at_414_17322)->dbl - (double)8);
    }

    /** 	key_location -= (r-nrecs)*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        _9736 = _r_17259 - _nrecs_17250;
        if ((long)((unsigned long)_9736 +(unsigned long) HIGH_BITS) >= 0){
            _9736 = NewDouble((double)_9736);
        }
    }
    else {
        _9736 = NewDouble((double)_r_17259 - DBL_PTR(_nrecs_17250)->dbl);
    }
    _0 = _key_location_17254;
    if (IS_ATOM_INT(_key_location_17254) && IS_ATOM_INT(_9736)) {
        _key_location_17254 = _key_location_17254 - _9736;
        if ((long)((unsigned long)_key_location_17254 +(unsigned long) HIGH_BITS) >= 0){
            _key_location_17254 = NewDouble((double)_key_location_17254);
        }
    }
    else {
        if (IS_ATOM_INT(_key_location_17254)) {
            _key_location_17254 = NewDouble((double)_key_location_17254 - DBL_PTR(_9736)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9736)) {
                _key_location_17254 = NewDouble(DBL_PTR(_key_location_17254)->dbl - (double)_9736);
            }
            else
            _key_location_17254 = NewDouble(DBL_PTR(_key_location_17254)->dbl - DBL_PTR(_9736)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_9736);
    _9736 = NOVALUE;

    /** 	io:seek(current_db, records_ptr+4*(key_location-1))*/
    if (IS_ATOM_INT(_key_location_17254)) {
        _9738 = _key_location_17254 - 1;
        if ((long)((unsigned long)_9738 +(unsigned long) HIGH_BITS) >= 0){
            _9738 = NewDouble((double)_9738);
        }
    }
    else {
        _9738 = NewDouble(DBL_PTR(_key_location_17254)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_9738)) {
        if (_9738 <= INT15 && _9738 >= -INT15)
        _9739 = 4 * _9738;
        else
        _9739 = NewDouble(4 * (double)_9738);
    }
    else {
        _9739 = NewDouble((double)4 * DBL_PTR(_9738)->dbl);
    }
    DeRef(_9738);
    _9738 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_17249) && IS_ATOM_INT(_9739)) {
        _9740 = _records_ptr_17249 + _9739;
        if ((long)((unsigned long)_9740 + (unsigned long)HIGH_BITS) >= 0) 
        _9740 = NewDouble((double)_9740);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_17249)) {
            _9740 = NewDouble((double)_records_ptr_17249 + DBL_PTR(_9739)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9739)) {
                _9740 = NewDouble(DBL_PTR(_records_ptr_17249)->dbl + (double)_9739);
            }
            else
            _9740 = NewDouble(DBL_PTR(_records_ptr_17249)->dbl + DBL_PTR(_9739)->dbl);
        }
    }
    DeRef(_9739);
    _9739 = NOVALUE;
    DeRef(_pos_inlined_seek_at_455_17330);
    _pos_inlined_seek_at_455_17330 = _9740;
    _9740 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_455_17330);
    DeRef(_seek_1__tmp_at458_17332);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_455_17330;
    _seek_1__tmp_at458_17332 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_458_17331 = machine(19, _seek_1__tmp_at458_17332);
    DeRef(_pos_inlined_seek_at_455_17330);
    _pos_inlined_seek_at_455_17330 = NOVALUE;
    DeRef(_seek_1__tmp_at458_17332);
    _seek_1__tmp_at458_17332 = NOVALUE;

    /** 	for i = key_location to nrecs+1 do*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        _9741 = _nrecs_17250 + 1;
        if (_9741 > MAXINT){
            _9741 = NewDouble((double)_9741);
        }
    }
    else
    _9741 = binary_op(PLUS, 1, _nrecs_17250);
    {
        int _i_17334;
        Ref(_key_location_17254);
        _i_17334 = _key_location_17254;
LA: 
        if (binary_op_a(GREATER, _i_17334, _9741)){
            goto LB; // [478] 537
        }

        /** 		put4(key_pointers[i+r-nrecs])*/
        if (IS_ATOM_INT(_i_17334)) {
            _9742 = _i_17334 + _r_17259;
            if ((long)((unsigned long)_9742 + (unsigned long)HIGH_BITS) >= 0) 
            _9742 = NewDouble((double)_9742);
        }
        else {
            _9742 = NewDouble(DBL_PTR(_i_17334)->dbl + (double)_r_17259);
        }
        if (IS_ATOM_INT(_9742) && IS_ATOM_INT(_nrecs_17250)) {
            _9743 = _9742 - _nrecs_17250;
        }
        else {
            if (IS_ATOM_INT(_9742)) {
                _9743 = NewDouble((double)_9742 - DBL_PTR(_nrecs_17250)->dbl);
            }
            else {
                if (IS_ATOM_INT(_nrecs_17250)) {
                    _9743 = NewDouble(DBL_PTR(_9742)->dbl - (double)_nrecs_17250);
                }
                else
                _9743 = NewDouble(DBL_PTR(_9742)->dbl - DBL_PTR(_nrecs_17250)->dbl);
            }
        }
        DeRef(_9742);
        _9742 = NOVALUE;
        _2 = (int)SEQ_PTR(_33key_pointers_15564);
        if (!IS_ATOM_INT(_9743)){
            _9744 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_9743)->dbl));
        }
        else{
            _9744 = (int)*(((s1_ptr)_2)->base + _9743);
        }
        Ref(_9744);
        DeRef(_x_inlined_put4_at_500_17340);
        _x_inlined_put4_at_500_17340 = _9744;
        _9744 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_33mem0_15599)){
            poke4_addr = (unsigned long *)_33mem0_15599;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_500_17340)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_500_17340;
        }
        else if (IS_ATOM(_x_inlined_put4_at_500_17340)) {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_500_17340)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_x_inlined_put4_at_500_17340);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at503_17341);
        _1 = (int)SEQ_PTR(_33memseq_15819);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at503_17341 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_33current_db_15557, _put4_1__tmp_at503_17341); // DJP 

        /** end procedure*/
        goto LC; // [525] 528
LC: 
        DeRef(_x_inlined_put4_at_500_17340);
        _x_inlined_put4_at_500_17340 = NOVALUE;
        DeRefi(_put4_1__tmp_at503_17341);
        _put4_1__tmp_at503_17341 = NOVALUE;

        /** 	end for*/
        _0 = _i_17334;
        if (IS_ATOM_INT(_i_17334)) {
            _i_17334 = _i_17334 + 1;
            if ((long)((unsigned long)_i_17334 +(unsigned long) HIGH_BITS) >= 0){
                _i_17334 = NewDouble((double)_i_17334);
            }
        }
        else {
            _i_17334 = binary_op_a(PLUS, _i_17334, 1);
        }
        DeRef(_0);
        goto LA; // [532] 485
LB: 
        ;
        DeRef(_i_17334);
    }

    /** 	io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_17251);
    DeRef(_seek_1__tmp_at540_17344);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _current_block_17251;
    _seek_1__tmp_at540_17344 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_540_17343 = machine(19, _seek_1__tmp_at540_17344);
    DeRef(_seek_1__tmp_at540_17344);
    _seek_1__tmp_at540_17344 = NOVALUE;

    /** 	nrecs += 1*/
    _0 = _nrecs_17250;
    if (IS_ATOM_INT(_nrecs_17250)) {
        _nrecs_17250 = _nrecs_17250 + 1;
        if (_nrecs_17250 > MAXINT){
            _nrecs_17250 = NewDouble((double)_nrecs_17250);
        }
    }
    else
    _nrecs_17250 = binary_op(PLUS, 1, _nrecs_17250);
    DeRef(_0);

    /** 	put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_17250)) {
        *poke4_addr = (unsigned long)_nrecs_17250;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_17250)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at561_17347);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at561_17347 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at561_17347); // DJP 

    /** end procedure*/
    goto LD; // [583] 586
LD: 
    DeRefi(_put4_1__tmp_at561_17347);
    _put4_1__tmp_at561_17347 = NOVALUE;

    /** 	io:seek(current_db, records_ptr - 4)*/
    if (IS_ATOM_INT(_records_ptr_17249)) {
        _9746 = _records_ptr_17249 - 4;
        if ((long)((unsigned long)_9746 +(unsigned long) HIGH_BITS) >= 0){
            _9746 = NewDouble((double)_9746);
        }
    }
    else {
        _9746 = NewDouble(DBL_PTR(_records_ptr_17249)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_595_17350);
    _pos_inlined_seek_at_595_17350 = _9746;
    _9746 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_595_17350);
    DeRef(_seek_1__tmp_at598_17352);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_595_17350;
    _seek_1__tmp_at598_17352 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_598_17351 = machine(19, _seek_1__tmp_at598_17352);
    DeRef(_pos_inlined_seek_at_595_17350);
    _pos_inlined_seek_at_595_17350 = NOVALUE;
    DeRef(_seek_1__tmp_at598_17352);
    _seek_1__tmp_at598_17352 = NOVALUE;

    /** 	size = get4() - 4*/
    _9747 = _33get4();
    DeRef(_size_17252);
    if (IS_ATOM_INT(_9747)) {
        _size_17252 = _9747 - 4;
        if ((long)((unsigned long)_size_17252 +(unsigned long) HIGH_BITS) >= 0){
            _size_17252 = NewDouble((double)_size_17252);
        }
    }
    else {
        _size_17252 = binary_op(MINUS, _9747, 4);
    }
    DeRef(_9747);
    _9747 = NOVALUE;

    /** 	if nrecs*4 > size-4 then*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        if (_nrecs_17250 == (short)_nrecs_17250)
        _9749 = _nrecs_17250 * 4;
        else
        _9749 = NewDouble(_nrecs_17250 * (double)4);
    }
    else {
        _9749 = NewDouble(DBL_PTR(_nrecs_17250)->dbl * (double)4);
    }
    if (IS_ATOM_INT(_size_17252)) {
        _9750 = _size_17252 - 4;
        if ((long)((unsigned long)_9750 +(unsigned long) HIGH_BITS) >= 0){
            _9750 = NewDouble((double)_9750);
        }
    }
    else {
        _9750 = NewDouble(DBL_PTR(_size_17252)->dbl - (double)4);
    }
    if (binary_op_a(LESSEQ, _9749, _9750)){
        DeRef(_9749);
        _9749 = NOVALUE;
        DeRef(_9750);
        _9750 = NOVALUE;
        goto LE; // [631] 1233
    }
    DeRef(_9749);
    _9749 = NOVALUE;
    DeRef(_9750);
    _9750 = NOVALUE;

    /** 		new_size = 8 * (20 + floor(sqrt(1.5 * total_recs)))*/
    if (IS_ATOM_INT(_total_recs_17258)) {
        _9753 = NewDouble(DBL_PTR(_9752)->dbl * (double)_total_recs_17258);
    }
    else
    _9753 = NewDouble(DBL_PTR(_9752)->dbl * DBL_PTR(_total_recs_17258)->dbl);
    _9754 = unary_op(SQRT, _9753);
    DeRefDS(_9753);
    _9753 = NOVALUE;
    _9755 = unary_op(FLOOR, _9754);
    DeRefDS(_9754);
    _9754 = NOVALUE;
    if (IS_ATOM_INT(_9755)) {
        _9756 = 20 + _9755;
        if ((long)((unsigned long)_9756 + (unsigned long)HIGH_BITS) >= 0) 
        _9756 = NewDouble((double)_9756);
    }
    else {
        _9756 = binary_op(PLUS, 20, _9755);
    }
    DeRef(_9755);
    _9755 = NOVALUE;
    DeRef(_new_size_17253);
    if (IS_ATOM_INT(_9756)) {
        if (_9756 <= INT15 && _9756 >= -INT15)
        _new_size_17253 = 8 * _9756;
        else
        _new_size_17253 = NewDouble(8 * (double)_9756);
    }
    else {
        _new_size_17253 = binary_op(MULTIPLY, 8, _9756);
    }
    DeRef(_9756);
    _9756 = NOVALUE;

    /** 		new_recs = floor(new_size/8)*/
    if (IS_ATOM_INT(_new_size_17253)) {
        if (8 > 0 && _new_size_17253 >= 0) {
            _new_recs_17261 = _new_size_17253 / 8;
        }
        else {
            temp_dbl = floor((double)_new_size_17253 / (double)8);
            _new_recs_17261 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _new_size_17253, 8);
        _new_recs_17261 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (!IS_ATOM_INT(_new_recs_17261)) {
        _1 = (long)(DBL_PTR(_new_recs_17261)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_17261)) && (DBL_PTR(_new_recs_17261)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_17261);
        _new_recs_17261 = _1;
    }

    /** 		if new_recs > floor(nrecs/2) then*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        _9759 = _nrecs_17250 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_17250, 2);
        _9759 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (binary_op_a(LESSEQ, _new_recs_17261, _9759)){
        DeRef(_9759);
        _9759 = NOVALUE;
        goto LF; // [671] 686
    }
    DeRef(_9759);
    _9759 = NOVALUE;

    /** 			new_recs = floor(nrecs/2)*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        _new_recs_17261 = _nrecs_17250 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_17250, 2);
        _new_recs_17261 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (!IS_ATOM_INT(_new_recs_17261)) {
        _1 = (long)(DBL_PTR(_new_recs_17261)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_17261)) && (DBL_PTR(_new_recs_17261)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_17261);
        _new_recs_17261 = _1;
    }
LF: 

    /** 		io:seek(current_db, records_ptr + (nrecs-new_recs)*4)*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        _9762 = _nrecs_17250 - _new_recs_17261;
        if ((long)((unsigned long)_9762 +(unsigned long) HIGH_BITS) >= 0){
            _9762 = NewDouble((double)_9762);
        }
    }
    else {
        _9762 = NewDouble(DBL_PTR(_nrecs_17250)->dbl - (double)_new_recs_17261);
    }
    if (IS_ATOM_INT(_9762)) {
        if (_9762 == (short)_9762)
        _9763 = _9762 * 4;
        else
        _9763 = NewDouble(_9762 * (double)4);
    }
    else {
        _9763 = NewDouble(DBL_PTR(_9762)->dbl * (double)4);
    }
    DeRef(_9762);
    _9762 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_17249) && IS_ATOM_INT(_9763)) {
        _9764 = _records_ptr_17249 + _9763;
        if ((long)((unsigned long)_9764 + (unsigned long)HIGH_BITS) >= 0) 
        _9764 = NewDouble((double)_9764);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_17249)) {
            _9764 = NewDouble((double)_records_ptr_17249 + DBL_PTR(_9763)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9763)) {
                _9764 = NewDouble(DBL_PTR(_records_ptr_17249)->dbl + (double)_9763);
            }
            else
            _9764 = NewDouble(DBL_PTR(_records_ptr_17249)->dbl + DBL_PTR(_9763)->dbl);
        }
    }
    DeRef(_9763);
    _9763 = NOVALUE;
    DeRef(_pos_inlined_seek_at_701_17374);
    _pos_inlined_seek_at_701_17374 = _9764;
    _9764 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_701_17374);
    DeRef(_seek_1__tmp_at704_17376);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_701_17374;
    _seek_1__tmp_at704_17376 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_704_17375 = machine(19, _seek_1__tmp_at704_17376);
    DeRef(_pos_inlined_seek_at_701_17374);
    _pos_inlined_seek_at_701_17374 = NOVALUE;
    DeRef(_seek_1__tmp_at704_17376);
    _seek_1__tmp_at704_17376 = NOVALUE;

    /** 		last_part = io:get_bytes(current_db, new_recs*4)*/
    if (_new_recs_17261 == (short)_new_recs_17261)
    _9765 = _new_recs_17261 * 4;
    else
    _9765 = NewDouble(_new_recs_17261 * (double)4);
    _0 = _last_part_17245;
    _last_part_17245 = _15get_bytes(_33current_db_15557, _9765);
    DeRef(_0);
    _9765 = NOVALUE;

    /** 		new_block = db_allocate(new_size)*/
    Ref(_new_size_17253);
    _0 = _new_block_17255;
    _new_block_17255 = _33db_allocate(_new_size_17253);
    DeRef(_0);

    /** 		putn(last_part)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _last_part_17245); // DJP 

    /** end procedure*/
    goto L10; // [750] 753
L10: 

    /** 		putn(repeat(0, new_size-length(last_part)))*/
    if (IS_SEQUENCE(_last_part_17245)){
            _9768 = SEQ_PTR(_last_part_17245)->length;
    }
    else {
        _9768 = 1;
    }
    if (IS_ATOM_INT(_new_size_17253)) {
        _9769 = _new_size_17253 - _9768;
    }
    else {
        _9769 = NewDouble(DBL_PTR(_new_size_17253)->dbl - (double)_9768);
    }
    _9768 = NOVALUE;
    _9770 = Repeat(0, _9769);
    DeRef(_9769);
    _9769 = NOVALUE;
    DeRefi(_s_inlined_putn_at_765_17385);
    _s_inlined_putn_at_765_17385 = _9770;
    _9770 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_765_17385); // DJP 

    /** end procedure*/
    goto L11; // [780] 783
L11: 
    DeRefi(_s_inlined_putn_at_765_17385);
    _s_inlined_putn_at_765_17385 = NOVALUE;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_17251);
    DeRef(_seek_1__tmp_at788_17388);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _current_block_17251;
    _seek_1__tmp_at788_17388 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_788_17387 = machine(19, _seek_1__tmp_at788_17388);
    DeRef(_seek_1__tmp_at788_17388);
    _seek_1__tmp_at788_17388 = NOVALUE;

    /** 		put4(nrecs-new_recs)*/
    if (IS_ATOM_INT(_nrecs_17250)) {
        _9771 = _nrecs_17250 - _new_recs_17261;
        if ((long)((unsigned long)_9771 +(unsigned long) HIGH_BITS) >= 0){
            _9771 = NewDouble((double)_9771);
        }
    }
    else {
        _9771 = NewDouble(DBL_PTR(_nrecs_17250)->dbl - (double)_new_recs_17261);
    }
    DeRef(_x_inlined_put4_at_807_17391);
    _x_inlined_put4_at_807_17391 = _9771;
    _9771 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_807_17391)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_807_17391;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_807_17391)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at810_17392);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at810_17392 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at810_17392); // DJP 

    /** end procedure*/
    goto L12; // [832] 835
L12: 
    DeRef(_x_inlined_put4_at_807_17391);
    _x_inlined_put4_at_807_17391 = NOVALUE;
    DeRefi(_put4_1__tmp_at810_17392);
    _put4_1__tmp_at810_17392 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_17251)) {
        _9772 = _current_block_17251 + 8;
        if ((long)((unsigned long)_9772 + (unsigned long)HIGH_BITS) >= 0) 
        _9772 = NewDouble((double)_9772);
    }
    else {
        _9772 = NewDouble(DBL_PTR(_current_block_17251)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_844_17395);
    _pos_inlined_seek_at_844_17395 = _9772;
    _9772 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_844_17395);
    DeRef(_seek_1__tmp_at847_17397);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_844_17395;
    _seek_1__tmp_at847_17397 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_847_17396 = machine(19, _seek_1__tmp_at847_17397);
    DeRef(_pos_inlined_seek_at_844_17395);
    _pos_inlined_seek_at_844_17395 = NOVALUE;
    DeRef(_seek_1__tmp_at847_17397);
    _seek_1__tmp_at847_17397 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, index_ptr+blocks*8-(current_block+8))*/
    if (_blocks_17260 == (short)_blocks_17260)
    _9773 = _blocks_17260 * 8;
    else
    _9773 = NewDouble(_blocks_17260 * (double)8);
    if (IS_ATOM_INT(_index_ptr_17256) && IS_ATOM_INT(_9773)) {
        _9774 = _index_ptr_17256 + _9773;
        if ((long)((unsigned long)_9774 + (unsigned long)HIGH_BITS) >= 0) 
        _9774 = NewDouble((double)_9774);
    }
    else {
        if (IS_ATOM_INT(_index_ptr_17256)) {
            _9774 = NewDouble((double)_index_ptr_17256 + DBL_PTR(_9773)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9773)) {
                _9774 = NewDouble(DBL_PTR(_index_ptr_17256)->dbl + (double)_9773);
            }
            else
            _9774 = NewDouble(DBL_PTR(_index_ptr_17256)->dbl + DBL_PTR(_9773)->dbl);
        }
    }
    DeRef(_9773);
    _9773 = NOVALUE;
    if (IS_ATOM_INT(_current_block_17251)) {
        _9775 = _current_block_17251 + 8;
        if ((long)((unsigned long)_9775 + (unsigned long)HIGH_BITS) >= 0) 
        _9775 = NewDouble((double)_9775);
    }
    else {
        _9775 = NewDouble(DBL_PTR(_current_block_17251)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_9774) && IS_ATOM_INT(_9775)) {
        _9776 = _9774 - _9775;
        if ((long)((unsigned long)_9776 +(unsigned long) HIGH_BITS) >= 0){
            _9776 = NewDouble((double)_9776);
        }
    }
    else {
        if (IS_ATOM_INT(_9774)) {
            _9776 = NewDouble((double)_9774 - DBL_PTR(_9775)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9775)) {
                _9776 = NewDouble(DBL_PTR(_9774)->dbl - (double)_9775);
            }
            else
            _9776 = NewDouble(DBL_PTR(_9774)->dbl - DBL_PTR(_9775)->dbl);
        }
    }
    DeRef(_9774);
    _9774 = NOVALUE;
    DeRef(_9775);
    _9775 = NOVALUE;
    _0 = _remaining_17246;
    _remaining_17246 = _15get_bytes(_33current_db_15557, _9776);
    DeRef(_0);
    _9776 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_17251)) {
        _9778 = _current_block_17251 + 8;
        if ((long)((unsigned long)_9778 + (unsigned long)HIGH_BITS) >= 0) 
        _9778 = NewDouble((double)_9778);
    }
    else {
        _9778 = NewDouble(DBL_PTR(_current_block_17251)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_895_17405);
    _pos_inlined_seek_at_895_17405 = _9778;
    _9778 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_895_17405);
    DeRef(_seek_1__tmp_at898_17407);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_895_17405;
    _seek_1__tmp_at898_17407 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_898_17406 = machine(19, _seek_1__tmp_at898_17407);
    DeRef(_pos_inlined_seek_at_895_17405);
    _pos_inlined_seek_at_895_17405 = NOVALUE;
    DeRef(_seek_1__tmp_at898_17407);
    _seek_1__tmp_at898_17407 = NOVALUE;

    /** 		put4(new_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)_new_recs_17261;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at913_17409);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at913_17409 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at913_17409); // DJP 

    /** end procedure*/
    goto L13; // [935] 938
L13: 
    DeRefi(_put4_1__tmp_at913_17409);
    _put4_1__tmp_at913_17409 = NOVALUE;

    /** 		put4(new_block)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_new_block_17255)) {
        *poke4_addr = (unsigned long)_new_block_17255;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_block_17255)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at941_17411);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at941_17411 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at941_17411); // DJP 

    /** end procedure*/
    goto L14; // [963] 966
L14: 
    DeRefi(_put4_1__tmp_at941_17411);
    _put4_1__tmp_at941_17411 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_17246); // DJP 

    /** end procedure*/
    goto L15; // [979] 982
L15: 

    /** 		io:seek(current_db, current_table_pos+8)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9779 = _33current_table_pos_15558 + 8;
        if ((long)((unsigned long)_9779 + (unsigned long)HIGH_BITS) >= 0) 
        _9779 = NewDouble((double)_9779);
    }
    else {
        _9779 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_991_17415);
    _pos_inlined_seek_at_991_17415 = _9779;
    _9779 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_991_17415);
    DeRef(_seek_1__tmp_at994_17417);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_991_17415;
    _seek_1__tmp_at994_17417 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_994_17416 = machine(19, _seek_1__tmp_at994_17417);
    DeRef(_pos_inlined_seek_at_991_17415);
    _pos_inlined_seek_at_991_17415 = NOVALUE;
    DeRef(_seek_1__tmp_at994_17417);
    _seek_1__tmp_at994_17417 = NOVALUE;

    /** 		blocks += 1*/
    _blocks_17260 = _blocks_17260 + 1;

    /** 		put4(blocks)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    *poke4_addr = (unsigned long)_blocks_17260;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1017_17420);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1017_17420 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at1017_17420); // DJP 

    /** end procedure*/
    goto L16; // [1039] 1042
L16: 
    DeRefi(_put4_1__tmp_at1017_17420);
    _put4_1__tmp_at1017_17420 = NOVALUE;

    /** 		io:seek(current_db, index_ptr-4)*/
    if (IS_ATOM_INT(_index_ptr_17256)) {
        _9781 = _index_ptr_17256 - 4;
        if ((long)((unsigned long)_9781 +(unsigned long) HIGH_BITS) >= 0){
            _9781 = NewDouble((double)_9781);
        }
    }
    else {
        _9781 = NewDouble(DBL_PTR(_index_ptr_17256)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_1051_17423);
    _pos_inlined_seek_at_1051_17423 = _9781;
    _9781 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1051_17423);
    DeRef(_seek_1__tmp_at1054_17425);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_1051_17423;
    _seek_1__tmp_at1054_17425 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1054_17424 = machine(19, _seek_1__tmp_at1054_17425);
    DeRef(_pos_inlined_seek_at_1051_17423);
    _pos_inlined_seek_at_1051_17423 = NOVALUE;
    DeRef(_seek_1__tmp_at1054_17425);
    _seek_1__tmp_at1054_17425 = NOVALUE;

    /** 		size = get4() - 4*/
    _9782 = _33get4();
    DeRef(_size_17252);
    if (IS_ATOM_INT(_9782)) {
        _size_17252 = _9782 - 4;
        if ((long)((unsigned long)_size_17252 +(unsigned long) HIGH_BITS) >= 0){
            _size_17252 = NewDouble((double)_size_17252);
        }
    }
    else {
        _size_17252 = binary_op(MINUS, _9782, 4);
    }
    DeRef(_9782);
    _9782 = NOVALUE;

    /** 		if blocks*8 > size-8 then*/
    if (_blocks_17260 == (short)_blocks_17260)
    _9784 = _blocks_17260 * 8;
    else
    _9784 = NewDouble(_blocks_17260 * (double)8);
    if (IS_ATOM_INT(_size_17252)) {
        _9785 = _size_17252 - 8;
        if ((long)((unsigned long)_9785 +(unsigned long) HIGH_BITS) >= 0){
            _9785 = NewDouble((double)_9785);
        }
    }
    else {
        _9785 = NewDouble(DBL_PTR(_size_17252)->dbl - (double)8);
    }
    if (binary_op_a(LESSEQ, _9784, _9785)){
        DeRef(_9784);
        _9784 = NOVALUE;
        DeRef(_9785);
        _9785 = NOVALUE;
        goto L17; // [1087] 1232
    }
    DeRef(_9784);
    _9784 = NOVALUE;
    DeRef(_9785);
    _9785 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, blocks*8)*/
    if (_blocks_17260 == (short)_blocks_17260)
    _9787 = _blocks_17260 * 8;
    else
    _9787 = NewDouble(_blocks_17260 * (double)8);
    _0 = _remaining_17246;
    _remaining_17246 = _15get_bytes(_33current_db_15557, _9787);
    DeRef(_0);
    _9787 = NOVALUE;

    /** 			new_size = floor(size + size/2)*/
    if (IS_ATOM_INT(_size_17252)) {
        if (_size_17252 & 1) {
            _9789 = NewDouble((_size_17252 >> 1) + 0.5);
        }
        else
        _9789 = _size_17252 >> 1;
    }
    else {
        _9789 = binary_op(DIVIDE, _size_17252, 2);
    }
    if (IS_ATOM_INT(_size_17252) && IS_ATOM_INT(_9789)) {
        _9790 = _size_17252 + _9789;
        if ((long)((unsigned long)_9790 + (unsigned long)HIGH_BITS) >= 0) 
        _9790 = NewDouble((double)_9790);
    }
    else {
        if (IS_ATOM_INT(_size_17252)) {
            _9790 = NewDouble((double)_size_17252 + DBL_PTR(_9789)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9789)) {
                _9790 = NewDouble(DBL_PTR(_size_17252)->dbl + (double)_9789);
            }
            else
            _9790 = NewDouble(DBL_PTR(_size_17252)->dbl + DBL_PTR(_9789)->dbl);
        }
    }
    DeRef(_9789);
    _9789 = NOVALUE;
    DeRef(_new_size_17253);
    if (IS_ATOM_INT(_9790))
    _new_size_17253 = e_floor(_9790);
    else
    _new_size_17253 = unary_op(FLOOR, _9790);
    DeRef(_9790);
    _9790 = NOVALUE;

    /** 			new_index_ptr = db_allocate(new_size)*/
    Ref(_new_size_17253);
    _0 = _new_index_ptr_17257;
    _new_index_ptr_17257 = _33db_allocate(_new_size_17253);
    DeRef(_0);

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_17246); // DJP 

    /** end procedure*/
    goto L18; // [1136] 1139
L18: 

    /** 			putn(repeat(0, new_size-blocks*8))*/
    if (_blocks_17260 == (short)_blocks_17260)
    _9793 = _blocks_17260 * 8;
    else
    _9793 = NewDouble(_blocks_17260 * (double)8);
    if (IS_ATOM_INT(_new_size_17253) && IS_ATOM_INT(_9793)) {
        _9794 = _new_size_17253 - _9793;
    }
    else {
        if (IS_ATOM_INT(_new_size_17253)) {
            _9794 = NewDouble((double)_new_size_17253 - DBL_PTR(_9793)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9793)) {
                _9794 = NewDouble(DBL_PTR(_new_size_17253)->dbl - (double)_9793);
            }
            else
            _9794 = NewDouble(DBL_PTR(_new_size_17253)->dbl - DBL_PTR(_9793)->dbl);
        }
    }
    DeRef(_9793);
    _9793 = NOVALUE;
    _9795 = Repeat(0, _9794);
    DeRef(_9794);
    _9794 = NOVALUE;
    DeRefi(_s_inlined_putn_at_1152_17443);
    _s_inlined_putn_at_1152_17443 = _9795;
    _9795 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _s_inlined_putn_at_1152_17443); // DJP 

    /** end procedure*/
    goto L19; // [1167] 1170
L19: 
    DeRefi(_s_inlined_putn_at_1152_17443);
    _s_inlined_putn_at_1152_17443 = NOVALUE;

    /** 			db_free(index_ptr)*/
    Ref(_index_ptr_17256);
    _33db_free(_index_ptr_17256);

    /** 			io:seek(current_db, current_table_pos+12)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9796 = _33current_table_pos_15558 + 12;
        if ((long)((unsigned long)_9796 + (unsigned long)HIGH_BITS) >= 0) 
        _9796 = NewDouble((double)_9796);
    }
    else {
        _9796 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_1186_17446);
    _pos_inlined_seek_at_1186_17446 = _9796;
    _9796 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1186_17446);
    DeRef(_seek_1__tmp_at1189_17448);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_1186_17446;
    _seek_1__tmp_at1189_17448 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1189_17447 = machine(19, _seek_1__tmp_at1189_17448);
    DeRef(_pos_inlined_seek_at_1186_17446);
    _pos_inlined_seek_at_1186_17446 = NOVALUE;
    DeRef(_seek_1__tmp_at1189_17448);
    _seek_1__tmp_at1189_17448 = NOVALUE;

    /** 			put4(new_index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_new_index_ptr_17257)) {
        *poke4_addr = (unsigned long)_new_index_ptr_17257;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_index_ptr_17257)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1204_17450);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1204_17450 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at1204_17450); // DJP 

    /** end procedure*/
    goto L1A; // [1226] 1229
L1A: 
    DeRefi(_put4_1__tmp_at1204_17450);
    _put4_1__tmp_at1204_17450 = NOVALUE;
L17: 
LE: 

    /** 	return DB_OK*/
    DeRef(_key_17240);
    DeRef(_data_17241);
    DeRef(_table_name_17242);
    DeRef(_key_string_17243);
    DeRef(_data_string_17244);
    DeRef(_last_part_17245);
    DeRef(_remaining_17246);
    DeRef(_key_ptr_17247);
    DeRef(_data_ptr_17248);
    DeRef(_records_ptr_17249);
    DeRef(_nrecs_17250);
    DeRef(_current_block_17251);
    DeRef(_size_17252);
    DeRef(_new_size_17253);
    DeRef(_key_location_17254);
    DeRef(_new_block_17255);
    DeRef(_index_ptr_17256);
    DeRef(_new_index_ptr_17257);
    DeRef(_total_recs_17258);
    DeRef(_9741);
    _9741 = NOVALUE;
    DeRef(_9743);
    _9743 = NOVALUE;
    return 0;
    ;
}


void _33db_delete_record(int _key_location_17453, int _table_name_17454)
{
    int _key_ptr_17455 = NOVALUE;
    int _nrecs_17456 = NOVALUE;
    int _records_ptr_17457 = NOVALUE;
    int _data_ptr_17458 = NOVALUE;
    int _index_ptr_17459 = NOVALUE;
    int _current_block_17460 = NOVALUE;
    int _r_17461 = NOVALUE;
    int _blocks_17462 = NOVALUE;
    int _n_17463 = NOVALUE;
    int _remaining_17464 = NOVALUE;
    int _seek_1__tmp_at132_17486 = NOVALUE;
    int _seek_inlined_seek_at_132_17485 = NOVALUE;
    int _seek_1__tmp_at277_17508 = NOVALUE;
    int _seek_inlined_seek_at_277_17507 = NOVALUE;
    int _pos_inlined_seek_at_274_17506 = NOVALUE;
    int _seek_1__tmp_at321_17516 = NOVALUE;
    int _seek_inlined_seek_at_321_17515 = NOVALUE;
    int _pos_inlined_seek_at_318_17514 = NOVALUE;
    int _put4_1__tmp_at336_17518 = NOVALUE;
    int _seek_1__tmp_at375_17523 = NOVALUE;
    int _seek_inlined_seek_at_375_17522 = NOVALUE;
    int _pos_inlined_seek_at_372_17521 = NOVALUE;
    int _seek_1__tmp_at397_17527 = NOVALUE;
    int _seek_inlined_seek_at_397_17526 = NOVALUE;
    int _where_inlined_where_at_472_17536 = NOVALUE;
    int _seek_1__tmp_at538_17550 = NOVALUE;
    int _seek_inlined_seek_at_538_17549 = NOVALUE;
    int _seek_1__tmp_at578_17556 = NOVALUE;
    int _seek_inlined_seek_at_578_17555 = NOVALUE;
    int _pos_inlined_seek_at_575_17554 = NOVALUE;
    int _put4_1__tmp_at600_17560 = NOVALUE;
    int _x_inlined_put4_at_597_17559 = NOVALUE;
    int _seek_1__tmp_at648_17565 = NOVALUE;
    int _seek_inlined_seek_at_648_17564 = NOVALUE;
    int _put4_1__tmp_at663_17567 = NOVALUE;
    int _seek_1__tmp_at710_17574 = NOVALUE;
    int _seek_inlined_seek_at_710_17573 = NOVALUE;
    int _pos_inlined_seek_at_707_17572 = NOVALUE;
    int _put4_1__tmp_at750_17582 = NOVALUE;
    int _x_inlined_put4_at_747_17581 = NOVALUE;
    int _9856 = NOVALUE;
    int _9855 = NOVALUE;
    int _9854 = NOVALUE;
    int _9853 = NOVALUE;
    int _9852 = NOVALUE;
    int _9851 = NOVALUE;
    int _9849 = NOVALUE;
    int _9848 = NOVALUE;
    int _9846 = NOVALUE;
    int _9845 = NOVALUE;
    int _9844 = NOVALUE;
    int _9843 = NOVALUE;
    int _9842 = NOVALUE;
    int _9841 = NOVALUE;
    int _9840 = NOVALUE;
    int _9831 = NOVALUE;
    int _9830 = NOVALUE;
    int _9827 = NOVALUE;
    int _9826 = NOVALUE;
    int _9824 = NOVALUE;
    int _9823 = NOVALUE;
    int _9821 = NOVALUE;
    int _9820 = NOVALUE;
    int _9819 = NOVALUE;
    int _9818 = NOVALUE;
    int _9816 = NOVALUE;
    int _9812 = NOVALUE;
    int _9810 = NOVALUE;
    int _9808 = NOVALUE;
    int _9807 = NOVALUE;
    int _9805 = NOVALUE;
    int _9804 = NOVALUE;
    int _9802 = NOVALUE;
    int _9799 = NOVALUE;
    int _9797 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_17453)) {
        _1 = (long)(DBL_PTR(_key_location_17453)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_17453)) && (DBL_PTR(_key_location_17453)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_17453);
        _key_location_17453 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_17454 == _33current_table_name_15559)
    _9797 = 1;
    else if (IS_ATOM_INT(_table_name_17454) && IS_ATOM_INT(_33current_table_name_15559))
    _9797 = 0;
    else
    _9797 = (compare(_table_name_17454, _33current_table_name_15559) == 0);
    if (_9797 != 0)
    goto L1; // [13] 49
    _9797 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_17454);
    _9799 = _33db_select_table(_table_name_17454);
    if (binary_op_a(EQUALS, _9799, 0)){
        DeRef(_9799);
        _9799 = NOVALUE;
        goto L2; // [24] 48
    }
    DeRef(_9799);
    _9799 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_delete_record", {key_location, table_name})*/
    Ref(_table_name_17454);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17453;
    ((int *)_2)[2] = _table_name_17454;
    _9802 = MAKE_SEQ(_1);
    RefDS(_9683);
    RefDS(_9801);
    _33fatal(903, _9683, _9801, _9802);
    _9802 = NOVALUE;

    /** 			return*/
    DeRef(_table_name_17454);
    DeRef(_key_ptr_17455);
    DeRef(_nrecs_17456);
    DeRef(_records_ptr_17457);
    DeRef(_data_ptr_17458);
    DeRef(_index_ptr_17459);
    DeRef(_current_block_17460);
    DeRef(_remaining_17464);
    return;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _33current_table_pos_15558, -1)){
        goto L3; // [53] 77
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_delete_record", {key_location, table_name})*/
    Ref(_table_name_17454);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17453;
    ((int *)_2)[2] = _table_name_17454;
    _9804 = MAKE_SEQ(_1);
    RefDS(_9687);
    RefDS(_9801);
    _33fatal(903, _9687, _9801, _9804);
    _9804 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_17454);
    DeRef(_key_ptr_17455);
    DeRef(_nrecs_17456);
    DeRef(_records_ptr_17457);
    DeRef(_data_ptr_17458);
    DeRef(_index_ptr_17459);
    DeRef(_current_block_17460);
    DeRef(_remaining_17464);
    return;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _9805 = (_key_location_17453 < 1);
    if (_9805 != 0) {
        goto L4; // [83] 101
    }
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _9807 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _9807 = 1;
    }
    _9808 = (_key_location_17453 > _9807);
    _9807 = NOVALUE;
    if (_9808 == 0)
    {
        DeRef(_9808);
        _9808 = NOVALUE;
        goto L5; // [97] 121
    }
    else{
        DeRef(_9808);
        _9808 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_delete_record", {key_location, table_name})*/
    Ref(_table_name_17454);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17453;
    ((int *)_2)[2] = _table_name_17454;
    _9810 = MAKE_SEQ(_1);
    RefDS(_9809);
    RefDS(_9801);
    _33fatal(905, _9809, _9801, _9810);
    _9810 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_17454);
    DeRef(_key_ptr_17455);
    DeRef(_nrecs_17456);
    DeRef(_records_ptr_17457);
    DeRef(_data_ptr_17458);
    DeRef(_index_ptr_17459);
    DeRef(_current_block_17460);
    DeRef(_remaining_17464);
    DeRef(_9805);
    _9805 = NOVALUE;
    return;
L5: 

    /** 	key_ptr = key_pointers[key_location]*/
    DeRef(_key_ptr_17455);
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    _key_ptr_17455 = (int)*(((s1_ptr)_2)->base + _key_location_17453);
    Ref(_key_ptr_17455);

    /** 	io:seek(current_db, key_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_key_ptr_17455);
    DeRef(_seek_1__tmp_at132_17486);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _key_ptr_17455;
    _seek_1__tmp_at132_17486 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_132_17485 = machine(19, _seek_1__tmp_at132_17486);
    DeRef(_seek_1__tmp_at132_17486);
    _seek_1__tmp_at132_17486 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return end if*/
    if (IS_SEQUENCE(_33vLastErrors_15581)){
            _9812 = SEQ_PTR(_33vLastErrors_15581)->length;
    }
    else {
        _9812 = 1;
    }
    if (_9812 <= 0)
    goto L6; // [153] 161
    DeRef(_table_name_17454);
    DeRef(_key_ptr_17455);
    DeRef(_nrecs_17456);
    DeRef(_records_ptr_17457);
    DeRef(_data_ptr_17458);
    DeRef(_index_ptr_17459);
    DeRef(_current_block_17460);
    DeRef(_remaining_17464);
    DeRef(_9805);
    _9805 = NOVALUE;
    return;
L6: 

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_17458;
    _data_ptr_17458 = _33get4();
    DeRef(_0);

    /** 	db_free(key_ptr)*/
    Ref(_key_ptr_17455);
    _33db_free(_key_ptr_17455);

    /** 	db_free(data_ptr)*/
    Ref(_data_ptr_17458);
    _33db_free(_data_ptr_17458);

    /** 	n = length(key_pointers)*/
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _n_17463 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _n_17463 = 1;
    }

    /** 	if key_location >= floor(n/2) then*/
    _9816 = _n_17463 >> 1;
    if (_key_location_17453 < _9816)
    goto L7; // [191] 235

    /** 		key_pointers[key_location..n-1] = key_pointers[key_location+1..n]*/
    _9818 = _n_17463 - 1;
    _9819 = _key_location_17453 + 1;
    rhs_slice_target = (object_ptr)&_9820;
    RHS_Slice(_33key_pointers_15564, _9819, _n_17463);
    assign_slice_seq = (s1_ptr *)&_33key_pointers_15564;
    AssignSlice(_key_location_17453, _9818, _9820);
    _9818 = NOVALUE;
    DeRefDS(_9820);
    _9820 = NOVALUE;

    /** 		key_pointers = key_pointers[1..n-1]*/
    _9821 = _n_17463 - 1;
    rhs_slice_target = (object_ptr)&_33key_pointers_15564;
    RHS_Slice(_33key_pointers_15564, 1, _9821);
    goto L8; // [232] 265
L7: 

    /** 		key_pointers[2..key_location] = key_pointers[1..key_location-1]*/
    _9823 = _key_location_17453 - 1;
    rhs_slice_target = (object_ptr)&_9824;
    RHS_Slice(_33key_pointers_15564, 1, _9823);
    assign_slice_seq = (s1_ptr *)&_33key_pointers_15564;
    AssignSlice(2, _key_location_17453, _9824);
    DeRefDS(_9824);
    _9824 = NOVALUE;

    /** 		key_pointers = key_pointers[2..n]*/
    rhs_slice_target = (object_ptr)&_33key_pointers_15564;
    RHS_Slice(_33key_pointers_15564, 2, _n_17463);
L8: 

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9826 = _33current_table_pos_15558 + 4;
        if ((long)((unsigned long)_9826 + (unsigned long)HIGH_BITS) >= 0) 
        _9826 = NewDouble((double)_9826);
    }
    else {
        _9826 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_274_17506);
    _pos_inlined_seek_at_274_17506 = _9826;
    _9826 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_274_17506);
    DeRef(_seek_1__tmp_at277_17508);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_274_17506;
    _seek_1__tmp_at277_17508 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_277_17507 = machine(19, _seek_1__tmp_at277_17508);
    DeRef(_pos_inlined_seek_at_274_17506);
    _pos_inlined_seek_at_274_17506 = NOVALUE;
    DeRef(_seek_1__tmp_at277_17508);
    _seek_1__tmp_at277_17508 = NOVALUE;

    /** 	nrecs = get4()-1*/
    _9827 = _33get4();
    DeRef(_nrecs_17456);
    if (IS_ATOM_INT(_9827)) {
        _nrecs_17456 = _9827 - 1;
        if ((long)((unsigned long)_nrecs_17456 +(unsigned long) HIGH_BITS) >= 0){
            _nrecs_17456 = NewDouble((double)_nrecs_17456);
        }
    }
    else {
        _nrecs_17456 = binary_op(MINUS, _9827, 1);
    }
    DeRef(_9827);
    _9827 = NOVALUE;

    /** 	blocks = get4()*/
    _blocks_17462 = _33get4();
    if (!IS_ATOM_INT(_blocks_17462)) {
        _1 = (long)(DBL_PTR(_blocks_17462)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_17462)) && (DBL_PTR(_blocks_17462)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_17462);
        _blocks_17462 = _1;
    }

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9830 = _33current_table_pos_15558 + 4;
        if ((long)((unsigned long)_9830 + (unsigned long)HIGH_BITS) >= 0) 
        _9830 = NewDouble((double)_9830);
    }
    else {
        _9830 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_318_17514);
    _pos_inlined_seek_at_318_17514 = _9830;
    _9830 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_318_17514);
    DeRef(_seek_1__tmp_at321_17516);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_318_17514;
    _seek_1__tmp_at321_17516 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_321_17515 = machine(19, _seek_1__tmp_at321_17516);
    DeRef(_pos_inlined_seek_at_318_17514);
    _pos_inlined_seek_at_318_17514 = NOVALUE;
    DeRef(_seek_1__tmp_at321_17516);
    _seek_1__tmp_at321_17516 = NOVALUE;

    /** 	put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_17456)) {
        *poke4_addr = (unsigned long)_nrecs_17456;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_17456)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at336_17518);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at336_17518 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at336_17518); // DJP 

    /** end procedure*/
    goto L9; // [358] 361
L9: 
    DeRefi(_put4_1__tmp_at336_17518);
    _put4_1__tmp_at336_17518 = NOVALUE;

    /** 	io:seek(current_db, current_table_pos+12)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9831 = _33current_table_pos_15558 + 12;
        if ((long)((unsigned long)_9831 + (unsigned long)HIGH_BITS) >= 0) 
        _9831 = NewDouble((double)_9831);
    }
    else {
        _9831 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_372_17521);
    _pos_inlined_seek_at_372_17521 = _9831;
    _9831 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_372_17521);
    DeRef(_seek_1__tmp_at375_17523);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_372_17521;
    _seek_1__tmp_at375_17523 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_375_17522 = machine(19, _seek_1__tmp_at375_17523);
    DeRef(_pos_inlined_seek_at_372_17521);
    _pos_inlined_seek_at_372_17521 = NOVALUE;
    DeRef(_seek_1__tmp_at375_17523);
    _seek_1__tmp_at375_17523 = NOVALUE;

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_17459;
    _index_ptr_17459 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, index_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_index_ptr_17459);
    DeRef(_seek_1__tmp_at397_17527);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _index_ptr_17459;
    _seek_1__tmp_at397_17527 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_397_17526 = machine(19, _seek_1__tmp_at397_17527);
    DeRef(_seek_1__tmp_at397_17527);
    _seek_1__tmp_at397_17527 = NOVALUE;

    /** 	r = 0*/
    _r_17461 = 0;

    /** 	while TRUE do*/
LA: 

    /** 		nrecs = get4()*/
    _0 = _nrecs_17456;
    _nrecs_17456 = _33get4();
    DeRef(_0);

    /** 		records_ptr = get4()*/
    _0 = _records_ptr_17457;
    _records_ptr_17457 = _33get4();
    DeRef(_0);

    /** 		r += nrecs*/
    if (IS_ATOM_INT(_nrecs_17456)) {
        _r_17461 = _r_17461 + _nrecs_17456;
    }
    else {
        _r_17461 = NewDouble((double)_r_17461 + DBL_PTR(_nrecs_17456)->dbl);
    }
    if (!IS_ATOM_INT(_r_17461)) {
        _1 = (long)(DBL_PTR(_r_17461)->dbl);
        if (UNIQUE(DBL_PTR(_r_17461)) && (DBL_PTR(_r_17461)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_17461);
        _r_17461 = _1;
    }

    /** 		if r >= key_location then*/
    if (_r_17461 < _key_location_17453)
    goto LA; // [445] 423

    /** 			exit*/
    goto LB; // [451] 459

    /** 	end while*/
    goto LA; // [456] 423
LB: 

    /** 	r -= nrecs*/
    if (IS_ATOM_INT(_nrecs_17456)) {
        _r_17461 = _r_17461 - _nrecs_17456;
    }
    else {
        _r_17461 = NewDouble((double)_r_17461 - DBL_PTR(_nrecs_17456)->dbl);
    }
    if (!IS_ATOM_INT(_r_17461)) {
        _1 = (long)(DBL_PTR(_r_17461)->dbl);
        if (UNIQUE(DBL_PTR(_r_17461)) && (DBL_PTR(_r_17461)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_17461);
        _r_17461 = _1;
    }

    /** 	current_block = io:where(current_db)-8*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_472_17536);
    _where_inlined_where_at_472_17536 = machine(20, _33current_db_15557);
    DeRef(_current_block_17460);
    if (IS_ATOM_INT(_where_inlined_where_at_472_17536)) {
        _current_block_17460 = _where_inlined_where_at_472_17536 - 8;
        if ((long)((unsigned long)_current_block_17460 +(unsigned long) HIGH_BITS) >= 0){
            _current_block_17460 = NewDouble((double)_current_block_17460);
        }
    }
    else {
        _current_block_17460 = NewDouble(DBL_PTR(_where_inlined_where_at_472_17536)->dbl - (double)8);
    }

    /** 	nrecs -= 1*/
    _0 = _nrecs_17456;
    if (IS_ATOM_INT(_nrecs_17456)) {
        _nrecs_17456 = _nrecs_17456 - 1;
        if ((long)((unsigned long)_nrecs_17456 +(unsigned long) HIGH_BITS) >= 0){
            _nrecs_17456 = NewDouble((double)_nrecs_17456);
        }
    }
    else {
        _nrecs_17456 = NewDouble(DBL_PTR(_nrecs_17456)->dbl - (double)1);
    }
    DeRef(_0);

    /** 	if nrecs = 0 and blocks > 1 then*/
    if (IS_ATOM_INT(_nrecs_17456)) {
        _9840 = (_nrecs_17456 == 0);
    }
    else {
        _9840 = (DBL_PTR(_nrecs_17456)->dbl == (double)0);
    }
    if (_9840 == 0) {
        goto LC; // [496] 637
    }
    _9842 = (_blocks_17462 > 1);
    if (_9842 == 0)
    {
        DeRef(_9842);
        _9842 = NOVALUE;
        goto LC; // [505] 637
    }
    else{
        DeRef(_9842);
        _9842 = NOVALUE;
    }

    /** 		remaining = io:get_bytes(current_db, index_ptr+blocks*8-(current_block+8))*/
    if (_blocks_17462 == (short)_blocks_17462)
    _9843 = _blocks_17462 * 8;
    else
    _9843 = NewDouble(_blocks_17462 * (double)8);
    if (IS_ATOM_INT(_index_ptr_17459) && IS_ATOM_INT(_9843)) {
        _9844 = _index_ptr_17459 + _9843;
        if ((long)((unsigned long)_9844 + (unsigned long)HIGH_BITS) >= 0) 
        _9844 = NewDouble((double)_9844);
    }
    else {
        if (IS_ATOM_INT(_index_ptr_17459)) {
            _9844 = NewDouble((double)_index_ptr_17459 + DBL_PTR(_9843)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9843)) {
                _9844 = NewDouble(DBL_PTR(_index_ptr_17459)->dbl + (double)_9843);
            }
            else
            _9844 = NewDouble(DBL_PTR(_index_ptr_17459)->dbl + DBL_PTR(_9843)->dbl);
        }
    }
    DeRef(_9843);
    _9843 = NOVALUE;
    if (IS_ATOM_INT(_current_block_17460)) {
        _9845 = _current_block_17460 + 8;
        if ((long)((unsigned long)_9845 + (unsigned long)HIGH_BITS) >= 0) 
        _9845 = NewDouble((double)_9845);
    }
    else {
        _9845 = NewDouble(DBL_PTR(_current_block_17460)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_9844) && IS_ATOM_INT(_9845)) {
        _9846 = _9844 - _9845;
        if ((long)((unsigned long)_9846 +(unsigned long) HIGH_BITS) >= 0){
            _9846 = NewDouble((double)_9846);
        }
    }
    else {
        if (IS_ATOM_INT(_9844)) {
            _9846 = NewDouble((double)_9844 - DBL_PTR(_9845)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9845)) {
                _9846 = NewDouble(DBL_PTR(_9844)->dbl - (double)_9845);
            }
            else
            _9846 = NewDouble(DBL_PTR(_9844)->dbl - DBL_PTR(_9845)->dbl);
        }
    }
    DeRef(_9844);
    _9844 = NOVALUE;
    DeRef(_9845);
    _9845 = NOVALUE;
    _0 = _remaining_17464;
    _remaining_17464 = _15get_bytes(_33current_db_15557, _9846);
    DeRef(_0);
    _9846 = NOVALUE;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_17460);
    DeRef(_seek_1__tmp_at538_17550);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _current_block_17460;
    _seek_1__tmp_at538_17550 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_538_17549 = machine(19, _seek_1__tmp_at538_17550);
    DeRef(_seek_1__tmp_at538_17550);
    _seek_1__tmp_at538_17550 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _remaining_17464); // DJP 

    /** end procedure*/
    goto LD; // [563] 566
LD: 

    /** 		io:seek(current_db, current_table_pos+8)*/
    if (IS_ATOM_INT(_33current_table_pos_15558)) {
        _9848 = _33current_table_pos_15558 + 8;
        if ((long)((unsigned long)_9848 + (unsigned long)HIGH_BITS) >= 0) 
        _9848 = NewDouble((double)_9848);
    }
    else {
        _9848 = NewDouble(DBL_PTR(_33current_table_pos_15558)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_575_17554);
    _pos_inlined_seek_at_575_17554 = _9848;
    _9848 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_575_17554);
    DeRef(_seek_1__tmp_at578_17556);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_575_17554;
    _seek_1__tmp_at578_17556 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_578_17555 = machine(19, _seek_1__tmp_at578_17556);
    DeRef(_pos_inlined_seek_at_575_17554);
    _pos_inlined_seek_at_575_17554 = NOVALUE;
    DeRef(_seek_1__tmp_at578_17556);
    _seek_1__tmp_at578_17556 = NOVALUE;

    /** 		put4(blocks-1)*/
    _9849 = _blocks_17462 - 1;
    if ((long)((unsigned long)_9849 +(unsigned long) HIGH_BITS) >= 0){
        _9849 = NewDouble((double)_9849);
    }
    DeRef(_x_inlined_put4_at_597_17559);
    _x_inlined_put4_at_597_17559 = _9849;
    _9849 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_597_17559)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_597_17559;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_597_17559)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at600_17560);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at600_17560 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at600_17560); // DJP 

    /** end procedure*/
    goto LE; // [622] 625
LE: 
    DeRef(_x_inlined_put4_at_597_17559);
    _x_inlined_put4_at_597_17559 = NOVALUE;
    DeRefi(_put4_1__tmp_at600_17560);
    _put4_1__tmp_at600_17560 = NOVALUE;

    /** 		db_free(records_ptr)*/
    Ref(_records_ptr_17457);
    _33db_free(_records_ptr_17457);
    goto LF; // [634] 785
LC: 

    /** 		key_location -= r*/
    _key_location_17453 = _key_location_17453 - _r_17461;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_17460);
    DeRef(_seek_1__tmp_at648_17565);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _current_block_17460;
    _seek_1__tmp_at648_17565 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_648_17564 = machine(19, _seek_1__tmp_at648_17565);
    DeRef(_seek_1__tmp_at648_17565);
    _seek_1__tmp_at648_17565 = NOVALUE;

    /** 		put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_17456)) {
        *poke4_addr = (unsigned long)_nrecs_17456;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_17456)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at663_17567);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at663_17567 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at663_17567); // DJP 

    /** end procedure*/
    goto L10; // [685] 688
L10: 
    DeRefi(_put4_1__tmp_at663_17567);
    _put4_1__tmp_at663_17567 = NOVALUE;

    /** 		io:seek(current_db, records_ptr+4*(key_location-1))*/
    _9851 = _key_location_17453 - 1;
    if ((long)((unsigned long)_9851 +(unsigned long) HIGH_BITS) >= 0){
        _9851 = NewDouble((double)_9851);
    }
    if (IS_ATOM_INT(_9851)) {
        if (_9851 <= INT15 && _9851 >= -INT15)
        _9852 = 4 * _9851;
        else
        _9852 = NewDouble(4 * (double)_9851);
    }
    else {
        _9852 = NewDouble((double)4 * DBL_PTR(_9851)->dbl);
    }
    DeRef(_9851);
    _9851 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_17457) && IS_ATOM_INT(_9852)) {
        _9853 = _records_ptr_17457 + _9852;
        if ((long)((unsigned long)_9853 + (unsigned long)HIGH_BITS) >= 0) 
        _9853 = NewDouble((double)_9853);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_17457)) {
            _9853 = NewDouble((double)_records_ptr_17457 + DBL_PTR(_9852)->dbl);
        }
        else {
            if (IS_ATOM_INT(_9852)) {
                _9853 = NewDouble(DBL_PTR(_records_ptr_17457)->dbl + (double)_9852);
            }
            else
            _9853 = NewDouble(DBL_PTR(_records_ptr_17457)->dbl + DBL_PTR(_9852)->dbl);
        }
    }
    DeRef(_9852);
    _9852 = NOVALUE;
    DeRef(_pos_inlined_seek_at_707_17572);
    _pos_inlined_seek_at_707_17572 = _9853;
    _9853 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_707_17572);
    DeRef(_seek_1__tmp_at710_17574);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_707_17572;
    _seek_1__tmp_at710_17574 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_710_17573 = machine(19, _seek_1__tmp_at710_17574);
    DeRef(_pos_inlined_seek_at_707_17572);
    _pos_inlined_seek_at_707_17572 = NOVALUE;
    DeRef(_seek_1__tmp_at710_17574);
    _seek_1__tmp_at710_17574 = NOVALUE;

    /** 		for i = key_location to nrecs do*/
    Ref(_nrecs_17456);
    DeRef(_9854);
    _9854 = _nrecs_17456;
    {
        int _i_17576;
        _i_17576 = _key_location_17453;
L11: 
        if (binary_op_a(GREATER, _i_17576, _9854)){
            goto L12; // [729] 784
        }

        /** 			put4(key_pointers[i+r])*/
        if (IS_ATOM_INT(_i_17576)) {
            _9855 = _i_17576 + _r_17461;
        }
        else {
            _9855 = NewDouble(DBL_PTR(_i_17576)->dbl + (double)_r_17461);
        }
        _2 = (int)SEQ_PTR(_33key_pointers_15564);
        if (!IS_ATOM_INT(_9855)){
            _9856 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_9855)->dbl));
        }
        else{
            _9856 = (int)*(((s1_ptr)_2)->base + _9855);
        }
        Ref(_9856);
        DeRef(_x_inlined_put4_at_747_17581);
        _x_inlined_put4_at_747_17581 = _9856;
        _9856 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_33mem0_15599)){
            poke4_addr = (unsigned long *)_33mem0_15599;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_747_17581)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_747_17581;
        }
        else if (IS_ATOM(_x_inlined_put4_at_747_17581)) {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_747_17581)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_x_inlined_put4_at_747_17581);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at750_17582);
        _1 = (int)SEQ_PTR(_33memseq_15819);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at750_17582 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_33current_db_15557, _put4_1__tmp_at750_17582); // DJP 

        /** end procedure*/
        goto L13; // [772] 775
L13: 
        DeRef(_x_inlined_put4_at_747_17581);
        _x_inlined_put4_at_747_17581 = NOVALUE;
        DeRefi(_put4_1__tmp_at750_17582);
        _put4_1__tmp_at750_17582 = NOVALUE;

        /** 		end for*/
        _0 = _i_17576;
        if (IS_ATOM_INT(_i_17576)) {
            _i_17576 = _i_17576 + 1;
            if ((long)((unsigned long)_i_17576 +(unsigned long) HIGH_BITS) >= 0){
                _i_17576 = NewDouble((double)_i_17576);
            }
        }
        else {
            _i_17576 = binary_op_a(PLUS, _i_17576, 1);
        }
        DeRef(_0);
        goto L11; // [779] 736
L12: 
        ;
        DeRef(_i_17576);
    }
LF: 

    /** end procedure*/
    DeRef(_table_name_17454);
    DeRef(_key_ptr_17455);
    DeRef(_nrecs_17456);
    DeRef(_records_ptr_17457);
    DeRef(_data_ptr_17458);
    DeRef(_index_ptr_17459);
    DeRef(_current_block_17460);
    DeRef(_remaining_17464);
    DeRef(_9805);
    _9805 = NOVALUE;
    DeRef(_9816);
    _9816 = NOVALUE;
    DeRef(_9821);
    _9821 = NOVALUE;
    DeRef(_9819);
    _9819 = NOVALUE;
    DeRef(_9823);
    _9823 = NOVALUE;
    DeRef(_9840);
    _9840 = NOVALUE;
    DeRef(_9855);
    _9855 = NOVALUE;
    return;
    ;
}


void _33db_replace_data(int _key_location_17585, int _data_17586, int _table_name_17587)
{
    int _9870 = NOVALUE;
    int _9869 = NOVALUE;
    int _9868 = NOVALUE;
    int _9867 = NOVALUE;
    int _9865 = NOVALUE;
    int _9864 = NOVALUE;
    int _9862 = NOVALUE;
    int _9859 = NOVALUE;
    int _9857 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_17585)) {
        _1 = (long)(DBL_PTR(_key_location_17585)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_17585)) && (DBL_PTR(_key_location_17585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_17585);
        _key_location_17585 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_17587 == _33current_table_name_15559)
    _9857 = 1;
    else if (IS_ATOM_INT(_table_name_17587) && IS_ATOM_INT(_33current_table_name_15559))
    _9857 = 0;
    else
    _9857 = (compare(_table_name_17587, _33current_table_name_15559) == 0);
    if (_9857 != 0)
    goto L1; // [13] 51
    _9857 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_17587);
    _9859 = _33db_select_table(_table_name_17587);
    if (binary_op_a(EQUALS, _9859, 0)){
        DeRef(_9859);
        _9859 = NOVALUE;
        goto L2; // [24] 50
    }
    DeRef(_9859);
    _9859 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_17585;
    Ref(_data_17586);
    *((int *)(_2+8)) = _data_17586;
    Ref(_table_name_17587);
    *((int *)(_2+12)) = _table_name_17587;
    _9862 = MAKE_SEQ(_1);
    RefDS(_9683);
    RefDS(_9861);
    _33fatal(903, _9683, _9861, _9862);
    _9862 = NOVALUE;

    /** 			return*/
    DeRef(_data_17586);
    DeRef(_table_name_17587);
    return;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _33current_table_pos_15558, -1)){
        goto L3; // [55] 81
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_17585;
    Ref(_data_17586);
    *((int *)(_2+8)) = _data_17586;
    Ref(_table_name_17587);
    *((int *)(_2+12)) = _table_name_17587;
    _9864 = MAKE_SEQ(_1);
    RefDS(_9687);
    RefDS(_9861);
    _33fatal(903, _9687, _9861, _9864);
    _9864 = NOVALUE;

    /** 		return*/
    DeRef(_data_17586);
    DeRef(_table_name_17587);
    return;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _9865 = (_key_location_17585 < 1);
    if (_9865 != 0) {
        goto L4; // [87] 105
    }
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _9867 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _9867 = 1;
    }
    _9868 = (_key_location_17585 > _9867);
    _9867 = NOVALUE;
    if (_9868 == 0)
    {
        DeRef(_9868);
        _9868 = NOVALUE;
        goto L5; // [101] 127
    }
    else{
        DeRef(_9868);
        _9868 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_17585;
    Ref(_data_17586);
    *((int *)(_2+8)) = _data_17586;
    Ref(_table_name_17587);
    *((int *)(_2+12)) = _table_name_17587;
    _9869 = MAKE_SEQ(_1);
    RefDS(_9809);
    RefDS(_9861);
    _33fatal(905, _9809, _9861, _9869);
    _9869 = NOVALUE;

    /** 		return*/
    DeRef(_data_17586);
    DeRef(_table_name_17587);
    DeRef(_9865);
    _9865 = NOVALUE;
    return;
L5: 

    /** 	db_replace_recid(key_pointers[key_location], data)*/
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    _9870 = (int)*(((s1_ptr)_2)->base + _key_location_17585);
    Ref(_9870);
    Ref(_data_17586);
    _33db_replace_recid(_9870, _data_17586);
    _9870 = NOVALUE;

    /** end procedure*/
    DeRef(_data_17586);
    DeRef(_table_name_17587);
    DeRef(_9865);
    _9865 = NOVALUE;
    return;
    ;
}


int _33db_table_size(int _table_name_17609)
{
    int _9879 = NOVALUE;
    int _9878 = NOVALUE;
    int _9876 = NOVALUE;
    int _9873 = NOVALUE;
    int _9871 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_17609 == _33current_table_name_15559)
    _9871 = 1;
    else if (IS_ATOM_INT(_table_name_17609) && IS_ATOM_INT(_33current_table_name_15559))
    _9871 = 0;
    else
    _9871 = (compare(_table_name_17609, _33current_table_name_15559) == 0);
    if (_9871 != 0)
    goto L1; // [9] 46
    _9871 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_17609);
    _9873 = _33db_select_table(_table_name_17609);
    if (binary_op_a(EQUALS, _9873, 0)){
        DeRef(_9873);
        _9873 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_9873);
    _9873 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_table_name_17609);
    *((int *)(_2+4)) = _table_name_17609;
    _9876 = MAKE_SEQ(_1);
    RefDS(_9683);
    RefDS(_9875);
    _33fatal(903, _9683, _9875, _9876);
    _9876 = NOVALUE;

    /** 			return -1*/
    DeRef(_table_name_17609);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _33current_table_pos_15558, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_table_name_17609);
    *((int *)(_2+4)) = _table_name_17609;
    _9878 = MAKE_SEQ(_1);
    RefDS(_9687);
    RefDS(_9875);
    _33fatal(903, _9687, _9875, _9878);
    _9878 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_17609);
    return -1;
L3: 

    /** 	return length(key_pointers)*/
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _9879 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _9879 = 1;
    }
    DeRef(_table_name_17609);
    return _9879;
    ;
}


int _33db_record_data(int _key_location_17624, int _table_name_17625)
{
    int _data_ptr_17626 = NOVALUE;
    int _data_value_17627 = NOVALUE;
    int _seek_1__tmp_at136_17649 = NOVALUE;
    int _seek_inlined_seek_at_136_17648 = NOVALUE;
    int _pos_inlined_seek_at_133_17647 = NOVALUE;
    int _seek_1__tmp_at174_17656 = NOVALUE;
    int _seek_inlined_seek_at_174_17655 = NOVALUE;
    int _9894 = NOVALUE;
    int _9893 = NOVALUE;
    int _9892 = NOVALUE;
    int _9891 = NOVALUE;
    int _9890 = NOVALUE;
    int _9888 = NOVALUE;
    int _9887 = NOVALUE;
    int _9885 = NOVALUE;
    int _9882 = NOVALUE;
    int _9880 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_17624)) {
        _1 = (long)(DBL_PTR(_key_location_17624)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_17624)) && (DBL_PTR(_key_location_17624)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_17624);
        _key_location_17624 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_17625 == _33current_table_name_15559)
    _9880 = 1;
    else if (IS_ATOM_INT(_table_name_17625) && IS_ATOM_INT(_33current_table_name_15559))
    _9880 = 0;
    else
    _9880 = (compare(_table_name_17625, _33current_table_name_15559) == 0);
    if (_9880 != 0)
    goto L1; // [13] 50
    _9880 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_17625);
    _9882 = _33db_select_table(_table_name_17625);
    if (binary_op_a(EQUALS, _9882, 0)){
        DeRef(_9882);
        _9882 = NOVALUE;
        goto L2; // [24] 49
    }
    DeRef(_9882);
    _9882 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_17625);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17624;
    ((int *)_2)[2] = _table_name_17625;
    _9885 = MAKE_SEQ(_1);
    RefDS(_9683);
    RefDS(_9884);
    _33fatal(903, _9683, _9884, _9885);
    _9885 = NOVALUE;

    /** 			return -1*/
    DeRef(_table_name_17625);
    DeRef(_data_ptr_17626);
    DeRef(_data_value_17627);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _33current_table_pos_15558, -1)){
        goto L3; // [54] 79
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_17625);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17624;
    ((int *)_2)[2] = _table_name_17625;
    _9887 = MAKE_SEQ(_1);
    RefDS(_9687);
    RefDS(_9884);
    _33fatal(903, _9687, _9884, _9887);
    _9887 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_17625);
    DeRef(_data_ptr_17626);
    DeRef(_data_value_17627);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _9888 = (_key_location_17624 < 1);
    if (_9888 != 0) {
        goto L4; // [85] 103
    }
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _9890 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _9890 = 1;
    }
    _9891 = (_key_location_17624 > _9890);
    _9890 = NOVALUE;
    if (_9891 == 0)
    {
        DeRef(_9891);
        _9891 = NOVALUE;
        goto L5; // [99] 124
    }
    else{
        DeRef(_9891);
        _9891 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_17625);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17624;
    ((int *)_2)[2] = _table_name_17625;
    _9892 = MAKE_SEQ(_1);
    RefDS(_9809);
    RefDS(_9884);
    _33fatal(905, _9809, _9884, _9892);
    _9892 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_17625);
    DeRef(_data_ptr_17626);
    DeRef(_data_value_17627);
    DeRef(_9888);
    _9888 = NOVALUE;
    return -1;
L5: 

    /** 	io:seek(current_db, key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    _9893 = (int)*(((s1_ptr)_2)->base + _key_location_17624);
    Ref(_9893);
    DeRef(_pos_inlined_seek_at_133_17647);
    _pos_inlined_seek_at_133_17647 = _9893;
    _9893 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_133_17647);
    DeRef(_seek_1__tmp_at136_17649);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _pos_inlined_seek_at_133_17647;
    _seek_1__tmp_at136_17649 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_136_17648 = machine(19, _seek_1__tmp_at136_17649);
    DeRef(_pos_inlined_seek_at_133_17647);
    _pos_inlined_seek_at_133_17647 = NOVALUE;
    DeRef(_seek_1__tmp_at136_17649);
    _seek_1__tmp_at136_17649 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_33vLastErrors_15581)){
            _9894 = SEQ_PTR(_33vLastErrors_15581)->length;
    }
    else {
        _9894 = 1;
    }
    if (_9894 <= 0)
    goto L6; // [157] 166
    DeRef(_table_name_17625);
    DeRef(_data_ptr_17626);
    DeRef(_data_value_17627);
    DeRef(_9888);
    _9888 = NOVALUE;
    return -1;
L6: 

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_17626;
    _data_ptr_17626 = _33get4();
    DeRef(_0);

    /** 	io:seek(current_db, data_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_data_ptr_17626);
    DeRef(_seek_1__tmp_at174_17656);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _33current_db_15557;
    ((int *)_2)[2] = _data_ptr_17626;
    _seek_1__tmp_at174_17656 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_174_17655 = machine(19, _seek_1__tmp_at174_17656);
    DeRef(_seek_1__tmp_at174_17656);
    _seek_1__tmp_at174_17656 = NOVALUE;

    /** 	data_value = decompress(0)*/
    _0 = _data_value_17627;
    _data_value_17627 = _33decompress(0);
    DeRef(_0);

    /** 	return data_value*/
    DeRef(_table_name_17625);
    DeRef(_data_ptr_17626);
    DeRef(_9888);
    _9888 = NOVALUE;
    return _data_value_17627;
    ;
}


int _33db_fetch_record(int _key_17660, int _table_name_17661)
{
    int _pos_17662 = NOVALUE;
    int _9900 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pos = db_find_key(key, table_name)*/
    Ref(_key_17660);
    Ref(_table_name_17661);
    _pos_17662 = _33db_find_key(_key_17660, _table_name_17661);
    if (!IS_ATOM_INT(_pos_17662)) {
        _1 = (long)(DBL_PTR(_pos_17662)->dbl);
        if (UNIQUE(DBL_PTR(_pos_17662)) && (DBL_PTR(_pos_17662)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_17662);
        _pos_17662 = _1;
    }

    /** 	if pos > 0 then*/
    if (_pos_17662 <= 0)
    goto L1; // [14] 32

    /** 		return db_record_data(pos, table_name)*/
    Ref(_table_name_17661);
    _9900 = _33db_record_data(_pos_17662, _table_name_17661);
    DeRef(_key_17660);
    DeRef(_table_name_17661);
    return _9900;
    goto L2; // [29] 39
L1: 

    /** 		return pos*/
    DeRef(_key_17660);
    DeRef(_table_name_17661);
    DeRef(_9900);
    _9900 = NOVALUE;
    return _pos_17662;
L2: 
    ;
}


int _33db_record_key(int _key_location_17670, int _table_name_17671)
{
    int _9915 = NOVALUE;
    int _9914 = NOVALUE;
    int _9913 = NOVALUE;
    int _9912 = NOVALUE;
    int _9911 = NOVALUE;
    int _9909 = NOVALUE;
    int _9908 = NOVALUE;
    int _9906 = NOVALUE;
    int _9903 = NOVALUE;
    int _9901 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_17670)) {
        _1 = (long)(DBL_PTR(_key_location_17670)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_17670)) && (DBL_PTR(_key_location_17670)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_17670);
        _key_location_17670 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_17671 == _33current_table_name_15559)
    _9901 = 1;
    else if (IS_ATOM_INT(_table_name_17671) && IS_ATOM_INT(_33current_table_name_15559))
    _9901 = 0;
    else
    _9901 = (compare(_table_name_17671, _33current_table_name_15559) == 0);
    if (_9901 != 0)
    goto L1; // [13] 50
    _9901 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_17671);
    _9903 = _33db_select_table(_table_name_17671);
    if (binary_op_a(EQUALS, _9903, 0)){
        DeRef(_9903);
        _9903 = NOVALUE;
        goto L2; // [24] 49
    }
    DeRef(_9903);
    _9903 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_17671);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17670;
    ((int *)_2)[2] = _table_name_17671;
    _9906 = MAKE_SEQ(_1);
    RefDS(_9683);
    RefDS(_9905);
    _33fatal(903, _9683, _9905, _9906);
    _9906 = NOVALUE;

    /** 			return -1*/
    DeRef(_table_name_17671);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _33current_table_pos_15558, -1)){
        goto L3; // [54] 79
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_17671);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17670;
    ((int *)_2)[2] = _table_name_17671;
    _9908 = MAKE_SEQ(_1);
    RefDS(_9687);
    RefDS(_9905);
    _33fatal(903, _9687, _9905, _9908);
    _9908 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_17671);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _9909 = (_key_location_17670 < 1);
    if (_9909 != 0) {
        goto L4; // [85] 103
    }
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _9911 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _9911 = 1;
    }
    _9912 = (_key_location_17670 > _9911);
    _9911 = NOVALUE;
    if (_9912 == 0)
    {
        DeRef(_9912);
        _9912 = NOVALUE;
        goto L5; // [99] 124
    }
    else{
        DeRef(_9912);
        _9912 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_17671);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_17670;
    ((int *)_2)[2] = _table_name_17671;
    _9913 = MAKE_SEQ(_1);
    RefDS(_9809);
    RefDS(_9905);
    _33fatal(905, _9809, _9905, _9913);
    _9913 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_17671);
    DeRef(_9909);
    _9909 = NOVALUE;
    return -1;
L5: 

    /** 	return key_value(key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    _9914 = (int)*(((s1_ptr)_2)->base + _key_location_17670);
    Ref(_9914);
    _9915 = _33key_value(_9914);
    _9914 = NOVALUE;
    DeRef(_table_name_17671);
    DeRef(_9909);
    _9909 = NOVALUE;
    return _9915;
    ;
}


int _33db_compress()
{
    int _index_17693 = NOVALUE;
    int _chunk_size_17694 = NOVALUE;
    int _nrecs_17695 = NOVALUE;
    int _r_17696 = NOVALUE;
    int _fn_17697 = NOVALUE;
    int _new_path_17698 = NOVALUE;
    int _table_list_17699 = NOVALUE;
    int _record_17700 = NOVALUE;
    int _chunk_17701 = NOVALUE;
    int _temp_path_17709 = NOVALUE;
    int _9961 = NOVALUE;
    int _9957 = NOVALUE;
    int _9956 = NOVALUE;
    int _9955 = NOVALUE;
    int _9954 = NOVALUE;
    int _9953 = NOVALUE;
    int _9952 = NOVALUE;
    int _9950 = NOVALUE;
    int _9945 = NOVALUE;
    int _9944 = NOVALUE;
    int _9943 = NOVALUE;
    int _9940 = NOVALUE;
    int _9936 = NOVALUE;
    int _9933 = NOVALUE;
    int _9931 = NOVALUE;
    int _9928 = NOVALUE;
    int _9925 = NOVALUE;
    int _9920 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_33current_db_15557 != -1)
    goto L1; // [5] 26

    /** 		fatal(NO_DATABASE, "no current database", "db_compress", {})*/
    RefDS(_9917);
    RefDS(_9918);
    RefDS(_5);
    _33fatal(901, _9917, _9918, _5);

    /** 		return -1*/
    DeRef(_new_path_17698);
    DeRef(_table_list_17699);
    DeRef(_record_17700);
    DeRef(_chunk_17701);
    DeRef(_temp_path_17709);
    return -1;
L1: 

    /** 	index = eu:find(current_db, db_file_nums)*/
    _index_17693 = find_from(_33current_db_15557, _33db_file_nums_15561, 1);

    /** 	new_path = text:trim(db_names[index])*/
    _2 = (int)SEQ_PTR(_33db_names_15560);
    _9920 = (int)*(((s1_ptr)_2)->base + _index_17693);
    Ref(_9920);
    RefDS(_2936);
    _0 = _new_path_17698;
    _new_path_17698 = _16trim(_9920, _2936, 0);
    DeRef(_0);
    _9920 = NOVALUE;

    /** 	db_close()*/
    _33db_close();

    /** 	fn = -1*/
    _fn_17697 = -1;

    /** 	sequence temp_path = filesys:temp_file()*/
    RefDS(_5);
    RefDS(_5);
    RefDS(_6029);
    _0 = _temp_path_17709;
    _temp_path_17709 = _8temp_file(_5, _5, _6029, 0);
    DeRef(_0);

    /** 	fn = open( temp_path, "r" )*/
    _fn_17697 = EOpen(_temp_path_17709, _4069, 0);

    /** 	if fn != -1 then*/
    if (_fn_17697 == -1)
    goto L2; // [88] 101

    /** 		return DB_EXISTS_ALREADY -- you better delete some temp files*/
    DeRefDS(_new_path_17698);
    DeRef(_table_list_17699);
    DeRef(_record_17700);
    DeRef(_chunk_17701);
    DeRefDS(_temp_path_17709);
    return -2;
L2: 

    /** 	filesys:move_file( new_path, temp_path )*/
    RefDS(_new_path_17698);
    RefDS(_temp_path_17709);
    _9925 = _8move_file(_new_path_17698, _temp_path_17709, 0);

    /** 	index = db_create(new_path, DB_LOCK_NO)*/
    RefDS(_new_path_17698);
    _index_17693 = _33db_create(_new_path_17698, 0, 5, 5);
    if (!IS_ATOM_INT(_index_17693)) {
        _1 = (long)(DBL_PTR(_index_17693)->dbl);
        if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_17693);
        _index_17693 = _1;
    }

    /** 	if index != DB_OK then*/
    if (_index_17693 == 0)
    goto L3; // [128] 147

    /** 		filesys:move_file( temp_path, new_path )*/
    RefDS(_temp_path_17709);
    RefDS(_new_path_17698);
    _9928 = _8move_file(_temp_path_17709, _new_path_17698, 0);

    /** 		return index*/
    DeRefDS(_new_path_17698);
    DeRef(_table_list_17699);
    DeRef(_record_17700);
    DeRef(_chunk_17701);
    DeRefDS(_temp_path_17709);
    DeRef(_9925);
    _9925 = NOVALUE;
    DeRef(_9928);
    _9928 = NOVALUE;
    return _index_17693;
L3: 

    /** 	index = db_open(temp_path, DB_LOCK_NO)*/
    RefDS(_temp_path_17709);
    _index_17693 = _33db_open(_temp_path_17709, 0);
    if (!IS_ATOM_INT(_index_17693)) {
        _1 = (long)(DBL_PTR(_index_17693)->dbl);
        if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_17693);
        _index_17693 = _1;
    }

    /** 	table_list = db_table_list()*/
    _0 = _table_list_17699;
    _table_list_17699 = _33db_table_list();
    DeRef(_0);

    /** 	for i = 1 to length(table_list) do*/
    if (IS_SEQUENCE(_table_list_17699)){
            _9931 = SEQ_PTR(_table_list_17699)->length;
    }
    else {
        _9931 = 1;
    }
    {
        int _i_17722;
        _i_17722 = 1;
L4: 
        if (_i_17722 > _9931){
            goto L5; // [172] 476
        }

        /** 		index = db_select(new_path)*/
        RefDS(_new_path_17698);
        _index_17693 = _33db_select(_new_path_17698, -1);
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 		index = db_create_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_17699);
        _9933 = (int)*(((s1_ptr)_2)->base + _i_17722);
        Ref(_9933);
        _index_17693 = _33db_create_table(_9933, 50);
        _9933 = NOVALUE;
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 		index = db_select(temp_path)*/
        RefDS(_temp_path_17709);
        _index_17693 = _33db_select(_temp_path_17709, -1);
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 		index = db_select_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_17699);
        _9936 = (int)*(((s1_ptr)_2)->base + _i_17722);
        Ref(_9936);
        _index_17693 = _33db_select_table(_9936);
        _9936 = NOVALUE;
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 		nrecs = db_table_size()*/
        RefDS(_33current_table_name_15559);
        _nrecs_17695 = _33db_table_size(_33current_table_name_15559);
        if (!IS_ATOM_INT(_nrecs_17695)) {
            _1 = (long)(DBL_PTR(_nrecs_17695)->dbl);
            if (UNIQUE(DBL_PTR(_nrecs_17695)) && (DBL_PTR(_nrecs_17695)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_nrecs_17695);
            _nrecs_17695 = _1;
        }

        /** 		r = 1*/
        _r_17696 = 1;

        /** 		while r <= nrecs do*/
L6: 
        if (_r_17696 > _nrecs_17695)
        goto L7; // [254] 469

        /** 			chunk_size = nrecs - r + 1*/
        _9940 = _nrecs_17695 - _r_17696;
        if ((long)((unsigned long)_9940 +(unsigned long) HIGH_BITS) >= 0){
            _9940 = NewDouble((double)_9940);
        }
        if (IS_ATOM_INT(_9940)) {
            _chunk_size_17694 = _9940 + 1;
        }
        else
        { // coercing _chunk_size_17694 to an integer 1
            _chunk_size_17694 = 1+(long)(DBL_PTR(_9940)->dbl);
            if( !IS_ATOM_INT(_chunk_size_17694) ){
                _chunk_size_17694 = (object)DBL_PTR(_chunk_size_17694)->dbl;
            }
        }
        DeRef(_9940);
        _9940 = NOVALUE;

        /** 			if chunk_size > 20 then*/
        if (_chunk_size_17694 <= 20)
        goto L8; // [272] 284

        /** 				chunk_size = 20  -- copy up to 20 records at a time*/
        _chunk_size_17694 = 20;
L8: 

        /** 			chunk = {}*/
        RefDS(_5);
        DeRef(_chunk_17701);
        _chunk_17701 = _5;

        /** 			for j = 1 to chunk_size do*/
        _9943 = _chunk_size_17694;
        {
            int _j_17738;
            _j_17738 = 1;
L9: 
            if (_j_17738 > _9943){
                goto LA; // [296] 344
            }

            /** 				record = {db_record_key(r), db_record_data(r)}*/
            RefDS(_33current_table_name_15559);
            _9944 = _33db_record_key(_r_17696, _33current_table_name_15559);
            RefDS(_33current_table_name_15559);
            _9945 = _33db_record_data(_r_17696, _33current_table_name_15559);
            DeRef(_record_17700);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _9944;
            ((int *)_2)[2] = _9945;
            _record_17700 = MAKE_SEQ(_1);
            _9945 = NOVALUE;
            _9944 = NOVALUE;

            /** 				r += 1*/
            _r_17696 = _r_17696 + 1;

            /** 				chunk = append(chunk, record)*/
            RefDS(_record_17700);
            Append(&_chunk_17701, _chunk_17701, _record_17700);

            /** 			end for*/
            _j_17738 = _j_17738 + 1;
            goto L9; // [339] 303
LA: 
            ;
        }

        /** 			index = db_select(new_path)*/
        RefDS(_new_path_17698);
        _index_17693 = _33db_select(_new_path_17698, -1);
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 			index = db_select_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_17699);
        _9950 = (int)*(((s1_ptr)_2)->base + _i_17722);
        Ref(_9950);
        _index_17693 = _33db_select_table(_9950);
        _9950 = NOVALUE;
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 			for j = 1 to chunk_size do*/
        _9952 = _chunk_size_17694;
        {
            int _j_17749;
            _j_17749 = 1;
LB: 
            if (_j_17749 > _9952){
                goto LC; // [374] 439
            }

            /** 				if db_insert(chunk[j][1], chunk[j][2]) != DB_OK then*/
            _2 = (int)SEQ_PTR(_chunk_17701);
            _9953 = (int)*(((s1_ptr)_2)->base + _j_17749);
            _2 = (int)SEQ_PTR(_9953);
            _9954 = (int)*(((s1_ptr)_2)->base + 1);
            _9953 = NOVALUE;
            _2 = (int)SEQ_PTR(_chunk_17701);
            _9955 = (int)*(((s1_ptr)_2)->base + _j_17749);
            _2 = (int)SEQ_PTR(_9955);
            _9956 = (int)*(((s1_ptr)_2)->base + 2);
            _9955 = NOVALUE;
            Ref(_9954);
            Ref(_9956);
            RefDS(_33current_table_name_15559);
            _9957 = _33db_insert(_9954, _9956, _33current_table_name_15559);
            _9954 = NOVALUE;
            _9956 = NOVALUE;
            if (binary_op_a(EQUALS, _9957, 0)){
                DeRef(_9957);
                _9957 = NOVALUE;
                goto LD; // [409] 432
            }
            DeRef(_9957);
            _9957 = NOVALUE;

            /** 					fatal(INSERT_FAILED, "couldn't insert into new database", "db_compress", {})*/
            RefDS(_9959);
            RefDS(_9918);
            RefDS(_5);
            _33fatal(906, _9959, _9918, _5);

            /** 					return DB_FATAL_FAIL*/
            DeRef(_new_path_17698);
            DeRef(_table_list_17699);
            DeRef(_record_17700);
            DeRefDS(_chunk_17701);
            DeRef(_temp_path_17709);
            DeRef(_9925);
            _9925 = NOVALUE;
            DeRef(_9928);
            _9928 = NOVALUE;
            return -404;
LD: 

            /** 			end for*/
            _j_17749 = _j_17749 + 1;
            goto LB; // [434] 381
LC: 
            ;
        }

        /** 			index = db_select(temp_path)*/
        RefDS(_temp_path_17709);
        _index_17693 = _33db_select(_temp_path_17709, -1);
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 			index = db_select_table(table_list[i])*/
        _2 = (int)SEQ_PTR(_table_list_17699);
        _9961 = (int)*(((s1_ptr)_2)->base + _i_17722);
        Ref(_9961);
        _index_17693 = _33db_select_table(_9961);
        _9961 = NOVALUE;
        if (!IS_ATOM_INT(_index_17693)) {
            _1 = (long)(DBL_PTR(_index_17693)->dbl);
            if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_index_17693);
            _index_17693 = _1;
        }

        /** 		end while*/
        goto L6; // [466] 254
L7: 

        /** 	end for*/
        _i_17722 = _i_17722 + 1;
        goto L4; // [471] 179
L5: 
        ;
    }

    /** 	db_close()*/
    _33db_close();

    /** 	index = db_select(new_path)*/
    RefDS(_new_path_17698);
    _index_17693 = _33db_select(_new_path_17698, -1);
    if (!IS_ATOM_INT(_index_17693)) {
        _1 = (long)(DBL_PTR(_index_17693)->dbl);
        if (UNIQUE(DBL_PTR(_index_17693)) && (DBL_PTR(_index_17693)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_17693);
        _index_17693 = _1;
    }

    /** 	return DB_OK*/
    DeRefDS(_new_path_17698);
    DeRef(_table_list_17699);
    DeRef(_record_17700);
    DeRef(_chunk_17701);
    DeRef(_temp_path_17709);
    DeRef(_9925);
    _9925 = NOVALUE;
    DeRef(_9928);
    _9928 = NOVALUE;
    return 0;
    ;
}


int _33db_current()
{
    int _index_17765 = NOVALUE;
    int _9966 = NOVALUE;
    int _0, _1, _2;
    

    /** 	index = find (current_db, db_file_nums)*/
    _index_17765 = find_from(_33current_db_15557, _33db_file_nums_15561, 1);

    /** 	if index != 0 then*/
    if (_index_17765 == 0)
    goto L1; // [16] 35

    /** 		return db_names [index]*/
    _2 = (int)SEQ_PTR(_33db_names_15560);
    _9966 = (int)*(((s1_ptr)_2)->base + _index_17765);
    Ref(_9966);
    return _9966;
    goto L2; // [32] 42
L1: 

    /** 		return ""*/
    RefDS(_5);
    _9966 = NOVALUE;
    return _5;
L2: 
    ;
}


void _33db_cache_clear()
{
    int _0, _1, _2;
    

    /** 	cache_index = {}*/
    RefDS(_5);
    DeRef(_33cache_index_15566);
    _33cache_index_15566 = _5;

    /** 	key_cache = {}*/
    RefDS(_5);
    DeRef(_33key_cache_15565);
    _33key_cache_15565 = _5;

    /** end procedure*/
    return;
    ;
}


int _33db_set_caching(int _new_setting_17775)
{
    int _lOldVal_17776 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lOldVal = caching_option*/
    _lOldVal_17776 = _33caching_option_15567;

    /** 	caching_option = (new_setting != 0)*/
    if (IS_ATOM_INT(_new_setting_17775)) {
        _33caching_option_15567 = (_new_setting_17775 != 0);
    }
    else {
        _33caching_option_15567 = (DBL_PTR(_new_setting_17775)->dbl != (double)0);
    }

    /** 	if caching_option = 0 then*/
    if (_33caching_option_15567 != 0)
    goto L1; // [24] 50

    /** 		db_cache_clear()*/

    /** 	cache_index = {}*/
    RefDS(_5);
    DeRef(_33cache_index_15566);
    _33cache_index_15566 = _5;

    /** 	key_cache = {}*/
    RefDS(_5);
    DeRef(_33key_cache_15565);
    _33key_cache_15565 = _5;

    /** end procedure*/
    goto L2; // [46] 49
L2: 
L1: 

    /** 	return lOldVal*/
    DeRef(_new_setting_17775);
    return _lOldVal_17776;
    ;
}


void _33db_replace_recid(int _recid_17783, int _data_17784)
{
    int _old_size_17785 = NOVALUE;
    int _new_size_17786 = NOVALUE;
    int _data_ptr_17787 = NOVALUE;
    int _data_string_17788 = NOVALUE;
    int _put4_1__tmp_at113_17808 = NOVALUE;
    int _10013 = NOVALUE;
    int _10012 = NOVALUE;
    int _10011 = NOVALUE;
    int _10010 = NOVALUE;
    int _10009 = NOVALUE;
    int _9981 = NOVALUE;
    int _9979 = NOVALUE;
    int _9978 = NOVALUE;
    int _9977 = NOVALUE;
    int _9976 = NOVALUE;
    int _9975 = NOVALUE;
    int _9971 = NOVALUE;
    int _9970 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recid_17783)) {
        _1 = (long)(DBL_PTR(_recid_17783)->dbl);
        if (UNIQUE(DBL_PTR(_recid_17783)) && (DBL_PTR(_recid_17783)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recid_17783);
        _recid_17783 = _1;
    }

    /** 	seek(current_db, recid)*/
    _10013 = _15seek(_33current_db_15557, _recid_17783);
    DeRef(_10013);
    _10013 = NOVALUE;

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_17787;
    _data_ptr_17787 = _33get4();
    DeRef(_0);

    /** 	seek(current_db, data_ptr-4)*/
    if (IS_ATOM_INT(_data_ptr_17787)) {
        _9970 = _data_ptr_17787 - 4;
        if ((long)((unsigned long)_9970 +(unsigned long) HIGH_BITS) >= 0){
            _9970 = NewDouble((double)_9970);
        }
    }
    else {
        _9970 = NewDouble(DBL_PTR(_data_ptr_17787)->dbl - (double)4);
    }
    _10012 = _15seek(_33current_db_15557, _9970);
    _9970 = NOVALUE;
    DeRef(_10012);
    _10012 = NOVALUE;

    /** 	old_size = get4()-4*/
    _9971 = _33get4();
    DeRef(_old_size_17785);
    if (IS_ATOM_INT(_9971)) {
        _old_size_17785 = _9971 - 4;
        if ((long)((unsigned long)_old_size_17785 +(unsigned long) HIGH_BITS) >= 0){
            _old_size_17785 = NewDouble((double)_old_size_17785);
        }
    }
    else {
        _old_size_17785 = binary_op(MINUS, _9971, 4);
    }
    DeRef(_9971);
    _9971 = NOVALUE;

    /** 	data_string = compress(data)*/
    Ref(_data_17784);
    _0 = _data_string_17788;
    _data_string_17788 = _33compress(_data_17784);
    DeRef(_0);

    /** 	new_size = length(data_string)*/
    if (IS_SEQUENCE(_data_string_17788)){
            _new_size_17786 = SEQ_PTR(_data_string_17788)->length;
    }
    else {
        _new_size_17786 = 1;
    }

    /** 	if new_size <= old_size and*/
    if (IS_ATOM_INT(_old_size_17785)) {
        _9975 = (_new_size_17786 <= _old_size_17785);
    }
    else {
        _9975 = ((double)_new_size_17786 <= DBL_PTR(_old_size_17785)->dbl);
    }
    if (_9975 == 0) {
        goto L1; // [64] 94
    }
    if (IS_ATOM_INT(_old_size_17785)) {
        _9977 = _old_size_17785 - 16;
        if ((long)((unsigned long)_9977 +(unsigned long) HIGH_BITS) >= 0){
            _9977 = NewDouble((double)_9977);
        }
    }
    else {
        _9977 = NewDouble(DBL_PTR(_old_size_17785)->dbl - (double)16);
    }
    if (IS_ATOM_INT(_9977)) {
        _9978 = (_new_size_17786 >= _9977);
    }
    else {
        _9978 = ((double)_new_size_17786 >= DBL_PTR(_9977)->dbl);
    }
    DeRef(_9977);
    _9977 = NOVALUE;
    if (_9978 == 0)
    {
        DeRef(_9978);
        _9978 = NOVALUE;
        goto L1; // [77] 94
    }
    else{
        DeRef(_9978);
        _9978 = NOVALUE;
    }

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_17787);
    _10011 = _15seek(_33current_db_15557, _data_ptr_17787);
    DeRef(_10011);
    _10011 = NOVALUE;
    goto L2; // [91] 170
L1: 

    /** 		db_free(data_ptr)*/
    Ref(_data_ptr_17787);
    _33db_free(_data_ptr_17787);

    /** 		data_ptr = db_allocate(new_size + 8)*/
    _9979 = _new_size_17786 + 8;
    if ((long)((unsigned long)_9979 + (unsigned long)HIGH_BITS) >= 0) 
    _9979 = NewDouble((double)_9979);
    _0 = _data_ptr_17787;
    _data_ptr_17787 = _33db_allocate(_9979);
    DeRef(_0);
    _9979 = NOVALUE;

    /** 		seek(current_db, recid)*/
    _10010 = _15seek(_33current_db_15557, _recid_17783);
    DeRef(_10010);
    _10010 = NOVALUE;

    /** 		put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_33mem0_15599)){
        poke4_addr = (unsigned long *)_33mem0_15599;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_33mem0_15599)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_17787)) {
        *poke4_addr = (unsigned long)_data_ptr_17787;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_17787)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at113_17808);
    _1 = (int)SEQ_PTR(_33memseq_15819);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at113_17808 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_33current_db_15557, _put4_1__tmp_at113_17808); // DJP 

    /** end procedure*/
    goto L3; // [143] 146
L3: 
    DeRefi(_put4_1__tmp_at113_17808);
    _put4_1__tmp_at113_17808 = NOVALUE;

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_17787);
    _10009 = _15seek(_33current_db_15557, _data_ptr_17787);
    DeRef(_10009);
    _10009 = NOVALUE;

    /** 		data_string &= repeat( 0, 8 )*/
    _9981 = Repeat(0, 8);
    Concat((object_ptr)&_data_string_17788, _data_string_17788, _9981);
    DeRefDS(_9981);
    _9981 = NOVALUE;
L2: 

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_33current_db_15557, _data_string_17788); // DJP 

    /** end procedure*/
    goto L4; // [181] 184
L4: 

    /** end procedure*/
    DeRef(_data_17784);
    DeRef(_old_size_17785);
    DeRef(_data_ptr_17787);
    DeRef(_data_string_17788);
    DeRef(_9975);
    _9975 = NOVALUE;
    return;
    ;
}


int _33db_record_recid(int _recid_17815)
{
    int _data_ptr_17816 = NOVALUE;
    int _data_value_17817 = NOVALUE;
    int _key_value_17818 = NOVALUE;
    int _10008 = NOVALUE;
    int _10007 = NOVALUE;
    int _9986 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recid_17815)) {
        _1 = (long)(DBL_PTR(_recid_17815)->dbl);
        if (UNIQUE(DBL_PTR(_recid_17815)) && (DBL_PTR(_recid_17815)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recid_17815);
        _recid_17815 = _1;
    }

    /** 	seek(current_db, recid)*/
    _10008 = _15seek(_33current_db_15557, _recid_17815);
    DeRef(_10008);
    _10008 = NOVALUE;

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_17816;
    _data_ptr_17816 = _33get4();
    DeRef(_0);

    /** 	key_value = decompress(0)*/
    _0 = _key_value_17818;
    _key_value_17818 = _33decompress(0);
    DeRef(_0);

    /** 	seek(current_db, data_ptr)*/
    Ref(_data_ptr_17816);
    _10007 = _15seek(_33current_db_15557, _data_ptr_17816);
    DeRef(_10007);
    _10007 = NOVALUE;

    /** 	data_value = decompress(0)*/
    _0 = _data_value_17817;
    _data_value_17817 = _33decompress(0);
    DeRef(_0);

    /** 	return {key_value, data_value}*/
    Ref(_data_value_17817);
    Ref(_key_value_17818);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_value_17818;
    ((int *)_2)[2] = _data_value_17817;
    _9986 = MAKE_SEQ(_1);
    DeRef(_data_ptr_17816);
    DeRef(_data_value_17817);
    DeRef(_key_value_17818);
    return _9986;
    ;
}


int _33db_get_recid(int _key_17827, int _table_name_17828)
{
    int _lo_17829 = NOVALUE;
    int _hi_17830 = NOVALUE;
    int _mid_17831 = NOVALUE;
    int _c_17832 = NOVALUE;
    int _10006 = NOVALUE;
    int _10000 = NOVALUE;
    int _9999 = NOVALUE;
    int _9997 = NOVALUE;
    int _9994 = NOVALUE;
    int _9992 = NOVALUE;
    int _9989 = NOVALUE;
    int _9987 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_17828 == _33current_table_name_15559)
    _9987 = 1;
    else if (IS_ATOM_INT(_table_name_17828) && IS_ATOM_INT(_33current_table_name_15559))
    _9987 = 0;
    else
    _9987 = (compare(_table_name_17828, _33current_table_name_15559) == 0);
    if (_9987 != 0)
    goto L1; // [9] 46
    _9987 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    Ref(_table_name_17828);
    _9989 = _33db_select_table(_table_name_17828);
    if (binary_op_a(EQUALS, _9989, 0)){
        DeRef(_9989);
        _9989 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_9989);
    _9989 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_get_recid", {key, table_name})*/
    Ref(_table_name_17828);
    Ref(_key_17827);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_17827;
    ((int *)_2)[2] = _table_name_17828;
    _9992 = MAKE_SEQ(_1);
    RefDS(_9683);
    RefDS(_9991);
    _33fatal(903, _9683, _9991, _9992);
    _9992 = NOVALUE;

    /** 			return 0*/
    DeRef(_key_17827);
    DeRef(_table_name_17828);
    return 0;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _33current_table_pos_15558, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_get_recid", {key, table_name})*/
    Ref(_table_name_17828);
    Ref(_key_17827);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_17827;
    ((int *)_2)[2] = _table_name_17828;
    _9994 = MAKE_SEQ(_1);
    RefDS(_9687);
    RefDS(_9991);
    _33fatal(903, _9687, _9991, _9994);
    _9994 = NOVALUE;

    /** 		return 0*/
    DeRef(_key_17827);
    DeRef(_table_name_17828);
    return 0;
L3: 

    /** 	lo = 1*/
    _lo_17829 = 1;

    /** 	hi = length(key_pointers)*/
    if (IS_SEQUENCE(_33key_pointers_15564)){
            _hi_17830 = SEQ_PTR(_33key_pointers_15564)->length;
    }
    else {
        _hi_17830 = 1;
    }

    /** 	mid = 1*/
    _mid_17831 = 1;

    /** 	c = 0*/
    _c_17832 = 0;

    /** 	while lo <= hi do*/
L4: 
    if (_lo_17829 > _hi_17830)
    goto L5; // [110] 198

    /** 		mid = floor((lo + hi) / 2)*/
    _9997 = _lo_17829 + _hi_17830;
    if ((long)((unsigned long)_9997 + (unsigned long)HIGH_BITS) >= 0) 
    _9997 = NewDouble((double)_9997);
    if (IS_ATOM_INT(_9997)) {
        _mid_17831 = _9997 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _9997, 2);
        _mid_17831 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_9997);
    _9997 = NOVALUE;
    if (!IS_ATOM_INT(_mid_17831)) {
        _1 = (long)(DBL_PTR(_mid_17831)->dbl);
        if (UNIQUE(DBL_PTR(_mid_17831)) && (DBL_PTR(_mid_17831)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_17831);
        _mid_17831 = _1;
    }

    /** 		c = eu:compare(key, key_value(key_pointers[mid]))*/
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    _9999 = (int)*(((s1_ptr)_2)->base + _mid_17831);
    Ref(_9999);
    _10000 = _33key_value(_9999);
    _9999 = NOVALUE;
    if (IS_ATOM_INT(_key_17827) && IS_ATOM_INT(_10000)){
        _c_17832 = (_key_17827 < _10000) ? -1 : (_key_17827 > _10000);
    }
    else{
        _c_17832 = compare(_key_17827, _10000);
    }
    DeRef(_10000);
    _10000 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_17832 >= 0)
    goto L6; // [148] 163

    /** 			hi = mid - 1*/
    _hi_17830 = _mid_17831 - 1;
    goto L4; // [160] 110
L6: 

    /** 		elsif c > 0 then*/
    if (_c_17832 <= 0)
    goto L7; // [165] 180

    /** 			lo = mid + 1*/
    _lo_17829 = _mid_17831 + 1;
    goto L4; // [177] 110
L7: 

    /** 			return key_pointers[mid]*/
    _2 = (int)SEQ_PTR(_33key_pointers_15564);
    _10006 = (int)*(((s1_ptr)_2)->base + _mid_17831);
    Ref(_10006);
    DeRef(_key_17827);
    DeRef(_table_name_17828);
    return _10006;

    /** 	end while*/
    goto L4; // [195] 110
L5: 

    /** 	return -1*/
    DeRef(_key_17827);
    DeRef(_table_name_17828);
    _10006 = NOVALUE;
    return -1;
    ;
}



// 0x21982E9B
