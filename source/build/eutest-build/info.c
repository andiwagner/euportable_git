// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _29version_node(int _full_11824)
{
    int _6692 = NOVALUE;
    int _6691 = NOVALUE;
    int _6690 = NOVALUE;
    int _6689 = NOVALUE;
    int _6688 = NOVALUE;
    int _6687 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or length(version_info[NODE]) < 12 then*/
    if (0 != 0) {
        goto L1; // [7] 31
    }
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6687 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6687)){
            _6688 = SEQ_PTR(_6687)->length;
    }
    else {
        _6688 = 1;
    }
    _6687 = NOVALUE;
    _6689 = (_6688 < 12);
    _6688 = NOVALUE;
    if (_6689 == 0)
    {
        DeRef(_6689);
        _6689 = NOVALUE;
        goto L2; // [27] 46
    }
    else{
        DeRef(_6689);
        _6689 = NOVALUE;
    }
L1: 

    /** 		return version_info[NODE]*/
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6690 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_6690);
    _6687 = NOVALUE;
    return _6690;
L2: 

    /** 	return version_info[NODE][1..12]*/
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6691 = (int)*(((s1_ptr)_2)->base + 5);
    rhs_slice_target = (object_ptr)&_6692;
    RHS_Slice(_6691, 1, 12);
    _6691 = NOVALUE;
    _6687 = NOVALUE;
    _6690 = NOVALUE;
    return _6692;
    ;
}


int _29version_date(int _full_11838)
{
    int _6701 = NOVALUE;
    int _6700 = NOVALUE;
    int _6699 = NOVALUE;
    int _6698 = NOVALUE;
    int _6697 = NOVALUE;
    int _6696 = NOVALUE;
    int _6694 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental or length(version_info[REVISION_DATE]) < 10 then*/
    if (_full_11838 != 0) {
        _6694 = 1;
        goto L1; // [7] 17
    }
    _6694 = (_29is_developmental_11788 != 0);
L1: 
    if (_6694 != 0) {
        goto L2; // [17] 41
    }
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6696 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_6696)){
            _6697 = SEQ_PTR(_6696)->length;
    }
    else {
        _6697 = 1;
    }
    _6696 = NOVALUE;
    _6698 = (_6697 < 10);
    _6697 = NOVALUE;
    if (_6698 == 0)
    {
        DeRef(_6698);
        _6698 = NOVALUE;
        goto L3; // [37] 56
    }
    else{
        DeRef(_6698);
        _6698 = NOVALUE;
    }
L2: 

    /** 		return version_info[REVISION_DATE]*/
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6699 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_6699);
    _6696 = NOVALUE;
    return _6699;
L3: 

    /** 	return version_info[REVISION_DATE][1..10]*/
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6700 = (int)*(((s1_ptr)_2)->base + 7);
    rhs_slice_target = (object_ptr)&_6701;
    RHS_Slice(_6700, 1, 10);
    _6700 = NOVALUE;
    _6696 = NOVALUE;
    _6699 = NOVALUE;
    return _6701;
    ;
}


int _29version_string(int _full_11853)
{
    int _version_revision_inlined_version_revision_at_51_11862 = NOVALUE;
    int _6721 = NOVALUE;
    int _6720 = NOVALUE;
    int _6719 = NOVALUE;
    int _6718 = NOVALUE;
    int _6717 = NOVALUE;
    int _6716 = NOVALUE;
    int _6715 = NOVALUE;
    int _6714 = NOVALUE;
    int _6712 = NOVALUE;
    int _6711 = NOVALUE;
    int _6710 = NOVALUE;
    int _6709 = NOVALUE;
    int _6708 = NOVALUE;
    int _6707 = NOVALUE;
    int _6706 = NOVALUE;
    int _6705 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental then*/
    if (0 != 0) {
        goto L1; // [7] 18
    }
    if (_29is_developmental_11788 == 0)
    {
        goto L2; // [14] 92
    }
    else{
    }
L1: 

    /** 		return sprintf("%d.%d.%d %s (%d:%s, %s)", {*/
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6705 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6706 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6707 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6708 = (int)*(((s1_ptr)_2)->base + 4);

    /** 	return version_info[REVISION]*/
    DeRef(_version_revision_inlined_version_revision_at_51_11862);
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _version_revision_inlined_version_revision_at_51_11862 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_version_revision_inlined_version_revision_at_51_11862);
    _6709 = _29version_node(0);
    _6710 = _29version_date(_full_11853);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6705);
    *((int *)(_2+4)) = _6705;
    Ref(_6706);
    *((int *)(_2+8)) = _6706;
    Ref(_6707);
    *((int *)(_2+12)) = _6707;
    Ref(_6708);
    *((int *)(_2+16)) = _6708;
    Ref(_version_revision_inlined_version_revision_at_51_11862);
    *((int *)(_2+20)) = _version_revision_inlined_version_revision_at_51_11862;
    *((int *)(_2+24)) = _6709;
    *((int *)(_2+28)) = _6710;
    _6711 = MAKE_SEQ(_1);
    _6710 = NOVALUE;
    _6709 = NOVALUE;
    _6708 = NOVALUE;
    _6707 = NOVALUE;
    _6706 = NOVALUE;
    _6705 = NOVALUE;
    _6712 = EPrintf(-9999999, _6704, _6711);
    DeRefDS(_6711);
    _6711 = NOVALUE;
    return _6712;
    goto L3; // [89] 152
L2: 

    /** 		return sprintf("%d.%d.%d %s (%s, %s)", {*/
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6714 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6715 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6716 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_29version_info_11786);
    _6717 = (int)*(((s1_ptr)_2)->base + 4);
    _6718 = _29version_node(0);
    _6719 = _29version_date(_full_11853);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6714);
    *((int *)(_2+4)) = _6714;
    Ref(_6715);
    *((int *)(_2+8)) = _6715;
    Ref(_6716);
    *((int *)(_2+12)) = _6716;
    Ref(_6717);
    *((int *)(_2+16)) = _6717;
    *((int *)(_2+20)) = _6718;
    *((int *)(_2+24)) = _6719;
    _6720 = MAKE_SEQ(_1);
    _6719 = NOVALUE;
    _6718 = NOVALUE;
    _6717 = NOVALUE;
    _6716 = NOVALUE;
    _6715 = NOVALUE;
    _6714 = NOVALUE;
    _6721 = EPrintf(-9999999, _6713, _6720);
    DeRefDS(_6720);
    _6720 = NOVALUE;
    DeRef(_6712);
    _6712 = NOVALUE;
    return _6721;
L3: 
    ;
}



// 0x0E880411
