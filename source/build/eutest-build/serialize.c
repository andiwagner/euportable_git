// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _21init_float80()
{
    int _code_5163 = NOVALUE;
    int _2654 = NOVALUE;
    int _2653 = NOVALUE;
    int _2651 = NOVALUE;
    int _0, _1, _2;
    

    /** 	f80 = allocate( 10 )*/
    _0 = _11allocate(10, 0);
    DeRef(_21f80_5152);
    _21f80_5152 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _21f64_5153, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _11allocate(8, 0);
    DeRef(_21f64_5153);
    _21f64_5153 = _0;
L1: 

    /** 	atom code*/

    /** 	code = machine:allocate_code(*/
    _1 = NewS1(25);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 12;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 85;
    *((int *)(_2+48)) = 8;
    *((int *)(_2+52)) = 219;
    *((int *)(_2+56)) = 42;
    *((int *)(_2+60)) = 221;
    *((int *)(_2+64)) = 93;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 221;
    *((int *)(_2+76)) = 69;
    *((int *)(_2+80)) = 248;
    *((int *)(_2+84)) = 221;
    *((int *)(_2+88)) = 24;
    *((int *)(_2+92)) = 201;
    *((int *)(_2+96)) = 195;
    *((int *)(_2+100)) = 0;
    _2651 = MAKE_SEQ(_1);
    _0 = _code_5163;
    _code_5163 = _11allocate_code(_2651, 1);
    DeRef(_0);
    _2651 = NOVALUE;

    /** 	F80_TO_ATOM = dll:define_c_proc( "", {'+', code}, { dll:C_POINTER, dll:C_POINTER } )*/
    Ref(_code_5163);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_5163;
    _2653 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _2654 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _10define_c_proc(_5, _2653, _2654);
    _21F80_TO_ATOM_5154 = _0;
    _2653 = NOVALUE;
    _2654 = NOVALUE;
    if (!IS_ATOM_INT(_21F80_TO_ATOM_5154)) {
        _1 = (long)(DBL_PTR(_21F80_TO_ATOM_5154)->dbl);
        if (UNIQUE(DBL_PTR(_21F80_TO_ATOM_5154)) && (DBL_PTR(_21F80_TO_ATOM_5154)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_21F80_TO_ATOM_5154);
        _21F80_TO_ATOM_5154 = _1;
    }

    /** end procedure*/
    DeRef(_code_5163);
    return;
    ;
}


int _21float80_to_atom(int _f_5185)
{
    int _2660 = NOVALUE;
    int _2659 = NOVALUE;
    int _2658 = NOVALUE;
    int _2657 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not f80 then*/
    if (IS_ATOM_INT(_21f80_5152)) {
        if (_21f80_5152 != 0){
            goto L1; // [7] 15
        }
    }
    else {
        if (DBL_PTR(_21f80_5152)->dbl != 0.0){
            goto L1; // [7] 15
        }
    }

    /** 		init_float80()*/
    _21init_float80();
L1: 

    /** 	poke( f80, f )*/
    if (IS_ATOM_INT(_21f80_5152)){
        poke_addr = (unsigned char *)_21f80_5152;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21f80_5152)->dbl);
    }
    _1 = (int)SEQ_PTR(_f_5185);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( F80_TO_ATOM, { f80, f64 } )*/
    Ref(_21f64_5153);
    Ref(_21f80_5152);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _21f80_5152;
    ((int *)_2)[2] = _21f64_5153;
    _2657 = MAKE_SEQ(_1);
    call_c(0, _21F80_TO_ATOM_5154, _2657);
    DeRefDS(_2657);
    _2657 = NOVALUE;

    /** 	return float64_to_atom( peek( { f64, 8 } ) )*/
    Ref(_21f64_5153);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _21f64_5153;
    ((int *)_2)[2] = 8;
    _2658 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_2658);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _2659 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_2658);
    _2658 = NOVALUE;
    _2660 = _17float64_to_atom(_2659);
    _2659 = NOVALUE;
    DeRefDS(_f_5185);
    return _2660;
    ;
}


void _21init_peek8s()
{
    int _code_5203 = NOVALUE;
    int _2670 = NOVALUE;
    int _2669 = NOVALUE;
    int _2667 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i64 = allocate( 8 )*/
    _0 = _11allocate(8, 0);
    DeRef(_21i64_5193);
    _21i64_5193 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _21f64_5153, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _11allocate(8, 0);
    DeRef(_21f64_5153);
    _21f64_5153 = _0;
L1: 

    /** 	atom code = machine:allocate_code(	{*/
    _1 = NewS1(30);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 8;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 80;
    *((int *)(_2+48)) = 4;
    *((int *)(_2+52)) = 139;
    *((int *)(_2+56)) = 0;
    *((int *)(_2+60)) = 137;
    *((int *)(_2+64)) = 69;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 137;
    *((int *)(_2+76)) = 85;
    *((int *)(_2+80)) = 252;
    *((int *)(_2+84)) = 223;
    *((int *)(_2+88)) = 109;
    *((int *)(_2+92)) = 248;
    *((int *)(_2+96)) = 139;
    *((int *)(_2+100)) = 69;
    *((int *)(_2+104)) = 12;
    *((int *)(_2+108)) = 221;
    *((int *)(_2+112)) = 24;
    *((int *)(_2+116)) = 201;
    *((int *)(_2+120)) = 195;
    _2667 = MAKE_SEQ(_1);
    _0 = _code_5203;
    _code_5203 = _11allocate_code(_2667, 1);
    DeRef(_0);
    _2667 = NOVALUE;

    /** 	PEEK8S = define_c_proc( {}, {'+', code}, { C_POINTER, C_POINTER } )*/
    Ref(_code_5203);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_5203;
    _2669 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _2670 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _10define_c_proc(_5, _2669, _2670);
    _21PEEK8S_5194 = _0;
    _2669 = NOVALUE;
    _2670 = NOVALUE;
    if (!IS_ATOM_INT(_21PEEK8S_5194)) {
        _1 = (long)(DBL_PTR(_21PEEK8S_5194)->dbl);
        if (UNIQUE(DBL_PTR(_21PEEK8S_5194)) && (DBL_PTR(_21PEEK8S_5194)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_21PEEK8S_5194);
        _21PEEK8S_5194 = _1;
    }

    /** end procedure*/
    DeRef(_code_5203);
    return;
    ;
}


int _21int64_to_atom(int _s_5217)
{
    int _2676 = NOVALUE;
    int _2675 = NOVALUE;
    int _2674 = NOVALUE;
    int _2673 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if i64 = 0 then*/
    if (binary_op_a(NOTEQ, _21i64_5193, 0)){
        goto L1; // [7] 16
    }

    /** 		init_peek8s()*/
    _21init_peek8s();
L1: 

    /** 	poke( i64, s )*/
    if (IS_ATOM_INT(_21i64_5193)){
        poke_addr = (unsigned char *)_21i64_5193;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21i64_5193)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_5217);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( PEEK8S, { i64, f64 } )*/
    Ref(_21f64_5153);
    Ref(_21i64_5193);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _21i64_5193;
    ((int *)_2)[2] = _21f64_5153;
    _2673 = MAKE_SEQ(_1);
    call_c(0, _21PEEK8S_5194, _2673);
    DeRefDS(_2673);
    _2673 = NOVALUE;

    /** 	return float64_to_atom( peek( f64 & 8 ) )*/
    Concat((object_ptr)&_2674, _21f64_5153, 8);
    _1 = (int)SEQ_PTR(_2674);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _2675 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_2674);
    _2674 = NOVALUE;
    _2676 = _17float64_to_atom(_2675);
    _2675 = NOVALUE;
    DeRefDS(_s_5217);
    return _2676;
    ;
}


int _21get4(int _fh_5227)
{
    int _2681 = NOVALUE;
    int _2680 = NOVALUE;
    int _2679 = NOVALUE;
    int _2678 = NOVALUE;
    int _2677 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_5227 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_5227, EF_READ);
        last_r_file_no = _fh_5227;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _2677 = getKBchar();
        }
        else
        _2677 = getc(last_r_file_ptr);
    }
    else
    _2677 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_21mem0_5144)){
        poke_addr = (unsigned char *)_21mem0_5144;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem0_5144)->dbl);
    }
    *poke_addr = (unsigned char)_2677;
    _2677 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_5227 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_5227, EF_READ);
        last_r_file_no = _fh_5227;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _2678 = getKBchar();
        }
        else
        _2678 = getc(last_r_file_ptr);
    }
    else
    _2678 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_21mem1_5145)){
        poke_addr = (unsigned char *)_21mem1_5145;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem1_5145)->dbl);
    }
    *poke_addr = (unsigned char)_2678;
    _2678 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_5227 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_5227, EF_READ);
        last_r_file_no = _fh_5227;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _2679 = getKBchar();
        }
        else
        _2679 = getc(last_r_file_ptr);
    }
    else
    _2679 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_21mem2_5146)){
        poke_addr = (unsigned char *)_21mem2_5146;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem2_5146)->dbl);
    }
    *poke_addr = (unsigned char)_2679;
    _2679 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_5227 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_5227, EF_READ);
        last_r_file_no = _fh_5227;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _2680 = getKBchar();
        }
        else
        _2680 = getc(last_r_file_ptr);
    }
    else
    _2680 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_21mem3_5147)){
        poke_addr = (unsigned char *)_21mem3_5147;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem3_5147)->dbl);
    }
    *poke_addr = (unsigned char)_2680;
    _2680 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_21mem0_5144)) {
        _2681 = *(unsigned long *)_21mem0_5144;
        if ((unsigned)_2681 > (unsigned)MAXINT)
        _2681 = NewDouble((double)(unsigned long)_2681);
    }
    else {
        _2681 = *(unsigned long *)(unsigned long)(DBL_PTR(_21mem0_5144)->dbl);
        if ((unsigned)_2681 > (unsigned)MAXINT)
        _2681 = NewDouble((double)(unsigned long)_2681);
    }
    return _2681;
    ;
}


int _21deserialize_file(int _fh_5235, int _c_5236)
{
    int _s_5237 = NOVALUE;
    int _len_5238 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_165_5274 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_162_5273 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_224_5287 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_221_5286 = NOVALUE;
    int _msg_inlined_crash_at_430_5332 = NOVALUE;
    int _2760 = NOVALUE;
    int _2759 = NOVALUE;
    int _2758 = NOVALUE;
    int _2757 = NOVALUE;
    int _2754 = NOVALUE;
    int _2751 = NOVALUE;
    int _2750 = NOVALUE;
    int _2749 = NOVALUE;
    int _2748 = NOVALUE;
    int _2747 = NOVALUE;
    int _2746 = NOVALUE;
    int _2745 = NOVALUE;
    int _2744 = NOVALUE;
    int _2743 = NOVALUE;
    int _2742 = NOVALUE;
    int _2741 = NOVALUE;
    int _2740 = NOVALUE;
    int _2738 = NOVALUE;
    int _2737 = NOVALUE;
    int _2736 = NOVALUE;
    int _2735 = NOVALUE;
    int _2734 = NOVALUE;
    int _2733 = NOVALUE;
    int _2732 = NOVALUE;
    int _2731 = NOVALUE;
    int _2730 = NOVALUE;
    int _2729 = NOVALUE;
    int _2727 = NOVALUE;
    int _2726 = NOVALUE;
    int _2725 = NOVALUE;
    int _2724 = NOVALUE;
    int _2723 = NOVALUE;
    int _2721 = NOVALUE;
    int _2717 = NOVALUE;
    int _2716 = NOVALUE;
    int _2715 = NOVALUE;
    int _2714 = NOVALUE;
    int _2713 = NOVALUE;
    int _2712 = NOVALUE;
    int _2711 = NOVALUE;
    int _2710 = NOVALUE;
    int _2709 = NOVALUE;
    int _2708 = NOVALUE;
    int _2707 = NOVALUE;
    int _2706 = NOVALUE;
    int _2705 = NOVALUE;
    int _2704 = NOVALUE;
    int _2703 = NOVALUE;
    int _2702 = NOVALUE;
    int _2701 = NOVALUE;
    int _2700 = NOVALUE;
    int _2699 = NOVALUE;
    int _2698 = NOVALUE;
    int _2696 = NOVALUE;
    int _2695 = NOVALUE;
    int _2694 = NOVALUE;
    int _2693 = NOVALUE;
    int _2692 = NOVALUE;
    int _2691 = NOVALUE;
    int _2690 = NOVALUE;
    int _2689 = NOVALUE;
    int _2688 = NOVALUE;
    int _2685 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_5236 != 0)
    goto L1; // [11] 40

    /** 		c = getc(fh)*/
    if (_fh_5235 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_5235, EF_READ);
        last_r_file_no = _fh_5235;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_5236 = getKBchar();
        }
        else
        _c_5236 = getc(last_r_file_ptr);
    }
    else
    _c_5236 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_5236 >= 249)
    goto L2; // [24] 39

    /** 			return c + MIN1B*/
    _2685 = _c_5236 + -9;
    DeRef(_s_5237);
    DeRef(_len_5238);
    return _2685;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_5236;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(fh) +*/
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2688 = getKBchar();
            }
            else
            _2688 = getc(last_r_file_ptr);
        }
        else
        _2688 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2689 = getKBchar();
            }
            else
            _2689 = getc(last_r_file_ptr);
        }
        else
        _2689 = getc(last_r_file_ptr);
        _2690 = 256 * _2689;
        _2689 = NOVALUE;
        _2691 = _2688 + _2690;
        _2688 = NOVALUE;
        _2690 = NOVALUE;
        _2692 = _2691 + _21MIN2B_5128;
        if ((long)((unsigned long)_2692 + (unsigned long)HIGH_BITS) >= 0) 
        _2692 = NewDouble((double)_2692);
        _2691 = NOVALUE;
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        return _2692;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(fh) +*/
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2693 = getKBchar();
            }
            else
            _2693 = getc(last_r_file_ptr);
        }
        else
        _2693 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2694 = getKBchar();
            }
            else
            _2694 = getc(last_r_file_ptr);
        }
        else
        _2694 = getc(last_r_file_ptr);
        _2695 = 256 * _2694;
        _2694 = NOVALUE;
        _2696 = _2693 + _2695;
        _2693 = NOVALUE;
        _2695 = NOVALUE;
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2698 = getKBchar();
            }
            else
            _2698 = getc(last_r_file_ptr);
        }
        else
        _2698 = getc(last_r_file_ptr);
        _2699 = 65536 * _2698;
        _2698 = NOVALUE;
        _2700 = _2696 + _2699;
        _2696 = NOVALUE;
        _2699 = NOVALUE;
        _2701 = _2700 + _21MIN3B_5134;
        if ((long)((unsigned long)_2701 + (unsigned long)HIGH_BITS) >= 0) 
        _2701 = NewDouble((double)_2701);
        _2700 = NOVALUE;
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        return _2701;

        /** 		case I4B then*/
        case 251:

        /** 			return get4(fh) + MIN4B*/
        _2702 = _21get4(_fh_5235);
        if (IS_ATOM_INT(_2702) && IS_ATOM_INT(_21MIN4B_5141)) {
            _2703 = _2702 + _21MIN4B_5141;
            if ((long)((unsigned long)_2703 + (unsigned long)HIGH_BITS) >= 0) 
            _2703 = NewDouble((double)_2703);
        }
        else {
            _2703 = binary_op(PLUS, _2702, _21MIN4B_5141);
        }
        DeRef(_2702);
        _2702 = NOVALUE;
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        DeRef(_2701);
        _2701 = NOVALUE;
        return _2703;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(fh), getc(fh),*/
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2704 = getKBchar();
            }
            else
            _2704 = getc(last_r_file_ptr);
        }
        else
        _2704 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2705 = getKBchar();
            }
            else
            _2705 = getc(last_r_file_ptr);
        }
        else
        _2705 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2706 = getKBchar();
            }
            else
            _2706 = getc(last_r_file_ptr);
        }
        else
        _2706 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2707 = getKBchar();
            }
            else
            _2707 = getc(last_r_file_ptr);
        }
        else
        _2707 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _2704;
        *((int *)(_2+8)) = _2705;
        *((int *)(_2+12)) = _2706;
        *((int *)(_2+16)) = _2707;
        _2708 = MAKE_SEQ(_1);
        _2707 = NOVALUE;
        _2706 = NOVALUE;
        _2705 = NOVALUE;
        _2704 = NOVALUE;
        DeRefi(_ieee32_inlined_float32_to_atom_at_162_5273);
        _ieee32_inlined_float32_to_atom_at_162_5273 = _2708;
        _2708 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_165_5274);
        _float32_to_atom_inlined_float32_to_atom_at_165_5274 = machine(49, _ieee32_inlined_float32_to_atom_at_162_5273);
        DeRefi(_ieee32_inlined_float32_to_atom_at_162_5273);
        _ieee32_inlined_float32_to_atom_at_162_5273 = NOVALUE;
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        DeRef(_2701);
        _2701 = NOVALUE;
        DeRef(_2703);
        _2703 = NOVALUE;
        return _float32_to_atom_inlined_float32_to_atom_at_165_5274;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(fh), getc(fh),*/
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2709 = getKBchar();
            }
            else
            _2709 = getc(last_r_file_ptr);
        }
        else
        _2709 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2710 = getKBchar();
            }
            else
            _2710 = getc(last_r_file_ptr);
        }
        else
        _2710 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2711 = getKBchar();
            }
            else
            _2711 = getc(last_r_file_ptr);
        }
        else
        _2711 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2712 = getKBchar();
            }
            else
            _2712 = getc(last_r_file_ptr);
        }
        else
        _2712 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2713 = getKBchar();
            }
            else
            _2713 = getc(last_r_file_ptr);
        }
        else
        _2713 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2714 = getKBchar();
            }
            else
            _2714 = getc(last_r_file_ptr);
        }
        else
        _2714 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2715 = getKBchar();
            }
            else
            _2715 = getc(last_r_file_ptr);
        }
        else
        _2715 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2716 = getKBchar();
            }
            else
            _2716 = getc(last_r_file_ptr);
        }
        else
        _2716 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _2709;
        *((int *)(_2+8)) = _2710;
        *((int *)(_2+12)) = _2711;
        *((int *)(_2+16)) = _2712;
        *((int *)(_2+20)) = _2713;
        *((int *)(_2+24)) = _2714;
        *((int *)(_2+28)) = _2715;
        *((int *)(_2+32)) = _2716;
        _2717 = MAKE_SEQ(_1);
        _2716 = NOVALUE;
        _2715 = NOVALUE;
        _2714 = NOVALUE;
        _2713 = NOVALUE;
        _2712 = NOVALUE;
        _2711 = NOVALUE;
        _2710 = NOVALUE;
        _2709 = NOVALUE;
        DeRefi(_ieee64_inlined_float64_to_atom_at_221_5286);
        _ieee64_inlined_float64_to_atom_at_221_5286 = _2717;
        _2717 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_224_5287);
        _float64_to_atom_inlined_float64_to_atom_at_224_5287 = machine(47, _ieee64_inlined_float64_to_atom_at_221_5286);
        DeRefi(_ieee64_inlined_float64_to_atom_at_221_5286);
        _ieee64_inlined_float64_to_atom_at_221_5286 = NOVALUE;
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        DeRef(_2701);
        _2701 = NOVALUE;
        DeRef(_2703);
        _2703 = NOVALUE;
        return _float64_to_atom_inlined_float64_to_atom_at_224_5287;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_5236 != 254)
        goto L3; // [246] 258

        /** 				len = getc(fh)*/
        DeRef(_len_5238);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_5238 = getKBchar();
            }
            else
            _len_5238 = getc(last_r_file_ptr);
        }
        else
        _len_5238 = getc(last_r_file_ptr);
        goto L4; // [255] 265
L3: 

        /** 				len = get4(fh)*/
        _0 = _len_5238;
        _len_5238 = _21get4(_fh_5235);
        DeRef(_0);
L4: 

        /** 			if len < 0  or not integer(len) then*/
        if (IS_ATOM_INT(_len_5238)) {
            _2721 = (_len_5238 < 0);
        }
        else {
            _2721 = (DBL_PTR(_len_5238)->dbl < (double)0);
        }
        if (_2721 != 0) {
            goto L5; // [273] 288
        }
        if (IS_ATOM_INT(_len_5238))
        _2723 = 1;
        else if (IS_ATOM_DBL(_len_5238))
        _2723 = IS_ATOM_INT(DoubleToInt(_len_5238));
        else
        _2723 = 0;
        _2724 = (_2723 == 0);
        _2723 = NOVALUE;
        if (_2724 == 0)
        {
            DeRef(_2724);
            _2724 = NOVALUE;
            goto L6; // [284] 295
        }
        else{
            DeRef(_2724);
            _2724 = NOVALUE;
        }
L5: 

        /** 				return 0*/
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        DeRef(_2701);
        _2701 = NOVALUE;
        DeRef(_2721);
        _2721 = NOVALUE;
        DeRef(_2703);
        _2703 = NOVALUE;
        return 0;
L6: 

        /** 			if c = S4B and len < 256 then*/
        _2725 = (_c_5236 == 255);
        if (_2725 == 0) {
            goto L7; // [301] 453
        }
        if (IS_ATOM_INT(_len_5238)) {
            _2727 = (_len_5238 < 256);
        }
        else {
            _2727 = (DBL_PTR(_len_5238)->dbl < (double)256);
        }
        if (_2727 == 0)
        {
            DeRef(_2727);
            _2727 = NOVALUE;
            goto L7; // [310] 453
        }
        else{
            DeRef(_2727);
            _2727 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (binary_op_a(NOTEQ, _len_5238, 0)){
            goto L8; // [315] 367
        }

        /** 					return int64_to_atom( {*/
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2729 = getKBchar();
            }
            else
            _2729 = getc(last_r_file_ptr);
        }
        else
        _2729 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2730 = getKBchar();
            }
            else
            _2730 = getc(last_r_file_ptr);
        }
        else
        _2730 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2731 = getKBchar();
            }
            else
            _2731 = getc(last_r_file_ptr);
        }
        else
        _2731 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2732 = getKBchar();
            }
            else
            _2732 = getc(last_r_file_ptr);
        }
        else
        _2732 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2733 = getKBchar();
            }
            else
            _2733 = getc(last_r_file_ptr);
        }
        else
        _2733 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2734 = getKBchar();
            }
            else
            _2734 = getc(last_r_file_ptr);
        }
        else
        _2734 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2735 = getKBchar();
            }
            else
            _2735 = getc(last_r_file_ptr);
        }
        else
        _2735 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2736 = getKBchar();
            }
            else
            _2736 = getc(last_r_file_ptr);
        }
        else
        _2736 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _2729;
        *((int *)(_2+8)) = _2730;
        *((int *)(_2+12)) = _2731;
        *((int *)(_2+16)) = _2732;
        *((int *)(_2+20)) = _2733;
        *((int *)(_2+24)) = _2734;
        *((int *)(_2+28)) = _2735;
        *((int *)(_2+32)) = _2736;
        _2737 = MAKE_SEQ(_1);
        _2736 = NOVALUE;
        _2735 = NOVALUE;
        _2734 = NOVALUE;
        _2733 = NOVALUE;
        _2732 = NOVALUE;
        _2731 = NOVALUE;
        _2730 = NOVALUE;
        _2729 = NOVALUE;
        _2738 = _21int64_to_atom(_2737);
        _2737 = NOVALUE;
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        DeRef(_2701);
        _2701 = NOVALUE;
        DeRef(_2721);
        _2721 = NOVALUE;
        DeRef(_2703);
        _2703 = NOVALUE;
        DeRef(_2725);
        _2725 = NOVALUE;
        return _2738;
        goto L9; // [364] 529
L8: 

        /** 				elsif len = F10B then*/
        if (binary_op_a(NOTEQ, _len_5238, 1)){
            goto LA; // [369] 429
        }

        /** 					return float80_to_atom(*/
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2740 = getKBchar();
            }
            else
            _2740 = getc(last_r_file_ptr);
        }
        else
        _2740 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2741 = getKBchar();
            }
            else
            _2741 = getc(last_r_file_ptr);
        }
        else
        _2741 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2742 = getKBchar();
            }
            else
            _2742 = getc(last_r_file_ptr);
        }
        else
        _2742 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2743 = getKBchar();
            }
            else
            _2743 = getc(last_r_file_ptr);
        }
        else
        _2743 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2744 = getKBchar();
            }
            else
            _2744 = getc(last_r_file_ptr);
        }
        else
        _2744 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2745 = getKBchar();
            }
            else
            _2745 = getc(last_r_file_ptr);
        }
        else
        _2745 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2746 = getKBchar();
            }
            else
            _2746 = getc(last_r_file_ptr);
        }
        else
        _2746 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2747 = getKBchar();
            }
            else
            _2747 = getc(last_r_file_ptr);
        }
        else
        _2747 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2748 = getKBchar();
            }
            else
            _2748 = getc(last_r_file_ptr);
        }
        else
        _2748 = getc(last_r_file_ptr);
        if (_fh_5235 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_5235, EF_READ);
            last_r_file_no = _fh_5235;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _2749 = getKBchar();
            }
            else
            _2749 = getc(last_r_file_ptr);
        }
        else
        _2749 = getc(last_r_file_ptr);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _2740;
        *((int *)(_2+8)) = _2741;
        *((int *)(_2+12)) = _2742;
        *((int *)(_2+16)) = _2743;
        *((int *)(_2+20)) = _2744;
        *((int *)(_2+24)) = _2745;
        *((int *)(_2+28)) = _2746;
        *((int *)(_2+32)) = _2747;
        *((int *)(_2+36)) = _2748;
        *((int *)(_2+40)) = _2749;
        _2750 = MAKE_SEQ(_1);
        _2749 = NOVALUE;
        _2748 = NOVALUE;
        _2747 = NOVALUE;
        _2746 = NOVALUE;
        _2745 = NOVALUE;
        _2744 = NOVALUE;
        _2743 = NOVALUE;
        _2742 = NOVALUE;
        _2741 = NOVALUE;
        _2740 = NOVALUE;
        _2751 = _21float80_to_atom(_2750);
        _2750 = NOVALUE;
        DeRef(_s_5237);
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        DeRef(_2701);
        _2701 = NOVALUE;
        DeRef(_2721);
        _2721 = NOVALUE;
        DeRef(_2703);
        _2703 = NOVALUE;
        DeRef(_2725);
        _2725 = NOVALUE;
        DeRef(_2738);
        _2738 = NOVALUE;
        return _2751;
        goto L9; // [426] 529
LA: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_430_5332);
        _msg_inlined_crash_at_430_5332 = EPrintf(-9999999, _2752, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_430_5332);

        /** end procedure*/
        goto LB; // [444] 447
LB: 
        DeRefi(_msg_inlined_crash_at_430_5332);
        _msg_inlined_crash_at_430_5332 = NOVALUE;
        goto L9; // [450] 529
L7: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_5237);
        _s_5237 = Repeat(0, _len_5238);

        /** 				for i = 1 to len do*/
        Ref(_len_5238);
        DeRef(_2754);
        _2754 = _len_5238;
        {
            int _i_5336;
            _i_5336 = 1;
LC: 
            if (binary_op_a(GREATER, _i_5336, _2754)){
                goto LD; // [464] 522
            }

            /** 					c = getc(fh)*/
            if (_fh_5235 != last_r_file_no) {
                last_r_file_ptr = which_file(_fh_5235, EF_READ);
                last_r_file_no = _fh_5235;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_5236 = getKBchar();
                }
                else
                _c_5236 = getc(last_r_file_ptr);
            }
            else
            _c_5236 = getc(last_r_file_ptr);

            /** 					if c < I2B then*/
            if (_c_5236 >= 249)
            goto LE; // [480] 497

            /** 						s[i] = c + MIN1B*/
            _2757 = _c_5236 + -9;
            _2 = (int)SEQ_PTR(_s_5237);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_5237 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_5336))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_5336)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_5336);
            _1 = *(int *)_2;
            *(int *)_2 = _2757;
            if( _1 != _2757 ){
                DeRef(_1);
            }
            _2757 = NOVALUE;
            goto LF; // [494] 515
LE: 

            /** 						s[i] = deserialize_file(fh, c)*/
            DeRef(_2758);
            _2758 = _fh_5235;
            DeRef(_2759);
            _2759 = _c_5236;
            _2760 = _21deserialize_file(_2758, _2759);
            _2758 = NOVALUE;
            _2759 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_5237);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_5237 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_5336))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_5336)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_5336);
            _1 = *(int *)_2;
            *(int *)_2 = _2760;
            if( _1 != _2760 ){
                DeRef(_1);
            }
            _2760 = NOVALUE;
LF: 

            /** 				end for*/
            _0 = _i_5336;
            if (IS_ATOM_INT(_i_5336)) {
                _i_5336 = _i_5336 + 1;
                if ((long)((unsigned long)_i_5336 +(unsigned long) HIGH_BITS) >= 0){
                    _i_5336 = NewDouble((double)_i_5336);
                }
            }
            else {
                _i_5336 = binary_op_a(PLUS, _i_5336, 1);
            }
            DeRef(_0);
            goto LC; // [517] 471
LD: 
            ;
            DeRef(_i_5336);
        }

        /** 				return s*/
        DeRef(_len_5238);
        DeRef(_2685);
        _2685 = NOVALUE;
        DeRef(_2692);
        _2692 = NOVALUE;
        DeRef(_2701);
        _2701 = NOVALUE;
        DeRef(_2721);
        _2721 = NOVALUE;
        DeRef(_2703);
        _2703 = NOVALUE;
        DeRef(_2725);
        _2725 = NOVALUE;
        DeRef(_2738);
        _2738 = NOVALUE;
        DeRef(_2751);
        _2751 = NOVALUE;
        return _s_5237;
L9: 
    ;}    ;
}


int _21getp4(int _sdata_5348, int _pos_5349)
{
    int _2769 = NOVALUE;
    int _2768 = NOVALUE;
    int _2767 = NOVALUE;
    int _2766 = NOVALUE;
    int _2765 = NOVALUE;
    int _2764 = NOVALUE;
    int _2763 = NOVALUE;
    int _2762 = NOVALUE;
    int _2761 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, sdata[pos+0])*/
    _2761 = _pos_5349 + 0;
    _2 = (int)SEQ_PTR(_sdata_5348);
    _2762 = (int)*(((s1_ptr)_2)->base + _2761);
    if (IS_ATOM_INT(_21mem0_5144)){
        poke_addr = (unsigned char *)_21mem0_5144;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem0_5144)->dbl);
    }
    if (IS_ATOM_INT(_2762)) {
        *poke_addr = (unsigned char)_2762;
    }
    else if (IS_ATOM(_2762)) {
        _1 = (signed char)DBL_PTR(_2762)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_2762);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _2762 = NOVALUE;

    /** 	poke(mem1, sdata[pos+1])*/
    _2763 = _pos_5349 + 1;
    _2 = (int)SEQ_PTR(_sdata_5348);
    _2764 = (int)*(((s1_ptr)_2)->base + _2763);
    if (IS_ATOM_INT(_21mem1_5145)){
        poke_addr = (unsigned char *)_21mem1_5145;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem1_5145)->dbl);
    }
    if (IS_ATOM_INT(_2764)) {
        *poke_addr = (unsigned char)_2764;
    }
    else if (IS_ATOM(_2764)) {
        _1 = (signed char)DBL_PTR(_2764)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_2764);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _2764 = NOVALUE;

    /** 	poke(mem2, sdata[pos+2])*/
    _2765 = _pos_5349 + 2;
    _2 = (int)SEQ_PTR(_sdata_5348);
    _2766 = (int)*(((s1_ptr)_2)->base + _2765);
    if (IS_ATOM_INT(_21mem2_5146)){
        poke_addr = (unsigned char *)_21mem2_5146;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem2_5146)->dbl);
    }
    if (IS_ATOM_INT(_2766)) {
        *poke_addr = (unsigned char)_2766;
    }
    else if (IS_ATOM(_2766)) {
        _1 = (signed char)DBL_PTR(_2766)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_2766);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _2766 = NOVALUE;

    /** 	poke(mem3, sdata[pos+3])*/
    _2767 = _pos_5349 + 3;
    _2 = (int)SEQ_PTR(_sdata_5348);
    _2768 = (int)*(((s1_ptr)_2)->base + _2767);
    if (IS_ATOM_INT(_21mem3_5147)){
        poke_addr = (unsigned char *)_21mem3_5147;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_21mem3_5147)->dbl);
    }
    if (IS_ATOM_INT(_2768)) {
        *poke_addr = (unsigned char)_2768;
    }
    else if (IS_ATOM(_2768)) {
        _1 = (signed char)DBL_PTR(_2768)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_2768);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _2768 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_21mem0_5144)) {
        _2769 = *(unsigned long *)_21mem0_5144;
        if ((unsigned)_2769 > (unsigned)MAXINT)
        _2769 = NewDouble((double)(unsigned long)_2769);
    }
    else {
        _2769 = *(unsigned long *)(unsigned long)(DBL_PTR(_21mem0_5144)->dbl);
        if ((unsigned)_2769 > (unsigned)MAXINT)
        _2769 = NewDouble((double)(unsigned long)_2769);
    }
    DeRefDS(_sdata_5348);
    _2761 = NOVALUE;
    _2763 = NOVALUE;
    _2765 = NOVALUE;
    _2767 = NOVALUE;
    return _2769;
    ;
}


int _21deserialize_object(int _sdata_5361, int _pos_5362, int _c_5363)
{
    int _s_5364 = NOVALUE;
    int _len_5365 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_238_5414 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_235_5413 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_341_5437 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_338_5436 = NOVALUE;
    int _msg_inlined_crash_at_509_5468 = NOVALUE;
    int _temp_5480 = NOVALUE;
    int _2863 = NOVALUE;
    int _2861 = NOVALUE;
    int _2859 = NOVALUE;
    int _2858 = NOVALUE;
    int _2857 = NOVALUE;
    int _2856 = NOVALUE;
    int _2852 = NOVALUE;
    int _2850 = NOVALUE;
    int _2849 = NOVALUE;
    int _2848 = NOVALUE;
    int _2847 = NOVALUE;
    int _2846 = NOVALUE;
    int _2844 = NOVALUE;
    int _2843 = NOVALUE;
    int _2842 = NOVALUE;
    int _2841 = NOVALUE;
    int _2840 = NOVALUE;
    int _2838 = NOVALUE;
    int _2837 = NOVALUE;
    int _2836 = NOVALUE;
    int _2830 = NOVALUE;
    int _2829 = NOVALUE;
    int _2828 = NOVALUE;
    int _2827 = NOVALUE;
    int _2826 = NOVALUE;
    int _2825 = NOVALUE;
    int _2824 = NOVALUE;
    int _2823 = NOVALUE;
    int _2822 = NOVALUE;
    int _2820 = NOVALUE;
    int _2819 = NOVALUE;
    int _2818 = NOVALUE;
    int _2817 = NOVALUE;
    int _2816 = NOVALUE;
    int _2815 = NOVALUE;
    int _2814 = NOVALUE;
    int _2813 = NOVALUE;
    int _2812 = NOVALUE;
    int _2811 = NOVALUE;
    int _2810 = NOVALUE;
    int _2809 = NOVALUE;
    int _2808 = NOVALUE;
    int _2807 = NOVALUE;
    int _2806 = NOVALUE;
    int _2805 = NOVALUE;
    int _2804 = NOVALUE;
    int _2803 = NOVALUE;
    int _2802 = NOVALUE;
    int _2801 = NOVALUE;
    int _2800 = NOVALUE;
    int _2799 = NOVALUE;
    int _2798 = NOVALUE;
    int _2797 = NOVALUE;
    int _2796 = NOVALUE;
    int _2795 = NOVALUE;
    int _2794 = NOVALUE;
    int _2793 = NOVALUE;
    int _2792 = NOVALUE;
    int _2791 = NOVALUE;
    int _2790 = NOVALUE;
    int _2789 = NOVALUE;
    int _2788 = NOVALUE;
    int _2787 = NOVALUE;
    int _2786 = NOVALUE;
    int _2785 = NOVALUE;
    int _2784 = NOVALUE;
    int _2783 = NOVALUE;
    int _2782 = NOVALUE;
    int _2781 = NOVALUE;
    int _2780 = NOVALUE;
    int _2779 = NOVALUE;
    int _2778 = NOVALUE;
    int _2775 = NOVALUE;
    int _2774 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_5363 != 0)
    goto L1; // [13] 55

    /** 		c = sdata[pos]*/
    _2 = (int)SEQ_PTR(_sdata_5361);
    _c_5363 = (int)*(((s1_ptr)_2)->base + _pos_5362);
    if (!IS_ATOM_INT(_c_5363))
    _c_5363 = (long)DBL_PTR(_c_5363)->dbl;

    /** 		pos += 1*/
    _pos_5362 = _pos_5362 + 1;

    /** 		if c < I2B then*/
    if (_c_5363 >= 249)
    goto L2; // [35] 54

    /** 			return {c + MIN1B, pos}*/
    _2774 = _c_5363 + -9;
    if ((long)((unsigned long)_2774 + (unsigned long)HIGH_BITS) >= 0) 
    _2774 = NewDouble((double)_2774);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2774;
    ((int *)_2)[2] = _pos_5362;
    _2775 = MAKE_SEQ(_1);
    _2774 = NOVALUE;
    DeRefDS(_sdata_5361);
    DeRef(_s_5364);
    return _2775;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_5363;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2778 = (int)*(((s1_ptr)_2)->base + _pos_5362);
        _2779 = _pos_5362 + 1;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2780 = (int)*(((s1_ptr)_2)->base + _2779);
        if (IS_ATOM_INT(_2780)) {
            if (_2780 <= INT15 && _2780 >= -INT15)
            _2781 = 256 * _2780;
            else
            _2781 = NewDouble(256 * (double)_2780);
        }
        else {
            _2781 = binary_op(MULTIPLY, 256, _2780);
        }
        _2780 = NOVALUE;
        if (IS_ATOM_INT(_2778) && IS_ATOM_INT(_2781)) {
            _2782 = _2778 + _2781;
            if ((long)((unsigned long)_2782 + (unsigned long)HIGH_BITS) >= 0) 
            _2782 = NewDouble((double)_2782);
        }
        else {
            _2782 = binary_op(PLUS, _2778, _2781);
        }
        _2778 = NOVALUE;
        DeRef(_2781);
        _2781 = NOVALUE;
        if (IS_ATOM_INT(_2782)) {
            _2783 = _2782 + _21MIN2B_5128;
            if ((long)((unsigned long)_2783 + (unsigned long)HIGH_BITS) >= 0) 
            _2783 = NewDouble((double)_2783);
        }
        else {
            _2783 = binary_op(PLUS, _2782, _21MIN2B_5128);
        }
        DeRef(_2782);
        _2782 = NOVALUE;
        _2784 = _pos_5362 + 2;
        if ((long)((unsigned long)_2784 + (unsigned long)HIGH_BITS) >= 0) 
        _2784 = NewDouble((double)_2784);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2783;
        ((int *)_2)[2] = _2784;
        _2785 = MAKE_SEQ(_1);
        _2784 = NOVALUE;
        _2783 = NOVALUE;
        DeRefDS(_sdata_5361);
        DeRef(_s_5364);
        DeRef(_2775);
        _2775 = NOVALUE;
        _2779 = NOVALUE;
        return _2785;

        /** 		case I3B then*/
        case 250:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2786 = (int)*(((s1_ptr)_2)->base + _pos_5362);
        _2787 = _pos_5362 + 1;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2788 = (int)*(((s1_ptr)_2)->base + _2787);
        if (IS_ATOM_INT(_2788)) {
            if (_2788 <= INT15 && _2788 >= -INT15)
            _2789 = 256 * _2788;
            else
            _2789 = NewDouble(256 * (double)_2788);
        }
        else {
            _2789 = binary_op(MULTIPLY, 256, _2788);
        }
        _2788 = NOVALUE;
        if (IS_ATOM_INT(_2786) && IS_ATOM_INT(_2789)) {
            _2790 = _2786 + _2789;
            if ((long)((unsigned long)_2790 + (unsigned long)HIGH_BITS) >= 0) 
            _2790 = NewDouble((double)_2790);
        }
        else {
            _2790 = binary_op(PLUS, _2786, _2789);
        }
        _2786 = NOVALUE;
        DeRef(_2789);
        _2789 = NOVALUE;
        _2791 = _pos_5362 + 2;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2792 = (int)*(((s1_ptr)_2)->base + _2791);
        if (IS_ATOM_INT(_2792)) {
            _2793 = NewDouble(65536 * (double)_2792);
        }
        else {
            _2793 = binary_op(MULTIPLY, 65536, _2792);
        }
        _2792 = NOVALUE;
        if (IS_ATOM_INT(_2790) && IS_ATOM_INT(_2793)) {
            _2794 = _2790 + _2793;
            if ((long)((unsigned long)_2794 + (unsigned long)HIGH_BITS) >= 0) 
            _2794 = NewDouble((double)_2794);
        }
        else {
            _2794 = binary_op(PLUS, _2790, _2793);
        }
        DeRef(_2790);
        _2790 = NOVALUE;
        DeRef(_2793);
        _2793 = NOVALUE;
        if (IS_ATOM_INT(_2794)) {
            _2795 = _2794 + _21MIN3B_5134;
            if ((long)((unsigned long)_2795 + (unsigned long)HIGH_BITS) >= 0) 
            _2795 = NewDouble((double)_2795);
        }
        else {
            _2795 = binary_op(PLUS, _2794, _21MIN3B_5134);
        }
        DeRef(_2794);
        _2794 = NOVALUE;
        _2796 = _pos_5362 + 3;
        if ((long)((unsigned long)_2796 + (unsigned long)HIGH_BITS) >= 0) 
        _2796 = NewDouble((double)_2796);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2795;
        ((int *)_2)[2] = _2796;
        _2797 = MAKE_SEQ(_1);
        _2796 = NOVALUE;
        _2795 = NOVALUE;
        DeRefDS(_sdata_5361);
        DeRef(_s_5364);
        DeRef(_2775);
        _2775 = NOVALUE;
        DeRef(_2785);
        _2785 = NOVALUE;
        DeRef(_2779);
        _2779 = NOVALUE;
        _2791 = NOVALUE;
        _2787 = NOVALUE;
        return _2797;

        /** 		case I4B then*/
        case 251:

        /** 			return {getp4(sdata, pos) + MIN4B, pos + 4}*/
        RefDS(_sdata_5361);
        _2798 = _21getp4(_sdata_5361, _pos_5362);
        if (IS_ATOM_INT(_2798) && IS_ATOM_INT(_21MIN4B_5141)) {
            _2799 = _2798 + _21MIN4B_5141;
            if ((long)((unsigned long)_2799 + (unsigned long)HIGH_BITS) >= 0) 
            _2799 = NewDouble((double)_2799);
        }
        else {
            _2799 = binary_op(PLUS, _2798, _21MIN4B_5141);
        }
        DeRef(_2798);
        _2798 = NOVALUE;
        _2800 = _pos_5362 + 4;
        if ((long)((unsigned long)_2800 + (unsigned long)HIGH_BITS) >= 0) 
        _2800 = NewDouble((double)_2800);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2799;
        ((int *)_2)[2] = _2800;
        _2801 = MAKE_SEQ(_1);
        _2800 = NOVALUE;
        _2799 = NOVALUE;
        DeRefDS(_sdata_5361);
        DeRef(_s_5364);
        DeRef(_2775);
        _2775 = NOVALUE;
        DeRef(_2785);
        _2785 = NOVALUE;
        DeRef(_2779);
        _2779 = NOVALUE;
        DeRef(_2791);
        _2791 = NOVALUE;
        DeRef(_2787);
        _2787 = NOVALUE;
        DeRef(_2797);
        _2797 = NOVALUE;
        return _2801;

        /** 		case F4B then*/
        case 252:

        /** 			return {convert:float32_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2802 = (int)*(((s1_ptr)_2)->base + _pos_5362);
        _2803 = _pos_5362 + 1;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2804 = (int)*(((s1_ptr)_2)->base + _2803);
        _2805 = _pos_5362 + 2;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2806 = (int)*(((s1_ptr)_2)->base + _2805);
        _2807 = _pos_5362 + 3;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2808 = (int)*(((s1_ptr)_2)->base + _2807);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_2802);
        *((int *)(_2+4)) = _2802;
        Ref(_2804);
        *((int *)(_2+8)) = _2804;
        Ref(_2806);
        *((int *)(_2+12)) = _2806;
        Ref(_2808);
        *((int *)(_2+16)) = _2808;
        _2809 = MAKE_SEQ(_1);
        _2808 = NOVALUE;
        _2806 = NOVALUE;
        _2804 = NOVALUE;
        _2802 = NOVALUE;
        DeRef(_ieee32_inlined_float32_to_atom_at_235_5413);
        _ieee32_inlined_float32_to_atom_at_235_5413 = _2809;
        _2809 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_238_5414);
        _float32_to_atom_inlined_float32_to_atom_at_238_5414 = machine(49, _ieee32_inlined_float32_to_atom_at_235_5413);
        DeRef(_ieee32_inlined_float32_to_atom_at_235_5413);
        _ieee32_inlined_float32_to_atom_at_235_5413 = NOVALUE;
        _2810 = _pos_5362 + 4;
        if ((long)((unsigned long)_2810 + (unsigned long)HIGH_BITS) >= 0) 
        _2810 = NewDouble((double)_2810);
        Ref(_float32_to_atom_inlined_float32_to_atom_at_238_5414);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _float32_to_atom_inlined_float32_to_atom_at_238_5414;
        ((int *)_2)[2] = _2810;
        _2811 = MAKE_SEQ(_1);
        _2810 = NOVALUE;
        DeRefDS(_sdata_5361);
        DeRef(_s_5364);
        DeRef(_2775);
        _2775 = NOVALUE;
        DeRef(_2785);
        _2785 = NOVALUE;
        DeRef(_2779);
        _2779 = NOVALUE;
        DeRef(_2791);
        _2791 = NOVALUE;
        DeRef(_2787);
        _2787 = NOVALUE;
        DeRef(_2797);
        _2797 = NOVALUE;
        DeRef(_2801);
        _2801 = NOVALUE;
        DeRef(_2803);
        _2803 = NOVALUE;
        DeRef(_2805);
        _2805 = NOVALUE;
        DeRef(_2807);
        _2807 = NOVALUE;
        return _2811;

        /** 		case F8B then*/
        case 253:

        /** 			return {convert:float64_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2812 = (int)*(((s1_ptr)_2)->base + _pos_5362);
        _2813 = _pos_5362 + 1;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2814 = (int)*(((s1_ptr)_2)->base + _2813);
        _2815 = _pos_5362 + 2;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2816 = (int)*(((s1_ptr)_2)->base + _2815);
        _2817 = _pos_5362 + 3;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2818 = (int)*(((s1_ptr)_2)->base + _2817);
        _2819 = _pos_5362 + 4;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2820 = (int)*(((s1_ptr)_2)->base + _2819);
        _2822 = _pos_5362 + 5;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2823 = (int)*(((s1_ptr)_2)->base + _2822);
        _2824 = _pos_5362 + 6;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2825 = (int)*(((s1_ptr)_2)->base + _2824);
        _2826 = _pos_5362 + 7;
        _2 = (int)SEQ_PTR(_sdata_5361);
        _2827 = (int)*(((s1_ptr)_2)->base + _2826);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_2812);
        *((int *)(_2+4)) = _2812;
        Ref(_2814);
        *((int *)(_2+8)) = _2814;
        Ref(_2816);
        *((int *)(_2+12)) = _2816;
        Ref(_2818);
        *((int *)(_2+16)) = _2818;
        Ref(_2820);
        *((int *)(_2+20)) = _2820;
        Ref(_2823);
        *((int *)(_2+24)) = _2823;
        Ref(_2825);
        *((int *)(_2+28)) = _2825;
        Ref(_2827);
        *((int *)(_2+32)) = _2827;
        _2828 = MAKE_SEQ(_1);
        _2827 = NOVALUE;
        _2825 = NOVALUE;
        _2823 = NOVALUE;
        _2820 = NOVALUE;
        _2818 = NOVALUE;
        _2816 = NOVALUE;
        _2814 = NOVALUE;
        _2812 = NOVALUE;
        DeRef(_ieee64_inlined_float64_to_atom_at_338_5436);
        _ieee64_inlined_float64_to_atom_at_338_5436 = _2828;
        _2828 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_341_5437);
        _float64_to_atom_inlined_float64_to_atom_at_341_5437 = machine(47, _ieee64_inlined_float64_to_atom_at_338_5436);
        DeRef(_ieee64_inlined_float64_to_atom_at_338_5436);
        _ieee64_inlined_float64_to_atom_at_338_5436 = NOVALUE;
        _2829 = _pos_5362 + 8;
        if ((long)((unsigned long)_2829 + (unsigned long)HIGH_BITS) >= 0) 
        _2829 = NewDouble((double)_2829);
        Ref(_float64_to_atom_inlined_float64_to_atom_at_341_5437);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _float64_to_atom_inlined_float64_to_atom_at_341_5437;
        ((int *)_2)[2] = _2829;
        _2830 = MAKE_SEQ(_1);
        _2829 = NOVALUE;
        DeRefDS(_sdata_5361);
        DeRef(_s_5364);
        DeRef(_2797);
        _2797 = NOVALUE;
        DeRef(_2822);
        _2822 = NOVALUE;
        DeRef(_2826);
        _2826 = NOVALUE;
        DeRef(_2803);
        _2803 = NOVALUE;
        DeRef(_2805);
        _2805 = NOVALUE;
        DeRef(_2819);
        _2819 = NOVALUE;
        DeRef(_2813);
        _2813 = NOVALUE;
        DeRef(_2815);
        _2815 = NOVALUE;
        DeRef(_2775);
        _2775 = NOVALUE;
        DeRef(_2785);
        _2785 = NOVALUE;
        DeRef(_2791);
        _2791 = NOVALUE;
        DeRef(_2787);
        _2787 = NOVALUE;
        DeRef(_2779);
        _2779 = NOVALUE;
        DeRef(_2801);
        _2801 = NOVALUE;
        DeRef(_2817);
        _2817 = NOVALUE;
        DeRef(_2807);
        _2807 = NOVALUE;
        DeRef(_2811);
        _2811 = NOVALUE;
        DeRef(_2824);
        _2824 = NOVALUE;
        return _2830;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_5363 != 254)
        goto L3; // [371] 394

        /** 				len = sdata[pos]*/
        _2 = (int)SEQ_PTR(_sdata_5361);
        _len_5365 = (int)*(((s1_ptr)_2)->base + _pos_5362);
        if (!IS_ATOM_INT(_len_5365))
        _len_5365 = (long)DBL_PTR(_len_5365)->dbl;

        /** 				pos += 1*/
        _pos_5362 = _pos_5362 + 1;
        goto L4; // [391] 414
L3: 

        /** 				len = getp4(sdata, pos)*/
        RefDS(_sdata_5361);
        _len_5365 = _21getp4(_sdata_5361, _pos_5362);
        if (!IS_ATOM_INT(_len_5365)) {
            _1 = (long)(DBL_PTR(_len_5365)->dbl);
            if (UNIQUE(DBL_PTR(_len_5365)) && (DBL_PTR(_len_5365)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_5365);
            _len_5365 = _1;
        }

        /** 				pos += 4*/
        _pos_5362 = _pos_5362 + 4;
L4: 

        /** 			if c = S4B and len < 256 then*/
        _2836 = (_c_5363 == 255);
        if (_2836 == 0) {
            goto L5; // [420] 532
        }
        _2838 = (_len_5365 < 256);
        if (_2838 == 0)
        {
            DeRef(_2838);
            _2838 = NOVALUE;
            goto L5; // [431] 532
        }
        else{
            DeRef(_2838);
            _2838 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (_len_5365 != 0)
        goto L6; // [438] 472

        /** 					return {int64_to_atom( sdata[pos..pos+7] ), pos + 8 }*/
        _2840 = _pos_5362 + 7;
        rhs_slice_target = (object_ptr)&_2841;
        RHS_Slice(_sdata_5361, _pos_5362, _2840);
        _2842 = _21int64_to_atom(_2841);
        _2841 = NOVALUE;
        _2843 = _pos_5362 + 8;
        if ((long)((unsigned long)_2843 + (unsigned long)HIGH_BITS) >= 0) 
        _2843 = NewDouble((double)_2843);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2842;
        ((int *)_2)[2] = _2843;
        _2844 = MAKE_SEQ(_1);
        _2843 = NOVALUE;
        _2842 = NOVALUE;
        DeRefDS(_sdata_5361);
        DeRef(_s_5364);
        DeRef(_2797);
        _2797 = NOVALUE;
        DeRef(_2822);
        _2822 = NOVALUE;
        DeRef(_2826);
        _2826 = NOVALUE;
        DeRef(_2803);
        _2803 = NOVALUE;
        DeRef(_2805);
        _2805 = NOVALUE;
        DeRef(_2819);
        _2819 = NOVALUE;
        DeRef(_2813);
        _2813 = NOVALUE;
        DeRef(_2830);
        _2830 = NOVALUE;
        DeRef(_2815);
        _2815 = NOVALUE;
        DeRef(_2836);
        _2836 = NOVALUE;
        DeRef(_2775);
        _2775 = NOVALUE;
        DeRef(_2785);
        _2785 = NOVALUE;
        DeRef(_2791);
        _2791 = NOVALUE;
        DeRef(_2787);
        _2787 = NOVALUE;
        DeRef(_2779);
        _2779 = NOVALUE;
        DeRef(_2801);
        _2801 = NOVALUE;
        DeRef(_2817);
        _2817 = NOVALUE;
        DeRef(_2807);
        _2807 = NOVALUE;
        _2840 = NOVALUE;
        DeRef(_2811);
        _2811 = NOVALUE;
        DeRef(_2824);
        _2824 = NOVALUE;
        return _2844;
        goto L7; // [469] 645
L6: 

        /** 				elsif len = F10B then*/
        if (_len_5365 != 1)
        goto L8; // [474] 508

        /** 					return { float80_to_atom( sdata[pos..pos+9] ), pos + 10 }*/
        _2846 = _pos_5362 + 9;
        rhs_slice_target = (object_ptr)&_2847;
        RHS_Slice(_sdata_5361, _pos_5362, _2846);
        _2848 = _21float80_to_atom(_2847);
        _2847 = NOVALUE;
        _2849 = _pos_5362 + 10;
        if ((long)((unsigned long)_2849 + (unsigned long)HIGH_BITS) >= 0) 
        _2849 = NewDouble((double)_2849);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2848;
        ((int *)_2)[2] = _2849;
        _2850 = MAKE_SEQ(_1);
        _2849 = NOVALUE;
        _2848 = NOVALUE;
        DeRefDS(_sdata_5361);
        DeRef(_s_5364);
        DeRef(_2797);
        _2797 = NOVALUE;
        DeRef(_2822);
        _2822 = NOVALUE;
        DeRef(_2826);
        _2826 = NOVALUE;
        DeRef(_2803);
        _2803 = NOVALUE;
        DeRef(_2805);
        _2805 = NOVALUE;
        DeRef(_2819);
        _2819 = NOVALUE;
        DeRef(_2813);
        _2813 = NOVALUE;
        DeRef(_2830);
        _2830 = NOVALUE;
        DeRef(_2815);
        _2815 = NOVALUE;
        DeRef(_2836);
        _2836 = NOVALUE;
        DeRef(_2775);
        _2775 = NOVALUE;
        DeRef(_2785);
        _2785 = NOVALUE;
        DeRef(_2791);
        _2791 = NOVALUE;
        DeRef(_2787);
        _2787 = NOVALUE;
        DeRef(_2844);
        _2844 = NOVALUE;
        DeRef(_2779);
        _2779 = NOVALUE;
        DeRef(_2801);
        _2801 = NOVALUE;
        DeRef(_2817);
        _2817 = NOVALUE;
        DeRef(_2807);
        _2807 = NOVALUE;
        DeRef(_2840);
        _2840 = NOVALUE;
        DeRef(_2811);
        _2811 = NOVALUE;
        DeRef(_2824);
        _2824 = NOVALUE;
        _2846 = NOVALUE;
        return _2850;
        goto L7; // [505] 645
L8: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_509_5468);
        _msg_inlined_crash_at_509_5468 = EPrintf(-9999999, _2752, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_509_5468);

        /** end procedure*/
        goto L9; // [523] 526
L9: 
        DeRefi(_msg_inlined_crash_at_509_5468);
        _msg_inlined_crash_at_509_5468 = NOVALUE;
        goto L7; // [529] 645
L5: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_5364);
        _s_5364 = Repeat(0, _len_5365);

        /** 				for i = 1 to len do*/
        _2852 = _len_5365;
        {
            int _i_5472;
            _i_5472 = 1;
LA: 
            if (_i_5472 > _2852){
                goto LB; // [545] 634
            }

            /** 					c = sdata[pos]*/
            _2 = (int)SEQ_PTR(_sdata_5361);
            _c_5363 = (int)*(((s1_ptr)_2)->base + _pos_5362);
            if (!IS_ATOM_INT(_c_5363))
            _c_5363 = (long)DBL_PTR(_c_5363)->dbl;

            /** 					pos += 1*/
            _pos_5362 = _pos_5362 + 1;

            /** 					if c < I2B then*/
            if (_c_5363 >= 249)
            goto LC; // [570] 587

            /** 						s[i] = c + MIN1B*/
            _2856 = _c_5363 + -9;
            if ((long)((unsigned long)_2856 + (unsigned long)HIGH_BITS) >= 0) 
            _2856 = NewDouble((double)_2856);
            _2 = (int)SEQ_PTR(_s_5364);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_5364 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_5472);
            _1 = *(int *)_2;
            *(int *)_2 = _2856;
            if( _1 != _2856 ){
                DeRef(_1);
            }
            _2856 = NOVALUE;
            goto LD; // [584] 627
LC: 

            /** 						sequence temp = deserialize_object(sdata, pos, c)*/
            RefDS(_sdata_5361);
            DeRef(_2857);
            _2857 = _sdata_5361;
            DeRef(_2858);
            _2858 = _pos_5362;
            DeRef(_2859);
            _2859 = _c_5363;
            _0 = _temp_5480;
            _temp_5480 = _21deserialize_object(_2857, _2858, _2859);
            DeRef(_0);
            _2857 = NOVALUE;
            _2858 = NOVALUE;
            _2859 = NOVALUE;

            /** 						s[i] = temp[1]*/
            _2 = (int)SEQ_PTR(_temp_5480);
            _2861 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_2861);
            _2 = (int)SEQ_PTR(_s_5364);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_5364 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_5472);
            _1 = *(int *)_2;
            *(int *)_2 = _2861;
            if( _1 != _2861 ){
                DeRef(_1);
            }
            _2861 = NOVALUE;

            /** 						pos = temp[2]*/
            _2 = (int)SEQ_PTR(_temp_5480);
            _pos_5362 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_pos_5362))
            _pos_5362 = (long)DBL_PTR(_pos_5362)->dbl;
            DeRefDS(_temp_5480);
            _temp_5480 = NOVALUE;
LD: 

            /** 				end for*/
            _i_5472 = _i_5472 + 1;
            goto LA; // [629] 552
LB: 
            ;
        }

        /** 				return {s, pos}*/
        RefDS(_s_5364);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _s_5364;
        ((int *)_2)[2] = _pos_5362;
        _2863 = MAKE_SEQ(_1);
        DeRefDS(_sdata_5361);
        DeRefDS(_s_5364);
        DeRef(_2797);
        _2797 = NOVALUE;
        DeRef(_2822);
        _2822 = NOVALUE;
        DeRef(_2826);
        _2826 = NOVALUE;
        DeRef(_2803);
        _2803 = NOVALUE;
        DeRef(_2805);
        _2805 = NOVALUE;
        DeRef(_2819);
        _2819 = NOVALUE;
        DeRef(_2813);
        _2813 = NOVALUE;
        DeRef(_2830);
        _2830 = NOVALUE;
        DeRef(_2815);
        _2815 = NOVALUE;
        DeRef(_2836);
        _2836 = NOVALUE;
        DeRef(_2775);
        _2775 = NOVALUE;
        DeRef(_2785);
        _2785 = NOVALUE;
        DeRef(_2791);
        _2791 = NOVALUE;
        DeRef(_2787);
        _2787 = NOVALUE;
        DeRef(_2844);
        _2844 = NOVALUE;
        DeRef(_2779);
        _2779 = NOVALUE;
        DeRef(_2801);
        _2801 = NOVALUE;
        DeRef(_2817);
        _2817 = NOVALUE;
        DeRef(_2807);
        _2807 = NOVALUE;
        DeRef(_2840);
        _2840 = NOVALUE;
        DeRef(_2811);
        _2811 = NOVALUE;
        DeRef(_2824);
        _2824 = NOVALUE;
        DeRef(_2846);
        _2846 = NOVALUE;
        DeRef(_2850);
        _2850 = NOVALUE;
        return _2863;
L7: 
    ;}    ;
}


int _21deserialize(int _sdata_5490, int _pos_5491)
{
    int _2867 = NOVALUE;
    int _2866 = NOVALUE;
    int _2865 = NOVALUE;
    int _2864 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(sdata) then*/
    _2864 = 1;
    if (_2864 == 0)
    {
        _2864 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _2864 = NOVALUE;
    }

    /** 		return deserialize_file(sdata, 0)*/
    _2865 = _21deserialize_file(_sdata_5490, 0);
    return _2865;
L1: 

    /** 	if atom(sdata) then*/
    _2866 = 1;
    if (_2866 == 0)
    {
        _2866 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _2866 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_2865);
    _2865 = NOVALUE;
    return 0;
L2: 

    /** 	return deserialize_object(sdata, pos, 0)*/
    _2867 = _21deserialize_object(_sdata_5490, _pos_5491, 0);
    DeRef(_2865);
    _2865 = NOVALUE;
    return _2867;
    ;
}


int _21serialize(int _x_5500)
{
    int _x4_5501 = NOVALUE;
    int _s_5502 = NOVALUE;
    int _atom_to_float32_inlined_atom_to_float32_at_192_5536 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_203_5539 = NOVALUE;
    int _atom_to_float64_inlined_atom_to_float64_at_229_5544 = NOVALUE;
    int _2906 = NOVALUE;
    int _2905 = NOVALUE;
    int _2904 = NOVALUE;
    int _2902 = NOVALUE;
    int _2901 = NOVALUE;
    int _2899 = NOVALUE;
    int _2897 = NOVALUE;
    int _2896 = NOVALUE;
    int _2895 = NOVALUE;
    int _2893 = NOVALUE;
    int _2892 = NOVALUE;
    int _2891 = NOVALUE;
    int _2890 = NOVALUE;
    int _2889 = NOVALUE;
    int _2888 = NOVALUE;
    int _2887 = NOVALUE;
    int _2886 = NOVALUE;
    int _2885 = NOVALUE;
    int _2883 = NOVALUE;
    int _2882 = NOVALUE;
    int _2881 = NOVALUE;
    int _2880 = NOVALUE;
    int _2879 = NOVALUE;
    int _2878 = NOVALUE;
    int _2876 = NOVALUE;
    int _2875 = NOVALUE;
    int _2874 = NOVALUE;
    int _2873 = NOVALUE;
    int _2872 = NOVALUE;
    int _2871 = NOVALUE;
    int _2870 = NOVALUE;
    int _2869 = NOVALUE;
    int _2868 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_5500))
    _2868 = 1;
    else if (IS_ATOM_DBL(_x_5500))
    _2868 = IS_ATOM_INT(DoubleToInt(_x_5500));
    else
    _2868 = 0;
    if (_2868 == 0)
    {
        _2868 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _2868 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_5500)) {
        _2869 = (_x_5500 >= -9);
    }
    else {
        _2869 = binary_op(GREATEREQ, _x_5500, -9);
    }
    if (IS_ATOM_INT(_2869)) {
        if (_2869 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_2869)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_5500)) {
        _2871 = (_x_5500 <= 239);
    }
    else {
        _2871 = binary_op(LESSEQ, _x_5500, 239);
    }
    if (_2871 == 0) {
        DeRef(_2871);
        _2871 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_2871) && DBL_PTR(_2871)->dbl == 0.0){
            DeRef(_2871);
            _2871 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_2871);
        _2871 = NOVALUE;
    }
    DeRef(_2871);
    _2871 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_5500)) {
        _2872 = _x_5500 - -9;
        if ((long)((unsigned long)_2872 +(unsigned long) HIGH_BITS) >= 0){
            _2872 = NewDouble((double)_2872);
        }
    }
    else {
        _2872 = binary_op(MINUS, _x_5500, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _2872;
    _2873 = MAKE_SEQ(_1);
    _2872 = NOVALUE;
    DeRef(_x_5500);
    DeRefi(_x4_5501);
    DeRef(_s_5502);
    DeRef(_2869);
    _2869 = NOVALUE;
    return _2873;
    goto L3; // [41] 328
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_5500)) {
        _2874 = (_x_5500 >= _21MIN2B_5128);
    }
    else {
        _2874 = binary_op(GREATEREQ, _x_5500, _21MIN2B_5128);
    }
    if (IS_ATOM_INT(_2874)) {
        if (_2874 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_2874)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_5500)) {
        _2876 = (_x_5500 <= 32767);
    }
    else {
        _2876 = binary_op(LESSEQ, _x_5500, 32767);
    }
    if (_2876 == 0) {
        DeRef(_2876);
        _2876 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_2876) && DBL_PTR(_2876)->dbl == 0.0){
            DeRef(_2876);
            _2876 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_2876);
        _2876 = NOVALUE;
    }
    DeRef(_2876);
    _2876 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_5500;
    if (IS_ATOM_INT(_x_5500)) {
        _x_5500 = _x_5500 - _21MIN2B_5128;
        if ((long)((unsigned long)_x_5500 +(unsigned long) HIGH_BITS) >= 0){
            _x_5500 = NewDouble((double)_x_5500);
        }
    }
    else {
        _x_5500 = binary_op(MINUS, _x_5500, _21MIN2B_5128);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_5500)) {
        {unsigned long tu;
             tu = (unsigned long)_x_5500 & (unsigned long)255;
             _2878 = MAKE_UINT(tu);
        }
    }
    else {
        _2878 = binary_op(AND_BITS, _x_5500, 255);
    }
    if (IS_ATOM_INT(_x_5500)) {
        if (256 > 0 && _x_5500 >= 0) {
            _2879 = _x_5500 / 256;
        }
        else {
            temp_dbl = floor((double)_x_5500 / (double)256);
            if (_x_5500 != MININT)
            _2879 = (long)temp_dbl;
            else
            _2879 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_5500, 256);
        _2879 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _2878;
    *((int *)(_2+12)) = _2879;
    _2880 = MAKE_SEQ(_1);
    _2879 = NOVALUE;
    _2878 = NOVALUE;
    DeRef(_x_5500);
    DeRefi(_x4_5501);
    DeRef(_s_5502);
    DeRef(_2869);
    _2869 = NOVALUE;
    DeRef(_2873);
    _2873 = NOVALUE;
    DeRef(_2874);
    _2874 = NOVALUE;
    return _2880;
    goto L3; // [94] 328
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_5500)) {
        _2881 = (_x_5500 >= _21MIN3B_5134);
    }
    else {
        _2881 = binary_op(GREATEREQ, _x_5500, _21MIN3B_5134);
    }
    if (IS_ATOM_INT(_2881)) {
        if (_2881 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_2881)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_5500)) {
        _2883 = (_x_5500 <= 8388607);
    }
    else {
        _2883 = binary_op(LESSEQ, _x_5500, 8388607);
    }
    if (_2883 == 0) {
        DeRef(_2883);
        _2883 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_2883) && DBL_PTR(_2883)->dbl == 0.0){
            DeRef(_2883);
            _2883 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_2883);
        _2883 = NOVALUE;
    }
    DeRef(_2883);
    _2883 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_5500;
    if (IS_ATOM_INT(_x_5500)) {
        _x_5500 = _x_5500 - _21MIN3B_5134;
        if ((long)((unsigned long)_x_5500 +(unsigned long) HIGH_BITS) >= 0){
            _x_5500 = NewDouble((double)_x_5500);
        }
    }
    else {
        _x_5500 = binary_op(MINUS, _x_5500, _21MIN3B_5134);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_5500)) {
        {unsigned long tu;
             tu = (unsigned long)_x_5500 & (unsigned long)255;
             _2885 = MAKE_UINT(tu);
        }
    }
    else {
        _2885 = binary_op(AND_BITS, _x_5500, 255);
    }
    if (IS_ATOM_INT(_x_5500)) {
        if (256 > 0 && _x_5500 >= 0) {
            _2886 = _x_5500 / 256;
        }
        else {
            temp_dbl = floor((double)_x_5500 / (double)256);
            if (_x_5500 != MININT)
            _2886 = (long)temp_dbl;
            else
            _2886 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_5500, 256);
        _2886 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_2886)) {
        {unsigned long tu;
             tu = (unsigned long)_2886 & (unsigned long)255;
             _2887 = MAKE_UINT(tu);
        }
    }
    else {
        _2887 = binary_op(AND_BITS, _2886, 255);
    }
    DeRef(_2886);
    _2886 = NOVALUE;
    if (IS_ATOM_INT(_x_5500)) {
        if (65536 > 0 && _x_5500 >= 0) {
            _2888 = _x_5500 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_5500 / (double)65536);
            if (_x_5500 != MININT)
            _2888 = (long)temp_dbl;
            else
            _2888 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_5500, 65536);
        _2888 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _2885;
    *((int *)(_2+12)) = _2887;
    *((int *)(_2+16)) = _2888;
    _2889 = MAKE_SEQ(_1);
    _2888 = NOVALUE;
    _2887 = NOVALUE;
    _2885 = NOVALUE;
    DeRef(_x_5500);
    DeRefi(_x4_5501);
    DeRef(_s_5502);
    DeRef(_2869);
    _2869 = NOVALUE;
    DeRef(_2873);
    _2873 = NOVALUE;
    DeRef(_2874);
    _2874 = NOVALUE;
    DeRef(_2880);
    _2880 = NOVALUE;
    DeRef(_2881);
    _2881 = NOVALUE;
    return _2889;
    goto L3; // [156] 328
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_5500) && IS_ATOM_INT(_21MIN4B_5141)) {
        _2890 = _x_5500 - _21MIN4B_5141;
        if ((long)((unsigned long)_2890 +(unsigned long) HIGH_BITS) >= 0){
            _2890 = NewDouble((double)_2890);
        }
    }
    else {
        _2890 = binary_op(MINUS, _x_5500, _21MIN4B_5141);
    }
    _2891 = _17int_to_bytes(_2890);
    _2890 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_2891)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_2891)) {
        Prepend(&_2892, _2891, 251);
    }
    else {
        Concat((object_ptr)&_2892, 251, _2891);
    }
    DeRef(_2891);
    _2891 = NOVALUE;
    DeRef(_x_5500);
    DeRefi(_x4_5501);
    DeRef(_s_5502);
    DeRef(_2869);
    _2869 = NOVALUE;
    DeRef(_2873);
    _2873 = NOVALUE;
    DeRef(_2874);
    _2874 = NOVALUE;
    DeRef(_2880);
    _2880 = NOVALUE;
    DeRef(_2881);
    _2881 = NOVALUE;
    DeRef(_2889);
    _2889 = NOVALUE;
    return _2892;
    goto L3; // [180] 328
L1: 

    /** 	elsif atom(x) then*/
    _2893 = IS_ATOM(_x_5500);
    if (_2893 == 0)
    {
        _2893 = NOVALUE;
        goto L6; // [188] 249
    }
    else{
        _2893 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/

    /** 	return machine_func(M_A_TO_F32, a)*/
    DeRefi(_x4_5501);
    _x4_5501 = machine(48, _x_5500);

    /** 		if x = convert:float32_to_atom(x4) then*/

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    DeRef(_float32_to_atom_inlined_float32_to_atom_at_203_5539);
    _float32_to_atom_inlined_float32_to_atom_at_203_5539 = machine(49, _x4_5501);
    if (binary_op_a(NOTEQ, _x_5500, _float32_to_atom_inlined_float32_to_atom_at_203_5539)){
        goto L7; // [211] 228
    }

    /** 			return F4B & x4*/
    Prepend(&_2895, _x4_5501, 252);
    DeRef(_x_5500);
    DeRefDSi(_x4_5501);
    DeRef(_s_5502);
    DeRef(_2869);
    _2869 = NOVALUE;
    DeRef(_2873);
    _2873 = NOVALUE;
    DeRef(_2874);
    _2874 = NOVALUE;
    DeRef(_2880);
    _2880 = NOVALUE;
    DeRef(_2881);
    _2881 = NOVALUE;
    DeRef(_2889);
    _2889 = NOVALUE;
    DeRef(_2892);
    _2892 = NOVALUE;
    return _2895;
    goto L3; // [225] 328
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/

    /** 	return machine_func(M_A_TO_F64, a)*/
    DeRefi(_atom_to_float64_inlined_atom_to_float64_at_229_5544);
    _atom_to_float64_inlined_atom_to_float64_at_229_5544 = machine(46, _x_5500);
    Prepend(&_2896, _atom_to_float64_inlined_atom_to_float64_at_229_5544, 253);
    DeRef(_x_5500);
    DeRefi(_x4_5501);
    DeRef(_s_5502);
    DeRef(_2869);
    _2869 = NOVALUE;
    DeRef(_2873);
    _2873 = NOVALUE;
    DeRef(_2874);
    _2874 = NOVALUE;
    DeRef(_2880);
    _2880 = NOVALUE;
    DeRef(_2881);
    _2881 = NOVALUE;
    DeRef(_2889);
    _2889 = NOVALUE;
    DeRef(_2892);
    _2892 = NOVALUE;
    DeRef(_2895);
    _2895 = NOVALUE;
    return _2896;
    goto L3; // [246] 328
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_5500)){
            _2897 = SEQ_PTR(_x_5500)->length;
    }
    else {
        _2897 = 1;
    }
    if (_2897 > 255)
    goto L8; // [254] 270

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_5500)){
            _2899 = SEQ_PTR(_x_5500)->length;
    }
    else {
        _2899 = 1;
    }
    DeRef(_s_5502);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _2899;
    _s_5502 = MAKE_SEQ(_1);
    _2899 = NOVALUE;
    goto L9; // [267] 284
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_5500)){
            _2901 = SEQ_PTR(_x_5500)->length;
    }
    else {
        _2901 = 1;
    }
    _2902 = _17int_to_bytes(_2901);
    _2901 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_2902)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_2902)) {
        Prepend(&_s_5502, _2902, 255);
    }
    else {
        Concat((object_ptr)&_s_5502, 255, _2902);
    }
    DeRef(_2902);
    _2902 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_5500)){
            _2904 = SEQ_PTR(_x_5500)->length;
    }
    else {
        _2904 = 1;
    }
    {
        int _i_5557;
        _i_5557 = 1;
LA: 
        if (_i_5557 > _2904){
            goto LB; // [289] 319
        }

        /** 			s &= serialize(x[i])*/
        _2 = (int)SEQ_PTR(_x_5500);
        _2905 = (int)*(((s1_ptr)_2)->base + _i_5557);
        Ref(_2905);
        _2906 = _21serialize(_2905);
        _2905 = NOVALUE;
        if (IS_SEQUENCE(_s_5502) && IS_ATOM(_2906)) {
            Ref(_2906);
            Append(&_s_5502, _s_5502, _2906);
        }
        else if (IS_ATOM(_s_5502) && IS_SEQUENCE(_2906)) {
        }
        else {
            Concat((object_ptr)&_s_5502, _s_5502, _2906);
        }
        DeRef(_2906);
        _2906 = NOVALUE;

        /** 		end for*/
        _i_5557 = _i_5557 + 1;
        goto LA; // [314] 296
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_5500);
    DeRefi(_x4_5501);
    DeRef(_2869);
    _2869 = NOVALUE;
    DeRef(_2873);
    _2873 = NOVALUE;
    DeRef(_2874);
    _2874 = NOVALUE;
    DeRef(_2880);
    _2880 = NOVALUE;
    DeRef(_2881);
    _2881 = NOVALUE;
    DeRef(_2889);
    _2889 = NOVALUE;
    DeRef(_2892);
    _2892 = NOVALUE;
    DeRef(_2895);
    _2895 = NOVALUE;
    DeRef(_2896);
    _2896 = NOVALUE;
    return _s_5502;
L3: 
    ;
}



// 0x4CF08E86
