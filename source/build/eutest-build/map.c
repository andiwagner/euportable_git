// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _25map(int _obj_p_11957)
{
    int _m__11961 = NOVALUE;
    int _6808 = NOVALUE;
    int _6807 = NOVALUE;
    int _6806 = NOVALUE;
    int _6805 = NOVALUE;
    int _6804 = NOVALUE;
    int _6803 = NOVALUE;
    int _6802 = NOVALUE;
    int _6801 = NOVALUE;
    int _6800 = NOVALUE;
    int _6799 = NOVALUE;
    int _6797 = NOVALUE;
    int _6796 = NOVALUE;
    int _6795 = NOVALUE;
    int _6794 = NOVALUE;
    int _6792 = NOVALUE;
    int _6791 = NOVALUE;
    int _6790 = NOVALUE;
    int _6789 = NOVALUE;
    int _6787 = NOVALUE;
    int _6786 = NOVALUE;
    int _6785 = NOVALUE;
    int _6784 = NOVALUE;
    int _6783 = NOVALUE;
    int _6782 = NOVALUE;
    int _6781 = NOVALUE;
    int _6780 = NOVALUE;
    int _6779 = NOVALUE;
    int _6778 = NOVALUE;
    int _6776 = NOVALUE;
    int _6774 = NOVALUE;
    int _6773 = NOVALUE;
    int _6771 = NOVALUE;
    int _6769 = NOVALUE;
    int _6768 = NOVALUE;
    int _6766 = NOVALUE;
    int _6765 = NOVALUE;
    int _6763 = NOVALUE;
    int _6761 = NOVALUE;
    int _6759 = NOVALUE;
    int _6756 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not eumem:valid(obj_p, "") then return 0 end if*/
    Ref(_obj_p_11957);
    RefDS(_5);
    _6756 = _26valid(_obj_p_11957, _5);
    if (IS_ATOM_INT(_6756)) {
        if (_6756 != 0){
            DeRef(_6756);
            _6756 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    else {
        if (DBL_PTR(_6756)->dbl != 0.0){
            DeRef(_6756);
            _6756 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    DeRef(_6756);
    _6756 = NOVALUE;
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L1: 

    /** 	object m_*/

    /** 	m_ = eumem:ram_space[obj_p]*/
    DeRef(_m__11961);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_obj_p_11957)){
        _m__11961 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_obj_p_11957)->dbl));
    }
    else{
        _m__11961 = (int)*(((s1_ptr)_2)->base + _obj_p_11957);
    }
    Ref(_m__11961);

    /** 	if not sequence(m_) then return 0 end if*/
    _6759 = IS_SEQUENCE(_m__11961);
    if (_6759 != 0)
    goto L2; // [31] 39
    _6759 = NOVALUE;
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L2: 

    /** 	if length(m_) < 6 then return 0 end if*/
    if (IS_SEQUENCE(_m__11961)){
            _6761 = SEQ_PTR(_m__11961)->length;
    }
    else {
        _6761 = 1;
    }
    if (_6761 >= 6)
    goto L3; // [44] 53
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L3: 

    /** 	if length(m_) > 7 then return 0 end if*/
    if (IS_SEQUENCE(_m__11961)){
            _6763 = SEQ_PTR(_m__11961)->length;
    }
    else {
        _6763 = 1;
    }
    if (_6763 <= 7)
    goto L4; // [58] 67
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L4: 

    /** 	if not equal(m_[TYPE_TAG], type_is_map) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6765 = (int)*(((s1_ptr)_2)->base + 1);
    if (_6765 == _25type_is_map_11930)
    _6766 = 1;
    else if (IS_ATOM_INT(_6765) && IS_ATOM_INT(_25type_is_map_11930))
    _6766 = 0;
    else
    _6766 = (compare(_6765, _25type_is_map_11930) == 0);
    _6765 = NOVALUE;
    if (_6766 != 0)
    goto L5; // [79] 87
    _6766 = NOVALUE;
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L5: 

    /** 	if not integer(m_[ELEMENT_COUNT]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6768 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6768))
    _6769 = 1;
    else if (IS_ATOM_DBL(_6768))
    _6769 = IS_ATOM_INT(DoubleToInt(_6768));
    else
    _6769 = 0;
    _6768 = NOVALUE;
    if (_6769 != 0)
    goto L6; // [98] 106
    _6769 = NOVALUE;
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L6: 

    /** 	if m_[ELEMENT_COUNT] < 0 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6771 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(GREATEREQ, _6771, 0)){
        _6771 = NOVALUE;
        goto L7; // [114] 123
    }
    _6771 = NOVALUE;
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L7: 

    /** 	if not integer(m_[IN_USE]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6773 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_6773))
    _6774 = 1;
    else if (IS_ATOM_DBL(_6773))
    _6774 = IS_ATOM_INT(DoubleToInt(_6773));
    else
    _6774 = 0;
    _6773 = NOVALUE;
    if (_6774 != 0)
    goto L8; // [134] 142
    _6774 = NOVALUE;
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L8: 

    /** 	if m_[IN_USE] < 0		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6776 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(GREATEREQ, _6776, 0)){
        _6776 = NOVALUE;
        goto L9; // [150] 159
    }
    _6776 = NOVALUE;
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
L9: 

    /** 	if equal(m_[MAP_TYPE],SMALLMAP) then*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6778 = (int)*(((s1_ptr)_2)->base + 4);
    if (_6778 == 115)
    _6779 = 1;
    else if (IS_ATOM_INT(_6778) && IS_ATOM_INT(115))
    _6779 = 0;
    else
    _6779 = (compare(_6778, 115) == 0);
    _6778 = NOVALUE;
    if (_6779 == 0)
    {
        _6779 = NOVALUE;
        goto LA; // [171] 312
    }
    else{
        _6779 = NOVALUE;
    }

    /** 		if atom(m_[KEY_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6780 = (int)*(((s1_ptr)_2)->base + 5);
    _6781 = IS_ATOM(_6780);
    _6780 = NOVALUE;
    if (_6781 == 0)
    {
        _6781 = NOVALUE;
        goto LB; // [185] 193
    }
    else{
        _6781 = NOVALUE;
    }
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
LB: 

    /** 		if atom(m_[VALUE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6782 = (int)*(((s1_ptr)_2)->base + 6);
    _6783 = IS_ATOM(_6782);
    _6782 = NOVALUE;
    if (_6783 == 0)
    {
        _6783 = NOVALUE;
        goto LC; // [204] 212
    }
    else{
        _6783 = NOVALUE;
    }
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
LC: 

    /** 		if atom(m_[FREE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6784 = (int)*(((s1_ptr)_2)->base + 7);
    _6785 = IS_ATOM(_6784);
    _6784 = NOVALUE;
    if (_6785 == 0)
    {
        _6785 = NOVALUE;
        goto LD; // [223] 231
    }
    else{
        _6785 = NOVALUE;
    }
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    return 0;
LD: 

    /** 		if length(m_[KEY_LIST]) = 0  then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6786 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6786)){
            _6787 = SEQ_PTR(_6786)->length;
    }
    else {
        _6787 = 1;
    }
    _6786 = NOVALUE;
    if (_6787 != 0)
    goto LE; // [242] 251
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    return 0;
LE: 

    /** 		if length(m_[KEY_LIST]) != length(m_[VALUE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6789 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6789)){
            _6790 = SEQ_PTR(_6789)->length;
    }
    else {
        _6790 = 1;
    }
    _6789 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11961);
    _6791 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_6791)){
            _6792 = SEQ_PTR(_6791)->length;
    }
    else {
        _6792 = 1;
    }
    _6791 = NOVALUE;
    if (_6790 == _6792)
    goto LF; // [271] 280
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    _6789 = NOVALUE;
    _6791 = NOVALUE;
    return 0;
LF: 

    /** 		if length(m_[KEY_LIST]) != length(m_[FREE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6794 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6794)){
            _6795 = SEQ_PTR(_6794)->length;
    }
    else {
        _6795 = 1;
    }
    _6794 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11961);
    _6796 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_6796)){
            _6797 = SEQ_PTR(_6796)->length;
    }
    else {
        _6797 = 1;
    }
    _6796 = NOVALUE;
    if (_6795 == _6797)
    goto L10; // [300] 404
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    _6789 = NOVALUE;
    _6791 = NOVALUE;
    _6794 = NOVALUE;
    _6796 = NOVALUE;
    return 0;
    goto L10; // [309] 404
LA: 

    /** 	elsif  equal(m_[MAP_TYPE],LARGEMAP) then*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6799 = (int)*(((s1_ptr)_2)->base + 4);
    if (_6799 == 76)
    _6800 = 1;
    else if (IS_ATOM_INT(_6799) && IS_ATOM_INT(76))
    _6800 = 0;
    else
    _6800 = (compare(_6799, 76) == 0);
    _6799 = NOVALUE;
    if (_6800 == 0)
    {
        _6800 = NOVALUE;
        goto L11; // [324] 397
    }
    else{
        _6800 = NOVALUE;
    }

    /** 		if atom(m_[KEY_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6801 = (int)*(((s1_ptr)_2)->base + 5);
    _6802 = IS_ATOM(_6801);
    _6801 = NOVALUE;
    if (_6802 == 0)
    {
        _6802 = NOVALUE;
        goto L12; // [338] 346
    }
    else{
        _6802 = NOVALUE;
    }
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    _6789 = NOVALUE;
    _6791 = NOVALUE;
    _6794 = NOVALUE;
    _6796 = NOVALUE;
    return 0;
L12: 

    /** 		if atom(m_[VALUE_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6803 = (int)*(((s1_ptr)_2)->base + 6);
    _6804 = IS_ATOM(_6803);
    _6803 = NOVALUE;
    if (_6804 == 0)
    {
        _6804 = NOVALUE;
        goto L13; // [357] 365
    }
    else{
        _6804 = NOVALUE;
    }
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    _6789 = NOVALUE;
    _6791 = NOVALUE;
    _6794 = NOVALUE;
    _6796 = NOVALUE;
    return 0;
L13: 

    /** 		if length(m_[KEY_BUCKETS]) != length(m_[VALUE_BUCKETS])	then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11961);
    _6805 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6805)){
            _6806 = SEQ_PTR(_6805)->length;
    }
    else {
        _6806 = 1;
    }
    _6805 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11961);
    _6807 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_6807)){
            _6808 = SEQ_PTR(_6807)->length;
    }
    else {
        _6808 = 1;
    }
    _6807 = NOVALUE;
    if (_6806 == _6808)
    goto L10; // [385] 404
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    _6789 = NOVALUE;
    _6791 = NOVALUE;
    _6794 = NOVALUE;
    _6796 = NOVALUE;
    _6805 = NOVALUE;
    _6807 = NOVALUE;
    return 0;
    goto L10; // [394] 404
L11: 

    /** 		return 0*/
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    _6789 = NOVALUE;
    _6791 = NOVALUE;
    _6794 = NOVALUE;
    _6796 = NOVALUE;
    _6805 = NOVALUE;
    _6807 = NOVALUE;
    return 0;
L10: 

    /** 	return 1*/
    DeRef(_obj_p_11957);
    DeRef(_m__11961);
    _6786 = NOVALUE;
    _6789 = NOVALUE;
    _6791 = NOVALUE;
    _6794 = NOVALUE;
    _6796 = NOVALUE;
    _6805 = NOVALUE;
    _6807 = NOVALUE;
    return 1;
    ;
}


int _25calc_hash(int _key_p_12038, int _max_hash_p_12039)
{
    int _ret__12040 = NOVALUE;
    int _6813 = NOVALUE;
    int _6812 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_hash_p_12039)) {
        _1 = (long)(DBL_PTR(_max_hash_p_12039)->dbl);
        if (UNIQUE(DBL_PTR(_max_hash_p_12039)) && (DBL_PTR(_max_hash_p_12039)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_hash_p_12039);
        _max_hash_p_12039 = _1;
    }

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    _ret__12040 = calc_hash(_key_p_12038, -6);
    if (!IS_ATOM_INT(_ret__12040)) {
        _1 = (long)(DBL_PTR(_ret__12040)->dbl);
        if (UNIQUE(DBL_PTR(_ret__12040)) && (DBL_PTR(_ret__12040)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__12040);
        _ret__12040 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _6812 = (_ret__12040 % _max_hash_p_12039);
    _6813 = _6812 + 1;
    if (_6813 > MAXINT){
        _6813 = NewDouble((double)_6813);
    }
    _6812 = NOVALUE;
    DeRef(_key_p_12038);
    return _6813;
    ;
}


int _25threshold(int _new_value_p_12046)
{
    int _old_value__12049 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_new_value_p_12046)) {
        _1 = (long)(DBL_PTR(_new_value_p_12046)->dbl);
        if (UNIQUE(DBL_PTR(_new_value_p_12046)) && (DBL_PTR(_new_value_p_12046)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_value_p_12046);
        _new_value_p_12046 = _1;
    }

    /** 	if new_value_p < 1 then*/
    if (_new_value_p_12046 >= 1)
    goto L1; // [7] 20

    /** 		return threshold_size*/
    return _25threshold_size_11951;
L1: 

    /** 	integer old_value_ = threshold_size*/
    _old_value__12049 = _25threshold_size_11951;

    /** 	threshold_size = new_value_p*/
    _25threshold_size_11951 = _new_value_p_12046;

    /** 	return old_value_*/
    return _old_value__12049;
    ;
}


int _25type_of(int _the_map_p_12052)
{
    int _6816 = NOVALUE;
    int _6815 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][MAP_TYPE]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12052)){
        _6815 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12052)->dbl));
    }
    else{
        _6815 = (int)*(((s1_ptr)_2)->base + _the_map_p_12052);
    }
    _2 = (int)SEQ_PTR(_6815);
    _6816 = (int)*(((s1_ptr)_2)->base + 4);
    _6815 = NOVALUE;
    Ref(_6816);
    DeRef(_the_map_p_12052);
    return _6816;
    ;
}


void _25rehash(int _the_map_p_12057, int _requested_bucket_size_p_12058)
{
    int _size__12059 = NOVALUE;
    int _index_2__12060 = NOVALUE;
    int _old_key_buckets__12061 = NOVALUE;
    int _old_val_buckets__12062 = NOVALUE;
    int _new_key_buckets__12063 = NOVALUE;
    int _new_val_buckets__12064 = NOVALUE;
    int _key__12065 = NOVALUE;
    int _value__12066 = NOVALUE;
    int _pos_12067 = NOVALUE;
    int _new_keys_12068 = NOVALUE;
    int _in_use_12069 = NOVALUE;
    int _elem_count_12070 = NOVALUE;
    int _6877 = NOVALUE;
    int _6876 = NOVALUE;
    int _6875 = NOVALUE;
    int _6874 = NOVALUE;
    int _6873 = NOVALUE;
    int _6872 = NOVALUE;
    int _6871 = NOVALUE;
    int _6870 = NOVALUE;
    int _6869 = NOVALUE;
    int _6867 = NOVALUE;
    int _6866 = NOVALUE;
    int _6865 = NOVALUE;
    int _6862 = NOVALUE;
    int _6861 = NOVALUE;
    int _6859 = NOVALUE;
    int _6858 = NOVALUE;
    int _6857 = NOVALUE;
    int _6856 = NOVALUE;
    int _6854 = NOVALUE;
    int _6852 = NOVALUE;
    int _6850 = NOVALUE;
    int _6846 = NOVALUE;
    int _6844 = NOVALUE;
    int _6843 = NOVALUE;
    int _6842 = NOVALUE;
    int _6841 = NOVALUE;
    int _6839 = NOVALUE;
    int _6837 = NOVALUE;
    int _6835 = NOVALUE;
    int _6834 = NOVALUE;
    int _6832 = NOVALUE;
    int _6830 = NOVALUE;
    int _6827 = NOVALUE;
    int _6825 = NOVALUE;
    int _6824 = NOVALUE;
    int _6823 = NOVALUE;
    int _6822 = NOVALUE;
    int _6821 = NOVALUE;
    int _6818 = NOVALUE;
    int _6817 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_12057)) {
        _1 = (long)(DBL_PTR(_the_map_p_12057)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12057)) && (DBL_PTR(_the_map_p_12057)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12057);
        _the_map_p_12057 = _1;
    }
    if (!IS_ATOM_INT(_requested_bucket_size_p_12058)) {
        _1 = (long)(DBL_PTR(_requested_bucket_size_p_12058)->dbl);
        if (UNIQUE(DBL_PTR(_requested_bucket_size_p_12058)) && (DBL_PTR(_requested_bucket_size_p_12058)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_requested_bucket_size_p_12058);
        _requested_bucket_size_p_12058 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = SMALLMAP then*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6817 = (int)*(((s1_ptr)_2)->base + _the_map_p_12057);
    _2 = (int)SEQ_PTR(_6817);
    _6818 = (int)*(((s1_ptr)_2)->base + 4);
    _6817 = NOVALUE;
    if (binary_op_a(NOTEQ, _6818, 115)){
        _6818 = NOVALUE;
        goto L1; // [23] 33
    }
    _6818 = NOVALUE;

    /** 		return -- small maps are not hashed.*/
    DeRef(_old_key_buckets__12061);
    DeRef(_old_val_buckets__12062);
    DeRef(_new_key_buckets__12063);
    DeRef(_new_val_buckets__12064);
    DeRef(_key__12065);
    DeRef(_value__12066);
    DeRef(_new_keys_12068);
    return;
L1: 

    /** 	if requested_bucket_size_p <= 0 then*/
    if (_requested_bucket_size_p_12058 > 0)
    goto L2; // [35] 72

    /** 		size_ = floor(length(eumem:ram_space[the_map_p][KEY_BUCKETS]) * 3.5) + 1*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6821 = (int)*(((s1_ptr)_2)->base + _the_map_p_12057);
    _2 = (int)SEQ_PTR(_6821);
    _6822 = (int)*(((s1_ptr)_2)->base + 5);
    _6821 = NOVALUE;
    if (IS_SEQUENCE(_6822)){
            _6823 = SEQ_PTR(_6822)->length;
    }
    else {
        _6823 = 1;
    }
    _6822 = NOVALUE;
    _6824 = NewDouble((double)_6823 * DBL_PTR(_6309)->dbl);
    _6823 = NOVALUE;
    _6825 = unary_op(FLOOR, _6824);
    DeRefDS(_6824);
    _6824 = NOVALUE;
    if (IS_ATOM_INT(_6825)) {
        _size__12059 = _6825 + 1;
    }
    else
    { // coercing _size__12059 to an integer 1
        _size__12059 = 1+(long)(DBL_PTR(_6825)->dbl);
        if( !IS_ATOM_INT(_size__12059) ){
            _size__12059 = (object)DBL_PTR(_size__12059)->dbl;
        }
    }
    DeRef(_6825);
    _6825 = NOVALUE;
    goto L3; // [69] 80
L2: 

    /** 		size_ = requested_bucket_size_p*/
    _size__12059 = _requested_bucket_size_p_12058;
L3: 

    /** 	size_ = primes:next_prime(size_, -size_, 2)	-- Allow up to 2 seconds to calc next prime.*/
    if ((unsigned long)_size__12059 == 0xC0000000)
    _6827 = (int)NewDouble((double)-0xC0000000);
    else
    _6827 = - _size__12059;
    _size__12059 = _27next_prime(_size__12059, _6827, 2);
    _6827 = NOVALUE;
    if (!IS_ATOM_INT(_size__12059)) {
        _1 = (long)(DBL_PTR(_size__12059)->dbl);
        if (UNIQUE(DBL_PTR(_size__12059)) && (DBL_PTR(_size__12059)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size__12059);
        _size__12059 = _1;
    }

    /** 	if size_ < 0 then*/
    if (_size__12059 >= 0)
    goto L4; // [99] 109

    /** 		return  -- don't do anything. New size would take too long.*/
    DeRef(_old_key_buckets__12061);
    DeRef(_old_val_buckets__12062);
    DeRef(_new_key_buckets__12063);
    DeRef(_new_val_buckets__12064);
    DeRef(_key__12065);
    DeRef(_value__12066);
    DeRef(_new_keys_12068);
    _6822 = NOVALUE;
    return;
L4: 

    /** 	old_key_buckets_ = eumem:ram_space[the_map_p][KEY_BUCKETS]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6830 = (int)*(((s1_ptr)_2)->base + _the_map_p_12057);
    DeRef(_old_key_buckets__12061);
    _2 = (int)SEQ_PTR(_6830);
    _old_key_buckets__12061 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_old_key_buckets__12061);
    _6830 = NOVALUE;

    /** 	old_val_buckets_ = eumem:ram_space[the_map_p][VALUE_BUCKETS]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6832 = (int)*(((s1_ptr)_2)->base + _the_map_p_12057);
    DeRef(_old_val_buckets__12062);
    _2 = (int)SEQ_PTR(_6832);
    _old_val_buckets__12062 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_old_val_buckets__12062);
    _6832 = NOVALUE;

    /** 	new_key_buckets_ = repeat(repeat(1, threshold_size + 1), size_)*/
    _6834 = _25threshold_size_11951 + 1;
    _6835 = Repeat(1, _6834);
    _6834 = NOVALUE;
    DeRef(_new_key_buckets__12063);
    _new_key_buckets__12063 = Repeat(_6835, _size__12059);
    DeRefDS(_6835);
    _6835 = NOVALUE;

    /** 	new_val_buckets_ = repeat(repeat(0, threshold_size), size_)*/
    _6837 = Repeat(0, _25threshold_size_11951);
    DeRef(_new_val_buckets__12064);
    _new_val_buckets__12064 = Repeat(_6837, _size__12059);
    DeRefDS(_6837);
    _6837 = NOVALUE;

    /** 	elem_count = eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6839 = (int)*(((s1_ptr)_2)->base + _the_map_p_12057);
    _2 = (int)SEQ_PTR(_6839);
    _elem_count_12070 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_elem_count_12070)){
        _elem_count_12070 = (long)DBL_PTR(_elem_count_12070)->dbl;
    }
    _6839 = NOVALUE;

    /** 	in_use = 0*/
    _in_use_12069 = 0;

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12057);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	for index = 1 to length(old_key_buckets_) do*/
    if (IS_SEQUENCE(_old_key_buckets__12061)){
            _6841 = SEQ_PTR(_old_key_buckets__12061)->length;
    }
    else {
        _6841 = 1;
    }
    {
        int _index_12100;
        _index_12100 = 1;
L5: 
        if (_index_12100 > _6841){
            goto L6; // [207] 387
        }

        /** 		for entry_idx = 1 to length(old_key_buckets_[index]) do*/
        _2 = (int)SEQ_PTR(_old_key_buckets__12061);
        _6842 = (int)*(((s1_ptr)_2)->base + _index_12100);
        if (IS_SEQUENCE(_6842)){
                _6843 = SEQ_PTR(_6842)->length;
        }
        else {
            _6843 = 1;
        }
        _6842 = NOVALUE;
        {
            int _entry_idx_12103;
            _entry_idx_12103 = 1;
L7: 
            if (_entry_idx_12103 > _6843){
                goto L8; // [223] 380
            }

            /** 			key_ = old_key_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_key_buckets__12061);
            _6844 = (int)*(((s1_ptr)_2)->base + _index_12100);
            DeRef(_key__12065);
            _2 = (int)SEQ_PTR(_6844);
            _key__12065 = (int)*(((s1_ptr)_2)->base + _entry_idx_12103);
            Ref(_key__12065);
            _6844 = NOVALUE;

            /** 			value_ = old_val_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_val_buckets__12062);
            _6846 = (int)*(((s1_ptr)_2)->base + _index_12100);
            DeRef(_value__12066);
            _2 = (int)SEQ_PTR(_6846);
            _value__12066 = (int)*(((s1_ptr)_2)->base + _entry_idx_12103);
            Ref(_value__12066);
            _6846 = NOVALUE;

            /** 			index_2_ = calc_hash(key_, size_)*/
            Ref(_key__12065);
            _index_2__12060 = _25calc_hash(_key__12065, _size__12059);
            if (!IS_ATOM_INT(_index_2__12060)) {
                _1 = (long)(DBL_PTR(_index_2__12060)->dbl);
                if (UNIQUE(DBL_PTR(_index_2__12060)) && (DBL_PTR(_index_2__12060)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_index_2__12060);
                _index_2__12060 = _1;
            }

            /** 			new_keys = new_key_buckets_[index_2_]*/
            DeRef(_new_keys_12068);
            _2 = (int)SEQ_PTR(_new_key_buckets__12063);
            _new_keys_12068 = (int)*(((s1_ptr)_2)->base + _index_2__12060);
            RefDS(_new_keys_12068);

            /** 			pos = new_keys[$]*/
            if (IS_SEQUENCE(_new_keys_12068)){
                    _6850 = SEQ_PTR(_new_keys_12068)->length;
            }
            else {
                _6850 = 1;
            }
            _2 = (int)SEQ_PTR(_new_keys_12068);
            _pos_12067 = (int)*(((s1_ptr)_2)->base + _6850);
            if (!IS_ATOM_INT(_pos_12067))
            _pos_12067 = (long)DBL_PTR(_pos_12067)->dbl;

            /** 			if length(new_keys) = pos then*/
            if (IS_SEQUENCE(_new_keys_12068)){
                    _6852 = SEQ_PTR(_new_keys_12068)->length;
            }
            else {
                _6852 = 1;
            }
            if (_6852 != _pos_12067)
            goto L9; // [285] 322

            /** 				new_keys &= repeat(pos, threshold_size)*/
            _6854 = Repeat(_pos_12067, _25threshold_size_11951);
            Concat((object_ptr)&_new_keys_12068, _new_keys_12068, _6854);
            DeRefDS(_6854);
            _6854 = NOVALUE;

            /** 				new_val_buckets_[index_2_] &= repeat(0, threshold_size)*/
            _6856 = Repeat(0, _25threshold_size_11951);
            _2 = (int)SEQ_PTR(_new_val_buckets__12064);
            _6857 = (int)*(((s1_ptr)_2)->base + _index_2__12060);
            if (IS_SEQUENCE(_6857) && IS_ATOM(_6856)) {
            }
            else if (IS_ATOM(_6857) && IS_SEQUENCE(_6856)) {
                Ref(_6857);
                Prepend(&_6858, _6856, _6857);
            }
            else {
                Concat((object_ptr)&_6858, _6857, _6856);
                _6857 = NOVALUE;
            }
            _6857 = NOVALUE;
            DeRefDS(_6856);
            _6856 = NOVALUE;
            _2 = (int)SEQ_PTR(_new_val_buckets__12064);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__12064 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__12060);
            _1 = *(int *)_2;
            *(int *)_2 = _6858;
            if( _1 != _6858 ){
                DeRef(_1);
            }
            _6858 = NOVALUE;
L9: 

            /** 			new_keys[pos] = key_*/
            Ref(_key__12065);
            _2 = (int)SEQ_PTR(_new_keys_12068);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_12068 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_12067);
            _1 = *(int *)_2;
            *(int *)_2 = _key__12065;
            DeRef(_1);

            /** 			new_val_buckets_[index_2_][pos] = value_*/
            _2 = (int)SEQ_PTR(_new_val_buckets__12064);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__12064 = MAKE_SEQ(_2);
            }
            _3 = (int)(_index_2__12060 + ((s1_ptr)_2)->base);
            Ref(_value__12066);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_12067);
            _1 = *(int *)_2;
            *(int *)_2 = _value__12066;
            DeRef(_1);
            _6859 = NOVALUE;

            /** 			new_keys[$] = pos + 1*/
            if (IS_SEQUENCE(_new_keys_12068)){
                    _6861 = SEQ_PTR(_new_keys_12068)->length;
            }
            else {
                _6861 = 1;
            }
            _6862 = _pos_12067 + 1;
            if (_6862 > MAXINT){
                _6862 = NewDouble((double)_6862);
            }
            _2 = (int)SEQ_PTR(_new_keys_12068);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_12068 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _6861);
            _1 = *(int *)_2;
            *(int *)_2 = _6862;
            if( _1 != _6862 ){
                DeRef(_1);
            }
            _6862 = NOVALUE;

            /** 			new_key_buckets_[index_2_] = new_keys*/
            RefDS(_new_keys_12068);
            _2 = (int)SEQ_PTR(_new_key_buckets__12063);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_key_buckets__12063 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__12060);
            _1 = *(int *)_2;
            *(int *)_2 = _new_keys_12068;
            DeRefDS(_1);

            /** 			if pos = 1 then*/
            if (_pos_12067 != 1)
            goto LA; // [360] 373

            /** 				in_use += 1*/
            _in_use_12069 = _in_use_12069 + 1;
LA: 

            /** 		end for*/
            _entry_idx_12103 = _entry_idx_12103 + 1;
            goto L7; // [375] 230
L8: 
            ;
        }

        /** 	end for*/
        _index_12100 = _index_12100 + 1;
        goto L5; // [382] 214
L6: 
        ;
    }

    /** 	for index = 1 to length(new_key_buckets_) do*/
    if (IS_SEQUENCE(_new_key_buckets__12063)){
            _6865 = SEQ_PTR(_new_key_buckets__12063)->length;
    }
    else {
        _6865 = 1;
    }
    {
        int _index_12130;
        _index_12130 = 1;
LB: 
        if (_index_12130 > _6865){
            goto LC; // [392] 467
        }

        /** 		pos = new_key_buckets_[index][$]*/
        _2 = (int)SEQ_PTR(_new_key_buckets__12063);
        _6866 = (int)*(((s1_ptr)_2)->base + _index_12130);
        if (IS_SEQUENCE(_6866)){
                _6867 = SEQ_PTR(_6866)->length;
        }
        else {
            _6867 = 1;
        }
        _2 = (int)SEQ_PTR(_6866);
        _pos_12067 = (int)*(((s1_ptr)_2)->base + _6867);
        if (!IS_ATOM_INT(_pos_12067)){
            _pos_12067 = (long)DBL_PTR(_pos_12067)->dbl;
        }
        _6866 = NOVALUE;

        /** 		new_key_buckets_[index] = remove(new_key_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_key_buckets__12063);
        _6869 = (int)*(((s1_ptr)_2)->base + _index_12130);
        _2 = (int)SEQ_PTR(_new_key_buckets__12063);
        _6870 = (int)*(((s1_ptr)_2)->base + _index_12130);
        if (IS_SEQUENCE(_6870)){
                _6871 = SEQ_PTR(_6870)->length;
        }
        else {
            _6871 = 1;
        }
        _6870 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_6869);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_12067)) ? _pos_12067 : (long)(DBL_PTR(_pos_12067)->dbl);
            int stop = (IS_ATOM_INT(_6871)) ? _6871 : (long)(DBL_PTR(_6871)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_6869);
                DeRef(_6872);
                _6872 = _6869;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_6869), start, &_6872 );
                }
                else Tail(SEQ_PTR(_6869), stop+1, &_6872);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_6869), start, &_6872);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_6872);
                _6872 = _1;
            }
        }
        _6869 = NOVALUE;
        _6871 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_key_buckets__12063);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_key_buckets__12063 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_12130);
        _1 = *(int *)_2;
        *(int *)_2 = _6872;
        if( _1 != _6872 ){
            DeRefDS(_1);
        }
        _6872 = NOVALUE;

        /** 		new_val_buckets_[index] = remove(new_val_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_val_buckets__12064);
        _6873 = (int)*(((s1_ptr)_2)->base + _index_12130);
        _2 = (int)SEQ_PTR(_new_val_buckets__12064);
        _6874 = (int)*(((s1_ptr)_2)->base + _index_12130);
        if (IS_SEQUENCE(_6874)){
                _6875 = SEQ_PTR(_6874)->length;
        }
        else {
            _6875 = 1;
        }
        _6874 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_6873);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_12067)) ? _pos_12067 : (long)(DBL_PTR(_pos_12067)->dbl);
            int stop = (IS_ATOM_INT(_6875)) ? _6875 : (long)(DBL_PTR(_6875)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_6873);
                DeRef(_6876);
                _6876 = _6873;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_6873), start, &_6876 );
                }
                else Tail(SEQ_PTR(_6873), stop+1, &_6876);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_6873), start, &_6876);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_6876);
                _6876 = _1;
            }
        }
        _6873 = NOVALUE;
        _6875 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_val_buckets__12064);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_val_buckets__12064 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_12130);
        _1 = *(int *)_2;
        *(int *)_2 = _6876;
        if( _1 != _6876 ){
            DeRef(_1);
        }
        _6876 = NOVALUE;

        /** 	end for*/
        _index_12130 = _index_12130 + 1;
        goto LB; // [462] 399
LC: 
        ;
    }

    /** 	eumem:ram_space[the_map_p] = { */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_25type_is_map_11930);
    *((int *)(_2+4)) = _25type_is_map_11930;
    *((int *)(_2+8)) = _elem_count_12070;
    *((int *)(_2+12)) = _in_use_12069;
    *((int *)(_2+16)) = 76;
    RefDS(_new_key_buckets__12063);
    *((int *)(_2+20)) = _new_key_buckets__12063;
    RefDS(_new_val_buckets__12064);
    *((int *)(_2+24)) = _new_val_buckets__12064;
    _6877 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12057);
    _1 = *(int *)_2;
    *(int *)_2 = _6877;
    if( _1 != _6877 ){
        DeRef(_1);
    }
    _6877 = NOVALUE;

    /** end procedure*/
    DeRef(_old_key_buckets__12061);
    DeRef(_old_val_buckets__12062);
    DeRefDS(_new_key_buckets__12063);
    DeRefDS(_new_val_buckets__12064);
    DeRef(_key__12065);
    DeRef(_value__12066);
    DeRef(_new_keys_12068);
    _6822 = NOVALUE;
    _6842 = NOVALUE;
    _6870 = NOVALUE;
    _6874 = NOVALUE;
    return;
    ;
}


int _25new(int _initial_size_p_12146)
{
    int _buckets__12148 = NOVALUE;
    int _new_map__12149 = NOVALUE;
    int _temp_map__12150 = NOVALUE;
    int _6890 = NOVALUE;
    int _6889 = NOVALUE;
    int _6888 = NOVALUE;
    int _6886 = NOVALUE;
    int _6885 = NOVALUE;
    int _6882 = NOVALUE;
    int _6881 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_12146)) {
        _1 = (long)(DBL_PTR(_initial_size_p_12146)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_12146)) && (DBL_PTR(_initial_size_p_12146)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_12146);
        _initial_size_p_12146 = _1;
    }

    /** 	if initial_size_p < 3 then*/
    if (_initial_size_p_12146 >= 3)
    goto L1; // [7] 19

    /** 		initial_size_p = 3*/
    _initial_size_p_12146 = 3;
L1: 

    /** 	if initial_size_p > threshold_size then*/
    if (_initial_size_p_12146 <= _25threshold_size_11951)
    goto L2; // [23] 83

    /** 		buckets_ = floor((initial_size_p + threshold_size - 1) / threshold_size)*/
    _6881 = _initial_size_p_12146 + _25threshold_size_11951;
    if ((long)((unsigned long)_6881 + (unsigned long)HIGH_BITS) >= 0) 
    _6881 = NewDouble((double)_6881);
    if (IS_ATOM_INT(_6881)) {
        _6882 = _6881 - 1;
        if ((long)((unsigned long)_6882 +(unsigned long) HIGH_BITS) >= 0){
            _6882 = NewDouble((double)_6882);
        }
    }
    else {
        _6882 = NewDouble(DBL_PTR(_6881)->dbl - (double)1);
    }
    DeRef(_6881);
    _6881 = NOVALUE;
    if (IS_ATOM_INT(_6882)) {
        if (_25threshold_size_11951 > 0 && _6882 >= 0) {
            _buckets__12148 = _6882 / _25threshold_size_11951;
        }
        else {
            temp_dbl = floor((double)_6882 / (double)_25threshold_size_11951);
            _buckets__12148 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _6882, _25threshold_size_11951);
        _buckets__12148 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_6882);
    _6882 = NOVALUE;
    if (!IS_ATOM_INT(_buckets__12148)) {
        _1 = (long)(DBL_PTR(_buckets__12148)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__12148)) && (DBL_PTR(_buckets__12148)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__12148);
        _buckets__12148 = _1;
    }

    /** 		buckets_ = primes:next_prime(buckets_)*/
    _buckets__12148 = _27next_prime(_buckets__12148, -1, 1);
    if (!IS_ATOM_INT(_buckets__12148)) {
        _1 = (long)(DBL_PTR(_buckets__12148)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__12148)) && (DBL_PTR(_buckets__12148)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__12148);
        _buckets__12148 = _1;
    }

    /** 		new_map_ = { type_is_map, 0, 0, LARGEMAP, repeat({}, buckets_), repeat({}, buckets_) }*/
    _6885 = Repeat(_5, _buckets__12148);
    _6886 = Repeat(_5, _buckets__12148);
    _0 = _new_map__12149;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_25type_is_map_11930);
    *((int *)(_2+4)) = _25type_is_map_11930;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 76;
    *((int *)(_2+20)) = _6885;
    *((int *)(_2+24)) = _6886;
    _new_map__12149 = MAKE_SEQ(_1);
    DeRef(_0);
    _6886 = NOVALUE;
    _6885 = NOVALUE;
    goto L3; // [80] 108
L2: 

    /** 		new_map_ = {*/
    _6888 = Repeat(_25init_small_map_key_11952, _initial_size_p_12146);
    _6889 = Repeat(0, _initial_size_p_12146);
    _6890 = Repeat(0, _initial_size_p_12146);
    _0 = _new_map__12149;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_25type_is_map_11930);
    *((int *)(_2+4)) = _25type_is_map_11930;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _6888;
    *((int *)(_2+24)) = _6889;
    *((int *)(_2+28)) = _6890;
    _new_map__12149 = MAKE_SEQ(_1);
    DeRef(_0);
    _6890 = NOVALUE;
    _6889 = NOVALUE;
    _6888 = NOVALUE;
L3: 

    /** 	temp_map_ = eumem:malloc()*/
    _0 = _temp_map__12150;
    _temp_map__12150 = _26malloc(1, 1);
    DeRef(_0);

    /** 	eumem:ram_space[temp_map_] = new_map_*/
    RefDS(_new_map__12149);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map__12150))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map__12150)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map__12150);
    _1 = *(int *)_2;
    *(int *)_2 = _new_map__12149;
    DeRef(_1);

    /** 	return temp_map_*/
    DeRefDS(_new_map__12149);
    return _temp_map__12150;
    ;
}


int _25new_extra(int _the_map_p_12170, int _initial_size_p_12171)
{
    int _6894 = NOVALUE;
    int _6893 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_12171)) {
        _1 = (long)(DBL_PTR(_initial_size_p_12171)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_12171)) && (DBL_PTR(_initial_size_p_12171)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_12171);
        _initial_size_p_12171 = _1;
    }

    /** 	if map(the_map_p) then*/
    Ref(_the_map_p_12170);
    _6893 = _25map(_the_map_p_12170);
    if (_6893 == 0) {
        DeRef(_6893);
        _6893 = NOVALUE;
        goto L1; // [11] 23
    }
    else {
        if (!IS_ATOM_INT(_6893) && DBL_PTR(_6893)->dbl == 0.0){
            DeRef(_6893);
            _6893 = NOVALUE;
            goto L1; // [11] 23
        }
        DeRef(_6893);
        _6893 = NOVALUE;
    }
    DeRef(_6893);
    _6893 = NOVALUE;

    /** 		return the_map_p*/
    return _the_map_p_12170;
    goto L2; // [20] 34
L1: 

    /** 		return new(initial_size_p)*/
    _6894 = _25new(_initial_size_p_12171);
    DeRef(_the_map_p_12170);
    return _6894;
L2: 
    ;
}


int _25compare(int _map_1_p_12178, int _map_2_p_12179, int _scope_p_12180)
{
    int _data_set_1__12181 = NOVALUE;
    int _data_set_2__12182 = NOVALUE;
    int _6908 = NOVALUE;
    int _6902 = NOVALUE;
    int _6900 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scope_p_12180)) {
        _1 = (long)(DBL_PTR(_scope_p_12180)->dbl);
        if (UNIQUE(DBL_PTR(_scope_p_12180)) && (DBL_PTR(_scope_p_12180)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_p_12180);
        _scope_p_12180 = _1;
    }

    /** 	if map_1_p = map_2_p then*/
    if (binary_op_a(NOTEQ, _map_1_p_12178, _map_2_p_12179)){
        goto L1; // [7] 18
    }

    /** 		return 0*/
    DeRef(_map_1_p_12178);
    DeRef(_map_2_p_12179);
    DeRef(_data_set_1__12181);
    DeRef(_data_set_2__12182);
    return 0;
L1: 

    /** 	switch scope_p do*/
    _0 = _scope_p_12180;
    switch ( _0 ){ 

        /** 		case 'v', 'V' then*/
        case 118:
        case 86:

        /** 			data_set_1_ = stdsort:sort(values(map_1_p))*/
        Ref(_map_1_p_12178);
        _6900 = _25values(_map_1_p_12178, 0, 0);
        _0 = _data_set_1__12181;
        _data_set_1__12181 = _7sort(_6900, 1);
        DeRef(_0);
        _6900 = NOVALUE;

        /** 			data_set_2_ = stdsort:sort(values(map_2_p))*/
        Ref(_map_2_p_12179);
        _6902 = _25values(_map_2_p_12179, 0, 0);
        _0 = _data_set_2__12182;
        _data_set_2__12182 = _7sort(_6902, 1);
        DeRef(_0);
        _6902 = NOVALUE;
        goto L2; // [61] 112

        /** 		case 'k', 'K' then*/
        case 107:
        case 75:

        /** 			data_set_1_ = keys(map_1_p, 1)*/
        Ref(_map_1_p_12178);
        _0 = _data_set_1__12181;
        _data_set_1__12181 = _25keys(_map_1_p_12178, 1);
        DeRef(_0);

        /** 			data_set_2_ = keys(map_2_p, 1)*/
        Ref(_map_2_p_12179);
        _0 = _data_set_2__12182;
        _data_set_2__12182 = _25keys(_map_2_p_12179, 1);
        DeRef(_0);
        goto L2; // [87] 112

        /** 		case else*/
        default:

        /** 			data_set_1_ = pairs(map_1_p, 1)*/
        Ref(_map_1_p_12178);
        _0 = _data_set_1__12181;
        _data_set_1__12181 = _25pairs(_map_1_p_12178, 1);
        DeRef(_0);

        /** 			data_set_2_ = pairs(map_2_p, 1)*/
        Ref(_map_2_p_12179);
        _0 = _data_set_2__12182;
        _data_set_2__12182 = _25pairs(_map_2_p_12179, 1);
        DeRef(_0);
    ;}L2: 

    /** 	if equal(data_set_1_, data_set_2_) then*/
    if (_data_set_1__12181 == _data_set_2__12182)
    _6908 = 1;
    else if (IS_ATOM_INT(_data_set_1__12181) && IS_ATOM_INT(_data_set_2__12182))
    _6908 = 0;
    else
    _6908 = (compare(_data_set_1__12181, _data_set_2__12182) == 0);
    if (_6908 == 0)
    {
        _6908 = NOVALUE;
        goto L3; // [122] 132
    }
    else{
        _6908 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_map_1_p_12178);
    DeRef(_map_2_p_12179);
    DeRefDS(_data_set_1__12181);
    DeRefDS(_data_set_2__12182);
    return 1;
L3: 

    /** 	return -1*/
    DeRef(_map_1_p_12178);
    DeRef(_map_2_p_12179);
    DeRef(_data_set_1__12181);
    DeRef(_data_set_2__12182);
    return -1;
    ;
}


int _25has(int _the_map_p_12210, int _the_key_p_12211)
{
    int _index__12212 = NOVALUE;
    int _pos__12213 = NOVALUE;
    int _from__12214 = NOVALUE;
    int _6933 = NOVALUE;
    int _6931 = NOVALUE;
    int _6930 = NOVALUE;
    int _6927 = NOVALUE;
    int _6926 = NOVALUE;
    int _6925 = NOVALUE;
    int _6923 = NOVALUE;
    int _6922 = NOVALUE;
    int _6920 = NOVALUE;
    int _6918 = NOVALUE;
    int _6917 = NOVALUE;
    int _6916 = NOVALUE;
    int _6914 = NOVALUE;
    int _6913 = NOVALUE;
    int _6912 = NOVALUE;
    int _6910 = NOVALUE;
    int _6909 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_12210)) {
        _1 = (long)(DBL_PTR(_the_map_p_12210)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12210)) && (DBL_PTR(_the_map_p_12210)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12210);
        _the_map_p_12210 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6909 = (int)*(((s1_ptr)_2)->base + _the_map_p_12210);
    _2 = (int)SEQ_PTR(_6909);
    _6910 = (int)*(((s1_ptr)_2)->base + 4);
    _6909 = NOVALUE;
    if (binary_op_a(NOTEQ, _6910, 76)){
        _6910 = NOVALUE;
        goto L1; // [19] 77
    }
    _6910 = NOVALUE;

    /** 		index_ = calc_hash(the_key_p, length(eumem:ram_space[the_map_p][KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6912 = (int)*(((s1_ptr)_2)->base + _the_map_p_12210);
    _2 = (int)SEQ_PTR(_6912);
    _6913 = (int)*(((s1_ptr)_2)->base + 5);
    _6912 = NOVALUE;
    if (IS_SEQUENCE(_6913)){
            _6914 = SEQ_PTR(_6913)->length;
    }
    else {
        _6914 = 1;
    }
    _6913 = NOVALUE;
    Ref(_the_key_p_12211);
    _index__12212 = _25calc_hash(_the_key_p_12211, _6914);
    _6914 = NOVALUE;
    if (!IS_ATOM_INT(_index__12212)) {
        _1 = (long)(DBL_PTR(_index__12212)->dbl);
        if (UNIQUE(DBL_PTR(_index__12212)) && (DBL_PTR(_index__12212)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index__12212);
        _index__12212 = _1;
    }

    /** 		pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_BUCKETS][index_])*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6916 = (int)*(((s1_ptr)_2)->base + _the_map_p_12210);
    _2 = (int)SEQ_PTR(_6916);
    _6917 = (int)*(((s1_ptr)_2)->base + 5);
    _6916 = NOVALUE;
    _2 = (int)SEQ_PTR(_6917);
    _6918 = (int)*(((s1_ptr)_2)->base + _index__12212);
    _6917 = NOVALUE;
    _pos__12213 = find_from(_the_key_p_12211, _6918, 1);
    _6918 = NOVALUE;
    goto L2; // [74] 206
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12211 == _25init_small_map_key_11952)
    _6920 = 1;
    else if (IS_ATOM_INT(_the_key_p_12211) && IS_ATOM_INT(_25init_small_map_key_11952))
    _6920 = 0;
    else
    _6920 = (compare(_the_key_p_12211, _25init_small_map_key_11952) == 0);
    if (_6920 == 0)
    {
        _6920 = NOVALUE;
        goto L3; // [83] 183
    }
    else{
        _6920 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12214 = 1;

    /** 			while from_ > 0 do*/
L4: 
    if (_from__12214 <= 0)
    goto L5; // [98] 205

    /** 				pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6922 = (int)*(((s1_ptr)_2)->base + _the_map_p_12210);
    _2 = (int)SEQ_PTR(_6922);
    _6923 = (int)*(((s1_ptr)_2)->base + 5);
    _6922 = NOVALUE;
    _pos__12213 = find_from(_the_key_p_12211, _6923, _from__12214);
    _6923 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__12213 == 0)
    {
        goto L6; // [125] 160
    }
    else{
    }

    /** 					if eumem:ram_space[the_map_p][FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6925 = (int)*(((s1_ptr)_2)->base + _the_map_p_12210);
    _2 = (int)SEQ_PTR(_6925);
    _6926 = (int)*(((s1_ptr)_2)->base + 7);
    _6925 = NOVALUE;
    _2 = (int)SEQ_PTR(_6926);
    _6927 = (int)*(((s1_ptr)_2)->base + _pos__12213);
    _6926 = NOVALUE;
    if (binary_op_a(NOTEQ, _6927, 1)){
        _6927 = NOVALUE;
        goto L7; // [146] 167
    }
    _6927 = NOVALUE;

    /** 						return 1*/
    DeRef(_the_key_p_12211);
    _6913 = NOVALUE;
    return 1;
    goto L7; // [157] 167
L6: 

    /** 					return 0*/
    DeRef(_the_key_p_12211);
    _6913 = NOVALUE;
    return 0;
L7: 

    /** 				from_ = pos_ + 1*/
    _from__12214 = _pos__12213 + 1;

    /** 			end while*/
    goto L4; // [177] 98
    goto L5; // [180] 205
L3: 

    /** 			pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST])*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _6930 = (int)*(((s1_ptr)_2)->base + _the_map_p_12210);
    _2 = (int)SEQ_PTR(_6930);
    _6931 = (int)*(((s1_ptr)_2)->base + 5);
    _6930 = NOVALUE;
    _pos__12213 = find_from(_the_key_p_12211, _6931, 1);
    _6931 = NOVALUE;
L5: 
L2: 

    /** 	return (pos_  != 0)	*/
    _6933 = (_pos__12213 != 0);
    DeRef(_the_key_p_12211);
    _6913 = NOVALUE;
    return _6933;
    ;
}


int _25get(int _the_map_p_12250, int _the_key_p_12251, int _default_value_p_12252)
{
    int _bucket__12253 = NOVALUE;
    int _pos__12254 = NOVALUE;
    int _from__12255 = NOVALUE;
    int _themap_12256 = NOVALUE;
    int _thekeys_12261 = NOVALUE;
    int _6959 = NOVALUE;
    int _6958 = NOVALUE;
    int _6956 = NOVALUE;
    int _6954 = NOVALUE;
    int _6953 = NOVALUE;
    int _6951 = NOVALUE;
    int _6950 = NOVALUE;
    int _6948 = NOVALUE;
    int _6946 = NOVALUE;
    int _6945 = NOVALUE;
    int _6944 = NOVALUE;
    int _6943 = NOVALUE;
    int _6940 = NOVALUE;
    int _6938 = NOVALUE;
    int _6935 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_12250)) {
        _1 = (long)(DBL_PTR(_the_map_p_12250)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12250)) && (DBL_PTR(_the_map_p_12250)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12250);
        _the_map_p_12250 = _1;
    }

    /** 	themap = eumem:ram_space[the_map_p]*/
    DeRef(_themap_12256);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _themap_12256 = (int)*(((s1_ptr)_2)->base + _the_map_p_12250);
    Ref(_themap_12256);

    /** 	if themap[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_themap_12256);
    _6935 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6935, 76)){
        _6935 = NOVALUE;
        goto L1; // [23] 104
    }
    _6935 = NOVALUE;

    /** 		sequence thekeys*/

    /** 		thekeys = themap[KEY_BUCKETS]*/
    DeRef(_thekeys_12261);
    _2 = (int)SEQ_PTR(_themap_12256);
    _thekeys_12261 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_thekeys_12261);

    /** 		bucket_ = calc_hash(the_key_p, length(thekeys))*/
    if (IS_SEQUENCE(_thekeys_12261)){
            _6938 = SEQ_PTR(_thekeys_12261)->length;
    }
    else {
        _6938 = 1;
    }
    Ref(_the_key_p_12251);
    _bucket__12253 = _25calc_hash(_the_key_p_12251, _6938);
    _6938 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12253)) {
        _1 = (long)(DBL_PTR(_bucket__12253)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12253)) && (DBL_PTR(_bucket__12253)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12253);
        _bucket__12253 = _1;
    }

    /** 		pos_ = find(the_key_p, thekeys[bucket_])*/
    _2 = (int)SEQ_PTR(_thekeys_12261);
    _6940 = (int)*(((s1_ptr)_2)->base + _bucket__12253);
    _pos__12254 = find_from(_the_key_p_12251, _6940, 1);
    _6940 = NOVALUE;

    /** 		if pos_ > 0 then*/
    if (_pos__12254 <= 0)
    goto L2; // [68] 93

    /** 			return themap[VALUE_BUCKETS][bucket_][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12256);
    _6943 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6943);
    _6944 = (int)*(((s1_ptr)_2)->base + _bucket__12253);
    _6943 = NOVALUE;
    _2 = (int)SEQ_PTR(_6944);
    _6945 = (int)*(((s1_ptr)_2)->base + _pos__12254);
    _6944 = NOVALUE;
    Ref(_6945);
    DeRefDS(_thekeys_12261);
    DeRef(_the_key_p_12251);
    DeRef(_default_value_p_12252);
    DeRefDS(_themap_12256);
    return _6945;
L2: 

    /** 		return default_value_p*/
    DeRef(_thekeys_12261);
    DeRef(_the_key_p_12251);
    DeRef(_themap_12256);
    _6945 = NOVALUE;
    return _default_value_p_12252;
    goto L3; // [101] 247
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12251 == _25init_small_map_key_11952)
    _6946 = 1;
    else if (IS_ATOM_INT(_the_key_p_12251) && IS_ATOM_INT(_25init_small_map_key_11952))
    _6946 = 0;
    else
    _6946 = (compare(_the_key_p_12251, _25init_small_map_key_11952) == 0);
    if (_6946 == 0)
    {
        _6946 = NOVALUE;
        goto L4; // [110] 208
    }
    else{
        _6946 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12255 = 1;

    /** 			while from_ > 0 do*/
L5: 
    if (_from__12255 <= 0)
    goto L6; // [125] 246

    /** 				pos_ = find(the_key_p, themap[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_themap_12256);
    _6948 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__12254 = find_from(_the_key_p_12251, _6948, _from__12255);
    _6948 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__12254 == 0)
    {
        goto L7; // [146] 185
    }
    else{
    }

    /** 					if themap[FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_themap_12256);
    _6950 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_6950);
    _6951 = (int)*(((s1_ptr)_2)->base + _pos__12254);
    _6950 = NOVALUE;
    if (binary_op_a(NOTEQ, _6951, 1)){
        _6951 = NOVALUE;
        goto L8; // [161] 192
    }
    _6951 = NOVALUE;

    /** 						return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12256);
    _6953 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6953);
    _6954 = (int)*(((s1_ptr)_2)->base + _pos__12254);
    _6953 = NOVALUE;
    Ref(_6954);
    DeRef(_the_key_p_12251);
    DeRef(_default_value_p_12252);
    DeRefDS(_themap_12256);
    _6945 = NOVALUE;
    return _6954;
    goto L8; // [182] 192
L7: 

    /** 					return default_value_p*/
    DeRef(_the_key_p_12251);
    DeRef(_themap_12256);
    _6945 = NOVALUE;
    _6954 = NOVALUE;
    return _default_value_p_12252;
L8: 

    /** 				from_ = pos_ + 1*/
    _from__12255 = _pos__12254 + 1;

    /** 			end while*/
    goto L5; // [202] 125
    goto L6; // [205] 246
L4: 

    /** 			pos_ = find(the_key_p, themap[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_themap_12256);
    _6956 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__12254 = find_from(_the_key_p_12251, _6956, 1);
    _6956 = NOVALUE;

    /** 			if pos_  then*/
    if (_pos__12254 == 0)
    {
        goto L9; // [225] 245
    }
    else{
    }

    /** 				return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12256);
    _6958 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6958);
    _6959 = (int)*(((s1_ptr)_2)->base + _pos__12254);
    _6958 = NOVALUE;
    Ref(_6959);
    DeRef(_the_key_p_12251);
    DeRef(_default_value_p_12252);
    DeRefDS(_themap_12256);
    _6945 = NOVALUE;
    _6954 = NOVALUE;
    return _6959;
L9: 
L6: 
L3: 

    /** 	return default_value_p*/
    DeRef(_the_key_p_12251);
    DeRef(_themap_12256);
    _6945 = NOVALUE;
    _6954 = NOVALUE;
    _6959 = NOVALUE;
    return _default_value_p_12252;
    ;
}


int _25nested_get(int _the_map_p_12296, int _the_keys_p_12297, int _default_value_p_12298)
{
    int _val__12303 = NOVALUE;
    int _6969 = NOVALUE;
    int _6968 = NOVALUE;
    int _6966 = NOVALUE;
    int _6964 = NOVALUE;
    int _6962 = NOVALUE;
    int _6961 = NOVALUE;
    int _6960 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( the_keys_p ) - 1 do*/
    if (IS_SEQUENCE(_the_keys_p_12297)){
            _6960 = SEQ_PTR(_the_keys_p_12297)->length;
    }
    else {
        _6960 = 1;
    }
    _6961 = _6960 - 1;
    _6960 = NOVALUE;
    {
        int _i_12300;
        _i_12300 = 1;
L1: 
        if (_i_12300 > _6961){
            goto L2; // [12] 74
        }

        /** 		object val_ = get( the_map_p, the_keys_p[1], 0 )*/
        _2 = (int)SEQ_PTR(_the_keys_p_12297);
        _6962 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_the_map_p_12296);
        Ref(_6962);
        _0 = _val__12303;
        _val__12303 = _25get(_the_map_p_12296, _6962, 0);
        DeRef(_0);
        _6962 = NOVALUE;

        /** 		if not map( val_ ) then*/
        Ref(_val__12303);
        _6964 = _25map(_val__12303);
        if (IS_ATOM_INT(_6964)) {
            if (_6964 != 0){
                DeRef(_6964);
                _6964 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        else {
            if (DBL_PTR(_6964)->dbl != 0.0){
                DeRef(_6964);
                _6964 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        DeRef(_6964);
        _6964 = NOVALUE;

        /** 			return default_value_p*/
        DeRef(_val__12303);
        DeRef(_the_map_p_12296);
        DeRefDS(_the_keys_p_12297);
        DeRef(_6961);
        _6961 = NOVALUE;
        return _default_value_p_12298;
        goto L4; // [46] 65
L3: 

        /** 			the_map_p = val_*/
        Ref(_val__12303);
        DeRef(_the_map_p_12296);
        _the_map_p_12296 = _val__12303;

        /** 			the_keys_p = the_keys_p[2..$]*/
        if (IS_SEQUENCE(_the_keys_p_12297)){
                _6966 = SEQ_PTR(_the_keys_p_12297)->length;
        }
        else {
            _6966 = 1;
        }
        rhs_slice_target = (object_ptr)&_the_keys_p_12297;
        RHS_Slice(_the_keys_p_12297, 2, _6966);
L4: 
        DeRef(_val__12303);
        _val__12303 = NOVALUE;

        /** 	end for*/
        _i_12300 = _i_12300 + 1;
        goto L1; // [69] 19
L2: 
        ;
    }

    /** 	return get( the_map_p, the_keys_p[1], default_value_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12297);
    _6968 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12296);
    Ref(_6968);
    Ref(_default_value_p_12298);
    _6969 = _25get(_the_map_p_12296, _6968, _default_value_p_12298);
    _6968 = NOVALUE;
    DeRef(_the_map_p_12296);
    DeRefDS(_the_keys_p_12297);
    DeRef(_default_value_p_12298);
    DeRef(_6961);
    _6961 = NOVALUE;
    return _6969;
    ;
}


void _25put(int _the_map_p_12316, int _the_key_p_12317, int _the_value_p_12318, int _operation_p_12319, int _trigger_p_12320)
{
    int _index__12321 = NOVALUE;
    int _bucket__12322 = NOVALUE;
    int _average_length__12323 = NOVALUE;
    int _from__12324 = NOVALUE;
    int _map_data_12325 = NOVALUE;
    int _data_12364 = NOVALUE;
    int _msg_inlined_crash_at_347_12380 = NOVALUE;
    int _msg_inlined_crash_at_394_12386 = NOVALUE;
    int _tmp_seqk_12400 = NOVALUE;
    int _tmp_seqv_12408 = NOVALUE;
    int _msg_inlined_crash_at_778_12444 = NOVALUE;
    int _msg_inlined_crash_at_1176_12507 = NOVALUE;
    int _7104 = NOVALUE;
    int _7103 = NOVALUE;
    int _7101 = NOVALUE;
    int _7100 = NOVALUE;
    int _7099 = NOVALUE;
    int _7098 = NOVALUE;
    int _7096 = NOVALUE;
    int _7095 = NOVALUE;
    int _7094 = NOVALUE;
    int _7092 = NOVALUE;
    int _7091 = NOVALUE;
    int _7090 = NOVALUE;
    int _7088 = NOVALUE;
    int _7087 = NOVALUE;
    int _7086 = NOVALUE;
    int _7084 = NOVALUE;
    int _7083 = NOVALUE;
    int _7082 = NOVALUE;
    int _7080 = NOVALUE;
    int _7078 = NOVALUE;
    int _7072 = NOVALUE;
    int _7071 = NOVALUE;
    int _7070 = NOVALUE;
    int _7069 = NOVALUE;
    int _7067 = NOVALUE;
    int _7065 = NOVALUE;
    int _7064 = NOVALUE;
    int _7063 = NOVALUE;
    int _7062 = NOVALUE;
    int _7061 = NOVALUE;
    int _7060 = NOVALUE;
    int _7057 = NOVALUE;
    int _7055 = NOVALUE;
    int _7052 = NOVALUE;
    int _7050 = NOVALUE;
    int _7047 = NOVALUE;
    int _7046 = NOVALUE;
    int _7044 = NOVALUE;
    int _7041 = NOVALUE;
    int _7040 = NOVALUE;
    int _7037 = NOVALUE;
    int _7034 = NOVALUE;
    int _7032 = NOVALUE;
    int _7030 = NOVALUE;
    int _7027 = NOVALUE;
    int _7025 = NOVALUE;
    int _7024 = NOVALUE;
    int _7023 = NOVALUE;
    int _7022 = NOVALUE;
    int _7021 = NOVALUE;
    int _7020 = NOVALUE;
    int _7019 = NOVALUE;
    int _7018 = NOVALUE;
    int _7017 = NOVALUE;
    int _7011 = NOVALUE;
    int _7009 = NOVALUE;
    int _7008 = NOVALUE;
    int _7006 = NOVALUE;
    int _7004 = NOVALUE;
    int _7001 = NOVALUE;
    int _7000 = NOVALUE;
    int _6999 = NOVALUE;
    int _6998 = NOVALUE;
    int _6996 = NOVALUE;
    int _6995 = NOVALUE;
    int _6994 = NOVALUE;
    int _6992 = NOVALUE;
    int _6991 = NOVALUE;
    int _6990 = NOVALUE;
    int _6988 = NOVALUE;
    int _6987 = NOVALUE;
    int _6986 = NOVALUE;
    int _6984 = NOVALUE;
    int _6982 = NOVALUE;
    int _6977 = NOVALUE;
    int _6976 = NOVALUE;
    int _6974 = NOVALUE;
    int _6973 = NOVALUE;
    int _6971 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_12316)) {
        _1 = (long)(DBL_PTR(_the_map_p_12316)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12316)) && (DBL_PTR(_the_map_p_12316)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12316);
        _the_map_p_12316 = _1;
    }
    if (!IS_ATOM_INT(_operation_p_12319)) {
        _1 = (long)(DBL_PTR(_operation_p_12319)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_12319)) && (DBL_PTR(_operation_p_12319)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_12319);
        _operation_p_12319 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_12320)) {
        _1 = (long)(DBL_PTR(_trigger_p_12320)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_12320)) && (DBL_PTR(_trigger_p_12320)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_12320);
        _trigger_p_12320 = _1;
    }

    /** 	sequence map_data = eumem:ram_space[the_map_p]*/
    DeRef(_map_data_12325);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _map_data_12325 = (int)*(((s1_ptr)_2)->base + _the_map_p_12316);
    Ref(_map_data_12325);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12316);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if map_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _6971 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6971, 76)){
        _6971 = NOVALUE;
        goto L1; // [39] 659
    }
    _6971 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p,  length(map_data[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _6973 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6973)){
            _6974 = SEQ_PTR(_6973)->length;
    }
    else {
        _6974 = 1;
    }
    _6973 = NOVALUE;
    Ref(_the_key_p_12317);
    _bucket__12322 = _25calc_hash(_the_key_p_12317, _6974);
    _6974 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12322)) {
        _1 = (long)(DBL_PTR(_bucket__12322)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12322)) && (DBL_PTR(_bucket__12322)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12322);
        _bucket__12322 = _1;
    }

    /** 		index_ = find(the_key_p, map_data[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _6976 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6976);
    _6977 = (int)*(((s1_ptr)_2)->base + _bucket__12322);
    _6976 = NOVALUE;
    _index__12321 = find_from(_the_key_p_12317, _6977, 1);
    _6977 = NOVALUE;

    /** 		if index_ > 0 then*/
    if (_index__12321 <= 0)
    goto L2; // [84] 381

    /** 			switch operation_p do*/
    _0 = _operation_p_12319;
    switch ( _0 ){ 

        /** 				case PUT then*/
        case 1:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12322 + ((s1_ptr)_2)->base);
        _6982 = NOVALUE;
        Ref(_the_value_p_12318);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_12318;
        DeRef(_1);
        _6982 = NOVALUE;
        goto L3; // [117] 367

        /** 				case ADD then*/
        case 2:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12322 + ((s1_ptr)_2)->base);
        _6984 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6986 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _6984 = NOVALUE;
        if (IS_ATOM_INT(_6986) && IS_ATOM_INT(_the_value_p_12318)) {
            _6987 = _6986 + _the_value_p_12318;
            if ((long)((unsigned long)_6987 + (unsigned long)HIGH_BITS) >= 0) 
            _6987 = NewDouble((double)_6987);
        }
        else {
            _6987 = binary_op(PLUS, _6986, _the_value_p_12318);
        }
        _6986 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _6987;
        if( _1 != _6987 ){
            DeRef(_1);
        }
        _6987 = NOVALUE;
        _6984 = NOVALUE;
        goto L3; // [149] 367

        /** 				case SUBTRACT then*/
        case 3:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12322 + ((s1_ptr)_2)->base);
        _6988 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6990 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _6988 = NOVALUE;
        if (IS_ATOM_INT(_6990) && IS_ATOM_INT(_the_value_p_12318)) {
            _6991 = _6990 - _the_value_p_12318;
            if ((long)((unsigned long)_6991 +(unsigned long) HIGH_BITS) >= 0){
                _6991 = NewDouble((double)_6991);
            }
        }
        else {
            _6991 = binary_op(MINUS, _6990, _the_value_p_12318);
        }
        _6990 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _6991;
        if( _1 != _6991 ){
            DeRef(_1);
        }
        _6991 = NOVALUE;
        _6988 = NOVALUE;
        goto L3; // [181] 367

        /** 				case MULTIPLY then*/
        case 4:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12322 + ((s1_ptr)_2)->base);
        _6992 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6994 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _6992 = NOVALUE;
        if (IS_ATOM_INT(_6994) && IS_ATOM_INT(_the_value_p_12318)) {
            if (_6994 == (short)_6994 && _the_value_p_12318 <= INT15 && _the_value_p_12318 >= -INT15)
            _6995 = _6994 * _the_value_p_12318;
            else
            _6995 = NewDouble(_6994 * (double)_the_value_p_12318);
        }
        else {
            _6995 = binary_op(MULTIPLY, _6994, _the_value_p_12318);
        }
        _6994 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _6995;
        if( _1 != _6995 ){
            DeRef(_1);
        }
        _6995 = NOVALUE;
        _6992 = NOVALUE;
        goto L3; // [213] 367

        /** 				case DIVIDE then*/
        case 5:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12322 + ((s1_ptr)_2)->base);
        _6996 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6998 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _6996 = NOVALUE;
        if (IS_ATOM_INT(_6998) && IS_ATOM_INT(_the_value_p_12318)) {
            _6999 = (_6998 % _the_value_p_12318) ? NewDouble((double)_6998 / _the_value_p_12318) : (_6998 / _the_value_p_12318);
        }
        else {
            _6999 = binary_op(DIVIDE, _6998, _the_value_p_12318);
        }
        _6998 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _6999;
        if( _1 != _6999 ){
            DeRef(_1);
        }
        _6999 = NOVALUE;
        _6996 = NOVALUE;
        goto L3; // [245] 367

        /** 				case APPEND then*/
        case 6:

        /** 					sequence data = map_data[VALUE_BUCKETS][bucket_][index_]*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        _7000 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7000);
        _7001 = (int)*(((s1_ptr)_2)->base + _bucket__12322);
        _7000 = NOVALUE;
        DeRef(_data_12364);
        _2 = (int)SEQ_PTR(_7001);
        _data_12364 = (int)*(((s1_ptr)_2)->base + _index__12321);
        Ref(_data_12364);
        _7001 = NOVALUE;

        /** 					data = append( data, the_value_p )*/
        Ref(_the_value_p_12318);
        Append(&_data_12364, _data_12364, _the_value_p_12318);

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = data*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12322 + ((s1_ptr)_2)->base);
        _7004 = NOVALUE;
        RefDS(_data_12364);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _data_12364;
        DeRef(_1);
        _7004 = NOVALUE;
        DeRefDS(_data_12364);
        _data_12364 = NOVALUE;
        goto L3; // [295] 367

        /** 				case CONCAT then*/
        case 7:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12322 + ((s1_ptr)_2)->base);
        _7006 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7008 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _7006 = NOVALUE;
        if (IS_SEQUENCE(_7008) && IS_ATOM(_the_value_p_12318)) {
            Ref(_the_value_p_12318);
            Append(&_7009, _7008, _the_value_p_12318);
        }
        else if (IS_ATOM(_7008) && IS_SEQUENCE(_the_value_p_12318)) {
            Ref(_7008);
            Prepend(&_7009, _the_value_p_12318, _7008);
        }
        else {
            Concat((object_ptr)&_7009, _7008, _the_value_p_12318);
            _7008 = NOVALUE;
        }
        _7008 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _7009;
        if( _1 != _7009 ){
            DeRef(_1);
        }
        _7009 = NOVALUE;
        _7006 = NOVALUE;
        goto L3; // [327] 367

        /** 				case LEAVE then*/
        case 8:

        /** 					operation_p = operation_p*/
        _operation_p_12319 = _operation_p_12319;
        goto L3; // [340] 367

        /** 				case else*/
        default:

        /** 					error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_347_12380);
        _msg_inlined_crash_at_347_12380 = EPrintf(-9999999, _7010, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_347_12380);

        /** end procedure*/
        goto L4; // [361] 364
L4: 
        DeRefi(_msg_inlined_crash_at_347_12380);
        _msg_inlined_crash_at_347_12380 = NOVALUE;
    ;}L3: 

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12325);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12316);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12325;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_12400);
    DeRef(_tmp_seqv_12408);
    DeRef(_the_key_p_12317);
    DeRef(_the_value_p_12318);
    DeRef(_average_length__12323);
    DeRefDS(_map_data_12325);
    _6973 = NOVALUE;
    return;
L2: 

    /** 		if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _7011 = find_from(_operation_p_12319, _25INIT_OPERATIONS_11947, 1);
    if (_7011 != 0)
    goto L5; // [390] 414
    _7011 = NOVALUE;

    /** 				error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_394_12386);
    _msg_inlined_crash_at_394_12386 = EPrintf(-9999999, _7013, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_394_12386);

    /** end procedure*/
    goto L6; // [408] 411
L6: 
    DeRefi(_msg_inlined_crash_at_394_12386);
    _msg_inlined_crash_at_394_12386 = NOVALUE;
L5: 

    /** 		if operation_p = LEAVE then*/
    if (_operation_p_12319 != 8)
    goto L7; // [418] 436

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12325);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12316);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12325;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_12400);
    DeRef(_tmp_seqv_12408);
    DeRef(_the_key_p_12317);
    DeRef(_the_value_p_12318);
    DeRef(_average_length__12323);
    DeRefDS(_map_data_12325);
    _6973 = NOVALUE;
    return;
L7: 

    /** 		if operation_p = APPEND then*/
    if (_operation_p_12319 != 6)
    goto L8; // [440] 451

    /** 			the_value_p = { the_value_p }*/
    _0 = _the_value_p_12318;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_12318);
    *((int *)(_2+4)) = _the_value_p_12318;
    _the_value_p_12318 = MAKE_SEQ(_1);
    DeRef(_0);
L8: 

    /** 		map_data[IN_USE] += (length(map_data[KEY_BUCKETS][bucket_]) = 0)*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7017 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7017);
    _7018 = (int)*(((s1_ptr)_2)->base + _bucket__12322);
    _7017 = NOVALUE;
    if (IS_SEQUENCE(_7018)){
            _7019 = SEQ_PTR(_7018)->length;
    }
    else {
        _7019 = 1;
    }
    _7018 = NOVALUE;
    _7020 = (_7019 == 0);
    _7019 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7021 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7021)) {
        _7022 = _7021 + _7020;
        if ((long)((unsigned long)_7022 + (unsigned long)HIGH_BITS) >= 0) 
        _7022 = NewDouble((double)_7022);
    }
    else {
        _7022 = binary_op(PLUS, _7021, _7020);
    }
    _7021 = NOVALUE;
    _7020 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7022;
    if( _1 != _7022 ){
        DeRef(_1);
    }
    _7022 = NOVALUE;

    /** 		map_data[ELEMENT_COUNT] += 1 -- elementCount		*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7023 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7023)) {
        _7024 = _7023 + 1;
        if (_7024 > MAXINT){
            _7024 = NewDouble((double)_7024);
        }
    }
    else
    _7024 = binary_op(PLUS, 1, _7023);
    _7023 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7024;
    if( _1 != _7024 ){
        DeRef(_1);
    }
    _7024 = NOVALUE;

    /** 		sequence tmp_seqk*/

    /** 		tmp_seqk = map_data[KEY_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7025 = (int)*(((s1_ptr)_2)->base + 5);
    DeRef(_tmp_seqk_12400);
    _2 = (int)SEQ_PTR(_7025);
    _tmp_seqk_12400 = (int)*(((s1_ptr)_2)->base + _bucket__12322);
    Ref(_tmp_seqk_12400);
    _7025 = NOVALUE;

    /** 		map_data[KEY_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12322);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7027 = NOVALUE;

    /** 		tmp_seqk = append( tmp_seqk, the_key_p)*/
    Ref(_the_key_p_12317);
    Append(&_tmp_seqk_12400, _tmp_seqk_12400, _the_key_p_12317);

    /** 		map_data[KEY_BUCKETS][bucket_] = tmp_seqk*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqk_12400);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12322);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqk_12400;
    DeRef(_1);
    _7030 = NOVALUE;

    /** 		sequence tmp_seqv*/

    /** 		tmp_seqv = map_data[VALUE_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7032 = (int)*(((s1_ptr)_2)->base + 6);
    DeRef(_tmp_seqv_12408);
    _2 = (int)SEQ_PTR(_7032);
    _tmp_seqv_12408 = (int)*(((s1_ptr)_2)->base + _bucket__12322);
    Ref(_tmp_seqv_12408);
    _7032 = NOVALUE;

    /** 		map_data[VALUE_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12322);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7034 = NOVALUE;

    /** 		tmp_seqv = append( tmp_seqv, the_value_p)*/
    Ref(_the_value_p_12318);
    Append(&_tmp_seqv_12408, _tmp_seqv_12408, _the_value_p_12318);

    /** 		map_data[VALUE_BUCKETS][bucket_] = tmp_seqv*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqv_12408);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12322);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqv_12408;
    DeRef(_1);
    _7037 = NOVALUE;

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12325);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12316);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12325;
    DeRef(_1);

    /** 		if trigger_p > 0 then*/
    if (_trigger_p_12320 <= 0)
    goto L9; // [606] 649

    /** 			average_length_ = map_data[ELEMENT_COUNT] / map_data[IN_USE]*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7040 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7041 = (int)*(((s1_ptr)_2)->base + 3);
    DeRef(_average_length__12323);
    if (IS_ATOM_INT(_7040) && IS_ATOM_INT(_7041)) {
        _average_length__12323 = (_7040 % _7041) ? NewDouble((double)_7040 / _7041) : (_7040 / _7041);
    }
    else {
        _average_length__12323 = binary_op(DIVIDE, _7040, _7041);
    }
    _7040 = NOVALUE;
    _7041 = NOVALUE;

    /** 			if (average_length_ >= trigger_p) then*/
    if (binary_op_a(LESS, _average_length__12323, _trigger_p_12320)){
        goto LA; // [630] 648
    }

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_12325);
    _map_data_12325 = _5;

    /** 				rehash(the_map_p)*/
    _25rehash(_the_map_p_12316, 0);
LA: 
L9: 

    /** 		return*/
    DeRef(_tmp_seqk_12400);
    DeRef(_tmp_seqv_12408);
    DeRef(_the_key_p_12317);
    DeRef(_the_value_p_12318);
    DeRef(_average_length__12323);
    DeRef(_map_data_12325);
    _6973 = NOVALUE;
    _7018 = NOVALUE;
    return;
    goto LB; // [656] 1209
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12317 == _25init_small_map_key_11952)
    _7044 = 1;
    else if (IS_ATOM_INT(_the_key_p_12317) && IS_ATOM_INT(_25init_small_map_key_11952))
    _7044 = 0;
    else
    _7044 = (compare(_the_key_p_12317, _25init_small_map_key_11952) == 0);
    if (_7044 == 0)
    {
        _7044 = NOVALUE;
        goto LC; // [665] 741
    }
    else{
        _7044 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12324 = 1;

    /** 			while index_ > 0 with entry do*/
    goto LD; // [677] 718
LE: 
    if (_index__12321 <= 0)
    goto LF; // [682] 757

    /** 				if map_data[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7046 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7046);
    _7047 = (int)*(((s1_ptr)_2)->base + _index__12321);
    _7046 = NOVALUE;
    if (binary_op_a(NOTEQ, _7047, 1)){
        _7047 = NOVALUE;
        goto L10; // [698] 707
    }
    _7047 = NOVALUE;

    /** 					exit*/
    goto LF; // [704] 757
L10: 

    /** 				from_ = index_ + 1*/
    _from__12324 = _index__12321 + 1;

    /** 			  entry*/
LD: 

    /** 				index_ = find(the_key_p, map_data[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7050 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12321 = find_from(_the_key_p_12317, _7050, _from__12324);
    _7050 = NOVALUE;

    /** 			end while*/
    goto LE; // [735] 680
    goto LF; // [738] 757
LC: 

    /** 			index_ = find(the_key_p, map_data[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7052 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12321 = find_from(_the_key_p_12317, _7052, 1);
    _7052 = NOVALUE;
LF: 

    /** 		if index_ = 0 then*/
    if (_index__12321 != 0)
    goto L11; // [761] 963

    /** 			if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _7055 = find_from(_operation_p_12319, _25INIT_OPERATIONS_11947, 1);
    if (_7055 != 0)
    goto L12; // [774] 798
    _7055 = NOVALUE;

    /** 					error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_778_12444);
    _msg_inlined_crash_at_778_12444 = EPrintf(-9999999, _7013, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_778_12444);

    /** end procedure*/
    goto L13; // [792] 795
L13: 
    DeRefi(_msg_inlined_crash_at_778_12444);
    _msg_inlined_crash_at_778_12444 = NOVALUE;
L12: 

    /** 			index_ = find(0, map_data[FREE_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7057 = (int)*(((s1_ptr)_2)->base + 7);
    _index__12321 = find_from(0, _7057, 1);
    _7057 = NOVALUE;

    /** 			if index_ = 0 then*/
    if (_index__12321 != 0)
    goto L14; // [815] 869

    /** 				eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12325);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12316);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12325;
    DeRef(_1);

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_12325);
    _map_data_12325 = _5;

    /** 				convert_to_large_map(the_map_p)*/
    _25convert_to_large_map(_the_map_p_12316);

    /** 				put(the_map_p, the_key_p, the_value_p, operation_p, trigger_p)*/
    DeRef(_7060);
    _7060 = _the_map_p_12316;
    Ref(_the_key_p_12317);
    DeRef(_7061);
    _7061 = _the_key_p_12317;
    Ref(_the_value_p_12318);
    DeRef(_7062);
    _7062 = _the_value_p_12318;
    DeRef(_7063);
    _7063 = _operation_p_12319;
    DeRef(_7064);
    _7064 = _trigger_p_12320;
    _25put(_7060, _7061, _7062, _7063, _7064);
    _7060 = NOVALUE;
    _7061 = NOVALUE;
    _7062 = NOVALUE;
    _7063 = NOVALUE;
    _7064 = NOVALUE;

    /** 				return*/
    DeRef(_the_key_p_12317);
    DeRef(_the_value_p_12318);
    DeRef(_average_length__12323);
    DeRefDS(_map_data_12325);
    _6973 = NOVALUE;
    _7018 = NOVALUE;
    return;
L14: 

    /** 			map_data[KEY_LIST][index_] = the_key_p*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    Ref(_the_key_p_12317);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12321);
    _1 = *(int *)_2;
    *(int *)_2 = _the_key_p_12317;
    DeRef(_1);
    _7065 = NOVALUE;

    /** 			map_data[FREE_LIST][index_] = 1*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12321);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _7067 = NOVALUE;

    /** 			map_data[IN_USE] += 1*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7069 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7069)) {
        _7070 = _7069 + 1;
        if (_7070 > MAXINT){
            _7070 = NewDouble((double)_7070);
        }
    }
    else
    _7070 = binary_op(PLUS, 1, _7069);
    _7069 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7070;
    if( _1 != _7070 ){
        DeRef(_1);
    }
    _7070 = NOVALUE;

    /** 			map_data[ELEMENT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_map_data_12325);
    _7071 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7071)) {
        _7072 = _7071 + 1;
        if (_7072 > MAXINT){
            _7072 = NewDouble((double)_7072);
        }
    }
    else
    _7072 = binary_op(PLUS, 1, _7071);
    _7071 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12325);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12325 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7072;
    if( _1 != _7072 ){
        DeRef(_1);
    }
    _7072 = NOVALUE;

    /** 			if operation_p = APPEND then*/
    if (_operation_p_12319 != 6)
    goto L15; // [931] 942

    /** 				the_value_p = { the_value_p }*/
    _0 = _the_value_p_12318;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_12318);
    *((int *)(_2+4)) = _the_value_p_12318;
    _the_value_p_12318 = MAKE_SEQ(_1);
    DeRef(_0);
L15: 

    /** 			if operation_p != LEAVE then*/
    if (_operation_p_12319 == 8)
    goto L16; // [946] 962

    /** 				operation_p = PUT	-- Initially, nearly everything is a PUT.*/
    _operation_p_12319 = 1;
L16: 
L11: 

    /** 		switch operation_p do*/
    _0 = _operation_p_12319;
    switch ( _0 ){ 

        /** 			case PUT then*/
        case 1:

        /** 				map_data[VALUE_LIST][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        Ref(_the_value_p_12318);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_12318;
        DeRef(_1);
        _7078 = NOVALUE;
        goto L17; // [987] 1195

        /** 			case ADD then*/
        case 2:

        /** 				map_data[VALUE_LIST][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7082 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _7080 = NOVALUE;
        if (IS_ATOM_INT(_7082) && IS_ATOM_INT(_the_value_p_12318)) {
            _7083 = _7082 + _the_value_p_12318;
            if ((long)((unsigned long)_7083 + (unsigned long)HIGH_BITS) >= 0) 
            _7083 = NewDouble((double)_7083);
        }
        else {
            _7083 = binary_op(PLUS, _7082, _the_value_p_12318);
        }
        _7082 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _7083;
        if( _1 != _7083 ){
            DeRef(_1);
        }
        _7083 = NOVALUE;
        _7080 = NOVALUE;
        goto L17; // [1014] 1195

        /** 			case SUBTRACT then*/
        case 3:

        /** 				map_data[VALUE_LIST][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7086 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _7084 = NOVALUE;
        if (IS_ATOM_INT(_7086) && IS_ATOM_INT(_the_value_p_12318)) {
            _7087 = _7086 - _the_value_p_12318;
            if ((long)((unsigned long)_7087 +(unsigned long) HIGH_BITS) >= 0){
                _7087 = NewDouble((double)_7087);
            }
        }
        else {
            _7087 = binary_op(MINUS, _7086, _the_value_p_12318);
        }
        _7086 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _7087;
        if( _1 != _7087 ){
            DeRef(_1);
        }
        _7087 = NOVALUE;
        _7084 = NOVALUE;
        goto L17; // [1041] 1195

        /** 			case MULTIPLY then*/
        case 4:

        /** 				map_data[VALUE_LIST][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7090 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _7088 = NOVALUE;
        if (IS_ATOM_INT(_7090) && IS_ATOM_INT(_the_value_p_12318)) {
            if (_7090 == (short)_7090 && _the_value_p_12318 <= INT15 && _the_value_p_12318 >= -INT15)
            _7091 = _7090 * _the_value_p_12318;
            else
            _7091 = NewDouble(_7090 * (double)_the_value_p_12318);
        }
        else {
            _7091 = binary_op(MULTIPLY, _7090, _the_value_p_12318);
        }
        _7090 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _7091;
        if( _1 != _7091 ){
            DeRef(_1);
        }
        _7091 = NOVALUE;
        _7088 = NOVALUE;
        goto L17; // [1068] 1195

        /** 			case DIVIDE then*/
        case 5:

        /** 				map_data[VALUE_LIST][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7094 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _7092 = NOVALUE;
        if (IS_ATOM_INT(_7094) && IS_ATOM_INT(_the_value_p_12318)) {
            _7095 = (_7094 % _the_value_p_12318) ? NewDouble((double)_7094 / _the_value_p_12318) : (_7094 / _the_value_p_12318);
        }
        else {
            _7095 = binary_op(DIVIDE, _7094, _the_value_p_12318);
        }
        _7094 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _7095;
        if( _1 != _7095 ){
            DeRef(_1);
        }
        _7095 = NOVALUE;
        _7092 = NOVALUE;
        goto L17; // [1095] 1195

        /** 			case APPEND then*/
        case 6:

        /** 				map_data[VALUE_LIST][index_] = append( map_data[VALUE_LIST][index_], the_value_p )*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_map_data_12325);
        _7098 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7098);
        _7099 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _7098 = NOVALUE;
        Ref(_the_value_p_12318);
        Append(&_7100, _7099, _the_value_p_12318);
        _7099 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _7100;
        if( _1 != _7100 ){
            DeRef(_1);
        }
        _7100 = NOVALUE;
        _7096 = NOVALUE;
        goto L17; // [1128] 1195

        /** 			case CONCAT then*/
        case 7:

        /** 				map_data[VALUE_LIST][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12325);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12325 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _7103 = (int)*(((s1_ptr)_2)->base + _index__12321);
        _7101 = NOVALUE;
        if (IS_SEQUENCE(_7103) && IS_ATOM(_the_value_p_12318)) {
            Ref(_the_value_p_12318);
            Append(&_7104, _7103, _the_value_p_12318);
        }
        else if (IS_ATOM(_7103) && IS_SEQUENCE(_the_value_p_12318)) {
            Ref(_7103);
            Prepend(&_7104, _the_value_p_12318, _7103);
        }
        else {
            Concat((object_ptr)&_7104, _7103, _the_value_p_12318);
            _7103 = NOVALUE;
        }
        _7103 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12321);
        _1 = *(int *)_2;
        *(int *)_2 = _7104;
        if( _1 != _7104 ){
            DeRef(_1);
        }
        _7104 = NOVALUE;
        _7101 = NOVALUE;
        goto L17; // [1155] 1195

        /** 			case LEAVE then*/
        case 8:

        /** 				operation_p = operation_p*/
        _operation_p_12319 = _operation_p_12319;
        goto L17; // [1168] 1195

        /** 			case else*/
        default:

        /** 				error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_1176_12507);
        _msg_inlined_crash_at_1176_12507 = EPrintf(-9999999, _7010, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_1176_12507);

        /** end procedure*/
        goto L18; // [1189] 1192
L18: 
        DeRefi(_msg_inlined_crash_at_1176_12507);
        _msg_inlined_crash_at_1176_12507 = NOVALUE;
    ;}L17: 

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12325);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12316);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12325;
    DeRef(_1);

    /** 		return*/
    DeRef(_the_key_p_12317);
    DeRef(_the_value_p_12318);
    DeRef(_average_length__12323);
    DeRefDS(_map_data_12325);
    _6973 = NOVALUE;
    _7018 = NOVALUE;
    return;
LB: 

    /** end procedure*/
    DeRef(_the_key_p_12317);
    DeRef(_the_value_p_12318);
    DeRef(_average_length__12323);
    DeRef(_map_data_12325);
    _6973 = NOVALUE;
    _7018 = NOVALUE;
    return;
    ;
}


void _25nested_put(int _the_map_p_12510, int _the_keys_p_12511, int _the_value_p_12512, int _operation_p_12513, int _trigger_p_12514)
{
    int _temp_map__12515 = NOVALUE;
    int _7117 = NOVALUE;
    int _7116 = NOVALUE;
    int _7115 = NOVALUE;
    int _7114 = NOVALUE;
    int _7113 = NOVALUE;
    int _7112 = NOVALUE;
    int _7111 = NOVALUE;
    int _7109 = NOVALUE;
    int _7108 = NOVALUE;
    int _7107 = NOVALUE;
    int _7105 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_operation_p_12513)) {
        _1 = (long)(DBL_PTR(_operation_p_12513)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_12513)) && (DBL_PTR(_operation_p_12513)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_12513);
        _operation_p_12513 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_12514)) {
        _1 = (long)(DBL_PTR(_trigger_p_12514)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_12514)) && (DBL_PTR(_trigger_p_12514)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_12514);
        _trigger_p_12514 = _1;
    }

    /** 	if length( the_keys_p ) = 1 then*/
    if (IS_SEQUENCE(_the_keys_p_12511)){
            _7105 = SEQ_PTR(_the_keys_p_12511)->length;
    }
    else {
        _7105 = 1;
    }
    if (_7105 != 1)
    goto L1; // [16] 36

    /** 		put( the_map_p, the_keys_p[1], the_value_p, operation_p, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12511);
    _7107 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12510);
    Ref(_7107);
    Ref(_the_value_p_12512);
    _25put(_the_map_p_12510, _7107, _the_value_p_12512, _operation_p_12513, _trigger_p_12514);
    _7107 = NOVALUE;
    goto L2; // [33] 98
L1: 

    /** 		temp_map_ = new_extra( get( the_map_p, the_keys_p[1] ) )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12511);
    _7108 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12510);
    Ref(_7108);
    _7109 = _25get(_the_map_p_12510, _7108, 0);
    _7108 = NOVALUE;
    _0 = _temp_map__12515;
    _temp_map__12515 = _25new_extra(_7109, 690);
    DeRef(_0);
    _7109 = NOVALUE;

    /** 		nested_put( temp_map_, the_keys_p[2..$], the_value_p, operation_p, trigger_p )*/
    if (IS_SEQUENCE(_the_keys_p_12511)){
            _7111 = SEQ_PTR(_the_keys_p_12511)->length;
    }
    else {
        _7111 = 1;
    }
    rhs_slice_target = (object_ptr)&_7112;
    RHS_Slice(_the_keys_p_12511, 2, _7111);
    Ref(_temp_map__12515);
    DeRef(_7113);
    _7113 = _temp_map__12515;
    Ref(_the_value_p_12512);
    DeRef(_7114);
    _7114 = _the_value_p_12512;
    DeRef(_7115);
    _7115 = _operation_p_12513;
    DeRef(_7116);
    _7116 = _trigger_p_12514;
    _25nested_put(_7113, _7112, _7114, _7115, _7116);
    _7113 = NOVALUE;
    _7112 = NOVALUE;
    _7114 = NOVALUE;
    _7115 = NOVALUE;
    _7116 = NOVALUE;

    /** 		put( the_map_p, the_keys_p[1], temp_map_, PUT, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12511);
    _7117 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12510);
    Ref(_7117);
    Ref(_temp_map__12515);
    _25put(_the_map_p_12510, _7117, _temp_map__12515, 1, _trigger_p_12514);
    _7117 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_the_map_p_12510);
    DeRefDS(_the_keys_p_12511);
    DeRef(_the_value_p_12512);
    DeRef(_temp_map__12515);
    return;
    ;
}


void _25remove(int _the_map_p_12533, int _the_key_p_12534)
{
    int _index__12535 = NOVALUE;
    int _bucket__12536 = NOVALUE;
    int _temp_map__12537 = NOVALUE;
    int _from__12538 = NOVALUE;
    int _7183 = NOVALUE;
    int _7182 = NOVALUE;
    int _7181 = NOVALUE;
    int _7180 = NOVALUE;
    int _7178 = NOVALUE;
    int _7176 = NOVALUE;
    int _7174 = NOVALUE;
    int _7172 = NOVALUE;
    int _7171 = NOVALUE;
    int _7169 = NOVALUE;
    int _7166 = NOVALUE;
    int _7165 = NOVALUE;
    int _7164 = NOVALUE;
    int _7163 = NOVALUE;
    int _7162 = NOVALUE;
    int _7161 = NOVALUE;
    int _7160 = NOVALUE;
    int _7159 = NOVALUE;
    int _7158 = NOVALUE;
    int _7157 = NOVALUE;
    int _7156 = NOVALUE;
    int _7155 = NOVALUE;
    int _7154 = NOVALUE;
    int _7152 = NOVALUE;
    int _7151 = NOVALUE;
    int _7150 = NOVALUE;
    int _7149 = NOVALUE;
    int _7148 = NOVALUE;
    int _7147 = NOVALUE;
    int _7146 = NOVALUE;
    int _7145 = NOVALUE;
    int _7144 = NOVALUE;
    int _7143 = NOVALUE;
    int _7142 = NOVALUE;
    int _7140 = NOVALUE;
    int _7138 = NOVALUE;
    int _7136 = NOVALUE;
    int _7135 = NOVALUE;
    int _7134 = NOVALUE;
    int _7132 = NOVALUE;
    int _7131 = NOVALUE;
    int _7130 = NOVALUE;
    int _7129 = NOVALUE;
    int _7128 = NOVALUE;
    int _7125 = NOVALUE;
    int _7124 = NOVALUE;
    int _7122 = NOVALUE;
    int _7121 = NOVALUE;
    int _7119 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12537);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12533)){
        _temp_map__12537 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12533)->dbl));
    }
    else{
        _temp_map__12537 = (int)*(((s1_ptr)_2)->base + _the_map_p_12533);
    }
    Ref(_temp_map__12537);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12533))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12533)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12533);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7119 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7119, 76)){
        _7119 = NOVALUE;
        goto L1; // [27] 318
    }
    _7119 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7121 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7121)){
            _7122 = SEQ_PTR(_7121)->length;
    }
    else {
        _7122 = 1;
    }
    _7121 = NOVALUE;
    Ref(_the_key_p_12534);
    _bucket__12536 = _25calc_hash(_the_key_p_12534, _7122);
    _7122 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12536)) {
        _1 = (long)(DBL_PTR(_bucket__12536)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12536)) && (DBL_PTR(_bucket__12536)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12536);
        _bucket__12536 = _1;
    }

    /** 		index_ = find(the_key_p, temp_map_[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7124 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7124);
    _7125 = (int)*(((s1_ptr)_2)->base + _bucket__12536);
    _7124 = NOVALUE;
    _index__12535 = find_from(_the_key_p_12534, _7125, 1);
    _7125 = NOVALUE;

    /** 		if index_ != 0 then*/
    if (_index__12535 == 0)
    goto L2; // [72] 464

    /** 			temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7128 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7128)) {
        _7129 = _7128 - 1;
        if ((long)((unsigned long)_7129 +(unsigned long) HIGH_BITS) >= 0){
            _7129 = NewDouble((double)_7129);
        }
    }
    else {
        _7129 = binary_op(MINUS, _7128, 1);
    }
    _7128 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7129;
    if( _1 != _7129 ){
        DeRef(_1);
    }
    _7129 = NOVALUE;

    /** 			if length(temp_map_[KEY_BUCKETS][bucket_]) = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7130 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7130);
    _7131 = (int)*(((s1_ptr)_2)->base + _bucket__12536);
    _7130 = NOVALUE;
    if (IS_SEQUENCE(_7131)){
            _7132 = SEQ_PTR(_7131)->length;
    }
    else {
        _7132 = 1;
    }
    _7131 = NOVALUE;
    if (_7132 != 1)
    goto L3; // [107] 156

    /** 				temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7134 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7134)) {
        _7135 = _7134 - 1;
        if ((long)((unsigned long)_7135 +(unsigned long) HIGH_BITS) >= 0){
            _7135 = NewDouble((double)_7135);
        }
    }
    else {
        _7135 = binary_op(MINUS, _7134, 1);
    }
    _7134 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7135;
    if( _1 != _7135 ){
        DeRef(_1);
    }
    _7135 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12536);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7136 = NOVALUE;

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12536);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7138 = NOVALUE;
    goto L4; // [153] 273
L3: 

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = temp_map_[VALUE_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7142 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7142);
    _7143 = (int)*(((s1_ptr)_2)->base + _bucket__12536);
    _7142 = NOVALUE;
    _7144 = _index__12535 - 1;
    rhs_slice_target = (object_ptr)&_7145;
    RHS_Slice(_7143, 1, _7144);
    _7143 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7146 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7146);
    _7147 = (int)*(((s1_ptr)_2)->base + _bucket__12536);
    _7146 = NOVALUE;
    _7148 = _index__12535 + 1;
    if (_7148 > MAXINT){
        _7148 = NewDouble((double)_7148);
    }
    if (IS_SEQUENCE(_7147)){
            _7149 = SEQ_PTR(_7147)->length;
    }
    else {
        _7149 = 1;
    }
    rhs_slice_target = (object_ptr)&_7150;
    RHS_Slice(_7147, _7148, _7149);
    _7147 = NOVALUE;
    Concat((object_ptr)&_7151, _7145, _7150);
    DeRefDS(_7145);
    _7145 = NOVALUE;
    DeRef(_7145);
    _7145 = NOVALUE;
    DeRefDS(_7150);
    _7150 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12536);
    _1 = *(int *)_2;
    *(int *)_2 = _7151;
    if( _1 != _7151 ){
        DeRef(_1);
    }
    _7151 = NOVALUE;
    _7140 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = temp_map_[KEY_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7154 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7154);
    _7155 = (int)*(((s1_ptr)_2)->base + _bucket__12536);
    _7154 = NOVALUE;
    _7156 = _index__12535 - 1;
    rhs_slice_target = (object_ptr)&_7157;
    RHS_Slice(_7155, 1, _7156);
    _7155 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7158 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7158);
    _7159 = (int)*(((s1_ptr)_2)->base + _bucket__12536);
    _7158 = NOVALUE;
    _7160 = _index__12535 + 1;
    if (_7160 > MAXINT){
        _7160 = NewDouble((double)_7160);
    }
    if (IS_SEQUENCE(_7159)){
            _7161 = SEQ_PTR(_7159)->length;
    }
    else {
        _7161 = 1;
    }
    rhs_slice_target = (object_ptr)&_7162;
    RHS_Slice(_7159, _7160, _7161);
    _7159 = NOVALUE;
    Concat((object_ptr)&_7163, _7157, _7162);
    DeRefDS(_7157);
    _7157 = NOVALUE;
    DeRef(_7157);
    _7157 = NOVALUE;
    DeRefDS(_7162);
    _7162 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12536);
    _1 = *(int *)_2;
    *(int *)_2 = _7163;
    if( _1 != _7163 ){
        DeRef(_1);
    }
    _7163 = NOVALUE;
    _7152 = NOVALUE;
L4: 

    /** 			if temp_map_[ELEMENT_COUNT] < floor(51 * threshold_size / 100) then*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7164 = (int)*(((s1_ptr)_2)->base + 2);
    if (_25threshold_size_11951 <= INT15 && _25threshold_size_11951 >= -INT15)
    _7165 = 51 * _25threshold_size_11951;
    else
    _7165 = NewDouble(51 * (double)_25threshold_size_11951);
    if (IS_ATOM_INT(_7165)) {
        if (100 > 0 && _7165 >= 0) {
            _7166 = _7165 / 100;
        }
        else {
            temp_dbl = floor((double)_7165 / (double)100);
            if (_7165 != MININT)
            _7166 = (long)temp_dbl;
            else
            _7166 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7165, 100);
        _7166 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_7165);
    _7165 = NOVALUE;
    if (binary_op_a(GREATEREQ, _7164, _7166)){
        _7164 = NOVALUE;
        DeRef(_7166);
        _7166 = NOVALUE;
        goto L2; // [291] 464
    }
    _7164 = NOVALUE;
    DeRef(_7166);
    _7166 = NOVALUE;

    /** 				eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12537);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12533))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12533)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12533);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12537;
    DeRef(_1);

    /** 				convert_to_small_map(the_map_p)*/
    Ref(_the_map_p_12533);
    _25convert_to_small_map(_the_map_p_12533);

    /** 				return*/
    DeRef(_the_map_p_12533);
    DeRef(_the_key_p_12534);
    DeRefDS(_temp_map__12537);
    _7121 = NOVALUE;
    _7131 = NOVALUE;
    DeRef(_7144);
    _7144 = NOVALUE;
    DeRef(_7156);
    _7156 = NOVALUE;
    DeRef(_7148);
    _7148 = NOVALUE;
    DeRef(_7160);
    _7160 = NOVALUE;
    return;
    goto L2; // [315] 464
L1: 

    /** 		from_ = 1*/
    _from__12538 = 1;

    /** 		while from_ > 0 do*/
L5: 
    if (_from__12538 <= 0)
    goto L6; // [330] 463

    /** 			index_ = find(the_key_p, temp_map_[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7169 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12535 = find_from(_the_key_p_12534, _7169, _from__12538);
    _7169 = NOVALUE;

    /** 			if index_ then*/
    if (_index__12535 == 0)
    {
        goto L6; // [351] 463
    }
    else{
    }

    /** 				if temp_map_[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7171 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7171);
    _7172 = (int)*(((s1_ptr)_2)->base + _index__12535);
    _7171 = NOVALUE;
    if (binary_op_a(NOTEQ, _7172, 1)){
        _7172 = NOVALUE;
        goto L7; // [366] 450
    }
    _7172 = NOVALUE;

    /** 					temp_map_[FREE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12535);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7174 = NOVALUE;

    /** 					temp_map_[KEY_LIST][index_] = init_small_map_key*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_25init_small_map_key_11952);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12535);
    _1 = *(int *)_2;
    *(int *)_2 = _25init_small_map_key_11952;
    DeRef(_1);
    _7176 = NOVALUE;

    /** 					temp_map_[VALUE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12535);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7178 = NOVALUE;

    /** 					temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7180 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7180)) {
        _7181 = _7180 - 1;
        if ((long)((unsigned long)_7181 +(unsigned long) HIGH_BITS) >= 0){
            _7181 = NewDouble((double)_7181);
        }
    }
    else {
        _7181 = binary_op(MINUS, _7180, 1);
    }
    _7180 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7181;
    if( _1 != _7181 ){
        DeRef(_1);
    }
    _7181 = NOVALUE;

    /** 					temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12537);
    _7182 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7182)) {
        _7183 = _7182 - 1;
        if ((long)((unsigned long)_7183 +(unsigned long) HIGH_BITS) >= 0){
            _7183 = NewDouble((double)_7183);
        }
    }
    else {
        _7183 = binary_op(MINUS, _7182, 1);
    }
    _7182 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12537);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12537 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7183;
    if( _1 != _7183 ){
        DeRef(_1);
    }
    _7183 = NOVALUE;
    goto L7; // [442] 450

    /** 				exit*/
    goto L6; // [447] 463
L7: 

    /** 			from_ = index_ + 1*/
    _from__12538 = _index__12535 + 1;

    /** 		end while*/
    goto L5; // [460] 330
L6: 
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12537);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12533))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12533)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12533);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12537;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_12533);
    DeRef(_the_key_p_12534);
    DeRefDS(_temp_map__12537);
    _7121 = NOVALUE;
    _7131 = NOVALUE;
    DeRef(_7144);
    _7144 = NOVALUE;
    DeRef(_7156);
    _7156 = NOVALUE;
    DeRef(_7148);
    _7148 = NOVALUE;
    DeRef(_7160);
    _7160 = NOVALUE;
    return;
    ;
}


void _25clear(int _the_map_p_12619)
{
    int _temp_map__12620 = NOVALUE;
    int _7202 = NOVALUE;
    int _7201 = NOVALUE;
    int _7200 = NOVALUE;
    int _7199 = NOVALUE;
    int _7198 = NOVALUE;
    int _7197 = NOVALUE;
    int _7196 = NOVALUE;
    int _7195 = NOVALUE;
    int _7194 = NOVALUE;
    int _7193 = NOVALUE;
    int _7192 = NOVALUE;
    int _7191 = NOVALUE;
    int _7190 = NOVALUE;
    int _7189 = NOVALUE;
    int _7188 = NOVALUE;
    int _7186 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12620);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12619)){
        _temp_map__12620 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12619)->dbl));
    }
    else{
        _temp_map__12620 = (int)*(((s1_ptr)_2)->base + _the_map_p_12619);
    }
    Ref(_temp_map__12620);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    _7186 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7186, 76)){
        _7186 = NOVALUE;
        goto L1; // [19] 84
    }
    _7186 = NOVALUE;

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_BUCKETS] = repeat({}, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    _7188 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7188)){
            _7189 = SEQ_PTR(_7188)->length;
    }
    else {
        _7189 = 1;
    }
    _7188 = NOVALUE;
    _7190 = Repeat(_5, _7189);
    _7189 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7190;
    if( _1 != _7190 ){
        DeRef(_1);
    }
    _7190 = NOVALUE;

    /** 		temp_map_[VALUE_BUCKETS] = repeat({}, length(temp_map_[VALUE_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    _7191 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7191)){
            _7192 = SEQ_PTR(_7191)->length;
    }
    else {
        _7192 = 1;
    }
    _7191 = NOVALUE;
    _7193 = Repeat(_5, _7192);
    _7192 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7193;
    if( _1 != _7193 ){
        DeRef(_1);
    }
    _7193 = NOVALUE;
    goto L2; // [81] 164
L1: 

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_LIST] = repeat(init_small_map_key, length(temp_map_[KEY_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    _7194 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7194)){
            _7195 = SEQ_PTR(_7194)->length;
    }
    else {
        _7195 = 1;
    }
    _7194 = NOVALUE;
    _7196 = Repeat(_25init_small_map_key_11952, _7195);
    _7195 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7196;
    if( _1 != _7196 ){
        DeRef(_1);
    }
    _7196 = NOVALUE;

    /** 		temp_map_[VALUE_LIST] = repeat(0, length(temp_map_[VALUE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    _7197 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7197)){
            _7198 = SEQ_PTR(_7197)->length;
    }
    else {
        _7198 = 1;
    }
    _7197 = NOVALUE;
    _7199 = Repeat(0, _7198);
    _7198 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7199;
    if( _1 != _7199 ){
        DeRef(_1);
    }
    _7199 = NOVALUE;

    /** 		temp_map_[FREE_LIST] = repeat(0, length(temp_map_[FREE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12620);
    _7200 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7200)){
            _7201 = SEQ_PTR(_7200)->length;
    }
    else {
        _7201 = 1;
    }
    _7200 = NOVALUE;
    _7202 = Repeat(0, _7201);
    _7201 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12620);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12620 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _7202;
    if( _1 != _7202 ){
        DeRef(_1);
    }
    _7202 = NOVALUE;
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12620);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12619))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12619)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12619);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12620;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_12619);
    DeRefDS(_temp_map__12620);
    _7188 = NOVALUE;
    _7191 = NOVALUE;
    _7194 = NOVALUE;
    _7197 = NOVALUE;
    _7200 = NOVALUE;
    return;
    ;
}


int _25size(int _the_map_p_12643)
{
    int _7204 = NOVALUE;
    int _7203 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12643)){
        _7203 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12643)->dbl));
    }
    else{
        _7203 = (int)*(((s1_ptr)_2)->base + _the_map_p_12643);
    }
    _2 = (int)SEQ_PTR(_7203);
    _7204 = (int)*(((s1_ptr)_2)->base + 2);
    _7203 = NOVALUE;
    Ref(_7204);
    DeRef(_the_map_p_12643);
    return _7204;
    ;
}


int _25statistics(int _the_map_p_12661)
{
    int _statistic_set__12662 = NOVALUE;
    int _lengths__12663 = NOVALUE;
    int _length__12664 = NOVALUE;
    int _temp_map__12665 = NOVALUE;
    int _7241 = NOVALUE;
    int _7240 = NOVALUE;
    int _7239 = NOVALUE;
    int _7238 = NOVALUE;
    int _7237 = NOVALUE;
    int _7236 = NOVALUE;
    int _7235 = NOVALUE;
    int _7234 = NOVALUE;
    int _7233 = NOVALUE;
    int _7232 = NOVALUE;
    int _7231 = NOVALUE;
    int _7230 = NOVALUE;
    int _7227 = NOVALUE;
    int _7225 = NOVALUE;
    int _7222 = NOVALUE;
    int _7221 = NOVALUE;
    int _7220 = NOVALUE;
    int _7219 = NOVALUE;
    int _7217 = NOVALUE;
    int _7216 = NOVALUE;
    int _7215 = NOVALUE;
    int _7214 = NOVALUE;
    int _7212 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12665);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12661)){
        _temp_map__12665 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12661)->dbl));
    }
    else{
        _temp_map__12665 = (int)*(((s1_ptr)_2)->base + _the_map_p_12661);
    }
    Ref(_temp_map__12665);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7212 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7212, 76)){
        _7212 = NOVALUE;
        goto L1; // [19] 196
    }
    _7212 = NOVALUE;

    /** 		statistic_set_ = { */
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7214 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7215 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7216 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7216)){
            _7217 = SEQ_PTR(_7216)->length;
    }
    else {
        _7217 = 1;
    }
    _7216 = NOVALUE;
    _0 = _statistic_set__12662;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7214);
    *((int *)(_2+4)) = _7214;
    Ref(_7215);
    *((int *)(_2+8)) = _7215;
    *((int *)(_2+12)) = _7217;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 1073741823;
    *((int *)(_2+24)) = 0;
    *((int *)(_2+28)) = 0;
    _statistic_set__12662 = MAKE_SEQ(_1);
    DeRef(_0);
    _7217 = NOVALUE;
    _7215 = NOVALUE;
    _7214 = NOVALUE;

    /** 		lengths_ = {}*/
    RefDS(_5);
    DeRefi(_lengths__12663);
    _lengths__12663 = _5;

    /** 		for i = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7219 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7219)){
            _7220 = SEQ_PTR(_7219)->length;
    }
    else {
        _7220 = 1;
    }
    _7219 = NOVALUE;
    {
        int _i_12676;
        _i_12676 = 1;
L2: 
        if (_i_12676 > _7220){
            goto L3; // [74] 160
        }

        /** 			length_ = length(temp_map_[KEY_BUCKETS][i])*/
        _2 = (int)SEQ_PTR(_temp_map__12665);
        _7221 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7221);
        _7222 = (int)*(((s1_ptr)_2)->base + _i_12676);
        _7221 = NOVALUE;
        if (IS_SEQUENCE(_7222)){
                _length__12664 = SEQ_PTR(_7222)->length;
        }
        else {
            _length__12664 = 1;
        }
        _7222 = NOVALUE;

        /** 			if length_ > 0 then*/
        if (_length__12664 <= 0)
        goto L4; // [100] 153

        /** 				if length_ > statistic_set_[LARGEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__12662);
        _7225 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(LESSEQ, _length__12664, _7225)){
            _7225 = NOVALUE;
            goto L5; // [112] 125
        }
        _7225 = NOVALUE;

        /** 					statistic_set_[LARGEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__12662);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__12662 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 4);
        _1 = *(int *)_2;
        *(int *)_2 = _length__12664;
        DeRef(_1);
L5: 

        /** 				if length_ < statistic_set_[SMALLEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__12662);
        _7227 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(GREATEREQ, _length__12664, _7227)){
            _7227 = NOVALUE;
            goto L6; // [133] 146
        }
        _7227 = NOVALUE;

        /** 					statistic_set_[SMALLEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__12662);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__12662 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _length__12664;
        DeRef(_1);
L6: 

        /** 				lengths_ &= length_*/
        Append(&_lengths__12663, _lengths__12663, _length__12664);
L4: 

        /** 		end for*/
        _i_12676 = _i_12676 + 1;
        goto L2; // [155] 81
L3: 
        ;
    }

    /** 		statistic_set_[AVERAGE_BUCKET] = stats:average(lengths_)*/
    RefDS(_lengths__12663);
    _7230 = _28average(_lengths__12663, 1);
    _2 = (int)SEQ_PTR(_statistic_set__12662);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__12662 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7230;
    if( _1 != _7230 ){
        DeRef(_1);
    }
    _7230 = NOVALUE;

    /** 		statistic_set_[STDEV_BUCKET] = stats:stdev(lengths_)*/
    RefDS(_lengths__12663);
    _7231 = _28stdev(_lengths__12663, 1, 2);
    _2 = (int)SEQ_PTR(_statistic_set__12662);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__12662 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _7231;
    if( _1 != _7231 ){
        DeRef(_1);
    }
    _7231 = NOVALUE;
    goto L7; // [193] 257
L1: 

    /** 		statistic_set_ = {*/
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7232 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7233 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7234 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7234)){
            _7235 = SEQ_PTR(_7234)->length;
    }
    else {
        _7235 = 1;
    }
    _7234 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7236 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7236)){
            _7237 = SEQ_PTR(_7236)->length;
    }
    else {
        _7237 = 1;
    }
    _7236 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7238 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7238)){
            _7239 = SEQ_PTR(_7238)->length;
    }
    else {
        _7239 = 1;
    }
    _7238 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12665);
    _7240 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7240)){
            _7241 = SEQ_PTR(_7240)->length;
    }
    else {
        _7241 = 1;
    }
    _7240 = NOVALUE;
    _0 = _statistic_set__12662;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7232);
    *((int *)(_2+4)) = _7232;
    Ref(_7233);
    *((int *)(_2+8)) = _7233;
    *((int *)(_2+12)) = _7235;
    *((int *)(_2+16)) = _7237;
    *((int *)(_2+20)) = _7239;
    *((int *)(_2+24)) = _7241;
    *((int *)(_2+28)) = 0;
    _statistic_set__12662 = MAKE_SEQ(_1);
    DeRef(_0);
    _7241 = NOVALUE;
    _7239 = NOVALUE;
    _7237 = NOVALUE;
    _7235 = NOVALUE;
    _7233 = NOVALUE;
    _7232 = NOVALUE;
L7: 

    /** 	return statistic_set_*/
    DeRef(_the_map_p_12661);
    DeRefi(_lengths__12663);
    DeRef(_temp_map__12665);
    _7219 = NOVALUE;
    _7222 = NOVALUE;
    _7216 = NOVALUE;
    _7234 = NOVALUE;
    _7236 = NOVALUE;
    _7238 = NOVALUE;
    _7240 = NOVALUE;
    return _statistic_set__12662;
    ;
}


int _25keys(int _the_map_p_12707, int _sorted_result_12708)
{
    int _buckets__12709 = NOVALUE;
    int _current_bucket__12710 = NOVALUE;
    int _results__12711 = NOVALUE;
    int _pos__12712 = NOVALUE;
    int _temp_map__12713 = NOVALUE;
    int _7266 = NOVALUE;
    int _7264 = NOVALUE;
    int _7263 = NOVALUE;
    int _7261 = NOVALUE;
    int _7260 = NOVALUE;
    int _7259 = NOVALUE;
    int _7258 = NOVALUE;
    int _7256 = NOVALUE;
    int _7255 = NOVALUE;
    int _7254 = NOVALUE;
    int _7253 = NOVALUE;
    int _7251 = NOVALUE;
    int _7249 = NOVALUE;
    int _7246 = NOVALUE;
    int _7244 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sorted_result_12708)) {
        _1 = (long)(DBL_PTR(_sorted_result_12708)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_12708)) && (DBL_PTR(_sorted_result_12708)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_12708);
        _sorted_result_12708 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12713);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12707)){
        _temp_map__12713 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12707)->dbl));
    }
    else{
        _temp_map__12713 = (int)*(((s1_ptr)_2)->base + _the_map_p_12707);
    }
    Ref(_temp_map__12713);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__12713);
    _7244 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12711);
    _results__12711 = Repeat(0, _7244);
    _7244 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12712 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12713);
    _7246 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7246, 76)){
        _7246 = NOVALUE;
        goto L1; // [42] 125
    }
    _7246 = NOVALUE;

    /** 		buckets_ = temp_map_[KEY_BUCKETS]*/
    DeRef(_buckets__12709);
    _2 = (int)SEQ_PTR(_temp_map__12713);
    _buckets__12709 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_buckets__12709);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__12709)){
            _7249 = SEQ_PTR(_buckets__12709)->length;
    }
    else {
        _7249 = 1;
    }
    {
        int _index_12722;
        _index_12722 = 1;
L2: 
        if (_index_12722 > _7249){
            goto L3; // [61] 122
        }

        /** 			current_bucket_ = buckets_[index]*/
        DeRef(_current_bucket__12710);
        _2 = (int)SEQ_PTR(_buckets__12709);
        _current_bucket__12710 = (int)*(((s1_ptr)_2)->base + _index_12722);
        Ref(_current_bucket__12710);

        /** 			if length(current_bucket_) > 0 then*/
        if (IS_SEQUENCE(_current_bucket__12710)){
                _7251 = SEQ_PTR(_current_bucket__12710)->length;
        }
        else {
            _7251 = 1;
        }
        if (_7251 <= 0)
        goto L4; // [81] 115

        /** 				results_[pos_ .. pos_ + length(current_bucket_) - 1] = current_bucket_*/
        if (IS_SEQUENCE(_current_bucket__12710)){
                _7253 = SEQ_PTR(_current_bucket__12710)->length;
        }
        else {
            _7253 = 1;
        }
        _7254 = _pos__12712 + _7253;
        if ((long)((unsigned long)_7254 + (unsigned long)HIGH_BITS) >= 0) 
        _7254 = NewDouble((double)_7254);
        _7253 = NOVALUE;
        if (IS_ATOM_INT(_7254)) {
            _7255 = _7254 - 1;
        }
        else {
            _7255 = NewDouble(DBL_PTR(_7254)->dbl - (double)1);
        }
        DeRef(_7254);
        _7254 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__12711;
        AssignSlice(_pos__12712, _7255, _current_bucket__12710);
        DeRef(_7255);
        _7255 = NOVALUE;

        /** 				pos_ += length(current_bucket_)*/
        if (IS_SEQUENCE(_current_bucket__12710)){
                _7256 = SEQ_PTR(_current_bucket__12710)->length;
        }
        else {
            _7256 = 1;
        }
        _pos__12712 = _pos__12712 + _7256;
        _7256 = NOVALUE;
L4: 

        /** 		end for*/
        _index_12722 = _index_12722 + 1;
        goto L2; // [117] 68
L3: 
        ;
    }
    goto L5; // [122] 192
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12713);
    _7258 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7258)){
            _7259 = SEQ_PTR(_7258)->length;
    }
    else {
        _7259 = 1;
    }
    _7258 = NOVALUE;
    {
        int _index_12735;
        _index_12735 = 1;
L6: 
        if (_index_12735 > _7259){
            goto L7; // [136] 191
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12713);
        _7260 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7260);
        _7261 = (int)*(((s1_ptr)_2)->base + _index_12735);
        _7260 = NOVALUE;
        if (binary_op_a(EQUALS, _7261, 0)){
            _7261 = NOVALUE;
            goto L8; // [155] 184
        }
        _7261 = NOVALUE;

        /** 				results_[pos_] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12713);
        _7263 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7263);
        _7264 = (int)*(((s1_ptr)_2)->base + _index_12735);
        _7263 = NOVALUE;
        Ref(_7264);
        _2 = (int)SEQ_PTR(_results__12711);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12711 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__12712);
        _1 = *(int *)_2;
        *(int *)_2 = _7264;
        if( _1 != _7264 ){
            DeRef(_1);
        }
        _7264 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12712 = _pos__12712 + 1;
L8: 

        /** 		end for*/
        _index_12735 = _index_12735 + 1;
        goto L6; // [186] 143
L7: 
        ;
    }
L5: 

    /** 	if sorted_result then*/
    if (_sorted_result_12708 == 0)
    {
        goto L9; // [194] 211
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__12711);
    _7266 = _7sort(_results__12711, 1);
    DeRef(_the_map_p_12707);
    DeRef(_buckets__12709);
    DeRef(_current_bucket__12710);
    DeRefDS(_results__12711);
    DeRef(_temp_map__12713);
    _7258 = NOVALUE;
    return _7266;
    goto LA; // [208] 218
L9: 

    /** 		return results_*/
    DeRef(_the_map_p_12707);
    DeRef(_buckets__12709);
    DeRef(_current_bucket__12710);
    DeRef(_temp_map__12713);
    _7258 = NOVALUE;
    DeRef(_7266);
    _7266 = NOVALUE;
    return _results__12711;
LA: 
    ;
}


int _25values(int _the_map_12750, int _keys_12751, int _default_values_12752)
{
    int _buckets__12776 = NOVALUE;
    int _bucket__12777 = NOVALUE;
    int _results__12778 = NOVALUE;
    int _pos__12779 = NOVALUE;
    int _temp_map__12780 = NOVALUE;
    int _7306 = NOVALUE;
    int _7305 = NOVALUE;
    int _7303 = NOVALUE;
    int _7302 = NOVALUE;
    int _7301 = NOVALUE;
    int _7300 = NOVALUE;
    int _7298 = NOVALUE;
    int _7297 = NOVALUE;
    int _7296 = NOVALUE;
    int _7295 = NOVALUE;
    int _7293 = NOVALUE;
    int _7291 = NOVALUE;
    int _7288 = NOVALUE;
    int _7286 = NOVALUE;
    int _7284 = NOVALUE;
    int _7283 = NOVALUE;
    int _7282 = NOVALUE;
    int _7281 = NOVALUE;
    int _7279 = NOVALUE;
    int _7278 = NOVALUE;
    int _7277 = NOVALUE;
    int _7276 = NOVALUE;
    int _7275 = NOVALUE;
    int _7274 = NOVALUE;
    int _7272 = NOVALUE;
    int _7271 = NOVALUE;
    int _7269 = NOVALUE;
    int _7268 = NOVALUE;
    int _7267 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(keys) then*/
    _7267 = IS_SEQUENCE(_keys_12751);
    if (_7267 == 0)
    {
        _7267 = NOVALUE;
        goto L1; // [6] 116
    }
    else{
        _7267 = NOVALUE;
    }

    /** 		if atom(default_values) then*/
    _7268 = IS_ATOM(_default_values_12752);
    if (_7268 == 0)
    {
        _7268 = NOVALUE;
        goto L2; // [14] 29
    }
    else{
        _7268 = NOVALUE;
    }

    /** 			default_values = repeat(default_values, length(keys))*/
    if (IS_SEQUENCE(_keys_12751)){
            _7269 = SEQ_PTR(_keys_12751)->length;
    }
    else {
        _7269 = 1;
    }
    _0 = _default_values_12752;
    _default_values_12752 = Repeat(_default_values_12752, _7269);
    DeRef(_0);
    _7269 = NOVALUE;
    goto L3; // [26] 70
L2: 

    /** 		elsif length(default_values) < length(keys) then*/
    if (IS_SEQUENCE(_default_values_12752)){
            _7271 = SEQ_PTR(_default_values_12752)->length;
    }
    else {
        _7271 = 1;
    }
    if (IS_SEQUENCE(_keys_12751)){
            _7272 = SEQ_PTR(_keys_12751)->length;
    }
    else {
        _7272 = 1;
    }
    if (_7271 >= _7272)
    goto L4; // [37] 69

    /** 			default_values &= repeat(default_values[$], length(keys) - length(default_values))*/
    if (IS_SEQUENCE(_default_values_12752)){
            _7274 = SEQ_PTR(_default_values_12752)->length;
    }
    else {
        _7274 = 1;
    }
    _2 = (int)SEQ_PTR(_default_values_12752);
    _7275 = (int)*(((s1_ptr)_2)->base + _7274);
    if (IS_SEQUENCE(_keys_12751)){
            _7276 = SEQ_PTR(_keys_12751)->length;
    }
    else {
        _7276 = 1;
    }
    if (IS_SEQUENCE(_default_values_12752)){
            _7277 = SEQ_PTR(_default_values_12752)->length;
    }
    else {
        _7277 = 1;
    }
    _7278 = _7276 - _7277;
    _7276 = NOVALUE;
    _7277 = NOVALUE;
    _7279 = Repeat(_7275, _7278);
    _7275 = NOVALUE;
    _7278 = NOVALUE;
    if (IS_SEQUENCE(_default_values_12752) && IS_ATOM(_7279)) {
    }
    else if (IS_ATOM(_default_values_12752) && IS_SEQUENCE(_7279)) {
        Ref(_default_values_12752);
        Prepend(&_default_values_12752, _7279, _default_values_12752);
    }
    else {
        Concat((object_ptr)&_default_values_12752, _default_values_12752, _7279);
    }
    DeRefDS(_7279);
    _7279 = NOVALUE;
L4: 
L3: 

    /** 		for i = 1 to length(keys) do*/
    if (IS_SEQUENCE(_keys_12751)){
            _7281 = SEQ_PTR(_keys_12751)->length;
    }
    else {
        _7281 = 1;
    }
    {
        int _i_12771;
        _i_12771 = 1;
L5: 
        if (_i_12771 > _7281){
            goto L6; // [75] 109
        }

        /** 			keys[i] = get(the_map, keys[i], default_values[i])*/
        _2 = (int)SEQ_PTR(_keys_12751);
        _7282 = (int)*(((s1_ptr)_2)->base + _i_12771);
        _2 = (int)SEQ_PTR(_default_values_12752);
        _7283 = (int)*(((s1_ptr)_2)->base + _i_12771);
        Ref(_the_map_12750);
        Ref(_7282);
        Ref(_7283);
        _7284 = _25get(_the_map_12750, _7282, _7283);
        _7282 = NOVALUE;
        _7283 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys_12751);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys_12751 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12771);
        _1 = *(int *)_2;
        *(int *)_2 = _7284;
        if( _1 != _7284 ){
            DeRef(_1);
        }
        _7284 = NOVALUE;

        /** 		end for*/
        _i_12771 = _i_12771 + 1;
        goto L5; // [104] 82
L6: 
        ;
    }

    /** 		return keys*/
    DeRef(_the_map_12750);
    DeRef(_default_values_12752);
    DeRef(_buckets__12776);
    DeRef(_bucket__12777);
    DeRef(_results__12778);
    DeRef(_temp_map__12780);
    return _keys_12751;
L1: 

    /** 	sequence buckets_*/

    /** 	sequence bucket_*/

    /** 	sequence results_*/

    /** 	integer pos_*/

    /** 	sequence temp_map_*/

    /** 	temp_map_ = eumem:ram_space[the_map]*/
    DeRef(_temp_map__12780);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_12750)){
        _temp_map__12780 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_12750)->dbl));
    }
    else{
        _temp_map__12780 = (int)*(((s1_ptr)_2)->base + _the_map_12750);
    }
    Ref(_temp_map__12780);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__12780);
    _7286 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12778);
    _results__12778 = Repeat(0, _7286);
    _7286 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12779 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12780);
    _7288 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7288, 76)){
        _7288 = NOVALUE;
        goto L7; // [163] 246
    }
    _7288 = NOVALUE;

    /** 		buckets_ = temp_map_[VALUE_BUCKETS]*/
    DeRef(_buckets__12776);
    _2 = (int)SEQ_PTR(_temp_map__12780);
    _buckets__12776 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_buckets__12776);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__12776)){
            _7291 = SEQ_PTR(_buckets__12776)->length;
    }
    else {
        _7291 = 1;
    }
    {
        int _index_12789;
        _index_12789 = 1;
L8: 
        if (_index_12789 > _7291){
            goto L9; // [182] 243
        }

        /** 			bucket_ = buckets_[index]*/
        DeRef(_bucket__12777);
        _2 = (int)SEQ_PTR(_buckets__12776);
        _bucket__12777 = (int)*(((s1_ptr)_2)->base + _index_12789);
        Ref(_bucket__12777);

        /** 			if length(bucket_) > 0 then*/
        if (IS_SEQUENCE(_bucket__12777)){
                _7293 = SEQ_PTR(_bucket__12777)->length;
        }
        else {
            _7293 = 1;
        }
        if (_7293 <= 0)
        goto LA; // [202] 236

        /** 				results_[pos_ .. pos_ + length(bucket_) - 1] = bucket_*/
        if (IS_SEQUENCE(_bucket__12777)){
                _7295 = SEQ_PTR(_bucket__12777)->length;
        }
        else {
            _7295 = 1;
        }
        _7296 = _pos__12779 + _7295;
        if ((long)((unsigned long)_7296 + (unsigned long)HIGH_BITS) >= 0) 
        _7296 = NewDouble((double)_7296);
        _7295 = NOVALUE;
        if (IS_ATOM_INT(_7296)) {
            _7297 = _7296 - 1;
        }
        else {
            _7297 = NewDouble(DBL_PTR(_7296)->dbl - (double)1);
        }
        DeRef(_7296);
        _7296 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__12778;
        AssignSlice(_pos__12779, _7297, _bucket__12777);
        DeRef(_7297);
        _7297 = NOVALUE;

        /** 				pos_ += length(bucket_)*/
        if (IS_SEQUENCE(_bucket__12777)){
                _7298 = SEQ_PTR(_bucket__12777)->length;
        }
        else {
            _7298 = 1;
        }
        _pos__12779 = _pos__12779 + _7298;
        _7298 = NOVALUE;
LA: 

        /** 		end for*/
        _index_12789 = _index_12789 + 1;
        goto L8; // [238] 189
L9: 
        ;
    }
    goto LB; // [243] 313
L7: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12780);
    _7300 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7300)){
            _7301 = SEQ_PTR(_7300)->length;
    }
    else {
        _7301 = 1;
    }
    _7300 = NOVALUE;
    {
        int _index_12802;
        _index_12802 = 1;
LC: 
        if (_index_12802 > _7301){
            goto LD; // [257] 312
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12780);
        _7302 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7302);
        _7303 = (int)*(((s1_ptr)_2)->base + _index_12802);
        _7302 = NOVALUE;
        if (binary_op_a(EQUALS, _7303, 0)){
            _7303 = NOVALUE;
            goto LE; // [276] 305
        }
        _7303 = NOVALUE;

        /** 				results_[pos_] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12780);
        _7305 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7305);
        _7306 = (int)*(((s1_ptr)_2)->base + _index_12802);
        _7305 = NOVALUE;
        Ref(_7306);
        _2 = (int)SEQ_PTR(_results__12778);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12778 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__12779);
        _1 = *(int *)_2;
        *(int *)_2 = _7306;
        if( _1 != _7306 ){
            DeRef(_1);
        }
        _7306 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12779 = _pos__12779 + 1;
LE: 

        /** 		end for*/
        _index_12802 = _index_12802 + 1;
        goto LC; // [307] 264
LD: 
        ;
    }
LB: 

    /** 	return results_*/
    DeRef(_the_map_12750);
    DeRef(_keys_12751);
    DeRef(_default_values_12752);
    DeRef(_buckets__12776);
    DeRef(_bucket__12777);
    DeRef(_temp_map__12780);
    _7300 = NOVALUE;
    return _results__12778;
    ;
}


int _25pairs(int _the_map_p_12814, int _sorted_result_12815)
{
    int _key_bucket__12816 = NOVALUE;
    int _value_bucket__12817 = NOVALUE;
    int _results__12818 = NOVALUE;
    int _pos__12819 = NOVALUE;
    int _temp_map__12820 = NOVALUE;
    int _7342 = NOVALUE;
    int _7340 = NOVALUE;
    int _7339 = NOVALUE;
    int _7337 = NOVALUE;
    int _7336 = NOVALUE;
    int _7335 = NOVALUE;
    int _7333 = NOVALUE;
    int _7331 = NOVALUE;
    int _7330 = NOVALUE;
    int _7329 = NOVALUE;
    int _7328 = NOVALUE;
    int _7326 = NOVALUE;
    int _7324 = NOVALUE;
    int _7323 = NOVALUE;
    int _7321 = NOVALUE;
    int _7320 = NOVALUE;
    int _7318 = NOVALUE;
    int _7316 = NOVALUE;
    int _7315 = NOVALUE;
    int _7314 = NOVALUE;
    int _7312 = NOVALUE;
    int _7310 = NOVALUE;
    int _7309 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sorted_result_12815)) {
        _1 = (long)(DBL_PTR(_sorted_result_12815)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_12815)) && (DBL_PTR(_sorted_result_12815)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_12815);
        _sorted_result_12815 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12820);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12814)){
        _temp_map__12820 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12814)->dbl));
    }
    else{
        _temp_map__12820 = (int)*(((s1_ptr)_2)->base + _the_map_p_12814);
    }
    Ref(_temp_map__12820);

    /** 	results_ = repeat({ 0, 0 }, temp_map_[ELEMENT_COUNT])*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _7309 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_temp_map__12820);
    _7310 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12818);
    _results__12818 = Repeat(_7309, _7310);
    DeRefDS(_7309);
    _7309 = NOVALUE;
    _7310 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12819 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12820);
    _7312 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7312, 76)){
        _7312 = NOVALUE;
        goto L1; // [46] 163
    }
    _7312 = NOVALUE;

    /** 		for index = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12820);
    _7314 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7314)){
            _7315 = SEQ_PTR(_7314)->length;
    }
    else {
        _7315 = 1;
    }
    _7314 = NOVALUE;
    {
        int _index_12829;
        _index_12829 = 1;
L2: 
        if (_index_12829 > _7315){
            goto L3; // [61] 160
        }

        /** 			key_bucket_ = temp_map_[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12820);
        _7316 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_key_bucket__12816);
        _2 = (int)SEQ_PTR(_7316);
        _key_bucket__12816 = (int)*(((s1_ptr)_2)->base + _index_12829);
        Ref(_key_bucket__12816);
        _7316 = NOVALUE;

        /** 			value_bucket_ = temp_map_[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12820);
        _7318 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_bucket__12817);
        _2 = (int)SEQ_PTR(_7318);
        _value_bucket__12817 = (int)*(((s1_ptr)_2)->base + _index_12829);
        Ref(_value_bucket__12817);
        _7318 = NOVALUE;

        /** 			for j = 1 to length(key_bucket_) do*/
        if (IS_SEQUENCE(_key_bucket__12816)){
                _7320 = SEQ_PTR(_key_bucket__12816)->length;
        }
        else {
            _7320 = 1;
        }
        {
            int _j_12837;
            _j_12837 = 1;
L4: 
            if (_j_12837 > _7320){
                goto L5; // [101] 153
            }

            /** 				results_[pos_][1] = key_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__12818);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__12818 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__12819 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_key_bucket__12816);
            _7323 = (int)*(((s1_ptr)_2)->base + _j_12837);
            Ref(_7323);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 1);
            _1 = *(int *)_2;
            *(int *)_2 = _7323;
            if( _1 != _7323 ){
                DeRef(_1);
            }
            _7323 = NOVALUE;
            _7321 = NOVALUE;

            /** 				results_[pos_][2] = value_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__12818);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__12818 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__12819 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_value_bucket__12817);
            _7326 = (int)*(((s1_ptr)_2)->base + _j_12837);
            Ref(_7326);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _7326;
            if( _1 != _7326 ){
                DeRef(_1);
            }
            _7326 = NOVALUE;
            _7324 = NOVALUE;

            /** 				pos_ += 1*/
            _pos__12819 = _pos__12819 + 1;

            /** 			end for*/
            _j_12837 = _j_12837 + 1;
            goto L4; // [148] 108
L5: 
            ;
        }

        /** 		end for*/
        _index_12829 = _index_12829 + 1;
        goto L2; // [155] 68
L3: 
        ;
    }
    goto L6; // [160] 256
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12820);
    _7328 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7328)){
            _7329 = SEQ_PTR(_7328)->length;
    }
    else {
        _7329 = 1;
    }
    _7328 = NOVALUE;
    {
        int _index_12848;
        _index_12848 = 1;
L7: 
        if (_index_12848 > _7329){
            goto L8; // [174] 255
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12820);
        _7330 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7330);
        _7331 = (int)*(((s1_ptr)_2)->base + _index_12848);
        _7330 = NOVALUE;
        if (binary_op_a(EQUALS, _7331, 0)){
            _7331 = NOVALUE;
            goto L9; // [193] 248
        }
        _7331 = NOVALUE;

        /** 				results_[pos_][1] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__12818);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12818 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__12819 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__12820);
        _7335 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7335);
        _7336 = (int)*(((s1_ptr)_2)->base + _index_12848);
        _7335 = NOVALUE;
        Ref(_7336);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _7336;
        if( _1 != _7336 ){
            DeRef(_1);
        }
        _7336 = NOVALUE;
        _7333 = NOVALUE;

        /** 				results_[pos_][2] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__12818);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12818 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__12819 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__12820);
        _7339 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7339);
        _7340 = (int)*(((s1_ptr)_2)->base + _index_12848);
        _7339 = NOVALUE;
        Ref(_7340);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _7340;
        if( _1 != _7340 ){
            DeRef(_1);
        }
        _7340 = NOVALUE;
        _7337 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12819 = _pos__12819 + 1;
L9: 

        /** 		end for	*/
        _index_12848 = _index_12848 + 1;
        goto L7; // [250] 181
L8: 
        ;
    }
L6: 

    /** 	if sorted_result then*/
    if (_sorted_result_12815 == 0)
    {
        goto LA; // [258] 275
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__12818);
    _7342 = _7sort(_results__12818, 1);
    DeRef(_the_map_p_12814);
    DeRef(_key_bucket__12816);
    DeRef(_value_bucket__12817);
    DeRefDS(_results__12818);
    DeRef(_temp_map__12820);
    _7314 = NOVALUE;
    _7328 = NOVALUE;
    return _7342;
    goto LB; // [272] 282
LA: 

    /** 		return results_*/
    DeRef(_the_map_p_12814);
    DeRef(_key_bucket__12816);
    DeRef(_value_bucket__12817);
    DeRef(_temp_map__12820);
    _7314 = NOVALUE;
    _7328 = NOVALUE;
    DeRef(_7342);
    _7342 = NOVALUE;
    return _results__12818;
LB: 
    ;
}


void _25optimize(int _the_map_p_12869, int _max_p_12870, int _grow_p_12871)
{
    int _stats__12873 = NOVALUE;
    int _next_guess__12874 = NOVALUE;
    int _prev_guess_12875 = NOVALUE;
    int _7363 = NOVALUE;
    int _7362 = NOVALUE;
    int _7360 = NOVALUE;
    int _7359 = NOVALUE;
    int _7358 = NOVALUE;
    int _7357 = NOVALUE;
    int _7356 = NOVALUE;
    int _7354 = NOVALUE;
    int _7352 = NOVALUE;
    int _7351 = NOVALUE;
    int _7350 = NOVALUE;
    int _7349 = NOVALUE;
    int _7345 = NOVALUE;
    int _7344 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_p_12870)) {
        _1 = (long)(DBL_PTR(_max_p_12870)->dbl);
        if (UNIQUE(DBL_PTR(_max_p_12870)) && (DBL_PTR(_max_p_12870)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_p_12870);
        _max_p_12870 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12869)){
        _7344 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12869)->dbl));
    }
    else{
        _7344 = (int)*(((s1_ptr)_2)->base + _the_map_p_12869);
    }
    _2 = (int)SEQ_PTR(_7344);
    _7345 = (int)*(((s1_ptr)_2)->base + 4);
    _7344 = NOVALUE;
    if (binary_op_a(NOTEQ, _7345, 76)){
        _7345 = NOVALUE;
        goto L1; // [19] 204
    }
    _7345 = NOVALUE;

    /** 		if grow_p < 1 then*/
    if (binary_op_a(GREATEREQ, _grow_p_12871, 1)){
        goto L2; // [25] 35
    }

    /** 			grow_p = 1.333*/
    RefDS(_7343);
    DeRef(_grow_p_12871);
    _grow_p_12871 = _7343;
L2: 

    /** 		if max_p < 3 then*/
    if (_max_p_12870 >= 3)
    goto L3; // [37] 49

    /** 			max_p = 3*/
    _max_p_12870 = 3;
L3: 

    /** 		next_guess_ = math:max({1, floor(eumem:ram_space[the_map_p][ELEMENT_COUNT] / max_p)})*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_the_map_p_12869)){
        _7349 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12869)->dbl));
    }
    else{
        _7349 = (int)*(((s1_ptr)_2)->base + _the_map_p_12869);
    }
    _2 = (int)SEQ_PTR(_7349);
    _7350 = (int)*(((s1_ptr)_2)->base + 2);
    _7349 = NOVALUE;
    if (IS_ATOM_INT(_7350)) {
        if (_max_p_12870 > 0 && _7350 >= 0) {
            _7351 = _7350 / _max_p_12870;
        }
        else {
            temp_dbl = floor((double)_7350 / (double)_max_p_12870);
            if (_7350 != MININT)
            _7351 = (long)temp_dbl;
            else
            _7351 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7350, _max_p_12870);
        _7351 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _7350 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = _7351;
    _7352 = MAKE_SEQ(_1);
    _7351 = NOVALUE;
    _next_guess__12874 = _18max(_7352);
    _7352 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__12874)) {
        _1 = (long)(DBL_PTR(_next_guess__12874)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__12874)) && (DBL_PTR(_next_guess__12874)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__12874);
        _next_guess__12874 = _1;
    }

    /** 		while 1 with entry do*/
    goto L4; // [81] 184
L5: 

    /** 			if stats_[LARGEST_BUCKET] <= max_p then*/
    _2 = (int)SEQ_PTR(_stats__12873);
    _7354 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(GREATER, _7354, _max_p_12870)){
        _7354 = NOVALUE;
        goto L6; // [94] 103
    }
    _7354 = NOVALUE;

    /** 				exit -- Largest is now smaller than the maximum I wanted.*/
    goto L7; // [100] 203
L6: 

    /** 			if stats_[LARGEST_BUCKET] <= (stats_[STDEV_BUCKET]*3 + stats_[AVERAGE_BUCKET]) then*/
    _2 = (int)SEQ_PTR(_stats__12873);
    _7356 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_stats__12873);
    _7357 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_ATOM_INT(_7357)) {
        if (_7357 == (short)_7357)
        _7358 = _7357 * 3;
        else
        _7358 = NewDouble(_7357 * (double)3);
    }
    else {
        _7358 = binary_op(MULTIPLY, _7357, 3);
    }
    _7357 = NOVALUE;
    _2 = (int)SEQ_PTR(_stats__12873);
    _7359 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_7358) && IS_ATOM_INT(_7359)) {
        _7360 = _7358 + _7359;
        if ((long)((unsigned long)_7360 + (unsigned long)HIGH_BITS) >= 0) 
        _7360 = NewDouble((double)_7360);
    }
    else {
        _7360 = binary_op(PLUS, _7358, _7359);
    }
    DeRef(_7358);
    _7358 = NOVALUE;
    _7359 = NOVALUE;
    if (binary_op_a(GREATER, _7356, _7360)){
        _7356 = NOVALUE;
        DeRef(_7360);
        _7360 = NOVALUE;
        goto L8; // [131] 140
    }
    _7356 = NOVALUE;
    DeRef(_7360);
    _7360 = NOVALUE;

    /** 				exit -- Largest is smaller than is statistically expected.*/
    goto L7; // [137] 203
L8: 

    /** 			prev_guess = next_guess_*/
    _prev_guess_12875 = _next_guess__12874;

    /** 			next_guess_ = floor(stats_[NUM_BUCKETS] * grow_p)*/
    _2 = (int)SEQ_PTR(_stats__12873);
    _7362 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7362) && IS_ATOM_INT(_grow_p_12871)) {
        if (_7362 == (short)_7362 && _grow_p_12871 <= INT15 && _grow_p_12871 >= -INT15)
        _7363 = _7362 * _grow_p_12871;
        else
        _7363 = NewDouble(_7362 * (double)_grow_p_12871);
    }
    else {
        _7363 = binary_op(MULTIPLY, _7362, _grow_p_12871);
    }
    _7362 = NOVALUE;
    if (IS_ATOM_INT(_7363))
    _next_guess__12874 = e_floor(_7363);
    else
    _next_guess__12874 = unary_op(FLOOR, _7363);
    DeRef(_7363);
    _7363 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__12874)) {
        _1 = (long)(DBL_PTR(_next_guess__12874)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__12874)) && (DBL_PTR(_next_guess__12874)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__12874);
        _next_guess__12874 = _1;
    }

    /** 			if prev_guess = next_guess_ then*/
    if (_prev_guess_12875 != _next_guess__12874)
    goto L9; // [168] 181

    /** 				next_guess_ += 1*/
    _next_guess__12874 = _next_guess__12874 + 1;
L9: 

    /** 		entry*/
L4: 

    /** 			rehash(the_map_p, next_guess_)*/
    Ref(_the_map_p_12869);
    _25rehash(_the_map_p_12869, _next_guess__12874);

    /** 			stats_ = statistics(the_map_p)*/
    Ref(_the_map_p_12869);
    _0 = _stats__12873;
    _stats__12873 = _25statistics(_the_map_p_12869);
    DeRef(_0);

    /** 		end while*/
    goto L5; // [200] 84
L7: 
L1: 

    /** end procedure*/
    DeRef(_the_map_p_12869);
    DeRef(_grow_p_12871);
    DeRef(_stats__12873);
    return;
    ;
}


int _25load_map(int _input_file_name_12909)
{
    int _file_handle_12910 = NOVALUE;
    int _line_in_12911 = NOVALUE;
    int _logical_line_12912 = NOVALUE;
    int _has_comment_12913 = NOVALUE;
    int _delim_pos_12914 = NOVALUE;
    int _data_value_12915 = NOVALUE;
    int _data_key_12916 = NOVALUE;
    int _conv_res_12917 = NOVALUE;
    int _new_map_12918 = NOVALUE;
    int _line_conts_12919 = NOVALUE;
    int _value_inlined_value_at_339_12986 = NOVALUE;
    int _value_inlined_value_at_510_13016 = NOVALUE;
    int _in_quote_13051 = NOVALUE;
    int _last_in_13052 = NOVALUE;
    int _cur_in_13053 = NOVALUE;
    int _seek_1__tmp_at925_13081 = NOVALUE;
    int _seek_inlined_seek_at_925_13080 = NOVALUE;
    int _7494 = NOVALUE;
    int _7493 = NOVALUE;
    int _7492 = NOVALUE;
    int _7491 = NOVALUE;
    int _7486 = NOVALUE;
    int _7484 = NOVALUE;
    int _7483 = NOVALUE;
    int _7481 = NOVALUE;
    int _7480 = NOVALUE;
    int _7479 = NOVALUE;
    int _7478 = NOVALUE;
    int _7470 = NOVALUE;
    int _7468 = NOVALUE;
    int _7461 = NOVALUE;
    int _7460 = NOVALUE;
    int _7459 = NOVALUE;
    int _7458 = NOVALUE;
    int _7454 = NOVALUE;
    int _7453 = NOVALUE;
    int _7449 = NOVALUE;
    int _7448 = NOVALUE;
    int _7445 = NOVALUE;
    int _7444 = NOVALUE;
    int _7442 = NOVALUE;
    int _7434 = NOVALUE;
    int _7433 = NOVALUE;
    int _7432 = NOVALUE;
    int _7431 = NOVALUE;
    int _7430 = NOVALUE;
    int _7429 = NOVALUE;
    int _7428 = NOVALUE;
    int _7427 = NOVALUE;
    int _7425 = NOVALUE;
    int _7424 = NOVALUE;
    int _7423 = NOVALUE;
    int _7420 = NOVALUE;
    int _7419 = NOVALUE;
    int _7417 = NOVALUE;
    int _7415 = NOVALUE;
    int _7414 = NOVALUE;
    int _7403 = NOVALUE;
    int _7401 = NOVALUE;
    int _7400 = NOVALUE;
    int _7395 = NOVALUE;
    int _7392 = NOVALUE;
    int _7388 = NOVALUE;
    int _7385 = NOVALUE;
    int _7382 = NOVALUE;
    int _7381 = NOVALUE;
    int _7377 = NOVALUE;
    int _7375 = NOVALUE;
    int _7369 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence line_conts =   ",${"*/
    RefDS(_7368);
    DeRefi(_line_conts_12919);
    _line_conts_12919 = _7368;

    /** 	if sequence(input_file_name) then*/
    _7369 = IS_SEQUENCE(_input_file_name_12909);
    if (_7369 == 0)
    {
        _7369 = NOVALUE;
        goto L1; // [13] 28
    }
    else{
        _7369 = NOVALUE;
    }

    /** 		file_handle = open(input_file_name, "rb")*/
    _file_handle_12910 = EOpen(_input_file_name_12909, _2913, 0);
    goto L2; // [25] 38
L1: 

    /** 		file_handle = input_file_name*/
    Ref(_input_file_name_12909);
    _file_handle_12910 = _input_file_name_12909;
    if (!IS_ATOM_INT(_file_handle_12910)) {
        _1 = (long)(DBL_PTR(_file_handle_12910)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle_12910)) && (DBL_PTR(_file_handle_12910)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle_12910);
        _file_handle_12910 = _1;
    }
L2: 

    /** 	if file_handle = -1 then*/
    if (_file_handle_12910 != -1)
    goto L3; // [42] 53

    /** 		return -1*/
    DeRef(_input_file_name_12909);
    DeRef(_line_in_12911);
    DeRef(_logical_line_12912);
    DeRef(_data_value_12915);
    DeRef(_data_key_12916);
    DeRef(_conv_res_12917);
    DeRef(_new_map_12918);
    DeRefi(_line_conts_12919);
    return -1;
L3: 

    /** 	new_map = new(threshold_size) -- Assume a small map initially.*/
    _0 = _new_map_12918;
    _new_map_12918 = _25new(_25threshold_size_11951);
    DeRef(_0);

    /** 	for i = 1 to 10 do*/
    {
        int _i_12929;
        _i_12929 = 1;
L4: 
        if (_i_12929 > 10){
            goto L5; // [63] 126
        }

        /** 		delim_pos = getc(file_handle)*/
        if (_file_handle_12910 != last_r_file_no) {
            last_r_file_ptr = which_file(_file_handle_12910, EF_READ);
            last_r_file_no = _file_handle_12910;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _delim_pos_12914 = getKBchar();
            }
            else
            _delim_pos_12914 = getc(last_r_file_ptr);
        }
        else
        _delim_pos_12914 = getc(last_r_file_ptr);

        /** 		if delim_pos = -1 then */
        if (_delim_pos_12914 != -1)
        goto L6; // [79] 88

        /** 			exit*/
        goto L5; // [85] 126
L6: 

        /** 		if not t_print(delim_pos) then */
        _7375 = _5t_print(_delim_pos_12914);
        if (IS_ATOM_INT(_7375)) {
            if (_7375 != 0){
                DeRef(_7375);
                _7375 = NOVALUE;
                goto L7; // [94] 112
            }
        }
        else {
            if (DBL_PTR(_7375)->dbl != 0.0){
                DeRef(_7375);
                _7375 = NOVALUE;
                goto L7; // [94] 112
            }
        }
        DeRef(_7375);
        _7375 = NOVALUE;

        /** 	    	if not t_space(delim_pos) then*/
        _7377 = _5t_space(_delim_pos_12914);
        if (IS_ATOM_INT(_7377)) {
            if (_7377 != 0){
                DeRef(_7377);
                _7377 = NOVALUE;
                goto L8; // [103] 111
            }
        }
        else {
            if (DBL_PTR(_7377)->dbl != 0.0){
                DeRef(_7377);
                _7377 = NOVALUE;
                goto L8; // [103] 111
            }
        }
        DeRef(_7377);
        _7377 = NOVALUE;

        /** 	    		exit*/
        goto L5; // [108] 126
L8: 
L7: 

        /** 	    delim_pos = -1*/
        _delim_pos_12914 = -1;

        /** 	end for*/
        _i_12929 = _i_12929 + 1;
        goto L4; // [121] 70
L5: 
        ;
    }

    /** 	if delim_pos = -1 then*/
    if (_delim_pos_12914 != -1)
    goto L9; // [130] 921

    /** 		close(file_handle)*/
    EClose(_file_handle_12910);

    /** 		file_handle = open(input_file_name, "r")*/
    _file_handle_12910 = EOpen(_input_file_name_12909, _4069, 0);

    /** 		while sequence(logical_line) with entry do*/
    goto LA; // [149] 574
LB: 
    _7381 = IS_SEQUENCE(_logical_line_12912);
    if (_7381 == 0)
    {
        _7381 = NOVALUE;
        goto LC; // [157] 1062
    }
    else{
        _7381 = NOVALUE;
    }

    /** 			logical_line = search:match_replace({-2}, logical_line, "\\#3D")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    _7382 = MAKE_SEQ(_1);
    Ref(_logical_line_12912);
    RefDS(_7383);
    _0 = _logical_line_12912;
    _logical_line_12912 = _6match_replace(_7382, _logical_line_12912, _7383, 0);
    DeRef(_0);
    _7382 = NOVALUE;

    /** 			logical_line = search:match_replace({-3}, logical_line, "\\#23")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -3;
    _7385 = MAKE_SEQ(_1);
    Ref(_logical_line_12912);
    RefDS(_7386);
    _0 = _logical_line_12912;
    _logical_line_12912 = _6match_replace(_7385, _logical_line_12912, _7386, 0);
    DeRef(_0);
    _7385 = NOVALUE;

    /** 			logical_line = search:match_replace({-4}, logical_line, "\\#24")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -4;
    _7388 = MAKE_SEQ(_1);
    Ref(_logical_line_12912);
    RefDS(_7389);
    _0 = _logical_line_12912;
    _logical_line_12912 = _6match_replace(_7388, _logical_line_12912, _7389, 0);
    DeRef(_0);
    _7388 = NOVALUE;

    /** 			logical_line = search:match_replace({-5}, logical_line, "\\#2C")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -5;
    _7392 = MAKE_SEQ(_1);
    Ref(_logical_line_12912);
    RefDS(_7393);
    _0 = _logical_line_12912;
    _logical_line_12912 = _6match_replace(_7392, _logical_line_12912, _7393, 0);
    DeRef(_0);
    _7392 = NOVALUE;

    /** 			logical_line = search:match_replace({-6}, logical_line, "\\-")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -6;
    _7395 = MAKE_SEQ(_1);
    Ref(_logical_line_12912);
    RefDS(_7396);
    _0 = _logical_line_12912;
    _logical_line_12912 = _6match_replace(_7395, _logical_line_12912, _7396, 0);
    DeRef(_0);
    _7395 = NOVALUE;

    /** 			delim_pos = find('=', logical_line)*/
    _delim_pos_12914 = find_from(61, _logical_line_12912, 1);

    /** 			if delim_pos > 0 then*/
    if (_delim_pos_12914 <= 0)
    goto LD; // [236] 571

    /** 				data_key = text:trim(logical_line[1..delim_pos-1])*/
    _7400 = _delim_pos_12914 - 1;
    rhs_slice_target = (object_ptr)&_7401;
    RHS_Slice(_logical_line_12912, 1, _7400);
    RefDS(_2936);
    _0 = _data_key_12916;
    _data_key_12916 = _16trim(_7401, _2936, 0);
    DeRef(_0);
    _7401 = NOVALUE;

    /** 				if length(data_key) > 0 then*/
    if (IS_SEQUENCE(_data_key_12916)){
            _7403 = SEQ_PTR(_data_key_12916)->length;
    }
    else {
        _7403 = 1;
    }
    if (_7403 <= 0)
    goto LE; // [262] 570

    /** 					data_key = search:match_replace("\\#2C", data_key, ",")*/
    RefDS(_7393);
    Ref(_data_key_12916);
    RefDS(_7405);
    _0 = _data_key_12916;
    _data_key_12916 = _6match_replace(_7393, _data_key_12916, _7405, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#24", data_key, "$")*/
    RefDS(_7389);
    Ref(_data_key_12916);
    RefDS(_7407);
    _0 = _data_key_12916;
    _data_key_12916 = _6match_replace(_7389, _data_key_12916, _7407, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#22", data_key, "\"")*/
    RefDS(_7409);
    Ref(_data_key_12916);
    RefDS(_3328);
    _0 = _data_key_12916;
    _data_key_12916 = _6match_replace(_7409, _data_key_12916, _3328, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#3D", data_key, "=")*/
    RefDS(_7383);
    Ref(_data_key_12916);
    RefDS(_1156);
    _0 = _data_key_12916;
    _data_key_12916 = _6match_replace(_7383, _data_key_12916, _1156, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#23", data_key, "#")*/
    RefDS(_7386);
    Ref(_data_key_12916);
    RefDS(_400);
    _0 = _data_key_12916;
    _data_key_12916 = _6match_replace(_7386, _data_key_12916, _400, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\-", data_key, "-")*/
    RefDS(_7396);
    Ref(_data_key_12916);
    RefDS(_6130);
    _0 = _data_key_12916;
    _data_key_12916 = _6match_replace(_7396, _data_key_12916, _6130, 0);
    DeRef(_0);

    /** 					if not t_alpha(data_key[1]) then*/
    _2 = (int)SEQ_PTR(_data_key_12916);
    _7414 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_7414);
    _7415 = _5t_alpha(_7414);
    _7414 = NOVALUE;
    if (IS_ATOM_INT(_7415)) {
        if (_7415 != 0){
            DeRef(_7415);
            _7415 = NOVALUE;
            goto LF; // [330] 386
        }
    }
    else {
        if (DBL_PTR(_7415)->dbl != 0.0){
            DeRef(_7415);
            _7415 = NOVALUE;
            goto LF; // [330] 386
        }
    }
    DeRef(_7415);
    _7415 = NOVALUE;

    /** 						conv_res = stdget:value(data_key,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_8126)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_8126)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_8126)) && (DBL_PTR(_14GET_LONG_ANSWER_8126)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_8126);
        _14GET_LONG_ANSWER_8126 = _1;
    }
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_8126)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_8126)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_8126)) && (DBL_PTR(_14GET_LONG_ANSWER_8126)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_8126);
        _14GET_LONG_ANSWER_8126 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_key_12916);
    _0 = _conv_res_12917;
    _conv_res_12917 = _14get_value(_data_key_12916, 1, _14GET_LONG_ANSWER_8126);
    DeRef(_0);

    /** 						if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_12917);
    _7417 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _7417, 0)){
        _7417 = NOVALUE;
        goto L10; // [360] 385
    }
    _7417 = NOVALUE;

    /** 							if conv_res[3] = length(data_key) then*/
    _2 = (int)SEQ_PTR(_conv_res_12917);
    _7419 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_key_12916)){
            _7420 = SEQ_PTR(_data_key_12916)->length;
    }
    else {
        _7420 = 1;
    }
    if (binary_op_a(NOTEQ, _7419, _7420)){
        _7419 = NOVALUE;
        _7420 = NOVALUE;
        goto L11; // [373] 384
    }
    _7419 = NOVALUE;
    _7420 = NOVALUE;

    /** 								data_key = conv_res[2]*/
    DeRef(_data_key_12916);
    _2 = (int)SEQ_PTR(_conv_res_12917);
    _data_key_12916 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_key_12916);
L11: 
L10: 
LF: 

    /** 					data_value = text:trim(logical_line[delim_pos+1..$])*/
    _7423 = _delim_pos_12914 + 1;
    if (_7423 > MAXINT){
        _7423 = NewDouble((double)_7423);
    }
    if (IS_SEQUENCE(_logical_line_12912)){
            _7424 = SEQ_PTR(_logical_line_12912)->length;
    }
    else {
        _7424 = 1;
    }
    rhs_slice_target = (object_ptr)&_7425;
    RHS_Slice(_logical_line_12912, _7423, _7424);
    RefDS(_2936);
    _0 = _data_value_12915;
    _data_value_12915 = _16trim(_7425, _2936, 0);
    DeRef(_0);
    _7425 = NOVALUE;

    /** 					if data_value[1] = '\"' and data_value[$] = '\"' then*/
    _2 = (int)SEQ_PTR(_data_value_12915);
    _7427 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_7427)) {
        _7428 = (_7427 == 34);
    }
    else {
        _7428 = binary_op(EQUALS, _7427, 34);
    }
    _7427 = NOVALUE;
    if (IS_ATOM_INT(_7428)) {
        if (_7428 == 0) {
            goto L12; // [416] 450
        }
    }
    else {
        if (DBL_PTR(_7428)->dbl == 0.0) {
            goto L12; // [416] 450
        }
    }
    if (IS_SEQUENCE(_data_value_12915)){
            _7430 = SEQ_PTR(_data_value_12915)->length;
    }
    else {
        _7430 = 1;
    }
    _2 = (int)SEQ_PTR(_data_value_12915);
    _7431 = (int)*(((s1_ptr)_2)->base + _7430);
    if (IS_ATOM_INT(_7431)) {
        _7432 = (_7431 == 34);
    }
    else {
        _7432 = binary_op(EQUALS, _7431, 34);
    }
    _7431 = NOVALUE;
    if (_7432 == 0) {
        DeRef(_7432);
        _7432 = NOVALUE;
        goto L12; // [432] 450
    }
    else {
        if (!IS_ATOM_INT(_7432) && DBL_PTR(_7432)->dbl == 0.0){
            DeRef(_7432);
            _7432 = NOVALUE;
            goto L12; // [432] 450
        }
        DeRef(_7432);
        _7432 = NOVALUE;
    }
    DeRef(_7432);
    _7432 = NOVALUE;

    /** 					    data_value = data_value[2..$-1]*/
    if (IS_SEQUENCE(_data_value_12915)){
            _7433 = SEQ_PTR(_data_value_12915)->length;
    }
    else {
        _7433 = 1;
    }
    _7434 = _7433 - 1;
    _7433 = NOVALUE;
    rhs_slice_target = (object_ptr)&_data_value_12915;
    RHS_Slice(_data_value_12915, 2, _7434);
L12: 

    /** 					data_value = search:match_replace("\\#2C", data_value, ",")*/
    RefDS(_7393);
    Ref(_data_value_12915);
    RefDS(_7405);
    _0 = _data_value_12915;
    _data_value_12915 = _6match_replace(_7393, _data_value_12915, _7405, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#24", data_value, "$")*/
    RefDS(_7389);
    Ref(_data_value_12915);
    RefDS(_7407);
    _0 = _data_value_12915;
    _data_value_12915 = _6match_replace(_7389, _data_value_12915, _7407, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#22", data_value, "\"")*/
    RefDS(_7409);
    Ref(_data_value_12915);
    RefDS(_3328);
    _0 = _data_value_12915;
    _data_value_12915 = _6match_replace(_7409, _data_value_12915, _3328, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#3D", data_value, "=")*/
    RefDS(_7383);
    Ref(_data_value_12915);
    RefDS(_1156);
    _0 = _data_value_12915;
    _data_value_12915 = _6match_replace(_7383, _data_value_12915, _1156, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#23", data_value, "#")*/
    RefDS(_7386);
    Ref(_data_value_12915);
    RefDS(_400);
    _0 = _data_value_12915;
    _data_value_12915 = _6match_replace(_7386, _data_value_12915, _400, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\-", data_value, "-")*/
    RefDS(_7396);
    Ref(_data_value_12915);
    RefDS(_6130);
    _0 = _data_value_12915;
    _data_value_12915 = _6match_replace(_7396, _data_value_12915, _6130, 0);
    DeRef(_0);

    /** 					conv_res = stdget:value(data_value,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_8126)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_8126)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_8126)) && (DBL_PTR(_14GET_LONG_ANSWER_8126)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_8126);
        _14GET_LONG_ANSWER_8126 = _1;
    }
    if (!IS_ATOM_INT(_14GET_LONG_ANSWER_8126)) {
        _1 = (long)(DBL_PTR(_14GET_LONG_ANSWER_8126)->dbl);
        if (UNIQUE(DBL_PTR(_14GET_LONG_ANSWER_8126)) && (DBL_PTR(_14GET_LONG_ANSWER_8126)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14GET_LONG_ANSWER_8126);
        _14GET_LONG_ANSWER_8126 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_value_12915);
    _0 = _conv_res_12917;
    _conv_res_12917 = _14get_value(_data_value_12915, 1, _14GET_LONG_ANSWER_8126);
    DeRef(_0);

    /** 					if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_12917);
    _7442 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _7442, 0)){
        _7442 = NOVALUE;
        goto L13; // [531] 556
    }
    _7442 = NOVALUE;

    /** 						if conv_res[3] = length(data_value) then*/
    _2 = (int)SEQ_PTR(_conv_res_12917);
    _7444 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_value_12915)){
            _7445 = SEQ_PTR(_data_value_12915)->length;
    }
    else {
        _7445 = 1;
    }
    if (binary_op_a(NOTEQ, _7444, _7445)){
        _7444 = NOVALUE;
        _7445 = NOVALUE;
        goto L14; // [544] 555
    }
    _7444 = NOVALUE;
    _7445 = NOVALUE;

    /** 							data_value = conv_res[2]*/
    DeRef(_data_value_12915);
    _2 = (int)SEQ_PTR(_conv_res_12917);
    _data_value_12915 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_value_12915);
L14: 
L13: 

    /** 					put(new_map, data_key, data_value)*/
    Ref(_new_map_12918);
    Ref(_data_key_12916);
    Ref(_data_value_12915);
    _25put(_new_map_12918, _data_key_12916, _data_value_12915, 1, _25threshold_size_11951);
LE: 
LD: 

    /** 		entry*/
LA: 

    /** 			logical_line = -1*/
    DeRef(_logical_line_12912);
    _logical_line_12912 = -1;

    /** 			while sequence(line_in) with entry do*/
    goto L15; // [581] 708
L16: 
    _7448 = IS_SEQUENCE(_line_in_12911);
    if (_7448 == 0)
    {
        _7448 = NOVALUE;
        goto LB; // [589] 152
    }
    else{
        _7448 = NOVALUE;
    }

    /** 				if atom(logical_line) then*/
    _7449 = IS_ATOM(_logical_line_12912);
    if (_7449 == 0)
    {
        _7449 = NOVALUE;
        goto L17; // [597] 606
    }
    else{
        _7449 = NOVALUE;
    }

    /** 					logical_line = ""*/
    RefDS(_5);
    DeRef(_logical_line_12912);
    _logical_line_12912 = _5;
L17: 

    /** 				has_comment = match("--", line_in)*/
    _has_comment_12913 = e_match_from(_7450, _line_in_12911, 1);

    /** 				if has_comment != 0 then*/
    if (_has_comment_12913 == 0)
    goto L18; // [617] 641

    /** 					line_in = text:trim(line_in[1..has_comment-1])*/
    _7453 = _has_comment_12913 - 1;
    rhs_slice_target = (object_ptr)&_7454;
    RHS_Slice(_line_in_12911, 1, _7453);
    RefDS(_2936);
    _0 = _line_in_12911;
    _line_in_12911 = _16trim(_7454, _2936, 0);
    DeRef(_0);
    _7454 = NOVALUE;
    goto L19; // [638] 650
L18: 

    /** 					line_in = text:trim(line_in)*/
    Ref(_line_in_12911);
    RefDS(_2936);
    _0 = _line_in_12911;
    _line_in_12911 = _16trim(_line_in_12911, _2936, 0);
    DeRef(_0);
L19: 

    /** 				logical_line &= line_in*/
    if (IS_SEQUENCE(_logical_line_12912) && IS_ATOM(_line_in_12911)) {
        Ref(_line_in_12911);
        Append(&_logical_line_12912, _logical_line_12912, _line_in_12911);
    }
    else if (IS_ATOM(_logical_line_12912) && IS_SEQUENCE(_line_in_12911)) {
        Ref(_logical_line_12912);
        Prepend(&_logical_line_12912, _line_in_12911, _logical_line_12912);
    }
    else {
        Concat((object_ptr)&_logical_line_12912, _logical_line_12912, _line_in_12911);
    }

    /** 				if length(line_in) then*/
    if (IS_SEQUENCE(_line_in_12911)){
            _7458 = SEQ_PTR(_line_in_12911)->length;
    }
    else {
        _7458 = 1;
    }
    if (_7458 == 0)
    {
        _7458 = NOVALUE;
        goto L1A; // [661] 705
    }
    else{
        _7458 = NOVALUE;
    }

    /** 					if not find(line_in[$], line_conts) then*/
    if (IS_SEQUENCE(_line_in_12911)){
            _7459 = SEQ_PTR(_line_in_12911)->length;
    }
    else {
        _7459 = 1;
    }
    _2 = (int)SEQ_PTR(_line_in_12911);
    _7460 = (int)*(((s1_ptr)_2)->base + _7459);
    _7461 = find_from(_7460, _line_conts_12919, 1);
    _7460 = NOVALUE;
    if (_7461 != 0)
    goto L1B; // [678] 704
    _7461 = NOVALUE;

    /** 						logical_line = search:match_replace(`",$"`, logical_line, "")*/
    RefDS(_7463);
    RefDS(_logical_line_12912);
    RefDS(_5);
    _0 = _logical_line_12912;
    _logical_line_12912 = _6match_replace(_7463, _logical_line_12912, _5, 0);
    DeRefDS(_0);

    /** 						logical_line = search:match_replace(`,$`, logical_line, "")*/
    RefDS(_7465);
    Ref(_logical_line_12912);
    RefDS(_5);
    _0 = _logical_line_12912;
    _logical_line_12912 = _6match_replace(_7465, _logical_line_12912, _5, 0);
    DeRef(_0);

    /** 						exit*/
    goto LB; // [701] 152
L1B: 
L1A: 

    /** 			entry*/
L15: 

    /** 				line_in = gets(file_handle)*/
    DeRef(_line_in_12911);
    _line_in_12911 = EGets(_file_handle_12910);

    /**                 integer in_quote = 0, last_in = -1, cur_in = -1*/
    _in_quote_13051 = 0;
    _last_in_13052 = -1;
    _cur_in_13053 = -1;

    /**                 if not equal(line_in, -1) then*/
    if (_line_in_12911 == -1)
    _7468 = 1;
    else if (IS_ATOM_INT(_line_in_12911) && IS_ATOM_INT(-1))
    _7468 = 0;
    else
    _7468 = (compare(_line_in_12911, -1) == 0);
    if (_7468 != 0)
    goto L1C; // [736] 906
    _7468 = NOVALUE;

    /**                     for i = 1 to length(line_in) do*/
    if (IS_SEQUENCE(_line_in_12911)){
            _7470 = SEQ_PTR(_line_in_12911)->length;
    }
    else {
        _7470 = 1;
    }
    {
        int _i_13058;
        _i_13058 = 1;
L1D: 
        if (_i_13058 > _7470){
            goto L1E; // [744] 905
        }

        /**                         cur_in = line_in[i]*/
        _2 = (int)SEQ_PTR(_line_in_12911);
        _cur_in_13053 = (int)*(((s1_ptr)_2)->base + _i_13058);
        if (!IS_ATOM_INT(_cur_in_13053)){
            _cur_in_13053 = (long)DBL_PTR(_cur_in_13053)->dbl;
        }

        /**                         if cur_in = '"' then*/
        if (_cur_in_13053 != 34)
        goto L1F; // [763] 777

        /**                             in_quote = not in_quote*/
        _in_quote_13051 = (_in_quote_13051 == 0);
        goto L20; // [774] 885
L1F: 

        /**                         elsif in_quote then*/
        if (_in_quote_13051 == 0)
        {
            goto L21; // [779] 884
        }
        else{
        }

        /**                             if cur_in = '=' then*/
        if (_cur_in_13053 != 61)
        goto L22; // [784] 798

        /**                                 cur_in = -2 */
        _cur_in_13053 = -2;
        goto L23; // [795] 883
L22: 

        /**                             elsif cur_in = '#' then*/
        if (_cur_in_13053 != 35)
        goto L24; // [800] 814

        /**                                 cur_in = -3 */
        _cur_in_13053 = -3;
        goto L23; // [811] 883
L24: 

        /**                             elsif cur_in = '$' then*/
        if (_cur_in_13053 != 36)
        goto L25; // [816] 830

        /**                                 cur_in = -4*/
        _cur_in_13053 = -4;
        goto L23; // [827] 883
L25: 

        /**                             elsif cur_in = ',' then*/
        if (_cur_in_13053 != 44)
        goto L26; // [832] 846

        /**                                 cur_in = -5*/
        _cur_in_13053 = -5;
        goto L23; // [843] 883
L26: 

        /**                             elsif cur_in = '-' and last_in = '-' then*/
        _7478 = (_cur_in_13053 == 45);
        if (_7478 == 0) {
            goto L27; // [852] 882
        }
        _7480 = (_last_in_13052 == 45);
        if (_7480 == 0)
        {
            DeRef(_7480);
            _7480 = NOVALUE;
            goto L27; // [861] 882
        }
        else{
            DeRef(_7480);
            _7480 = NOVALUE;
        }

        /**                                 cur_in = -6*/
        _cur_in_13053 = -6;

        /**                                 line_in[i-1] = -6*/
        _7481 = _i_13058 - 1;
        _2 = (int)SEQ_PTR(_line_in_12911);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_12911 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _7481);
        _1 = *(int *)_2;
        *(int *)_2 = -6;
        DeRef(_1);
L27: 
L23: 
L21: 
L20: 

        /**                         line_in[i] = cur_in*/
        _2 = (int)SEQ_PTR(_line_in_12911);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_12911 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13058);
        _1 = *(int *)_2;
        *(int *)_2 = _cur_in_13053;
        DeRef(_1);

        /**                         last_in = cur_in*/
        _last_in_13052 = _cur_in_13053;

        /**                     end for*/
        _i_13058 = _i_13058 + 1;
        goto L1D; // [900] 751
L1E: 
        ;
    }
L1C: 

    /** 			end while*/
    goto L16; // [910] 584

    /** 		end while*/
    goto LB; // [915] 152
    goto LC; // [918] 1062
L9: 

    /** 		io:seek(file_handle, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at925_13081);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_handle_12910;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at925_13081 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_925_13080 = machine(19, _seek_1__tmp_at925_13081);
    DeRefi(_seek_1__tmp_at925_13081);
    _seek_1__tmp_at925_13081 = NOVALUE;

    /** 		line_in  = serialize:deserialize(file_handle)*/
    _0 = _line_in_12911;
    _line_in_12911 = _21deserialize(_file_handle_12910, 1);
    DeRef(_0);

    /** 		if atom(line_in) then*/
    _7483 = IS_ATOM(_line_in_12911);
    if (_7483 == 0)
    {
        _7483 = NOVALUE;
        goto L28; // [948] 958
    }
    else{
        _7483 = NOVALUE;
    }

    /** 			return -2*/
    DeRef(_input_file_name_12909);
    DeRef(_line_in_12911);
    DeRef(_logical_line_12912);
    DeRef(_data_value_12915);
    DeRef(_data_key_12916);
    DeRef(_conv_res_12917);
    DeRef(_new_map_12918);
    DeRefi(_line_conts_12919);
    DeRef(_7400);
    _7400 = NOVALUE;
    DeRef(_7423);
    _7423 = NOVALUE;
    DeRef(_7434);
    _7434 = NOVALUE;
    DeRef(_7428);
    _7428 = NOVALUE;
    DeRef(_7453);
    _7453 = NOVALUE;
    DeRef(_7478);
    _7478 = NOVALUE;
    DeRef(_7481);
    _7481 = NOVALUE;
    return -2;
L28: 

    /** 		if length(line_in) > 1 then*/
    if (IS_SEQUENCE(_line_in_12911)){
            _7484 = SEQ_PTR(_line_in_12911)->length;
    }
    else {
        _7484 = 1;
    }
    if (_7484 <= 1)
    goto L29; // [963] 1054

    /** 			switch line_in[1] do*/
    _2 = (int)SEQ_PTR(_line_in_12911);
    _7486 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_7486) ){
        goto L2A; // [973] 1042
    }
    if(!IS_ATOM_INT(_7486)){
        if( (DBL_PTR(_7486)->dbl != (double) ((int) DBL_PTR(_7486)->dbl) ) ){
            goto L2A; // [973] 1042
        }
        _0 = (int) DBL_PTR(_7486)->dbl;
    }
    else {
        _0 = _7486;
    };
    _7486 = NOVALUE;
    switch ( _0 ){ 

        /** 				case 1, 2 then*/
        case 1:
        case 2:

        /** 					data_key   = serialize:deserialize(file_handle)*/
        _0 = _data_key_12916;
        _data_key_12916 = _21deserialize(_file_handle_12910, 1);
        DeRef(_0);

        /** 					data_value =  serialize:deserialize(file_handle)*/
        _0 = _data_value_12915;
        _data_value_12915 = _21deserialize(_file_handle_12910, 1);
        DeRef(_0);

        /** 					for i = 1 to length(data_key) do*/
        if (IS_SEQUENCE(_data_key_12916)){
                _7491 = SEQ_PTR(_data_key_12916)->length;
        }
        else {
            _7491 = 1;
        }
        {
            int _i_13095;
            _i_13095 = 1;
L2B: 
            if (_i_13095 > _7491){
                goto L2C; // [1003] 1038
            }

            /** 						put(new_map, data_key[i], data_value[i])*/
            _2 = (int)SEQ_PTR(_data_key_12916);
            _7492 = (int)*(((s1_ptr)_2)->base + _i_13095);
            _2 = (int)SEQ_PTR(_data_value_12915);
            _7493 = (int)*(((s1_ptr)_2)->base + _i_13095);
            Ref(_new_map_12918);
            Ref(_7492);
            Ref(_7493);
            _25put(_new_map_12918, _7492, _7493, 1, _25threshold_size_11951);
            _7492 = NOVALUE;
            _7493 = NOVALUE;

            /** 					end for*/
            _i_13095 = _i_13095 + 1;
            goto L2B; // [1033] 1010
L2C: 
            ;
        }
        goto L2D; // [1038] 1061

        /** 				case else*/
        default:
L2A: 

        /** 					return -2*/
        DeRef(_input_file_name_12909);
        DeRef(_line_in_12911);
        DeRef(_logical_line_12912);
        DeRef(_data_value_12915);
        DeRef(_data_key_12916);
        DeRef(_conv_res_12917);
        DeRef(_new_map_12918);
        DeRefi(_line_conts_12919);
        DeRef(_7400);
        _7400 = NOVALUE;
        DeRef(_7423);
        _7423 = NOVALUE;
        DeRef(_7434);
        _7434 = NOVALUE;
        DeRef(_7428);
        _7428 = NOVALUE;
        DeRef(_7453);
        _7453 = NOVALUE;
        DeRef(_7478);
        _7478 = NOVALUE;
        DeRef(_7481);
        _7481 = NOVALUE;
        return -2;
    ;}    goto L2D; // [1051] 1061
L29: 

    /** 			return -2*/
    DeRef(_input_file_name_12909);
    DeRef(_line_in_12911);
    DeRef(_logical_line_12912);
    DeRef(_data_value_12915);
    DeRef(_data_key_12916);
    DeRef(_conv_res_12917);
    DeRef(_new_map_12918);
    DeRefi(_line_conts_12919);
    DeRef(_7400);
    _7400 = NOVALUE;
    DeRef(_7423);
    _7423 = NOVALUE;
    DeRef(_7434);
    _7434 = NOVALUE;
    DeRef(_7428);
    _7428 = NOVALUE;
    DeRef(_7453);
    _7453 = NOVALUE;
    DeRef(_7478);
    _7478 = NOVALUE;
    DeRef(_7481);
    _7481 = NOVALUE;
    return -2;
L2D: 
LC: 

    /** 	if sequence(input_file_name) then*/
    _7494 = IS_SEQUENCE(_input_file_name_12909);
    if (_7494 == 0)
    {
        _7494 = NOVALUE;
        goto L2E; // [1067] 1075
    }
    else{
        _7494 = NOVALUE;
    }

    /** 		close(file_handle)*/
    EClose(_file_handle_12910);
L2E: 

    /** 	optimize(new_map)*/
    Ref(_new_map_12918);
    RefDS(_7343);
    _25optimize(_new_map_12918, _25threshold_size_11951, _7343);

    /** 	return new_map*/
    DeRef(_input_file_name_12909);
    DeRef(_line_in_12911);
    DeRef(_logical_line_12912);
    DeRef(_data_value_12915);
    DeRef(_data_key_12916);
    DeRef(_conv_res_12917);
    DeRefi(_line_conts_12919);
    DeRef(_7400);
    _7400 = NOVALUE;
    DeRef(_7423);
    _7423 = NOVALUE;
    DeRef(_7434);
    _7434 = NOVALUE;
    DeRef(_7428);
    _7428 = NOVALUE;
    DeRef(_7453);
    _7453 = NOVALUE;
    DeRef(_7478);
    _7478 = NOVALUE;
    DeRef(_7481);
    _7481 = NOVALUE;
    return _new_map_12918;
    ;
}


int _25save_map(int _the_map__13108, int _file_name_p_13109, int _type__13110)
{
    int _file_handle__13111 = NOVALUE;
    int _keys__13112 = NOVALUE;
    int _values__13113 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_188_13146 = NOVALUE;
    int _options_inlined_pretty_sprint_at_185_13145 = NOVALUE;
    int _x_inlined_pretty_sprint_at_182_13144 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_344_13164 = NOVALUE;
    int _options_inlined_pretty_sprint_at_341_13163 = NOVALUE;
    int _x_inlined_pretty_sprint_at_338_13162 = NOVALUE;
    int _7546 = NOVALUE;
    int _7545 = NOVALUE;
    int _7544 = NOVALUE;
    int _7543 = NOVALUE;
    int _7542 = NOVALUE;
    int _7540 = NOVALUE;
    int _7539 = NOVALUE;
    int _7538 = NOVALUE;
    int _7537 = NOVALUE;
    int _7536 = NOVALUE;
    int _7535 = NOVALUE;
    int _7534 = NOVALUE;
    int _7533 = NOVALUE;
    int _7532 = NOVALUE;
    int _7531 = NOVALUE;
    int _7530 = NOVALUE;
    int _7529 = NOVALUE;
    int _7528 = NOVALUE;
    int _7527 = NOVALUE;
    int _7526 = NOVALUE;
    int _7525 = NOVALUE;
    int _7524 = NOVALUE;
    int _7523 = NOVALUE;
    int _7522 = NOVALUE;
    int _7521 = NOVALUE;
    int _7520 = NOVALUE;
    int _7519 = NOVALUE;
    int _7518 = NOVALUE;
    int _7517 = NOVALUE;
    int _7516 = NOVALUE;
    int _7515 = NOVALUE;
    int _7514 = NOVALUE;
    int _7513 = NOVALUE;
    int _7512 = NOVALUE;
    int _7511 = NOVALUE;
    int _7510 = NOVALUE;
    int _7509 = NOVALUE;
    int _7508 = NOVALUE;
    int _7507 = NOVALUE;
    int _7506 = NOVALUE;
    int _7504 = NOVALUE;
    int _7496 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_type__13110)) {
        _1 = (long)(DBL_PTR(_type__13110)->dbl);
        if (UNIQUE(DBL_PTR(_type__13110)) && (DBL_PTR(_type__13110)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type__13110);
        _type__13110 = _1;
    }

    /** 	integer file_handle_ = -2*/
    _file_handle__13111 = -2;

    /** 	if sequence(file_name_p) then*/
    _7496 = IS_SEQUENCE(_file_name_p_13109);
    if (_7496 == 0)
    {
        _7496 = NOVALUE;
        goto L1; // [17] 53
    }
    else{
        _7496 = NOVALUE;
    }

    /** 		if type_ = SM_TEXT then*/
    if (_type__13110 != 1)
    goto L2; // [24] 40

    /** 			file_handle_ = open(file_name_p, "w")*/
    _file_handle__13111 = EOpen(_file_name_p_13109, _4110, 0);
    goto L3; // [37] 63
L2: 

    /** 			file_handle_ = open(file_name_p, "wb")*/
    _file_handle__13111 = EOpen(_file_name_p_13109, _2908, 0);
    goto L3; // [50] 63
L1: 

    /** 		file_handle_ = file_name_p*/
    Ref(_file_name_p_13109);
    _file_handle__13111 = _file_name_p_13109;
    if (!IS_ATOM_INT(_file_handle__13111)) {
        _1 = (long)(DBL_PTR(_file_handle__13111)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle__13111)) && (DBL_PTR(_file_handle__13111)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle__13111);
        _file_handle__13111 = _1;
    }
L3: 

    /** 	if file_handle_ < 0 then*/
    if (_file_handle__13111 >= 0)
    goto L4; // [65] 76

    /** 		return -1*/
    DeRef(_the_map__13108);
    DeRef(_file_name_p_13109);
    DeRef(_keys__13112);
    DeRef(_values__13113);
    return -1;
L4: 

    /** 	keys_ = keys(the_map_)*/
    Ref(_the_map__13108);
    _0 = _keys__13112;
    _keys__13112 = _25keys(_the_map__13108, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    Ref(_the_map__13108);
    _0 = _values__13113;
    _values__13113 = _25values(_the_map__13108, 0, 0);
    DeRef(_0);

    /** 	if type_ = SM_RAW then*/
    if (_type__13110 != 2)
    goto L5; // [99] 151

    /** 		puts(file_handle_, serialize:serialize({*/
    _7504 = _9now_gmt();
    RefDS(_7505);
    _7506 = _9format(_7504, _7505);
    _7504 = NOVALUE;
    _7507 = _29version_string(0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = _7506;
    *((int *)(_2+12)) = _7507;
    _7508 = MAKE_SEQ(_1);
    _7507 = NOVALUE;
    _7506 = NOVALUE;
    _7509 = _21serialize(_7508);
    _7508 = NOVALUE;
    EPuts(_file_handle__13111, _7509); // DJP 
    DeRef(_7509);
    _7509 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(keys_))*/
    RefDS(_keys__13112);
    _7510 = _21serialize(_keys__13112);
    EPuts(_file_handle__13111, _7510); // DJP 
    DeRef(_7510);
    _7510 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(values_))*/
    RefDS(_values__13113);
    _7511 = _21serialize(_values__13113);
    EPuts(_file_handle__13111, _7511); // DJP 
    DeRef(_7511);
    _7511 = NOVALUE;
    goto L6; // [148] 501
L5: 

    /** 		for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__13112)){
            _7512 = SEQ_PTR(_keys__13112)->length;
    }
    else {
        _7512 = 1;
    }
    {
        int _i_13139;
        _i_13139 = 1;
L7: 
        if (_i_13139 > _7512){
            goto L8; // [156] 500
        }

        /** 			keys_[i] = pretty:pretty_sprint(keys_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7513 = (int)*(((s1_ptr)_2)->base + _i_13139);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_106);
        *((int *)(_2+20)) = _106;
        RefDS(_3816);
        *((int *)(_2+24)) = _3816;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _7514 = MAKE_SEQ(_1);
        Ref(_7513);
        DeRef(_x_inlined_pretty_sprint_at_182_13144);
        _x_inlined_pretty_sprint_at_182_13144 = _7513;
        _7513 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_185_13145);
        _options_inlined_pretty_sprint_at_185_13145 = _7514;
        _7514 = NOVALUE;

        /** 	pretty_printing = 0*/
        _2pretty_printing_157 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_182_13144);
        RefDS(_options_inlined_pretty_sprint_at_185_13145);
        _2pretty(_x_inlined_pretty_sprint_at_182_13144, _options_inlined_pretty_sprint_at_185_13145);

        /** 	return pretty_line*/
        RefDS(_2pretty_line_160);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_188_13146);
        _pretty_sprint_inlined_pretty_sprint_at_188_13146 = _2pretty_line_160;
        DeRef(_x_inlined_pretty_sprint_at_182_13144);
        _x_inlined_pretty_sprint_at_182_13144 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_185_13145);
        _options_inlined_pretty_sprint_at_185_13145 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_188_13146);
        _2 = (int)SEQ_PTR(_keys__13112);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__13112 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_188_13146;
        DeRef(_1);

        /** 			keys_[i] = search:match_replace("#", keys_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7515 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_400);
        Ref(_7515);
        RefDS(_7386);
        _7516 = _6match_replace(_400, _7515, _7386, 0);
        _7515 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__13112);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__13112 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7516;
        if( _1 != _7516 ){
            DeRef(_1);
        }
        _7516 = NOVALUE;

        /** 			keys_[i] = search:match_replace("\"", keys_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7517 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_3328);
        Ref(_7517);
        RefDS(_7409);
        _7518 = _6match_replace(_3328, _7517, _7409, 0);
        _7517 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__13112);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__13112 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7518;
        if( _1 != _7518 ){
            DeRef(_1);
        }
        _7518 = NOVALUE;

        /** 			keys_[i] = search:match_replace("$", keys_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7519 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_7407);
        Ref(_7519);
        RefDS(_7389);
        _7520 = _6match_replace(_7407, _7519, _7389, 0);
        _7519 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__13112);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__13112 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7520;
        if( _1 != _7520 ){
            DeRef(_1);
        }
        _7520 = NOVALUE;

        /** 			keys_[i] = search:match_replace(",", keys_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7521 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_7405);
        Ref(_7521);
        RefDS(_7393);
        _7522 = _6match_replace(_7405, _7521, _7393, 0);
        _7521 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__13112);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__13112 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7522;
        if( _1 != _7522 ){
            DeRef(_1);
        }
        _7522 = NOVALUE;

        /** 			keys_[i] = search:match_replace("=", keys_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7523 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_1156);
        Ref(_7523);
        RefDS(_7383);
        _7524 = _6match_replace(_1156, _7523, _7383, 0);
        _7523 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__13112);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__13112 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7524;
        if( _1 != _7524 ){
            DeRef(_1);
        }
        _7524 = NOVALUE;

        /** 			keys_[i] = search:match_replace("-", keys_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7525 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_6130);
        Ref(_7525);
        RefDS(_7396);
        _7526 = _6match_replace(_6130, _7525, _7396, 0);
        _7525 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__13112);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__13112 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7526;
        if( _1 != _7526 ){
            DeRef(_1);
        }
        _7526 = NOVALUE;

        /** 			values_[i] = pretty:pretty_sprint(values_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_values__13113);
        _7527 = (int)*(((s1_ptr)_2)->base + _i_13139);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_106);
        *((int *)(_2+20)) = _106;
        RefDS(_3816);
        *((int *)(_2+24)) = _3816;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _7528 = MAKE_SEQ(_1);
        Ref(_7527);
        DeRef(_x_inlined_pretty_sprint_at_338_13162);
        _x_inlined_pretty_sprint_at_338_13162 = _7527;
        _7527 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_341_13163);
        _options_inlined_pretty_sprint_at_341_13163 = _7528;
        _7528 = NOVALUE;

        /** 	pretty_printing = 0*/
        _2pretty_printing_157 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_338_13162);
        RefDS(_options_inlined_pretty_sprint_at_341_13163);
        _2pretty(_x_inlined_pretty_sprint_at_338_13162, _options_inlined_pretty_sprint_at_341_13163);

        /** 	return pretty_line*/
        RefDS(_2pretty_line_160);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_344_13164);
        _pretty_sprint_inlined_pretty_sprint_at_344_13164 = _2pretty_line_160;
        DeRef(_x_inlined_pretty_sprint_at_338_13162);
        _x_inlined_pretty_sprint_at_338_13162 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_341_13163);
        _options_inlined_pretty_sprint_at_341_13163 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_344_13164);
        _2 = (int)SEQ_PTR(_values__13113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__13113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_344_13164;
        DeRef(_1);

        /** 			values_[i] = search:match_replace("#", values_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_values__13113);
        _7529 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_400);
        Ref(_7529);
        RefDS(_7386);
        _7530 = _6match_replace(_400, _7529, _7386, 0);
        _7529 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__13113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__13113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7530;
        if( _1 != _7530 ){
            DeRef(_1);
        }
        _7530 = NOVALUE;

        /** 			values_[i] = search:match_replace("\"", values_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_values__13113);
        _7531 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_3328);
        Ref(_7531);
        RefDS(_7409);
        _7532 = _6match_replace(_3328, _7531, _7409, 0);
        _7531 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__13113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__13113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7532;
        if( _1 != _7532 ){
            DeRef(_1);
        }
        _7532 = NOVALUE;

        /** 			values_[i] = search:match_replace("$", values_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_values__13113);
        _7533 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_7407);
        Ref(_7533);
        RefDS(_7389);
        _7534 = _6match_replace(_7407, _7533, _7389, 0);
        _7533 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__13113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__13113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7534;
        if( _1 != _7534 ){
            DeRef(_1);
        }
        _7534 = NOVALUE;

        /** 			values_[i] = search:match_replace(",", values_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_values__13113);
        _7535 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_7405);
        Ref(_7535);
        RefDS(_7393);
        _7536 = _6match_replace(_7405, _7535, _7393, 0);
        _7535 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__13113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__13113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7536;
        if( _1 != _7536 ){
            DeRef(_1);
        }
        _7536 = NOVALUE;

        /** 			values_[i] = search:match_replace("=", values_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_values__13113);
        _7537 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_1156);
        Ref(_7537);
        RefDS(_7383);
        _7538 = _6match_replace(_1156, _7537, _7383, 0);
        _7537 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__13113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__13113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7538;
        if( _1 != _7538 ){
            DeRef(_1);
        }
        _7538 = NOVALUE;

        /** 			values_[i] = search:match_replace("-", values_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_values__13113);
        _7539 = (int)*(((s1_ptr)_2)->base + _i_13139);
        RefDS(_6130);
        Ref(_7539);
        RefDS(_7396);
        _7540 = _6match_replace(_6130, _7539, _7396, 0);
        _7539 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__13113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__13113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_13139);
        _1 = *(int *)_2;
        *(int *)_2 = _7540;
        if( _1 != _7540 ){
            DeRef(_1);
        }
        _7540 = NOVALUE;

        /** 			printf(file_handle_, "%s = %s\n", {keys_[i], values_[i]})*/
        _2 = (int)SEQ_PTR(_keys__13112);
        _7542 = (int)*(((s1_ptr)_2)->base + _i_13139);
        _2 = (int)SEQ_PTR(_values__13113);
        _7543 = (int)*(((s1_ptr)_2)->base + _i_13139);
        Ref(_7543);
        Ref(_7542);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _7542;
        ((int *)_2)[2] = _7543;
        _7544 = MAKE_SEQ(_1);
        _7543 = NOVALUE;
        _7542 = NOVALUE;
        EPrintf(_file_handle__13111, _7541, _7544);
        DeRefDS(_7544);
        _7544 = NOVALUE;

        /** 		end for*/
        _i_13139 = _i_13139 + 1;
        goto L7; // [495] 163
L8: 
        ;
    }
L6: 

    /** 	if sequence(file_name_p) then*/
    _7545 = IS_SEQUENCE(_file_name_p_13109);
    if (_7545 == 0)
    {
        _7545 = NOVALUE;
        goto L9; // [506] 514
    }
    else{
        _7545 = NOVALUE;
    }

    /** 		close(file_handle_)*/
    EClose(_file_handle__13111);
L9: 

    /** 	return length(keys_)*/
    if (IS_SEQUENCE(_keys__13112)){
            _7546 = SEQ_PTR(_keys__13112)->length;
    }
    else {
        _7546 = 1;
    }
    DeRef(_the_map__13108);
    DeRef(_file_name_p_13109);
    DeRefDS(_keys__13112);
    DeRef(_values__13113);
    return _7546;
    ;
}


int _25copy(int _source_map_13186, int _dest_map_13187, int _put_operation_13188)
{
    int _keys_set_13191 = NOVALUE;
    int _value_set_13192 = NOVALUE;
    int _source_data_13193 = NOVALUE;
    int _temp_map_13225 = NOVALUE;
    int _7570 = NOVALUE;
    int _7568 = NOVALUE;
    int _7567 = NOVALUE;
    int _7566 = NOVALUE;
    int _7565 = NOVALUE;
    int _7563 = NOVALUE;
    int _7562 = NOVALUE;
    int _7561 = NOVALUE;
    int _7560 = NOVALUE;
    int _7559 = NOVALUE;
    int _7558 = NOVALUE;
    int _7557 = NOVALUE;
    int _7555 = NOVALUE;
    int _7553 = NOVALUE;
    int _7552 = NOVALUE;
    int _7551 = NOVALUE;
    int _7549 = NOVALUE;
    int _7547 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_put_operation_13188)) {
        _1 = (long)(DBL_PTR(_put_operation_13188)->dbl);
        if (UNIQUE(DBL_PTR(_put_operation_13188)) && (DBL_PTR(_put_operation_13188)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_put_operation_13188);
        _put_operation_13188 = _1;
    }

    /** 	if map(dest_map) then*/
    Ref(_dest_map_13187);
    _7547 = _25map(_dest_map_13187);
    if (_7547 == 0) {
        DeRef(_7547);
        _7547 = NOVALUE;
        goto L1; // [11] 221
    }
    else {
        if (!IS_ATOM_INT(_7547) && DBL_PTR(_7547)->dbl == 0.0){
            DeRef(_7547);
            _7547 = NOVALUE;
            goto L1; // [11] 221
        }
        DeRef(_7547);
        _7547 = NOVALUE;
    }
    DeRef(_7547);
    _7547 = NOVALUE;

    /** 		sequence keys_set*/

    /** 		sequence value_set		*/

    /** 		sequence source_data*/

    /** 		source_data = eumem:ram_space[source_map]	*/
    DeRef(_source_data_13193);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_source_map_13186)){
        _source_data_13193 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_13186)->dbl));
    }
    else{
        _source_data_13193 = (int)*(((s1_ptr)_2)->base + _source_map_13186);
    }
    Ref(_source_data_13193);

    /** 		if source_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_source_data_13193);
    _7549 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7549, 76)){
        _7549 = NOVALUE;
        goto L2; // [38] 136
    }
    _7549 = NOVALUE;

    /** 			for index = 1 to length(source_data[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_source_data_13193);
    _7551 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7551)){
            _7552 = SEQ_PTR(_7551)->length;
    }
    else {
        _7552 = 1;
    }
    _7551 = NOVALUE;
    {
        int _index_13199;
        _index_13199 = 1;
L3: 
        if (_index_13199 > _7552){
            goto L4; // [53] 133
        }

        /** 				keys_set = source_data[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_13193);
        _7553 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_keys_set_13191);
        _2 = (int)SEQ_PTR(_7553);
        _keys_set_13191 = (int)*(((s1_ptr)_2)->base + _index_13199);
        Ref(_keys_set_13191);
        _7553 = NOVALUE;

        /** 				value_set = source_data[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_13193);
        _7555 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_set_13192);
        _2 = (int)SEQ_PTR(_7555);
        _value_set_13192 = (int)*(((s1_ptr)_2)->base + _index_13199);
        Ref(_value_set_13192);
        _7555 = NOVALUE;

        /** 				for j = 1 to length(keys_set) do*/
        if (IS_SEQUENCE(_keys_set_13191)){
                _7557 = SEQ_PTR(_keys_set_13191)->length;
        }
        else {
            _7557 = 1;
        }
        {
            int _j_13207;
            _j_13207 = 1;
L5: 
            if (_j_13207 > _7557){
                goto L6; // [93] 126
            }

            /** 					put(dest_map, keys_set[j], value_set[j], put_operation)*/
            _2 = (int)SEQ_PTR(_keys_set_13191);
            _7558 = (int)*(((s1_ptr)_2)->base + _j_13207);
            _2 = (int)SEQ_PTR(_value_set_13192);
            _7559 = (int)*(((s1_ptr)_2)->base + _j_13207);
            Ref(_dest_map_13187);
            Ref(_7558);
            Ref(_7559);
            _25put(_dest_map_13187, _7558, _7559, _put_operation_13188, _25threshold_size_11951);
            _7558 = NOVALUE;
            _7559 = NOVALUE;

            /** 				end for*/
            _j_13207 = _j_13207 + 1;
            goto L5; // [121] 100
L6: 
            ;
        }

        /** 			end for*/
        _index_13199 = _index_13199 + 1;
        goto L3; // [128] 60
L4: 
        ;
    }
    goto L7; // [133] 210
L2: 

    /** 			for index = 1 to length(source_data[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_source_data_13193);
    _7560 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7560)){
            _7561 = SEQ_PTR(_7560)->length;
    }
    else {
        _7561 = 1;
    }
    _7560 = NOVALUE;
    {
        int _index_13213;
        _index_13213 = 1;
L8: 
        if (_index_13213 > _7561){
            goto L9; // [147] 209
        }

        /** 				if source_data[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_source_data_13193);
        _7562 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7562);
        _7563 = (int)*(((s1_ptr)_2)->base + _index_13213);
        _7562 = NOVALUE;
        if (binary_op_a(EQUALS, _7563, 0)){
            _7563 = NOVALUE;
            goto LA; // [166] 202
        }
        _7563 = NOVALUE;

        /** 					put(dest_map, source_data[KEY_LIST][index], */
        _2 = (int)SEQ_PTR(_source_data_13193);
        _7565 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7565);
        _7566 = (int)*(((s1_ptr)_2)->base + _index_13213);
        _7565 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_data_13193);
        _7567 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7567);
        _7568 = (int)*(((s1_ptr)_2)->base + _index_13213);
        _7567 = NOVALUE;
        Ref(_dest_map_13187);
        Ref(_7566);
        Ref(_7568);
        _25put(_dest_map_13187, _7566, _7568, _put_operation_13188, _25threshold_size_11951);
        _7566 = NOVALUE;
        _7568 = NOVALUE;
LA: 

        /** 			end for*/
        _index_13213 = _index_13213 + 1;
        goto L8; // [204] 154
L9: 
        ;
    }
L7: 

    /** 		return dest_map*/
    DeRef(_keys_set_13191);
    DeRef(_value_set_13192);
    DeRef(_source_data_13193);
    DeRef(_source_map_13186);
    _7551 = NOVALUE;
    _7560 = NOVALUE;
    return _dest_map_13187;
    goto LB; // [218] 251
L1: 

    /** 		atom temp_map = eumem:malloc()*/
    _0 = _temp_map_13225;
    _temp_map_13225 = _26malloc(1, 1);
    DeRef(_0);

    /** 	 	eumem:ram_space[temp_map] = eumem:ram_space[source_map]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_source_map_13186)){
        _7570 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_13186)->dbl));
    }
    else{
        _7570 = (int)*(((s1_ptr)_2)->base + _source_map_13186);
    }
    Ref(_7570);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map_13225))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map_13225)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map_13225);
    _1 = *(int *)_2;
    *(int *)_2 = _7570;
    if( _1 != _7570 ){
        DeRef(_1);
    }
    _7570 = NOVALUE;

    /** 		return temp_map*/
    DeRef(_source_map_13186);
    DeRef(_dest_map_13187);
    _7551 = NOVALUE;
    _7560 = NOVALUE;
    return _temp_map_13225;
    DeRef(_temp_map_13225);
    _temp_map_13225 = NOVALUE;
LB: 
    ;
}


int _25new_from_kvpairs(int _kv_pairs_13230)
{
    int _new_map_13231 = NOVALUE;
    int _7582 = NOVALUE;
    int _7581 = NOVALUE;
    int _7580 = NOVALUE;
    int _7579 = NOVALUE;
    int _7577 = NOVALUE;
    int _7576 = NOVALUE;
    int _7575 = NOVALUE;
    int _7573 = NOVALUE;
    int _7572 = NOVALUE;
    int _7571 = NOVALUE;
    int _0, _1, _2;
    

    /** 	new_map = new( floor(7 * length(kv_pairs) / 2) )*/
    if (IS_SEQUENCE(_kv_pairs_13230)){
            _7571 = SEQ_PTR(_kv_pairs_13230)->length;
    }
    else {
        _7571 = 1;
    }
    if (_7571 <= INT15)
    _7572 = 7 * _7571;
    else
    _7572 = NewDouble(7 * (double)_7571);
    _7571 = NOVALUE;
    if (IS_ATOM_INT(_7572)) {
        _7573 = _7572 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _7572, 2);
        _7573 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_7572);
    _7572 = NOVALUE;
    _0 = _new_map_13231;
    _new_map_13231 = _25new(_7573);
    DeRef(_0);
    _7573 = NOVALUE;

    /** 	for i = 1 to length(kv_pairs) do*/
    if (IS_SEQUENCE(_kv_pairs_13230)){
            _7575 = SEQ_PTR(_kv_pairs_13230)->length;
    }
    else {
        _7575 = 1;
    }
    {
        int _i_13237;
        _i_13237 = 1;
L1: 
        if (_i_13237 > _7575){
            goto L2; // [25] 82
        }

        /** 		if length(kv_pairs[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_kv_pairs_13230);
        _7576 = (int)*(((s1_ptr)_2)->base + _i_13237);
        if (IS_SEQUENCE(_7576)){
                _7577 = SEQ_PTR(_7576)->length;
        }
        else {
            _7577 = 1;
        }
        _7576 = NOVALUE;
        if (_7577 != 2)
        goto L3; // [41] 75

        /** 			put(new_map, kv_pairs[i][1], kv_pairs[i][2])*/
        _2 = (int)SEQ_PTR(_kv_pairs_13230);
        _7579 = (int)*(((s1_ptr)_2)->base + _i_13237);
        _2 = (int)SEQ_PTR(_7579);
        _7580 = (int)*(((s1_ptr)_2)->base + 1);
        _7579 = NOVALUE;
        _2 = (int)SEQ_PTR(_kv_pairs_13230);
        _7581 = (int)*(((s1_ptr)_2)->base + _i_13237);
        _2 = (int)SEQ_PTR(_7581);
        _7582 = (int)*(((s1_ptr)_2)->base + 2);
        _7581 = NOVALUE;
        Ref(_new_map_13231);
        Ref(_7580);
        Ref(_7582);
        _25put(_new_map_13231, _7580, _7582, 1, _25threshold_size_11951);
        _7580 = NOVALUE;
        _7582 = NOVALUE;
L3: 

        /** 	end for*/
        _i_13237 = _i_13237 + 1;
        goto L1; // [77] 32
L2: 
        ;
    }

    /** 	return new_map	*/
    DeRefDS(_kv_pairs_13230);
    _7576 = NOVALUE;
    return _new_map_13231;
    ;
}


int _25new_from_string(int _kv_string_13249)
{
    int _7584 = NOVALUE;
    int _7583 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return new_from_kvpairs( text:keyvalues (kv_string) )*/
    RefDS(_kv_string_13249);
    RefDS(_3151);
    RefDS(_3152);
    RefDS(_3153);
    RefDS(_232);
    _7583 = _16keyvalues(_kv_string_13249, _3151, _3152, _3153, _232, 1);
    _7584 = _25new_from_kvpairs(_7583);
    _7583 = NOVALUE;
    DeRefDS(_kv_string_13249);
    return _7584;
    ;
}


int _25for_each(int _source_map_13254, int _user_rid_13255, int _user_data_13256, int _in_sorted_order_13257, int _signal_boundary_13258)
{
    int _lKV_13259 = NOVALUE;
    int _lRes_13260 = NOVALUE;
    int _progress_code_13261 = NOVALUE;
    int _7603 = NOVALUE;
    int _7601 = NOVALUE;
    int _7600 = NOVALUE;
    int _7599 = NOVALUE;
    int _7598 = NOVALUE;
    int _7597 = NOVALUE;
    int _7595 = NOVALUE;
    int _7594 = NOVALUE;
    int _7593 = NOVALUE;
    int _7592 = NOVALUE;
    int _7591 = NOVALUE;
    int _7590 = NOVALUE;
    int _7589 = NOVALUE;
    int _7588 = NOVALUE;
    int _7587 = NOVALUE;
    int _7586 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_user_rid_13255)) {
        _1 = (long)(DBL_PTR(_user_rid_13255)->dbl);
        if (UNIQUE(DBL_PTR(_user_rid_13255)) && (DBL_PTR(_user_rid_13255)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_user_rid_13255);
        _user_rid_13255 = _1;
    }
    if (!IS_ATOM_INT(_in_sorted_order_13257)) {
        _1 = (long)(DBL_PTR(_in_sorted_order_13257)->dbl);
        if (UNIQUE(DBL_PTR(_in_sorted_order_13257)) && (DBL_PTR(_in_sorted_order_13257)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_in_sorted_order_13257);
        _in_sorted_order_13257 = _1;
    }
    if (!IS_ATOM_INT(_signal_boundary_13258)) {
        _1 = (long)(DBL_PTR(_signal_boundary_13258)->dbl);
        if (UNIQUE(DBL_PTR(_signal_boundary_13258)) && (DBL_PTR(_signal_boundary_13258)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_signal_boundary_13258);
        _signal_boundary_13258 = _1;
    }

    /** 	lKV = pairs(source_map, in_sorted_order)	*/
    Ref(_source_map_13254);
    _0 = _lKV_13259;
    _lKV_13259 = _25pairs(_source_map_13254, _in_sorted_order_13257);
    DeRef(_0);

    /** 	if length(lKV) = 0 and signal_boundary != 0 then*/
    if (IS_SEQUENCE(_lKV_13259)){
            _7586 = SEQ_PTR(_lKV_13259)->length;
    }
    else {
        _7586 = 1;
    }
    _7587 = (_7586 == 0);
    _7586 = NOVALUE;
    if (_7587 == 0) {
        goto L1; // [31] 61
    }
    _7589 = (_signal_boundary_13258 != 0);
    if (_7589 == 0)
    {
        DeRef(_7589);
        _7589 = NOVALUE;
        goto L1; // [40] 61
    }
    else{
        DeRef(_7589);
        _7589 = NOVALUE;
    }

    /** 		return call_func(user_rid, {0,0,user_data,0} )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    Ref(_user_data_13256);
    *((int *)(_2+12)) = _user_data_13256;
    *((int *)(_2+16)) = 0;
    _7590 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_7590);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_user_rid_13255].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    Ref(*(int *)(_2+16));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12), 
                        *(int *)(_2+16)
                         );
    DeRef(_7591);
    _7591 = _1;
    DeRefDS(_7590);
    _7590 = NOVALUE;
    DeRef(_source_map_13254);
    DeRef(_user_data_13256);
    DeRef(_lKV_13259);
    DeRef(_lRes_13260);
    DeRef(_7587);
    _7587 = NOVALUE;
    return _7591;
L1: 

    /** 	for i = 1 to length(lKV) do*/
    if (IS_SEQUENCE(_lKV_13259)){
            _7592 = SEQ_PTR(_lKV_13259)->length;
    }
    else {
        _7592 = 1;
    }
    {
        int _i_13271;
        _i_13271 = 1;
L2: 
        if (_i_13271 > _7592){
            goto L3; // [66] 164
        }

        /** 		if i = length(lKV) and signal_boundary then*/
        if (IS_SEQUENCE(_lKV_13259)){
                _7593 = SEQ_PTR(_lKV_13259)->length;
        }
        else {
            _7593 = 1;
        }
        _7594 = (_i_13271 == _7593);
        _7593 = NOVALUE;
        if (_7594 == 0) {
            goto L4; // [82] 102
        }
        if (_signal_boundary_13258 == 0)
        {
            goto L4; // [87] 102
        }
        else{
        }

        /** 			progress_code = -i*/
        _progress_code_13261 = - _i_13271;
        goto L5; // [99] 110
L4: 

        /** 			progress_code = i*/
        _progress_code_13261 = _i_13271;
L5: 

        /** 		lRes = call_func(user_rid, {lKV[i][1], lKV[i][2], user_data, progress_code})*/
        _2 = (int)SEQ_PTR(_lKV_13259);
        _7597 = (int)*(((s1_ptr)_2)->base + _i_13271);
        _2 = (int)SEQ_PTR(_7597);
        _7598 = (int)*(((s1_ptr)_2)->base + 1);
        _7597 = NOVALUE;
        _2 = (int)SEQ_PTR(_lKV_13259);
        _7599 = (int)*(((s1_ptr)_2)->base + _i_13271);
        _2 = (int)SEQ_PTR(_7599);
        _7600 = (int)*(((s1_ptr)_2)->base + 2);
        _7599 = NOVALUE;
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_7598);
        *((int *)(_2+4)) = _7598;
        Ref(_7600);
        *((int *)(_2+8)) = _7600;
        Ref(_user_data_13256);
        *((int *)(_2+12)) = _user_data_13256;
        *((int *)(_2+16)) = _progress_code_13261;
        _7601 = MAKE_SEQ(_1);
        _7600 = NOVALUE;
        _7598 = NOVALUE;
        _1 = (int)SEQ_PTR(_7601);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_user_rid_13255].addr;
        Ref(*(int *)(_2+4));
        Ref(*(int *)(_2+8));
        Ref(*(int *)(_2+12));
        Ref(*(int *)(_2+16));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4), 
                            *(int *)(_2+8), 
                            *(int *)(_2+12), 
                            *(int *)(_2+16)
                             );
        DeRef(_lRes_13260);
        _lRes_13260 = _1;
        DeRefDS(_7601);
        _7601 = NOVALUE;

        /** 		if not equal(lRes, 0) then*/
        if (_lRes_13260 == 0)
        _7603 = 1;
        else if (IS_ATOM_INT(_lRes_13260) && IS_ATOM_INT(0))
        _7603 = 0;
        else
        _7603 = (compare(_lRes_13260, 0) == 0);
        if (_7603 != 0)
        goto L6; // [147] 157
        _7603 = NOVALUE;

        /** 			return lRes*/
        DeRef(_source_map_13254);
        DeRef(_user_data_13256);
        DeRefDS(_lKV_13259);
        DeRef(_7587);
        _7587 = NOVALUE;
        DeRef(_7594);
        _7594 = NOVALUE;
        DeRef(_7591);
        _7591 = NOVALUE;
        return _lRes_13260;
L6: 

        /** 	end for*/
        _i_13271 = _i_13271 + 1;
        goto L2; // [159] 73
L3: 
        ;
    }

    /** 	return 0*/
    DeRef(_source_map_13254);
    DeRef(_user_data_13256);
    DeRef(_lKV_13259);
    DeRef(_lRes_13260);
    DeRef(_7587);
    _7587 = NOVALUE;
    DeRef(_7594);
    _7594 = NOVALUE;
    DeRef(_7591);
    _7591 = NOVALUE;
    return 0;
    ;
}


void _25convert_to_large_map(int _the_map__13290)
{
    int _temp_map__13291 = NOVALUE;
    int _map_handle__13292 = NOVALUE;
    int _7616 = NOVALUE;
    int _7615 = NOVALUE;
    int _7614 = NOVALUE;
    int _7613 = NOVALUE;
    int _7612 = NOVALUE;
    int _7610 = NOVALUE;
    int _7609 = NOVALUE;
    int _7608 = NOVALUE;
    int _7607 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_]*/
    DeRef(_temp_map__13291);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    _temp_map__13291 = (int)*(((s1_ptr)_2)->base + _the_map__13290);
    Ref(_temp_map__13291);

    /** 	map_handle_ = new()*/
    _0 = _map_handle__13292;
    _map_handle__13292 = _25new(690);
    DeRef(_0);

    /** 	for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__13291);
    _7607 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7607)){
            _7608 = SEQ_PTR(_7607)->length;
    }
    else {
        _7608 = 1;
    }
    _7607 = NOVALUE;
    {
        int _index_13296;
        _index_13296 = 1;
L1: 
        if (_index_13296 > _7608){
            goto L2; // [32] 96
        }

        /** 		if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__13291);
        _7609 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7609);
        _7610 = (int)*(((s1_ptr)_2)->base + _index_13296);
        _7609 = NOVALUE;
        if (binary_op_a(EQUALS, _7610, 0)){
            _7610 = NOVALUE;
            goto L3; // [51] 89
        }
        _7610 = NOVALUE;

        /** 			put(map_handle_, temp_map_[KEY_LIST][index], temp_map_[VALUE_LIST][index])*/
        _2 = (int)SEQ_PTR(_temp_map__13291);
        _7612 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7612);
        _7613 = (int)*(((s1_ptr)_2)->base + _index_13296);
        _7612 = NOVALUE;
        _2 = (int)SEQ_PTR(_temp_map__13291);
        _7614 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7614);
        _7615 = (int)*(((s1_ptr)_2)->base + _index_13296);
        _7614 = NOVALUE;
        Ref(_map_handle__13292);
        Ref(_7613);
        Ref(_7615);
        _25put(_map_handle__13292, _7613, _7615, 1, _25threshold_size_11951);
        _7613 = NOVALUE;
        _7615 = NOVALUE;
L3: 

        /** 	end for*/
        _index_13296 = _index_13296 + 1;
        goto L1; // [91] 39
L2: 
        ;
    }

    /** 	eumem:ram_space[the_map_] = eumem:ram_space[map_handle_]*/
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!IS_ATOM_INT(_map_handle__13292)){
        _7616 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_map_handle__13292)->dbl));
    }
    else{
        _7616 = (int)*(((s1_ptr)_2)->base + _map_handle__13292);
    }
    Ref(_7616);
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__13290);
    _1 = *(int *)_2;
    *(int *)_2 = _7616;
    if( _1 != _7616 ){
        DeRef(_1);
    }
    _7616 = NOVALUE;

    /** end procedure*/
    DeRef(_temp_map__13291);
    DeRef(_map_handle__13292);
    _7607 = NOVALUE;
    return;
    ;
}


void _25convert_to_small_map(int _the_map__13310)
{
    int _keys__13311 = NOVALUE;
    int _values__13312 = NOVALUE;
    int _7625 = NOVALUE;
    int _7624 = NOVALUE;
    int _7623 = NOVALUE;
    int _7622 = NOVALUE;
    int _7621 = NOVALUE;
    int _7620 = NOVALUE;
    int _7619 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map__13310)) {
        _1 = (long)(DBL_PTR(_the_map__13310)->dbl);
        if (UNIQUE(DBL_PTR(_the_map__13310)) && (DBL_PTR(_the_map__13310)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map__13310);
        _the_map__13310 = _1;
    }

    /** 	keys_ = keys(the_map_)*/
    _0 = _keys__13311;
    _keys__13311 = _25keys(_the_map__13310, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    _0 = _values__13312;
    _values__13312 = _25values(_the_map__13310, 0, 0);
    DeRef(_0);

    /** 	eumem:ram_space[the_map_] = {*/
    _7619 = Repeat(_25init_small_map_key_11952, _25threshold_size_11951);
    _7620 = Repeat(0, _25threshold_size_11951);
    _7621 = Repeat(0, _25threshold_size_11951);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_25type_is_map_11930);
    *((int *)(_2+4)) = _25type_is_map_11930;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _7619;
    *((int *)(_2+24)) = _7620;
    *((int *)(_2+28)) = _7621;
    _7622 = MAKE_SEQ(_1);
    _7621 = NOVALUE;
    _7620 = NOVALUE;
    _7619 = NOVALUE;
    _2 = (int)SEQ_PTR(_26ram_space_11041);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _26ram_space_11041 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__13310);
    _1 = *(int *)_2;
    *(int *)_2 = _7622;
    if( _1 != _7622 ){
        DeRef(_1);
    }
    _7622 = NOVALUE;

    /** 	for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__13311)){
            _7623 = SEQ_PTR(_keys__13311)->length;
    }
    else {
        _7623 = 1;
    }
    {
        int _i_13320;
        _i_13320 = 1;
L1: 
        if (_i_13320 > _7623){
            goto L2; // [65] 98
        }

        /** 		put(the_map_, keys_[i], values_[i], PUT, 0)*/
        _2 = (int)SEQ_PTR(_keys__13311);
        _7624 = (int)*(((s1_ptr)_2)->base + _i_13320);
        _2 = (int)SEQ_PTR(_values__13312);
        _7625 = (int)*(((s1_ptr)_2)->base + _i_13320);
        Ref(_7624);
        Ref(_7625);
        _25put(_the_map__13310, _7624, _7625, 1, 0);
        _7624 = NOVALUE;
        _7625 = NOVALUE;

        /** 	end for*/
        _i_13320 = _i_13320 + 1;
        goto L1; // [93] 72
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_keys__13311);
    DeRef(_values__13312);
    return;
    ;
}



// 0x8BEF3278
