// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _10open_dll(int _file_name_3839)
{
    int _fh_3849 = NOVALUE;
    int _1987 = NOVALUE;
    int _1985 = NOVALUE;
    int _1984 = NOVALUE;
    int _1983 = NOVALUE;
    int _1982 = NOVALUE;
    int _1981 = NOVALUE;
    int _1980 = NOVALUE;
    int _1979 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(file_name) > 0 and types:string(file_name) then*/
    if (IS_SEQUENCE(_file_name_3839)){
            _1979 = SEQ_PTR(_file_name_3839)->length;
    }
    else {
        _1979 = 1;
    }
    _1980 = (_1979 > 0);
    _1979 = NOVALUE;
    if (_1980 == 0) {
        goto L1; // [12] 35
    }
    RefDS(_file_name_3839);
    _1982 = _5string(_file_name_3839);
    if (_1982 == 0) {
        DeRef(_1982);
        _1982 = NOVALUE;
        goto L1; // [21] 35
    }
    else {
        if (!IS_ATOM_INT(_1982) && DBL_PTR(_1982)->dbl == 0.0){
            DeRef(_1982);
            _1982 = NOVALUE;
            goto L1; // [21] 35
        }
        DeRef(_1982);
        _1982 = NOVALUE;
    }
    DeRef(_1982);
    _1982 = NOVALUE;

    /** 		return machine_func(M_OPEN_DLL, file_name)*/
    _1983 = machine(50, _file_name_3839);
    DeRefDSi(_file_name_3839);
    DeRef(_1980);
    _1980 = NOVALUE;
    return _1983;
L1: 

    /** 	for idx = 1 to length(file_name) do*/
    if (IS_SEQUENCE(_file_name_3839)){
            _1984 = SEQ_PTR(_file_name_3839)->length;
    }
    else {
        _1984 = 1;
    }
    {
        int _idx_3847;
        _idx_3847 = 1;
L2: 
        if (_idx_3847 > _1984){
            goto L3; // [40] 82
        }

        /** 		atom fh = machine_func(M_OPEN_DLL, file_name[idx])*/
        _2 = (int)SEQ_PTR(_file_name_3839);
        _1985 = (int)*(((s1_ptr)_2)->base + _idx_3847);
        DeRef(_fh_3849);
        _fh_3849 = machine(50, _1985);
        _1985 = NOVALUE;

        /** 		if not fh = 0 then*/
        if (IS_ATOM_INT(_fh_3849)) {
            _1987 = (_fh_3849 == 0);
        }
        else {
            _1987 = unary_op(NOT, _fh_3849);
        }
        if (_1987 != 0)
        goto L4; // [62] 73

        /** 			return fh*/
        DeRefDSi(_file_name_3839);
        DeRef(_1980);
        _1980 = NOVALUE;
        DeRef(_1983);
        _1983 = NOVALUE;
        _1987 = NOVALUE;
        return _fh_3849;
L4: 
        DeRef(_fh_3849);
        _fh_3849 = NOVALUE;

        /** 	end for*/
        _idx_3847 = _idx_3847 + 1;
        goto L2; // [77] 47
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDSi(_file_name_3839);
    DeRef(_1980);
    _1980 = NOVALUE;
    DeRef(_1983);
    _1983 = NOVALUE;
    DeRef(_1987);
    _1987 = NOVALUE;
    return 0;
    ;
}


int _10define_c_proc(int _lib_3863, int _routine_name_3864, int _arg_types_3865)
{
    int _safe_address_inlined_safe_address_at_13_3870 = NOVALUE;
    int _msg_inlined_crash_at_28_3874 = NOVALUE;
    int _1996 = NOVALUE;
    int _1995 = NOVALUE;
    int _1993 = NOVALUE;
    int _1992 = NOVALUE;
    int _1991 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _1991 = 0;
    if (_1991 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_3870 = 1;
    _1993 = (1 == 0);
    if (_1993 == 0)
    {
        DeRef(_1993);
        _1993 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_1993);
        _1993 = NOVALUE;
    }

    /**         error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_3874);
    _msg_inlined_crash_at_28_3874 = EPrintf(-9999999, _1994, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_3874);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_3874);
    _msg_inlined_crash_at_28_3874 = NOVALUE;
L1: 

    /** 	return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, 0})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_3863);
    *((int *)(_2+4)) = _lib_3863;
    Ref(_routine_name_3864);
    *((int *)(_2+8)) = _routine_name_3864;
    RefDS(_arg_types_3865);
    *((int *)(_2+12)) = _arg_types_3865;
    *((int *)(_2+16)) = 0;
    _1995 = MAKE_SEQ(_1);
    _1996 = machine(51, _1995);
    DeRefDS(_1995);
    _1995 = NOVALUE;
    DeRef(_lib_3863);
    DeRef(_routine_name_3864);
    DeRefDSi(_arg_types_3865);
    return _1996;
    ;
}


int _10define_c_func(int _lib_3879, int _routine_name_3880, int _arg_types_3881, int _return_type_3882)
{
    int _safe_address_inlined_safe_address_at_13_3887 = NOVALUE;
    int _msg_inlined_crash_at_28_3890 = NOVALUE;
    int _2001 = NOVALUE;
    int _2000 = NOVALUE;
    int _1999 = NOVALUE;
    int _1998 = NOVALUE;
    int _1997 = NOVALUE;
    int _0, _1, _2;
    

    /** 	  if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _1997 = 0;
    if (_1997 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_3887 = 1;
    _1999 = (1 == 0);
    if (_1999 == 0)
    {
        DeRef(_1999);
        _1999 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_1999);
        _1999 = NOVALUE;
    }

    /** 	      error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_3890);
    _msg_inlined_crash_at_28_3890 = EPrintf(-9999999, _1994, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_3890);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_3890);
    _msg_inlined_crash_at_28_3890 = NOVALUE;
L1: 

    /** 	  return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, return_type})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_3879);
    *((int *)(_2+4)) = _lib_3879;
    Ref(_routine_name_3880);
    *((int *)(_2+8)) = _routine_name_3880;
    RefDS(_arg_types_3881);
    *((int *)(_2+12)) = _arg_types_3881;
    *((int *)(_2+16)) = _return_type_3882;
    _2000 = MAKE_SEQ(_1);
    _2001 = machine(51, _2000);
    DeRefDS(_2000);
    _2000 = NOVALUE;
    DeRef(_lib_3879);
    DeRefi(_routine_name_3880);
    DeRefDSi(_arg_types_3881);
    return _2001;
    ;
}



// 0xB9975B50
