// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _11allocate(int _n_3410, int _cleanup_3411)
{
    int _iaddr_3412 = NOVALUE;
    int _eaddr_3413 = NOVALUE;
    int _1811 = NOVALUE;
    int _1810 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE then*/

    /** 		iaddr = eu:machine_func( memconst:M_ALLOC, n + memory:BORDER_SPACE * 2)*/
    _1810 = 0;
    _1811 = _n_3410 + 0;
    _1810 = NOVALUE;
    DeRef(_iaddr_3412);
    _iaddr_3412 = machine(16, _1811);
    _1811 = NOVALUE;

    /** 		eaddr = memory:prepare_block( iaddr, n, PAGE_READ_WRITE )*/
    Ref(_iaddr_3412);
    _0 = _eaddr_3413;
    _eaddr_3413 = _13prepare_block(_iaddr_3412, _n_3410, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 	return eaddr*/
    DeRef(_iaddr_3412);
    return _eaddr_3413;
    ;
}


int _11allocate_data(int _n_3423, int _cleanup_3424)
{
    int _a_3425 = NOVALUE;
    int _sla_3427 = NOVALUE;
    int _1816 = NOVALUE;
    int _1815 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = eu:machine_func( memconst:M_ALLOC, n+BORDER_SPACE*2)*/
    _1815 = 0;
    _1816 = 4;
    _1815 = NOVALUE;
    DeRef(_a_3425);
    _a_3425 = machine(16, 4);
    _1816 = NOVALUE;

    /** 	sla = memory:prepare_block(a, n, PAGE_READ_WRITE )*/
    Ref(_a_3425);
    _0 = _sla_3427;
    _sla_3427 = _13prepare_block(_a_3425, 4, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 		return sla*/
    DeRef(_a_3425);
    return _sla_3427;
    ;
}


int _11VirtualAlloc(int _addr_3606, int _size_3607, int _allocation_type_3608, int _protect__3609)
{
    int _r1_3610 = NOVALUE;
    int _1899 = NOVALUE;
    int _0, _1, _2;
    

    /** 		r1 = c_func( VirtualAlloc_rid, {addr, size, allocation_type, protect_ } )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 1;
    Ref(_allocation_type_3608);
    *((int *)(_2+12)) = _allocation_type_3608;
    *((int *)(_2+16)) = 64;
    _1899 = MAKE_SEQ(_1);
    DeRef(_r1_3610);
    _r1_3610 = call_c(1, _11VirtualAlloc_rid_3523, _1899);
    DeRefDS(_1899);
    _1899 = NOVALUE;

    /** 		return r1*/
    DeRef(_allocation_type_3608);
    return _r1_3610;
    ;
}


int _11allocate_code(int _data_3622, int _wordsize_3623)
{
    int _1901 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return allocate_protect( data, wordsize, PAGE_EXECUTE )*/
    RefDS(_data_3622);
    _1901 = _11allocate_protect(_data_3622, 1, 16);
    DeRefDSi(_data_3622);
    return _1901;
    ;
}


int _11allocate_string(int _s_3629, int _cleanup_3630)
{
    int _mem_3631 = NOVALUE;
    int _1906 = NOVALUE;
    int _1905 = NOVALUE;
    int _1903 = NOVALUE;
    int _1902 = NOVALUE;
    int _0, _1, _2;
    

    /** 	mem = allocate( length(s) + 1) -- Thanks to Igor*/
    if (IS_SEQUENCE(_s_3629)){
            _1902 = SEQ_PTR(_s_3629)->length;
    }
    else {
        _1902 = 1;
    }
    _1903 = _1902 + 1;
    _1902 = NOVALUE;
    _0 = _mem_3631;
    _mem_3631 = _11allocate(_1903, 0);
    DeRef(_0);
    _1903 = NOVALUE;

    /** 	if mem then*/
    if (_mem_3631 == 0) {
        goto L1; // [19] 54
    }
    else {
        if (!IS_ATOM_INT(_mem_3631) && DBL_PTR(_mem_3631)->dbl == 0.0){
            goto L1; // [19] 54
        }
    }

    /** 		poke(mem, s)*/
    if (IS_ATOM_INT(_mem_3631)){
        poke_addr = (unsigned char *)_mem_3631;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_mem_3631)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_3629);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 		poke(mem+length(s), 0)  -- Thanks to Aku*/
    if (IS_SEQUENCE(_s_3629)){
            _1905 = SEQ_PTR(_s_3629)->length;
    }
    else {
        _1905 = 1;
    }
    if (IS_ATOM_INT(_mem_3631)) {
        _1906 = _mem_3631 + _1905;
        if ((long)((unsigned long)_1906 + (unsigned long)HIGH_BITS) >= 0) 
        _1906 = NewDouble((double)_1906);
    }
    else {
        _1906 = NewDouble(DBL_PTR(_mem_3631)->dbl + (double)_1905);
    }
    _1905 = NOVALUE;
    if (IS_ATOM_INT(_1906)){
        poke_addr = (unsigned char *)_1906;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_1906)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_1906);
    _1906 = NOVALUE;

    /** 		if cleanup then*/
L1: 

    /** 	return mem*/
    DeRefDS(_s_3629);
    return _mem_3631;
    ;
}


int _11allocate_protect(int _data_3642, int _wordsize_3643, int _protection_3645)
{
    int _iaddr_3646 = NOVALUE;
    int _eaddr_3648 = NOVALUE;
    int _size_3649 = NOVALUE;
    int _first_protection_3651 = NOVALUE;
    int _true_protection_3653 = NOVALUE;
    int _prepare_block_inlined_prepare_block_at_108_3668 = NOVALUE;
    int _msg_inlined_crash_at_192_3681 = NOVALUE;
    int _1927 = NOVALUE;
    int _1926 = NOVALUE;
    int _1924 = NOVALUE;
    int _1923 = NOVALUE;
    int _1922 = NOVALUE;
    int _1918 = NOVALUE;
    int _1916 = NOVALUE;
    int _1913 = NOVALUE;
    int _1912 = NOVALUE;
    int _1910 = NOVALUE;
    int _1908 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom iaddr = 0*/
    DeRef(_iaddr_3646);
    _iaddr_3646 = 0;

    /** 	integer size*/

    /** 	valid_memory_protection_constant true_protection = protection*/
    _true_protection_3653 = 16;

    /** 	ifdef SAFE then	*/

    /** 	if atom(data) then*/
    _1908 = 0;
    if (_1908 == 0)
    {
        _1908 = NOVALUE;
        goto L1; // [20] 41
    }
    else{
        _1908 = NOVALUE;
    }

    /** 		size = data * wordsize*/
    _size_3649 = binary_op(MULTIPLY, _data_3642, 1);
    if (!IS_ATOM_INT(_size_3649)) {
        _1 = (long)(DBL_PTR(_size_3649)->dbl);
        if (UNIQUE(DBL_PTR(_size_3649)) && (DBL_PTR(_size_3649)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_3649);
        _size_3649 = _1;
    }

    /** 		first_protection = true_protection*/
    _first_protection_3651 = 16;
    goto L2; // [38] 62
L1: 

    /** 		size = length(data) * wordsize*/
    if (IS_SEQUENCE(_data_3642)){
            _1910 = SEQ_PTR(_data_3642)->length;
    }
    else {
        _1910 = 1;
    }
    _size_3649 = _1910 * _wordsize_3643;
    _1910 = NOVALUE;

    /** 		first_protection = PAGE_READ_WRITE*/
    _first_protection_3651 = 4;
L2: 

    /** 	iaddr = local_allocate_protected_memory( size + memory:BORDER_SPACE * 2, first_protection )*/
    _1912 = 0;
    _1913 = _size_3649 + 0;
    _1912 = NOVALUE;
    _0 = _iaddr_3646;
    _iaddr_3646 = _11local_allocate_protected_memory(_1913, _first_protection_3651);
    DeRef(_0);
    _1913 = NOVALUE;

    /** 	if iaddr = 0 then*/
    if (binary_op_a(NOTEQ, _iaddr_3646, 0)){
        goto L3; // [83] 94
    }

    /** 		return 0*/
    DeRefi(_data_3642);
    DeRef(_iaddr_3646);
    DeRef(_eaddr_3648);
    return 0;
L3: 

    /** 	eaddr = memory:prepare_block( iaddr, size, protection )*/
    if (!IS_ATOM_INT(_protection_3645)) {
        _1 = (long)(DBL_PTR(_protection_3645)->dbl);
        if (UNIQUE(DBL_PTR(_protection_3645)) && (DBL_PTR(_protection_3645)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_3645);
        _protection_3645 = _1;
    }

    /** 	return addr*/
    Ref(_iaddr_3646);
    DeRef(_eaddr_3648);
    _eaddr_3648 = _iaddr_3646;

    /** 	if eaddr = 0 or atom( data ) then*/
    if (IS_ATOM_INT(_eaddr_3648)) {
        _1916 = (_eaddr_3648 == 0);
    }
    else {
        _1916 = (DBL_PTR(_eaddr_3648)->dbl == (double)0);
    }
    if (_1916 != 0) {
        goto L4; // [112] 124
    }
    _1918 = IS_ATOM(_data_3642);
    if (_1918 == 0)
    {
        _1918 = NOVALUE;
        goto L5; // [120] 131
    }
    else{
        _1918 = NOVALUE;
    }
L4: 

    /** 		return eaddr*/
    DeRefi(_data_3642);
    DeRef(_iaddr_3646);
    DeRef(_1916);
    _1916 = NOVALUE;
    return _eaddr_3648;
L5: 

    /** 	switch wordsize do*/
    _0 = _wordsize_3643;
    switch ( _0 ){ 

        /** 		case 1 then*/
        case 1:

        /** 			eu:poke( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_3648)){
            poke_addr = (unsigned char *)_eaddr_3648;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_eaddr_3648)->dbl);
        }
        if (IS_ATOM_INT(_data_3642)) {
            *poke_addr = (unsigned char)_data_3642;
        }
        else if (IS_ATOM(_data_3642)) {
            _1 = (signed char)DBL_PTR(_data_3642)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_3642);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        goto L6; // [147] 196

        /** 		case 2 then*/
        case 2:

        /** 			eu:poke2( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_3648)){
            poke2_addr = (unsigned short *)_eaddr_3648;
        }
        else {
            poke2_addr = (unsigned short *)(unsigned long)(DBL_PTR(_eaddr_3648)->dbl);
        }
        if (IS_ATOM_INT(_data_3642)) {
            *poke2_addr = (unsigned short)_data_3642;
        }
        else if (IS_ATOM(_data_3642)) {
            _1 = (signed char)DBL_PTR(_data_3642)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_3642);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke2_addr++ = (unsigned short)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned short)DBL_PTR(_2)->dbl;
                    *poke2_addr++ = (unsigned short)_0;
                }
            }
        }
        goto L6; // [158] 196

        /** 		case 4 then*/
        case 4:

        /** 			eu:poke4( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_3648)){
            poke4_addr = (unsigned long *)_eaddr_3648;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_eaddr_3648)->dbl);
        }
        if (IS_ATOM_INT(_data_3642)) {
            *poke4_addr = (unsigned long)_data_3642;
        }
        else if (IS_ATOM(_data_3642)) {
            _1 = (unsigned long)DBL_PTR(_data_3642)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_3642);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        goto L6; // [169] 196

        /** 		case else*/
        default:

        /** 			error:crash("Parameter error: Wrong word size %d in allocate_protect().", wordsize)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_192_3681);
        _msg_inlined_crash_at_192_3681 = EPrintf(-9999999, _1921, _wordsize_3643);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_192_3681);

        /** end procedure*/
        goto L7; // [190] 193
L7: 
        DeRefi(_msg_inlined_crash_at_192_3681);
        _msg_inlined_crash_at_192_3681 = NOVALUE;
    ;}L6: 

    /** 	ifdef SAFE then*/

    /** 	if local_change_protection_on_protected_memory( iaddr, size + memory:BORDER_SPACE * 2, true_protection ) = -1 then*/
    _1922 = 0;
    _1923 = _size_3649 + 0;
    _1922 = NOVALUE;
    Ref(_iaddr_3646);
    _1924 = _11local_change_protection_on_protected_memory(_iaddr_3646, _1923, _true_protection_3653);
    _1923 = NOVALUE;
    if (binary_op_a(NOTEQ, _1924, -1)){
        DeRef(_1924);
        _1924 = NOVALUE;
        goto L8; // [214] 238
    }
    DeRef(_1924);
    _1924 = NOVALUE;

    /** 		local_free_protected_memory( iaddr, size + memory:BORDER_SPACE * 2 )*/
    _1926 = 0;
    _1927 = _size_3649 + 0;
    _1926 = NOVALUE;
    Ref(_iaddr_3646);
    _11local_free_protected_memory(_iaddr_3646, _1927);
    _1927 = NOVALUE;

    /** 		eaddr = 0*/
    DeRef(_eaddr_3648);
    _eaddr_3648 = 0;
L8: 

    /** 	return eaddr*/
    DeRefi(_data_3642);
    DeRef(_iaddr_3646);
    DeRef(_1916);
    _1916 = NOVALUE;
    return _eaddr_3648;
    ;
}


int _11local_allocate_protected_memory(int _s_3696, int _first_protection_3697)
{
    int _1933 = NOVALUE;
    int _1932 = NOVALUE;
    int _1931 = NOVALUE;
    int _1930 = NOVALUE;
    int _1929 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _1929 = _13dep_works();
    if (_1929 == 0) {
        DeRef(_1929);
        _1929 = NOVALUE;
        goto L1; // [16] 50
    }
    else {
        if (!IS_ATOM_INT(_1929) && DBL_PTR(_1929)->dbl == 0.0){
            DeRef(_1929);
            _1929 = NOVALUE;
            goto L1; // [16] 50
        }
        DeRef(_1929);
        _1929 = NOVALUE;
    }
    DeRef(_1929);
    _1929 = NOVALUE;

    /** 			return eu:c_func(VirtualAlloc_rid, */
    {unsigned long tu;
         tu = (unsigned long)8192 | (unsigned long)4096;
         _1930 = MAKE_UINT(tu);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = _s_3696;
    *((int *)(_2+12)) = _1930;
    *((int *)(_2+16)) = _first_protection_3697;
    _1931 = MAKE_SEQ(_1);
    _1930 = NOVALUE;
    _1932 = call_c(1, _11VirtualAlloc_rid_3523, _1931);
    DeRefDS(_1931);
    _1931 = NOVALUE;
    return _1932;
    goto L2; // [47] 65
L1: 

    /** 			return machine_func(M_ALLOC, PAGE_SIZE)*/
    _1933 = machine(16, _11PAGE_SIZE_3603);
    DeRef(_1932);
    _1932 = NOVALUE;
    return _1933;
L2: 
    ;
}


int _11local_change_protection_on_protected_memory(int _p_3711, int _s_3712, int _new_protection_3713)
{
    int _1936 = NOVALUE;
    int _1935 = NOVALUE;
    int _1934 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _1934 = _13dep_works();
    if (_1934 == 0) {
        DeRef(_1934);
        _1934 = NOVALUE;
        goto L1; // [16] 49
    }
    else {
        if (!IS_ATOM_INT(_1934) && DBL_PTR(_1934)->dbl == 0.0){
            DeRef(_1934);
            _1934 = NOVALUE;
            goto L1; // [16] 49
        }
        DeRef(_1934);
        _1934 = NOVALUE;
    }
    DeRef(_1934);
    _1934 = NOVALUE;

    /** 			if eu:c_func( VirtualProtect_rid, { p, s, new_protection , oldprotptr } ) = 0 then*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_3711);
    *((int *)(_2+4)) = _p_3711;
    *((int *)(_2+8)) = _s_3712;
    *((int *)(_2+12)) = _new_protection_3713;
    Ref(_11oldprotptr_3692);
    *((int *)(_2+16)) = _11oldprotptr_3692;
    _1935 = MAKE_SEQ(_1);
    _1936 = call_c(1, _11VirtualProtect_rid_3524, _1935);
    DeRefDS(_1935);
    _1935 = NOVALUE;
    if (binary_op_a(NOTEQ, _1936, 0)){
        DeRef(_1936);
        _1936 = NOVALUE;
        goto L2; // [37] 48
    }
    DeRef(_1936);
    _1936 = NOVALUE;

    /** 				return -1*/
    DeRef(_p_3711);
    return -1;
L2: 
L1: 

    /** 		return 0*/
    DeRef(_p_3711);
    return 0;
    ;
}


void _11local_free_protected_memory(int _p_3723, int _s_3724)
{
    int _1942 = NOVALUE;
    int _1941 = NOVALUE;
    int _1940 = NOVALUE;
    int _1939 = NOVALUE;
    int _1938 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _1938 = _13dep_works();
    if (_1938 == 0) {
        DeRef(_1938);
        _1938 = NOVALUE;
        goto L1; // [12] 35
    }
    else {
        if (!IS_ATOM_INT(_1938) && DBL_PTR(_1938)->dbl == 0.0){
            DeRef(_1938);
            _1938 = NOVALUE;
            goto L1; // [12] 35
        }
        DeRef(_1938);
        _1938 = NOVALUE;
    }
    DeRef(_1938);
    _1938 = NOVALUE;

    /** 			c_func(VirtualFree_rid, { p, s, MEM_RELEASE })*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_3723);
    *((int *)(_2+4)) = _p_3723;
    *((int *)(_2+8)) = _s_3724;
    *((int *)(_2+12)) = 32768;
    _1939 = MAKE_SEQ(_1);
    _1940 = call_c(1, _13VirtualFree_rid_3392, _1939);
    DeRefDS(_1939);
    _1939 = NOVALUE;
    goto L2; // [32] 48
L1: 

    /** 			machine_func(M_FREE, {p})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_3723);
    *((int *)(_2+4)) = _p_3723;
    _1941 = MAKE_SEQ(_1);
    _1942 = machine(17, _1941);
    DeRefDS(_1941);
    _1941 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_p_3723);
    DeRef(_1940);
    _1940 = NOVALUE;
    DeRef(_1942);
    _1942 = NOVALUE;
    return;
    ;
}


void _11free(int _addr_3738)
{
    int _msg_inlined_crash_at_27_3747 = NOVALUE;
    int _data_inlined_crash_at_24_3746 = NOVALUE;
    int _addr_inlined_deallocate_at_64_3753 = NOVALUE;
    int _msg_inlined_crash_at_106_3758 = NOVALUE;
    int _1949 = NOVALUE;
    int _1948 = NOVALUE;
    int _1947 = NOVALUE;
    int _1946 = NOVALUE;
    int _1944 = NOVALUE;
    int _1943 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if types:number_array (addr) then*/
    Ref(_addr_3738);
    _1943 = _5number_array(_addr_3738);
    if (_1943 == 0) {
        DeRef(_1943);
        _1943 = NOVALUE;
        goto L1; // [7] 97
    }
    else {
        if (!IS_ATOM_INT(_1943) && DBL_PTR(_1943)->dbl == 0.0){
            DeRef(_1943);
            _1943 = NOVALUE;
            goto L1; // [7] 97
        }
        DeRef(_1943);
        _1943 = NOVALUE;
    }
    DeRef(_1943);
    _1943 = NOVALUE;

    /** 		if types:ascii_string(addr) then*/
    Ref(_addr_3738);
    _1944 = _5ascii_string(_addr_3738);
    if (_1944 == 0) {
        DeRef(_1944);
        _1944 = NOVALUE;
        goto L2; // [16] 47
    }
    else {
        if (!IS_ATOM_INT(_1944) && DBL_PTR(_1944)->dbl == 0.0){
            DeRef(_1944);
            _1944 = NOVALUE;
            goto L2; // [16] 47
        }
        DeRef(_1944);
        _1944 = NOVALUE;
    }
    DeRef(_1944);
    _1944 = NOVALUE;

    /** 			error:crash("free(\"%s\") is not a valid address", {addr})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_3738);
    *((int *)(_2+4)) = _addr_3738;
    _1946 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_24_3746);
    _data_inlined_crash_at_24_3746 = _1946;
    _1946 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_27_3747);
    _msg_inlined_crash_at_27_3747 = EPrintf(-9999999, _1945, _data_inlined_crash_at_24_3746);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_27_3747);

    /** end procedure*/
    goto L3; // [41] 44
L3: 
    DeRef(_data_inlined_crash_at_24_3746);
    _data_inlined_crash_at_24_3746 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_27_3747);
    _msg_inlined_crash_at_27_3747 = NOVALUE;
L2: 

    /** 		for i = 1 to length(addr) do*/
    if (IS_SEQUENCE(_addr_3738)){
            _1947 = SEQ_PTR(_addr_3738)->length;
    }
    else {
        _1947 = 1;
    }
    {
        int _i_3749;
        _i_3749 = 1;
L4: 
        if (_i_3749 > _1947){
            goto L5; // [52] 89
        }

        /** 			memory:deallocate( addr[i] )*/
        _2 = (int)SEQ_PTR(_addr_3738);
        _1948 = (int)*(((s1_ptr)_2)->base + _i_3749);
        Ref(_1948);
        DeRef(_addr_inlined_deallocate_at_64_3753);
        _addr_inlined_deallocate_at_64_3753 = _1948;
        _1948 = NOVALUE;

        /** 	ifdef DATA_EXECUTE and WINDOWS then*/

        /**    	machine_proc( memconst:M_FREE, addr)*/
        machine(17, _addr_inlined_deallocate_at_64_3753);

        /** end procedure*/
        goto L6; // [77] 80
L6: 
        DeRef(_addr_inlined_deallocate_at_64_3753);
        _addr_inlined_deallocate_at_64_3753 = NOVALUE;

        /** 		end for*/
        _i_3749 = _i_3749 + 1;
        goto L4; // [84] 59
L5: 
        ;
    }

    /** 		return*/
    DeRef(_addr_3738);
    return;
    goto L7; // [94] 127
L1: 

    /** 	elsif sequence(addr) then*/
    _1949 = IS_SEQUENCE(_addr_3738);
    if (_1949 == 0)
    {
        _1949 = NOVALUE;
        goto L8; // [102] 126
    }
    else{
        _1949 = NOVALUE;
    }

    /** 		error:crash("free() called with nested sequence")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_106_3758);
    _msg_inlined_crash_at_106_3758 = EPrintf(-9999999, _1950, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_106_3758);

    /** end procedure*/
    goto L9; // [120] 123
L9: 
    DeRefi(_msg_inlined_crash_at_106_3758);
    _msg_inlined_crash_at_106_3758 = NOVALUE;
L8: 
L7: 

    /** 	if addr = 0 then*/
    if (binary_op_a(NOTEQ, _addr_3738, 0)){
        goto LA; // [129] 139
    }

    /** 		return*/
    DeRef(_addr_3738);
    return;
LA: 

    /** 	memory:deallocate( addr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_3738);

    /** end procedure*/
    goto LB; // [150] 153
LB: 

    /** end procedure*/
    DeRef(_addr_3738);
    return;
    ;
}


void _11free_pointer_array(int _pointers_array_3766)
{
    int _saved_3767 = NOVALUE;
    int _ptr_3768 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom saved = pointers_array*/
    Ref(_pointers_array_3766);
    DeRef(_saved_3767);
    _saved_3767 = _pointers_array_3766;

    /** 	while ptr with entry do*/
    goto L1; // [8] 39
L2: 
    if (_ptr_3768 <= 0) {
        if (_ptr_3768 == 0) {
            goto L3; // [13] 49
        }
        else {
            if (!IS_ATOM_INT(_ptr_3768) && DBL_PTR(_ptr_3768)->dbl == 0.0){
                goto L3; // [13] 49
            }
        }
    }

    /** 		memory:deallocate( ptr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _ptr_3768);

    /** end procedure*/
    goto L4; // [27] 30
L4: 

    /** 		pointers_array += ADDRESS_LENGTH*/
    _0 = _pointers_array_3766;
    if (IS_ATOM_INT(_pointers_array_3766)) {
        _pointers_array_3766 = _pointers_array_3766 + 4;
        if ((long)((unsigned long)_pointers_array_3766 + (unsigned long)HIGH_BITS) >= 0) 
        _pointers_array_3766 = NewDouble((double)_pointers_array_3766);
    }
    else {
        _pointers_array_3766 = NewDouble(DBL_PTR(_pointers_array_3766)->dbl + (double)4);
    }
    DeRef(_0);

    /** 	entry*/
L1: 

    /** 		ptr = peek4u(pointers_array)*/
    DeRef(_ptr_3768);
    if (IS_ATOM_INT(_pointers_array_3766)) {
        _ptr_3768 = *(unsigned long *)_pointers_array_3766;
        if ((unsigned)_ptr_3768 > (unsigned)MAXINT)
        _ptr_3768 = NewDouble((double)(unsigned long)_ptr_3768);
    }
    else {
        _ptr_3768 = *(unsigned long *)(unsigned long)(DBL_PTR(_pointers_array_3766)->dbl);
        if ((unsigned)_ptr_3768 > (unsigned)MAXINT)
        _ptr_3768 = NewDouble((double)(unsigned long)_ptr_3768);
    }

    /** 	end while*/
    goto L2; // [46] 11
L3: 

    /** 	free(saved)*/
    Ref(_saved_3767);
    _11free(_saved_3767);

    /** end procedure*/
    DeRef(_pointers_array_3766);
    DeRef(_saved_3767);
    DeRef(_ptr_3768);
    return;
    ;
}



// 0x27848892
