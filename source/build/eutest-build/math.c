// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _18abs(int _a_4492)
{
    int _t_4493 = NOVALUE;
    int _2305 = NOVALUE;
    int _2304 = NOVALUE;
    int _2303 = NOVALUE;
    int _2301 = NOVALUE;
    int _2299 = NOVALUE;
    int _2298 = NOVALUE;
    int _2296 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2296 = IS_ATOM(_a_4492);
    if (_2296 == 0)
    {
        _2296 = NOVALUE;
        goto L1; // [6] 35
    }
    else{
        _2296 = NOVALUE;
    }

    /** 		if a >= 0 then*/
    if (binary_op_a(LESS, _a_4492, 0)){
        goto L2; // [11] 24
    }

    /** 			return a*/
    DeRef(_t_4493);
    return _a_4492;
    goto L3; // [21] 34
L2: 

    /** 			return - a*/
    if (IS_ATOM_INT(_a_4492)) {
        if ((unsigned long)_a_4492 == 0xC0000000)
        _2298 = (int)NewDouble((double)-0xC0000000);
        else
        _2298 = - _a_4492;
    }
    else {
        _2298 = unary_op(UMINUS, _a_4492);
    }
    DeRef(_a_4492);
    DeRef(_t_4493);
    return _2298;
L3: 
L1: 

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4492)){
            _2299 = SEQ_PTR(_a_4492)->length;
    }
    else {
        _2299 = 1;
    }
    {
        int _i_4501;
        _i_4501 = 1;
L4: 
        if (_i_4501 > _2299){
            goto L5; // [40] 101
        }

        /** 		t = a[i]*/
        DeRef(_t_4493);
        _2 = (int)SEQ_PTR(_a_4492);
        _t_4493 = (int)*(((s1_ptr)_2)->base + _i_4501);
        Ref(_t_4493);

        /** 		if atom(t) then*/
        _2301 = IS_ATOM(_t_4493);
        if (_2301 == 0)
        {
            _2301 = NOVALUE;
            goto L6; // [58] 80
        }
        else{
            _2301 = NOVALUE;
        }

        /** 			if t < 0 then*/
        if (binary_op_a(GREATEREQ, _t_4493, 0)){
            goto L7; // [63] 94
        }

        /** 				a[i] = - t*/
        if (IS_ATOM_INT(_t_4493)) {
            if ((unsigned long)_t_4493 == 0xC0000000)
            _2303 = (int)NewDouble((double)-0xC0000000);
            else
            _2303 = - _t_4493;
        }
        else {
            _2303 = unary_op(UMINUS, _t_4493);
        }
        _2 = (int)SEQ_PTR(_a_4492);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_4492 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4501);
        _1 = *(int *)_2;
        *(int *)_2 = _2303;
        if( _1 != _2303 ){
            DeRef(_1);
        }
        _2303 = NOVALUE;
        goto L7; // [77] 94
L6: 

        /** 			a[i] = abs(t)*/
        Ref(_t_4493);
        DeRef(_2304);
        _2304 = _t_4493;
        _2305 = _18abs(_2304);
        _2304 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_4492);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_4492 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4501);
        _1 = *(int *)_2;
        *(int *)_2 = _2305;
        if( _1 != _2305 ){
            DeRef(_1);
        }
        _2305 = NOVALUE;
L7: 

        /** 	end for*/
        _i_4501 = _i_4501 + 1;
        goto L4; // [96] 47
L5: 
        ;
    }

    /** 	return a*/
    DeRef(_t_4493);
    DeRef(_2298);
    _2298 = NOVALUE;
    return _a_4492;
    ;
}


int _18sign(int _a_4514)
{
    int _2308 = NOVALUE;
    int _2307 = NOVALUE;
    int _2306 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return (a > 0) - (a < 0)*/
    if (IS_ATOM_INT(_a_4514)) {
        _2306 = (_a_4514 > 0);
    }
    else {
        _2306 = binary_op(GREATER, _a_4514, 0);
    }
    if (IS_ATOM_INT(_a_4514)) {
        _2307 = (_a_4514 < 0);
    }
    else {
        _2307 = binary_op(LESS, _a_4514, 0);
    }
    if (IS_ATOM_INT(_2306) && IS_ATOM_INT(_2307)) {
        _2308 = _2306 - _2307;
        if ((long)((unsigned long)_2308 +(unsigned long) HIGH_BITS) >= 0){
            _2308 = NewDouble((double)_2308);
        }
    }
    else {
        _2308 = binary_op(MINUS, _2306, _2307);
    }
    DeRef(_2306);
    _2306 = NOVALUE;
    DeRef(_2307);
    _2307 = NOVALUE;
    DeRef(_a_4514);
    return _2308;
    ;
}


int _18larger_of(int _objA_4520, int _objB_4521)
{
    int _2309 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if compare(objA, objB) > 0 then*/
    if (IS_ATOM_INT(_objA_4520) && IS_ATOM_INT(_objB_4521)){
        _2309 = (_objA_4520 < _objB_4521) ? -1 : (_objA_4520 > _objB_4521);
    }
    else{
        _2309 = compare(_objA_4520, _objB_4521);
    }
    if (_2309 <= 0)
    goto L1; // [7] 20

    /** 		return objA*/
    DeRef(_objB_4521);
    return _objA_4520;
    goto L2; // [17] 27
L1: 

    /** 		return objB*/
    DeRef(_objA_4520);
    return _objB_4521;
L2: 
    ;
}


int _18smaller_of(int _objA_4528, int _objB_4529)
{
    int _2311 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if compare(objA, objB) < 0 then*/
    if (IS_ATOM_INT(_objA_4528) && IS_ATOM_INT(_objB_4529)){
        _2311 = (_objA_4528 < _objB_4529) ? -1 : (_objA_4528 > _objB_4529);
    }
    else{
        _2311 = compare(_objA_4528, _objB_4529);
    }
    if (_2311 >= 0)
    goto L1; // [7] 20

    /** 		return objA*/
    DeRef(_objB_4529);
    return _objA_4528;
    goto L2; // [17] 27
L1: 

    /** 		return objB*/
    DeRef(_objA_4528);
    return _objB_4529;
L2: 
    ;
}


int _18max(int _a_4536)
{
    int _b_4537 = NOVALUE;
    int _c_4538 = NOVALUE;
    int _2315 = NOVALUE;
    int _2314 = NOVALUE;
    int _2313 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2313 = IS_ATOM(_a_4536);
    if (_2313 == 0)
    {
        _2313 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2313 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4537);
    DeRef(_c_4538);
    return _a_4536;
L1: 

    /** 	b = mathcons:MINF*/
    RefDS(_20MINF_4470);
    DeRef(_b_4537);
    _b_4537 = _20MINF_4470;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4536)){
            _2314 = SEQ_PTR(_a_4536)->length;
    }
    else {
        _2314 = 1;
    }
    {
        int _i_4542;
        _i_4542 = 1;
L2: 
        if (_i_4542 > _2314){
            goto L3; // [28] 64
        }

        /** 		c = max(a[i])*/
        _2 = (int)SEQ_PTR(_a_4536);
        _2315 = (int)*(((s1_ptr)_2)->base + _i_4542);
        Ref(_2315);
        _0 = _c_4538;
        _c_4538 = _18max(_2315);
        DeRef(_0);
        _2315 = NOVALUE;

        /** 		if c > b then*/
        if (binary_op_a(LESSEQ, _c_4538, _b_4537)){
            goto L4; // [47] 57
        }

        /** 			b = c*/
        Ref(_c_4538);
        DeRef(_b_4537);
        _b_4537 = _c_4538;
L4: 

        /** 	end for*/
        _i_4542 = _i_4542 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4536);
    DeRef(_c_4538);
    return _b_4537;
    ;
}


int _18min(int _a_4550)
{
    int _b_4551 = NOVALUE;
    int _c_4552 = NOVALUE;
    int _2320 = NOVALUE;
    int _2319 = NOVALUE;
    int _2318 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2318 = IS_ATOM(_a_4550);
    if (_2318 == 0)
    {
        _2318 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2318 = NOVALUE;
    }

    /** 			return a*/
    DeRef(_b_4551);
    DeRef(_c_4552);
    return _a_4550;
L1: 

    /** 	b = mathcons:PINF*/
    RefDS(_20PINF_4466);
    DeRef(_b_4551);
    _b_4551 = _20PINF_4466;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4550)){
            _2319 = SEQ_PTR(_a_4550)->length;
    }
    else {
        _2319 = 1;
    }
    {
        int _i_4556;
        _i_4556 = 1;
L2: 
        if (_i_4556 > _2319){
            goto L3; // [28] 64
        }

        /** 		c = min(a[i])*/
        _2 = (int)SEQ_PTR(_a_4550);
        _2320 = (int)*(((s1_ptr)_2)->base + _i_4556);
        Ref(_2320);
        _0 = _c_4552;
        _c_4552 = _18min(_2320);
        DeRef(_0);
        _2320 = NOVALUE;

        /** 			if c < b then*/
        if (binary_op_a(GREATEREQ, _c_4552, _b_4551)){
            goto L4; // [47] 57
        }

        /** 				b = c*/
        Ref(_c_4552);
        DeRef(_b_4551);
        _b_4551 = _c_4552;
L4: 

        /** 	end for*/
        _i_4556 = _i_4556 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4550);
    DeRef(_c_4552);
    return _b_4551;
    ;
}


int _18ensure_in_range(int _item_4564, int _range_limits_4565)
{
    int _2334 = NOVALUE;
    int _2333 = NOVALUE;
    int _2331 = NOVALUE;
    int _2330 = NOVALUE;
    int _2329 = NOVALUE;
    int _2328 = NOVALUE;
    int _2326 = NOVALUE;
    int _2325 = NOVALUE;
    int _2323 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(range_limits) < 2 then*/
    if (IS_SEQUENCE(_range_limits_4565)){
            _2323 = SEQ_PTR(_range_limits_4565)->length;
    }
    else {
        _2323 = 1;
    }
    if (_2323 >= 2)
    goto L1; // [8] 19

    /** 		return item*/
    DeRefDS(_range_limits_4565);
    return _item_4564;
L1: 

    /** 	if eu:compare(item, range_limits[1]) < 0 then*/
    _2 = (int)SEQ_PTR(_range_limits_4565);
    _2325 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_item_4564) && IS_ATOM_INT(_2325)){
        _2326 = (_item_4564 < _2325) ? -1 : (_item_4564 > _2325);
    }
    else{
        _2326 = compare(_item_4564, _2325);
    }
    _2325 = NOVALUE;
    if (_2326 >= 0)
    goto L2; // [29] 44

    /** 		return range_limits[1]*/
    _2 = (int)SEQ_PTR(_range_limits_4565);
    _2328 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_2328);
    DeRef(_item_4564);
    DeRefDS(_range_limits_4565);
    return _2328;
L2: 

    /** 	if eu:compare(item, range_limits[$]) > 0 then*/
    if (IS_SEQUENCE(_range_limits_4565)){
            _2329 = SEQ_PTR(_range_limits_4565)->length;
    }
    else {
        _2329 = 1;
    }
    _2 = (int)SEQ_PTR(_range_limits_4565);
    _2330 = (int)*(((s1_ptr)_2)->base + _2329);
    if (IS_ATOM_INT(_item_4564) && IS_ATOM_INT(_2330)){
        _2331 = (_item_4564 < _2330) ? -1 : (_item_4564 > _2330);
    }
    else{
        _2331 = compare(_item_4564, _2330);
    }
    _2330 = NOVALUE;
    if (_2331 <= 0)
    goto L3; // [57] 75

    /** 		return range_limits[$]*/
    if (IS_SEQUENCE(_range_limits_4565)){
            _2333 = SEQ_PTR(_range_limits_4565)->length;
    }
    else {
        _2333 = 1;
    }
    _2 = (int)SEQ_PTR(_range_limits_4565);
    _2334 = (int)*(((s1_ptr)_2)->base + _2333);
    Ref(_2334);
    DeRef(_item_4564);
    DeRefDS(_range_limits_4565);
    _2328 = NOVALUE;
    return _2334;
L3: 

    /** 	return item*/
    DeRefDS(_range_limits_4565);
    _2328 = NOVALUE;
    _2334 = NOVALUE;
    return _item_4564;
    ;
}


int _18ensure_in_list(int _item_4583, int _list_4584, int _default_4585)
{
    int _2344 = NOVALUE;
    int _2343 = NOVALUE;
    int _2342 = NOVALUE;
    int _2341 = NOVALUE;
    int _2340 = NOVALUE;
    int _2339 = NOVALUE;
    int _2337 = NOVALUE;
    int _2335 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_default_4585)) {
        _1 = (long)(DBL_PTR(_default_4585)->dbl);
        if (UNIQUE(DBL_PTR(_default_4585)) && (DBL_PTR(_default_4585)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_default_4585);
        _default_4585 = _1;
    }

    /** 	if length(list) = 0 then*/
    if (IS_SEQUENCE(_list_4584)){
            _2335 = SEQ_PTR(_list_4584)->length;
    }
    else {
        _2335 = 1;
    }
    if (_2335 != 0)
    goto L1; // [12] 23

    /** 		return item*/
    DeRefDS(_list_4584);
    return _item_4583;
L1: 

    /** 	if find(item, list) = 0 then*/
    _2337 = find_from(_item_4583, _list_4584, 1);
    if (_2337 != 0)
    goto L2; // [30] 80

    /** 		if default>=1 and default<=length(list) then*/
    _2339 = (_default_4585 >= 1);
    if (_2339 == 0) {
        goto L3; // [40] 68
    }
    if (IS_SEQUENCE(_list_4584)){
            _2341 = SEQ_PTR(_list_4584)->length;
    }
    else {
        _2341 = 1;
    }
    _2342 = (_default_4585 <= _2341);
    _2341 = NOVALUE;
    if (_2342 == 0)
    {
        DeRef(_2342);
        _2342 = NOVALUE;
        goto L3; // [52] 68
    }
    else{
        DeRef(_2342);
        _2342 = NOVALUE;
    }

    /** 		    return list[default]*/
    _2 = (int)SEQ_PTR(_list_4584);
    _2343 = (int)*(((s1_ptr)_2)->base + _default_4585);
    Ref(_2343);
    DeRef(_item_4583);
    DeRefDS(_list_4584);
    DeRef(_2339);
    _2339 = NOVALUE;
    return _2343;
    goto L4; // [65] 79
L3: 

    /** 			return list[1]*/
    _2 = (int)SEQ_PTR(_list_4584);
    _2344 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_2344);
    DeRef(_item_4583);
    DeRefDS(_list_4584);
    DeRef(_2339);
    _2339 = NOVALUE;
    _2343 = NOVALUE;
    return _2344;
L4: 
L2: 

    /** 	return item*/
    DeRefDS(_list_4584);
    DeRef(_2339);
    _2339 = NOVALUE;
    _2343 = NOVALUE;
    _2344 = NOVALUE;
    return _item_4583;
    ;
}


int _18mod(int _x_4602, int _y_4603)
{
    int _sign_2__tmp_at2_4608 = NOVALUE;
    int _sign_1__tmp_at2_4607 = NOVALUE;
    int _sign_inlined_sign_at_2_4606 = NOVALUE;
    int _sign_2__tmp_at19_4612 = NOVALUE;
    int _sign_1__tmp_at19_4611 = NOVALUE;
    int _sign_inlined_sign_at_19_4610 = NOVALUE;
    int _2349 = NOVALUE;
    int _2348 = NOVALUE;
    int _2347 = NOVALUE;
    int _2346 = NOVALUE;
    int _2345 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(sign(x), sign(y)) then*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at2_4607);
    if (IS_ATOM_INT(_x_4602)) {
        _sign_1__tmp_at2_4607 = (_x_4602 > 0);
    }
    else {
        _sign_1__tmp_at2_4607 = binary_op(GREATER, _x_4602, 0);
    }
    DeRef(_sign_2__tmp_at2_4608);
    if (IS_ATOM_INT(_x_4602)) {
        _sign_2__tmp_at2_4608 = (_x_4602 < 0);
    }
    else {
        _sign_2__tmp_at2_4608 = binary_op(LESS, _x_4602, 0);
    }
    DeRef(_sign_inlined_sign_at_2_4606);
    if (IS_ATOM_INT(_sign_1__tmp_at2_4607) && IS_ATOM_INT(_sign_2__tmp_at2_4608)) {
        _sign_inlined_sign_at_2_4606 = _sign_1__tmp_at2_4607 - _sign_2__tmp_at2_4608;
        if ((long)((unsigned long)_sign_inlined_sign_at_2_4606 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_2_4606 = NewDouble((double)_sign_inlined_sign_at_2_4606);
        }
    }
    else {
        _sign_inlined_sign_at_2_4606 = binary_op(MINUS, _sign_1__tmp_at2_4607, _sign_2__tmp_at2_4608);
    }
    DeRef(_sign_1__tmp_at2_4607);
    _sign_1__tmp_at2_4607 = NOVALUE;
    DeRef(_sign_2__tmp_at2_4608);
    _sign_2__tmp_at2_4608 = NOVALUE;

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at19_4611);
    if (IS_ATOM_INT(_y_4603)) {
        _sign_1__tmp_at19_4611 = (_y_4603 > 0);
    }
    else {
        _sign_1__tmp_at19_4611 = binary_op(GREATER, _y_4603, 0);
    }
    DeRef(_sign_2__tmp_at19_4612);
    if (IS_ATOM_INT(_y_4603)) {
        _sign_2__tmp_at19_4612 = (_y_4603 < 0);
    }
    else {
        _sign_2__tmp_at19_4612 = binary_op(LESS, _y_4603, 0);
    }
    DeRef(_sign_inlined_sign_at_19_4610);
    if (IS_ATOM_INT(_sign_1__tmp_at19_4611) && IS_ATOM_INT(_sign_2__tmp_at19_4612)) {
        _sign_inlined_sign_at_19_4610 = _sign_1__tmp_at19_4611 - _sign_2__tmp_at19_4612;
        if ((long)((unsigned long)_sign_inlined_sign_at_19_4610 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_19_4610 = NewDouble((double)_sign_inlined_sign_at_19_4610);
        }
    }
    else {
        _sign_inlined_sign_at_19_4610 = binary_op(MINUS, _sign_1__tmp_at19_4611, _sign_2__tmp_at19_4612);
    }
    DeRef(_sign_1__tmp_at19_4611);
    _sign_1__tmp_at19_4611 = NOVALUE;
    DeRef(_sign_2__tmp_at19_4612);
    _sign_2__tmp_at19_4612 = NOVALUE;
    if (_sign_inlined_sign_at_2_4606 == _sign_inlined_sign_at_19_4610)
    _2345 = 1;
    else if (IS_ATOM_INT(_sign_inlined_sign_at_2_4606) && IS_ATOM_INT(_sign_inlined_sign_at_19_4610))
    _2345 = 0;
    else
    _2345 = (compare(_sign_inlined_sign_at_2_4606, _sign_inlined_sign_at_19_4610) == 0);
    if (_2345 == 0)
    {
        _2345 = NOVALUE;
        goto L1; // [41] 55
    }
    else{
        _2345 = NOVALUE;
    }

    /** 		return remainder(x,y)*/
    if (IS_ATOM_INT(_x_4602) && IS_ATOM_INT(_y_4603)) {
        _2346 = (_x_4602 % _y_4603);
    }
    else {
        _2346 = binary_op(REMAINDER, _x_4602, _y_4603);
    }
    DeRef(_x_4602);
    DeRef(_y_4603);
    return _2346;
L1: 

    /** 	return x - y * floor(x / y)*/
    if (IS_ATOM_INT(_x_4602) && IS_ATOM_INT(_y_4603)) {
        if (_y_4603 > 0 && _x_4602 >= 0) {
            _2347 = _x_4602 / _y_4603;
        }
        else {
            temp_dbl = floor((double)_x_4602 / (double)_y_4603);
            if (_x_4602 != MININT)
            _2347 = (long)temp_dbl;
            else
            _2347 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_4602, _y_4603);
        _2347 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_y_4603) && IS_ATOM_INT(_2347)) {
        if (_y_4603 == (short)_y_4603 && _2347 <= INT15 && _2347 >= -INT15)
        _2348 = _y_4603 * _2347;
        else
        _2348 = NewDouble(_y_4603 * (double)_2347);
    }
    else {
        _2348 = binary_op(MULTIPLY, _y_4603, _2347);
    }
    DeRef(_2347);
    _2347 = NOVALUE;
    if (IS_ATOM_INT(_x_4602) && IS_ATOM_INT(_2348)) {
        _2349 = _x_4602 - _2348;
        if ((long)((unsigned long)_2349 +(unsigned long) HIGH_BITS) >= 0){
            _2349 = NewDouble((double)_2349);
        }
    }
    else {
        _2349 = binary_op(MINUS, _x_4602, _2348);
    }
    DeRef(_2348);
    _2348 = NOVALUE;
    DeRef(_x_4602);
    DeRef(_y_4603);
    DeRef(_2346);
    _2346 = NOVALUE;
    return _2349;
    ;
}


int _18trunc(int _x_4620)
{
    int _sign_2__tmp_at2_4624 = NOVALUE;
    int _sign_1__tmp_at2_4623 = NOVALUE;
    int _sign_inlined_sign_at_2_4622 = NOVALUE;
    int _2352 = NOVALUE;
    int _2351 = NOVALUE;
    int _2350 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sign(x) * floor(abs(x))*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at2_4623);
    if (IS_ATOM_INT(_x_4620)) {
        _sign_1__tmp_at2_4623 = (_x_4620 > 0);
    }
    else {
        _sign_1__tmp_at2_4623 = binary_op(GREATER, _x_4620, 0);
    }
    DeRef(_sign_2__tmp_at2_4624);
    if (IS_ATOM_INT(_x_4620)) {
        _sign_2__tmp_at2_4624 = (_x_4620 < 0);
    }
    else {
        _sign_2__tmp_at2_4624 = binary_op(LESS, _x_4620, 0);
    }
    DeRef(_sign_inlined_sign_at_2_4622);
    if (IS_ATOM_INT(_sign_1__tmp_at2_4623) && IS_ATOM_INT(_sign_2__tmp_at2_4624)) {
        _sign_inlined_sign_at_2_4622 = _sign_1__tmp_at2_4623 - _sign_2__tmp_at2_4624;
        if ((long)((unsigned long)_sign_inlined_sign_at_2_4622 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_2_4622 = NewDouble((double)_sign_inlined_sign_at_2_4622);
        }
    }
    else {
        _sign_inlined_sign_at_2_4622 = binary_op(MINUS, _sign_1__tmp_at2_4623, _sign_2__tmp_at2_4624);
    }
    DeRef(_sign_1__tmp_at2_4623);
    _sign_1__tmp_at2_4623 = NOVALUE;
    DeRef(_sign_2__tmp_at2_4624);
    _sign_2__tmp_at2_4624 = NOVALUE;
    Ref(_x_4620);
    _2350 = _18abs(_x_4620);
    if (IS_ATOM_INT(_2350))
    _2351 = e_floor(_2350);
    else
    _2351 = unary_op(FLOOR, _2350);
    DeRef(_2350);
    _2350 = NOVALUE;
    if (IS_ATOM_INT(_sign_inlined_sign_at_2_4622) && IS_ATOM_INT(_2351)) {
        if (_sign_inlined_sign_at_2_4622 == (short)_sign_inlined_sign_at_2_4622 && _2351 <= INT15 && _2351 >= -INT15)
        _2352 = _sign_inlined_sign_at_2_4622 * _2351;
        else
        _2352 = NewDouble(_sign_inlined_sign_at_2_4622 * (double)_2351);
    }
    else {
        _2352 = binary_op(MULTIPLY, _sign_inlined_sign_at_2_4622, _2351);
    }
    DeRef(_2351);
    _2351 = NOVALUE;
    DeRef(_x_4620);
    return _2352;
    ;
}


int _18frac(int _x_4630)
{
    int _temp_4631 = NOVALUE;
    int _sign_2__tmp_at8_4636 = NOVALUE;
    int _sign_1__tmp_at8_4635 = NOVALUE;
    int _sign_inlined_sign_at_8_4634 = NOVALUE;
    int _2356 = NOVALUE;
    int _2355 = NOVALUE;
    int _2354 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object temp = abs(x)*/
    Ref(_x_4630);
    _0 = _temp_4631;
    _temp_4631 = _18abs(_x_4630);
    DeRef(_0);

    /** 	return sign(x) * (temp - floor(temp))*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at8_4635);
    if (IS_ATOM_INT(_x_4630)) {
        _sign_1__tmp_at8_4635 = (_x_4630 > 0);
    }
    else {
        _sign_1__tmp_at8_4635 = binary_op(GREATER, _x_4630, 0);
    }
    DeRef(_sign_2__tmp_at8_4636);
    if (IS_ATOM_INT(_x_4630)) {
        _sign_2__tmp_at8_4636 = (_x_4630 < 0);
    }
    else {
        _sign_2__tmp_at8_4636 = binary_op(LESS, _x_4630, 0);
    }
    DeRef(_sign_inlined_sign_at_8_4634);
    if (IS_ATOM_INT(_sign_1__tmp_at8_4635) && IS_ATOM_INT(_sign_2__tmp_at8_4636)) {
        _sign_inlined_sign_at_8_4634 = _sign_1__tmp_at8_4635 - _sign_2__tmp_at8_4636;
        if ((long)((unsigned long)_sign_inlined_sign_at_8_4634 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_8_4634 = NewDouble((double)_sign_inlined_sign_at_8_4634);
        }
    }
    else {
        _sign_inlined_sign_at_8_4634 = binary_op(MINUS, _sign_1__tmp_at8_4635, _sign_2__tmp_at8_4636);
    }
    DeRef(_sign_1__tmp_at8_4635);
    _sign_1__tmp_at8_4635 = NOVALUE;
    DeRef(_sign_2__tmp_at8_4636);
    _sign_2__tmp_at8_4636 = NOVALUE;
    if (IS_ATOM_INT(_temp_4631))
    _2354 = e_floor(_temp_4631);
    else
    _2354 = unary_op(FLOOR, _temp_4631);
    if (IS_ATOM_INT(_temp_4631) && IS_ATOM_INT(_2354)) {
        _2355 = _temp_4631 - _2354;
        if ((long)((unsigned long)_2355 +(unsigned long) HIGH_BITS) >= 0){
            _2355 = NewDouble((double)_2355);
        }
    }
    else {
        _2355 = binary_op(MINUS, _temp_4631, _2354);
    }
    DeRef(_2354);
    _2354 = NOVALUE;
    if (IS_ATOM_INT(_sign_inlined_sign_at_8_4634) && IS_ATOM_INT(_2355)) {
        if (_sign_inlined_sign_at_8_4634 == (short)_sign_inlined_sign_at_8_4634 && _2355 <= INT15 && _2355 >= -INT15)
        _2356 = _sign_inlined_sign_at_8_4634 * _2355;
        else
        _2356 = NewDouble(_sign_inlined_sign_at_8_4634 * (double)_2355);
    }
    else {
        _2356 = binary_op(MULTIPLY, _sign_inlined_sign_at_8_4634, _2355);
    }
    DeRef(_2355);
    _2355 = NOVALUE;
    DeRef(_x_4630);
    DeRef(_temp_4631);
    return _2356;
    ;
}


int _18intdiv(int _a_4642, int _b_4643)
{
    int _sign_2__tmp_at2_4647 = NOVALUE;
    int _sign_1__tmp_at2_4646 = NOVALUE;
    int _sign_inlined_sign_at_2_4645 = NOVALUE;
    int _2361 = NOVALUE;
    int _2360 = NOVALUE;
    int _2359 = NOVALUE;
    int _2358 = NOVALUE;
    int _2357 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sign(a)*ceil(abs(a)/abs(b))*/

    /** 	return (a > 0) - (a < 0)*/
    DeRef(_sign_1__tmp_at2_4646);
    if (IS_ATOM_INT(_a_4642)) {
        _sign_1__tmp_at2_4646 = (_a_4642 > 0);
    }
    else {
        _sign_1__tmp_at2_4646 = binary_op(GREATER, _a_4642, 0);
    }
    DeRef(_sign_2__tmp_at2_4647);
    if (IS_ATOM_INT(_a_4642)) {
        _sign_2__tmp_at2_4647 = (_a_4642 < 0);
    }
    else {
        _sign_2__tmp_at2_4647 = binary_op(LESS, _a_4642, 0);
    }
    DeRef(_sign_inlined_sign_at_2_4645);
    if (IS_ATOM_INT(_sign_1__tmp_at2_4646) && IS_ATOM_INT(_sign_2__tmp_at2_4647)) {
        _sign_inlined_sign_at_2_4645 = _sign_1__tmp_at2_4646 - _sign_2__tmp_at2_4647;
        if ((long)((unsigned long)_sign_inlined_sign_at_2_4645 +(unsigned long) HIGH_BITS) >= 0){
            _sign_inlined_sign_at_2_4645 = NewDouble((double)_sign_inlined_sign_at_2_4645);
        }
    }
    else {
        _sign_inlined_sign_at_2_4645 = binary_op(MINUS, _sign_1__tmp_at2_4646, _sign_2__tmp_at2_4647);
    }
    DeRef(_sign_1__tmp_at2_4646);
    _sign_1__tmp_at2_4646 = NOVALUE;
    DeRef(_sign_2__tmp_at2_4647);
    _sign_2__tmp_at2_4647 = NOVALUE;
    Ref(_a_4642);
    _2357 = _18abs(_a_4642);
    Ref(_b_4643);
    _2358 = _18abs(_b_4643);
    if (IS_ATOM_INT(_2357) && IS_ATOM_INT(_2358)) {
        _2359 = (_2357 % _2358) ? NewDouble((double)_2357 / _2358) : (_2357 / _2358);
    }
    else {
        _2359 = binary_op(DIVIDE, _2357, _2358);
    }
    DeRef(_2357);
    _2357 = NOVALUE;
    DeRef(_2358);
    _2358 = NOVALUE;
    _2360 = _18ceil(_2359);
    _2359 = NOVALUE;
    if (IS_ATOM_INT(_sign_inlined_sign_at_2_4645) && IS_ATOM_INT(_2360)) {
        if (_sign_inlined_sign_at_2_4645 == (short)_sign_inlined_sign_at_2_4645 && _2360 <= INT15 && _2360 >= -INT15)
        _2361 = _sign_inlined_sign_at_2_4645 * _2360;
        else
        _2361 = NewDouble(_sign_inlined_sign_at_2_4645 * (double)_2360);
    }
    else {
        _2361 = binary_op(MULTIPLY, _sign_inlined_sign_at_2_4645, _2360);
    }
    DeRef(_2360);
    _2360 = NOVALUE;
    DeRef(_a_4642);
    DeRef(_b_4643);
    return _2361;
    ;
}


int _18ceil(int _a_4656)
{
    int _2364 = NOVALUE;
    int _2363 = NOVALUE;
    int _2362 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return -floor(-a)*/
    if (IS_ATOM_INT(_a_4656)) {
        if ((unsigned long)_a_4656 == 0xC0000000)
        _2362 = (int)NewDouble((double)-0xC0000000);
        else
        _2362 = - _a_4656;
    }
    else {
        _2362 = unary_op(UMINUS, _a_4656);
    }
    if (IS_ATOM_INT(_2362))
    _2363 = e_floor(_2362);
    else
    _2363 = unary_op(FLOOR, _2362);
    DeRef(_2362);
    _2362 = NOVALUE;
    if (IS_ATOM_INT(_2363)) {
        if ((unsigned long)_2363 == 0xC0000000)
        _2364 = (int)NewDouble((double)-0xC0000000);
        else
        _2364 = - _2363;
    }
    else {
        _2364 = unary_op(UMINUS, _2363);
    }
    DeRef(_2363);
    _2363 = NOVALUE;
    DeRef(_a_4656);
    return _2364;
    ;
}


int _18round(int _a_4662, int _precision_4663)
{
    int _len_4664 = NOVALUE;
    int _s_4665 = NOVALUE;
    int _t_4666 = NOVALUE;
    int _u_4667 = NOVALUE;
    int _msg_inlined_crash_at_263_4717 = NOVALUE;
    int _2416 = NOVALUE;
    int _2415 = NOVALUE;
    int _2414 = NOVALUE;
    int _2413 = NOVALUE;
    int _2412 = NOVALUE;
    int _2411 = NOVALUE;
    int _2410 = NOVALUE;
    int _2409 = NOVALUE;
    int _2408 = NOVALUE;
    int _2407 = NOVALUE;
    int _2406 = NOVALUE;
    int _2404 = NOVALUE;
    int _2402 = NOVALUE;
    int _2398 = NOVALUE;
    int _2396 = NOVALUE;
    int _2395 = NOVALUE;
    int _2394 = NOVALUE;
    int _2393 = NOVALUE;
    int _2392 = NOVALUE;
    int _2391 = NOVALUE;
    int _2390 = NOVALUE;
    int _2389 = NOVALUE;
    int _2387 = NOVALUE;
    int _2384 = NOVALUE;
    int _2383 = NOVALUE;
    int _2382 = NOVALUE;
    int _2381 = NOVALUE;
    int _2380 = NOVALUE;
    int _2379 = NOVALUE;
    int _2378 = NOVALUE;
    int _2377 = NOVALUE;
    int _2376 = NOVALUE;
    int _2374 = NOVALUE;
    int _2371 = NOVALUE;
    int _2370 = NOVALUE;
    int _2369 = NOVALUE;
    int _2368 = NOVALUE;
    int _2367 = NOVALUE;
    int _2366 = NOVALUE;
    int _0, _1, _2;
    

    /** 	precision = abs(precision)*/
    Ref(_precision_4663);
    _0 = _precision_4663;
    _precision_4663 = _18abs(_precision_4663);
    DeRef(_0);

    /** 	if atom(a) then*/
    _2366 = IS_ATOM(_a_4662);
    if (_2366 == 0)
    {
        _2366 = NOVALUE;
        goto L1; // [12] 142
    }
    else{
        _2366 = NOVALUE;
    }

    /** 		if atom(precision) then*/
    _2367 = IS_ATOM(_precision_4663);
    if (_2367 == 0)
    {
        _2367 = NOVALUE;
        goto L2; // [20] 45
    }
    else{
        _2367 = NOVALUE;
    }

    /** 			return floor(0.5 + (a * precision )) / precision*/
    if (IS_ATOM_INT(_a_4662) && IS_ATOM_INT(_precision_4663)) {
        if (_a_4662 == (short)_a_4662 && _precision_4663 <= INT15 && _precision_4663 >= -INT15)
        _2368 = _a_4662 * _precision_4663;
        else
        _2368 = NewDouble(_a_4662 * (double)_precision_4663);
    }
    else {
        _2368 = binary_op(MULTIPLY, _a_4662, _precision_4663);
    }
    _2369 = binary_op(PLUS, _1692, _2368);
    DeRef(_2368);
    _2368 = NOVALUE;
    if (IS_ATOM_INT(_2369))
    _2370 = e_floor(_2369);
    else
    _2370 = unary_op(FLOOR, _2369);
    DeRef(_2369);
    _2369 = NOVALUE;
    if (IS_ATOM_INT(_2370) && IS_ATOM_INT(_precision_4663)) {
        _2371 = (_2370 % _precision_4663) ? NewDouble((double)_2370 / _precision_4663) : (_2370 / _precision_4663);
    }
    else {
        _2371 = binary_op(DIVIDE, _2370, _precision_4663);
    }
    DeRef(_2370);
    _2370 = NOVALUE;
    DeRef(_a_4662);
    DeRef(_precision_4663);
    DeRef(_s_4665);
    DeRef(_t_4666);
    DeRef(_u_4667);
    return _2371;
L2: 

    /** 		len = length(precision)*/
    if (IS_SEQUENCE(_precision_4663)){
            _len_4664 = SEQ_PTR(_precision_4663)->length;
    }
    else {
        _len_4664 = 1;
    }

    /** 		s = repeat(0, len)*/
    DeRef(_s_4665);
    _s_4665 = Repeat(0, _len_4664);

    /** 		for i = 1 to len do*/
    _2374 = _len_4664;
    {
        int _i_4680;
        _i_4680 = 1;
L3: 
        if (_i_4680 > _2374){
            goto L4; // [63] 133
        }

        /** 			t = precision[i]*/
        DeRef(_t_4666);
        _2 = (int)SEQ_PTR(_precision_4663);
        _t_4666 = (int)*(((s1_ptr)_2)->base + _i_4680);
        Ref(_t_4666);

        /** 			if atom (t) then*/
        _2376 = IS_ATOM(_t_4666);
        if (_2376 == 0)
        {
            _2376 = NOVALUE;
            goto L5; // [81] 108
        }
        else{
            _2376 = NOVALUE;
        }

        /** 				s[i] = floor( 0.5 + (a * t)) / t*/
        if (IS_ATOM_INT(_a_4662) && IS_ATOM_INT(_t_4666)) {
            if (_a_4662 == (short)_a_4662 && _t_4666 <= INT15 && _t_4666 >= -INT15)
            _2377 = _a_4662 * _t_4666;
            else
            _2377 = NewDouble(_a_4662 * (double)_t_4666);
        }
        else {
            _2377 = binary_op(MULTIPLY, _a_4662, _t_4666);
        }
        _2378 = binary_op(PLUS, _1692, _2377);
        DeRef(_2377);
        _2377 = NOVALUE;
        if (IS_ATOM_INT(_2378))
        _2379 = e_floor(_2378);
        else
        _2379 = unary_op(FLOOR, _2378);
        DeRef(_2378);
        _2378 = NOVALUE;
        if (IS_ATOM_INT(_2379) && IS_ATOM_INT(_t_4666)) {
            _2380 = (_2379 % _t_4666) ? NewDouble((double)_2379 / _t_4666) : (_2379 / _t_4666);
        }
        else {
            _2380 = binary_op(DIVIDE, _2379, _t_4666);
        }
        DeRef(_2379);
        _2379 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4680);
        _1 = *(int *)_2;
        *(int *)_2 = _2380;
        if( _1 != _2380 ){
            DeRef(_1);
        }
        _2380 = NOVALUE;
        goto L6; // [105] 126
L5: 

        /** 				s[i] = round(a, t)*/
        Ref(_a_4662);
        DeRef(_2381);
        _2381 = _a_4662;
        Ref(_t_4666);
        DeRef(_2382);
        _2382 = _t_4666;
        _2383 = _18round(_2381, _2382);
        _2381 = NOVALUE;
        _2382 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4680);
        _1 = *(int *)_2;
        *(int *)_2 = _2383;
        if( _1 != _2383 ){
            DeRef(_1);
        }
        _2383 = NOVALUE;
L6: 

        /** 		end for*/
        _i_4680 = _i_4680 + 1;
        goto L3; // [128] 70
L4: 
        ;
    }

    /** 		return s*/
    DeRef(_a_4662);
    DeRef(_precision_4663);
    DeRef(_t_4666);
    DeRef(_u_4667);
    DeRef(_2371);
    _2371 = NOVALUE;
    return _s_4665;
    goto L7; // [139] 246
L1: 

    /** 	elsif atom(precision) then*/
    _2384 = IS_ATOM(_precision_4663);
    if (_2384 == 0)
    {
        _2384 = NOVALUE;
        goto L8; // [147] 245
    }
    else{
        _2384 = NOVALUE;
    }

    /** 		len = length(a)*/
    if (IS_SEQUENCE(_a_4662)){
            _len_4664 = SEQ_PTR(_a_4662)->length;
    }
    else {
        _len_4664 = 1;
    }

    /** 		s = repeat(0, len)*/
    DeRef(_s_4665);
    _s_4665 = Repeat(0, _len_4664);

    /** 		for i = 1 to len do*/
    _2387 = _len_4664;
    {
        int _i_4698;
        _i_4698 = 1;
L9: 
        if (_i_4698 > _2387){
            goto LA; // [168] 238
        }

        /** 			t = a[i]*/
        DeRef(_t_4666);
        _2 = (int)SEQ_PTR(_a_4662);
        _t_4666 = (int)*(((s1_ptr)_2)->base + _i_4698);
        Ref(_t_4666);

        /** 			if atom(t) then*/
        _2389 = IS_ATOM(_t_4666);
        if (_2389 == 0)
        {
            _2389 = NOVALUE;
            goto LB; // [186] 213
        }
        else{
            _2389 = NOVALUE;
        }

        /** 				s[i] = floor(0.5 + (t * precision)) / precision*/
        if (IS_ATOM_INT(_t_4666) && IS_ATOM_INT(_precision_4663)) {
            if (_t_4666 == (short)_t_4666 && _precision_4663 <= INT15 && _precision_4663 >= -INT15)
            _2390 = _t_4666 * _precision_4663;
            else
            _2390 = NewDouble(_t_4666 * (double)_precision_4663);
        }
        else {
            _2390 = binary_op(MULTIPLY, _t_4666, _precision_4663);
        }
        _2391 = binary_op(PLUS, _1692, _2390);
        DeRef(_2390);
        _2390 = NOVALUE;
        if (IS_ATOM_INT(_2391))
        _2392 = e_floor(_2391);
        else
        _2392 = unary_op(FLOOR, _2391);
        DeRef(_2391);
        _2391 = NOVALUE;
        if (IS_ATOM_INT(_2392) && IS_ATOM_INT(_precision_4663)) {
            _2393 = (_2392 % _precision_4663) ? NewDouble((double)_2392 / _precision_4663) : (_2392 / _precision_4663);
        }
        else {
            _2393 = binary_op(DIVIDE, _2392, _precision_4663);
        }
        DeRef(_2392);
        _2392 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4698);
        _1 = *(int *)_2;
        *(int *)_2 = _2393;
        if( _1 != _2393 ){
            DeRef(_1);
        }
        _2393 = NOVALUE;
        goto LC; // [210] 231
LB: 

        /** 				s[i] = round(t, precision)*/
        Ref(_t_4666);
        DeRef(_2394);
        _2394 = _t_4666;
        Ref(_precision_4663);
        DeRef(_2395);
        _2395 = _precision_4663;
        _2396 = _18round(_2394, _2395);
        _2394 = NOVALUE;
        _2395 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4698);
        _1 = *(int *)_2;
        *(int *)_2 = _2396;
        if( _1 != _2396 ){
            DeRef(_1);
        }
        _2396 = NOVALUE;
LC: 

        /** 		end for*/
        _i_4698 = _i_4698 + 1;
        goto L9; // [233] 175
LA: 
        ;
    }

    /** 		return s*/
    DeRef(_a_4662);
    DeRef(_precision_4663);
    DeRef(_t_4666);
    DeRef(_u_4667);
    DeRef(_2371);
    _2371 = NOVALUE;
    return _s_4665;
L8: 
L7: 

    /** 	len = length(a)*/
    if (IS_SEQUENCE(_a_4662)){
            _len_4664 = SEQ_PTR(_a_4662)->length;
    }
    else {
        _len_4664 = 1;
    }

    /** 	if len != length(precision) then*/
    if (IS_SEQUENCE(_precision_4663)){
            _2398 = SEQ_PTR(_precision_4663)->length;
    }
    else {
        _2398 = 1;
    }
    if (_len_4664 == _2398)
    goto LD; // [258] 283

    /** 		error:crash("The lengths of the two supplied sequences do not match.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_263_4717);
    _msg_inlined_crash_at_263_4717 = EPrintf(-9999999, _2400, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_263_4717);

    /** end procedure*/
    goto LE; // [277] 280
LE: 
    DeRefi(_msg_inlined_crash_at_263_4717);
    _msg_inlined_crash_at_263_4717 = NOVALUE;
LD: 

    /** 	s = repeat(0, len)*/
    DeRef(_s_4665);
    _s_4665 = Repeat(0, _len_4664);

    /** 	for i = 1 to len do*/
    _2402 = _len_4664;
    {
        int _i_4720;
        _i_4720 = 1;
LF: 
        if (_i_4720 > _2402){
            goto L10; // [294] 400
        }

        /** 		t = precision[i]*/
        DeRef(_t_4666);
        _2 = (int)SEQ_PTR(_precision_4663);
        _t_4666 = (int)*(((s1_ptr)_2)->base + _i_4720);
        Ref(_t_4666);

        /** 		if atom(t) then*/
        _2404 = IS_ATOM(_t_4666);
        if (_2404 == 0)
        {
            _2404 = NOVALUE;
            goto L11; // [312] 374
        }
        else{
            _2404 = NOVALUE;
        }

        /** 			u = a[i]*/
        DeRef(_u_4667);
        _2 = (int)SEQ_PTR(_a_4662);
        _u_4667 = (int)*(((s1_ptr)_2)->base + _i_4720);
        Ref(_u_4667);

        /** 			if atom(u) then*/
        _2406 = IS_ATOM(_u_4667);
        if (_2406 == 0)
        {
            _2406 = NOVALUE;
            goto L12; // [326] 353
        }
        else{
            _2406 = NOVALUE;
        }

        /** 				s[i] = floor(0.5 + (u * t)) / t*/
        if (IS_ATOM_INT(_u_4667) && IS_ATOM_INT(_t_4666)) {
            if (_u_4667 == (short)_u_4667 && _t_4666 <= INT15 && _t_4666 >= -INT15)
            _2407 = _u_4667 * _t_4666;
            else
            _2407 = NewDouble(_u_4667 * (double)_t_4666);
        }
        else {
            _2407 = binary_op(MULTIPLY, _u_4667, _t_4666);
        }
        _2408 = binary_op(PLUS, _1692, _2407);
        DeRef(_2407);
        _2407 = NOVALUE;
        if (IS_ATOM_INT(_2408))
        _2409 = e_floor(_2408);
        else
        _2409 = unary_op(FLOOR, _2408);
        DeRef(_2408);
        _2408 = NOVALUE;
        if (IS_ATOM_INT(_2409) && IS_ATOM_INT(_t_4666)) {
            _2410 = (_2409 % _t_4666) ? NewDouble((double)_2409 / _t_4666) : (_2409 / _t_4666);
        }
        else {
            _2410 = binary_op(DIVIDE, _2409, _t_4666);
        }
        DeRef(_2409);
        _2409 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4720);
        _1 = *(int *)_2;
        *(int *)_2 = _2410;
        if( _1 != _2410 ){
            DeRef(_1);
        }
        _2410 = NOVALUE;
        goto L13; // [350] 393
L12: 

        /** 				s[i] = round(u, t)*/
        Ref(_u_4667);
        DeRef(_2411);
        _2411 = _u_4667;
        Ref(_t_4666);
        DeRef(_2412);
        _2412 = _t_4666;
        _2413 = _18round(_2411, _2412);
        _2411 = NOVALUE;
        _2412 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4720);
        _1 = *(int *)_2;
        *(int *)_2 = _2413;
        if( _1 != _2413 ){
            DeRef(_1);
        }
        _2413 = NOVALUE;
        goto L13; // [371] 393
L11: 

        /** 			s[i] = round(a[i], t)*/
        _2 = (int)SEQ_PTR(_a_4662);
        _2414 = (int)*(((s1_ptr)_2)->base + _i_4720);
        Ref(_t_4666);
        DeRef(_2415);
        _2415 = _t_4666;
        Ref(_2414);
        _2416 = _18round(_2414, _2415);
        _2414 = NOVALUE;
        _2415 = NOVALUE;
        _2 = (int)SEQ_PTR(_s_4665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_4665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4720);
        _1 = *(int *)_2;
        *(int *)_2 = _2416;
        if( _1 != _2416 ){
            DeRef(_1);
        }
        _2416 = NOVALUE;
L13: 

        /** 	end for*/
        _i_4720 = _i_4720 + 1;
        goto LF; // [395] 301
L10: 
        ;
    }

    /** 	return s*/
    DeRef(_a_4662);
    DeRef(_precision_4663);
    DeRef(_t_4666);
    DeRef(_u_4667);
    DeRef(_2371);
    _2371 = NOVALUE;
    return _s_4665;
    ;
}


int _18arccos(int _x_4742)
{
    int _2424 = NOVALUE;
    int _2423 = NOVALUE;
    int _2422 = NOVALUE;
    int _2421 = NOVALUE;
    int _2420 = NOVALUE;
    int _2419 = NOVALUE;
    int _2418 = NOVALUE;
    int _2417 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return mathcons:HALFPI - 2 * arctan(x / (1.0 + sqrt(1.0 - x * x)))*/
    if (IS_ATOM_INT(_x_4742) && IS_ATOM_INT(_x_4742)) {
        if (_x_4742 == (short)_x_4742 && _x_4742 <= INT15 && _x_4742 >= -INT15)
        _2417 = _x_4742 * _x_4742;
        else
        _2417 = NewDouble(_x_4742 * (double)_x_4742);
    }
    else {
        _2417 = binary_op(MULTIPLY, _x_4742, _x_4742);
    }
    _2418 = binary_op(MINUS, _2234, _2417);
    DeRef(_2417);
    _2417 = NOVALUE;
    if (IS_ATOM_INT(_2418))
    _2419 = e_sqrt(_2418);
    else
    _2419 = unary_op(SQRT, _2418);
    DeRef(_2418);
    _2418 = NOVALUE;
    _2420 = binary_op(PLUS, _2234, _2419);
    DeRef(_2419);
    _2419 = NOVALUE;
    if (IS_ATOM_INT(_x_4742) && IS_ATOM_INT(_2420)) {
        _2421 = (_x_4742 % _2420) ? NewDouble((double)_x_4742 / _2420) : (_x_4742 / _2420);
    }
    else {
        _2421 = binary_op(DIVIDE, _x_4742, _2420);
    }
    DeRef(_2420);
    _2420 = NOVALUE;
    if (IS_ATOM_INT(_2421))
    _2422 = e_arctan(_2421);
    else
    _2422 = unary_op(ARCTAN, _2421);
    DeRef(_2421);
    _2421 = NOVALUE;
    if (IS_ATOM_INT(_2422) && IS_ATOM_INT(_2422)) {
        _2423 = _2422 + _2422;
        if ((long)((unsigned long)_2423 + (unsigned long)HIGH_BITS) >= 0) 
        _2423 = NewDouble((double)_2423);
    }
    else {
        _2423 = binary_op(PLUS, _2422, _2422);
    }
    DeRef(_2422);
    _2422 = NOVALUE;
    _2422 = NOVALUE;
    _2424 = binary_op(MINUS, _20HALFPI_4432, _2423);
    DeRef(_2423);
    _2423 = NOVALUE;
    DeRef(_x_4742);
    return _2424;
    ;
}


int _18arcsin(int _x_4753)
{
    int _2431 = NOVALUE;
    int _2430 = NOVALUE;
    int _2429 = NOVALUE;
    int _2428 = NOVALUE;
    int _2427 = NOVALUE;
    int _2426 = NOVALUE;
    int _2425 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return 2 * arctan(x / (1.0 + sqrt(1.0 - x * x)))*/
    if (IS_ATOM_INT(_x_4753) && IS_ATOM_INT(_x_4753)) {
        if (_x_4753 == (short)_x_4753 && _x_4753 <= INT15 && _x_4753 >= -INT15)
        _2425 = _x_4753 * _x_4753;
        else
        _2425 = NewDouble(_x_4753 * (double)_x_4753);
    }
    else {
        _2425 = binary_op(MULTIPLY, _x_4753, _x_4753);
    }
    _2426 = binary_op(MINUS, _2234, _2425);
    DeRef(_2425);
    _2425 = NOVALUE;
    if (IS_ATOM_INT(_2426))
    _2427 = e_sqrt(_2426);
    else
    _2427 = unary_op(SQRT, _2426);
    DeRef(_2426);
    _2426 = NOVALUE;
    _2428 = binary_op(PLUS, _2234, _2427);
    DeRef(_2427);
    _2427 = NOVALUE;
    if (IS_ATOM_INT(_x_4753) && IS_ATOM_INT(_2428)) {
        _2429 = (_x_4753 % _2428) ? NewDouble((double)_x_4753 / _2428) : (_x_4753 / _2428);
    }
    else {
        _2429 = binary_op(DIVIDE, _x_4753, _2428);
    }
    DeRef(_2428);
    _2428 = NOVALUE;
    if (IS_ATOM_INT(_2429))
    _2430 = e_arctan(_2429);
    else
    _2430 = unary_op(ARCTAN, _2429);
    DeRef(_2429);
    _2429 = NOVALUE;
    if (IS_ATOM_INT(_2430) && IS_ATOM_INT(_2430)) {
        _2431 = _2430 + _2430;
        if ((long)((unsigned long)_2431 + (unsigned long)HIGH_BITS) >= 0) 
        _2431 = NewDouble((double)_2431);
    }
    else {
        _2431 = binary_op(PLUS, _2430, _2430);
    }
    DeRef(_2430);
    _2430 = NOVALUE;
    _2430 = NOVALUE;
    DeRef(_x_4753);
    return _2431;
    ;
}


int _18atan2(int _y_4763, int _x_4764)
{
    int _2442 = NOVALUE;
    int _2441 = NOVALUE;
    int _2440 = NOVALUE;
    int _2439 = NOVALUE;
    int _2438 = NOVALUE;
    int _2437 = NOVALUE;
    int _2434 = NOVALUE;
    int _2433 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if x > 0 then*/
    if (binary_op_a(LESSEQ, _x_4764, 0)){
        goto L1; // [3] 23
    }

    /** 		return arctan(y/x)*/
    if (IS_ATOM_INT(_y_4763) && IS_ATOM_INT(_x_4764)) {
        _2433 = (_y_4763 % _x_4764) ? NewDouble((double)_y_4763 / _x_4764) : (_y_4763 / _x_4764);
    }
    else {
        if (IS_ATOM_INT(_y_4763)) {
            _2433 = NewDouble((double)_y_4763 / DBL_PTR(_x_4764)->dbl);
        }
        else {
            if (IS_ATOM_INT(_x_4764)) {
                _2433 = NewDouble(DBL_PTR(_y_4763)->dbl / (double)_x_4764);
            }
            else
            _2433 = NewDouble(DBL_PTR(_y_4763)->dbl / DBL_PTR(_x_4764)->dbl);
        }
    }
    if (IS_ATOM_INT(_2433))
    _2434 = e_arctan(_2433);
    else
    _2434 = unary_op(ARCTAN, _2433);
    DeRef(_2433);
    _2433 = NOVALUE;
    DeRef(_y_4763);
    DeRef(_x_4764);
    return _2434;
    goto L2; // [20] 113
L1: 

    /** 	elsif x < 0 then*/
    if (binary_op_a(GREATEREQ, _x_4764, 0)){
        goto L3; // [25] 76
    }

    /** 		if y < 0 then*/
    if (binary_op_a(GREATEREQ, _y_4763, 0)){
        goto L4; // [31] 55
    }

    /** 			return arctan(y/x) - mathcons:PI*/
    if (IS_ATOM_INT(_y_4763) && IS_ATOM_INT(_x_4764)) {
        _2437 = (_y_4763 % _x_4764) ? NewDouble((double)_y_4763 / _x_4764) : (_y_4763 / _x_4764);
    }
    else {
        if (IS_ATOM_INT(_y_4763)) {
            _2437 = NewDouble((double)_y_4763 / DBL_PTR(_x_4764)->dbl);
        }
        else {
            if (IS_ATOM_INT(_x_4764)) {
                _2437 = NewDouble(DBL_PTR(_y_4763)->dbl / (double)_x_4764);
            }
            else
            _2437 = NewDouble(DBL_PTR(_y_4763)->dbl / DBL_PTR(_x_4764)->dbl);
        }
    }
    if (IS_ATOM_INT(_2437))
    _2438 = e_arctan(_2437);
    else
    _2438 = unary_op(ARCTAN, _2437);
    DeRef(_2437);
    _2437 = NOVALUE;
    _2439 = NewDouble(DBL_PTR(_2438)->dbl - DBL_PTR(_20PI_4428)->dbl);
    DeRefDS(_2438);
    _2438 = NOVALUE;
    DeRef(_y_4763);
    DeRef(_x_4764);
    DeRef(_2434);
    _2434 = NOVALUE;
    return _2439;
    goto L2; // [52] 113
L4: 

    /** 			return arctan(y/x) + mathcons:PI*/
    if (IS_ATOM_INT(_y_4763) && IS_ATOM_INT(_x_4764)) {
        _2440 = (_y_4763 % _x_4764) ? NewDouble((double)_y_4763 / _x_4764) : (_y_4763 / _x_4764);
    }
    else {
        if (IS_ATOM_INT(_y_4763)) {
            _2440 = NewDouble((double)_y_4763 / DBL_PTR(_x_4764)->dbl);
        }
        else {
            if (IS_ATOM_INT(_x_4764)) {
                _2440 = NewDouble(DBL_PTR(_y_4763)->dbl / (double)_x_4764);
            }
            else
            _2440 = NewDouble(DBL_PTR(_y_4763)->dbl / DBL_PTR(_x_4764)->dbl);
        }
    }
    if (IS_ATOM_INT(_2440))
    _2441 = e_arctan(_2440);
    else
    _2441 = unary_op(ARCTAN, _2440);
    DeRef(_2440);
    _2440 = NOVALUE;
    _2442 = NewDouble(DBL_PTR(_2441)->dbl + DBL_PTR(_20PI_4428)->dbl);
    DeRefDS(_2441);
    _2441 = NOVALUE;
    DeRef(_y_4763);
    DeRef(_x_4764);
    DeRef(_2434);
    _2434 = NOVALUE;
    DeRef(_2439);
    _2439 = NOVALUE;
    return _2442;
    goto L2; // [73] 113
L3: 

    /** 	elsif y > 0 then*/
    if (binary_op_a(LESSEQ, _y_4763, 0)){
        goto L5; // [78] 91
    }

    /** 		return mathcons:HALFPI*/
    RefDS(_20HALFPI_4432);
    DeRef(_y_4763);
    DeRef(_x_4764);
    DeRef(_2434);
    _2434 = NOVALUE;
    DeRef(_2439);
    _2439 = NOVALUE;
    DeRef(_2442);
    _2442 = NOVALUE;
    return _20HALFPI_4432;
    goto L2; // [88] 113
L5: 

    /** 	elsif y < 0 then*/
    if (binary_op_a(GREATEREQ, _y_4763, 0)){
        goto L6; // [93] 106
    }

    /** 		return -(mathcons:HALFPI)*/
    RefDS(_2445);
    DeRef(_y_4763);
    DeRef(_x_4764);
    DeRef(_2434);
    _2434 = NOVALUE;
    DeRef(_2439);
    _2439 = NOVALUE;
    DeRef(_2442);
    _2442 = NOVALUE;
    return _2445;
    goto L2; // [103] 113
L6: 

    /** 		return 0*/
    DeRef(_y_4763);
    DeRef(_x_4764);
    DeRef(_2434);
    _2434 = NOVALUE;
    DeRef(_2439);
    _2439 = NOVALUE;
    DeRef(_2442);
    _2442 = NOVALUE;
    return 0;
L2: 
    ;
}


int _18rad2deg(int _x_4788)
{
    int _2446 = NOVALUE;
    int _0, _1, _2;
    

    /**    return x * mathcons:RADIANS_TO_DEGREES*/
    _2446 = binary_op(MULTIPLY, _x_4788, _20RADIANS_TO_DEGREES_4460);
    DeRef(_x_4788);
    return _2446;
    ;
}


int _18deg2rad(int _x_4792)
{
    int _2447 = NOVALUE;
    int _0, _1, _2;
    

    /**    return x * mathcons:DEGREES_TO_RADIANS*/
    _2447 = binary_op(MULTIPLY, _x_4792, _20DEGREES_TO_RADIANS_4458);
    DeRef(_x_4792);
    return _2447;
    ;
}


int _18log10(int _x1_4796)
{
    int _2449 = NOVALUE;
    int _2448 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return log(x1) * mathcons:INVLN10*/
    if (IS_ATOM_INT(_x1_4796))
    _2448 = e_log(_x1_4796);
    else
    _2448 = unary_op(LOG, _x1_4796);
    _2449 = binary_op(MULTIPLY, _2448, _20INVLN10_4450);
    DeRef(_2448);
    _2448 = NOVALUE;
    DeRef(_x1_4796);
    return _2449;
    ;
}


int _18exp(int _x_4801)
{
    int _2450 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return power( mathcons:E, x)*/
    if (IS_ATOM_INT(_x_4801)) {
        temp_d.dbl = (double)_x_4801;
        _2450 = Dpower(DBL_PTR(_20E_4442), &temp_d);
    }
    else
    _2450 = Dpower(DBL_PTR(_20E_4442), DBL_PTR(_x_4801));
    DeRef(_x_4801);
    return _2450;
    ;
}


int _18fib(int _i_4805)
{
    int _2454 = NOVALUE;
    int _2453 = NOVALUE;
    int _2452 = NOVALUE;
    int _2451 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_i_4805)) {
        _1 = (long)(DBL_PTR(_i_4805)->dbl);
        if (UNIQUE(DBL_PTR(_i_4805)) && (DBL_PTR(_i_4805)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_4805);
        _i_4805 = _1;
    }

    /** 	return floor((power( mathcons:PHI, i) / mathcons:SQRT5) + 0.5)*/
    temp_d.dbl = (double)_i_4805;
    _2451 = Dpower(DBL_PTR(_20PHI_4440), &temp_d);
    _2452 = NewDouble(DBL_PTR(_2451)->dbl / DBL_PTR(_20SQRT5_4472)->dbl);
    DeRefDS(_2451);
    _2451 = NOVALUE;
    _2453 = NewDouble(DBL_PTR(_2452)->dbl + DBL_PTR(_1692)->dbl);
    DeRefDS(_2452);
    _2452 = NOVALUE;
    _2454 = unary_op(FLOOR, _2453);
    DeRefDS(_2453);
    _2453 = NOVALUE;
    return _2454;
    ;
}


int _18cosh(int _a_4812)
{
    int _exp_inlined_exp_at_2_4814 = NOVALUE;
    int _exp_inlined_exp_at_15_4818 = NOVALUE;
    int _x_inlined_exp_at_12_4817 = NOVALUE;
    int _2457 = NOVALUE;
    int _2456 = NOVALUE;
    int _2455 = NOVALUE;
    int _0, _1, _2;
    

    /**     return (exp(a)+exp(-a))/2*/

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_2_4814);
    _exp_inlined_exp_at_2_4814 = binary_op(POWER, _20E_4442, _a_4812);
    if (IS_ATOM_INT(_a_4812)) {
        if ((unsigned long)_a_4812 == 0xC0000000)
        _2455 = (int)NewDouble((double)-0xC0000000);
        else
        _2455 = - _a_4812;
    }
    else {
        _2455 = unary_op(UMINUS, _a_4812);
    }
    DeRef(_x_inlined_exp_at_12_4817);
    _x_inlined_exp_at_12_4817 = _2455;
    _2455 = NOVALUE;

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_15_4818);
    _exp_inlined_exp_at_15_4818 = binary_op(POWER, _20E_4442, _x_inlined_exp_at_12_4817);
    DeRef(_x_inlined_exp_at_12_4817);
    _x_inlined_exp_at_12_4817 = NOVALUE;
    if (IS_ATOM_INT(_exp_inlined_exp_at_2_4814) && IS_ATOM_INT(_exp_inlined_exp_at_15_4818)) {
        _2456 = _exp_inlined_exp_at_2_4814 + _exp_inlined_exp_at_15_4818;
        if ((long)((unsigned long)_2456 + (unsigned long)HIGH_BITS) >= 0) 
        _2456 = NewDouble((double)_2456);
    }
    else {
        _2456 = binary_op(PLUS, _exp_inlined_exp_at_2_4814, _exp_inlined_exp_at_15_4818);
    }
    if (IS_ATOM_INT(_2456)) {
        if (_2456 & 1) {
            _2457 = NewDouble((_2456 >> 1) + 0.5);
        }
        else
        _2457 = _2456 >> 1;
    }
    else {
        _2457 = binary_op(DIVIDE, _2456, 2);
    }
    DeRef(_2456);
    _2456 = NOVALUE;
    DeRef(_a_4812);
    return _2457;
    ;
}


int _18sinh(int _a_4823)
{
    int _exp_inlined_exp_at_2_4825 = NOVALUE;
    int _exp_inlined_exp_at_15_4829 = NOVALUE;
    int _x_inlined_exp_at_12_4828 = NOVALUE;
    int _2460 = NOVALUE;
    int _2459 = NOVALUE;
    int _2458 = NOVALUE;
    int _0, _1, _2;
    

    /**     return (exp(a)-exp(-a))/2*/

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_2_4825);
    _exp_inlined_exp_at_2_4825 = binary_op(POWER, _20E_4442, _a_4823);
    if (IS_ATOM_INT(_a_4823)) {
        if ((unsigned long)_a_4823 == 0xC0000000)
        _2458 = (int)NewDouble((double)-0xC0000000);
        else
        _2458 = - _a_4823;
    }
    else {
        _2458 = unary_op(UMINUS, _a_4823);
    }
    DeRef(_x_inlined_exp_at_12_4828);
    _x_inlined_exp_at_12_4828 = _2458;
    _2458 = NOVALUE;

    /** 	return power( mathcons:E, x)*/
    DeRef(_exp_inlined_exp_at_15_4829);
    _exp_inlined_exp_at_15_4829 = binary_op(POWER, _20E_4442, _x_inlined_exp_at_12_4828);
    DeRef(_x_inlined_exp_at_12_4828);
    _x_inlined_exp_at_12_4828 = NOVALUE;
    if (IS_ATOM_INT(_exp_inlined_exp_at_2_4825) && IS_ATOM_INT(_exp_inlined_exp_at_15_4829)) {
        _2459 = _exp_inlined_exp_at_2_4825 - _exp_inlined_exp_at_15_4829;
        if ((long)((unsigned long)_2459 +(unsigned long) HIGH_BITS) >= 0){
            _2459 = NewDouble((double)_2459);
        }
    }
    else {
        _2459 = binary_op(MINUS, _exp_inlined_exp_at_2_4825, _exp_inlined_exp_at_15_4829);
    }
    if (IS_ATOM_INT(_2459)) {
        if (_2459 & 1) {
            _2460 = NewDouble((_2459 >> 1) + 0.5);
        }
        else
        _2460 = _2459 >> 1;
    }
    else {
        _2460 = binary_op(DIVIDE, _2459, 2);
    }
    DeRef(_2459);
    _2459 = NOVALUE;
    DeRef(_a_4823);
    return _2460;
    ;
}


int _18tanh(int _a_4834)
{
    int _2463 = NOVALUE;
    int _2462 = NOVALUE;
    int _2461 = NOVALUE;
    int _0, _1, _2;
    

    /**     return sinh(a)/cosh(a)*/
    Ref(_a_4834);
    _2461 = _18sinh(_a_4834);
    Ref(_a_4834);
    _2462 = _18cosh(_a_4834);
    if (IS_ATOM_INT(_2461) && IS_ATOM_INT(_2462)) {
        _2463 = (_2461 % _2462) ? NewDouble((double)_2461 / _2462) : (_2461 / _2462);
    }
    else {
        _2463 = binary_op(DIVIDE, _2461, _2462);
    }
    DeRef(_2461);
    _2461 = NOVALUE;
    DeRef(_2462);
    _2462 = NOVALUE;
    DeRef(_a_4834);
    return _2463;
    ;
}


int _18arcsinh(int _a_4840)
{
    int _2468 = NOVALUE;
    int _2467 = NOVALUE;
    int _2466 = NOVALUE;
    int _2465 = NOVALUE;
    int _2464 = NOVALUE;
    int _0, _1, _2;
    

    /**     return log(a+sqrt(1+a*a))*/
    if (IS_ATOM_INT(_a_4840) && IS_ATOM_INT(_a_4840)) {
        if (_a_4840 == (short)_a_4840 && _a_4840 <= INT15 && _a_4840 >= -INT15)
        _2464 = _a_4840 * _a_4840;
        else
        _2464 = NewDouble(_a_4840 * (double)_a_4840);
    }
    else {
        _2464 = binary_op(MULTIPLY, _a_4840, _a_4840);
    }
    if (IS_ATOM_INT(_2464)) {
        _2465 = _2464 + 1;
        if (_2465 > MAXINT){
            _2465 = NewDouble((double)_2465);
        }
    }
    else
    _2465 = binary_op(PLUS, 1, _2464);
    DeRef(_2464);
    _2464 = NOVALUE;
    if (IS_ATOM_INT(_2465))
    _2466 = e_sqrt(_2465);
    else
    _2466 = unary_op(SQRT, _2465);
    DeRef(_2465);
    _2465 = NOVALUE;
    if (IS_ATOM_INT(_a_4840) && IS_ATOM_INT(_2466)) {
        _2467 = _a_4840 + _2466;
        if ((long)((unsigned long)_2467 + (unsigned long)HIGH_BITS) >= 0) 
        _2467 = NewDouble((double)_2467);
    }
    else {
        _2467 = binary_op(PLUS, _a_4840, _2466);
    }
    DeRef(_2466);
    _2466 = NOVALUE;
    if (IS_ATOM_INT(_2467))
    _2468 = e_log(_2467);
    else
    _2468 = unary_op(LOG, _2467);
    DeRef(_2467);
    _2467 = NOVALUE;
    DeRef(_a_4840);
    return _2468;
    ;
}


int _18arccosh(int _a_4861)
{
    int _2479 = NOVALUE;
    int _2478 = NOVALUE;
    int _2477 = NOVALUE;
    int _2476 = NOVALUE;
    int _2475 = NOVALUE;
    int _0, _1, _2;
    

    /**     return log(a+sqrt(a*a-1))*/
    if (IS_ATOM_INT(_a_4861) && IS_ATOM_INT(_a_4861)) {
        if (_a_4861 == (short)_a_4861 && _a_4861 <= INT15 && _a_4861 >= -INT15)
        _2475 = _a_4861 * _a_4861;
        else
        _2475 = NewDouble(_a_4861 * (double)_a_4861);
    }
    else {
        _2475 = binary_op(MULTIPLY, _a_4861, _a_4861);
    }
    if (IS_ATOM_INT(_2475)) {
        _2476 = _2475 - 1;
        if ((long)((unsigned long)_2476 +(unsigned long) HIGH_BITS) >= 0){
            _2476 = NewDouble((double)_2476);
        }
    }
    else {
        _2476 = binary_op(MINUS, _2475, 1);
    }
    DeRef(_2475);
    _2475 = NOVALUE;
    if (IS_ATOM_INT(_2476))
    _2477 = e_sqrt(_2476);
    else
    _2477 = unary_op(SQRT, _2476);
    DeRef(_2476);
    _2476 = NOVALUE;
    if (IS_ATOM_INT(_a_4861) && IS_ATOM_INT(_2477)) {
        _2478 = _a_4861 + _2477;
        if ((long)((unsigned long)_2478 + (unsigned long)HIGH_BITS) >= 0) 
        _2478 = NewDouble((double)_2478);
    }
    else {
        _2478 = binary_op(PLUS, _a_4861, _2477);
    }
    DeRef(_2477);
    _2477 = NOVALUE;
    if (IS_ATOM_INT(_2478))
    _2479 = e_log(_2478);
    else
    _2479 = unary_op(LOG, _2478);
    DeRef(_2478);
    _2478 = NOVALUE;
    DeRef(_a_4861);
    return _2479;
    ;
}


int _18arctanh(int _a_4885)
{
    int _2493 = NOVALUE;
    int _2492 = NOVALUE;
    int _2491 = NOVALUE;
    int _2490 = NOVALUE;
    int _2489 = NOVALUE;
    int _0, _1, _2;
    

    /**     return log((1+a)/(1-a))/2*/
    if (IS_ATOM_INT(_a_4885)) {
        _2489 = _a_4885 + 1;
        if (_2489 > MAXINT){
            _2489 = NewDouble((double)_2489);
        }
    }
    else
    _2489 = binary_op(PLUS, 1, _a_4885);
    if (IS_ATOM_INT(_a_4885)) {
        _2490 = 1 - _a_4885;
        if ((long)((unsigned long)_2490 +(unsigned long) HIGH_BITS) >= 0){
            _2490 = NewDouble((double)_2490);
        }
    }
    else {
        _2490 = binary_op(MINUS, 1, _a_4885);
    }
    if (IS_ATOM_INT(_2489) && IS_ATOM_INT(_2490)) {
        _2491 = (_2489 % _2490) ? NewDouble((double)_2489 / _2490) : (_2489 / _2490);
    }
    else {
        _2491 = binary_op(DIVIDE, _2489, _2490);
    }
    DeRef(_2489);
    _2489 = NOVALUE;
    DeRef(_2490);
    _2490 = NOVALUE;
    if (IS_ATOM_INT(_2491))
    _2492 = e_log(_2491);
    else
    _2492 = unary_op(LOG, _2491);
    DeRef(_2491);
    _2491 = NOVALUE;
    if (IS_ATOM_INT(_2492)) {
        if (_2492 & 1) {
            _2493 = NewDouble((_2492 >> 1) + 0.5);
        }
        else
        _2493 = _2492 >> 1;
    }
    else {
        _2493 = binary_op(DIVIDE, _2492, 2);
    }
    DeRef(_2492);
    _2492 = NOVALUE;
    DeRef(_a_4885);
    return _2493;
    ;
}


int _18sum(int _a_4893)
{
    int _b_4894 = NOVALUE;
    int _2501 = NOVALUE;
    int _2500 = NOVALUE;
    int _2498 = NOVALUE;
    int _2497 = NOVALUE;
    int _2496 = NOVALUE;
    int _2495 = NOVALUE;
    int _2494 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2494 = IS_ATOM(_a_4893);
    if (_2494 == 0)
    {
        _2494 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2494 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4894);
    return _a_4893;
L1: 

    /** 	b = 0*/
    DeRef(_b_4894);
    _b_4894 = 0;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4893)){
            _2495 = SEQ_PTR(_a_4893)->length;
    }
    else {
        _2495 = 1;
    }
    {
        int _i_4898;
        _i_4898 = 1;
L2: 
        if (_i_4898 > _2495){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4893);
        _2496 = (int)*(((s1_ptr)_2)->base + _i_4898);
        _2497 = IS_ATOM(_2496);
        _2496 = NOVALUE;
        if (_2497 == 0)
        {
            _2497 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2497 = NOVALUE;
        }

        /** 			b += a[i]*/
        _2 = (int)SEQ_PTR(_a_4893);
        _2498 = (int)*(((s1_ptr)_2)->base + _i_4898);
        _0 = _b_4894;
        if (IS_ATOM_INT(_b_4894) && IS_ATOM_INT(_2498)) {
            _b_4894 = _b_4894 + _2498;
            if ((long)((unsigned long)_b_4894 + (unsigned long)HIGH_BITS) >= 0) 
            _b_4894 = NewDouble((double)_b_4894);
        }
        else {
            _b_4894 = binary_op(PLUS, _b_4894, _2498);
        }
        DeRef(_0);
        _2498 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b += sum(a[i])*/
        _2 = (int)SEQ_PTR(_a_4893);
        _2500 = (int)*(((s1_ptr)_2)->base + _i_4898);
        Ref(_2500);
        _2501 = _18sum(_2500);
        _2500 = NOVALUE;
        _0 = _b_4894;
        if (IS_ATOM_INT(_b_4894) && IS_ATOM_INT(_2501)) {
            _b_4894 = _b_4894 + _2501;
            if ((long)((unsigned long)_b_4894 + (unsigned long)HIGH_BITS) >= 0) 
            _b_4894 = NewDouble((double)_b_4894);
        }
        else {
            _b_4894 = binary_op(PLUS, _b_4894, _2501);
        }
        DeRef(_0);
        DeRef(_2501);
        _2501 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4898 = _i_4898 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4893);
    return _b_4894;
    ;
}


int _18product(int _a_4911)
{
    int _b_4912 = NOVALUE;
    int _2510 = NOVALUE;
    int _2509 = NOVALUE;
    int _2507 = NOVALUE;
    int _2506 = NOVALUE;
    int _2505 = NOVALUE;
    int _2504 = NOVALUE;
    int _2503 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2503 = IS_ATOM(_a_4911);
    if (_2503 == 0)
    {
        _2503 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2503 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4912);
    return _a_4911;
L1: 

    /** 	b = 1*/
    DeRef(_b_4912);
    _b_4912 = 1;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4911)){
            _2504 = SEQ_PTR(_a_4911)->length;
    }
    else {
        _2504 = 1;
    }
    {
        int _i_4916;
        _i_4916 = 1;
L2: 
        if (_i_4916 > _2504){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4911);
        _2505 = (int)*(((s1_ptr)_2)->base + _i_4916);
        _2506 = IS_ATOM(_2505);
        _2505 = NOVALUE;
        if (_2506 == 0)
        {
            _2506 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2506 = NOVALUE;
        }

        /** 			b *= a[i]*/
        _2 = (int)SEQ_PTR(_a_4911);
        _2507 = (int)*(((s1_ptr)_2)->base + _i_4916);
        _0 = _b_4912;
        if (IS_ATOM_INT(_b_4912) && IS_ATOM_INT(_2507)) {
            if (_b_4912 == (short)_b_4912 && _2507 <= INT15 && _2507 >= -INT15)
            _b_4912 = _b_4912 * _2507;
            else
            _b_4912 = NewDouble(_b_4912 * (double)_2507);
        }
        else {
            _b_4912 = binary_op(MULTIPLY, _b_4912, _2507);
        }
        DeRef(_0);
        _2507 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b *= product(a[i])*/
        _2 = (int)SEQ_PTR(_a_4911);
        _2509 = (int)*(((s1_ptr)_2)->base + _i_4916);
        Ref(_2509);
        _2510 = _18product(_2509);
        _2509 = NOVALUE;
        _0 = _b_4912;
        if (IS_ATOM_INT(_b_4912) && IS_ATOM_INT(_2510)) {
            if (_b_4912 == (short)_b_4912 && _2510 <= INT15 && _2510 >= -INT15)
            _b_4912 = _b_4912 * _2510;
            else
            _b_4912 = NewDouble(_b_4912 * (double)_2510);
        }
        else {
            _b_4912 = binary_op(MULTIPLY, _b_4912, _2510);
        }
        DeRef(_0);
        DeRef(_2510);
        _2510 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4916 = _i_4916 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4911);
    return _b_4912;
    ;
}


int _18or_all(int _a_4929)
{
    int _b_4930 = NOVALUE;
    int _2519 = NOVALUE;
    int _2518 = NOVALUE;
    int _2516 = NOVALUE;
    int _2515 = NOVALUE;
    int _2514 = NOVALUE;
    int _2513 = NOVALUE;
    int _2512 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _2512 = IS_ATOM(_a_4929);
    if (_2512 == 0)
    {
        _2512 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _2512 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_4930);
    return _a_4929;
L1: 

    /** 	b = 0*/
    DeRef(_b_4930);
    _b_4930 = 0;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4929)){
            _2513 = SEQ_PTR(_a_4929)->length;
    }
    else {
        _2513 = 1;
    }
    {
        int _i_4934;
        _i_4934 = 1;
L2: 
        if (_i_4934 > _2513){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_4929);
        _2514 = (int)*(((s1_ptr)_2)->base + _i_4934);
        _2515 = IS_ATOM(_2514);
        _2514 = NOVALUE;
        if (_2515 == 0)
        {
            _2515 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _2515 = NOVALUE;
        }

        /** 			b = or_bits(b, a[i])*/
        _2 = (int)SEQ_PTR(_a_4929);
        _2516 = (int)*(((s1_ptr)_2)->base + _i_4934);
        _0 = _b_4930;
        if (IS_ATOM_INT(_b_4930) && IS_ATOM_INT(_2516)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4930 | (unsigned long)_2516;
                 _b_4930 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4930 = binary_op(OR_BITS, _b_4930, _2516);
        }
        DeRef(_0);
        _2516 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b = or_bits(b, or_all(a[i]))*/
        _2 = (int)SEQ_PTR(_a_4929);
        _2518 = (int)*(((s1_ptr)_2)->base + _i_4934);
        Ref(_2518);
        _2519 = _18or_all(_2518);
        _2518 = NOVALUE;
        _0 = _b_4930;
        if (IS_ATOM_INT(_b_4930) && IS_ATOM_INT(_2519)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_4930 | (unsigned long)_2519;
                 _b_4930 = MAKE_UINT(tu);
            }
        }
        else {
            _b_4930 = binary_op(OR_BITS, _b_4930, _2519);
        }
        DeRef(_0);
        DeRef(_2519);
        _2519 = NOVALUE;
L5: 

        /** 	end for*/
        _i_4934 = _i_4934 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_4929);
    return _b_4930;
    ;
}


int _18shift_bits(int _source_number_4947, int _shift_distance_4948)
{
    int _lSigned_4966 = NOVALUE;
    int _2543 = NOVALUE;
    int _2541 = NOVALUE;
    int _2540 = NOVALUE;
    int _2539 = NOVALUE;
    int _2538 = NOVALUE;
    int _2536 = NOVALUE;
    int _2533 = NOVALUE;
    int _2530 = NOVALUE;
    int _2529 = NOVALUE;
    int _2525 = NOVALUE;
    int _2524 = NOVALUE;
    int _2523 = NOVALUE;
    int _2522 = NOVALUE;
    int _2521 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_shift_distance_4948)) {
        _1 = (long)(DBL_PTR(_shift_distance_4948)->dbl);
        if (UNIQUE(DBL_PTR(_shift_distance_4948)) && (DBL_PTR(_shift_distance_4948)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_shift_distance_4948);
        _shift_distance_4948 = _1;
    }

    /** 	if sequence(source_number) then*/
    _2521 = IS_SEQUENCE(_source_number_4947);
    if (_2521 == 0)
    {
        _2521 = NOVALUE;
        goto L1; // [10] 57
    }
    else{
        _2521 = NOVALUE;
    }

    /** 		for i = 1 to length(source_number) do*/
    if (IS_SEQUENCE(_source_number_4947)){
            _2522 = SEQ_PTR(_source_number_4947)->length;
    }
    else {
        _2522 = 1;
    }
    {
        int _i_4952;
        _i_4952 = 1;
L2: 
        if (_i_4952 > _2522){
            goto L3; // [18] 50
        }

        /** 			source_number[i] = shift_bits(source_number[i], shift_distance)*/
        _2 = (int)SEQ_PTR(_source_number_4947);
        _2523 = (int)*(((s1_ptr)_2)->base + _i_4952);
        DeRef(_2524);
        _2524 = _shift_distance_4948;
        Ref(_2523);
        _2525 = _18shift_bits(_2523, _2524);
        _2523 = NOVALUE;
        _2524 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_number_4947);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_number_4947 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4952);
        _1 = *(int *)_2;
        *(int *)_2 = _2525;
        if( _1 != _2525 ){
            DeRef(_1);
        }
        _2525 = NOVALUE;

        /** 		end for*/
        _i_4952 = _i_4952 + 1;
        goto L2; // [45] 25
L3: 
        ;
    }

    /** 		return source_number*/
    return _source_number_4947;
L1: 

    /** 	source_number = and_bits(source_number, 0xFFFFFFFF)*/
    _0 = _source_number_4947;
    _source_number_4947 = binary_op(AND_BITS, _source_number_4947, _2226);
    DeRef(_0);

    /** 	if shift_distance = 0 then*/
    if (_shift_distance_4948 != 0)
    goto L4; // [65] 76

    /** 		return source_number*/
    return _source_number_4947;
L4: 

    /** 	if shift_distance < 0 then*/
    if (_shift_distance_4948 >= 0)
    goto L5; // [78] 98

    /** 		source_number *= power(2, -shift_distance)*/
    if ((unsigned long)_shift_distance_4948 == 0xC0000000)
    _2529 = (int)NewDouble((double)-0xC0000000);
    else
    _2529 = - _shift_distance_4948;
    if (IS_ATOM_INT(_2529)) {
        _2530 = power(2, _2529);
    }
    else {
        temp_d.dbl = (double)2;
        _2530 = Dpower(&temp_d, DBL_PTR(_2529));
    }
    DeRef(_2529);
    _2529 = NOVALUE;
    _0 = _source_number_4947;
    if (IS_ATOM_INT(_source_number_4947) && IS_ATOM_INT(_2530)) {
        if (_source_number_4947 == (short)_source_number_4947 && _2530 <= INT15 && _2530 >= -INT15)
        _source_number_4947 = _source_number_4947 * _2530;
        else
        _source_number_4947 = NewDouble(_source_number_4947 * (double)_2530);
    }
    else {
        _source_number_4947 = binary_op(MULTIPLY, _source_number_4947, _2530);
    }
    DeRef(_0);
    DeRef(_2530);
    _2530 = NOVALUE;
    goto L6; // [95] 170
L5: 

    /** 		integer lSigned = 0*/
    _lSigned_4966 = 0;

    /** 		if and_bits(source_number, 0x80000000) then*/
    _2533 = binary_op(AND_BITS, _source_number_4947, _2532);
    if (_2533 == 0) {
        DeRef(_2533);
        _2533 = NOVALUE;
        goto L7; // [111] 128
    }
    else {
        if (!IS_ATOM_INT(_2533) && DBL_PTR(_2533)->dbl == 0.0){
            DeRef(_2533);
            _2533 = NOVALUE;
            goto L7; // [111] 128
        }
        DeRef(_2533);
        _2533 = NOVALUE;
    }
    DeRef(_2533);
    _2533 = NOVALUE;

    /** 			lSigned = 1*/
    _lSigned_4966 = 1;

    /** 			source_number = and_bits(source_number, 0x7FFFFFFF)*/
    _0 = _source_number_4947;
    _source_number_4947 = binary_op(AND_BITS, _source_number_4947, _2534);
    DeRef(_0);
L7: 

    /** 		source_number /= power(2, shift_distance)*/
    _2536 = power(2, _shift_distance_4948);
    _0 = _source_number_4947;
    if (IS_ATOM_INT(_source_number_4947) && IS_ATOM_INT(_2536)) {
        _source_number_4947 = (_source_number_4947 % _2536) ? NewDouble((double)_source_number_4947 / _2536) : (_source_number_4947 / _2536);
    }
    else {
        _source_number_4947 = binary_op(DIVIDE, _source_number_4947, _2536);
    }
    DeRef(_0);
    DeRef(_2536);
    _2536 = NOVALUE;

    /** 		if lSigned and shift_distance < 32 then*/
    if (_lSigned_4966 == 0) {
        goto L8; // [140] 167
    }
    _2539 = (_shift_distance_4948 < 32);
    if (_2539 == 0)
    {
        DeRef(_2539);
        _2539 = NOVALUE;
        goto L8; // [149] 167
    }
    else{
        DeRef(_2539);
        _2539 = NOVALUE;
    }

    /** 			source_number = or_bits(source_number, power(2, 31-shift_distance))*/
    _2540 = 31 - _shift_distance_4948;
    if ((long)((unsigned long)_2540 +(unsigned long) HIGH_BITS) >= 0){
        _2540 = NewDouble((double)_2540);
    }
    if (IS_ATOM_INT(_2540)) {
        _2541 = power(2, _2540);
    }
    else {
        temp_d.dbl = (double)2;
        _2541 = Dpower(&temp_d, DBL_PTR(_2540));
    }
    DeRef(_2540);
    _2540 = NOVALUE;
    _0 = _source_number_4947;
    if (IS_ATOM_INT(_source_number_4947) && IS_ATOM_INT(_2541)) {
        {unsigned long tu;
             tu = (unsigned long)_source_number_4947 | (unsigned long)_2541;
             _source_number_4947 = MAKE_UINT(tu);
        }
    }
    else {
        _source_number_4947 = binary_op(OR_BITS, _source_number_4947, _2541);
    }
    DeRef(_0);
    DeRef(_2541);
    _2541 = NOVALUE;
L8: 
L6: 

    /** 	return and_bits(source_number, 0xFFFFFFFF)*/
    _2543 = binary_op(AND_BITS, _source_number_4947, _2226);
    DeRef(_source_number_4947);
    return _2543;
    ;
}


int _18rotate_bits(int _source_number_4983, int _shift_distance_4984)
{
    int _lTemp_4985 = NOVALUE;
    int _lSave_4986 = NOVALUE;
    int _lRest_4987 = NOVALUE;
    int _2563 = NOVALUE;
    int _2560 = NOVALUE;
    int _2557 = NOVALUE;
    int _2554 = NOVALUE;
    int _2553 = NOVALUE;
    int _2552 = NOVALUE;
    int _2548 = NOVALUE;
    int _2547 = NOVALUE;
    int _2546 = NOVALUE;
    int _2545 = NOVALUE;
    int _2544 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_shift_distance_4984)) {
        _1 = (long)(DBL_PTR(_shift_distance_4984)->dbl);
        if (UNIQUE(DBL_PTR(_shift_distance_4984)) && (DBL_PTR(_shift_distance_4984)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_shift_distance_4984);
        _shift_distance_4984 = _1;
    }

    /** 	if sequence(source_number) then*/
    _2544 = IS_SEQUENCE(_source_number_4983);
    if (_2544 == 0)
    {
        _2544 = NOVALUE;
        goto L1; // [10] 57
    }
    else{
        _2544 = NOVALUE;
    }

    /** 		for i = 1 to length(source_number) do*/
    if (IS_SEQUENCE(_source_number_4983)){
            _2545 = SEQ_PTR(_source_number_4983)->length;
    }
    else {
        _2545 = 1;
    }
    {
        int _i_4991;
        _i_4991 = 1;
L2: 
        if (_i_4991 > _2545){
            goto L3; // [18] 50
        }

        /** 			source_number[i] = rotate_bits(source_number[i], shift_distance)*/
        _2 = (int)SEQ_PTR(_source_number_4983);
        _2546 = (int)*(((s1_ptr)_2)->base + _i_4991);
        DeRef(_2547);
        _2547 = _shift_distance_4984;
        Ref(_2546);
        _2548 = _18rotate_bits(_2546, _2547);
        _2546 = NOVALUE;
        _2547 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_number_4983);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_number_4983 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4991);
        _1 = *(int *)_2;
        *(int *)_2 = _2548;
        if( _1 != _2548 ){
            DeRef(_1);
        }
        _2548 = NOVALUE;

        /** 		end for*/
        _i_4991 = _i_4991 + 1;
        goto L2; // [45] 25
L3: 
        ;
    }

    /** 		return source_number*/
    DeRef(_lTemp_4985);
    DeRef(_lSave_4986);
    return _source_number_4983;
L1: 

    /** 	source_number = and_bits(source_number, 0xFFFFFFFF)*/
    _0 = _source_number_4983;
    _source_number_4983 = binary_op(AND_BITS, _source_number_4983, _2226);
    DeRef(_0);

    /** 	if shift_distance = 0 then*/
    if (_shift_distance_4984 != 0)
    goto L4; // [65] 76

    /** 		return source_number*/
    DeRef(_lTemp_4985);
    DeRef(_lSave_4986);
    return _source_number_4983;
L4: 

    /** 	if shift_distance < 0 then*/
    if (_shift_distance_4984 >= 0)
    goto L5; // [78] 110

    /** 		lSave = not_bits(power(2, 32 + shift_distance) - 1) 	*/
    _2552 = 32 + _shift_distance_4984;
    if ((long)((unsigned long)_2552 + (unsigned long)HIGH_BITS) >= 0) 
    _2552 = NewDouble((double)_2552);
    if (IS_ATOM_INT(_2552)) {
        _2553 = power(2, _2552);
    }
    else {
        temp_d.dbl = (double)2;
        _2553 = Dpower(&temp_d, DBL_PTR(_2552));
    }
    DeRef(_2552);
    _2552 = NOVALUE;
    if (IS_ATOM_INT(_2553)) {
        _2554 = _2553 - 1;
        if ((long)((unsigned long)_2554 +(unsigned long) HIGH_BITS) >= 0){
            _2554 = NewDouble((double)_2554);
        }
    }
    else {
        _2554 = NewDouble(DBL_PTR(_2553)->dbl - (double)1);
    }
    DeRef(_2553);
    _2553 = NOVALUE;
    DeRef(_lSave_4986);
    if (IS_ATOM_INT(_2554))
    _lSave_4986 = not_bits(_2554);
    else
    _lSave_4986 = unary_op(NOT_BITS, _2554);
    DeRef(_2554);
    _2554 = NOVALUE;

    /** 		lRest = 32 + shift_distance*/
    _lRest_4987 = 32 + _shift_distance_4984;
    goto L6; // [107] 129
L5: 

    /** 		lSave = power(2, shift_distance) - 1*/
    _2557 = power(2, _shift_distance_4984);
    DeRef(_lSave_4986);
    if (IS_ATOM_INT(_2557)) {
        _lSave_4986 = _2557 - 1;
        if ((long)((unsigned long)_lSave_4986 +(unsigned long) HIGH_BITS) >= 0){
            _lSave_4986 = NewDouble((double)_lSave_4986);
        }
    }
    else {
        _lSave_4986 = NewDouble(DBL_PTR(_2557)->dbl - (double)1);
    }
    DeRef(_2557);
    _2557 = NOVALUE;

    /** 		lRest = shift_distance - 32*/
    _lRest_4987 = _shift_distance_4984 - 32;
L6: 

    /** 	lTemp = shift_bits(and_bits(source_number, lSave), lRest)*/
    if (IS_ATOM_INT(_source_number_4983) && IS_ATOM_INT(_lSave_4986)) {
        {unsigned long tu;
             tu = (unsigned long)_source_number_4983 & (unsigned long)_lSave_4986;
             _2560 = MAKE_UINT(tu);
        }
    }
    else {
        _2560 = binary_op(AND_BITS, _source_number_4983, _lSave_4986);
    }
    _0 = _lTemp_4985;
    _lTemp_4985 = _18shift_bits(_2560, _lRest_4987);
    DeRef(_0);
    _2560 = NOVALUE;

    /** 	source_number = shift_bits(source_number, shift_distance)*/
    Ref(_source_number_4983);
    _0 = _source_number_4983;
    _source_number_4983 = _18shift_bits(_source_number_4983, _shift_distance_4984);
    DeRef(_0);

    /** 	return or_bits(source_number, lTemp)*/
    if (IS_ATOM_INT(_source_number_4983) && IS_ATOM_INT(_lTemp_4985)) {
        {unsigned long tu;
             tu = (unsigned long)_source_number_4983 | (unsigned long)_lTemp_4985;
             _2563 = MAKE_UINT(tu);
        }
    }
    else {
        _2563 = binary_op(OR_BITS, _source_number_4983, _lTemp_4985);
    }
    DeRef(_source_number_4983);
    DeRef(_lTemp_4985);
    DeRef(_lSave_4986);
    return _2563;
    ;
}


int _18gcd(int _p_5016, int _q_5017)
{
    int _r_5018 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if p < 0 then*/
    if (binary_op_a(GREATEREQ, _p_5016, 0)){
        goto L1; // [3] 13
    }

    /** 		p = -p*/
    _0 = _p_5016;
    if (IS_ATOM_INT(_p_5016)) {
        if ((unsigned long)_p_5016 == 0xC0000000)
        _p_5016 = (int)NewDouble((double)-0xC0000000);
        else
        _p_5016 = - _p_5016;
    }
    else {
        _p_5016 = unary_op(UMINUS, _p_5016);
    }
    DeRef(_0);
L1: 

    /** 	if q < 0 then*/
    if (binary_op_a(GREATEREQ, _q_5017, 0)){
        goto L2; // [15] 25
    }

    /** 		q = -q*/
    _0 = _q_5017;
    if (IS_ATOM_INT(_q_5017)) {
        if ((unsigned long)_q_5017 == 0xC0000000)
        _q_5017 = (int)NewDouble((double)-0xC0000000);
        else
        _q_5017 = - _q_5017;
    }
    else {
        _q_5017 = unary_op(UMINUS, _q_5017);
    }
    DeRef(_0);
L2: 

    /** 	p = floor(p)*/
    _0 = _p_5016;
    if (IS_ATOM_INT(_p_5016))
    _p_5016 = e_floor(_p_5016);
    else
    _p_5016 = unary_op(FLOOR, _p_5016);
    DeRef(_0);

    /** 	q = floor(q)*/
    _0 = _q_5017;
    if (IS_ATOM_INT(_q_5017))
    _q_5017 = e_floor(_q_5017);
    else
    _q_5017 = unary_op(FLOOR, _q_5017);
    DeRef(_0);

    /** 	if p < q then*/
    if (binary_op_a(GREATEREQ, _p_5016, _q_5017)){
        goto L3; // [37] 57
    }

    /** 		r = p*/
    Ref(_p_5016);
    DeRef(_r_5018);
    _r_5018 = _p_5016;

    /** 		p = q*/
    Ref(_q_5017);
    DeRef(_p_5016);
    _p_5016 = _q_5017;

    /** 		q = r*/
    Ref(_r_5018);
    DeRef(_q_5017);
    _q_5017 = _r_5018;
L3: 

    /** 	if q = 0 then*/
    if (binary_op_a(NOTEQ, _q_5017, 0)){
        goto L4; // [59] 94
    }

    /** 		return p*/
    DeRef(_q_5017);
    DeRef(_r_5018);
    return _p_5016;

    /**     while r > 1 with entry do*/
    goto L4; // [72] 94
L5: 
    if (binary_op_a(LESSEQ, _r_5018, 1)){
        goto L6; // [77] 105
    }

    /** 		p = q*/
    Ref(_q_5017);
    DeRef(_p_5016);
    _p_5016 = _q_5017;

    /** 		q = r*/
    Ref(_r_5018);
    DeRef(_q_5017);
    _q_5017 = _r_5018;

    /** 	entry*/
L4: 

    /** 		r = remainder(p, q)*/
    DeRef(_r_5018);
    if (IS_ATOM_INT(_p_5016) && IS_ATOM_INT(_q_5017)) {
        _r_5018 = (_p_5016 % _q_5017);
    }
    else {
        if (IS_ATOM_INT(_p_5016)) {
            temp_d.dbl = (double)_p_5016;
            _r_5018 = Dremainder(&temp_d, DBL_PTR(_q_5017));
        }
        else {
            if (IS_ATOM_INT(_q_5017)) {
                temp_d.dbl = (double)_q_5017;
                _r_5018 = Dremainder(DBL_PTR(_p_5016), &temp_d);
            }
            else
            _r_5018 = Dremainder(DBL_PTR(_p_5016), DBL_PTR(_q_5017));
        }
    }

    /**     end while*/
    goto L5; // [102] 75
L6: 

    /** 	if r = 1 then*/
    if (binary_op_a(NOTEQ, _r_5018, 1)){
        goto L7; // [109] 122
    }

    /** 		return 1*/
    DeRef(_p_5016);
    DeRef(_q_5017);
    DeRef(_r_5018);
    return 1;
    goto L8; // [119] 129
L7: 

    /** 		return q*/
    DeRef(_p_5016);
    DeRef(_r_5018);
    return _q_5017;
L8: 
    ;
}


int _18approx(int _p_5039, int _q_5040, int _epsilon_5041)
{
    int _msg_inlined_crash_at_30_5053 = NOVALUE;
    int _2597 = NOVALUE;
    int _2595 = NOVALUE;
    int _2594 = NOVALUE;
    int _2593 = NOVALUE;
    int _2592 = NOVALUE;
    int _2591 = NOVALUE;
    int _2590 = NOVALUE;
    int _2589 = NOVALUE;
    int _2588 = NOVALUE;
    int _2587 = NOVALUE;
    int _2586 = NOVALUE;
    int _2585 = NOVALUE;
    int _2584 = NOVALUE;
    int _2583 = NOVALUE;
    int _2582 = NOVALUE;
    int _2579 = NOVALUE;
    int _2578 = NOVALUE;
    int _2577 = NOVALUE;
    int _2576 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(p) then*/
    _2576 = IS_SEQUENCE(_p_5039);
    if (_2576 == 0)
    {
        _2576 = NOVALUE;
        goto L1; // [6] 146
    }
    else{
        _2576 = NOVALUE;
    }

    /** 		if sequence(q) then*/
    _2577 = IS_SEQUENCE(_q_5040);
    if (_2577 == 0)
    {
        _2577 = NOVALUE;
        goto L2; // [14] 98
    }
    else{
        _2577 = NOVALUE;
    }

    /** 			if length(p) != length(q) then*/
    if (IS_SEQUENCE(_p_5039)){
            _2578 = SEQ_PTR(_p_5039)->length;
    }
    else {
        _2578 = 1;
    }
    if (IS_SEQUENCE(_q_5040)){
            _2579 = SEQ_PTR(_q_5040)->length;
    }
    else {
        _2579 = 1;
    }
    if (_2578 == _2579)
    goto L3; // [25] 50

    /** 				error:crash("approx(): Sequence arguments must be the same length")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_30_5053);
    _msg_inlined_crash_at_30_5053 = EPrintf(-9999999, _2581, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_30_5053);

    /** end procedure*/
    goto L4; // [44] 47
L4: 
    DeRefi(_msg_inlined_crash_at_30_5053);
    _msg_inlined_crash_at_30_5053 = NOVALUE;
L3: 

    /** 			for i = 1 to length(p) do*/
    if (IS_SEQUENCE(_p_5039)){
            _2582 = SEQ_PTR(_p_5039)->length;
    }
    else {
        _2582 = 1;
    }
    {
        int _i_5055;
        _i_5055 = 1;
L5: 
        if (_i_5055 > _2582){
            goto L6; // [55] 89
        }

        /** 				p[i] = approx(p[i], q[i])*/
        _2 = (int)SEQ_PTR(_p_5039);
        _2583 = (int)*(((s1_ptr)_2)->base + _i_5055);
        _2 = (int)SEQ_PTR(_q_5040);
        _2584 = (int)*(((s1_ptr)_2)->base + _i_5055);
        Ref(_2583);
        Ref(_2584);
        RefDS(_2575);
        _2585 = _18approx(_2583, _2584, _2575);
        _2583 = NOVALUE;
        _2584 = NOVALUE;
        _2 = (int)SEQ_PTR(_p_5039);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _p_5039 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5055);
        _1 = *(int *)_2;
        *(int *)_2 = _2585;
        if( _1 != _2585 ){
            DeRef(_1);
        }
        _2585 = NOVALUE;

        /** 			end for*/
        _i_5055 = _i_5055 + 1;
        goto L5; // [84] 62
L6: 
        ;
    }

    /** 			return p*/
    DeRef(_q_5040);
    DeRef(_epsilon_5041);
    return _p_5039;
    goto L7; // [95] 242
L2: 

    /** 			for i = 1 to length(p) do*/
    if (IS_SEQUENCE(_p_5039)){
            _2586 = SEQ_PTR(_p_5039)->length;
    }
    else {
        _2586 = 1;
    }
    {
        int _i_5062;
        _i_5062 = 1;
L8: 
        if (_i_5062 > _2586){
            goto L9; // [103] 136
        }

        /** 				p[i] = approx(p[i], q)*/
        _2 = (int)SEQ_PTR(_p_5039);
        _2587 = (int)*(((s1_ptr)_2)->base + _i_5062);
        Ref(_q_5040);
        DeRef(_2588);
        _2588 = _q_5040;
        Ref(_2587);
        RefDS(_2575);
        _2589 = _18approx(_2587, _2588, _2575);
        _2587 = NOVALUE;
        _2588 = NOVALUE;
        _2 = (int)SEQ_PTR(_p_5039);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _p_5039 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5062);
        _1 = *(int *)_2;
        *(int *)_2 = _2589;
        if( _1 != _2589 ){
            DeRef(_1);
        }
        _2589 = NOVALUE;

        /** 			end for*/
        _i_5062 = _i_5062 + 1;
        goto L8; // [131] 110
L9: 
        ;
    }

    /** 			return p*/
    DeRef(_q_5040);
    DeRef(_epsilon_5041);
    return _p_5039;
    goto L7; // [143] 242
L1: 

    /** 	elsif sequence(q) then*/
    _2590 = IS_SEQUENCE(_q_5040);
    if (_2590 == 0)
    {
        _2590 = NOVALUE;
        goto LA; // [151] 201
    }
    else{
        _2590 = NOVALUE;
    }

    /** 			for i = 1 to length(q) do*/
    if (IS_SEQUENCE(_q_5040)){
            _2591 = SEQ_PTR(_q_5040)->length;
    }
    else {
        _2591 = 1;
    }
    {
        int _i_5070;
        _i_5070 = 1;
LB: 
        if (_i_5070 > _2591){
            goto LC; // [159] 192
        }

        /** 				q[i] = approx(p, q[i])*/
        _2 = (int)SEQ_PTR(_q_5040);
        _2592 = (int)*(((s1_ptr)_2)->base + _i_5070);
        Ref(_p_5039);
        DeRef(_2593);
        _2593 = _p_5039;
        Ref(_2592);
        RefDS(_2575);
        _2594 = _18approx(_2593, _2592, _2575);
        _2593 = NOVALUE;
        _2592 = NOVALUE;
        _2 = (int)SEQ_PTR(_q_5040);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _q_5040 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5070);
        _1 = *(int *)_2;
        *(int *)_2 = _2594;
        if( _1 != _2594 ){
            DeRef(_1);
        }
        _2594 = NOVALUE;

        /** 			end for*/
        _i_5070 = _i_5070 + 1;
        goto LB; // [187] 166
LC: 
        ;
    }

    /** 			return q*/
    DeRef(_p_5039);
    DeRef(_epsilon_5041);
    return _q_5040;
    goto L7; // [198] 242
LA: 

    /** 		if p > (q + epsilon) then*/
    if (IS_ATOM_INT(_q_5040) && IS_ATOM_INT(_epsilon_5041)) {
        _2595 = _q_5040 + _epsilon_5041;
        if ((long)((unsigned long)_2595 + (unsigned long)HIGH_BITS) >= 0) 
        _2595 = NewDouble((double)_2595);
    }
    else {
        _2595 = binary_op(PLUS, _q_5040, _epsilon_5041);
    }
    if (binary_op_a(LESSEQ, _p_5039, _2595)){
        DeRef(_2595);
        _2595 = NOVALUE;
        goto LD; // [207] 218
    }
    DeRef(_2595);
    _2595 = NOVALUE;

    /** 			return 1*/
    DeRef(_p_5039);
    DeRef(_q_5040);
    DeRef(_epsilon_5041);
    return 1;
LD: 

    /** 		if p < (q - epsilon) then*/
    if (IS_ATOM_INT(_q_5040) && IS_ATOM_INT(_epsilon_5041)) {
        _2597 = _q_5040 - _epsilon_5041;
        if ((long)((unsigned long)_2597 +(unsigned long) HIGH_BITS) >= 0){
            _2597 = NewDouble((double)_2597);
        }
    }
    else {
        _2597 = binary_op(MINUS, _q_5040, _epsilon_5041);
    }
    if (binary_op_a(GREATEREQ, _p_5039, _2597)){
        DeRef(_2597);
        _2597 = NOVALUE;
        goto LE; // [224] 235
    }
    DeRef(_2597);
    _2597 = NOVALUE;

    /** 			return -1*/
    DeRef(_p_5039);
    DeRef(_q_5040);
    DeRef(_epsilon_5041);
    return -1;
LE: 

    /** 		return 0*/
    DeRef(_p_5039);
    DeRef(_q_5040);
    DeRef(_epsilon_5041);
    return 0;
L7: 
    ;
}


int _18powof2(int _p_5084)
{
    int _2601 = NOVALUE;
    int _2600 = NOVALUE;
    int _2599 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return not (and_bits(p, p-1))*/
    if (IS_ATOM_INT(_p_5084)) {
        _2599 = _p_5084 - 1;
        if ((long)((unsigned long)_2599 +(unsigned long) HIGH_BITS) >= 0){
            _2599 = NewDouble((double)_2599);
        }
    }
    else {
        _2599 = binary_op(MINUS, _p_5084, 1);
    }
    if (IS_ATOM_INT(_p_5084) && IS_ATOM_INT(_2599)) {
        {unsigned long tu;
             tu = (unsigned long)_p_5084 & (unsigned long)_2599;
             _2600 = MAKE_UINT(tu);
        }
    }
    else {
        _2600 = binary_op(AND_BITS, _p_5084, _2599);
    }
    DeRef(_2599);
    _2599 = NOVALUE;
    if (IS_ATOM_INT(_2600)) {
        _2601 = (_2600 == 0);
    }
    else {
        _2601 = unary_op(NOT, _2600);
    }
    DeRef(_2600);
    _2600 = NOVALUE;
    DeRef(_p_5084);
    return _2601;
    ;
}


int _18is_even(int _test_integer_5090)
{
    int _2603 = NOVALUE;
    int _2602 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_test_integer_5090)) {
        _1 = (long)(DBL_PTR(_test_integer_5090)->dbl);
        if (UNIQUE(DBL_PTR(_test_integer_5090)) && (DBL_PTR(_test_integer_5090)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_test_integer_5090);
        _test_integer_5090 = _1;
    }

    /** 	return (and_bits(test_integer, 1) = 0)*/
    {unsigned long tu;
         tu = (unsigned long)_test_integer_5090 & (unsigned long)1;
         _2602 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_2602)) {
        _2603 = (_2602 == 0);
    }
    else {
        _2603 = (DBL_PTR(_2602)->dbl == (double)0);
    }
    DeRef(_2602);
    _2602 = NOVALUE;
    return _2603;
    ;
}


int _18is_even_obj(int _test_object_5095)
{
    int _2610 = NOVALUE;
    int _2609 = NOVALUE;
    int _2608 = NOVALUE;
    int _2607 = NOVALUE;
    int _2606 = NOVALUE;
    int _2605 = NOVALUE;
    int _2604 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(test_object) then*/
    _2604 = IS_ATOM(_test_object_5095);
    if (_2604 == 0)
    {
        _2604 = NOVALUE;
        goto L1; // [6] 39
    }
    else{
        _2604 = NOVALUE;
    }

    /** 		if integer(test_object) then*/
    if (IS_ATOM_INT(_test_object_5095))
    _2605 = 1;
    else if (IS_ATOM_DBL(_test_object_5095))
    _2605 = IS_ATOM_INT(DoubleToInt(_test_object_5095));
    else
    _2605 = 0;
    if (_2605 == 0)
    {
        _2605 = NOVALUE;
        goto L2; // [14] 32
    }
    else{
        _2605 = NOVALUE;
    }

    /** 			return (and_bits(test_object, 1) = 0)*/
    if (IS_ATOM_INT(_test_object_5095)) {
        {unsigned long tu;
             tu = (unsigned long)_test_object_5095 & (unsigned long)1;
             _2606 = MAKE_UINT(tu);
        }
    }
    else {
        _2606 = binary_op(AND_BITS, _test_object_5095, 1);
    }
    if (IS_ATOM_INT(_2606)) {
        _2607 = (_2606 == 0);
    }
    else {
        _2607 = binary_op(EQUALS, _2606, 0);
    }
    DeRef(_2606);
    _2606 = NOVALUE;
    DeRef(_test_object_5095);
    return _2607;
L2: 

    /** 		return 0*/
    DeRef(_test_object_5095);
    DeRef(_2607);
    _2607 = NOVALUE;
    return 0;
L1: 

    /** 	for i = 1 to length(test_object) do*/
    if (IS_SEQUENCE(_test_object_5095)){
            _2608 = SEQ_PTR(_test_object_5095)->length;
    }
    else {
        _2608 = 1;
    }
    {
        int _i_5103;
        _i_5103 = 1;
L3: 
        if (_i_5103 > _2608){
            goto L4; // [44] 72
        }

        /** 		test_object[i] = is_even_obj(test_object[i])*/
        _2 = (int)SEQ_PTR(_test_object_5095);
        _2609 = (int)*(((s1_ptr)_2)->base + _i_5103);
        Ref(_2609);
        _2610 = _18is_even_obj(_2609);
        _2609 = NOVALUE;
        _2 = (int)SEQ_PTR(_test_object_5095);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _test_object_5095 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5103);
        _1 = *(int *)_2;
        *(int *)_2 = _2610;
        if( _1 != _2610 ){
            DeRef(_1);
        }
        _2610 = NOVALUE;

        /** 	end for*/
        _i_5103 = _i_5103 + 1;
        goto L3; // [67] 51
L4: 
        ;
    }

    /** 	return test_object*/
    DeRef(_2607);
    _2607 = NOVALUE;
    return _test_object_5095;
    ;
}



// 0xBFA53871
