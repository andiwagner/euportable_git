// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _15get_bytes(int _fn_7230, int _n_7231)
{
    int _s_7232 = NOVALUE;
    int _c_7233 = NOVALUE;
    int _first_7234 = NOVALUE;
    int _last_7235 = NOVALUE;
    int _4002 = NOVALUE;
    int _3999 = NOVALUE;
    int _3997 = NOVALUE;
    int _3996 = NOVALUE;
    int _3995 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fn_7230)) {
        _1 = (long)(DBL_PTR(_fn_7230)->dbl);
        if (UNIQUE(DBL_PTR(_fn_7230)) && (DBL_PTR(_fn_7230)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_7230);
        _fn_7230 = _1;
    }
    if (!IS_ATOM_INT(_n_7231)) {
        _1 = (long)(DBL_PTR(_n_7231)->dbl);
        if (UNIQUE(DBL_PTR(_n_7231)) && (DBL_PTR(_n_7231)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_7231);
        _n_7231 = _1;
    }

    /** 	if n = 0 then*/
    if (_n_7231 != 0)
    goto L1; // [11] 22

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_7232);
    return _5;
L1: 

    /** 	c = getc(fn)*/
    if (_fn_7230 != last_r_file_no) {
        last_r_file_ptr = which_file(_fn_7230, EF_READ);
        last_r_file_no = _fn_7230;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_7233 = getKBchar();
        }
        else
        _c_7233 = getc(last_r_file_ptr);
    }
    else
    _c_7233 = getc(last_r_file_ptr);

    /** 	if c = EOF then*/
    if (_c_7233 != -1)
    goto L2; // [31] 42

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_7232);
    return _5;
L2: 

    /** 	s = repeat(c, n)*/
    DeRefi(_s_7232);
    _s_7232 = Repeat(_c_7233, _n_7231);

    /** 	last = 1*/
    _last_7235 = 1;

    /** 	while last < n do*/
L3: 
    if (_last_7235 >= _n_7231)
    goto L4; // [60] 175

    /** 		first = last+1*/
    _first_7234 = _last_7235 + 1;

    /** 		last  = last+CHUNK*/
    _last_7235 = _last_7235 + 100;

    /** 		if last > n then*/
    if (_last_7235 <= _n_7231)
    goto L5; // [82] 94

    /** 			last = n*/
    _last_7235 = _n_7231;
L5: 

    /** 		for i = first to last do*/
    _3995 = _last_7235;
    {
        int _i_7249;
        _i_7249 = _first_7234;
L6: 
        if (_i_7249 > _3995){
            goto L7; // [99] 122
        }

        /** 			s[i] = getc(fn)*/
        if (_fn_7230 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_7230, EF_READ);
            last_r_file_no = _fn_7230;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _3996 = getKBchar();
            }
            else
            _3996 = getc(last_r_file_ptr);
        }
        else
        _3996 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_s_7232);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_7232 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_7249);
        *(int *)_2 = _3996;
        if( _1 != _3996 ){
        }
        _3996 = NOVALUE;

        /** 		end for*/
        _i_7249 = _i_7249 + 1;
        goto L6; // [117] 106
L7: 
        ;
    }

    /** 		if s[last] = EOF then*/
    _2 = (int)SEQ_PTR(_s_7232);
    _3997 = (int)*(((s1_ptr)_2)->base + _last_7235);
    if (_3997 != -1)
    goto L3; // [128] 60

    /** 			while s[last] = EOF do*/
L8: 
    _2 = (int)SEQ_PTR(_s_7232);
    _3999 = (int)*(((s1_ptr)_2)->base + _last_7235);
    if (_3999 != -1)
    goto L9; // [141] 158

    /** 				last -= 1*/
    _last_7235 = _last_7235 - 1;

    /** 			end while*/
    goto L8; // [155] 137
L9: 

    /** 			return s[1..last]*/
    rhs_slice_target = (object_ptr)&_4002;
    RHS_Slice(_s_7232, 1, _last_7235);
    DeRefDSi(_s_7232);
    _3997 = NOVALUE;
    _3999 = NOVALUE;
    return _4002;

    /** 	end while*/
    goto L3; // [172] 60
L4: 

    /** 	return s*/
    _3997 = NOVALUE;
    _3999 = NOVALUE;
    DeRef(_4002);
    _4002 = NOVALUE;
    return _s_7232;
    ;
}


int _15get_integer32(int _fh_7270)
{
    int _4011 = NOVALUE;
    int _4010 = NOVALUE;
    int _4009 = NOVALUE;
    int _4008 = NOVALUE;
    int _4007 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_7270)) {
        _1 = (long)(DBL_PTR(_fh_7270)->dbl);
        if (UNIQUE(DBL_PTR(_fh_7270)) && (DBL_PTR(_fh_7270)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_7270);
        _fh_7270 = _1;
    }

    /** 	poke(mem0, getc(fh))*/
    if (_fh_7270 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7270, EF_READ);
        last_r_file_no = _fh_7270;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4007 = getKBchar();
        }
        else
        _4007 = getc(last_r_file_ptr);
    }
    else
    _4007 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_15mem0_7260)){
        poke_addr = (unsigned char *)_15mem0_7260;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_15mem0_7260)->dbl);
    }
    *poke_addr = (unsigned char)_4007;
    _4007 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_7270 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7270, EF_READ);
        last_r_file_no = _fh_7270;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4008 = getKBchar();
        }
        else
        _4008 = getc(last_r_file_ptr);
    }
    else
    _4008 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_15mem1_7261)){
        poke_addr = (unsigned char *)_15mem1_7261;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_15mem1_7261)->dbl);
    }
    *poke_addr = (unsigned char)_4008;
    _4008 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_7270 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7270, EF_READ);
        last_r_file_no = _fh_7270;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4009 = getKBchar();
        }
        else
        _4009 = getc(last_r_file_ptr);
    }
    else
    _4009 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_15mem2_7262)){
        poke_addr = (unsigned char *)_15mem2_7262;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_15mem2_7262)->dbl);
    }
    *poke_addr = (unsigned char)_4009;
    _4009 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_7270 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7270, EF_READ);
        last_r_file_no = _fh_7270;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4010 = getKBchar();
        }
        else
        _4010 = getc(last_r_file_ptr);
    }
    else
    _4010 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_15mem3_7263)){
        poke_addr = (unsigned char *)_15mem3_7263;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_15mem3_7263)->dbl);
    }
    *poke_addr = (unsigned char)_4010;
    _4010 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_15mem0_7260)) {
        _4011 = *(unsigned long *)_15mem0_7260;
        if ((unsigned)_4011 > (unsigned)MAXINT)
        _4011 = NewDouble((double)(unsigned long)_4011);
    }
    else {
        _4011 = *(unsigned long *)(unsigned long)(DBL_PTR(_15mem0_7260)->dbl);
        if ((unsigned)_4011 > (unsigned)MAXINT)
        _4011 = NewDouble((double)(unsigned long)_4011);
    }
    return _4011;
    ;
}


int _15get_integer16(int _fh_7278)
{
    int _4014 = NOVALUE;
    int _4013 = NOVALUE;
    int _4012 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_7278)) {
        _1 = (long)(DBL_PTR(_fh_7278)->dbl);
        if (UNIQUE(DBL_PTR(_fh_7278)) && (DBL_PTR(_fh_7278)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_7278);
        _fh_7278 = _1;
    }

    /** 	poke(mem0, getc(fh))*/
    if (_fh_7278 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7278, EF_READ);
        last_r_file_no = _fh_7278;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4012 = getKBchar();
        }
        else
        _4012 = getc(last_r_file_ptr);
    }
    else
    _4012 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_15mem0_7260)){
        poke_addr = (unsigned char *)_15mem0_7260;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_15mem0_7260)->dbl);
    }
    *poke_addr = (unsigned char)_4012;
    _4012 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_7278 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7278, EF_READ);
        last_r_file_no = _fh_7278;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4013 = getKBchar();
        }
        else
        _4013 = getc(last_r_file_ptr);
    }
    else
    _4013 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_15mem1_7261)){
        poke_addr = (unsigned char *)_15mem1_7261;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_15mem1_7261)->dbl);
    }
    *poke_addr = (unsigned char)_4013;
    _4013 = NOVALUE;

    /** 	return peek2u(mem0)*/
    if (IS_ATOM_INT(_15mem0_7260)) {
        _4014 = *(unsigned short *)_15mem0_7260;
    }
    else {
        _4014 = *(unsigned short *)(unsigned long)(DBL_PTR(_15mem0_7260)->dbl);
    }
    return _4014;
    ;
}


void _15put_integer32(int _fh_7284, int _val_7285)
{
    int _4016 = NOVALUE;
    int _4015 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_7284)) {
        _1 = (long)(DBL_PTR(_fh_7284)->dbl);
        if (UNIQUE(DBL_PTR(_fh_7284)) && (DBL_PTR(_fh_7284)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_7284);
        _fh_7284 = _1;
    }

    /** 	poke4(mem0, val)*/
    if (IS_ATOM_INT(_15mem0_7260)){
        poke4_addr = (unsigned long *)_15mem0_7260;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_15mem0_7260)->dbl);
    }
    if (IS_ATOM_INT(_val_7285)) {
        *poke4_addr = (unsigned long)_val_7285;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_val_7285)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(fh, peek({mem0,4}))*/
    Ref(_15mem0_7260);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _15mem0_7260;
    ((int *)_2)[2] = 4;
    _4015 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_4015);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4016 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4015);
    _4015 = NOVALUE;
    EPuts(_fh_7284, _4016); // DJP 
    DeRefDS(_4016);
    _4016 = NOVALUE;

    /** end procedure*/
    DeRef(_val_7285);
    return;
    ;
}


void _15put_integer16(int _fh_7290, int _val_7291)
{
    int _4018 = NOVALUE;
    int _4017 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_7290)) {
        _1 = (long)(DBL_PTR(_fh_7290)->dbl);
        if (UNIQUE(DBL_PTR(_fh_7290)) && (DBL_PTR(_fh_7290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_7290);
        _fh_7290 = _1;
    }

    /** 	poke2(mem0, val)*/
    if (IS_ATOM_INT(_15mem0_7260)){
        poke2_addr = (unsigned short *)_15mem0_7260;
    }
    else {
        poke2_addr = (unsigned short *)(unsigned long)(DBL_PTR(_15mem0_7260)->dbl);
    }
    if (IS_ATOM_INT(_val_7291)) {
        *poke2_addr = (unsigned short)_val_7291;
    }
    else {
        _1 = (signed char)DBL_PTR(_val_7291)->dbl;
        *poke_addr = _1;
    }

    /** 	puts(fh, peek({mem0,2}))*/
    Ref(_15mem0_7260);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _15mem0_7260;
    ((int *)_2)[2] = 2;
    _4017 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_4017);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4018 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4017);
    _4017 = NOVALUE;
    EPuts(_fh_7290, _4018); // DJP 
    DeRefDS(_4018);
    _4018 = NOVALUE;

    /** end procedure*/
    DeRef(_val_7291);
    return;
    ;
}


int _15get_dstring(int _fh_7296, int _delim_7297)
{
    int _s_7298 = NOVALUE;
    int _c_7299 = NOVALUE;
    int _i_7300 = NOVALUE;
    int _4028 = NOVALUE;
    int _4024 = NOVALUE;
    int _4022 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_7296)) {
        _1 = (long)(DBL_PTR(_fh_7296)->dbl);
        if (UNIQUE(DBL_PTR(_fh_7296)) && (DBL_PTR(_fh_7296)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_7296);
        _fh_7296 = _1;
    }
    if (!IS_ATOM_INT(_delim_7297)) {
        _1 = (long)(DBL_PTR(_delim_7297)->dbl);
        if (UNIQUE(DBL_PTR(_delim_7297)) && (DBL_PTR(_delim_7297)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_delim_7297);
        _delim_7297 = _1;
    }

    /** 	s = repeat(-1, 256)*/
    DeRefi(_s_7298);
    _s_7298 = Repeat(-1, 256);

    /** 	i = 0*/
    _i_7300 = 0;

    /** 	while c != delim with entry do*/
    goto L1; // [24] 81
L2: 
    if (_c_7299 == _delim_7297)
    goto L3; // [29] 93

    /** 		i += 1*/
    _i_7300 = _i_7300 + 1;

    /** 		if i > length(s) then*/
    if (IS_SEQUENCE(_s_7298)){
            _4022 = SEQ_PTR(_s_7298)->length;
    }
    else {
        _4022 = 1;
    }
    if (_i_7300 <= _4022)
    goto L4; // [46] 61

    /** 			s &= repeat(-1, 256)*/
    _4024 = Repeat(-1, 256);
    Concat((object_ptr)&_s_7298, _s_7298, _4024);
    DeRefDS(_4024);
    _4024 = NOVALUE;
L4: 

    /** 		if c = -1 then*/
    if (_c_7299 != -1)
    goto L5; // [63] 72

    /** 			exit*/
    goto L3; // [69] 93
L5: 

    /** 		s[i] = c*/
    _2 = (int)SEQ_PTR(_s_7298);
    _2 = (int)(((s1_ptr)_2)->base + _i_7300);
    *(int *)_2 = _c_7299;

    /** 	  entry*/
L1: 

    /** 		c = getc(fh)*/
    if (_fh_7296 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7296, EF_READ);
        last_r_file_no = _fh_7296;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_7299 = getKBchar();
        }
        else
        _c_7299 = getc(last_r_file_ptr);
    }
    else
    _c_7299 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [90] 27
L3: 

    /** 	return s[1..i]*/
    rhs_slice_target = (object_ptr)&_4028;
    RHS_Slice(_s_7298, 1, _i_7300);
    DeRefDSi(_s_7298);
    return _4028;
    ;
}


int _15file_number(int _f_7319)
{
    int _4032 = NOVALUE;
    int _4031 = NOVALUE;
    int _4030 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(f) and f >= 0 then*/
    if (IS_ATOM_INT(_f_7319))
    _4030 = 1;
    else if (IS_ATOM_DBL(_f_7319))
    _4030 = IS_ATOM_INT(DoubleToInt(_f_7319));
    else
    _4030 = 0;
    if (_4030 == 0) {
        goto L1; // [6] 27
    }
    if (IS_ATOM_INT(_f_7319)) {
        _4032 = (_f_7319 >= 0);
    }
    else {
        _4032 = binary_op(GREATEREQ, _f_7319, 0);
    }
    if (_4032 == 0) {
        DeRef(_4032);
        _4032 = NOVALUE;
        goto L1; // [15] 27
    }
    else {
        if (!IS_ATOM_INT(_4032) && DBL_PTR(_4032)->dbl == 0.0){
            DeRef(_4032);
            _4032 = NOVALUE;
            goto L1; // [15] 27
        }
        DeRef(_4032);
        _4032 = NOVALUE;
    }
    DeRef(_4032);
    _4032 = NOVALUE;

    /** 		return 1*/
    DeRef(_f_7319);
    return 1;
    goto L2; // [24] 34
L1: 

    /** 		return 0*/
    DeRef(_f_7319);
    return 0;
L2: 
    ;
}


int _15file_position(int _p_7327)
{
    int _4035 = NOVALUE;
    int _4034 = NOVALUE;
    int _4033 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(p) and p >= -1 then*/
    _4033 = IS_ATOM(_p_7327);
    if (_4033 == 0) {
        goto L1; // [6] 27
    }
    if (IS_ATOM_INT(_p_7327)) {
        _4035 = (_p_7327 >= -1);
    }
    else {
        _4035 = binary_op(GREATEREQ, _p_7327, -1);
    }
    if (_4035 == 0) {
        DeRef(_4035);
        _4035 = NOVALUE;
        goto L1; // [15] 27
    }
    else {
        if (!IS_ATOM_INT(_4035) && DBL_PTR(_4035)->dbl == 0.0){
            DeRef(_4035);
            _4035 = NOVALUE;
            goto L1; // [15] 27
        }
        DeRef(_4035);
        _4035 = NOVALUE;
    }
    DeRef(_4035);
    _4035 = NOVALUE;

    /** 		return 1*/
    DeRef(_p_7327);
    return 1;
    goto L2; // [24] 34
L1: 

    /** 		return 0*/
    DeRef(_p_7327);
    return 0;
L2: 
    ;
}


int _15lock_type(int _t_7335)
{
    int _4040 = NOVALUE;
    int _4039 = NOVALUE;
    int _4038 = NOVALUE;
    int _4037 = NOVALUE;
    int _4036 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(t) and (t = LOCK_SHARED or t = LOCK_EXCLUSIVE) then*/
    if (IS_ATOM_INT(_t_7335))
    _4036 = 1;
    else if (IS_ATOM_DBL(_t_7335))
    _4036 = IS_ATOM_INT(DoubleToInt(_t_7335));
    else
    _4036 = 0;
    if (_4036 == 0) {
        goto L1; // [6] 43
    }
    if (IS_ATOM_INT(_t_7335)) {
        _4038 = (_t_7335 == 1);
    }
    else {
        _4038 = binary_op(EQUALS, _t_7335, 1);
    }
    if (IS_ATOM_INT(_4038)) {
        if (_4038 != 0) {
            _4039 = 1;
            goto L2; // [16] 30
        }
    }
    else {
        if (DBL_PTR(_4038)->dbl != 0.0) {
            _4039 = 1;
            goto L2; // [16] 30
        }
    }
    if (IS_ATOM_INT(_t_7335)) {
        _4040 = (_t_7335 == 2);
    }
    else {
        _4040 = binary_op(EQUALS, _t_7335, 2);
    }
    DeRef(_4039);
    if (IS_ATOM_INT(_4040))
    _4039 = (_4040 != 0);
    else
    _4039 = DBL_PTR(_4040)->dbl != 0.0;
L2: 
    if (_4039 == 0)
    {
        _4039 = NOVALUE;
        goto L1; // [31] 43
    }
    else{
        _4039 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_t_7335);
    DeRef(_4038);
    _4038 = NOVALUE;
    DeRef(_4040);
    _4040 = NOVALUE;
    return 1;
    goto L3; // [40] 50
L1: 

    /** 		return 0*/
    DeRef(_t_7335);
    DeRef(_4038);
    _4038 = NOVALUE;
    DeRef(_4040);
    _4040 = NOVALUE;
    return 0;
L3: 
    ;
}


int _15byte_range(int _r_7345)
{
    int _4059 = NOVALUE;
    int _4058 = NOVALUE;
    int _4057 = NOVALUE;
    int _4056 = NOVALUE;
    int _4055 = NOVALUE;
    int _4053 = NOVALUE;
    int _4052 = NOVALUE;
    int _4050 = NOVALUE;
    int _4049 = NOVALUE;
    int _4048 = NOVALUE;
    int _4047 = NOVALUE;
    int _4046 = NOVALUE;
    int _4044 = NOVALUE;
    int _4042 = NOVALUE;
    int _4041 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(r) then*/
    _4041 = IS_ATOM(_r_7345);
    if (_4041 == 0)
    {
        _4041 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _4041 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_r_7345);
    return 0;
L1: 

    /** 	if length(r) = 0 then*/
    if (IS_SEQUENCE(_r_7345)){
            _4042 = SEQ_PTR(_r_7345)->length;
    }
    else {
        _4042 = 1;
    }
    if (_4042 != 0)
    goto L2; // [21] 32

    /** 		return 1*/
    DeRef(_r_7345);
    return 1;
L2: 

    /** 	if length(r) != 2 then*/
    if (IS_SEQUENCE(_r_7345)){
            _4044 = SEQ_PTR(_r_7345)->length;
    }
    else {
        _4044 = 1;
    }
    if (_4044 == 2)
    goto L3; // [37] 48

    /** 		return 0*/
    DeRef(_r_7345);
    return 0;
L3: 

    /** 	if not (atom(r[1]) and atom(r[2])) then*/
    _2 = (int)SEQ_PTR(_r_7345);
    _4046 = (int)*(((s1_ptr)_2)->base + 1);
    _4047 = IS_ATOM(_4046);
    _4046 = NOVALUE;
    if (_4047 == 0) {
        DeRef(_4048);
        _4048 = 0;
        goto L4; // [57] 72
    }
    _2 = (int)SEQ_PTR(_r_7345);
    _4049 = (int)*(((s1_ptr)_2)->base + 2);
    _4050 = IS_ATOM(_4049);
    _4049 = NOVALUE;
    _4048 = (_4050 != 0);
L4: 
    if (_4048 != 0)
    goto L5; // [72] 82
    _4048 = NOVALUE;

    /** 		return 0*/
    DeRef(_r_7345);
    return 0;
L5: 

    /** 	if r[1] < 0 or r[2] < 0 then*/
    _2 = (int)SEQ_PTR(_r_7345);
    _4052 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4052)) {
        _4053 = (_4052 < 0);
    }
    else {
        _4053 = binary_op(LESS, _4052, 0);
    }
    _4052 = NOVALUE;
    if (IS_ATOM_INT(_4053)) {
        if (_4053 != 0) {
            goto L6; // [92] 109
        }
    }
    else {
        if (DBL_PTR(_4053)->dbl != 0.0) {
            goto L6; // [92] 109
        }
    }
    _2 = (int)SEQ_PTR(_r_7345);
    _4055 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4055)) {
        _4056 = (_4055 < 0);
    }
    else {
        _4056 = binary_op(LESS, _4055, 0);
    }
    _4055 = NOVALUE;
    if (_4056 == 0) {
        DeRef(_4056);
        _4056 = NOVALUE;
        goto L7; // [105] 116
    }
    else {
        if (!IS_ATOM_INT(_4056) && DBL_PTR(_4056)->dbl == 0.0){
            DeRef(_4056);
            _4056 = NOVALUE;
            goto L7; // [105] 116
        }
        DeRef(_4056);
        _4056 = NOVALUE;
    }
    DeRef(_4056);
    _4056 = NOVALUE;
L6: 

    /** 		return 0*/
    DeRef(_r_7345);
    DeRef(_4053);
    _4053 = NOVALUE;
    return 0;
L7: 

    /** 	return r[1] <= r[2]*/
    _2 = (int)SEQ_PTR(_r_7345);
    _4057 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_r_7345);
    _4058 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_4057) && IS_ATOM_INT(_4058)) {
        _4059 = (_4057 <= _4058);
    }
    else {
        _4059 = binary_op(LESSEQ, _4057, _4058);
    }
    _4057 = NOVALUE;
    _4058 = NOVALUE;
    DeRef(_r_7345);
    DeRef(_4053);
    _4053 = NOVALUE;
    return _4059;
    ;
}


int _15seek(int _fn_7372, int _pos_7373)
{
    int _4061 = NOVALUE;
    int _4060 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_7373);
    Ref(_fn_7372);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_7372;
    ((int *)_2)[2] = _pos_7373;
    _4060 = MAKE_SEQ(_1);
    _4061 = machine(19, _4060);
    DeRefDS(_4060);
    _4060 = NOVALUE;
    DeRef(_fn_7372);
    DeRef(_pos_7373);
    return _4061;
    ;
}


int _15where(int _fn_7378)
{
    int _4062 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_WHERE, fn)*/
    _4062 = machine(20, _fn_7378);
    DeRef(_fn_7378);
    return _4062;
    ;
}


void _15flush(int _fn_7382)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_FLUSH, fn)*/
    machine(60, _fn_7382);

    /** end procedure*/
    DeRef(_fn_7382);
    return;
    ;
}


int _15lock_file(int _fn_7385, int _t_7386, int _r_7387)
{
    int _4064 = NOVALUE;
    int _4063 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_fn_7385);
    *((int *)(_2+4)) = _fn_7385;
    Ref(_t_7386);
    *((int *)(_2+8)) = _t_7386;
    Ref(_r_7387);
    *((int *)(_2+12)) = _r_7387;
    _4063 = MAKE_SEQ(_1);
    _4064 = machine(61, _4063);
    DeRefDS(_4063);
    _4063 = NOVALUE;
    DeRef(_fn_7385);
    DeRef(_t_7386);
    DeRef(_r_7387);
    return _4064;
    ;
}


void _15unlock_file(int _fn_7392, int _r_7393)
{
    int _4065 = NOVALUE;
    int _0, _1, _2;
    

    /** 	machine_proc(M_UNLOCK_FILE, {fn, r})*/
    Ref(_r_7393);
    Ref(_fn_7392);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_7392;
    ((int *)_2)[2] = _r_7393;
    _4065 = MAKE_SEQ(_1);
    machine(62, _4065);
    DeRefDS(_4065);
    _4065 = NOVALUE;

    /** end procedure*/
    DeRef(_fn_7392);
    DeRef(_r_7393);
    return;
    ;
}


int _15read_lines(int _file_7397)
{
    int _fn_7398 = NOVALUE;
    int _ret_7399 = NOVALUE;
    int _y_7400 = NOVALUE;
    int _4085 = NOVALUE;
    int _4084 = NOVALUE;
    int _4083 = NOVALUE;
    int _4082 = NOVALUE;
    int _4077 = NOVALUE;
    int _4076 = NOVALUE;
    int _4074 = NOVALUE;
    int _4073 = NOVALUE;
    int _4072 = NOVALUE;
    int _4067 = NOVALUE;
    int _4066 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _4066 = IS_SEQUENCE(_file_7397);
    if (_4066 == 0)
    {
        _4066 = NOVALUE;
        goto L1; // [6] 37
    }
    else{
        _4066 = NOVALUE;
    }

    /** 		if length(file) = 0 then*/
    if (IS_SEQUENCE(_file_7397)){
            _4067 = SEQ_PTR(_file_7397)->length;
    }
    else {
        _4067 = 1;
    }
    if (_4067 != 0)
    goto L2; // [14] 26

    /** 			fn = 0*/
    DeRef(_fn_7398);
    _fn_7398 = 0;
    goto L3; // [23] 43
L2: 

    /** 			fn = open(file, "r")*/
    DeRef(_fn_7398);
    _fn_7398 = EOpen(_file_7397, _4069, 0);
    goto L3; // [34] 43
L1: 

    /** 		fn = file*/
    Ref(_file_7397);
    DeRef(_fn_7398);
    _fn_7398 = _file_7397;
L3: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_7398, 0)){
        goto L4; // [47] 56
    }
    DeRef(_file_7397);
    DeRef(_fn_7398);
    DeRef(_ret_7399);
    DeRefi(_y_7400);
    return -1;
L4: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_7399);
    _ret_7399 = _5;

    /** 	while sequence(y) with entry do*/
    goto L5; // [63] 125
L6: 
    _4072 = IS_SEQUENCE(_y_7400);
    if (_4072 == 0)
    {
        _4072 = NOVALUE;
        goto L7; // [71] 135
    }
    else{
        _4072 = NOVALUE;
    }

    /** 		if y[$] = '\n' then*/
    if (IS_SEQUENCE(_y_7400)){
            _4073 = SEQ_PTR(_y_7400)->length;
    }
    else {
        _4073 = 1;
    }
    _2 = (int)SEQ_PTR(_y_7400);
    _4074 = (int)*(((s1_ptr)_2)->base + _4073);
    if (_4074 != 10)
    goto L8; // [83] 104

    /** 			y = y[1..$-1]*/
    if (IS_SEQUENCE(_y_7400)){
            _4076 = SEQ_PTR(_y_7400)->length;
    }
    else {
        _4076 = 1;
    }
    _4077 = _4076 - 1;
    _4076 = NOVALUE;
    rhs_slice_target = (object_ptr)&_y_7400;
    RHS_Slice(_y_7400, 1, _4077);

    /** 			ifdef UNIX then*/
L8: 

    /** 		ret = append(ret, y)*/
    Ref(_y_7400);
    Append(&_ret_7399, _ret_7399, _y_7400);

    /** 		if fn = 0 then*/
    if (binary_op_a(NOTEQ, _fn_7398, 0)){
        goto L9; // [112] 122
    }

    /** 			puts(2, '\n')*/
    EPuts(2, 10); // DJP 
L9: 

    /** 	entry*/
L5: 

    /** 		y = gets(fn)*/
    DeRefi(_y_7400);
    _y_7400 = EGets(_fn_7398);

    /** 	end while*/
    goto L6; // [132] 66
L7: 

    /** 	if sequence(file) and length(file) != 0 then*/
    _4082 = IS_SEQUENCE(_file_7397);
    if (_4082 == 0) {
        goto LA; // [140] 160
    }
    if (IS_SEQUENCE(_file_7397)){
            _4084 = SEQ_PTR(_file_7397)->length;
    }
    else {
        _4084 = 1;
    }
    _4085 = (_4084 != 0);
    _4084 = NOVALUE;
    if (_4085 == 0)
    {
        DeRef(_4085);
        _4085 = NOVALUE;
        goto LA; // [152] 160
    }
    else{
        DeRef(_4085);
        _4085 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_7398))
    EClose(_fn_7398);
    else
    EClose((int)DBL_PTR(_fn_7398)->dbl);
LA: 

    /** 	return ret*/
    DeRef(_file_7397);
    DeRef(_fn_7398);
    DeRefi(_y_7400);
    _4074 = NOVALUE;
    DeRef(_4077);
    _4077 = NOVALUE;
    return _ret_7399;
    ;
}


int _15process_lines(int _file_7432, int _proc_7433, int _user_data_7434)
{
    int _fh_7435 = NOVALUE;
    int _aLine_7436 = NOVALUE;
    int _res_7437 = NOVALUE;
    int _line_no_7438 = NOVALUE;
    int _4108 = NOVALUE;
    int _4107 = NOVALUE;
    int _4106 = NOVALUE;
    int _4105 = NOVALUE;
    int _4102 = NOVALUE;
    int _4100 = NOVALUE;
    int _4098 = NOVALUE;
    int _4097 = NOVALUE;
    int _4095 = NOVALUE;
    int _4094 = NOVALUE;
    int _4093 = NOVALUE;
    int _4091 = NOVALUE;
    int _4087 = NOVALUE;
    int _4086 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_7433)) {
        _1 = (long)(DBL_PTR(_proc_7433)->dbl);
        if (UNIQUE(DBL_PTR(_proc_7433)) && (DBL_PTR(_proc_7433)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_7433);
        _proc_7433 = _1;
    }

    /** 	integer line_no = 0*/
    _line_no_7438 = 0;

    /** 	res = 0*/
    DeRef(_res_7437);
    _res_7437 = 0;

    /** 	if sequence(file) then*/
    _4086 = IS_SEQUENCE(_file_7432);
    if (_4086 == 0)
    {
        _4086 = NOVALUE;
        goto L1; // [22] 57
    }
    else{
        _4086 = NOVALUE;
    }

    /** 		if length(file) = 0 then*/
    if (IS_SEQUENCE(_file_7432)){
            _4087 = SEQ_PTR(_file_7432)->length;
    }
    else {
        _4087 = 1;
    }
    if (_4087 != 0)
    goto L2; // [30] 44

    /** 			fh = 0*/
    _fh_7435 = 0;
    goto L3; // [41] 67
L2: 

    /** 			fh = open(file, "r")*/
    _fh_7435 = EOpen(_file_7432, _4069, 0);
    goto L3; // [54] 67
L1: 

    /** 		fh = file*/
    Ref(_file_7432);
    _fh_7435 = _file_7432;
    if (!IS_ATOM_INT(_fh_7435)) {
        _1 = (long)(DBL_PTR(_fh_7435)->dbl);
        if (UNIQUE(DBL_PTR(_fh_7435)) && (DBL_PTR(_fh_7435)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_7435);
        _fh_7435 = _1;
    }
L3: 

    /** 	if fh < 0 then */
    if (_fh_7435 >= 0)
    goto L4; // [71] 171

    /** 		return -1 */
    DeRef(_file_7432);
    DeRef(_user_data_7434);
    DeRefi(_aLine_7436);
    DeRef(_res_7437);
    return -1;

    /** 	while sequence(aLine) with entry do*/
    goto L4; // [84] 171
L5: 
    _4091 = IS_SEQUENCE(_aLine_7436);
    if (_4091 == 0)
    {
        _4091 = NOVALUE;
        goto L6; // [92] 181
    }
    else{
        _4091 = NOVALUE;
    }

    /** 		line_no += 1*/
    _line_no_7438 = _line_no_7438 + 1;

    /** 		if length(aLine) then*/
    if (IS_SEQUENCE(_aLine_7436)){
            _4093 = SEQ_PTR(_aLine_7436)->length;
    }
    else {
        _4093 = 1;
    }
    if (_4093 == 0)
    {
        _4093 = NOVALUE;
        goto L7; // [108] 142
    }
    else{
        _4093 = NOVALUE;
    }

    /** 			if aLine[$] = '\n' then*/
    if (IS_SEQUENCE(_aLine_7436)){
            _4094 = SEQ_PTR(_aLine_7436)->length;
    }
    else {
        _4094 = 1;
    }
    _2 = (int)SEQ_PTR(_aLine_7436);
    _4095 = (int)*(((s1_ptr)_2)->base + _4094);
    if (_4095 != 10)
    goto L8; // [120] 141

    /** 				aLine = aLine[1 .. $-1]*/
    if (IS_SEQUENCE(_aLine_7436)){
            _4097 = SEQ_PTR(_aLine_7436)->length;
    }
    else {
        _4097 = 1;
    }
    _4098 = _4097 - 1;
    _4097 = NOVALUE;
    rhs_slice_target = (object_ptr)&_aLine_7436;
    RHS_Slice(_aLine_7436, 1, _4098);

    /** 				ifdef UNIX then*/
L8: 
L7: 

    /** 		res = call_func(proc, {aLine, line_no, user_data})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_aLine_7436);
    *((int *)(_2+4)) = _aLine_7436;
    *((int *)(_2+8)) = _line_no_7438;
    Ref(_user_data_7434);
    *((int *)(_2+12)) = _user_data_7434;
    _4100 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_4100);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_proc_7433].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12)
                         );
    DeRef(_res_7437);
    _res_7437 = _1;
    DeRefDS(_4100);
    _4100 = NOVALUE;

    /** 		if not equal(res, 0) then*/
    if (_res_7437 == 0)
    _4102 = 1;
    else if (IS_ATOM_INT(_res_7437) && IS_ATOM_INT(0))
    _4102 = 0;
    else
    _4102 = (compare(_res_7437, 0) == 0);
    if (_4102 != 0)
    goto L9; // [160] 168
    _4102 = NOVALUE;

    /** 			exit*/
    goto L6; // [165] 181
L9: 

    /** 	entry*/
L4: 

    /** 		aLine = gets(fh)*/
    DeRefi(_aLine_7436);
    _aLine_7436 = EGets(_fh_7435);

    /** 	end while*/
    goto L5; // [178] 87
L6: 

    /** 	if sequence(file) and length(file) != 0 then*/
    _4105 = IS_SEQUENCE(_file_7432);
    if (_4105 == 0) {
        goto LA; // [186] 206
    }
    if (IS_SEQUENCE(_file_7432)){
            _4107 = SEQ_PTR(_file_7432)->length;
    }
    else {
        _4107 = 1;
    }
    _4108 = (_4107 != 0);
    _4107 = NOVALUE;
    if (_4108 == 0)
    {
        DeRef(_4108);
        _4108 = NOVALUE;
        goto LA; // [198] 206
    }
    else{
        DeRef(_4108);
        _4108 = NOVALUE;
    }

    /** 		close(fh)*/
    EClose(_fh_7435);
LA: 

    /** 	return res*/
    DeRef(_file_7432);
    DeRef(_user_data_7434);
    DeRefi(_aLine_7436);
    _4095 = NOVALUE;
    DeRef(_4098);
    _4098 = NOVALUE;
    return _res_7437;
    ;
}


int _15write_lines(int _file_7474, int _lines_7475)
{
    int _fn_7476 = NOVALUE;
    int _4115 = NOVALUE;
    int _4114 = NOVALUE;
    int _4113 = NOVALUE;
    int _4109 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _4109 = IS_SEQUENCE(_file_7474);
    if (_4109 == 0)
    {
        _4109 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _4109 = NOVALUE;
    }

    /**     	fn = open(file, "w")*/
    DeRef(_fn_7476);
    _fn_7476 = EOpen(_file_7474, _4110, 0);
    goto L2; // [18] 27
L1: 

    /** 		fn = file*/
    Ref(_file_7474);
    DeRef(_fn_7476);
    _fn_7476 = _file_7474;
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_7476, 0)){
        goto L3; // [31] 40
    }
    DeRef(_file_7474);
    DeRefDS(_lines_7475);
    DeRef(_fn_7476);
    return -1;
L3: 

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_7475)){
            _4113 = SEQ_PTR(_lines_7475)->length;
    }
    else {
        _4113 = 1;
    }
    {
        int _i_7485;
        _i_7485 = 1;
L4: 
        if (_i_7485 > _4113){
            goto L5; // [45] 73
        }

        /** 		puts(fn, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_7475);
        _4114 = (int)*(((s1_ptr)_2)->base + _i_7485);
        EPuts(_fn_7476, _4114); // DJP 
        _4114 = NOVALUE;

        /** 		puts(fn, '\n')*/
        EPuts(_fn_7476, 10); // DJP 

        /** 	end for*/
        _i_7485 = _i_7485 + 1;
        goto L4; // [68] 52
L5: 
        ;
    }

    /** 	if sequence(file) then*/
    _4115 = IS_SEQUENCE(_file_7474);
    if (_4115 == 0)
    {
        _4115 = NOVALUE;
        goto L6; // [78] 86
    }
    else{
        _4115 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_7476))
    EClose(_fn_7476);
    else
    EClose((int)DBL_PTR(_fn_7476)->dbl);
L6: 

    /** 	return 1*/
    DeRef(_file_7474);
    DeRefDS(_lines_7475);
    DeRef(_fn_7476);
    return 1;
    ;
}


int _15append_lines(int _file_7492, int _lines_7493)
{
    int _fn_7494 = NOVALUE;
    int _4120 = NOVALUE;
    int _4119 = NOVALUE;
    int _0, _1, _2;
    

    /**   	fn = open(file, "a")*/
    _fn_7494 = EOpen(_file_7492, _4116, 0);

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_7494 >= 0)
    goto L1; // [14] 23
    DeRefDS(_file_7492);
    DeRefDS(_lines_7493);
    return -1;
L1: 

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_7493)){
            _4119 = SEQ_PTR(_lines_7493)->length;
    }
    else {
        _4119 = 1;
    }
    {
        int _i_7500;
        _i_7500 = 1;
L2: 
        if (_i_7500 > _4119){
            goto L3; // [28] 56
        }

        /** 		puts(fn, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_7493);
        _4120 = (int)*(((s1_ptr)_2)->base + _i_7500);
        EPuts(_fn_7494, _4120); // DJP 
        _4120 = NOVALUE;

        /** 		puts(fn, '\n')*/
        EPuts(_fn_7494, 10); // DJP 

        /** 	end for*/
        _i_7500 = _i_7500 + 1;
        goto L2; // [51] 35
L3: 
        ;
    }

    /** 	close(fn)*/
    EClose(_fn_7494);

    /** 	return 1*/
    DeRefDS(_file_7492);
    DeRefDS(_lines_7493);
    return 1;
    ;
}


int _15read_file(int _file_7512, int _as_text_7513)
{
    int _fn_7514 = NOVALUE;
    int _len_7515 = NOVALUE;
    int _ret_7516 = NOVALUE;
    int _seek_1__tmp_at49_7525 = NOVALUE;
    int _seek_inlined_seek_at_49_7524 = NOVALUE;
    int _where_inlined_where_at_64_7527 = NOVALUE;
    int _seek_1__tmp_at77_7530 = NOVALUE;
    int _seek_inlined_seek_at_77_7529 = NOVALUE;
    int _4144 = NOVALUE;
    int _4143 = NOVALUE;
    int _4141 = NOVALUE;
    int _4137 = NOVALUE;
    int _4131 = NOVALUE;
    int _4130 = NOVALUE;
    int _4129 = NOVALUE;
    int _4128 = NOVALUE;
    int _4124 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_as_text_7513)) {
        _1 = (long)(DBL_PTR(_as_text_7513)->dbl);
        if (UNIQUE(DBL_PTR(_as_text_7513)) && (DBL_PTR(_as_text_7513)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_as_text_7513);
        _as_text_7513 = _1;
    }

    /** 	if sequence(file) then*/
    _4124 = IS_SEQUENCE(_file_7512);
    if (_4124 == 0)
    {
        _4124 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _4124 = NOVALUE;
    }

    /** 		fn = open(file, "rb")*/
    _fn_7514 = EOpen(_file_7512, _2913, 0);
    goto L2; // [22] 35
L1: 

    /** 		fn = file*/
    Ref(_file_7512);
    _fn_7514 = _file_7512;
    if (!IS_ATOM_INT(_fn_7514)) {
        _1 = (long)(DBL_PTR(_fn_7514)->dbl);
        if (UNIQUE(DBL_PTR(_fn_7514)) && (DBL_PTR(_fn_7514)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_7514);
        _fn_7514 = _1;
    }
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_7514 >= 0)
    goto L3; // [39] 48
    DeRef(_file_7512);
    DeRef(_ret_7516);
    return -1;
L3: 

    /** 	seek(fn, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at49_7525);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_7514;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at49_7525 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_49_7524 = machine(19, _seek_1__tmp_at49_7525);
    DeRefi(_seek_1__tmp_at49_7525);
    _seek_1__tmp_at49_7525 = NOVALUE;

    /** 	len = where(fn)*/

    /** 	return machine_func(M_WHERE, fn)*/
    _len_7515 = machine(20, _fn_7514);
    if (!IS_ATOM_INT(_len_7515)) {
        _1 = (long)(DBL_PTR(_len_7515)->dbl);
        if (UNIQUE(DBL_PTR(_len_7515)) && (DBL_PTR(_len_7515)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_7515);
        _len_7515 = _1;
    }

    /** 	seek(fn, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at77_7530);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_7514;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at77_7530 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_77_7529 = machine(19, _seek_1__tmp_at77_7530);
    DeRefi(_seek_1__tmp_at77_7530);
    _seek_1__tmp_at77_7530 = NOVALUE;

    /** 	ret = repeat(0, len)*/
    DeRef(_ret_7516);
    _ret_7516 = Repeat(0, _len_7515);

    /** 	for i = 1 to len do*/
    _4128 = _len_7515;
    {
        int _i_7533;
        _i_7533 = 1;
L4: 
        if (_i_7533 > _4128){
            goto L5; // [102] 125
        }

        /** 		ret[i] = getc(fn)*/
        if (_fn_7514 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_7514, EF_READ);
            last_r_file_no = _fn_7514;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4129 = getKBchar();
            }
            else
            _4129 = getc(last_r_file_ptr);
        }
        else
        _4129 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_ret_7516);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_7516 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_7533);
        _1 = *(int *)_2;
        *(int *)_2 = _4129;
        if( _1 != _4129 ){
            DeRef(_1);
        }
        _4129 = NOVALUE;

        /** 	end for*/
        _i_7533 = _i_7533 + 1;
        goto L4; // [120] 109
L5: 
        ;
    }

    /** 	if sequence(file) then*/
    _4130 = IS_SEQUENCE(_file_7512);
    if (_4130 == 0)
    {
        _4130 = NOVALUE;
        goto L6; // [130] 138
    }
    else{
        _4130 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_7514);
L6: 

    /** 	ifdef WINDOWS then*/

    /** 		for i = len to 1 by -1 do*/
    {
        int _i_7539;
        _i_7539 = _len_7515;
L7: 
        if (_i_7539 < 1){
            goto L8; // [142] 185
        }

        /** 			if ret[i] != -1 then*/
        _2 = (int)SEQ_PTR(_ret_7516);
        _4131 = (int)*(((s1_ptr)_2)->base + _i_7539);
        if (binary_op_a(EQUALS, _4131, -1)){
            _4131 = NOVALUE;
            goto L9; // [155] 178
        }
        _4131 = NOVALUE;

        /** 				if i != len then*/
        if (_i_7539 == _len_7515)
        goto L8; // [161] 185

        /** 					ret = ret[1 .. i]*/
        rhs_slice_target = (object_ptr)&_ret_7516;
        RHS_Slice(_ret_7516, 1, _i_7539);

        /** 				exit*/
        goto L8; // [175] 185
L9: 

        /** 		end for*/
        _i_7539 = _i_7539 + -1;
        goto L7; // [180] 149
L8: 
        ;
    }

    /** 	if as_text = BINARY_MODE then*/
    if (_as_text_7513 != 1)
    goto LA; // [189] 200

    /** 		return ret*/
    DeRef(_file_7512);
    return _ret_7516;
LA: 

    /** 	fn = find(26, ret) -- Any Ctrl-Z found?*/
    _fn_7514 = find_from(26, _ret_7516, 1);

    /** 	if fn then*/
    if (_fn_7514 == 0)
    {
        goto LB; // [211] 226
    }
    else{
    }

    /** 		ret = ret[1 .. fn - 1]*/
    _4137 = _fn_7514 - 1;
    rhs_slice_target = (object_ptr)&_ret_7516;
    RHS_Slice(_ret_7516, 1, _4137);
LB: 

    /** 	ret = search:match_replace({13,10}, ret, {10})*/
    RefDS(_4139);
    RefDS(_ret_7516);
    RefDS(_3957);
    _0 = _ret_7516;
    _ret_7516 = _6match_replace(_4139, _ret_7516, _3957, 0);
    DeRefDS(_0);

    /** 	if length(ret) > 0 then*/
    if (IS_SEQUENCE(_ret_7516)){
            _4141 = SEQ_PTR(_ret_7516)->length;
    }
    else {
        _4141 = 1;
    }
    if (_4141 <= 0)
    goto LC; // [242] 269

    /** 		if ret[$] != 10 then*/
    if (IS_SEQUENCE(_ret_7516)){
            _4143 = SEQ_PTR(_ret_7516)->length;
    }
    else {
        _4143 = 1;
    }
    _2 = (int)SEQ_PTR(_ret_7516);
    _4144 = (int)*(((s1_ptr)_2)->base + _4143);
    if (binary_op_a(EQUALS, _4144, 10)){
        _4144 = NOVALUE;
        goto LD; // [255] 277
    }
    _4144 = NOVALUE;

    /** 			ret &= 10*/
    Append(&_ret_7516, _ret_7516, 10);
    goto LD; // [266] 277
LC: 

    /** 		ret = {10}*/
    RefDS(_3957);
    DeRef(_ret_7516);
    _ret_7516 = _3957;
LD: 

    /** 	return ret*/
    DeRef(_file_7512);
    DeRef(_4137);
    _4137 = NOVALUE;
    return _ret_7516;
    ;
}


int _15write_file(int _file_7565, int _data_7566, int _as_text_7567)
{
    int _fn_7568 = NOVALUE;
    int _4169 = NOVALUE;
    int _4164 = NOVALUE;
    int _4154 = NOVALUE;
    int _4153 = NOVALUE;
    int _4151 = NOVALUE;
    int _4149 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_as_text_7567)) {
        _1 = (long)(DBL_PTR(_as_text_7567)->dbl);
        if (UNIQUE(DBL_PTR(_as_text_7567)) && (DBL_PTR(_as_text_7567)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_as_text_7567);
        _as_text_7567 = _1;
    }

    /** 	if as_text != BINARY_MODE then*/
    if (_as_text_7567 == 1)
    goto L1; // [11] 158

    /** 		fn = find(26, data)*/
    _fn_7568 = find_from(26, _data_7566, 1);

    /** 		if fn then*/
    if (_fn_7568 == 0)
    {
        goto L2; // [26] 41
    }
    else{
    }

    /** 			data = data[1 .. fn-1]*/
    _4149 = _fn_7568 - 1;
    rhs_slice_target = (object_ptr)&_data_7566;
    RHS_Slice(_data_7566, 1, _4149);
L2: 

    /** 		if length(data) > 0 then*/
    if (IS_SEQUENCE(_data_7566)){
            _4151 = SEQ_PTR(_data_7566)->length;
    }
    else {
        _4151 = 1;
    }
    if (_4151 <= 0)
    goto L3; // [46] 73

    /** 			if data[$] != 10 then*/
    if (IS_SEQUENCE(_data_7566)){
            _4153 = SEQ_PTR(_data_7566)->length;
    }
    else {
        _4153 = 1;
    }
    _2 = (int)SEQ_PTR(_data_7566);
    _4154 = (int)*(((s1_ptr)_2)->base + _4153);
    if (binary_op_a(EQUALS, _4154, 10)){
        _4154 = NOVALUE;
        goto L4; // [59] 81
    }
    _4154 = NOVALUE;

    /** 				data &= 10*/
    Append(&_data_7566, _data_7566, 10);
    goto L4; // [70] 81
L3: 

    /** 			data = {10}*/
    RefDS(_3957);
    DeRefDS(_data_7566);
    _data_7566 = _3957;
L4: 

    /** 		if as_text = TEXT_MODE then*/
    if (_as_text_7567 != 2)
    goto L5; // [85] 103

    /** 			data = search:match_replace({13,10}, data, {10})*/
    RefDS(_4139);
    RefDS(_data_7566);
    RefDS(_3957);
    _0 = _data_7566;
    _data_7566 = _6match_replace(_4139, _data_7566, _3957, 0);
    DeRefDS(_0);
    goto L6; // [100] 157
L5: 

    /** 		elsif as_text = UNIX_TEXT then*/
    if (_as_text_7567 != 3)
    goto L7; // [107] 125

    /** 			data = search:match_replace({13,10}, data, {10})*/
    RefDS(_4139);
    RefDS(_data_7566);
    RefDS(_3957);
    _0 = _data_7566;
    _data_7566 = _6match_replace(_4139, _data_7566, _3957, 0);
    DeRefDS(_0);
    goto L6; // [122] 157
L7: 

    /** 		elsif as_text = DOS_TEXT then*/
    if (_as_text_7567 != 4)
    goto L8; // [129] 156

    /** 			data = search:match_replace({13,10}, data, {10})*/
    RefDS(_4139);
    RefDS(_data_7566);
    RefDS(_3957);
    _0 = _data_7566;
    _data_7566 = _6match_replace(_4139, _data_7566, _3957, 0);
    DeRefDS(_0);

    /** 			data = search:match_replace({10}, data, {13,10})*/
    RefDS(_3957);
    RefDS(_data_7566);
    RefDS(_4139);
    _0 = _data_7566;
    _data_7566 = _6match_replace(_3957, _data_7566, _4139, 0);
    DeRefDS(_0);
L8: 
L6: 
L1: 

    /** 	if sequence(file) then*/
    _4164 = IS_SEQUENCE(_file_7565);
    if (_4164 == 0)
    {
        _4164 = NOVALUE;
        goto L9; // [163] 199
    }
    else{
        _4164 = NOVALUE;
    }

    /** 		if as_text = TEXT_MODE then*/
    if (_as_text_7567 != 2)
    goto LA; // [170] 186

    /** 			fn = open(file, "w")*/
    _fn_7568 = EOpen(_file_7565, _4110, 0);
    goto LB; // [183] 209
LA: 

    /** 			fn = open(file, "wb")*/
    _fn_7568 = EOpen(_file_7565, _2908, 0);
    goto LB; // [196] 209
L9: 

    /** 		fn = file*/
    Ref(_file_7565);
    _fn_7568 = _file_7565;
    if (!IS_ATOM_INT(_fn_7568)) {
        _1 = (long)(DBL_PTR(_fn_7568)->dbl);
        if (UNIQUE(DBL_PTR(_fn_7568)) && (DBL_PTR(_fn_7568)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_7568);
        _fn_7568 = _1;
    }
LB: 

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_7568 >= 0)
    goto LC; // [213] 222
    DeRef(_file_7565);
    DeRefDS(_data_7566);
    DeRef(_4149);
    _4149 = NOVALUE;
    return -1;
LC: 

    /** 	puts(fn, data)*/
    EPuts(_fn_7568, _data_7566); // DJP 

    /** 	if sequence(file) then*/
    _4169 = IS_SEQUENCE(_file_7565);
    if (_4169 == 0)
    {
        _4169 = NOVALUE;
        goto LD; // [232] 240
    }
    else{
        _4169 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_7568);
LD: 

    /** 	return 1*/
    DeRef(_file_7565);
    DeRefDS(_data_7566);
    DeRef(_4149);
    _4149 = NOVALUE;
    return 1;
    ;
}


void _15writef(int _fm_7608, int _data_7609, int _fn_7610, int _data_not_string_7611)
{
    int _real_fn_7612 = NOVALUE;
    int _close_fn_7613 = NOVALUE;
    int _out_style_7614 = NOVALUE;
    int _ts_7617 = NOVALUE;
    int _msg_inlined_crash_at_169_7642 = NOVALUE;
    int _data_inlined_crash_at_166_7641 = NOVALUE;
    int _4189 = NOVALUE;
    int _4187 = NOVALUE;
    int _4186 = NOVALUE;
    int _4185 = NOVALUE;
    int _4179 = NOVALUE;
    int _4178 = NOVALUE;
    int _4177 = NOVALUE;
    int _4176 = NOVALUE;
    int _4175 = NOVALUE;
    int _4174 = NOVALUE;
    int _4172 = NOVALUE;
    int _4171 = NOVALUE;
    int _4170 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer real_fn = 0*/
    _real_fn_7612 = 0;

    /** 	integer close_fn = 0*/
    _close_fn_7613 = 0;

    /** 	sequence out_style = "w"*/
    RefDS(_4110);
    DeRefi(_out_style_7614);
    _out_style_7614 = _4110;

    /** 	if integer(fm) then*/
    if (IS_ATOM_INT(_fm_7608))
    _4170 = 1;
    else if (IS_ATOM_DBL(_fm_7608))
    _4170 = IS_ATOM_INT(DoubleToInt(_fm_7608));
    else
    _4170 = 0;
    if (_4170 == 0)
    {
        _4170 = NOVALUE;
        goto L1; // [27] 53
    }
    else{
        _4170 = NOVALUE;
    }

    /** 		object ts*/

    /** 		ts = fm*/
    Ref(_fm_7608);
    DeRef(_ts_7617);
    _ts_7617 = _fm_7608;

    /** 		fm = data*/
    Ref(_data_7609);
    DeRef(_fm_7608);
    _fm_7608 = _data_7609;

    /** 		data = fn*/
    Ref(_fn_7610);
    DeRef(_data_7609);
    _data_7609 = _fn_7610;

    /** 		fn = ts*/
    Ref(_ts_7617);
    DeRef(_fn_7610);
    _fn_7610 = _ts_7617;
L1: 
    DeRef(_ts_7617);
    _ts_7617 = NOVALUE;

    /** 	if sequence(fn) then*/
    _4171 = IS_SEQUENCE(_fn_7610);
    if (_4171 == 0)
    {
        _4171 = NOVALUE;
        goto L2; // [60] 199
    }
    else{
        _4171 = NOVALUE;
    }

    /** 		if length(fn) = 2 then*/
    if (IS_SEQUENCE(_fn_7610)){
            _4172 = SEQ_PTR(_fn_7610)->length;
    }
    else {
        _4172 = 1;
    }
    if (_4172 != 2)
    goto L3; // [68] 146

    /** 			if sequence(fn[1]) then*/
    _2 = (int)SEQ_PTR(_fn_7610);
    _4174 = (int)*(((s1_ptr)_2)->base + 1);
    _4175 = IS_SEQUENCE(_4174);
    _4174 = NOVALUE;
    if (_4175 == 0)
    {
        _4175 = NOVALUE;
        goto L4; // [81] 145
    }
    else{
        _4175 = NOVALUE;
    }

    /** 				if equal(fn[2], 'a') then*/
    _2 = (int)SEQ_PTR(_fn_7610);
    _4176 = (int)*(((s1_ptr)_2)->base + 2);
    if (_4176 == 97)
    _4177 = 1;
    else if (IS_ATOM_INT(_4176) && IS_ATOM_INT(97))
    _4177 = 0;
    else
    _4177 = (compare(_4176, 97) == 0);
    _4176 = NOVALUE;
    if (_4177 == 0)
    {
        _4177 = NOVALUE;
        goto L5; // [94] 107
    }
    else{
        _4177 = NOVALUE;
    }

    /** 					out_style = "a"*/
    RefDS(_4116);
    DeRefi(_out_style_7614);
    _out_style_7614 = _4116;
    goto L6; // [104] 138
L5: 

    /** 				elsif not equal(fn[2], "a") then*/
    _2 = (int)SEQ_PTR(_fn_7610);
    _4178 = (int)*(((s1_ptr)_2)->base + 2);
    if (_4178 == _4116)
    _4179 = 1;
    else if (IS_ATOM_INT(_4178) && IS_ATOM_INT(_4116))
    _4179 = 0;
    else
    _4179 = (compare(_4178, _4116) == 0);
    _4178 = NOVALUE;
    if (_4179 != 0)
    goto L7; // [117] 130
    _4179 = NOVALUE;

    /** 					out_style = "w"*/
    RefDS(_4110);
    DeRefi(_out_style_7614);
    _out_style_7614 = _4110;
    goto L6; // [127] 138
L7: 

    /** 					out_style = "a"*/
    RefDS(_4116);
    DeRefi(_out_style_7614);
    _out_style_7614 = _4116;
L6: 

    /** 				fn = fn[1]*/
    _0 = _fn_7610;
    _2 = (int)SEQ_PTR(_fn_7610);
    _fn_7610 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_fn_7610);
    DeRef(_0);
L4: 
L3: 

    /** 		real_fn = open(fn, out_style)*/
    _real_fn_7612 = EOpen(_fn_7610, _out_style_7614, 0);

    /** 		if real_fn = -1 then*/
    if (_real_fn_7612 != -1)
    goto L8; // [157] 189

    /** 			error:crash("Unable to write to '%s'", {fn})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_fn_7610);
    *((int *)(_2+4)) = _fn_7610;
    _4185 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_166_7641);
    _data_inlined_crash_at_166_7641 = _4185;
    _4185 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_169_7642);
    _msg_inlined_crash_at_169_7642 = EPrintf(-9999999, _4184, _data_inlined_crash_at_166_7641);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_169_7642);

    /** end procedure*/
    goto L9; // [183] 186
L9: 
    DeRef(_data_inlined_crash_at_166_7641);
    _data_inlined_crash_at_166_7641 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_169_7642);
    _msg_inlined_crash_at_169_7642 = NOVALUE;
L8: 

    /** 		close_fn = 1*/
    _close_fn_7613 = 1;
    goto LA; // [196] 209
L2: 

    /** 		real_fn = fn*/
    Ref(_fn_7610);
    _real_fn_7612 = _fn_7610;
    if (!IS_ATOM_INT(_real_fn_7612)) {
        _1 = (long)(DBL_PTR(_real_fn_7612)->dbl);
        if (UNIQUE(DBL_PTR(_real_fn_7612)) && (DBL_PTR(_real_fn_7612)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_real_fn_7612);
        _real_fn_7612 = _1;
    }
LA: 

    /** 	if equal(data_not_string, 0) then*/
    if (_data_not_string_7611 == 0)
    _4186 = 1;
    else if (IS_ATOM_INT(_data_not_string_7611) && IS_ATOM_INT(0))
    _4186 = 0;
    else
    _4186 = (compare(_data_not_string_7611, 0) == 0);
    if (_4186 == 0)
    {
        _4186 = NOVALUE;
        goto LB; // [215] 235
    }
    else{
        _4186 = NOVALUE;
    }

    /** 		if types:t_display(data) then*/
    Ref(_data_7609);
    _4187 = _5t_display(_data_7609);
    if (_4187 == 0) {
        DeRef(_4187);
        _4187 = NOVALUE;
        goto LC; // [224] 234
    }
    else {
        if (!IS_ATOM_INT(_4187) && DBL_PTR(_4187)->dbl == 0.0){
            DeRef(_4187);
            _4187 = NOVALUE;
            goto LC; // [224] 234
        }
        DeRef(_4187);
        _4187 = NOVALUE;
    }
    DeRef(_4187);
    _4187 = NOVALUE;

    /** 			data = {data}*/
    _0 = _data_7609;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_data_7609);
    *((int *)(_2+4)) = _data_7609;
    _data_7609 = MAKE_SEQ(_1);
    DeRef(_0);
LC: 
LB: 

    /**     puts(real_fn, text:format( fm, data ) )*/
    Ref(_fm_7608);
    Ref(_data_7609);
    _4189 = _16format(_fm_7608, _data_7609);
    EPuts(_real_fn_7612, _4189); // DJP 
    DeRef(_4189);
    _4189 = NOVALUE;

    /**     if close_fn then*/
    if (_close_fn_7613 == 0)
    {
        goto LD; // [247] 255
    }
    else{
    }

    /**     	close(real_fn)*/
    EClose(_real_fn_7612);
LD: 

    /** end procedure*/
    DeRef(_fm_7608);
    DeRef(_data_7609);
    DeRef(_fn_7610);
    DeRef(_data_not_string_7611);
    DeRefi(_out_style_7614);
    return;
    ;
}


void _15writefln(int _fm_7653, int _data_7654, int _fn_7655, int _data_not_string_7656)
{
    int _4192 = NOVALUE;
    int _4191 = NOVALUE;
    int _4190 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(fm) then*/
    if (IS_ATOM_INT(_fm_7653))
    _4190 = 1;
    else if (IS_ATOM_DBL(_fm_7653))
    _4190 = IS_ATOM_INT(DoubleToInt(_fm_7653));
    else
    _4190 = 0;
    if (_4190 == 0)
    {
        _4190 = NOVALUE;
        goto L1; // [6] 24
    }
    else{
        _4190 = NOVALUE;
    }

    /** 		writef(data & '\n', fn, fm, data_not_string)*/
    if (IS_SEQUENCE(_data_7654) && IS_ATOM(10)) {
        Append(&_4191, _data_7654, 10);
    }
    else if (IS_ATOM(_data_7654) && IS_SEQUENCE(10)) {
    }
    else {
        Concat((object_ptr)&_4191, _data_7654, 10);
    }
    Ref(_fn_7655);
    Ref(_fm_7653);
    Ref(_data_not_string_7656);
    _15writef(_4191, _fn_7655, _fm_7653, _data_not_string_7656);
    _4191 = NOVALUE;
    goto L2; // [21] 37
L1: 

    /** 		writef(fm & '\n', data, fn, data_not_string)*/
    if (IS_SEQUENCE(_fm_7653) && IS_ATOM(10)) {
        Append(&_4192, _fm_7653, 10);
    }
    else if (IS_ATOM(_fm_7653) && IS_SEQUENCE(10)) {
    }
    else {
        Concat((object_ptr)&_4192, _fm_7653, 10);
    }
    Ref(_data_7654);
    Ref(_fn_7655);
    Ref(_data_not_string_7656);
    _15writef(_4192, _data_7654, _fn_7655, _data_not_string_7656);
    _4192 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_fm_7653);
    DeRef(_data_7654);
    DeRef(_fn_7655);
    DeRef(_data_not_string_7656);
    return;
    ;
}



// 0xA7CD0D97
