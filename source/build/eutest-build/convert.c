// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _17int_to_bytes(int _x_3918)
{
    int _a_3919 = NOVALUE;
    int _b_3920 = NOVALUE;
    int _c_3921 = NOVALUE;
    int _d_3922 = NOVALUE;
    int _2015 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_3918)) {
        _a_3919 = (_x_3918 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _a_3919 = Dremainder(DBL_PTR(_x_3918), &temp_d);
    }
    if (!IS_ATOM_INT(_a_3919)) {
        _1 = (long)(DBL_PTR(_a_3919)->dbl);
        if (UNIQUE(DBL_PTR(_a_3919)) && (DBL_PTR(_a_3919)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_3919);
        _a_3919 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_3918;
    if (IS_ATOM_INT(_x_3918)) {
        if (256 > 0 && _x_3918 >= 0) {
            _x_3918 = _x_3918 / 256;
        }
        else {
            temp_dbl = floor((double)_x_3918 / (double)256);
            if (_x_3918 != MININT)
            _x_3918 = (long)temp_dbl;
            else
            _x_3918 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_3918, 256);
        _x_3918 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	b = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_3918)) {
        _b_3920 = (_x_3918 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _b_3920 = Dremainder(DBL_PTR(_x_3918), &temp_d);
    }
    if (!IS_ATOM_INT(_b_3920)) {
        _1 = (long)(DBL_PTR(_b_3920)->dbl);
        if (UNIQUE(DBL_PTR(_b_3920)) && (DBL_PTR(_b_3920)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_3920);
        _b_3920 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_3918;
    if (IS_ATOM_INT(_x_3918)) {
        if (256 > 0 && _x_3918 >= 0) {
            _x_3918 = _x_3918 / 256;
        }
        else {
            temp_dbl = floor((double)_x_3918 / (double)256);
            if (_x_3918 != MININT)
            _x_3918 = (long)temp_dbl;
            else
            _x_3918 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_3918, 256);
        _x_3918 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	c = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_3918)) {
        _c_3921 = (_x_3918 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _c_3921 = Dremainder(DBL_PTR(_x_3918), &temp_d);
    }
    if (!IS_ATOM_INT(_c_3921)) {
        _1 = (long)(DBL_PTR(_c_3921)->dbl);
        if (UNIQUE(DBL_PTR(_c_3921)) && (DBL_PTR(_c_3921)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_3921);
        _c_3921 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_3918;
    if (IS_ATOM_INT(_x_3918)) {
        if (256 > 0 && _x_3918 >= 0) {
            _x_3918 = _x_3918 / 256;
        }
        else {
            temp_dbl = floor((double)_x_3918 / (double)256);
            if (_x_3918 != MININT)
            _x_3918 = (long)temp_dbl;
            else
            _x_3918 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_3918, 256);
        _x_3918 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	d = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_3918)) {
        _d_3922 = (_x_3918 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _d_3922 = Dremainder(DBL_PTR(_x_3918), &temp_d);
    }
    if (!IS_ATOM_INT(_d_3922)) {
        _1 = (long)(DBL_PTR(_d_3922)->dbl);
        if (UNIQUE(DBL_PTR(_d_3922)) && (DBL_PTR(_d_3922)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_d_3922);
        _d_3922 = _1;
    }

    /** 	return {a,b,c,d}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _a_3919;
    *((int *)(_2+8)) = _b_3920;
    *((int *)(_2+12)) = _c_3921;
    *((int *)(_2+16)) = _d_3922;
    _2015 = MAKE_SEQ(_1);
    DeRef(_x_3918);
    return _2015;
    ;
}


int _17int_to_bits(int _x_3960, int _nbits_3961)
{
    int _bits_3962 = NOVALUE;
    int _mask_3963 = NOVALUE;
    int _2041 = NOVALUE;
    int _2040 = NOVALUE;
    int _2038 = NOVALUE;
    int _2035 = NOVALUE;
    int _2034 = NOVALUE;
    int _2033 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if nbits < 1 then*/

    /** 	bits = repeat(0, nbits)*/
    DeRef(_bits_3962);
    _bits_3962 = Repeat(0, 32);

    /** 	if nbits <= 32 then*/

    /** 		mask = 1*/
    DeRef(_mask_3963);
    _mask_3963 = 1;

    /** 		for i = 1 to nbits do*/
    _2033 = 32;
    {
        int _i_3970;
        _i_3970 = 1;
L1: 
        if (_i_3970 > 32){
            goto L2; // [40] 74
        }

        /** 			bits[i] = and_bits(x, mask) and 1*/
        if (IS_ATOM_INT(_x_3960) && IS_ATOM_INT(_mask_3963)) {
            {unsigned long tu;
                 tu = (unsigned long)_x_3960 & (unsigned long)_mask_3963;
                 _2034 = MAKE_UINT(tu);
            }
        }
        else {
            if (IS_ATOM_INT(_x_3960)) {
                temp_d.dbl = (double)_x_3960;
                _2034 = Dand_bits(&temp_d, DBL_PTR(_mask_3963));
            }
            else {
                if (IS_ATOM_INT(_mask_3963)) {
                    temp_d.dbl = (double)_mask_3963;
                    _2034 = Dand_bits(DBL_PTR(_x_3960), &temp_d);
                }
                else
                _2034 = Dand_bits(DBL_PTR(_x_3960), DBL_PTR(_mask_3963));
            }
        }
        if (IS_ATOM_INT(_2034)) {
            _2035 = (_2034 != 0 && 1 != 0);
        }
        else {
            temp_d.dbl = (double)1;
            _2035 = Dand(DBL_PTR(_2034), &temp_d);
        }
        DeRef(_2034);
        _2034 = NOVALUE;
        _2 = (int)SEQ_PTR(_bits_3962);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_3962 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_3970);
        _1 = *(int *)_2;
        *(int *)_2 = _2035;
        if( _1 != _2035 ){
            DeRef(_1);
        }
        _2035 = NOVALUE;

        /** 			mask *= 2*/
        _0 = _mask_3963;
        if (IS_ATOM_INT(_mask_3963) && IS_ATOM_INT(_mask_3963)) {
            _mask_3963 = _mask_3963 + _mask_3963;
            if ((long)((unsigned long)_mask_3963 + (unsigned long)HIGH_BITS) >= 0) 
            _mask_3963 = NewDouble((double)_mask_3963);
        }
        else {
            if (IS_ATOM_INT(_mask_3963)) {
                _mask_3963 = NewDouble((double)_mask_3963 + DBL_PTR(_mask_3963)->dbl);
            }
            else {
                if (IS_ATOM_INT(_mask_3963)) {
                    _mask_3963 = NewDouble(DBL_PTR(_mask_3963)->dbl + (double)_mask_3963);
                }
                else
                _mask_3963 = NewDouble(DBL_PTR(_mask_3963)->dbl + DBL_PTR(_mask_3963)->dbl);
            }
        }
        DeRef(_0);

        /** 		end for*/
        _i_3970 = _i_3970 + 1;
        goto L1; // [69] 47
L2: 
        ;
    }
    goto L3; // [74] 130

    /** 		if x < 0 then*/
    if (binary_op_a(GREATEREQ, _x_3960, 0)){
        goto L4; // [79] 94
    }

    /** 			x += power(2, nbits) -- for 2's complement bit pattern*/
    _2038 = power(2, _nbits_3961);
    _0 = _x_3960;
    if (IS_ATOM_INT(_x_3960) && IS_ATOM_INT(_2038)) {
        _x_3960 = _x_3960 + _2038;
        if ((long)((unsigned long)_x_3960 + (unsigned long)HIGH_BITS) >= 0) 
        _x_3960 = NewDouble((double)_x_3960);
    }
    else {
        if (IS_ATOM_INT(_x_3960)) {
            _x_3960 = NewDouble((double)_x_3960 + DBL_PTR(_2038)->dbl);
        }
        else {
            if (IS_ATOM_INT(_2038)) {
                _x_3960 = NewDouble(DBL_PTR(_x_3960)->dbl + (double)_2038);
            }
            else
            _x_3960 = NewDouble(DBL_PTR(_x_3960)->dbl + DBL_PTR(_2038)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_2038);
    _2038 = NOVALUE;
L4: 

    /** 		for i = 1 to nbits do*/
    _2040 = _nbits_3961;
    {
        int _i_3981;
        _i_3981 = 1;
L5: 
        if (_i_3981 > _2040){
            goto L6; // [99] 129
        }

        /** 			bits[i] = remainder(x, 2)*/
        if (IS_ATOM_INT(_x_3960)) {
            _2041 = (_x_3960 % 2);
        }
        else {
            temp_d.dbl = (double)2;
            _2041 = Dremainder(DBL_PTR(_x_3960), &temp_d);
        }
        _2 = (int)SEQ_PTR(_bits_3962);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_3962 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_3981);
        _1 = *(int *)_2;
        *(int *)_2 = _2041;
        if( _1 != _2041 ){
            DeRef(_1);
        }
        _2041 = NOVALUE;

        /** 			x = floor(x / 2)*/
        _0 = _x_3960;
        if (IS_ATOM_INT(_x_3960)) {
            _x_3960 = _x_3960 >> 1;
        }
        else {
            _1 = binary_op(DIVIDE, _x_3960, 2);
            _x_3960 = unary_op(FLOOR, _1);
            DeRef(_1);
        }
        DeRef(_0);

        /** 		end for*/
        _i_3981 = _i_3981 + 1;
        goto L5; // [124] 106
L6: 
        ;
    }
L3: 

    /** 	return bits*/
    DeRef(_x_3960);
    DeRef(_mask_3963);
    return _bits_3962;
    ;
}


int _17float64_to_atom(int _ieee64_4007)
{
    int _2049 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F64_TO_A, ieee64)*/
    _2049 = machine(47, _ieee64_4007);
    DeRefDSi(_ieee64_4007);
    return _2049;
    ;
}


int _17hex_text(int _text_4015)
{
    int _res_4016 = NOVALUE;
    int _fp_4017 = NOVALUE;
    int _div_4018 = NOVALUE;
    int _pos_4019 = NOVALUE;
    int _sign_4020 = NOVALUE;
    int _n_4021 = NOVALUE;
    int _2077 = NOVALUE;
    int _2075 = NOVALUE;
    int _2067 = NOVALUE;
    int _2066 = NOVALUE;
    int _2065 = NOVALUE;
    int _2064 = NOVALUE;
    int _2061 = NOVALUE;
    int _2058 = NOVALUE;
    int _2054 = NOVALUE;
    int _2052 = NOVALUE;
    int _2051 = NOVALUE;
    int _0, _1, _2;
    

    /** 	res = 0*/
    DeRef(_res_4016);
    _res_4016 = 0;

    /** 	fp = 0*/
    DeRef(_fp_4017);
    _fp_4017 = 0;

    /** 	div = 0*/
    _div_4018 = 0;

    /** 	sign = 0*/
    _sign_4020 = 0;

    /** 	n = 0*/
    _n_4021 = 0;

    /** 	for i = 1 to length(text) do*/
    if (IS_SEQUENCE(_text_4015)){
            _2051 = SEQ_PTR(_text_4015)->length;
    }
    else {
        _2051 = 1;
    }
    {
        int _i_4023;
        _i_4023 = 1;
L1: 
        if (_i_4023 > _2051){
            goto L2; // [39] 274
        }

        /** 		if text[i] = '_' then*/
        _2 = (int)SEQ_PTR(_text_4015);
        _2052 = (int)*(((s1_ptr)_2)->base + _i_4023);
        if (binary_op_a(NOTEQ, _2052, 95)){
            _2052 = NOVALUE;
            goto L3; // [52] 61
        }
        _2052 = NOVALUE;

        /** 			continue*/
        goto L4; // [58] 269
L3: 

        /** 		if text[i] = '#' then*/
        _2 = (int)SEQ_PTR(_text_4015);
        _2054 = (int)*(((s1_ptr)_2)->base + _i_4023);
        if (binary_op_a(NOTEQ, _2054, 35)){
            _2054 = NOVALUE;
            goto L5; // [67] 90
        }
        _2054 = NOVALUE;

        /** 			if n = 0 then*/
        if (_n_4021 != 0)
        goto L2; // [73] 274

        /** 				continue*/
        goto L4; // [79] 269
        goto L6; // [81] 89

        /** 				exit*/
        goto L2; // [86] 274
L6: 
L5: 

        /** 		if text[i] = '.' then*/
        _2 = (int)SEQ_PTR(_text_4015);
        _2058 = (int)*(((s1_ptr)_2)->base + _i_4023);
        if (binary_op_a(NOTEQ, _2058, 46)){
            _2058 = NOVALUE;
            goto L7; // [96] 126
        }
        _2058 = NOVALUE;

        /** 			if div = 0 then*/
        if (_div_4018 != 0)
        goto L2; // [102] 274

        /** 				div = 1*/
        _div_4018 = 1;

        /** 				continue*/
        goto L4; // [115] 269
        goto L8; // [117] 125

        /** 				exit*/
        goto L2; // [122] 274
L8: 
L7: 

        /** 		if text[i] = '-' then*/
        _2 = (int)SEQ_PTR(_text_4015);
        _2061 = (int)*(((s1_ptr)_2)->base + _i_4023);
        if (binary_op_a(NOTEQ, _2061, 45)){
            _2061 = NOVALUE;
            goto L9; // [132] 174
        }
        _2061 = NOVALUE;

        /** 			if sign = 0 and n = 0 then*/
        _2064 = (_sign_4020 == 0);
        if (_2064 == 0) {
            goto L2; // [142] 274
        }
        _2066 = (_n_4021 == 0);
        if (_2066 == 0)
        {
            DeRef(_2066);
            _2066 = NOVALUE;
            goto L2; // [151] 274
        }
        else{
            DeRef(_2066);
            _2066 = NOVALUE;
        }

        /** 				sign = -1*/
        _sign_4020 = -1;

        /** 				continue*/
        goto L4; // [163] 269
        goto LA; // [165] 173

        /** 				exit*/
        goto L2; // [170] 274
LA: 
L9: 

        /** 		pos = eu:find(text[i], "0123456789abcdefABCDEF")*/
        _2 = (int)SEQ_PTR(_text_4015);
        _2067 = (int)*(((s1_ptr)_2)->base + _i_4023);
        _pos_4019 = find_from(_2067, _2068, 1);
        _2067 = NOVALUE;

        /** 		if pos = 0 then*/
        if (_pos_4019 != 0)
        goto LB; // [189] 198

        /** 			exit*/
        goto L2; // [195] 274
LB: 

        /** 		if pos > 16 then*/
        if (_pos_4019 <= 16)
        goto LC; // [200] 213

        /** 			pos -= 6*/
        _pos_4019 = _pos_4019 - 6;
LC: 

        /** 		pos -= 1*/
        _pos_4019 = _pos_4019 - 1;

        /** 		if div = 0 then*/
        if (_div_4018 != 0)
        goto LD; // [223] 240

        /** 			res = res * 16 + pos*/
        if (IS_ATOM_INT(_res_4016)) {
            if (_res_4016 == (short)_res_4016)
            _2075 = _res_4016 * 16;
            else
            _2075 = NewDouble(_res_4016 * (double)16);
        }
        else {
            _2075 = NewDouble(DBL_PTR(_res_4016)->dbl * (double)16);
        }
        DeRef(_res_4016);
        if (IS_ATOM_INT(_2075)) {
            _res_4016 = _2075 + _pos_4019;
            if ((long)((unsigned long)_res_4016 + (unsigned long)HIGH_BITS) >= 0) 
            _res_4016 = NewDouble((double)_res_4016);
        }
        else {
            _res_4016 = NewDouble(DBL_PTR(_2075)->dbl + (double)_pos_4019);
        }
        DeRef(_2075);
        _2075 = NOVALUE;
        goto LE; // [237] 259
LD: 

        /** 		    fp = fp * 16 + pos*/
        if (IS_ATOM_INT(_fp_4017)) {
            if (_fp_4017 == (short)_fp_4017)
            _2077 = _fp_4017 * 16;
            else
            _2077 = NewDouble(_fp_4017 * (double)16);
        }
        else {
            _2077 = NewDouble(DBL_PTR(_fp_4017)->dbl * (double)16);
        }
        DeRef(_fp_4017);
        if (IS_ATOM_INT(_2077)) {
            _fp_4017 = _2077 + _pos_4019;
            if ((long)((unsigned long)_fp_4017 + (unsigned long)HIGH_BITS) >= 0) 
            _fp_4017 = NewDouble((double)_fp_4017);
        }
        else {
            _fp_4017 = NewDouble(DBL_PTR(_2077)->dbl + (double)_pos_4019);
        }
        DeRef(_2077);
        _2077 = NOVALUE;

        /** 		    div += 1*/
        _div_4018 = _div_4018 + 1;
LE: 

        /** 		n += 1*/
        _n_4021 = _n_4021 + 1;

        /** 	end for*/
L4: 
        _i_4023 = _i_4023 + 1;
        goto L1; // [269] 46
L2: 
        ;
    }

    /** 	while div > 1 do*/
LF: 
    if (_div_4018 <= 1)
    goto L10; // [279] 302

    /** 		fp /= 16*/
    _0 = _fp_4017;
    if (IS_ATOM_INT(_fp_4017)) {
        _fp_4017 = (_fp_4017 % 16) ? NewDouble((double)_fp_4017 / 16) : (_fp_4017 / 16);
    }
    else {
        _fp_4017 = NewDouble(DBL_PTR(_fp_4017)->dbl / (double)16);
    }
    DeRef(_0);

    /** 		div -= 1*/
    _div_4018 = _div_4018 - 1;

    /** 	end while*/
    goto LF; // [299] 279
L10: 

    /** 	res += fp*/
    _0 = _res_4016;
    if (IS_ATOM_INT(_res_4016) && IS_ATOM_INT(_fp_4017)) {
        _res_4016 = _res_4016 + _fp_4017;
        if ((long)((unsigned long)_res_4016 + (unsigned long)HIGH_BITS) >= 0) 
        _res_4016 = NewDouble((double)_res_4016);
    }
    else {
        if (IS_ATOM_INT(_res_4016)) {
            _res_4016 = NewDouble((double)_res_4016 + DBL_PTR(_fp_4017)->dbl);
        }
        else {
            if (IS_ATOM_INT(_fp_4017)) {
                _res_4016 = NewDouble(DBL_PTR(_res_4016)->dbl + (double)_fp_4017);
            }
            else
            _res_4016 = NewDouble(DBL_PTR(_res_4016)->dbl + DBL_PTR(_fp_4017)->dbl);
        }
    }
    DeRef(_0);

    /** 	if sign != 0 then*/
    if (_sign_4020 == 0)
    goto L11; // [310] 320

    /** 		res = -res*/
    _0 = _res_4016;
    if (IS_ATOM_INT(_res_4016)) {
        if ((unsigned long)_res_4016 == 0xC0000000)
        _res_4016 = (int)NewDouble((double)-0xC0000000);
        else
        _res_4016 = - _res_4016;
    }
    else {
        _res_4016 = unary_op(UMINUS, _res_4016);
    }
    DeRef(_0);
L11: 

    /** 	return res*/
    DeRefDS(_text_4015);
    DeRef(_fp_4017);
    DeRef(_2064);
    _2064 = NOVALUE;
    return _res_4016;
    ;
}


int _17to_number(int _text_in_4089, int _return_bad_pos_4090)
{
    int _lDotFound_4091 = NOVALUE;
    int _lSignFound_4092 = NOVALUE;
    int _lCharValue_4093 = NOVALUE;
    int _lBadPos_4094 = NOVALUE;
    int _lLeftSize_4095 = NOVALUE;
    int _lRightSize_4096 = NOVALUE;
    int _lLeftValue_4097 = NOVALUE;
    int _lRightValue_4098 = NOVALUE;
    int _lBase_4099 = NOVALUE;
    int _lPercent_4100 = NOVALUE;
    int _lResult_4101 = NOVALUE;
    int _lDigitCount_4102 = NOVALUE;
    int _lCurrencyFound_4103 = NOVALUE;
    int _lLastDigit_4104 = NOVALUE;
    int _lChar_4105 = NOVALUE;
    int _2172 = NOVALUE;
    int _2171 = NOVALUE;
    int _2164 = NOVALUE;
    int _2162 = NOVALUE;
    int _2161 = NOVALUE;
    int _2156 = NOVALUE;
    int _2155 = NOVALUE;
    int _2154 = NOVALUE;
    int _2153 = NOVALUE;
    int _2152 = NOVALUE;
    int _2151 = NOVALUE;
    int _2147 = NOVALUE;
    int _2143 = NOVALUE;
    int _2134 = NOVALUE;
    int _2122 = NOVALUE;
    int _2121 = NOVALUE;
    int _2115 = NOVALUE;
    int _2113 = NOVALUE;
    int _2107 = NOVALUE;
    int _2106 = NOVALUE;
    int _2105 = NOVALUE;
    int _2104 = NOVALUE;
    int _2103 = NOVALUE;
    int _2102 = NOVALUE;
    int _2101 = NOVALUE;
    int _2100 = NOVALUE;
    int _2099 = NOVALUE;
    int _2092 = NOVALUE;
    int _2091 = NOVALUE;
    int _2090 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lDotFound = 0*/
    _lDotFound_4091 = 0;

    /** 	integer lSignFound = 2*/
    _lSignFound_4092 = 2;

    /** 	integer lBadPos = 0*/
    _lBadPos_4094 = 0;

    /** 	atom    lLeftSize = 0*/
    DeRef(_lLeftSize_4095);
    _lLeftSize_4095 = 0;

    /** 	atom    lRightSize = 1*/
    DeRef(_lRightSize_4096);
    _lRightSize_4096 = 1;

    /** 	atom    lLeftValue = 0*/
    DeRef(_lLeftValue_4097);
    _lLeftValue_4097 = 0;

    /** 	atom    lRightValue = 0*/
    DeRef(_lRightValue_4098);
    _lRightValue_4098 = 0;

    /** 	integer lBase = 10*/
    _lBase_4099 = 10;

    /** 	integer lPercent = 1*/
    _lPercent_4100 = 1;

    /** 	integer lDigitCount = 0*/
    _lDigitCount_4102 = 0;

    /** 	integer lCurrencyFound = 0*/
    _lCurrencyFound_4103 = 0;

    /** 	integer lLastDigit = 0*/
    _lLastDigit_4104 = 0;

    /** 	for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_4089)){
            _2090 = SEQ_PTR(_text_in_4089)->length;
    }
    else {
        _2090 = 1;
    }
    {
        int _i_4107;
        _i_4107 = 1;
L1: 
        if (_i_4107 > _2090){
            goto L2; // [88] 754
        }

        /** 		if not integer(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_4089);
        _2091 = (int)*(((s1_ptr)_2)->base + _i_4107);
        if (IS_ATOM_INT(_2091))
        _2092 = 1;
        else if (IS_ATOM_DBL(_2091))
        _2092 = IS_ATOM_INT(DoubleToInt(_2091));
        else
        _2092 = 0;
        _2091 = NOVALUE;
        if (_2092 != 0)
        goto L3; // [104] 112
        _2092 = NOVALUE;

        /** 			exit*/
        goto L2; // [109] 754
L3: 

        /** 		lChar = text_in[i]*/
        _2 = (int)SEQ_PTR(_text_in_4089);
        _lChar_4105 = (int)*(((s1_ptr)_2)->base + _i_4107);
        if (!IS_ATOM_INT(_lChar_4105))
        _lChar_4105 = (long)DBL_PTR(_lChar_4105)->dbl;

        /** 		switch lChar do*/
        _0 = _lChar_4105;
        switch ( _0 ){ 

            /** 			case '-' then*/
            case 45:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_4092 != 2)
            goto L4; // [133] 154

            /** 					lSignFound = -1*/
            _lSignFound_4092 = -1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_4104 = _lDigitCount_4102;
            goto L5; // [151] 736
L4: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [162] 736

            /** 			case '+' then*/
            case 43:

            /** 				if lSignFound = 2 then*/
            if (_lSignFound_4092 != 2)
            goto L6; // [170] 191

            /** 					lSignFound = 1*/
            _lSignFound_4092 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_4104 = _lDigitCount_4102;
            goto L5; // [188] 736
L6: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [199] 736

            /** 			case '#' then*/
            case 35:

            /** 				if lDigitCount = 0 and lBase = 10 then*/
            _2099 = (_lDigitCount_4102 == 0);
            if (_2099 == 0) {
                goto L7; // [211] 233
            }
            _2101 = (_lBase_4099 == 10);
            if (_2101 == 0)
            {
                DeRef(_2101);
                _2101 = NOVALUE;
                goto L7; // [220] 233
            }
            else{
                DeRef(_2101);
                _2101 = NOVALUE;
            }

            /** 					lBase = 16*/
            _lBase_4099 = 16;
            goto L5; // [230] 736
L7: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [241] 736

            /** 			case '@' then*/
            case 64:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _2102 = (_lDigitCount_4102 == 0);
            if (_2102 == 0) {
                goto L8; // [253] 275
            }
            _2104 = (_lBase_4099 == 10);
            if (_2104 == 0)
            {
                DeRef(_2104);
                _2104 = NOVALUE;
                goto L8; // [262] 275
            }
            else{
                DeRef(_2104);
                _2104 = NOVALUE;
            }

            /** 					lBase = 8*/
            _lBase_4099 = 8;
            goto L5; // [272] 736
L8: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [283] 736

            /** 			case '!' then*/
            case 33:

            /** 				if lDigitCount = 0  and lBase = 10 then*/
            _2105 = (_lDigitCount_4102 == 0);
            if (_2105 == 0) {
                goto L9; // [295] 317
            }
            _2107 = (_lBase_4099 == 10);
            if (_2107 == 0)
            {
                DeRef(_2107);
                _2107 = NOVALUE;
                goto L9; // [304] 317
            }
            else{
                DeRef(_2107);
                _2107 = NOVALUE;
            }

            /** 					lBase = 2*/
            _lBase_4099 = 2;
            goto L5; // [314] 736
L9: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [325] 736

            /** 			case '$', '�', '�', '�', '�' then*/
            case 36:
            case 163:
            case 164:
            case 165:
            case 128:

            /** 				if lCurrencyFound = 0 then*/
            if (_lCurrencyFound_4103 != 0)
            goto LA; // [341] 362

            /** 					lCurrencyFound = 1*/
            _lCurrencyFound_4103 = 1;

            /** 					lLastDigit = lDigitCount*/
            _lLastDigit_4104 = _lDigitCount_4102;
            goto L5; // [359] 736
LA: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [370] 736

            /** 			case '_' then -- grouping character*/
            case 95:

            /** 				if lDigitCount = 0 or lLastDigit != 0 then*/
            _2113 = (_lDigitCount_4102 == 0);
            if (_2113 != 0) {
                goto LB; // [382] 395
            }
            _2115 = (_lLastDigit_4104 != 0);
            if (_2115 == 0)
            {
                DeRef(_2115);
                _2115 = NOVALUE;
                goto L5; // [391] 736
            }
            else{
                DeRef(_2115);
                _2115 = NOVALUE;
            }
LB: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [403] 736

            /** 			case '.', ',' then*/
            case 46:
            case 44:

            /** 				if lLastDigit = 0 then*/
            if (_lLastDigit_4104 != 0)
            goto LC; // [413] 456

            /** 					if decimal_mark = lChar then*/
            if (46 != _lChar_4105)
            goto L5; // [421] 736

            /** 						if lDotFound = 0 then*/
            if (_lDotFound_4091 != 0)
            goto LD; // [427] 441

            /** 							lDotFound = 1*/
            _lDotFound_4091 = 1;
            goto L5; // [438] 736
LD: 

            /** 							lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [449] 736
            goto L5; // [453] 736
LC: 

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [464] 736

            /** 			case '%' then*/
            case 37:

            /** 				lLastDigit = lDigitCount*/
            _lLastDigit_4104 = _lDigitCount_4102;

            /** 				if lPercent = 1 then*/
            if (_lPercent_4100 != 1)
            goto LE; // [479] 493

            /** 					lPercent = 100*/
            _lPercent_4100 = 100;
            goto L5; // [490] 736
LE: 

            /** 					if text_in[i-1] = '%' then*/
            _2121 = _i_4107 - 1;
            _2 = (int)SEQ_PTR(_text_in_4089);
            _2122 = (int)*(((s1_ptr)_2)->base + _2121);
            if (binary_op_a(NOTEQ, _2122, 37)){
                _2122 = NOVALUE;
                goto LF; // [503] 520
            }
            _2122 = NOVALUE;

            /** 						lPercent *= 10 -- Yes ten not one hundred.*/
            _lPercent_4100 = _lPercent_4100 * 10;
            goto L5; // [517] 736
LF: 

            /** 						lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [529] 736

            /** 			case '\t', ' ', #A0 then*/
            case 9:
            case 32:
            case 160:

            /** 				if lDigitCount = 0 then*/
            if (_lDigitCount_4102 != 0)
            goto L10; // [541] 548
            goto L5; // [545] 736
L10: 

            /** 					lLastDigit = i*/
            _lLastDigit_4104 = _i_4107;
            goto L5; // [556] 736

            /** 			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',*/
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 97:
            case 98:
            case 99:
            case 100:
            case 101:
            case 102:

            /** 	            lCharValue = find(lChar, vDigits) - 1*/
            _2134 = find_from(_lChar_4105, _17vDigits_4076, 1);
            _lCharValue_4093 = _2134 - 1;
            _2134 = NOVALUE;

            /** 	            if lCharValue > 15 then*/
            if (_lCharValue_4093 <= 15)
            goto L11; // [619] 632

            /** 	            	lCharValue -= 6*/
            _lCharValue_4093 = _lCharValue_4093 - 6;
L11: 

            /** 	            if lCharValue >= lBase then*/
            if (_lCharValue_4093 < _lBase_4099)
            goto L12; // [634] 648

            /** 	                lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [645] 736
L12: 

            /** 	            elsif lLastDigit != 0 then  -- shouldn't be any more digits*/
            if (_lLastDigit_4104 == 0)
            goto L13; // [650] 664

            /** 					lBadPos = i*/
            _lBadPos_4094 = _i_4107;
            goto L5; // [661] 736
L13: 

            /** 				elsif lDotFound = 1 then*/
            if (_lDotFound_4091 != 1)
            goto L14; // [666] 697

            /** 					lRightSize *= lBase*/
            _0 = _lRightSize_4096;
            if (IS_ATOM_INT(_lRightSize_4096)) {
                if (_lRightSize_4096 == (short)_lRightSize_4096 && _lBase_4099 <= INT15 && _lBase_4099 >= -INT15)
                _lRightSize_4096 = _lRightSize_4096 * _lBase_4099;
                else
                _lRightSize_4096 = NewDouble(_lRightSize_4096 * (double)_lBase_4099);
            }
            else {
                _lRightSize_4096 = NewDouble(DBL_PTR(_lRightSize_4096)->dbl * (double)_lBase_4099);
            }
            DeRef(_0);

            /** 					lRightValue = (lRightValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lRightValue_4098)) {
                if (_lRightValue_4098 == (short)_lRightValue_4098 && _lBase_4099 <= INT15 && _lBase_4099 >= -INT15)
                _2143 = _lRightValue_4098 * _lBase_4099;
                else
                _2143 = NewDouble(_lRightValue_4098 * (double)_lBase_4099);
            }
            else {
                _2143 = NewDouble(DBL_PTR(_lRightValue_4098)->dbl * (double)_lBase_4099);
            }
            DeRef(_lRightValue_4098);
            if (IS_ATOM_INT(_2143)) {
                _lRightValue_4098 = _2143 + _lCharValue_4093;
                if ((long)((unsigned long)_lRightValue_4098 + (unsigned long)HIGH_BITS) >= 0) 
                _lRightValue_4098 = NewDouble((double)_lRightValue_4098);
            }
            else {
                _lRightValue_4098 = NewDouble(DBL_PTR(_2143)->dbl + (double)_lCharValue_4093);
            }
            DeRef(_2143);
            _2143 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_4102 = _lDigitCount_4102 + 1;
            goto L5; // [694] 736
L14: 

            /** 					lLeftSize += 1*/
            _0 = _lLeftSize_4095;
            if (IS_ATOM_INT(_lLeftSize_4095)) {
                _lLeftSize_4095 = _lLeftSize_4095 + 1;
                if (_lLeftSize_4095 > MAXINT){
                    _lLeftSize_4095 = NewDouble((double)_lLeftSize_4095);
                }
            }
            else
            _lLeftSize_4095 = binary_op(PLUS, 1, _lLeftSize_4095);
            DeRef(_0);

            /** 					lLeftValue = (lLeftValue * lBase) + lCharValue*/
            if (IS_ATOM_INT(_lLeftValue_4097)) {
                if (_lLeftValue_4097 == (short)_lLeftValue_4097 && _lBase_4099 <= INT15 && _lBase_4099 >= -INT15)
                _2147 = _lLeftValue_4097 * _lBase_4099;
                else
                _2147 = NewDouble(_lLeftValue_4097 * (double)_lBase_4099);
            }
            else {
                _2147 = NewDouble(DBL_PTR(_lLeftValue_4097)->dbl * (double)_lBase_4099);
            }
            DeRef(_lLeftValue_4097);
            if (IS_ATOM_INT(_2147)) {
                _lLeftValue_4097 = _2147 + _lCharValue_4093;
                if ((long)((unsigned long)_lLeftValue_4097 + (unsigned long)HIGH_BITS) >= 0) 
                _lLeftValue_4097 = NewDouble((double)_lLeftValue_4097);
            }
            else {
                _lLeftValue_4097 = NewDouble(DBL_PTR(_2147)->dbl + (double)_lCharValue_4093);
            }
            DeRef(_2147);
            _2147 = NOVALUE;

            /** 					lDigitCount += 1*/
            _lDigitCount_4102 = _lDigitCount_4102 + 1;
            goto L5; // [722] 736

            /** 			case else*/
            default:

            /** 				lBadPos = i*/
            _lBadPos_4094 = _i_4107;
        ;}L5: 

        /** 		if lBadPos != 0 then*/
        if (_lBadPos_4094 == 0)
        goto L15; // [738] 747

        /** 			exit*/
        goto L2; // [744] 754
L15: 

        /** 	end for*/
        _i_4107 = _i_4107 + 1;
        goto L1; // [749] 95
L2: 
        ;
    }

    /** 	if lBadPos = 0 and lDigitCount = 0 then*/
    _2151 = (_lBadPos_4094 == 0);
    if (_2151 == 0) {
        goto L16; // [760] 780
    }
    _2153 = (_lDigitCount_4102 == 0);
    if (_2153 == 0)
    {
        DeRef(_2153);
        _2153 = NOVALUE;
        goto L16; // [769] 780
    }
    else{
        DeRef(_2153);
        _2153 = NOVALUE;
    }

    /** 		lBadPos = 1*/
    _lBadPos_4094 = 1;
L16: 

    /** 	if return_bad_pos = 0 and lBadPos != 0 then*/
    _2154 = (_return_bad_pos_4090 == 0);
    if (_2154 == 0) {
        goto L17; // [786] 805
    }
    _2156 = (_lBadPos_4094 != 0);
    if (_2156 == 0)
    {
        DeRef(_2156);
        _2156 = NOVALUE;
        goto L17; // [795] 805
    }
    else{
        DeRef(_2156);
        _2156 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_text_in_4089);
    DeRef(_lLeftSize_4095);
    DeRef(_lRightSize_4096);
    DeRef(_lLeftValue_4097);
    DeRef(_lRightValue_4098);
    DeRef(_lResult_4101);
    DeRef(_2099);
    _2099 = NOVALUE;
    DeRef(_2102);
    _2102 = NOVALUE;
    DeRef(_2105);
    _2105 = NOVALUE;
    DeRef(_2113);
    _2113 = NOVALUE;
    DeRef(_2121);
    _2121 = NOVALUE;
    DeRef(_2151);
    _2151 = NOVALUE;
    DeRef(_2154);
    _2154 = NOVALUE;
    return 0;
L17: 

    /** 	if lRightValue = 0 then*/
    if (binary_op_a(NOTEQ, _lRightValue_4098, 0)){
        goto L18; // [807] 835
    }

    /** 	    if lPercent != 1 then*/
    if (_lPercent_4100 == 1)
    goto L19; // [813] 826

    /** 			lResult = (lLeftValue / lPercent)*/
    DeRef(_lResult_4101);
    if (IS_ATOM_INT(_lLeftValue_4097)) {
        _lResult_4101 = (_lLeftValue_4097 % _lPercent_4100) ? NewDouble((double)_lLeftValue_4097 / _lPercent_4100) : (_lLeftValue_4097 / _lPercent_4100);
    }
    else {
        _lResult_4101 = NewDouble(DBL_PTR(_lLeftValue_4097)->dbl / (double)_lPercent_4100);
    }
    goto L1A; // [823] 870
L19: 

    /** 	        lResult = lLeftValue*/
    Ref(_lLeftValue_4097);
    DeRef(_lResult_4101);
    _lResult_4101 = _lLeftValue_4097;
    goto L1A; // [832] 870
L18: 

    /** 	    if lPercent != 1 then*/
    if (_lPercent_4100 == 1)
    goto L1B; // [837] 858

    /** 	        lResult = (lLeftValue  + (lRightValue / (lRightSize))) / lPercent*/
    if (IS_ATOM_INT(_lRightValue_4098) && IS_ATOM_INT(_lRightSize_4096)) {
        _2161 = (_lRightValue_4098 % _lRightSize_4096) ? NewDouble((double)_lRightValue_4098 / _lRightSize_4096) : (_lRightValue_4098 / _lRightSize_4096);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_4098)) {
            _2161 = NewDouble((double)_lRightValue_4098 / DBL_PTR(_lRightSize_4096)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_4096)) {
                _2161 = NewDouble(DBL_PTR(_lRightValue_4098)->dbl / (double)_lRightSize_4096);
            }
            else
            _2161 = NewDouble(DBL_PTR(_lRightValue_4098)->dbl / DBL_PTR(_lRightSize_4096)->dbl);
        }
    }
    if (IS_ATOM_INT(_lLeftValue_4097) && IS_ATOM_INT(_2161)) {
        _2162 = _lLeftValue_4097 + _2161;
        if ((long)((unsigned long)_2162 + (unsigned long)HIGH_BITS) >= 0) 
        _2162 = NewDouble((double)_2162);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_4097)) {
            _2162 = NewDouble((double)_lLeftValue_4097 + DBL_PTR(_2161)->dbl);
        }
        else {
            if (IS_ATOM_INT(_2161)) {
                _2162 = NewDouble(DBL_PTR(_lLeftValue_4097)->dbl + (double)_2161);
            }
            else
            _2162 = NewDouble(DBL_PTR(_lLeftValue_4097)->dbl + DBL_PTR(_2161)->dbl);
        }
    }
    DeRef(_2161);
    _2161 = NOVALUE;
    DeRef(_lResult_4101);
    if (IS_ATOM_INT(_2162)) {
        _lResult_4101 = (_2162 % _lPercent_4100) ? NewDouble((double)_2162 / _lPercent_4100) : (_2162 / _lPercent_4100);
    }
    else {
        _lResult_4101 = NewDouble(DBL_PTR(_2162)->dbl / (double)_lPercent_4100);
    }
    DeRef(_2162);
    _2162 = NOVALUE;
    goto L1C; // [855] 869
L1B: 

    /** 	        lResult = lLeftValue + (lRightValue / lRightSize)*/
    if (IS_ATOM_INT(_lRightValue_4098) && IS_ATOM_INT(_lRightSize_4096)) {
        _2164 = (_lRightValue_4098 % _lRightSize_4096) ? NewDouble((double)_lRightValue_4098 / _lRightSize_4096) : (_lRightValue_4098 / _lRightSize_4096);
    }
    else {
        if (IS_ATOM_INT(_lRightValue_4098)) {
            _2164 = NewDouble((double)_lRightValue_4098 / DBL_PTR(_lRightSize_4096)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lRightSize_4096)) {
                _2164 = NewDouble(DBL_PTR(_lRightValue_4098)->dbl / (double)_lRightSize_4096);
            }
            else
            _2164 = NewDouble(DBL_PTR(_lRightValue_4098)->dbl / DBL_PTR(_lRightSize_4096)->dbl);
        }
    }
    DeRef(_lResult_4101);
    if (IS_ATOM_INT(_lLeftValue_4097) && IS_ATOM_INT(_2164)) {
        _lResult_4101 = _lLeftValue_4097 + _2164;
        if ((long)((unsigned long)_lResult_4101 + (unsigned long)HIGH_BITS) >= 0) 
        _lResult_4101 = NewDouble((double)_lResult_4101);
    }
    else {
        if (IS_ATOM_INT(_lLeftValue_4097)) {
            _lResult_4101 = NewDouble((double)_lLeftValue_4097 + DBL_PTR(_2164)->dbl);
        }
        else {
            if (IS_ATOM_INT(_2164)) {
                _lResult_4101 = NewDouble(DBL_PTR(_lLeftValue_4097)->dbl + (double)_2164);
            }
            else
            _lResult_4101 = NewDouble(DBL_PTR(_lLeftValue_4097)->dbl + DBL_PTR(_2164)->dbl);
        }
    }
    DeRef(_2164);
    _2164 = NOVALUE;
L1C: 
L1A: 

    /** 	if lSignFound < 0 then*/
    if (_lSignFound_4092 >= 0)
    goto L1D; // [872] 884

    /** 		lResult = -lResult*/
    _0 = _lResult_4101;
    if (IS_ATOM_INT(_lResult_4101)) {
        if ((unsigned long)_lResult_4101 == 0xC0000000)
        _lResult_4101 = (int)NewDouble((double)-0xC0000000);
        else
        _lResult_4101 = - _lResult_4101;
    }
    else {
        _lResult_4101 = unary_op(UMINUS, _lResult_4101);
    }
    DeRef(_0);
L1D: 

    /** 	if return_bad_pos = 0 then*/
    if (_return_bad_pos_4090 != 0)
    goto L1E; // [886] 899

    /** 		return lResult*/
    DeRefDS(_text_in_4089);
    DeRef(_lLeftSize_4095);
    DeRef(_lRightSize_4096);
    DeRef(_lLeftValue_4097);
    DeRef(_lRightValue_4098);
    DeRef(_2099);
    _2099 = NOVALUE;
    DeRef(_2102);
    _2102 = NOVALUE;
    DeRef(_2105);
    _2105 = NOVALUE;
    DeRef(_2113);
    _2113 = NOVALUE;
    DeRef(_2121);
    _2121 = NOVALUE;
    DeRef(_2151);
    _2151 = NOVALUE;
    DeRef(_2154);
    _2154 = NOVALUE;
    return _lResult_4101;
L1E: 

    /** 	if return_bad_pos = -1 then*/
    if (_return_bad_pos_4090 != -1)
    goto L1F; // [901] 934

    /** 		if lBadPos = 0 then*/
    if (_lBadPos_4094 != 0)
    goto L20; // [907] 922

    /** 			return lResult*/
    DeRefDS(_text_in_4089);
    DeRef(_lLeftSize_4095);
    DeRef(_lRightSize_4096);
    DeRef(_lLeftValue_4097);
    DeRef(_lRightValue_4098);
    DeRef(_2099);
    _2099 = NOVALUE;
    DeRef(_2102);
    _2102 = NOVALUE;
    DeRef(_2105);
    _2105 = NOVALUE;
    DeRef(_2113);
    _2113 = NOVALUE;
    DeRef(_2121);
    _2121 = NOVALUE;
    DeRef(_2151);
    _2151 = NOVALUE;
    DeRef(_2154);
    _2154 = NOVALUE;
    return _lResult_4101;
    goto L21; // [919] 933
L20: 

    /** 			return {lBadPos}	*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _lBadPos_4094;
    _2171 = MAKE_SEQ(_1);
    DeRefDS(_text_in_4089);
    DeRef(_lLeftSize_4095);
    DeRef(_lRightSize_4096);
    DeRef(_lLeftValue_4097);
    DeRef(_lRightValue_4098);
    DeRef(_lResult_4101);
    DeRef(_2099);
    _2099 = NOVALUE;
    DeRef(_2102);
    _2102 = NOVALUE;
    DeRef(_2105);
    _2105 = NOVALUE;
    DeRef(_2113);
    _2113 = NOVALUE;
    DeRef(_2121);
    _2121 = NOVALUE;
    DeRef(_2151);
    _2151 = NOVALUE;
    DeRef(_2154);
    _2154 = NOVALUE;
    return _2171;
L21: 
L1F: 

    /** 	return {lResult, lBadPos}*/
    Ref(_lResult_4101);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_4101;
    ((int *)_2)[2] = _lBadPos_4094;
    _2172 = MAKE_SEQ(_1);
    DeRefDS(_text_in_4089);
    DeRef(_lLeftSize_4095);
    DeRef(_lRightSize_4096);
    DeRef(_lLeftValue_4097);
    DeRef(_lRightValue_4098);
    DeRef(_lResult_4101);
    DeRef(_2099);
    _2099 = NOVALUE;
    DeRef(_2102);
    _2102 = NOVALUE;
    DeRef(_2105);
    _2105 = NOVALUE;
    DeRef(_2113);
    _2113 = NOVALUE;
    DeRef(_2121);
    _2121 = NOVALUE;
    DeRef(_2151);
    _2151 = NOVALUE;
    DeRef(_2154);
    _2154 = NOVALUE;
    DeRef(_2171);
    _2171 = NOVALUE;
    return _2172;
    ;
}



// 0x3D89CC77
