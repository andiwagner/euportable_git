CC     = gcc
CFLAGS =  -DEWINDOWS -fomit-frame-pointer -c -w -fsigned-char -O2 -m32 -Ic:/develop/offEu/euphoria -ffast-math
LINKER = gcc
LFLAGS = c:/develop/offEu/euphoria/source/build/eu.a -m32 
EUTEST_SOURCES = init-.c eutest.c main-.c pretty.c sequence.c error.c types.c search.c sort.c filesys.c datetime.c dll.c machine.c memconst.c memory.c get.c io.c text.c convert.c math.c rand.c serialize.c os.c map.c eumem.c primes.c stats.c info.c cmdline.c console.c eds.c regex.c flags.c
EUTEST_OBJECTS = init-.o eutest.o main-.o pretty.o sequence.o error.o types.o search.o sort.o filesys.o datetime.o dll.o machine.o memconst.o memory.o get.o io.o text.o convert.o math.o rand.o serialize.o os.o map.o eumem.o primes.o stats.o info.o cmdline.o console.o eds.o regex.o flags.o
EUTEST_GENERATED_FILES =  init-.c init-.o main-.h eutest.c eutest.o main-.c main-.o pretty.c pretty.o sequence.c sequence.o error.c error.o types.c types.o search.c search.o sort.c sort.o filesys.c filesys.o datetime.c datetime.o dll.c dll.o machine.c machine.o memconst.c memconst.o memory.c memory.o get.c get.o io.c io.o text.c text.o convert.c convert.o math.c math.o rand.c rand.o serialize.c serialize.o os.c os.o map.c map.o eumem.c eumem.o primes.c primes.o stats.c stats.o info.c info.o cmdline.c cmdline.o console.c console.o eds.c eds.o regex.c regex.o flags.c flags.o

c:/develop/offEu/euphoria/source/build/eutest: $(EUTEST_OBJECTS) c:/develop/offEu/euphoria/source/build/eu.a 
	$(LINKER) -o c:/develop/offEu/euphoria/source/build/eutest $(EUTEST_OBJECTS)  $(LFLAGS)

.PHONY: eutest-clean eutest-clean-all

eutest-clean:
	rm -rf $(EUTEST_OBJECTS) 

eutest-clean-all: eutest-clean
	rm -rf $(EUTEST_SOURCES)  c:/develop/offEu/euphoria/source/build/eutest

%.o: %.c
	$(CC) $(CFLAGS) $*.c -o $*.o

