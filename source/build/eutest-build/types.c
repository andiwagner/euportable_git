// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _5char_test(int _test_data_460, int _char_set_461)
{
    int _lChr_462 = NOVALUE;
    int _201 = NOVALUE;
    int _200 = NOVALUE;
    int _199 = NOVALUE;
    int _198 = NOVALUE;
    int _197 = NOVALUE;
    int _196 = NOVALUE;
    int _195 = NOVALUE;
    int _194 = NOVALUE;
    int _193 = NOVALUE;
    int _192 = NOVALUE;
    int _191 = NOVALUE;
    int _188 = NOVALUE;
    int _187 = NOVALUE;
    int _186 = NOVALUE;
    int _185 = NOVALUE;
    int _183 = NOVALUE;
    int _181 = NOVALUE;
    int _180 = NOVALUE;
    int _179 = NOVALUE;
    int _178 = NOVALUE;
    int _177 = NOVALUE;
    int _176 = NOVALUE;
    int _175 = NOVALUE;
    int _174 = NOVALUE;
    int _173 = NOVALUE;
    int _172 = NOVALUE;
    int _171 = NOVALUE;
    int _170 = NOVALUE;
    int _169 = NOVALUE;
    int _168 = NOVALUE;
    int _167 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(test_data) then*/
    if (IS_ATOM_INT(_test_data_460))
    _167 = 1;
    else if (IS_ATOM_DBL(_test_data_460))
    _167 = IS_ATOM_INT(DoubleToInt(_test_data_460));
    else
    _167 = 0;
    if (_167 == 0)
    {
        _167 = NOVALUE;
        goto L1; // [8] 115
    }
    else{
        _167 = NOVALUE;
    }

    /** 		if sequence(char_set[1]) then*/
    _2 = (int)SEQ_PTR(_char_set_461);
    _168 = (int)*(((s1_ptr)_2)->base + 1);
    _169 = IS_SEQUENCE(_168);
    _168 = NOVALUE;
    if (_169 == 0)
    {
        _169 = NOVALUE;
        goto L2; // [20] 96
    }
    else{
        _169 = NOVALUE;
    }

    /** 			for j = 1 to length(char_set) do*/
    if (IS_SEQUENCE(_char_set_461)){
            _170 = SEQ_PTR(_char_set_461)->length;
    }
    else {
        _170 = 1;
    }
    {
        int _j_469;
        _j_469 = 1;
L3: 
        if (_j_469 > _170){
            goto L4; // [28] 85
        }

        /** 				if test_data >= char_set[j][1] and test_data <= char_set[j][2] then */
        _2 = (int)SEQ_PTR(_char_set_461);
        _171 = (int)*(((s1_ptr)_2)->base + _j_469);
        _2 = (int)SEQ_PTR(_171);
        _172 = (int)*(((s1_ptr)_2)->base + 1);
        _171 = NOVALUE;
        if (IS_ATOM_INT(_test_data_460) && IS_ATOM_INT(_172)) {
            _173 = (_test_data_460 >= _172);
        }
        else {
            _173 = binary_op(GREATEREQ, _test_data_460, _172);
        }
        _172 = NOVALUE;
        if (IS_ATOM_INT(_173)) {
            if (_173 == 0) {
                goto L5; // [49] 78
            }
        }
        else {
            if (DBL_PTR(_173)->dbl == 0.0) {
                goto L5; // [49] 78
            }
        }
        _2 = (int)SEQ_PTR(_char_set_461);
        _175 = (int)*(((s1_ptr)_2)->base + _j_469);
        _2 = (int)SEQ_PTR(_175);
        _176 = (int)*(((s1_ptr)_2)->base + 2);
        _175 = NOVALUE;
        if (IS_ATOM_INT(_test_data_460) && IS_ATOM_INT(_176)) {
            _177 = (_test_data_460 <= _176);
        }
        else {
            _177 = binary_op(LESSEQ, _test_data_460, _176);
        }
        _176 = NOVALUE;
        if (_177 == 0) {
            DeRef(_177);
            _177 = NOVALUE;
            goto L5; // [66] 78
        }
        else {
            if (!IS_ATOM_INT(_177) && DBL_PTR(_177)->dbl == 0.0){
                DeRef(_177);
                _177 = NOVALUE;
                goto L5; // [66] 78
            }
            DeRef(_177);
            _177 = NOVALUE;
        }
        DeRef(_177);
        _177 = NOVALUE;

        /** 					return TRUE */
        DeRef(_test_data_460);
        DeRefDS(_char_set_461);
        DeRef(_173);
        _173 = NOVALUE;
        return _5TRUE_415;
L5: 

        /** 			end for*/
        _j_469 = _j_469 + 1;
        goto L3; // [80] 35
L4: 
        ;
    }

    /** 			return FALSE*/
    DeRef(_test_data_460);
    DeRefDS(_char_set_461);
    DeRef(_173);
    _173 = NOVALUE;
    return _5FALSE_413;
    goto L6; // [93] 330
L2: 

    /** 			return find(test_data, char_set) > 0*/
    _178 = find_from(_test_data_460, _char_set_461, 1);
    _179 = (_178 > 0);
    _178 = NOVALUE;
    DeRef(_test_data_460);
    DeRefDS(_char_set_461);
    DeRef(_173);
    _173 = NOVALUE;
    return _179;
    goto L6; // [112] 330
L1: 

    /** 	elsif sequence(test_data) then*/
    _180 = IS_SEQUENCE(_test_data_460);
    if (_180 == 0)
    {
        _180 = NOVALUE;
        goto L7; // [120] 321
    }
    else{
        _180 = NOVALUE;
    }

    /** 		if length(test_data) = 0 then */
    if (IS_SEQUENCE(_test_data_460)){
            _181 = SEQ_PTR(_test_data_460)->length;
    }
    else {
        _181 = 1;
    }
    if (_181 != 0)
    goto L8; // [128] 141

    /** 			return FALSE */
    DeRef(_test_data_460);
    DeRefDS(_char_set_461);
    DeRef(_179);
    _179 = NOVALUE;
    DeRef(_173);
    _173 = NOVALUE;
    return _5FALSE_413;
L8: 

    /** 		for i = 1 to length(test_data) label "NXTCHR" do*/
    if (IS_SEQUENCE(_test_data_460)){
            _183 = SEQ_PTR(_test_data_460)->length;
    }
    else {
        _183 = 1;
    }
    {
        int _i_488;
        _i_488 = 1;
L9: 
        if (_i_488 > _183){
            goto LA; // [146] 310
        }

        /** 			if sequence(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_460);
        _185 = (int)*(((s1_ptr)_2)->base + _i_488);
        _186 = IS_SEQUENCE(_185);
        _185 = NOVALUE;
        if (_186 == 0)
        {
            _186 = NOVALUE;
            goto LB; // [162] 174
        }
        else{
            _186 = NOVALUE;
        }

        /** 				return FALSE*/
        DeRef(_test_data_460);
        DeRefDS(_char_set_461);
        DeRef(_179);
        _179 = NOVALUE;
        DeRef(_173);
        _173 = NOVALUE;
        return _5FALSE_413;
LB: 

        /** 			if not integer(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_460);
        _187 = (int)*(((s1_ptr)_2)->base + _i_488);
        if (IS_ATOM_INT(_187))
        _188 = 1;
        else if (IS_ATOM_DBL(_187))
        _188 = IS_ATOM_INT(DoubleToInt(_187));
        else
        _188 = 0;
        _187 = NOVALUE;
        if (_188 != 0)
        goto LC; // [183] 195
        _188 = NOVALUE;

        /** 				return FALSE*/
        DeRef(_test_data_460);
        DeRefDS(_char_set_461);
        DeRef(_179);
        _179 = NOVALUE;
        DeRef(_173);
        _173 = NOVALUE;
        return _5FALSE_413;
LC: 

        /** 			lChr = test_data[i]*/
        _2 = (int)SEQ_PTR(_test_data_460);
        _lChr_462 = (int)*(((s1_ptr)_2)->base + _i_488);
        if (!IS_ATOM_INT(_lChr_462)){
            _lChr_462 = (long)DBL_PTR(_lChr_462)->dbl;
        }

        /** 			if sequence(char_set[1]) then*/
        _2 = (int)SEQ_PTR(_char_set_461);
        _191 = (int)*(((s1_ptr)_2)->base + 1);
        _192 = IS_SEQUENCE(_191);
        _191 = NOVALUE;
        if (_192 == 0)
        {
            _192 = NOVALUE;
            goto LD; // [214] 278
        }
        else{
            _192 = NOVALUE;
        }

        /** 				for j = 1 to length(char_set) do*/
        if (IS_SEQUENCE(_char_set_461)){
                _193 = SEQ_PTR(_char_set_461)->length;
        }
        else {
            _193 = 1;
        }
        {
            int _j_503;
            _j_503 = 1;
LE: 
            if (_j_503 > _193){
                goto LF; // [222] 275
            }

            /** 					if lChr >= char_set[j][1] and lChr <= char_set[j][2] then*/
            _2 = (int)SEQ_PTR(_char_set_461);
            _194 = (int)*(((s1_ptr)_2)->base + _j_503);
            _2 = (int)SEQ_PTR(_194);
            _195 = (int)*(((s1_ptr)_2)->base + 1);
            _194 = NOVALUE;
            if (IS_ATOM_INT(_195)) {
                _196 = (_lChr_462 >= _195);
            }
            else {
                _196 = binary_op(GREATEREQ, _lChr_462, _195);
            }
            _195 = NOVALUE;
            if (IS_ATOM_INT(_196)) {
                if (_196 == 0) {
                    goto L10; // [243] 268
                }
            }
            else {
                if (DBL_PTR(_196)->dbl == 0.0) {
                    goto L10; // [243] 268
                }
            }
            _2 = (int)SEQ_PTR(_char_set_461);
            _198 = (int)*(((s1_ptr)_2)->base + _j_503);
            _2 = (int)SEQ_PTR(_198);
            _199 = (int)*(((s1_ptr)_2)->base + 2);
            _198 = NOVALUE;
            if (IS_ATOM_INT(_199)) {
                _200 = (_lChr_462 <= _199);
            }
            else {
                _200 = binary_op(LESSEQ, _lChr_462, _199);
            }
            _199 = NOVALUE;
            if (_200 == 0) {
                DeRef(_200);
                _200 = NOVALUE;
                goto L10; // [260] 268
            }
            else {
                if (!IS_ATOM_INT(_200) && DBL_PTR(_200)->dbl == 0.0){
                    DeRef(_200);
                    _200 = NOVALUE;
                    goto L10; // [260] 268
                }
                DeRef(_200);
                _200 = NOVALUE;
            }
            DeRef(_200);
            _200 = NOVALUE;

            /** 						continue "NXTCHR" */
            goto L11; // [265] 305
L10: 

            /** 				end for*/
            _j_503 = _j_503 + 1;
            goto LE; // [270] 229
LF: 
            ;
        }
        goto L12; // [275] 295
LD: 

        /** 				if find(lChr, char_set) > 0 then*/
        _201 = find_from(_lChr_462, _char_set_461, 1);
        if (_201 <= 0)
        goto L13; // [285] 294

        /** 					continue "NXTCHR"*/
        goto L11; // [291] 305
L13: 
L12: 

        /** 			return FALSE*/
        DeRef(_test_data_460);
        DeRefDS(_char_set_461);
        DeRef(_179);
        _179 = NOVALUE;
        DeRef(_173);
        _173 = NOVALUE;
        DeRef(_196);
        _196 = NOVALUE;
        return _5FALSE_413;

        /** 		end for*/
L11: 
        _i_488 = _i_488 + 1;
        goto L9; // [305] 153
LA: 
        ;
    }

    /** 		return TRUE*/
    DeRef(_test_data_460);
    DeRefDS(_char_set_461);
    DeRef(_179);
    _179 = NOVALUE;
    DeRef(_173);
    _173 = NOVALUE;
    DeRef(_196);
    _196 = NOVALUE;
    return _5TRUE_415;
    goto L6; // [318] 330
L7: 

    /** 		return FALSE*/
    DeRef(_test_data_460);
    DeRefDS(_char_set_461);
    DeRef(_179);
    _179 = NOVALUE;
    DeRef(_173);
    _173 = NOVALUE;
    DeRef(_196);
    _196 = NOVALUE;
    return _5FALSE_413;
L6: 
    ;
}


void _5set_default_charsets()
{
    int _267 = NOVALUE;
    int _265 = NOVALUE;
    int _264 = NOVALUE;
    int _262 = NOVALUE;
    int _259 = NOVALUE;
    int _258 = NOVALUE;
    int _257 = NOVALUE;
    int _256 = NOVALUE;
    int _254 = NOVALUE;
    int _252 = NOVALUE;
    int _242 = NOVALUE;
    int _235 = NOVALUE;
    int _231 = NOVALUE;
    int _222 = NOVALUE;
    int _220 = NOVALUE;
    int _219 = NOVALUE;
    int _218 = NOVALUE;
    int _215 = NOVALUE;
    int _212 = NOVALUE;
    int _204 = NOVALUE;
    int _203 = NOVALUE;
    int _0, _1, _2;
    

    /** 	Defined_Sets = repeat(0, CS_LAST - CS_FIRST - 1)*/
    _203 = 20;
    _204 = 19;
    _203 = NOVALUE;
    DeRef(_5Defined_Sets_518);
    _5Defined_Sets_518 = Repeat(0, 19);
    _204 = NOVALUE;

    /** 	Defined_Sets[CS_Alphabetic	] = {{'a', 'z'}, {'A', 'Z'}}*/
    RefDS(_211);
    RefDS(_208);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _208;
    ((int *)_2)[2] = _211;
    _212 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 12);
    *(int *)_2 = _212;
    if( _1 != _212 ){
    }
    _212 = NOVALUE;

    /** 	Defined_Sets[CS_Alphanumeric] = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_214);
    *((int *)(_2+4)) = _214;
    RefDS(_208);
    *((int *)(_2+8)) = _208;
    RefDS(_211);
    *((int *)(_2+12)) = _211;
    _215 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = _215;
    if( _1 != _215 ){
        DeRef(_1);
    }
    _215 = NOVALUE;

    /** 	Defined_Sets[CS_Identifier]   = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}, {'_', '_'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_214);
    *((int *)(_2+4)) = _214;
    RefDS(_208);
    *((int *)(_2+8)) = _208;
    RefDS(_211);
    *((int *)(_2+12)) = _211;
    RefDS(_217);
    *((int *)(_2+16)) = _217;
    _218 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _218;
    if( _1 != _218 ){
        DeRef(_1);
    }
    _218 = NOVALUE;

    /** 	Defined_Sets[CS_Uppercase 	] = {{'A', 'Z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_211);
    *((int *)(_2+4)) = _211;
    _219 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _219;
    if( _1 != _219 ){
        DeRef(_1);
    }
    _219 = NOVALUE;

    /** 	Defined_Sets[CS_Lowercase 	] = {{'a', 'z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_208);
    *((int *)(_2+4)) = _208;
    _220 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _220;
    if( _1 != _220 ){
        DeRef(_1);
    }
    _220 = NOVALUE;

    /** 	Defined_Sets[CS_Printable 	] = {{' ', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_221);
    *((int *)(_2+4)) = _221;
    _222 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _222;
    if( _1 != _222 ){
        DeRef(_1);
    }
    _222 = NOVALUE;

    /** 	Defined_Sets[CS_Displayable ] = {{' ', '~'}, "  ", "\t\t", "\n\n", "\r\r", {8,8}, {7,7} }*/
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_221);
    *((int *)(_2+4)) = _221;
    RefDS(_223);
    *((int *)(_2+8)) = _223;
    RefDS(_224);
    *((int *)(_2+12)) = _224;
    RefDS(_225);
    *((int *)(_2+16)) = _225;
    RefDS(_226);
    *((int *)(_2+20)) = _226;
    RefDS(_228);
    *((int *)(_2+24)) = _228;
    RefDS(_230);
    *((int *)(_2+28)) = _230;
    _231 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _231;
    if( _1 != _231 ){
        DeRef(_1);
    }
    _231 = NOVALUE;

    /** 	Defined_Sets[CS_Whitespace 	] = " \t\n\r" & 11 & 160*/
    {
        int concat_list[3];

        concat_list[0] = 160;
        concat_list[1] = 11;
        concat_list[2] = _232;
        Concat_N((object_ptr)&_235, concat_list, 3);
    }
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _235;
    if( _1 != _235 ){
        DeRef(_1);
    }
    _235 = NOVALUE;

    /** 	Defined_Sets[CS_Consonant 	] = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"*/
    RefDS(_236);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _236;
    DeRef(_1);

    /** 	Defined_Sets[CS_Vowel 		] = "aeiouAEIOU"*/
    RefDS(_237);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _237;
    DeRef(_1);

    /** 	Defined_Sets[CS_Hexadecimal ] = {{'0', '9'}, {'A', 'F'},{'a', 'f'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_214);
    *((int *)(_2+4)) = _214;
    RefDS(_239);
    *((int *)(_2+8)) = _239;
    RefDS(_241);
    *((int *)(_2+12)) = _241;
    _242 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _242;
    if( _1 != _242 ){
        DeRef(_1);
    }
    _242 = NOVALUE;

    /** 	Defined_Sets[CS_Punctuation ] = {{' ', '/'}, {':', '?'}, {'[', '`'}, {'{', '~'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_244);
    *((int *)(_2+4)) = _244;
    RefDS(_247);
    *((int *)(_2+8)) = _247;
    RefDS(_250);
    *((int *)(_2+12)) = _250;
    RefDS(_251);
    *((int *)(_2+16)) = _251;
    _252 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _252;
    if( _1 != _252 ){
        DeRef(_1);
    }
    _252 = NOVALUE;

    /** 	Defined_Sets[CS_Control 	] = {{0, 31}, {127, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 31;
    _254 = MAKE_SEQ(_1);
    RefDS(_255);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _254;
    ((int *)_2)[2] = _255;
    _256 = MAKE_SEQ(_1);
    _254 = NOVALUE;
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = _256;
    if( _1 != _256 ){
        DeRef(_1);
    }
    _256 = NOVALUE;

    /** 	Defined_Sets[CS_ASCII 		] = {{0, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 127;
    _257 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _257;
    _258 = MAKE_SEQ(_1);
    _257 = NOVALUE;
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 13);
    _1 = *(int *)_2;
    *(int *)_2 = _258;
    if( _1 != _258 ){
        DeRef(_1);
    }
    _258 = NOVALUE;

    /** 	Defined_Sets[CS_Digit 		] = {{'0', '9'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_214);
    *((int *)(_2+4)) = _214;
    _259 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _259;
    if( _1 != _259 ){
        DeRef(_1);
    }
    _259 = NOVALUE;

    /** 	Defined_Sets[CS_Graphic 	] = {{'!', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_261);
    *((int *)(_2+4)) = _261;
    _262 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 16);
    _1 = *(int *)_2;
    *(int *)_2 = _262;
    if( _1 != _262 ){
        DeRef(_1);
    }
    _262 = NOVALUE;

    /** 	Defined_Sets[CS_Bytes	 	] = {{0, 255}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 255;
    _264 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _264;
    _265 = MAKE_SEQ(_1);
    _264 = NOVALUE;
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 17);
    _1 = *(int *)_2;
    *(int *)_2 = _265;
    if( _1 != _265 ){
        DeRef(_1);
    }
    _265 = NOVALUE;

    /** 	Defined_Sets[CS_SpecWord 	] = "_"*/
    RefDS(_266);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 18);
    _1 = *(int *)_2;
    *(int *)_2 = _266;
    DeRef(_1);

    /** 	Defined_Sets[CS_Boolean     ] = {TRUE,FALSE}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5TRUE_415;
    ((int *)_2)[2] = _5FALSE_413;
    _267 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _2 = (int)(((s1_ptr)_2)->base + 19);
    _1 = *(int *)_2;
    *(int *)_2 = _267;
    if( _1 != _267 ){
        DeRef(_1);
    }
    _267 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _5get_charsets()
{
    int _result__588 = NOVALUE;
    int _271 = NOVALUE;
    int _270 = NOVALUE;
    int _269 = NOVALUE;
    int _268 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_ = {}*/
    RefDS(_5);
    DeRef(_result__588);
    _result__588 = _5;

    /** 	for i = CS_FIRST + 1 to CS_LAST - 1 do*/
    _268 = 1;
    _269 = 19;
    {
        int _i_590;
        _i_590 = 1;
L1: 
        if (_i_590 > 19){
            goto L2; // [22] 52
        }

        /** 		result_ = append(result_, {i, Defined_Sets[i]} )*/
        _2 = (int)SEQ_PTR(_5Defined_Sets_518);
        _270 = (int)*(((s1_ptr)_2)->base + _i_590);
        Ref(_270);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_590;
        ((int *)_2)[2] = _270;
        _271 = MAKE_SEQ(_1);
        _270 = NOVALUE;
        RefDS(_271);
        Append(&_result__588, _result__588, _271);
        DeRefDS(_271);
        _271 = NOVALUE;

        /** 	end for*/
        _i_590 = _i_590 + 1;
        goto L1; // [47] 29
L2: 
        ;
    }

    /** 	return result_*/
    DeRef(_268);
    _268 = NOVALUE;
    DeRef(_269);
    _269 = NOVALUE;
    return _result__588;
    ;
}


void _5set_charsets(int _charset_list_598)
{
    int _294 = NOVALUE;
    int _293 = NOVALUE;
    int _292 = NOVALUE;
    int _291 = NOVALUE;
    int _290 = NOVALUE;
    int _289 = NOVALUE;
    int _288 = NOVALUE;
    int _287 = NOVALUE;
    int _286 = NOVALUE;
    int _285 = NOVALUE;
    int _284 = NOVALUE;
    int _283 = NOVALUE;
    int _282 = NOVALUE;
    int _281 = NOVALUE;
    int _280 = NOVALUE;
    int _279 = NOVALUE;
    int _278 = NOVALUE;
    int _277 = NOVALUE;
    int _276 = NOVALUE;
    int _275 = NOVALUE;
    int _274 = NOVALUE;
    int _273 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(charset_list) do*/
    if (IS_SEQUENCE(_charset_list_598)){
            _273 = SEQ_PTR(_charset_list_598)->length;
    }
    else {
        _273 = 1;
    }
    {
        int _i_600;
        _i_600 = 1;
L1: 
        if (_i_600 > _273){
            goto L2; // [8] 133
        }

        /** 		if sequence(charset_list[i]) and length(charset_list[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_charset_list_598);
        _274 = (int)*(((s1_ptr)_2)->base + _i_600);
        _275 = IS_SEQUENCE(_274);
        _274 = NOVALUE;
        if (_275 == 0) {
            goto L3; // [24] 126
        }
        _2 = (int)SEQ_PTR(_charset_list_598);
        _277 = (int)*(((s1_ptr)_2)->base + _i_600);
        if (IS_SEQUENCE(_277)){
                _278 = SEQ_PTR(_277)->length;
        }
        else {
            _278 = 1;
        }
        _277 = NOVALUE;
        _279 = (_278 == 2);
        _278 = NOVALUE;
        if (_279 == 0)
        {
            DeRef(_279);
            _279 = NOVALUE;
            goto L3; // [40] 126
        }
        else{
            DeRef(_279);
            _279 = NOVALUE;
        }

        /** 			if integer(charset_list[i][1]) and charset_list[i][1] > CS_FIRST and charset_list[i][1] < CS_LAST then*/
        _2 = (int)SEQ_PTR(_charset_list_598);
        _280 = (int)*(((s1_ptr)_2)->base + _i_600);
        _2 = (int)SEQ_PTR(_280);
        _281 = (int)*(((s1_ptr)_2)->base + 1);
        _280 = NOVALUE;
        if (IS_ATOM_INT(_281))
        _282 = 1;
        else if (IS_ATOM_DBL(_281))
        _282 = IS_ATOM_INT(DoubleToInt(_281));
        else
        _282 = 0;
        _281 = NOVALUE;
        if (_282 == 0) {
            _283 = 0;
            goto L4; // [56] 78
        }
        _2 = (int)SEQ_PTR(_charset_list_598);
        _284 = (int)*(((s1_ptr)_2)->base + _i_600);
        _2 = (int)SEQ_PTR(_284);
        _285 = (int)*(((s1_ptr)_2)->base + 1);
        _284 = NOVALUE;
        if (IS_ATOM_INT(_285)) {
            _286 = (_285 > 0);
        }
        else {
            _286 = binary_op(GREATER, _285, 0);
        }
        _285 = NOVALUE;
        if (IS_ATOM_INT(_286))
        _283 = (_286 != 0);
        else
        _283 = DBL_PTR(_286)->dbl != 0.0;
L4: 
        if (_283 == 0) {
            goto L5; // [78] 125
        }
        _2 = (int)SEQ_PTR(_charset_list_598);
        _288 = (int)*(((s1_ptr)_2)->base + _i_600);
        _2 = (int)SEQ_PTR(_288);
        _289 = (int)*(((s1_ptr)_2)->base + 1);
        _288 = NOVALUE;
        if (IS_ATOM_INT(_289)) {
            _290 = (_289 < 20);
        }
        else {
            _290 = binary_op(LESS, _289, 20);
        }
        _289 = NOVALUE;
        if (_290 == 0) {
            DeRef(_290);
            _290 = NOVALUE;
            goto L5; // [97] 125
        }
        else {
            if (!IS_ATOM_INT(_290) && DBL_PTR(_290)->dbl == 0.0){
                DeRef(_290);
                _290 = NOVALUE;
                goto L5; // [97] 125
            }
            DeRef(_290);
            _290 = NOVALUE;
        }
        DeRef(_290);
        _290 = NOVALUE;

        /** 				Defined_Sets[charset_list[i][1]] = charset_list[i][2]*/
        _2 = (int)SEQ_PTR(_charset_list_598);
        _291 = (int)*(((s1_ptr)_2)->base + _i_600);
        _2 = (int)SEQ_PTR(_291);
        _292 = (int)*(((s1_ptr)_2)->base + 1);
        _291 = NOVALUE;
        _2 = (int)SEQ_PTR(_charset_list_598);
        _293 = (int)*(((s1_ptr)_2)->base + _i_600);
        _2 = (int)SEQ_PTR(_293);
        _294 = (int)*(((s1_ptr)_2)->base + 2);
        _293 = NOVALUE;
        Ref(_294);
        _2 = (int)SEQ_PTR(_5Defined_Sets_518);
        if (!IS_ATOM_INT(_292))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_292)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _292);
        _1 = *(int *)_2;
        *(int *)_2 = _294;
        if( _1 != _294 ){
            DeRef(_1);
        }
        _294 = NOVALUE;
L5: 
L3: 

        /** 	end for*/
        _i_600 = _i_600 + 1;
        goto L1; // [128] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_charset_list_598);
    _277 = NOVALUE;
    _292 = NOVALUE;
    DeRef(_286);
    _286 = NOVALUE;
    return;
    ;
}


int _5boolean(int _test_data_627)
{
    int _297 = NOVALUE;
    int _296 = NOVALUE;
    int _295 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return find(test_data,{1,0}) != 0*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _295 = MAKE_SEQ(_1);
    _296 = find_from(_test_data_627, _295, 1);
    DeRefDS(_295);
    _295 = NOVALUE;
    _297 = (_296 != 0);
    _296 = NOVALUE;
    DeRef(_test_data_627);
    return _297;
    ;
}


int _5t_boolean(int _test_data_633)
{
    int _299 = NOVALUE;
    int _298 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Boolean])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _298 = (int)*(((s1_ptr)_2)->base + 19);
    Ref(_test_data_633);
    Ref(_298);
    _299 = _5char_test(_test_data_633, _298);
    _298 = NOVALUE;
    DeRef(_test_data_633);
    return _299;
    ;
}


int _5t_alnum(int _test_data_638)
{
    int _301 = NOVALUE;
    int _300 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphanumeric])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _300 = (int)*(((s1_ptr)_2)->base + 10);
    Ref(_test_data_638);
    Ref(_300);
    _301 = _5char_test(_test_data_638, _300);
    _300 = NOVALUE;
    DeRef(_test_data_638);
    return _301;
    ;
}


int _5t_identifier(int _test_data_643)
{
    int _311 = NOVALUE;
    int _310 = NOVALUE;
    int _309 = NOVALUE;
    int _308 = NOVALUE;
    int _307 = NOVALUE;
    int _306 = NOVALUE;
    int _305 = NOVALUE;
    int _304 = NOVALUE;
    int _303 = NOVALUE;
    int _302 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if t_digit(test_data) then*/
    Ref(_test_data_643);
    _302 = _5t_digit(_test_data_643);
    if (_302 == 0) {
        DeRef(_302);
        _302 = NOVALUE;
        goto L1; // [7] 19
    }
    else {
        if (!IS_ATOM_INT(_302) && DBL_PTR(_302)->dbl == 0.0){
            DeRef(_302);
            _302 = NOVALUE;
            goto L1; // [7] 19
        }
        DeRef(_302);
        _302 = NOVALUE;
    }
    DeRef(_302);
    _302 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_643);
    return 0;
    goto L2; // [16] 63
L1: 

    /** 	elsif sequence(test_data) and length(test_data) > 0 and t_digit(test_data[1]) then*/
    _303 = IS_SEQUENCE(_test_data_643);
    if (_303 == 0) {
        _304 = 0;
        goto L3; // [24] 39
    }
    if (IS_SEQUENCE(_test_data_643)){
            _305 = SEQ_PTR(_test_data_643)->length;
    }
    else {
        _305 = 1;
    }
    _306 = (_305 > 0);
    _305 = NOVALUE;
    _304 = (_306 != 0);
L3: 
    if (_304 == 0) {
        goto L4; // [39] 62
    }
    _2 = (int)SEQ_PTR(_test_data_643);
    _308 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_308);
    _309 = _5t_digit(_308);
    _308 = NOVALUE;
    if (_309 == 0) {
        DeRef(_309);
        _309 = NOVALUE;
        goto L4; // [52] 62
    }
    else {
        if (!IS_ATOM_INT(_309) && DBL_PTR(_309)->dbl == 0.0){
            DeRef(_309);
            _309 = NOVALUE;
            goto L4; // [52] 62
        }
        DeRef(_309);
        _309 = NOVALUE;
    }
    DeRef(_309);
    _309 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_643);
    DeRef(_306);
    _306 = NOVALUE;
    return 0;
L4: 
L2: 

    /** 	return char_test(test_data, Defined_Sets[CS_Identifier])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _310 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_test_data_643);
    Ref(_310);
    _311 = _5char_test(_test_data_643, _310);
    _310 = NOVALUE;
    DeRef(_test_data_643);
    DeRef(_306);
    _306 = NOVALUE;
    return _311;
    ;
}


int _5t_alpha(int _test_data_660)
{
    int _313 = NOVALUE;
    int _312 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphabetic])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _312 = (int)*(((s1_ptr)_2)->base + 12);
    Ref(_test_data_660);
    Ref(_312);
    _313 = _5char_test(_test_data_660, _312);
    _312 = NOVALUE;
    DeRef(_test_data_660);
    return _313;
    ;
}


int _5t_ascii(int _test_data_665)
{
    int _315 = NOVALUE;
    int _314 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_ASCII])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _314 = (int)*(((s1_ptr)_2)->base + 13);
    Ref(_test_data_665);
    Ref(_314);
    _315 = _5char_test(_test_data_665, _314);
    _314 = NOVALUE;
    DeRef(_test_data_665);
    return _315;
    ;
}


int _5t_cntrl(int _test_data_670)
{
    int _317 = NOVALUE;
    int _316 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Control])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _316 = (int)*(((s1_ptr)_2)->base + 14);
    Ref(_test_data_670);
    Ref(_316);
    _317 = _5char_test(_test_data_670, _316);
    _316 = NOVALUE;
    DeRef(_test_data_670);
    return _317;
    ;
}


int _5t_digit(int _test_data_675)
{
    int _319 = NOVALUE;
    int _318 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Digit])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _318 = (int)*(((s1_ptr)_2)->base + 15);
    Ref(_test_data_675);
    Ref(_318);
    _319 = _5char_test(_test_data_675, _318);
    _318 = NOVALUE;
    DeRef(_test_data_675);
    return _319;
    ;
}


int _5t_graph(int _test_data_680)
{
    int _321 = NOVALUE;
    int _320 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Graphic])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _320 = (int)*(((s1_ptr)_2)->base + 16);
    Ref(_test_data_680);
    Ref(_320);
    _321 = _5char_test(_test_data_680, _320);
    _320 = NOVALUE;
    DeRef(_test_data_680);
    return _321;
    ;
}


int _5t_specword(int _test_data_685)
{
    int _323 = NOVALUE;
    int _322 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_SpecWord])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _322 = (int)*(((s1_ptr)_2)->base + 18);
    Ref(_test_data_685);
    Ref(_322);
    _323 = _5char_test(_test_data_685, _322);
    _322 = NOVALUE;
    DeRef(_test_data_685);
    return _323;
    ;
}


int _5t_bytearray(int _test_data_690)
{
    int _325 = NOVALUE;
    int _324 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Bytes])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _324 = (int)*(((s1_ptr)_2)->base + 17);
    Ref(_test_data_690);
    Ref(_324);
    _325 = _5char_test(_test_data_690, _324);
    _324 = NOVALUE;
    DeRef(_test_data_690);
    return _325;
    ;
}


int _5t_lower(int _test_data_695)
{
    int _327 = NOVALUE;
    int _326 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Lowercase])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _326 = (int)*(((s1_ptr)_2)->base + 8);
    Ref(_test_data_695);
    Ref(_326);
    _327 = _5char_test(_test_data_695, _326);
    _326 = NOVALUE;
    DeRef(_test_data_695);
    return _327;
    ;
}


int _5t_print(int _test_data_700)
{
    int _329 = NOVALUE;
    int _328 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Printable])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _328 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_test_data_700);
    Ref(_328);
    _329 = _5char_test(_test_data_700, _328);
    _328 = NOVALUE;
    DeRef(_test_data_700);
    return _329;
    ;
}


int _5t_display(int _test_data_705)
{
    int _331 = NOVALUE;
    int _330 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Displayable])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _330 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_test_data_705);
    Ref(_330);
    _331 = _5char_test(_test_data_705, _330);
    _330 = NOVALUE;
    DeRef(_test_data_705);
    return _331;
    ;
}


int _5t_punct(int _test_data_710)
{
    int _333 = NOVALUE;
    int _332 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Punctuation])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _332 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_test_data_710);
    Ref(_332);
    _333 = _5char_test(_test_data_710, _332);
    _332 = NOVALUE;
    DeRef(_test_data_710);
    return _333;
    ;
}


int _5t_space(int _test_data_715)
{
    int _335 = NOVALUE;
    int _334 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Whitespace])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _334 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_test_data_715);
    Ref(_334);
    _335 = _5char_test(_test_data_715, _334);
    _334 = NOVALUE;
    DeRef(_test_data_715);
    return _335;
    ;
}


int _5t_upper(int _test_data_720)
{
    int _337 = NOVALUE;
    int _336 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Uppercase])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _336 = (int)*(((s1_ptr)_2)->base + 9);
    Ref(_test_data_720);
    Ref(_336);
    _337 = _5char_test(_test_data_720, _336);
    _336 = NOVALUE;
    DeRef(_test_data_720);
    return _337;
    ;
}


int _5t_xdigit(int _test_data_725)
{
    int _339 = NOVALUE;
    int _338 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Hexadecimal])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _338 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_test_data_725);
    Ref(_338);
    _339 = _5char_test(_test_data_725, _338);
    _338 = NOVALUE;
    DeRef(_test_data_725);
    return _339;
    ;
}


int _5t_vowel(int _test_data_730)
{
    int _341 = NOVALUE;
    int _340 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Vowel])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _340 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_test_data_730);
    Ref(_340);
    _341 = _5char_test(_test_data_730, _340);
    _340 = NOVALUE;
    DeRef(_test_data_730);
    return _341;
    ;
}


int _5t_consonant(int _test_data_735)
{
    int _343 = NOVALUE;
    int _342 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Consonant])*/
    _2 = (int)SEQ_PTR(_5Defined_Sets_518);
    _342 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_test_data_735);
    Ref(_342);
    _343 = _5char_test(_test_data_735, _342);
    _342 = NOVALUE;
    DeRef(_test_data_735);
    return _343;
    ;
}


int _5integer_array(int _x_740)
{
    int _348 = NOVALUE;
    int _347 = NOVALUE;
    int _346 = NOVALUE;
    int _344 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _344 = IS_SEQUENCE(_x_740);
    if (_344 != 0)
    goto L1; // [6] 16
    _344 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_740);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_740)){
            _346 = SEQ_PTR(_x_740)->length;
    }
    else {
        _346 = 1;
    }
    {
        int _i_745;
        _i_745 = 1;
L2: 
        if (_i_745 > _346){
            goto L3; // [21] 54
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_740);
        _347 = (int)*(((s1_ptr)_2)->base + _i_745);
        if (IS_ATOM_INT(_347))
        _348 = 1;
        else if (IS_ATOM_DBL(_347))
        _348 = IS_ATOM_INT(DoubleToInt(_347));
        else
        _348 = 0;
        _347 = NOVALUE;
        if (_348 != 0)
        goto L4; // [37] 47
        _348 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_740);
        return 0;
L4: 

        /** 	end for*/
        _i_745 = _i_745 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_740);
    return 1;
    ;
}


int _5t_text(int _x_753)
{
    int _356 = NOVALUE;
    int _354 = NOVALUE;
    int _353 = NOVALUE;
    int _352 = NOVALUE;
    int _350 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _350 = IS_SEQUENCE(_x_753);
    if (_350 != 0)
    goto L1; // [6] 16
    _350 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_753);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_753)){
            _352 = SEQ_PTR(_x_753)->length;
    }
    else {
        _352 = 1;
    }
    {
        int _i_758;
        _i_758 = 1;
L2: 
        if (_i_758 > _352){
            goto L3; // [21] 71
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_753);
        _353 = (int)*(((s1_ptr)_2)->base + _i_758);
        if (IS_ATOM_INT(_353))
        _354 = 1;
        else if (IS_ATOM_DBL(_353))
        _354 = IS_ATOM_INT(DoubleToInt(_353));
        else
        _354 = 0;
        _353 = NOVALUE;
        if (_354 != 0)
        goto L4; // [37] 47
        _354 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_753);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_753);
        _356 = (int)*(((s1_ptr)_2)->base + _i_758);
        if (binary_op_a(GREATEREQ, _356, 0)){
            _356 = NOVALUE;
            goto L5; // [53] 64
        }
        _356 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_753);
        return 0;
L5: 

        /** 	end for*/
        _i_758 = _i_758 + 1;
        goto L2; // [66] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_753);
    return 1;
    ;
}


int _5number_array(int _x_769)
{
    int _362 = NOVALUE;
    int _361 = NOVALUE;
    int _360 = NOVALUE;
    int _358 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _358 = IS_SEQUENCE(_x_769);
    if (_358 != 0)
    goto L1; // [6] 16
    _358 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_769);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_769)){
            _360 = SEQ_PTR(_x_769)->length;
    }
    else {
        _360 = 1;
    }
    {
        int _i_774;
        _i_774 = 1;
L2: 
        if (_i_774 > _360){
            goto L3; // [21] 54
        }

        /** 		if not atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_769);
        _361 = (int)*(((s1_ptr)_2)->base + _i_774);
        _362 = IS_ATOM(_361);
        _361 = NOVALUE;
        if (_362 != 0)
        goto L4; // [37] 47
        _362 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_769);
        return 0;
L4: 

        /** 	end for*/
        _i_774 = _i_774 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_769);
    return 1;
    ;
}


int _5sequence_array(int _x_782)
{
    int _368 = NOVALUE;
    int _367 = NOVALUE;
    int _366 = NOVALUE;
    int _364 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _364 = IS_SEQUENCE(_x_782);
    if (_364 != 0)
    goto L1; // [6] 16
    _364 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_782);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_782)){
            _366 = SEQ_PTR(_x_782)->length;
    }
    else {
        _366 = 1;
    }
    {
        int _i_787;
        _i_787 = 1;
L2: 
        if (_i_787 > _366){
            goto L3; // [21] 54
        }

        /** 		if not sequence(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_782);
        _367 = (int)*(((s1_ptr)_2)->base + _i_787);
        _368 = IS_SEQUENCE(_367);
        _367 = NOVALUE;
        if (_368 != 0)
        goto L4; // [37] 47
        _368 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_782);
        return 0;
L4: 

        /** 	end for*/
        _i_787 = _i_787 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_782);
    return 1;
    ;
}


int _5ascii_string(int _x_795)
{
    int _378 = NOVALUE;
    int _376 = NOVALUE;
    int _374 = NOVALUE;
    int _373 = NOVALUE;
    int _372 = NOVALUE;
    int _370 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _370 = IS_SEQUENCE(_x_795);
    if (_370 != 0)
    goto L1; // [6] 16
    _370 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_795);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_795)){
            _372 = SEQ_PTR(_x_795)->length;
    }
    else {
        _372 = 1;
    }
    {
        int _i_800;
        _i_800 = 1;
L2: 
        if (_i_800 > _372){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_795);
        _373 = (int)*(((s1_ptr)_2)->base + _i_800);
        if (IS_ATOM_INT(_373))
        _374 = 1;
        else if (IS_ATOM_DBL(_373))
        _374 = IS_ATOM_INT(DoubleToInt(_373));
        else
        _374 = 0;
        _373 = NOVALUE;
        if (_374 != 0)
        goto L4; // [37] 47
        _374 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_795);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_795);
        _376 = (int)*(((s1_ptr)_2)->base + _i_800);
        if (binary_op_a(GREATEREQ, _376, 0)){
            _376 = NOVALUE;
            goto L5; // [53] 64
        }
        _376 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_795);
        return 0;
L5: 

        /** 		if x[i] > 127 then*/
        _2 = (int)SEQ_PTR(_x_795);
        _378 = (int)*(((s1_ptr)_2)->base + _i_800);
        if (binary_op_a(LESSEQ, _378, 127)){
            _378 = NOVALUE;
            goto L6; // [70] 81
        }
        _378 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_795);
        return 0;
L6: 

        /** 	end for*/
        _i_800 = _i_800 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_795);
    return 1;
    ;
}


int _5string(int _x_814)
{
    int _388 = NOVALUE;
    int _386 = NOVALUE;
    int _384 = NOVALUE;
    int _383 = NOVALUE;
    int _382 = NOVALUE;
    int _380 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _380 = IS_SEQUENCE(_x_814);
    if (_380 != 0)
    goto L1; // [6] 16
    _380 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_814);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_814)){
            _382 = SEQ_PTR(_x_814)->length;
    }
    else {
        _382 = 1;
    }
    {
        int _i_819;
        _i_819 = 1;
L2: 
        if (_i_819 > _382){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_814);
        _383 = (int)*(((s1_ptr)_2)->base + _i_819);
        if (IS_ATOM_INT(_383))
        _384 = 1;
        else if (IS_ATOM_DBL(_383))
        _384 = IS_ATOM_INT(DoubleToInt(_383));
        else
        _384 = 0;
        _383 = NOVALUE;
        if (_384 != 0)
        goto L4; // [37] 47
        _384 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_814);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_814);
        _386 = (int)*(((s1_ptr)_2)->base + _i_819);
        if (binary_op_a(GREATEREQ, _386, 0)){
            _386 = NOVALUE;
            goto L5; // [53] 64
        }
        _386 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_814);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_814);
        _388 = (int)*(((s1_ptr)_2)->base + _i_819);
        if (binary_op_a(LESSEQ, _388, 255)){
            _388 = NOVALUE;
            goto L6; // [70] 81
        }
        _388 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_814);
        return 0;
L6: 

        /** 	end for*/
        _i_819 = _i_819 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_814);
    return 1;
    ;
}


int _5cstring(int _x_833)
{
    int _398 = NOVALUE;
    int _396 = NOVALUE;
    int _394 = NOVALUE;
    int _393 = NOVALUE;
    int _392 = NOVALUE;
    int _390 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _390 = IS_SEQUENCE(_x_833);
    if (_390 != 0)
    goto L1; // [6] 16
    _390 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_833);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_833)){
            _392 = SEQ_PTR(_x_833)->length;
    }
    else {
        _392 = 1;
    }
    {
        int _i_838;
        _i_838 = 1;
L2: 
        if (_i_838 > _392){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_833);
        _393 = (int)*(((s1_ptr)_2)->base + _i_838);
        if (IS_ATOM_INT(_393))
        _394 = 1;
        else if (IS_ATOM_DBL(_393))
        _394 = IS_ATOM_INT(DoubleToInt(_393));
        else
        _394 = 0;
        _393 = NOVALUE;
        if (_394 != 0)
        goto L4; // [37] 47
        _394 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_833);
        return 0;
L4: 

        /** 		if x[i] <= 0 then*/
        _2 = (int)SEQ_PTR(_x_833);
        _396 = (int)*(((s1_ptr)_2)->base + _i_838);
        if (binary_op_a(GREATER, _396, 0)){
            _396 = NOVALUE;
            goto L5; // [53] 64
        }
        _396 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_833);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_833);
        _398 = (int)*(((s1_ptr)_2)->base + _i_838);
        if (binary_op_a(LESSEQ, _398, 255)){
            _398 = NOVALUE;
            goto L6; // [70] 81
        }
        _398 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_833);
        return 0;
L6: 

        /** 	end for*/
        _i_838 = _i_838 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_833);
    return 1;
    ;
}



// 0x95755D0E
