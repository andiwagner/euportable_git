// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _3binop_ok(int _a_1624, int _b_1625)
{
    int _815 = NOVALUE;
    int _814 = NOVALUE;
    int _813 = NOVALUE;
    int _812 = NOVALUE;
    int _810 = NOVALUE;
    int _809 = NOVALUE;
    int _808 = NOVALUE;
    int _806 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) or atom(b) then*/
    _806 = IS_ATOM(_a_1624);
    if (_806 != 0) {
        goto L1; // [6] 18
    }
    _808 = IS_ATOM(_b_1625);
    if (_808 == 0)
    {
        _808 = NOVALUE;
        goto L2; // [14] 25
    }
    else{
        _808 = NOVALUE;
    }
L1: 

    /** 		return 1*/
    DeRef(_a_1624);
    DeRef(_b_1625);
    return 1;
L2: 

    /** 	if length(a) != length(b) then*/
    if (IS_SEQUENCE(_a_1624)){
            _809 = SEQ_PTR(_a_1624)->length;
    }
    else {
        _809 = 1;
    }
    if (IS_SEQUENCE(_b_1625)){
            _810 = SEQ_PTR(_b_1625)->length;
    }
    else {
        _810 = 1;
    }
    if (_809 == _810)
    goto L3; // [33] 44

    /** 		return 0*/
    DeRef(_a_1624);
    DeRef(_b_1625);
    return 0;
L3: 

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_1624)){
            _812 = SEQ_PTR(_a_1624)->length;
    }
    else {
        _812 = 1;
    }
    {
        int _i_1635;
        _i_1635 = 1;
L4: 
        if (_i_1635 > _812){
            goto L5; // [49] 88
        }

        /** 		if not binop_ok(a[i], b[i]) then*/
        _2 = (int)SEQ_PTR(_a_1624);
        _813 = (int)*(((s1_ptr)_2)->base + _i_1635);
        _2 = (int)SEQ_PTR(_b_1625);
        _814 = (int)*(((s1_ptr)_2)->base + _i_1635);
        Ref(_813);
        Ref(_814);
        _815 = _3binop_ok(_813, _814);
        _813 = NOVALUE;
        _814 = NOVALUE;
        if (IS_ATOM_INT(_815)) {
            if (_815 != 0){
                DeRef(_815);
                _815 = NOVALUE;
                goto L6; // [71] 81
            }
        }
        else {
            if (DBL_PTR(_815)->dbl != 0.0){
                DeRef(_815);
                _815 = NOVALUE;
                goto L6; // [71] 81
            }
        }
        DeRef(_815);
        _815 = NOVALUE;

        /** 			return 0*/
        DeRef(_a_1624);
        DeRef(_b_1625);
        return 0;
L6: 

        /** 	end for*/
        _i_1635 = _i_1635 + 1;
        goto L4; // [83] 56
L5: 
        ;
    }

    /** 	return 1*/
    DeRef(_a_1624);
    DeRef(_b_1625);
    return 1;
    ;
}


int _3fetch(int _source_1644, int _indexes_1645)
{
    int _x_1646 = NOVALUE;
    int _827 = NOVALUE;
    int _826 = NOVALUE;
    int _825 = NOVALUE;
    int _824 = NOVALUE;
    int _823 = NOVALUE;
    int _821 = NOVALUE;
    int _819 = NOVALUE;
    int _818 = NOVALUE;
    int _817 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i=1 to length(indexes)-1 do*/
    if (IS_SEQUENCE(_indexes_1645)){
            _817 = SEQ_PTR(_indexes_1645)->length;
    }
    else {
        _817 = 1;
    }
    _818 = _817 - 1;
    _817 = NOVALUE;
    {
        int _i_1648;
        _i_1648 = 1;
L1: 
        if (_i_1648 > _818){
            goto L2; // [14] 40
        }

        /** 		source = source[indexes[i]]*/
        _2 = (int)SEQ_PTR(_indexes_1645);
        _819 = (int)*(((s1_ptr)_2)->base + _i_1648);
        _0 = _source_1644;
        _2 = (int)SEQ_PTR(_source_1644);
        if (!IS_ATOM_INT(_819)){
            _source_1644 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_819)->dbl));
        }
        else{
            _source_1644 = (int)*(((s1_ptr)_2)->base + _819);
        }
        Ref(_source_1644);
        DeRefDS(_0);

        /** 	end for*/
        _i_1648 = _i_1648 + 1;
        goto L1; // [35] 21
L2: 
        ;
    }

    /** 	x = indexes[$]*/
    if (IS_SEQUENCE(_indexes_1645)){
            _821 = SEQ_PTR(_indexes_1645)->length;
    }
    else {
        _821 = 1;
    }
    DeRef(_x_1646);
    _2 = (int)SEQ_PTR(_indexes_1645);
    _x_1646 = (int)*(((s1_ptr)_2)->base + _821);
    Ref(_x_1646);

    /** 	if atom(x) then*/
    _823 = IS_ATOM(_x_1646);
    if (_823 == 0)
    {
        _823 = NOVALUE;
        goto L3; // [54] 70
    }
    else{
        _823 = NOVALUE;
    }

    /** 		return source[x]*/
    _2 = (int)SEQ_PTR(_source_1644);
    if (!IS_ATOM_INT(_x_1646)){
        _824 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_x_1646)->dbl));
    }
    else{
        _824 = (int)*(((s1_ptr)_2)->base + _x_1646);
    }
    Ref(_824);
    DeRefDS(_source_1644);
    DeRefDS(_indexes_1645);
    DeRef(_x_1646);
    DeRef(_818);
    _818 = NOVALUE;
    _819 = NOVALUE;
    return _824;
    goto L4; // [67] 90
L3: 

    /** 		return source[x[1]..x[2]]*/
    _2 = (int)SEQ_PTR(_x_1646);
    _825 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_x_1646);
    _826 = (int)*(((s1_ptr)_2)->base + 2);
    rhs_slice_target = (object_ptr)&_827;
    RHS_Slice(_source_1644, _825, _826);
    DeRefDS(_source_1644);
    DeRefDS(_indexes_1645);
    DeRef(_x_1646);
    DeRef(_818);
    _818 = NOVALUE;
    _819 = NOVALUE;
    _824 = NOVALUE;
    _825 = NOVALUE;
    _826 = NOVALUE;
    return _827;
L4: 
    ;
}


int _3store(int _target_1664, int _indexes_1665, int _x_1666)
{
    int _partials_1667 = NOVALUE;
    int _result_1668 = NOVALUE;
    int _branch_1669 = NOVALUE;
    int _last_idx_1670 = NOVALUE;
    int _848 = NOVALUE;
    int _847 = NOVALUE;
    int _845 = NOVALUE;
    int _844 = NOVALUE;
    int _842 = NOVALUE;
    int _841 = NOVALUE;
    int _840 = NOVALUE;
    int _838 = NOVALUE;
    int _836 = NOVALUE;
    int _835 = NOVALUE;
    int _834 = NOVALUE;
    int _832 = NOVALUE;
    int _831 = NOVALUE;
    int _830 = NOVALUE;
    int _828 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(indexes) = 1 then*/
    if (IS_SEQUENCE(_indexes_1665)){
            _828 = SEQ_PTR(_indexes_1665)->length;
    }
    else {
        _828 = 1;
    }
    if (_828 != 1)
    goto L1; // [10] 31

    /** 		target[indexes[1]] = x*/
    _2 = (int)SEQ_PTR(_indexes_1665);
    _830 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_x_1666);
    _2 = (int)SEQ_PTR(_target_1664);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _target_1664 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_830))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_830)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _830);
    _1 = *(int *)_2;
    *(int *)_2 = _x_1666;
    DeRef(_1);

    /** 		return target*/
    DeRefDS(_indexes_1665);
    DeRef(_x_1666);
    DeRef(_partials_1667);
    DeRef(_result_1668);
    DeRef(_branch_1669);
    DeRef(_last_idx_1670);
    _830 = NOVALUE;
    return _target_1664;
L1: 

    /** 	partials = repeat(target,length(indexes)-1)*/
    if (IS_SEQUENCE(_indexes_1665)){
            _831 = SEQ_PTR(_indexes_1665)->length;
    }
    else {
        _831 = 1;
    }
    _832 = _831 - 1;
    _831 = NOVALUE;
    DeRef(_partials_1667);
    _partials_1667 = Repeat(_target_1664, _832);
    _832 = NOVALUE;

    /** 	branch = target*/
    RefDS(_target_1664);
    DeRef(_branch_1669);
    _branch_1669 = _target_1664;

    /** 	for i=1 to length(indexes)-1 do*/
    if (IS_SEQUENCE(_indexes_1665)){
            _834 = SEQ_PTR(_indexes_1665)->length;
    }
    else {
        _834 = 1;
    }
    _835 = _834 - 1;
    _834 = NOVALUE;
    {
        int _i_1679;
        _i_1679 = 1;
L2: 
        if (_i_1679 > _835){
            goto L3; // [60] 92
        }

        /** 		branch=branch[indexes[i]]*/
        _2 = (int)SEQ_PTR(_indexes_1665);
        _836 = (int)*(((s1_ptr)_2)->base + _i_1679);
        _0 = _branch_1669;
        _2 = (int)SEQ_PTR(_branch_1669);
        if (!IS_ATOM_INT(_836)){
            _branch_1669 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_836)->dbl));
        }
        else{
            _branch_1669 = (int)*(((s1_ptr)_2)->base + _836);
        }
        Ref(_branch_1669);
        DeRef(_0);

        /** 		partials[i]=branch*/
        RefDS(_branch_1669);
        _2 = (int)SEQ_PTR(_partials_1667);
        _2 = (int)(((s1_ptr)_2)->base + _i_1679);
        _1 = *(int *)_2;
        *(int *)_2 = _branch_1669;
        DeRef(_1);

        /** 	end for*/
        _i_1679 = _i_1679 + 1;
        goto L2; // [87] 67
L3: 
        ;
    }

    /** 	last_idx = indexes[$]*/
    if (IS_SEQUENCE(_indexes_1665)){
            _838 = SEQ_PTR(_indexes_1665)->length;
    }
    else {
        _838 = 1;
    }
    DeRef(_last_idx_1670);
    _2 = (int)SEQ_PTR(_indexes_1665);
    _last_idx_1670 = (int)*(((s1_ptr)_2)->base + _838);
    Ref(_last_idx_1670);

    /** 	if atom(last_idx) then*/
    _840 = IS_ATOM(_last_idx_1670);
    if (_840 == 0)
    {
        _840 = NOVALUE;
        goto L4; // [106] 118
    }
    else{
        _840 = NOVALUE;
    }

    /** 		branch[last_idx]=x*/
    Ref(_x_1666);
    _2 = (int)SEQ_PTR(_branch_1669);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _branch_1669 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_last_idx_1670))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_last_idx_1670)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _last_idx_1670);
    _1 = *(int *)_2;
    *(int *)_2 = _x_1666;
    DeRef(_1);
    goto L5; // [115] 134
L4: 

    /** 		branch[last_idx[1]..last_idx[2]]=x*/
    _2 = (int)SEQ_PTR(_last_idx_1670);
    _841 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_last_idx_1670);
    _842 = (int)*(((s1_ptr)_2)->base + 2);
    assign_slice_seq = (s1_ptr *)&_branch_1669;
    AssignSlice(_841, _842, _x_1666);
    _841 = NOVALUE;
    _842 = NOVALUE;
L5: 

    /** 	partials = prepend(partials,0) -- avoids computing temp=i+1 a few times*/
    Prepend(&_partials_1667, _partials_1667, 0);

    /** 	for i=length(indexes)-1 to 2 by -1 do*/
    if (IS_SEQUENCE(_indexes_1665)){
            _844 = SEQ_PTR(_indexes_1665)->length;
    }
    else {
        _844 = 1;
    }
    _845 = _844 - 1;
    _844 = NOVALUE;
    {
        int _i_1693;
        _i_1693 = _845;
L6: 
        if (_i_1693 < 2){
            goto L7; // [149] 188
        }

        /** 		result = partials[i]*/
        DeRef(_result_1668);
        _2 = (int)SEQ_PTR(_partials_1667);
        _result_1668 = (int)*(((s1_ptr)_2)->base + _i_1693);
        Ref(_result_1668);

        /** 		result[indexes[i]] = branch*/
        _2 = (int)SEQ_PTR(_indexes_1665);
        _847 = (int)*(((s1_ptr)_2)->base + _i_1693);
        RefDS(_branch_1669);
        _2 = (int)SEQ_PTR(_result_1668);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_1668 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_847))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_847)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _847);
        _1 = *(int *)_2;
        *(int *)_2 = _branch_1669;
        DeRef(_1);

        /** 		branch = result*/
        RefDS(_result_1668);
        DeRefDS(_branch_1669);
        _branch_1669 = _result_1668;

        /** 	end for*/
        _i_1693 = _i_1693 + -1;
        goto L6; // [183] 156
L7: 
        ;
    }

    /** 	target[indexes[1]] = branch*/
    _2 = (int)SEQ_PTR(_indexes_1665);
    _848 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_branch_1669);
    _2 = (int)SEQ_PTR(_target_1664);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _target_1664 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_848))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_848)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _848);
    _1 = *(int *)_2;
    *(int *)_2 = _branch_1669;
    DeRef(_1);

    /** 	return target*/
    DeRefDS(_indexes_1665);
    DeRef(_x_1666);
    DeRef(_partials_1667);
    DeRef(_result_1668);
    DeRefDS(_branch_1669);
    DeRef(_last_idx_1670);
    _830 = NOVALUE;
    DeRef(_835);
    _835 = NOVALUE;
    _836 = NOVALUE;
    DeRef(_845);
    _845 = NOVALUE;
    _847 = NOVALUE;
    _848 = NOVALUE;
    return _target_1664;
    ;
}


int _3valid_index(int _st_1701, int _x_1702)
{
    int _853 = NOVALUE;
    int _852 = NOVALUE;
    int _849 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(x) then*/
    _849 = IS_ATOM(_x_1702);
    if (_849 != 0)
    goto L1; // [8] 18
    _849 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_st_1701);
    DeRef(_x_1702);
    return 0;
L1: 

    /** 	if x < 1 then*/
    if (binary_op_a(GREATEREQ, _x_1702, 1)){
        goto L2; // [20] 31
    }

    /** 		return 0*/
    DeRefDS(_st_1701);
    DeRef(_x_1702);
    return 0;
L2: 

    /** 	if floor(x) > length(st) then*/
    if (IS_ATOM_INT(_x_1702))
    _852 = e_floor(_x_1702);
    else
    _852 = unary_op(FLOOR, _x_1702);
    if (IS_SEQUENCE(_st_1701)){
            _853 = SEQ_PTR(_st_1701)->length;
    }
    else {
        _853 = 1;
    }
    if (binary_op_a(LESSEQ, _852, _853)){
        DeRef(_852);
        _852 = NOVALUE;
        _853 = NOVALUE;
        goto L3; // [39] 50
    }
    DeRef(_852);
    _852 = NOVALUE;
    _853 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_st_1701);
    DeRef(_x_1702);
    return 0;
L3: 

    /** 	return 1*/
    DeRefDS(_st_1701);
    DeRef(_x_1702);
    return 1;
    ;
}


int _3rotate(int _source_1714, int _shift_1715, int _start_1716, int _stop_1717)
{
    int _shifted_1719 = NOVALUE;
    int _len_1720 = NOVALUE;
    int _lSize_1721 = NOVALUE;
    int _msg_inlined_crash_at_66_1734 = NOVALUE;
    int _msg_inlined_crash_at_97_1740 = NOVALUE;
    int _881 = NOVALUE;
    int _880 = NOVALUE;
    int _879 = NOVALUE;
    int _878 = NOVALUE;
    int _877 = NOVALUE;
    int _875 = NOVALUE;
    int _874 = NOVALUE;
    int _868 = NOVALUE;
    int _865 = NOVALUE;
    int _862 = NOVALUE;
    int _861 = NOVALUE;
    int _859 = NOVALUE;
    int _858 = NOVALUE;
    int _857 = NOVALUE;
    int _856 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_shift_1715)) {
        _1 = (long)(DBL_PTR(_shift_1715)->dbl);
        if (UNIQUE(DBL_PTR(_shift_1715)) && (DBL_PTR(_shift_1715)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_shift_1715);
        _shift_1715 = _1;
    }
    if (!IS_ATOM_INT(_start_1716)) {
        _1 = (long)(DBL_PTR(_start_1716)->dbl);
        if (UNIQUE(DBL_PTR(_start_1716)) && (DBL_PTR(_start_1716)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_1716);
        _start_1716 = _1;
    }
    if (!IS_ATOM_INT(_stop_1717)) {
        _1 = (long)(DBL_PTR(_stop_1717)->dbl);
        if (UNIQUE(DBL_PTR(_stop_1717)) && (DBL_PTR(_stop_1717)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stop_1717);
        _stop_1717 = _1;
    }

    /** 	if start >= stop or length(source)=0 or not shift then*/
    _856 = (_start_1716 >= _stop_1717);
    if (_856 != 0) {
        _857 = 1;
        goto L1; // [21] 36
    }
    if (IS_SEQUENCE(_source_1714)){
            _858 = SEQ_PTR(_source_1714)->length;
    }
    else {
        _858 = 1;
    }
    _859 = (_858 == 0);
    _858 = NOVALUE;
    _857 = (_859 != 0);
L1: 
    if (_857 != 0) {
        goto L2; // [36] 48
    }
    _861 = (_shift_1715 == 0);
    if (_861 == 0)
    {
        DeRef(_861);
        _861 = NOVALUE;
        goto L3; // [44] 55
    }
    else{
        DeRef(_861);
        _861 = NOVALUE;
    }
L2: 

    /** 		return source*/
    DeRef(_shifted_1719);
    DeRef(_856);
    _856 = NOVALUE;
    DeRef(_859);
    _859 = NOVALUE;
    return _source_1714;
L3: 

    /** 	if not valid_index(source, start) then*/
    RefDS(_source_1714);
    _862 = _3valid_index(_source_1714, _start_1716);
    if (IS_ATOM_INT(_862)) {
        if (_862 != 0){
            DeRef(_862);
            _862 = NOVALUE;
            goto L4; // [62] 86
        }
    }
    else {
        if (DBL_PTR(_862)->dbl != 0.0){
            DeRef(_862);
            _862 = NOVALUE;
            goto L4; // [62] 86
        }
    }
    DeRef(_862);
    _862 = NOVALUE;

    /** 		error:crash("sequence:rotate(): invalid 'start' parameter %d", start)*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_66_1734);
    _msg_inlined_crash_at_66_1734 = EPrintf(-9999999, _864, _start_1716);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_66_1734);

    /** end procedure*/
    goto L5; // [80] 83
L5: 
    DeRefi(_msg_inlined_crash_at_66_1734);
    _msg_inlined_crash_at_66_1734 = NOVALUE;
L4: 

    /** 	if not valid_index(source, stop) then*/
    RefDS(_source_1714);
    _865 = _3valid_index(_source_1714, _stop_1717);
    if (IS_ATOM_INT(_865)) {
        if (_865 != 0){
            DeRef(_865);
            _865 = NOVALUE;
            goto L6; // [93] 117
        }
    }
    else {
        if (DBL_PTR(_865)->dbl != 0.0){
            DeRef(_865);
            _865 = NOVALUE;
            goto L6; // [93] 117
        }
    }
    DeRef(_865);
    _865 = NOVALUE;

    /** 		error:crash("sequence:rotate(): invalid 'stop' parameter %d", stop)*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_97_1740);
    _msg_inlined_crash_at_97_1740 = EPrintf(-9999999, _867, _stop_1717);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_97_1740);

    /** end procedure*/
    goto L7; // [111] 114
L7: 
    DeRefi(_msg_inlined_crash_at_97_1740);
    _msg_inlined_crash_at_97_1740 = NOVALUE;
L6: 

    /** 	len = stop - start + 1*/
    _868 = _stop_1717 - _start_1716;
    if ((long)((unsigned long)_868 +(unsigned long) HIGH_BITS) >= 0){
        _868 = NewDouble((double)_868);
    }
    if (IS_ATOM_INT(_868)) {
        _len_1720 = _868 + 1;
    }
    else
    { // coercing _len_1720 to an integer 1
        _len_1720 = 1+(long)(DBL_PTR(_868)->dbl);
        if( !IS_ATOM_INT(_len_1720) ){
            _len_1720 = (object)DBL_PTR(_len_1720)->dbl;
        }
    }
    DeRef(_868);
    _868 = NOVALUE;

    /** 	lSize = remainder(shift, len)*/
    _lSize_1721 = (_shift_1715 % _len_1720);

    /** 	if lSize = 0 then*/
    if (_lSize_1721 != 0)
    goto L8; // [139] 150

    /** 		return source*/
    DeRef(_shifted_1719);
    DeRef(_856);
    _856 = NOVALUE;
    DeRef(_859);
    _859 = NOVALUE;
    return _source_1714;
L8: 

    /** 	if lSize < 0 then -- convert right shift to left shift*/
    if (_lSize_1721 >= 0)
    goto L9; // [152] 165

    /** 		lSize += len*/
    _lSize_1721 = _lSize_1721 + _len_1720;
L9: 

    /** 	shifted = source[start .. start + lSize-1]*/
    _874 = _start_1716 + _lSize_1721;
    if ((long)((unsigned long)_874 + (unsigned long)HIGH_BITS) >= 0) 
    _874 = NewDouble((double)_874);
    if (IS_ATOM_INT(_874)) {
        _875 = _874 - 1;
    }
    else {
        _875 = NewDouble(DBL_PTR(_874)->dbl - (double)1);
    }
    DeRef(_874);
    _874 = NOVALUE;
    rhs_slice_target = (object_ptr)&_shifted_1719;
    RHS_Slice(_source_1714, _start_1716, _875);

    /** 	source[start .. stop - lSize] = source[start + lSize .. stop]*/
    _877 = _stop_1717 - _lSize_1721;
    if ((long)((unsigned long)_877 +(unsigned long) HIGH_BITS) >= 0){
        _877 = NewDouble((double)_877);
    }
    _878 = _start_1716 + _lSize_1721;
    rhs_slice_target = (object_ptr)&_879;
    RHS_Slice(_source_1714, _878, _stop_1717);
    assign_slice_seq = (s1_ptr *)&_source_1714;
    AssignSlice(_start_1716, _877, _879);
    DeRef(_877);
    _877 = NOVALUE;
    DeRefDS(_879);
    _879 = NOVALUE;

    /** 	source[stop - lSize + 1.. stop] = shifted*/
    _880 = _stop_1717 - _lSize_1721;
    if ((long)((unsigned long)_880 +(unsigned long) HIGH_BITS) >= 0){
        _880 = NewDouble((double)_880);
    }
    if (IS_ATOM_INT(_880)) {
        _881 = _880 + 1;
    }
    else
    _881 = binary_op(PLUS, 1, _880);
    DeRef(_880);
    _880 = NOVALUE;
    assign_slice_seq = (s1_ptr *)&_source_1714;
    AssignSlice(_881, _stop_1717, _shifted_1719);
    DeRef(_881);
    _881 = NOVALUE;

    /** 	return source*/
    DeRefDS(_shifted_1719);
    DeRef(_856);
    _856 = NOVALUE;
    DeRef(_859);
    _859 = NOVALUE;
    DeRef(_875);
    _875 = NOVALUE;
    _878 = NOVALUE;
    return _source_1714;
    ;
}


int _3columnize(int _source_1759, int _cols_1760, int _defval_1761)
{
    int _result_1762 = NOVALUE;
    int _collist_1763 = NOVALUE;
    int _col_1789 = NOVALUE;
    int _915 = NOVALUE;
    int _914 = NOVALUE;
    int _913 = NOVALUE;
    int _912 = NOVALUE;
    int _911 = NOVALUE;
    int _910 = NOVALUE;
    int _909 = NOVALUE;
    int _908 = NOVALUE;
    int _907 = NOVALUE;
    int _906 = NOVALUE;
    int _905 = NOVALUE;
    int _904 = NOVALUE;
    int _903 = NOVALUE;
    int _902 = NOVALUE;
    int _901 = NOVALUE;
    int _900 = NOVALUE;
    int _899 = NOVALUE;
    int _898 = NOVALUE;
    int _896 = NOVALUE;
    int _894 = NOVALUE;
    int _892 = NOVALUE;
    int _890 = NOVALUE;
    int _888 = NOVALUE;
    int _887 = NOVALUE;
    int _886 = NOVALUE;
    int _884 = NOVALUE;
    int _882 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(cols) then*/
    _882 = IS_SEQUENCE(_cols_1760);
    if (_882 == 0)
    {
        _882 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _882 = NOVALUE;
    }

    /** 		collist = cols*/
    Ref(_cols_1760);
    DeRef(_collist_1763);
    _collist_1763 = _cols_1760;
    goto L2; // [18] 28
L1: 

    /** 		collist = {cols}*/
    _0 = _collist_1763;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_cols_1760);
    *((int *)(_2+4)) = _cols_1760;
    _collist_1763 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	if length(collist) = 0 then*/
    if (IS_SEQUENCE(_collist_1763)){
            _884 = SEQ_PTR(_collist_1763)->length;
    }
    else {
        _884 = 1;
    }
    if (_884 != 0)
    goto L3; // [35] 112

    /** 		cols = 0*/
    DeRef(_cols_1760);
    _cols_1760 = 0;

    /** 		for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_1759)){
            _886 = SEQ_PTR(_source_1759)->length;
    }
    else {
        _886 = 1;
    }
    {
        int _i_1772;
        _i_1772 = 1;
L4: 
        if (_i_1772 > _886){
            goto L5; // [49] 86
        }

        /** 			if cols < length(source[i]) then*/
        _2 = (int)SEQ_PTR(_source_1759);
        _887 = (int)*(((s1_ptr)_2)->base + _i_1772);
        if (IS_SEQUENCE(_887)){
                _888 = SEQ_PTR(_887)->length;
        }
        else {
            _888 = 1;
        }
        _887 = NOVALUE;
        if (binary_op_a(GREATEREQ, _cols_1760, _888)){
            _888 = NOVALUE;
            goto L6; // [65] 79
        }
        _888 = NOVALUE;

        /** 				cols = length(source[i])*/
        _2 = (int)SEQ_PTR(_source_1759);
        _890 = (int)*(((s1_ptr)_2)->base + _i_1772);
        DeRef(_cols_1760);
        if (IS_SEQUENCE(_890)){
                _cols_1760 = SEQ_PTR(_890)->length;
        }
        else {
            _cols_1760 = 1;
        }
        _890 = NOVALUE;
L6: 

        /** 		end for*/
        _i_1772 = _i_1772 + 1;
        goto L4; // [81] 56
L5: 
        ;
    }

    /** 		for i = 1 to cols do*/
    Ref(_cols_1760);
    DeRef(_892);
    _892 = _cols_1760;
    {
        int _i_1781;
        _i_1781 = 1;
L7: 
        if (binary_op_a(GREATER, _i_1781, _892)){
            goto L8; // [91] 111
        }

        /** 			collist &= i*/
        Ref(_i_1781);
        Append(&_collist_1763, _collist_1763, _i_1781);

        /** 		end for*/
        _0 = _i_1781;
        if (IS_ATOM_INT(_i_1781)) {
            _i_1781 = _i_1781 + 1;
            if ((long)((unsigned long)_i_1781 +(unsigned long) HIGH_BITS) >= 0){
                _i_1781 = NewDouble((double)_i_1781);
            }
        }
        else {
            _i_1781 = binary_op_a(PLUS, _i_1781, 1);
        }
        DeRef(_0);
        goto L7; // [106] 98
L8: 
        ;
        DeRef(_i_1781);
    }
L3: 

    /** 	result = repeat({}, length(collist))*/
    if (IS_SEQUENCE(_collist_1763)){
            _894 = SEQ_PTR(_collist_1763)->length;
    }
    else {
        _894 = 1;
    }
    DeRef(_result_1762);
    _result_1762 = Repeat(_5, _894);
    _894 = NOVALUE;

    /** 	for i = 1 to length(collist) do*/
    if (IS_SEQUENCE(_collist_1763)){
            _896 = SEQ_PTR(_collist_1763)->length;
    }
    else {
        _896 = 1;
    }
    {
        int _i_1787;
        _i_1787 = 1;
L9: 
        if (_i_1787 > _896){
            goto LA; // [126] 271
        }

        /** 		integer col = collist[i]*/
        _2 = (int)SEQ_PTR(_collist_1763);
        _col_1789 = (int)*(((s1_ptr)_2)->base + _i_1787);
        if (!IS_ATOM_INT(_col_1789))
        _col_1789 = (long)DBL_PTR(_col_1789)->dbl;

        /** 		for j = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_1759)){
                _898 = SEQ_PTR(_source_1759)->length;
        }
        else {
            _898 = 1;
        }
        {
            int _j_1792;
            _j_1792 = 1;
LB: 
            if (_j_1792 > _898){
                goto LC; // [146] 262
            }

            /** 			if sequence(source[j]) and length(source[j]) < col then*/
            _2 = (int)SEQ_PTR(_source_1759);
            _899 = (int)*(((s1_ptr)_2)->base + _j_1792);
            _900 = IS_SEQUENCE(_899);
            _899 = NOVALUE;
            if (_900 == 0) {
                goto LD; // [162] 198
            }
            _2 = (int)SEQ_PTR(_source_1759);
            _902 = (int)*(((s1_ptr)_2)->base + _j_1792);
            if (IS_SEQUENCE(_902)){
                    _903 = SEQ_PTR(_902)->length;
            }
            else {
                _903 = 1;
            }
            _902 = NOVALUE;
            _904 = (_903 < _col_1789);
            _903 = NOVALUE;
            if (_904 == 0)
            {
                DeRef(_904);
                _904 = NOVALUE;
                goto LD; // [178] 198
            }
            else{
                DeRef(_904);
                _904 = NOVALUE;
            }

            /** 				result[i] = append(result[i], defval)*/
            _2 = (int)SEQ_PTR(_result_1762);
            _905 = (int)*(((s1_ptr)_2)->base + _i_1787);
            Ref(_defval_1761);
            Append(&_906, _905, _defval_1761);
            _905 = NOVALUE;
            _2 = (int)SEQ_PTR(_result_1762);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_1762 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_1787);
            _1 = *(int *)_2;
            *(int *)_2 = _906;
            if( _1 != _906 ){
                DeRefDS(_1);
            }
            _906 = NOVALUE;
            goto LE; // [195] 255
LD: 

            /** 				if atom(source[j]) then*/
            _2 = (int)SEQ_PTR(_source_1759);
            _907 = (int)*(((s1_ptr)_2)->base + _j_1792);
            _908 = IS_ATOM(_907);
            _907 = NOVALUE;
            if (_908 == 0)
            {
                _908 = NOVALUE;
                goto LF; // [207] 231
            }
            else{
                _908 = NOVALUE;
            }

            /** 					result[i] = append(result[i], source[j])*/
            _2 = (int)SEQ_PTR(_result_1762);
            _909 = (int)*(((s1_ptr)_2)->base + _i_1787);
            _2 = (int)SEQ_PTR(_source_1759);
            _910 = (int)*(((s1_ptr)_2)->base + _j_1792);
            Ref(_910);
            Append(&_911, _909, _910);
            _909 = NOVALUE;
            _910 = NOVALUE;
            _2 = (int)SEQ_PTR(_result_1762);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_1762 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_1787);
            _1 = *(int *)_2;
            *(int *)_2 = _911;
            if( _1 != _911 ){
                DeRefDS(_1);
            }
            _911 = NOVALUE;
            goto L10; // [228] 254
LF: 

            /** 					result[i] = append(result[i], source[j][col])*/
            _2 = (int)SEQ_PTR(_result_1762);
            _912 = (int)*(((s1_ptr)_2)->base + _i_1787);
            _2 = (int)SEQ_PTR(_source_1759);
            _913 = (int)*(((s1_ptr)_2)->base + _j_1792);
            _2 = (int)SEQ_PTR(_913);
            _914 = (int)*(((s1_ptr)_2)->base + _col_1789);
            _913 = NOVALUE;
            Ref(_914);
            Append(&_915, _912, _914);
            _912 = NOVALUE;
            _914 = NOVALUE;
            _2 = (int)SEQ_PTR(_result_1762);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_1762 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_1787);
            _1 = *(int *)_2;
            *(int *)_2 = _915;
            if( _1 != _915 ){
                DeRefDS(_1);
            }
            _915 = NOVALUE;
L10: 
LE: 

            /** 		end for*/
            _j_1792 = _j_1792 + 1;
            goto LB; // [257] 153
LC: 
            ;
        }

        /** 	end for*/
        _i_1787 = _i_1787 + 1;
        goto L9; // [266] 133
LA: 
        ;
    }

    /** 	return result*/
    DeRefDS(_source_1759);
    DeRef(_cols_1760);
    DeRef(_defval_1761);
    DeRef(_collist_1763);
    _887 = NOVALUE;
    _890 = NOVALUE;
    _902 = NOVALUE;
    return _result_1762;
    ;
}


int _3apply(int _source_1817, int _rid_1818, int _userdata_1819)
{
    int _919 = NOVALUE;
    int _918 = NOVALUE;
    int _917 = NOVALUE;
    int _916 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_1818)) {
        _1 = (long)(DBL_PTR(_rid_1818)->dbl);
        if (UNIQUE(DBL_PTR(_rid_1818)) && (DBL_PTR(_rid_1818)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_1818);
        _rid_1818 = _1;
    }

    /** 	for a = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_1817)){
            _916 = SEQ_PTR(_source_1817)->length;
    }
    else {
        _916 = 1;
    }
    {
        int _a_1821;
        _a_1821 = 1;
L1: 
        if (_a_1821 > _916){
            goto L2; // [12] 44
        }

        /** 		source[a] = call_func(rid, {source[a], userdata})*/
        _2 = (int)SEQ_PTR(_source_1817);
        _917 = (int)*(((s1_ptr)_2)->base + _a_1821);
        Ref(_userdata_1819);
        Ref(_917);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _917;
        ((int *)_2)[2] = _userdata_1819;
        _918 = MAKE_SEQ(_1);
        _917 = NOVALUE;
        _1 = (int)SEQ_PTR(_918);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_rid_1818].addr;
        Ref(*(int *)(_2+4));
        Ref(*(int *)(_2+8));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4), 
                            *(int *)(_2+8)
                             );
        DeRef(_919);
        _919 = _1;
        DeRefDS(_918);
        _918 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_1817);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_1817 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_1821);
        _1 = *(int *)_2;
        *(int *)_2 = _919;
        if( _1 != _919 ){
            DeRef(_1);
        }
        _919 = NOVALUE;

        /** 	end for*/
        _a_1821 = _a_1821 + 1;
        goto L1; // [39] 19
L2: 
        ;
    }

    /** 	return source*/
    DeRef(_userdata_1819);
    return _source_1817;
    ;
}


int _3mapping(int _source_arg_1828, int _from_set_1829, int _to_set_1830, int _one_level_1831)
{
    int _pos_1832 = NOVALUE;
    int _941 = NOVALUE;
    int _940 = NOVALUE;
    int _939 = NOVALUE;
    int _938 = NOVALUE;
    int _937 = NOVALUE;
    int _936 = NOVALUE;
    int _935 = NOVALUE;
    int _934 = NOVALUE;
    int _933 = NOVALUE;
    int _931 = NOVALUE;
    int _929 = NOVALUE;
    int _928 = NOVALUE;
    int _927 = NOVALUE;
    int _925 = NOVALUE;
    int _924 = NOVALUE;
    int _923 = NOVALUE;
    int _922 = NOVALUE;
    int _920 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_one_level_1831)) {
        _1 = (long)(DBL_PTR(_one_level_1831)->dbl);
        if (UNIQUE(DBL_PTR(_one_level_1831)) && (DBL_PTR(_one_level_1831)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_one_level_1831);
        _one_level_1831 = _1;
    }

    /** 	if atom(source_arg) then*/
    _920 = IS_ATOM(_source_arg_1828);
    if (_920 == 0)
    {
        _920 = NOVALUE;
        goto L1; // [14] 57
    }
    else{
        _920 = NOVALUE;
    }

    /** 		pos = find(source_arg, from_set)*/
    _pos_1832 = find_from(_source_arg_1828, _from_set_1829, 1);

    /** 		if pos >= 1  and pos <= length(to_set) then*/
    _922 = (_pos_1832 >= 1);
    if (_922 == 0) {
        goto L2; // [32] 167
    }
    if (IS_SEQUENCE(_to_set_1830)){
            _924 = SEQ_PTR(_to_set_1830)->length;
    }
    else {
        _924 = 1;
    }
    _925 = (_pos_1832 <= _924);
    _924 = NOVALUE;
    if (_925 == 0)
    {
        DeRef(_925);
        _925 = NOVALUE;
        goto L2; // [44] 167
    }
    else{
        DeRef(_925);
        _925 = NOVALUE;
    }

    /** 			source_arg = to_set[pos]*/
    DeRef(_source_arg_1828);
    _2 = (int)SEQ_PTR(_to_set_1830);
    _source_arg_1828 = (int)*(((s1_ptr)_2)->base + _pos_1832);
    Ref(_source_arg_1828);
    goto L2; // [54] 167
L1: 

    /** 		for i = 1 to length(source_arg) do*/
    if (IS_SEQUENCE(_source_arg_1828)){
            _927 = SEQ_PTR(_source_arg_1828)->length;
    }
    else {
        _927 = 1;
    }
    {
        int _i_1844;
        _i_1844 = 1;
L3: 
        if (_i_1844 > _927){
            goto L4; // [62] 166
        }

        /** 			if atom(source_arg[i]) or one_level then*/
        _2 = (int)SEQ_PTR(_source_arg_1828);
        _928 = (int)*(((s1_ptr)_2)->base + _i_1844);
        _929 = IS_ATOM(_928);
        _928 = NOVALUE;
        if (_929 != 0) {
            goto L5; // [78] 87
        }
        if (_one_level_1831 == 0)
        {
            goto L6; // [83] 135
        }
        else{
        }
L5: 

        /** 				pos = find(source_arg[i], from_set)*/
        _2 = (int)SEQ_PTR(_source_arg_1828);
        _931 = (int)*(((s1_ptr)_2)->base + _i_1844);
        _pos_1832 = find_from(_931, _from_set_1829, 1);
        _931 = NOVALUE;

        /** 				if pos >= 1  and pos <= length(to_set) then*/
        _933 = (_pos_1832 >= 1);
        if (_933 == 0) {
            goto L7; // [106] 159
        }
        if (IS_SEQUENCE(_to_set_1830)){
                _935 = SEQ_PTR(_to_set_1830)->length;
        }
        else {
            _935 = 1;
        }
        _936 = (_pos_1832 <= _935);
        _935 = NOVALUE;
        if (_936 == 0)
        {
            DeRef(_936);
            _936 = NOVALUE;
            goto L7; // [118] 159
        }
        else{
            DeRef(_936);
            _936 = NOVALUE;
        }

        /** 					source_arg[i] = to_set[pos]*/
        _2 = (int)SEQ_PTR(_to_set_1830);
        _937 = (int)*(((s1_ptr)_2)->base + _pos_1832);
        Ref(_937);
        _2 = (int)SEQ_PTR(_source_arg_1828);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_arg_1828 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1844);
        _1 = *(int *)_2;
        *(int *)_2 = _937;
        if( _1 != _937 ){
            DeRef(_1);
        }
        _937 = NOVALUE;
        goto L7; // [132] 159
L6: 

        /** 				source_arg[i] = mapping(source_arg[i], from_set, to_set)*/
        _2 = (int)SEQ_PTR(_source_arg_1828);
        _938 = (int)*(((s1_ptr)_2)->base + _i_1844);
        RefDS(_from_set_1829);
        DeRef(_939);
        _939 = _from_set_1829;
        RefDS(_to_set_1830);
        DeRef(_940);
        _940 = _to_set_1830;
        Ref(_938);
        _941 = _3mapping(_938, _939, _940, 0);
        _938 = NOVALUE;
        _939 = NOVALUE;
        _940 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_arg_1828);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_arg_1828 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1844);
        _1 = *(int *)_2;
        *(int *)_2 = _941;
        if( _1 != _941 ){
            DeRef(_1);
        }
        _941 = NOVALUE;
L7: 

        /** 		end for*/
        _i_1844 = _i_1844 + 1;
        goto L3; // [161] 69
L4: 
        ;
    }
L2: 

    /** 	return source_arg*/
    DeRefDS(_from_set_1829);
    DeRefDS(_to_set_1830);
    DeRef(_922);
    _922 = NOVALUE;
    DeRef(_933);
    _933 = NOVALUE;
    return _source_arg_1828;
    ;
}


int _3reverse(int _target_1865, int _pFrom_1866, int _pTo_1867)
{
    int _uppr_1868 = NOVALUE;
    int _n_1869 = NOVALUE;
    int _lLimit_1870 = NOVALUE;
    int _t_1871 = NOVALUE;
    int _956 = NOVALUE;
    int _955 = NOVALUE;
    int _954 = NOVALUE;
    int _952 = NOVALUE;
    int _951 = NOVALUE;
    int _949 = NOVALUE;
    int _947 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pFrom_1866)) {
        _1 = (long)(DBL_PTR(_pFrom_1866)->dbl);
        if (UNIQUE(DBL_PTR(_pFrom_1866)) && (DBL_PTR(_pFrom_1866)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pFrom_1866);
        _pFrom_1866 = _1;
    }
    if (!IS_ATOM_INT(_pTo_1867)) {
        _1 = (long)(DBL_PTR(_pTo_1867)->dbl);
        if (UNIQUE(DBL_PTR(_pTo_1867)) && (DBL_PTR(_pTo_1867)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pTo_1867);
        _pTo_1867 = _1;
    }

    /** 	n = length(target)*/
    if (IS_SEQUENCE(_target_1865)){
            _n_1869 = SEQ_PTR(_target_1865)->length;
    }
    else {
        _n_1869 = 1;
    }

    /** 	if n < 2 then*/
    if (_n_1869 >= 2)
    goto L1; // [18] 29

    /** 		return target*/
    DeRef(_t_1871);
    return _target_1865;
L1: 

    /** 	if pFrom < 1 then*/
    if (_pFrom_1866 >= 1)
    goto L2; // [31] 43

    /** 		pFrom = 1*/
    _pFrom_1866 = 1;
L2: 

    /** 	if pTo < 1 then*/
    if (_pTo_1867 >= 1)
    goto L3; // [45] 58

    /** 		pTo = n + pTo*/
    _pTo_1867 = _n_1869 + _pTo_1867;
L3: 

    /** 	if pTo < pFrom or pFrom >= n then*/
    _947 = (_pTo_1867 < _pFrom_1866);
    if (_947 != 0) {
        goto L4; // [64] 77
    }
    _949 = (_pFrom_1866 >= _n_1869);
    if (_949 == 0)
    {
        DeRef(_949);
        _949 = NOVALUE;
        goto L5; // [73] 84
    }
    else{
        DeRef(_949);
        _949 = NOVALUE;
    }
L4: 

    /** 		return target*/
    DeRef(_t_1871);
    DeRef(_947);
    _947 = NOVALUE;
    return _target_1865;
L5: 

    /** 	if pTo > n then*/
    if (_pTo_1867 <= _n_1869)
    goto L6; // [86] 98

    /** 		pTo = n*/
    _pTo_1867 = _n_1869;
L6: 

    /** 	lLimit = floor((pFrom+pTo-1)/2)*/
    _951 = _pFrom_1866 + _pTo_1867;
    if ((long)((unsigned long)_951 + (unsigned long)HIGH_BITS) >= 0) 
    _951 = NewDouble((double)_951);
    if (IS_ATOM_INT(_951)) {
        _952 = _951 - 1;
        if ((long)((unsigned long)_952 +(unsigned long) HIGH_BITS) >= 0){
            _952 = NewDouble((double)_952);
        }
    }
    else {
        _952 = NewDouble(DBL_PTR(_951)->dbl - (double)1);
    }
    DeRef(_951);
    _951 = NOVALUE;
    if (IS_ATOM_INT(_952)) {
        _lLimit_1870 = _952 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _952, 2);
        _lLimit_1870 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_952);
    _952 = NOVALUE;
    if (!IS_ATOM_INT(_lLimit_1870)) {
        _1 = (long)(DBL_PTR(_lLimit_1870)->dbl);
        if (UNIQUE(DBL_PTR(_lLimit_1870)) && (DBL_PTR(_lLimit_1870)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lLimit_1870);
        _lLimit_1870 = _1;
    }

    /** 	t = target*/
    Ref(_target_1865);
    DeRef(_t_1871);
    _t_1871 = _target_1865;

    /** 	uppr = pTo*/
    _uppr_1868 = _pTo_1867;

    /** 	for lowr = pFrom to lLimit do*/
    _954 = _lLimit_1870;
    {
        int _lowr_1890;
        _lowr_1890 = _pFrom_1866;
L7: 
        if (_lowr_1890 > _954){
            goto L8; // [135] 177
        }

        /** 		t[uppr] = target[lowr]*/
        _2 = (int)SEQ_PTR(_target_1865);
        _955 = (int)*(((s1_ptr)_2)->base + _lowr_1890);
        Ref(_955);
        _2 = (int)SEQ_PTR(_t_1871);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_1871 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _uppr_1868);
        _1 = *(int *)_2;
        *(int *)_2 = _955;
        if( _1 != _955 ){
            DeRef(_1);
        }
        _955 = NOVALUE;

        /** 		t[lowr] = target[uppr]*/
        _2 = (int)SEQ_PTR(_target_1865);
        _956 = (int)*(((s1_ptr)_2)->base + _uppr_1868);
        Ref(_956);
        _2 = (int)SEQ_PTR(_t_1871);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_1871 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lowr_1890);
        _1 = *(int *)_2;
        *(int *)_2 = _956;
        if( _1 != _956 ){
            DeRef(_1);
        }
        _956 = NOVALUE;

        /** 		uppr -= 1*/
        _uppr_1868 = _uppr_1868 - 1;

        /** 	end for*/
        _lowr_1890 = _lowr_1890 + 1;
        goto L7; // [172] 142
L8: 
        ;
    }

    /** 	return t*/
    DeRef(_target_1865);
    DeRef(_947);
    _947 = NOVALUE;
    return _t_1871;
    ;
}


int _3shuffle(int _seq_1897)
{
    int _fromIdx_1901 = NOVALUE;
    int _swapValue_1903 = NOVALUE;
    int _961 = NOVALUE;
    int _958 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for toIdx = length(seq) to 2 by -1 do*/
    if (IS_SEQUENCE(_seq_1897)){
            _958 = SEQ_PTR(_seq_1897)->length;
    }
    else {
        _958 = 1;
    }
    {
        int _toIdx_1899;
        _toIdx_1899 = _958;
L1: 
        if (_toIdx_1899 < 2){
            goto L2; // [6] 51
        }

        /** 		integer fromIdx = rand(toIdx)*/
        _fromIdx_1901 = good_rand() % ((unsigned)_toIdx_1899) + 1;

        /** 		object swapValue = seq[fromIdx]*/
        DeRef(_swapValue_1903);
        _2 = (int)SEQ_PTR(_seq_1897);
        _swapValue_1903 = (int)*(((s1_ptr)_2)->base + _fromIdx_1901);
        Ref(_swapValue_1903);

        /** 		seq[fromIdx] = seq[toIdx]*/
        _2 = (int)SEQ_PTR(_seq_1897);
        _961 = (int)*(((s1_ptr)_2)->base + _toIdx_1899);
        Ref(_961);
        _2 = (int)SEQ_PTR(_seq_1897);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _seq_1897 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _fromIdx_1901);
        _1 = *(int *)_2;
        *(int *)_2 = _961;
        if( _1 != _961 ){
            DeRef(_1);
        }
        _961 = NOVALUE;

        /** 		seq[toIdx] = swapValue*/
        Ref(_swapValue_1903);
        _2 = (int)SEQ_PTR(_seq_1897);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _seq_1897 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _toIdx_1899);
        _1 = *(int *)_2;
        *(int *)_2 = _swapValue_1903;
        DeRef(_1);
        DeRef(_swapValue_1903);
        _swapValue_1903 = NOVALUE;

        /** 	end for*/
        _toIdx_1899 = _toIdx_1899 + -1;
        goto L1; // [46] 13
L2: 
        ;
    }

    /** 	return seq*/
    return _seq_1897;
    ;
}


int _3series(int _start_1908, int _increment_1909, int _count_1910, int _op_1911)
{
    int _result_1913 = NOVALUE;
    int _973 = NOVALUE;
    int _970 = NOVALUE;
    int _964 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_1910)) {
        _1 = (long)(DBL_PTR(_count_1910)->dbl);
        if (UNIQUE(DBL_PTR(_count_1910)) && (DBL_PTR(_count_1910)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_1910);
        _count_1910 = _1;
    }
    if (!IS_ATOM_INT(_op_1911)) {
        _1 = (long)(DBL_PTR(_op_1911)->dbl);
        if (UNIQUE(DBL_PTR(_op_1911)) && (DBL_PTR(_op_1911)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_1911);
        _op_1911 = _1;
    }

    /** 	if count < 0 then*/
    if (_count_1910 >= 0)
    goto L1; // [11] 22

    /** 		return 0*/
    DeRef(_start_1908);
    DeRef(_increment_1909);
    DeRef(_result_1913);
    return 0;
L1: 

    /** 	if not binop_ok(start, increment) then*/
    Ref(_start_1908);
    Ref(_increment_1909);
    _964 = _3binop_ok(_start_1908, _increment_1909);
    if (IS_ATOM_INT(_964)) {
        if (_964 != 0){
            DeRef(_964);
            _964 = NOVALUE;
            goto L2; // [29] 39
        }
    }
    else {
        if (DBL_PTR(_964)->dbl != 0.0){
            DeRef(_964);
            _964 = NOVALUE;
            goto L2; // [29] 39
        }
    }
    DeRef(_964);
    _964 = NOVALUE;

    /** 		return 0*/
    DeRef(_start_1908);
    DeRef(_increment_1909);
    DeRef(_result_1913);
    return 0;
L2: 

    /** 	if count = 0 then*/
    if (_count_1910 != 0)
    goto L3; // [41] 52

    /** 		return {}*/
    RefDS(_5);
    DeRef(_start_1908);
    DeRef(_increment_1909);
    DeRef(_result_1913);
    return _5;
L3: 

    /** 	result = repeat(0, count )*/
    DeRef(_result_1913);
    _result_1913 = Repeat(0, _count_1910);

    /** 	result[1] = start*/
    Ref(_start_1908);
    _2 = (int)SEQ_PTR(_result_1913);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _result_1913 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _start_1908;

    /** 	switch op do*/
    _0 = _op_1911;
    switch ( _0 ){ 

        /** 		case '+' then*/
        case 43:

        /** 			for i = 2 to count  do*/
        _970 = _count_1910;
        {
            int _i_1926;
            _i_1926 = 2;
L4: 
            if (_i_1926 > _970){
                goto L5; // [80] 106
            }

            /** 				start += increment*/
            _0 = _start_1908;
            if (IS_ATOM_INT(_start_1908) && IS_ATOM_INT(_increment_1909)) {
                _start_1908 = _start_1908 + _increment_1909;
                if ((long)((unsigned long)_start_1908 + (unsigned long)HIGH_BITS) >= 0) 
                _start_1908 = NewDouble((double)_start_1908);
            }
            else {
                _start_1908 = binary_op(PLUS, _start_1908, _increment_1909);
            }
            DeRef(_0);

            /** 				result[i] = start*/
            Ref(_start_1908);
            _2 = (int)SEQ_PTR(_result_1913);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_1913 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_1926);
            _1 = *(int *)_2;
            *(int *)_2 = _start_1908;
            DeRef(_1);

            /** 			end for*/
            _i_1926 = _i_1926 + 1;
            goto L4; // [101] 87
L5: 
            ;
        }
        goto L6; // [106] 156

        /** 		case '*' then*/
        case 42:

        /** 			for i = 2 to count do*/
        _973 = _count_1910;
        {
            int _i_1932;
            _i_1932 = 2;
L7: 
            if (_i_1932 > _973){
                goto L8; // [117] 143
            }

            /** 				start *= increment*/
            _0 = _start_1908;
            if (IS_ATOM_INT(_start_1908) && IS_ATOM_INT(_increment_1909)) {
                if (_start_1908 == (short)_start_1908 && _increment_1909 <= INT15 && _increment_1909 >= -INT15)
                _start_1908 = _start_1908 * _increment_1909;
                else
                _start_1908 = NewDouble(_start_1908 * (double)_increment_1909);
            }
            else {
                _start_1908 = binary_op(MULTIPLY, _start_1908, _increment_1909);
            }
            DeRef(_0);

            /** 				result[i] = start*/
            Ref(_start_1908);
            _2 = (int)SEQ_PTR(_result_1913);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_1913 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_1932);
            _1 = *(int *)_2;
            *(int *)_2 = _start_1908;
            DeRef(_1);

            /** 			end for*/
            _i_1932 = _i_1932 + 1;
            goto L7; // [138] 124
L8: 
            ;
        }
        goto L6; // [143] 156

        /** 		case else*/
        default:

        /** 			return 0*/
        DeRef(_start_1908);
        DeRef(_increment_1909);
        DeRef(_result_1913);
        return 0;
    ;}L6: 

    /** 	return result*/
    DeRef(_start_1908);
    DeRef(_increment_1909);
    return _result_1913;
    ;
}


int _3repeat_pattern(int _pattern_1938, int _count_1939)
{
    int _ls_1940 = NOVALUE;
    int _result_1941 = NOVALUE;
    int _982 = NOVALUE;
    int _981 = NOVALUE;
    int _980 = NOVALUE;
    int _979 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_1939)) {
        _1 = (long)(DBL_PTR(_count_1939)->dbl);
        if (UNIQUE(DBL_PTR(_count_1939)) && (DBL_PTR(_count_1939)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_1939);
        _count_1939 = _1;
    }

    /** 	if count<=0 then*/
    if (_count_1939 > 0)
    goto L1; // [7] 18

    /** 		return {}*/
    RefDS(_5);
    DeRef(_pattern_1938);
    DeRef(_result_1941);
    return _5;
L1: 

    /** 	ls = length(pattern)*/
    if (IS_SEQUENCE(_pattern_1938)){
            _ls_1940 = SEQ_PTR(_pattern_1938)->length;
    }
    else {
        _ls_1940 = 1;
    }

    /** 	count *= ls*/
    _count_1939 = _count_1939 * _ls_1940;

    /** 	result=repeat(0,count)*/
    DeRef(_result_1941);
    _result_1941 = Repeat(0, _count_1939);

    /** 	for i=1 to count by ls do*/
    _979 = _ls_1940;
    _980 = _count_1939;
    {
        int _i_1948;
        _i_1948 = 1;
L2: 
        if (_i_1948 > _980){
            goto L3; // [49] 78
        }

        /** 		result[i..i+ls-1] = pattern*/
        _981 = _i_1948 + _ls_1940;
        if ((long)((unsigned long)_981 + (unsigned long)HIGH_BITS) >= 0) 
        _981 = NewDouble((double)_981);
        if (IS_ATOM_INT(_981)) {
            _982 = _981 - 1;
        }
        else {
            _982 = NewDouble(DBL_PTR(_981)->dbl - (double)1);
        }
        DeRef(_981);
        _981 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_result_1941;
        AssignSlice(_i_1948, _982, _pattern_1938);
        DeRef(_982);
        _982 = NOVALUE;

        /** 	end for*/
        _i_1948 = _i_1948 + _979;
        goto L2; // [73] 56
L3: 
        ;
    }

    /** 	return result*/
    DeRef(_pattern_1938);
    return _result_1941;
    ;
}


int _3pad_head(int _target_1955, int _size_1956, int _ch_1957)
{
    int _988 = NOVALUE;
    int _987 = NOVALUE;
    int _986 = NOVALUE;
    int _985 = NOVALUE;
    int _983 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_1956)) {
        _1 = (long)(DBL_PTR(_size_1956)->dbl);
        if (UNIQUE(DBL_PTR(_size_1956)) && (DBL_PTR(_size_1956)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_1956);
        _size_1956 = _1;
    }

    /** 	if size <= length(target) then*/
    if (IS_SEQUENCE(_target_1955)){
            _983 = SEQ_PTR(_target_1955)->length;
    }
    else {
        _983 = 1;
    }
    if (_size_1956 > _983)
    goto L1; // [10] 21

    /** 		return target*/
    DeRef(_ch_1957);
    return _target_1955;
L1: 

    /** 	return repeat(ch, size - length(target)) & target*/
    if (IS_SEQUENCE(_target_1955)){
            _985 = SEQ_PTR(_target_1955)->length;
    }
    else {
        _985 = 1;
    }
    _986 = _size_1956 - _985;
    _985 = NOVALUE;
    _987 = Repeat(_ch_1957, _986);
    _986 = NOVALUE;
    if (IS_SEQUENCE(_987) && IS_ATOM(_target_1955)) {
        Ref(_target_1955);
        Append(&_988, _987, _target_1955);
    }
    else if (IS_ATOM(_987) && IS_SEQUENCE(_target_1955)) {
    }
    else {
        Concat((object_ptr)&_988, _987, _target_1955);
        DeRefDS(_987);
        _987 = NOVALUE;
    }
    DeRef(_987);
    _987 = NOVALUE;
    DeRef(_target_1955);
    DeRef(_ch_1957);
    return _988;
    ;
}


int _3pad_tail(int _target_1967, int _size_1968, int _ch_1969)
{
    int _994 = NOVALUE;
    int _993 = NOVALUE;
    int _992 = NOVALUE;
    int _991 = NOVALUE;
    int _989 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_1968)) {
        _1 = (long)(DBL_PTR(_size_1968)->dbl);
        if (UNIQUE(DBL_PTR(_size_1968)) && (DBL_PTR(_size_1968)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_1968);
        _size_1968 = _1;
    }

    /** 	if size <= length(target) then*/
    if (IS_SEQUENCE(_target_1967)){
            _989 = SEQ_PTR(_target_1967)->length;
    }
    else {
        _989 = 1;
    }
    if (_size_1968 > _989)
    goto L1; // [10] 21

    /** 		return target*/
    DeRef(_ch_1969);
    return _target_1967;
L1: 

    /** 	return target & repeat(ch, size - length(target))*/
    if (IS_SEQUENCE(_target_1967)){
            _991 = SEQ_PTR(_target_1967)->length;
    }
    else {
        _991 = 1;
    }
    _992 = _size_1968 - _991;
    _991 = NOVALUE;
    _993 = Repeat(_ch_1969, _992);
    _992 = NOVALUE;
    if (IS_SEQUENCE(_target_1967) && IS_ATOM(_993)) {
    }
    else if (IS_ATOM(_target_1967) && IS_SEQUENCE(_993)) {
        Ref(_target_1967);
        Prepend(&_994, _993, _target_1967);
    }
    else {
        Concat((object_ptr)&_994, _target_1967, _993);
    }
    DeRefDS(_993);
    _993 = NOVALUE;
    DeRef(_target_1967);
    DeRef(_ch_1969);
    return _994;
    ;
}


int _3add_item(int _needle_1979, int _haystack_1980, int _pOrder_1981)
{
    int _msg_inlined_crash_at_110_1999 = NOVALUE;
    int _1003 = NOVALUE;
    int _1002 = NOVALUE;
    int _1001 = NOVALUE;
    int _1000 = NOVALUE;
    int _999 = NOVALUE;
    int _998 = NOVALUE;
    int _995 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pOrder_1981)) {
        _1 = (long)(DBL_PTR(_pOrder_1981)->dbl);
        if (UNIQUE(DBL_PTR(_pOrder_1981)) && (DBL_PTR(_pOrder_1981)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pOrder_1981);
        _pOrder_1981 = _1;
    }

    /** 	if find(needle, haystack) then*/
    _995 = find_from(_needle_1979, _haystack_1980, 1);
    if (_995 == 0)
    {
        _995 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _995 = NOVALUE;
    }

    /** 		return haystack*/
    DeRef(_needle_1979);
    return _haystack_1980;
L1: 

    /** 	switch pOrder do*/
    _0 = _pOrder_1981;
    switch ( _0 ){ 

        /** 		case ADD_PREPEND then*/
        case 1:

        /** 			return prepend(haystack, needle)*/
        Ref(_needle_1979);
        Prepend(&_998, _haystack_1980, _needle_1979);
        DeRef(_needle_1979);
        DeRefDS(_haystack_1980);
        return _998;
        goto L2; // [45] 130

        /** 		case ADD_APPEND then*/
        case 2:

        /** 			return append(haystack, needle)*/
        Ref(_needle_1979);
        Append(&_999, _haystack_1980, _needle_1979);
        DeRef(_needle_1979);
        DeRefDS(_haystack_1980);
        DeRef(_998);
        _998 = NOVALUE;
        return _999;
        goto L2; // [61] 130

        /** 		case ADD_SORT_UP then*/
        case 3:

        /** 			return stdsort:sort(append(haystack, needle))*/
        Ref(_needle_1979);
        Append(&_1000, _haystack_1980, _needle_1979);
        _1001 = _7sort(_1000, 1);
        _1000 = NOVALUE;
        DeRef(_needle_1979);
        DeRefDS(_haystack_1980);
        DeRef(_998);
        _998 = NOVALUE;
        DeRef(_999);
        _999 = NOVALUE;
        return _1001;
        goto L2; // [82] 130

        /** 		case ADD_SORT_DOWN then*/
        case 4:

        /** 			return stdsort:sort(append(haystack, needle), stdsort:DESCENDING)*/
        Ref(_needle_1979);
        Append(&_1002, _haystack_1980, _needle_1979);
        _1003 = _7sort(_1002, -1);
        _1002 = NOVALUE;
        DeRef(_needle_1979);
        DeRefDS(_haystack_1980);
        DeRef(_998);
        _998 = NOVALUE;
        DeRef(_999);
        _999 = NOVALUE;
        DeRef(_1001);
        _1001 = NOVALUE;
        return _1003;
        goto L2; // [103] 130

        /** 		case else*/
        default:

        /** 			error:crash("sequence.e:add_item() invalid Order argument '%d'", pOrder)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_110_1999);
        _msg_inlined_crash_at_110_1999 = EPrintf(-9999999, _1004, _pOrder_1981);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_110_1999);

        /** end procedure*/
        goto L3; // [124] 127
L3: 
        DeRefi(_msg_inlined_crash_at_110_1999);
        _msg_inlined_crash_at_110_1999 = NOVALUE;
    ;}L2: 

    /** 	return haystack*/
    DeRef(_needle_1979);
    DeRef(_998);
    _998 = NOVALUE;
    DeRef(_999);
    _999 = NOVALUE;
    DeRef(_1001);
    _1001 = NOVALUE;
    DeRef(_1003);
    _1003 = NOVALUE;
    return _haystack_1980;
    ;
}


int _3remove_item(int _needle_2002, int _haystack_2003)
{
    int _lIdx_2004 = NOVALUE;
    int _1020 = NOVALUE;
    int _1019 = NOVALUE;
    int _1018 = NOVALUE;
    int _1017 = NOVALUE;
    int _1016 = NOVALUE;
    int _1015 = NOVALUE;
    int _1014 = NOVALUE;
    int _1013 = NOVALUE;
    int _1012 = NOVALUE;
    int _1010 = NOVALUE;
    int _1009 = NOVALUE;
    int _1008 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lIdx = find(needle, haystack)*/
    _lIdx_2004 = find_from(_needle_2002, _haystack_2003, 1);

    /** 	if not lIdx then*/
    if (_lIdx_2004 != 0)
    goto L1; // [14] 24

    /** 		return haystack*/
    DeRef(_needle_2002);
    return _haystack_2003;
L1: 

    /** 	if lIdx = 1 then*/
    if (_lIdx_2004 != 1)
    goto L2; // [26] 47

    /** 		return haystack[2 .. $]*/
    if (IS_SEQUENCE(_haystack_2003)){
            _1008 = SEQ_PTR(_haystack_2003)->length;
    }
    else {
        _1008 = 1;
    }
    rhs_slice_target = (object_ptr)&_1009;
    RHS_Slice(_haystack_2003, 2, _1008);
    DeRef(_needle_2002);
    DeRefDS(_haystack_2003);
    return _1009;
    goto L3; // [44] 109
L2: 

    /** 	elsif lIdx = length(haystack) then*/
    if (IS_SEQUENCE(_haystack_2003)){
            _1010 = SEQ_PTR(_haystack_2003)->length;
    }
    else {
        _1010 = 1;
    }
    if (_lIdx_2004 != _1010)
    goto L4; // [52] 77

    /** 		return haystack[1 .. $-1]*/
    if (IS_SEQUENCE(_haystack_2003)){
            _1012 = SEQ_PTR(_haystack_2003)->length;
    }
    else {
        _1012 = 1;
    }
    _1013 = _1012 - 1;
    _1012 = NOVALUE;
    rhs_slice_target = (object_ptr)&_1014;
    RHS_Slice(_haystack_2003, 1, _1013);
    DeRef(_needle_2002);
    DeRefDS(_haystack_2003);
    DeRef(_1009);
    _1009 = NOVALUE;
    _1013 = NOVALUE;
    return _1014;
    goto L3; // [74] 109
L4: 

    /** 		return haystack[1 .. lIdx - 1] & haystack[lIdx + 1 .. $]*/
    _1015 = _lIdx_2004 - 1;
    rhs_slice_target = (object_ptr)&_1016;
    RHS_Slice(_haystack_2003, 1, _1015);
    _1017 = _lIdx_2004 + 1;
    if (_1017 > MAXINT){
        _1017 = NewDouble((double)_1017);
    }
    if (IS_SEQUENCE(_haystack_2003)){
            _1018 = SEQ_PTR(_haystack_2003)->length;
    }
    else {
        _1018 = 1;
    }
    rhs_slice_target = (object_ptr)&_1019;
    RHS_Slice(_haystack_2003, _1017, _1018);
    Concat((object_ptr)&_1020, _1016, _1019);
    DeRefDS(_1016);
    _1016 = NOVALUE;
    DeRef(_1016);
    _1016 = NOVALUE;
    DeRefDS(_1019);
    _1019 = NOVALUE;
    DeRef(_needle_2002);
    DeRefDS(_haystack_2003);
    DeRef(_1009);
    _1009 = NOVALUE;
    DeRef(_1013);
    _1013 = NOVALUE;
    DeRef(_1014);
    _1014 = NOVALUE;
    _1015 = NOVALUE;
    DeRef(_1017);
    _1017 = NOVALUE;
    return _1020;
L3: 
    ;
}


int _3mid(int _source_2027, int _start_2028, int _len_2029)
{
    int _msg_inlined_crash_at_45_2044 = NOVALUE;
    int _data_inlined_crash_at_42_2043 = NOVALUE;
    int _1044 = NOVALUE;
    int _1043 = NOVALUE;
    int _1042 = NOVALUE;
    int _1041 = NOVALUE;
    int _1040 = NOVALUE;
    int _1038 = NOVALUE;
    int _1037 = NOVALUE;
    int _1036 = NOVALUE;
    int _1034 = NOVALUE;
    int _1032 = NOVALUE;
    int _1031 = NOVALUE;
    int _1030 = NOVALUE;
    int _1029 = NOVALUE;
    int _1028 = NOVALUE;
    int _1027 = NOVALUE;
    int _1026 = NOVALUE;
    int _1022 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if len<0 then*/
    if (binary_op_a(GREATEREQ, _len_2029, 0)){
        goto L1; // [5] 66
    }

    /** 		len += length(source)*/
    if (IS_SEQUENCE(_source_2027)){
            _1022 = SEQ_PTR(_source_2027)->length;
    }
    else {
        _1022 = 1;
    }
    _0 = _len_2029;
    if (IS_ATOM_INT(_len_2029)) {
        _len_2029 = _len_2029 + _1022;
        if ((long)((unsigned long)_len_2029 + (unsigned long)HIGH_BITS) >= 0) 
        _len_2029 = NewDouble((double)_len_2029);
    }
    else {
        _len_2029 = NewDouble(DBL_PTR(_len_2029)->dbl + (double)_1022);
    }
    DeRef(_0);
    _1022 = NOVALUE;

    /** 		if len<0 then*/
    if (binary_op_a(GREATEREQ, _len_2029, 0)){
        goto L2; // [20] 65
    }

    /** 			error:crash("mid(): len was %d and should be greater than %d.",*/
    if (IS_SEQUENCE(_source_2027)){
            _1026 = SEQ_PTR(_source_2027)->length;
    }
    else {
        _1026 = 1;
    }
    if (IS_ATOM_INT(_len_2029)) {
        _1027 = _len_2029 - _1026;
        if ((long)((unsigned long)_1027 +(unsigned long) HIGH_BITS) >= 0){
            _1027 = NewDouble((double)_1027);
        }
    }
    else {
        _1027 = NewDouble(DBL_PTR(_len_2029)->dbl - (double)_1026);
    }
    _1026 = NOVALUE;
    if (IS_SEQUENCE(_source_2027)){
            _1028 = SEQ_PTR(_source_2027)->length;
    }
    else {
        _1028 = 1;
    }
    _1029 = - _1028;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1027;
    ((int *)_2)[2] = _1029;
    _1030 = MAKE_SEQ(_1);
    _1029 = NOVALUE;
    _1027 = NOVALUE;
    DeRef(_data_inlined_crash_at_42_2043);
    _data_inlined_crash_at_42_2043 = _1030;
    _1030 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_45_2044);
    _msg_inlined_crash_at_45_2044 = EPrintf(-9999999, _1025, _data_inlined_crash_at_42_2043);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_45_2044);

    /** end procedure*/
    goto L3; // [59] 62
L3: 
    DeRef(_data_inlined_crash_at_42_2043);
    _data_inlined_crash_at_42_2043 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_45_2044);
    _msg_inlined_crash_at_45_2044 = NOVALUE;
L2: 
L1: 

    /** 	if start > length(source) or len=0 then*/
    if (IS_SEQUENCE(_source_2027)){
            _1031 = SEQ_PTR(_source_2027)->length;
    }
    else {
        _1031 = 1;
    }
    if (IS_ATOM_INT(_start_2028)) {
        _1032 = (_start_2028 > _1031);
    }
    else {
        _1032 = (DBL_PTR(_start_2028)->dbl > (double)_1031);
    }
    _1031 = NOVALUE;
    if (_1032 != 0) {
        goto L4; // [75] 88
    }
    if (IS_ATOM_INT(_len_2029)) {
        _1034 = (_len_2029 == 0);
    }
    else {
        _1034 = (DBL_PTR(_len_2029)->dbl == (double)0);
    }
    if (_1034 == 0)
    {
        DeRef(_1034);
        _1034 = NOVALUE;
        goto L5; // [84] 95
    }
    else{
        DeRef(_1034);
        _1034 = NOVALUE;
    }
L4: 

    /** 		return ""*/
    RefDS(_5);
    DeRefDS(_source_2027);
    DeRef(_start_2028);
    DeRef(_len_2029);
    DeRef(_1032);
    _1032 = NOVALUE;
    return _5;
L5: 

    /** 	if start<1 then*/
    if (binary_op_a(GREATEREQ, _start_2028, 1)){
        goto L6; // [97] 107
    }

    /** 		start=1*/
    DeRef(_start_2028);
    _start_2028 = 1;
L6: 

    /** 	if start+len-1 >= length(source) then*/
    if (IS_ATOM_INT(_start_2028) && IS_ATOM_INT(_len_2029)) {
        _1036 = _start_2028 + _len_2029;
        if ((long)((unsigned long)_1036 + (unsigned long)HIGH_BITS) >= 0) 
        _1036 = NewDouble((double)_1036);
    }
    else {
        if (IS_ATOM_INT(_start_2028)) {
            _1036 = NewDouble((double)_start_2028 + DBL_PTR(_len_2029)->dbl);
        }
        else {
            if (IS_ATOM_INT(_len_2029)) {
                _1036 = NewDouble(DBL_PTR(_start_2028)->dbl + (double)_len_2029);
            }
            else
            _1036 = NewDouble(DBL_PTR(_start_2028)->dbl + DBL_PTR(_len_2029)->dbl);
        }
    }
    if (IS_ATOM_INT(_1036)) {
        _1037 = _1036 - 1;
        if ((long)((unsigned long)_1037 +(unsigned long) HIGH_BITS) >= 0){
            _1037 = NewDouble((double)_1037);
        }
    }
    else {
        _1037 = NewDouble(DBL_PTR(_1036)->dbl - (double)1);
    }
    DeRef(_1036);
    _1036 = NOVALUE;
    if (IS_SEQUENCE(_source_2027)){
            _1038 = SEQ_PTR(_source_2027)->length;
    }
    else {
        _1038 = 1;
    }
    if (binary_op_a(LESS, _1037, _1038)){
        DeRef(_1037);
        _1037 = NOVALUE;
        _1038 = NOVALUE;
        goto L7; // [120] 141
    }
    DeRef(_1037);
    _1037 = NOVALUE;
    _1038 = NOVALUE;

    /** 		return source[start..$]*/
    if (IS_SEQUENCE(_source_2027)){
            _1040 = SEQ_PTR(_source_2027)->length;
    }
    else {
        _1040 = 1;
    }
    rhs_slice_target = (object_ptr)&_1041;
    RHS_Slice(_source_2027, _start_2028, _1040);
    DeRefDS(_source_2027);
    DeRef(_start_2028);
    DeRef(_len_2029);
    DeRef(_1032);
    _1032 = NOVALUE;
    return _1041;
    goto L8; // [138] 161
L7: 

    /** 		return source[start..len+start-1]*/
    if (IS_ATOM_INT(_len_2029) && IS_ATOM_INT(_start_2028)) {
        _1042 = _len_2029 + _start_2028;
        if ((long)((unsigned long)_1042 + (unsigned long)HIGH_BITS) >= 0) 
        _1042 = NewDouble((double)_1042);
    }
    else {
        if (IS_ATOM_INT(_len_2029)) {
            _1042 = NewDouble((double)_len_2029 + DBL_PTR(_start_2028)->dbl);
        }
        else {
            if (IS_ATOM_INT(_start_2028)) {
                _1042 = NewDouble(DBL_PTR(_len_2029)->dbl + (double)_start_2028);
            }
            else
            _1042 = NewDouble(DBL_PTR(_len_2029)->dbl + DBL_PTR(_start_2028)->dbl);
        }
    }
    if (IS_ATOM_INT(_1042)) {
        _1043 = _1042 - 1;
    }
    else {
        _1043 = NewDouble(DBL_PTR(_1042)->dbl - (double)1);
    }
    DeRef(_1042);
    _1042 = NOVALUE;
    rhs_slice_target = (object_ptr)&_1044;
    RHS_Slice(_source_2027, _start_2028, _1043);
    DeRefDS(_source_2027);
    DeRef(_start_2028);
    DeRef(_len_2029);
    DeRef(_1032);
    _1032 = NOVALUE;
    DeRef(_1041);
    _1041 = NOVALUE;
    DeRef(_1043);
    _1043 = NOVALUE;
    return _1044;
L8: 
    ;
}


int _3slice(int _source_2065, int _start_2066, int _stop_2067)
{
    int _1053 = NOVALUE;
    int _1048 = NOVALUE;
    int _1046 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if stop < 1 then */
    if (binary_op_a(GREATEREQ, _stop_2067, 1)){
        goto L1; // [5] 21
    }

    /** 		stop += length(source) */
    if (IS_SEQUENCE(_source_2065)){
            _1046 = SEQ_PTR(_source_2065)->length;
    }
    else {
        _1046 = 1;
    }
    _0 = _stop_2067;
    if (IS_ATOM_INT(_stop_2067)) {
        _stop_2067 = _stop_2067 + _1046;
        if ((long)((unsigned long)_stop_2067 + (unsigned long)HIGH_BITS) >= 0) 
        _stop_2067 = NewDouble((double)_stop_2067);
    }
    else {
        _stop_2067 = NewDouble(DBL_PTR(_stop_2067)->dbl + (double)_1046);
    }
    DeRef(_0);
    _1046 = NOVALUE;
    goto L2; // [18] 37
L1: 

    /** 	elsif stop > length(source) then */
    if (IS_SEQUENCE(_source_2065)){
            _1048 = SEQ_PTR(_source_2065)->length;
    }
    else {
        _1048 = 1;
    }
    if (binary_op_a(LESSEQ, _stop_2067, _1048)){
        _1048 = NOVALUE;
        goto L3; // [26] 36
    }
    _1048 = NOVALUE;

    /** 		stop = length(source) */
    DeRef(_stop_2067);
    if (IS_SEQUENCE(_source_2065)){
            _stop_2067 = SEQ_PTR(_source_2065)->length;
    }
    else {
        _stop_2067 = 1;
    }
L3: 
L2: 

    /** 	if start < 1 then */
    if (binary_op_a(GREATEREQ, _start_2066, 1)){
        goto L4; // [39] 49
    }

    /** 		start = 1 */
    DeRef(_start_2066);
    _start_2066 = 1;
L4: 

    /** 	if start > stop then*/
    if (binary_op_a(LESSEQ, _start_2066, _stop_2067)){
        goto L5; // [51] 62
    }

    /** 		return ""*/
    RefDS(_5);
    DeRefDS(_source_2065);
    DeRef(_start_2066);
    DeRef(_stop_2067);
    return _5;
L5: 

    /** 	return source[start..stop]*/
    rhs_slice_target = (object_ptr)&_1053;
    RHS_Slice(_source_2065, _start_2066, _stop_2067);
    DeRefDS(_source_2065);
    DeRef(_start_2066);
    DeRef(_stop_2067);
    return _1053;
    ;
}


int _3vslice(int _source_2083, int _colno_2084, int _error_control_2085)
{
    int _substitutes_2086 = NOVALUE;
    int _current_sub_2087 = NOVALUE;
    int _msg_inlined_crash_at_10_2092 = NOVALUE;
    int _msg_inlined_crash_at_109_2112 = NOVALUE;
    int _data_inlined_crash_at_106_2111 = NOVALUE;
    int _1077 = NOVALUE;
    int _1076 = NOVALUE;
    int _1075 = NOVALUE;
    int _1074 = NOVALUE;
    int _1073 = NOVALUE;
    int _1071 = NOVALUE;
    int _1069 = NOVALUE;
    int _1068 = NOVALUE;
    int _1066 = NOVALUE;
    int _1062 = NOVALUE;
    int _1061 = NOVALUE;
    int _1060 = NOVALUE;
    int _1057 = NOVALUE;
    int _1056 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if colno < 1 then*/
    if (binary_op_a(GREATEREQ, _colno_2084, 1)){
        goto L1; // [5] 30
    }

    /** 		error:crash("sequence:vslice(): colno should be a valid index, but was %d",colno)*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_10_2092);
    _msg_inlined_crash_at_10_2092 = EPrintf(-9999999, _1055, _colno_2084);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_10_2092);

    /** end procedure*/
    goto L2; // [24] 27
L2: 
    DeRefi(_msg_inlined_crash_at_10_2092);
    _msg_inlined_crash_at_10_2092 = NOVALUE;
L1: 

    /** 	if atom(error_control) then*/
    _1056 = IS_ATOM(_error_control_2085);
    if (_1056 == 0)
    {
        _1056 = NOVALUE;
        goto L3; // [35] 53
    }
    else{
        _1056 = NOVALUE;
    }

    /** 		substitutes =-(not error_control)*/
    if (IS_ATOM_INT(_error_control_2085)) {
        _1057 = (_error_control_2085 == 0);
    }
    else {
        _1057 = unary_op(NOT, _error_control_2085);
    }
    if (IS_ATOM_INT(_1057)) {
        if ((unsigned long)_1057 == 0xC0000000)
        _substitutes_2086 = (int)NewDouble((double)-0xC0000000);
        else
        _substitutes_2086 = - _1057;
    }
    else {
        _substitutes_2086 = unary_op(UMINUS, _1057);
    }
    DeRef(_1057);
    _1057 = NOVALUE;
    if (!IS_ATOM_INT(_substitutes_2086)) {
        _1 = (long)(DBL_PTR(_substitutes_2086)->dbl);
        if (UNIQUE(DBL_PTR(_substitutes_2086)) && (DBL_PTR(_substitutes_2086)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_substitutes_2086);
        _substitutes_2086 = _1;
    }
    goto L4; // [50] 68
L3: 

    /** 		substitutes = length(error_control)*/
    if (IS_SEQUENCE(_error_control_2085)){
            _substitutes_2086 = SEQ_PTR(_error_control_2085)->length;
    }
    else {
        _substitutes_2086 = 1;
    }

    /** 		current_sub = 0*/
    _current_sub_2087 = 0;
L4: 

    /** 	for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_2083)){
            _1060 = SEQ_PTR(_source_2083)->length;
    }
    else {
        _1060 = 1;
    }
    {
        int _i_2100;
        _i_2100 = 1;
L5: 
        if (_i_2100 > _1060){
            goto L6; // [73] 231
        }

        /** 		if colno > length(source[i]) then*/
        _2 = (int)SEQ_PTR(_source_2083);
        _1061 = (int)*(((s1_ptr)_2)->base + _i_2100);
        if (IS_SEQUENCE(_1061)){
                _1062 = SEQ_PTR(_1061)->length;
        }
        else {
            _1062 = 1;
        }
        _1061 = NOVALUE;
        if (binary_op_a(LESSEQ, _colno_2084, _1062)){
            _1062 = NOVALUE;
            goto L7; // [89] 196
        }
        _1062 = NOVALUE;

        /** 			if substitutes = -1 then*/
        if (_substitutes_2086 != -1)
        goto L8; // [97] 131

        /** 				error:crash("sequence:vslice(): colno should be a valid index on the %d-th element, but was %d", {i, colno})*/
        Ref(_colno_2084);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_2100;
        ((int *)_2)[2] = _colno_2084;
        _1066 = MAKE_SEQ(_1);
        DeRef(_data_inlined_crash_at_106_2111);
        _data_inlined_crash_at_106_2111 = _1066;
        _1066 = NOVALUE;

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_109_2112);
        _msg_inlined_crash_at_109_2112 = EPrintf(-9999999, _1065, _data_inlined_crash_at_106_2111);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_109_2112);

        /** end procedure*/
        goto L9; // [123] 126
L9: 
        DeRef(_data_inlined_crash_at_106_2111);
        _data_inlined_crash_at_106_2111 = NOVALUE;
        DeRefi(_msg_inlined_crash_at_109_2112);
        _msg_inlined_crash_at_109_2112 = NOVALUE;
        goto LA; // [128] 224
L8: 

        /** 			elsif substitutes = 0 then*/
        if (_substitutes_2086 != 0)
        goto LB; // [133] 155

        /** 				return source[1..i-1]*/
        _1068 = _i_2100 - 1;
        rhs_slice_target = (object_ptr)&_1069;
        RHS_Slice(_source_2083, 1, _1068);
        DeRefDS(_source_2083);
        DeRef(_colno_2084);
        DeRef(_error_control_2085);
        _1061 = NOVALUE;
        _1068 = NOVALUE;
        return _1069;
        goto LA; // [152] 224
LB: 

        /** 				current_sub += 1*/
        _current_sub_2087 = _current_sub_2087 + 1;

        /** 				if current_sub > length(error_control) then*/
        if (IS_SEQUENCE(_error_control_2085)){
                _1071 = SEQ_PTR(_error_control_2085)->length;
        }
        else {
            _1071 = 1;
        }
        if (_current_sub_2087 <= _1071)
        goto LC; // [170] 182

        /** 					current_sub = 1*/
        _current_sub_2087 = 1;
LC: 

        /** 				source[i] = error_control[current_sub]*/
        _2 = (int)SEQ_PTR(_error_control_2085);
        _1073 = (int)*(((s1_ptr)_2)->base + _current_sub_2087);
        Ref(_1073);
        _2 = (int)SEQ_PTR(_source_2083);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_2083 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2100);
        _1 = *(int *)_2;
        *(int *)_2 = _1073;
        if( _1 != _1073 ){
            DeRef(_1);
        }
        _1073 = NOVALUE;
        goto LA; // [193] 224
L7: 

        /** 			if sequence(source[i]) then*/
        _2 = (int)SEQ_PTR(_source_2083);
        _1074 = (int)*(((s1_ptr)_2)->base + _i_2100);
        _1075 = IS_SEQUENCE(_1074);
        _1074 = NOVALUE;
        if (_1075 == 0)
        {
            _1075 = NOVALUE;
            goto LD; // [205] 223
        }
        else{
            _1075 = NOVALUE;
        }

        /** 				source[i] = source[i][colno]*/
        _2 = (int)SEQ_PTR(_source_2083);
        _1076 = (int)*(((s1_ptr)_2)->base + _i_2100);
        _2 = (int)SEQ_PTR(_1076);
        if (!IS_ATOM_INT(_colno_2084)){
            _1077 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_colno_2084)->dbl));
        }
        else{
            _1077 = (int)*(((s1_ptr)_2)->base + _colno_2084);
        }
        _1076 = NOVALUE;
        Ref(_1077);
        _2 = (int)SEQ_PTR(_source_2083);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_2083 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2100);
        _1 = *(int *)_2;
        *(int *)_2 = _1077;
        if( _1 != _1077 ){
            DeRef(_1);
        }
        _1077 = NOVALUE;
LD: 
LA: 

        /** 	end for*/
        _i_2100 = _i_2100 + 1;
        goto L5; // [226] 80
L6: 
        ;
    }

    /** 	return source*/
    DeRef(_colno_2084);
    DeRef(_error_control_2085);
    _1061 = NOVALUE;
    DeRef(_1068);
    _1068 = NOVALUE;
    DeRef(_1069);
    _1069 = NOVALUE;
    return _source_2083;
    ;
}


int _3patch(int _target_2131, int _source_2132, int _start_2133, int _filler_2134)
{
    int _1115 = NOVALUE;
    int _1114 = NOVALUE;
    int _1113 = NOVALUE;
    int _1112 = NOVALUE;
    int _1111 = NOVALUE;
    int _1110 = NOVALUE;
    int _1109 = NOVALUE;
    int _1108 = NOVALUE;
    int _1106 = NOVALUE;
    int _1105 = NOVALUE;
    int _1103 = NOVALUE;
    int _1102 = NOVALUE;
    int _1101 = NOVALUE;
    int _1100 = NOVALUE;
    int _1099 = NOVALUE;
    int _1098 = NOVALUE;
    int _1097 = NOVALUE;
    int _1096 = NOVALUE;
    int _1095 = NOVALUE;
    int _1094 = NOVALUE;
    int _1093 = NOVALUE;
    int _1092 = NOVALUE;
    int _1089 = NOVALUE;
    int _1088 = NOVALUE;
    int _1087 = NOVALUE;
    int _1086 = NOVALUE;
    int _1085 = NOVALUE;
    int _1084 = NOVALUE;
    int _1083 = NOVALUE;
    int _1082 = NOVALUE;
    int _1081 = NOVALUE;
    int _1079 = NOVALUE;
    int _1078 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_2133)) {
        _1 = (long)(DBL_PTR(_start_2133)->dbl);
        if (UNIQUE(DBL_PTR(_start_2133)) && (DBL_PTR(_start_2133)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_2133);
        _start_2133 = _1;
    }

    /** 	if start + length(source) <= 0 then*/
    if (IS_SEQUENCE(_source_2132)){
            _1078 = SEQ_PTR(_source_2132)->length;
    }
    else {
        _1078 = 1;
    }
    _1079 = _start_2133 + _1078;
    if ((long)((unsigned long)_1079 + (unsigned long)HIGH_BITS) >= 0) 
    _1079 = NewDouble((double)_1079);
    _1078 = NOVALUE;
    if (binary_op_a(GREATER, _1079, 0)){
        DeRef(_1079);
        _1079 = NOVALUE;
        goto L1; // [18] 55
    }
    DeRef(_1079);
    _1079 = NOVALUE;

    /** 		return source & repeat(filler, -start-length(source))+1 & target*/
    if ((unsigned long)_start_2133 == 0xC0000000)
    _1081 = (int)NewDouble((double)-0xC0000000);
    else
    _1081 = - _start_2133;
    if (IS_SEQUENCE(_source_2132)){
            _1082 = SEQ_PTR(_source_2132)->length;
    }
    else {
        _1082 = 1;
    }
    if (IS_ATOM_INT(_1081)) {
        _1083 = _1081 - _1082;
    }
    else {
        _1083 = NewDouble(DBL_PTR(_1081)->dbl - (double)_1082);
    }
    DeRef(_1081);
    _1081 = NOVALUE;
    _1082 = NOVALUE;
    _1084 = Repeat(_filler_2134, _1083);
    DeRef(_1083);
    _1083 = NOVALUE;
    _1085 = binary_op(PLUS, 1, _1084);
    DeRefDS(_1084);
    _1084 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _target_2131;
        concat_list[1] = _1085;
        concat_list[2] = _source_2132;
        Concat_N((object_ptr)&_1086, concat_list, 3);
    }
    DeRefDS(_1085);
    _1085 = NOVALUE;
    DeRefDS(_target_2131);
    DeRefDS(_source_2132);
    DeRef(_filler_2134);
    return _1086;
    goto L2; // [52] 223
L1: 

    /** 	elsif start + length(source) <= length(target) then*/
    if (IS_SEQUENCE(_source_2132)){
            _1087 = SEQ_PTR(_source_2132)->length;
    }
    else {
        _1087 = 1;
    }
    _1088 = _start_2133 + _1087;
    if ((long)((unsigned long)_1088 + (unsigned long)HIGH_BITS) >= 0) 
    _1088 = NewDouble((double)_1088);
    _1087 = NOVALUE;
    if (IS_SEQUENCE(_target_2131)){
            _1089 = SEQ_PTR(_target_2131)->length;
    }
    else {
        _1089 = 1;
    }
    if (binary_op_a(GREATER, _1088, _1089)){
        DeRef(_1088);
        _1088 = NOVALUE;
        _1089 = NOVALUE;
        goto L3; // [67] 145
    }
    DeRef(_1088);
    _1088 = NOVALUE;
    _1089 = NOVALUE;

    /** 		if start<=0 then*/
    if (_start_2133 > 0)
    goto L4; // [73] 105

    /** 			return source & target[start+length(source)..$]*/
    if (IS_SEQUENCE(_source_2132)){
            _1092 = SEQ_PTR(_source_2132)->length;
    }
    else {
        _1092 = 1;
    }
    _1093 = _start_2133 + _1092;
    if ((long)((unsigned long)_1093 + (unsigned long)HIGH_BITS) >= 0) 
    _1093 = NewDouble((double)_1093);
    _1092 = NOVALUE;
    if (IS_SEQUENCE(_target_2131)){
            _1094 = SEQ_PTR(_target_2131)->length;
    }
    else {
        _1094 = 1;
    }
    rhs_slice_target = (object_ptr)&_1095;
    RHS_Slice(_target_2131, _1093, _1094);
    Concat((object_ptr)&_1096, _source_2132, _1095);
    DeRefDS(_1095);
    _1095 = NOVALUE;
    DeRefDS(_target_2131);
    DeRefDS(_source_2132);
    DeRef(_filler_2134);
    DeRef(_1086);
    _1086 = NOVALUE;
    DeRef(_1093);
    _1093 = NOVALUE;
    return _1096;
    goto L2; // [102] 223
L4: 

    /**         	return target[1..start-1] & source &  target[start+length(source)..$]*/
    _1097 = _start_2133 - 1;
    rhs_slice_target = (object_ptr)&_1098;
    RHS_Slice(_target_2131, 1, _1097);
    if (IS_SEQUENCE(_source_2132)){
            _1099 = SEQ_PTR(_source_2132)->length;
    }
    else {
        _1099 = 1;
    }
    _1100 = _start_2133 + _1099;
    if ((long)((unsigned long)_1100 + (unsigned long)HIGH_BITS) >= 0) 
    _1100 = NewDouble((double)_1100);
    _1099 = NOVALUE;
    if (IS_SEQUENCE(_target_2131)){
            _1101 = SEQ_PTR(_target_2131)->length;
    }
    else {
        _1101 = 1;
    }
    rhs_slice_target = (object_ptr)&_1102;
    RHS_Slice(_target_2131, _1100, _1101);
    {
        int concat_list[3];

        concat_list[0] = _1102;
        concat_list[1] = _source_2132;
        concat_list[2] = _1098;
        Concat_N((object_ptr)&_1103, concat_list, 3);
    }
    DeRefDS(_1102);
    _1102 = NOVALUE;
    DeRefDS(_1098);
    _1098 = NOVALUE;
    DeRefDS(_target_2131);
    DeRefDS(_source_2132);
    DeRef(_filler_2134);
    DeRef(_1086);
    _1086 = NOVALUE;
    DeRef(_1093);
    _1093 = NOVALUE;
    DeRef(_1096);
    _1096 = NOVALUE;
    _1097 = NOVALUE;
    DeRef(_1100);
    _1100 = NOVALUE;
    return _1103;
    goto L2; // [142] 223
L3: 

    /** 	elsif start <= 1 then*/
    if (_start_2133 > 1)
    goto L5; // [147] 160

    /** 		return source*/
    DeRefDS(_target_2131);
    DeRef(_filler_2134);
    DeRef(_1086);
    _1086 = NOVALUE;
    DeRef(_1093);
    _1093 = NOVALUE;
    DeRef(_1096);
    _1096 = NOVALUE;
    DeRef(_1097);
    _1097 = NOVALUE;
    DeRef(_1103);
    _1103 = NOVALUE;
    DeRef(_1100);
    _1100 = NOVALUE;
    return _source_2132;
    goto L2; // [157] 223
L5: 

    /** 	elsif start <= length(target)+1 then*/
    if (IS_SEQUENCE(_target_2131)){
            _1105 = SEQ_PTR(_target_2131)->length;
    }
    else {
        _1105 = 1;
    }
    _1106 = _1105 + 1;
    _1105 = NOVALUE;
    if (_start_2133 > _1106)
    goto L6; // [169] 195

    /** 		return target[1..start-1] & source*/
    _1108 = _start_2133 - 1;
    rhs_slice_target = (object_ptr)&_1109;
    RHS_Slice(_target_2131, 1, _1108);
    Concat((object_ptr)&_1110, _1109, _source_2132);
    DeRefDS(_1109);
    _1109 = NOVALUE;
    DeRef(_1109);
    _1109 = NOVALUE;
    DeRefDS(_target_2131);
    DeRefDS(_source_2132);
    DeRef(_filler_2134);
    DeRef(_1086);
    _1086 = NOVALUE;
    DeRef(_1093);
    _1093 = NOVALUE;
    DeRef(_1096);
    _1096 = NOVALUE;
    DeRef(_1097);
    _1097 = NOVALUE;
    DeRef(_1103);
    _1103 = NOVALUE;
    DeRef(_1100);
    _1100 = NOVALUE;
    _1106 = NOVALUE;
    _1108 = NOVALUE;
    return _1110;
    goto L2; // [192] 223
L6: 

    /** 		return target & repeat(filler,start-length(target)-1) & source*/
    if (IS_SEQUENCE(_target_2131)){
            _1111 = SEQ_PTR(_target_2131)->length;
    }
    else {
        _1111 = 1;
    }
    _1112 = _start_2133 - _1111;
    if ((long)((unsigned long)_1112 +(unsigned long) HIGH_BITS) >= 0){
        _1112 = NewDouble((double)_1112);
    }
    _1111 = NOVALUE;
    if (IS_ATOM_INT(_1112)) {
        _1113 = _1112 - 1;
    }
    else {
        _1113 = NewDouble(DBL_PTR(_1112)->dbl - (double)1);
    }
    DeRef(_1112);
    _1112 = NOVALUE;
    _1114 = Repeat(_filler_2134, _1113);
    DeRef(_1113);
    _1113 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _source_2132;
        concat_list[1] = _1114;
        concat_list[2] = _target_2131;
        Concat_N((object_ptr)&_1115, concat_list, 3);
    }
    DeRefDS(_1114);
    _1114 = NOVALUE;
    DeRefDS(_target_2131);
    DeRefDS(_source_2132);
    DeRef(_filler_2134);
    DeRef(_1086);
    _1086 = NOVALUE;
    DeRef(_1093);
    _1093 = NOVALUE;
    DeRef(_1096);
    _1096 = NOVALUE;
    DeRef(_1097);
    _1097 = NOVALUE;
    DeRef(_1103);
    _1103 = NOVALUE;
    DeRef(_1100);
    _1100 = NOVALUE;
    DeRef(_1106);
    _1106 = NOVALUE;
    DeRef(_1108);
    _1108 = NOVALUE;
    DeRef(_1110);
    _1110 = NOVALUE;
    return _1115;
L2: 
    ;
}


int _3remove_all(int _needle_2182, int _haystack_2183)
{
    int _found_2184 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer found = 1*/
    _found_2184 = 1;

    /** 	while found entry do*/
    goto L1; // [12] 28
L2: 
    if (_found_2184 == 0)
    {
        goto L3; // [15] 42
    }
    else{
    }

    /** 		haystack = remove( haystack, found )*/
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_2183);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_found_2184)) ? _found_2184 : (long)(DBL_PTR(_found_2184)->dbl);
        int stop = (IS_ATOM_INT(_found_2184)) ? _found_2184 : (long)(DBL_PTR(_found_2184)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_2183), start, &_haystack_2183 );
            }
            else Tail(SEQ_PTR(_haystack_2183), stop+1, &_haystack_2183);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_2183), start, &_haystack_2183);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_2183 = Remove_elements(start, stop, (SEQ_PTR(_haystack_2183)->ref == 1));
        }
    }

    /** 	entry*/
L1: 

    /** 		found = find( needle, haystack, found )*/
    _found_2184 = find_from(_needle_2182, _haystack_2183, _found_2184);

    /** 	end while*/
    goto L2; // [39] 15
L3: 

    /** 	return haystack*/
    DeRef(_needle_2182);
    return _haystack_2183;
    ;
}


int _3retain_all(int _needles_2190, int _haystack_2191)
{
    int _lp_2192 = NOVALUE;
    int _np_2193 = NOVALUE;
    int _result_2194 = NOVALUE;
    int _1133 = NOVALUE;
    int _1130 = NOVALUE;
    int _1129 = NOVALUE;
    int _1127 = NOVALUE;
    int _1126 = NOVALUE;
    int _1125 = NOVALUE;
    int _1122 = NOVALUE;
    int _1120 = NOVALUE;
    int _1118 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(needles) then*/
    _1118 = IS_ATOM(_needles_2190);
    if (_1118 == 0)
    {
        _1118 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _1118 = NOVALUE;
    }

    /** 		needles = {needles}*/
    _0 = _needles_2190;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needles_2190);
    *((int *)(_2+4)) = _needles_2190;
    _needles_2190 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	if length(needles) = 0 then*/
    if (IS_SEQUENCE(_needles_2190)){
            _1120 = SEQ_PTR(_needles_2190)->length;
    }
    else {
        _1120 = 1;
    }
    if (_1120 != 0)
    goto L2; // [23] 34

    /** 		return {}*/
    RefDS(_5);
    DeRef(_needles_2190);
    DeRefDS(_haystack_2191);
    DeRef(_result_2194);
    return _5;
L2: 

    /** 	if length(haystack) = 0 then*/
    if (IS_SEQUENCE(_haystack_2191)){
            _1122 = SEQ_PTR(_haystack_2191)->length;
    }
    else {
        _1122 = 1;
    }
    if (_1122 != 0)
    goto L3; // [39] 50

    /** 		return {}*/
    RefDS(_5);
    DeRef(_needles_2190);
    DeRefDS(_haystack_2191);
    DeRef(_result_2194);
    return _5;
L3: 

    /** 	result = haystack*/
    RefDS(_haystack_2191);
    DeRef(_result_2194);
    _result_2194 = _haystack_2191;

    /** 	lp = length(haystack)*/
    if (IS_SEQUENCE(_haystack_2191)){
            _lp_2192 = SEQ_PTR(_haystack_2191)->length;
    }
    else {
        _lp_2192 = 1;
    }

    /** 	np = 1*/
    _np_2193 = 1;

    /** 	for i = 1 to length(haystack) do*/
    if (IS_SEQUENCE(_haystack_2191)){
            _1125 = SEQ_PTR(_haystack_2191)->length;
    }
    else {
        _1125 = 1;
    }
    {
        int _i_2206;
        _i_2206 = 1;
L4: 
        if (_i_2206 > _1125){
            goto L5; // [76] 146
        }

        /** 		if find(haystack[i], needles) then*/
        _2 = (int)SEQ_PTR(_haystack_2191);
        _1126 = (int)*(((s1_ptr)_2)->base + _i_2206);
        _1127 = find_from(_1126, _needles_2190, 1);
        _1126 = NOVALUE;
        if (_1127 == 0)
        {
            _1127 = NOVALUE;
            goto L6; // [94] 130
        }
        else{
            _1127 = NOVALUE;
        }

        /** 			if np < i then*/
        if (_np_2193 >= _i_2206)
        goto L7; // [99] 119

        /** 				result[np .. lp] = haystack[i..$]*/
        if (IS_SEQUENCE(_haystack_2191)){
                _1129 = SEQ_PTR(_haystack_2191)->length;
        }
        else {
            _1129 = 1;
        }
        rhs_slice_target = (object_ptr)&_1130;
        RHS_Slice(_haystack_2191, _i_2206, _1129);
        assign_slice_seq = (s1_ptr *)&_result_2194;
        AssignSlice(_np_2193, _lp_2192, _1130);
        DeRefDS(_1130);
        _1130 = NOVALUE;
L7: 

        /** 			np += 1*/
        _np_2193 = _np_2193 + 1;
        goto L8; // [127] 139
L6: 

        /** 			lp -= 1*/
        _lp_2192 = _lp_2192 - 1;
L8: 

        /** 	end for*/
        _i_2206 = _i_2206 + 1;
        goto L4; // [141] 83
L5: 
        ;
    }

    /** 	return result[1 .. lp]*/
    rhs_slice_target = (object_ptr)&_1133;
    RHS_Slice(_result_2194, 1, _lp_2192);
    DeRef(_needles_2190);
    DeRefDS(_haystack_2191);
    DeRefDS(_result_2194);
    return _1133;
    ;
}


int _3filter(int _source_2221, int _rid_2222, int _userdata_2223, int _rangetype_2224)
{
    int _dest_2225 = NOVALUE;
    int _idx_2226 = NOVALUE;
    int _1308 = NOVALUE;
    int _1307 = NOVALUE;
    int _1305 = NOVALUE;
    int _1304 = NOVALUE;
    int _1303 = NOVALUE;
    int _1302 = NOVALUE;
    int _1301 = NOVALUE;
    int _1298 = NOVALUE;
    int _1297 = NOVALUE;
    int _1296 = NOVALUE;
    int _1295 = NOVALUE;
    int _1292 = NOVALUE;
    int _1291 = NOVALUE;
    int _1290 = NOVALUE;
    int _1289 = NOVALUE;
    int _1288 = NOVALUE;
    int _1285 = NOVALUE;
    int _1284 = NOVALUE;
    int _1283 = NOVALUE;
    int _1282 = NOVALUE;
    int _1279 = NOVALUE;
    int _1278 = NOVALUE;
    int _1277 = NOVALUE;
    int _1276 = NOVALUE;
    int _1275 = NOVALUE;
    int _1272 = NOVALUE;
    int _1271 = NOVALUE;
    int _1270 = NOVALUE;
    int _1269 = NOVALUE;
    int _1266 = NOVALUE;
    int _1265 = NOVALUE;
    int _1264 = NOVALUE;
    int _1263 = NOVALUE;
    int _1262 = NOVALUE;
    int _1259 = NOVALUE;
    int _1258 = NOVALUE;
    int _1257 = NOVALUE;
    int _1256 = NOVALUE;
    int _1253 = NOVALUE;
    int _1252 = NOVALUE;
    int _1251 = NOVALUE;
    int _1250 = NOVALUE;
    int _1249 = NOVALUE;
    int _1246 = NOVALUE;
    int _1245 = NOVALUE;
    int _1244 = NOVALUE;
    int _1240 = NOVALUE;
    int _1237 = NOVALUE;
    int _1236 = NOVALUE;
    int _1235 = NOVALUE;
    int _1233 = NOVALUE;
    int _1232 = NOVALUE;
    int _1231 = NOVALUE;
    int _1230 = NOVALUE;
    int _1229 = NOVALUE;
    int _1226 = NOVALUE;
    int _1225 = NOVALUE;
    int _1224 = NOVALUE;
    int _1222 = NOVALUE;
    int _1221 = NOVALUE;
    int _1220 = NOVALUE;
    int _1219 = NOVALUE;
    int _1218 = NOVALUE;
    int _1215 = NOVALUE;
    int _1214 = NOVALUE;
    int _1213 = NOVALUE;
    int _1211 = NOVALUE;
    int _1210 = NOVALUE;
    int _1209 = NOVALUE;
    int _1208 = NOVALUE;
    int _1207 = NOVALUE;
    int _1204 = NOVALUE;
    int _1203 = NOVALUE;
    int _1202 = NOVALUE;
    int _1200 = NOVALUE;
    int _1199 = NOVALUE;
    int _1198 = NOVALUE;
    int _1197 = NOVALUE;
    int _1196 = NOVALUE;
    int _1194 = NOVALUE;
    int _1193 = NOVALUE;
    int _1192 = NOVALUE;
    int _1188 = NOVALUE;
    int _1185 = NOVALUE;
    int _1184 = NOVALUE;
    int _1183 = NOVALUE;
    int _1180 = NOVALUE;
    int _1177 = NOVALUE;
    int _1176 = NOVALUE;
    int _1175 = NOVALUE;
    int _1172 = NOVALUE;
    int _1169 = NOVALUE;
    int _1168 = NOVALUE;
    int _1167 = NOVALUE;
    int _1164 = NOVALUE;
    int _1161 = NOVALUE;
    int _1160 = NOVALUE;
    int _1159 = NOVALUE;
    int _1155 = NOVALUE;
    int _1152 = NOVALUE;
    int _1151 = NOVALUE;
    int _1150 = NOVALUE;
    int _1147 = NOVALUE;
    int _1144 = NOVALUE;
    int _1143 = NOVALUE;
    int _1142 = NOVALUE;
    int _1136 = NOVALUE;
    int _1134 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_2221)){
            _1134 = SEQ_PTR(_source_2221)->length;
    }
    else {
        _1134 = 1;
    }
    if (_1134 != 0)
    goto L1; // [8] 19

    /** 		return source*/
    DeRef(_rid_2222);
    DeRef(_userdata_2223);
    DeRef(_rangetype_2224);
    DeRef(_dest_2225);
    return _source_2221;
L1: 

    /** 	dest = repeat(0, length(source))*/
    if (IS_SEQUENCE(_source_2221)){
            _1136 = SEQ_PTR(_source_2221)->length;
    }
    else {
        _1136 = 1;
    }
    DeRef(_dest_2225);
    _dest_2225 = Repeat(0, _1136);
    _1136 = NOVALUE;

    /** 	idx = 0*/
    _idx_2226 = 0;

    /** 	switch rid do*/
    _1 = find(_rid_2222, _1138);
    switch ( _1 ){ 

        /** 		case "<", "lt" then*/
        case 1:
        case 2:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_2221)){
                _1142 = SEQ_PTR(_source_2221)->length;
        }
        else {
            _1142 = 1;
        }
        {
            int _a_2238;
            _a_2238 = 1;
L2: 
            if (_a_2238 > _1142){
                goto L3; // [53] 100
            }

            /** 				if compare(source[a], userdata) < 0 then*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1143 = (int)*(((s1_ptr)_2)->base + _a_2238);
            if (IS_ATOM_INT(_1143) && IS_ATOM_INT(_userdata_2223)){
                _1144 = (_1143 < _userdata_2223) ? -1 : (_1143 > _userdata_2223);
            }
            else{
                _1144 = compare(_1143, _userdata_2223);
            }
            _1143 = NOVALUE;
            if (_1144 >= 0)
            goto L4; // [70] 93

            /** 					idx += 1*/
            _idx_2226 = _idx_2226 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1147 = (int)*(((s1_ptr)_2)->base + _a_2238);
            Ref(_1147);
            _2 = (int)SEQ_PTR(_dest_2225);
            _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
            _1 = *(int *)_2;
            *(int *)_2 = _1147;
            if( _1 != _1147 ){
                DeRef(_1);
            }
            _1147 = NOVALUE;
L4: 

            /** 			end for*/
            _a_2238 = _a_2238 + 1;
            goto L2; // [95] 60
L3: 
            ;
        }
        goto L5; // [100] 1348

        /** 		case "<=", "le" then*/
        case 3:
        case 4:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_2221)){
                _1150 = SEQ_PTR(_source_2221)->length;
        }
        else {
            _1150 = 1;
        }
        {
            int _a_2250;
            _a_2250 = 1;
L6: 
            if (_a_2250 > _1150){
                goto L7; // [113] 160
            }

            /** 				if compare(source[a], userdata) <= 0 then*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1151 = (int)*(((s1_ptr)_2)->base + _a_2250);
            if (IS_ATOM_INT(_1151) && IS_ATOM_INT(_userdata_2223)){
                _1152 = (_1151 < _userdata_2223) ? -1 : (_1151 > _userdata_2223);
            }
            else{
                _1152 = compare(_1151, _userdata_2223);
            }
            _1151 = NOVALUE;
            if (_1152 > 0)
            goto L8; // [130] 153

            /** 					idx += 1*/
            _idx_2226 = _idx_2226 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1155 = (int)*(((s1_ptr)_2)->base + _a_2250);
            Ref(_1155);
            _2 = (int)SEQ_PTR(_dest_2225);
            _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
            _1 = *(int *)_2;
            *(int *)_2 = _1155;
            if( _1 != _1155 ){
                DeRef(_1);
            }
            _1155 = NOVALUE;
L8: 

            /** 			end for*/
            _a_2250 = _a_2250 + 1;
            goto L6; // [155] 120
L7: 
            ;
        }
        goto L5; // [160] 1348

        /** 		case "=", "==", "eq" then*/
        case 5:
        case 6:
        case 7:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_2221)){
                _1159 = SEQ_PTR(_source_2221)->length;
        }
        else {
            _1159 = 1;
        }
        {
            int _a_2263;
            _a_2263 = 1;
L9: 
            if (_a_2263 > _1159){
                goto LA; // [175] 222
            }

            /** 				if compare(source[a], userdata) = 0 then*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1160 = (int)*(((s1_ptr)_2)->base + _a_2263);
            if (IS_ATOM_INT(_1160) && IS_ATOM_INT(_userdata_2223)){
                _1161 = (_1160 < _userdata_2223) ? -1 : (_1160 > _userdata_2223);
            }
            else{
                _1161 = compare(_1160, _userdata_2223);
            }
            _1160 = NOVALUE;
            if (_1161 != 0)
            goto LB; // [192] 215

            /** 					idx += 1*/
            _idx_2226 = _idx_2226 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1164 = (int)*(((s1_ptr)_2)->base + _a_2263);
            Ref(_1164);
            _2 = (int)SEQ_PTR(_dest_2225);
            _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
            _1 = *(int *)_2;
            *(int *)_2 = _1164;
            if( _1 != _1164 ){
                DeRef(_1);
            }
            _1164 = NOVALUE;
LB: 

            /** 			end for*/
            _a_2263 = _a_2263 + 1;
            goto L9; // [217] 182
LA: 
            ;
        }
        goto L5; // [222] 1348

        /** 		case "!=", "ne" then*/
        case 8:
        case 9:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_2221)){
                _1167 = SEQ_PTR(_source_2221)->length;
        }
        else {
            _1167 = 1;
        }
        {
            int _a_2275;
            _a_2275 = 1;
LC: 
            if (_a_2275 > _1167){
                goto LD; // [235] 282
            }

            /** 				if compare(source[a], userdata) != 0 then*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1168 = (int)*(((s1_ptr)_2)->base + _a_2275);
            if (IS_ATOM_INT(_1168) && IS_ATOM_INT(_userdata_2223)){
                _1169 = (_1168 < _userdata_2223) ? -1 : (_1168 > _userdata_2223);
            }
            else{
                _1169 = compare(_1168, _userdata_2223);
            }
            _1168 = NOVALUE;
            if (_1169 == 0)
            goto LE; // [252] 275

            /** 					idx += 1*/
            _idx_2226 = _idx_2226 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1172 = (int)*(((s1_ptr)_2)->base + _a_2275);
            Ref(_1172);
            _2 = (int)SEQ_PTR(_dest_2225);
            _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
            _1 = *(int *)_2;
            *(int *)_2 = _1172;
            if( _1 != _1172 ){
                DeRef(_1);
            }
            _1172 = NOVALUE;
LE: 

            /** 			end for*/
            _a_2275 = _a_2275 + 1;
            goto LC; // [277] 242
LD: 
            ;
        }
        goto L5; // [282] 1348

        /** 		case ">", "gt" then*/
        case 10:
        case 11:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_2221)){
                _1175 = SEQ_PTR(_source_2221)->length;
        }
        else {
            _1175 = 1;
        }
        {
            int _a_2287;
            _a_2287 = 1;
LF: 
            if (_a_2287 > _1175){
                goto L10; // [295] 342
            }

            /** 				if compare(source[a], userdata) > 0 then*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1176 = (int)*(((s1_ptr)_2)->base + _a_2287);
            if (IS_ATOM_INT(_1176) && IS_ATOM_INT(_userdata_2223)){
                _1177 = (_1176 < _userdata_2223) ? -1 : (_1176 > _userdata_2223);
            }
            else{
                _1177 = compare(_1176, _userdata_2223);
            }
            _1176 = NOVALUE;
            if (_1177 <= 0)
            goto L11; // [312] 335

            /** 					idx += 1*/
            _idx_2226 = _idx_2226 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1180 = (int)*(((s1_ptr)_2)->base + _a_2287);
            Ref(_1180);
            _2 = (int)SEQ_PTR(_dest_2225);
            _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
            _1 = *(int *)_2;
            *(int *)_2 = _1180;
            if( _1 != _1180 ){
                DeRef(_1);
            }
            _1180 = NOVALUE;
L11: 

            /** 			end for*/
            _a_2287 = _a_2287 + 1;
            goto LF; // [337] 302
L10: 
            ;
        }
        goto L5; // [342] 1348

        /** 		case ">=", "ge" then*/
        case 12:
        case 13:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_2221)){
                _1183 = SEQ_PTR(_source_2221)->length;
        }
        else {
            _1183 = 1;
        }
        {
            int _a_2299;
            _a_2299 = 1;
L12: 
            if (_a_2299 > _1183){
                goto L13; // [355] 402
            }

            /** 				if compare(source[a], userdata) >= 0 then*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1184 = (int)*(((s1_ptr)_2)->base + _a_2299);
            if (IS_ATOM_INT(_1184) && IS_ATOM_INT(_userdata_2223)){
                _1185 = (_1184 < _userdata_2223) ? -1 : (_1184 > _userdata_2223);
            }
            else{
                _1185 = compare(_1184, _userdata_2223);
            }
            _1184 = NOVALUE;
            if (_1185 < 0)
            goto L14; // [372] 395

            /** 					idx += 1*/
            _idx_2226 = _idx_2226 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1188 = (int)*(((s1_ptr)_2)->base + _a_2299);
            Ref(_1188);
            _2 = (int)SEQ_PTR(_dest_2225);
            _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
            _1 = *(int *)_2;
            *(int *)_2 = _1188;
            if( _1 != _1188 ){
                DeRef(_1);
            }
            _1188 = NOVALUE;
L14: 

            /** 			end for*/
            _a_2299 = _a_2299 + 1;
            goto L12; // [397] 362
L13: 
            ;
        }
        goto L5; // [402] 1348

        /** 		case "in" then*/
        case 14:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_2224, _1190);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1192 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1192 = 1;
            }
            {
                int _a_2313;
                _a_2313 = 1;
L15: 
                if (_a_2313 > _1192){
                    goto L16; // [424] 471
                }

                /** 						if find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1193 = (int)*(((s1_ptr)_2)->base + _a_2313);
                _1194 = find_from(_1193, _userdata_2223, 1);
                _1193 = NOVALUE;
                if (_1194 == 0)
                {
                    _1194 = NOVALUE;
                    goto L17; // [442] 464
                }
                else{
                    _1194 = NOVALUE;
                }

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1196 = (int)*(((s1_ptr)_2)->base + _a_2313);
                Ref(_1196);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1196;
                if( _1 != _1196 ){
                    DeRef(_1);
                }
                _1196 = NOVALUE;
L17: 

                /** 					end for*/
                _a_2313 = _a_2313 + 1;
                goto L15; // [466] 431
L16: 
                ;
            }
            goto L5; // [471] 1348

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1197 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1197 = 1;
            }
            {
                int _a_2322;
                _a_2322 = 1;
L18: 
                if (_a_2322 > _1197){
                    goto L19; // [482] 552
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1198 = (int)*(((s1_ptr)_2)->base + _a_2322);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1199 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1198) && IS_ATOM_INT(_1199)){
                    _1200 = (_1198 < _1199) ? -1 : (_1198 > _1199);
                }
                else{
                    _1200 = compare(_1198, _1199);
                }
                _1198 = NOVALUE;
                _1199 = NOVALUE;
                if (_1200 < 0)
                goto L1A; // [503] 545

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1202 = (int)*(((s1_ptr)_2)->base + _a_2322);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1203 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1202) && IS_ATOM_INT(_1203)){
                    _1204 = (_1202 < _1203) ? -1 : (_1202 > _1203);
                }
                else{
                    _1204 = compare(_1202, _1203);
                }
                _1202 = NOVALUE;
                _1203 = NOVALUE;
                if (_1204 > 0)
                goto L1B; // [521] 544

                /** 								idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1207 = (int)*(((s1_ptr)_2)->base + _a_2322);
                Ref(_1207);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1207;
                if( _1 != _1207 ){
                    DeRef(_1);
                }
                _1207 = NOVALUE;
L1B: 
L1A: 

                /** 					end for*/
                _a_2322 = _a_2322 + 1;
                goto L18; // [547] 489
L19: 
                ;
            }
            goto L5; // [552] 1348

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1208 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1208 = 1;
            }
            {
                int _a_2338;
                _a_2338 = 1;
L1C: 
                if (_a_2338 > _1208){
                    goto L1D; // [563] 633
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1209 = (int)*(((s1_ptr)_2)->base + _a_2338);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1210 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1209) && IS_ATOM_INT(_1210)){
                    _1211 = (_1209 < _1210) ? -1 : (_1209 > _1210);
                }
                else{
                    _1211 = compare(_1209, _1210);
                }
                _1209 = NOVALUE;
                _1210 = NOVALUE;
                if (_1211 < 0)
                goto L1E; // [584] 626

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1213 = (int)*(((s1_ptr)_2)->base + _a_2338);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1214 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1213) && IS_ATOM_INT(_1214)){
                    _1215 = (_1213 < _1214) ? -1 : (_1213 > _1214);
                }
                else{
                    _1215 = compare(_1213, _1214);
                }
                _1213 = NOVALUE;
                _1214 = NOVALUE;
                if (_1215 >= 0)
                goto L1F; // [602] 625

                /** 								idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1218 = (int)*(((s1_ptr)_2)->base + _a_2338);
                Ref(_1218);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1218;
                if( _1 != _1218 ){
                    DeRef(_1);
                }
                _1218 = NOVALUE;
L1F: 
L1E: 

                /** 					end for*/
                _a_2338 = _a_2338 + 1;
                goto L1C; // [628] 570
L1D: 
                ;
            }
            goto L5; // [633] 1348

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1219 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1219 = 1;
            }
            {
                int _a_2354;
                _a_2354 = 1;
L20: 
                if (_a_2354 > _1219){
                    goto L21; // [644] 714
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1220 = (int)*(((s1_ptr)_2)->base + _a_2354);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1221 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1220) && IS_ATOM_INT(_1221)){
                    _1222 = (_1220 < _1221) ? -1 : (_1220 > _1221);
                }
                else{
                    _1222 = compare(_1220, _1221);
                }
                _1220 = NOVALUE;
                _1221 = NOVALUE;
                if (_1222 <= 0)
                goto L22; // [665] 707

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1224 = (int)*(((s1_ptr)_2)->base + _a_2354);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1225 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1224) && IS_ATOM_INT(_1225)){
                    _1226 = (_1224 < _1225) ? -1 : (_1224 > _1225);
                }
                else{
                    _1226 = compare(_1224, _1225);
                }
                _1224 = NOVALUE;
                _1225 = NOVALUE;
                if (_1226 > 0)
                goto L23; // [683] 706

                /** 								idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1229 = (int)*(((s1_ptr)_2)->base + _a_2354);
                Ref(_1229);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1229;
                if( _1 != _1229 ){
                    DeRef(_1);
                }
                _1229 = NOVALUE;
L23: 
L22: 

                /** 					end for*/
                _a_2354 = _a_2354 + 1;
                goto L20; // [709] 651
L21: 
                ;
            }
            goto L5; // [714] 1348

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1230 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1230 = 1;
            }
            {
                int _a_2370;
                _a_2370 = 1;
L24: 
                if (_a_2370 > _1230){
                    goto L25; // [725] 795
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1231 = (int)*(((s1_ptr)_2)->base + _a_2370);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1232 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1231) && IS_ATOM_INT(_1232)){
                    _1233 = (_1231 < _1232) ? -1 : (_1231 > _1232);
                }
                else{
                    _1233 = compare(_1231, _1232);
                }
                _1231 = NOVALUE;
                _1232 = NOVALUE;
                if (_1233 <= 0)
                goto L26; // [746] 788

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1235 = (int)*(((s1_ptr)_2)->base + _a_2370);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1236 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1235) && IS_ATOM_INT(_1236)){
                    _1237 = (_1235 < _1236) ? -1 : (_1235 > _1236);
                }
                else{
                    _1237 = compare(_1235, _1236);
                }
                _1235 = NOVALUE;
                _1236 = NOVALUE;
                if (_1237 >= 0)
                goto L27; // [764] 787

                /** 								idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1240 = (int)*(((s1_ptr)_2)->base + _a_2370);
                Ref(_1240);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1240;
                if( _1 != _1240 ){
                    DeRef(_1);
                }
                _1240 = NOVALUE;
L27: 
L26: 

                /** 					end for*/
                _a_2370 = _a_2370 + 1;
                goto L24; // [790] 732
L25: 
                ;
            }
            goto L5; // [795] 1348

            /** 				case else*/
            case 0:
        ;}        goto L5; // [802] 1348

        /** 		case "out" then*/
        case 15:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_2224, _1242);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1244 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1244 = 1;
            }
            {
                int _a_2391;
                _a_2391 = 1;
L28: 
                if (_a_2391 > _1244){
                    goto L29; // [824] 871
                }

                /** 						if not find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1245 = (int)*(((s1_ptr)_2)->base + _a_2391);
                _1246 = find_from(_1245, _userdata_2223, 1);
                _1245 = NOVALUE;
                if (_1246 != 0)
                goto L2A; // [842] 864
                _1246 = NOVALUE;

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1249 = (int)*(((s1_ptr)_2)->base + _a_2391);
                Ref(_1249);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1249;
                if( _1 != _1249 ){
                    DeRef(_1);
                }
                _1249 = NOVALUE;
L2A: 

                /** 					end for*/
                _a_2391 = _a_2391 + 1;
                goto L28; // [866] 831
L29: 
                ;
            }
            goto L5; // [871] 1348

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1250 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1250 = 1;
            }
            {
                int _a_2401;
                _a_2401 = 1;
L2B: 
                if (_a_2401 > _1250){
                    goto L2C; // [882] 973
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1251 = (int)*(((s1_ptr)_2)->base + _a_2401);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1252 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1251) && IS_ATOM_INT(_1252)){
                    _1253 = (_1251 < _1252) ? -1 : (_1251 > _1252);
                }
                else{
                    _1253 = compare(_1251, _1252);
                }
                _1251 = NOVALUE;
                _1252 = NOVALUE;
                if (_1253 >= 0)
                goto L2D; // [903] 928

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1256 = (int)*(((s1_ptr)_2)->base + _a_2401);
                Ref(_1256);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1256;
                if( _1 != _1256 ){
                    DeRef(_1);
                }
                _1256 = NOVALUE;
                goto L2E; // [925] 966
L2D: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1257 = (int)*(((s1_ptr)_2)->base + _a_2401);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1258 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1257) && IS_ATOM_INT(_1258)){
                    _1259 = (_1257 < _1258) ? -1 : (_1257 > _1258);
                }
                else{
                    _1259 = compare(_1257, _1258);
                }
                _1257 = NOVALUE;
                _1258 = NOVALUE;
                if (_1259 <= 0)
                goto L2F; // [942] 965

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1262 = (int)*(((s1_ptr)_2)->base + _a_2401);
                Ref(_1262);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1262;
                if( _1 != _1262 ){
                    DeRef(_1);
                }
                _1262 = NOVALUE;
L2F: 
L2E: 

                /** 					end for*/
                _a_2401 = _a_2401 + 1;
                goto L2B; // [968] 889
L2C: 
                ;
            }
            goto L5; // [973] 1348

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1263 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1263 = 1;
            }
            {
                int _a_2419;
                _a_2419 = 1;
L30: 
                if (_a_2419 > _1263){
                    goto L31; // [984] 1075
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1264 = (int)*(((s1_ptr)_2)->base + _a_2419);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1265 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1264) && IS_ATOM_INT(_1265)){
                    _1266 = (_1264 < _1265) ? -1 : (_1264 > _1265);
                }
                else{
                    _1266 = compare(_1264, _1265);
                }
                _1264 = NOVALUE;
                _1265 = NOVALUE;
                if (_1266 >= 0)
                goto L32; // [1005] 1030

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1269 = (int)*(((s1_ptr)_2)->base + _a_2419);
                Ref(_1269);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1269;
                if( _1 != _1269 ){
                    DeRef(_1);
                }
                _1269 = NOVALUE;
                goto L33; // [1027] 1068
L32: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1270 = (int)*(((s1_ptr)_2)->base + _a_2419);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1271 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1270) && IS_ATOM_INT(_1271)){
                    _1272 = (_1270 < _1271) ? -1 : (_1270 > _1271);
                }
                else{
                    _1272 = compare(_1270, _1271);
                }
                _1270 = NOVALUE;
                _1271 = NOVALUE;
                if (_1272 < 0)
                goto L34; // [1044] 1067

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1275 = (int)*(((s1_ptr)_2)->base + _a_2419);
                Ref(_1275);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1275;
                if( _1 != _1275 ){
                    DeRef(_1);
                }
                _1275 = NOVALUE;
L34: 
L33: 

                /** 					end for*/
                _a_2419 = _a_2419 + 1;
                goto L30; // [1070] 991
L31: 
                ;
            }
            goto L5; // [1075] 1348

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1276 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1276 = 1;
            }
            {
                int _a_2437;
                _a_2437 = 1;
L35: 
                if (_a_2437 > _1276){
                    goto L36; // [1086] 1177
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1277 = (int)*(((s1_ptr)_2)->base + _a_2437);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1278 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1277) && IS_ATOM_INT(_1278)){
                    _1279 = (_1277 < _1278) ? -1 : (_1277 > _1278);
                }
                else{
                    _1279 = compare(_1277, _1278);
                }
                _1277 = NOVALUE;
                _1278 = NOVALUE;
                if (_1279 > 0)
                goto L37; // [1107] 1132

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1282 = (int)*(((s1_ptr)_2)->base + _a_2437);
                Ref(_1282);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1282;
                if( _1 != _1282 ){
                    DeRef(_1);
                }
                _1282 = NOVALUE;
                goto L38; // [1129] 1170
L37: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1283 = (int)*(((s1_ptr)_2)->base + _a_2437);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1284 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1283) && IS_ATOM_INT(_1284)){
                    _1285 = (_1283 < _1284) ? -1 : (_1283 > _1284);
                }
                else{
                    _1285 = compare(_1283, _1284);
                }
                _1283 = NOVALUE;
                _1284 = NOVALUE;
                if (_1285 <= 0)
                goto L39; // [1146] 1169

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1288 = (int)*(((s1_ptr)_2)->base + _a_2437);
                Ref(_1288);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1288;
                if( _1 != _1288 ){
                    DeRef(_1);
                }
                _1288 = NOVALUE;
L39: 
L38: 

                /** 					end for*/
                _a_2437 = _a_2437 + 1;
                goto L35; // [1172] 1093
L36: 
                ;
            }
            goto L5; // [1177] 1348

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_2221)){
                    _1289 = SEQ_PTR(_source_2221)->length;
            }
            else {
                _1289 = 1;
            }
            {
                int _a_2455;
                _a_2455 = 1;
L3A: 
                if (_a_2455 > _1289){
                    goto L3B; // [1188] 1279
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1290 = (int)*(((s1_ptr)_2)->base + _a_2455);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1291 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_1290) && IS_ATOM_INT(_1291)){
                    _1292 = (_1290 < _1291) ? -1 : (_1290 > _1291);
                }
                else{
                    _1292 = compare(_1290, _1291);
                }
                _1290 = NOVALUE;
                _1291 = NOVALUE;
                if (_1292 > 0)
                goto L3C; // [1209] 1234

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1295 = (int)*(((s1_ptr)_2)->base + _a_2455);
                Ref(_1295);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1295;
                if( _1 != _1295 ){
                    DeRef(_1);
                }
                _1295 = NOVALUE;
                goto L3D; // [1231] 1272
L3C: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1296 = (int)*(((s1_ptr)_2)->base + _a_2455);
                _2 = (int)SEQ_PTR(_userdata_2223);
                _1297 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_1296) && IS_ATOM_INT(_1297)){
                    _1298 = (_1296 < _1297) ? -1 : (_1296 > _1297);
                }
                else{
                    _1298 = compare(_1296, _1297);
                }
                _1296 = NOVALUE;
                _1297 = NOVALUE;
                if (_1298 < 0)
                goto L3E; // [1248] 1271

                /** 							idx += 1*/
                _idx_2226 = _idx_2226 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_2221);
                _1301 = (int)*(((s1_ptr)_2)->base + _a_2455);
                Ref(_1301);
                _2 = (int)SEQ_PTR(_dest_2225);
                _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
                _1 = *(int *)_2;
                *(int *)_2 = _1301;
                if( _1 != _1301 ){
                    DeRef(_1);
                }
                _1301 = NOVALUE;
L3E: 
L3D: 

                /** 					end for*/
                _a_2455 = _a_2455 + 1;
                goto L3A; // [1274] 1195
L3B: 
                ;
            }
            goto L5; // [1279] 1348

            /** 				case else*/
            case 0:
        ;}        goto L5; // [1286] 1348

        /** 		case else*/
        case 0:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_2221)){
                _1302 = SEQ_PTR(_source_2221)->length;
        }
        else {
            _1302 = 1;
        }
        {
            int _a_2474;
            _a_2474 = 1;
L3F: 
            if (_a_2474 > _1302){
                goto L40; // [1297] 1347
            }

            /** 				if call_func(rid, {source[a], userdata}) then*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1303 = (int)*(((s1_ptr)_2)->base + _a_2474);
            Ref(_userdata_2223);
            Ref(_1303);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _1303;
            ((int *)_2)[2] = _userdata_2223;
            _1304 = MAKE_SEQ(_1);
            _1303 = NOVALUE;
            _1 = (int)SEQ_PTR(_1304);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_rid_2222].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8)
                                 );
            DeRef(_1305);
            _1305 = _1;
            DeRefDS(_1304);
            _1304 = NOVALUE;
            if (_1305 == 0) {
                DeRef(_1305);
                _1305 = NOVALUE;
                goto L41; // [1318] 1340
            }
            else {
                if (!IS_ATOM_INT(_1305) && DBL_PTR(_1305)->dbl == 0.0){
                    DeRef(_1305);
                    _1305 = NOVALUE;
                    goto L41; // [1318] 1340
                }
                DeRef(_1305);
                _1305 = NOVALUE;
            }
            DeRef(_1305);
            _1305 = NOVALUE;

            /** 					idx += 1*/
            _idx_2226 = _idx_2226 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_2221);
            _1307 = (int)*(((s1_ptr)_2)->base + _a_2474);
            Ref(_1307);
            _2 = (int)SEQ_PTR(_dest_2225);
            _2 = (int)(((s1_ptr)_2)->base + _idx_2226);
            _1 = *(int *)_2;
            *(int *)_2 = _1307;
            if( _1 != _1307 ){
                DeRef(_1);
            }
            _1307 = NOVALUE;
L41: 

            /** 			end for*/
            _a_2474 = _a_2474 + 1;
            goto L3F; // [1342] 1304
L40: 
            ;
        }
    ;}L5: 

    /** 	return dest[1..idx]*/
    rhs_slice_target = (object_ptr)&_1308;
    RHS_Slice(_dest_2225, 1, _idx_2226);
    DeRefDS(_source_2221);
    DeRef(_rid_2222);
    DeRef(_userdata_2223);
    DeRef(_rangetype_2224);
    DeRefDS(_dest_2225);
    return _1308;
    ;
}


int _3filter_alpha(int _elem_2486, int _ud_2487)
{
    int _1309 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return t_alpha(elem)*/
    Ref(_elem_2486);
    _1309 = _5t_alpha(_elem_2486);
    DeRef(_elem_2486);
    return _1309;
    ;
}


int _3extract(int _source_2495, int _indexes_2496)
{
    int _p_2497 = NOVALUE;
    int _msg_inlined_crash_at_34_2507 = NOVALUE;
    int _1317 = NOVALUE;
    int _1314 = NOVALUE;
    int _1312 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(indexes) do*/
    if (IS_SEQUENCE(_indexes_2496)){
            _1312 = SEQ_PTR(_indexes_2496)->length;
    }
    else {
        _1312 = 1;
    }
    {
        int _i_2499;
        _i_2499 = 1;
L1: 
        if (_i_2499 > _1312){
            goto L2; // [10] 71
        }

        /** 		p = indexes[i]*/
        DeRef(_p_2497);
        _2 = (int)SEQ_PTR(_indexes_2496);
        _p_2497 = (int)*(((s1_ptr)_2)->base + _i_2499);
        Ref(_p_2497);

        /** 		if not valid_index(source,p) then*/
        RefDS(_source_2495);
        Ref(_p_2497);
        _1314 = _3valid_index(_source_2495, _p_2497);
        if (IS_ATOM_INT(_1314)) {
            if (_1314 != 0){
                DeRef(_1314);
                _1314 = NOVALUE;
                goto L3; // [30] 54
            }
        }
        else {
            if (DBL_PTR(_1314)->dbl != 0.0){
                DeRef(_1314);
                _1314 = NOVALUE;
                goto L3; // [30] 54
            }
        }
        DeRef(_1314);
        _1314 = NOVALUE;

        /** 			error:crash("%d is not a valid index for the input sequence",p)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_34_2507);
        _msg_inlined_crash_at_34_2507 = EPrintf(-9999999, _1316, _p_2497);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_34_2507);

        /** end procedure*/
        goto L4; // [48] 51
L4: 
        DeRefi(_msg_inlined_crash_at_34_2507);
        _msg_inlined_crash_at_34_2507 = NOVALUE;
L3: 

        /** 		indexes[i] = source[p]*/
        _2 = (int)SEQ_PTR(_source_2495);
        if (!IS_ATOM_INT(_p_2497)){
            _1317 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_p_2497)->dbl));
        }
        else{
            _1317 = (int)*(((s1_ptr)_2)->base + _p_2497);
        }
        Ref(_1317);
        _2 = (int)SEQ_PTR(_indexes_2496);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _indexes_2496 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2499);
        _1 = *(int *)_2;
        *(int *)_2 = _1317;
        if( _1 != _1317 ){
            DeRef(_1);
        }
        _1317 = NOVALUE;

        /** 	end for*/
        _i_2499 = _i_2499 + 1;
        goto L1; // [66] 17
L2: 
        ;
    }

    /** 	return indexes*/
    DeRefDS(_source_2495);
    DeRef(_p_2497);
    return _indexes_2496;
    ;
}


int _3project(int _source_2511, int _coords_2512)
{
    int _result_2513 = NOVALUE;
    int _1328 = NOVALUE;
    int _1327 = NOVALUE;
    int _1326 = NOVALUE;
    int _1324 = NOVALUE;
    int _1323 = NOVALUE;
    int _1322 = NOVALUE;
    int _1320 = NOVALUE;
    int _1319 = NOVALUE;
    int _1318 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	result = repeat( repeat(0, length(coords)), length(source) )*/
    if (IS_SEQUENCE(_coords_2512)){
            _1318 = SEQ_PTR(_coords_2512)->length;
    }
    else {
        _1318 = 1;
    }
    _1319 = Repeat(0, _1318);
    _1318 = NOVALUE;
    if (IS_SEQUENCE(_source_2511)){
            _1320 = SEQ_PTR(_source_2511)->length;
    }
    else {
        _1320 = 1;
    }
    DeRef(_result_2513);
    _result_2513 = Repeat(_1319, _1320);
    DeRefDS(_1319);
    _1319 = NOVALUE;
    _1320 = NOVALUE;

    /** 	for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_2511)){
            _1322 = SEQ_PTR(_source_2511)->length;
    }
    else {
        _1322 = 1;
    }
    {
        int _i_2519;
        _i_2519 = 1;
L1: 
        if (_i_2519 > _1322){
            goto L2; // [26] 83
        }

        /** 		for j = 1 to length(coords) do*/
        if (IS_SEQUENCE(_coords_2512)){
                _1323 = SEQ_PTR(_coords_2512)->length;
        }
        else {
            _1323 = 1;
        }
        {
            int _j_2522;
            _j_2522 = 1;
L3: 
            if (_j_2522 > _1323){
                goto L4; // [38] 76
            }

            /** 			result[i][j] = extract(source[i], coords[j])*/
            _2 = (int)SEQ_PTR(_result_2513);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_2513 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_2519 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_source_2511);
            _1326 = (int)*(((s1_ptr)_2)->base + _i_2519);
            _2 = (int)SEQ_PTR(_coords_2512);
            _1327 = (int)*(((s1_ptr)_2)->base + _j_2522);
            Ref(_1326);
            Ref(_1327);
            _1328 = _3extract(_1326, _1327);
            _1326 = NOVALUE;
            _1327 = NOVALUE;
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_2522);
            _1 = *(int *)_2;
            *(int *)_2 = _1328;
            if( _1 != _1328 ){
                DeRef(_1);
            }
            _1328 = NOVALUE;
            _1324 = NOVALUE;

            /** 		end for*/
            _j_2522 = _j_2522 + 1;
            goto L3; // [71] 45
L4: 
            ;
        }

        /** 	end for*/
        _i_2519 = _i_2519 + 1;
        goto L1; // [78] 33
L2: 
        ;
    }

    /** 	return result*/
    DeRefDS(_source_2511);
    DeRefDS(_coords_2512);
    return _result_2513;
    ;
}


int _3split(int _st_2531, int _delim_2532, int _no_empty_2533, int _limit_2534)
{
    int _ret_2535 = NOVALUE;
    int _start_2536 = NOVALUE;
    int _pos_2537 = NOVALUE;
    int _k_2589 = NOVALUE;
    int _1377 = NOVALUE;
    int _1375 = NOVALUE;
    int _1374 = NOVALUE;
    int _1370 = NOVALUE;
    int _1369 = NOVALUE;
    int _1368 = NOVALUE;
    int _1365 = NOVALUE;
    int _1364 = NOVALUE;
    int _1359 = NOVALUE;
    int _1358 = NOVALUE;
    int _1354 = NOVALUE;
    int _1350 = NOVALUE;
    int _1348 = NOVALUE;
    int _1347 = NOVALUE;
    int _1343 = NOVALUE;
    int _1341 = NOVALUE;
    int _1340 = NOVALUE;
    int _1339 = NOVALUE;
    int _1338 = NOVALUE;
    int _1335 = NOVALUE;
    int _1334 = NOVALUE;
    int _1333 = NOVALUE;
    int _1332 = NOVALUE;
    int _1331 = NOVALUE;
    int _1329 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_no_empty_2533)) {
        _1 = (long)(DBL_PTR(_no_empty_2533)->dbl);
        if (UNIQUE(DBL_PTR(_no_empty_2533)) && (DBL_PTR(_no_empty_2533)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_no_empty_2533);
        _no_empty_2533 = _1;
    }
    if (!IS_ATOM_INT(_limit_2534)) {
        _1 = (long)(DBL_PTR(_limit_2534)->dbl);
        if (UNIQUE(DBL_PTR(_limit_2534)) && (DBL_PTR(_limit_2534)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_2534);
        _limit_2534 = _1;
    }

    /** 	sequence ret = {}*/
    RefDS(_5);
    DeRef(_ret_2535);
    _ret_2535 = _5;

    /** 	if length(st) = 0 then*/
    if (IS_SEQUENCE(_st_2531)){
            _1329 = SEQ_PTR(_st_2531)->length;
    }
    else {
        _1329 = 1;
    }
    if (_1329 != 0)
    goto L1; // [23] 34

    /** 		return ret*/
    DeRefDS(_st_2531);
    DeRef(_delim_2532);
    return _ret_2535;
L1: 

    /** 	if sequence(delim) then*/
    _1331 = IS_SEQUENCE(_delim_2532);
    if (_1331 == 0)
    {
        _1331 = NOVALUE;
        goto L2; // [39] 225
    }
    else{
        _1331 = NOVALUE;
    }

    /** 		if equal(delim, "") then*/
    if (_delim_2532 == _5)
    _1332 = 1;
    else if (IS_ATOM_INT(_delim_2532) && IS_ATOM_INT(_5))
    _1332 = 0;
    else
    _1332 = (compare(_delim_2532, _5) == 0);
    if (_1332 == 0)
    {
        _1332 = NOVALUE;
        goto L3; // [48] 133
    }
    else{
        _1332 = NOVALUE;
    }

    /** 			for i = 1 to length(st) do*/
    if (IS_SEQUENCE(_st_2531)){
            _1333 = SEQ_PTR(_st_2531)->length;
    }
    else {
        _1333 = 1;
    }
    {
        int _i_2546;
        _i_2546 = 1;
L4: 
        if (_i_2546 > _1333){
            goto L5; // [56] 126
        }

        /** 				st[i] = {st[i]}*/
        _2 = (int)SEQ_PTR(_st_2531);
        _1334 = (int)*(((s1_ptr)_2)->base + _i_2546);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_1334);
        *((int *)(_2+4)) = _1334;
        _1335 = MAKE_SEQ(_1);
        _1334 = NOVALUE;
        _2 = (int)SEQ_PTR(_st_2531);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _st_2531 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2546);
        _1 = *(int *)_2;
        *(int *)_2 = _1335;
        if( _1 != _1335 ){
            DeRef(_1);
        }
        _1335 = NOVALUE;

        /** 				limit -= 1*/
        _limit_2534 = _limit_2534 - 1;

        /** 				if limit = 0 then*/
        if (_limit_2534 != 0)
        goto L6; // [87] 119

        /** 					st = append(st[1 .. i],st[i+1 .. $])*/
        rhs_slice_target = (object_ptr)&_1338;
        RHS_Slice(_st_2531, 1, _i_2546);
        _1339 = _i_2546 + 1;
        if (IS_SEQUENCE(_st_2531)){
                _1340 = SEQ_PTR(_st_2531)->length;
        }
        else {
            _1340 = 1;
        }
        rhs_slice_target = (object_ptr)&_1341;
        RHS_Slice(_st_2531, _1339, _1340);
        RefDS(_1341);
        Append(&_st_2531, _1338, _1341);
        DeRefDS(_1338);
        _1338 = NOVALUE;
        DeRefDS(_1341);
        _1341 = NOVALUE;

        /** 					exit*/
        goto L5; // [116] 126
L6: 

        /** 			end for*/
        _i_2546 = _i_2546 + 1;
        goto L4; // [121] 63
L5: 
        ;
    }

    /** 			return st*/
    DeRef(_delim_2532);
    DeRef(_ret_2535);
    DeRef(_1339);
    _1339 = NOVALUE;
    return _st_2531;
L3: 

    /** 		start = 1*/
    _start_2536 = 1;

    /** 		while start <= length(st) do*/
L7: 
    if (IS_SEQUENCE(_st_2531)){
            _1343 = SEQ_PTR(_st_2531)->length;
    }
    else {
        _1343 = 1;
    }
    if (_start_2536 > _1343)
    goto L8; // [148] 312

    /** 			pos = match(delim, st, start)*/
    _pos_2537 = e_match_from(_delim_2532, _st_2531, _start_2536);

    /** 			if pos = 0 then*/
    if (_pos_2537 != 0)
    goto L9; // [163] 172

    /** 				exit*/
    goto L8; // [169] 312
L9: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _1347 = _pos_2537 - 1;
    rhs_slice_target = (object_ptr)&_1348;
    RHS_Slice(_st_2531, _start_2536, _1347);
    RefDS(_1348);
    Append(&_ret_2535, _ret_2535, _1348);
    DeRefDS(_1348);
    _1348 = NOVALUE;

    /** 			start = pos+length(delim)*/
    if (IS_SEQUENCE(_delim_2532)){
            _1350 = SEQ_PTR(_delim_2532)->length;
    }
    else {
        _1350 = 1;
    }
    _start_2536 = _pos_2537 + _1350;
    _1350 = NOVALUE;

    /** 			limit -= 1*/
    _limit_2534 = _limit_2534 - 1;

    /** 			if limit = 0 then*/
    if (_limit_2534 != 0)
    goto L7; // [208] 145

    /** 				exit*/
    goto L8; // [214] 312

    /** 		end while*/
    goto L7; // [219] 145
    goto L8; // [222] 312
L2: 

    /** 		start = 1*/
    _start_2536 = 1;

    /** 		while start <= length(st) do*/
LA: 
    if (IS_SEQUENCE(_st_2531)){
            _1354 = SEQ_PTR(_st_2531)->length;
    }
    else {
        _1354 = 1;
    }
    if (_start_2536 > _1354)
    goto LB; // [240] 311

    /** 			pos = find(delim, st, start)*/
    _pos_2537 = find_from(_delim_2532, _st_2531, _start_2536);

    /** 			if pos = 0 then*/
    if (_pos_2537 != 0)
    goto LC; // [255] 264

    /** 				exit*/
    goto LB; // [261] 311
LC: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _1358 = _pos_2537 - 1;
    rhs_slice_target = (object_ptr)&_1359;
    RHS_Slice(_st_2531, _start_2536, _1358);
    RefDS(_1359);
    Append(&_ret_2535, _ret_2535, _1359);
    DeRefDS(_1359);
    _1359 = NOVALUE;

    /** 			start = pos + 1*/
    _start_2536 = _pos_2537 + 1;

    /** 			limit -= 1*/
    _limit_2534 = _limit_2534 - 1;

    /** 			if limit = 0 then*/
    if (_limit_2534 != 0)
    goto LA; // [297] 237

    /** 				exit*/
    goto LB; // [303] 311

    /** 		end while*/
    goto LA; // [308] 237
LB: 
L8: 

    /** 	ret = append(ret, st[start..$])*/
    if (IS_SEQUENCE(_st_2531)){
            _1364 = SEQ_PTR(_st_2531)->length;
    }
    else {
        _1364 = 1;
    }
    rhs_slice_target = (object_ptr)&_1365;
    RHS_Slice(_st_2531, _start_2536, _1364);
    RefDS(_1365);
    Append(&_ret_2535, _ret_2535, _1365);
    DeRefDS(_1365);
    _1365 = NOVALUE;

    /** 	integer k = length(ret)*/
    if (IS_SEQUENCE(_ret_2535)){
            _k_2589 = SEQ_PTR(_ret_2535)->length;
    }
    else {
        _k_2589 = 1;
    }

    /** 	if no_empty then*/
    if (_no_empty_2533 == 0)
    {
        goto LD; // [337] 406
    }
    else{
    }

    /** 		k = 0*/
    _k_2589 = 0;

    /** 		for i = 1 to length(ret) do*/
    if (IS_SEQUENCE(_ret_2535)){
            _1368 = SEQ_PTR(_ret_2535)->length;
    }
    else {
        _1368 = 1;
    }
    {
        int _i_2593;
        _i_2593 = 1;
LE: 
        if (_i_2593 > _1368){
            goto LF; // [352] 405
        }

        /** 			if length(ret[i]) != 0 then*/
        _2 = (int)SEQ_PTR(_ret_2535);
        _1369 = (int)*(((s1_ptr)_2)->base + _i_2593);
        if (IS_SEQUENCE(_1369)){
                _1370 = SEQ_PTR(_1369)->length;
        }
        else {
            _1370 = 1;
        }
        _1369 = NOVALUE;
        if (_1370 == 0)
        goto L10; // [368] 398

        /** 				k += 1*/
        _k_2589 = _k_2589 + 1;

        /** 				if k != i then*/
        if (_k_2589 == _i_2593)
        goto L11; // [382] 397

        /** 					ret[k] = ret[i]*/
        _2 = (int)SEQ_PTR(_ret_2535);
        _1374 = (int)*(((s1_ptr)_2)->base + _i_2593);
        Ref(_1374);
        _2 = (int)SEQ_PTR(_ret_2535);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_2535 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _k_2589);
        _1 = *(int *)_2;
        *(int *)_2 = _1374;
        if( _1 != _1374 ){
            DeRef(_1);
        }
        _1374 = NOVALUE;
L11: 
L10: 

        /** 		end for*/
        _i_2593 = _i_2593 + 1;
        goto LE; // [400] 359
LF: 
        ;
    }
LD: 

    /** 	if k < length(ret) then*/
    if (IS_SEQUENCE(_ret_2535)){
            _1375 = SEQ_PTR(_ret_2535)->length;
    }
    else {
        _1375 = 1;
    }
    if (_k_2589 >= _1375)
    goto L12; // [411] 429

    /** 		return ret[1 .. k]*/
    rhs_slice_target = (object_ptr)&_1377;
    RHS_Slice(_ret_2535, 1, _k_2589);
    DeRefDS(_st_2531);
    DeRef(_delim_2532);
    DeRefDS(_ret_2535);
    DeRef(_1347);
    _1347 = NOVALUE;
    DeRef(_1339);
    _1339 = NOVALUE;
    DeRef(_1358);
    _1358 = NOVALUE;
    _1369 = NOVALUE;
    return _1377;
    goto L13; // [426] 436
L12: 

    /** 		return ret*/
    DeRefDS(_st_2531);
    DeRef(_delim_2532);
    DeRef(_1347);
    _1347 = NOVALUE;
    DeRef(_1339);
    _1339 = NOVALUE;
    DeRef(_1358);
    _1358 = NOVALUE;
    _1369 = NOVALUE;
    DeRef(_1377);
    _1377 = NOVALUE;
    return _ret_2535;
L13: 
    ;
}


int _3split_any(int _source_2610, int _delim_2611, int _limit_2613, int _no_empty_2614)
{
    int _ret_2615 = NOVALUE;
    int _start_2616 = NOVALUE;
    int _pos_2617 = NOVALUE;
    int _next_pos_2618 = NOVALUE;
    int _k_2637 = NOVALUE;
    int _1402 = NOVALUE;
    int _1400 = NOVALUE;
    int _1399 = NOVALUE;
    int _1395 = NOVALUE;
    int _1394 = NOVALUE;
    int _1393 = NOVALUE;
    int _1390 = NOVALUE;
    int _1389 = NOVALUE;
    int _1385 = NOVALUE;
    int _1384 = NOVALUE;
    int _1381 = NOVALUE;
    int _1379 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_2613)) {
        _1 = (long)(DBL_PTR(_limit_2613)->dbl);
        if (UNIQUE(DBL_PTR(_limit_2613)) && (DBL_PTR(_limit_2613)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_2613);
        _limit_2613 = _1;
    }
    if (!IS_ATOM_INT(_no_empty_2614)) {
        _1 = (long)(DBL_PTR(_no_empty_2614)->dbl);
        if (UNIQUE(DBL_PTR(_no_empty_2614)) && (DBL_PTR(_no_empty_2614)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_no_empty_2614);
        _no_empty_2614 = _1;
    }

    /** 	sequence ret = {}*/
    RefDS(_5);
    DeRef(_ret_2615);
    _ret_2615 = _5;

    /** 	integer start = 1, pos, next_pos*/
    _start_2616 = 1;

    /** 	if length(delim) = 0 then*/
    if (IS_SEQUENCE(_delim_2611)){
            _1379 = SEQ_PTR(_delim_2611)->length;
    }
    else {
        _1379 = 1;
    }
    if (_1379 != 0)
    goto L1; // [30] 45

    /** 		return {source}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_source_2610);
    *((int *)(_2+4)) = _source_2610;
    _1381 = MAKE_SEQ(_1);
    DeRefDS(_source_2610);
    DeRef(_delim_2611);
    DeRefDS(_ret_2615);
    return _1381;
L1: 

    /** 	while 1 do*/
L2: 

    /** 		pos = search:find_any(delim, source, start)*/
    Ref(_delim_2611);
    RefDS(_source_2610);
    _pos_2617 = _6find_any(_delim_2611, _source_2610, _start_2616);
    if (!IS_ATOM_INT(_pos_2617)) {
        _1 = (long)(DBL_PTR(_pos_2617)->dbl);
        if (UNIQUE(DBL_PTR(_pos_2617)) && (DBL_PTR(_pos_2617)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_2617);
        _pos_2617 = _1;
    }

    /** 		next_pos = pos + 1*/
    _next_pos_2618 = _pos_2617 + 1;

    /** 		if pos then*/
    if (_pos_2617 == 0)
    {
        goto L3; // [72] 129
    }
    else{
    }

    /** 			ret = append(ret, source[start..pos-1])*/
    _1384 = _pos_2617 - 1;
    rhs_slice_target = (object_ptr)&_1385;
    RHS_Slice(_source_2610, _start_2616, _1384);
    RefDS(_1385);
    Append(&_ret_2615, _ret_2615, _1385);
    DeRefDS(_1385);
    _1385 = NOVALUE;

    /** 			start = next_pos*/
    _start_2616 = _next_pos_2618;

    /** 			limit -= 1*/
    _limit_2613 = _limit_2613 - 1;

    /** 			if limit = 0 then*/
    if (_limit_2613 != 0)
    goto L2; // [107] 50

    /** 				exit*/
    goto L3; // [113] 129
    goto L2; // [116] 50

    /** 			exit*/
    goto L3; // [121] 129

    /** 	end while*/
    goto L2; // [126] 50
L3: 

    /** 	ret = append(ret, source[start..$])*/
    if (IS_SEQUENCE(_source_2610)){
            _1389 = SEQ_PTR(_source_2610)->length;
    }
    else {
        _1389 = 1;
    }
    rhs_slice_target = (object_ptr)&_1390;
    RHS_Slice(_source_2610, _start_2616, _1389);
    RefDS(_1390);
    Append(&_ret_2615, _ret_2615, _1390);
    DeRefDS(_1390);
    _1390 = NOVALUE;

    /** 	integer k = length(ret)*/
    if (IS_SEQUENCE(_ret_2615)){
            _k_2637 = SEQ_PTR(_ret_2615)->length;
    }
    else {
        _k_2637 = 1;
    }

    /** 	if no_empty then*/
    if (_no_empty_2614 == 0)
    {
        goto L4; // [152] 221
    }
    else{
    }

    /** 		k = 0*/
    _k_2637 = 0;

    /** 		for i = 1 to length(ret) do*/
    if (IS_SEQUENCE(_ret_2615)){
            _1393 = SEQ_PTR(_ret_2615)->length;
    }
    else {
        _1393 = 1;
    }
    {
        int _i_2641;
        _i_2641 = 1;
L5: 
        if (_i_2641 > _1393){
            goto L6; // [167] 220
        }

        /** 			if length(ret[i]) != 0 then*/
        _2 = (int)SEQ_PTR(_ret_2615);
        _1394 = (int)*(((s1_ptr)_2)->base + _i_2641);
        if (IS_SEQUENCE(_1394)){
                _1395 = SEQ_PTR(_1394)->length;
        }
        else {
            _1395 = 1;
        }
        _1394 = NOVALUE;
        if (_1395 == 0)
        goto L7; // [183] 213

        /** 				k += 1*/
        _k_2637 = _k_2637 + 1;

        /** 				if k != i then*/
        if (_k_2637 == _i_2641)
        goto L8; // [197] 212

        /** 					ret[k] = ret[i]*/
        _2 = (int)SEQ_PTR(_ret_2615);
        _1399 = (int)*(((s1_ptr)_2)->base + _i_2641);
        Ref(_1399);
        _2 = (int)SEQ_PTR(_ret_2615);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_2615 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _k_2637);
        _1 = *(int *)_2;
        *(int *)_2 = _1399;
        if( _1 != _1399 ){
            DeRef(_1);
        }
        _1399 = NOVALUE;
L8: 
L7: 

        /** 		end for*/
        _i_2641 = _i_2641 + 1;
        goto L5; // [215] 174
L6: 
        ;
    }
L4: 

    /** 	if k < length(ret) then*/
    if (IS_SEQUENCE(_ret_2615)){
            _1400 = SEQ_PTR(_ret_2615)->length;
    }
    else {
        _1400 = 1;
    }
    if (_k_2637 >= _1400)
    goto L9; // [226] 244

    /** 		return ret[1 .. k]*/
    rhs_slice_target = (object_ptr)&_1402;
    RHS_Slice(_ret_2615, 1, _k_2637);
    DeRefDS(_source_2610);
    DeRef(_delim_2611);
    DeRefDS(_ret_2615);
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1384);
    _1384 = NOVALUE;
    _1394 = NOVALUE;
    return _1402;
    goto LA; // [241] 251
L9: 

    /** 		return ret*/
    DeRefDS(_source_2610);
    DeRef(_delim_2611);
    DeRef(_1381);
    _1381 = NOVALUE;
    DeRef(_1384);
    _1384 = NOVALUE;
    _1394 = NOVALUE;
    DeRef(_1402);
    _1402 = NOVALUE;
    return _ret_2615;
LA: 
    ;
}


int _3join(int _items_2658, int _delim_2659)
{
    int _ret_2661 = NOVALUE;
    int _1412 = NOVALUE;
    int _1411 = NOVALUE;
    int _1409 = NOVALUE;
    int _1408 = NOVALUE;
    int _1407 = NOVALUE;
    int _1406 = NOVALUE;
    int _1404 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(items) then return {} end if*/
    if (IS_SEQUENCE(_items_2658)){
            _1404 = SEQ_PTR(_items_2658)->length;
    }
    else {
        _1404 = 1;
    }
    if (_1404 != 0)
    goto L1; // [8] 16
    _1404 = NOVALUE;
    RefDS(_5);
    DeRefDS(_items_2658);
    DeRef(_delim_2659);
    DeRef(_ret_2661);
    return _5;
L1: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_2661);
    _ret_2661 = _5;

    /** 	for i=1 to length(items)-1 do*/
    if (IS_SEQUENCE(_items_2658)){
            _1406 = SEQ_PTR(_items_2658)->length;
    }
    else {
        _1406 = 1;
    }
    _1407 = _1406 - 1;
    _1406 = NOVALUE;
    {
        int _i_2666;
        _i_2666 = 1;
L2: 
        if (_i_2666 > _1407){
            goto L3; // [30] 58
        }

        /** 		ret &= items[i] & delim*/
        _2 = (int)SEQ_PTR(_items_2658);
        _1408 = (int)*(((s1_ptr)_2)->base + _i_2666);
        if (IS_SEQUENCE(_1408) && IS_ATOM(_delim_2659)) {
            Ref(_delim_2659);
            Append(&_1409, _1408, _delim_2659);
        }
        else if (IS_ATOM(_1408) && IS_SEQUENCE(_delim_2659)) {
            Ref(_1408);
            Prepend(&_1409, _delim_2659, _1408);
        }
        else {
            Concat((object_ptr)&_1409, _1408, _delim_2659);
            _1408 = NOVALUE;
        }
        _1408 = NOVALUE;
        if (IS_SEQUENCE(_ret_2661) && IS_ATOM(_1409)) {
        }
        else if (IS_ATOM(_ret_2661) && IS_SEQUENCE(_1409)) {
            Ref(_ret_2661);
            Prepend(&_ret_2661, _1409, _ret_2661);
        }
        else {
            Concat((object_ptr)&_ret_2661, _ret_2661, _1409);
        }
        DeRefDS(_1409);
        _1409 = NOVALUE;

        /** 	end for*/
        _i_2666 = _i_2666 + 1;
        goto L2; // [53] 37
L3: 
        ;
    }

    /** 	ret &= items[$]*/
    if (IS_SEQUENCE(_items_2658)){
            _1411 = SEQ_PTR(_items_2658)->length;
    }
    else {
        _1411 = 1;
    }
    _2 = (int)SEQ_PTR(_items_2658);
    _1412 = (int)*(((s1_ptr)_2)->base + _1411);
    if (IS_SEQUENCE(_ret_2661) && IS_ATOM(_1412)) {
        Ref(_1412);
        Append(&_ret_2661, _ret_2661, _1412);
    }
    else if (IS_ATOM(_ret_2661) && IS_SEQUENCE(_1412)) {
        Ref(_ret_2661);
        Prepend(&_ret_2661, _1412, _ret_2661);
    }
    else {
        Concat((object_ptr)&_ret_2661, _ret_2661, _1412);
    }
    _1412 = NOVALUE;

    /** 	return ret*/
    DeRefDS(_items_2658);
    DeRef(_delim_2659);
    DeRef(_1407);
    _1407 = NOVALUE;
    return _ret_2661;
    ;
}


int _3breakup(int _source_2680, int _size_2681, int _style_2682)
{
    int _len_2691 = NOVALUE;
    int _rem_2692 = NOVALUE;
    int _ns_2733 = NOVALUE;
    int _source_idx_2736 = NOVALUE;
    int _k_2743 = NOVALUE;
    int _1472 = NOVALUE;
    int _1471 = NOVALUE;
    int _1469 = NOVALUE;
    int _1466 = NOVALUE;
    int _1464 = NOVALUE;
    int _1463 = NOVALUE;
    int _1462 = NOVALUE;
    int _1461 = NOVALUE;
    int _1459 = NOVALUE;
    int _1458 = NOVALUE;
    int _1457 = NOVALUE;
    int _1456 = NOVALUE;
    int _1454 = NOVALUE;
    int _1453 = NOVALUE;
    int _1451 = NOVALUE;
    int _1449 = NOVALUE;
    int _1448 = NOVALUE;
    int _1446 = NOVALUE;
    int _1443 = NOVALUE;
    int _1442 = NOVALUE;
    int _1439 = NOVALUE;
    int _1438 = NOVALUE;
    int _1434 = NOVALUE;
    int _1429 = NOVALUE;
    int _1427 = NOVALUE;
    int _1426 = NOVALUE;
    int _1425 = NOVALUE;
    int _1424 = NOVALUE;
    int _1422 = NOVALUE;
    int _1420 = NOVALUE;
    int _1418 = NOVALUE;
    int _1417 = NOVALUE;
    int _1416 = NOVALUE;
    int _1415 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_style_2682)) {
        _1 = (long)(DBL_PTR(_style_2682)->dbl);
        if (UNIQUE(DBL_PTR(_style_2682)) && (DBL_PTR(_style_2682)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_style_2682);
        _style_2682 = _1;
    }

    /** 	if atom(size) and not integer(size) then*/
    _1415 = IS_ATOM(_size_2681);
    if (_1415 == 0) {
        goto L1; // [12] 32
    }
    if (IS_ATOM_INT(_size_2681))
    _1417 = 1;
    else if (IS_ATOM_DBL(_size_2681))
    _1417 = IS_ATOM_INT(DoubleToInt(_size_2681));
    else
    _1417 = 0;
    _1418 = (_1417 == 0);
    _1417 = NOVALUE;
    if (_1418 == 0)
    {
        DeRef(_1418);
        _1418 = NOVALUE;
        goto L1; // [23] 32
    }
    else{
        DeRef(_1418);
        _1418 = NOVALUE;
    }

    /** 		size = floor(size)*/
    _0 = _size_2681;
    if (IS_ATOM_INT(_size_2681))
    _size_2681 = e_floor(_size_2681);
    else
    _size_2681 = unary_op(FLOOR, _size_2681);
    DeRef(_0);
L1: 

    /** 	if integer(size) then*/
    if (IS_ATOM_INT(_size_2681))
    _1420 = 1;
    else if (IS_ATOM_DBL(_size_2681))
    _1420 = IS_ATOM_INT(DoubleToInt(_size_2681));
    else
    _1420 = 0;
    if (_1420 == 0)
    {
        _1420 = NOVALUE;
        goto L2; // [37] 269
    }
    else{
        _1420 = NOVALUE;
    }

    /** 		integer len*/

    /** 		integer rem*/

    /** 		if style = BK_LEN then*/
    if (_style_2682 != 1)
    goto L3; // [48] 133

    /** 			if size < 1 or size >= length(source) then*/
    if (IS_ATOM_INT(_size_2681)) {
        _1422 = (_size_2681 < 1);
    }
    else {
        _1422 = binary_op(LESS, _size_2681, 1);
    }
    if (IS_ATOM_INT(_1422)) {
        if (_1422 != 0) {
            goto L4; // [58] 74
        }
    }
    else {
        if (DBL_PTR(_1422)->dbl != 0.0) {
            goto L4; // [58] 74
        }
    }
    if (IS_SEQUENCE(_source_2680)){
            _1424 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1424 = 1;
    }
    if (IS_ATOM_INT(_size_2681)) {
        _1425 = (_size_2681 >= _1424);
    }
    else {
        _1425 = binary_op(GREATEREQ, _size_2681, _1424);
    }
    _1424 = NOVALUE;
    if (_1425 == 0) {
        DeRef(_1425);
        _1425 = NOVALUE;
        goto L5; // [70] 85
    }
    else {
        if (!IS_ATOM_INT(_1425) && DBL_PTR(_1425)->dbl == 0.0){
            DeRef(_1425);
            _1425 = NOVALUE;
            goto L5; // [70] 85
        }
        DeRef(_1425);
        _1425 = NOVALUE;
    }
    DeRef(_1425);
    _1425 = NOVALUE;
L4: 

    /** 				return {source}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_source_2680);
    *((int *)(_2+4)) = _source_2680;
    _1426 = MAKE_SEQ(_1);
    DeRefDS(_source_2680);
    DeRef(_size_2681);
    DeRef(_ns_2733);
    DeRef(_1422);
    _1422 = NOVALUE;
    return _1426;
L5: 

    /** 			len = floor(length(source) / size)*/
    if (IS_SEQUENCE(_source_2680)){
            _1427 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1427 = 1;
    }
    if (IS_ATOM_INT(_size_2681)) {
        if (_size_2681 > 0 && _1427 >= 0) {
            _len_2691 = _1427 / _size_2681;
        }
        else {
            temp_dbl = floor((double)_1427 / (double)_size_2681);
            _len_2691 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _1427, _size_2681);
        _len_2691 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1427 = NOVALUE;
    if (!IS_ATOM_INT(_len_2691)) {
        _1 = (long)(DBL_PTR(_len_2691)->dbl);
        if (UNIQUE(DBL_PTR(_len_2691)) && (DBL_PTR(_len_2691)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_2691);
        _len_2691 = _1;
    }

    /** 			rem = remainder(length(source), size)*/
    if (IS_SEQUENCE(_source_2680)){
            _1429 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1429 = 1;
    }
    if (IS_ATOM_INT(_size_2681)) {
        _rem_2692 = (_1429 % _size_2681);
    }
    else {
        _rem_2692 = binary_op(REMAINDER, _1429, _size_2681);
    }
    _1429 = NOVALUE;
    if (!IS_ATOM_INT(_rem_2692)) {
        _1 = (long)(DBL_PTR(_rem_2692)->dbl);
        if (UNIQUE(DBL_PTR(_rem_2692)) && (DBL_PTR(_rem_2692)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rem_2692);
        _rem_2692 = _1;
    }

    /** 			size = repeat(size, len)*/
    _0 = _size_2681;
    _size_2681 = Repeat(_size_2681, _len_2691);
    DeRef(_0);

    /** 			if rem > 0 then*/
    if (_rem_2692 <= 0)
    goto L6; // [119] 268

    /** 				size &= rem*/
    Append(&_size_2681, _size_2681, _rem_2692);
    goto L6; // [130] 268
L3: 

    /** 			if size > length(source) then*/
    if (IS_SEQUENCE(_source_2680)){
            _1434 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1434 = 1;
    }
    if (binary_op_a(LESSEQ, _size_2681, _1434)){
        _1434 = NOVALUE;
        goto L7; // [138] 148
    }
    _1434 = NOVALUE;

    /** 				size = length(source)*/
    DeRef(_size_2681);
    if (IS_SEQUENCE(_source_2680)){
            _size_2681 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _size_2681 = 1;
    }
L7: 

    /** 			if size < 1 then*/
    if (binary_op_a(GREATEREQ, _size_2681, 1)){
        goto L8; // [150] 165
    }

    /** 				return {source}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_source_2680);
    *((int *)(_2+4)) = _source_2680;
    _1438 = MAKE_SEQ(_1);
    DeRefDS(_source_2680);
    DeRef(_size_2681);
    DeRef(_ns_2733);
    DeRef(_1422);
    _1422 = NOVALUE;
    DeRef(_1426);
    _1426 = NOVALUE;
    return _1438;
L8: 

    /** 			len = floor(length(source) / size)*/
    if (IS_SEQUENCE(_source_2680)){
            _1439 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1439 = 1;
    }
    if (IS_ATOM_INT(_size_2681)) {
        if (_size_2681 > 0 && _1439 >= 0) {
            _len_2691 = _1439 / _size_2681;
        }
        else {
            temp_dbl = floor((double)_1439 / (double)_size_2681);
            _len_2691 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _1439, _size_2681);
        _len_2691 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1439 = NOVALUE;
    if (!IS_ATOM_INT(_len_2691)) {
        _1 = (long)(DBL_PTR(_len_2691)->dbl);
        if (UNIQUE(DBL_PTR(_len_2691)) && (DBL_PTR(_len_2691)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_2691);
        _len_2691 = _1;
    }

    /** 			if len < 1 then*/
    if (_len_2691 >= 1)
    goto L9; // [180] 192

    /** 				len = 1*/
    _len_2691 = 1;
L9: 

    /** 			rem = length(source) - (size * len)*/
    if (IS_SEQUENCE(_source_2680)){
            _1442 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1442 = 1;
    }
    if (IS_ATOM_INT(_size_2681)) {
        if (_size_2681 == (short)_size_2681 && _len_2691 <= INT15 && _len_2691 >= -INT15)
        _1443 = _size_2681 * _len_2691;
        else
        _1443 = NewDouble(_size_2681 * (double)_len_2691);
    }
    else {
        _1443 = binary_op(MULTIPLY, _size_2681, _len_2691);
    }
    if (IS_ATOM_INT(_1443)) {
        _rem_2692 = _1442 - _1443;
    }
    else {
        _rem_2692 = binary_op(MINUS, _1442, _1443);
    }
    _1442 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    if (!IS_ATOM_INT(_rem_2692)) {
        _1 = (long)(DBL_PTR(_rem_2692)->dbl);
        if (UNIQUE(DBL_PTR(_rem_2692)) && (DBL_PTR(_rem_2692)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rem_2692);
        _rem_2692 = _1;
    }

    /** 			size = repeat(len, size)*/
    _0 = _size_2681;
    _size_2681 = Repeat(_len_2691, _size_2681);
    DeRef(_0);

    /** 			for i = 1 to length(size) do*/
    if (IS_SEQUENCE(_size_2681)){
            _1446 = SEQ_PTR(_size_2681)->length;
    }
    else {
        _1446 = 1;
    }
    {
        int _i_2726;
        _i_2726 = 1;
LA: 
        if (_i_2726 > _1446){
            goto LB; // [220] 267
        }

        /** 				if rem = 0 then*/
        if (_rem_2692 != 0)
        goto LC; // [229] 238

        /** 					exit*/
        goto LB; // [235] 267
LC: 

        /** 				size[i] += 1*/
        _2 = (int)SEQ_PTR(_size_2681);
        _1448 = (int)*(((s1_ptr)_2)->base + _i_2726);
        if (IS_ATOM_INT(_1448)) {
            _1449 = _1448 + 1;
            if (_1449 > MAXINT){
                _1449 = NewDouble((double)_1449);
            }
        }
        else
        _1449 = binary_op(PLUS, 1, _1448);
        _1448 = NOVALUE;
        _2 = (int)SEQ_PTR(_size_2681);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _size_2681 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2726);
        _1 = *(int *)_2;
        *(int *)_2 = _1449;
        if( _1 != _1449 ){
            DeRef(_1);
        }
        _1449 = NOVALUE;

        /** 				rem -= 1*/
        _rem_2692 = _rem_2692 - 1;

        /** 			end for*/
        _i_2726 = _i_2726 + 1;
        goto LA; // [262] 227
LB: 
        ;
    }
L6: 
L2: 

    /** 	sequence ns = repeat(0, length(size))*/
    if (IS_SEQUENCE(_size_2681)){
            _1451 = SEQ_PTR(_size_2681)->length;
    }
    else {
        _1451 = 1;
    }
    DeRef(_ns_2733);
    _ns_2733 = Repeat(0, _1451);
    _1451 = NOVALUE;

    /** 	integer source_idx = 1*/
    _source_idx_2736 = 1;

    /** 	for i = 1 to length(size) do*/
    if (IS_SEQUENCE(_size_2681)){
            _1453 = SEQ_PTR(_size_2681)->length;
    }
    else {
        _1453 = 1;
    }
    {
        int _i_2738;
        _i_2738 = 1;
LD: 
        if (_i_2738 > _1453){
            goto LE; // [292] 432
        }

        /** 		if source_idx <= length(source) then*/
        if (IS_SEQUENCE(_source_2680)){
                _1454 = SEQ_PTR(_source_2680)->length;
        }
        else {
            _1454 = 1;
        }
        if (_source_idx_2736 > _1454)
        goto LF; // [304] 418

        /** 			integer k = 1*/
        _k_2743 = 1;

        /** 			ns[i] = repeat(0, size[i])*/
        _2 = (int)SEQ_PTR(_size_2681);
        _1456 = (int)*(((s1_ptr)_2)->base + _i_2738);
        _1457 = Repeat(0, _1456);
        _1456 = NOVALUE;
        _2 = (int)SEQ_PTR(_ns_2733);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ns_2733 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2738);
        _1 = *(int *)_2;
        *(int *)_2 = _1457;
        if( _1 != _1457 ){
            DeRef(_1);
        }
        _1457 = NOVALUE;

        /** 			for j = 1 to size[i] do*/
        _2 = (int)SEQ_PTR(_size_2681);
        _1458 = (int)*(((s1_ptr)_2)->base + _i_2738);
        {
            int _j_2747;
            _j_2747 = 1;
L10: 
            if (binary_op_a(GREATER, _j_2747, _1458)){
                goto L11; // [335] 413
            }

            /** 				if source_idx > length(source) then*/
            if (IS_SEQUENCE(_source_2680)){
                    _1459 = SEQ_PTR(_source_2680)->length;
            }
            else {
                _1459 = 1;
            }
            if (_source_idx_2736 <= _1459)
            goto L12; // [347] 375

            /** 					ns[i] = ns[i][1 .. k-1]*/
            _2 = (int)SEQ_PTR(_ns_2733);
            _1461 = (int)*(((s1_ptr)_2)->base + _i_2738);
            _1462 = _k_2743 - 1;
            rhs_slice_target = (object_ptr)&_1463;
            RHS_Slice(_1461, 1, _1462);
            _1461 = NOVALUE;
            _2 = (int)SEQ_PTR(_ns_2733);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _ns_2733 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_2738);
            _1 = *(int *)_2;
            *(int *)_2 = _1463;
            if( _1 != _1463 ){
                DeRef(_1);
            }
            _1463 = NOVALUE;

            /** 					exit*/
            goto L11; // [372] 413
L12: 

            /** 				ns[i][k] = source[source_idx]*/
            _2 = (int)SEQ_PTR(_ns_2733);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _ns_2733 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_2738 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_source_2680);
            _1466 = (int)*(((s1_ptr)_2)->base + _source_idx_2736);
            Ref(_1466);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _k_2743);
            _1 = *(int *)_2;
            *(int *)_2 = _1466;
            if( _1 != _1466 ){
                DeRef(_1);
            }
            _1466 = NOVALUE;
            _1464 = NOVALUE;

            /** 				k += 1*/
            _k_2743 = _k_2743 + 1;

            /** 				source_idx += 1*/
            _source_idx_2736 = _source_idx_2736 + 1;

            /** 			end for*/
            _0 = _j_2747;
            if (IS_ATOM_INT(_j_2747)) {
                _j_2747 = _j_2747 + 1;
                if ((long)((unsigned long)_j_2747 +(unsigned long) HIGH_BITS) >= 0){
                    _j_2747 = NewDouble((double)_j_2747);
                }
            }
            else {
                _j_2747 = binary_op_a(PLUS, _j_2747, 1);
            }
            DeRef(_0);
            goto L10; // [408] 342
L11: 
            ;
            DeRef(_j_2747);
        }
        goto L13; // [415] 425
LF: 

        /** 			ns[i] = {}*/
        RefDS(_5);
        _2 = (int)SEQ_PTR(_ns_2733);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ns_2733 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2738);
        _1 = *(int *)_2;
        *(int *)_2 = _5;
        DeRef(_1);
L13: 

        /** 	end for*/
        _i_2738 = _i_2738 + 1;
        goto LD; // [427] 299
LE: 
        ;
    }

    /** 	if source_idx <= length(source) then*/
    if (IS_SEQUENCE(_source_2680)){
            _1469 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1469 = 1;
    }
    if (_source_idx_2736 > _1469)
    goto L14; // [437] 456

    /** 		ns = append(ns, source[source_idx .. $])*/
    if (IS_SEQUENCE(_source_2680)){
            _1471 = SEQ_PTR(_source_2680)->length;
    }
    else {
        _1471 = 1;
    }
    rhs_slice_target = (object_ptr)&_1472;
    RHS_Slice(_source_2680, _source_idx_2736, _1471);
    RefDS(_1472);
    Append(&_ns_2733, _ns_2733, _1472);
    DeRefDS(_1472);
    _1472 = NOVALUE;
L14: 

    /** 	return ns*/
    DeRefDS(_source_2680);
    DeRef(_size_2681);
    DeRef(_1422);
    _1422 = NOVALUE;
    DeRef(_1426);
    _1426 = NOVALUE;
    DeRef(_1438);
    _1438 = NOVALUE;
    _1458 = NOVALUE;
    DeRef(_1462);
    _1462 = NOVALUE;
    return _ns_2733;
    ;
}


int _3flatten(int _s_2769, int _delim_2770)
{
    int _ret_2771 = NOVALUE;
    int _x_2772 = NOVALUE;
    int _len_2773 = NOVALUE;
    int _pos_2774 = NOVALUE;
    int _temp_2793 = NOVALUE;
    int _1501 = NOVALUE;
    int _1500 = NOVALUE;
    int _1499 = NOVALUE;
    int _1497 = NOVALUE;
    int _1496 = NOVALUE;
    int _1495 = NOVALUE;
    int _1493 = NOVALUE;
    int _1491 = NOVALUE;
    int _1490 = NOVALUE;
    int _1489 = NOVALUE;
    int _1488 = NOVALUE;
    int _1486 = NOVALUE;
    int _1485 = NOVALUE;
    int _1484 = NOVALUE;
    int _1483 = NOVALUE;
    int _1482 = NOVALUE;
    int _1481 = NOVALUE;
    int _1480 = NOVALUE;
    int _1478 = NOVALUE;
    int _1477 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ret = s*/
    RefDS(_s_2769);
    DeRef(_ret_2771);
    _ret_2771 = _s_2769;

    /** 	pos = 1*/
    _pos_2774 = 1;

    /** 	len = length(ret)*/
    if (IS_SEQUENCE(_ret_2771)){
            _len_2773 = SEQ_PTR(_ret_2771)->length;
    }
    else {
        _len_2773 = 1;
    }

    /** 	while pos <= len do*/
L1: 
    if (_pos_2774 > _len_2773)
    goto L2; // [29] 197

    /** 		x = ret[pos]*/
    DeRef(_x_2772);
    _2 = (int)SEQ_PTR(_ret_2771);
    _x_2772 = (int)*(((s1_ptr)_2)->base + _pos_2774);
    Ref(_x_2772);

    /** 		if sequence(x) then*/
    _1477 = IS_SEQUENCE(_x_2772);
    if (_1477 == 0)
    {
        _1477 = NOVALUE;
        goto L3; // [44] 183
    }
    else{
        _1477 = NOVALUE;
    }

    /** 			if length(delim) = 0 then*/
    if (IS_SEQUENCE(_delim_2770)){
            _1478 = SEQ_PTR(_delim_2770)->length;
    }
    else {
        _1478 = 1;
    }
    if (_1478 != 0)
    goto L4; // [52] 96

    /** 				ret = ret[1..pos-1] & flatten(x) & ret[pos+1 .. $]*/
    _1480 = _pos_2774 - 1;
    rhs_slice_target = (object_ptr)&_1481;
    RHS_Slice(_ret_2771, 1, _1480);
    Ref(_x_2772);
    DeRef(_1482);
    _1482 = _x_2772;
    RefDS(_5);
    _1483 = _3flatten(_1482, _5);
    _1482 = NOVALUE;
    _1484 = _pos_2774 + 1;
    if (_1484 > MAXINT){
        _1484 = NewDouble((double)_1484);
    }
    if (IS_SEQUENCE(_ret_2771)){
            _1485 = SEQ_PTR(_ret_2771)->length;
    }
    else {
        _1485 = 1;
    }
    rhs_slice_target = (object_ptr)&_1486;
    RHS_Slice(_ret_2771, _1484, _1485);
    {
        int concat_list[3];

        concat_list[0] = _1486;
        concat_list[1] = _1483;
        concat_list[2] = _1481;
        Concat_N((object_ptr)&_ret_2771, concat_list, 3);
    }
    DeRefDS(_1486);
    _1486 = NOVALUE;
    DeRef(_1483);
    _1483 = NOVALUE;
    DeRefDS(_1481);
    _1481 = NOVALUE;
    goto L5; // [93] 173
L4: 

    /** 				sequence temp = ret[1..pos-1] & flatten(x)*/
    _1488 = _pos_2774 - 1;
    rhs_slice_target = (object_ptr)&_1489;
    RHS_Slice(_ret_2771, 1, _1488);
    Ref(_x_2772);
    DeRef(_1490);
    _1490 = _x_2772;
    RefDS(_5);
    _1491 = _3flatten(_1490, _5);
    _1490 = NOVALUE;
    if (IS_SEQUENCE(_1489) && IS_ATOM(_1491)) {
        Ref(_1491);
        Append(&_temp_2793, _1489, _1491);
    }
    else if (IS_ATOM(_1489) && IS_SEQUENCE(_1491)) {
    }
    else {
        Concat((object_ptr)&_temp_2793, _1489, _1491);
        DeRefDS(_1489);
        _1489 = NOVALUE;
    }
    DeRef(_1489);
    _1489 = NOVALUE;
    DeRef(_1491);
    _1491 = NOVALUE;

    /** 				if pos != length(ret) then*/
    if (IS_SEQUENCE(_ret_2771)){
            _1493 = SEQ_PTR(_ret_2771)->length;
    }
    else {
        _1493 = 1;
    }
    if (_pos_2774 == _1493)
    goto L6; // [124] 151

    /** 					ret = temp &  delim & ret[pos+1 .. $]*/
    _1495 = _pos_2774 + 1;
    if (_1495 > MAXINT){
        _1495 = NewDouble((double)_1495);
    }
    if (IS_SEQUENCE(_ret_2771)){
            _1496 = SEQ_PTR(_ret_2771)->length;
    }
    else {
        _1496 = 1;
    }
    rhs_slice_target = (object_ptr)&_1497;
    RHS_Slice(_ret_2771, _1495, _1496);
    {
        int concat_list[3];

        concat_list[0] = _1497;
        concat_list[1] = _delim_2770;
        concat_list[2] = _temp_2793;
        Concat_N((object_ptr)&_ret_2771, concat_list, 3);
    }
    DeRefDS(_1497);
    _1497 = NOVALUE;
    goto L7; // [148] 170
L6: 

    /** 					ret = temp & ret[pos+1 .. $]*/
    _1499 = _pos_2774 + 1;
    if (_1499 > MAXINT){
        _1499 = NewDouble((double)_1499);
    }
    if (IS_SEQUENCE(_ret_2771)){
            _1500 = SEQ_PTR(_ret_2771)->length;
    }
    else {
        _1500 = 1;
    }
    rhs_slice_target = (object_ptr)&_1501;
    RHS_Slice(_ret_2771, _1499, _1500);
    Concat((object_ptr)&_ret_2771, _temp_2793, _1501);
    DeRefDS(_1501);
    _1501 = NOVALUE;
L7: 
    DeRef(_temp_2793);
    _temp_2793 = NOVALUE;
L5: 

    /** 			len = length(ret)*/
    if (IS_SEQUENCE(_ret_2771)){
            _len_2773 = SEQ_PTR(_ret_2771)->length;
    }
    else {
        _len_2773 = 1;
    }
    goto L1; // [180] 29
L3: 

    /** 			pos += 1*/
    _pos_2774 = _pos_2774 + 1;

    /** 	end while*/
    goto L1; // [194] 29
L2: 

    /** 	return ret*/
    DeRefDS(_s_2769);
    DeRef(_delim_2770);
    DeRef(_x_2772);
    DeRef(_1480);
    _1480 = NOVALUE;
    DeRef(_1488);
    _1488 = NOVALUE;
    DeRef(_1495);
    _1495 = NOVALUE;
    DeRef(_1484);
    _1484 = NOVALUE;
    DeRef(_1499);
    _1499 = NOVALUE;
    return _ret_2771;
    ;
}


int _3pivot(int _data_p_2816, int _pivot_p_2817)
{
    int _result__2818 = NOVALUE;
    int _pos__2819 = NOVALUE;
    int _1514 = NOVALUE;
    int _1513 = NOVALUE;
    int _1512 = NOVALUE;
    int _1510 = NOVALUE;
    int _1509 = NOVALUE;
    int _1508 = NOVALUE;
    int _1506 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_ = {{}, {}, {}}*/
    _0 = _result__2818;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_5, 3);
    *((int *)(_2+4)) = _5;
    *((int *)(_2+8)) = _5;
    *((int *)(_2+12)) = _5;
    _result__2818 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	if atom(data_p) then*/
    _1506 = IS_ATOM(_data_p_2816);
    if (_1506 == 0)
    {
        _1506 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _1506 = NOVALUE;
    }

    /** 		data_p = {data_p}*/
    _0 = _data_p_2816;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_data_p_2816);
    *((int *)(_2+4)) = _data_p_2816;
    _data_p_2816 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = 1 to length(data_p) do*/
    if (IS_SEQUENCE(_data_p_2816)){
            _1508 = SEQ_PTR(_data_p_2816)->length;
    }
    else {
        _1508 = 1;
    }
    {
        int _i_2825;
        _i_2825 = 1;
L2: 
        if (_i_2825 > _1508){
            goto L3; // [29] 77
        }

        /** 		pos_ = eu:compare(data_p[i], pivot_p) + 2*/
        _2 = (int)SEQ_PTR(_data_p_2816);
        _1509 = (int)*(((s1_ptr)_2)->base + _i_2825);
        if (IS_ATOM_INT(_1509) && IS_ATOM_INT(_pivot_p_2817)){
            _1510 = (_1509 < _pivot_p_2817) ? -1 : (_1509 > _pivot_p_2817);
        }
        else{
            _1510 = compare(_1509, _pivot_p_2817);
        }
        _1509 = NOVALUE;
        _pos__2819 = _1510 + 2;
        _1510 = NOVALUE;

        /** 		result_[pos_] = append(result_[pos_], data_p[i])*/
        _2 = (int)SEQ_PTR(_result__2818);
        _1512 = (int)*(((s1_ptr)_2)->base + _pos__2819);
        _2 = (int)SEQ_PTR(_data_p_2816);
        _1513 = (int)*(((s1_ptr)_2)->base + _i_2825);
        Ref(_1513);
        Append(&_1514, _1512, _1513);
        _1512 = NOVALUE;
        _1513 = NOVALUE;
        _2 = (int)SEQ_PTR(_result__2818);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result__2818 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__2819);
        _1 = *(int *)_2;
        *(int *)_2 = _1514;
        if( _1 != _1514 ){
            DeRefDS(_1);
        }
        _1514 = NOVALUE;

        /** 	end for*/
        _i_2825 = _i_2825 + 1;
        goto L2; // [72] 36
L3: 
        ;
    }

    /** 	return result_*/
    DeRef(_data_p_2816);
    DeRef(_pivot_p_2817);
    return _result__2818;
    ;
}


int _3build_list(int _source_2835, int _transformer_2836, int _singleton_2837, int _user_data_2838)
{
    int _result_2839 = NOVALUE;
    int _x_2840 = NOVALUE;
    int _new_x_2841 = NOVALUE;
    int _1531 = NOVALUE;
    int _1529 = NOVALUE;
    int _1527 = NOVALUE;
    int _1525 = NOVALUE;
    int _1523 = NOVALUE;
    int _1522 = NOVALUE;
    int _1520 = NOVALUE;
    int _1519 = NOVALUE;
    int _1518 = NOVALUE;
    int _1517 = NOVALUE;
    int _1515 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_singleton_2837)) {
        _1 = (long)(DBL_PTR(_singleton_2837)->dbl);
        if (UNIQUE(DBL_PTR(_singleton_2837)) && (DBL_PTR(_singleton_2837)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_singleton_2837);
        _singleton_2837 = _1;
    }

    /** 	sequence result = {}*/
    RefDS(_5);
    DeRef(_result_2839);
    _result_2839 = _5;

    /** 	if atom(transformer) then*/
    _1515 = IS_ATOM(_transformer_2836);
    if (_1515 == 0)
    {
        _1515 = NOVALUE;
        goto L1; // [19] 29
    }
    else{
        _1515 = NOVALUE;
    }

    /** 		transformer = {transformer}*/
    _0 = _transformer_2836;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_transformer_2836);
    *((int *)(_2+4)) = _transformer_2836;
    _transformer_2836 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_2835)){
            _1517 = SEQ_PTR(_source_2835)->length;
    }
    else {
        _1517 = 1;
    }
    {
        int _i_2846;
        _i_2846 = 1;
L2: 
        if (_i_2846 > _1517){
            goto L3; // [34] 195
        }

        /** 		x = {source[i], {i, length(source)}, user_data}*/
        _2 = (int)SEQ_PTR(_source_2835);
        _1518 = (int)*(((s1_ptr)_2)->base + _i_2846);
        if (IS_SEQUENCE(_source_2835)){
                _1519 = SEQ_PTR(_source_2835)->length;
        }
        else {
            _1519 = 1;
        }
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_2846;
        ((int *)_2)[2] = _1519;
        _1520 = MAKE_SEQ(_1);
        _1519 = NOVALUE;
        _0 = _x_2840;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_1518);
        *((int *)(_2+4)) = _1518;
        *((int *)(_2+8)) = _1520;
        Ref(_user_data_2838);
        *((int *)(_2+12)) = _user_data_2838;
        _x_2840 = MAKE_SEQ(_1);
        DeRef(_0);
        _1520 = NOVALUE;
        _1518 = NOVALUE;

        /** 		for j = 1 to length(transformer) do*/
        if (IS_SEQUENCE(_transformer_2836)){
                _1522 = SEQ_PTR(_transformer_2836)->length;
        }
        else {
            _1522 = 1;
        }
        {
            int _j_2853;
            _j_2853 = 1;
L4: 
            if (_j_2853 > _1522){
                goto L5; // [65] 188
            }

            /** 			if transformer[j] >= 0 then*/
            _2 = (int)SEQ_PTR(_transformer_2836);
            _1523 = (int)*(((s1_ptr)_2)->base + _j_2853);
            if (binary_op_a(LESS, _1523, 0)){
                _1523 = NOVALUE;
                goto L6; // [78] 145
            }
            _1523 = NOVALUE;

            /** 				new_x = call_func(transformer[j], x)*/
            _2 = (int)SEQ_PTR(_transformer_2836);
            _1525 = (int)*(((s1_ptr)_2)->base + _j_2853);
            _1 = (int)SEQ_PTR(_x_2840);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_1525].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            Ref(*(int *)(_2+12));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8), 
                                *(int *)(_2+12)
                                 );
            DeRef(_new_x_2841);
            _new_x_2841 = _1;

            /** 				if length(new_x) = 0 then*/
            if (IS_SEQUENCE(_new_x_2841)){
                    _1527 = SEQ_PTR(_new_x_2841)->length;
            }
            else {
                _1527 = 1;
            }
            if (_1527 != 0)
            goto L7; // [97] 106

            /** 					continue*/
            goto L8; // [103] 183
L7: 

            /** 				if new_x[1] = 0 then*/
            _2 = (int)SEQ_PTR(_new_x_2841);
            _1529 = (int)*(((s1_ptr)_2)->base + 1);
            if (binary_op_a(NOTEQ, _1529, 0)){
                _1529 = NOVALUE;
                goto L9; // [112] 121
            }
            _1529 = NOVALUE;

            /** 					continue*/
            goto L8; // [118] 183
L9: 

            /** 				if new_x[1] < 0 then*/
            _2 = (int)SEQ_PTR(_new_x_2841);
            _1531 = (int)*(((s1_ptr)_2)->base + 1);
            if (binary_op_a(GREATEREQ, _1531, 0)){
                _1531 = NOVALUE;
                goto LA; // [127] 136
            }
            _1531 = NOVALUE;

            /** 					exit*/
            goto L5; // [133] 188
LA: 

            /** 				new_x = new_x[2]*/
            _0 = _new_x_2841;
            _2 = (int)SEQ_PTR(_new_x_2841);
            _new_x_2841 = (int)*(((s1_ptr)_2)->base + 2);
            Ref(_new_x_2841);
            DeRef(_0);
            goto LB; // [142] 152
L6: 

            /** 				new_x = x[1]*/
            DeRef(_new_x_2841);
            _2 = (int)SEQ_PTR(_x_2840);
            _new_x_2841 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_new_x_2841);
LB: 

            /** 			if singleton then*/
            if (_singleton_2837 == 0)
            {
                goto LC; // [154] 168
            }
            else{
            }

            /** 				result = append(result, new_x)*/
            Ref(_new_x_2841);
            Append(&_result_2839, _result_2839, _new_x_2841);
            goto L5; // [165] 188
LC: 

            /** 				result &= new_x*/
            if (IS_SEQUENCE(_result_2839) && IS_ATOM(_new_x_2841)) {
                Ref(_new_x_2841);
                Append(&_result_2839, _result_2839, _new_x_2841);
            }
            else if (IS_ATOM(_result_2839) && IS_SEQUENCE(_new_x_2841)) {
            }
            else {
                Concat((object_ptr)&_result_2839, _result_2839, _new_x_2841);
            }

            /** 			exit*/
            goto L5; // [179] 188

            /** 		end for*/
L8: 
            _j_2853 = _j_2853 + 1;
            goto L4; // [183] 72
L5: 
            ;
        }

        /** 	end for*/
        _i_2846 = _i_2846 + 1;
        goto L2; // [190] 41
L3: 
        ;
    }

    /** 	return result*/
    DeRefDS(_source_2835);
    DeRef(_transformer_2836);
    DeRef(_user_data_2838);
    DeRef(_x_2840);
    DeRef(_new_x_2841);
    _1525 = NOVALUE;
    return _result_2839;
    ;
}


int _3transform(int _source_data_2878, int _transformer_rids_2879)
{
    int _lResult_2880 = NOVALUE;
    int _1551 = NOVALUE;
    int _1550 = NOVALUE;
    int _1549 = NOVALUE;
    int _1548 = NOVALUE;
    int _1547 = NOVALUE;
    int _1546 = NOVALUE;
    int _1545 = NOVALUE;
    int _1543 = NOVALUE;
    int _1542 = NOVALUE;
    int _1541 = NOVALUE;
    int _1540 = NOVALUE;
    int _1539 = NOVALUE;
    int _1537 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lResult = source_data*/
    RefDS(_source_data_2878);
    DeRef(_lResult_2880);
    _lResult_2880 = _source_data_2878;

    /** 	if atom(transformer_rids) then*/
    _1537 = IS_ATOM(_transformer_rids_2879);
    if (_1537 == 0)
    {
        _1537 = NOVALUE;
        goto L1; // [15] 25
    }
    else{
        _1537 = NOVALUE;
    }

    /** 		transformer_rids = {transformer_rids}*/
    _0 = _transformer_rids_2879;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_transformer_rids_2879);
    *((int *)(_2+4)) = _transformer_rids_2879;
    _transformer_rids_2879 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = 1 to length(transformer_rids) do*/
    if (IS_SEQUENCE(_transformer_rids_2879)){
            _1539 = SEQ_PTR(_transformer_rids_2879)->length;
    }
    else {
        _1539 = 1;
    }
    {
        int _i_2885;
        _i_2885 = 1;
L2: 
        if (_i_2885 > _1539){
            goto L3; // [30] 112
        }

        /** 		if atom(transformer_rids[i]) then*/
        _2 = (int)SEQ_PTR(_transformer_rids_2879);
        _1540 = (int)*(((s1_ptr)_2)->base + _i_2885);
        _1541 = IS_ATOM(_1540);
        _1540 = NOVALUE;
        if (_1541 == 0)
        {
            _1541 = NOVALUE;
            goto L4; // [46] 68
        }
        else{
            _1541 = NOVALUE;
        }

        /** 			lResult = call_func(transformer_rids[i], {lResult})*/
        _2 = (int)SEQ_PTR(_transformer_rids_2879);
        _1542 = (int)*(((s1_ptr)_2)->base + _i_2885);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_lResult_2880);
        *((int *)(_2+4)) = _lResult_2880;
        _1543 = MAKE_SEQ(_1);
        _1 = (int)SEQ_PTR(_1543);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_1542].addr;
        Ref(*(int *)(_2+4));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRefDS(_lResult_2880);
        _lResult_2880 = _1;
        DeRefDS(_1543);
        _1543 = NOVALUE;
        goto L5; // [65] 105
L4: 

        /** 			lResult = call_func(transformer_rids[i][1], {lResult} & transformer_rids[i][2..$])*/
        _2 = (int)SEQ_PTR(_transformer_rids_2879);
        _1545 = (int)*(((s1_ptr)_2)->base + _i_2885);
        _2 = (int)SEQ_PTR(_1545);
        _1546 = (int)*(((s1_ptr)_2)->base + 1);
        _1545 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_lResult_2880);
        *((int *)(_2+4)) = _lResult_2880;
        _1547 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_transformer_rids_2879);
        _1548 = (int)*(((s1_ptr)_2)->base + _i_2885);
        if (IS_SEQUENCE(_1548)){
                _1549 = SEQ_PTR(_1548)->length;
        }
        else {
            _1549 = 1;
        }
        rhs_slice_target = (object_ptr)&_1550;
        RHS_Slice(_1548, 2, _1549);
        _1548 = NOVALUE;
        Concat((object_ptr)&_1551, _1547, _1550);
        DeRefDS(_1547);
        _1547 = NOVALUE;
        DeRef(_1547);
        _1547 = NOVALUE;
        DeRefDS(_1550);
        _1550 = NOVALUE;
        _1 = (int)SEQ_PTR(_1551);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_1546].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
        }
        DeRefDS(_lResult_2880);
        _lResult_2880 = _1;
        DeRefDS(_1551);
        _1551 = NOVALUE;
L5: 

        /** 	end for*/
        _i_2885 = _i_2885 + 1;
        goto L2; // [107] 37
L3: 
        ;
    }

    /** 	return lResult*/
    DeRefDS(_source_data_2878);
    DeRef(_transformer_rids_2879);
    _1542 = NOVALUE;
    _1546 = NOVALUE;
    return _lResult_2880;
    ;
}


int _3transmute(int _source_data_2904, int _current_items_2905, int _new_items_2906, int _start_2907, int _limit_2908)
{
    int _pos_2910 = NOVALUE;
    int _cs_2911 = NOVALUE;
    int _ns_2912 = NOVALUE;
    int _i_2913 = NOVALUE;
    int _elen_2914 = NOVALUE;
    int _1622 = NOVALUE;
    int _1621 = NOVALUE;
    int _1620 = NOVALUE;
    int _1618 = NOVALUE;
    int _1617 = NOVALUE;
    int _1615 = NOVALUE;
    int _1614 = NOVALUE;
    int _1613 = NOVALUE;
    int _1612 = NOVALUE;
    int _1611 = NOVALUE;
    int _1610 = NOVALUE;
    int _1609 = NOVALUE;
    int _1604 = NOVALUE;
    int _1602 = NOVALUE;
    int _1601 = NOVALUE;
    int _1600 = NOVALUE;
    int _1598 = NOVALUE;
    int _1597 = NOVALUE;
    int _1596 = NOVALUE;
    int _1595 = NOVALUE;
    int _1594 = NOVALUE;
    int _1593 = NOVALUE;
    int _1592 = NOVALUE;
    int _1587 = NOVALUE;
    int _1584 = NOVALUE;
    int _1583 = NOVALUE;
    int _1582 = NOVALUE;
    int _1580 = NOVALUE;
    int _1578 = NOVALUE;
    int _1573 = NOVALUE;
    int _1572 = NOVALUE;
    int _1570 = NOVALUE;
    int _1565 = NOVALUE;
    int _1560 = NOVALUE;
    int _1559 = NOVALUE;
    int _1558 = NOVALUE;
    int _1556 = NOVALUE;
    int _1555 = NOVALUE;
    int _1554 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_2907)) {
        _1 = (long)(DBL_PTR(_start_2907)->dbl);
        if (UNIQUE(DBL_PTR(_start_2907)) && (DBL_PTR(_start_2907)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_2907);
        _start_2907 = _1;
    }
    if (!IS_ATOM_INT(_limit_2908)) {
        _1 = (long)(DBL_PTR(_limit_2908)->dbl);
        if (UNIQUE(DBL_PTR(_limit_2908)) && (DBL_PTR(_limit_2908)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_2908);
        _limit_2908 = _1;
    }

    /** 	if equal(current_items[1], {}) then*/
    _2 = (int)SEQ_PTR(_current_items_2905);
    _1554 = (int)*(((s1_ptr)_2)->base + 1);
    if (_1554 == _5)
    _1555 = 1;
    else if (IS_ATOM_INT(_1554) && IS_ATOM_INT(_5))
    _1555 = 0;
    else
    _1555 = (compare(_1554, _5) == 0);
    _1554 = NOVALUE;
    if (_1555 == 0)
    {
        _1555 = NOVALUE;
        goto L1; // [25] 48
    }
    else{
        _1555 = NOVALUE;
    }

    /** 		cs = 1*/
    _cs_2911 = 1;

    /** 		current_items = current_items[2 .. $]*/
    if (IS_SEQUENCE(_current_items_2905)){
            _1556 = SEQ_PTR(_current_items_2905)->length;
    }
    else {
        _1556 = 1;
    }
    rhs_slice_target = (object_ptr)&_current_items_2905;
    RHS_Slice(_current_items_2905, 2, _1556);
    goto L2; // [45] 56
L1: 

    /** 		cs = 0*/
    _cs_2911 = 0;
L2: 

    /** 	if equal(new_items[1], {}) then*/
    _2 = (int)SEQ_PTR(_new_items_2906);
    _1558 = (int)*(((s1_ptr)_2)->base + 1);
    if (_1558 == _5)
    _1559 = 1;
    else if (IS_ATOM_INT(_1558) && IS_ATOM_INT(_5))
    _1559 = 0;
    else
    _1559 = (compare(_1558, _5) == 0);
    _1558 = NOVALUE;
    if (_1559 == 0)
    {
        _1559 = NOVALUE;
        goto L3; // [66] 89
    }
    else{
        _1559 = NOVALUE;
    }

    /** 		ns = 1*/
    _ns_2912 = 1;

    /** 		new_items = new_items[2 .. $]*/
    if (IS_SEQUENCE(_new_items_2906)){
            _1560 = SEQ_PTR(_new_items_2906)->length;
    }
    else {
        _1560 = 1;
    }
    rhs_slice_target = (object_ptr)&_new_items_2906;
    RHS_Slice(_new_items_2906, 2, _1560);
    goto L4; // [86] 97
L3: 

    /** 		ns = 0*/
    _ns_2912 = 0;
L4: 

    /** 	i = start - 1*/
    _i_2913 = _start_2907 - 1;

    /** 	if cs = 0 then*/
    if (_cs_2911 != 0)
    goto L5; // [109] 297

    /** 		if ns = 0 then*/
    if (_ns_2912 != 0)
    goto L6; // [117] 197

    /** 			while i < length(source_data) do*/
L7: 
    if (IS_SEQUENCE(_source_data_2904)){
            _1565 = SEQ_PTR(_source_data_2904)->length;
    }
    else {
        _1565 = 1;
    }
    if (_i_2913 >= _1565)
    goto L8; // [129] 617

    /** 				if limit <= 0 then*/
    if (_limit_2908 > 0)
    goto L9; // [135] 144

    /** 					exit*/
    goto L8; // [141] 617
L9: 

    /** 				limit -= 1*/
    _limit_2908 = _limit_2908 - 1;

    /** 				i += 1*/
    _i_2913 = _i_2913 + 1;

    /** 				pos = find(source_data[i], current_items) */
    _2 = (int)SEQ_PTR(_source_data_2904);
    _1570 = (int)*(((s1_ptr)_2)->base + _i_2913);
    _pos_2910 = find_from(_1570, _current_items_2905, 1);
    _1570 = NOVALUE;

    /** 				if pos then*/
    if (_pos_2910 == 0)
    {
        goto L7; // [175] 126
    }
    else{
    }

    /** 					source_data[i] = new_items[pos]*/
    _2 = (int)SEQ_PTR(_new_items_2906);
    _1572 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    Ref(_1572);
    _2 = (int)SEQ_PTR(_source_data_2904);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _source_data_2904 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_2913);
    _1 = *(int *)_2;
    *(int *)_2 = _1572;
    if( _1 != _1572 ){
        DeRef(_1);
    }
    _1572 = NOVALUE;

    /** 			end while*/
    goto L7; // [191] 126
    goto L8; // [194] 617
L6: 

    /** 			while i < length(source_data) do*/
LA: 
    if (IS_SEQUENCE(_source_data_2904)){
            _1573 = SEQ_PTR(_source_data_2904)->length;
    }
    else {
        _1573 = 1;
    }
    if (_i_2913 >= _1573)
    goto L8; // [205] 617

    /** 				if limit <= 0 then*/
    if (_limit_2908 > 0)
    goto LB; // [211] 220

    /** 					exit*/
    goto L8; // [217] 617
LB: 

    /** 				limit -= 1*/
    _limit_2908 = _limit_2908 - 1;

    /** 				i += 1*/
    _i_2913 = _i_2913 + 1;

    /** 				pos = find(source_data[i], current_items) */
    _2 = (int)SEQ_PTR(_source_data_2904);
    _1578 = (int)*(((s1_ptr)_2)->base + _i_2913);
    _pos_2910 = find_from(_1578, _current_items_2905, 1);
    _1578 = NOVALUE;

    /** 				if pos then*/
    if (_pos_2910 == 0)
    {
        goto LA; // [251] 202
    }
    else{
    }

    /** 					source_data = replace(source_data, new_items[pos], i, i)*/
    _2 = (int)SEQ_PTR(_new_items_2906);
    _1580 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    {
        int p1 = _source_data_2904;
        int p2 = _1580;
        int p3 = _i_2913;
        int p4 = _i_2913;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_source_data_2904;
        Replace( &replace_params );
    }
    _1580 = NOVALUE;

    /** 					i += length(new_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_new_items_2906);
    _1582 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    if (IS_SEQUENCE(_1582)){
            _1583 = SEQ_PTR(_1582)->length;
    }
    else {
        _1583 = 1;
    }
    _1582 = NOVALUE;
    _1584 = _1583 - 1;
    _1583 = NOVALUE;
    _i_2913 = _i_2913 + _1584;
    _1584 = NOVALUE;

    /** 			end while*/
    goto LA; // [290] 202
    goto L8; // [294] 617
L5: 

    /** 		if ns = 0 then*/
    if (_ns_2912 != 0)
    goto LC; // [301] 453

    /** 			while i < length(source_data) do*/
LD: 
    if (IS_SEQUENCE(_source_data_2904)){
            _1587 = SEQ_PTR(_source_data_2904)->length;
    }
    else {
        _1587 = 1;
    }
    if (_i_2913 >= _1587)
    goto LE; // [313] 616

    /** 				if limit <= 0 then*/
    if (_limit_2908 > 0)
    goto LF; // [319] 328

    /** 					exit*/
    goto LE; // [325] 616
LF: 

    /** 				limit -= 1*/
    _limit_2908 = _limit_2908 - 1;

    /** 				i += 1*/
    _i_2913 = _i_2913 + 1;

    /** 				pos = 0*/
    _pos_2910 = 0;

    /** 				for j = 1 to length(current_items) do*/
    if (IS_SEQUENCE(_current_items_2905)){
            _1592 = SEQ_PTR(_current_items_2905)->length;
    }
    else {
        _1592 = 1;
    }
    {
        int _j_2971;
        _j_2971 = 1;
L10: 
        if (_j_2971 > _1592){
            goto L11; // [356] 404
        }

        /** 					if search:begins(current_items[j], source_data[i .. $]) then*/
        _2 = (int)SEQ_PTR(_current_items_2905);
        _1593 = (int)*(((s1_ptr)_2)->base + _j_2971);
        if (IS_SEQUENCE(_source_data_2904)){
                _1594 = SEQ_PTR(_source_data_2904)->length;
        }
        else {
            _1594 = 1;
        }
        rhs_slice_target = (object_ptr)&_1595;
        RHS_Slice(_source_data_2904, _i_2913, _1594);
        Ref(_1593);
        _1596 = _6begins(_1593, _1595);
        _1593 = NOVALUE;
        _1595 = NOVALUE;
        if (_1596 == 0) {
            DeRef(_1596);
            _1596 = NOVALUE;
            goto L12; // [382] 397
        }
        else {
            if (!IS_ATOM_INT(_1596) && DBL_PTR(_1596)->dbl == 0.0){
                DeRef(_1596);
                _1596 = NOVALUE;
                goto L12; // [382] 397
            }
            DeRef(_1596);
            _1596 = NOVALUE;
        }
        DeRef(_1596);
        _1596 = NOVALUE;

        /** 						pos = j*/
        _pos_2910 = _j_2971;

        /** 						exit*/
        goto L11; // [394] 404
L12: 

        /** 				end for*/
        _j_2971 = _j_2971 + 1;
        goto L10; // [399] 363
L11: 
        ;
    }

    /** 				if pos then*/
    if (_pos_2910 == 0)
    {
        goto LD; // [406] 310
    }
    else{
    }

    /** 			    	elen = length(current_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_current_items_2905);
    _1597 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    if (IS_SEQUENCE(_1597)){
            _1598 = SEQ_PTR(_1597)->length;
    }
    else {
        _1598 = 1;
    }
    _1597 = NOVALUE;
    _elen_2914 = _1598 - 1;
    _1598 = NOVALUE;

    /** 					source_data = replace(source_data, {new_items[pos]}, i, i + elen)*/
    _2 = (int)SEQ_PTR(_new_items_2906);
    _1600 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_1600);
    *((int *)(_2+4)) = _1600;
    _1601 = MAKE_SEQ(_1);
    _1600 = NOVALUE;
    _1602 = _i_2913 + _elen_2914;
    if ((long)((unsigned long)_1602 + (unsigned long)HIGH_BITS) >= 0) 
    _1602 = NewDouble((double)_1602);
    {
        int p1 = _source_data_2904;
        int p2 = _1601;
        int p3 = _i_2913;
        int p4 = _1602;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_source_data_2904;
        Replace( &replace_params );
    }
    DeRefDS(_1601);
    _1601 = NOVALUE;
    DeRef(_1602);
    _1602 = NOVALUE;

    /** 			end while*/
    goto LD; // [447] 310
    goto LE; // [450] 616
LC: 

    /** 			while i < length(source_data) do*/
L13: 
    if (IS_SEQUENCE(_source_data_2904)){
            _1604 = SEQ_PTR(_source_data_2904)->length;
    }
    else {
        _1604 = 1;
    }
    if (_i_2913 >= _1604)
    goto L14; // [461] 615

    /** 				if limit <= 0 then*/
    if (_limit_2908 > 0)
    goto L15; // [467] 476

    /** 					exit*/
    goto L14; // [473] 615
L15: 

    /** 				limit -= 1*/
    _limit_2908 = _limit_2908 - 1;

    /** 				i += 1*/
    _i_2913 = _i_2913 + 1;

    /** 				pos = 0*/
    _pos_2910 = 0;

    /** 				for j = 1 to length(current_items) do*/
    if (IS_SEQUENCE(_current_items_2905)){
            _1609 = SEQ_PTR(_current_items_2905)->length;
    }
    else {
        _1609 = 1;
    }
    {
        int _j_2995;
        _j_2995 = 1;
L16: 
        if (_j_2995 > _1609){
            goto L17; // [504] 552
        }

        /** 					if search:begins(current_items[j], source_data[i .. $]) then*/
        _2 = (int)SEQ_PTR(_current_items_2905);
        _1610 = (int)*(((s1_ptr)_2)->base + _j_2995);
        if (IS_SEQUENCE(_source_data_2904)){
                _1611 = SEQ_PTR(_source_data_2904)->length;
        }
        else {
            _1611 = 1;
        }
        rhs_slice_target = (object_ptr)&_1612;
        RHS_Slice(_source_data_2904, _i_2913, _1611);
        Ref(_1610);
        _1613 = _6begins(_1610, _1612);
        _1610 = NOVALUE;
        _1612 = NOVALUE;
        if (_1613 == 0) {
            DeRef(_1613);
            _1613 = NOVALUE;
            goto L18; // [530] 545
        }
        else {
            if (!IS_ATOM_INT(_1613) && DBL_PTR(_1613)->dbl == 0.0){
                DeRef(_1613);
                _1613 = NOVALUE;
                goto L18; // [530] 545
            }
            DeRef(_1613);
            _1613 = NOVALUE;
        }
        DeRef(_1613);
        _1613 = NOVALUE;

        /** 						pos = j*/
        _pos_2910 = _j_2995;

        /** 						exit*/
        goto L17; // [542] 552
L18: 

        /** 				end for*/
        _j_2995 = _j_2995 + 1;
        goto L16; // [547] 511
L17: 
        ;
    }

    /** 				if pos then*/
    if (_pos_2910 == 0)
    {
        goto L13; // [554] 458
    }
    else{
    }

    /** 			    	elen = length(current_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_current_items_2905);
    _1614 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    if (IS_SEQUENCE(_1614)){
            _1615 = SEQ_PTR(_1614)->length;
    }
    else {
        _1615 = 1;
    }
    _1614 = NOVALUE;
    _elen_2914 = _1615 - 1;
    _1615 = NOVALUE;

    /** 					source_data = replace(source_data, new_items[pos], i, i + elen)*/
    _2 = (int)SEQ_PTR(_new_items_2906);
    _1617 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    _1618 = _i_2913 + _elen_2914;
    if ((long)((unsigned long)_1618 + (unsigned long)HIGH_BITS) >= 0) 
    _1618 = NewDouble((double)_1618);
    {
        int p1 = _source_data_2904;
        int p2 = _1617;
        int p3 = _i_2913;
        int p4 = _1618;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_source_data_2904;
        Replace( &replace_params );
    }
    _1617 = NOVALUE;
    DeRef(_1618);
    _1618 = NOVALUE;

    /** 					i += length(new_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_new_items_2906);
    _1620 = (int)*(((s1_ptr)_2)->base + _pos_2910);
    if (IS_SEQUENCE(_1620)){
            _1621 = SEQ_PTR(_1620)->length;
    }
    else {
        _1621 = 1;
    }
    _1620 = NOVALUE;
    _1622 = _1621 - 1;
    _1621 = NOVALUE;
    _i_2913 = _i_2913 + _1622;
    _1622 = NOVALUE;

    /** 			end while*/
    goto L13; // [612] 458
L14: 
LE: 
L8: 

    /** 	return source_data*/
    DeRefDS(_current_items_2905);
    DeRefDS(_new_items_2906);
    _1582 = NOVALUE;
    _1597 = NOVALUE;
    _1614 = NOVALUE;
    _1620 = NOVALUE;
    return _source_data_2904;
    ;
}


int _3sim_index(int _A_3015, int _B_3016)
{
    int _accum_score_3017 = NOVALUE;
    int _pos_factor_3018 = NOVALUE;
    int _indx_a_3019 = NOVALUE;
    int _indx_b_3020 = NOVALUE;
    int _used_A_3021 = NOVALUE;
    int _used_B_3022 = NOVALUE;
    int _total_elems_3081 = NOVALUE;
    int _1693 = NOVALUE;
    int _1691 = NOVALUE;
    int _1690 = NOVALUE;
    int _1687 = NOVALUE;
    int _1686 = NOVALUE;
    int _1685 = NOVALUE;
    int _1684 = NOVALUE;
    int _1682 = NOVALUE;
    int _1681 = NOVALUE;
    int _1680 = NOVALUE;
    int _1679 = NOVALUE;
    int _1678 = NOVALUE;
    int _1676 = NOVALUE;
    int _1675 = NOVALUE;
    int _1672 = NOVALUE;
    int _1671 = NOVALUE;
    int _1670 = NOVALUE;
    int _1669 = NOVALUE;
    int _1668 = NOVALUE;
    int _1666 = NOVALUE;
    int _1665 = NOVALUE;
    int _1664 = NOVALUE;
    int _1663 = NOVALUE;
    int _1662 = NOVALUE;
    int _1660 = NOVALUE;
    int _1659 = NOVALUE;
    int _1653 = NOVALUE;
    int _1652 = NOVALUE;
    int _1651 = NOVALUE;
    int _1650 = NOVALUE;
    int _1649 = NOVALUE;
    int _1648 = NOVALUE;
    int _1647 = NOVALUE;
    int _1646 = NOVALUE;
    int _1644 = NOVALUE;
    int _1643 = NOVALUE;
    int _1642 = NOVALUE;
    int _1641 = NOVALUE;
    int _1640 = NOVALUE;
    int _1639 = NOVALUE;
    int _1637 = NOVALUE;
    int _1635 = NOVALUE;
    int _1634 = NOVALUE;
    int _1633 = NOVALUE;
    int _1632 = NOVALUE;
    int _1631 = NOVALUE;
    int _1628 = NOVALUE;
    int _1626 = NOVALUE;
    int _1624 = NOVALUE;
    int _0, _1, _2;
    

    /** 	accum_score = 0*/
    DeRef(_accum_score_3017);
    _accum_score_3017 = 0;

    /** 	indx_a = 1*/
    _indx_a_3019 = 1;

    /** 	used_A = repeat(0, length(A))*/
    if (IS_SEQUENCE(_A_3015)){
            _1624 = SEQ_PTR(_A_3015)->length;
    }
    else {
        _1624 = 1;
    }
    DeRefi(_used_A_3021);
    _used_A_3021 = Repeat(0, _1624);
    _1624 = NOVALUE;

    /** 	used_B = repeat(0, length(B))*/
    if (IS_SEQUENCE(_B_3016)){
            _1626 = SEQ_PTR(_B_3016)->length;
    }
    else {
        _1626 = 1;
    }
    DeRefi(_used_B_3022);
    _used_B_3022 = Repeat(0, _1626);
    _1626 = NOVALUE;

    /** 	while indx_a <= length(A) label "DoA" do*/
L1: 
    if (IS_SEQUENCE(_A_3015)){
            _1628 = SEQ_PTR(_A_3015)->length;
    }
    else {
        _1628 = 1;
    }
    if (_indx_a_3019 > _1628)
    goto L2; // [43] 244

    /** 		pos_factor = power((1 + length(A) - indx_a) / length(A),2)*/
    if (IS_SEQUENCE(_A_3015)){
            _1631 = SEQ_PTR(_A_3015)->length;
    }
    else {
        _1631 = 1;
    }
    _1632 = _1631 + 1;
    _1631 = NOVALUE;
    _1633 = _1632 - _indx_a_3019;
    if ((long)((unsigned long)_1633 +(unsigned long) HIGH_BITS) >= 0){
        _1633 = NewDouble((double)_1633);
    }
    _1632 = NOVALUE;
    if (IS_SEQUENCE(_A_3015)){
            _1634 = SEQ_PTR(_A_3015)->length;
    }
    else {
        _1634 = 1;
    }
    if (IS_ATOM_INT(_1633)) {
        _1635 = (_1633 % _1634) ? NewDouble((double)_1633 / _1634) : (_1633 / _1634);
    }
    else {
        _1635 = NewDouble(DBL_PTR(_1633)->dbl / (double)_1634);
    }
    DeRef(_1633);
    _1633 = NOVALUE;
    _1634 = NOVALUE;
    DeRef(_pos_factor_3018);
    if (IS_ATOM_INT(_1635) && IS_ATOM_INT(_1635)) {
        if (_1635 == (short)_1635 && _1635 <= INT15 && _1635 >= -INT15)
        _pos_factor_3018 = _1635 * _1635;
        else
        _pos_factor_3018 = NewDouble(_1635 * (double)_1635);
    }
    else {
        if (IS_ATOM_INT(_1635)) {
            _pos_factor_3018 = NewDouble((double)_1635 * DBL_PTR(_1635)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1635)) {
                _pos_factor_3018 = NewDouble(DBL_PTR(_1635)->dbl * (double)_1635);
            }
            else
            _pos_factor_3018 = NewDouble(DBL_PTR(_1635)->dbl * DBL_PTR(_1635)->dbl);
        }
    }
    DeRef(_1635);
    _1635 = NOVALUE;
    _1635 = NOVALUE;

    /** 		indx_b = 1*/
    _indx_b_3020 = 1;

    /** 		while indx_b <= length(B) do*/
L3: 
    if (IS_SEQUENCE(_B_3016)){
            _1637 = SEQ_PTR(_B_3016)->length;
    }
    else {
        _1637 = 1;
    }
    if (_indx_b_3020 > _1637)
    goto L4; // [86] 231

    /** 			if equal(A[indx_a],B[indx_b]) then*/
    _2 = (int)SEQ_PTR(_A_3015);
    _1639 = (int)*(((s1_ptr)_2)->base + _indx_a_3019);
    _2 = (int)SEQ_PTR(_B_3016);
    _1640 = (int)*(((s1_ptr)_2)->base + _indx_b_3020);
    if (_1639 == _1640)
    _1641 = 1;
    else if (IS_ATOM_INT(_1639) && IS_ATOM_INT(_1640))
    _1641 = 0;
    else
    _1641 = (compare(_1639, _1640) == 0);
    _1639 = NOVALUE;
    _1640 = NOVALUE;
    if (_1641 == 0)
    {
        _1641 = NOVALUE;
        goto L5; // [104] 218
    }
    else{
        _1641 = NOVALUE;
    }

    /** 				accum_score += power((indx_b - indx_a) * pos_factor,2)*/
    _1642 = _indx_b_3020 - _indx_a_3019;
    if ((long)((unsigned long)_1642 +(unsigned long) HIGH_BITS) >= 0){
        _1642 = NewDouble((double)_1642);
    }
    if (IS_ATOM_INT(_1642) && IS_ATOM_INT(_pos_factor_3018)) {
        if (_1642 == (short)_1642 && _pos_factor_3018 <= INT15 && _pos_factor_3018 >= -INT15)
        _1643 = _1642 * _pos_factor_3018;
        else
        _1643 = NewDouble(_1642 * (double)_pos_factor_3018);
    }
    else {
        if (IS_ATOM_INT(_1642)) {
            _1643 = NewDouble((double)_1642 * DBL_PTR(_pos_factor_3018)->dbl);
        }
        else {
            if (IS_ATOM_INT(_pos_factor_3018)) {
                _1643 = NewDouble(DBL_PTR(_1642)->dbl * (double)_pos_factor_3018);
            }
            else
            _1643 = NewDouble(DBL_PTR(_1642)->dbl * DBL_PTR(_pos_factor_3018)->dbl);
        }
    }
    DeRef(_1642);
    _1642 = NOVALUE;
    if (IS_ATOM_INT(_1643) && IS_ATOM_INT(_1643)) {
        if (_1643 == (short)_1643 && _1643 <= INT15 && _1643 >= -INT15)
        _1644 = _1643 * _1643;
        else
        _1644 = NewDouble(_1643 * (double)_1643);
    }
    else {
        if (IS_ATOM_INT(_1643)) {
            _1644 = NewDouble((double)_1643 * DBL_PTR(_1643)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1643)) {
                _1644 = NewDouble(DBL_PTR(_1643)->dbl * (double)_1643);
            }
            else
            _1644 = NewDouble(DBL_PTR(_1643)->dbl * DBL_PTR(_1643)->dbl);
        }
    }
    DeRef(_1643);
    _1643 = NOVALUE;
    _1643 = NOVALUE;
    _0 = _accum_score_3017;
    if (IS_ATOM_INT(_accum_score_3017) && IS_ATOM_INT(_1644)) {
        _accum_score_3017 = _accum_score_3017 + _1644;
        if ((long)((unsigned long)_accum_score_3017 + (unsigned long)HIGH_BITS) >= 0) 
        _accum_score_3017 = NewDouble((double)_accum_score_3017);
    }
    else {
        if (IS_ATOM_INT(_accum_score_3017)) {
            _accum_score_3017 = NewDouble((double)_accum_score_3017 + DBL_PTR(_1644)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1644)) {
                _accum_score_3017 = NewDouble(DBL_PTR(_accum_score_3017)->dbl + (double)_1644);
            }
            else
            _accum_score_3017 = NewDouble(DBL_PTR(_accum_score_3017)->dbl + DBL_PTR(_1644)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1644);
    _1644 = NOVALUE;

    /** 				while indx_a <= length(A) and indx_b <= length(B) with entry do*/
    goto L6; // [127] 180
L7: 
    if (IS_SEQUENCE(_A_3015)){
            _1646 = SEQ_PTR(_A_3015)->length;
    }
    else {
        _1646 = 1;
    }
    _1647 = (_indx_a_3019 <= _1646);
    _1646 = NOVALUE;
    if (_1647 == 0) {
        DeRef(_1648);
        _1648 = 0;
        goto L8; // [137] 152
    }
    if (IS_SEQUENCE(_B_3016)){
            _1649 = SEQ_PTR(_B_3016)->length;
    }
    else {
        _1649 = 1;
    }
    _1650 = (_indx_b_3020 <= _1649);
    _1649 = NOVALUE;
    _1648 = (_1650 != 0);
L8: 
    if (_1648 == 0)
    {
        _1648 = NOVALUE;
        goto L1; // [152] 40
    }
    else{
        _1648 = NOVALUE;
    }

    /** 					if not equal(A[indx_a], B[indx_b]) then*/
    _2 = (int)SEQ_PTR(_A_3015);
    _1651 = (int)*(((s1_ptr)_2)->base + _indx_a_3019);
    _2 = (int)SEQ_PTR(_B_3016);
    _1652 = (int)*(((s1_ptr)_2)->base + _indx_b_3020);
    if (_1651 == _1652)
    _1653 = 1;
    else if (IS_ATOM_INT(_1651) && IS_ATOM_INT(_1652))
    _1653 = 0;
    else
    _1653 = (compare(_1651, _1652) == 0);
    _1651 = NOVALUE;
    _1652 = NOVALUE;
    if (_1653 != 0)
    goto L9; // [169] 177
    _1653 = NOVALUE;

    /** 						exit*/
    goto L1; // [174] 40
L9: 

    /** 				entry*/
L6: 

    /** 					used_B[indx_b] = 1*/
    _2 = (int)SEQ_PTR(_used_B_3022);
    _2 = (int)(((s1_ptr)_2)->base + _indx_b_3020);
    *(int *)_2 = 1;

    /** 					used_A[indx_a] = 1*/
    _2 = (int)SEQ_PTR(_used_A_3021);
    _2 = (int)(((s1_ptr)_2)->base + _indx_a_3019);
    *(int *)_2 = 1;

    /** 					indx_a += 1*/
    _indx_a_3019 = _indx_a_3019 + 1;

    /** 					indx_b += 1*/
    _indx_b_3020 = _indx_b_3020 + 1;

    /** 				end while*/
    goto L7; // [210] 130

    /** 				continue "DoA"*/
    goto L1; // [215] 40
L5: 

    /** 			indx_b += 1*/
    _indx_b_3020 = _indx_b_3020 + 1;

    /** 		end while*/
    goto L3; // [228] 83
L4: 

    /** 		indx_a += 1*/
    _indx_a_3019 = _indx_a_3019 + 1;

    /** 	end while*/
    goto L1; // [241] 40
L2: 

    /**  	for i = 1 to length(A) do*/
    if (IS_SEQUENCE(_A_3015)){
            _1659 = SEQ_PTR(_A_3015)->length;
    }
    else {
        _1659 = 1;
    }
    {
        int _i_3064;
        _i_3064 = 1;
LA: 
        if (_i_3064 > _1659){
            goto LB; // [249] 323
        }

        /**  		if used_A[i] = 0 then*/
        _2 = (int)SEQ_PTR(_used_A_3021);
        _1660 = (int)*(((s1_ptr)_2)->base + _i_3064);
        if (_1660 != 0)
        goto LC; // [262] 316

        /** 			pos_factor = power((1 + length(A) - i) / length(A),2)*/
        if (IS_SEQUENCE(_A_3015)){
                _1662 = SEQ_PTR(_A_3015)->length;
        }
        else {
            _1662 = 1;
        }
        _1663 = _1662 + 1;
        _1662 = NOVALUE;
        _1664 = _1663 - _i_3064;
        _1663 = NOVALUE;
        if (IS_SEQUENCE(_A_3015)){
                _1665 = SEQ_PTR(_A_3015)->length;
        }
        else {
            _1665 = 1;
        }
        _1666 = (_1664 % _1665) ? NewDouble((double)_1664 / _1665) : (_1664 / _1665);
        _1664 = NOVALUE;
        _1665 = NOVALUE;
        DeRef(_pos_factor_3018);
        if (IS_ATOM_INT(_1666) && IS_ATOM_INT(_1666)) {
            if (_1666 == (short)_1666 && _1666 <= INT15 && _1666 >= -INT15)
            _pos_factor_3018 = _1666 * _1666;
            else
            _pos_factor_3018 = NewDouble(_1666 * (double)_1666);
        }
        else {
            if (IS_ATOM_INT(_1666)) {
                _pos_factor_3018 = NewDouble((double)_1666 * DBL_PTR(_1666)->dbl);
            }
            else {
                if (IS_ATOM_INT(_1666)) {
                    _pos_factor_3018 = NewDouble(DBL_PTR(_1666)->dbl * (double)_1666);
                }
                else
                _pos_factor_3018 = NewDouble(DBL_PTR(_1666)->dbl * DBL_PTR(_1666)->dbl);
            }
        }
        DeRef(_1666);
        _1666 = NOVALUE;
        _1666 = NOVALUE;

        /**  			accum_score += power((length(A) - i + 1) * pos_factor,2)*/
        if (IS_SEQUENCE(_A_3015)){
                _1668 = SEQ_PTR(_A_3015)->length;
        }
        else {
            _1668 = 1;
        }
        _1669 = _1668 - _i_3064;
        _1668 = NOVALUE;
        _1670 = _1669 + 1;
        _1669 = NOVALUE;
        if (IS_ATOM_INT(_pos_factor_3018)) {
            if (_1670 == (short)_1670 && _pos_factor_3018 <= INT15 && _pos_factor_3018 >= -INT15)
            _1671 = _1670 * _pos_factor_3018;
            else
            _1671 = NewDouble(_1670 * (double)_pos_factor_3018);
        }
        else {
            _1671 = NewDouble((double)_1670 * DBL_PTR(_pos_factor_3018)->dbl);
        }
        _1670 = NOVALUE;
        if (IS_ATOM_INT(_1671) && IS_ATOM_INT(_1671)) {
            if (_1671 == (short)_1671 && _1671 <= INT15 && _1671 >= -INT15)
            _1672 = _1671 * _1671;
            else
            _1672 = NewDouble(_1671 * (double)_1671);
        }
        else {
            if (IS_ATOM_INT(_1671)) {
                _1672 = NewDouble((double)_1671 * DBL_PTR(_1671)->dbl);
            }
            else {
                if (IS_ATOM_INT(_1671)) {
                    _1672 = NewDouble(DBL_PTR(_1671)->dbl * (double)_1671);
                }
                else
                _1672 = NewDouble(DBL_PTR(_1671)->dbl * DBL_PTR(_1671)->dbl);
            }
        }
        DeRef(_1671);
        _1671 = NOVALUE;
        _1671 = NOVALUE;
        _0 = _accum_score_3017;
        if (IS_ATOM_INT(_accum_score_3017) && IS_ATOM_INT(_1672)) {
            _accum_score_3017 = _accum_score_3017 + _1672;
            if ((long)((unsigned long)_accum_score_3017 + (unsigned long)HIGH_BITS) >= 0) 
            _accum_score_3017 = NewDouble((double)_accum_score_3017);
        }
        else {
            if (IS_ATOM_INT(_accum_score_3017)) {
                _accum_score_3017 = NewDouble((double)_accum_score_3017 + DBL_PTR(_1672)->dbl);
            }
            else {
                if (IS_ATOM_INT(_1672)) {
                    _accum_score_3017 = NewDouble(DBL_PTR(_accum_score_3017)->dbl + (double)_1672);
                }
                else
                _accum_score_3017 = NewDouble(DBL_PTR(_accum_score_3017)->dbl + DBL_PTR(_1672)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_1672);
        _1672 = NOVALUE;
LC: 

        /**  	end for*/
        _i_3064 = _i_3064 + 1;
        goto LA; // [318] 256
LB: 
        ;
    }

    /**  	integer total_elems = length(A)*/
    if (IS_SEQUENCE(_A_3015)){
            _total_elems_3081 = SEQ_PTR(_A_3015)->length;
    }
    else {
        _total_elems_3081 = 1;
    }

    /**  	for i = 1 to length(B) do*/
    if (IS_SEQUENCE(_B_3016)){
            _1675 = SEQ_PTR(_B_3016)->length;
    }
    else {
        _1675 = 1;
    }
    {
        int _i_3084;
        _i_3084 = 1;
LD: 
        if (_i_3084 > _1675){
            goto LE; // [335] 413
        }

        /**  		if used_B[i] = 0 then*/
        _2 = (int)SEQ_PTR(_used_B_3022);
        _1676 = (int)*(((s1_ptr)_2)->base + _i_3084);
        if (_1676 != 0)
        goto LF; // [348] 406

        /** 			pos_factor = power((1 + length(B) - i) / length(B),2)*/
        if (IS_SEQUENCE(_B_3016)){
                _1678 = SEQ_PTR(_B_3016)->length;
        }
        else {
            _1678 = 1;
        }
        _1679 = _1678 + 1;
        _1678 = NOVALUE;
        _1680 = _1679 - _i_3084;
        _1679 = NOVALUE;
        if (IS_SEQUENCE(_B_3016)){
                _1681 = SEQ_PTR(_B_3016)->length;
        }
        else {
            _1681 = 1;
        }
        _1682 = (_1680 % _1681) ? NewDouble((double)_1680 / _1681) : (_1680 / _1681);
        _1680 = NOVALUE;
        _1681 = NOVALUE;
        DeRef(_pos_factor_3018);
        if (IS_ATOM_INT(_1682) && IS_ATOM_INT(_1682)) {
            if (_1682 == (short)_1682 && _1682 <= INT15 && _1682 >= -INT15)
            _pos_factor_3018 = _1682 * _1682;
            else
            _pos_factor_3018 = NewDouble(_1682 * (double)_1682);
        }
        else {
            if (IS_ATOM_INT(_1682)) {
                _pos_factor_3018 = NewDouble((double)_1682 * DBL_PTR(_1682)->dbl);
            }
            else {
                if (IS_ATOM_INT(_1682)) {
                    _pos_factor_3018 = NewDouble(DBL_PTR(_1682)->dbl * (double)_1682);
                }
                else
                _pos_factor_3018 = NewDouble(DBL_PTR(_1682)->dbl * DBL_PTR(_1682)->dbl);
            }
        }
        DeRef(_1682);
        _1682 = NOVALUE;
        _1682 = NOVALUE;

        /**  			accum_score += (length(B) - i + 1) * pos_factor*/
        if (IS_SEQUENCE(_B_3016)){
                _1684 = SEQ_PTR(_B_3016)->length;
        }
        else {
            _1684 = 1;
        }
        _1685 = _1684 - _i_3084;
        _1684 = NOVALUE;
        _1686 = _1685 + 1;
        _1685 = NOVALUE;
        if (IS_ATOM_INT(_pos_factor_3018)) {
            if (_1686 == (short)_1686 && _pos_factor_3018 <= INT15 && _pos_factor_3018 >= -INT15)
            _1687 = _1686 * _pos_factor_3018;
            else
            _1687 = NewDouble(_1686 * (double)_pos_factor_3018);
        }
        else {
            _1687 = NewDouble((double)_1686 * DBL_PTR(_pos_factor_3018)->dbl);
        }
        _1686 = NOVALUE;
        _0 = _accum_score_3017;
        if (IS_ATOM_INT(_accum_score_3017) && IS_ATOM_INT(_1687)) {
            _accum_score_3017 = _accum_score_3017 + _1687;
            if ((long)((unsigned long)_accum_score_3017 + (unsigned long)HIGH_BITS) >= 0) 
            _accum_score_3017 = NewDouble((double)_accum_score_3017);
        }
        else {
            if (IS_ATOM_INT(_accum_score_3017)) {
                _accum_score_3017 = NewDouble((double)_accum_score_3017 + DBL_PTR(_1687)->dbl);
            }
            else {
                if (IS_ATOM_INT(_1687)) {
                    _accum_score_3017 = NewDouble(DBL_PTR(_accum_score_3017)->dbl + (double)_1687);
                }
                else
                _accum_score_3017 = NewDouble(DBL_PTR(_accum_score_3017)->dbl + DBL_PTR(_1687)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_1687);
        _1687 = NOVALUE;

        /**  			total_elems += 1*/
        _total_elems_3081 = _total_elems_3081 + 1;
LF: 

        /**  	end for*/
        _i_3084 = _i_3084 + 1;
        goto LD; // [408] 342
LE: 
        ;
    }

    /** 	return power(accum_score / power(total_elems,2), 0.5)*/
    if (_total_elems_3081 == (short)_total_elems_3081 && _total_elems_3081 <= INT15 && _total_elems_3081 >= -INT15)
    _1690 = _total_elems_3081 * _total_elems_3081;
    else
    _1690 = NewDouble(_total_elems_3081 * (double)_total_elems_3081);
    if (IS_ATOM_INT(_accum_score_3017) && IS_ATOM_INT(_1690)) {
        _1691 = (_accum_score_3017 % _1690) ? NewDouble((double)_accum_score_3017 / _1690) : (_accum_score_3017 / _1690);
    }
    else {
        if (IS_ATOM_INT(_accum_score_3017)) {
            _1691 = NewDouble((double)_accum_score_3017 / DBL_PTR(_1690)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1690)) {
                _1691 = NewDouble(DBL_PTR(_accum_score_3017)->dbl / (double)_1690);
            }
            else
            _1691 = NewDouble(DBL_PTR(_accum_score_3017)->dbl / DBL_PTR(_1690)->dbl);
        }
    }
    DeRef(_1690);
    _1690 = NOVALUE;
    if (IS_ATOM_INT(_1691)) {
        temp_d.dbl = (double)_1691;
        _1693 = Dpower(&temp_d, DBL_PTR(_1692));
    }
    else {
        _1693 = Dpower(DBL_PTR(_1691), DBL_PTR(_1692));
    }
    DeRef(_1691);
    _1691 = NOVALUE;
    DeRefDS(_A_3015);
    DeRefDS(_B_3016);
    DeRef(_accum_score_3017);
    DeRef(_pos_factor_3018);
    DeRefi(_used_A_3021);
    DeRefi(_used_B_3022);
    DeRef(_1647);
    _1647 = NOVALUE;
    DeRef(_1650);
    _1650 = NOVALUE;
    _1660 = NOVALUE;
    _1676 = NOVALUE;
    return _1693;
    ;
}


int _3remove_subseq(int _source_list_3111, int _alt_value_3112)
{
    int _lResult_3113 = NOVALUE;
    int _lCOW_3114 = NOVALUE;
    int _1710 = NOVALUE;
    int _1709 = NOVALUE;
    int _1705 = NOVALUE;
    int _1702 = NOVALUE;
    int _1699 = NOVALUE;
    int _1698 = NOVALUE;
    int _1697 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lCOW = 0*/
    _lCOW_3114 = 0;

    /** 	for i = 1 to length(source_list) do*/
    if (IS_SEQUENCE(_source_list_3111)){
            _1697 = SEQ_PTR(_source_list_3111)->length;
    }
    else {
        _1697 = 1;
    }
    {
        int _i_3116;
        _i_3116 = 1;
L1: 
        if (_i_3116 > _1697){
            goto L2; // [15] 129
        }

        /** 		if atom(source_list[i]) then*/
        _2 = (int)SEQ_PTR(_source_list_3111);
        _1698 = (int)*(((s1_ptr)_2)->base + _i_3116);
        _1699 = IS_ATOM(_1698);
        _1698 = NOVALUE;
        if (_1699 == 0)
        {
            _1699 = NOVALUE;
            goto L3; // [31] 73
        }
        else{
            _1699 = NOVALUE;
        }

        /** 			if lCOW != 0 then*/
        if (_lCOW_3114 == 0)
        goto L4; // [36] 124

        /** 				if lCOW != i then*/
        if (_lCOW_3114 == _i_3116)
        goto L5; // [42] 59

        /** 					lResult[lCOW] = source_list[i]*/
        _2 = (int)SEQ_PTR(_source_list_3111);
        _1702 = (int)*(((s1_ptr)_2)->base + _i_3116);
        Ref(_1702);
        _2 = (int)SEQ_PTR(_lResult_3113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_3113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_3114);
        _1 = *(int *)_2;
        *(int *)_2 = _1702;
        if( _1 != _1702 ){
            DeRef(_1);
        }
        _1702 = NOVALUE;
L5: 

        /** 				lCOW += 1*/
        _lCOW_3114 = _lCOW_3114 + 1;

        /** 			continue*/
        goto L4; // [70] 124
L3: 

        /** 		if lCOW = 0 then*/
        if (_lCOW_3114 != 0)
        goto L6; // [75] 94

        /** 			lResult = source_list*/
        RefDS(_source_list_3111);
        DeRef(_lResult_3113);
        _lResult_3113 = _source_list_3111;

        /** 			lCOW = i*/
        _lCOW_3114 = _i_3116;
L6: 

        /** 		if not equal(alt_value, SEQ_NOALT) then*/
        if (_alt_value_3112 == _3SEQ_NOALT_3105)
        _1705 = 1;
        else if (IS_ATOM_INT(_alt_value_3112) && IS_ATOM_INT(_3SEQ_NOALT_3105))
        _1705 = 0;
        else
        _1705 = (compare(_alt_value_3112, _3SEQ_NOALT_3105) == 0);
        if (_1705 != 0)
        goto L7; // [102] 122
        _1705 = NOVALUE;

        /** 			lResult[lCOW] = alt_value*/
        Ref(_alt_value_3112);
        _2 = (int)SEQ_PTR(_lResult_3113);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_3113 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_3114);
        _1 = *(int *)_2;
        *(int *)_2 = _alt_value_3112;
        DeRef(_1);

        /** 			lCOW += 1*/
        _lCOW_3114 = _lCOW_3114 + 1;
L7: 

        /** 	end for*/
L4: 
        _i_3116 = _i_3116 + 1;
        goto L1; // [124] 22
L2: 
        ;
    }

    /** 	if lCOW = 0 then*/
    if (_lCOW_3114 != 0)
    goto L8; // [131] 142

    /** 		return source_list*/
    DeRef(_alt_value_3112);
    DeRef(_lResult_3113);
    return _source_list_3111;
L8: 

    /** 	return lResult[1.. lCOW - 1]*/
    _1709 = _lCOW_3114 - 1;
    rhs_slice_target = (object_ptr)&_1710;
    RHS_Slice(_lResult_3113, 1, _1709);
    DeRefDS(_source_list_3111);
    DeRef(_alt_value_3112);
    DeRefDS(_lResult_3113);
    _1709 = NOVALUE;
    return _1710;
    ;
}


int _3remove_dups(int _source_data_3144, int _proc_option_3145)
{
    int _lTo_3146 = NOVALUE;
    int _lFrom_3147 = NOVALUE;
    int _lResult_3170 = NOVALUE;
    int _1733 = NOVALUE;
    int _1731 = NOVALUE;
    int _1730 = NOVALUE;
    int _1729 = NOVALUE;
    int _1728 = NOVALUE;
    int _1726 = NOVALUE;
    int _1722 = NOVALUE;
    int _1721 = NOVALUE;
    int _1720 = NOVALUE;
    int _1718 = NOVALUE;
    int _1713 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_option_3145)) {
        _1 = (long)(DBL_PTR(_proc_option_3145)->dbl);
        if (UNIQUE(DBL_PTR(_proc_option_3145)) && (DBL_PTR(_proc_option_3145)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_option_3145);
        _proc_option_3145 = _1;
    }

    /** 	if length(source_data) < 2 then*/
    if (IS_SEQUENCE(_source_data_3144)){
            _1713 = SEQ_PTR(_source_data_3144)->length;
    }
    else {
        _1713 = 1;
    }
    if (_1713 >= 2)
    goto L1; // [12] 23

    /** 		return source_data*/
    DeRef(_lResult_3170);
    return _source_data_3144;
L1: 

    /** 	if proc_option = RD_SORT then*/
    if (_proc_option_3145 != 3)
    goto L2; // [27] 52

    /** 		source_data = stdsort:sort(source_data)*/
    RefDS(_source_data_3144);
    _0 = _source_data_3144;
    _source_data_3144 = _7sort(_source_data_3144, 1);
    DeRefDS(_0);

    /** 		proc_option = RD_PRESORTED*/
    _proc_option_3145 = 2;
L2: 

    /** 	if proc_option = RD_PRESORTED then*/
    if (_proc_option_3145 != 2)
    goto L3; // [56] 154

    /** 		lTo = 1*/
    _lTo_3146 = 1;

    /** 		lFrom = 2*/
    _lFrom_3147 = 2;

    /** 		while lFrom <= length(source_data) do*/
L4: 
    if (IS_SEQUENCE(_source_data_3144)){
            _1718 = SEQ_PTR(_source_data_3144)->length;
    }
    else {
        _1718 = 1;
    }
    if (_lFrom_3147 > _1718)
    goto L5; // [82] 142

    /** 			if not equal(source_data[lFrom], source_data[lTo]) then*/
    _2 = (int)SEQ_PTR(_source_data_3144);
    _1720 = (int)*(((s1_ptr)_2)->base + _lFrom_3147);
    _2 = (int)SEQ_PTR(_source_data_3144);
    _1721 = (int)*(((s1_ptr)_2)->base + _lTo_3146);
    if (_1720 == _1721)
    _1722 = 1;
    else if (IS_ATOM_INT(_1720) && IS_ATOM_INT(_1721))
    _1722 = 0;
    else
    _1722 = (compare(_1720, _1721) == 0);
    _1720 = NOVALUE;
    _1721 = NOVALUE;
    if (_1722 != 0)
    goto L6; // [100] 129
    _1722 = NOVALUE;

    /** 				lTo += 1*/
    _lTo_3146 = _lTo_3146 + 1;

    /** 				if lTo != lFrom then*/
    if (_lTo_3146 == _lFrom_3147)
    goto L7; // [113] 128

    /** 					source_data[lTo] = source_data[lFrom]*/
    _2 = (int)SEQ_PTR(_source_data_3144);
    _1726 = (int)*(((s1_ptr)_2)->base + _lFrom_3147);
    Ref(_1726);
    _2 = (int)SEQ_PTR(_source_data_3144);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _source_data_3144 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _lTo_3146);
    _1 = *(int *)_2;
    *(int *)_2 = _1726;
    if( _1 != _1726 ){
        DeRef(_1);
    }
    _1726 = NOVALUE;
L7: 
L6: 

    /** 			lFrom += 1*/
    _lFrom_3147 = _lFrom_3147 + 1;

    /** 		end while*/
    goto L4; // [139] 79
L5: 

    /** 		return source_data[1 .. lTo]*/
    rhs_slice_target = (object_ptr)&_1728;
    RHS_Slice(_source_data_3144, 1, _lTo_3146);
    DeRefDS(_source_data_3144);
    DeRef(_lResult_3170);
    return _1728;
L3: 

    /** 	sequence lResult*/

    /** 	lResult = {}*/
    RefDS(_5);
    DeRef(_lResult_3170);
    _lResult_3170 = _5;

    /** 	for i = 1 to length(source_data) do*/
    if (IS_SEQUENCE(_source_data_3144)){
            _1729 = SEQ_PTR(_source_data_3144)->length;
    }
    else {
        _1729 = 1;
    }
    {
        int _i_3172;
        _i_3172 = 1;
L8: 
        if (_i_3172 > _1729){
            goto L9; // [168] 207
        }

        /** 		if not find(source_data[i], lResult) then*/
        _2 = (int)SEQ_PTR(_source_data_3144);
        _1730 = (int)*(((s1_ptr)_2)->base + _i_3172);
        _1731 = find_from(_1730, _lResult_3170, 1);
        _1730 = NOVALUE;
        if (_1731 != 0)
        goto LA; // [186] 200
        _1731 = NOVALUE;

        /** 			lResult = append(lResult, source_data[i])*/
        _2 = (int)SEQ_PTR(_source_data_3144);
        _1733 = (int)*(((s1_ptr)_2)->base + _i_3172);
        Ref(_1733);
        Append(&_lResult_3170, _lResult_3170, _1733);
        _1733 = NOVALUE;
LA: 

        /** 	end for*/
        _i_3172 = _i_3172 + 1;
        goto L8; // [202] 175
L9: 
        ;
    }

    /** 	return lResult*/
    DeRefDS(_source_data_3144);
    DeRef(_1728);
    _1728 = NOVALUE;
    return _lResult_3170;
    ;
}


int _3combine(int _source_data_3185, int _proc_option_3186)
{
    int _lResult_3187 = NOVALUE;
    int _lTotalSize_3188 = NOVALUE;
    int _lPos_3189 = NOVALUE;
    int _1756 = NOVALUE;
    int _1753 = NOVALUE;
    int _1752 = NOVALUE;
    int _1751 = NOVALUE;
    int _1750 = NOVALUE;
    int _1749 = NOVALUE;
    int _1748 = NOVALUE;
    int _1747 = NOVALUE;
    int _1746 = NOVALUE;
    int _1743 = NOVALUE;
    int _1742 = NOVALUE;
    int _1741 = NOVALUE;
    int _1740 = NOVALUE;
    int _1738 = NOVALUE;
    int _1736 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_option_3186)) {
        _1 = (long)(DBL_PTR(_proc_option_3186)->dbl);
        if (UNIQUE(DBL_PTR(_proc_option_3186)) && (DBL_PTR(_proc_option_3186)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_option_3186);
        _proc_option_3186 = _1;
    }

    /** 	integer lTotalSize = 0*/
    _lTotalSize_3188 = 0;

    /** 	if length(source_data) = 0 then*/
    if (IS_SEQUENCE(_source_data_3185)){
            _1736 = SEQ_PTR(_source_data_3185)->length;
    }
    else {
        _1736 = 1;
    }
    if (_1736 != 0)
    goto L1; // [19] 30

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_source_data_3185);
    DeRef(_lResult_3187);
    return _5;
L1: 

    /** 	if length(source_data) = 1 then*/
    if (IS_SEQUENCE(_source_data_3185)){
            _1738 = SEQ_PTR(_source_data_3185)->length;
    }
    else {
        _1738 = 1;
    }
    if (_1738 != 1)
    goto L2; // [35] 50

    /** 		return source_data[1]*/
    _2 = (int)SEQ_PTR(_source_data_3185);
    _1740 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_1740);
    DeRefDS(_source_data_3185);
    DeRef(_lResult_3187);
    return _1740;
L2: 

    /** 	for i = 1 to length(source_data) do*/
    if (IS_SEQUENCE(_source_data_3185)){
            _1741 = SEQ_PTR(_source_data_3185)->length;
    }
    else {
        _1741 = 1;
    }
    {
        int _i_3198;
        _i_3198 = 1;
L3: 
        if (_i_3198 > _1741){
            goto L4; // [55] 84
        }

        /** 		lTotalSize += length(source_data[i])*/
        _2 = (int)SEQ_PTR(_source_data_3185);
        _1742 = (int)*(((s1_ptr)_2)->base + _i_3198);
        if (IS_SEQUENCE(_1742)){
                _1743 = SEQ_PTR(_1742)->length;
        }
        else {
            _1743 = 1;
        }
        _1742 = NOVALUE;
        _lTotalSize_3188 = _lTotalSize_3188 + _1743;
        _1743 = NOVALUE;

        /** 	end for*/
        _i_3198 = _i_3198 + 1;
        goto L3; // [79] 62
L4: 
        ;
    }

    /** 	lResult = repeat(0, lTotalSize)*/
    DeRef(_lResult_3187);
    _lResult_3187 = Repeat(0, _lTotalSize_3188);

    /** 	lPos = 1*/
    _lPos_3189 = 1;

    /** 	for i = 1 to length(source_data) do*/
    if (IS_SEQUENCE(_source_data_3185)){
            _1746 = SEQ_PTR(_source_data_3185)->length;
    }
    else {
        _1746 = 1;
    }
    {
        int _i_3205;
        _i_3205 = 1;
L5: 
        if (_i_3205 > _1746){
            goto L6; // [102] 157
        }

        /** 		lResult[lPos .. length(source_data[i]) + lPos - 1] = source_data[i]*/
        _2 = (int)SEQ_PTR(_source_data_3185);
        _1747 = (int)*(((s1_ptr)_2)->base + _i_3205);
        if (IS_SEQUENCE(_1747)){
                _1748 = SEQ_PTR(_1747)->length;
        }
        else {
            _1748 = 1;
        }
        _1747 = NOVALUE;
        _1749 = _1748 + _lPos_3189;
        if ((long)((unsigned long)_1749 + (unsigned long)HIGH_BITS) >= 0) 
        _1749 = NewDouble((double)_1749);
        _1748 = NOVALUE;
        if (IS_ATOM_INT(_1749)) {
            _1750 = _1749 - 1;
            if ((long)((unsigned long)_1750 +(unsigned long) HIGH_BITS) >= 0){
                _1750 = NewDouble((double)_1750);
            }
        }
        else {
            _1750 = NewDouble(DBL_PTR(_1749)->dbl - (double)1);
        }
        DeRef(_1749);
        _1749 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_data_3185);
        _1751 = (int)*(((s1_ptr)_2)->base + _i_3205);
        assign_slice_seq = (s1_ptr *)&_lResult_3187;
        AssignSlice(_lPos_3189, _1750, _1751);
        DeRef(_1750);
        _1750 = NOVALUE;
        _1751 = NOVALUE;

        /** 		lPos += length(source_data[i])*/
        _2 = (int)SEQ_PTR(_source_data_3185);
        _1752 = (int)*(((s1_ptr)_2)->base + _i_3205);
        if (IS_SEQUENCE(_1752)){
                _1753 = SEQ_PTR(_1752)->length;
        }
        else {
            _1753 = 1;
        }
        _1752 = NOVALUE;
        _lPos_3189 = _lPos_3189 + _1753;
        _1753 = NOVALUE;

        /** 	end for*/
        _i_3205 = _i_3205 + 1;
        goto L5; // [152] 109
L6: 
        ;
    }

    /** 	if proc_option = COMBINE_SORTED then*/
    if (_proc_option_3186 != 1)
    goto L7; // [161] 179

    /** 		return stdsort:sort(lResult)*/
    RefDS(_lResult_3187);
    _1756 = _7sort(_lResult_3187, 1);
    DeRefDS(_source_data_3185);
    DeRefDS(_lResult_3187);
    _1740 = NOVALUE;
    _1742 = NOVALUE;
    _1747 = NOVALUE;
    _1752 = NOVALUE;
    return _1756;
    goto L8; // [176] 186
L7: 

    /** 		return lResult*/
    DeRefDS(_source_data_3185);
    _1740 = NOVALUE;
    _1742 = NOVALUE;
    _1747 = NOVALUE;
    _1752 = NOVALUE;
    DeRef(_1756);
    _1756 = NOVALUE;
    return _lResult_3187;
L8: 
    ;
}


int _3minsize(int _source_data_3221, int _min_size_3222, int _new_data_3227)
{
    int _1765 = NOVALUE;
    int _1764 = NOVALUE;
    int _1763 = NOVALUE;
    int _1761 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_min_size_3222)) {
        _1 = (long)(DBL_PTR(_min_size_3222)->dbl);
        if (UNIQUE(DBL_PTR(_min_size_3222)) && (DBL_PTR(_min_size_3222)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_min_size_3222);
        _min_size_3222 = _1;
    }

    /**     if length(source_data) < min_size then*/
    if (IS_SEQUENCE(_source_data_3221)){
            _1761 = SEQ_PTR(_source_data_3221)->length;
    }
    else {
        _1761 = 1;
    }
    if (_1761 >= _min_size_3222)
    goto L1; // [10] 32

    /**         source_data &= repeat(new_data, min_size - length(source_data))*/
    if (IS_SEQUENCE(_source_data_3221)){
            _1763 = SEQ_PTR(_source_data_3221)->length;
    }
    else {
        _1763 = 1;
    }
    _1764 = _min_size_3222 - _1763;
    _1763 = NOVALUE;
    _1765 = Repeat(_new_data_3227, _1764);
    _1764 = NOVALUE;
    if (IS_SEQUENCE(_source_data_3221) && IS_ATOM(_1765)) {
    }
    else if (IS_ATOM(_source_data_3221) && IS_SEQUENCE(_1765)) {
        Ref(_source_data_3221);
        Prepend(&_source_data_3221, _1765, _source_data_3221);
    }
    else {
        Concat((object_ptr)&_source_data_3221, _source_data_3221, _1765);
    }
    DeRefDS(_1765);
    _1765 = NOVALUE;
L1: 

    /**     return source_data*/
    DeRef(_new_data_3227);
    return _source_data_3221;
    ;
}



// 0xD24837F4
