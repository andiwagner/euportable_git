// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _27calc_primes(int _approx_limit_11103, int _time_limit_p_11104)
{
    int _result__11105 = NOVALUE;
    int _candidate__11106 = NOVALUE;
    int _pos__11107 = NOVALUE;
    int _time_out__11108 = NOVALUE;
    int _maxp__11109 = NOVALUE;
    int _maxf__11110 = NOVALUE;
    int _maxf_idx_11111 = NOVALUE;
    int _next_trigger_11112 = NOVALUE;
    int _growth_11113 = NOVALUE;
    int _6344 = NOVALUE;
    int _6341 = NOVALUE;
    int _6339 = NOVALUE;
    int _6336 = NOVALUE;
    int _6333 = NOVALUE;
    int _6330 = NOVALUE;
    int _6323 = NOVALUE;
    int _6321 = NOVALUE;
    int _6318 = NOVALUE;
    int _6315 = NOVALUE;
    int _6311 = NOVALUE;
    int _6310 = NOVALUE;
    int _6306 = NOVALUE;
    int _6300 = NOVALUE;
    int _6298 = NOVALUE;
    int _6296 = NOVALUE;
    int _6291 = NOVALUE;
    int _6290 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if approx_limit <= list_of_primes[$] then*/
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _6290 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _6290 = 1;
    }
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _6291 = (int)*(((s1_ptr)_2)->base + _6290);
    if (binary_op_a(GREATER, _approx_limit_11103, _6291)){
        _6291 = NOVALUE;
        goto L1; // [16] 65
    }
    _6291 = NOVALUE;

    /** 		pos_ = search:binary_search(approx_limit, list_of_primes)*/
    RefDS(_27list_of_primes_11099);
    _pos__11107 = _6binary_search(_approx_limit_11103, _27list_of_primes_11099, 1, 0);
    if (!IS_ATOM_INT(_pos__11107)) {
        _1 = (long)(DBL_PTR(_pos__11107)->dbl);
        if (UNIQUE(DBL_PTR(_pos__11107)) && (DBL_PTR(_pos__11107)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos__11107);
        _pos__11107 = _1;
    }

    /** 		if pos_ < 0 then*/
    if (_pos__11107 >= 0)
    goto L2; // [37] 51

    /** 			pos_ = (-pos_)*/
    _pos__11107 = - _pos__11107;
L2: 

    /** 		return list_of_primes[1..pos_]*/
    rhs_slice_target = (object_ptr)&_6296;
    RHS_Slice(_27list_of_primes_11099, 1, _pos__11107);
    DeRef(_result__11105);
    DeRef(_time_out__11108);
    return _6296;
L1: 

    /** 	pos_ = length(list_of_primes)*/
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _pos__11107 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _pos__11107 = 1;
    }

    /** 	candidate_ = list_of_primes[$]*/
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _6298 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _6298 = 1;
    }
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _candidate__11106 = (int)*(((s1_ptr)_2)->base + _6298);
    if (!IS_ATOM_INT(_candidate__11106))
    _candidate__11106 = (long)DBL_PTR(_candidate__11106)->dbl;

    /** 	maxf_ = floor(power(candidate_, 0.5))*/
    temp_d.dbl = (double)_candidate__11106;
    _6300 = Dpower(&temp_d, DBL_PTR(_1692));
    _maxf__11110 = unary_op(FLOOR, _6300);
    DeRefDS(_6300);
    _6300 = NOVALUE;
    if (!IS_ATOM_INT(_maxf__11110)) {
        _1 = (long)(DBL_PTR(_maxf__11110)->dbl);
        if (UNIQUE(DBL_PTR(_maxf__11110)) && (DBL_PTR(_maxf__11110)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf__11110);
        _maxf__11110 = _1;
    }

    /** 	maxf_idx = search:binary_search(maxf_, list_of_primes)*/
    RefDS(_27list_of_primes_11099);
    _maxf_idx_11111 = _6binary_search(_maxf__11110, _27list_of_primes_11099, 1, 0);
    if (!IS_ATOM_INT(_maxf_idx_11111)) {
        _1 = (long)(DBL_PTR(_maxf_idx_11111)->dbl);
        if (UNIQUE(DBL_PTR(_maxf_idx_11111)) && (DBL_PTR(_maxf_idx_11111)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf_idx_11111);
        _maxf_idx_11111 = _1;
    }

    /** 	if maxf_idx < 0 then*/
    if (_maxf_idx_11111 >= 0)
    goto L3; // [117] 141

    /** 		maxf_idx = (-maxf_idx)*/
    _maxf_idx_11111 = - _maxf_idx_11111;

    /** 		maxf_ = list_of_primes[maxf_idx]*/
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _maxf__11110 = (int)*(((s1_ptr)_2)->base + _maxf_idx_11111);
    if (!IS_ATOM_INT(_maxf__11110))
    _maxf__11110 = (long)DBL_PTR(_maxf__11110)->dbl;
L3: 

    /** 	next_trigger = list_of_primes[maxf_idx+1]*/
    _6306 = _maxf_idx_11111 + 1;
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _next_trigger_11112 = (int)*(((s1_ptr)_2)->base + _6306);
    if (!IS_ATOM_INT(_next_trigger_11112))
    _next_trigger_11112 = (long)DBL_PTR(_next_trigger_11112)->dbl;

    /** 	next_trigger *= next_trigger*/
    _next_trigger_11112 = _next_trigger_11112 * _next_trigger_11112;

    /** 	growth = floor(approx_limit  / 3.5) - length(list_of_primes)*/
    _2 = binary_op(DIVIDE, _approx_limit_11103, _6309);
    _6310 = unary_op(FLOOR, _2);
    DeRef(_2);
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _6311 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _6311 = 1;
    }
    if (IS_ATOM_INT(_6310)) {
        _growth_11113 = _6310 - _6311;
    }
    else {
        _growth_11113 = NewDouble(DBL_PTR(_6310)->dbl - (double)_6311);
    }
    DeRef(_6310);
    _6310 = NOVALUE;
    _6311 = NOVALUE;
    if (!IS_ATOM_INT(_growth_11113)) {
        _1 = (long)(DBL_PTR(_growth_11113)->dbl);
        if (UNIQUE(DBL_PTR(_growth_11113)) && (DBL_PTR(_growth_11113)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_growth_11113);
        _growth_11113 = _1;
    }

    /** 	if growth <= 0 then*/
    if (_growth_11113 > 0)
    goto L4; // [186] 200

    /** 		growth = length(list_of_primes)*/
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _growth_11113 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _growth_11113 = 1;
    }
L4: 

    /** 	result_ = list_of_primes & repeat(0, growth)*/
    _6315 = Repeat(0, _growth_11113);
    Concat((object_ptr)&_result__11105, _27list_of_primes_11099, _6315);
    DeRefDS(_6315);
    _6315 = NOVALUE;

    /** 	if time_limit_p < 0 then*/
    if (_time_limit_p_11104 >= 0)
    goto L5; // [214] 229

    /** 		time_out_ = time() + 100_000_000*/
    DeRef(_6318);
    _6318 = NewDouble(current_time());
    DeRef(_time_out__11108);
    _time_out__11108 = NewDouble(DBL_PTR(_6318)->dbl + (double)100000000);
    DeRefDS(_6318);
    _6318 = NOVALUE;
    goto L6; // [226] 238
L5: 

    /** 		time_out_ = time() + time_limit_p*/
    DeRef(_6321);
    _6321 = NewDouble(current_time());
    DeRef(_time_out__11108);
    _time_out__11108 = NewDouble(DBL_PTR(_6321)->dbl + (double)_time_limit_p_11104);
    DeRefDS(_6321);
    _6321 = NOVALUE;
L6: 

    /** 	while time_out_ >= time()  label "MW" do*/
L7: 
    DeRef(_6323);
    _6323 = NewDouble(current_time());
    if (binary_op_a(LESS, _time_out__11108, _6323)){
        DeRefDS(_6323);
        _6323 = NOVALUE;
        goto L8; // [247] 410
    }
    DeRef(_6323);
    _6323 = NOVALUE;

    /** 		task_yield()*/
    task_yield();

    /** 		candidate_ += 2*/
    _candidate__11106 = _candidate__11106 + 2;

    /** 		if candidate_ >= next_trigger then*/
    if (_candidate__11106 < _next_trigger_11112)
    goto L9; // [264] 307

    /** 			maxf_idx += 1*/
    _maxf_idx_11111 = _maxf_idx_11111 + 1;

    /** 			maxf_ = result_[maxf_idx]*/
    _2 = (int)SEQ_PTR(_result__11105);
    _maxf__11110 = (int)*(((s1_ptr)_2)->base + _maxf_idx_11111);
    if (!IS_ATOM_INT(_maxf__11110))
    _maxf__11110 = (long)DBL_PTR(_maxf__11110)->dbl;

    /** 			next_trigger = result_[maxf_idx+1]*/
    _6330 = _maxf_idx_11111 + 1;
    _2 = (int)SEQ_PTR(_result__11105);
    _next_trigger_11112 = (int)*(((s1_ptr)_2)->base + _6330);
    if (!IS_ATOM_INT(_next_trigger_11112))
    _next_trigger_11112 = (long)DBL_PTR(_next_trigger_11112)->dbl;

    /** 			next_trigger *= next_trigger*/
    _next_trigger_11112 = _next_trigger_11112 * _next_trigger_11112;
L9: 

    /** 		for i = 2 to pos_ do*/
    _6333 = _pos__11107;
    {
        int _i_11166;
        _i_11166 = 2;
LA: 
        if (_i_11166 > _6333){
            goto LB; // [312] 360
        }

        /** 			maxp_ = result_[i]*/
        _2 = (int)SEQ_PTR(_result__11105);
        _maxp__11109 = (int)*(((s1_ptr)_2)->base + _i_11166);
        if (!IS_ATOM_INT(_maxp__11109))
        _maxp__11109 = (long)DBL_PTR(_maxp__11109)->dbl;

        /** 			if maxp_ > maxf_ then*/
        if (_maxp__11109 <= _maxf__11110)
        goto LC; // [329] 338

        /** 				exit*/
        goto LB; // [335] 360
LC: 

        /** 			if remainder(candidate_, maxp_) = 0 then*/
        _6336 = (_candidate__11106 % _maxp__11109);
        if (_6336 != 0)
        goto LD; // [344] 353

        /** 				continue "MW"*/
        goto L7; // [350] 243
LD: 

        /** 		end for*/
        _i_11166 = _i_11166 + 1;
        goto LA; // [355] 319
LB: 
        ;
    }

    /** 		pos_ += 1*/
    _pos__11107 = _pos__11107 + 1;

    /** 		if pos_ >= length(result_) then*/
    if (IS_SEQUENCE(_result__11105)){
            _6339 = SEQ_PTR(_result__11105)->length;
    }
    else {
        _6339 = 1;
    }
    if (_pos__11107 < _6339)
    goto LE; // [373] 388

    /** 			result_ &= repeat(0, 1000)*/
    _6341 = Repeat(0, 1000);
    Concat((object_ptr)&_result__11105, _result__11105, _6341);
    DeRefDS(_6341);
    _6341 = NOVALUE;
LE: 

    /** 		result_[pos_] = candidate_*/
    _2 = (int)SEQ_PTR(_result__11105);
    _2 = (int)(((s1_ptr)_2)->base + _pos__11107);
    _1 = *(int *)_2;
    *(int *)_2 = _candidate__11106;
    DeRef(_1);

    /** 		if candidate_ >= approx_limit then*/
    if (_candidate__11106 < _approx_limit_11103)
    goto L7; // [396] 243

    /** 			exit*/
    goto L8; // [402] 410

    /** 	end while*/
    goto L7; // [407] 243
L8: 

    /** 	return result_[1..pos_]*/
    rhs_slice_target = (object_ptr)&_6344;
    RHS_Slice(_result__11105, 1, _pos__11107);
    DeRefDS(_result__11105);
    DeRef(_time_out__11108);
    DeRef(_6296);
    _6296 = NOVALUE;
    DeRef(_6306);
    _6306 = NOVALUE;
    DeRef(_6330);
    _6330 = NOVALUE;
    DeRef(_6336);
    _6336 = NOVALUE;
    return _6344;
    ;
}


int _27next_prime(int _n_11185, int _fail_signal_p_11186, int _time_out_p_11187)
{
    int _i_11188 = NOVALUE;
    int _6364 = NOVALUE;
    int _6358 = NOVALUE;
    int _6357 = NOVALUE;
    int _6356 = NOVALUE;
    int _6355 = NOVALUE;
    int _6354 = NOVALUE;
    int _6351 = NOVALUE;
    int _6350 = NOVALUE;
    int _6347 = NOVALUE;
    int _6346 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if n < 0 then*/
    if (_n_11185 >= 0)
    goto L1; // [7] 18

    /** 		return fail_signal_p*/
    return _fail_signal_p_11186;
L1: 

    /** 	if list_of_primes[$] < n then*/
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _6346 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _6346 = 1;
    }
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _6347 = (int)*(((s1_ptr)_2)->base + _6346);
    if (binary_op_a(GREATEREQ, _6347, _n_11185)){
        _6347 = NOVALUE;
        goto L2; // [29] 43
    }
    _6347 = NOVALUE;

    /** 		list_of_primes = calc_primes(n,time_out_p)*/
    _0 = _27calc_primes(_n_11185, _time_out_p_11187);
    DeRefDS(_27list_of_primes_11099);
    _27list_of_primes_11099 = _0;
L2: 

    /** 	if n > list_of_primes[$] then*/
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _6350 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _6350 = 1;
    }
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _6351 = (int)*(((s1_ptr)_2)->base + _6350);
    if (binary_op_a(LESSEQ, _n_11185, _6351)){
        _6351 = NOVALUE;
        goto L3; // [54] 65
    }
    _6351 = NOVALUE;

    /** 		return fail_signal_p*/
    return _fail_signal_p_11186;
L3: 

    /** 	if n < 1009 and 1009 <= list_of_primes[$] then*/
    _6354 = (_n_11185 < 1009);
    if (_6354 == 0) {
        goto L4; // [71] 110
    }
    if (IS_SEQUENCE(_27list_of_primes_11099)){
            _6356 = SEQ_PTR(_27list_of_primes_11099)->length;
    }
    else {
        _6356 = 1;
    }
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _6357 = (int)*(((s1_ptr)_2)->base + _6356);
    if (IS_ATOM_INT(_6357)) {
        _6358 = (1009 <= _6357);
    }
    else {
        _6358 = binary_op(LESSEQ, 1009, _6357);
    }
    _6357 = NOVALUE;
    if (_6358 == 0) {
        DeRef(_6358);
        _6358 = NOVALUE;
        goto L4; // [89] 110
    }
    else {
        if (!IS_ATOM_INT(_6358) && DBL_PTR(_6358)->dbl == 0.0){
            DeRef(_6358);
            _6358 = NOVALUE;
            goto L4; // [89] 110
        }
        DeRef(_6358);
        _6358 = NOVALUE;
    }
    DeRef(_6358);
    _6358 = NOVALUE;

    /** 		i = search:binary_search(n, list_of_primes, ,169)*/
    RefDS(_27list_of_primes_11099);
    _i_11188 = _6binary_search(_n_11185, _27list_of_primes_11099, 1, 169);
    if (!IS_ATOM_INT(_i_11188)) {
        _1 = (long)(DBL_PTR(_i_11188)->dbl);
        if (UNIQUE(DBL_PTR(_i_11188)) && (DBL_PTR(_i_11188)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_11188);
        _i_11188 = _1;
    }
    goto L5; // [107] 126
L4: 

    /** 		i = search:binary_search(n, list_of_primes)*/
    RefDS(_27list_of_primes_11099);
    _i_11188 = _6binary_search(_n_11185, _27list_of_primes_11099, 1, 0);
    if (!IS_ATOM_INT(_i_11188)) {
        _1 = (long)(DBL_PTR(_i_11188)->dbl);
        if (UNIQUE(DBL_PTR(_i_11188)) && (DBL_PTR(_i_11188)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_11188);
        _i_11188 = _1;
    }
L5: 

    /** 	if i < 0 then*/
    if (_i_11188 >= 0)
    goto L6; // [130] 144

    /** 		i = (-i)*/
    _i_11188 = - _i_11188;
L6: 

    /** 	return list_of_primes[i]*/
    _2 = (int)SEQ_PTR(_27list_of_primes_11099);
    _6364 = (int)*(((s1_ptr)_2)->base + _i_11188);
    Ref(_6364);
    DeRef(_fail_signal_p_11186);
    DeRef(_6354);
    _6354 = NOVALUE;
    return _6364;
    ;
}



// 0x98742BA5
