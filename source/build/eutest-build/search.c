// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _6find_any(int _needles_860, int _haystack_861, int _start_862)
{
    int _408 = NOVALUE;
    int _407 = NOVALUE;
    int _406 = NOVALUE;
    int _404 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_862)) {
        _1 = (long)(DBL_PTR(_start_862)->dbl);
        if (UNIQUE(DBL_PTR(_start_862)) && (DBL_PTR(_start_862)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_862);
        _start_862 = _1;
    }

    /** 	if atom(needles) then*/
    _404 = IS_ATOM(_needles_860);
    if (_404 == 0)
    {
        _404 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _404 = NOVALUE;
    }

    /** 		needles = {needles}*/
    _0 = _needles_860;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needles_860);
    *((int *)(_2+4)) = _needles_860;
    _needles_860 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = start to length(haystack) do*/
    if (IS_SEQUENCE(_haystack_861)){
            _406 = SEQ_PTR(_haystack_861)->length;
    }
    else {
        _406 = 1;
    }
    {
        int _i_867;
        _i_867 = _start_862;
L2: 
        if (_i_867 > _406){
            goto L3; // [27] 62
        }

        /** 		if find(haystack[i],needles) then*/
        _2 = (int)SEQ_PTR(_haystack_861);
        _407 = (int)*(((s1_ptr)_2)->base + _i_867);
        _408 = find_from(_407, _needles_860, 1);
        _407 = NOVALUE;
        if (_408 == 0)
        {
            _408 = NOVALUE;
            goto L4; // [45] 55
        }
        else{
            _408 = NOVALUE;
        }

        /** 			return i*/
        DeRef(_needles_860);
        DeRefDS(_haystack_861);
        return _i_867;
L4: 

        /** 	end for*/
        _i_867 = _i_867 + 1;
        goto L2; // [57] 34
L3: 
        ;
    }

    /** 	return 0*/
    DeRef(_needles_860);
    DeRefDS(_haystack_861);
    return 0;
    ;
}


int _6match_any(int _needles_874, int _haystack_875, int _start_876)
{
    int _411 = NOVALUE;
    int _410 = NOVALUE;
    int _409 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_876)) {
        _1 = (long)(DBL_PTR(_start_876)->dbl);
        if (UNIQUE(DBL_PTR(_start_876)) && (DBL_PTR(_start_876)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_876);
        _start_876 = _1;
    }

    /** 	for i = start to length(haystack) do*/
    if (IS_SEQUENCE(_haystack_875)){
            _409 = SEQ_PTR(_haystack_875)->length;
    }
    else {
        _409 = 1;
    }
    {
        int _i_878;
        _i_878 = _start_876;
L1: 
        if (_i_878 > _409){
            goto L2; // [14] 49
        }

        /** 		if find(haystack[i],needles) then*/
        _2 = (int)SEQ_PTR(_haystack_875);
        _410 = (int)*(((s1_ptr)_2)->base + _i_878);
        _411 = find_from(_410, _needles_874, 1);
        _410 = NOVALUE;
        if (_411 == 0)
        {
            _411 = NOVALUE;
            goto L3; // [32] 42
        }
        else{
            _411 = NOVALUE;
        }

        /** 			return 1*/
        DeRefDS(_needles_874);
        DeRefDS(_haystack_875);
        return 1;
L3: 

        /** 	end for*/
        _i_878 = _i_878 + 1;
        goto L1; // [44] 21
L2: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_needles_874);
    DeRefDS(_haystack_875);
    return 0;
    ;
}


int _6find_each(int _needles_885, int _haystack_886, int _start_887)
{
    int _kx_888 = NOVALUE;
    int _417 = NOVALUE;
    int _416 = NOVALUE;
    int _414 = NOVALUE;
    int _413 = NOVALUE;
    int _412 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_887)) {
        _1 = (long)(DBL_PTR(_start_887)->dbl);
        if (UNIQUE(DBL_PTR(_start_887)) && (DBL_PTR(_start_887)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_887);
        _start_887 = _1;
    }

    /** 	integer kx = 0*/
    _kx_888 = 0;

    /** 	for i = start to length(haystack) do*/
    if (IS_SEQUENCE(_haystack_886)){
            _412 = SEQ_PTR(_haystack_886)->length;
    }
    else {
        _412 = 1;
    }
    {
        int _i_890;
        _i_890 = _start_887;
L1: 
        if (_i_890 > _412){
            goto L2; // [21] 64
        }

        /** 		if find(haystack[i],needles) then*/
        _2 = (int)SEQ_PTR(_haystack_886);
        _413 = (int)*(((s1_ptr)_2)->base + _i_890);
        _414 = find_from(_413, _needles_885, 1);
        _413 = NOVALUE;
        if (_414 == 0)
        {
            _414 = NOVALUE;
            goto L3; // [39] 57
        }
        else{
            _414 = NOVALUE;
        }

        /** 			kx += 1*/
        _kx_888 = _kx_888 + 1;

        /** 			haystack[kx] = i*/
        _2 = (int)SEQ_PTR(_haystack_886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _haystack_886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _kx_888);
        _1 = *(int *)_2;
        *(int *)_2 = _i_890;
        DeRef(_1);
L3: 

        /** 	end for*/
        _i_890 = _i_890 + 1;
        goto L1; // [59] 28
L2: 
        ;
    }

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _416 = _kx_888 + 1;
    if (_416 > MAXINT){
        _416 = NewDouble((double)_416);
    }
    if (IS_SEQUENCE(_haystack_886)){
            _417 = SEQ_PTR(_haystack_886)->length;
    }
    else {
        _417 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_886);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_416)) ? _416 : (long)(DBL_PTR(_416)->dbl);
        int stop = (IS_ATOM_INT(_417)) ? _417 : (long)(DBL_PTR(_417)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_886), start, &_haystack_886 );
            }
            else Tail(SEQ_PTR(_haystack_886), stop+1, &_haystack_886);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_886), start, &_haystack_886);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_886 = Remove_elements(start, stop, (SEQ_PTR(_haystack_886)->ref == 1));
        }
    }
    DeRef(_416);
    _416 = NOVALUE;
    _417 = NOVALUE;

    /** 	return haystack*/
    DeRefDS(_needles_885);
    return _haystack_886;
    ;
}


int _6find_all(int _needle_901, int _haystack_902, int _start_903)
{
    int _kx_904 = NOVALUE;
    int _423 = NOVALUE;
    int _422 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_903)) {
        _1 = (long)(DBL_PTR(_start_903)->dbl);
        if (UNIQUE(DBL_PTR(_start_903)) && (DBL_PTR(_start_903)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_903);
        _start_903 = _1;
    }

    /** 	integer kx = 0*/
    _kx_904 = 0;

    /** 	while start with entry do*/
    goto L1; // [16] 47
L2: 
    if (_start_903 == 0)
    {
        goto L3; // [19] 61
    }
    else{
    }

    /** 		kx += 1*/
    _kx_904 = _kx_904 + 1;

    /** 		haystack[kx] = start*/
    _2 = (int)SEQ_PTR(_haystack_902);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_902 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _kx_904);
    _1 = *(int *)_2;
    *(int *)_2 = _start_903;
    DeRef(_1);

    /** 		start += 1*/
    _start_903 = _start_903 + 1;

    /** 	entry*/
L1: 

    /** 		start = find(needle, haystack, start)*/
    _start_903 = find_from(_needle_901, _haystack_902, _start_903);

    /** 	end while*/
    goto L2; // [58] 19
L3: 

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _422 = _kx_904 + 1;
    if (_422 > MAXINT){
        _422 = NewDouble((double)_422);
    }
    if (IS_SEQUENCE(_haystack_902)){
            _423 = SEQ_PTR(_haystack_902)->length;
    }
    else {
        _423 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_902);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_422)) ? _422 : (long)(DBL_PTR(_422)->dbl);
        int stop = (IS_ATOM_INT(_423)) ? _423 : (long)(DBL_PTR(_423)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_902), start, &_haystack_902 );
            }
            else Tail(SEQ_PTR(_haystack_902), stop+1, &_haystack_902);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_902), start, &_haystack_902);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_902 = Remove_elements(start, stop, (SEQ_PTR(_haystack_902)->ref == 1));
        }
    }
    DeRef(_422);
    _422 = NOVALUE;
    _423 = NOVALUE;

    /** 	return haystack*/
    DeRef(_needle_901);
    return _haystack_902;
    ;
}


int _6find_all_but(int _needle_914, int _haystack_915, int _start_916)
{
    int _ix_917 = NOVALUE;
    int _jx_918 = NOVALUE;
    int _kx_919 = NOVALUE;
    int _432 = NOVALUE;
    int _431 = NOVALUE;
    int _429 = NOVALUE;
    int _425 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_916)) {
        _1 = (long)(DBL_PTR(_start_916)->dbl);
        if (UNIQUE(DBL_PTR(_start_916)) && (DBL_PTR(_start_916)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_916);
        _start_916 = _1;
    }

    /** 	integer ix = start */
    _ix_917 = _start_916;

    /** 	integer kx = 0 */
    _kx_919 = 0;

    /** 	while jx with entry do*/
    goto L1; // [23] 76
L2: 
    if (_jx_918 == 0)
    {
        goto L3; // [28] 90
    }
    else{
    }

    /** 		for i = ix to jx - 1 do */
    _425 = _jx_918 - 1;
    if ((long)((unsigned long)_425 +(unsigned long) HIGH_BITS) >= 0){
        _425 = NewDouble((double)_425);
    }
    {
        int _i_922;
        _i_922 = _ix_917;
L4: 
        if (binary_op_a(GREATER, _i_922, _425)){
            goto L5; // [37] 65
        }

        /** 			kx += 1 */
        _kx_919 = _kx_919 + 1;

        /** 			haystack[kx] = i */
        Ref(_i_922);
        _2 = (int)SEQ_PTR(_haystack_915);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _haystack_915 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _kx_919);
        _1 = *(int *)_2;
        *(int *)_2 = _i_922;
        DeRef(_1);

        /** 		end for */
        _0 = _i_922;
        if (IS_ATOM_INT(_i_922)) {
            _i_922 = _i_922 + 1;
            if ((long)((unsigned long)_i_922 +(unsigned long) HIGH_BITS) >= 0){
                _i_922 = NewDouble((double)_i_922);
            }
        }
        else {
            _i_922 = binary_op_a(PLUS, _i_922, 1);
        }
        DeRef(_0);
        goto L4; // [60] 44
L5: 
        ;
        DeRef(_i_922);
    }

    /** 		ix = jx + 1 */
    _ix_917 = _jx_918 + 1;

    /** 	entry */
L1: 

    /** 		jx = find( needle, haystack, ix ) */
    _jx_918 = find_from(_needle_914, _haystack_915, _ix_917);

    /** 	end while */
    goto L2; // [87] 26
L3: 

    /** 	for i = ix to length(haystack) do */
    if (IS_SEQUENCE(_haystack_915)){
            _429 = SEQ_PTR(_haystack_915)->length;
    }
    else {
        _429 = 1;
    }
    {
        int _i_928;
        _i_928 = _ix_917;
L6: 
        if (_i_928 > _429){
            goto L7; // [95] 123
        }

        /** 		kx += 1 */
        _kx_919 = _kx_919 + 1;

        /** 		haystack[kx] = i */
        _2 = (int)SEQ_PTR(_haystack_915);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _haystack_915 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _kx_919);
        _1 = *(int *)_2;
        *(int *)_2 = _i_928;
        DeRef(_1);

        /** 	end for */
        _i_928 = _i_928 + 1;
        goto L6; // [118] 102
L7: 
        ;
    }

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _431 = _kx_919 + 1;
    if (_431 > MAXINT){
        _431 = NewDouble((double)_431);
    }
    if (IS_SEQUENCE(_haystack_915)){
            _432 = SEQ_PTR(_haystack_915)->length;
    }
    else {
        _432 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_915);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_431)) ? _431 : (long)(DBL_PTR(_431)->dbl);
        int stop = (IS_ATOM_INT(_432)) ? _432 : (long)(DBL_PTR(_432)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_915), start, &_haystack_915 );
            }
            else Tail(SEQ_PTR(_haystack_915), stop+1, &_haystack_915);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_915), start, &_haystack_915);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_915 = Remove_elements(start, stop, (SEQ_PTR(_haystack_915)->ref == 1));
        }
    }
    DeRef(_431);
    _431 = NOVALUE;
    _432 = NOVALUE;

    /** 	return haystack*/
    DeRef(_needle_914);
    DeRef(_425);
    _425 = NOVALUE;
    return _haystack_915;
    ;
}


int _6find_nested(int _needle_941, int _haystack_942, int _flags_943, int _rtn_id_944)
{
    int _occurrences_945 = NOVALUE;
    int _depth_946 = NOVALUE;
    int _branches_947 = NOVALUE;
    int _indexes_948 = NOVALUE;
    int _last_indexes_949 = NOVALUE;
    int _last_idx_950 = NOVALUE;
    int _current_idx_952 = NOVALUE;
    int _direction_953 = NOVALUE;
    int _x_954 = NOVALUE;
    int _rc_955 = NOVALUE;
    int _any_956 = NOVALUE;
    int _info_978 = NOVALUE;
    int _472 = NOVALUE;
    int _460 = NOVALUE;
    int _457 = NOVALUE;
    int _455 = NOVALUE;
    int _453 = NOVALUE;
    int _450 = NOVALUE;
    int _448 = NOVALUE;
    int _446 = NOVALUE;
    int _440 = NOVALUE;
    int _438 = NOVALUE;
    int _437 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_flags_943)) {
        _1 = (long)(DBL_PTR(_flags_943)->dbl);
        if (UNIQUE(DBL_PTR(_flags_943)) && (DBL_PTR(_flags_943)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_flags_943);
        _flags_943 = _1;
    }
    if (!IS_ATOM_INT(_rtn_id_944)) {
        _1 = (long)(DBL_PTR(_rtn_id_944)->dbl);
        if (UNIQUE(DBL_PTR(_rtn_id_944)) && (DBL_PTR(_rtn_id_944)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rtn_id_944);
        _rtn_id_944 = _1;
    }

    /** 	sequence occurrences = {} -- accumulated results*/
    RefDS(_5);
    DeRef(_occurrences_945);
    _occurrences_945 = _5;

    /** 	integer depth = 0*/
    _depth_946 = 0;

    /** 	sequence branches = {}, indexes = {}, last_indexes = {} -- saved states*/
    RefDS(_5);
    DeRef(_branches_947);
    _branches_947 = _5;
    RefDS(_5);
    DeRefi(_indexes_948);
    _indexes_948 = _5;
    RefDS(_5);
    DeRefi(_last_indexes_949);
    _last_indexes_949 = _5;

    /** 	integer last_idx = length(haystack), current_idx = 1, direction = 1 -- assume forward searches more frequent*/
    if (IS_SEQUENCE(_haystack_942)){
            _last_idx_950 = SEQ_PTR(_haystack_942)->length;
    }
    else {
        _last_idx_950 = 1;
    }
    _current_idx_952 = 1;
    _direction_953 = 1;

    /** 	integer rc, any = and_bits(flags, NESTED_ANY)*/
    {unsigned long tu;
         tu = (unsigned long)_flags_943 & (unsigned long)1;
         _any_956 = MAKE_UINT(tu);
    }
    if (!IS_ATOM_INT(_any_956)) {
        _1 = (long)(DBL_PTR(_any_956)->dbl);
        if (UNIQUE(DBL_PTR(_any_956)) && (DBL_PTR(_any_956)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_any_956);
        _any_956 = _1;
    }

    /** 	if and_bits(flags,NESTED_BACKWARD) then*/
    {unsigned long tu;
         tu = (unsigned long)_flags_943 & (unsigned long)8;
         _437 = MAKE_UINT(tu);
    }
    if (_437 == 0) {
        DeRef(_437);
        _437 = NOVALUE;
        goto L1; // [73] 98
    }
    else {
        if (!IS_ATOM_INT(_437) && DBL_PTR(_437)->dbl == 0.0){
            DeRef(_437);
            _437 = NOVALUE;
            goto L1; // [73] 98
        }
        DeRef(_437);
        _437 = NOVALUE;
    }
    DeRef(_437);
    _437 = NOVALUE;

    /** 	    current_idx = last_idx*/
    _current_idx_952 = _last_idx_950;

    /** 	    last_idx = 1*/
    _last_idx_950 = 1;

    /** 	    direction = -1*/
    _direction_953 = -1;
L1: 

    /** 	any = any and sequence(needle)*/
    _438 = IS_SEQUENCE(_needle_941);
    _any_956 = (_any_956 != 0 && _438 != 0);
    _438 = NOVALUE;

    /** 	while 1 do -- traverse the whole haystack tree*/
L2: 

    /** 		while eu:compare(current_idx, last_idx) != direction do*/
L3: 
    if (IS_ATOM_INT(_current_idx_952) && IS_ATOM_INT(_last_idx_950)){
        _440 = (_current_idx_952 < _last_idx_950) ? -1 : (_current_idx_952 > _last_idx_950);
    }
    else{
        _440 = compare(_current_idx_952, _last_idx_950);
    }
    if (_440 == _direction_953)
    goto L4; // [123] 403

    /** 	        x = haystack[current_idx]*/
    DeRef(_x_954);
    _2 = (int)SEQ_PTR(_haystack_942);
    _x_954 = (int)*(((s1_ptr)_2)->base + _current_idx_952);
    Ref(_x_954);

    /** 			if rtn_id = NO_ROUTINE_ID then*/
    if (_rtn_id_944 != -99999)
    goto L5; // [137] 170

    /** 	         	if any then*/
    if (_any_956 == 0)
    {
        goto L6; // [143] 158
    }
    else{
    }

    /** 	         		rc = find(x, needle)*/
    _rc_955 = find_from(_x_954, _needle_941, 1);
    goto L7; // [155] 185
L6: 

    /** 					rc = equal(x, needle)*/
    if (_x_954 == _needle_941)
    _rc_955 = 1;
    else if (IS_ATOM_INT(_x_954) && IS_ATOM_INT(_needle_941))
    _rc_955 = 0;
    else
    _rc_955 = (compare(_x_954, _needle_941) == 0);
    goto L7; // [167] 185
L5: 

    /** 		        rc = call_func(rtn_id, {x, needle})*/
    Ref(_needle_941);
    Ref(_x_954);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _x_954;
    ((int *)_2)[2] = _needle_941;
    _446 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_446);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_rtn_id_944].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8)
                         );
    _rc_955 = _1;
    DeRefDS(_446);
    _446 = NOVALUE;
    if (!IS_ATOM_INT(_rc_955)) {
        _1 = (long)(DBL_PTR(_rc_955)->dbl);
        if (UNIQUE(DBL_PTR(_rc_955)) && (DBL_PTR(_rc_955)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rc_955);
        _rc_955 = _1;
    }
L7: 

    /** 	        if rc then*/
    if (_rc_955 == 0)
    {
        goto L8; // [189] 272
    }
    else{
    }

    /** 	            sequence info*/

    /** 				if depth < length(indexes) then*/
    if (IS_SEQUENCE(_indexes_948)){
            _448 = SEQ_PTR(_indexes_948)->length;
    }
    else {
        _448 = 1;
    }
    if (_depth_946 >= _448)
    goto L9; // [199] 217

    /** 					info = indexes[1..depth] & current_idx*/
    rhs_slice_target = (object_ptr)&_450;
    RHS_Slice(_indexes_948, 1, _depth_946);
    Append(&_info_978, _450, _current_idx_952);
    DeRefDS(_450);
    _450 = NOVALUE;
    goto LA; // [214] 224
L9: 

    /** 					info = indexes & current_idx*/
    Append(&_info_978, _indexes_948, _current_idx_952);
LA: 

    /** 	            if and_bits(flags, NESTED_INDEX) then*/
    {unsigned long tu;
         tu = (unsigned long)_flags_943 & (unsigned long)4;
         _453 = MAKE_UINT(tu);
    }
    if (_453 == 0) {
        DeRef(_453);
        _453 = NOVALUE;
        goto LB; // [230] 242
    }
    else {
        if (!IS_ATOM_INT(_453) && DBL_PTR(_453)->dbl == 0.0){
            DeRef(_453);
            _453 = NOVALUE;
            goto LB; // [230] 242
        }
        DeRef(_453);
        _453 = NOVALUE;
    }
    DeRef(_453);
    _453 = NOVALUE;

    /** 	               info = {info, rc}*/
    RefDS(_info_978);
    _0 = _info_978;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _info_978;
    ((int *)_2)[2] = _rc_955;
    _info_978 = MAKE_SEQ(_1);
    DeRefDS(_0);
LB: 

    /** 	            if and_bits(flags, NESTED_ALL) then*/
    {unsigned long tu;
         tu = (unsigned long)_flags_943 & (unsigned long)2;
         _455 = MAKE_UINT(tu);
    }
    if (_455 == 0) {
        DeRef(_455);
        _455 = NOVALUE;
        goto LC; // [248] 262
    }
    else {
        if (!IS_ATOM_INT(_455) && DBL_PTR(_455)->dbl == 0.0){
            DeRef(_455);
            _455 = NOVALUE;
            goto LC; // [248] 262
        }
        DeRef(_455);
        _455 = NOVALUE;
    }
    DeRef(_455);
    _455 = NOVALUE;

    /** 	                occurrences = append(occurrences, info)*/
    RefDS(_info_978);
    Append(&_occurrences_945, _occurrences_945, _info_978);
    goto LD; // [259] 271
LC: 

    /** 	                return info*/
    DeRef(_needle_941);
    DeRefDS(_haystack_942);
    DeRef(_occurrences_945);
    DeRef(_branches_947);
    DeRefi(_indexes_948);
    DeRefi(_last_indexes_949);
    DeRef(_x_954);
    return _info_978;
LD: 
L8: 
    DeRef(_info_978);
    _info_978 = NOVALUE;

    /** 	        if eu:compare(x, {})=1 then*/
    if (IS_ATOM_INT(_x_954) && IS_ATOM_INT(_5)){
        _457 = (_x_954 < _5) ? -1 : (_x_954 > _5);
    }
    else{
        _457 = compare(_x_954, _5);
    }
    if (_457 != 1)
    goto LE; // [280] 389

    /** 				depth += 1*/
    _depth_946 = _depth_946 + 1;

    /** 	            if length(indexes) < depth then*/
    if (IS_SEQUENCE(_indexes_948)){
            _460 = SEQ_PTR(_indexes_948)->length;
    }
    else {
        _460 = 1;
    }
    if (_460 >= _depth_946)
    goto LF; // [297] 322

    /** 	                indexes &= current_idx*/
    Append(&_indexes_948, _indexes_948, _current_idx_952);

    /** 	                branches = append(branches, haystack)*/
    RefDS(_haystack_942);
    Append(&_branches_947, _branches_947, _haystack_942);

    /** 	                last_indexes &= last_idx*/
    Append(&_last_indexes_949, _last_indexes_949, _last_idx_950);
    goto L10; // [319] 341
LF: 

    /** 	                indexes[depth] = current_idx*/
    _2 = (int)SEQ_PTR(_indexes_948);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _indexes_948 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _depth_946);
    *(int *)_2 = _current_idx_952;

    /** 	                branches[depth] = haystack*/
    RefDS(_haystack_942);
    _2 = (int)SEQ_PTR(_branches_947);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _branches_947 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _depth_946);
    _1 = *(int *)_2;
    *(int *)_2 = _haystack_942;
    DeRefDS(_1);

    /** 	                last_indexes[depth] = last_idx*/
    _2 = (int)SEQ_PTR(_last_indexes_949);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _last_indexes_949 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _depth_946);
    *(int *)_2 = _last_idx_950;
L10: 

    /** 	            haystack = x*/
    Ref(_x_954);
    DeRefDS(_haystack_942);
    _haystack_942 = _x_954;

    /** 	            if direction = 1 then*/
    if (_direction_953 != 1)
    goto L11; // [350] 371

    /** 	                current_idx = 1*/
    _current_idx_952 = 1;

    /** 	                last_idx = length(haystack)*/
    if (IS_SEQUENCE(_haystack_942)){
            _last_idx_950 = SEQ_PTR(_haystack_942)->length;
    }
    else {
        _last_idx_950 = 1;
    }
    goto L3; // [368] 119
L11: 

    /** 	                last_idx = 1*/
    _last_idx_950 = 1;

    /** 	                current_idx = length(haystack)*/
    if (IS_SEQUENCE(_haystack_942)){
            _current_idx_952 = SEQ_PTR(_haystack_942)->length;
    }
    else {
        _current_idx_952 = 1;
    }
    goto L3; // [386] 119
LE: 

    /** 				current_idx += direction*/
    _current_idx_952 = _current_idx_952 + _direction_953;

    /** 	    end while*/
    goto L3; // [400] 119
L4: 

    /** 	    if depth=0 then*/
    if (_depth_946 != 0)
    goto L12; // [405] 416

    /** 	        return occurrences -- either accumulated results, or {} if none -> ok*/
    DeRef(_needle_941);
    DeRefDS(_haystack_942);
    DeRef(_branches_947);
    DeRefi(_indexes_948);
    DeRefi(_last_indexes_949);
    DeRef(_x_954);
    return _occurrences_945;
L12: 

    /** 	    haystack = branches[depth]*/
    DeRefDS(_haystack_942);
    _2 = (int)SEQ_PTR(_branches_947);
    _haystack_942 = (int)*(((s1_ptr)_2)->base + _depth_946);
    RefDS(_haystack_942);

    /** 	    last_idx = last_indexes[depth]*/
    _2 = (int)SEQ_PTR(_last_indexes_949);
    _last_idx_950 = (int)*(((s1_ptr)_2)->base + _depth_946);

    /** 	    current_idx = indexes[depth] + direction*/
    _2 = (int)SEQ_PTR(_indexes_948);
    _472 = (int)*(((s1_ptr)_2)->base + _depth_946);
    _current_idx_952 = _472 + _direction_953;
    _472 = NOVALUE;

    /** 	    depth -= 1*/
    _depth_946 = _depth_946 - 1;

    /** 	end while*/
    goto L2; // [456] 114
    ;
}


int _6rfind(int _needle_1020, int _haystack_1021, int _start_1022)
{
    int _len_1024 = NOVALUE;
    int _485 = NOVALUE;
    int _484 = NOVALUE;
    int _481 = NOVALUE;
    int _480 = NOVALUE;
    int _478 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_1022)) {
        _1 = (long)(DBL_PTR(_start_1022)->dbl);
        if (UNIQUE(DBL_PTR(_start_1022)) && (DBL_PTR(_start_1022)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_1022);
        _start_1022 = _1;
    }

    /** 	integer len = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1021)){
            _len_1024 = SEQ_PTR(_haystack_1021)->length;
    }
    else {
        _len_1024 = 1;
    }

    /** 	if start = 0 then start = len end if*/
    if (_start_1022 != 0)
    goto L1; // [16] 26
    _start_1022 = _len_1024;
L1: 

    /** 	if (start > len) or (len + start < 1) then*/
    _478 = (_start_1022 > _len_1024);
    if (_478 != 0) {
        goto L2; // [32] 49
    }
    _480 = _len_1024 + _start_1022;
    if ((long)((unsigned long)_480 + (unsigned long)HIGH_BITS) >= 0) 
    _480 = NewDouble((double)_480);
    if (IS_ATOM_INT(_480)) {
        _481 = (_480 < 1);
    }
    else {
        _481 = (DBL_PTR(_480)->dbl < (double)1);
    }
    DeRef(_480);
    _480 = NOVALUE;
    if (_481 == 0)
    {
        DeRef(_481);
        _481 = NOVALUE;
        goto L3; // [45] 56
    }
    else{
        DeRef(_481);
        _481 = NOVALUE;
    }
L2: 

    /** 		return 0*/
    DeRef(_needle_1020);
    DeRefDS(_haystack_1021);
    DeRef(_478);
    _478 = NOVALUE;
    return 0;
L3: 

    /** 	if start < 1 then*/
    if (_start_1022 >= 1)
    goto L4; // [58] 71

    /** 		start = len + start*/
    _start_1022 = _len_1024 + _start_1022;
L4: 

    /** 	for i = start to 1 by -1 do*/
    {
        int _i_1037;
        _i_1037 = _start_1022;
L5: 
        if (_i_1037 < 1){
            goto L6; // [73] 107
        }

        /** 		if equal(haystack[i], needle) then*/
        _2 = (int)SEQ_PTR(_haystack_1021);
        _484 = (int)*(((s1_ptr)_2)->base + _i_1037);
        if (_484 == _needle_1020)
        _485 = 1;
        else if (IS_ATOM_INT(_484) && IS_ATOM_INT(_needle_1020))
        _485 = 0;
        else
        _485 = (compare(_484, _needle_1020) == 0);
        _484 = NOVALUE;
        if (_485 == 0)
        {
            _485 = NOVALUE;
            goto L7; // [90] 100
        }
        else{
            _485 = NOVALUE;
        }

        /** 			return i*/
        DeRef(_needle_1020);
        DeRefDS(_haystack_1021);
        DeRef(_478);
        _478 = NOVALUE;
        return _i_1037;
L7: 

        /** 	end for*/
        _i_1037 = _i_1037 + -1;
        goto L5; // [102] 80
L6: 
        ;
    }

    /** 	return 0*/
    DeRef(_needle_1020);
    DeRefDS(_haystack_1021);
    DeRef(_478);
    _478 = NOVALUE;
    return 0;
    ;
}


int _6find_replace(int _needle_1043, int _haystack_1044, int _replacement_1045, int _max_1046)
{
    int _posn_1047 = NOVALUE;
    int _489 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_1046)) {
        _1 = (long)(DBL_PTR(_max_1046)->dbl);
        if (UNIQUE(DBL_PTR(_max_1046)) && (DBL_PTR(_max_1046)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_1046);
        _max_1046 = _1;
    }

    /** 	integer posn = 0*/
    _posn_1047 = 0;

    /** 	while posn != 0 entry do */
    goto L1; // [16] 51
L2: 
    if (_posn_1047 == 0)
    goto L3; // [19] 69

    /** 		haystack[posn] = replacement*/
    Ref(_replacement_1045);
    _2 = (int)SEQ_PTR(_haystack_1044);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_1044 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _posn_1047);
    _1 = *(int *)_2;
    *(int *)_2 = _replacement_1045;
    DeRef(_1);

    /** 		max -= 1*/
    _max_1046 = _max_1046 - 1;

    /** 		if max = 0 then*/
    if (_max_1046 != 0)
    goto L4; // [39] 48

    /** 			exit*/
    goto L3; // [45] 69
L4: 

    /** 	entry*/
L1: 

    /** 		posn = find(needle, haystack, posn + 1)*/
    _489 = _posn_1047 + 1;
    if (_489 > MAXINT){
        _489 = NewDouble((double)_489);
    }
    _posn_1047 = find_from(_needle_1043, _haystack_1044, _489);
    DeRef(_489);
    _489 = NOVALUE;

    /** 	end while*/
    goto L2; // [66] 19
L3: 

    /** 	return haystack*/
    DeRef(_needle_1043);
    DeRef(_replacement_1045);
    return _haystack_1044;
    ;
}


int _6match_replace(int _needle_1057, int _haystack_1058, int _replacement_1059, int _max_1060)
{
    int _posn_1061 = NOVALUE;
    int _needle_len_1062 = NOVALUE;
    int _replacement_len_1063 = NOVALUE;
    int _scan_from_1064 = NOVALUE;
    int _cnt_1065 = NOVALUE;
    int _501 = NOVALUE;
    int _498 = NOVALUE;
    int _496 = NOVALUE;
    int _494 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_1060)) {
        _1 = (long)(DBL_PTR(_max_1060)->dbl);
        if (UNIQUE(DBL_PTR(_max_1060)) && (DBL_PTR(_max_1060)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_1060);
        _max_1060 = _1;
    }

    /** 	if max < 0 then*/
    if (_max_1060 >= 0)
    goto L1; // [9] 20

    /** 		return haystack*/
    DeRef(_needle_1057);
    DeRef(_replacement_1059);
    return _haystack_1058;
L1: 

    /** 	cnt = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1058)){
            _cnt_1065 = SEQ_PTR(_haystack_1058)->length;
    }
    else {
        _cnt_1065 = 1;
    }

    /** 	if max != 0 then*/
    if (_max_1060 == 0)
    goto L2; // [29] 41

    /** 		cnt = max*/
    _cnt_1065 = _max_1060;
L2: 

    /** 	if atom(needle) then*/
    _494 = IS_ATOM(_needle_1057);
    if (_494 == 0)
    {
        _494 = NOVALUE;
        goto L3; // [46] 56
    }
    else{
        _494 = NOVALUE;
    }

    /** 		needle = {needle}*/
    _0 = _needle_1057;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needle_1057);
    *((int *)(_2+4)) = _needle_1057;
    _needle_1057 = MAKE_SEQ(_1);
    DeRef(_0);
L3: 

    /** 	if atom(replacement) then*/
    _496 = IS_ATOM(_replacement_1059);
    if (_496 == 0)
    {
        _496 = NOVALUE;
        goto L4; // [61] 71
    }
    else{
        _496 = NOVALUE;
    }

    /** 		replacement = {replacement}*/
    _0 = _replacement_1059;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_replacement_1059);
    *((int *)(_2+4)) = _replacement_1059;
    _replacement_1059 = MAKE_SEQ(_1);
    DeRef(_0);
L4: 

    /** 	needle_len = length(needle) - 1*/
    if (IS_SEQUENCE(_needle_1057)){
            _498 = SEQ_PTR(_needle_1057)->length;
    }
    else {
        _498 = 1;
    }
    _needle_len_1062 = _498 - 1;
    _498 = NOVALUE;

    /** 	replacement_len = length(replacement)*/
    if (IS_SEQUENCE(_replacement_1059)){
            _replacement_len_1063 = SEQ_PTR(_replacement_1059)->length;
    }
    else {
        _replacement_len_1063 = 1;
    }

    /** 	scan_from = 1*/
    _scan_from_1064 = 1;

    /** 	while posn with entry do*/
    goto L5; // [98] 148
L6: 
    if (_posn_1061 == 0)
    {
        goto L7; // [103] 162
    }
    else{
    }

    /** 		haystack = replace(haystack, replacement, posn, posn + needle_len)*/
    _501 = _posn_1061 + _needle_len_1062;
    if ((long)((unsigned long)_501 + (unsigned long)HIGH_BITS) >= 0) 
    _501 = NewDouble((double)_501);
    {
        int p1 = _haystack_1058;
        int p2 = _replacement_1059;
        int p3 = _posn_1061;
        int p4 = _501;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_haystack_1058;
        Replace( &replace_params );
    }
    DeRef(_501);
    _501 = NOVALUE;

    /** 		cnt -= 1*/
    _cnt_1065 = _cnt_1065 - 1;

    /** 		if cnt = 0 then*/
    if (_cnt_1065 != 0)
    goto L8; // [128] 137

    /** 			exit*/
    goto L7; // [134] 162
L8: 

    /** 		scan_from = posn + replacement_len*/
    _scan_from_1064 = _posn_1061 + _replacement_len_1063;

    /** 	entry*/
L5: 

    /** 		posn = match(needle, haystack, scan_from)*/
    _posn_1061 = e_match_from(_needle_1057, _haystack_1058, _scan_from_1064);

    /** 	end while*/
    goto L6; // [159] 101
L7: 

    /** 	return haystack*/
    DeRef(_needle_1057);
    DeRef(_replacement_1059);
    return _haystack_1058;
    ;
}


int _6binary_search(int _needle_1090, int _haystack_1091, int _start_point_1092, int _end_point_1093)
{
    int _lo_1094 = NOVALUE;
    int _hi_1095 = NOVALUE;
    int _mid_1096 = NOVALUE;
    int _c_1097 = NOVALUE;
    int _527 = NOVALUE;
    int _519 = NOVALUE;
    int _517 = NOVALUE;
    int _514 = NOVALUE;
    int _513 = NOVALUE;
    int _512 = NOVALUE;
    int _511 = NOVALUE;
    int _508 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_point_1092)) {
        _1 = (long)(DBL_PTR(_start_point_1092)->dbl);
        if (UNIQUE(DBL_PTR(_start_point_1092)) && (DBL_PTR(_start_point_1092)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_point_1092);
        _start_point_1092 = _1;
    }
    if (!IS_ATOM_INT(_end_point_1093)) {
        _1 = (long)(DBL_PTR(_end_point_1093)->dbl);
        if (UNIQUE(DBL_PTR(_end_point_1093)) && (DBL_PTR(_end_point_1093)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_end_point_1093);
        _end_point_1093 = _1;
    }

    /** 	lo = start_point*/
    _lo_1094 = _start_point_1092;

    /** 	if end_point <= 0 then*/
    if (_end_point_1093 > 0)
    goto L1; // [20] 38

    /** 		hi = length(haystack) + end_point*/
    if (IS_SEQUENCE(_haystack_1091)){
            _508 = SEQ_PTR(_haystack_1091)->length;
    }
    else {
        _508 = 1;
    }
    _hi_1095 = _508 + _end_point_1093;
    _508 = NOVALUE;
    goto L2; // [35] 46
L1: 

    /** 		hi = end_point*/
    _hi_1095 = _end_point_1093;
L2: 

    /** 	if lo<1 then*/
    if (_lo_1094 >= 1)
    goto L3; // [48] 60

    /** 		lo=1*/
    _lo_1094 = 1;
L3: 

    /** 	if lo > hi and length(haystack) > 0 then*/
    _511 = (_lo_1094 > _hi_1095);
    if (_511 == 0) {
        goto L4; // [68] 91
    }
    if (IS_SEQUENCE(_haystack_1091)){
            _513 = SEQ_PTR(_haystack_1091)->length;
    }
    else {
        _513 = 1;
    }
    _514 = (_513 > 0);
    _513 = NOVALUE;
    if (_514 == 0)
    {
        DeRef(_514);
        _514 = NOVALUE;
        goto L4; // [80] 91
    }
    else{
        DeRef(_514);
        _514 = NOVALUE;
    }

    /** 		hi = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1091)){
            _hi_1095 = SEQ_PTR(_haystack_1091)->length;
    }
    else {
        _hi_1095 = 1;
    }
L4: 

    /** 	mid = start_point*/
    _mid_1096 = _start_point_1092;

    /** 	c = 0*/
    _c_1097 = 0;

    /** 	while lo <= hi do*/
L5: 
    if (_lo_1094 > _hi_1095)
    goto L6; // [110] 186

    /** 		mid = floor((lo + hi) / 2)*/
    _517 = _lo_1094 + _hi_1095;
    if ((long)((unsigned long)_517 + (unsigned long)HIGH_BITS) >= 0) 
    _517 = NewDouble((double)_517);
    if (IS_ATOM_INT(_517)) {
        _mid_1096 = _517 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _517, 2);
        _mid_1096 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_517);
    _517 = NOVALUE;
    if (!IS_ATOM_INT(_mid_1096)) {
        _1 = (long)(DBL_PTR(_mid_1096)->dbl);
        if (UNIQUE(DBL_PTR(_mid_1096)) && (DBL_PTR(_mid_1096)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_1096);
        _mid_1096 = _1;
    }

    /** 		c = eu:compare(needle, haystack[mid])*/
    _2 = (int)SEQ_PTR(_haystack_1091);
    _519 = (int)*(((s1_ptr)_2)->base + _mid_1096);
    if (IS_ATOM_INT(_needle_1090) && IS_ATOM_INT(_519)){
        _c_1097 = (_needle_1090 < _519) ? -1 : (_needle_1090 > _519);
    }
    else{
        _c_1097 = compare(_needle_1090, _519);
    }
    _519 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_1097 >= 0)
    goto L7; // [142] 157

    /** 			hi = mid - 1*/
    _hi_1095 = _mid_1096 - 1;
    goto L5; // [154] 110
L7: 

    /** 		elsif c > 0 then*/
    if (_c_1097 <= 0)
    goto L8; // [159] 174

    /** 			lo = mid + 1*/
    _lo_1094 = _mid_1096 + 1;
    goto L5; // [171] 110
L8: 

    /** 			return mid*/
    DeRef(_needle_1090);
    DeRefDS(_haystack_1091);
    DeRef(_511);
    _511 = NOVALUE;
    return _mid_1096;

    /** 	end while*/
    goto L5; // [183] 110
L6: 

    /** 	if c > 0 then*/
    if (_c_1097 <= 0)
    goto L9; // [188] 201

    /** 		mid += 1*/
    _mid_1096 = _mid_1096 + 1;
L9: 

    /** 	return -mid*/
    if ((unsigned long)_mid_1096 == 0xC0000000)
    _527 = (int)NewDouble((double)-0xC0000000);
    else
    _527 = - _mid_1096;
    DeRef(_needle_1090);
    DeRefDS(_haystack_1091);
    DeRef(_511);
    _511 = NOVALUE;
    return _527;
    ;
}


int _6match_all(int _needle_1130, int _haystack_1131, int _start_1132)
{
    int _kx_1133 = NOVALUE;
    int _534 = NOVALUE;
    int _533 = NOVALUE;
    int _530 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_1132)) {
        _1 = (long)(DBL_PTR(_start_1132)->dbl);
        if (UNIQUE(DBL_PTR(_start_1132)) && (DBL_PTR(_start_1132)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_1132);
        _start_1132 = _1;
    }

    /** 	integer kx = 0*/
    _kx_1133 = 0;

    /** 	while start > 0 with entry do*/
    goto L1; // [18] 53
L2: 
    if (_start_1132 <= 0)
    goto L3; // [21] 67

    /** 		kx += 1*/
    _kx_1133 = _kx_1133 + 1;

    /** 		haystack[kx] = start*/
    _2 = (int)SEQ_PTR(_haystack_1131);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_1131 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _kx_1133);
    _1 = *(int *)_2;
    *(int *)_2 = _start_1132;
    DeRef(_1);

    /** 		start += length(needle)*/
    if (IS_SEQUENCE(_needle_1130)){
            _530 = SEQ_PTR(_needle_1130)->length;
    }
    else {
        _530 = 1;
    }
    _start_1132 = _start_1132 + _530;
    _530 = NOVALUE;

    /** 	entry*/
L1: 

    /** 		start = match(needle, haystack, start)*/
    _start_1132 = e_match_from(_needle_1130, _haystack_1131, _start_1132);

    /** 	end while*/
    goto L2; // [64] 21
L3: 

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _533 = _kx_1133 + 1;
    if (_533 > MAXINT){
        _533 = NewDouble((double)_533);
    }
    if (IS_SEQUENCE(_haystack_1131)){
            _534 = SEQ_PTR(_haystack_1131)->length;
    }
    else {
        _534 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_1131);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_533)) ? _533 : (long)(DBL_PTR(_533)->dbl);
        int stop = (IS_ATOM_INT(_534)) ? _534 : (long)(DBL_PTR(_534)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_1131), start, &_haystack_1131 );
            }
            else Tail(SEQ_PTR(_haystack_1131), stop+1, &_haystack_1131);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_1131), start, &_haystack_1131);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_1131 = Remove_elements(start, stop, (SEQ_PTR(_haystack_1131)->ref == 1));
        }
    }
    DeRef(_533);
    _533 = NOVALUE;
    _534 = NOVALUE;

    /** 	return haystack*/
    DeRefDS(_needle_1130);
    return _haystack_1131;
    ;
}


int _6rmatch(int _needle_1145, int _haystack_1146, int _start_1147)
{
    int _len_1149 = NOVALUE;
    int _lenX_1150 = NOVALUE;
    int _554 = NOVALUE;
    int _553 = NOVALUE;
    int _552 = NOVALUE;
    int _549 = NOVALUE;
    int _547 = NOVALUE;
    int _546 = NOVALUE;
    int _543 = NOVALUE;
    int _542 = NOVALUE;
    int _540 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_1147)) {
        _1 = (long)(DBL_PTR(_start_1147)->dbl);
        if (UNIQUE(DBL_PTR(_start_1147)) && (DBL_PTR(_start_1147)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_1147);
        _start_1147 = _1;
    }

    /** 	len = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1146)){
            _len_1149 = SEQ_PTR(_haystack_1146)->length;
    }
    else {
        _len_1149 = 1;
    }

    /** 	lenX = length(needle)*/
    if (IS_SEQUENCE(_needle_1145)){
            _lenX_1150 = SEQ_PTR(_needle_1145)->length;
    }
    else {
        _lenX_1150 = 1;
    }

    /** 	if lenX = 0 then*/
    if (_lenX_1150 != 0)
    goto L1; // [25] 38

    /** 		return 0*/
    DeRefDS(_needle_1145);
    DeRefDS(_haystack_1146);
    return 0;
    goto L2; // [35] 69
L1: 

    /** 	elsif (start > len) or  (len + start < 1) then*/
    _540 = (_start_1147 > _len_1149);
    if (_540 != 0) {
        goto L3; // [44] 61
    }
    _542 = _len_1149 + _start_1147;
    if ((long)((unsigned long)_542 + (unsigned long)HIGH_BITS) >= 0) 
    _542 = NewDouble((double)_542);
    if (IS_ATOM_INT(_542)) {
        _543 = (_542 < 1);
    }
    else {
        _543 = (DBL_PTR(_542)->dbl < (double)1);
    }
    DeRef(_542);
    _542 = NOVALUE;
    if (_543 == 0)
    {
        DeRef(_543);
        _543 = NOVALUE;
        goto L4; // [57] 68
    }
    else{
        DeRef(_543);
        _543 = NOVALUE;
    }
L3: 

    /** 		return 0*/
    DeRefDS(_needle_1145);
    DeRefDS(_haystack_1146);
    DeRef(_540);
    _540 = NOVALUE;
    return 0;
L4: 
L2: 

    /** 	if start < 1 then*/
    if (_start_1147 >= 1)
    goto L5; // [71] 84

    /** 		start = len + start*/
    _start_1147 = _len_1149 + _start_1147;
L5: 

    /** 	if start + lenX - 1 > len then*/
    _546 = _start_1147 + _lenX_1150;
    if ((long)((unsigned long)_546 + (unsigned long)HIGH_BITS) >= 0) 
    _546 = NewDouble((double)_546);
    if (IS_ATOM_INT(_546)) {
        _547 = _546 - 1;
        if ((long)((unsigned long)_547 +(unsigned long) HIGH_BITS) >= 0){
            _547 = NewDouble((double)_547);
        }
    }
    else {
        _547 = NewDouble(DBL_PTR(_546)->dbl - (double)1);
    }
    DeRef(_546);
    _546 = NOVALUE;
    if (binary_op_a(LESSEQ, _547, _len_1149)){
        DeRef(_547);
        _547 = NOVALUE;
        goto L6; // [94] 111
    }
    DeRef(_547);
    _547 = NOVALUE;

    /** 		start = len - lenX + 1*/
    _549 = _len_1149 - _lenX_1150;
    if ((long)((unsigned long)_549 +(unsigned long) HIGH_BITS) >= 0){
        _549 = NewDouble((double)_549);
    }
    if (IS_ATOM_INT(_549)) {
        _start_1147 = _549 + 1;
    }
    else
    { // coercing _start_1147 to an integer 1
        _start_1147 = 1+(long)(DBL_PTR(_549)->dbl);
        if( !IS_ATOM_INT(_start_1147) ){
            _start_1147 = (object)DBL_PTR(_start_1147)->dbl;
        }
    }
    DeRef(_549);
    _549 = NOVALUE;
L6: 

    /** 	lenX -= 1*/
    _lenX_1150 = _lenX_1150 - 1;

    /** 	for i=start to 1 by -1 do*/
    {
        int _i_1171;
        _i_1171 = _start_1147;
L7: 
        if (_i_1171 < 1){
            goto L8; // [121] 160
        }

        /** 		if equal(needle, haystack[i..i + lenX]) then*/
        _552 = _i_1171 + _lenX_1150;
        rhs_slice_target = (object_ptr)&_553;
        RHS_Slice(_haystack_1146, _i_1171, _552);
        if (_needle_1145 == _553)
        _554 = 1;
        else if (IS_ATOM_INT(_needle_1145) && IS_ATOM_INT(_553))
        _554 = 0;
        else
        _554 = (compare(_needle_1145, _553) == 0);
        DeRefDS(_553);
        _553 = NOVALUE;
        if (_554 == 0)
        {
            _554 = NOVALUE;
            goto L9; // [143] 153
        }
        else{
            _554 = NOVALUE;
        }

        /** 			return i*/
        DeRefDS(_needle_1145);
        DeRefDS(_haystack_1146);
        DeRef(_540);
        _540 = NOVALUE;
        _552 = NOVALUE;
        return _i_1171;
L9: 

        /** 	end for*/
        _i_1171 = _i_1171 + -1;
        goto L7; // [155] 128
L8: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_needle_1145);
    DeRefDS(_haystack_1146);
    DeRef(_540);
    _540 = NOVALUE;
    DeRef(_552);
    _552 = NOVALUE;
    return 0;
    ;
}


int _6begins(int _sub_text_1178, int _full_text_1179)
{
    int _565 = NOVALUE;
    int _564 = NOVALUE;
    int _563 = NOVALUE;
    int _561 = NOVALUE;
    int _560 = NOVALUE;
    int _559 = NOVALUE;
    int _558 = NOVALUE;
    int _557 = NOVALUE;
    int _555 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1179)){
            _555 = SEQ_PTR(_full_text_1179)->length;
    }
    else {
        _555 = 1;
    }
    if (_555 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_1178);
    DeRefDS(_full_text_1179);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _557 = IS_ATOM(_sub_text_1178);
    if (_557 == 0)
    {
        _557 = NOVALUE;
        goto L2; // [24] 57
    }
    else{
        _557 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[1]) then*/
    _2 = (int)SEQ_PTR(_full_text_1179);
    _558 = (int)*(((s1_ptr)_2)->base + 1);
    if (_sub_text_1178 == _558)
    _559 = 1;
    else if (IS_ATOM_INT(_sub_text_1178) && IS_ATOM_INT(_558))
    _559 = 0;
    else
    _559 = (compare(_sub_text_1178, _558) == 0);
    _558 = NOVALUE;
    if (_559 == 0)
    {
        _559 = NOVALUE;
        goto L3; // [37] 49
    }
    else{
        _559 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_1178);
    DeRefDS(_full_text_1179);
    return 1;
    goto L4; // [46] 56
L3: 

    /** 			return 0*/
    DeRef(_sub_text_1178);
    DeRefDS(_full_text_1179);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1178)){
            _560 = SEQ_PTR(_sub_text_1178)->length;
    }
    else {
        _560 = 1;
    }
    if (IS_SEQUENCE(_full_text_1179)){
            _561 = SEQ_PTR(_full_text_1179)->length;
    }
    else {
        _561 = 1;
    }
    if (_560 <= _561)
    goto L5; // [65] 76

    /** 		return 0*/
    DeRef(_sub_text_1178);
    DeRefDS(_full_text_1179);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[1.. length(sub_text)]) then*/
    if (IS_SEQUENCE(_sub_text_1178)){
            _563 = SEQ_PTR(_sub_text_1178)->length;
    }
    else {
        _563 = 1;
    }
    rhs_slice_target = (object_ptr)&_564;
    RHS_Slice(_full_text_1179, 1, _563);
    if (_sub_text_1178 == _564)
    _565 = 1;
    else if (IS_ATOM_INT(_sub_text_1178) && IS_ATOM_INT(_564))
    _565 = 0;
    else
    _565 = (compare(_sub_text_1178, _564) == 0);
    DeRefDS(_564);
    _564 = NOVALUE;
    if (_565 == 0)
    {
        _565 = NOVALUE;
        goto L6; // [90] 102
    }
    else{
        _565 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_1178);
    DeRefDS(_full_text_1179);
    return 1;
    goto L7; // [99] 109
L6: 

    /** 		return 0*/
    DeRef(_sub_text_1178);
    DeRefDS(_full_text_1179);
    return 0;
L7: 
    ;
}


int _6ends(int _sub_text_1200, int _full_text_1201)
{
    int _581 = NOVALUE;
    int _580 = NOVALUE;
    int _579 = NOVALUE;
    int _578 = NOVALUE;
    int _577 = NOVALUE;
    int _576 = NOVALUE;
    int _575 = NOVALUE;
    int _573 = NOVALUE;
    int _572 = NOVALUE;
    int _571 = NOVALUE;
    int _570 = NOVALUE;
    int _569 = NOVALUE;
    int _568 = NOVALUE;
    int _566 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1201)){
            _566 = SEQ_PTR(_full_text_1201)->length;
    }
    else {
        _566 = 1;
    }
    if (_566 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_1200);
    DeRefDS(_full_text_1201);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _568 = IS_ATOM(_sub_text_1200);
    if (_568 == 0)
    {
        _568 = NOVALUE;
        goto L2; // [24] 60
    }
    else{
        _568 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[$]) then*/
    if (IS_SEQUENCE(_full_text_1201)){
            _569 = SEQ_PTR(_full_text_1201)->length;
    }
    else {
        _569 = 1;
    }
    _2 = (int)SEQ_PTR(_full_text_1201);
    _570 = (int)*(((s1_ptr)_2)->base + _569);
    if (_sub_text_1200 == _570)
    _571 = 1;
    else if (IS_ATOM_INT(_sub_text_1200) && IS_ATOM_INT(_570))
    _571 = 0;
    else
    _571 = (compare(_sub_text_1200, _570) == 0);
    _570 = NOVALUE;
    if (_571 == 0)
    {
        _571 = NOVALUE;
        goto L3; // [40] 52
    }
    else{
        _571 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_1200);
    DeRefDS(_full_text_1201);
    return 1;
    goto L4; // [49] 59
L3: 

    /** 			return 0*/
    DeRef(_sub_text_1200);
    DeRefDS(_full_text_1201);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1200)){
            _572 = SEQ_PTR(_sub_text_1200)->length;
    }
    else {
        _572 = 1;
    }
    if (IS_SEQUENCE(_full_text_1201)){
            _573 = SEQ_PTR(_full_text_1201)->length;
    }
    else {
        _573 = 1;
    }
    if (_572 <= _573)
    goto L5; // [68] 79

    /** 		return 0*/
    DeRef(_sub_text_1200);
    DeRefDS(_full_text_1201);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[$ - length(sub_text) + 1 .. $]) then*/
    if (IS_SEQUENCE(_full_text_1201)){
            _575 = SEQ_PTR(_full_text_1201)->length;
    }
    else {
        _575 = 1;
    }
    if (IS_SEQUENCE(_sub_text_1200)){
            _576 = SEQ_PTR(_sub_text_1200)->length;
    }
    else {
        _576 = 1;
    }
    _577 = _575 - _576;
    _575 = NOVALUE;
    _576 = NOVALUE;
    _578 = _577 + 1;
    _577 = NOVALUE;
    if (IS_SEQUENCE(_full_text_1201)){
            _579 = SEQ_PTR(_full_text_1201)->length;
    }
    else {
        _579 = 1;
    }
    rhs_slice_target = (object_ptr)&_580;
    RHS_Slice(_full_text_1201, _578, _579);
    if (_sub_text_1200 == _580)
    _581 = 1;
    else if (IS_ATOM_INT(_sub_text_1200) && IS_ATOM_INT(_580))
    _581 = 0;
    else
    _581 = (compare(_sub_text_1200, _580) == 0);
    DeRefDS(_580);
    _580 = NOVALUE;
    if (_581 == 0)
    {
        _581 = NOVALUE;
        goto L6; // [107] 119
    }
    else{
        _581 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_1200);
    DeRefDS(_full_text_1201);
    _578 = NOVALUE;
    return 1;
    goto L7; // [116] 126
L6: 

    /** 		return 0*/
    DeRef(_sub_text_1200);
    DeRefDS(_full_text_1201);
    DeRef(_578);
    _578 = NOVALUE;
    return 0;
L7: 
    ;
}


int _6is_in_range(int _item_1227, int _range_limits_1228, int _boundries_1229)
{
    int _616 = NOVALUE;
    int _615 = NOVALUE;
    int _614 = NOVALUE;
    int _612 = NOVALUE;
    int _611 = NOVALUE;
    int _609 = NOVALUE;
    int _608 = NOVALUE;
    int _607 = NOVALUE;
    int _605 = NOVALUE;
    int _604 = NOVALUE;
    int _601 = NOVALUE;
    int _600 = NOVALUE;
    int _599 = NOVALUE;
    int _597 = NOVALUE;
    int _596 = NOVALUE;
    int _593 = NOVALUE;
    int _592 = NOVALUE;
    int _591 = NOVALUE;
    int _589 = NOVALUE;
    int _588 = NOVALUE;
    int _583 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(range_limits) < 2 then*/
    if (IS_SEQUENCE(_range_limits_1228)){
            _583 = SEQ_PTR(_range_limits_1228)->length;
    }
    else {
        _583 = 1;
    }
    if (_583 >= 2)
    goto L1; // [10] 21

    /** 		return 0*/
    DeRef(_item_1227);
    DeRefDS(_range_limits_1228);
    DeRefDS(_boundries_1229);
    return 0;
L1: 

    /** 	switch boundries do*/
    _1 = find(_boundries_1229, _585);
    switch ( _1 ){ 

        /** 		case "()" then*/
        case 1:

        /** 			if eu:compare(item, range_limits[1]) <= 0 then*/
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _588 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_588)){
            _589 = (_item_1227 < _588) ? -1 : (_item_1227 > _588);
        }
        else{
            _589 = compare(_item_1227, _588);
        }
        _588 = NOVALUE;
        if (_589 > 0)
        goto L2; // [42] 53

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
L2: 

        /** 			if eu:compare(item, range_limits[$]) >= 0 then*/
        if (IS_SEQUENCE(_range_limits_1228)){
                _591 = SEQ_PTR(_range_limits_1228)->length;
        }
        else {
            _591 = 1;
        }
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _592 = (int)*(((s1_ptr)_2)->base + _591);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_592)){
            _593 = (_item_1227 < _592) ? -1 : (_item_1227 > _592);
        }
        else{
            _593 = compare(_item_1227, _592);
        }
        _592 = NOVALUE;
        if (_593 < 0)
        goto L3; // [66] 231

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
        goto L3; // [77] 231

        /** 		case "[)" then*/
        case 2:

        /** 			if eu:compare(item, range_limits[1]) < 0 then*/
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _596 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_596)){
            _597 = (_item_1227 < _596) ? -1 : (_item_1227 > _596);
        }
        else{
            _597 = compare(_item_1227, _596);
        }
        _596 = NOVALUE;
        if (_597 >= 0)
        goto L4; // [93] 104

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
L4: 

        /** 			if eu:compare(item, range_limits[$]) >= 0 then*/
        if (IS_SEQUENCE(_range_limits_1228)){
                _599 = SEQ_PTR(_range_limits_1228)->length;
        }
        else {
            _599 = 1;
        }
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _600 = (int)*(((s1_ptr)_2)->base + _599);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_600)){
            _601 = (_item_1227 < _600) ? -1 : (_item_1227 > _600);
        }
        else{
            _601 = compare(_item_1227, _600);
        }
        _600 = NOVALUE;
        if (_601 < 0)
        goto L3; // [117] 231

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
        goto L3; // [128] 231

        /** 		case "(]" then*/
        case 3:

        /** 			if eu:compare(item, range_limits[1]) <= 0 then*/
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _604 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_604)){
            _605 = (_item_1227 < _604) ? -1 : (_item_1227 > _604);
        }
        else{
            _605 = compare(_item_1227, _604);
        }
        _604 = NOVALUE;
        if (_605 > 0)
        goto L5; // [144] 155

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
L5: 

        /** 			if eu:compare(item, range_limits[$]) > 0 then*/
        if (IS_SEQUENCE(_range_limits_1228)){
                _607 = SEQ_PTR(_range_limits_1228)->length;
        }
        else {
            _607 = 1;
        }
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _608 = (int)*(((s1_ptr)_2)->base + _607);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_608)){
            _609 = (_item_1227 < _608) ? -1 : (_item_1227 > _608);
        }
        else{
            _609 = compare(_item_1227, _608);
        }
        _608 = NOVALUE;
        if (_609 <= 0)
        goto L3; // [168] 231

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
        goto L3; // [179] 231

        /** 		case else*/
        case 0:

        /** 			if eu:compare(item, range_limits[1]) < 0 then*/
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _611 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_611)){
            _612 = (_item_1227 < _611) ? -1 : (_item_1227 > _611);
        }
        else{
            _612 = compare(_item_1227, _611);
        }
        _611 = NOVALUE;
        if (_612 >= 0)
        goto L6; // [195] 206

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
L6: 

        /** 			if eu:compare(item, range_limits[$]) > 0 then*/
        if (IS_SEQUENCE(_range_limits_1228)){
                _614 = SEQ_PTR(_range_limits_1228)->length;
        }
        else {
            _614 = 1;
        }
        _2 = (int)SEQ_PTR(_range_limits_1228);
        _615 = (int)*(((s1_ptr)_2)->base + _614);
        if (IS_ATOM_INT(_item_1227) && IS_ATOM_INT(_615)){
            _616 = (_item_1227 < _615) ? -1 : (_item_1227 > _615);
        }
        else{
            _616 = compare(_item_1227, _615);
        }
        _615 = NOVALUE;
        if (_616 <= 0)
        goto L7; // [219] 230

        /** 				return 0*/
        DeRef(_item_1227);
        DeRefDS(_range_limits_1228);
        DeRefDS(_boundries_1229);
        return 0;
L7: 
    ;}L3: 

    /** 	return 1*/
    DeRef(_item_1227);
    DeRefDS(_range_limits_1228);
    DeRefDS(_boundries_1229);
    return 1;
    ;
}


int _6is_in_list(int _item_1281, int _list_1282)
{
    int _619 = NOVALUE;
    int _618 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return (find(item, list) != 0)*/
    _618 = find_from(_item_1281, _list_1282, 1);
    _619 = (_618 != 0);
    _618 = NOVALUE;
    DeRef(_item_1281);
    DeRefDS(_list_1282);
    return _619;
    ;
}


int _6lookup(int _find_item_1287, int _source_list_1288, int _target_list_1289, int _def_value_1290)
{
    int _lPosn_1291 = NOVALUE;
    int _628 = NOVALUE;
    int _627 = NOVALUE;
    int _625 = NOVALUE;
    int _624 = NOVALUE;
    int _623 = NOVALUE;
    int _621 = NOVALUE;
    int _0, _1, _2;
    

    /**     lPosn = find(find_item, source_list)*/
    _lPosn_1291 = find_from(_find_item_1287, _source_list_1288, 1);

    /**     if lPosn then*/
    if (_lPosn_1291 == 0)
    {
        goto L1; // [16] 51
    }
    else{
    }

    /**         if lPosn <= length(target_list) then*/
    if (IS_SEQUENCE(_target_list_1289)){
            _621 = SEQ_PTR(_target_list_1289)->length;
    }
    else {
        _621 = 1;
    }
    if (_lPosn_1291 > _621)
    goto L2; // [24] 41

    /**             return target_list[lPosn]*/
    _2 = (int)SEQ_PTR(_target_list_1289);
    _623 = (int)*(((s1_ptr)_2)->base + _lPosn_1291);
    Ref(_623);
    DeRef(_find_item_1287);
    DeRefDS(_source_list_1288);
    DeRefDS(_target_list_1289);
    DeRef(_def_value_1290);
    return _623;
    goto L3; // [38] 86
L2: 

    /**         	return def_value*/
    DeRef(_find_item_1287);
    DeRefDS(_source_list_1288);
    DeRefDS(_target_list_1289);
    _623 = NOVALUE;
    return _def_value_1290;
    goto L3; // [48] 86
L1: 

    /**     elsif length(target_list) > length(source_list) then*/
    if (IS_SEQUENCE(_target_list_1289)){
            _624 = SEQ_PTR(_target_list_1289)->length;
    }
    else {
        _624 = 1;
    }
    if (IS_SEQUENCE(_source_list_1288)){
            _625 = SEQ_PTR(_source_list_1288)->length;
    }
    else {
        _625 = 1;
    }
    if (_624 <= _625)
    goto L4; // [59] 79

    /**         return target_list[$]*/
    if (IS_SEQUENCE(_target_list_1289)){
            _627 = SEQ_PTR(_target_list_1289)->length;
    }
    else {
        _627 = 1;
    }
    _2 = (int)SEQ_PTR(_target_list_1289);
    _628 = (int)*(((s1_ptr)_2)->base + _627);
    Ref(_628);
    DeRef(_find_item_1287);
    DeRefDS(_source_list_1288);
    DeRefDS(_target_list_1289);
    DeRef(_def_value_1290);
    _623 = NOVALUE;
    return _628;
    goto L3; // [76] 86
L4: 

    /**     	return def_value*/
    DeRef(_find_item_1287);
    DeRefDS(_source_list_1288);
    DeRefDS(_target_list_1289);
    _623 = NOVALUE;
    _628 = NOVALUE;
    return _def_value_1290;
L3: 
    ;
}


int _6vlookup(int _find_item_1308, int _grid_data_1309, int _source_col_1310, int _target_col_1311, int _def_value_1312)
{
    int _642 = NOVALUE;
    int _641 = NOVALUE;
    int _639 = NOVALUE;
    int _638 = NOVALUE;
    int _637 = NOVALUE;
    int _636 = NOVALUE;
    int _635 = NOVALUE;
    int _633 = NOVALUE;
    int _632 = NOVALUE;
    int _631 = NOVALUE;
    int _630 = NOVALUE;
    int _629 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_source_col_1310)) {
        _1 = (long)(DBL_PTR(_source_col_1310)->dbl);
        if (UNIQUE(DBL_PTR(_source_col_1310)) && (DBL_PTR(_source_col_1310)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_source_col_1310);
        _source_col_1310 = _1;
    }
    if (!IS_ATOM_INT(_target_col_1311)) {
        _1 = (long)(DBL_PTR(_target_col_1311)->dbl);
        if (UNIQUE(DBL_PTR(_target_col_1311)) && (DBL_PTR(_target_col_1311)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_target_col_1311);
        _target_col_1311 = _1;
    }

    /**     for i = 1 to length(grid_data) do*/
    if (IS_SEQUENCE(_grid_data_1309)){
            _629 = SEQ_PTR(_grid_data_1309)->length;
    }
    else {
        _629 = 1;
    }
    {
        int _i_1314;
        _i_1314 = 1;
L1: 
        if (_i_1314 > _629){
            goto L2; // [16] 117
        }

        /**     	if atom(grid_data[i]) then*/
        _2 = (int)SEQ_PTR(_grid_data_1309);
        _630 = (int)*(((s1_ptr)_2)->base + _i_1314);
        _631 = IS_ATOM(_630);
        _630 = NOVALUE;
        if (_631 == 0)
        {
            _631 = NOVALUE;
            goto L3; // [32] 40
        }
        else{
            _631 = NOVALUE;
        }

        /**     		continue*/
        goto L4; // [37] 112
L3: 

        /**     	if length(grid_data[i]) < source_col then*/
        _2 = (int)SEQ_PTR(_grid_data_1309);
        _632 = (int)*(((s1_ptr)_2)->base + _i_1314);
        if (IS_SEQUENCE(_632)){
                _633 = SEQ_PTR(_632)->length;
        }
        else {
            _633 = 1;
        }
        _632 = NOVALUE;
        if (_633 >= _source_col_1310)
        goto L5; // [49] 58

        /**     		continue*/
        goto L4; // [55] 112
L5: 

        /**     	if equal(find_item, grid_data[i][source_col]) then*/
        _2 = (int)SEQ_PTR(_grid_data_1309);
        _635 = (int)*(((s1_ptr)_2)->base + _i_1314);
        _2 = (int)SEQ_PTR(_635);
        _636 = (int)*(((s1_ptr)_2)->base + _source_col_1310);
        _635 = NOVALUE;
        if (_find_item_1308 == _636)
        _637 = 1;
        else if (IS_ATOM_INT(_find_item_1308) && IS_ATOM_INT(_636))
        _637 = 0;
        else
        _637 = (compare(_find_item_1308, _636) == 0);
        _636 = NOVALUE;
        if (_637 == 0)
        {
            _637 = NOVALUE;
            goto L6; // [72] 110
        }
        else{
            _637 = NOVALUE;
        }

        /** 	    	if length(grid_data[i]) < target_col then*/
        _2 = (int)SEQ_PTR(_grid_data_1309);
        _638 = (int)*(((s1_ptr)_2)->base + _i_1314);
        if (IS_SEQUENCE(_638)){
                _639 = SEQ_PTR(_638)->length;
        }
        else {
            _639 = 1;
        }
        _638 = NOVALUE;
        if (_639 >= _target_col_1311)
        goto L7; // [84] 95

        /**     			return def_value*/
        DeRef(_find_item_1308);
        DeRefDS(_grid_data_1309);
        _632 = NOVALUE;
        _638 = NOVALUE;
        return _def_value_1312;
L7: 

        /**     		return grid_data[i][target_col]*/
        _2 = (int)SEQ_PTR(_grid_data_1309);
        _641 = (int)*(((s1_ptr)_2)->base + _i_1314);
        _2 = (int)SEQ_PTR(_641);
        _642 = (int)*(((s1_ptr)_2)->base + _target_col_1311);
        _641 = NOVALUE;
        Ref(_642);
        DeRef(_find_item_1308);
        DeRefDS(_grid_data_1309);
        DeRef(_def_value_1312);
        _632 = NOVALUE;
        _638 = NOVALUE;
        return _642;
L6: 

        /**     end for*/
L4: 
        _i_1314 = _i_1314 + 1;
        goto L1; // [112] 23
L2: 
        ;
    }

    /**     return def_value*/
    DeRef(_find_item_1308);
    DeRefDS(_grid_data_1309);
    _632 = NOVALUE;
    _638 = NOVALUE;
    _642 = NOVALUE;
    return _def_value_1312;
    ;
}



// 0x00597E67
