// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _8find_first_wildcard(int _name_9279, int _from_9280)
{
    int _asterisk_at_9281 = NOVALUE;
    int _question_at_9283 = NOVALUE;
    int _first_wildcard_at_9285 = NOVALUE;
    int _5180 = NOVALUE;
    int _5179 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer asterisk_at = eu:find('*', name, from)*/
    _asterisk_at_9281 = find_from(42, _name_9279, 1);

    /** 	integer question_at = eu:find('?', name, from)*/
    _question_at_9283 = find_from(63, _name_9279, 1);

    /** 	integer first_wildcard_at = asterisk_at*/
    _first_wildcard_at_9285 = _asterisk_at_9281;

    /** 	if asterisk_at or question_at then*/
    if (_asterisk_at_9281 != 0) {
        goto L1; // [34] 43
    }
    if (_question_at_9283 == 0)
    {
        goto L2; // [39] 66
    }
    else{
    }
L1: 

    /** 		if question_at and question_at < asterisk_at then*/
    if (_question_at_9283 == 0) {
        goto L3; // [45] 65
    }
    _5180 = (_question_at_9283 < _asterisk_at_9281);
    if (_5180 == 0)
    {
        DeRef(_5180);
        _5180 = NOVALUE;
        goto L3; // [54] 65
    }
    else{
        DeRef(_5180);
        _5180 = NOVALUE;
    }

    /** 			first_wildcard_at = question_at*/
    _first_wildcard_at_9285 = _question_at_9283;
L3: 
L2: 

    /** 	return first_wildcard_at*/
    DeRefDS(_name_9279);
    return _first_wildcard_at_9285;
    ;
}


int _8dir(int _name_9293)
{
    int _5181 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    _5181 = machine(22, _name_9293);
    DeRefDS(_name_9293);
    return _5181;
    ;
}


int _8current_dir()
{
    int _5183 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    _5183 = machine(23, 0);
    return _5183;
    ;
}


int _8chdir(int _newdir_9301)
{
    int _5184 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CHDIR, newdir)*/
    _5184 = machine(63, _newdir_9301);
    DeRefDS(_newdir_9301);
    return _5184;
    ;
}


int _8walk_dir(int _path_name_9317, int _your_function_9318, int _scan_subdirs_9319, int _dir_source_9320)
{
    int _d_9321 = NOVALUE;
    int _abort_now_9322 = NOVALUE;
    int _orig_func_9323 = NOVALUE;
    int _user_data_9324 = NOVALUE;
    int _source_orig_func_9326 = NOVALUE;
    int _source_user_data_9327 = NOVALUE;
    int _5234 = NOVALUE;
    int _5233 = NOVALUE;
    int _5232 = NOVALUE;
    int _5231 = NOVALUE;
    int _5230 = NOVALUE;
    int _5228 = NOVALUE;
    int _5227 = NOVALUE;
    int _5226 = NOVALUE;
    int _5225 = NOVALUE;
    int _5224 = NOVALUE;
    int _5223 = NOVALUE;
    int _5222 = NOVALUE;
    int _5221 = NOVALUE;
    int _5220 = NOVALUE;
    int _5218 = NOVALUE;
    int _5216 = NOVALUE;
    int _5215 = NOVALUE;
    int _5214 = NOVALUE;
    int _5212 = NOVALUE;
    int _5211 = NOVALUE;
    int _5210 = NOVALUE;
    int _5206 = NOVALUE;
    int _5204 = NOVALUE;
    int _5200 = NOVALUE;
    int _5198 = NOVALUE;
    int _5197 = NOVALUE;
    int _5195 = NOVALUE;
    int _5192 = NOVALUE;
    int _5189 = NOVALUE;
    int _5188 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scan_subdirs_9319)) {
        _1 = (long)(DBL_PTR(_scan_subdirs_9319)->dbl);
        if (UNIQUE(DBL_PTR(_scan_subdirs_9319)) && (DBL_PTR(_scan_subdirs_9319)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scan_subdirs_9319);
        _scan_subdirs_9319 = _1;
    }

    /** 	sequence user_data = {path_name, 0}*/
    RefDS(_path_name_9317);
    DeRef(_user_data_9324);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_9317;
    ((int *)_2)[2] = 0;
    _user_data_9324 = MAKE_SEQ(_1);

    /** 	object source_user_data = ""*/
    RefDS(_5);
    DeRef(_source_user_data_9327);
    _source_user_data_9327 = _5;

    /** 	orig_func = your_function*/
    Ref(_your_function_9318);
    DeRef(_orig_func_9323);
    _orig_func_9323 = _your_function_9318;

    /** 	if sequence(your_function) then*/
    _5188 = IS_SEQUENCE(_your_function_9318);
    if (_5188 == 0)
    {
        _5188 = NOVALUE;
        goto L1; // [28] 48
    }
    else{
        _5188 = NOVALUE;
    }

    /** 		user_data = append(user_data, your_function[2])*/
    _2 = (int)SEQ_PTR(_your_function_9318);
    _5189 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_5189);
    Append(&_user_data_9324, _user_data_9324, _5189);
    _5189 = NOVALUE;

    /** 		your_function = your_function[1]*/
    _0 = _your_function_9318;
    _2 = (int)SEQ_PTR(_your_function_9318);
    _your_function_9318 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_your_function_9318);
    DeRef(_0);
L1: 

    /** 	source_orig_func = dir_source*/
    Ref(_dir_source_9320);
    DeRef(_source_orig_func_9326);
    _source_orig_func_9326 = _dir_source_9320;

    /** 	if sequence(dir_source) then*/
    _5192 = IS_SEQUENCE(_dir_source_9320);
    if (_5192 == 0)
    {
        _5192 = NOVALUE;
        goto L2; // [58] 74
    }
    else{
        _5192 = NOVALUE;
    }

    /** 		source_user_data = dir_source[2]*/
    DeRef(_source_user_data_9327);
    _2 = (int)SEQ_PTR(_dir_source_9320);
    _source_user_data_9327 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_source_user_data_9327);

    /** 		dir_source = dir_source[1]*/
    _0 = _dir_source_9320;
    _2 = (int)SEQ_PTR(_dir_source_9320);
    _dir_source_9320 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_dir_source_9320);
    DeRef(_0);
L2: 

    /** 	if not equal(dir_source, types:NO_ROUTINE_ID) then*/
    if (_dir_source_9320 == -99999)
    _5195 = 1;
    else if (IS_ATOM_INT(_dir_source_9320) && IS_ATOM_INT(-99999))
    _5195 = 0;
    else
    _5195 = (compare(_dir_source_9320, -99999) == 0);
    if (_5195 != 0)
    goto L3; // [80] 118
    _5195 = NOVALUE;

    /** 		if atom(source_orig_func) then*/
    _5197 = IS_ATOM(_source_orig_func_9326);
    if (_5197 == 0)
    {
        _5197 = NOVALUE;
        goto L4; // [88] 104
    }
    else{
        _5197 = NOVALUE;
    }

    /** 			d = call_func(dir_source, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_9317);
    *((int *)(_2+4)) = _path_name_9317;
    _5198 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_5198);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_9320].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_9321);
    _d_9321 = _1;
    DeRefDS(_5198);
    _5198 = NOVALUE;
    goto L5; // [101] 148
L4: 

    /** 			d = call_func(dir_source, {path_name, source_user_data})*/
    Ref(_source_user_data_9327);
    RefDS(_path_name_9317);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_9317;
    ((int *)_2)[2] = _source_user_data_9327;
    _5200 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_5200);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_9320].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8)
                         );
    DeRef(_d_9321);
    _d_9321 = _1;
    DeRefDS(_5200);
    _5200 = NOVALUE;
    goto L5; // [115] 148
L3: 

    /** 	elsif my_dir = DEFAULT_DIR_SOURCE then*/

    /** 		d = machine_func(M_DIR, path_name)*/
    DeRef(_d_9321);
    _d_9321 = machine(22, _path_name_9317);
    goto L5; // [132] 148

    /** 		d = call_func(my_dir, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_9317);
    *((int *)(_2+4)) = _path_name_9317;
    _5204 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_5204);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[-2].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_9321);
    _d_9321 = _1;
    DeRefDS(_5204);
    _5204 = NOVALUE;
L5: 

    /** 	if atom(d) then*/
    _5206 = IS_ATOM(_d_9321);
    if (_5206 == 0)
    {
        _5206 = NOVALUE;
        goto L6; // [155] 165
    }
    else{
        _5206 = NOVALUE;
    }

    /** 		return W_BAD_PATH*/
    DeRefDS(_path_name_9317);
    DeRef(_your_function_9318);
    DeRef(_dir_source_9320);
    DeRef(_d_9321);
    DeRef(_abort_now_9322);
    DeRef(_orig_func_9323);
    DeRef(_user_data_9324);
    DeRef(_source_orig_func_9326);
    DeRef(_source_user_data_9327);
    return -1;
L6: 

    /** 	ifdef not UNIX then*/

    /** 		path_name = match_replace('/', path_name, '\\')*/
    RefDS(_path_name_9317);
    _0 = _path_name_9317;
    _path_name_9317 = _6match_replace(47, _path_name_9317, 92, 0);
    DeRefDS(_0);

    /** 	path_name = text:trim_tail(path_name, {' ', SLASH, '\n'})*/
    RefDS(_path_name_9317);
    RefDS(_5208);
    _0 = _path_name_9317;
    _path_name_9317 = _16trim_tail(_path_name_9317, _5208, 0);
    DeRefDS(_0);

    /** 	user_data[1] = path_name*/
    RefDS(_path_name_9317);
    _2 = (int)SEQ_PTR(_user_data_9324);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _path_name_9317;
    DeRef(_1);

    /** 	for i = 1 to length(d) do*/
    if (IS_SEQUENCE(_d_9321)){
            _5210 = SEQ_PTR(_d_9321)->length;
    }
    else {
        _5210 = 1;
    }
    {
        int _i_9360;
        _i_9360 = 1;
L7: 
        if (_i_9360 > _5210){
            goto L8; // [199] 366
        }

        /** 		if eu:find(d[i][D_NAME], {".", ".."}) then*/
        _2 = (int)SEQ_PTR(_d_9321);
        _5211 = (int)*(((s1_ptr)_2)->base + _i_9360);
        _2 = (int)SEQ_PTR(_5211);
        _5212 = (int)*(((s1_ptr)_2)->base + 1);
        _5211 = NOVALUE;
        RefDS(_5213);
        RefDS(_5182);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _5182;
        ((int *)_2)[2] = _5213;
        _5214 = MAKE_SEQ(_1);
        _5215 = find_from(_5212, _5214, 1);
        _5212 = NOVALUE;
        DeRefDS(_5214);
        _5214 = NOVALUE;
        if (_5215 == 0)
        {
            _5215 = NOVALUE;
            goto L9; // [227] 235
        }
        else{
            _5215 = NOVALUE;
        }

        /** 			continue*/
        goto LA; // [232] 361
L9: 

        /** 		user_data[2] = d[i]*/
        _2 = (int)SEQ_PTR(_d_9321);
        _5216 = (int)*(((s1_ptr)_2)->base + _i_9360);
        Ref(_5216);
        _2 = (int)SEQ_PTR(_user_data_9324);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _5216;
        if( _1 != _5216 ){
            DeRef(_1);
        }
        _5216 = NOVALUE;

        /** 		abort_now = call_func(your_function, user_data)*/
        _1 = (int)SEQ_PTR(_user_data_9324);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_your_function_9318].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
        }
        DeRef(_abort_now_9322);
        _abort_now_9322 = _1;

        /** 		if not equal(abort_now, 0) then*/
        if (_abort_now_9322 == 0)
        _5218 = 1;
        else if (IS_ATOM_INT(_abort_now_9322) && IS_ATOM_INT(0))
        _5218 = 0;
        else
        _5218 = (compare(_abort_now_9322, 0) == 0);
        if (_5218 != 0)
        goto LB; // [257] 267
        _5218 = NOVALUE;

        /** 			return abort_now*/
        DeRefDS(_path_name_9317);
        DeRef(_your_function_9318);
        DeRef(_dir_source_9320);
        DeRef(_d_9321);
        DeRef(_orig_func_9323);
        DeRefDS(_user_data_9324);
        DeRef(_source_orig_func_9326);
        DeRef(_source_user_data_9327);
        return _abort_now_9322;
LB: 

        /** 		if eu:find('d', d[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_d_9321);
        _5220 = (int)*(((s1_ptr)_2)->base + _i_9360);
        _2 = (int)SEQ_PTR(_5220);
        _5221 = (int)*(((s1_ptr)_2)->base + 2);
        _5220 = NOVALUE;
        _5222 = find_from(100, _5221, 1);
        _5221 = NOVALUE;
        if (_5222 == 0)
        {
            _5222 = NOVALUE;
            goto LC; // [284] 359
        }
        else{
            _5222 = NOVALUE;
        }

        /** 			if scan_subdirs then*/
        if (_scan_subdirs_9319 == 0)
        {
            goto LD; // [289] 358
        }
        else{
        }

        /** 				abort_now = walk_dir(path_name & SLASH & d[i][D_NAME],*/
        _2 = (int)SEQ_PTR(_d_9321);
        _5223 = (int)*(((s1_ptr)_2)->base + _i_9360);
        _2 = (int)SEQ_PTR(_5223);
        _5224 = (int)*(((s1_ptr)_2)->base + 1);
        _5223 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _5224;
            concat_list[1] = 92;
            concat_list[2] = _path_name_9317;
            Concat_N((object_ptr)&_5225, concat_list, 3);
        }
        _5224 = NOVALUE;
        Ref(_orig_func_9323);
        DeRef(_5226);
        _5226 = _orig_func_9323;
        DeRef(_5227);
        _5227 = _scan_subdirs_9319;
        Ref(_source_orig_func_9326);
        DeRef(_5228);
        _5228 = _source_orig_func_9326;
        _0 = _abort_now_9322;
        _abort_now_9322 = _8walk_dir(_5225, _5226, _5227, _5228);
        DeRef(_0);
        _5225 = NOVALUE;
        _5226 = NOVALUE;
        _5227 = NOVALUE;
        _5228 = NOVALUE;

        /** 				if not equal(abort_now, 0) and */
        if (_abort_now_9322 == 0)
        _5230 = 1;
        else if (IS_ATOM_INT(_abort_now_9322) && IS_ATOM_INT(0))
        _5230 = 0;
        else
        _5230 = (compare(_abort_now_9322, 0) == 0);
        _5231 = (_5230 == 0);
        _5230 = NOVALUE;
        if (_5231 == 0) {
            goto LE; // [335] 357
        }
        if (_abort_now_9322 == -1)
        _5233 = 1;
        else if (IS_ATOM_INT(_abort_now_9322) && IS_ATOM_INT(-1))
        _5233 = 0;
        else
        _5233 = (compare(_abort_now_9322, -1) == 0);
        _5234 = (_5233 == 0);
        _5233 = NOVALUE;
        if (_5234 == 0)
        {
            DeRef(_5234);
            _5234 = NOVALUE;
            goto LE; // [347] 357
        }
        else{
            DeRef(_5234);
            _5234 = NOVALUE;
        }

        /** 					return abort_now*/
        DeRefDS(_path_name_9317);
        DeRef(_your_function_9318);
        DeRef(_dir_source_9320);
        DeRef(_d_9321);
        DeRef(_orig_func_9323);
        DeRef(_user_data_9324);
        DeRef(_source_orig_func_9326);
        DeRef(_source_user_data_9327);
        DeRef(_5231);
        _5231 = NOVALUE;
        return _abort_now_9322;
LE: 
LD: 
LC: 

        /** 	end for*/
LA: 
        _i_9360 = _i_9360 + 1;
        goto L7; // [361] 206
L8: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_path_name_9317);
    DeRef(_your_function_9318);
    DeRef(_dir_source_9320);
    DeRef(_d_9321);
    DeRef(_abort_now_9322);
    DeRef(_orig_func_9323);
    DeRef(_user_data_9324);
    DeRef(_source_orig_func_9326);
    DeRef(_source_user_data_9327);
    DeRef(_5231);
    _5231 = NOVALUE;
    return 0;
    ;
}


int _8create_directory(int _name_9393, int _mode_9394, int _mkparent_9396)
{
    int _pname_9397 = NOVALUE;
    int _ret_9398 = NOVALUE;
    int _pos_9399 = NOVALUE;
    int _5254 = NOVALUE;
    int _5251 = NOVALUE;
    int _5250 = NOVALUE;
    int _5249 = NOVALUE;
    int _5248 = NOVALUE;
    int _5245 = NOVALUE;
    int _5242 = NOVALUE;
    int _5241 = NOVALUE;
    int _5239 = NOVALUE;
    int _5238 = NOVALUE;
    int _5236 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_mode_9394)) {
        _1 = (long)(DBL_PTR(_mode_9394)->dbl);
        if (UNIQUE(DBL_PTR(_mode_9394)) && (DBL_PTR(_mode_9394)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mode_9394);
        _mode_9394 = _1;
    }
    if (!IS_ATOM_INT(_mkparent_9396)) {
        _1 = (long)(DBL_PTR(_mkparent_9396)->dbl);
        if (UNIQUE(DBL_PTR(_mkparent_9396)) && (DBL_PTR(_mkparent_9396)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mkparent_9396);
        _mkparent_9396 = _1;
    }

    /** 	if length(name) = 0 then*/
    if (IS_SEQUENCE(_name_9393)){
            _5236 = SEQ_PTR(_name_9393)->length;
    }
    else {
        _5236 = 1;
    }
    if (_5236 != 0)
    goto L1; // [16] 27

    /** 		return 0 -- failed*/
    DeRefDS(_name_9393);
    DeRef(_pname_9397);
    DeRef(_ret_9398);
    return 0;
L1: 

    /** 	if name[$] = SLASH then*/
    if (IS_SEQUENCE(_name_9393)){
            _5238 = SEQ_PTR(_name_9393)->length;
    }
    else {
        _5238 = 1;
    }
    _2 = (int)SEQ_PTR(_name_9393);
    _5239 = (int)*(((s1_ptr)_2)->base + _5238);
    if (binary_op_a(NOTEQ, _5239, 92)){
        _5239 = NOVALUE;
        goto L2; // [36] 55
    }
    _5239 = NOVALUE;

    /** 		name = name[1 .. $-1]*/
    if (IS_SEQUENCE(_name_9393)){
            _5241 = SEQ_PTR(_name_9393)->length;
    }
    else {
        _5241 = 1;
    }
    _5242 = _5241 - 1;
    _5241 = NOVALUE;
    rhs_slice_target = (object_ptr)&_name_9393;
    RHS_Slice(_name_9393, 1, _5242);
L2: 

    /** 	if mkparent != 0 then*/
    if (_mkparent_9396 == 0)
    goto L3; // [57] 107

    /** 		pos = search:rfind(SLASH, name)*/
    if (IS_SEQUENCE(_name_9393)){
            _5245 = SEQ_PTR(_name_9393)->length;
    }
    else {
        _5245 = 1;
    }
    RefDS(_name_9393);
    _pos_9399 = _6rfind(92, _name_9393, _5245);
    _5245 = NOVALUE;
    if (!IS_ATOM_INT(_pos_9399)) {
        _1 = (long)(DBL_PTR(_pos_9399)->dbl);
        if (UNIQUE(DBL_PTR(_pos_9399)) && (DBL_PTR(_pos_9399)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_9399);
        _pos_9399 = _1;
    }

    /** 		if pos != 0 then*/
    if (_pos_9399 == 0)
    goto L4; // [78] 106

    /** 			ret = create_directory(name[1.. pos-1], mode, mkparent)*/
    _5248 = _pos_9399 - 1;
    rhs_slice_target = (object_ptr)&_5249;
    RHS_Slice(_name_9393, 1, _5248);
    DeRef(_5250);
    _5250 = _mode_9394;
    DeRef(_5251);
    _5251 = _mkparent_9396;
    _0 = _ret_9398;
    _ret_9398 = _8create_directory(_5249, _5250, _5251);
    DeRef(_0);
    _5249 = NOVALUE;
    _5250 = NOVALUE;
    _5251 = NOVALUE;
L4: 
L3: 

    /** 	pname = machine:allocate_string(name)*/
    RefDS(_name_9393);
    _0 = _pname_9397;
    _pname_9397 = _11allocate_string(_name_9393, 0);
    DeRef(_0);

    /** 	ifdef UNIX then*/

    /** 		ret = c_func(xCreateDirectory, {pname, 0})*/
    Ref(_pname_9397);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pname_9397;
    ((int *)_2)[2] = 0;
    _5254 = MAKE_SEQ(_1);
    DeRef(_ret_9398);
    _ret_9398 = call_c(1, _8xCreateDirectory_9222, _5254);
    DeRefDS(_5254);
    _5254 = NOVALUE;

    /** 		mode = mode -- get rid of not used warning*/
    _mode_9394 = _mode_9394;

    /** 	return ret*/
    DeRefDS(_name_9393);
    DeRef(_pname_9397);
    DeRef(_5242);
    _5242 = NOVALUE;
    DeRef(_5248);
    _5248 = NOVALUE;
    return _ret_9398;
    ;
}


int _8create_file(int _name_9426)
{
    int _fh_9427 = NOVALUE;
    int _ret_9429 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fh = open(name, "wb")*/
    _fh_9427 = EOpen(_name_9426, _2908, 0);

    /** 	integer ret = (fh != -1)*/
    _ret_9429 = (_fh_9427 != -1);

    /** 	if ret then*/
    if (_ret_9429 == 0)
    {
        goto L1; // [22] 30
    }
    else{
    }

    /** 		close(fh)*/
    EClose(_fh_9427);
L1: 

    /** 	return ret*/
    DeRefDS(_name_9426);
    return _ret_9429;
    ;
}


int _8delete_file(int _name_9434)
{
    int _pfilename_9435 = NOVALUE;
    int _success_9437 = NOVALUE;
    int _5259 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom pfilename = machine:allocate_string(name)*/
    RefDS(_name_9434);
    _0 = _pfilename_9435;
    _pfilename_9435 = _11allocate_string(_name_9434, 0);
    DeRef(_0);

    /** 	integer success = c_func(xDeleteFile, {pfilename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pfilename_9435);
    *((int *)(_2+4)) = _pfilename_9435;
    _5259 = MAKE_SEQ(_1);
    _success_9437 = call_c(1, _8xDeleteFile_9218, _5259);
    DeRefDS(_5259);
    _5259 = NOVALUE;
    if (!IS_ATOM_INT(_success_9437)) {
        _1 = (long)(DBL_PTR(_success_9437)->dbl);
        if (UNIQUE(DBL_PTR(_success_9437)) && (DBL_PTR(_success_9437)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_9437);
        _success_9437 = _1;
    }

    /** 	ifdef UNIX then*/

    /** 	machine:free(pfilename)*/
    Ref(_pfilename_9435);
    _11free(_pfilename_9435);

    /** 	return success*/
    DeRefDS(_name_9434);
    DeRef(_pfilename_9435);
    return _success_9437;
    ;
}


int _8curdir(int _drive_id_9442)
{
    int _lCurDir_9443 = NOVALUE;
    int _lOrigDir_9444 = NOVALUE;
    int _lDrive_9445 = NOVALUE;
    int _current_dir_inlined_current_dir_at_27_9450 = NOVALUE;
    int _chdir_inlined_chdir_at_57_9453 = NOVALUE;
    int _current_dir_inlined_current_dir_at_79_9456 = NOVALUE;
    int _chdir_inlined_chdir_at_111_9463 = NOVALUE;
    int _newdir_inlined_chdir_at_108_9462 = NOVALUE;
    int _5267 = NOVALUE;
    int _5266 = NOVALUE;
    int _5265 = NOVALUE;
    int _5263 = NOVALUE;
    int _5261 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_drive_id_9442)) {
        _1 = (long)(DBL_PTR(_drive_id_9442)->dbl);
        if (UNIQUE(DBL_PTR(_drive_id_9442)) && (DBL_PTR(_drive_id_9442)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_drive_id_9442);
        _drive_id_9442 = _1;
    }

    /** 	ifdef not LINUX then*/

    /** 	    sequence lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_9444);
    _lOrigDir_9444 = _5;

    /** 	    sequence lDrive*/

    /** 	    if t_alpha(drive_id) then*/
    _5261 = _5t_alpha(_drive_id_9442);
    if (_5261 == 0) {
        DeRef(_5261);
        _5261 = NOVALUE;
        goto L1; // [22] 77
    }
    else {
        if (!IS_ATOM_INT(_5261) && DBL_PTR(_5261)->dbl == 0.0){
            DeRef(_5261);
            _5261 = NOVALUE;
            goto L1; // [22] 77
        }
        DeRef(_5261);
        _5261 = NOVALUE;
    }
    DeRef(_5261);
    _5261 = NOVALUE;

    /** 		    lOrigDir =  current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefDSi(_lOrigDir_9444);
    _lOrigDir_9444 = machine(23, 0);

    /** 		    lDrive = "  "*/
    RefDS(_223);
    DeRefi(_lDrive_9445);
    _lDrive_9445 = _223;

    /** 		    lDrive[1] = drive_id*/
    _2 = (int)SEQ_PTR(_lDrive_9445);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_9445 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _drive_id_9442;

    /** 		    lDrive[2] = ':'*/
    _2 = (int)SEQ_PTR(_lDrive_9445);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_9445 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    *(int *)_2 = 58;

    /** 		    if chdir(lDrive) = 0 then*/

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_57_9453 = machine(63, _lDrive_9445);
    if (_chdir_inlined_chdir_at_57_9453 != 0)
    goto L2; // [64] 76

    /** 		    	lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_9444);
    _lOrigDir_9444 = _5;
L2: 
L1: 

    /**     lCurDir = current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_lCurDir_9443);
    _lCurDir_9443 = machine(23, 0);

    /** 	ifdef not LINUX then*/

    /** 		if length(lOrigDir) > 0 then*/
    if (IS_SEQUENCE(_lOrigDir_9444)){
            _5263 = SEQ_PTR(_lOrigDir_9444)->length;
    }
    else {
        _5263 = 1;
    }
    if (_5263 <= 0)
    goto L3; // [97] 123

    /** 	    	chdir(lOrigDir[1..2])*/
    rhs_slice_target = (object_ptr)&_5265;
    RHS_Slice(_lOrigDir_9444, 1, 2);
    DeRefi(_newdir_inlined_chdir_at_108_9462);
    _newdir_inlined_chdir_at_108_9462 = _5265;
    _5265 = NOVALUE;

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_111_9463 = machine(63, _newdir_inlined_chdir_at_108_9462);
    DeRefi(_newdir_inlined_chdir_at_108_9462);
    _newdir_inlined_chdir_at_108_9462 = NOVALUE;
L3: 

    /** 	if (lCurDir[$] != SLASH) then*/
    if (IS_SEQUENCE(_lCurDir_9443)){
            _5266 = SEQ_PTR(_lCurDir_9443)->length;
    }
    else {
        _5266 = 1;
    }
    _2 = (int)SEQ_PTR(_lCurDir_9443);
    _5267 = (int)*(((s1_ptr)_2)->base + _5266);
    if (_5267 == 92)
    goto L4; // [132] 143

    /** 		lCurDir &= SLASH*/
    Append(&_lCurDir_9443, _lCurDir_9443, 92);
L4: 

    /** 	return lCurDir*/
    DeRefi(_lOrigDir_9444);
    DeRefi(_lDrive_9445);
    _5267 = NOVALUE;
    return _lCurDir_9443;
    ;
}


int _8init_curdir()
{
    int _0, _1, _2;
    

    /** 	return InitCurDir*/
    RefDS(_8InitCurDir_9469);
    return _8InitCurDir_9469;
    ;
}


int _8clear_directory(int _path_9475, int _recurse_9476)
{
    int _files_9477 = NOVALUE;
    int _ret_9478 = NOVALUE;
    int _dir_inlined_dir_at_91_9499 = NOVALUE;
    int _cnt_9524 = NOVALUE;
    int _5311 = NOVALUE;
    int _5310 = NOVALUE;
    int _5309 = NOVALUE;
    int _5308 = NOVALUE;
    int _5304 = NOVALUE;
    int _5303 = NOVALUE;
    int _5302 = NOVALUE;
    int _5301 = NOVALUE;
    int _5300 = NOVALUE;
    int _5299 = NOVALUE;
    int _5298 = NOVALUE;
    int _5297 = NOVALUE;
    int _5294 = NOVALUE;
    int _5293 = NOVALUE;
    int _5292 = NOVALUE;
    int _5290 = NOVALUE;
    int _5289 = NOVALUE;
    int _5288 = NOVALUE;
    int _5286 = NOVALUE;
    int _5285 = NOVALUE;
    int _5283 = NOVALUE;
    int _5281 = NOVALUE;
    int _5279 = NOVALUE;
    int _5277 = NOVALUE;
    int _5276 = NOVALUE;
    int _5274 = NOVALUE;
    int _5273 = NOVALUE;
    int _5271 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recurse_9476)) {
        _1 = (long)(DBL_PTR(_recurse_9476)->dbl);
        if (UNIQUE(DBL_PTR(_recurse_9476)) && (DBL_PTR(_recurse_9476)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recurse_9476);
        _recurse_9476 = _1;
    }

    /** 	if length(path) > 0 then*/
    if (IS_SEQUENCE(_path_9475)){
            _5271 = SEQ_PTR(_path_9475)->length;
    }
    else {
        _5271 = 1;
    }
    if (_5271 <= 0)
    goto L1; // [12] 45

    /** 		if path[$] = SLASH then*/
    if (IS_SEQUENCE(_path_9475)){
            _5273 = SEQ_PTR(_path_9475)->length;
    }
    else {
        _5273 = 1;
    }
    _2 = (int)SEQ_PTR(_path_9475);
    _5274 = (int)*(((s1_ptr)_2)->base + _5273);
    if (binary_op_a(NOTEQ, _5274, 92)){
        _5274 = NOVALUE;
        goto L2; // [25] 44
    }
    _5274 = NOVALUE;

    /** 			path = path[1 .. $-1]*/
    if (IS_SEQUENCE(_path_9475)){
            _5276 = SEQ_PTR(_path_9475)->length;
    }
    else {
        _5276 = 1;
    }
    _5277 = _5276 - 1;
    _5276 = NOVALUE;
    rhs_slice_target = (object_ptr)&_path_9475;
    RHS_Slice(_path_9475, 1, _5277);
L2: 
L1: 

    /** 	if length(path) = 0 then*/
    if (IS_SEQUENCE(_path_9475)){
            _5279 = SEQ_PTR(_path_9475)->length;
    }
    else {
        _5279 = 1;
    }
    if (_5279 != 0)
    goto L3; // [50] 61

    /** 		return 0 -- Nothing specified to clear. Not safe to assume anything.*/
    DeRefDS(_path_9475);
    DeRef(_files_9477);
    DeRef(_5277);
    _5277 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(path) = 2 then*/
    if (IS_SEQUENCE(_path_9475)){
            _5281 = SEQ_PTR(_path_9475)->length;
    }
    else {
        _5281 = 1;
    }
    if (_5281 != 2)
    goto L4; // [68] 90

    /** 			if path[2] = ':' then*/
    _2 = (int)SEQ_PTR(_path_9475);
    _5283 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5283, 58)){
        _5283 = NOVALUE;
        goto L5; // [78] 89
    }
    _5283 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_path_9475);
    DeRef(_files_9477);
    DeRef(_5277);
    _5277 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(path)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_9477);
    _files_9477 = machine(22, _path_9475);

    /** 	if atom(files) then*/
    _5285 = IS_ATOM(_files_9477);
    if (_5285 == 0)
    {
        _5285 = NOVALUE;
        goto L6; // [106] 116
    }
    else{
        _5285 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_path_9475);
    DeRef(_files_9477);
    DeRef(_5277);
    _5277 = NOVALUE;
    return 0;
L6: 

    /** 	ifdef WINDOWS then*/

    /** 		if length( files ) < 3 then*/
    if (IS_SEQUENCE(_files_9477)){
            _5286 = SEQ_PTR(_files_9477)->length;
    }
    else {
        _5286 = 1;
    }
    if (_5286 >= 3)
    goto L7; // [123] 134

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_9475);
    DeRef(_files_9477);
    DeRef(_5277);
    _5277 = NOVALUE;
    return 0;
L7: 

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_9477);
    _5288 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5288);
    _5289 = (int)*(((s1_ptr)_2)->base + 1);
    _5288 = NOVALUE;
    if (_5289 == _5182)
    _5290 = 1;
    else if (IS_ATOM_INT(_5289) && IS_ATOM_INT(_5182))
    _5290 = 0;
    else
    _5290 = (compare(_5289, _5182) == 0);
    _5289 = NOVALUE;
    if (_5290 != 0)
    goto L8; // [150] 160
    _5290 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_9475);
    DeRef(_files_9477);
    DeRef(_5277);
    _5277 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_9477);
    _5292 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5292);
    _5293 = (int)*(((s1_ptr)_2)->base + 2);
    _5292 = NOVALUE;
    _5294 = find_from(100, _5293, 1);
    _5293 = NOVALUE;
    if (_5294 != 0)
    goto L9; // [177] 187
    _5294 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_9475);
    DeRef(_files_9477);
    DeRef(_5277);
    _5277 = NOVALUE;
    return 0;
L9: 

    /** 	ret = 1*/
    _ret_9478 = 1;

    /** 	path &= SLASH*/
    Append(&_path_9475, _path_9475, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_9477)){
            _5297 = SEQ_PTR(_files_9477)->length;
    }
    else {
        _5297 = 1;
    }
    {
        int _i_9517;
        _i_9517 = 3;
LA: 
        if (_i_9517 > _5297){
            goto LB; // [207] 348
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_9477);
        _5298 = (int)*(((s1_ptr)_2)->base + _i_9517);
        _2 = (int)SEQ_PTR(_5298);
        _5299 = (int)*(((s1_ptr)_2)->base + 2);
        _5298 = NOVALUE;
        _5300 = find_from(100, _5299, 1);
        _5299 = NOVALUE;
        if (_5300 == 0)
        {
            _5300 = NOVALUE;
            goto LC; // [231] 301
        }
        else{
            _5300 = NOVALUE;
        }

        /** 				if recurse then*/
        if (_recurse_9476 == 0)
        {
            goto LD; // [236] 343
        }
        else{
        }

        /** 					integer cnt = clear_directory(path & files[i][D_NAME], recurse)*/
        _2 = (int)SEQ_PTR(_files_9477);
        _5301 = (int)*(((s1_ptr)_2)->base + _i_9517);
        _2 = (int)SEQ_PTR(_5301);
        _5302 = (int)*(((s1_ptr)_2)->base + 1);
        _5301 = NOVALUE;
        if (IS_SEQUENCE(_path_9475) && IS_ATOM(_5302)) {
            Ref(_5302);
            Append(&_5303, _path_9475, _5302);
        }
        else if (IS_ATOM(_path_9475) && IS_SEQUENCE(_5302)) {
        }
        else {
            Concat((object_ptr)&_5303, _path_9475, _5302);
        }
        _5302 = NOVALUE;
        DeRef(_5304);
        _5304 = _recurse_9476;
        _cnt_9524 = _8clear_directory(_5303, _5304);
        _5303 = NOVALUE;
        _5304 = NOVALUE;
        if (!IS_ATOM_INT(_cnt_9524)) {
            _1 = (long)(DBL_PTR(_cnt_9524)->dbl);
            if (UNIQUE(DBL_PTR(_cnt_9524)) && (DBL_PTR(_cnt_9524)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_cnt_9524);
            _cnt_9524 = _1;
        }

        /** 					if cnt = 0 then*/
        if (_cnt_9524 != 0)
        goto LE; // [269] 280

        /** 						return 0*/
        DeRefDS(_path_9475);
        DeRef(_files_9477);
        DeRef(_5277);
        _5277 = NOVALUE;
        return 0;
LE: 

        /** 					ret += cnt*/
        _ret_9478 = _ret_9478 + _cnt_9524;
        goto LF; // [290] 341

        /** 					continue*/
        goto LD; // [295] 343
        goto LF; // [298] 341
LC: 

        /** 				if delete_file(path & files[i][D_NAME]) = 0 then*/
        _2 = (int)SEQ_PTR(_files_9477);
        _5308 = (int)*(((s1_ptr)_2)->base + _i_9517);
        _2 = (int)SEQ_PTR(_5308);
        _5309 = (int)*(((s1_ptr)_2)->base + 1);
        _5308 = NOVALUE;
        if (IS_SEQUENCE(_path_9475) && IS_ATOM(_5309)) {
            Ref(_5309);
            Append(&_5310, _path_9475, _5309);
        }
        else if (IS_ATOM(_path_9475) && IS_SEQUENCE(_5309)) {
        }
        else {
            Concat((object_ptr)&_5310, _path_9475, _5309);
        }
        _5309 = NOVALUE;
        _5311 = _8delete_file(_5310);
        _5310 = NOVALUE;
        if (binary_op_a(NOTEQ, _5311, 0)){
            DeRef(_5311);
            _5311 = NOVALUE;
            goto L10; // [321] 332
        }
        DeRef(_5311);
        _5311 = NOVALUE;

        /** 					return 0*/
        DeRefDS(_path_9475);
        DeRef(_files_9477);
        DeRef(_5277);
        _5277 = NOVALUE;
        return 0;
L10: 

        /** 				ret += 1*/
        _ret_9478 = _ret_9478 + 1;
LF: 

        /** 		end for*/
LD: 
        _i_9517 = _i_9517 + 1;
        goto LA; // [343] 214
LB: 
        ;
    }

    /** 	return ret*/
    DeRefDS(_path_9475);
    DeRef(_files_9477);
    DeRef(_5277);
    _5277 = NOVALUE;
    return _ret_9478;
    ;
}


int _8remove_directory(int _dir_name_9544, int _force_9545)
{
    int _pname_9546 = NOVALUE;
    int _ret_9547 = NOVALUE;
    int _files_9548 = NOVALUE;
    int _D_NAME_9549 = NOVALUE;
    int _D_ATTRIBUTES_9550 = NOVALUE;
    int _dir_inlined_dir_at_103_9571 = NOVALUE;
    int _5358 = NOVALUE;
    int _5354 = NOVALUE;
    int _5353 = NOVALUE;
    int _5352 = NOVALUE;
    int _5350 = NOVALUE;
    int _5349 = NOVALUE;
    int _5348 = NOVALUE;
    int _5347 = NOVALUE;
    int _5346 = NOVALUE;
    int _5345 = NOVALUE;
    int _5344 = NOVALUE;
    int _5343 = NOVALUE;
    int _5339 = NOVALUE;
    int _5337 = NOVALUE;
    int _5336 = NOVALUE;
    int _5335 = NOVALUE;
    int _5333 = NOVALUE;
    int _5332 = NOVALUE;
    int _5331 = NOVALUE;
    int _5329 = NOVALUE;
    int _5328 = NOVALUE;
    int _5326 = NOVALUE;
    int _5324 = NOVALUE;
    int _5322 = NOVALUE;
    int _5320 = NOVALUE;
    int _5319 = NOVALUE;
    int _5317 = NOVALUE;
    int _5316 = NOVALUE;
    int _5314 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_force_9545)) {
        _1 = (long)(DBL_PTR(_force_9545)->dbl);
        if (UNIQUE(DBL_PTR(_force_9545)) && (DBL_PTR(_force_9545)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_force_9545);
        _force_9545 = _1;
    }

    /** 	integer D_NAME = 1, D_ATTRIBUTES = 2*/
    _D_NAME_9549 = 1;
    _D_ATTRIBUTES_9550 = 2;

    /**  	if length(dir_name) > 0 then*/
    if (IS_SEQUENCE(_dir_name_9544)){
            _5314 = SEQ_PTR(_dir_name_9544)->length;
    }
    else {
        _5314 = 1;
    }
    if (_5314 <= 0)
    goto L1; // [24] 57

    /** 		if dir_name[$] = SLASH then*/
    if (IS_SEQUENCE(_dir_name_9544)){
            _5316 = SEQ_PTR(_dir_name_9544)->length;
    }
    else {
        _5316 = 1;
    }
    _2 = (int)SEQ_PTR(_dir_name_9544);
    _5317 = (int)*(((s1_ptr)_2)->base + _5316);
    if (binary_op_a(NOTEQ, _5317, 92)){
        _5317 = NOVALUE;
        goto L2; // [37] 56
    }
    _5317 = NOVALUE;

    /** 			dir_name = dir_name[1 .. $-1]*/
    if (IS_SEQUENCE(_dir_name_9544)){
            _5319 = SEQ_PTR(_dir_name_9544)->length;
    }
    else {
        _5319 = 1;
    }
    _5320 = _5319 - 1;
    _5319 = NOVALUE;
    rhs_slice_target = (object_ptr)&_dir_name_9544;
    RHS_Slice(_dir_name_9544, 1, _5320);
L2: 
L1: 

    /** 	if length(dir_name) = 0 then*/
    if (IS_SEQUENCE(_dir_name_9544)){
            _5322 = SEQ_PTR(_dir_name_9544)->length;
    }
    else {
        _5322 = 1;
    }
    if (_5322 != 0)
    goto L3; // [62] 73

    /** 		return 0	-- nothing specified to delete.*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_ret_9547);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(dir_name) = 2 then*/
    if (IS_SEQUENCE(_dir_name_9544)){
            _5324 = SEQ_PTR(_dir_name_9544)->length;
    }
    else {
        _5324 = 1;
    }
    if (_5324 != 2)
    goto L4; // [80] 102

    /** 			if dir_name[2] = ':' then*/
    _2 = (int)SEQ_PTR(_dir_name_9544);
    _5326 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5326, 58)){
        _5326 = NOVALUE;
        goto L5; // [90] 101
    }
    _5326 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_ret_9547);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(dir_name)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_9548);
    _files_9548 = machine(22, _dir_name_9544);

    /** 	if atom(files) then*/
    _5328 = IS_ATOM(_files_9548);
    if (_5328 == 0)
    {
        _5328 = NOVALUE;
        goto L6; // [118] 128
    }
    else{
        _5328 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_ret_9547);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return 0;
L6: 

    /** 	if length( files ) < 2 then*/
    if (IS_SEQUENCE(_files_9548)){
            _5329 = SEQ_PTR(_files_9548)->length;
    }
    else {
        _5329 = 1;
    }
    if (_5329 >= 2)
    goto L7; // [133] 144

    /** 		return 0	-- Supplied dir_name was not a directory*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_ret_9547);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return 0;
L7: 

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_9548);
    _5331 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5331);
    _5332 = (int)*(((s1_ptr)_2)->base + _D_NAME_9549);
    _5331 = NOVALUE;
    if (_5332 == _5182)
    _5333 = 1;
    else if (IS_ATOM_INT(_5332) && IS_ATOM_INT(_5182))
    _5333 = 0;
    else
    _5333 = (compare(_5332, _5182) == 0);
    _5332 = NOVALUE;
    if (_5333 != 0)
    goto L8; // [160] 170
    _5333 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_ret_9547);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_9548);
    _5335 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5335);
    _5336 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_9550);
    _5335 = NOVALUE;
    _5337 = find_from(100, _5336, 1);
    _5336 = NOVALUE;
    if (_5337 != 0)
    goto L9; // [185] 195
    _5337 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_ret_9547);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return 0;
L9: 

    /** 		if length(files) > 2 then*/
    if (IS_SEQUENCE(_files_9548)){
            _5339 = SEQ_PTR(_files_9548)->length;
    }
    else {
        _5339 = 1;
    }
    if (_5339 <= 2)
    goto LA; // [200] 217

    /** 			if not force then*/
    if (_force_9545 != 0)
    goto LB; // [206] 216

    /** 				return 0 -- Directory is not already emptied.*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_ret_9547);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return 0;
LB: 
LA: 

    /** 	dir_name &= SLASH*/
    Append(&_dir_name_9544, _dir_name_9544, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_9548)){
            _5343 = SEQ_PTR(_files_9548)->length;
    }
    else {
        _5343 = 1;
    }
    {
        int _i_9594;
        _i_9594 = 3;
LC: 
        if (_i_9594 > _5343){
            goto LD; // [230] 322
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_9548);
        _5344 = (int)*(((s1_ptr)_2)->base + _i_9594);
        _2 = (int)SEQ_PTR(_5344);
        _5345 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_9550);
        _5344 = NOVALUE;
        _5346 = find_from(100, _5345, 1);
        _5345 = NOVALUE;
        if (_5346 == 0)
        {
            _5346 = NOVALUE;
            goto LE; // [252] 282
        }
        else{
            _5346 = NOVALUE;
        }

        /** 				ret = remove_directory(dir_name & files[i][D_NAME] & SLASH, force)*/
        _2 = (int)SEQ_PTR(_files_9548);
        _5347 = (int)*(((s1_ptr)_2)->base + _i_9594);
        _2 = (int)SEQ_PTR(_5347);
        _5348 = (int)*(((s1_ptr)_2)->base + _D_NAME_9549);
        _5347 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = 92;
            concat_list[1] = _5348;
            concat_list[2] = _dir_name_9544;
            Concat_N((object_ptr)&_5349, concat_list, 3);
        }
        _5348 = NOVALUE;
        DeRef(_5350);
        _5350 = _force_9545;
        _0 = _ret_9547;
        _ret_9547 = _8remove_directory(_5349, _5350);
        DeRef(_0);
        _5349 = NOVALUE;
        _5350 = NOVALUE;
        goto LF; // [279] 301
LE: 

        /** 				ret = delete_file(dir_name & files[i][D_NAME])*/
        _2 = (int)SEQ_PTR(_files_9548);
        _5352 = (int)*(((s1_ptr)_2)->base + _i_9594);
        _2 = (int)SEQ_PTR(_5352);
        _5353 = (int)*(((s1_ptr)_2)->base + _D_NAME_9549);
        _5352 = NOVALUE;
        if (IS_SEQUENCE(_dir_name_9544) && IS_ATOM(_5353)) {
            Ref(_5353);
            Append(&_5354, _dir_name_9544, _5353);
        }
        else if (IS_ATOM(_dir_name_9544) && IS_SEQUENCE(_5353)) {
        }
        else {
            Concat((object_ptr)&_5354, _dir_name_9544, _5353);
        }
        _5353 = NOVALUE;
        _0 = _ret_9547;
        _ret_9547 = _8delete_file(_5354);
        DeRef(_0);
        _5354 = NOVALUE;
LF: 

        /** 			if not ret then*/
        if (IS_ATOM_INT(_ret_9547)) {
            if (_ret_9547 != 0){
                goto L10; // [305] 315
            }
        }
        else {
            if (DBL_PTR(_ret_9547)->dbl != 0.0){
                goto L10; // [305] 315
            }
        }

        /** 				return 0*/
        DeRefDS(_dir_name_9544);
        DeRef(_pname_9546);
        DeRef(_ret_9547);
        DeRef(_files_9548);
        DeRef(_5320);
        _5320 = NOVALUE;
        return 0;
L10: 

        /** 		end for*/
        _i_9594 = _i_9594 + 1;
        goto LC; // [317] 237
LD: 
        ;
    }

    /** 	pname = machine:allocate_string(dir_name)*/
    RefDS(_dir_name_9544);
    _0 = _pname_9546;
    _pname_9546 = _11allocate_string(_dir_name_9544, 0);
    DeRef(_0);

    /** 	ret = c_func(xRemoveDirectory, {pname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pname_9546);
    *((int *)(_2+4)) = _pname_9546;
    _5358 = MAKE_SEQ(_1);
    DeRef(_ret_9547);
    _ret_9547 = call_c(1, _8xRemoveDirectory_9229, _5358);
    DeRefDS(_5358);
    _5358 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free(pname)*/
    Ref(_pname_9546);
    _11free(_pname_9546);

    /** 	return ret*/
    DeRefDS(_dir_name_9544);
    DeRef(_pname_9546);
    DeRef(_files_9548);
    DeRef(_5320);
    _5320 = NOVALUE;
    return _ret_9547;
    ;
}


int _8pathinfo(int _path_9626, int _std_slash_9627)
{
    int _slash_9628 = NOVALUE;
    int _period_9629 = NOVALUE;
    int _ch_9630 = NOVALUE;
    int _dir_name_9631 = NOVALUE;
    int _file_name_9632 = NOVALUE;
    int _file_ext_9633 = NOVALUE;
    int _file_full_9634 = NOVALUE;
    int _drive_id_9635 = NOVALUE;
    int _from_slash_9675 = NOVALUE;
    int _5396 = NOVALUE;
    int _5389 = NOVALUE;
    int _5388 = NOVALUE;
    int _5385 = NOVALUE;
    int _5384 = NOVALUE;
    int _5382 = NOVALUE;
    int _5381 = NOVALUE;
    int _5378 = NOVALUE;
    int _5377 = NOVALUE;
    int _5375 = NOVALUE;
    int _5371 = NOVALUE;
    int _5369 = NOVALUE;
    int _5368 = NOVALUE;
    int _5367 = NOVALUE;
    int _5366 = NOVALUE;
    int _5364 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_std_slash_9627)) {
        _1 = (long)(DBL_PTR(_std_slash_9627)->dbl);
        if (UNIQUE(DBL_PTR(_std_slash_9627)) && (DBL_PTR(_std_slash_9627)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_std_slash_9627);
        _std_slash_9627 = _1;
    }

    /** 	dir_name  = ""*/
    RefDS(_5);
    DeRef(_dir_name_9631);
    _dir_name_9631 = _5;

    /** 	file_name = ""*/
    RefDS(_5);
    DeRef(_file_name_9632);
    _file_name_9632 = _5;

    /** 	file_ext  = ""*/
    RefDS(_5);
    DeRef(_file_ext_9633);
    _file_ext_9633 = _5;

    /** 	file_full = ""*/
    RefDS(_5);
    DeRef(_file_full_9634);
    _file_full_9634 = _5;

    /** 	drive_id  = ""*/
    RefDS(_5);
    DeRef(_drive_id_9635);
    _drive_id_9635 = _5;

    /** 	slash = 0*/
    _slash_9628 = 0;

    /** 	period = 0*/
    _period_9629 = 0;

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_9626)){
            _5364 = SEQ_PTR(_path_9626)->length;
    }
    else {
        _5364 = 1;
    }
    {
        int _i_9637;
        _i_9637 = _5364;
L1: 
        if (_i_9637 < 1){
            goto L2; // [61] 134
        }

        /** 		ch = path[i]*/
        _2 = (int)SEQ_PTR(_path_9626);
        _ch_9630 = (int)*(((s1_ptr)_2)->base + _i_9637);
        if (!IS_ATOM_INT(_ch_9630))
        _ch_9630 = (long)DBL_PTR(_ch_9630)->dbl;

        /** 		if period = 0 and ch = '.' then*/
        _5366 = (_period_9629 == 0);
        if (_5366 == 0) {
            goto L3; // [82] 104
        }
        _5368 = (_ch_9630 == 46);
        if (_5368 == 0)
        {
            DeRef(_5368);
            _5368 = NOVALUE;
            goto L3; // [91] 104
        }
        else{
            DeRef(_5368);
            _5368 = NOVALUE;
        }

        /** 			period = i*/
        _period_9629 = _i_9637;
        goto L4; // [101] 127
L3: 

        /** 		elsif eu:find(ch, SLASHES) then*/
        _5369 = find_from(_ch_9630, _8SLASHES_9246, 1);
        if (_5369 == 0)
        {
            _5369 = NOVALUE;
            goto L5; // [111] 126
        }
        else{
            _5369 = NOVALUE;
        }

        /** 			slash = i*/
        _slash_9628 = _i_9637;

        /** 			exit*/
        goto L2; // [123] 134
L5: 
L4: 

        /** 	end for*/
        _i_9637 = _i_9637 + -1;
        goto L1; // [129] 68
L2: 
        ;
    }

    /** 	if slash > 0 then*/
    if (_slash_9628 <= 0)
    goto L6; // [136] 195

    /** 		dir_name = path[1..slash-1]*/
    _5371 = _slash_9628 - 1;
    rhs_slice_target = (object_ptr)&_dir_name_9631;
    RHS_Slice(_path_9626, 1, _5371);

    /** 		ifdef not UNIX then*/

    /** 			ch = eu:find(':', dir_name)*/
    _ch_9630 = find_from(58, _dir_name_9631, 1);

    /** 			if ch != 0 then*/
    if (_ch_9630 == 0)
    goto L7; // [164] 194

    /** 				drive_id = dir_name[1..ch-1]*/
    _5375 = _ch_9630 - 1;
    rhs_slice_target = (object_ptr)&_drive_id_9635;
    RHS_Slice(_dir_name_9631, 1, _5375);

    /** 				dir_name = dir_name[ch+1..$]*/
    _5377 = _ch_9630 + 1;
    if (IS_SEQUENCE(_dir_name_9631)){
            _5378 = SEQ_PTR(_dir_name_9631)->length;
    }
    else {
        _5378 = 1;
    }
    rhs_slice_target = (object_ptr)&_dir_name_9631;
    RHS_Slice(_dir_name_9631, _5377, _5378);
L7: 
L6: 

    /** 	if period > 0 then*/
    if (_period_9629 <= 0)
    goto L8; // [197] 241

    /** 		file_name = path[slash+1..period-1]*/
    _5381 = _slash_9628 + 1;
    if (_5381 > MAXINT){
        _5381 = NewDouble((double)_5381);
    }
    _5382 = _period_9629 - 1;
    rhs_slice_target = (object_ptr)&_file_name_9632;
    RHS_Slice(_path_9626, _5381, _5382);

    /** 		file_ext = path[period+1..$]*/
    _5384 = _period_9629 + 1;
    if (_5384 > MAXINT){
        _5384 = NewDouble((double)_5384);
    }
    if (IS_SEQUENCE(_path_9626)){
            _5385 = SEQ_PTR(_path_9626)->length;
    }
    else {
        _5385 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_ext_9633;
    RHS_Slice(_path_9626, _5384, _5385);

    /** 		file_full = file_name & '.' & file_ext*/
    {
        int concat_list[3];

        concat_list[0] = _file_ext_9633;
        concat_list[1] = 46;
        concat_list[2] = _file_name_9632;
        Concat_N((object_ptr)&_file_full_9634, concat_list, 3);
    }
    goto L9; // [238] 263
L8: 

    /** 		file_name = path[slash+1..$]*/
    _5388 = _slash_9628 + 1;
    if (_5388 > MAXINT){
        _5388 = NewDouble((double)_5388);
    }
    if (IS_SEQUENCE(_path_9626)){
            _5389 = SEQ_PTR(_path_9626)->length;
    }
    else {
        _5389 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_name_9632;
    RHS_Slice(_path_9626, _5388, _5389);

    /** 		file_full = file_name*/
    RefDS(_file_name_9632);
    DeRef(_file_full_9634);
    _file_full_9634 = _file_name_9632;
L9: 

    /** 	if std_slash != 0 then*/
    if (_std_slash_9627 == 0)
    goto LA; // [265] 333

    /** 		if std_slash < 0 then*/
    if (_std_slash_9627 >= 0)
    goto LB; // [271] 309

    /** 			std_slash = SLASH*/
    _std_slash_9627 = 92;

    /** 			ifdef UNIX then*/

    /** 			sequence from_slash = "/"*/
    RefDS(_5159);
    DeRefi(_from_slash_9675);
    _from_slash_9675 = _5159;

    /** 			dir_name = search:match_replace(from_slash, dir_name, std_slash)*/
    RefDS(_from_slash_9675);
    RefDS(_dir_name_9631);
    _0 = _dir_name_9631;
    _dir_name_9631 = _6match_replace(_from_slash_9675, _dir_name_9631, 92, 0);
    DeRefDS(_0);
    DeRefDSi(_from_slash_9675);
    _from_slash_9675 = NOVALUE;
    goto LC; // [306] 332
LB: 

    /** 			dir_name = search:match_replace("\\", dir_name, std_slash)*/
    RefDS(_2185);
    RefDS(_dir_name_9631);
    _0 = _dir_name_9631;
    _dir_name_9631 = _6match_replace(_2185, _dir_name_9631, _std_slash_9627, 0);
    DeRefDS(_0);

    /** 			dir_name = search:match_replace("/", dir_name, std_slash)*/
    RefDS(_5159);
    RefDS(_dir_name_9631);
    _0 = _dir_name_9631;
    _dir_name_9631 = _6match_replace(_5159, _dir_name_9631, _std_slash_9627, 0);
    DeRefDS(_0);
LC: 
LA: 

    /** 	return {dir_name, file_full, file_name, file_ext, drive_id}*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_dir_name_9631);
    *((int *)(_2+4)) = _dir_name_9631;
    RefDS(_file_full_9634);
    *((int *)(_2+8)) = _file_full_9634;
    RefDS(_file_name_9632);
    *((int *)(_2+12)) = _file_name_9632;
    RefDS(_file_ext_9633);
    *((int *)(_2+16)) = _file_ext_9633;
    RefDS(_drive_id_9635);
    *((int *)(_2+20)) = _drive_id_9635;
    _5396 = MAKE_SEQ(_1);
    DeRefDS(_path_9626);
    DeRefDS(_dir_name_9631);
    DeRefDS(_file_name_9632);
    DeRefDS(_file_ext_9633);
    DeRefDS(_file_full_9634);
    DeRefDS(_drive_id_9635);
    DeRef(_5366);
    _5366 = NOVALUE;
    DeRef(_5371);
    _5371 = NOVALUE;
    DeRef(_5375);
    _5375 = NOVALUE;
    DeRef(_5377);
    _5377 = NOVALUE;
    DeRef(_5381);
    _5381 = NOVALUE;
    DeRef(_5382);
    _5382 = NOVALUE;
    DeRef(_5384);
    _5384 = NOVALUE;
    DeRef(_5388);
    _5388 = NOVALUE;
    return _5396;
    ;
}


int _8dirname(int _path_9683, int _pcd_9684)
{
    int _data_9685 = NOVALUE;
    int _5401 = NOVALUE;
    int _5399 = NOVALUE;
    int _5398 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pcd_9684)) {
        _1 = (long)(DBL_PTR(_pcd_9684)->dbl);
        if (UNIQUE(DBL_PTR(_pcd_9684)) && (DBL_PTR(_pcd_9684)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pcd_9684);
        _pcd_9684 = _1;
    }

    /** 	data = pathinfo(path)*/
    RefDS(_path_9683);
    _0 = _data_9685;
    _data_9685 = _8pathinfo(_path_9683, 0);
    DeRef(_0);

    /** 	if pcd then*/
    if (_pcd_9684 == 0)
    {
        goto L1; // [18] 42
    }
    else{
    }

    /** 		if length(data[1]) = 0 then*/
    _2 = (int)SEQ_PTR(_data_9685);
    _5398 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_5398)){
            _5399 = SEQ_PTR(_5398)->length;
    }
    else {
        _5399 = 1;
    }
    _5398 = NOVALUE;
    if (_5399 != 0)
    goto L2; // [30] 41

    /** 			return "."*/
    RefDS(_5182);
    DeRefDS(_path_9683);
    DeRefDS(_data_9685);
    _5398 = NOVALUE;
    return _5182;
L2: 
L1: 

    /** 	return data[1]*/
    _2 = (int)SEQ_PTR(_data_9685);
    _5401 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5401);
    DeRefDS(_path_9683);
    DeRefDS(_data_9685);
    _5398 = NOVALUE;
    return _5401;
    ;
}


int _8pathname(int _path_9695)
{
    int _data_9696 = NOVALUE;
    int _stop_9697 = NOVALUE;
    int _5406 = NOVALUE;
    int _5405 = NOVALUE;
    int _5403 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = canonical_path(path)*/
    RefDS(_path_9695);
    _0 = _data_9696;
    _data_9696 = _8canonical_path(_path_9695, 0, 0);
    DeRef(_0);

    /** 	stop = search:rfind(SLASH, data)*/
    if (IS_SEQUENCE(_data_9696)){
            _5403 = SEQ_PTR(_data_9696)->length;
    }
    else {
        _5403 = 1;
    }
    RefDS(_data_9696);
    _stop_9697 = _6rfind(92, _data_9696, _5403);
    _5403 = NOVALUE;
    if (!IS_ATOM_INT(_stop_9697)) {
        _1 = (long)(DBL_PTR(_stop_9697)->dbl);
        if (UNIQUE(DBL_PTR(_stop_9697)) && (DBL_PTR(_stop_9697)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stop_9697);
        _stop_9697 = _1;
    }

    /** 	return data[1 .. stop - 1]*/
    _5405 = _stop_9697 - 1;
    rhs_slice_target = (object_ptr)&_5406;
    RHS_Slice(_data_9696, 1, _5405);
    DeRefDS(_path_9695);
    DeRefDS(_data_9696);
    _5405 = NOVALUE;
    return _5406;
    ;
}


int _8filename(int _path_9706)
{
    int _data_9707 = NOVALUE;
    int _5408 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_9706);
    _0 = _data_9707;
    _data_9707 = _8pathinfo(_path_9706, 0);
    DeRef(_0);

    /** 	return data[2]*/
    _2 = (int)SEQ_PTR(_data_9707);
    _5408 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_5408);
    DeRefDS(_path_9706);
    DeRefDS(_data_9707);
    return _5408;
    ;
}


int _8filebase(int _path_9712)
{
    int _data_9713 = NOVALUE;
    int _5410 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_9712);
    _0 = _data_9713;
    _data_9713 = _8pathinfo(_path_9712, 0);
    DeRef(_0);

    /** 	return data[3]*/
    _2 = (int)SEQ_PTR(_data_9713);
    _5410 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_5410);
    DeRefDS(_path_9712);
    DeRefDS(_data_9713);
    return _5410;
    ;
}


int _8fileext(int _path_9718)
{
    int _data_9719 = NOVALUE;
    int _5412 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_9718);
    _0 = _data_9719;
    _data_9719 = _8pathinfo(_path_9718, 0);
    DeRef(_0);

    /** 	return data[4]*/
    _2 = (int)SEQ_PTR(_data_9719);
    _5412 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_5412);
    DeRefDS(_path_9718);
    DeRefDS(_data_9719);
    return _5412;
    ;
}


int _8driveid(int _path_9724)
{
    int _data_9725 = NOVALUE;
    int _5414 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_9724);
    _0 = _data_9725;
    _data_9725 = _8pathinfo(_path_9724, 0);
    DeRef(_0);

    /** 	return data[5]*/
    _2 = (int)SEQ_PTR(_data_9725);
    _5414 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_5414);
    DeRefDS(_path_9724);
    DeRefDS(_data_9725);
    return _5414;
    ;
}


int _8defaultext(int _path_9730, int _defext_9731)
{
    int _5427 = NOVALUE;
    int _5424 = NOVALUE;
    int _5422 = NOVALUE;
    int _5421 = NOVALUE;
    int _5420 = NOVALUE;
    int _5418 = NOVALUE;
    int _5417 = NOVALUE;
    int _5415 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(defext) = 0 then*/
    if (IS_SEQUENCE(_defext_9731)){
            _5415 = SEQ_PTR(_defext_9731)->length;
    }
    else {
        _5415 = 1;
    }
    if (_5415 != 0)
    goto L1; // [10] 21

    /** 		return path*/
    DeRefDS(_defext_9731);
    return _path_9730;
L1: 

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_9730)){
            _5417 = SEQ_PTR(_path_9730)->length;
    }
    else {
        _5417 = 1;
    }
    {
        int _i_9736;
        _i_9736 = _5417;
L2: 
        if (_i_9736 < 1){
            goto L3; // [26] 95
        }

        /** 		if path[i] = '.' then*/
        _2 = (int)SEQ_PTR(_path_9730);
        _5418 = (int)*(((s1_ptr)_2)->base + _i_9736);
        if (binary_op_a(NOTEQ, _5418, 46)){
            _5418 = NOVALUE;
            goto L4; // [39] 50
        }
        _5418 = NOVALUE;

        /** 			return path*/
        DeRefDS(_defext_9731);
        return _path_9730;
L4: 

        /** 		if find(path[i], SLASHES) then*/
        _2 = (int)SEQ_PTR(_path_9730);
        _5420 = (int)*(((s1_ptr)_2)->base + _i_9736);
        _5421 = find_from(_5420, _8SLASHES_9246, 1);
        _5420 = NOVALUE;
        if (_5421 == 0)
        {
            _5421 = NOVALUE;
            goto L5; // [61] 88
        }
        else{
            _5421 = NOVALUE;
        }

        /** 			if i = length(path) then*/
        if (IS_SEQUENCE(_path_9730)){
                _5422 = SEQ_PTR(_path_9730)->length;
        }
        else {
            _5422 = 1;
        }
        if (_i_9736 != _5422)
        goto L3; // [69] 95

        /** 				return path*/
        DeRefDS(_defext_9731);
        return _path_9730;
        goto L6; // [79] 87

        /** 				exit*/
        goto L3; // [84] 95
L6: 
L5: 

        /** 	end for*/
        _i_9736 = _i_9736 + -1;
        goto L2; // [90] 33
L3: 
        ;
    }

    /** 	if defext[1] != '.' then*/
    _2 = (int)SEQ_PTR(_defext_9731);
    _5424 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _5424, 46)){
        _5424 = NOVALUE;
        goto L7; // [101] 112
    }
    _5424 = NOVALUE;

    /** 		path &= '.'*/
    Append(&_path_9730, _path_9730, 46);
L7: 

    /** 	return path & defext*/
    Concat((object_ptr)&_5427, _path_9730, _defext_9731);
    DeRefDS(_path_9730);
    DeRefDS(_defext_9731);
    return _5427;
    ;
}


int _8absolute_path(int _filename_9755)
{
    int _5439 = NOVALUE;
    int _5438 = NOVALUE;
    int _5436 = NOVALUE;
    int _5434 = NOVALUE;
    int _5432 = NOVALUE;
    int _5431 = NOVALUE;
    int _5430 = NOVALUE;
    int _5428 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(filename) = 0 then*/
    if (IS_SEQUENCE(_filename_9755)){
            _5428 = SEQ_PTR(_filename_9755)->length;
    }
    else {
        _5428 = 1;
    }
    if (_5428 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRefDS(_filename_9755);
    return 0;
L1: 

    /** 	if eu:find(filename[1], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_9755);
    _5430 = (int)*(((s1_ptr)_2)->base + 1);
    _5431 = find_from(_5430, _8SLASHES_9246, 1);
    _5430 = NOVALUE;
    if (_5431 == 0)
    {
        _5431 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _5431 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_filename_9755);
    return 1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 1 then*/
    if (IS_SEQUENCE(_filename_9755)){
            _5432 = SEQ_PTR(_filename_9755)->length;
    }
    else {
        _5432 = 1;
    }
    if (_5432 != 1)
    goto L3; // [47] 58

    /** 			return 0*/
    DeRefDS(_filename_9755);
    return 0;
L3: 

    /** 		if filename[2] != ':' then*/
    _2 = (int)SEQ_PTR(_filename_9755);
    _5434 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(EQUALS, _5434, 58)){
        _5434 = NOVALUE;
        goto L4; // [64] 75
    }
    _5434 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_filename_9755);
    return 0;
L4: 

    /** 		if length(filename) < 3 then*/
    if (IS_SEQUENCE(_filename_9755)){
            _5436 = SEQ_PTR(_filename_9755)->length;
    }
    else {
        _5436 = 1;
    }
    if (_5436 >= 3)
    goto L5; // [80] 91

    /** 			return 0*/
    DeRefDS(_filename_9755);
    return 0;
L5: 

    /** 		if eu:find(filename[3], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_9755);
    _5438 = (int)*(((s1_ptr)_2)->base + 3);
    _5439 = find_from(_5438, _8SLASHES_9246, 1);
    _5438 = NOVALUE;
    if (_5439 == 0)
    {
        _5439 = NOVALUE;
        goto L6; // [102] 112
    }
    else{
        _5439 = NOVALUE;
    }

    /** 			return 1*/
    DeRefDS(_filename_9755);
    return 1;
L6: 

    /** 	return 0*/
    DeRefDS(_filename_9755);
    return 0;
    ;
}


int _8case_flagset_type(int _x_9787)
{
    int _5448 = NOVALUE;
    int _5447 = NOVALUE;
    int _5446 = NOVALUE;
    int _5445 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_9787)) {
        _1 = (long)(DBL_PTR(_x_9787)->dbl);
        if (UNIQUE(DBL_PTR(_x_9787)) && (DBL_PTR(_x_9787)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_9787);
        _x_9787 = _1;
    }

    /** 	return x >= AS_IS and x < 2*TO_SHORT*/
    _5445 = (_x_9787 >= 0);
    _5446 = 8;
    _5447 = (_x_9787 < 8);
    _5446 = NOVALUE;
    _5448 = (_5445 != 0 && _5447 != 0);
    _5445 = NOVALUE;
    _5447 = NOVALUE;
    return _5448;
    ;
}


int _8canonical_path(int _path_in_9794, int _directory_given_9795, int _case_flags_9796)
{
    int _lPath_9797 = NOVALUE;
    int _lPosA_9798 = NOVALUE;
    int _lPosB_9799 = NOVALUE;
    int _lLevel_9800 = NOVALUE;
    int _lHome_9801 = NOVALUE;
    int _lDrive_9802 = NOVALUE;
    int _current_dir_inlined_current_dir_at_306_9862 = NOVALUE;
    int _driveid_inlined_driveid_at_313_9865 = NOVALUE;
    int _data_inlined_driveid_at_313_9864 = NOVALUE;
    int _wildcard_suffix_9867 = NOVALUE;
    int _first_wildcard_at_9868 = NOVALUE;
    int _last_slash_9871 = NOVALUE;
    int _sl_9943 = NOVALUE;
    int _short_name_9946 = NOVALUE;
    int _correct_name_9949 = NOVALUE;
    int _lower_name_9952 = NOVALUE;
    int _part_9968 = NOVALUE;
    int _list_9973 = NOVALUE;
    int _dir_inlined_dir_at_929_9977 = NOVALUE;
    int _name_inlined_dir_at_926_9976 = NOVALUE;
    int _supplied_name_9978 = NOVALUE;
    int _read_name_9997 = NOVALUE;
    int _read_name_10022 = NOVALUE;
    int _5661 = NOVALUE;
    int _5658 = NOVALUE;
    int _5654 = NOVALUE;
    int _5653 = NOVALUE;
    int _5651 = NOVALUE;
    int _5650 = NOVALUE;
    int _5649 = NOVALUE;
    int _5648 = NOVALUE;
    int _5647 = NOVALUE;
    int _5645 = NOVALUE;
    int _5644 = NOVALUE;
    int _5643 = NOVALUE;
    int _5642 = NOVALUE;
    int _5641 = NOVALUE;
    int _5640 = NOVALUE;
    int _5639 = NOVALUE;
    int _5638 = NOVALUE;
    int _5636 = NOVALUE;
    int _5635 = NOVALUE;
    int _5634 = NOVALUE;
    int _5633 = NOVALUE;
    int _5632 = NOVALUE;
    int _5631 = NOVALUE;
    int _5630 = NOVALUE;
    int _5629 = NOVALUE;
    int _5628 = NOVALUE;
    int _5626 = NOVALUE;
    int _5625 = NOVALUE;
    int _5624 = NOVALUE;
    int _5623 = NOVALUE;
    int _5622 = NOVALUE;
    int _5621 = NOVALUE;
    int _5620 = NOVALUE;
    int _5619 = NOVALUE;
    int _5618 = NOVALUE;
    int _5617 = NOVALUE;
    int _5616 = NOVALUE;
    int _5615 = NOVALUE;
    int _5614 = NOVALUE;
    int _5613 = NOVALUE;
    int _5612 = NOVALUE;
    int _5610 = NOVALUE;
    int _5609 = NOVALUE;
    int _5608 = NOVALUE;
    int _5607 = NOVALUE;
    int _5606 = NOVALUE;
    int _5604 = NOVALUE;
    int _5603 = NOVALUE;
    int _5602 = NOVALUE;
    int _5601 = NOVALUE;
    int _5600 = NOVALUE;
    int _5599 = NOVALUE;
    int _5598 = NOVALUE;
    int _5597 = NOVALUE;
    int _5596 = NOVALUE;
    int _5595 = NOVALUE;
    int _5594 = NOVALUE;
    int _5593 = NOVALUE;
    int _5592 = NOVALUE;
    int _5590 = NOVALUE;
    int _5589 = NOVALUE;
    int _5587 = NOVALUE;
    int _5586 = NOVALUE;
    int _5585 = NOVALUE;
    int _5584 = NOVALUE;
    int _5583 = NOVALUE;
    int _5581 = NOVALUE;
    int _5580 = NOVALUE;
    int _5579 = NOVALUE;
    int _5578 = NOVALUE;
    int _5577 = NOVALUE;
    int _5576 = NOVALUE;
    int _5574 = NOVALUE;
    int _5573 = NOVALUE;
    int _5572 = NOVALUE;
    int _5570 = NOVALUE;
    int _5569 = NOVALUE;
    int _5567 = NOVALUE;
    int _5566 = NOVALUE;
    int _5565 = NOVALUE;
    int _5563 = NOVALUE;
    int _5562 = NOVALUE;
    int _5560 = NOVALUE;
    int _5558 = NOVALUE;
    int _5556 = NOVALUE;
    int _5549 = NOVALUE;
    int _5546 = NOVALUE;
    int _5545 = NOVALUE;
    int _5544 = NOVALUE;
    int _5543 = NOVALUE;
    int _5537 = NOVALUE;
    int _5533 = NOVALUE;
    int _5532 = NOVALUE;
    int _5531 = NOVALUE;
    int _5530 = NOVALUE;
    int _5529 = NOVALUE;
    int _5527 = NOVALUE;
    int _5524 = NOVALUE;
    int _5523 = NOVALUE;
    int _5522 = NOVALUE;
    int _5521 = NOVALUE;
    int _5520 = NOVALUE;
    int _5519 = NOVALUE;
    int _5517 = NOVALUE;
    int _5516 = NOVALUE;
    int _5514 = NOVALUE;
    int _5512 = NOVALUE;
    int _5511 = NOVALUE;
    int _5510 = NOVALUE;
    int _5508 = NOVALUE;
    int _5507 = NOVALUE;
    int _5506 = NOVALUE;
    int _5505 = NOVALUE;
    int _5503 = NOVALUE;
    int _5501 = NOVALUE;
    int _5496 = NOVALUE;
    int _5494 = NOVALUE;
    int _5493 = NOVALUE;
    int _5492 = NOVALUE;
    int _5491 = NOVALUE;
    int _5490 = NOVALUE;
    int _5488 = NOVALUE;
    int _5487 = NOVALUE;
    int _5485 = NOVALUE;
    int _5484 = NOVALUE;
    int _5483 = NOVALUE;
    int _5482 = NOVALUE;
    int _5481 = NOVALUE;
    int _5480 = NOVALUE;
    int _5479 = NOVALUE;
    int _5476 = NOVALUE;
    int _5475 = NOVALUE;
    int _5473 = NOVALUE;
    int _5471 = NOVALUE;
    int _5469 = NOVALUE;
    int _5466 = NOVALUE;
    int _5465 = NOVALUE;
    int _5464 = NOVALUE;
    int _5463 = NOVALUE;
    int _5462 = NOVALUE;
    int _5460 = NOVALUE;
    int _5459 = NOVALUE;
    int _5458 = NOVALUE;
    int _5457 = NOVALUE;
    int _5456 = NOVALUE;
    int _5455 = NOVALUE;
    int _5454 = NOVALUE;
    int _5453 = NOVALUE;
    int _5452 = NOVALUE;
    int _5451 = NOVALUE;
    int _5450 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_directory_given_9795)) {
        _1 = (long)(DBL_PTR(_directory_given_9795)->dbl);
        if (UNIQUE(DBL_PTR(_directory_given_9795)) && (DBL_PTR(_directory_given_9795)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_directory_given_9795);
        _directory_given_9795 = _1;
    }
    if (!IS_ATOM_INT(_case_flags_9796)) {
        _1 = (long)(DBL_PTR(_case_flags_9796)->dbl);
        if (UNIQUE(DBL_PTR(_case_flags_9796)) && (DBL_PTR(_case_flags_9796)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_case_flags_9796);
        _case_flags_9796 = _1;
    }

    /**     sequence lPath = ""*/
    RefDS(_5);
    DeRef(_lPath_9797);
    _lPath_9797 = _5;

    /**     integer lPosA = -1*/
    _lPosA_9798 = -1;

    /**     integer lPosB = -1*/
    _lPosB_9799 = -1;

    /**     sequence lLevel = ""*/
    RefDS(_5);
    DeRefi(_lLevel_9800);
    _lLevel_9800 = _5;

    /**     path_in = path_in*/
    RefDS(_path_in_9794);
    DeRefDS(_path_in_9794);
    _path_in_9794 = _path_in_9794;

    /** 	ifdef UNIX then*/

    /** 	    sequence lDrive*/

    /** 	    lPath = match_replace("/", path_in, SLASH)*/
    RefDS(_5159);
    RefDS(_path_in_9794);
    _0 = _lPath_9797;
    _lPath_9797 = _6match_replace(_5159, _path_in_9794, 92, 0);
    DeRefDS(_0);

    /**     if (length(lPath) > 2 and lPath[1] = '"' and lPath[$] = '"') then*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5450 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5450 = 1;
    }
    _5451 = (_5450 > 2);
    _5450 = NOVALUE;
    if (_5451 == 0) {
        _5452 = 0;
        goto L1; // [68] 84
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5453 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5453)) {
        _5454 = (_5453 == 34);
    }
    else {
        _5454 = binary_op(EQUALS, _5453, 34);
    }
    _5453 = NOVALUE;
    if (IS_ATOM_INT(_5454))
    _5452 = (_5454 != 0);
    else
    _5452 = DBL_PTR(_5454)->dbl != 0.0;
L1: 
    if (_5452 == 0) {
        DeRef(_5455);
        _5455 = 0;
        goto L2; // [84] 103
    }
    if (IS_SEQUENCE(_lPath_9797)){
            _5456 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5456 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5457 = (int)*(((s1_ptr)_2)->base + _5456);
    if (IS_ATOM_INT(_5457)) {
        _5458 = (_5457 == 34);
    }
    else {
        _5458 = binary_op(EQUALS, _5457, 34);
    }
    _5457 = NOVALUE;
    if (IS_ATOM_INT(_5458))
    _5455 = (_5458 != 0);
    else
    _5455 = DBL_PTR(_5458)->dbl != 0.0;
L2: 
    if (_5455 == 0)
    {
        _5455 = NOVALUE;
        goto L3; // [103] 121
    }
    else{
        _5455 = NOVALUE;
    }

    /**         lPath = lPath[2..$-1]*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5459 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5459 = 1;
    }
    _5460 = _5459 - 1;
    _5459 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lPath_9797;
    RHS_Slice(_lPath_9797, 2, _5460);
L3: 

    /**     if (length(lPath) > 0 and lPath[1] = '~') then*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5462 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5462 = 1;
    }
    _5463 = (_5462 > 0);
    _5462 = NOVALUE;
    if (_5463 == 0) {
        DeRef(_5464);
        _5464 = 0;
        goto L4; // [130] 146
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5465 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5465)) {
        _5466 = (_5465 == 126);
    }
    else {
        _5466 = binary_op(EQUALS, _5465, 126);
    }
    _5465 = NOVALUE;
    if (IS_ATOM_INT(_5466))
    _5464 = (_5466 != 0);
    else
    _5464 = DBL_PTR(_5466)->dbl != 0.0;
L4: 
    if (_5464 == 0)
    {
        _5464 = NOVALUE;
        goto L5; // [146] 255
    }
    else{
        _5464 = NOVALUE;
    }

    /** 		lHome = getenv("HOME")*/
    DeRef(_lHome_9801);
    _lHome_9801 = EGetEnv(_5467);

    /** 		ifdef WINDOWS then*/

    /** 			if atom(lHome) then*/
    _5469 = IS_ATOM(_lHome_9801);
    if (_5469 == 0)
    {
        _5469 = NOVALUE;
        goto L6; // [161] 177
    }
    else{
        _5469 = NOVALUE;
    }

    /** 				lHome = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _5471 = EGetEnv(_5470);
    _5473 = EGetEnv(_5472);
    if (IS_SEQUENCE(_5471) && IS_ATOM(_5473)) {
        Ref(_5473);
        Append(&_lHome_9801, _5471, _5473);
    }
    else if (IS_ATOM(_5471) && IS_SEQUENCE(_5473)) {
        Ref(_5471);
        Prepend(&_lHome_9801, _5473, _5471);
    }
    else {
        Concat((object_ptr)&_lHome_9801, _5471, _5473);
        DeRef(_5471);
        _5471 = NOVALUE;
    }
    DeRef(_5471);
    _5471 = NOVALUE;
    DeRef(_5473);
    _5473 = NOVALUE;
L6: 

    /** 		if lHome[$] != SLASH then*/
    if (IS_SEQUENCE(_lHome_9801)){
            _5475 = SEQ_PTR(_lHome_9801)->length;
    }
    else {
        _5475 = 1;
    }
    _2 = (int)SEQ_PTR(_lHome_9801);
    _5476 = (int)*(((s1_ptr)_2)->base + _5475);
    if (binary_op_a(EQUALS, _5476, 92)){
        _5476 = NOVALUE;
        goto L7; // [186] 197
    }
    _5476 = NOVALUE;

    /** 			lHome &= SLASH*/
    if (IS_SEQUENCE(_lHome_9801) && IS_ATOM(92)) {
        Append(&_lHome_9801, _lHome_9801, 92);
    }
    else if (IS_ATOM(_lHome_9801) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_lHome_9801, _lHome_9801, 92);
    }
L7: 

    /** 		if length(lPath) > 1 and lPath[2] = SLASH then*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5479 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5479 = 1;
    }
    _5480 = (_5479 > 1);
    _5479 = NOVALUE;
    if (_5480 == 0) {
        goto L8; // [206] 239
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5482 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5482)) {
        _5483 = (_5482 == 92);
    }
    else {
        _5483 = binary_op(EQUALS, _5482, 92);
    }
    _5482 = NOVALUE;
    if (_5483 == 0) {
        DeRef(_5483);
        _5483 = NOVALUE;
        goto L8; // [219] 239
    }
    else {
        if (!IS_ATOM_INT(_5483) && DBL_PTR(_5483)->dbl == 0.0){
            DeRef(_5483);
            _5483 = NOVALUE;
            goto L8; // [219] 239
        }
        DeRef(_5483);
        _5483 = NOVALUE;
    }
    DeRef(_5483);
    _5483 = NOVALUE;

    /** 			lPath = lHome & lPath[3 .. $]*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5484 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5484 = 1;
    }
    rhs_slice_target = (object_ptr)&_5485;
    RHS_Slice(_lPath_9797, 3, _5484);
    if (IS_SEQUENCE(_lHome_9801) && IS_ATOM(_5485)) {
    }
    else if (IS_ATOM(_lHome_9801) && IS_SEQUENCE(_5485)) {
        Ref(_lHome_9801);
        Prepend(&_lPath_9797, _5485, _lHome_9801);
    }
    else {
        Concat((object_ptr)&_lPath_9797, _lHome_9801, _5485);
    }
    DeRefDS(_5485);
    _5485 = NOVALUE;
    goto L9; // [236] 254
L8: 

    /** 			lPath = lHome & lPath[2 .. $]*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5487 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5487 = 1;
    }
    rhs_slice_target = (object_ptr)&_5488;
    RHS_Slice(_lPath_9797, 2, _5487);
    if (IS_SEQUENCE(_lHome_9801) && IS_ATOM(_5488)) {
    }
    else if (IS_ATOM(_lHome_9801) && IS_SEQUENCE(_5488)) {
        Ref(_lHome_9801);
        Prepend(&_lPath_9797, _5488, _lHome_9801);
    }
    else {
        Concat((object_ptr)&_lPath_9797, _lHome_9801, _5488);
    }
    DeRefDS(_5488);
    _5488 = NOVALUE;
L9: 
L5: 

    /** 	ifdef WINDOWS then*/

    /** 	    if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5490 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5490 = 1;
    }
    _5491 = (_5490 > 1);
    _5490 = NOVALUE;
    if (_5491 == 0) {
        DeRef(_5492);
        _5492 = 0;
        goto LA; // [266] 282
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5493 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5493)) {
        _5494 = (_5493 == 58);
    }
    else {
        _5494 = binary_op(EQUALS, _5493, 58);
    }
    _5493 = NOVALUE;
    if (IS_ATOM_INT(_5494))
    _5492 = (_5494 != 0);
    else
    _5492 = DBL_PTR(_5494)->dbl != 0.0;
LA: 
    if (_5492 == 0)
    {
        _5492 = NOVALUE;
        goto LB; // [282] 305
    }
    else{
        _5492 = NOVALUE;
    }

    /** 			lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_9802;
    RHS_Slice(_lPath_9797, 1, 2);

    /** 			lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5496 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5496 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_9797;
    RHS_Slice(_lPath_9797, 3, _5496);
    goto LC; // [302] 339
LB: 

    /** 			lDrive = driveid(current_dir()) & ':'*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_current_dir_inlined_current_dir_at_306_9862);
    _current_dir_inlined_current_dir_at_306_9862 = machine(23, 0);

    /** 	data = pathinfo(path)*/
    RefDS(_current_dir_inlined_current_dir_at_306_9862);
    _0 = _data_inlined_driveid_at_313_9864;
    _data_inlined_driveid_at_313_9864 = _8pathinfo(_current_dir_inlined_current_dir_at_306_9862, 0);
    DeRef(_0);

    /** 	return data[5]*/
    DeRef(_driveid_inlined_driveid_at_313_9865);
    _2 = (int)SEQ_PTR(_data_inlined_driveid_at_313_9864);
    _driveid_inlined_driveid_at_313_9865 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_driveid_inlined_driveid_at_313_9865);
    DeRef(_data_inlined_driveid_at_313_9864);
    _data_inlined_driveid_at_313_9864 = NOVALUE;
    if (IS_SEQUENCE(_driveid_inlined_driveid_at_313_9865) && IS_ATOM(58)) {
        Append(&_lDrive_9802, _driveid_inlined_driveid_at_313_9865, 58);
    }
    else if (IS_ATOM(_driveid_inlined_driveid_at_313_9865) && IS_SEQUENCE(58)) {
    }
    else {
        Concat((object_ptr)&_lDrive_9802, _driveid_inlined_driveid_at_313_9865, 58);
    }
LC: 

    /** 	sequence wildcard_suffix*/

    /** 	integer first_wildcard_at = find_first_wildcard( lPath )*/
    RefDS(_lPath_9797);
    _first_wildcard_at_9868 = _8find_first_wildcard(_lPath_9797, 1);
    if (!IS_ATOM_INT(_first_wildcard_at_9868)) {
        _1 = (long)(DBL_PTR(_first_wildcard_at_9868)->dbl);
        if (UNIQUE(DBL_PTR(_first_wildcard_at_9868)) && (DBL_PTR(_first_wildcard_at_9868)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_first_wildcard_at_9868);
        _first_wildcard_at_9868 = _1;
    }

    /** 	if first_wildcard_at then*/
    if (_first_wildcard_at_9868 == 0)
    {
        goto LD; // [354] 417
    }
    else{
    }

    /** 		integer last_slash = search:rfind( SLASH, lPath, first_wildcard_at )*/
    RefDS(_lPath_9797);
    _last_slash_9871 = _6rfind(92, _lPath_9797, _first_wildcard_at_9868);
    if (!IS_ATOM_INT(_last_slash_9871)) {
        _1 = (long)(DBL_PTR(_last_slash_9871)->dbl);
        if (UNIQUE(DBL_PTR(_last_slash_9871)) && (DBL_PTR(_last_slash_9871)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_slash_9871);
        _last_slash_9871 = _1;
    }

    /** 		if last_slash then*/
    if (_last_slash_9871 == 0)
    {
        goto LE; // [371] 397
    }
    else{
    }

    /** 			wildcard_suffix = lPath[last_slash..$]*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5501 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5501 = 1;
    }
    rhs_slice_target = (object_ptr)&_wildcard_suffix_9867;
    RHS_Slice(_lPath_9797, _last_slash_9871, _5501);

    /** 			lPath = remove( lPath, last_slash, length( lPath ) )*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5503 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5503 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_9797);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_last_slash_9871)) ? _last_slash_9871 : (long)(DBL_PTR(_last_slash_9871)->dbl);
        int stop = (IS_ATOM_INT(_5503)) ? _5503 : (long)(DBL_PTR(_5503)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_9797), start, &_lPath_9797 );
            }
            else Tail(SEQ_PTR(_lPath_9797), stop+1, &_lPath_9797);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_9797), start, &_lPath_9797);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_9797 = Remove_elements(start, stop, (SEQ_PTR(_lPath_9797)->ref == 1));
        }
    }
    _5503 = NOVALUE;
    goto LF; // [394] 412
LE: 

    /** 			wildcard_suffix = lPath*/
    RefDS(_lPath_9797);
    DeRef(_wildcard_suffix_9867);
    _wildcard_suffix_9867 = _lPath_9797;

    /** 			lPath = ""*/
    RefDS(_5);
    DeRefDS(_lPath_9797);
    _lPath_9797 = _5;
LF: 
    goto L10; // [414] 425
LD: 

    /** 		wildcard_suffix = ""*/
    RefDS(_5);
    DeRef(_wildcard_suffix_9867);
    _wildcard_suffix_9867 = _5;
L10: 

    /** 	if ((length(lPath) = 0) or not find(lPath[1], "/\\")) then*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5505 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5505 = 1;
    }
    _5506 = (_5505 == 0);
    _5505 = NOVALUE;
    if (_5506 != 0) {
        DeRef(_5507);
        _5507 = 1;
        goto L11; // [434] 454
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5508 = (int)*(((s1_ptr)_2)->base + 1);
    _5510 = find_from(_5508, _5509, 1);
    _5508 = NOVALUE;
    _5511 = (_5510 == 0);
    _5510 = NOVALUE;
    _5507 = (_5511 != 0);
L11: 
    if (_5507 == 0)
    {
        _5507 = NOVALUE;
        goto L12; // [454] 555
    }
    else{
        _5507 = NOVALUE;
    }

    /** 		ifdef UNIX then*/

    /** 			if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_9802)){
            _5512 = SEQ_PTR(_lDrive_9802)->length;
    }
    else {
        _5512 = 1;
    }
    if (_5512 != 0)
    goto L13; // [466] 483

    /** 				lPath = curdir() & lPath*/
    _5514 = _8curdir(0);
    if (IS_SEQUENCE(_5514) && IS_ATOM(_lPath_9797)) {
    }
    else if (IS_ATOM(_5514) && IS_SEQUENCE(_lPath_9797)) {
        Ref(_5514);
        Prepend(&_lPath_9797, _lPath_9797, _5514);
    }
    else {
        Concat((object_ptr)&_lPath_9797, _5514, _lPath_9797);
        DeRef(_5514);
        _5514 = NOVALUE;
    }
    DeRef(_5514);
    _5514 = NOVALUE;
    goto L14; // [480] 498
L13: 

    /** 				lPath = curdir(lDrive[1]) & lPath*/
    _2 = (int)SEQ_PTR(_lDrive_9802);
    _5516 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5516);
    _5517 = _8curdir(_5516);
    _5516 = NOVALUE;
    if (IS_SEQUENCE(_5517) && IS_ATOM(_lPath_9797)) {
    }
    else if (IS_ATOM(_5517) && IS_SEQUENCE(_lPath_9797)) {
        Ref(_5517);
        Prepend(&_lPath_9797, _lPath_9797, _5517);
    }
    else {
        Concat((object_ptr)&_lPath_9797, _5517, _lPath_9797);
        DeRef(_5517);
        _5517 = NOVALUE;
    }
    DeRef(_5517);
    _5517 = NOVALUE;
L14: 

    /** 			if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5519 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5519 = 1;
    }
    _5520 = (_5519 > 1);
    _5519 = NOVALUE;
    if (_5520 == 0) {
        DeRef(_5521);
        _5521 = 0;
        goto L15; // [507] 523
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5522 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5522)) {
        _5523 = (_5522 == 58);
    }
    else {
        _5523 = binary_op(EQUALS, _5522, 58);
    }
    _5522 = NOVALUE;
    if (IS_ATOM_INT(_5523))
    _5521 = (_5523 != 0);
    else
    _5521 = DBL_PTR(_5523)->dbl != 0.0;
L15: 
    if (_5521 == 0)
    {
        _5521 = NOVALUE;
        goto L16; // [523] 554
    }
    else{
        _5521 = NOVALUE;
    }

    /** 				if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_9802)){
            _5524 = SEQ_PTR(_lDrive_9802)->length;
    }
    else {
        _5524 = 1;
    }
    if (_5524 != 0)
    goto L17; // [531] 543

    /** 					lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_9802;
    RHS_Slice(_lPath_9797, 1, 2);
L17: 

    /** 				lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5527 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5527 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_9797;
    RHS_Slice(_lPath_9797, 3, _5527);
L16: 
L12: 

    /** 	if ((directory_given != 0) and (lPath[$] != SLASH) ) then*/
    _5529 = (_directory_given_9795 != 0);
    if (_5529 == 0) {
        DeRef(_5530);
        _5530 = 0;
        goto L18; // [561] 580
    }
    if (IS_SEQUENCE(_lPath_9797)){
            _5531 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5531 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5532 = (int)*(((s1_ptr)_2)->base + _5531);
    if (IS_ATOM_INT(_5532)) {
        _5533 = (_5532 != 92);
    }
    else {
        _5533 = binary_op(NOTEQ, _5532, 92);
    }
    _5532 = NOVALUE;
    if (IS_ATOM_INT(_5533))
    _5530 = (_5533 != 0);
    else
    _5530 = DBL_PTR(_5533)->dbl != 0.0;
L18: 
    if (_5530 == 0)
    {
        _5530 = NOVALUE;
        goto L19; // [580] 590
    }
    else{
        _5530 = NOVALUE;
    }

    /** 		lPath &= SLASH*/
    Append(&_lPath_9797, _lPath_9797, 92);
L19: 

    /** 	lLevel = SLASH & '.' & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = 46;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_9800, concat_list, 3);
    }

    /** 	lPosA = 1*/
    _lPosA_9798 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1A; // [607] 628
L1B: 
    if (_lPosA_9798 == 0)
    goto L1C; // [610] 642

    /** 		lPath = eu:remove(lPath, lPosA, lPosA + 1)*/
    _5537 = _lPosA_9798 + 1;
    if (_5537 > MAXINT){
        _5537 = NewDouble((double)_5537);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_9797);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosA_9798)) ? _lPosA_9798 : (long)(DBL_PTR(_lPosA_9798)->dbl);
        int stop = (IS_ATOM_INT(_5537)) ? _5537 : (long)(DBL_PTR(_5537)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_9797), start, &_lPath_9797 );
            }
            else Tail(SEQ_PTR(_lPath_9797), stop+1, &_lPath_9797);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_9797), start, &_lPath_9797);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_9797 = Remove_elements(start, stop, (SEQ_PTR(_lPath_9797)->ref == 1));
        }
    }
    DeRef(_5537);
    _5537 = NOVALUE;

    /** 	  entry*/
L1A: 

    /** 		lPosA = match(lLevel, lPath, lPosA )*/
    _lPosA_9798 = e_match_from(_lLevel_9800, _lPath_9797, _lPosA_9798);

    /** 	end while*/
    goto L1B; // [639] 610
L1C: 

    /** 	lLevel = SLASH & ".." & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = _5213;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_9800, concat_list, 3);
    }

    /** 	lPosB = 1*/
    _lPosB_9799 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1D; // [659] 743
L1E: 
    if (_lPosA_9798 == 0)
    goto L1F; // [662] 757

    /** 		lPosB = lPosA-1*/
    _lPosB_9799 = _lPosA_9798 - 1;

    /** 		while((lPosB > 0) and (lPath[lPosB] != SLASH)) do*/
L20: 
    _5543 = (_lPosB_9799 > 0);
    if (_5543 == 0) {
        DeRef(_5544);
        _5544 = 0;
        goto L21; // [683] 699
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5545 = (int)*(((s1_ptr)_2)->base + _lPosB_9799);
    if (IS_ATOM_INT(_5545)) {
        _5546 = (_5545 != 92);
    }
    else {
        _5546 = binary_op(NOTEQ, _5545, 92);
    }
    _5545 = NOVALUE;
    if (IS_ATOM_INT(_5546))
    _5544 = (_5546 != 0);
    else
    _5544 = DBL_PTR(_5546)->dbl != 0.0;
L21: 
    if (_5544 == 0)
    {
        _5544 = NOVALUE;
        goto L22; // [699] 715
    }
    else{
        _5544 = NOVALUE;
    }

    /** 			lPosB -= 1*/
    _lPosB_9799 = _lPosB_9799 - 1;

    /** 		end while*/
    goto L20; // [712] 679
L22: 

    /** 		if (lPosB <= 0) then*/
    if (_lPosB_9799 > 0)
    goto L23; // [717] 729

    /** 			lPosB = 1*/
    _lPosB_9799 = 1;
L23: 

    /** 		lPath = eu:remove(lPath, lPosB, lPosA + 2)*/
    _5549 = _lPosA_9798 + 2;
    if ((long)((unsigned long)_5549 + (unsigned long)HIGH_BITS) >= 0) 
    _5549 = NewDouble((double)_5549);
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_9797);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosB_9799)) ? _lPosB_9799 : (long)(DBL_PTR(_lPosB_9799)->dbl);
        int stop = (IS_ATOM_INT(_5549)) ? _5549 : (long)(DBL_PTR(_5549)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_9797), start, &_lPath_9797 );
            }
            else Tail(SEQ_PTR(_lPath_9797), stop+1, &_lPath_9797);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_9797), start, &_lPath_9797);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_9797 = Remove_elements(start, stop, (SEQ_PTR(_lPath_9797)->ref == 1));
        }
    }
    DeRef(_5549);
    _5549 = NOVALUE;

    /** 	  entry*/
L1D: 

    /** 		lPosA = match(lLevel, lPath, lPosB )*/
    _lPosA_9798 = e_match_from(_lLevel_9800, _lPath_9797, _lPosB_9799);

    /** 	end while*/
    goto L1E; // [754] 662
L1F: 

    /** 	if case_flags = TO_LOWER then*/
    if (_case_flags_9796 != 1)
    goto L24; // [761] 776

    /** 		lPath = lower( lPath )*/
    RefDS(_lPath_9797);
    _0 = _lPath_9797;
    _lPath_9797 = _16lower(_lPath_9797);
    DeRefDS(_0);
    goto L25; // [773] 1437
L24: 

    /** 	elsif case_flags != AS_IS then*/
    if (_case_flags_9796 == 0)
    goto L26; // [780] 1434

    /** 		sequence sl = find_all(SLASH,lPath) -- split apart lPath*/
    RefDS(_lPath_9797);
    _0 = _sl_9943;
    _sl_9943 = _6find_all(92, _lPath_9797, 1);
    DeRef(_0);

    /** 		integer short_name = and_bits(TO_SHORT,case_flags)=TO_SHORT*/
    {unsigned long tu;
         tu = (unsigned long)4 & (unsigned long)_case_flags_9796;
         _5556 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_5556)) {
        _short_name_9946 = (_5556 == 4);
    }
    else {
        _short_name_9946 = (DBL_PTR(_5556)->dbl == (double)4);
    }
    DeRef(_5556);
    _5556 = NOVALUE;

    /** 		integer correct_name = and_bits(case_flags,CORRECT)=CORRECT*/
    {unsigned long tu;
         tu = (unsigned long)_case_flags_9796 & (unsigned long)2;
         _5558 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_5558)) {
        _correct_name_9949 = (_5558 == 2);
    }
    else {
        _correct_name_9949 = (DBL_PTR(_5558)->dbl == (double)2);
    }
    DeRef(_5558);
    _5558 = NOVALUE;

    /** 		integer lower_name = and_bits(TO_LOWER,case_flags)=TO_LOWER*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_9796;
         _5560 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_5560)) {
        _lower_name_9952 = (_5560 == 1);
    }
    else {
        _lower_name_9952 = (DBL_PTR(_5560)->dbl == (double)1);
    }
    DeRef(_5560);
    _5560 = NOVALUE;

    /** 		if lPath[$] != SLASH then*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5562 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5562 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_9797);
    _5563 = (int)*(((s1_ptr)_2)->base + _5562);
    if (binary_op_a(EQUALS, _5563, 92)){
        _5563 = NOVALUE;
        goto L27; // [857] 879
    }
    _5563 = NOVALUE;

    /** 			sl = sl & {length(lPath)+1}*/
    if (IS_SEQUENCE(_lPath_9797)){
            _5565 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5565 = 1;
    }
    _5566 = _5565 + 1;
    _5565 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5566;
    _5567 = MAKE_SEQ(_1);
    _5566 = NOVALUE;
    Concat((object_ptr)&_sl_9943, _sl_9943, _5567);
    DeRefDS(_5567);
    _5567 = NOVALUE;
L27: 

    /** 		for i = length(sl)-1 to 1 by -1 label "partloop" do*/
    if (IS_SEQUENCE(_sl_9943)){
            _5569 = SEQ_PTR(_sl_9943)->length;
    }
    else {
        _5569 = 1;
    }
    _5570 = _5569 - 1;
    _5569 = NOVALUE;
    {
        int _i_9964;
        _i_9964 = _5570;
L28: 
        if (_i_9964 < 1){
            goto L29; // [888] 1393
        }

        /** 			ifdef WINDOWS then*/

        /** 				sequence part = lDrive & lPath[1..sl[i]-1]*/
        _2 = (int)SEQ_PTR(_sl_9943);
        _5572 = (int)*(((s1_ptr)_2)->base + _i_9964);
        if (IS_ATOM_INT(_5572)) {
            _5573 = _5572 - 1;
        }
        else {
            _5573 = binary_op(MINUS, _5572, 1);
        }
        _5572 = NOVALUE;
        rhs_slice_target = (object_ptr)&_5574;
        RHS_Slice(_lPath_9797, 1, _5573);
        Concat((object_ptr)&_part_9968, _lDrive_9802, _5574);
        DeRefDS(_5574);
        _5574 = NOVALUE;

        /** 			object list = dir( part & SLASH )*/
        Append(&_5576, _part_9968, 92);
        DeRef(_name_inlined_dir_at_926_9976);
        _name_inlined_dir_at_926_9976 = _5576;
        _5576 = NOVALUE;

        /** 	ifdef WINDOWS then*/

        /** 		return machine_func(M_DIR, name)*/
        DeRef(_list_9973);
        _list_9973 = machine(22, _name_inlined_dir_at_926_9976);
        DeRef(_name_inlined_dir_at_926_9976);
        _name_inlined_dir_at_926_9976 = NOVALUE;

        /** 			sequence supplied_name = lPath[sl[i]+1..sl[i+1]-1]*/
        _2 = (int)SEQ_PTR(_sl_9943);
        _5577 = (int)*(((s1_ptr)_2)->base + _i_9964);
        if (IS_ATOM_INT(_5577)) {
            _5578 = _5577 + 1;
            if (_5578 > MAXINT){
                _5578 = NewDouble((double)_5578);
            }
        }
        else
        _5578 = binary_op(PLUS, 1, _5577);
        _5577 = NOVALUE;
        _5579 = _i_9964 + 1;
        _2 = (int)SEQ_PTR(_sl_9943);
        _5580 = (int)*(((s1_ptr)_2)->base + _5579);
        if (IS_ATOM_INT(_5580)) {
            _5581 = _5580 - 1;
        }
        else {
            _5581 = binary_op(MINUS, _5580, 1);
        }
        _5580 = NOVALUE;
        rhs_slice_target = (object_ptr)&_supplied_name_9978;
        RHS_Slice(_lPath_9797, _5578, _5581);

        /** 			if atom(list) then*/
        _5583 = IS_ATOM(_list_9973);
        if (_5583 == 0)
        {
            _5583 = NOVALUE;
            goto L2A; // [974] 1012
        }
        else{
            _5583 = NOVALUE;
        }

        /** 				if lower_name then*/
        if (_lower_name_9952 == 0)
        {
            goto L2B; // [979] 1005
        }
        else{
        }

        /** 					lPath = part & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_9943);
        _5584 = (int)*(((s1_ptr)_2)->base + _i_9964);
        if (IS_SEQUENCE(_lPath_9797)){
                _5585 = SEQ_PTR(_lPath_9797)->length;
        }
        else {
            _5585 = 1;
        }
        rhs_slice_target = (object_ptr)&_5586;
        RHS_Slice(_lPath_9797, _5584, _5585);
        _5587 = _16lower(_5586);
        _5586 = NOVALUE;
        if (IS_SEQUENCE(_part_9968) && IS_ATOM(_5587)) {
            Ref(_5587);
            Append(&_lPath_9797, _part_9968, _5587);
        }
        else if (IS_ATOM(_part_9968) && IS_SEQUENCE(_5587)) {
        }
        else {
            Concat((object_ptr)&_lPath_9797, _part_9968, _5587);
        }
        DeRef(_5587);
        _5587 = NOVALUE;
L2B: 

        /** 				continue*/
        DeRef(_part_9968);
        _part_9968 = NOVALUE;
        DeRef(_list_9973);
        _list_9973 = NOVALUE;
        DeRef(_supplied_name_9978);
        _supplied_name_9978 = NOVALUE;
        goto L2C; // [1009] 1388
L2A: 

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_9973)){
                _5589 = SEQ_PTR(_list_9973)->length;
        }
        else {
            _5589 = 1;
        }
        {
            int _j_9995;
            _j_9995 = 1;
L2D: 
            if (_j_9995 > _5589){
                goto L2E; // [1017] 1148
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_9973);
            _5590 = (int)*(((s1_ptr)_2)->base + _j_9995);
            DeRef(_read_name_9997);
            _2 = (int)SEQ_PTR(_5590);
            _read_name_9997 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_9997);
            _5590 = NOVALUE;

            /** 				if equal(read_name, supplied_name) then*/
            if (_read_name_9997 == _supplied_name_9978)
            _5592 = 1;
            else if (IS_ATOM_INT(_read_name_9997) && IS_ATOM_INT(_supplied_name_9978))
            _5592 = 0;
            else
            _5592 = (compare(_read_name_9997, _supplied_name_9978) == 0);
            if (_5592 == 0)
            {
                _5592 = NOVALUE;
                goto L2F; // [1044] 1139
            }
            else{
                _5592 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_9946 == 0) {
                goto L30; // [1049] 1130
            }
            _2 = (int)SEQ_PTR(_list_9973);
            _5594 = (int)*(((s1_ptr)_2)->base + _j_9995);
            _2 = (int)SEQ_PTR(_5594);
            _5595 = (int)*(((s1_ptr)_2)->base + 11);
            _5594 = NOVALUE;
            _5596 = IS_SEQUENCE(_5595);
            _5595 = NOVALUE;
            if (_5596 == 0)
            {
                _5596 = NOVALUE;
                goto L30; // [1067] 1130
            }
            else{
                _5596 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_9943);
            _5597 = (int)*(((s1_ptr)_2)->base + _i_9964);
            rhs_slice_target = (object_ptr)&_5598;
            RHS_Slice(_lPath_9797, 1, _5597);
            _2 = (int)SEQ_PTR(_list_9973);
            _5599 = (int)*(((s1_ptr)_2)->base + _j_9995);
            _2 = (int)SEQ_PTR(_5599);
            _5600 = (int)*(((s1_ptr)_2)->base + 11);
            _5599 = NOVALUE;
            _5601 = _i_9964 + 1;
            _2 = (int)SEQ_PTR(_sl_9943);
            _5602 = (int)*(((s1_ptr)_2)->base + _5601);
            if (IS_SEQUENCE(_lPath_9797)){
                    _5603 = SEQ_PTR(_lPath_9797)->length;
            }
            else {
                _5603 = 1;
            }
            rhs_slice_target = (object_ptr)&_5604;
            RHS_Slice(_lPath_9797, _5602, _5603);
            {
                int concat_list[3];

                concat_list[0] = _5604;
                concat_list[1] = _5600;
                concat_list[2] = _5598;
                Concat_N((object_ptr)&_lPath_9797, concat_list, 3);
            }
            DeRefDS(_5604);
            _5604 = NOVALUE;
            _5600 = NOVALUE;
            DeRefDS(_5598);
            _5598 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_9943)){
                    _5606 = SEQ_PTR(_sl_9943)->length;
            }
            else {
                _5606 = 1;
            }
            if (IS_SEQUENCE(_lPath_9797)){
                    _5607 = SEQ_PTR(_lPath_9797)->length;
            }
            else {
                _5607 = 1;
            }
            _5608 = _5607 + 1;
            _5607 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_9943);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_9943 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _5606);
            _1 = *(int *)_2;
            *(int *)_2 = _5608;
            if( _1 != _5608 ){
                DeRef(_1);
            }
            _5608 = NOVALUE;
L30: 

            /** 					continue "partloop"*/
            DeRef(_read_name_9997);
            _read_name_9997 = NOVALUE;
            DeRef(_part_9968);
            _part_9968 = NOVALUE;
            DeRef(_list_9973);
            _list_9973 = NOVALUE;
            DeRef(_supplied_name_9978);
            _supplied_name_9978 = NOVALUE;
            goto L2C; // [1136] 1388
L2F: 
            DeRef(_read_name_9997);
            _read_name_9997 = NOVALUE;

            /** 			end for*/
            _j_9995 = _j_9995 + 1;
            goto L2D; // [1143] 1024
L2E: 
            ;
        }

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_9973)){
                _5609 = SEQ_PTR(_list_9973)->length;
        }
        else {
            _5609 = 1;
        }
        {
            int _j_10020;
            _j_10020 = 1;
L31: 
            if (_j_10020 > _5609){
                goto L32; // [1153] 1331
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_9973);
            _5610 = (int)*(((s1_ptr)_2)->base + _j_10020);
            DeRef(_read_name_10022);
            _2 = (int)SEQ_PTR(_5610);
            _read_name_10022 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_10022);
            _5610 = NOVALUE;

            /** 				if equal(lower(read_name), lower(supplied_name)) then*/
            RefDS(_read_name_10022);
            _5612 = _16lower(_read_name_10022);
            RefDS(_supplied_name_9978);
            _5613 = _16lower(_supplied_name_9978);
            if (_5612 == _5613)
            _5614 = 1;
            else if (IS_ATOM_INT(_5612) && IS_ATOM_INT(_5613))
            _5614 = 0;
            else
            _5614 = (compare(_5612, _5613) == 0);
            DeRef(_5612);
            _5612 = NOVALUE;
            DeRef(_5613);
            _5613 = NOVALUE;
            if (_5614 == 0)
            {
                _5614 = NOVALUE;
                goto L33; // [1188] 1322
            }
            else{
                _5614 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_9946 == 0) {
                goto L34; // [1193] 1274
            }
            _2 = (int)SEQ_PTR(_list_9973);
            _5616 = (int)*(((s1_ptr)_2)->base + _j_10020);
            _2 = (int)SEQ_PTR(_5616);
            _5617 = (int)*(((s1_ptr)_2)->base + 11);
            _5616 = NOVALUE;
            _5618 = IS_SEQUENCE(_5617);
            _5617 = NOVALUE;
            if (_5618 == 0)
            {
                _5618 = NOVALUE;
                goto L34; // [1211] 1274
            }
            else{
                _5618 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_9943);
            _5619 = (int)*(((s1_ptr)_2)->base + _i_9964);
            rhs_slice_target = (object_ptr)&_5620;
            RHS_Slice(_lPath_9797, 1, _5619);
            _2 = (int)SEQ_PTR(_list_9973);
            _5621 = (int)*(((s1_ptr)_2)->base + _j_10020);
            _2 = (int)SEQ_PTR(_5621);
            _5622 = (int)*(((s1_ptr)_2)->base + 11);
            _5621 = NOVALUE;
            _5623 = _i_9964 + 1;
            _2 = (int)SEQ_PTR(_sl_9943);
            _5624 = (int)*(((s1_ptr)_2)->base + _5623);
            if (IS_SEQUENCE(_lPath_9797)){
                    _5625 = SEQ_PTR(_lPath_9797)->length;
            }
            else {
                _5625 = 1;
            }
            rhs_slice_target = (object_ptr)&_5626;
            RHS_Slice(_lPath_9797, _5624, _5625);
            {
                int concat_list[3];

                concat_list[0] = _5626;
                concat_list[1] = _5622;
                concat_list[2] = _5620;
                Concat_N((object_ptr)&_lPath_9797, concat_list, 3);
            }
            DeRefDS(_5626);
            _5626 = NOVALUE;
            _5622 = NOVALUE;
            DeRefDS(_5620);
            _5620 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_9943)){
                    _5628 = SEQ_PTR(_sl_9943)->length;
            }
            else {
                _5628 = 1;
            }
            if (IS_SEQUENCE(_lPath_9797)){
                    _5629 = SEQ_PTR(_lPath_9797)->length;
            }
            else {
                _5629 = 1;
            }
            _5630 = _5629 + 1;
            _5629 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_9943);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_9943 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _5628);
            _1 = *(int *)_2;
            *(int *)_2 = _5630;
            if( _1 != _5630 ){
                DeRef(_1);
            }
            _5630 = NOVALUE;
L34: 

            /** 					if correct_name then*/
            if (_correct_name_9949 == 0)
            {
                goto L35; // [1276] 1313
            }
            else{
            }

            /** 						lPath = lPath[1..sl[i]] & read_name & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_9943);
            _5631 = (int)*(((s1_ptr)_2)->base + _i_9964);
            rhs_slice_target = (object_ptr)&_5632;
            RHS_Slice(_lPath_9797, 1, _5631);
            _5633 = _i_9964 + 1;
            _2 = (int)SEQ_PTR(_sl_9943);
            _5634 = (int)*(((s1_ptr)_2)->base + _5633);
            if (IS_SEQUENCE(_lPath_9797)){
                    _5635 = SEQ_PTR(_lPath_9797)->length;
            }
            else {
                _5635 = 1;
            }
            rhs_slice_target = (object_ptr)&_5636;
            RHS_Slice(_lPath_9797, _5634, _5635);
            {
                int concat_list[3];

                concat_list[0] = _5636;
                concat_list[1] = _read_name_10022;
                concat_list[2] = _5632;
                Concat_N((object_ptr)&_lPath_9797, concat_list, 3);
            }
            DeRefDS(_5636);
            _5636 = NOVALUE;
            DeRefDS(_5632);
            _5632 = NOVALUE;
L35: 

            /** 					continue "partloop"*/
            DeRef(_read_name_10022);
            _read_name_10022 = NOVALUE;
            DeRef(_part_9968);
            _part_9968 = NOVALUE;
            DeRef(_list_9973);
            _list_9973 = NOVALUE;
            DeRef(_supplied_name_9978);
            _supplied_name_9978 = NOVALUE;
            goto L2C; // [1319] 1388
L33: 
            DeRef(_read_name_10022);
            _read_name_10022 = NOVALUE;

            /** 			end for*/
            _j_10020 = _j_10020 + 1;
            goto L31; // [1326] 1160
L32: 
            ;
        }

        /** 			if and_bits(TO_LOWER,case_flags) then*/
        {unsigned long tu;
             tu = (unsigned long)1 & (unsigned long)_case_flags_9796;
             _5638 = MAKE_UINT(tu);
        }
        if (_5638 == 0) {
            DeRef(_5638);
            _5638 = NOVALUE;
            goto L36; // [1339] 1378
        }
        else {
            if (!IS_ATOM_INT(_5638) && DBL_PTR(_5638)->dbl == 0.0){
                DeRef(_5638);
                _5638 = NOVALUE;
                goto L36; // [1339] 1378
            }
            DeRef(_5638);
            _5638 = NOVALUE;
        }
        DeRef(_5638);
        _5638 = NOVALUE;

        /** 				lPath = lPath[1..sl[i]-1] & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_9943);
        _5639 = (int)*(((s1_ptr)_2)->base + _i_9964);
        if (IS_ATOM_INT(_5639)) {
            _5640 = _5639 - 1;
        }
        else {
            _5640 = binary_op(MINUS, _5639, 1);
        }
        _5639 = NOVALUE;
        rhs_slice_target = (object_ptr)&_5641;
        RHS_Slice(_lPath_9797, 1, _5640);
        _2 = (int)SEQ_PTR(_sl_9943);
        _5642 = (int)*(((s1_ptr)_2)->base + _i_9964);
        if (IS_SEQUENCE(_lPath_9797)){
                _5643 = SEQ_PTR(_lPath_9797)->length;
        }
        else {
            _5643 = 1;
        }
        rhs_slice_target = (object_ptr)&_5644;
        RHS_Slice(_lPath_9797, _5642, _5643);
        _5645 = _16lower(_5644);
        _5644 = NOVALUE;
        if (IS_SEQUENCE(_5641) && IS_ATOM(_5645)) {
            Ref(_5645);
            Append(&_lPath_9797, _5641, _5645);
        }
        else if (IS_ATOM(_5641) && IS_SEQUENCE(_5645)) {
        }
        else {
            Concat((object_ptr)&_lPath_9797, _5641, _5645);
            DeRefDS(_5641);
            _5641 = NOVALUE;
        }
        DeRef(_5641);
        _5641 = NOVALUE;
        DeRef(_5645);
        _5645 = NOVALUE;
L36: 

        /** 			exit*/
        DeRef(_part_9968);
        _part_9968 = NOVALUE;
        DeRef(_list_9973);
        _list_9973 = NOVALUE;
        DeRef(_supplied_name_9978);
        _supplied_name_9978 = NOVALUE;
        goto L29; // [1382] 1393

        /** 		end for*/
L2C: 
        _i_9964 = _i_9964 + -1;
        goto L28; // [1388] 895
L29: 
        ;
    }

    /** 		if and_bits(case_flags,or_bits(CORRECT,TO_LOWER))=TO_LOWER and length(lPath) then*/
    {unsigned long tu;
         tu = (unsigned long)2 | (unsigned long)1;
         _5647 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_5647)) {
        {unsigned long tu;
             tu = (unsigned long)_case_flags_9796 & (unsigned long)_5647;
             _5648 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_case_flags_9796;
        _5648 = Dand_bits(&temp_d, DBL_PTR(_5647));
    }
    DeRef(_5647);
    _5647 = NOVALUE;
    if (IS_ATOM_INT(_5648)) {
        _5649 = (_5648 == 1);
    }
    else {
        _5649 = (DBL_PTR(_5648)->dbl == (double)1);
    }
    DeRef(_5648);
    _5648 = NOVALUE;
    if (_5649 == 0) {
        goto L37; // [1413] 1433
    }
    if (IS_SEQUENCE(_lPath_9797)){
            _5651 = SEQ_PTR(_lPath_9797)->length;
    }
    else {
        _5651 = 1;
    }
    if (_5651 == 0)
    {
        _5651 = NOVALUE;
        goto L37; // [1421] 1433
    }
    else{
        _5651 = NOVALUE;
    }

    /** 			lPath = lower(lPath)*/
    RefDS(_lPath_9797);
    _0 = _lPath_9797;
    _lPath_9797 = _16lower(_lPath_9797);
    DeRefDS(_0);
L37: 
L26: 
    DeRef(_sl_9943);
    _sl_9943 = NOVALUE;
L25: 

    /** 	ifdef WINDOWS then*/

    /** 		if and_bits(CORRECT,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)2 & (unsigned long)_case_flags_9796;
         _5653 = MAKE_UINT(tu);
    }
    if (_5653 == 0) {
        DeRef(_5653);
        _5653 = NOVALUE;
        goto L38; // [1447] 1489
    }
    else {
        if (!IS_ATOM_INT(_5653) && DBL_PTR(_5653)->dbl == 0.0){
            DeRef(_5653);
            _5653 = NOVALUE;
            goto L38; // [1447] 1489
        }
        DeRef(_5653);
        _5653 = NOVALUE;
    }
    DeRef(_5653);
    _5653 = NOVALUE;

    /** 			if or_bits(system_drive_case,'A') = 'a' then*/
    if (IS_ATOM_INT(_8system_drive_case_9780)) {
        {unsigned long tu;
             tu = (unsigned long)_8system_drive_case_9780 | (unsigned long)65;
             _5654 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)65;
        _5654 = Dor_bits(DBL_PTR(_8system_drive_case_9780), &temp_d);
    }
    if (binary_op_a(NOTEQ, _5654, 97)){
        DeRef(_5654);
        _5654 = NOVALUE;
        goto L39; // [1458] 1475
    }
    DeRef(_5654);
    _5654 = NOVALUE;

    /** 				lDrive = lower(lDrive)*/
    RefDS(_lDrive_9802);
    _0 = _lDrive_9802;
    _lDrive_9802 = _16lower(_lDrive_9802);
    DeRefDS(_0);
    goto L3A; // [1472] 1512
L39: 

    /** 				lDrive = upper(lDrive)*/
    RefDS(_lDrive_9802);
    _0 = _lDrive_9802;
    _lDrive_9802 = _16upper(_lDrive_9802);
    DeRefDS(_0);
    goto L3A; // [1486] 1512
L38: 

    /** 		elsif and_bits(TO_LOWER,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_9796;
         _5658 = MAKE_UINT(tu);
    }
    if (_5658 == 0) {
        DeRef(_5658);
        _5658 = NOVALUE;
        goto L3B; // [1497] 1511
    }
    else {
        if (!IS_ATOM_INT(_5658) && DBL_PTR(_5658)->dbl == 0.0){
            DeRef(_5658);
            _5658 = NOVALUE;
            goto L3B; // [1497] 1511
        }
        DeRef(_5658);
        _5658 = NOVALUE;
    }
    DeRef(_5658);
    _5658 = NOVALUE;

    /** 			lDrive = lower(lDrive)*/
    RefDS(_lDrive_9802);
    _0 = _lDrive_9802;
    _lDrive_9802 = _16lower(_lDrive_9802);
    DeRefDS(_0);
L3B: 
L3A: 

    /** 		lPath = lDrive & lPath*/
    Concat((object_ptr)&_lPath_9797, _lDrive_9802, _lPath_9797);

    /** 	return lPath & wildcard_suffix*/
    Concat((object_ptr)&_5661, _lPath_9797, _wildcard_suffix_9867);
    DeRefDS(_path_in_9794);
    DeRefDS(_lPath_9797);
    DeRefi(_lLevel_9800);
    DeRef(_lHome_9801);
    DeRefDS(_lDrive_9802);
    DeRefDS(_wildcard_suffix_9867);
    DeRef(_5573);
    _5573 = NOVALUE;
    DeRef(_5491);
    _5491 = NOVALUE;
    DeRef(_5529);
    _5529 = NOVALUE;
    DeRef(_5623);
    _5623 = NOVALUE;
    DeRef(_5570);
    _5570 = NOVALUE;
    _5624 = NOVALUE;
    DeRef(_5458);
    _5458 = NOVALUE;
    DeRef(_5523);
    _5523 = NOVALUE;
    DeRef(_5480);
    _5480 = NOVALUE;
    DeRef(_5506);
    _5506 = NOVALUE;
    DeRef(_5546);
    _5546 = NOVALUE;
    DeRef(_5543);
    _5543 = NOVALUE;
    DeRef(_5451);
    _5451 = NOVALUE;
    DeRef(_5581);
    _5581 = NOVALUE;
    _5597 = NOVALUE;
    DeRef(_5633);
    _5633 = NOVALUE;
    DeRef(_5649);
    _5649 = NOVALUE;
    _5631 = NOVALUE;
    DeRef(_5511);
    _5511 = NOVALUE;
    _5642 = NOVALUE;
    DeRef(_5520);
    _5520 = NOVALUE;
    DeRef(_5454);
    _5454 = NOVALUE;
    DeRef(_5579);
    _5579 = NOVALUE;
    _5584 = NOVALUE;
    _5619 = NOVALUE;
    DeRef(_5533);
    _5533 = NOVALUE;
    DeRef(_5601);
    _5601 = NOVALUE;
    _5634 = NOVALUE;
    DeRef(_5466);
    _5466 = NOVALUE;
    _5602 = NOVALUE;
    DeRef(_5460);
    _5460 = NOVALUE;
    DeRef(_5640);
    _5640 = NOVALUE;
    DeRef(_5463);
    _5463 = NOVALUE;
    DeRef(_5494);
    _5494 = NOVALUE;
    DeRef(_5578);
    _5578 = NOVALUE;
    return _5661;
    ;
}


int _8fs_case(int _s_10093)
{
    int _5662 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return lower(s)*/
    RefDS(_s_10093);
    _5662 = _16lower(_s_10093);
    DeRefDS(_s_10093);
    return _5662;
    ;
}


int _8abbreviate_path(int _orig_path_10098, int _base_paths_10099)
{
    int _expanded_path_10100 = NOVALUE;
    int _lowered_expanded_path_10110 = NOVALUE;
    int _5707 = NOVALUE;
    int _5706 = NOVALUE;
    int _5703 = NOVALUE;
    int _5702 = NOVALUE;
    int _5701 = NOVALUE;
    int _5700 = NOVALUE;
    int _5699 = NOVALUE;
    int _5697 = NOVALUE;
    int _5696 = NOVALUE;
    int _5695 = NOVALUE;
    int _5694 = NOVALUE;
    int _5693 = NOVALUE;
    int _5692 = NOVALUE;
    int _5691 = NOVALUE;
    int _5690 = NOVALUE;
    int _5689 = NOVALUE;
    int _5686 = NOVALUE;
    int _5685 = NOVALUE;
    int _5683 = NOVALUE;
    int _5682 = NOVALUE;
    int _5681 = NOVALUE;
    int _5680 = NOVALUE;
    int _5679 = NOVALUE;
    int _5678 = NOVALUE;
    int _5677 = NOVALUE;
    int _5676 = NOVALUE;
    int _5675 = NOVALUE;
    int _5674 = NOVALUE;
    int _5673 = NOVALUE;
    int _5672 = NOVALUE;
    int _5671 = NOVALUE;
    int _5668 = NOVALUE;
    int _5667 = NOVALUE;
    int _5666 = NOVALUE;
    int _5664 = NOVALUE;
    int _0, _1, _2;
    

    /** 	expanded_path = canonical_path(orig_path)*/
    RefDS(_orig_path_10098);
    _0 = _expanded_path_10100;
    _expanded_path_10100 = _8canonical_path(_orig_path_10098, 0, 0);
    DeRef(_0);

    /** 	base_paths = append(base_paths, curdir())*/
    _5664 = _8curdir(0);
    Ref(_5664);
    Append(&_base_paths_10099, _base_paths_10099, _5664);
    DeRef(_5664);
    _5664 = NOVALUE;

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_10099)){
            _5666 = SEQ_PTR(_base_paths_10099)->length;
    }
    else {
        _5666 = 1;
    }
    {
        int _i_10105;
        _i_10105 = 1;
L1: 
        if (_i_10105 > _5666){
            goto L2; // [32] 64
        }

        /** 		base_paths[i] = canonical_path(base_paths[i], 1) -- assume each base path is meant to be a directory.*/
        _2 = (int)SEQ_PTR(_base_paths_10099);
        _5667 = (int)*(((s1_ptr)_2)->base + _i_10105);
        Ref(_5667);
        _5668 = _8canonical_path(_5667, 1, 0);
        _5667 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_10099);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _base_paths_10099 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_10105);
        _1 = *(int *)_2;
        *(int *)_2 = _5668;
        if( _1 != _5668 ){
            DeRef(_1);
        }
        _5668 = NOVALUE;

        /** 	end for*/
        _i_10105 = _i_10105 + 1;
        goto L1; // [59] 39
L2: 
        ;
    }

    /** 	base_paths = fs_case(base_paths)*/
    RefDS(_base_paths_10099);
    _0 = _base_paths_10099;
    _base_paths_10099 = _8fs_case(_base_paths_10099);
    DeRefDS(_0);

    /** 	sequence lowered_expanded_path = fs_case(expanded_path)*/
    RefDS(_expanded_path_10100);
    _0 = _lowered_expanded_path_10110;
    _lowered_expanded_path_10110 = _8fs_case(_expanded_path_10100);
    DeRef(_0);

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_10099)){
            _5671 = SEQ_PTR(_base_paths_10099)->length;
    }
    else {
        _5671 = 1;
    }
    {
        int _i_10113;
        _i_10113 = 1;
L3: 
        if (_i_10113 > _5671){
            goto L4; // [85] 139
        }

        /** 		if search:begins(base_paths[i], lowered_expanded_path) then*/
        _2 = (int)SEQ_PTR(_base_paths_10099);
        _5672 = (int)*(((s1_ptr)_2)->base + _i_10113);
        Ref(_5672);
        RefDS(_lowered_expanded_path_10110);
        _5673 = _6begins(_5672, _lowered_expanded_path_10110);
        _5672 = NOVALUE;
        if (_5673 == 0) {
            DeRef(_5673);
            _5673 = NOVALUE;
            goto L5; // [103] 132
        }
        else {
            if (!IS_ATOM_INT(_5673) && DBL_PTR(_5673)->dbl == 0.0){
                DeRef(_5673);
                _5673 = NOVALUE;
                goto L5; // [103] 132
            }
            DeRef(_5673);
            _5673 = NOVALUE;
        }
        DeRef(_5673);
        _5673 = NOVALUE;

        /** 			return expanded_path[length(base_paths[i]) + 1 .. $]*/
        _2 = (int)SEQ_PTR(_base_paths_10099);
        _5674 = (int)*(((s1_ptr)_2)->base + _i_10113);
        if (IS_SEQUENCE(_5674)){
                _5675 = SEQ_PTR(_5674)->length;
        }
        else {
            _5675 = 1;
        }
        _5674 = NOVALUE;
        _5676 = _5675 + 1;
        _5675 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_10100)){
                _5677 = SEQ_PTR(_expanded_path_10100)->length;
        }
        else {
            _5677 = 1;
        }
        rhs_slice_target = (object_ptr)&_5678;
        RHS_Slice(_expanded_path_10100, _5676, _5677);
        DeRefDS(_orig_path_10098);
        DeRefDS(_base_paths_10099);
        DeRefDS(_expanded_path_10100);
        DeRefDS(_lowered_expanded_path_10110);
        _5674 = NOVALUE;
        _5676 = NOVALUE;
        return _5678;
L5: 

        /** 	end for*/
        _i_10113 = _i_10113 + 1;
        goto L3; // [134] 92
L4: 
        ;
    }

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(base_paths[$][1], lowered_expanded_path[1]) then*/
    if (IS_SEQUENCE(_base_paths_10099)){
            _5679 = SEQ_PTR(_base_paths_10099)->length;
    }
    else {
        _5679 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_10099);
    _5680 = (int)*(((s1_ptr)_2)->base + _5679);
    _2 = (int)SEQ_PTR(_5680);
    _5681 = (int)*(((s1_ptr)_2)->base + 1);
    _5680 = NOVALUE;
    _2 = (int)SEQ_PTR(_lowered_expanded_path_10110);
    _5682 = (int)*(((s1_ptr)_2)->base + 1);
    if (_5681 == _5682)
    _5683 = 1;
    else if (IS_ATOM_INT(_5681) && IS_ATOM_INT(_5682))
    _5683 = 0;
    else
    _5683 = (compare(_5681, _5682) == 0);
    _5681 = NOVALUE;
    _5682 = NOVALUE;
    if (_5683 != 0)
    goto L6; // [162] 172
    _5683 = NOVALUE;

    /** 			return orig_path*/
    DeRefDS(_base_paths_10099);
    DeRef(_expanded_path_10100);
    DeRefDS(_lowered_expanded_path_10110);
    _5674 = NOVALUE;
    DeRef(_5676);
    _5676 = NOVALUE;
    DeRef(_5678);
    _5678 = NOVALUE;
    return _orig_path_10098;
L6: 

    /** 	base_paths = stdseq:split(base_paths[$], SLASH)*/
    if (IS_SEQUENCE(_base_paths_10099)){
            _5685 = SEQ_PTR(_base_paths_10099)->length;
    }
    else {
        _5685 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_10099);
    _5686 = (int)*(((s1_ptr)_2)->base + _5685);
    Ref(_5686);
    _0 = _base_paths_10099;
    _base_paths_10099 = _3split(_5686, 92, 0, 0);
    DeRefDS(_0);
    _5686 = NOVALUE;

    /** 	expanded_path = stdseq:split(expanded_path, SLASH)*/
    RefDS(_expanded_path_10100);
    _0 = _expanded_path_10100;
    _expanded_path_10100 = _3split(_expanded_path_10100, 92, 0, 0);
    DeRefDS(_0);

    /** 	lowered_expanded_path = ""*/
    RefDS(_5);
    DeRef(_lowered_expanded_path_10110);
    _lowered_expanded_path_10110 = _5;

    /** 	for i = 1 to math:min({length(expanded_path), length(base_paths) - 1}) do*/
    if (IS_SEQUENCE(_expanded_path_10100)){
            _5689 = SEQ_PTR(_expanded_path_10100)->length;
    }
    else {
        _5689 = 1;
    }
    if (IS_SEQUENCE(_base_paths_10099)){
            _5690 = SEQ_PTR(_base_paths_10099)->length;
    }
    else {
        _5690 = 1;
    }
    _5691 = _5690 - 1;
    _5690 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5689;
    ((int *)_2)[2] = _5691;
    _5692 = MAKE_SEQ(_1);
    _5691 = NOVALUE;
    _5689 = NOVALUE;
    _5693 = _18min(_5692);
    _5692 = NOVALUE;
    {
        int _i_10135;
        _i_10135 = 1;
L7: 
        if (binary_op_a(GREATER, _i_10135, _5693)){
            goto L8; // [228] 321
        }

        /** 		if not equal(fs_case(expanded_path[i]), base_paths[i]) then*/
        _2 = (int)SEQ_PTR(_expanded_path_10100);
        if (!IS_ATOM_INT(_i_10135)){
            _5694 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_10135)->dbl));
        }
        else{
            _5694 = (int)*(((s1_ptr)_2)->base + _i_10135);
        }
        Ref(_5694);
        _5695 = _8fs_case(_5694);
        _5694 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_10099);
        if (!IS_ATOM_INT(_i_10135)){
            _5696 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_10135)->dbl));
        }
        else{
            _5696 = (int)*(((s1_ptr)_2)->base + _i_10135);
        }
        if (_5695 == _5696)
        _5697 = 1;
        else if (IS_ATOM_INT(_5695) && IS_ATOM_INT(_5696))
        _5697 = 0;
        else
        _5697 = (compare(_5695, _5696) == 0);
        DeRef(_5695);
        _5695 = NOVALUE;
        _5696 = NOVALUE;
        if (_5697 != 0)
        goto L9; // [253] 314
        _5697 = NOVALUE;

        /** 			expanded_path = repeat("..", length(base_paths) - i) & expanded_path[i .. $]*/
        if (IS_SEQUENCE(_base_paths_10099)){
                _5699 = SEQ_PTR(_base_paths_10099)->length;
        }
        else {
            _5699 = 1;
        }
        if (IS_ATOM_INT(_i_10135)) {
            _5700 = _5699 - _i_10135;
        }
        else {
            _5700 = NewDouble((double)_5699 - DBL_PTR(_i_10135)->dbl);
        }
        _5699 = NOVALUE;
        _5701 = Repeat(_5213, _5700);
        DeRef(_5700);
        _5700 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_10100)){
                _5702 = SEQ_PTR(_expanded_path_10100)->length;
        }
        else {
            _5702 = 1;
        }
        rhs_slice_target = (object_ptr)&_5703;
        RHS_Slice(_expanded_path_10100, _i_10135, _5702);
        Concat((object_ptr)&_expanded_path_10100, _5701, _5703);
        DeRefDS(_5701);
        _5701 = NOVALUE;
        DeRef(_5701);
        _5701 = NOVALUE;
        DeRefDS(_5703);
        _5703 = NOVALUE;

        /** 			expanded_path = stdseq:join(expanded_path, SLASH)*/
        RefDS(_expanded_path_10100);
        _0 = _expanded_path_10100;
        _expanded_path_10100 = _3join(_expanded_path_10100, 92);
        DeRefDS(_0);

        /** 			if length(expanded_path) < length(orig_path) then*/
        if (IS_SEQUENCE(_expanded_path_10100)){
                _5706 = SEQ_PTR(_expanded_path_10100)->length;
        }
        else {
            _5706 = 1;
        }
        if (IS_SEQUENCE(_orig_path_10098)){
                _5707 = SEQ_PTR(_orig_path_10098)->length;
        }
        else {
            _5707 = 1;
        }
        if (_5706 >= _5707)
        goto L8; // [298] 321

        /** 		  		return expanded_path*/
        DeRef(_i_10135);
        DeRefDS(_orig_path_10098);
        DeRefDS(_base_paths_10099);
        DeRef(_lowered_expanded_path_10110);
        _5674 = NOVALUE;
        DeRef(_5676);
        _5676 = NOVALUE;
        DeRef(_5678);
        _5678 = NOVALUE;
        DeRef(_5693);
        _5693 = NOVALUE;
        return _expanded_path_10100;

        /** 			exit*/
        goto L8; // [311] 321
L9: 

        /** 	end for*/
        _0 = _i_10135;
        if (IS_ATOM_INT(_i_10135)) {
            _i_10135 = _i_10135 + 1;
            if ((long)((unsigned long)_i_10135 +(unsigned long) HIGH_BITS) >= 0){
                _i_10135 = NewDouble((double)_i_10135);
            }
        }
        else {
            _i_10135 = binary_op_a(PLUS, _i_10135, 1);
        }
        DeRef(_0);
        goto L7; // [316] 235
L8: 
        ;
        DeRef(_i_10135);
    }

    /** 	return orig_path*/
    DeRefDS(_base_paths_10099);
    DeRef(_expanded_path_10100);
    DeRef(_lowered_expanded_path_10110);
    _5674 = NOVALUE;
    DeRef(_5676);
    _5676 = NOVALUE;
    DeRef(_5678);
    _5678 = NOVALUE;
    DeRef(_5693);
    _5693 = NOVALUE;
    return _orig_path_10098;
    ;
}


int _8split_path(int _fname_10160)
{
    int _5709 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return stdseq:split(fname, SLASH, 1)*/
    RefDS(_fname_10160);
    _5709 = _3split(_fname_10160, 92, 1, 0);
    DeRefDS(_fname_10160);
    return _5709;
    ;
}


int _8join_path(int _path_elements_10164)
{
    int _fname_10165 = NOVALUE;
    int _elem_10169 = NOVALUE;
    int _5723 = NOVALUE;
    int _5722 = NOVALUE;
    int _5721 = NOVALUE;
    int _5720 = NOVALUE;
    int _5719 = NOVALUE;
    int _5718 = NOVALUE;
    int _5716 = NOVALUE;
    int _5715 = NOVALUE;
    int _5713 = NOVALUE;
    int _5712 = NOVALUE;
    int _5710 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fname = ""*/
    RefDS(_5);
    DeRef(_fname_10165);
    _fname_10165 = _5;

    /** 	for i = 1 to length(path_elements) do*/
    if (IS_SEQUENCE(_path_elements_10164)){
            _5710 = SEQ_PTR(_path_elements_10164)->length;
    }
    else {
        _5710 = 1;
    }
    {
        int _i_10167;
        _i_10167 = 1;
L1: 
        if (_i_10167 > _5710){
            goto L2; // [15] 117
        }

        /** 		sequence elem = path_elements[i]*/
        DeRef(_elem_10169);
        _2 = (int)SEQ_PTR(_path_elements_10164);
        _elem_10169 = (int)*(((s1_ptr)_2)->base + _i_10167);
        Ref(_elem_10169);

        /** 		if elem[$] = SLASH then*/
        if (IS_SEQUENCE(_elem_10169)){
                _5712 = SEQ_PTR(_elem_10169)->length;
        }
        else {
            _5712 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_10169);
        _5713 = (int)*(((s1_ptr)_2)->base + _5712);
        if (binary_op_a(NOTEQ, _5713, 92)){
            _5713 = NOVALUE;
            goto L3; // [39] 58
        }
        _5713 = NOVALUE;

        /** 			elem = elem[1..$ - 1]*/
        if (IS_SEQUENCE(_elem_10169)){
                _5715 = SEQ_PTR(_elem_10169)->length;
        }
        else {
            _5715 = 1;
        }
        _5716 = _5715 - 1;
        _5715 = NOVALUE;
        rhs_slice_target = (object_ptr)&_elem_10169;
        RHS_Slice(_elem_10169, 1, _5716);
L3: 

        /** 		if length(elem) and elem[1] != SLASH then*/
        if (IS_SEQUENCE(_elem_10169)){
                _5718 = SEQ_PTR(_elem_10169)->length;
        }
        else {
            _5718 = 1;
        }
        if (_5718 == 0) {
            goto L4; // [63] 102
        }
        _2 = (int)SEQ_PTR(_elem_10169);
        _5720 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_5720)) {
            _5721 = (_5720 != 92);
        }
        else {
            _5721 = binary_op(NOTEQ, _5720, 92);
        }
        _5720 = NOVALUE;
        if (_5721 == 0) {
            DeRef(_5721);
            _5721 = NOVALUE;
            goto L4; // [76] 102
        }
        else {
            if (!IS_ATOM_INT(_5721) && DBL_PTR(_5721)->dbl == 0.0){
                DeRef(_5721);
                _5721 = NOVALUE;
                goto L4; // [76] 102
            }
            DeRef(_5721);
            _5721 = NOVALUE;
        }
        DeRef(_5721);
        _5721 = NOVALUE;

        /** 			ifdef WINDOWS then*/

        /** 				if elem[$] != ':' then*/
        if (IS_SEQUENCE(_elem_10169)){
                _5722 = SEQ_PTR(_elem_10169)->length;
        }
        else {
            _5722 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_10169);
        _5723 = (int)*(((s1_ptr)_2)->base + _5722);
        if (binary_op_a(EQUALS, _5723, 58)){
            _5723 = NOVALUE;
            goto L5; // [90] 101
        }
        _5723 = NOVALUE;

        /** 					elem = SLASH & elem*/
        Prepend(&_elem_10169, _elem_10169, 92);
L5: 
L4: 

        /** 		fname &= elem*/
        Concat((object_ptr)&_fname_10165, _fname_10165, _elem_10169);
        DeRefDS(_elem_10169);
        _elem_10169 = NOVALUE;

        /** 	end for*/
        _i_10167 = _i_10167 + 1;
        goto L1; // [112] 22
L2: 
        ;
    }

    /** 	return fname*/
    DeRefDS(_path_elements_10164);
    DeRef(_5716);
    _5716 = NOVALUE;
    return _fname_10165;
    ;
}


int _8file_type(int _filename_10198)
{
    int _dirfil_10199 = NOVALUE;
    int _dir_inlined_dir_at_66_10212 = NOVALUE;
    int _5751 = NOVALUE;
    int _5750 = NOVALUE;
    int _5749 = NOVALUE;
    int _5748 = NOVALUE;
    int _5747 = NOVALUE;
    int _5745 = NOVALUE;
    int _5744 = NOVALUE;
    int _5743 = NOVALUE;
    int _5742 = NOVALUE;
    int _5741 = NOVALUE;
    int _5740 = NOVALUE;
    int _5739 = NOVALUE;
    int _5737 = NOVALUE;
    int _5736 = NOVALUE;
    int _5735 = NOVALUE;
    int _5734 = NOVALUE;
    int _5733 = NOVALUE;
    int _5732 = NOVALUE;
    int _5730 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if eu:find('*', filename) or eu:find('?', filename) then return FILETYPE_UNDEFINED end if*/
    _5730 = find_from(42, _filename_10198, 1);
    if (_5730 != 0) {
        goto L1; // [10] 24
    }
    _5732 = find_from(63, _filename_10198, 1);
    if (_5732 == 0)
    {
        _5732 = NOVALUE;
        goto L2; // [20] 31
    }
    else{
        _5732 = NOVALUE;
    }
L1: 
    DeRefDS(_filename_10198);
    DeRef(_dirfil_10199);
    return -1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 2 and filename[2] = ':' then*/
    if (IS_SEQUENCE(_filename_10198)){
            _5733 = SEQ_PTR(_filename_10198)->length;
    }
    else {
        _5733 = 1;
    }
    _5734 = (_5733 == 2);
    _5733 = NOVALUE;
    if (_5734 == 0) {
        goto L3; // [42] 65
    }
    _2 = (int)SEQ_PTR(_filename_10198);
    _5736 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5736)) {
        _5737 = (_5736 == 58);
    }
    else {
        _5737 = binary_op(EQUALS, _5736, 58);
    }
    _5736 = NOVALUE;
    if (_5737 == 0) {
        DeRef(_5737);
        _5737 = NOVALUE;
        goto L3; // [55] 65
    }
    else {
        if (!IS_ATOM_INT(_5737) && DBL_PTR(_5737)->dbl == 0.0){
            DeRef(_5737);
            _5737 = NOVALUE;
            goto L3; // [55] 65
        }
        DeRef(_5737);
        _5737 = NOVALUE;
    }
    DeRef(_5737);
    _5737 = NOVALUE;

    /** 			filename &= "\\"*/
    Concat((object_ptr)&_filename_10198, _filename_10198, _2185);
L3: 

    /** 	dirfil = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_dirfil_10199);
    _dirfil_10199 = machine(22, _filename_10198);

    /** 	if sequence(dirfil) then*/
    _5739 = IS_SEQUENCE(_dirfil_10199);
    if (_5739 == 0)
    {
        _5739 = NOVALUE;
        goto L4; // [81] 169
    }
    else{
        _5739 = NOVALUE;
    }

    /** 		if length( dirfil ) > 1 or eu:find('d', dirfil[1][2]) or (length(filename)=3 and filename[2]=':') then*/
    if (IS_SEQUENCE(_dirfil_10199)){
            _5740 = SEQ_PTR(_dirfil_10199)->length;
    }
    else {
        _5740 = 1;
    }
    _5741 = (_5740 > 1);
    _5740 = NOVALUE;
    if (_5741 != 0) {
        _5742 = 1;
        goto L5; // [93] 114
    }
    _2 = (int)SEQ_PTR(_dirfil_10199);
    _5743 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5743);
    _5744 = (int)*(((s1_ptr)_2)->base + 2);
    _5743 = NOVALUE;
    _5745 = find_from(100, _5744, 1);
    _5744 = NOVALUE;
    _5742 = (_5745 != 0);
L5: 
    if (_5742 != 0) {
        goto L6; // [114] 146
    }
    if (IS_SEQUENCE(_filename_10198)){
            _5747 = SEQ_PTR(_filename_10198)->length;
    }
    else {
        _5747 = 1;
    }
    _5748 = (_5747 == 3);
    _5747 = NOVALUE;
    if (_5748 == 0) {
        DeRef(_5749);
        _5749 = 0;
        goto L7; // [125] 141
    }
    _2 = (int)SEQ_PTR(_filename_10198);
    _5750 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5750)) {
        _5751 = (_5750 == 58);
    }
    else {
        _5751 = binary_op(EQUALS, _5750, 58);
    }
    _5750 = NOVALUE;
    if (IS_ATOM_INT(_5751))
    _5749 = (_5751 != 0);
    else
    _5749 = DBL_PTR(_5751)->dbl != 0.0;
L7: 
    if (_5749 == 0)
    {
        _5749 = NOVALUE;
        goto L8; // [142] 157
    }
    else{
        _5749 = NOVALUE;
    }
L6: 

    /** 			return FILETYPE_DIRECTORY*/
    DeRefDS(_filename_10198);
    DeRef(_dirfil_10199);
    DeRef(_5734);
    _5734 = NOVALUE;
    DeRef(_5741);
    _5741 = NOVALUE;
    DeRef(_5748);
    _5748 = NOVALUE;
    DeRef(_5751);
    _5751 = NOVALUE;
    return 2;
    goto L9; // [154] 178
L8: 

    /** 			return FILETYPE_FILE*/
    DeRefDS(_filename_10198);
    DeRef(_dirfil_10199);
    DeRef(_5734);
    _5734 = NOVALUE;
    DeRef(_5741);
    _5741 = NOVALUE;
    DeRef(_5748);
    _5748 = NOVALUE;
    DeRef(_5751);
    _5751 = NOVALUE;
    return 1;
    goto L9; // [166] 178
L4: 

    /** 		return FILETYPE_NOT_FOUND*/
    DeRefDS(_filename_10198);
    DeRef(_dirfil_10199);
    DeRef(_5734);
    _5734 = NOVALUE;
    DeRef(_5741);
    _5741 = NOVALUE;
    DeRef(_5748);
    _5748 = NOVALUE;
    DeRef(_5751);
    _5751 = NOVALUE;
    return 0;
L9: 
    ;
}


int _8file_exists(int _name_10256)
{
    int _pName_10259 = NOVALUE;
    int _r_10262 = NOVALUE;
    int _5766 = NOVALUE;
    int _5764 = NOVALUE;
    int _5762 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(name) then*/
    _5762 = IS_ATOM(_name_10256);
    if (_5762 == 0)
    {
        _5762 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _5762 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_name_10256);
    DeRef(_pName_10259);
    DeRef(_r_10262);
    return 0;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom pName = allocate_string(name)*/
    Ref(_name_10256);
    _0 = _pName_10259;
    _pName_10259 = _11allocate_string(_name_10256, 0);
    DeRef(_0);

    /** 		atom r = c_func(xGetFileAttributes, {pName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pName_10259);
    *((int *)(_2+4)) = _pName_10259;
    _5764 = MAKE_SEQ(_1);
    DeRef(_r_10262);
    _r_10262 = call_c(1, _8xGetFileAttributes_9233, _5764);
    DeRefDS(_5764);
    _5764 = NOVALUE;

    /** 		free(pName)*/
    Ref(_pName_10259);
    _11free(_pName_10259);

    /** 		return r > 0*/
    if (IS_ATOM_INT(_r_10262)) {
        _5766 = (_r_10262 > 0);
    }
    else {
        _5766 = (DBL_PTR(_r_10262)->dbl > (double)0);
    }
    DeRef(_name_10256);
    DeRef(_pName_10259);
    DeRef(_r_10262);
    return _5766;
    ;
}


int _8file_timestamp(int _fname_10269)
{
    int _d_10270 = NOVALUE;
    int _dir_inlined_dir_at_4_10272 = NOVALUE;
    int _5780 = NOVALUE;
    int _5779 = NOVALUE;
    int _5778 = NOVALUE;
    int _5777 = NOVALUE;
    int _5776 = NOVALUE;
    int _5775 = NOVALUE;
    int _5774 = NOVALUE;
    int _5773 = NOVALUE;
    int _5772 = NOVALUE;
    int _5771 = NOVALUE;
    int _5770 = NOVALUE;
    int _5769 = NOVALUE;
    int _5768 = NOVALUE;
    int _5767 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d = dir(fname)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_d_10270);
    _d_10270 = machine(22, _fname_10269);

    /** 	if atom(d) then return -1 end if*/
    _5767 = IS_ATOM(_d_10270);
    if (_5767 == 0)
    {
        _5767 = NOVALUE;
        goto L1; // [19] 27
    }
    else{
        _5767 = NOVALUE;
    }
    DeRefDS(_fname_10269);
    DeRef(_d_10270);
    return -1;
L1: 

    /** 	return datetime:new(d[1][D_YEAR], d[1][D_MONTH], d[1][D_DAY],*/
    _2 = (int)SEQ_PTR(_d_10270);
    _5768 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5768);
    _5769 = (int)*(((s1_ptr)_2)->base + 4);
    _5768 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_10270);
    _5770 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5770);
    _5771 = (int)*(((s1_ptr)_2)->base + 5);
    _5770 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_10270);
    _5772 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5772);
    _5773 = (int)*(((s1_ptr)_2)->base + 6);
    _5772 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_10270);
    _5774 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5774);
    _5775 = (int)*(((s1_ptr)_2)->base + 7);
    _5774 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_10270);
    _5776 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5776);
    _5777 = (int)*(((s1_ptr)_2)->base + 8);
    _5776 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_10270);
    _5778 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5778);
    _5779 = (int)*(((s1_ptr)_2)->base + 9);
    _5778 = NOVALUE;
    Ref(_5769);
    Ref(_5771);
    Ref(_5773);
    Ref(_5775);
    Ref(_5777);
    Ref(_5779);
    _5780 = _9new(_5769, _5771, _5773, _5775, _5777, _5779);
    _5769 = NOVALUE;
    _5771 = NOVALUE;
    _5773 = NOVALUE;
    _5775 = NOVALUE;
    _5777 = NOVALUE;
    _5779 = NOVALUE;
    DeRefDS(_fname_10269);
    DeRef(_d_10270);
    return _5780;
    ;
}


int _8copy_file(int _src_10290, int _dest_10291, int _overwrite_10292)
{
    int _info_10303 = NOVALUE;
    int _psrc_10307 = NOVALUE;
    int _pdest_10310 = NOVALUE;
    int _success_10313 = NOVALUE;
    int _5796 = NOVALUE;
    int _5794 = NOVALUE;
    int _5793 = NOVALUE;
    int _5789 = NOVALUE;
    int _5785 = NOVALUE;
    int _5784 = NOVALUE;
    int _5782 = NOVALUE;
    int _5781 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_10292)) {
        _1 = (long)(DBL_PTR(_overwrite_10292)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_10292)) && (DBL_PTR(_overwrite_10292)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_10292);
        _overwrite_10292 = _1;
    }

    /** 	if length(dest) then*/
    if (IS_SEQUENCE(_dest_10291)){
            _5781 = SEQ_PTR(_dest_10291)->length;
    }
    else {
        _5781 = 1;
    }
    if (_5781 == 0)
    {
        _5781 = NOVALUE;
        goto L1; // [14] 74
    }
    else{
        _5781 = NOVALUE;
    }

    /** 		if file_type( dest ) = FILETYPE_DIRECTORY then*/
    RefDS(_dest_10291);
    _5782 = _8file_type(_dest_10291);
    if (binary_op_a(NOTEQ, _5782, 2)){
        DeRef(_5782);
        _5782 = NOVALUE;
        goto L2; // [25] 71
    }
    DeRef(_5782);
    _5782 = NOVALUE;

    /** 			if dest[$] != SLASH then*/
    if (IS_SEQUENCE(_dest_10291)){
            _5784 = SEQ_PTR(_dest_10291)->length;
    }
    else {
        _5784 = 1;
    }
    _2 = (int)SEQ_PTR(_dest_10291);
    _5785 = (int)*(((s1_ptr)_2)->base + _5784);
    if (binary_op_a(EQUALS, _5785, 92)){
        _5785 = NOVALUE;
        goto L3; // [38] 49
    }
    _5785 = NOVALUE;

    /** 				dest &= SLASH*/
    Append(&_dest_10291, _dest_10291, 92);
L3: 

    /** 			sequence info = pathinfo( src )*/
    RefDS(_src_10290);
    _0 = _info_10303;
    _info_10303 = _8pathinfo(_src_10290, 0);
    DeRef(_0);

    /** 			dest &= info[PATH_FILENAME]*/
    _2 = (int)SEQ_PTR(_info_10303);
    _5789 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_dest_10291) && IS_ATOM(_5789)) {
        Ref(_5789);
        Append(&_dest_10291, _dest_10291, _5789);
    }
    else if (IS_ATOM(_dest_10291) && IS_SEQUENCE(_5789)) {
    }
    else {
        Concat((object_ptr)&_dest_10291, _dest_10291, _5789);
    }
    _5789 = NOVALUE;
L2: 
    DeRef(_info_10303);
    _info_10303 = NOVALUE;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom psrc = allocate_string(src)*/
    RefDS(_src_10290);
    _0 = _psrc_10307;
    _psrc_10307 = _11allocate_string(_src_10290, 0);
    DeRef(_0);

    /** 		atom pdest = allocate_string(dest)*/
    RefDS(_dest_10291);
    _0 = _pdest_10310;
    _pdest_10310 = _11allocate_string(_dest_10291, 0);
    DeRef(_0);

    /** 		integer success = c_func(xCopyFile, {psrc, pdest, not overwrite})*/
    _5793 = (_overwrite_10292 == 0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_psrc_10307);
    *((int *)(_2+4)) = _psrc_10307;
    Ref(_pdest_10310);
    *((int *)(_2+8)) = _pdest_10310;
    *((int *)(_2+12)) = _5793;
    _5794 = MAKE_SEQ(_1);
    _5793 = NOVALUE;
    _success_10313 = call_c(1, _8xCopyFile_9209, _5794);
    DeRefDS(_5794);
    _5794 = NOVALUE;
    if (!IS_ATOM_INT(_success_10313)) {
        _1 = (long)(DBL_PTR(_success_10313)->dbl);
        if (UNIQUE(DBL_PTR(_success_10313)) && (DBL_PTR(_success_10313)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_10313);
        _success_10313 = _1;
    }

    /** 		free({pdest, psrc})*/
    Ref(_psrc_10307);
    Ref(_pdest_10310);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_10310;
    ((int *)_2)[2] = _psrc_10307;
    _5796 = MAKE_SEQ(_1);
    _11free(_5796);
    _5796 = NOVALUE;

    /** 	return success*/
    DeRefDS(_src_10290);
    DeRefDS(_dest_10291);
    DeRef(_psrc_10307);
    DeRef(_pdest_10310);
    return _success_10313;
    ;
}


int _8rename_file(int _old_name_10321, int _new_name_10322, int _overwrite_10323)
{
    int _psrc_10324 = NOVALUE;
    int _pdest_10325 = NOVALUE;
    int _ret_10326 = NOVALUE;
    int _tempfile_10327 = NOVALUE;
    int _5811 = NOVALUE;
    int _5808 = NOVALUE;
    int _5806 = NOVALUE;
    int _5804 = NOVALUE;
    int _5799 = NOVALUE;
    int _5798 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_10323)) {
        _1 = (long)(DBL_PTR(_overwrite_10323)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_10323)) && (DBL_PTR(_overwrite_10323)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_10323);
        _overwrite_10323 = _1;
    }

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_10327);
    _tempfile_10327 = _5;

    /** 	if not overwrite then*/
    if (_overwrite_10323 != 0)
    goto L1; // [18] 40

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_10322);
    _5798 = _8file_exists(_new_name_10322);
    if (_5798 == 0) {
        DeRef(_5798);
        _5798 = NOVALUE;
        goto L2; // [27] 70
    }
    else {
        if (!IS_ATOM_INT(_5798) && DBL_PTR(_5798)->dbl == 0.0){
            DeRef(_5798);
            _5798 = NOVALUE;
            goto L2; // [27] 70
        }
        DeRef(_5798);
        _5798 = NOVALUE;
    }
    DeRef(_5798);
    _5798 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_old_name_10321);
    DeRefDS(_new_name_10322);
    DeRef(_psrc_10324);
    DeRef(_pdest_10325);
    DeRef(_ret_10326);
    DeRefDS(_tempfile_10327);
    return 0;
    goto L2; // [37] 70
L1: 

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_10322);
    _5799 = _8file_exists(_new_name_10322);
    if (_5799 == 0) {
        DeRef(_5799);
        _5799 = NOVALUE;
        goto L3; // [46] 69
    }
    else {
        if (!IS_ATOM_INT(_5799) && DBL_PTR(_5799)->dbl == 0.0){
            DeRef(_5799);
            _5799 = NOVALUE;
            goto L3; // [46] 69
        }
        DeRef(_5799);
        _5799 = NOVALUE;
    }
    DeRef(_5799);
    _5799 = NOVALUE;

    /** 			tempfile = temp_file(new_name)*/
    RefDS(_new_name_10322);
    RefDS(_5);
    RefDS(_6029);
    _0 = _tempfile_10327;
    _tempfile_10327 = _8temp_file(_new_name_10322, _5, _6029, 0);
    DeRef(_0);

    /** 			ret = move_file(new_name, tempfile)*/
    RefDS(_new_name_10322);
    RefDS(_tempfile_10327);
    _0 = _ret_10326;
    _ret_10326 = _8move_file(_new_name_10322, _tempfile_10327, 0);
    DeRef(_0);
L3: 
L2: 

    /** 	psrc = machine:allocate_string(old_name)*/
    RefDS(_old_name_10321);
    _0 = _psrc_10324;
    _psrc_10324 = _11allocate_string(_old_name_10321, 0);
    DeRef(_0);

    /** 	pdest = machine:allocate_string(new_name)*/
    RefDS(_new_name_10322);
    _0 = _pdest_10325;
    _pdest_10325 = _11allocate_string(_new_name_10322, 0);
    DeRef(_0);

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_10325);
    Ref(_psrc_10324);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_10324;
    ((int *)_2)[2] = _pdest_10325;
    _5804 = MAKE_SEQ(_1);
    DeRef(_ret_10326);
    _ret_10326 = call_c(1, _8xMoveFile_9214, _5804);
    DeRefDS(_5804);
    _5804 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_10324);
    Ref(_pdest_10325);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_10325;
    ((int *)_2)[2] = _psrc_10324;
    _5806 = MAKE_SEQ(_1);
    _11free(_5806);
    _5806 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_10323 == 0)
    {
        goto L4; // [110] 144
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_10326)) {
        if (_ret_10326 != 0){
            goto L5; // [115] 137
        }
    }
    else {
        if (DBL_PTR(_ret_10326)->dbl != 0.0){
            goto L5; // [115] 137
        }
    }

    /** 			if length(tempfile) > 0 then*/
    if (IS_SEQUENCE(_tempfile_10327)){
            _5808 = SEQ_PTR(_tempfile_10327)->length;
    }
    else {
        _5808 = 1;
    }
    if (_5808 <= 0)
    goto L6; // [123] 136

    /** 				ret = move_file(tempfile, new_name)*/
    RefDS(_tempfile_10327);
    RefDS(_new_name_10322);
    _0 = _ret_10326;
    _ret_10326 = _8move_file(_tempfile_10327, _new_name_10322, 0);
    DeRef(_0);
L6: 
L5: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_10327);
    _5811 = _8delete_file(_tempfile_10327);
L4: 

    /** 	return ret*/
    DeRefDS(_old_name_10321);
    DeRefDS(_new_name_10322);
    DeRef(_psrc_10324);
    DeRef(_pdest_10325);
    DeRef(_tempfile_10327);
    DeRef(_5811);
    _5811 = NOVALUE;
    return _ret_10326;
    ;
}


int _8move_file(int _src_10355, int _dest_10356, int _overwrite_10357)
{
    int _psrc_10358 = NOVALUE;
    int _pdest_10359 = NOVALUE;
    int _ret_10360 = NOVALUE;
    int _tempfile_10361 = NOVALUE;
    int _5829 = NOVALUE;
    int _5828 = NOVALUE;
    int _5827 = NOVALUE;
    int _5826 = NOVALUE;
    int _5824 = NOVALUE;
    int _5822 = NOVALUE;
    int _5821 = NOVALUE;
    int _5820 = NOVALUE;
    int _5819 = NOVALUE;
    int _5815 = NOVALUE;
    int _5812 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_10357)) {
        _1 = (long)(DBL_PTR(_overwrite_10357)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_10357)) && (DBL_PTR(_overwrite_10357)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_10357);
        _overwrite_10357 = _1;
    }

    /** 	atom psrc = 0, pdest = 0, ret*/
    DeRef(_psrc_10358);
    _psrc_10358 = 0;
    DeRef(_pdest_10359);
    _pdest_10359 = 0;

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_10361);
    _tempfile_10361 = _5;

    /** 	if not file_exists(src) then*/
    RefDS(_src_10355);
    _5812 = _8file_exists(_src_10355);
    if (IS_ATOM_INT(_5812)) {
        if (_5812 != 0){
            DeRef(_5812);
            _5812 = NOVALUE;
            goto L1; // [30] 40
        }
    }
    else {
        if (DBL_PTR(_5812)->dbl != 0.0){
            DeRef(_5812);
            _5812 = NOVALUE;
            goto L1; // [30] 40
        }
    }
    DeRef(_5812);
    _5812 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_src_10355);
    DeRefDS(_dest_10356);
    DeRef(_ret_10360);
    DeRefDS(_tempfile_10361);
    return 0;
L1: 

    /** 	if not overwrite then*/
    if (_overwrite_10357 != 0)
    goto L2; // [42] 62

    /** 		if file_exists( dest ) then*/
    RefDS(_dest_10356);
    _5815 = _8file_exists(_dest_10356);
    if (_5815 == 0) {
        DeRef(_5815);
        _5815 = NOVALUE;
        goto L3; // [51] 61
    }
    else {
        if (!IS_ATOM_INT(_5815) && DBL_PTR(_5815)->dbl == 0.0){
            DeRef(_5815);
            _5815 = NOVALUE;
            goto L3; // [51] 61
        }
        DeRef(_5815);
        _5815 = NOVALUE;
    }
    DeRef(_5815);
    _5815 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_src_10355);
    DeRefDS(_dest_10356);
    DeRef(_psrc_10358);
    DeRef(_pdest_10359);
    DeRef(_ret_10360);
    DeRef(_tempfile_10361);
    return 0;
L3: 
L2: 

    /** 	ifdef UNIX then*/

    /** 	ifdef LINUX then*/

    /** 	ifdef UNIX then*/

    /** 		psrc  = machine:allocate_string(src)*/
    RefDS(_src_10355);
    _0 = _psrc_10358;
    _psrc_10358 = _11allocate_string(_src_10355, 0);
    DeRef(_0);

    /** 		pdest = machine:allocate_string(dest)*/
    RefDS(_dest_10356);
    _0 = _pdest_10359;
    _pdest_10359 = _11allocate_string(_dest_10356, 0);
    DeRef(_0);

    /** 	if overwrite then*/
    if (_overwrite_10357 == 0)
    {
        goto L4; // [84] 113
    }
    else{
    }

    /** 		tempfile = temp_file(dest)*/
    RefDS(_dest_10356);
    RefDS(_5);
    RefDS(_6029);
    _0 = _tempfile_10361;
    _tempfile_10361 = _8temp_file(_dest_10356, _5, _6029, 0);
    DeRef(_0);

    /** 		move_file(dest, tempfile)*/
    RefDS(_dest_10356);
    DeRef(_5819);
    _5819 = _dest_10356;
    RefDS(_tempfile_10361);
    DeRef(_5820);
    _5820 = _tempfile_10361;
    _5821 = _8move_file(_5819, _5820, 0);
    _5819 = NOVALUE;
    _5820 = NOVALUE;
L4: 

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_10359);
    Ref(_psrc_10358);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_10358;
    ((int *)_2)[2] = _pdest_10359;
    _5822 = MAKE_SEQ(_1);
    DeRef(_ret_10360);
    _ret_10360 = call_c(1, _8xMoveFile_9214, _5822);
    DeRefDS(_5822);
    _5822 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_10358);
    Ref(_pdest_10359);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_10359;
    ((int *)_2)[2] = _psrc_10358;
    _5824 = MAKE_SEQ(_1);
    _11free(_5824);
    _5824 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_10357 == 0)
    {
        goto L5; // [139] 169
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_10360)) {
        if (_ret_10360 != 0){
            goto L6; // [144] 162
        }
    }
    else {
        if (DBL_PTR(_ret_10360)->dbl != 0.0){
            goto L6; // [144] 162
        }
    }

    /** 			move_file(tempfile, dest)*/
    RefDS(_tempfile_10361);
    DeRef(_5826);
    _5826 = _tempfile_10361;
    RefDS(_dest_10356);
    DeRef(_5827);
    _5827 = _dest_10356;
    _5828 = _8move_file(_5826, _5827, 0);
    _5826 = NOVALUE;
    _5827 = NOVALUE;
L6: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_10361);
    _5829 = _8delete_file(_tempfile_10361);
L5: 

    /** 	return ret*/
    DeRefDS(_src_10355);
    DeRefDS(_dest_10356);
    DeRef(_psrc_10358);
    DeRef(_pdest_10359);
    DeRef(_tempfile_10361);
    DeRef(_5821);
    _5821 = NOVALUE;
    DeRef(_5828);
    _5828 = NOVALUE;
    DeRef(_5829);
    _5829 = NOVALUE;
    return _ret_10360;
    ;
}


int _8file_length(int _filename_10389)
{
    int _list_10390 = NOVALUE;
    int _dir_inlined_dir_at_4_10392 = NOVALUE;
    int _5835 = NOVALUE;
    int _5834 = NOVALUE;
    int _5833 = NOVALUE;
    int _5832 = NOVALUE;
    int _5830 = NOVALUE;
    int _0, _1, _2;
    

    /** 	list = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_list_10390);
    _list_10390 = machine(22, _filename_10389);

    /** 	if atom(list) or length(list) = 0 then*/
    _5830 = IS_ATOM(_list_10390);
    if (_5830 != 0) {
        goto L1; // [19] 35
    }
    if (IS_SEQUENCE(_list_10390)){
            _5832 = SEQ_PTR(_list_10390)->length;
    }
    else {
        _5832 = 1;
    }
    _5833 = (_5832 == 0);
    _5832 = NOVALUE;
    if (_5833 == 0)
    {
        DeRef(_5833);
        _5833 = NOVALUE;
        goto L2; // [31] 42
    }
    else{
        DeRef(_5833);
        _5833 = NOVALUE;
    }
L1: 

    /** 		return -1*/
    DeRefDS(_filename_10389);
    DeRef(_list_10390);
    return -1;
L2: 

    /** 	return list[1][D_SIZE]*/
    _2 = (int)SEQ_PTR(_list_10390);
    _5834 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_5834);
    _5835 = (int)*(((s1_ptr)_2)->base + 3);
    _5834 = NOVALUE;
    Ref(_5835);
    DeRefDS(_filename_10389);
    DeRef(_list_10390);
    return _5835;
    ;
}


int _8locate_file(int _filename_10402, int _search_list_10403, int _subdir_10404)
{
    int _extra_paths_10405 = NOVALUE;
    int _this_path_10406 = NOVALUE;
    int _5919 = NOVALUE;
    int _5918 = NOVALUE;
    int _5916 = NOVALUE;
    int _5914 = NOVALUE;
    int _5912 = NOVALUE;
    int _5911 = NOVALUE;
    int _5910 = NOVALUE;
    int _5908 = NOVALUE;
    int _5907 = NOVALUE;
    int _5906 = NOVALUE;
    int _5904 = NOVALUE;
    int _5903 = NOVALUE;
    int _5902 = NOVALUE;
    int _5899 = NOVALUE;
    int _5898 = NOVALUE;
    int _5896 = NOVALUE;
    int _5894 = NOVALUE;
    int _5893 = NOVALUE;
    int _5890 = NOVALUE;
    int _5885 = NOVALUE;
    int _5881 = NOVALUE;
    int _5872 = NOVALUE;
    int _5869 = NOVALUE;
    int _5866 = NOVALUE;
    int _5865 = NOVALUE;
    int _5861 = NOVALUE;
    int _5858 = NOVALUE;
    int _5856 = NOVALUE;
    int _5852 = NOVALUE;
    int _5850 = NOVALUE;
    int _5849 = NOVALUE;
    int _5847 = NOVALUE;
    int _5846 = NOVALUE;
    int _5843 = NOVALUE;
    int _5842 = NOVALUE;
    int _5839 = NOVALUE;
    int _5837 = NOVALUE;
    int _5836 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path(filename) then*/
    RefDS(_filename_10402);
    _5836 = _8absolute_path(_filename_10402);
    if (_5836 == 0) {
        DeRef(_5836);
        _5836 = NOVALUE;
        goto L1; // [13] 23
    }
    else {
        if (!IS_ATOM_INT(_5836) && DBL_PTR(_5836)->dbl == 0.0){
            DeRef(_5836);
            _5836 = NOVALUE;
            goto L1; // [13] 23
        }
        DeRef(_5836);
        _5836 = NOVALUE;
    }
    DeRef(_5836);
    _5836 = NOVALUE;

    /** 		return filename*/
    DeRefDS(_search_list_10403);
    DeRefDS(_subdir_10404);
    DeRef(_extra_paths_10405);
    DeRef(_this_path_10406);
    return _filename_10402;
L1: 

    /** 	if length(search_list) = 0 then*/
    if (IS_SEQUENCE(_search_list_10403)){
            _5837 = SEQ_PTR(_search_list_10403)->length;
    }
    else {
        _5837 = 1;
    }
    if (_5837 != 0)
    goto L2; // [28] 281

    /** 		search_list = append(search_list, "." & SLASH)*/
    Append(&_5839, _5182, 92);
    RefDS(_5839);
    Append(&_search_list_10403, _search_list_10403, _5839);
    DeRefDS(_5839);
    _5839 = NOVALUE;

    /** 		extra_paths = command_line()*/
    DeRef(_extra_paths_10405);
    _extra_paths_10405 = Command_Line();

    /** 		extra_paths = canonical_path(dirname(extra_paths[2]), 1)*/
    _2 = (int)SEQ_PTR(_extra_paths_10405);
    _5842 = (int)*(((s1_ptr)_2)->base + 2);
    RefDS(_5842);
    _5843 = _8dirname(_5842, 0);
    _5842 = NOVALUE;
    _0 = _extra_paths_10405;
    _extra_paths_10405 = _8canonical_path(_5843, 1, 0);
    DeRefDS(_0);
    _5843 = NOVALUE;

    /** 		search_list = append(search_list, extra_paths)*/
    Ref(_extra_paths_10405);
    Append(&_search_list_10403, _search_list_10403, _extra_paths_10405);

    /** 		ifdef UNIX then*/

    /** 			extra_paths = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _5846 = EGetEnv(_5470);
    _5847 = EGetEnv(_5472);
    if (IS_SEQUENCE(_5846) && IS_ATOM(_5847)) {
        Ref(_5847);
        Append(&_extra_paths_10405, _5846, _5847);
    }
    else if (IS_ATOM(_5846) && IS_SEQUENCE(_5847)) {
        Ref(_5846);
        Prepend(&_extra_paths_10405, _5847, _5846);
    }
    else {
        Concat((object_ptr)&_extra_paths_10405, _5846, _5847);
        DeRef(_5846);
        _5846 = NOVALUE;
    }
    DeRef(_5846);
    _5846 = NOVALUE;
    DeRef(_5847);
    _5847 = NOVALUE;

    /** 		if sequence(extra_paths) then*/
    _5849 = 1;
    if (_5849 == 0)
    {
        _5849 = NOVALUE;
        goto L3; // [90] 104
    }
    else{
        _5849 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    Append(&_5850, _extra_paths_10405, 92);
    RefDS(_5850);
    Append(&_search_list_10403, _search_list_10403, _5850);
    DeRefDS(_5850);
    _5850 = NOVALUE;
L3: 

    /** 		search_list = append(search_list, ".." & SLASH)*/
    Append(&_5852, _5213, 92);
    RefDS(_5852);
    Append(&_search_list_10403, _search_list_10403, _5852);
    DeRefDS(_5852);
    _5852 = NOVALUE;

    /** 		extra_paths = getenv("EUDIR")*/
    DeRef(_extra_paths_10405);
    _extra_paths_10405 = EGetEnv(_5854);

    /** 		if sequence(extra_paths) then*/
    _5856 = IS_SEQUENCE(_extra_paths_10405);
    if (_5856 == 0)
    {
        _5856 = NOVALUE;
        goto L4; // [124] 154
    }
    else{
        _5856 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH & "bin" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _5857;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_10405;
        Concat_N((object_ptr)&_5858, concat_list, 4);
    }
    RefDS(_5858);
    Append(&_search_list_10403, _search_list_10403, _5858);
    DeRefDS(_5858);
    _5858 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "docs" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _5860;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_10405;
        Concat_N((object_ptr)&_5861, concat_list, 4);
    }
    RefDS(_5861);
    Append(&_search_list_10403, _search_list_10403, _5861);
    DeRefDS(_5861);
    _5861 = NOVALUE;
L4: 

    /** 		extra_paths = getenv("EUDIST")*/
    DeRef(_extra_paths_10405);
    _extra_paths_10405 = EGetEnv(_5863);

    /** 		if sequence(extra_paths) then*/
    _5865 = IS_SEQUENCE(_extra_paths_10405);
    if (_5865 == 0)
    {
        _5865 = NOVALUE;
        goto L5; // [164] 204
    }
    else{
        _5865 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    if (IS_SEQUENCE(_extra_paths_10405) && IS_ATOM(92)) {
        Append(&_5866, _extra_paths_10405, 92);
    }
    else if (IS_ATOM(_extra_paths_10405) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_5866, _extra_paths_10405, 92);
    }
    RefDS(_5866);
    Append(&_search_list_10403, _search_list_10403, _5866);
    DeRefDS(_5866);
    _5866 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "etc" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _5868;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_10405;
        Concat_N((object_ptr)&_5869, concat_list, 4);
    }
    RefDS(_5869);
    Append(&_search_list_10403, _search_list_10403, _5869);
    DeRefDS(_5869);
    _5869 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "data" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _5871;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_10405;
        Concat_N((object_ptr)&_5872, concat_list, 4);
    }
    RefDS(_5872);
    Append(&_search_list_10403, _search_list_10403, _5872);
    DeRefDS(_5872);
    _5872 = NOVALUE;
L5: 

    /** 		ifdef UNIX then*/

    /** 		search_list &= include_paths(1)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_5880);
    *((int *)(_2+4)) = _5880;
    RefDS(_5879);
    *((int *)(_2+8)) = _5879;
    RefDS(_5878);
    *((int *)(_2+12)) = _5878;
    RefDS(_5877);
    *((int *)(_2+16)) = _5877;
    RefDS(_5876);
    *((int *)(_2+20)) = _5876;
    _5881 = MAKE_SEQ(_1);
    Concat((object_ptr)&_search_list_10403, _search_list_10403, _5881);
    DeRefDS(_5881);
    _5881 = NOVALUE;

    /** 		extra_paths = getenv("USERPATH")*/
    DeRef(_extra_paths_10405);
    _extra_paths_10405 = EGetEnv(_5883);

    /** 		if sequence(extra_paths) then*/
    _5885 = IS_SEQUENCE(_extra_paths_10405);
    if (_5885 == 0)
    {
        _5885 = NOVALUE;
        goto L6; // [230] 249
    }
    else{
        _5885 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_10405);
    _0 = _extra_paths_10405;
    _extra_paths_10405 = _3split(_extra_paths_10405, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_10403) && IS_ATOM(_extra_paths_10405)) {
        Ref(_extra_paths_10405);
        Append(&_search_list_10403, _search_list_10403, _extra_paths_10405);
    }
    else if (IS_ATOM(_search_list_10403) && IS_SEQUENCE(_extra_paths_10405)) {
    }
    else {
        Concat((object_ptr)&_search_list_10403, _search_list_10403, _extra_paths_10405);
    }
L6: 

    /** 		extra_paths = getenv("PATH")*/
    DeRef(_extra_paths_10405);
    _extra_paths_10405 = EGetEnv(_5888);

    /** 		if sequence(extra_paths) then*/
    _5890 = IS_SEQUENCE(_extra_paths_10405);
    if (_5890 == 0)
    {
        _5890 = NOVALUE;
        goto L7; // [259] 306
    }
    else{
        _5890 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_10405);
    _0 = _extra_paths_10405;
    _extra_paths_10405 = _3split(_extra_paths_10405, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_10403) && IS_ATOM(_extra_paths_10405)) {
        Ref(_extra_paths_10405);
        Append(&_search_list_10403, _search_list_10403, _extra_paths_10405);
    }
    else if (IS_ATOM(_search_list_10403) && IS_SEQUENCE(_extra_paths_10405)) {
    }
    else {
        Concat((object_ptr)&_search_list_10403, _search_list_10403, _extra_paths_10405);
    }
    goto L7; // [278] 306
L2: 

    /** 		if integer(search_list[1]) then*/
    _2 = (int)SEQ_PTR(_search_list_10403);
    _5893 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5893))
    _5894 = 1;
    else if (IS_ATOM_DBL(_5893))
    _5894 = IS_ATOM_INT(DoubleToInt(_5893));
    else
    _5894 = 0;
    _5893 = NOVALUE;
    if (_5894 == 0)
    {
        _5894 = NOVALUE;
        goto L8; // [290] 305
    }
    else{
        _5894 = NOVALUE;
    }

    /** 			search_list = stdseq:split(search_list, PATHSEP)*/
    RefDS(_search_list_10403);
    _0 = _search_list_10403;
    _search_list_10403 = _3split(_search_list_10403, 59, 0, 0);
    DeRefDS(_0);
L8: 
L7: 

    /** 	if length(subdir) > 0 then*/
    if (IS_SEQUENCE(_subdir_10404)){
            _5896 = SEQ_PTR(_subdir_10404)->length;
    }
    else {
        _5896 = 1;
    }
    if (_5896 <= 0)
    goto L9; // [311] 336

    /** 		if subdir[$] != SLASH then*/
    if (IS_SEQUENCE(_subdir_10404)){
            _5898 = SEQ_PTR(_subdir_10404)->length;
    }
    else {
        _5898 = 1;
    }
    _2 = (int)SEQ_PTR(_subdir_10404);
    _5899 = (int)*(((s1_ptr)_2)->base + _5898);
    if (binary_op_a(EQUALS, _5899, 92)){
        _5899 = NOVALUE;
        goto LA; // [324] 335
    }
    _5899 = NOVALUE;

    /** 			subdir &= SLASH*/
    Append(&_subdir_10404, _subdir_10404, 92);
LA: 
L9: 

    /** 	for i = 1 to length(search_list) do*/
    if (IS_SEQUENCE(_search_list_10403)){
            _5902 = SEQ_PTR(_search_list_10403)->length;
    }
    else {
        _5902 = 1;
    }
    {
        int _i_10485;
        _i_10485 = 1;
LB: 
        if (_i_10485 > _5902){
            goto LC; // [341] 466
        }

        /** 		if length(search_list[i]) = 0 then*/
        _2 = (int)SEQ_PTR(_search_list_10403);
        _5903 = (int)*(((s1_ptr)_2)->base + _i_10485);
        if (IS_SEQUENCE(_5903)){
                _5904 = SEQ_PTR(_5903)->length;
        }
        else {
            _5904 = 1;
        }
        _5903 = NOVALUE;
        if (_5904 != 0)
        goto LD; // [357] 366

        /** 			continue*/
        goto LE; // [363] 461
LD: 

        /** 		if search_list[i][$] != SLASH then*/
        _2 = (int)SEQ_PTR(_search_list_10403);
        _5906 = (int)*(((s1_ptr)_2)->base + _i_10485);
        if (IS_SEQUENCE(_5906)){
                _5907 = SEQ_PTR(_5906)->length;
        }
        else {
            _5907 = 1;
        }
        _2 = (int)SEQ_PTR(_5906);
        _5908 = (int)*(((s1_ptr)_2)->base + _5907);
        _5906 = NOVALUE;
        if (binary_op_a(EQUALS, _5908, 92)){
            _5908 = NOVALUE;
            goto LF; // [379] 398
        }
        _5908 = NOVALUE;

        /** 			search_list[i] &= SLASH*/
        _2 = (int)SEQ_PTR(_search_list_10403);
        _5910 = (int)*(((s1_ptr)_2)->base + _i_10485);
        if (IS_SEQUENCE(_5910) && IS_ATOM(92)) {
            Append(&_5911, _5910, 92);
        }
        else if (IS_ATOM(_5910) && IS_SEQUENCE(92)) {
        }
        else {
            Concat((object_ptr)&_5911, _5910, 92);
            _5910 = NOVALUE;
        }
        _5910 = NOVALUE;
        _2 = (int)SEQ_PTR(_search_list_10403);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _search_list_10403 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_10485);
        _1 = *(int *)_2;
        *(int *)_2 = _5911;
        if( _1 != _5911 ){
            DeRef(_1);
        }
        _5911 = NOVALUE;
LF: 

        /** 		if length(subdir) > 0 then*/
        if (IS_SEQUENCE(_subdir_10404)){
                _5912 = SEQ_PTR(_subdir_10404)->length;
        }
        else {
            _5912 = 1;
        }
        if (_5912 <= 0)
        goto L10; // [403] 422

        /** 			this_path = search_list[i] & subdir & filename*/
        _2 = (int)SEQ_PTR(_search_list_10403);
        _5914 = (int)*(((s1_ptr)_2)->base + _i_10485);
        {
            int concat_list[3];

            concat_list[0] = _filename_10402;
            concat_list[1] = _subdir_10404;
            concat_list[2] = _5914;
            Concat_N((object_ptr)&_this_path_10406, concat_list, 3);
        }
        _5914 = NOVALUE;
        goto L11; // [419] 433
L10: 

        /** 			this_path = search_list[i] & filename*/
        _2 = (int)SEQ_PTR(_search_list_10403);
        _5916 = (int)*(((s1_ptr)_2)->base + _i_10485);
        if (IS_SEQUENCE(_5916) && IS_ATOM(_filename_10402)) {
        }
        else if (IS_ATOM(_5916) && IS_SEQUENCE(_filename_10402)) {
            Ref(_5916);
            Prepend(&_this_path_10406, _filename_10402, _5916);
        }
        else {
            Concat((object_ptr)&_this_path_10406, _5916, _filename_10402);
            _5916 = NOVALUE;
        }
        _5916 = NOVALUE;
L11: 

        /** 		if file_exists(this_path) then*/
        RefDS(_this_path_10406);
        _5918 = _8file_exists(_this_path_10406);
        if (_5918 == 0) {
            DeRef(_5918);
            _5918 = NOVALUE;
            goto L12; // [441] 459
        }
        else {
            if (!IS_ATOM_INT(_5918) && DBL_PTR(_5918)->dbl == 0.0){
                DeRef(_5918);
                _5918 = NOVALUE;
                goto L12; // [441] 459
            }
            DeRef(_5918);
            _5918 = NOVALUE;
        }
        DeRef(_5918);
        _5918 = NOVALUE;

        /** 			return canonical_path(this_path)*/
        RefDS(_this_path_10406);
        _5919 = _8canonical_path(_this_path_10406, 0, 0);
        DeRefDS(_filename_10402);
        DeRefDS(_search_list_10403);
        DeRefDS(_subdir_10404);
        DeRef(_extra_paths_10405);
        DeRefDS(_this_path_10406);
        _5903 = NOVALUE;
        return _5919;
L12: 

        /** 	end for*/
LE: 
        _i_10485 = _i_10485 + 1;
        goto LB; // [461] 348
LC: 
        ;
    }

    /** 	return filename*/
    DeRefDS(_search_list_10403);
    DeRefDS(_subdir_10404);
    DeRef(_extra_paths_10405);
    DeRef(_this_path_10406);
    _5903 = NOVALUE;
    DeRef(_5919);
    _5919 = NOVALUE;
    return _filename_10402;
    ;
}


int _8disk_metrics(int _disk_path_10511)
{
    int _result_10512 = NOVALUE;
    int _path_addr_10514 = NOVALUE;
    int _metric_addr_10515 = NOVALUE;
    int _5932 = NOVALUE;
    int _5930 = NOVALUE;
    int _5929 = NOVALUE;
    int _5928 = NOVALUE;
    int _5927 = NOVALUE;
    int _5926 = NOVALUE;
    int _5925 = NOVALUE;
    int _5924 = NOVALUE;
    int _5921 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence result = {0, 0, 0, 0} */
    _0 = _result_10512;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    _result_10512 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	atom path_addr = 0*/
    DeRef(_path_addr_10514);
    _path_addr_10514 = 0;

    /** 	atom metric_addr = 0*/
    DeRef(_metric_addr_10515);
    _metric_addr_10515 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		if sequence(disk_path) then */
    _5921 = IS_SEQUENCE(_disk_path_10511);
    if (_5921 == 0)
    {
        _5921 = NOVALUE;
        goto L1; // [27] 40
    }
    else{
        _5921 = NOVALUE;
    }

    /** 			path_addr = allocate_string(disk_path) */
    Ref(_disk_path_10511);
    _path_addr_10514 = _11allocate_string(_disk_path_10511, 0);
    goto L2; // [37] 46
L1: 

    /** 			path_addr = 0 */
    DeRef(_path_addr_10514);
    _path_addr_10514 = 0;
L2: 

    /** 		metric_addr = allocate(16) */
    _0 = _metric_addr_10515;
    _metric_addr_10515 = _11allocate(16, 0);
    DeRef(_0);

    /** 		if c_func(xGetDiskFreeSpace, {path_addr, */
    if (IS_ATOM_INT(_metric_addr_10515)) {
        _5924 = _metric_addr_10515 + 0;
        if ((long)((unsigned long)_5924 + (unsigned long)HIGH_BITS) >= 0) 
        _5924 = NewDouble((double)_5924);
    }
    else {
        _5924 = NewDouble(DBL_PTR(_metric_addr_10515)->dbl + (double)0);
    }
    if (IS_ATOM_INT(_metric_addr_10515)) {
        _5925 = _metric_addr_10515 + 4;
        if ((long)((unsigned long)_5925 + (unsigned long)HIGH_BITS) >= 0) 
        _5925 = NewDouble((double)_5925);
    }
    else {
        _5925 = NewDouble(DBL_PTR(_metric_addr_10515)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_metric_addr_10515)) {
        _5926 = _metric_addr_10515 + 8;
        if ((long)((unsigned long)_5926 + (unsigned long)HIGH_BITS) >= 0) 
        _5926 = NewDouble((double)_5926);
    }
    else {
        _5926 = NewDouble(DBL_PTR(_metric_addr_10515)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_metric_addr_10515)) {
        _5927 = _metric_addr_10515 + 12;
        if ((long)((unsigned long)_5927 + (unsigned long)HIGH_BITS) >= 0) 
        _5927 = NewDouble((double)_5927);
    }
    else {
        _5927 = NewDouble(DBL_PTR(_metric_addr_10515)->dbl + (double)12);
    }
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_path_addr_10514);
    *((int *)(_2+4)) = _path_addr_10514;
    *((int *)(_2+8)) = _5924;
    *((int *)(_2+12)) = _5925;
    *((int *)(_2+16)) = _5926;
    *((int *)(_2+20)) = _5927;
    _5928 = MAKE_SEQ(_1);
    _5927 = NOVALUE;
    _5926 = NOVALUE;
    _5925 = NOVALUE;
    _5924 = NOVALUE;
    _5929 = call_c(1, _8xGetDiskFreeSpace_9237, _5928);
    DeRefDS(_5928);
    _5928 = NOVALUE;
    if (_5929 == 0) {
        DeRef(_5929);
        _5929 = NOVALUE;
        goto L3; // [86] 101
    }
    else {
        if (!IS_ATOM_INT(_5929) && DBL_PTR(_5929)->dbl == 0.0){
            DeRef(_5929);
            _5929 = NOVALUE;
            goto L3; // [86] 101
        }
        DeRef(_5929);
        _5929 = NOVALUE;
    }
    DeRef(_5929);
    _5929 = NOVALUE;

    /** 			result = peek4s({metric_addr, 4}) */
    Ref(_metric_addr_10515);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _metric_addr_10515;
    ((int *)_2)[2] = 4;
    _5930 = MAKE_SEQ(_1);
    DeRef(_result_10512);
    _1 = (int)SEQ_PTR(_5930);
    peek4_addr = (unsigned long *)get_pos_int("peek4s/peek4u", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _result_10512 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)*peek4_addr++;
        if (_1 < MININT || _1 > MAXINT)
        _1 = NewDouble((double)(long)_1);
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_5930);
    _5930 = NOVALUE;
L3: 

    /** 		free({path_addr, metric_addr}) */
    Ref(_metric_addr_10515);
    Ref(_path_addr_10514);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_addr_10514;
    ((int *)_2)[2] = _metric_addr_10515;
    _5932 = MAKE_SEQ(_1);
    _11free(_5932);
    _5932 = NOVALUE;

    /** 	return result */
    DeRef(_disk_path_10511);
    DeRef(_path_addr_10514);
    DeRef(_metric_addr_10515);
    return _result_10512;
    ;
}


int _8disk_size(int _disk_path_10537)
{
    int _disk_size_10538 = NOVALUE;
    int _result_10540 = NOVALUE;
    int _bytes_per_cluster_10541 = NOVALUE;
    int _5945 = NOVALUE;
    int _5944 = NOVALUE;
    int _5943 = NOVALUE;
    int _5942 = NOVALUE;
    int _5941 = NOVALUE;
    int _5940 = NOVALUE;
    int _5939 = NOVALUE;
    int _5937 = NOVALUE;
    int _5936 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence disk_size = {0,0,0, disk_path}*/
    _0 = _disk_size_10538;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    Ref(_disk_path_10537);
    *((int *)(_2+16)) = _disk_path_10537;
    _disk_size_10538 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	ifdef WINDOWS then*/

    /** 		sequence result */

    /** 		atom bytes_per_cluster*/

    /** 		result = disk_metrics(disk_path) */
    Ref(_disk_path_10537);
    _0 = _result_10540;
    _result_10540 = _8disk_metrics(_disk_path_10537);
    DeRef(_0);

    /** 		bytes_per_cluster = result[BYTES_PER_SECTOR] * result[SECTORS_PER_CLUSTER]*/
    _2 = (int)SEQ_PTR(_result_10540);
    _5936 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_result_10540);
    _5937 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_bytes_per_cluster_10541);
    if (IS_ATOM_INT(_5936) && IS_ATOM_INT(_5937)) {
        if (_5936 == (short)_5936 && _5937 <= INT15 && _5937 >= -INT15)
        _bytes_per_cluster_10541 = _5936 * _5937;
        else
        _bytes_per_cluster_10541 = NewDouble(_5936 * (double)_5937);
    }
    else {
        _bytes_per_cluster_10541 = binary_op(MULTIPLY, _5936, _5937);
    }
    _5936 = NOVALUE;
    _5937 = NOVALUE;

    /** 		disk_size[TOTAL_BYTES] = bytes_per_cluster * result[TOTAL_NUMBER_OF_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_10540);
    _5939 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_bytes_per_cluster_10541) && IS_ATOM_INT(_5939)) {
        if (_bytes_per_cluster_10541 == (short)_bytes_per_cluster_10541 && _5939 <= INT15 && _5939 >= -INT15)
        _5940 = _bytes_per_cluster_10541 * _5939;
        else
        _5940 = NewDouble(_bytes_per_cluster_10541 * (double)_5939);
    }
    else {
        _5940 = binary_op(MULTIPLY, _bytes_per_cluster_10541, _5939);
    }
    _5939 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_10538);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_10538 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _5940;
    if( _1 != _5940 ){
        DeRef(_1);
    }
    _5940 = NOVALUE;

    /** 		disk_size[FREE_BYTES]  = bytes_per_cluster * result[NUMBER_OF_FREE_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_10540);
    _5941 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_bytes_per_cluster_10541) && IS_ATOM_INT(_5941)) {
        if (_bytes_per_cluster_10541 == (short)_bytes_per_cluster_10541 && _5941 <= INT15 && _5941 >= -INT15)
        _5942 = _bytes_per_cluster_10541 * _5941;
        else
        _5942 = NewDouble(_bytes_per_cluster_10541 * (double)_5941);
    }
    else {
        _5942 = binary_op(MULTIPLY, _bytes_per_cluster_10541, _5941);
    }
    _5941 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_10538);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_10538 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _5942;
    if( _1 != _5942 ){
        DeRef(_1);
    }
    _5942 = NOVALUE;

    /** 		disk_size[USED_BYTES]  = disk_size[TOTAL_BYTES] - disk_size[FREE_BYTES] */
    _2 = (int)SEQ_PTR(_disk_size_10538);
    _5943 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_disk_size_10538);
    _5944 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5943) && IS_ATOM_INT(_5944)) {
        _5945 = _5943 - _5944;
        if ((long)((unsigned long)_5945 +(unsigned long) HIGH_BITS) >= 0){
            _5945 = NewDouble((double)_5945);
        }
    }
    else {
        _5945 = binary_op(MINUS, _5943, _5944);
    }
    _5943 = NOVALUE;
    _5944 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_10538);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_10538 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _5945;
    if( _1 != _5945 ){
        DeRef(_1);
    }
    _5945 = NOVALUE;

    /** 	return disk_size */
    DeRef(_disk_path_10537);
    DeRefDS(_result_10540);
    DeRef(_bytes_per_cluster_10541);
    return _disk_size_10538;
    ;
}


int _8count_files(int _orig_path_10560, int _dir_info_10561, int _inst_10562)
{
    int _pos_10563 = NOVALUE;
    int _ext_10564 = NOVALUE;
    int _fileext_inlined_fileext_at_245_10607 = NOVALUE;
    int _data_inlined_fileext_at_245_10606 = NOVALUE;
    int _path_inlined_fileext_at_242_10605 = NOVALUE;
    int _6013 = NOVALUE;
    int _6012 = NOVALUE;
    int _6011 = NOVALUE;
    int _6009 = NOVALUE;
    int _6008 = NOVALUE;
    int _6007 = NOVALUE;
    int _6006 = NOVALUE;
    int _6004 = NOVALUE;
    int _6003 = NOVALUE;
    int _6001 = NOVALUE;
    int _6000 = NOVALUE;
    int _5999 = NOVALUE;
    int _5998 = NOVALUE;
    int _5997 = NOVALUE;
    int _5996 = NOVALUE;
    int _5995 = NOVALUE;
    int _5993 = NOVALUE;
    int _5992 = NOVALUE;
    int _5990 = NOVALUE;
    int _5989 = NOVALUE;
    int _5988 = NOVALUE;
    int _5987 = NOVALUE;
    int _5986 = NOVALUE;
    int _5985 = NOVALUE;
    int _5984 = NOVALUE;
    int _5983 = NOVALUE;
    int _5982 = NOVALUE;
    int _5981 = NOVALUE;
    int _5980 = NOVALUE;
    int _5979 = NOVALUE;
    int _5978 = NOVALUE;
    int _5977 = NOVALUE;
    int _5975 = NOVALUE;
    int _5974 = NOVALUE;
    int _5973 = NOVALUE;
    int _5972 = NOVALUE;
    int _5970 = NOVALUE;
    int _5969 = NOVALUE;
    int _5968 = NOVALUE;
    int _5967 = NOVALUE;
    int _5966 = NOVALUE;
    int _5965 = NOVALUE;
    int _5964 = NOVALUE;
    int _5962 = NOVALUE;
    int _5961 = NOVALUE;
    int _5960 = NOVALUE;
    int _5959 = NOVALUE;
    int _5958 = NOVALUE;
    int _5957 = NOVALUE;
    int _5954 = NOVALUE;
    int _5953 = NOVALUE;
    int _5952 = NOVALUE;
    int _5951 = NOVALUE;
    int _5950 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer pos = 0*/
    _pos_10563 = 0;

    /** 	orig_path = orig_path*/
    RefDS(_orig_path_10560);
    DeRefDS(_orig_path_10560);
    _orig_path_10560 = _orig_path_10560;

    /** 	if equal(dir_info[D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _5950 = (int)*(((s1_ptr)_2)->base + 1);
    if (_5950 == _5182)
    _5951 = 1;
    else if (IS_ATOM_INT(_5950) && IS_ATOM_INT(_5182))
    _5951 = 0;
    else
    _5951 = (compare(_5950, _5182) == 0);
    _5950 = NOVALUE;
    if (_5951 == 0)
    {
        _5951 = NOVALUE;
        goto L1; // [33] 43
    }
    else{
        _5951 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_10560);
    DeRefDS(_dir_info_10561);
    DeRefDS(_inst_10562);
    DeRef(_ext_10564);
    return 0;
L1: 

    /** 	if equal(dir_info[D_NAME], "..") then*/
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _5952 = (int)*(((s1_ptr)_2)->base + 1);
    if (_5952 == _5213)
    _5953 = 1;
    else if (IS_ATOM_INT(_5952) && IS_ATOM_INT(_5213))
    _5953 = 0;
    else
    _5953 = (compare(_5952, _5213) == 0);
    _5952 = NOVALUE;
    if (_5953 == 0)
    {
        _5953 = NOVALUE;
        goto L2; // [55] 65
    }
    else{
        _5953 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_10560);
    DeRefDS(_dir_info_10561);
    DeRefDS(_inst_10562);
    DeRef(_ext_10564);
    return 0;
L2: 

    /** 	if inst[1] = 0 then -- count all is false*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _5954 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5954, 0)){
        _5954 = NOVALUE;
        goto L3; // [71] 122
    }
    _5954 = NOVALUE;

    /** 		if find('h', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _5957 = (int)*(((s1_ptr)_2)->base + 2);
    _5958 = find_from(104, _5957, 1);
    _5957 = NOVALUE;
    if (_5958 == 0)
    {
        _5958 = NOVALUE;
        goto L4; // [88] 98
    }
    else{
        _5958 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_10560);
    DeRefDS(_dir_info_10561);
    DeRefDS(_inst_10562);
    DeRef(_ext_10564);
    return 0;
L4: 

    /** 		if find('s', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _5959 = (int)*(((s1_ptr)_2)->base + 2);
    _5960 = find_from(115, _5959, 1);
    _5959 = NOVALUE;
    if (_5960 == 0)
    {
        _5960 = NOVALUE;
        goto L5; // [111] 121
    }
    else{
        _5960 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_10560);
    DeRefDS(_dir_info_10561);
    DeRefDS(_inst_10562);
    DeRef(_ext_10564);
    return 0;
L5: 
L3: 

    /** 	file_counters[inst[2]][COUNT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _5961 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_10557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_5961))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_5961)->dbl));
    else
    _3 = (int)(_5961 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _5964 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _5965 = (int)*(((s1_ptr)_2)->base + 3);
    _5962 = NOVALUE;
    if (IS_ATOM_INT(_5965) && IS_ATOM_INT(_5964)) {
        _5966 = _5965 + _5964;
        if ((long)((unsigned long)_5966 + (unsigned long)HIGH_BITS) >= 0) 
        _5966 = NewDouble((double)_5966);
    }
    else {
        _5966 = binary_op(PLUS, _5965, _5964);
    }
    _5965 = NOVALUE;
    _5964 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _5966;
    if( _1 != _5966 ){
        DeRef(_1);
    }
    _5966 = NOVALUE;
    _5962 = NOVALUE;

    /** 	if find('d', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _5967 = (int)*(((s1_ptr)_2)->base + 2);
    _5968 = find_from(100, _5967, 1);
    _5967 = NOVALUE;
    if (_5968 == 0)
    {
        _5968 = NOVALUE;
        goto L6; // [168] 201
    }
    else{
        _5968 = NOVALUE;
    }

    /** 		file_counters[inst[2]][COUNT_DIRS] += 1*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _5969 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_10557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_5969))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_5969)->dbl));
    else
    _3 = (int)(_5969 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _5972 = (int)*(((s1_ptr)_2)->base + 1);
    _5970 = NOVALUE;
    if (IS_ATOM_INT(_5972)) {
        _5973 = _5972 + 1;
        if (_5973 > MAXINT){
            _5973 = NewDouble((double)_5973);
        }
    }
    else
    _5973 = binary_op(PLUS, 1, _5972);
    _5972 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _5973;
    if( _1 != _5973 ){
        DeRef(_1);
    }
    _5973 = NOVALUE;
    _5970 = NOVALUE;
    goto L7; // [198] 512
L6: 

    /** 		file_counters[inst[2]][COUNT_FILES] += 1*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _5974 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_10557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_5974))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_5974)->dbl));
    else
    _3 = (int)(_5974 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _5977 = (int)*(((s1_ptr)_2)->base + 2);
    _5975 = NOVALUE;
    if (IS_ATOM_INT(_5977)) {
        _5978 = _5977 + 1;
        if (_5978 > MAXINT){
            _5978 = NewDouble((double)_5978);
        }
    }
    else
    _5978 = binary_op(PLUS, 1, _5977);
    _5977 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _5978;
    if( _1 != _5978 ){
        DeRef(_1);
    }
    _5978 = NOVALUE;
    _5975 = NOVALUE;

    /** 		ifdef not UNIX then*/

    /** 			ext = fileext(lower(dir_info[D_NAME]))*/
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _5979 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5979);
    _5980 = _16lower(_5979);
    _5979 = NOVALUE;
    DeRef(_path_inlined_fileext_at_242_10605);
    _path_inlined_fileext_at_242_10605 = _5980;
    _5980 = NOVALUE;

    /** 	data = pathinfo(path)*/
    Ref(_path_inlined_fileext_at_242_10605);
    _0 = _data_inlined_fileext_at_245_10606;
    _data_inlined_fileext_at_245_10606 = _8pathinfo(_path_inlined_fileext_at_242_10605, 0);
    DeRef(_0);

    /** 	return data[4]*/
    DeRef(_ext_10564);
    _2 = (int)SEQ_PTR(_data_inlined_fileext_at_245_10606);
    _ext_10564 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_ext_10564);
    DeRef(_path_inlined_fileext_at_242_10605);
    _path_inlined_fileext_at_242_10605 = NOVALUE;
    DeRef(_data_inlined_fileext_at_245_10606);
    _data_inlined_fileext_at_245_10606 = NOVALUE;

    /** 		pos = 0*/
    _pos_10563 = 0;

    /** 		for i = 1 to length(file_counters[inst[2]][COUNT_TYPES]) do*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _5981 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!IS_ATOM_INT(_5981)){
        _5982 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_5981)->dbl));
    }
    else{
        _5982 = (int)*(((s1_ptr)_2)->base + _5981);
    }
    _2 = (int)SEQ_PTR(_5982);
    _5983 = (int)*(((s1_ptr)_2)->base + 4);
    _5982 = NOVALUE;
    if (IS_SEQUENCE(_5983)){
            _5984 = SEQ_PTR(_5983)->length;
    }
    else {
        _5984 = 1;
    }
    _5983 = NOVALUE;
    {
        int _i_10609;
        _i_10609 = 1;
L8: 
        if (_i_10609 > _5984){
            goto L9; // [295] 358
        }

        /** 			if equal(file_counters[inst[2]][COUNT_TYPES][i][EXT_NAME], ext) then*/
        _2 = (int)SEQ_PTR(_inst_10562);
        _5985 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_8file_counters_10557);
        if (!IS_ATOM_INT(_5985)){
            _5986 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_5985)->dbl));
        }
        else{
            _5986 = (int)*(((s1_ptr)_2)->base + _5985);
        }
        _2 = (int)SEQ_PTR(_5986);
        _5987 = (int)*(((s1_ptr)_2)->base + 4);
        _5986 = NOVALUE;
        _2 = (int)SEQ_PTR(_5987);
        _5988 = (int)*(((s1_ptr)_2)->base + _i_10609);
        _5987 = NOVALUE;
        _2 = (int)SEQ_PTR(_5988);
        _5989 = (int)*(((s1_ptr)_2)->base + 1);
        _5988 = NOVALUE;
        if (_5989 == _ext_10564)
        _5990 = 1;
        else if (IS_ATOM_INT(_5989) && IS_ATOM_INT(_ext_10564))
        _5990 = 0;
        else
        _5990 = (compare(_5989, _ext_10564) == 0);
        _5989 = NOVALUE;
        if (_5990 == 0)
        {
            _5990 = NOVALUE;
            goto LA; // [336] 351
        }
        else{
            _5990 = NOVALUE;
        }

        /** 				pos = i*/
        _pos_10563 = _i_10609;

        /** 				exit*/
        goto L9; // [348] 358
LA: 

        /** 		end for*/
        _i_10609 = _i_10609 + 1;
        goto L8; // [353] 302
L9: 
        ;
    }

    /** 		if pos = 0 then*/
    if (_pos_10563 != 0)
    goto LB; // [360] 427

    /** 			file_counters[inst[2]][COUNT_TYPES] &= {{ext, 0, 0}}*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _5992 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_10557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_5992))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_5992)->dbl));
    else
    _3 = (int)(_5992 + ((s1_ptr)_2)->base);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ext_10564);
    *((int *)(_2+4)) = _ext_10564;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    _5995 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5995;
    _5996 = MAKE_SEQ(_1);
    _5995 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _5997 = (int)*(((s1_ptr)_2)->base + 4);
    _5993 = NOVALUE;
    if (IS_SEQUENCE(_5997) && IS_ATOM(_5996)) {
    }
    else if (IS_ATOM(_5997) && IS_SEQUENCE(_5996)) {
        Ref(_5997);
        Prepend(&_5998, _5996, _5997);
    }
    else {
        Concat((object_ptr)&_5998, _5997, _5996);
        _5997 = NOVALUE;
    }
    _5997 = NOVALUE;
    DeRefDS(_5996);
    _5996 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _5998;
    if( _1 != _5998 ){
        DeRef(_1);
    }
    _5998 = NOVALUE;
    _5993 = NOVALUE;

    /** 			pos = length(file_counters[inst[2]][COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _5999 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!IS_ATOM_INT(_5999)){
        _6000 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_5999)->dbl));
    }
    else{
        _6000 = (int)*(((s1_ptr)_2)->base + _5999);
    }
    _2 = (int)SEQ_PTR(_6000);
    _6001 = (int)*(((s1_ptr)_2)->base + 4);
    _6000 = NOVALUE;
    if (IS_SEQUENCE(_6001)){
            _pos_10563 = SEQ_PTR(_6001)->length;
    }
    else {
        _pos_10563 = 1;
    }
    _6001 = NOVALUE;
LB: 

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _6003 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_10557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_6003))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_6003)->dbl));
    else
    _3 = (int)(_6003 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _6004 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_10563 + ((s1_ptr)_2)->base);
    _6004 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _6006 = (int)*(((s1_ptr)_2)->base + 2);
    _6004 = NOVALUE;
    if (IS_ATOM_INT(_6006)) {
        _6007 = _6006 + 1;
        if (_6007 > MAXINT){
            _6007 = NewDouble((double)_6007);
        }
    }
    else
    _6007 = binary_op(PLUS, 1, _6006);
    _6006 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _6007;
    if( _1 != _6007 ){
        DeRef(_1);
    }
    _6007 = NOVALUE;
    _6004 = NOVALUE;

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_10562);
    _6008 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _8file_counters_10557 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_6008))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_6008)->dbl));
    else
    _3 = (int)(_6008 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _6009 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_10563 + ((s1_ptr)_2)->base);
    _6009 = NOVALUE;
    _2 = (int)SEQ_PTR(_dir_info_10561);
    _6011 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _6012 = (int)*(((s1_ptr)_2)->base + 3);
    _6009 = NOVALUE;
    if (IS_ATOM_INT(_6012) && IS_ATOM_INT(_6011)) {
        _6013 = _6012 + _6011;
        if ((long)((unsigned long)_6013 + (unsigned long)HIGH_BITS) >= 0) 
        _6013 = NewDouble((double)_6013);
    }
    else {
        _6013 = binary_op(PLUS, _6012, _6011);
    }
    _6012 = NOVALUE;
    _6011 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _6013;
    if( _1 != _6013 ){
        DeRef(_1);
    }
    _6013 = NOVALUE;
    _6009 = NOVALUE;
L7: 

    /** 	return 0*/
    DeRefDS(_orig_path_10560);
    DeRefDS(_dir_info_10561);
    DeRefDS(_inst_10562);
    DeRef(_ext_10564);
    _5961 = NOVALUE;
    _5969 = NOVALUE;
    _5974 = NOVALUE;
    _5981 = NOVALUE;
    _5983 = NOVALUE;
    _5985 = NOVALUE;
    _5992 = NOVALUE;
    _5999 = NOVALUE;
    _6001 = NOVALUE;
    _6003 = NOVALUE;
    _6008 = NOVALUE;
    return 0;
    ;
}


int _8dir_size(int _dir_path_10647, int _count_all_10648)
{
    int _fc_10649 = NOVALUE;
    int _6028 = NOVALUE;
    int _6027 = NOVALUE;
    int _6025 = NOVALUE;
    int _6024 = NOVALUE;
    int _6022 = NOVALUE;
    int _6021 = NOVALUE;
    int _6020 = NOVALUE;
    int _6019 = NOVALUE;
    int _6018 = NOVALUE;
    int _6017 = NOVALUE;
    int _6014 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_all_10648)) {
        _1 = (long)(DBL_PTR(_count_all_10648)->dbl);
        if (UNIQUE(DBL_PTR(_count_all_10648)) && (DBL_PTR(_count_all_10648)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_all_10648);
        _count_all_10648 = _1;
    }

    /** 	file_counters = append(file_counters, {0,0,0,{}})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    RefDS(_5);
    *((int *)(_2+16)) = _5;
    _6014 = MAKE_SEQ(_1);
    RefDS(_6014);
    Append(&_8file_counters_10557, _8file_counters_10557, _6014);
    DeRefDS(_6014);
    _6014 = NOVALUE;

    /** 	walk_dir(dir_path, {routine_id("count_files"), {count_all, length(file_counters)}}, 0)*/
    _6017 = CRoutineId(380, 8, _6016);
    if (IS_SEQUENCE(_8file_counters_10557)){
            _6018 = SEQ_PTR(_8file_counters_10557)->length;
    }
    else {
        _6018 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _count_all_10648;
    ((int *)_2)[2] = _6018;
    _6019 = MAKE_SEQ(_1);
    _6018 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6017;
    ((int *)_2)[2] = _6019;
    _6020 = MAKE_SEQ(_1);
    _6019 = NOVALUE;
    _6017 = NOVALUE;
    RefDS(_dir_path_10647);
    _6021 = _8walk_dir(_dir_path_10647, _6020, 0, -99999);
    _6020 = NOVALUE;

    /** 	fc = file_counters[$]*/
    if (IS_SEQUENCE(_8file_counters_10557)){
            _6022 = SEQ_PTR(_8file_counters_10557)->length;
    }
    else {
        _6022 = 1;
    }
    DeRef(_fc_10649);
    _2 = (int)SEQ_PTR(_8file_counters_10557);
    _fc_10649 = (int)*(((s1_ptr)_2)->base + _6022);
    RefDS(_fc_10649);

    /** 	file_counters = file_counters[1 .. $-1]*/
    if (IS_SEQUENCE(_8file_counters_10557)){
            _6024 = SEQ_PTR(_8file_counters_10557)->length;
    }
    else {
        _6024 = 1;
    }
    _6025 = _6024 - 1;
    _6024 = NOVALUE;
    rhs_slice_target = (object_ptr)&_8file_counters_10557;
    RHS_Slice(_8file_counters_10557, 1, _6025);

    /** 	fc[COUNT_TYPES] = stdsort:sort(fc[COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_fc_10649);
    _6027 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_6027);
    _6028 = _7sort(_6027, 1);
    _6027 = NOVALUE;
    _2 = (int)SEQ_PTR(_fc_10649);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _fc_10649 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _6028;
    if( _1 != _6028 ){
        DeRef(_1);
    }
    _6028 = NOVALUE;

    /** 	return fc*/
    DeRefDS(_dir_path_10647);
    _6025 = NOVALUE;
    DeRef(_6021);
    _6021 = NOVALUE;
    return _fc_10649;
    ;
}


int _8temp_file(int _temp_location_10667, int _temp_prefix_10668, int _temp_extn_10669, int _reserve_temp_10671)
{
    int _randname_10672 = NOVALUE;
    int _envtmp_10676 = NOVALUE;
    int _tdir_10695 = NOVALUE;
    int _6067 = NOVALUE;
    int _6065 = NOVALUE;
    int _6063 = NOVALUE;
    int _6061 = NOVALUE;
    int _6059 = NOVALUE;
    int _6058 = NOVALUE;
    int _6057 = NOVALUE;
    int _6053 = NOVALUE;
    int _6052 = NOVALUE;
    int _6051 = NOVALUE;
    int _6050 = NOVALUE;
    int _6047 = NOVALUE;
    int _6046 = NOVALUE;
    int _6045 = NOVALUE;
    int _6040 = NOVALUE;
    int _6037 = NOVALUE;
    int _6034 = NOVALUE;
    int _6030 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_reserve_temp_10671)) {
        _1 = (long)(DBL_PTR(_reserve_temp_10671)->dbl);
        if (UNIQUE(DBL_PTR(_reserve_temp_10671)) && (DBL_PTR(_reserve_temp_10671)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_reserve_temp_10671);
        _reserve_temp_10671 = _1;
    }

    /** 	if length(temp_location) = 0 then*/
    if (IS_SEQUENCE(_temp_location_10667)){
            _6030 = SEQ_PTR(_temp_location_10667)->length;
    }
    else {
        _6030 = 1;
    }
    if (_6030 != 0)
    goto L1; // [16] 69

    /** 		object envtmp*/

    /** 		envtmp = getenv("TEMP")*/
    DeRefi(_envtmp_10676);
    _envtmp_10676 = EGetEnv(_6032);

    /** 		if atom(envtmp) then*/
    _6034 = IS_ATOM(_envtmp_10676);
    if (_6034 == 0)
    {
        _6034 = NOVALUE;
        goto L2; // [32] 41
    }
    else{
        _6034 = NOVALUE;
    }

    /** 			envtmp = getenv("TMP")*/
    DeRefi(_envtmp_10676);
    _envtmp_10676 = EGetEnv(_6035);
L2: 

    /** 		ifdef WINDOWS then			*/

    /** 			if atom(envtmp) then*/
    _6037 = IS_ATOM(_envtmp_10676);
    if (_6037 == 0)
    {
        _6037 = NOVALUE;
        goto L3; // [48] 57
    }
    else{
        _6037 = NOVALUE;
    }

    /** 				envtmp = "C:\\temp\\"*/
    RefDS(_6038);
    DeRefi(_envtmp_10676);
    _envtmp_10676 = _6038;
L3: 

    /** 		temp_location = envtmp*/
    Ref(_envtmp_10676);
    DeRefDS(_temp_location_10667);
    _temp_location_10667 = _envtmp_10676;
    DeRefi(_envtmp_10676);
    _envtmp_10676 = NOVALUE;
    goto L4; // [66] 163
L1: 

    /** 		switch file_type(temp_location) do*/
    RefDS(_temp_location_10667);
    _6040 = _8file_type(_temp_location_10667);
    if (IS_SEQUENCE(_6040) ){
        goto L5; // [75] 152
    }
    if(!IS_ATOM_INT(_6040)){
        if( (DBL_PTR(_6040)->dbl != (double) ((int) DBL_PTR(_6040)->dbl) ) ){
            goto L5; // [75] 152
        }
        _0 = (int) DBL_PTR(_6040)->dbl;
    }
    else {
        _0 = _6040;
    };
    DeRef(_6040);
    _6040 = NOVALUE;
    switch ( _0 ){ 

        /** 			case FILETYPE_FILE then*/
        case 1:

        /** 				temp_location = dirname(temp_location, 1)*/
        RefDS(_temp_location_10667);
        _0 = _temp_location_10667;
        _temp_location_10667 = _8dirname(_temp_location_10667, 1);
        DeRefDS(_0);
        goto L6; // [93] 162

        /** 			case FILETYPE_DIRECTORY then*/
        case 2:

        /** 				temp_location = temp_location*/
        RefDS(_temp_location_10667);
        DeRefDS(_temp_location_10667);
        _temp_location_10667 = _temp_location_10667;
        goto L6; // [106] 162

        /** 			case FILETYPE_NOT_FOUND then*/
        case 0:

        /** 				object tdir = dirname(temp_location, 1)*/
        RefDS(_temp_location_10667);
        _0 = _tdir_10695;
        _tdir_10695 = _8dirname(_temp_location_10667, 1);
        DeRef(_0);

        /** 				if file_exists(tdir) then*/
        Ref(_tdir_10695);
        _6045 = _8file_exists(_tdir_10695);
        if (_6045 == 0) {
            DeRef(_6045);
            _6045 = NOVALUE;
            goto L7; // [125] 138
        }
        else {
            if (!IS_ATOM_INT(_6045) && DBL_PTR(_6045)->dbl == 0.0){
                DeRef(_6045);
                _6045 = NOVALUE;
                goto L7; // [125] 138
            }
            DeRef(_6045);
            _6045 = NOVALUE;
        }
        DeRef(_6045);
        _6045 = NOVALUE;

        /** 					temp_location = tdir*/
        Ref(_tdir_10695);
        DeRefDS(_temp_location_10667);
        _temp_location_10667 = _tdir_10695;
        goto L8; // [135] 146
L7: 

        /** 					temp_location = "."*/
        RefDS(_5182);
        DeRefDS(_temp_location_10667);
        _temp_location_10667 = _5182;
L8: 
        DeRef(_tdir_10695);
        _tdir_10695 = NOVALUE;
        goto L6; // [148] 162

        /** 			case else*/
        default:
L5: 

        /** 				temp_location = "."*/
        RefDS(_5182);
        DeRefDS(_temp_location_10667);
        _temp_location_10667 = _5182;
    ;}L6: 
L4: 

    /** 	if temp_location[$] != SLASH then*/
    if (IS_SEQUENCE(_temp_location_10667)){
            _6046 = SEQ_PTR(_temp_location_10667)->length;
    }
    else {
        _6046 = 1;
    }
    _2 = (int)SEQ_PTR(_temp_location_10667);
    _6047 = (int)*(((s1_ptr)_2)->base + _6046);
    if (binary_op_a(EQUALS, _6047, 92)){
        _6047 = NOVALUE;
        goto L9; // [172] 183
    }
    _6047 = NOVALUE;

    /** 		temp_location &= SLASH*/
    Append(&_temp_location_10667, _temp_location_10667, 92);
L9: 

    /** 	if length(temp_extn) and temp_extn[1] != '.' then*/
    if (IS_SEQUENCE(_temp_extn_10669)){
            _6050 = SEQ_PTR(_temp_extn_10669)->length;
    }
    else {
        _6050 = 1;
    }
    if (_6050 == 0) {
        goto LA; // [188] 211
    }
    _2 = (int)SEQ_PTR(_temp_extn_10669);
    _6052 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_6052)) {
        _6053 = (_6052 != 46);
    }
    else {
        _6053 = binary_op(NOTEQ, _6052, 46);
    }
    _6052 = NOVALUE;
    if (_6053 == 0) {
        DeRef(_6053);
        _6053 = NOVALUE;
        goto LA; // [201] 211
    }
    else {
        if (!IS_ATOM_INT(_6053) && DBL_PTR(_6053)->dbl == 0.0){
            DeRef(_6053);
            _6053 = NOVALUE;
            goto LA; // [201] 211
        }
        DeRef(_6053);
        _6053 = NOVALUE;
    }
    DeRef(_6053);
    _6053 = NOVALUE;

    /** 		temp_extn = '.' & temp_extn*/
    Prepend(&_temp_extn_10669, _temp_extn_10669, 46);
LA: 

    /** 	while 1 do*/
LB: 

    /** 		randname = sprintf("%s%s%06d%s", {temp_location, temp_prefix, rand(1_000_000) - 1, temp_extn})*/
    _6057 = good_rand() % ((unsigned)1000000) + 1;
    _6058 = _6057 - 1;
    _6057 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_temp_location_10667);
    *((int *)(_2+4)) = _temp_location_10667;
    RefDS(_temp_prefix_10668);
    *((int *)(_2+8)) = _temp_prefix_10668;
    *((int *)(_2+12)) = _6058;
    RefDS(_temp_extn_10669);
    *((int *)(_2+16)) = _temp_extn_10669;
    _6059 = MAKE_SEQ(_1);
    _6058 = NOVALUE;
    DeRefi(_randname_10672);
    _randname_10672 = EPrintf(-9999999, _6055, _6059);
    DeRefDS(_6059);
    _6059 = NOVALUE;

    /** 		if not file_exists( randname ) then*/
    RefDS(_randname_10672);
    _6061 = _8file_exists(_randname_10672);
    if (IS_ATOM_INT(_6061)) {
        if (_6061 != 0){
            DeRef(_6061);
            _6061 = NOVALUE;
            goto LB; // [242] 216
        }
    }
    else {
        if (DBL_PTR(_6061)->dbl != 0.0){
            DeRef(_6061);
            _6061 = NOVALUE;
            goto LB; // [242] 216
        }
    }
    DeRef(_6061);
    _6061 = NOVALUE;

    /** 			exit*/
    goto LC; // [247] 255

    /** 	end while*/
    goto LB; // [252] 216
LC: 

    /** 	if reserve_temp then*/
    if (_reserve_temp_10671 == 0)
    {
        goto LD; // [257] 302
    }
    else{
    }

    /** 		if not file_exists(temp_location) then*/
    RefDS(_temp_location_10667);
    _6063 = _8file_exists(_temp_location_10667);
    if (IS_ATOM_INT(_6063)) {
        if (_6063 != 0){
            DeRef(_6063);
            _6063 = NOVALUE;
            goto LE; // [266] 289
        }
    }
    else {
        if (DBL_PTR(_6063)->dbl != 0.0){
            DeRef(_6063);
            _6063 = NOVALUE;
            goto LE; // [266] 289
        }
    }
    DeRef(_6063);
    _6063 = NOVALUE;

    /** 			if create_directory(temp_location) = 0 then*/
    RefDS(_temp_location_10667);
    _6065 = _8create_directory(_temp_location_10667, 448, 1);
    if (binary_op_a(NOTEQ, _6065, 0)){
        DeRef(_6065);
        _6065 = NOVALUE;
        goto LF; // [277] 288
    }
    DeRef(_6065);
    _6065 = NOVALUE;

    /** 				return ""*/
    RefDS(_5);
    DeRefDS(_temp_location_10667);
    DeRefDS(_temp_prefix_10668);
    DeRefDS(_temp_extn_10669);
    DeRefi(_randname_10672);
    return _5;
LF: 
LE: 

    /** 		io:write_file(randname, "")*/
    RefDS(_randname_10672);
    RefDS(_5);
    _6067 = _15write_file(_randname_10672, _5, 1);
LD: 

    /** 	return randname*/
    DeRefDS(_temp_location_10667);
    DeRefDS(_temp_prefix_10668);
    DeRefDS(_temp_extn_10669);
    DeRef(_6067);
    _6067 = NOVALUE;
    return _randname_10672;
    ;
}


int _8checksum(int _filename_10732, int _size_10733, int _usename_10734, int _return_text_10735)
{
    int _fn_10736 = NOVALUE;
    int _cs_10737 = NOVALUE;
    int _hits_10738 = NOVALUE;
    int _ix_10739 = NOVALUE;
    int _jx_10740 = NOVALUE;
    int _fx_10741 = NOVALUE;
    int _data_10742 = NOVALUE;
    int _nhit_10743 = NOVALUE;
    int _nmiss_10744 = NOVALUE;
    int _cs_text_10816 = NOVALUE;
    int _6127 = NOVALUE;
    int _6125 = NOVALUE;
    int _6124 = NOVALUE;
    int _6122 = NOVALUE;
    int _6120 = NOVALUE;
    int _6119 = NOVALUE;
    int _6118 = NOVALUE;
    int _6117 = NOVALUE;
    int _6116 = NOVALUE;
    int _6114 = NOVALUE;
    int _6112 = NOVALUE;
    int _6110 = NOVALUE;
    int _6109 = NOVALUE;
    int _6108 = NOVALUE;
    int _6107 = NOVALUE;
    int _6106 = NOVALUE;
    int _6105 = NOVALUE;
    int _6104 = NOVALUE;
    int _6102 = NOVALUE;
    int _6099 = NOVALUE;
    int _6097 = NOVALUE;
    int _6096 = NOVALUE;
    int _6095 = NOVALUE;
    int _6094 = NOVALUE;
    int _6093 = NOVALUE;
    int _6090 = NOVALUE;
    int _6089 = NOVALUE;
    int _6088 = NOVALUE;
    int _6087 = NOVALUE;
    int _6085 = NOVALUE;
    int _6082 = NOVALUE;
    int _6080 = NOVALUE;
    int _6076 = NOVALUE;
    int _6075 = NOVALUE;
    int _6074 = NOVALUE;
    int _6073 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_10733)) {
        _1 = (long)(DBL_PTR(_size_10733)->dbl);
        if (UNIQUE(DBL_PTR(_size_10733)) && (DBL_PTR(_size_10733)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_10733);
        _size_10733 = _1;
    }
    if (!IS_ATOM_INT(_usename_10734)) {
        _1 = (long)(DBL_PTR(_usename_10734)->dbl);
        if (UNIQUE(DBL_PTR(_usename_10734)) && (DBL_PTR(_usename_10734)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_usename_10734);
        _usename_10734 = _1;
    }
    if (!IS_ATOM_INT(_return_text_10735)) {
        _1 = (long)(DBL_PTR(_return_text_10735)->dbl);
        if (UNIQUE(DBL_PTR(_return_text_10735)) && (DBL_PTR(_return_text_10735)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_return_text_10735);
        _return_text_10735 = _1;
    }

    /** 	if size <= 0 then*/
    if (_size_10733 > 0)
    goto L1; // [17] 28

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_10732);
    DeRef(_cs_10737);
    DeRef(_hits_10738);
    DeRef(_jx_10740);
    DeRef(_fx_10741);
    DeRefi(_data_10742);
    return _5;
L1: 

    /** 	fn = open(filename, "rb")*/
    _fn_10736 = EOpen(_filename_10732, _2913, 0);

    /** 	if fn = -1 then*/
    if (_fn_10736 != -1)
    goto L2; // [39] 50

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_10732);
    DeRef(_cs_10737);
    DeRef(_hits_10738);
    DeRef(_jx_10740);
    DeRef(_fx_10741);
    DeRefi(_data_10742);
    return _5;
L2: 

    /** 	jx = file_length(filename)*/
    RefDS(_filename_10732);
    _0 = _jx_10740;
    _jx_10740 = _8file_length(_filename_10732);
    DeRef(_0);

    /** 	cs = repeat(jx, size)*/
    DeRef(_cs_10737);
    _cs_10737 = Repeat(_jx_10740, _size_10733);

    /** 	for i = 1 to size do*/
    _6073 = _size_10733;
    {
        int _i_10753;
        _i_10753 = 1;
L3: 
        if (_i_10753 > _6073){
            goto L4; // [67] 99
        }

        /** 		cs[i] = hash(i + size, cs[i])*/
        _6074 = _i_10753 + _size_10733;
        if ((long)((unsigned long)_6074 + (unsigned long)HIGH_BITS) >= 0) 
        _6074 = NewDouble((double)_6074);
        _2 = (int)SEQ_PTR(_cs_10737);
        _6075 = (int)*(((s1_ptr)_2)->base + _i_10753);
        _6076 = calc_hash(_6074, _6075);
        DeRef(_6074);
        _6074 = NOVALUE;
        _6075 = NOVALUE;
        _2 = (int)SEQ_PTR(_cs_10737);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _cs_10737 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_10753);
        _1 = *(int *)_2;
        *(int *)_2 = _6076;
        if( _1 != _6076 ){
            DeRef(_1);
        }
        _6076 = NOVALUE;

        /** 	end for*/
        _i_10753 = _i_10753 + 1;
        goto L3; // [94] 74
L4: 
        ;
    }

    /** 	if usename != 0 then*/
    if (_usename_10734 == 0)
    goto L5; // [101] 236

    /** 		nhit = 0*/
    _nhit_10743 = 0;

    /** 		nmiss = 0*/
    _nmiss_10744 = 0;

    /** 		hits = {0,0}*/
    DeRef(_hits_10738);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _hits_10738 = MAKE_SEQ(_1);

    /** 		fx = hash(filename, stdhash:HSIEH32) -- Get a hash value for the whole name.*/
    DeRef(_fx_10741);
    _fx_10741 = calc_hash(_filename_10732, -5);

    /** 		while find(0, hits) do*/
L6: 
    _6080 = find_from(0, _hits_10738, 1);
    if (_6080 == 0)
    {
        _6080 = NOVALUE;
        goto L7; // [143] 235
    }
    else{
        _6080 = NOVALUE;
    }

    /** 			nhit += 1*/
    _nhit_10743 = _nhit_10743 + 1;

    /** 			if nhit > length(filename) then*/
    if (IS_SEQUENCE(_filename_10732)){
            _6082 = SEQ_PTR(_filename_10732)->length;
    }
    else {
        _6082 = 1;
    }
    if (_nhit_10743 <= _6082)
    goto L8; // [159] 177

    /** 				nhit = 1*/
    _nhit_10743 = 1;

    /** 				hits[1] = 1*/
    _2 = (int)SEQ_PTR(_hits_10738);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L8: 

    /** 			nmiss += 1*/
    _nmiss_10744 = _nmiss_10744 + 1;

    /** 			if nmiss > length(cs) then*/
    if (IS_SEQUENCE(_cs_10737)){
            _6085 = SEQ_PTR(_cs_10737)->length;
    }
    else {
        _6085 = 1;
    }
    if (_nmiss_10744 <= _6085)
    goto L9; // [190] 208

    /** 				nmiss = 1*/
    _nmiss_10744 = 1;

    /** 				hits[2] = 1*/
    _2 = (int)SEQ_PTR(_hits_10738);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L9: 

    /** 			cs[nmiss] = hash(filename[nhit], xor_bits(fx, cs[nmiss]))*/
    _2 = (int)SEQ_PTR(_filename_10732);
    _6087 = (int)*(((s1_ptr)_2)->base + _nhit_10743);
    _2 = (int)SEQ_PTR(_cs_10737);
    _6088 = (int)*(((s1_ptr)_2)->base + _nmiss_10744);
    if (IS_ATOM_INT(_fx_10741) && IS_ATOM_INT(_6088)) {
        {unsigned long tu;
             tu = (unsigned long)_fx_10741 ^ (unsigned long)_6088;
             _6089 = MAKE_UINT(tu);
        }
    }
    else {
        _6089 = binary_op(XOR_BITS, _fx_10741, _6088);
    }
    _6088 = NOVALUE;
    _6090 = calc_hash(_6087, _6089);
    _6087 = NOVALUE;
    DeRef(_6089);
    _6089 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_10737);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_10737 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_10744);
    _1 = *(int *)_2;
    *(int *)_2 = _6090;
    if( _1 != _6090 ){
        DeRef(_1);
    }
    _6090 = NOVALUE;

    /** 		end while -- repeat until every bucket and every character has been used.*/
    goto L6; // [232] 138
L7: 
L5: 

    /** 	hits = repeat(0, size)*/
    DeRef(_hits_10738);
    _hits_10738 = Repeat(0, _size_10733);

    /** 	if jx != 0 then*/
    if (binary_op_a(EQUALS, _jx_10740, 0)){
        goto LA; // [244] 494
    }

    /** 		data = repeat(0, remainder( hash(jx * jx / size , stdhash:HSIEH32), 8) + 7)*/
    if (IS_ATOM_INT(_jx_10740) && IS_ATOM_INT(_jx_10740)) {
        if (_jx_10740 == (short)_jx_10740 && _jx_10740 <= INT15 && _jx_10740 >= -INT15)
        _6093 = _jx_10740 * _jx_10740;
        else
        _6093 = NewDouble(_jx_10740 * (double)_jx_10740);
    }
    else {
        if (IS_ATOM_INT(_jx_10740)) {
            _6093 = NewDouble((double)_jx_10740 * DBL_PTR(_jx_10740)->dbl);
        }
        else {
            if (IS_ATOM_INT(_jx_10740)) {
                _6093 = NewDouble(DBL_PTR(_jx_10740)->dbl * (double)_jx_10740);
            }
            else
            _6093 = NewDouble(DBL_PTR(_jx_10740)->dbl * DBL_PTR(_jx_10740)->dbl);
        }
    }
    if (IS_ATOM_INT(_6093)) {
        _6094 = (_6093 % _size_10733) ? NewDouble((double)_6093 / _size_10733) : (_6093 / _size_10733);
    }
    else {
        _6094 = NewDouble(DBL_PTR(_6093)->dbl / (double)_size_10733);
    }
    DeRef(_6093);
    _6093 = NOVALUE;
    _6095 = calc_hash(_6094, -5);
    DeRef(_6094);
    _6094 = NOVALUE;
    if (IS_ATOM_INT(_6095)) {
        _6096 = (_6095 % 8);
    }
    else {
        temp_d.dbl = (double)8;
        _6096 = Dremainder(DBL_PTR(_6095), &temp_d);
    }
    DeRef(_6095);
    _6095 = NOVALUE;
    if (IS_ATOM_INT(_6096)) {
        _6097 = _6096 + 7;
    }
    else {
        _6097 = NewDouble(DBL_PTR(_6096)->dbl + (double)7);
    }
    DeRef(_6096);
    _6096 = NOVALUE;
    DeRefi(_data_10742);
    _data_10742 = Repeat(0, _6097);
    DeRef(_6097);
    _6097 = NOVALUE;

    /** 		while data[1] != -1 with entry do*/
    goto LB; // [278] 344
LC: 
    _2 = (int)SEQ_PTR(_data_10742);
    _6099 = (int)*(((s1_ptr)_2)->base + 1);
    if (_6099 == -1)
    goto LD; // [285] 377

    /** 			jx = hash(jx, data)*/
    _0 = _jx_10740;
    _jx_10740 = calc_hash(_jx_10740, _data_10742);
    DeRef(_0);

    /** 			ix = remainder(jx, size) + 1*/
    if (IS_ATOM_INT(_jx_10740)) {
        _6102 = (_jx_10740 % _size_10733);
    }
    else {
        temp_d.dbl = (double)_size_10733;
        _6102 = Dremainder(DBL_PTR(_jx_10740), &temp_d);
    }
    if (IS_ATOM_INT(_6102)) {
        _ix_10739 = _6102 + 1;
    }
    else
    { // coercing _ix_10739 to an integer 1
        _ix_10739 = 1+(long)(DBL_PTR(_6102)->dbl);
        if( !IS_ATOM_INT(_ix_10739) ){
            _ix_10739 = (object)DBL_PTR(_ix_10739)->dbl;
        }
    }
    DeRef(_6102);
    _6102 = NOVALUE;

    /** 			cs[ix] = xor_bits(cs[ix], hash(data, stdhash:HSIEH32))*/
    _2 = (int)SEQ_PTR(_cs_10737);
    _6104 = (int)*(((s1_ptr)_2)->base + _ix_10739);
    _6105 = calc_hash(_data_10742, -5);
    if (IS_ATOM_INT(_6104) && IS_ATOM_INT(_6105)) {
        {unsigned long tu;
             tu = (unsigned long)_6104 ^ (unsigned long)_6105;
             _6106 = MAKE_UINT(tu);
        }
    }
    else {
        _6106 = binary_op(XOR_BITS, _6104, _6105);
    }
    _6104 = NOVALUE;
    DeRef(_6105);
    _6105 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_10737);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_10737 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ix_10739);
    _1 = *(int *)_2;
    *(int *)_2 = _6106;
    if( _1 != _6106 ){
        DeRef(_1);
    }
    _6106 = NOVALUE;

    /** 			hits[ix] += 1*/
    _2 = (int)SEQ_PTR(_hits_10738);
    _6107 = (int)*(((s1_ptr)_2)->base + _ix_10739);
    if (IS_ATOM_INT(_6107)) {
        _6108 = _6107 + 1;
        if (_6108 > MAXINT){
            _6108 = NewDouble((double)_6108);
        }
    }
    else
    _6108 = binary_op(PLUS, 1, _6107);
    _6107 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_10738);
    _2 = (int)(((s1_ptr)_2)->base + _ix_10739);
    _1 = *(int *)_2;
    *(int *)_2 = _6108;
    if( _1 != _6108 ){
        DeRef(_1);
    }
    _6108 = NOVALUE;

    /** 		entry*/
LB: 

    /** 			for i = 1 to length(data) do*/
    if (IS_SEQUENCE(_data_10742)){
            _6109 = SEQ_PTR(_data_10742)->length;
    }
    else {
        _6109 = 1;
    }
    {
        int _i_10797;
        _i_10797 = 1;
LE: 
        if (_i_10797 > _6109){
            goto LF; // [349] 372
        }

        /** 				data[i] = getc(fn)*/
        if (_fn_10736 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_10736, EF_READ);
            last_r_file_no = _fn_10736;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _6110 = getKBchar();
            }
            else
            _6110 = getc(last_r_file_ptr);
        }
        else
        _6110 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_data_10742);
        _2 = (int)(((s1_ptr)_2)->base + _i_10797);
        *(int *)_2 = _6110;
        if( _1 != _6110 ){
        }
        _6110 = NOVALUE;

        /** 			end for*/
        _i_10797 = _i_10797 + 1;
        goto LE; // [367] 356
LF: 
        ;
    }

    /** 		end while*/
    goto LC; // [374] 281
LD: 

    /** 		nhit = 0*/
    _nhit_10743 = 0;

    /** 		while nmiss with entry do*/
    goto L10; // [386] 479
L11: 
    if (_nmiss_10744 == 0)
    {
        goto L12; // [391] 493
    }
    else{
    }

    /** 			while 1 do*/
L13: 

    /** 				nhit += 1*/
    _nhit_10743 = _nhit_10743 + 1;

    /** 				if nhit > length(hits) then*/
    if (IS_SEQUENCE(_hits_10738)){
            _6112 = SEQ_PTR(_hits_10738)->length;
    }
    else {
        _6112 = 1;
    }
    if (_nhit_10743 <= _6112)
    goto L14; // [412] 424

    /** 					nhit = 1*/
    _nhit_10743 = 1;
L14: 

    /** 				if hits[nhit] != 0 then*/
    _2 = (int)SEQ_PTR(_hits_10738);
    _6114 = (int)*(((s1_ptr)_2)->base + _nhit_10743);
    if (binary_op_a(EQUALS, _6114, 0)){
        _6114 = NOVALUE;
        goto L13; // [430] 399
    }
    _6114 = NOVALUE;

    /** 					exit*/
    goto L15; // [436] 444

    /** 			end while*/
    goto L13; // [441] 399
L15: 

    /** 			cs[nmiss] = hash(cs[nmiss], cs[nhit])*/
    _2 = (int)SEQ_PTR(_cs_10737);
    _6116 = (int)*(((s1_ptr)_2)->base + _nmiss_10744);
    _2 = (int)SEQ_PTR(_cs_10737);
    _6117 = (int)*(((s1_ptr)_2)->base + _nhit_10743);
    _6118 = calc_hash(_6116, _6117);
    _6116 = NOVALUE;
    _6117 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_10737);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_10737 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_10744);
    _1 = *(int *)_2;
    *(int *)_2 = _6118;
    if( _1 != _6118 ){
        DeRef(_1);
    }
    _6118 = NOVALUE;

    /** 			hits[nmiss] += 1*/
    _2 = (int)SEQ_PTR(_hits_10738);
    _6119 = (int)*(((s1_ptr)_2)->base + _nmiss_10744);
    if (IS_ATOM_INT(_6119)) {
        _6120 = _6119 + 1;
        if (_6120 > MAXINT){
            _6120 = NewDouble((double)_6120);
        }
    }
    else
    _6120 = binary_op(PLUS, 1, _6119);
    _6119 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_10738);
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_10744);
    _1 = *(int *)_2;
    *(int *)_2 = _6120;
    if( _1 != _6120 ){
        DeRef(_1);
    }
    _6120 = NOVALUE;

    /** 		entry*/
L10: 

    /** 			nmiss = find(0, hits)	*/
    _nmiss_10744 = find_from(0, _hits_10738, 1);

    /** 		end while*/
    goto L11; // [490] 389
L12: 
LA: 

    /** 	close(fn)*/
    EClose(_fn_10736);

    /** 	if return_text then*/
    if (_return_text_10735 == 0)
    {
        goto L16; // [500] 571
    }
    else{
    }

    /** 		sequence cs_text = ""*/
    RefDS(_5);
    DeRef(_cs_text_10816);
    _cs_text_10816 = _5;

    /** 		for i = 1 to length(cs) do*/
    if (IS_SEQUENCE(_cs_10737)){
            _6122 = SEQ_PTR(_cs_10737)->length;
    }
    else {
        _6122 = 1;
    }
    {
        int _i_10818;
        _i_10818 = 1;
L17: 
        if (_i_10818 > _6122){
            goto L18; // [515] 560
        }

        /** 			cs_text &= text:format("[:08X]", cs[i])*/
        _2 = (int)SEQ_PTR(_cs_10737);
        _6124 = (int)*(((s1_ptr)_2)->base + _i_10818);
        RefDS(_6123);
        Ref(_6124);
        _6125 = _16format(_6123, _6124);
        _6124 = NOVALUE;
        if (IS_SEQUENCE(_cs_text_10816) && IS_ATOM(_6125)) {
            Ref(_6125);
            Append(&_cs_text_10816, _cs_text_10816, _6125);
        }
        else if (IS_ATOM(_cs_text_10816) && IS_SEQUENCE(_6125)) {
        }
        else {
            Concat((object_ptr)&_cs_text_10816, _cs_text_10816, _6125);
        }
        DeRef(_6125);
        _6125 = NOVALUE;

        /** 			if i != length(cs) then*/
        if (IS_SEQUENCE(_cs_10737)){
                _6127 = SEQ_PTR(_cs_10737)->length;
        }
        else {
            _6127 = 1;
        }
        if (_i_10818 == _6127)
        goto L19; // [542] 553

        /** 				cs_text &= ' '*/
        Append(&_cs_text_10816, _cs_text_10816, 32);
L19: 

        /** 		end for*/
        _i_10818 = _i_10818 + 1;
        goto L17; // [555] 522
L18: 
        ;
    }

    /** 		return cs_text*/
    DeRefDS(_filename_10732);
    DeRef(_cs_10737);
    DeRef(_hits_10738);
    DeRef(_jx_10740);
    DeRef(_fx_10741);
    DeRefi(_data_10742);
    _6099 = NOVALUE;
    return _cs_text_10816;
    DeRefDS(_cs_text_10816);
    _cs_text_10816 = NOVALUE;
    goto L1A; // [568] 578
L16: 

    /** 		return cs*/
    DeRefDS(_filename_10732);
    DeRef(_hits_10738);
    DeRef(_jx_10740);
    DeRef(_fx_10741);
    DeRefi(_data_10742);
    _6099 = NOVALUE;
    return _cs_10737;
L1A: 
    ;
}



// 0xE861E357
