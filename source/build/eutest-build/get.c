// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _14get_ch()
{
    int _4202 = NOVALUE;
    int _4201 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(input_string) then*/
    _4201 = IS_SEQUENCE(_14input_string_7685);
    if (_4201 == 0)
    {
        _4201 = NOVALUE;
        goto L1; // [8] 56
    }
    else{
        _4201 = NOVALUE;
    }

    /** 		if string_next <= length(input_string) then*/
    if (IS_SEQUENCE(_14input_string_7685)){
            _4202 = SEQ_PTR(_14input_string_7685)->length;
    }
    else {
        _4202 = 1;
    }
    if (_14string_next_7686 > _4202)
    goto L2; // [20] 47

    /** 			ch = input_string[string_next]*/
    _2 = (int)SEQ_PTR(_14input_string_7685);
    _14ch_7687 = (int)*(((s1_ptr)_2)->base + _14string_next_7686);
    if (!IS_ATOM_INT(_14ch_7687)){
        _14ch_7687 = (long)DBL_PTR(_14ch_7687)->dbl;
    }

    /** 			string_next += 1*/
    _14string_next_7686 = _14string_next_7686 + 1;
    goto L3; // [44] 81
L2: 

    /** 			ch = GET_EOF*/
    _14ch_7687 = -1;
    goto L3; // [53] 81
L1: 

    /** 		ch = getc(input_file)*/
    if (_14input_file_7684 != last_r_file_no) {
        last_r_file_ptr = which_file(_14input_file_7684, EF_READ);
        last_r_file_no = _14input_file_7684;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _14ch_7687 = getKBchar();
        }
        else
        _14ch_7687 = getc(last_r_file_ptr);
    }
    else
    _14ch_7687 = getc(last_r_file_ptr);

    /** 		if ch = GET_EOF then*/
    if (_14ch_7687 != -1)
    goto L4; // [67] 80

    /** 			string_next += 1*/
    _14string_next_7686 = _14string_next_7686 + 1;
L4: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _14escape_char(int _c_7714)
{
    int _i_7715 = NOVALUE;
    int _4214 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = find(c, ESCAPE_CHARS)*/
    _i_7715 = find_from(_c_7714, _14ESCAPE_CHARS_7708, 1);

    /** 	if i = 0 then*/
    if (_i_7715 != 0)
    goto L1; // [12] 25

    /** 		return GET_FAIL*/
    return 1;
    goto L2; // [22] 36
L1: 

    /** 		return ESCAPED_CHARS[i]*/
    _2 = (int)SEQ_PTR(_14ESCAPED_CHARS_7710);
    _4214 = (int)*(((s1_ptr)_2)->base + _i_7715);
    Ref(_4214);
    return _4214;
L2: 
    ;
}


int _14get_qchar()
{
    int _c_7723 = NOVALUE;
    int _4223 = NOVALUE;
    int _4222 = NOVALUE;
    int _4220 = NOVALUE;
    int _4218 = NOVALUE;
    int _0, _1, _2;
    

    /** 	get_ch()*/
    _14get_ch();

    /** 	c = ch*/
    _c_7723 = _14ch_7687;

    /** 	if ch = '\\' then*/
    if (_14ch_7687 != 92)
    goto L1; // [16] 54

    /** 		get_ch()*/
    _14get_ch();

    /** 		c = escape_char(ch)*/
    _c_7723 = _14escape_char(_14ch_7687);
    if (!IS_ATOM_INT(_c_7723)) {
        _1 = (long)(DBL_PTR(_c_7723)->dbl);
        if (UNIQUE(DBL_PTR(_c_7723)) && (DBL_PTR(_c_7723)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_7723);
        _c_7723 = _1;
    }

    /** 		if c = GET_FAIL then*/
    if (_c_7723 != 1)
    goto L2; // [36] 74

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4218 = MAKE_SEQ(_1);
    return _4218;
    goto L2; // [51] 74
L1: 

    /** 	elsif ch = '\'' then*/
    if (_14ch_7687 != 39)
    goto L3; // [58] 73

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4220 = MAKE_SEQ(_1);
    DeRef(_4218);
    _4218 = NOVALUE;
    return _4220;
L3: 
L2: 

    /** 	get_ch()*/
    _14get_ch();

    /** 	if ch != '\'' then*/
    if (_14ch_7687 == 39)
    goto L4; // [82] 99

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4222 = MAKE_SEQ(_1);
    DeRef(_4218);
    _4218 = NOVALUE;
    DeRef(_4220);
    _4220 = NOVALUE;
    return _4222;
    goto L5; // [96] 114
L4: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		return {GET_SUCCESS, c}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _c_7723;
    _4223 = MAKE_SEQ(_1);
    DeRef(_4218);
    _4218 = NOVALUE;
    DeRef(_4220);
    _4220 = NOVALUE;
    DeRef(_4222);
    _4222 = NOVALUE;
    return _4223;
L5: 
    ;
}


int _14get_heredoc(int _terminator_7740)
{
    int _text_7741 = NOVALUE;
    int _ends_at_7742 = NOVALUE;
    int _4238 = NOVALUE;
    int _4237 = NOVALUE;
    int _4236 = NOVALUE;
    int _4235 = NOVALUE;
    int _4234 = NOVALUE;
    int _4231 = NOVALUE;
    int _4229 = NOVALUE;
    int _4228 = NOVALUE;
    int _4227 = NOVALUE;
    int _4226 = NOVALUE;
    int _4224 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence text = ""*/
    RefDS(_5);
    DeRefi(_text_7741);
    _text_7741 = _5;

    /** 	integer ends_at = 1 - length( terminator )*/
    if (IS_SEQUENCE(_terminator_7740)){
            _4224 = SEQ_PTR(_terminator_7740)->length;
    }
    else {
        _4224 = 1;
    }
    _ends_at_7742 = 1 - _4224;
    _4224 = NOVALUE;

    /** 	while ends_at < 1 or not match( terminator, text, ends_at ) with entry do*/
    goto L1; // [23] 71
L2: 
    _4226 = (_ends_at_7742 < 1);
    if (_4226 != 0) {
        DeRef(_4227);
        _4227 = 1;
        goto L3; // [30] 46
    }
    _4228 = e_match_from(_terminator_7740, _text_7741, _ends_at_7742);
    _4229 = (_4228 == 0);
    _4228 = NOVALUE;
    _4227 = (_4229 != 0);
L3: 
    if (_4227 == 0)
    {
        _4227 = NOVALUE;
        goto L4; // [46] 96
    }
    else{
        _4227 = NOVALUE;
    }

    /** 		if ch = GET_EOF then*/
    if (_14ch_7687 != -1)
    goto L5; // [53] 68

    /** 			return { GET_FAIL, 0 }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4231 = MAKE_SEQ(_1);
    DeRefDSi(_terminator_7740);
    DeRefi(_text_7741);
    DeRef(_4226);
    _4226 = NOVALUE;
    DeRef(_4229);
    _4229 = NOVALUE;
    return _4231;
L5: 

    /** 	entry*/
L1: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		text &= ch*/
    Append(&_text_7741, _text_7741, _14ch_7687);

    /** 		ends_at += 1*/
    _ends_at_7742 = _ends_at_7742 + 1;

    /** 	end while*/
    goto L2; // [93] 26
L4: 

    /** 	return { GET_SUCCESS, head( text, length( text ) - length( terminator ) ) }*/
    if (IS_SEQUENCE(_text_7741)){
            _4234 = SEQ_PTR(_text_7741)->length;
    }
    else {
        _4234 = 1;
    }
    if (IS_SEQUENCE(_terminator_7740)){
            _4235 = SEQ_PTR(_terminator_7740)->length;
    }
    else {
        _4235 = 1;
    }
    _4236 = _4234 - _4235;
    _4234 = NOVALUE;
    _4235 = NOVALUE;
    {
        int len = SEQ_PTR(_text_7741)->length;
        int size = (IS_ATOM_INT(_4236)) ? _4236 : (object)(DBL_PTR(_4236)->dbl);
        if (size <= 0){
            DeRef( _4237 );
            _4237 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_text_7741);
            DeRef(_4237);
            _4237 = _text_7741;
        }
        else{
            Head(SEQ_PTR(_text_7741),size+1,&_4237);
        }
    }
    _4236 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _4237;
    _4238 = MAKE_SEQ(_1);
    _4237 = NOVALUE;
    DeRefDSi(_terminator_7740);
    DeRefDSi(_text_7741);
    DeRef(_4226);
    _4226 = NOVALUE;
    DeRef(_4229);
    _4229 = NOVALUE;
    DeRef(_4231);
    _4231 = NOVALUE;
    return _4238;
    ;
}


int _14get_string()
{
    int _text_7762 = NOVALUE;
    int _4255 = NOVALUE;
    int _4251 = NOVALUE;
    int _4250 = NOVALUE;
    int _4247 = NOVALUE;
    int _4246 = NOVALUE;
    int _4245 = NOVALUE;
    int _4244 = NOVALUE;
    int _4242 = NOVALUE;
    int _4241 = NOVALUE;
    int _4239 = NOVALUE;
    int _0, _1, _2;
    

    /** 	text = ""*/
    RefDS(_5);
    DeRefi(_text_7762);
    _text_7762 = _5;

    /** 	while TRUE do*/
L1: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch = GET_EOF or ch = '\n' then*/
    _4239 = (_14ch_7687 == -1);
    if (_4239 != 0) {
        goto L2; // [25] 40
    }
    _4241 = (_14ch_7687 == 10);
    if (_4241 == 0)
    {
        DeRef(_4241);
        _4241 = NOVALUE;
        goto L3; // [36] 53
    }
    else{
        DeRef(_4241);
        _4241 = NOVALUE;
    }
L2: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4242 = MAKE_SEQ(_1);
    DeRefi(_text_7762);
    DeRef(_4239);
    _4239 = NOVALUE;
    return _4242;
    goto L4; // [50] 164
L3: 

    /** 		elsif ch = '"' then*/
    if (_14ch_7687 != 34)
    goto L5; // [57] 121

    /** 			get_ch()*/
    _14get_ch();

    /** 			if length( text ) = 0 and ch = '"' then*/
    if (IS_SEQUENCE(_text_7762)){
            _4244 = SEQ_PTR(_text_7762)->length;
    }
    else {
        _4244 = 1;
    }
    _4245 = (_4244 == 0);
    _4244 = NOVALUE;
    if (_4245 == 0) {
        goto L6; // [74] 108
    }
    _4247 = (_14ch_7687 == 34);
    if (_4247 == 0)
    {
        DeRef(_4247);
        _4247 = NOVALUE;
        goto L6; // [85] 108
    }
    else{
        DeRef(_4247);
        _4247 = NOVALUE;
    }

    /** 				if ch = '"' then*/
    if (_14ch_7687 != 34)
    goto L7; // [92] 107

    /** 					return get_heredoc( `"""` )*/
    RefDS(_4249);
    _4250 = _14get_heredoc(_4249);
    DeRefi(_text_7762);
    DeRef(_4239);
    _4239 = NOVALUE;
    DeRef(_4242);
    _4242 = NOVALUE;
    DeRef(_4245);
    _4245 = NOVALUE;
    return _4250;
L7: 
L6: 

    /** 			return {GET_SUCCESS, text}*/
    RefDS(_text_7762);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _text_7762;
    _4251 = MAKE_SEQ(_1);
    DeRefDSi(_text_7762);
    DeRef(_4239);
    _4239 = NOVALUE;
    DeRef(_4242);
    _4242 = NOVALUE;
    DeRef(_4245);
    _4245 = NOVALUE;
    DeRef(_4250);
    _4250 = NOVALUE;
    return _4251;
    goto L4; // [118] 164
L5: 

    /** 		elsif ch = '\\' then*/
    if (_14ch_7687 != 92)
    goto L8; // [125] 163

    /** 			get_ch()*/
    _14get_ch();

    /** 			ch = escape_char(ch)*/
    _0 = _14escape_char(_14ch_7687);
    _14ch_7687 = _0;
    if (!IS_ATOM_INT(_14ch_7687)) {
        _1 = (long)(DBL_PTR(_14ch_7687)->dbl);
        if (UNIQUE(DBL_PTR(_14ch_7687)) && (DBL_PTR(_14ch_7687)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14ch_7687);
        _14ch_7687 = _1;
    }

    /** 			if ch = GET_FAIL then*/
    if (_14ch_7687 != 1)
    goto L9; // [147] 162

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4255 = MAKE_SEQ(_1);
    DeRefi(_text_7762);
    DeRef(_4239);
    _4239 = NOVALUE;
    DeRef(_4242);
    _4242 = NOVALUE;
    DeRef(_4245);
    _4245 = NOVALUE;
    DeRef(_4250);
    _4250 = NOVALUE;
    DeRef(_4251);
    _4251 = NOVALUE;
    return _4255;
L9: 
L8: 
L4: 

    /** 		text = text & ch*/
    Append(&_text_7762, _text_7762, _14ch_7687);

    /** 	end while*/
    goto L1; // [174] 13
    ;
}


int _14read_comment()
{
    int _4276 = NOVALUE;
    int _4275 = NOVALUE;
    int _4273 = NOVALUE;
    int _4271 = NOVALUE;
    int _4269 = NOVALUE;
    int _4268 = NOVALUE;
    int _4267 = NOVALUE;
    int _4265 = NOVALUE;
    int _4264 = NOVALUE;
    int _4263 = NOVALUE;
    int _4262 = NOVALUE;
    int _4261 = NOVALUE;
    int _4260 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(input_string) then*/
    _4260 = IS_ATOM(_14input_string_7685);
    if (_4260 == 0)
    {
        _4260 = NOVALUE;
        goto L1; // [8] 98
    }
    else{
        _4260 = NOVALUE;
    }

    /** 		while ch!='\n' and ch!='\r' and ch!=-1 do*/
L2: 
    _4261 = (_14ch_7687 != 10);
    if (_4261 == 0) {
        _4262 = 0;
        goto L3; // [22] 36
    }
    _4263 = (_14ch_7687 != 13);
    _4262 = (_4263 != 0);
L3: 
    if (_4262 == 0) {
        goto L4; // [36] 59
    }
    _4265 = (_14ch_7687 != -1);
    if (_4265 == 0)
    {
        DeRef(_4265);
        _4265 = NOVALUE;
        goto L4; // [47] 59
    }
    else{
        DeRef(_4265);
        _4265 = NOVALUE;
    }

    /** 			get_ch()*/
    _14get_ch();

    /** 		end while*/
    goto L2; // [56] 16
L4: 

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch=-1 then*/
    if (_14ch_7687 != -1)
    goto L5; // [67] 84

    /** 			return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _4267 = MAKE_SEQ(_1);
    DeRef(_4261);
    _4261 = NOVALUE;
    DeRef(_4263);
    _4263 = NOVALUE;
    return _4267;
    goto L6; // [81] 182
L5: 

    /** 			return {GET_IGNORE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _4268 = MAKE_SEQ(_1);
    DeRef(_4261);
    _4261 = NOVALUE;
    DeRef(_4263);
    _4263 = NOVALUE;
    DeRef(_4267);
    _4267 = NOVALUE;
    return _4268;
    goto L6; // [95] 182
L1: 

    /** 		for i=string_next to length(input_string) do*/
    if (IS_SEQUENCE(_14input_string_7685)){
            _4269 = SEQ_PTR(_14input_string_7685)->length;
    }
    else {
        _4269 = 1;
    }
    {
        int _i_7812;
        _i_7812 = _14string_next_7686;
L7: 
        if (_i_7812 > _4269){
            goto L8; // [107] 171
        }

        /** 			ch=input_string[i]*/
        _2 = (int)SEQ_PTR(_14input_string_7685);
        _14ch_7687 = (int)*(((s1_ptr)_2)->base + _i_7812);
        if (!IS_ATOM_INT(_14ch_7687)){
            _14ch_7687 = (long)DBL_PTR(_14ch_7687)->dbl;
        }

        /** 			if ch='\n' or ch='\r' then*/
        _4271 = (_14ch_7687 == 10);
        if (_4271 != 0) {
            goto L9; // [132] 147
        }
        _4273 = (_14ch_7687 == 13);
        if (_4273 == 0)
        {
            DeRef(_4273);
            _4273 = NOVALUE;
            goto LA; // [143] 164
        }
        else{
            DeRef(_4273);
            _4273 = NOVALUE;
        }
L9: 

        /** 				string_next=i+1*/
        _14string_next_7686 = _i_7812 + 1;

        /** 				return {GET_IGNORE, 0}*/
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = 0;
        _4275 = MAKE_SEQ(_1);
        DeRef(_4261);
        _4261 = NOVALUE;
        DeRef(_4263);
        _4263 = NOVALUE;
        DeRef(_4267);
        _4267 = NOVALUE;
        DeRef(_4268);
        _4268 = NOVALUE;
        DeRef(_4271);
        _4271 = NOVALUE;
        return _4275;
LA: 

        /** 		end for*/
        _i_7812 = _i_7812 + 1;
        goto L7; // [166] 114
L8: 
        ;
    }

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _4276 = MAKE_SEQ(_1);
    DeRef(_4261);
    _4261 = NOVALUE;
    DeRef(_4263);
    _4263 = NOVALUE;
    DeRef(_4267);
    _4267 = NOVALUE;
    DeRef(_4268);
    _4268 = NOVALUE;
    DeRef(_4271);
    _4271 = NOVALUE;
    DeRef(_4275);
    _4275 = NOVALUE;
    return _4276;
L6: 
    ;
}


int _14get_number()
{
    int _sign_7824 = NOVALUE;
    int _e_sign_7825 = NOVALUE;
    int _ndigits_7826 = NOVALUE;
    int _hex_digit_7827 = NOVALUE;
    int _mantissa_7828 = NOVALUE;
    int _dec_7829 = NOVALUE;
    int _e_mag_7830 = NOVALUE;
    int _4337 = NOVALUE;
    int _4335 = NOVALUE;
    int _4333 = NOVALUE;
    int _4330 = NOVALUE;
    int _4326 = NOVALUE;
    int _4324 = NOVALUE;
    int _4323 = NOVALUE;
    int _4322 = NOVALUE;
    int _4321 = NOVALUE;
    int _4320 = NOVALUE;
    int _4318 = NOVALUE;
    int _4317 = NOVALUE;
    int _4316 = NOVALUE;
    int _4313 = NOVALUE;
    int _4311 = NOVALUE;
    int _4309 = NOVALUE;
    int _4305 = NOVALUE;
    int _4304 = NOVALUE;
    int _4302 = NOVALUE;
    int _4301 = NOVALUE;
    int _4300 = NOVALUE;
    int _4297 = NOVALUE;
    int _4296 = NOVALUE;
    int _4294 = NOVALUE;
    int _4293 = NOVALUE;
    int _4292 = NOVALUE;
    int _4291 = NOVALUE;
    int _4290 = NOVALUE;
    int _4289 = NOVALUE;
    int _4286 = NOVALUE;
    int _4282 = NOVALUE;
    int _4279 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sign = +1*/
    _sign_7824 = 1;

    /** 	mantissa = 0*/
    DeRef(_mantissa_7828);
    _mantissa_7828 = 0;

    /** 	ndigits = 0*/
    _ndigits_7826 = 0;

    /** 	if ch = '-' then*/
    if (_14ch_7687 != 45)
    goto L1; // [20] 54

    /** 		sign = -1*/
    _sign_7824 = -1;

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch='-' then*/
    if (_14ch_7687 != 45)
    goto L2; // [37] 68

    /** 			return read_comment()*/
    _4279 = _14read_comment();
    DeRef(_dec_7829);
    DeRef(_e_mag_7830);
    return _4279;
    goto L2; // [51] 68
L1: 

    /** 	elsif ch = '+' then*/
    if (_14ch_7687 != 43)
    goto L3; // [58] 67

    /** 		get_ch()*/
    _14get_ch();
L3: 
L2: 

    /** 	if ch = '#' then*/
    if (_14ch_7687 != 35)
    goto L4; // [72] 172

    /** 		get_ch()*/
    _14get_ch();

    /** 		while TRUE do*/
L5: 

    /** 			hex_digit = find(ch, HEX_DIGITS)-1*/
    _4282 = find_from(_14ch_7687, _14HEX_DIGITS_7667, 1);
    _hex_digit_7827 = _4282 - 1;
    _4282 = NOVALUE;

    /** 			if hex_digit >= 0 then*/
    if (_hex_digit_7827 < 0)
    goto L6; // [104] 131

    /** 				ndigits += 1*/
    _ndigits_7826 = _ndigits_7826 + 1;

    /** 				mantissa = mantissa * 16 + hex_digit*/
    if (IS_ATOM_INT(_mantissa_7828)) {
        if (_mantissa_7828 == (short)_mantissa_7828)
        _4286 = _mantissa_7828 * 16;
        else
        _4286 = NewDouble(_mantissa_7828 * (double)16);
    }
    else {
        _4286 = NewDouble(DBL_PTR(_mantissa_7828)->dbl * (double)16);
    }
    DeRef(_mantissa_7828);
    if (IS_ATOM_INT(_4286)) {
        _mantissa_7828 = _4286 + _hex_digit_7827;
        if ((long)((unsigned long)_mantissa_7828 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_7828 = NewDouble((double)_mantissa_7828);
    }
    else {
        _mantissa_7828 = NewDouble(DBL_PTR(_4286)->dbl + (double)_hex_digit_7827);
    }
    DeRef(_4286);
    _4286 = NOVALUE;

    /** 				get_ch()*/
    _14get_ch();
    goto L5; // [128] 85
L6: 

    /** 				if ndigits > 0 then*/
    if (_ndigits_7826 <= 0)
    goto L7; // [133] 154

    /** 					return {GET_SUCCESS, sign * mantissa}*/
    if (IS_ATOM_INT(_mantissa_7828)) {
        if (_sign_7824 == (short)_sign_7824 && _mantissa_7828 <= INT15 && _mantissa_7828 >= -INT15)
        _4289 = _sign_7824 * _mantissa_7828;
        else
        _4289 = NewDouble(_sign_7824 * (double)_mantissa_7828);
    }
    else {
        _4289 = NewDouble((double)_sign_7824 * DBL_PTR(_mantissa_7828)->dbl);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _4289;
    _4290 = MAKE_SEQ(_1);
    _4289 = NOVALUE;
    DeRef(_mantissa_7828);
    DeRef(_dec_7829);
    DeRef(_e_mag_7830);
    DeRef(_4279);
    _4279 = NOVALUE;
    return _4290;
    goto L5; // [151] 85
L7: 

    /** 					return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4291 = MAKE_SEQ(_1);
    DeRef(_mantissa_7828);
    DeRef(_dec_7829);
    DeRef(_e_mag_7830);
    DeRef(_4279);
    _4279 = NOVALUE;
    DeRef(_4290);
    _4290 = NOVALUE;
    return _4291;

    /** 		end while*/
    goto L5; // [168] 85
L4: 

    /** 	while ch >= '0' and ch <= '9' do*/
L8: 
    _4292 = (_14ch_7687 >= 48);
    if (_4292 == 0) {
        goto L9; // [183] 228
    }
    _4294 = (_14ch_7687 <= 57);
    if (_4294 == 0)
    {
        DeRef(_4294);
        _4294 = NOVALUE;
        goto L9; // [194] 228
    }
    else{
        DeRef(_4294);
        _4294 = NOVALUE;
    }

    /** 		ndigits += 1*/
    _ndigits_7826 = _ndigits_7826 + 1;

    /** 		mantissa = mantissa * 10 + (ch - '0')*/
    if (IS_ATOM_INT(_mantissa_7828)) {
        if (_mantissa_7828 == (short)_mantissa_7828)
        _4296 = _mantissa_7828 * 10;
        else
        _4296 = NewDouble(_mantissa_7828 * (double)10);
    }
    else {
        _4296 = NewDouble(DBL_PTR(_mantissa_7828)->dbl * (double)10);
    }
    _4297 = _14ch_7687 - 48;
    if ((long)((unsigned long)_4297 +(unsigned long) HIGH_BITS) >= 0){
        _4297 = NewDouble((double)_4297);
    }
    DeRef(_mantissa_7828);
    if (IS_ATOM_INT(_4296) && IS_ATOM_INT(_4297)) {
        _mantissa_7828 = _4296 + _4297;
        if ((long)((unsigned long)_mantissa_7828 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_7828 = NewDouble((double)_mantissa_7828);
    }
    else {
        if (IS_ATOM_INT(_4296)) {
            _mantissa_7828 = NewDouble((double)_4296 + DBL_PTR(_4297)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4297)) {
                _mantissa_7828 = NewDouble(DBL_PTR(_4296)->dbl + (double)_4297);
            }
            else
            _mantissa_7828 = NewDouble(DBL_PTR(_4296)->dbl + DBL_PTR(_4297)->dbl);
        }
    }
    DeRef(_4296);
    _4296 = NOVALUE;
    DeRef(_4297);
    _4297 = NOVALUE;

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L8; // [225] 177
L9: 

    /** 	if ch = '.' then*/
    if (_14ch_7687 != 46)
    goto LA; // [232] 308

    /** 		get_ch()*/
    _14get_ch();

    /** 		dec = 10*/
    DeRef(_dec_7829);
    _dec_7829 = 10;

    /** 		while ch >= '0' and ch <= '9' do*/
LB: 
    _4300 = (_14ch_7687 >= 48);
    if (_4300 == 0) {
        goto LC; // [256] 307
    }
    _4302 = (_14ch_7687 <= 57);
    if (_4302 == 0)
    {
        DeRef(_4302);
        _4302 = NOVALUE;
        goto LC; // [267] 307
    }
    else{
        DeRef(_4302);
        _4302 = NOVALUE;
    }

    /** 			ndigits += 1*/
    _ndigits_7826 = _ndigits_7826 + 1;

    /** 			mantissa += (ch - '0') / dec*/
    _4304 = _14ch_7687 - 48;
    if ((long)((unsigned long)_4304 +(unsigned long) HIGH_BITS) >= 0){
        _4304 = NewDouble((double)_4304);
    }
    if (IS_ATOM_INT(_4304) && IS_ATOM_INT(_dec_7829)) {
        _4305 = (_4304 % _dec_7829) ? NewDouble((double)_4304 / _dec_7829) : (_4304 / _dec_7829);
    }
    else {
        if (IS_ATOM_INT(_4304)) {
            _4305 = NewDouble((double)_4304 / DBL_PTR(_dec_7829)->dbl);
        }
        else {
            if (IS_ATOM_INT(_dec_7829)) {
                _4305 = NewDouble(DBL_PTR(_4304)->dbl / (double)_dec_7829);
            }
            else
            _4305 = NewDouble(DBL_PTR(_4304)->dbl / DBL_PTR(_dec_7829)->dbl);
        }
    }
    DeRef(_4304);
    _4304 = NOVALUE;
    _0 = _mantissa_7828;
    if (IS_ATOM_INT(_mantissa_7828) && IS_ATOM_INT(_4305)) {
        _mantissa_7828 = _mantissa_7828 + _4305;
        if ((long)((unsigned long)_mantissa_7828 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_7828 = NewDouble((double)_mantissa_7828);
    }
    else {
        if (IS_ATOM_INT(_mantissa_7828)) {
            _mantissa_7828 = NewDouble((double)_mantissa_7828 + DBL_PTR(_4305)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4305)) {
                _mantissa_7828 = NewDouble(DBL_PTR(_mantissa_7828)->dbl + (double)_4305);
            }
            else
            _mantissa_7828 = NewDouble(DBL_PTR(_mantissa_7828)->dbl + DBL_PTR(_4305)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_4305);
    _4305 = NOVALUE;

    /** 			dec *= 10*/
    _0 = _dec_7829;
    if (IS_ATOM_INT(_dec_7829)) {
        if (_dec_7829 == (short)_dec_7829)
        _dec_7829 = _dec_7829 * 10;
        else
        _dec_7829 = NewDouble(_dec_7829 * (double)10);
    }
    else {
        _dec_7829 = NewDouble(DBL_PTR(_dec_7829)->dbl * (double)10);
    }
    DeRef(_0);

    /** 			get_ch()*/
    _14get_ch();

    /** 		end while*/
    goto LB; // [304] 250
LC: 
LA: 

    /** 	if ndigits = 0 then*/
    if (_ndigits_7826 != 0)
    goto LD; // [310] 325

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4309 = MAKE_SEQ(_1);
    DeRef(_mantissa_7828);
    DeRef(_dec_7829);
    DeRef(_e_mag_7830);
    DeRef(_4279);
    _4279 = NOVALUE;
    DeRef(_4290);
    _4290 = NOVALUE;
    DeRef(_4291);
    _4291 = NOVALUE;
    DeRef(_4292);
    _4292 = NOVALUE;
    DeRef(_4300);
    _4300 = NOVALUE;
    return _4309;
LD: 

    /** 	mantissa = sign * mantissa*/
    _0 = _mantissa_7828;
    if (IS_ATOM_INT(_mantissa_7828)) {
        if (_sign_7824 == (short)_sign_7824 && _mantissa_7828 <= INT15 && _mantissa_7828 >= -INT15)
        _mantissa_7828 = _sign_7824 * _mantissa_7828;
        else
        _mantissa_7828 = NewDouble(_sign_7824 * (double)_mantissa_7828);
    }
    else {
        _mantissa_7828 = NewDouble((double)_sign_7824 * DBL_PTR(_mantissa_7828)->dbl);
    }
    DeRef(_0);

    /** 	if ch = 'e' or ch = 'E' then*/
    _4311 = (_14ch_7687 == 101);
    if (_4311 != 0) {
        goto LE; // [339] 354
    }
    _4313 = (_14ch_7687 == 69);
    if (_4313 == 0)
    {
        DeRef(_4313);
        _4313 = NOVALUE;
        goto LF; // [350] 575
    }
    else{
        DeRef(_4313);
        _4313 = NOVALUE;
    }
LE: 

    /** 		e_sign = +1*/
    _e_sign_7825 = 1;

    /** 		e_mag = 0*/
    DeRef(_e_mag_7830);
    _e_mag_7830 = 0;

    /** 		get_ch()*/
    _14get_ch();

    /** 		if ch = '-' then*/
    if (_14ch_7687 != 45)
    goto L10; // [372] 388

    /** 			e_sign = -1*/
    _e_sign_7825 = -1;

    /** 			get_ch()*/
    _14get_ch();
    goto L11; // [385] 402
L10: 

    /** 		elsif ch = '+' then*/
    if (_14ch_7687 != 43)
    goto L12; // [392] 401

    /** 			get_ch()*/
    _14get_ch();
L12: 
L11: 

    /** 		if ch >= '0' and ch <= '9' then*/
    _4316 = (_14ch_7687 >= 48);
    if (_4316 == 0) {
        goto L13; // [410] 489
    }
    _4318 = (_14ch_7687 <= 57);
    if (_4318 == 0)
    {
        DeRef(_4318);
        _4318 = NOVALUE;
        goto L13; // [421] 489
    }
    else{
        DeRef(_4318);
        _4318 = NOVALUE;
    }

    /** 			e_mag = ch - '0'*/
    DeRef(_e_mag_7830);
    _e_mag_7830 = _14ch_7687 - 48;
    if ((long)((unsigned long)_e_mag_7830 +(unsigned long) HIGH_BITS) >= 0){
        _e_mag_7830 = NewDouble((double)_e_mag_7830);
    }

    /** 			get_ch()*/
    _14get_ch();

    /** 			while ch >= '0' and ch <= '9' do*/
L14: 
    _4320 = (_14ch_7687 >= 48);
    if (_4320 == 0) {
        goto L15; // [447] 500
    }
    _4322 = (_14ch_7687 <= 57);
    if (_4322 == 0)
    {
        DeRef(_4322);
        _4322 = NOVALUE;
        goto L15; // [458] 500
    }
    else{
        DeRef(_4322);
        _4322 = NOVALUE;
    }

    /** 				e_mag = e_mag * 10 + ch - '0'*/
    if (IS_ATOM_INT(_e_mag_7830)) {
        if (_e_mag_7830 == (short)_e_mag_7830)
        _4323 = _e_mag_7830 * 10;
        else
        _4323 = NewDouble(_e_mag_7830 * (double)10);
    }
    else {
        _4323 = NewDouble(DBL_PTR(_e_mag_7830)->dbl * (double)10);
    }
    if (IS_ATOM_INT(_4323)) {
        _4324 = _4323 + _14ch_7687;
        if ((long)((unsigned long)_4324 + (unsigned long)HIGH_BITS) >= 0) 
        _4324 = NewDouble((double)_4324);
    }
    else {
        _4324 = NewDouble(DBL_PTR(_4323)->dbl + (double)_14ch_7687);
    }
    DeRef(_4323);
    _4323 = NOVALUE;
    DeRef(_e_mag_7830);
    if (IS_ATOM_INT(_4324)) {
        _e_mag_7830 = _4324 - 48;
        if ((long)((unsigned long)_e_mag_7830 +(unsigned long) HIGH_BITS) >= 0){
            _e_mag_7830 = NewDouble((double)_e_mag_7830);
        }
    }
    else {
        _e_mag_7830 = NewDouble(DBL_PTR(_4324)->dbl - (double)48);
    }
    DeRef(_4324);
    _4324 = NOVALUE;

    /** 				get_ch()*/
    _14get_ch();

    /** 			end while*/
    goto L14; // [483] 441
    goto L15; // [486] 500
L13: 

    /** 			return {GET_FAIL, 0} -- no exponent*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4326 = MAKE_SEQ(_1);
    DeRef(_mantissa_7828);
    DeRef(_dec_7829);
    DeRef(_e_mag_7830);
    DeRef(_4279);
    _4279 = NOVALUE;
    DeRef(_4290);
    _4290 = NOVALUE;
    DeRef(_4291);
    _4291 = NOVALUE;
    DeRef(_4292);
    _4292 = NOVALUE;
    DeRef(_4300);
    _4300 = NOVALUE;
    DeRef(_4309);
    _4309 = NOVALUE;
    DeRef(_4311);
    _4311 = NOVALUE;
    DeRef(_4316);
    _4316 = NOVALUE;
    DeRef(_4320);
    _4320 = NOVALUE;
    return _4326;
L15: 

    /** 		e_mag *= e_sign*/
    _0 = _e_mag_7830;
    if (IS_ATOM_INT(_e_mag_7830)) {
        if (_e_mag_7830 == (short)_e_mag_7830 && _e_sign_7825 <= INT15 && _e_sign_7825 >= -INT15)
        _e_mag_7830 = _e_mag_7830 * _e_sign_7825;
        else
        _e_mag_7830 = NewDouble(_e_mag_7830 * (double)_e_sign_7825);
    }
    else {
        _e_mag_7830 = NewDouble(DBL_PTR(_e_mag_7830)->dbl * (double)_e_sign_7825);
    }
    DeRef(_0);

    /** 		if e_mag > 308 then*/
    if (binary_op_a(LESSEQ, _e_mag_7830, 308)){
        goto L16; // [508] 563
    }

    /** 			mantissa *= power(10, 308)*/
    _4330 = power(10, 308);
    _0 = _mantissa_7828;
    if (IS_ATOM_INT(_mantissa_7828) && IS_ATOM_INT(_4330)) {
        if (_mantissa_7828 == (short)_mantissa_7828 && _4330 <= INT15 && _4330 >= -INT15)
        _mantissa_7828 = _mantissa_7828 * _4330;
        else
        _mantissa_7828 = NewDouble(_mantissa_7828 * (double)_4330);
    }
    else {
        if (IS_ATOM_INT(_mantissa_7828)) {
            _mantissa_7828 = NewDouble((double)_mantissa_7828 * DBL_PTR(_4330)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4330)) {
                _mantissa_7828 = NewDouble(DBL_PTR(_mantissa_7828)->dbl * (double)_4330);
            }
            else
            _mantissa_7828 = NewDouble(DBL_PTR(_mantissa_7828)->dbl * DBL_PTR(_4330)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_4330);
    _4330 = NOVALUE;

    /** 			if e_mag > 1000 then*/
    if (binary_op_a(LESSEQ, _e_mag_7830, 1000)){
        goto L17; // [524] 534
    }

    /** 				e_mag = 1000*/
    DeRef(_e_mag_7830);
    _e_mag_7830 = 1000;
L17: 

    /** 			for i = 1 to e_mag - 308 do*/
    if (IS_ATOM_INT(_e_mag_7830)) {
        _4333 = _e_mag_7830 - 308;
        if ((long)((unsigned long)_4333 +(unsigned long) HIGH_BITS) >= 0){
            _4333 = NewDouble((double)_4333);
        }
    }
    else {
        _4333 = NewDouble(DBL_PTR(_e_mag_7830)->dbl - (double)308);
    }
    {
        int _i_7909;
        _i_7909 = 1;
L18: 
        if (binary_op_a(GREATER, _i_7909, _4333)){
            goto L19; // [540] 560
        }

        /** 				mantissa *= 10*/
        _0 = _mantissa_7828;
        if (IS_ATOM_INT(_mantissa_7828)) {
            if (_mantissa_7828 == (short)_mantissa_7828)
            _mantissa_7828 = _mantissa_7828 * 10;
            else
            _mantissa_7828 = NewDouble(_mantissa_7828 * (double)10);
        }
        else {
            _mantissa_7828 = NewDouble(DBL_PTR(_mantissa_7828)->dbl * (double)10);
        }
        DeRef(_0);

        /** 			end for*/
        _0 = _i_7909;
        if (IS_ATOM_INT(_i_7909)) {
            _i_7909 = _i_7909 + 1;
            if ((long)((unsigned long)_i_7909 +(unsigned long) HIGH_BITS) >= 0){
                _i_7909 = NewDouble((double)_i_7909);
            }
        }
        else {
            _i_7909 = binary_op_a(PLUS, _i_7909, 1);
        }
        DeRef(_0);
        goto L18; // [555] 547
L19: 
        ;
        DeRef(_i_7909);
    }
    goto L1A; // [560] 574
L16: 

    /** 			mantissa *= power(10, e_mag)*/
    if (IS_ATOM_INT(_e_mag_7830)) {
        _4335 = power(10, _e_mag_7830);
    }
    else {
        temp_d.dbl = (double)10;
        _4335 = Dpower(&temp_d, DBL_PTR(_e_mag_7830));
    }
    _0 = _mantissa_7828;
    if (IS_ATOM_INT(_mantissa_7828) && IS_ATOM_INT(_4335)) {
        if (_mantissa_7828 == (short)_mantissa_7828 && _4335 <= INT15 && _4335 >= -INT15)
        _mantissa_7828 = _mantissa_7828 * _4335;
        else
        _mantissa_7828 = NewDouble(_mantissa_7828 * (double)_4335);
    }
    else {
        if (IS_ATOM_INT(_mantissa_7828)) {
            _mantissa_7828 = NewDouble((double)_mantissa_7828 * DBL_PTR(_4335)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4335)) {
                _mantissa_7828 = NewDouble(DBL_PTR(_mantissa_7828)->dbl * (double)_4335);
            }
            else
            _mantissa_7828 = NewDouble(DBL_PTR(_mantissa_7828)->dbl * DBL_PTR(_4335)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_4335);
    _4335 = NOVALUE;
L1A: 
LF: 

    /** 	return {GET_SUCCESS, mantissa}*/
    Ref(_mantissa_7828);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _mantissa_7828;
    _4337 = MAKE_SEQ(_1);
    DeRef(_mantissa_7828);
    DeRef(_dec_7829);
    DeRef(_e_mag_7830);
    DeRef(_4279);
    _4279 = NOVALUE;
    DeRef(_4290);
    _4290 = NOVALUE;
    DeRef(_4291);
    _4291 = NOVALUE;
    DeRef(_4292);
    _4292 = NOVALUE;
    DeRef(_4300);
    _4300 = NOVALUE;
    DeRef(_4309);
    _4309 = NOVALUE;
    DeRef(_4311);
    _4311 = NOVALUE;
    DeRef(_4316);
    _4316 = NOVALUE;
    DeRef(_4320);
    _4320 = NOVALUE;
    DeRef(_4326);
    _4326 = NOVALUE;
    DeRef(_4333);
    _4333 = NOVALUE;
    return _4337;
    ;
}


int _14Get()
{
    int _skip_blanks_1__tmp_at330_7962 = NOVALUE;
    int _skip_blanks_1__tmp_at177_7943 = NOVALUE;
    int _skip_blanks_1__tmp_at88_7934 = NOVALUE;
    int _s_7918 = NOVALUE;
    int _e_7919 = NOVALUE;
    int _e1_7920 = NOVALUE;
    int _4376 = NOVALUE;
    int _4375 = NOVALUE;
    int _4373 = NOVALUE;
    int _4370 = NOVALUE;
    int _4368 = NOVALUE;
    int _4366 = NOVALUE;
    int _4364 = NOVALUE;
    int _4361 = NOVALUE;
    int _4359 = NOVALUE;
    int _4355 = NOVALUE;
    int _4351 = NOVALUE;
    int _4348 = NOVALUE;
    int _4347 = NOVALUE;
    int _4345 = NOVALUE;
    int _4343 = NOVALUE;
    int _4341 = NOVALUE;
    int _4340 = NOVALUE;
    int _4338 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while find(ch, white_space) do*/
L1: 
    _4338 = find_from(_14ch_7687, _14white_space_7703, 1);
    if (_4338 == 0)
    {
        _4338 = NOVALUE;
        goto L2; // [13] 25
    }
    else{
        _4338 = NOVALUE;
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L1; // [22] 6
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_14ch_7687 != -1)
    goto L3; // [29] 44

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _4340 = MAKE_SEQ(_1);
    DeRef(_s_7918);
    DeRef(_e_7919);
    return _4340;
L3: 

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _4341 = find_from(_14ch_7687, _14START_NUMERIC_7670, 1);
    if (_4341 == 0)
    {
        _4341 = NOVALUE;
        goto L5; // [60] 157
    }
    else{
        _4341 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_7919;
    _e_7919 = _14get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_7919);
    _4343 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _4343, -2)){
        _4343 = NOVALUE;
        goto L6; // [76] 87
    }
    _4343 = NOVALUE;

    /** 				return e*/
    DeRef(_s_7918);
    DeRef(_4340);
    _4340 = NOVALUE;
    return _e_7919;
L6: 

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L7: 
    _skip_blanks_1__tmp_at88_7934 = find_from(_14ch_7687, _14white_space_7703, 1);
    if (_skip_blanks_1__tmp_at88_7934 == 0)
    {
        goto L8; // [101] 118
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L7; // [110] 94

    /** end procedure*/
    goto L8; // [115] 118
L8: 

    /** 			if ch=-1 or ch='}' then -- '}' is expected only in the "{--\n}" case*/
    _4345 = (_14ch_7687 == -1);
    if (_4345 != 0) {
        goto L9; // [128] 143
    }
    _4347 = (_14ch_7687 == 125);
    if (_4347 == 0)
    {
        DeRef(_4347);
        _4347 = NOVALUE;
        goto L4; // [139] 49
    }
    else{
        DeRef(_4347);
        _4347 = NOVALUE;
    }
L9: 

    /** 				return {GET_NOTHING, 0} -- just a comment*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _4348 = MAKE_SEQ(_1);
    DeRef(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    return _4348;
    goto L4; // [154] 49
L5: 

    /** 		elsif ch = '{' then*/
    if (_14ch_7687 != 123)
    goto LA; // [161] 467

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_7918);
    _s_7918 = _5;

    /** 			get_ch()*/
    _14get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
LB: 
    _skip_blanks_1__tmp_at177_7943 = find_from(_14ch_7687, _14white_space_7703, 1);
    if (_skip_blanks_1__tmp_at177_7943 == 0)
    {
        goto LC; // [190] 207
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto LB; // [199] 183

    /** end procedure*/
    goto LC; // [204] 207
LC: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_14ch_7687 != 125)
    goto LD; // [213] 232

    /** 				get_ch()*/
    _14get_ch();

    /** 				return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_7918);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_7918;
    _4351 = MAKE_SEQ(_1);
    DeRefDS(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    return _4351;
LD: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LE: 

    /** 				while 1 do -- read zero or more comments and an element*/
LF: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_7919;
    _e_7919 = _14Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_7919);
    _e1_7920 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_7920))
    _e1_7920 = (long)DBL_PTR(_e1_7920)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_7920 != 0)
    goto L10; // [259] 280

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_7919);
    _4355 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_4355);
    Append(&_s_7918, _s_7918, _4355);
    _4355 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto L11; // [275] 324
    goto LF; // [277] 242
L10: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_7920 == -2)
    goto L12; // [282] 295

    /** 						return e*/
    DeRef(_s_7918);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    return _e_7919;
    goto LF; // [292] 242
L12: 

    /** 					elsif ch='}' then*/
    if (_14ch_7687 != 125)
    goto LF; // [299] 242

    /** 						get_ch()*/
    _14get_ch();

    /** 						return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_7918);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_7918;
    _4359 = MAKE_SEQ(_1);
    DeRefDS(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    return _4359;

    /** 				end while*/
    goto LF; // [321] 242
L11: 

    /** 				while 1 do -- now read zero or more post element comments*/
L13: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L14: 
    _skip_blanks_1__tmp_at330_7962 = find_from(_14ch_7687, _14white_space_7703, 1);
    if (_skip_blanks_1__tmp_at330_7962 == 0)
    {
        goto L15; // [343] 360
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L14; // [352] 336

    /** end procedure*/
    goto L15; // [357] 360
L15: 

    /** 					if ch = '}' then*/
    if (_14ch_7687 != 125)
    goto L16; // [366] 387

    /** 						get_ch()*/
    _14get_ch();

    /** 					return {GET_SUCCESS, s}*/
    RefDS(_s_7918);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_7918;
    _4361 = MAKE_SEQ(_1);
    DeRefDS(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    DeRef(_4359);
    _4359 = NOVALUE;
    return _4361;
    goto L13; // [384] 329
L16: 

    /** 					elsif ch!='-' then*/
    if (_14ch_7687 == 45)
    goto L17; // [391] 402

    /** 						exit*/
    goto L18; // [397] 436
    goto L13; // [399] 329
L17: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_7919;
    _e_7919 = _14get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it wasn't a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_7919);
    _4364 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _4364, -2)){
        _4364 = NOVALUE;
        goto L13; // [415] 329
    }
    _4364 = NOVALUE;

    /** 							return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4366 = MAKE_SEQ(_1);
    DeRef(_s_7918);
    DeRefDS(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    DeRef(_4359);
    _4359 = NOVALUE;
    DeRef(_4361);
    _4361 = NOVALUE;
    return _4366;

    /** 			end while*/
    goto L13; // [433] 329
L18: 

    /** 				if ch != ',' then*/
    if (_14ch_7687 == 44)
    goto L19; // [440] 455

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4368 = MAKE_SEQ(_1);
    DeRef(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    DeRef(_4359);
    _4359 = NOVALUE;
    DeRef(_4361);
    _4361 = NOVALUE;
    DeRef(_4366);
    _4366 = NOVALUE;
    return _4368;
L19: 

    /** 			get_ch() -- skip comma*/
    _14get_ch();

    /** 			end while*/
    goto LE; // [461] 237
    goto L4; // [464] 49
LA: 

    /** 		elsif ch = '\"' then*/
    if (_14ch_7687 != 34)
    goto L1A; // [471] 487

    /** 			return get_string()*/
    _4370 = _14get_string();
    DeRef(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    DeRef(_4359);
    _4359 = NOVALUE;
    DeRef(_4361);
    _4361 = NOVALUE;
    DeRef(_4366);
    _4366 = NOVALUE;
    DeRef(_4368);
    _4368 = NOVALUE;
    return _4370;
    goto L4; // [484] 49
L1A: 

    /** 		elsif ch = '`' then*/
    if (_14ch_7687 != 96)
    goto L1B; // [491] 508

    /** 			return get_heredoc("`")*/
    RefDS(_4372);
    _4373 = _14get_heredoc(_4372);
    DeRef(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    DeRef(_4359);
    _4359 = NOVALUE;
    DeRef(_4361);
    _4361 = NOVALUE;
    DeRef(_4366);
    _4366 = NOVALUE;
    DeRef(_4368);
    _4368 = NOVALUE;
    DeRef(_4370);
    _4370 = NOVALUE;
    return _4373;
    goto L4; // [505] 49
L1B: 

    /** 		elsif ch = '\'' then*/
    if (_14ch_7687 != 39)
    goto L1C; // [512] 528

    /** 			return get_qchar()*/
    _4375 = _14get_qchar();
    DeRef(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    DeRef(_4359);
    _4359 = NOVALUE;
    DeRef(_4361);
    _4361 = NOVALUE;
    DeRef(_4366);
    _4366 = NOVALUE;
    DeRef(_4368);
    _4368 = NOVALUE;
    DeRef(_4370);
    _4370 = NOVALUE;
    DeRef(_4373);
    _4373 = NOVALUE;
    return _4375;
    goto L4; // [525] 49
L1C: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _4376 = MAKE_SEQ(_1);
    DeRef(_s_7918);
    DeRef(_e_7919);
    DeRef(_4340);
    _4340 = NOVALUE;
    DeRef(_4345);
    _4345 = NOVALUE;
    DeRef(_4348);
    _4348 = NOVALUE;
    DeRef(_4351);
    _4351 = NOVALUE;
    DeRef(_4359);
    _4359 = NOVALUE;
    DeRef(_4361);
    _4361 = NOVALUE;
    DeRef(_4366);
    _4366 = NOVALUE;
    DeRef(_4368);
    _4368 = NOVALUE;
    DeRef(_4370);
    _4370 = NOVALUE;
    DeRef(_4373);
    _4373 = NOVALUE;
    DeRef(_4375);
    _4375 = NOVALUE;
    return _4376;

    /** 	end while*/
    goto L4; // [541] 49
    ;
}


int _14Get2()
{
    int _skip_blanks_1__tmp_at468_8063 = NOVALUE;
    int _skip_blanks_1__tmp_at235_8030 = NOVALUE;
    int _s_7992 = NOVALUE;
    int _e_7993 = NOVALUE;
    int _e1_7994 = NOVALUE;
    int _offset_7995 = NOVALUE;
    int _4476 = NOVALUE;
    int _4475 = NOVALUE;
    int _4474 = NOVALUE;
    int _4473 = NOVALUE;
    int _4472 = NOVALUE;
    int _4471 = NOVALUE;
    int _4470 = NOVALUE;
    int _4469 = NOVALUE;
    int _4468 = NOVALUE;
    int _4467 = NOVALUE;
    int _4466 = NOVALUE;
    int _4463 = NOVALUE;
    int _4462 = NOVALUE;
    int _4461 = NOVALUE;
    int _4460 = NOVALUE;
    int _4459 = NOVALUE;
    int _4458 = NOVALUE;
    int _4455 = NOVALUE;
    int _4454 = NOVALUE;
    int _4453 = NOVALUE;
    int _4452 = NOVALUE;
    int _4451 = NOVALUE;
    int _4450 = NOVALUE;
    int _4447 = NOVALUE;
    int _4446 = NOVALUE;
    int _4445 = NOVALUE;
    int _4444 = NOVALUE;
    int _4443 = NOVALUE;
    int _4441 = NOVALUE;
    int _4440 = NOVALUE;
    int _4439 = NOVALUE;
    int _4438 = NOVALUE;
    int _4437 = NOVALUE;
    int _4435 = NOVALUE;
    int _4432 = NOVALUE;
    int _4431 = NOVALUE;
    int _4430 = NOVALUE;
    int _4429 = NOVALUE;
    int _4428 = NOVALUE;
    int _4426 = NOVALUE;
    int _4425 = NOVALUE;
    int _4424 = NOVALUE;
    int _4423 = NOVALUE;
    int _4422 = NOVALUE;
    int _4420 = NOVALUE;
    int _4419 = NOVALUE;
    int _4418 = NOVALUE;
    int _4417 = NOVALUE;
    int _4416 = NOVALUE;
    int _4415 = NOVALUE;
    int _4412 = NOVALUE;
    int _4408 = NOVALUE;
    int _4407 = NOVALUE;
    int _4406 = NOVALUE;
    int _4405 = NOVALUE;
    int _4404 = NOVALUE;
    int _4401 = NOVALUE;
    int _4400 = NOVALUE;
    int _4399 = NOVALUE;
    int _4398 = NOVALUE;
    int _4397 = NOVALUE;
    int _4395 = NOVALUE;
    int _4394 = NOVALUE;
    int _4393 = NOVALUE;
    int _4392 = NOVALUE;
    int _4391 = NOVALUE;
    int _4390 = NOVALUE;
    int _4388 = NOVALUE;
    int _4386 = NOVALUE;
    int _4384 = NOVALUE;
    int _4383 = NOVALUE;
    int _4382 = NOVALUE;
    int _4381 = NOVALUE;
    int _4380 = NOVALUE;
    int _4378 = NOVALUE;
    int _0, _1, _2;
    

    /** 	offset = string_next-1*/
    _offset_7995 = _14string_next_7686 - 1;

    /** 	get_ch()*/
    _14get_ch();

    /** 	while find(ch, white_space) do*/
L1: 
    _4378 = find_from(_14ch_7687, _14white_space_7703, 1);
    if (_4378 == 0)
    {
        _4378 = NOVALUE;
        goto L2; // [25] 37
    }
    else{
        _4378 = NOVALUE;
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L1; // [34] 18
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_14ch_7687 != -1)
    goto L3; // [41] 75

    /** 		return {GET_EOF, 0, string_next-1-offset ,string_next-1}*/
    _4380 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4380 +(unsigned long) HIGH_BITS) >= 0){
        _4380 = NewDouble((double)_4380);
    }
    if (IS_ATOM_INT(_4380)) {
        _4381 = _4380 - _offset_7995;
        if ((long)((unsigned long)_4381 +(unsigned long) HIGH_BITS) >= 0){
            _4381 = NewDouble((double)_4381);
        }
    }
    else {
        _4381 = NewDouble(DBL_PTR(_4380)->dbl - (double)_offset_7995);
    }
    DeRef(_4380);
    _4380 = NOVALUE;
    _4382 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4382 +(unsigned long) HIGH_BITS) >= 0){
        _4382 = NewDouble((double)_4382);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _4381;
    *((int *)(_2+16)) = _4382;
    _4383 = MAKE_SEQ(_1);
    _4382 = NOVALUE;
    _4381 = NOVALUE;
    DeRef(_s_7992);
    DeRef(_e_7993);
    return _4383;
L3: 

    /** 	leading_whitespace = string_next-2-offset -- index of the last whitespace: string_next points past the first non whitespace*/
    _4384 = _14string_next_7686 - 2;
    if ((long)((unsigned long)_4384 +(unsigned long) HIGH_BITS) >= 0){
        _4384 = NewDouble((double)_4384);
    }
    if (IS_ATOM_INT(_4384)) {
        _14leading_whitespace_7989 = _4384 - _offset_7995;
    }
    else {
        _14leading_whitespace_7989 = NewDouble(DBL_PTR(_4384)->dbl - (double)_offset_7995);
    }
    DeRef(_4384);
    _4384 = NOVALUE;
    if (!IS_ATOM_INT(_14leading_whitespace_7989)) {
        _1 = (long)(DBL_PTR(_14leading_whitespace_7989)->dbl);
        if (UNIQUE(DBL_PTR(_14leading_whitespace_7989)) && (DBL_PTR(_14leading_whitespace_7989)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14leading_whitespace_7989);
        _14leading_whitespace_7989 = _1;
    }

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _4386 = find_from(_14ch_7687, _14START_NUMERIC_7670, 1);
    if (_4386 == 0)
    {
        _4386 = NOVALUE;
        goto L5; // [107] 215
    }
    else{
        _4386 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_7993;
    _e_7993 = _14get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_7993);
    _4388 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _4388, -2)){
        _4388 = NOVALUE;
        goto L6; // [123] 164
    }
    _4388 = NOVALUE;

    /** 				return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4390 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4390 +(unsigned long) HIGH_BITS) >= 0){
        _4390 = NewDouble((double)_4390);
    }
    if (IS_ATOM_INT(_4390)) {
        _4391 = _4390 - _offset_7995;
        if ((long)((unsigned long)_4391 +(unsigned long) HIGH_BITS) >= 0){
            _4391 = NewDouble((double)_4391);
        }
    }
    else {
        _4391 = NewDouble(DBL_PTR(_4390)->dbl - (double)_offset_7995);
    }
    DeRef(_4390);
    _4390 = NOVALUE;
    _4392 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4391)) {
        _4393 = _4391 - _4392;
        if ((long)((unsigned long)_4393 +(unsigned long) HIGH_BITS) >= 0){
            _4393 = NewDouble((double)_4393);
        }
    }
    else {
        _4393 = NewDouble(DBL_PTR(_4391)->dbl - (double)_4392);
    }
    DeRef(_4391);
    _4391 = NOVALUE;
    _4392 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4393;
    ((int *)_2)[2] = _14leading_whitespace_7989;
    _4394 = MAKE_SEQ(_1);
    _4393 = NOVALUE;
    Concat((object_ptr)&_4395, _e_7993, _4394);
    DeRefDS(_4394);
    _4394 = NOVALUE;
    DeRef(_s_7992);
    DeRefDS(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    return _4395;
L6: 

    /** 			get_ch()*/
    _14get_ch();

    /** 			if ch=-1 then*/
    if (_14ch_7687 != -1)
    goto L4; // [172] 96

    /** 				return {GET_NOTHING, 0, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _4397 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4397 +(unsigned long) HIGH_BITS) >= 0){
        _4397 = NewDouble((double)_4397);
    }
    if (IS_ATOM_INT(_4397)) {
        _4398 = _4397 - _offset_7995;
        if ((long)((unsigned long)_4398 +(unsigned long) HIGH_BITS) >= 0){
            _4398 = NewDouble((double)_4398);
        }
    }
    else {
        _4398 = NewDouble(DBL_PTR(_4397)->dbl - (double)_offset_7995);
    }
    DeRef(_4397);
    _4397 = NOVALUE;
    _4399 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4398)) {
        _4400 = _4398 - _4399;
        if ((long)((unsigned long)_4400 +(unsigned long) HIGH_BITS) >= 0){
            _4400 = NewDouble((double)_4400);
        }
    }
    else {
        _4400 = NewDouble(DBL_PTR(_4398)->dbl - (double)_4399);
    }
    DeRef(_4398);
    _4398 = NOVALUE;
    _4399 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _4400;
    *((int *)(_2+16)) = _14leading_whitespace_7989;
    _4401 = MAKE_SEQ(_1);
    _4400 = NOVALUE;
    DeRef(_s_7992);
    DeRef(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    return _4401;
    goto L4; // [212] 96
L5: 

    /** 		elsif ch = '{' then*/
    if (_14ch_7687 != 123)
    goto L7; // [219] 680

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_7992);
    _s_7992 = _5;

    /** 			get_ch()*/
    _14get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L8: 
    _skip_blanks_1__tmp_at235_8030 = find_from(_14ch_7687, _14white_space_7703, 1);
    if (_skip_blanks_1__tmp_at235_8030 == 0)
    {
        goto L9; // [248] 265
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L8; // [257] 241

    /** end procedure*/
    goto L9; // [262] 265
L9: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_14ch_7687 != 125)
    goto LA; // [271] 315

    /** 				get_ch()*/
    _14get_ch();

    /** 				return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _4404 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4404 +(unsigned long) HIGH_BITS) >= 0){
        _4404 = NewDouble((double)_4404);
    }
    if (IS_ATOM_INT(_4404)) {
        _4405 = _4404 - _offset_7995;
        if ((long)((unsigned long)_4405 +(unsigned long) HIGH_BITS) >= 0){
            _4405 = NewDouble((double)_4405);
        }
    }
    else {
        _4405 = NewDouble(DBL_PTR(_4404)->dbl - (double)_offset_7995);
    }
    DeRef(_4404);
    _4404 = NOVALUE;
    _4406 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4405)) {
        _4407 = _4405 - _4406;
        if ((long)((unsigned long)_4407 +(unsigned long) HIGH_BITS) >= 0){
            _4407 = NewDouble((double)_4407);
        }
    }
    else {
        _4407 = NewDouble(DBL_PTR(_4405)->dbl - (double)_4406);
    }
    DeRef(_4405);
    _4405 = NOVALUE;
    _4406 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_7992);
    *((int *)(_2+8)) = _s_7992;
    *((int *)(_2+12)) = _4407;
    *((int *)(_2+16)) = _14leading_whitespace_7989;
    _4408 = MAKE_SEQ(_1);
    _4407 = NOVALUE;
    DeRefDS(_s_7992);
    DeRef(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    return _4408;
LA: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LB: 

    /** 				while 1 do -- read zero or more comments and an element*/
LC: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_7993;
    _e_7993 = _14Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_7993);
    _e1_7994 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_7994))
    _e1_7994 = (long)DBL_PTR(_e1_7994)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_7994 != 0)
    goto LD; // [342] 363

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_7993);
    _4412 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_4412);
    Append(&_s_7992, _s_7992, _4412);
    _4412 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto LE; // [358] 462
    goto LC; // [360] 325
LD: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_7994 == -2)
    goto LF; // [365] 408

    /** 						return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4415 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4415 +(unsigned long) HIGH_BITS) >= 0){
        _4415 = NewDouble((double)_4415);
    }
    if (IS_ATOM_INT(_4415)) {
        _4416 = _4415 - _offset_7995;
        if ((long)((unsigned long)_4416 +(unsigned long) HIGH_BITS) >= 0){
            _4416 = NewDouble((double)_4416);
        }
    }
    else {
        _4416 = NewDouble(DBL_PTR(_4415)->dbl - (double)_offset_7995);
    }
    DeRef(_4415);
    _4415 = NOVALUE;
    _4417 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4416)) {
        _4418 = _4416 - _4417;
        if ((long)((unsigned long)_4418 +(unsigned long) HIGH_BITS) >= 0){
            _4418 = NewDouble((double)_4418);
        }
    }
    else {
        _4418 = NewDouble(DBL_PTR(_4416)->dbl - (double)_4417);
    }
    DeRef(_4416);
    _4416 = NOVALUE;
    _4417 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4418;
    ((int *)_2)[2] = _14leading_whitespace_7989;
    _4419 = MAKE_SEQ(_1);
    _4418 = NOVALUE;
    Concat((object_ptr)&_4420, _e_7993, _4419);
    DeRefDS(_4419);
    _4419 = NOVALUE;
    DeRef(_s_7992);
    DeRefDS(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    return _4420;
    goto LC; // [405] 325
LF: 

    /** 					elsif ch='}' then*/
    if (_14ch_7687 != 125)
    goto LC; // [412] 325

    /** 						get_ch()*/
    _14get_ch();

    /** 						return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1),leading_whitespace} -- empty sequence*/
    _4422 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4422 +(unsigned long) HIGH_BITS) >= 0){
        _4422 = NewDouble((double)_4422);
    }
    if (IS_ATOM_INT(_4422)) {
        _4423 = _4422 - _offset_7995;
        if ((long)((unsigned long)_4423 +(unsigned long) HIGH_BITS) >= 0){
            _4423 = NewDouble((double)_4423);
        }
    }
    else {
        _4423 = NewDouble(DBL_PTR(_4422)->dbl - (double)_offset_7995);
    }
    DeRef(_4422);
    _4422 = NOVALUE;
    _4424 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4423)) {
        _4425 = _4423 - _4424;
        if ((long)((unsigned long)_4425 +(unsigned long) HIGH_BITS) >= 0){
            _4425 = NewDouble((double)_4425);
        }
    }
    else {
        _4425 = NewDouble(DBL_PTR(_4423)->dbl - (double)_4424);
    }
    DeRef(_4423);
    _4423 = NOVALUE;
    _4424 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_7992);
    *((int *)(_2+8)) = _s_7992;
    *((int *)(_2+12)) = _4425;
    *((int *)(_2+16)) = _14leading_whitespace_7989;
    _4426 = MAKE_SEQ(_1);
    _4425 = NOVALUE;
    DeRefDS(_s_7992);
    DeRef(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    return _4426;

    /** 				end while*/
    goto LC; // [459] 325
LE: 

    /** 				while 1 do -- now read zero or more post element comments*/
L10: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L11: 
    _skip_blanks_1__tmp_at468_8063 = find_from(_14ch_7687, _14white_space_7703, 1);
    if (_skip_blanks_1__tmp_at468_8063 == 0)
    {
        goto L12; // [481] 498
    }
    else{
    }

    /** 		get_ch()*/
    _14get_ch();

    /** 	end while*/
    goto L11; // [490] 474

    /** end procedure*/
    goto L12; // [495] 498
L12: 

    /** 					if ch = '}' then*/
    if (_14ch_7687 != 125)
    goto L13; // [504] 550

    /** 						get_ch()*/
    _14get_ch();

    /** 					return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4428 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4428 +(unsigned long) HIGH_BITS) >= 0){
        _4428 = NewDouble((double)_4428);
    }
    if (IS_ATOM_INT(_4428)) {
        _4429 = _4428 - _offset_7995;
        if ((long)((unsigned long)_4429 +(unsigned long) HIGH_BITS) >= 0){
            _4429 = NewDouble((double)_4429);
        }
    }
    else {
        _4429 = NewDouble(DBL_PTR(_4428)->dbl - (double)_offset_7995);
    }
    DeRef(_4428);
    _4428 = NOVALUE;
    _4430 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4429)) {
        _4431 = _4429 - _4430;
        if ((long)((unsigned long)_4431 +(unsigned long) HIGH_BITS) >= 0){
            _4431 = NewDouble((double)_4431);
        }
    }
    else {
        _4431 = NewDouble(DBL_PTR(_4429)->dbl - (double)_4430);
    }
    DeRef(_4429);
    _4429 = NOVALUE;
    _4430 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_7992);
    *((int *)(_2+8)) = _s_7992;
    *((int *)(_2+12)) = _4431;
    *((int *)(_2+16)) = _14leading_whitespace_7989;
    _4432 = MAKE_SEQ(_1);
    _4431 = NOVALUE;
    DeRefDS(_s_7992);
    DeRef(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    DeRef(_4426);
    _4426 = NOVALUE;
    return _4432;
    goto L10; // [547] 467
L13: 

    /** 					elsif ch!='-' then*/
    if (_14ch_7687 == 45)
    goto L14; // [554] 565

    /** 						exit*/
    goto L15; // [560] 624
    goto L10; // [562] 467
L14: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_7993;
    _e_7993 = _14get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it was not a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_7993);
    _4435 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _4435, -2)){
        _4435 = NOVALUE;
        goto L10; // [578] 467
    }
    _4435 = NOVALUE;

    /** 							return {GET_FAIL, 0, string_next-1-offset-(ch!=-1),leading_whitespace}*/
    _4437 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4437 +(unsigned long) HIGH_BITS) >= 0){
        _4437 = NewDouble((double)_4437);
    }
    if (IS_ATOM_INT(_4437)) {
        _4438 = _4437 - _offset_7995;
        if ((long)((unsigned long)_4438 +(unsigned long) HIGH_BITS) >= 0){
            _4438 = NewDouble((double)_4438);
        }
    }
    else {
        _4438 = NewDouble(DBL_PTR(_4437)->dbl - (double)_offset_7995);
    }
    DeRef(_4437);
    _4437 = NOVALUE;
    _4439 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4438)) {
        _4440 = _4438 - _4439;
        if ((long)((unsigned long)_4440 +(unsigned long) HIGH_BITS) >= 0){
            _4440 = NewDouble((double)_4440);
        }
    }
    else {
        _4440 = NewDouble(DBL_PTR(_4438)->dbl - (double)_4439);
    }
    DeRef(_4438);
    _4438 = NOVALUE;
    _4439 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _4440;
    *((int *)(_2+16)) = _14leading_whitespace_7989;
    _4441 = MAKE_SEQ(_1);
    _4440 = NOVALUE;
    DeRef(_s_7992);
    DeRefDS(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    DeRef(_4426);
    _4426 = NOVALUE;
    DeRef(_4432);
    _4432 = NOVALUE;
    return _4441;

    /** 			end while*/
    goto L10; // [621] 467
L15: 

    /** 				if ch != ',' then*/
    if (_14ch_7687 == 44)
    goto L16; // [628] 668

    /** 				return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4443 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4443 +(unsigned long) HIGH_BITS) >= 0){
        _4443 = NewDouble((double)_4443);
    }
    if (IS_ATOM_INT(_4443)) {
        _4444 = _4443 - _offset_7995;
        if ((long)((unsigned long)_4444 +(unsigned long) HIGH_BITS) >= 0){
            _4444 = NewDouble((double)_4444);
        }
    }
    else {
        _4444 = NewDouble(DBL_PTR(_4443)->dbl - (double)_offset_7995);
    }
    DeRef(_4443);
    _4443 = NOVALUE;
    _4445 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4444)) {
        _4446 = _4444 - _4445;
        if ((long)((unsigned long)_4446 +(unsigned long) HIGH_BITS) >= 0){
            _4446 = NewDouble((double)_4446);
        }
    }
    else {
        _4446 = NewDouble(DBL_PTR(_4444)->dbl - (double)_4445);
    }
    DeRef(_4444);
    _4444 = NOVALUE;
    _4445 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _4446;
    *((int *)(_2+16)) = _14leading_whitespace_7989;
    _4447 = MAKE_SEQ(_1);
    _4446 = NOVALUE;
    DeRef(_s_7992);
    DeRef(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    DeRef(_4426);
    _4426 = NOVALUE;
    DeRef(_4432);
    _4432 = NOVALUE;
    DeRef(_4441);
    _4441 = NOVALUE;
    return _4447;
L16: 

    /** 			get_ch() -- skip comma*/
    _14get_ch();

    /** 			end while*/
    goto LB; // [674] 320
    goto L4; // [677] 96
L7: 

    /** 		elsif ch = '\"' then*/
    if (_14ch_7687 != 34)
    goto L17; // [684] 734

    /** 			e = get_string()*/
    _0 = _e_7993;
    _e_7993 = _14get_string();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4450 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4450 +(unsigned long) HIGH_BITS) >= 0){
        _4450 = NewDouble((double)_4450);
    }
    if (IS_ATOM_INT(_4450)) {
        _4451 = _4450 - _offset_7995;
        if ((long)((unsigned long)_4451 +(unsigned long) HIGH_BITS) >= 0){
            _4451 = NewDouble((double)_4451);
        }
    }
    else {
        _4451 = NewDouble(DBL_PTR(_4450)->dbl - (double)_offset_7995);
    }
    DeRef(_4450);
    _4450 = NOVALUE;
    _4452 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4451)) {
        _4453 = _4451 - _4452;
        if ((long)((unsigned long)_4453 +(unsigned long) HIGH_BITS) >= 0){
            _4453 = NewDouble((double)_4453);
        }
    }
    else {
        _4453 = NewDouble(DBL_PTR(_4451)->dbl - (double)_4452);
    }
    DeRef(_4451);
    _4451 = NOVALUE;
    _4452 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4453;
    ((int *)_2)[2] = _14leading_whitespace_7989;
    _4454 = MAKE_SEQ(_1);
    _4453 = NOVALUE;
    Concat((object_ptr)&_4455, _e_7993, _4454);
    DeRefDS(_4454);
    _4454 = NOVALUE;
    DeRef(_s_7992);
    DeRefDS(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    DeRef(_4426);
    _4426 = NOVALUE;
    DeRef(_4432);
    _4432 = NOVALUE;
    DeRef(_4441);
    _4441 = NOVALUE;
    DeRef(_4447);
    _4447 = NOVALUE;
    return _4455;
    goto L4; // [731] 96
L17: 

    /** 		elsif ch = '`' then*/
    if (_14ch_7687 != 96)
    goto L18; // [738] 789

    /** 			e = get_heredoc("`")*/
    RefDS(_4372);
    _0 = _e_7993;
    _e_7993 = _14get_heredoc(_4372);
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4458 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4458 +(unsigned long) HIGH_BITS) >= 0){
        _4458 = NewDouble((double)_4458);
    }
    if (IS_ATOM_INT(_4458)) {
        _4459 = _4458 - _offset_7995;
        if ((long)((unsigned long)_4459 +(unsigned long) HIGH_BITS) >= 0){
            _4459 = NewDouble((double)_4459);
        }
    }
    else {
        _4459 = NewDouble(DBL_PTR(_4458)->dbl - (double)_offset_7995);
    }
    DeRef(_4458);
    _4458 = NOVALUE;
    _4460 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4459)) {
        _4461 = _4459 - _4460;
        if ((long)((unsigned long)_4461 +(unsigned long) HIGH_BITS) >= 0){
            _4461 = NewDouble((double)_4461);
        }
    }
    else {
        _4461 = NewDouble(DBL_PTR(_4459)->dbl - (double)_4460);
    }
    DeRef(_4459);
    _4459 = NOVALUE;
    _4460 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4461;
    ((int *)_2)[2] = _14leading_whitespace_7989;
    _4462 = MAKE_SEQ(_1);
    _4461 = NOVALUE;
    Concat((object_ptr)&_4463, _e_7993, _4462);
    DeRefDS(_4462);
    _4462 = NOVALUE;
    DeRef(_s_7992);
    DeRefDS(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    DeRef(_4426);
    _4426 = NOVALUE;
    DeRef(_4432);
    _4432 = NOVALUE;
    DeRef(_4441);
    _4441 = NOVALUE;
    DeRef(_4447);
    _4447 = NOVALUE;
    DeRef(_4455);
    _4455 = NOVALUE;
    return _4463;
    goto L4; // [786] 96
L18: 

    /** 		elsif ch = '\'' then*/
    if (_14ch_7687 != 39)
    goto L19; // [793] 843

    /** 			e = get_qchar()*/
    _0 = _e_7993;
    _e_7993 = _14get_qchar();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4466 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4466 +(unsigned long) HIGH_BITS) >= 0){
        _4466 = NewDouble((double)_4466);
    }
    if (IS_ATOM_INT(_4466)) {
        _4467 = _4466 - _offset_7995;
        if ((long)((unsigned long)_4467 +(unsigned long) HIGH_BITS) >= 0){
            _4467 = NewDouble((double)_4467);
        }
    }
    else {
        _4467 = NewDouble(DBL_PTR(_4466)->dbl - (double)_offset_7995);
    }
    DeRef(_4466);
    _4466 = NOVALUE;
    _4468 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4467)) {
        _4469 = _4467 - _4468;
        if ((long)((unsigned long)_4469 +(unsigned long) HIGH_BITS) >= 0){
            _4469 = NewDouble((double)_4469);
        }
    }
    else {
        _4469 = NewDouble(DBL_PTR(_4467)->dbl - (double)_4468);
    }
    DeRef(_4467);
    _4467 = NOVALUE;
    _4468 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4469;
    ((int *)_2)[2] = _14leading_whitespace_7989;
    _4470 = MAKE_SEQ(_1);
    _4469 = NOVALUE;
    Concat((object_ptr)&_4471, _e_7993, _4470);
    DeRefDS(_4470);
    _4470 = NOVALUE;
    DeRef(_s_7992);
    DeRefDS(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    DeRef(_4426);
    _4426 = NOVALUE;
    DeRef(_4432);
    _4432 = NOVALUE;
    DeRef(_4441);
    _4441 = NOVALUE;
    DeRef(_4447);
    _4447 = NOVALUE;
    DeRef(_4455);
    _4455 = NOVALUE;
    DeRef(_4463);
    _4463 = NOVALUE;
    return _4471;
    goto L4; // [840] 96
L19: 

    /** 			return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _4472 = _14string_next_7686 - 1;
    if ((long)((unsigned long)_4472 +(unsigned long) HIGH_BITS) >= 0){
        _4472 = NewDouble((double)_4472);
    }
    if (IS_ATOM_INT(_4472)) {
        _4473 = _4472 - _offset_7995;
        if ((long)((unsigned long)_4473 +(unsigned long) HIGH_BITS) >= 0){
            _4473 = NewDouble((double)_4473);
        }
    }
    else {
        _4473 = NewDouble(DBL_PTR(_4472)->dbl - (double)_offset_7995);
    }
    DeRef(_4472);
    _4472 = NOVALUE;
    _4474 = (_14ch_7687 != -1);
    if (IS_ATOM_INT(_4473)) {
        _4475 = _4473 - _4474;
        if ((long)((unsigned long)_4475 +(unsigned long) HIGH_BITS) >= 0){
            _4475 = NewDouble((double)_4475);
        }
    }
    else {
        _4475 = NewDouble(DBL_PTR(_4473)->dbl - (double)_4474);
    }
    DeRef(_4473);
    _4473 = NOVALUE;
    _4474 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _4475;
    *((int *)(_2+16)) = _14leading_whitespace_7989;
    _4476 = MAKE_SEQ(_1);
    _4475 = NOVALUE;
    DeRef(_s_7992);
    DeRef(_e_7993);
    DeRef(_4383);
    _4383 = NOVALUE;
    DeRef(_4395);
    _4395 = NOVALUE;
    DeRef(_4401);
    _4401 = NOVALUE;
    DeRef(_4408);
    _4408 = NOVALUE;
    DeRef(_4420);
    _4420 = NOVALUE;
    DeRef(_4426);
    _4426 = NOVALUE;
    DeRef(_4432);
    _4432 = NOVALUE;
    DeRef(_4441);
    _4441 = NOVALUE;
    DeRef(_4447);
    _4447 = NOVALUE;
    DeRef(_4455);
    _4455 = NOVALUE;
    DeRef(_4463);
    _4463 = NOVALUE;
    DeRef(_4471);
    _4471 = NOVALUE;
    return _4476;

    /** 	end while*/
    goto L4; // [881] 96
    ;
}


int _14get_value(int _target_8131, int _start_point_8132, int _answer_type_8133)
{
    int _msg_inlined_crash_at_39_8144 = NOVALUE;
    int _data_inlined_crash_at_36_8143 = NOVALUE;
    int _where_inlined_where_at_80_8150 = NOVALUE;
    int _seek_1__tmp_at94_8155 = NOVALUE;
    int _seek_inlined_seek_at_94_8154 = NOVALUE;
    int _pos_inlined_seek_at_91_8153 = NOVALUE;
    int _msg_inlined_crash_at_112_8158 = NOVALUE;
    int _4492 = NOVALUE;
    int _4489 = NOVALUE;
    int _4488 = NOVALUE;
    int _4487 = NOVALUE;
    int _4483 = NOVALUE;
    int _4482 = NOVALUE;
    int _4481 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if answer_type != GET_SHORT_ANSWER and answer_type != GET_LONG_ANSWER then*/
    _4481 = (_answer_type_8133 != _14GET_SHORT_ANSWER_8123);
    if (_4481 == 0) {
        goto L1; // [17] 59
    }
    _4483 = (_answer_type_8133 != _14GET_LONG_ANSWER_8126);
    if (_4483 == 0)
    {
        DeRef(_4483);
        _4483 = NOVALUE;
        goto L1; // [28] 59
    }
    else{
        DeRef(_4483);
        _4483 = NOVALUE;
    }

    /** 		error:crash("Invalid type of answer, please only use %s (the default) or %s.", {"GET_SHORT_ANSWER", "GET_LONG_ANSWER"})*/
    RefDS(_4486);
    RefDS(_4485);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4485;
    ((int *)_2)[2] = _4486;
    _4487 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_36_8143);
    _data_inlined_crash_at_36_8143 = _4487;
    _4487 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_39_8144);
    _msg_inlined_crash_at_39_8144 = EPrintf(-9999999, _4484, _data_inlined_crash_at_36_8143);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_39_8144);

    /** end procedure*/
    goto L2; // [53] 56
L2: 
    DeRef(_data_inlined_crash_at_36_8143);
    _data_inlined_crash_at_36_8143 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_39_8144);
    _msg_inlined_crash_at_39_8144 = NOVALUE;
L1: 

    /** 	if atom(target) then -- get()*/
    _4488 = IS_ATOM(_target_8131);
    if (_4488 == 0)
    {
        _4488 = NOVALUE;
        goto L3; // [64] 146
    }
    else{
        _4488 = NOVALUE;
    }

    /** 		input_file = target*/
    Ref(_target_8131);
    _14input_file_7684 = _target_8131;
    if (!IS_ATOM_INT(_14input_file_7684)) {
        _1 = (long)(DBL_PTR(_14input_file_7684)->dbl);
        if (UNIQUE(DBL_PTR(_14input_file_7684)) && (DBL_PTR(_14input_file_7684)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_14input_file_7684);
        _14input_file_7684 = _1;
    }

    /** 		if start_point then*/
    if (_start_point_8132 == 0)
    {
        goto L4; // [76] 133
    }
    else{
    }

    /** 			if io:seek(target, io:where(target)+start_point) then*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_80_8150);
    _where_inlined_where_at_80_8150 = machine(20, _target_8131);
    if (IS_ATOM_INT(_where_inlined_where_at_80_8150)) {
        _4489 = _where_inlined_where_at_80_8150 + _start_point_8132;
        if ((long)((unsigned long)_4489 + (unsigned long)HIGH_BITS) >= 0) 
        _4489 = NewDouble((double)_4489);
    }
    else {
        _4489 = NewDouble(DBL_PTR(_where_inlined_where_at_80_8150)->dbl + (double)_start_point_8132);
    }
    DeRef(_pos_inlined_seek_at_91_8153);
    _pos_inlined_seek_at_91_8153 = _4489;
    _4489 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_91_8153);
    Ref(_target_8131);
    DeRef(_seek_1__tmp_at94_8155);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _target_8131;
    ((int *)_2)[2] = _pos_inlined_seek_at_91_8153;
    _seek_1__tmp_at94_8155 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_94_8154 = machine(19, _seek_1__tmp_at94_8155);
    DeRef(_pos_inlined_seek_at_91_8153);
    _pos_inlined_seek_at_91_8153 = NOVALUE;
    DeRef(_seek_1__tmp_at94_8155);
    _seek_1__tmp_at94_8155 = NOVALUE;
    if (_seek_inlined_seek_at_94_8154 == 0)
    {
        goto L5; // [108] 132
    }
    else{
    }

    /** 				error:crash("Initial seek() for get() failed!")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_112_8158);
    _msg_inlined_crash_at_112_8158 = EPrintf(-9999999, _4490, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_112_8158);

    /** end procedure*/
    goto L6; // [126] 129
L6: 
    DeRefi(_msg_inlined_crash_at_112_8158);
    _msg_inlined_crash_at_112_8158 = NOVALUE;
L5: 
L4: 

    /** 		string_next = 1*/
    _14string_next_7686 = 1;

    /** 		input_string = 0*/
    DeRef(_14input_string_7685);
    _14input_string_7685 = 0;
    goto L7; // [143] 157
L3: 

    /** 		input_string = target*/
    Ref(_target_8131);
    DeRef(_14input_string_7685);
    _14input_string_7685 = _target_8131;

    /** 		string_next = start_point*/
    _14string_next_7686 = _start_point_8132;
L7: 

    /** 	if answer_type = GET_SHORT_ANSWER then*/
    if (_answer_type_8133 != _14GET_SHORT_ANSWER_8123)
    goto L8; // [161] 170

    /** 		get_ch()*/
    _14get_ch();
L8: 

    /** 	return call_func(answer_type, {})*/
    _0 = (int)_00[_answer_type_8133].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_4492);
    _4492 = _1;
    DeRef(_target_8131);
    DeRef(_4481);
    _4481 = NOVALUE;
    return _4492;
    ;
}


int _14get(int _file_8165, int _offset_8166, int _answer_8167)
{
    int _4493 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_file_8165)) {
        _1 = (long)(DBL_PTR(_file_8165)->dbl);
        if (UNIQUE(DBL_PTR(_file_8165)) && (DBL_PTR(_file_8165)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_8165);
        _file_8165 = _1;
    }
    if (!IS_ATOM_INT(_offset_8166)) {
        _1 = (long)(DBL_PTR(_offset_8166)->dbl);
        if (UNIQUE(DBL_PTR(_offset_8166)) && (DBL_PTR(_offset_8166)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_8166);
        _offset_8166 = _1;
    }
    if (!IS_ATOM_INT(_answer_8167)) {
        _1 = (long)(DBL_PTR(_answer_8167)->dbl);
        if (UNIQUE(DBL_PTR(_answer_8167)) && (DBL_PTR(_answer_8167)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_answer_8167);
        _answer_8167 = _1;
    }

    /** 	return get_value(file, offset, answer)*/
    _4493 = _14get_value(_file_8165, _offset_8166, _answer_8167);
    return _4493;
    ;
}


int _14value(int _st_8171, int _start_point_8172, int _answer_8173)
{
    int _4494 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_point_8172)) {
        _1 = (long)(DBL_PTR(_start_point_8172)->dbl);
        if (UNIQUE(DBL_PTR(_start_point_8172)) && (DBL_PTR(_start_point_8172)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_point_8172);
        _start_point_8172 = _1;
    }
    if (!IS_ATOM_INT(_answer_8173)) {
        _1 = (long)(DBL_PTR(_answer_8173)->dbl);
        if (UNIQUE(DBL_PTR(_answer_8173)) && (DBL_PTR(_answer_8173)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_answer_8173);
        _answer_8173 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    RefDS(_st_8171);
    _4494 = _14get_value(_st_8171, _start_point_8172, _answer_8173);
    DeRefDS(_st_8171);
    return _4494;
    ;
}


int _14defaulted_value(int _st_8177, int _def_8178, int _start_point_8179)
{
    int _result_8182 = NOVALUE;
    int _4499 = NOVALUE;
    int _4497 = NOVALUE;
    int _4495 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_point_8179)) {
        _1 = (long)(DBL_PTR(_start_point_8179)->dbl);
        if (UNIQUE(DBL_PTR(_start_point_8179)) && (DBL_PTR(_start_point_8179)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_point_8179);
        _start_point_8179 = _1;
    }

    /** 	if atom(st) then*/
    _4495 = IS_ATOM(_st_8177);
    if (_4495 == 0)
    {
        _4495 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _4495 = NOVALUE;
    }

    /** 		return def*/
    DeRef(_st_8177);
    DeRef(_result_8182);
    return _def_8178;
L1: 

    /** 	object result = get_value(st,start_point, GET_SHORT_ANSWER)*/
    Ref(_st_8177);
    _0 = _result_8182;
    _result_8182 = _14get_value(_st_8177, _start_point_8179, _14GET_SHORT_ANSWER_8123);
    DeRef(_0);

    /** 	if result[1] = GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_result_8182);
    _4497 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _4497, 0)){
        _4497 = NOVALUE;
        goto L2; // [36] 51
    }
    _4497 = NOVALUE;

    /** 		return result[2]*/
    _2 = (int)SEQ_PTR(_result_8182);
    _4499 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_4499);
    DeRef(_st_8177);
    DeRef(_def_8178);
    DeRef(_result_8182);
    return _4499;
L2: 

    /** 	return def*/
    DeRef(_st_8177);
    DeRef(_result_8182);
    _4499 = NOVALUE;
    return _def_8178;
    ;
}



// 0xB6AED231
