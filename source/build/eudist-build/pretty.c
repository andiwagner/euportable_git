// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _24pretty_out(int _text_7556)
{
    int _4154 = NOVALUE;
    int _4152 = NOVALUE;
    int _4150 = NOVALUE;
    int _4149 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pretty_line &= text*/
    if (IS_SEQUENCE(_24pretty_line_7553) && IS_ATOM(_text_7556)) {
        Ref(_text_7556);
        Append(&_24pretty_line_7553, _24pretty_line_7553, _text_7556);
    }
    else if (IS_ATOM(_24pretty_line_7553) && IS_SEQUENCE(_text_7556)) {
    }
    else {
        Concat((object_ptr)&_24pretty_line_7553, _24pretty_line_7553, _text_7556);
    }

    /** 	if equal(text, '\n') and pretty_printing then*/
    if (_text_7556 == 10)
    _4149 = 1;
    else if (IS_ATOM_INT(_text_7556) && IS_ATOM_INT(10))
    _4149 = 0;
    else
    _4149 = (compare(_text_7556, 10) == 0);
    if (_4149 == 0) {
        goto L1; // [15] 52
    }
    if (_24pretty_printing_7550 == 0)
    {
        goto L1; // [22] 52
    }
    else{
    }

    /** 		puts(pretty_file, pretty_line)*/
    EPuts(1, _24pretty_line_7553); // DJP 

    /** 		pretty_line = ""*/
    RefDS(_5);
    DeRefDS(_24pretty_line_7553);
    _24pretty_line_7553 = _5;

    /** 		pretty_line_count += 1*/
    _24pretty_line_count_7546 = _24pretty_line_count_7546 + 1;
L1: 

    /** 	if atom(text) then*/
    _4152 = IS_ATOM(_text_7556);
    if (_4152 == 0)
    {
        _4152 = NOVALUE;
        goto L2; // [57] 73
    }
    else{
        _4152 = NOVALUE;
    }

    /** 		pretty_chars += 1*/
    _24pretty_chars_7538 = _24pretty_chars_7538 + 1;
    goto L3; // [70] 87
L2: 

    /** 		pretty_chars += length(text)*/
    if (IS_SEQUENCE(_text_7556)){
            _4154 = SEQ_PTR(_text_7556)->length;
    }
    else {
        _4154 = 1;
    }
    _24pretty_chars_7538 = _24pretty_chars_7538 + _4154;
    _4154 = NOVALUE;
L3: 

    /** end procedure*/
    DeRef(_text_7556);
    return;
    ;
}


void _24cut_line(int _n_7570)
{
    int _4157 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not pretty_line_breaks then	*/
    if (_24pretty_line_breaks_7549 != 0)
    goto L1; // [9] 25

    /** 		pretty_chars = 0*/
    _24pretty_chars_7538 = 0;

    /** 		return*/
    return;
L1: 

    /** 	if pretty_chars + n > pretty_end_col then*/
    _4157 = _24pretty_chars_7538 + _n_7570;
    if ((long)((unsigned long)_4157 + (unsigned long)HIGH_BITS) >= 0) 
    _4157 = NewDouble((double)_4157);
    if (binary_op_a(LESSEQ, _4157, _24pretty_end_col_7537)){
        DeRef(_4157);
        _4157 = NOVALUE;
        goto L2; // [35] 52
    }
    DeRef(_4157);
    _4157 = NOVALUE;

    /** 		pretty_out('\n')*/
    _24pretty_out(10);

    /** 		pretty_chars = 0*/
    _24pretty_chars_7538 = 0;
L2: 

    /** end procedure*/
    return;
    ;
}


void _24indent()
{
    int _4165 = NOVALUE;
    int _4164 = NOVALUE;
    int _4163 = NOVALUE;
    int _4162 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if pretty_line_breaks = 0 then	*/
    if (_24pretty_line_breaks_7549 != 0)
    goto L1; // [5] 24

    /** 		pretty_chars = 0*/
    _24pretty_chars_7538 = 0;

    /** 		return*/
    return;
    goto L2; // [21] 89
L1: 

    /** 	elsif pretty_line_breaks = -1 then*/
    if (_24pretty_line_breaks_7549 != -1)
    goto L3; // [28] 40

    /** 		cut_line( 0 )*/
    _24cut_line(0);
    goto L2; // [37] 89
L3: 

    /** 		if pretty_chars > 0 then*/
    if (_24pretty_chars_7538 <= 0)
    goto L4; // [44] 61

    /** 			pretty_out('\n')*/
    _24pretty_out(10);

    /** 			pretty_chars = 0*/
    _24pretty_chars_7538 = 0;
L4: 

    /** 		pretty_out(repeat(' ', (pretty_start_col-1) + */
    _4162 = _24pretty_start_col_7539 - 1;
    if ((long)((unsigned long)_4162 +(unsigned long) HIGH_BITS) >= 0){
        _4162 = NewDouble((double)_4162);
    }
    if (_24pretty_level_7540 == (short)_24pretty_level_7540 && _24pretty_indent_7543 <= INT15 && _24pretty_indent_7543 >= -INT15)
    _4163 = _24pretty_level_7540 * _24pretty_indent_7543;
    else
    _4163 = NewDouble(_24pretty_level_7540 * (double)_24pretty_indent_7543);
    if (IS_ATOM_INT(_4162) && IS_ATOM_INT(_4163)) {
        _4164 = _4162 + _4163;
    }
    else {
        if (IS_ATOM_INT(_4162)) {
            _4164 = NewDouble((double)_4162 + DBL_PTR(_4163)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4163)) {
                _4164 = NewDouble(DBL_PTR(_4162)->dbl + (double)_4163);
            }
            else
            _4164 = NewDouble(DBL_PTR(_4162)->dbl + DBL_PTR(_4163)->dbl);
        }
    }
    DeRef(_4162);
    _4162 = NOVALUE;
    DeRef(_4163);
    _4163 = NOVALUE;
    _4165 = Repeat(32, _4164);
    DeRef(_4164);
    _4164 = NOVALUE;
    _24pretty_out(_4165);
    _4165 = NOVALUE;
L2: 

    /** end procedure*/
    return;
    ;
}


int _24esc_char(int _a_7591)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_7591)) {
        _1 = (long)(DBL_PTR(_a_7591)->dbl);
        if (UNIQUE(DBL_PTR(_a_7591)) && (DBL_PTR(_a_7591)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_7591);
        _a_7591 = _1;
    }

    /** 	switch a do*/
    _0 = _a_7591;
    switch ( _0 ){ 

        /** 		case'\t' then*/
        case 9:

        /** 			return `\t`*/
        RefDS(_4168);
        return _4168;
        goto L1; // [22] 83

        /** 		case'\n' then*/
        case 10:

        /** 			return `\n`*/
        RefDS(_4169);
        return _4169;
        goto L1; // [34] 83

        /** 		case'\r' then*/
        case 13:

        /** 			return `\r`*/
        RefDS(_4170);
        return _4170;
        goto L1; // [46] 83

        /** 		case'\\' then*/
        case 92:

        /** 			return `\\`*/
        RefDS(_944);
        return _944;
        goto L1; // [58] 83

        /** 		case'"' then*/
        case 34:

        /** 			return `\"`*/
        RefDS(_4171);
        return _4171;
        goto L1; // [70] 83

        /** 		case else*/
        default:

        /** 			return a*/
        return _a_7591;
    ;}L1: 
    ;
}


void _24rPrint(int _a_7606)
{
    int _sbuff_7607 = NOVALUE;
    int _multi_line_7608 = NOVALUE;
    int _all_ascii_7609 = NOVALUE;
    int _4228 = NOVALUE;
    int _4227 = NOVALUE;
    int _4226 = NOVALUE;
    int _4225 = NOVALUE;
    int _4221 = NOVALUE;
    int _4220 = NOVALUE;
    int _4219 = NOVALUE;
    int _4218 = NOVALUE;
    int _4216 = NOVALUE;
    int _4215 = NOVALUE;
    int _4213 = NOVALUE;
    int _4212 = NOVALUE;
    int _4210 = NOVALUE;
    int _4209 = NOVALUE;
    int _4208 = NOVALUE;
    int _4207 = NOVALUE;
    int _4206 = NOVALUE;
    int _4205 = NOVALUE;
    int _4204 = NOVALUE;
    int _4203 = NOVALUE;
    int _4202 = NOVALUE;
    int _4201 = NOVALUE;
    int _4200 = NOVALUE;
    int _4199 = NOVALUE;
    int _4198 = NOVALUE;
    int _4197 = NOVALUE;
    int _4196 = NOVALUE;
    int _4195 = NOVALUE;
    int _4194 = NOVALUE;
    int _4190 = NOVALUE;
    int _4189 = NOVALUE;
    int _4188 = NOVALUE;
    int _4187 = NOVALUE;
    int _4186 = NOVALUE;
    int _4185 = NOVALUE;
    int _4183 = NOVALUE;
    int _4182 = NOVALUE;
    int _4178 = NOVALUE;
    int _4177 = NOVALUE;
    int _4176 = NOVALUE;
    int _4173 = NOVALUE;
    int _4172 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _4172 = IS_ATOM(_a_7606);
    if (_4172 == 0)
    {
        _4172 = NOVALUE;
        goto L1; // [6] 176
    }
    else{
        _4172 = NOVALUE;
    }

    /** 		if integer(a) then*/
    if (IS_ATOM_INT(_a_7606))
    _4173 = 1;
    else if (IS_ATOM_DBL(_a_7606))
    _4173 = IS_ATOM_INT(DoubleToInt(_a_7606));
    else
    _4173 = 0;
    if (_4173 == 0)
    {
        _4173 = NOVALUE;
        goto L2; // [14] 157
    }
    else{
        _4173 = NOVALUE;
    }

    /** 			sbuff = sprintf(pretty_int_format, a)*/
    DeRef(_sbuff_7607);
    _sbuff_7607 = EPrintf(-9999999, _24pretty_int_format_7552, _a_7606);

    /** 			if pretty_ascii then */
    if (_24pretty_ascii_7542 == 0)
    {
        goto L3; // [29] 166
    }
    else{
    }

    /** 				if pretty_ascii >= 3 then */
    if (_24pretty_ascii_7542 < 3)
    goto L4; // [36] 103

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) then*/
    if (IS_ATOM_INT(_a_7606)) {
        _4176 = (_a_7606 >= _24pretty_ascii_min_7544);
    }
    else {
        _4176 = binary_op(GREATEREQ, _a_7606, _24pretty_ascii_min_7544);
    }
    if (IS_ATOM_INT(_4176)) {
        if (_4176 == 0) {
            _4177 = 0;
            goto L5; // [48] 62
        }
    }
    else {
        if (DBL_PTR(_4176)->dbl == 0.0) {
            _4177 = 0;
            goto L5; // [48] 62
        }
    }
    if (IS_ATOM_INT(_a_7606)) {
        _4178 = (_a_7606 <= _24pretty_ascii_max_7545);
    }
    else {
        _4178 = binary_op(LESSEQ, _a_7606, _24pretty_ascii_max_7545);
    }
    DeRef(_4177);
    if (IS_ATOM_INT(_4178))
    _4177 = (_4178 != 0);
    else
    _4177 = DBL_PTR(_4178)->dbl != 0.0;
L5: 
    if (_4177 == 0)
    {
        _4177 = NOVALUE;
        goto L6; // [62] 76
    }
    else{
        _4177 = NOVALUE;
    }

    /** 						sbuff = '\'' & a & '\''  -- display char only*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_7606;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_7607, concat_list, 3);
    }
    goto L3; // [73] 166
L6: 

    /** 					elsif find(a, "\t\n\r\\") then*/
    _4182 = find_from(_a_7606, _4181, 1);
    if (_4182 == 0)
    {
        _4182 = NOVALUE;
        goto L3; // [83] 166
    }
    else{
        _4182 = NOVALUE;
    }

    /** 						sbuff = '\'' & esc_char(a) & '\''  -- display char only*/
    Ref(_a_7606);
    _4183 = _24esc_char(_a_7606);
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _4183;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_7607, concat_list, 3);
    }
    DeRef(_4183);
    _4183 = NOVALUE;
    goto L3; // [100] 166
L4: 

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) and pretty_ascii < 2 then*/
    if (IS_ATOM_INT(_a_7606)) {
        _4185 = (_a_7606 >= _24pretty_ascii_min_7544);
    }
    else {
        _4185 = binary_op(GREATEREQ, _a_7606, _24pretty_ascii_min_7544);
    }
    if (IS_ATOM_INT(_4185)) {
        if (_4185 == 0) {
            DeRef(_4186);
            _4186 = 0;
            goto L7; // [111] 125
        }
    }
    else {
        if (DBL_PTR(_4185)->dbl == 0.0) {
            DeRef(_4186);
            _4186 = 0;
            goto L7; // [111] 125
        }
    }
    if (IS_ATOM_INT(_a_7606)) {
        _4187 = (_a_7606 <= _24pretty_ascii_max_7545);
    }
    else {
        _4187 = binary_op(LESSEQ, _a_7606, _24pretty_ascii_max_7545);
    }
    DeRef(_4186);
    if (IS_ATOM_INT(_4187))
    _4186 = (_4187 != 0);
    else
    _4186 = DBL_PTR(_4187)->dbl != 0.0;
L7: 
    if (_4186 == 0) {
        goto L3; // [125] 166
    }
    _4189 = (_24pretty_ascii_7542 < 2);
    if (_4189 == 0)
    {
        DeRef(_4189);
        _4189 = NOVALUE;
        goto L3; // [136] 166
    }
    else{
        DeRef(_4189);
        _4189 = NOVALUE;
    }

    /** 						sbuff &= '\'' & a & '\'' -- add to numeric display*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_7606;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_4190, concat_list, 3);
    }
    Concat((object_ptr)&_sbuff_7607, _sbuff_7607, _4190);
    DeRefDS(_4190);
    _4190 = NOVALUE;
    goto L3; // [154] 166
L2: 

    /** 			sbuff = sprintf(pretty_fp_format, a)*/
    DeRef(_sbuff_7607);
    _sbuff_7607 = EPrintf(-9999999, _24pretty_fp_format_7551, _a_7606);
L3: 

    /** 		pretty_out(sbuff)*/
    RefDS(_sbuff_7607);
    _24pretty_out(_sbuff_7607);
    goto L8; // [173] 551
L1: 

    /** 		cut_line(1)*/
    _24cut_line(1);

    /** 		multi_line = 0*/
    _multi_line_7608 = 0;

    /** 		all_ascii = pretty_ascii > 1*/
    _all_ascii_7609 = (_24pretty_ascii_7542 > 1);

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_7606)){
            _4194 = SEQ_PTR(_a_7606)->length;
    }
    else {
        _4194 = 1;
    }
    {
        int _i_7643;
        _i_7643 = 1;
L9: 
        if (_i_7643 > _4194){
            goto LA; // [203] 355
        }

        /** 			if sequence(a[i]) and length(a[i]) > 0 then*/
        _2 = (int)SEQ_PTR(_a_7606);
        _4195 = (int)*(((s1_ptr)_2)->base + _i_7643);
        _4196 = IS_SEQUENCE(_4195);
        _4195 = NOVALUE;
        if (_4196 == 0) {
            goto LB; // [219] 257
        }
        _2 = (int)SEQ_PTR(_a_7606);
        _4198 = (int)*(((s1_ptr)_2)->base + _i_7643);
        if (IS_SEQUENCE(_4198)){
                _4199 = SEQ_PTR(_4198)->length;
        }
        else {
            _4199 = 1;
        }
        _4198 = NOVALUE;
        _4200 = (_4199 > 0);
        _4199 = NOVALUE;
        if (_4200 == 0)
        {
            DeRef(_4200);
            _4200 = NOVALUE;
            goto LB; // [235] 257
        }
        else{
            DeRef(_4200);
            _4200 = NOVALUE;
        }

        /** 				multi_line = 1*/
        _multi_line_7608 = 1;

        /** 				all_ascii = 0*/
        _all_ascii_7609 = 0;

        /** 				exit*/
        goto LA; // [254] 355
LB: 

        /** 			if not integer(a[i]) or*/
        _2 = (int)SEQ_PTR(_a_7606);
        _4201 = (int)*(((s1_ptr)_2)->base + _i_7643);
        if (IS_ATOM_INT(_4201))
        _4202 = 1;
        else if (IS_ATOM_DBL(_4201))
        _4202 = IS_ATOM_INT(DoubleToInt(_4201));
        else
        _4202 = 0;
        _4201 = NOVALUE;
        _4203 = (_4202 == 0);
        _4202 = NOVALUE;
        if (_4203 != 0) {
            _4204 = 1;
            goto LC; // [269] 321
        }
        _2 = (int)SEQ_PTR(_a_7606);
        _4205 = (int)*(((s1_ptr)_2)->base + _i_7643);
        if (IS_ATOM_INT(_4205)) {
            _4206 = (_4205 < _24pretty_ascii_min_7544);
        }
        else {
            _4206 = binary_op(LESS, _4205, _24pretty_ascii_min_7544);
        }
        _4205 = NOVALUE;
        if (IS_ATOM_INT(_4206)) {
            if (_4206 == 0) {
                DeRef(_4207);
                _4207 = 0;
                goto LD; // [283] 317
            }
        }
        else {
            if (DBL_PTR(_4206)->dbl == 0.0) {
                DeRef(_4207);
                _4207 = 0;
                goto LD; // [283] 317
            }
        }
        _4208 = (_24pretty_ascii_7542 < 2);
        if (_4208 != 0) {
            _4209 = 1;
            goto LE; // [293] 313
        }
        _2 = (int)SEQ_PTR(_a_7606);
        _4210 = (int)*(((s1_ptr)_2)->base + _i_7643);
        _4212 = find_from(_4210, _4211, 1);
        _4210 = NOVALUE;
        _4213 = (_4212 == 0);
        _4212 = NOVALUE;
        _4209 = (_4213 != 0);
LE: 
        DeRef(_4207);
        _4207 = (_4209 != 0);
LD: 
        _4204 = (_4207 != 0);
LC: 
        if (_4204 != 0) {
            goto LF; // [321] 340
        }
        _2 = (int)SEQ_PTR(_a_7606);
        _4215 = (int)*(((s1_ptr)_2)->base + _i_7643);
        if (IS_ATOM_INT(_4215)) {
            _4216 = (_4215 > _24pretty_ascii_max_7545);
        }
        else {
            _4216 = binary_op(GREATER, _4215, _24pretty_ascii_max_7545);
        }
        _4215 = NOVALUE;
        if (_4216 == 0) {
            DeRef(_4216);
            _4216 = NOVALUE;
            goto L10; // [336] 348
        }
        else {
            if (!IS_ATOM_INT(_4216) && DBL_PTR(_4216)->dbl == 0.0){
                DeRef(_4216);
                _4216 = NOVALUE;
                goto L10; // [336] 348
            }
            DeRef(_4216);
            _4216 = NOVALUE;
        }
        DeRef(_4216);
        _4216 = NOVALUE;
LF: 

        /** 				all_ascii = 0*/
        _all_ascii_7609 = 0;
L10: 

        /** 		end for*/
        _i_7643 = _i_7643 + 1;
        goto L9; // [350] 210
LA: 
        ;
    }

    /** 		if all_ascii then*/
    if (_all_ascii_7609 == 0)
    {
        goto L11; // [357] 368
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _24pretty_out(34);
    goto L12; // [365] 374
L11: 

    /** 			pretty_out('{')*/
    _24pretty_out(123);
L12: 

    /** 		pretty_level += 1*/
    _24pretty_level_7540 = _24pretty_level_7540 + 1;

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_7606)){
            _4218 = SEQ_PTR(_a_7606)->length;
    }
    else {
        _4218 = 1;
    }
    {
        int _i_7673;
        _i_7673 = 1;
L13: 
        if (_i_7673 > _4218){
            goto L14; // [389] 511
        }

        /** 			if multi_line then*/
        if (_multi_line_7608 == 0)
        {
            goto L15; // [398] 406
        }
        else{
        }

        /** 				indent()*/
        _24indent();
L15: 

        /** 			if all_ascii then*/
        if (_all_ascii_7609 == 0)
        {
            goto L16; // [408] 427
        }
        else{
        }

        /** 				pretty_out(esc_char(a[i]))*/
        _2 = (int)SEQ_PTR(_a_7606);
        _4219 = (int)*(((s1_ptr)_2)->base + _i_7673);
        Ref(_4219);
        _4220 = _24esc_char(_4219);
        _4219 = NOVALUE;
        _24pretty_out(_4220);
        _4220 = NOVALUE;
        goto L17; // [424] 437
L16: 

        /** 				rPrint(a[i])*/
        _2 = (int)SEQ_PTR(_a_7606);
        _4221 = (int)*(((s1_ptr)_2)->base + _i_7673);
        Ref(_4221);
        _24rPrint(_4221);
        _4221 = NOVALUE;
L17: 

        /** 			if pretty_line_count >= pretty_line_max then*/
        if (_24pretty_line_count_7546 < _24pretty_line_max_7547)
        goto L18; // [443] 473

        /** 				if not pretty_dots then*/
        if (_24pretty_dots_7548 != 0)
        goto L19; // [451] 460

        /** 					pretty_out(" ...")*/
        RefDS(_4224);
        _24pretty_out(_4224);
L19: 

        /** 				pretty_dots = 1*/
        _24pretty_dots_7548 = 1;

        /** 				return*/
        DeRef(_a_7606);
        DeRef(_sbuff_7607);
        DeRef(_4176);
        _4176 = NOVALUE;
        DeRef(_4178);
        _4178 = NOVALUE;
        DeRef(_4185);
        _4185 = NOVALUE;
        DeRef(_4187);
        _4187 = NOVALUE;
        _4198 = NOVALUE;
        DeRef(_4203);
        _4203 = NOVALUE;
        DeRef(_4208);
        _4208 = NOVALUE;
        DeRef(_4206);
        _4206 = NOVALUE;
        DeRef(_4213);
        _4213 = NOVALUE;
        return;
L18: 

        /** 			if i != length(a) and not all_ascii then*/
        if (IS_SEQUENCE(_a_7606)){
                _4225 = SEQ_PTR(_a_7606)->length;
        }
        else {
            _4225 = 1;
        }
        _4226 = (_i_7673 != _4225);
        _4225 = NOVALUE;
        if (_4226 == 0) {
            goto L1A; // [482] 504
        }
        _4228 = (_all_ascii_7609 == 0);
        if (_4228 == 0)
        {
            DeRef(_4228);
            _4228 = NOVALUE;
            goto L1A; // [490] 504
        }
        else{
            DeRef(_4228);
            _4228 = NOVALUE;
        }

        /** 				pretty_out(',')*/
        _24pretty_out(44);

        /** 				cut_line(6)*/
        _24cut_line(6);
L1A: 

        /** 		end for*/
        _i_7673 = _i_7673 + 1;
        goto L13; // [506] 396
L14: 
        ;
    }

    /** 		pretty_level -= 1*/
    _24pretty_level_7540 = _24pretty_level_7540 - 1;

    /** 		if multi_line then*/
    if (_multi_line_7608 == 0)
    {
        goto L1B; // [523] 531
    }
    else{
    }

    /** 			indent()*/
    _24indent();
L1B: 

    /** 		if all_ascii then*/
    if (_all_ascii_7609 == 0)
    {
        goto L1C; // [533] 544
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _24pretty_out(34);
    goto L1D; // [541] 550
L1C: 

    /** 			pretty_out('}')*/
    _24pretty_out(125);
L1D: 
L8: 

    /** end procedure*/
    DeRef(_a_7606);
    DeRef(_sbuff_7607);
    DeRef(_4176);
    _4176 = NOVALUE;
    DeRef(_4178);
    _4178 = NOVALUE;
    DeRef(_4185);
    _4185 = NOVALUE;
    DeRef(_4187);
    _4187 = NOVALUE;
    _4198 = NOVALUE;
    DeRef(_4203);
    _4203 = NOVALUE;
    DeRef(_4208);
    _4208 = NOVALUE;
    DeRef(_4206);
    _4206 = NOVALUE;
    DeRef(_4213);
    _4213 = NOVALUE;
    DeRef(_4226);
    _4226 = NOVALUE;
    return;
    ;
}


void _24pretty(int _x_7721, int _options_7722)
{
    int _4249 = NOVALUE;
    int _4248 = NOVALUE;
    int _4247 = NOVALUE;
    int _4246 = NOVALUE;
    int _4244 = NOVALUE;
    int _4243 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(options) < length( PRETTY_DEFAULT ) then*/
    if (IS_SEQUENCE(_options_7722)){
            _4243 = SEQ_PTR(_options_7722)->length;
    }
    else {
        _4243 = 1;
    }
    _4244 = 10;
    if (_4243 >= 10)
    goto L1; // [13] 41

    /** 		options &= PRETTY_DEFAULT[length(options)+1..$]*/
    if (IS_SEQUENCE(_options_7722)){
            _4246 = SEQ_PTR(_options_7722)->length;
    }
    else {
        _4246 = 1;
    }
    _4247 = _4246 + 1;
    _4246 = NOVALUE;
    _4248 = 10;
    rhs_slice_target = (object_ptr)&_4249;
    RHS_Slice(_24PRETTY_DEFAULT_7698, _4247, 10);
    Concat((object_ptr)&_options_7722, _options_7722, _4249);
    DeRefDS(_4249);
    _4249 = NOVALUE;
L1: 

    /** 	pretty_ascii = options[DISPLAY_ASCII] */
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_ascii_7542 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_24pretty_ascii_7542))
    _24pretty_ascii_7542 = (long)DBL_PTR(_24pretty_ascii_7542)->dbl;

    /** 	pretty_indent = options[INDENT]*/
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_indent_7543 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_24pretty_indent_7543))
    _24pretty_indent_7543 = (long)DBL_PTR(_24pretty_indent_7543)->dbl;

    /** 	pretty_start_col = options[START_COLUMN]*/
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_start_col_7539 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_24pretty_start_col_7539))
    _24pretty_start_col_7539 = (long)DBL_PTR(_24pretty_start_col_7539)->dbl;

    /** 	pretty_end_col = options[WRAP]*/
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_end_col_7537 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_24pretty_end_col_7537))
    _24pretty_end_col_7537 = (long)DBL_PTR(_24pretty_end_col_7537)->dbl;

    /** 	pretty_int_format = options[INT_FORMAT]*/
    DeRef(_24pretty_int_format_7552);
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_int_format_7552 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_24pretty_int_format_7552);

    /** 	pretty_fp_format = options[FP_FORMAT]*/
    DeRef(_24pretty_fp_format_7551);
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_fp_format_7551 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24pretty_fp_format_7551);

    /** 	pretty_ascii_min = options[MIN_ASCII]*/
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_ascii_min_7544 = (int)*(((s1_ptr)_2)->base + 7);
    if (!IS_ATOM_INT(_24pretty_ascii_min_7544))
    _24pretty_ascii_min_7544 = (long)DBL_PTR(_24pretty_ascii_min_7544)->dbl;

    /** 	pretty_ascii_max = options[MAX_ASCII]*/
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_ascii_max_7545 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_24pretty_ascii_max_7545))
    _24pretty_ascii_max_7545 = (long)DBL_PTR(_24pretty_ascii_max_7545)->dbl;

    /** 	pretty_line_max = options[MAX_LINES]*/
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_line_max_7547 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_24pretty_line_max_7547))
    _24pretty_line_max_7547 = (long)DBL_PTR(_24pretty_line_max_7547)->dbl;

    /** 	pretty_line_breaks = options[LINE_BREAKS]*/
    _2 = (int)SEQ_PTR(_options_7722);
    _24pretty_line_breaks_7549 = (int)*(((s1_ptr)_2)->base + 10);
    if (!IS_ATOM_INT(_24pretty_line_breaks_7549))
    _24pretty_line_breaks_7549 = (long)DBL_PTR(_24pretty_line_breaks_7549)->dbl;

    /** 	pretty_chars = pretty_start_col*/
    _24pretty_chars_7538 = _24pretty_start_col_7539;

    /** 	pretty_level = 0 */
    _24pretty_level_7540 = 0;

    /** 	pretty_line = ""*/
    RefDS(_5);
    DeRef(_24pretty_line_7553);
    _24pretty_line_7553 = _5;

    /** 	pretty_line_count = 0*/
    _24pretty_line_count_7546 = 0;

    /** 	pretty_dots = 0*/
    _24pretty_dots_7548 = 0;

    /** 	rPrint(x)*/
    Ref(_x_7721);
    _24rPrint(_x_7721);

    /** end procedure*/
    DeRef(_x_7721);
    DeRefDS(_options_7722);
    DeRef(_4247);
    _4247 = NOVALUE;
    return;
    ;
}


void _24pretty_print(int _fn_7744, int _x_7745, int _options_7746)
{
    int _0, _1, _2;
    

    /** 	pretty_printing = 1*/
    _24pretty_printing_7550 = 1;

    /** 	pretty_file = fn*/
    _24pretty_file_7541 = 1;

    /** 	pretty( x, options )*/
    Ref(_x_7745);
    RefDS(_options_7746);
    _24pretty(_x_7745, _options_7746);

    /** 	puts(pretty_file, pretty_line)*/
    EPuts(1, _24pretty_line_7553); // DJP 

    /** end procedure*/
    DeRef(_x_7745);
    DeRefDS(_options_7746);
    return;
    ;
}



// 0x06137A3A
