// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _18abs(int _a_3280)
{
    int _t_3281 = NOVALUE;
    int _1652 = NOVALUE;
    int _1651 = NOVALUE;
    int _1650 = NOVALUE;
    int _1648 = NOVALUE;
    int _1646 = NOVALUE;
    int _1645 = NOVALUE;
    int _1643 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _1643 = IS_ATOM(_a_3280);
    if (_1643 == 0)
    {
        _1643 = NOVALUE;
        goto L1; // [6] 35
    }
    else{
        _1643 = NOVALUE;
    }

    /** 		if a >= 0 then*/
    if (binary_op_a(LESS, _a_3280, 0)){
        goto L2; // [11] 24
    }

    /** 			return a*/
    DeRef(_t_3281);
    return _a_3280;
    goto L3; // [21] 34
L2: 

    /** 			return - a*/
    if (IS_ATOM_INT(_a_3280)) {
        if ((unsigned long)_a_3280 == 0xC0000000)
        _1645 = (int)NewDouble((double)-0xC0000000);
        else
        _1645 = - _a_3280;
    }
    else {
        _1645 = unary_op(UMINUS, _a_3280);
    }
    DeRef(_a_3280);
    DeRef(_t_3281);
    return _1645;
L3: 
L1: 

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_3280)){
            _1646 = SEQ_PTR(_a_3280)->length;
    }
    else {
        _1646 = 1;
    }
    {
        int _i_3289;
        _i_3289 = 1;
L4: 
        if (_i_3289 > _1646){
            goto L5; // [40] 101
        }

        /** 		t = a[i]*/
        DeRef(_t_3281);
        _2 = (int)SEQ_PTR(_a_3280);
        _t_3281 = (int)*(((s1_ptr)_2)->base + _i_3289);
        Ref(_t_3281);

        /** 		if atom(t) then*/
        _1648 = IS_ATOM(_t_3281);
        if (_1648 == 0)
        {
            _1648 = NOVALUE;
            goto L6; // [58] 80
        }
        else{
            _1648 = NOVALUE;
        }

        /** 			if t < 0 then*/
        if (binary_op_a(GREATEREQ, _t_3281, 0)){
            goto L7; // [63] 94
        }

        /** 				a[i] = - t*/
        if (IS_ATOM_INT(_t_3281)) {
            if ((unsigned long)_t_3281 == 0xC0000000)
            _1650 = (int)NewDouble((double)-0xC0000000);
            else
            _1650 = - _t_3281;
        }
        else {
            _1650 = unary_op(UMINUS, _t_3281);
        }
        _2 = (int)SEQ_PTR(_a_3280);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_3280 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_3289);
        _1 = *(int *)_2;
        *(int *)_2 = _1650;
        if( _1 != _1650 ){
            DeRef(_1);
        }
        _1650 = NOVALUE;
        goto L7; // [77] 94
L6: 

        /** 			a[i] = abs(t)*/
        Ref(_t_3281);
        DeRef(_1651);
        _1651 = _t_3281;
        _1652 = _18abs(_1651);
        _1651 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_3280);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_3280 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_3289);
        _1 = *(int *)_2;
        *(int *)_2 = _1652;
        if( _1 != _1652 ){
            DeRef(_1);
        }
        _1652 = NOVALUE;
L7: 

        /** 	end for*/
        _i_3289 = _i_3289 + 1;
        goto L4; // [96] 47
L5: 
        ;
    }

    /** 	return a*/
    DeRef(_t_3281);
    DeRef(_1645);
    _1645 = NOVALUE;
    return _a_3280;
    ;
}


int _18max(int _a_3324)
{
    int _b_3325 = NOVALUE;
    int _c_3326 = NOVALUE;
    int _1662 = NOVALUE;
    int _1661 = NOVALUE;
    int _1660 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _1660 = IS_ATOM(_a_3324);
    if (_1660 == 0)
    {
        _1660 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _1660 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_3325);
    DeRef(_c_3326);
    return _a_3324;
L1: 

    /** 	b = mathcons:MINF*/
    RefDS(_20MINF_3258);
    DeRef(_b_3325);
    _b_3325 = _20MINF_3258;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_3324)){
            _1661 = SEQ_PTR(_a_3324)->length;
    }
    else {
        _1661 = 1;
    }
    {
        int _i_3330;
        _i_3330 = 1;
L2: 
        if (_i_3330 > _1661){
            goto L3; // [28] 64
        }

        /** 		c = max(a[i])*/
        _2 = (int)SEQ_PTR(_a_3324);
        _1662 = (int)*(((s1_ptr)_2)->base + _i_3330);
        Ref(_1662);
        _0 = _c_3326;
        _c_3326 = _18max(_1662);
        DeRef(_0);
        _1662 = NOVALUE;

        /** 		if c > b then*/
        if (binary_op_a(LESSEQ, _c_3326, _b_3325)){
            goto L4; // [47] 57
        }

        /** 			b = c*/
        Ref(_c_3326);
        DeRef(_b_3325);
        _b_3325 = _c_3326;
L4: 

        /** 	end for*/
        _i_3330 = _i_3330 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_3324);
    DeRef(_c_3326);
    return _b_3325;
    ;
}


int _18min(int _a_3338)
{
    int _b_3339 = NOVALUE;
    int _c_3340 = NOVALUE;
    int _1667 = NOVALUE;
    int _1666 = NOVALUE;
    int _1665 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _1665 = IS_ATOM(_a_3338);
    if (_1665 == 0)
    {
        _1665 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _1665 = NOVALUE;
    }

    /** 			return a*/
    DeRef(_b_3339);
    DeRef(_c_3340);
    return _a_3338;
L1: 

    /** 	b = mathcons:PINF*/
    RefDS(_20PINF_3254);
    DeRef(_b_3339);
    _b_3339 = _20PINF_3254;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_3338)){
            _1666 = SEQ_PTR(_a_3338)->length;
    }
    else {
        _1666 = 1;
    }
    {
        int _i_3344;
        _i_3344 = 1;
L2: 
        if (_i_3344 > _1666){
            goto L3; // [28] 64
        }

        /** 		c = min(a[i])*/
        _2 = (int)SEQ_PTR(_a_3338);
        _1667 = (int)*(((s1_ptr)_2)->base + _i_3344);
        Ref(_1667);
        _0 = _c_3340;
        _c_3340 = _18min(_1667);
        DeRef(_0);
        _1667 = NOVALUE;

        /** 			if c < b then*/
        if (binary_op_a(GREATEREQ, _c_3340, _b_3339)){
            goto L4; // [47] 57
        }

        /** 				b = c*/
        Ref(_c_3340);
        DeRef(_b_3339);
        _b_3339 = _c_3340;
L4: 

        /** 	end for*/
        _i_3344 = _i_3344 + 1;
        goto L2; // [59] 35
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_3338);
    DeRef(_c_3340);
    return _b_3339;
    ;
}


int _18or_all(int _a_3718)
{
    int _b_3719 = NOVALUE;
    int _1867 = NOVALUE;
    int _1866 = NOVALUE;
    int _1864 = NOVALUE;
    int _1863 = NOVALUE;
    int _1862 = NOVALUE;
    int _1861 = NOVALUE;
    int _1860 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _1860 = IS_ATOM(_a_3718);
    if (_1860 == 0)
    {
        _1860 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _1860 = NOVALUE;
    }

    /** 		return a*/
    DeRef(_b_3719);
    return _a_3718;
L1: 

    /** 	b = 0*/
    DeRef(_b_3719);
    _b_3719 = 0;

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_3718)){
            _1861 = SEQ_PTR(_a_3718)->length;
    }
    else {
        _1861 = 1;
    }
    {
        int _i_3723;
        _i_3723 = 1;
L2: 
        if (_i_3723 > _1861){
            goto L3; // [26] 80
        }

        /** 		if atom(a[i]) then*/
        _2 = (int)SEQ_PTR(_a_3718);
        _1862 = (int)*(((s1_ptr)_2)->base + _i_3723);
        _1863 = IS_ATOM(_1862);
        _1862 = NOVALUE;
        if (_1863 == 0)
        {
            _1863 = NOVALUE;
            goto L4; // [42] 58
        }
        else{
            _1863 = NOVALUE;
        }

        /** 			b = or_bits(b, a[i])*/
        _2 = (int)SEQ_PTR(_a_3718);
        _1864 = (int)*(((s1_ptr)_2)->base + _i_3723);
        _0 = _b_3719;
        if (IS_ATOM_INT(_b_3719) && IS_ATOM_INT(_1864)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_3719 | (unsigned long)_1864;
                 _b_3719 = MAKE_UINT(tu);
            }
        }
        else {
            _b_3719 = binary_op(OR_BITS, _b_3719, _1864);
        }
        DeRef(_0);
        _1864 = NOVALUE;
        goto L5; // [55] 73
L4: 

        /** 			b = or_bits(b, or_all(a[i]))*/
        _2 = (int)SEQ_PTR(_a_3718);
        _1866 = (int)*(((s1_ptr)_2)->base + _i_3723);
        Ref(_1866);
        _1867 = _18or_all(_1866);
        _1866 = NOVALUE;
        _0 = _b_3719;
        if (IS_ATOM_INT(_b_3719) && IS_ATOM_INT(_1867)) {
            {unsigned long tu;
                 tu = (unsigned long)_b_3719 | (unsigned long)_1867;
                 _b_3719 = MAKE_UINT(tu);
            }
        }
        else {
            _b_3719 = binary_op(OR_BITS, _b_3719, _1867);
        }
        DeRef(_0);
        DeRef(_1867);
        _1867 = NOVALUE;
L5: 

        /** 	end for*/
        _i_3723 = _i_3723 + 1;
        goto L2; // [75] 33
L3: 
        ;
    }

    /** 	return b*/
    DeRef(_a_3718);
    return _b_3719;
    ;
}



// 0x440C078B
