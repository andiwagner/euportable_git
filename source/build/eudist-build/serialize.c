// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _25init_float80()
{
    int _code_7806 = NOVALUE;
    int _4302 = NOVALUE;
    int _4301 = NOVALUE;
    int _4299 = NOVALUE;
    int _0, _1, _2;
    

    /** 	f80 = allocate( 10 )*/
    _0 = _7allocate(10, 0);
    DeRef(_25f80_7795);
    _25f80_7795 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _25f64_7796, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _7allocate(8, 0);
    DeRef(_25f64_7796);
    _25f64_7796 = _0;
L1: 

    /** 	atom code*/

    /** 	code = machine:allocate_code(*/
    _1 = NewS1(25);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 12;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 85;
    *((int *)(_2+48)) = 8;
    *((int *)(_2+52)) = 219;
    *((int *)(_2+56)) = 42;
    *((int *)(_2+60)) = 221;
    *((int *)(_2+64)) = 93;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 221;
    *((int *)(_2+76)) = 69;
    *((int *)(_2+80)) = 248;
    *((int *)(_2+84)) = 221;
    *((int *)(_2+88)) = 24;
    *((int *)(_2+92)) = 201;
    *((int *)(_2+96)) = 195;
    *((int *)(_2+100)) = 0;
    _4299 = MAKE_SEQ(_1);
    _0 = _code_7806;
    _code_7806 = _7allocate_code(_4299, 1);
    DeRef(_0);
    _4299 = NOVALUE;

    /** 	F80_TO_ATOM = dll:define_c_proc( "", {'+', code}, { dll:C_POINTER, dll:C_POINTER } )*/
    Ref(_code_7806);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_7806;
    _4301 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _4302 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _10define_c_proc(_5, _4301, _4302);
    _25F80_TO_ATOM_7797 = _0;
    _4301 = NOVALUE;
    _4302 = NOVALUE;
    if (!IS_ATOM_INT(_25F80_TO_ATOM_7797)) {
        _1 = (long)(DBL_PTR(_25F80_TO_ATOM_7797)->dbl);
        if (UNIQUE(DBL_PTR(_25F80_TO_ATOM_7797)) && (DBL_PTR(_25F80_TO_ATOM_7797)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_25F80_TO_ATOM_7797);
        _25F80_TO_ATOM_7797 = _1;
    }

    /** end procedure*/
    DeRef(_code_7806);
    return;
    ;
}


int _25float80_to_atom(int _f_7827)
{
    int _4308 = NOVALUE;
    int _4307 = NOVALUE;
    int _4306 = NOVALUE;
    int _4305 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not f80 then*/
    if (IS_ATOM_INT(_25f80_7795)) {
        if (_25f80_7795 != 0){
            goto L1; // [7] 15
        }
    }
    else {
        if (DBL_PTR(_25f80_7795)->dbl != 0.0){
            goto L1; // [7] 15
        }
    }

    /** 		init_float80()*/
    _25init_float80();
L1: 

    /** 	poke( f80, f )*/
    if (IS_ATOM_INT(_25f80_7795)){
        poke_addr = (unsigned char *)_25f80_7795;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25f80_7795)->dbl);
    }
    _1 = (int)SEQ_PTR(_f_7827);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( F80_TO_ATOM, { f80, f64 } )*/
    Ref(_25f64_7796);
    Ref(_25f80_7795);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25f80_7795;
    ((int *)_2)[2] = _25f64_7796;
    _4305 = MAKE_SEQ(_1);
    call_c(0, _25F80_TO_ATOM_7797, _4305);
    DeRefDS(_4305);
    _4305 = NOVALUE;

    /** 	return float64_to_atom( peek( { f64, 8 } ) )*/
    Ref(_25f64_7796);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25f64_7796;
    ((int *)_2)[2] = 8;
    _4306 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_4306);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4307 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4306);
    _4306 = NOVALUE;
    _4308 = _13float64_to_atom(_4307);
    _4307 = NOVALUE;
    DeRefDS(_f_7827);
    return _4308;
    ;
}


void _25init_peek8s()
{
    int _code_7845 = NOVALUE;
    int _4316 = NOVALUE;
    int _4315 = NOVALUE;
    int _4313 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i64 = allocate( 8 )*/
    _0 = _7allocate(8, 0);
    DeRef(_25i64_7835);
    _25i64_7835 = _0;

    /** 	if f64 = 0 then*/
    if (binary_op_a(NOTEQ, _25f64_7796, 0)){
        goto L1; // [12] 24
    }

    /** 		f64 = allocate( 8 )*/
    _0 = _7allocate(8, 0);
    DeRef(_25f64_7796);
    _25f64_7796 = _0;
L1: 

    /** 	atom code = machine:allocate_code(	{*/
    _1 = NewS1(30);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 85;
    *((int *)(_2+8)) = 137;
    *((int *)(_2+12)) = 229;
    *((int *)(_2+16)) = 131;
    *((int *)(_2+20)) = 236;
    *((int *)(_2+24)) = 8;
    *((int *)(_2+28)) = 139;
    *((int *)(_2+32)) = 69;
    *((int *)(_2+36)) = 8;
    *((int *)(_2+40)) = 139;
    *((int *)(_2+44)) = 80;
    *((int *)(_2+48)) = 4;
    *((int *)(_2+52)) = 139;
    *((int *)(_2+56)) = 0;
    *((int *)(_2+60)) = 137;
    *((int *)(_2+64)) = 69;
    *((int *)(_2+68)) = 248;
    *((int *)(_2+72)) = 137;
    *((int *)(_2+76)) = 85;
    *((int *)(_2+80)) = 252;
    *((int *)(_2+84)) = 223;
    *((int *)(_2+88)) = 109;
    *((int *)(_2+92)) = 248;
    *((int *)(_2+96)) = 139;
    *((int *)(_2+100)) = 69;
    *((int *)(_2+104)) = 12;
    *((int *)(_2+108)) = 221;
    *((int *)(_2+112)) = 24;
    *((int *)(_2+116)) = 201;
    *((int *)(_2+120)) = 195;
    _4313 = MAKE_SEQ(_1);
    _0 = _code_7845;
    _code_7845 = _7allocate_code(_4313, 1);
    DeRef(_0);
    _4313 = NOVALUE;

    /** 	PEEK8S = define_c_proc( {}, {'+', code}, { C_POINTER, C_POINTER } )*/
    Ref(_code_7845);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 43;
    ((int *)_2)[2] = _code_7845;
    _4315 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 33554436;
    ((int *)_2)[2] = 33554436;
    _4316 = MAKE_SEQ(_1);
    RefDS(_5);
    _0 = _10define_c_proc(_5, _4315, _4316);
    _25PEEK8S_7836 = _0;
    _4315 = NOVALUE;
    _4316 = NOVALUE;
    if (!IS_ATOM_INT(_25PEEK8S_7836)) {
        _1 = (long)(DBL_PTR(_25PEEK8S_7836)->dbl);
        if (UNIQUE(DBL_PTR(_25PEEK8S_7836)) && (DBL_PTR(_25PEEK8S_7836)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_25PEEK8S_7836);
        _25PEEK8S_7836 = _1;
    }

    /** end procedure*/
    DeRef(_code_7845);
    return;
    ;
}


int _25int64_to_atom(int _s_7857)
{
    int _4322 = NOVALUE;
    int _4321 = NOVALUE;
    int _4320 = NOVALUE;
    int _4319 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if i64 = 0 then*/
    if (binary_op_a(NOTEQ, _25i64_7835, 0)){
        goto L1; // [7] 16
    }

    /** 		init_peek8s()*/
    _25init_peek8s();
L1: 

    /** 	poke( i64, s )*/
    if (IS_ATOM_INT(_25i64_7835)){
        poke_addr = (unsigned char *)_25i64_7835;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25i64_7835)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_7857);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	c_proc( PEEK8S, { i64, f64 } )*/
    Ref(_25f64_7796);
    Ref(_25i64_7835);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _25i64_7835;
    ((int *)_2)[2] = _25f64_7796;
    _4319 = MAKE_SEQ(_1);
    call_c(0, _25PEEK8S_7836, _4319);
    DeRefDS(_4319);
    _4319 = NOVALUE;

    /** 	return float64_to_atom( peek( f64 & 8 ) )*/
    Concat((object_ptr)&_4320, _25f64_7796, 8);
    _1 = (int)SEQ_PTR(_4320);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _4321 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4320);
    _4320 = NOVALUE;
    _4322 = _13float64_to_atom(_4321);
    _4321 = NOVALUE;
    DeRefDS(_s_7857);
    return _4322;
    ;
}


int _25get4(int _fh_7867)
{
    int _4327 = NOVALUE;
    int _4326 = NOVALUE;
    int _4325 = NOVALUE;
    int _4324 = NOVALUE;
    int _4323 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(fh))*/
    if (_fh_7867 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7867, EF_READ);
        last_r_file_no = _fh_7867;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4323 = getKBchar();
        }
        else
        _4323 = getc(last_r_file_ptr);
    }
    else
    _4323 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_25mem0_7787)){
        poke_addr = (unsigned char *)_25mem0_7787;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem0_7787)->dbl);
    }
    *poke_addr = (unsigned char)_4323;
    _4323 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_7867 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7867, EF_READ);
        last_r_file_no = _fh_7867;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4324 = getKBchar();
        }
        else
        _4324 = getc(last_r_file_ptr);
    }
    else
    _4324 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_25mem1_7788)){
        poke_addr = (unsigned char *)_25mem1_7788;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem1_7788)->dbl);
    }
    *poke_addr = (unsigned char)_4324;
    _4324 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_7867 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7867, EF_READ);
        last_r_file_no = _fh_7867;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4325 = getKBchar();
        }
        else
        _4325 = getc(last_r_file_ptr);
    }
    else
    _4325 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_25mem2_7789)){
        poke_addr = (unsigned char *)_25mem2_7789;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem2_7789)->dbl);
    }
    *poke_addr = (unsigned char)_4325;
    _4325 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_7867 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7867, EF_READ);
        last_r_file_no = _fh_7867;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4326 = getKBchar();
        }
        else
        _4326 = getc(last_r_file_ptr);
    }
    else
    _4326 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_25mem3_7790)){
        poke_addr = (unsigned char *)_25mem3_7790;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem3_7790)->dbl);
    }
    *poke_addr = (unsigned char)_4326;
    _4326 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_25mem0_7787)) {
        _4327 = *(unsigned long *)_25mem0_7787;
        if ((unsigned)_4327 > (unsigned)MAXINT)
        _4327 = NewDouble((double)(unsigned long)_4327);
    }
    else {
        _4327 = *(unsigned long *)(unsigned long)(DBL_PTR(_25mem0_7787)->dbl);
        if ((unsigned)_4327 > (unsigned)MAXINT)
        _4327 = NewDouble((double)(unsigned long)_4327);
    }
    return _4327;
    ;
}


int _25deserialize_file(int _fh_7875, int _c_7876)
{
    int _s_7877 = NOVALUE;
    int _len_7878 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_165_7914 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_162_7913 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_224_7927 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_221_7926 = NOVALUE;
    int _msg_inlined_crash_at_430_7972 = NOVALUE;
    int _4406 = NOVALUE;
    int _4405 = NOVALUE;
    int _4404 = NOVALUE;
    int _4403 = NOVALUE;
    int _4400 = NOVALUE;
    int _4397 = NOVALUE;
    int _4396 = NOVALUE;
    int _4395 = NOVALUE;
    int _4394 = NOVALUE;
    int _4393 = NOVALUE;
    int _4392 = NOVALUE;
    int _4391 = NOVALUE;
    int _4390 = NOVALUE;
    int _4389 = NOVALUE;
    int _4388 = NOVALUE;
    int _4387 = NOVALUE;
    int _4386 = NOVALUE;
    int _4384 = NOVALUE;
    int _4383 = NOVALUE;
    int _4382 = NOVALUE;
    int _4381 = NOVALUE;
    int _4380 = NOVALUE;
    int _4379 = NOVALUE;
    int _4378 = NOVALUE;
    int _4377 = NOVALUE;
    int _4376 = NOVALUE;
    int _4375 = NOVALUE;
    int _4373 = NOVALUE;
    int _4372 = NOVALUE;
    int _4371 = NOVALUE;
    int _4370 = NOVALUE;
    int _4369 = NOVALUE;
    int _4367 = NOVALUE;
    int _4363 = NOVALUE;
    int _4362 = NOVALUE;
    int _4361 = NOVALUE;
    int _4360 = NOVALUE;
    int _4359 = NOVALUE;
    int _4358 = NOVALUE;
    int _4357 = NOVALUE;
    int _4356 = NOVALUE;
    int _4355 = NOVALUE;
    int _4354 = NOVALUE;
    int _4353 = NOVALUE;
    int _4352 = NOVALUE;
    int _4351 = NOVALUE;
    int _4350 = NOVALUE;
    int _4349 = NOVALUE;
    int _4348 = NOVALUE;
    int _4347 = NOVALUE;
    int _4346 = NOVALUE;
    int _4345 = NOVALUE;
    int _4344 = NOVALUE;
    int _4342 = NOVALUE;
    int _4341 = NOVALUE;
    int _4340 = NOVALUE;
    int _4339 = NOVALUE;
    int _4338 = NOVALUE;
    int _4337 = NOVALUE;
    int _4336 = NOVALUE;
    int _4335 = NOVALUE;
    int _4334 = NOVALUE;
    int _4331 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_7876 != 0)
    goto L1; // [11] 40

    /** 		c = getc(fh)*/
    if (_fh_7875 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_7875, EF_READ);
        last_r_file_no = _fh_7875;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_7876 = getKBchar();
        }
        else
        _c_7876 = getc(last_r_file_ptr);
    }
    else
    _c_7876 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_7876 >= 249)
    goto L2; // [24] 39

    /** 			return c + MIN1B*/
    _4331 = _c_7876 + -9;
    DeRef(_s_7877);
    DeRef(_len_7878);
    return _4331;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_7876;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(fh) +*/
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4334 = getKBchar();
            }
            else
            _4334 = getc(last_r_file_ptr);
        }
        else
        _4334 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4335 = getKBchar();
            }
            else
            _4335 = getc(last_r_file_ptr);
        }
        else
        _4335 = getc(last_r_file_ptr);
        _4336 = 256 * _4335;
        _4335 = NOVALUE;
        _4337 = _4334 + _4336;
        _4334 = NOVALUE;
        _4336 = NOVALUE;
        _4338 = _4337 + _25MIN2B_7772;
        if ((long)((unsigned long)_4338 + (unsigned long)HIGH_BITS) >= 0) 
        _4338 = NewDouble((double)_4338);
        _4337 = NOVALUE;
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        return _4338;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(fh) +*/
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4339 = getKBchar();
            }
            else
            _4339 = getc(last_r_file_ptr);
        }
        else
        _4339 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4340 = getKBchar();
            }
            else
            _4340 = getc(last_r_file_ptr);
        }
        else
        _4340 = getc(last_r_file_ptr);
        _4341 = 256 * _4340;
        _4340 = NOVALUE;
        _4342 = _4339 + _4341;
        _4339 = NOVALUE;
        _4341 = NOVALUE;
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4344 = getKBchar();
            }
            else
            _4344 = getc(last_r_file_ptr);
        }
        else
        _4344 = getc(last_r_file_ptr);
        _4345 = 65536 * _4344;
        _4344 = NOVALUE;
        _4346 = _4342 + _4345;
        _4342 = NOVALUE;
        _4345 = NOVALUE;
        _4347 = _4346 + _25MIN3B_7778;
        if ((long)((unsigned long)_4347 + (unsigned long)HIGH_BITS) >= 0) 
        _4347 = NewDouble((double)_4347);
        _4346 = NOVALUE;
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        return _4347;

        /** 		case I4B then*/
        case 251:

        /** 			return get4(fh) + MIN4B*/
        _4348 = _25get4(_fh_7875);
        if (IS_ATOM_INT(_4348) && IS_ATOM_INT(_25MIN4B_7784)) {
            _4349 = _4348 + _25MIN4B_7784;
            if ((long)((unsigned long)_4349 + (unsigned long)HIGH_BITS) >= 0) 
            _4349 = NewDouble((double)_4349);
        }
        else {
            _4349 = binary_op(PLUS, _4348, _25MIN4B_7784);
        }
        DeRef(_4348);
        _4348 = NOVALUE;
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        DeRef(_4347);
        _4347 = NOVALUE;
        return _4349;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(fh), getc(fh),*/
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4350 = getKBchar();
            }
            else
            _4350 = getc(last_r_file_ptr);
        }
        else
        _4350 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4351 = getKBchar();
            }
            else
            _4351 = getc(last_r_file_ptr);
        }
        else
        _4351 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4352 = getKBchar();
            }
            else
            _4352 = getc(last_r_file_ptr);
        }
        else
        _4352 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4353 = getKBchar();
            }
            else
            _4353 = getc(last_r_file_ptr);
        }
        else
        _4353 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4350;
        *((int *)(_2+8)) = _4351;
        *((int *)(_2+12)) = _4352;
        *((int *)(_2+16)) = _4353;
        _4354 = MAKE_SEQ(_1);
        _4353 = NOVALUE;
        _4352 = NOVALUE;
        _4351 = NOVALUE;
        _4350 = NOVALUE;
        DeRefi(_ieee32_inlined_float32_to_atom_at_162_7913);
        _ieee32_inlined_float32_to_atom_at_162_7913 = _4354;
        _4354 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_165_7914);
        _float32_to_atom_inlined_float32_to_atom_at_165_7914 = machine(49, _ieee32_inlined_float32_to_atom_at_162_7913);
        DeRefi(_ieee32_inlined_float32_to_atom_at_162_7913);
        _ieee32_inlined_float32_to_atom_at_162_7913 = NOVALUE;
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        DeRef(_4347);
        _4347 = NOVALUE;
        DeRef(_4349);
        _4349 = NOVALUE;
        return _float32_to_atom_inlined_float32_to_atom_at_165_7914;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(fh), getc(fh),*/
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4355 = getKBchar();
            }
            else
            _4355 = getc(last_r_file_ptr);
        }
        else
        _4355 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4356 = getKBchar();
            }
            else
            _4356 = getc(last_r_file_ptr);
        }
        else
        _4356 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4357 = getKBchar();
            }
            else
            _4357 = getc(last_r_file_ptr);
        }
        else
        _4357 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4358 = getKBchar();
            }
            else
            _4358 = getc(last_r_file_ptr);
        }
        else
        _4358 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4359 = getKBchar();
            }
            else
            _4359 = getc(last_r_file_ptr);
        }
        else
        _4359 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4360 = getKBchar();
            }
            else
            _4360 = getc(last_r_file_ptr);
        }
        else
        _4360 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4361 = getKBchar();
            }
            else
            _4361 = getc(last_r_file_ptr);
        }
        else
        _4361 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4362 = getKBchar();
            }
            else
            _4362 = getc(last_r_file_ptr);
        }
        else
        _4362 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4355;
        *((int *)(_2+8)) = _4356;
        *((int *)(_2+12)) = _4357;
        *((int *)(_2+16)) = _4358;
        *((int *)(_2+20)) = _4359;
        *((int *)(_2+24)) = _4360;
        *((int *)(_2+28)) = _4361;
        *((int *)(_2+32)) = _4362;
        _4363 = MAKE_SEQ(_1);
        _4362 = NOVALUE;
        _4361 = NOVALUE;
        _4360 = NOVALUE;
        _4359 = NOVALUE;
        _4358 = NOVALUE;
        _4357 = NOVALUE;
        _4356 = NOVALUE;
        _4355 = NOVALUE;
        DeRefi(_ieee64_inlined_float64_to_atom_at_221_7926);
        _ieee64_inlined_float64_to_atom_at_221_7926 = _4363;
        _4363 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_224_7927);
        _float64_to_atom_inlined_float64_to_atom_at_224_7927 = machine(47, _ieee64_inlined_float64_to_atom_at_221_7926);
        DeRefi(_ieee64_inlined_float64_to_atom_at_221_7926);
        _ieee64_inlined_float64_to_atom_at_221_7926 = NOVALUE;
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        DeRef(_4347);
        _4347 = NOVALUE;
        DeRef(_4349);
        _4349 = NOVALUE;
        return _float64_to_atom_inlined_float64_to_atom_at_224_7927;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_7876 != 254)
        goto L3; // [246] 258

        /** 				len = getc(fh)*/
        DeRef(_len_7878);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_7878 = getKBchar();
            }
            else
            _len_7878 = getc(last_r_file_ptr);
        }
        else
        _len_7878 = getc(last_r_file_ptr);
        goto L4; // [255] 265
L3: 

        /** 				len = get4(fh)*/
        _0 = _len_7878;
        _len_7878 = _25get4(_fh_7875);
        DeRef(_0);
L4: 

        /** 			if len < 0  or not integer(len) then*/
        if (IS_ATOM_INT(_len_7878)) {
            _4367 = (_len_7878 < 0);
        }
        else {
            _4367 = (DBL_PTR(_len_7878)->dbl < (double)0);
        }
        if (_4367 != 0) {
            goto L5; // [273] 288
        }
        if (IS_ATOM_INT(_len_7878))
        _4369 = 1;
        else if (IS_ATOM_DBL(_len_7878))
        _4369 = IS_ATOM_INT(DoubleToInt(_len_7878));
        else
        _4369 = 0;
        _4370 = (_4369 == 0);
        _4369 = NOVALUE;
        if (_4370 == 0)
        {
            DeRef(_4370);
            _4370 = NOVALUE;
            goto L6; // [284] 295
        }
        else{
            DeRef(_4370);
            _4370 = NOVALUE;
        }
L5: 

        /** 				return 0*/
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        DeRef(_4347);
        _4347 = NOVALUE;
        DeRef(_4367);
        _4367 = NOVALUE;
        DeRef(_4349);
        _4349 = NOVALUE;
        return 0;
L6: 

        /** 			if c = S4B and len < 256 then*/
        _4371 = (_c_7876 == 255);
        if (_4371 == 0) {
            goto L7; // [301] 453
        }
        if (IS_ATOM_INT(_len_7878)) {
            _4373 = (_len_7878 < 256);
        }
        else {
            _4373 = (DBL_PTR(_len_7878)->dbl < (double)256);
        }
        if (_4373 == 0)
        {
            DeRef(_4373);
            _4373 = NOVALUE;
            goto L7; // [310] 453
        }
        else{
            DeRef(_4373);
            _4373 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (binary_op_a(NOTEQ, _len_7878, 0)){
            goto L8; // [315] 367
        }

        /** 					return int64_to_atom( {*/
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4375 = getKBchar();
            }
            else
            _4375 = getc(last_r_file_ptr);
        }
        else
        _4375 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4376 = getKBchar();
            }
            else
            _4376 = getc(last_r_file_ptr);
        }
        else
        _4376 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4377 = getKBchar();
            }
            else
            _4377 = getc(last_r_file_ptr);
        }
        else
        _4377 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4378 = getKBchar();
            }
            else
            _4378 = getc(last_r_file_ptr);
        }
        else
        _4378 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4379 = getKBchar();
            }
            else
            _4379 = getc(last_r_file_ptr);
        }
        else
        _4379 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4380 = getKBchar();
            }
            else
            _4380 = getc(last_r_file_ptr);
        }
        else
        _4380 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4381 = getKBchar();
            }
            else
            _4381 = getc(last_r_file_ptr);
        }
        else
        _4381 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4382 = getKBchar();
            }
            else
            _4382 = getc(last_r_file_ptr);
        }
        else
        _4382 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4375;
        *((int *)(_2+8)) = _4376;
        *((int *)(_2+12)) = _4377;
        *((int *)(_2+16)) = _4378;
        *((int *)(_2+20)) = _4379;
        *((int *)(_2+24)) = _4380;
        *((int *)(_2+28)) = _4381;
        *((int *)(_2+32)) = _4382;
        _4383 = MAKE_SEQ(_1);
        _4382 = NOVALUE;
        _4381 = NOVALUE;
        _4380 = NOVALUE;
        _4379 = NOVALUE;
        _4378 = NOVALUE;
        _4377 = NOVALUE;
        _4376 = NOVALUE;
        _4375 = NOVALUE;
        _4384 = _25int64_to_atom(_4383);
        _4383 = NOVALUE;
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        DeRef(_4347);
        _4347 = NOVALUE;
        DeRef(_4367);
        _4367 = NOVALUE;
        DeRef(_4349);
        _4349 = NOVALUE;
        DeRef(_4371);
        _4371 = NOVALUE;
        return _4384;
        goto L9; // [364] 529
L8: 

        /** 				elsif len = F10B then*/
        if (binary_op_a(NOTEQ, _len_7878, 1)){
            goto LA; // [369] 429
        }

        /** 					return float80_to_atom(*/
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4386 = getKBchar();
            }
            else
            _4386 = getc(last_r_file_ptr);
        }
        else
        _4386 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4387 = getKBchar();
            }
            else
            _4387 = getc(last_r_file_ptr);
        }
        else
        _4387 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4388 = getKBchar();
            }
            else
            _4388 = getc(last_r_file_ptr);
        }
        else
        _4388 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4389 = getKBchar();
            }
            else
            _4389 = getc(last_r_file_ptr);
        }
        else
        _4389 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4390 = getKBchar();
            }
            else
            _4390 = getc(last_r_file_ptr);
        }
        else
        _4390 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4391 = getKBchar();
            }
            else
            _4391 = getc(last_r_file_ptr);
        }
        else
        _4391 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4392 = getKBchar();
            }
            else
            _4392 = getc(last_r_file_ptr);
        }
        else
        _4392 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4393 = getKBchar();
            }
            else
            _4393 = getc(last_r_file_ptr);
        }
        else
        _4393 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4394 = getKBchar();
            }
            else
            _4394 = getc(last_r_file_ptr);
        }
        else
        _4394 = getc(last_r_file_ptr);
        if (_fh_7875 != last_r_file_no) {
            last_r_file_ptr = which_file(_fh_7875, EF_READ);
            last_r_file_no = _fh_7875;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4395 = getKBchar();
            }
            else
            _4395 = getc(last_r_file_ptr);
        }
        else
        _4395 = getc(last_r_file_ptr);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _4386;
        *((int *)(_2+8)) = _4387;
        *((int *)(_2+12)) = _4388;
        *((int *)(_2+16)) = _4389;
        *((int *)(_2+20)) = _4390;
        *((int *)(_2+24)) = _4391;
        *((int *)(_2+28)) = _4392;
        *((int *)(_2+32)) = _4393;
        *((int *)(_2+36)) = _4394;
        *((int *)(_2+40)) = _4395;
        _4396 = MAKE_SEQ(_1);
        _4395 = NOVALUE;
        _4394 = NOVALUE;
        _4393 = NOVALUE;
        _4392 = NOVALUE;
        _4391 = NOVALUE;
        _4390 = NOVALUE;
        _4389 = NOVALUE;
        _4388 = NOVALUE;
        _4387 = NOVALUE;
        _4386 = NOVALUE;
        _4397 = _25float80_to_atom(_4396);
        _4396 = NOVALUE;
        DeRef(_s_7877);
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        DeRef(_4347);
        _4347 = NOVALUE;
        DeRef(_4367);
        _4367 = NOVALUE;
        DeRef(_4349);
        _4349 = NOVALUE;
        DeRef(_4371);
        _4371 = NOVALUE;
        DeRef(_4384);
        _4384 = NOVALUE;
        return _4397;
        goto L9; // [426] 529
LA: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_430_7972);
        _msg_inlined_crash_at_430_7972 = EPrintf(-9999999, _4398, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_430_7972);

        /** end procedure*/
        goto LB; // [444] 447
LB: 
        DeRefi(_msg_inlined_crash_at_430_7972);
        _msg_inlined_crash_at_430_7972 = NOVALUE;
        goto L9; // [450] 529
L7: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_7877);
        _s_7877 = Repeat(0, _len_7878);

        /** 				for i = 1 to len do*/
        Ref(_len_7878);
        DeRef(_4400);
        _4400 = _len_7878;
        {
            int _i_7976;
            _i_7976 = 1;
LC: 
            if (binary_op_a(GREATER, _i_7976, _4400)){
                goto LD; // [464] 522
            }

            /** 					c = getc(fh)*/
            if (_fh_7875 != last_r_file_no) {
                last_r_file_ptr = which_file(_fh_7875, EF_READ);
                last_r_file_no = _fh_7875;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_7876 = getKBchar();
                }
                else
                _c_7876 = getc(last_r_file_ptr);
            }
            else
            _c_7876 = getc(last_r_file_ptr);

            /** 					if c < I2B then*/
            if (_c_7876 >= 249)
            goto LE; // [480] 497

            /** 						s[i] = c + MIN1B*/
            _4403 = _c_7876 + -9;
            _2 = (int)SEQ_PTR(_s_7877);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_7877 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_7976))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7976)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_7976);
            _1 = *(int *)_2;
            *(int *)_2 = _4403;
            if( _1 != _4403 ){
                DeRef(_1);
            }
            _4403 = NOVALUE;
            goto LF; // [494] 515
LE: 

            /** 						s[i] = deserialize_file(fh, c)*/
            DeRef(_4404);
            _4404 = _fh_7875;
            DeRef(_4405);
            _4405 = _c_7876;
            _4406 = _25deserialize_file(_4404, _4405);
            _4404 = NOVALUE;
            _4405 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_7877);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_7877 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_i_7976))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_7976)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _i_7976);
            _1 = *(int *)_2;
            *(int *)_2 = _4406;
            if( _1 != _4406 ){
                DeRef(_1);
            }
            _4406 = NOVALUE;
LF: 

            /** 				end for*/
            _0 = _i_7976;
            if (IS_ATOM_INT(_i_7976)) {
                _i_7976 = _i_7976 + 1;
                if ((long)((unsigned long)_i_7976 +(unsigned long) HIGH_BITS) >= 0){
                    _i_7976 = NewDouble((double)_i_7976);
                }
            }
            else {
                _i_7976 = binary_op_a(PLUS, _i_7976, 1);
            }
            DeRef(_0);
            goto LC; // [517] 471
LD: 
            ;
            DeRef(_i_7976);
        }

        /** 				return s*/
        DeRef(_len_7878);
        DeRef(_4331);
        _4331 = NOVALUE;
        DeRef(_4338);
        _4338 = NOVALUE;
        DeRef(_4347);
        _4347 = NOVALUE;
        DeRef(_4367);
        _4367 = NOVALUE;
        DeRef(_4349);
        _4349 = NOVALUE;
        DeRef(_4371);
        _4371 = NOVALUE;
        DeRef(_4384);
        _4384 = NOVALUE;
        DeRef(_4397);
        _4397 = NOVALUE;
        return _s_7877;
L9: 
    ;}    ;
}


int _25getp4(int _sdata_7988, int _pos_7989)
{
    int _4415 = NOVALUE;
    int _4414 = NOVALUE;
    int _4413 = NOVALUE;
    int _4412 = NOVALUE;
    int _4411 = NOVALUE;
    int _4410 = NOVALUE;
    int _4409 = NOVALUE;
    int _4408 = NOVALUE;
    int _4407 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, sdata[pos+0])*/
    _4407 = _pos_7989 + 0;
    _2 = (int)SEQ_PTR(_sdata_7988);
    _4408 = (int)*(((s1_ptr)_2)->base + _4407);
    if (IS_ATOM_INT(_25mem0_7787)){
        poke_addr = (unsigned char *)_25mem0_7787;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem0_7787)->dbl);
    }
    if (IS_ATOM_INT(_4408)) {
        *poke_addr = (unsigned char)_4408;
    }
    else if (IS_ATOM(_4408)) {
        _1 = (signed char)DBL_PTR(_4408)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4408);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4408 = NOVALUE;

    /** 	poke(mem1, sdata[pos+1])*/
    _4409 = _pos_7989 + 1;
    _2 = (int)SEQ_PTR(_sdata_7988);
    _4410 = (int)*(((s1_ptr)_2)->base + _4409);
    if (IS_ATOM_INT(_25mem1_7788)){
        poke_addr = (unsigned char *)_25mem1_7788;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem1_7788)->dbl);
    }
    if (IS_ATOM_INT(_4410)) {
        *poke_addr = (unsigned char)_4410;
    }
    else if (IS_ATOM(_4410)) {
        _1 = (signed char)DBL_PTR(_4410)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4410);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4410 = NOVALUE;

    /** 	poke(mem2, sdata[pos+2])*/
    _4411 = _pos_7989 + 2;
    _2 = (int)SEQ_PTR(_sdata_7988);
    _4412 = (int)*(((s1_ptr)_2)->base + _4411);
    if (IS_ATOM_INT(_25mem2_7789)){
        poke_addr = (unsigned char *)_25mem2_7789;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem2_7789)->dbl);
    }
    if (IS_ATOM_INT(_4412)) {
        *poke_addr = (unsigned char)_4412;
    }
    else if (IS_ATOM(_4412)) {
        _1 = (signed char)DBL_PTR(_4412)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4412);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4412 = NOVALUE;

    /** 	poke(mem3, sdata[pos+3])*/
    _4413 = _pos_7989 + 3;
    _2 = (int)SEQ_PTR(_sdata_7988);
    _4414 = (int)*(((s1_ptr)_2)->base + _4413);
    if (IS_ATOM_INT(_25mem3_7790)){
        poke_addr = (unsigned char *)_25mem3_7790;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_25mem3_7790)->dbl);
    }
    if (IS_ATOM_INT(_4414)) {
        *poke_addr = (unsigned char)_4414;
    }
    else if (IS_ATOM(_4414)) {
        _1 = (signed char)DBL_PTR(_4414)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_4414);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    _4414 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_25mem0_7787)) {
        _4415 = *(unsigned long *)_25mem0_7787;
        if ((unsigned)_4415 > (unsigned)MAXINT)
        _4415 = NewDouble((double)(unsigned long)_4415);
    }
    else {
        _4415 = *(unsigned long *)(unsigned long)(DBL_PTR(_25mem0_7787)->dbl);
        if ((unsigned)_4415 > (unsigned)MAXINT)
        _4415 = NewDouble((double)(unsigned long)_4415);
    }
    DeRefDS(_sdata_7988);
    _4407 = NOVALUE;
    _4409 = NOVALUE;
    _4411 = NOVALUE;
    _4413 = NOVALUE;
    return _4415;
    ;
}


int _25deserialize_object(int _sdata_8001, int _pos_8002, int _c_8003)
{
    int _s_8004 = NOVALUE;
    int _len_8005 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_238_8054 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_235_8053 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_341_8076 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_338_8075 = NOVALUE;
    int _msg_inlined_crash_at_509_8107 = NOVALUE;
    int _temp_8119 = NOVALUE;
    int _4508 = NOVALUE;
    int _4506 = NOVALUE;
    int _4504 = NOVALUE;
    int _4503 = NOVALUE;
    int _4502 = NOVALUE;
    int _4501 = NOVALUE;
    int _4497 = NOVALUE;
    int _4495 = NOVALUE;
    int _4494 = NOVALUE;
    int _4493 = NOVALUE;
    int _4492 = NOVALUE;
    int _4491 = NOVALUE;
    int _4489 = NOVALUE;
    int _4488 = NOVALUE;
    int _4487 = NOVALUE;
    int _4486 = NOVALUE;
    int _4485 = NOVALUE;
    int _4483 = NOVALUE;
    int _4482 = NOVALUE;
    int _4481 = NOVALUE;
    int _4475 = NOVALUE;
    int _4474 = NOVALUE;
    int _4473 = NOVALUE;
    int _4472 = NOVALUE;
    int _4471 = NOVALUE;
    int _4470 = NOVALUE;
    int _4469 = NOVALUE;
    int _4468 = NOVALUE;
    int _4467 = NOVALUE;
    int _4466 = NOVALUE;
    int _4465 = NOVALUE;
    int _4464 = NOVALUE;
    int _4463 = NOVALUE;
    int _4462 = NOVALUE;
    int _4461 = NOVALUE;
    int _4460 = NOVALUE;
    int _4459 = NOVALUE;
    int _4458 = NOVALUE;
    int _4457 = NOVALUE;
    int _4456 = NOVALUE;
    int _4455 = NOVALUE;
    int _4454 = NOVALUE;
    int _4453 = NOVALUE;
    int _4452 = NOVALUE;
    int _4451 = NOVALUE;
    int _4450 = NOVALUE;
    int _4449 = NOVALUE;
    int _4448 = NOVALUE;
    int _4447 = NOVALUE;
    int _4446 = NOVALUE;
    int _4445 = NOVALUE;
    int _4444 = NOVALUE;
    int _4443 = NOVALUE;
    int _4442 = NOVALUE;
    int _4441 = NOVALUE;
    int _4440 = NOVALUE;
    int _4439 = NOVALUE;
    int _4438 = NOVALUE;
    int _4437 = NOVALUE;
    int _4436 = NOVALUE;
    int _4435 = NOVALUE;
    int _4434 = NOVALUE;
    int _4433 = NOVALUE;
    int _4432 = NOVALUE;
    int _4431 = NOVALUE;
    int _4430 = NOVALUE;
    int _4429 = NOVALUE;
    int _4428 = NOVALUE;
    int _4427 = NOVALUE;
    int _4426 = NOVALUE;
    int _4425 = NOVALUE;
    int _4424 = NOVALUE;
    int _4421 = NOVALUE;
    int _4420 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_8003 != 0)
    goto L1; // [13] 55

    /** 		c = sdata[pos]*/
    _2 = (int)SEQ_PTR(_sdata_8001);
    _c_8003 = (int)*(((s1_ptr)_2)->base + _pos_8002);
    if (!IS_ATOM_INT(_c_8003))
    _c_8003 = (long)DBL_PTR(_c_8003)->dbl;

    /** 		pos += 1*/
    _pos_8002 = _pos_8002 + 1;

    /** 		if c < I2B then*/
    if (_c_8003 >= 249)
    goto L2; // [35] 54

    /** 			return {c + MIN1B, pos}*/
    _4420 = _c_8003 + -9;
    if ((long)((unsigned long)_4420 + (unsigned long)HIGH_BITS) >= 0) 
    _4420 = NewDouble((double)_4420);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4420;
    ((int *)_2)[2] = _pos_8002;
    _4421 = MAKE_SEQ(_1);
    _4420 = NOVALUE;
    DeRefDS(_sdata_8001);
    DeRef(_s_8004);
    return _4421;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_8003;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4424 = (int)*(((s1_ptr)_2)->base + _pos_8002);
        _4425 = _pos_8002 + 1;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4426 = (int)*(((s1_ptr)_2)->base + _4425);
        if (IS_ATOM_INT(_4426)) {
            if (_4426 <= INT15 && _4426 >= -INT15)
            _4427 = 256 * _4426;
            else
            _4427 = NewDouble(256 * (double)_4426);
        }
        else {
            _4427 = binary_op(MULTIPLY, 256, _4426);
        }
        _4426 = NOVALUE;
        if (IS_ATOM_INT(_4424) && IS_ATOM_INT(_4427)) {
            _4428 = _4424 + _4427;
            if ((long)((unsigned long)_4428 + (unsigned long)HIGH_BITS) >= 0) 
            _4428 = NewDouble((double)_4428);
        }
        else {
            _4428 = binary_op(PLUS, _4424, _4427);
        }
        _4424 = NOVALUE;
        DeRef(_4427);
        _4427 = NOVALUE;
        if (IS_ATOM_INT(_4428)) {
            _4429 = _4428 + _25MIN2B_7772;
            if ((long)((unsigned long)_4429 + (unsigned long)HIGH_BITS) >= 0) 
            _4429 = NewDouble((double)_4429);
        }
        else {
            _4429 = binary_op(PLUS, _4428, _25MIN2B_7772);
        }
        DeRef(_4428);
        _4428 = NOVALUE;
        _4430 = _pos_8002 + 2;
        if ((long)((unsigned long)_4430 + (unsigned long)HIGH_BITS) >= 0) 
        _4430 = NewDouble((double)_4430);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4429;
        ((int *)_2)[2] = _4430;
        _4431 = MAKE_SEQ(_1);
        _4430 = NOVALUE;
        _4429 = NOVALUE;
        DeRefDS(_sdata_8001);
        DeRef(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        _4425 = NOVALUE;
        return _4431;

        /** 		case I3B then*/
        case 250:

        /** 			return {sdata[pos] +*/
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4432 = (int)*(((s1_ptr)_2)->base + _pos_8002);
        _4433 = _pos_8002 + 1;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4434 = (int)*(((s1_ptr)_2)->base + _4433);
        if (IS_ATOM_INT(_4434)) {
            if (_4434 <= INT15 && _4434 >= -INT15)
            _4435 = 256 * _4434;
            else
            _4435 = NewDouble(256 * (double)_4434);
        }
        else {
            _4435 = binary_op(MULTIPLY, 256, _4434);
        }
        _4434 = NOVALUE;
        if (IS_ATOM_INT(_4432) && IS_ATOM_INT(_4435)) {
            _4436 = _4432 + _4435;
            if ((long)((unsigned long)_4436 + (unsigned long)HIGH_BITS) >= 0) 
            _4436 = NewDouble((double)_4436);
        }
        else {
            _4436 = binary_op(PLUS, _4432, _4435);
        }
        _4432 = NOVALUE;
        DeRef(_4435);
        _4435 = NOVALUE;
        _4437 = _pos_8002 + 2;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4438 = (int)*(((s1_ptr)_2)->base + _4437);
        if (IS_ATOM_INT(_4438)) {
            _4439 = NewDouble(65536 * (double)_4438);
        }
        else {
            _4439 = binary_op(MULTIPLY, 65536, _4438);
        }
        _4438 = NOVALUE;
        if (IS_ATOM_INT(_4436) && IS_ATOM_INT(_4439)) {
            _4440 = _4436 + _4439;
            if ((long)((unsigned long)_4440 + (unsigned long)HIGH_BITS) >= 0) 
            _4440 = NewDouble((double)_4440);
        }
        else {
            _4440 = binary_op(PLUS, _4436, _4439);
        }
        DeRef(_4436);
        _4436 = NOVALUE;
        DeRef(_4439);
        _4439 = NOVALUE;
        if (IS_ATOM_INT(_4440)) {
            _4441 = _4440 + _25MIN3B_7778;
            if ((long)((unsigned long)_4441 + (unsigned long)HIGH_BITS) >= 0) 
            _4441 = NewDouble((double)_4441);
        }
        else {
            _4441 = binary_op(PLUS, _4440, _25MIN3B_7778);
        }
        DeRef(_4440);
        _4440 = NOVALUE;
        _4442 = _pos_8002 + 3;
        if ((long)((unsigned long)_4442 + (unsigned long)HIGH_BITS) >= 0) 
        _4442 = NewDouble((double)_4442);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4441;
        ((int *)_2)[2] = _4442;
        _4443 = MAKE_SEQ(_1);
        _4442 = NOVALUE;
        _4441 = NOVALUE;
        DeRefDS(_sdata_8001);
        DeRef(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        DeRef(_4431);
        _4431 = NOVALUE;
        DeRef(_4425);
        _4425 = NOVALUE;
        _4437 = NOVALUE;
        _4433 = NOVALUE;
        return _4443;

        /** 		case I4B then*/
        case 251:

        /** 			return {getp4(sdata, pos) + MIN4B, pos + 4}*/
        RefDS(_sdata_8001);
        _4444 = _25getp4(_sdata_8001, _pos_8002);
        if (IS_ATOM_INT(_4444) && IS_ATOM_INT(_25MIN4B_7784)) {
            _4445 = _4444 + _25MIN4B_7784;
            if ((long)((unsigned long)_4445 + (unsigned long)HIGH_BITS) >= 0) 
            _4445 = NewDouble((double)_4445);
        }
        else {
            _4445 = binary_op(PLUS, _4444, _25MIN4B_7784);
        }
        DeRef(_4444);
        _4444 = NOVALUE;
        _4446 = _pos_8002 + 4;
        if ((long)((unsigned long)_4446 + (unsigned long)HIGH_BITS) >= 0) 
        _4446 = NewDouble((double)_4446);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4445;
        ((int *)_2)[2] = _4446;
        _4447 = MAKE_SEQ(_1);
        _4446 = NOVALUE;
        _4445 = NOVALUE;
        DeRefDS(_sdata_8001);
        DeRef(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        DeRef(_4431);
        _4431 = NOVALUE;
        DeRef(_4425);
        _4425 = NOVALUE;
        DeRef(_4437);
        _4437 = NOVALUE;
        DeRef(_4433);
        _4433 = NOVALUE;
        DeRef(_4443);
        _4443 = NOVALUE;
        return _4447;

        /** 		case F4B then*/
        case 252:

        /** 			return {convert:float32_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4448 = (int)*(((s1_ptr)_2)->base + _pos_8002);
        _4449 = _pos_8002 + 1;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4450 = (int)*(((s1_ptr)_2)->base + _4449);
        _4451 = _pos_8002 + 2;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4452 = (int)*(((s1_ptr)_2)->base + _4451);
        _4453 = _pos_8002 + 3;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4454 = (int)*(((s1_ptr)_2)->base + _4453);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_4448);
        *((int *)(_2+4)) = _4448;
        Ref(_4450);
        *((int *)(_2+8)) = _4450;
        Ref(_4452);
        *((int *)(_2+12)) = _4452;
        Ref(_4454);
        *((int *)(_2+16)) = _4454;
        _4455 = MAKE_SEQ(_1);
        _4454 = NOVALUE;
        _4452 = NOVALUE;
        _4450 = NOVALUE;
        _4448 = NOVALUE;
        DeRef(_ieee32_inlined_float32_to_atom_at_235_8053);
        _ieee32_inlined_float32_to_atom_at_235_8053 = _4455;
        _4455 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_238_8054);
        _float32_to_atom_inlined_float32_to_atom_at_238_8054 = machine(49, _ieee32_inlined_float32_to_atom_at_235_8053);
        DeRef(_ieee32_inlined_float32_to_atom_at_235_8053);
        _ieee32_inlined_float32_to_atom_at_235_8053 = NOVALUE;
        _4456 = _pos_8002 + 4;
        if ((long)((unsigned long)_4456 + (unsigned long)HIGH_BITS) >= 0) 
        _4456 = NewDouble((double)_4456);
        Ref(_float32_to_atom_inlined_float32_to_atom_at_238_8054);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _float32_to_atom_inlined_float32_to_atom_at_238_8054;
        ((int *)_2)[2] = _4456;
        _4457 = MAKE_SEQ(_1);
        _4456 = NOVALUE;
        DeRefDS(_sdata_8001);
        DeRef(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        DeRef(_4431);
        _4431 = NOVALUE;
        DeRef(_4425);
        _4425 = NOVALUE;
        DeRef(_4437);
        _4437 = NOVALUE;
        DeRef(_4433);
        _4433 = NOVALUE;
        DeRef(_4443);
        _4443 = NOVALUE;
        DeRef(_4447);
        _4447 = NOVALUE;
        DeRef(_4449);
        _4449 = NOVALUE;
        DeRef(_4451);
        _4451 = NOVALUE;
        DeRef(_4453);
        _4453 = NOVALUE;
        return _4457;

        /** 		case F8B then*/
        case 253:

        /** 			return {convert:float64_to_atom({sdata[pos], sdata[pos+1],*/
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4458 = (int)*(((s1_ptr)_2)->base + _pos_8002);
        _4459 = _pos_8002 + 1;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4460 = (int)*(((s1_ptr)_2)->base + _4459);
        _4461 = _pos_8002 + 2;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4462 = (int)*(((s1_ptr)_2)->base + _4461);
        _4463 = _pos_8002 + 3;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4464 = (int)*(((s1_ptr)_2)->base + _4463);
        _4465 = _pos_8002 + 4;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4466 = (int)*(((s1_ptr)_2)->base + _4465);
        _4467 = _pos_8002 + 5;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4468 = (int)*(((s1_ptr)_2)->base + _4467);
        _4469 = _pos_8002 + 6;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4470 = (int)*(((s1_ptr)_2)->base + _4469);
        _4471 = _pos_8002 + 7;
        _2 = (int)SEQ_PTR(_sdata_8001);
        _4472 = (int)*(((s1_ptr)_2)->base + _4471);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_4458);
        *((int *)(_2+4)) = _4458;
        Ref(_4460);
        *((int *)(_2+8)) = _4460;
        Ref(_4462);
        *((int *)(_2+12)) = _4462;
        Ref(_4464);
        *((int *)(_2+16)) = _4464;
        Ref(_4466);
        *((int *)(_2+20)) = _4466;
        Ref(_4468);
        *((int *)(_2+24)) = _4468;
        Ref(_4470);
        *((int *)(_2+28)) = _4470;
        Ref(_4472);
        *((int *)(_2+32)) = _4472;
        _4473 = MAKE_SEQ(_1);
        _4472 = NOVALUE;
        _4470 = NOVALUE;
        _4468 = NOVALUE;
        _4466 = NOVALUE;
        _4464 = NOVALUE;
        _4462 = NOVALUE;
        _4460 = NOVALUE;
        _4458 = NOVALUE;
        DeRef(_ieee64_inlined_float64_to_atom_at_338_8075);
        _ieee64_inlined_float64_to_atom_at_338_8075 = _4473;
        _4473 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_341_8076);
        _float64_to_atom_inlined_float64_to_atom_at_341_8076 = machine(47, _ieee64_inlined_float64_to_atom_at_338_8075);
        DeRef(_ieee64_inlined_float64_to_atom_at_338_8075);
        _ieee64_inlined_float64_to_atom_at_338_8075 = NOVALUE;
        _4474 = _pos_8002 + 8;
        if ((long)((unsigned long)_4474 + (unsigned long)HIGH_BITS) >= 0) 
        _4474 = NewDouble((double)_4474);
        Ref(_float64_to_atom_inlined_float64_to_atom_at_341_8076);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _float64_to_atom_inlined_float64_to_atom_at_341_8076;
        ((int *)_2)[2] = _4474;
        _4475 = MAKE_SEQ(_1);
        _4474 = NOVALUE;
        DeRefDS(_sdata_8001);
        DeRef(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        DeRef(_4449);
        _4449 = NOVALUE;
        DeRef(_4467);
        _4467 = NOVALUE;
        DeRef(_4459);
        _4459 = NOVALUE;
        DeRef(_4437);
        _4437 = NOVALUE;
        DeRef(_4451);
        _4451 = NOVALUE;
        DeRef(_4433);
        _4433 = NOVALUE;
        DeRef(_4425);
        _4425 = NOVALUE;
        DeRef(_4457);
        _4457 = NOVALUE;
        DeRef(_4471);
        _4471 = NOVALUE;
        DeRef(_4431);
        _4431 = NOVALUE;
        DeRef(_4453);
        _4453 = NOVALUE;
        DeRef(_4461);
        _4461 = NOVALUE;
        DeRef(_4443);
        _4443 = NOVALUE;
        DeRef(_4469);
        _4469 = NOVALUE;
        DeRef(_4463);
        _4463 = NOVALUE;
        DeRef(_4465);
        _4465 = NOVALUE;
        DeRef(_4447);
        _4447 = NOVALUE;
        return _4475;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_8003 != 254)
        goto L3; // [371] 394

        /** 				len = sdata[pos]*/
        _2 = (int)SEQ_PTR(_sdata_8001);
        _len_8005 = (int)*(((s1_ptr)_2)->base + _pos_8002);
        if (!IS_ATOM_INT(_len_8005))
        _len_8005 = (long)DBL_PTR(_len_8005)->dbl;

        /** 				pos += 1*/
        _pos_8002 = _pos_8002 + 1;
        goto L4; // [391] 414
L3: 

        /** 				len = getp4(sdata, pos)*/
        RefDS(_sdata_8001);
        _len_8005 = _25getp4(_sdata_8001, _pos_8002);
        if (!IS_ATOM_INT(_len_8005)) {
            _1 = (long)(DBL_PTR(_len_8005)->dbl);
            if (UNIQUE(DBL_PTR(_len_8005)) && (DBL_PTR(_len_8005)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_8005);
            _len_8005 = _1;
        }

        /** 				pos += 4*/
        _pos_8002 = _pos_8002 + 4;
L4: 

        /** 			if c = S4B and len < 256 then*/
        _4481 = (_c_8003 == 255);
        if (_4481 == 0) {
            goto L5; // [420] 532
        }
        _4483 = (_len_8005 < 256);
        if (_4483 == 0)
        {
            DeRef(_4483);
            _4483 = NOVALUE;
            goto L5; // [431] 532
        }
        else{
            DeRef(_4483);
            _4483 = NOVALUE;
        }

        /** 				if len = I8B then*/
        if (_len_8005 != 0)
        goto L6; // [438] 472

        /** 					return {int64_to_atom( sdata[pos..pos+7] ), pos + 8 }*/
        _4485 = _pos_8002 + 7;
        rhs_slice_target = (object_ptr)&_4486;
        RHS_Slice(_sdata_8001, _pos_8002, _4485);
        _4487 = _25int64_to_atom(_4486);
        _4486 = NOVALUE;
        _4488 = _pos_8002 + 8;
        if ((long)((unsigned long)_4488 + (unsigned long)HIGH_BITS) >= 0) 
        _4488 = NewDouble((double)_4488);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4487;
        ((int *)_2)[2] = _4488;
        _4489 = MAKE_SEQ(_1);
        _4488 = NOVALUE;
        _4487 = NOVALUE;
        DeRefDS(_sdata_8001);
        DeRef(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        DeRef(_4449);
        _4449 = NOVALUE;
        DeRef(_4467);
        _4467 = NOVALUE;
        DeRef(_4475);
        _4475 = NOVALUE;
        DeRef(_4459);
        _4459 = NOVALUE;
        DeRef(_4437);
        _4437 = NOVALUE;
        DeRef(_4451);
        _4451 = NOVALUE;
        DeRef(_4433);
        _4433 = NOVALUE;
        DeRef(_4425);
        _4425 = NOVALUE;
        DeRef(_4457);
        _4457 = NOVALUE;
        DeRef(_4471);
        _4471 = NOVALUE;
        DeRef(_4431);
        _4431 = NOVALUE;
        DeRef(_4453);
        _4453 = NOVALUE;
        DeRef(_4461);
        _4461 = NOVALUE;
        DeRef(_4443);
        _4443 = NOVALUE;
        DeRef(_4469);
        _4469 = NOVALUE;
        _4485 = NOVALUE;
        DeRef(_4481);
        _4481 = NOVALUE;
        DeRef(_4463);
        _4463 = NOVALUE;
        DeRef(_4465);
        _4465 = NOVALUE;
        DeRef(_4447);
        _4447 = NOVALUE;
        return _4489;
        goto L7; // [469] 645
L6: 

        /** 				elsif len = F10B then*/
        if (_len_8005 != 1)
        goto L8; // [474] 508

        /** 					return { float80_to_atom( sdata[pos..pos+9] ), pos + 10 }*/
        _4491 = _pos_8002 + 9;
        rhs_slice_target = (object_ptr)&_4492;
        RHS_Slice(_sdata_8001, _pos_8002, _4491);
        _4493 = _25float80_to_atom(_4492);
        _4492 = NOVALUE;
        _4494 = _pos_8002 + 10;
        if ((long)((unsigned long)_4494 + (unsigned long)HIGH_BITS) >= 0) 
        _4494 = NewDouble((double)_4494);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _4493;
        ((int *)_2)[2] = _4494;
        _4495 = MAKE_SEQ(_1);
        _4494 = NOVALUE;
        _4493 = NOVALUE;
        DeRefDS(_sdata_8001);
        DeRef(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        DeRef(_4449);
        _4449 = NOVALUE;
        DeRef(_4467);
        _4467 = NOVALUE;
        DeRef(_4475);
        _4475 = NOVALUE;
        DeRef(_4489);
        _4489 = NOVALUE;
        DeRef(_4459);
        _4459 = NOVALUE;
        DeRef(_4437);
        _4437 = NOVALUE;
        DeRef(_4451);
        _4451 = NOVALUE;
        DeRef(_4433);
        _4433 = NOVALUE;
        DeRef(_4425);
        _4425 = NOVALUE;
        DeRef(_4457);
        _4457 = NOVALUE;
        DeRef(_4471);
        _4471 = NOVALUE;
        DeRef(_4431);
        _4431 = NOVALUE;
        DeRef(_4453);
        _4453 = NOVALUE;
        DeRef(_4461);
        _4461 = NOVALUE;
        _4491 = NOVALUE;
        DeRef(_4443);
        _4443 = NOVALUE;
        DeRef(_4469);
        _4469 = NOVALUE;
        DeRef(_4485);
        _4485 = NOVALUE;
        DeRef(_4481);
        _4481 = NOVALUE;
        DeRef(_4463);
        _4463 = NOVALUE;
        DeRef(_4465);
        _4465 = NOVALUE;
        DeRef(_4447);
        _4447 = NOVALUE;
        return _4495;
        goto L7; // [505] 645
L8: 

        /** 					error:crash( "Invalid sequence serialization" )*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_509_8107);
        _msg_inlined_crash_at_509_8107 = EPrintf(-9999999, _4398, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_509_8107);

        /** end procedure*/
        goto L9; // [523] 526
L9: 
        DeRefi(_msg_inlined_crash_at_509_8107);
        _msg_inlined_crash_at_509_8107 = NOVALUE;
        goto L7; // [529] 645
L5: 

        /** 				s = repeat(0, len)*/
        DeRef(_s_8004);
        _s_8004 = Repeat(0, _len_8005);

        /** 				for i = 1 to len do*/
        _4497 = _len_8005;
        {
            int _i_8111;
            _i_8111 = 1;
LA: 
            if (_i_8111 > _4497){
                goto LB; // [545] 634
            }

            /** 					c = sdata[pos]*/
            _2 = (int)SEQ_PTR(_sdata_8001);
            _c_8003 = (int)*(((s1_ptr)_2)->base + _pos_8002);
            if (!IS_ATOM_INT(_c_8003))
            _c_8003 = (long)DBL_PTR(_c_8003)->dbl;

            /** 					pos += 1*/
            _pos_8002 = _pos_8002 + 1;

            /** 					if c < I2B then*/
            if (_c_8003 >= 249)
            goto LC; // [570] 587

            /** 						s[i] = c + MIN1B*/
            _4501 = _c_8003 + -9;
            if ((long)((unsigned long)_4501 + (unsigned long)HIGH_BITS) >= 0) 
            _4501 = NewDouble((double)_4501);
            _2 = (int)SEQ_PTR(_s_8004);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8004 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_8111);
            _1 = *(int *)_2;
            *(int *)_2 = _4501;
            if( _1 != _4501 ){
                DeRef(_1);
            }
            _4501 = NOVALUE;
            goto LD; // [584] 627
LC: 

            /** 						sequence temp = deserialize_object(sdata, pos, c)*/
            RefDS(_sdata_8001);
            DeRef(_4502);
            _4502 = _sdata_8001;
            DeRef(_4503);
            _4503 = _pos_8002;
            DeRef(_4504);
            _4504 = _c_8003;
            _0 = _temp_8119;
            _temp_8119 = _25deserialize_object(_4502, _4503, _4504);
            DeRef(_0);
            _4502 = NOVALUE;
            _4503 = NOVALUE;
            _4504 = NOVALUE;

            /** 						s[i] = temp[1]*/
            _2 = (int)SEQ_PTR(_temp_8119);
            _4506 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_4506);
            _2 = (int)SEQ_PTR(_s_8004);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_8004 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_8111);
            _1 = *(int *)_2;
            *(int *)_2 = _4506;
            if( _1 != _4506 ){
                DeRef(_1);
            }
            _4506 = NOVALUE;

            /** 						pos = temp[2]*/
            _2 = (int)SEQ_PTR(_temp_8119);
            _pos_8002 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_pos_8002))
            _pos_8002 = (long)DBL_PTR(_pos_8002)->dbl;
            DeRefDS(_temp_8119);
            _temp_8119 = NOVALUE;
LD: 

            /** 				end for*/
            _i_8111 = _i_8111 + 1;
            goto LA; // [629] 552
LB: 
            ;
        }

        /** 				return {s, pos}*/
        RefDS(_s_8004);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _s_8004;
        ((int *)_2)[2] = _pos_8002;
        _4508 = MAKE_SEQ(_1);
        DeRefDS(_sdata_8001);
        DeRefDS(_s_8004);
        DeRef(_4421);
        _4421 = NOVALUE;
        DeRef(_4449);
        _4449 = NOVALUE;
        DeRef(_4467);
        _4467 = NOVALUE;
        DeRef(_4475);
        _4475 = NOVALUE;
        DeRef(_4489);
        _4489 = NOVALUE;
        DeRef(_4495);
        _4495 = NOVALUE;
        DeRef(_4459);
        _4459 = NOVALUE;
        DeRef(_4437);
        _4437 = NOVALUE;
        DeRef(_4451);
        _4451 = NOVALUE;
        DeRef(_4433);
        _4433 = NOVALUE;
        DeRef(_4425);
        _4425 = NOVALUE;
        DeRef(_4457);
        _4457 = NOVALUE;
        DeRef(_4471);
        _4471 = NOVALUE;
        DeRef(_4431);
        _4431 = NOVALUE;
        DeRef(_4453);
        _4453 = NOVALUE;
        DeRef(_4461);
        _4461 = NOVALUE;
        DeRef(_4491);
        _4491 = NOVALUE;
        DeRef(_4443);
        _4443 = NOVALUE;
        DeRef(_4469);
        _4469 = NOVALUE;
        DeRef(_4485);
        _4485 = NOVALUE;
        DeRef(_4481);
        _4481 = NOVALUE;
        DeRef(_4463);
        _4463 = NOVALUE;
        DeRef(_4465);
        _4465 = NOVALUE;
        DeRef(_4447);
        _4447 = NOVALUE;
        return _4508;
L7: 
    ;}    ;
}


int _25deserialize(int _sdata_8129, int _pos_8130)
{
    int _4512 = NOVALUE;
    int _4511 = NOVALUE;
    int _4510 = NOVALUE;
    int _4509 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(sdata) then*/
    _4509 = 1;
    if (_4509 == 0)
    {
        _4509 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _4509 = NOVALUE;
    }

    /** 		return deserialize_file(sdata, 0)*/
    _4510 = _25deserialize_file(_sdata_8129, 0);
    return _4510;
L1: 

    /** 	if atom(sdata) then*/
    _4511 = 1;
    if (_4511 == 0)
    {
        _4511 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _4511 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_4510);
    _4510 = NOVALUE;
    return 0;
L2: 

    /** 	return deserialize_object(sdata, pos, 0)*/
    _4512 = _25deserialize_object(_sdata_8129, _pos_8130, 0);
    DeRef(_4510);
    _4510 = NOVALUE;
    return _4512;
    ;
}


int _25serialize(int _x_8139)
{
    int _x4_8140 = NOVALUE;
    int _s_8141 = NOVALUE;
    int _atom_to_float32_inlined_atom_to_float32_at_192_8175 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_203_8178 = NOVALUE;
    int _atom_to_float64_inlined_atom_to_float64_at_229_8183 = NOVALUE;
    int _4551 = NOVALUE;
    int _4550 = NOVALUE;
    int _4549 = NOVALUE;
    int _4547 = NOVALUE;
    int _4546 = NOVALUE;
    int _4544 = NOVALUE;
    int _4542 = NOVALUE;
    int _4541 = NOVALUE;
    int _4540 = NOVALUE;
    int _4538 = NOVALUE;
    int _4537 = NOVALUE;
    int _4536 = NOVALUE;
    int _4535 = NOVALUE;
    int _4534 = NOVALUE;
    int _4533 = NOVALUE;
    int _4532 = NOVALUE;
    int _4531 = NOVALUE;
    int _4530 = NOVALUE;
    int _4528 = NOVALUE;
    int _4527 = NOVALUE;
    int _4526 = NOVALUE;
    int _4525 = NOVALUE;
    int _4524 = NOVALUE;
    int _4523 = NOVALUE;
    int _4521 = NOVALUE;
    int _4520 = NOVALUE;
    int _4519 = NOVALUE;
    int _4518 = NOVALUE;
    int _4517 = NOVALUE;
    int _4516 = NOVALUE;
    int _4515 = NOVALUE;
    int _4514 = NOVALUE;
    int _4513 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_8139))
    _4513 = 1;
    else if (IS_ATOM_DBL(_x_8139))
    _4513 = IS_ATOM_INT(DoubleToInt(_x_8139));
    else
    _4513 = 0;
    if (_4513 == 0)
    {
        _4513 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _4513 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_8139)) {
        _4514 = (_x_8139 >= -9);
    }
    else {
        _4514 = binary_op(GREATEREQ, _x_8139, -9);
    }
    if (IS_ATOM_INT(_4514)) {
        if (_4514 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_4514)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_8139)) {
        _4516 = (_x_8139 <= 239);
    }
    else {
        _4516 = binary_op(LESSEQ, _x_8139, 239);
    }
    if (_4516 == 0) {
        DeRef(_4516);
        _4516 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_4516) && DBL_PTR(_4516)->dbl == 0.0){
            DeRef(_4516);
            _4516 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_4516);
        _4516 = NOVALUE;
    }
    DeRef(_4516);
    _4516 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_8139)) {
        _4517 = _x_8139 - -9;
        if ((long)((unsigned long)_4517 +(unsigned long) HIGH_BITS) >= 0){
            _4517 = NewDouble((double)_4517);
        }
    }
    else {
        _4517 = binary_op(MINUS, _x_8139, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4517;
    _4518 = MAKE_SEQ(_1);
    _4517 = NOVALUE;
    DeRef(_x_8139);
    DeRefi(_x4_8140);
    DeRef(_s_8141);
    DeRef(_4514);
    _4514 = NOVALUE;
    return _4518;
    goto L3; // [41] 328
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_8139)) {
        _4519 = (_x_8139 >= _25MIN2B_7772);
    }
    else {
        _4519 = binary_op(GREATEREQ, _x_8139, _25MIN2B_7772);
    }
    if (IS_ATOM_INT(_4519)) {
        if (_4519 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_4519)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_8139)) {
        _4521 = (_x_8139 <= 32767);
    }
    else {
        _4521 = binary_op(LESSEQ, _x_8139, 32767);
    }
    if (_4521 == 0) {
        DeRef(_4521);
        _4521 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_4521) && DBL_PTR(_4521)->dbl == 0.0){
            DeRef(_4521);
            _4521 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_4521);
        _4521 = NOVALUE;
    }
    DeRef(_4521);
    _4521 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_8139;
    if (IS_ATOM_INT(_x_8139)) {
        _x_8139 = _x_8139 - _25MIN2B_7772;
        if ((long)((unsigned long)_x_8139 +(unsigned long) HIGH_BITS) >= 0){
            _x_8139 = NewDouble((double)_x_8139);
        }
    }
    else {
        _x_8139 = binary_op(MINUS, _x_8139, _25MIN2B_7772);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_8139)) {
        {unsigned long tu;
             tu = (unsigned long)_x_8139 & (unsigned long)255;
             _4523 = MAKE_UINT(tu);
        }
    }
    else {
        _4523 = binary_op(AND_BITS, _x_8139, 255);
    }
    if (IS_ATOM_INT(_x_8139)) {
        if (256 > 0 && _x_8139 >= 0) {
            _4524 = _x_8139 / 256;
        }
        else {
            temp_dbl = floor((double)_x_8139 / (double)256);
            if (_x_8139 != MININT)
            _4524 = (long)temp_dbl;
            else
            _4524 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_8139, 256);
        _4524 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _4523;
    *((int *)(_2+12)) = _4524;
    _4525 = MAKE_SEQ(_1);
    _4524 = NOVALUE;
    _4523 = NOVALUE;
    DeRef(_x_8139);
    DeRefi(_x4_8140);
    DeRef(_s_8141);
    DeRef(_4514);
    _4514 = NOVALUE;
    DeRef(_4518);
    _4518 = NOVALUE;
    DeRef(_4519);
    _4519 = NOVALUE;
    return _4525;
    goto L3; // [94] 328
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_8139)) {
        _4526 = (_x_8139 >= _25MIN3B_7778);
    }
    else {
        _4526 = binary_op(GREATEREQ, _x_8139, _25MIN3B_7778);
    }
    if (IS_ATOM_INT(_4526)) {
        if (_4526 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_4526)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_8139)) {
        _4528 = (_x_8139 <= 8388607);
    }
    else {
        _4528 = binary_op(LESSEQ, _x_8139, 8388607);
    }
    if (_4528 == 0) {
        DeRef(_4528);
        _4528 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_4528) && DBL_PTR(_4528)->dbl == 0.0){
            DeRef(_4528);
            _4528 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_4528);
        _4528 = NOVALUE;
    }
    DeRef(_4528);
    _4528 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_8139;
    if (IS_ATOM_INT(_x_8139)) {
        _x_8139 = _x_8139 - _25MIN3B_7778;
        if ((long)((unsigned long)_x_8139 +(unsigned long) HIGH_BITS) >= 0){
            _x_8139 = NewDouble((double)_x_8139);
        }
    }
    else {
        _x_8139 = binary_op(MINUS, _x_8139, _25MIN3B_7778);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_8139)) {
        {unsigned long tu;
             tu = (unsigned long)_x_8139 & (unsigned long)255;
             _4530 = MAKE_UINT(tu);
        }
    }
    else {
        _4530 = binary_op(AND_BITS, _x_8139, 255);
    }
    if (IS_ATOM_INT(_x_8139)) {
        if (256 > 0 && _x_8139 >= 0) {
            _4531 = _x_8139 / 256;
        }
        else {
            temp_dbl = floor((double)_x_8139 / (double)256);
            if (_x_8139 != MININT)
            _4531 = (long)temp_dbl;
            else
            _4531 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_8139, 256);
        _4531 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_4531)) {
        {unsigned long tu;
             tu = (unsigned long)_4531 & (unsigned long)255;
             _4532 = MAKE_UINT(tu);
        }
    }
    else {
        _4532 = binary_op(AND_BITS, _4531, 255);
    }
    DeRef(_4531);
    _4531 = NOVALUE;
    if (IS_ATOM_INT(_x_8139)) {
        if (65536 > 0 && _x_8139 >= 0) {
            _4533 = _x_8139 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_8139 / (double)65536);
            if (_x_8139 != MININT)
            _4533 = (long)temp_dbl;
            else
            _4533 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_8139, 65536);
        _4533 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _4530;
    *((int *)(_2+12)) = _4532;
    *((int *)(_2+16)) = _4533;
    _4534 = MAKE_SEQ(_1);
    _4533 = NOVALUE;
    _4532 = NOVALUE;
    _4530 = NOVALUE;
    DeRef(_x_8139);
    DeRefi(_x4_8140);
    DeRef(_s_8141);
    DeRef(_4514);
    _4514 = NOVALUE;
    DeRef(_4518);
    _4518 = NOVALUE;
    DeRef(_4519);
    _4519 = NOVALUE;
    DeRef(_4525);
    _4525 = NOVALUE;
    DeRef(_4526);
    _4526 = NOVALUE;
    return _4534;
    goto L3; // [156] 328
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_8139) && IS_ATOM_INT(_25MIN4B_7784)) {
        _4535 = _x_8139 - _25MIN4B_7784;
        if ((long)((unsigned long)_4535 +(unsigned long) HIGH_BITS) >= 0){
            _4535 = NewDouble((double)_4535);
        }
    }
    else {
        _4535 = binary_op(MINUS, _x_8139, _25MIN4B_7784);
    }
    _4536 = _13int_to_bytes(_4535);
    _4535 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_4536)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_4536)) {
        Prepend(&_4537, _4536, 251);
    }
    else {
        Concat((object_ptr)&_4537, 251, _4536);
    }
    DeRef(_4536);
    _4536 = NOVALUE;
    DeRef(_x_8139);
    DeRefi(_x4_8140);
    DeRef(_s_8141);
    DeRef(_4514);
    _4514 = NOVALUE;
    DeRef(_4518);
    _4518 = NOVALUE;
    DeRef(_4519);
    _4519 = NOVALUE;
    DeRef(_4525);
    _4525 = NOVALUE;
    DeRef(_4526);
    _4526 = NOVALUE;
    DeRef(_4534);
    _4534 = NOVALUE;
    return _4537;
    goto L3; // [180] 328
L1: 

    /** 	elsif atom(x) then*/
    _4538 = IS_ATOM(_x_8139);
    if (_4538 == 0)
    {
        _4538 = NOVALUE;
        goto L6; // [188] 249
    }
    else{
        _4538 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/

    /** 	return machine_func(M_A_TO_F32, a)*/
    DeRefi(_x4_8140);
    _x4_8140 = machine(48, _x_8139);

    /** 		if x = convert:float32_to_atom(x4) then*/

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    DeRef(_float32_to_atom_inlined_float32_to_atom_at_203_8178);
    _float32_to_atom_inlined_float32_to_atom_at_203_8178 = machine(49, _x4_8140);
    if (binary_op_a(NOTEQ, _x_8139, _float32_to_atom_inlined_float32_to_atom_at_203_8178)){
        goto L7; // [211] 228
    }

    /** 			return F4B & x4*/
    Prepend(&_4540, _x4_8140, 252);
    DeRef(_x_8139);
    DeRefDSi(_x4_8140);
    DeRef(_s_8141);
    DeRef(_4514);
    _4514 = NOVALUE;
    DeRef(_4518);
    _4518 = NOVALUE;
    DeRef(_4519);
    _4519 = NOVALUE;
    DeRef(_4525);
    _4525 = NOVALUE;
    DeRef(_4526);
    _4526 = NOVALUE;
    DeRef(_4534);
    _4534 = NOVALUE;
    DeRef(_4537);
    _4537 = NOVALUE;
    return _4540;
    goto L3; // [225] 328
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/

    /** 	return machine_func(M_A_TO_F64, a)*/
    DeRefi(_atom_to_float64_inlined_atom_to_float64_at_229_8183);
    _atom_to_float64_inlined_atom_to_float64_at_229_8183 = machine(46, _x_8139);
    Prepend(&_4541, _atom_to_float64_inlined_atom_to_float64_at_229_8183, 253);
    DeRef(_x_8139);
    DeRefi(_x4_8140);
    DeRef(_s_8141);
    DeRef(_4514);
    _4514 = NOVALUE;
    DeRef(_4518);
    _4518 = NOVALUE;
    DeRef(_4519);
    _4519 = NOVALUE;
    DeRef(_4525);
    _4525 = NOVALUE;
    DeRef(_4526);
    _4526 = NOVALUE;
    DeRef(_4534);
    _4534 = NOVALUE;
    DeRef(_4537);
    _4537 = NOVALUE;
    DeRef(_4540);
    _4540 = NOVALUE;
    return _4541;
    goto L3; // [246] 328
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_8139)){
            _4542 = SEQ_PTR(_x_8139)->length;
    }
    else {
        _4542 = 1;
    }
    if (_4542 > 255)
    goto L8; // [254] 270

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_8139)){
            _4544 = SEQ_PTR(_x_8139)->length;
    }
    else {
        _4544 = 1;
    }
    DeRef(_s_8141);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _4544;
    _s_8141 = MAKE_SEQ(_1);
    _4544 = NOVALUE;
    goto L9; // [267] 284
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_8139)){
            _4546 = SEQ_PTR(_x_8139)->length;
    }
    else {
        _4546 = 1;
    }
    _4547 = _13int_to_bytes(_4546);
    _4546 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_4547)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_4547)) {
        Prepend(&_s_8141, _4547, 255);
    }
    else {
        Concat((object_ptr)&_s_8141, 255, _4547);
    }
    DeRef(_4547);
    _4547 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_8139)){
            _4549 = SEQ_PTR(_x_8139)->length;
    }
    else {
        _4549 = 1;
    }
    {
        int _i_8196;
        _i_8196 = 1;
LA: 
        if (_i_8196 > _4549){
            goto LB; // [289] 319
        }

        /** 			s &= serialize(x[i])*/
        _2 = (int)SEQ_PTR(_x_8139);
        _4550 = (int)*(((s1_ptr)_2)->base + _i_8196);
        Ref(_4550);
        _4551 = _25serialize(_4550);
        _4550 = NOVALUE;
        if (IS_SEQUENCE(_s_8141) && IS_ATOM(_4551)) {
            Ref(_4551);
            Append(&_s_8141, _s_8141, _4551);
        }
        else if (IS_ATOM(_s_8141) && IS_SEQUENCE(_4551)) {
        }
        else {
            Concat((object_ptr)&_s_8141, _s_8141, _4551);
        }
        DeRef(_4551);
        _4551 = NOVALUE;

        /** 		end for*/
        _i_8196 = _i_8196 + 1;
        goto LA; // [314] 296
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_8139);
    DeRefi(_x4_8140);
    DeRef(_4514);
    _4514 = NOVALUE;
    DeRef(_4518);
    _4518 = NOVALUE;
    DeRef(_4519);
    _4519 = NOVALUE;
    DeRef(_4525);
    _4525 = NOVALUE;
    DeRef(_4526);
    _4526 = NOVALUE;
    DeRef(_4534);
    _4534 = NOVALUE;
    DeRef(_4537);
    _4537 = NOVALUE;
    DeRef(_4540);
    _4540 = NOVALUE;
    DeRef(_4541);
    _4541 = NOVALUE;
    return _s_8141;
L3: 
    ;
}



// 0x03CA903A
