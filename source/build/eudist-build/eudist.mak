CC     = gcc
CFLAGS =  -DEWINDOWS -fomit-frame-pointer -c -w -fsigned-char -O2 -m32 -Ic:/develop/offEu/euphoria -ffast-math
LINKER = gcc
LFLAGS = c:/develop/offEu/euphoria/source/build/eu.a -m32 
EUDIST_SOURCES = init-.c eudist.c main-.c cmdline.c console.c get.c io.c machine.c memconst.c memory.c dll.c types.c text.c convert.c search.c filesys.c datetime.c math.c rand.c sequence.c sort.c pretty.c serialize.c map.c eumem.c primes.c stats.c info.c graphcst.c regex.c flags.c
EUDIST_OBJECTS = init-.o eudist.o main-.o cmdline.o console.o get.o io.o machine.o memconst.o memory.o dll.o types.o text.o convert.o search.o filesys.o datetime.o math.o rand.o sequence.o sort.o pretty.o serialize.o map.o eumem.o primes.o stats.o info.o graphcst.o regex.o flags.o
EUDIST_GENERATED_FILES =  init-.c init-.o main-.h eudist.c eudist.o main-.c main-.o cmdline.c cmdline.o console.c console.o get.c get.o io.c io.o machine.c machine.o memconst.c memconst.o memory.c memory.o dll.c dll.o types.c types.o text.c text.o convert.c convert.o search.c search.o filesys.c filesys.o datetime.c datetime.o math.c math.o rand.c rand.o sequence.c sequence.o sort.c sort.o pretty.c pretty.o serialize.c serialize.o map.c map.o eumem.c eumem.o primes.c primes.o stats.c stats.o info.c info.o graphcst.c graphcst.o regex.c regex.o flags.c flags.o

c:/develop/offEu/euphoria/source/build/eudist: $(EUDIST_OBJECTS) c:/develop/offEu/euphoria/source/build/eu.a 
	$(LINKER) -o c:/develop/offEu/euphoria/source/build/eudist $(EUDIST_OBJECTS)  $(LFLAGS)

.PHONY: eudist-clean eudist-clean-all

eudist-clean:
	rm -rf $(EUDIST_OBJECTS) 

eudist-clean-all: eudist-clean
	rm -rf $(EUDIST_SOURCES)  c:/develop/offEu/euphoria/source/build/eudist

%.o: %.c
	$(CC) $(CFLAGS) $*.c -o $*.o

