// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _22sort(int _x_3906, int _order_3907)
{
    int _gap_3908 = NOVALUE;
    int _j_3909 = NOVALUE;
    int _first_3910 = NOVALUE;
    int _last_3911 = NOVALUE;
    int _tempi_3912 = NOVALUE;
    int _tempj_3913 = NOVALUE;
    int _1975 = NOVALUE;
    int _1971 = NOVALUE;
    int _1968 = NOVALUE;
    int _1964 = NOVALUE;
    int _1961 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if order >= 0 then*/
    if (_order_3907 < 0)
    goto L1; // [9] 23

    /** 		order = -1*/
    _order_3907 = -1;
    goto L2; // [20] 31
L1: 

    /** 		order = 1*/
    _order_3907 = 1;
L2: 

    /** 	last = length(x)*/
    if (IS_SEQUENCE(_x_3906)){
            _last_3911 = SEQ_PTR(_x_3906)->length;
    }
    else {
        _last_3911 = 1;
    }

    /** 	gap = floor(last / 10) + 1*/
    if (10 > 0 && _last_3911 >= 0) {
        _1961 = _last_3911 / 10;
    }
    else {
        temp_dbl = floor((double)_last_3911 / (double)10);
        _1961 = (long)temp_dbl;
    }
    _gap_3908 = _1961 + 1;
    _1961 = NOVALUE;

    /** 	while 1 do*/
L3: 

    /** 		first = gap + 1*/
    _first_3910 = _gap_3908 + 1;

    /** 		for i = first to last do*/
    _1964 = _last_3911;
    {
        int _i_3923;
        _i_3923 = _first_3910;
L4: 
        if (_i_3923 > _1964){
            goto L5; // [68] 170
        }

        /** 			tempi = x[i]*/
        DeRef(_tempi_3912);
        _2 = (int)SEQ_PTR(_x_3906);
        _tempi_3912 = (int)*(((s1_ptr)_2)->base + _i_3923);
        Ref(_tempi_3912);

        /** 			j = i - gap*/
        _j_3909 = _i_3923 - _gap_3908;

        /** 			while 1 do*/
L6: 

        /** 				tempj = x[j]*/
        DeRef(_tempj_3913);
        _2 = (int)SEQ_PTR(_x_3906);
        _tempj_3913 = (int)*(((s1_ptr)_2)->base + _j_3909);
        Ref(_tempj_3913);

        /** 				if eu:compare(tempi, tempj) != order then*/
        if (IS_ATOM_INT(_tempi_3912) && IS_ATOM_INT(_tempj_3913)){
            _1968 = (_tempi_3912 < _tempj_3913) ? -1 : (_tempi_3912 > _tempj_3913);
        }
        else{
            _1968 = compare(_tempi_3912, _tempj_3913);
        }
        if (_1968 == _order_3907)
        goto L7; // [106] 123

        /** 					j += gap*/
        _j_3909 = _j_3909 + _gap_3908;

        /** 					exit*/
        goto L8; // [120] 157
L7: 

        /** 				x[j+gap] = tempj*/
        _1971 = _j_3909 + _gap_3908;
        Ref(_tempj_3913);
        _2 = (int)SEQ_PTR(_x_3906);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_3906 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _1971);
        _1 = *(int *)_2;
        *(int *)_2 = _tempj_3913;
        DeRef(_1);

        /** 				if j <= gap then*/
        if (_j_3909 > _gap_3908)
        goto L9; // [135] 144

        /** 					exit*/
        goto L8; // [141] 157
L9: 

        /** 				j -= gap*/
        _j_3909 = _j_3909 - _gap_3908;

        /** 			end while*/
        goto L6; // [154] 94
L8: 

        /** 			x[j] = tempi*/
        Ref(_tempi_3912);
        _2 = (int)SEQ_PTR(_x_3906);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_3906 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _j_3909);
        _1 = *(int *)_2;
        *(int *)_2 = _tempi_3912;
        DeRef(_1);

        /** 		end for*/
        _i_3923 = _i_3923 + 1;
        goto L4; // [165] 75
L5: 
        ;
    }

    /** 		if gap = 1 then*/
    if (_gap_3908 != 1)
    goto LA; // [172] 185

    /** 			return x*/
    DeRef(_tempi_3912);
    DeRef(_tempj_3913);
    DeRef(_1971);
    _1971 = NOVALUE;
    return _x_3906;
    goto L3; // [182] 55
LA: 

    /** 			gap = floor(gap / 7) + 1*/
    if (7 > 0 && _gap_3908 >= 0) {
        _1975 = _gap_3908 / 7;
    }
    else {
        temp_dbl = floor((double)_gap_3908 / (double)7);
        _1975 = (long)temp_dbl;
    }
    _gap_3908 = _1975 + 1;
    _1975 = NOVALUE;

    /** 	end while*/
    goto L3; // [200] 55
    ;
}


int _22column_compare(int _a_3999, int _b_4000, int _cols_4001)
{
    int _sign_4002 = NOVALUE;
    int _column_4003 = NOVALUE;
    int _2026 = NOVALUE;
    int _2024 = NOVALUE;
    int _2023 = NOVALUE;
    int _2022 = NOVALUE;
    int _2021 = NOVALUE;
    int _2020 = NOVALUE;
    int _2019 = NOVALUE;
    int _2017 = NOVALUE;
    int _2016 = NOVALUE;
    int _2015 = NOVALUE;
    int _2013 = NOVALUE;
    int _2011 = NOVALUE;
    int _2008 = NOVALUE;
    int _2006 = NOVALUE;
    int _2005 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(cols) do*/
    if (IS_SEQUENCE(_cols_4001)){
            _2005 = SEQ_PTR(_cols_4001)->length;
    }
    else {
        _2005 = 1;
    }
    {
        int _i_4005;
        _i_4005 = 1;
L1: 
        if (_i_4005 > _2005){
            goto L2; // [6] 184
        }

        /** 		if cols[i] < 0 then*/
        _2 = (int)SEQ_PTR(_cols_4001);
        _2006 = (int)*(((s1_ptr)_2)->base + _i_4005);
        if (binary_op_a(GREATEREQ, _2006, 0)){
            _2006 = NOVALUE;
            goto L3; // [19] 46
        }
        _2006 = NOVALUE;

        /** 			sign = -1*/
        _sign_4002 = -1;

        /** 			column = -cols[i]*/
        _2 = (int)SEQ_PTR(_cols_4001);
        _2008 = (int)*(((s1_ptr)_2)->base + _i_4005);
        if (IS_ATOM_INT(_2008)) {
            if ((unsigned long)_2008 == 0xC0000000)
            _column_4003 = (int)NewDouble((double)-0xC0000000);
            else
            _column_4003 = - _2008;
        }
        else {
            _column_4003 = unary_op(UMINUS, _2008);
        }
        _2008 = NOVALUE;
        if (!IS_ATOM_INT(_column_4003)) {
            _1 = (long)(DBL_PTR(_column_4003)->dbl);
            if (UNIQUE(DBL_PTR(_column_4003)) && (DBL_PTR(_column_4003)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_column_4003);
            _column_4003 = _1;
        }
        goto L4; // [43] 64
L3: 

        /** 			sign = 1*/
        _sign_4002 = 1;

        /** 			column = cols[i]*/
        _2 = (int)SEQ_PTR(_cols_4001);
        _column_4003 = (int)*(((s1_ptr)_2)->base + _i_4005);
        if (!IS_ATOM_INT(_column_4003)){
            _column_4003 = (long)DBL_PTR(_column_4003)->dbl;
        }
L4: 

        /** 		if column <= length(a) then*/
        if (IS_SEQUENCE(_a_3999)){
                _2011 = SEQ_PTR(_a_3999)->length;
        }
        else {
            _2011 = 1;
        }
        if (_column_4003 > _2011)
        goto L5; // [71] 145

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_4000)){
                _2013 = SEQ_PTR(_b_4000)->length;
        }
        else {
            _2013 = 1;
        }
        if (_column_4003 > _2013)
        goto L6; // [80] 129

        /** 				if not equal(a[column], b[column]) then*/
        _2 = (int)SEQ_PTR(_a_3999);
        _2015 = (int)*(((s1_ptr)_2)->base + _column_4003);
        _2 = (int)SEQ_PTR(_b_4000);
        _2016 = (int)*(((s1_ptr)_2)->base + _column_4003);
        if (_2015 == _2016)
        _2017 = 1;
        else if (IS_ATOM_INT(_2015) && IS_ATOM_INT(_2016))
        _2017 = 0;
        else
        _2017 = (compare(_2015, _2016) == 0);
        _2015 = NOVALUE;
        _2016 = NOVALUE;
        if (_2017 != 0)
        goto L7; // [98] 177
        _2017 = NOVALUE;

        /** 					return sign * eu:compare(a[column], b[column])*/
        _2 = (int)SEQ_PTR(_a_3999);
        _2019 = (int)*(((s1_ptr)_2)->base + _column_4003);
        _2 = (int)SEQ_PTR(_b_4000);
        _2020 = (int)*(((s1_ptr)_2)->base + _column_4003);
        if (IS_ATOM_INT(_2019) && IS_ATOM_INT(_2020)){
            _2021 = (_2019 < _2020) ? -1 : (_2019 > _2020);
        }
        else{
            _2021 = compare(_2019, _2020);
        }
        _2019 = NOVALUE;
        _2020 = NOVALUE;
        if (_sign_4002 == (short)_sign_4002)
        _2022 = _sign_4002 * _2021;
        else
        _2022 = NewDouble(_sign_4002 * (double)_2021);
        _2021 = NOVALUE;
        DeRef(_a_3999);
        DeRef(_b_4000);
        DeRef(_cols_4001);
        return _2022;
        goto L7; // [126] 177
L6: 

        /** 				return sign * -1*/
        if (_sign_4002 == (short)_sign_4002)
        _2023 = _sign_4002 * -1;
        else
        _2023 = NewDouble(_sign_4002 * (double)-1);
        DeRef(_a_3999);
        DeRef(_b_4000);
        DeRef(_cols_4001);
        DeRef(_2022);
        _2022 = NOVALUE;
        return _2023;
        goto L7; // [142] 177
L5: 

        /** 			if column <= length(b) then*/
        if (IS_SEQUENCE(_b_4000)){
                _2024 = SEQ_PTR(_b_4000)->length;
        }
        else {
            _2024 = 1;
        }
        if (_column_4003 > _2024)
        goto L8; // [150] 169

        /** 				return sign * 1*/
        _2026 = _sign_4002 * 1;
        DeRef(_a_3999);
        DeRef(_b_4000);
        DeRef(_cols_4001);
        DeRef(_2022);
        _2022 = NOVALUE;
        DeRef(_2023);
        _2023 = NOVALUE;
        return _2026;
        goto L9; // [166] 176
L8: 

        /** 				return 0*/
        DeRef(_a_3999);
        DeRef(_b_4000);
        DeRef(_cols_4001);
        DeRef(_2022);
        _2022 = NOVALUE;
        DeRef(_2023);
        _2023 = NOVALUE;
        DeRef(_2026);
        _2026 = NOVALUE;
        return 0;
L9: 
L7: 

        /** 	end for*/
        _i_4005 = _i_4005 + 1;
        goto L1; // [179] 13
L2: 
        ;
    }

    /** 	return 0*/
    DeRef(_a_3999);
    DeRef(_b_4000);
    DeRef(_cols_4001);
    DeRef(_2022);
    _2022 = NOVALUE;
    DeRef(_2023);
    _2023 = NOVALUE;
    DeRef(_2026);
    _2026 = NOVALUE;
    return 0;
    ;
}



// 0xD03BEE81
