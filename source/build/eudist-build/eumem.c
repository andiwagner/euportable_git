// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _27malloc(int _mem_struct_p_10818, int _cleanup_p_10819)
{
    int _temp__10820 = NOVALUE;
    int _6138 = NOVALUE;
    int _6136 = NOVALUE;
    int _6135 = NOVALUE;
    int _6134 = NOVALUE;
    int _6130 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_cleanup_p_10819)) {
        _1 = (long)(DBL_PTR(_cleanup_p_10819)->dbl);
        if (UNIQUE(DBL_PTR(_cleanup_p_10819)) && (DBL_PTR(_cleanup_p_10819)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_cleanup_p_10819);
        _cleanup_p_10819 = _1;
    }

    /** 	if atom(mem_struct_p) then*/
    _6130 = IS_ATOM(_mem_struct_p_10818);
    if (_6130 == 0)
    {
        _6130 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _6130 = NOVALUE;
    }

    /** 		mem_struct_p = repeat(0, mem_struct_p)*/
    _0 = _mem_struct_p_10818;
    _mem_struct_p_10818 = Repeat(0, _mem_struct_p_10818);
    DeRef(_0);
L1: 

    /** 	if ram_free_list = 0 then*/
    if (_27ram_free_list_10814 != 0)
    goto L2; // [24] 74

    /** 		ram_space = append(ram_space, mem_struct_p)*/
    Ref(_mem_struct_p_10818);
    Append(&_27ram_space_10813, _27ram_space_10813, _mem_struct_p_10818);

    /** 		if cleanup_p then*/
    if (_cleanup_p_10819 == 0)
    {
        goto L3; // [38] 61
    }
    else{
    }

    /** 			return delete_routine( length(ram_space), free_rid )*/
    if (IS_SEQUENCE(_27ram_space_10813)){
            _6134 = SEQ_PTR(_27ram_space_10813)->length;
    }
    else {
        _6134 = 1;
    }
    _6135 = NewDouble( (double) _6134 );
    _1 = (int) _00[_27free_rid_10815].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_27free_rid_10815].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _27free_rid_10815;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6135)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6135)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6135)) ){
        DeRefDS(_6135);
        _6135 = NewDouble( DBL_PTR(_6135)->dbl );
    }
    DBL_PTR(_6135)->cleanup = (cleanup_ptr)_1;
    _6134 = NOVALUE;
    DeRef(_mem_struct_p_10818);
    return _6135;
    goto L4; // [58] 73
L3: 

    /** 			return length(ram_space)*/
    if (IS_SEQUENCE(_27ram_space_10813)){
            _6136 = SEQ_PTR(_27ram_space_10813)->length;
    }
    else {
        _6136 = 1;
    }
    DeRef(_mem_struct_p_10818);
    DeRef(_6135);
    _6135 = NOVALUE;
    return _6136;
L4: 
L2: 

    /** 	temp_ = ram_free_list*/
    _temp__10820 = _27ram_free_list_10814;

    /** 	ram_free_list = ram_space[temp_]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _27ram_free_list_10814 = (int)*(((s1_ptr)_2)->base + _temp__10820);
    if (!IS_ATOM_INT(_27ram_free_list_10814))
    _27ram_free_list_10814 = (long)DBL_PTR(_27ram_free_list_10814)->dbl;

    /** 	ram_space[temp_] = mem_struct_p*/
    Ref(_mem_struct_p_10818);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp__10820);
    _1 = *(int *)_2;
    *(int *)_2 = _mem_struct_p_10818;
    DeRef(_1);

    /** 	if cleanup_p then*/
    if (_cleanup_p_10819 == 0)
    {
        goto L5; // [103] 121
    }
    else{
    }

    /** 		return delete_routine( temp_, free_rid )*/
    _6138 = NewDouble( (double) _temp__10820 );
    _1 = (int) _00[_27free_rid_10815].cleanup;
    if( _1 == 0 ){
        _1 = (int) TransAlloc( sizeof(struct cleanup) );
        _00[_27free_rid_10815].cleanup = (cleanup_ptr)_1;
    }
    ((cleanup_ptr)_1)->type = CLEAN_UDT_RT;
    ((cleanup_ptr)_1)->func.rid = _27free_rid_10815;
    ((cleanup_ptr)_1)->next = 0;
    if(DBL_PTR(_6138)->cleanup != 0 ){
        _1 = (int) ChainDeleteRoutine( (cleanup_ptr)_1, DBL_PTR(_6138)->cleanup );
    }
    else if( !UNIQUE(DBL_PTR(_6138)) ){
        DeRefDS(_6138);
        _6138 = NewDouble( DBL_PTR(_6138)->dbl );
    }
    DBL_PTR(_6138)->cleanup = (cleanup_ptr)_1;
    DeRef(_mem_struct_p_10818);
    DeRef(_6135);
    _6135 = NOVALUE;
    return _6138;
    goto L6; // [118] 128
L5: 

    /** 		return temp_*/
    DeRef(_mem_struct_p_10818);
    DeRef(_6135);
    _6135 = NOVALUE;
    DeRef(_6138);
    _6138 = NOVALUE;
    return _temp__10820;
L6: 
    ;
}


void _27free(int _mem_p_10838)
{
    int _6140 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if mem_p < 1 then return end if*/
    if (binary_op_a(GREATEREQ, _mem_p_10838, 1)){
        goto L1; // [3] 11
    }
    DeRef(_mem_p_10838);
    return;
L1: 

    /** 	if mem_p > length(ram_space) then return end if*/
    if (IS_SEQUENCE(_27ram_space_10813)){
            _6140 = SEQ_PTR(_27ram_space_10813)->length;
    }
    else {
        _6140 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_10838, _6140)){
        _6140 = NOVALUE;
        goto L2; // [18] 26
    }
    _6140 = NOVALUE;
    DeRef(_mem_p_10838);
    return;
L2: 

    /** 	ram_space[mem_p] = ram_free_list*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_mem_p_10838))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_10838)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _mem_p_10838);
    _1 = *(int *)_2;
    *(int *)_2 = _27ram_free_list_10814;
    DeRef(_1);

    /** 	ram_free_list = floor(mem_p)*/
    if (IS_ATOM_INT(_mem_p_10838))
    _27ram_free_list_10814 = e_floor(_mem_p_10838);
    else
    _27ram_free_list_10814 = unary_op(FLOOR, _mem_p_10838);
    if (!IS_ATOM_INT(_27ram_free_list_10814)) {
        _1 = (long)(DBL_PTR(_27ram_free_list_10814)->dbl);
        if (UNIQUE(DBL_PTR(_27ram_free_list_10814)) && (DBL_PTR(_27ram_free_list_10814)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_27ram_free_list_10814);
        _27ram_free_list_10814 = _1;
    }

    /** end procedure*/
    DeRef(_mem_p_10838);
    return;
    ;
}


int _27valid(int _mem_p_10848, int _mem_struct_p_10849)
{
    int _6154 = NOVALUE;
    int _6153 = NOVALUE;
    int _6151 = NOVALUE;
    int _6150 = NOVALUE;
    int _6149 = NOVALUE;
    int _6147 = NOVALUE;
    int _6144 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(mem_p) then return 0 end if*/
    if (IS_ATOM_INT(_mem_p_10848))
    _6144 = 1;
    else if (IS_ATOM_DBL(_mem_p_10848))
    _6144 = IS_ATOM_INT(DoubleToInt(_mem_p_10848));
    else
    _6144 = 0;
    if (_6144 != 0)
    goto L1; // [6] 14
    _6144 = NOVALUE;
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    return 0;
L1: 

    /** 	if mem_p < 1 then return 0 end if*/
    if (binary_op_a(GREATEREQ, _mem_p_10848, 1)){
        goto L2; // [16] 25
    }
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    return 0;
L2: 

    /** 	if mem_p > length(ram_space) then return 0 end if*/
    if (IS_SEQUENCE(_27ram_space_10813)){
            _6147 = SEQ_PTR(_27ram_space_10813)->length;
    }
    else {
        _6147 = 1;
    }
    if (binary_op_a(LESSEQ, _mem_p_10848, _6147)){
        _6147 = NOVALUE;
        goto L3; // [32] 41
    }
    _6147 = NOVALUE;
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    return 0;
L3: 

    /** 	if sequence(mem_struct_p) then return 1 end if*/
    _6149 = IS_SEQUENCE(_mem_struct_p_10849);
    if (_6149 == 0)
    {
        _6149 = NOVALUE;
        goto L4; // [46] 54
    }
    else{
        _6149 = NOVALUE;
    }
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    return 1;
L4: 

    /** 	if atom(ram_space[mem_p]) then */
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_mem_p_10848)){
        _6150 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_10848)->dbl));
    }
    else{
        _6150 = (int)*(((s1_ptr)_2)->base + _mem_p_10848);
    }
    _6151 = IS_ATOM(_6150);
    _6150 = NOVALUE;
    if (_6151 == 0)
    {
        _6151 = NOVALUE;
        goto L5; // [65] 88
    }
    else{
        _6151 = NOVALUE;
    }

    /** 		if mem_struct_p >= 0 then*/
    if (binary_op_a(LESS, _mem_struct_p_10849, 0)){
        goto L6; // [70] 81
    }

    /** 			return 0*/
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    return 0;
L6: 

    /** 		return 1*/
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    return 1;
L5: 

    /** 	if length(ram_space[mem_p]) != mem_struct_p then*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_mem_p_10848)){
        _6153 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_mem_p_10848)->dbl));
    }
    else{
        _6153 = (int)*(((s1_ptr)_2)->base + _mem_p_10848);
    }
    if (IS_SEQUENCE(_6153)){
            _6154 = SEQ_PTR(_6153)->length;
    }
    else {
        _6154 = 1;
    }
    _6153 = NOVALUE;
    if (binary_op_a(EQUALS, _6154, _mem_struct_p_10849)){
        _6154 = NOVALUE;
        goto L7; // [99] 110
    }
    _6154 = NOVALUE;

    /** 		return 0*/
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    _6153 = NOVALUE;
    return 0;
L7: 

    /** 	return 1*/
    DeRef(_mem_p_10848);
    DeRef(_mem_struct_p_10849);
    _6153 = NOVALUE;
    return 1;
    ;
}



// 0x537547C1
