// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _29massage(int _data_set_11111, int _subseq_opt_11112)
{
    int _6292 = NOVALUE;
    int _6291 = NOVALUE;
    int _0, _1, _2;
    

    /** 	switch subseq_opt do*/
    _0 = _subseq_opt_11112;
    switch ( _0 ){ 

        /** 		case ST_IGNSTR then*/
        case 2:

        /** 			return stdseq:remove_subseq(data_set, stdseq:SEQ_NOALT)*/
        RefDS(_data_set_11111);
        RefDS(_21SEQ_NOALT_5668);
        _6291 = _21remove_subseq(_data_set_11111, _21SEQ_NOALT_5668);
        DeRefDS(_data_set_11111);
        return _6291;
        goto L1; // [27] 57

        /** 		case ST_ZEROSTR then*/
        case 3:

        /** 			return stdseq:remove_subseq(data_set, 0)*/
        RefDS(_data_set_11111);
        _6292 = _21remove_subseq(_data_set_11111, 0);
        DeRefDS(_data_set_11111);
        DeRef(_6291);
        _6291 = NOVALUE;
        return _6292;
        goto L1; // [44] 57

        /** 		case else*/
        default:

        /** 			return data_set*/
        DeRef(_6291);
        _6291 = NOVALUE;
        DeRef(_6292);
        _6292 = NOVALUE;
        return _data_set_11111;
    ;}L1: 
    ;
}


int _29stdev(int _data_set_11122, int _subseq_opt_11123, int _population_type_11124)
{
    int _lSum_11125 = NOVALUE;
    int _lMean_11126 = NOVALUE;
    int _lCnt_11127 = NOVALUE;
    int _6309 = NOVALUE;
    int _6308 = NOVALUE;
    int _6304 = NOVALUE;
    int _6303 = NOVALUE;
    int _6302 = NOVALUE;
    int _6301 = NOVALUE;
    int _6298 = NOVALUE;
    int _6297 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data_set = massage(data_set, subseq_opt)*/
    RefDS(_data_set_11122);
    _0 = _data_set_11122;
    _data_set_11122 = _29massage(_data_set_11122, 1);
    DeRefDSi(_0);

    /** 	lCnt = length(data_set)*/
    if (IS_SEQUENCE(_data_set_11122)){
            _lCnt_11127 = SEQ_PTR(_data_set_11122)->length;
    }
    else {
        _lCnt_11127 = 1;
    }

    /** 	if lCnt = 0 then*/
    if (_lCnt_11127 != 0)
    goto L1; // [25] 36

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_data_set_11122);
    DeRef(_lSum_11125);
    DeRef(_lMean_11126);
    return _5;
L1: 

    /** 	if lCnt = 1 then*/
    if (_lCnt_11127 != 1)
    goto L2; // [38] 49

    /** 		return 0*/
    DeRefDS(_data_set_11122);
    DeRef(_lSum_11125);
    DeRef(_lMean_11126);
    return 0;
L2: 

    /** 	lSum = 0*/
    DeRef(_lSum_11125);
    _lSum_11125 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_11122)){
            _6297 = SEQ_PTR(_data_set_11122)->length;
    }
    else {
        _6297 = 1;
    }
    {
        int _i_11135;
        _i_11135 = 1;
L3: 
        if (_i_11135 > _6297){
            goto L4; // [59] 83
        }

        /** 		lSum += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_11122);
        _6298 = (int)*(((s1_ptr)_2)->base + _i_11135);
        _0 = _lSum_11125;
        if (IS_ATOM_INT(_lSum_11125) && IS_ATOM_INT(_6298)) {
            _lSum_11125 = _lSum_11125 + _6298;
            if ((long)((unsigned long)_lSum_11125 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_11125 = NewDouble((double)_lSum_11125);
        }
        else {
            _lSum_11125 = binary_op(PLUS, _lSum_11125, _6298);
        }
        DeRef(_0);
        _6298 = NOVALUE;

        /** 	end for*/
        _i_11135 = _i_11135 + 1;
        goto L3; // [78] 66
L4: 
        ;
    }

    /** 	lMean = lSum / lCnt*/
    DeRef(_lMean_11126);
    if (IS_ATOM_INT(_lSum_11125)) {
        _lMean_11126 = (_lSum_11125 % _lCnt_11127) ? NewDouble((double)_lSum_11125 / _lCnt_11127) : (_lSum_11125 / _lCnt_11127);
    }
    else {
        _lMean_11126 = NewDouble(DBL_PTR(_lSum_11125)->dbl / (double)_lCnt_11127);
    }

    /** 	lSum = 0*/
    DeRef(_lSum_11125);
    _lSum_11125 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_11122)){
            _6301 = SEQ_PTR(_data_set_11122)->length;
    }
    else {
        _6301 = 1;
    }
    {
        int _i_11141;
        _i_11141 = 1;
L5: 
        if (_i_11141 > _6301){
            goto L6; // [99] 131
        }

        /** 		lSum += power(data_set[i] - lMean, 2)*/
        _2 = (int)SEQ_PTR(_data_set_11122);
        _6302 = (int)*(((s1_ptr)_2)->base + _i_11141);
        if (IS_ATOM_INT(_6302) && IS_ATOM_INT(_lMean_11126)) {
            _6303 = _6302 - _lMean_11126;
            if ((long)((unsigned long)_6303 +(unsigned long) HIGH_BITS) >= 0){
                _6303 = NewDouble((double)_6303);
            }
        }
        else {
            _6303 = binary_op(MINUS, _6302, _lMean_11126);
        }
        _6302 = NOVALUE;
        if (IS_ATOM_INT(_6303) && IS_ATOM_INT(_6303)) {
            if (_6303 == (short)_6303 && _6303 <= INT15 && _6303 >= -INT15)
            _6304 = _6303 * _6303;
            else
            _6304 = NewDouble(_6303 * (double)_6303);
        }
        else {
            _6304 = binary_op(MULTIPLY, _6303, _6303);
        }
        DeRef(_6303);
        _6303 = NOVALUE;
        _6303 = NOVALUE;
        _0 = _lSum_11125;
        if (IS_ATOM_INT(_lSum_11125) && IS_ATOM_INT(_6304)) {
            _lSum_11125 = _lSum_11125 + _6304;
            if ((long)((unsigned long)_lSum_11125 + (unsigned long)HIGH_BITS) >= 0) 
            _lSum_11125 = NewDouble((double)_lSum_11125);
        }
        else {
            _lSum_11125 = binary_op(PLUS, _lSum_11125, _6304);
        }
        DeRef(_0);
        DeRef(_6304);
        _6304 = NOVALUE;

        /** 	end for*/
        _i_11141 = _i_11141 + 1;
        goto L5; // [126] 106
L6: 
        ;
    }

    /** 	if population_type = ST_SAMPLE then*/
    if (_population_type_11124 != 2)
    goto L7; // [135] 148

    /** 		lCnt -= 1*/
    _lCnt_11127 = _lCnt_11127 - 1;
L7: 

    /** 	return power(lSum / lCnt, 0.5)*/
    if (IS_ATOM_INT(_lSum_11125)) {
        _6308 = (_lSum_11125 % _lCnt_11127) ? NewDouble((double)_lSum_11125 / _lCnt_11127) : (_lSum_11125 / _lCnt_11127);
    }
    else {
        _6308 = NewDouble(DBL_PTR(_lSum_11125)->dbl / (double)_lCnt_11127);
    }
    if (IS_ATOM_INT(_6308)) {
        temp_d.dbl = (double)_6308;
        _6309 = Dpower(&temp_d, DBL_PTR(_1715));
    }
    else {
        _6309 = Dpower(DBL_PTR(_6308), DBL_PTR(_1715));
    }
    DeRef(_6308);
    _6308 = NOVALUE;
    DeRefDS(_data_set_11122);
    DeRef(_lSum_11125);
    DeRef(_lMean_11126);
    return _6309;
    ;
}


int _29sum(int _data_set_11191, int _subseq_opt_11192)
{
    int _result__11193 = NOVALUE;
    int _6333 = NOVALUE;
    int _6332 = NOVALUE;
    int _6330 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _6330 = IS_ATOM(_data_set_11191);
    if (_6330 == 0)
    {
        _6330 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _6330 = NOVALUE;
    }

    /** 		return data_set*/
    DeRef(_result__11193);
    return _data_set_11191;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_11191);
    _0 = _data_set_11191;
    _data_set_11191 = _29massage(_data_set_11191, _subseq_opt_11192);
    DeRef(_0);

    /** 	result_ = 0*/
    DeRef(_result__11193);
    _result__11193 = 0;

    /** 	for i = 1 to length(data_set) do*/
    if (IS_SEQUENCE(_data_set_11191)){
            _6332 = SEQ_PTR(_data_set_11191)->length;
    }
    else {
        _6332 = 1;
    }
    {
        int _i_11198;
        _i_11198 = 1;
L2: 
        if (_i_11198 > _6332){
            goto L3; // [33] 57
        }

        /** 		result_ += data_set[i]*/
        _2 = (int)SEQ_PTR(_data_set_11191);
        _6333 = (int)*(((s1_ptr)_2)->base + _i_11198);
        _0 = _result__11193;
        if (IS_ATOM_INT(_result__11193) && IS_ATOM_INT(_6333)) {
            _result__11193 = _result__11193 + _6333;
            if ((long)((unsigned long)_result__11193 + (unsigned long)HIGH_BITS) >= 0) 
            _result__11193 = NewDouble((double)_result__11193);
        }
        else {
            _result__11193 = binary_op(PLUS, _result__11193, _6333);
        }
        DeRef(_0);
        _6333 = NOVALUE;

        /** 	end for*/
        _i_11198 = _i_11198 + 1;
        goto L2; // [52] 40
L3: 
        ;
    }

    /** 	return result_*/
    DeRef(_data_set_11191);
    return _result__11193;
    ;
}


int _29average(int _data_set_11212, int _subseq_opt_11213)
{
    int _6344 = NOVALUE;
    int _6343 = NOVALUE;
    int _6342 = NOVALUE;
    int _6340 = NOVALUE;
    int _6338 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(data_set) then*/
    _6338 = 0;
    if (_6338 == 0)
    {
        _6338 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _6338 = NOVALUE;
    }

    /** 		return data_set*/
    return _data_set_11212;
L1: 

    /** 	data_set = massage(data_set, subseq_opt)*/
    Ref(_data_set_11212);
    _0 = _data_set_11212;
    _data_set_11212 = _29massage(_data_set_11212, _subseq_opt_11213);
    DeRef(_0);

    /** 	if length(data_set) = 0 then*/
    if (IS_SEQUENCE(_data_set_11212)){
            _6340 = SEQ_PTR(_data_set_11212)->length;
    }
    else {
        _6340 = 1;
    }
    if (_6340 != 0)
    goto L2; // [28] 39

    /** 		return {}*/
    RefDS(_5);
    DeRef(_data_set_11212);
    return _5;
L2: 

    /** 	return sum(data_set) / length(data_set)*/
    Ref(_data_set_11212);
    _6342 = _29sum(_data_set_11212, 1);
    if (IS_SEQUENCE(_data_set_11212)){
            _6343 = SEQ_PTR(_data_set_11212)->length;
    }
    else {
        _6343 = 1;
    }
    if (IS_ATOM_INT(_6342)) {
        _6344 = (_6342 % _6343) ? NewDouble((double)_6342 / _6343) : (_6342 / _6343);
    }
    else {
        _6344 = binary_op(DIVIDE, _6342, _6343);
    }
    DeRef(_6342);
    _6342 = NOVALUE;
    _6343 = NOVALUE;
    DeRef(_data_set_11212);
    return _6344;
    ;
}



// 0xB41C14BF
