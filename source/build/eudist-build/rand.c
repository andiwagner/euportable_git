// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _19rand_range(int _lo_3107, int _hi_3108)
{
    int _temp_3111 = NOVALUE;
    int _1572 = NOVALUE;
    int _1570 = NOVALUE;
    int _1567 = NOVALUE;
    int _1566 = NOVALUE;
    int _1565 = NOVALUE;
    int _1564 = NOVALUE;
    int _1562 = NOVALUE;
    int _1561 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if lo > hi then*/
    if (binary_op_a(LESSEQ, _lo_3107, _hi_3108)){
        goto L1; // [3] 23
    }

    /** 		atom temp = hi*/
    Ref(_hi_3108);
    DeRef(_temp_3111);
    _temp_3111 = _hi_3108;

    /** 		hi = lo*/
    Ref(_lo_3107);
    DeRef(_hi_3108);
    _hi_3108 = _lo_3107;

    /** 		lo = temp*/
    Ref(_temp_3111);
    DeRef(_lo_3107);
    _lo_3107 = _temp_3111;
L1: 
    DeRef(_temp_3111);
    _temp_3111 = NOVALUE;

    /** 	if not integer(lo) or not integer(hi) then*/
    if (IS_ATOM_INT(_lo_3107))
    _1561 = 1;
    else if (IS_ATOM_DBL(_lo_3107))
    _1561 = IS_ATOM_INT(DoubleToInt(_lo_3107));
    else
    _1561 = 0;
    _1562 = (_1561 == 0);
    _1561 = NOVALUE;
    if (_1562 != 0) {
        goto L2; // [33] 48
    }
    if (IS_ATOM_INT(_hi_3108))
    _1564 = 1;
    else if (IS_ATOM_DBL(_hi_3108))
    _1564 = IS_ATOM_INT(DoubleToInt(_hi_3108));
    else
    _1564 = 0;
    _1565 = (_1564 == 0);
    _1564 = NOVALUE;
    if (_1565 == 0)
    {
        DeRef(_1565);
        _1565 = NOVALUE;
        goto L3; // [44] 64
    }
    else{
        DeRef(_1565);
        _1565 = NOVALUE;
    }
L2: 

    /**    		hi = rnd() * (hi - lo)*/
    _1566 = _19rnd();
    if (IS_ATOM_INT(_hi_3108) && IS_ATOM_INT(_lo_3107)) {
        _1567 = _hi_3108 - _lo_3107;
        if ((long)((unsigned long)_1567 +(unsigned long) HIGH_BITS) >= 0){
            _1567 = NewDouble((double)_1567);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_3108)) {
            _1567 = NewDouble((double)_hi_3108 - DBL_PTR(_lo_3107)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_3107)) {
                _1567 = NewDouble(DBL_PTR(_hi_3108)->dbl - (double)_lo_3107);
            }
            else
            _1567 = NewDouble(DBL_PTR(_hi_3108)->dbl - DBL_PTR(_lo_3107)->dbl);
        }
    }
    DeRef(_hi_3108);
    if (IS_ATOM_INT(_1566) && IS_ATOM_INT(_1567)) {
        if (_1566 == (short)_1566 && _1567 <= INT15 && _1567 >= -INT15)
        _hi_3108 = _1566 * _1567;
        else
        _hi_3108 = NewDouble(_1566 * (double)_1567);
    }
    else {
        _hi_3108 = binary_op(MULTIPLY, _1566, _1567);
    }
    DeRef(_1566);
    _1566 = NOVALUE;
    DeRef(_1567);
    _1567 = NOVALUE;
    goto L4; // [61] 80
L3: 

    /** 		lo -= 1*/
    _0 = _lo_3107;
    if (IS_ATOM_INT(_lo_3107)) {
        _lo_3107 = _lo_3107 - 1;
        if ((long)((unsigned long)_lo_3107 +(unsigned long) HIGH_BITS) >= 0){
            _lo_3107 = NewDouble((double)_lo_3107);
        }
    }
    else {
        _lo_3107 = NewDouble(DBL_PTR(_lo_3107)->dbl - (double)1);
    }
    DeRef(_0);

    /**    		hi = rand(hi - lo)*/
    if (IS_ATOM_INT(_hi_3108) && IS_ATOM_INT(_lo_3107)) {
        _1570 = _hi_3108 - _lo_3107;
        if ((long)((unsigned long)_1570 +(unsigned long) HIGH_BITS) >= 0){
            _1570 = NewDouble((double)_1570);
        }
    }
    else {
        if (IS_ATOM_INT(_hi_3108)) {
            _1570 = NewDouble((double)_hi_3108 - DBL_PTR(_lo_3107)->dbl);
        }
        else {
            if (IS_ATOM_INT(_lo_3107)) {
                _1570 = NewDouble(DBL_PTR(_hi_3108)->dbl - (double)_lo_3107);
            }
            else
            _1570 = NewDouble(DBL_PTR(_hi_3108)->dbl - DBL_PTR(_lo_3107)->dbl);
        }
    }
    DeRef(_hi_3108);
    if (IS_ATOM_INT(_1570)) {
        _hi_3108 = good_rand() % ((unsigned)_1570) + 1;
    }
    else {
        _hi_3108 = unary_op(RAND, _1570);
    }
    DeRef(_1570);
    _1570 = NOVALUE;
L4: 

    /**    	return lo + hi*/
    if (IS_ATOM_INT(_lo_3107) && IS_ATOM_INT(_hi_3108)) {
        _1572 = _lo_3107 + _hi_3108;
        if ((long)((unsigned long)_1572 + (unsigned long)HIGH_BITS) >= 0) 
        _1572 = NewDouble((double)_1572);
    }
    else {
        if (IS_ATOM_INT(_lo_3107)) {
            _1572 = NewDouble((double)_lo_3107 + DBL_PTR(_hi_3108)->dbl);
        }
        else {
            if (IS_ATOM_INT(_hi_3108)) {
                _1572 = NewDouble(DBL_PTR(_lo_3107)->dbl + (double)_hi_3108);
            }
            else
            _1572 = NewDouble(DBL_PTR(_lo_3107)->dbl + DBL_PTR(_hi_3108)->dbl);
        }
    }
    DeRef(_lo_3107);
    DeRef(_hi_3108);
    DeRef(_1562);
    _1562 = NOVALUE;
    return _1572;
    ;
}


int _19rnd()
{
    int _a_3131 = NOVALUE;
    int _b_3132 = NOVALUE;
    int _r_3133 = NOVALUE;
    int _0, _1, _2;
    

    /** 	 a = rand(#FFFFFFFF)*/
    DeRef(_a_3131);
    _a_3131 = unary_op(RAND, _1573);

    /** 	 if a = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _a_3131, 1)){
        goto L1; // [8] 17
    }
    DeRef(_a_3131);
    DeRef(_b_3132);
    DeRef(_r_3133);
    return 0;
L1: 

    /** 	 b = rand(#FFFFFFFF)*/
    DeRef(_b_3132);
    _b_3132 = unary_op(RAND, _1573);

    /** 	 if b = 1 then return 0 end if*/
    if (binary_op_a(NOTEQ, _b_3132, 1)){
        goto L2; // [24] 33
    }
    DeRef(_a_3131);
    DeRef(_b_3132);
    DeRef(_r_3133);
    return 0;
L2: 

    /** 	 if a > b then*/
    if (binary_op_a(LESSEQ, _a_3131, _b_3132)){
        goto L3; // [35] 48
    }

    /** 	 	r = b / a*/
    DeRef(_r_3133);
    if (IS_ATOM_INT(_b_3132) && IS_ATOM_INT(_a_3131)) {
        _r_3133 = (_b_3132 % _a_3131) ? NewDouble((double)_b_3132 / _a_3131) : (_b_3132 / _a_3131);
    }
    else {
        if (IS_ATOM_INT(_b_3132)) {
            _r_3133 = NewDouble((double)_b_3132 / DBL_PTR(_a_3131)->dbl);
        }
        else {
            if (IS_ATOM_INT(_a_3131)) {
                _r_3133 = NewDouble(DBL_PTR(_b_3132)->dbl / (double)_a_3131);
            }
            else
            _r_3133 = NewDouble(DBL_PTR(_b_3132)->dbl / DBL_PTR(_a_3131)->dbl);
        }
    }
    goto L4; // [45] 55
L3: 

    /** 	 	r = a / b*/
    DeRef(_r_3133);
    if (IS_ATOM_INT(_a_3131) && IS_ATOM_INT(_b_3132)) {
        _r_3133 = (_a_3131 % _b_3132) ? NewDouble((double)_a_3131 / _b_3132) : (_a_3131 / _b_3132);
    }
    else {
        if (IS_ATOM_INT(_a_3131)) {
            _r_3133 = NewDouble((double)_a_3131 / DBL_PTR(_b_3132)->dbl);
        }
        else {
            if (IS_ATOM_INT(_b_3132)) {
                _r_3133 = NewDouble(DBL_PTR(_a_3131)->dbl / (double)_b_3132);
            }
            else
            _r_3133 = NewDouble(DBL_PTR(_a_3131)->dbl / DBL_PTR(_b_3132)->dbl);
        }
    }
L4: 

    /** 	 return r*/
    DeRef(_a_3131);
    DeRef(_b_3132);
    return _r_3133;
    ;
}


int _19rnd_1()
{
    int _r_3148 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while r >= 1.0 with entry do*/
    goto L1; // [3] 15
L2: 
    if (binary_op_a(LESS, _r_3148, _1581)){
        goto L3; // [8] 25
    }

    /** 	entry*/
L1: 

    /** 		r = rnd()*/
    _0 = _r_3148;
    _r_3148 = _19rnd();
    DeRef(_0);

    /** 	end while	 */
    goto L2; // [22] 6
L3: 

    /** 	return r*/
    return _r_3148;
    ;
}


void _19set_rand(int _seed_3155)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_SET_RAND, seed)*/
    machine(35, _seed_3155);

    /** end procedure*/
    DeRef(_seed_3155);
    return;
    ;
}


int _19get_rand()
{
    int _1584 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_GET_RAND, {})*/
    _1584 = machine(98, _5);
    return _1584;
    ;
}


int _19chance(int _my_limit_3161, int _top_limit_3162)
{
    int _1587 = NOVALUE;
    int _1586 = NOVALUE;
    int _1585 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return (rnd_1() * top_limit) <= my_limit*/
    _1585 = _19rnd_1();
    if (IS_ATOM_INT(_1585) && IS_ATOM_INT(_top_limit_3162)) {
        if (_1585 == (short)_1585 && _top_limit_3162 <= INT15 && _top_limit_3162 >= -INT15)
        _1586 = _1585 * _top_limit_3162;
        else
        _1586 = NewDouble(_1585 * (double)_top_limit_3162);
    }
    else {
        _1586 = binary_op(MULTIPLY, _1585, _top_limit_3162);
    }
    DeRef(_1585);
    _1585 = NOVALUE;
    if (IS_ATOM_INT(_1586) && IS_ATOM_INT(_my_limit_3161)) {
        _1587 = (_1586 <= _my_limit_3161);
    }
    else {
        _1587 = binary_op(LESSEQ, _1586, _my_limit_3161);
    }
    DeRef(_1586);
    _1586 = NOVALUE;
    DeRef(_my_limit_3161);
    DeRef(_top_limit_3162);
    return _1587;
    ;
}


int _19roll(int _desired_3168, int _sides_3169)
{
    int _rolled_3170 = NOVALUE;
    int _1592 = NOVALUE;
    int _1589 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sides_3169)) {
        _1 = (long)(DBL_PTR(_sides_3169)->dbl);
        if (UNIQUE(DBL_PTR(_sides_3169)) && (DBL_PTR(_sides_3169)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sides_3169);
        _sides_3169 = _1;
    }

    /** 	if sides < 2 then*/
    if (_sides_3169 >= 2)
    goto L1; // [7] 18

    /** 		return 0*/
    DeRef(_desired_3168);
    return 0;
L1: 

    /** 	if atom(desired) then*/
    _1589 = IS_ATOM(_desired_3168);
    if (_1589 == 0)
    {
        _1589 = NOVALUE;
        goto L2; // [23] 33
    }
    else{
        _1589 = NOVALUE;
    }

    /** 		desired = {desired}*/
    _0 = _desired_3168;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_desired_3168);
    *((int *)(_2+4)) = _desired_3168;
    _desired_3168 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	rolled =  rand(sides)*/
    _rolled_3170 = good_rand() % ((unsigned)_sides_3169) + 1;

    /** 	if find(rolled, desired) then*/
    _1592 = find_from(_rolled_3170, _desired_3168, 1);
    if (_1592 == 0)
    {
        _1592 = NOVALUE;
        goto L3; // [47] 59
    }
    else{
        _1592 = NOVALUE;
    }

    /** 		return rolled*/
    DeRef(_desired_3168);
    return _rolled_3170;
    goto L4; // [56] 66
L3: 

    /** 		return 0*/
    DeRef(_desired_3168);
    return 0;
L4: 
    ;
}


int _19sample(int _population_3182, int _sample_size_3183, int _sampling_method_3184)
{
    int _lResult_3185 = NOVALUE;
    int _lIdx_3186 = NOVALUE;
    int _lChoice_3187 = NOVALUE;
    int _1610 = NOVALUE;
    int _1606 = NOVALUE;
    int _1603 = NOVALUE;
    int _1599 = NOVALUE;
    int _1598 = NOVALUE;
    int _1597 = NOVALUE;
    int _1596 = NOVALUE;
    int _1595 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sample_size_3183)) {
        _1 = (long)(DBL_PTR(_sample_size_3183)->dbl);
        if (UNIQUE(DBL_PTR(_sample_size_3183)) && (DBL_PTR(_sample_size_3183)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sample_size_3183);
        _sample_size_3183 = _1;
    }
    if (!IS_ATOM_INT(_sampling_method_3184)) {
        _1 = (long)(DBL_PTR(_sampling_method_3184)->dbl);
        if (UNIQUE(DBL_PTR(_sampling_method_3184)) && (DBL_PTR(_sampling_method_3184)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sampling_method_3184);
        _sampling_method_3184 = _1;
    }

    /** 	if sample_size < 1 then*/
    if (_sample_size_3183 >= 1)
    goto L1; // [13] 44

    /** 		if sampling_method > 0 then*/
    if (_sampling_method_3184 <= 0)
    goto L2; // [19] 36

    /** 			return {{}, population}	*/
    RefDS(_population_3182);
    RefDS(_5);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5;
    ((int *)_2)[2] = _population_3182;
    _1595 = MAKE_SEQ(_1);
    DeRefDS(_population_3182);
    DeRef(_lResult_3185);
    return _1595;
    goto L3; // [33] 43
L2: 

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_population_3182);
    DeRef(_lResult_3185);
    DeRef(_1595);
    _1595 = NOVALUE;
    return _5;
L3: 
L1: 

    /** 	if sampling_method >= 0 and sample_size >= length(population) then*/
    _1596 = (_sampling_method_3184 >= 0);
    if (_1596 == 0) {
        goto L4; // [50] 73
    }
    if (IS_SEQUENCE(_population_3182)){
            _1598 = SEQ_PTR(_population_3182)->length;
    }
    else {
        _1598 = 1;
    }
    _1599 = (_sample_size_3183 >= _1598);
    _1598 = NOVALUE;
    if (_1599 == 0)
    {
        DeRef(_1599);
        _1599 = NOVALUE;
        goto L4; // [62] 73
    }
    else{
        DeRef(_1599);
        _1599 = NOVALUE;
    }

    /** 		sample_size = length(population)*/
    if (IS_SEQUENCE(_population_3182)){
            _sample_size_3183 = SEQ_PTR(_population_3182)->length;
    }
    else {
        _sample_size_3183 = 1;
    }
L4: 

    /** 	lResult = repeat(0, sample_size)*/
    DeRef(_lResult_3185);
    _lResult_3185 = Repeat(0, _sample_size_3183);

    /** 	lIdx = 0*/
    _lIdx_3186 = 0;

    /** 	while lIdx < sample_size do*/
L5: 
    if (_lIdx_3186 >= _sample_size_3183)
    goto L6; // [91] 142

    /** 		lChoice = rand(length(population))*/
    if (IS_SEQUENCE(_population_3182)){
            _1603 = SEQ_PTR(_population_3182)->length;
    }
    else {
        _1603 = 1;
    }
    _lChoice_3187 = good_rand() % ((unsigned)_1603) + 1;
    _1603 = NOVALUE;

    /** 		lIdx += 1*/
    _lIdx_3186 = _lIdx_3186 + 1;

    /** 		lResult[lIdx] = population[lChoice]*/
    _2 = (int)SEQ_PTR(_population_3182);
    _1606 = (int)*(((s1_ptr)_2)->base + _lChoice_3187);
    Ref(_1606);
    _2 = (int)SEQ_PTR(_lResult_3185);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lResult_3185 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _lIdx_3186);
    _1 = *(int *)_2;
    *(int *)_2 = _1606;
    if( _1 != _1606 ){
        DeRef(_1);
    }
    _1606 = NOVALUE;

    /** 		if sampling_method >= 0 then*/
    if (_sampling_method_3184 < 0)
    goto L5; // [125] 91

    /** 			population = remove(population, lChoice)*/
    {
        s1_ptr assign_space = SEQ_PTR(_population_3182);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lChoice_3187)) ? _lChoice_3187 : (long)(DBL_PTR(_lChoice_3187)->dbl);
        int stop = (IS_ATOM_INT(_lChoice_3187)) ? _lChoice_3187 : (long)(DBL_PTR(_lChoice_3187)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_population_3182), start, &_population_3182 );
            }
            else Tail(SEQ_PTR(_population_3182), stop+1, &_population_3182);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_population_3182), start, &_population_3182);
        }
        else {
            assign_slice_seq = &assign_space;
            _population_3182 = Remove_elements(start, stop, (SEQ_PTR(_population_3182)->ref == 1));
        }
    }

    /** 	end while*/
    goto L5; // [139] 91
L6: 

    /** 	if sampling_method > 0 then*/
    if (_sampling_method_3184 <= 0)
    goto L7; // [144] 161

    /** 		return {lResult, population}	*/
    RefDS(_population_3182);
    RefDS(_lResult_3185);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lResult_3185;
    ((int *)_2)[2] = _population_3182;
    _1610 = MAKE_SEQ(_1);
    DeRefDS(_population_3182);
    DeRefDS(_lResult_3185);
    DeRef(_1595);
    _1595 = NOVALUE;
    DeRef(_1596);
    _1596 = NOVALUE;
    return _1610;
    goto L8; // [158] 168
L7: 

    /** 		return lResult*/
    DeRefDS(_population_3182);
    DeRef(_1595);
    _1595 = NOVALUE;
    DeRef(_1596);
    _1596 = NOVALUE;
    DeRef(_1610);
    _1610 = NOVALUE;
    return _lResult_3185;
L8: 
    ;
}



// 0xCF1C976D
