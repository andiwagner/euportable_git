// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _15find_first_wildcard(int _name_5976, int _from_5977)
{
    int _asterisk_at_5978 = NOVALUE;
    int _question_at_5980 = NOVALUE;
    int _first_wildcard_at_5982 = NOVALUE;
    int _3192 = NOVALUE;
    int _3191 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer asterisk_at = eu:find('*', name, from)*/
    _asterisk_at_5978 = find_from(42, _name_5976, 1);

    /** 	integer question_at = eu:find('?', name, from)*/
    _question_at_5980 = find_from(63, _name_5976, 1);

    /** 	integer first_wildcard_at = asterisk_at*/
    _first_wildcard_at_5982 = _asterisk_at_5978;

    /** 	if asterisk_at or question_at then*/
    if (_asterisk_at_5978 != 0) {
        goto L1; // [34] 43
    }
    if (_question_at_5980 == 0)
    {
        goto L2; // [39] 66
    }
    else{
    }
L1: 

    /** 		if question_at and question_at < asterisk_at then*/
    if (_question_at_5980 == 0) {
        goto L3; // [45] 65
    }
    _3192 = (_question_at_5980 < _asterisk_at_5978);
    if (_3192 == 0)
    {
        DeRef(_3192);
        _3192 = NOVALUE;
        goto L3; // [54] 65
    }
    else{
        DeRef(_3192);
        _3192 = NOVALUE;
    }

    /** 			first_wildcard_at = question_at*/
    _first_wildcard_at_5982 = _question_at_5980;
L3: 
L2: 

    /** 	return first_wildcard_at*/
    DeRefDS(_name_5976);
    return _first_wildcard_at_5982;
    ;
}


int _15dir(int _name_5990)
{
    int _3193 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    _3193 = machine(22, _name_5990);
    DeRefDS(_name_5990);
    return _3193;
    ;
}


int _15current_dir()
{
    int _3195 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    _3195 = machine(23, 0);
    return _3195;
    ;
}


int _15chdir(int _newdir_5998)
{
    int _3196 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CHDIR, newdir)*/
    _3196 = machine(63, _newdir_5998);
    DeRefDS(_newdir_5998);
    return _3196;
    ;
}


int _15walk_dir(int _path_name_6015, int _your_function_6016, int _scan_subdirs_6017, int _dir_source_6018)
{
    int _d_6019 = NOVALUE;
    int _abort_now_6020 = NOVALUE;
    int _orig_func_6021 = NOVALUE;
    int _user_data_6022 = NOVALUE;
    int _source_orig_func_6024 = NOVALUE;
    int _source_user_data_6025 = NOVALUE;
    int _3247 = NOVALUE;
    int _3246 = NOVALUE;
    int _3245 = NOVALUE;
    int _3244 = NOVALUE;
    int _3243 = NOVALUE;
    int _3241 = NOVALUE;
    int _3240 = NOVALUE;
    int _3239 = NOVALUE;
    int _3238 = NOVALUE;
    int _3237 = NOVALUE;
    int _3236 = NOVALUE;
    int _3235 = NOVALUE;
    int _3234 = NOVALUE;
    int _3233 = NOVALUE;
    int _3231 = NOVALUE;
    int _3229 = NOVALUE;
    int _3228 = NOVALUE;
    int _3227 = NOVALUE;
    int _3225 = NOVALUE;
    int _3224 = NOVALUE;
    int _3223 = NOVALUE;
    int _3219 = NOVALUE;
    int _3217 = NOVALUE;
    int _3213 = NOVALUE;
    int _3211 = NOVALUE;
    int _3210 = NOVALUE;
    int _3208 = NOVALUE;
    int _3205 = NOVALUE;
    int _3202 = NOVALUE;
    int _3201 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scan_subdirs_6017)) {
        _1 = (long)(DBL_PTR(_scan_subdirs_6017)->dbl);
        if (UNIQUE(DBL_PTR(_scan_subdirs_6017)) && (DBL_PTR(_scan_subdirs_6017)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scan_subdirs_6017);
        _scan_subdirs_6017 = _1;
    }

    /** 	sequence user_data = {path_name, 0}*/
    RefDS(_path_name_6015);
    DeRef(_user_data_6022);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_6015;
    ((int *)_2)[2] = 0;
    _user_data_6022 = MAKE_SEQ(_1);

    /** 	object source_user_data = ""*/
    RefDS(_5);
    DeRef(_source_user_data_6025);
    _source_user_data_6025 = _5;

    /** 	orig_func = your_function*/
    Ref(_your_function_6016);
    DeRef(_orig_func_6021);
    _orig_func_6021 = _your_function_6016;

    /** 	if sequence(your_function) then*/
    _3201 = IS_SEQUENCE(_your_function_6016);
    if (_3201 == 0)
    {
        _3201 = NOVALUE;
        goto L1; // [28] 48
    }
    else{
        _3201 = NOVALUE;
    }

    /** 		user_data = append(user_data, your_function[2])*/
    _2 = (int)SEQ_PTR(_your_function_6016);
    _3202 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_3202);
    Append(&_user_data_6022, _user_data_6022, _3202);
    _3202 = NOVALUE;

    /** 		your_function = your_function[1]*/
    _0 = _your_function_6016;
    _2 = (int)SEQ_PTR(_your_function_6016);
    _your_function_6016 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_your_function_6016);
    DeRef(_0);
L1: 

    /** 	source_orig_func = dir_source*/
    Ref(_dir_source_6018);
    DeRef(_source_orig_func_6024);
    _source_orig_func_6024 = _dir_source_6018;

    /** 	if sequence(dir_source) then*/
    _3205 = IS_SEQUENCE(_dir_source_6018);
    if (_3205 == 0)
    {
        _3205 = NOVALUE;
        goto L2; // [58] 74
    }
    else{
        _3205 = NOVALUE;
    }

    /** 		source_user_data = dir_source[2]*/
    DeRef(_source_user_data_6025);
    _2 = (int)SEQ_PTR(_dir_source_6018);
    _source_user_data_6025 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_source_user_data_6025);

    /** 		dir_source = dir_source[1]*/
    _0 = _dir_source_6018;
    _2 = (int)SEQ_PTR(_dir_source_6018);
    _dir_source_6018 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_dir_source_6018);
    DeRef(_0);
L2: 

    /** 	if not equal(dir_source, types:NO_ROUTINE_ID) then*/
    if (_dir_source_6018 == -99999)
    _3208 = 1;
    else if (IS_ATOM_INT(_dir_source_6018) && IS_ATOM_INT(-99999))
    _3208 = 0;
    else
    _3208 = (compare(_dir_source_6018, -99999) == 0);
    if (_3208 != 0)
    goto L3; // [80] 118
    _3208 = NOVALUE;

    /** 		if atom(source_orig_func) then*/
    _3210 = IS_ATOM(_source_orig_func_6024);
    if (_3210 == 0)
    {
        _3210 = NOVALUE;
        goto L4; // [88] 104
    }
    else{
        _3210 = NOVALUE;
    }

    /** 			d = call_func(dir_source, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_6015);
    *((int *)(_2+4)) = _path_name_6015;
    _3211 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3211);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_6018].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_6019);
    _d_6019 = _1;
    DeRefDS(_3211);
    _3211 = NOVALUE;
    goto L5; // [101] 148
L4: 

    /** 			d = call_func(dir_source, {path_name, source_user_data})*/
    Ref(_source_user_data_6025);
    RefDS(_path_name_6015);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_name_6015;
    ((int *)_2)[2] = _source_user_data_6025;
    _3213 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3213);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_dir_source_6018].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8)
                         );
    DeRef(_d_6019);
    _d_6019 = _1;
    DeRefDS(_3213);
    _3213 = NOVALUE;
    goto L5; // [115] 148
L3: 

    /** 	elsif my_dir = DEFAULT_DIR_SOURCE then*/

    /** 		d = machine_func(M_DIR, path_name)*/
    DeRef(_d_6019);
    _d_6019 = machine(22, _path_name_6015);
    goto L5; // [132] 148

    /** 		d = call_func(my_dir, {path_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_path_name_6015);
    *((int *)(_2+4)) = _path_name_6015;
    _3217 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_3217);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[-2].addr;
    Ref(*(int *)(_2+4));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4)
                         );
    DeRef(_d_6019);
    _d_6019 = _1;
    DeRefDS(_3217);
    _3217 = NOVALUE;
L5: 

    /** 	if atom(d) then*/
    _3219 = IS_ATOM(_d_6019);
    if (_3219 == 0)
    {
        _3219 = NOVALUE;
        goto L6; // [155] 165
    }
    else{
        _3219 = NOVALUE;
    }

    /** 		return W_BAD_PATH*/
    DeRefDS(_path_name_6015);
    DeRef(_your_function_6016);
    DeRef(_dir_source_6018);
    DeRef(_d_6019);
    DeRef(_abort_now_6020);
    DeRef(_orig_func_6021);
    DeRef(_user_data_6022);
    DeRef(_source_orig_func_6024);
    DeRef(_source_user_data_6025);
    return -1;
L6: 

    /** 	ifdef not UNIX then*/

    /** 		path_name = match_replace('/', path_name, '\\')*/
    RefDS(_path_name_6015);
    _0 = _path_name_6015;
    _path_name_6015 = _14match_replace(47, _path_name_6015, 92, 0);
    DeRefDS(_0);

    /** 	path_name = text:trim_tail(path_name, {' ', SLASH, '\n'})*/
    RefDS(_path_name_6015);
    RefDS(_3221);
    _0 = _path_name_6015;
    _path_name_6015 = _12trim_tail(_path_name_6015, _3221, 0);
    DeRefDS(_0);

    /** 	user_data[1] = path_name*/
    RefDS(_path_name_6015);
    _2 = (int)SEQ_PTR(_user_data_6022);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _path_name_6015;
    DeRef(_1);

    /** 	for i = 1 to length(d) do*/
    if (IS_SEQUENCE(_d_6019)){
            _3223 = SEQ_PTR(_d_6019)->length;
    }
    else {
        _3223 = 1;
    }
    {
        int _i_6059;
        _i_6059 = 1;
L7: 
        if (_i_6059 > _3223){
            goto L8; // [199] 366
        }

        /** 		if eu:find(d[i][D_NAME], {".", ".."}) then*/
        _2 = (int)SEQ_PTR(_d_6019);
        _3224 = (int)*(((s1_ptr)_2)->base + _i_6059);
        _2 = (int)SEQ_PTR(_3224);
        _3225 = (int)*(((s1_ptr)_2)->base + 1);
        _3224 = NOVALUE;
        RefDS(_3226);
        RefDS(_3194);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _3194;
        ((int *)_2)[2] = _3226;
        _3227 = MAKE_SEQ(_1);
        _3228 = find_from(_3225, _3227, 1);
        _3225 = NOVALUE;
        DeRefDS(_3227);
        _3227 = NOVALUE;
        if (_3228 == 0)
        {
            _3228 = NOVALUE;
            goto L9; // [227] 235
        }
        else{
            _3228 = NOVALUE;
        }

        /** 			continue*/
        goto LA; // [232] 361
L9: 

        /** 		user_data[2] = d[i]*/
        _2 = (int)SEQ_PTR(_d_6019);
        _3229 = (int)*(((s1_ptr)_2)->base + _i_6059);
        Ref(_3229);
        _2 = (int)SEQ_PTR(_user_data_6022);
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _3229;
        if( _1 != _3229 ){
            DeRef(_1);
        }
        _3229 = NOVALUE;

        /** 		abort_now = call_func(your_function, user_data)*/
        _1 = (int)SEQ_PTR(_user_data_6022);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_your_function_6016].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
        }
        DeRef(_abort_now_6020);
        _abort_now_6020 = _1;

        /** 		if not equal(abort_now, 0) then*/
        if (_abort_now_6020 == 0)
        _3231 = 1;
        else if (IS_ATOM_INT(_abort_now_6020) && IS_ATOM_INT(0))
        _3231 = 0;
        else
        _3231 = (compare(_abort_now_6020, 0) == 0);
        if (_3231 != 0)
        goto LB; // [257] 267
        _3231 = NOVALUE;

        /** 			return abort_now*/
        DeRefDS(_path_name_6015);
        DeRef(_your_function_6016);
        DeRef(_dir_source_6018);
        DeRef(_d_6019);
        DeRef(_orig_func_6021);
        DeRefDS(_user_data_6022);
        DeRef(_source_orig_func_6024);
        DeRef(_source_user_data_6025);
        return _abort_now_6020;
LB: 

        /** 		if eu:find('d', d[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_d_6019);
        _3233 = (int)*(((s1_ptr)_2)->base + _i_6059);
        _2 = (int)SEQ_PTR(_3233);
        _3234 = (int)*(((s1_ptr)_2)->base + 2);
        _3233 = NOVALUE;
        _3235 = find_from(100, _3234, 1);
        _3234 = NOVALUE;
        if (_3235 == 0)
        {
            _3235 = NOVALUE;
            goto LC; // [284] 359
        }
        else{
            _3235 = NOVALUE;
        }

        /** 			if scan_subdirs then*/
        if (_scan_subdirs_6017 == 0)
        {
            goto LD; // [289] 358
        }
        else{
        }

        /** 				abort_now = walk_dir(path_name & SLASH & d[i][D_NAME],*/
        _2 = (int)SEQ_PTR(_d_6019);
        _3236 = (int)*(((s1_ptr)_2)->base + _i_6059);
        _2 = (int)SEQ_PTR(_3236);
        _3237 = (int)*(((s1_ptr)_2)->base + 1);
        _3236 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _3237;
            concat_list[1] = 92;
            concat_list[2] = _path_name_6015;
            Concat_N((object_ptr)&_3238, concat_list, 3);
        }
        _3237 = NOVALUE;
        Ref(_orig_func_6021);
        DeRef(_3239);
        _3239 = _orig_func_6021;
        DeRef(_3240);
        _3240 = _scan_subdirs_6017;
        Ref(_source_orig_func_6024);
        DeRef(_3241);
        _3241 = _source_orig_func_6024;
        _0 = _abort_now_6020;
        _abort_now_6020 = _15walk_dir(_3238, _3239, _3240, _3241);
        DeRef(_0);
        _3238 = NOVALUE;
        _3239 = NOVALUE;
        _3240 = NOVALUE;
        _3241 = NOVALUE;

        /** 				if not equal(abort_now, 0) and */
        if (_abort_now_6020 == 0)
        _3243 = 1;
        else if (IS_ATOM_INT(_abort_now_6020) && IS_ATOM_INT(0))
        _3243 = 0;
        else
        _3243 = (compare(_abort_now_6020, 0) == 0);
        _3244 = (_3243 == 0);
        _3243 = NOVALUE;
        if (_3244 == 0) {
            goto LE; // [335] 357
        }
        if (_abort_now_6020 == -1)
        _3246 = 1;
        else if (IS_ATOM_INT(_abort_now_6020) && IS_ATOM_INT(-1))
        _3246 = 0;
        else
        _3246 = (compare(_abort_now_6020, -1) == 0);
        _3247 = (_3246 == 0);
        _3246 = NOVALUE;
        if (_3247 == 0)
        {
            DeRef(_3247);
            _3247 = NOVALUE;
            goto LE; // [347] 357
        }
        else{
            DeRef(_3247);
            _3247 = NOVALUE;
        }

        /** 					return abort_now*/
        DeRefDS(_path_name_6015);
        DeRef(_your_function_6016);
        DeRef(_dir_source_6018);
        DeRef(_d_6019);
        DeRef(_orig_func_6021);
        DeRef(_user_data_6022);
        DeRef(_source_orig_func_6024);
        DeRef(_source_user_data_6025);
        DeRef(_3244);
        _3244 = NOVALUE;
        return _abort_now_6020;
LE: 
LD: 
LC: 

        /** 	end for*/
LA: 
        _i_6059 = _i_6059 + 1;
        goto L7; // [361] 206
L8: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_path_name_6015);
    DeRef(_your_function_6016);
    DeRef(_dir_source_6018);
    DeRef(_d_6019);
    DeRef(_abort_now_6020);
    DeRef(_orig_func_6021);
    DeRef(_user_data_6022);
    DeRef(_source_orig_func_6024);
    DeRef(_source_user_data_6025);
    DeRef(_3244);
    _3244 = NOVALUE;
    return 0;
    ;
}


int _15create_directory(int _name_6092, int _mode_6093, int _mkparent_6095)
{
    int _pname_6096 = NOVALUE;
    int _ret_6097 = NOVALUE;
    int _pos_6098 = NOVALUE;
    int _3267 = NOVALUE;
    int _3264 = NOVALUE;
    int _3263 = NOVALUE;
    int _3262 = NOVALUE;
    int _3261 = NOVALUE;
    int _3258 = NOVALUE;
    int _3255 = NOVALUE;
    int _3254 = NOVALUE;
    int _3252 = NOVALUE;
    int _3251 = NOVALUE;
    int _3249 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_mode_6093)) {
        _1 = (long)(DBL_PTR(_mode_6093)->dbl);
        if (UNIQUE(DBL_PTR(_mode_6093)) && (DBL_PTR(_mode_6093)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mode_6093);
        _mode_6093 = _1;
    }
    if (!IS_ATOM_INT(_mkparent_6095)) {
        _1 = (long)(DBL_PTR(_mkparent_6095)->dbl);
        if (UNIQUE(DBL_PTR(_mkparent_6095)) && (DBL_PTR(_mkparent_6095)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mkparent_6095);
        _mkparent_6095 = _1;
    }

    /** 	if length(name) = 0 then*/
    if (IS_SEQUENCE(_name_6092)){
            _3249 = SEQ_PTR(_name_6092)->length;
    }
    else {
        _3249 = 1;
    }
    if (_3249 != 0)
    goto L1; // [16] 27

    /** 		return 0 -- failed*/
    DeRefDS(_name_6092);
    DeRef(_pname_6096);
    DeRef(_ret_6097);
    return 0;
L1: 

    /** 	if name[$] = SLASH then*/
    if (IS_SEQUENCE(_name_6092)){
            _3251 = SEQ_PTR(_name_6092)->length;
    }
    else {
        _3251 = 1;
    }
    _2 = (int)SEQ_PTR(_name_6092);
    _3252 = (int)*(((s1_ptr)_2)->base + _3251);
    if (binary_op_a(NOTEQ, _3252, 92)){
        _3252 = NOVALUE;
        goto L2; // [36] 55
    }
    _3252 = NOVALUE;

    /** 		name = name[1 .. $-1]*/
    if (IS_SEQUENCE(_name_6092)){
            _3254 = SEQ_PTR(_name_6092)->length;
    }
    else {
        _3254 = 1;
    }
    _3255 = _3254 - 1;
    _3254 = NOVALUE;
    rhs_slice_target = (object_ptr)&_name_6092;
    RHS_Slice(_name_6092, 1, _3255);
L2: 

    /** 	if mkparent != 0 then*/
    if (_mkparent_6095 == 0)
    goto L3; // [57] 107

    /** 		pos = search:rfind(SLASH, name)*/
    if (IS_SEQUENCE(_name_6092)){
            _3258 = SEQ_PTR(_name_6092)->length;
    }
    else {
        _3258 = 1;
    }
    RefDS(_name_6092);
    _pos_6098 = _14rfind(92, _name_6092, _3258);
    _3258 = NOVALUE;
    if (!IS_ATOM_INT(_pos_6098)) {
        _1 = (long)(DBL_PTR(_pos_6098)->dbl);
        if (UNIQUE(DBL_PTR(_pos_6098)) && (DBL_PTR(_pos_6098)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_6098);
        _pos_6098 = _1;
    }

    /** 		if pos != 0 then*/
    if (_pos_6098 == 0)
    goto L4; // [78] 106

    /** 			ret = create_directory(name[1.. pos-1], mode, mkparent)*/
    _3261 = _pos_6098 - 1;
    rhs_slice_target = (object_ptr)&_3262;
    RHS_Slice(_name_6092, 1, _3261);
    DeRef(_3263);
    _3263 = _mode_6093;
    DeRef(_3264);
    _3264 = _mkparent_6095;
    _0 = _ret_6097;
    _ret_6097 = _15create_directory(_3262, _3263, _3264);
    DeRef(_0);
    _3262 = NOVALUE;
    _3263 = NOVALUE;
    _3264 = NOVALUE;
L4: 
L3: 

    /** 	pname = machine:allocate_string(name)*/
    RefDS(_name_6092);
    _0 = _pname_6096;
    _pname_6096 = _7allocate_string(_name_6092, 0);
    DeRef(_0);

    /** 	ifdef UNIX then*/

    /** 		ret = c_func(xCreateDirectory, {pname, 0})*/
    Ref(_pname_6096);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pname_6096;
    ((int *)_2)[2] = 0;
    _3267 = MAKE_SEQ(_1);
    DeRef(_ret_6097);
    _ret_6097 = call_c(1, _15xCreateDirectory_5916, _3267);
    DeRefDS(_3267);
    _3267 = NOVALUE;

    /** 		mode = mode -- get rid of not used warning*/
    _mode_6093 = _mode_6093;

    /** 	return ret*/
    DeRefDS(_name_6092);
    DeRef(_pname_6096);
    DeRef(_3255);
    _3255 = NOVALUE;
    DeRef(_3261);
    _3261 = NOVALUE;
    return _ret_6097;
    ;
}


int _15create_file(int _name_6125)
{
    int _fh_6126 = NOVALUE;
    int _ret_6129 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fh = open(name, "wb")*/
    _fh_6126 = EOpen(_name_6125, _3269, 0);

    /** 	integer ret = (fh != -1)*/
    _ret_6129 = (_fh_6126 != -1);

    /** 	if ret then*/
    if (_ret_6129 == 0)
    {
        goto L1; // [22] 30
    }
    else{
    }

    /** 		close(fh)*/
    EClose(_fh_6126);
L1: 

    /** 	return ret*/
    DeRefDS(_name_6125);
    return _ret_6129;
    ;
}


int _15delete_file(int _name_6134)
{
    int _pfilename_6135 = NOVALUE;
    int _success_6137 = NOVALUE;
    int _3273 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom pfilename = machine:allocate_string(name)*/
    RefDS(_name_6134);
    _0 = _pfilename_6135;
    _pfilename_6135 = _7allocate_string(_name_6134, 0);
    DeRef(_0);

    /** 	integer success = c_func(xDeleteFile, {pfilename})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pfilename_6135);
    *((int *)(_2+4)) = _pfilename_6135;
    _3273 = MAKE_SEQ(_1);
    _success_6137 = call_c(1, _15xDeleteFile_5912, _3273);
    DeRefDS(_3273);
    _3273 = NOVALUE;
    if (!IS_ATOM_INT(_success_6137)) {
        _1 = (long)(DBL_PTR(_success_6137)->dbl);
        if (UNIQUE(DBL_PTR(_success_6137)) && (DBL_PTR(_success_6137)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_6137);
        _success_6137 = _1;
    }

    /** 	ifdef UNIX then*/

    /** 	machine:free(pfilename)*/
    Ref(_pfilename_6135);
    _7free(_pfilename_6135);

    /** 	return success*/
    DeRefDS(_name_6134);
    DeRef(_pfilename_6135);
    return _success_6137;
    ;
}


int _15curdir(int _drive_id_6142)
{
    int _lCurDir_6143 = NOVALUE;
    int _lOrigDir_6144 = NOVALUE;
    int _lDrive_6145 = NOVALUE;
    int _current_dir_inlined_current_dir_at_27_6150 = NOVALUE;
    int _chdir_inlined_chdir_at_57_6153 = NOVALUE;
    int _current_dir_inlined_current_dir_at_79_6156 = NOVALUE;
    int _chdir_inlined_chdir_at_111_6163 = NOVALUE;
    int _newdir_inlined_chdir_at_108_6162 = NOVALUE;
    int _3281 = NOVALUE;
    int _3280 = NOVALUE;
    int _3279 = NOVALUE;
    int _3277 = NOVALUE;
    int _3275 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_drive_id_6142)) {
        _1 = (long)(DBL_PTR(_drive_id_6142)->dbl);
        if (UNIQUE(DBL_PTR(_drive_id_6142)) && (DBL_PTR(_drive_id_6142)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_drive_id_6142);
        _drive_id_6142 = _1;
    }

    /** 	ifdef not LINUX then*/

    /** 	    sequence lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_6144);
    _lOrigDir_6144 = _5;

    /** 	    sequence lDrive*/

    /** 	    if t_alpha(drive_id) then*/
    _3275 = _11t_alpha(_drive_id_6142);
    if (_3275 == 0) {
        DeRef(_3275);
        _3275 = NOVALUE;
        goto L1; // [22] 77
    }
    else {
        if (!IS_ATOM_INT(_3275) && DBL_PTR(_3275)->dbl == 0.0){
            DeRef(_3275);
            _3275 = NOVALUE;
            goto L1; // [22] 77
        }
        DeRef(_3275);
        _3275 = NOVALUE;
    }
    DeRef(_3275);
    _3275 = NOVALUE;

    /** 		    lOrigDir =  current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefDSi(_lOrigDir_6144);
    _lOrigDir_6144 = machine(23, 0);

    /** 		    lDrive = "  "*/
    RefDS(_141);
    DeRefi(_lDrive_6145);
    _lDrive_6145 = _141;

    /** 		    lDrive[1] = drive_id*/
    _2 = (int)SEQ_PTR(_lDrive_6145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_6145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _drive_id_6142;

    /** 		    lDrive[2] = ':'*/
    _2 = (int)SEQ_PTR(_lDrive_6145);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _lDrive_6145 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    *(int *)_2 = 58;

    /** 		    if chdir(lDrive) = 0 then*/

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_57_6153 = machine(63, _lDrive_6145);
    if (_chdir_inlined_chdir_at_57_6153 != 0)
    goto L2; // [64] 76

    /** 		    	lOrigDir = ""*/
    RefDS(_5);
    DeRefi(_lOrigDir_6144);
    _lOrigDir_6144 = _5;
L2: 
L1: 

    /**     lCurDir = current_dir()*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_lCurDir_6143);
    _lCurDir_6143 = machine(23, 0);

    /** 	ifdef not LINUX then*/

    /** 		if length(lOrigDir) > 0 then*/
    if (IS_SEQUENCE(_lOrigDir_6144)){
            _3277 = SEQ_PTR(_lOrigDir_6144)->length;
    }
    else {
        _3277 = 1;
    }
    if (_3277 <= 0)
    goto L3; // [97] 123

    /** 	    	chdir(lOrigDir[1..2])*/
    rhs_slice_target = (object_ptr)&_3279;
    RHS_Slice(_lOrigDir_6144, 1, 2);
    DeRefi(_newdir_inlined_chdir_at_108_6162);
    _newdir_inlined_chdir_at_108_6162 = _3279;
    _3279 = NOVALUE;

    /** 	return machine_func(M_CHDIR, newdir)*/
    _chdir_inlined_chdir_at_111_6163 = machine(63, _newdir_inlined_chdir_at_108_6162);
    DeRefi(_newdir_inlined_chdir_at_108_6162);
    _newdir_inlined_chdir_at_108_6162 = NOVALUE;
L3: 

    /** 	if (lCurDir[$] != SLASH) then*/
    if (IS_SEQUENCE(_lCurDir_6143)){
            _3280 = SEQ_PTR(_lCurDir_6143)->length;
    }
    else {
        _3280 = 1;
    }
    _2 = (int)SEQ_PTR(_lCurDir_6143);
    _3281 = (int)*(((s1_ptr)_2)->base + _3280);
    if (_3281 == 92)
    goto L4; // [132] 143

    /** 		lCurDir &= SLASH*/
    Append(&_lCurDir_6143, _lCurDir_6143, 92);
L4: 

    /** 	return lCurDir*/
    DeRefi(_lOrigDir_6144);
    DeRefi(_lDrive_6145);
    _3281 = NOVALUE;
    return _lCurDir_6143;
    ;
}


int _15init_curdir()
{
    int _0, _1, _2;
    

    /** 	return InitCurDir*/
    RefDS(_15InitCurDir_6169);
    return _15InitCurDir_6169;
    ;
}


int _15clear_directory(int _path_6175, int _recurse_6176)
{
    int _files_6177 = NOVALUE;
    int _ret_6178 = NOVALUE;
    int _dir_inlined_dir_at_91_6199 = NOVALUE;
    int _cnt_6224 = NOVALUE;
    int _3325 = NOVALUE;
    int _3324 = NOVALUE;
    int _3323 = NOVALUE;
    int _3322 = NOVALUE;
    int _3318 = NOVALUE;
    int _3317 = NOVALUE;
    int _3316 = NOVALUE;
    int _3315 = NOVALUE;
    int _3314 = NOVALUE;
    int _3313 = NOVALUE;
    int _3312 = NOVALUE;
    int _3311 = NOVALUE;
    int _3308 = NOVALUE;
    int _3307 = NOVALUE;
    int _3306 = NOVALUE;
    int _3304 = NOVALUE;
    int _3303 = NOVALUE;
    int _3302 = NOVALUE;
    int _3300 = NOVALUE;
    int _3299 = NOVALUE;
    int _3297 = NOVALUE;
    int _3295 = NOVALUE;
    int _3293 = NOVALUE;
    int _3291 = NOVALUE;
    int _3290 = NOVALUE;
    int _3288 = NOVALUE;
    int _3287 = NOVALUE;
    int _3285 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recurse_6176)) {
        _1 = (long)(DBL_PTR(_recurse_6176)->dbl);
        if (UNIQUE(DBL_PTR(_recurse_6176)) && (DBL_PTR(_recurse_6176)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recurse_6176);
        _recurse_6176 = _1;
    }

    /** 	if length(path) > 0 then*/
    if (IS_SEQUENCE(_path_6175)){
            _3285 = SEQ_PTR(_path_6175)->length;
    }
    else {
        _3285 = 1;
    }
    if (_3285 <= 0)
    goto L1; // [12] 45

    /** 		if path[$] = SLASH then*/
    if (IS_SEQUENCE(_path_6175)){
            _3287 = SEQ_PTR(_path_6175)->length;
    }
    else {
        _3287 = 1;
    }
    _2 = (int)SEQ_PTR(_path_6175);
    _3288 = (int)*(((s1_ptr)_2)->base + _3287);
    if (binary_op_a(NOTEQ, _3288, 92)){
        _3288 = NOVALUE;
        goto L2; // [25] 44
    }
    _3288 = NOVALUE;

    /** 			path = path[1 .. $-1]*/
    if (IS_SEQUENCE(_path_6175)){
            _3290 = SEQ_PTR(_path_6175)->length;
    }
    else {
        _3290 = 1;
    }
    _3291 = _3290 - 1;
    _3290 = NOVALUE;
    rhs_slice_target = (object_ptr)&_path_6175;
    RHS_Slice(_path_6175, 1, _3291);
L2: 
L1: 

    /** 	if length(path) = 0 then*/
    if (IS_SEQUENCE(_path_6175)){
            _3293 = SEQ_PTR(_path_6175)->length;
    }
    else {
        _3293 = 1;
    }
    if (_3293 != 0)
    goto L3; // [50] 61

    /** 		return 0 -- Nothing specified to clear. Not safe to assume anything.*/
    DeRefDS(_path_6175);
    DeRef(_files_6177);
    DeRef(_3291);
    _3291 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(path) = 2 then*/
    if (IS_SEQUENCE(_path_6175)){
            _3295 = SEQ_PTR(_path_6175)->length;
    }
    else {
        _3295 = 1;
    }
    if (_3295 != 2)
    goto L4; // [68] 90

    /** 			if path[2] = ':' then*/
    _2 = (int)SEQ_PTR(_path_6175);
    _3297 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3297, 58)){
        _3297 = NOVALUE;
        goto L5; // [78] 89
    }
    _3297 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_path_6175);
    DeRef(_files_6177);
    DeRef(_3291);
    _3291 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(path)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_6177);
    _files_6177 = machine(22, _path_6175);

    /** 	if atom(files) then*/
    _3299 = IS_ATOM(_files_6177);
    if (_3299 == 0)
    {
        _3299 = NOVALUE;
        goto L6; // [106] 116
    }
    else{
        _3299 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_path_6175);
    DeRef(_files_6177);
    DeRef(_3291);
    _3291 = NOVALUE;
    return 0;
L6: 

    /** 	ifdef WINDOWS then*/

    /** 		if length( files ) < 3 then*/
    if (IS_SEQUENCE(_files_6177)){
            _3300 = SEQ_PTR(_files_6177)->length;
    }
    else {
        _3300 = 1;
    }
    if (_3300 >= 3)
    goto L7; // [123] 134

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_6175);
    DeRef(_files_6177);
    DeRef(_3291);
    _3291 = NOVALUE;
    return 0;
L7: 

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_6177);
    _3302 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3302);
    _3303 = (int)*(((s1_ptr)_2)->base + 1);
    _3302 = NOVALUE;
    if (_3303 == _3194)
    _3304 = 1;
    else if (IS_ATOM_INT(_3303) && IS_ATOM_INT(_3194))
    _3304 = 0;
    else
    _3304 = (compare(_3303, _3194) == 0);
    _3303 = NOVALUE;
    if (_3304 != 0)
    goto L8; // [150] 160
    _3304 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_6175);
    DeRef(_files_6177);
    DeRef(_3291);
    _3291 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_6177);
    _3306 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3306);
    _3307 = (int)*(((s1_ptr)_2)->base + 2);
    _3306 = NOVALUE;
    _3308 = find_from(100, _3307, 1);
    _3307 = NOVALUE;
    if (_3308 != 0)
    goto L9; // [177] 187
    _3308 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_path_6175);
    DeRef(_files_6177);
    DeRef(_3291);
    _3291 = NOVALUE;
    return 0;
L9: 

    /** 	ret = 1*/
    _ret_6178 = 1;

    /** 	path &= SLASH*/
    Append(&_path_6175, _path_6175, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_6177)){
            _3311 = SEQ_PTR(_files_6177)->length;
    }
    else {
        _3311 = 1;
    }
    {
        int _i_6217;
        _i_6217 = 3;
LA: 
        if (_i_6217 > _3311){
            goto LB; // [207] 348
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_6177);
        _3312 = (int)*(((s1_ptr)_2)->base + _i_6217);
        _2 = (int)SEQ_PTR(_3312);
        _3313 = (int)*(((s1_ptr)_2)->base + 2);
        _3312 = NOVALUE;
        _3314 = find_from(100, _3313, 1);
        _3313 = NOVALUE;
        if (_3314 == 0)
        {
            _3314 = NOVALUE;
            goto LC; // [231] 301
        }
        else{
            _3314 = NOVALUE;
        }

        /** 				if recurse then*/
        if (_recurse_6176 == 0)
        {
            goto LD; // [236] 343
        }
        else{
        }

        /** 					integer cnt = clear_directory(path & files[i][D_NAME], recurse)*/
        _2 = (int)SEQ_PTR(_files_6177);
        _3315 = (int)*(((s1_ptr)_2)->base + _i_6217);
        _2 = (int)SEQ_PTR(_3315);
        _3316 = (int)*(((s1_ptr)_2)->base + 1);
        _3315 = NOVALUE;
        if (IS_SEQUENCE(_path_6175) && IS_ATOM(_3316)) {
            Ref(_3316);
            Append(&_3317, _path_6175, _3316);
        }
        else if (IS_ATOM(_path_6175) && IS_SEQUENCE(_3316)) {
        }
        else {
            Concat((object_ptr)&_3317, _path_6175, _3316);
        }
        _3316 = NOVALUE;
        DeRef(_3318);
        _3318 = _recurse_6176;
        _cnt_6224 = _15clear_directory(_3317, _3318);
        _3317 = NOVALUE;
        _3318 = NOVALUE;
        if (!IS_ATOM_INT(_cnt_6224)) {
            _1 = (long)(DBL_PTR(_cnt_6224)->dbl);
            if (UNIQUE(DBL_PTR(_cnt_6224)) && (DBL_PTR(_cnt_6224)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_cnt_6224);
            _cnt_6224 = _1;
        }

        /** 					if cnt = 0 then*/
        if (_cnt_6224 != 0)
        goto LE; // [269] 280

        /** 						return 0*/
        DeRefDS(_path_6175);
        DeRef(_files_6177);
        DeRef(_3291);
        _3291 = NOVALUE;
        return 0;
LE: 

        /** 					ret += cnt*/
        _ret_6178 = _ret_6178 + _cnt_6224;
        goto LF; // [290] 341

        /** 					continue*/
        goto LD; // [295] 343
        goto LF; // [298] 341
LC: 

        /** 				if delete_file(path & files[i][D_NAME]) = 0 then*/
        _2 = (int)SEQ_PTR(_files_6177);
        _3322 = (int)*(((s1_ptr)_2)->base + _i_6217);
        _2 = (int)SEQ_PTR(_3322);
        _3323 = (int)*(((s1_ptr)_2)->base + 1);
        _3322 = NOVALUE;
        if (IS_SEQUENCE(_path_6175) && IS_ATOM(_3323)) {
            Ref(_3323);
            Append(&_3324, _path_6175, _3323);
        }
        else if (IS_ATOM(_path_6175) && IS_SEQUENCE(_3323)) {
        }
        else {
            Concat((object_ptr)&_3324, _path_6175, _3323);
        }
        _3323 = NOVALUE;
        _3325 = _15delete_file(_3324);
        _3324 = NOVALUE;
        if (binary_op_a(NOTEQ, _3325, 0)){
            DeRef(_3325);
            _3325 = NOVALUE;
            goto L10; // [321] 332
        }
        DeRef(_3325);
        _3325 = NOVALUE;

        /** 					return 0*/
        DeRefDS(_path_6175);
        DeRef(_files_6177);
        DeRef(_3291);
        _3291 = NOVALUE;
        return 0;
L10: 

        /** 				ret += 1*/
        _ret_6178 = _ret_6178 + 1;
LF: 

        /** 		end for*/
LD: 
        _i_6217 = _i_6217 + 1;
        goto LA; // [343] 214
LB: 
        ;
    }

    /** 	return ret*/
    DeRefDS(_path_6175);
    DeRef(_files_6177);
    DeRef(_3291);
    _3291 = NOVALUE;
    return _ret_6178;
    ;
}


int _15remove_directory(int _dir_name_6244, int _force_6245)
{
    int _pname_6246 = NOVALUE;
    int _ret_6247 = NOVALUE;
    int _files_6248 = NOVALUE;
    int _D_NAME_6249 = NOVALUE;
    int _D_ATTRIBUTES_6250 = NOVALUE;
    int _dir_inlined_dir_at_103_6271 = NOVALUE;
    int _3372 = NOVALUE;
    int _3368 = NOVALUE;
    int _3367 = NOVALUE;
    int _3366 = NOVALUE;
    int _3364 = NOVALUE;
    int _3363 = NOVALUE;
    int _3362 = NOVALUE;
    int _3361 = NOVALUE;
    int _3360 = NOVALUE;
    int _3359 = NOVALUE;
    int _3358 = NOVALUE;
    int _3357 = NOVALUE;
    int _3353 = NOVALUE;
    int _3351 = NOVALUE;
    int _3350 = NOVALUE;
    int _3349 = NOVALUE;
    int _3347 = NOVALUE;
    int _3346 = NOVALUE;
    int _3345 = NOVALUE;
    int _3343 = NOVALUE;
    int _3342 = NOVALUE;
    int _3340 = NOVALUE;
    int _3338 = NOVALUE;
    int _3336 = NOVALUE;
    int _3334 = NOVALUE;
    int _3333 = NOVALUE;
    int _3331 = NOVALUE;
    int _3330 = NOVALUE;
    int _3328 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_force_6245)) {
        _1 = (long)(DBL_PTR(_force_6245)->dbl);
        if (UNIQUE(DBL_PTR(_force_6245)) && (DBL_PTR(_force_6245)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_force_6245);
        _force_6245 = _1;
    }

    /** 	integer D_NAME = 1, D_ATTRIBUTES = 2*/
    _D_NAME_6249 = 1;
    _D_ATTRIBUTES_6250 = 2;

    /**  	if length(dir_name) > 0 then*/
    if (IS_SEQUENCE(_dir_name_6244)){
            _3328 = SEQ_PTR(_dir_name_6244)->length;
    }
    else {
        _3328 = 1;
    }
    if (_3328 <= 0)
    goto L1; // [24] 57

    /** 		if dir_name[$] = SLASH then*/
    if (IS_SEQUENCE(_dir_name_6244)){
            _3330 = SEQ_PTR(_dir_name_6244)->length;
    }
    else {
        _3330 = 1;
    }
    _2 = (int)SEQ_PTR(_dir_name_6244);
    _3331 = (int)*(((s1_ptr)_2)->base + _3330);
    if (binary_op_a(NOTEQ, _3331, 92)){
        _3331 = NOVALUE;
        goto L2; // [37] 56
    }
    _3331 = NOVALUE;

    /** 			dir_name = dir_name[1 .. $-1]*/
    if (IS_SEQUENCE(_dir_name_6244)){
            _3333 = SEQ_PTR(_dir_name_6244)->length;
    }
    else {
        _3333 = 1;
    }
    _3334 = _3333 - 1;
    _3333 = NOVALUE;
    rhs_slice_target = (object_ptr)&_dir_name_6244;
    RHS_Slice(_dir_name_6244, 1, _3334);
L2: 
L1: 

    /** 	if length(dir_name) = 0 then*/
    if (IS_SEQUENCE(_dir_name_6244)){
            _3336 = SEQ_PTR(_dir_name_6244)->length;
    }
    else {
        _3336 = 1;
    }
    if (_3336 != 0)
    goto L3; // [62] 73

    /** 		return 0	-- nothing specified to delete.*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_ret_6247);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return 0;
L3: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(dir_name) = 2 then*/
    if (IS_SEQUENCE(_dir_name_6244)){
            _3338 = SEQ_PTR(_dir_name_6244)->length;
    }
    else {
        _3338 = 1;
    }
    if (_3338 != 2)
    goto L4; // [80] 102

    /** 			if dir_name[2] = ':' then*/
    _2 = (int)SEQ_PTR(_dir_name_6244);
    _3340 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _3340, 58)){
        _3340 = NOVALUE;
        goto L5; // [90] 101
    }
    _3340 = NOVALUE;

    /** 				return 0 -- nothing specified to delete*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_ret_6247);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return 0;
L5: 
L4: 

    /** 	files = dir(dir_name)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_files_6248);
    _files_6248 = machine(22, _dir_name_6244);

    /** 	if atom(files) then*/
    _3342 = IS_ATOM(_files_6248);
    if (_3342 == 0)
    {
        _3342 = NOVALUE;
        goto L6; // [118] 128
    }
    else{
        _3342 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_ret_6247);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return 0;
L6: 

    /** 	if length( files ) < 2 then*/
    if (IS_SEQUENCE(_files_6248)){
            _3343 = SEQ_PTR(_files_6248)->length;
    }
    else {
        _3343 = 1;
    }
    if (_3343 >= 2)
    goto L7; // [133] 144

    /** 		return 0	-- Supplied dir_name was not a directory*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_ret_6247);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return 0;
L7: 

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(files[1][D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_files_6248);
    _3345 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3345);
    _3346 = (int)*(((s1_ptr)_2)->base + _D_NAME_6249);
    _3345 = NOVALUE;
    if (_3346 == _3194)
    _3347 = 1;
    else if (IS_ATOM_INT(_3346) && IS_ATOM_INT(_3194))
    _3347 = 0;
    else
    _3347 = (compare(_3346, _3194) == 0);
    _3346 = NOVALUE;
    if (_3347 != 0)
    goto L8; // [160] 170
    _3347 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_ret_6247);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return 0;
L8: 

    /** 		if not eu:find('d', files[1][D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_files_6248);
    _3349 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3349);
    _3350 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_6250);
    _3349 = NOVALUE;
    _3351 = find_from(100, _3350, 1);
    _3350 = NOVALUE;
    if (_3351 != 0)
    goto L9; // [185] 195
    _3351 = NOVALUE;

    /** 			return 0 -- Supplied name was not a directory*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_ret_6247);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return 0;
L9: 

    /** 		if length(files) > 2 then*/
    if (IS_SEQUENCE(_files_6248)){
            _3353 = SEQ_PTR(_files_6248)->length;
    }
    else {
        _3353 = 1;
    }
    if (_3353 <= 2)
    goto LA; // [200] 217

    /** 			if not force then*/
    if (_force_6245 != 0)
    goto LB; // [206] 216

    /** 				return 0 -- Directory is not already emptied.*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_ret_6247);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return 0;
LB: 
LA: 

    /** 	dir_name &= SLASH*/
    Append(&_dir_name_6244, _dir_name_6244, 92);

    /** 	ifdef WINDOWS then*/

    /** 		for i = 3 to length(files) do*/
    if (IS_SEQUENCE(_files_6248)){
            _3357 = SEQ_PTR(_files_6248)->length;
    }
    else {
        _3357 = 1;
    }
    {
        int _i_6294;
        _i_6294 = 3;
LC: 
        if (_i_6294 > _3357){
            goto LD; // [230] 322
        }

        /** 			if eu:find('d', files[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_files_6248);
        _3358 = (int)*(((s1_ptr)_2)->base + _i_6294);
        _2 = (int)SEQ_PTR(_3358);
        _3359 = (int)*(((s1_ptr)_2)->base + _D_ATTRIBUTES_6250);
        _3358 = NOVALUE;
        _3360 = find_from(100, _3359, 1);
        _3359 = NOVALUE;
        if (_3360 == 0)
        {
            _3360 = NOVALUE;
            goto LE; // [252] 282
        }
        else{
            _3360 = NOVALUE;
        }

        /** 				ret = remove_directory(dir_name & files[i][D_NAME] & SLASH, force)*/
        _2 = (int)SEQ_PTR(_files_6248);
        _3361 = (int)*(((s1_ptr)_2)->base + _i_6294);
        _2 = (int)SEQ_PTR(_3361);
        _3362 = (int)*(((s1_ptr)_2)->base + _D_NAME_6249);
        _3361 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = 92;
            concat_list[1] = _3362;
            concat_list[2] = _dir_name_6244;
            Concat_N((object_ptr)&_3363, concat_list, 3);
        }
        _3362 = NOVALUE;
        DeRef(_3364);
        _3364 = _force_6245;
        _0 = _ret_6247;
        _ret_6247 = _15remove_directory(_3363, _3364);
        DeRef(_0);
        _3363 = NOVALUE;
        _3364 = NOVALUE;
        goto LF; // [279] 301
LE: 

        /** 				ret = delete_file(dir_name & files[i][D_NAME])*/
        _2 = (int)SEQ_PTR(_files_6248);
        _3366 = (int)*(((s1_ptr)_2)->base + _i_6294);
        _2 = (int)SEQ_PTR(_3366);
        _3367 = (int)*(((s1_ptr)_2)->base + _D_NAME_6249);
        _3366 = NOVALUE;
        if (IS_SEQUENCE(_dir_name_6244) && IS_ATOM(_3367)) {
            Ref(_3367);
            Append(&_3368, _dir_name_6244, _3367);
        }
        else if (IS_ATOM(_dir_name_6244) && IS_SEQUENCE(_3367)) {
        }
        else {
            Concat((object_ptr)&_3368, _dir_name_6244, _3367);
        }
        _3367 = NOVALUE;
        _0 = _ret_6247;
        _ret_6247 = _15delete_file(_3368);
        DeRef(_0);
        _3368 = NOVALUE;
LF: 

        /** 			if not ret then*/
        if (IS_ATOM_INT(_ret_6247)) {
            if (_ret_6247 != 0){
                goto L10; // [305] 315
            }
        }
        else {
            if (DBL_PTR(_ret_6247)->dbl != 0.0){
                goto L10; // [305] 315
            }
        }

        /** 				return 0*/
        DeRefDS(_dir_name_6244);
        DeRef(_pname_6246);
        DeRef(_ret_6247);
        DeRef(_files_6248);
        DeRef(_3334);
        _3334 = NOVALUE;
        return 0;
L10: 

        /** 		end for*/
        _i_6294 = _i_6294 + 1;
        goto LC; // [317] 237
LD: 
        ;
    }

    /** 	pname = machine:allocate_string(dir_name)*/
    RefDS(_dir_name_6244);
    _0 = _pname_6246;
    _pname_6246 = _7allocate_string(_dir_name_6244, 0);
    DeRef(_0);

    /** 	ret = c_func(xRemoveDirectory, {pname})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pname_6246);
    *((int *)(_2+4)) = _pname_6246;
    _3372 = MAKE_SEQ(_1);
    DeRef(_ret_6247);
    _ret_6247 = call_c(1, _15xRemoveDirectory_5923, _3372);
    DeRefDS(_3372);
    _3372 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free(pname)*/
    Ref(_pname_6246);
    _7free(_pname_6246);

    /** 	return ret*/
    DeRefDS(_dir_name_6244);
    DeRef(_pname_6246);
    DeRef(_files_6248);
    DeRef(_3334);
    _3334 = NOVALUE;
    return _ret_6247;
    ;
}


int _15pathinfo(int _path_6326, int _std_slash_6327)
{
    int _slash_6328 = NOVALUE;
    int _period_6329 = NOVALUE;
    int _ch_6330 = NOVALUE;
    int _dir_name_6331 = NOVALUE;
    int _file_name_6332 = NOVALUE;
    int _file_ext_6333 = NOVALUE;
    int _file_full_6334 = NOVALUE;
    int _drive_id_6335 = NOVALUE;
    int _from_slash_6375 = NOVALUE;
    int _3410 = NOVALUE;
    int _3403 = NOVALUE;
    int _3402 = NOVALUE;
    int _3399 = NOVALUE;
    int _3398 = NOVALUE;
    int _3396 = NOVALUE;
    int _3395 = NOVALUE;
    int _3392 = NOVALUE;
    int _3391 = NOVALUE;
    int _3389 = NOVALUE;
    int _3385 = NOVALUE;
    int _3383 = NOVALUE;
    int _3382 = NOVALUE;
    int _3381 = NOVALUE;
    int _3380 = NOVALUE;
    int _3378 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_std_slash_6327)) {
        _1 = (long)(DBL_PTR(_std_slash_6327)->dbl);
        if (UNIQUE(DBL_PTR(_std_slash_6327)) && (DBL_PTR(_std_slash_6327)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_std_slash_6327);
        _std_slash_6327 = _1;
    }

    /** 	dir_name  = ""*/
    RefDS(_5);
    DeRef(_dir_name_6331);
    _dir_name_6331 = _5;

    /** 	file_name = ""*/
    RefDS(_5);
    DeRef(_file_name_6332);
    _file_name_6332 = _5;

    /** 	file_ext  = ""*/
    RefDS(_5);
    DeRef(_file_ext_6333);
    _file_ext_6333 = _5;

    /** 	file_full = ""*/
    RefDS(_5);
    DeRef(_file_full_6334);
    _file_full_6334 = _5;

    /** 	drive_id  = ""*/
    RefDS(_5);
    DeRef(_drive_id_6335);
    _drive_id_6335 = _5;

    /** 	slash = 0*/
    _slash_6328 = 0;

    /** 	period = 0*/
    _period_6329 = 0;

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_6326)){
            _3378 = SEQ_PTR(_path_6326)->length;
    }
    else {
        _3378 = 1;
    }
    {
        int _i_6337;
        _i_6337 = _3378;
L1: 
        if (_i_6337 < 1){
            goto L2; // [61] 134
        }

        /** 		ch = path[i]*/
        _2 = (int)SEQ_PTR(_path_6326);
        _ch_6330 = (int)*(((s1_ptr)_2)->base + _i_6337);
        if (!IS_ATOM_INT(_ch_6330))
        _ch_6330 = (long)DBL_PTR(_ch_6330)->dbl;

        /** 		if period = 0 and ch = '.' then*/
        _3380 = (_period_6329 == 0);
        if (_3380 == 0) {
            goto L3; // [82] 104
        }
        _3382 = (_ch_6330 == 46);
        if (_3382 == 0)
        {
            DeRef(_3382);
            _3382 = NOVALUE;
            goto L3; // [91] 104
        }
        else{
            DeRef(_3382);
            _3382 = NOVALUE;
        }

        /** 			period = i*/
        _period_6329 = _i_6337;
        goto L4; // [101] 127
L3: 

        /** 		elsif eu:find(ch, SLASHES) then*/
        _3383 = find_from(_ch_6330, _15SLASHES_5942, 1);
        if (_3383 == 0)
        {
            _3383 = NOVALUE;
            goto L5; // [111] 126
        }
        else{
            _3383 = NOVALUE;
        }

        /** 			slash = i*/
        _slash_6328 = _i_6337;

        /** 			exit*/
        goto L2; // [123] 134
L5: 
L4: 

        /** 	end for*/
        _i_6337 = _i_6337 + -1;
        goto L1; // [129] 68
L2: 
        ;
    }

    /** 	if slash > 0 then*/
    if (_slash_6328 <= 0)
    goto L6; // [136] 195

    /** 		dir_name = path[1..slash-1]*/
    _3385 = _slash_6328 - 1;
    rhs_slice_target = (object_ptr)&_dir_name_6331;
    RHS_Slice(_path_6326, 1, _3385);

    /** 		ifdef not UNIX then*/

    /** 			ch = eu:find(':', dir_name)*/
    _ch_6330 = find_from(58, _dir_name_6331, 1);

    /** 			if ch != 0 then*/
    if (_ch_6330 == 0)
    goto L7; // [164] 194

    /** 				drive_id = dir_name[1..ch-1]*/
    _3389 = _ch_6330 - 1;
    rhs_slice_target = (object_ptr)&_drive_id_6335;
    RHS_Slice(_dir_name_6331, 1, _3389);

    /** 				dir_name = dir_name[ch+1..$]*/
    _3391 = _ch_6330 + 1;
    if (IS_SEQUENCE(_dir_name_6331)){
            _3392 = SEQ_PTR(_dir_name_6331)->length;
    }
    else {
        _3392 = 1;
    }
    rhs_slice_target = (object_ptr)&_dir_name_6331;
    RHS_Slice(_dir_name_6331, _3391, _3392);
L7: 
L6: 

    /** 	if period > 0 then*/
    if (_period_6329 <= 0)
    goto L8; // [197] 241

    /** 		file_name = path[slash+1..period-1]*/
    _3395 = _slash_6328 + 1;
    if (_3395 > MAXINT){
        _3395 = NewDouble((double)_3395);
    }
    _3396 = _period_6329 - 1;
    rhs_slice_target = (object_ptr)&_file_name_6332;
    RHS_Slice(_path_6326, _3395, _3396);

    /** 		file_ext = path[period+1..$]*/
    _3398 = _period_6329 + 1;
    if (_3398 > MAXINT){
        _3398 = NewDouble((double)_3398);
    }
    if (IS_SEQUENCE(_path_6326)){
            _3399 = SEQ_PTR(_path_6326)->length;
    }
    else {
        _3399 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_ext_6333;
    RHS_Slice(_path_6326, _3398, _3399);

    /** 		file_full = file_name & '.' & file_ext*/
    {
        int concat_list[3];

        concat_list[0] = _file_ext_6333;
        concat_list[1] = 46;
        concat_list[2] = _file_name_6332;
        Concat_N((object_ptr)&_file_full_6334, concat_list, 3);
    }
    goto L9; // [238] 263
L8: 

    /** 		file_name = path[slash+1..$]*/
    _3402 = _slash_6328 + 1;
    if (_3402 > MAXINT){
        _3402 = NewDouble((double)_3402);
    }
    if (IS_SEQUENCE(_path_6326)){
            _3403 = SEQ_PTR(_path_6326)->length;
    }
    else {
        _3403 = 1;
    }
    rhs_slice_target = (object_ptr)&_file_name_6332;
    RHS_Slice(_path_6326, _3402, _3403);

    /** 		file_full = file_name*/
    RefDS(_file_name_6332);
    DeRef(_file_full_6334);
    _file_full_6334 = _file_name_6332;
L9: 

    /** 	if std_slash != 0 then*/
    if (_std_slash_6327 == 0)
    goto LA; // [265] 333

    /** 		if std_slash < 0 then*/
    if (_std_slash_6327 >= 0)
    goto LB; // [271] 309

    /** 			std_slash = SLASH*/
    _std_slash_6327 = 92;

    /** 			ifdef UNIX then*/

    /** 			sequence from_slash = "/"*/
    RefDS(_3168);
    DeRefi(_from_slash_6375);
    _from_slash_6375 = _3168;

    /** 			dir_name = search:match_replace(from_slash, dir_name, std_slash)*/
    RefDS(_from_slash_6375);
    RefDS(_dir_name_6331);
    _0 = _dir_name_6331;
    _dir_name_6331 = _14match_replace(_from_slash_6375, _dir_name_6331, 92, 0);
    DeRefDS(_0);
    DeRefDSi(_from_slash_6375);
    _from_slash_6375 = NOVALUE;
    goto LC; // [306] 332
LB: 

    /** 			dir_name = search:match_replace("\\", dir_name, std_slash)*/
    RefDS(_943);
    RefDS(_dir_name_6331);
    _0 = _dir_name_6331;
    _dir_name_6331 = _14match_replace(_943, _dir_name_6331, _std_slash_6327, 0);
    DeRefDS(_0);

    /** 			dir_name = search:match_replace("/", dir_name, std_slash)*/
    RefDS(_3168);
    RefDS(_dir_name_6331);
    _0 = _dir_name_6331;
    _dir_name_6331 = _14match_replace(_3168, _dir_name_6331, _std_slash_6327, 0);
    DeRefDS(_0);
LC: 
LA: 

    /** 	return {dir_name, file_full, file_name, file_ext, drive_id}*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_dir_name_6331);
    *((int *)(_2+4)) = _dir_name_6331;
    RefDS(_file_full_6334);
    *((int *)(_2+8)) = _file_full_6334;
    RefDS(_file_name_6332);
    *((int *)(_2+12)) = _file_name_6332;
    RefDS(_file_ext_6333);
    *((int *)(_2+16)) = _file_ext_6333;
    RefDS(_drive_id_6335);
    *((int *)(_2+20)) = _drive_id_6335;
    _3410 = MAKE_SEQ(_1);
    DeRefDS(_path_6326);
    DeRefDS(_dir_name_6331);
    DeRefDS(_file_name_6332);
    DeRefDS(_file_ext_6333);
    DeRefDS(_file_full_6334);
    DeRefDS(_drive_id_6335);
    DeRef(_3380);
    _3380 = NOVALUE;
    DeRef(_3385);
    _3385 = NOVALUE;
    DeRef(_3389);
    _3389 = NOVALUE;
    DeRef(_3391);
    _3391 = NOVALUE;
    DeRef(_3395);
    _3395 = NOVALUE;
    DeRef(_3396);
    _3396 = NOVALUE;
    DeRef(_3398);
    _3398 = NOVALUE;
    DeRef(_3402);
    _3402 = NOVALUE;
    return _3410;
    ;
}


int _15dirname(int _path_6383, int _pcd_6384)
{
    int _data_6385 = NOVALUE;
    int _3415 = NOVALUE;
    int _3413 = NOVALUE;
    int _3412 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pcd_6384)) {
        _1 = (long)(DBL_PTR(_pcd_6384)->dbl);
        if (UNIQUE(DBL_PTR(_pcd_6384)) && (DBL_PTR(_pcd_6384)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pcd_6384);
        _pcd_6384 = _1;
    }

    /** 	data = pathinfo(path)*/
    RefDS(_path_6383);
    _0 = _data_6385;
    _data_6385 = _15pathinfo(_path_6383, 0);
    DeRef(_0);

    /** 	if pcd then*/
    if (_pcd_6384 == 0)
    {
        goto L1; // [18] 42
    }
    else{
    }

    /** 		if length(data[1]) = 0 then*/
    _2 = (int)SEQ_PTR(_data_6385);
    _3412 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_3412)){
            _3413 = SEQ_PTR(_3412)->length;
    }
    else {
        _3413 = 1;
    }
    _3412 = NOVALUE;
    if (_3413 != 0)
    goto L2; // [30] 41

    /** 			return "."*/
    RefDS(_3194);
    DeRefDS(_path_6383);
    DeRefDS(_data_6385);
    _3412 = NOVALUE;
    return _3194;
L2: 
L1: 

    /** 	return data[1]*/
    _2 = (int)SEQ_PTR(_data_6385);
    _3415 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3415);
    DeRefDS(_path_6383);
    DeRefDS(_data_6385);
    _3412 = NOVALUE;
    return _3415;
    ;
}


int _15pathname(int _path_6395)
{
    int _data_6396 = NOVALUE;
    int _stop_6397 = NOVALUE;
    int _3420 = NOVALUE;
    int _3419 = NOVALUE;
    int _3417 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = canonical_path(path)*/
    RefDS(_path_6395);
    _0 = _data_6396;
    _data_6396 = _15canonical_path(_path_6395, 0, 0);
    DeRef(_0);

    /** 	stop = search:rfind(SLASH, data)*/
    if (IS_SEQUENCE(_data_6396)){
            _3417 = SEQ_PTR(_data_6396)->length;
    }
    else {
        _3417 = 1;
    }
    RefDS(_data_6396);
    _stop_6397 = _14rfind(92, _data_6396, _3417);
    _3417 = NOVALUE;
    if (!IS_ATOM_INT(_stop_6397)) {
        _1 = (long)(DBL_PTR(_stop_6397)->dbl);
        if (UNIQUE(DBL_PTR(_stop_6397)) && (DBL_PTR(_stop_6397)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stop_6397);
        _stop_6397 = _1;
    }

    /** 	return data[1 .. stop - 1]*/
    _3419 = _stop_6397 - 1;
    rhs_slice_target = (object_ptr)&_3420;
    RHS_Slice(_data_6396, 1, _3419);
    DeRefDS(_path_6395);
    DeRefDS(_data_6396);
    _3419 = NOVALUE;
    return _3420;
    ;
}


int _15filename(int _path_6406)
{
    int _data_6407 = NOVALUE;
    int _3422 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_6406);
    _0 = _data_6407;
    _data_6407 = _15pathinfo(_path_6406, 0);
    DeRef(_0);

    /** 	return data[2]*/
    _2 = (int)SEQ_PTR(_data_6407);
    _3422 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_3422);
    DeRefDS(_path_6406);
    DeRefDS(_data_6407);
    return _3422;
    ;
}


int _15filebase(int _path_6412)
{
    int _data_6413 = NOVALUE;
    int _3424 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_6412);
    _0 = _data_6413;
    _data_6413 = _15pathinfo(_path_6412, 0);
    DeRef(_0);

    /** 	return data[3]*/
    _2 = (int)SEQ_PTR(_data_6413);
    _3424 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_3424);
    DeRefDS(_path_6412);
    DeRefDS(_data_6413);
    return _3424;
    ;
}


int _15fileext(int _path_6418)
{
    int _data_6419 = NOVALUE;
    int _3426 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_6418);
    _0 = _data_6419;
    _data_6419 = _15pathinfo(_path_6418, 0);
    DeRef(_0);

    /** 	return data[4]*/
    _2 = (int)SEQ_PTR(_data_6419);
    _3426 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_3426);
    DeRefDS(_path_6418);
    DeRefDS(_data_6419);
    return _3426;
    ;
}


int _15driveid(int _path_6424)
{
    int _data_6425 = NOVALUE;
    int _3428 = NOVALUE;
    int _0, _1, _2;
    

    /** 	data = pathinfo(path)*/
    RefDS(_path_6424);
    _0 = _data_6425;
    _data_6425 = _15pathinfo(_path_6424, 0);
    DeRef(_0);

    /** 	return data[5]*/
    _2 = (int)SEQ_PTR(_data_6425);
    _3428 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_3428);
    DeRefDS(_path_6424);
    DeRefDS(_data_6425);
    return _3428;
    ;
}


int _15defaultext(int _path_6430, int _defext_6431)
{
    int _3441 = NOVALUE;
    int _3438 = NOVALUE;
    int _3436 = NOVALUE;
    int _3435 = NOVALUE;
    int _3434 = NOVALUE;
    int _3432 = NOVALUE;
    int _3431 = NOVALUE;
    int _3429 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(defext) = 0 then*/
    if (IS_SEQUENCE(_defext_6431)){
            _3429 = SEQ_PTR(_defext_6431)->length;
    }
    else {
        _3429 = 1;
    }
    if (_3429 != 0)
    goto L1; // [10] 21

    /** 		return path*/
    DeRefDS(_defext_6431);
    return _path_6430;
L1: 

    /** 	for i = length(path) to 1 by -1 do*/
    if (IS_SEQUENCE(_path_6430)){
            _3431 = SEQ_PTR(_path_6430)->length;
    }
    else {
        _3431 = 1;
    }
    {
        int _i_6436;
        _i_6436 = _3431;
L2: 
        if (_i_6436 < 1){
            goto L3; // [26] 95
        }

        /** 		if path[i] = '.' then*/
        _2 = (int)SEQ_PTR(_path_6430);
        _3432 = (int)*(((s1_ptr)_2)->base + _i_6436);
        if (binary_op_a(NOTEQ, _3432, 46)){
            _3432 = NOVALUE;
            goto L4; // [39] 50
        }
        _3432 = NOVALUE;

        /** 			return path*/
        DeRefDS(_defext_6431);
        return _path_6430;
L4: 

        /** 		if find(path[i], SLASHES) then*/
        _2 = (int)SEQ_PTR(_path_6430);
        _3434 = (int)*(((s1_ptr)_2)->base + _i_6436);
        _3435 = find_from(_3434, _15SLASHES_5942, 1);
        _3434 = NOVALUE;
        if (_3435 == 0)
        {
            _3435 = NOVALUE;
            goto L5; // [61] 88
        }
        else{
            _3435 = NOVALUE;
        }

        /** 			if i = length(path) then*/
        if (IS_SEQUENCE(_path_6430)){
                _3436 = SEQ_PTR(_path_6430)->length;
        }
        else {
            _3436 = 1;
        }
        if (_i_6436 != _3436)
        goto L3; // [69] 95

        /** 				return path*/
        DeRefDS(_defext_6431);
        return _path_6430;
        goto L6; // [79] 87

        /** 				exit*/
        goto L3; // [84] 95
L6: 
L5: 

        /** 	end for*/
        _i_6436 = _i_6436 + -1;
        goto L2; // [90] 33
L3: 
        ;
    }

    /** 	if defext[1] != '.' then*/
    _2 = (int)SEQ_PTR(_defext_6431);
    _3438 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _3438, 46)){
        _3438 = NOVALUE;
        goto L7; // [101] 112
    }
    _3438 = NOVALUE;

    /** 		path &= '.'*/
    Append(&_path_6430, _path_6430, 46);
L7: 

    /** 	return path & defext*/
    Concat((object_ptr)&_3441, _path_6430, _defext_6431);
    DeRefDS(_path_6430);
    DeRefDS(_defext_6431);
    return _3441;
    ;
}


int _15absolute_path(int _filename_6455)
{
    int _3453 = NOVALUE;
    int _3452 = NOVALUE;
    int _3450 = NOVALUE;
    int _3448 = NOVALUE;
    int _3446 = NOVALUE;
    int _3445 = NOVALUE;
    int _3444 = NOVALUE;
    int _3442 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(filename) = 0 then*/
    if (IS_SEQUENCE(_filename_6455)){
            _3442 = SEQ_PTR(_filename_6455)->length;
    }
    else {
        _3442 = 1;
    }
    if (_3442 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRefDS(_filename_6455);
    return 0;
L1: 

    /** 	if eu:find(filename[1], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_6455);
    _3444 = (int)*(((s1_ptr)_2)->base + 1);
    _3445 = find_from(_3444, _15SLASHES_5942, 1);
    _3444 = NOVALUE;
    if (_3445 == 0)
    {
        _3445 = NOVALUE;
        goto L2; // [30] 40
    }
    else{
        _3445 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_filename_6455);
    return 1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 1 then*/
    if (IS_SEQUENCE(_filename_6455)){
            _3446 = SEQ_PTR(_filename_6455)->length;
    }
    else {
        _3446 = 1;
    }
    if (_3446 != 1)
    goto L3; // [47] 58

    /** 			return 0*/
    DeRefDS(_filename_6455);
    return 0;
L3: 

    /** 		if filename[2] != ':' then*/
    _2 = (int)SEQ_PTR(_filename_6455);
    _3448 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(EQUALS, _3448, 58)){
        _3448 = NOVALUE;
        goto L4; // [64] 75
    }
    _3448 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_filename_6455);
    return 0;
L4: 

    /** 		if length(filename) < 3 then*/
    if (IS_SEQUENCE(_filename_6455)){
            _3450 = SEQ_PTR(_filename_6455)->length;
    }
    else {
        _3450 = 1;
    }
    if (_3450 >= 3)
    goto L5; // [80] 91

    /** 			return 0*/
    DeRefDS(_filename_6455);
    return 0;
L5: 

    /** 		if eu:find(filename[3], SLASHES) then*/
    _2 = (int)SEQ_PTR(_filename_6455);
    _3452 = (int)*(((s1_ptr)_2)->base + 3);
    _3453 = find_from(_3452, _15SLASHES_5942, 1);
    _3452 = NOVALUE;
    if (_3453 == 0)
    {
        _3453 = NOVALUE;
        goto L6; // [102] 112
    }
    else{
        _3453 = NOVALUE;
    }

    /** 			return 1*/
    DeRefDS(_filename_6455);
    return 1;
L6: 

    /** 	return 0*/
    DeRefDS(_filename_6455);
    return 0;
    ;
}


int _15case_flagset_type(int _x_6487)
{
    int _3462 = NOVALUE;
    int _3461 = NOVALUE;
    int _3460 = NOVALUE;
    int _3459 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_6487)) {
        _1 = (long)(DBL_PTR(_x_6487)->dbl);
        if (UNIQUE(DBL_PTR(_x_6487)) && (DBL_PTR(_x_6487)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_6487);
        _x_6487 = _1;
    }

    /** 	return x >= AS_IS and x < 2*TO_SHORT*/
    _3459 = (_x_6487 >= 0);
    _3460 = 8;
    _3461 = (_x_6487 < 8);
    _3460 = NOVALUE;
    _3462 = (_3459 != 0 && _3461 != 0);
    _3459 = NOVALUE;
    _3461 = NOVALUE;
    return _3462;
    ;
}


int _15canonical_path(int _path_in_6494, int _directory_given_6495, int _case_flags_6496)
{
    int _lPath_6497 = NOVALUE;
    int _lPosA_6498 = NOVALUE;
    int _lPosB_6499 = NOVALUE;
    int _lLevel_6500 = NOVALUE;
    int _lHome_6501 = NOVALUE;
    int _lDrive_6502 = NOVALUE;
    int _current_dir_inlined_current_dir_at_306_6562 = NOVALUE;
    int _driveid_inlined_driveid_at_313_6565 = NOVALUE;
    int _data_inlined_driveid_at_313_6564 = NOVALUE;
    int _wildcard_suffix_6567 = NOVALUE;
    int _first_wildcard_at_6568 = NOVALUE;
    int _last_slash_6571 = NOVALUE;
    int _sl_6643 = NOVALUE;
    int _short_name_6646 = NOVALUE;
    int _correct_name_6649 = NOVALUE;
    int _lower_name_6652 = NOVALUE;
    int _part_6668 = NOVALUE;
    int _list_6673 = NOVALUE;
    int _dir_inlined_dir_at_929_6677 = NOVALUE;
    int _name_inlined_dir_at_926_6676 = NOVALUE;
    int _supplied_name_6678 = NOVALUE;
    int _read_name_6697 = NOVALUE;
    int _read_name_6722 = NOVALUE;
    int _3675 = NOVALUE;
    int _3672 = NOVALUE;
    int _3668 = NOVALUE;
    int _3667 = NOVALUE;
    int _3665 = NOVALUE;
    int _3664 = NOVALUE;
    int _3663 = NOVALUE;
    int _3662 = NOVALUE;
    int _3661 = NOVALUE;
    int _3659 = NOVALUE;
    int _3658 = NOVALUE;
    int _3657 = NOVALUE;
    int _3656 = NOVALUE;
    int _3655 = NOVALUE;
    int _3654 = NOVALUE;
    int _3653 = NOVALUE;
    int _3652 = NOVALUE;
    int _3650 = NOVALUE;
    int _3649 = NOVALUE;
    int _3648 = NOVALUE;
    int _3647 = NOVALUE;
    int _3646 = NOVALUE;
    int _3645 = NOVALUE;
    int _3644 = NOVALUE;
    int _3643 = NOVALUE;
    int _3642 = NOVALUE;
    int _3640 = NOVALUE;
    int _3639 = NOVALUE;
    int _3638 = NOVALUE;
    int _3637 = NOVALUE;
    int _3636 = NOVALUE;
    int _3635 = NOVALUE;
    int _3634 = NOVALUE;
    int _3633 = NOVALUE;
    int _3632 = NOVALUE;
    int _3631 = NOVALUE;
    int _3630 = NOVALUE;
    int _3629 = NOVALUE;
    int _3628 = NOVALUE;
    int _3627 = NOVALUE;
    int _3626 = NOVALUE;
    int _3624 = NOVALUE;
    int _3623 = NOVALUE;
    int _3622 = NOVALUE;
    int _3621 = NOVALUE;
    int _3620 = NOVALUE;
    int _3618 = NOVALUE;
    int _3617 = NOVALUE;
    int _3616 = NOVALUE;
    int _3615 = NOVALUE;
    int _3614 = NOVALUE;
    int _3613 = NOVALUE;
    int _3612 = NOVALUE;
    int _3611 = NOVALUE;
    int _3610 = NOVALUE;
    int _3609 = NOVALUE;
    int _3608 = NOVALUE;
    int _3607 = NOVALUE;
    int _3606 = NOVALUE;
    int _3604 = NOVALUE;
    int _3603 = NOVALUE;
    int _3601 = NOVALUE;
    int _3600 = NOVALUE;
    int _3599 = NOVALUE;
    int _3598 = NOVALUE;
    int _3597 = NOVALUE;
    int _3595 = NOVALUE;
    int _3594 = NOVALUE;
    int _3593 = NOVALUE;
    int _3592 = NOVALUE;
    int _3591 = NOVALUE;
    int _3590 = NOVALUE;
    int _3588 = NOVALUE;
    int _3587 = NOVALUE;
    int _3586 = NOVALUE;
    int _3584 = NOVALUE;
    int _3583 = NOVALUE;
    int _3581 = NOVALUE;
    int _3580 = NOVALUE;
    int _3579 = NOVALUE;
    int _3577 = NOVALUE;
    int _3576 = NOVALUE;
    int _3574 = NOVALUE;
    int _3572 = NOVALUE;
    int _3570 = NOVALUE;
    int _3563 = NOVALUE;
    int _3560 = NOVALUE;
    int _3559 = NOVALUE;
    int _3558 = NOVALUE;
    int _3557 = NOVALUE;
    int _3551 = NOVALUE;
    int _3547 = NOVALUE;
    int _3546 = NOVALUE;
    int _3545 = NOVALUE;
    int _3544 = NOVALUE;
    int _3543 = NOVALUE;
    int _3541 = NOVALUE;
    int _3538 = NOVALUE;
    int _3537 = NOVALUE;
    int _3536 = NOVALUE;
    int _3535 = NOVALUE;
    int _3534 = NOVALUE;
    int _3533 = NOVALUE;
    int _3531 = NOVALUE;
    int _3530 = NOVALUE;
    int _3528 = NOVALUE;
    int _3526 = NOVALUE;
    int _3525 = NOVALUE;
    int _3524 = NOVALUE;
    int _3522 = NOVALUE;
    int _3521 = NOVALUE;
    int _3520 = NOVALUE;
    int _3519 = NOVALUE;
    int _3517 = NOVALUE;
    int _3515 = NOVALUE;
    int _3510 = NOVALUE;
    int _3508 = NOVALUE;
    int _3507 = NOVALUE;
    int _3506 = NOVALUE;
    int _3505 = NOVALUE;
    int _3504 = NOVALUE;
    int _3502 = NOVALUE;
    int _3501 = NOVALUE;
    int _3499 = NOVALUE;
    int _3498 = NOVALUE;
    int _3497 = NOVALUE;
    int _3496 = NOVALUE;
    int _3495 = NOVALUE;
    int _3494 = NOVALUE;
    int _3493 = NOVALUE;
    int _3490 = NOVALUE;
    int _3489 = NOVALUE;
    int _3487 = NOVALUE;
    int _3485 = NOVALUE;
    int _3483 = NOVALUE;
    int _3480 = NOVALUE;
    int _3479 = NOVALUE;
    int _3478 = NOVALUE;
    int _3477 = NOVALUE;
    int _3476 = NOVALUE;
    int _3474 = NOVALUE;
    int _3473 = NOVALUE;
    int _3472 = NOVALUE;
    int _3471 = NOVALUE;
    int _3470 = NOVALUE;
    int _3469 = NOVALUE;
    int _3468 = NOVALUE;
    int _3467 = NOVALUE;
    int _3466 = NOVALUE;
    int _3465 = NOVALUE;
    int _3464 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_directory_given_6495)) {
        _1 = (long)(DBL_PTR(_directory_given_6495)->dbl);
        if (UNIQUE(DBL_PTR(_directory_given_6495)) && (DBL_PTR(_directory_given_6495)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_directory_given_6495);
        _directory_given_6495 = _1;
    }
    if (!IS_ATOM_INT(_case_flags_6496)) {
        _1 = (long)(DBL_PTR(_case_flags_6496)->dbl);
        if (UNIQUE(DBL_PTR(_case_flags_6496)) && (DBL_PTR(_case_flags_6496)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_case_flags_6496);
        _case_flags_6496 = _1;
    }

    /**     sequence lPath = ""*/
    RefDS(_5);
    DeRef(_lPath_6497);
    _lPath_6497 = _5;

    /**     integer lPosA = -1*/
    _lPosA_6498 = -1;

    /**     integer lPosB = -1*/
    _lPosB_6499 = -1;

    /**     sequence lLevel = ""*/
    RefDS(_5);
    DeRefi(_lLevel_6500);
    _lLevel_6500 = _5;

    /**     path_in = path_in*/
    RefDS(_path_in_6494);
    DeRefDS(_path_in_6494);
    _path_in_6494 = _path_in_6494;

    /** 	ifdef UNIX then*/

    /** 	    sequence lDrive*/

    /** 	    lPath = match_replace("/", path_in, SLASH)*/
    RefDS(_3168);
    RefDS(_path_in_6494);
    _0 = _lPath_6497;
    _lPath_6497 = _14match_replace(_3168, _path_in_6494, 92, 0);
    DeRefDS(_0);

    /**     if (length(lPath) > 2 and lPath[1] = '"' and lPath[$] = '"') then*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3464 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3464 = 1;
    }
    _3465 = (_3464 > 2);
    _3464 = NOVALUE;
    if (_3465 == 0) {
        _3466 = 0;
        goto L1; // [68] 84
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3467 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3467)) {
        _3468 = (_3467 == 34);
    }
    else {
        _3468 = binary_op(EQUALS, _3467, 34);
    }
    _3467 = NOVALUE;
    if (IS_ATOM_INT(_3468))
    _3466 = (_3468 != 0);
    else
    _3466 = DBL_PTR(_3468)->dbl != 0.0;
L1: 
    if (_3466 == 0) {
        DeRef(_3469);
        _3469 = 0;
        goto L2; // [84] 103
    }
    if (IS_SEQUENCE(_lPath_6497)){
            _3470 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3470 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3471 = (int)*(((s1_ptr)_2)->base + _3470);
    if (IS_ATOM_INT(_3471)) {
        _3472 = (_3471 == 34);
    }
    else {
        _3472 = binary_op(EQUALS, _3471, 34);
    }
    _3471 = NOVALUE;
    if (IS_ATOM_INT(_3472))
    _3469 = (_3472 != 0);
    else
    _3469 = DBL_PTR(_3472)->dbl != 0.0;
L2: 
    if (_3469 == 0)
    {
        _3469 = NOVALUE;
        goto L3; // [103] 121
    }
    else{
        _3469 = NOVALUE;
    }

    /**         lPath = lPath[2..$-1]*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3473 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3473 = 1;
    }
    _3474 = _3473 - 1;
    _3473 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lPath_6497;
    RHS_Slice(_lPath_6497, 2, _3474);
L3: 

    /**     if (length(lPath) > 0 and lPath[1] = '~') then*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3476 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3476 = 1;
    }
    _3477 = (_3476 > 0);
    _3476 = NOVALUE;
    if (_3477 == 0) {
        DeRef(_3478);
        _3478 = 0;
        goto L4; // [130] 146
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3479 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3479)) {
        _3480 = (_3479 == 126);
    }
    else {
        _3480 = binary_op(EQUALS, _3479, 126);
    }
    _3479 = NOVALUE;
    if (IS_ATOM_INT(_3480))
    _3478 = (_3480 != 0);
    else
    _3478 = DBL_PTR(_3480)->dbl != 0.0;
L4: 
    if (_3478 == 0)
    {
        _3478 = NOVALUE;
        goto L5; // [146] 255
    }
    else{
        _3478 = NOVALUE;
    }

    /** 		lHome = getenv("HOME")*/
    DeRef(_lHome_6501);
    _lHome_6501 = EGetEnv(_3481);

    /** 		ifdef WINDOWS then*/

    /** 			if atom(lHome) then*/
    _3483 = IS_ATOM(_lHome_6501);
    if (_3483 == 0)
    {
        _3483 = NOVALUE;
        goto L6; // [161] 177
    }
    else{
        _3483 = NOVALUE;
    }

    /** 				lHome = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _3485 = EGetEnv(_3484);
    _3487 = EGetEnv(_3486);
    if (IS_SEQUENCE(_3485) && IS_ATOM(_3487)) {
        Ref(_3487);
        Append(&_lHome_6501, _3485, _3487);
    }
    else if (IS_ATOM(_3485) && IS_SEQUENCE(_3487)) {
        Ref(_3485);
        Prepend(&_lHome_6501, _3487, _3485);
    }
    else {
        Concat((object_ptr)&_lHome_6501, _3485, _3487);
        DeRef(_3485);
        _3485 = NOVALUE;
    }
    DeRef(_3485);
    _3485 = NOVALUE;
    DeRef(_3487);
    _3487 = NOVALUE;
L6: 

    /** 		if lHome[$] != SLASH then*/
    if (IS_SEQUENCE(_lHome_6501)){
            _3489 = SEQ_PTR(_lHome_6501)->length;
    }
    else {
        _3489 = 1;
    }
    _2 = (int)SEQ_PTR(_lHome_6501);
    _3490 = (int)*(((s1_ptr)_2)->base + _3489);
    if (binary_op_a(EQUALS, _3490, 92)){
        _3490 = NOVALUE;
        goto L7; // [186] 197
    }
    _3490 = NOVALUE;

    /** 			lHome &= SLASH*/
    if (IS_SEQUENCE(_lHome_6501) && IS_ATOM(92)) {
        Append(&_lHome_6501, _lHome_6501, 92);
    }
    else if (IS_ATOM(_lHome_6501) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_lHome_6501, _lHome_6501, 92);
    }
L7: 

    /** 		if length(lPath) > 1 and lPath[2] = SLASH then*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3493 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3493 = 1;
    }
    _3494 = (_3493 > 1);
    _3493 = NOVALUE;
    if (_3494 == 0) {
        goto L8; // [206] 239
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3496 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3496)) {
        _3497 = (_3496 == 92);
    }
    else {
        _3497 = binary_op(EQUALS, _3496, 92);
    }
    _3496 = NOVALUE;
    if (_3497 == 0) {
        DeRef(_3497);
        _3497 = NOVALUE;
        goto L8; // [219] 239
    }
    else {
        if (!IS_ATOM_INT(_3497) && DBL_PTR(_3497)->dbl == 0.0){
            DeRef(_3497);
            _3497 = NOVALUE;
            goto L8; // [219] 239
        }
        DeRef(_3497);
        _3497 = NOVALUE;
    }
    DeRef(_3497);
    _3497 = NOVALUE;

    /** 			lPath = lHome & lPath[3 .. $]*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3498 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3498 = 1;
    }
    rhs_slice_target = (object_ptr)&_3499;
    RHS_Slice(_lPath_6497, 3, _3498);
    if (IS_SEQUENCE(_lHome_6501) && IS_ATOM(_3499)) {
    }
    else if (IS_ATOM(_lHome_6501) && IS_SEQUENCE(_3499)) {
        Ref(_lHome_6501);
        Prepend(&_lPath_6497, _3499, _lHome_6501);
    }
    else {
        Concat((object_ptr)&_lPath_6497, _lHome_6501, _3499);
    }
    DeRefDS(_3499);
    _3499 = NOVALUE;
    goto L9; // [236] 254
L8: 

    /** 			lPath = lHome & lPath[2 .. $]*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3501 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3501 = 1;
    }
    rhs_slice_target = (object_ptr)&_3502;
    RHS_Slice(_lPath_6497, 2, _3501);
    if (IS_SEQUENCE(_lHome_6501) && IS_ATOM(_3502)) {
    }
    else if (IS_ATOM(_lHome_6501) && IS_SEQUENCE(_3502)) {
        Ref(_lHome_6501);
        Prepend(&_lPath_6497, _3502, _lHome_6501);
    }
    else {
        Concat((object_ptr)&_lPath_6497, _lHome_6501, _3502);
    }
    DeRefDS(_3502);
    _3502 = NOVALUE;
L9: 
L5: 

    /** 	ifdef WINDOWS then*/

    /** 	    if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3504 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3504 = 1;
    }
    _3505 = (_3504 > 1);
    _3504 = NOVALUE;
    if (_3505 == 0) {
        DeRef(_3506);
        _3506 = 0;
        goto LA; // [266] 282
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3507 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3507)) {
        _3508 = (_3507 == 58);
    }
    else {
        _3508 = binary_op(EQUALS, _3507, 58);
    }
    _3507 = NOVALUE;
    if (IS_ATOM_INT(_3508))
    _3506 = (_3508 != 0);
    else
    _3506 = DBL_PTR(_3508)->dbl != 0.0;
LA: 
    if (_3506 == 0)
    {
        _3506 = NOVALUE;
        goto LB; // [282] 305
    }
    else{
        _3506 = NOVALUE;
    }

    /** 			lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_6502;
    RHS_Slice(_lPath_6497, 1, 2);

    /** 			lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3510 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3510 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_6497;
    RHS_Slice(_lPath_6497, 3, _3510);
    goto LC; // [302] 339
LB: 

    /** 			lDrive = driveid(current_dir()) & ':'*/

    /** 	return machine_func(M_CURRENT_DIR, 0)*/
    DeRefi(_current_dir_inlined_current_dir_at_306_6562);
    _current_dir_inlined_current_dir_at_306_6562 = machine(23, 0);

    /** 	data = pathinfo(path)*/
    RefDS(_current_dir_inlined_current_dir_at_306_6562);
    _0 = _data_inlined_driveid_at_313_6564;
    _data_inlined_driveid_at_313_6564 = _15pathinfo(_current_dir_inlined_current_dir_at_306_6562, 0);
    DeRef(_0);

    /** 	return data[5]*/
    DeRef(_driveid_inlined_driveid_at_313_6565);
    _2 = (int)SEQ_PTR(_data_inlined_driveid_at_313_6564);
    _driveid_inlined_driveid_at_313_6565 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_driveid_inlined_driveid_at_313_6565);
    DeRef(_data_inlined_driveid_at_313_6564);
    _data_inlined_driveid_at_313_6564 = NOVALUE;
    if (IS_SEQUENCE(_driveid_inlined_driveid_at_313_6565) && IS_ATOM(58)) {
        Append(&_lDrive_6502, _driveid_inlined_driveid_at_313_6565, 58);
    }
    else if (IS_ATOM(_driveid_inlined_driveid_at_313_6565) && IS_SEQUENCE(58)) {
    }
    else {
        Concat((object_ptr)&_lDrive_6502, _driveid_inlined_driveid_at_313_6565, 58);
    }
LC: 

    /** 	sequence wildcard_suffix*/

    /** 	integer first_wildcard_at = find_first_wildcard( lPath )*/
    RefDS(_lPath_6497);
    _first_wildcard_at_6568 = _15find_first_wildcard(_lPath_6497, 1);
    if (!IS_ATOM_INT(_first_wildcard_at_6568)) {
        _1 = (long)(DBL_PTR(_first_wildcard_at_6568)->dbl);
        if (UNIQUE(DBL_PTR(_first_wildcard_at_6568)) && (DBL_PTR(_first_wildcard_at_6568)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_first_wildcard_at_6568);
        _first_wildcard_at_6568 = _1;
    }

    /** 	if first_wildcard_at then*/
    if (_first_wildcard_at_6568 == 0)
    {
        goto LD; // [354] 417
    }
    else{
    }

    /** 		integer last_slash = search:rfind( SLASH, lPath, first_wildcard_at )*/
    RefDS(_lPath_6497);
    _last_slash_6571 = _14rfind(92, _lPath_6497, _first_wildcard_at_6568);
    if (!IS_ATOM_INT(_last_slash_6571)) {
        _1 = (long)(DBL_PTR(_last_slash_6571)->dbl);
        if (UNIQUE(DBL_PTR(_last_slash_6571)) && (DBL_PTR(_last_slash_6571)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_last_slash_6571);
        _last_slash_6571 = _1;
    }

    /** 		if last_slash then*/
    if (_last_slash_6571 == 0)
    {
        goto LE; // [371] 397
    }
    else{
    }

    /** 			wildcard_suffix = lPath[last_slash..$]*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3515 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3515 = 1;
    }
    rhs_slice_target = (object_ptr)&_wildcard_suffix_6567;
    RHS_Slice(_lPath_6497, _last_slash_6571, _3515);

    /** 			lPath = remove( lPath, last_slash, length( lPath ) )*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3517 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3517 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_6497);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_last_slash_6571)) ? _last_slash_6571 : (long)(DBL_PTR(_last_slash_6571)->dbl);
        int stop = (IS_ATOM_INT(_3517)) ? _3517 : (long)(DBL_PTR(_3517)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_6497), start, &_lPath_6497 );
            }
            else Tail(SEQ_PTR(_lPath_6497), stop+1, &_lPath_6497);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_6497), start, &_lPath_6497);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_6497 = Remove_elements(start, stop, (SEQ_PTR(_lPath_6497)->ref == 1));
        }
    }
    _3517 = NOVALUE;
    goto LF; // [394] 412
LE: 

    /** 			wildcard_suffix = lPath*/
    RefDS(_lPath_6497);
    DeRef(_wildcard_suffix_6567);
    _wildcard_suffix_6567 = _lPath_6497;

    /** 			lPath = ""*/
    RefDS(_5);
    DeRefDS(_lPath_6497);
    _lPath_6497 = _5;
LF: 
    goto L10; // [414] 425
LD: 

    /** 		wildcard_suffix = ""*/
    RefDS(_5);
    DeRef(_wildcard_suffix_6567);
    _wildcard_suffix_6567 = _5;
L10: 

    /** 	if ((length(lPath) = 0) or not find(lPath[1], "/\\")) then*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3519 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3519 = 1;
    }
    _3520 = (_3519 == 0);
    _3519 = NOVALUE;
    if (_3520 != 0) {
        DeRef(_3521);
        _3521 = 1;
        goto L11; // [434] 454
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3522 = (int)*(((s1_ptr)_2)->base + 1);
    _3524 = find_from(_3522, _3523, 1);
    _3522 = NOVALUE;
    _3525 = (_3524 == 0);
    _3524 = NOVALUE;
    _3521 = (_3525 != 0);
L11: 
    if (_3521 == 0)
    {
        _3521 = NOVALUE;
        goto L12; // [454] 555
    }
    else{
        _3521 = NOVALUE;
    }

    /** 		ifdef UNIX then*/

    /** 			if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_6502)){
            _3526 = SEQ_PTR(_lDrive_6502)->length;
    }
    else {
        _3526 = 1;
    }
    if (_3526 != 0)
    goto L13; // [466] 483

    /** 				lPath = curdir() & lPath*/
    _3528 = _15curdir(0);
    if (IS_SEQUENCE(_3528) && IS_ATOM(_lPath_6497)) {
    }
    else if (IS_ATOM(_3528) && IS_SEQUENCE(_lPath_6497)) {
        Ref(_3528);
        Prepend(&_lPath_6497, _lPath_6497, _3528);
    }
    else {
        Concat((object_ptr)&_lPath_6497, _3528, _lPath_6497);
        DeRef(_3528);
        _3528 = NOVALUE;
    }
    DeRef(_3528);
    _3528 = NOVALUE;
    goto L14; // [480] 498
L13: 

    /** 				lPath = curdir(lDrive[1]) & lPath*/
    _2 = (int)SEQ_PTR(_lDrive_6502);
    _3530 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3530);
    _3531 = _15curdir(_3530);
    _3530 = NOVALUE;
    if (IS_SEQUENCE(_3531) && IS_ATOM(_lPath_6497)) {
    }
    else if (IS_ATOM(_3531) && IS_SEQUENCE(_lPath_6497)) {
        Ref(_3531);
        Prepend(&_lPath_6497, _lPath_6497, _3531);
    }
    else {
        Concat((object_ptr)&_lPath_6497, _3531, _lPath_6497);
        DeRef(_3531);
        _3531 = NOVALUE;
    }
    DeRef(_3531);
    _3531 = NOVALUE;
L14: 

    /** 			if ( (length(lPath) > 1) and (lPath[2] = ':' ) ) then*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3533 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3533 = 1;
    }
    _3534 = (_3533 > 1);
    _3533 = NOVALUE;
    if (_3534 == 0) {
        DeRef(_3535);
        _3535 = 0;
        goto L15; // [507] 523
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3536 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3536)) {
        _3537 = (_3536 == 58);
    }
    else {
        _3537 = binary_op(EQUALS, _3536, 58);
    }
    _3536 = NOVALUE;
    if (IS_ATOM_INT(_3537))
    _3535 = (_3537 != 0);
    else
    _3535 = DBL_PTR(_3537)->dbl != 0.0;
L15: 
    if (_3535 == 0)
    {
        _3535 = NOVALUE;
        goto L16; // [523] 554
    }
    else{
        _3535 = NOVALUE;
    }

    /** 				if (length(lDrive) = 0) then*/
    if (IS_SEQUENCE(_lDrive_6502)){
            _3538 = SEQ_PTR(_lDrive_6502)->length;
    }
    else {
        _3538 = 1;
    }
    if (_3538 != 0)
    goto L17; // [531] 543

    /** 					lDrive = lPath[1..2]*/
    rhs_slice_target = (object_ptr)&_lDrive_6502;
    RHS_Slice(_lPath_6497, 1, 2);
L17: 

    /** 				lPath = lPath[3..$]*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3541 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3541 = 1;
    }
    rhs_slice_target = (object_ptr)&_lPath_6497;
    RHS_Slice(_lPath_6497, 3, _3541);
L16: 
L12: 

    /** 	if ((directory_given != 0) and (lPath[$] != SLASH) ) then*/
    _3543 = (_directory_given_6495 != 0);
    if (_3543 == 0) {
        DeRef(_3544);
        _3544 = 0;
        goto L18; // [561] 580
    }
    if (IS_SEQUENCE(_lPath_6497)){
            _3545 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3545 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3546 = (int)*(((s1_ptr)_2)->base + _3545);
    if (IS_ATOM_INT(_3546)) {
        _3547 = (_3546 != 92);
    }
    else {
        _3547 = binary_op(NOTEQ, _3546, 92);
    }
    _3546 = NOVALUE;
    if (IS_ATOM_INT(_3547))
    _3544 = (_3547 != 0);
    else
    _3544 = DBL_PTR(_3547)->dbl != 0.0;
L18: 
    if (_3544 == 0)
    {
        _3544 = NOVALUE;
        goto L19; // [580] 590
    }
    else{
        _3544 = NOVALUE;
    }

    /** 		lPath &= SLASH*/
    Append(&_lPath_6497, _lPath_6497, 92);
L19: 

    /** 	lLevel = SLASH & '.' & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = 46;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_6500, concat_list, 3);
    }

    /** 	lPosA = 1*/
    _lPosA_6498 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1A; // [607] 628
L1B: 
    if (_lPosA_6498 == 0)
    goto L1C; // [610] 642

    /** 		lPath = eu:remove(lPath, lPosA, lPosA + 1)*/
    _3551 = _lPosA_6498 + 1;
    if (_3551 > MAXINT){
        _3551 = NewDouble((double)_3551);
    }
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_6497);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosA_6498)) ? _lPosA_6498 : (long)(DBL_PTR(_lPosA_6498)->dbl);
        int stop = (IS_ATOM_INT(_3551)) ? _3551 : (long)(DBL_PTR(_3551)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_6497), start, &_lPath_6497 );
            }
            else Tail(SEQ_PTR(_lPath_6497), stop+1, &_lPath_6497);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_6497), start, &_lPath_6497);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_6497 = Remove_elements(start, stop, (SEQ_PTR(_lPath_6497)->ref == 1));
        }
    }
    DeRef(_3551);
    _3551 = NOVALUE;

    /** 	  entry*/
L1A: 

    /** 		lPosA = match(lLevel, lPath, lPosA )*/
    _lPosA_6498 = e_match_from(_lLevel_6500, _lPath_6497, _lPosA_6498);

    /** 	end while*/
    goto L1B; // [639] 610
L1C: 

    /** 	lLevel = SLASH & ".." & SLASH*/
    {
        int concat_list[3];

        concat_list[0] = 92;
        concat_list[1] = _3226;
        concat_list[2] = 92;
        Concat_N((object_ptr)&_lLevel_6500, concat_list, 3);
    }

    /** 	lPosB = 1*/
    _lPosB_6499 = 1;

    /** 	while( lPosA != 0 ) with entry do*/
    goto L1D; // [659] 743
L1E: 
    if (_lPosA_6498 == 0)
    goto L1F; // [662] 757

    /** 		lPosB = lPosA-1*/
    _lPosB_6499 = _lPosA_6498 - 1;

    /** 		while((lPosB > 0) and (lPath[lPosB] != SLASH)) do*/
L20: 
    _3557 = (_lPosB_6499 > 0);
    if (_3557 == 0) {
        DeRef(_3558);
        _3558 = 0;
        goto L21; // [683] 699
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3559 = (int)*(((s1_ptr)_2)->base + _lPosB_6499);
    if (IS_ATOM_INT(_3559)) {
        _3560 = (_3559 != 92);
    }
    else {
        _3560 = binary_op(NOTEQ, _3559, 92);
    }
    _3559 = NOVALUE;
    if (IS_ATOM_INT(_3560))
    _3558 = (_3560 != 0);
    else
    _3558 = DBL_PTR(_3560)->dbl != 0.0;
L21: 
    if (_3558 == 0)
    {
        _3558 = NOVALUE;
        goto L22; // [699] 715
    }
    else{
        _3558 = NOVALUE;
    }

    /** 			lPosB -= 1*/
    _lPosB_6499 = _lPosB_6499 - 1;

    /** 		end while*/
    goto L20; // [712] 679
L22: 

    /** 		if (lPosB <= 0) then*/
    if (_lPosB_6499 > 0)
    goto L23; // [717] 729

    /** 			lPosB = 1*/
    _lPosB_6499 = 1;
L23: 

    /** 		lPath = eu:remove(lPath, lPosB, lPosA + 2)*/
    _3563 = _lPosA_6498 + 2;
    if ((long)((unsigned long)_3563 + (unsigned long)HIGH_BITS) >= 0) 
    _3563 = NewDouble((double)_3563);
    {
        s1_ptr assign_space = SEQ_PTR(_lPath_6497);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_lPosB_6499)) ? _lPosB_6499 : (long)(DBL_PTR(_lPosB_6499)->dbl);
        int stop = (IS_ATOM_INT(_3563)) ? _3563 : (long)(DBL_PTR(_3563)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_lPath_6497), start, &_lPath_6497 );
            }
            else Tail(SEQ_PTR(_lPath_6497), stop+1, &_lPath_6497);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_lPath_6497), start, &_lPath_6497);
        }
        else {
            assign_slice_seq = &assign_space;
            _lPath_6497 = Remove_elements(start, stop, (SEQ_PTR(_lPath_6497)->ref == 1));
        }
    }
    DeRef(_3563);
    _3563 = NOVALUE;

    /** 	  entry*/
L1D: 

    /** 		lPosA = match(lLevel, lPath, lPosB )*/
    _lPosA_6498 = e_match_from(_lLevel_6500, _lPath_6497, _lPosB_6499);

    /** 	end while*/
    goto L1E; // [754] 662
L1F: 

    /** 	if case_flags = TO_LOWER then*/
    if (_case_flags_6496 != 1)
    goto L24; // [761] 776

    /** 		lPath = lower( lPath )*/
    RefDS(_lPath_6497);
    _0 = _lPath_6497;
    _lPath_6497 = _12lower(_lPath_6497);
    DeRefDS(_0);
    goto L25; // [773] 1437
L24: 

    /** 	elsif case_flags != AS_IS then*/
    if (_case_flags_6496 == 0)
    goto L26; // [780] 1434

    /** 		sequence sl = find_all(SLASH,lPath) -- split apart lPath*/
    RefDS(_lPath_6497);
    _0 = _sl_6643;
    _sl_6643 = _14find_all(92, _lPath_6497, 1);
    DeRef(_0);

    /** 		integer short_name = and_bits(TO_SHORT,case_flags)=TO_SHORT*/
    {unsigned long tu;
         tu = (unsigned long)4 & (unsigned long)_case_flags_6496;
         _3570 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3570)) {
        _short_name_6646 = (_3570 == 4);
    }
    else {
        _short_name_6646 = (DBL_PTR(_3570)->dbl == (double)4);
    }
    DeRef(_3570);
    _3570 = NOVALUE;

    /** 		integer correct_name = and_bits(case_flags,CORRECT)=CORRECT*/
    {unsigned long tu;
         tu = (unsigned long)_case_flags_6496 & (unsigned long)2;
         _3572 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3572)) {
        _correct_name_6649 = (_3572 == 2);
    }
    else {
        _correct_name_6649 = (DBL_PTR(_3572)->dbl == (double)2);
    }
    DeRef(_3572);
    _3572 = NOVALUE;

    /** 		integer lower_name = and_bits(TO_LOWER,case_flags)=TO_LOWER*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_6496;
         _3574 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3574)) {
        _lower_name_6652 = (_3574 == 1);
    }
    else {
        _lower_name_6652 = (DBL_PTR(_3574)->dbl == (double)1);
    }
    DeRef(_3574);
    _3574 = NOVALUE;

    /** 		if lPath[$] != SLASH then*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3576 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3576 = 1;
    }
    _2 = (int)SEQ_PTR(_lPath_6497);
    _3577 = (int)*(((s1_ptr)_2)->base + _3576);
    if (binary_op_a(EQUALS, _3577, 92)){
        _3577 = NOVALUE;
        goto L27; // [857] 879
    }
    _3577 = NOVALUE;

    /** 			sl = sl & {length(lPath)+1}*/
    if (IS_SEQUENCE(_lPath_6497)){
            _3579 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3579 = 1;
    }
    _3580 = _3579 + 1;
    _3579 = NOVALUE;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _3580;
    _3581 = MAKE_SEQ(_1);
    _3580 = NOVALUE;
    Concat((object_ptr)&_sl_6643, _sl_6643, _3581);
    DeRefDS(_3581);
    _3581 = NOVALUE;
L27: 

    /** 		for i = length(sl)-1 to 1 by -1 label "partloop" do*/
    if (IS_SEQUENCE(_sl_6643)){
            _3583 = SEQ_PTR(_sl_6643)->length;
    }
    else {
        _3583 = 1;
    }
    _3584 = _3583 - 1;
    _3583 = NOVALUE;
    {
        int _i_6664;
        _i_6664 = _3584;
L28: 
        if (_i_6664 < 1){
            goto L29; // [888] 1393
        }

        /** 			ifdef WINDOWS then*/

        /** 				sequence part = lDrive & lPath[1..sl[i]-1]*/
        _2 = (int)SEQ_PTR(_sl_6643);
        _3586 = (int)*(((s1_ptr)_2)->base + _i_6664);
        if (IS_ATOM_INT(_3586)) {
            _3587 = _3586 - 1;
        }
        else {
            _3587 = binary_op(MINUS, _3586, 1);
        }
        _3586 = NOVALUE;
        rhs_slice_target = (object_ptr)&_3588;
        RHS_Slice(_lPath_6497, 1, _3587);
        Concat((object_ptr)&_part_6668, _lDrive_6502, _3588);
        DeRefDS(_3588);
        _3588 = NOVALUE;

        /** 			object list = dir( part & SLASH )*/
        Append(&_3590, _part_6668, 92);
        DeRef(_name_inlined_dir_at_926_6676);
        _name_inlined_dir_at_926_6676 = _3590;
        _3590 = NOVALUE;

        /** 	ifdef WINDOWS then*/

        /** 		return machine_func(M_DIR, name)*/
        DeRef(_list_6673);
        _list_6673 = machine(22, _name_inlined_dir_at_926_6676);
        DeRef(_name_inlined_dir_at_926_6676);
        _name_inlined_dir_at_926_6676 = NOVALUE;

        /** 			sequence supplied_name = lPath[sl[i]+1..sl[i+1]-1]*/
        _2 = (int)SEQ_PTR(_sl_6643);
        _3591 = (int)*(((s1_ptr)_2)->base + _i_6664);
        if (IS_ATOM_INT(_3591)) {
            _3592 = _3591 + 1;
            if (_3592 > MAXINT){
                _3592 = NewDouble((double)_3592);
            }
        }
        else
        _3592 = binary_op(PLUS, 1, _3591);
        _3591 = NOVALUE;
        _3593 = _i_6664 + 1;
        _2 = (int)SEQ_PTR(_sl_6643);
        _3594 = (int)*(((s1_ptr)_2)->base + _3593);
        if (IS_ATOM_INT(_3594)) {
            _3595 = _3594 - 1;
        }
        else {
            _3595 = binary_op(MINUS, _3594, 1);
        }
        _3594 = NOVALUE;
        rhs_slice_target = (object_ptr)&_supplied_name_6678;
        RHS_Slice(_lPath_6497, _3592, _3595);

        /** 			if atom(list) then*/
        _3597 = IS_ATOM(_list_6673);
        if (_3597 == 0)
        {
            _3597 = NOVALUE;
            goto L2A; // [974] 1012
        }
        else{
            _3597 = NOVALUE;
        }

        /** 				if lower_name then*/
        if (_lower_name_6652 == 0)
        {
            goto L2B; // [979] 1005
        }
        else{
        }

        /** 					lPath = part & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_6643);
        _3598 = (int)*(((s1_ptr)_2)->base + _i_6664);
        if (IS_SEQUENCE(_lPath_6497)){
                _3599 = SEQ_PTR(_lPath_6497)->length;
        }
        else {
            _3599 = 1;
        }
        rhs_slice_target = (object_ptr)&_3600;
        RHS_Slice(_lPath_6497, _3598, _3599);
        _3601 = _12lower(_3600);
        _3600 = NOVALUE;
        if (IS_SEQUENCE(_part_6668) && IS_ATOM(_3601)) {
            Ref(_3601);
            Append(&_lPath_6497, _part_6668, _3601);
        }
        else if (IS_ATOM(_part_6668) && IS_SEQUENCE(_3601)) {
        }
        else {
            Concat((object_ptr)&_lPath_6497, _part_6668, _3601);
        }
        DeRef(_3601);
        _3601 = NOVALUE;
L2B: 

        /** 				continue*/
        DeRef(_part_6668);
        _part_6668 = NOVALUE;
        DeRef(_list_6673);
        _list_6673 = NOVALUE;
        DeRef(_supplied_name_6678);
        _supplied_name_6678 = NOVALUE;
        goto L2C; // [1009] 1388
L2A: 

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_6673)){
                _3603 = SEQ_PTR(_list_6673)->length;
        }
        else {
            _3603 = 1;
        }
        {
            int _j_6695;
            _j_6695 = 1;
L2D: 
            if (_j_6695 > _3603){
                goto L2E; // [1017] 1148
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_6673);
            _3604 = (int)*(((s1_ptr)_2)->base + _j_6695);
            DeRef(_read_name_6697);
            _2 = (int)SEQ_PTR(_3604);
            _read_name_6697 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_6697);
            _3604 = NOVALUE;

            /** 				if equal(read_name, supplied_name) then*/
            if (_read_name_6697 == _supplied_name_6678)
            _3606 = 1;
            else if (IS_ATOM_INT(_read_name_6697) && IS_ATOM_INT(_supplied_name_6678))
            _3606 = 0;
            else
            _3606 = (compare(_read_name_6697, _supplied_name_6678) == 0);
            if (_3606 == 0)
            {
                _3606 = NOVALUE;
                goto L2F; // [1044] 1139
            }
            else{
                _3606 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_6646 == 0) {
                goto L30; // [1049] 1130
            }
            _2 = (int)SEQ_PTR(_list_6673);
            _3608 = (int)*(((s1_ptr)_2)->base + _j_6695);
            _2 = (int)SEQ_PTR(_3608);
            _3609 = (int)*(((s1_ptr)_2)->base + 11);
            _3608 = NOVALUE;
            _3610 = IS_SEQUENCE(_3609);
            _3609 = NOVALUE;
            if (_3610 == 0)
            {
                _3610 = NOVALUE;
                goto L30; // [1067] 1130
            }
            else{
                _3610 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_6643);
            _3611 = (int)*(((s1_ptr)_2)->base + _i_6664);
            rhs_slice_target = (object_ptr)&_3612;
            RHS_Slice(_lPath_6497, 1, _3611);
            _2 = (int)SEQ_PTR(_list_6673);
            _3613 = (int)*(((s1_ptr)_2)->base + _j_6695);
            _2 = (int)SEQ_PTR(_3613);
            _3614 = (int)*(((s1_ptr)_2)->base + 11);
            _3613 = NOVALUE;
            _3615 = _i_6664 + 1;
            _2 = (int)SEQ_PTR(_sl_6643);
            _3616 = (int)*(((s1_ptr)_2)->base + _3615);
            if (IS_SEQUENCE(_lPath_6497)){
                    _3617 = SEQ_PTR(_lPath_6497)->length;
            }
            else {
                _3617 = 1;
            }
            rhs_slice_target = (object_ptr)&_3618;
            RHS_Slice(_lPath_6497, _3616, _3617);
            {
                int concat_list[3];

                concat_list[0] = _3618;
                concat_list[1] = _3614;
                concat_list[2] = _3612;
                Concat_N((object_ptr)&_lPath_6497, concat_list, 3);
            }
            DeRefDS(_3618);
            _3618 = NOVALUE;
            _3614 = NOVALUE;
            DeRefDS(_3612);
            _3612 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_6643)){
                    _3620 = SEQ_PTR(_sl_6643)->length;
            }
            else {
                _3620 = 1;
            }
            if (IS_SEQUENCE(_lPath_6497)){
                    _3621 = SEQ_PTR(_lPath_6497)->length;
            }
            else {
                _3621 = 1;
            }
            _3622 = _3621 + 1;
            _3621 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_6643);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_6643 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _3620);
            _1 = *(int *)_2;
            *(int *)_2 = _3622;
            if( _1 != _3622 ){
                DeRef(_1);
            }
            _3622 = NOVALUE;
L30: 

            /** 					continue "partloop"*/
            DeRef(_read_name_6697);
            _read_name_6697 = NOVALUE;
            DeRef(_part_6668);
            _part_6668 = NOVALUE;
            DeRef(_list_6673);
            _list_6673 = NOVALUE;
            DeRef(_supplied_name_6678);
            _supplied_name_6678 = NOVALUE;
            goto L2C; // [1136] 1388
L2F: 
            DeRef(_read_name_6697);
            _read_name_6697 = NOVALUE;

            /** 			end for*/
            _j_6695 = _j_6695 + 1;
            goto L2D; // [1143] 1024
L2E: 
            ;
        }

        /** 			for j = 1 to length(list) do*/
        if (IS_SEQUENCE(_list_6673)){
                _3623 = SEQ_PTR(_list_6673)->length;
        }
        else {
            _3623 = 1;
        }
        {
            int _j_6720;
            _j_6720 = 1;
L31: 
            if (_j_6720 > _3623){
                goto L32; // [1153] 1331
            }

            /** 				sequence read_name = list[j][D_NAME]*/
            _2 = (int)SEQ_PTR(_list_6673);
            _3624 = (int)*(((s1_ptr)_2)->base + _j_6720);
            DeRef(_read_name_6722);
            _2 = (int)SEQ_PTR(_3624);
            _read_name_6722 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_read_name_6722);
            _3624 = NOVALUE;

            /** 				if equal(lower(read_name), lower(supplied_name)) then*/
            RefDS(_read_name_6722);
            _3626 = _12lower(_read_name_6722);
            RefDS(_supplied_name_6678);
            _3627 = _12lower(_supplied_name_6678);
            if (_3626 == _3627)
            _3628 = 1;
            else if (IS_ATOM_INT(_3626) && IS_ATOM_INT(_3627))
            _3628 = 0;
            else
            _3628 = (compare(_3626, _3627) == 0);
            DeRef(_3626);
            _3626 = NOVALUE;
            DeRef(_3627);
            _3627 = NOVALUE;
            if (_3628 == 0)
            {
                _3628 = NOVALUE;
                goto L33; // [1188] 1322
            }
            else{
                _3628 = NOVALUE;
            }

            /** 					if short_name and sequence(list[j][D_ALTNAME]) then*/
            if (_short_name_6646 == 0) {
                goto L34; // [1193] 1274
            }
            _2 = (int)SEQ_PTR(_list_6673);
            _3630 = (int)*(((s1_ptr)_2)->base + _j_6720);
            _2 = (int)SEQ_PTR(_3630);
            _3631 = (int)*(((s1_ptr)_2)->base + 11);
            _3630 = NOVALUE;
            _3632 = IS_SEQUENCE(_3631);
            _3631 = NOVALUE;
            if (_3632 == 0)
            {
                _3632 = NOVALUE;
                goto L34; // [1211] 1274
            }
            else{
                _3632 = NOVALUE;
            }

            /** 						lPath = lPath[1..sl[i]] & list[j][D_ALTNAME] & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_6643);
            _3633 = (int)*(((s1_ptr)_2)->base + _i_6664);
            rhs_slice_target = (object_ptr)&_3634;
            RHS_Slice(_lPath_6497, 1, _3633);
            _2 = (int)SEQ_PTR(_list_6673);
            _3635 = (int)*(((s1_ptr)_2)->base + _j_6720);
            _2 = (int)SEQ_PTR(_3635);
            _3636 = (int)*(((s1_ptr)_2)->base + 11);
            _3635 = NOVALUE;
            _3637 = _i_6664 + 1;
            _2 = (int)SEQ_PTR(_sl_6643);
            _3638 = (int)*(((s1_ptr)_2)->base + _3637);
            if (IS_SEQUENCE(_lPath_6497)){
                    _3639 = SEQ_PTR(_lPath_6497)->length;
            }
            else {
                _3639 = 1;
            }
            rhs_slice_target = (object_ptr)&_3640;
            RHS_Slice(_lPath_6497, _3638, _3639);
            {
                int concat_list[3];

                concat_list[0] = _3640;
                concat_list[1] = _3636;
                concat_list[2] = _3634;
                Concat_N((object_ptr)&_lPath_6497, concat_list, 3);
            }
            DeRefDS(_3640);
            _3640 = NOVALUE;
            _3636 = NOVALUE;
            DeRefDS(_3634);
            _3634 = NOVALUE;

            /** 						sl[$] = length(lPath)+1*/
            if (IS_SEQUENCE(_sl_6643)){
                    _3642 = SEQ_PTR(_sl_6643)->length;
            }
            else {
                _3642 = 1;
            }
            if (IS_SEQUENCE(_lPath_6497)){
                    _3643 = SEQ_PTR(_lPath_6497)->length;
            }
            else {
                _3643 = 1;
            }
            _3644 = _3643 + 1;
            _3643 = NOVALUE;
            _2 = (int)SEQ_PTR(_sl_6643);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _sl_6643 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _3642);
            _1 = *(int *)_2;
            *(int *)_2 = _3644;
            if( _1 != _3644 ){
                DeRef(_1);
            }
            _3644 = NOVALUE;
L34: 

            /** 					if correct_name then*/
            if (_correct_name_6649 == 0)
            {
                goto L35; // [1276] 1313
            }
            else{
            }

            /** 						lPath = lPath[1..sl[i]] & read_name & lPath[sl[i+1]..$]*/
            _2 = (int)SEQ_PTR(_sl_6643);
            _3645 = (int)*(((s1_ptr)_2)->base + _i_6664);
            rhs_slice_target = (object_ptr)&_3646;
            RHS_Slice(_lPath_6497, 1, _3645);
            _3647 = _i_6664 + 1;
            _2 = (int)SEQ_PTR(_sl_6643);
            _3648 = (int)*(((s1_ptr)_2)->base + _3647);
            if (IS_SEQUENCE(_lPath_6497)){
                    _3649 = SEQ_PTR(_lPath_6497)->length;
            }
            else {
                _3649 = 1;
            }
            rhs_slice_target = (object_ptr)&_3650;
            RHS_Slice(_lPath_6497, _3648, _3649);
            {
                int concat_list[3];

                concat_list[0] = _3650;
                concat_list[1] = _read_name_6722;
                concat_list[2] = _3646;
                Concat_N((object_ptr)&_lPath_6497, concat_list, 3);
            }
            DeRefDS(_3650);
            _3650 = NOVALUE;
            DeRefDS(_3646);
            _3646 = NOVALUE;
L35: 

            /** 					continue "partloop"*/
            DeRef(_read_name_6722);
            _read_name_6722 = NOVALUE;
            DeRef(_part_6668);
            _part_6668 = NOVALUE;
            DeRef(_list_6673);
            _list_6673 = NOVALUE;
            DeRef(_supplied_name_6678);
            _supplied_name_6678 = NOVALUE;
            goto L2C; // [1319] 1388
L33: 
            DeRef(_read_name_6722);
            _read_name_6722 = NOVALUE;

            /** 			end for*/
            _j_6720 = _j_6720 + 1;
            goto L31; // [1326] 1160
L32: 
            ;
        }

        /** 			if and_bits(TO_LOWER,case_flags) then*/
        {unsigned long tu;
             tu = (unsigned long)1 & (unsigned long)_case_flags_6496;
             _3652 = MAKE_UINT(tu);
        }
        if (_3652 == 0) {
            DeRef(_3652);
            _3652 = NOVALUE;
            goto L36; // [1339] 1378
        }
        else {
            if (!IS_ATOM_INT(_3652) && DBL_PTR(_3652)->dbl == 0.0){
                DeRef(_3652);
                _3652 = NOVALUE;
                goto L36; // [1339] 1378
            }
            DeRef(_3652);
            _3652 = NOVALUE;
        }
        DeRef(_3652);
        _3652 = NOVALUE;

        /** 				lPath = lPath[1..sl[i]-1] & lower(lPath[sl[i]..$])*/
        _2 = (int)SEQ_PTR(_sl_6643);
        _3653 = (int)*(((s1_ptr)_2)->base + _i_6664);
        if (IS_ATOM_INT(_3653)) {
            _3654 = _3653 - 1;
        }
        else {
            _3654 = binary_op(MINUS, _3653, 1);
        }
        _3653 = NOVALUE;
        rhs_slice_target = (object_ptr)&_3655;
        RHS_Slice(_lPath_6497, 1, _3654);
        _2 = (int)SEQ_PTR(_sl_6643);
        _3656 = (int)*(((s1_ptr)_2)->base + _i_6664);
        if (IS_SEQUENCE(_lPath_6497)){
                _3657 = SEQ_PTR(_lPath_6497)->length;
        }
        else {
            _3657 = 1;
        }
        rhs_slice_target = (object_ptr)&_3658;
        RHS_Slice(_lPath_6497, _3656, _3657);
        _3659 = _12lower(_3658);
        _3658 = NOVALUE;
        if (IS_SEQUENCE(_3655) && IS_ATOM(_3659)) {
            Ref(_3659);
            Append(&_lPath_6497, _3655, _3659);
        }
        else if (IS_ATOM(_3655) && IS_SEQUENCE(_3659)) {
        }
        else {
            Concat((object_ptr)&_lPath_6497, _3655, _3659);
            DeRefDS(_3655);
            _3655 = NOVALUE;
        }
        DeRef(_3655);
        _3655 = NOVALUE;
        DeRef(_3659);
        _3659 = NOVALUE;
L36: 

        /** 			exit*/
        DeRef(_part_6668);
        _part_6668 = NOVALUE;
        DeRef(_list_6673);
        _list_6673 = NOVALUE;
        DeRef(_supplied_name_6678);
        _supplied_name_6678 = NOVALUE;
        goto L29; // [1382] 1393

        /** 		end for*/
L2C: 
        _i_6664 = _i_6664 + -1;
        goto L28; // [1388] 895
L29: 
        ;
    }

    /** 		if and_bits(case_flags,or_bits(CORRECT,TO_LOWER))=TO_LOWER and length(lPath) then*/
    {unsigned long tu;
         tu = (unsigned long)2 | (unsigned long)1;
         _3661 = MAKE_UINT(tu);
    }
    if (IS_ATOM_INT(_3661)) {
        {unsigned long tu;
             tu = (unsigned long)_case_flags_6496 & (unsigned long)_3661;
             _3662 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)_case_flags_6496;
        _3662 = Dand_bits(&temp_d, DBL_PTR(_3661));
    }
    DeRef(_3661);
    _3661 = NOVALUE;
    if (IS_ATOM_INT(_3662)) {
        _3663 = (_3662 == 1);
    }
    else {
        _3663 = (DBL_PTR(_3662)->dbl == (double)1);
    }
    DeRef(_3662);
    _3662 = NOVALUE;
    if (_3663 == 0) {
        goto L37; // [1413] 1433
    }
    if (IS_SEQUENCE(_lPath_6497)){
            _3665 = SEQ_PTR(_lPath_6497)->length;
    }
    else {
        _3665 = 1;
    }
    if (_3665 == 0)
    {
        _3665 = NOVALUE;
        goto L37; // [1421] 1433
    }
    else{
        _3665 = NOVALUE;
    }

    /** 			lPath = lower(lPath)*/
    RefDS(_lPath_6497);
    _0 = _lPath_6497;
    _lPath_6497 = _12lower(_lPath_6497);
    DeRefDS(_0);
L37: 
L26: 
    DeRef(_sl_6643);
    _sl_6643 = NOVALUE;
L25: 

    /** 	ifdef WINDOWS then*/

    /** 		if and_bits(CORRECT,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)2 & (unsigned long)_case_flags_6496;
         _3667 = MAKE_UINT(tu);
    }
    if (_3667 == 0) {
        DeRef(_3667);
        _3667 = NOVALUE;
        goto L38; // [1447] 1489
    }
    else {
        if (!IS_ATOM_INT(_3667) && DBL_PTR(_3667)->dbl == 0.0){
            DeRef(_3667);
            _3667 = NOVALUE;
            goto L38; // [1447] 1489
        }
        DeRef(_3667);
        _3667 = NOVALUE;
    }
    DeRef(_3667);
    _3667 = NOVALUE;

    /** 			if or_bits(system_drive_case,'A') = 'a' then*/
    if (IS_ATOM_INT(_15system_drive_case_6480)) {
        {unsigned long tu;
             tu = (unsigned long)_15system_drive_case_6480 | (unsigned long)65;
             _3668 = MAKE_UINT(tu);
        }
    }
    else {
        temp_d.dbl = (double)65;
        _3668 = Dor_bits(DBL_PTR(_15system_drive_case_6480), &temp_d);
    }
    if (binary_op_a(NOTEQ, _3668, 97)){
        DeRef(_3668);
        _3668 = NOVALUE;
        goto L39; // [1458] 1475
    }
    DeRef(_3668);
    _3668 = NOVALUE;

    /** 				lDrive = lower(lDrive)*/
    RefDS(_lDrive_6502);
    _0 = _lDrive_6502;
    _lDrive_6502 = _12lower(_lDrive_6502);
    DeRefDS(_0);
    goto L3A; // [1472] 1512
L39: 

    /** 				lDrive = upper(lDrive)*/
    RefDS(_lDrive_6502);
    _0 = _lDrive_6502;
    _lDrive_6502 = _12upper(_lDrive_6502);
    DeRefDS(_0);
    goto L3A; // [1486] 1512
L38: 

    /** 		elsif and_bits(TO_LOWER,case_flags) then*/
    {unsigned long tu;
         tu = (unsigned long)1 & (unsigned long)_case_flags_6496;
         _3672 = MAKE_UINT(tu);
    }
    if (_3672 == 0) {
        DeRef(_3672);
        _3672 = NOVALUE;
        goto L3B; // [1497] 1511
    }
    else {
        if (!IS_ATOM_INT(_3672) && DBL_PTR(_3672)->dbl == 0.0){
            DeRef(_3672);
            _3672 = NOVALUE;
            goto L3B; // [1497] 1511
        }
        DeRef(_3672);
        _3672 = NOVALUE;
    }
    DeRef(_3672);
    _3672 = NOVALUE;

    /** 			lDrive = lower(lDrive)*/
    RefDS(_lDrive_6502);
    _0 = _lDrive_6502;
    _lDrive_6502 = _12lower(_lDrive_6502);
    DeRefDS(_0);
L3B: 
L3A: 

    /** 		lPath = lDrive & lPath*/
    Concat((object_ptr)&_lPath_6497, _lDrive_6502, _lPath_6497);

    /** 	return lPath & wildcard_suffix*/
    Concat((object_ptr)&_3675, _lPath_6497, _wildcard_suffix_6567);
    DeRefDS(_path_in_6494);
    DeRefDS(_lPath_6497);
    DeRefi(_lLevel_6500);
    DeRef(_lHome_6501);
    DeRefDS(_lDrive_6502);
    DeRefDS(_wildcard_suffix_6567);
    DeRef(_3593);
    _3593 = NOVALUE;
    DeRef(_3663);
    _3663 = NOVALUE;
    DeRef(_3468);
    _3468 = NOVALUE;
    DeRef(_3505);
    _3505 = NOVALUE;
    _3616 = NOVALUE;
    DeRef(_3508);
    _3508 = NOVALUE;
    DeRef(_3587);
    _3587 = NOVALUE;
    DeRef(_3595);
    _3595 = NOVALUE;
    DeRef(_3480);
    _3480 = NOVALUE;
    DeRef(_3560);
    _3560 = NOVALUE;
    DeRef(_3557);
    _3557 = NOVALUE;
    DeRef(_3584);
    _3584 = NOVALUE;
    DeRef(_3520);
    _3520 = NOVALUE;
    _3598 = NOVALUE;
    DeRef(_3537);
    _3537 = NOVALUE;
    DeRef(_3465);
    _3465 = NOVALUE;
    _3633 = NOVALUE;
    DeRef(_3592);
    _3592 = NOVALUE;
    DeRef(_3525);
    _3525 = NOVALUE;
    DeRef(_3474);
    _3474 = NOVALUE;
    DeRef(_3547);
    _3547 = NOVALUE;
    _3656 = NOVALUE;
    _3611 = NOVALUE;
    DeRef(_3615);
    _3615 = NOVALUE;
    DeRef(_3534);
    _3534 = NOVALUE;
    DeRef(_3477);
    _3477 = NOVALUE;
    DeRef(_3647);
    _3647 = NOVALUE;
    DeRef(_3543);
    _3543 = NOVALUE;
    DeRef(_3472);
    _3472 = NOVALUE;
    _3645 = NOVALUE;
    DeRef(_3637);
    _3637 = NOVALUE;
    _3648 = NOVALUE;
    DeRef(_3494);
    _3494 = NOVALUE;
    _3638 = NOVALUE;
    DeRef(_3654);
    _3654 = NOVALUE;
    return _3675;
    ;
}


int _15fs_case(int _s_6793)
{
    int _3676 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return lower(s)*/
    RefDS(_s_6793);
    _3676 = _12lower(_s_6793);
    DeRefDS(_s_6793);
    return _3676;
    ;
}


int _15abbreviate_path(int _orig_path_6798, int _base_paths_6799)
{
    int _expanded_path_6800 = NOVALUE;
    int _lowered_expanded_path_6810 = NOVALUE;
    int _3721 = NOVALUE;
    int _3720 = NOVALUE;
    int _3717 = NOVALUE;
    int _3716 = NOVALUE;
    int _3715 = NOVALUE;
    int _3714 = NOVALUE;
    int _3713 = NOVALUE;
    int _3711 = NOVALUE;
    int _3710 = NOVALUE;
    int _3709 = NOVALUE;
    int _3708 = NOVALUE;
    int _3707 = NOVALUE;
    int _3706 = NOVALUE;
    int _3705 = NOVALUE;
    int _3704 = NOVALUE;
    int _3703 = NOVALUE;
    int _3700 = NOVALUE;
    int _3699 = NOVALUE;
    int _3697 = NOVALUE;
    int _3696 = NOVALUE;
    int _3695 = NOVALUE;
    int _3694 = NOVALUE;
    int _3693 = NOVALUE;
    int _3692 = NOVALUE;
    int _3691 = NOVALUE;
    int _3690 = NOVALUE;
    int _3689 = NOVALUE;
    int _3688 = NOVALUE;
    int _3687 = NOVALUE;
    int _3686 = NOVALUE;
    int _3685 = NOVALUE;
    int _3682 = NOVALUE;
    int _3681 = NOVALUE;
    int _3680 = NOVALUE;
    int _3678 = NOVALUE;
    int _0, _1, _2;
    

    /** 	expanded_path = canonical_path(orig_path)*/
    RefDS(_orig_path_6798);
    _0 = _expanded_path_6800;
    _expanded_path_6800 = _15canonical_path(_orig_path_6798, 0, 0);
    DeRef(_0);

    /** 	base_paths = append(base_paths, curdir())*/
    _3678 = _15curdir(0);
    Ref(_3678);
    Append(&_base_paths_6799, _base_paths_6799, _3678);
    DeRef(_3678);
    _3678 = NOVALUE;

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_6799)){
            _3680 = SEQ_PTR(_base_paths_6799)->length;
    }
    else {
        _3680 = 1;
    }
    {
        int _i_6805;
        _i_6805 = 1;
L1: 
        if (_i_6805 > _3680){
            goto L2; // [32] 64
        }

        /** 		base_paths[i] = canonical_path(base_paths[i], 1) -- assume each base path is meant to be a directory.*/
        _2 = (int)SEQ_PTR(_base_paths_6799);
        _3681 = (int)*(((s1_ptr)_2)->base + _i_6805);
        Ref(_3681);
        _3682 = _15canonical_path(_3681, 1, 0);
        _3681 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_6799);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _base_paths_6799 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_6805);
        _1 = *(int *)_2;
        *(int *)_2 = _3682;
        if( _1 != _3682 ){
            DeRef(_1);
        }
        _3682 = NOVALUE;

        /** 	end for*/
        _i_6805 = _i_6805 + 1;
        goto L1; // [59] 39
L2: 
        ;
    }

    /** 	base_paths = fs_case(base_paths)*/
    RefDS(_base_paths_6799);
    _0 = _base_paths_6799;
    _base_paths_6799 = _15fs_case(_base_paths_6799);
    DeRefDS(_0);

    /** 	sequence lowered_expanded_path = fs_case(expanded_path)*/
    RefDS(_expanded_path_6800);
    _0 = _lowered_expanded_path_6810;
    _lowered_expanded_path_6810 = _15fs_case(_expanded_path_6800);
    DeRef(_0);

    /** 	for i = 1 to length(base_paths) do*/
    if (IS_SEQUENCE(_base_paths_6799)){
            _3685 = SEQ_PTR(_base_paths_6799)->length;
    }
    else {
        _3685 = 1;
    }
    {
        int _i_6813;
        _i_6813 = 1;
L3: 
        if (_i_6813 > _3685){
            goto L4; // [85] 139
        }

        /** 		if search:begins(base_paths[i], lowered_expanded_path) then*/
        _2 = (int)SEQ_PTR(_base_paths_6799);
        _3686 = (int)*(((s1_ptr)_2)->base + _i_6813);
        Ref(_3686);
        RefDS(_lowered_expanded_path_6810);
        _3687 = _14begins(_3686, _lowered_expanded_path_6810);
        _3686 = NOVALUE;
        if (_3687 == 0) {
            DeRef(_3687);
            _3687 = NOVALUE;
            goto L5; // [103] 132
        }
        else {
            if (!IS_ATOM_INT(_3687) && DBL_PTR(_3687)->dbl == 0.0){
                DeRef(_3687);
                _3687 = NOVALUE;
                goto L5; // [103] 132
            }
            DeRef(_3687);
            _3687 = NOVALUE;
        }
        DeRef(_3687);
        _3687 = NOVALUE;

        /** 			return expanded_path[length(base_paths[i]) + 1 .. $]*/
        _2 = (int)SEQ_PTR(_base_paths_6799);
        _3688 = (int)*(((s1_ptr)_2)->base + _i_6813);
        if (IS_SEQUENCE(_3688)){
                _3689 = SEQ_PTR(_3688)->length;
        }
        else {
            _3689 = 1;
        }
        _3688 = NOVALUE;
        _3690 = _3689 + 1;
        _3689 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_6800)){
                _3691 = SEQ_PTR(_expanded_path_6800)->length;
        }
        else {
            _3691 = 1;
        }
        rhs_slice_target = (object_ptr)&_3692;
        RHS_Slice(_expanded_path_6800, _3690, _3691);
        DeRefDS(_orig_path_6798);
        DeRefDS(_base_paths_6799);
        DeRefDS(_expanded_path_6800);
        DeRefDS(_lowered_expanded_path_6810);
        _3688 = NOVALUE;
        _3690 = NOVALUE;
        return _3692;
L5: 

        /** 	end for*/
        _i_6813 = _i_6813 + 1;
        goto L3; // [134] 92
L4: 
        ;
    }

    /** 	ifdef WINDOWS then*/

    /** 		if not equal(base_paths[$][1], lowered_expanded_path[1]) then*/
    if (IS_SEQUENCE(_base_paths_6799)){
            _3693 = SEQ_PTR(_base_paths_6799)->length;
    }
    else {
        _3693 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_6799);
    _3694 = (int)*(((s1_ptr)_2)->base + _3693);
    _2 = (int)SEQ_PTR(_3694);
    _3695 = (int)*(((s1_ptr)_2)->base + 1);
    _3694 = NOVALUE;
    _2 = (int)SEQ_PTR(_lowered_expanded_path_6810);
    _3696 = (int)*(((s1_ptr)_2)->base + 1);
    if (_3695 == _3696)
    _3697 = 1;
    else if (IS_ATOM_INT(_3695) && IS_ATOM_INT(_3696))
    _3697 = 0;
    else
    _3697 = (compare(_3695, _3696) == 0);
    _3695 = NOVALUE;
    _3696 = NOVALUE;
    if (_3697 != 0)
    goto L6; // [162] 172
    _3697 = NOVALUE;

    /** 			return orig_path*/
    DeRefDS(_base_paths_6799);
    DeRef(_expanded_path_6800);
    DeRefDS(_lowered_expanded_path_6810);
    _3688 = NOVALUE;
    DeRef(_3690);
    _3690 = NOVALUE;
    DeRef(_3692);
    _3692 = NOVALUE;
    return _orig_path_6798;
L6: 

    /** 	base_paths = stdseq:split(base_paths[$], SLASH)*/
    if (IS_SEQUENCE(_base_paths_6799)){
            _3699 = SEQ_PTR(_base_paths_6799)->length;
    }
    else {
        _3699 = 1;
    }
    _2 = (int)SEQ_PTR(_base_paths_6799);
    _3700 = (int)*(((s1_ptr)_2)->base + _3699);
    Ref(_3700);
    _0 = _base_paths_6799;
    _base_paths_6799 = _21split(_3700, 92, 0, 0);
    DeRefDS(_0);
    _3700 = NOVALUE;

    /** 	expanded_path = stdseq:split(expanded_path, SLASH)*/
    RefDS(_expanded_path_6800);
    _0 = _expanded_path_6800;
    _expanded_path_6800 = _21split(_expanded_path_6800, 92, 0, 0);
    DeRefDS(_0);

    /** 	lowered_expanded_path = ""*/
    RefDS(_5);
    DeRef(_lowered_expanded_path_6810);
    _lowered_expanded_path_6810 = _5;

    /** 	for i = 1 to math:min({length(expanded_path), length(base_paths) - 1}) do*/
    if (IS_SEQUENCE(_expanded_path_6800)){
            _3703 = SEQ_PTR(_expanded_path_6800)->length;
    }
    else {
        _3703 = 1;
    }
    if (IS_SEQUENCE(_base_paths_6799)){
            _3704 = SEQ_PTR(_base_paths_6799)->length;
    }
    else {
        _3704 = 1;
    }
    _3705 = _3704 - 1;
    _3704 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _3703;
    ((int *)_2)[2] = _3705;
    _3706 = MAKE_SEQ(_1);
    _3705 = NOVALUE;
    _3703 = NOVALUE;
    _3707 = _18min(_3706);
    _3706 = NOVALUE;
    {
        int _i_6835;
        _i_6835 = 1;
L7: 
        if (binary_op_a(GREATER, _i_6835, _3707)){
            goto L8; // [228] 321
        }

        /** 		if not equal(fs_case(expanded_path[i]), base_paths[i]) then*/
        _2 = (int)SEQ_PTR(_expanded_path_6800);
        if (!IS_ATOM_INT(_i_6835)){
            _3708 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_6835)->dbl));
        }
        else{
            _3708 = (int)*(((s1_ptr)_2)->base + _i_6835);
        }
        Ref(_3708);
        _3709 = _15fs_case(_3708);
        _3708 = NOVALUE;
        _2 = (int)SEQ_PTR(_base_paths_6799);
        if (!IS_ATOM_INT(_i_6835)){
            _3710 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_6835)->dbl));
        }
        else{
            _3710 = (int)*(((s1_ptr)_2)->base + _i_6835);
        }
        if (_3709 == _3710)
        _3711 = 1;
        else if (IS_ATOM_INT(_3709) && IS_ATOM_INT(_3710))
        _3711 = 0;
        else
        _3711 = (compare(_3709, _3710) == 0);
        DeRef(_3709);
        _3709 = NOVALUE;
        _3710 = NOVALUE;
        if (_3711 != 0)
        goto L9; // [253] 314
        _3711 = NOVALUE;

        /** 			expanded_path = repeat("..", length(base_paths) - i) & expanded_path[i .. $]*/
        if (IS_SEQUENCE(_base_paths_6799)){
                _3713 = SEQ_PTR(_base_paths_6799)->length;
        }
        else {
            _3713 = 1;
        }
        if (IS_ATOM_INT(_i_6835)) {
            _3714 = _3713 - _i_6835;
        }
        else {
            _3714 = NewDouble((double)_3713 - DBL_PTR(_i_6835)->dbl);
        }
        _3713 = NOVALUE;
        _3715 = Repeat(_3226, _3714);
        DeRef(_3714);
        _3714 = NOVALUE;
        if (IS_SEQUENCE(_expanded_path_6800)){
                _3716 = SEQ_PTR(_expanded_path_6800)->length;
        }
        else {
            _3716 = 1;
        }
        rhs_slice_target = (object_ptr)&_3717;
        RHS_Slice(_expanded_path_6800, _i_6835, _3716);
        Concat((object_ptr)&_expanded_path_6800, _3715, _3717);
        DeRefDS(_3715);
        _3715 = NOVALUE;
        DeRef(_3715);
        _3715 = NOVALUE;
        DeRefDS(_3717);
        _3717 = NOVALUE;

        /** 			expanded_path = stdseq:join(expanded_path, SLASH)*/
        RefDS(_expanded_path_6800);
        _0 = _expanded_path_6800;
        _expanded_path_6800 = _21join(_expanded_path_6800, 92);
        DeRefDS(_0);

        /** 			if length(expanded_path) < length(orig_path) then*/
        if (IS_SEQUENCE(_expanded_path_6800)){
                _3720 = SEQ_PTR(_expanded_path_6800)->length;
        }
        else {
            _3720 = 1;
        }
        if (IS_SEQUENCE(_orig_path_6798)){
                _3721 = SEQ_PTR(_orig_path_6798)->length;
        }
        else {
            _3721 = 1;
        }
        if (_3720 >= _3721)
        goto L8; // [298] 321

        /** 		  		return expanded_path*/
        DeRef(_i_6835);
        DeRefDS(_orig_path_6798);
        DeRefDS(_base_paths_6799);
        DeRef(_lowered_expanded_path_6810);
        _3688 = NOVALUE;
        DeRef(_3690);
        _3690 = NOVALUE;
        DeRef(_3692);
        _3692 = NOVALUE;
        DeRef(_3707);
        _3707 = NOVALUE;
        return _expanded_path_6800;

        /** 			exit*/
        goto L8; // [311] 321
L9: 

        /** 	end for*/
        _0 = _i_6835;
        if (IS_ATOM_INT(_i_6835)) {
            _i_6835 = _i_6835 + 1;
            if ((long)((unsigned long)_i_6835 +(unsigned long) HIGH_BITS) >= 0){
                _i_6835 = NewDouble((double)_i_6835);
            }
        }
        else {
            _i_6835 = binary_op_a(PLUS, _i_6835, 1);
        }
        DeRef(_0);
        goto L7; // [316] 235
L8: 
        ;
        DeRef(_i_6835);
    }

    /** 	return orig_path*/
    DeRefDS(_base_paths_6799);
    DeRef(_expanded_path_6800);
    DeRef(_lowered_expanded_path_6810);
    _3688 = NOVALUE;
    DeRef(_3690);
    _3690 = NOVALUE;
    DeRef(_3692);
    _3692 = NOVALUE;
    DeRef(_3707);
    _3707 = NOVALUE;
    return _orig_path_6798;
    ;
}


int _15split_path(int _fname_6860)
{
    int _3723 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return stdseq:split(fname, SLASH, 1)*/
    RefDS(_fname_6860);
    _3723 = _21split(_fname_6860, 92, 1, 0);
    DeRefDS(_fname_6860);
    return _3723;
    ;
}


int _15join_path(int _path_elements_6864)
{
    int _fname_6865 = NOVALUE;
    int _elem_6869 = NOVALUE;
    int _3737 = NOVALUE;
    int _3736 = NOVALUE;
    int _3735 = NOVALUE;
    int _3734 = NOVALUE;
    int _3733 = NOVALUE;
    int _3732 = NOVALUE;
    int _3730 = NOVALUE;
    int _3729 = NOVALUE;
    int _3727 = NOVALUE;
    int _3726 = NOVALUE;
    int _3724 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence fname = ""*/
    RefDS(_5);
    DeRef(_fname_6865);
    _fname_6865 = _5;

    /** 	for i = 1 to length(path_elements) do*/
    if (IS_SEQUENCE(_path_elements_6864)){
            _3724 = SEQ_PTR(_path_elements_6864)->length;
    }
    else {
        _3724 = 1;
    }
    {
        int _i_6867;
        _i_6867 = 1;
L1: 
        if (_i_6867 > _3724){
            goto L2; // [15] 117
        }

        /** 		sequence elem = path_elements[i]*/
        DeRef(_elem_6869);
        _2 = (int)SEQ_PTR(_path_elements_6864);
        _elem_6869 = (int)*(((s1_ptr)_2)->base + _i_6867);
        Ref(_elem_6869);

        /** 		if elem[$] = SLASH then*/
        if (IS_SEQUENCE(_elem_6869)){
                _3726 = SEQ_PTR(_elem_6869)->length;
        }
        else {
            _3726 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_6869);
        _3727 = (int)*(((s1_ptr)_2)->base + _3726);
        if (binary_op_a(NOTEQ, _3727, 92)){
            _3727 = NOVALUE;
            goto L3; // [39] 58
        }
        _3727 = NOVALUE;

        /** 			elem = elem[1..$ - 1]*/
        if (IS_SEQUENCE(_elem_6869)){
                _3729 = SEQ_PTR(_elem_6869)->length;
        }
        else {
            _3729 = 1;
        }
        _3730 = _3729 - 1;
        _3729 = NOVALUE;
        rhs_slice_target = (object_ptr)&_elem_6869;
        RHS_Slice(_elem_6869, 1, _3730);
L3: 

        /** 		if length(elem) and elem[1] != SLASH then*/
        if (IS_SEQUENCE(_elem_6869)){
                _3732 = SEQ_PTR(_elem_6869)->length;
        }
        else {
            _3732 = 1;
        }
        if (_3732 == 0) {
            goto L4; // [63] 102
        }
        _2 = (int)SEQ_PTR(_elem_6869);
        _3734 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_3734)) {
            _3735 = (_3734 != 92);
        }
        else {
            _3735 = binary_op(NOTEQ, _3734, 92);
        }
        _3734 = NOVALUE;
        if (_3735 == 0) {
            DeRef(_3735);
            _3735 = NOVALUE;
            goto L4; // [76] 102
        }
        else {
            if (!IS_ATOM_INT(_3735) && DBL_PTR(_3735)->dbl == 0.0){
                DeRef(_3735);
                _3735 = NOVALUE;
                goto L4; // [76] 102
            }
            DeRef(_3735);
            _3735 = NOVALUE;
        }
        DeRef(_3735);
        _3735 = NOVALUE;

        /** 			ifdef WINDOWS then*/

        /** 				if elem[$] != ':' then*/
        if (IS_SEQUENCE(_elem_6869)){
                _3736 = SEQ_PTR(_elem_6869)->length;
        }
        else {
            _3736 = 1;
        }
        _2 = (int)SEQ_PTR(_elem_6869);
        _3737 = (int)*(((s1_ptr)_2)->base + _3736);
        if (binary_op_a(EQUALS, _3737, 58)){
            _3737 = NOVALUE;
            goto L5; // [90] 101
        }
        _3737 = NOVALUE;

        /** 					elem = SLASH & elem*/
        Prepend(&_elem_6869, _elem_6869, 92);
L5: 
L4: 

        /** 		fname &= elem*/
        Concat((object_ptr)&_fname_6865, _fname_6865, _elem_6869);
        DeRefDS(_elem_6869);
        _elem_6869 = NOVALUE;

        /** 	end for*/
        _i_6867 = _i_6867 + 1;
        goto L1; // [112] 22
L2: 
        ;
    }

    /** 	return fname*/
    DeRefDS(_path_elements_6864);
    DeRef(_3730);
    _3730 = NOVALUE;
    return _fname_6865;
    ;
}


int _15file_type(int _filename_6898)
{
    int _dirfil_6899 = NOVALUE;
    int _dir_inlined_dir_at_66_6912 = NOVALUE;
    int _3765 = NOVALUE;
    int _3764 = NOVALUE;
    int _3763 = NOVALUE;
    int _3762 = NOVALUE;
    int _3761 = NOVALUE;
    int _3759 = NOVALUE;
    int _3758 = NOVALUE;
    int _3757 = NOVALUE;
    int _3756 = NOVALUE;
    int _3755 = NOVALUE;
    int _3754 = NOVALUE;
    int _3753 = NOVALUE;
    int _3751 = NOVALUE;
    int _3750 = NOVALUE;
    int _3749 = NOVALUE;
    int _3748 = NOVALUE;
    int _3747 = NOVALUE;
    int _3746 = NOVALUE;
    int _3744 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if eu:find('*', filename) or eu:find('?', filename) then return FILETYPE_UNDEFINED end if*/
    _3744 = find_from(42, _filename_6898, 1);
    if (_3744 != 0) {
        goto L1; // [10] 24
    }
    _3746 = find_from(63, _filename_6898, 1);
    if (_3746 == 0)
    {
        _3746 = NOVALUE;
        goto L2; // [20] 31
    }
    else{
        _3746 = NOVALUE;
    }
L1: 
    DeRefDS(_filename_6898);
    DeRef(_dirfil_6899);
    return -1;
L2: 

    /** 	ifdef WINDOWS then*/

    /** 		if length(filename) = 2 and filename[2] = ':' then*/
    if (IS_SEQUENCE(_filename_6898)){
            _3747 = SEQ_PTR(_filename_6898)->length;
    }
    else {
        _3747 = 1;
    }
    _3748 = (_3747 == 2);
    _3747 = NOVALUE;
    if (_3748 == 0) {
        goto L3; // [42] 65
    }
    _2 = (int)SEQ_PTR(_filename_6898);
    _3750 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3750)) {
        _3751 = (_3750 == 58);
    }
    else {
        _3751 = binary_op(EQUALS, _3750, 58);
    }
    _3750 = NOVALUE;
    if (_3751 == 0) {
        DeRef(_3751);
        _3751 = NOVALUE;
        goto L3; // [55] 65
    }
    else {
        if (!IS_ATOM_INT(_3751) && DBL_PTR(_3751)->dbl == 0.0){
            DeRef(_3751);
            _3751 = NOVALUE;
            goto L3; // [55] 65
        }
        DeRef(_3751);
        _3751 = NOVALUE;
    }
    DeRef(_3751);
    _3751 = NOVALUE;

    /** 			filename &= "\\"*/
    Concat((object_ptr)&_filename_6898, _filename_6898, _943);
L3: 

    /** 	dirfil = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_dirfil_6899);
    _dirfil_6899 = machine(22, _filename_6898);

    /** 	if sequence(dirfil) then*/
    _3753 = IS_SEQUENCE(_dirfil_6899);
    if (_3753 == 0)
    {
        _3753 = NOVALUE;
        goto L4; // [81] 169
    }
    else{
        _3753 = NOVALUE;
    }

    /** 		if length( dirfil ) > 1 or eu:find('d', dirfil[1][2]) or (length(filename)=3 and filename[2]=':') then*/
    if (IS_SEQUENCE(_dirfil_6899)){
            _3754 = SEQ_PTR(_dirfil_6899)->length;
    }
    else {
        _3754 = 1;
    }
    _3755 = (_3754 > 1);
    _3754 = NOVALUE;
    if (_3755 != 0) {
        _3756 = 1;
        goto L5; // [93] 114
    }
    _2 = (int)SEQ_PTR(_dirfil_6899);
    _3757 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3757);
    _3758 = (int)*(((s1_ptr)_2)->base + 2);
    _3757 = NOVALUE;
    _3759 = find_from(100, _3758, 1);
    _3758 = NOVALUE;
    _3756 = (_3759 != 0);
L5: 
    if (_3756 != 0) {
        goto L6; // [114] 146
    }
    if (IS_SEQUENCE(_filename_6898)){
            _3761 = SEQ_PTR(_filename_6898)->length;
    }
    else {
        _3761 = 1;
    }
    _3762 = (_3761 == 3);
    _3761 = NOVALUE;
    if (_3762 == 0) {
        DeRef(_3763);
        _3763 = 0;
        goto L7; // [125] 141
    }
    _2 = (int)SEQ_PTR(_filename_6898);
    _3764 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3764)) {
        _3765 = (_3764 == 58);
    }
    else {
        _3765 = binary_op(EQUALS, _3764, 58);
    }
    _3764 = NOVALUE;
    if (IS_ATOM_INT(_3765))
    _3763 = (_3765 != 0);
    else
    _3763 = DBL_PTR(_3765)->dbl != 0.0;
L7: 
    if (_3763 == 0)
    {
        _3763 = NOVALUE;
        goto L8; // [142] 157
    }
    else{
        _3763 = NOVALUE;
    }
L6: 

    /** 			return FILETYPE_DIRECTORY*/
    DeRefDS(_filename_6898);
    DeRef(_dirfil_6899);
    DeRef(_3748);
    _3748 = NOVALUE;
    DeRef(_3755);
    _3755 = NOVALUE;
    DeRef(_3762);
    _3762 = NOVALUE;
    DeRef(_3765);
    _3765 = NOVALUE;
    return 2;
    goto L9; // [154] 178
L8: 

    /** 			return FILETYPE_FILE*/
    DeRefDS(_filename_6898);
    DeRef(_dirfil_6899);
    DeRef(_3748);
    _3748 = NOVALUE;
    DeRef(_3755);
    _3755 = NOVALUE;
    DeRef(_3762);
    _3762 = NOVALUE;
    DeRef(_3765);
    _3765 = NOVALUE;
    return 1;
    goto L9; // [166] 178
L4: 

    /** 		return FILETYPE_NOT_FOUND*/
    DeRefDS(_filename_6898);
    DeRef(_dirfil_6899);
    DeRef(_3748);
    _3748 = NOVALUE;
    DeRef(_3755);
    _3755 = NOVALUE;
    DeRef(_3762);
    _3762 = NOVALUE;
    DeRef(_3765);
    _3765 = NOVALUE;
    return 0;
L9: 
    ;
}


int _15file_exists(int _name_6956)
{
    int _pName_6959 = NOVALUE;
    int _r_6962 = NOVALUE;
    int _3780 = NOVALUE;
    int _3778 = NOVALUE;
    int _3776 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(name) then*/
    _3776 = IS_ATOM(_name_6956);
    if (_3776 == 0)
    {
        _3776 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _3776 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_name_6956);
    DeRef(_pName_6959);
    DeRef(_r_6962);
    return 0;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom pName = allocate_string(name)*/
    Ref(_name_6956);
    _0 = _pName_6959;
    _pName_6959 = _7allocate_string(_name_6956, 0);
    DeRef(_0);

    /** 		atom r = c_func(xGetFileAttributes, {pName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pName_6959);
    *((int *)(_2+4)) = _pName_6959;
    _3778 = MAKE_SEQ(_1);
    DeRef(_r_6962);
    _r_6962 = call_c(1, _15xGetFileAttributes_5927, _3778);
    DeRefDS(_3778);
    _3778 = NOVALUE;

    /** 		free(pName)*/
    Ref(_pName_6959);
    _7free(_pName_6959);

    /** 		return r > 0*/
    if (IS_ATOM_INT(_r_6962)) {
        _3780 = (_r_6962 > 0);
    }
    else {
        _3780 = (DBL_PTR(_r_6962)->dbl > (double)0);
    }
    DeRef(_name_6956);
    DeRef(_pName_6959);
    DeRef(_r_6962);
    return _3780;
    ;
}


int _15file_timestamp(int _fname_6969)
{
    int _d_6970 = NOVALUE;
    int _dir_inlined_dir_at_4_6972 = NOVALUE;
    int _3794 = NOVALUE;
    int _3793 = NOVALUE;
    int _3792 = NOVALUE;
    int _3791 = NOVALUE;
    int _3790 = NOVALUE;
    int _3789 = NOVALUE;
    int _3788 = NOVALUE;
    int _3787 = NOVALUE;
    int _3786 = NOVALUE;
    int _3785 = NOVALUE;
    int _3784 = NOVALUE;
    int _3783 = NOVALUE;
    int _3782 = NOVALUE;
    int _3781 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d = dir(fname)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_d_6970);
    _d_6970 = machine(22, _fname_6969);

    /** 	if atom(d) then return -1 end if*/
    _3781 = IS_ATOM(_d_6970);
    if (_3781 == 0)
    {
        _3781 = NOVALUE;
        goto L1; // [19] 27
    }
    else{
        _3781 = NOVALUE;
    }
    DeRefDS(_fname_6969);
    DeRef(_d_6970);
    return -1;
L1: 

    /** 	return datetime:new(d[1][D_YEAR], d[1][D_MONTH], d[1][D_DAY],*/
    _2 = (int)SEQ_PTR(_d_6970);
    _3782 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3782);
    _3783 = (int)*(((s1_ptr)_2)->base + 4);
    _3782 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_6970);
    _3784 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3784);
    _3785 = (int)*(((s1_ptr)_2)->base + 5);
    _3784 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_6970);
    _3786 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3786);
    _3787 = (int)*(((s1_ptr)_2)->base + 6);
    _3786 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_6970);
    _3788 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3788);
    _3789 = (int)*(((s1_ptr)_2)->base + 7);
    _3788 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_6970);
    _3790 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3790);
    _3791 = (int)*(((s1_ptr)_2)->base + 8);
    _3790 = NOVALUE;
    _2 = (int)SEQ_PTR(_d_6970);
    _3792 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3792);
    _3793 = (int)*(((s1_ptr)_2)->base + 9);
    _3792 = NOVALUE;
    Ref(_3783);
    Ref(_3785);
    Ref(_3787);
    Ref(_3789);
    Ref(_3791);
    Ref(_3793);
    _3794 = _16new(_3783, _3785, _3787, _3789, _3791, _3793);
    _3783 = NOVALUE;
    _3785 = NOVALUE;
    _3787 = NOVALUE;
    _3789 = NOVALUE;
    _3791 = NOVALUE;
    _3793 = NOVALUE;
    DeRefDS(_fname_6969);
    DeRef(_d_6970);
    return _3794;
    ;
}


int _15copy_file(int _src_6990, int _dest_6991, int _overwrite_6992)
{
    int _info_7003 = NOVALUE;
    int _psrc_7007 = NOVALUE;
    int _pdest_7010 = NOVALUE;
    int _success_7013 = NOVALUE;
    int _3810 = NOVALUE;
    int _3808 = NOVALUE;
    int _3807 = NOVALUE;
    int _3803 = NOVALUE;
    int _3799 = NOVALUE;
    int _3798 = NOVALUE;
    int _3796 = NOVALUE;
    int _3795 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_6992)) {
        _1 = (long)(DBL_PTR(_overwrite_6992)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_6992)) && (DBL_PTR(_overwrite_6992)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_6992);
        _overwrite_6992 = _1;
    }

    /** 	if length(dest) then*/
    if (IS_SEQUENCE(_dest_6991)){
            _3795 = SEQ_PTR(_dest_6991)->length;
    }
    else {
        _3795 = 1;
    }
    if (_3795 == 0)
    {
        _3795 = NOVALUE;
        goto L1; // [14] 74
    }
    else{
        _3795 = NOVALUE;
    }

    /** 		if file_type( dest ) = FILETYPE_DIRECTORY then*/
    RefDS(_dest_6991);
    _3796 = _15file_type(_dest_6991);
    if (binary_op_a(NOTEQ, _3796, 2)){
        DeRef(_3796);
        _3796 = NOVALUE;
        goto L2; // [25] 71
    }
    DeRef(_3796);
    _3796 = NOVALUE;

    /** 			if dest[$] != SLASH then*/
    if (IS_SEQUENCE(_dest_6991)){
            _3798 = SEQ_PTR(_dest_6991)->length;
    }
    else {
        _3798 = 1;
    }
    _2 = (int)SEQ_PTR(_dest_6991);
    _3799 = (int)*(((s1_ptr)_2)->base + _3798);
    if (binary_op_a(EQUALS, _3799, 92)){
        _3799 = NOVALUE;
        goto L3; // [38] 49
    }
    _3799 = NOVALUE;

    /** 				dest &= SLASH*/
    Append(&_dest_6991, _dest_6991, 92);
L3: 

    /** 			sequence info = pathinfo( src )*/
    RefDS(_src_6990);
    _0 = _info_7003;
    _info_7003 = _15pathinfo(_src_6990, 0);
    DeRef(_0);

    /** 			dest &= info[PATH_FILENAME]*/
    _2 = (int)SEQ_PTR(_info_7003);
    _3803 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_dest_6991) && IS_ATOM(_3803)) {
        Ref(_3803);
        Append(&_dest_6991, _dest_6991, _3803);
    }
    else if (IS_ATOM(_dest_6991) && IS_SEQUENCE(_3803)) {
    }
    else {
        Concat((object_ptr)&_dest_6991, _dest_6991, _3803);
    }
    _3803 = NOVALUE;
L2: 
    DeRef(_info_7003);
    _info_7003 = NOVALUE;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		atom psrc = allocate_string(src)*/
    RefDS(_src_6990);
    _0 = _psrc_7007;
    _psrc_7007 = _7allocate_string(_src_6990, 0);
    DeRef(_0);

    /** 		atom pdest = allocate_string(dest)*/
    RefDS(_dest_6991);
    _0 = _pdest_7010;
    _pdest_7010 = _7allocate_string(_dest_6991, 0);
    DeRef(_0);

    /** 		integer success = c_func(xCopyFile, {psrc, pdest, not overwrite})*/
    _3807 = (_overwrite_6992 == 0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_psrc_7007);
    *((int *)(_2+4)) = _psrc_7007;
    Ref(_pdest_7010);
    *((int *)(_2+8)) = _pdest_7010;
    *((int *)(_2+12)) = _3807;
    _3808 = MAKE_SEQ(_1);
    _3807 = NOVALUE;
    _success_7013 = call_c(1, _15xCopyFile_5903, _3808);
    DeRefDS(_3808);
    _3808 = NOVALUE;
    if (!IS_ATOM_INT(_success_7013)) {
        _1 = (long)(DBL_PTR(_success_7013)->dbl);
        if (UNIQUE(DBL_PTR(_success_7013)) && (DBL_PTR(_success_7013)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_success_7013);
        _success_7013 = _1;
    }

    /** 		free({pdest, psrc})*/
    Ref(_psrc_7007);
    Ref(_pdest_7010);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_7010;
    ((int *)_2)[2] = _psrc_7007;
    _3810 = MAKE_SEQ(_1);
    _7free(_3810);
    _3810 = NOVALUE;

    /** 	return success*/
    DeRefDS(_src_6990);
    DeRefDS(_dest_6991);
    DeRef(_psrc_7007);
    DeRef(_pdest_7010);
    return _success_7013;
    ;
}


int _15rename_file(int _old_name_7022, int _new_name_7023, int _overwrite_7024)
{
    int _psrc_7025 = NOVALUE;
    int _pdest_7026 = NOVALUE;
    int _ret_7027 = NOVALUE;
    int _tempfile_7028 = NOVALUE;
    int _3826 = NOVALUE;
    int _3823 = NOVALUE;
    int _3821 = NOVALUE;
    int _3819 = NOVALUE;
    int _3814 = NOVALUE;
    int _3813 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_7024)) {
        _1 = (long)(DBL_PTR(_overwrite_7024)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_7024)) && (DBL_PTR(_overwrite_7024)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_7024);
        _overwrite_7024 = _1;
    }

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_7028);
    _tempfile_7028 = _5;

    /** 	if not overwrite then*/
    if (_overwrite_7024 != 0)
    goto L1; // [18] 40

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_7023);
    _3813 = _15file_exists(_new_name_7023);
    if (_3813 == 0) {
        DeRef(_3813);
        _3813 = NOVALUE;
        goto L2; // [27] 70
    }
    else {
        if (!IS_ATOM_INT(_3813) && DBL_PTR(_3813)->dbl == 0.0){
            DeRef(_3813);
            _3813 = NOVALUE;
            goto L2; // [27] 70
        }
        DeRef(_3813);
        _3813 = NOVALUE;
    }
    DeRef(_3813);
    _3813 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_old_name_7022);
    DeRefDS(_new_name_7023);
    DeRef(_psrc_7025);
    DeRef(_pdest_7026);
    DeRef(_ret_7027);
    DeRefDS(_tempfile_7028);
    return 0;
    goto L2; // [37] 70
L1: 

    /** 		if file_exists(new_name) then*/
    RefDS(_new_name_7023);
    _3814 = _15file_exists(_new_name_7023);
    if (_3814 == 0) {
        DeRef(_3814);
        _3814 = NOVALUE;
        goto L3; // [46] 69
    }
    else {
        if (!IS_ATOM_INT(_3814) && DBL_PTR(_3814)->dbl == 0.0){
            DeRef(_3814);
            _3814 = NOVALUE;
            goto L3; // [46] 69
        }
        DeRef(_3814);
        _3814 = NOVALUE;
    }
    DeRef(_3814);
    _3814 = NOVALUE;

    /** 			tempfile = temp_file(new_name)*/
    RefDS(_new_name_7023);
    RefDS(_5);
    RefDS(_4048);
    _0 = _tempfile_7028;
    _tempfile_7028 = _15temp_file(_new_name_7023, _5, _4048, 0);
    DeRef(_0);

    /** 			ret = move_file(new_name, tempfile)*/
    RefDS(_new_name_7023);
    RefDS(_tempfile_7028);
    _0 = _ret_7027;
    _ret_7027 = _15move_file(_new_name_7023, _tempfile_7028, 0);
    DeRef(_0);
L3: 
L2: 

    /** 	psrc = machine:allocate_string(old_name)*/
    RefDS(_old_name_7022);
    _0 = _psrc_7025;
    _psrc_7025 = _7allocate_string(_old_name_7022, 0);
    DeRef(_0);

    /** 	pdest = machine:allocate_string(new_name)*/
    RefDS(_new_name_7023);
    _0 = _pdest_7026;
    _pdest_7026 = _7allocate_string(_new_name_7023, 0);
    DeRef(_0);

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_7026);
    Ref(_psrc_7025);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_7025;
    ((int *)_2)[2] = _pdest_7026;
    _3819 = MAKE_SEQ(_1);
    DeRef(_ret_7027);
    _ret_7027 = call_c(1, _15xMoveFile_5908, _3819);
    DeRefDS(_3819);
    _3819 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_7025);
    Ref(_pdest_7026);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_7026;
    ((int *)_2)[2] = _psrc_7025;
    _3821 = MAKE_SEQ(_1);
    _7free(_3821);
    _3821 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_7024 == 0)
    {
        goto L4; // [110] 144
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_7027)) {
        if (_ret_7027 != 0){
            goto L5; // [115] 137
        }
    }
    else {
        if (DBL_PTR(_ret_7027)->dbl != 0.0){
            goto L5; // [115] 137
        }
    }

    /** 			if length(tempfile) > 0 then*/
    if (IS_SEQUENCE(_tempfile_7028)){
            _3823 = SEQ_PTR(_tempfile_7028)->length;
    }
    else {
        _3823 = 1;
    }
    if (_3823 <= 0)
    goto L6; // [123] 136

    /** 				ret = move_file(tempfile, new_name)*/
    RefDS(_tempfile_7028);
    RefDS(_new_name_7023);
    _0 = _ret_7027;
    _ret_7027 = _15move_file(_tempfile_7028, _new_name_7023, 0);
    DeRef(_0);
L6: 
L5: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_7028);
    _3826 = _15delete_file(_tempfile_7028);
L4: 

    /** 	return ret*/
    DeRefDS(_old_name_7022);
    DeRefDS(_new_name_7023);
    DeRef(_psrc_7025);
    DeRef(_pdest_7026);
    DeRef(_tempfile_7028);
    DeRef(_3826);
    _3826 = NOVALUE;
    return _ret_7027;
    ;
}


int _15move_file(int _src_7056, int _dest_7057, int _overwrite_7058)
{
    int _psrc_7059 = NOVALUE;
    int _pdest_7060 = NOVALUE;
    int _ret_7061 = NOVALUE;
    int _tempfile_7062 = NOVALUE;
    int _3845 = NOVALUE;
    int _3844 = NOVALUE;
    int _3843 = NOVALUE;
    int _3842 = NOVALUE;
    int _3840 = NOVALUE;
    int _3838 = NOVALUE;
    int _3837 = NOVALUE;
    int _3836 = NOVALUE;
    int _3835 = NOVALUE;
    int _3830 = NOVALUE;
    int _3827 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_overwrite_7058)) {
        _1 = (long)(DBL_PTR(_overwrite_7058)->dbl);
        if (UNIQUE(DBL_PTR(_overwrite_7058)) && (DBL_PTR(_overwrite_7058)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_overwrite_7058);
        _overwrite_7058 = _1;
    }

    /** 	atom psrc = 0, pdest = 0, ret*/
    DeRef(_psrc_7059);
    _psrc_7059 = 0;
    DeRef(_pdest_7060);
    _pdest_7060 = 0;

    /** 	sequence tempfile = ""*/
    RefDS(_5);
    DeRef(_tempfile_7062);
    _tempfile_7062 = _5;

    /** 	if not file_exists(src) then*/
    RefDS(_src_7056);
    _3827 = _15file_exists(_src_7056);
    if (IS_ATOM_INT(_3827)) {
        if (_3827 != 0){
            DeRef(_3827);
            _3827 = NOVALUE;
            goto L1; // [30] 40
        }
    }
    else {
        if (DBL_PTR(_3827)->dbl != 0.0){
            DeRef(_3827);
            _3827 = NOVALUE;
            goto L1; // [30] 40
        }
    }
    DeRef(_3827);
    _3827 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_src_7056);
    DeRefDS(_dest_7057);
    DeRef(_ret_7061);
    DeRefDS(_tempfile_7062);
    return 0;
L1: 

    /** 	if not overwrite then*/
    if (_overwrite_7058 != 0)
    goto L2; // [42] 62

    /** 		if file_exists( dest ) then*/
    RefDS(_dest_7057);
    _3830 = _15file_exists(_dest_7057);
    if (_3830 == 0) {
        DeRef(_3830);
        _3830 = NOVALUE;
        goto L3; // [51] 61
    }
    else {
        if (!IS_ATOM_INT(_3830) && DBL_PTR(_3830)->dbl == 0.0){
            DeRef(_3830);
            _3830 = NOVALUE;
            goto L3; // [51] 61
        }
        DeRef(_3830);
        _3830 = NOVALUE;
    }
    DeRef(_3830);
    _3830 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_src_7056);
    DeRefDS(_dest_7057);
    DeRef(_psrc_7059);
    DeRef(_pdest_7060);
    DeRef(_ret_7061);
    DeRef(_tempfile_7062);
    return 0;
L3: 
L2: 

    /** 	ifdef UNIX then*/

    /** 	ifdef LINUX then*/

    /** 	ifdef UNIX then*/

    /** 		psrc  = machine:allocate_string(src)*/
    RefDS(_src_7056);
    _0 = _psrc_7059;
    _psrc_7059 = _7allocate_string(_src_7056, 0);
    DeRef(_0);

    /** 		pdest = machine:allocate_string(dest)*/
    RefDS(_dest_7057);
    _0 = _pdest_7060;
    _pdest_7060 = _7allocate_string(_dest_7057, 0);
    DeRef(_0);

    /** 	if overwrite then*/
    if (_overwrite_7058 == 0)
    {
        goto L4; // [84] 113
    }
    else{
    }

    /** 		tempfile = temp_file(dest)*/
    RefDS(_dest_7057);
    RefDS(_5);
    RefDS(_4048);
    _0 = _tempfile_7062;
    _tempfile_7062 = _15temp_file(_dest_7057, _5, _4048, 0);
    DeRef(_0);

    /** 		move_file(dest, tempfile)*/
    RefDS(_dest_7057);
    DeRef(_3835);
    _3835 = _dest_7057;
    RefDS(_tempfile_7062);
    DeRef(_3836);
    _3836 = _tempfile_7062;
    _3837 = _15move_file(_3835, _3836, 0);
    _3835 = NOVALUE;
    _3836 = NOVALUE;
L4: 

    /** 	ret = c_func(xMoveFile, {psrc, pdest})*/
    Ref(_pdest_7060);
    Ref(_psrc_7059);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _psrc_7059;
    ((int *)_2)[2] = _pdest_7060;
    _3838 = MAKE_SEQ(_1);
    DeRef(_ret_7061);
    _ret_7061 = call_c(1, _15xMoveFile_5908, _3838);
    DeRefDS(_3838);
    _3838 = NOVALUE;

    /** 	ifdef UNIX then*/

    /** 	machine:free({pdest, psrc})*/
    Ref(_psrc_7059);
    Ref(_pdest_7060);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pdest_7060;
    ((int *)_2)[2] = _psrc_7059;
    _3840 = MAKE_SEQ(_1);
    _7free(_3840);
    _3840 = NOVALUE;

    /** 	if overwrite then*/
    if (_overwrite_7058 == 0)
    {
        goto L5; // [139] 169
    }
    else{
    }

    /** 		if not ret then*/
    if (IS_ATOM_INT(_ret_7061)) {
        if (_ret_7061 != 0){
            goto L6; // [144] 162
        }
    }
    else {
        if (DBL_PTR(_ret_7061)->dbl != 0.0){
            goto L6; // [144] 162
        }
    }

    /** 			move_file(tempfile, dest)*/
    RefDS(_tempfile_7062);
    DeRef(_3842);
    _3842 = _tempfile_7062;
    RefDS(_dest_7057);
    DeRef(_3843);
    _3843 = _dest_7057;
    _3844 = _15move_file(_3842, _3843, 0);
    _3842 = NOVALUE;
    _3843 = NOVALUE;
L6: 

    /** 		delete_file(tempfile)*/
    RefDS(_tempfile_7062);
    _3845 = _15delete_file(_tempfile_7062);
L5: 

    /** 	return ret*/
    DeRefDS(_src_7056);
    DeRefDS(_dest_7057);
    DeRef(_psrc_7059);
    DeRef(_pdest_7060);
    DeRef(_tempfile_7062);
    DeRef(_3837);
    _3837 = NOVALUE;
    DeRef(_3844);
    _3844 = NOVALUE;
    DeRef(_3845);
    _3845 = NOVALUE;
    return _ret_7061;
    ;
}


int _15file_length(int _filename_7091)
{
    int _list_7092 = NOVALUE;
    int _dir_inlined_dir_at_4_7094 = NOVALUE;
    int _3851 = NOVALUE;
    int _3850 = NOVALUE;
    int _3849 = NOVALUE;
    int _3848 = NOVALUE;
    int _3846 = NOVALUE;
    int _0, _1, _2;
    

    /** 	list = dir(filename)*/

    /** 	ifdef WINDOWS then*/

    /** 		return machine_func(M_DIR, name)*/
    DeRef(_list_7092);
    _list_7092 = machine(22, _filename_7091);

    /** 	if atom(list) or length(list) = 0 then*/
    _3846 = IS_ATOM(_list_7092);
    if (_3846 != 0) {
        goto L1; // [19] 35
    }
    if (IS_SEQUENCE(_list_7092)){
            _3848 = SEQ_PTR(_list_7092)->length;
    }
    else {
        _3848 = 1;
    }
    _3849 = (_3848 == 0);
    _3848 = NOVALUE;
    if (_3849 == 0)
    {
        DeRef(_3849);
        _3849 = NOVALUE;
        goto L2; // [31] 42
    }
    else{
        DeRef(_3849);
        _3849 = NOVALUE;
    }
L1: 

    /** 		return -1*/
    DeRefDS(_filename_7091);
    DeRef(_list_7092);
    return -1;
L2: 

    /** 	return list[1][D_SIZE]*/
    _2 = (int)SEQ_PTR(_list_7092);
    _3850 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_3850);
    _3851 = (int)*(((s1_ptr)_2)->base + 3);
    _3850 = NOVALUE;
    Ref(_3851);
    DeRefDS(_filename_7091);
    DeRef(_list_7092);
    return _3851;
    ;
}


int _15locate_file(int _filename_7104, int _search_list_7105, int _subdir_7106)
{
    int _extra_paths_7107 = NOVALUE;
    int _this_path_7108 = NOVALUE;
    int _3935 = NOVALUE;
    int _3934 = NOVALUE;
    int _3932 = NOVALUE;
    int _3930 = NOVALUE;
    int _3928 = NOVALUE;
    int _3927 = NOVALUE;
    int _3926 = NOVALUE;
    int _3924 = NOVALUE;
    int _3923 = NOVALUE;
    int _3922 = NOVALUE;
    int _3920 = NOVALUE;
    int _3919 = NOVALUE;
    int _3918 = NOVALUE;
    int _3915 = NOVALUE;
    int _3914 = NOVALUE;
    int _3912 = NOVALUE;
    int _3910 = NOVALUE;
    int _3909 = NOVALUE;
    int _3906 = NOVALUE;
    int _3901 = NOVALUE;
    int _3897 = NOVALUE;
    int _3888 = NOVALUE;
    int _3885 = NOVALUE;
    int _3882 = NOVALUE;
    int _3881 = NOVALUE;
    int _3877 = NOVALUE;
    int _3874 = NOVALUE;
    int _3872 = NOVALUE;
    int _3868 = NOVALUE;
    int _3866 = NOVALUE;
    int _3865 = NOVALUE;
    int _3863 = NOVALUE;
    int _3862 = NOVALUE;
    int _3859 = NOVALUE;
    int _3858 = NOVALUE;
    int _3855 = NOVALUE;
    int _3853 = NOVALUE;
    int _3852 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path(filename) then*/
    RefDS(_filename_7104);
    _3852 = _15absolute_path(_filename_7104);
    if (_3852 == 0) {
        DeRef(_3852);
        _3852 = NOVALUE;
        goto L1; // [13] 23
    }
    else {
        if (!IS_ATOM_INT(_3852) && DBL_PTR(_3852)->dbl == 0.0){
            DeRef(_3852);
            _3852 = NOVALUE;
            goto L1; // [13] 23
        }
        DeRef(_3852);
        _3852 = NOVALUE;
    }
    DeRef(_3852);
    _3852 = NOVALUE;

    /** 		return filename*/
    DeRefDS(_search_list_7105);
    DeRefDS(_subdir_7106);
    DeRef(_extra_paths_7107);
    DeRef(_this_path_7108);
    return _filename_7104;
L1: 

    /** 	if length(search_list) = 0 then*/
    if (IS_SEQUENCE(_search_list_7105)){
            _3853 = SEQ_PTR(_search_list_7105)->length;
    }
    else {
        _3853 = 1;
    }
    if (_3853 != 0)
    goto L2; // [28] 281

    /** 		search_list = append(search_list, "." & SLASH)*/
    Append(&_3855, _3194, 92);
    RefDS(_3855);
    Append(&_search_list_7105, _search_list_7105, _3855);
    DeRefDS(_3855);
    _3855 = NOVALUE;

    /** 		extra_paths = command_line()*/
    DeRef(_extra_paths_7107);
    _extra_paths_7107 = Command_Line();

    /** 		extra_paths = canonical_path(dirname(extra_paths[2]), 1)*/
    _2 = (int)SEQ_PTR(_extra_paths_7107);
    _3858 = (int)*(((s1_ptr)_2)->base + 2);
    RefDS(_3858);
    _3859 = _15dirname(_3858, 0);
    _3858 = NOVALUE;
    _0 = _extra_paths_7107;
    _extra_paths_7107 = _15canonical_path(_3859, 1, 0);
    DeRefDS(_0);
    _3859 = NOVALUE;

    /** 		search_list = append(search_list, extra_paths)*/
    Ref(_extra_paths_7107);
    Append(&_search_list_7105, _search_list_7105, _extra_paths_7107);

    /** 		ifdef UNIX then*/

    /** 			extra_paths = getenv("HOMEDRIVE") & getenv("HOMEPATH")*/
    _3862 = EGetEnv(_3484);
    _3863 = EGetEnv(_3486);
    if (IS_SEQUENCE(_3862) && IS_ATOM(_3863)) {
        Ref(_3863);
        Append(&_extra_paths_7107, _3862, _3863);
    }
    else if (IS_ATOM(_3862) && IS_SEQUENCE(_3863)) {
        Ref(_3862);
        Prepend(&_extra_paths_7107, _3863, _3862);
    }
    else {
        Concat((object_ptr)&_extra_paths_7107, _3862, _3863);
        DeRef(_3862);
        _3862 = NOVALUE;
    }
    DeRef(_3862);
    _3862 = NOVALUE;
    DeRef(_3863);
    _3863 = NOVALUE;

    /** 		if sequence(extra_paths) then*/
    _3865 = 1;
    if (_3865 == 0)
    {
        _3865 = NOVALUE;
        goto L3; // [90] 104
    }
    else{
        _3865 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    Append(&_3866, _extra_paths_7107, 92);
    RefDS(_3866);
    Append(&_search_list_7105, _search_list_7105, _3866);
    DeRefDS(_3866);
    _3866 = NOVALUE;
L3: 

    /** 		search_list = append(search_list, ".." & SLASH)*/
    Append(&_3868, _3226, 92);
    RefDS(_3868);
    Append(&_search_list_7105, _search_list_7105, _3868);
    DeRefDS(_3868);
    _3868 = NOVALUE;

    /** 		extra_paths = getenv("EUDIR")*/
    DeRef(_extra_paths_7107);
    _extra_paths_7107 = EGetEnv(_3870);

    /** 		if sequence(extra_paths) then*/
    _3872 = IS_SEQUENCE(_extra_paths_7107);
    if (_3872 == 0)
    {
        _3872 = NOVALUE;
        goto L4; // [124] 154
    }
    else{
        _3872 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH & "bin" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _3873;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7107;
        Concat_N((object_ptr)&_3874, concat_list, 4);
    }
    RefDS(_3874);
    Append(&_search_list_7105, _search_list_7105, _3874);
    DeRefDS(_3874);
    _3874 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "docs" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _3876;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7107;
        Concat_N((object_ptr)&_3877, concat_list, 4);
    }
    RefDS(_3877);
    Append(&_search_list_7105, _search_list_7105, _3877);
    DeRefDS(_3877);
    _3877 = NOVALUE;
L4: 

    /** 		extra_paths = getenv("EUDIST")*/
    DeRef(_extra_paths_7107);
    _extra_paths_7107 = EGetEnv(_3879);

    /** 		if sequence(extra_paths) then*/
    _3881 = IS_SEQUENCE(_extra_paths_7107);
    if (_3881 == 0)
    {
        _3881 = NOVALUE;
        goto L5; // [164] 204
    }
    else{
        _3881 = NOVALUE;
    }

    /** 			search_list = append(search_list, extra_paths & SLASH)*/
    if (IS_SEQUENCE(_extra_paths_7107) && IS_ATOM(92)) {
        Append(&_3882, _extra_paths_7107, 92);
    }
    else if (IS_ATOM(_extra_paths_7107) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_3882, _extra_paths_7107, 92);
    }
    RefDS(_3882);
    Append(&_search_list_7105, _search_list_7105, _3882);
    DeRefDS(_3882);
    _3882 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "etc" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _3884;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7107;
        Concat_N((object_ptr)&_3885, concat_list, 4);
    }
    RefDS(_3885);
    Append(&_search_list_7105, _search_list_7105, _3885);
    DeRefDS(_3885);
    _3885 = NOVALUE;

    /** 			search_list = append(search_list, extra_paths & SLASH & "data" & SLASH)*/
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _3887;
        concat_list[2] = 92;
        concat_list[3] = _extra_paths_7107;
        Concat_N((object_ptr)&_3888, concat_list, 4);
    }
    RefDS(_3888);
    Append(&_search_list_7105, _search_list_7105, _3888);
    DeRefDS(_3888);
    _3888 = NOVALUE;
L5: 

    /** 		ifdef UNIX then*/

    /** 		search_list &= include_paths(1)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_3896);
    *((int *)(_2+4)) = _3896;
    RefDS(_3895);
    *((int *)(_2+8)) = _3895;
    RefDS(_3894);
    *((int *)(_2+12)) = _3894;
    RefDS(_3893);
    *((int *)(_2+16)) = _3893;
    RefDS(_3892);
    *((int *)(_2+20)) = _3892;
    _3897 = MAKE_SEQ(_1);
    Concat((object_ptr)&_search_list_7105, _search_list_7105, _3897);
    DeRefDS(_3897);
    _3897 = NOVALUE;

    /** 		extra_paths = getenv("USERPATH")*/
    DeRef(_extra_paths_7107);
    _extra_paths_7107 = EGetEnv(_3899);

    /** 		if sequence(extra_paths) then*/
    _3901 = IS_SEQUENCE(_extra_paths_7107);
    if (_3901 == 0)
    {
        _3901 = NOVALUE;
        goto L6; // [230] 249
    }
    else{
        _3901 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_7107);
    _0 = _extra_paths_7107;
    _extra_paths_7107 = _21split(_extra_paths_7107, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_7105) && IS_ATOM(_extra_paths_7107)) {
        Ref(_extra_paths_7107);
        Append(&_search_list_7105, _search_list_7105, _extra_paths_7107);
    }
    else if (IS_ATOM(_search_list_7105) && IS_SEQUENCE(_extra_paths_7107)) {
    }
    else {
        Concat((object_ptr)&_search_list_7105, _search_list_7105, _extra_paths_7107);
    }
L6: 

    /** 		extra_paths = getenv("PATH")*/
    DeRef(_extra_paths_7107);
    _extra_paths_7107 = EGetEnv(_3904);

    /** 		if sequence(extra_paths) then*/
    _3906 = IS_SEQUENCE(_extra_paths_7107);
    if (_3906 == 0)
    {
        _3906 = NOVALUE;
        goto L7; // [259] 306
    }
    else{
        _3906 = NOVALUE;
    }

    /** 			extra_paths = stdseq:split(extra_paths, PATHSEP)*/
    Ref(_extra_paths_7107);
    _0 = _extra_paths_7107;
    _extra_paths_7107 = _21split(_extra_paths_7107, 59, 0, 0);
    DeRefi(_0);

    /** 			search_list &= extra_paths*/
    if (IS_SEQUENCE(_search_list_7105) && IS_ATOM(_extra_paths_7107)) {
        Ref(_extra_paths_7107);
        Append(&_search_list_7105, _search_list_7105, _extra_paths_7107);
    }
    else if (IS_ATOM(_search_list_7105) && IS_SEQUENCE(_extra_paths_7107)) {
    }
    else {
        Concat((object_ptr)&_search_list_7105, _search_list_7105, _extra_paths_7107);
    }
    goto L7; // [278] 306
L2: 

    /** 		if integer(search_list[1]) then*/
    _2 = (int)SEQ_PTR(_search_list_7105);
    _3909 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_3909))
    _3910 = 1;
    else if (IS_ATOM_DBL(_3909))
    _3910 = IS_ATOM_INT(DoubleToInt(_3909));
    else
    _3910 = 0;
    _3909 = NOVALUE;
    if (_3910 == 0)
    {
        _3910 = NOVALUE;
        goto L8; // [290] 305
    }
    else{
        _3910 = NOVALUE;
    }

    /** 			search_list = stdseq:split(search_list, PATHSEP)*/
    RefDS(_search_list_7105);
    _0 = _search_list_7105;
    _search_list_7105 = _21split(_search_list_7105, 59, 0, 0);
    DeRefDS(_0);
L8: 
L7: 

    /** 	if length(subdir) > 0 then*/
    if (IS_SEQUENCE(_subdir_7106)){
            _3912 = SEQ_PTR(_subdir_7106)->length;
    }
    else {
        _3912 = 1;
    }
    if (_3912 <= 0)
    goto L9; // [311] 336

    /** 		if subdir[$] != SLASH then*/
    if (IS_SEQUENCE(_subdir_7106)){
            _3914 = SEQ_PTR(_subdir_7106)->length;
    }
    else {
        _3914 = 1;
    }
    _2 = (int)SEQ_PTR(_subdir_7106);
    _3915 = (int)*(((s1_ptr)_2)->base + _3914);
    if (binary_op_a(EQUALS, _3915, 92)){
        _3915 = NOVALUE;
        goto LA; // [324] 335
    }
    _3915 = NOVALUE;

    /** 			subdir &= SLASH*/
    Append(&_subdir_7106, _subdir_7106, 92);
LA: 
L9: 

    /** 	for i = 1 to length(search_list) do*/
    if (IS_SEQUENCE(_search_list_7105)){
            _3918 = SEQ_PTR(_search_list_7105)->length;
    }
    else {
        _3918 = 1;
    }
    {
        int _i_7187;
        _i_7187 = 1;
LB: 
        if (_i_7187 > _3918){
            goto LC; // [341] 466
        }

        /** 		if length(search_list[i]) = 0 then*/
        _2 = (int)SEQ_PTR(_search_list_7105);
        _3919 = (int)*(((s1_ptr)_2)->base + _i_7187);
        if (IS_SEQUENCE(_3919)){
                _3920 = SEQ_PTR(_3919)->length;
        }
        else {
            _3920 = 1;
        }
        _3919 = NOVALUE;
        if (_3920 != 0)
        goto LD; // [357] 366

        /** 			continue*/
        goto LE; // [363] 461
LD: 

        /** 		if search_list[i][$] != SLASH then*/
        _2 = (int)SEQ_PTR(_search_list_7105);
        _3922 = (int)*(((s1_ptr)_2)->base + _i_7187);
        if (IS_SEQUENCE(_3922)){
                _3923 = SEQ_PTR(_3922)->length;
        }
        else {
            _3923 = 1;
        }
        _2 = (int)SEQ_PTR(_3922);
        _3924 = (int)*(((s1_ptr)_2)->base + _3923);
        _3922 = NOVALUE;
        if (binary_op_a(EQUALS, _3924, 92)){
            _3924 = NOVALUE;
            goto LF; // [379] 398
        }
        _3924 = NOVALUE;

        /** 			search_list[i] &= SLASH*/
        _2 = (int)SEQ_PTR(_search_list_7105);
        _3926 = (int)*(((s1_ptr)_2)->base + _i_7187);
        if (IS_SEQUENCE(_3926) && IS_ATOM(92)) {
            Append(&_3927, _3926, 92);
        }
        else if (IS_ATOM(_3926) && IS_SEQUENCE(92)) {
        }
        else {
            Concat((object_ptr)&_3927, _3926, 92);
            _3926 = NOVALUE;
        }
        _3926 = NOVALUE;
        _2 = (int)SEQ_PTR(_search_list_7105);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _search_list_7105 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_7187);
        _1 = *(int *)_2;
        *(int *)_2 = _3927;
        if( _1 != _3927 ){
            DeRef(_1);
        }
        _3927 = NOVALUE;
LF: 

        /** 		if length(subdir) > 0 then*/
        if (IS_SEQUENCE(_subdir_7106)){
                _3928 = SEQ_PTR(_subdir_7106)->length;
        }
        else {
            _3928 = 1;
        }
        if (_3928 <= 0)
        goto L10; // [403] 422

        /** 			this_path = search_list[i] & subdir & filename*/
        _2 = (int)SEQ_PTR(_search_list_7105);
        _3930 = (int)*(((s1_ptr)_2)->base + _i_7187);
        {
            int concat_list[3];

            concat_list[0] = _filename_7104;
            concat_list[1] = _subdir_7106;
            concat_list[2] = _3930;
            Concat_N((object_ptr)&_this_path_7108, concat_list, 3);
        }
        _3930 = NOVALUE;
        goto L11; // [419] 433
L10: 

        /** 			this_path = search_list[i] & filename*/
        _2 = (int)SEQ_PTR(_search_list_7105);
        _3932 = (int)*(((s1_ptr)_2)->base + _i_7187);
        if (IS_SEQUENCE(_3932) && IS_ATOM(_filename_7104)) {
        }
        else if (IS_ATOM(_3932) && IS_SEQUENCE(_filename_7104)) {
            Ref(_3932);
            Prepend(&_this_path_7108, _filename_7104, _3932);
        }
        else {
            Concat((object_ptr)&_this_path_7108, _3932, _filename_7104);
            _3932 = NOVALUE;
        }
        _3932 = NOVALUE;
L11: 

        /** 		if file_exists(this_path) then*/
        RefDS(_this_path_7108);
        _3934 = _15file_exists(_this_path_7108);
        if (_3934 == 0) {
            DeRef(_3934);
            _3934 = NOVALUE;
            goto L12; // [441] 459
        }
        else {
            if (!IS_ATOM_INT(_3934) && DBL_PTR(_3934)->dbl == 0.0){
                DeRef(_3934);
                _3934 = NOVALUE;
                goto L12; // [441] 459
            }
            DeRef(_3934);
            _3934 = NOVALUE;
        }
        DeRef(_3934);
        _3934 = NOVALUE;

        /** 			return canonical_path(this_path)*/
        RefDS(_this_path_7108);
        _3935 = _15canonical_path(_this_path_7108, 0, 0);
        DeRefDS(_filename_7104);
        DeRefDS(_search_list_7105);
        DeRefDS(_subdir_7106);
        DeRef(_extra_paths_7107);
        DeRefDS(_this_path_7108);
        _3919 = NOVALUE;
        return _3935;
L12: 

        /** 	end for*/
LE: 
        _i_7187 = _i_7187 + 1;
        goto LB; // [461] 348
LC: 
        ;
    }

    /** 	return filename*/
    DeRefDS(_search_list_7105);
    DeRefDS(_subdir_7106);
    DeRef(_extra_paths_7107);
    DeRef(_this_path_7108);
    _3919 = NOVALUE;
    DeRef(_3935);
    _3935 = NOVALUE;
    return _filename_7104;
    ;
}


int _15disk_metrics(int _disk_path_7213)
{
    int _result_7214 = NOVALUE;
    int _path_addr_7216 = NOVALUE;
    int _metric_addr_7217 = NOVALUE;
    int _3948 = NOVALUE;
    int _3946 = NOVALUE;
    int _3945 = NOVALUE;
    int _3944 = NOVALUE;
    int _3943 = NOVALUE;
    int _3942 = NOVALUE;
    int _3941 = NOVALUE;
    int _3940 = NOVALUE;
    int _3937 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence result = {0, 0, 0, 0} */
    _0 = _result_7214;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    _result_7214 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	atom path_addr = 0*/
    DeRef(_path_addr_7216);
    _path_addr_7216 = 0;

    /** 	atom metric_addr = 0*/
    DeRef(_metric_addr_7217);
    _metric_addr_7217 = 0;

    /** 	ifdef WINDOWS then*/

    /** 		if sequence(disk_path) then */
    _3937 = IS_SEQUENCE(_disk_path_7213);
    if (_3937 == 0)
    {
        _3937 = NOVALUE;
        goto L1; // [27] 40
    }
    else{
        _3937 = NOVALUE;
    }

    /** 			path_addr = allocate_string(disk_path) */
    Ref(_disk_path_7213);
    _path_addr_7216 = _7allocate_string(_disk_path_7213, 0);
    goto L2; // [37] 46
L1: 

    /** 			path_addr = 0 */
    DeRef(_path_addr_7216);
    _path_addr_7216 = 0;
L2: 

    /** 		metric_addr = allocate(16) */
    _0 = _metric_addr_7217;
    _metric_addr_7217 = _7allocate(16, 0);
    DeRef(_0);

    /** 		if c_func(xGetDiskFreeSpace, {path_addr, */
    if (IS_ATOM_INT(_metric_addr_7217)) {
        _3940 = _metric_addr_7217 + 0;
        if ((long)((unsigned long)_3940 + (unsigned long)HIGH_BITS) >= 0) 
        _3940 = NewDouble((double)_3940);
    }
    else {
        _3940 = NewDouble(DBL_PTR(_metric_addr_7217)->dbl + (double)0);
    }
    if (IS_ATOM_INT(_metric_addr_7217)) {
        _3941 = _metric_addr_7217 + 4;
        if ((long)((unsigned long)_3941 + (unsigned long)HIGH_BITS) >= 0) 
        _3941 = NewDouble((double)_3941);
    }
    else {
        _3941 = NewDouble(DBL_PTR(_metric_addr_7217)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_metric_addr_7217)) {
        _3942 = _metric_addr_7217 + 8;
        if ((long)((unsigned long)_3942 + (unsigned long)HIGH_BITS) >= 0) 
        _3942 = NewDouble((double)_3942);
    }
    else {
        _3942 = NewDouble(DBL_PTR(_metric_addr_7217)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_metric_addr_7217)) {
        _3943 = _metric_addr_7217 + 12;
        if ((long)((unsigned long)_3943 + (unsigned long)HIGH_BITS) >= 0) 
        _3943 = NewDouble((double)_3943);
    }
    else {
        _3943 = NewDouble(DBL_PTR(_metric_addr_7217)->dbl + (double)12);
    }
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_path_addr_7216);
    *((int *)(_2+4)) = _path_addr_7216;
    *((int *)(_2+8)) = _3940;
    *((int *)(_2+12)) = _3941;
    *((int *)(_2+16)) = _3942;
    *((int *)(_2+20)) = _3943;
    _3944 = MAKE_SEQ(_1);
    _3943 = NOVALUE;
    _3942 = NOVALUE;
    _3941 = NOVALUE;
    _3940 = NOVALUE;
    _3945 = call_c(1, _15xGetDiskFreeSpace_5931, _3944);
    DeRefDS(_3944);
    _3944 = NOVALUE;
    if (_3945 == 0) {
        DeRef(_3945);
        _3945 = NOVALUE;
        goto L3; // [86] 101
    }
    else {
        if (!IS_ATOM_INT(_3945) && DBL_PTR(_3945)->dbl == 0.0){
            DeRef(_3945);
            _3945 = NOVALUE;
            goto L3; // [86] 101
        }
        DeRef(_3945);
        _3945 = NOVALUE;
    }
    DeRef(_3945);
    _3945 = NOVALUE;

    /** 			result = peek4s({metric_addr, 4}) */
    Ref(_metric_addr_7217);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _metric_addr_7217;
    ((int *)_2)[2] = 4;
    _3946 = MAKE_SEQ(_1);
    DeRef(_result_7214);
    _1 = (int)SEQ_PTR(_3946);
    peek4_addr = (unsigned long *)get_pos_int("peek4s/peek4u", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _result_7214 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)*peek4_addr++;
        if (_1 < MININT || _1 > MAXINT)
        _1 = NewDouble((double)(long)_1);
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_3946);
    _3946 = NOVALUE;
L3: 

    /** 		free({path_addr, metric_addr}) */
    Ref(_metric_addr_7217);
    Ref(_path_addr_7216);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _path_addr_7216;
    ((int *)_2)[2] = _metric_addr_7217;
    _3948 = MAKE_SEQ(_1);
    _7free(_3948);
    _3948 = NOVALUE;

    /** 	return result */
    DeRef(_disk_path_7213);
    DeRef(_path_addr_7216);
    DeRef(_metric_addr_7217);
    return _result_7214;
    ;
}


int _15disk_size(int _disk_path_7239)
{
    int _disk_size_7240 = NOVALUE;
    int _result_7242 = NOVALUE;
    int _bytes_per_cluster_7243 = NOVALUE;
    int _3961 = NOVALUE;
    int _3960 = NOVALUE;
    int _3959 = NOVALUE;
    int _3958 = NOVALUE;
    int _3957 = NOVALUE;
    int _3956 = NOVALUE;
    int _3955 = NOVALUE;
    int _3953 = NOVALUE;
    int _3952 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence disk_size = {0,0,0, disk_path}*/
    _0 = _disk_size_7240;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    Ref(_disk_path_7239);
    *((int *)(_2+16)) = _disk_path_7239;
    _disk_size_7240 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	ifdef WINDOWS then*/

    /** 		sequence result */

    /** 		atom bytes_per_cluster*/

    /** 		result = disk_metrics(disk_path) */
    Ref(_disk_path_7239);
    _0 = _result_7242;
    _result_7242 = _15disk_metrics(_disk_path_7239);
    DeRef(_0);

    /** 		bytes_per_cluster = result[BYTES_PER_SECTOR] * result[SECTORS_PER_CLUSTER]*/
    _2 = (int)SEQ_PTR(_result_7242);
    _3952 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_result_7242);
    _3953 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_bytes_per_cluster_7243);
    if (IS_ATOM_INT(_3952) && IS_ATOM_INT(_3953)) {
        if (_3952 == (short)_3952 && _3953 <= INT15 && _3953 >= -INT15)
        _bytes_per_cluster_7243 = _3952 * _3953;
        else
        _bytes_per_cluster_7243 = NewDouble(_3952 * (double)_3953);
    }
    else {
        _bytes_per_cluster_7243 = binary_op(MULTIPLY, _3952, _3953);
    }
    _3952 = NOVALUE;
    _3953 = NOVALUE;

    /** 		disk_size[TOTAL_BYTES] = bytes_per_cluster * result[TOTAL_NUMBER_OF_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_7242);
    _3955 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_bytes_per_cluster_7243) && IS_ATOM_INT(_3955)) {
        if (_bytes_per_cluster_7243 == (short)_bytes_per_cluster_7243 && _3955 <= INT15 && _3955 >= -INT15)
        _3956 = _bytes_per_cluster_7243 * _3955;
        else
        _3956 = NewDouble(_bytes_per_cluster_7243 * (double)_3955);
    }
    else {
        _3956 = binary_op(MULTIPLY, _bytes_per_cluster_7243, _3955);
    }
    _3955 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_7240);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_7240 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _3956;
    if( _1 != _3956 ){
        DeRef(_1);
    }
    _3956 = NOVALUE;

    /** 		disk_size[FREE_BYTES]  = bytes_per_cluster * result[NUMBER_OF_FREE_CLUSTERS] */
    _2 = (int)SEQ_PTR(_result_7242);
    _3957 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_bytes_per_cluster_7243) && IS_ATOM_INT(_3957)) {
        if (_bytes_per_cluster_7243 == (short)_bytes_per_cluster_7243 && _3957 <= INT15 && _3957 >= -INT15)
        _3958 = _bytes_per_cluster_7243 * _3957;
        else
        _3958 = NewDouble(_bytes_per_cluster_7243 * (double)_3957);
    }
    else {
        _3958 = binary_op(MULTIPLY, _bytes_per_cluster_7243, _3957);
    }
    _3957 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_7240);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_7240 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _3958;
    if( _1 != _3958 ){
        DeRef(_1);
    }
    _3958 = NOVALUE;

    /** 		disk_size[USED_BYTES]  = disk_size[TOTAL_BYTES] - disk_size[FREE_BYTES] */
    _2 = (int)SEQ_PTR(_disk_size_7240);
    _3959 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_disk_size_7240);
    _3960 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_3959) && IS_ATOM_INT(_3960)) {
        _3961 = _3959 - _3960;
        if ((long)((unsigned long)_3961 +(unsigned long) HIGH_BITS) >= 0){
            _3961 = NewDouble((double)_3961);
        }
    }
    else {
        _3961 = binary_op(MINUS, _3959, _3960);
    }
    _3959 = NOVALUE;
    _3960 = NOVALUE;
    _2 = (int)SEQ_PTR(_disk_size_7240);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _disk_size_7240 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _3961;
    if( _1 != _3961 ){
        DeRef(_1);
    }
    _3961 = NOVALUE;

    /** 	return disk_size */
    DeRef(_disk_path_7239);
    DeRefDS(_result_7242);
    DeRef(_bytes_per_cluster_7243);
    return _disk_size_7240;
    ;
}


int _15count_files(int _orig_path_7265, int _dir_info_7266, int _inst_7267)
{
    int _pos_7268 = NOVALUE;
    int _ext_7269 = NOVALUE;
    int _fileext_inlined_fileext_at_245_7312 = NOVALUE;
    int _data_inlined_fileext_at_245_7311 = NOVALUE;
    int _path_inlined_fileext_at_242_7310 = NOVALUE;
    int _4032 = NOVALUE;
    int _4031 = NOVALUE;
    int _4030 = NOVALUE;
    int _4028 = NOVALUE;
    int _4027 = NOVALUE;
    int _4026 = NOVALUE;
    int _4025 = NOVALUE;
    int _4023 = NOVALUE;
    int _4022 = NOVALUE;
    int _4020 = NOVALUE;
    int _4019 = NOVALUE;
    int _4018 = NOVALUE;
    int _4017 = NOVALUE;
    int _4016 = NOVALUE;
    int _4015 = NOVALUE;
    int _4014 = NOVALUE;
    int _4012 = NOVALUE;
    int _4011 = NOVALUE;
    int _4009 = NOVALUE;
    int _4008 = NOVALUE;
    int _4007 = NOVALUE;
    int _4006 = NOVALUE;
    int _4005 = NOVALUE;
    int _4004 = NOVALUE;
    int _4003 = NOVALUE;
    int _4002 = NOVALUE;
    int _4001 = NOVALUE;
    int _4000 = NOVALUE;
    int _3999 = NOVALUE;
    int _3998 = NOVALUE;
    int _3997 = NOVALUE;
    int _3996 = NOVALUE;
    int _3994 = NOVALUE;
    int _3993 = NOVALUE;
    int _3992 = NOVALUE;
    int _3991 = NOVALUE;
    int _3989 = NOVALUE;
    int _3988 = NOVALUE;
    int _3987 = NOVALUE;
    int _3986 = NOVALUE;
    int _3985 = NOVALUE;
    int _3984 = NOVALUE;
    int _3983 = NOVALUE;
    int _3981 = NOVALUE;
    int _3980 = NOVALUE;
    int _3979 = NOVALUE;
    int _3978 = NOVALUE;
    int _3977 = NOVALUE;
    int _3976 = NOVALUE;
    int _3973 = NOVALUE;
    int _3972 = NOVALUE;
    int _3971 = NOVALUE;
    int _3970 = NOVALUE;
    int _3969 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer pos = 0*/
    _pos_7268 = 0;

    /** 	orig_path = orig_path*/
    RefDS(_orig_path_7265);
    DeRefDS(_orig_path_7265);
    _orig_path_7265 = _orig_path_7265;

    /** 	if equal(dir_info[D_NAME], ".") then*/
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _3969 = (int)*(((s1_ptr)_2)->base + 1);
    if (_3969 == _3194)
    _3970 = 1;
    else if (IS_ATOM_INT(_3969) && IS_ATOM_INT(_3194))
    _3970 = 0;
    else
    _3970 = (compare(_3969, _3194) == 0);
    _3969 = NOVALUE;
    if (_3970 == 0)
    {
        _3970 = NOVALUE;
        goto L1; // [33] 43
    }
    else{
        _3970 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_7265);
    DeRefDS(_dir_info_7266);
    DeRefDS(_inst_7267);
    DeRef(_ext_7269);
    return 0;
L1: 

    /** 	if equal(dir_info[D_NAME], "..") then*/
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _3971 = (int)*(((s1_ptr)_2)->base + 1);
    if (_3971 == _3226)
    _3972 = 1;
    else if (IS_ATOM_INT(_3971) && IS_ATOM_INT(_3226))
    _3972 = 0;
    else
    _3972 = (compare(_3971, _3226) == 0);
    _3971 = NOVALUE;
    if (_3972 == 0)
    {
        _3972 = NOVALUE;
        goto L2; // [55] 65
    }
    else{
        _3972 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_orig_path_7265);
    DeRefDS(_dir_info_7266);
    DeRefDS(_inst_7267);
    DeRef(_ext_7269);
    return 0;
L2: 

    /** 	if inst[1] = 0 then -- count all is false*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _3973 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _3973, 0)){
        _3973 = NOVALUE;
        goto L3; // [71] 122
    }
    _3973 = NOVALUE;

    /** 		if find('h', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _3976 = (int)*(((s1_ptr)_2)->base + 2);
    _3977 = find_from(104, _3976, 1);
    _3976 = NOVALUE;
    if (_3977 == 0)
    {
        _3977 = NOVALUE;
        goto L4; // [88] 98
    }
    else{
        _3977 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_7265);
    DeRefDS(_dir_info_7266);
    DeRefDS(_inst_7267);
    DeRef(_ext_7269);
    return 0;
L4: 

    /** 		if find('s', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _3978 = (int)*(((s1_ptr)_2)->base + 2);
    _3979 = find_from(115, _3978, 1);
    _3978 = NOVALUE;
    if (_3979 == 0)
    {
        _3979 = NOVALUE;
        goto L5; // [111] 121
    }
    else{
        _3979 = NOVALUE;
    }

    /** 			return 0*/
    DeRefDS(_orig_path_7265);
    DeRefDS(_dir_info_7266);
    DeRefDS(_inst_7267);
    DeRef(_ext_7269);
    return 0;
L5: 
L3: 

    /** 	file_counters[inst[2]][COUNT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _3980 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _15file_counters_7262 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_3980))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_3980)->dbl));
    else
    _3 = (int)(_3980 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _3983 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _3984 = (int)*(((s1_ptr)_2)->base + 3);
    _3981 = NOVALUE;
    if (IS_ATOM_INT(_3984) && IS_ATOM_INT(_3983)) {
        _3985 = _3984 + _3983;
        if ((long)((unsigned long)_3985 + (unsigned long)HIGH_BITS) >= 0) 
        _3985 = NewDouble((double)_3985);
    }
    else {
        _3985 = binary_op(PLUS, _3984, _3983);
    }
    _3984 = NOVALUE;
    _3983 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _3985;
    if( _1 != _3985 ){
        DeRef(_1);
    }
    _3985 = NOVALUE;
    _3981 = NOVALUE;

    /** 	if find('d', dir_info[D_ATTRIBUTES]) then*/
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _3986 = (int)*(((s1_ptr)_2)->base + 2);
    _3987 = find_from(100, _3986, 1);
    _3986 = NOVALUE;
    if (_3987 == 0)
    {
        _3987 = NOVALUE;
        goto L6; // [168] 201
    }
    else{
        _3987 = NOVALUE;
    }

    /** 		file_counters[inst[2]][COUNT_DIRS] += 1*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _3988 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _15file_counters_7262 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_3988))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_3988)->dbl));
    else
    _3 = (int)(_3988 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _3991 = (int)*(((s1_ptr)_2)->base + 1);
    _3989 = NOVALUE;
    if (IS_ATOM_INT(_3991)) {
        _3992 = _3991 + 1;
        if (_3992 > MAXINT){
            _3992 = NewDouble((double)_3992);
        }
    }
    else
    _3992 = binary_op(PLUS, 1, _3991);
    _3991 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _3992;
    if( _1 != _3992 ){
        DeRef(_1);
    }
    _3992 = NOVALUE;
    _3989 = NOVALUE;
    goto L7; // [198] 512
L6: 

    /** 		file_counters[inst[2]][COUNT_FILES] += 1*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _3993 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _15file_counters_7262 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_3993))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_3993)->dbl));
    else
    _3 = (int)(_3993 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _3996 = (int)*(((s1_ptr)_2)->base + 2);
    _3994 = NOVALUE;
    if (IS_ATOM_INT(_3996)) {
        _3997 = _3996 + 1;
        if (_3997 > MAXINT){
            _3997 = NewDouble((double)_3997);
        }
    }
    else
    _3997 = binary_op(PLUS, 1, _3996);
    _3996 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _3997;
    if( _1 != _3997 ){
        DeRef(_1);
    }
    _3997 = NOVALUE;
    _3994 = NOVALUE;

    /** 		ifdef not UNIX then*/

    /** 			ext = fileext(lower(dir_info[D_NAME]))*/
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _3998 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3998);
    _3999 = _12lower(_3998);
    _3998 = NOVALUE;
    DeRef(_path_inlined_fileext_at_242_7310);
    _path_inlined_fileext_at_242_7310 = _3999;
    _3999 = NOVALUE;

    /** 	data = pathinfo(path)*/
    Ref(_path_inlined_fileext_at_242_7310);
    _0 = _data_inlined_fileext_at_245_7311;
    _data_inlined_fileext_at_245_7311 = _15pathinfo(_path_inlined_fileext_at_242_7310, 0);
    DeRef(_0);

    /** 	return data[4]*/
    DeRef(_ext_7269);
    _2 = (int)SEQ_PTR(_data_inlined_fileext_at_245_7311);
    _ext_7269 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_ext_7269);
    DeRef(_path_inlined_fileext_at_242_7310);
    _path_inlined_fileext_at_242_7310 = NOVALUE;
    DeRef(_data_inlined_fileext_at_245_7311);
    _data_inlined_fileext_at_245_7311 = NOVALUE;

    /** 		pos = 0*/
    _pos_7268 = 0;

    /** 		for i = 1 to length(file_counters[inst[2]][COUNT_TYPES]) do*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _4000 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!IS_ATOM_INT(_4000)){
        _4001 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4000)->dbl));
    }
    else{
        _4001 = (int)*(((s1_ptr)_2)->base + _4000);
    }
    _2 = (int)SEQ_PTR(_4001);
    _4002 = (int)*(((s1_ptr)_2)->base + 4);
    _4001 = NOVALUE;
    if (IS_SEQUENCE(_4002)){
            _4003 = SEQ_PTR(_4002)->length;
    }
    else {
        _4003 = 1;
    }
    _4002 = NOVALUE;
    {
        int _i_7314;
        _i_7314 = 1;
L8: 
        if (_i_7314 > _4003){
            goto L9; // [295] 358
        }

        /** 			if equal(file_counters[inst[2]][COUNT_TYPES][i][EXT_NAME], ext) then*/
        _2 = (int)SEQ_PTR(_inst_7267);
        _4004 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_15file_counters_7262);
        if (!IS_ATOM_INT(_4004)){
            _4005 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4004)->dbl));
        }
        else{
            _4005 = (int)*(((s1_ptr)_2)->base + _4004);
        }
        _2 = (int)SEQ_PTR(_4005);
        _4006 = (int)*(((s1_ptr)_2)->base + 4);
        _4005 = NOVALUE;
        _2 = (int)SEQ_PTR(_4006);
        _4007 = (int)*(((s1_ptr)_2)->base + _i_7314);
        _4006 = NOVALUE;
        _2 = (int)SEQ_PTR(_4007);
        _4008 = (int)*(((s1_ptr)_2)->base + 1);
        _4007 = NOVALUE;
        if (_4008 == _ext_7269)
        _4009 = 1;
        else if (IS_ATOM_INT(_4008) && IS_ATOM_INT(_ext_7269))
        _4009 = 0;
        else
        _4009 = (compare(_4008, _ext_7269) == 0);
        _4008 = NOVALUE;
        if (_4009 == 0)
        {
            _4009 = NOVALUE;
            goto LA; // [336] 351
        }
        else{
            _4009 = NOVALUE;
        }

        /** 				pos = i*/
        _pos_7268 = _i_7314;

        /** 				exit*/
        goto L9; // [348] 358
LA: 

        /** 		end for*/
        _i_7314 = _i_7314 + 1;
        goto L8; // [353] 302
L9: 
        ;
    }

    /** 		if pos = 0 then*/
    if (_pos_7268 != 0)
    goto LB; // [360] 427

    /** 			file_counters[inst[2]][COUNT_TYPES] &= {{ext, 0, 0}}*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _4011 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _15file_counters_7262 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4011))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4011)->dbl));
    else
    _3 = (int)(_4011 + ((s1_ptr)_2)->base);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_ext_7269);
    *((int *)(_2+4)) = _ext_7269;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    _4014 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _4014;
    _4015 = MAKE_SEQ(_1);
    _4014 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4016 = (int)*(((s1_ptr)_2)->base + 4);
    _4012 = NOVALUE;
    if (IS_SEQUENCE(_4016) && IS_ATOM(_4015)) {
    }
    else if (IS_ATOM(_4016) && IS_SEQUENCE(_4015)) {
        Ref(_4016);
        Prepend(&_4017, _4015, _4016);
    }
    else {
        Concat((object_ptr)&_4017, _4016, _4015);
        _4016 = NOVALUE;
    }
    _4016 = NOVALUE;
    DeRefDS(_4015);
    _4015 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _4017;
    if( _1 != _4017 ){
        DeRef(_1);
    }
    _4017 = NOVALUE;
    _4012 = NOVALUE;

    /** 			pos = length(file_counters[inst[2]][COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _4018 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!IS_ATOM_INT(_4018)){
        _4019 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_4018)->dbl));
    }
    else{
        _4019 = (int)*(((s1_ptr)_2)->base + _4018);
    }
    _2 = (int)SEQ_PTR(_4019);
    _4020 = (int)*(((s1_ptr)_2)->base + 4);
    _4019 = NOVALUE;
    if (IS_SEQUENCE(_4020)){
            _pos_7268 = SEQ_PTR(_4020)->length;
    }
    else {
        _pos_7268 = 1;
    }
    _4020 = NOVALUE;
LB: 

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _4022 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _15file_counters_7262 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4022))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4022)->dbl));
    else
    _3 = (int)(_4022 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4023 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_7268 + ((s1_ptr)_2)->base);
    _4023 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4025 = (int)*(((s1_ptr)_2)->base + 2);
    _4023 = NOVALUE;
    if (IS_ATOM_INT(_4025)) {
        _4026 = _4025 + 1;
        if (_4026 > MAXINT){
            _4026 = NewDouble((double)_4026);
        }
    }
    else
    _4026 = binary_op(PLUS, 1, _4025);
    _4025 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _4026;
    if( _1 != _4026 ){
        DeRef(_1);
    }
    _4026 = NOVALUE;
    _4023 = NOVALUE;

    /** 		file_counters[inst[2]][COUNT_TYPES][pos][EXT_SIZE] += dir_info[D_SIZE]*/
    _2 = (int)SEQ_PTR(_inst_7267);
    _4027 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _15file_counters_7262 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_4027))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_4027)->dbl));
    else
    _3 = (int)(_4027 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(4 + ((s1_ptr)_2)->base);
    _4028 = NOVALUE;
    _2 = (int)SEQ_PTR(*(object_ptr)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(object_ptr)_3 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pos_7268 + ((s1_ptr)_2)->base);
    _4028 = NOVALUE;
    _2 = (int)SEQ_PTR(_dir_info_7266);
    _4030 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _4031 = (int)*(((s1_ptr)_2)->base + 3);
    _4028 = NOVALUE;
    if (IS_ATOM_INT(_4031) && IS_ATOM_INT(_4030)) {
        _4032 = _4031 + _4030;
        if ((long)((unsigned long)_4032 + (unsigned long)HIGH_BITS) >= 0) 
        _4032 = NewDouble((double)_4032);
    }
    else {
        _4032 = binary_op(PLUS, _4031, _4030);
    }
    _4031 = NOVALUE;
    _4030 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _4032;
    if( _1 != _4032 ){
        DeRef(_1);
    }
    _4032 = NOVALUE;
    _4028 = NOVALUE;
L7: 

    /** 	return 0*/
    DeRefDS(_orig_path_7265);
    DeRefDS(_dir_info_7266);
    DeRefDS(_inst_7267);
    DeRef(_ext_7269);
    _3980 = NOVALUE;
    _3988 = NOVALUE;
    _3993 = NOVALUE;
    _4000 = NOVALUE;
    _4002 = NOVALUE;
    _4004 = NOVALUE;
    _4011 = NOVALUE;
    _4018 = NOVALUE;
    _4020 = NOVALUE;
    _4022 = NOVALUE;
    _4027 = NOVALUE;
    return 0;
    ;
}


int _15dir_size(int _dir_path_7352, int _count_all_7353)
{
    int _fc_7354 = NOVALUE;
    int _4047 = NOVALUE;
    int _4046 = NOVALUE;
    int _4044 = NOVALUE;
    int _4043 = NOVALUE;
    int _4041 = NOVALUE;
    int _4040 = NOVALUE;
    int _4039 = NOVALUE;
    int _4038 = NOVALUE;
    int _4037 = NOVALUE;
    int _4036 = NOVALUE;
    int _4033 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_all_7353)) {
        _1 = (long)(DBL_PTR(_count_all_7353)->dbl);
        if (UNIQUE(DBL_PTR(_count_all_7353)) && (DBL_PTR(_count_all_7353)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_all_7353);
        _count_all_7353 = _1;
    }

    /** 	file_counters = append(file_counters, {0,0,0,{}})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    RefDS(_5);
    *((int *)(_2+16)) = _5;
    _4033 = MAKE_SEQ(_1);
    RefDS(_4033);
    Append(&_15file_counters_7262, _15file_counters_7262, _4033);
    DeRefDS(_4033);
    _4033 = NOVALUE;

    /** 	walk_dir(dir_path, {routine_id("count_files"), {count_all, length(file_counters)}}, 0)*/
    _4036 = CRoutineId(302, 15, _4035);
    if (IS_SEQUENCE(_15file_counters_7262)){
            _4037 = SEQ_PTR(_15file_counters_7262)->length;
    }
    else {
        _4037 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _count_all_7353;
    ((int *)_2)[2] = _4037;
    _4038 = MAKE_SEQ(_1);
    _4037 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4036;
    ((int *)_2)[2] = _4038;
    _4039 = MAKE_SEQ(_1);
    _4038 = NOVALUE;
    _4036 = NOVALUE;
    RefDS(_dir_path_7352);
    _4040 = _15walk_dir(_dir_path_7352, _4039, 0, -99999);
    _4039 = NOVALUE;

    /** 	fc = file_counters[$]*/
    if (IS_SEQUENCE(_15file_counters_7262)){
            _4041 = SEQ_PTR(_15file_counters_7262)->length;
    }
    else {
        _4041 = 1;
    }
    DeRef(_fc_7354);
    _2 = (int)SEQ_PTR(_15file_counters_7262);
    _fc_7354 = (int)*(((s1_ptr)_2)->base + _4041);
    RefDS(_fc_7354);

    /** 	file_counters = file_counters[1 .. $-1]*/
    if (IS_SEQUENCE(_15file_counters_7262)){
            _4043 = SEQ_PTR(_15file_counters_7262)->length;
    }
    else {
        _4043 = 1;
    }
    _4044 = _4043 - 1;
    _4043 = NOVALUE;
    rhs_slice_target = (object_ptr)&_15file_counters_7262;
    RHS_Slice(_15file_counters_7262, 1, _4044);

    /** 	fc[COUNT_TYPES] = stdsort:sort(fc[COUNT_TYPES])*/
    _2 = (int)SEQ_PTR(_fc_7354);
    _4046 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_4046);
    _4047 = _22sort(_4046, 1);
    _4046 = NOVALUE;
    _2 = (int)SEQ_PTR(_fc_7354);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _fc_7354 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _4047;
    if( _1 != _4047 ){
        DeRef(_1);
    }
    _4047 = NOVALUE;

    /** 	return fc*/
    DeRefDS(_dir_path_7352);
    _4044 = NOVALUE;
    DeRef(_4040);
    _4040 = NOVALUE;
    return _fc_7354;
    ;
}


int _15temp_file(int _temp_location_7372, int _temp_prefix_7373, int _temp_extn_7374, int _reserve_temp_7376)
{
    int _randname_7377 = NOVALUE;
    int _envtmp_7381 = NOVALUE;
    int _tdir_7400 = NOVALUE;
    int _5822 = NOVALUE;
    int _4084 = NOVALUE;
    int _4082 = NOVALUE;
    int _4080 = NOVALUE;
    int _4078 = NOVALUE;
    int _4077 = NOVALUE;
    int _4076 = NOVALUE;
    int _4072 = NOVALUE;
    int _4071 = NOVALUE;
    int _4070 = NOVALUE;
    int _4069 = NOVALUE;
    int _4066 = NOVALUE;
    int _4065 = NOVALUE;
    int _4064 = NOVALUE;
    int _4059 = NOVALUE;
    int _4056 = NOVALUE;
    int _4053 = NOVALUE;
    int _4049 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_reserve_temp_7376)) {
        _1 = (long)(DBL_PTR(_reserve_temp_7376)->dbl);
        if (UNIQUE(DBL_PTR(_reserve_temp_7376)) && (DBL_PTR(_reserve_temp_7376)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_reserve_temp_7376);
        _reserve_temp_7376 = _1;
    }

    /** 	if length(temp_location) = 0 then*/
    if (IS_SEQUENCE(_temp_location_7372)){
            _4049 = SEQ_PTR(_temp_location_7372)->length;
    }
    else {
        _4049 = 1;
    }
    if (_4049 != 0)
    goto L1; // [16] 69

    /** 		object envtmp*/

    /** 		envtmp = getenv("TEMP")*/
    DeRefi(_envtmp_7381);
    _envtmp_7381 = EGetEnv(_4051);

    /** 		if atom(envtmp) then*/
    _4053 = IS_ATOM(_envtmp_7381);
    if (_4053 == 0)
    {
        _4053 = NOVALUE;
        goto L2; // [32] 41
    }
    else{
        _4053 = NOVALUE;
    }

    /** 			envtmp = getenv("TMP")*/
    DeRefi(_envtmp_7381);
    _envtmp_7381 = EGetEnv(_4054);
L2: 

    /** 		ifdef WINDOWS then			*/

    /** 			if atom(envtmp) then*/
    _4056 = IS_ATOM(_envtmp_7381);
    if (_4056 == 0)
    {
        _4056 = NOVALUE;
        goto L3; // [48] 57
    }
    else{
        _4056 = NOVALUE;
    }

    /** 				envtmp = "C:\\temp\\"*/
    RefDS(_4057);
    DeRefi(_envtmp_7381);
    _envtmp_7381 = _4057;
L3: 

    /** 		temp_location = envtmp*/
    Ref(_envtmp_7381);
    DeRefDS(_temp_location_7372);
    _temp_location_7372 = _envtmp_7381;
    DeRefi(_envtmp_7381);
    _envtmp_7381 = NOVALUE;
    goto L4; // [66] 163
L1: 

    /** 		switch file_type(temp_location) do*/
    RefDS(_temp_location_7372);
    _4059 = _15file_type(_temp_location_7372);
    if (IS_SEQUENCE(_4059) ){
        goto L5; // [75] 152
    }
    if(!IS_ATOM_INT(_4059)){
        if( (DBL_PTR(_4059)->dbl != (double) ((int) DBL_PTR(_4059)->dbl) ) ){
            goto L5; // [75] 152
        }
        _0 = (int) DBL_PTR(_4059)->dbl;
    }
    else {
        _0 = _4059;
    };
    DeRef(_4059);
    _4059 = NOVALUE;
    switch ( _0 ){ 

        /** 			case FILETYPE_FILE then*/
        case 1:

        /** 				temp_location = dirname(temp_location, 1)*/
        RefDS(_temp_location_7372);
        _0 = _temp_location_7372;
        _temp_location_7372 = _15dirname(_temp_location_7372, 1);
        DeRefDS(_0);
        goto L6; // [93] 162

        /** 			case FILETYPE_DIRECTORY then*/
        case 2:

        /** 				temp_location = temp_location*/
        RefDS(_temp_location_7372);
        DeRefDS(_temp_location_7372);
        _temp_location_7372 = _temp_location_7372;
        goto L6; // [106] 162

        /** 			case FILETYPE_NOT_FOUND then*/
        case 0:

        /** 				object tdir = dirname(temp_location, 1)*/
        RefDS(_temp_location_7372);
        _0 = _tdir_7400;
        _tdir_7400 = _15dirname(_temp_location_7372, 1);
        DeRef(_0);

        /** 				if file_exists(tdir) then*/
        Ref(_tdir_7400);
        _4064 = _15file_exists(_tdir_7400);
        if (_4064 == 0) {
            DeRef(_4064);
            _4064 = NOVALUE;
            goto L7; // [125] 138
        }
        else {
            if (!IS_ATOM_INT(_4064) && DBL_PTR(_4064)->dbl == 0.0){
                DeRef(_4064);
                _4064 = NOVALUE;
                goto L7; // [125] 138
            }
            DeRef(_4064);
            _4064 = NOVALUE;
        }
        DeRef(_4064);
        _4064 = NOVALUE;

        /** 					temp_location = tdir*/
        Ref(_tdir_7400);
        DeRefDS(_temp_location_7372);
        _temp_location_7372 = _tdir_7400;
        goto L8; // [135] 146
L7: 

        /** 					temp_location = "."*/
        RefDS(_3194);
        DeRefDS(_temp_location_7372);
        _temp_location_7372 = _3194;
L8: 
        DeRef(_tdir_7400);
        _tdir_7400 = NOVALUE;
        goto L6; // [148] 162

        /** 			case else*/
        default:
L5: 

        /** 				temp_location = "."*/
        RefDS(_3194);
        DeRefDS(_temp_location_7372);
        _temp_location_7372 = _3194;
    ;}L6: 
L4: 

    /** 	if temp_location[$] != SLASH then*/
    if (IS_SEQUENCE(_temp_location_7372)){
            _4065 = SEQ_PTR(_temp_location_7372)->length;
    }
    else {
        _4065 = 1;
    }
    _2 = (int)SEQ_PTR(_temp_location_7372);
    _4066 = (int)*(((s1_ptr)_2)->base + _4065);
    if (binary_op_a(EQUALS, _4066, 92)){
        _4066 = NOVALUE;
        goto L9; // [172] 183
    }
    _4066 = NOVALUE;

    /** 		temp_location &= SLASH*/
    Append(&_temp_location_7372, _temp_location_7372, 92);
L9: 

    /** 	if length(temp_extn) and temp_extn[1] != '.' then*/
    if (IS_SEQUENCE(_temp_extn_7374)){
            _4069 = SEQ_PTR(_temp_extn_7374)->length;
    }
    else {
        _4069 = 1;
    }
    if (_4069 == 0) {
        goto LA; // [188] 211
    }
    _2 = (int)SEQ_PTR(_temp_extn_7374);
    _4071 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4071)) {
        _4072 = (_4071 != 46);
    }
    else {
        _4072 = binary_op(NOTEQ, _4071, 46);
    }
    _4071 = NOVALUE;
    if (_4072 == 0) {
        DeRef(_4072);
        _4072 = NOVALUE;
        goto LA; // [201] 211
    }
    else {
        if (!IS_ATOM_INT(_4072) && DBL_PTR(_4072)->dbl == 0.0){
            DeRef(_4072);
            _4072 = NOVALUE;
            goto LA; // [201] 211
        }
        DeRef(_4072);
        _4072 = NOVALUE;
    }
    DeRef(_4072);
    _4072 = NOVALUE;

    /** 		temp_extn = '.' & temp_extn*/
    Prepend(&_temp_extn_7374, _temp_extn_7374, 46);
LA: 

    /** 	while 1 do*/
LB: 

    /** 		randname = sprintf("%s%s%06d%s", {temp_location, temp_prefix, rand(1_000_000) - 1, temp_extn})*/
    _4076 = good_rand() % ((unsigned)1000000) + 1;
    _4077 = _4076 - 1;
    _4076 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_temp_location_7372);
    *((int *)(_2+4)) = _temp_location_7372;
    RefDS(_temp_prefix_7373);
    *((int *)(_2+8)) = _temp_prefix_7373;
    *((int *)(_2+12)) = _4077;
    RefDS(_temp_extn_7374);
    *((int *)(_2+16)) = _temp_extn_7374;
    _4078 = MAKE_SEQ(_1);
    _4077 = NOVALUE;
    DeRefi(_randname_7377);
    _randname_7377 = EPrintf(-9999999, _4074, _4078);
    DeRefDS(_4078);
    _4078 = NOVALUE;

    /** 		if not file_exists( randname ) then*/
    RefDS(_randname_7377);
    _4080 = _15file_exists(_randname_7377);
    if (IS_ATOM_INT(_4080)) {
        if (_4080 != 0){
            DeRef(_4080);
            _4080 = NOVALUE;
            goto LB; // [242] 216
        }
    }
    else {
        if (DBL_PTR(_4080)->dbl != 0.0){
            DeRef(_4080);
            _4080 = NOVALUE;
            goto LB; // [242] 216
        }
    }
    DeRef(_4080);
    _4080 = NOVALUE;

    /** 			exit*/
    goto LC; // [247] 255

    /** 	end while*/
    goto LB; // [252] 216
LC: 

    /** 	if reserve_temp then*/
    if (_reserve_temp_7376 == 0)
    {
        goto LD; // [257] 304
    }
    else{
    }

    /** 		if not file_exists(temp_location) then*/
    RefDS(_temp_location_7372);
    _4082 = _15file_exists(_temp_location_7372);
    if (IS_ATOM_INT(_4082)) {
        if (_4082 != 0){
            DeRef(_4082);
            _4082 = NOVALUE;
            goto LE; // [266] 289
        }
    }
    else {
        if (DBL_PTR(_4082)->dbl != 0.0){
            DeRef(_4082);
            _4082 = NOVALUE;
            goto LE; // [266] 289
        }
    }
    DeRef(_4082);
    _4082 = NOVALUE;

    /** 			if create_directory(temp_location) = 0 then*/
    RefDS(_temp_location_7372);
    _4084 = _15create_directory(_temp_location_7372, 448, 1);
    if (binary_op_a(NOTEQ, _4084, 0)){
        DeRef(_4084);
        _4084 = NOVALUE;
        goto LF; // [277] 288
    }
    DeRef(_4084);
    _4084 = NOVALUE;

    /** 				return ""*/
    RefDS(_5);
    DeRefDS(_temp_location_7372);
    DeRefDS(_temp_prefix_7373);
    DeRefDS(_temp_extn_7374);
    DeRefi(_randname_7377);
    return _5;
LF: 
LE: 

    /** 		io:write_file(randname, "")*/
    RefDS(_randname_7377);
    RefDS(_5);
    _5822 = _6write_file(_randname_7377, _5, 1);
    DeRef(_5822);
    _5822 = NOVALUE;
LD: 

    /** 	return randname*/
    DeRefDS(_temp_location_7372);
    DeRefDS(_temp_prefix_7373);
    DeRefDS(_temp_extn_7374);
    return _randname_7377;
    ;
}


int _15checksum(int _filename_7437, int _size_7438, int _usename_7439, int _return_text_7440)
{
    int _fn_7441 = NOVALUE;
    int _cs_7442 = NOVALUE;
    int _hits_7443 = NOVALUE;
    int _ix_7444 = NOVALUE;
    int _jx_7445 = NOVALUE;
    int _fx_7446 = NOVALUE;
    int _data_7447 = NOVALUE;
    int _nhit_7448 = NOVALUE;
    int _nmiss_7449 = NOVALUE;
    int _cs_text_7521 = NOVALUE;
    int _4145 = NOVALUE;
    int _4143 = NOVALUE;
    int _4142 = NOVALUE;
    int _4140 = NOVALUE;
    int _4138 = NOVALUE;
    int _4137 = NOVALUE;
    int _4136 = NOVALUE;
    int _4135 = NOVALUE;
    int _4134 = NOVALUE;
    int _4132 = NOVALUE;
    int _4130 = NOVALUE;
    int _4128 = NOVALUE;
    int _4127 = NOVALUE;
    int _4126 = NOVALUE;
    int _4125 = NOVALUE;
    int _4124 = NOVALUE;
    int _4123 = NOVALUE;
    int _4122 = NOVALUE;
    int _4120 = NOVALUE;
    int _4117 = NOVALUE;
    int _4115 = NOVALUE;
    int _4114 = NOVALUE;
    int _4113 = NOVALUE;
    int _4112 = NOVALUE;
    int _4111 = NOVALUE;
    int _4108 = NOVALUE;
    int _4107 = NOVALUE;
    int _4106 = NOVALUE;
    int _4105 = NOVALUE;
    int _4103 = NOVALUE;
    int _4100 = NOVALUE;
    int _4098 = NOVALUE;
    int _4094 = NOVALUE;
    int _4093 = NOVALUE;
    int _4092 = NOVALUE;
    int _4091 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_7438)) {
        _1 = (long)(DBL_PTR(_size_7438)->dbl);
        if (UNIQUE(DBL_PTR(_size_7438)) && (DBL_PTR(_size_7438)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_7438);
        _size_7438 = _1;
    }
    if (!IS_ATOM_INT(_usename_7439)) {
        _1 = (long)(DBL_PTR(_usename_7439)->dbl);
        if (UNIQUE(DBL_PTR(_usename_7439)) && (DBL_PTR(_usename_7439)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_usename_7439);
        _usename_7439 = _1;
    }
    if (!IS_ATOM_INT(_return_text_7440)) {
        _1 = (long)(DBL_PTR(_return_text_7440)->dbl);
        if (UNIQUE(DBL_PTR(_return_text_7440)) && (DBL_PTR(_return_text_7440)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_return_text_7440);
        _return_text_7440 = _1;
    }

    /** 	if size <= 0 then*/
    if (_size_7438 > 0)
    goto L1; // [17] 28

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_7437);
    DeRef(_cs_7442);
    DeRef(_hits_7443);
    DeRef(_jx_7445);
    DeRef(_fx_7446);
    DeRefi(_data_7447);
    return _5;
L1: 

    /** 	fn = open(filename, "rb")*/
    _fn_7441 = EOpen(_filename_7437, _3811, 0);

    /** 	if fn = -1 then*/
    if (_fn_7441 != -1)
    goto L2; // [39] 50

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_filename_7437);
    DeRef(_cs_7442);
    DeRef(_hits_7443);
    DeRef(_jx_7445);
    DeRef(_fx_7446);
    DeRefi(_data_7447);
    return _5;
L2: 

    /** 	jx = file_length(filename)*/
    RefDS(_filename_7437);
    _0 = _jx_7445;
    _jx_7445 = _15file_length(_filename_7437);
    DeRef(_0);

    /** 	cs = repeat(jx, size)*/
    DeRef(_cs_7442);
    _cs_7442 = Repeat(_jx_7445, _size_7438);

    /** 	for i = 1 to size do*/
    _4091 = _size_7438;
    {
        int _i_7458;
        _i_7458 = 1;
L3: 
        if (_i_7458 > _4091){
            goto L4; // [67] 99
        }

        /** 		cs[i] = hash(i + size, cs[i])*/
        _4092 = _i_7458 + _size_7438;
        if ((long)((unsigned long)_4092 + (unsigned long)HIGH_BITS) >= 0) 
        _4092 = NewDouble((double)_4092);
        _2 = (int)SEQ_PTR(_cs_7442);
        _4093 = (int)*(((s1_ptr)_2)->base + _i_7458);
        _4094 = calc_hash(_4092, _4093);
        DeRef(_4092);
        _4092 = NOVALUE;
        _4093 = NOVALUE;
        _2 = (int)SEQ_PTR(_cs_7442);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _cs_7442 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_7458);
        _1 = *(int *)_2;
        *(int *)_2 = _4094;
        if( _1 != _4094 ){
            DeRef(_1);
        }
        _4094 = NOVALUE;

        /** 	end for*/
        _i_7458 = _i_7458 + 1;
        goto L3; // [94] 74
L4: 
        ;
    }

    /** 	if usename != 0 then*/
    if (_usename_7439 == 0)
    goto L5; // [101] 236

    /** 		nhit = 0*/
    _nhit_7448 = 0;

    /** 		nmiss = 0*/
    _nmiss_7449 = 0;

    /** 		hits = {0,0}*/
    DeRef(_hits_7443);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _hits_7443 = MAKE_SEQ(_1);

    /** 		fx = hash(filename, stdhash:HSIEH32) -- Get a hash value for the whole name.*/
    DeRef(_fx_7446);
    _fx_7446 = calc_hash(_filename_7437, -5);

    /** 		while find(0, hits) do*/
L6: 
    _4098 = find_from(0, _hits_7443, 1);
    if (_4098 == 0)
    {
        _4098 = NOVALUE;
        goto L7; // [143] 235
    }
    else{
        _4098 = NOVALUE;
    }

    /** 			nhit += 1*/
    _nhit_7448 = _nhit_7448 + 1;

    /** 			if nhit > length(filename) then*/
    if (IS_SEQUENCE(_filename_7437)){
            _4100 = SEQ_PTR(_filename_7437)->length;
    }
    else {
        _4100 = 1;
    }
    if (_nhit_7448 <= _4100)
    goto L8; // [159] 177

    /** 				nhit = 1*/
    _nhit_7448 = 1;

    /** 				hits[1] = 1*/
    _2 = (int)SEQ_PTR(_hits_7443);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L8: 

    /** 			nmiss += 1*/
    _nmiss_7449 = _nmiss_7449 + 1;

    /** 			if nmiss > length(cs) then*/
    if (IS_SEQUENCE(_cs_7442)){
            _4103 = SEQ_PTR(_cs_7442)->length;
    }
    else {
        _4103 = 1;
    }
    if (_nmiss_7449 <= _4103)
    goto L9; // [190] 208

    /** 				nmiss = 1*/
    _nmiss_7449 = 1;

    /** 				hits[2] = 1*/
    _2 = (int)SEQ_PTR(_hits_7443);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
L9: 

    /** 			cs[nmiss] = hash(filename[nhit], xor_bits(fx, cs[nmiss]))*/
    _2 = (int)SEQ_PTR(_filename_7437);
    _4105 = (int)*(((s1_ptr)_2)->base + _nhit_7448);
    _2 = (int)SEQ_PTR(_cs_7442);
    _4106 = (int)*(((s1_ptr)_2)->base + _nmiss_7449);
    if (IS_ATOM_INT(_fx_7446) && IS_ATOM_INT(_4106)) {
        {unsigned long tu;
             tu = (unsigned long)_fx_7446 ^ (unsigned long)_4106;
             _4107 = MAKE_UINT(tu);
        }
    }
    else {
        _4107 = binary_op(XOR_BITS, _fx_7446, _4106);
    }
    _4106 = NOVALUE;
    _4108 = calc_hash(_4105, _4107);
    _4105 = NOVALUE;
    DeRef(_4107);
    _4107 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_7442);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_7442 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_7449);
    _1 = *(int *)_2;
    *(int *)_2 = _4108;
    if( _1 != _4108 ){
        DeRef(_1);
    }
    _4108 = NOVALUE;

    /** 		end while -- repeat until every bucket and every character has been used.*/
    goto L6; // [232] 138
L7: 
L5: 

    /** 	hits = repeat(0, size)*/
    DeRef(_hits_7443);
    _hits_7443 = Repeat(0, _size_7438);

    /** 	if jx != 0 then*/
    if (binary_op_a(EQUALS, _jx_7445, 0)){
        goto LA; // [244] 494
    }

    /** 		data = repeat(0, remainder( hash(jx * jx / size , stdhash:HSIEH32), 8) + 7)*/
    if (IS_ATOM_INT(_jx_7445) && IS_ATOM_INT(_jx_7445)) {
        if (_jx_7445 == (short)_jx_7445 && _jx_7445 <= INT15 && _jx_7445 >= -INT15)
        _4111 = _jx_7445 * _jx_7445;
        else
        _4111 = NewDouble(_jx_7445 * (double)_jx_7445);
    }
    else {
        if (IS_ATOM_INT(_jx_7445)) {
            _4111 = NewDouble((double)_jx_7445 * DBL_PTR(_jx_7445)->dbl);
        }
        else {
            if (IS_ATOM_INT(_jx_7445)) {
                _4111 = NewDouble(DBL_PTR(_jx_7445)->dbl * (double)_jx_7445);
            }
            else
            _4111 = NewDouble(DBL_PTR(_jx_7445)->dbl * DBL_PTR(_jx_7445)->dbl);
        }
    }
    if (IS_ATOM_INT(_4111)) {
        _4112 = (_4111 % _size_7438) ? NewDouble((double)_4111 / _size_7438) : (_4111 / _size_7438);
    }
    else {
        _4112 = NewDouble(DBL_PTR(_4111)->dbl / (double)_size_7438);
    }
    DeRef(_4111);
    _4111 = NOVALUE;
    _4113 = calc_hash(_4112, -5);
    DeRef(_4112);
    _4112 = NOVALUE;
    if (IS_ATOM_INT(_4113)) {
        _4114 = (_4113 % 8);
    }
    else {
        temp_d.dbl = (double)8;
        _4114 = Dremainder(DBL_PTR(_4113), &temp_d);
    }
    DeRef(_4113);
    _4113 = NOVALUE;
    if (IS_ATOM_INT(_4114)) {
        _4115 = _4114 + 7;
    }
    else {
        _4115 = NewDouble(DBL_PTR(_4114)->dbl + (double)7);
    }
    DeRef(_4114);
    _4114 = NOVALUE;
    DeRefi(_data_7447);
    _data_7447 = Repeat(0, _4115);
    DeRef(_4115);
    _4115 = NOVALUE;

    /** 		while data[1] != -1 with entry do*/
    goto LB; // [278] 344
LC: 
    _2 = (int)SEQ_PTR(_data_7447);
    _4117 = (int)*(((s1_ptr)_2)->base + 1);
    if (_4117 == -1)
    goto LD; // [285] 377

    /** 			jx = hash(jx, data)*/
    _0 = _jx_7445;
    _jx_7445 = calc_hash(_jx_7445, _data_7447);
    DeRef(_0);

    /** 			ix = remainder(jx, size) + 1*/
    if (IS_ATOM_INT(_jx_7445)) {
        _4120 = (_jx_7445 % _size_7438);
    }
    else {
        temp_d.dbl = (double)_size_7438;
        _4120 = Dremainder(DBL_PTR(_jx_7445), &temp_d);
    }
    if (IS_ATOM_INT(_4120)) {
        _ix_7444 = _4120 + 1;
    }
    else
    { // coercing _ix_7444 to an integer 1
        _ix_7444 = 1+(long)(DBL_PTR(_4120)->dbl);
        if( !IS_ATOM_INT(_ix_7444) ){
            _ix_7444 = (object)DBL_PTR(_ix_7444)->dbl;
        }
    }
    DeRef(_4120);
    _4120 = NOVALUE;

    /** 			cs[ix] = xor_bits(cs[ix], hash(data, stdhash:HSIEH32))*/
    _2 = (int)SEQ_PTR(_cs_7442);
    _4122 = (int)*(((s1_ptr)_2)->base + _ix_7444);
    _4123 = calc_hash(_data_7447, -5);
    if (IS_ATOM_INT(_4122) && IS_ATOM_INT(_4123)) {
        {unsigned long tu;
             tu = (unsigned long)_4122 ^ (unsigned long)_4123;
             _4124 = MAKE_UINT(tu);
        }
    }
    else {
        _4124 = binary_op(XOR_BITS, _4122, _4123);
    }
    _4122 = NOVALUE;
    DeRef(_4123);
    _4123 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_7442);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_7442 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ix_7444);
    _1 = *(int *)_2;
    *(int *)_2 = _4124;
    if( _1 != _4124 ){
        DeRef(_1);
    }
    _4124 = NOVALUE;

    /** 			hits[ix] += 1*/
    _2 = (int)SEQ_PTR(_hits_7443);
    _4125 = (int)*(((s1_ptr)_2)->base + _ix_7444);
    if (IS_ATOM_INT(_4125)) {
        _4126 = _4125 + 1;
        if (_4126 > MAXINT){
            _4126 = NewDouble((double)_4126);
        }
    }
    else
    _4126 = binary_op(PLUS, 1, _4125);
    _4125 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_7443);
    _2 = (int)(((s1_ptr)_2)->base + _ix_7444);
    _1 = *(int *)_2;
    *(int *)_2 = _4126;
    if( _1 != _4126 ){
        DeRef(_1);
    }
    _4126 = NOVALUE;

    /** 		entry*/
LB: 

    /** 			for i = 1 to length(data) do*/
    if (IS_SEQUENCE(_data_7447)){
            _4127 = SEQ_PTR(_data_7447)->length;
    }
    else {
        _4127 = 1;
    }
    {
        int _i_7502;
        _i_7502 = 1;
LE: 
        if (_i_7502 > _4127){
            goto LF; // [349] 372
        }

        /** 				data[i] = getc(fn)*/
        if (_fn_7441 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_7441, EF_READ);
            last_r_file_no = _fn_7441;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _4128 = getKBchar();
            }
            else
            _4128 = getc(last_r_file_ptr);
        }
        else
        _4128 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_data_7447);
        _2 = (int)(((s1_ptr)_2)->base + _i_7502);
        *(int *)_2 = _4128;
        if( _1 != _4128 ){
        }
        _4128 = NOVALUE;

        /** 			end for*/
        _i_7502 = _i_7502 + 1;
        goto LE; // [367] 356
LF: 
        ;
    }

    /** 		end while*/
    goto LC; // [374] 281
LD: 

    /** 		nhit = 0*/
    _nhit_7448 = 0;

    /** 		while nmiss with entry do*/
    goto L10; // [386] 479
L11: 
    if (_nmiss_7449 == 0)
    {
        goto L12; // [391] 493
    }
    else{
    }

    /** 			while 1 do*/
L13: 

    /** 				nhit += 1*/
    _nhit_7448 = _nhit_7448 + 1;

    /** 				if nhit > length(hits) then*/
    if (IS_SEQUENCE(_hits_7443)){
            _4130 = SEQ_PTR(_hits_7443)->length;
    }
    else {
        _4130 = 1;
    }
    if (_nhit_7448 <= _4130)
    goto L14; // [412] 424

    /** 					nhit = 1*/
    _nhit_7448 = 1;
L14: 

    /** 				if hits[nhit] != 0 then*/
    _2 = (int)SEQ_PTR(_hits_7443);
    _4132 = (int)*(((s1_ptr)_2)->base + _nhit_7448);
    if (binary_op_a(EQUALS, _4132, 0)){
        _4132 = NOVALUE;
        goto L13; // [430] 399
    }
    _4132 = NOVALUE;

    /** 					exit*/
    goto L15; // [436] 444

    /** 			end while*/
    goto L13; // [441] 399
L15: 

    /** 			cs[nmiss] = hash(cs[nmiss], cs[nhit])*/
    _2 = (int)SEQ_PTR(_cs_7442);
    _4134 = (int)*(((s1_ptr)_2)->base + _nmiss_7449);
    _2 = (int)SEQ_PTR(_cs_7442);
    _4135 = (int)*(((s1_ptr)_2)->base + _nhit_7448);
    _4136 = calc_hash(_4134, _4135);
    _4134 = NOVALUE;
    _4135 = NOVALUE;
    _2 = (int)SEQ_PTR(_cs_7442);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cs_7442 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_7449);
    _1 = *(int *)_2;
    *(int *)_2 = _4136;
    if( _1 != _4136 ){
        DeRef(_1);
    }
    _4136 = NOVALUE;

    /** 			hits[nmiss] += 1*/
    _2 = (int)SEQ_PTR(_hits_7443);
    _4137 = (int)*(((s1_ptr)_2)->base + _nmiss_7449);
    if (IS_ATOM_INT(_4137)) {
        _4138 = _4137 + 1;
        if (_4138 > MAXINT){
            _4138 = NewDouble((double)_4138);
        }
    }
    else
    _4138 = binary_op(PLUS, 1, _4137);
    _4137 = NOVALUE;
    _2 = (int)SEQ_PTR(_hits_7443);
    _2 = (int)(((s1_ptr)_2)->base + _nmiss_7449);
    _1 = *(int *)_2;
    *(int *)_2 = _4138;
    if( _1 != _4138 ){
        DeRef(_1);
    }
    _4138 = NOVALUE;

    /** 		entry*/
L10: 

    /** 			nmiss = find(0, hits)	*/
    _nmiss_7449 = find_from(0, _hits_7443, 1);

    /** 		end while*/
    goto L11; // [490] 389
L12: 
LA: 

    /** 	close(fn)*/
    EClose(_fn_7441);

    /** 	if return_text then*/
    if (_return_text_7440 == 0)
    {
        goto L16; // [500] 571
    }
    else{
    }

    /** 		sequence cs_text = ""*/
    RefDS(_5);
    DeRef(_cs_text_7521);
    _cs_text_7521 = _5;

    /** 		for i = 1 to length(cs) do*/
    if (IS_SEQUENCE(_cs_7442)){
            _4140 = SEQ_PTR(_cs_7442)->length;
    }
    else {
        _4140 = 1;
    }
    {
        int _i_7523;
        _i_7523 = 1;
L17: 
        if (_i_7523 > _4140){
            goto L18; // [515] 560
        }

        /** 			cs_text &= text:format("[:08X]", cs[i])*/
        _2 = (int)SEQ_PTR(_cs_7442);
        _4142 = (int)*(((s1_ptr)_2)->base + _i_7523);
        RefDS(_4141);
        Ref(_4142);
        _4143 = _12format(_4141, _4142);
        _4142 = NOVALUE;
        if (IS_SEQUENCE(_cs_text_7521) && IS_ATOM(_4143)) {
            Ref(_4143);
            Append(&_cs_text_7521, _cs_text_7521, _4143);
        }
        else if (IS_ATOM(_cs_text_7521) && IS_SEQUENCE(_4143)) {
        }
        else {
            Concat((object_ptr)&_cs_text_7521, _cs_text_7521, _4143);
        }
        DeRef(_4143);
        _4143 = NOVALUE;

        /** 			if i != length(cs) then*/
        if (IS_SEQUENCE(_cs_7442)){
                _4145 = SEQ_PTR(_cs_7442)->length;
        }
        else {
            _4145 = 1;
        }
        if (_i_7523 == _4145)
        goto L19; // [542] 553

        /** 				cs_text &= ' '*/
        Append(&_cs_text_7521, _cs_text_7521, 32);
L19: 

        /** 		end for*/
        _i_7523 = _i_7523 + 1;
        goto L17; // [555] 522
L18: 
        ;
    }

    /** 		return cs_text*/
    DeRefDS(_filename_7437);
    DeRef(_cs_7442);
    DeRef(_hits_7443);
    DeRef(_jx_7445);
    DeRef(_fx_7446);
    DeRefi(_data_7447);
    _4117 = NOVALUE;
    return _cs_text_7521;
    DeRefDS(_cs_text_7521);
    _cs_text_7521 = NOVALUE;
    goto L1A; // [568] 578
L16: 

    /** 		return cs*/
    DeRefDS(_filename_7437);
    DeRef(_hits_7443);
    DeRef(_jx_7445);
    DeRef(_fx_7446);
    DeRefi(_data_7447);
    _4117 = NOVALUE;
    return _cs_7442;
L1A: 
    ;
}



// 0xCC25A4C9
