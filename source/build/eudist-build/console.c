// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _3has_console()
{
    int _7531 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_HAS_CONSOLE, 0)*/
    _7531 = machine(99, 0);
    return _7531;
    ;
}


int _3key_codes(int _codes_13205)
{
    int _7532 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_KEY_CODES, codes)*/
    _7532 = machine(100, _codes_13205);
    DeRef(_codes_13205);
    return _7532;
    ;
}


int _3set_keycodes(int _kcfile_13530)
{
    int _m_13531 = NOVALUE;
    int _kcv_13532 = NOVALUE;
    int _kc_13533 = NOVALUE;
    int _keyname_13534 = NOVALUE;
    int _keycode_13535 = NOVALUE;
    int _key_codes_inlined_key_codes_at_34_13543 = NOVALUE;
    int _key_codes_inlined_key_codes_at_2015_13829 = NOVALUE;
    int _7728 = NOVALUE;
    int _7726 = NOVALUE;
    int _7725 = NOVALUE;
    int _7724 = NOVALUE;
    int _7723 = NOVALUE;
    int _7722 = NOVALUE;
    int _7721 = NOVALUE;
    int _7718 = NOVALUE;
    int _0, _1, _2;
    

    /** 	m = map:load_map(kcfile)*/
    Ref(_kcfile_13530);
    _0 = _m_13531;
    _m_13531 = _26load_map(_kcfile_13530);
    DeRef(_0);

    /** 	if not map(m) then*/
    Ref(_m_13531);
    _7718 = _26map(_m_13531);
    if (IS_ATOM_INT(_7718)) {
        if (_7718 != 0){
            DeRef(_7718);
            _7718 = NOVALUE;
            goto L1; // [13] 23
        }
    }
    else {
        if (DBL_PTR(_7718)->dbl != 0.0){
            DeRef(_7718);
            _7718 = NOVALUE;
            goto L1; // [13] 23
        }
    }
    DeRef(_7718);
    _7718 = NOVALUE;

    /** 		return -1 -- The file could not be loaded intoa map.*/
    DeRef(_kcfile_13530);
    DeRef(_m_13531);
    DeRef(_kcv_13532);
    DeRef(_kc_13533);
    DeRef(_keyname_13534);
    return -1;
L1: 

    /** 	kcv = map:pairs(m)*/
    Ref(_m_13531);
    _0 = _kcv_13532;
    _kcv_13532 = _26pairs(_m_13531, 0);
    DeRef(_0);

    /** 	kc = key_codes(0)*/

    /** 	return machine_func(M_KEY_CODES, codes)*/
    DeRef(_kc_13533);
    _kc_13533 = machine(100, 0);

    /** 	for i = 1 to length(kcv) do*/
    if (IS_SEQUENCE(_kcv_13532)){
            _7721 = SEQ_PTR(_kcv_13532)->length;
    }
    else {
        _7721 = 1;
    }
    {
        int _i_13545;
        _i_13545 = 1;
L2: 
        if (_i_13545 > _7721){
            goto L3; // [48] 2012
        }

        /** 		if integer(kcv[i][2]) then*/
        _2 = (int)SEQ_PTR(_kcv_13532);
        _7722 = (int)*(((s1_ptr)_2)->base + _i_13545);
        _2 = (int)SEQ_PTR(_7722);
        _7723 = (int)*(((s1_ptr)_2)->base + 2);
        _7722 = NOVALUE;
        if (IS_ATOM_INT(_7723))
        _7724 = 1;
        else if (IS_ATOM_DBL(_7723))
        _7724 = IS_ATOM_INT(DoubleToInt(_7723));
        else
        _7724 = 0;
        _7723 = NOVALUE;
        if (_7724 == 0)
        {
            _7724 = NOVALUE;
            goto L4; // [68] 1998
        }
        else{
            _7724 = NOVALUE;
        }

        /** 			keyname = upper(kcv[i][1])*/
        _2 = (int)SEQ_PTR(_kcv_13532);
        _7725 = (int)*(((s1_ptr)_2)->base + _i_13545);
        _2 = (int)SEQ_PTR(_7725);
        _7726 = (int)*(((s1_ptr)_2)->base + 1);
        _7725 = NOVALUE;
        Ref(_7726);
        _0 = _keyname_13534;
        _keyname_13534 = _12upper(_7726);
        DeRef(_0);
        _7726 = NOVALUE;

        /** 			keycode = kcv[i][2]*/
        _2 = (int)SEQ_PTR(_kcv_13532);
        _7728 = (int)*(((s1_ptr)_2)->base + _i_13545);
        _2 = (int)SEQ_PTR(_7728);
        _keycode_13535 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_keycode_13535)){
            _keycode_13535 = (long)DBL_PTR(_keycode_13535)->dbl;
        }
        _7728 = NOVALUE;

        /** 			switch keyname do*/
        _1 = find(_keyname_13534, _7730);
        switch ( _1 ){ 

            /** 				case "LBUTTON" then*/
            case 1:

            /** 					kc[KC_LBUTTON] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [120] 2005

            /** 				case "RBUTTON" then*/
            case 2:

            /** 					kc[KC_RBUTTON] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 3);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [134] 2005

            /** 				case "CANCEL" then*/
            case 3:

            /** 					kc[KC_CANCEL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 4);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [148] 2005

            /** 				case "MBUTTON" then*/
            case 4:

            /** 					kc[KC_MBUTTON] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 5);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [162] 2005

            /** 				case "XBUTTON1" then*/
            case 5:

            /** 					kc[KC_XBUTTON1] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 6);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [176] 2005

            /** 				case "XBUTTON2" then*/
            case 6:

            /** 					kc[KC_XBUTTON2] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 7);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [190] 2005

            /** 				case "BACK" then*/
            case 7:

            /** 					kc[KC_BACK] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 9);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [204] 2005

            /** 				case "TAB" then*/
            case 8:

            /** 					kc[KC_TAB] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 10);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [218] 2005

            /** 				case "CLEAR" then*/
            case 9:

            /** 					kc[KC_CLEAR] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 13);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [232] 2005

            /** 				case "RETURN" then*/
            case 10:

            /** 					kc[KC_RETURN] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 14);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [246] 2005

            /** 				case "SHIFT" then*/
            case 11:

            /** 					kc[KC_SHIFT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 17);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [260] 2005

            /** 				case "CONTROL" then*/
            case 12:

            /** 					kc[KC_CONTROL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 18);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [274] 2005

            /** 				case "MENU" then*/
            case 13:

            /** 					kc[KC_MENU] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 19);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [288] 2005

            /** 				case "PAUSE" then*/
            case 14:

            /** 					kc[KC_PAUSE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 20);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [302] 2005

            /** 				case "CAPITAL" then*/
            case 15:

            /** 					kc[KC_CAPITAL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 21);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [316] 2005

            /** 				case "KANA" then*/
            case 16:

            /** 					kc[KC_KANA] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 22);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [330] 2005

            /** 				case "JUNJA" then*/
            case 17:

            /** 					kc[KC_JUNJA] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 24);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [344] 2005

            /** 				case "FINAL" then*/
            case 18:

            /** 					kc[KC_FINAL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 25);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [358] 2005

            /** 				case "HANJA" then*/
            case 19:

            /** 					kc[KC_HANJA] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 26);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [372] 2005

            /** 				case "ESCAPE" then*/
            case 20:

            /** 					kc[KC_ESCAPE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 28);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [386] 2005

            /** 				case "CONVERT" then*/
            case 21:

            /** 					kc[KC_CONVERT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 29);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [400] 2005

            /** 				case "NONCONVERT" then*/
            case 22:

            /** 					kc[KC_NONCONVERT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 30);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [414] 2005

            /** 				case "ACCEPT" then*/
            case 23:

            /** 					kc[KC_ACCEPT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 31);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [428] 2005

            /** 				case "MODECHANGE" then*/
            case 24:

            /** 					kc[KC_MODECHANGE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 32);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [442] 2005

            /** 				case "SPACE" then*/
            case 25:

            /** 					kc[KC_SPACE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 33);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [456] 2005

            /** 				case "PRIOR" then*/
            case 26:

            /** 					kc[KC_PRIOR] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 34);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [470] 2005

            /** 				case "NEXT" then*/
            case 27:

            /** 					kc[KC_NEXT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 35);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [484] 2005

            /** 				case "END" then*/
            case 28:

            /** 					kc[KC_END] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 36);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [498] 2005

            /** 				case "HOME" then*/
            case 29:

            /** 					kc[KC_HOME] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 37);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [512] 2005

            /** 				case "LEFT" then*/
            case 30:

            /** 					kc[KC_LEFT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 38);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [526] 2005

            /** 				case "UP" then*/
            case 31:

            /** 					kc[KC_UP] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 39);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [540] 2005

            /** 				case "RIGHT" then*/
            case 32:

            /** 					kc[KC_RIGHT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 40);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [554] 2005

            /** 				case "DOWN" then*/
            case 33:

            /** 					kc[KC_DOWN] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 41);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [568] 2005

            /** 				case "SELECT" then*/
            case 34:

            /** 					kc[KC_SELECT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 42);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [582] 2005

            /** 				case "PRINT" then*/
            case 35:

            /** 					kc[KC_PRINT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 43);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [596] 2005

            /** 				case "EXECUTE" then*/
            case 36:

            /** 					kc[KC_EXECUTE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 44);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [610] 2005

            /** 				case "SNAPSHOT" then*/
            case 37:

            /** 					kc[KC_SNAPSHOT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 45);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [624] 2005

            /** 				case "INSERT" then*/
            case 38:

            /** 					kc[KC_INSERT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 46);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [638] 2005

            /** 				case "DELETE" then*/
            case 39:

            /** 					kc[KC_DELETE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 47);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [652] 2005

            /** 				case "HELP" then*/
            case 40:

            /** 					kc[KC_HELP] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 48);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [666] 2005

            /** 				case "LWIN" then*/
            case 41:

            /** 					kc[KC_LWIN] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 92);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [680] 2005

            /** 				case "RWIN" then*/
            case 42:

            /** 					kc[KC_RWIN] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 93);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [694] 2005

            /** 				case "APPS" then*/
            case 43:

            /** 					kc[KC_APPS] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 94);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [708] 2005

            /** 				case "SLEEP" then*/
            case 44:

            /** 					kc[KC_SLEEP] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 96);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [722] 2005

            /** 				case "NUMPAD0" then*/
            case 45:

            /** 					kc[KC_NUMPAD0] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 97);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [736] 2005

            /** 				case "NUMPAD1" then*/
            case 46:

            /** 					kc[KC_NUMPAD1] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 98);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [750] 2005

            /** 				case "NUMPAD2" then*/
            case 47:

            /** 					kc[KC_NUMPAD2] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 99);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [764] 2005

            /** 				case "NUMPAD3" then*/
            case 48:

            /** 					kc[KC_NUMPAD3] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 100);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [778] 2005

            /** 				case "NUMPAD4" then*/
            case 49:

            /** 					kc[KC_NUMPAD4] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 101);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [792] 2005

            /** 				case "NUMPAD5" then*/
            case 50:

            /** 					kc[KC_NUMPAD5] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 102);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [806] 2005

            /** 				case "NUMPAD6" then*/
            case 51:

            /** 					kc[KC_NUMPAD6] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 103);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [820] 2005

            /** 				case "NUMPAD7" then*/
            case 52:

            /** 					kc[KC_NUMPAD7] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 104);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [834] 2005

            /** 				case "NUMPAD8" then*/
            case 53:

            /** 					kc[KC_NUMPAD8] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 105);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [848] 2005

            /** 				case "NUMPAD9" then*/
            case 54:

            /** 					kc[KC_NUMPAD9] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 106);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [862] 2005

            /** 				case "MULTIPLY" then*/
            case 55:

            /** 					kc[KC_MULTIPLY] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 107);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [876] 2005

            /** 				case "ADD" then*/
            case 56:

            /** 					kc[KC_ADD] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 108);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [890] 2005

            /** 				case "SEPARATOR" then*/
            case 57:

            /** 					kc[KC_SEPARATOR] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 109);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [904] 2005

            /** 				case "SUBTRACT" then*/
            case 58:

            /** 					kc[KC_SUBTRACT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 110);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [918] 2005

            /** 				case "DECIMAL" then*/
            case 59:

            /** 					kc[KC_DECIMAL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 111);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [932] 2005

            /** 				case "DIVIDE" then*/
            case 60:

            /** 					kc[KC_DIVIDE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 112);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [946] 2005

            /** 				case "F1" then*/
            case 61:

            /** 					kc[KC_F1] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 113);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [960] 2005

            /** 				case "F2" then*/
            case 62:

            /** 					kc[KC_F2] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 114);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [974] 2005

            /** 				case "F3" then*/
            case 63:

            /** 					kc[KC_F3] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 115);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [988] 2005

            /** 				case "F4" then*/
            case 64:

            /** 					kc[KC_F4] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 116);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1002] 2005

            /** 				case "F5" then*/
            case 65:

            /** 					kc[KC_F5] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 117);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1016] 2005

            /** 				case "F6" then*/
            case 66:

            /** 					kc[KC_F6] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 118);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1030] 2005

            /** 				case "F7" then*/
            case 67:

            /** 					kc[KC_F7] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 119);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1044] 2005

            /** 				case "F8" then*/
            case 68:

            /** 					kc[KC_F8] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 120);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1058] 2005

            /** 				case "F9" then*/
            case 69:

            /** 					kc[KC_F9] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 121);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1072] 2005

            /** 				case "F10" then*/
            case 70:

            /** 					kc[KC_F10] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 122);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1086] 2005

            /** 				case "F11" then*/
            case 71:

            /** 					kc[KC_F11] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 123);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1100] 2005

            /** 				case "F12" then*/
            case 72:

            /** 					kc[KC_F12] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 124);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1114] 2005

            /** 				case "F13" then*/
            case 73:

            /** 					kc[KC_F13] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 125);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1128] 2005

            /** 				case "F14" then*/
            case 74:

            /** 					kc[KC_F14] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 126);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1142] 2005

            /** 				case "F15" then*/
            case 75:

            /** 					kc[KC_F15] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 127);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1156] 2005

            /** 				case "F16" then*/
            case 76:

            /** 					kc[KC_F16] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 128);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1170] 2005

            /** 				case "F17" then*/
            case 77:

            /** 					kc[KC_F17] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 129);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1184] 2005

            /** 				case "F18" then*/
            case 78:

            /** 					kc[KC_F18] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 130);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1198] 2005

            /** 				case "F19" then*/
            case 79:

            /** 					kc[KC_F19] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 131);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1212] 2005

            /** 				case "F20" then*/
            case 80:

            /** 					kc[KC_F20] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 132);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1226] 2005

            /** 				case "F21" then*/
            case 81:

            /** 					kc[KC_F21] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 133);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1240] 2005

            /** 				case "F22" then*/
            case 82:

            /** 					kc[KC_F22] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 134);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1254] 2005

            /** 				case "F23" then*/
            case 83:

            /** 					kc[KC_F23] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 135);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1268] 2005

            /** 				case "F24" then*/
            case 84:

            /** 					kc[KC_F24] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 136);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1282] 2005

            /** 				case "NUMLOCK" then*/
            case 85:

            /** 					kc[KC_NUMLOCK] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 145);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1296] 2005

            /** 				case "SCROLL" then*/
            case 86:

            /** 					kc[KC_SCROLL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 146);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1310] 2005

            /** 				case "LSHIFT" then*/
            case 87:

            /** 					kc[KC_LSHIFT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 161);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1324] 2005

            /** 				case "RSHIFT" then*/
            case 88:

            /** 					kc[KC_RSHIFT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 162);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1338] 2005

            /** 				case "LCONTROL" then*/
            case 89:

            /** 					kc[KC_LCONTROL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 163);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1352] 2005

            /** 				case "RCONTROL" then*/
            case 90:

            /** 					kc[KC_RCONTROL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 164);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1366] 2005

            /** 				case "LMENU" then*/
            case 91:

            /** 					kc[KC_LMENU] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 165);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1380] 2005

            /** 				case "RMENU" then*/
            case 92:

            /** 					kc[KC_RMENU] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 166);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1394] 2005

            /** 				case "BROWSER_BACK" then*/
            case 93:

            /** 					kc[KC_BROWSER_BACK] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 167);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1408] 2005

            /** 				case "BROWSER_FORWARD" then*/
            case 94:

            /** 					kc[KC_BROWSER_FORWARD] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 168);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1422] 2005

            /** 				case "BROWSER_REFRESH" then*/
            case 95:

            /** 					kc[KC_BROWSER_REFRESH] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 169);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1436] 2005

            /** 				case "BROWSER_STOP" then*/
            case 96:

            /** 					kc[KC_BROWSER_STOP] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 170);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1450] 2005

            /** 				case "BROWSER_SEARCH" then*/
            case 97:

            /** 					kc[KC_BROWSER_SEARCH] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 171);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1464] 2005

            /** 				case "BROWSER_FAVORITES" then*/
            case 98:

            /** 					kc[KC_BROWSER_FAVORITES] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 172);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1478] 2005

            /** 				case "BROWSER_HOME" then*/
            case 99:

            /** 					kc[KC_BROWSER_HOME] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 173);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1492] 2005

            /** 				case "VOLUME_MUTE" then*/
            case 100:

            /** 					kc[KC_VOLUME_MUTE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 174);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1506] 2005

            /** 				case "VOLUME_DOWN" then*/
            case 101:

            /** 					kc[KC_VOLUME_DOWN] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 175);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1520] 2005

            /** 				case "VOLUME_UP" then*/
            case 102:

            /** 					kc[KC_VOLUME_UP] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 176);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1534] 2005

            /** 				case "MEDIA_NEXT_TRACK" then*/
            case 103:

            /** 					kc[KC_MEDIA_NEXT_TRACK] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 177);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1548] 2005

            /** 				case "MEDIA_PREV_TRACK" then*/
            case 104:

            /** 					kc[KC_MEDIA_PREV_TRACK] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 178);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1562] 2005

            /** 				case "MEDIA_STOP" then*/
            case 105:

            /** 					kc[KC_MEDIA_STOP] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 179);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1576] 2005

            /** 				case "MEDIA_PLAY_PAUSE" then*/
            case 106:

            /** 					kc[KC_MEDIA_PLAY_PAUSE] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 180);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1590] 2005

            /** 				case "LAUNCH_MAIL" then*/
            case 107:

            /** 					kc[KC_LAUNCH_MAIL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 181);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1604] 2005

            /** 				case "LAUNCH_MEDIA_SELECT" then*/
            case 108:

            /** 					kc[KC_LAUNCH_MEDIA_SELECT] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 182);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1618] 2005

            /** 				case "LAUNCH_APP1" then*/
            case 109:

            /** 					kc[KC_LAUNCH_APP1] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 183);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1632] 2005

            /** 				case "LAUNCH_APP2" then*/
            case 110:

            /** 					kc[KC_LAUNCH_APP2] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 184);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1646] 2005

            /** 				case "OEM_1" then*/
            case 111:

            /** 					kc[KC_OEM_1] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 187);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1660] 2005

            /** 				case "OEM_PLUS" then*/
            case 112:

            /** 					kc[KC_OEM_PLUS] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 188);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1674] 2005

            /** 				case "OEM_COMMA" then*/
            case 113:

            /** 					kc[KC_OEM_COMMA] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 189);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1688] 2005

            /** 				case "OEM_MINUS" then*/
            case 114:

            /** 					kc[KC_OEM_MINUS] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 190);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1702] 2005

            /** 				case "OEM_PERIOD" then*/
            case 115:

            /** 					kc[KC_OEM_PERIOD] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 191);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1716] 2005

            /** 				case "OEM_2" then*/
            case 116:

            /** 					kc[KC_OEM_2] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 192);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1730] 2005

            /** 				case "OEM_3" then*/
            case 117:

            /** 					kc[KC_OEM_3] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 193);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1744] 2005

            /** 				case "OEM_4" then*/
            case 118:

            /** 					kc[KC_OEM_4] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 220);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1758] 2005

            /** 				case "OEM_5" then*/
            case 119:

            /** 					kc[KC_OEM_5] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 221);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1772] 2005

            /** 				case "OEM_6" then*/
            case 120:

            /** 					kc[KC_OEM_6] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 222);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1786] 2005

            /** 				case "OEM_7" then*/
            case 121:

            /** 					kc[KC_OEM_7] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 223);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1800] 2005

            /** 				case "OEM_8" then*/
            case 122:

            /** 					kc[KC_OEM_8] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 224);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1814] 2005

            /** 				case "OEM_102" then*/
            case 123:

            /** 					kc[KC_OEM_102] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 227);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1828] 2005

            /** 				case "PROCESSKEY" then*/
            case 124:

            /** 					kc[KC_PROCESSKEY] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 230);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1842] 2005

            /** 				case "PACKET" then*/
            case 125:

            /** 					kc[KC_PACKET] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 232);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1856] 2005

            /** 				case "ATTN" then*/
            case 126:

            /** 					kc[KC_ATTN] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 247);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1870] 2005

            /** 				case "CRSEL" then*/
            case 127:

            /** 					kc[KC_CRSEL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 248);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1884] 2005

            /** 				case "EXSEL" then*/
            case 128:

            /** 					kc[KC_EXSEL] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 249);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1898] 2005

            /** 				case "EREOF" then*/
            case 129:

            /** 					kc[KC_EREOF] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 250);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1912] 2005

            /** 				case "PLAY" then*/
            case 130:

            /** 					kc[KC_PLAY] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 251);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1926] 2005

            /** 				case "ZOOM" then*/
            case 131:

            /** 					kc[KC_ZOOM] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 252);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1940] 2005

            /** 				case "NONAME" then*/
            case 132:

            /** 					kc[KC_NONAME] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 253);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1954] 2005

            /** 				case "PA1" then*/
            case 133:

            /** 					kc[KC_PA1] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 254);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1968] 2005

            /** 				case "OEM_CLEAR" then*/
            case 134:

            /** 					kc[KC_OEM_CLEAR] = keycode*/
            _2 = (int)SEQ_PTR(_kc_13533);
            _2 = (int)(((s1_ptr)_2)->base + 255);
            _1 = *(int *)_2;
            *(int *)_2 = _keycode_13535;
            DeRef(_1);
            goto L5; // [1982] 2005

            /** 				case else*/
            case 0:

            /** 					return -3 -- Unknown keyname used.*/
            DeRef(_kcfile_13530);
            DeRef(_m_13531);
            DeRef(_kcv_13532);
            DeRef(_kc_13533);
            DeRef(_keyname_13534);
            return -3;
        ;}        goto L5; // [1995] 2005
L4: 

        /** 			return -2 -- New key value is not an integer*/
        DeRef(_kcfile_13530);
        DeRef(_m_13531);
        DeRef(_kcv_13532);
        DeRef(_kc_13533);
        DeRef(_keyname_13534);
        return -2;
L5: 

        /** 	end for*/
        _i_13545 = _i_13545 + 1;
        goto L2; // [2007] 55
L3: 
        ;
    }

    /** 	key_codes(kc)*/

    /** 	return machine_func(M_KEY_CODES, codes)*/
    DeRef(_key_codes_inlined_key_codes_at_2015_13829);
    _key_codes_inlined_key_codes_at_2015_13829 = machine(100, _kc_13533);

    /**   return 0 -- All done okay*/
    DeRef(_kcfile_13530);
    DeRef(_m_13531);
    DeRef(_kcv_13532);
    DeRef(_kc_13533);
    DeRef(_keyname_13534);
    return 0;
    ;
}


void _3allow_break(int _b_13840)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_ALLOW_BREAK, b)*/
    machine(42, _b_13840);

    /** end procedure*/
    DeRef(_b_13840);
    return;
    ;
}


int _3check_break()
{
    int _7868 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_CHECK_BREAK, 0)*/
    _7868 = machine(43, 0);
    return _7868;
    ;
}


int _3wait_key()
{
    int _7869 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_WAIT_KEY, 0)*/
    _7869 = machine(26, 0);
    return _7869;
    ;
}


void _3any_key(int _prompt_13849, int _con_13851)
{
    int _wait_key_inlined_wait_key_at_31_13856 = NOVALUE;
    int _7871 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_con_13851)) {
        _1 = (long)(DBL_PTR(_con_13851)->dbl);
        if (UNIQUE(DBL_PTR(_con_13851)) && (DBL_PTR(_con_13851)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_con_13851);
        _con_13851 = _1;
    }

    /** 	if not find(con, {1,2}) then*/
    _7871 = find_from(_con_13851, _7356, 1);
    if (_7871 != 0)
    goto L1; // [14] 25
    _7871 = NOVALUE;

    /** 		con = 1*/
    _con_13851 = 1;
L1: 

    /** 	puts(con, prompt)*/
    EPuts(_con_13851, _prompt_13849); // DJP 

    /** 	wait_key()*/

    /** 	return machine_func(M_WAIT_KEY, 0)*/
    _wait_key_inlined_wait_key_at_31_13856 = machine(26, 0);

    /** 	puts(con, "\n")*/
    EPuts(_con_13851, _3169); // DJP 

    /** end procedure*/
    DeRefDS(_prompt_13849);
    return;
    ;
}


void _3maybe_any_key(int _prompt_13859, int _con_13860)
{
    int _has_console_inlined_has_console_at_8_13863 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_con_13860)) {
        _1 = (long)(DBL_PTR(_con_13860)->dbl);
        if (UNIQUE(DBL_PTR(_con_13860)) && (DBL_PTR(_con_13860)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_con_13860);
        _con_13860 = _1;
    }

    /** 	if not has_console() then*/

    /** 	return machine_func(M_HAS_CONSOLE, 0)*/
    DeRef(_has_console_inlined_has_console_at_8_13863);
    _has_console_inlined_has_console_at_8_13863 = machine(99, 0);
    if (IS_ATOM_INT(_has_console_inlined_has_console_at_8_13863)) {
        if (_has_console_inlined_has_console_at_8_13863 != 0){
            goto L1; // [16] 26
        }
    }
    else {
        if (DBL_PTR(_has_console_inlined_has_console_at_8_13863)->dbl != 0.0){
            goto L1; // [16] 26
        }
    }

    /** 		any_key(prompt, con)*/
    RefDS(_prompt_13859);
    _3any_key(_prompt_13859, _con_13860);
L1: 

    /** end procedure*/
    DeRefDS(_prompt_13859);
    return;
    ;
}


int _3prompt_number(int _prompt_13867, int _range_13868)
{
    int _answer_13869 = NOVALUE;
    int _value_inlined_value_at_28_13873 = NOVALUE;
    int _7892 = NOVALUE;
    int _7890 = NOVALUE;
    int _7889 = NOVALUE;
    int _7888 = NOVALUE;
    int _7887 = NOVALUE;
    int _7886 = NOVALUE;
    int _7885 = NOVALUE;
    int _7884 = NOVALUE;
    int _7883 = NOVALUE;
    int _7881 = NOVALUE;
    int _7879 = NOVALUE;
    int _7878 = NOVALUE;
    int _7876 = NOVALUE;
    int _7875 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while 1 do*/
L1: 

    /** 		 puts(1, prompt)*/
    EPuts(1, _prompt_13867); // DJP 

    /** 		 answer = gets(0) -- make sure whole line is read*/
    DeRef(_answer_13869);
    _answer_13869 = EGets(0);

    /** 		 puts(1, '\n')*/
    EPuts(1, 10); // DJP 

    /** 		 answer = stdget:value(answer)*/
    if (!IS_ATOM_INT(_4GET_SHORT_ANSWER_10744)) {
        _1 = (long)(DBL_PTR(_4GET_SHORT_ANSWER_10744)->dbl);
        if (UNIQUE(DBL_PTR(_4GET_SHORT_ANSWER_10744)) && (DBL_PTR(_4GET_SHORT_ANSWER_10744)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4GET_SHORT_ANSWER_10744);
        _4GET_SHORT_ANSWER_10744 = _1;
    }
    if (!IS_ATOM_INT(_4GET_SHORT_ANSWER_10744)) {
        _1 = (long)(DBL_PTR(_4GET_SHORT_ANSWER_10744)->dbl);
        if (UNIQUE(DBL_PTR(_4GET_SHORT_ANSWER_10744)) && (DBL_PTR(_4GET_SHORT_ANSWER_10744)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4GET_SHORT_ANSWER_10744);
        _4GET_SHORT_ANSWER_10744 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_answer_13869);
    _0 = _answer_13869;
    _answer_13869 = _4get_value(_answer_13869, 1, _4GET_SHORT_ANSWER_10744);
    DeRefi(_0);

    /** 		 if answer[1] != stdget:GET_SUCCESS or sequence(answer[2]) then*/
    _2 = (int)SEQ_PTR(_answer_13869);
    _7875 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_7875)) {
        _7876 = (_7875 != 0);
    }
    else {
        _7876 = binary_op(NOTEQ, _7875, 0);
    }
    _7875 = NOVALUE;
    if (IS_ATOM_INT(_7876)) {
        if (_7876 != 0) {
            goto L2; // [54] 70
        }
    }
    else {
        if (DBL_PTR(_7876)->dbl != 0.0) {
            goto L2; // [54] 70
        }
    }
    _2 = (int)SEQ_PTR(_answer_13869);
    _7878 = (int)*(((s1_ptr)_2)->base + 2);
    _7879 = IS_SEQUENCE(_7878);
    _7878 = NOVALUE;
    if (_7879 == 0)
    {
        _7879 = NOVALUE;
        goto L3; // [66] 78
    }
    else{
        _7879 = NOVALUE;
    }
L2: 

    /** 			  puts(1, "A number is expected - try again\n")*/
    EPuts(1, _7880); // DJP 
    goto L1; // [75] 10
L3: 

    /** 			 if length(range) = 2 then*/
    if (IS_SEQUENCE(_range_13868)){
            _7881 = SEQ_PTR(_range_13868)->length;
    }
    else {
        _7881 = 1;
    }
    if (_7881 != 2)
    goto L4; // [83] 144

    /** 				  if range[1] <= answer[2] and answer[2] <= range[2] then*/
    _2 = (int)SEQ_PTR(_range_13868);
    _7883 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_answer_13869);
    _7884 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7883) && IS_ATOM_INT(_7884)) {
        _7885 = (_7883 <= _7884);
    }
    else {
        _7885 = binary_op(LESSEQ, _7883, _7884);
    }
    _7883 = NOVALUE;
    _7884 = NOVALUE;
    if (IS_ATOM_INT(_7885)) {
        if (_7885 == 0) {
            goto L5; // [101] 134
        }
    }
    else {
        if (DBL_PTR(_7885)->dbl == 0.0) {
            goto L5; // [101] 134
        }
    }
    _2 = (int)SEQ_PTR(_answer_13869);
    _7887 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_range_13868);
    _7888 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7887) && IS_ATOM_INT(_7888)) {
        _7889 = (_7887 <= _7888);
    }
    else {
        _7889 = binary_op(LESSEQ, _7887, _7888);
    }
    _7887 = NOVALUE;
    _7888 = NOVALUE;
    if (_7889 == 0) {
        DeRef(_7889);
        _7889 = NOVALUE;
        goto L5; // [118] 134
    }
    else {
        if (!IS_ATOM_INT(_7889) && DBL_PTR(_7889)->dbl == 0.0){
            DeRef(_7889);
            _7889 = NOVALUE;
            goto L5; // [118] 134
        }
        DeRef(_7889);
        _7889 = NOVALUE;
    }
    DeRef(_7889);
    _7889 = NOVALUE;

    /** 					  return answer[2]*/
    _2 = (int)SEQ_PTR(_answer_13869);
    _7890 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_7890);
    DeRefDS(_prompt_13867);
    DeRefDS(_range_13868);
    DeRef(_answer_13869);
    DeRef(_7876);
    _7876 = NOVALUE;
    DeRef(_7885);
    _7885 = NOVALUE;
    return _7890;
    goto L1; // [131] 10
L5: 

    /** 					printf(1, "A number from %g to %g is expected here - try again\n", range)*/
    EPrintf(1, _7891, _range_13868);
    goto L1; // [141] 10
L4: 

    /** 				  return answer[2]*/
    _2 = (int)SEQ_PTR(_answer_13869);
    _7892 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_7892);
    DeRefDS(_prompt_13867);
    DeRefDS(_range_13868);
    DeRef(_answer_13869);
    _7890 = NOVALUE;
    DeRef(_7876);
    _7876 = NOVALUE;
    DeRef(_7885);
    _7885 = NOVALUE;
    return _7892;

    /** 	end while*/
    goto L1; // [158] 10
    ;
}


int _3prompt_string(int _prompt_13900)
{
    int _answer_13901 = NOVALUE;
    int _7900 = NOVALUE;
    int _7899 = NOVALUE;
    int _7898 = NOVALUE;
    int _7897 = NOVALUE;
    int _7896 = NOVALUE;
    int _7895 = NOVALUE;
    int _7894 = NOVALUE;
    int _0, _1, _2;
    

    /** 	puts(1, prompt)*/
    EPuts(1, _prompt_13900); // DJP 

    /** 	answer = gets(0)*/
    DeRefi(_answer_13901);
    _answer_13901 = EGets(0);

    /** 	puts(1, '\n')*/
    EPuts(1, 10); // DJP 

    /** 	if sequence(answer) and length(answer) > 0 then*/
    _7894 = IS_SEQUENCE(_answer_13901);
    if (_7894 == 0) {
        goto L1; // [23] 59
    }
    if (IS_SEQUENCE(_answer_13901)){
            _7896 = SEQ_PTR(_answer_13901)->length;
    }
    else {
        _7896 = 1;
    }
    _7897 = (_7896 > 0);
    _7896 = NOVALUE;
    if (_7897 == 0)
    {
        DeRef(_7897);
        _7897 = NOVALUE;
        goto L1; // [35] 59
    }
    else{
        DeRef(_7897);
        _7897 = NOVALUE;
    }

    /** 		return answer[1..$-1] -- trim the \n*/
    if (IS_SEQUENCE(_answer_13901)){
            _7898 = SEQ_PTR(_answer_13901)->length;
    }
    else {
        _7898 = 1;
    }
    _7899 = _7898 - 1;
    _7898 = NOVALUE;
    rhs_slice_target = (object_ptr)&_7900;
    RHS_Slice(_answer_13901, 1, _7899);
    DeRefDS(_prompt_13900);
    DeRefi(_answer_13901);
    _7899 = NOVALUE;
    return _7900;
    goto L2; // [56] 66
L1: 

    /** 		return ""*/
    RefDS(_5);
    DeRefDS(_prompt_13900);
    DeRefi(_answer_13901);
    DeRef(_7899);
    _7899 = NOVALUE;
    DeRef(_7900);
    _7900 = NOVALUE;
    return _5;
L2: 
    ;
}


int _3positive_int(int _x_13937)
{
    int _7920 = NOVALUE;
    int _7919 = NOVALUE;
    int _7918 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) and x >= 1 then*/
    if (IS_ATOM_INT(_x_13937))
    _7918 = 1;
    else if (IS_ATOM_DBL(_x_13937))
    _7918 = IS_ATOM_INT(DoubleToInt(_x_13937));
    else
    _7918 = 0;
    if (_7918 == 0) {
        goto L1; // [6] 27
    }
    if (IS_ATOM_INT(_x_13937)) {
        _7920 = (_x_13937 >= 1);
    }
    else {
        _7920 = binary_op(GREATEREQ, _x_13937, 1);
    }
    if (_7920 == 0) {
        DeRef(_7920);
        _7920 = NOVALUE;
        goto L1; // [15] 27
    }
    else {
        if (!IS_ATOM_INT(_7920) && DBL_PTR(_7920)->dbl == 0.0){
            DeRef(_7920);
            _7920 = NOVALUE;
            goto L1; // [15] 27
        }
        DeRef(_7920);
        _7920 = NOVALUE;
    }
    DeRef(_7920);
    _7920 = NOVALUE;

    /** 		return 1*/
    DeRef(_x_13937);
    return 1;
    goto L2; // [24] 34
L1: 

    /** 		return 0*/
    DeRef(_x_13937);
    return 0;
L2: 
    ;
}


int _3get_screen_char(int _line_13945, int _column_13946, int _fgbg_13947)
{
    int _ca_13948 = NOVALUE;
    int _7928 = NOVALUE;
    int _7927 = NOVALUE;
    int _7926 = NOVALUE;
    int _7925 = NOVALUE;
    int _7924 = NOVALUE;
    int _7923 = NOVALUE;
    int _7921 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fgbg_13947)) {
        _1 = (long)(DBL_PTR(_fgbg_13947)->dbl);
        if (UNIQUE(DBL_PTR(_fgbg_13947)) && (DBL_PTR(_fgbg_13947)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fgbg_13947);
        _fgbg_13947 = _1;
    }

    /** 	ca = machine_func(M_GET_SCREEN_CHAR, {line, column})*/
    Ref(_column_13946);
    Ref(_line_13945);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _line_13945;
    ((int *)_2)[2] = _column_13946;
    _7921 = MAKE_SEQ(_1);
    DeRef(_ca_13948);
    _ca_13948 = machine(58, _7921);
    DeRefDS(_7921);
    _7921 = NOVALUE;

    /** 	if fgbg then*/
    if (_fgbg_13947 == 0)
    {
        goto L1; // [19] 53
    }
    else{
    }

    /** 		ca = ca[1] & and_bits({ca[2], ca[2]/16}, 0x0F)*/
    _2 = (int)SEQ_PTR(_ca_13948);
    _7923 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_ca_13948);
    _7924 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_ca_13948);
    _7925 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7925)) {
        _7926 = (_7925 % 16) ? NewDouble((double)_7925 / 16) : (_7925 / 16);
    }
    else {
        _7926 = binary_op(DIVIDE, _7925, 16);
    }
    _7925 = NOVALUE;
    Ref(_7924);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _7924;
    ((int *)_2)[2] = _7926;
    _7927 = MAKE_SEQ(_1);
    _7926 = NOVALUE;
    _7924 = NOVALUE;
    _7928 = binary_op(AND_BITS, _7927, 15);
    DeRefDS(_7927);
    _7927 = NOVALUE;
    if (IS_SEQUENCE(_7923) && IS_ATOM(_7928)) {
    }
    else if (IS_ATOM(_7923) && IS_SEQUENCE(_7928)) {
        Ref(_7923);
        Prepend(&_ca_13948, _7928, _7923);
    }
    else {
        Concat((object_ptr)&_ca_13948, _7923, _7928);
        _7923 = NOVALUE;
    }
    _7923 = NOVALUE;
    DeRefDS(_7928);
    _7928 = NOVALUE;
L1: 

    /** 	return ca*/
    DeRef(_line_13945);
    DeRef(_column_13946);
    return _ca_13948;
    ;
}


void _3put_screen_char(int _line_13961, int _column_13962, int _char_attr_13963)
{
    int _7930 = NOVALUE;
    int _0, _1, _2;
    

    /** 	machine_proc(M_PUT_SCREEN_CHAR, {line, column, char_attr})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_line_13961);
    *((int *)(_2+4)) = _line_13961;
    Ref(_column_13962);
    *((int *)(_2+8)) = _column_13962;
    RefDS(_char_attr_13963);
    *((int *)(_2+12)) = _char_attr_13963;
    _7930 = MAKE_SEQ(_1);
    machine(59, _7930);
    DeRefDS(_7930);
    _7930 = NOVALUE;

    /** end procedure*/
    DeRef(_line_13961);
    DeRef(_column_13962);
    DeRefDS(_char_attr_13963);
    return;
    ;
}


int _3attr_to_colors(int _attr_code_13967)
{
    int _fgbg_13968 = NOVALUE;
    int _7940 = NOVALUE;
    int _7939 = NOVALUE;
    int _7938 = NOVALUE;
    int _7937 = NOVALUE;
    int _7936 = NOVALUE;
    int _7935 = NOVALUE;
    int _7934 = NOVALUE;
    int _7932 = NOVALUE;
    int _7931 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_attr_code_13967)) {
        _1 = (long)(DBL_PTR(_attr_code_13967)->dbl);
        if (UNIQUE(DBL_PTR(_attr_code_13967)) && (DBL_PTR(_attr_code_13967)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_attr_code_13967);
        _attr_code_13967 = _1;
    }

    /**     sequence fgbg = and_bits({attr_code, attr_code/16}, 0x0F)*/
    _7931 = (_attr_code_13967 % 16) ? NewDouble((double)_attr_code_13967 / 16) : (_attr_code_13967 / 16);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _attr_code_13967;
    ((int *)_2)[2] = _7931;
    _7932 = MAKE_SEQ(_1);
    _7931 = NOVALUE;
    DeRef(_fgbg_13968);
    _fgbg_13968 = binary_op(AND_BITS, _7932, 15);
    DeRefDS(_7932);
    _7932 = NOVALUE;

    /**     return {find(fgbg[1],true_fgcolor)-1, find(fgbg[2],true_bgcolor)-1}*/
    _2 = (int)SEQ_PTR(_fgbg_13968);
    _7934 = (int)*(((s1_ptr)_2)->base + 1);
    _7935 = find_from(_7934, _31true_fgcolor_13145, 1);
    _7934 = NOVALUE;
    _7936 = _7935 - 1;
    _7935 = NOVALUE;
    _2 = (int)SEQ_PTR(_fgbg_13968);
    _7937 = (int)*(((s1_ptr)_2)->base + 2);
    _7938 = find_from(_7937, _31true_bgcolor_13151, 1);
    _7937 = NOVALUE;
    _7939 = _7938 - 1;
    _7938 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _7936;
    ((int *)_2)[2] = _7939;
    _7940 = MAKE_SEQ(_1);
    _7939 = NOVALUE;
    _7936 = NOVALUE;
    DeRefDS(_fgbg_13968);
    return _7940;
    ;
}


int _3colors_to_attr(int _fgbg_13983, int _bg_13984)
{
    int _7955 = NOVALUE;
    int _7954 = NOVALUE;
    int _7953 = NOVALUE;
    int _7952 = NOVALUE;
    int _7951 = NOVALUE;
    int _7950 = NOVALUE;
    int _7949 = NOVALUE;
    int _7948 = NOVALUE;
    int _7947 = NOVALUE;
    int _7946 = NOVALUE;
    int _7945 = NOVALUE;
    int _7944 = NOVALUE;
    int _7943 = NOVALUE;
    int _7942 = NOVALUE;
    int _7941 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_bg_13984)) {
        _1 = (long)(DBL_PTR(_bg_13984)->dbl);
        if (UNIQUE(DBL_PTR(_bg_13984)) && (DBL_PTR(_bg_13984)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bg_13984);
        _bg_13984 = _1;
    }

    /** 	if sequence(fgbg) then*/
    _7941 = IS_SEQUENCE(_fgbg_13983);
    if (_7941 == 0)
    {
        _7941 = NOVALUE;
        goto L1; // [10] 58
    }
    else{
        _7941 = NOVALUE;
    }

    /**                 return true_fgcolor[fgbg[1]+1] + true_bgcolor[fgbg[2]+1] * 16*/
    _2 = (int)SEQ_PTR(_fgbg_13983);
    _7942 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_7942)) {
        _7943 = _7942 + 1;
    }
    else
    _7943 = binary_op(PLUS, 1, _7942);
    _7942 = NOVALUE;
    _2 = (int)SEQ_PTR(_31true_fgcolor_13145);
    if (!IS_ATOM_INT(_7943)){
        _7944 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_7943)->dbl));
    }
    else{
        _7944 = (int)*(((s1_ptr)_2)->base + _7943);
    }
    _2 = (int)SEQ_PTR(_fgbg_13983);
    _7945 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7945)) {
        _7946 = _7945 + 1;
    }
    else
    _7946 = binary_op(PLUS, 1, _7945);
    _7945 = NOVALUE;
    _2 = (int)SEQ_PTR(_31true_bgcolor_13151);
    if (!IS_ATOM_INT(_7946)){
        _7947 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_7946)->dbl));
    }
    else{
        _7947 = (int)*(((s1_ptr)_2)->base + _7946);
    }
    if (_7947 == (short)_7947)
    _7948 = _7947 * 16;
    else
    _7948 = NewDouble(_7947 * (double)16);
    _7947 = NOVALUE;
    if (IS_ATOM_INT(_7948)) {
        _7949 = _7944 + _7948;
        if ((long)((unsigned long)_7949 + (unsigned long)HIGH_BITS) >= 0) 
        _7949 = NewDouble((double)_7949);
    }
    else {
        _7949 = NewDouble((double)_7944 + DBL_PTR(_7948)->dbl);
    }
    _7944 = NOVALUE;
    DeRef(_7948);
    _7948 = NOVALUE;
    DeRef(_fgbg_13983);
    DeRef(_7943);
    _7943 = NOVALUE;
    DeRef(_7946);
    _7946 = NOVALUE;
    return _7949;
    goto L2; // [55] 93
L1: 

    /**                 return true_fgcolor[fgbg+1] + true_bgcolor[bg+1] * 16*/
    if (IS_ATOM_INT(_fgbg_13983)) {
        _7950 = _fgbg_13983 + 1;
    }
    else
    _7950 = binary_op(PLUS, 1, _fgbg_13983);
    _2 = (int)SEQ_PTR(_31true_fgcolor_13145);
    if (!IS_ATOM_INT(_7950)){
        _7951 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_7950)->dbl));
    }
    else{
        _7951 = (int)*(((s1_ptr)_2)->base + _7950);
    }
    _7952 = _bg_13984 + 1;
    _2 = (int)SEQ_PTR(_31true_bgcolor_13151);
    _7953 = (int)*(((s1_ptr)_2)->base + _7952);
    if (_7953 == (short)_7953)
    _7954 = _7953 * 16;
    else
    _7954 = NewDouble(_7953 * (double)16);
    _7953 = NOVALUE;
    if (IS_ATOM_INT(_7954)) {
        _7955 = _7951 + _7954;
        if ((long)((unsigned long)_7955 + (unsigned long)HIGH_BITS) >= 0) 
        _7955 = NewDouble((double)_7955);
    }
    else {
        _7955 = NewDouble((double)_7951 + DBL_PTR(_7954)->dbl);
    }
    _7951 = NOVALUE;
    DeRef(_7954);
    _7954 = NOVALUE;
    DeRef(_fgbg_13983);
    DeRef(_7950);
    _7950 = NOVALUE;
    DeRef(_7943);
    _7943 = NOVALUE;
    DeRef(_7949);
    _7949 = NOVALUE;
    DeRef(_7946);
    _7946 = NOVALUE;
    _7952 = NOVALUE;
    return _7955;
L2: 
    ;
}


void _3display_text_image(int _xy_14008, int _text_14009)
{
    int _extra_col2_14010 = NOVALUE;
    int _extra_lines_14011 = NOVALUE;
    int _vc_14012 = NOVALUE;
    int _one_row_14013 = NOVALUE;
    int _video_config_inlined_video_config_at_6_14015 = NOVALUE;
    int _7984 = NOVALUE;
    int _7983 = NOVALUE;
    int _7982 = NOVALUE;
    int _7981 = NOVALUE;
    int _7980 = NOVALUE;
    int _7976 = NOVALUE;
    int _7974 = NOVALUE;
    int _7972 = NOVALUE;
    int _7971 = NOVALUE;
    int _7970 = NOVALUE;
    int _7969 = NOVALUE;
    int _7965 = NOVALUE;
    int _7963 = NOVALUE;
    int _7962 = NOVALUE;
    int _7961 = NOVALUE;
    int _7960 = NOVALUE;
    int _7959 = NOVALUE;
    int _7957 = NOVALUE;
    int _7956 = NOVALUE;
    int _0, _1, _2;
    

    /** 	vc = graphcst:video_config()*/

    /** 	return machine_func(M_VIDEO_CONFIG, 0)*/
    DeRefi(_vc_14012);
    _vc_14012 = machine(13, 0);

    /** 	if xy[1] < 1 or xy[2] < 1 then*/
    _2 = (int)SEQ_PTR(_xy_14008);
    _7956 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_7956)) {
        _7957 = (_7956 < 1);
    }
    else {
        _7957 = binary_op(LESS, _7956, 1);
    }
    _7956 = NOVALUE;
    if (IS_ATOM_INT(_7957)) {
        if (_7957 != 0) {
            goto L1; // [26] 43
        }
    }
    else {
        if (DBL_PTR(_7957)->dbl != 0.0) {
            goto L1; // [26] 43
        }
    }
    _2 = (int)SEQ_PTR(_xy_14008);
    _7959 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7959)) {
        _7960 = (_7959 < 1);
    }
    else {
        _7960 = binary_op(LESS, _7959, 1);
    }
    _7959 = NOVALUE;
    if (_7960 == 0) {
        DeRef(_7960);
        _7960 = NOVALUE;
        goto L2; // [39] 49
    }
    else {
        if (!IS_ATOM_INT(_7960) && DBL_PTR(_7960)->dbl == 0.0){
            DeRef(_7960);
            _7960 = NOVALUE;
            goto L2; // [39] 49
        }
        DeRef(_7960);
        _7960 = NOVALUE;
    }
    DeRef(_7960);
    _7960 = NOVALUE;
L1: 

    /** 		return -- bad starting point*/
    DeRefDS(_xy_14008);
    DeRefDS(_text_14009);
    DeRefi(_vc_14012);
    DeRef(_one_row_14013);
    DeRef(_7957);
    _7957 = NOVALUE;
    return;
L2: 

    /** 	extra_lines = vc[graphcst:VC_LINES] - xy[1] + 1*/
    _2 = (int)SEQ_PTR(_vc_14012);
    _7961 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_xy_14008);
    _7962 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_7962)) {
        _7963 = _7961 - _7962;
        if ((long)((unsigned long)_7963 +(unsigned long) HIGH_BITS) >= 0){
            _7963 = NewDouble((double)_7963);
        }
    }
    else {
        _7963 = binary_op(MINUS, _7961, _7962);
    }
    _7961 = NOVALUE;
    _7962 = NOVALUE;
    if (IS_ATOM_INT(_7963)) {
        _extra_lines_14011 = _7963 + 1;
    }
    else
    { // coercing _extra_lines_14011 to an integer 1
        _extra_lines_14011 = 1+(long)(DBL_PTR(_7963)->dbl);
        if( !IS_ATOM_INT(_extra_lines_14011) ){
            _extra_lines_14011 = (object)DBL_PTR(_extra_lines_14011)->dbl;
        }
    }
    DeRef(_7963);
    _7963 = NOVALUE;

    /** 	if length(text) > extra_lines then*/
    if (IS_SEQUENCE(_text_14009)){
            _7965 = SEQ_PTR(_text_14009)->length;
    }
    else {
        _7965 = 1;
    }
    if (_7965 <= _extra_lines_14011)
    goto L3; // [76] 100

    /** 		if extra_lines <= 0 then*/
    if (_extra_lines_14011 > 0)
    goto L4; // [82] 92

    /** 			return -- nothing to display*/
    DeRefDS(_xy_14008);
    DeRefDS(_text_14009);
    DeRefDSi(_vc_14012);
    DeRef(_one_row_14013);
    DeRef(_7957);
    _7957 = NOVALUE;
    return;
L4: 

    /** 		text = text[1..extra_lines] -- truncate*/
    rhs_slice_target = (object_ptr)&_text_14009;
    RHS_Slice(_text_14009, 1, _extra_lines_14011);
L3: 

    /** 	extra_col2 = 2 * (vc[graphcst:VC_COLUMNS] - xy[2] + 1)*/
    _2 = (int)SEQ_PTR(_vc_14012);
    _7969 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_xy_14008);
    _7970 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7970)) {
        _7971 = _7969 - _7970;
        if ((long)((unsigned long)_7971 +(unsigned long) HIGH_BITS) >= 0){
            _7971 = NewDouble((double)_7971);
        }
    }
    else {
        _7971 = binary_op(MINUS, _7969, _7970);
    }
    _7969 = NOVALUE;
    _7970 = NOVALUE;
    if (IS_ATOM_INT(_7971)) {
        _7972 = _7971 + 1;
        if (_7972 > MAXINT){
            _7972 = NewDouble((double)_7972);
        }
    }
    else
    _7972 = binary_op(PLUS, 1, _7971);
    DeRef(_7971);
    _7971 = NOVALUE;
    if (IS_ATOM_INT(_7972) && IS_ATOM_INT(_7972)) {
        _extra_col2_14010 = _7972 + _7972;
    }
    else {
        _extra_col2_14010 = binary_op(PLUS, _7972, _7972);
    }
    DeRef(_7972);
    _7972 = NOVALUE;
    _7972 = NOVALUE;
    if (!IS_ATOM_INT(_extra_col2_14010)) {
        _1 = (long)(DBL_PTR(_extra_col2_14010)->dbl);
        if (UNIQUE(DBL_PTR(_extra_col2_14010)) && (DBL_PTR(_extra_col2_14010)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_extra_col2_14010);
        _extra_col2_14010 = _1;
    }

    /** 	for row = 1 to length(text) do*/
    if (IS_SEQUENCE(_text_14009)){
            _7974 = SEQ_PTR(_text_14009)->length;
    }
    else {
        _7974 = 1;
    }
    {
        int _row_14038;
        _row_14038 = 1;
L5: 
        if (_row_14038 > _7974){
            goto L6; // [133] 211
        }

        /** 		one_row = text[row]*/
        DeRef(_one_row_14013);
        _2 = (int)SEQ_PTR(_text_14009);
        _one_row_14013 = (int)*(((s1_ptr)_2)->base + _row_14038);
        Ref(_one_row_14013);

        /** 		if length(one_row) > extra_col2 then*/
        if (IS_SEQUENCE(_one_row_14013)){
                _7976 = SEQ_PTR(_one_row_14013)->length;
        }
        else {
            _7976 = 1;
        }
        if (_7976 <= _extra_col2_14010)
        goto L7; // [153] 177

        /** 			if extra_col2 <= 0 then*/
        if (_extra_col2_14010 > 0)
        goto L8; // [159] 169

        /** 				return -- nothing to display*/
        DeRefDS(_xy_14008);
        DeRefDS(_text_14009);
        DeRefi(_vc_14012);
        DeRefDS(_one_row_14013);
        DeRef(_7957);
        _7957 = NOVALUE;
        return;
L8: 

        /** 			one_row = one_row[1..extra_col2] -- truncate*/
        rhs_slice_target = (object_ptr)&_one_row_14013;
        RHS_Slice(_one_row_14013, 1, _extra_col2_14010);
L7: 

        /** 		machine_proc(M_PUT_SCREEN_CHAR, {xy[1]+row-1, xy[2], one_row})*/
        _2 = (int)SEQ_PTR(_xy_14008);
        _7980 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_7980)) {
            _7981 = _7980 + _row_14038;
            if ((long)((unsigned long)_7981 + (unsigned long)HIGH_BITS) >= 0) 
            _7981 = NewDouble((double)_7981);
        }
        else {
            _7981 = binary_op(PLUS, _7980, _row_14038);
        }
        _7980 = NOVALUE;
        if (IS_ATOM_INT(_7981)) {
            _7982 = _7981 - 1;
            if ((long)((unsigned long)_7982 +(unsigned long) HIGH_BITS) >= 0){
                _7982 = NewDouble((double)_7982);
            }
        }
        else {
            _7982 = binary_op(MINUS, _7981, 1);
        }
        DeRef(_7981);
        _7981 = NOVALUE;
        _2 = (int)SEQ_PTR(_xy_14008);
        _7983 = (int)*(((s1_ptr)_2)->base + 2);
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _7982;
        Ref(_7983);
        *((int *)(_2+8)) = _7983;
        RefDS(_one_row_14013);
        *((int *)(_2+12)) = _one_row_14013;
        _7984 = MAKE_SEQ(_1);
        _7983 = NOVALUE;
        _7982 = NOVALUE;
        machine(59, _7984);
        DeRefDS(_7984);
        _7984 = NOVALUE;

        /** 	end for*/
        _row_14038 = _row_14038 + 1;
        goto L5; // [206] 140
L6: 
        ;
    }

    /** end procedure*/
    DeRefDS(_xy_14008);
    DeRefDS(_text_14009);
    DeRefi(_vc_14012);
    DeRef(_one_row_14013);
    DeRef(_7957);
    _7957 = NOVALUE;
    return;
    ;
}


int _3save_text_image(int _top_left_14054, int _bottom_right_14055)
{
    int _image_14056 = NOVALUE;
    int _row_chars_14057 = NOVALUE;
    int _7990 = NOVALUE;
    int _7989 = NOVALUE;
    int _7988 = NOVALUE;
    int _7987 = NOVALUE;
    int _7986 = NOVALUE;
    int _7985 = NOVALUE;
    int _0, _1, _2;
    

    /** 	image = {}*/
    RefDS(_5);
    DeRef(_image_14056);
    _image_14056 = _5;

    /** 	for row = top_left[1] to bottom_right[1] do*/
    _2 = (int)SEQ_PTR(_top_left_14054);
    _7985 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_bottom_right_14055);
    _7986 = (int)*(((s1_ptr)_2)->base + 1);
    {
        int _row_14059;
        Ref(_7985);
        _row_14059 = _7985;
L1: 
        if (binary_op_a(GREATER, _row_14059, _7986)){
            goto L2; // [22] 87
        }

        /** 		row_chars = {}*/
        RefDS(_5);
        DeRef(_row_chars_14057);
        _row_chars_14057 = _5;

        /** 		for col = top_left[2] to bottom_right[2] do*/
        _2 = (int)SEQ_PTR(_top_left_14054);
        _7987 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_bottom_right_14055);
        _7988 = (int)*(((s1_ptr)_2)->base + 2);
        {
            int _col_14063;
            Ref(_7987);
            _col_14063 = _7987;
L3: 
            if (binary_op_a(GREATER, _col_14063, _7988)){
                goto L4; // [46] 74
            }

            /** 			row_chars &= machine_func(M_GET_SCREEN_CHAR, {row, col})*/
            Ref(_col_14063);
            Ref(_row_14059);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _row_14059;
            ((int *)_2)[2] = _col_14063;
            _7989 = MAKE_SEQ(_1);
            _7990 = machine(58, _7989);
            DeRefDS(_7989);
            _7989 = NOVALUE;
            Concat((object_ptr)&_row_chars_14057, _row_chars_14057, _7990);
            DeRefDS(_7990);
            _7990 = NOVALUE;

            /** 		end for*/
            _0 = _col_14063;
            if (IS_ATOM_INT(_col_14063)) {
                _col_14063 = _col_14063 + 1;
                if ((long)((unsigned long)_col_14063 +(unsigned long) HIGH_BITS) >= 0){
                    _col_14063 = NewDouble((double)_col_14063);
                }
            }
            else {
                _col_14063 = binary_op_a(PLUS, _col_14063, 1);
            }
            DeRef(_0);
            goto L3; // [69] 53
L4: 
            ;
            DeRef(_col_14063);
        }

        /** 		image = append(image, row_chars)*/
        RefDS(_row_chars_14057);
        Append(&_image_14056, _image_14056, _row_chars_14057);

        /** 	end for*/
        _0 = _row_14059;
        if (IS_ATOM_INT(_row_14059)) {
            _row_14059 = _row_14059 + 1;
            if ((long)((unsigned long)_row_14059 +(unsigned long) HIGH_BITS) >= 0){
                _row_14059 = NewDouble((double)_row_14059);
            }
        }
        else {
            _row_14059 = binary_op_a(PLUS, _row_14059, 1);
        }
        DeRef(_0);
        goto L1; // [82] 29
L2: 
        ;
        DeRef(_row_14059);
    }

    /** 	return image*/
    DeRefDS(_top_left_14054);
    DeRefDS(_bottom_right_14055);
    DeRef(_row_chars_14057);
    _7985 = NOVALUE;
    _7986 = NOVALUE;
    _7987 = NOVALUE;
    _7988 = NOVALUE;
    return _image_14056;
    ;
}


int _3text_rows(int _rows_14072)
{
    int _7993 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_TEXTROWS, rows)*/
    _7993 = machine(12, _rows_14072);
    DeRef(_rows_14072);
    return _7993;
    ;
}


void _3cursor(int _style_14076)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_style_14076)) {
        _1 = (long)(DBL_PTR(_style_14076)->dbl);
        if (UNIQUE(DBL_PTR(_style_14076)) && (DBL_PTR(_style_14076)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_style_14076);
        _style_14076 = _1;
    }

    /** 	machine_proc(M_CURSOR, style)*/
    machine(6, _style_14076);

    /** end procedure*/
    return;
    ;
}


void _3free_console()
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_FREE_CONSOLE, 0)*/
    machine(54, 0);

    /** end procedure*/
    return;
    ;
}


void _3display(int _data_in_14081, int _args_14082, int _finalnl_14083)
{
    int _8021 = NOVALUE;
    int _8020 = NOVALUE;
    int _8019 = NOVALUE;
    int _8016 = NOVALUE;
    int _8014 = NOVALUE;
    int _8013 = NOVALUE;
    int _8011 = NOVALUE;
    int _8010 = NOVALUE;
    int _8008 = NOVALUE;
    int _8007 = NOVALUE;
    int _8005 = NOVALUE;
    int _8004 = NOVALUE;
    int _8003 = NOVALUE;
    int _8001 = NOVALUE;
    int _8000 = NOVALUE;
    int _7999 = NOVALUE;
    int _7997 = NOVALUE;
    int _7996 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_finalnl_14083)) {
        _1 = (long)(DBL_PTR(_finalnl_14083)->dbl);
        if (UNIQUE(DBL_PTR(_finalnl_14083)) && (DBL_PTR(_finalnl_14083)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_finalnl_14083);
        _finalnl_14083 = _1;
    }

    /** 	if atom(data_in) then*/
    _7996 = IS_ATOM(_data_in_14081);
    if (_7996 == 0)
    {
        _7996 = NOVALUE;
        goto L1; // [10] 49
    }
    else{
        _7996 = NOVALUE;
    }

    /** 		if integer(data_in) then*/
    if (IS_ATOM_INT(_data_in_14081))
    _7997 = 1;
    else if (IS_ATOM_DBL(_data_in_14081))
    _7997 = IS_ATOM_INT(DoubleToInt(_data_in_14081));
    else
    _7997 = 0;
    if (_7997 == 0)
    {
        _7997 = NOVALUE;
        goto L2; // [18] 30
    }
    else{
        _7997 = NOVALUE;
    }

    /** 			printf(1, "%d", data_in)*/
    EPrintf(1, _952, _data_in_14081);
    goto L3; // [27] 174
L2: 

    /** 			puts(1, text:trim(sprintf("%15.15f", data_in), '0'))*/
    _7999 = EPrintf(-9999999, _7998, _data_in_14081);
    _8000 = _12trim(_7999, 48, 0);
    _7999 = NOVALUE;
    EPuts(1, _8000); // DJP 
    DeRef(_8000);
    _8000 = NOVALUE;
    goto L3; // [46] 174
L1: 

    /** 	elsif length(data_in) > 0 then*/
    if (IS_SEQUENCE(_data_in_14081)){
            _8001 = SEQ_PTR(_data_in_14081)->length;
    }
    else {
        _8001 = 1;
    }
    if (_8001 <= 0)
    goto L4; // [54] 158

    /** 		if types:t_display( data_in ) then*/
    Ref(_data_in_14081);
    _8003 = _11t_display(_data_in_14081);
    if (_8003 == 0) {
        DeRef(_8003);
        _8003 = NOVALUE;
        goto L5; // [64] 115
    }
    else {
        if (!IS_ATOM_INT(_8003) && DBL_PTR(_8003)->dbl == 0.0){
            DeRef(_8003);
            _8003 = NOVALUE;
            goto L5; // [64] 115
        }
        DeRef(_8003);
        _8003 = NOVALUE;
    }
    DeRef(_8003);
    _8003 = NOVALUE;

    /** 			if data_in[$] = '_' then*/
    if (IS_SEQUENCE(_data_in_14081)){
            _8004 = SEQ_PTR(_data_in_14081)->length;
    }
    else {
        _8004 = 1;
    }
    _2 = (int)SEQ_PTR(_data_in_14081);
    _8005 = (int)*(((s1_ptr)_2)->base + _8004);
    if (binary_op_a(NOTEQ, _8005, 95)){
        _8005 = NOVALUE;
        goto L6; // [76] 102
    }
    _8005 = NOVALUE;

    /** 				data_in = data_in[1..$-1]*/
    if (IS_SEQUENCE(_data_in_14081)){
            _8007 = SEQ_PTR(_data_in_14081)->length;
    }
    else {
        _8007 = 1;
    }
    _8008 = _8007 - 1;
    _8007 = NOVALUE;
    rhs_slice_target = (object_ptr)&_data_in_14081;
    RHS_Slice(_data_in_14081, 1, _8008);

    /** 				finalnl = 0*/
    _finalnl_14083 = 0;
L6: 

    /** 			puts(1, text:format(data_in, args))*/
    Ref(_data_in_14081);
    Ref(_args_14082);
    _8010 = _12format(_data_in_14081, _args_14082);
    EPuts(1, _8010); // DJP 
    DeRef(_8010);
    _8010 = NOVALUE;
    goto L3; // [112] 174
L5: 

    /** 			if atom(args) or length(args) = 0 then*/
    _8011 = IS_ATOM(_args_14082);
    if (_8011 != 0) {
        goto L7; // [120] 136
    }
    if (IS_SEQUENCE(_args_14082)){
            _8013 = SEQ_PTR(_args_14082)->length;
    }
    else {
        _8013 = 1;
    }
    _8014 = (_8013 == 0);
    _8013 = NOVALUE;
    if (_8014 == 0)
    {
        DeRef(_8014);
        _8014 = NOVALUE;
        goto L8; // [132] 146
    }
    else{
        DeRef(_8014);
        _8014 = NOVALUE;
    }
L7: 

    /** 				pretty:pretty_print(1, data_in, {2})*/
    Ref(_data_in_14081);
    RefDS(_8015);
    _24pretty_print(1, _data_in_14081, _8015);
    goto L3; // [143] 174
L8: 

    /** 				pretty:pretty_print(1, data_in, args)*/
    Ref(_data_in_14081);
    Ref(_args_14082);
    _24pretty_print(1, _data_in_14081, _args_14082);
    goto L3; // [155] 174
L4: 

    /** 		if equal(args, 2) then*/
    if (_args_14082 == 2)
    _8016 = 1;
    else if (IS_ATOM_INT(_args_14082) && IS_ATOM_INT(2))
    _8016 = 0;
    else
    _8016 = (compare(_args_14082, 2) == 0);
    if (_8016 == 0)
    {
        _8016 = NOVALUE;
        goto L9; // [164] 173
    }
    else{
        _8016 = NOVALUE;
    }

    /** 			puts(1, `""`)*/
    EPuts(1, _8017); // DJP 
L9: 
L3: 

    /** 	if finalnl = 0 then*/
    if (_finalnl_14083 != 0)
    goto LA; // [176] 183
    goto LB; // [180] 210
LA: 

    /** 	elsif finalnl = -918_273_645 and equal(args,0) then*/
    _8019 = (_finalnl_14083 == -918273645);
    if (_8019 == 0) {
        goto LC; // [189] 204
    }
    if (_args_14082 == 0)
    _8021 = 1;
    else if (IS_ATOM_INT(_args_14082) && IS_ATOM_INT(0))
    _8021 = 0;
    else
    _8021 = (compare(_args_14082, 0) == 0);
    if (_8021 == 0)
    {
        _8021 = NOVALUE;
        goto LC; // [198] 204
    }
    else{
        _8021 = NOVALUE;
    }
    goto LB; // [201] 210
LC: 

    /** 		puts(1, '\n')*/
    EPuts(1, 10); // DJP 
LB: 

    /** 	return*/
    DeRef(_data_in_14081);
    DeRef(_args_14082);
    DeRef(_8008);
    _8008 = NOVALUE;
    DeRef(_8019);
    _8019 = NOVALUE;
    return;

    /** end procedure*/
    DeRef(_8008);
    _8008 = NOVALUE;
    DeRef(_8019);
    _8019 = NOVALUE;
    return;
    ;
}



// 0xADAD4C24
