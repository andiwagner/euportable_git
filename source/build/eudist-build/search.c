// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _14find_any(int _needles_1281, int _haystack_1282, int _start_1283)
{
    int _523 = NOVALUE;
    int _522 = NOVALUE;
    int _521 = NOVALUE;
    int _519 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(needles) then*/
    _519 = IS_ATOM(_needles_1281);
    if (_519 == 0)
    {
        _519 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _519 = NOVALUE;
    }

    /** 		needles = {needles}*/
    _0 = _needles_1281;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needles_1281);
    *((int *)(_2+4)) = _needles_1281;
    _needles_1281 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = start to length(haystack) do*/
    if (IS_SEQUENCE(_haystack_1282)){
            _521 = SEQ_PTR(_haystack_1282)->length;
    }
    else {
        _521 = 1;
    }
    {
        int _i_1288;
        _i_1288 = _start_1283;
L2: 
        if (_i_1288 > _521){
            goto L3; // [27] 62
        }

        /** 		if find(haystack[i],needles) then*/
        _2 = (int)SEQ_PTR(_haystack_1282);
        _522 = (int)*(((s1_ptr)_2)->base + _i_1288);
        _523 = find_from(_522, _needles_1281, 1);
        _522 = NOVALUE;
        if (_523 == 0)
        {
            _523 = NOVALUE;
            goto L4; // [45] 55
        }
        else{
            _523 = NOVALUE;
        }

        /** 			return i*/
        DeRef(_needles_1281);
        DeRefDS(_haystack_1282);
        return _i_1288;
L4: 

        /** 	end for*/
        _i_1288 = _i_1288 + 1;
        goto L2; // [57] 34
L3: 
        ;
    }

    /** 	return 0*/
    DeRef(_needles_1281);
    DeRefDS(_haystack_1282);
    return 0;
    ;
}


int _14find_all(int _needle_1322, int _haystack_1323, int _start_1324)
{
    int _kx_1325 = NOVALUE;
    int _538 = NOVALUE;
    int _537 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer kx = 0*/
    _kx_1325 = 0;

    /** 	while start with entry do*/
    goto L1; // [16] 47
L2: 
    if (_start_1324 == 0)
    {
        goto L3; // [19] 61
    }
    else{
    }

    /** 		kx += 1*/
    _kx_1325 = _kx_1325 + 1;

    /** 		haystack[kx] = start*/
    _2 = (int)SEQ_PTR(_haystack_1323);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_1323 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _kx_1325);
    _1 = *(int *)_2;
    *(int *)_2 = _start_1324;
    DeRef(_1);

    /** 		start += 1*/
    _start_1324 = _start_1324 + 1;

    /** 	entry*/
L1: 

    /** 		start = find(needle, haystack, start)*/
    _start_1324 = find_from(_needle_1322, _haystack_1323, _start_1324);

    /** 	end while*/
    goto L2; // [58] 19
L3: 

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _537 = _kx_1325 + 1;
    if (_537 > MAXINT){
        _537 = NewDouble((double)_537);
    }
    if (IS_SEQUENCE(_haystack_1323)){
            _538 = SEQ_PTR(_haystack_1323)->length;
    }
    else {
        _538 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_1323);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_537)) ? _537 : (long)(DBL_PTR(_537)->dbl);
        int stop = (IS_ATOM_INT(_538)) ? _538 : (long)(DBL_PTR(_538)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_1323), start, &_haystack_1323 );
            }
            else Tail(SEQ_PTR(_haystack_1323), stop+1, &_haystack_1323);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_1323), start, &_haystack_1323);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_1323 = Remove_elements(start, stop, (SEQ_PTR(_haystack_1323)->ref == 1));
        }
    }
    DeRef(_537);
    _537 = NOVALUE;
    _538 = NOVALUE;

    /** 	return haystack*/
    return _haystack_1323;
    ;
}


int _14rfind(int _needle_1440, int _haystack_1441, int _start_1442)
{
    int _len_1444 = NOVALUE;
    int _599 = NOVALUE;
    int _598 = NOVALUE;
    int _595 = NOVALUE;
    int _594 = NOVALUE;
    int _592 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer len = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1441)){
            _len_1444 = SEQ_PTR(_haystack_1441)->length;
    }
    else {
        _len_1444 = 1;
    }

    /** 	if start = 0 then start = len end if*/
    if (_start_1442 != 0)
    goto L1; // [16] 26
    _start_1442 = _len_1444;
L1: 

    /** 	if (start > len) or (len + start < 1) then*/
    _592 = (_start_1442 > _len_1444);
    if (_592 != 0) {
        goto L2; // [32] 49
    }
    _594 = _len_1444 + _start_1442;
    if ((long)((unsigned long)_594 + (unsigned long)HIGH_BITS) >= 0) 
    _594 = NewDouble((double)_594);
    if (IS_ATOM_INT(_594)) {
        _595 = (_594 < 1);
    }
    else {
        _595 = (DBL_PTR(_594)->dbl < (double)1);
    }
    DeRef(_594);
    _594 = NOVALUE;
    if (_595 == 0)
    {
        DeRef(_595);
        _595 = NOVALUE;
        goto L3; // [45] 56
    }
    else{
        DeRef(_595);
        _595 = NOVALUE;
    }
L2: 

    /** 		return 0*/
    DeRefDS(_haystack_1441);
    DeRef(_592);
    _592 = NOVALUE;
    return 0;
L3: 

    /** 	if start < 1 then*/
    if (_start_1442 >= 1)
    goto L4; // [58] 71

    /** 		start = len + start*/
    _start_1442 = _len_1444 + _start_1442;
L4: 

    /** 	for i = start to 1 by -1 do*/
    {
        int _i_1457;
        _i_1457 = _start_1442;
L5: 
        if (_i_1457 < 1){
            goto L6; // [73] 107
        }

        /** 		if equal(haystack[i], needle) then*/
        _2 = (int)SEQ_PTR(_haystack_1441);
        _598 = (int)*(((s1_ptr)_2)->base + _i_1457);
        if (_598 == _needle_1440)
        _599 = 1;
        else if (IS_ATOM_INT(_598) && IS_ATOM_INT(_needle_1440))
        _599 = 0;
        else
        _599 = (compare(_598, _needle_1440) == 0);
        _598 = NOVALUE;
        if (_599 == 0)
        {
            _599 = NOVALUE;
            goto L7; // [90] 100
        }
        else{
            _599 = NOVALUE;
        }

        /** 			return i*/
        DeRefDS(_haystack_1441);
        DeRef(_592);
        _592 = NOVALUE;
        return _i_1457;
L7: 

        /** 	end for*/
        _i_1457 = _i_1457 + -1;
        goto L5; // [102] 80
L6: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_haystack_1441);
    DeRef(_592);
    _592 = NOVALUE;
    return 0;
    ;
}


int _14match_replace(int _needle_1477, int _haystack_1478, int _replacement_1479, int _max_1480)
{
    int _posn_1481 = NOVALUE;
    int _needle_len_1482 = NOVALUE;
    int _replacement_len_1483 = NOVALUE;
    int _scan_from_1484 = NOVALUE;
    int _cnt_1485 = NOVALUE;
    int _615 = NOVALUE;
    int _612 = NOVALUE;
    int _610 = NOVALUE;
    int _608 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if max < 0 then*/

    /** 	cnt = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1478)){
            _cnt_1485 = SEQ_PTR(_haystack_1478)->length;
    }
    else {
        _cnt_1485 = 1;
    }

    /** 	if max != 0 then*/

    /** 	if atom(needle) then*/
    _608 = IS_ATOM(_needle_1477);
    if (_608 == 0)
    {
        _608 = NOVALUE;
        goto L1; // [46] 56
    }
    else{
        _608 = NOVALUE;
    }

    /** 		needle = {needle}*/
    _0 = _needle_1477;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needle_1477);
    *((int *)(_2+4)) = _needle_1477;
    _needle_1477 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	if atom(replacement) then*/
    _610 = IS_ATOM(_replacement_1479);
    if (_610 == 0)
    {
        _610 = NOVALUE;
        goto L2; // [61] 71
    }
    else{
        _610 = NOVALUE;
    }

    /** 		replacement = {replacement}*/
    _0 = _replacement_1479;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_replacement_1479);
    *((int *)(_2+4)) = _replacement_1479;
    _replacement_1479 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	needle_len = length(needle) - 1*/
    if (IS_SEQUENCE(_needle_1477)){
            _612 = SEQ_PTR(_needle_1477)->length;
    }
    else {
        _612 = 1;
    }
    _needle_len_1482 = _612 - 1;
    _612 = NOVALUE;

    /** 	replacement_len = length(replacement)*/
    if (IS_SEQUENCE(_replacement_1479)){
            _replacement_len_1483 = SEQ_PTR(_replacement_1479)->length;
    }
    else {
        _replacement_len_1483 = 1;
    }

    /** 	scan_from = 1*/
    _scan_from_1484 = 1;

    /** 	while posn with entry do*/
    goto L3; // [98] 148
L4: 
    if (_posn_1481 == 0)
    {
        goto L5; // [103] 162
    }
    else{
    }

    /** 		haystack = replace(haystack, replacement, posn, posn + needle_len)*/
    _615 = _posn_1481 + _needle_len_1482;
    if ((long)((unsigned long)_615 + (unsigned long)HIGH_BITS) >= 0) 
    _615 = NewDouble((double)_615);
    {
        int p1 = _haystack_1478;
        int p2 = _replacement_1479;
        int p3 = _posn_1481;
        int p4 = _615;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_haystack_1478;
        Replace( &replace_params );
    }
    DeRef(_615);
    _615 = NOVALUE;

    /** 		cnt -= 1*/
    _cnt_1485 = _cnt_1485 - 1;

    /** 		if cnt = 0 then*/
    if (_cnt_1485 != 0)
    goto L6; // [128] 137

    /** 			exit*/
    goto L5; // [134] 162
L6: 

    /** 		scan_from = posn + replacement_len*/
    _scan_from_1484 = _posn_1481 + _replacement_len_1483;

    /** 	entry*/
L3: 

    /** 		posn = match(needle, haystack, scan_from)*/
    _posn_1481 = e_match_from(_needle_1477, _haystack_1478, _scan_from_1484);

    /** 	end while*/
    goto L4; // [159] 101
L5: 

    /** 	return haystack*/
    DeRef(_needle_1477);
    DeRef(_replacement_1479);
    return _haystack_1478;
    ;
}


int _14binary_search(int _needle_1510, int _haystack_1511, int _start_point_1512, int _end_point_1513)
{
    int _lo_1514 = NOVALUE;
    int _hi_1515 = NOVALUE;
    int _mid_1516 = NOVALUE;
    int _c_1517 = NOVALUE;
    int _641 = NOVALUE;
    int _633 = NOVALUE;
    int _631 = NOVALUE;
    int _628 = NOVALUE;
    int _627 = NOVALUE;
    int _626 = NOVALUE;
    int _625 = NOVALUE;
    int _622 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lo = start_point*/
    _lo_1514 = 1;

    /** 	if end_point <= 0 then*/
    if (_end_point_1513 > 0)
    goto L1; // [20] 38

    /** 		hi = length(haystack) + end_point*/
    if (IS_SEQUENCE(_haystack_1511)){
            _622 = SEQ_PTR(_haystack_1511)->length;
    }
    else {
        _622 = 1;
    }
    _hi_1515 = _622 + _end_point_1513;
    _622 = NOVALUE;
    goto L2; // [35] 46
L1: 

    /** 		hi = end_point*/
    _hi_1515 = _end_point_1513;
L2: 

    /** 	if lo<1 then*/
    if (_lo_1514 >= 1)
    goto L3; // [48] 60

    /** 		lo=1*/
    _lo_1514 = 1;
L3: 

    /** 	if lo > hi and length(haystack) > 0 then*/
    _625 = (_lo_1514 > _hi_1515);
    if (_625 == 0) {
        goto L4; // [68] 91
    }
    if (IS_SEQUENCE(_haystack_1511)){
            _627 = SEQ_PTR(_haystack_1511)->length;
    }
    else {
        _627 = 1;
    }
    _628 = (_627 > 0);
    _627 = NOVALUE;
    if (_628 == 0)
    {
        DeRef(_628);
        _628 = NOVALUE;
        goto L4; // [80] 91
    }
    else{
        DeRef(_628);
        _628 = NOVALUE;
    }

    /** 		hi = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1511)){
            _hi_1515 = SEQ_PTR(_haystack_1511)->length;
    }
    else {
        _hi_1515 = 1;
    }
L4: 

    /** 	mid = start_point*/
    _mid_1516 = _start_point_1512;

    /** 	c = 0*/
    _c_1517 = 0;

    /** 	while lo <= hi do*/
L5: 
    if (_lo_1514 > _hi_1515)
    goto L6; // [110] 186

    /** 		mid = floor((lo + hi) / 2)*/
    _631 = _lo_1514 + _hi_1515;
    if ((long)((unsigned long)_631 + (unsigned long)HIGH_BITS) >= 0) 
    _631 = NewDouble((double)_631);
    if (IS_ATOM_INT(_631)) {
        _mid_1516 = _631 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _631, 2);
        _mid_1516 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_631);
    _631 = NOVALUE;
    if (!IS_ATOM_INT(_mid_1516)) {
        _1 = (long)(DBL_PTR(_mid_1516)->dbl);
        if (UNIQUE(DBL_PTR(_mid_1516)) && (DBL_PTR(_mid_1516)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_1516);
        _mid_1516 = _1;
    }

    /** 		c = eu:compare(needle, haystack[mid])*/
    _2 = (int)SEQ_PTR(_haystack_1511);
    _633 = (int)*(((s1_ptr)_2)->base + _mid_1516);
    if (IS_ATOM_INT(_needle_1510) && IS_ATOM_INT(_633)){
        _c_1517 = (_needle_1510 < _633) ? -1 : (_needle_1510 > _633);
    }
    else{
        _c_1517 = compare(_needle_1510, _633);
    }
    _633 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_1517 >= 0)
    goto L7; // [142] 157

    /** 			hi = mid - 1*/
    _hi_1515 = _mid_1516 - 1;
    goto L5; // [154] 110
L7: 

    /** 		elsif c > 0 then*/
    if (_c_1517 <= 0)
    goto L8; // [159] 174

    /** 			lo = mid + 1*/
    _lo_1514 = _mid_1516 + 1;
    goto L5; // [171] 110
L8: 

    /** 			return mid*/
    DeRefDS(_haystack_1511);
    DeRef(_625);
    _625 = NOVALUE;
    return _mid_1516;

    /** 	end while*/
    goto L5; // [183] 110
L6: 

    /** 	if c > 0 then*/
    if (_c_1517 <= 0)
    goto L9; // [188] 201

    /** 		mid += 1*/
    _mid_1516 = _mid_1516 + 1;
L9: 

    /** 	return -mid*/
    if ((unsigned long)_mid_1516 == 0xC0000000)
    _641 = (int)NewDouble((double)-0xC0000000);
    else
    _641 = - _mid_1516;
    DeRefDS(_haystack_1511);
    DeRef(_625);
    _625 = NOVALUE;
    return _641;
    ;
}


int _14begins(int _sub_text_1598, int _full_text_1599)
{
    int _679 = NOVALUE;
    int _678 = NOVALUE;
    int _677 = NOVALUE;
    int _675 = NOVALUE;
    int _674 = NOVALUE;
    int _673 = NOVALUE;
    int _672 = NOVALUE;
    int _671 = NOVALUE;
    int _669 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1599)){
            _669 = SEQ_PTR(_full_text_1599)->length;
    }
    else {
        _669 = 1;
    }
    if (_669 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_1598);
    DeRefDS(_full_text_1599);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _671 = IS_ATOM(_sub_text_1598);
    if (_671 == 0)
    {
        _671 = NOVALUE;
        goto L2; // [24] 57
    }
    else{
        _671 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[1]) then*/
    _2 = (int)SEQ_PTR(_full_text_1599);
    _672 = (int)*(((s1_ptr)_2)->base + 1);
    if (_sub_text_1598 == _672)
    _673 = 1;
    else if (IS_ATOM_INT(_sub_text_1598) && IS_ATOM_INT(_672))
    _673 = 0;
    else
    _673 = (compare(_sub_text_1598, _672) == 0);
    _672 = NOVALUE;
    if (_673 == 0)
    {
        _673 = NOVALUE;
        goto L3; // [37] 49
    }
    else{
        _673 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_1598);
    DeRefDS(_full_text_1599);
    return 1;
    goto L4; // [46] 56
L3: 

    /** 			return 0*/
    DeRef(_sub_text_1598);
    DeRefDS(_full_text_1599);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1598)){
            _674 = SEQ_PTR(_sub_text_1598)->length;
    }
    else {
        _674 = 1;
    }
    if (IS_SEQUENCE(_full_text_1599)){
            _675 = SEQ_PTR(_full_text_1599)->length;
    }
    else {
        _675 = 1;
    }
    if (_674 <= _675)
    goto L5; // [65] 76

    /** 		return 0*/
    DeRef(_sub_text_1598);
    DeRefDS(_full_text_1599);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[1.. length(sub_text)]) then*/
    if (IS_SEQUENCE(_sub_text_1598)){
            _677 = SEQ_PTR(_sub_text_1598)->length;
    }
    else {
        _677 = 1;
    }
    rhs_slice_target = (object_ptr)&_678;
    RHS_Slice(_full_text_1599, 1, _677);
    if (_sub_text_1598 == _678)
    _679 = 1;
    else if (IS_ATOM_INT(_sub_text_1598) && IS_ATOM_INT(_678))
    _679 = 0;
    else
    _679 = (compare(_sub_text_1598, _678) == 0);
    DeRefDS(_678);
    _678 = NOVALUE;
    if (_679 == 0)
    {
        _679 = NOVALUE;
        goto L6; // [90] 102
    }
    else{
        _679 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_1598);
    DeRefDS(_full_text_1599);
    return 1;
    goto L7; // [99] 109
L6: 

    /** 		return 0*/
    DeRef(_sub_text_1598);
    DeRefDS(_full_text_1599);
    return 0;
L7: 
    ;
}


int _14ends(int _sub_text_1620, int _full_text_1621)
{
    int _695 = NOVALUE;
    int _694 = NOVALUE;
    int _693 = NOVALUE;
    int _692 = NOVALUE;
    int _691 = NOVALUE;
    int _690 = NOVALUE;
    int _689 = NOVALUE;
    int _687 = NOVALUE;
    int _686 = NOVALUE;
    int _685 = NOVALUE;
    int _684 = NOVALUE;
    int _683 = NOVALUE;
    int _682 = NOVALUE;
    int _680 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1621)){
            _680 = SEQ_PTR(_full_text_1621)->length;
    }
    else {
        _680 = 1;
    }
    if (_680 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_1620);
    DeRefDS(_full_text_1621);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _682 = IS_ATOM(_sub_text_1620);
    if (_682 == 0)
    {
        _682 = NOVALUE;
        goto L2; // [24] 60
    }
    else{
        _682 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[$]) then*/
    if (IS_SEQUENCE(_full_text_1621)){
            _683 = SEQ_PTR(_full_text_1621)->length;
    }
    else {
        _683 = 1;
    }
    _2 = (int)SEQ_PTR(_full_text_1621);
    _684 = (int)*(((s1_ptr)_2)->base + _683);
    if (_sub_text_1620 == _684)
    _685 = 1;
    else if (IS_ATOM_INT(_sub_text_1620) && IS_ATOM_INT(_684))
    _685 = 0;
    else
    _685 = (compare(_sub_text_1620, _684) == 0);
    _684 = NOVALUE;
    if (_685 == 0)
    {
        _685 = NOVALUE;
        goto L3; // [40] 52
    }
    else{
        _685 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_1620);
    DeRefDS(_full_text_1621);
    return 1;
    goto L4; // [49] 59
L3: 

    /** 			return 0*/
    DeRef(_sub_text_1620);
    DeRefDS(_full_text_1621);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1620)){
            _686 = SEQ_PTR(_sub_text_1620)->length;
    }
    else {
        _686 = 1;
    }
    if (IS_SEQUENCE(_full_text_1621)){
            _687 = SEQ_PTR(_full_text_1621)->length;
    }
    else {
        _687 = 1;
    }
    if (_686 <= _687)
    goto L5; // [68] 79

    /** 		return 0*/
    DeRef(_sub_text_1620);
    DeRefDS(_full_text_1621);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[$ - length(sub_text) + 1 .. $]) then*/
    if (IS_SEQUENCE(_full_text_1621)){
            _689 = SEQ_PTR(_full_text_1621)->length;
    }
    else {
        _689 = 1;
    }
    if (IS_SEQUENCE(_sub_text_1620)){
            _690 = SEQ_PTR(_sub_text_1620)->length;
    }
    else {
        _690 = 1;
    }
    _691 = _689 - _690;
    _689 = NOVALUE;
    _690 = NOVALUE;
    _692 = _691 + 1;
    _691 = NOVALUE;
    if (IS_SEQUENCE(_full_text_1621)){
            _693 = SEQ_PTR(_full_text_1621)->length;
    }
    else {
        _693 = 1;
    }
    rhs_slice_target = (object_ptr)&_694;
    RHS_Slice(_full_text_1621, _692, _693);
    if (_sub_text_1620 == _694)
    _695 = 1;
    else if (IS_ATOM_INT(_sub_text_1620) && IS_ATOM_INT(_694))
    _695 = 0;
    else
    _695 = (compare(_sub_text_1620, _694) == 0);
    DeRefDS(_694);
    _694 = NOVALUE;
    if (_695 == 0)
    {
        _695 = NOVALUE;
        goto L6; // [107] 119
    }
    else{
        _695 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_1620);
    DeRefDS(_full_text_1621);
    _692 = NOVALUE;
    return 1;
    goto L7; // [116] 126
L6: 

    /** 		return 0*/
    DeRef(_sub_text_1620);
    DeRefDS(_full_text_1621);
    DeRef(_692);
    _692 = NOVALUE;
    return 0;
L7: 
    ;
}


int _14vlookup(int _find_item_1728, int _grid_data_1729, int _source_col_1730, int _target_col_1731, int _def_value_1732)
{
    int _756 = NOVALUE;
    int _755 = NOVALUE;
    int _753 = NOVALUE;
    int _752 = NOVALUE;
    int _751 = NOVALUE;
    int _750 = NOVALUE;
    int _749 = NOVALUE;
    int _747 = NOVALUE;
    int _746 = NOVALUE;
    int _745 = NOVALUE;
    int _744 = NOVALUE;
    int _743 = NOVALUE;
    int _0, _1, _2;
    

    /**     for i = 1 to length(grid_data) do*/
    _743 = 24;
    {
        int _i_1734;
        _i_1734 = 1;
L1: 
        if (_i_1734 > 24){
            goto L2; // [16] 117
        }

        /**     	if atom(grid_data[i]) then*/
        _2 = (int)SEQ_PTR(_grid_data_1729);
        _744 = (int)*(((s1_ptr)_2)->base + _i_1734);
        _745 = 0;
        _744 = NOVALUE;
        if (_745 == 0)
        {
            _745 = NOVALUE;
            goto L3; // [32] 40
        }
        else{
            _745 = NOVALUE;
        }

        /**     		continue*/
        goto L4; // [37] 112
L3: 

        /**     	if length(grid_data[i]) < source_col then*/
        _2 = (int)SEQ_PTR(_grid_data_1729);
        _746 = (int)*(((s1_ptr)_2)->base + _i_1734);
        if (IS_SEQUENCE(_746)){
                _747 = SEQ_PTR(_746)->length;
        }
        else {
            _747 = 1;
        }
        _746 = NOVALUE;
        if (_747 >= _source_col_1730)
        goto L5; // [49] 58

        /**     		continue*/
        goto L4; // [55] 112
L5: 

        /**     	if equal(find_item, grid_data[i][source_col]) then*/
        _2 = (int)SEQ_PTR(_grid_data_1729);
        _749 = (int)*(((s1_ptr)_2)->base + _i_1734);
        _2 = (int)SEQ_PTR(_749);
        _750 = (int)*(((s1_ptr)_2)->base + _source_col_1730);
        _749 = NOVALUE;
        if (_find_item_1728 == _750)
        _751 = 1;
        else if (IS_ATOM_INT(_find_item_1728) && IS_ATOM_INT(_750))
        _751 = 0;
        else
        _751 = (compare(_find_item_1728, _750) == 0);
        _750 = NOVALUE;
        if (_751 == 0)
        {
            _751 = NOVALUE;
            goto L6; // [72] 110
        }
        else{
            _751 = NOVALUE;
        }

        /** 	    	if length(grid_data[i]) < target_col then*/
        _2 = (int)SEQ_PTR(_grid_data_1729);
        _752 = (int)*(((s1_ptr)_2)->base + _i_1734);
        if (IS_SEQUENCE(_752)){
                _753 = SEQ_PTR(_752)->length;
        }
        else {
            _753 = 1;
        }
        _752 = NOVALUE;
        if (_753 >= _target_col_1731)
        goto L7; // [84] 95

        /**     			return def_value*/
        DeRefDS(_grid_data_1729);
        _746 = NOVALUE;
        _752 = NOVALUE;
        return _def_value_1732;
L7: 

        /**     		return grid_data[i][target_col]*/
        _2 = (int)SEQ_PTR(_grid_data_1729);
        _755 = (int)*(((s1_ptr)_2)->base + _i_1734);
        _2 = (int)SEQ_PTR(_755);
        _756 = (int)*(((s1_ptr)_2)->base + _target_col_1731);
        _755 = NOVALUE;
        Ref(_756);
        DeRefDS(_grid_data_1729);
        DeRefi(_def_value_1732);
        _746 = NOVALUE;
        _752 = NOVALUE;
        return _756;
L6: 

        /**     end for*/
L4: 
        _i_1734 = _i_1734 + 1;
        goto L1; // [112] 23
L2: 
        ;
    }

    /**     return def_value*/
    DeRefDS(_grid_data_1729);
    _746 = NOVALUE;
    _752 = NOVALUE;
    _756 = NOVALUE;
    return _def_value_1732;
    ;
}



// 0x73462E37
