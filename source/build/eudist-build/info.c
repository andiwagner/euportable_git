// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _30version_node(int _full_11596)
{
    int _6559 = NOVALUE;
    int _6558 = NOVALUE;
    int _6557 = NOVALUE;
    int _6556 = NOVALUE;
    int _6555 = NOVALUE;
    int _6554 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or length(version_info[NODE]) < 12 then*/
    if (0 != 0) {
        goto L1; // [7] 31
    }
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6554 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6554)){
            _6555 = SEQ_PTR(_6554)->length;
    }
    else {
        _6555 = 1;
    }
    _6554 = NOVALUE;
    _6556 = (_6555 < 12);
    _6555 = NOVALUE;
    if (_6556 == 0)
    {
        DeRef(_6556);
        _6556 = NOVALUE;
        goto L2; // [27] 46
    }
    else{
        DeRef(_6556);
        _6556 = NOVALUE;
    }
L1: 

    /** 		return version_info[NODE]*/
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6557 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_6557);
    _6554 = NOVALUE;
    return _6557;
L2: 

    /** 	return version_info[NODE][1..12]*/
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6558 = (int)*(((s1_ptr)_2)->base + 5);
    rhs_slice_target = (object_ptr)&_6559;
    RHS_Slice(_6558, 1, 12);
    _6558 = NOVALUE;
    _6554 = NOVALUE;
    _6557 = NOVALUE;
    return _6559;
    ;
}


int _30version_date(int _full_11610)
{
    int _6568 = NOVALUE;
    int _6567 = NOVALUE;
    int _6566 = NOVALUE;
    int _6565 = NOVALUE;
    int _6564 = NOVALUE;
    int _6563 = NOVALUE;
    int _6561 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental or length(version_info[REVISION_DATE]) < 10 then*/
    if (_full_11610 != 0) {
        _6561 = 1;
        goto L1; // [7] 17
    }
    _6561 = (_30is_developmental_11560 != 0);
L1: 
    if (_6561 != 0) {
        goto L2; // [17] 41
    }
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6563 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_6563)){
            _6564 = SEQ_PTR(_6563)->length;
    }
    else {
        _6564 = 1;
    }
    _6563 = NOVALUE;
    _6565 = (_6564 < 10);
    _6564 = NOVALUE;
    if (_6565 == 0)
    {
        DeRef(_6565);
        _6565 = NOVALUE;
        goto L3; // [37] 56
    }
    else{
        DeRef(_6565);
        _6565 = NOVALUE;
    }
L2: 

    /** 		return version_info[REVISION_DATE]*/
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6566 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_6566);
    _6563 = NOVALUE;
    return _6566;
L3: 

    /** 	return version_info[REVISION_DATE][1..10]*/
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6567 = (int)*(((s1_ptr)_2)->base + 7);
    rhs_slice_target = (object_ptr)&_6568;
    RHS_Slice(_6567, 1, 10);
    _6567 = NOVALUE;
    _6563 = NOVALUE;
    _6566 = NOVALUE;
    return _6568;
    ;
}


int _30version_string(int _full_11625)
{
    int _version_revision_inlined_version_revision_at_51_11634 = NOVALUE;
    int _6588 = NOVALUE;
    int _6587 = NOVALUE;
    int _6586 = NOVALUE;
    int _6585 = NOVALUE;
    int _6584 = NOVALUE;
    int _6583 = NOVALUE;
    int _6582 = NOVALUE;
    int _6581 = NOVALUE;
    int _6579 = NOVALUE;
    int _6578 = NOVALUE;
    int _6577 = NOVALUE;
    int _6576 = NOVALUE;
    int _6575 = NOVALUE;
    int _6574 = NOVALUE;
    int _6573 = NOVALUE;
    int _6572 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if full or is_developmental then*/
    if (0 != 0) {
        goto L1; // [7] 18
    }
    if (_30is_developmental_11560 == 0)
    {
        goto L2; // [14] 92
    }
    else{
    }
L1: 

    /** 		return sprintf("%d.%d.%d %s (%d:%s, %s)", {*/
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6572 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6573 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6574 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6575 = (int)*(((s1_ptr)_2)->base + 4);

    /** 	return version_info[REVISION]*/
    DeRef(_version_revision_inlined_version_revision_at_51_11634);
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _version_revision_inlined_version_revision_at_51_11634 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_version_revision_inlined_version_revision_at_51_11634);
    _6576 = _30version_node(0);
    _6577 = _30version_date(_full_11625);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6572);
    *((int *)(_2+4)) = _6572;
    Ref(_6573);
    *((int *)(_2+8)) = _6573;
    Ref(_6574);
    *((int *)(_2+12)) = _6574;
    Ref(_6575);
    *((int *)(_2+16)) = _6575;
    Ref(_version_revision_inlined_version_revision_at_51_11634);
    *((int *)(_2+20)) = _version_revision_inlined_version_revision_at_51_11634;
    *((int *)(_2+24)) = _6576;
    *((int *)(_2+28)) = _6577;
    _6578 = MAKE_SEQ(_1);
    _6577 = NOVALUE;
    _6576 = NOVALUE;
    _6575 = NOVALUE;
    _6574 = NOVALUE;
    _6573 = NOVALUE;
    _6572 = NOVALUE;
    _6579 = EPrintf(-9999999, _6571, _6578);
    DeRefDS(_6578);
    _6578 = NOVALUE;
    return _6579;
    goto L3; // [89] 152
L2: 

    /** 		return sprintf("%d.%d.%d %s (%s, %s)", {*/
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6581 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6582 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6583 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_30version_info_11558);
    _6584 = (int)*(((s1_ptr)_2)->base + 4);
    _6585 = _30version_node(0);
    _6586 = _30version_date(_full_11625);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_6581);
    *((int *)(_2+4)) = _6581;
    Ref(_6582);
    *((int *)(_2+8)) = _6582;
    Ref(_6583);
    *((int *)(_2+12)) = _6583;
    Ref(_6584);
    *((int *)(_2+16)) = _6584;
    *((int *)(_2+20)) = _6585;
    *((int *)(_2+24)) = _6586;
    _6587 = MAKE_SEQ(_1);
    _6586 = NOVALUE;
    _6585 = NOVALUE;
    _6584 = NOVALUE;
    _6583 = NOVALUE;
    _6582 = NOVALUE;
    _6581 = NOVALUE;
    _6588 = EPrintf(-9999999, _6580, _6587);
    DeRefDS(_6587);
    _6587 = NOVALUE;
    DeRef(_6579);
    _6579 = NOVALUE;
    return _6588;
L3: 
    ;
}



// 0xAD35B3F7
