// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _6get_bytes(int _fn_9852, int _n_9853)
{
    int _s_9854 = NOVALUE;
    int _c_9855 = NOVALUE;
    int _first_9856 = NOVALUE;
    int _last_9857 = NOVALUE;
    int _5633 = NOVALUE;
    int _5630 = NOVALUE;
    int _5628 = NOVALUE;
    int _5627 = NOVALUE;
    int _5626 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fn_9852)) {
        _1 = (long)(DBL_PTR(_fn_9852)->dbl);
        if (UNIQUE(DBL_PTR(_fn_9852)) && (DBL_PTR(_fn_9852)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_9852);
        _fn_9852 = _1;
    }
    if (!IS_ATOM_INT(_n_9853)) {
        _1 = (long)(DBL_PTR(_n_9853)->dbl);
        if (UNIQUE(DBL_PTR(_n_9853)) && (DBL_PTR(_n_9853)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_n_9853);
        _n_9853 = _1;
    }

    /** 	if n = 0 then*/
    if (_n_9853 != 0)
    goto L1; // [11] 22

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_9854);
    return _5;
L1: 

    /** 	c = getc(fn)*/
    if (_fn_9852 != last_r_file_no) {
        last_r_file_ptr = which_file(_fn_9852, EF_READ);
        last_r_file_no = _fn_9852;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_9855 = getKBchar();
        }
        else
        _c_9855 = getc(last_r_file_ptr);
    }
    else
    _c_9855 = getc(last_r_file_ptr);

    /** 	if c = EOF then*/
    if (_c_9855 != -1)
    goto L2; // [31] 42

    /** 		return {}*/
    RefDS(_5);
    DeRefi(_s_9854);
    return _5;
L2: 

    /** 	s = repeat(c, n)*/
    DeRefi(_s_9854);
    _s_9854 = Repeat(_c_9855, _n_9853);

    /** 	last = 1*/
    _last_9857 = 1;

    /** 	while last < n do*/
L3: 
    if (_last_9857 >= _n_9853)
    goto L4; // [60] 175

    /** 		first = last+1*/
    _first_9856 = _last_9857 + 1;

    /** 		last  = last+CHUNK*/
    _last_9857 = _last_9857 + 100;

    /** 		if last > n then*/
    if (_last_9857 <= _n_9853)
    goto L5; // [82] 94

    /** 			last = n*/
    _last_9857 = _n_9853;
L5: 

    /** 		for i = first to last do*/
    _5626 = _last_9857;
    {
        int _i_9871;
        _i_9871 = _first_9856;
L6: 
        if (_i_9871 > _5626){
            goto L7; // [99] 122
        }

        /** 			s[i] = getc(fn)*/
        if (_fn_9852 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_9852, EF_READ);
            last_r_file_no = _fn_9852;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _5627 = getKBchar();
            }
            else
            _5627 = getc(last_r_file_ptr);
        }
        else
        _5627 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_s_9854);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _s_9854 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9871);
        *(int *)_2 = _5627;
        if( _1 != _5627 ){
        }
        _5627 = NOVALUE;

        /** 		end for*/
        _i_9871 = _i_9871 + 1;
        goto L6; // [117] 106
L7: 
        ;
    }

    /** 		if s[last] = EOF then*/
    _2 = (int)SEQ_PTR(_s_9854);
    _5628 = (int)*(((s1_ptr)_2)->base + _last_9857);
    if (_5628 != -1)
    goto L3; // [128] 60

    /** 			while s[last] = EOF do*/
L8: 
    _2 = (int)SEQ_PTR(_s_9854);
    _5630 = (int)*(((s1_ptr)_2)->base + _last_9857);
    if (_5630 != -1)
    goto L9; // [141] 158

    /** 				last -= 1*/
    _last_9857 = _last_9857 - 1;

    /** 			end while*/
    goto L8; // [155] 137
L9: 

    /** 			return s[1..last]*/
    rhs_slice_target = (object_ptr)&_5633;
    RHS_Slice(_s_9854, 1, _last_9857);
    DeRefDSi(_s_9854);
    _5628 = NOVALUE;
    _5630 = NOVALUE;
    return _5633;

    /** 	end while*/
    goto L3; // [172] 60
L4: 

    /** 	return s*/
    _5628 = NOVALUE;
    _5630 = NOVALUE;
    DeRef(_5633);
    _5633 = NOVALUE;
    return _s_9854;
    ;
}


int _6get_integer32(int _fh_9892)
{
    int _5642 = NOVALUE;
    int _5641 = NOVALUE;
    int _5640 = NOVALUE;
    int _5639 = NOVALUE;
    int _5638 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_9892)) {
        _1 = (long)(DBL_PTR(_fh_9892)->dbl);
        if (UNIQUE(DBL_PTR(_fh_9892)) && (DBL_PTR(_fh_9892)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_9892);
        _fh_9892 = _1;
    }

    /** 	poke(mem0, getc(fh))*/
    if (_fh_9892 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_9892, EF_READ);
        last_r_file_no = _fh_9892;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _5638 = getKBchar();
        }
        else
        _5638 = getc(last_r_file_ptr);
    }
    else
    _5638 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_6mem0_9882)){
        poke_addr = (unsigned char *)_6mem0_9882;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_6mem0_9882)->dbl);
    }
    *poke_addr = (unsigned char)_5638;
    _5638 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_9892 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_9892, EF_READ);
        last_r_file_no = _fh_9892;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _5639 = getKBchar();
        }
        else
        _5639 = getc(last_r_file_ptr);
    }
    else
    _5639 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_6mem1_9883)){
        poke_addr = (unsigned char *)_6mem1_9883;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_6mem1_9883)->dbl);
    }
    *poke_addr = (unsigned char)_5639;
    _5639 = NOVALUE;

    /** 	poke(mem2, getc(fh))*/
    if (_fh_9892 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_9892, EF_READ);
        last_r_file_no = _fh_9892;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _5640 = getKBchar();
        }
        else
        _5640 = getc(last_r_file_ptr);
    }
    else
    _5640 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_6mem2_9884)){
        poke_addr = (unsigned char *)_6mem2_9884;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_6mem2_9884)->dbl);
    }
    *poke_addr = (unsigned char)_5640;
    _5640 = NOVALUE;

    /** 	poke(mem3, getc(fh))*/
    if (_fh_9892 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_9892, EF_READ);
        last_r_file_no = _fh_9892;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _5641 = getKBchar();
        }
        else
        _5641 = getc(last_r_file_ptr);
    }
    else
    _5641 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_6mem3_9885)){
        poke_addr = (unsigned char *)_6mem3_9885;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_6mem3_9885)->dbl);
    }
    *poke_addr = (unsigned char)_5641;
    _5641 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_6mem0_9882)) {
        _5642 = *(unsigned long *)_6mem0_9882;
        if ((unsigned)_5642 > (unsigned)MAXINT)
        _5642 = NewDouble((double)(unsigned long)_5642);
    }
    else {
        _5642 = *(unsigned long *)(unsigned long)(DBL_PTR(_6mem0_9882)->dbl);
        if ((unsigned)_5642 > (unsigned)MAXINT)
        _5642 = NewDouble((double)(unsigned long)_5642);
    }
    return _5642;
    ;
}


int _6get_integer16(int _fh_9900)
{
    int _5645 = NOVALUE;
    int _5644 = NOVALUE;
    int _5643 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_9900)) {
        _1 = (long)(DBL_PTR(_fh_9900)->dbl);
        if (UNIQUE(DBL_PTR(_fh_9900)) && (DBL_PTR(_fh_9900)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_9900);
        _fh_9900 = _1;
    }

    /** 	poke(mem0, getc(fh))*/
    if (_fh_9900 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_9900, EF_READ);
        last_r_file_no = _fh_9900;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _5643 = getKBchar();
        }
        else
        _5643 = getc(last_r_file_ptr);
    }
    else
    _5643 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_6mem0_9882)){
        poke_addr = (unsigned char *)_6mem0_9882;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_6mem0_9882)->dbl);
    }
    *poke_addr = (unsigned char)_5643;
    _5643 = NOVALUE;

    /** 	poke(mem1, getc(fh))*/
    if (_fh_9900 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_9900, EF_READ);
        last_r_file_no = _fh_9900;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _5644 = getKBchar();
        }
        else
        _5644 = getc(last_r_file_ptr);
    }
    else
    _5644 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_6mem1_9883)){
        poke_addr = (unsigned char *)_6mem1_9883;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_6mem1_9883)->dbl);
    }
    *poke_addr = (unsigned char)_5644;
    _5644 = NOVALUE;

    /** 	return peek2u(mem0)*/
    if (IS_ATOM_INT(_6mem0_9882)) {
        _5645 = *(unsigned short *)_6mem0_9882;
    }
    else {
        _5645 = *(unsigned short *)(unsigned long)(DBL_PTR(_6mem0_9882)->dbl);
    }
    return _5645;
    ;
}


void _6put_integer32(int _fh_9906, int _val_9907)
{
    int _5647 = NOVALUE;
    int _5646 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_9906)) {
        _1 = (long)(DBL_PTR(_fh_9906)->dbl);
        if (UNIQUE(DBL_PTR(_fh_9906)) && (DBL_PTR(_fh_9906)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_9906);
        _fh_9906 = _1;
    }

    /** 	poke4(mem0, val)*/
    if (IS_ATOM_INT(_6mem0_9882)){
        poke4_addr = (unsigned long *)_6mem0_9882;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_6mem0_9882)->dbl);
    }
    if (IS_ATOM_INT(_val_9907)) {
        *poke4_addr = (unsigned long)_val_9907;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_val_9907)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(fh, peek({mem0,4}))*/
    Ref(_6mem0_9882);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6mem0_9882;
    ((int *)_2)[2] = 4;
    _5646 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_5646);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _5647 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_5646);
    _5646 = NOVALUE;
    EPuts(_fh_9906, _5647); // DJP 
    DeRefDS(_5647);
    _5647 = NOVALUE;

    /** end procedure*/
    DeRef(_val_9907);
    return;
    ;
}


void _6put_integer16(int _fh_9912, int _val_9913)
{
    int _5649 = NOVALUE;
    int _5648 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_9912)) {
        _1 = (long)(DBL_PTR(_fh_9912)->dbl);
        if (UNIQUE(DBL_PTR(_fh_9912)) && (DBL_PTR(_fh_9912)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_9912);
        _fh_9912 = _1;
    }

    /** 	poke2(mem0, val)*/
    if (IS_ATOM_INT(_6mem0_9882)){
        poke2_addr = (unsigned short *)_6mem0_9882;
    }
    else {
        poke2_addr = (unsigned short *)(unsigned long)(DBL_PTR(_6mem0_9882)->dbl);
    }
    if (IS_ATOM_INT(_val_9913)) {
        *poke2_addr = (unsigned short)_val_9913;
    }
    else {
        _1 = (signed char)DBL_PTR(_val_9913)->dbl;
        *poke_addr = _1;
    }

    /** 	puts(fh, peek({mem0,2}))*/
    Ref(_6mem0_9882);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6mem0_9882;
    ((int *)_2)[2] = 2;
    _5648 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_5648);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _5649 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_5648);
    _5648 = NOVALUE;
    EPuts(_fh_9912, _5649); // DJP 
    DeRefDS(_5649);
    _5649 = NOVALUE;

    /** end procedure*/
    DeRef(_val_9913);
    return;
    ;
}


int _6get_dstring(int _fh_9918, int _delim_9919)
{
    int _s_9920 = NOVALUE;
    int _c_9921 = NOVALUE;
    int _i_9922 = NOVALUE;
    int _5659 = NOVALUE;
    int _5655 = NOVALUE;
    int _5653 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fh_9918)) {
        _1 = (long)(DBL_PTR(_fh_9918)->dbl);
        if (UNIQUE(DBL_PTR(_fh_9918)) && (DBL_PTR(_fh_9918)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_9918);
        _fh_9918 = _1;
    }
    if (!IS_ATOM_INT(_delim_9919)) {
        _1 = (long)(DBL_PTR(_delim_9919)->dbl);
        if (UNIQUE(DBL_PTR(_delim_9919)) && (DBL_PTR(_delim_9919)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_delim_9919);
        _delim_9919 = _1;
    }

    /** 	s = repeat(-1, 256)*/
    DeRefi(_s_9920);
    _s_9920 = Repeat(-1, 256);

    /** 	i = 0*/
    _i_9922 = 0;

    /** 	while c != delim with entry do*/
    goto L1; // [24] 81
L2: 
    if (_c_9921 == _delim_9919)
    goto L3; // [29] 93

    /** 		i += 1*/
    _i_9922 = _i_9922 + 1;

    /** 		if i > length(s) then*/
    if (IS_SEQUENCE(_s_9920)){
            _5653 = SEQ_PTR(_s_9920)->length;
    }
    else {
        _5653 = 1;
    }
    if (_i_9922 <= _5653)
    goto L4; // [46] 61

    /** 			s &= repeat(-1, 256)*/
    _5655 = Repeat(-1, 256);
    Concat((object_ptr)&_s_9920, _s_9920, _5655);
    DeRefDS(_5655);
    _5655 = NOVALUE;
L4: 

    /** 		if c = -1 then*/
    if (_c_9921 != -1)
    goto L5; // [63] 72

    /** 			exit*/
    goto L3; // [69] 93
L5: 

    /** 		s[i] = c*/
    _2 = (int)SEQ_PTR(_s_9920);
    _2 = (int)(((s1_ptr)_2)->base + _i_9922);
    *(int *)_2 = _c_9921;

    /** 	  entry*/
L1: 

    /** 		c = getc(fh)*/
    if (_fh_9918 != last_r_file_no) {
        last_r_file_ptr = which_file(_fh_9918, EF_READ);
        last_r_file_no = _fh_9918;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_9921 = getKBchar();
        }
        else
        _c_9921 = getc(last_r_file_ptr);
    }
    else
    _c_9921 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [90] 27
L3: 

    /** 	return s[1..i]*/
    rhs_slice_target = (object_ptr)&_5659;
    RHS_Slice(_s_9920, 1, _i_9922);
    DeRefDSi(_s_9920);
    return _5659;
    ;
}


int _6file_number(int _f_9941)
{
    int _5663 = NOVALUE;
    int _5662 = NOVALUE;
    int _5661 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(f) and f >= 0 then*/
    if (IS_ATOM_INT(_f_9941))
    _5661 = 1;
    else if (IS_ATOM_DBL(_f_9941))
    _5661 = IS_ATOM_INT(DoubleToInt(_f_9941));
    else
    _5661 = 0;
    if (_5661 == 0) {
        goto L1; // [6] 27
    }
    if (IS_ATOM_INT(_f_9941)) {
        _5663 = (_f_9941 >= 0);
    }
    else {
        _5663 = binary_op(GREATEREQ, _f_9941, 0);
    }
    if (_5663 == 0) {
        DeRef(_5663);
        _5663 = NOVALUE;
        goto L1; // [15] 27
    }
    else {
        if (!IS_ATOM_INT(_5663) && DBL_PTR(_5663)->dbl == 0.0){
            DeRef(_5663);
            _5663 = NOVALUE;
            goto L1; // [15] 27
        }
        DeRef(_5663);
        _5663 = NOVALUE;
    }
    DeRef(_5663);
    _5663 = NOVALUE;

    /** 		return 1*/
    DeRef(_f_9941);
    return 1;
    goto L2; // [24] 34
L1: 

    /** 		return 0*/
    DeRef(_f_9941);
    return 0;
L2: 
    ;
}


int _6file_position(int _p_9949)
{
    int _5666 = NOVALUE;
    int _5665 = NOVALUE;
    int _5664 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(p) and p >= -1 then*/
    _5664 = IS_ATOM(_p_9949);
    if (_5664 == 0) {
        goto L1; // [6] 27
    }
    if (IS_ATOM_INT(_p_9949)) {
        _5666 = (_p_9949 >= -1);
    }
    else {
        _5666 = binary_op(GREATEREQ, _p_9949, -1);
    }
    if (_5666 == 0) {
        DeRef(_5666);
        _5666 = NOVALUE;
        goto L1; // [15] 27
    }
    else {
        if (!IS_ATOM_INT(_5666) && DBL_PTR(_5666)->dbl == 0.0){
            DeRef(_5666);
            _5666 = NOVALUE;
            goto L1; // [15] 27
        }
        DeRef(_5666);
        _5666 = NOVALUE;
    }
    DeRef(_5666);
    _5666 = NOVALUE;

    /** 		return 1*/
    DeRef(_p_9949);
    return 1;
    goto L2; // [24] 34
L1: 

    /** 		return 0*/
    DeRef(_p_9949);
    return 0;
L2: 
    ;
}


int _6lock_type(int _t_9957)
{
    int _5671 = NOVALUE;
    int _5670 = NOVALUE;
    int _5669 = NOVALUE;
    int _5668 = NOVALUE;
    int _5667 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(t) and (t = LOCK_SHARED or t = LOCK_EXCLUSIVE) then*/
    if (IS_ATOM_INT(_t_9957))
    _5667 = 1;
    else if (IS_ATOM_DBL(_t_9957))
    _5667 = IS_ATOM_INT(DoubleToInt(_t_9957));
    else
    _5667 = 0;
    if (_5667 == 0) {
        goto L1; // [6] 43
    }
    if (IS_ATOM_INT(_t_9957)) {
        _5669 = (_t_9957 == 1);
    }
    else {
        _5669 = binary_op(EQUALS, _t_9957, 1);
    }
    if (IS_ATOM_INT(_5669)) {
        if (_5669 != 0) {
            _5670 = 1;
            goto L2; // [16] 30
        }
    }
    else {
        if (DBL_PTR(_5669)->dbl != 0.0) {
            _5670 = 1;
            goto L2; // [16] 30
        }
    }
    if (IS_ATOM_INT(_t_9957)) {
        _5671 = (_t_9957 == 2);
    }
    else {
        _5671 = binary_op(EQUALS, _t_9957, 2);
    }
    DeRef(_5670);
    if (IS_ATOM_INT(_5671))
    _5670 = (_5671 != 0);
    else
    _5670 = DBL_PTR(_5671)->dbl != 0.0;
L2: 
    if (_5670 == 0)
    {
        _5670 = NOVALUE;
        goto L1; // [31] 43
    }
    else{
        _5670 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_t_9957);
    DeRef(_5669);
    _5669 = NOVALUE;
    DeRef(_5671);
    _5671 = NOVALUE;
    return 1;
    goto L3; // [40] 50
L1: 

    /** 		return 0*/
    DeRef(_t_9957);
    DeRef(_5669);
    _5669 = NOVALUE;
    DeRef(_5671);
    _5671 = NOVALUE;
    return 0;
L3: 
    ;
}


int _6byte_range(int _r_9967)
{
    int _5690 = NOVALUE;
    int _5689 = NOVALUE;
    int _5688 = NOVALUE;
    int _5687 = NOVALUE;
    int _5686 = NOVALUE;
    int _5684 = NOVALUE;
    int _5683 = NOVALUE;
    int _5681 = NOVALUE;
    int _5680 = NOVALUE;
    int _5679 = NOVALUE;
    int _5678 = NOVALUE;
    int _5677 = NOVALUE;
    int _5675 = NOVALUE;
    int _5673 = NOVALUE;
    int _5672 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(r) then*/
    _5672 = IS_ATOM(_r_9967);
    if (_5672 == 0)
    {
        _5672 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _5672 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_r_9967);
    return 0;
L1: 

    /** 	if length(r) = 0 then*/
    if (IS_SEQUENCE(_r_9967)){
            _5673 = SEQ_PTR(_r_9967)->length;
    }
    else {
        _5673 = 1;
    }
    if (_5673 != 0)
    goto L2; // [21] 32

    /** 		return 1*/
    DeRef(_r_9967);
    return 1;
L2: 

    /** 	if length(r) != 2 then*/
    if (IS_SEQUENCE(_r_9967)){
            _5675 = SEQ_PTR(_r_9967)->length;
    }
    else {
        _5675 = 1;
    }
    if (_5675 == 2)
    goto L3; // [37] 48

    /** 		return 0*/
    DeRef(_r_9967);
    return 0;
L3: 

    /** 	if not (atom(r[1]) and atom(r[2])) then*/
    _2 = (int)SEQ_PTR(_r_9967);
    _5677 = (int)*(((s1_ptr)_2)->base + 1);
    _5678 = IS_ATOM(_5677);
    _5677 = NOVALUE;
    if (_5678 == 0) {
        DeRef(_5679);
        _5679 = 0;
        goto L4; // [57] 72
    }
    _2 = (int)SEQ_PTR(_r_9967);
    _5680 = (int)*(((s1_ptr)_2)->base + 2);
    _5681 = IS_ATOM(_5680);
    _5680 = NOVALUE;
    _5679 = (_5681 != 0);
L4: 
    if (_5679 != 0)
    goto L5; // [72] 82
    _5679 = NOVALUE;

    /** 		return 0*/
    DeRef(_r_9967);
    return 0;
L5: 

    /** 	if r[1] < 0 or r[2] < 0 then*/
    _2 = (int)SEQ_PTR(_r_9967);
    _5683 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5683)) {
        _5684 = (_5683 < 0);
    }
    else {
        _5684 = binary_op(LESS, _5683, 0);
    }
    _5683 = NOVALUE;
    if (IS_ATOM_INT(_5684)) {
        if (_5684 != 0) {
            goto L6; // [92] 109
        }
    }
    else {
        if (DBL_PTR(_5684)->dbl != 0.0) {
            goto L6; // [92] 109
        }
    }
    _2 = (int)SEQ_PTR(_r_9967);
    _5686 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5686)) {
        _5687 = (_5686 < 0);
    }
    else {
        _5687 = binary_op(LESS, _5686, 0);
    }
    _5686 = NOVALUE;
    if (_5687 == 0) {
        DeRef(_5687);
        _5687 = NOVALUE;
        goto L7; // [105] 116
    }
    else {
        if (!IS_ATOM_INT(_5687) && DBL_PTR(_5687)->dbl == 0.0){
            DeRef(_5687);
            _5687 = NOVALUE;
            goto L7; // [105] 116
        }
        DeRef(_5687);
        _5687 = NOVALUE;
    }
    DeRef(_5687);
    _5687 = NOVALUE;
L6: 

    /** 		return 0*/
    DeRef(_r_9967);
    DeRef(_5684);
    _5684 = NOVALUE;
    return 0;
L7: 

    /** 	return r[1] <= r[2]*/
    _2 = (int)SEQ_PTR(_r_9967);
    _5688 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_r_9967);
    _5689 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_5688) && IS_ATOM_INT(_5689)) {
        _5690 = (_5688 <= _5689);
    }
    else {
        _5690 = binary_op(LESSEQ, _5688, _5689);
    }
    _5688 = NOVALUE;
    _5689 = NOVALUE;
    DeRef(_r_9967);
    DeRef(_5684);
    _5684 = NOVALUE;
    return _5690;
    ;
}


int _6seek(int _fn_9994, int _pos_9995)
{
    int _5692 = NOVALUE;
    int _5691 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_9995);
    Ref(_fn_9994);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_9994;
    ((int *)_2)[2] = _pos_9995;
    _5691 = MAKE_SEQ(_1);
    _5692 = machine(19, _5691);
    DeRefDS(_5691);
    _5691 = NOVALUE;
    DeRef(_fn_9994);
    DeRef(_pos_9995);
    return _5692;
    ;
}


int _6where(int _fn_10000)
{
    int _5693 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_WHERE, fn)*/
    _5693 = machine(20, _fn_10000);
    DeRef(_fn_10000);
    return _5693;
    ;
}


void _6flush(int _fn_10004)
{
    int _0, _1, _2;
    

    /** 	machine_proc(M_FLUSH, fn)*/
    machine(60, _fn_10004);

    /** end procedure*/
    DeRef(_fn_10004);
    return;
    ;
}


int _6lock_file(int _fn_10007, int _t_10008, int _r_10009)
{
    int _5695 = NOVALUE;
    int _5694 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_fn_10007);
    *((int *)(_2+4)) = _fn_10007;
    Ref(_t_10008);
    *((int *)(_2+8)) = _t_10008;
    Ref(_r_10009);
    *((int *)(_2+12)) = _r_10009;
    _5694 = MAKE_SEQ(_1);
    _5695 = machine(61, _5694);
    DeRefDS(_5694);
    _5694 = NOVALUE;
    DeRef(_fn_10007);
    DeRef(_t_10008);
    DeRef(_r_10009);
    return _5695;
    ;
}


void _6unlock_file(int _fn_10014, int _r_10015)
{
    int _5696 = NOVALUE;
    int _0, _1, _2;
    

    /** 	machine_proc(M_UNLOCK_FILE, {fn, r})*/
    Ref(_r_10015);
    Ref(_fn_10014);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_10014;
    ((int *)_2)[2] = _r_10015;
    _5696 = MAKE_SEQ(_1);
    machine(62, _5696);
    DeRefDS(_5696);
    _5696 = NOVALUE;

    /** end procedure*/
    DeRef(_fn_10014);
    DeRef(_r_10015);
    return;
    ;
}


int _6read_lines(int _file_10019)
{
    int _fn_10020 = NOVALUE;
    int _ret_10021 = NOVALUE;
    int _y_10022 = NOVALUE;
    int _5715 = NOVALUE;
    int _5714 = NOVALUE;
    int _5713 = NOVALUE;
    int _5712 = NOVALUE;
    int _5707 = NOVALUE;
    int _5706 = NOVALUE;
    int _5704 = NOVALUE;
    int _5703 = NOVALUE;
    int _5702 = NOVALUE;
    int _5698 = NOVALUE;
    int _5697 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _5697 = IS_SEQUENCE(_file_10019);
    if (_5697 == 0)
    {
        _5697 = NOVALUE;
        goto L1; // [6] 37
    }
    else{
        _5697 = NOVALUE;
    }

    /** 		if length(file) = 0 then*/
    if (IS_SEQUENCE(_file_10019)){
            _5698 = SEQ_PTR(_file_10019)->length;
    }
    else {
        _5698 = 1;
    }
    if (_5698 != 0)
    goto L2; // [14] 26

    /** 			fn = 0*/
    DeRef(_fn_10020);
    _fn_10020 = 0;
    goto L3; // [23] 43
L2: 

    /** 			fn = open(file, "r")*/
    DeRef(_fn_10020);
    _fn_10020 = EOpen(_file_10019, _3966, 0);
    goto L3; // [34] 43
L1: 

    /** 		fn = file*/
    Ref(_file_10019);
    DeRef(_fn_10020);
    _fn_10020 = _file_10019;
L3: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_10020, 0)){
        goto L4; // [47] 56
    }
    DeRef(_file_10019);
    DeRef(_fn_10020);
    DeRef(_ret_10021);
    DeRefi(_y_10022);
    return -1;
L4: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_10021);
    _ret_10021 = _5;

    /** 	while sequence(y) with entry do*/
    goto L5; // [63] 125
L6: 
    _5702 = IS_SEQUENCE(_y_10022);
    if (_5702 == 0)
    {
        _5702 = NOVALUE;
        goto L7; // [71] 135
    }
    else{
        _5702 = NOVALUE;
    }

    /** 		if y[$] = '\n' then*/
    if (IS_SEQUENCE(_y_10022)){
            _5703 = SEQ_PTR(_y_10022)->length;
    }
    else {
        _5703 = 1;
    }
    _2 = (int)SEQ_PTR(_y_10022);
    _5704 = (int)*(((s1_ptr)_2)->base + _5703);
    if (_5704 != 10)
    goto L8; // [83] 104

    /** 			y = y[1..$-1]*/
    if (IS_SEQUENCE(_y_10022)){
            _5706 = SEQ_PTR(_y_10022)->length;
    }
    else {
        _5706 = 1;
    }
    _5707 = _5706 - 1;
    _5706 = NOVALUE;
    rhs_slice_target = (object_ptr)&_y_10022;
    RHS_Slice(_y_10022, 1, _5707);

    /** 			ifdef UNIX then*/
L8: 

    /** 		ret = append(ret, y)*/
    Ref(_y_10022);
    Append(&_ret_10021, _ret_10021, _y_10022);

    /** 		if fn = 0 then*/
    if (binary_op_a(NOTEQ, _fn_10020, 0)){
        goto L9; // [112] 122
    }

    /** 			puts(2, '\n')*/
    EPuts(2, 10); // DJP 
L9: 

    /** 	entry*/
L5: 

    /** 		y = gets(fn)*/
    DeRefi(_y_10022);
    _y_10022 = EGets(_fn_10020);

    /** 	end while*/
    goto L6; // [132] 66
L7: 

    /** 	if sequence(file) and length(file) != 0 then*/
    _5712 = IS_SEQUENCE(_file_10019);
    if (_5712 == 0) {
        goto LA; // [140] 160
    }
    if (IS_SEQUENCE(_file_10019)){
            _5714 = SEQ_PTR(_file_10019)->length;
    }
    else {
        _5714 = 1;
    }
    _5715 = (_5714 != 0);
    _5714 = NOVALUE;
    if (_5715 == 0)
    {
        DeRef(_5715);
        _5715 = NOVALUE;
        goto LA; // [152] 160
    }
    else{
        DeRef(_5715);
        _5715 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_10020))
    EClose(_fn_10020);
    else
    EClose((int)DBL_PTR(_fn_10020)->dbl);
LA: 

    /** 	return ret*/
    DeRef(_file_10019);
    DeRef(_fn_10020);
    DeRefi(_y_10022);
    _5704 = NOVALUE;
    DeRef(_5707);
    _5707 = NOVALUE;
    return _ret_10021;
    ;
}


int _6process_lines(int _file_10053, int _proc_10054, int _user_data_10055)
{
    int _fh_10056 = NOVALUE;
    int _aLine_10057 = NOVALUE;
    int _res_10058 = NOVALUE;
    int _line_no_10059 = NOVALUE;
    int _5738 = NOVALUE;
    int _5737 = NOVALUE;
    int _5736 = NOVALUE;
    int _5735 = NOVALUE;
    int _5732 = NOVALUE;
    int _5730 = NOVALUE;
    int _5728 = NOVALUE;
    int _5727 = NOVALUE;
    int _5725 = NOVALUE;
    int _5724 = NOVALUE;
    int _5723 = NOVALUE;
    int _5721 = NOVALUE;
    int _5717 = NOVALUE;
    int _5716 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_10054)) {
        _1 = (long)(DBL_PTR(_proc_10054)->dbl);
        if (UNIQUE(DBL_PTR(_proc_10054)) && (DBL_PTR(_proc_10054)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_10054);
        _proc_10054 = _1;
    }

    /** 	integer line_no = 0*/
    _line_no_10059 = 0;

    /** 	res = 0*/
    DeRef(_res_10058);
    _res_10058 = 0;

    /** 	if sequence(file) then*/
    _5716 = IS_SEQUENCE(_file_10053);
    if (_5716 == 0)
    {
        _5716 = NOVALUE;
        goto L1; // [22] 57
    }
    else{
        _5716 = NOVALUE;
    }

    /** 		if length(file) = 0 then*/
    if (IS_SEQUENCE(_file_10053)){
            _5717 = SEQ_PTR(_file_10053)->length;
    }
    else {
        _5717 = 1;
    }
    if (_5717 != 0)
    goto L2; // [30] 44

    /** 			fh = 0*/
    _fh_10056 = 0;
    goto L3; // [41] 67
L2: 

    /** 			fh = open(file, "r")*/
    _fh_10056 = EOpen(_file_10053, _3966, 0);
    goto L3; // [54] 67
L1: 

    /** 		fh = file*/
    Ref(_file_10053);
    _fh_10056 = _file_10053;
    if (!IS_ATOM_INT(_fh_10056)) {
        _1 = (long)(DBL_PTR(_fh_10056)->dbl);
        if (UNIQUE(DBL_PTR(_fh_10056)) && (DBL_PTR(_fh_10056)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_10056);
        _fh_10056 = _1;
    }
L3: 

    /** 	if fh < 0 then */
    if (_fh_10056 >= 0)
    goto L4; // [71] 171

    /** 		return -1 */
    DeRef(_file_10053);
    DeRef(_user_data_10055);
    DeRefi(_aLine_10057);
    DeRef(_res_10058);
    return -1;

    /** 	while sequence(aLine) with entry do*/
    goto L4; // [84] 171
L5: 
    _5721 = IS_SEQUENCE(_aLine_10057);
    if (_5721 == 0)
    {
        _5721 = NOVALUE;
        goto L6; // [92] 181
    }
    else{
        _5721 = NOVALUE;
    }

    /** 		line_no += 1*/
    _line_no_10059 = _line_no_10059 + 1;

    /** 		if length(aLine) then*/
    if (IS_SEQUENCE(_aLine_10057)){
            _5723 = SEQ_PTR(_aLine_10057)->length;
    }
    else {
        _5723 = 1;
    }
    if (_5723 == 0)
    {
        _5723 = NOVALUE;
        goto L7; // [108] 142
    }
    else{
        _5723 = NOVALUE;
    }

    /** 			if aLine[$] = '\n' then*/
    if (IS_SEQUENCE(_aLine_10057)){
            _5724 = SEQ_PTR(_aLine_10057)->length;
    }
    else {
        _5724 = 1;
    }
    _2 = (int)SEQ_PTR(_aLine_10057);
    _5725 = (int)*(((s1_ptr)_2)->base + _5724);
    if (_5725 != 10)
    goto L8; // [120] 141

    /** 				aLine = aLine[1 .. $-1]*/
    if (IS_SEQUENCE(_aLine_10057)){
            _5727 = SEQ_PTR(_aLine_10057)->length;
    }
    else {
        _5727 = 1;
    }
    _5728 = _5727 - 1;
    _5727 = NOVALUE;
    rhs_slice_target = (object_ptr)&_aLine_10057;
    RHS_Slice(_aLine_10057, 1, _5728);

    /** 				ifdef UNIX then*/
L8: 
L7: 

    /** 		res = call_func(proc, {aLine, line_no, user_data})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_aLine_10057);
    *((int *)(_2+4)) = _aLine_10057;
    *((int *)(_2+8)) = _line_no_10059;
    Ref(_user_data_10055);
    *((int *)(_2+12)) = _user_data_10055;
    _5730 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_5730);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_proc_10054].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12)
                         );
    DeRef(_res_10058);
    _res_10058 = _1;
    DeRefDS(_5730);
    _5730 = NOVALUE;

    /** 		if not equal(res, 0) then*/
    if (_res_10058 == 0)
    _5732 = 1;
    else if (IS_ATOM_INT(_res_10058) && IS_ATOM_INT(0))
    _5732 = 0;
    else
    _5732 = (compare(_res_10058, 0) == 0);
    if (_5732 != 0)
    goto L9; // [160] 168
    _5732 = NOVALUE;

    /** 			exit*/
    goto L6; // [165] 181
L9: 

    /** 	entry*/
L4: 

    /** 		aLine = gets(fh)*/
    DeRefi(_aLine_10057);
    _aLine_10057 = EGets(_fh_10056);

    /** 	end while*/
    goto L5; // [178] 87
L6: 

    /** 	if sequence(file) and length(file) != 0 then*/
    _5735 = IS_SEQUENCE(_file_10053);
    if (_5735 == 0) {
        goto LA; // [186] 206
    }
    if (IS_SEQUENCE(_file_10053)){
            _5737 = SEQ_PTR(_file_10053)->length;
    }
    else {
        _5737 = 1;
    }
    _5738 = (_5737 != 0);
    _5737 = NOVALUE;
    if (_5738 == 0)
    {
        DeRef(_5738);
        _5738 = NOVALUE;
        goto LA; // [198] 206
    }
    else{
        DeRef(_5738);
        _5738 = NOVALUE;
    }

    /** 		close(fh)*/
    EClose(_fh_10056);
LA: 

    /** 	return res*/
    DeRef(_file_10053);
    DeRef(_user_data_10055);
    DeRefi(_aLine_10057);
    _5725 = NOVALUE;
    DeRef(_5728);
    _5728 = NOVALUE;
    return _res_10058;
    ;
}


int _6write_lines(int _file_10095, int _lines_10096)
{
    int _fn_10097 = NOVALUE;
    int _5745 = NOVALUE;
    int _5744 = NOVALUE;
    int _5743 = NOVALUE;
    int _5739 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(file) then*/
    _5739 = IS_SEQUENCE(_file_10095);
    if (_5739 == 0)
    {
        _5739 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _5739 = NOVALUE;
    }

    /**     	fn = open(file, "w")*/
    DeRef(_fn_10097);
    _fn_10097 = EOpen(_file_10095, _5740, 0);
    goto L2; // [18] 27
L1: 

    /** 		fn = file*/
    Ref(_file_10095);
    DeRef(_fn_10097);
    _fn_10097 = _file_10095;
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (binary_op_a(GREATEREQ, _fn_10097, 0)){
        goto L3; // [31] 40
    }
    DeRef(_file_10095);
    DeRefDS(_lines_10096);
    DeRef(_fn_10097);
    return -1;
L3: 

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_10096)){
            _5743 = SEQ_PTR(_lines_10096)->length;
    }
    else {
        _5743 = 1;
    }
    {
        int _i_10106;
        _i_10106 = 1;
L4: 
        if (_i_10106 > _5743){
            goto L5; // [45] 73
        }

        /** 		puts(fn, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_10096);
        _5744 = (int)*(((s1_ptr)_2)->base + _i_10106);
        EPuts(_fn_10097, _5744); // DJP 
        _5744 = NOVALUE;

        /** 		puts(fn, '\n')*/
        EPuts(_fn_10097, 10); // DJP 

        /** 	end for*/
        _i_10106 = _i_10106 + 1;
        goto L4; // [68] 52
L5: 
        ;
    }

    /** 	if sequence(file) then*/
    _5745 = IS_SEQUENCE(_file_10095);
    if (_5745 == 0)
    {
        _5745 = NOVALUE;
        goto L6; // [78] 86
    }
    else{
        _5745 = NOVALUE;
    }

    /** 		close(fn)*/
    if (IS_ATOM_INT(_fn_10097))
    EClose(_fn_10097);
    else
    EClose((int)DBL_PTR(_fn_10097)->dbl);
L6: 

    /** 	return 1*/
    DeRef(_file_10095);
    DeRefDS(_lines_10096);
    DeRef(_fn_10097);
    return 1;
    ;
}


int _6append_lines(int _file_10113, int _lines_10114)
{
    int _fn_10115 = NOVALUE;
    int _5750 = NOVALUE;
    int _5749 = NOVALUE;
    int _0, _1, _2;
    

    /**   	fn = open(file, "a")*/
    _fn_10115 = EOpen(_file_10113, _5746, 0);

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_10115 >= 0)
    goto L1; // [14] 23
    DeRefDS(_file_10113);
    DeRefDS(_lines_10114);
    return -1;
L1: 

    /** 	for i = 1 to length(lines) do*/
    if (IS_SEQUENCE(_lines_10114)){
            _5749 = SEQ_PTR(_lines_10114)->length;
    }
    else {
        _5749 = 1;
    }
    {
        int _i_10121;
        _i_10121 = 1;
L2: 
        if (_i_10121 > _5749){
            goto L3; // [28] 56
        }

        /** 		puts(fn, lines[i])*/
        _2 = (int)SEQ_PTR(_lines_10114);
        _5750 = (int)*(((s1_ptr)_2)->base + _i_10121);
        EPuts(_fn_10115, _5750); // DJP 
        _5750 = NOVALUE;

        /** 		puts(fn, '\n')*/
        EPuts(_fn_10115, 10); // DJP 

        /** 	end for*/
        _i_10121 = _i_10121 + 1;
        goto L2; // [51] 35
L3: 
        ;
    }

    /** 	close(fn)*/
    EClose(_fn_10115);

    /** 	return 1*/
    DeRefDS(_file_10113);
    DeRefDS(_lines_10114);
    return 1;
    ;
}


int _6read_file(int _file_10133, int _as_text_10134)
{
    int _fn_10135 = NOVALUE;
    int _len_10136 = NOVALUE;
    int _ret_10137 = NOVALUE;
    int _seek_1__tmp_at49_10146 = NOVALUE;
    int _seek_inlined_seek_at_49_10145 = NOVALUE;
    int _where_inlined_where_at_64_10148 = NOVALUE;
    int _seek_1__tmp_at77_10151 = NOVALUE;
    int _seek_inlined_seek_at_77_10150 = NOVALUE;
    int _5773 = NOVALUE;
    int _5772 = NOVALUE;
    int _5770 = NOVALUE;
    int _5767 = NOVALUE;
    int _5761 = NOVALUE;
    int _5760 = NOVALUE;
    int _5759 = NOVALUE;
    int _5758 = NOVALUE;
    int _5754 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_as_text_10134)) {
        _1 = (long)(DBL_PTR(_as_text_10134)->dbl);
        if (UNIQUE(DBL_PTR(_as_text_10134)) && (DBL_PTR(_as_text_10134)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_as_text_10134);
        _as_text_10134 = _1;
    }

    /** 	if sequence(file) then*/
    _5754 = IS_SEQUENCE(_file_10133);
    if (_5754 == 0)
    {
        _5754 = NOVALUE;
        goto L1; // [10] 25
    }
    else{
        _5754 = NOVALUE;
    }

    /** 		fn = open(file, "rb")*/
    _fn_10135 = EOpen(_file_10133, _3811, 0);
    goto L2; // [22] 35
L1: 

    /** 		fn = file*/
    Ref(_file_10133);
    _fn_10135 = _file_10133;
    if (!IS_ATOM_INT(_fn_10135)) {
        _1 = (long)(DBL_PTR(_fn_10135)->dbl);
        if (UNIQUE(DBL_PTR(_fn_10135)) && (DBL_PTR(_fn_10135)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_10135);
        _fn_10135 = _1;
    }
L2: 

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_10135 >= 0)
    goto L3; // [39] 48
    DeRef(_file_10133);
    DeRef(_ret_10137);
    return -1;
L3: 

    /** 	seek(fn, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at49_10146);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_10135;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at49_10146 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_49_10145 = machine(19, _seek_1__tmp_at49_10146);
    DeRefi(_seek_1__tmp_at49_10146);
    _seek_1__tmp_at49_10146 = NOVALUE;

    /** 	len = where(fn)*/

    /** 	return machine_func(M_WHERE, fn)*/
    _len_10136 = machine(20, _fn_10135);
    if (!IS_ATOM_INT(_len_10136)) {
        _1 = (long)(DBL_PTR(_len_10136)->dbl);
        if (UNIQUE(DBL_PTR(_len_10136)) && (DBL_PTR(_len_10136)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_10136);
        _len_10136 = _1;
    }

    /** 	seek(fn, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at77_10151);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _fn_10135;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at77_10151 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_77_10150 = machine(19, _seek_1__tmp_at77_10151);
    DeRefi(_seek_1__tmp_at77_10151);
    _seek_1__tmp_at77_10151 = NOVALUE;

    /** 	ret = repeat(0, len)*/
    DeRef(_ret_10137);
    _ret_10137 = Repeat(0, _len_10136);

    /** 	for i = 1 to len do*/
    _5758 = _len_10136;
    {
        int _i_10154;
        _i_10154 = 1;
L4: 
        if (_i_10154 > _5758){
            goto L5; // [102] 125
        }

        /** 		ret[i] = getc(fn)*/
        if (_fn_10135 != last_r_file_no) {
            last_r_file_ptr = which_file(_fn_10135, EF_READ);
            last_r_file_no = _fn_10135;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _5759 = getKBchar();
            }
            else
            _5759 = getc(last_r_file_ptr);
        }
        else
        _5759 = getc(last_r_file_ptr);
        _2 = (int)SEQ_PTR(_ret_10137);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_10137 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_10154);
        _1 = *(int *)_2;
        *(int *)_2 = _5759;
        if( _1 != _5759 ){
            DeRef(_1);
        }
        _5759 = NOVALUE;

        /** 	end for*/
        _i_10154 = _i_10154 + 1;
        goto L4; // [120] 109
L5: 
        ;
    }

    /** 	if sequence(file) then*/
    _5760 = IS_SEQUENCE(_file_10133);
    if (_5760 == 0)
    {
        _5760 = NOVALUE;
        goto L6; // [130] 138
    }
    else{
        _5760 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_10135);
L6: 

    /** 	ifdef WINDOWS then*/

    /** 		for i = len to 1 by -1 do*/
    {
        int _i_10160;
        _i_10160 = _len_10136;
L7: 
        if (_i_10160 < 1){
            goto L8; // [142] 185
        }

        /** 			if ret[i] != -1 then*/
        _2 = (int)SEQ_PTR(_ret_10137);
        _5761 = (int)*(((s1_ptr)_2)->base + _i_10160);
        if (binary_op_a(EQUALS, _5761, -1)){
            _5761 = NOVALUE;
            goto L9; // [155] 178
        }
        _5761 = NOVALUE;

        /** 				if i != len then*/
        if (_i_10160 == _len_10136)
        goto L8; // [161] 185

        /** 					ret = ret[1 .. i]*/
        rhs_slice_target = (object_ptr)&_ret_10137;
        RHS_Slice(_ret_10137, 1, _i_10160);

        /** 				exit*/
        goto L8; // [175] 185
L9: 

        /** 		end for*/
        _i_10160 = _i_10160 + -1;
        goto L7; // [180] 149
L8: 
        ;
    }

    /** 	if as_text = BINARY_MODE then*/
    if (_as_text_10134 != 1)
    goto LA; // [189] 200

    /** 		return ret*/
    DeRef(_file_10133);
    return _ret_10137;
LA: 

    /** 	fn = find(26, ret) -- Any Ctrl-Z found?*/
    _fn_10135 = find_from(26, _ret_10137, 1);

    /** 	if fn then*/
    if (_fn_10135 == 0)
    {
        goto LB; // [211] 226
    }
    else{
    }

    /** 		ret = ret[1 .. fn - 1]*/
    _5767 = _fn_10135 - 1;
    rhs_slice_target = (object_ptr)&_ret_10137;
    RHS_Slice(_ret_10137, 1, _5767);
LB: 

    /** 	ret = search:match_replace({13,10}, ret, {10})*/
    RefDS(_3175);
    RefDS(_ret_10137);
    RefDS(_3169);
    _0 = _ret_10137;
    _ret_10137 = _14match_replace(_3175, _ret_10137, _3169, 0);
    DeRefDS(_0);

    /** 	if length(ret) > 0 then*/
    if (IS_SEQUENCE(_ret_10137)){
            _5770 = SEQ_PTR(_ret_10137)->length;
    }
    else {
        _5770 = 1;
    }
    if (_5770 <= 0)
    goto LC; // [242] 269

    /** 		if ret[$] != 10 then*/
    if (IS_SEQUENCE(_ret_10137)){
            _5772 = SEQ_PTR(_ret_10137)->length;
    }
    else {
        _5772 = 1;
    }
    _2 = (int)SEQ_PTR(_ret_10137);
    _5773 = (int)*(((s1_ptr)_2)->base + _5772);
    if (binary_op_a(EQUALS, _5773, 10)){
        _5773 = NOVALUE;
        goto LD; // [255] 277
    }
    _5773 = NOVALUE;

    /** 			ret &= 10*/
    Append(&_ret_10137, _ret_10137, 10);
    goto LD; // [266] 277
LC: 

    /** 		ret = {10}*/
    RefDS(_3169);
    DeRef(_ret_10137);
    _ret_10137 = _3169;
LD: 

    /** 	return ret*/
    DeRef(_file_10133);
    DeRef(_5767);
    _5767 = NOVALUE;
    return _ret_10137;
    ;
}


int _6write_file(int _file_10185, int _data_10186, int _as_text_10187)
{
    int _fn_10188 = NOVALUE;
    int _5798 = NOVALUE;
    int _5793 = NOVALUE;
    int _5783 = NOVALUE;
    int _5782 = NOVALUE;
    int _5780 = NOVALUE;
    int _5778 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_as_text_10187)) {
        _1 = (long)(DBL_PTR(_as_text_10187)->dbl);
        if (UNIQUE(DBL_PTR(_as_text_10187)) && (DBL_PTR(_as_text_10187)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_as_text_10187);
        _as_text_10187 = _1;
    }

    /** 	if as_text != BINARY_MODE then*/
    if (_as_text_10187 == 1)
    goto L1; // [11] 158

    /** 		fn = find(26, data)*/
    _fn_10188 = find_from(26, _data_10186, 1);

    /** 		if fn then*/
    if (_fn_10188 == 0)
    {
        goto L2; // [26] 41
    }
    else{
    }

    /** 			data = data[1 .. fn-1]*/
    _5778 = _fn_10188 - 1;
    rhs_slice_target = (object_ptr)&_data_10186;
    RHS_Slice(_data_10186, 1, _5778);
L2: 

    /** 		if length(data) > 0 then*/
    if (IS_SEQUENCE(_data_10186)){
            _5780 = SEQ_PTR(_data_10186)->length;
    }
    else {
        _5780 = 1;
    }
    if (_5780 <= 0)
    goto L3; // [46] 73

    /** 			if data[$] != 10 then*/
    if (IS_SEQUENCE(_data_10186)){
            _5782 = SEQ_PTR(_data_10186)->length;
    }
    else {
        _5782 = 1;
    }
    _2 = (int)SEQ_PTR(_data_10186);
    _5783 = (int)*(((s1_ptr)_2)->base + _5782);
    if (binary_op_a(EQUALS, _5783, 10)){
        _5783 = NOVALUE;
        goto L4; // [59] 81
    }
    _5783 = NOVALUE;

    /** 				data &= 10*/
    Append(&_data_10186, _data_10186, 10);
    goto L4; // [70] 81
L3: 

    /** 			data = {10}*/
    RefDS(_3169);
    DeRefDS(_data_10186);
    _data_10186 = _3169;
L4: 

    /** 		if as_text = TEXT_MODE then*/
    if (_as_text_10187 != 2)
    goto L5; // [85] 103

    /** 			data = search:match_replace({13,10}, data, {10})*/
    RefDS(_3175);
    RefDS(_data_10186);
    RefDS(_3169);
    _0 = _data_10186;
    _data_10186 = _14match_replace(_3175, _data_10186, _3169, 0);
    DeRefDS(_0);
    goto L6; // [100] 157
L5: 

    /** 		elsif as_text = UNIX_TEXT then*/
    if (_as_text_10187 != 3)
    goto L7; // [107] 125

    /** 			data = search:match_replace({13,10}, data, {10})*/
    RefDS(_3175);
    RefDS(_data_10186);
    RefDS(_3169);
    _0 = _data_10186;
    _data_10186 = _14match_replace(_3175, _data_10186, _3169, 0);
    DeRefDS(_0);
    goto L6; // [122] 157
L7: 

    /** 		elsif as_text = DOS_TEXT then*/
    if (_as_text_10187 != 4)
    goto L8; // [129] 156

    /** 			data = search:match_replace({13,10}, data, {10})*/
    RefDS(_3175);
    RefDS(_data_10186);
    RefDS(_3169);
    _0 = _data_10186;
    _data_10186 = _14match_replace(_3175, _data_10186, _3169, 0);
    DeRefDS(_0);

    /** 			data = search:match_replace({10}, data, {13,10})*/
    RefDS(_3169);
    RefDS(_data_10186);
    RefDS(_3175);
    _0 = _data_10186;
    _data_10186 = _14match_replace(_3169, _data_10186, _3175, 0);
    DeRefDS(_0);
L8: 
L6: 
L1: 

    /** 	if sequence(file) then*/
    _5793 = IS_SEQUENCE(_file_10185);
    if (_5793 == 0)
    {
        _5793 = NOVALUE;
        goto L9; // [163] 199
    }
    else{
        _5793 = NOVALUE;
    }

    /** 		if as_text = TEXT_MODE then*/
    if (_as_text_10187 != 2)
    goto LA; // [170] 186

    /** 			fn = open(file, "w")*/
    _fn_10188 = EOpen(_file_10185, _5740, 0);
    goto LB; // [183] 209
LA: 

    /** 			fn = open(file, "wb")*/
    _fn_10188 = EOpen(_file_10185, _3269, 0);
    goto LB; // [196] 209
L9: 

    /** 		fn = file*/
    Ref(_file_10185);
    _fn_10188 = _file_10185;
    if (!IS_ATOM_INT(_fn_10188)) {
        _1 = (long)(DBL_PTR(_fn_10188)->dbl);
        if (UNIQUE(DBL_PTR(_fn_10188)) && (DBL_PTR(_fn_10188)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fn_10188);
        _fn_10188 = _1;
    }
LB: 

    /** 	if fn < 0 then return -1 end if*/
    if (_fn_10188 >= 0)
    goto LC; // [213] 222
    DeRef(_file_10185);
    DeRefDS(_data_10186);
    DeRef(_5778);
    _5778 = NOVALUE;
    return -1;
LC: 

    /** 	puts(fn, data)*/
    EPuts(_fn_10188, _data_10186); // DJP 

    /** 	if sequence(file) then*/
    _5798 = IS_SEQUENCE(_file_10185);
    if (_5798 == 0)
    {
        _5798 = NOVALUE;
        goto LD; // [232] 240
    }
    else{
        _5798 = NOVALUE;
    }

    /** 		close(fn)*/
    EClose(_fn_10188);
LD: 

    /** 	return 1*/
    DeRef(_file_10185);
    DeRefDS(_data_10186);
    DeRef(_5778);
    _5778 = NOVALUE;
    return 1;
    ;
}


void _6writef(int _fm_10228, int _data_10229, int _fn_10230, int _data_not_string_10231)
{
    int _real_fn_10232 = NOVALUE;
    int _close_fn_10233 = NOVALUE;
    int _out_style_10234 = NOVALUE;
    int _ts_10237 = NOVALUE;
    int _msg_inlined_crash_at_169_10262 = NOVALUE;
    int _data_inlined_crash_at_166_10261 = NOVALUE;
    int _5818 = NOVALUE;
    int _5816 = NOVALUE;
    int _5815 = NOVALUE;
    int _5814 = NOVALUE;
    int _5808 = NOVALUE;
    int _5807 = NOVALUE;
    int _5806 = NOVALUE;
    int _5805 = NOVALUE;
    int _5804 = NOVALUE;
    int _5803 = NOVALUE;
    int _5801 = NOVALUE;
    int _5800 = NOVALUE;
    int _5799 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer real_fn = 0*/
    _real_fn_10232 = 0;

    /** 	integer close_fn = 0*/
    _close_fn_10233 = 0;

    /** 	sequence out_style = "w"*/
    RefDS(_5740);
    DeRefi(_out_style_10234);
    _out_style_10234 = _5740;

    /** 	if integer(fm) then*/
    if (IS_ATOM_INT(_fm_10228))
    _5799 = 1;
    else if (IS_ATOM_DBL(_fm_10228))
    _5799 = IS_ATOM_INT(DoubleToInt(_fm_10228));
    else
    _5799 = 0;
    if (_5799 == 0)
    {
        _5799 = NOVALUE;
        goto L1; // [27] 53
    }
    else{
        _5799 = NOVALUE;
    }

    /** 		object ts*/

    /** 		ts = fm*/
    Ref(_fm_10228);
    DeRef(_ts_10237);
    _ts_10237 = _fm_10228;

    /** 		fm = data*/
    Ref(_data_10229);
    DeRef(_fm_10228);
    _fm_10228 = _data_10229;

    /** 		data = fn*/
    Ref(_fn_10230);
    DeRef(_data_10229);
    _data_10229 = _fn_10230;

    /** 		fn = ts*/
    Ref(_ts_10237);
    DeRef(_fn_10230);
    _fn_10230 = _ts_10237;
L1: 
    DeRef(_ts_10237);
    _ts_10237 = NOVALUE;

    /** 	if sequence(fn) then*/
    _5800 = IS_SEQUENCE(_fn_10230);
    if (_5800 == 0)
    {
        _5800 = NOVALUE;
        goto L2; // [60] 199
    }
    else{
        _5800 = NOVALUE;
    }

    /** 		if length(fn) = 2 then*/
    if (IS_SEQUENCE(_fn_10230)){
            _5801 = SEQ_PTR(_fn_10230)->length;
    }
    else {
        _5801 = 1;
    }
    if (_5801 != 2)
    goto L3; // [68] 146

    /** 			if sequence(fn[1]) then*/
    _2 = (int)SEQ_PTR(_fn_10230);
    _5803 = (int)*(((s1_ptr)_2)->base + 1);
    _5804 = IS_SEQUENCE(_5803);
    _5803 = NOVALUE;
    if (_5804 == 0)
    {
        _5804 = NOVALUE;
        goto L4; // [81] 145
    }
    else{
        _5804 = NOVALUE;
    }

    /** 				if equal(fn[2], 'a') then*/
    _2 = (int)SEQ_PTR(_fn_10230);
    _5805 = (int)*(((s1_ptr)_2)->base + 2);
    if (_5805 == 97)
    _5806 = 1;
    else if (IS_ATOM_INT(_5805) && IS_ATOM_INT(97))
    _5806 = 0;
    else
    _5806 = (compare(_5805, 97) == 0);
    _5805 = NOVALUE;
    if (_5806 == 0)
    {
        _5806 = NOVALUE;
        goto L5; // [94] 107
    }
    else{
        _5806 = NOVALUE;
    }

    /** 					out_style = "a"*/
    RefDS(_5746);
    DeRefi(_out_style_10234);
    _out_style_10234 = _5746;
    goto L6; // [104] 138
L5: 

    /** 				elsif not equal(fn[2], "a") then*/
    _2 = (int)SEQ_PTR(_fn_10230);
    _5807 = (int)*(((s1_ptr)_2)->base + 2);
    if (_5807 == _5746)
    _5808 = 1;
    else if (IS_ATOM_INT(_5807) && IS_ATOM_INT(_5746))
    _5808 = 0;
    else
    _5808 = (compare(_5807, _5746) == 0);
    _5807 = NOVALUE;
    if (_5808 != 0)
    goto L7; // [117] 130
    _5808 = NOVALUE;

    /** 					out_style = "w"*/
    RefDS(_5740);
    DeRefi(_out_style_10234);
    _out_style_10234 = _5740;
    goto L6; // [127] 138
L7: 

    /** 					out_style = "a"*/
    RefDS(_5746);
    DeRefi(_out_style_10234);
    _out_style_10234 = _5746;
L6: 

    /** 				fn = fn[1]*/
    _0 = _fn_10230;
    _2 = (int)SEQ_PTR(_fn_10230);
    _fn_10230 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_fn_10230);
    DeRef(_0);
L4: 
L3: 

    /** 		real_fn = open(fn, out_style)*/
    _real_fn_10232 = EOpen(_fn_10230, _out_style_10234, 0);

    /** 		if real_fn = -1 then*/
    if (_real_fn_10232 != -1)
    goto L8; // [157] 189

    /** 			error:crash("Unable to write to '%s'", {fn})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_fn_10230);
    *((int *)(_2+4)) = _fn_10230;
    _5814 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_166_10261);
    _data_inlined_crash_at_166_10261 = _5814;
    _5814 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_169_10262);
    _msg_inlined_crash_at_169_10262 = EPrintf(-9999999, _5813, _data_inlined_crash_at_166_10261);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_169_10262);

    /** end procedure*/
    goto L9; // [183] 186
L9: 
    DeRef(_data_inlined_crash_at_166_10261);
    _data_inlined_crash_at_166_10261 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_169_10262);
    _msg_inlined_crash_at_169_10262 = NOVALUE;
L8: 

    /** 		close_fn = 1*/
    _close_fn_10233 = 1;
    goto LA; // [196] 209
L2: 

    /** 		real_fn = fn*/
    Ref(_fn_10230);
    _real_fn_10232 = _fn_10230;
    if (!IS_ATOM_INT(_real_fn_10232)) {
        _1 = (long)(DBL_PTR(_real_fn_10232)->dbl);
        if (UNIQUE(DBL_PTR(_real_fn_10232)) && (DBL_PTR(_real_fn_10232)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_real_fn_10232);
        _real_fn_10232 = _1;
    }
LA: 

    /** 	if equal(data_not_string, 0) then*/
    if (_data_not_string_10231 == 0)
    _5815 = 1;
    else if (IS_ATOM_INT(_data_not_string_10231) && IS_ATOM_INT(0))
    _5815 = 0;
    else
    _5815 = (compare(_data_not_string_10231, 0) == 0);
    if (_5815 == 0)
    {
        _5815 = NOVALUE;
        goto LB; // [215] 235
    }
    else{
        _5815 = NOVALUE;
    }

    /** 		if types:t_display(data) then*/
    Ref(_data_10229);
    _5816 = _11t_display(_data_10229);
    if (_5816 == 0) {
        DeRef(_5816);
        _5816 = NOVALUE;
        goto LC; // [224] 234
    }
    else {
        if (!IS_ATOM_INT(_5816) && DBL_PTR(_5816)->dbl == 0.0){
            DeRef(_5816);
            _5816 = NOVALUE;
            goto LC; // [224] 234
        }
        DeRef(_5816);
        _5816 = NOVALUE;
    }
    DeRef(_5816);
    _5816 = NOVALUE;

    /** 			data = {data}*/
    _0 = _data_10229;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_data_10229);
    *((int *)(_2+4)) = _data_10229;
    _data_10229 = MAKE_SEQ(_1);
    DeRef(_0);
LC: 
LB: 

    /**     puts(real_fn, text:format( fm, data ) )*/
    Ref(_fm_10228);
    Ref(_data_10229);
    _5818 = _12format(_fm_10228, _data_10229);
    EPuts(_real_fn_10232, _5818); // DJP 
    DeRef(_5818);
    _5818 = NOVALUE;

    /**     if close_fn then*/
    if (_close_fn_10233 == 0)
    {
        goto LD; // [247] 255
    }
    else{
    }

    /**     	close(real_fn)*/
    EClose(_real_fn_10232);
LD: 

    /** end procedure*/
    DeRef(_fm_10228);
    DeRef(_data_10229);
    DeRef(_fn_10230);
    DeRef(_data_not_string_10231);
    DeRefi(_out_style_10234);
    return;
    ;
}


void _6writefln(int _fm_10273, int _data_10274, int _fn_10275, int _data_not_string_10276)
{
    int _5821 = NOVALUE;
    int _5820 = NOVALUE;
    int _5819 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(fm) then*/
    if (IS_ATOM_INT(_fm_10273))
    _5819 = 1;
    else if (IS_ATOM_DBL(_fm_10273))
    _5819 = IS_ATOM_INT(DoubleToInt(_fm_10273));
    else
    _5819 = 0;
    if (_5819 == 0)
    {
        _5819 = NOVALUE;
        goto L1; // [6] 24
    }
    else{
        _5819 = NOVALUE;
    }

    /** 		writef(data & '\n', fn, fm, data_not_string)*/
    if (IS_SEQUENCE(_data_10274) && IS_ATOM(10)) {
        Append(&_5820, _data_10274, 10);
    }
    else if (IS_ATOM(_data_10274) && IS_SEQUENCE(10)) {
    }
    else {
        Concat((object_ptr)&_5820, _data_10274, 10);
    }
    Ref(_fn_10275);
    Ref(_fm_10273);
    Ref(_data_not_string_10276);
    _6writef(_5820, _fn_10275, _fm_10273, _data_not_string_10276);
    _5820 = NOVALUE;
    goto L2; // [21] 37
L1: 

    /** 		writef(fm & '\n', data, fn, data_not_string)*/
    if (IS_SEQUENCE(_fm_10273) && IS_ATOM(10)) {
        Append(&_5821, _fm_10273, 10);
    }
    else if (IS_ATOM(_fm_10273) && IS_SEQUENCE(10)) {
    }
    else {
        Concat((object_ptr)&_5821, _fm_10273, 10);
    }
    Ref(_data_10274);
    Ref(_fn_10275);
    Ref(_data_not_string_10276);
    _6writef(_5821, _data_10274, _fn_10275, _data_not_string_10276);
    _5821 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_fm_10273);
    DeRef(_data_10274);
    DeRef(_fn_10275);
    DeRef(_data_not_string_10276);
    return;
    ;
}



// 0x2F20F043
