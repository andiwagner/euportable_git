// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _33regex(int _o_15759)
{
    int _9104 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return sequence(o)*/
    _9104 = IS_SEQUENCE(_o_15759);
    DeRef(_o_15759);
    return _9104;
    ;
}


int _33option_spec(int _o_15763)
{
    int _9111 = NOVALUE;
    int _9110 = NOVALUE;
    int _9108 = NOVALUE;
    int _9106 = NOVALUE;
    int _9105 = NOVALUE;
    int _0, _1, _2;
    
L1: 

    /** 	if atom(o) then*/
    _9105 = IS_ATOM(_o_15763);
    if (_9105 == 0)
    {
        _9105 = NOVALUE;
        goto L2; // [6] 60
    }
    else{
        _9105 = NOVALUE;
    }

    /** 		if not integer(o) then*/
    if (IS_ATOM_INT(_o_15763))
    _9106 = 1;
    else if (IS_ATOM_DBL(_o_15763))
    _9106 = IS_ATOM_INT(DoubleToInt(_o_15763));
    else
    _9106 = 0;
    if (_9106 != 0)
    goto L3; // [14] 26
    _9106 = NOVALUE;

    /** 			return 0*/
    DeRef(_o_15763);
    return 0;
    goto L4; // [23] 89
L3: 

    /** 			if (or_bits(o,all_options) != all_options) then*/
    if (IS_ATOM_INT(_o_15763) && IS_ATOM_INT(_33all_options_15754)) {
        {unsigned long tu;
             tu = (unsigned long)_o_15763 | (unsigned long)_33all_options_15754;
             _9108 = MAKE_UINT(tu);
        }
    }
    else {
        _9108 = binary_op(OR_BITS, _o_15763, _33all_options_15754);
    }
    if (binary_op_a(EQUALS, _9108, _33all_options_15754)){
        DeRef(_9108);
        _9108 = NOVALUE;
        goto L5; // [36] 49
    }
    DeRef(_9108);
    _9108 = NOVALUE;

    /** 				return 0*/
    DeRef(_o_15763);
    return 0;
    goto L4; // [46] 89
L5: 

    /** 				return 1*/
    DeRef(_o_15763);
    return 1;
    goto L4; // [57] 89
L2: 

    /** 	elsif integer_array(o) then*/
    Ref(_o_15763);
    _9110 = _11integer_array(_o_15763);
    if (_9110 == 0) {
        DeRef(_9110);
        _9110 = NOVALUE;
        goto L6; // [66] 82
    }
    else {
        if (!IS_ATOM_INT(_9110) && DBL_PTR(_9110)->dbl == 0.0){
            DeRef(_9110);
            _9110 = NOVALUE;
            goto L6; // [66] 82
        }
        DeRef(_9110);
        _9110 = NOVALUE;
    }
    DeRef(_9110);
    _9110 = NOVALUE;

    /** 		return option_spec( math:or_all(o) )*/
    Ref(_o_15763);
    _9111 = _18or_all(_o_15763);
    _0 = _o_15763;
    _o_15763 = _9111;
    Ref(_o_15763);
    DeRef(_0);
    DeRef(_9111);
    _9111 = NOVALUE;
    goto L1; // [75] 1
    goto L4; // [79] 89
L6: 

    /** 		return 0*/
    DeRef(_o_15763);
    return 0;
L4: 
    ;
}


int _33option_spec_to_string(int _o_15782)
{
    int _9113 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return flags:flags_to_string(o, option_names)*/
    Ref(_o_15782);
    RefDS(_33option_names_15605);
    _9113 = _34flags_to_string(_o_15782, _33option_names_15605, 0);
    DeRef(_o_15782);
    return _9113;
    ;
}


int _33error_to_string(int _i_15786)
{
    int _9120 = NOVALUE;
    int _9118 = NOVALUE;
    int _9117 = NOVALUE;
    int _9116 = NOVALUE;
    int _9114 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_i_15786)) {
        _1 = (long)(DBL_PTR(_i_15786)->dbl);
        if (UNIQUE(DBL_PTR(_i_15786)) && (DBL_PTR(_i_15786)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_15786);
        _i_15786 = _1;
    }

    /** 	if i >= 0 or i < -23 then*/
    _9114 = (_i_15786 >= 0);
    if (_9114 != 0) {
        goto L1; // [11] 24
    }
    _9116 = (_i_15786 < -23);
    if (_9116 == 0)
    {
        DeRef(_9116);
        _9116 = NOVALUE;
        goto L2; // [20] 41
    }
    else{
        DeRef(_9116);
        _9116 = NOVALUE;
    }
L1: 

    /** 		return sprintf("%d",{i})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _i_15786;
    _9117 = MAKE_SEQ(_1);
    _9118 = EPrintf(-9999999, _952, _9117);
    DeRefDS(_9117);
    _9117 = NOVALUE;
    DeRef(_9114);
    _9114 = NOVALUE;
    return _9118;
    goto L3; // [38] 58
L2: 

    /** 		return search:vlookup(i, error_names, 1, 2, "Unknown Error")*/
    RefDS(_33error_names_15705);
    RefDS(_9119);
    _9120 = _14vlookup(_i_15786, _33error_names_15705, 1, 2, _9119);
    DeRef(_9114);
    _9114 = NOVALUE;
    DeRef(_9118);
    _9118 = NOVALUE;
    return _9120;
L3: 
    ;
}


int _33new(int _pattern_15799, int _options_15800)
{
    int _9124 = NOVALUE;
    int _9123 = NOVALUE;
    int _9121 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(options) then */
    _9121 = IS_SEQUENCE(_options_15800);
    if (_9121 == 0)
    {
        _9121 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _9121 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_15800);
    _0 = _options_15800;
    _options_15800 = _18or_all(_options_15800);
    DeRef(_0);
L1: 

    /** 	return machine_func(M_PCRE_COMPILE, { pattern, options })*/
    Ref(_options_15800);
    Ref(_pattern_15799);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _pattern_15799;
    ((int *)_2)[2] = _options_15800;
    _9123 = MAKE_SEQ(_1);
    _9124 = machine(68, _9123);
    DeRefDS(_9123);
    _9123 = NOVALUE;
    DeRef(_pattern_15799);
    DeRef(_options_15800);
    return _9124;
    ;
}


int _33error_message(int _re_15808)
{
    int _9126 = NOVALUE;
    int _9125 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_PCRE_ERROR_MESSAGE, { re })*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_15808);
    *((int *)(_2+4)) = _re_15808;
    _9125 = MAKE_SEQ(_1);
    _9126 = machine(95, _9125);
    DeRefDS(_9125);
    _9125 = NOVALUE;
    DeRef(_re_15808);
    return _9126;
    ;
}


int _33escape(int _s_15814)
{
    int _9128 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return text:escape(s, ".\\+*?[^]$(){}=!<>|:-")*/
    Ref(_s_15814);
    RefDS(_9127);
    _9128 = _12escape(_s_15814, _9127);
    DeRef(_s_15814);
    return _9128;
    ;
}


int _33get_ovector_size(int _ex_15819, int _maxsize_15820)
{
    int _m_15821 = NOVALUE;
    int _9132 = NOVALUE;
    int _9129 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_maxsize_15820)) {
        _1 = (long)(DBL_PTR(_maxsize_15820)->dbl);
        if (UNIQUE(DBL_PTR(_maxsize_15820)) && (DBL_PTR(_maxsize_15820)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxsize_15820);
        _maxsize_15820 = _1;
    }

    /** 	integer m = machine_func(M_PCRE_GET_OVECTOR_SIZE, {ex})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_15819);
    *((int *)(_2+4)) = _ex_15819;
    _9129 = MAKE_SEQ(_1);
    _m_15821 = machine(97, _9129);
    DeRefDS(_9129);
    _9129 = NOVALUE;
    if (!IS_ATOM_INT(_m_15821)) {
        _1 = (long)(DBL_PTR(_m_15821)->dbl);
        if (UNIQUE(DBL_PTR(_m_15821)) && (DBL_PTR(_m_15821)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_m_15821);
        _m_15821 = _1;
    }

    /** 	if (m > maxsize) then*/
    if (_m_15821 <= _maxsize_15820)
    goto L1; // [23] 34

    /** 		return maxsize*/
    DeRef(_ex_15819);
    return _maxsize_15820;
L1: 

    /** 	return m+1*/
    _9132 = _m_15821 + 1;
    if (_9132 > MAXINT){
        _9132 = NewDouble((double)_9132);
    }
    DeRef(_ex_15819);
    return _9132;
    ;
}


int _33find(int _re_15829, int _haystack_15831, int _from_15832, int _options_15833, int _size_15834)
{
    int _9139 = NOVALUE;
    int _9138 = NOVALUE;
    int _9137 = NOVALUE;
    int _9134 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_15832)) {
        _1 = (long)(DBL_PTR(_from_15832)->dbl);
        if (UNIQUE(DBL_PTR(_from_15832)) && (DBL_PTR(_from_15832)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_15832);
        _from_15832 = _1;
    }
    if (!IS_ATOM_INT(_size_15834)) {
        _1 = (long)(DBL_PTR(_size_15834)->dbl);
        if (UNIQUE(DBL_PTR(_size_15834)) && (DBL_PTR(_size_15834)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_15834);
        _size_15834 = _1;
    }

    /** 	if sequence(options) then */
    _9134 = IS_SEQUENCE(_options_15833);
    if (_9134 == 0)
    {
        _9134 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _9134 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_15833);
    _0 = _options_15833;
    _options_15833 = _18or_all(_options_15833);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_15834 >= 0)
    goto L2; // [26] 38

    /** 		size = 0*/
    _size_15834 = 0;
L2: 

    /** 	return machine_func(M_PCRE_EXEC, { re, haystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_15831)){
            _9137 = SEQ_PTR(_haystack_15831)->length;
    }
    else {
        _9137 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_15829);
    *((int *)(_2+4)) = _re_15829;
    Ref(_haystack_15831);
    *((int *)(_2+8)) = _haystack_15831;
    *((int *)(_2+12)) = _9137;
    Ref(_options_15833);
    *((int *)(_2+16)) = _options_15833;
    *((int *)(_2+20)) = _from_15832;
    *((int *)(_2+24)) = _size_15834;
    _9138 = MAKE_SEQ(_1);
    _9137 = NOVALUE;
    _9139 = machine(70, _9138);
    DeRefDS(_9138);
    _9138 = NOVALUE;
    DeRef(_re_15829);
    DeRef(_haystack_15831);
    DeRef(_options_15833);
    return _9139;
    ;
}


int _33find_all(int _re_15846, int _haystack_15848, int _from_15849, int _options_15850, int _size_15851)
{
    int _result_15858 = NOVALUE;
    int _results_15859 = NOVALUE;
    int _pHaystack_15860 = NOVALUE;
    int _9152 = NOVALUE;
    int _9151 = NOVALUE;
    int _9149 = NOVALUE;
    int _9147 = NOVALUE;
    int _9145 = NOVALUE;
    int _9141 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_15849)) {
        _1 = (long)(DBL_PTR(_from_15849)->dbl);
        if (UNIQUE(DBL_PTR(_from_15849)) && (DBL_PTR(_from_15849)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_15849);
        _from_15849 = _1;
    }
    if (!IS_ATOM_INT(_size_15851)) {
        _1 = (long)(DBL_PTR(_size_15851)->dbl);
        if (UNIQUE(DBL_PTR(_size_15851)) && (DBL_PTR(_size_15851)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_15851);
        _size_15851 = _1;
    }

    /** 	if sequence(options) then */
    _9141 = IS_SEQUENCE(_options_15850);
    if (_9141 == 0)
    {
        _9141 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _9141 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_15850);
    _0 = _options_15850;
    _options_15850 = _18or_all(_options_15850);
    DeRef(_0);
L1: 

    /** 	if size < 0 then*/
    if (_size_15851 >= 0)
    goto L2; // [26] 38

    /** 		size = 0*/
    _size_15851 = 0;
L2: 

    /** 	object result*/

    /** 	sequence results = {}*/
    RefDS(_5);
    DeRef(_results_15859);
    _results_15859 = _5;

    /** 	atom pHaystack = machine:allocate_string(haystack)*/
    Ref(_haystack_15848);
    _0 = _pHaystack_15860;
    _pHaystack_15860 = _7allocate_string(_haystack_15848, 0);
    DeRef(_0);

    /** 	while sequence(result) with entry do*/
    goto L3; // [56] 102
L4: 
    _9145 = IS_SEQUENCE(_result_15858);
    if (_9145 == 0)
    {
        _9145 = NOVALUE;
        goto L5; // [64] 127
    }
    else{
        _9145 = NOVALUE;
    }

    /** 		results = append(results, result)*/
    Ref(_result_15858);
    Append(&_results_15859, _results_15859, _result_15858);

    /** 		from = math:max(result) + 1*/
    Ref(_result_15858);
    _9147 = _18max(_result_15858);
    if (IS_ATOM_INT(_9147)) {
        _from_15849 = _9147 + 1;
    }
    else
    { // coercing _from_15849 to an integer 1
        _from_15849 = 1+(long)(DBL_PTR(_9147)->dbl);
        if( !IS_ATOM_INT(_from_15849) ){
            _from_15849 = (object)DBL_PTR(_from_15849)->dbl;
        }
    }
    DeRef(_9147);
    _9147 = NOVALUE;

    /** 		if from > length(haystack) then*/
    if (IS_SEQUENCE(_haystack_15848)){
            _9149 = SEQ_PTR(_haystack_15848)->length;
    }
    else {
        _9149 = 1;
    }
    if (_from_15849 <= _9149)
    goto L6; // [90] 99

    /** 			exit*/
    goto L5; // [96] 127
L6: 

    /** 	entry*/
L3: 

    /** 		result = machine_func(M_PCRE_EXEC, { re, pHaystack, length(haystack), options, from, size })*/
    if (IS_SEQUENCE(_haystack_15848)){
            _9151 = SEQ_PTR(_haystack_15848)->length;
    }
    else {
        _9151 = 1;
    }
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_re_15846);
    *((int *)(_2+4)) = _re_15846;
    Ref(_pHaystack_15860);
    *((int *)(_2+8)) = _pHaystack_15860;
    *((int *)(_2+12)) = _9151;
    Ref(_options_15850);
    *((int *)(_2+16)) = _options_15850;
    *((int *)(_2+20)) = _from_15849;
    *((int *)(_2+24)) = _size_15851;
    _9152 = MAKE_SEQ(_1);
    _9151 = NOVALUE;
    DeRef(_result_15858);
    _result_15858 = machine(70, _9152);
    DeRefDS(_9152);
    _9152 = NOVALUE;

    /** 	end while*/
    goto L4; // [124] 59
L5: 

    /** 	machine:free(pHaystack)*/
    Ref(_pHaystack_15860);
    _7free(_pHaystack_15860);

    /** 	return results*/
    DeRef(_re_15846);
    DeRef(_haystack_15848);
    DeRef(_options_15850);
    DeRef(_result_15858);
    DeRef(_pHaystack_15860);
    return _results_15859;
    ;
}


int _33has_match(int _re_15875, int _haystack_15877, int _from_15878, int _options_15879)
{
    int _9156 = NOVALUE;
    int _9155 = NOVALUE;
    int _9154 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_15878)) {
        _1 = (long)(DBL_PTR(_from_15878)->dbl);
        if (UNIQUE(DBL_PTR(_from_15878)) && (DBL_PTR(_from_15878)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_15878);
        _from_15878 = _1;
    }

    /** 	return sequence(find(re, haystack, from, options))*/
    Ref(_re_15875);
    _9154 = _33get_ovector_size(_re_15875, 30);
    Ref(_re_15875);
    Ref(_haystack_15877);
    Ref(_options_15879);
    _9155 = _33find(_re_15875, _haystack_15877, _from_15878, _options_15879, _9154);
    _9154 = NOVALUE;
    _9156 = IS_SEQUENCE(_9155);
    DeRef(_9155);
    _9155 = NOVALUE;
    DeRef(_re_15875);
    DeRef(_haystack_15877);
    DeRef(_options_15879);
    return _9156;
    ;
}


int _33is_match(int _re_15885, int _haystack_15887, int _from_15888, int _options_15889)
{
    int _m_15890 = NOVALUE;
    int _9171 = NOVALUE;
    int _9170 = NOVALUE;
    int _9169 = NOVALUE;
    int _9168 = NOVALUE;
    int _9167 = NOVALUE;
    int _9166 = NOVALUE;
    int _9165 = NOVALUE;
    int _9164 = NOVALUE;
    int _9163 = NOVALUE;
    int _9162 = NOVALUE;
    int _9161 = NOVALUE;
    int _9160 = NOVALUE;
    int _9159 = NOVALUE;
    int _9157 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_15888)) {
        _1 = (long)(DBL_PTR(_from_15888)->dbl);
        if (UNIQUE(DBL_PTR(_from_15888)) && (DBL_PTR(_from_15888)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_15888);
        _from_15888 = _1;
    }

    /** 	object m = find(re, haystack, from, options)*/
    Ref(_re_15885);
    _9157 = _33get_ovector_size(_re_15885, 30);
    Ref(_re_15885);
    Ref(_haystack_15887);
    Ref(_options_15889);
    _0 = _m_15890;
    _m_15890 = _33find(_re_15885, _haystack_15887, _from_15888, _options_15889, _9157);
    DeRef(_0);
    _9157 = NOVALUE;

    /** 	if sequence(m) and length(m) > 0 and m[1][1] = from and m[1][2] = length(haystack) then*/
    _9159 = IS_SEQUENCE(_m_15890);
    if (_9159 == 0) {
        _9160 = 0;
        goto L1; // [25] 40
    }
    if (IS_SEQUENCE(_m_15890)){
            _9161 = SEQ_PTR(_m_15890)->length;
    }
    else {
        _9161 = 1;
    }
    _9162 = (_9161 > 0);
    _9161 = NOVALUE;
    _9160 = (_9162 != 0);
L1: 
    if (_9160 == 0) {
        _9163 = 0;
        goto L2; // [40] 60
    }
    _2 = (int)SEQ_PTR(_m_15890);
    _9164 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_9164);
    _9165 = (int)*(((s1_ptr)_2)->base + 1);
    _9164 = NOVALUE;
    if (IS_ATOM_INT(_9165)) {
        _9166 = (_9165 == _from_15888);
    }
    else {
        _9166 = binary_op(EQUALS, _9165, _from_15888);
    }
    _9165 = NOVALUE;
    if (IS_ATOM_INT(_9166))
    _9163 = (_9166 != 0);
    else
    _9163 = DBL_PTR(_9166)->dbl != 0.0;
L2: 
    if (_9163 == 0) {
        goto L3; // [60] 90
    }
    _2 = (int)SEQ_PTR(_m_15890);
    _9168 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_9168);
    _9169 = (int)*(((s1_ptr)_2)->base + 2);
    _9168 = NOVALUE;
    if (IS_SEQUENCE(_haystack_15887)){
            _9170 = SEQ_PTR(_haystack_15887)->length;
    }
    else {
        _9170 = 1;
    }
    if (IS_ATOM_INT(_9169)) {
        _9171 = (_9169 == _9170);
    }
    else {
        _9171 = binary_op(EQUALS, _9169, _9170);
    }
    _9169 = NOVALUE;
    _9170 = NOVALUE;
    if (_9171 == 0) {
        DeRef(_9171);
        _9171 = NOVALUE;
        goto L3; // [80] 90
    }
    else {
        if (!IS_ATOM_INT(_9171) && DBL_PTR(_9171)->dbl == 0.0){
            DeRef(_9171);
            _9171 = NOVALUE;
            goto L3; // [80] 90
        }
        DeRef(_9171);
        _9171 = NOVALUE;
    }
    DeRef(_9171);
    _9171 = NOVALUE;

    /** 		return 1*/
    DeRef(_re_15885);
    DeRef(_haystack_15887);
    DeRef(_options_15889);
    DeRef(_m_15890);
    DeRef(_9162);
    _9162 = NOVALUE;
    DeRef(_9166);
    _9166 = NOVALUE;
    return 1;
L3: 

    /** 	return 0*/
    DeRef(_re_15885);
    DeRef(_haystack_15887);
    DeRef(_options_15889);
    DeRef(_m_15890);
    DeRef(_9162);
    _9162 = NOVALUE;
    DeRef(_9166);
    _9166 = NOVALUE;
    return 0;
    ;
}


int _33matches(int _re_15909, int _haystack_15911, int _from_15912, int _options_15913)
{
    int _str_offsets_15917 = NOVALUE;
    int _match_data_15919 = NOVALUE;
    int _tmp_15929 = NOVALUE;
    int _9193 = NOVALUE;
    int _9192 = NOVALUE;
    int _9191 = NOVALUE;
    int _9190 = NOVALUE;
    int _9189 = NOVALUE;
    int _9187 = NOVALUE;
    int _9186 = NOVALUE;
    int _9185 = NOVALUE;
    int _9184 = NOVALUE;
    int _9182 = NOVALUE;
    int _9181 = NOVALUE;
    int _9180 = NOVALUE;
    int _9179 = NOVALUE;
    int _9177 = NOVALUE;
    int _9176 = NOVALUE;
    int _9175 = NOVALUE;
    int _9172 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_15912)) {
        _1 = (long)(DBL_PTR(_from_15912)->dbl);
        if (UNIQUE(DBL_PTR(_from_15912)) && (DBL_PTR(_from_15912)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_15912);
        _from_15912 = _1;
    }

    /** 	if sequence(options) then */
    _9172 = IS_SEQUENCE(_options_15913);
    if (_9172 == 0)
    {
        _9172 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _9172 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_15913);
    _0 = _options_15913;
    _options_15913 = _18or_all(_options_15913);
    DeRef(_0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_15913)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_15913;
             _str_offsets_15917 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_15917 = binary_op(AND_BITS, 201326592, _options_15913);
    }
    if (!IS_ATOM_INT(_str_offsets_15917)) {
        _1 = (long)(DBL_PTR(_str_offsets_15917)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_15917)) && (DBL_PTR(_str_offsets_15917)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_15917);
        _str_offsets_15917 = _1;
    }

    /** 	object match_data = find(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _9175 = not_bits(201326592);
    if (IS_ATOM_INT(_options_15913) && IS_ATOM_INT(_9175)) {
        {unsigned long tu;
             tu = (unsigned long)_options_15913 & (unsigned long)_9175;
             _9176 = MAKE_UINT(tu);
        }
    }
    else {
        _9176 = binary_op(AND_BITS, _options_15913, _9175);
    }
    DeRef(_9175);
    _9175 = NOVALUE;
    Ref(_re_15909);
    _9177 = _33get_ovector_size(_re_15909, 30);
    Ref(_re_15909);
    Ref(_haystack_15911);
    _0 = _match_data_15919;
    _match_data_15919 = _33find(_re_15909, _haystack_15911, _from_15912, _9176, _9177);
    DeRef(_0);
    _9176 = NOVALUE;
    _9177 = NOVALUE;

    /** 	if atom(match_data) then */
    _9179 = IS_ATOM(_match_data_15919);
    if (_9179 == 0)
    {
        _9179 = NOVALUE;
        goto L2; // [57] 67
    }
    else{
        _9179 = NOVALUE;
    }

    /** 		return ERROR_NOMATCH */
    DeRef(_re_15909);
    DeRef(_haystack_15911);
    DeRef(_options_15913);
    DeRef(_match_data_15919);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_15919)){
            _9180 = SEQ_PTR(_match_data_15919)->length;
    }
    else {
        _9180 = 1;
    }
    {
        int _i_15927;
        _i_15927 = 1;
L3: 
        if (_i_15927 > _9180){
            goto L4; // [72] 185
        }

        /** 		sequence tmp*/

        /** 		if match_data[i][1] = 0 then*/
        _2 = (int)SEQ_PTR(_match_data_15919);
        _9181 = (int)*(((s1_ptr)_2)->base + _i_15927);
        _2 = (int)SEQ_PTR(_9181);
        _9182 = (int)*(((s1_ptr)_2)->base + 1);
        _9181 = NOVALUE;
        if (binary_op_a(NOTEQ, _9182, 0)){
            _9182 = NOVALUE;
            goto L5; // [91] 105
        }
        _9182 = NOVALUE;

        /** 			tmp = ""*/
        RefDS(_5);
        DeRef(_tmp_15929);
        _tmp_15929 = _5;
        goto L6; // [102] 129
L5: 

        /** 			tmp = haystack[match_data[i][1]..match_data[i][2]]*/
        _2 = (int)SEQ_PTR(_match_data_15919);
        _9184 = (int)*(((s1_ptr)_2)->base + _i_15927);
        _2 = (int)SEQ_PTR(_9184);
        _9185 = (int)*(((s1_ptr)_2)->base + 1);
        _9184 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_15919);
        _9186 = (int)*(((s1_ptr)_2)->base + _i_15927);
        _2 = (int)SEQ_PTR(_9186);
        _9187 = (int)*(((s1_ptr)_2)->base + 2);
        _9186 = NOVALUE;
        rhs_slice_target = (object_ptr)&_tmp_15929;
        RHS_Slice(_haystack_15911, _9185, _9187);
L6: 

        /** 		if str_offsets then*/
        if (_str_offsets_15917 == 0)
        {
            goto L7; // [131] 167
        }
        else{
        }

        /** 			match_data[i] = { tmp, match_data[i][1], match_data[i][2] }*/
        _2 = (int)SEQ_PTR(_match_data_15919);
        _9189 = (int)*(((s1_ptr)_2)->base + _i_15927);
        _2 = (int)SEQ_PTR(_9189);
        _9190 = (int)*(((s1_ptr)_2)->base + 1);
        _9189 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_15919);
        _9191 = (int)*(((s1_ptr)_2)->base + _i_15927);
        _2 = (int)SEQ_PTR(_9191);
        _9192 = (int)*(((s1_ptr)_2)->base + 2);
        _9191 = NOVALUE;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_tmp_15929);
        *((int *)(_2+4)) = _tmp_15929;
        Ref(_9190);
        *((int *)(_2+8)) = _9190;
        Ref(_9192);
        *((int *)(_2+12)) = _9192;
        _9193 = MAKE_SEQ(_1);
        _9192 = NOVALUE;
        _9190 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_15919);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_15919 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15927);
        _1 = *(int *)_2;
        *(int *)_2 = _9193;
        if( _1 != _9193 ){
            DeRef(_1);
        }
        _9193 = NOVALUE;
        goto L8; // [164] 176
L7: 

        /** 			match_data[i] = tmp*/
        RefDS(_tmp_15929);
        _2 = (int)SEQ_PTR(_match_data_15919);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _match_data_15919 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15927);
        _1 = *(int *)_2;
        *(int *)_2 = _tmp_15929;
        DeRef(_1);
L8: 
        DeRef(_tmp_15929);
        _tmp_15929 = NOVALUE;

        /** 	end for*/
        _i_15927 = _i_15927 + 1;
        goto L3; // [180] 79
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_15909);
    DeRef(_haystack_15911);
    DeRef(_options_15913);
    _9185 = NOVALUE;
    _9187 = NOVALUE;
    return _match_data_15919;
    ;
}


int _33all_matches(int _re_15949, int _haystack_15951, int _from_15952, int _options_15953)
{
    int _str_offsets_15957 = NOVALUE;
    int _match_data_15959 = NOVALUE;
    int _tmp_15974 = NOVALUE;
    int _a_15975 = NOVALUE;
    int _b_15976 = NOVALUE;
    int _9217 = NOVALUE;
    int _9216 = NOVALUE;
    int _9214 = NOVALUE;
    int _9211 = NOVALUE;
    int _9210 = NOVALUE;
    int _9207 = NOVALUE;
    int _9206 = NOVALUE;
    int _9205 = NOVALUE;
    int _9204 = NOVALUE;
    int _9203 = NOVALUE;
    int _9201 = NOVALUE;
    int _9199 = NOVALUE;
    int _9198 = NOVALUE;
    int _9197 = NOVALUE;
    int _9194 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_from_15952)) {
        _1 = (long)(DBL_PTR(_from_15952)->dbl);
        if (UNIQUE(DBL_PTR(_from_15952)) && (DBL_PTR(_from_15952)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_15952);
        _from_15952 = _1;
    }

    /** 	if sequence(options) then */
    _9194 = IS_SEQUENCE(_options_15953);
    if (_9194 == 0)
    {
        _9194 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _9194 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_15953);
    _0 = _options_15953;
    _options_15953 = _18or_all(_options_15953);
    DeRef(_0);
L1: 

    /** 	integer str_offsets = and_bits(STRING_OFFSETS, options)*/
    if (IS_ATOM_INT(_options_15953)) {
        {unsigned long tu;
             tu = (unsigned long)201326592 & (unsigned long)_options_15953;
             _str_offsets_15957 = MAKE_UINT(tu);
        }
    }
    else {
        _str_offsets_15957 = binary_op(AND_BITS, 201326592, _options_15953);
    }
    if (!IS_ATOM_INT(_str_offsets_15957)) {
        _1 = (long)(DBL_PTR(_str_offsets_15957)->dbl);
        if (UNIQUE(DBL_PTR(_str_offsets_15957)) && (DBL_PTR(_str_offsets_15957)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_str_offsets_15957);
        _str_offsets_15957 = _1;
    }

    /** 	object match_data = find_all(re, haystack, from, and_bits(options, not_bits(STRING_OFFSETS)))*/
    _9197 = not_bits(201326592);
    if (IS_ATOM_INT(_options_15953) && IS_ATOM_INT(_9197)) {
        {unsigned long tu;
             tu = (unsigned long)_options_15953 & (unsigned long)_9197;
             _9198 = MAKE_UINT(tu);
        }
    }
    else {
        _9198 = binary_op(AND_BITS, _options_15953, _9197);
    }
    DeRef(_9197);
    _9197 = NOVALUE;
    Ref(_re_15949);
    _9199 = _33get_ovector_size(_re_15949, 30);
    Ref(_re_15949);
    Ref(_haystack_15951);
    _0 = _match_data_15959;
    _match_data_15959 = _33find_all(_re_15949, _haystack_15951, _from_15952, _9198, _9199);
    DeRef(_0);
    _9198 = NOVALUE;
    _9199 = NOVALUE;

    /** 	if length(match_data) = 0 then */
    if (IS_SEQUENCE(_match_data_15959)){
            _9201 = SEQ_PTR(_match_data_15959)->length;
    }
    else {
        _9201 = 1;
    }
    if (_9201 != 0)
    goto L2; // [57] 68

    /** 		return ERROR_NOMATCH */
    DeRef(_re_15949);
    DeRef(_haystack_15951);
    DeRef(_options_15953);
    DeRef(_match_data_15959);
    return -1;
L2: 

    /** 	for i = 1 to length(match_data) do*/
    if (IS_SEQUENCE(_match_data_15959)){
            _9203 = SEQ_PTR(_match_data_15959)->length;
    }
    else {
        _9203 = 1;
    }
    {
        int _i_15968;
        _i_15968 = 1;
L3: 
        if (_i_15968 > _9203){
            goto L4; // [73] 219
        }

        /** 		for j = 1 to length(match_data[i]) do*/
        _2 = (int)SEQ_PTR(_match_data_15959);
        _9204 = (int)*(((s1_ptr)_2)->base + _i_15968);
        if (IS_SEQUENCE(_9204)){
                _9205 = SEQ_PTR(_9204)->length;
        }
        else {
            _9205 = 1;
        }
        _9204 = NOVALUE;
        {
            int _j_15971;
            _j_15971 = 1;
L5: 
            if (_j_15971 > _9205){
                goto L6; // [89] 212
            }

            /** 			sequence tmp*/

            /** 			integer a,b*/

            /** 			a = match_data[i][j][1]*/
            _2 = (int)SEQ_PTR(_match_data_15959);
            _9206 = (int)*(((s1_ptr)_2)->base + _i_15968);
            _2 = (int)SEQ_PTR(_9206);
            _9207 = (int)*(((s1_ptr)_2)->base + _j_15971);
            _9206 = NOVALUE;
            _2 = (int)SEQ_PTR(_9207);
            _a_15975 = (int)*(((s1_ptr)_2)->base + 1);
            if (!IS_ATOM_INT(_a_15975)){
                _a_15975 = (long)DBL_PTR(_a_15975)->dbl;
            }
            _9207 = NOVALUE;

            /** 			if a = 0 then*/
            if (_a_15975 != 0)
            goto L7; // [120] 134

            /** 				tmp = ""*/
            RefDS(_5);
            DeRef(_tmp_15974);
            _tmp_15974 = _5;
            goto L8; // [131] 160
L7: 

            /** 				b = match_data[i][j][2]*/
            _2 = (int)SEQ_PTR(_match_data_15959);
            _9210 = (int)*(((s1_ptr)_2)->base + _i_15968);
            _2 = (int)SEQ_PTR(_9210);
            _9211 = (int)*(((s1_ptr)_2)->base + _j_15971);
            _9210 = NOVALUE;
            _2 = (int)SEQ_PTR(_9211);
            _b_15976 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_b_15976)){
                _b_15976 = (long)DBL_PTR(_b_15976)->dbl;
            }
            _9211 = NOVALUE;

            /** 				tmp = haystack[a..b]*/
            rhs_slice_target = (object_ptr)&_tmp_15974;
            RHS_Slice(_haystack_15951, _a_15975, _b_15976);
L8: 

            /** 			if str_offsets then*/
            if (_str_offsets_15957 == 0)
            {
                goto L9; // [162] 189
            }
            else{
            }

            /** 				match_data[i][j] = { tmp, a, b }*/
            _2 = (int)SEQ_PTR(_match_data_15959);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _match_data_15959 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_15968 + ((s1_ptr)_2)->base);
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            RefDS(_tmp_15974);
            *((int *)(_2+4)) = _tmp_15974;
            *((int *)(_2+8)) = _a_15975;
            *((int *)(_2+12)) = _b_15976;
            _9216 = MAKE_SEQ(_1);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_15971);
            _1 = *(int *)_2;
            *(int *)_2 = _9216;
            if( _1 != _9216 ){
                DeRef(_1);
            }
            _9216 = NOVALUE;
            _9214 = NOVALUE;
            goto LA; // [186] 203
L9: 

            /** 				match_data[i][j] = tmp*/
            _2 = (int)SEQ_PTR(_match_data_15959);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _match_data_15959 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_15968 + ((s1_ptr)_2)->base);
            RefDS(_tmp_15974);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_15971);
            _1 = *(int *)_2;
            *(int *)_2 = _tmp_15974;
            DeRef(_1);
            _9217 = NOVALUE;
LA: 
            DeRef(_tmp_15974);
            _tmp_15974 = NOVALUE;

            /** 		end for*/
            _j_15971 = _j_15971 + 1;
            goto L5; // [207] 96
L6: 
            ;
        }

        /** 	end for*/
        _i_15968 = _i_15968 + 1;
        goto L3; // [214] 80
L4: 
        ;
    }

    /** 	return match_data*/
    DeRef(_re_15949);
    DeRef(_haystack_15951);
    DeRef(_options_15953);
    _9204 = NOVALUE;
    return _match_data_15959;
    ;
}


int _33split(int _re_15996, int _text_15998, int _from_15999, int _options_16000)
{
    int _9219 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_15999)) {
        _1 = (long)(DBL_PTR(_from_15999)->dbl);
        if (UNIQUE(DBL_PTR(_from_15999)) && (DBL_PTR(_from_15999)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_15999);
        _from_15999 = _1;
    }

    /** 	return split_limit(re, text, 0, from, options)*/
    Ref(_re_15996);
    Ref(_text_15998);
    Ref(_options_16000);
    _9219 = _33split_limit(_re_15996, _text_15998, 0, _from_15999, _options_16000);
    DeRef(_re_15996);
    DeRef(_text_15998);
    DeRef(_options_16000);
    return _9219;
    ;
}


int _33split_limit(int _re_16005, int _text_16007, int _limit_16008, int _from_16009, int _options_16010)
{
    int _match_data_16014 = NOVALUE;
    int _result_16017 = NOVALUE;
    int _last_16018 = NOVALUE;
    int _a_16029 = NOVALUE;
    int _9245 = NOVALUE;
    int _9244 = NOVALUE;
    int _9243 = NOVALUE;
    int _9241 = NOVALUE;
    int _9239 = NOVALUE;
    int _9238 = NOVALUE;
    int _9237 = NOVALUE;
    int _9236 = NOVALUE;
    int _9235 = NOVALUE;
    int _9232 = NOVALUE;
    int _9231 = NOVALUE;
    int _9230 = NOVALUE;
    int _9227 = NOVALUE;
    int _9226 = NOVALUE;
    int _9224 = NOVALUE;
    int _9222 = NOVALUE;
    int _9220 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_16008)) {
        _1 = (long)(DBL_PTR(_limit_16008)->dbl);
        if (UNIQUE(DBL_PTR(_limit_16008)) && (DBL_PTR(_limit_16008)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_16008);
        _limit_16008 = _1;
    }
    if (!IS_ATOM_INT(_from_16009)) {
        _1 = (long)(DBL_PTR(_from_16009)->dbl);
        if (UNIQUE(DBL_PTR(_from_16009)) && (DBL_PTR(_from_16009)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_16009);
        _from_16009 = _1;
    }

    /** 	if sequence(options) then */
    _9220 = IS_SEQUENCE(_options_16010);
    if (_9220 == 0)
    {
        _9220 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _9220 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_16010);
    _0 = _options_16010;
    _options_16010 = _18or_all(_options_16010);
    DeRef(_0);
L1: 

    /** 	sequence match_data = find_all(re, text, from, options), result*/
    Ref(_re_16005);
    _9222 = _33get_ovector_size(_re_16005, 30);
    Ref(_re_16005);
    Ref(_text_16007);
    Ref(_options_16010);
    _0 = _match_data_16014;
    _match_data_16014 = _33find_all(_re_16005, _text_16007, _from_16009, _options_16010, _9222);
    DeRef(_0);
    _9222 = NOVALUE;

    /** 	integer last = 1*/
    _last_16018 = 1;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _9224 = (_limit_16008 == 0);
    if (_9224 != 0) {
        goto L2; // [54] 70
    }
    if (IS_SEQUENCE(_match_data_16014)){
            _9226 = SEQ_PTR(_match_data_16014)->length;
    }
    else {
        _9226 = 1;
    }
    _9227 = (_limit_16008 > _9226);
    _9226 = NOVALUE;
    if (_9227 == 0)
    {
        DeRef(_9227);
        _9227 = NOVALUE;
        goto L3; // [66] 78
    }
    else{
        DeRef(_9227);
        _9227 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_16014)){
            _limit_16008 = SEQ_PTR(_match_data_16014)->length;
    }
    else {
        _limit_16008 = 1;
    }
L3: 

    /** 	result = repeat(0, limit)*/
    DeRef(_result_16017);
    _result_16017 = Repeat(0, _limit_16008);

    /** 	for i = 1 to limit do*/
    _9230 = _limit_16008;
    {
        int _i_16027;
        _i_16027 = 1;
L4: 
        if (_i_16027 > _9230){
            goto L5; // [89] 176
        }

        /** 		integer a*/

        /** 		a = match_data[i][1][1]*/
        _2 = (int)SEQ_PTR(_match_data_16014);
        _9231 = (int)*(((s1_ptr)_2)->base + _i_16027);
        _2 = (int)SEQ_PTR(_9231);
        _9232 = (int)*(((s1_ptr)_2)->base + 1);
        _9231 = NOVALUE;
        _2 = (int)SEQ_PTR(_9232);
        _a_16029 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_a_16029)){
            _a_16029 = (long)DBL_PTR(_a_16029)->dbl;
        }
        _9232 = NOVALUE;

        /** 		if a = 0 then*/
        if (_a_16029 != 0)
        goto L6; // [118] 131

        /** 			result[i] = ""*/
        RefDS(_5);
        _2 = (int)SEQ_PTR(_result_16017);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_16017 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_16027);
        _1 = *(int *)_2;
        *(int *)_2 = _5;
        DeRef(_1);
        goto L7; // [128] 167
L6: 

        /** 			result[i] = text[last..a - 1]*/
        _9235 = _a_16029 - 1;
        rhs_slice_target = (object_ptr)&_9236;
        RHS_Slice(_text_16007, _last_16018, _9235);
        _2 = (int)SEQ_PTR(_result_16017);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_16017 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_16027);
        _1 = *(int *)_2;
        *(int *)_2 = _9236;
        if( _1 != _9236 ){
            DeRef(_1);
        }
        _9236 = NOVALUE;

        /** 			last = match_data[i][1][2] + 1*/
        _2 = (int)SEQ_PTR(_match_data_16014);
        _9237 = (int)*(((s1_ptr)_2)->base + _i_16027);
        _2 = (int)SEQ_PTR(_9237);
        _9238 = (int)*(((s1_ptr)_2)->base + 1);
        _9237 = NOVALUE;
        _2 = (int)SEQ_PTR(_9238);
        _9239 = (int)*(((s1_ptr)_2)->base + 2);
        _9238 = NOVALUE;
        if (IS_ATOM_INT(_9239)) {
            _last_16018 = _9239 + 1;
        }
        else
        { // coercing _last_16018 to an integer 1
            _last_16018 = 1+(long)(DBL_PTR(_9239)->dbl);
            if( !IS_ATOM_INT(_last_16018) ){
                _last_16018 = (object)DBL_PTR(_last_16018)->dbl;
            }
        }
        _9239 = NOVALUE;
L7: 

        /** 	end for*/
        _i_16027 = _i_16027 + 1;
        goto L4; // [171] 96
L5: 
        ;
    }

    /** 	if last < length(text) then*/
    if (IS_SEQUENCE(_text_16007)){
            _9241 = SEQ_PTR(_text_16007)->length;
    }
    else {
        _9241 = 1;
    }
    if (_last_16018 >= _9241)
    goto L8; // [181] 204

    /** 		result &= { text[last..$] }*/
    if (IS_SEQUENCE(_text_16007)){
            _9243 = SEQ_PTR(_text_16007)->length;
    }
    else {
        _9243 = 1;
    }
    rhs_slice_target = (object_ptr)&_9244;
    RHS_Slice(_text_16007, _last_16018, _9243);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9244;
    _9245 = MAKE_SEQ(_1);
    _9244 = NOVALUE;
    Concat((object_ptr)&_result_16017, _result_16017, _9245);
    DeRefDS(_9245);
    _9245 = NOVALUE;
L8: 

    /** 	return result*/
    DeRef(_re_16005);
    DeRef(_text_16007);
    DeRef(_options_16010);
    DeRef(_match_data_16014);
    DeRef(_9224);
    _9224 = NOVALUE;
    DeRef(_9235);
    _9235 = NOVALUE;
    return _result_16017;
    ;
}


int _33find_replace(int _ex_16051, int _text_16053, int _replacement_16054, int _from_16055, int _options_16056)
{
    int _9247 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_from_16055)) {
        _1 = (long)(DBL_PTR(_from_16055)->dbl);
        if (UNIQUE(DBL_PTR(_from_16055)) && (DBL_PTR(_from_16055)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_16055);
        _from_16055 = _1;
    }

    /** 	return find_replace_limit(ex, text, replacement, -1, from, options)*/
    Ref(_ex_16051);
    Ref(_text_16053);
    RefDS(_replacement_16054);
    Ref(_options_16056);
    _9247 = _33find_replace_limit(_ex_16051, _text_16053, _replacement_16054, -1, _from_16055, _options_16056);
    DeRef(_ex_16051);
    DeRef(_text_16053);
    DeRefDS(_replacement_16054);
    DeRef(_options_16056);
    return _9247;
    ;
}


int _33find_replace_limit(int _ex_16061, int _text_16063, int _replacement_16064, int _limit_16065, int _from_16066, int _options_16067)
{
    int _9251 = NOVALUE;
    int _9250 = NOVALUE;
    int _9248 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_16065)) {
        _1 = (long)(DBL_PTR(_limit_16065)->dbl);
        if (UNIQUE(DBL_PTR(_limit_16065)) && (DBL_PTR(_limit_16065)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_16065);
        _limit_16065 = _1;
    }
    if (!IS_ATOM_INT(_from_16066)) {
        _1 = (long)(DBL_PTR(_from_16066)->dbl);
        if (UNIQUE(DBL_PTR(_from_16066)) && (DBL_PTR(_from_16066)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_16066);
        _from_16066 = _1;
    }

    /** 	if sequence(options) then */
    _9248 = IS_SEQUENCE(_options_16067);
    if (_9248 == 0)
    {
        _9248 = NOVALUE;
        goto L1; // [16] 26
    }
    else{
        _9248 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_16067);
    _0 = _options_16067;
    _options_16067 = _18or_all(_options_16067);
    DeRef(_0);
L1: 

    /**     return machine_func(M_PCRE_REPLACE, { ex, text, replacement, options, */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ex_16061);
    *((int *)(_2+4)) = _ex_16061;
    Ref(_text_16063);
    *((int *)(_2+8)) = _text_16063;
    RefDS(_replacement_16064);
    *((int *)(_2+12)) = _replacement_16064;
    Ref(_options_16067);
    *((int *)(_2+16)) = _options_16067;
    *((int *)(_2+20)) = _from_16066;
    *((int *)(_2+24)) = _limit_16065;
    _9250 = MAKE_SEQ(_1);
    _9251 = machine(71, _9250);
    DeRefDS(_9250);
    _9250 = NOVALUE;
    DeRef(_ex_16061);
    DeRef(_text_16063);
    DeRefDS(_replacement_16064);
    DeRef(_options_16067);
    return _9251;
    ;
}


int _33find_replace_callback(int _ex_16075, int _text_16077, int _rid_16078, int _limit_16079, int _from_16080, int _options_16081)
{
    int _match_data_16085 = NOVALUE;
    int _replace_data_16088 = NOVALUE;
    int _params_16099 = NOVALUE;
    int _9287 = NOVALUE;
    int _9286 = NOVALUE;
    int _9285 = NOVALUE;
    int _9284 = NOVALUE;
    int _9283 = NOVALUE;
    int _9282 = NOVALUE;
    int _9281 = NOVALUE;
    int _9280 = NOVALUE;
    int _9279 = NOVALUE;
    int _9278 = NOVALUE;
    int _9277 = NOVALUE;
    int _9276 = NOVALUE;
    int _9275 = NOVALUE;
    int _9274 = NOVALUE;
    int _9273 = NOVALUE;
    int _9272 = NOVALUE;
    int _9271 = NOVALUE;
    int _9270 = NOVALUE;
    int _9269 = NOVALUE;
    int _9268 = NOVALUE;
    int _9267 = NOVALUE;
    int _9266 = NOVALUE;
    int _9264 = NOVALUE;
    int _9263 = NOVALUE;
    int _9262 = NOVALUE;
    int _9259 = NOVALUE;
    int _9258 = NOVALUE;
    int _9256 = NOVALUE;
    int _9254 = NOVALUE;
    int _9252 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_16078)) {
        _1 = (long)(DBL_PTR(_rid_16078)->dbl);
        if (UNIQUE(DBL_PTR(_rid_16078)) && (DBL_PTR(_rid_16078)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_16078);
        _rid_16078 = _1;
    }
    if (!IS_ATOM_INT(_limit_16079)) {
        _1 = (long)(DBL_PTR(_limit_16079)->dbl);
        if (UNIQUE(DBL_PTR(_limit_16079)) && (DBL_PTR(_limit_16079)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_16079);
        _limit_16079 = _1;
    }
    if (!IS_ATOM_INT(_from_16080)) {
        _1 = (long)(DBL_PTR(_from_16080)->dbl);
        if (UNIQUE(DBL_PTR(_from_16080)) && (DBL_PTR(_from_16080)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_from_16080);
        _from_16080 = _1;
    }

    /** 	if sequence(options) then */
    _9252 = IS_SEQUENCE(_options_16081);
    if (_9252 == 0)
    {
        _9252 = NOVALUE;
        goto L1; // [18] 28
    }
    else{
        _9252 = NOVALUE;
    }

    /** 		options = math:or_all(options) */
    Ref(_options_16081);
    _0 = _options_16081;
    _options_16081 = _18or_all(_options_16081);
    DeRef(_0);
L1: 

    /** 	sequence match_data = find_all(ex, text, from, options), replace_data*/
    Ref(_ex_16075);
    _9254 = _33get_ovector_size(_ex_16075, 30);
    Ref(_ex_16075);
    Ref(_text_16077);
    Ref(_options_16081);
    _0 = _match_data_16085;
    _match_data_16085 = _33find_all(_ex_16075, _text_16077, _from_16080, _options_16081, _9254);
    DeRef(_0);
    _9254 = NOVALUE;

    /** 	if limit = 0 or limit > length(match_data) then*/
    _9256 = (_limit_16079 == 0);
    if (_9256 != 0) {
        goto L2; // [51] 67
    }
    if (IS_SEQUENCE(_match_data_16085)){
            _9258 = SEQ_PTR(_match_data_16085)->length;
    }
    else {
        _9258 = 1;
    }
    _9259 = (_limit_16079 > _9258);
    _9258 = NOVALUE;
    if (_9259 == 0)
    {
        DeRef(_9259);
        _9259 = NOVALUE;
        goto L3; // [63] 75
    }
    else{
        DeRef(_9259);
        _9259 = NOVALUE;
    }
L2: 

    /** 		limit = length(match_data)*/
    if (IS_SEQUENCE(_match_data_16085)){
            _limit_16079 = SEQ_PTR(_match_data_16085)->length;
    }
    else {
        _limit_16079 = 1;
    }
L3: 

    /** 	replace_data = repeat(0, limit)*/
    DeRef(_replace_data_16088);
    _replace_data_16088 = Repeat(0, _limit_16079);

    /** 	for i = 1 to limit do*/
    _9262 = _limit_16079;
    {
        int _i_16097;
        _i_16097 = 1;
L4: 
        if (_i_16097 > _9262){
            goto L5; // [86] 218
        }

        /** 		sequence params = repeat(0, length(match_data[i]))*/
        _2 = (int)SEQ_PTR(_match_data_16085);
        _9263 = (int)*(((s1_ptr)_2)->base + _i_16097);
        if (IS_SEQUENCE(_9263)){
                _9264 = SEQ_PTR(_9263)->length;
        }
        else {
            _9264 = 1;
        }
        _9263 = NOVALUE;
        DeRef(_params_16099);
        _params_16099 = Repeat(0, _9264);
        _9264 = NOVALUE;

        /** 		for j = 1 to length(match_data[i]) do*/
        _2 = (int)SEQ_PTR(_match_data_16085);
        _9266 = (int)*(((s1_ptr)_2)->base + _i_16097);
        if (IS_SEQUENCE(_9266)){
                _9267 = SEQ_PTR(_9266)->length;
        }
        else {
            _9267 = 1;
        }
        _9266 = NOVALUE;
        {
            int _j_16104;
            _j_16104 = 1;
L6: 
            if (_j_16104 > _9267){
                goto L7; // [115] 195
            }

            /** 			if equal(match_data[i][j],{0,0}) then*/
            _2 = (int)SEQ_PTR(_match_data_16085);
            _9268 = (int)*(((s1_ptr)_2)->base + _i_16097);
            _2 = (int)SEQ_PTR(_9268);
            _9269 = (int)*(((s1_ptr)_2)->base + _j_16104);
            _9268 = NOVALUE;
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = 0;
            ((int *)_2)[2] = 0;
            _9270 = MAKE_SEQ(_1);
            if (_9269 == _9270)
            _9271 = 1;
            else if (IS_ATOM_INT(_9269) && IS_ATOM_INT(_9270))
            _9271 = 0;
            else
            _9271 = (compare(_9269, _9270) == 0);
            _9269 = NOVALUE;
            DeRefDS(_9270);
            _9270 = NOVALUE;
            if (_9271 == 0)
            {
                _9271 = NOVALUE;
                goto L8; // [140] 152
            }
            else{
                _9271 = NOVALUE;
            }

            /** 				params[j] = 0*/
            _2 = (int)SEQ_PTR(_params_16099);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _params_16099 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_16104);
            _1 = *(int *)_2;
            *(int *)_2 = 0;
            DeRef(_1);
            goto L9; // [149] 188
L8: 

            /** 				params[j] = text[match_data[i][j][1]..match_data[i][j][2]]*/
            _2 = (int)SEQ_PTR(_match_data_16085);
            _9272 = (int)*(((s1_ptr)_2)->base + _i_16097);
            _2 = (int)SEQ_PTR(_9272);
            _9273 = (int)*(((s1_ptr)_2)->base + _j_16104);
            _9272 = NOVALUE;
            _2 = (int)SEQ_PTR(_9273);
            _9274 = (int)*(((s1_ptr)_2)->base + 1);
            _9273 = NOVALUE;
            _2 = (int)SEQ_PTR(_match_data_16085);
            _9275 = (int)*(((s1_ptr)_2)->base + _i_16097);
            _2 = (int)SEQ_PTR(_9275);
            _9276 = (int)*(((s1_ptr)_2)->base + _j_16104);
            _9275 = NOVALUE;
            _2 = (int)SEQ_PTR(_9276);
            _9277 = (int)*(((s1_ptr)_2)->base + 2);
            _9276 = NOVALUE;
            rhs_slice_target = (object_ptr)&_9278;
            RHS_Slice(_text_16077, _9274, _9277);
            _2 = (int)SEQ_PTR(_params_16099);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _params_16099 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_16104);
            _1 = *(int *)_2;
            *(int *)_2 = _9278;
            if( _1 != _9278 ){
                DeRef(_1);
            }
            _9278 = NOVALUE;
L9: 

            /** 		end for*/
            _j_16104 = _j_16104 + 1;
            goto L6; // [190] 122
L7: 
            ;
        }

        /** 		replace_data[i] = call_func(rid, { params })*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_params_16099);
        *((int *)(_2+4)) = _params_16099;
        _9279 = MAKE_SEQ(_1);
        _1 = (int)SEQ_PTR(_9279);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_rid_16078].addr;
        Ref(*(int *)(_2+4));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRef(_9280);
        _9280 = _1;
        DeRefDS(_9279);
        _9279 = NOVALUE;
        _2 = (int)SEQ_PTR(_replace_data_16088);
        _2 = (int)(((s1_ptr)_2)->base + _i_16097);
        _1 = *(int *)_2;
        *(int *)_2 = _9280;
        if( _1 != _9280 ){
            DeRef(_1);
        }
        _9280 = NOVALUE;
        DeRefDS(_params_16099);
        _params_16099 = NOVALUE;

        /** 	end for*/
        _i_16097 = _i_16097 + 1;
        goto L4; // [213] 93
L5: 
        ;
    }

    /** 	for i = limit to 1 by -1 do*/
    {
        int _i_16123;
        _i_16123 = _limit_16079;
LA: 
        if (_i_16123 < 1){
            goto LB; // [220] 270
        }

        /** 		text = replace(text, replace_data[i], match_data[i][1][1], match_data[i][1][2])*/
        _2 = (int)SEQ_PTR(_replace_data_16088);
        _9281 = (int)*(((s1_ptr)_2)->base + _i_16123);
        _2 = (int)SEQ_PTR(_match_data_16085);
        _9282 = (int)*(((s1_ptr)_2)->base + _i_16123);
        _2 = (int)SEQ_PTR(_9282);
        _9283 = (int)*(((s1_ptr)_2)->base + 1);
        _9282 = NOVALUE;
        _2 = (int)SEQ_PTR(_9283);
        _9284 = (int)*(((s1_ptr)_2)->base + 1);
        _9283 = NOVALUE;
        _2 = (int)SEQ_PTR(_match_data_16085);
        _9285 = (int)*(((s1_ptr)_2)->base + _i_16123);
        _2 = (int)SEQ_PTR(_9285);
        _9286 = (int)*(((s1_ptr)_2)->base + 1);
        _9285 = NOVALUE;
        _2 = (int)SEQ_PTR(_9286);
        _9287 = (int)*(((s1_ptr)_2)->base + 2);
        _9286 = NOVALUE;
        {
            int p1 = _text_16077;
            int p2 = _9281;
            int p3 = _9284;
            int p4 = _9287;
            struct replace_block replace_params;
            replace_params.copy_to   = &p1;
            replace_params.copy_from = &p2;
            replace_params.start     = &p3;
            replace_params.stop      = &p4;
            replace_params.target    = &_text_16077;
            Replace( &replace_params );
        }
        _9281 = NOVALUE;
        _9284 = NOVALUE;
        _9287 = NOVALUE;

        /** 	end for*/
        _i_16123 = _i_16123 + -1;
        goto LA; // [265] 227
LB: 
        ;
    }

    /** 	return text*/
    DeRef(_ex_16075);
    DeRef(_options_16081);
    DeRef(_match_data_16085);
    DeRef(_replace_data_16088);
    DeRef(_9256);
    _9256 = NOVALUE;
    _9263 = NOVALUE;
    _9266 = NOVALUE;
    _9274 = NOVALUE;
    _9277 = NOVALUE;
    return _text_16077;
    ;
}



// 0xD777FB3F
