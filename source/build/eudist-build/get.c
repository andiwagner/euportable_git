// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _4get_ch()
{
    int _5832 = NOVALUE;
    int _5831 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(input_string) then*/
    _5831 = IS_SEQUENCE(_4input_string_10306);
    if (_5831 == 0)
    {
        _5831 = NOVALUE;
        goto L1; // [8] 56
    }
    else{
        _5831 = NOVALUE;
    }

    /** 		if string_next <= length(input_string) then*/
    if (IS_SEQUENCE(_4input_string_10306)){
            _5832 = SEQ_PTR(_4input_string_10306)->length;
    }
    else {
        _5832 = 1;
    }
    if (_4string_next_10307 > _5832)
    goto L2; // [20] 47

    /** 			ch = input_string[string_next]*/
    _2 = (int)SEQ_PTR(_4input_string_10306);
    _4ch_10308 = (int)*(((s1_ptr)_2)->base + _4string_next_10307);
    if (!IS_ATOM_INT(_4ch_10308)){
        _4ch_10308 = (long)DBL_PTR(_4ch_10308)->dbl;
    }

    /** 			string_next += 1*/
    _4string_next_10307 = _4string_next_10307 + 1;
    goto L3; // [44] 81
L2: 

    /** 			ch = GET_EOF*/
    _4ch_10308 = -1;
    goto L3; // [53] 81
L1: 

    /** 		ch = getc(input_file)*/
    if (_4input_file_10305 != last_r_file_no) {
        last_r_file_ptr = which_file(_4input_file_10305, EF_READ);
        last_r_file_no = _4input_file_10305;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _4ch_10308 = getKBchar();
        }
        else
        _4ch_10308 = getc(last_r_file_ptr);
    }
    else
    _4ch_10308 = getc(last_r_file_ptr);

    /** 		if ch = GET_EOF then*/
    if (_4ch_10308 != -1)
    goto L4; // [67] 80

    /** 			string_next += 1*/
    _4string_next_10307 = _4string_next_10307 + 1;
L4: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _4escape_char(int _c_10335)
{
    int _i_10336 = NOVALUE;
    int _5844 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = find(c, ESCAPE_CHARS)*/
    _i_10336 = find_from(_c_10335, _4ESCAPE_CHARS_10329, 1);

    /** 	if i = 0 then*/
    if (_i_10336 != 0)
    goto L1; // [12] 25

    /** 		return GET_FAIL*/
    return 1;
    goto L2; // [22] 36
L1: 

    /** 		return ESCAPED_CHARS[i]*/
    _2 = (int)SEQ_PTR(_4ESCAPED_CHARS_10331);
    _5844 = (int)*(((s1_ptr)_2)->base + _i_10336);
    Ref(_5844);
    return _5844;
L2: 
    ;
}


int _4get_qchar()
{
    int _c_10344 = NOVALUE;
    int _5853 = NOVALUE;
    int _5852 = NOVALUE;
    int _5850 = NOVALUE;
    int _5848 = NOVALUE;
    int _0, _1, _2;
    

    /** 	get_ch()*/
    _4get_ch();

    /** 	c = ch*/
    _c_10344 = _4ch_10308;

    /** 	if ch = '\\' then*/
    if (_4ch_10308 != 92)
    goto L1; // [16] 54

    /** 		get_ch()*/
    _4get_ch();

    /** 		c = escape_char(ch)*/
    _c_10344 = _4escape_char(_4ch_10308);
    if (!IS_ATOM_INT(_c_10344)) {
        _1 = (long)(DBL_PTR(_c_10344)->dbl);
        if (UNIQUE(DBL_PTR(_c_10344)) && (DBL_PTR(_c_10344)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_10344);
        _c_10344 = _1;
    }

    /** 		if c = GET_FAIL then*/
    if (_c_10344 != 1)
    goto L2; // [36] 74

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5848 = MAKE_SEQ(_1);
    return _5848;
    goto L2; // [51] 74
L1: 

    /** 	elsif ch = '\'' then*/
    if (_4ch_10308 != 39)
    goto L3; // [58] 73

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5850 = MAKE_SEQ(_1);
    DeRef(_5848);
    _5848 = NOVALUE;
    return _5850;
L3: 
L2: 

    /** 	get_ch()*/
    _4get_ch();

    /** 	if ch != '\'' then*/
    if (_4ch_10308 == 39)
    goto L4; // [82] 99

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5852 = MAKE_SEQ(_1);
    DeRef(_5848);
    _5848 = NOVALUE;
    DeRef(_5850);
    _5850 = NOVALUE;
    return _5852;
    goto L5; // [96] 114
L4: 

    /** 		get_ch()*/
    _4get_ch();

    /** 		return {GET_SUCCESS, c}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _c_10344;
    _5853 = MAKE_SEQ(_1);
    DeRef(_5848);
    _5848 = NOVALUE;
    DeRef(_5850);
    _5850 = NOVALUE;
    DeRef(_5852);
    _5852 = NOVALUE;
    return _5853;
L5: 
    ;
}


int _4get_heredoc(int _terminator_10361)
{
    int _text_10362 = NOVALUE;
    int _ends_at_10363 = NOVALUE;
    int _5868 = NOVALUE;
    int _5867 = NOVALUE;
    int _5866 = NOVALUE;
    int _5865 = NOVALUE;
    int _5864 = NOVALUE;
    int _5861 = NOVALUE;
    int _5859 = NOVALUE;
    int _5858 = NOVALUE;
    int _5857 = NOVALUE;
    int _5856 = NOVALUE;
    int _5854 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence text = ""*/
    RefDS(_5);
    DeRefi(_text_10362);
    _text_10362 = _5;

    /** 	integer ends_at = 1 - length( terminator )*/
    if (IS_SEQUENCE(_terminator_10361)){
            _5854 = SEQ_PTR(_terminator_10361)->length;
    }
    else {
        _5854 = 1;
    }
    _ends_at_10363 = 1 - _5854;
    _5854 = NOVALUE;

    /** 	while ends_at < 1 or not match( terminator, text, ends_at ) with entry do*/
    goto L1; // [23] 71
L2: 
    _5856 = (_ends_at_10363 < 1);
    if (_5856 != 0) {
        DeRef(_5857);
        _5857 = 1;
        goto L3; // [30] 46
    }
    _5858 = e_match_from(_terminator_10361, _text_10362, _ends_at_10363);
    _5859 = (_5858 == 0);
    _5858 = NOVALUE;
    _5857 = (_5859 != 0);
L3: 
    if (_5857 == 0)
    {
        _5857 = NOVALUE;
        goto L4; // [46] 96
    }
    else{
        _5857 = NOVALUE;
    }

    /** 		if ch = GET_EOF then*/
    if (_4ch_10308 != -1)
    goto L5; // [53] 68

    /** 			return { GET_FAIL, 0 }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5861 = MAKE_SEQ(_1);
    DeRefDSi(_terminator_10361);
    DeRefi(_text_10362);
    DeRef(_5856);
    _5856 = NOVALUE;
    DeRef(_5859);
    _5859 = NOVALUE;
    return _5861;
L5: 

    /** 	entry*/
L1: 

    /** 		get_ch()*/
    _4get_ch();

    /** 		text &= ch*/
    Append(&_text_10362, _text_10362, _4ch_10308);

    /** 		ends_at += 1*/
    _ends_at_10363 = _ends_at_10363 + 1;

    /** 	end while*/
    goto L2; // [93] 26
L4: 

    /** 	return { GET_SUCCESS, head( text, length( text ) - length( terminator ) ) }*/
    if (IS_SEQUENCE(_text_10362)){
            _5864 = SEQ_PTR(_text_10362)->length;
    }
    else {
        _5864 = 1;
    }
    if (IS_SEQUENCE(_terminator_10361)){
            _5865 = SEQ_PTR(_terminator_10361)->length;
    }
    else {
        _5865 = 1;
    }
    _5866 = _5864 - _5865;
    _5864 = NOVALUE;
    _5865 = NOVALUE;
    {
        int len = SEQ_PTR(_text_10362)->length;
        int size = (IS_ATOM_INT(_5866)) ? _5866 : (object)(DBL_PTR(_5866)->dbl);
        if (size <= 0){
            DeRef( _5867 );
            _5867 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_text_10362);
            DeRef(_5867);
            _5867 = _text_10362;
        }
        else{
            Head(SEQ_PTR(_text_10362),size+1,&_5867);
        }
    }
    _5866 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _5867;
    _5868 = MAKE_SEQ(_1);
    _5867 = NOVALUE;
    DeRefDSi(_terminator_10361);
    DeRefDSi(_text_10362);
    DeRef(_5856);
    _5856 = NOVALUE;
    DeRef(_5859);
    _5859 = NOVALUE;
    DeRef(_5861);
    _5861 = NOVALUE;
    return _5868;
    ;
}


int _4get_string()
{
    int _text_10383 = NOVALUE;
    int _5885 = NOVALUE;
    int _5881 = NOVALUE;
    int _5880 = NOVALUE;
    int _5877 = NOVALUE;
    int _5876 = NOVALUE;
    int _5875 = NOVALUE;
    int _5874 = NOVALUE;
    int _5872 = NOVALUE;
    int _5871 = NOVALUE;
    int _5869 = NOVALUE;
    int _0, _1, _2;
    

    /** 	text = ""*/
    RefDS(_5);
    DeRefi(_text_10383);
    _text_10383 = _5;

    /** 	while TRUE do*/
L1: 

    /** 		get_ch()*/
    _4get_ch();

    /** 		if ch = GET_EOF or ch = '\n' then*/
    _5869 = (_4ch_10308 == -1);
    if (_5869 != 0) {
        goto L2; // [25] 40
    }
    _5871 = (_4ch_10308 == 10);
    if (_5871 == 0)
    {
        DeRef(_5871);
        _5871 = NOVALUE;
        goto L3; // [36] 53
    }
    else{
        DeRef(_5871);
        _5871 = NOVALUE;
    }
L2: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5872 = MAKE_SEQ(_1);
    DeRefi(_text_10383);
    DeRef(_5869);
    _5869 = NOVALUE;
    return _5872;
    goto L4; // [50] 164
L3: 

    /** 		elsif ch = '"' then*/
    if (_4ch_10308 != 34)
    goto L5; // [57] 121

    /** 			get_ch()*/
    _4get_ch();

    /** 			if length( text ) = 0 and ch = '"' then*/
    if (IS_SEQUENCE(_text_10383)){
            _5874 = SEQ_PTR(_text_10383)->length;
    }
    else {
        _5874 = 1;
    }
    _5875 = (_5874 == 0);
    _5874 = NOVALUE;
    if (_5875 == 0) {
        goto L6; // [74] 108
    }
    _5877 = (_4ch_10308 == 34);
    if (_5877 == 0)
    {
        DeRef(_5877);
        _5877 = NOVALUE;
        goto L6; // [85] 108
    }
    else{
        DeRef(_5877);
        _5877 = NOVALUE;
    }

    /** 				if ch = '"' then*/
    if (_4ch_10308 != 34)
    goto L7; // [92] 107

    /** 					return get_heredoc( `"""` )*/
    RefDS(_5879);
    _5880 = _4get_heredoc(_5879);
    DeRefi(_text_10383);
    DeRef(_5869);
    _5869 = NOVALUE;
    DeRef(_5872);
    _5872 = NOVALUE;
    DeRef(_5875);
    _5875 = NOVALUE;
    return _5880;
L7: 
L6: 

    /** 			return {GET_SUCCESS, text}*/
    RefDS(_text_10383);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _text_10383;
    _5881 = MAKE_SEQ(_1);
    DeRefDSi(_text_10383);
    DeRef(_5869);
    _5869 = NOVALUE;
    DeRef(_5872);
    _5872 = NOVALUE;
    DeRef(_5875);
    _5875 = NOVALUE;
    DeRef(_5880);
    _5880 = NOVALUE;
    return _5881;
    goto L4; // [118] 164
L5: 

    /** 		elsif ch = '\\' then*/
    if (_4ch_10308 != 92)
    goto L8; // [125] 163

    /** 			get_ch()*/
    _4get_ch();

    /** 			ch = escape_char(ch)*/
    _0 = _4escape_char(_4ch_10308);
    _4ch_10308 = _0;
    if (!IS_ATOM_INT(_4ch_10308)) {
        _1 = (long)(DBL_PTR(_4ch_10308)->dbl);
        if (UNIQUE(DBL_PTR(_4ch_10308)) && (DBL_PTR(_4ch_10308)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4ch_10308);
        _4ch_10308 = _1;
    }

    /** 			if ch = GET_FAIL then*/
    if (_4ch_10308 != 1)
    goto L9; // [147] 162

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5885 = MAKE_SEQ(_1);
    DeRefi(_text_10383);
    DeRef(_5869);
    _5869 = NOVALUE;
    DeRef(_5872);
    _5872 = NOVALUE;
    DeRef(_5875);
    _5875 = NOVALUE;
    DeRef(_5880);
    _5880 = NOVALUE;
    DeRef(_5881);
    _5881 = NOVALUE;
    return _5885;
L9: 
L8: 
L4: 

    /** 		text = text & ch*/
    Append(&_text_10383, _text_10383, _4ch_10308);

    /** 	end while*/
    goto L1; // [174] 13
    ;
}


int _4read_comment()
{
    int _5906 = NOVALUE;
    int _5905 = NOVALUE;
    int _5903 = NOVALUE;
    int _5901 = NOVALUE;
    int _5899 = NOVALUE;
    int _5898 = NOVALUE;
    int _5897 = NOVALUE;
    int _5895 = NOVALUE;
    int _5894 = NOVALUE;
    int _5893 = NOVALUE;
    int _5892 = NOVALUE;
    int _5891 = NOVALUE;
    int _5890 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(input_string) then*/
    _5890 = IS_ATOM(_4input_string_10306);
    if (_5890 == 0)
    {
        _5890 = NOVALUE;
        goto L1; // [8] 98
    }
    else{
        _5890 = NOVALUE;
    }

    /** 		while ch!='\n' and ch!='\r' and ch!=-1 do*/
L2: 
    _5891 = (_4ch_10308 != 10);
    if (_5891 == 0) {
        _5892 = 0;
        goto L3; // [22] 36
    }
    _5893 = (_4ch_10308 != 13);
    _5892 = (_5893 != 0);
L3: 
    if (_5892 == 0) {
        goto L4; // [36] 59
    }
    _5895 = (_4ch_10308 != -1);
    if (_5895 == 0)
    {
        DeRef(_5895);
        _5895 = NOVALUE;
        goto L4; // [47] 59
    }
    else{
        DeRef(_5895);
        _5895 = NOVALUE;
    }

    /** 			get_ch()*/
    _4get_ch();

    /** 		end while*/
    goto L2; // [56] 16
L4: 

    /** 		get_ch()*/
    _4get_ch();

    /** 		if ch=-1 then*/
    if (_4ch_10308 != -1)
    goto L5; // [67] 84

    /** 			return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _5897 = MAKE_SEQ(_1);
    DeRef(_5891);
    _5891 = NOVALUE;
    DeRef(_5893);
    _5893 = NOVALUE;
    return _5897;
    goto L6; // [81] 182
L5: 

    /** 			return {GET_IGNORE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _5898 = MAKE_SEQ(_1);
    DeRef(_5891);
    _5891 = NOVALUE;
    DeRef(_5893);
    _5893 = NOVALUE;
    DeRef(_5897);
    _5897 = NOVALUE;
    return _5898;
    goto L6; // [95] 182
L1: 

    /** 		for i=string_next to length(input_string) do*/
    if (IS_SEQUENCE(_4input_string_10306)){
            _5899 = SEQ_PTR(_4input_string_10306)->length;
    }
    else {
        _5899 = 1;
    }
    {
        int _i_10433;
        _i_10433 = _4string_next_10307;
L7: 
        if (_i_10433 > _5899){
            goto L8; // [107] 171
        }

        /** 			ch=input_string[i]*/
        _2 = (int)SEQ_PTR(_4input_string_10306);
        _4ch_10308 = (int)*(((s1_ptr)_2)->base + _i_10433);
        if (!IS_ATOM_INT(_4ch_10308)){
            _4ch_10308 = (long)DBL_PTR(_4ch_10308)->dbl;
        }

        /** 			if ch='\n' or ch='\r' then*/
        _5901 = (_4ch_10308 == 10);
        if (_5901 != 0) {
            goto L9; // [132] 147
        }
        _5903 = (_4ch_10308 == 13);
        if (_5903 == 0)
        {
            DeRef(_5903);
            _5903 = NOVALUE;
            goto LA; // [143] 164
        }
        else{
            DeRef(_5903);
            _5903 = NOVALUE;
        }
L9: 

        /** 				string_next=i+1*/
        _4string_next_10307 = _i_10433 + 1;

        /** 				return {GET_IGNORE, 0}*/
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = 0;
        _5905 = MAKE_SEQ(_1);
        DeRef(_5891);
        _5891 = NOVALUE;
        DeRef(_5893);
        _5893 = NOVALUE;
        DeRef(_5897);
        _5897 = NOVALUE;
        DeRef(_5898);
        _5898 = NOVALUE;
        DeRef(_5901);
        _5901 = NOVALUE;
        return _5905;
LA: 

        /** 		end for*/
        _i_10433 = _i_10433 + 1;
        goto L7; // [166] 114
L8: 
        ;
    }

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _5906 = MAKE_SEQ(_1);
    DeRef(_5891);
    _5891 = NOVALUE;
    DeRef(_5893);
    _5893 = NOVALUE;
    DeRef(_5897);
    _5897 = NOVALUE;
    DeRef(_5898);
    _5898 = NOVALUE;
    DeRef(_5901);
    _5901 = NOVALUE;
    DeRef(_5905);
    _5905 = NOVALUE;
    return _5906;
L6: 
    ;
}


int _4get_number()
{
    int _sign_10445 = NOVALUE;
    int _e_sign_10446 = NOVALUE;
    int _ndigits_10447 = NOVALUE;
    int _hex_digit_10448 = NOVALUE;
    int _mantissa_10449 = NOVALUE;
    int _dec_10450 = NOVALUE;
    int _e_mag_10451 = NOVALUE;
    int _5967 = NOVALUE;
    int _5965 = NOVALUE;
    int _5963 = NOVALUE;
    int _5960 = NOVALUE;
    int _5956 = NOVALUE;
    int _5954 = NOVALUE;
    int _5953 = NOVALUE;
    int _5952 = NOVALUE;
    int _5951 = NOVALUE;
    int _5950 = NOVALUE;
    int _5948 = NOVALUE;
    int _5947 = NOVALUE;
    int _5946 = NOVALUE;
    int _5943 = NOVALUE;
    int _5941 = NOVALUE;
    int _5939 = NOVALUE;
    int _5935 = NOVALUE;
    int _5934 = NOVALUE;
    int _5932 = NOVALUE;
    int _5931 = NOVALUE;
    int _5930 = NOVALUE;
    int _5927 = NOVALUE;
    int _5926 = NOVALUE;
    int _5924 = NOVALUE;
    int _5923 = NOVALUE;
    int _5922 = NOVALUE;
    int _5921 = NOVALUE;
    int _5920 = NOVALUE;
    int _5919 = NOVALUE;
    int _5916 = NOVALUE;
    int _5912 = NOVALUE;
    int _5909 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sign = +1*/
    _sign_10445 = 1;

    /** 	mantissa = 0*/
    DeRef(_mantissa_10449);
    _mantissa_10449 = 0;

    /** 	ndigits = 0*/
    _ndigits_10447 = 0;

    /** 	if ch = '-' then*/
    if (_4ch_10308 != 45)
    goto L1; // [20] 54

    /** 		sign = -1*/
    _sign_10445 = -1;

    /** 		get_ch()*/
    _4get_ch();

    /** 		if ch='-' then*/
    if (_4ch_10308 != 45)
    goto L2; // [37] 68

    /** 			return read_comment()*/
    _5909 = _4read_comment();
    DeRef(_dec_10450);
    DeRef(_e_mag_10451);
    return _5909;
    goto L2; // [51] 68
L1: 

    /** 	elsif ch = '+' then*/
    if (_4ch_10308 != 43)
    goto L3; // [58] 67

    /** 		get_ch()*/
    _4get_ch();
L3: 
L2: 

    /** 	if ch = '#' then*/
    if (_4ch_10308 != 35)
    goto L4; // [72] 172

    /** 		get_ch()*/
    _4get_ch();

    /** 		while TRUE do*/
L5: 

    /** 			hex_digit = find(ch, HEX_DIGITS)-1*/
    _5912 = find_from(_4ch_10308, _4HEX_DIGITS_10288, 1);
    _hex_digit_10448 = _5912 - 1;
    _5912 = NOVALUE;

    /** 			if hex_digit >= 0 then*/
    if (_hex_digit_10448 < 0)
    goto L6; // [104] 131

    /** 				ndigits += 1*/
    _ndigits_10447 = _ndigits_10447 + 1;

    /** 				mantissa = mantissa * 16 + hex_digit*/
    if (IS_ATOM_INT(_mantissa_10449)) {
        if (_mantissa_10449 == (short)_mantissa_10449)
        _5916 = _mantissa_10449 * 16;
        else
        _5916 = NewDouble(_mantissa_10449 * (double)16);
    }
    else {
        _5916 = NewDouble(DBL_PTR(_mantissa_10449)->dbl * (double)16);
    }
    DeRef(_mantissa_10449);
    if (IS_ATOM_INT(_5916)) {
        _mantissa_10449 = _5916 + _hex_digit_10448;
        if ((long)((unsigned long)_mantissa_10449 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_10449 = NewDouble((double)_mantissa_10449);
    }
    else {
        _mantissa_10449 = NewDouble(DBL_PTR(_5916)->dbl + (double)_hex_digit_10448);
    }
    DeRef(_5916);
    _5916 = NOVALUE;

    /** 				get_ch()*/
    _4get_ch();
    goto L5; // [128] 85
L6: 

    /** 				if ndigits > 0 then*/
    if (_ndigits_10447 <= 0)
    goto L7; // [133] 154

    /** 					return {GET_SUCCESS, sign * mantissa}*/
    if (IS_ATOM_INT(_mantissa_10449)) {
        if (_sign_10445 == (short)_sign_10445 && _mantissa_10449 <= INT15 && _mantissa_10449 >= -INT15)
        _5919 = _sign_10445 * _mantissa_10449;
        else
        _5919 = NewDouble(_sign_10445 * (double)_mantissa_10449);
    }
    else {
        _5919 = NewDouble((double)_sign_10445 * DBL_PTR(_mantissa_10449)->dbl);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _5919;
    _5920 = MAKE_SEQ(_1);
    _5919 = NOVALUE;
    DeRef(_mantissa_10449);
    DeRef(_dec_10450);
    DeRef(_e_mag_10451);
    DeRef(_5909);
    _5909 = NOVALUE;
    return _5920;
    goto L5; // [151] 85
L7: 

    /** 					return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5921 = MAKE_SEQ(_1);
    DeRef(_mantissa_10449);
    DeRef(_dec_10450);
    DeRef(_e_mag_10451);
    DeRef(_5909);
    _5909 = NOVALUE;
    DeRef(_5920);
    _5920 = NOVALUE;
    return _5921;

    /** 		end while*/
    goto L5; // [168] 85
L4: 

    /** 	while ch >= '0' and ch <= '9' do*/
L8: 
    _5922 = (_4ch_10308 >= 48);
    if (_5922 == 0) {
        goto L9; // [183] 228
    }
    _5924 = (_4ch_10308 <= 57);
    if (_5924 == 0)
    {
        DeRef(_5924);
        _5924 = NOVALUE;
        goto L9; // [194] 228
    }
    else{
        DeRef(_5924);
        _5924 = NOVALUE;
    }

    /** 		ndigits += 1*/
    _ndigits_10447 = _ndigits_10447 + 1;

    /** 		mantissa = mantissa * 10 + (ch - '0')*/
    if (IS_ATOM_INT(_mantissa_10449)) {
        if (_mantissa_10449 == (short)_mantissa_10449)
        _5926 = _mantissa_10449 * 10;
        else
        _5926 = NewDouble(_mantissa_10449 * (double)10);
    }
    else {
        _5926 = NewDouble(DBL_PTR(_mantissa_10449)->dbl * (double)10);
    }
    _5927 = _4ch_10308 - 48;
    if ((long)((unsigned long)_5927 +(unsigned long) HIGH_BITS) >= 0){
        _5927 = NewDouble((double)_5927);
    }
    DeRef(_mantissa_10449);
    if (IS_ATOM_INT(_5926) && IS_ATOM_INT(_5927)) {
        _mantissa_10449 = _5926 + _5927;
        if ((long)((unsigned long)_mantissa_10449 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_10449 = NewDouble((double)_mantissa_10449);
    }
    else {
        if (IS_ATOM_INT(_5926)) {
            _mantissa_10449 = NewDouble((double)_5926 + DBL_PTR(_5927)->dbl);
        }
        else {
            if (IS_ATOM_INT(_5927)) {
                _mantissa_10449 = NewDouble(DBL_PTR(_5926)->dbl + (double)_5927);
            }
            else
            _mantissa_10449 = NewDouble(DBL_PTR(_5926)->dbl + DBL_PTR(_5927)->dbl);
        }
    }
    DeRef(_5926);
    _5926 = NOVALUE;
    DeRef(_5927);
    _5927 = NOVALUE;

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto L8; // [225] 177
L9: 

    /** 	if ch = '.' then*/
    if (_4ch_10308 != 46)
    goto LA; // [232] 308

    /** 		get_ch()*/
    _4get_ch();

    /** 		dec = 10*/
    DeRef(_dec_10450);
    _dec_10450 = 10;

    /** 		while ch >= '0' and ch <= '9' do*/
LB: 
    _5930 = (_4ch_10308 >= 48);
    if (_5930 == 0) {
        goto LC; // [256] 307
    }
    _5932 = (_4ch_10308 <= 57);
    if (_5932 == 0)
    {
        DeRef(_5932);
        _5932 = NOVALUE;
        goto LC; // [267] 307
    }
    else{
        DeRef(_5932);
        _5932 = NOVALUE;
    }

    /** 			ndigits += 1*/
    _ndigits_10447 = _ndigits_10447 + 1;

    /** 			mantissa += (ch - '0') / dec*/
    _5934 = _4ch_10308 - 48;
    if ((long)((unsigned long)_5934 +(unsigned long) HIGH_BITS) >= 0){
        _5934 = NewDouble((double)_5934);
    }
    if (IS_ATOM_INT(_5934) && IS_ATOM_INT(_dec_10450)) {
        _5935 = (_5934 % _dec_10450) ? NewDouble((double)_5934 / _dec_10450) : (_5934 / _dec_10450);
    }
    else {
        if (IS_ATOM_INT(_5934)) {
            _5935 = NewDouble((double)_5934 / DBL_PTR(_dec_10450)->dbl);
        }
        else {
            if (IS_ATOM_INT(_dec_10450)) {
                _5935 = NewDouble(DBL_PTR(_5934)->dbl / (double)_dec_10450);
            }
            else
            _5935 = NewDouble(DBL_PTR(_5934)->dbl / DBL_PTR(_dec_10450)->dbl);
        }
    }
    DeRef(_5934);
    _5934 = NOVALUE;
    _0 = _mantissa_10449;
    if (IS_ATOM_INT(_mantissa_10449) && IS_ATOM_INT(_5935)) {
        _mantissa_10449 = _mantissa_10449 + _5935;
        if ((long)((unsigned long)_mantissa_10449 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_10449 = NewDouble((double)_mantissa_10449);
    }
    else {
        if (IS_ATOM_INT(_mantissa_10449)) {
            _mantissa_10449 = NewDouble((double)_mantissa_10449 + DBL_PTR(_5935)->dbl);
        }
        else {
            if (IS_ATOM_INT(_5935)) {
                _mantissa_10449 = NewDouble(DBL_PTR(_mantissa_10449)->dbl + (double)_5935);
            }
            else
            _mantissa_10449 = NewDouble(DBL_PTR(_mantissa_10449)->dbl + DBL_PTR(_5935)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_5935);
    _5935 = NOVALUE;

    /** 			dec *= 10*/
    _0 = _dec_10450;
    if (IS_ATOM_INT(_dec_10450)) {
        if (_dec_10450 == (short)_dec_10450)
        _dec_10450 = _dec_10450 * 10;
        else
        _dec_10450 = NewDouble(_dec_10450 * (double)10);
    }
    else {
        _dec_10450 = NewDouble(DBL_PTR(_dec_10450)->dbl * (double)10);
    }
    DeRef(_0);

    /** 			get_ch()*/
    _4get_ch();

    /** 		end while*/
    goto LB; // [304] 250
LC: 
LA: 

    /** 	if ndigits = 0 then*/
    if (_ndigits_10447 != 0)
    goto LD; // [310] 325

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5939 = MAKE_SEQ(_1);
    DeRef(_mantissa_10449);
    DeRef(_dec_10450);
    DeRef(_e_mag_10451);
    DeRef(_5909);
    _5909 = NOVALUE;
    DeRef(_5920);
    _5920 = NOVALUE;
    DeRef(_5921);
    _5921 = NOVALUE;
    DeRef(_5922);
    _5922 = NOVALUE;
    DeRef(_5930);
    _5930 = NOVALUE;
    return _5939;
LD: 

    /** 	mantissa = sign * mantissa*/
    _0 = _mantissa_10449;
    if (IS_ATOM_INT(_mantissa_10449)) {
        if (_sign_10445 == (short)_sign_10445 && _mantissa_10449 <= INT15 && _mantissa_10449 >= -INT15)
        _mantissa_10449 = _sign_10445 * _mantissa_10449;
        else
        _mantissa_10449 = NewDouble(_sign_10445 * (double)_mantissa_10449);
    }
    else {
        _mantissa_10449 = NewDouble((double)_sign_10445 * DBL_PTR(_mantissa_10449)->dbl);
    }
    DeRef(_0);

    /** 	if ch = 'e' or ch = 'E' then*/
    _5941 = (_4ch_10308 == 101);
    if (_5941 != 0) {
        goto LE; // [339] 354
    }
    _5943 = (_4ch_10308 == 69);
    if (_5943 == 0)
    {
        DeRef(_5943);
        _5943 = NOVALUE;
        goto LF; // [350] 575
    }
    else{
        DeRef(_5943);
        _5943 = NOVALUE;
    }
LE: 

    /** 		e_sign = +1*/
    _e_sign_10446 = 1;

    /** 		e_mag = 0*/
    DeRef(_e_mag_10451);
    _e_mag_10451 = 0;

    /** 		get_ch()*/
    _4get_ch();

    /** 		if ch = '-' then*/
    if (_4ch_10308 != 45)
    goto L10; // [372] 388

    /** 			e_sign = -1*/
    _e_sign_10446 = -1;

    /** 			get_ch()*/
    _4get_ch();
    goto L11; // [385] 402
L10: 

    /** 		elsif ch = '+' then*/
    if (_4ch_10308 != 43)
    goto L12; // [392] 401

    /** 			get_ch()*/
    _4get_ch();
L12: 
L11: 

    /** 		if ch >= '0' and ch <= '9' then*/
    _5946 = (_4ch_10308 >= 48);
    if (_5946 == 0) {
        goto L13; // [410] 489
    }
    _5948 = (_4ch_10308 <= 57);
    if (_5948 == 0)
    {
        DeRef(_5948);
        _5948 = NOVALUE;
        goto L13; // [421] 489
    }
    else{
        DeRef(_5948);
        _5948 = NOVALUE;
    }

    /** 			e_mag = ch - '0'*/
    DeRef(_e_mag_10451);
    _e_mag_10451 = _4ch_10308 - 48;
    if ((long)((unsigned long)_e_mag_10451 +(unsigned long) HIGH_BITS) >= 0){
        _e_mag_10451 = NewDouble((double)_e_mag_10451);
    }

    /** 			get_ch()*/
    _4get_ch();

    /** 			while ch >= '0' and ch <= '9' do*/
L14: 
    _5950 = (_4ch_10308 >= 48);
    if (_5950 == 0) {
        goto L15; // [447] 500
    }
    _5952 = (_4ch_10308 <= 57);
    if (_5952 == 0)
    {
        DeRef(_5952);
        _5952 = NOVALUE;
        goto L15; // [458] 500
    }
    else{
        DeRef(_5952);
        _5952 = NOVALUE;
    }

    /** 				e_mag = e_mag * 10 + ch - '0'*/
    if (IS_ATOM_INT(_e_mag_10451)) {
        if (_e_mag_10451 == (short)_e_mag_10451)
        _5953 = _e_mag_10451 * 10;
        else
        _5953 = NewDouble(_e_mag_10451 * (double)10);
    }
    else {
        _5953 = NewDouble(DBL_PTR(_e_mag_10451)->dbl * (double)10);
    }
    if (IS_ATOM_INT(_5953)) {
        _5954 = _5953 + _4ch_10308;
        if ((long)((unsigned long)_5954 + (unsigned long)HIGH_BITS) >= 0) 
        _5954 = NewDouble((double)_5954);
    }
    else {
        _5954 = NewDouble(DBL_PTR(_5953)->dbl + (double)_4ch_10308);
    }
    DeRef(_5953);
    _5953 = NOVALUE;
    DeRef(_e_mag_10451);
    if (IS_ATOM_INT(_5954)) {
        _e_mag_10451 = _5954 - 48;
        if ((long)((unsigned long)_e_mag_10451 +(unsigned long) HIGH_BITS) >= 0){
            _e_mag_10451 = NewDouble((double)_e_mag_10451);
        }
    }
    else {
        _e_mag_10451 = NewDouble(DBL_PTR(_5954)->dbl - (double)48);
    }
    DeRef(_5954);
    _5954 = NOVALUE;

    /** 				get_ch()*/
    _4get_ch();

    /** 			end while*/
    goto L14; // [483] 441
    goto L15; // [486] 500
L13: 

    /** 			return {GET_FAIL, 0} -- no exponent*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5956 = MAKE_SEQ(_1);
    DeRef(_mantissa_10449);
    DeRef(_dec_10450);
    DeRef(_e_mag_10451);
    DeRef(_5909);
    _5909 = NOVALUE;
    DeRef(_5920);
    _5920 = NOVALUE;
    DeRef(_5921);
    _5921 = NOVALUE;
    DeRef(_5922);
    _5922 = NOVALUE;
    DeRef(_5930);
    _5930 = NOVALUE;
    DeRef(_5939);
    _5939 = NOVALUE;
    DeRef(_5941);
    _5941 = NOVALUE;
    DeRef(_5946);
    _5946 = NOVALUE;
    DeRef(_5950);
    _5950 = NOVALUE;
    return _5956;
L15: 

    /** 		e_mag *= e_sign*/
    _0 = _e_mag_10451;
    if (IS_ATOM_INT(_e_mag_10451)) {
        if (_e_mag_10451 == (short)_e_mag_10451 && _e_sign_10446 <= INT15 && _e_sign_10446 >= -INT15)
        _e_mag_10451 = _e_mag_10451 * _e_sign_10446;
        else
        _e_mag_10451 = NewDouble(_e_mag_10451 * (double)_e_sign_10446);
    }
    else {
        _e_mag_10451 = NewDouble(DBL_PTR(_e_mag_10451)->dbl * (double)_e_sign_10446);
    }
    DeRef(_0);

    /** 		if e_mag > 308 then*/
    if (binary_op_a(LESSEQ, _e_mag_10451, 308)){
        goto L16; // [508] 563
    }

    /** 			mantissa *= power(10, 308)*/
    _5960 = power(10, 308);
    _0 = _mantissa_10449;
    if (IS_ATOM_INT(_mantissa_10449) && IS_ATOM_INT(_5960)) {
        if (_mantissa_10449 == (short)_mantissa_10449 && _5960 <= INT15 && _5960 >= -INT15)
        _mantissa_10449 = _mantissa_10449 * _5960;
        else
        _mantissa_10449 = NewDouble(_mantissa_10449 * (double)_5960);
    }
    else {
        if (IS_ATOM_INT(_mantissa_10449)) {
            _mantissa_10449 = NewDouble((double)_mantissa_10449 * DBL_PTR(_5960)->dbl);
        }
        else {
            if (IS_ATOM_INT(_5960)) {
                _mantissa_10449 = NewDouble(DBL_PTR(_mantissa_10449)->dbl * (double)_5960);
            }
            else
            _mantissa_10449 = NewDouble(DBL_PTR(_mantissa_10449)->dbl * DBL_PTR(_5960)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_5960);
    _5960 = NOVALUE;

    /** 			if e_mag > 1000 then*/
    if (binary_op_a(LESSEQ, _e_mag_10451, 1000)){
        goto L17; // [524] 534
    }

    /** 				e_mag = 1000*/
    DeRef(_e_mag_10451);
    _e_mag_10451 = 1000;
L17: 

    /** 			for i = 1 to e_mag - 308 do*/
    if (IS_ATOM_INT(_e_mag_10451)) {
        _5963 = _e_mag_10451 - 308;
        if ((long)((unsigned long)_5963 +(unsigned long) HIGH_BITS) >= 0){
            _5963 = NewDouble((double)_5963);
        }
    }
    else {
        _5963 = NewDouble(DBL_PTR(_e_mag_10451)->dbl - (double)308);
    }
    {
        int _i_10530;
        _i_10530 = 1;
L18: 
        if (binary_op_a(GREATER, _i_10530, _5963)){
            goto L19; // [540] 560
        }

        /** 				mantissa *= 10*/
        _0 = _mantissa_10449;
        if (IS_ATOM_INT(_mantissa_10449)) {
            if (_mantissa_10449 == (short)_mantissa_10449)
            _mantissa_10449 = _mantissa_10449 * 10;
            else
            _mantissa_10449 = NewDouble(_mantissa_10449 * (double)10);
        }
        else {
            _mantissa_10449 = NewDouble(DBL_PTR(_mantissa_10449)->dbl * (double)10);
        }
        DeRef(_0);

        /** 			end for*/
        _0 = _i_10530;
        if (IS_ATOM_INT(_i_10530)) {
            _i_10530 = _i_10530 + 1;
            if ((long)((unsigned long)_i_10530 +(unsigned long) HIGH_BITS) >= 0){
                _i_10530 = NewDouble((double)_i_10530);
            }
        }
        else {
            _i_10530 = binary_op_a(PLUS, _i_10530, 1);
        }
        DeRef(_0);
        goto L18; // [555] 547
L19: 
        ;
        DeRef(_i_10530);
    }
    goto L1A; // [560] 574
L16: 

    /** 			mantissa *= power(10, e_mag)*/
    if (IS_ATOM_INT(_e_mag_10451)) {
        _5965 = power(10, _e_mag_10451);
    }
    else {
        temp_d.dbl = (double)10;
        _5965 = Dpower(&temp_d, DBL_PTR(_e_mag_10451));
    }
    _0 = _mantissa_10449;
    if (IS_ATOM_INT(_mantissa_10449) && IS_ATOM_INT(_5965)) {
        if (_mantissa_10449 == (short)_mantissa_10449 && _5965 <= INT15 && _5965 >= -INT15)
        _mantissa_10449 = _mantissa_10449 * _5965;
        else
        _mantissa_10449 = NewDouble(_mantissa_10449 * (double)_5965);
    }
    else {
        if (IS_ATOM_INT(_mantissa_10449)) {
            _mantissa_10449 = NewDouble((double)_mantissa_10449 * DBL_PTR(_5965)->dbl);
        }
        else {
            if (IS_ATOM_INT(_5965)) {
                _mantissa_10449 = NewDouble(DBL_PTR(_mantissa_10449)->dbl * (double)_5965);
            }
            else
            _mantissa_10449 = NewDouble(DBL_PTR(_mantissa_10449)->dbl * DBL_PTR(_5965)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_5965);
    _5965 = NOVALUE;
L1A: 
LF: 

    /** 	return {GET_SUCCESS, mantissa}*/
    Ref(_mantissa_10449);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _mantissa_10449;
    _5967 = MAKE_SEQ(_1);
    DeRef(_mantissa_10449);
    DeRef(_dec_10450);
    DeRef(_e_mag_10451);
    DeRef(_5909);
    _5909 = NOVALUE;
    DeRef(_5920);
    _5920 = NOVALUE;
    DeRef(_5921);
    _5921 = NOVALUE;
    DeRef(_5922);
    _5922 = NOVALUE;
    DeRef(_5930);
    _5930 = NOVALUE;
    DeRef(_5939);
    _5939 = NOVALUE;
    DeRef(_5941);
    _5941 = NOVALUE;
    DeRef(_5946);
    _5946 = NOVALUE;
    DeRef(_5950);
    _5950 = NOVALUE;
    DeRef(_5956);
    _5956 = NOVALUE;
    DeRef(_5963);
    _5963 = NOVALUE;
    return _5967;
    ;
}


int _4Get()
{
    int _skip_blanks_1__tmp_at330_10583 = NOVALUE;
    int _skip_blanks_1__tmp_at177_10564 = NOVALUE;
    int _skip_blanks_1__tmp_at88_10555 = NOVALUE;
    int _s_10539 = NOVALUE;
    int _e_10540 = NOVALUE;
    int _e1_10541 = NOVALUE;
    int _6006 = NOVALUE;
    int _6005 = NOVALUE;
    int _6003 = NOVALUE;
    int _6000 = NOVALUE;
    int _5998 = NOVALUE;
    int _5996 = NOVALUE;
    int _5994 = NOVALUE;
    int _5991 = NOVALUE;
    int _5989 = NOVALUE;
    int _5985 = NOVALUE;
    int _5981 = NOVALUE;
    int _5978 = NOVALUE;
    int _5977 = NOVALUE;
    int _5975 = NOVALUE;
    int _5973 = NOVALUE;
    int _5971 = NOVALUE;
    int _5970 = NOVALUE;
    int _5968 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while find(ch, white_space) do*/
L1: 
    _5968 = find_from(_4ch_10308, _4white_space_10324, 1);
    if (_5968 == 0)
    {
        _5968 = NOVALUE;
        goto L2; // [13] 25
    }
    else{
        _5968 = NOVALUE;
    }

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto L1; // [22] 6
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_4ch_10308 != -1)
    goto L3; // [29] 44

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _5970 = MAKE_SEQ(_1);
    DeRef(_s_10539);
    DeRef(_e_10540);
    return _5970;
L3: 

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _5971 = find_from(_4ch_10308, _4START_NUMERIC_10291, 1);
    if (_5971 == 0)
    {
        _5971 = NOVALUE;
        goto L5; // [60] 157
    }
    else{
        _5971 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_10540;
    _e_10540 = _4get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_10540);
    _5973 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _5973, -2)){
        _5973 = NOVALUE;
        goto L6; // [76] 87
    }
    _5973 = NOVALUE;

    /** 				return e*/
    DeRef(_s_10539);
    DeRef(_5970);
    _5970 = NOVALUE;
    return _e_10540;
L6: 

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L7: 
    _skip_blanks_1__tmp_at88_10555 = find_from(_4ch_10308, _4white_space_10324, 1);
    if (_skip_blanks_1__tmp_at88_10555 == 0)
    {
        goto L8; // [101] 118
    }
    else{
    }

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto L7; // [110] 94

    /** end procedure*/
    goto L8; // [115] 118
L8: 

    /** 			if ch=-1 or ch='}' then -- '}' is expected only in the "{--\n}" case*/
    _5975 = (_4ch_10308 == -1);
    if (_5975 != 0) {
        goto L9; // [128] 143
    }
    _5977 = (_4ch_10308 == 125);
    if (_5977 == 0)
    {
        DeRef(_5977);
        _5977 = NOVALUE;
        goto L4; // [139] 49
    }
    else{
        DeRef(_5977);
        _5977 = NOVALUE;
    }
L9: 

    /** 				return {GET_NOTHING, 0} -- just a comment*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _5978 = MAKE_SEQ(_1);
    DeRef(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    return _5978;
    goto L4; // [154] 49
L5: 

    /** 		elsif ch = '{' then*/
    if (_4ch_10308 != 123)
    goto LA; // [161] 467

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_10539);
    _s_10539 = _5;

    /** 			get_ch()*/
    _4get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
LB: 
    _skip_blanks_1__tmp_at177_10564 = find_from(_4ch_10308, _4white_space_10324, 1);
    if (_skip_blanks_1__tmp_at177_10564 == 0)
    {
        goto LC; // [190] 207
    }
    else{
    }

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto LB; // [199] 183

    /** end procedure*/
    goto LC; // [204] 207
LC: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_4ch_10308 != 125)
    goto LD; // [213] 232

    /** 				get_ch()*/
    _4get_ch();

    /** 				return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_10539);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_10539;
    _5981 = MAKE_SEQ(_1);
    DeRefDS(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    return _5981;
LD: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LE: 

    /** 				while 1 do -- read zero or more comments and an element*/
LF: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_10540;
    _e_10540 = _4Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_10540);
    _e1_10541 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_10541))
    _e1_10541 = (long)DBL_PTR(_e1_10541)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_10541 != 0)
    goto L10; // [259] 280

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_10540);
    _5985 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_5985);
    Append(&_s_10539, _s_10539, _5985);
    _5985 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto L11; // [275] 324
    goto LF; // [277] 242
L10: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_10541 == -2)
    goto L12; // [282] 295

    /** 						return e*/
    DeRef(_s_10539);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    return _e_10540;
    goto LF; // [292] 242
L12: 

    /** 					elsif ch='}' then*/
    if (_4ch_10308 != 125)
    goto LF; // [299] 242

    /** 						get_ch()*/
    _4get_ch();

    /** 						return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_10539);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_10539;
    _5989 = MAKE_SEQ(_1);
    DeRefDS(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    return _5989;

    /** 				end while*/
    goto LF; // [321] 242
L11: 

    /** 				while 1 do -- now read zero or more post element comments*/
L13: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L14: 
    _skip_blanks_1__tmp_at330_10583 = find_from(_4ch_10308, _4white_space_10324, 1);
    if (_skip_blanks_1__tmp_at330_10583 == 0)
    {
        goto L15; // [343] 360
    }
    else{
    }

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto L14; // [352] 336

    /** end procedure*/
    goto L15; // [357] 360
L15: 

    /** 					if ch = '}' then*/
    if (_4ch_10308 != 125)
    goto L16; // [366] 387

    /** 						get_ch()*/
    _4get_ch();

    /** 					return {GET_SUCCESS, s}*/
    RefDS(_s_10539);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_10539;
    _5991 = MAKE_SEQ(_1);
    DeRefDS(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    DeRef(_5989);
    _5989 = NOVALUE;
    return _5991;
    goto L13; // [384] 329
L16: 

    /** 					elsif ch!='-' then*/
    if (_4ch_10308 == 45)
    goto L17; // [391] 402

    /** 						exit*/
    goto L18; // [397] 436
    goto L13; // [399] 329
L17: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_10540;
    _e_10540 = _4get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it wasn't a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_10540);
    _5994 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _5994, -2)){
        _5994 = NOVALUE;
        goto L13; // [415] 329
    }
    _5994 = NOVALUE;

    /** 							return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5996 = MAKE_SEQ(_1);
    DeRef(_s_10539);
    DeRefDS(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    DeRef(_5989);
    _5989 = NOVALUE;
    DeRef(_5991);
    _5991 = NOVALUE;
    return _5996;

    /** 			end while*/
    goto L13; // [433] 329
L18: 

    /** 				if ch != ',' then*/
    if (_4ch_10308 == 44)
    goto L19; // [440] 455

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _5998 = MAKE_SEQ(_1);
    DeRef(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    DeRef(_5989);
    _5989 = NOVALUE;
    DeRef(_5991);
    _5991 = NOVALUE;
    DeRef(_5996);
    _5996 = NOVALUE;
    return _5998;
L19: 

    /** 			get_ch() -- skip comma*/
    _4get_ch();

    /** 			end while*/
    goto LE; // [461] 237
    goto L4; // [464] 49
LA: 

    /** 		elsif ch = '\"' then*/
    if (_4ch_10308 != 34)
    goto L1A; // [471] 487

    /** 			return get_string()*/
    _6000 = _4get_string();
    DeRef(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    DeRef(_5989);
    _5989 = NOVALUE;
    DeRef(_5991);
    _5991 = NOVALUE;
    DeRef(_5996);
    _5996 = NOVALUE;
    DeRef(_5998);
    _5998 = NOVALUE;
    return _6000;
    goto L4; // [484] 49
L1A: 

    /** 		elsif ch = '`' then*/
    if (_4ch_10308 != 96)
    goto L1B; // [491] 508

    /** 			return get_heredoc("`")*/
    RefDS(_6002);
    _6003 = _4get_heredoc(_6002);
    DeRef(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    DeRef(_5989);
    _5989 = NOVALUE;
    DeRef(_5991);
    _5991 = NOVALUE;
    DeRef(_5996);
    _5996 = NOVALUE;
    DeRef(_5998);
    _5998 = NOVALUE;
    DeRef(_6000);
    _6000 = NOVALUE;
    return _6003;
    goto L4; // [505] 49
L1B: 

    /** 		elsif ch = '\'' then*/
    if (_4ch_10308 != 39)
    goto L1C; // [512] 528

    /** 			return get_qchar()*/
    _6005 = _4get_qchar();
    DeRef(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    DeRef(_5989);
    _5989 = NOVALUE;
    DeRef(_5991);
    _5991 = NOVALUE;
    DeRef(_5996);
    _5996 = NOVALUE;
    DeRef(_5998);
    _5998 = NOVALUE;
    DeRef(_6000);
    _6000 = NOVALUE;
    DeRef(_6003);
    _6003 = NOVALUE;
    return _6005;
    goto L4; // [525] 49
L1C: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _6006 = MAKE_SEQ(_1);
    DeRef(_s_10539);
    DeRef(_e_10540);
    DeRef(_5970);
    _5970 = NOVALUE;
    DeRef(_5975);
    _5975 = NOVALUE;
    DeRef(_5978);
    _5978 = NOVALUE;
    DeRef(_5981);
    _5981 = NOVALUE;
    DeRef(_5989);
    _5989 = NOVALUE;
    DeRef(_5991);
    _5991 = NOVALUE;
    DeRef(_5996);
    _5996 = NOVALUE;
    DeRef(_5998);
    _5998 = NOVALUE;
    DeRef(_6000);
    _6000 = NOVALUE;
    DeRef(_6003);
    _6003 = NOVALUE;
    DeRef(_6005);
    _6005 = NOVALUE;
    return _6006;

    /** 	end while*/
    goto L4; // [541] 49
    ;
}


int _4Get2()
{
    int _skip_blanks_1__tmp_at468_10684 = NOVALUE;
    int _skip_blanks_1__tmp_at235_10651 = NOVALUE;
    int _s_10613 = NOVALUE;
    int _e_10614 = NOVALUE;
    int _e1_10615 = NOVALUE;
    int _offset_10616 = NOVALUE;
    int _6106 = NOVALUE;
    int _6105 = NOVALUE;
    int _6104 = NOVALUE;
    int _6103 = NOVALUE;
    int _6102 = NOVALUE;
    int _6101 = NOVALUE;
    int _6100 = NOVALUE;
    int _6099 = NOVALUE;
    int _6098 = NOVALUE;
    int _6097 = NOVALUE;
    int _6096 = NOVALUE;
    int _6093 = NOVALUE;
    int _6092 = NOVALUE;
    int _6091 = NOVALUE;
    int _6090 = NOVALUE;
    int _6089 = NOVALUE;
    int _6088 = NOVALUE;
    int _6085 = NOVALUE;
    int _6084 = NOVALUE;
    int _6083 = NOVALUE;
    int _6082 = NOVALUE;
    int _6081 = NOVALUE;
    int _6080 = NOVALUE;
    int _6077 = NOVALUE;
    int _6076 = NOVALUE;
    int _6075 = NOVALUE;
    int _6074 = NOVALUE;
    int _6073 = NOVALUE;
    int _6071 = NOVALUE;
    int _6070 = NOVALUE;
    int _6069 = NOVALUE;
    int _6068 = NOVALUE;
    int _6067 = NOVALUE;
    int _6065 = NOVALUE;
    int _6062 = NOVALUE;
    int _6061 = NOVALUE;
    int _6060 = NOVALUE;
    int _6059 = NOVALUE;
    int _6058 = NOVALUE;
    int _6056 = NOVALUE;
    int _6055 = NOVALUE;
    int _6054 = NOVALUE;
    int _6053 = NOVALUE;
    int _6052 = NOVALUE;
    int _6050 = NOVALUE;
    int _6049 = NOVALUE;
    int _6048 = NOVALUE;
    int _6047 = NOVALUE;
    int _6046 = NOVALUE;
    int _6045 = NOVALUE;
    int _6042 = NOVALUE;
    int _6038 = NOVALUE;
    int _6037 = NOVALUE;
    int _6036 = NOVALUE;
    int _6035 = NOVALUE;
    int _6034 = NOVALUE;
    int _6031 = NOVALUE;
    int _6030 = NOVALUE;
    int _6029 = NOVALUE;
    int _6028 = NOVALUE;
    int _6027 = NOVALUE;
    int _6025 = NOVALUE;
    int _6024 = NOVALUE;
    int _6023 = NOVALUE;
    int _6022 = NOVALUE;
    int _6021 = NOVALUE;
    int _6020 = NOVALUE;
    int _6018 = NOVALUE;
    int _6016 = NOVALUE;
    int _6014 = NOVALUE;
    int _6013 = NOVALUE;
    int _6012 = NOVALUE;
    int _6011 = NOVALUE;
    int _6010 = NOVALUE;
    int _6008 = NOVALUE;
    int _0, _1, _2;
    

    /** 	offset = string_next-1*/
    _offset_10616 = _4string_next_10307 - 1;

    /** 	get_ch()*/
    _4get_ch();

    /** 	while find(ch, white_space) do*/
L1: 
    _6008 = find_from(_4ch_10308, _4white_space_10324, 1);
    if (_6008 == 0)
    {
        _6008 = NOVALUE;
        goto L2; // [25] 37
    }
    else{
        _6008 = NOVALUE;
    }

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto L1; // [34] 18
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_4ch_10308 != -1)
    goto L3; // [41] 75

    /** 		return {GET_EOF, 0, string_next-1-offset ,string_next-1}*/
    _6010 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6010 +(unsigned long) HIGH_BITS) >= 0){
        _6010 = NewDouble((double)_6010);
    }
    if (IS_ATOM_INT(_6010)) {
        _6011 = _6010 - _offset_10616;
        if ((long)((unsigned long)_6011 +(unsigned long) HIGH_BITS) >= 0){
            _6011 = NewDouble((double)_6011);
        }
    }
    else {
        _6011 = NewDouble(DBL_PTR(_6010)->dbl - (double)_offset_10616);
    }
    DeRef(_6010);
    _6010 = NOVALUE;
    _6012 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6012 +(unsigned long) HIGH_BITS) >= 0){
        _6012 = NewDouble((double)_6012);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _6011;
    *((int *)(_2+16)) = _6012;
    _6013 = MAKE_SEQ(_1);
    _6012 = NOVALUE;
    _6011 = NOVALUE;
    DeRef(_s_10613);
    DeRef(_e_10614);
    return _6013;
L3: 

    /** 	leading_whitespace = string_next-2-offset -- index of the last whitespace: string_next points past the first non whitespace*/
    _6014 = _4string_next_10307 - 2;
    if ((long)((unsigned long)_6014 +(unsigned long) HIGH_BITS) >= 0){
        _6014 = NewDouble((double)_6014);
    }
    if (IS_ATOM_INT(_6014)) {
        _4leading_whitespace_10610 = _6014 - _offset_10616;
    }
    else {
        _4leading_whitespace_10610 = NewDouble(DBL_PTR(_6014)->dbl - (double)_offset_10616);
    }
    DeRef(_6014);
    _6014 = NOVALUE;
    if (!IS_ATOM_INT(_4leading_whitespace_10610)) {
        _1 = (long)(DBL_PTR(_4leading_whitespace_10610)->dbl);
        if (UNIQUE(DBL_PTR(_4leading_whitespace_10610)) && (DBL_PTR(_4leading_whitespace_10610)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4leading_whitespace_10610);
        _4leading_whitespace_10610 = _1;
    }

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _6016 = find_from(_4ch_10308, _4START_NUMERIC_10291, 1);
    if (_6016 == 0)
    {
        _6016 = NOVALUE;
        goto L5; // [107] 215
    }
    else{
        _6016 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_10614;
    _e_10614 = _4get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_10614);
    _6018 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _6018, -2)){
        _6018 = NOVALUE;
        goto L6; // [123] 164
    }
    _6018 = NOVALUE;

    /** 				return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6020 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6020 +(unsigned long) HIGH_BITS) >= 0){
        _6020 = NewDouble((double)_6020);
    }
    if (IS_ATOM_INT(_6020)) {
        _6021 = _6020 - _offset_10616;
        if ((long)((unsigned long)_6021 +(unsigned long) HIGH_BITS) >= 0){
            _6021 = NewDouble((double)_6021);
        }
    }
    else {
        _6021 = NewDouble(DBL_PTR(_6020)->dbl - (double)_offset_10616);
    }
    DeRef(_6020);
    _6020 = NOVALUE;
    _6022 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6021)) {
        _6023 = _6021 - _6022;
        if ((long)((unsigned long)_6023 +(unsigned long) HIGH_BITS) >= 0){
            _6023 = NewDouble((double)_6023);
        }
    }
    else {
        _6023 = NewDouble(DBL_PTR(_6021)->dbl - (double)_6022);
    }
    DeRef(_6021);
    _6021 = NOVALUE;
    _6022 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6023;
    ((int *)_2)[2] = _4leading_whitespace_10610;
    _6024 = MAKE_SEQ(_1);
    _6023 = NOVALUE;
    Concat((object_ptr)&_6025, _e_10614, _6024);
    DeRefDS(_6024);
    _6024 = NOVALUE;
    DeRef(_s_10613);
    DeRefDS(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    return _6025;
L6: 

    /** 			get_ch()*/
    _4get_ch();

    /** 			if ch=-1 then*/
    if (_4ch_10308 != -1)
    goto L4; // [172] 96

    /** 				return {GET_NOTHING, 0, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _6027 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6027 +(unsigned long) HIGH_BITS) >= 0){
        _6027 = NewDouble((double)_6027);
    }
    if (IS_ATOM_INT(_6027)) {
        _6028 = _6027 - _offset_10616;
        if ((long)((unsigned long)_6028 +(unsigned long) HIGH_BITS) >= 0){
            _6028 = NewDouble((double)_6028);
        }
    }
    else {
        _6028 = NewDouble(DBL_PTR(_6027)->dbl - (double)_offset_10616);
    }
    DeRef(_6027);
    _6027 = NOVALUE;
    _6029 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6028)) {
        _6030 = _6028 - _6029;
        if ((long)((unsigned long)_6030 +(unsigned long) HIGH_BITS) >= 0){
            _6030 = NewDouble((double)_6030);
        }
    }
    else {
        _6030 = NewDouble(DBL_PTR(_6028)->dbl - (double)_6029);
    }
    DeRef(_6028);
    _6028 = NOVALUE;
    _6029 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _6030;
    *((int *)(_2+16)) = _4leading_whitespace_10610;
    _6031 = MAKE_SEQ(_1);
    _6030 = NOVALUE;
    DeRef(_s_10613);
    DeRef(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    return _6031;
    goto L4; // [212] 96
L5: 

    /** 		elsif ch = '{' then*/
    if (_4ch_10308 != 123)
    goto L7; // [219] 680

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_10613);
    _s_10613 = _5;

    /** 			get_ch()*/
    _4get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L8: 
    _skip_blanks_1__tmp_at235_10651 = find_from(_4ch_10308, _4white_space_10324, 1);
    if (_skip_blanks_1__tmp_at235_10651 == 0)
    {
        goto L9; // [248] 265
    }
    else{
    }

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto L8; // [257] 241

    /** end procedure*/
    goto L9; // [262] 265
L9: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_4ch_10308 != 125)
    goto LA; // [271] 315

    /** 				get_ch()*/
    _4get_ch();

    /** 				return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _6034 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6034 +(unsigned long) HIGH_BITS) >= 0){
        _6034 = NewDouble((double)_6034);
    }
    if (IS_ATOM_INT(_6034)) {
        _6035 = _6034 - _offset_10616;
        if ((long)((unsigned long)_6035 +(unsigned long) HIGH_BITS) >= 0){
            _6035 = NewDouble((double)_6035);
        }
    }
    else {
        _6035 = NewDouble(DBL_PTR(_6034)->dbl - (double)_offset_10616);
    }
    DeRef(_6034);
    _6034 = NOVALUE;
    _6036 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6035)) {
        _6037 = _6035 - _6036;
        if ((long)((unsigned long)_6037 +(unsigned long) HIGH_BITS) >= 0){
            _6037 = NewDouble((double)_6037);
        }
    }
    else {
        _6037 = NewDouble(DBL_PTR(_6035)->dbl - (double)_6036);
    }
    DeRef(_6035);
    _6035 = NOVALUE;
    _6036 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_10613);
    *((int *)(_2+8)) = _s_10613;
    *((int *)(_2+12)) = _6037;
    *((int *)(_2+16)) = _4leading_whitespace_10610;
    _6038 = MAKE_SEQ(_1);
    _6037 = NOVALUE;
    DeRefDS(_s_10613);
    DeRef(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    return _6038;
LA: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LB: 

    /** 				while 1 do -- read zero or more comments and an element*/
LC: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_10614;
    _e_10614 = _4Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_10614);
    _e1_10615 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_10615))
    _e1_10615 = (long)DBL_PTR(_e1_10615)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_10615 != 0)
    goto LD; // [342] 363

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_10614);
    _6042 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_6042);
    Append(&_s_10613, _s_10613, _6042);
    _6042 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto LE; // [358] 462
    goto LC; // [360] 325
LD: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_10615 == -2)
    goto LF; // [365] 408

    /** 						return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6045 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6045 +(unsigned long) HIGH_BITS) >= 0){
        _6045 = NewDouble((double)_6045);
    }
    if (IS_ATOM_INT(_6045)) {
        _6046 = _6045 - _offset_10616;
        if ((long)((unsigned long)_6046 +(unsigned long) HIGH_BITS) >= 0){
            _6046 = NewDouble((double)_6046);
        }
    }
    else {
        _6046 = NewDouble(DBL_PTR(_6045)->dbl - (double)_offset_10616);
    }
    DeRef(_6045);
    _6045 = NOVALUE;
    _6047 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6046)) {
        _6048 = _6046 - _6047;
        if ((long)((unsigned long)_6048 +(unsigned long) HIGH_BITS) >= 0){
            _6048 = NewDouble((double)_6048);
        }
    }
    else {
        _6048 = NewDouble(DBL_PTR(_6046)->dbl - (double)_6047);
    }
    DeRef(_6046);
    _6046 = NOVALUE;
    _6047 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6048;
    ((int *)_2)[2] = _4leading_whitespace_10610;
    _6049 = MAKE_SEQ(_1);
    _6048 = NOVALUE;
    Concat((object_ptr)&_6050, _e_10614, _6049);
    DeRefDS(_6049);
    _6049 = NOVALUE;
    DeRef(_s_10613);
    DeRefDS(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    return _6050;
    goto LC; // [405] 325
LF: 

    /** 					elsif ch='}' then*/
    if (_4ch_10308 != 125)
    goto LC; // [412] 325

    /** 						get_ch()*/
    _4get_ch();

    /** 						return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1),leading_whitespace} -- empty sequence*/
    _6052 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6052 +(unsigned long) HIGH_BITS) >= 0){
        _6052 = NewDouble((double)_6052);
    }
    if (IS_ATOM_INT(_6052)) {
        _6053 = _6052 - _offset_10616;
        if ((long)((unsigned long)_6053 +(unsigned long) HIGH_BITS) >= 0){
            _6053 = NewDouble((double)_6053);
        }
    }
    else {
        _6053 = NewDouble(DBL_PTR(_6052)->dbl - (double)_offset_10616);
    }
    DeRef(_6052);
    _6052 = NOVALUE;
    _6054 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6053)) {
        _6055 = _6053 - _6054;
        if ((long)((unsigned long)_6055 +(unsigned long) HIGH_BITS) >= 0){
            _6055 = NewDouble((double)_6055);
        }
    }
    else {
        _6055 = NewDouble(DBL_PTR(_6053)->dbl - (double)_6054);
    }
    DeRef(_6053);
    _6053 = NOVALUE;
    _6054 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_10613);
    *((int *)(_2+8)) = _s_10613;
    *((int *)(_2+12)) = _6055;
    *((int *)(_2+16)) = _4leading_whitespace_10610;
    _6056 = MAKE_SEQ(_1);
    _6055 = NOVALUE;
    DeRefDS(_s_10613);
    DeRef(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    return _6056;

    /** 				end while*/
    goto LC; // [459] 325
LE: 

    /** 				while 1 do -- now read zero or more post element comments*/
L10: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L11: 
    _skip_blanks_1__tmp_at468_10684 = find_from(_4ch_10308, _4white_space_10324, 1);
    if (_skip_blanks_1__tmp_at468_10684 == 0)
    {
        goto L12; // [481] 498
    }
    else{
    }

    /** 		get_ch()*/
    _4get_ch();

    /** 	end while*/
    goto L11; // [490] 474

    /** end procedure*/
    goto L12; // [495] 498
L12: 

    /** 					if ch = '}' then*/
    if (_4ch_10308 != 125)
    goto L13; // [504] 550

    /** 						get_ch()*/
    _4get_ch();

    /** 					return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6058 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6058 +(unsigned long) HIGH_BITS) >= 0){
        _6058 = NewDouble((double)_6058);
    }
    if (IS_ATOM_INT(_6058)) {
        _6059 = _6058 - _offset_10616;
        if ((long)((unsigned long)_6059 +(unsigned long) HIGH_BITS) >= 0){
            _6059 = NewDouble((double)_6059);
        }
    }
    else {
        _6059 = NewDouble(DBL_PTR(_6058)->dbl - (double)_offset_10616);
    }
    DeRef(_6058);
    _6058 = NOVALUE;
    _6060 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6059)) {
        _6061 = _6059 - _6060;
        if ((long)((unsigned long)_6061 +(unsigned long) HIGH_BITS) >= 0){
            _6061 = NewDouble((double)_6061);
        }
    }
    else {
        _6061 = NewDouble(DBL_PTR(_6059)->dbl - (double)_6060);
    }
    DeRef(_6059);
    _6059 = NOVALUE;
    _6060 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_10613);
    *((int *)(_2+8)) = _s_10613;
    *((int *)(_2+12)) = _6061;
    *((int *)(_2+16)) = _4leading_whitespace_10610;
    _6062 = MAKE_SEQ(_1);
    _6061 = NOVALUE;
    DeRefDS(_s_10613);
    DeRef(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6056);
    _6056 = NOVALUE;
    return _6062;
    goto L10; // [547] 467
L13: 

    /** 					elsif ch!='-' then*/
    if (_4ch_10308 == 45)
    goto L14; // [554] 565

    /** 						exit*/
    goto L15; // [560] 624
    goto L10; // [562] 467
L14: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_10614;
    _e_10614 = _4get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it was not a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_10614);
    _6065 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _6065, -2)){
        _6065 = NOVALUE;
        goto L10; // [578] 467
    }
    _6065 = NOVALUE;

    /** 							return {GET_FAIL, 0, string_next-1-offset-(ch!=-1),leading_whitespace}*/
    _6067 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6067 +(unsigned long) HIGH_BITS) >= 0){
        _6067 = NewDouble((double)_6067);
    }
    if (IS_ATOM_INT(_6067)) {
        _6068 = _6067 - _offset_10616;
        if ((long)((unsigned long)_6068 +(unsigned long) HIGH_BITS) >= 0){
            _6068 = NewDouble((double)_6068);
        }
    }
    else {
        _6068 = NewDouble(DBL_PTR(_6067)->dbl - (double)_offset_10616);
    }
    DeRef(_6067);
    _6067 = NOVALUE;
    _6069 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6068)) {
        _6070 = _6068 - _6069;
        if ((long)((unsigned long)_6070 +(unsigned long) HIGH_BITS) >= 0){
            _6070 = NewDouble((double)_6070);
        }
    }
    else {
        _6070 = NewDouble(DBL_PTR(_6068)->dbl - (double)_6069);
    }
    DeRef(_6068);
    _6068 = NOVALUE;
    _6069 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _6070;
    *((int *)(_2+16)) = _4leading_whitespace_10610;
    _6071 = MAKE_SEQ(_1);
    _6070 = NOVALUE;
    DeRef(_s_10613);
    DeRefDS(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6056);
    _6056 = NOVALUE;
    DeRef(_6062);
    _6062 = NOVALUE;
    return _6071;

    /** 			end while*/
    goto L10; // [621] 467
L15: 

    /** 				if ch != ',' then*/
    if (_4ch_10308 == 44)
    goto L16; // [628] 668

    /** 				return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6073 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6073 +(unsigned long) HIGH_BITS) >= 0){
        _6073 = NewDouble((double)_6073);
    }
    if (IS_ATOM_INT(_6073)) {
        _6074 = _6073 - _offset_10616;
        if ((long)((unsigned long)_6074 +(unsigned long) HIGH_BITS) >= 0){
            _6074 = NewDouble((double)_6074);
        }
    }
    else {
        _6074 = NewDouble(DBL_PTR(_6073)->dbl - (double)_offset_10616);
    }
    DeRef(_6073);
    _6073 = NOVALUE;
    _6075 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6074)) {
        _6076 = _6074 - _6075;
        if ((long)((unsigned long)_6076 +(unsigned long) HIGH_BITS) >= 0){
            _6076 = NewDouble((double)_6076);
        }
    }
    else {
        _6076 = NewDouble(DBL_PTR(_6074)->dbl - (double)_6075);
    }
    DeRef(_6074);
    _6074 = NOVALUE;
    _6075 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _6076;
    *((int *)(_2+16)) = _4leading_whitespace_10610;
    _6077 = MAKE_SEQ(_1);
    _6076 = NOVALUE;
    DeRef(_s_10613);
    DeRef(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6056);
    _6056 = NOVALUE;
    DeRef(_6062);
    _6062 = NOVALUE;
    DeRef(_6071);
    _6071 = NOVALUE;
    return _6077;
L16: 

    /** 			get_ch() -- skip comma*/
    _4get_ch();

    /** 			end while*/
    goto LB; // [674] 320
    goto L4; // [677] 96
L7: 

    /** 		elsif ch = '\"' then*/
    if (_4ch_10308 != 34)
    goto L17; // [684] 734

    /** 			e = get_string()*/
    _0 = _e_10614;
    _e_10614 = _4get_string();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6080 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6080 +(unsigned long) HIGH_BITS) >= 0){
        _6080 = NewDouble((double)_6080);
    }
    if (IS_ATOM_INT(_6080)) {
        _6081 = _6080 - _offset_10616;
        if ((long)((unsigned long)_6081 +(unsigned long) HIGH_BITS) >= 0){
            _6081 = NewDouble((double)_6081);
        }
    }
    else {
        _6081 = NewDouble(DBL_PTR(_6080)->dbl - (double)_offset_10616);
    }
    DeRef(_6080);
    _6080 = NOVALUE;
    _6082 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6081)) {
        _6083 = _6081 - _6082;
        if ((long)((unsigned long)_6083 +(unsigned long) HIGH_BITS) >= 0){
            _6083 = NewDouble((double)_6083);
        }
    }
    else {
        _6083 = NewDouble(DBL_PTR(_6081)->dbl - (double)_6082);
    }
    DeRef(_6081);
    _6081 = NOVALUE;
    _6082 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6083;
    ((int *)_2)[2] = _4leading_whitespace_10610;
    _6084 = MAKE_SEQ(_1);
    _6083 = NOVALUE;
    Concat((object_ptr)&_6085, _e_10614, _6084);
    DeRefDS(_6084);
    _6084 = NOVALUE;
    DeRef(_s_10613);
    DeRefDS(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6056);
    _6056 = NOVALUE;
    DeRef(_6062);
    _6062 = NOVALUE;
    DeRef(_6071);
    _6071 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    return _6085;
    goto L4; // [731] 96
L17: 

    /** 		elsif ch = '`' then*/
    if (_4ch_10308 != 96)
    goto L18; // [738] 789

    /** 			e = get_heredoc("`")*/
    RefDS(_6002);
    _0 = _e_10614;
    _e_10614 = _4get_heredoc(_6002);
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6088 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6088 +(unsigned long) HIGH_BITS) >= 0){
        _6088 = NewDouble((double)_6088);
    }
    if (IS_ATOM_INT(_6088)) {
        _6089 = _6088 - _offset_10616;
        if ((long)((unsigned long)_6089 +(unsigned long) HIGH_BITS) >= 0){
            _6089 = NewDouble((double)_6089);
        }
    }
    else {
        _6089 = NewDouble(DBL_PTR(_6088)->dbl - (double)_offset_10616);
    }
    DeRef(_6088);
    _6088 = NOVALUE;
    _6090 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6089)) {
        _6091 = _6089 - _6090;
        if ((long)((unsigned long)_6091 +(unsigned long) HIGH_BITS) >= 0){
            _6091 = NewDouble((double)_6091);
        }
    }
    else {
        _6091 = NewDouble(DBL_PTR(_6089)->dbl - (double)_6090);
    }
    DeRef(_6089);
    _6089 = NOVALUE;
    _6090 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6091;
    ((int *)_2)[2] = _4leading_whitespace_10610;
    _6092 = MAKE_SEQ(_1);
    _6091 = NOVALUE;
    Concat((object_ptr)&_6093, _e_10614, _6092);
    DeRefDS(_6092);
    _6092 = NOVALUE;
    DeRef(_s_10613);
    DeRefDS(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6056);
    _6056 = NOVALUE;
    DeRef(_6062);
    _6062 = NOVALUE;
    DeRef(_6071);
    _6071 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    DeRef(_6085);
    _6085 = NOVALUE;
    return _6093;
    goto L4; // [786] 96
L18: 

    /** 		elsif ch = '\'' then*/
    if (_4ch_10308 != 39)
    goto L19; // [793] 843

    /** 			e = get_qchar()*/
    _0 = _e_10614;
    _e_10614 = _4get_qchar();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6096 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6096 +(unsigned long) HIGH_BITS) >= 0){
        _6096 = NewDouble((double)_6096);
    }
    if (IS_ATOM_INT(_6096)) {
        _6097 = _6096 - _offset_10616;
        if ((long)((unsigned long)_6097 +(unsigned long) HIGH_BITS) >= 0){
            _6097 = NewDouble((double)_6097);
        }
    }
    else {
        _6097 = NewDouble(DBL_PTR(_6096)->dbl - (double)_offset_10616);
    }
    DeRef(_6096);
    _6096 = NOVALUE;
    _6098 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6097)) {
        _6099 = _6097 - _6098;
        if ((long)((unsigned long)_6099 +(unsigned long) HIGH_BITS) >= 0){
            _6099 = NewDouble((double)_6099);
        }
    }
    else {
        _6099 = NewDouble(DBL_PTR(_6097)->dbl - (double)_6098);
    }
    DeRef(_6097);
    _6097 = NOVALUE;
    _6098 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6099;
    ((int *)_2)[2] = _4leading_whitespace_10610;
    _6100 = MAKE_SEQ(_1);
    _6099 = NOVALUE;
    Concat((object_ptr)&_6101, _e_10614, _6100);
    DeRefDS(_6100);
    _6100 = NOVALUE;
    DeRef(_s_10613);
    DeRefDS(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6056);
    _6056 = NOVALUE;
    DeRef(_6062);
    _6062 = NOVALUE;
    DeRef(_6071);
    _6071 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    DeRef(_6085);
    _6085 = NOVALUE;
    DeRef(_6093);
    _6093 = NOVALUE;
    return _6101;
    goto L4; // [840] 96
L19: 

    /** 			return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _6102 = _4string_next_10307 - 1;
    if ((long)((unsigned long)_6102 +(unsigned long) HIGH_BITS) >= 0){
        _6102 = NewDouble((double)_6102);
    }
    if (IS_ATOM_INT(_6102)) {
        _6103 = _6102 - _offset_10616;
        if ((long)((unsigned long)_6103 +(unsigned long) HIGH_BITS) >= 0){
            _6103 = NewDouble((double)_6103);
        }
    }
    else {
        _6103 = NewDouble(DBL_PTR(_6102)->dbl - (double)_offset_10616);
    }
    DeRef(_6102);
    _6102 = NOVALUE;
    _6104 = (_4ch_10308 != -1);
    if (IS_ATOM_INT(_6103)) {
        _6105 = _6103 - _6104;
        if ((long)((unsigned long)_6105 +(unsigned long) HIGH_BITS) >= 0){
            _6105 = NewDouble((double)_6105);
        }
    }
    else {
        _6105 = NewDouble(DBL_PTR(_6103)->dbl - (double)_6104);
    }
    DeRef(_6103);
    _6103 = NOVALUE;
    _6104 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _6105;
    *((int *)(_2+16)) = _4leading_whitespace_10610;
    _6106 = MAKE_SEQ(_1);
    _6105 = NOVALUE;
    DeRef(_s_10613);
    DeRef(_e_10614);
    DeRef(_6013);
    _6013 = NOVALUE;
    DeRef(_6025);
    _6025 = NOVALUE;
    DeRef(_6031);
    _6031 = NOVALUE;
    DeRef(_6038);
    _6038 = NOVALUE;
    DeRef(_6050);
    _6050 = NOVALUE;
    DeRef(_6056);
    _6056 = NOVALUE;
    DeRef(_6062);
    _6062 = NOVALUE;
    DeRef(_6071);
    _6071 = NOVALUE;
    DeRef(_6077);
    _6077 = NOVALUE;
    DeRef(_6085);
    _6085 = NOVALUE;
    DeRef(_6093);
    _6093 = NOVALUE;
    DeRef(_6101);
    _6101 = NOVALUE;
    return _6106;

    /** 	end while*/
    goto L4; // [881] 96
    ;
}


int _4get_value(int _target_10752, int _start_point_10753, int _answer_type_10754)
{
    int _msg_inlined_crash_at_39_10765 = NOVALUE;
    int _data_inlined_crash_at_36_10764 = NOVALUE;
    int _where_inlined_where_at_80_10771 = NOVALUE;
    int _seek_1__tmp_at94_10776 = NOVALUE;
    int _seek_inlined_seek_at_94_10775 = NOVALUE;
    int _pos_inlined_seek_at_91_10774 = NOVALUE;
    int _msg_inlined_crash_at_112_10779 = NOVALUE;
    int _6122 = NOVALUE;
    int _6119 = NOVALUE;
    int _6118 = NOVALUE;
    int _6117 = NOVALUE;
    int _6113 = NOVALUE;
    int _6112 = NOVALUE;
    int _6111 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if answer_type != GET_SHORT_ANSWER and answer_type != GET_LONG_ANSWER then*/
    _6111 = (_answer_type_10754 != _4GET_SHORT_ANSWER_10744);
    if (_6111 == 0) {
        goto L1; // [17] 59
    }
    _6113 = (_answer_type_10754 != _4GET_LONG_ANSWER_10747);
    if (_6113 == 0)
    {
        DeRef(_6113);
        _6113 = NOVALUE;
        goto L1; // [28] 59
    }
    else{
        DeRef(_6113);
        _6113 = NOVALUE;
    }

    /** 		error:crash("Invalid type of answer, please only use %s (the default) or %s.", {"GET_SHORT_ANSWER", "GET_LONG_ANSWER"})*/
    RefDS(_6116);
    RefDS(_6115);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _6115;
    ((int *)_2)[2] = _6116;
    _6117 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_36_10764);
    _data_inlined_crash_at_36_10764 = _6117;
    _6117 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_39_10765);
    _msg_inlined_crash_at_39_10765 = EPrintf(-9999999, _6114, _data_inlined_crash_at_36_10764);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_39_10765);

    /** end procedure*/
    goto L2; // [53] 56
L2: 
    DeRef(_data_inlined_crash_at_36_10764);
    _data_inlined_crash_at_36_10764 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_39_10765);
    _msg_inlined_crash_at_39_10765 = NOVALUE;
L1: 

    /** 	if atom(target) then -- get()*/
    _6118 = IS_ATOM(_target_10752);
    if (_6118 == 0)
    {
        _6118 = NOVALUE;
        goto L3; // [64] 146
    }
    else{
        _6118 = NOVALUE;
    }

    /** 		input_file = target*/
    Ref(_target_10752);
    _4input_file_10305 = _target_10752;
    if (!IS_ATOM_INT(_4input_file_10305)) {
        _1 = (long)(DBL_PTR(_4input_file_10305)->dbl);
        if (UNIQUE(DBL_PTR(_4input_file_10305)) && (DBL_PTR(_4input_file_10305)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4input_file_10305);
        _4input_file_10305 = _1;
    }

    /** 		if start_point then*/
    if (_start_point_10753 == 0)
    {
        goto L4; // [76] 133
    }
    else{
    }

    /** 			if io:seek(target, io:where(target)+start_point) then*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_80_10771);
    _where_inlined_where_at_80_10771 = machine(20, _target_10752);
    if (IS_ATOM_INT(_where_inlined_where_at_80_10771)) {
        _6119 = _where_inlined_where_at_80_10771 + _start_point_10753;
        if ((long)((unsigned long)_6119 + (unsigned long)HIGH_BITS) >= 0) 
        _6119 = NewDouble((double)_6119);
    }
    else {
        _6119 = NewDouble(DBL_PTR(_where_inlined_where_at_80_10771)->dbl + (double)_start_point_10753);
    }
    DeRef(_pos_inlined_seek_at_91_10774);
    _pos_inlined_seek_at_91_10774 = _6119;
    _6119 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_91_10774);
    Ref(_target_10752);
    DeRef(_seek_1__tmp_at94_10776);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _target_10752;
    ((int *)_2)[2] = _pos_inlined_seek_at_91_10774;
    _seek_1__tmp_at94_10776 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_94_10775 = machine(19, _seek_1__tmp_at94_10776);
    DeRef(_pos_inlined_seek_at_91_10774);
    _pos_inlined_seek_at_91_10774 = NOVALUE;
    DeRef(_seek_1__tmp_at94_10776);
    _seek_1__tmp_at94_10776 = NOVALUE;
    if (_seek_inlined_seek_at_94_10775 == 0)
    {
        goto L5; // [108] 132
    }
    else{
    }

    /** 				error:crash("Initial seek() for get() failed!")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_112_10779);
    _msg_inlined_crash_at_112_10779 = EPrintf(-9999999, _6120, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_112_10779);

    /** end procedure*/
    goto L6; // [126] 129
L6: 
    DeRefi(_msg_inlined_crash_at_112_10779);
    _msg_inlined_crash_at_112_10779 = NOVALUE;
L5: 
L4: 

    /** 		string_next = 1*/
    _4string_next_10307 = 1;

    /** 		input_string = 0*/
    DeRef(_4input_string_10306);
    _4input_string_10306 = 0;
    goto L7; // [143] 157
L3: 

    /** 		input_string = target*/
    Ref(_target_10752);
    DeRef(_4input_string_10306);
    _4input_string_10306 = _target_10752;

    /** 		string_next = start_point*/
    _4string_next_10307 = _start_point_10753;
L7: 

    /** 	if answer_type = GET_SHORT_ANSWER then*/
    if (_answer_type_10754 != _4GET_SHORT_ANSWER_10744)
    goto L8; // [161] 170

    /** 		get_ch()*/
    _4get_ch();
L8: 

    /** 	return call_func(answer_type, {})*/
    _0 = (int)_00[_answer_type_10754].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_6122);
    _6122 = _1;
    DeRef(_target_10752);
    DeRef(_6111);
    _6111 = NOVALUE;
    return _6122;
    ;
}


int _4get(int _file_10786, int _offset_10787, int _answer_10788)
{
    int _6123 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_file_10786)) {
        _1 = (long)(DBL_PTR(_file_10786)->dbl);
        if (UNIQUE(DBL_PTR(_file_10786)) && (DBL_PTR(_file_10786)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_10786);
        _file_10786 = _1;
    }
    if (!IS_ATOM_INT(_offset_10787)) {
        _1 = (long)(DBL_PTR(_offset_10787)->dbl);
        if (UNIQUE(DBL_PTR(_offset_10787)) && (DBL_PTR(_offset_10787)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_offset_10787);
        _offset_10787 = _1;
    }
    if (!IS_ATOM_INT(_answer_10788)) {
        _1 = (long)(DBL_PTR(_answer_10788)->dbl);
        if (UNIQUE(DBL_PTR(_answer_10788)) && (DBL_PTR(_answer_10788)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_answer_10788);
        _answer_10788 = _1;
    }

    /** 	return get_value(file, offset, answer)*/
    _6123 = _4get_value(_file_10786, _offset_10787, _answer_10788);
    return _6123;
    ;
}


int _4value(int _st_10792, int _start_point_10793, int _answer_10794)
{
    int _6124 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_point_10793)) {
        _1 = (long)(DBL_PTR(_start_point_10793)->dbl);
        if (UNIQUE(DBL_PTR(_start_point_10793)) && (DBL_PTR(_start_point_10793)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_point_10793);
        _start_point_10793 = _1;
    }
    if (!IS_ATOM_INT(_answer_10794)) {
        _1 = (long)(DBL_PTR(_answer_10794)->dbl);
        if (UNIQUE(DBL_PTR(_answer_10794)) && (DBL_PTR(_answer_10794)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_answer_10794);
        _answer_10794 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    RefDS(_st_10792);
    _6124 = _4get_value(_st_10792, _start_point_10793, _answer_10794);
    DeRefDS(_st_10792);
    return _6124;
    ;
}


int _4defaulted_value(int _st_10798, int _def_10799, int _start_point_10800)
{
    int _result_10803 = NOVALUE;
    int _6129 = NOVALUE;
    int _6127 = NOVALUE;
    int _6125 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_point_10800)) {
        _1 = (long)(DBL_PTR(_start_point_10800)->dbl);
        if (UNIQUE(DBL_PTR(_start_point_10800)) && (DBL_PTR(_start_point_10800)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_point_10800);
        _start_point_10800 = _1;
    }

    /** 	if atom(st) then*/
    _6125 = IS_ATOM(_st_10798);
    if (_6125 == 0)
    {
        _6125 = NOVALUE;
        goto L1; // [10] 20
    }
    else{
        _6125 = NOVALUE;
    }

    /** 		return def*/
    DeRef(_st_10798);
    DeRef(_result_10803);
    return _def_10799;
L1: 

    /** 	object result = get_value(st,start_point, GET_SHORT_ANSWER)*/
    Ref(_st_10798);
    _0 = _result_10803;
    _result_10803 = _4get_value(_st_10798, _start_point_10800, _4GET_SHORT_ANSWER_10744);
    DeRef(_0);

    /** 	if result[1] = GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_result_10803);
    _6127 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _6127, 0)){
        _6127 = NOVALUE;
        goto L2; // [36] 51
    }
    _6127 = NOVALUE;

    /** 		return result[2]*/
    _2 = (int)SEQ_PTR(_result_10803);
    _6129 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_6129);
    DeRef(_st_10798);
    DeRef(_def_10799);
    DeRef(_result_10803);
    return _6129;
L2: 

    /** 	return def*/
    DeRef(_st_10798);
    DeRef(_result_10803);
    _6129 = NOVALUE;
    return _def_10799;
    ;
}



// 0x3BDEC714
