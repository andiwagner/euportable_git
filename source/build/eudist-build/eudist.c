// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _1stringifier(int _s_16148)
{
    int _9290 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(s) then*/
    _9290 = IS_ATOM(_s_16148);
    if (_9290 == 0)
    {
        _9290 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _9290 = NOVALUE;
    }

    /** 		return ""*/
    RefDS(_5);
    DeRef(_s_16148);
    return _5;
L1: 

    /** 	return s*/
    return _s_16148;
    ;
}


int _1slashifier(int _s_16153, int _null_16154)
{
    int _9295 = NOVALUE;
    int _9294 = NOVALUE;
    int _9293 = NOVALUE;
    int _9292 = NOVALUE;
    int _9291 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( s) and s[$] != SLASH then*/
    if (IS_SEQUENCE(_s_16153)){
            _9291 = SEQ_PTR(_s_16153)->length;
    }
    else {
        _9291 = 1;
    }
    if (_9291 == 0) {
        goto L1; // [8] 38
    }
    if (IS_SEQUENCE(_s_16153)){
            _9293 = SEQ_PTR(_s_16153)->length;
    }
    else {
        _9293 = 1;
    }
    _2 = (int)SEQ_PTR(_s_16153);
    _9294 = (int)*(((s1_ptr)_2)->base + _9293);
    if (IS_ATOM_INT(_9294)) {
        _9295 = (_9294 != 92);
    }
    else {
        _9295 = binary_op(NOTEQ, _9294, 92);
    }
    _9294 = NOVALUE;
    if (_9295 == 0) {
        DeRef(_9295);
        _9295 = NOVALUE;
        goto L1; // [26] 38
    }
    else {
        if (!IS_ATOM_INT(_9295) && DBL_PTR(_9295)->dbl == 0.0){
            DeRef(_9295);
            _9295 = NOVALUE;
            goto L1; // [26] 38
        }
        DeRef(_9295);
        _9295 = NOVALUE;
    }
    DeRef(_9295);
    _9295 = NOVALUE;

    /** 		s &= SLASH*/
    Append(&_s_16153, _s_16153, 92);
L1: 

    /** 	return s*/
    return _s_16153;
    ;
}


int _1findFile(int _fName_16169, int _showWarning_16170)
{
    int _9314 = NOVALUE;
    int _9312 = NOVALUE;
    int _9311 = NOVALUE;
    int _9310 = NOVALUE;
    int _9309 = NOVALUE;
    int _9308 = NOVALUE;
    int _9307 = NOVALUE;
    int _9306 = NOVALUE;
    int _9305 = NOVALUE;
    int _9303 = NOVALUE;
    int _9302 = NOVALUE;
    int _9301 = NOVALUE;
    int _9299 = NOVALUE;
    int _9298 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_showWarning_16170)) {
        _1 = (long)(DBL_PTR(_showWarning_16170)->dbl);
        if (UNIQUE(DBL_PTR(_showWarning_16170)) && (DBL_PTR(_showWarning_16170)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_showWarning_16170);
        _showWarning_16170 = _1;
    }

    /**     if find(fName[length(fName)], {10, 13}) then*/
    if (IS_SEQUENCE(_fName_16169)){
            _9298 = SEQ_PTR(_fName_16169)->length;
    }
    else {
        _9298 = 1;
    }
    _2 = (int)SEQ_PTR(_fName_16169);
    _9299 = (int)*(((s1_ptr)_2)->base + _9298);
    _9301 = find_from(_9299, _9300, 1);
    _9299 = NOVALUE;
    if (_9301 == 0)
    {
        _9301 = NOVALUE;
        goto L1; // [21] 39
    }
    else{
        _9301 = NOVALUE;
    }

    /** 		fName = fName[1..length(fName)-1]*/
    if (IS_SEQUENCE(_fName_16169)){
            _9302 = SEQ_PTR(_fName_16169)->length;
    }
    else {
        _9302 = 1;
    }
    _9303 = _9302 - 1;
    _9302 = NOVALUE;
    rhs_slice_target = (object_ptr)&_fName_16169;
    RHS_Slice(_fName_16169, 1, _9303);
L1: 

    /**     for i = 1 to length( Place ) do*/
    if (IS_SEQUENCE(_1Place_16166)){
            _9305 = SEQ_PTR(_1Place_16166)->length;
    }
    else {
        _9305 = 1;
    }
    {
        int _i_16180;
        _i_16180 = 1;
L2: 
        if (_i_16180 > _9305){
            goto L3; // [46] 105
        }

        /** 		if sequence( dir( Place[i] & fName ) ) then*/
        _2 = (int)SEQ_PTR(_1Place_16166);
        _9306 = (int)*(((s1_ptr)_2)->base + _i_16180);
        if (IS_SEQUENCE(_9306) && IS_ATOM(_fName_16169)) {
        }
        else if (IS_ATOM(_9306) && IS_SEQUENCE(_fName_16169)) {
            Ref(_9306);
            Prepend(&_9307, _fName_16169, _9306);
        }
        else {
            Concat((object_ptr)&_9307, _9306, _fName_16169);
            _9306 = NOVALUE;
        }
        _9306 = NOVALUE;
        _9308 = _15dir(_9307);
        _9307 = NOVALUE;
        _9309 = IS_SEQUENCE(_9308);
        DeRef(_9308);
        _9308 = NOVALUE;
        if (_9309 == 0)
        {
            _9309 = NOVALUE;
            goto L4; // [72] 98
        }
        else{
            _9309 = NOVALUE;
        }

        /** 			ifdef WINDOWS then*/

        /** 				return upper( Place[i] & fName )*/
        _2 = (int)SEQ_PTR(_1Place_16166);
        _9310 = (int)*(((s1_ptr)_2)->base + _i_16180);
        if (IS_SEQUENCE(_9310) && IS_ATOM(_fName_16169)) {
        }
        else if (IS_ATOM(_9310) && IS_SEQUENCE(_fName_16169)) {
            Ref(_9310);
            Prepend(&_9311, _fName_16169, _9310);
        }
        else {
            Concat((object_ptr)&_9311, _9310, _fName_16169);
            _9310 = NOVALUE;
        }
        _9310 = NOVALUE;
        _9312 = _12upper(_9311);
        _9311 = NOVALUE;
        DeRefDS(_fName_16169);
        DeRef(_9303);
        _9303 = NOVALUE;
        return _9312;
L4: 

        /**     end for*/
        _i_16180 = _i_16180 + 1;
        goto L2; // [100] 53
L3: 
        ;
    }

    /**     if showWarning then*/
    if (_showWarning_16170 == 0)
    {
        goto L5; // [107] 121
    }
    else{
    }

    /** 		printf( 1, "Warning: Unable to locate file %s.\n", {fName} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_fName_16169);
    *((int *)(_2+4)) = _fName_16169;
    _9314 = MAKE_SEQ(_1);
    EPrintf(1, _9313, _9314);
    DeRefDS(_9314);
    _9314 = NOVALUE;
L5: 

    /**     return fName*/
    DeRef(_9303);
    _9303 = NOVALUE;
    DeRef(_9312);
    _9312 = NOVALUE;
    return _fName_16169;
    ;
}


int _1getIncludeName(int _data_16197)
{
    int _at_16198 = NOVALUE;
    int _includeType_16217 = NOVALUE;
    int _nameSpace_16236 = NOVALUE;
    int _9349 = NOVALUE;
    int _9347 = NOVALUE;
    int _9345 = NOVALUE;
    int _9343 = NOVALUE;
    int _9341 = NOVALUE;
    int _9339 = NOVALUE;
    int _9337 = NOVALUE;
    int _9336 = NOVALUE;
    int _9335 = NOVALUE;
    int _9334 = NOVALUE;
    int _9332 = NOVALUE;
    int _9330 = NOVALUE;
    int _9329 = NOVALUE;
    int _9327 = NOVALUE;
    int _9326 = NOVALUE;
    int _9324 = NOVALUE;
    int _9323 = NOVALUE;
    int _9320 = NOVALUE;
    int _9319 = NOVALUE;
    int _9318 = NOVALUE;
    int _9316 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not match( "include ", data ) then*/
    _9316 = e_match_from(_9315, _data_16197, 1);
    if (_9316 != 0)
    goto L1; // [10] 26
    _9316 = NOVALUE;

    /** 		return {"","",""}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_5, 3);
    *((int *)(_2+4)) = _5;
    *((int *)(_2+8)) = _5;
    *((int *)(_2+12)) = _5;
    _9318 = MAKE_SEQ(_1);
    DeRefDS(_data_16197);
    DeRef(_includeType_16217);
    DeRef(_nameSpace_16236);
    return _9318;
L1: 

    /** 	while charClass[ data[1] ] = WHITE_SPACE do*/
L2: 
    _2 = (int)SEQ_PTR(_data_16197);
    _9319 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_1charClass_16132);
    if (!IS_ATOM_INT(_9319)){
        _9320 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_9319)->dbl));
    }
    else{
        _9320 = (int)*(((s1_ptr)_2)->base + _9319);
    }
    if (_9320 != 7)
    goto L3; // [41] 57

    /** 		data = remove( data, 1 )*/
    {
        s1_ptr assign_space = SEQ_PTR(_data_16197);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(1)) ? 1 : (long)(DBL_PTR(1)->dbl);
        int stop = (IS_ATOM_INT(1)) ? 1 : (long)(DBL_PTR(1)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_data_16197), start, &_data_16197 );
            }
            else Tail(SEQ_PTR(_data_16197), stop+1, &_data_16197);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_data_16197), start, &_data_16197);
        }
        else {
            assign_slice_seq = &assign_space;
            _data_16197 = Remove_elements(start, stop, (SEQ_PTR(_data_16197)->ref == 1));
        }
    }

    /** 	end while      */
    goto L2; // [54] 31
L3: 

    /** 	if find( '\n', data ) then*/
    _9323 = find_from(10, _data_16197, 1);
    if (_9323 == 0)
    {
        _9323 = NOVALUE;
        goto L4; // [64] 78
    }
    else{
        _9323 = NOVALUE;
    }

    /** 		data = remove( data, length( data ) )*/
    if (IS_SEQUENCE(_data_16197)){
            _9324 = SEQ_PTR(_data_16197)->length;
    }
    else {
        _9324 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_data_16197);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_9324)) ? _9324 : (long)(DBL_PTR(_9324)->dbl);
        int stop = (IS_ATOM_INT(_9324)) ? _9324 : (long)(DBL_PTR(_9324)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_data_16197), start, &_data_16197 );
            }
            else Tail(SEQ_PTR(_data_16197), stop+1, &_data_16197);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_data_16197), start, &_data_16197);
        }
        else {
            assign_slice_seq = &assign_space;
            _data_16197 = Remove_elements(start, stop, (SEQ_PTR(_data_16197)->ref == 1));
        }
    }
    _9324 = NOVALUE;
    _9324 = NOVALUE;
L4: 

    /** 	if find( '\r', data ) then*/
    _9326 = find_from(13, _data_16197, 1);
    if (_9326 == 0)
    {
        _9326 = NOVALUE;
        goto L5; // [85] 99
    }
    else{
        _9326 = NOVALUE;
    }

    /** 		data = remove( data, length( data ) )*/
    if (IS_SEQUENCE(_data_16197)){
            _9327 = SEQ_PTR(_data_16197)->length;
    }
    else {
        _9327 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_data_16197);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_9327)) ? _9327 : (long)(DBL_PTR(_9327)->dbl);
        int stop = (IS_ATOM_INT(_9327)) ? _9327 : (long)(DBL_PTR(_9327)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_data_16197), start, &_data_16197 );
            }
            else Tail(SEQ_PTR(_data_16197), stop+1, &_data_16197);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_data_16197), start, &_data_16197);
        }
        else {
            assign_slice_seq = &assign_space;
            _data_16197 = Remove_elements(start, stop, (SEQ_PTR(_data_16197)->ref == 1));
        }
    }
    _9327 = NOVALUE;
    _9327 = NOVALUE;
L5: 

    /** 	sequence includeType*/

    /** 	if equal( data[1..8], "include " ) then*/
    rhs_slice_target = (object_ptr)&_9329;
    RHS_Slice(_data_16197, 1, 8);
    if (_9329 == _9315)
    _9330 = 1;
    else if (IS_ATOM_INT(_9329) && IS_ATOM_INT(_9315))
    _9330 = 0;
    else
    _9330 = (compare(_9329, _9315) == 0);
    DeRefDS(_9329);
    _9329 = NOVALUE;
    if (_9330 == 0)
    {
        _9330 = NOVALUE;
        goto L6; // [112] 135
    }
    else{
        _9330 = NOVALUE;
    }

    /** 		includeType = data[1..8]*/
    rhs_slice_target = (object_ptr)&_includeType_16217;
    RHS_Slice(_data_16197, 1, 8);

    /** 		data = data[9..length(data)]*/
    if (IS_SEQUENCE(_data_16197)){
            _9332 = SEQ_PTR(_data_16197)->length;
    }
    else {
        _9332 = 1;
    }
    rhs_slice_target = (object_ptr)&_data_16197;
    RHS_Slice(_data_16197, 9, _9332);
    goto L7; // [132] 194
L6: 

    /** 	elsif length(data) > 15 and equal( data[1..15], "public include " ) then*/
    if (IS_SEQUENCE(_data_16197)){
            _9334 = SEQ_PTR(_data_16197)->length;
    }
    else {
        _9334 = 1;
    }
    _9335 = (_9334 > 15);
    _9334 = NOVALUE;
    if (_9335 == 0) {
        goto L8; // [144] 181
    }
    rhs_slice_target = (object_ptr)&_9337;
    RHS_Slice(_data_16197, 1, 15);
    if (_9337 == _9338)
    _9339 = 1;
    else if (IS_ATOM_INT(_9337) && IS_ATOM_INT(_9338))
    _9339 = 0;
    else
    _9339 = (compare(_9337, _9338) == 0);
    DeRefDS(_9337);
    _9337 = NOVALUE;
    if (_9339 == 0)
    {
        _9339 = NOVALUE;
        goto L8; // [158] 181
    }
    else{
        _9339 = NOVALUE;
    }

    /** 		includeType = data[1..15]*/
    rhs_slice_target = (object_ptr)&_includeType_16217;
    RHS_Slice(_data_16197, 1, 15);

    /** 		data = data[16..length(data)]*/
    if (IS_SEQUENCE(_data_16197)){
            _9341 = SEQ_PTR(_data_16197)->length;
    }
    else {
        _9341 = 1;
    }
    rhs_slice_target = (object_ptr)&_data_16197;
    RHS_Slice(_data_16197, 16, _9341);
    goto L7; // [178] 194
L8: 

    /** 		return {"","",""}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_5, 3);
    *((int *)(_2+4)) = _5;
    *((int *)(_2+8)) = _5;
    *((int *)(_2+12)) = _5;
    _9343 = MAKE_SEQ(_1);
    DeRefDS(_data_16197);
    DeRef(_includeType_16217);
    DeRef(_nameSpace_16236);
    DeRef(_9318);
    _9318 = NOVALUE;
    _9319 = NOVALUE;
    _9320 = NOVALUE;
    DeRef(_9335);
    _9335 = NOVALUE;
    return _9343;
L7: 

    /** 	sequence nameSpace = ""*/
    RefDS(_5);
    DeRef(_nameSpace_16236);
    _nameSpace_16236 = _5;

    /** 	at = find( ' ', data )*/
    _at_16198 = find_from(32, _data_16197, 1);

    /** 	if at then*/
    if (_at_16198 == 0)
    {
        goto L9; // [212] 237
    }
    else{
    }

    /** 		nameSpace = data[at..$]*/
    if (IS_SEQUENCE(_data_16197)){
            _9345 = SEQ_PTR(_data_16197)->length;
    }
    else {
        _9345 = 1;
    }
    rhs_slice_target = (object_ptr)&_nameSpace_16236;
    RHS_Slice(_data_16197, _at_16198, _9345);

    /** 		data = data[1..at-1]*/
    _9347 = _at_16198 - 1;
    rhs_slice_target = (object_ptr)&_data_16197;
    RHS_Slice(_data_16197, 1, _9347);
L9: 

    /** 	return {data,nameSpace,includeType}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_data_16197);
    *((int *)(_2+4)) = _data_16197;
    RefDS(_nameSpace_16236);
    *((int *)(_2+8)) = _nameSpace_16236;
    RefDS(_includeType_16217);
    *((int *)(_2+12)) = _includeType_16217;
    _9349 = MAKE_SEQ(_1);
    DeRefDS(_data_16197);
    DeRefDS(_includeType_16217);
    DeRefDS(_nameSpace_16236);
    DeRef(_9318);
    _9318 = NOVALUE;
    _9319 = NOVALUE;
    _9320 = NOVALUE;
    DeRef(_9335);
    _9335 = NOVALUE;
    DeRef(_9343);
    _9343 = NOVALUE;
    DeRef(_9347);
    _9347 = NOVALUE;
    return _9349;
    ;
}


int _1trimer(int _s_16246)
{
    int _t_16247 = NOVALUE;
    int _u_16248 = NOVALUE;
    int _9353 = NOVALUE;
    int _9351 = NOVALUE;
    int _9350 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if s[$] = '\n' then*/
    if (IS_SEQUENCE(_s_16246)){
            _9350 = SEQ_PTR(_s_16246)->length;
    }
    else {
        _9350 = 1;
    }
    _2 = (int)SEQ_PTR(_s_16246);
    _9351 = (int)*(((s1_ptr)_2)->base + _9350);
    if (binary_op_a(NOTEQ, _9351, 10)){
        _9351 = NOVALUE;
        goto L1; // [12] 27
    }
    _9351 = NOVALUE;

    /** 		s = remove( s, length(s) )*/
    if (IS_SEQUENCE(_s_16246)){
            _9353 = SEQ_PTR(_s_16246)->length;
    }
    else {
        _9353 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_s_16246);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_9353)) ? _9353 : (long)(DBL_PTR(_9353)->dbl);
        int stop = (IS_ATOM_INT(_9353)) ? _9353 : (long)(DBL_PTR(_9353)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_s_16246), start, &_s_16246 );
            }
            else Tail(SEQ_PTR(_s_16246), stop+1, &_s_16246);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_s_16246), start, &_s_16246);
        }
        else {
            assign_slice_seq = &assign_space;
            _s_16246 = Remove_elements(start, stop, (SEQ_PTR(_s_16246)->ref == 1));
        }
    }
    _9353 = NOVALUE;
    _9353 = NOVALUE;
L1: 

    /**     return s*/
    return _s_16246;
    ;
}


int _1includable(int _name_16257)
{
    int _9358 = NOVALUE;
    int _9357 = NOVALUE;
    int _9356 = NOVALUE;
    int _0, _1, _2;
    

    /**     name = findFile(name,0)*/
    RefDS(_name_16257);
    _0 = _name_16257;
    _name_16257 = _1findFile(_name_16257, 0);
    DeRefDS(_0);

    /**     return not find(canonical_path(name), excludedIncludes)*/
    RefDS(_name_16257);
    _9356 = _15canonical_path(_name_16257, 0, 0);
    _9357 = find_from(_9356, _1excludedIncludes_16143, 1);
    DeRef(_9356);
    _9356 = NOVALUE;
    _9358 = (_9357 == 0);
    _9357 = NOVALUE;
    DeRefDS(_name_16257);
    return _9358;
    ;
}


int _1convertAbsoluteToRelative(int _name_16265)
{
    int _9365 = NOVALUE;
    int _9364 = NOVALUE;
    int _9363 = NOVALUE;
    int _9362 = NOVALUE;
    int _9361 = NOVALUE;
    int _9360 = NOVALUE;
    int _9359 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path( name ) then*/
    RefDS(_name_16265);
    _9359 = _15absolute_path(_name_16265);
    if (_9359 == 0) {
        DeRef(_9359);
        _9359 = NOVALUE;
        goto L1; // [9] 65
    }
    else {
        if (!IS_ATOM_INT(_9359) && DBL_PTR(_9359)->dbl == 0.0){
            DeRef(_9359);
            _9359 = NOVALUE;
            goto L1; // [9] 65
        }
        DeRef(_9359);
        _9359 = NOVALUE;
    }
    DeRef(_9359);
    _9359 = NOVALUE;

    /** 		for i = 1 to length( name ) do*/
    if (IS_SEQUENCE(_name_16265)){
            _9360 = SEQ_PTR(_name_16265)->length;
    }
    else {
        _9360 = 1;
    }
    {
        int _i_16270;
        _i_16270 = 1;
L2: 
        if (_i_16270 > _9360){
            goto L3; // [17] 64
        }

        /** 			if find( name[i], `/\` ) then*/
        _2 = (int)SEQ_PTR(_name_16265);
        _9361 = (int)*(((s1_ptr)_2)->base + _i_16270);
        _9362 = find_from(_9361, _3523, 1);
        _9361 = NOVALUE;
        if (_9362 == 0)
        {
            _9362 = NOVALUE;
            goto L4; // [35] 57
        }
        else{
            _9362 = NOVALUE;
        }

        /** 				return name[i+1..$]*/
        _9363 = _i_16270 + 1;
        if (IS_SEQUENCE(_name_16265)){
                _9364 = SEQ_PTR(_name_16265)->length;
        }
        else {
            _9364 = 1;
        }
        rhs_slice_target = (object_ptr)&_9365;
        RHS_Slice(_name_16265, _9363, _9364);
        DeRefDS(_name_16265);
        _9363 = NOVALUE;
        return _9365;
L4: 

        /** 		end for*/
        _i_16270 = _i_16270 + 1;
        goto L2; // [59] 24
L3: 
        ;
    }
L1: 

    /** 	return name*/
    DeRef(_9363);
    _9363 = NOVALUE;
    DeRef(_9365);
    _9365 = NOVALUE;
    return _name_16265;
    ;
}


int _1parseFile(int _fName_16280, int _fromPath_16281)
{
    int _inFile_16282 = NOVALUE;
    int _outFile_16283 = NOVALUE;
    int _newIncludeName_16284 = NOVALUE;
    int _newfName_16285 = NOVALUE;
    int _nameSpace_16286 = NOVALUE;
    int _includeType_16287 = NOVALUE;
    int _includeName_16288 = NOVALUE;
    int _data_16289 = NOVALUE;
    int _includeFile_16291 = NOVALUE;
    int _includePath_16293 = NOVALUE;
    int _tryName_16304 = NOVALUE;
    int _newPath_16321 = NOVALUE;
    int _ix_16369 = NOVALUE;
    int _9615 = NOVALUE;
    int _9425 = NOVALUE;
    int _9423 = NOVALUE;
    int _9421 = NOVALUE;
    int _9417 = NOVALUE;
    int _9416 = NOVALUE;
    int _9415 = NOVALUE;
    int _9413 = NOVALUE;
    int _9411 = NOVALUE;
    int _9410 = NOVALUE;
    int _9408 = NOVALUE;
    int _9407 = NOVALUE;
    int _9406 = NOVALUE;
    int _9405 = NOVALUE;
    int _9404 = NOVALUE;
    int _9399 = NOVALUE;
    int _9397 = NOVALUE;
    int _9396 = NOVALUE;
    int _9392 = NOVALUE;
    int _9389 = NOVALUE;
    int _9387 = NOVALUE;
    int _9386 = NOVALUE;
    int _9383 = NOVALUE;
    int _9378 = NOVALUE;
    int _9377 = NOVALUE;
    int _9375 = NOVALUE;
    int _9374 = NOVALUE;
    int _9373 = NOVALUE;
    int _9371 = NOVALUE;
    int _9369 = NOVALUE;
    int _9368 = NOVALUE;
    int _0, _1, _2;
    

    /** 	included = append( included, fName )*/
    RefDS(_fName_16280);
    Append(&_1included_16142, _1included_16142, _fName_16280);

    /** 	inFile = -1*/
    _inFile_16282 = -1;

    /** 	sequence includeFile = convertAbsoluteToRelative( fName )*/
    RefDS(_fName_16280);
    _0 = _includeFile_16291;
    _includeFile_16291 = _1convertAbsoluteToRelative(_fName_16280);
    DeRef(_0);

    /** 	sequence includePath = ""*/
    RefDS(_5);
    DeRef(_includePath_16293);
    _includePath_16293 = _5;

    /** 	for i = length( includeFile ) to 1 by -1 do*/
    if (IS_SEQUENCE(_includeFile_16291)){
            _9368 = SEQ_PTR(_includeFile_16291)->length;
    }
    else {
        _9368 = 1;
    }
    {
        int _i_16295;
        _i_16295 = _9368;
L1: 
        if (_i_16295 < 1){
            goto L2; // [40] 80
        }

        /** 		if find( includeFile[i], `\/` ) then*/
        _2 = (int)SEQ_PTR(_includeFile_16291);
        _9369 = (int)*(((s1_ptr)_2)->base + _i_16295);
        _9371 = find_from(_9369, _9370, 1);
        _9369 = NOVALUE;
        if (_9371 == 0)
        {
            _9371 = NOVALUE;
            goto L3; // [58] 73
        }
        else{
            _9371 = NOVALUE;
        }

        /** 			includePath = includeFile[1..i]*/
        rhs_slice_target = (object_ptr)&_includePath_16293;
        RHS_Slice(_includeFile_16291, 1, _i_16295);

        /** 			exit*/
        goto L2; // [70] 80
L3: 

        /** 	end for*/
        _i_16295 = _i_16295 + -1;
        goto L1; // [75] 47
L2: 
        ;
    }

    /** 	if length( fromPath ) then*/
    if (IS_SEQUENCE(_fromPath_16281)){
            _9373 = SEQ_PTR(_fromPath_16281)->length;
    }
    else {
        _9373 = 1;
    }
    if (_9373 == 0)
    {
        _9373 = NOVALUE;
        goto L4; // [85] 144
    }
    else{
        _9373 = NOVALUE;
    }

    /** 		sequence tryName = findFile( slashifier( fromPath ) & fName )*/
    RefDS(_fromPath_16281);
    _9374 = _1slashifier(_fromPath_16281, 0);
    if (IS_SEQUENCE(_9374) && IS_ATOM(_fName_16280)) {
    }
    else if (IS_ATOM(_9374) && IS_SEQUENCE(_fName_16280)) {
        Ref(_9374);
        Prepend(&_9375, _fName_16280, _9374);
    }
    else {
        Concat((object_ptr)&_9375, _9374, _fName_16280);
        DeRef(_9374);
        _9374 = NOVALUE;
    }
    DeRef(_9374);
    _9374 = NOVALUE;
    _0 = _tryName_16304;
    _tryName_16304 = _1findFile(_9375, 0);
    DeRef(_0);
    _9375 = NOVALUE;

    /** 		if file_exists( tryName ) then*/
    RefDS(_tryName_16304);
    _9377 = _15file_exists(_tryName_16304);
    if (_9377 == 0) {
        DeRef(_9377);
        _9377 = NOVALUE;
        goto L5; // [114] 143
    }
    else {
        if (!IS_ATOM_INT(_9377) && DBL_PTR(_9377)->dbl == 0.0){
            DeRef(_9377);
            _9377 = NOVALUE;
            goto L5; // [114] 143
        }
        DeRef(_9377);
        _9377 = NOVALUE;
    }
    DeRef(_9377);
    _9377 = NOVALUE;

    /** 			includePath = fromPath*/
    RefDS(_fromPath_16281);
    DeRef(_includePath_16293);
    _includePath_16293 = _fromPath_16281;

    /** 			includeFile = slashifier( fromPath ) & fName*/
    RefDS(_fromPath_16281);
    _9378 = _1slashifier(_fromPath_16281, 0);
    if (IS_SEQUENCE(_9378) && IS_ATOM(_fName_16280)) {
    }
    else if (IS_ATOM(_9378) && IS_SEQUENCE(_fName_16280)) {
        Ref(_9378);
        Prepend(&_includeFile_16291, _fName_16280, _9378);
    }
    else {
        Concat((object_ptr)&_includeFile_16291, _9378, _fName_16280);
        DeRef(_9378);
        _9378 = NOVALUE;
    }
    DeRef(_9378);
    _9378 = NOVALUE;

    /** 			fName = includeFile*/
    RefDS(_includeFile_16291);
    DeRefDS(_fName_16280);
    _fName_16280 = _includeFile_16291;
L5: 
L4: 
    DeRef(_tryName_16304);
    _tryName_16304 = NOVALUE;

    /** 	fName = findFile( fName )*/
    RefDS(_fName_16280);
    _0 = _fName_16280;
    _fName_16280 = _1findFile(_fName_16280, 0);
    DeRefDS(_0);

    /** 	inFile = open( fName, "r" )*/
    _inFile_16282 = EOpen(_fName_16280, _3966, 0);

    /**     if inFile = -1 then*/
    if (_inFile_16282 != -1)
    goto L6; // [168] 203

    /** 		included = remove( included, length( included ) )*/
    if (IS_SEQUENCE(_1included_16142)){
            _9383 = SEQ_PTR(_1included_16142)->length;
    }
    else {
        _9383 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_1included_16142);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_9383)) ? _9383 : (long)(DBL_PTR(_9383)->dbl);
        int stop = (IS_ATOM_INT(_9383)) ? _9383 : (long)(DBL_PTR(_9383)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_1included_16142), start, &_1included_16142 );
            }
            else Tail(SEQ_PTR(_1included_16142), stop+1, &_1included_16142);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_1included_16142), start, &_1included_16142);
        }
        else {
            assign_slice_seq = &assign_space;
            _1included_16142 = Remove_elements(start, stop, (SEQ_PTR(_1included_16142)->ref == 1));
        }
    }
    _9383 = NOVALUE;
    _9383 = NOVALUE;

    /** 		printf(1, "Error finding file: %s\n", { fName } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_fName_16280);
    *((int *)(_2+4)) = _fName_16280;
    _9386 = MAKE_SEQ(_1);
    EPrintf(1, _9385, _9386);
    DeRefDS(_9386);
    _9386 = NOVALUE;

    /** 		return includeFile*/
    DeRefDS(_fName_16280);
    DeRefDS(_fromPath_16281);
    DeRef(_newIncludeName_16284);
    DeRef(_nameSpace_16286);
    DeRef(_includeType_16287);
    DeRef(_includeName_16288);
    DeRef(_data_16289);
    DeRef(_includePath_16293);
    DeRef(_newPath_16321);
    return _includeFile_16291;
L6: 

    /** 	sequence newPath = slashifier( outputDir & includePath )*/
    if (IS_SEQUENCE(_1outputDir_16144) && IS_ATOM(_includePath_16293)) {
    }
    else if (IS_ATOM(_1outputDir_16144) && IS_SEQUENCE(_includePath_16293)) {
        Ref(_1outputDir_16144);
        Prepend(&_9387, _includePath_16293, _1outputDir_16144);
    }
    else {
        Concat((object_ptr)&_9387, _1outputDir_16144, _includePath_16293);
    }
    _0 = _newPath_16321;
    _newPath_16321 = _1slashifier(_9387, 0);
    DeRef(_0);
    _9387 = NOVALUE;

    /** 	if not file_exists( newPath ) then*/
    RefDS(_newPath_16321);
    _9389 = _15file_exists(_newPath_16321);
    if (IS_ATOM_INT(_9389)) {
        if (_9389 != 0){
            DeRef(_9389);
            _9389 = NOVALUE;
            goto L7; // [224] 238
        }
    }
    else {
        if (DBL_PTR(_9389)->dbl != 0.0){
            DeRef(_9389);
            _9389 = NOVALUE;
            goto L7; // [224] 238
        }
    }
    DeRef(_9389);
    _9389 = NOVALUE;

    /** 		create_directory( newPath )*/
    RefDS(_newPath_16321);
    _9615 = _15create_directory(_newPath_16321, 448, 1);
    DeRef(_9615);
    _9615 = NOVALUE;
L7: 

    /** 	if verbose then*/

    /** 	outFile = open( outputDir & SLASH & includeFile, "w" )*/
    {
        int concat_list[3];

        concat_list[0] = _includeFile_16291;
        concat_list[1] = 92;
        concat_list[2] = _1outputDir_16144;
        Concat_N((object_ptr)&_9392, concat_list, 3);
    }
    _outFile_16283 = EOpen(_9392, _5740, 0);
    DeRefDS(_9392);
    _9392 = NOVALUE;

    /** 	if outFile = -1 then*/
    if (_outFile_16283 != -1)
    goto L8; // [283] 308

    /** 		printf(1, "Warning: Unable to open %s for writing\n",*/
    {
        int concat_list[3];

        concat_list[0] = _includeFile_16291;
        concat_list[1] = 92;
        concat_list[2] = _1outputDir_16144;
        Concat_N((object_ptr)&_9396, concat_list, 3);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9396;
    _9397 = MAKE_SEQ(_1);
    _9396 = NOVALUE;
    EPrintf(1, _9395, _9397);
    DeRefDS(_9397);
    _9397 = NOVALUE;
L8: 

    /** 	while 1 do        */
L9: 

    /** 		data = gets( inFile )*/
    DeRef(_data_16289);
    _data_16289 = EGets(_inFile_16282);

    /** 		if integer( data ) then*/
    if (IS_ATOM_INT(_data_16289))
    _9399 = 1;
    else if (IS_ATOM_DBL(_data_16289))
    _9399 = IS_ATOM_INT(DoubleToInt(_data_16289));
    else
    _9399 = 0;
    if (_9399 == 0)
    {
        _9399 = NOVALUE;
        goto LA; // [323] 331
    }
    else{
        _9399 = NOVALUE;
    }

    /** 			exit*/
    goto LB; // [328] 540
LA: 

    /** 		includeName = getIncludeName( data )*/
    Ref(_data_16289);
    _0 = _includeName_16288;
    _includeName_16288 = _1getIncludeName(_data_16289);
    DeRef(_0);

    /** 		includeType = includeName[3]*/
    DeRef(_includeType_16287);
    _2 = (int)SEQ_PTR(_includeName_16288);
    _includeType_16287 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_includeType_16287);

    /** 		nameSpace = includeName[2]*/
    DeRef(_nameSpace_16286);
    _2 = (int)SEQ_PTR(_includeName_16288);
    _nameSpace_16286 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_nameSpace_16286);

    /** 		includeName = includeName[1]*/
    _0 = _includeName_16288;
    _2 = (int)SEQ_PTR(_includeName_16288);
    _includeName_16288 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_includeName_16288);
    DeRefDS(_0);

    /** 		if length( includeName ) and includable(trimer(includeName)) then*/
    if (IS_SEQUENCE(_includeName_16288)){
            _9404 = SEQ_PTR(_includeName_16288)->length;
    }
    else {
        _9404 = 1;
    }
    if (_9404 == 0) {
        goto LC; // [368] 503
    }
    RefDS(_includeName_16288);
    _9406 = _1trimer(_includeName_16288);
    _9407 = _1includable(_9406);
    _9406 = NOVALUE;
    if (_9407 == 0) {
        DeRef(_9407);
        _9407 = NOVALUE;
        goto LC; // [381] 503
    }
    else {
        if (!IS_ATOM_INT(_9407) && DBL_PTR(_9407)->dbl == 0.0){
            DeRef(_9407);
            _9407 = NOVALUE;
            goto LC; // [381] 503
        }
        DeRef(_9407);
        _9407 = NOVALUE;
    }
    DeRef(_9407);
    _9407 = NOVALUE;

    /** 			newIncludeName = includeName*/
    RefDS(_includeName_16288);
    DeRef(_newIncludeName_16284);
    _newIncludeName_16284 = _includeName_16288;

    /** 			if not find( includeName, included ) then*/
    _9408 = find_from(_includeName_16288, _1included_16142, 1);
    if (_9408 != 0)
    goto LD; // [400] 421
    _9408 = NOVALUE;

    /** 				newIncludeName = parseFile( includeName, includePath )*/
    RefDS(_includeName_16288);
    DeRef(_9410);
    _9410 = _includeName_16288;
    RefDS(_includePath_16293);
    DeRef(_9411);
    _9411 = _includePath_16293;
    _0 = _newIncludeName_16284;
    _newIncludeName_16284 = _1parseFile(_9410, _9411);
    DeRefDS(_0);
    _9410 = NOVALUE;
    _9411 = NOVALUE;
    goto LE; // [418] 440
LD: 

    /** 			elsif absolute_path( includeName ) then*/
    RefDS(_includeName_16288);
    _9413 = _15absolute_path(_includeName_16288);
    if (_9413 == 0) {
        DeRef(_9413);
        _9413 = NOVALUE;
        goto LF; // [427] 439
    }
    else {
        if (!IS_ATOM_INT(_9413) && DBL_PTR(_9413)->dbl == 0.0){
            DeRef(_9413);
            _9413 = NOVALUE;
            goto LF; // [427] 439
        }
        DeRef(_9413);
        _9413 = NOVALUE;
    }
    DeRef(_9413);
    _9413 = NOVALUE;

    /** 				newIncludeName = convertAbsoluteToRelative( includeName )*/
    RefDS(_includeName_16288);
    _0 = _newIncludeName_16284;
    _newIncludeName_16284 = _1convertAbsoluteToRelative(_includeName_16288);
    DeRef(_0);
LF: 
LE: 

    /** 			if absolute_path( includeName ) and eu:compare( includeName, newIncludeName ) then*/
    RefDS(_includeName_16288);
    _9415 = _15absolute_path(_includeName_16288);
    if (IS_ATOM_INT(_9415)) {
        if (_9415 == 0) {
            goto L10; // [446] 498
        }
    }
    else {
        if (DBL_PTR(_9415)->dbl == 0.0) {
            goto L10; // [446] 498
        }
    }
    if (IS_ATOM_INT(_includeName_16288) && IS_ATOM_INT(_newIncludeName_16284)){
        _9417 = (_includeName_16288 < _newIncludeName_16284) ? -1 : (_includeName_16288 > _newIncludeName_16284);
    }
    else{
        _9417 = compare(_includeName_16288, _newIncludeName_16284);
    }
    if (_9417 == 0)
    {
        _9417 = NOVALUE;
        goto L10; // [455] 498
    }
    else{
        _9417 = NOVALUE;
    }

    /** 				integer ix = match( newIncludeName, data )*/
    _ix_16369 = e_match_from(_newIncludeName_16284, _data_16289, 1);

    /** 				if verbose then*/

    /** 				data = replace( data, "include ", 1, ix-1 )*/
    _9421 = _ix_16369 - 1;
    {
        int p1 = _data_16289;
        int p2 = _9315;
        int p3 = 1;
        int p4 = _9421;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_data_16289;
        Replace( &replace_params );
    }
    _9421 = NOVALUE;
L10: 
    goto L11; // [500] 523
LC: 

    /** 		elsif length( includeName ) then*/
    if (IS_SEQUENCE(_includeName_16288)){
            _9423 = SEQ_PTR(_includeName_16288)->length;
    }
    else {
        _9423 = 1;
    }
    if (_9423 == 0)
    {
        _9423 = NOVALUE;
        goto L12; // [508] 522
    }
    else{
        _9423 = NOVALUE;
    }

    /** 			printf(1, "Error finding include file %s\n", { includeName } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_includeName_16288);
    *((int *)(_2+4)) = _includeName_16288;
    _9425 = MAKE_SEQ(_1);
    EPrintf(1, _9424, _9425);
    DeRefDS(_9425);
    _9425 = NOVALUE;
L12: 
L11: 

    /** 		if outFile != -1 then*/
    if (_outFile_16283 == -1)
    goto L9; // [525] 313

    /** 			puts( outFile, data )*/
    EPuts(_outFile_16283, _data_16289); // DJP 

    /** 	end while*/
    goto L9; // [537] 313
LB: 

    /** 	close( inFile )*/
    EClose(_inFile_16282);

    /** 	if outFile != -1 then*/
    if (_outFile_16283 == -1)
    goto L13; // [546] 555

    /** 		close( outFile )*/
    EClose(_outFile_16283);
L13: 

    /** 	return includeFile*/
    DeRefDS(_fName_16280);
    DeRefDS(_fromPath_16281);
    DeRef(_newIncludeName_16284);
    DeRef(_nameSpace_16286);
    DeRef(_includeType_16287);
    DeRef(_includeName_16288);
    DeRef(_data_16289);
    DeRef(_includePath_16293);
    DeRef(_newPath_16321);
    DeRef(_9415);
    _9415 = NOVALUE;
    return _includeFile_16291;
    ;
}


int _1getListOfFiles(int _inDir_16386, int _recursive_16387)
{
    int _s_16388 = NOVALUE;
    int _z_16391 = NOVALUE;
    int _9451 = NOVALUE;
    int _9450 = NOVALUE;
    int _9449 = NOVALUE;
    int _9448 = NOVALUE;
    int _9447 = NOVALUE;
    int _9446 = NOVALUE;
    int _9445 = NOVALUE;
    int _9444 = NOVALUE;
    int _9443 = NOVALUE;
    int _9442 = NOVALUE;
    int _9441 = NOVALUE;
    int _9439 = NOVALUE;
    int _9438 = NOVALUE;
    int _9437 = NOVALUE;
    int _9436 = NOVALUE;
    int _9435 = NOVALUE;
    int _9433 = NOVALUE;
    int _9432 = NOVALUE;
    int _9431 = NOVALUE;
    int _9430 = NOVALUE;
    int _9429 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recursive_16387)) {
        _1 = (long)(DBL_PTR(_recursive_16387)->dbl);
        if (UNIQUE(DBL_PTR(_recursive_16387)) && (DBL_PTR(_recursive_16387)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recursive_16387);
        _recursive_16387 = _1;
    }

    /** 	object s = dir(inDir)*/
    RefDS(_inDir_16386);
    _0 = _s_16388;
    _s_16388 = _15dir(_inDir_16386);
    DeRef(_0);

    /** 	sequence z = ""*/
    RefDS(_5);
    DeRef(_z_16391);
    _z_16391 = _5;

    /** 	if atom(s) then*/
    _9429 = IS_ATOM(_s_16388);
    if (_9429 == 0)
    {
        _9429 = NOVALUE;
        goto L1; // [25] 34
    }
    else{
        _9429 = NOVALUE;
    }

    /** 		s = ""*/
    RefDS(_5);
    DeRef(_s_16388);
    _s_16388 = _5;
L1: 

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_16388)){
            _9430 = SEQ_PTR(_s_16388)->length;
    }
    else {
        _9430 = 1;
    }
    {
        int _i_16395;
        _i_16395 = 1;
L2: 
        if (_i_16395 > _9430){
            goto L3; // [39] 178
        }

        /** 		if not find('d', s[i][D_ATTRIBUTES]) then*/
        _2 = (int)SEQ_PTR(_s_16388);
        _9431 = (int)*(((s1_ptr)_2)->base + _i_16395);
        _2 = (int)SEQ_PTR(_9431);
        _9432 = (int)*(((s1_ptr)_2)->base + 2);
        _9431 = NOVALUE;
        _9433 = find_from(100, _9432, 1);
        _9432 = NOVALUE;
        if (_9433 != 0)
        goto L4; // [63] 105
        _9433 = NOVALUE;

        /** 			z &= {canonical_path(inDir & SLASH & s[i][D_NAME])}*/
        _2 = (int)SEQ_PTR(_s_16388);
        _9435 = (int)*(((s1_ptr)_2)->base + _i_16395);
        _2 = (int)SEQ_PTR(_9435);
        _9436 = (int)*(((s1_ptr)_2)->base + 1);
        _9435 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _9436;
            concat_list[1] = 92;
            concat_list[2] = _inDir_16386;
            Concat_N((object_ptr)&_9437, concat_list, 3);
        }
        _9436 = NOVALUE;
        _9438 = _15canonical_path(_9437, 0, 0);
        _9437 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _9438;
        _9439 = MAKE_SEQ(_1);
        _9438 = NOVALUE;
        Concat((object_ptr)&_z_16391, _z_16391, _9439);
        DeRefDS(_9439);
        _9439 = NOVALUE;
        goto L5; // [102] 171
L4: 

        /** 		elsif recursive and not find(s[i][D_NAME], {".",".."}) then*/
        if (_recursive_16387 == 0) {
            goto L6; // [107] 170
        }
        _2 = (int)SEQ_PTR(_s_16388);
        _9442 = (int)*(((s1_ptr)_2)->base + _i_16395);
        _2 = (int)SEQ_PTR(_9442);
        _9443 = (int)*(((s1_ptr)_2)->base + 1);
        _9442 = NOVALUE;
        RefDS(_3226);
        RefDS(_3194);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _3194;
        ((int *)_2)[2] = _3226;
        _9444 = MAKE_SEQ(_1);
        _9445 = find_from(_9443, _9444, 1);
        _9443 = NOVALUE;
        DeRefDS(_9444);
        _9444 = NOVALUE;
        _9446 = (_9445 == 0);
        _9445 = NOVALUE;
        if (_9446 == 0)
        {
            DeRef(_9446);
            _9446 = NOVALUE;
            goto L6; // [134] 170
        }
        else{
            DeRef(_9446);
            _9446 = NOVALUE;
        }

        /** 			z &= getListOfFiles(inDir & SLASH & s[i][D_NAME],recursive)*/
        _2 = (int)SEQ_PTR(_s_16388);
        _9447 = (int)*(((s1_ptr)_2)->base + _i_16395);
        _2 = (int)SEQ_PTR(_9447);
        _9448 = (int)*(((s1_ptr)_2)->base + 1);
        _9447 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _9448;
            concat_list[1] = 92;
            concat_list[2] = _inDir_16386;
            Concat_N((object_ptr)&_9449, concat_list, 3);
        }
        _9448 = NOVALUE;
        DeRef(_9450);
        _9450 = _recursive_16387;
        _9451 = _1getListOfFiles(_9449, _9450);
        _9449 = NOVALUE;
        _9450 = NOVALUE;
        if (IS_SEQUENCE(_z_16391) && IS_ATOM(_9451)) {
            Ref(_9451);
            Append(&_z_16391, _z_16391, _9451);
        }
        else if (IS_ATOM(_z_16391) && IS_SEQUENCE(_9451)) {
        }
        else {
            Concat((object_ptr)&_z_16391, _z_16391, _9451);
        }
        DeRef(_9451);
        _9451 = NOVALUE;
L6: 
L5: 

        /** 	end for*/
        _i_16395 = _i_16395 + 1;
        goto L2; // [173] 46
L3: 
        ;
    }

    /** 	return z*/
    DeRefDS(_inDir_16386);
    DeRef(_s_16388);
    return _z_16391;
    ;
}


void _1read_config(int _eu_cfg_16499)
{
    int _orig_dir_16500 = NOVALUE;
    int _lines_16503 = NOVALUE;
    int _cfg_path_16506 = NOVALUE;
    int _m_16515 = NOVALUE;
    int _9614 = NOVALUE;
    int _9613 = NOVALUE;
    int _9505 = NOVALUE;
    int _9504 = NOVALUE;
    int _9503 = NOVALUE;
    int _9502 = NOVALUE;
    int _9500 = NOVALUE;
    int _9499 = NOVALUE;
    int _9497 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence orig_dir = current_dir()*/
    _0 = _orig_dir_16500;
    _orig_dir_16500 = _15current_dir();
    DeRef(_0);

    /** 	sequence lines = read_lines( eu_cfg )*/
    RefDS(_eu_cfg_16499);
    _0 = _lines_16503;
    _lines_16503 = _6read_lines(_eu_cfg_16499);
    DeRef(_0);

    /** 	sequence cfg_path = pathname( canonical_path( eu_cfg ) )*/
    RefDS(_eu_cfg_16499);
    _9497 = _15canonical_path(_eu_cfg_16499, 0, 0);
    _0 = _cfg_path_16506;
    _cfg_path_16506 = _15pathname(_9497);
    DeRef(_0);
    _9497 = NOVALUE;

    /** 	chdir( cfg_path )*/
    RefDS(_cfg_path_16506);
    _9614 = _15chdir(_cfg_path_16506);
    DeRef(_9614);
    _9614 = NOVALUE;

    /** 	for lx = 1 to length( lines ) do*/
    if (IS_SEQUENCE(_lines_16503)){
            _9499 = SEQ_PTR(_lines_16503)->length;
    }
    else {
        _9499 = 1;
    }
    {
        int _lx_16513;
        _lx_16513 = 1;
L1: 
        if (_lx_16513 > _9499){
            goto L2; // [47] 112
        }

        /** 		object m = regex:matches( inc_path, lines[lx] )*/
        _2 = (int)SEQ_PTR(_lines_16503);
        _9500 = (int)*(((s1_ptr)_2)->base + _lx_16513);
        Ref(_1inc_path_16494);
        Ref(_9500);
        _0 = _m_16515;
        _m_16515 = _33matches(_1inc_path_16494, _9500, 1, 0);
        DeRef(_0);
        _9500 = NOVALUE;

        /** 		if sequence( m ) then*/
        _9502 = IS_SEQUENCE(_m_16515);
        if (_9502 == 0)
        {
            _9502 = NOVALUE;
            goto L3; // [74] 103
        }
        else{
            _9502 = NOVALUE;
        }

        /** 			Place = append( Place, canonical_path( slashifier( m[2] ) ) )*/
        _2 = (int)SEQ_PTR(_m_16515);
        _9503 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_9503);
        _9504 = _1slashifier(_9503, 0);
        _9503 = NOVALUE;
        _9505 = _15canonical_path(_9504, 0, 0);
        _9504 = NOVALUE;
        Ref(_9505);
        Append(&_1Place_16166, _1Place_16166, _9505);
        DeRef(_9505);
        _9505 = NOVALUE;
L3: 
        DeRef(_m_16515);
        _m_16515 = NOVALUE;

        /** 	end for*/
        _lx_16513 = _lx_16513 + 1;
        goto L1; // [107] 54
L2: 
        ;
    }

    /** 	chdir( orig_dir )*/
    RefDS(_orig_dir_16500);
    _9613 = _15chdir(_orig_dir_16500);
    DeRef(_9613);
    _9613 = NOVALUE;

    /** end procedure*/
    DeRefDS(_eu_cfg_16499);
    DeRefDS(_orig_dir_16500);
    DeRef(_lines_16503);
    DeRef(_cfg_path_16506);
    return;
    ;
}


void _1run()
{
    int _stringifier_1__tmp_at430_16630 = NOVALUE;
    int _stringifier_inlined_stringifier_at_430_16629 = NOVALUE;
    int _s_inlined_stringifier_at_427_16628 = NOVALUE;
    int _stringifier_1__tmp_at234_16588 = NOVALUE;
    int _stringifier_inlined_stringifier_at_234_16587 = NOVALUE;
    int _s_inlined_stringifier_at_231_16586 = NOVALUE;
    int _default_dir_16529 = NOVALUE;
    int _start_dir_16535 = NOVALUE;
    int _params_16540 = NOVALUE;
    int _inFileName_16543 = NOVALUE;
    int _configFiles_16545 = NOVALUE;
    int _excludeDirRec_16547 = NOVALUE;
    int _excludeDirs_16549 = NOVALUE;
    int _excludeFiles_16551 = NOVALUE;
    int _verbose_16553 = NOVALUE;
    int _default_config_file_16593 = NOVALUE;
    int _9612 = NOVALUE;
    int _9611 = NOVALUE;
    int _9610 = NOVALUE;
    int _9609 = NOVALUE;
    int _9608 = NOVALUE;
    int _9607 = NOVALUE;
    int _9606 = NOVALUE;
    int _9605 = NOVALUE;
    int _9604 = NOVALUE;
    int _9603 = NOVALUE;
    int _9601 = NOVALUE;
    int _9600 = NOVALUE;
    int _9599 = NOVALUE;
    int _9598 = NOVALUE;
    int _9595 = NOVALUE;
    int _9593 = NOVALUE;
    int _9591 = NOVALUE;
    int _9590 = NOVALUE;
    int _9589 = NOVALUE;
    int _9588 = NOVALUE;
    int _9587 = NOVALUE;
    int _9586 = NOVALUE;
    int _9585 = NOVALUE;
    int _9583 = NOVALUE;
    int _9582 = NOVALUE;
    int _9581 = NOVALUE;
    int _9580 = NOVALUE;
    int _9579 = NOVALUE;
    int _9578 = NOVALUE;
    int _9577 = NOVALUE;
    int _9575 = NOVALUE;
    int _9574 = NOVALUE;
    int _9573 = NOVALUE;
    int _9572 = NOVALUE;
    int _9571 = NOVALUE;
    int _9570 = NOVALUE;
    int _9569 = NOVALUE;
    int _9567 = NOVALUE;
    int _9565 = NOVALUE;
    int _9564 = NOVALUE;
    int _9563 = NOVALUE;
    int _9562 = NOVALUE;
    int _9561 = NOVALUE;
    int _9560 = NOVALUE;
    int _9557 = NOVALUE;
    int _9556 = NOVALUE;
    int _9555 = NOVALUE;
    int _9554 = NOVALUE;
    int _9552 = NOVALUE;
    int _9551 = NOVALUE;
    int _9550 = NOVALUE;
    int _9549 = NOVALUE;
    int _9548 = NOVALUE;
    int _9546 = NOVALUE;
    int _9544 = NOVALUE;
    int _9543 = NOVALUE;
    int _9542 = NOVALUE;
    int _9540 = NOVALUE;
    int _9539 = NOVALUE;
    int _9537 = NOVALUE;
    int _9536 = NOVALUE;
    int _9535 = NOVALUE;
    int _9534 = NOVALUE;
    int _9533 = NOVALUE;
    int _9532 = NOVALUE;
    int _9531 = NOVALUE;
    int _9530 = NOVALUE;
    int _9529 = NOVALUE;
    int _9526 = NOVALUE;
    int _9522 = NOVALUE;
    int _9521 = NOVALUE;
    int _9511 = NOVALUE;
    int _9508 = NOVALUE;
    int _0, _1, _2;
    

    /**     puts(1, "Euphoria distribution helper v1.0\n")*/
    EPuts(1, _9507); // DJP 

    /** 	sequence default_dir = current_dir() & SLASH & "eudist"*/
    _9508 = _15current_dir();
    {
        int concat_list[3];

        concat_list[0] = _9509;
        concat_list[1] = 92;
        concat_list[2] = _9508;
        Concat_N((object_ptr)&_default_dir_16529, concat_list, 3);
    }
    DeRef(_9508);
    _9508 = NOVALUE;

    /** 	sequence start_dir   = current_dir() & SLASH*/
    _9511 = _15current_dir();
    if (IS_SEQUENCE(_9511) && IS_ATOM(92)) {
        Append(&_start_dir_16535, _9511, 92);
    }
    else if (IS_ATOM(_9511) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_start_dir_16535, _9511, 92);
        DeRef(_9511);
        _9511 = NOVALUE;
    }
    DeRef(_9511);
    _9511 = NOVALUE;

    /**     map:map params = cmd_parse(cmd_params)*/
    _9612 = Command_Line();
    RefDS(_1cmd_params_16428);
    RefDS(_5);
    _0 = _params_16540;
    _params_16540 = _2cmd_parse(_1cmd_params_16428, _5, _9612);
    DeRef(_0);
    _9612 = NOVALUE;

    /**     object */

    /** 		inFileName    = map:get( params, cmdline:EXTRAS, {} ),*/
    Ref(_params_16540);
    RefDS(_2EXTRAS_14364);
    RefDS(_5);
    _0 = _inFileName_16543;
    _inFileName_16543 = _26get(_params_16540, _2EXTRAS_14364, _5);
    DeRef(_0);

    /** 		configFiles   = map:get( params, "c", {} ),*/
    Ref(_params_16540);
    RefDS(_9453);
    RefDS(_5);
    _0 = _configFiles_16545;
    _configFiles_16545 = _26get(_params_16540, _9453, _5);
    DeRef(_0);

    /** 		excludeDirRec = map:get( params, "edr"),*/
    Ref(_params_16540);
    RefDS(_9477);
    _0 = _excludeDirRec_16547;
    _excludeDirRec_16547 = _26get(_params_16540, _9477, 0);
    DeRef(_0);

    /** 		excludeDirs   = map:get( params, "ed"),*/
    Ref(_params_16540);
    RefDS(_9472);
    _0 = _excludeDirs_16549;
    _excludeDirs_16549 = _26get(_params_16540, _9472, 0);
    DeRef(_0);

    /** 		excludeFiles  = map:get( params, "e"),*/
    Ref(_params_16540);
    RefDS(_9466);
    _0 = _excludeFiles_16551;
    _excludeFiles_16551 = _26get(_params_16540, _9466, 0);
    DeRef(_0);

    /** 		verbose       = map:get( params, "verbose", 0 )*/
    Ref(_params_16540);
    RefDS(_9488);
    _0 = _verbose_16553;
    _verbose_16553 = _26get(_params_16540, _9488, 0);
    DeRef(_0);

    /** 	outputDir = map:get(params, "d")*/
    Ref(_params_16540);
    RefDS(_9461);
    _0 = _26get(_params_16540, _9461, 0);
    DeRef(_1outputDir_16144);
    _1outputDir_16144 = _0;

    /** 	if atom( outputDir ) then*/
    _9521 = IS_ATOM(_1outputDir_16144);
    if (_9521 == 0)
    {
        _9521 = NOVALUE;
        goto L1; // [109] 118
    }
    else{
        _9521 = NOVALUE;
    }

    /** 		outputDir = default_dir*/
    RefDS(_default_dir_16529);
    DeRef(_1outputDir_16144);
    _1outputDir_16144 = _default_dir_16529;
L1: 

    /** 	if not absolute_path( outputDir ) then*/
    Ref(_1outputDir_16144);
    _9522 = _15absolute_path(_1outputDir_16144);
    if (IS_ATOM_INT(_9522)) {
        if (_9522 != 0){
            DeRef(_9522);
            _9522 = NOVALUE;
            goto L2; // [126] 138
        }
    }
    else {
        if (DBL_PTR(_9522)->dbl != 0.0){
            DeRef(_9522);
            _9522 = NOVALUE;
            goto L2; // [126] 138
        }
    }
    DeRef(_9522);
    _9522 = NOVALUE;

    /** 		outputDir = start_dir & outputDir*/
    if (IS_SEQUENCE(_start_dir_16535) && IS_ATOM(_1outputDir_16144)) {
        Ref(_1outputDir_16144);
        Append(&_1outputDir_16144, _start_dir_16535, _1outputDir_16144);
    }
    else if (IS_ATOM(_start_dir_16535) && IS_SEQUENCE(_1outputDir_16144)) {
    }
    else {
        Concat((object_ptr)&_1outputDir_16144, _start_dir_16535, _1outputDir_16144);
    }
L2: 

    /** 	outputDir = slashifier( outputDir )*/
    Ref(_1outputDir_16144);
    _0 = _1slashifier(_1outputDir_16144, 0);
    DeRef(_1outputDir_16144);
    _1outputDir_16144 = _0;

    /** 	if length( inFileName ) < 1 then*/
    if (IS_SEQUENCE(_inFileName_16543)){
            _9526 = SEQ_PTR(_inFileName_16543)->length;
    }
    else {
        _9526 = 1;
    }
    if (_9526 >= 1)
    goto L3; // [152] 168

    /** 		puts(2, "You must specify at least a single file\n" )*/
    EPuts(2, _9528); // DJP 

    /** 		abort( 1 )*/
    UserCleanup(1);
    goto L4; // [165] 220
L3: 

    /** 		if eu:compare( pathname( inFileName[1] ), current_dir() ) then*/
    _2 = (int)SEQ_PTR(_inFileName_16543);
    _9529 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_9529);
    _9530 = _15pathname(_9529);
    _9529 = NOVALUE;
    _9531 = _15current_dir();
    if (IS_ATOM_INT(_9530) && IS_ATOM_INT(_9531)){
        _9532 = (_9530 < _9531) ? -1 : (_9530 > _9531);
    }
    else{
        _9532 = compare(_9530, _9531);
    }
    DeRef(_9530);
    _9530 = NOVALUE;
    DeRef(_9531);
    _9531 = NOVALUE;
    if (_9532 == 0)
    {
        _9532 = NOVALUE;
        goto L5; // [185] 219
    }
    else{
        _9532 = NOVALUE;
    }

    /** 			chdir( pathname( inFileName[1] ) )*/
    _2 = (int)SEQ_PTR(_inFileName_16543);
    _9533 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_9533);
    _9534 = _15pathname(_9533);
    _9533 = NOVALUE;
    _9611 = _15chdir(_9534);
    _9534 = NOVALUE;
    DeRef(_9611);
    _9611 = NOVALUE;

    /** 			inFileName[1] = filename( inFileName[1] )*/
    _2 = (int)SEQ_PTR(_inFileName_16543);
    _9535 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_9535);
    _9536 = _15filename(_9535);
    _9535 = NOVALUE;
    _2 = (int)SEQ_PTR(_inFileName_16543);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _inFileName_16543 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _9536;
    if( _1 != _9536 ){
        DeRef(_1);
    }
    _9536 = NOVALUE;
L5: 
L4: 

    /**     Place &= apply(stringifier(map:get(params, "include")), routine_id("slashifier"))*/
    Ref(_params_16540);
    RefDS(_9483);
    _9537 = _26get(_params_16540, _9483, 0);
    DeRef(_s_inlined_stringifier_at_231_16586);
    _s_inlined_stringifier_at_231_16586 = _9537;
    _9537 = NOVALUE;

    /** 	if atom(s) then*/
    _stringifier_1__tmp_at234_16588 = IS_ATOM(_s_inlined_stringifier_at_231_16586);
    if (_stringifier_1__tmp_at234_16588 == 0)
    {
        goto L6; // [238] 249
    }
    else{
    }

    /** 		return ""*/
    RefDS(_5);
    DeRef(_stringifier_inlined_stringifier_at_234_16587);
    _stringifier_inlined_stringifier_at_234_16587 = _5;
    goto L7; // [246] 255
L6: 

    /** 	return s*/
    Ref(_s_inlined_stringifier_at_231_16586);
    DeRef(_stringifier_inlined_stringifier_at_234_16587);
    _stringifier_inlined_stringifier_at_234_16587 = _s_inlined_stringifier_at_231_16586;
L7: 
    DeRef(_s_inlined_stringifier_at_231_16586);
    _s_inlined_stringifier_at_231_16586 = NOVALUE;
    _9539 = CRoutineId(539, 1, _9538);
    Ref(_stringifier_inlined_stringifier_at_234_16587);
    RefDS(_5);
    _9540 = _21apply(_stringifier_inlined_stringifier_at_234_16587, _9539, _5);
    _9539 = NOVALUE;
    if (IS_SEQUENCE(_1Place_16166) && IS_ATOM(_9540)) {
        Ref(_9540);
        Append(&_1Place_16166, _1Place_16166, _9540);
    }
    else if (IS_ATOM(_1Place_16166) && IS_SEQUENCE(_9540)) {
    }
    else {
        Concat((object_ptr)&_1Place_16166, _1Place_16166, _9540);
    }
    DeRef(_9540);
    _9540 = NOVALUE;

    /** 	sequence default_config_file = slashifier( pathname( inFileName[1] ) ) & "eu.cfg"*/
    _2 = (int)SEQ_PTR(_inFileName_16543);
    _9542 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_9542);
    _9543 = _15pathname(_9542);
    _9542 = NOVALUE;
    _9544 = _1slashifier(_9543, 0);
    _9543 = NOVALUE;
    if (IS_SEQUENCE(_9544) && IS_ATOM(_9455)) {
    }
    else if (IS_ATOM(_9544) && IS_SEQUENCE(_9455)) {
        Ref(_9544);
        Prepend(&_default_config_file_16593, _9455, _9544);
    }
    else {
        Concat((object_ptr)&_default_config_file_16593, _9544, _9455);
        DeRef(_9544);
        _9544 = NOVALUE;
    }
    DeRef(_9544);
    _9544 = NOVALUE;

    /** 	if file_exists( default_config_file ) then*/
    RefDS(_default_config_file_16593);
    _9546 = _15file_exists(_default_config_file_16593);
    if (_9546 == 0) {
        DeRef(_9546);
        _9546 = NOVALUE;
        goto L8; // [297] 307
    }
    else {
        if (!IS_ATOM_INT(_9546) && DBL_PTR(_9546)->dbl == 0.0){
            DeRef(_9546);
            _9546 = NOVALUE;
            goto L8; // [297] 307
        }
        DeRef(_9546);
        _9546 = NOVALUE;
    }
    DeRef(_9546);
    _9546 = NOVALUE;

    /** 		configFiles = prepend( configFiles, default_config_file )*/
    RefDS(_default_config_file_16593);
    Prepend(&_configFiles_16545, _configFiles_16545, _default_config_file_16593);
L8: 

    /** 	for i = 1 to length( configFiles ) do*/
    if (IS_SEQUENCE(_configFiles_16545)){
            _9548 = SEQ_PTR(_configFiles_16545)->length;
    }
    else {
        _9548 = 1;
    }
    {
        int _i_16604;
        _i_16604 = 1;
L9: 
        if (_i_16604 > _9548){
            goto LA; // [312] 335
        }

        /** 		read_config( configFiles[i] )*/
        _2 = (int)SEQ_PTR(_configFiles_16545);
        _9549 = (int)*(((s1_ptr)_2)->base + _i_16604);
        Ref(_9549);
        _1read_config(_9549);
        _9549 = NOVALUE;

        /** 	end for*/
        _i_16604 = _i_16604 + 1;
        goto L9; // [330] 319
LA: 
        ;
    }

    /** 	Place &= apply( include_paths( 1 ), routine_id("slashifier") )*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_3896);
    *((int *)(_2+4)) = _3896;
    RefDS(_3895);
    *((int *)(_2+8)) = _3895;
    RefDS(_3894);
    *((int *)(_2+12)) = _3894;
    RefDS(_3893);
    *((int *)(_2+16)) = _3893;
    RefDS(_3892);
    *((int *)(_2+20)) = _3892;
    _9550 = MAKE_SEQ(_1);
    _9551 = CRoutineId(539, 1, _9538);
    RefDS(_5);
    _9552 = _21apply(_9550, _9551, _5);
    _9550 = NOVALUE;
    _9551 = NOVALUE;
    if (IS_SEQUENCE(_1Place_16166) && IS_ATOM(_9552)) {
        Ref(_9552);
        Append(&_1Place_16166, _1Place_16166, _9552);
    }
    else if (IS_ATOM(_1Place_16166) && IS_SEQUENCE(_9552)) {
    }
    else {
        Concat((object_ptr)&_1Place_16166, _1Place_16166, _9552);
    }
    DeRef(_9552);
    _9552 = NOVALUE;

    /** 	if sequence( EuPlace ) then*/
    _9554 = IS_SEQUENCE(_1EuPlace_16164);
    if (_9554 == 0)
    {
        _9554 = NOVALUE;
        goto LB; // [369] 406
    }
    else{
        _9554 = NOVALUE;
    }

    /** 		Place &= { EuPlace & SLASH, EuPlace & SLASH & "include" & SLASH }*/
    if (IS_SEQUENCE(_1EuPlace_16164) && IS_ATOM(92)) {
        Append(&_9555, _1EuPlace_16164, 92);
    }
    else if (IS_ATOM(_1EuPlace_16164) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_9555, _1EuPlace_16164, 92);
    }
    {
        int concat_list[4];

        concat_list[0] = 92;
        concat_list[1] = _9483;
        concat_list[2] = 92;
        concat_list[3] = _1EuPlace_16164;
        Concat_N((object_ptr)&_9556, concat_list, 4);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _9555;
    ((int *)_2)[2] = _9556;
    _9557 = MAKE_SEQ(_1);
    _9556 = NOVALUE;
    _9555 = NOVALUE;
    Concat((object_ptr)&_1Place_16166, _1Place_16166, _9557);
    DeRefDS(_9557);
    _9557 = NOVALUE;
LB: 

    /** 	if sequence( getenv( "EUINC" ) ) then*/
    _9560 = EGetEnv(_9559);
    _9561 = IS_SEQUENCE(_9560);
    DeRef(_9560);
    _9560 = NOVALUE;
    if (_9561 == 0)
    {
        _9561 = NOVALUE;
        goto LC; // [414] 476
    }
    else{
        _9561 = NOVALUE;
    }

    /** 		Place &= apply( stdseq:split( stringifier(getenv("EUINC")),PATHSEP), routine_id("slashifier") )*/
    _9562 = EGetEnv(_9559);
    DeRefi(_s_inlined_stringifier_at_427_16628);
    _s_inlined_stringifier_at_427_16628 = _9562;
    _9562 = NOVALUE;

    /** 	if atom(s) then*/
    _stringifier_1__tmp_at430_16630 = IS_ATOM(_s_inlined_stringifier_at_427_16628);
    if (_stringifier_1__tmp_at430_16630 == 0)
    {
        goto LD; // [432] 443
    }
    else{
    }

    /** 		return ""*/
    RefDS(_5);
    DeRefi(_stringifier_inlined_stringifier_at_430_16629);
    _stringifier_inlined_stringifier_at_430_16629 = _5;
    goto LE; // [440] 449
LD: 

    /** 	return s*/
    Ref(_s_inlined_stringifier_at_427_16628);
    DeRefi(_stringifier_inlined_stringifier_at_430_16629);
    _stringifier_inlined_stringifier_at_430_16629 = _s_inlined_stringifier_at_427_16628;
LE: 
    DeRefi(_s_inlined_stringifier_at_427_16628);
    _s_inlined_stringifier_at_427_16628 = NOVALUE;
    Ref(_stringifier_inlined_stringifier_at_430_16629);
    _9563 = _21split(_stringifier_inlined_stringifier_at_430_16629, 59, 0, 0);
    _9564 = CRoutineId(539, 1, _9538);
    RefDS(_5);
    _9565 = _21apply(_9563, _9564, _5);
    _9563 = NOVALUE;
    _9564 = NOVALUE;
    if (IS_SEQUENCE(_1Place_16166) && IS_ATOM(_9565)) {
        Ref(_9565);
        Append(&_1Place_16166, _1Place_16166, _9565);
    }
    else if (IS_ATOM(_1Place_16166) && IS_SEQUENCE(_9565)) {
    }
    else {
        Concat((object_ptr)&_1Place_16166, _1Place_16166, _9565);
    }
    DeRef(_9565);
    _9565 = NOVALUE;
LC: 

    /** 	Place &= { "" }*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_5);
    *((int *)(_2+4)) = _5;
    _9567 = MAKE_SEQ(_1);
    Concat((object_ptr)&_1Place_16166, _1Place_16166, _9567);
    DeRefDS(_9567);
    _9567 = NOVALUE;

    /** 	if sequence(excludeFiles) and length(excludeFiles) then*/
    _9569 = IS_SEQUENCE(_excludeFiles_16551);
    if (_9569 == 0) {
        goto LF; // [493] 548
    }
    if (IS_SEQUENCE(_excludeFiles_16551)){
            _9571 = SEQ_PTR(_excludeFiles_16551)->length;
    }
    else {
        _9571 = 1;
    }
    if (_9571 == 0)
    {
        _9571 = NOVALUE;
        goto LF; // [501] 548
    }
    else{
        _9571 = NOVALUE;
    }

    /** 		for i = 1 to length(excludeFiles) do*/
    if (IS_SEQUENCE(_excludeFiles_16551)){
            _9572 = SEQ_PTR(_excludeFiles_16551)->length;
    }
    else {
        _9572 = 1;
    }
    {
        int _i_16643;
        _i_16643 = 1;
L10: 
        if (_i_16643 > _9572){
            goto L11; // [509] 547
        }

        /** 			excludedIncludes = append(excludedIncludes, canonical_path(excludeFiles[i+1]))*/
        _9573 = _i_16643 + 1;
        _2 = (int)SEQ_PTR(_excludeFiles_16551);
        _9574 = (int)*(((s1_ptr)_2)->base + _9573);
        Ref(_9574);
        _9575 = _15canonical_path(_9574, 0, 0);
        _9574 = NOVALUE;
        Ref(_9575);
        Append(&_1excludedIncludes_16143, _1excludedIncludes_16143, _9575);
        DeRef(_9575);
        _9575 = NOVALUE;

        /** 		end for*/
        _i_16643 = _i_16643 + 1;
        goto L10; // [542] 516
L11: 
        ;
    }
LF: 

    /** 	if sequence(excludeDirs) and length(excludeDirs) then*/
    _9577 = IS_SEQUENCE(_excludeDirs_16549);
    if (_9577 == 0) {
        goto L12; // [553] 605
    }
    if (IS_SEQUENCE(_excludeDirs_16549)){
            _9579 = SEQ_PTR(_excludeDirs_16549)->length;
    }
    else {
        _9579 = 1;
    }
    if (_9579 == 0)
    {
        _9579 = NOVALUE;
        goto L12; // [561] 605
    }
    else{
        _9579 = NOVALUE;
    }

    /** 		for i = 1 to length(excludeDirs) do*/
    if (IS_SEQUENCE(_excludeDirs_16549)){
            _9580 = SEQ_PTR(_excludeDirs_16549)->length;
    }
    else {
        _9580 = 1;
    }
    {
        int _i_16655;
        _i_16655 = 1;
L13: 
        if (_i_16655 > _9580){
            goto L14; // [569] 604
        }

        /** 			excludedIncludes &= getListOfFiles(excludeDirs[i+1])*/
        _9581 = _i_16655 + 1;
        _2 = (int)SEQ_PTR(_excludeDirs_16549);
        _9582 = (int)*(((s1_ptr)_2)->base + _9581);
        Ref(_9582);
        _9583 = _1getListOfFiles(_9582, 0);
        _9582 = NOVALUE;
        if (IS_SEQUENCE(_1excludedIncludes_16143) && IS_ATOM(_9583)) {
            Ref(_9583);
            Append(&_1excludedIncludes_16143, _1excludedIncludes_16143, _9583);
        }
        else if (IS_ATOM(_1excludedIncludes_16143) && IS_SEQUENCE(_9583)) {
        }
        else {
            Concat((object_ptr)&_1excludedIncludes_16143, _1excludedIncludes_16143, _9583);
        }
        DeRef(_9583);
        _9583 = NOVALUE;

        /** 		end for*/
        _i_16655 = _i_16655 + 1;
        goto L13; // [599] 576
L14: 
        ;
    }
L12: 

    /** 	if sequence(excludeDirs) and length(excludeDirs) then*/
    _9585 = IS_SEQUENCE(_excludeDirs_16549);
    if (_9585 == 0) {
        goto L15; // [610] 662
    }
    if (IS_SEQUENCE(_excludeDirs_16549)){
            _9587 = SEQ_PTR(_excludeDirs_16549)->length;
    }
    else {
        _9587 = 1;
    }
    if (_9587 == 0)
    {
        _9587 = NOVALUE;
        goto L15; // [618] 662
    }
    else{
        _9587 = NOVALUE;
    }

    /** 		for i = 1 to length(excludeFiles) do*/
    if (IS_SEQUENCE(_excludeFiles_16551)){
            _9588 = SEQ_PTR(_excludeFiles_16551)->length;
    }
    else {
        _9588 = 1;
    }
    {
        int _i_16666;
        _i_16666 = 1;
L16: 
        if (_i_16666 > _9588){
            goto L17; // [626] 661
        }

        /** 			excludedIncludes &= getListOfFiles(excludeDirRec[i+1],1)*/
        _9589 = _i_16666 + 1;
        _2 = (int)SEQ_PTR(_excludeDirRec_16547);
        _9590 = (int)*(((s1_ptr)_2)->base + _9589);
        Ref(_9590);
        _9591 = _1getListOfFiles(_9590, 1);
        _9590 = NOVALUE;
        if (IS_SEQUENCE(_1excludedIncludes_16143) && IS_ATOM(_9591)) {
            Ref(_9591);
            Append(&_1excludedIncludes_16143, _1excludedIncludes_16143, _9591);
        }
        else if (IS_ATOM(_1excludedIncludes_16143) && IS_SEQUENCE(_9591)) {
        }
        else {
            Concat((object_ptr)&_1excludedIncludes_16143, _1excludedIncludes_16143, _9591);
        }
        DeRef(_9591);
        _9591 = NOVALUE;

        /** 		end for*/
        _i_16666 = _i_16666 + 1;
        goto L16; // [656] 633
L17: 
        ;
    }
L15: 

    /** 	if not file_exists( outputDir ) then*/
    Ref(_1outputDir_16144);
    _9593 = _15file_exists(_1outputDir_16144);
    if (IS_ATOM_INT(_9593)) {
        if (_9593 != 0){
            DeRef(_9593);
            _9593 = NOVALUE;
            goto L18; // [670] 688
        }
    }
    else {
        if (DBL_PTR(_9593)->dbl != 0.0){
            DeRef(_9593);
            _9593 = NOVALUE;
            goto L18; // [670] 688
        }
    }
    DeRef(_9593);
    _9593 = NOVALUE;

    /** 		create_directory( outputDir )*/
    Ref(_1outputDir_16144);
    _9610 = _15create_directory(_1outputDir_16144, 448, 1);
    DeRef(_9610);
    _9610 = NOVALUE;
    goto L19; // [685] 735
L18: 

    /** 	elsif map:get( params, "clear", 0 ) then*/
    Ref(_params_16540);
    RefDS(_9458);
    _9595 = _26get(_params_16540, _9458, 0);
    if (_9595 == 0) {
        DeRef(_9595);
        _9595 = NOVALUE;
        goto L1A; // [696] 734
    }
    else {
        if (!IS_ATOM_INT(_9595) && DBL_PTR(_9595)->dbl == 0.0){
            DeRef(_9595);
            _9595 = NOVALUE;
            goto L1A; // [696] 734
        }
        DeRef(_9595);
        _9595 = NOVALUE;
    }
    DeRef(_9595);
    _9595 = NOVALUE;

    /** 		remove_directory( outputDir, 1 )*/
    Ref(_1outputDir_16144);
    _9609 = _15remove_directory(_1outputDir_16144, 1);
    DeRef(_9609);
    _9609 = NOVALUE;

    /** 		create_directory( outputDir )*/
    Ref(_1outputDir_16144);
    _9608 = _15create_directory(_1outputDir_16144, 448, 1);
    DeRef(_9608);
    _9608 = NOVALUE;

    /** 		if verbose then*/
    if (_verbose_16553 == 0) {
        goto L1B; // [724] 733
    }
    else {
        if (!IS_ATOM_INT(_verbose_16553) && DBL_PTR(_verbose_16553)->dbl == 0.0){
            goto L1B; // [724] 733
        }
    }

    /** 			puts(1, "clearing the output directory\n")*/
    EPuts(1, _9596); // DJP 
L1B: 
L1A: 
L19: 

    /** 	printf(1, "Outputting files to directory: %s\n", {outputDir})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_1outputDir_16144);
    *((int *)(_2+4)) = _1outputDir_16144;
    _9598 = MAKE_SEQ(_1);
    EPrintf(1, _9597, _9598);
    DeRefDS(_9598);
    _9598 = NOVALUE;

    /** 	for i = 1 to length(inFileName) do*/
    if (IS_SEQUENCE(_inFileName_16543)){
            _9599 = SEQ_PTR(_inFileName_16543)->length;
    }
    else {
        _9599 = 1;
    }
    {
        int _i_16686;
        _i_16686 = 1;
L1C: 
        if (_i_16686 > _9599){
            goto L1D; // [752] 777
        }

        /** 	parseFile( inFileName[i] )*/
        _2 = (int)SEQ_PTR(_inFileName_16543);
        _9600 = (int)*(((s1_ptr)_2)->base + _i_16686);
        Ref(_9600);
        RefDS(_5);
        _9601 = _1parseFile(_9600, _5);
        _9600 = NOVALUE;

        /** 	end for*/
        _i_16686 = _i_16686 + 1;
        goto L1C; // [772] 759
L1D: 
        ;
    }

    /** 	printf(1, "\n%d files were found.\n", {length(included)})*/
    if (IS_SEQUENCE(_1included_16142)){
            _9603 = SEQ_PTR(_1included_16142)->length;
    }
    else {
        _9603 = 1;
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _9603;
    _9604 = MAKE_SEQ(_1);
    _9603 = NOVALUE;
    EPrintf(1, _9602, _9604);
    DeRefDS(_9604);
    _9604 = NOVALUE;

    /** 	if verbose then*/
    if (_verbose_16553 == 0) {
        goto L1E; // [794] 837
    }
    else {
        if (!IS_ATOM_INT(_verbose_16553) && DBL_PTR(_verbose_16553)->dbl == 0.0){
            goto L1E; // [794] 837
        }
    }

    /** 		for i = 1 to length(included) do*/
    if (IS_SEQUENCE(_1included_16142)){
            _9605 = SEQ_PTR(_1included_16142)->length;
    }
    else {
        _9605 = 1;
    }
    {
        int _i_16695;
        _i_16695 = 1;
L1F: 
        if (_i_16695 > _9605){
            goto L20; // [804] 834
        }

        /** 			printf(1, "%s\n", {included[i]})*/
        _2 = (int)SEQ_PTR(_1included_16142);
        _9606 = (int)*(((s1_ptr)_2)->base + _i_16695);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_9606);
        *((int *)(_2+4)) = _9606;
        _9607 = MAKE_SEQ(_1);
        _9606 = NOVALUE;
        EPrintf(1, _8888, _9607);
        DeRefDS(_9607);
        _9607 = NOVALUE;

        /** 		end for*/
        _i_16695 = _i_16695 + 1;
        goto L1F; // [829] 811
L20: 
        ;
    }
    goto L21; // [834] 838
L1E: 
L21: 

    /** end procedure*/
    DeRef(_default_dir_16529);
    DeRef(_start_dir_16535);
    DeRef(_params_16540);
    DeRef(_inFileName_16543);
    DeRef(_configFiles_16545);
    DeRef(_excludeDirRec_16547);
    DeRef(_excludeDirs_16549);
    DeRef(_excludeFiles_16551);
    DeRef(_verbose_16553);
    DeRef(_default_config_file_16593);
    DeRef(_9573);
    _9573 = NOVALUE;
    DeRef(_9581);
    _9581 = NOVALUE;
    DeRef(_9589);
    _9589 = NOVALUE;
    DeRef(_9601);
    _9601 = NOVALUE;
    return;
    ;
}



// 0x5C78DC29
