// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _12sprint(int _x_8225)
{
    int _s_8226 = NOVALUE;
    int _4577 = NOVALUE;
    int _4575 = NOVALUE;
    int _4574 = NOVALUE;
    int _4571 = NOVALUE;
    int _4570 = NOVALUE;
    int _4568 = NOVALUE;
    int _4567 = NOVALUE;
    int _4566 = NOVALUE;
    int _4565 = NOVALUE;
    int _4564 = NOVALUE;
    int _4563 = NOVALUE;
    int _4562 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(x) then*/
    _4562 = IS_ATOM(_x_8225);
    if (_4562 == 0)
    {
        _4562 = NOVALUE;
        goto L1; // [6] 22
    }
    else{
        _4562 = NOVALUE;
    }

    /** 		return sprintf("%.10g", x)*/
    _4563 = EPrintf(-9999999, _4231, _x_8225);
    DeRef(_x_8225);
    DeRef(_s_8226);
    return _4563;
    goto L2; // [19] 137
L1: 

    /** 		s = "{"*/
    RefDS(_962);
    DeRef(_s_8226);
    _s_8226 = _962;

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_8225)){
            _4564 = SEQ_PTR(_x_8225)->length;
    }
    else {
        _4564 = 1;
    }
    {
        int _i_8232;
        _i_8232 = 1;
L3: 
        if (_i_8232 > _4564){
            goto L4; // [34] 98
        }

        /** 			if atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_8225);
        _4565 = (int)*(((s1_ptr)_2)->base + _i_8232);
        _4566 = IS_ATOM(_4565);
        _4565 = NOVALUE;
        if (_4566 == 0)
        {
            _4566 = NOVALUE;
            goto L5; // [50] 70
        }
        else{
            _4566 = NOVALUE;
        }

        /** 				s &= sprintf("%.10g", x[i])*/
        _2 = (int)SEQ_PTR(_x_8225);
        _4567 = (int)*(((s1_ptr)_2)->base + _i_8232);
        _4568 = EPrintf(-9999999, _4231, _4567);
        _4567 = NOVALUE;
        Concat((object_ptr)&_s_8226, _s_8226, _4568);
        DeRefDS(_4568);
        _4568 = NOVALUE;
        goto L6; // [67] 85
L5: 

        /** 				s &= sprint(x[i])*/
        _2 = (int)SEQ_PTR(_x_8225);
        _4570 = (int)*(((s1_ptr)_2)->base + _i_8232);
        Ref(_4570);
        _4571 = _12sprint(_4570);
        _4570 = NOVALUE;
        if (IS_SEQUENCE(_s_8226) && IS_ATOM(_4571)) {
            Ref(_4571);
            Append(&_s_8226, _s_8226, _4571);
        }
        else if (IS_ATOM(_s_8226) && IS_SEQUENCE(_4571)) {
        }
        else {
            Concat((object_ptr)&_s_8226, _s_8226, _4571);
        }
        DeRef(_4571);
        _4571 = NOVALUE;
L6: 

        /** 			s &= ','*/
        Append(&_s_8226, _s_8226, 44);

        /** 		end for*/
        _i_8232 = _i_8232 + 1;
        goto L3; // [93] 41
L4: 
        ;
    }

    /** 		if s[$] = ',' then*/
    if (IS_SEQUENCE(_s_8226)){
            _4574 = SEQ_PTR(_s_8226)->length;
    }
    else {
        _4574 = 1;
    }
    _2 = (int)SEQ_PTR(_s_8226);
    _4575 = (int)*(((s1_ptr)_2)->base + _4574);
    if (binary_op_a(NOTEQ, _4575, 44)){
        _4575 = NOVALUE;
        goto L7; // [107] 123
    }
    _4575 = NOVALUE;

    /** 			s[$] = '}'*/
    if (IS_SEQUENCE(_s_8226)){
            _4577 = SEQ_PTR(_s_8226)->length;
    }
    else {
        _4577 = 1;
    }
    _2 = (int)SEQ_PTR(_s_8226);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _s_8226 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _4577);
    _1 = *(int *)_2;
    *(int *)_2 = 125;
    DeRef(_1);
    goto L8; // [120] 130
L7: 

    /** 			s &= '}'*/
    Append(&_s_8226, _s_8226, 125);
L8: 

    /** 		return s*/
    DeRef(_x_8225);
    DeRef(_4563);
    _4563 = NOVALUE;
    return _s_8226;
L2: 
    ;
}


int _12trim_head(int _source_8254, int _what_8255, int _ret_index_8256)
{
    int _lpos_8257 = NOVALUE;
    int _4588 = NOVALUE;
    int _4587 = NOVALUE;
    int _4584 = NOVALUE;
    int _4583 = NOVALUE;
    int _4581 = NOVALUE;
    int _4579 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_8256)) {
        _1 = (long)(DBL_PTR(_ret_index_8256)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_8256)) && (DBL_PTR(_ret_index_8256)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_8256);
        _ret_index_8256 = _1;
    }

    /** 	if atom(what) then*/
    _4579 = IS_ATOM(_what_8255);
    if (_4579 == 0)
    {
        _4579 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _4579 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_8255;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_8255);
    *((int *)(_2+4)) = _what_8255;
    _what_8255 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_8257 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_8254)){
            _4581 = SEQ_PTR(_source_8254)->length;
    }
    else {
        _4581 = 1;
    }
    if (_lpos_8257 > _4581)
    goto L3; // [37] 73

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_8254);
    _4583 = (int)*(((s1_ptr)_2)->base + _lpos_8257);
    _4584 = find_from(_4583, _what_8255, 1);
    _4583 = NOVALUE;
    if (_4584 != 0)
    goto L4; // [52] 60
    _4584 = NOVALUE;

    /** 			exit*/
    goto L3; // [57] 73
L4: 

    /** 		lpos += 1*/
    _lpos_8257 = _lpos_8257 + 1;

    /** 	end while*/
    goto L2; // [70] 34
L3: 

    /** 	if ret_index then*/
    if (_ret_index_8256 == 0)
    {
        goto L5; // [75] 87
    }
    else{
    }

    /** 		return lpos*/
    DeRefDS(_source_8254);
    DeRef(_what_8255);
    return _lpos_8257;
    goto L6; // [84] 102
L5: 

    /** 		return source[lpos .. $]*/
    if (IS_SEQUENCE(_source_8254)){
            _4587 = SEQ_PTR(_source_8254)->length;
    }
    else {
        _4587 = 1;
    }
    rhs_slice_target = (object_ptr)&_4588;
    RHS_Slice(_source_8254, _lpos_8257, _4587);
    DeRefDS(_source_8254);
    DeRef(_what_8255);
    return _4588;
L6: 
    ;
}


int _12trim_tail(int _source_8275, int _what_8276, int _ret_index_8277)
{
    int _rpos_8278 = NOVALUE;
    int _4597 = NOVALUE;
    int _4594 = NOVALUE;
    int _4593 = NOVALUE;
    int _4589 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_8277)) {
        _1 = (long)(DBL_PTR(_ret_index_8277)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_8277)) && (DBL_PTR(_ret_index_8277)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_8277);
        _ret_index_8277 = _1;
    }

    /** 	if atom(what) then*/
    _4589 = IS_ATOM(_what_8276);
    if (_4589 == 0)
    {
        _4589 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _4589 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_8276;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_8276);
    *((int *)(_2+4)) = _what_8276;
    _what_8276 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_8275)){
            _rpos_8278 = SEQ_PTR(_source_8275)->length;
    }
    else {
        _rpos_8278 = 1;
    }

    /** 	while rpos > 0 do*/
L2: 
    if (_rpos_8278 <= 0)
    goto L3; // [34] 70

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_8275);
    _4593 = (int)*(((s1_ptr)_2)->base + _rpos_8278);
    _4594 = find_from(_4593, _what_8276, 1);
    _4593 = NOVALUE;
    if (_4594 != 0)
    goto L4; // [49] 57
    _4594 = NOVALUE;

    /** 			exit*/
    goto L3; // [54] 70
L4: 

    /** 		rpos -= 1*/
    _rpos_8278 = _rpos_8278 - 1;

    /** 	end while*/
    goto L2; // [67] 34
L3: 

    /** 	if ret_index then*/
    if (_ret_index_8277 == 0)
    {
        goto L5; // [72] 84
    }
    else{
    }

    /** 		return rpos*/
    DeRefDS(_source_8275);
    DeRef(_what_8276);
    return _rpos_8278;
    goto L6; // [81] 96
L5: 

    /** 		return source[1..rpos]*/
    rhs_slice_target = (object_ptr)&_4597;
    RHS_Slice(_source_8275, 1, _rpos_8278);
    DeRefDS(_source_8275);
    DeRef(_what_8276);
    return _4597;
L6: 
    ;
}


int _12trim(int _source_8295, int _what_8296, int _ret_index_8297)
{
    int _rpos_8298 = NOVALUE;
    int _lpos_8299 = NOVALUE;
    int _4618 = NOVALUE;
    int _4616 = NOVALUE;
    int _4614 = NOVALUE;
    int _4612 = NOVALUE;
    int _4609 = NOVALUE;
    int _4608 = NOVALUE;
    int _4603 = NOVALUE;
    int _4602 = NOVALUE;
    int _4600 = NOVALUE;
    int _4598 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_ret_index_8297)) {
        _1 = (long)(DBL_PTR(_ret_index_8297)->dbl);
        if (UNIQUE(DBL_PTR(_ret_index_8297)) && (DBL_PTR(_ret_index_8297)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_index_8297);
        _ret_index_8297 = _1;
    }

    /** 	if atom(what) then*/
    _4598 = IS_ATOM(_what_8296);
    if (_4598 == 0)
    {
        _4598 = NOVALUE;
        goto L1; // [12] 22
    }
    else{
        _4598 = NOVALUE;
    }

    /** 		what = {what}*/
    _0 = _what_8296;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_what_8296);
    *((int *)(_2+4)) = _what_8296;
    _what_8296 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	lpos = 1*/
    _lpos_8299 = 1;

    /** 	while lpos <= length(source) do*/
L2: 
    if (IS_SEQUENCE(_source_8295)){
            _4600 = SEQ_PTR(_source_8295)->length;
    }
    else {
        _4600 = 1;
    }
    if (_lpos_8299 > _4600)
    goto L3; // [37] 73

    /** 		if not find(source[lpos], what) then*/
    _2 = (int)SEQ_PTR(_source_8295);
    _4602 = (int)*(((s1_ptr)_2)->base + _lpos_8299);
    _4603 = find_from(_4602, _what_8296, 1);
    _4602 = NOVALUE;
    if (_4603 != 0)
    goto L4; // [52] 60
    _4603 = NOVALUE;

    /** 			exit*/
    goto L3; // [57] 73
L4: 

    /** 		lpos += 1*/
    _lpos_8299 = _lpos_8299 + 1;

    /** 	end while*/
    goto L2; // [70] 34
L3: 

    /** 	rpos = length(source)*/
    if (IS_SEQUENCE(_source_8295)){
            _rpos_8298 = SEQ_PTR(_source_8295)->length;
    }
    else {
        _rpos_8298 = 1;
    }

    /** 	while rpos > lpos do*/
L5: 
    if (_rpos_8298 <= _lpos_8299)
    goto L6; // [85] 121

    /** 		if not find(source[rpos], what) then*/
    _2 = (int)SEQ_PTR(_source_8295);
    _4608 = (int)*(((s1_ptr)_2)->base + _rpos_8298);
    _4609 = find_from(_4608, _what_8296, 1);
    _4608 = NOVALUE;
    if (_4609 != 0)
    goto L7; // [100] 108
    _4609 = NOVALUE;

    /** 			exit*/
    goto L6; // [105] 121
L7: 

    /** 		rpos -= 1*/
    _rpos_8298 = _rpos_8298 - 1;

    /** 	end while*/
    goto L5; // [118] 85
L6: 

    /** 	if ret_index then*/
    if (_ret_index_8297 == 0)
    {
        goto L8; // [123] 139
    }
    else{
    }

    /** 		return {lpos, rpos}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _lpos_8299;
    ((int *)_2)[2] = _rpos_8298;
    _4612 = MAKE_SEQ(_1);
    DeRefDS(_source_8295);
    DeRef(_what_8296);
    return _4612;
    goto L9; // [136] 190
L8: 

    /** 		if lpos = 1 then*/
    if (_lpos_8299 != 1)
    goto LA; // [141] 162

    /** 			if rpos = length(source) then*/
    if (IS_SEQUENCE(_source_8295)){
            _4614 = SEQ_PTR(_source_8295)->length;
    }
    else {
        _4614 = 1;
    }
    if (_rpos_8298 != _4614)
    goto LB; // [150] 161

    /** 				return source*/
    DeRef(_what_8296);
    DeRef(_4612);
    _4612 = NOVALUE;
    return _source_8295;
LB: 
LA: 

    /** 		if lpos > length(source) then*/
    if (IS_SEQUENCE(_source_8295)){
            _4616 = SEQ_PTR(_source_8295)->length;
    }
    else {
        _4616 = 1;
    }
    if (_lpos_8299 <= _4616)
    goto LC; // [167] 178

    /** 			return {}*/
    RefDS(_5);
    DeRefDS(_source_8295);
    DeRef(_what_8296);
    DeRef(_4612);
    _4612 = NOVALUE;
    return _5;
LC: 

    /** 		return source[lpos..rpos]*/
    rhs_slice_target = (object_ptr)&_4618;
    RHS_Slice(_source_8295, _lpos_8299, _rpos_8298);
    DeRefDS(_source_8295);
    DeRef(_what_8296);
    DeRef(_4612);
    _4612 = NOVALUE;
    return _4618;
L9: 
    ;
}


int _12load_code_page(int _cpname_8337)
{
    int _cpdata_8338 = NOVALUE;
    int _pos_8339 = NOVALUE;
    int _kv_8340 = NOVALUE;
    int _cp_source_8341 = NOVALUE;
    int _cp_db_8342 = NOVALUE;
    int _fh_8415 = NOVALUE;
    int _idx_8419 = NOVALUE;
    int _vers_8420 = NOVALUE;
    int _4694 = NOVALUE;
    int _4693 = NOVALUE;
    int _4690 = NOVALUE;
    int _4684 = NOVALUE;
    int _4682 = NOVALUE;
    int _4681 = NOVALUE;
    int _4679 = NOVALUE;
    int _4672 = NOVALUE;
    int _4671 = NOVALUE;
    int _4670 = NOVALUE;
    int _4668 = NOVALUE;
    int _4667 = NOVALUE;
    int _4666 = NOVALUE;
    int _4664 = NOVALUE;
    int _4662 = NOVALUE;
    int _4661 = NOVALUE;
    int _4659 = NOVALUE;
    int _4658 = NOVALUE;
    int _4656 = NOVALUE;
    int _4655 = NOVALUE;
    int _4654 = NOVALUE;
    int _4653 = NOVALUE;
    int _4650 = NOVALUE;
    int _4648 = NOVALUE;
    int _4646 = NOVALUE;
    int _4645 = NOVALUE;
    int _4643 = NOVALUE;
    int _4642 = NOVALUE;
    int _4641 = NOVALUE;
    int _4639 = NOVALUE;
    int _4638 = NOVALUE;
    int _4637 = NOVALUE;
    int _4634 = NOVALUE;
    int _4633 = NOVALUE;
    int _4632 = NOVALUE;
    int _4631 = NOVALUE;
    int _4629 = NOVALUE;
    int _4628 = NOVALUE;
    int _4625 = NOVALUE;
    int _4624 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cp_source = filesys:defaultext(cpname, ".ecp")*/
    RefDS(_cpname_8337);
    RefDS(_4620);
    _0 = _cp_source_8341;
    _cp_source_8341 = _15defaultext(_cpname_8337, _4620);
    DeRef(_0);

    /** 	cp_source = filesys:locate_file(cp_source)*/
    RefDS(_cp_source_8341);
    RefDS(_5);
    RefDS(_5);
    _0 = _cp_source_8341;
    _cp_source_8341 = _15locate_file(_cp_source_8341, _5, _5);
    DeRefDS(_0);

    /** 	cpdata = io:read_lines(cp_source)*/
    RefDS(_cp_source_8341);
    _0 = _cpdata_8338;
    _cpdata_8338 = _6read_lines(_cp_source_8341);
    DeRef(_0);

    /** 	if sequence(cpdata) then*/
    _4624 = IS_SEQUENCE(_cpdata_8338);
    if (_4624 == 0)
    {
        _4624 = NOVALUE;
        goto L1; // [33] 379
    }
    else{
        _4624 = NOVALUE;
    }

    /** 		pos = 0*/
    _pos_8339 = 0;

    /** 		while pos < length(cpdata) do*/
L2: 
    if (IS_SEQUENCE(_cpdata_8338)){
            _4625 = SEQ_PTR(_cpdata_8338)->length;
    }
    else {
        _4625 = 1;
    }
    if (_pos_8339 >= _4625)
    goto L3; // [51] 192

    /** 			pos += 1*/
    _pos_8339 = _pos_8339 + 1;

    /** 			cpdata[pos]  = trim(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4628 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    Ref(_4628);
    RefDS(_3967);
    _4629 = _12trim(_4628, _3967, 0);
    _4628 = NOVALUE;
    _2 = (int)SEQ_PTR(_cpdata_8338);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cpdata_8338 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pos_8339);
    _1 = *(int *)_2;
    *(int *)_2 = _4629;
    if( _1 != _4629 ){
        DeRef(_1);
    }
    _4629 = NOVALUE;

    /** 			if search:begins("--HEAD--", cpdata[pos]) then*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4631 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    RefDS(_4630);
    Ref(_4631);
    _4632 = _14begins(_4630, _4631);
    _4631 = NOVALUE;
    if (_4632 == 0) {
        DeRef(_4632);
        _4632 = NOVALUE;
        goto L4; // [90] 98
    }
    else {
        if (!IS_ATOM_INT(_4632) && DBL_PTR(_4632)->dbl == 0.0){
            DeRef(_4632);
            _4632 = NOVALUE;
            goto L4; // [90] 98
        }
        DeRef(_4632);
        _4632 = NOVALUE;
    }
    DeRef(_4632);
    _4632 = NOVALUE;

    /** 				continue*/
    goto L2; // [95] 48
L4: 

    /** 			if cpdata[pos][1] = ';' then*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4633 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    _2 = (int)SEQ_PTR(_4633);
    _4634 = (int)*(((s1_ptr)_2)->base + 1);
    _4633 = NOVALUE;
    if (binary_op_a(NOTEQ, _4634, 59)){
        _4634 = NOVALUE;
        goto L5; // [108] 117
    }
    _4634 = NOVALUE;

    /** 				continue	-- A comment line*/
    goto L2; // [114] 48
L5: 

    /** 			if search:begins("--CASE--", cpdata[pos]) then*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4637 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    RefDS(_4636);
    Ref(_4637);
    _4638 = _14begins(_4636, _4637);
    _4637 = NOVALUE;
    if (_4638 == 0) {
        DeRef(_4638);
        _4638 = NOVALUE;
        goto L6; // [128] 136
    }
    else {
        if (!IS_ATOM_INT(_4638) && DBL_PTR(_4638)->dbl == 0.0){
            DeRef(_4638);
            _4638 = NOVALUE;
            goto L6; // [128] 136
        }
        DeRef(_4638);
        _4638 = NOVALUE;
    }
    DeRef(_4638);
    _4638 = NOVALUE;

    /** 				exit*/
    goto L3; // [133] 192
L6: 

    /** 			kv = keyvalues(cpdata[pos],,,,"")*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4639 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    Ref(_4639);
    RefDS(_4790);
    RefDS(_4791);
    RefDS(_4792);
    RefDS(_5);
    _0 = _kv_8340;
    _kv_8340 = _12keyvalues(_4639, _4790, _4791, _4792, _5, 1);
    DeRef(_0);
    _4639 = NOVALUE;

    /** 			if equal(lower(kv[1][1]), "title") then*/
    _2 = (int)SEQ_PTR(_kv_8340);
    _4641 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4641);
    _4642 = (int)*(((s1_ptr)_2)->base + 1);
    _4641 = NOVALUE;
    Ref(_4642);
    _4643 = _12lower(_4642);
    _4642 = NOVALUE;
    if (_4643 == _4644)
    _4645 = 1;
    else if (IS_ATOM_INT(_4643) && IS_ATOM_INT(_4644))
    _4645 = 0;
    else
    _4645 = (compare(_4643, _4644) == 0);
    DeRef(_4643);
    _4643 = NOVALUE;
    if (_4645 == 0)
    {
        _4645 = NOVALUE;
        goto L2; // [171] 48
    }
    else{
        _4645 = NOVALUE;
    }

    /** 				encoding_NAME = kv[1][2]*/
    _2 = (int)SEQ_PTR(_kv_8340);
    _4646 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_12encoding_NAME_8333);
    _2 = (int)SEQ_PTR(_4646);
    _12encoding_NAME_8333 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_12encoding_NAME_8333);
    _4646 = NOVALUE;

    /** 		end while*/
    goto L2; // [189] 48
L3: 

    /** 		if pos > length(cpdata) then*/
    if (IS_SEQUENCE(_cpdata_8338)){
            _4648 = SEQ_PTR(_cpdata_8338)->length;
    }
    else {
        _4648 = 1;
    }
    if (_pos_8339 <= _4648)
    goto L7; // [197] 208

    /** 			return -2 -- No Case Conversion table found.*/
    DeRefDS(_cpname_8337);
    DeRef(_cpdata_8338);
    DeRef(_kv_8340);
    DeRef(_cp_source_8341);
    DeRef(_cp_db_8342);
    return -2;
L7: 

    /** 		upper_case_SET = ""*/
    RefDS(_5);
    DeRef(_12upper_case_SET_8332);
    _12upper_case_SET_8332 = _5;

    /** 		lower_case_SET = ""*/
    RefDS(_5);
    DeRef(_12lower_case_SET_8331);
    _12lower_case_SET_8331 = _5;

    /** 		while pos < length(cpdata) do*/
L8: 
    if (IS_SEQUENCE(_cpdata_8338)){
            _4650 = SEQ_PTR(_cpdata_8338)->length;
    }
    else {
        _4650 = 1;
    }
    if (_pos_8339 >= _4650)
    goto L9; // [230] 578

    /** 			pos += 1*/
    _pos_8339 = _pos_8339 + 1;

    /** 			cpdata[pos]  = trim(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4653 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    Ref(_4653);
    RefDS(_3967);
    _4654 = _12trim(_4653, _3967, 0);
    _4653 = NOVALUE;
    _2 = (int)SEQ_PTR(_cpdata_8338);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _cpdata_8338 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pos_8339);
    _1 = *(int *)_2;
    *(int *)_2 = _4654;
    if( _1 != _4654 ){
        DeRef(_1);
    }
    _4654 = NOVALUE;

    /** 			if length(cpdata[pos]) < 3 then*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4655 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    if (IS_SEQUENCE(_4655)){
            _4656 = SEQ_PTR(_4655)->length;
    }
    else {
        _4656 = 1;
    }
    _4655 = NOVALUE;
    if (_4656 >= 3)
    goto LA; // [267] 276

    /** 				continue*/
    goto L8; // [273] 227
LA: 

    /** 			if cpdata[pos][1] = ';' then*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4658 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    _2 = (int)SEQ_PTR(_4658);
    _4659 = (int)*(((s1_ptr)_2)->base + 1);
    _4658 = NOVALUE;
    if (binary_op_a(NOTEQ, _4659, 59)){
        _4659 = NOVALUE;
        goto LB; // [286] 295
    }
    _4659 = NOVALUE;

    /** 				continue	-- A comment line*/
    goto L8; // [292] 227
LB: 

    /** 			if cpdata[pos][1] = '-' then*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4661 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    _2 = (int)SEQ_PTR(_4661);
    _4662 = (int)*(((s1_ptr)_2)->base + 1);
    _4661 = NOVALUE;
    if (binary_op_a(NOTEQ, _4662, 45)){
        _4662 = NOVALUE;
        goto LC; // [305] 314
    }
    _4662 = NOVALUE;

    /** 				exit*/
    goto L9; // [311] 578
LC: 

    /** 			kv = keyvalues(cpdata[pos])*/
    _2 = (int)SEQ_PTR(_cpdata_8338);
    _4664 = (int)*(((s1_ptr)_2)->base + _pos_8339);
    Ref(_4664);
    RefDS(_4790);
    RefDS(_4791);
    RefDS(_4792);
    RefDS(_149);
    _0 = _kv_8340;
    _kv_8340 = _12keyvalues(_4664, _4790, _4791, _4792, _149, 1);
    DeRef(_0);
    _4664 = NOVALUE;

    /** 			upper_case_SET &= convert:hex_text(kv[1][1])*/
    _2 = (int)SEQ_PTR(_kv_8340);
    _4666 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4666);
    _4667 = (int)*(((s1_ptr)_2)->base + 1);
    _4666 = NOVALUE;
    Ref(_4667);
    _4668 = _13hex_text(_4667);
    _4667 = NOVALUE;
    if (IS_SEQUENCE(_12upper_case_SET_8332) && IS_ATOM(_4668)) {
        Ref(_4668);
        Append(&_12upper_case_SET_8332, _12upper_case_SET_8332, _4668);
    }
    else if (IS_ATOM(_12upper_case_SET_8332) && IS_SEQUENCE(_4668)) {
    }
    else {
        Concat((object_ptr)&_12upper_case_SET_8332, _12upper_case_SET_8332, _4668);
    }
    DeRef(_4668);
    _4668 = NOVALUE;

    /** 			lower_case_SET &= convert:hex_text(kv[1][2])*/
    _2 = (int)SEQ_PTR(_kv_8340);
    _4670 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_4670);
    _4671 = (int)*(((s1_ptr)_2)->base + 2);
    _4670 = NOVALUE;
    Ref(_4671);
    _4672 = _13hex_text(_4671);
    _4671 = NOVALUE;
    if (IS_SEQUENCE(_12lower_case_SET_8331) && IS_ATOM(_4672)) {
        Ref(_4672);
        Append(&_12lower_case_SET_8331, _12lower_case_SET_8331, _4672);
    }
    else if (IS_ATOM(_12lower_case_SET_8331) && IS_SEQUENCE(_4672)) {
    }
    else {
        Concat((object_ptr)&_12lower_case_SET_8331, _12lower_case_SET_8331, _4672);
    }
    DeRef(_4672);
    _4672 = NOVALUE;

    /** 		end while*/
    goto L8; // [373] 227
    goto L9; // [376] 578
L1: 

    /** 		cp_db = filesys:locate_file("ecp.dat")*/
    RefDS(_4674);
    RefDS(_5);
    RefDS(_5);
    _0 = _cp_db_8342;
    _cp_db_8342 = _15locate_file(_4674, _5, _5);
    DeRef(_0);

    /** 		integer fh = open(cp_db, "rb")*/
    _fh_8415 = EOpen(_cp_db_8342, _3811, 0);

    /** 		if fh = -1 then*/
    if (_fh_8415 != -1)
    goto LD; // [400] 411

    /** 			return -2 -- Couldn't open DB*/
    DeRef(_idx_8419);
    DeRef(_vers_8420);
    DeRefDS(_cpname_8337);
    DeRef(_cpdata_8338);
    DeRef(_kv_8340);
    DeRef(_cp_source_8341);
    DeRefDS(_cp_db_8342);
    _4655 = NOVALUE;
    return -2;
LD: 

    /** 		object idx*/

    /** 		object vers*/

    /** 		vers = serialize:deserialize(fh)  -- get the database version*/
    _0 = _vers_8420;
    _vers_8420 = _25deserialize(_fh_8415, 1);
    DeRef(_0);

    /** 		if atom(vers) or length(vers) = 0 then*/
    _4679 = IS_ATOM(_vers_8420);
    if (_4679 != 0) {
        goto LE; // [427] 443
    }
    if (IS_SEQUENCE(_vers_8420)){
            _4681 = SEQ_PTR(_vers_8420)->length;
    }
    else {
        _4681 = 1;
    }
    _4682 = (_4681 == 0);
    _4681 = NOVALUE;
    if (_4682 == 0)
    {
        DeRef(_4682);
        _4682 = NOVALUE;
        goto LF; // [439] 450
    }
    else{
        DeRef(_4682);
        _4682 = NOVALUE;
    }
LE: 

    /** 			return -3 -- DB is wrong or corrupted.*/
    DeRef(_idx_8419);
    DeRef(_vers_8420);
    DeRefDS(_cpname_8337);
    DeRef(_cpdata_8338);
    DeRef(_kv_8340);
    DeRef(_cp_source_8341);
    DeRef(_cp_db_8342);
    _4655 = NOVALUE;
    return -3;
LF: 

    /** 		switch vers[1] do*/
    _2 = (int)SEQ_PTR(_vers_8420);
    _4684 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_4684) ){
        goto L10; // [456] 562
    }
    if(!IS_ATOM_INT(_4684)){
        if( (DBL_PTR(_4684)->dbl != (double) ((int) DBL_PTR(_4684)->dbl) ) ){
            goto L10; // [456] 562
        }
        _0 = (int) DBL_PTR(_4684)->dbl;
    }
    else {
        _0 = _4684;
    };
    _4684 = NOVALUE;
    switch ( _0 ){ 

        /** 			case 1, 2 then*/
        case 1:
        case 2:

        /** 				idx = serialize:deserialize(fh)  -- get Code Page index offset*/
        _0 = _idx_8419;
        _idx_8419 = _25deserialize(_fh_8415, 1);
        DeRef(_0);

        /** 				pos = io:seek(fh, idx)*/
        Ref(_idx_8419);
        _pos_8339 = _6seek(_fh_8415, _idx_8419);
        if (!IS_ATOM_INT(_pos_8339)) {
            _1 = (long)(DBL_PTR(_pos_8339)->dbl);
            if (UNIQUE(DBL_PTR(_pos_8339)) && (DBL_PTR(_pos_8339)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_8339);
            _pos_8339 = _1;
        }

        /** 				idx = serialize:deserialize(fh)	-- get the Code Page Index*/
        _0 = _idx_8419;
        _idx_8419 = _25deserialize(_fh_8415, 1);
        DeRef(_0);

        /** 				pos = find(cpname, idx[1])*/
        _2 = (int)SEQ_PTR(_idx_8419);
        _4690 = (int)*(((s1_ptr)_2)->base + 1);
        _pos_8339 = find_from(_cpname_8337, _4690, 1);
        _4690 = NOVALUE;

        /** 				if pos != 0 then*/
        if (_pos_8339 == 0)
        goto L11; // [507] 571

        /** 					pos = io:seek(fh, idx[2][pos])*/
        _2 = (int)SEQ_PTR(_idx_8419);
        _4693 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_4693);
        _4694 = (int)*(((s1_ptr)_2)->base + _pos_8339);
        _4693 = NOVALUE;
        Ref(_4694);
        _pos_8339 = _6seek(_fh_8415, _4694);
        _4694 = NOVALUE;
        if (!IS_ATOM_INT(_pos_8339)) {
            _1 = (long)(DBL_PTR(_pos_8339)->dbl);
            if (UNIQUE(DBL_PTR(_pos_8339)) && (DBL_PTR(_pos_8339)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_8339);
            _pos_8339 = _1;
        }

        /** 					upper_case_SET = serialize:deserialize(fh) -- "uppercase"*/
        _0 = _25deserialize(_fh_8415, 1);
        DeRef(_12upper_case_SET_8332);
        _12upper_case_SET_8332 = _0;

        /** 					lower_case_SET = serialize:deserialize(fh) -- "lowercase"*/
        _0 = _25deserialize(_fh_8415, 1);
        DeRef(_12lower_case_SET_8331);
        _12lower_case_SET_8331 = _0;

        /** 					encoding_NAME = serialize:deserialize(fh) -- "title"*/
        _0 = _25deserialize(_fh_8415, 1);
        DeRef(_12encoding_NAME_8333);
        _12encoding_NAME_8333 = _0;
        goto L11; // [558] 571

        /** 			case else*/
        default:
L10: 

        /** 				return -4 -- Unhandled ecp database version.*/
        DeRef(_idx_8419);
        DeRef(_vers_8420);
        DeRefDS(_cpname_8337);
        DeRef(_cpdata_8338);
        DeRef(_kv_8340);
        DeRef(_cp_source_8341);
        DeRef(_cp_db_8342);
        _4655 = NOVALUE;
        return -4;
    ;}L11: 

    /** 		close(fh)*/
    EClose(_fh_8415);
    DeRef(_idx_8419);
    _idx_8419 = NOVALUE;
    DeRef(_vers_8420);
    _vers_8420 = NOVALUE;
L9: 

    /** 	return 0*/
    DeRefDS(_cpname_8337);
    DeRef(_cpdata_8338);
    DeRef(_kv_8340);
    DeRef(_cp_source_8341);
    DeRef(_cp_db_8342);
    _4655 = NOVALUE;
    return 0;
    ;
}


void _12set_encoding_properties(int _en_8451, int _lc_8452, int _uc_8453)
{
    int _res_8454 = NOVALUE;
    int _4719 = NOVALUE;
    int _4718 = NOVALUE;
    int _4717 = NOVALUE;
    int _4716 = NOVALUE;
    int _4715 = NOVALUE;
    int _4713 = NOVALUE;
    int _4712 = NOVALUE;
    int _4711 = NOVALUE;
    int _4707 = NOVALUE;
    int _4706 = NOVALUE;
    int _4705 = NOVALUE;
    int _4704 = NOVALUE;
    int _4703 = NOVALUE;
    int _4702 = NOVALUE;
    int _4701 = NOVALUE;
    int _4700 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(en) > 0 and length(lc) = 0 and length(uc) = 0 then*/
    if (IS_SEQUENCE(_en_8451)){
            _4700 = SEQ_PTR(_en_8451)->length;
    }
    else {
        _4700 = 1;
    }
    _4701 = (_4700 > 0);
    _4700 = NOVALUE;
    if (_4701 == 0) {
        _4702 = 0;
        goto L1; // [16] 31
    }
    if (IS_SEQUENCE(_lc_8452)){
            _4703 = SEQ_PTR(_lc_8452)->length;
    }
    else {
        _4703 = 1;
    }
    _4704 = (_4703 == 0);
    _4703 = NOVALUE;
    _4702 = (_4704 != 0);
L1: 
    if (_4702 == 0) {
        goto L2; // [31] 79
    }
    if (IS_SEQUENCE(_uc_8453)){
            _4706 = SEQ_PTR(_uc_8453)->length;
    }
    else {
        _4706 = 1;
    }
    _4707 = (_4706 == 0);
    _4706 = NOVALUE;
    if (_4707 == 0)
    {
        DeRef(_4707);
        _4707 = NOVALUE;
        goto L2; // [43] 79
    }
    else{
        DeRef(_4707);
        _4707 = NOVALUE;
    }

    /** 		res = load_code_page(en)*/
    RefDS(_en_8451);
    _res_8454 = _12load_code_page(_en_8451);
    if (!IS_ATOM_INT(_res_8454)) {
        _1 = (long)(DBL_PTR(_res_8454)->dbl);
        if (UNIQUE(DBL_PTR(_res_8454)) && (DBL_PTR(_res_8454)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_res_8454);
        _res_8454 = _1;
    }

    /** 		if res != 0 then*/
    if (_res_8454 == 0)
    goto L3; // [58] 73

    /** 			printf(2, "Failed to load code page '%s'. Error # %d\n", {en, res})*/
    RefDS(_en_8451);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _en_8451;
    ((int *)_2)[2] = _res_8454;
    _4711 = MAKE_SEQ(_1);
    EPrintf(2, _4710, _4711);
    DeRefDS(_4711);
    _4711 = NOVALUE;
L3: 

    /** 		return*/
    DeRefDS(_en_8451);
    DeRefDS(_lc_8452);
    DeRefDS(_uc_8453);
    DeRef(_4701);
    _4701 = NOVALUE;
    DeRef(_4704);
    _4704 = NOVALUE;
    return;
L2: 

    /** 	if length(lc) = length(uc) then*/
    if (IS_SEQUENCE(_lc_8452)){
            _4712 = SEQ_PTR(_lc_8452)->length;
    }
    else {
        _4712 = 1;
    }
    if (IS_SEQUENCE(_uc_8453)){
            _4713 = SEQ_PTR(_uc_8453)->length;
    }
    else {
        _4713 = 1;
    }
    if (_4712 != _4713)
    goto L4; // [87] 145

    /** 		if length(lc) = 0 and length(en) = 0 then*/
    if (IS_SEQUENCE(_lc_8452)){
            _4715 = SEQ_PTR(_lc_8452)->length;
    }
    else {
        _4715 = 1;
    }
    _4716 = (_4715 == 0);
    _4715 = NOVALUE;
    if (_4716 == 0) {
        goto L5; // [100] 123
    }
    if (IS_SEQUENCE(_en_8451)){
            _4718 = SEQ_PTR(_en_8451)->length;
    }
    else {
        _4718 = 1;
    }
    _4719 = (_4718 == 0);
    _4718 = NOVALUE;
    if (_4719 == 0)
    {
        DeRef(_4719);
        _4719 = NOVALUE;
        goto L5; // [112] 123
    }
    else{
        DeRef(_4719);
        _4719 = NOVALUE;
    }

    /** 			en = "ASCII"*/
    RefDS(_4619);
    DeRefDS(_en_8451);
    _en_8451 = _4619;
L5: 

    /** 		lower_case_SET = lc*/
    RefDS(_lc_8452);
    DeRef(_12lower_case_SET_8331);
    _12lower_case_SET_8331 = _lc_8452;

    /** 		upper_case_SET = uc*/
    RefDS(_uc_8453);
    DeRef(_12upper_case_SET_8332);
    _12upper_case_SET_8332 = _uc_8453;

    /** 		encoding_NAME = en*/
    RefDS(_en_8451);
    DeRef(_12encoding_NAME_8333);
    _12encoding_NAME_8333 = _en_8451;
L4: 

    /** end procedure*/
    DeRefDS(_en_8451);
    DeRefDS(_lc_8452);
    DeRefDS(_uc_8453);
    DeRef(_4701);
    _4701 = NOVALUE;
    DeRef(_4704);
    _4704 = NOVALUE;
    DeRef(_4716);
    _4716 = NOVALUE;
    return;
    ;
}


int _12get_encoding_properties()
{
    int _4720 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {encoding_NAME, lower_case_SET, upper_case_SET}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_12encoding_NAME_8333);
    *((int *)(_2+4)) = _12encoding_NAME_8333;
    RefDS(_12lower_case_SET_8331);
    *((int *)(_2+8)) = _12lower_case_SET_8331;
    RefDS(_12upper_case_SET_8332);
    *((int *)(_2+12)) = _12upper_case_SET_8332;
    _4720 = MAKE_SEQ(_1);
    return _4720;
    ;
}


int _12change_case(int _x_8508, int _api_8509)
{
    int _changed_text_8510 = NOVALUE;
    int _single_char_8511 = NOVALUE;
    int _len_8512 = NOVALUE;
    int _4751 = NOVALUE;
    int _4749 = NOVALUE;
    int _4747 = NOVALUE;
    int _4746 = NOVALUE;
    int _4743 = NOVALUE;
    int _4741 = NOVALUE;
    int _4739 = NOVALUE;
    int _4738 = NOVALUE;
    int _4737 = NOVALUE;
    int _4736 = NOVALUE;
    int _4735 = NOVALUE;
    int _4732 = NOVALUE;
    int _4730 = NOVALUE;
    int _0, _1, _2;
    

    /** 		integer single_char = 0*/
    _single_char_8511 = 0;

    /** 		if not string(x) then*/
    Ref(_x_8508);
    _4730 = _11string(_x_8508);
    if (IS_ATOM_INT(_4730)) {
        if (_4730 != 0){
            DeRef(_4730);
            _4730 = NOVALUE;
            goto L1; // [14] 99
        }
    }
    else {
        if (DBL_PTR(_4730)->dbl != 0.0){
            DeRef(_4730);
            _4730 = NOVALUE;
            goto L1; // [14] 99
        }
    }
    DeRef(_4730);
    _4730 = NOVALUE;

    /** 			if atom(x) then*/
    _4732 = IS_ATOM(_x_8508);
    if (_4732 == 0)
    {
        _4732 = NOVALUE;
        goto L2; // [22] 54
    }
    else{
        _4732 = NOVALUE;
    }

    /** 				if x = 0 then*/
    if (binary_op_a(NOTEQ, _x_8508, 0)){
        goto L3; // [27] 38
    }

    /** 					return 0*/
    DeRef(_x_8508);
    DeRef(_api_8509);
    DeRefi(_changed_text_8510);
    return 0;
L3: 

    /** 				x = {x}*/
    _0 = _x_8508;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_x_8508);
    *((int *)(_2+4)) = _x_8508;
    _x_8508 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 				single_char = 1*/
    _single_char_8511 = 1;
    goto L4; // [51] 98
L2: 

    /** 				for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_8508)){
            _4735 = SEQ_PTR(_x_8508)->length;
    }
    else {
        _4735 = 1;
    }
    {
        int _i_8524;
        _i_8524 = 1;
L5: 
        if (_i_8524 > _4735){
            goto L6; // [59] 91
        }

        /** 					x[i] = change_case(x[i], api)*/
        _2 = (int)SEQ_PTR(_x_8508);
        _4736 = (int)*(((s1_ptr)_2)->base + _i_8524);
        Ref(_api_8509);
        DeRef(_4737);
        _4737 = _api_8509;
        Ref(_4736);
        _4738 = _12change_case(_4736, _4737);
        _4736 = NOVALUE;
        _4737 = NOVALUE;
        _2 = (int)SEQ_PTR(_x_8508);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _x_8508 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8524);
        _1 = *(int *)_2;
        *(int *)_2 = _4738;
        if( _1 != _4738 ){
            DeRef(_1);
        }
        _4738 = NOVALUE;

        /** 				end for*/
        _i_8524 = _i_8524 + 1;
        goto L5; // [86] 66
L6: 
        ;
    }

    /** 				return x*/
    DeRef(_api_8509);
    DeRefi(_changed_text_8510);
    return _x_8508;
L4: 
L1: 

    /** 		if length(x) = 0 then*/
    if (IS_SEQUENCE(_x_8508)){
            _4739 = SEQ_PTR(_x_8508)->length;
    }
    else {
        _4739 = 1;
    }
    if (_4739 != 0)
    goto L7; // [104] 115

    /** 			return x*/
    DeRef(_api_8509);
    DeRefi(_changed_text_8510);
    return _x_8508;
L7: 

    /** 		if length(x) >= tm_size then*/
    if (IS_SEQUENCE(_x_8508)){
            _4741 = SEQ_PTR(_x_8508)->length;
    }
    else {
        _4741 = 1;
    }
    if (_4741 < _12tm_size_8502)
    goto L8; // [122] 152

    /** 			tm_size = length(x) + 1*/
    if (IS_SEQUENCE(_x_8508)){
            _4743 = SEQ_PTR(_x_8508)->length;
    }
    else {
        _4743 = 1;
    }
    _12tm_size_8502 = _4743 + 1;
    _4743 = NOVALUE;

    /** 			free(temp_mem)*/
    Ref(_12temp_mem_8503);
    _7free(_12temp_mem_8503);

    /** 			temp_mem = allocate(tm_size)*/
    _0 = _7allocate(_12tm_size_8502, 0);
    DeRef(_12temp_mem_8503);
    _12temp_mem_8503 = _0;
L8: 

    /** 		poke(temp_mem, x)*/
    if (IS_ATOM_INT(_12temp_mem_8503)){
        poke_addr = (unsigned char *)_12temp_mem_8503;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_12temp_mem_8503)->dbl);
    }
    if (IS_ATOM_INT(_x_8508)) {
        *poke_addr = (unsigned char)_x_8508;
    }
    else if (IS_ATOM(_x_8508)) {
        _1 = (signed char)DBL_PTR(_x_8508)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_x_8508);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }

    /** 		len = c_func(api, {temp_mem, length(x)} )*/
    if (IS_SEQUENCE(_x_8508)){
            _4746 = SEQ_PTR(_x_8508)->length;
    }
    else {
        _4746 = 1;
    }
    Ref(_12temp_mem_8503);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _12temp_mem_8503;
    ((int *)_2)[2] = _4746;
    _4747 = MAKE_SEQ(_1);
    _4746 = NOVALUE;
    _len_8512 = call_c(1, _api_8509, _4747);
    DeRefDS(_4747);
    _4747 = NOVALUE;
    if (!IS_ATOM_INT(_len_8512)) {
        _1 = (long)(DBL_PTR(_len_8512)->dbl);
        if (UNIQUE(DBL_PTR(_len_8512)) && (DBL_PTR(_len_8512)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_8512);
        _len_8512 = _1;
    }

    /** 		changed_text = peek({temp_mem, len})*/
    Ref(_12temp_mem_8503);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _12temp_mem_8503;
    ((int *)_2)[2] = _len_8512;
    _4749 = MAKE_SEQ(_1);
    DeRefi(_changed_text_8510);
    _1 = (int)SEQ_PTR(_4749);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _changed_text_8510 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_4749);
    _4749 = NOVALUE;

    /** 		if single_char then*/
    if (_single_char_8511 == 0)
    {
        goto L9; // [194] 210
    }
    else{
    }

    /** 			return changed_text[1]*/
    _2 = (int)SEQ_PTR(_changed_text_8510);
    _4751 = (int)*(((s1_ptr)_2)->base + 1);
    DeRef(_x_8508);
    DeRef(_api_8509);
    DeRefDSi(_changed_text_8510);
    return _4751;
    goto LA; // [207] 217
L9: 

    /** 			return changed_text*/
    DeRef(_x_8508);
    DeRef(_api_8509);
    _4751 = NOVALUE;
    return _changed_text_8510;
LA: 
    ;
}


int _12lower(int _x_8550)
{
    int _4755 = NOVALUE;
    int _4754 = NOVALUE;
    int _4752 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(lower_case_SET) != 0 then*/
    if (IS_SEQUENCE(_12lower_case_SET_8331)){
            _4752 = SEQ_PTR(_12lower_case_SET_8331)->length;
    }
    else {
        _4752 = 1;
    }
    if (_4752 == 0)
    goto L1; // [8] 30

    /** 		return stdseq:mapping(x, upper_case_SET, lower_case_SET)*/
    Ref(_x_8550);
    RefDS(_12upper_case_SET_8332);
    RefDS(_12lower_case_SET_8331);
    _4754 = _21mapping(_x_8550, _12upper_case_SET_8332, _12lower_case_SET_8331, 0);
    DeRef(_x_8550);
    return _4754;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharLowerBuff)*/
    Ref(_x_8550);
    Ref(_12api_CharLowerBuff_8486);
    _4755 = _12change_case(_x_8550, _12api_CharLowerBuff_8486);
    DeRef(_x_8550);
    DeRef(_4754);
    _4754 = NOVALUE;
    return _4755;
    ;
}


int _12upper(int _x_8558)
{
    int _4759 = NOVALUE;
    int _4758 = NOVALUE;
    int _4756 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(upper_case_SET) != 0 then*/
    if (IS_SEQUENCE(_12upper_case_SET_8332)){
            _4756 = SEQ_PTR(_12upper_case_SET_8332)->length;
    }
    else {
        _4756 = 1;
    }
    if (_4756 == 0)
    goto L1; // [8] 30

    /** 		return stdseq:mapping(x, lower_case_SET, upper_case_SET)*/
    Ref(_x_8558);
    RefDS(_12lower_case_SET_8331);
    RefDS(_12upper_case_SET_8332);
    _4758 = _21mapping(_x_8558, _12lower_case_SET_8331, _12upper_case_SET_8332, 0);
    DeRef(_x_8558);
    return _4758;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		return change_case(x, api_CharUpperBuff)*/
    Ref(_x_8558);
    Ref(_12api_CharUpperBuff_8494);
    _4759 = _12change_case(_x_8558, _12api_CharUpperBuff_8494);
    DeRef(_x_8558);
    DeRef(_4758);
    _4758 = NOVALUE;
    return _4759;
    ;
}


int _12proper(int _x_8566)
{
    int _pos_8567 = NOVALUE;
    int _inword_8568 = NOVALUE;
    int _convert_8569 = NOVALUE;
    int _res_8570 = NOVALUE;
    int _4789 = NOVALUE;
    int _4788 = NOVALUE;
    int _4787 = NOVALUE;
    int _4786 = NOVALUE;
    int _4785 = NOVALUE;
    int _4784 = NOVALUE;
    int _4783 = NOVALUE;
    int _4782 = NOVALUE;
    int _4781 = NOVALUE;
    int _4780 = NOVALUE;
    int _4778 = NOVALUE;
    int _4777 = NOVALUE;
    int _4772 = NOVALUE;
    int _4769 = NOVALUE;
    int _4766 = NOVALUE;
    int _4763 = NOVALUE;
    int _4762 = NOVALUE;
    int _4761 = NOVALUE;
    int _4760 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inword = 0	-- Initially not in a word*/
    _inword_8568 = 0;

    /** 	convert = 1	-- Initially convert text*/
    _convert_8569 = 1;

    /** 	res = x		-- Work on a copy of the original, in case we need to restore.*/
    RefDS(_x_8566);
    DeRef(_res_8570);
    _res_8570 = _x_8566;

    /** 	for i = 1 to length(res) do*/
    if (IS_SEQUENCE(_res_8570)){
            _4760 = SEQ_PTR(_res_8570)->length;
    }
    else {
        _4760 = 1;
    }
    {
        int _i_8572;
        _i_8572 = 1;
L1: 
        if (_i_8572 > _4760){
            goto L2; // [29] 320
        }

        /** 		if integer(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4761 = (int)*(((s1_ptr)_2)->base + _i_8572);
        if (IS_ATOM_INT(_4761))
        _4762 = 1;
        else if (IS_ATOM_DBL(_4761))
        _4762 = IS_ATOM_INT(DoubleToInt(_4761));
        else
        _4762 = 0;
        _4761 = NOVALUE;
        if (_4762 == 0)
        {
            _4762 = NOVALUE;
            goto L3; // [45] 229
        }
        else{
            _4762 = NOVALUE;
        }

        /** 			if convert then*/
        if (_convert_8569 == 0)
        {
            goto L4; // [50] 313
        }
        else{
        }

        /** 				pos = types:t_upper(res[i])*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4763 = (int)*(((s1_ptr)_2)->base + _i_8572);
        Ref(_4763);
        _pos_8567 = _11t_upper(_4763);
        _4763 = NOVALUE;
        if (!IS_ATOM_INT(_pos_8567)) {
            _1 = (long)(DBL_PTR(_pos_8567)->dbl);
            if (UNIQUE(DBL_PTR(_pos_8567)) && (DBL_PTR(_pos_8567)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_8567);
            _pos_8567 = _1;
        }

        /** 				if pos = 0 then*/
        if (_pos_8567 != 0)
        goto L5; // [69] 193

        /** 					pos = types:t_lower(res[i])*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4766 = (int)*(((s1_ptr)_2)->base + _i_8572);
        Ref(_4766);
        _pos_8567 = _11t_lower(_4766);
        _4766 = NOVALUE;
        if (!IS_ATOM_INT(_pos_8567)) {
            _1 = (long)(DBL_PTR(_pos_8567)->dbl);
            if (UNIQUE(DBL_PTR(_pos_8567)) && (DBL_PTR(_pos_8567)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_8567);
            _pos_8567 = _1;
        }

        /** 					if pos = 0 then*/
        if (_pos_8567 != 0)
        goto L6; // [89] 154

        /** 						pos = t_digit(res[i])*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4769 = (int)*(((s1_ptr)_2)->base + _i_8572);
        Ref(_4769);
        _pos_8567 = _11t_digit(_4769);
        _4769 = NOVALUE;
        if (!IS_ATOM_INT(_pos_8567)) {
            _1 = (long)(DBL_PTR(_pos_8567)->dbl);
            if (UNIQUE(DBL_PTR(_pos_8567)) && (DBL_PTR(_pos_8567)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_8567);
            _pos_8567 = _1;
        }

        /** 						if pos = 0 then*/
        if (_pos_8567 != 0)
        goto L4; // [109] 313

        /** 							pos = t_specword(res[i])*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4772 = (int)*(((s1_ptr)_2)->base + _i_8572);
        Ref(_4772);
        _pos_8567 = _11t_specword(_4772);
        _4772 = NOVALUE;
        if (!IS_ATOM_INT(_pos_8567)) {
            _1 = (long)(DBL_PTR(_pos_8567)->dbl);
            if (UNIQUE(DBL_PTR(_pos_8567)) && (DBL_PTR(_pos_8567)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_pos_8567);
            _pos_8567 = _1;
        }

        /** 							if pos then*/
        if (_pos_8567 == 0)
        {
            goto L7; // [129] 142
        }
        else{
        }

        /** 								inword = 1*/
        _inword_8568 = 1;
        goto L4; // [139] 313
L7: 

        /** 								inword = 0*/
        _inword_8568 = 0;
        goto L4; // [151] 313
L6: 

        /** 						if inword = 0 then*/
        if (_inword_8568 != 0)
        goto L4; // [156] 313

        /** 							if pos <= 26 then*/
        if (_pos_8567 > 26)
        goto L8; // [162] 181

        /** 								res[i] = upper(res[i]) -- Convert to uppercase*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4777 = (int)*(((s1_ptr)_2)->base + _i_8572);
        Ref(_4777);
        _4778 = _12upper(_4777);
        _4777 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_8570);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_8570 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8572);
        _1 = *(int *)_2;
        *(int *)_2 = _4778;
        if( _1 != _4778 ){
            DeRef(_1);
        }
        _4778 = NOVALUE;
L8: 

        /** 							inword = 1	-- now we are in a word*/
        _inword_8568 = 1;
        goto L4; // [190] 313
L5: 

        /** 					if inword = 1 then*/
        if (_inword_8568 != 1)
        goto L9; // [195] 216

        /** 						res[i] = lower(res[i]) -- Convert to lowercase*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4780 = (int)*(((s1_ptr)_2)->base + _i_8572);
        Ref(_4780);
        _4781 = _12lower(_4780);
        _4780 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_8570);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_8570 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8572);
        _1 = *(int *)_2;
        *(int *)_2 = _4781;
        if( _1 != _4781 ){
            DeRef(_1);
        }
        _4781 = NOVALUE;
        goto L4; // [213] 313
L9: 

        /** 						inword = 1	-- now we are in a word*/
        _inword_8568 = 1;
        goto L4; // [226] 313
L3: 

        /** 			if convert then*/
        if (_convert_8569 == 0)
        {
            goto LA; // [231] 285
        }
        else{
        }

        /** 				for j = 1 to i-1 do*/
        _4782 = _i_8572 - 1;
        {
            int _j_8613;
            _j_8613 = 1;
LB: 
            if (_j_8613 > _4782){
                goto LC; // [240] 277
            }

            /** 					if atom(x[j]) then*/
            _2 = (int)SEQ_PTR(_x_8566);
            _4783 = (int)*(((s1_ptr)_2)->base + _j_8613);
            _4784 = IS_ATOM(_4783);
            _4783 = NOVALUE;
            if (_4784 == 0)
            {
                _4784 = NOVALUE;
                goto LD; // [256] 270
            }
            else{
                _4784 = NOVALUE;
            }

            /** 						res[j] = x[j]*/
            _2 = (int)SEQ_PTR(_x_8566);
            _4785 = (int)*(((s1_ptr)_2)->base + _j_8613);
            Ref(_4785);
            _2 = (int)SEQ_PTR(_res_8570);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _res_8570 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_8613);
            _1 = *(int *)_2;
            *(int *)_2 = _4785;
            if( _1 != _4785 ){
                DeRef(_1);
            }
            _4785 = NOVALUE;
LD: 

            /** 				end for*/
            _j_8613 = _j_8613 + 1;
            goto LB; // [272] 247
LC: 
            ;
        }

        /** 				convert = 0*/
        _convert_8569 = 0;
LA: 

        /** 			if sequence(res[i]) then*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4786 = (int)*(((s1_ptr)_2)->base + _i_8572);
        _4787 = IS_SEQUENCE(_4786);
        _4786 = NOVALUE;
        if (_4787 == 0)
        {
            _4787 = NOVALUE;
            goto LE; // [294] 312
        }
        else{
            _4787 = NOVALUE;
        }

        /** 				res[i] = proper(res[i])	-- recursive conversion*/
        _2 = (int)SEQ_PTR(_res_8570);
        _4788 = (int)*(((s1_ptr)_2)->base + _i_8572);
        Ref(_4788);
        _4789 = _12proper(_4788);
        _4788 = NOVALUE;
        _2 = (int)SEQ_PTR(_res_8570);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _res_8570 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8572);
        _1 = *(int *)_2;
        *(int *)_2 = _4789;
        if( _1 != _4789 ){
            DeRef(_1);
        }
        _4789 = NOVALUE;
LE: 
L4: 

        /** 	end for*/
        _i_8572 = _i_8572 + 1;
        goto L1; // [315] 36
L2: 
        ;
    }

    /** 	return res*/
    DeRefDS(_x_8566);
    DeRef(_4782);
    _4782 = NOVALUE;
    return _res_8570;
    ;
}


int _12keyvalues(int _source_8626, int _pair_delim_8627, int _kv_delim_8629, int _quotes_8631, int _whitespace_8633, int _haskeys_8634)
{
    int _lKeyValues_8635 = NOVALUE;
    int _value__8636 = NOVALUE;
    int _key__8637 = NOVALUE;
    int _lAllDelim_8638 = NOVALUE;
    int _lWhitePair_8639 = NOVALUE;
    int _lStartBracket_8640 = NOVALUE;
    int _lEndBracket_8641 = NOVALUE;
    int _lBracketed_8642 = NOVALUE;
    int _lQuote_8643 = NOVALUE;
    int _pos__8644 = NOVALUE;
    int _lChar_8645 = NOVALUE;
    int _lBPos_8646 = NOVALUE;
    int _lWasKV_8647 = NOVALUE;
    int _4965 = NOVALUE;
    int _4962 = NOVALUE;
    int _4958 = NOVALUE;
    int _4955 = NOVALUE;
    int _4954 = NOVALUE;
    int _4953 = NOVALUE;
    int _4952 = NOVALUE;
    int _4951 = NOVALUE;
    int _4950 = NOVALUE;
    int _4949 = NOVALUE;
    int _4947 = NOVALUE;
    int _4946 = NOVALUE;
    int _4945 = NOVALUE;
    int _4944 = NOVALUE;
    int _4943 = NOVALUE;
    int _4942 = NOVALUE;
    int _4941 = NOVALUE;
    int _4940 = NOVALUE;
    int _4937 = NOVALUE;
    int _4936 = NOVALUE;
    int _4935 = NOVALUE;
    int _4934 = NOVALUE;
    int _4933 = NOVALUE;
    int _4932 = NOVALUE;
    int _4928 = NOVALUE;
    int _4926 = NOVALUE;
    int _4925 = NOVALUE;
    int _4922 = NOVALUE;
    int _4918 = NOVALUE;
    int _4916 = NOVALUE;
    int _4913 = NOVALUE;
    int _4910 = NOVALUE;
    int _4907 = NOVALUE;
    int _4904 = NOVALUE;
    int _4901 = NOVALUE;
    int _4898 = NOVALUE;
    int _4894 = NOVALUE;
    int _4893 = NOVALUE;
    int _4892 = NOVALUE;
    int _4891 = NOVALUE;
    int _4890 = NOVALUE;
    int _4889 = NOVALUE;
    int _4888 = NOVALUE;
    int _4886 = NOVALUE;
    int _4885 = NOVALUE;
    int _4884 = NOVALUE;
    int _4883 = NOVALUE;
    int _4882 = NOVALUE;
    int _4881 = NOVALUE;
    int _4880 = NOVALUE;
    int _4879 = NOVALUE;
    int _4877 = NOVALUE;
    int _4874 = NOVALUE;
    int _4872 = NOVALUE;
    int _4870 = NOVALUE;
    int _4869 = NOVALUE;
    int _4868 = NOVALUE;
    int _4867 = NOVALUE;
    int _4866 = NOVALUE;
    int _4865 = NOVALUE;
    int _4864 = NOVALUE;
    int _4863 = NOVALUE;
    int _4860 = NOVALUE;
    int _4859 = NOVALUE;
    int _4858 = NOVALUE;
    int _4857 = NOVALUE;
    int _4856 = NOVALUE;
    int _4853 = NOVALUE;
    int _4850 = NOVALUE;
    int _4847 = NOVALUE;
    int _4844 = NOVALUE;
    int _4843 = NOVALUE;
    int _4841 = NOVALUE;
    int _4840 = NOVALUE;
    int _4836 = NOVALUE;
    int _4833 = NOVALUE;
    int _4830 = NOVALUE;
    int _4826 = NOVALUE;
    int _4825 = NOVALUE;
    int _4824 = NOVALUE;
    int _4823 = NOVALUE;
    int _4819 = NOVALUE;
    int _4816 = NOVALUE;
    int _4813 = NOVALUE;
    int _4812 = NOVALUE;
    int _4810 = NOVALUE;
    int _4808 = NOVALUE;
    int _4802 = NOVALUE;
    int _4800 = NOVALUE;
    int _4798 = NOVALUE;
    int _4796 = NOVALUE;
    int _4794 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_haskeys_8634)) {
        _1 = (long)(DBL_PTR(_haskeys_8634)->dbl);
        if (UNIQUE(DBL_PTR(_haskeys_8634)) && (DBL_PTR(_haskeys_8634)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_haskeys_8634);
        _haskeys_8634 = _1;
    }

    /** 	source = trim(source)*/
    RefDS(_source_8626);
    RefDS(_3967);
    _0 = _source_8626;
    _source_8626 = _12trim(_source_8626, _3967, 0);
    DeRefDS(_0);

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_8626)){
            _4794 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4794 = 1;
    }
    if (_4794 != 0)
    goto L1; // [22] 33

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_source_8626);
    DeRef(_pair_delim_8627);
    DeRef(_kv_delim_8629);
    DeRef(_quotes_8631);
    DeRef(_whitespace_8633);
    DeRef(_lKeyValues_8635);
    DeRef(_value__8636);
    DeRef(_key__8637);
    DeRef(_lAllDelim_8638);
    DeRef(_lWhitePair_8639);
    DeRefi(_lStartBracket_8640);
    DeRefi(_lEndBracket_8641);
    DeRefi(_lBracketed_8642);
    return _5;
L1: 

    /** 	if atom(pair_delim) then*/
    _4796 = IS_ATOM(_pair_delim_8627);
    if (_4796 == 0)
    {
        _4796 = NOVALUE;
        goto L2; // [38] 48
    }
    else{
        _4796 = NOVALUE;
    }

    /** 		pair_delim = {pair_delim}*/
    _0 = _pair_delim_8627;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_pair_delim_8627);
    *((int *)(_2+4)) = _pair_delim_8627;
    _pair_delim_8627 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	if atom(kv_delim) then*/
    _4798 = IS_ATOM(_kv_delim_8629);
    if (_4798 == 0)
    {
        _4798 = NOVALUE;
        goto L3; // [53] 63
    }
    else{
        _4798 = NOVALUE;
    }

    /** 		kv_delim = {kv_delim}*/
    _0 = _kv_delim_8629;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_kv_delim_8629);
    *((int *)(_2+4)) = _kv_delim_8629;
    _kv_delim_8629 = MAKE_SEQ(_1);
    DeRef(_0);
L3: 

    /** 	if atom(quotes) then*/
    _4800 = IS_ATOM(_quotes_8631);
    if (_4800 == 0)
    {
        _4800 = NOVALUE;
        goto L4; // [68] 78
    }
    else{
        _4800 = NOVALUE;
    }

    /** 		quotes = {quotes}*/
    _0 = _quotes_8631;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quotes_8631);
    *((int *)(_2+4)) = _quotes_8631;
    _quotes_8631 = MAKE_SEQ(_1);
    DeRef(_0);
L4: 

    /** 	if atom(whitespace) then*/
    _4802 = IS_ATOM(_whitespace_8633);
    if (_4802 == 0)
    {
        _4802 = NOVALUE;
        goto L5; // [83] 93
    }
    else{
        _4802 = NOVALUE;
    }

    /** 		whitespace = {whitespace}*/
    _0 = _whitespace_8633;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_whitespace_8633);
    *((int *)(_2+4)) = _whitespace_8633;
    _whitespace_8633 = MAKE_SEQ(_1);
    DeRef(_0);
L5: 

    /** 	lAllDelim = whitespace & pair_delim & kv_delim*/
    {
        int concat_list[3];

        concat_list[0] = _kv_delim_8629;
        concat_list[1] = _pair_delim_8627;
        concat_list[2] = _whitespace_8633;
        Concat_N((object_ptr)&_lAllDelim_8638, concat_list, 3);
    }

    /** 	lWhitePair = whitespace & pair_delim*/
    if (IS_SEQUENCE(_whitespace_8633) && IS_ATOM(_pair_delim_8627)) {
        Ref(_pair_delim_8627);
        Append(&_lWhitePair_8639, _whitespace_8633, _pair_delim_8627);
    }
    else if (IS_ATOM(_whitespace_8633) && IS_SEQUENCE(_pair_delim_8627)) {
        Ref(_whitespace_8633);
        Prepend(&_lWhitePair_8639, _pair_delim_8627, _whitespace_8633);
    }
    else {
        Concat((object_ptr)&_lWhitePair_8639, _whitespace_8633, _pair_delim_8627);
    }

    /** 	lStartBracket = "{[("*/
    RefDS(_4806);
    DeRefi(_lStartBracket_8640);
    _lStartBracket_8640 = _4806;

    /** 	lEndBracket   = "}])"*/
    RefDS(_4807);
    DeRefi(_lEndBracket_8641);
    _lEndBracket_8641 = _4807;

    /** 	lKeyValues = {}*/
    RefDS(_5);
    DeRef(_lKeyValues_8635);
    _lKeyValues_8635 = _5;

    /** 	pos_ = 1*/
    _pos__8644 = 1;

    /** 	while pos_ <= length(source) do*/
L6: 
    if (IS_SEQUENCE(_source_8626)){
            _4808 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4808 = 1;
    }
    if (_pos__8644 > _4808)
    goto L7; // [143] 1298

    /** 		while pos_ < length(source) do*/
L8: 
    if (IS_SEQUENCE(_source_8626)){
            _4810 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4810 = 1;
    }
    if (_pos__8644 >= _4810)
    goto L9; // [155] 192

    /** 			if find(source[pos_], whitespace) = 0 then*/
    _2 = (int)SEQ_PTR(_source_8626);
    _4812 = (int)*(((s1_ptr)_2)->base + _pos__8644);
    _4813 = find_from(_4812, _whitespace_8633, 1);
    _4812 = NOVALUE;
    if (_4813 != 0)
    goto LA; // [170] 179

    /** 				exit*/
    goto L9; // [176] 192
LA: 

    /** 			pos_ +=1*/
    _pos__8644 = _pos__8644 + 1;

    /** 		end while*/
    goto L8; // [189] 152
L9: 

    /** 		key_ = ""*/
    RefDS(_5);
    DeRef(_key__8637);
    _key__8637 = _5;

    /** 		lQuote = 0*/
    _lQuote_8643 = 0;

    /** 		lChar = 0*/
    _lChar_8645 = 0;

    /** 		lWasKV = 0*/
    _lWasKV_8647 = 0;

    /** 		if haskeys then*/
    if (_haskeys_8634 == 0)
    {
        goto LB; // [222] 431
    }
    else{
    }

    /** 			while pos_ <= length(source) do*/
LC: 
    if (IS_SEQUENCE(_source_8626)){
            _4816 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4816 = 1;
    }
    if (_pos__8644 > _4816)
    goto LD; // [233] 359

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_8626);
    _lChar_8645 = (int)*(((s1_ptr)_2)->base + _pos__8644);
    if (!IS_ATOM_INT(_lChar_8645))
    _lChar_8645 = (long)DBL_PTR(_lChar_8645)->dbl;

    /** 				if find(lChar, quotes) != 0 then*/
    _4819 = find_from(_lChar_8645, _quotes_8631, 1);
    if (_4819 == 0)
    goto LE; // [252] 304

    /** 					if lChar = lQuote then*/
    if (_lChar_8645 != _lQuote_8643)
    goto LF; // [258] 279

    /** 						lQuote = 0*/
    _lQuote_8643 = 0;

    /** 						lChar = -1*/
    _lChar_8645 = -1;
    goto L10; // [276] 333
LF: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_8643 != 0)
    goto L10; // [281] 333

    /** 						lQuote = lChar*/
    _lQuote_8643 = _lChar_8645;

    /** 						lChar = -1*/
    _lChar_8645 = -1;
    goto L10; // [301] 333
LE: 

    /** 				elsif lQuote = 0 and find(lChar, lAllDelim) != 0 then*/
    _4823 = (_lQuote_8643 == 0);
    if (_4823 == 0) {
        goto L11; // [310] 332
    }
    _4825 = find_from(_lChar_8645, _lAllDelim_8638, 1);
    _4826 = (_4825 != 0);
    _4825 = NOVALUE;
    if (_4826 == 0)
    {
        DeRef(_4826);
        _4826 = NOVALUE;
        goto L11; // [324] 332
    }
    else{
        DeRef(_4826);
        _4826 = NOVALUE;
    }

    /** 					exit*/
    goto LD; // [329] 359
L11: 
L10: 

    /** 				if lChar > 0 then*/
    if (_lChar_8645 <= 0)
    goto L12; // [335] 346

    /** 					key_ &= lChar*/
    Append(&_key__8637, _key__8637, _lChar_8645);
L12: 

    /** 				pos_ += 1*/
    _pos__8644 = _pos__8644 + 1;

    /** 			end while*/
    goto LC; // [356] 230
LD: 

    /** 			if find(lChar, whitespace) != 0 then*/
    _4830 = find_from(_lChar_8645, _whitespace_8633, 1);
    if (_4830 == 0)
    goto L13; // [366] 440

    /** 				pos_ += 1*/
    _pos__8644 = _pos__8644 + 1;

    /** 				while pos_ <= length(source) do*/
L14: 
    if (IS_SEQUENCE(_source_8626)){
            _4833 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4833 = 1;
    }
    if (_pos__8644 > _4833)
    goto L13; // [386] 440

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_8626);
    _lChar_8645 = (int)*(((s1_ptr)_2)->base + _pos__8644);
    if (!IS_ATOM_INT(_lChar_8645))
    _lChar_8645 = (long)DBL_PTR(_lChar_8645)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _4836 = find_from(_lChar_8645, _whitespace_8633, 1);
    if (_4836 != 0)
    goto L15; // [405] 414

    /** 						exit*/
    goto L13; // [411] 440
L15: 

    /** 					pos_ +=1*/
    _pos__8644 = _pos__8644 + 1;

    /** 				end while*/
    goto L14; // [424] 383
    goto L13; // [428] 440
LB: 

    /** 			pos_ -= 1	-- Put back the last char.*/
    _pos__8644 = _pos__8644 - 1;
L13: 

    /** 		value_ = ""*/
    RefDS(_5);
    DeRef(_value__8636);
    _value__8636 = _5;

    /** 		if find(lChar, kv_delim) != 0  or not haskeys then*/
    _4840 = find_from(_lChar_8645, _kv_delim_8629, 1);
    _4841 = (_4840 != 0);
    _4840 = NOVALUE;
    if (_4841 != 0) {
        goto L16; // [458] 470
    }
    _4843 = (_haskeys_8634 == 0);
    if (_4843 == 0)
    {
        DeRef(_4843);
        _4843 = NOVALUE;
        goto L17; // [466] 981
    }
    else{
        DeRef(_4843);
        _4843 = NOVALUE;
    }
L16: 

    /** 			if find(lChar, kv_delim) != 0 then*/
    _4844 = find_from(_lChar_8645, _kv_delim_8629, 1);
    if (_4844 == 0)
    goto L18; // [477] 489

    /** 				lWasKV = 1*/
    _lWasKV_8647 = 1;
L18: 

    /** 			pos_ += 1*/
    _pos__8644 = _pos__8644 + 1;

    /** 			while pos_ <= length(source) do*/
L19: 
    if (IS_SEQUENCE(_source_8626)){
            _4847 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4847 = 1;
    }
    if (_pos__8644 > _4847)
    goto L1A; // [505] 546

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_8626);
    _lChar_8645 = (int)*(((s1_ptr)_2)->base + _pos__8644);
    if (!IS_ATOM_INT(_lChar_8645))
    _lChar_8645 = (long)DBL_PTR(_lChar_8645)->dbl;

    /** 				if find(lChar, whitespace) = 0 then*/
    _4850 = find_from(_lChar_8645, _whitespace_8633, 1);
    if (_4850 != 0)
    goto L1B; // [524] 533

    /** 					exit*/
    goto L1A; // [530] 546
L1B: 

    /** 				pos_ +=1*/
    _pos__8644 = _pos__8644 + 1;

    /** 			end while*/
    goto L19; // [543] 502
L1A: 

    /** 			lQuote = 0*/
    _lQuote_8643 = 0;

    /** 			lChar = 0*/
    _lChar_8645 = 0;

    /** 			lBracketed = {}*/
    RefDS(_5);
    DeRefi(_lBracketed_8642);
    _lBracketed_8642 = _5;

    /** 			while pos_ <= length(source) do*/
L1C: 
    if (IS_SEQUENCE(_source_8626)){
            _4853 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4853 = 1;
    }
    if (_pos__8644 > _4853)
    goto L1D; // [575] 873

    /** 				lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_8626);
    _lChar_8645 = (int)*(((s1_ptr)_2)->base + _pos__8644);
    if (!IS_ATOM_INT(_lChar_8645))
    _lChar_8645 = (long)DBL_PTR(_lChar_8645)->dbl;

    /** 				if length(lBracketed) = 0 and find(lChar, quotes) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_8642)){
            _4856 = SEQ_PTR(_lBracketed_8642)->length;
    }
    else {
        _4856 = 1;
    }
    _4857 = (_4856 == 0);
    _4856 = NOVALUE;
    if (_4857 == 0) {
        goto L1E; // [596] 661
    }
    _4859 = find_from(_lChar_8645, _quotes_8631, 1);
    _4860 = (_4859 != 0);
    _4859 = NOVALUE;
    if (_4860 == 0)
    {
        DeRef(_4860);
        _4860 = NOVALUE;
        goto L1E; // [610] 661
    }
    else{
        DeRef(_4860);
        _4860 = NOVALUE;
    }

    /** 					if lChar = lQuote then*/
    if (_lChar_8645 != _lQuote_8643)
    goto L1F; // [615] 636

    /** 						lQuote = 0*/
    _lQuote_8643 = 0;

    /** 						lChar = -1*/
    _lChar_8645 = -1;
    goto L20; // [633] 847
L1F: 

    /** 					elsif lQuote = 0 then*/
    if (_lQuote_8643 != 0)
    goto L20; // [638] 847

    /** 						lQuote = lChar*/
    _lQuote_8643 = _lChar_8645;

    /** 						lChar = -1*/
    _lChar_8645 = -1;
    goto L20; // [658] 847
L1E: 

    /** 				elsif length(value_) = 1 and value_[1] = '~' and find(lChar, lStartBracket) > 0 then*/
    if (IS_SEQUENCE(_value__8636)){
            _4863 = SEQ_PTR(_value__8636)->length;
    }
    else {
        _4863 = 1;
    }
    _4864 = (_4863 == 1);
    _4863 = NOVALUE;
    if (_4864 == 0) {
        _4865 = 0;
        goto L21; // [670] 686
    }
    _2 = (int)SEQ_PTR(_value__8636);
    _4866 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_4866)) {
        _4867 = (_4866 == 126);
    }
    else {
        _4867 = binary_op(EQUALS, _4866, 126);
    }
    _4866 = NOVALUE;
    if (IS_ATOM_INT(_4867))
    _4865 = (_4867 != 0);
    else
    _4865 = DBL_PTR(_4867)->dbl != 0.0;
L21: 
    if (_4865 == 0) {
        goto L22; // [686] 725
    }
    _4869 = find_from(_lChar_8645, _lStartBracket_8640, 1);
    _4870 = (_4869 > 0);
    _4869 = NOVALUE;
    if (_4870 == 0)
    {
        DeRef(_4870);
        _4870 = NOVALUE;
        goto L22; // [700] 725
    }
    else{
        DeRef(_4870);
        _4870 = NOVALUE;
    }

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_8646 = find_from(_lChar_8645, _lStartBracket_8640, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_8641);
    _4872 = (int)*(((s1_ptr)_2)->base + _lBPos_8646);
    Append(&_lBracketed_8642, _lBracketed_8642, _4872);
    _4872 = NOVALUE;
    goto L20; // [722] 847
L22: 

    /** 				elsif find(lChar, lStartBracket) > 0 then*/
    _4874 = find_from(_lChar_8645, _lStartBracket_8640, 1);
    if (_4874 <= 0)
    goto L23; // [732] 758

    /** 					lBPos = find(lChar, lStartBracket)*/
    _lBPos_8646 = find_from(_lChar_8645, _lStartBracket_8640, 1);

    /** 					lBracketed &= lEndBracket[lBPos]*/
    _2 = (int)SEQ_PTR(_lEndBracket_8641);
    _4877 = (int)*(((s1_ptr)_2)->base + _lBPos_8646);
    Append(&_lBracketed_8642, _lBracketed_8642, _4877);
    _4877 = NOVALUE;
    goto L20; // [755] 847
L23: 

    /** 				elsif length(lBracketed) != 0 and lChar = lBracketed[$] then*/
    if (IS_SEQUENCE(_lBracketed_8642)){
            _4879 = SEQ_PTR(_lBracketed_8642)->length;
    }
    else {
        _4879 = 1;
    }
    _4880 = (_4879 != 0);
    _4879 = NOVALUE;
    if (_4880 == 0) {
        goto L24; // [767] 803
    }
    if (IS_SEQUENCE(_lBracketed_8642)){
            _4882 = SEQ_PTR(_lBracketed_8642)->length;
    }
    else {
        _4882 = 1;
    }
    _2 = (int)SEQ_PTR(_lBracketed_8642);
    _4883 = (int)*(((s1_ptr)_2)->base + _4882);
    _4884 = (_lChar_8645 == _4883);
    _4883 = NOVALUE;
    if (_4884 == 0)
    {
        DeRef(_4884);
        _4884 = NOVALUE;
        goto L24; // [783] 803
    }
    else{
        DeRef(_4884);
        _4884 = NOVALUE;
    }

    /** 					lBracketed = lBracketed[1..$-1]*/
    if (IS_SEQUENCE(_lBracketed_8642)){
            _4885 = SEQ_PTR(_lBracketed_8642)->length;
    }
    else {
        _4885 = 1;
    }
    _4886 = _4885 - 1;
    _4885 = NOVALUE;
    rhs_slice_target = (object_ptr)&_lBracketed_8642;
    RHS_Slice(_lBracketed_8642, 1, _4886);
    goto L20; // [800] 847
L24: 

    /** 				elsif length(lBracketed) = 0 and lQuote = 0 and find(lChar, lWhitePair) != 0 then*/
    if (IS_SEQUENCE(_lBracketed_8642)){
            _4888 = SEQ_PTR(_lBracketed_8642)->length;
    }
    else {
        _4888 = 1;
    }
    _4889 = (_4888 == 0);
    _4888 = NOVALUE;
    if (_4889 == 0) {
        _4890 = 0;
        goto L25; // [812] 824
    }
    _4891 = (_lQuote_8643 == 0);
    _4890 = (_4891 != 0);
L25: 
    if (_4890 == 0) {
        goto L26; // [824] 846
    }
    _4893 = find_from(_lChar_8645, _lWhitePair_8639, 1);
    _4894 = (_4893 != 0);
    _4893 = NOVALUE;
    if (_4894 == 0)
    {
        DeRef(_4894);
        _4894 = NOVALUE;
        goto L26; // [838] 846
    }
    else{
        DeRef(_4894);
        _4894 = NOVALUE;
    }

    /** 					exit*/
    goto L1D; // [843] 873
L26: 
L20: 

    /** 				if lChar > 0 then*/
    if (_lChar_8645 <= 0)
    goto L27; // [849] 860

    /** 					value_ &= lChar*/
    Append(&_value__8636, _value__8636, _lChar_8645);
L27: 

    /** 				pos_ += 1*/
    _pos__8644 = _pos__8644 + 1;

    /** 			end while*/
    goto L1C; // [870] 572
L1D: 

    /** 			if find(lChar, whitespace) != 0  then*/
    _4898 = find_from(_lChar_8645, _whitespace_8633, 1);
    if (_4898 == 0)
    goto L28; // [880] 942

    /** 				pos_ += 1*/
    _pos__8644 = _pos__8644 + 1;

    /** 				while pos_ <= length(source) do*/
L29: 
    if (IS_SEQUENCE(_source_8626)){
            _4901 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4901 = 1;
    }
    if (_pos__8644 > _4901)
    goto L2A; // [900] 941

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_8626);
    _lChar_8645 = (int)*(((s1_ptr)_2)->base + _pos__8644);
    if (!IS_ATOM_INT(_lChar_8645))
    _lChar_8645 = (long)DBL_PTR(_lChar_8645)->dbl;

    /** 					if find(lChar, whitespace) = 0 then*/
    _4904 = find_from(_lChar_8645, _whitespace_8633, 1);
    if (_4904 != 0)
    goto L2B; // [919] 928

    /** 						exit*/
    goto L2A; // [925] 941
L2B: 

    /** 					pos_ +=1*/
    _pos__8644 = _pos__8644 + 1;

    /** 				end while*/
    goto L29; // [938] 897
L2A: 
L28: 

    /** 			if find(lChar, pair_delim) != 0  then*/
    _4907 = find_from(_lChar_8645, _pair_delim_8627, 1);
    if (_4907 == 0)
    goto L2C; // [949] 980

    /** 				pos_ += 1*/
    _pos__8644 = _pos__8644 + 1;

    /** 				if pos_ <= length(source) then*/
    if (IS_SEQUENCE(_source_8626)){
            _4910 = SEQ_PTR(_source_8626)->length;
    }
    else {
        _4910 = 1;
    }
    if (_pos__8644 > _4910)
    goto L2D; // [966] 979

    /** 					lChar = source[pos_]*/
    _2 = (int)SEQ_PTR(_source_8626);
    _lChar_8645 = (int)*(((s1_ptr)_2)->base + _pos__8644);
    if (!IS_ATOM_INT(_lChar_8645))
    _lChar_8645 = (long)DBL_PTR(_lChar_8645)->dbl;
L2D: 
L2C: 
L17: 

    /** 		if find(lChar, pair_delim) != 0  then*/
    _4913 = find_from(_lChar_8645, _pair_delim_8627, 1);
    if (_4913 == 0)
    goto L2E; // [988] 1001

    /** 			pos_ += 1*/
    _pos__8644 = _pos__8644 + 1;
L2E: 

    /** 		if length(value_) = 0 then*/
    if (IS_SEQUENCE(_value__8636)){
            _4916 = SEQ_PTR(_value__8636)->length;
    }
    else {
        _4916 = 1;
    }
    if (_4916 != 0)
    goto L2F; // [1006] 1051

    /** 			if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__8637)){
            _4918 = SEQ_PTR(_key__8637)->length;
    }
    else {
        _4918 = 1;
    }
    if (_4918 != 0)
    goto L30; // [1015] 1030

    /** 				lKeyValues = append(lKeyValues, {})*/
    RefDS(_5);
    Append(&_lKeyValues_8635, _lKeyValues_8635, _5);

    /** 				continue*/
    goto L6; // [1027] 140
L30: 

    /** 			if not lWasKV then*/
    if (_lWasKV_8647 != 0)
    goto L31; // [1032] 1050

    /** 				value_ = key_*/
    RefDS(_key__8637);
    DeRef(_value__8636);
    _value__8636 = _key__8637;

    /** 				key_ = ""*/
    RefDS(_5);
    DeRefDS(_key__8637);
    _key__8637 = _5;
L31: 
L2F: 

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__8637)){
            _4922 = SEQ_PTR(_key__8637)->length;
    }
    else {
        _4922 = 1;
    }
    if (_4922 != 0)
    goto L32; // [1056] 1080

    /** 			if haskeys then*/
    if (_haskeys_8634 == 0)
    {
        goto L33; // [1062] 1079
    }
    else{
    }

    /** 				key_ =  sprintf("p[%d]", length(lKeyValues) + 1)*/
    if (IS_SEQUENCE(_lKeyValues_8635)){
            _4925 = SEQ_PTR(_lKeyValues_8635)->length;
    }
    else {
        _4925 = 1;
    }
    _4926 = _4925 + 1;
    _4925 = NOVALUE;
    DeRefDS(_key__8637);
    _key__8637 = EPrintf(-9999999, _4924, _4926);
    _4926 = NOVALUE;
L33: 
L32: 

    /** 		if length(value_) > 0 then*/
    if (IS_SEQUENCE(_value__8636)){
            _4928 = SEQ_PTR(_value__8636)->length;
    }
    else {
        _4928 = 1;
    }
    if (_4928 <= 0)
    goto L34; // [1085] 1244

    /** 			lChar = value_[1]*/
    _2 = (int)SEQ_PTR(_value__8636);
    _lChar_8645 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lChar_8645))
    _lChar_8645 = (long)DBL_PTR(_lChar_8645)->dbl;

    /** 			lBPos = find(lChar, lStartBracket)*/
    _lBPos_8646 = find_from(_lChar_8645, _lStartBracket_8640, 1);

    /** 			if lBPos > 0 and value_[$] = lEndBracket[lBPos] then*/
    _4932 = (_lBPos_8646 > 0);
    if (_4932 == 0) {
        goto L35; // [1112] 1225
    }
    if (IS_SEQUENCE(_value__8636)){
            _4934 = SEQ_PTR(_value__8636)->length;
    }
    else {
        _4934 = 1;
    }
    _2 = (int)SEQ_PTR(_value__8636);
    _4935 = (int)*(((s1_ptr)_2)->base + _4934);
    _2 = (int)SEQ_PTR(_lEndBracket_8641);
    _4936 = (int)*(((s1_ptr)_2)->base + _lBPos_8646);
    if (IS_ATOM_INT(_4935)) {
        _4937 = (_4935 == _4936);
    }
    else {
        _4937 = binary_op(EQUALS, _4935, _4936);
    }
    _4935 = NOVALUE;
    _4936 = NOVALUE;
    if (_4937 == 0) {
        DeRef(_4937);
        _4937 = NOVALUE;
        goto L35; // [1132] 1225
    }
    else {
        if (!IS_ATOM_INT(_4937) && DBL_PTR(_4937)->dbl == 0.0){
            DeRef(_4937);
            _4937 = NOVALUE;
            goto L35; // [1132] 1225
        }
        DeRef(_4937);
        _4937 = NOVALUE;
    }
    DeRef(_4937);
    _4937 = NOVALUE;

    /** 				if lChar = '(' then*/
    if (_lChar_8645 != 40)
    goto L36; // [1137] 1184

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, haskeys)*/
    if (IS_SEQUENCE(_value__8636)){
            _4940 = SEQ_PTR(_value__8636)->length;
    }
    else {
        _4940 = 1;
    }
    _4941 = _4940 - 1;
    _4940 = NOVALUE;
    rhs_slice_target = (object_ptr)&_4942;
    RHS_Slice(_value__8636, 2, _4941);
    Ref(_pair_delim_8627);
    DeRef(_4943);
    _4943 = _pair_delim_8627;
    Ref(_kv_delim_8629);
    DeRef(_4944);
    _4944 = _kv_delim_8629;
    Ref(_quotes_8631);
    DeRef(_4945);
    _4945 = _quotes_8631;
    Ref(_whitespace_8633);
    DeRef(_4946);
    _4946 = _whitespace_8633;
    DeRef(_4947);
    _4947 = _haskeys_8634;
    _0 = _value__8636;
    _value__8636 = _12keyvalues(_4942, _4943, _4944, _4945, _4946, _4947);
    DeRefDS(_0);
    _4942 = NOVALUE;
    _4943 = NOVALUE;
    _4944 = NOVALUE;
    _4945 = NOVALUE;
    _4946 = NOVALUE;
    _4947 = NOVALUE;
    goto L37; // [1181] 1243
L36: 

    /** 					value_ = keyvalues(value_[2..$-1], pair_delim, kv_delim, quotes, whitespace, 0)*/
    if (IS_SEQUENCE(_value__8636)){
            _4949 = SEQ_PTR(_value__8636)->length;
    }
    else {
        _4949 = 1;
    }
    _4950 = _4949 - 1;
    _4949 = NOVALUE;
    rhs_slice_target = (object_ptr)&_4951;
    RHS_Slice(_value__8636, 2, _4950);
    Ref(_pair_delim_8627);
    DeRef(_4952);
    _4952 = _pair_delim_8627;
    Ref(_kv_delim_8629);
    DeRef(_4953);
    _4953 = _kv_delim_8629;
    Ref(_quotes_8631);
    DeRef(_4954);
    _4954 = _quotes_8631;
    Ref(_whitespace_8633);
    DeRef(_4955);
    _4955 = _whitespace_8633;
    _0 = _value__8636;
    _value__8636 = _12keyvalues(_4951, _4952, _4953, _4954, _4955, 0);
    DeRefDS(_0);
    _4951 = NOVALUE;
    _4952 = NOVALUE;
    _4953 = NOVALUE;
    _4954 = NOVALUE;
    _4955 = NOVALUE;
    goto L37; // [1222] 1243
L35: 

    /** 			elsif lChar = '~' then*/
    if (_lChar_8645 != 126)
    goto L38; // [1227] 1242

    /** 				value_ = value_[2 .. $]*/
    if (IS_SEQUENCE(_value__8636)){
            _4958 = SEQ_PTR(_value__8636)->length;
    }
    else {
        _4958 = 1;
    }
    rhs_slice_target = (object_ptr)&_value__8636;
    RHS_Slice(_value__8636, 2, _4958);
L38: 
L37: 
L34: 

    /** 		key_ = trim(key_)*/
    RefDS(_key__8637);
    RefDS(_3967);
    _0 = _key__8637;
    _key__8637 = _12trim(_key__8637, _3967, 0);
    DeRefDS(_0);

    /** 		value_ = trim(value_)*/
    RefDS(_value__8636);
    RefDS(_3967);
    _0 = _value__8636;
    _value__8636 = _12trim(_value__8636, _3967, 0);
    DeRefDS(_0);

    /** 		if length(key_) = 0 then*/
    if (IS_SEQUENCE(_key__8637)){
            _4962 = SEQ_PTR(_key__8637)->length;
    }
    else {
        _4962 = 1;
    }
    if (_4962 != 0)
    goto L39; // [1269] 1282

    /** 			lKeyValues = append(lKeyValues, value_)*/
    RefDS(_value__8636);
    Append(&_lKeyValues_8635, _lKeyValues_8635, _value__8636);
    goto L6; // [1279] 140
L39: 

    /** 			lKeyValues = append(lKeyValues, {key_, value_})*/
    RefDS(_value__8636);
    RefDS(_key__8637);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key__8637;
    ((int *)_2)[2] = _value__8636;
    _4965 = MAKE_SEQ(_1);
    RefDS(_4965);
    Append(&_lKeyValues_8635, _lKeyValues_8635, _4965);
    DeRefDS(_4965);
    _4965 = NOVALUE;

    /** 	end while*/
    goto L6; // [1295] 140
L7: 

    /** 	return lKeyValues*/
    DeRefDS(_source_8626);
    DeRef(_pair_delim_8627);
    DeRef(_kv_delim_8629);
    DeRef(_quotes_8631);
    DeRef(_whitespace_8633);
    DeRef(_value__8636);
    DeRef(_key__8637);
    DeRef(_lAllDelim_8638);
    DeRef(_lWhitePair_8639);
    DeRefi(_lStartBracket_8640);
    DeRefi(_lEndBracket_8641);
    DeRefi(_lBracketed_8642);
    DeRef(_4823);
    _4823 = NOVALUE;
    DeRef(_4841);
    _4841 = NOVALUE;
    DeRef(_4857);
    _4857 = NOVALUE;
    DeRef(_4864);
    _4864 = NOVALUE;
    DeRef(_4880);
    _4880 = NOVALUE;
    DeRef(_4867);
    _4867 = NOVALUE;
    DeRef(_4886);
    _4886 = NOVALUE;
    DeRef(_4889);
    _4889 = NOVALUE;
    DeRef(_4891);
    _4891 = NOVALUE;
    DeRef(_4932);
    _4932 = NOVALUE;
    DeRef(_4941);
    _4941 = NOVALUE;
    DeRef(_4950);
    _4950 = NOVALUE;
    return _lKeyValues_8635;
    ;
}


int _12escape(int _s_8874, int _what_8875)
{
    int _r_8877 = NOVALUE;
    int _4972 = NOVALUE;
    int _4970 = NOVALUE;
    int _4969 = NOVALUE;
    int _4968 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence r = ""*/
    RefDS(_5);
    DeRef(_r_8877);
    _r_8877 = _5;

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_8874)){
            _4968 = SEQ_PTR(_s_8874)->length;
    }
    else {
        _4968 = 1;
    }
    {
        int _i_8879;
        _i_8879 = 1;
L1: 
        if (_i_8879 > _4968){
            goto L2; // [17] 62
        }

        /** 		if find(s[i], what) then*/
        _2 = (int)SEQ_PTR(_s_8874);
        _4969 = (int)*(((s1_ptr)_2)->base + _i_8879);
        _4970 = find_from(_4969, _what_8875, 1);
        _4969 = NOVALUE;
        if (_4970 == 0)
        {
            _4970 = NOVALUE;
            goto L3; // [35] 45
        }
        else{
            _4970 = NOVALUE;
        }

        /** 			r &= "\\"*/
        Concat((object_ptr)&_r_8877, _r_8877, _943);
L3: 

        /** 		r &= s[i]*/
        _2 = (int)SEQ_PTR(_s_8874);
        _4972 = (int)*(((s1_ptr)_2)->base + _i_8879);
        if (IS_SEQUENCE(_r_8877) && IS_ATOM(_4972)) {
            Ref(_4972);
            Append(&_r_8877, _r_8877, _4972);
        }
        else if (IS_ATOM(_r_8877) && IS_SEQUENCE(_4972)) {
        }
        else {
            Concat((object_ptr)&_r_8877, _r_8877, _4972);
        }
        _4972 = NOVALUE;

        /** 	end for*/
        _i_8879 = _i_8879 + 1;
        goto L1; // [57] 24
L2: 
        ;
    }

    /** 	return r*/
    DeRefDS(_s_8874);
    DeRefDS(_what_8875);
    return _r_8877;
    ;
}


int _12quote(int _text_in_8889, int _quote_pair_8890, int _esc_8892, int _sp_8894)
{
    int _5060 = NOVALUE;
    int _5059 = NOVALUE;
    int _5058 = NOVALUE;
    int _5056 = NOVALUE;
    int _5055 = NOVALUE;
    int _5054 = NOVALUE;
    int _5052 = NOVALUE;
    int _5051 = NOVALUE;
    int _5050 = NOVALUE;
    int _5049 = NOVALUE;
    int _5048 = NOVALUE;
    int _5047 = NOVALUE;
    int _5046 = NOVALUE;
    int _5045 = NOVALUE;
    int _5044 = NOVALUE;
    int _5042 = NOVALUE;
    int _5041 = NOVALUE;
    int _5040 = NOVALUE;
    int _5038 = NOVALUE;
    int _5037 = NOVALUE;
    int _5036 = NOVALUE;
    int _5035 = NOVALUE;
    int _5034 = NOVALUE;
    int _5033 = NOVALUE;
    int _5032 = NOVALUE;
    int _5031 = NOVALUE;
    int _5030 = NOVALUE;
    int _5028 = NOVALUE;
    int _5027 = NOVALUE;
    int _5025 = NOVALUE;
    int _5024 = NOVALUE;
    int _5023 = NOVALUE;
    int _5021 = NOVALUE;
    int _5020 = NOVALUE;
    int _5019 = NOVALUE;
    int _5018 = NOVALUE;
    int _5017 = NOVALUE;
    int _5016 = NOVALUE;
    int _5015 = NOVALUE;
    int _5014 = NOVALUE;
    int _5013 = NOVALUE;
    int _5012 = NOVALUE;
    int _5011 = NOVALUE;
    int _5010 = NOVALUE;
    int _5009 = NOVALUE;
    int _5008 = NOVALUE;
    int _5007 = NOVALUE;
    int _5006 = NOVALUE;
    int _5005 = NOVALUE;
    int _5002 = NOVALUE;
    int _5001 = NOVALUE;
    int _5000 = NOVALUE;
    int _4999 = NOVALUE;
    int _4998 = NOVALUE;
    int _4997 = NOVALUE;
    int _4996 = NOVALUE;
    int _4995 = NOVALUE;
    int _4994 = NOVALUE;
    int _4993 = NOVALUE;
    int _4992 = NOVALUE;
    int _4991 = NOVALUE;
    int _4990 = NOVALUE;
    int _4989 = NOVALUE;
    int _4986 = NOVALUE;
    int _4984 = NOVALUE;
    int _4983 = NOVALUE;
    int _4981 = NOVALUE;
    int _4979 = NOVALUE;
    int _4978 = NOVALUE;
    int _4977 = NOVALUE;
    int _4975 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_esc_8892)) {
        _1 = (long)(DBL_PTR(_esc_8892)->dbl);
        if (UNIQUE(DBL_PTR(_esc_8892)) && (DBL_PTR(_esc_8892)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_esc_8892);
        _esc_8892 = _1;
    }

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_8889)){
            _4975 = SEQ_PTR(_text_in_8889)->length;
    }
    else {
        _4975 = 1;
    }
    if (_4975 != 0)
    goto L1; // [12] 23

    /** 		return text_in*/
    DeRef(_quote_pair_8890);
    DeRef(_sp_8894);
    return _text_in_8889;
L1: 

    /** 	if atom(quote_pair) then*/
    _4977 = IS_ATOM(_quote_pair_8890);
    if (_4977 == 0)
    {
        _4977 = NOVALUE;
        goto L2; // [28] 48
    }
    else{
        _4977 = NOVALUE;
    }

    /** 		quote_pair = {{quote_pair}, {quote_pair}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_8890);
    *((int *)(_2+4)) = _quote_pair_8890;
    _4978 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pair_8890);
    *((int *)(_2+4)) = _quote_pair_8890;
    _4979 = MAKE_SEQ(_1);
    DeRef(_quote_pair_8890);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4978;
    ((int *)_2)[2] = _4979;
    _quote_pair_8890 = MAKE_SEQ(_1);
    _4979 = NOVALUE;
    _4978 = NOVALUE;
    goto L3; // [45] 91
L2: 

    /** 	elsif length(quote_pair) = 1 then*/
    if (IS_SEQUENCE(_quote_pair_8890)){
            _4981 = SEQ_PTR(_quote_pair_8890)->length;
    }
    else {
        _4981 = 1;
    }
    if (_4981 != 1)
    goto L4; // [53] 74

    /** 		quote_pair = {quote_pair[1], quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _4983 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _4984 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_4984);
    Ref(_4983);
    DeRef(_quote_pair_8890);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4983;
    ((int *)_2)[2] = _4984;
    _quote_pair_8890 = MAKE_SEQ(_1);
    _4984 = NOVALUE;
    _4983 = NOVALUE;
    goto L3; // [71] 91
L4: 

    /** 	elsif length(quote_pair) = 0 then*/
    if (IS_SEQUENCE(_quote_pair_8890)){
            _4986 = SEQ_PTR(_quote_pair_8890)->length;
    }
    else {
        _4986 = 1;
    }
    if (_4986 != 0)
    goto L5; // [79] 90

    /** 		quote_pair = {"\"", "\""}*/
    RefDS(_4967);
    RefDS(_4967);
    DeRef(_quote_pair_8890);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4967;
    ((int *)_2)[2] = _4967;
    _quote_pair_8890 = MAKE_SEQ(_1);
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_8889);
    _4989 = (int)*(((s1_ptr)_2)->base + 1);
    _4990 = IS_SEQUENCE(_4989);
    _4989 = NOVALUE;
    if (_4990 == 0)
    {
        _4990 = NOVALUE;
        goto L6; // [100] 168
    }
    else{
        _4990 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_8889)){
            _4991 = SEQ_PTR(_text_in_8889)->length;
    }
    else {
        _4991 = 1;
    }
    {
        int _i_8917;
        _i_8917 = 1;
L7: 
        if (_i_8917 > _4991){
            goto L8; // [108] 161
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_8889);
        _4992 = (int)*(((s1_ptr)_2)->base + _i_8917);
        _4993 = IS_SEQUENCE(_4992);
        _4992 = NOVALUE;
        if (_4993 == 0)
        {
            _4993 = NOVALUE;
            goto L9; // [124] 154
        }
        else{
            _4993 = NOVALUE;
        }

        /** 				text_in[i] = quote(text_in[i], quote_pair, esc, sp)*/
        _2 = (int)SEQ_PTR(_text_in_8889);
        _4994 = (int)*(((s1_ptr)_2)->base + _i_8917);
        Ref(_quote_pair_8890);
        DeRef(_4995);
        _4995 = _quote_pair_8890;
        DeRef(_4996);
        _4996 = _esc_8892;
        Ref(_sp_8894);
        DeRef(_4997);
        _4997 = _sp_8894;
        Ref(_4994);
        _4998 = _12quote(_4994, _4995, _4996, _4997);
        _4994 = NOVALUE;
        _4995 = NOVALUE;
        _4996 = NOVALUE;
        _4997 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_8889);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_8889 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_8917);
        _1 = *(int *)_2;
        *(int *)_2 = _4998;
        if( _1 != _4998 ){
            DeRef(_1);
        }
        _4998 = NOVALUE;
L9: 

        /** 		end for*/
        _i_8917 = _i_8917 + 1;
        goto L7; // [156] 115
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pair_8890);
    DeRef(_sp_8894);
    return _text_in_8889;
L6: 

    /** 	for i = 1 to length(sp) do*/
    if (IS_SEQUENCE(_sp_8894)){
            _4999 = SEQ_PTR(_sp_8894)->length;
    }
    else {
        _4999 = 1;
    }
    {
        int _i_8928;
        _i_8928 = 1;
LA: 
        if (_i_8928 > _4999){
            goto LB; // [173] 222
        }

        /** 		if find(sp[i], text_in) then*/
        _2 = (int)SEQ_PTR(_sp_8894);
        _5000 = (int)*(((s1_ptr)_2)->base + _i_8928);
        _5001 = find_from(_5000, _text_in_8889, 1);
        _5000 = NOVALUE;
        if (_5001 == 0)
        {
            _5001 = NOVALUE;
            goto LC; // [191] 199
        }
        else{
            _5001 = NOVALUE;
        }

        /** 			exit*/
        goto LB; // [196] 222
LC: 

        /** 		if i = length(sp) then*/
        if (IS_SEQUENCE(_sp_8894)){
                _5002 = SEQ_PTR(_sp_8894)->length;
        }
        else {
            _5002 = 1;
        }
        if (_i_8928 != _5002)
        goto LD; // [204] 215

        /** 			return text_in*/
        DeRef(_quote_pair_8890);
        DeRef(_sp_8894);
        return _text_in_8889;
LD: 

        /** 	end for*/
        _i_8928 = _i_8928 + 1;
        goto LA; // [217] 180
LB: 
        ;
    }

    /** 	if esc >= 0  then*/
    if (_esc_8892 < 0)
    goto LE; // [224] 563

    /** 		if atom(quote_pair[1]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5005 = (int)*(((s1_ptr)_2)->base + 1);
    _5006 = IS_ATOM(_5005);
    _5005 = NOVALUE;
    if (_5006 == 0)
    {
        _5006 = NOVALUE;
        goto LF; // [237] 255
    }
    else{
        _5006 = NOVALUE;
    }

    /** 			quote_pair[1] = {quote_pair[1]}*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5007 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5007);
    *((int *)(_2+4)) = _5007;
    _5008 = MAKE_SEQ(_1);
    _5007 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_8890 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _5008;
    if( _1 != _5008 ){
        DeRef(_1);
    }
    _5008 = NOVALUE;
LF: 

    /** 		if atom(quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5009 = (int)*(((s1_ptr)_2)->base + 2);
    _5010 = IS_ATOM(_5009);
    _5009 = NOVALUE;
    if (_5010 == 0)
    {
        _5010 = NOVALUE;
        goto L10; // [264] 282
    }
    else{
        _5010 = NOVALUE;
    }

    /** 			quote_pair[2] = {quote_pair[2]}*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5011 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_5011);
    *((int *)(_2+4)) = _5011;
    _5012 = MAKE_SEQ(_1);
    _5011 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _quote_pair_8890 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _5012;
    if( _1 != _5012 ){
        DeRef(_1);
    }
    _5012 = NOVALUE;
L10: 

    /** 		if equal(quote_pair[1], quote_pair[2]) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5013 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5014 = (int)*(((s1_ptr)_2)->base + 2);
    if (_5013 == _5014)
    _5015 = 1;
    else if (IS_ATOM_INT(_5013) && IS_ATOM_INT(_5014))
    _5015 = 0;
    else
    _5015 = (compare(_5013, _5014) == 0);
    _5013 = NOVALUE;
    _5014 = NOVALUE;
    if (_5015 == 0)
    {
        _5015 = NOVALUE;
        goto L11; // [296] 374
    }
    else{
        _5015 = NOVALUE;
    }

    /** 			if match(quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5016 = (int)*(((s1_ptr)_2)->base + 1);
    _5017 = e_match_from(_5016, _text_in_8889, 1);
    _5016 = NOVALUE;
    if (_5017 == 0)
    {
        _5017 = NOVALUE;
        goto L12; // [310] 562
    }
    else{
        _5017 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5018 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5018)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5018)) {
        Prepend(&_5019, _5018, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5019, _esc_8892, _5018);
    }
    _5018 = NOVALUE;
    _5020 = e_match_from(_5019, _text_in_8889, 1);
    DeRefDS(_5019);
    _5019 = NOVALUE;
    if (_5020 == 0)
    {
        _5020 = NOVALUE;
        goto L13; // [328] 347
    }
    else{
        _5020 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc, text_in, esc & esc)*/
    Concat((object_ptr)&_5021, _esc_8892, _esc_8892);
    RefDS(_text_in_8889);
    _0 = _text_in_8889;
    _text_in_8889 = _14match_replace(_esc_8892, _text_in_8889, _5021, 0);
    DeRefDS(_0);
    _5021 = NOVALUE;
L13: 

    /** 				text_in = search:match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5023 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5024 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5024)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5024)) {
        Prepend(&_5025, _5024, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5025, _esc_8892, _5024);
    }
    _5024 = NOVALUE;
    Ref(_5023);
    RefDS(_text_in_8889);
    _0 = _text_in_8889;
    _text_in_8889 = _14match_replace(_5023, _text_in_8889, _5025, 0);
    DeRefDS(_0);
    _5023 = NOVALUE;
    _5025 = NOVALUE;
    goto L12; // [371] 562
L11: 

    /** 			if match(quote_pair[1], text_in) or*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5027 = (int)*(((s1_ptr)_2)->base + 1);
    _5028 = e_match_from(_5027, _text_in_8889, 1);
    _5027 = NOVALUE;
    if (_5028 != 0) {
        goto L14; // [385] 403
    }
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5030 = (int)*(((s1_ptr)_2)->base + 2);
    _5031 = e_match_from(_5030, _text_in_8889, 1);
    _5030 = NOVALUE;
    if (_5031 == 0)
    {
        _5031 = NOVALUE;
        goto L15; // [399] 475
    }
    else{
        _5031 = NOVALUE;
    }
L14: 

    /** 				if match(esc & quote_pair[1], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5032 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5032)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5032)) {
        Prepend(&_5033, _5032, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5033, _esc_8892, _5032);
    }
    _5032 = NOVALUE;
    _5034 = e_match_from(_5033, _text_in_8889, 1);
    DeRefDS(_5033);
    _5033 = NOVALUE;
    if (_5034 == 0)
    {
        _5034 = NOVALUE;
        goto L16; // [418] 451
    }
    else{
        _5034 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[1], text_in, esc & esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5035 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5035)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5035)) {
        Prepend(&_5036, _5035, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5036, _esc_8892, _5035);
    }
    _5035 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5037 = (int)*(((s1_ptr)_2)->base + 1);
    {
        int concat_list[3];

        concat_list[0] = _5037;
        concat_list[1] = _esc_8892;
        concat_list[2] = _esc_8892;
        Concat_N((object_ptr)&_5038, concat_list, 3);
    }
    _5037 = NOVALUE;
    RefDS(_text_in_8889);
    _0 = _text_in_8889;
    _text_in_8889 = _14match_replace(_5036, _text_in_8889, _5038, 0);
    DeRefDS(_0);
    _5036 = NOVALUE;
    _5038 = NOVALUE;
L16: 

    /** 				text_in = match_replace(quote_pair[1], text_in, esc & quote_pair[1])*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5040 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5041 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5041)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5041)) {
        Prepend(&_5042, _5041, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5042, _esc_8892, _5041);
    }
    _5041 = NOVALUE;
    Ref(_5040);
    RefDS(_text_in_8889);
    _0 = _text_in_8889;
    _text_in_8889 = _14match_replace(_5040, _text_in_8889, _5042, 0);
    DeRefDS(_0);
    _5040 = NOVALUE;
    _5042 = NOVALUE;
L15: 

    /** 			if match(quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5044 = (int)*(((s1_ptr)_2)->base + 2);
    _5045 = e_match_from(_5044, _text_in_8889, 1);
    _5044 = NOVALUE;
    if (_5045 == 0)
    {
        _5045 = NOVALUE;
        goto L17; // [486] 561
    }
    else{
        _5045 = NOVALUE;
    }

    /** 				if match(esc & quote_pair[2], text_in) then*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5046 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5046)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5046)) {
        Prepend(&_5047, _5046, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5047, _esc_8892, _5046);
    }
    _5046 = NOVALUE;
    _5048 = e_match_from(_5047, _text_in_8889, 1);
    DeRefDS(_5047);
    _5047 = NOVALUE;
    if (_5048 == 0)
    {
        _5048 = NOVALUE;
        goto L18; // [504] 537
    }
    else{
        _5048 = NOVALUE;
    }

    /** 					text_in = search:match_replace(esc & quote_pair[2], text_in, esc & esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5049 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5049)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5049)) {
        Prepend(&_5050, _5049, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5050, _esc_8892, _5049);
    }
    _5049 = NOVALUE;
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5051 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5051;
        concat_list[1] = _esc_8892;
        concat_list[2] = _esc_8892;
        Concat_N((object_ptr)&_5052, concat_list, 3);
    }
    _5051 = NOVALUE;
    RefDS(_text_in_8889);
    _0 = _text_in_8889;
    _text_in_8889 = _14match_replace(_5050, _text_in_8889, _5052, 0);
    DeRefDS(_0);
    _5050 = NOVALUE;
    _5052 = NOVALUE;
L18: 

    /** 				text_in = search:match_replace(quote_pair[2], text_in, esc & quote_pair[2])*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5054 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5055 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_esc_8892) && IS_ATOM(_5055)) {
    }
    else if (IS_ATOM(_esc_8892) && IS_SEQUENCE(_5055)) {
        Prepend(&_5056, _5055, _esc_8892);
    }
    else {
        Concat((object_ptr)&_5056, _esc_8892, _5055);
    }
    _5055 = NOVALUE;
    Ref(_5054);
    RefDS(_text_in_8889);
    _0 = _text_in_8889;
    _text_in_8889 = _14match_replace(_5054, _text_in_8889, _5056, 0);
    DeRefDS(_0);
    _5054 = NOVALUE;
    _5056 = NOVALUE;
L17: 
L12: 
LE: 

    /** 	return quote_pair[1] & text_in & quote_pair[2]*/
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5058 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pair_8890);
    _5059 = (int)*(((s1_ptr)_2)->base + 2);
    {
        int concat_list[3];

        concat_list[0] = _5059;
        concat_list[1] = _text_in_8889;
        concat_list[2] = _5058;
        Concat_N((object_ptr)&_5060, concat_list, 3);
    }
    _5059 = NOVALUE;
    _5058 = NOVALUE;
    DeRefDS(_text_in_8889);
    DeRef(_quote_pair_8890);
    DeRef(_sp_8894);
    return _5060;
    ;
}


int _12dequote(int _text_in_9007, int _quote_pairs_9008, int _esc_9011)
{
    int _pos_9076 = NOVALUE;
    int _5138 = NOVALUE;
    int _5137 = NOVALUE;
    int _5136 = NOVALUE;
    int _5135 = NOVALUE;
    int _5134 = NOVALUE;
    int _5133 = NOVALUE;
    int _5132 = NOVALUE;
    int _5131 = NOVALUE;
    int _5130 = NOVALUE;
    int _5129 = NOVALUE;
    int _5128 = NOVALUE;
    int _5126 = NOVALUE;
    int _5125 = NOVALUE;
    int _5124 = NOVALUE;
    int _5123 = NOVALUE;
    int _5122 = NOVALUE;
    int _5121 = NOVALUE;
    int _5120 = NOVALUE;
    int _5119 = NOVALUE;
    int _5118 = NOVALUE;
    int _5117 = NOVALUE;
    int _5116 = NOVALUE;
    int _5113 = NOVALUE;
    int _5112 = NOVALUE;
    int _5111 = NOVALUE;
    int _5110 = NOVALUE;
    int _5109 = NOVALUE;
    int _5108 = NOVALUE;
    int _5107 = NOVALUE;
    int _5106 = NOVALUE;
    int _5105 = NOVALUE;
    int _5104 = NOVALUE;
    int _5103 = NOVALUE;
    int _5102 = NOVALUE;
    int _5101 = NOVALUE;
    int _5100 = NOVALUE;
    int _5099 = NOVALUE;
    int _5098 = NOVALUE;
    int _5096 = NOVALUE;
    int _5095 = NOVALUE;
    int _5094 = NOVALUE;
    int _5093 = NOVALUE;
    int _5092 = NOVALUE;
    int _5091 = NOVALUE;
    int _5090 = NOVALUE;
    int _5089 = NOVALUE;
    int _5088 = NOVALUE;
    int _5087 = NOVALUE;
    int _5086 = NOVALUE;
    int _5085 = NOVALUE;
    int _5084 = NOVALUE;
    int _5083 = NOVALUE;
    int _5082 = NOVALUE;
    int _5081 = NOVALUE;
    int _5080 = NOVALUE;
    int _5079 = NOVALUE;
    int _5077 = NOVALUE;
    int _5075 = NOVALUE;
    int _5073 = NOVALUE;
    int _5072 = NOVALUE;
    int _5070 = NOVALUE;
    int _5068 = NOVALUE;
    int _5067 = NOVALUE;
    int _5066 = NOVALUE;
    int _5065 = NOVALUE;
    int _5063 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_esc_9011)) {
        _1 = (long)(DBL_PTR(_esc_9011)->dbl);
        if (UNIQUE(DBL_PTR(_esc_9011)) && (DBL_PTR(_esc_9011)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_esc_9011);
        _esc_9011 = _1;
    }

    /** 	if length(text_in) = 0 then*/
    if (IS_SEQUENCE(_text_in_9007)){
            _5063 = SEQ_PTR(_text_in_9007)->length;
    }
    else {
        _5063 = 1;
    }
    if (_5063 != 0)
    goto L1; // [12] 23

    /** 		return text_in*/
    DeRef(_quote_pairs_9008);
    return _text_in_9007;
L1: 

    /** 	if atom(quote_pairs) then*/
    _5065 = IS_ATOM(_quote_pairs_9008);
    if (_5065 == 0)
    {
        _5065 = NOVALUE;
        goto L2; // [28] 52
    }
    else{
        _5065 = NOVALUE;
    }

    /** 		quote_pairs = {{{quote_pairs}, {quote_pairs}}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pairs_9008);
    *((int *)(_2+4)) = _quote_pairs_9008;
    _5066 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_quote_pairs_9008);
    *((int *)(_2+4)) = _quote_pairs_9008;
    _5067 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5066;
    ((int *)_2)[2] = _5067;
    _5068 = MAKE_SEQ(_1);
    _5067 = NOVALUE;
    _5066 = NOVALUE;
    _0 = _quote_pairs_9008;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5068;
    _quote_pairs_9008 = MAKE_SEQ(_1);
    DeRef(_0);
    _5068 = NOVALUE;
    goto L3; // [49] 99
L2: 

    /** 	elsif length(quote_pairs) = 1 then*/
    if (IS_SEQUENCE(_quote_pairs_9008)){
            _5070 = SEQ_PTR(_quote_pairs_9008)->length;
    }
    else {
        _5070 = 1;
    }
    if (_5070 != 1)
    goto L4; // [57] 78

    /** 		quote_pairs = {quote_pairs[1], quote_pairs[1]}*/
    _2 = (int)SEQ_PTR(_quote_pairs_9008);
    _5072 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_quote_pairs_9008);
    _5073 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_5073);
    Ref(_5072);
    DeRef(_quote_pairs_9008);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5072;
    ((int *)_2)[2] = _5073;
    _quote_pairs_9008 = MAKE_SEQ(_1);
    _5073 = NOVALUE;
    _5072 = NOVALUE;
    goto L3; // [75] 99
L4: 

    /** 	elsif length(quote_pairs) = 0 then*/
    if (IS_SEQUENCE(_quote_pairs_9008)){
            _5075 = SEQ_PTR(_quote_pairs_9008)->length;
    }
    else {
        _5075 = 1;
    }
    if (_5075 != 0)
    goto L5; // [83] 98

    /** 		quote_pairs = {{"\"", "\""}}*/
    RefDS(_4967);
    RefDS(_4967);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _4967;
    ((int *)_2)[2] = _4967;
    _5077 = MAKE_SEQ(_1);
    _0 = _quote_pairs_9008;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5077;
    _quote_pairs_9008 = MAKE_SEQ(_1);
    DeRef(_0);
    _5077 = NOVALUE;
L5: 
L3: 

    /** 	if sequence(text_in[1]) then*/
    _2 = (int)SEQ_PTR(_text_in_9007);
    _5079 = (int)*(((s1_ptr)_2)->base + 1);
    _5080 = IS_SEQUENCE(_5079);
    _5079 = NOVALUE;
    if (_5080 == 0)
    {
        _5080 = NOVALUE;
        goto L6; // [108] 172
    }
    else{
        _5080 = NOVALUE;
    }

    /** 		for i = 1 to length(text_in) do*/
    if (IS_SEQUENCE(_text_in_9007)){
            _5081 = SEQ_PTR(_text_in_9007)->length;
    }
    else {
        _5081 = 1;
    }
    {
        int _i_9036;
        _i_9036 = 1;
L7: 
        if (_i_9036 > _5081){
            goto L8; // [116] 165
        }

        /** 			if sequence(text_in[i]) then*/
        _2 = (int)SEQ_PTR(_text_in_9007);
        _5082 = (int)*(((s1_ptr)_2)->base + _i_9036);
        _5083 = IS_SEQUENCE(_5082);
        _5082 = NOVALUE;
        if (_5083 == 0)
        {
            _5083 = NOVALUE;
            goto L9; // [132] 158
        }
        else{
            _5083 = NOVALUE;
        }

        /** 				text_in[i] = dequote(text_in[i], quote_pairs, esc)*/
        _2 = (int)SEQ_PTR(_text_in_9007);
        _5084 = (int)*(((s1_ptr)_2)->base + _i_9036);
        Ref(_quote_pairs_9008);
        DeRef(_5085);
        _5085 = _quote_pairs_9008;
        DeRef(_5086);
        _5086 = _esc_9011;
        Ref(_5084);
        _5087 = _12dequote(_5084, _5085, _5086);
        _5084 = NOVALUE;
        _5085 = NOVALUE;
        _5086 = NOVALUE;
        _2 = (int)SEQ_PTR(_text_in_9007);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _text_in_9007 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_9036);
        _1 = *(int *)_2;
        *(int *)_2 = _5087;
        if( _1 != _5087 ){
            DeRef(_1);
        }
        _5087 = NOVALUE;
L9: 

        /** 		end for*/
        _i_9036 = _i_9036 + 1;
        goto L7; // [160] 123
L8: 
        ;
    }

    /** 		return text_in*/
    DeRef(_quote_pairs_9008);
    return _text_in_9007;
L6: 

    /** 	for i = 1 to length(quote_pairs) do*/
    if (IS_SEQUENCE(_quote_pairs_9008)){
            _5088 = SEQ_PTR(_quote_pairs_9008)->length;
    }
    else {
        _5088 = 1;
    }
    {
        int _i_9046;
        _i_9046 = 1;
LA: 
        if (_i_9046 > _5088){
            goto LB; // [177] 474
        }

        /** 		if length(text_in) >= length(quote_pairs[i][1]) + length(quote_pairs[i][2]) then*/
        if (IS_SEQUENCE(_text_in_9007)){
                _5089 = SEQ_PTR(_text_in_9007)->length;
        }
        else {
            _5089 = 1;
        }
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5090 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5090);
        _5091 = (int)*(((s1_ptr)_2)->base + 1);
        _5090 = NOVALUE;
        if (IS_SEQUENCE(_5091)){
                _5092 = SEQ_PTR(_5091)->length;
        }
        else {
            _5092 = 1;
        }
        _5091 = NOVALUE;
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5093 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5093);
        _5094 = (int)*(((s1_ptr)_2)->base + 2);
        _5093 = NOVALUE;
        if (IS_SEQUENCE(_5094)){
                _5095 = SEQ_PTR(_5094)->length;
        }
        else {
            _5095 = 1;
        }
        _5094 = NOVALUE;
        _5096 = _5092 + _5095;
        if ((long)((unsigned long)_5096 + (unsigned long)HIGH_BITS) >= 0) 
        _5096 = NewDouble((double)_5096);
        _5092 = NOVALUE;
        _5095 = NOVALUE;
        if (binary_op_a(LESS, _5089, _5096)){
            _5089 = NOVALUE;
            DeRef(_5096);
            _5096 = NOVALUE;
            goto LC; // [215] 467
        }
        _5089 = NOVALUE;
        DeRef(_5096);
        _5096 = NOVALUE;

        /** 			if search:begins(quote_pairs[i][1], text_in) and search:ends(quote_pairs[i][2], text_in) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5098 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5098);
        _5099 = (int)*(((s1_ptr)_2)->base + 1);
        _5098 = NOVALUE;
        Ref(_5099);
        RefDS(_text_in_9007);
        _5100 = _14begins(_5099, _text_in_9007);
        _5099 = NOVALUE;
        if (IS_ATOM_INT(_5100)) {
            if (_5100 == 0) {
                goto LD; // [234] 464
            }
        }
        else {
            if (DBL_PTR(_5100)->dbl == 0.0) {
                goto LD; // [234] 464
            }
        }
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5102 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5102);
        _5103 = (int)*(((s1_ptr)_2)->base + 2);
        _5102 = NOVALUE;
        Ref(_5103);
        RefDS(_text_in_9007);
        _5104 = _14ends(_5103, _text_in_9007);
        _5103 = NOVALUE;
        if (_5104 == 0) {
            DeRef(_5104);
            _5104 = NOVALUE;
            goto LD; // [252] 464
        }
        else {
            if (!IS_ATOM_INT(_5104) && DBL_PTR(_5104)->dbl == 0.0){
                DeRef(_5104);
                _5104 = NOVALUE;
                goto LD; // [252] 464
            }
            DeRef(_5104);
            _5104 = NOVALUE;
        }
        DeRef(_5104);
        _5104 = NOVALUE;

        /** 				text_in = text_in[1 + length(quote_pairs[i][1]) .. $ - length(quote_pairs[i][2])]*/
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5105 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5105);
        _5106 = (int)*(((s1_ptr)_2)->base + 1);
        _5105 = NOVALUE;
        if (IS_SEQUENCE(_5106)){
                _5107 = SEQ_PTR(_5106)->length;
        }
        else {
            _5107 = 1;
        }
        _5106 = NOVALUE;
        _5108 = _5107 + 1;
        _5107 = NOVALUE;
        if (IS_SEQUENCE(_text_in_9007)){
                _5109 = SEQ_PTR(_text_in_9007)->length;
        }
        else {
            _5109 = 1;
        }
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5110 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5110);
        _5111 = (int)*(((s1_ptr)_2)->base + 2);
        _5110 = NOVALUE;
        if (IS_SEQUENCE(_5111)){
                _5112 = SEQ_PTR(_5111)->length;
        }
        else {
            _5112 = 1;
        }
        _5111 = NOVALUE;
        _5113 = _5109 - _5112;
        _5109 = NOVALUE;
        _5112 = NOVALUE;
        rhs_slice_target = (object_ptr)&_text_in_9007;
        RHS_Slice(_text_in_9007, _5108, _5113);

        /** 				integer pos = 1*/
        _pos_9076 = 1;

        /** 				while pos > 0 with entry do*/
        goto LE; // [304] 443
LF: 
        if (_pos_9076 <= 0)
        goto L10; // [307] 457

        /** 					if search:begins(quote_pairs[i][1], text_in[pos+1 .. $]) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5116 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5116);
        _5117 = (int)*(((s1_ptr)_2)->base + 1);
        _5116 = NOVALUE;
        _5118 = _pos_9076 + 1;
        if (_5118 > MAXINT){
            _5118 = NewDouble((double)_5118);
        }
        if (IS_SEQUENCE(_text_in_9007)){
                _5119 = SEQ_PTR(_text_in_9007)->length;
        }
        else {
            _5119 = 1;
        }
        rhs_slice_target = (object_ptr)&_5120;
        RHS_Slice(_text_in_9007, _5118, _5119);
        Ref(_5117);
        _5121 = _14begins(_5117, _5120);
        _5117 = NOVALUE;
        _5120 = NOVALUE;
        if (_5121 == 0) {
            DeRef(_5121);
            _5121 = NOVALUE;
            goto L11; // [338] 371
        }
        else {
            if (!IS_ATOM_INT(_5121) && DBL_PTR(_5121)->dbl == 0.0){
                DeRef(_5121);
                _5121 = NOVALUE;
                goto L11; // [338] 371
            }
            DeRef(_5121);
            _5121 = NOVALUE;
        }
        DeRef(_5121);
        _5121 = NOVALUE;

        /** 						text_in = text_in[1 .. pos-1] & text_in[pos + 1 .. $]*/
        _5122 = _pos_9076 - 1;
        rhs_slice_target = (object_ptr)&_5123;
        RHS_Slice(_text_in_9007, 1, _5122);
        _5124 = _pos_9076 + 1;
        if (_5124 > MAXINT){
            _5124 = NewDouble((double)_5124);
        }
        if (IS_SEQUENCE(_text_in_9007)){
                _5125 = SEQ_PTR(_text_in_9007)->length;
        }
        else {
            _5125 = 1;
        }
        rhs_slice_target = (object_ptr)&_5126;
        RHS_Slice(_text_in_9007, _5124, _5125);
        Concat((object_ptr)&_text_in_9007, _5123, _5126);
        DeRefDS(_5123);
        _5123 = NOVALUE;
        DeRef(_5123);
        _5123 = NOVALUE;
        DeRefDS(_5126);
        _5126 = NOVALUE;
        goto L12; // [368] 440
L11: 

        /** 					elsif search:begins(quote_pairs[i][2], text_in[pos+1 .. $]) then*/
        _2 = (int)SEQ_PTR(_quote_pairs_9008);
        _5128 = (int)*(((s1_ptr)_2)->base + _i_9046);
        _2 = (int)SEQ_PTR(_5128);
        _5129 = (int)*(((s1_ptr)_2)->base + 2);
        _5128 = NOVALUE;
        _5130 = _pos_9076 + 1;
        if (_5130 > MAXINT){
            _5130 = NewDouble((double)_5130);
        }
        if (IS_SEQUENCE(_text_in_9007)){
                _5131 = SEQ_PTR(_text_in_9007)->length;
        }
        else {
            _5131 = 1;
        }
        rhs_slice_target = (object_ptr)&_5132;
        RHS_Slice(_text_in_9007, _5130, _5131);
        Ref(_5129);
        _5133 = _14begins(_5129, _5132);
        _5129 = NOVALUE;
        _5132 = NOVALUE;
        if (_5133 == 0) {
            DeRef(_5133);
            _5133 = NOVALUE;
            goto L13; // [398] 431
        }
        else {
            if (!IS_ATOM_INT(_5133) && DBL_PTR(_5133)->dbl == 0.0){
                DeRef(_5133);
                _5133 = NOVALUE;
                goto L13; // [398] 431
            }
            DeRef(_5133);
            _5133 = NOVALUE;
        }
        DeRef(_5133);
        _5133 = NOVALUE;

        /** 						text_in = text_in[1 .. pos-1] & text_in[pos + 1 .. $]*/
        _5134 = _pos_9076 - 1;
        rhs_slice_target = (object_ptr)&_5135;
        RHS_Slice(_text_in_9007, 1, _5134);
        _5136 = _pos_9076 + 1;
        if (_5136 > MAXINT){
            _5136 = NewDouble((double)_5136);
        }
        if (IS_SEQUENCE(_text_in_9007)){
                _5137 = SEQ_PTR(_text_in_9007)->length;
        }
        else {
            _5137 = 1;
        }
        rhs_slice_target = (object_ptr)&_5138;
        RHS_Slice(_text_in_9007, _5136, _5137);
        Concat((object_ptr)&_text_in_9007, _5135, _5138);
        DeRefDS(_5135);
        _5135 = NOVALUE;
        DeRef(_5135);
        _5135 = NOVALUE;
        DeRefDS(_5138);
        _5138 = NOVALUE;
        goto L12; // [428] 440
L13: 

        /** 						pos += 1*/
        _pos_9076 = _pos_9076 + 1;
L12: 

        /** 				entry*/
LE: 

        /** 					pos = find(esc, text_in, pos)*/
        _pos_9076 = find_from(_esc_9011, _text_in_9007, _pos_9076);

        /** 				end while*/
        goto LF; // [454] 307
L10: 

        /** 				exit*/
        goto LB; // [461] 474
LD: 
LC: 

        /** 	end for*/
        _i_9046 = _i_9046 + 1;
        goto LA; // [469] 184
LB: 
        ;
    }

    /** 	return text_in*/
    DeRef(_quote_pairs_9008);
    _5091 = NOVALUE;
    _5094 = NOVALUE;
    _5106 = NOVALUE;
    DeRef(_5100);
    _5100 = NOVALUE;
    DeRef(_5108);
    _5108 = NOVALUE;
    _5111 = NOVALUE;
    DeRef(_5113);
    _5113 = NOVALUE;
    DeRef(_5122);
    _5122 = NOVALUE;
    DeRef(_5118);
    _5118 = NOVALUE;
    DeRef(_5134);
    _5134 = NOVALUE;
    DeRef(_5124);
    _5124 = NOVALUE;
    DeRef(_5130);
    _5130 = NOVALUE;
    DeRef(_5136);
    _5136 = NOVALUE;
    return _text_in_9007;
    ;
}


int _12format(int _format_pattern_9110, int _arg_list_9111)
{
    int _result_9112 = NOVALUE;
    int _in_token_9113 = NOVALUE;
    int _tch_9114 = NOVALUE;
    int _i_9115 = NOVALUE;
    int _tend_9116 = NOVALUE;
    int _cap_9117 = NOVALUE;
    int _align_9118 = NOVALUE;
    int _psign_9119 = NOVALUE;
    int _msign_9120 = NOVALUE;
    int _zfill_9121 = NOVALUE;
    int _bwz_9122 = NOVALUE;
    int _spacer_9123 = NOVALUE;
    int _alt_9124 = NOVALUE;
    int _width_9125 = NOVALUE;
    int _decs_9126 = NOVALUE;
    int _pos_9127 = NOVALUE;
    int _argn_9128 = NOVALUE;
    int _argl_9129 = NOVALUE;
    int _trimming_9130 = NOVALUE;
    int _hexout_9131 = NOVALUE;
    int _binout_9132 = NOVALUE;
    int _tsep_9133 = NOVALUE;
    int _istext_9134 = NOVALUE;
    int _prevargv_9135 = NOVALUE;
    int _currargv_9136 = NOVALUE;
    int _idname_9137 = NOVALUE;
    int _envsym_9138 = NOVALUE;
    int _envvar_9139 = NOVALUE;
    int _ep_9140 = NOVALUE;
    int _pflag_9141 = NOVALUE;
    int _count_9142 = NOVALUE;
    int _sp_9215 = NOVALUE;
    int _sp_9251 = NOVALUE;
    int _argtext_9298 = NOVALUE;
    int _tempv_9534 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2648_9589 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2645_9588 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_2706_9596 = NOVALUE;
    int _options_inlined_pretty_sprint_at_2703_9595 = NOVALUE;
    int _x_inlined_pretty_sprint_at_2700_9594 = NOVALUE;
    int _msg_inlined_crash_at_2860_9618 = NOVALUE;
    int _dpos_9680 = NOVALUE;
    int _dist_9681 = NOVALUE;
    int _bracketed_9682 = NOVALUE;
    int _5585 = NOVALUE;
    int _5584 = NOVALUE;
    int _5583 = NOVALUE;
    int _5581 = NOVALUE;
    int _5580 = NOVALUE;
    int _5579 = NOVALUE;
    int _5576 = NOVALUE;
    int _5575 = NOVALUE;
    int _5572 = NOVALUE;
    int _5570 = NOVALUE;
    int _5567 = NOVALUE;
    int _5566 = NOVALUE;
    int _5565 = NOVALUE;
    int _5562 = NOVALUE;
    int _5559 = NOVALUE;
    int _5558 = NOVALUE;
    int _5557 = NOVALUE;
    int _5556 = NOVALUE;
    int _5553 = NOVALUE;
    int _5552 = NOVALUE;
    int _5551 = NOVALUE;
    int _5548 = NOVALUE;
    int _5546 = NOVALUE;
    int _5543 = NOVALUE;
    int _5542 = NOVALUE;
    int _5541 = NOVALUE;
    int _5540 = NOVALUE;
    int _5537 = NOVALUE;
    int _5532 = NOVALUE;
    int _5531 = NOVALUE;
    int _5530 = NOVALUE;
    int _5529 = NOVALUE;
    int _5527 = NOVALUE;
    int _5526 = NOVALUE;
    int _5525 = NOVALUE;
    int _5524 = NOVALUE;
    int _5523 = NOVALUE;
    int _5522 = NOVALUE;
    int _5521 = NOVALUE;
    int _5516 = NOVALUE;
    int _5512 = NOVALUE;
    int _5511 = NOVALUE;
    int _5509 = NOVALUE;
    int _5507 = NOVALUE;
    int _5506 = NOVALUE;
    int _5505 = NOVALUE;
    int _5504 = NOVALUE;
    int _5503 = NOVALUE;
    int _5500 = NOVALUE;
    int _5498 = NOVALUE;
    int _5497 = NOVALUE;
    int _5496 = NOVALUE;
    int _5495 = NOVALUE;
    int _5492 = NOVALUE;
    int _5491 = NOVALUE;
    int _5489 = NOVALUE;
    int _5488 = NOVALUE;
    int _5487 = NOVALUE;
    int _5486 = NOVALUE;
    int _5485 = NOVALUE;
    int _5482 = NOVALUE;
    int _5481 = NOVALUE;
    int _5480 = NOVALUE;
    int _5477 = NOVALUE;
    int _5476 = NOVALUE;
    int _5474 = NOVALUE;
    int _5470 = NOVALUE;
    int _5469 = NOVALUE;
    int _5467 = NOVALUE;
    int _5466 = NOVALUE;
    int _5458 = NOVALUE;
    int _5454 = NOVALUE;
    int _5452 = NOVALUE;
    int _5451 = NOVALUE;
    int _5450 = NOVALUE;
    int _5447 = NOVALUE;
    int _5445 = NOVALUE;
    int _5444 = NOVALUE;
    int _5443 = NOVALUE;
    int _5441 = NOVALUE;
    int _5440 = NOVALUE;
    int _5439 = NOVALUE;
    int _5438 = NOVALUE;
    int _5435 = NOVALUE;
    int _5434 = NOVALUE;
    int _5433 = NOVALUE;
    int _5431 = NOVALUE;
    int _5430 = NOVALUE;
    int _5429 = NOVALUE;
    int _5428 = NOVALUE;
    int _5426 = NOVALUE;
    int _5424 = NOVALUE;
    int _5422 = NOVALUE;
    int _5420 = NOVALUE;
    int _5418 = NOVALUE;
    int _5416 = NOVALUE;
    int _5415 = NOVALUE;
    int _5414 = NOVALUE;
    int _5413 = NOVALUE;
    int _5412 = NOVALUE;
    int _5411 = NOVALUE;
    int _5409 = NOVALUE;
    int _5408 = NOVALUE;
    int _5406 = NOVALUE;
    int _5405 = NOVALUE;
    int _5403 = NOVALUE;
    int _5401 = NOVALUE;
    int _5400 = NOVALUE;
    int _5397 = NOVALUE;
    int _5395 = NOVALUE;
    int _5391 = NOVALUE;
    int _5389 = NOVALUE;
    int _5388 = NOVALUE;
    int _5387 = NOVALUE;
    int _5385 = NOVALUE;
    int _5384 = NOVALUE;
    int _5383 = NOVALUE;
    int _5382 = NOVALUE;
    int _5381 = NOVALUE;
    int _5379 = NOVALUE;
    int _5377 = NOVALUE;
    int _5376 = NOVALUE;
    int _5375 = NOVALUE;
    int _5374 = NOVALUE;
    int _5370 = NOVALUE;
    int _5367 = NOVALUE;
    int _5366 = NOVALUE;
    int _5363 = NOVALUE;
    int _5362 = NOVALUE;
    int _5361 = NOVALUE;
    int _5359 = NOVALUE;
    int _5358 = NOVALUE;
    int _5357 = NOVALUE;
    int _5356 = NOVALUE;
    int _5354 = NOVALUE;
    int _5352 = NOVALUE;
    int _5351 = NOVALUE;
    int _5350 = NOVALUE;
    int _5349 = NOVALUE;
    int _5348 = NOVALUE;
    int _5347 = NOVALUE;
    int _5345 = NOVALUE;
    int _5344 = NOVALUE;
    int _5343 = NOVALUE;
    int _5341 = NOVALUE;
    int _5340 = NOVALUE;
    int _5339 = NOVALUE;
    int _5338 = NOVALUE;
    int _5336 = NOVALUE;
    int _5333 = NOVALUE;
    int _5332 = NOVALUE;
    int _5330 = NOVALUE;
    int _5329 = NOVALUE;
    int _5327 = NOVALUE;
    int _5324 = NOVALUE;
    int _5323 = NOVALUE;
    int _5320 = NOVALUE;
    int _5318 = NOVALUE;
    int _5314 = NOVALUE;
    int _5312 = NOVALUE;
    int _5311 = NOVALUE;
    int _5310 = NOVALUE;
    int _5308 = NOVALUE;
    int _5306 = NOVALUE;
    int _5305 = NOVALUE;
    int _5304 = NOVALUE;
    int _5303 = NOVALUE;
    int _5302 = NOVALUE;
    int _5300 = NOVALUE;
    int _5298 = NOVALUE;
    int _5297 = NOVALUE;
    int _5296 = NOVALUE;
    int _5295 = NOVALUE;
    int _5293 = NOVALUE;
    int _5290 = NOVALUE;
    int _5288 = NOVALUE;
    int _5287 = NOVALUE;
    int _5286 = NOVALUE;
    int _5285 = NOVALUE;
    int _5284 = NOVALUE;
    int _5282 = NOVALUE;
    int _5281 = NOVALUE;
    int _5280 = NOVALUE;
    int _5278 = NOVALUE;
    int _5277 = NOVALUE;
    int _5276 = NOVALUE;
    int _5275 = NOVALUE;
    int _5273 = NOVALUE;
    int _5272 = NOVALUE;
    int _5271 = NOVALUE;
    int _5268 = NOVALUE;
    int _5267 = NOVALUE;
    int _5266 = NOVALUE;
    int _5265 = NOVALUE;
    int _5263 = NOVALUE;
    int _5262 = NOVALUE;
    int _5261 = NOVALUE;
    int _5260 = NOVALUE;
    int _5259 = NOVALUE;
    int _5256 = NOVALUE;
    int _5255 = NOVALUE;
    int _5254 = NOVALUE;
    int _5253 = NOVALUE;
    int _5251 = NOVALUE;
    int _5250 = NOVALUE;
    int _5249 = NOVALUE;
    int _5247 = NOVALUE;
    int _5246 = NOVALUE;
    int _5245 = NOVALUE;
    int _5243 = NOVALUE;
    int _5236 = NOVALUE;
    int _5234 = NOVALUE;
    int _5233 = NOVALUE;
    int _5226 = NOVALUE;
    int _5223 = NOVALUE;
    int _5219 = NOVALUE;
    int _5217 = NOVALUE;
    int _5216 = NOVALUE;
    int _5213 = NOVALUE;
    int _5211 = NOVALUE;
    int _5209 = NOVALUE;
    int _5206 = NOVALUE;
    int _5204 = NOVALUE;
    int _5203 = NOVALUE;
    int _5202 = NOVALUE;
    int _5201 = NOVALUE;
    int _5200 = NOVALUE;
    int _5197 = NOVALUE;
    int _5194 = NOVALUE;
    int _5193 = NOVALUE;
    int _5192 = NOVALUE;
    int _5189 = NOVALUE;
    int _5187 = NOVALUE;
    int _5185 = NOVALUE;
    int _5182 = NOVALUE;
    int _5181 = NOVALUE;
    int _5174 = NOVALUE;
    int _5171 = NOVALUE;
    int _5170 = NOVALUE;
    int _5162 = NOVALUE;
    int _5157 = NOVALUE;
    int _5154 = NOVALUE;
    int _5144 = NOVALUE;
    int _5142 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(arg_list) then*/
    _5142 = IS_ATOM(_arg_list_9111);
    if (_5142 == 0)
    {
        _5142 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _5142 = NOVALUE;
    }

    /** 		arg_list = {arg_list}*/
    _0 = _arg_list_9111;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_arg_list_9111);
    *((int *)(_2+4)) = _arg_list_9111;
    _arg_list_9111 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	result = ""*/
    RefDS(_5);
    DeRef(_result_9112);
    _result_9112 = _5;

    /** 	in_token = 0*/
    _in_token_9113 = 0;

    /** 	i = 0*/
    _i_9115 = 0;

    /** 	tend = 0*/
    _tend_9116 = 0;

    /** 	argl = 0*/
    _argl_9129 = 0;

    /** 	spacer = 0*/
    _spacer_9123 = 0;

    /** 	prevargv = 0*/
    DeRef(_prevargv_9135);
    _prevargv_9135 = 0;

    /**     while i < length(format_pattern) do*/
L2: 
    if (IS_SEQUENCE(_format_pattern_9110)){
            _5144 = SEQ_PTR(_format_pattern_9110)->length;
    }
    else {
        _5144 = 1;
    }
    if (_i_9115 >= _5144)
    goto L3; // [73] 3791

    /**     	i += 1*/
    _i_9115 = _i_9115 + 1;

    /**     	tch = format_pattern[i]*/
    _2 = (int)SEQ_PTR(_format_pattern_9110);
    _tch_9114 = (int)*(((s1_ptr)_2)->base + _i_9115);
    if (!IS_ATOM_INT(_tch_9114))
    _tch_9114 = (long)DBL_PTR(_tch_9114)->dbl;

    /**     	if not in_token then*/
    if (_in_token_9113 != 0)
    goto L4; // [95] 260

    /**     		if tch = '[' then*/
    if (_tch_9114 != 91)
    goto L5; // [100] 250

    /**     			in_token = 1*/
    _in_token_9113 = 1;

    /**     			tend = 0*/
    _tend_9116 = 0;

    /** 				cap = 0*/
    _cap_9117 = 0;

    /** 				align = 0*/
    _align_9118 = 0;

    /** 				psign = 0*/
    _psign_9119 = 0;

    /** 				msign = 0*/
    _msign_9120 = 0;

    /** 				zfill = 0*/
    _zfill_9121 = 0;

    /** 				bwz = 0*/
    _bwz_9122 = 0;

    /** 				spacer = 0*/
    _spacer_9123 = 0;

    /** 				alt = 0*/
    _alt_9124 = 0;

    /**     			width = 0*/
    _width_9125 = 0;

    /**     			decs = -1*/
    _decs_9126 = -1;

    /**     			argn = 0*/
    _argn_9128 = 0;

    /**     			hexout = 0*/
    _hexout_9131 = 0;

    /**     			binout = 0*/
    _binout_9132 = 0;

    /**     			trimming = 0*/
    _trimming_9130 = 0;

    /**     			tsep = 0*/
    _tsep_9133 = 0;

    /**     			istext = 0*/
    _istext_9134 = 0;

    /**     			idname = ""*/
    RefDS(_5);
    DeRef(_idname_9137);
    _idname_9137 = _5;

    /**     			envvar = ""*/
    RefDS(_5);
    DeRefi(_envvar_9139);
    _envvar_9139 = _5;

    /**     			envsym = ""*/
    RefDS(_5);
    DeRef(_envsym_9138);
    _envsym_9138 = _5;
    goto L2; // [247] 70
L5: 

    /**     			result &= tch*/
    Append(&_result_9112, _result_9112, _tch_9114);
    goto L2; // [257] 70
L4: 

    /** 			switch tch do*/
    _0 = _tch_9114;
    switch ( _0 ){ 

        /**     			case ']' then*/
        case 93:

        /**     				in_token = 0*/
        _in_token_9113 = 0;

        /**     				tend = i*/
        _tend_9116 = _i_9115;
        goto L6; // [285] 1218

        /**     			case '[' then*/
        case 91:

        /** 	    			result &= tch*/
        Append(&_result_9112, _result_9112, _tch_9114);

        /** 	    			while i < length(format_pattern) do*/
L7: 
        if (IS_SEQUENCE(_format_pattern_9110)){
                _5154 = SEQ_PTR(_format_pattern_9110)->length;
        }
        else {
            _5154 = 1;
        }
        if (_i_9115 >= _5154)
        goto L6; // [305] 1218

        /** 	    				i += 1*/
        _i_9115 = _i_9115 + 1;

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _5157 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (binary_op_a(NOTEQ, _5157, 93)){
            _5157 = NOVALUE;
            goto L7; // [323] 302
        }
        _5157 = NOVALUE;

        /** 	    					in_token = 0*/
        _in_token_9113 = 0;

        /** 	    					tend = 0*/
        _tend_9116 = 0;

        /** 	    					exit*/
        goto L6; // [343] 1218

        /** 	    			end while*/
        goto L7; // [348] 302
        goto L6; // [351] 1218

        /** 	    		case 'w', 'u', 'l' then*/
        case 119:
        case 117:
        case 108:

        /** 	    			cap = tch*/
        _cap_9117 = _tch_9114;
        goto L6; // [368] 1218

        /** 	    		case 'b' then*/
        case 98:

        /** 	    			bwz = 1*/
        _bwz_9122 = 1;
        goto L6; // [381] 1218

        /** 	    		case 's' then*/
        case 115:

        /** 	    			spacer = 1*/
        _spacer_9123 = 1;
        goto L6; // [394] 1218

        /** 	    		case 't' then*/
        case 116:

        /** 	    			trimming = 1*/
        _trimming_9130 = 1;
        goto L6; // [407] 1218

        /** 	    		case 'z' then*/
        case 122:

        /** 	    			zfill = 1*/
        _zfill_9121 = 1;
        goto L6; // [420] 1218

        /** 	    		case 'X' then*/
        case 88:

        /** 	    			hexout = 1*/
        _hexout_9131 = 1;
        goto L6; // [433] 1218

        /** 	    		case 'B' then*/
        case 66:

        /** 	    			binout = 1*/
        _binout_9132 = 1;
        goto L6; // [446] 1218

        /** 	    		case 'c', '<', '>' then*/
        case 99:
        case 60:
        case 62:

        /** 	    			align = tch*/
        _align_9118 = _tch_9114;
        goto L6; // [463] 1218

        /** 	    		case '+' then*/
        case 43:

        /** 	    			psign = 1*/
        _psign_9119 = 1;
        goto L6; // [476] 1218

        /** 	    		case '(' then*/
        case 40:

        /** 	    			msign = 1*/
        _msign_9120 = 1;
        goto L6; // [489] 1218

        /** 	    		case '?' then*/
        case 63:

        /** 	    			alt = 1*/
        _alt_9124 = 1;
        goto L6; // [502] 1218

        /** 	    		case 'T' then*/
        case 84:

        /** 	    			istext = 1*/
        _istext_9134 = 1;
        goto L6; // [515] 1218

        /** 	    		case ':' then*/
        case 58:

        /** 	    			while i < length(format_pattern) do*/
L8: 
        if (IS_SEQUENCE(_format_pattern_9110)){
                _5162 = SEQ_PTR(_format_pattern_9110)->length;
        }
        else {
            _5162 = 1;
        }
        if (_i_9115 >= _5162)
        goto L6; // [529] 1218

        /** 	    				i += 1*/
        _i_9115 = _i_9115 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _tch_9114 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (!IS_ATOM_INT(_tch_9114))
        _tch_9114 = (long)DBL_PTR(_tch_9114)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_9127 = find_from(_tch_9114, _5166, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_9127 != 0)
        goto L9; // [560] 577

        /** 	    					i -= 1*/
        _i_9115 = _i_9115 - 1;

        /** 	    					exit*/
        goto L6; // [574] 1218
L9: 

        /** 	    				width = width * 10 + pos - 1*/
        if (_width_9125 == (short)_width_9125)
        _5170 = _width_9125 * 10;
        else
        _5170 = NewDouble(_width_9125 * (double)10);
        if (IS_ATOM_INT(_5170)) {
            _5171 = _5170 + _pos_9127;
            if ((long)((unsigned long)_5171 + (unsigned long)HIGH_BITS) >= 0) 
            _5171 = NewDouble((double)_5171);
        }
        else {
            _5171 = NewDouble(DBL_PTR(_5170)->dbl + (double)_pos_9127);
        }
        DeRef(_5170);
        _5170 = NOVALUE;
        if (IS_ATOM_INT(_5171)) {
            _width_9125 = _5171 - 1;
        }
        else {
            _width_9125 = NewDouble(DBL_PTR(_5171)->dbl - (double)1);
        }
        DeRef(_5171);
        _5171 = NOVALUE;
        if (!IS_ATOM_INT(_width_9125)) {
            _1 = (long)(DBL_PTR(_width_9125)->dbl);
            if (UNIQUE(DBL_PTR(_width_9125)) && (DBL_PTR(_width_9125)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_width_9125);
            _width_9125 = _1;
        }

        /** 	    				if width = 0 then*/
        if (_width_9125 != 0)
        goto L8; // [599] 526

        /** 	    					zfill = '0'*/
        _zfill_9121 = 48;

        /** 	    			end while*/
        goto L8; // [613] 526
        goto L6; // [616] 1218

        /** 	    		case '.' then*/
        case 46:

        /** 	    			decs = 0*/
        _decs_9126 = 0;

        /** 	    			while i < length(format_pattern) do*/
LA: 
        if (IS_SEQUENCE(_format_pattern_9110)){
                _5174 = SEQ_PTR(_format_pattern_9110)->length;
        }
        else {
            _5174 = 1;
        }
        if (_i_9115 >= _5174)
        goto L6; // [637] 1218

        /** 	    				i += 1*/
        _i_9115 = _i_9115 + 1;

        /** 	    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _tch_9114 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (!IS_ATOM_INT(_tch_9114))
        _tch_9114 = (long)DBL_PTR(_tch_9114)->dbl;

        /** 	    				pos = find(tch, "0123456789")*/
        _pos_9127 = find_from(_tch_9114, _5166, 1);

        /** 	    				if pos = 0 then*/
        if (_pos_9127 != 0)
        goto LB; // [668] 685

        /** 	    					i -= 1*/
        _i_9115 = _i_9115 - 1;

        /** 	    					exit*/
        goto L6; // [682] 1218
LB: 

        /** 	    				decs = decs * 10 + pos - 1*/
        if (_decs_9126 == (short)_decs_9126)
        _5181 = _decs_9126 * 10;
        else
        _5181 = NewDouble(_decs_9126 * (double)10);
        if (IS_ATOM_INT(_5181)) {
            _5182 = _5181 + _pos_9127;
            if ((long)((unsigned long)_5182 + (unsigned long)HIGH_BITS) >= 0) 
            _5182 = NewDouble((double)_5182);
        }
        else {
            _5182 = NewDouble(DBL_PTR(_5181)->dbl + (double)_pos_9127);
        }
        DeRef(_5181);
        _5181 = NOVALUE;
        if (IS_ATOM_INT(_5182)) {
            _decs_9126 = _5182 - 1;
        }
        else {
            _decs_9126 = NewDouble(DBL_PTR(_5182)->dbl - (double)1);
        }
        DeRef(_5182);
        _5182 = NOVALUE;
        if (!IS_ATOM_INT(_decs_9126)) {
            _1 = (long)(DBL_PTR(_decs_9126)->dbl);
            if (UNIQUE(DBL_PTR(_decs_9126)) && (DBL_PTR(_decs_9126)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_decs_9126);
            _decs_9126 = _1;
        }

        /** 	    			end while*/
        goto LA; // [705] 634
        goto L6; // [708] 1218

        /** 	    		case '{' then*/
        case 123:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_9215 = _i_9115 + 1;

        /** 	    			i = sp*/
        _i_9115 = _sp_9215;

        /** 	    			while i < length(format_pattern) do*/
LC: 
        if (IS_SEQUENCE(_format_pattern_9110)){
                _5185 = SEQ_PTR(_format_pattern_9110)->length;
        }
        else {
            _5185 = 1;
        }
        if (_i_9115 >= _5185)
        goto LD; // [739] 786

        /** 	    				if format_pattern[i] = '}' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _5187 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (binary_op_a(NOTEQ, _5187, 125)){
            _5187 = NOVALUE;
            goto LE; // [749] 758
        }
        _5187 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [755] 786
LE: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _5189 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (binary_op_a(NOTEQ, _5189, 93)){
            _5189 = NOVALUE;
            goto LF; // [764] 773
        }
        _5189 = NOVALUE;

        /** 	    					exit*/
        goto LD; // [770] 786
LF: 

        /** 	    				i += 1*/
        _i_9115 = _i_9115 + 1;

        /** 	    			end while*/
        goto LC; // [783] 736
LD: 

        /** 	    			idname = trim(format_pattern[sp .. i-1]) & '='*/
        _5192 = _i_9115 - 1;
        rhs_slice_target = (object_ptr)&_5193;
        RHS_Slice(_format_pattern_9110, _sp_9215, _5192);
        RefDS(_3967);
        _5194 = _12trim(_5193, _3967, 0);
        _5193 = NOVALUE;
        if (IS_SEQUENCE(_5194) && IS_ATOM(61)) {
            Append(&_idname_9137, _5194, 61);
        }
        else if (IS_ATOM(_5194) && IS_SEQUENCE(61)) {
        }
        else {
            Concat((object_ptr)&_idname_9137, _5194, 61);
            DeRef(_5194);
            _5194 = NOVALUE;
        }
        DeRef(_5194);
        _5194 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _5197 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (binary_op_a(NOTEQ, _5197, 93)){
            _5197 = NOVALUE;
            goto L10; // [813] 826
        }
        _5197 = NOVALUE;

        /**     					i -= 1*/
        _i_9115 = _i_9115 - 1;
L10: 

        /**     				for j = 1 to length(arg_list) do*/
        if (IS_SEQUENCE(_arg_list_9111)){
                _5200 = SEQ_PTR(_arg_list_9111)->length;
        }
        else {
            _5200 = 1;
        }
        {
            int _j_9237;
            _j_9237 = 1;
L11: 
            if (_j_9237 > _5200){
                goto L12; // [831] 917
            }

            /**     					if sequence(arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_9111);
            _5201 = (int)*(((s1_ptr)_2)->base + _j_9237);
            _5202 = IS_SEQUENCE(_5201);
            _5201 = NOVALUE;
            if (_5202 == 0)
            {
                _5202 = NOVALUE;
                goto L13; // [847] 886
            }
            else{
                _5202 = NOVALUE;
            }

            /**     						if search:begins(idname, arg_list[j]) then*/
            _2 = (int)SEQ_PTR(_arg_list_9111);
            _5203 = (int)*(((s1_ptr)_2)->base + _j_9237);
            RefDS(_idname_9137);
            Ref(_5203);
            _5204 = _14begins(_idname_9137, _5203);
            _5203 = NOVALUE;
            if (_5204 == 0) {
                DeRef(_5204);
                _5204 = NOVALUE;
                goto L14; // [861] 885
            }
            else {
                if (!IS_ATOM_INT(_5204) && DBL_PTR(_5204)->dbl == 0.0){
                    DeRef(_5204);
                    _5204 = NOVALUE;
                    goto L14; // [861] 885
                }
                DeRef(_5204);
                _5204 = NOVALUE;
            }
            DeRef(_5204);
            _5204 = NOVALUE;

            /**     							if argn = 0 then*/
            if (_argn_9128 != 0)
            goto L15; // [868] 884

            /**     								argn = j*/
            _argn_9128 = _j_9237;

            /**     								exit*/
            goto L12; // [881] 917
L15: 
L14: 
L13: 

            /**     					if j = length(arg_list) then*/
            if (IS_SEQUENCE(_arg_list_9111)){
                    _5206 = SEQ_PTR(_arg_list_9111)->length;
            }
            else {
                _5206 = 1;
            }
            if (_j_9237 != _5206)
            goto L16; // [891] 910

            /**     						idname = ""*/
            RefDS(_5);
            DeRef(_idname_9137);
            _idname_9137 = _5;

            /**     						argn = -1*/
            _argn_9128 = -1;
L16: 

            /**     				end for*/
            _j_9237 = _j_9237 + 1;
            goto L11; // [912] 838
L12: 
            ;
        }
        goto L6; // [919] 1218

        /** 	    		case '%' then*/
        case 37:

        /** 	    			integer sp*/

        /** 	    			sp = i + 1*/
        _sp_9251 = _i_9115 + 1;

        /** 	    			i = sp*/
        _i_9115 = _sp_9251;

        /** 	    			while i < length(format_pattern) do*/
L17: 
        if (IS_SEQUENCE(_format_pattern_9110)){
                _5209 = SEQ_PTR(_format_pattern_9110)->length;
        }
        else {
            _5209 = 1;
        }
        if (_i_9115 >= _5209)
        goto L18; // [950] 997

        /** 	    				if format_pattern[i] = '%' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _5211 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (binary_op_a(NOTEQ, _5211, 37)){
            _5211 = NOVALUE;
            goto L19; // [960] 969
        }
        _5211 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [966] 997
L19: 

        /** 	    				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _5213 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (binary_op_a(NOTEQ, _5213, 93)){
            _5213 = NOVALUE;
            goto L1A; // [975] 984
        }
        _5213 = NOVALUE;

        /** 	    					exit*/
        goto L18; // [981] 997
L1A: 

        /** 	    				i += 1*/
        _i_9115 = _i_9115 + 1;

        /** 	    			end while*/
        goto L17; // [994] 947
L18: 

        /** 	    			envsym = trim(format_pattern[sp .. i-1])*/
        _5216 = _i_9115 - 1;
        rhs_slice_target = (object_ptr)&_5217;
        RHS_Slice(_format_pattern_9110, _sp_9251, _5216);
        RefDS(_3967);
        _0 = _envsym_9138;
        _envsym_9138 = _12trim(_5217, _3967, 0);
        DeRef(_0);
        _5217 = NOVALUE;

        /**     				if format_pattern[i] = ']' then*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _5219 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (binary_op_a(NOTEQ, _5219, 93)){
            _5219 = NOVALUE;
            goto L1B; // [1020] 1033
        }
        _5219 = NOVALUE;

        /**     					i -= 1*/
        _i_9115 = _i_9115 - 1;
L1B: 

        /**     				envvar = getenv(envsym)*/
        DeRefi(_envvar_9139);
        _envvar_9139 = EGetEnv(_envsym_9138);

        /**     				argn = -1*/
        _argn_9128 = -1;

        /**     				if atom(envvar) then*/
        _5223 = IS_ATOM(_envvar_9139);
        if (_5223 == 0)
        {
            _5223 = NOVALUE;
            goto L1C; // [1050] 1059
        }
        else{
            _5223 = NOVALUE;
        }

        /**     					envvar = ""*/
        RefDS(_5);
        DeRefi(_envvar_9139);
        _envvar_9139 = _5;
L1C: 
        goto L6; // [1061] 1218

        /** 	    		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' then*/
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:

        /** 	    			if argn = 0 then*/
        if (_argn_9128 != 0)
        goto L6; // [1087] 1218

        /** 		    			i -= 1*/
        _i_9115 = _i_9115 - 1;

        /** 		    			while i < length(format_pattern) do*/
L1D: 
        if (IS_SEQUENCE(_format_pattern_9110)){
                _5226 = SEQ_PTR(_format_pattern_9110)->length;
        }
        else {
            _5226 = 1;
        }
        if (_i_9115 >= _5226)
        goto L6; // [1107] 1218

        /** 		    				i += 1*/
        _i_9115 = _i_9115 + 1;

        /** 		    				tch = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _tch_9114 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (!IS_ATOM_INT(_tch_9114))
        _tch_9114 = (long)DBL_PTR(_tch_9114)->dbl;

        /** 		    				pos = find(tch, "0123456789")*/
        _pos_9127 = find_from(_tch_9114, _5166, 1);

        /** 		    				if pos = 0 then*/
        if (_pos_9127 != 0)
        goto L1E; // [1138] 1155

        /** 		    					i -= 1*/
        _i_9115 = _i_9115 - 1;

        /** 		    					exit*/
        goto L6; // [1152] 1218
L1E: 

        /** 		    				argn = argn * 10 + pos - 1*/
        if (_argn_9128 == (short)_argn_9128)
        _5233 = _argn_9128 * 10;
        else
        _5233 = NewDouble(_argn_9128 * (double)10);
        if (IS_ATOM_INT(_5233)) {
            _5234 = _5233 + _pos_9127;
            if ((long)((unsigned long)_5234 + (unsigned long)HIGH_BITS) >= 0) 
            _5234 = NewDouble((double)_5234);
        }
        else {
            _5234 = NewDouble(DBL_PTR(_5233)->dbl + (double)_pos_9127);
        }
        DeRef(_5233);
        _5233 = NOVALUE;
        if (IS_ATOM_INT(_5234)) {
            _argn_9128 = _5234 - 1;
        }
        else {
            _argn_9128 = NewDouble(DBL_PTR(_5234)->dbl - (double)1);
        }
        DeRef(_5234);
        _5234 = NOVALUE;
        if (!IS_ATOM_INT(_argn_9128)) {
            _1 = (long)(DBL_PTR(_argn_9128)->dbl);
            if (UNIQUE(DBL_PTR(_argn_9128)) && (DBL_PTR(_argn_9128)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_argn_9128);
            _argn_9128 = _1;
        }

        /** 		    			end while*/
        goto L1D; // [1175] 1104
        goto L6; // [1179] 1218

        /** 	    		case ',' then*/
        case 44:

        /** 	    			if i < length(format_pattern) then*/
        if (IS_SEQUENCE(_format_pattern_9110)){
                _5236 = SEQ_PTR(_format_pattern_9110)->length;
        }
        else {
            _5236 = 1;
        }
        if (_i_9115 >= _5236)
        goto L6; // [1190] 1218

        /** 	    				i +=1*/
        _i_9115 = _i_9115 + 1;

        /** 	    				tsep = format_pattern[i]*/
        _2 = (int)SEQ_PTR(_format_pattern_9110);
        _tsep_9133 = (int)*(((s1_ptr)_2)->base + _i_9115);
        if (!IS_ATOM_INT(_tsep_9133))
        _tsep_9133 = (long)DBL_PTR(_tsep_9133)->dbl;
        goto L6; // [1211] 1218

        /** 	    		case else*/
        default:
    ;}L6: 

    /**     		if tend > 0 then*/
    if (_tend_9116 <= 0)
    goto L1F; // [1220] 3783

    /**     			sequence argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_9298);
    _argtext_9298 = _5;

    /**     			if argn = 0 then*/
    if (_argn_9128 != 0)
    goto L20; // [1235] 1248

    /**     				argn = argl + 1*/
    _argn_9128 = _argl_9129 + 1;
L20: 

    /**     			argl = argn*/
    _argl_9129 = _argn_9128;

    /**     			if argn < 1 or argn > length(arg_list) then*/
    _5243 = (_argn_9128 < 1);
    if (_5243 != 0) {
        goto L21; // [1261] 1277
    }
    if (IS_SEQUENCE(_arg_list_9111)){
            _5245 = SEQ_PTR(_arg_list_9111)->length;
    }
    else {
        _5245 = 1;
    }
    _5246 = (_argn_9128 > _5245);
    _5245 = NOVALUE;
    if (_5246 == 0)
    {
        DeRef(_5246);
        _5246 = NOVALUE;
        goto L22; // [1273] 1319
    }
    else{
        DeRef(_5246);
        _5246 = NOVALUE;
    }
L21: 

    /**     				if length(envvar) > 0 then*/
    if (IS_SEQUENCE(_envvar_9139)){
            _5247 = SEQ_PTR(_envvar_9139)->length;
    }
    else {
        _5247 = 1;
    }
    if (_5247 <= 0)
    goto L23; // [1284] 1303

    /**     					argtext = envvar*/
    Ref(_envvar_9139);
    DeRef(_argtext_9298);
    _argtext_9298 = _envvar_9139;

    /** 	    				currargv = envvar*/
    Ref(_envvar_9139);
    DeRef(_currargv_9136);
    _currargv_9136 = _envvar_9139;
    goto L24; // [1300] 2780
L23: 

    /**     					argtext = ""*/
    RefDS(_5);
    DeRef(_argtext_9298);
    _argtext_9298 = _5;

    /** 	    				currargv =""*/
    RefDS(_5);
    DeRef(_currargv_9136);
    _currargv_9136 = _5;
    goto L24; // [1316] 2780
L22: 

    /** 					if string(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5249 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    Ref(_5249);
    _5250 = _11string(_5249);
    _5249 = NOVALUE;
    if (_5250 == 0) {
        DeRef(_5250);
        _5250 = NOVALUE;
        goto L25; // [1329] 1379
    }
    else {
        if (!IS_ATOM_INT(_5250) && DBL_PTR(_5250)->dbl == 0.0){
            DeRef(_5250);
            _5250 = NOVALUE;
            goto L25; // [1329] 1379
        }
        DeRef(_5250);
        _5250 = NOVALUE;
    }
    DeRef(_5250);
    _5250 = NOVALUE;

    /** 						if length(idname) > 0 then*/
    if (IS_SEQUENCE(_idname_9137)){
            _5251 = SEQ_PTR(_idname_9137)->length;
    }
    else {
        _5251 = 1;
    }
    if (_5251 <= 0)
    goto L26; // [1339] 1367

    /** 							argtext = arg_list[argn][length(idname) + 1 .. $]*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5253 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (IS_SEQUENCE(_idname_9137)){
            _5254 = SEQ_PTR(_idname_9137)->length;
    }
    else {
        _5254 = 1;
    }
    _5255 = _5254 + 1;
    _5254 = NOVALUE;
    if (IS_SEQUENCE(_5253)){
            _5256 = SEQ_PTR(_5253)->length;
    }
    else {
        _5256 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_5253, _5255, _5256);
    _5253 = NOVALUE;
    goto L27; // [1364] 2773
L26: 

    /** 							argtext = arg_list[argn]*/
    DeRef(_argtext_9298);
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _argtext_9298 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    Ref(_argtext_9298);
    goto L27; // [1376] 2773
L25: 

    /** 					elsif integer(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5259 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (IS_ATOM_INT(_5259))
    _5260 = 1;
    else if (IS_ATOM_DBL(_5259))
    _5260 = IS_ATOM_INT(DoubleToInt(_5259));
    else
    _5260 = 0;
    _5259 = NOVALUE;
    if (_5260 == 0)
    {
        _5260 = NOVALUE;
        goto L28; // [1388] 1937
    }
    else{
        _5260 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_9134 == 0)
    {
        goto L29; // [1395] 1419
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(arg_list[argn]))}*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5261 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    Ref(_5261);
    _5262 = _18abs(_5261);
    _5261 = NOVALUE;
    _5263 = binary_op(AND_BITS, _1573, _5262);
    DeRef(_5262);
    _5262 = NOVALUE;
    _0 = _argtext_9298;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5263;
    _argtext_9298 = MAKE_SEQ(_1);
    DeRef(_0);
    _5263 = NOVALUE;
    goto L27; // [1416] 2773
L29: 

    /** 						elsif bwz != 0 and arg_list[argn] = 0 then*/
    _5265 = (_bwz_9122 != 0);
    if (_5265 == 0) {
        goto L2A; // [1427] 1454
    }
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5267 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (IS_ATOM_INT(_5267)) {
        _5268 = (_5267 == 0);
    }
    else {
        _5268 = binary_op(EQUALS, _5267, 0);
    }
    _5267 = NOVALUE;
    if (_5268 == 0) {
        DeRef(_5268);
        _5268 = NOVALUE;
        goto L2A; // [1440] 1454
    }
    else {
        if (!IS_ATOM_INT(_5268) && DBL_PTR(_5268)->dbl == 0.0){
            DeRef(_5268);
            _5268 = NOVALUE;
            goto L2A; // [1440] 1454
        }
        DeRef(_5268);
        _5268 = NOVALUE;
    }
    DeRef(_5268);
    _5268 = NOVALUE;

    /** 							argtext = repeat(' ', width)*/
    DeRef(_argtext_9298);
    _argtext_9298 = Repeat(32, _width_9125);
    goto L27; // [1451] 2773
L2A: 

    /** 						elsif binout = 1 then*/
    if (_binout_9132 != 1)
    goto L2B; // [1458] 1601

    /** 							argtext = stdseq:reverse( convert:int_to_bits(arg_list[argn], 32)) + '0'*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5271 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    Ref(_5271);
    _5272 = _13int_to_bits(_5271, 32);
    _5271 = NOVALUE;
    _5273 = _21reverse(_5272, 1, 0);
    _5272 = NOVALUE;
    DeRef(_argtext_9298);
    if (IS_ATOM_INT(_5273)) {
        _argtext_9298 = _5273 + 48;
        if ((long)((unsigned long)_argtext_9298 + (unsigned long)HIGH_BITS) >= 0) 
        _argtext_9298 = NewDouble((double)_argtext_9298);
    }
    else {
        _argtext_9298 = binary_op(PLUS, _5273, 48);
    }
    DeRef(_5273);
    _5273 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5275 = (_zfill_9121 != 0);
    if (_5275 == 0) {
        goto L2C; // [1493] 1539
    }
    _5277 = (_width_9125 > 0);
    if (_5277 == 0)
    {
        DeRef(_5277);
        _5277 = NOVALUE;
        goto L2C; // [1504] 1539
    }
    else{
        DeRef(_5277);
        _5277 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5278 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5278 = 1;
    }
    if (_width_9125 <= _5278)
    goto L27; // [1514] 2773

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5280 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5280 = 1;
    }
    _5281 = _width_9125 - _5280;
    _5280 = NOVALUE;
    _5282 = Repeat(48, _5281);
    _5281 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _5282, _argtext_9298);
    DeRefDS(_5282);
    _5282 = NOVALUE;
    DeRef(_5282);
    _5282 = NOVALUE;
    goto L27; // [1536] 2773
L2C: 

    /** 								count = 1*/
    _count_9142 = 1;

    /** 								while count < length(argtext) and argtext[count] = '0' do*/
L2D: 
    if (IS_SEQUENCE(_argtext_9298)){
            _5284 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5284 = 1;
    }
    _5285 = (_count_9142 < _5284);
    _5284 = NOVALUE;
    if (_5285 == 0) {
        goto L2E; // [1558] 1587
    }
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5287 = (int)*(((s1_ptr)_2)->base + _count_9142);
    if (IS_ATOM_INT(_5287)) {
        _5288 = (_5287 == 48);
    }
    else {
        _5288 = binary_op(EQUALS, _5287, 48);
    }
    _5287 = NOVALUE;
    if (_5288 <= 0) {
        if (_5288 == 0) {
            DeRef(_5288);
            _5288 = NOVALUE;
            goto L2E; // [1571] 1587
        }
        else {
            if (!IS_ATOM_INT(_5288) && DBL_PTR(_5288)->dbl == 0.0){
                DeRef(_5288);
                _5288 = NOVALUE;
                goto L2E; // [1571] 1587
            }
            DeRef(_5288);
            _5288 = NOVALUE;
        }
    }
    DeRef(_5288);
    _5288 = NOVALUE;

    /** 									count += 1*/
    _count_9142 = _count_9142 + 1;

    /** 								end while*/
    goto L2D; // [1584] 1551
L2E: 

    /** 								argtext = argtext[count .. $]*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5290 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5290 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, _count_9142, _5290);
    goto L27; // [1598] 2773
L2B: 

    /** 						elsif hexout = 0 then*/
    if (_hexout_9131 != 0)
    goto L2F; // [1605] 1871

    /** 							argtext = sprintf("%d", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5293 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    DeRef(_argtext_9298);
    _argtext_9298 = EPrintf(-9999999, _952, _5293);
    _5293 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5295 = (_zfill_9121 != 0);
    if (_5295 == 0) {
        goto L30; // [1627] 1724
    }
    _5297 = (_width_9125 > 0);
    if (_5297 == 0)
    {
        DeRef(_5297);
        _5297 = NOVALUE;
        goto L30; // [1638] 1724
    }
    else{
        DeRef(_5297);
        _5297 = NOVALUE;
    }

    /** 								if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5298 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5298, 45)){
        _5298 = NOVALUE;
        goto L31; // [1647] 1693
    }
    _5298 = NOVALUE;

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5300 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5300 = 1;
    }
    if (_width_9125 <= _5300)
    goto L32; // [1658] 1723

    /** 										argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5302 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5302 = 1;
    }
    _5303 = _width_9125 - _5302;
    _5302 = NOVALUE;
    _5304 = Repeat(48, _5303);
    _5303 = NOVALUE;
    if (IS_SEQUENCE(_argtext_9298)){
            _5305 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5305 = 1;
    }
    rhs_slice_target = (object_ptr)&_5306;
    RHS_Slice(_argtext_9298, 2, _5305);
    {
        int concat_list[3];

        concat_list[0] = _5306;
        concat_list[1] = _5304;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5306);
    _5306 = NOVALUE;
    DeRefDS(_5304);
    _5304 = NOVALUE;
    goto L32; // [1690] 1723
L31: 

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5308 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5308 = 1;
    }
    if (_width_9125 <= _5308)
    goto L33; // [1700] 1722

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5310 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5310 = 1;
    }
    _5311 = _width_9125 - _5310;
    _5310 = NOVALUE;
    _5312 = Repeat(48, _5311);
    _5311 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _5312, _argtext_9298);
    DeRefDS(_5312);
    _5312 = NOVALUE;
    DeRef(_5312);
    _5312 = NOVALUE;
L33: 
L32: 
L30: 

    /** 							if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5314 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (binary_op_a(LESSEQ, _5314, 0)){
        _5314 = NOVALUE;
        goto L34; // [1730] 1778
    }
    _5314 = NOVALUE;

    /** 								if psign then*/
    if (_psign_9119 == 0)
    {
        goto L27; // [1738] 2773
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_9121 != 0)
    goto L35; // [1743] 1756

    /** 										argtext = '+' & argtext*/
    Prepend(&_argtext_9298, _argtext_9298, 43);
    goto L27; // [1753] 2773
L35: 

    /** 									elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5318 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5318, 48)){
        _5318 = NOVALUE;
        goto L27; // [1762] 2773
    }
    _5318 = NOVALUE;

    /** 										argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_9298 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [1775] 2773
L34: 

    /** 							elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5320 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (binary_op_a(GREATEREQ, _5320, 0)){
        _5320 = NOVALUE;
        goto L27; // [1784] 2773
    }
    _5320 = NOVALUE;

    /** 								if msign then*/
    if (_msign_9120 == 0)
    {
        goto L27; // [1792] 2773
    }
    else{
    }

    /** 									if zfill = 0 then*/
    if (_zfill_9121 != 0)
    goto L36; // [1797] 1820

    /** 										argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5323 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5323 = 1;
    }
    rhs_slice_target = (object_ptr)&_5324;
    RHS_Slice(_argtext_9298, 2, _5323);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5324;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5324);
    _5324 = NOVALUE;
    goto L27; // [1817] 2773
L36: 

    /** 										if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5327 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5327, 48)){
        _5327 = NOVALUE;
        goto L37; // [1826] 1849
    }
    _5327 = NOVALUE;

    /** 											argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5329 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5329 = 1;
    }
    rhs_slice_target = (object_ptr)&_5330;
    RHS_Slice(_argtext_9298, 3, _5329);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5330;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5330);
    _5330 = NOVALUE;
    goto L27; // [1846] 2773
L37: 

    /** 											argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5332 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5332 = 1;
    }
    rhs_slice_target = (object_ptr)&_5333;
    RHS_Slice(_argtext_9298, 2, _5332);
    Append(&_argtext_9298, _5333, 41);
    DeRefDS(_5333);
    _5333 = NOVALUE;
    goto L27; // [1868] 2773
L2F: 

    /** 							argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5336 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    DeRef(_argtext_9298);
    _argtext_9298 = EPrintf(-9999999, _5335, _5336);
    _5336 = NOVALUE;

    /** 							if zfill != 0 and width > 0 then*/
    _5338 = (_zfill_9121 != 0);
    if (_5338 == 0) {
        goto L27; // [1889] 2773
    }
    _5340 = (_width_9125 > 0);
    if (_5340 == 0)
    {
        DeRef(_5340);
        _5340 = NOVALUE;
        goto L27; // [1900] 2773
    }
    else{
        DeRef(_5340);
        _5340 = NOVALUE;
    }

    /** 								if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5341 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5341 = 1;
    }
    if (_width_9125 <= _5341)
    goto L27; // [1910] 2773

    /** 									argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5343 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5343 = 1;
    }
    _5344 = _width_9125 - _5343;
    _5343 = NOVALUE;
    _5345 = Repeat(48, _5344);
    _5344 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _5345, _argtext_9298);
    DeRefDS(_5345);
    _5345 = NOVALUE;
    DeRef(_5345);
    _5345 = NOVALUE;
    goto L27; // [1934] 2773
L28: 

    /** 					elsif atom(arg_list[argn]) then*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5347 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    _5348 = IS_ATOM(_5347);
    _5347 = NOVALUE;
    if (_5348 == 0)
    {
        _5348 = NOVALUE;
        goto L38; // [1946] 2351
    }
    else{
        _5348 = NOVALUE;
    }

    /** 						if istext then*/
    if (_istext_9134 == 0)
    {
        goto L39; // [1953] 1980
    }
    else{
    }

    /** 							argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(arg_list[argn])))}*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5349 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (IS_ATOM_INT(_5349))
    _5350 = e_floor(_5349);
    else
    _5350 = unary_op(FLOOR, _5349);
    _5349 = NOVALUE;
    _5351 = _18abs(_5350);
    _5350 = NOVALUE;
    _5352 = binary_op(AND_BITS, _1573, _5351);
    DeRef(_5351);
    _5351 = NOVALUE;
    _0 = _argtext_9298;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5352;
    _argtext_9298 = MAKE_SEQ(_1);
    DeRef(_0);
    _5352 = NOVALUE;
    goto L27; // [1977] 2773
L39: 

    /** 							if hexout then*/
    if (_hexout_9131 == 0)
    {
        goto L3A; // [1984] 2052
    }
    else{
    }

    /** 								argtext = sprintf("%x", arg_list[argn])*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5354 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    DeRef(_argtext_9298);
    _argtext_9298 = EPrintf(-9999999, _5335, _5354);
    _5354 = NOVALUE;

    /** 								if zfill != 0 and width > 0 then*/
    _5356 = (_zfill_9121 != 0);
    if (_5356 == 0) {
        goto L27; // [2005] 2773
    }
    _5358 = (_width_9125 > 0);
    if (_5358 == 0)
    {
        DeRef(_5358);
        _5358 = NOVALUE;
        goto L27; // [2016] 2773
    }
    else{
        DeRef(_5358);
        _5358 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5359 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5359 = 1;
    }
    if (_width_9125 <= _5359)
    goto L27; // [2026] 2773

    /** 										argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5361 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5361 = 1;
    }
    _5362 = _width_9125 - _5361;
    _5361 = NOVALUE;
    _5363 = Repeat(48, _5362);
    _5362 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _5363, _argtext_9298);
    DeRefDS(_5363);
    _5363 = NOVALUE;
    DeRef(_5363);
    _5363 = NOVALUE;
    goto L27; // [2049] 2773
L3A: 

    /** 								argtext = trim(sprintf("%15.15g", arg_list[argn]))*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5366 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    _5367 = EPrintf(-9999999, _5365, _5366);
    _5366 = NOVALUE;
    RefDS(_3967);
    _0 = _argtext_9298;
    _argtext_9298 = _12trim(_5367, _3967, 0);
    DeRef(_0);
    _5367 = NOVALUE;

    /** 								while ep != 0 with entry do*/
    goto L3B; // [2072] 2095
L3C: 
    if (_ep_9140 == 0)
    goto L3D; // [2077] 2109

    /** 									argtext = remove(argtext, ep+2)*/
    _5370 = _ep_9140 + 2;
    if ((long)((unsigned long)_5370 + (unsigned long)HIGH_BITS) >= 0) 
    _5370 = NewDouble((double)_5370);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_9298);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5370)) ? _5370 : (long)(DBL_PTR(_5370)->dbl);
        int stop = (IS_ATOM_INT(_5370)) ? _5370 : (long)(DBL_PTR(_5370)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_9298), start, &_argtext_9298 );
            }
            else Tail(SEQ_PTR(_argtext_9298), stop+1, &_argtext_9298);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_9298), start, &_argtext_9298);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_9298 = Remove_elements(start, stop, (SEQ_PTR(_argtext_9298)->ref == 1));
        }
    }
    DeRef(_5370);
    _5370 = NOVALUE;
    _5370 = NOVALUE;

    /** 								entry*/
L3B: 

    /** 									ep = match("e+0", argtext)*/
    _ep_9140 = e_match_from(_5372, _argtext_9298, 1);

    /** 								end while*/
    goto L3C; // [2106] 2075
L3D: 

    /** 								if zfill != 0 and width > 0 then*/
    _5374 = (_zfill_9121 != 0);
    if (_5374 == 0) {
        goto L3E; // [2117] 2202
    }
    _5376 = (_width_9125 > 0);
    if (_5376 == 0)
    {
        DeRef(_5376);
        _5376 = NOVALUE;
        goto L3E; // [2128] 2202
    }
    else{
        DeRef(_5376);
        _5376 = NOVALUE;
    }

    /** 									if width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5377 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5377 = 1;
    }
    if (_width_9125 <= _5377)
    goto L3F; // [2138] 2201

    /** 										if argtext[1] = '-' then*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5379 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5379, 45)){
        _5379 = NOVALUE;
        goto L40; // [2148] 2182
    }
    _5379 = NOVALUE;

    /** 											argtext = '-' & repeat('0', width - length(argtext)) & argtext[2..$]*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5381 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5381 = 1;
    }
    _5382 = _width_9125 - _5381;
    _5381 = NOVALUE;
    _5383 = Repeat(48, _5382);
    _5382 = NOVALUE;
    if (IS_SEQUENCE(_argtext_9298)){
            _5384 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5384 = 1;
    }
    rhs_slice_target = (object_ptr)&_5385;
    RHS_Slice(_argtext_9298, 2, _5384);
    {
        int concat_list[3];

        concat_list[0] = _5385;
        concat_list[1] = _5383;
        concat_list[2] = 45;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5385);
    _5385 = NOVALUE;
    DeRefDS(_5383);
    _5383 = NOVALUE;
    goto L41; // [2179] 2200
L40: 

    /** 											argtext = repeat('0', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5387 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5387 = 1;
    }
    _5388 = _width_9125 - _5387;
    _5387 = NOVALUE;
    _5389 = Repeat(48, _5388);
    _5388 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _5389, _argtext_9298);
    DeRefDS(_5389);
    _5389 = NOVALUE;
    DeRef(_5389);
    _5389 = NOVALUE;
L41: 
L3F: 
L3E: 

    /** 								if arg_list[argn] > 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5391 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (binary_op_a(LESSEQ, _5391, 0)){
        _5391 = NOVALUE;
        goto L42; // [2208] 2256
    }
    _5391 = NOVALUE;

    /** 									if psign  then*/
    if (_psign_9119 == 0)
    {
        goto L27; // [2216] 2773
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_9121 != 0)
    goto L43; // [2221] 2234

    /** 											argtext = '+' & argtext*/
    Prepend(&_argtext_9298, _argtext_9298, 43);
    goto L27; // [2231] 2773
L43: 

    /** 										elsif argtext[1] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5395 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _5395, 48)){
        _5395 = NOVALUE;
        goto L27; // [2240] 2773
    }
    _5395 = NOVALUE;

    /** 											argtext[1] = '+'*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_9298 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 43;
    DeRef(_1);
    goto L27; // [2253] 2773
L42: 

    /** 								elsif arg_list[argn] < 0 then*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5397 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (binary_op_a(GREATEREQ, _5397, 0)){
        _5397 = NOVALUE;
        goto L27; // [2262] 2773
    }
    _5397 = NOVALUE;

    /** 									if msign then*/
    if (_msign_9120 == 0)
    {
        goto L27; // [2270] 2773
    }
    else{
    }

    /** 										if zfill = 0 then*/
    if (_zfill_9121 != 0)
    goto L44; // [2275] 2298

    /** 											argtext = '(' & argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5400 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5400 = 1;
    }
    rhs_slice_target = (object_ptr)&_5401;
    RHS_Slice(_argtext_9298, 2, _5400);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5401;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5401);
    _5401 = NOVALUE;
    goto L27; // [2295] 2773
L44: 

    /** 											if argtext[2] = '0' then*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5403 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(NOTEQ, _5403, 48)){
        _5403 = NOVALUE;
        goto L45; // [2304] 2327
    }
    _5403 = NOVALUE;

    /** 												argtext = '(' & argtext[3..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5405 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5405 = 1;
    }
    rhs_slice_target = (object_ptr)&_5406;
    RHS_Slice(_argtext_9298, 3, _5405);
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5406;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5406);
    _5406 = NOVALUE;
    goto L27; // [2324] 2773
L45: 

    /** 												argtext = argtext[2..$] & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5408 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5408 = 1;
    }
    rhs_slice_target = (object_ptr)&_5409;
    RHS_Slice(_argtext_9298, 2, _5408);
    Append(&_argtext_9298, _5409, 41);
    DeRefDS(_5409);
    _5409 = NOVALUE;
    goto L27; // [2348] 2773
L38: 

    /** 						if alt != 0 and length(arg_list[argn]) = 2 then*/
    _5411 = (_alt_9124 != 0);
    if (_5411 == 0) {
        goto L46; // [2359] 2680
    }
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5413 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    if (IS_SEQUENCE(_5413)){
            _5414 = SEQ_PTR(_5413)->length;
    }
    else {
        _5414 = 1;
    }
    _5413 = NOVALUE;
    _5415 = (_5414 == 2);
    _5414 = NOVALUE;
    if (_5415 == 0)
    {
        DeRef(_5415);
        _5415 = NOVALUE;
        goto L46; // [2375] 2680
    }
    else{
        DeRef(_5415);
        _5415 = NOVALUE;
    }

    /** 							object tempv*/

    /** 							if atom(prevargv) then*/
    _5416 = IS_ATOM(_prevargv_9135);
    if (_5416 == 0)
    {
        _5416 = NOVALUE;
        goto L47; // [2385] 2421
    }
    else{
        _5416 = NOVALUE;
    }

    /** 								if prevargv != 1 then*/
    if (binary_op_a(EQUALS, _prevargv_9135, 1)){
        goto L48; // [2390] 2407
    }

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5418 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    DeRef(_tempv_9534);
    _2 = (int)SEQ_PTR(_5418);
    _tempv_9534 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_9534);
    _5418 = NOVALUE;
    goto L49; // [2404] 2455
L48: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5420 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    DeRef(_tempv_9534);
    _2 = (int)SEQ_PTR(_5420);
    _tempv_9534 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_9534);
    _5420 = NOVALUE;
    goto L49; // [2418] 2455
L47: 

    /** 								if length(prevargv) = 0 then*/
    if (IS_SEQUENCE(_prevargv_9135)){
            _5422 = SEQ_PTR(_prevargv_9135)->length;
    }
    else {
        _5422 = 1;
    }
    if (_5422 != 0)
    goto L4A; // [2426] 2443

    /** 									tempv = arg_list[argn][1]*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5424 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    DeRef(_tempv_9534);
    _2 = (int)SEQ_PTR(_5424);
    _tempv_9534 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_tempv_9534);
    _5424 = NOVALUE;
    goto L4B; // [2440] 2454
L4A: 

    /** 									tempv = arg_list[argn][2]*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5426 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    DeRef(_tempv_9534);
    _2 = (int)SEQ_PTR(_5426);
    _tempv_9534 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_tempv_9534);
    _5426 = NOVALUE;
L4B: 
L49: 

    /** 							if string(tempv) then*/
    Ref(_tempv_9534);
    _5428 = _11string(_tempv_9534);
    if (_5428 == 0) {
        DeRef(_5428);
        _5428 = NOVALUE;
        goto L4C; // [2463] 2476
    }
    else {
        if (!IS_ATOM_INT(_5428) && DBL_PTR(_5428)->dbl == 0.0){
            DeRef(_5428);
            _5428 = NOVALUE;
            goto L4C; // [2463] 2476
        }
        DeRef(_5428);
        _5428 = NOVALUE;
    }
    DeRef(_5428);
    _5428 = NOVALUE;

    /** 								argtext = tempv*/
    Ref(_tempv_9534);
    DeRef(_argtext_9298);
    _argtext_9298 = _tempv_9534;
    goto L4D; // [2473] 2675
L4C: 

    /** 							elsif integer(tempv) then*/
    if (IS_ATOM_INT(_tempv_9534))
    _5429 = 1;
    else if (IS_ATOM_DBL(_tempv_9534))
    _5429 = IS_ATOM_INT(DoubleToInt(_tempv_9534));
    else
    _5429 = 0;
    if (_5429 == 0)
    {
        _5429 = NOVALUE;
        goto L4E; // [2481] 2547
    }
    else{
        _5429 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_9134 == 0)
    {
        goto L4F; // [2486] 2506
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(tempv))}*/
    Ref(_tempv_9534);
    _5430 = _18abs(_tempv_9534);
    _5431 = binary_op(AND_BITS, _1573, _5430);
    DeRef(_5430);
    _5430 = NOVALUE;
    _0 = _argtext_9298;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5431;
    _argtext_9298 = MAKE_SEQ(_1);
    DeRef(_0);
    _5431 = NOVALUE;
    goto L4D; // [2503] 2675
L4F: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5433 = (_bwz_9122 != 0);
    if (_5433 == 0) {
        goto L50; // [2514] 2537
    }
    if (IS_ATOM_INT(_tempv_9534)) {
        _5435 = (_tempv_9534 == 0);
    }
    else {
        _5435 = binary_op(EQUALS, _tempv_9534, 0);
    }
    if (_5435 == 0) {
        DeRef(_5435);
        _5435 = NOVALUE;
        goto L50; // [2523] 2537
    }
    else {
        if (!IS_ATOM_INT(_5435) && DBL_PTR(_5435)->dbl == 0.0){
            DeRef(_5435);
            _5435 = NOVALUE;
            goto L50; // [2523] 2537
        }
        DeRef(_5435);
        _5435 = NOVALUE;
    }
    DeRef(_5435);
    _5435 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_9298);
    _argtext_9298 = Repeat(32, _width_9125);
    goto L4D; // [2534] 2675
L50: 

    /** 									argtext = sprintf("%d", tempv)*/
    DeRef(_argtext_9298);
    _argtext_9298 = EPrintf(-9999999, _952, _tempv_9534);
    goto L4D; // [2544] 2675
L4E: 

    /** 							elsif atom(tempv) then*/
    _5438 = IS_ATOM(_tempv_9534);
    if (_5438 == 0)
    {
        _5438 = NOVALUE;
        goto L51; // [2552] 2629
    }
    else{
        _5438 = NOVALUE;
    }

    /** 								if istext then*/
    if (_istext_9134 == 0)
    {
        goto L52; // [2557] 2580
    }
    else{
    }

    /** 									argtext = {and_bits(0xFFFF_FFFF, math:abs(floor(tempv)))}*/
    if (IS_ATOM_INT(_tempv_9534))
    _5439 = e_floor(_tempv_9534);
    else
    _5439 = unary_op(FLOOR, _tempv_9534);
    _5440 = _18abs(_5439);
    _5439 = NOVALUE;
    _5441 = binary_op(AND_BITS, _1573, _5440);
    DeRef(_5440);
    _5440 = NOVALUE;
    _0 = _argtext_9298;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _5441;
    _argtext_9298 = MAKE_SEQ(_1);
    DeRef(_0);
    _5441 = NOVALUE;
    goto L4D; // [2577] 2675
L52: 

    /** 								elsif bwz != 0 and tempv = 0 then*/
    _5443 = (_bwz_9122 != 0);
    if (_5443 == 0) {
        goto L53; // [2588] 2611
    }
    if (IS_ATOM_INT(_tempv_9534)) {
        _5445 = (_tempv_9534 == 0);
    }
    else {
        _5445 = binary_op(EQUALS, _tempv_9534, 0);
    }
    if (_5445 == 0) {
        DeRef(_5445);
        _5445 = NOVALUE;
        goto L53; // [2597] 2611
    }
    else {
        if (!IS_ATOM_INT(_5445) && DBL_PTR(_5445)->dbl == 0.0){
            DeRef(_5445);
            _5445 = NOVALUE;
            goto L53; // [2597] 2611
        }
        DeRef(_5445);
        _5445 = NOVALUE;
    }
    DeRef(_5445);
    _5445 = NOVALUE;

    /** 									argtext = repeat(' ', width)*/
    DeRef(_argtext_9298);
    _argtext_9298 = Repeat(32, _width_9125);
    goto L4D; // [2608] 2675
L53: 

    /** 									argtext = trim(sprintf("%15.15g", tempv))*/
    _5447 = EPrintf(-9999999, _5365, _tempv_9534);
    RefDS(_3967);
    _0 = _argtext_9298;
    _argtext_9298 = _12trim(_5447, _3967, 0);
    DeRef(_0);
    _5447 = NOVALUE;
    goto L4D; // [2626] 2675
L51: 

    /** 								argtext = pretty:pretty_sprint( tempv,*/
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_952);
    *((int *)(_2+20)) = _952;
    RefDS(_5449);
    *((int *)(_2+24)) = _5449;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5450 = MAKE_SEQ(_1);
    DeRef(_options_inlined_pretty_sprint_at_2645_9588);
    _options_inlined_pretty_sprint_at_2645_9588 = _5450;
    _5450 = NOVALUE;

    /** 	pretty_printing = 0*/
    _24pretty_printing_7550 = 0;

    /** 	pretty( x, options )*/
    Ref(_tempv_9534);
    RefDS(_options_inlined_pretty_sprint_at_2645_9588);
    _24pretty(_tempv_9534, _options_inlined_pretty_sprint_at_2645_9588);

    /** 	return pretty_line*/
    RefDS(_24pretty_line_7553);
    DeRef(_argtext_9298);
    _argtext_9298 = _24pretty_line_7553;
    DeRef(_options_inlined_pretty_sprint_at_2645_9588);
    _options_inlined_pretty_sprint_at_2645_9588 = NOVALUE;
L4D: 
    DeRef(_tempv_9534);
    _tempv_9534 = NOVALUE;
    goto L54; // [2677] 2758
L46: 

    /** 							argtext = pretty:pretty_sprint( arg_list[argn],*/
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _5451 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    _1 = NewS1(10);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 1;
    *((int *)(_2+16)) = 1000;
    RefDS(_952);
    *((int *)(_2+20)) = _952;
    RefDS(_5449);
    *((int *)(_2+24)) = _5449;
    *((int *)(_2+28)) = 32;
    *((int *)(_2+32)) = 127;
    *((int *)(_2+36)) = 1;
    *((int *)(_2+40)) = 0;
    _5452 = MAKE_SEQ(_1);
    Ref(_5451);
    DeRef(_x_inlined_pretty_sprint_at_2700_9594);
    _x_inlined_pretty_sprint_at_2700_9594 = _5451;
    _5451 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2703_9595);
    _options_inlined_pretty_sprint_at_2703_9595 = _5452;
    _5452 = NOVALUE;

    /** 	pretty_printing = 0*/
    _24pretty_printing_7550 = 0;

    /** 	pretty( x, options )*/
    Ref(_x_inlined_pretty_sprint_at_2700_9594);
    RefDS(_options_inlined_pretty_sprint_at_2703_9595);
    _24pretty(_x_inlined_pretty_sprint_at_2700_9594, _options_inlined_pretty_sprint_at_2703_9595);

    /** 	return pretty_line*/
    RefDS(_24pretty_line_7553);
    DeRef(_argtext_9298);
    _argtext_9298 = _24pretty_line_7553;
    DeRef(_x_inlined_pretty_sprint_at_2700_9594);
    _x_inlined_pretty_sprint_at_2700_9594 = NOVALUE;
    DeRef(_options_inlined_pretty_sprint_at_2703_9595);
    _options_inlined_pretty_sprint_at_2703_9595 = NOVALUE;

    /** 						while ep != 0 with entry do*/
    goto L54; // [2735] 2758
L55: 
    if (_ep_9140 == 0)
    goto L56; // [2740] 2772

    /** 							argtext = remove(argtext, ep+2)*/
    _5454 = _ep_9140 + 2;
    if ((long)((unsigned long)_5454 + (unsigned long)HIGH_BITS) >= 0) 
    _5454 = NewDouble((double)_5454);
    {
        s1_ptr assign_space = SEQ_PTR(_argtext_9298);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_5454)) ? _5454 : (long)(DBL_PTR(_5454)->dbl);
        int stop = (IS_ATOM_INT(_5454)) ? _5454 : (long)(DBL_PTR(_5454)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_argtext_9298), start, &_argtext_9298 );
            }
            else Tail(SEQ_PTR(_argtext_9298), stop+1, &_argtext_9298);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_argtext_9298), start, &_argtext_9298);
        }
        else {
            assign_slice_seq = &assign_space;
            _argtext_9298 = Remove_elements(start, stop, (SEQ_PTR(_argtext_9298)->ref == 1));
        }
    }
    DeRef(_5454);
    _5454 = NOVALUE;
    _5454 = NOVALUE;

    /** 						entry*/
L54: 

    /** 							ep = match("e+0", argtext)*/
    _ep_9140 = e_match_from(_5372, _argtext_9298, 1);

    /** 						end while*/
    goto L55; // [2769] 2738
L56: 
L27: 

    /** 	    			currargv = arg_list[argn]*/
    DeRef(_currargv_9136);
    _2 = (int)SEQ_PTR(_arg_list_9111);
    _currargv_9136 = (int)*(((s1_ptr)_2)->base + _argn_9128);
    Ref(_currargv_9136);
L24: 

    /**     			if length(argtext) > 0 then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5458 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5458 = 1;
    }
    if (_5458 <= 0)
    goto L57; // [2785] 3737

    /**     				switch cap do*/
    _0 = _cap_9117;
    switch ( _0 ){ 

        /**     					case 'u' then*/
        case 117:

        /**     						argtext = upper(argtext)*/
        RefDS(_argtext_9298);
        _0 = _argtext_9298;
        _argtext_9298 = _12upper(_argtext_9298);
        DeRefDS(_0);
        goto L58; // [2810] 2878

        /**     					case 'l' then*/
        case 108:

        /**     						argtext = lower(argtext)*/
        RefDS(_argtext_9298);
        _0 = _argtext_9298;
        _argtext_9298 = _12lower(_argtext_9298);
        DeRefDS(_0);
        goto L58; // [2824] 2878

        /**     					case 'w' then*/
        case 119:

        /**     						argtext = proper(argtext)*/
        RefDS(_argtext_9298);
        _0 = _argtext_9298;
        _argtext_9298 = _12proper(_argtext_9298);
        DeRefDS(_0);
        goto L58; // [2838] 2878

        /**     					case 0 then*/
        case 0:

        /** 							cap = cap*/
        _cap_9117 = _cap_9117;
        goto L58; // [2851] 2878

        /**     					case else*/
        default:

        /**     						error:crash("logic error: 'cap' mode in format.")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_2860_9618);
        _msg_inlined_crash_at_2860_9618 = EPrintf(-9999999, _5465, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_2860_9618);

        /** end procedure*/
        goto L59; // [2872] 2875
L59: 
        DeRefi(_msg_inlined_crash_at_2860_9618);
        _msg_inlined_crash_at_2860_9618 = NOVALUE;
    ;}L58: 

    /** 					if atom(currargv) then*/
    _5466 = IS_ATOM(_currargv_9136);
    if (_5466 == 0)
    {
        _5466 = NOVALUE;
        goto L5A; // [2885] 3136
    }
    else{
        _5466 = NOVALUE;
    }

    /** 						if find('e', argtext) = 0 then*/
    _5467 = find_from(101, _argtext_9298, 1);
    if (_5467 != 0)
    goto L5B; // [2895] 3135

    /** 							pflag = 0*/
    _pflag_9141 = 0;

    /** 							if msign and currargv < 0 then*/
    if (_msign_9120 == 0) {
        goto L5C; // [2910] 2930
    }
    if (IS_ATOM_INT(_currargv_9136)) {
        _5470 = (_currargv_9136 < 0);
    }
    else {
        _5470 = binary_op(LESS, _currargv_9136, 0);
    }
    if (_5470 == 0) {
        DeRef(_5470);
        _5470 = NOVALUE;
        goto L5C; // [2919] 2930
    }
    else {
        if (!IS_ATOM_INT(_5470) && DBL_PTR(_5470)->dbl == 0.0){
            DeRef(_5470);
            _5470 = NOVALUE;
            goto L5C; // [2919] 2930
        }
        DeRef(_5470);
        _5470 = NOVALUE;
    }
    DeRef(_5470);
    _5470 = NOVALUE;

    /** 								pflag = 1*/
    _pflag_9141 = 1;
L5C: 

    /** 							if decs != -1 then*/
    if (_decs_9126 == -1)
    goto L5D; // [2934] 3134

    /** 								pos = find('.', argtext)*/
    _pos_9127 = find_from(46, _argtext_9298, 1);

    /** 								if pos then*/
    if (_pos_9127 == 0)
    {
        goto L5E; // [2949] 3079
    }
    else{
    }

    /** 									if decs = 0 then*/
    if (_decs_9126 != 0)
    goto L5F; // [2954] 2972

    /** 										argtext = argtext [1 .. pos-1 ]*/
    _5474 = _pos_9127 - 1;
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, 1, _5474);
    goto L60; // [2969] 3133
L5F: 

    /** 										pos = length(argtext) - pos - pflag*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5476 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5476 = 1;
    }
    _5477 = _5476 - _pos_9127;
    if ((long)((unsigned long)_5477 +(unsigned long) HIGH_BITS) >= 0){
        _5477 = NewDouble((double)_5477);
    }
    _5476 = NOVALUE;
    if (IS_ATOM_INT(_5477)) {
        _pos_9127 = _5477 - _pflag_9141;
    }
    else {
        _pos_9127 = NewDouble(DBL_PTR(_5477)->dbl - (double)_pflag_9141);
    }
    DeRef(_5477);
    _5477 = NOVALUE;
    if (!IS_ATOM_INT(_pos_9127)) {
        _1 = (long)(DBL_PTR(_pos_9127)->dbl);
        if (UNIQUE(DBL_PTR(_pos_9127)) && (DBL_PTR(_pos_9127)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_9127);
        _pos_9127 = _1;
    }

    /** 										if pos > decs then*/
    if (_pos_9127 <= _decs_9126)
    goto L61; // [2991] 3016

    /** 											argtext = argtext[ 1 .. $ - pos + decs ]*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5480 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5480 = 1;
    }
    _5481 = _5480 - _pos_9127;
    if ((long)((unsigned long)_5481 +(unsigned long) HIGH_BITS) >= 0){
        _5481 = NewDouble((double)_5481);
    }
    _5480 = NOVALUE;
    if (IS_ATOM_INT(_5481)) {
        _5482 = _5481 + _decs_9126;
    }
    else {
        _5482 = NewDouble(DBL_PTR(_5481)->dbl + (double)_decs_9126);
    }
    DeRef(_5481);
    _5481 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, 1, _5482);
    goto L60; // [3013] 3133
L61: 

    /** 										elsif pos < decs then*/
    if (_pos_9127 >= _decs_9126)
    goto L60; // [3018] 3133

    /** 											if pflag then*/
    if (_pflag_9141 == 0)
    {
        goto L62; // [3024] 3058
    }
    else{
    }

    /** 												argtext = argtext[ 1 .. $ - 1 ] & repeat('0', decs - pos) & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5485 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5485 = 1;
    }
    _5486 = _5485 - 1;
    _5485 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5487;
    RHS_Slice(_argtext_9298, 1, _5486);
    _5488 = _decs_9126 - _pos_9127;
    _5489 = Repeat(48, _5488);
    _5488 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _5489;
        concat_list[2] = _5487;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5489);
    _5489 = NOVALUE;
    DeRefDS(_5487);
    _5487 = NOVALUE;
    goto L60; // [3055] 3133
L62: 

    /** 												argtext = argtext & repeat('0', decs - pos)*/
    _5491 = _decs_9126 - _pos_9127;
    _5492 = Repeat(48, _5491);
    _5491 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _argtext_9298, _5492);
    DeRefDS(_5492);
    _5492 = NOVALUE;
    goto L60; // [3076] 3133
L5E: 

    /** 								elsif decs > 0 then*/
    if (_decs_9126 <= 0)
    goto L63; // [3081] 3132

    /** 									if pflag then*/
    if (_pflag_9141 == 0)
    {
        goto L64; // [3087] 3118
    }
    else{
    }

    /** 										argtext = argtext[1 .. $ - 1] & '.' & repeat('0', decs) & ')'*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5495 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5495 = 1;
    }
    _5496 = _5495 - 1;
    _5495 = NOVALUE;
    rhs_slice_target = (object_ptr)&_5497;
    RHS_Slice(_argtext_9298, 1, _5496);
    _5498 = Repeat(48, _decs_9126);
    {
        int concat_list[4];

        concat_list[0] = 41;
        concat_list[1] = _5498;
        concat_list[2] = 46;
        concat_list[3] = _5497;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 4);
    }
    DeRefDS(_5498);
    _5498 = NOVALUE;
    DeRefDS(_5497);
    _5497 = NOVALUE;
    goto L65; // [3115] 3131
L64: 

    /** 										argtext = argtext & '.' & repeat('0', decs)*/
    _5500 = Repeat(48, _decs_9126);
    {
        int concat_list[3];

        concat_list[0] = _5500;
        concat_list[1] = 46;
        concat_list[2] = _argtext_9298;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5500);
    _5500 = NOVALUE;
L65: 
L63: 
L60: 
L5D: 
L5B: 
L5A: 

    /**     				if align = 0 then*/
    if (_align_9118 != 0)
    goto L66; // [3140] 3171

    /**     					if atom(currargv) then*/
    _5503 = IS_ATOM(_currargv_9136);
    if (_5503 == 0)
    {
        _5503 = NOVALUE;
        goto L67; // [3149] 3162
    }
    else{
        _5503 = NOVALUE;
    }

    /**     						align = '>'*/
    _align_9118 = 62;
    goto L68; // [3159] 3170
L67: 

    /**     						align = '<'*/
    _align_9118 = 60;
L68: 
L66: 

    /**     				if atom(currargv) then*/
    _5504 = IS_ATOM(_currargv_9136);
    if (_5504 == 0)
    {
        _5504 = NOVALUE;
        goto L69; // [3176] 3427
    }
    else{
        _5504 = NOVALUE;
    }

    /** 	    				if tsep != 0 and zfill = 0 then*/
    _5505 = (_tsep_9133 != 0);
    if (_5505 == 0) {
        goto L6A; // [3187] 3424
    }
    _5507 = (_zfill_9121 == 0);
    if (_5507 == 0)
    {
        DeRef(_5507);
        _5507 = NOVALUE;
        goto L6A; // [3198] 3424
    }
    else{
        DeRef(_5507);
        _5507 = NOVALUE;
    }

    /** 	    					integer dpos*/

    /** 	    					integer dist*/

    /** 	    					integer bracketed*/

    /** 	    					if binout or hexout then*/
    if (_binout_9132 != 0) {
        goto L6B; // [3211] 3220
    }
    if (_hexout_9131 == 0)
    {
        goto L6C; // [3216] 3237
    }
    else{
    }
L6B: 

    /** 	    						dist = 4*/
    _dist_9681 = 4;

    /** 							psign = 0*/
    _psign_9119 = 0;
    goto L6D; // [3234] 3245
L6C: 

    /** 	    						dist = 3*/
    _dist_9681 = 3;
L6D: 

    /** 	    					bracketed = (argtext[1] = '(')*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    _5509 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_5509)) {
        _bracketed_9682 = (_5509 == 40);
    }
    else {
        _bracketed_9682 = binary_op(EQUALS, _5509, 40);
    }
    _5509 = NOVALUE;
    if (!IS_ATOM_INT(_bracketed_9682)) {
        _1 = (long)(DBL_PTR(_bracketed_9682)->dbl);
        if (UNIQUE(DBL_PTR(_bracketed_9682)) && (DBL_PTR(_bracketed_9682)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bracketed_9682);
        _bracketed_9682 = _1;
    }

    /** 	    					if bracketed then*/
    if (_bracketed_9682 == 0)
    {
        goto L6E; // [3261] 3279
    }
    else{
    }

    /** 	    						argtext = argtext[2 .. $-1]*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5511 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5511 = 1;
    }
    _5512 = _5511 - 1;
    _5511 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, 2, _5512);
L6E: 

    /** 	    					dpos = find('.', argtext)*/
    _dpos_9680 = find_from(46, _argtext_9298, 1);

    /** 	    					if dpos = 0 then*/
    if (_dpos_9680 != 0)
    goto L6F; // [3290] 3308

    /** 	    						dpos = length(argtext) + 1*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5516 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5516 = 1;
    }
    _dpos_9680 = _5516 + 1;
    _5516 = NOVALUE;
    goto L70; // [3305] 3322
L6F: 

    /** 	    						if tsep = '.' then*/
    if (_tsep_9133 != 46)
    goto L71; // [3310] 3321

    /** 	    							argtext[dpos] = ','*/
    _2 = (int)SEQ_PTR(_argtext_9298);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _argtext_9298 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _dpos_9680);
    _1 = *(int *)_2;
    *(int *)_2 = 44;
    DeRef(_1);
L71: 
L70: 

    /** 	    					while dpos > dist do*/
L72: 
    if (_dpos_9680 <= _dist_9681)
    goto L73; // [3329] 3409

    /** 	    						dpos -= dist*/
    _dpos_9680 = _dpos_9680 - _dist_9681;

    /** 	    						if dpos > 1 + (currargv < 0) * not msign + (currargv > 0) * psign then*/
    if (IS_ATOM_INT(_currargv_9136)) {
        _5521 = (_currargv_9136 < 0);
    }
    else {
        _5521 = binary_op(LESS, _currargv_9136, 0);
    }
    _5522 = (_msign_9120 == 0);
    if (IS_ATOM_INT(_5521)) {
        if (_5521 == (short)_5521 && _5522 <= INT15 && _5522 >= -INT15)
        _5523 = _5521 * _5522;
        else
        _5523 = NewDouble(_5521 * (double)_5522);
    }
    else {
        _5523 = binary_op(MULTIPLY, _5521, _5522);
    }
    DeRef(_5521);
    _5521 = NOVALUE;
    _5522 = NOVALUE;
    if (IS_ATOM_INT(_5523)) {
        _5524 = _5523 + 1;
        if (_5524 > MAXINT){
            _5524 = NewDouble((double)_5524);
        }
    }
    else
    _5524 = binary_op(PLUS, 1, _5523);
    DeRef(_5523);
    _5523 = NOVALUE;
    if (IS_ATOM_INT(_currargv_9136)) {
        _5525 = (_currargv_9136 > 0);
    }
    else {
        _5525 = binary_op(GREATER, _currargv_9136, 0);
    }
    if (IS_ATOM_INT(_5525)) {
        if (_5525 == (short)_5525 && _psign_9119 <= INT15 && _psign_9119 >= -INT15)
        _5526 = _5525 * _psign_9119;
        else
        _5526 = NewDouble(_5525 * (double)_psign_9119);
    }
    else {
        _5526 = binary_op(MULTIPLY, _5525, _psign_9119);
    }
    DeRef(_5525);
    _5525 = NOVALUE;
    if (IS_ATOM_INT(_5524) && IS_ATOM_INT(_5526)) {
        _5527 = _5524 + _5526;
        if ((long)((unsigned long)_5527 + (unsigned long)HIGH_BITS) >= 0) 
        _5527 = NewDouble((double)_5527);
    }
    else {
        _5527 = binary_op(PLUS, _5524, _5526);
    }
    DeRef(_5524);
    _5524 = NOVALUE;
    DeRef(_5526);
    _5526 = NOVALUE;
    if (binary_op_a(LESSEQ, _dpos_9680, _5527)){
        DeRef(_5527);
        _5527 = NOVALUE;
        goto L72; // [3374] 3327
    }
    DeRef(_5527);
    _5527 = NOVALUE;

    /** 	    							argtext = argtext[1.. dpos - 1] & tsep & argtext[dpos .. $]*/
    _5529 = _dpos_9680 - 1;
    rhs_slice_target = (object_ptr)&_5530;
    RHS_Slice(_argtext_9298, 1, _5529);
    if (IS_SEQUENCE(_argtext_9298)){
            _5531 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5531 = 1;
    }
    rhs_slice_target = (object_ptr)&_5532;
    RHS_Slice(_argtext_9298, _dpos_9680, _5531);
    {
        int concat_list[3];

        concat_list[0] = _5532;
        concat_list[1] = _tsep_9133;
        concat_list[2] = _5530;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5532);
    _5532 = NOVALUE;
    DeRefDS(_5530);
    _5530 = NOVALUE;

    /** 	    					end while*/
    goto L72; // [3406] 3327
L73: 

    /** 	    					if bracketed then*/
    if (_bracketed_9682 == 0)
    {
        goto L74; // [3411] 3423
    }
    else{
    }

    /** 	    						argtext = '(' & argtext & ')'*/
    {
        int concat_list[3];

        concat_list[0] = 41;
        concat_list[1] = _argtext_9298;
        concat_list[2] = 40;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
L74: 
L6A: 
L69: 

    /**     				if width <= 0 then*/
    if (_width_9125 > 0)
    goto L75; // [3431] 3443

    /**     					width = length(argtext)*/
    if (IS_SEQUENCE(_argtext_9298)){
            _width_9125 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _width_9125 = 1;
    }
L75: 

    /**     				if width < length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5537 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5537 = 1;
    }
    if (_width_9125 >= _5537)
    goto L76; // [3448] 3585

    /**     					if align = '>' then*/
    if (_align_9118 != 62)
    goto L77; // [3454] 3482

    /**     						argtext = argtext[ $ - width + 1 .. $]*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5540 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5540 = 1;
    }
    _5541 = _5540 - _width_9125;
    if ((long)((unsigned long)_5541 +(unsigned long) HIGH_BITS) >= 0){
        _5541 = NewDouble((double)_5541);
    }
    _5540 = NOVALUE;
    if (IS_ATOM_INT(_5541)) {
        _5542 = _5541 + 1;
        if (_5542 > MAXINT){
            _5542 = NewDouble((double)_5542);
        }
    }
    else
    _5542 = binary_op(PLUS, 1, _5541);
    DeRef(_5541);
    _5541 = NOVALUE;
    if (IS_SEQUENCE(_argtext_9298)){
            _5543 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5543 = 1;
    }
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, _5542, _5543);
    goto L78; // [3479] 3728
L77: 

    /**     					elsif align = 'c' then*/
    if (_align_9118 != 99)
    goto L79; // [3484] 3574

    /**     						pos = length(argtext) - width*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5546 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5546 = 1;
    }
    _pos_9127 = _5546 - _width_9125;
    _5546 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _5548 = (_pos_9127 % 2);
    if (_5548 != 0)
    goto L7A; // [3505] 3540

    /**     							pos = pos / 2*/
    if (_pos_9127 & 1) {
        _pos_9127 = NewDouble((_pos_9127 >> 1) + 0.5);
    }
    else
    _pos_9127 = _pos_9127 >> 1;
    if (!IS_ATOM_INT(_pos_9127)) {
        _1 = (long)(DBL_PTR(_pos_9127)->dbl);
        if (UNIQUE(DBL_PTR(_pos_9127)) && (DBL_PTR(_pos_9127)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_9127);
        _pos_9127 = _1;
    }

    /**     							argtext = argtext[ pos + 1 .. $ - pos ]*/
    _5551 = _pos_9127 + 1;
    if (_5551 > MAXINT){
        _5551 = NewDouble((double)_5551);
    }
    if (IS_SEQUENCE(_argtext_9298)){
            _5552 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5552 = 1;
    }
    _5553 = _5552 - _pos_9127;
    _5552 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, _5551, _5553);
    goto L78; // [3537] 3728
L7A: 

    /**     							pos = floor(pos / 2)*/
    _pos_9127 = _pos_9127 >> 1;

    /**     							argtext = argtext[ pos + 1 .. $ - pos - 1]*/
    _5556 = _pos_9127 + 1;
    if (_5556 > MAXINT){
        _5556 = NewDouble((double)_5556);
    }
    if (IS_SEQUENCE(_argtext_9298)){
            _5557 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5557 = 1;
    }
    _5558 = _5557 - _pos_9127;
    if ((long)((unsigned long)_5558 +(unsigned long) HIGH_BITS) >= 0){
        _5558 = NewDouble((double)_5558);
    }
    _5557 = NOVALUE;
    if (IS_ATOM_INT(_5558)) {
        _5559 = _5558 - 1;
    }
    else {
        _5559 = NewDouble(DBL_PTR(_5558)->dbl - (double)1);
    }
    DeRef(_5558);
    _5558 = NOVALUE;
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, _5556, _5559);
    goto L78; // [3571] 3728
L79: 

    /**     						argtext = argtext[ 1 .. width]*/
    rhs_slice_target = (object_ptr)&_argtext_9298;
    RHS_Slice(_argtext_9298, 1, _width_9125);
    goto L78; // [3582] 3728
L76: 

    /**     				elsif width > length(argtext) then*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5562 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5562 = 1;
    }
    if (_width_9125 <= _5562)
    goto L7B; // [3590] 3727

    /** 						if align = '>' then*/
    if (_align_9118 != 62)
    goto L7C; // [3596] 3620

    /** 							argtext = repeat(' ', width - length(argtext)) & argtext*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5565 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5565 = 1;
    }
    _5566 = _width_9125 - _5565;
    _5565 = NOVALUE;
    _5567 = Repeat(32, _5566);
    _5566 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _5567, _argtext_9298);
    DeRefDS(_5567);
    _5567 = NOVALUE;
    DeRef(_5567);
    _5567 = NOVALUE;
    goto L7D; // [3617] 3726
L7C: 

    /**     					elsif align = 'c' then*/
    if (_align_9118 != 99)
    goto L7E; // [3622] 3708

    /**     						pos = width - length(argtext)*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5570 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5570 = 1;
    }
    _pos_9127 = _width_9125 - _5570;
    _5570 = NOVALUE;

    /**     						if remainder(pos, 2) = 0 then*/
    _5572 = (_pos_9127 % 2);
    if (_5572 != 0)
    goto L7F; // [3643] 3676

    /**     							pos = pos / 2*/
    if (_pos_9127 & 1) {
        _pos_9127 = NewDouble((_pos_9127 >> 1) + 0.5);
    }
    else
    _pos_9127 = _pos_9127 >> 1;
    if (!IS_ATOM_INT(_pos_9127)) {
        _1 = (long)(DBL_PTR(_pos_9127)->dbl);
        if (UNIQUE(DBL_PTR(_pos_9127)) && (DBL_PTR(_pos_9127)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_9127);
        _pos_9127 = _1;
    }

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos)*/
    _5575 = Repeat(32, _pos_9127);
    _5576 = Repeat(32, _pos_9127);
    {
        int concat_list[3];

        concat_list[0] = _5576;
        concat_list[1] = _argtext_9298;
        concat_list[2] = _5575;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5576);
    _5576 = NOVALUE;
    DeRefDS(_5575);
    _5575 = NOVALUE;
    goto L7D; // [3673] 3726
L7F: 

    /**     							pos = floor(pos / 2)*/
    _pos_9127 = _pos_9127 >> 1;

    /**     							argtext = repeat(' ', pos) & argtext & repeat(' ', pos + 1)*/
    _5579 = Repeat(32, _pos_9127);
    _5580 = _pos_9127 + 1;
    _5581 = Repeat(32, _5580);
    _5580 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _5581;
        concat_list[1] = _argtext_9298;
        concat_list[2] = _5579;
        Concat_N((object_ptr)&_argtext_9298, concat_list, 3);
    }
    DeRefDS(_5581);
    _5581 = NOVALUE;
    DeRefDS(_5579);
    _5579 = NOVALUE;
    goto L7D; // [3705] 3726
L7E: 

    /** 							argtext = argtext & repeat(' ', width - length(argtext))*/
    if (IS_SEQUENCE(_argtext_9298)){
            _5583 = SEQ_PTR(_argtext_9298)->length;
    }
    else {
        _5583 = 1;
    }
    _5584 = _width_9125 - _5583;
    _5583 = NOVALUE;
    _5585 = Repeat(32, _5584);
    _5584 = NOVALUE;
    Concat((object_ptr)&_argtext_9298, _argtext_9298, _5585);
    DeRefDS(_5585);
    _5585 = NOVALUE;
L7D: 
L7B: 
L78: 

    /**     				result &= argtext*/
    Concat((object_ptr)&_result_9112, _result_9112, _argtext_9298);
    goto L80; // [3734] 3750
L57: 

    /**     				if spacer then*/
    if (_spacer_9123 == 0)
    {
        goto L81; // [3739] 3749
    }
    else{
    }

    /**     					result &= ' '*/
    Append(&_result_9112, _result_9112, 32);
L81: 
L80: 

    /**    				if trimming then*/
    if (_trimming_9130 == 0)
    {
        goto L82; // [3754] 3768
    }
    else{
    }

    /**    					result = trim(result)*/
    RefDS(_result_9112);
    RefDS(_3967);
    _0 = _result_9112;
    _result_9112 = _12trim(_result_9112, _3967, 0);
    DeRefDS(_0);
L82: 

    /**     			tend = 0*/
    _tend_9116 = 0;

    /** 		    	prevargv = currargv*/
    Ref(_currargv_9136);
    DeRef(_prevargv_9135);
    _prevargv_9135 = _currargv_9136;
L1F: 
    DeRef(_argtext_9298);
    _argtext_9298 = NOVALUE;

    /**     end while*/
    goto L2; // [3788] 70
L3: 

    /** 	return result*/
    DeRefDS(_format_pattern_9110);
    DeRef(_arg_list_9111);
    DeRef(_prevargv_9135);
    DeRef(_currargv_9136);
    DeRef(_idname_9137);
    DeRef(_envsym_9138);
    DeRefi(_envvar_9139);
    DeRef(_5542);
    _5542 = NOVALUE;
    DeRef(_5338);
    _5338 = NOVALUE;
    DeRef(_5265);
    _5265 = NOVALUE;
    DeRef(_5433);
    _5433 = NOVALUE;
    DeRef(_5529);
    _5529 = NOVALUE;
    DeRef(_5474);
    _5474 = NOVALUE;
    DeRef(_5295);
    _5295 = NOVALUE;
    DeRef(_5482);
    _5482 = NOVALUE;
    DeRef(_5192);
    _5192 = NOVALUE;
    DeRef(_5548);
    _5548 = NOVALUE;
    DeRef(_5551);
    _5551 = NOVALUE;
    DeRef(_5255);
    _5255 = NOVALUE;
    DeRef(_5275);
    _5275 = NOVALUE;
    DeRef(_5486);
    _5486 = NOVALUE;
    DeRef(_5496);
    _5496 = NOVALUE;
    _5413 = NOVALUE;
    DeRef(_5556);
    _5556 = NOVALUE;
    DeRef(_5411);
    _5411 = NOVALUE;
    DeRef(_5553);
    _5553 = NOVALUE;
    DeRef(_5285);
    _5285 = NOVALUE;
    DeRef(_5512);
    _5512 = NOVALUE;
    DeRef(_5572);
    _5572 = NOVALUE;
    DeRef(_5216);
    _5216 = NOVALUE;
    DeRef(_5505);
    _5505 = NOVALUE;
    DeRef(_5356);
    _5356 = NOVALUE;
    DeRef(_5559);
    _5559 = NOVALUE;
    DeRef(_5243);
    _5243 = NOVALUE;
    DeRef(_5374);
    _5374 = NOVALUE;
    DeRef(_5443);
    _5443 = NOVALUE;
    return _result_9112;
    ;
}


int _12wrap(int _content_9792, int _width_9793, int _wrap_with_9794, int _wrap_at_9795)
{
    int _result_9800 = NOVALUE;
    int _split_at_9803 = NOVALUE;
    int _5615 = NOVALUE;
    int _5613 = NOVALUE;
    int _5611 = NOVALUE;
    int _5610 = NOVALUE;
    int _5609 = NOVALUE;
    int _5607 = NOVALUE;
    int _5606 = NOVALUE;
    int _5604 = NOVALUE;
    int _5601 = NOVALUE;
    int _5599 = NOVALUE;
    int _5598 = NOVALUE;
    int _5597 = NOVALUE;
    int _5595 = NOVALUE;
    int _5594 = NOVALUE;
    int _5593 = NOVALUE;
    int _5591 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_width_9793)) {
        _1 = (long)(DBL_PTR(_width_9793)->dbl);
        if (UNIQUE(DBL_PTR(_width_9793)) && (DBL_PTR(_width_9793)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_width_9793);
        _width_9793 = _1;
    }

    /** 	if length(content) < width then*/
    if (IS_SEQUENCE(_content_9792)){
            _5591 = SEQ_PTR(_content_9792)->length;
    }
    else {
        _5591 = 1;
    }
    if (_5591 >= _width_9793)
    goto L1; // [16] 27

    /** 		return content*/
    DeRefDS(_wrap_with_9794);
    DeRefDS(_wrap_at_9795);
    DeRef(_result_9800);
    return _content_9792;
L1: 

    /** 	sequence result = ""*/
    RefDS(_5);
    DeRef(_result_9800);
    _result_9800 = _5;

    /** 	while length(content) do*/
L2: 
    if (IS_SEQUENCE(_content_9792)){
            _5593 = SEQ_PTR(_content_9792)->length;
    }
    else {
        _5593 = 1;
    }
    if (_5593 == 0)
    {
        _5593 = NOVALUE;
        goto L3; // [42] 267
    }
    else{
        _5593 = NOVALUE;
    }

    /** 		integer split_at = 0*/
    _split_at_9803 = 0;

    /** 		for i = width to 1 by -1 do*/
    {
        int _i_9805;
        _i_9805 = _width_9793;
L4: 
        if (_i_9805 < 1){
            goto L5; // [54] 94
        }

        /** 			if find(content[i], wrap_at) then*/
        _2 = (int)SEQ_PTR(_content_9792);
        _5594 = (int)*(((s1_ptr)_2)->base + _i_9805);
        _5595 = find_from(_5594, _wrap_at_9795, 1);
        _5594 = NOVALUE;
        if (_5595 == 0)
        {
            _5595 = NOVALUE;
            goto L6; // [72] 87
        }
        else{
            _5595 = NOVALUE;
        }

        /** 				split_at = i*/
        _split_at_9803 = _i_9805;

        /** 				exit*/
        goto L5; // [84] 94
L6: 

        /** 		end for*/
        _i_9805 = _i_9805 + -1;
        goto L4; // [89] 61
L5: 
        ;
    }

    /** 		if split_at = 0 then*/
    if (_split_at_9803 != 0)
    goto L7; // [96] 180

    /** 			for i = width to length(content) do*/
    if (IS_SEQUENCE(_content_9792)){
            _5597 = SEQ_PTR(_content_9792)->length;
    }
    else {
        _5597 = 1;
    }
    {
        int _i_9812;
        _i_9812 = _width_9793;
L8: 
        if (_i_9812 > _5597){
            goto L9; // [105] 145
        }

        /** 				if find(content[i], wrap_at) then*/
        _2 = (int)SEQ_PTR(_content_9792);
        _5598 = (int)*(((s1_ptr)_2)->base + _i_9812);
        _5599 = find_from(_5598, _wrap_at_9795, 1);
        _5598 = NOVALUE;
        if (_5599 == 0)
        {
            _5599 = NOVALUE;
            goto LA; // [123] 138
        }
        else{
            _5599 = NOVALUE;
        }

        /** 					split_at = i*/
        _split_at_9803 = _i_9812;

        /** 					exit*/
        goto L9; // [135] 145
LA: 

        /** 			end for*/
        _i_9812 = _i_9812 + 1;
        goto L8; // [140] 112
L9: 
        ;
    }

    /** 			if split_at = 0 then*/
    if (_split_at_9803 != 0)
    goto LB; // [147] 179

    /** 				if length(result) then*/
    if (IS_SEQUENCE(_result_9800)){
            _5601 = SEQ_PTR(_result_9800)->length;
    }
    else {
        _5601 = 1;
    }
    if (_5601 == 0)
    {
        _5601 = NOVALUE;
        goto LC; // [156] 166
    }
    else{
        _5601 = NOVALUE;
    }

    /** 					result &= wrap_with*/
    Concat((object_ptr)&_result_9800, _result_9800, _wrap_with_9794);
LC: 

    /** 				result &= content*/
    Concat((object_ptr)&_result_9800, _result_9800, _content_9792);

    /** 				exit*/
    goto L3; // [176] 267
LB: 
L7: 

    /** 		if length(result) then*/
    if (IS_SEQUENCE(_result_9800)){
            _5604 = SEQ_PTR(_result_9800)->length;
    }
    else {
        _5604 = 1;
    }
    if (_5604 == 0)
    {
        _5604 = NOVALUE;
        goto LD; // [185] 195
    }
    else{
        _5604 = NOVALUE;
    }

    /** 			result &= wrap_with*/
    Concat((object_ptr)&_result_9800, _result_9800, _wrap_with_9794);
LD: 

    /** 		result &= trim(content[1..split_at])*/
    rhs_slice_target = (object_ptr)&_5606;
    RHS_Slice(_content_9792, 1, _split_at_9803);
    RefDS(_3967);
    _5607 = _12trim(_5606, _3967, 0);
    _5606 = NOVALUE;
    if (IS_SEQUENCE(_result_9800) && IS_ATOM(_5607)) {
        Ref(_5607);
        Append(&_result_9800, _result_9800, _5607);
    }
    else if (IS_ATOM(_result_9800) && IS_SEQUENCE(_5607)) {
    }
    else {
        Concat((object_ptr)&_result_9800, _result_9800, _5607);
    }
    DeRef(_5607);
    _5607 = NOVALUE;

    /** 		content = trim(content[split_at + 1..$])*/
    _5609 = _split_at_9803 + 1;
    if (_5609 > MAXINT){
        _5609 = NewDouble((double)_5609);
    }
    if (IS_SEQUENCE(_content_9792)){
            _5610 = SEQ_PTR(_content_9792)->length;
    }
    else {
        _5610 = 1;
    }
    rhs_slice_target = (object_ptr)&_5611;
    RHS_Slice(_content_9792, _5609, _5610);
    RefDS(_3967);
    _0 = _content_9792;
    _content_9792 = _12trim(_5611, _3967, 0);
    DeRefDS(_0);
    _5611 = NOVALUE;

    /** 		if length(content) < width then*/
    if (IS_SEQUENCE(_content_9792)){
            _5613 = SEQ_PTR(_content_9792)->length;
    }
    else {
        _5613 = 1;
    }
    if (_5613 >= _width_9793)
    goto LE; // [239] 260

    /** 			result &= wrap_with & content*/
    Concat((object_ptr)&_5615, _wrap_with_9794, _content_9792);
    Concat((object_ptr)&_result_9800, _result_9800, _5615);
    DeRefDS(_5615);
    _5615 = NOVALUE;

    /** 			exit*/
    goto L3; // [257] 267
LE: 

    /** 	end while*/
    goto L2; // [264] 39
L3: 

    /** 	return result*/
    DeRefDS(_content_9792);
    DeRefDS(_wrap_with_9794);
    DeRefDS(_wrap_at_9795);
    DeRef(_5609);
    _5609 = NOVALUE;
    return _result_9800;
    ;
}



// 0x5A58A844
