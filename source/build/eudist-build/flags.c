// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _34flags_to_string(int _flag_bits_15504, int _flag_names_15505, int _expand_flags_15506)
{
    int _s_15507 = NOVALUE;
    int _has_one_bit_15524 = NOVALUE;
    int _which_bit_inlined_which_bit_at_102_15526 = NOVALUE;
    int _current_flag_15530 = NOVALUE;
    int _which_bit_inlined_which_bit_at_247_15556 = NOVALUE;
    int _8972 = NOVALUE;
    int _8970 = NOVALUE;
    int _8969 = NOVALUE;
    int _8967 = NOVALUE;
    int _8965 = NOVALUE;
    int _8964 = NOVALUE;
    int _8960 = NOVALUE;
    int _8959 = NOVALUE;
    int _8956 = NOVALUE;
    int _8955 = NOVALUE;
    int _8951 = NOVALUE;
    int _8950 = NOVALUE;
    int _8949 = NOVALUE;
    int _8948 = NOVALUE;
    int _8947 = NOVALUE;
    int _8946 = NOVALUE;
    int _8945 = NOVALUE;
    int _8943 = NOVALUE;
    int _8942 = NOVALUE;
    int _8941 = NOVALUE;
    int _8940 = NOVALUE;
    int _0, _1, _2;
    

    /**     sequence s = {}*/
    RefDS(_5);
    DeRef(_s_15507);
    _s_15507 = _5;

    /**     if sequence(flag_bits) then*/
    _8940 = IS_SEQUENCE(_flag_bits_15504);
    if (_8940 == 0)
    {
        _8940 = NOVALUE;
        goto L1; // [19] 99
    }
    else{
        _8940 = NOVALUE;
    }

    /** 		for i = 1 to length(flag_bits) do*/
    if (IS_SEQUENCE(_flag_bits_15504)){
            _8941 = SEQ_PTR(_flag_bits_15504)->length;
    }
    else {
        _8941 = 1;
    }
    {
        int _i_15511;
        _i_15511 = 1;
L2: 
        if (_i_15511 > _8941){
            goto L3; // [27] 89
        }

        /** 			if not sequence(flag_bits[i]) then*/
        _2 = (int)SEQ_PTR(_flag_bits_15504);
        _8942 = (int)*(((s1_ptr)_2)->base + _i_15511);
        _8943 = IS_SEQUENCE(_8942);
        _8942 = NOVALUE;
        if (_8943 != 0)
        goto L4; // [43] 71
        _8943 = NOVALUE;

        /** 				flag_bits[i] = flags_to_string(flag_bits[i], flag_names, expand_flags)*/
        _2 = (int)SEQ_PTR(_flag_bits_15504);
        _8945 = (int)*(((s1_ptr)_2)->base + _i_15511);
        RefDS(_flag_names_15505);
        DeRef(_8946);
        _8946 = _flag_names_15505;
        DeRef(_8947);
        _8947 = _expand_flags_15506;
        Ref(_8945);
        _8948 = _34flags_to_string(_8945, _8946, _8947);
        _8945 = NOVALUE;
        _8946 = NOVALUE;
        _8947 = NOVALUE;
        _2 = (int)SEQ_PTR(_flag_bits_15504);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _flag_bits_15504 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15511);
        _1 = *(int *)_2;
        *(int *)_2 = _8948;
        if( _1 != _8948 ){
            DeRef(_1);
        }
        _8948 = NOVALUE;
        goto L5; // [68] 82
L4: 

        /** 				flag_bits[i] = {"?"}*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_8289);
        *((int *)(_2+4)) = _8289;
        _8949 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_flag_bits_15504);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _flag_bits_15504 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_15511);
        _1 = *(int *)_2;
        *(int *)_2 = _8949;
        if( _1 != _8949 ){
            DeRef(_1);
        }
        _8949 = NOVALUE;
L5: 

        /** 		end for*/
        _i_15511 = _i_15511 + 1;
        goto L2; // [84] 34
L3: 
        ;
    }

    /** 		s = flag_bits*/
    Ref(_flag_bits_15504);
    DeRef(_s_15507);
    _s_15507 = _flag_bits_15504;
    goto L6; // [96] 301
L1: 

    /** 		integer has_one_bit*/

    /** 		has_one_bit = which_bit(flag_bits)*/

    /** 	return map:get(one_bit_numbers, theValue, 0)*/
    Ref(_34one_bit_numbers_15482);
    Ref(_flag_bits_15504);
    _has_one_bit_15524 = _26get(_34one_bit_numbers_15482, _flag_bits_15504, 0);
    if (!IS_ATOM_INT(_has_one_bit_15524)) {
        _1 = (long)(DBL_PTR(_has_one_bit_15524)->dbl);
        if (UNIQUE(DBL_PTR(_has_one_bit_15524)) && (DBL_PTR(_has_one_bit_15524)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_has_one_bit_15524);
        _has_one_bit_15524 = _1;
    }

    /** 		for i = 1 to length(flag_names) do*/
    if (IS_SEQUENCE(_flag_names_15505)){
            _8950 = SEQ_PTR(_flag_names_15505)->length;
    }
    else {
        _8950 = 1;
    }
    {
        int _i_15528;
        _i_15528 = 1;
L7: 
        if (_i_15528 > _8950){
            goto L8; // [123] 298
        }

        /** 			atom current_flag = flag_names[i][1]*/
        _2 = (int)SEQ_PTR(_flag_names_15505);
        _8951 = (int)*(((s1_ptr)_2)->base + _i_15528);
        DeRef(_current_flag_15530);
        _2 = (int)SEQ_PTR(_8951);
        _current_flag_15530 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_current_flag_15530);
        _8951 = NOVALUE;

        /** 			if flag_bits = 0 then*/
        if (binary_op_a(NOTEQ, _flag_bits_15504, 0)){
            goto L9; // [142] 176
        }

        /** 				if current_flag = 0 then*/
        if (binary_op_a(NOTEQ, _current_flag_15530, 0)){
            goto LA; // [148] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_15505);
        _8955 = (int)*(((s1_ptr)_2)->base + _i_15528);
        _2 = (int)SEQ_PTR(_8955);
        _8956 = (int)*(((s1_ptr)_2)->base + 2);
        _8955 = NOVALUE;
        Ref(_8956);
        Append(&_s_15507, _s_15507, _8956);
        _8956 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_15530);
        _current_flag_15530 = NOVALUE;
        goto L8; // [170] 298
        goto LA; // [173] 289
L9: 

        /** 			elsif has_one_bit then*/
        if (_has_one_bit_15524 == 0)
        {
            goto LB; // [178] 211
        }
        else{
        }

        /** 				if current_flag = flag_bits then*/
        if (binary_op_a(NOTEQ, _current_flag_15530, _flag_bits_15504)){
            goto LA; // [183] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_15505);
        _8959 = (int)*(((s1_ptr)_2)->base + _i_15528);
        _2 = (int)SEQ_PTR(_8959);
        _8960 = (int)*(((s1_ptr)_2)->base + 2);
        _8959 = NOVALUE;
        Ref(_8960);
        Append(&_s_15507, _s_15507, _8960);
        _8960 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_15530);
        _current_flag_15530 = NOVALUE;
        goto L8; // [205] 298
        goto LA; // [208] 289
LB: 

        /** 			elsif not expand_flags then*/
        if (_expand_flags_15506 != 0)
        goto LC; // [213] 246

        /** 				if current_flag = flag_bits then*/
        if (binary_op_a(NOTEQ, _current_flag_15530, _flag_bits_15504)){
            goto LA; // [218] 289
        }

        /** 					s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_15505);
        _8964 = (int)*(((s1_ptr)_2)->base + _i_15528);
        _2 = (int)SEQ_PTR(_8964);
        _8965 = (int)*(((s1_ptr)_2)->base + 2);
        _8964 = NOVALUE;
        Ref(_8965);
        Append(&_s_15507, _s_15507, _8965);
        _8965 = NOVALUE;

        /** 					exit*/
        DeRef(_current_flag_15530);
        _current_flag_15530 = NOVALUE;
        goto L8; // [240] 298
        goto LA; // [243] 289
LC: 

        /** 				if which_bit(current_flag) then*/

        /** 	return map:get(one_bit_numbers, theValue, 0)*/
        Ref(_34one_bit_numbers_15482);
        Ref(_current_flag_15530);
        _0 = _which_bit_inlined_which_bit_at_247_15556;
        _which_bit_inlined_which_bit_at_247_15556 = _26get(_34one_bit_numbers_15482, _current_flag_15530, 0);
        DeRef(_0);
        if (_which_bit_inlined_which_bit_at_247_15556 == 0) {
            goto LD; // [259] 288
        }
        else {
            if (!IS_ATOM_INT(_which_bit_inlined_which_bit_at_247_15556) && DBL_PTR(_which_bit_inlined_which_bit_at_247_15556)->dbl == 0.0){
                goto LD; // [259] 288
            }
        }

        /** 					if and_bits( current_flag, flag_bits ) = current_flag then*/
        if (IS_ATOM_INT(_current_flag_15530) && IS_ATOM_INT(_flag_bits_15504)) {
            {unsigned long tu;
                 tu = (unsigned long)_current_flag_15530 & (unsigned long)_flag_bits_15504;
                 _8967 = MAKE_UINT(tu);
            }
        }
        else {
            _8967 = binary_op(AND_BITS, _current_flag_15530, _flag_bits_15504);
        }
        if (binary_op_a(NOTEQ, _8967, _current_flag_15530)){
            DeRef(_8967);
            _8967 = NOVALUE;
            goto LE; // [268] 287
        }
        DeRef(_8967);
        _8967 = NOVALUE;

        /** 						s = append(s,flag_names[i][2])*/
        _2 = (int)SEQ_PTR(_flag_names_15505);
        _8969 = (int)*(((s1_ptr)_2)->base + _i_15528);
        _2 = (int)SEQ_PTR(_8969);
        _8970 = (int)*(((s1_ptr)_2)->base + 2);
        _8969 = NOVALUE;
        Ref(_8970);
        Append(&_s_15507, _s_15507, _8970);
        _8970 = NOVALUE;
LE: 
LD: 
LA: 
        DeRef(_current_flag_15530);
        _current_flag_15530 = NOVALUE;

        /** 		end for*/
        _i_15528 = _i_15528 + 1;
        goto L7; // [293] 130
L8: 
        ;
    }
L6: 

    /**     if length(s) = 0 then*/
    if (IS_SEQUENCE(_s_15507)){
            _8972 = SEQ_PTR(_s_15507)->length;
    }
    else {
        _8972 = 1;
    }
    if (_8972 != 0)
    goto LF; // [306] 317

    /** 		s = append(s,"?")*/
    RefDS(_8289);
    Append(&_s_15507, _s_15507, _8289);
LF: 

    /**     return s*/
    DeRef(_flag_bits_15504);
    DeRefDS(_flag_names_15505);
    return _s_15507;
    ;
}



// 0x37DB00FA
