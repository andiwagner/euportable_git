// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _21binop_ok(int _a_4189, int _b_4190)
{
    int _2131 = NOVALUE;
    int _2130 = NOVALUE;
    int _2129 = NOVALUE;
    int _2128 = NOVALUE;
    int _2126 = NOVALUE;
    int _2125 = NOVALUE;
    int _2124 = NOVALUE;
    int _2122 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) or atom(b) then*/
    _2122 = IS_ATOM(_a_4189);
    if (_2122 != 0) {
        goto L1; // [6] 18
    }
    _2124 = IS_ATOM(_b_4190);
    if (_2124 == 0)
    {
        _2124 = NOVALUE;
        goto L2; // [14] 25
    }
    else{
        _2124 = NOVALUE;
    }
L1: 

    /** 		return 1*/
    DeRef(_a_4189);
    DeRef(_b_4190);
    return 1;
L2: 

    /** 	if length(a) != length(b) then*/
    if (IS_SEQUENCE(_a_4189)){
            _2125 = SEQ_PTR(_a_4189)->length;
    }
    else {
        _2125 = 1;
    }
    if (IS_SEQUENCE(_b_4190)){
            _2126 = SEQ_PTR(_b_4190)->length;
    }
    else {
        _2126 = 1;
    }
    if (_2125 == _2126)
    goto L3; // [33] 44

    /** 		return 0*/
    DeRef(_a_4189);
    DeRef(_b_4190);
    return 0;
L3: 

    /** 	for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_4189)){
            _2128 = SEQ_PTR(_a_4189)->length;
    }
    else {
        _2128 = 1;
    }
    {
        int _i_4200;
        _i_4200 = 1;
L4: 
        if (_i_4200 > _2128){
            goto L5; // [49] 88
        }

        /** 		if not binop_ok(a[i], b[i]) then*/
        _2 = (int)SEQ_PTR(_a_4189);
        _2129 = (int)*(((s1_ptr)_2)->base + _i_4200);
        _2 = (int)SEQ_PTR(_b_4190);
        _2130 = (int)*(((s1_ptr)_2)->base + _i_4200);
        Ref(_2129);
        Ref(_2130);
        _2131 = _21binop_ok(_2129, _2130);
        _2129 = NOVALUE;
        _2130 = NOVALUE;
        if (IS_ATOM_INT(_2131)) {
            if (_2131 != 0){
                DeRef(_2131);
                _2131 = NOVALUE;
                goto L6; // [71] 81
            }
        }
        else {
            if (DBL_PTR(_2131)->dbl != 0.0){
                DeRef(_2131);
                _2131 = NOVALUE;
                goto L6; // [71] 81
            }
        }
        DeRef(_2131);
        _2131 = NOVALUE;

        /** 			return 0*/
        DeRef(_a_4189);
        DeRef(_b_4190);
        return 0;
L6: 

        /** 	end for*/
        _i_4200 = _i_4200 + 1;
        goto L4; // [83] 56
L5: 
        ;
    }

    /** 	return 1*/
    DeRef(_a_4189);
    DeRef(_b_4190);
    return 1;
    ;
}


int _21fetch(int _source_4209, int _indexes_4210)
{
    int _x_4211 = NOVALUE;
    int _2143 = NOVALUE;
    int _2142 = NOVALUE;
    int _2141 = NOVALUE;
    int _2140 = NOVALUE;
    int _2139 = NOVALUE;
    int _2137 = NOVALUE;
    int _2135 = NOVALUE;
    int _2134 = NOVALUE;
    int _2133 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i=1 to length(indexes)-1 do*/
    if (IS_SEQUENCE(_indexes_4210)){
            _2133 = SEQ_PTR(_indexes_4210)->length;
    }
    else {
        _2133 = 1;
    }
    _2134 = _2133 - 1;
    _2133 = NOVALUE;
    {
        int _i_4213;
        _i_4213 = 1;
L1: 
        if (_i_4213 > _2134){
            goto L2; // [14] 40
        }

        /** 		source = source[indexes[i]]*/
        _2 = (int)SEQ_PTR(_indexes_4210);
        _2135 = (int)*(((s1_ptr)_2)->base + _i_4213);
        _0 = _source_4209;
        _2 = (int)SEQ_PTR(_source_4209);
        if (!IS_ATOM_INT(_2135)){
            _source_4209 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_2135)->dbl));
        }
        else{
            _source_4209 = (int)*(((s1_ptr)_2)->base + _2135);
        }
        Ref(_source_4209);
        DeRefDS(_0);

        /** 	end for*/
        _i_4213 = _i_4213 + 1;
        goto L1; // [35] 21
L2: 
        ;
    }

    /** 	x = indexes[$]*/
    if (IS_SEQUENCE(_indexes_4210)){
            _2137 = SEQ_PTR(_indexes_4210)->length;
    }
    else {
        _2137 = 1;
    }
    DeRef(_x_4211);
    _2 = (int)SEQ_PTR(_indexes_4210);
    _x_4211 = (int)*(((s1_ptr)_2)->base + _2137);
    Ref(_x_4211);

    /** 	if atom(x) then*/
    _2139 = IS_ATOM(_x_4211);
    if (_2139 == 0)
    {
        _2139 = NOVALUE;
        goto L3; // [54] 70
    }
    else{
        _2139 = NOVALUE;
    }

    /** 		return source[x]*/
    _2 = (int)SEQ_PTR(_source_4209);
    if (!IS_ATOM_INT(_x_4211)){
        _2140 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_x_4211)->dbl));
    }
    else{
        _2140 = (int)*(((s1_ptr)_2)->base + _x_4211);
    }
    Ref(_2140);
    DeRefDS(_source_4209);
    DeRefDS(_indexes_4210);
    DeRef(_x_4211);
    DeRef(_2134);
    _2134 = NOVALUE;
    _2135 = NOVALUE;
    return _2140;
    goto L4; // [67] 90
L3: 

    /** 		return source[x[1]..x[2]]*/
    _2 = (int)SEQ_PTR(_x_4211);
    _2141 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_x_4211);
    _2142 = (int)*(((s1_ptr)_2)->base + 2);
    rhs_slice_target = (object_ptr)&_2143;
    RHS_Slice(_source_4209, _2141, _2142);
    DeRefDS(_source_4209);
    DeRefDS(_indexes_4210);
    DeRef(_x_4211);
    DeRef(_2134);
    _2134 = NOVALUE;
    _2135 = NOVALUE;
    _2140 = NOVALUE;
    _2141 = NOVALUE;
    _2142 = NOVALUE;
    return _2143;
L4: 
    ;
}


int _21store(int _target_4229, int _indexes_4230, int _x_4231)
{
    int _partials_4232 = NOVALUE;
    int _result_4233 = NOVALUE;
    int _branch_4234 = NOVALUE;
    int _last_idx_4235 = NOVALUE;
    int _2164 = NOVALUE;
    int _2163 = NOVALUE;
    int _2161 = NOVALUE;
    int _2160 = NOVALUE;
    int _2158 = NOVALUE;
    int _2157 = NOVALUE;
    int _2156 = NOVALUE;
    int _2154 = NOVALUE;
    int _2152 = NOVALUE;
    int _2151 = NOVALUE;
    int _2150 = NOVALUE;
    int _2148 = NOVALUE;
    int _2147 = NOVALUE;
    int _2146 = NOVALUE;
    int _2144 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(indexes) = 1 then*/
    if (IS_SEQUENCE(_indexes_4230)){
            _2144 = SEQ_PTR(_indexes_4230)->length;
    }
    else {
        _2144 = 1;
    }
    if (_2144 != 1)
    goto L1; // [10] 31

    /** 		target[indexes[1]] = x*/
    _2 = (int)SEQ_PTR(_indexes_4230);
    _2146 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_x_4231);
    _2 = (int)SEQ_PTR(_target_4229);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _target_4229 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_2146))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_2146)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _2146);
    _1 = *(int *)_2;
    *(int *)_2 = _x_4231;
    DeRef(_1);

    /** 		return target*/
    DeRefDS(_indexes_4230);
    DeRef(_x_4231);
    DeRef(_partials_4232);
    DeRef(_result_4233);
    DeRef(_branch_4234);
    DeRef(_last_idx_4235);
    _2146 = NOVALUE;
    return _target_4229;
L1: 

    /** 	partials = repeat(target,length(indexes)-1)*/
    if (IS_SEQUENCE(_indexes_4230)){
            _2147 = SEQ_PTR(_indexes_4230)->length;
    }
    else {
        _2147 = 1;
    }
    _2148 = _2147 - 1;
    _2147 = NOVALUE;
    DeRef(_partials_4232);
    _partials_4232 = Repeat(_target_4229, _2148);
    _2148 = NOVALUE;

    /** 	branch = target*/
    RefDS(_target_4229);
    DeRef(_branch_4234);
    _branch_4234 = _target_4229;

    /** 	for i=1 to length(indexes)-1 do*/
    if (IS_SEQUENCE(_indexes_4230)){
            _2150 = SEQ_PTR(_indexes_4230)->length;
    }
    else {
        _2150 = 1;
    }
    _2151 = _2150 - 1;
    _2150 = NOVALUE;
    {
        int _i_4244;
        _i_4244 = 1;
L2: 
        if (_i_4244 > _2151){
            goto L3; // [60] 92
        }

        /** 		branch=branch[indexes[i]]*/
        _2 = (int)SEQ_PTR(_indexes_4230);
        _2152 = (int)*(((s1_ptr)_2)->base + _i_4244);
        _0 = _branch_4234;
        _2 = (int)SEQ_PTR(_branch_4234);
        if (!IS_ATOM_INT(_2152)){
            _branch_4234 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_2152)->dbl));
        }
        else{
            _branch_4234 = (int)*(((s1_ptr)_2)->base + _2152);
        }
        Ref(_branch_4234);
        DeRef(_0);

        /** 		partials[i]=branch*/
        RefDS(_branch_4234);
        _2 = (int)SEQ_PTR(_partials_4232);
        _2 = (int)(((s1_ptr)_2)->base + _i_4244);
        _1 = *(int *)_2;
        *(int *)_2 = _branch_4234;
        DeRef(_1);

        /** 	end for*/
        _i_4244 = _i_4244 + 1;
        goto L2; // [87] 67
L3: 
        ;
    }

    /** 	last_idx = indexes[$]*/
    if (IS_SEQUENCE(_indexes_4230)){
            _2154 = SEQ_PTR(_indexes_4230)->length;
    }
    else {
        _2154 = 1;
    }
    DeRef(_last_idx_4235);
    _2 = (int)SEQ_PTR(_indexes_4230);
    _last_idx_4235 = (int)*(((s1_ptr)_2)->base + _2154);
    Ref(_last_idx_4235);

    /** 	if atom(last_idx) then*/
    _2156 = IS_ATOM(_last_idx_4235);
    if (_2156 == 0)
    {
        _2156 = NOVALUE;
        goto L4; // [106] 118
    }
    else{
        _2156 = NOVALUE;
    }

    /** 		branch[last_idx]=x*/
    Ref(_x_4231);
    _2 = (int)SEQ_PTR(_branch_4234);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _branch_4234 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_last_idx_4235))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_last_idx_4235)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _last_idx_4235);
    _1 = *(int *)_2;
    *(int *)_2 = _x_4231;
    DeRef(_1);
    goto L5; // [115] 134
L4: 

    /** 		branch[last_idx[1]..last_idx[2]]=x*/
    _2 = (int)SEQ_PTR(_last_idx_4235);
    _2157 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_last_idx_4235);
    _2158 = (int)*(((s1_ptr)_2)->base + 2);
    assign_slice_seq = (s1_ptr *)&_branch_4234;
    AssignSlice(_2157, _2158, _x_4231);
    _2157 = NOVALUE;
    _2158 = NOVALUE;
L5: 

    /** 	partials = prepend(partials,0) -- avoids computing temp=i+1 a few times*/
    Prepend(&_partials_4232, _partials_4232, 0);

    /** 	for i=length(indexes)-1 to 2 by -1 do*/
    if (IS_SEQUENCE(_indexes_4230)){
            _2160 = SEQ_PTR(_indexes_4230)->length;
    }
    else {
        _2160 = 1;
    }
    _2161 = _2160 - 1;
    _2160 = NOVALUE;
    {
        int _i_4258;
        _i_4258 = _2161;
L6: 
        if (_i_4258 < 2){
            goto L7; // [149] 188
        }

        /** 		result = partials[i]*/
        DeRef(_result_4233);
        _2 = (int)SEQ_PTR(_partials_4232);
        _result_4233 = (int)*(((s1_ptr)_2)->base + _i_4258);
        Ref(_result_4233);

        /** 		result[indexes[i]] = branch*/
        _2 = (int)SEQ_PTR(_indexes_4230);
        _2163 = (int)*(((s1_ptr)_2)->base + _i_4258);
        RefDS(_branch_4234);
        _2 = (int)SEQ_PTR(_result_4233);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result_4233 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_2163))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_2163)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _2163);
        _1 = *(int *)_2;
        *(int *)_2 = _branch_4234;
        DeRef(_1);

        /** 		branch = result*/
        RefDS(_result_4233);
        DeRefDS(_branch_4234);
        _branch_4234 = _result_4233;

        /** 	end for*/
        _i_4258 = _i_4258 + -1;
        goto L6; // [183] 156
L7: 
        ;
    }

    /** 	target[indexes[1]] = branch*/
    _2 = (int)SEQ_PTR(_indexes_4230);
    _2164 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_branch_4234);
    _2 = (int)SEQ_PTR(_target_4229);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _target_4229 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_2164))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_2164)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _2164);
    _1 = *(int *)_2;
    *(int *)_2 = _branch_4234;
    DeRef(_1);

    /** 	return target*/
    DeRefDS(_indexes_4230);
    DeRef(_x_4231);
    DeRef(_partials_4232);
    DeRef(_result_4233);
    DeRefDS(_branch_4234);
    DeRef(_last_idx_4235);
    _2146 = NOVALUE;
    DeRef(_2151);
    _2151 = NOVALUE;
    _2152 = NOVALUE;
    DeRef(_2161);
    _2161 = NOVALUE;
    _2163 = NOVALUE;
    _2164 = NOVALUE;
    return _target_4229;
    ;
}


int _21valid_index(int _st_4266, int _x_4267)
{
    int _2169 = NOVALUE;
    int _2168 = NOVALUE;
    int _2165 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(x) then*/
    _2165 = IS_ATOM(_x_4267);
    if (_2165 != 0)
    goto L1; // [8] 18
    _2165 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_st_4266);
    DeRef(_x_4267);
    return 0;
L1: 

    /** 	if x < 1 then*/
    if (binary_op_a(GREATEREQ, _x_4267, 1)){
        goto L2; // [20] 31
    }

    /** 		return 0*/
    DeRefDS(_st_4266);
    DeRef(_x_4267);
    return 0;
L2: 

    /** 	if floor(x) > length(st) then*/
    if (IS_ATOM_INT(_x_4267))
    _2168 = e_floor(_x_4267);
    else
    _2168 = unary_op(FLOOR, _x_4267);
    if (IS_SEQUENCE(_st_4266)){
            _2169 = SEQ_PTR(_st_4266)->length;
    }
    else {
        _2169 = 1;
    }
    if (binary_op_a(LESSEQ, _2168, _2169)){
        DeRef(_2168);
        _2168 = NOVALUE;
        _2169 = NOVALUE;
        goto L3; // [39] 50
    }
    DeRef(_2168);
    _2168 = NOVALUE;
    _2169 = NOVALUE;

    /** 		return 0*/
    DeRefDS(_st_4266);
    DeRef(_x_4267);
    return 0;
L3: 

    /** 	return 1*/
    DeRefDS(_st_4266);
    DeRef(_x_4267);
    return 1;
    ;
}


int _21rotate(int _source_4279, int _shift_4280, int _start_4281, int _stop_4282)
{
    int _shifted_4284 = NOVALUE;
    int _len_4285 = NOVALUE;
    int _lSize_4286 = NOVALUE;
    int _msg_inlined_crash_at_66_4299 = NOVALUE;
    int _msg_inlined_crash_at_97_4305 = NOVALUE;
    int _2197 = NOVALUE;
    int _2196 = NOVALUE;
    int _2195 = NOVALUE;
    int _2194 = NOVALUE;
    int _2193 = NOVALUE;
    int _2191 = NOVALUE;
    int _2190 = NOVALUE;
    int _2184 = NOVALUE;
    int _2181 = NOVALUE;
    int _2178 = NOVALUE;
    int _2177 = NOVALUE;
    int _2175 = NOVALUE;
    int _2174 = NOVALUE;
    int _2173 = NOVALUE;
    int _2172 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_shift_4280)) {
        _1 = (long)(DBL_PTR(_shift_4280)->dbl);
        if (UNIQUE(DBL_PTR(_shift_4280)) && (DBL_PTR(_shift_4280)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_shift_4280);
        _shift_4280 = _1;
    }
    if (!IS_ATOM_INT(_start_4281)) {
        _1 = (long)(DBL_PTR(_start_4281)->dbl);
        if (UNIQUE(DBL_PTR(_start_4281)) && (DBL_PTR(_start_4281)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_4281);
        _start_4281 = _1;
    }
    if (!IS_ATOM_INT(_stop_4282)) {
        _1 = (long)(DBL_PTR(_stop_4282)->dbl);
        if (UNIQUE(DBL_PTR(_stop_4282)) && (DBL_PTR(_stop_4282)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_stop_4282);
        _stop_4282 = _1;
    }

    /** 	if start >= stop or length(source)=0 or not shift then*/
    _2172 = (_start_4281 >= _stop_4282);
    if (_2172 != 0) {
        _2173 = 1;
        goto L1; // [21] 36
    }
    if (IS_SEQUENCE(_source_4279)){
            _2174 = SEQ_PTR(_source_4279)->length;
    }
    else {
        _2174 = 1;
    }
    _2175 = (_2174 == 0);
    _2174 = NOVALUE;
    _2173 = (_2175 != 0);
L1: 
    if (_2173 != 0) {
        goto L2; // [36] 48
    }
    _2177 = (_shift_4280 == 0);
    if (_2177 == 0)
    {
        DeRef(_2177);
        _2177 = NOVALUE;
        goto L3; // [44] 55
    }
    else{
        DeRef(_2177);
        _2177 = NOVALUE;
    }
L2: 

    /** 		return source*/
    DeRef(_shifted_4284);
    DeRef(_2172);
    _2172 = NOVALUE;
    DeRef(_2175);
    _2175 = NOVALUE;
    return _source_4279;
L3: 

    /** 	if not valid_index(source, start) then*/
    RefDS(_source_4279);
    _2178 = _21valid_index(_source_4279, _start_4281);
    if (IS_ATOM_INT(_2178)) {
        if (_2178 != 0){
            DeRef(_2178);
            _2178 = NOVALUE;
            goto L4; // [62] 86
        }
    }
    else {
        if (DBL_PTR(_2178)->dbl != 0.0){
            DeRef(_2178);
            _2178 = NOVALUE;
            goto L4; // [62] 86
        }
    }
    DeRef(_2178);
    _2178 = NOVALUE;

    /** 		error:crash("sequence:rotate(): invalid 'start' parameter %d", start)*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_66_4299);
    _msg_inlined_crash_at_66_4299 = EPrintf(-9999999, _2180, _start_4281);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_66_4299);

    /** end procedure*/
    goto L5; // [80] 83
L5: 
    DeRefi(_msg_inlined_crash_at_66_4299);
    _msg_inlined_crash_at_66_4299 = NOVALUE;
L4: 

    /** 	if not valid_index(source, stop) then*/
    RefDS(_source_4279);
    _2181 = _21valid_index(_source_4279, _stop_4282);
    if (IS_ATOM_INT(_2181)) {
        if (_2181 != 0){
            DeRef(_2181);
            _2181 = NOVALUE;
            goto L6; // [93] 117
        }
    }
    else {
        if (DBL_PTR(_2181)->dbl != 0.0){
            DeRef(_2181);
            _2181 = NOVALUE;
            goto L6; // [93] 117
        }
    }
    DeRef(_2181);
    _2181 = NOVALUE;

    /** 		error:crash("sequence:rotate(): invalid 'stop' parameter %d", stop)*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_97_4305);
    _msg_inlined_crash_at_97_4305 = EPrintf(-9999999, _2183, _stop_4282);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_97_4305);

    /** end procedure*/
    goto L7; // [111] 114
L7: 
    DeRefi(_msg_inlined_crash_at_97_4305);
    _msg_inlined_crash_at_97_4305 = NOVALUE;
L6: 

    /** 	len = stop - start + 1*/
    _2184 = _stop_4282 - _start_4281;
    if ((long)((unsigned long)_2184 +(unsigned long) HIGH_BITS) >= 0){
        _2184 = NewDouble((double)_2184);
    }
    if (IS_ATOM_INT(_2184)) {
        _len_4285 = _2184 + 1;
    }
    else
    { // coercing _len_4285 to an integer 1
        _len_4285 = 1+(long)(DBL_PTR(_2184)->dbl);
        if( !IS_ATOM_INT(_len_4285) ){
            _len_4285 = (object)DBL_PTR(_len_4285)->dbl;
        }
    }
    DeRef(_2184);
    _2184 = NOVALUE;

    /** 	lSize = remainder(shift, len)*/
    _lSize_4286 = (_shift_4280 % _len_4285);

    /** 	if lSize = 0 then*/
    if (_lSize_4286 != 0)
    goto L8; // [139] 150

    /** 		return source*/
    DeRef(_shifted_4284);
    DeRef(_2172);
    _2172 = NOVALUE;
    DeRef(_2175);
    _2175 = NOVALUE;
    return _source_4279;
L8: 

    /** 	if lSize < 0 then -- convert right shift to left shift*/
    if (_lSize_4286 >= 0)
    goto L9; // [152] 165

    /** 		lSize += len*/
    _lSize_4286 = _lSize_4286 + _len_4285;
L9: 

    /** 	shifted = source[start .. start + lSize-1]*/
    _2190 = _start_4281 + _lSize_4286;
    if ((long)((unsigned long)_2190 + (unsigned long)HIGH_BITS) >= 0) 
    _2190 = NewDouble((double)_2190);
    if (IS_ATOM_INT(_2190)) {
        _2191 = _2190 - 1;
    }
    else {
        _2191 = NewDouble(DBL_PTR(_2190)->dbl - (double)1);
    }
    DeRef(_2190);
    _2190 = NOVALUE;
    rhs_slice_target = (object_ptr)&_shifted_4284;
    RHS_Slice(_source_4279, _start_4281, _2191);

    /** 	source[start .. stop - lSize] = source[start + lSize .. stop]*/
    _2193 = _stop_4282 - _lSize_4286;
    if ((long)((unsigned long)_2193 +(unsigned long) HIGH_BITS) >= 0){
        _2193 = NewDouble((double)_2193);
    }
    _2194 = _start_4281 + _lSize_4286;
    rhs_slice_target = (object_ptr)&_2195;
    RHS_Slice(_source_4279, _2194, _stop_4282);
    assign_slice_seq = (s1_ptr *)&_source_4279;
    AssignSlice(_start_4281, _2193, _2195);
    DeRef(_2193);
    _2193 = NOVALUE;
    DeRefDS(_2195);
    _2195 = NOVALUE;

    /** 	source[stop - lSize + 1.. stop] = shifted*/
    _2196 = _stop_4282 - _lSize_4286;
    if ((long)((unsigned long)_2196 +(unsigned long) HIGH_BITS) >= 0){
        _2196 = NewDouble((double)_2196);
    }
    if (IS_ATOM_INT(_2196)) {
        _2197 = _2196 + 1;
    }
    else
    _2197 = binary_op(PLUS, 1, _2196);
    DeRef(_2196);
    _2196 = NOVALUE;
    assign_slice_seq = (s1_ptr *)&_source_4279;
    AssignSlice(_2197, _stop_4282, _shifted_4284);
    DeRef(_2197);
    _2197 = NOVALUE;

    /** 	return source*/
    DeRefDS(_shifted_4284);
    DeRef(_2172);
    _2172 = NOVALUE;
    DeRef(_2175);
    _2175 = NOVALUE;
    DeRef(_2191);
    _2191 = NOVALUE;
    _2194 = NOVALUE;
    return _source_4279;
    ;
}


int _21columnize(int _source_4324, int _cols_4325, int _defval_4326)
{
    int _result_4327 = NOVALUE;
    int _collist_4328 = NOVALUE;
    int _col_4354 = NOVALUE;
    int _2231 = NOVALUE;
    int _2230 = NOVALUE;
    int _2229 = NOVALUE;
    int _2228 = NOVALUE;
    int _2227 = NOVALUE;
    int _2226 = NOVALUE;
    int _2225 = NOVALUE;
    int _2224 = NOVALUE;
    int _2223 = NOVALUE;
    int _2222 = NOVALUE;
    int _2221 = NOVALUE;
    int _2220 = NOVALUE;
    int _2219 = NOVALUE;
    int _2218 = NOVALUE;
    int _2217 = NOVALUE;
    int _2216 = NOVALUE;
    int _2215 = NOVALUE;
    int _2214 = NOVALUE;
    int _2212 = NOVALUE;
    int _2210 = NOVALUE;
    int _2208 = NOVALUE;
    int _2206 = NOVALUE;
    int _2204 = NOVALUE;
    int _2203 = NOVALUE;
    int _2202 = NOVALUE;
    int _2200 = NOVALUE;
    int _2198 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(cols) then*/
    _2198 = IS_SEQUENCE(_cols_4325);
    if (_2198 == 0)
    {
        _2198 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _2198 = NOVALUE;
    }

    /** 		collist = cols*/
    Ref(_cols_4325);
    DeRef(_collist_4328);
    _collist_4328 = _cols_4325;
    goto L2; // [18] 28
L1: 

    /** 		collist = {cols}*/
    _0 = _collist_4328;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_cols_4325);
    *((int *)(_2+4)) = _cols_4325;
    _collist_4328 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	if length(collist) = 0 then*/
    if (IS_SEQUENCE(_collist_4328)){
            _2200 = SEQ_PTR(_collist_4328)->length;
    }
    else {
        _2200 = 1;
    }
    if (_2200 != 0)
    goto L3; // [35] 112

    /** 		cols = 0*/
    DeRef(_cols_4325);
    _cols_4325 = 0;

    /** 		for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_4324)){
            _2202 = SEQ_PTR(_source_4324)->length;
    }
    else {
        _2202 = 1;
    }
    {
        int _i_4337;
        _i_4337 = 1;
L4: 
        if (_i_4337 > _2202){
            goto L5; // [49] 86
        }

        /** 			if cols < length(source[i]) then*/
        _2 = (int)SEQ_PTR(_source_4324);
        _2203 = (int)*(((s1_ptr)_2)->base + _i_4337);
        if (IS_SEQUENCE(_2203)){
                _2204 = SEQ_PTR(_2203)->length;
        }
        else {
            _2204 = 1;
        }
        _2203 = NOVALUE;
        if (binary_op_a(GREATEREQ, _cols_4325, _2204)){
            _2204 = NOVALUE;
            goto L6; // [65] 79
        }
        _2204 = NOVALUE;

        /** 				cols = length(source[i])*/
        _2 = (int)SEQ_PTR(_source_4324);
        _2206 = (int)*(((s1_ptr)_2)->base + _i_4337);
        DeRef(_cols_4325);
        if (IS_SEQUENCE(_2206)){
                _cols_4325 = SEQ_PTR(_2206)->length;
        }
        else {
            _cols_4325 = 1;
        }
        _2206 = NOVALUE;
L6: 

        /** 		end for*/
        _i_4337 = _i_4337 + 1;
        goto L4; // [81] 56
L5: 
        ;
    }

    /** 		for i = 1 to cols do*/
    Ref(_cols_4325);
    DeRef(_2208);
    _2208 = _cols_4325;
    {
        int _i_4346;
        _i_4346 = 1;
L7: 
        if (binary_op_a(GREATER, _i_4346, _2208)){
            goto L8; // [91] 111
        }

        /** 			collist &= i*/
        Ref(_i_4346);
        Append(&_collist_4328, _collist_4328, _i_4346);

        /** 		end for*/
        _0 = _i_4346;
        if (IS_ATOM_INT(_i_4346)) {
            _i_4346 = _i_4346 + 1;
            if ((long)((unsigned long)_i_4346 +(unsigned long) HIGH_BITS) >= 0){
                _i_4346 = NewDouble((double)_i_4346);
            }
        }
        else {
            _i_4346 = binary_op_a(PLUS, _i_4346, 1);
        }
        DeRef(_0);
        goto L7; // [106] 98
L8: 
        ;
        DeRef(_i_4346);
    }
L3: 

    /** 	result = repeat({}, length(collist))*/
    if (IS_SEQUENCE(_collist_4328)){
            _2210 = SEQ_PTR(_collist_4328)->length;
    }
    else {
        _2210 = 1;
    }
    DeRef(_result_4327);
    _result_4327 = Repeat(_5, _2210);
    _2210 = NOVALUE;

    /** 	for i = 1 to length(collist) do*/
    if (IS_SEQUENCE(_collist_4328)){
            _2212 = SEQ_PTR(_collist_4328)->length;
    }
    else {
        _2212 = 1;
    }
    {
        int _i_4352;
        _i_4352 = 1;
L9: 
        if (_i_4352 > _2212){
            goto LA; // [126] 271
        }

        /** 		integer col = collist[i]*/
        _2 = (int)SEQ_PTR(_collist_4328);
        _col_4354 = (int)*(((s1_ptr)_2)->base + _i_4352);
        if (!IS_ATOM_INT(_col_4354))
        _col_4354 = (long)DBL_PTR(_col_4354)->dbl;

        /** 		for j = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4324)){
                _2214 = SEQ_PTR(_source_4324)->length;
        }
        else {
            _2214 = 1;
        }
        {
            int _j_4357;
            _j_4357 = 1;
LB: 
            if (_j_4357 > _2214){
                goto LC; // [146] 262
            }

            /** 			if sequence(source[j]) and length(source[j]) < col then*/
            _2 = (int)SEQ_PTR(_source_4324);
            _2215 = (int)*(((s1_ptr)_2)->base + _j_4357);
            _2216 = IS_SEQUENCE(_2215);
            _2215 = NOVALUE;
            if (_2216 == 0) {
                goto LD; // [162] 198
            }
            _2 = (int)SEQ_PTR(_source_4324);
            _2218 = (int)*(((s1_ptr)_2)->base + _j_4357);
            if (IS_SEQUENCE(_2218)){
                    _2219 = SEQ_PTR(_2218)->length;
            }
            else {
                _2219 = 1;
            }
            _2218 = NOVALUE;
            _2220 = (_2219 < _col_4354);
            _2219 = NOVALUE;
            if (_2220 == 0)
            {
                DeRef(_2220);
                _2220 = NOVALUE;
                goto LD; // [178] 198
            }
            else{
                DeRef(_2220);
                _2220 = NOVALUE;
            }

            /** 				result[i] = append(result[i], defval)*/
            _2 = (int)SEQ_PTR(_result_4327);
            _2221 = (int)*(((s1_ptr)_2)->base + _i_4352);
            Ref(_defval_4326);
            Append(&_2222, _2221, _defval_4326);
            _2221 = NOVALUE;
            _2 = (int)SEQ_PTR(_result_4327);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_4327 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_4352);
            _1 = *(int *)_2;
            *(int *)_2 = _2222;
            if( _1 != _2222 ){
                DeRefDS(_1);
            }
            _2222 = NOVALUE;
            goto LE; // [195] 255
LD: 

            /** 				if atom(source[j]) then*/
            _2 = (int)SEQ_PTR(_source_4324);
            _2223 = (int)*(((s1_ptr)_2)->base + _j_4357);
            _2224 = IS_ATOM(_2223);
            _2223 = NOVALUE;
            if (_2224 == 0)
            {
                _2224 = NOVALUE;
                goto LF; // [207] 231
            }
            else{
                _2224 = NOVALUE;
            }

            /** 					result[i] = append(result[i], source[j])*/
            _2 = (int)SEQ_PTR(_result_4327);
            _2225 = (int)*(((s1_ptr)_2)->base + _i_4352);
            _2 = (int)SEQ_PTR(_source_4324);
            _2226 = (int)*(((s1_ptr)_2)->base + _j_4357);
            Ref(_2226);
            Append(&_2227, _2225, _2226);
            _2225 = NOVALUE;
            _2226 = NOVALUE;
            _2 = (int)SEQ_PTR(_result_4327);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_4327 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_4352);
            _1 = *(int *)_2;
            *(int *)_2 = _2227;
            if( _1 != _2227 ){
                DeRefDS(_1);
            }
            _2227 = NOVALUE;
            goto L10; // [228] 254
LF: 

            /** 					result[i] = append(result[i], source[j][col])*/
            _2 = (int)SEQ_PTR(_result_4327);
            _2228 = (int)*(((s1_ptr)_2)->base + _i_4352);
            _2 = (int)SEQ_PTR(_source_4324);
            _2229 = (int)*(((s1_ptr)_2)->base + _j_4357);
            _2 = (int)SEQ_PTR(_2229);
            _2230 = (int)*(((s1_ptr)_2)->base + _col_4354);
            _2229 = NOVALUE;
            Ref(_2230);
            Append(&_2231, _2228, _2230);
            _2228 = NOVALUE;
            _2230 = NOVALUE;
            _2 = (int)SEQ_PTR(_result_4327);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_4327 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_4352);
            _1 = *(int *)_2;
            *(int *)_2 = _2231;
            if( _1 != _2231 ){
                DeRefDS(_1);
            }
            _2231 = NOVALUE;
L10: 
LE: 

            /** 		end for*/
            _j_4357 = _j_4357 + 1;
            goto LB; // [257] 153
LC: 
            ;
        }

        /** 	end for*/
        _i_4352 = _i_4352 + 1;
        goto L9; // [266] 133
LA: 
        ;
    }

    /** 	return result*/
    DeRefDS(_source_4324);
    DeRef(_cols_4325);
    DeRef(_defval_4326);
    DeRef(_collist_4328);
    _2203 = NOVALUE;
    _2206 = NOVALUE;
    _2218 = NOVALUE;
    return _result_4327;
    ;
}


int _21apply(int _source_4382, int _rid_4383, int _userdata_4384)
{
    int _2235 = NOVALUE;
    int _2234 = NOVALUE;
    int _2233 = NOVALUE;
    int _2232 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_rid_4383)) {
        _1 = (long)(DBL_PTR(_rid_4383)->dbl);
        if (UNIQUE(DBL_PTR(_rid_4383)) && (DBL_PTR(_rid_4383)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_4383);
        _rid_4383 = _1;
    }

    /** 	for a = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_4382)){
            _2232 = SEQ_PTR(_source_4382)->length;
    }
    else {
        _2232 = 1;
    }
    {
        int _a_4386;
        _a_4386 = 1;
L1: 
        if (_a_4386 > _2232){
            goto L2; // [12] 44
        }

        /** 		source[a] = call_func(rid, {source[a], userdata})*/
        _2 = (int)SEQ_PTR(_source_4382);
        _2233 = (int)*(((s1_ptr)_2)->base + _a_4386);
        Ref(_userdata_4384);
        Ref(_2233);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _2233;
        ((int *)_2)[2] = _userdata_4384;
        _2234 = MAKE_SEQ(_1);
        _2233 = NOVALUE;
        _1 = (int)SEQ_PTR(_2234);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_rid_4383].addr;
        Ref(*(int *)(_2+4));
        Ref(*(int *)(_2+8));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4), 
                            *(int *)(_2+8)
                             );
        DeRef(_2235);
        _2235 = _1;
        DeRefDS(_2234);
        _2234 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_4382);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_4382 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _a_4386);
        _1 = *(int *)_2;
        *(int *)_2 = _2235;
        if( _1 != _2235 ){
            DeRef(_1);
        }
        _2235 = NOVALUE;

        /** 	end for*/
        _a_4386 = _a_4386 + 1;
        goto L1; // [39] 19
L2: 
        ;
    }

    /** 	return source*/
    DeRef(_userdata_4384);
    return _source_4382;
    ;
}


int _21mapping(int _source_arg_4393, int _from_set_4394, int _to_set_4395, int _one_level_4396)
{
    int _pos_4397 = NOVALUE;
    int _2257 = NOVALUE;
    int _2256 = NOVALUE;
    int _2255 = NOVALUE;
    int _2254 = NOVALUE;
    int _2253 = NOVALUE;
    int _2252 = NOVALUE;
    int _2251 = NOVALUE;
    int _2250 = NOVALUE;
    int _2249 = NOVALUE;
    int _2247 = NOVALUE;
    int _2245 = NOVALUE;
    int _2244 = NOVALUE;
    int _2243 = NOVALUE;
    int _2241 = NOVALUE;
    int _2240 = NOVALUE;
    int _2239 = NOVALUE;
    int _2238 = NOVALUE;
    int _2236 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_one_level_4396)) {
        _1 = (long)(DBL_PTR(_one_level_4396)->dbl);
        if (UNIQUE(DBL_PTR(_one_level_4396)) && (DBL_PTR(_one_level_4396)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_one_level_4396);
        _one_level_4396 = _1;
    }

    /** 	if atom(source_arg) then*/
    _2236 = IS_ATOM(_source_arg_4393);
    if (_2236 == 0)
    {
        _2236 = NOVALUE;
        goto L1; // [14] 57
    }
    else{
        _2236 = NOVALUE;
    }

    /** 		pos = find(source_arg, from_set)*/
    _pos_4397 = find_from(_source_arg_4393, _from_set_4394, 1);

    /** 		if pos >= 1  and pos <= length(to_set) then*/
    _2238 = (_pos_4397 >= 1);
    if (_2238 == 0) {
        goto L2; // [32] 167
    }
    if (IS_SEQUENCE(_to_set_4395)){
            _2240 = SEQ_PTR(_to_set_4395)->length;
    }
    else {
        _2240 = 1;
    }
    _2241 = (_pos_4397 <= _2240);
    _2240 = NOVALUE;
    if (_2241 == 0)
    {
        DeRef(_2241);
        _2241 = NOVALUE;
        goto L2; // [44] 167
    }
    else{
        DeRef(_2241);
        _2241 = NOVALUE;
    }

    /** 			source_arg = to_set[pos]*/
    DeRef(_source_arg_4393);
    _2 = (int)SEQ_PTR(_to_set_4395);
    _source_arg_4393 = (int)*(((s1_ptr)_2)->base + _pos_4397);
    Ref(_source_arg_4393);
    goto L2; // [54] 167
L1: 

    /** 		for i = 1 to length(source_arg) do*/
    if (IS_SEQUENCE(_source_arg_4393)){
            _2243 = SEQ_PTR(_source_arg_4393)->length;
    }
    else {
        _2243 = 1;
    }
    {
        int _i_4409;
        _i_4409 = 1;
L3: 
        if (_i_4409 > _2243){
            goto L4; // [62] 166
        }

        /** 			if atom(source_arg[i]) or one_level then*/
        _2 = (int)SEQ_PTR(_source_arg_4393);
        _2244 = (int)*(((s1_ptr)_2)->base + _i_4409);
        _2245 = IS_ATOM(_2244);
        _2244 = NOVALUE;
        if (_2245 != 0) {
            goto L5; // [78] 87
        }
        if (_one_level_4396 == 0)
        {
            goto L6; // [83] 135
        }
        else{
        }
L5: 

        /** 				pos = find(source_arg[i], from_set)*/
        _2 = (int)SEQ_PTR(_source_arg_4393);
        _2247 = (int)*(((s1_ptr)_2)->base + _i_4409);
        _pos_4397 = find_from(_2247, _from_set_4394, 1);
        _2247 = NOVALUE;

        /** 				if pos >= 1  and pos <= length(to_set) then*/
        _2249 = (_pos_4397 >= 1);
        if (_2249 == 0) {
            goto L7; // [106] 159
        }
        if (IS_SEQUENCE(_to_set_4395)){
                _2251 = SEQ_PTR(_to_set_4395)->length;
        }
        else {
            _2251 = 1;
        }
        _2252 = (_pos_4397 <= _2251);
        _2251 = NOVALUE;
        if (_2252 == 0)
        {
            DeRef(_2252);
            _2252 = NOVALUE;
            goto L7; // [118] 159
        }
        else{
            DeRef(_2252);
            _2252 = NOVALUE;
        }

        /** 					source_arg[i] = to_set[pos]*/
        _2 = (int)SEQ_PTR(_to_set_4395);
        _2253 = (int)*(((s1_ptr)_2)->base + _pos_4397);
        Ref(_2253);
        _2 = (int)SEQ_PTR(_source_arg_4393);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_arg_4393 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4409);
        _1 = *(int *)_2;
        *(int *)_2 = _2253;
        if( _1 != _2253 ){
            DeRef(_1);
        }
        _2253 = NOVALUE;
        goto L7; // [132] 159
L6: 

        /** 				source_arg[i] = mapping(source_arg[i], from_set, to_set)*/
        _2 = (int)SEQ_PTR(_source_arg_4393);
        _2254 = (int)*(((s1_ptr)_2)->base + _i_4409);
        RefDS(_from_set_4394);
        DeRef(_2255);
        _2255 = _from_set_4394;
        RefDS(_to_set_4395);
        DeRef(_2256);
        _2256 = _to_set_4395;
        Ref(_2254);
        _2257 = _21mapping(_2254, _2255, _2256, 0);
        _2254 = NOVALUE;
        _2255 = NOVALUE;
        _2256 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_arg_4393);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_arg_4393 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4409);
        _1 = *(int *)_2;
        *(int *)_2 = _2257;
        if( _1 != _2257 ){
            DeRef(_1);
        }
        _2257 = NOVALUE;
L7: 

        /** 		end for*/
        _i_4409 = _i_4409 + 1;
        goto L3; // [161] 69
L4: 
        ;
    }
L2: 

    /** 	return source_arg*/
    DeRefDS(_from_set_4394);
    DeRefDS(_to_set_4395);
    DeRef(_2238);
    _2238 = NOVALUE;
    DeRef(_2249);
    _2249 = NOVALUE;
    return _source_arg_4393;
    ;
}


int _21reverse(int _target_4430, int _pFrom_4431, int _pTo_4432)
{
    int _uppr_4433 = NOVALUE;
    int _n_4434 = NOVALUE;
    int _lLimit_4435 = NOVALUE;
    int _t_4436 = NOVALUE;
    int _2272 = NOVALUE;
    int _2271 = NOVALUE;
    int _2270 = NOVALUE;
    int _2268 = NOVALUE;
    int _2267 = NOVALUE;
    int _2265 = NOVALUE;
    int _2263 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pFrom_4431)) {
        _1 = (long)(DBL_PTR(_pFrom_4431)->dbl);
        if (UNIQUE(DBL_PTR(_pFrom_4431)) && (DBL_PTR(_pFrom_4431)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pFrom_4431);
        _pFrom_4431 = _1;
    }
    if (!IS_ATOM_INT(_pTo_4432)) {
        _1 = (long)(DBL_PTR(_pTo_4432)->dbl);
        if (UNIQUE(DBL_PTR(_pTo_4432)) && (DBL_PTR(_pTo_4432)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pTo_4432);
        _pTo_4432 = _1;
    }

    /** 	n = length(target)*/
    if (IS_SEQUENCE(_target_4430)){
            _n_4434 = SEQ_PTR(_target_4430)->length;
    }
    else {
        _n_4434 = 1;
    }

    /** 	if n < 2 then*/
    if (_n_4434 >= 2)
    goto L1; // [18] 29

    /** 		return target*/
    DeRef(_t_4436);
    return _target_4430;
L1: 

    /** 	if pFrom < 1 then*/
    if (_pFrom_4431 >= 1)
    goto L2; // [31] 43

    /** 		pFrom = 1*/
    _pFrom_4431 = 1;
L2: 

    /** 	if pTo < 1 then*/
    if (_pTo_4432 >= 1)
    goto L3; // [45] 58

    /** 		pTo = n + pTo*/
    _pTo_4432 = _n_4434 + _pTo_4432;
L3: 

    /** 	if pTo < pFrom or pFrom >= n then*/
    _2263 = (_pTo_4432 < _pFrom_4431);
    if (_2263 != 0) {
        goto L4; // [64] 77
    }
    _2265 = (_pFrom_4431 >= _n_4434);
    if (_2265 == 0)
    {
        DeRef(_2265);
        _2265 = NOVALUE;
        goto L5; // [73] 84
    }
    else{
        DeRef(_2265);
        _2265 = NOVALUE;
    }
L4: 

    /** 		return target*/
    DeRef(_t_4436);
    DeRef(_2263);
    _2263 = NOVALUE;
    return _target_4430;
L5: 

    /** 	if pTo > n then*/
    if (_pTo_4432 <= _n_4434)
    goto L6; // [86] 98

    /** 		pTo = n*/
    _pTo_4432 = _n_4434;
L6: 

    /** 	lLimit = floor((pFrom+pTo-1)/2)*/
    _2267 = _pFrom_4431 + _pTo_4432;
    if ((long)((unsigned long)_2267 + (unsigned long)HIGH_BITS) >= 0) 
    _2267 = NewDouble((double)_2267);
    if (IS_ATOM_INT(_2267)) {
        _2268 = _2267 - 1;
        if ((long)((unsigned long)_2268 +(unsigned long) HIGH_BITS) >= 0){
            _2268 = NewDouble((double)_2268);
        }
    }
    else {
        _2268 = NewDouble(DBL_PTR(_2267)->dbl - (double)1);
    }
    DeRef(_2267);
    _2267 = NOVALUE;
    if (IS_ATOM_INT(_2268)) {
        _lLimit_4435 = _2268 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _2268, 2);
        _lLimit_4435 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_2268);
    _2268 = NOVALUE;
    if (!IS_ATOM_INT(_lLimit_4435)) {
        _1 = (long)(DBL_PTR(_lLimit_4435)->dbl);
        if (UNIQUE(DBL_PTR(_lLimit_4435)) && (DBL_PTR(_lLimit_4435)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lLimit_4435);
        _lLimit_4435 = _1;
    }

    /** 	t = target*/
    Ref(_target_4430);
    DeRef(_t_4436);
    _t_4436 = _target_4430;

    /** 	uppr = pTo*/
    _uppr_4433 = _pTo_4432;

    /** 	for lowr = pFrom to lLimit do*/
    _2270 = _lLimit_4435;
    {
        int _lowr_4455;
        _lowr_4455 = _pFrom_4431;
L7: 
        if (_lowr_4455 > _2270){
            goto L8; // [135] 177
        }

        /** 		t[uppr] = target[lowr]*/
        _2 = (int)SEQ_PTR(_target_4430);
        _2271 = (int)*(((s1_ptr)_2)->base + _lowr_4455);
        Ref(_2271);
        _2 = (int)SEQ_PTR(_t_4436);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_4436 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _uppr_4433);
        _1 = *(int *)_2;
        *(int *)_2 = _2271;
        if( _1 != _2271 ){
            DeRef(_1);
        }
        _2271 = NOVALUE;

        /** 		t[lowr] = target[uppr]*/
        _2 = (int)SEQ_PTR(_target_4430);
        _2272 = (int)*(((s1_ptr)_2)->base + _uppr_4433);
        Ref(_2272);
        _2 = (int)SEQ_PTR(_t_4436);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_4436 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lowr_4455);
        _1 = *(int *)_2;
        *(int *)_2 = _2272;
        if( _1 != _2272 ){
            DeRef(_1);
        }
        _2272 = NOVALUE;

        /** 		uppr -= 1*/
        _uppr_4433 = _uppr_4433 - 1;

        /** 	end for*/
        _lowr_4455 = _lowr_4455 + 1;
        goto L7; // [172] 142
L8: 
        ;
    }

    /** 	return t*/
    DeRef(_target_4430);
    DeRef(_2263);
    _2263 = NOVALUE;
    return _t_4436;
    ;
}


int _21shuffle(int _seq_4462)
{
    int _fromIdx_4466 = NOVALUE;
    int _swapValue_4468 = NOVALUE;
    int _2277 = NOVALUE;
    int _2274 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for toIdx = length(seq) to 2 by -1 do*/
    if (IS_SEQUENCE(_seq_4462)){
            _2274 = SEQ_PTR(_seq_4462)->length;
    }
    else {
        _2274 = 1;
    }
    {
        int _toIdx_4464;
        _toIdx_4464 = _2274;
L1: 
        if (_toIdx_4464 < 2){
            goto L2; // [6] 51
        }

        /** 		integer fromIdx = rand(toIdx)*/
        _fromIdx_4466 = good_rand() % ((unsigned)_toIdx_4464) + 1;

        /** 		object swapValue = seq[fromIdx]*/
        DeRef(_swapValue_4468);
        _2 = (int)SEQ_PTR(_seq_4462);
        _swapValue_4468 = (int)*(((s1_ptr)_2)->base + _fromIdx_4466);
        Ref(_swapValue_4468);

        /** 		seq[fromIdx] = seq[toIdx]*/
        _2 = (int)SEQ_PTR(_seq_4462);
        _2277 = (int)*(((s1_ptr)_2)->base + _toIdx_4464);
        Ref(_2277);
        _2 = (int)SEQ_PTR(_seq_4462);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _seq_4462 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _fromIdx_4466);
        _1 = *(int *)_2;
        *(int *)_2 = _2277;
        if( _1 != _2277 ){
            DeRef(_1);
        }
        _2277 = NOVALUE;

        /** 		seq[toIdx] = swapValue*/
        Ref(_swapValue_4468);
        _2 = (int)SEQ_PTR(_seq_4462);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _seq_4462 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _toIdx_4464);
        _1 = *(int *)_2;
        *(int *)_2 = _swapValue_4468;
        DeRef(_1);
        DeRef(_swapValue_4468);
        _swapValue_4468 = NOVALUE;

        /** 	end for*/
        _toIdx_4464 = _toIdx_4464 + -1;
        goto L1; // [46] 13
L2: 
        ;
    }

    /** 	return seq*/
    return _seq_4462;
    ;
}


int _21series(int _start_4473, int _increment_4474, int _count_4475, int _op_4476)
{
    int _result_4477 = NOVALUE;
    int _2288 = NOVALUE;
    int _2285 = NOVALUE;
    int _2279 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_4475)) {
        _1 = (long)(DBL_PTR(_count_4475)->dbl);
        if (UNIQUE(DBL_PTR(_count_4475)) && (DBL_PTR(_count_4475)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_4475);
        _count_4475 = _1;
    }
    if (!IS_ATOM_INT(_op_4476)) {
        _1 = (long)(DBL_PTR(_op_4476)->dbl);
        if (UNIQUE(DBL_PTR(_op_4476)) && (DBL_PTR(_op_4476)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_op_4476);
        _op_4476 = _1;
    }

    /** 	if count < 0 then*/
    if (_count_4475 >= 0)
    goto L1; // [11] 22

    /** 		return 0*/
    DeRef(_start_4473);
    DeRef(_increment_4474);
    DeRef(_result_4477);
    return 0;
L1: 

    /** 	if not binop_ok(start, increment) then*/
    Ref(_start_4473);
    Ref(_increment_4474);
    _2279 = _21binop_ok(_start_4473, _increment_4474);
    if (IS_ATOM_INT(_2279)) {
        if (_2279 != 0){
            DeRef(_2279);
            _2279 = NOVALUE;
            goto L2; // [29] 39
        }
    }
    else {
        if (DBL_PTR(_2279)->dbl != 0.0){
            DeRef(_2279);
            _2279 = NOVALUE;
            goto L2; // [29] 39
        }
    }
    DeRef(_2279);
    _2279 = NOVALUE;

    /** 		return 0*/
    DeRef(_start_4473);
    DeRef(_increment_4474);
    DeRef(_result_4477);
    return 0;
L2: 

    /** 	if count = 0 then*/
    if (_count_4475 != 0)
    goto L3; // [41] 52

    /** 		return {}*/
    RefDS(_5);
    DeRef(_start_4473);
    DeRef(_increment_4474);
    DeRef(_result_4477);
    return _5;
L3: 

    /** 	result = repeat(0, count )*/
    DeRef(_result_4477);
    _result_4477 = Repeat(0, _count_4475);

    /** 	result[1] = start*/
    Ref(_start_4473);
    _2 = (int)SEQ_PTR(_result_4477);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _result_4477 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    *(int *)_2 = _start_4473;

    /** 	switch op do*/
    _0 = _op_4476;
    switch ( _0 ){ 

        /** 		case '+' then*/
        case 43:

        /** 			for i = 2 to count  do*/
        _2285 = _count_4475;
        {
            int _i_4490;
            _i_4490 = 2;
L4: 
            if (_i_4490 > _2285){
                goto L5; // [80] 106
            }

            /** 				start += increment*/
            _0 = _start_4473;
            if (IS_ATOM_INT(_start_4473) && IS_ATOM_INT(_increment_4474)) {
                _start_4473 = _start_4473 + _increment_4474;
                if ((long)((unsigned long)_start_4473 + (unsigned long)HIGH_BITS) >= 0) 
                _start_4473 = NewDouble((double)_start_4473);
            }
            else {
                _start_4473 = binary_op(PLUS, _start_4473, _increment_4474);
            }
            DeRef(_0);

            /** 				result[i] = start*/
            Ref(_start_4473);
            _2 = (int)SEQ_PTR(_result_4477);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_4477 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_4490);
            _1 = *(int *)_2;
            *(int *)_2 = _start_4473;
            DeRef(_1);

            /** 			end for*/
            _i_4490 = _i_4490 + 1;
            goto L4; // [101] 87
L5: 
            ;
        }
        goto L6; // [106] 156

        /** 		case '*' then*/
        case 42:

        /** 			for i = 2 to count do*/
        _2288 = _count_4475;
        {
            int _i_4496;
            _i_4496 = 2;
L7: 
            if (_i_4496 > _2288){
                goto L8; // [117] 143
            }

            /** 				start *= increment*/
            _0 = _start_4473;
            if (IS_ATOM_INT(_start_4473) && IS_ATOM_INT(_increment_4474)) {
                if (_start_4473 == (short)_start_4473 && _increment_4474 <= INT15 && _increment_4474 >= -INT15)
                _start_4473 = _start_4473 * _increment_4474;
                else
                _start_4473 = NewDouble(_start_4473 * (double)_increment_4474);
            }
            else {
                _start_4473 = binary_op(MULTIPLY, _start_4473, _increment_4474);
            }
            DeRef(_0);

            /** 				result[i] = start*/
            Ref(_start_4473);
            _2 = (int)SEQ_PTR(_result_4477);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_4477 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_4496);
            _1 = *(int *)_2;
            *(int *)_2 = _start_4473;
            DeRef(_1);

            /** 			end for*/
            _i_4496 = _i_4496 + 1;
            goto L7; // [138] 124
L8: 
            ;
        }
        goto L6; // [143] 156

        /** 		case else*/
        default:

        /** 			return 0*/
        DeRef(_start_4473);
        DeRef(_increment_4474);
        DeRef(_result_4477);
        return 0;
    ;}L6: 

    /** 	return result*/
    DeRef(_start_4473);
    DeRef(_increment_4474);
    return _result_4477;
    ;
}


int _21repeat_pattern(int _pattern_4502, int _count_4503)
{
    int _ls_4504 = NOVALUE;
    int _result_4505 = NOVALUE;
    int _2297 = NOVALUE;
    int _2296 = NOVALUE;
    int _2295 = NOVALUE;
    int _2294 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_count_4503)) {
        _1 = (long)(DBL_PTR(_count_4503)->dbl);
        if (UNIQUE(DBL_PTR(_count_4503)) && (DBL_PTR(_count_4503)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_count_4503);
        _count_4503 = _1;
    }

    /** 	if count<=0 then*/
    if (_count_4503 > 0)
    goto L1; // [7] 18

    /** 		return {}*/
    RefDS(_5);
    DeRef(_pattern_4502);
    DeRef(_result_4505);
    return _5;
L1: 

    /** 	ls = length(pattern)*/
    if (IS_SEQUENCE(_pattern_4502)){
            _ls_4504 = SEQ_PTR(_pattern_4502)->length;
    }
    else {
        _ls_4504 = 1;
    }

    /** 	count *= ls*/
    _count_4503 = _count_4503 * _ls_4504;

    /** 	result=repeat(0,count)*/
    DeRef(_result_4505);
    _result_4505 = Repeat(0, _count_4503);

    /** 	for i=1 to count by ls do*/
    _2294 = _ls_4504;
    _2295 = _count_4503;
    {
        int _i_4512;
        _i_4512 = 1;
L2: 
        if (_i_4512 > _2295){
            goto L3; // [49] 78
        }

        /** 		result[i..i+ls-1] = pattern*/
        _2296 = _i_4512 + _ls_4504;
        if ((long)((unsigned long)_2296 + (unsigned long)HIGH_BITS) >= 0) 
        _2296 = NewDouble((double)_2296);
        if (IS_ATOM_INT(_2296)) {
            _2297 = _2296 - 1;
        }
        else {
            _2297 = NewDouble(DBL_PTR(_2296)->dbl - (double)1);
        }
        DeRef(_2296);
        _2296 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_result_4505;
        AssignSlice(_i_4512, _2297, _pattern_4502);
        DeRef(_2297);
        _2297 = NOVALUE;

        /** 	end for*/
        _i_4512 = _i_4512 + _2294;
        goto L2; // [73] 56
L3: 
        ;
    }

    /** 	return result*/
    DeRef(_pattern_4502);
    return _result_4505;
    ;
}


int _21pad_head(int _target_4519, int _size_4520, int _ch_4521)
{
    int _2303 = NOVALUE;
    int _2302 = NOVALUE;
    int _2301 = NOVALUE;
    int _2300 = NOVALUE;
    int _2298 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_4520)) {
        _1 = (long)(DBL_PTR(_size_4520)->dbl);
        if (UNIQUE(DBL_PTR(_size_4520)) && (DBL_PTR(_size_4520)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_4520);
        _size_4520 = _1;
    }

    /** 	if size <= length(target) then*/
    if (IS_SEQUENCE(_target_4519)){
            _2298 = SEQ_PTR(_target_4519)->length;
    }
    else {
        _2298 = 1;
    }
    if (_size_4520 > _2298)
    goto L1; // [10] 21

    /** 		return target*/
    DeRef(_ch_4521);
    return _target_4519;
L1: 

    /** 	return repeat(ch, size - length(target)) & target*/
    if (IS_SEQUENCE(_target_4519)){
            _2300 = SEQ_PTR(_target_4519)->length;
    }
    else {
        _2300 = 1;
    }
    _2301 = _size_4520 - _2300;
    _2300 = NOVALUE;
    _2302 = Repeat(_ch_4521, _2301);
    _2301 = NOVALUE;
    if (IS_SEQUENCE(_2302) && IS_ATOM(_target_4519)) {
        Ref(_target_4519);
        Append(&_2303, _2302, _target_4519);
    }
    else if (IS_ATOM(_2302) && IS_SEQUENCE(_target_4519)) {
    }
    else {
        Concat((object_ptr)&_2303, _2302, _target_4519);
        DeRefDS(_2302);
        _2302 = NOVALUE;
    }
    DeRef(_2302);
    _2302 = NOVALUE;
    DeRef(_target_4519);
    DeRef(_ch_4521);
    return _2303;
    ;
}


int _21pad_tail(int _target_4531, int _size_4532, int _ch_4533)
{
    int _2309 = NOVALUE;
    int _2308 = NOVALUE;
    int _2307 = NOVALUE;
    int _2306 = NOVALUE;
    int _2304 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_4532)) {
        _1 = (long)(DBL_PTR(_size_4532)->dbl);
        if (UNIQUE(DBL_PTR(_size_4532)) && (DBL_PTR(_size_4532)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_4532);
        _size_4532 = _1;
    }

    /** 	if size <= length(target) then*/
    if (IS_SEQUENCE(_target_4531)){
            _2304 = SEQ_PTR(_target_4531)->length;
    }
    else {
        _2304 = 1;
    }
    if (_size_4532 > _2304)
    goto L1; // [10] 21

    /** 		return target*/
    DeRef(_ch_4533);
    return _target_4531;
L1: 

    /** 	return target & repeat(ch, size - length(target))*/
    if (IS_SEQUENCE(_target_4531)){
            _2306 = SEQ_PTR(_target_4531)->length;
    }
    else {
        _2306 = 1;
    }
    _2307 = _size_4532 - _2306;
    _2306 = NOVALUE;
    _2308 = Repeat(_ch_4533, _2307);
    _2307 = NOVALUE;
    if (IS_SEQUENCE(_target_4531) && IS_ATOM(_2308)) {
    }
    else if (IS_ATOM(_target_4531) && IS_SEQUENCE(_2308)) {
        Ref(_target_4531);
        Prepend(&_2309, _2308, _target_4531);
    }
    else {
        Concat((object_ptr)&_2309, _target_4531, _2308);
    }
    DeRefDS(_2308);
    _2308 = NOVALUE;
    DeRef(_target_4531);
    DeRef(_ch_4533);
    return _2309;
    ;
}


int _21add_item(int _needle_4543, int _haystack_4544, int _pOrder_4545)
{
    int _msg_inlined_crash_at_110_4563 = NOVALUE;
    int _2318 = NOVALUE;
    int _2317 = NOVALUE;
    int _2316 = NOVALUE;
    int _2315 = NOVALUE;
    int _2314 = NOVALUE;
    int _2313 = NOVALUE;
    int _2310 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pOrder_4545)) {
        _1 = (long)(DBL_PTR(_pOrder_4545)->dbl);
        if (UNIQUE(DBL_PTR(_pOrder_4545)) && (DBL_PTR(_pOrder_4545)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pOrder_4545);
        _pOrder_4545 = _1;
    }

    /** 	if find(needle, haystack) then*/
    _2310 = find_from(_needle_4543, _haystack_4544, 1);
    if (_2310 == 0)
    {
        _2310 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _2310 = NOVALUE;
    }

    /** 		return haystack*/
    DeRef(_needle_4543);
    return _haystack_4544;
L1: 

    /** 	switch pOrder do*/
    _0 = _pOrder_4545;
    switch ( _0 ){ 

        /** 		case ADD_PREPEND then*/
        case 1:

        /** 			return prepend(haystack, needle)*/
        Ref(_needle_4543);
        Prepend(&_2313, _haystack_4544, _needle_4543);
        DeRef(_needle_4543);
        DeRefDS(_haystack_4544);
        return _2313;
        goto L2; // [45] 130

        /** 		case ADD_APPEND then*/
        case 2:

        /** 			return append(haystack, needle)*/
        Ref(_needle_4543);
        Append(&_2314, _haystack_4544, _needle_4543);
        DeRef(_needle_4543);
        DeRefDS(_haystack_4544);
        DeRef(_2313);
        _2313 = NOVALUE;
        return _2314;
        goto L2; // [61] 130

        /** 		case ADD_SORT_UP then*/
        case 3:

        /** 			return stdsort:sort(append(haystack, needle))*/
        Ref(_needle_4543);
        Append(&_2315, _haystack_4544, _needle_4543);
        _2316 = _22sort(_2315, 1);
        _2315 = NOVALUE;
        DeRef(_needle_4543);
        DeRefDS(_haystack_4544);
        DeRef(_2313);
        _2313 = NOVALUE;
        DeRef(_2314);
        _2314 = NOVALUE;
        return _2316;
        goto L2; // [82] 130

        /** 		case ADD_SORT_DOWN then*/
        case 4:

        /** 			return stdsort:sort(append(haystack, needle), stdsort:DESCENDING)*/
        Ref(_needle_4543);
        Append(&_2317, _haystack_4544, _needle_4543);
        _2318 = _22sort(_2317, -1);
        _2317 = NOVALUE;
        DeRef(_needle_4543);
        DeRefDS(_haystack_4544);
        DeRef(_2313);
        _2313 = NOVALUE;
        DeRef(_2314);
        _2314 = NOVALUE;
        DeRef(_2316);
        _2316 = NOVALUE;
        return _2318;
        goto L2; // [103] 130

        /** 		case else*/
        default:

        /** 			error:crash("sequence.e:add_item() invalid Order argument '%d'", pOrder)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_110_4563);
        _msg_inlined_crash_at_110_4563 = EPrintf(-9999999, _2319, _pOrder_4545);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_110_4563);

        /** end procedure*/
        goto L3; // [124] 127
L3: 
        DeRefi(_msg_inlined_crash_at_110_4563);
        _msg_inlined_crash_at_110_4563 = NOVALUE;
    ;}L2: 

    /** 	return haystack*/
    DeRef(_needle_4543);
    DeRef(_2313);
    _2313 = NOVALUE;
    DeRef(_2314);
    _2314 = NOVALUE;
    DeRef(_2316);
    _2316 = NOVALUE;
    DeRef(_2318);
    _2318 = NOVALUE;
    return _haystack_4544;
    ;
}


int _21remove_item(int _needle_4566, int _haystack_4567)
{
    int _lIdx_4568 = NOVALUE;
    int _2335 = NOVALUE;
    int _2334 = NOVALUE;
    int _2333 = NOVALUE;
    int _2332 = NOVALUE;
    int _2331 = NOVALUE;
    int _2330 = NOVALUE;
    int _2329 = NOVALUE;
    int _2328 = NOVALUE;
    int _2327 = NOVALUE;
    int _2325 = NOVALUE;
    int _2324 = NOVALUE;
    int _2323 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lIdx = find(needle, haystack)*/
    _lIdx_4568 = find_from(_needle_4566, _haystack_4567, 1);

    /** 	if not lIdx then*/
    if (_lIdx_4568 != 0)
    goto L1; // [14] 24

    /** 		return haystack*/
    DeRef(_needle_4566);
    return _haystack_4567;
L1: 

    /** 	if lIdx = 1 then*/
    if (_lIdx_4568 != 1)
    goto L2; // [26] 47

    /** 		return haystack[2 .. $]*/
    if (IS_SEQUENCE(_haystack_4567)){
            _2323 = SEQ_PTR(_haystack_4567)->length;
    }
    else {
        _2323 = 1;
    }
    rhs_slice_target = (object_ptr)&_2324;
    RHS_Slice(_haystack_4567, 2, _2323);
    DeRef(_needle_4566);
    DeRefDS(_haystack_4567);
    return _2324;
    goto L3; // [44] 109
L2: 

    /** 	elsif lIdx = length(haystack) then*/
    if (IS_SEQUENCE(_haystack_4567)){
            _2325 = SEQ_PTR(_haystack_4567)->length;
    }
    else {
        _2325 = 1;
    }
    if (_lIdx_4568 != _2325)
    goto L4; // [52] 77

    /** 		return haystack[1 .. $-1]*/
    if (IS_SEQUENCE(_haystack_4567)){
            _2327 = SEQ_PTR(_haystack_4567)->length;
    }
    else {
        _2327 = 1;
    }
    _2328 = _2327 - 1;
    _2327 = NOVALUE;
    rhs_slice_target = (object_ptr)&_2329;
    RHS_Slice(_haystack_4567, 1, _2328);
    DeRef(_needle_4566);
    DeRefDS(_haystack_4567);
    DeRef(_2324);
    _2324 = NOVALUE;
    _2328 = NOVALUE;
    return _2329;
    goto L3; // [74] 109
L4: 

    /** 		return haystack[1 .. lIdx - 1] & haystack[lIdx + 1 .. $]*/
    _2330 = _lIdx_4568 - 1;
    rhs_slice_target = (object_ptr)&_2331;
    RHS_Slice(_haystack_4567, 1, _2330);
    _2332 = _lIdx_4568 + 1;
    if (_2332 > MAXINT){
        _2332 = NewDouble((double)_2332);
    }
    if (IS_SEQUENCE(_haystack_4567)){
            _2333 = SEQ_PTR(_haystack_4567)->length;
    }
    else {
        _2333 = 1;
    }
    rhs_slice_target = (object_ptr)&_2334;
    RHS_Slice(_haystack_4567, _2332, _2333);
    Concat((object_ptr)&_2335, _2331, _2334);
    DeRefDS(_2331);
    _2331 = NOVALUE;
    DeRef(_2331);
    _2331 = NOVALUE;
    DeRefDS(_2334);
    _2334 = NOVALUE;
    DeRef(_needle_4566);
    DeRefDS(_haystack_4567);
    DeRef(_2324);
    _2324 = NOVALUE;
    DeRef(_2328);
    _2328 = NOVALUE;
    DeRef(_2329);
    _2329 = NOVALUE;
    _2330 = NOVALUE;
    DeRef(_2332);
    _2332 = NOVALUE;
    return _2335;
L3: 
    ;
}


int _21mid(int _source_4591, int _start_4592, int _len_4593)
{
    int _msg_inlined_crash_at_45_4608 = NOVALUE;
    int _data_inlined_crash_at_42_4607 = NOVALUE;
    int _2359 = NOVALUE;
    int _2358 = NOVALUE;
    int _2357 = NOVALUE;
    int _2356 = NOVALUE;
    int _2355 = NOVALUE;
    int _2353 = NOVALUE;
    int _2352 = NOVALUE;
    int _2351 = NOVALUE;
    int _2349 = NOVALUE;
    int _2347 = NOVALUE;
    int _2346 = NOVALUE;
    int _2345 = NOVALUE;
    int _2344 = NOVALUE;
    int _2343 = NOVALUE;
    int _2342 = NOVALUE;
    int _2341 = NOVALUE;
    int _2337 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if len<0 then*/
    if (binary_op_a(GREATEREQ, _len_4593, 0)){
        goto L1; // [5] 66
    }

    /** 		len += length(source)*/
    if (IS_SEQUENCE(_source_4591)){
            _2337 = SEQ_PTR(_source_4591)->length;
    }
    else {
        _2337 = 1;
    }
    _0 = _len_4593;
    if (IS_ATOM_INT(_len_4593)) {
        _len_4593 = _len_4593 + _2337;
        if ((long)((unsigned long)_len_4593 + (unsigned long)HIGH_BITS) >= 0) 
        _len_4593 = NewDouble((double)_len_4593);
    }
    else {
        _len_4593 = NewDouble(DBL_PTR(_len_4593)->dbl + (double)_2337);
    }
    DeRef(_0);
    _2337 = NOVALUE;

    /** 		if len<0 then*/
    if (binary_op_a(GREATEREQ, _len_4593, 0)){
        goto L2; // [20] 65
    }

    /** 			error:crash("mid(): len was %d and should be greater than %d.",*/
    if (IS_SEQUENCE(_source_4591)){
            _2341 = SEQ_PTR(_source_4591)->length;
    }
    else {
        _2341 = 1;
    }
    if (IS_ATOM_INT(_len_4593)) {
        _2342 = _len_4593 - _2341;
        if ((long)((unsigned long)_2342 +(unsigned long) HIGH_BITS) >= 0){
            _2342 = NewDouble((double)_2342);
        }
    }
    else {
        _2342 = NewDouble(DBL_PTR(_len_4593)->dbl - (double)_2341);
    }
    _2341 = NOVALUE;
    if (IS_SEQUENCE(_source_4591)){
            _2343 = SEQ_PTR(_source_4591)->length;
    }
    else {
        _2343 = 1;
    }
    _2344 = - _2343;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _2342;
    ((int *)_2)[2] = _2344;
    _2345 = MAKE_SEQ(_1);
    _2344 = NOVALUE;
    _2342 = NOVALUE;
    DeRef(_data_inlined_crash_at_42_4607);
    _data_inlined_crash_at_42_4607 = _2345;
    _2345 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_45_4608);
    _msg_inlined_crash_at_45_4608 = EPrintf(-9999999, _2340, _data_inlined_crash_at_42_4607);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_45_4608);

    /** end procedure*/
    goto L3; // [59] 62
L3: 
    DeRef(_data_inlined_crash_at_42_4607);
    _data_inlined_crash_at_42_4607 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_45_4608);
    _msg_inlined_crash_at_45_4608 = NOVALUE;
L2: 
L1: 

    /** 	if start > length(source) or len=0 then*/
    if (IS_SEQUENCE(_source_4591)){
            _2346 = SEQ_PTR(_source_4591)->length;
    }
    else {
        _2346 = 1;
    }
    if (IS_ATOM_INT(_start_4592)) {
        _2347 = (_start_4592 > _2346);
    }
    else {
        _2347 = (DBL_PTR(_start_4592)->dbl > (double)_2346);
    }
    _2346 = NOVALUE;
    if (_2347 != 0) {
        goto L4; // [75] 88
    }
    if (IS_ATOM_INT(_len_4593)) {
        _2349 = (_len_4593 == 0);
    }
    else {
        _2349 = (DBL_PTR(_len_4593)->dbl == (double)0);
    }
    if (_2349 == 0)
    {
        DeRef(_2349);
        _2349 = NOVALUE;
        goto L5; // [84] 95
    }
    else{
        DeRef(_2349);
        _2349 = NOVALUE;
    }
L4: 

    /** 		return ""*/
    RefDS(_5);
    DeRefDS(_source_4591);
    DeRef(_start_4592);
    DeRef(_len_4593);
    DeRef(_2347);
    _2347 = NOVALUE;
    return _5;
L5: 

    /** 	if start<1 then*/
    if (binary_op_a(GREATEREQ, _start_4592, 1)){
        goto L6; // [97] 107
    }

    /** 		start=1*/
    DeRef(_start_4592);
    _start_4592 = 1;
L6: 

    /** 	if start+len-1 >= length(source) then*/
    if (IS_ATOM_INT(_start_4592) && IS_ATOM_INT(_len_4593)) {
        _2351 = _start_4592 + _len_4593;
        if ((long)((unsigned long)_2351 + (unsigned long)HIGH_BITS) >= 0) 
        _2351 = NewDouble((double)_2351);
    }
    else {
        if (IS_ATOM_INT(_start_4592)) {
            _2351 = NewDouble((double)_start_4592 + DBL_PTR(_len_4593)->dbl);
        }
        else {
            if (IS_ATOM_INT(_len_4593)) {
                _2351 = NewDouble(DBL_PTR(_start_4592)->dbl + (double)_len_4593);
            }
            else
            _2351 = NewDouble(DBL_PTR(_start_4592)->dbl + DBL_PTR(_len_4593)->dbl);
        }
    }
    if (IS_ATOM_INT(_2351)) {
        _2352 = _2351 - 1;
        if ((long)((unsigned long)_2352 +(unsigned long) HIGH_BITS) >= 0){
            _2352 = NewDouble((double)_2352);
        }
    }
    else {
        _2352 = NewDouble(DBL_PTR(_2351)->dbl - (double)1);
    }
    DeRef(_2351);
    _2351 = NOVALUE;
    if (IS_SEQUENCE(_source_4591)){
            _2353 = SEQ_PTR(_source_4591)->length;
    }
    else {
        _2353 = 1;
    }
    if (binary_op_a(LESS, _2352, _2353)){
        DeRef(_2352);
        _2352 = NOVALUE;
        _2353 = NOVALUE;
        goto L7; // [120] 141
    }
    DeRef(_2352);
    _2352 = NOVALUE;
    _2353 = NOVALUE;

    /** 		return source[start..$]*/
    if (IS_SEQUENCE(_source_4591)){
            _2355 = SEQ_PTR(_source_4591)->length;
    }
    else {
        _2355 = 1;
    }
    rhs_slice_target = (object_ptr)&_2356;
    RHS_Slice(_source_4591, _start_4592, _2355);
    DeRefDS(_source_4591);
    DeRef(_start_4592);
    DeRef(_len_4593);
    DeRef(_2347);
    _2347 = NOVALUE;
    return _2356;
    goto L8; // [138] 161
L7: 

    /** 		return source[start..len+start-1]*/
    if (IS_ATOM_INT(_len_4593) && IS_ATOM_INT(_start_4592)) {
        _2357 = _len_4593 + _start_4592;
        if ((long)((unsigned long)_2357 + (unsigned long)HIGH_BITS) >= 0) 
        _2357 = NewDouble((double)_2357);
    }
    else {
        if (IS_ATOM_INT(_len_4593)) {
            _2357 = NewDouble((double)_len_4593 + DBL_PTR(_start_4592)->dbl);
        }
        else {
            if (IS_ATOM_INT(_start_4592)) {
                _2357 = NewDouble(DBL_PTR(_len_4593)->dbl + (double)_start_4592);
            }
            else
            _2357 = NewDouble(DBL_PTR(_len_4593)->dbl + DBL_PTR(_start_4592)->dbl);
        }
    }
    if (IS_ATOM_INT(_2357)) {
        _2358 = _2357 - 1;
    }
    else {
        _2358 = NewDouble(DBL_PTR(_2357)->dbl - (double)1);
    }
    DeRef(_2357);
    _2357 = NOVALUE;
    rhs_slice_target = (object_ptr)&_2359;
    RHS_Slice(_source_4591, _start_4592, _2358);
    DeRefDS(_source_4591);
    DeRef(_start_4592);
    DeRef(_len_4593);
    DeRef(_2347);
    _2347 = NOVALUE;
    DeRef(_2356);
    _2356 = NOVALUE;
    DeRef(_2358);
    _2358 = NOVALUE;
    return _2359;
L8: 
    ;
}


int _21slice(int _source_4629, int _start_4630, int _stop_4631)
{
    int _2368 = NOVALUE;
    int _2363 = NOVALUE;
    int _2361 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if stop < 1 then */
    if (binary_op_a(GREATEREQ, _stop_4631, 1)){
        goto L1; // [5] 21
    }

    /** 		stop += length(source) */
    if (IS_SEQUENCE(_source_4629)){
            _2361 = SEQ_PTR(_source_4629)->length;
    }
    else {
        _2361 = 1;
    }
    _0 = _stop_4631;
    if (IS_ATOM_INT(_stop_4631)) {
        _stop_4631 = _stop_4631 + _2361;
        if ((long)((unsigned long)_stop_4631 + (unsigned long)HIGH_BITS) >= 0) 
        _stop_4631 = NewDouble((double)_stop_4631);
    }
    else {
        _stop_4631 = NewDouble(DBL_PTR(_stop_4631)->dbl + (double)_2361);
    }
    DeRef(_0);
    _2361 = NOVALUE;
    goto L2; // [18] 37
L1: 

    /** 	elsif stop > length(source) then */
    if (IS_SEQUENCE(_source_4629)){
            _2363 = SEQ_PTR(_source_4629)->length;
    }
    else {
        _2363 = 1;
    }
    if (binary_op_a(LESSEQ, _stop_4631, _2363)){
        _2363 = NOVALUE;
        goto L3; // [26] 36
    }
    _2363 = NOVALUE;

    /** 		stop = length(source) */
    DeRef(_stop_4631);
    if (IS_SEQUENCE(_source_4629)){
            _stop_4631 = SEQ_PTR(_source_4629)->length;
    }
    else {
        _stop_4631 = 1;
    }
L3: 
L2: 

    /** 	if start < 1 then */
    if (binary_op_a(GREATEREQ, _start_4630, 1)){
        goto L4; // [39] 49
    }

    /** 		start = 1 */
    DeRef(_start_4630);
    _start_4630 = 1;
L4: 

    /** 	if start > stop then*/
    if (binary_op_a(LESSEQ, _start_4630, _stop_4631)){
        goto L5; // [51] 62
    }

    /** 		return ""*/
    RefDS(_5);
    DeRefDS(_source_4629);
    DeRef(_start_4630);
    DeRef(_stop_4631);
    return _5;
L5: 

    /** 	return source[start..stop]*/
    rhs_slice_target = (object_ptr)&_2368;
    RHS_Slice(_source_4629, _start_4630, _stop_4631);
    DeRefDS(_source_4629);
    DeRef(_start_4630);
    DeRef(_stop_4631);
    return _2368;
    ;
}


int _21vslice(int _source_4647, int _colno_4648, int _error_control_4649)
{
    int _substitutes_4650 = NOVALUE;
    int _current_sub_4651 = NOVALUE;
    int _msg_inlined_crash_at_10_4656 = NOVALUE;
    int _msg_inlined_crash_at_109_4676 = NOVALUE;
    int _data_inlined_crash_at_106_4675 = NOVALUE;
    int _2392 = NOVALUE;
    int _2391 = NOVALUE;
    int _2390 = NOVALUE;
    int _2389 = NOVALUE;
    int _2388 = NOVALUE;
    int _2386 = NOVALUE;
    int _2384 = NOVALUE;
    int _2383 = NOVALUE;
    int _2381 = NOVALUE;
    int _2377 = NOVALUE;
    int _2376 = NOVALUE;
    int _2375 = NOVALUE;
    int _2372 = NOVALUE;
    int _2371 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if colno < 1 then*/
    if (binary_op_a(GREATEREQ, _colno_4648, 1)){
        goto L1; // [5] 30
    }

    /** 		error:crash("sequence:vslice(): colno should be a valid index, but was %d",colno)*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_10_4656);
    _msg_inlined_crash_at_10_4656 = EPrintf(-9999999, _2370, _colno_4648);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_10_4656);

    /** end procedure*/
    goto L2; // [24] 27
L2: 
    DeRefi(_msg_inlined_crash_at_10_4656);
    _msg_inlined_crash_at_10_4656 = NOVALUE;
L1: 

    /** 	if atom(error_control) then*/
    _2371 = IS_ATOM(_error_control_4649);
    if (_2371 == 0)
    {
        _2371 = NOVALUE;
        goto L3; // [35] 53
    }
    else{
        _2371 = NOVALUE;
    }

    /** 		substitutes =-(not error_control)*/
    if (IS_ATOM_INT(_error_control_4649)) {
        _2372 = (_error_control_4649 == 0);
    }
    else {
        _2372 = unary_op(NOT, _error_control_4649);
    }
    if (IS_ATOM_INT(_2372)) {
        if ((unsigned long)_2372 == 0xC0000000)
        _substitutes_4650 = (int)NewDouble((double)-0xC0000000);
        else
        _substitutes_4650 = - _2372;
    }
    else {
        _substitutes_4650 = unary_op(UMINUS, _2372);
    }
    DeRef(_2372);
    _2372 = NOVALUE;
    if (!IS_ATOM_INT(_substitutes_4650)) {
        _1 = (long)(DBL_PTR(_substitutes_4650)->dbl);
        if (UNIQUE(DBL_PTR(_substitutes_4650)) && (DBL_PTR(_substitutes_4650)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_substitutes_4650);
        _substitutes_4650 = _1;
    }
    goto L4; // [50] 68
L3: 

    /** 		substitutes = length(error_control)*/
    if (IS_SEQUENCE(_error_control_4649)){
            _substitutes_4650 = SEQ_PTR(_error_control_4649)->length;
    }
    else {
        _substitutes_4650 = 1;
    }

    /** 		current_sub = 0*/
    _current_sub_4651 = 0;
L4: 

    /** 	for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_4647)){
            _2375 = SEQ_PTR(_source_4647)->length;
    }
    else {
        _2375 = 1;
    }
    {
        int _i_4664;
        _i_4664 = 1;
L5: 
        if (_i_4664 > _2375){
            goto L6; // [73] 231
        }

        /** 		if colno > length(source[i]) then*/
        _2 = (int)SEQ_PTR(_source_4647);
        _2376 = (int)*(((s1_ptr)_2)->base + _i_4664);
        if (IS_SEQUENCE(_2376)){
                _2377 = SEQ_PTR(_2376)->length;
        }
        else {
            _2377 = 1;
        }
        _2376 = NOVALUE;
        if (binary_op_a(LESSEQ, _colno_4648, _2377)){
            _2377 = NOVALUE;
            goto L7; // [89] 196
        }
        _2377 = NOVALUE;

        /** 			if substitutes = -1 then*/
        if (_substitutes_4650 != -1)
        goto L8; // [97] 131

        /** 				error:crash("sequence:vslice(): colno should be a valid index on the %d-th element, but was %d", {i, colno})*/
        Ref(_colno_4648);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_4664;
        ((int *)_2)[2] = _colno_4648;
        _2381 = MAKE_SEQ(_1);
        DeRef(_data_inlined_crash_at_106_4675);
        _data_inlined_crash_at_106_4675 = _2381;
        _2381 = NOVALUE;

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_109_4676);
        _msg_inlined_crash_at_109_4676 = EPrintf(-9999999, _2380, _data_inlined_crash_at_106_4675);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_109_4676);

        /** end procedure*/
        goto L9; // [123] 126
L9: 
        DeRef(_data_inlined_crash_at_106_4675);
        _data_inlined_crash_at_106_4675 = NOVALUE;
        DeRefi(_msg_inlined_crash_at_109_4676);
        _msg_inlined_crash_at_109_4676 = NOVALUE;
        goto LA; // [128] 224
L8: 

        /** 			elsif substitutes = 0 then*/
        if (_substitutes_4650 != 0)
        goto LB; // [133] 155

        /** 				return source[1..i-1]*/
        _2383 = _i_4664 - 1;
        rhs_slice_target = (object_ptr)&_2384;
        RHS_Slice(_source_4647, 1, _2383);
        DeRefDS(_source_4647);
        DeRef(_colno_4648);
        DeRef(_error_control_4649);
        _2376 = NOVALUE;
        _2383 = NOVALUE;
        return _2384;
        goto LA; // [152] 224
LB: 

        /** 				current_sub += 1*/
        _current_sub_4651 = _current_sub_4651 + 1;

        /** 				if current_sub > length(error_control) then*/
        if (IS_SEQUENCE(_error_control_4649)){
                _2386 = SEQ_PTR(_error_control_4649)->length;
        }
        else {
            _2386 = 1;
        }
        if (_current_sub_4651 <= _2386)
        goto LC; // [170] 182

        /** 					current_sub = 1*/
        _current_sub_4651 = 1;
LC: 

        /** 				source[i] = error_control[current_sub]*/
        _2 = (int)SEQ_PTR(_error_control_4649);
        _2388 = (int)*(((s1_ptr)_2)->base + _current_sub_4651);
        Ref(_2388);
        _2 = (int)SEQ_PTR(_source_4647);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_4647 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4664);
        _1 = *(int *)_2;
        *(int *)_2 = _2388;
        if( _1 != _2388 ){
            DeRef(_1);
        }
        _2388 = NOVALUE;
        goto LA; // [193] 224
L7: 

        /** 			if sequence(source[i]) then*/
        _2 = (int)SEQ_PTR(_source_4647);
        _2389 = (int)*(((s1_ptr)_2)->base + _i_4664);
        _2390 = IS_SEQUENCE(_2389);
        _2389 = NOVALUE;
        if (_2390 == 0)
        {
            _2390 = NOVALUE;
            goto LD; // [205] 223
        }
        else{
            _2390 = NOVALUE;
        }

        /** 				source[i] = source[i][colno]*/
        _2 = (int)SEQ_PTR(_source_4647);
        _2391 = (int)*(((s1_ptr)_2)->base + _i_4664);
        _2 = (int)SEQ_PTR(_2391);
        if (!IS_ATOM_INT(_colno_4648)){
            _2392 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_colno_4648)->dbl));
        }
        else{
            _2392 = (int)*(((s1_ptr)_2)->base + _colno_4648);
        }
        _2391 = NOVALUE;
        Ref(_2392);
        _2 = (int)SEQ_PTR(_source_4647);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _source_4647 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_4664);
        _1 = *(int *)_2;
        *(int *)_2 = _2392;
        if( _1 != _2392 ){
            DeRef(_1);
        }
        _2392 = NOVALUE;
LD: 
LA: 

        /** 	end for*/
        _i_4664 = _i_4664 + 1;
        goto L5; // [226] 80
L6: 
        ;
    }

    /** 	return source*/
    DeRef(_colno_4648);
    DeRef(_error_control_4649);
    _2376 = NOVALUE;
    DeRef(_2383);
    _2383 = NOVALUE;
    DeRef(_2384);
    _2384 = NOVALUE;
    return _source_4647;
    ;
}


int _21patch(int _target_4695, int _source_4696, int _start_4697, int _filler_4698)
{
    int _2430 = NOVALUE;
    int _2429 = NOVALUE;
    int _2428 = NOVALUE;
    int _2427 = NOVALUE;
    int _2426 = NOVALUE;
    int _2425 = NOVALUE;
    int _2424 = NOVALUE;
    int _2423 = NOVALUE;
    int _2421 = NOVALUE;
    int _2420 = NOVALUE;
    int _2418 = NOVALUE;
    int _2417 = NOVALUE;
    int _2416 = NOVALUE;
    int _2415 = NOVALUE;
    int _2414 = NOVALUE;
    int _2413 = NOVALUE;
    int _2412 = NOVALUE;
    int _2411 = NOVALUE;
    int _2410 = NOVALUE;
    int _2409 = NOVALUE;
    int _2408 = NOVALUE;
    int _2407 = NOVALUE;
    int _2404 = NOVALUE;
    int _2403 = NOVALUE;
    int _2402 = NOVALUE;
    int _2401 = NOVALUE;
    int _2400 = NOVALUE;
    int _2399 = NOVALUE;
    int _2398 = NOVALUE;
    int _2397 = NOVALUE;
    int _2396 = NOVALUE;
    int _2394 = NOVALUE;
    int _2393 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_4697)) {
        _1 = (long)(DBL_PTR(_start_4697)->dbl);
        if (UNIQUE(DBL_PTR(_start_4697)) && (DBL_PTR(_start_4697)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_4697);
        _start_4697 = _1;
    }

    /** 	if start + length(source) <= 0 then*/
    if (IS_SEQUENCE(_source_4696)){
            _2393 = SEQ_PTR(_source_4696)->length;
    }
    else {
        _2393 = 1;
    }
    _2394 = _start_4697 + _2393;
    if ((long)((unsigned long)_2394 + (unsigned long)HIGH_BITS) >= 0) 
    _2394 = NewDouble((double)_2394);
    _2393 = NOVALUE;
    if (binary_op_a(GREATER, _2394, 0)){
        DeRef(_2394);
        _2394 = NOVALUE;
        goto L1; // [18] 55
    }
    DeRef(_2394);
    _2394 = NOVALUE;

    /** 		return source & repeat(filler, -start-length(source))+1 & target*/
    if ((unsigned long)_start_4697 == 0xC0000000)
    _2396 = (int)NewDouble((double)-0xC0000000);
    else
    _2396 = - _start_4697;
    if (IS_SEQUENCE(_source_4696)){
            _2397 = SEQ_PTR(_source_4696)->length;
    }
    else {
        _2397 = 1;
    }
    if (IS_ATOM_INT(_2396)) {
        _2398 = _2396 - _2397;
    }
    else {
        _2398 = NewDouble(DBL_PTR(_2396)->dbl - (double)_2397);
    }
    DeRef(_2396);
    _2396 = NOVALUE;
    _2397 = NOVALUE;
    _2399 = Repeat(_filler_4698, _2398);
    DeRef(_2398);
    _2398 = NOVALUE;
    _2400 = binary_op(PLUS, 1, _2399);
    DeRefDS(_2399);
    _2399 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _target_4695;
        concat_list[1] = _2400;
        concat_list[2] = _source_4696;
        Concat_N((object_ptr)&_2401, concat_list, 3);
    }
    DeRefDS(_2400);
    _2400 = NOVALUE;
    DeRefDS(_target_4695);
    DeRefDS(_source_4696);
    DeRef(_filler_4698);
    return _2401;
    goto L2; // [52] 223
L1: 

    /** 	elsif start + length(source) <= length(target) then*/
    if (IS_SEQUENCE(_source_4696)){
            _2402 = SEQ_PTR(_source_4696)->length;
    }
    else {
        _2402 = 1;
    }
    _2403 = _start_4697 + _2402;
    if ((long)((unsigned long)_2403 + (unsigned long)HIGH_BITS) >= 0) 
    _2403 = NewDouble((double)_2403);
    _2402 = NOVALUE;
    if (IS_SEQUENCE(_target_4695)){
            _2404 = SEQ_PTR(_target_4695)->length;
    }
    else {
        _2404 = 1;
    }
    if (binary_op_a(GREATER, _2403, _2404)){
        DeRef(_2403);
        _2403 = NOVALUE;
        _2404 = NOVALUE;
        goto L3; // [67] 145
    }
    DeRef(_2403);
    _2403 = NOVALUE;
    _2404 = NOVALUE;

    /** 		if start<=0 then*/
    if (_start_4697 > 0)
    goto L4; // [73] 105

    /** 			return source & target[start+length(source)..$]*/
    if (IS_SEQUENCE(_source_4696)){
            _2407 = SEQ_PTR(_source_4696)->length;
    }
    else {
        _2407 = 1;
    }
    _2408 = _start_4697 + _2407;
    if ((long)((unsigned long)_2408 + (unsigned long)HIGH_BITS) >= 0) 
    _2408 = NewDouble((double)_2408);
    _2407 = NOVALUE;
    if (IS_SEQUENCE(_target_4695)){
            _2409 = SEQ_PTR(_target_4695)->length;
    }
    else {
        _2409 = 1;
    }
    rhs_slice_target = (object_ptr)&_2410;
    RHS_Slice(_target_4695, _2408, _2409);
    Concat((object_ptr)&_2411, _source_4696, _2410);
    DeRefDS(_2410);
    _2410 = NOVALUE;
    DeRefDS(_target_4695);
    DeRefDS(_source_4696);
    DeRef(_filler_4698);
    DeRef(_2401);
    _2401 = NOVALUE;
    DeRef(_2408);
    _2408 = NOVALUE;
    return _2411;
    goto L2; // [102] 223
L4: 

    /**         	return target[1..start-1] & source &  target[start+length(source)..$]*/
    _2412 = _start_4697 - 1;
    rhs_slice_target = (object_ptr)&_2413;
    RHS_Slice(_target_4695, 1, _2412);
    if (IS_SEQUENCE(_source_4696)){
            _2414 = SEQ_PTR(_source_4696)->length;
    }
    else {
        _2414 = 1;
    }
    _2415 = _start_4697 + _2414;
    if ((long)((unsigned long)_2415 + (unsigned long)HIGH_BITS) >= 0) 
    _2415 = NewDouble((double)_2415);
    _2414 = NOVALUE;
    if (IS_SEQUENCE(_target_4695)){
            _2416 = SEQ_PTR(_target_4695)->length;
    }
    else {
        _2416 = 1;
    }
    rhs_slice_target = (object_ptr)&_2417;
    RHS_Slice(_target_4695, _2415, _2416);
    {
        int concat_list[3];

        concat_list[0] = _2417;
        concat_list[1] = _source_4696;
        concat_list[2] = _2413;
        Concat_N((object_ptr)&_2418, concat_list, 3);
    }
    DeRefDS(_2417);
    _2417 = NOVALUE;
    DeRefDS(_2413);
    _2413 = NOVALUE;
    DeRefDS(_target_4695);
    DeRefDS(_source_4696);
    DeRef(_filler_4698);
    DeRef(_2401);
    _2401 = NOVALUE;
    DeRef(_2408);
    _2408 = NOVALUE;
    DeRef(_2411);
    _2411 = NOVALUE;
    _2412 = NOVALUE;
    DeRef(_2415);
    _2415 = NOVALUE;
    return _2418;
    goto L2; // [142] 223
L3: 

    /** 	elsif start <= 1 then*/
    if (_start_4697 > 1)
    goto L5; // [147] 160

    /** 		return source*/
    DeRefDS(_target_4695);
    DeRef(_filler_4698);
    DeRef(_2401);
    _2401 = NOVALUE;
    DeRef(_2408);
    _2408 = NOVALUE;
    DeRef(_2411);
    _2411 = NOVALUE;
    DeRef(_2412);
    _2412 = NOVALUE;
    DeRef(_2418);
    _2418 = NOVALUE;
    DeRef(_2415);
    _2415 = NOVALUE;
    return _source_4696;
    goto L2; // [157] 223
L5: 

    /** 	elsif start <= length(target)+1 then*/
    if (IS_SEQUENCE(_target_4695)){
            _2420 = SEQ_PTR(_target_4695)->length;
    }
    else {
        _2420 = 1;
    }
    _2421 = _2420 + 1;
    _2420 = NOVALUE;
    if (_start_4697 > _2421)
    goto L6; // [169] 195

    /** 		return target[1..start-1] & source*/
    _2423 = _start_4697 - 1;
    rhs_slice_target = (object_ptr)&_2424;
    RHS_Slice(_target_4695, 1, _2423);
    Concat((object_ptr)&_2425, _2424, _source_4696);
    DeRefDS(_2424);
    _2424 = NOVALUE;
    DeRef(_2424);
    _2424 = NOVALUE;
    DeRefDS(_target_4695);
    DeRefDS(_source_4696);
    DeRef(_filler_4698);
    DeRef(_2401);
    _2401 = NOVALUE;
    DeRef(_2408);
    _2408 = NOVALUE;
    DeRef(_2411);
    _2411 = NOVALUE;
    DeRef(_2412);
    _2412 = NOVALUE;
    DeRef(_2418);
    _2418 = NOVALUE;
    DeRef(_2415);
    _2415 = NOVALUE;
    _2421 = NOVALUE;
    _2423 = NOVALUE;
    return _2425;
    goto L2; // [192] 223
L6: 

    /** 		return target & repeat(filler,start-length(target)-1) & source*/
    if (IS_SEQUENCE(_target_4695)){
            _2426 = SEQ_PTR(_target_4695)->length;
    }
    else {
        _2426 = 1;
    }
    _2427 = _start_4697 - _2426;
    if ((long)((unsigned long)_2427 +(unsigned long) HIGH_BITS) >= 0){
        _2427 = NewDouble((double)_2427);
    }
    _2426 = NOVALUE;
    if (IS_ATOM_INT(_2427)) {
        _2428 = _2427 - 1;
    }
    else {
        _2428 = NewDouble(DBL_PTR(_2427)->dbl - (double)1);
    }
    DeRef(_2427);
    _2427 = NOVALUE;
    _2429 = Repeat(_filler_4698, _2428);
    DeRef(_2428);
    _2428 = NOVALUE;
    {
        int concat_list[3];

        concat_list[0] = _source_4696;
        concat_list[1] = _2429;
        concat_list[2] = _target_4695;
        Concat_N((object_ptr)&_2430, concat_list, 3);
    }
    DeRefDS(_2429);
    _2429 = NOVALUE;
    DeRefDS(_target_4695);
    DeRefDS(_source_4696);
    DeRef(_filler_4698);
    DeRef(_2401);
    _2401 = NOVALUE;
    DeRef(_2408);
    _2408 = NOVALUE;
    DeRef(_2411);
    _2411 = NOVALUE;
    DeRef(_2412);
    _2412 = NOVALUE;
    DeRef(_2418);
    _2418 = NOVALUE;
    DeRef(_2415);
    _2415 = NOVALUE;
    DeRef(_2421);
    _2421 = NOVALUE;
    DeRef(_2423);
    _2423 = NOVALUE;
    DeRef(_2425);
    _2425 = NOVALUE;
    return _2430;
L2: 
    ;
}


int _21remove_all(int _needle_4746, int _haystack_4747)
{
    int _found_4748 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer found = 1*/
    _found_4748 = 1;

    /** 	while found entry do*/
    goto L1; // [12] 28
L2: 
    if (_found_4748 == 0)
    {
        goto L3; // [15] 42
    }
    else{
    }

    /** 		haystack = remove( haystack, found )*/
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_4747);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_found_4748)) ? _found_4748 : (long)(DBL_PTR(_found_4748)->dbl);
        int stop = (IS_ATOM_INT(_found_4748)) ? _found_4748 : (long)(DBL_PTR(_found_4748)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_4747), start, &_haystack_4747 );
            }
            else Tail(SEQ_PTR(_haystack_4747), stop+1, &_haystack_4747);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_4747), start, &_haystack_4747);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_4747 = Remove_elements(start, stop, (SEQ_PTR(_haystack_4747)->ref == 1));
        }
    }

    /** 	entry*/
L1: 

    /** 		found = find( needle, haystack, found )*/
    _found_4748 = find_from(_needle_4746, _haystack_4747, _found_4748);

    /** 	end while*/
    goto L2; // [39] 15
L3: 

    /** 	return haystack*/
    DeRef(_needle_4746);
    return _haystack_4747;
    ;
}


int _21retain_all(int _needles_4754, int _haystack_4755)
{
    int _lp_4756 = NOVALUE;
    int _np_4757 = NOVALUE;
    int _result_4758 = NOVALUE;
    int _2448 = NOVALUE;
    int _2445 = NOVALUE;
    int _2444 = NOVALUE;
    int _2442 = NOVALUE;
    int _2441 = NOVALUE;
    int _2440 = NOVALUE;
    int _2437 = NOVALUE;
    int _2435 = NOVALUE;
    int _2433 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(needles) then*/
    _2433 = IS_ATOM(_needles_4754);
    if (_2433 == 0)
    {
        _2433 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _2433 = NOVALUE;
    }

    /** 		needles = {needles}*/
    _0 = _needles_4754;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needles_4754);
    *((int *)(_2+4)) = _needles_4754;
    _needles_4754 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	if length(needles) = 0 then*/
    if (IS_SEQUENCE(_needles_4754)){
            _2435 = SEQ_PTR(_needles_4754)->length;
    }
    else {
        _2435 = 1;
    }
    if (_2435 != 0)
    goto L2; // [23] 34

    /** 		return {}*/
    RefDS(_5);
    DeRef(_needles_4754);
    DeRefDS(_haystack_4755);
    DeRef(_result_4758);
    return _5;
L2: 

    /** 	if length(haystack) = 0 then*/
    if (IS_SEQUENCE(_haystack_4755)){
            _2437 = SEQ_PTR(_haystack_4755)->length;
    }
    else {
        _2437 = 1;
    }
    if (_2437 != 0)
    goto L3; // [39] 50

    /** 		return {}*/
    RefDS(_5);
    DeRef(_needles_4754);
    DeRefDS(_haystack_4755);
    DeRef(_result_4758);
    return _5;
L3: 

    /** 	result = haystack*/
    RefDS(_haystack_4755);
    DeRef(_result_4758);
    _result_4758 = _haystack_4755;

    /** 	lp = length(haystack)*/
    if (IS_SEQUENCE(_haystack_4755)){
            _lp_4756 = SEQ_PTR(_haystack_4755)->length;
    }
    else {
        _lp_4756 = 1;
    }

    /** 	np = 1*/
    _np_4757 = 1;

    /** 	for i = 1 to length(haystack) do*/
    if (IS_SEQUENCE(_haystack_4755)){
            _2440 = SEQ_PTR(_haystack_4755)->length;
    }
    else {
        _2440 = 1;
    }
    {
        int _i_4770;
        _i_4770 = 1;
L4: 
        if (_i_4770 > _2440){
            goto L5; // [76] 146
        }

        /** 		if find(haystack[i], needles) then*/
        _2 = (int)SEQ_PTR(_haystack_4755);
        _2441 = (int)*(((s1_ptr)_2)->base + _i_4770);
        _2442 = find_from(_2441, _needles_4754, 1);
        _2441 = NOVALUE;
        if (_2442 == 0)
        {
            _2442 = NOVALUE;
            goto L6; // [94] 130
        }
        else{
            _2442 = NOVALUE;
        }

        /** 			if np < i then*/
        if (_np_4757 >= _i_4770)
        goto L7; // [99] 119

        /** 				result[np .. lp] = haystack[i..$]*/
        if (IS_SEQUENCE(_haystack_4755)){
                _2444 = SEQ_PTR(_haystack_4755)->length;
        }
        else {
            _2444 = 1;
        }
        rhs_slice_target = (object_ptr)&_2445;
        RHS_Slice(_haystack_4755, _i_4770, _2444);
        assign_slice_seq = (s1_ptr *)&_result_4758;
        AssignSlice(_np_4757, _lp_4756, _2445);
        DeRefDS(_2445);
        _2445 = NOVALUE;
L7: 

        /** 			np += 1*/
        _np_4757 = _np_4757 + 1;
        goto L8; // [127] 139
L6: 

        /** 			lp -= 1*/
        _lp_4756 = _lp_4756 - 1;
L8: 

        /** 	end for*/
        _i_4770 = _i_4770 + 1;
        goto L4; // [141] 83
L5: 
        ;
    }

    /** 	return result[1 .. lp]*/
    rhs_slice_target = (object_ptr)&_2448;
    RHS_Slice(_result_4758, 1, _lp_4756);
    DeRef(_needles_4754);
    DeRefDS(_haystack_4755);
    DeRefDS(_result_4758);
    return _2448;
    ;
}


int _21filter(int _source_4785, int _rid_4786, int _userdata_4787, int _rangetype_4788)
{
    int _dest_4789 = NOVALUE;
    int _idx_4790 = NOVALUE;
    int _2623 = NOVALUE;
    int _2622 = NOVALUE;
    int _2620 = NOVALUE;
    int _2619 = NOVALUE;
    int _2618 = NOVALUE;
    int _2617 = NOVALUE;
    int _2616 = NOVALUE;
    int _2613 = NOVALUE;
    int _2612 = NOVALUE;
    int _2611 = NOVALUE;
    int _2610 = NOVALUE;
    int _2607 = NOVALUE;
    int _2606 = NOVALUE;
    int _2605 = NOVALUE;
    int _2604 = NOVALUE;
    int _2603 = NOVALUE;
    int _2600 = NOVALUE;
    int _2599 = NOVALUE;
    int _2598 = NOVALUE;
    int _2597 = NOVALUE;
    int _2594 = NOVALUE;
    int _2593 = NOVALUE;
    int _2592 = NOVALUE;
    int _2591 = NOVALUE;
    int _2590 = NOVALUE;
    int _2587 = NOVALUE;
    int _2586 = NOVALUE;
    int _2585 = NOVALUE;
    int _2584 = NOVALUE;
    int _2581 = NOVALUE;
    int _2580 = NOVALUE;
    int _2579 = NOVALUE;
    int _2578 = NOVALUE;
    int _2577 = NOVALUE;
    int _2574 = NOVALUE;
    int _2573 = NOVALUE;
    int _2572 = NOVALUE;
    int _2571 = NOVALUE;
    int _2568 = NOVALUE;
    int _2567 = NOVALUE;
    int _2566 = NOVALUE;
    int _2565 = NOVALUE;
    int _2564 = NOVALUE;
    int _2561 = NOVALUE;
    int _2560 = NOVALUE;
    int _2559 = NOVALUE;
    int _2555 = NOVALUE;
    int _2552 = NOVALUE;
    int _2551 = NOVALUE;
    int _2550 = NOVALUE;
    int _2548 = NOVALUE;
    int _2547 = NOVALUE;
    int _2546 = NOVALUE;
    int _2545 = NOVALUE;
    int _2544 = NOVALUE;
    int _2541 = NOVALUE;
    int _2540 = NOVALUE;
    int _2539 = NOVALUE;
    int _2537 = NOVALUE;
    int _2536 = NOVALUE;
    int _2535 = NOVALUE;
    int _2534 = NOVALUE;
    int _2533 = NOVALUE;
    int _2530 = NOVALUE;
    int _2529 = NOVALUE;
    int _2528 = NOVALUE;
    int _2526 = NOVALUE;
    int _2525 = NOVALUE;
    int _2524 = NOVALUE;
    int _2523 = NOVALUE;
    int _2522 = NOVALUE;
    int _2519 = NOVALUE;
    int _2518 = NOVALUE;
    int _2517 = NOVALUE;
    int _2515 = NOVALUE;
    int _2514 = NOVALUE;
    int _2513 = NOVALUE;
    int _2512 = NOVALUE;
    int _2511 = NOVALUE;
    int _2509 = NOVALUE;
    int _2508 = NOVALUE;
    int _2507 = NOVALUE;
    int _2503 = NOVALUE;
    int _2500 = NOVALUE;
    int _2499 = NOVALUE;
    int _2498 = NOVALUE;
    int _2495 = NOVALUE;
    int _2492 = NOVALUE;
    int _2491 = NOVALUE;
    int _2490 = NOVALUE;
    int _2487 = NOVALUE;
    int _2484 = NOVALUE;
    int _2483 = NOVALUE;
    int _2482 = NOVALUE;
    int _2479 = NOVALUE;
    int _2476 = NOVALUE;
    int _2475 = NOVALUE;
    int _2474 = NOVALUE;
    int _2470 = NOVALUE;
    int _2467 = NOVALUE;
    int _2466 = NOVALUE;
    int _2465 = NOVALUE;
    int _2462 = NOVALUE;
    int _2459 = NOVALUE;
    int _2458 = NOVALUE;
    int _2457 = NOVALUE;
    int _2451 = NOVALUE;
    int _2449 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(source) = 0 then*/
    if (IS_SEQUENCE(_source_4785)){
            _2449 = SEQ_PTR(_source_4785)->length;
    }
    else {
        _2449 = 1;
    }
    if (_2449 != 0)
    goto L1; // [8] 19

    /** 		return source*/
    DeRef(_rid_4786);
    DeRef(_userdata_4787);
    DeRef(_rangetype_4788);
    DeRef(_dest_4789);
    return _source_4785;
L1: 

    /** 	dest = repeat(0, length(source))*/
    if (IS_SEQUENCE(_source_4785)){
            _2451 = SEQ_PTR(_source_4785)->length;
    }
    else {
        _2451 = 1;
    }
    DeRef(_dest_4789);
    _dest_4789 = Repeat(0, _2451);
    _2451 = NOVALUE;

    /** 	idx = 0*/
    _idx_4790 = 0;

    /** 	switch rid do*/
    _1 = find(_rid_4786, _2453);
    switch ( _1 ){ 

        /** 		case "<", "lt" then*/
        case 1:
        case 2:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4785)){
                _2457 = SEQ_PTR(_source_4785)->length;
        }
        else {
            _2457 = 1;
        }
        {
            int _a_4802;
            _a_4802 = 1;
L2: 
            if (_a_4802 > _2457){
                goto L3; // [53] 100
            }

            /** 				if compare(source[a], userdata) < 0 then*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2458 = (int)*(((s1_ptr)_2)->base + _a_4802);
            if (IS_ATOM_INT(_2458) && IS_ATOM_INT(_userdata_4787)){
                _2459 = (_2458 < _userdata_4787) ? -1 : (_2458 > _userdata_4787);
            }
            else{
                _2459 = compare(_2458, _userdata_4787);
            }
            _2458 = NOVALUE;
            if (_2459 >= 0)
            goto L4; // [70] 93

            /** 					idx += 1*/
            _idx_4790 = _idx_4790 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2462 = (int)*(((s1_ptr)_2)->base + _a_4802);
            Ref(_2462);
            _2 = (int)SEQ_PTR(_dest_4789);
            _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
            _1 = *(int *)_2;
            *(int *)_2 = _2462;
            if( _1 != _2462 ){
                DeRef(_1);
            }
            _2462 = NOVALUE;
L4: 

            /** 			end for*/
            _a_4802 = _a_4802 + 1;
            goto L2; // [95] 60
L3: 
            ;
        }
        goto L5; // [100] 1348

        /** 		case "<=", "le" then*/
        case 3:
        case 4:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4785)){
                _2465 = SEQ_PTR(_source_4785)->length;
        }
        else {
            _2465 = 1;
        }
        {
            int _a_4814;
            _a_4814 = 1;
L6: 
            if (_a_4814 > _2465){
                goto L7; // [113] 160
            }

            /** 				if compare(source[a], userdata) <= 0 then*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2466 = (int)*(((s1_ptr)_2)->base + _a_4814);
            if (IS_ATOM_INT(_2466) && IS_ATOM_INT(_userdata_4787)){
                _2467 = (_2466 < _userdata_4787) ? -1 : (_2466 > _userdata_4787);
            }
            else{
                _2467 = compare(_2466, _userdata_4787);
            }
            _2466 = NOVALUE;
            if (_2467 > 0)
            goto L8; // [130] 153

            /** 					idx += 1*/
            _idx_4790 = _idx_4790 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2470 = (int)*(((s1_ptr)_2)->base + _a_4814);
            Ref(_2470);
            _2 = (int)SEQ_PTR(_dest_4789);
            _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
            _1 = *(int *)_2;
            *(int *)_2 = _2470;
            if( _1 != _2470 ){
                DeRef(_1);
            }
            _2470 = NOVALUE;
L8: 

            /** 			end for*/
            _a_4814 = _a_4814 + 1;
            goto L6; // [155] 120
L7: 
            ;
        }
        goto L5; // [160] 1348

        /** 		case "=", "==", "eq" then*/
        case 5:
        case 6:
        case 7:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4785)){
                _2474 = SEQ_PTR(_source_4785)->length;
        }
        else {
            _2474 = 1;
        }
        {
            int _a_4827;
            _a_4827 = 1;
L9: 
            if (_a_4827 > _2474){
                goto LA; // [175] 222
            }

            /** 				if compare(source[a], userdata) = 0 then*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2475 = (int)*(((s1_ptr)_2)->base + _a_4827);
            if (IS_ATOM_INT(_2475) && IS_ATOM_INT(_userdata_4787)){
                _2476 = (_2475 < _userdata_4787) ? -1 : (_2475 > _userdata_4787);
            }
            else{
                _2476 = compare(_2475, _userdata_4787);
            }
            _2475 = NOVALUE;
            if (_2476 != 0)
            goto LB; // [192] 215

            /** 					idx += 1*/
            _idx_4790 = _idx_4790 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2479 = (int)*(((s1_ptr)_2)->base + _a_4827);
            Ref(_2479);
            _2 = (int)SEQ_PTR(_dest_4789);
            _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
            _1 = *(int *)_2;
            *(int *)_2 = _2479;
            if( _1 != _2479 ){
                DeRef(_1);
            }
            _2479 = NOVALUE;
LB: 

            /** 			end for*/
            _a_4827 = _a_4827 + 1;
            goto L9; // [217] 182
LA: 
            ;
        }
        goto L5; // [222] 1348

        /** 		case "!=", "ne" then*/
        case 8:
        case 9:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4785)){
                _2482 = SEQ_PTR(_source_4785)->length;
        }
        else {
            _2482 = 1;
        }
        {
            int _a_4839;
            _a_4839 = 1;
LC: 
            if (_a_4839 > _2482){
                goto LD; // [235] 282
            }

            /** 				if compare(source[a], userdata) != 0 then*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2483 = (int)*(((s1_ptr)_2)->base + _a_4839);
            if (IS_ATOM_INT(_2483) && IS_ATOM_INT(_userdata_4787)){
                _2484 = (_2483 < _userdata_4787) ? -1 : (_2483 > _userdata_4787);
            }
            else{
                _2484 = compare(_2483, _userdata_4787);
            }
            _2483 = NOVALUE;
            if (_2484 == 0)
            goto LE; // [252] 275

            /** 					idx += 1*/
            _idx_4790 = _idx_4790 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2487 = (int)*(((s1_ptr)_2)->base + _a_4839);
            Ref(_2487);
            _2 = (int)SEQ_PTR(_dest_4789);
            _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
            _1 = *(int *)_2;
            *(int *)_2 = _2487;
            if( _1 != _2487 ){
                DeRef(_1);
            }
            _2487 = NOVALUE;
LE: 

            /** 			end for*/
            _a_4839 = _a_4839 + 1;
            goto LC; // [277] 242
LD: 
            ;
        }
        goto L5; // [282] 1348

        /** 		case ">", "gt" then*/
        case 10:
        case 11:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4785)){
                _2490 = SEQ_PTR(_source_4785)->length;
        }
        else {
            _2490 = 1;
        }
        {
            int _a_4851;
            _a_4851 = 1;
LF: 
            if (_a_4851 > _2490){
                goto L10; // [295] 342
            }

            /** 				if compare(source[a], userdata) > 0 then*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2491 = (int)*(((s1_ptr)_2)->base + _a_4851);
            if (IS_ATOM_INT(_2491) && IS_ATOM_INT(_userdata_4787)){
                _2492 = (_2491 < _userdata_4787) ? -1 : (_2491 > _userdata_4787);
            }
            else{
                _2492 = compare(_2491, _userdata_4787);
            }
            _2491 = NOVALUE;
            if (_2492 <= 0)
            goto L11; // [312] 335

            /** 					idx += 1*/
            _idx_4790 = _idx_4790 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2495 = (int)*(((s1_ptr)_2)->base + _a_4851);
            Ref(_2495);
            _2 = (int)SEQ_PTR(_dest_4789);
            _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
            _1 = *(int *)_2;
            *(int *)_2 = _2495;
            if( _1 != _2495 ){
                DeRef(_1);
            }
            _2495 = NOVALUE;
L11: 

            /** 			end for*/
            _a_4851 = _a_4851 + 1;
            goto LF; // [337] 302
L10: 
            ;
        }
        goto L5; // [342] 1348

        /** 		case ">=", "ge" then*/
        case 12:
        case 13:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4785)){
                _2498 = SEQ_PTR(_source_4785)->length;
        }
        else {
            _2498 = 1;
        }
        {
            int _a_4863;
            _a_4863 = 1;
L12: 
            if (_a_4863 > _2498){
                goto L13; // [355] 402
            }

            /** 				if compare(source[a], userdata) >= 0 then*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2499 = (int)*(((s1_ptr)_2)->base + _a_4863);
            if (IS_ATOM_INT(_2499) && IS_ATOM_INT(_userdata_4787)){
                _2500 = (_2499 < _userdata_4787) ? -1 : (_2499 > _userdata_4787);
            }
            else{
                _2500 = compare(_2499, _userdata_4787);
            }
            _2499 = NOVALUE;
            if (_2500 < 0)
            goto L14; // [372] 395

            /** 					idx += 1*/
            _idx_4790 = _idx_4790 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2503 = (int)*(((s1_ptr)_2)->base + _a_4863);
            Ref(_2503);
            _2 = (int)SEQ_PTR(_dest_4789);
            _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
            _1 = *(int *)_2;
            *(int *)_2 = _2503;
            if( _1 != _2503 ){
                DeRef(_1);
            }
            _2503 = NOVALUE;
L14: 

            /** 			end for*/
            _a_4863 = _a_4863 + 1;
            goto L12; // [397] 362
L13: 
            ;
        }
        goto L5; // [402] 1348

        /** 		case "in" then*/
        case 14:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_4788, _2505);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2507 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2507 = 1;
            }
            {
                int _a_4877;
                _a_4877 = 1;
L15: 
                if (_a_4877 > _2507){
                    goto L16; // [424] 471
                }

                /** 						if find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2508 = (int)*(((s1_ptr)_2)->base + _a_4877);
                _2509 = find_from(_2508, _userdata_4787, 1);
                _2508 = NOVALUE;
                if (_2509 == 0)
                {
                    _2509 = NOVALUE;
                    goto L17; // [442] 464
                }
                else{
                    _2509 = NOVALUE;
                }

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2511 = (int)*(((s1_ptr)_2)->base + _a_4877);
                Ref(_2511);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2511;
                if( _1 != _2511 ){
                    DeRef(_1);
                }
                _2511 = NOVALUE;
L17: 

                /** 					end for*/
                _a_4877 = _a_4877 + 1;
                goto L15; // [466] 431
L16: 
                ;
            }
            goto L5; // [471] 1348

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2512 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2512 = 1;
            }
            {
                int _a_4886;
                _a_4886 = 1;
L18: 
                if (_a_4886 > _2512){
                    goto L19; // [482] 552
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2513 = (int)*(((s1_ptr)_2)->base + _a_4886);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2514 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2513) && IS_ATOM_INT(_2514)){
                    _2515 = (_2513 < _2514) ? -1 : (_2513 > _2514);
                }
                else{
                    _2515 = compare(_2513, _2514);
                }
                _2513 = NOVALUE;
                _2514 = NOVALUE;
                if (_2515 < 0)
                goto L1A; // [503] 545

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2517 = (int)*(((s1_ptr)_2)->base + _a_4886);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2518 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2517) && IS_ATOM_INT(_2518)){
                    _2519 = (_2517 < _2518) ? -1 : (_2517 > _2518);
                }
                else{
                    _2519 = compare(_2517, _2518);
                }
                _2517 = NOVALUE;
                _2518 = NOVALUE;
                if (_2519 > 0)
                goto L1B; // [521] 544

                /** 								idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2522 = (int)*(((s1_ptr)_2)->base + _a_4886);
                Ref(_2522);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2522;
                if( _1 != _2522 ){
                    DeRef(_1);
                }
                _2522 = NOVALUE;
L1B: 
L1A: 

                /** 					end for*/
                _a_4886 = _a_4886 + 1;
                goto L18; // [547] 489
L19: 
                ;
            }
            goto L5; // [552] 1348

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2523 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2523 = 1;
            }
            {
                int _a_4902;
                _a_4902 = 1;
L1C: 
                if (_a_4902 > _2523){
                    goto L1D; // [563] 633
                }

                /** 						if compare(source[a], userdata[1]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2524 = (int)*(((s1_ptr)_2)->base + _a_4902);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2525 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2524) && IS_ATOM_INT(_2525)){
                    _2526 = (_2524 < _2525) ? -1 : (_2524 > _2525);
                }
                else{
                    _2526 = compare(_2524, _2525);
                }
                _2524 = NOVALUE;
                _2525 = NOVALUE;
                if (_2526 < 0)
                goto L1E; // [584] 626

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2528 = (int)*(((s1_ptr)_2)->base + _a_4902);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2529 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2528) && IS_ATOM_INT(_2529)){
                    _2530 = (_2528 < _2529) ? -1 : (_2528 > _2529);
                }
                else{
                    _2530 = compare(_2528, _2529);
                }
                _2528 = NOVALUE;
                _2529 = NOVALUE;
                if (_2530 >= 0)
                goto L1F; // [602] 625

                /** 								idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2533 = (int)*(((s1_ptr)_2)->base + _a_4902);
                Ref(_2533);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2533;
                if( _1 != _2533 ){
                    DeRef(_1);
                }
                _2533 = NOVALUE;
L1F: 
L1E: 

                /** 					end for*/
                _a_4902 = _a_4902 + 1;
                goto L1C; // [628] 570
L1D: 
                ;
            }
            goto L5; // [633] 1348

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2534 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2534 = 1;
            }
            {
                int _a_4918;
                _a_4918 = 1;
L20: 
                if (_a_4918 > _2534){
                    goto L21; // [644] 714
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2535 = (int)*(((s1_ptr)_2)->base + _a_4918);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2536 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2535) && IS_ATOM_INT(_2536)){
                    _2537 = (_2535 < _2536) ? -1 : (_2535 > _2536);
                }
                else{
                    _2537 = compare(_2535, _2536);
                }
                _2535 = NOVALUE;
                _2536 = NOVALUE;
                if (_2537 <= 0)
                goto L22; // [665] 707

                /** 							if compare(source[a], userdata[2]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2539 = (int)*(((s1_ptr)_2)->base + _a_4918);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2540 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2539) && IS_ATOM_INT(_2540)){
                    _2541 = (_2539 < _2540) ? -1 : (_2539 > _2540);
                }
                else{
                    _2541 = compare(_2539, _2540);
                }
                _2539 = NOVALUE;
                _2540 = NOVALUE;
                if (_2541 > 0)
                goto L23; // [683] 706

                /** 								idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2544 = (int)*(((s1_ptr)_2)->base + _a_4918);
                Ref(_2544);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2544;
                if( _1 != _2544 ){
                    DeRef(_1);
                }
                _2544 = NOVALUE;
L23: 
L22: 

                /** 					end for*/
                _a_4918 = _a_4918 + 1;
                goto L20; // [709] 651
L21: 
                ;
            }
            goto L5; // [714] 1348

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2545 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2545 = 1;
            }
            {
                int _a_4934;
                _a_4934 = 1;
L24: 
                if (_a_4934 > _2545){
                    goto L25; // [725] 795
                }

                /** 						if compare(source[a], userdata[1]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2546 = (int)*(((s1_ptr)_2)->base + _a_4934);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2547 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2546) && IS_ATOM_INT(_2547)){
                    _2548 = (_2546 < _2547) ? -1 : (_2546 > _2547);
                }
                else{
                    _2548 = compare(_2546, _2547);
                }
                _2546 = NOVALUE;
                _2547 = NOVALUE;
                if (_2548 <= 0)
                goto L26; // [746] 788

                /** 							if compare(source[a], userdata[2]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2550 = (int)*(((s1_ptr)_2)->base + _a_4934);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2551 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2550) && IS_ATOM_INT(_2551)){
                    _2552 = (_2550 < _2551) ? -1 : (_2550 > _2551);
                }
                else{
                    _2552 = compare(_2550, _2551);
                }
                _2550 = NOVALUE;
                _2551 = NOVALUE;
                if (_2552 >= 0)
                goto L27; // [764] 787

                /** 								idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 								dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2555 = (int)*(((s1_ptr)_2)->base + _a_4934);
                Ref(_2555);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2555;
                if( _1 != _2555 ){
                    DeRef(_1);
                }
                _2555 = NOVALUE;
L27: 
L26: 

                /** 					end for*/
                _a_4934 = _a_4934 + 1;
                goto L24; // [790] 732
L25: 
                ;
            }
            goto L5; // [795] 1348

            /** 				case else*/
            case 0:
        ;}        goto L5; // [802] 1348

        /** 		case "out" then*/
        case 15:

        /** 			switch rangetype do*/
        _1 = find(_rangetype_4788, _2557);
        switch ( _1 ){ 

            /** 				case "" then*/
            case 1:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2559 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2559 = 1;
            }
            {
                int _a_4955;
                _a_4955 = 1;
L28: 
                if (_a_4955 > _2559){
                    goto L29; // [824] 871
                }

                /** 						if not find(source[a], userdata)  then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2560 = (int)*(((s1_ptr)_2)->base + _a_4955);
                _2561 = find_from(_2560, _userdata_4787, 1);
                _2560 = NOVALUE;
                if (_2561 != 0)
                goto L2A; // [842] 864
                _2561 = NOVALUE;

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2564 = (int)*(((s1_ptr)_2)->base + _a_4955);
                Ref(_2564);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2564;
                if( _1 != _2564 ){
                    DeRef(_1);
                }
                _2564 = NOVALUE;
L2A: 

                /** 					end for*/
                _a_4955 = _a_4955 + 1;
                goto L28; // [866] 831
L29: 
                ;
            }
            goto L5; // [871] 1348

            /** 				case "[]" then*/
            case 2:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2565 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2565 = 1;
            }
            {
                int _a_4965;
                _a_4965 = 1;
L2B: 
                if (_a_4965 > _2565){
                    goto L2C; // [882] 973
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2566 = (int)*(((s1_ptr)_2)->base + _a_4965);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2567 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2566) && IS_ATOM_INT(_2567)){
                    _2568 = (_2566 < _2567) ? -1 : (_2566 > _2567);
                }
                else{
                    _2568 = compare(_2566, _2567);
                }
                _2566 = NOVALUE;
                _2567 = NOVALUE;
                if (_2568 >= 0)
                goto L2D; // [903] 928

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2571 = (int)*(((s1_ptr)_2)->base + _a_4965);
                Ref(_2571);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2571;
                if( _1 != _2571 ){
                    DeRef(_1);
                }
                _2571 = NOVALUE;
                goto L2E; // [925] 966
L2D: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2572 = (int)*(((s1_ptr)_2)->base + _a_4965);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2573 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2572) && IS_ATOM_INT(_2573)){
                    _2574 = (_2572 < _2573) ? -1 : (_2572 > _2573);
                }
                else{
                    _2574 = compare(_2572, _2573);
                }
                _2572 = NOVALUE;
                _2573 = NOVALUE;
                if (_2574 <= 0)
                goto L2F; // [942] 965

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2577 = (int)*(((s1_ptr)_2)->base + _a_4965);
                Ref(_2577);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2577;
                if( _1 != _2577 ){
                    DeRef(_1);
                }
                _2577 = NOVALUE;
L2F: 
L2E: 

                /** 					end for*/
                _a_4965 = _a_4965 + 1;
                goto L2B; // [968] 889
L2C: 
                ;
            }
            goto L5; // [973] 1348

            /** 				case "[)" then*/
            case 3:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2578 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2578 = 1;
            }
            {
                int _a_4983;
                _a_4983 = 1;
L30: 
                if (_a_4983 > _2578){
                    goto L31; // [984] 1075
                }

                /** 						if compare(source[a], userdata[1]) < 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2579 = (int)*(((s1_ptr)_2)->base + _a_4983);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2580 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2579) && IS_ATOM_INT(_2580)){
                    _2581 = (_2579 < _2580) ? -1 : (_2579 > _2580);
                }
                else{
                    _2581 = compare(_2579, _2580);
                }
                _2579 = NOVALUE;
                _2580 = NOVALUE;
                if (_2581 >= 0)
                goto L32; // [1005] 1030

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2584 = (int)*(((s1_ptr)_2)->base + _a_4983);
                Ref(_2584);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2584;
                if( _1 != _2584 ){
                    DeRef(_1);
                }
                _2584 = NOVALUE;
                goto L33; // [1027] 1068
L32: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2585 = (int)*(((s1_ptr)_2)->base + _a_4983);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2586 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2585) && IS_ATOM_INT(_2586)){
                    _2587 = (_2585 < _2586) ? -1 : (_2585 > _2586);
                }
                else{
                    _2587 = compare(_2585, _2586);
                }
                _2585 = NOVALUE;
                _2586 = NOVALUE;
                if (_2587 < 0)
                goto L34; // [1044] 1067

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2590 = (int)*(((s1_ptr)_2)->base + _a_4983);
                Ref(_2590);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2590;
                if( _1 != _2590 ){
                    DeRef(_1);
                }
                _2590 = NOVALUE;
L34: 
L33: 

                /** 					end for*/
                _a_4983 = _a_4983 + 1;
                goto L30; // [1070] 991
L31: 
                ;
            }
            goto L5; // [1075] 1348

            /** 				case "(]" then*/
            case 4:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2591 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2591 = 1;
            }
            {
                int _a_5001;
                _a_5001 = 1;
L35: 
                if (_a_5001 > _2591){
                    goto L36; // [1086] 1177
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2592 = (int)*(((s1_ptr)_2)->base + _a_5001);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2593 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2592) && IS_ATOM_INT(_2593)){
                    _2594 = (_2592 < _2593) ? -1 : (_2592 > _2593);
                }
                else{
                    _2594 = compare(_2592, _2593);
                }
                _2592 = NOVALUE;
                _2593 = NOVALUE;
                if (_2594 > 0)
                goto L37; // [1107] 1132

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2597 = (int)*(((s1_ptr)_2)->base + _a_5001);
                Ref(_2597);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2597;
                if( _1 != _2597 ){
                    DeRef(_1);
                }
                _2597 = NOVALUE;
                goto L38; // [1129] 1170
L37: 

                /** 						elsif compare(source[a], userdata[2]) > 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2598 = (int)*(((s1_ptr)_2)->base + _a_5001);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2599 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2598) && IS_ATOM_INT(_2599)){
                    _2600 = (_2598 < _2599) ? -1 : (_2598 > _2599);
                }
                else{
                    _2600 = compare(_2598, _2599);
                }
                _2598 = NOVALUE;
                _2599 = NOVALUE;
                if (_2600 <= 0)
                goto L39; // [1146] 1169

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2603 = (int)*(((s1_ptr)_2)->base + _a_5001);
                Ref(_2603);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2603;
                if( _1 != _2603 ){
                    DeRef(_1);
                }
                _2603 = NOVALUE;
L39: 
L38: 

                /** 					end for*/
                _a_5001 = _a_5001 + 1;
                goto L35; // [1172] 1093
L36: 
                ;
            }
            goto L5; // [1177] 1348

            /** 				case "()" then*/
            case 5:

            /** 					for a = 1 to length(source) do*/
            if (IS_SEQUENCE(_source_4785)){
                    _2604 = SEQ_PTR(_source_4785)->length;
            }
            else {
                _2604 = 1;
            }
            {
                int _a_5019;
                _a_5019 = 1;
L3A: 
                if (_a_5019 > _2604){
                    goto L3B; // [1188] 1279
                }

                /** 						if compare(source[a], userdata[1]) <= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2605 = (int)*(((s1_ptr)_2)->base + _a_5019);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2606 = (int)*(((s1_ptr)_2)->base + 1);
                if (IS_ATOM_INT(_2605) && IS_ATOM_INT(_2606)){
                    _2607 = (_2605 < _2606) ? -1 : (_2605 > _2606);
                }
                else{
                    _2607 = compare(_2605, _2606);
                }
                _2605 = NOVALUE;
                _2606 = NOVALUE;
                if (_2607 > 0)
                goto L3C; // [1209] 1234

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2610 = (int)*(((s1_ptr)_2)->base + _a_5019);
                Ref(_2610);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2610;
                if( _1 != _2610 ){
                    DeRef(_1);
                }
                _2610 = NOVALUE;
                goto L3D; // [1231] 1272
L3C: 

                /** 						elsif compare(source[a], userdata[2]) >= 0 then*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2611 = (int)*(((s1_ptr)_2)->base + _a_5019);
                _2 = (int)SEQ_PTR(_userdata_4787);
                _2612 = (int)*(((s1_ptr)_2)->base + 2);
                if (IS_ATOM_INT(_2611) && IS_ATOM_INT(_2612)){
                    _2613 = (_2611 < _2612) ? -1 : (_2611 > _2612);
                }
                else{
                    _2613 = compare(_2611, _2612);
                }
                _2611 = NOVALUE;
                _2612 = NOVALUE;
                if (_2613 < 0)
                goto L3E; // [1248] 1271

                /** 							idx += 1*/
                _idx_4790 = _idx_4790 + 1;

                /** 							dest[idx] = source[a]*/
                _2 = (int)SEQ_PTR(_source_4785);
                _2616 = (int)*(((s1_ptr)_2)->base + _a_5019);
                Ref(_2616);
                _2 = (int)SEQ_PTR(_dest_4789);
                _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
                _1 = *(int *)_2;
                *(int *)_2 = _2616;
                if( _1 != _2616 ){
                    DeRef(_1);
                }
                _2616 = NOVALUE;
L3E: 
L3D: 

                /** 					end for*/
                _a_5019 = _a_5019 + 1;
                goto L3A; // [1274] 1195
L3B: 
                ;
            }
            goto L5; // [1279] 1348

            /** 				case else*/
            case 0:
        ;}        goto L5; // [1286] 1348

        /** 		case else*/
        case 0:

        /** 			for a = 1 to length(source) do*/
        if (IS_SEQUENCE(_source_4785)){
                _2617 = SEQ_PTR(_source_4785)->length;
        }
        else {
            _2617 = 1;
        }
        {
            int _a_5038;
            _a_5038 = 1;
L3F: 
            if (_a_5038 > _2617){
                goto L40; // [1297] 1347
            }

            /** 				if call_func(rid, {source[a], userdata}) then*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2618 = (int)*(((s1_ptr)_2)->base + _a_5038);
            Ref(_userdata_4787);
            Ref(_2618);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _2618;
            ((int *)_2)[2] = _userdata_4787;
            _2619 = MAKE_SEQ(_1);
            _2618 = NOVALUE;
            _1 = (int)SEQ_PTR(_2619);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_rid_4786].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8)
                                 );
            DeRef(_2620);
            _2620 = _1;
            DeRefDS(_2619);
            _2619 = NOVALUE;
            if (_2620 == 0) {
                DeRef(_2620);
                _2620 = NOVALUE;
                goto L41; // [1318] 1340
            }
            else {
                if (!IS_ATOM_INT(_2620) && DBL_PTR(_2620)->dbl == 0.0){
                    DeRef(_2620);
                    _2620 = NOVALUE;
                    goto L41; // [1318] 1340
                }
                DeRef(_2620);
                _2620 = NOVALUE;
            }
            DeRef(_2620);
            _2620 = NOVALUE;

            /** 					idx += 1*/
            _idx_4790 = _idx_4790 + 1;

            /** 					dest[idx] = source[a]*/
            _2 = (int)SEQ_PTR(_source_4785);
            _2622 = (int)*(((s1_ptr)_2)->base + _a_5038);
            Ref(_2622);
            _2 = (int)SEQ_PTR(_dest_4789);
            _2 = (int)(((s1_ptr)_2)->base + _idx_4790);
            _1 = *(int *)_2;
            *(int *)_2 = _2622;
            if( _1 != _2622 ){
                DeRef(_1);
            }
            _2622 = NOVALUE;
L41: 

            /** 			end for*/
            _a_5038 = _a_5038 + 1;
            goto L3F; // [1342] 1304
L40: 
            ;
        }
    ;}L5: 

    /** 	return dest[1..idx]*/
    rhs_slice_target = (object_ptr)&_2623;
    RHS_Slice(_dest_4789, 1, _idx_4790);
    DeRefDS(_source_4785);
    DeRef(_rid_4786);
    DeRef(_userdata_4787);
    DeRef(_rangetype_4788);
    DeRefDS(_dest_4789);
    return _2623;
    ;
}


int _21filter_alpha(int _elem_5050, int _ud_5051)
{
    int _2624 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return t_alpha(elem)*/
    Ref(_elem_5050);
    _2624 = _11t_alpha(_elem_5050);
    DeRef(_elem_5050);
    return _2624;
    ;
}


int _21extract(int _source_5059, int _indexes_5060)
{
    int _p_5061 = NOVALUE;
    int _msg_inlined_crash_at_34_5071 = NOVALUE;
    int _2632 = NOVALUE;
    int _2629 = NOVALUE;
    int _2627 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(indexes) do*/
    if (IS_SEQUENCE(_indexes_5060)){
            _2627 = SEQ_PTR(_indexes_5060)->length;
    }
    else {
        _2627 = 1;
    }
    {
        int _i_5063;
        _i_5063 = 1;
L1: 
        if (_i_5063 > _2627){
            goto L2; // [10] 71
        }

        /** 		p = indexes[i]*/
        DeRef(_p_5061);
        _2 = (int)SEQ_PTR(_indexes_5060);
        _p_5061 = (int)*(((s1_ptr)_2)->base + _i_5063);
        Ref(_p_5061);

        /** 		if not valid_index(source,p) then*/
        RefDS(_source_5059);
        Ref(_p_5061);
        _2629 = _21valid_index(_source_5059, _p_5061);
        if (IS_ATOM_INT(_2629)) {
            if (_2629 != 0){
                DeRef(_2629);
                _2629 = NOVALUE;
                goto L3; // [30] 54
            }
        }
        else {
            if (DBL_PTR(_2629)->dbl != 0.0){
                DeRef(_2629);
                _2629 = NOVALUE;
                goto L3; // [30] 54
            }
        }
        DeRef(_2629);
        _2629 = NOVALUE;

        /** 			error:crash("%d is not a valid index for the input sequence",p)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_34_5071);
        _msg_inlined_crash_at_34_5071 = EPrintf(-9999999, _2631, _p_5061);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_34_5071);

        /** end procedure*/
        goto L4; // [48] 51
L4: 
        DeRefi(_msg_inlined_crash_at_34_5071);
        _msg_inlined_crash_at_34_5071 = NOVALUE;
L3: 

        /** 		indexes[i] = source[p]*/
        _2 = (int)SEQ_PTR(_source_5059);
        if (!IS_ATOM_INT(_p_5061)){
            _2632 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_p_5061)->dbl));
        }
        else{
            _2632 = (int)*(((s1_ptr)_2)->base + _p_5061);
        }
        Ref(_2632);
        _2 = (int)SEQ_PTR(_indexes_5060);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _indexes_5060 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5063);
        _1 = *(int *)_2;
        *(int *)_2 = _2632;
        if( _1 != _2632 ){
            DeRef(_1);
        }
        _2632 = NOVALUE;

        /** 	end for*/
        _i_5063 = _i_5063 + 1;
        goto L1; // [66] 17
L2: 
        ;
    }

    /** 	return indexes*/
    DeRefDS(_source_5059);
    DeRef(_p_5061);
    return _indexes_5060;
    ;
}


int _21project(int _source_5075, int _coords_5076)
{
    int _result_5077 = NOVALUE;
    int _2643 = NOVALUE;
    int _2642 = NOVALUE;
    int _2641 = NOVALUE;
    int _2639 = NOVALUE;
    int _2638 = NOVALUE;
    int _2637 = NOVALUE;
    int _2635 = NOVALUE;
    int _2634 = NOVALUE;
    int _2633 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	result = repeat( repeat(0, length(coords)), length(source) )*/
    if (IS_SEQUENCE(_coords_5076)){
            _2633 = SEQ_PTR(_coords_5076)->length;
    }
    else {
        _2633 = 1;
    }
    _2634 = Repeat(0, _2633);
    _2633 = NOVALUE;
    if (IS_SEQUENCE(_source_5075)){
            _2635 = SEQ_PTR(_source_5075)->length;
    }
    else {
        _2635 = 1;
    }
    DeRef(_result_5077);
    _result_5077 = Repeat(_2634, _2635);
    DeRefDS(_2634);
    _2634 = NOVALUE;
    _2635 = NOVALUE;

    /** 	for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_5075)){
            _2637 = SEQ_PTR(_source_5075)->length;
    }
    else {
        _2637 = 1;
    }
    {
        int _i_5083;
        _i_5083 = 1;
L1: 
        if (_i_5083 > _2637){
            goto L2; // [26] 83
        }

        /** 		for j = 1 to length(coords) do*/
        if (IS_SEQUENCE(_coords_5076)){
                _2638 = SEQ_PTR(_coords_5076)->length;
        }
        else {
            _2638 = 1;
        }
        {
            int _j_5086;
            _j_5086 = 1;
L3: 
            if (_j_5086 > _2638){
                goto L4; // [38] 76
            }

            /** 			result[i][j] = extract(source[i], coords[j])*/
            _2 = (int)SEQ_PTR(_result_5077);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _result_5077 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_5083 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_source_5075);
            _2641 = (int)*(((s1_ptr)_2)->base + _i_5083);
            _2 = (int)SEQ_PTR(_coords_5076);
            _2642 = (int)*(((s1_ptr)_2)->base + _j_5086);
            Ref(_2641);
            Ref(_2642);
            _2643 = _21extract(_2641, _2642);
            _2641 = NOVALUE;
            _2642 = NOVALUE;
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _j_5086);
            _1 = *(int *)_2;
            *(int *)_2 = _2643;
            if( _1 != _2643 ){
                DeRef(_1);
            }
            _2643 = NOVALUE;
            _2639 = NOVALUE;

            /** 		end for*/
            _j_5086 = _j_5086 + 1;
            goto L3; // [71] 45
L4: 
            ;
        }

        /** 	end for*/
        _i_5083 = _i_5083 + 1;
        goto L1; // [78] 33
L2: 
        ;
    }

    /** 	return result*/
    DeRefDS(_source_5075);
    DeRefDS(_coords_5076);
    return _result_5077;
    ;
}


int _21split(int _st_5095, int _delim_5096, int _no_empty_5097, int _limit_5098)
{
    int _ret_5099 = NOVALUE;
    int _start_5100 = NOVALUE;
    int _pos_5101 = NOVALUE;
    int _k_5153 = NOVALUE;
    int _2692 = NOVALUE;
    int _2690 = NOVALUE;
    int _2689 = NOVALUE;
    int _2685 = NOVALUE;
    int _2684 = NOVALUE;
    int _2683 = NOVALUE;
    int _2680 = NOVALUE;
    int _2679 = NOVALUE;
    int _2674 = NOVALUE;
    int _2673 = NOVALUE;
    int _2669 = NOVALUE;
    int _2665 = NOVALUE;
    int _2663 = NOVALUE;
    int _2662 = NOVALUE;
    int _2658 = NOVALUE;
    int _2656 = NOVALUE;
    int _2655 = NOVALUE;
    int _2654 = NOVALUE;
    int _2653 = NOVALUE;
    int _2650 = NOVALUE;
    int _2649 = NOVALUE;
    int _2648 = NOVALUE;
    int _2647 = NOVALUE;
    int _2646 = NOVALUE;
    int _2644 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_no_empty_5097)) {
        _1 = (long)(DBL_PTR(_no_empty_5097)->dbl);
        if (UNIQUE(DBL_PTR(_no_empty_5097)) && (DBL_PTR(_no_empty_5097)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_no_empty_5097);
        _no_empty_5097 = _1;
    }
    if (!IS_ATOM_INT(_limit_5098)) {
        _1 = (long)(DBL_PTR(_limit_5098)->dbl);
        if (UNIQUE(DBL_PTR(_limit_5098)) && (DBL_PTR(_limit_5098)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_5098);
        _limit_5098 = _1;
    }

    /** 	sequence ret = {}*/
    RefDS(_5);
    DeRef(_ret_5099);
    _ret_5099 = _5;

    /** 	if length(st) = 0 then*/
    if (IS_SEQUENCE(_st_5095)){
            _2644 = SEQ_PTR(_st_5095)->length;
    }
    else {
        _2644 = 1;
    }
    if (_2644 != 0)
    goto L1; // [23] 34

    /** 		return ret*/
    DeRefDS(_st_5095);
    DeRef(_delim_5096);
    return _ret_5099;
L1: 

    /** 	if sequence(delim) then*/
    _2646 = IS_SEQUENCE(_delim_5096);
    if (_2646 == 0)
    {
        _2646 = NOVALUE;
        goto L2; // [39] 225
    }
    else{
        _2646 = NOVALUE;
    }

    /** 		if equal(delim, "") then*/
    if (_delim_5096 == _5)
    _2647 = 1;
    else if (IS_ATOM_INT(_delim_5096) && IS_ATOM_INT(_5))
    _2647 = 0;
    else
    _2647 = (compare(_delim_5096, _5) == 0);
    if (_2647 == 0)
    {
        _2647 = NOVALUE;
        goto L3; // [48] 133
    }
    else{
        _2647 = NOVALUE;
    }

    /** 			for i = 1 to length(st) do*/
    if (IS_SEQUENCE(_st_5095)){
            _2648 = SEQ_PTR(_st_5095)->length;
    }
    else {
        _2648 = 1;
    }
    {
        int _i_5110;
        _i_5110 = 1;
L4: 
        if (_i_5110 > _2648){
            goto L5; // [56] 126
        }

        /** 				st[i] = {st[i]}*/
        _2 = (int)SEQ_PTR(_st_5095);
        _2649 = (int)*(((s1_ptr)_2)->base + _i_5110);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_2649);
        *((int *)(_2+4)) = _2649;
        _2650 = MAKE_SEQ(_1);
        _2649 = NOVALUE;
        _2 = (int)SEQ_PTR(_st_5095);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _st_5095 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5110);
        _1 = *(int *)_2;
        *(int *)_2 = _2650;
        if( _1 != _2650 ){
            DeRef(_1);
        }
        _2650 = NOVALUE;

        /** 				limit -= 1*/
        _limit_5098 = _limit_5098 - 1;

        /** 				if limit = 0 then*/
        if (_limit_5098 != 0)
        goto L6; // [87] 119

        /** 					st = append(st[1 .. i],st[i+1 .. $])*/
        rhs_slice_target = (object_ptr)&_2653;
        RHS_Slice(_st_5095, 1, _i_5110);
        _2654 = _i_5110 + 1;
        if (IS_SEQUENCE(_st_5095)){
                _2655 = SEQ_PTR(_st_5095)->length;
        }
        else {
            _2655 = 1;
        }
        rhs_slice_target = (object_ptr)&_2656;
        RHS_Slice(_st_5095, _2654, _2655);
        RefDS(_2656);
        Append(&_st_5095, _2653, _2656);
        DeRefDS(_2653);
        _2653 = NOVALUE;
        DeRefDS(_2656);
        _2656 = NOVALUE;

        /** 					exit*/
        goto L5; // [116] 126
L6: 

        /** 			end for*/
        _i_5110 = _i_5110 + 1;
        goto L4; // [121] 63
L5: 
        ;
    }

    /** 			return st*/
    DeRef(_delim_5096);
    DeRef(_ret_5099);
    DeRef(_2654);
    _2654 = NOVALUE;
    return _st_5095;
L3: 

    /** 		start = 1*/
    _start_5100 = 1;

    /** 		while start <= length(st) do*/
L7: 
    if (IS_SEQUENCE(_st_5095)){
            _2658 = SEQ_PTR(_st_5095)->length;
    }
    else {
        _2658 = 1;
    }
    if (_start_5100 > _2658)
    goto L8; // [148] 312

    /** 			pos = match(delim, st, start)*/
    _pos_5101 = e_match_from(_delim_5096, _st_5095, _start_5100);

    /** 			if pos = 0 then*/
    if (_pos_5101 != 0)
    goto L9; // [163] 172

    /** 				exit*/
    goto L8; // [169] 312
L9: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _2662 = _pos_5101 - 1;
    rhs_slice_target = (object_ptr)&_2663;
    RHS_Slice(_st_5095, _start_5100, _2662);
    RefDS(_2663);
    Append(&_ret_5099, _ret_5099, _2663);
    DeRefDS(_2663);
    _2663 = NOVALUE;

    /** 			start = pos+length(delim)*/
    if (IS_SEQUENCE(_delim_5096)){
            _2665 = SEQ_PTR(_delim_5096)->length;
    }
    else {
        _2665 = 1;
    }
    _start_5100 = _pos_5101 + _2665;
    _2665 = NOVALUE;

    /** 			limit -= 1*/
    _limit_5098 = _limit_5098 - 1;

    /** 			if limit = 0 then*/
    if (_limit_5098 != 0)
    goto L7; // [208] 145

    /** 				exit*/
    goto L8; // [214] 312

    /** 		end while*/
    goto L7; // [219] 145
    goto L8; // [222] 312
L2: 

    /** 		start = 1*/
    _start_5100 = 1;

    /** 		while start <= length(st) do*/
LA: 
    if (IS_SEQUENCE(_st_5095)){
            _2669 = SEQ_PTR(_st_5095)->length;
    }
    else {
        _2669 = 1;
    }
    if (_start_5100 > _2669)
    goto LB; // [240] 311

    /** 			pos = find(delim, st, start)*/
    _pos_5101 = find_from(_delim_5096, _st_5095, _start_5100);

    /** 			if pos = 0 then*/
    if (_pos_5101 != 0)
    goto LC; // [255] 264

    /** 				exit*/
    goto LB; // [261] 311
LC: 

    /** 			ret = append(ret, st[start..pos-1])*/
    _2673 = _pos_5101 - 1;
    rhs_slice_target = (object_ptr)&_2674;
    RHS_Slice(_st_5095, _start_5100, _2673);
    RefDS(_2674);
    Append(&_ret_5099, _ret_5099, _2674);
    DeRefDS(_2674);
    _2674 = NOVALUE;

    /** 			start = pos + 1*/
    _start_5100 = _pos_5101 + 1;

    /** 			limit -= 1*/
    _limit_5098 = _limit_5098 - 1;

    /** 			if limit = 0 then*/
    if (_limit_5098 != 0)
    goto LA; // [297] 237

    /** 				exit*/
    goto LB; // [303] 311

    /** 		end while*/
    goto LA; // [308] 237
LB: 
L8: 

    /** 	ret = append(ret, st[start..$])*/
    if (IS_SEQUENCE(_st_5095)){
            _2679 = SEQ_PTR(_st_5095)->length;
    }
    else {
        _2679 = 1;
    }
    rhs_slice_target = (object_ptr)&_2680;
    RHS_Slice(_st_5095, _start_5100, _2679);
    RefDS(_2680);
    Append(&_ret_5099, _ret_5099, _2680);
    DeRefDS(_2680);
    _2680 = NOVALUE;

    /** 	integer k = length(ret)*/
    if (IS_SEQUENCE(_ret_5099)){
            _k_5153 = SEQ_PTR(_ret_5099)->length;
    }
    else {
        _k_5153 = 1;
    }

    /** 	if no_empty then*/
    if (_no_empty_5097 == 0)
    {
        goto LD; // [337] 406
    }
    else{
    }

    /** 		k = 0*/
    _k_5153 = 0;

    /** 		for i = 1 to length(ret) do*/
    if (IS_SEQUENCE(_ret_5099)){
            _2683 = SEQ_PTR(_ret_5099)->length;
    }
    else {
        _2683 = 1;
    }
    {
        int _i_5157;
        _i_5157 = 1;
LE: 
        if (_i_5157 > _2683){
            goto LF; // [352] 405
        }

        /** 			if length(ret[i]) != 0 then*/
        _2 = (int)SEQ_PTR(_ret_5099);
        _2684 = (int)*(((s1_ptr)_2)->base + _i_5157);
        if (IS_SEQUENCE(_2684)){
                _2685 = SEQ_PTR(_2684)->length;
        }
        else {
            _2685 = 1;
        }
        _2684 = NOVALUE;
        if (_2685 == 0)
        goto L10; // [368] 398

        /** 				k += 1*/
        _k_5153 = _k_5153 + 1;

        /** 				if k != i then*/
        if (_k_5153 == _i_5157)
        goto L11; // [382] 397

        /** 					ret[k] = ret[i]*/
        _2 = (int)SEQ_PTR(_ret_5099);
        _2689 = (int)*(((s1_ptr)_2)->base + _i_5157);
        Ref(_2689);
        _2 = (int)SEQ_PTR(_ret_5099);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_5099 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _k_5153);
        _1 = *(int *)_2;
        *(int *)_2 = _2689;
        if( _1 != _2689 ){
            DeRef(_1);
        }
        _2689 = NOVALUE;
L11: 
L10: 

        /** 		end for*/
        _i_5157 = _i_5157 + 1;
        goto LE; // [400] 359
LF: 
        ;
    }
LD: 

    /** 	if k < length(ret) then*/
    if (IS_SEQUENCE(_ret_5099)){
            _2690 = SEQ_PTR(_ret_5099)->length;
    }
    else {
        _2690 = 1;
    }
    if (_k_5153 >= _2690)
    goto L12; // [411] 429

    /** 		return ret[1 .. k]*/
    rhs_slice_target = (object_ptr)&_2692;
    RHS_Slice(_ret_5099, 1, _k_5153);
    DeRefDS(_st_5095);
    DeRef(_delim_5096);
    DeRefDS(_ret_5099);
    DeRef(_2662);
    _2662 = NOVALUE;
    DeRef(_2654);
    _2654 = NOVALUE;
    DeRef(_2673);
    _2673 = NOVALUE;
    _2684 = NOVALUE;
    return _2692;
    goto L13; // [426] 436
L12: 

    /** 		return ret*/
    DeRefDS(_st_5095);
    DeRef(_delim_5096);
    DeRef(_2662);
    _2662 = NOVALUE;
    DeRef(_2654);
    _2654 = NOVALUE;
    DeRef(_2673);
    _2673 = NOVALUE;
    _2684 = NOVALUE;
    DeRef(_2692);
    _2692 = NOVALUE;
    return _ret_5099;
L13: 
    ;
}


int _21split_any(int _source_5174, int _delim_5175, int _limit_5177, int _no_empty_5178)
{
    int _ret_5179 = NOVALUE;
    int _start_5180 = NOVALUE;
    int _pos_5181 = NOVALUE;
    int _next_pos_5182 = NOVALUE;
    int _k_5201 = NOVALUE;
    int _2717 = NOVALUE;
    int _2715 = NOVALUE;
    int _2714 = NOVALUE;
    int _2710 = NOVALUE;
    int _2709 = NOVALUE;
    int _2708 = NOVALUE;
    int _2705 = NOVALUE;
    int _2704 = NOVALUE;
    int _2700 = NOVALUE;
    int _2699 = NOVALUE;
    int _2696 = NOVALUE;
    int _2694 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_limit_5177)) {
        _1 = (long)(DBL_PTR(_limit_5177)->dbl);
        if (UNIQUE(DBL_PTR(_limit_5177)) && (DBL_PTR(_limit_5177)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_5177);
        _limit_5177 = _1;
    }
    if (!IS_ATOM_INT(_no_empty_5178)) {
        _1 = (long)(DBL_PTR(_no_empty_5178)->dbl);
        if (UNIQUE(DBL_PTR(_no_empty_5178)) && (DBL_PTR(_no_empty_5178)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_no_empty_5178);
        _no_empty_5178 = _1;
    }

    /** 	sequence ret = {}*/
    RefDS(_5);
    DeRef(_ret_5179);
    _ret_5179 = _5;

    /** 	integer start = 1, pos, next_pos*/
    _start_5180 = 1;

    /** 	if length(delim) = 0 then*/
    if (IS_SEQUENCE(_delim_5175)){
            _2694 = SEQ_PTR(_delim_5175)->length;
    }
    else {
        _2694 = 1;
    }
    if (_2694 != 0)
    goto L1; // [30] 45

    /** 		return {source}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_source_5174);
    *((int *)(_2+4)) = _source_5174;
    _2696 = MAKE_SEQ(_1);
    DeRefDS(_source_5174);
    DeRef(_delim_5175);
    DeRefDS(_ret_5179);
    return _2696;
L1: 

    /** 	while 1 do*/
L2: 

    /** 		pos = search:find_any(delim, source, start)*/
    Ref(_delim_5175);
    RefDS(_source_5174);
    _pos_5181 = _14find_any(_delim_5175, _source_5174, _start_5180);
    if (!IS_ATOM_INT(_pos_5181)) {
        _1 = (long)(DBL_PTR(_pos_5181)->dbl);
        if (UNIQUE(DBL_PTR(_pos_5181)) && (DBL_PTR(_pos_5181)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_5181);
        _pos_5181 = _1;
    }

    /** 		next_pos = pos + 1*/
    _next_pos_5182 = _pos_5181 + 1;

    /** 		if pos then*/
    if (_pos_5181 == 0)
    {
        goto L3; // [72] 129
    }
    else{
    }

    /** 			ret = append(ret, source[start..pos-1])*/
    _2699 = _pos_5181 - 1;
    rhs_slice_target = (object_ptr)&_2700;
    RHS_Slice(_source_5174, _start_5180, _2699);
    RefDS(_2700);
    Append(&_ret_5179, _ret_5179, _2700);
    DeRefDS(_2700);
    _2700 = NOVALUE;

    /** 			start = next_pos*/
    _start_5180 = _next_pos_5182;

    /** 			limit -= 1*/
    _limit_5177 = _limit_5177 - 1;

    /** 			if limit = 0 then*/
    if (_limit_5177 != 0)
    goto L2; // [107] 50

    /** 				exit*/
    goto L3; // [113] 129
    goto L2; // [116] 50

    /** 			exit*/
    goto L3; // [121] 129

    /** 	end while*/
    goto L2; // [126] 50
L3: 

    /** 	ret = append(ret, source[start..$])*/
    if (IS_SEQUENCE(_source_5174)){
            _2704 = SEQ_PTR(_source_5174)->length;
    }
    else {
        _2704 = 1;
    }
    rhs_slice_target = (object_ptr)&_2705;
    RHS_Slice(_source_5174, _start_5180, _2704);
    RefDS(_2705);
    Append(&_ret_5179, _ret_5179, _2705);
    DeRefDS(_2705);
    _2705 = NOVALUE;

    /** 	integer k = length(ret)*/
    if (IS_SEQUENCE(_ret_5179)){
            _k_5201 = SEQ_PTR(_ret_5179)->length;
    }
    else {
        _k_5201 = 1;
    }

    /** 	if no_empty then*/
    if (_no_empty_5178 == 0)
    {
        goto L4; // [152] 221
    }
    else{
    }

    /** 		k = 0*/
    _k_5201 = 0;

    /** 		for i = 1 to length(ret) do*/
    if (IS_SEQUENCE(_ret_5179)){
            _2708 = SEQ_PTR(_ret_5179)->length;
    }
    else {
        _2708 = 1;
    }
    {
        int _i_5205;
        _i_5205 = 1;
L5: 
        if (_i_5205 > _2708){
            goto L6; // [167] 220
        }

        /** 			if length(ret[i]) != 0 then*/
        _2 = (int)SEQ_PTR(_ret_5179);
        _2709 = (int)*(((s1_ptr)_2)->base + _i_5205);
        if (IS_SEQUENCE(_2709)){
                _2710 = SEQ_PTR(_2709)->length;
        }
        else {
            _2710 = 1;
        }
        _2709 = NOVALUE;
        if (_2710 == 0)
        goto L7; // [183] 213

        /** 				k += 1*/
        _k_5201 = _k_5201 + 1;

        /** 				if k != i then*/
        if (_k_5201 == _i_5205)
        goto L8; // [197] 212

        /** 					ret[k] = ret[i]*/
        _2 = (int)SEQ_PTR(_ret_5179);
        _2714 = (int)*(((s1_ptr)_2)->base + _i_5205);
        Ref(_2714);
        _2 = (int)SEQ_PTR(_ret_5179);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_5179 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _k_5201);
        _1 = *(int *)_2;
        *(int *)_2 = _2714;
        if( _1 != _2714 ){
            DeRef(_1);
        }
        _2714 = NOVALUE;
L8: 
L7: 

        /** 		end for*/
        _i_5205 = _i_5205 + 1;
        goto L5; // [215] 174
L6: 
        ;
    }
L4: 

    /** 	if k < length(ret) then*/
    if (IS_SEQUENCE(_ret_5179)){
            _2715 = SEQ_PTR(_ret_5179)->length;
    }
    else {
        _2715 = 1;
    }
    if (_k_5201 >= _2715)
    goto L9; // [226] 244

    /** 		return ret[1 .. k]*/
    rhs_slice_target = (object_ptr)&_2717;
    RHS_Slice(_ret_5179, 1, _k_5201);
    DeRefDS(_source_5174);
    DeRef(_delim_5175);
    DeRefDS(_ret_5179);
    DeRef(_2696);
    _2696 = NOVALUE;
    DeRef(_2699);
    _2699 = NOVALUE;
    _2709 = NOVALUE;
    return _2717;
    goto LA; // [241] 251
L9: 

    /** 		return ret*/
    DeRefDS(_source_5174);
    DeRef(_delim_5175);
    DeRef(_2696);
    _2696 = NOVALUE;
    DeRef(_2699);
    _2699 = NOVALUE;
    _2709 = NOVALUE;
    DeRef(_2717);
    _2717 = NOVALUE;
    return _ret_5179;
LA: 
    ;
}


int _21join(int _items_5222, int _delim_5223)
{
    int _ret_5225 = NOVALUE;
    int _2727 = NOVALUE;
    int _2726 = NOVALUE;
    int _2724 = NOVALUE;
    int _2723 = NOVALUE;
    int _2722 = NOVALUE;
    int _2721 = NOVALUE;
    int _2719 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length(items) then return {} end if*/
    if (IS_SEQUENCE(_items_5222)){
            _2719 = SEQ_PTR(_items_5222)->length;
    }
    else {
        _2719 = 1;
    }
    if (_2719 != 0)
    goto L1; // [8] 16
    _2719 = NOVALUE;
    RefDS(_5);
    DeRefDS(_items_5222);
    DeRef(_delim_5223);
    DeRef(_ret_5225);
    return _5;
L1: 

    /** 	ret = {}*/
    RefDS(_5);
    DeRef(_ret_5225);
    _ret_5225 = _5;

    /** 	for i=1 to length(items)-1 do*/
    if (IS_SEQUENCE(_items_5222)){
            _2721 = SEQ_PTR(_items_5222)->length;
    }
    else {
        _2721 = 1;
    }
    _2722 = _2721 - 1;
    _2721 = NOVALUE;
    {
        int _i_5230;
        _i_5230 = 1;
L2: 
        if (_i_5230 > _2722){
            goto L3; // [30] 58
        }

        /** 		ret &= items[i] & delim*/
        _2 = (int)SEQ_PTR(_items_5222);
        _2723 = (int)*(((s1_ptr)_2)->base + _i_5230);
        if (IS_SEQUENCE(_2723) && IS_ATOM(_delim_5223)) {
            Ref(_delim_5223);
            Append(&_2724, _2723, _delim_5223);
        }
        else if (IS_ATOM(_2723) && IS_SEQUENCE(_delim_5223)) {
            Ref(_2723);
            Prepend(&_2724, _delim_5223, _2723);
        }
        else {
            Concat((object_ptr)&_2724, _2723, _delim_5223);
            _2723 = NOVALUE;
        }
        _2723 = NOVALUE;
        if (IS_SEQUENCE(_ret_5225) && IS_ATOM(_2724)) {
        }
        else if (IS_ATOM(_ret_5225) && IS_SEQUENCE(_2724)) {
            Ref(_ret_5225);
            Prepend(&_ret_5225, _2724, _ret_5225);
        }
        else {
            Concat((object_ptr)&_ret_5225, _ret_5225, _2724);
        }
        DeRefDS(_2724);
        _2724 = NOVALUE;

        /** 	end for*/
        _i_5230 = _i_5230 + 1;
        goto L2; // [53] 37
L3: 
        ;
    }

    /** 	ret &= items[$]*/
    if (IS_SEQUENCE(_items_5222)){
            _2726 = SEQ_PTR(_items_5222)->length;
    }
    else {
        _2726 = 1;
    }
    _2 = (int)SEQ_PTR(_items_5222);
    _2727 = (int)*(((s1_ptr)_2)->base + _2726);
    if (IS_SEQUENCE(_ret_5225) && IS_ATOM(_2727)) {
        Ref(_2727);
        Append(&_ret_5225, _ret_5225, _2727);
    }
    else if (IS_ATOM(_ret_5225) && IS_SEQUENCE(_2727)) {
        Ref(_ret_5225);
        Prepend(&_ret_5225, _2727, _ret_5225);
    }
    else {
        Concat((object_ptr)&_ret_5225, _ret_5225, _2727);
    }
    _2727 = NOVALUE;

    /** 	return ret*/
    DeRefDS(_items_5222);
    DeRef(_delim_5223);
    DeRef(_2722);
    _2722 = NOVALUE;
    return _ret_5225;
    ;
}


int _21breakup(int _source_5244, int _size_5245, int _style_5246)
{
    int _len_5255 = NOVALUE;
    int _rem_5256 = NOVALUE;
    int _ns_5297 = NOVALUE;
    int _source_idx_5300 = NOVALUE;
    int _k_5307 = NOVALUE;
    int _2787 = NOVALUE;
    int _2786 = NOVALUE;
    int _2784 = NOVALUE;
    int _2781 = NOVALUE;
    int _2779 = NOVALUE;
    int _2778 = NOVALUE;
    int _2777 = NOVALUE;
    int _2776 = NOVALUE;
    int _2774 = NOVALUE;
    int _2773 = NOVALUE;
    int _2772 = NOVALUE;
    int _2771 = NOVALUE;
    int _2769 = NOVALUE;
    int _2768 = NOVALUE;
    int _2766 = NOVALUE;
    int _2764 = NOVALUE;
    int _2763 = NOVALUE;
    int _2761 = NOVALUE;
    int _2758 = NOVALUE;
    int _2757 = NOVALUE;
    int _2754 = NOVALUE;
    int _2753 = NOVALUE;
    int _2749 = NOVALUE;
    int _2744 = NOVALUE;
    int _2742 = NOVALUE;
    int _2741 = NOVALUE;
    int _2740 = NOVALUE;
    int _2739 = NOVALUE;
    int _2737 = NOVALUE;
    int _2735 = NOVALUE;
    int _2733 = NOVALUE;
    int _2732 = NOVALUE;
    int _2731 = NOVALUE;
    int _2730 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_style_5246)) {
        _1 = (long)(DBL_PTR(_style_5246)->dbl);
        if (UNIQUE(DBL_PTR(_style_5246)) && (DBL_PTR(_style_5246)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_style_5246);
        _style_5246 = _1;
    }

    /** 	if atom(size) and not integer(size) then*/
    _2730 = IS_ATOM(_size_5245);
    if (_2730 == 0) {
        goto L1; // [12] 32
    }
    if (IS_ATOM_INT(_size_5245))
    _2732 = 1;
    else if (IS_ATOM_DBL(_size_5245))
    _2732 = IS_ATOM_INT(DoubleToInt(_size_5245));
    else
    _2732 = 0;
    _2733 = (_2732 == 0);
    _2732 = NOVALUE;
    if (_2733 == 0)
    {
        DeRef(_2733);
        _2733 = NOVALUE;
        goto L1; // [23] 32
    }
    else{
        DeRef(_2733);
        _2733 = NOVALUE;
    }

    /** 		size = floor(size)*/
    _0 = _size_5245;
    if (IS_ATOM_INT(_size_5245))
    _size_5245 = e_floor(_size_5245);
    else
    _size_5245 = unary_op(FLOOR, _size_5245);
    DeRef(_0);
L1: 

    /** 	if integer(size) then*/
    if (IS_ATOM_INT(_size_5245))
    _2735 = 1;
    else if (IS_ATOM_DBL(_size_5245))
    _2735 = IS_ATOM_INT(DoubleToInt(_size_5245));
    else
    _2735 = 0;
    if (_2735 == 0)
    {
        _2735 = NOVALUE;
        goto L2; // [37] 269
    }
    else{
        _2735 = NOVALUE;
    }

    /** 		integer len*/

    /** 		integer rem*/

    /** 		if style = BK_LEN then*/
    if (_style_5246 != 1)
    goto L3; // [48] 133

    /** 			if size < 1 or size >= length(source) then*/
    if (IS_ATOM_INT(_size_5245)) {
        _2737 = (_size_5245 < 1);
    }
    else {
        _2737 = binary_op(LESS, _size_5245, 1);
    }
    if (IS_ATOM_INT(_2737)) {
        if (_2737 != 0) {
            goto L4; // [58] 74
        }
    }
    else {
        if (DBL_PTR(_2737)->dbl != 0.0) {
            goto L4; // [58] 74
        }
    }
    if (IS_SEQUENCE(_source_5244)){
            _2739 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2739 = 1;
    }
    if (IS_ATOM_INT(_size_5245)) {
        _2740 = (_size_5245 >= _2739);
    }
    else {
        _2740 = binary_op(GREATEREQ, _size_5245, _2739);
    }
    _2739 = NOVALUE;
    if (_2740 == 0) {
        DeRef(_2740);
        _2740 = NOVALUE;
        goto L5; // [70] 85
    }
    else {
        if (!IS_ATOM_INT(_2740) && DBL_PTR(_2740)->dbl == 0.0){
            DeRef(_2740);
            _2740 = NOVALUE;
            goto L5; // [70] 85
        }
        DeRef(_2740);
        _2740 = NOVALUE;
    }
    DeRef(_2740);
    _2740 = NOVALUE;
L4: 

    /** 				return {source}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_source_5244);
    *((int *)(_2+4)) = _source_5244;
    _2741 = MAKE_SEQ(_1);
    DeRefDS(_source_5244);
    DeRef(_size_5245);
    DeRef(_ns_5297);
    DeRef(_2737);
    _2737 = NOVALUE;
    return _2741;
L5: 

    /** 			len = floor(length(source) / size)*/
    if (IS_SEQUENCE(_source_5244)){
            _2742 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2742 = 1;
    }
    if (IS_ATOM_INT(_size_5245)) {
        if (_size_5245 > 0 && _2742 >= 0) {
            _len_5255 = _2742 / _size_5245;
        }
        else {
            temp_dbl = floor((double)_2742 / (double)_size_5245);
            _len_5255 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _2742, _size_5245);
        _len_5255 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _2742 = NOVALUE;
    if (!IS_ATOM_INT(_len_5255)) {
        _1 = (long)(DBL_PTR(_len_5255)->dbl);
        if (UNIQUE(DBL_PTR(_len_5255)) && (DBL_PTR(_len_5255)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_5255);
        _len_5255 = _1;
    }

    /** 			rem = remainder(length(source), size)*/
    if (IS_SEQUENCE(_source_5244)){
            _2744 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2744 = 1;
    }
    if (IS_ATOM_INT(_size_5245)) {
        _rem_5256 = (_2744 % _size_5245);
    }
    else {
        _rem_5256 = binary_op(REMAINDER, _2744, _size_5245);
    }
    _2744 = NOVALUE;
    if (!IS_ATOM_INT(_rem_5256)) {
        _1 = (long)(DBL_PTR(_rem_5256)->dbl);
        if (UNIQUE(DBL_PTR(_rem_5256)) && (DBL_PTR(_rem_5256)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rem_5256);
        _rem_5256 = _1;
    }

    /** 			size = repeat(size, len)*/
    _0 = _size_5245;
    _size_5245 = Repeat(_size_5245, _len_5255);
    DeRef(_0);

    /** 			if rem > 0 then*/
    if (_rem_5256 <= 0)
    goto L6; // [119] 268

    /** 				size &= rem*/
    Append(&_size_5245, _size_5245, _rem_5256);
    goto L6; // [130] 268
L3: 

    /** 			if size > length(source) then*/
    if (IS_SEQUENCE(_source_5244)){
            _2749 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2749 = 1;
    }
    if (binary_op_a(LESSEQ, _size_5245, _2749)){
        _2749 = NOVALUE;
        goto L7; // [138] 148
    }
    _2749 = NOVALUE;

    /** 				size = length(source)*/
    DeRef(_size_5245);
    if (IS_SEQUENCE(_source_5244)){
            _size_5245 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _size_5245 = 1;
    }
L7: 

    /** 			if size < 1 then*/
    if (binary_op_a(GREATEREQ, _size_5245, 1)){
        goto L8; // [150] 165
    }

    /** 				return {source}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_source_5244);
    *((int *)(_2+4)) = _source_5244;
    _2753 = MAKE_SEQ(_1);
    DeRefDS(_source_5244);
    DeRef(_size_5245);
    DeRef(_ns_5297);
    DeRef(_2737);
    _2737 = NOVALUE;
    DeRef(_2741);
    _2741 = NOVALUE;
    return _2753;
L8: 

    /** 			len = floor(length(source) / size)*/
    if (IS_SEQUENCE(_source_5244)){
            _2754 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2754 = 1;
    }
    if (IS_ATOM_INT(_size_5245)) {
        if (_size_5245 > 0 && _2754 >= 0) {
            _len_5255 = _2754 / _size_5245;
        }
        else {
            temp_dbl = floor((double)_2754 / (double)_size_5245);
            _len_5255 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _2754, _size_5245);
        _len_5255 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _2754 = NOVALUE;
    if (!IS_ATOM_INT(_len_5255)) {
        _1 = (long)(DBL_PTR(_len_5255)->dbl);
        if (UNIQUE(DBL_PTR(_len_5255)) && (DBL_PTR(_len_5255)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_5255);
        _len_5255 = _1;
    }

    /** 			if len < 1 then*/
    if (_len_5255 >= 1)
    goto L9; // [180] 192

    /** 				len = 1*/
    _len_5255 = 1;
L9: 

    /** 			rem = length(source) - (size * len)*/
    if (IS_SEQUENCE(_source_5244)){
            _2757 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2757 = 1;
    }
    if (IS_ATOM_INT(_size_5245)) {
        if (_size_5245 == (short)_size_5245 && _len_5255 <= INT15 && _len_5255 >= -INT15)
        _2758 = _size_5245 * _len_5255;
        else
        _2758 = NewDouble(_size_5245 * (double)_len_5255);
    }
    else {
        _2758 = binary_op(MULTIPLY, _size_5245, _len_5255);
    }
    if (IS_ATOM_INT(_2758)) {
        _rem_5256 = _2757 - _2758;
    }
    else {
        _rem_5256 = binary_op(MINUS, _2757, _2758);
    }
    _2757 = NOVALUE;
    DeRef(_2758);
    _2758 = NOVALUE;
    if (!IS_ATOM_INT(_rem_5256)) {
        _1 = (long)(DBL_PTR(_rem_5256)->dbl);
        if (UNIQUE(DBL_PTR(_rem_5256)) && (DBL_PTR(_rem_5256)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rem_5256);
        _rem_5256 = _1;
    }

    /** 			size = repeat(len, size)*/
    _0 = _size_5245;
    _size_5245 = Repeat(_len_5255, _size_5245);
    DeRef(_0);

    /** 			for i = 1 to length(size) do*/
    if (IS_SEQUENCE(_size_5245)){
            _2761 = SEQ_PTR(_size_5245)->length;
    }
    else {
        _2761 = 1;
    }
    {
        int _i_5290;
        _i_5290 = 1;
LA: 
        if (_i_5290 > _2761){
            goto LB; // [220] 267
        }

        /** 				if rem = 0 then*/
        if (_rem_5256 != 0)
        goto LC; // [229] 238

        /** 					exit*/
        goto LB; // [235] 267
LC: 

        /** 				size[i] += 1*/
        _2 = (int)SEQ_PTR(_size_5245);
        _2763 = (int)*(((s1_ptr)_2)->base + _i_5290);
        if (IS_ATOM_INT(_2763)) {
            _2764 = _2763 + 1;
            if (_2764 > MAXINT){
                _2764 = NewDouble((double)_2764);
            }
        }
        else
        _2764 = binary_op(PLUS, 1, _2763);
        _2763 = NOVALUE;
        _2 = (int)SEQ_PTR(_size_5245);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _size_5245 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5290);
        _1 = *(int *)_2;
        *(int *)_2 = _2764;
        if( _1 != _2764 ){
            DeRef(_1);
        }
        _2764 = NOVALUE;

        /** 				rem -= 1*/
        _rem_5256 = _rem_5256 - 1;

        /** 			end for*/
        _i_5290 = _i_5290 + 1;
        goto LA; // [262] 227
LB: 
        ;
    }
L6: 
L2: 

    /** 	sequence ns = repeat(0, length(size))*/
    if (IS_SEQUENCE(_size_5245)){
            _2766 = SEQ_PTR(_size_5245)->length;
    }
    else {
        _2766 = 1;
    }
    DeRef(_ns_5297);
    _ns_5297 = Repeat(0, _2766);
    _2766 = NOVALUE;

    /** 	integer source_idx = 1*/
    _source_idx_5300 = 1;

    /** 	for i = 1 to length(size) do*/
    if (IS_SEQUENCE(_size_5245)){
            _2768 = SEQ_PTR(_size_5245)->length;
    }
    else {
        _2768 = 1;
    }
    {
        int _i_5302;
        _i_5302 = 1;
LD: 
        if (_i_5302 > _2768){
            goto LE; // [292] 432
        }

        /** 		if source_idx <= length(source) then*/
        if (IS_SEQUENCE(_source_5244)){
                _2769 = SEQ_PTR(_source_5244)->length;
        }
        else {
            _2769 = 1;
        }
        if (_source_idx_5300 > _2769)
        goto LF; // [304] 418

        /** 			integer k = 1*/
        _k_5307 = 1;

        /** 			ns[i] = repeat(0, size[i])*/
        _2 = (int)SEQ_PTR(_size_5245);
        _2771 = (int)*(((s1_ptr)_2)->base + _i_5302);
        _2772 = Repeat(0, _2771);
        _2771 = NOVALUE;
        _2 = (int)SEQ_PTR(_ns_5297);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ns_5297 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5302);
        _1 = *(int *)_2;
        *(int *)_2 = _2772;
        if( _1 != _2772 ){
            DeRef(_1);
        }
        _2772 = NOVALUE;

        /** 			for j = 1 to size[i] do*/
        _2 = (int)SEQ_PTR(_size_5245);
        _2773 = (int)*(((s1_ptr)_2)->base + _i_5302);
        {
            int _j_5311;
            _j_5311 = 1;
L10: 
            if (binary_op_a(GREATER, _j_5311, _2773)){
                goto L11; // [335] 413
            }

            /** 				if source_idx > length(source) then*/
            if (IS_SEQUENCE(_source_5244)){
                    _2774 = SEQ_PTR(_source_5244)->length;
            }
            else {
                _2774 = 1;
            }
            if (_source_idx_5300 <= _2774)
            goto L12; // [347] 375

            /** 					ns[i] = ns[i][1 .. k-1]*/
            _2 = (int)SEQ_PTR(_ns_5297);
            _2776 = (int)*(((s1_ptr)_2)->base + _i_5302);
            _2777 = _k_5307 - 1;
            rhs_slice_target = (object_ptr)&_2778;
            RHS_Slice(_2776, 1, _2777);
            _2776 = NOVALUE;
            _2 = (int)SEQ_PTR(_ns_5297);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _ns_5297 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_5302);
            _1 = *(int *)_2;
            *(int *)_2 = _2778;
            if( _1 != _2778 ){
                DeRef(_1);
            }
            _2778 = NOVALUE;

            /** 					exit*/
            goto L11; // [372] 413
L12: 

            /** 				ns[i][k] = source[source_idx]*/
            _2 = (int)SEQ_PTR(_ns_5297);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _ns_5297 = MAKE_SEQ(_2);
            }
            _3 = (int)(_i_5302 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_source_5244);
            _2781 = (int)*(((s1_ptr)_2)->base + _source_idx_5300);
            Ref(_2781);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _k_5307);
            _1 = *(int *)_2;
            *(int *)_2 = _2781;
            if( _1 != _2781 ){
                DeRef(_1);
            }
            _2781 = NOVALUE;
            _2779 = NOVALUE;

            /** 				k += 1*/
            _k_5307 = _k_5307 + 1;

            /** 				source_idx += 1*/
            _source_idx_5300 = _source_idx_5300 + 1;

            /** 			end for*/
            _0 = _j_5311;
            if (IS_ATOM_INT(_j_5311)) {
                _j_5311 = _j_5311 + 1;
                if ((long)((unsigned long)_j_5311 +(unsigned long) HIGH_BITS) >= 0){
                    _j_5311 = NewDouble((double)_j_5311);
                }
            }
            else {
                _j_5311 = binary_op_a(PLUS, _j_5311, 1);
            }
            DeRef(_0);
            goto L10; // [408] 342
L11: 
            ;
            DeRef(_j_5311);
        }
        goto L13; // [415] 425
LF: 

        /** 			ns[i] = {}*/
        RefDS(_5);
        _2 = (int)SEQ_PTR(_ns_5297);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ns_5297 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_5302);
        _1 = *(int *)_2;
        *(int *)_2 = _5;
        DeRef(_1);
L13: 

        /** 	end for*/
        _i_5302 = _i_5302 + 1;
        goto LD; // [427] 299
LE: 
        ;
    }

    /** 	if source_idx <= length(source) then*/
    if (IS_SEQUENCE(_source_5244)){
            _2784 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2784 = 1;
    }
    if (_source_idx_5300 > _2784)
    goto L14; // [437] 456

    /** 		ns = append(ns, source[source_idx .. $])*/
    if (IS_SEQUENCE(_source_5244)){
            _2786 = SEQ_PTR(_source_5244)->length;
    }
    else {
        _2786 = 1;
    }
    rhs_slice_target = (object_ptr)&_2787;
    RHS_Slice(_source_5244, _source_idx_5300, _2786);
    RefDS(_2787);
    Append(&_ns_5297, _ns_5297, _2787);
    DeRefDS(_2787);
    _2787 = NOVALUE;
L14: 

    /** 	return ns*/
    DeRefDS(_source_5244);
    DeRef(_size_5245);
    DeRef(_2737);
    _2737 = NOVALUE;
    DeRef(_2741);
    _2741 = NOVALUE;
    DeRef(_2753);
    _2753 = NOVALUE;
    _2773 = NOVALUE;
    DeRef(_2777);
    _2777 = NOVALUE;
    return _ns_5297;
    ;
}


int _21flatten(int _s_5333, int _delim_5334)
{
    int _ret_5335 = NOVALUE;
    int _x_5336 = NOVALUE;
    int _len_5337 = NOVALUE;
    int _pos_5338 = NOVALUE;
    int _temp_5357 = NOVALUE;
    int _2816 = NOVALUE;
    int _2815 = NOVALUE;
    int _2814 = NOVALUE;
    int _2812 = NOVALUE;
    int _2811 = NOVALUE;
    int _2810 = NOVALUE;
    int _2808 = NOVALUE;
    int _2806 = NOVALUE;
    int _2805 = NOVALUE;
    int _2804 = NOVALUE;
    int _2803 = NOVALUE;
    int _2801 = NOVALUE;
    int _2800 = NOVALUE;
    int _2799 = NOVALUE;
    int _2798 = NOVALUE;
    int _2797 = NOVALUE;
    int _2796 = NOVALUE;
    int _2795 = NOVALUE;
    int _2793 = NOVALUE;
    int _2792 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ret = s*/
    RefDS(_s_5333);
    DeRef(_ret_5335);
    _ret_5335 = _s_5333;

    /** 	pos = 1*/
    _pos_5338 = 1;

    /** 	len = length(ret)*/
    if (IS_SEQUENCE(_ret_5335)){
            _len_5337 = SEQ_PTR(_ret_5335)->length;
    }
    else {
        _len_5337 = 1;
    }

    /** 	while pos <= len do*/
L1: 
    if (_pos_5338 > _len_5337)
    goto L2; // [29] 197

    /** 		x = ret[pos]*/
    DeRef(_x_5336);
    _2 = (int)SEQ_PTR(_ret_5335);
    _x_5336 = (int)*(((s1_ptr)_2)->base + _pos_5338);
    Ref(_x_5336);

    /** 		if sequence(x) then*/
    _2792 = IS_SEQUENCE(_x_5336);
    if (_2792 == 0)
    {
        _2792 = NOVALUE;
        goto L3; // [44] 183
    }
    else{
        _2792 = NOVALUE;
    }

    /** 			if length(delim) = 0 then*/
    if (IS_SEQUENCE(_delim_5334)){
            _2793 = SEQ_PTR(_delim_5334)->length;
    }
    else {
        _2793 = 1;
    }
    if (_2793 != 0)
    goto L4; // [52] 96

    /** 				ret = ret[1..pos-1] & flatten(x) & ret[pos+1 .. $]*/
    _2795 = _pos_5338 - 1;
    rhs_slice_target = (object_ptr)&_2796;
    RHS_Slice(_ret_5335, 1, _2795);
    Ref(_x_5336);
    DeRef(_2797);
    _2797 = _x_5336;
    RefDS(_5);
    _2798 = _21flatten(_2797, _5);
    _2797 = NOVALUE;
    _2799 = _pos_5338 + 1;
    if (_2799 > MAXINT){
        _2799 = NewDouble((double)_2799);
    }
    if (IS_SEQUENCE(_ret_5335)){
            _2800 = SEQ_PTR(_ret_5335)->length;
    }
    else {
        _2800 = 1;
    }
    rhs_slice_target = (object_ptr)&_2801;
    RHS_Slice(_ret_5335, _2799, _2800);
    {
        int concat_list[3];

        concat_list[0] = _2801;
        concat_list[1] = _2798;
        concat_list[2] = _2796;
        Concat_N((object_ptr)&_ret_5335, concat_list, 3);
    }
    DeRefDS(_2801);
    _2801 = NOVALUE;
    DeRef(_2798);
    _2798 = NOVALUE;
    DeRefDS(_2796);
    _2796 = NOVALUE;
    goto L5; // [93] 173
L4: 

    /** 				sequence temp = ret[1..pos-1] & flatten(x)*/
    _2803 = _pos_5338 - 1;
    rhs_slice_target = (object_ptr)&_2804;
    RHS_Slice(_ret_5335, 1, _2803);
    Ref(_x_5336);
    DeRef(_2805);
    _2805 = _x_5336;
    RefDS(_5);
    _2806 = _21flatten(_2805, _5);
    _2805 = NOVALUE;
    if (IS_SEQUENCE(_2804) && IS_ATOM(_2806)) {
        Ref(_2806);
        Append(&_temp_5357, _2804, _2806);
    }
    else if (IS_ATOM(_2804) && IS_SEQUENCE(_2806)) {
    }
    else {
        Concat((object_ptr)&_temp_5357, _2804, _2806);
        DeRefDS(_2804);
        _2804 = NOVALUE;
    }
    DeRef(_2804);
    _2804 = NOVALUE;
    DeRef(_2806);
    _2806 = NOVALUE;

    /** 				if pos != length(ret) then*/
    if (IS_SEQUENCE(_ret_5335)){
            _2808 = SEQ_PTR(_ret_5335)->length;
    }
    else {
        _2808 = 1;
    }
    if (_pos_5338 == _2808)
    goto L6; // [124] 151

    /** 					ret = temp &  delim & ret[pos+1 .. $]*/
    _2810 = _pos_5338 + 1;
    if (_2810 > MAXINT){
        _2810 = NewDouble((double)_2810);
    }
    if (IS_SEQUENCE(_ret_5335)){
            _2811 = SEQ_PTR(_ret_5335)->length;
    }
    else {
        _2811 = 1;
    }
    rhs_slice_target = (object_ptr)&_2812;
    RHS_Slice(_ret_5335, _2810, _2811);
    {
        int concat_list[3];

        concat_list[0] = _2812;
        concat_list[1] = _delim_5334;
        concat_list[2] = _temp_5357;
        Concat_N((object_ptr)&_ret_5335, concat_list, 3);
    }
    DeRefDS(_2812);
    _2812 = NOVALUE;
    goto L7; // [148] 170
L6: 

    /** 					ret = temp & ret[pos+1 .. $]*/
    _2814 = _pos_5338 + 1;
    if (_2814 > MAXINT){
        _2814 = NewDouble((double)_2814);
    }
    if (IS_SEQUENCE(_ret_5335)){
            _2815 = SEQ_PTR(_ret_5335)->length;
    }
    else {
        _2815 = 1;
    }
    rhs_slice_target = (object_ptr)&_2816;
    RHS_Slice(_ret_5335, _2814, _2815);
    Concat((object_ptr)&_ret_5335, _temp_5357, _2816);
    DeRefDS(_2816);
    _2816 = NOVALUE;
L7: 
    DeRef(_temp_5357);
    _temp_5357 = NOVALUE;
L5: 

    /** 			len = length(ret)*/
    if (IS_SEQUENCE(_ret_5335)){
            _len_5337 = SEQ_PTR(_ret_5335)->length;
    }
    else {
        _len_5337 = 1;
    }
    goto L1; // [180] 29
L3: 

    /** 			pos += 1*/
    _pos_5338 = _pos_5338 + 1;

    /** 	end while*/
    goto L1; // [194] 29
L2: 

    /** 	return ret*/
    DeRefDS(_s_5333);
    DeRef(_delim_5334);
    DeRef(_x_5336);
    DeRef(_2795);
    _2795 = NOVALUE;
    DeRef(_2803);
    _2803 = NOVALUE;
    DeRef(_2810);
    _2810 = NOVALUE;
    DeRef(_2799);
    _2799 = NOVALUE;
    DeRef(_2814);
    _2814 = NOVALUE;
    return _ret_5335;
    ;
}


int _21pivot(int _data_p_5380, int _pivot_p_5381)
{
    int _result__5382 = NOVALUE;
    int _pos__5383 = NOVALUE;
    int _2829 = NOVALUE;
    int _2828 = NOVALUE;
    int _2827 = NOVALUE;
    int _2825 = NOVALUE;
    int _2824 = NOVALUE;
    int _2823 = NOVALUE;
    int _2821 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_ = {{}, {}, {}}*/
    _0 = _result__5382;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDSn(_5, 3);
    *((int *)(_2+4)) = _5;
    *((int *)(_2+8)) = _5;
    *((int *)(_2+12)) = _5;
    _result__5382 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	if atom(data_p) then*/
    _2821 = IS_ATOM(_data_p_5380);
    if (_2821 == 0)
    {
        _2821 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _2821 = NOVALUE;
    }

    /** 		data_p = {data_p}*/
    _0 = _data_p_5380;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_data_p_5380);
    *((int *)(_2+4)) = _data_p_5380;
    _data_p_5380 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = 1 to length(data_p) do*/
    if (IS_SEQUENCE(_data_p_5380)){
            _2823 = SEQ_PTR(_data_p_5380)->length;
    }
    else {
        _2823 = 1;
    }
    {
        int _i_5389;
        _i_5389 = 1;
L2: 
        if (_i_5389 > _2823){
            goto L3; // [29] 77
        }

        /** 		pos_ = eu:compare(data_p[i], pivot_p) + 2*/
        _2 = (int)SEQ_PTR(_data_p_5380);
        _2824 = (int)*(((s1_ptr)_2)->base + _i_5389);
        if (IS_ATOM_INT(_2824) && IS_ATOM_INT(_pivot_p_5381)){
            _2825 = (_2824 < _pivot_p_5381) ? -1 : (_2824 > _pivot_p_5381);
        }
        else{
            _2825 = compare(_2824, _pivot_p_5381);
        }
        _2824 = NOVALUE;
        _pos__5383 = _2825 + 2;
        _2825 = NOVALUE;

        /** 		result_[pos_] = append(result_[pos_], data_p[i])*/
        _2 = (int)SEQ_PTR(_result__5382);
        _2827 = (int)*(((s1_ptr)_2)->base + _pos__5383);
        _2 = (int)SEQ_PTR(_data_p_5380);
        _2828 = (int)*(((s1_ptr)_2)->base + _i_5389);
        Ref(_2828);
        Append(&_2829, _2827, _2828);
        _2827 = NOVALUE;
        _2828 = NOVALUE;
        _2 = (int)SEQ_PTR(_result__5382);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _result__5382 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__5383);
        _1 = *(int *)_2;
        *(int *)_2 = _2829;
        if( _1 != _2829 ){
            DeRefDS(_1);
        }
        _2829 = NOVALUE;

        /** 	end for*/
        _i_5389 = _i_5389 + 1;
        goto L2; // [72] 36
L3: 
        ;
    }

    /** 	return result_*/
    DeRef(_data_p_5380);
    DeRef(_pivot_p_5381);
    return _result__5382;
    ;
}


int _21build_list(int _source_5399, int _transformer_5400, int _singleton_5401, int _user_data_5402)
{
    int _result_5403 = NOVALUE;
    int _x_5404 = NOVALUE;
    int _new_x_5405 = NOVALUE;
    int _2846 = NOVALUE;
    int _2844 = NOVALUE;
    int _2842 = NOVALUE;
    int _2840 = NOVALUE;
    int _2838 = NOVALUE;
    int _2837 = NOVALUE;
    int _2835 = NOVALUE;
    int _2834 = NOVALUE;
    int _2833 = NOVALUE;
    int _2832 = NOVALUE;
    int _2830 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_singleton_5401)) {
        _1 = (long)(DBL_PTR(_singleton_5401)->dbl);
        if (UNIQUE(DBL_PTR(_singleton_5401)) && (DBL_PTR(_singleton_5401)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_singleton_5401);
        _singleton_5401 = _1;
    }

    /** 	sequence result = {}*/
    RefDS(_5);
    DeRef(_result_5403);
    _result_5403 = _5;

    /** 	if atom(transformer) then*/
    _2830 = IS_ATOM(_transformer_5400);
    if (_2830 == 0)
    {
        _2830 = NOVALUE;
        goto L1; // [19] 29
    }
    else{
        _2830 = NOVALUE;
    }

    /** 		transformer = {transformer}*/
    _0 = _transformer_5400;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_transformer_5400);
    *((int *)(_2+4)) = _transformer_5400;
    _transformer_5400 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = 1 to length(source) do*/
    if (IS_SEQUENCE(_source_5399)){
            _2832 = SEQ_PTR(_source_5399)->length;
    }
    else {
        _2832 = 1;
    }
    {
        int _i_5410;
        _i_5410 = 1;
L2: 
        if (_i_5410 > _2832){
            goto L3; // [34] 195
        }

        /** 		x = {source[i], {i, length(source)}, user_data}*/
        _2 = (int)SEQ_PTR(_source_5399);
        _2833 = (int)*(((s1_ptr)_2)->base + _i_5410);
        if (IS_SEQUENCE(_source_5399)){
                _2834 = SEQ_PTR(_source_5399)->length;
        }
        else {
            _2834 = 1;
        }
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_5410;
        ((int *)_2)[2] = _2834;
        _2835 = MAKE_SEQ(_1);
        _2834 = NOVALUE;
        _0 = _x_5404;
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_2833);
        *((int *)(_2+4)) = _2833;
        *((int *)(_2+8)) = _2835;
        Ref(_user_data_5402);
        *((int *)(_2+12)) = _user_data_5402;
        _x_5404 = MAKE_SEQ(_1);
        DeRef(_0);
        _2835 = NOVALUE;
        _2833 = NOVALUE;

        /** 		for j = 1 to length(transformer) do*/
        if (IS_SEQUENCE(_transformer_5400)){
                _2837 = SEQ_PTR(_transformer_5400)->length;
        }
        else {
            _2837 = 1;
        }
        {
            int _j_5417;
            _j_5417 = 1;
L4: 
            if (_j_5417 > _2837){
                goto L5; // [65] 188
            }

            /** 			if transformer[j] >= 0 then*/
            _2 = (int)SEQ_PTR(_transformer_5400);
            _2838 = (int)*(((s1_ptr)_2)->base + _j_5417);
            if (binary_op_a(LESS, _2838, 0)){
                _2838 = NOVALUE;
                goto L6; // [78] 145
            }
            _2838 = NOVALUE;

            /** 				new_x = call_func(transformer[j], x)*/
            _2 = (int)SEQ_PTR(_transformer_5400);
            _2840 = (int)*(((s1_ptr)_2)->base + _j_5417);
            _1 = (int)SEQ_PTR(_x_5404);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_2840].addr;
            Ref(*(int *)(_2+4));
            Ref(*(int *)(_2+8));
            Ref(*(int *)(_2+12));
            _1 = (*(int (*)())_0)(
                                *(int *)(_2+4), 
                                *(int *)(_2+8), 
                                *(int *)(_2+12)
                                 );
            DeRef(_new_x_5405);
            _new_x_5405 = _1;

            /** 				if length(new_x) = 0 then*/
            if (IS_SEQUENCE(_new_x_5405)){
                    _2842 = SEQ_PTR(_new_x_5405)->length;
            }
            else {
                _2842 = 1;
            }
            if (_2842 != 0)
            goto L7; // [97] 106

            /** 					continue*/
            goto L8; // [103] 183
L7: 

            /** 				if new_x[1] = 0 then*/
            _2 = (int)SEQ_PTR(_new_x_5405);
            _2844 = (int)*(((s1_ptr)_2)->base + 1);
            if (binary_op_a(NOTEQ, _2844, 0)){
                _2844 = NOVALUE;
                goto L9; // [112] 121
            }
            _2844 = NOVALUE;

            /** 					continue*/
            goto L8; // [118] 183
L9: 

            /** 				if new_x[1] < 0 then*/
            _2 = (int)SEQ_PTR(_new_x_5405);
            _2846 = (int)*(((s1_ptr)_2)->base + 1);
            if (binary_op_a(GREATEREQ, _2846, 0)){
                _2846 = NOVALUE;
                goto LA; // [127] 136
            }
            _2846 = NOVALUE;

            /** 					exit*/
            goto L5; // [133] 188
LA: 

            /** 				new_x = new_x[2]*/
            _0 = _new_x_5405;
            _2 = (int)SEQ_PTR(_new_x_5405);
            _new_x_5405 = (int)*(((s1_ptr)_2)->base + 2);
            Ref(_new_x_5405);
            DeRef(_0);
            goto LB; // [142] 152
L6: 

            /** 				new_x = x[1]*/
            DeRef(_new_x_5405);
            _2 = (int)SEQ_PTR(_x_5404);
            _new_x_5405 = (int)*(((s1_ptr)_2)->base + 1);
            Ref(_new_x_5405);
LB: 

            /** 			if singleton then*/
            if (_singleton_5401 == 0)
            {
                goto LC; // [154] 168
            }
            else{
            }

            /** 				result = append(result, new_x)*/
            Ref(_new_x_5405);
            Append(&_result_5403, _result_5403, _new_x_5405);
            goto L5; // [165] 188
LC: 

            /** 				result &= new_x*/
            if (IS_SEQUENCE(_result_5403) && IS_ATOM(_new_x_5405)) {
                Ref(_new_x_5405);
                Append(&_result_5403, _result_5403, _new_x_5405);
            }
            else if (IS_ATOM(_result_5403) && IS_SEQUENCE(_new_x_5405)) {
            }
            else {
                Concat((object_ptr)&_result_5403, _result_5403, _new_x_5405);
            }

            /** 			exit*/
            goto L5; // [179] 188

            /** 		end for*/
L8: 
            _j_5417 = _j_5417 + 1;
            goto L4; // [183] 72
L5: 
            ;
        }

        /** 	end for*/
        _i_5410 = _i_5410 + 1;
        goto L2; // [190] 41
L3: 
        ;
    }

    /** 	return result*/
    DeRefDS(_source_5399);
    DeRef(_transformer_5400);
    DeRef(_user_data_5402);
    DeRef(_x_5404);
    DeRef(_new_x_5405);
    _2840 = NOVALUE;
    return _result_5403;
    ;
}


int _21transform(int _source_data_5442, int _transformer_rids_5443)
{
    int _lResult_5444 = NOVALUE;
    int _2866 = NOVALUE;
    int _2865 = NOVALUE;
    int _2864 = NOVALUE;
    int _2863 = NOVALUE;
    int _2862 = NOVALUE;
    int _2861 = NOVALUE;
    int _2860 = NOVALUE;
    int _2858 = NOVALUE;
    int _2857 = NOVALUE;
    int _2856 = NOVALUE;
    int _2855 = NOVALUE;
    int _2854 = NOVALUE;
    int _2852 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lResult = source_data*/
    RefDS(_source_data_5442);
    DeRef(_lResult_5444);
    _lResult_5444 = _source_data_5442;

    /** 	if atom(transformer_rids) then*/
    _2852 = IS_ATOM(_transformer_rids_5443);
    if (_2852 == 0)
    {
        _2852 = NOVALUE;
        goto L1; // [15] 25
    }
    else{
        _2852 = NOVALUE;
    }

    /** 		transformer_rids = {transformer_rids}*/
    _0 = _transformer_rids_5443;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_transformer_rids_5443);
    *((int *)(_2+4)) = _transformer_rids_5443;
    _transformer_rids_5443 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	for i = 1 to length(transformer_rids) do*/
    if (IS_SEQUENCE(_transformer_rids_5443)){
            _2854 = SEQ_PTR(_transformer_rids_5443)->length;
    }
    else {
        _2854 = 1;
    }
    {
        int _i_5449;
        _i_5449 = 1;
L2: 
        if (_i_5449 > _2854){
            goto L3; // [30] 112
        }

        /** 		if atom(transformer_rids[i]) then*/
        _2 = (int)SEQ_PTR(_transformer_rids_5443);
        _2855 = (int)*(((s1_ptr)_2)->base + _i_5449);
        _2856 = IS_ATOM(_2855);
        _2855 = NOVALUE;
        if (_2856 == 0)
        {
            _2856 = NOVALUE;
            goto L4; // [46] 68
        }
        else{
            _2856 = NOVALUE;
        }

        /** 			lResult = call_func(transformer_rids[i], {lResult})*/
        _2 = (int)SEQ_PTR(_transformer_rids_5443);
        _2857 = (int)*(((s1_ptr)_2)->base + _i_5449);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_lResult_5444);
        *((int *)(_2+4)) = _lResult_5444;
        _2858 = MAKE_SEQ(_1);
        _1 = (int)SEQ_PTR(_2858);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_2857].addr;
        Ref(*(int *)(_2+4));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4)
                             );
        DeRefDS(_lResult_5444);
        _lResult_5444 = _1;
        DeRefDS(_2858);
        _2858 = NOVALUE;
        goto L5; // [65] 105
L4: 

        /** 			lResult = call_func(transformer_rids[i][1], {lResult} & transformer_rids[i][2..$])*/
        _2 = (int)SEQ_PTR(_transformer_rids_5443);
        _2860 = (int)*(((s1_ptr)_2)->base + _i_5449);
        _2 = (int)SEQ_PTR(_2860);
        _2861 = (int)*(((s1_ptr)_2)->base + 1);
        _2860 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_lResult_5444);
        *((int *)(_2+4)) = _lResult_5444;
        _2862 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_transformer_rids_5443);
        _2863 = (int)*(((s1_ptr)_2)->base + _i_5449);
        if (IS_SEQUENCE(_2863)){
                _2864 = SEQ_PTR(_2863)->length;
        }
        else {
            _2864 = 1;
        }
        rhs_slice_target = (object_ptr)&_2865;
        RHS_Slice(_2863, 2, _2864);
        _2863 = NOVALUE;
        Concat((object_ptr)&_2866, _2862, _2865);
        DeRefDS(_2862);
        _2862 = NOVALUE;
        DeRef(_2862);
        _2862 = NOVALUE;
        DeRefDS(_2865);
        _2865 = NOVALUE;
        _1 = (int)SEQ_PTR(_2866);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_2861].addr;
        switch(((s1_ptr)_1)->length) {
            case 0:
                _1 = (*(int (*)())_0)(
                                     );
                break;
            case 1:
                Ref(*(int *)(_2+4));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4)
                                     );
                break;
            case 2:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8)
                                     );
                break;
            case 3:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12)
                                     );
                break;
            case 4:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16)
                                     );
                break;
            case 5:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20)
                                     );
                break;
            case 6:
                Ref(*(int *)(_2+4));
                Ref(*(int *)(_2+8));
                Ref(*(int *)(_2+12));
                Ref(*(int *)(_2+16));
                Ref(*(int *)(_2+20));
                Ref(*(int *)(_2+24));
                _1 = (*(int (*)())_0)(
                                    *(int *)(_2+4), 
                                    *(int *)(_2+8), 
                                    *(int *)(_2+12), 
                                    *(int *)(_2+16), 
                                    *(int *)(_2+20), 
                                    *(int *)(_2+24)
                                     );
                break;
        }
        DeRefDS(_lResult_5444);
        _lResult_5444 = _1;
        DeRefDS(_2866);
        _2866 = NOVALUE;
L5: 

        /** 	end for*/
        _i_5449 = _i_5449 + 1;
        goto L2; // [107] 37
L3: 
        ;
    }

    /** 	return lResult*/
    DeRefDS(_source_data_5442);
    DeRef(_transformer_rids_5443);
    _2857 = NOVALUE;
    _2861 = NOVALUE;
    return _lResult_5444;
    ;
}


int _21transmute(int _source_data_5468, int _current_items_5469, int _new_items_5470, int _start_5471, int _limit_5472)
{
    int _pos_5474 = NOVALUE;
    int _cs_5475 = NOVALUE;
    int _ns_5476 = NOVALUE;
    int _i_5477 = NOVALUE;
    int _elen_5478 = NOVALUE;
    int _2937 = NOVALUE;
    int _2936 = NOVALUE;
    int _2935 = NOVALUE;
    int _2933 = NOVALUE;
    int _2932 = NOVALUE;
    int _2930 = NOVALUE;
    int _2929 = NOVALUE;
    int _2928 = NOVALUE;
    int _2927 = NOVALUE;
    int _2926 = NOVALUE;
    int _2925 = NOVALUE;
    int _2924 = NOVALUE;
    int _2919 = NOVALUE;
    int _2917 = NOVALUE;
    int _2916 = NOVALUE;
    int _2915 = NOVALUE;
    int _2913 = NOVALUE;
    int _2912 = NOVALUE;
    int _2911 = NOVALUE;
    int _2910 = NOVALUE;
    int _2909 = NOVALUE;
    int _2908 = NOVALUE;
    int _2907 = NOVALUE;
    int _2902 = NOVALUE;
    int _2899 = NOVALUE;
    int _2898 = NOVALUE;
    int _2897 = NOVALUE;
    int _2895 = NOVALUE;
    int _2893 = NOVALUE;
    int _2888 = NOVALUE;
    int _2887 = NOVALUE;
    int _2885 = NOVALUE;
    int _2880 = NOVALUE;
    int _2875 = NOVALUE;
    int _2874 = NOVALUE;
    int _2873 = NOVALUE;
    int _2871 = NOVALUE;
    int _2870 = NOVALUE;
    int _2869 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_5471)) {
        _1 = (long)(DBL_PTR(_start_5471)->dbl);
        if (UNIQUE(DBL_PTR(_start_5471)) && (DBL_PTR(_start_5471)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_5471);
        _start_5471 = _1;
    }
    if (!IS_ATOM_INT(_limit_5472)) {
        _1 = (long)(DBL_PTR(_limit_5472)->dbl);
        if (UNIQUE(DBL_PTR(_limit_5472)) && (DBL_PTR(_limit_5472)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_limit_5472);
        _limit_5472 = _1;
    }

    /** 	if equal(current_items[1], {}) then*/
    _2 = (int)SEQ_PTR(_current_items_5469);
    _2869 = (int)*(((s1_ptr)_2)->base + 1);
    if (_2869 == _5)
    _2870 = 1;
    else if (IS_ATOM_INT(_2869) && IS_ATOM_INT(_5))
    _2870 = 0;
    else
    _2870 = (compare(_2869, _5) == 0);
    _2869 = NOVALUE;
    if (_2870 == 0)
    {
        _2870 = NOVALUE;
        goto L1; // [25] 48
    }
    else{
        _2870 = NOVALUE;
    }

    /** 		cs = 1*/
    _cs_5475 = 1;

    /** 		current_items = current_items[2 .. $]*/
    if (IS_SEQUENCE(_current_items_5469)){
            _2871 = SEQ_PTR(_current_items_5469)->length;
    }
    else {
        _2871 = 1;
    }
    rhs_slice_target = (object_ptr)&_current_items_5469;
    RHS_Slice(_current_items_5469, 2, _2871);
    goto L2; // [45] 56
L1: 

    /** 		cs = 0*/
    _cs_5475 = 0;
L2: 

    /** 	if equal(new_items[1], {}) then*/
    _2 = (int)SEQ_PTR(_new_items_5470);
    _2873 = (int)*(((s1_ptr)_2)->base + 1);
    if (_2873 == _5)
    _2874 = 1;
    else if (IS_ATOM_INT(_2873) && IS_ATOM_INT(_5))
    _2874 = 0;
    else
    _2874 = (compare(_2873, _5) == 0);
    _2873 = NOVALUE;
    if (_2874 == 0)
    {
        _2874 = NOVALUE;
        goto L3; // [66] 89
    }
    else{
        _2874 = NOVALUE;
    }

    /** 		ns = 1*/
    _ns_5476 = 1;

    /** 		new_items = new_items[2 .. $]*/
    if (IS_SEQUENCE(_new_items_5470)){
            _2875 = SEQ_PTR(_new_items_5470)->length;
    }
    else {
        _2875 = 1;
    }
    rhs_slice_target = (object_ptr)&_new_items_5470;
    RHS_Slice(_new_items_5470, 2, _2875);
    goto L4; // [86] 97
L3: 

    /** 		ns = 0*/
    _ns_5476 = 0;
L4: 

    /** 	i = start - 1*/
    _i_5477 = _start_5471 - 1;

    /** 	if cs = 0 then*/
    if (_cs_5475 != 0)
    goto L5; // [109] 297

    /** 		if ns = 0 then*/
    if (_ns_5476 != 0)
    goto L6; // [117] 197

    /** 			while i < length(source_data) do*/
L7: 
    if (IS_SEQUENCE(_source_data_5468)){
            _2880 = SEQ_PTR(_source_data_5468)->length;
    }
    else {
        _2880 = 1;
    }
    if (_i_5477 >= _2880)
    goto L8; // [129] 617

    /** 				if limit <= 0 then*/
    if (_limit_5472 > 0)
    goto L9; // [135] 144

    /** 					exit*/
    goto L8; // [141] 617
L9: 

    /** 				limit -= 1*/
    _limit_5472 = _limit_5472 - 1;

    /** 				i += 1*/
    _i_5477 = _i_5477 + 1;

    /** 				pos = find(source_data[i], current_items) */
    _2 = (int)SEQ_PTR(_source_data_5468);
    _2885 = (int)*(((s1_ptr)_2)->base + _i_5477);
    _pos_5474 = find_from(_2885, _current_items_5469, 1);
    _2885 = NOVALUE;

    /** 				if pos then*/
    if (_pos_5474 == 0)
    {
        goto L7; // [175] 126
    }
    else{
    }

    /** 					source_data[i] = new_items[pos]*/
    _2 = (int)SEQ_PTR(_new_items_5470);
    _2887 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    Ref(_2887);
    _2 = (int)SEQ_PTR(_source_data_5468);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _source_data_5468 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_5477);
    _1 = *(int *)_2;
    *(int *)_2 = _2887;
    if( _1 != _2887 ){
        DeRef(_1);
    }
    _2887 = NOVALUE;

    /** 			end while*/
    goto L7; // [191] 126
    goto L8; // [194] 617
L6: 

    /** 			while i < length(source_data) do*/
LA: 
    if (IS_SEQUENCE(_source_data_5468)){
            _2888 = SEQ_PTR(_source_data_5468)->length;
    }
    else {
        _2888 = 1;
    }
    if (_i_5477 >= _2888)
    goto L8; // [205] 617

    /** 				if limit <= 0 then*/
    if (_limit_5472 > 0)
    goto LB; // [211] 220

    /** 					exit*/
    goto L8; // [217] 617
LB: 

    /** 				limit -= 1*/
    _limit_5472 = _limit_5472 - 1;

    /** 				i += 1*/
    _i_5477 = _i_5477 + 1;

    /** 				pos = find(source_data[i], current_items) */
    _2 = (int)SEQ_PTR(_source_data_5468);
    _2893 = (int)*(((s1_ptr)_2)->base + _i_5477);
    _pos_5474 = find_from(_2893, _current_items_5469, 1);
    _2893 = NOVALUE;

    /** 				if pos then*/
    if (_pos_5474 == 0)
    {
        goto LA; // [251] 202
    }
    else{
    }

    /** 					source_data = replace(source_data, new_items[pos], i, i)*/
    _2 = (int)SEQ_PTR(_new_items_5470);
    _2895 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    {
        int p1 = _source_data_5468;
        int p2 = _2895;
        int p3 = _i_5477;
        int p4 = _i_5477;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_source_data_5468;
        Replace( &replace_params );
    }
    _2895 = NOVALUE;

    /** 					i += length(new_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_new_items_5470);
    _2897 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    if (IS_SEQUENCE(_2897)){
            _2898 = SEQ_PTR(_2897)->length;
    }
    else {
        _2898 = 1;
    }
    _2897 = NOVALUE;
    _2899 = _2898 - 1;
    _2898 = NOVALUE;
    _i_5477 = _i_5477 + _2899;
    _2899 = NOVALUE;

    /** 			end while*/
    goto LA; // [290] 202
    goto L8; // [294] 617
L5: 

    /** 		if ns = 0 then*/
    if (_ns_5476 != 0)
    goto LC; // [301] 453

    /** 			while i < length(source_data) do*/
LD: 
    if (IS_SEQUENCE(_source_data_5468)){
            _2902 = SEQ_PTR(_source_data_5468)->length;
    }
    else {
        _2902 = 1;
    }
    if (_i_5477 >= _2902)
    goto LE; // [313] 616

    /** 				if limit <= 0 then*/
    if (_limit_5472 > 0)
    goto LF; // [319] 328

    /** 					exit*/
    goto LE; // [325] 616
LF: 

    /** 				limit -= 1*/
    _limit_5472 = _limit_5472 - 1;

    /** 				i += 1*/
    _i_5477 = _i_5477 + 1;

    /** 				pos = 0*/
    _pos_5474 = 0;

    /** 				for j = 1 to length(current_items) do*/
    if (IS_SEQUENCE(_current_items_5469)){
            _2907 = SEQ_PTR(_current_items_5469)->length;
    }
    else {
        _2907 = 1;
    }
    {
        int _j_5535;
        _j_5535 = 1;
L10: 
        if (_j_5535 > _2907){
            goto L11; // [356] 404
        }

        /** 					if search:begins(current_items[j], source_data[i .. $]) then*/
        _2 = (int)SEQ_PTR(_current_items_5469);
        _2908 = (int)*(((s1_ptr)_2)->base + _j_5535);
        if (IS_SEQUENCE(_source_data_5468)){
                _2909 = SEQ_PTR(_source_data_5468)->length;
        }
        else {
            _2909 = 1;
        }
        rhs_slice_target = (object_ptr)&_2910;
        RHS_Slice(_source_data_5468, _i_5477, _2909);
        Ref(_2908);
        _2911 = _14begins(_2908, _2910);
        _2908 = NOVALUE;
        _2910 = NOVALUE;
        if (_2911 == 0) {
            DeRef(_2911);
            _2911 = NOVALUE;
            goto L12; // [382] 397
        }
        else {
            if (!IS_ATOM_INT(_2911) && DBL_PTR(_2911)->dbl == 0.0){
                DeRef(_2911);
                _2911 = NOVALUE;
                goto L12; // [382] 397
            }
            DeRef(_2911);
            _2911 = NOVALUE;
        }
        DeRef(_2911);
        _2911 = NOVALUE;

        /** 						pos = j*/
        _pos_5474 = _j_5535;

        /** 						exit*/
        goto L11; // [394] 404
L12: 

        /** 				end for*/
        _j_5535 = _j_5535 + 1;
        goto L10; // [399] 363
L11: 
        ;
    }

    /** 				if pos then*/
    if (_pos_5474 == 0)
    {
        goto LD; // [406] 310
    }
    else{
    }

    /** 			    	elen = length(current_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_current_items_5469);
    _2912 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    if (IS_SEQUENCE(_2912)){
            _2913 = SEQ_PTR(_2912)->length;
    }
    else {
        _2913 = 1;
    }
    _2912 = NOVALUE;
    _elen_5478 = _2913 - 1;
    _2913 = NOVALUE;

    /** 					source_data = replace(source_data, {new_items[pos]}, i, i + elen)*/
    _2 = (int)SEQ_PTR(_new_items_5470);
    _2915 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_2915);
    *((int *)(_2+4)) = _2915;
    _2916 = MAKE_SEQ(_1);
    _2915 = NOVALUE;
    _2917 = _i_5477 + _elen_5478;
    if ((long)((unsigned long)_2917 + (unsigned long)HIGH_BITS) >= 0) 
    _2917 = NewDouble((double)_2917);
    {
        int p1 = _source_data_5468;
        int p2 = _2916;
        int p3 = _i_5477;
        int p4 = _2917;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_source_data_5468;
        Replace( &replace_params );
    }
    DeRefDS(_2916);
    _2916 = NOVALUE;
    DeRef(_2917);
    _2917 = NOVALUE;

    /** 			end while*/
    goto LD; // [447] 310
    goto LE; // [450] 616
LC: 

    /** 			while i < length(source_data) do*/
L13: 
    if (IS_SEQUENCE(_source_data_5468)){
            _2919 = SEQ_PTR(_source_data_5468)->length;
    }
    else {
        _2919 = 1;
    }
    if (_i_5477 >= _2919)
    goto L14; // [461] 615

    /** 				if limit <= 0 then*/
    if (_limit_5472 > 0)
    goto L15; // [467] 476

    /** 					exit*/
    goto L14; // [473] 615
L15: 

    /** 				limit -= 1*/
    _limit_5472 = _limit_5472 - 1;

    /** 				i += 1*/
    _i_5477 = _i_5477 + 1;

    /** 				pos = 0*/
    _pos_5474 = 0;

    /** 				for j = 1 to length(current_items) do*/
    if (IS_SEQUENCE(_current_items_5469)){
            _2924 = SEQ_PTR(_current_items_5469)->length;
    }
    else {
        _2924 = 1;
    }
    {
        int _j_5559;
        _j_5559 = 1;
L16: 
        if (_j_5559 > _2924){
            goto L17; // [504] 552
        }

        /** 					if search:begins(current_items[j], source_data[i .. $]) then*/
        _2 = (int)SEQ_PTR(_current_items_5469);
        _2925 = (int)*(((s1_ptr)_2)->base + _j_5559);
        if (IS_SEQUENCE(_source_data_5468)){
                _2926 = SEQ_PTR(_source_data_5468)->length;
        }
        else {
            _2926 = 1;
        }
        rhs_slice_target = (object_ptr)&_2927;
        RHS_Slice(_source_data_5468, _i_5477, _2926);
        Ref(_2925);
        _2928 = _14begins(_2925, _2927);
        _2925 = NOVALUE;
        _2927 = NOVALUE;
        if (_2928 == 0) {
            DeRef(_2928);
            _2928 = NOVALUE;
            goto L18; // [530] 545
        }
        else {
            if (!IS_ATOM_INT(_2928) && DBL_PTR(_2928)->dbl == 0.0){
                DeRef(_2928);
                _2928 = NOVALUE;
                goto L18; // [530] 545
            }
            DeRef(_2928);
            _2928 = NOVALUE;
        }
        DeRef(_2928);
        _2928 = NOVALUE;

        /** 						pos = j*/
        _pos_5474 = _j_5559;

        /** 						exit*/
        goto L17; // [542] 552
L18: 

        /** 				end for*/
        _j_5559 = _j_5559 + 1;
        goto L16; // [547] 511
L17: 
        ;
    }

    /** 				if pos then*/
    if (_pos_5474 == 0)
    {
        goto L13; // [554] 458
    }
    else{
    }

    /** 			    	elen = length(current_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_current_items_5469);
    _2929 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    if (IS_SEQUENCE(_2929)){
            _2930 = SEQ_PTR(_2929)->length;
    }
    else {
        _2930 = 1;
    }
    _2929 = NOVALUE;
    _elen_5478 = _2930 - 1;
    _2930 = NOVALUE;

    /** 					source_data = replace(source_data, new_items[pos], i, i + elen)*/
    _2 = (int)SEQ_PTR(_new_items_5470);
    _2932 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    _2933 = _i_5477 + _elen_5478;
    if ((long)((unsigned long)_2933 + (unsigned long)HIGH_BITS) >= 0) 
    _2933 = NewDouble((double)_2933);
    {
        int p1 = _source_data_5468;
        int p2 = _2932;
        int p3 = _i_5477;
        int p4 = _2933;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_source_data_5468;
        Replace( &replace_params );
    }
    _2932 = NOVALUE;
    DeRef(_2933);
    _2933 = NOVALUE;

    /** 					i += length(new_items[pos]) - 1*/
    _2 = (int)SEQ_PTR(_new_items_5470);
    _2935 = (int)*(((s1_ptr)_2)->base + _pos_5474);
    if (IS_SEQUENCE(_2935)){
            _2936 = SEQ_PTR(_2935)->length;
    }
    else {
        _2936 = 1;
    }
    _2935 = NOVALUE;
    _2937 = _2936 - 1;
    _2936 = NOVALUE;
    _i_5477 = _i_5477 + _2937;
    _2937 = NOVALUE;

    /** 			end while*/
    goto L13; // [612] 458
L14: 
LE: 
L8: 

    /** 	return source_data*/
    DeRefDS(_current_items_5469);
    DeRefDS(_new_items_5470);
    _2897 = NOVALUE;
    _2912 = NOVALUE;
    _2929 = NOVALUE;
    _2935 = NOVALUE;
    return _source_data_5468;
    ;
}


int _21sim_index(int _A_5579, int _B_5580)
{
    int _accum_score_5581 = NOVALUE;
    int _pos_factor_5582 = NOVALUE;
    int _indx_a_5583 = NOVALUE;
    int _indx_b_5584 = NOVALUE;
    int _used_A_5585 = NOVALUE;
    int _used_B_5586 = NOVALUE;
    int _total_elems_5645 = NOVALUE;
    int _3007 = NOVALUE;
    int _3006 = NOVALUE;
    int _3005 = NOVALUE;
    int _3002 = NOVALUE;
    int _3001 = NOVALUE;
    int _3000 = NOVALUE;
    int _2999 = NOVALUE;
    int _2997 = NOVALUE;
    int _2996 = NOVALUE;
    int _2995 = NOVALUE;
    int _2994 = NOVALUE;
    int _2993 = NOVALUE;
    int _2991 = NOVALUE;
    int _2990 = NOVALUE;
    int _2987 = NOVALUE;
    int _2986 = NOVALUE;
    int _2985 = NOVALUE;
    int _2984 = NOVALUE;
    int _2983 = NOVALUE;
    int _2981 = NOVALUE;
    int _2980 = NOVALUE;
    int _2979 = NOVALUE;
    int _2978 = NOVALUE;
    int _2977 = NOVALUE;
    int _2975 = NOVALUE;
    int _2974 = NOVALUE;
    int _2968 = NOVALUE;
    int _2967 = NOVALUE;
    int _2966 = NOVALUE;
    int _2965 = NOVALUE;
    int _2964 = NOVALUE;
    int _2963 = NOVALUE;
    int _2962 = NOVALUE;
    int _2961 = NOVALUE;
    int _2959 = NOVALUE;
    int _2958 = NOVALUE;
    int _2957 = NOVALUE;
    int _2956 = NOVALUE;
    int _2955 = NOVALUE;
    int _2954 = NOVALUE;
    int _2952 = NOVALUE;
    int _2950 = NOVALUE;
    int _2949 = NOVALUE;
    int _2948 = NOVALUE;
    int _2947 = NOVALUE;
    int _2946 = NOVALUE;
    int _2943 = NOVALUE;
    int _2941 = NOVALUE;
    int _2939 = NOVALUE;
    int _0, _1, _2;
    

    /** 	accum_score = 0*/
    DeRef(_accum_score_5581);
    _accum_score_5581 = 0;

    /** 	indx_a = 1*/
    _indx_a_5583 = 1;

    /** 	used_A = repeat(0, length(A))*/
    if (IS_SEQUENCE(_A_5579)){
            _2939 = SEQ_PTR(_A_5579)->length;
    }
    else {
        _2939 = 1;
    }
    DeRefi(_used_A_5585);
    _used_A_5585 = Repeat(0, _2939);
    _2939 = NOVALUE;

    /** 	used_B = repeat(0, length(B))*/
    if (IS_SEQUENCE(_B_5580)){
            _2941 = SEQ_PTR(_B_5580)->length;
    }
    else {
        _2941 = 1;
    }
    DeRefi(_used_B_5586);
    _used_B_5586 = Repeat(0, _2941);
    _2941 = NOVALUE;

    /** 	while indx_a <= length(A) label "DoA" do*/
L1: 
    if (IS_SEQUENCE(_A_5579)){
            _2943 = SEQ_PTR(_A_5579)->length;
    }
    else {
        _2943 = 1;
    }
    if (_indx_a_5583 > _2943)
    goto L2; // [43] 244

    /** 		pos_factor = power((1 + length(A) - indx_a) / length(A),2)*/
    if (IS_SEQUENCE(_A_5579)){
            _2946 = SEQ_PTR(_A_5579)->length;
    }
    else {
        _2946 = 1;
    }
    _2947 = _2946 + 1;
    _2946 = NOVALUE;
    _2948 = _2947 - _indx_a_5583;
    if ((long)((unsigned long)_2948 +(unsigned long) HIGH_BITS) >= 0){
        _2948 = NewDouble((double)_2948);
    }
    _2947 = NOVALUE;
    if (IS_SEQUENCE(_A_5579)){
            _2949 = SEQ_PTR(_A_5579)->length;
    }
    else {
        _2949 = 1;
    }
    if (IS_ATOM_INT(_2948)) {
        _2950 = (_2948 % _2949) ? NewDouble((double)_2948 / _2949) : (_2948 / _2949);
    }
    else {
        _2950 = NewDouble(DBL_PTR(_2948)->dbl / (double)_2949);
    }
    DeRef(_2948);
    _2948 = NOVALUE;
    _2949 = NOVALUE;
    DeRef(_pos_factor_5582);
    if (IS_ATOM_INT(_2950) && IS_ATOM_INT(_2950)) {
        if (_2950 == (short)_2950 && _2950 <= INT15 && _2950 >= -INT15)
        _pos_factor_5582 = _2950 * _2950;
        else
        _pos_factor_5582 = NewDouble(_2950 * (double)_2950);
    }
    else {
        if (IS_ATOM_INT(_2950)) {
            _pos_factor_5582 = NewDouble((double)_2950 * DBL_PTR(_2950)->dbl);
        }
        else {
            if (IS_ATOM_INT(_2950)) {
                _pos_factor_5582 = NewDouble(DBL_PTR(_2950)->dbl * (double)_2950);
            }
            else
            _pos_factor_5582 = NewDouble(DBL_PTR(_2950)->dbl * DBL_PTR(_2950)->dbl);
        }
    }
    DeRef(_2950);
    _2950 = NOVALUE;
    _2950 = NOVALUE;

    /** 		indx_b = 1*/
    _indx_b_5584 = 1;

    /** 		while indx_b <= length(B) do*/
L3: 
    if (IS_SEQUENCE(_B_5580)){
            _2952 = SEQ_PTR(_B_5580)->length;
    }
    else {
        _2952 = 1;
    }
    if (_indx_b_5584 > _2952)
    goto L4; // [86] 231

    /** 			if equal(A[indx_a],B[indx_b]) then*/
    _2 = (int)SEQ_PTR(_A_5579);
    _2954 = (int)*(((s1_ptr)_2)->base + _indx_a_5583);
    _2 = (int)SEQ_PTR(_B_5580);
    _2955 = (int)*(((s1_ptr)_2)->base + _indx_b_5584);
    if (_2954 == _2955)
    _2956 = 1;
    else if (IS_ATOM_INT(_2954) && IS_ATOM_INT(_2955))
    _2956 = 0;
    else
    _2956 = (compare(_2954, _2955) == 0);
    _2954 = NOVALUE;
    _2955 = NOVALUE;
    if (_2956 == 0)
    {
        _2956 = NOVALUE;
        goto L5; // [104] 218
    }
    else{
        _2956 = NOVALUE;
    }

    /** 				accum_score += power((indx_b - indx_a) * pos_factor,2)*/
    _2957 = _indx_b_5584 - _indx_a_5583;
    if ((long)((unsigned long)_2957 +(unsigned long) HIGH_BITS) >= 0){
        _2957 = NewDouble((double)_2957);
    }
    if (IS_ATOM_INT(_2957) && IS_ATOM_INT(_pos_factor_5582)) {
        if (_2957 == (short)_2957 && _pos_factor_5582 <= INT15 && _pos_factor_5582 >= -INT15)
        _2958 = _2957 * _pos_factor_5582;
        else
        _2958 = NewDouble(_2957 * (double)_pos_factor_5582);
    }
    else {
        if (IS_ATOM_INT(_2957)) {
            _2958 = NewDouble((double)_2957 * DBL_PTR(_pos_factor_5582)->dbl);
        }
        else {
            if (IS_ATOM_INT(_pos_factor_5582)) {
                _2958 = NewDouble(DBL_PTR(_2957)->dbl * (double)_pos_factor_5582);
            }
            else
            _2958 = NewDouble(DBL_PTR(_2957)->dbl * DBL_PTR(_pos_factor_5582)->dbl);
        }
    }
    DeRef(_2957);
    _2957 = NOVALUE;
    if (IS_ATOM_INT(_2958) && IS_ATOM_INT(_2958)) {
        if (_2958 == (short)_2958 && _2958 <= INT15 && _2958 >= -INT15)
        _2959 = _2958 * _2958;
        else
        _2959 = NewDouble(_2958 * (double)_2958);
    }
    else {
        if (IS_ATOM_INT(_2958)) {
            _2959 = NewDouble((double)_2958 * DBL_PTR(_2958)->dbl);
        }
        else {
            if (IS_ATOM_INT(_2958)) {
                _2959 = NewDouble(DBL_PTR(_2958)->dbl * (double)_2958);
            }
            else
            _2959 = NewDouble(DBL_PTR(_2958)->dbl * DBL_PTR(_2958)->dbl);
        }
    }
    DeRef(_2958);
    _2958 = NOVALUE;
    _2958 = NOVALUE;
    _0 = _accum_score_5581;
    if (IS_ATOM_INT(_accum_score_5581) && IS_ATOM_INT(_2959)) {
        _accum_score_5581 = _accum_score_5581 + _2959;
        if ((long)((unsigned long)_accum_score_5581 + (unsigned long)HIGH_BITS) >= 0) 
        _accum_score_5581 = NewDouble((double)_accum_score_5581);
    }
    else {
        if (IS_ATOM_INT(_accum_score_5581)) {
            _accum_score_5581 = NewDouble((double)_accum_score_5581 + DBL_PTR(_2959)->dbl);
        }
        else {
            if (IS_ATOM_INT(_2959)) {
                _accum_score_5581 = NewDouble(DBL_PTR(_accum_score_5581)->dbl + (double)_2959);
            }
            else
            _accum_score_5581 = NewDouble(DBL_PTR(_accum_score_5581)->dbl + DBL_PTR(_2959)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_2959);
    _2959 = NOVALUE;

    /** 				while indx_a <= length(A) and indx_b <= length(B) with entry do*/
    goto L6; // [127] 180
L7: 
    if (IS_SEQUENCE(_A_5579)){
            _2961 = SEQ_PTR(_A_5579)->length;
    }
    else {
        _2961 = 1;
    }
    _2962 = (_indx_a_5583 <= _2961);
    _2961 = NOVALUE;
    if (_2962 == 0) {
        DeRef(_2963);
        _2963 = 0;
        goto L8; // [137] 152
    }
    if (IS_SEQUENCE(_B_5580)){
            _2964 = SEQ_PTR(_B_5580)->length;
    }
    else {
        _2964 = 1;
    }
    _2965 = (_indx_b_5584 <= _2964);
    _2964 = NOVALUE;
    _2963 = (_2965 != 0);
L8: 
    if (_2963 == 0)
    {
        _2963 = NOVALUE;
        goto L1; // [152] 40
    }
    else{
        _2963 = NOVALUE;
    }

    /** 					if not equal(A[indx_a], B[indx_b]) then*/
    _2 = (int)SEQ_PTR(_A_5579);
    _2966 = (int)*(((s1_ptr)_2)->base + _indx_a_5583);
    _2 = (int)SEQ_PTR(_B_5580);
    _2967 = (int)*(((s1_ptr)_2)->base + _indx_b_5584);
    if (_2966 == _2967)
    _2968 = 1;
    else if (IS_ATOM_INT(_2966) && IS_ATOM_INT(_2967))
    _2968 = 0;
    else
    _2968 = (compare(_2966, _2967) == 0);
    _2966 = NOVALUE;
    _2967 = NOVALUE;
    if (_2968 != 0)
    goto L9; // [169] 177
    _2968 = NOVALUE;

    /** 						exit*/
    goto L1; // [174] 40
L9: 

    /** 				entry*/
L6: 

    /** 					used_B[indx_b] = 1*/
    _2 = (int)SEQ_PTR(_used_B_5586);
    _2 = (int)(((s1_ptr)_2)->base + _indx_b_5584);
    *(int *)_2 = 1;

    /** 					used_A[indx_a] = 1*/
    _2 = (int)SEQ_PTR(_used_A_5585);
    _2 = (int)(((s1_ptr)_2)->base + _indx_a_5583);
    *(int *)_2 = 1;

    /** 					indx_a += 1*/
    _indx_a_5583 = _indx_a_5583 + 1;

    /** 					indx_b += 1*/
    _indx_b_5584 = _indx_b_5584 + 1;

    /** 				end while*/
    goto L7; // [210] 130

    /** 				continue "DoA"*/
    goto L1; // [215] 40
L5: 

    /** 			indx_b += 1*/
    _indx_b_5584 = _indx_b_5584 + 1;

    /** 		end while*/
    goto L3; // [228] 83
L4: 

    /** 		indx_a += 1*/
    _indx_a_5583 = _indx_a_5583 + 1;

    /** 	end while*/
    goto L1; // [241] 40
L2: 

    /**  	for i = 1 to length(A) do*/
    if (IS_SEQUENCE(_A_5579)){
            _2974 = SEQ_PTR(_A_5579)->length;
    }
    else {
        _2974 = 1;
    }
    {
        int _i_5628;
        _i_5628 = 1;
LA: 
        if (_i_5628 > _2974){
            goto LB; // [249] 323
        }

        /**  		if used_A[i] = 0 then*/
        _2 = (int)SEQ_PTR(_used_A_5585);
        _2975 = (int)*(((s1_ptr)_2)->base + _i_5628);
        if (_2975 != 0)
        goto LC; // [262] 316

        /** 			pos_factor = power((1 + length(A) - i) / length(A),2)*/
        if (IS_SEQUENCE(_A_5579)){
                _2977 = SEQ_PTR(_A_5579)->length;
        }
        else {
            _2977 = 1;
        }
        _2978 = _2977 + 1;
        _2977 = NOVALUE;
        _2979 = _2978 - _i_5628;
        _2978 = NOVALUE;
        if (IS_SEQUENCE(_A_5579)){
                _2980 = SEQ_PTR(_A_5579)->length;
        }
        else {
            _2980 = 1;
        }
        _2981 = (_2979 % _2980) ? NewDouble((double)_2979 / _2980) : (_2979 / _2980);
        _2979 = NOVALUE;
        _2980 = NOVALUE;
        DeRef(_pos_factor_5582);
        if (IS_ATOM_INT(_2981) && IS_ATOM_INT(_2981)) {
            if (_2981 == (short)_2981 && _2981 <= INT15 && _2981 >= -INT15)
            _pos_factor_5582 = _2981 * _2981;
            else
            _pos_factor_5582 = NewDouble(_2981 * (double)_2981);
        }
        else {
            if (IS_ATOM_INT(_2981)) {
                _pos_factor_5582 = NewDouble((double)_2981 * DBL_PTR(_2981)->dbl);
            }
            else {
                if (IS_ATOM_INT(_2981)) {
                    _pos_factor_5582 = NewDouble(DBL_PTR(_2981)->dbl * (double)_2981);
                }
                else
                _pos_factor_5582 = NewDouble(DBL_PTR(_2981)->dbl * DBL_PTR(_2981)->dbl);
            }
        }
        DeRef(_2981);
        _2981 = NOVALUE;
        _2981 = NOVALUE;

        /**  			accum_score += power((length(A) - i + 1) * pos_factor,2)*/
        if (IS_SEQUENCE(_A_5579)){
                _2983 = SEQ_PTR(_A_5579)->length;
        }
        else {
            _2983 = 1;
        }
        _2984 = _2983 - _i_5628;
        _2983 = NOVALUE;
        _2985 = _2984 + 1;
        _2984 = NOVALUE;
        if (IS_ATOM_INT(_pos_factor_5582)) {
            if (_2985 == (short)_2985 && _pos_factor_5582 <= INT15 && _pos_factor_5582 >= -INT15)
            _2986 = _2985 * _pos_factor_5582;
            else
            _2986 = NewDouble(_2985 * (double)_pos_factor_5582);
        }
        else {
            _2986 = NewDouble((double)_2985 * DBL_PTR(_pos_factor_5582)->dbl);
        }
        _2985 = NOVALUE;
        if (IS_ATOM_INT(_2986) && IS_ATOM_INT(_2986)) {
            if (_2986 == (short)_2986 && _2986 <= INT15 && _2986 >= -INT15)
            _2987 = _2986 * _2986;
            else
            _2987 = NewDouble(_2986 * (double)_2986);
        }
        else {
            if (IS_ATOM_INT(_2986)) {
                _2987 = NewDouble((double)_2986 * DBL_PTR(_2986)->dbl);
            }
            else {
                if (IS_ATOM_INT(_2986)) {
                    _2987 = NewDouble(DBL_PTR(_2986)->dbl * (double)_2986);
                }
                else
                _2987 = NewDouble(DBL_PTR(_2986)->dbl * DBL_PTR(_2986)->dbl);
            }
        }
        DeRef(_2986);
        _2986 = NOVALUE;
        _2986 = NOVALUE;
        _0 = _accum_score_5581;
        if (IS_ATOM_INT(_accum_score_5581) && IS_ATOM_INT(_2987)) {
            _accum_score_5581 = _accum_score_5581 + _2987;
            if ((long)((unsigned long)_accum_score_5581 + (unsigned long)HIGH_BITS) >= 0) 
            _accum_score_5581 = NewDouble((double)_accum_score_5581);
        }
        else {
            if (IS_ATOM_INT(_accum_score_5581)) {
                _accum_score_5581 = NewDouble((double)_accum_score_5581 + DBL_PTR(_2987)->dbl);
            }
            else {
                if (IS_ATOM_INT(_2987)) {
                    _accum_score_5581 = NewDouble(DBL_PTR(_accum_score_5581)->dbl + (double)_2987);
                }
                else
                _accum_score_5581 = NewDouble(DBL_PTR(_accum_score_5581)->dbl + DBL_PTR(_2987)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_2987);
        _2987 = NOVALUE;
LC: 

        /**  	end for*/
        _i_5628 = _i_5628 + 1;
        goto LA; // [318] 256
LB: 
        ;
    }

    /**  	integer total_elems = length(A)*/
    if (IS_SEQUENCE(_A_5579)){
            _total_elems_5645 = SEQ_PTR(_A_5579)->length;
    }
    else {
        _total_elems_5645 = 1;
    }

    /**  	for i = 1 to length(B) do*/
    if (IS_SEQUENCE(_B_5580)){
            _2990 = SEQ_PTR(_B_5580)->length;
    }
    else {
        _2990 = 1;
    }
    {
        int _i_5648;
        _i_5648 = 1;
LD: 
        if (_i_5648 > _2990){
            goto LE; // [335] 413
        }

        /**  		if used_B[i] = 0 then*/
        _2 = (int)SEQ_PTR(_used_B_5586);
        _2991 = (int)*(((s1_ptr)_2)->base + _i_5648);
        if (_2991 != 0)
        goto LF; // [348] 406

        /** 			pos_factor = power((1 + length(B) - i) / length(B),2)*/
        if (IS_SEQUENCE(_B_5580)){
                _2993 = SEQ_PTR(_B_5580)->length;
        }
        else {
            _2993 = 1;
        }
        _2994 = _2993 + 1;
        _2993 = NOVALUE;
        _2995 = _2994 - _i_5648;
        _2994 = NOVALUE;
        if (IS_SEQUENCE(_B_5580)){
                _2996 = SEQ_PTR(_B_5580)->length;
        }
        else {
            _2996 = 1;
        }
        _2997 = (_2995 % _2996) ? NewDouble((double)_2995 / _2996) : (_2995 / _2996);
        _2995 = NOVALUE;
        _2996 = NOVALUE;
        DeRef(_pos_factor_5582);
        if (IS_ATOM_INT(_2997) && IS_ATOM_INT(_2997)) {
            if (_2997 == (short)_2997 && _2997 <= INT15 && _2997 >= -INT15)
            _pos_factor_5582 = _2997 * _2997;
            else
            _pos_factor_5582 = NewDouble(_2997 * (double)_2997);
        }
        else {
            if (IS_ATOM_INT(_2997)) {
                _pos_factor_5582 = NewDouble((double)_2997 * DBL_PTR(_2997)->dbl);
            }
            else {
                if (IS_ATOM_INT(_2997)) {
                    _pos_factor_5582 = NewDouble(DBL_PTR(_2997)->dbl * (double)_2997);
                }
                else
                _pos_factor_5582 = NewDouble(DBL_PTR(_2997)->dbl * DBL_PTR(_2997)->dbl);
            }
        }
        DeRef(_2997);
        _2997 = NOVALUE;
        _2997 = NOVALUE;

        /**  			accum_score += (length(B) - i + 1) * pos_factor*/
        if (IS_SEQUENCE(_B_5580)){
                _2999 = SEQ_PTR(_B_5580)->length;
        }
        else {
            _2999 = 1;
        }
        _3000 = _2999 - _i_5648;
        _2999 = NOVALUE;
        _3001 = _3000 + 1;
        _3000 = NOVALUE;
        if (IS_ATOM_INT(_pos_factor_5582)) {
            if (_3001 == (short)_3001 && _pos_factor_5582 <= INT15 && _pos_factor_5582 >= -INT15)
            _3002 = _3001 * _pos_factor_5582;
            else
            _3002 = NewDouble(_3001 * (double)_pos_factor_5582);
        }
        else {
            _3002 = NewDouble((double)_3001 * DBL_PTR(_pos_factor_5582)->dbl);
        }
        _3001 = NOVALUE;
        _0 = _accum_score_5581;
        if (IS_ATOM_INT(_accum_score_5581) && IS_ATOM_INT(_3002)) {
            _accum_score_5581 = _accum_score_5581 + _3002;
            if ((long)((unsigned long)_accum_score_5581 + (unsigned long)HIGH_BITS) >= 0) 
            _accum_score_5581 = NewDouble((double)_accum_score_5581);
        }
        else {
            if (IS_ATOM_INT(_accum_score_5581)) {
                _accum_score_5581 = NewDouble((double)_accum_score_5581 + DBL_PTR(_3002)->dbl);
            }
            else {
                if (IS_ATOM_INT(_3002)) {
                    _accum_score_5581 = NewDouble(DBL_PTR(_accum_score_5581)->dbl + (double)_3002);
                }
                else
                _accum_score_5581 = NewDouble(DBL_PTR(_accum_score_5581)->dbl + DBL_PTR(_3002)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_3002);
        _3002 = NOVALUE;

        /**  			total_elems += 1*/
        _total_elems_5645 = _total_elems_5645 + 1;
LF: 

        /**  	end for*/
        _i_5648 = _i_5648 + 1;
        goto LD; // [408] 342
LE: 
        ;
    }

    /** 	return power(accum_score / power(total_elems,2), 0.5)*/
    if (_total_elems_5645 == (short)_total_elems_5645 && _total_elems_5645 <= INT15 && _total_elems_5645 >= -INT15)
    _3005 = _total_elems_5645 * _total_elems_5645;
    else
    _3005 = NewDouble(_total_elems_5645 * (double)_total_elems_5645);
    if (IS_ATOM_INT(_accum_score_5581) && IS_ATOM_INT(_3005)) {
        _3006 = (_accum_score_5581 % _3005) ? NewDouble((double)_accum_score_5581 / _3005) : (_accum_score_5581 / _3005);
    }
    else {
        if (IS_ATOM_INT(_accum_score_5581)) {
            _3006 = NewDouble((double)_accum_score_5581 / DBL_PTR(_3005)->dbl);
        }
        else {
            if (IS_ATOM_INT(_3005)) {
                _3006 = NewDouble(DBL_PTR(_accum_score_5581)->dbl / (double)_3005);
            }
            else
            _3006 = NewDouble(DBL_PTR(_accum_score_5581)->dbl / DBL_PTR(_3005)->dbl);
        }
    }
    DeRef(_3005);
    _3005 = NOVALUE;
    if (IS_ATOM_INT(_3006)) {
        temp_d.dbl = (double)_3006;
        _3007 = Dpower(&temp_d, DBL_PTR(_1715));
    }
    else {
        _3007 = Dpower(DBL_PTR(_3006), DBL_PTR(_1715));
    }
    DeRef(_3006);
    _3006 = NOVALUE;
    DeRefDS(_A_5579);
    DeRefDS(_B_5580);
    DeRef(_accum_score_5581);
    DeRef(_pos_factor_5582);
    DeRefi(_used_A_5585);
    DeRefi(_used_B_5586);
    DeRef(_2962);
    _2962 = NOVALUE;
    DeRef(_2965);
    _2965 = NOVALUE;
    _2975 = NOVALUE;
    _2991 = NOVALUE;
    return _3007;
    ;
}


int _21remove_subseq(int _source_list_5674, int _alt_value_5675)
{
    int _lResult_5676 = NOVALUE;
    int _lCOW_5677 = NOVALUE;
    int _3024 = NOVALUE;
    int _3023 = NOVALUE;
    int _3019 = NOVALUE;
    int _3016 = NOVALUE;
    int _3013 = NOVALUE;
    int _3012 = NOVALUE;
    int _3011 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer lCOW = 0*/
    _lCOW_5677 = 0;

    /** 	for i = 1 to length(source_list) do*/
    if (IS_SEQUENCE(_source_list_5674)){
            _3011 = SEQ_PTR(_source_list_5674)->length;
    }
    else {
        _3011 = 1;
    }
    {
        int _i_5679;
        _i_5679 = 1;
L1: 
        if (_i_5679 > _3011){
            goto L2; // [15] 129
        }

        /** 		if atom(source_list[i]) then*/
        _2 = (int)SEQ_PTR(_source_list_5674);
        _3012 = (int)*(((s1_ptr)_2)->base + _i_5679);
        _3013 = IS_ATOM(_3012);
        _3012 = NOVALUE;
        if (_3013 == 0)
        {
            _3013 = NOVALUE;
            goto L3; // [31] 73
        }
        else{
            _3013 = NOVALUE;
        }

        /** 			if lCOW != 0 then*/
        if (_lCOW_5677 == 0)
        goto L4; // [36] 124

        /** 				if lCOW != i then*/
        if (_lCOW_5677 == _i_5679)
        goto L5; // [42] 59

        /** 					lResult[lCOW] = source_list[i]*/
        _2 = (int)SEQ_PTR(_source_list_5674);
        _3016 = (int)*(((s1_ptr)_2)->base + _i_5679);
        Ref(_3016);
        _2 = (int)SEQ_PTR(_lResult_5676);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_5676 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_5677);
        _1 = *(int *)_2;
        *(int *)_2 = _3016;
        if( _1 != _3016 ){
            DeRef(_1);
        }
        _3016 = NOVALUE;
L5: 

        /** 				lCOW += 1*/
        _lCOW_5677 = _lCOW_5677 + 1;

        /** 			continue*/
        goto L4; // [70] 124
L3: 

        /** 		if lCOW = 0 then*/
        if (_lCOW_5677 != 0)
        goto L6; // [75] 94

        /** 			lResult = source_list*/
        RefDS(_source_list_5674);
        DeRef(_lResult_5676);
        _lResult_5676 = _source_list_5674;

        /** 			lCOW = i*/
        _lCOW_5677 = _i_5679;
L6: 

        /** 		if not equal(alt_value, SEQ_NOALT) then*/
        if (_alt_value_5675 == _21SEQ_NOALT_5668)
        _3019 = 1;
        else if (IS_ATOM_INT(_alt_value_5675) && IS_ATOM_INT(_21SEQ_NOALT_5668))
        _3019 = 0;
        else
        _3019 = (compare(_alt_value_5675, _21SEQ_NOALT_5668) == 0);
        if (_3019 != 0)
        goto L7; // [102] 122
        _3019 = NOVALUE;

        /** 			lResult[lCOW] = alt_value*/
        Ref(_alt_value_5675);
        _2 = (int)SEQ_PTR(_lResult_5676);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _lResult_5676 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lCOW_5677);
        _1 = *(int *)_2;
        *(int *)_2 = _alt_value_5675;
        DeRef(_1);

        /** 			lCOW += 1*/
        _lCOW_5677 = _lCOW_5677 + 1;
L7: 

        /** 	end for*/
L4: 
        _i_5679 = _i_5679 + 1;
        goto L1; // [124] 22
L2: 
        ;
    }

    /** 	if lCOW = 0 then*/
    if (_lCOW_5677 != 0)
    goto L8; // [131] 142

    /** 		return source_list*/
    DeRef(_alt_value_5675);
    DeRef(_lResult_5676);
    return _source_list_5674;
L8: 

    /** 	return lResult[1.. lCOW - 1]*/
    _3023 = _lCOW_5677 - 1;
    rhs_slice_target = (object_ptr)&_3024;
    RHS_Slice(_lResult_5676, 1, _3023);
    DeRefDS(_source_list_5674);
    DeRef(_alt_value_5675);
    DeRefDS(_lResult_5676);
    _3023 = NOVALUE;
    return _3024;
    ;
}


int _21remove_dups(int _source_data_5707, int _proc_option_5708)
{
    int _lTo_5709 = NOVALUE;
    int _lFrom_5710 = NOVALUE;
    int _lResult_5733 = NOVALUE;
    int _3047 = NOVALUE;
    int _3045 = NOVALUE;
    int _3044 = NOVALUE;
    int _3043 = NOVALUE;
    int _3042 = NOVALUE;
    int _3040 = NOVALUE;
    int _3036 = NOVALUE;
    int _3035 = NOVALUE;
    int _3034 = NOVALUE;
    int _3032 = NOVALUE;
    int _3027 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_option_5708)) {
        _1 = (long)(DBL_PTR(_proc_option_5708)->dbl);
        if (UNIQUE(DBL_PTR(_proc_option_5708)) && (DBL_PTR(_proc_option_5708)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_option_5708);
        _proc_option_5708 = _1;
    }

    /** 	if length(source_data) < 2 then*/
    if (IS_SEQUENCE(_source_data_5707)){
            _3027 = SEQ_PTR(_source_data_5707)->length;
    }
    else {
        _3027 = 1;
    }
    if (_3027 >= 2)
    goto L1; // [12] 23

    /** 		return source_data*/
    DeRef(_lResult_5733);
    return _source_data_5707;
L1: 

    /** 	if proc_option = RD_SORT then*/
    if (_proc_option_5708 != 3)
    goto L2; // [27] 52

    /** 		source_data = stdsort:sort(source_data)*/
    RefDS(_source_data_5707);
    _0 = _source_data_5707;
    _source_data_5707 = _22sort(_source_data_5707, 1);
    DeRefDS(_0);

    /** 		proc_option = RD_PRESORTED*/
    _proc_option_5708 = 2;
L2: 

    /** 	if proc_option = RD_PRESORTED then*/
    if (_proc_option_5708 != 2)
    goto L3; // [56] 154

    /** 		lTo = 1*/
    _lTo_5709 = 1;

    /** 		lFrom = 2*/
    _lFrom_5710 = 2;

    /** 		while lFrom <= length(source_data) do*/
L4: 
    if (IS_SEQUENCE(_source_data_5707)){
            _3032 = SEQ_PTR(_source_data_5707)->length;
    }
    else {
        _3032 = 1;
    }
    if (_lFrom_5710 > _3032)
    goto L5; // [82] 142

    /** 			if not equal(source_data[lFrom], source_data[lTo]) then*/
    _2 = (int)SEQ_PTR(_source_data_5707);
    _3034 = (int)*(((s1_ptr)_2)->base + _lFrom_5710);
    _2 = (int)SEQ_PTR(_source_data_5707);
    _3035 = (int)*(((s1_ptr)_2)->base + _lTo_5709);
    if (_3034 == _3035)
    _3036 = 1;
    else if (IS_ATOM_INT(_3034) && IS_ATOM_INT(_3035))
    _3036 = 0;
    else
    _3036 = (compare(_3034, _3035) == 0);
    _3034 = NOVALUE;
    _3035 = NOVALUE;
    if (_3036 != 0)
    goto L6; // [100] 129
    _3036 = NOVALUE;

    /** 				lTo += 1*/
    _lTo_5709 = _lTo_5709 + 1;

    /** 				if lTo != lFrom then*/
    if (_lTo_5709 == _lFrom_5710)
    goto L7; // [113] 128

    /** 					source_data[lTo] = source_data[lFrom]*/
    _2 = (int)SEQ_PTR(_source_data_5707);
    _3040 = (int)*(((s1_ptr)_2)->base + _lFrom_5710);
    Ref(_3040);
    _2 = (int)SEQ_PTR(_source_data_5707);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _source_data_5707 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _lTo_5709);
    _1 = *(int *)_2;
    *(int *)_2 = _3040;
    if( _1 != _3040 ){
        DeRef(_1);
    }
    _3040 = NOVALUE;
L7: 
L6: 

    /** 			lFrom += 1*/
    _lFrom_5710 = _lFrom_5710 + 1;

    /** 		end while*/
    goto L4; // [139] 79
L5: 

    /** 		return source_data[1 .. lTo]*/
    rhs_slice_target = (object_ptr)&_3042;
    RHS_Slice(_source_data_5707, 1, _lTo_5709);
    DeRefDS(_source_data_5707);
    DeRef(_lResult_5733);
    return _3042;
L3: 

    /** 	sequence lResult*/

    /** 	lResult = {}*/
    RefDS(_5);
    DeRef(_lResult_5733);
    _lResult_5733 = _5;

    /** 	for i = 1 to length(source_data) do*/
    if (IS_SEQUENCE(_source_data_5707)){
            _3043 = SEQ_PTR(_source_data_5707)->length;
    }
    else {
        _3043 = 1;
    }
    {
        int _i_5735;
        _i_5735 = 1;
L8: 
        if (_i_5735 > _3043){
            goto L9; // [168] 207
        }

        /** 		if not find(source_data[i], lResult) then*/
        _2 = (int)SEQ_PTR(_source_data_5707);
        _3044 = (int)*(((s1_ptr)_2)->base + _i_5735);
        _3045 = find_from(_3044, _lResult_5733, 1);
        _3044 = NOVALUE;
        if (_3045 != 0)
        goto LA; // [186] 200
        _3045 = NOVALUE;

        /** 			lResult = append(lResult, source_data[i])*/
        _2 = (int)SEQ_PTR(_source_data_5707);
        _3047 = (int)*(((s1_ptr)_2)->base + _i_5735);
        Ref(_3047);
        Append(&_lResult_5733, _lResult_5733, _3047);
        _3047 = NOVALUE;
LA: 

        /** 	end for*/
        _i_5735 = _i_5735 + 1;
        goto L8; // [202] 175
L9: 
        ;
    }

    /** 	return lResult*/
    DeRefDS(_source_data_5707);
    DeRef(_3042);
    _3042 = NOVALUE;
    return _lResult_5733;
    ;
}


int _21combine(int _source_data_5748, int _proc_option_5749)
{
    int _lResult_5750 = NOVALUE;
    int _lTotalSize_5751 = NOVALUE;
    int _lPos_5752 = NOVALUE;
    int _3070 = NOVALUE;
    int _3067 = NOVALUE;
    int _3066 = NOVALUE;
    int _3065 = NOVALUE;
    int _3064 = NOVALUE;
    int _3063 = NOVALUE;
    int _3062 = NOVALUE;
    int _3061 = NOVALUE;
    int _3060 = NOVALUE;
    int _3057 = NOVALUE;
    int _3056 = NOVALUE;
    int _3055 = NOVALUE;
    int _3054 = NOVALUE;
    int _3052 = NOVALUE;
    int _3050 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_proc_option_5749)) {
        _1 = (long)(DBL_PTR(_proc_option_5749)->dbl);
        if (UNIQUE(DBL_PTR(_proc_option_5749)) && (DBL_PTR(_proc_option_5749)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_proc_option_5749);
        _proc_option_5749 = _1;
    }

    /** 	integer lTotalSize = 0*/
    _lTotalSize_5751 = 0;

    /** 	if length(source_data) = 0 then*/
    if (IS_SEQUENCE(_source_data_5748)){
            _3050 = SEQ_PTR(_source_data_5748)->length;
    }
    else {
        _3050 = 1;
    }
    if (_3050 != 0)
    goto L1; // [19] 30

    /** 		return {}*/
    RefDS(_5);
    DeRefDS(_source_data_5748);
    DeRef(_lResult_5750);
    return _5;
L1: 

    /** 	if length(source_data) = 1 then*/
    if (IS_SEQUENCE(_source_data_5748)){
            _3052 = SEQ_PTR(_source_data_5748)->length;
    }
    else {
        _3052 = 1;
    }
    if (_3052 != 1)
    goto L2; // [35] 50

    /** 		return source_data[1]*/
    _2 = (int)SEQ_PTR(_source_data_5748);
    _3054 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_3054);
    DeRefDS(_source_data_5748);
    DeRef(_lResult_5750);
    return _3054;
L2: 

    /** 	for i = 1 to length(source_data) do*/
    if (IS_SEQUENCE(_source_data_5748)){
            _3055 = SEQ_PTR(_source_data_5748)->length;
    }
    else {
        _3055 = 1;
    }
    {
        int _i_5761;
        _i_5761 = 1;
L3: 
        if (_i_5761 > _3055){
            goto L4; // [55] 84
        }

        /** 		lTotalSize += length(source_data[i])*/
        _2 = (int)SEQ_PTR(_source_data_5748);
        _3056 = (int)*(((s1_ptr)_2)->base + _i_5761);
        if (IS_SEQUENCE(_3056)){
                _3057 = SEQ_PTR(_3056)->length;
        }
        else {
            _3057 = 1;
        }
        _3056 = NOVALUE;
        _lTotalSize_5751 = _lTotalSize_5751 + _3057;
        _3057 = NOVALUE;

        /** 	end for*/
        _i_5761 = _i_5761 + 1;
        goto L3; // [79] 62
L4: 
        ;
    }

    /** 	lResult = repeat(0, lTotalSize)*/
    DeRef(_lResult_5750);
    _lResult_5750 = Repeat(0, _lTotalSize_5751);

    /** 	lPos = 1*/
    _lPos_5752 = 1;

    /** 	for i = 1 to length(source_data) do*/
    if (IS_SEQUENCE(_source_data_5748)){
            _3060 = SEQ_PTR(_source_data_5748)->length;
    }
    else {
        _3060 = 1;
    }
    {
        int _i_5768;
        _i_5768 = 1;
L5: 
        if (_i_5768 > _3060){
            goto L6; // [102] 157
        }

        /** 		lResult[lPos .. length(source_data[i]) + lPos - 1] = source_data[i]*/
        _2 = (int)SEQ_PTR(_source_data_5748);
        _3061 = (int)*(((s1_ptr)_2)->base + _i_5768);
        if (IS_SEQUENCE(_3061)){
                _3062 = SEQ_PTR(_3061)->length;
        }
        else {
            _3062 = 1;
        }
        _3061 = NOVALUE;
        _3063 = _3062 + _lPos_5752;
        if ((long)((unsigned long)_3063 + (unsigned long)HIGH_BITS) >= 0) 
        _3063 = NewDouble((double)_3063);
        _3062 = NOVALUE;
        if (IS_ATOM_INT(_3063)) {
            _3064 = _3063 - 1;
            if ((long)((unsigned long)_3064 +(unsigned long) HIGH_BITS) >= 0){
                _3064 = NewDouble((double)_3064);
            }
        }
        else {
            _3064 = NewDouble(DBL_PTR(_3063)->dbl - (double)1);
        }
        DeRef(_3063);
        _3063 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_data_5748);
        _3065 = (int)*(((s1_ptr)_2)->base + _i_5768);
        assign_slice_seq = (s1_ptr *)&_lResult_5750;
        AssignSlice(_lPos_5752, _3064, _3065);
        DeRef(_3064);
        _3064 = NOVALUE;
        _3065 = NOVALUE;

        /** 		lPos += length(source_data[i])*/
        _2 = (int)SEQ_PTR(_source_data_5748);
        _3066 = (int)*(((s1_ptr)_2)->base + _i_5768);
        if (IS_SEQUENCE(_3066)){
                _3067 = SEQ_PTR(_3066)->length;
        }
        else {
            _3067 = 1;
        }
        _3066 = NOVALUE;
        _lPos_5752 = _lPos_5752 + _3067;
        _3067 = NOVALUE;

        /** 	end for*/
        _i_5768 = _i_5768 + 1;
        goto L5; // [152] 109
L6: 
        ;
    }

    /** 	if proc_option = COMBINE_SORTED then*/
    if (_proc_option_5749 != 1)
    goto L7; // [161] 179

    /** 		return stdsort:sort(lResult)*/
    RefDS(_lResult_5750);
    _3070 = _22sort(_lResult_5750, 1);
    DeRefDS(_source_data_5748);
    DeRefDS(_lResult_5750);
    _3054 = NOVALUE;
    _3056 = NOVALUE;
    _3061 = NOVALUE;
    _3066 = NOVALUE;
    return _3070;
    goto L8; // [176] 186
L7: 

    /** 		return lResult*/
    DeRefDS(_source_data_5748);
    _3054 = NOVALUE;
    _3056 = NOVALUE;
    _3061 = NOVALUE;
    _3066 = NOVALUE;
    DeRef(_3070);
    _3070 = NOVALUE;
    return _lResult_5750;
L8: 
    ;
}


int _21minsize(int _source_data_5784, int _min_size_5785, int _new_data_5790)
{
    int _3079 = NOVALUE;
    int _3078 = NOVALUE;
    int _3077 = NOVALUE;
    int _3075 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_min_size_5785)) {
        _1 = (long)(DBL_PTR(_min_size_5785)->dbl);
        if (UNIQUE(DBL_PTR(_min_size_5785)) && (DBL_PTR(_min_size_5785)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_min_size_5785);
        _min_size_5785 = _1;
    }

    /**     if length(source_data) < min_size then*/
    if (IS_SEQUENCE(_source_data_5784)){
            _3075 = SEQ_PTR(_source_data_5784)->length;
    }
    else {
        _3075 = 1;
    }
    if (_3075 >= _min_size_5785)
    goto L1; // [10] 32

    /**         source_data &= repeat(new_data, min_size - length(source_data))*/
    if (IS_SEQUENCE(_source_data_5784)){
            _3077 = SEQ_PTR(_source_data_5784)->length;
    }
    else {
        _3077 = 1;
    }
    _3078 = _min_size_5785 - _3077;
    _3077 = NOVALUE;
    _3079 = Repeat(_new_data_5790, _3078);
    _3078 = NOVALUE;
    if (IS_SEQUENCE(_source_data_5784) && IS_ATOM(_3079)) {
    }
    else if (IS_ATOM(_source_data_5784) && IS_SEQUENCE(_3079)) {
        Ref(_source_data_5784);
        Prepend(&_source_data_5784, _3079, _source_data_5784);
    }
    else {
        Concat((object_ptr)&_source_data_5784, _source_data_5784, _3079);
    }
    DeRefDS(_3079);
    _3079 = NOVALUE;
L1: 

    /**     return source_data*/
    DeRef(_new_data_5790);
    return _source_data_5784;
    ;
}



// 0x0BC4F721
