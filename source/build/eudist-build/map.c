// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _26map(int _obj_p_11730)
{
    int _m__11734 = NOVALUE;
    int _6676 = NOVALUE;
    int _6675 = NOVALUE;
    int _6674 = NOVALUE;
    int _6673 = NOVALUE;
    int _6672 = NOVALUE;
    int _6671 = NOVALUE;
    int _6670 = NOVALUE;
    int _6669 = NOVALUE;
    int _6668 = NOVALUE;
    int _6667 = NOVALUE;
    int _6665 = NOVALUE;
    int _6664 = NOVALUE;
    int _6663 = NOVALUE;
    int _6662 = NOVALUE;
    int _6660 = NOVALUE;
    int _6659 = NOVALUE;
    int _6658 = NOVALUE;
    int _6657 = NOVALUE;
    int _6655 = NOVALUE;
    int _6654 = NOVALUE;
    int _6653 = NOVALUE;
    int _6652 = NOVALUE;
    int _6651 = NOVALUE;
    int _6650 = NOVALUE;
    int _6649 = NOVALUE;
    int _6648 = NOVALUE;
    int _6647 = NOVALUE;
    int _6646 = NOVALUE;
    int _6644 = NOVALUE;
    int _6642 = NOVALUE;
    int _6641 = NOVALUE;
    int _6639 = NOVALUE;
    int _6637 = NOVALUE;
    int _6636 = NOVALUE;
    int _6634 = NOVALUE;
    int _6633 = NOVALUE;
    int _6631 = NOVALUE;
    int _6629 = NOVALUE;
    int _6627 = NOVALUE;
    int _6624 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not eumem:valid(obj_p, "") then return 0 end if*/
    Ref(_obj_p_11730);
    RefDS(_5);
    _6624 = _27valid(_obj_p_11730, _5);
    if (IS_ATOM_INT(_6624)) {
        if (_6624 != 0){
            DeRef(_6624);
            _6624 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    else {
        if (DBL_PTR(_6624)->dbl != 0.0){
            DeRef(_6624);
            _6624 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    DeRef(_6624);
    _6624 = NOVALUE;
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L1: 

    /** 	object m_*/

    /** 	m_ = eumem:ram_space[obj_p]*/
    DeRef(_m__11734);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_obj_p_11730)){
        _m__11734 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_obj_p_11730)->dbl));
    }
    else{
        _m__11734 = (int)*(((s1_ptr)_2)->base + _obj_p_11730);
    }
    Ref(_m__11734);

    /** 	if not sequence(m_) then return 0 end if*/
    _6627 = IS_SEQUENCE(_m__11734);
    if (_6627 != 0)
    goto L2; // [31] 39
    _6627 = NOVALUE;
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L2: 

    /** 	if length(m_) < 6 then return 0 end if*/
    if (IS_SEQUENCE(_m__11734)){
            _6629 = SEQ_PTR(_m__11734)->length;
    }
    else {
        _6629 = 1;
    }
    if (_6629 >= 6)
    goto L3; // [44] 53
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L3: 

    /** 	if length(m_) > 7 then return 0 end if*/
    if (IS_SEQUENCE(_m__11734)){
            _6631 = SEQ_PTR(_m__11734)->length;
    }
    else {
        _6631 = 1;
    }
    if (_6631 <= 7)
    goto L4; // [58] 67
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L4: 

    /** 	if not equal(m_[TYPE_TAG], type_is_map) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6633 = (int)*(((s1_ptr)_2)->base + 1);
    if (_6633 == _26type_is_map_11702)
    _6634 = 1;
    else if (IS_ATOM_INT(_6633) && IS_ATOM_INT(_26type_is_map_11702))
    _6634 = 0;
    else
    _6634 = (compare(_6633, _26type_is_map_11702) == 0);
    _6633 = NOVALUE;
    if (_6634 != 0)
    goto L5; // [79] 87
    _6634 = NOVALUE;
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L5: 

    /** 	if not integer(m_[ELEMENT_COUNT]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6636 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6636))
    _6637 = 1;
    else if (IS_ATOM_DBL(_6636))
    _6637 = IS_ATOM_INT(DoubleToInt(_6636));
    else
    _6637 = 0;
    _6636 = NOVALUE;
    if (_6637 != 0)
    goto L6; // [98] 106
    _6637 = NOVALUE;
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L6: 

    /** 	if m_[ELEMENT_COUNT] < 0 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6639 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(GREATEREQ, _6639, 0)){
        _6639 = NOVALUE;
        goto L7; // [114] 123
    }
    _6639 = NOVALUE;
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L7: 

    /** 	if not integer(m_[IN_USE]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6641 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_6641))
    _6642 = 1;
    else if (IS_ATOM_DBL(_6641))
    _6642 = IS_ATOM_INT(DoubleToInt(_6641));
    else
    _6642 = 0;
    _6641 = NOVALUE;
    if (_6642 != 0)
    goto L8; // [134] 142
    _6642 = NOVALUE;
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L8: 

    /** 	if m_[IN_USE] < 0		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6644 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(GREATEREQ, _6644, 0)){
        _6644 = NOVALUE;
        goto L9; // [150] 159
    }
    _6644 = NOVALUE;
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
L9: 

    /** 	if equal(m_[MAP_TYPE],SMALLMAP) then*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6646 = (int)*(((s1_ptr)_2)->base + 4);
    if (_6646 == 115)
    _6647 = 1;
    else if (IS_ATOM_INT(_6646) && IS_ATOM_INT(115))
    _6647 = 0;
    else
    _6647 = (compare(_6646, 115) == 0);
    _6646 = NOVALUE;
    if (_6647 == 0)
    {
        _6647 = NOVALUE;
        goto LA; // [171] 312
    }
    else{
        _6647 = NOVALUE;
    }

    /** 		if atom(m_[KEY_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6648 = (int)*(((s1_ptr)_2)->base + 5);
    _6649 = IS_ATOM(_6648);
    _6648 = NOVALUE;
    if (_6649 == 0)
    {
        _6649 = NOVALUE;
        goto LB; // [185] 193
    }
    else{
        _6649 = NOVALUE;
    }
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
LB: 

    /** 		if atom(m_[VALUE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6650 = (int)*(((s1_ptr)_2)->base + 6);
    _6651 = IS_ATOM(_6650);
    _6650 = NOVALUE;
    if (_6651 == 0)
    {
        _6651 = NOVALUE;
        goto LC; // [204] 212
    }
    else{
        _6651 = NOVALUE;
    }
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
LC: 

    /** 		if atom(m_[FREE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6652 = (int)*(((s1_ptr)_2)->base + 7);
    _6653 = IS_ATOM(_6652);
    _6652 = NOVALUE;
    if (_6653 == 0)
    {
        _6653 = NOVALUE;
        goto LD; // [223] 231
    }
    else{
        _6653 = NOVALUE;
    }
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    return 0;
LD: 

    /** 		if length(m_[KEY_LIST]) = 0  then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6654 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6654)){
            _6655 = SEQ_PTR(_6654)->length;
    }
    else {
        _6655 = 1;
    }
    _6654 = NOVALUE;
    if (_6655 != 0)
    goto LE; // [242] 251
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    return 0;
LE: 

    /** 		if length(m_[KEY_LIST]) != length(m_[VALUE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6657 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6657)){
            _6658 = SEQ_PTR(_6657)->length;
    }
    else {
        _6658 = 1;
    }
    _6657 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11734);
    _6659 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_6659)){
            _6660 = SEQ_PTR(_6659)->length;
    }
    else {
        _6660 = 1;
    }
    _6659 = NOVALUE;
    if (_6658 == _6660)
    goto LF; // [271] 280
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    _6657 = NOVALUE;
    _6659 = NOVALUE;
    return 0;
LF: 

    /** 		if length(m_[KEY_LIST]) != length(m_[FREE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6662 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6662)){
            _6663 = SEQ_PTR(_6662)->length;
    }
    else {
        _6663 = 1;
    }
    _6662 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11734);
    _6664 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_6664)){
            _6665 = SEQ_PTR(_6664)->length;
    }
    else {
        _6665 = 1;
    }
    _6664 = NOVALUE;
    if (_6663 == _6665)
    goto L10; // [300] 404
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    _6657 = NOVALUE;
    _6659 = NOVALUE;
    _6662 = NOVALUE;
    _6664 = NOVALUE;
    return 0;
    goto L10; // [309] 404
LA: 

    /** 	elsif  equal(m_[MAP_TYPE],LARGEMAP) then*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6667 = (int)*(((s1_ptr)_2)->base + 4);
    if (_6667 == 76)
    _6668 = 1;
    else if (IS_ATOM_INT(_6667) && IS_ATOM_INT(76))
    _6668 = 0;
    else
    _6668 = (compare(_6667, 76) == 0);
    _6667 = NOVALUE;
    if (_6668 == 0)
    {
        _6668 = NOVALUE;
        goto L11; // [324] 397
    }
    else{
        _6668 = NOVALUE;
    }

    /** 		if atom(m_[KEY_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6669 = (int)*(((s1_ptr)_2)->base + 5);
    _6670 = IS_ATOM(_6669);
    _6669 = NOVALUE;
    if (_6670 == 0)
    {
        _6670 = NOVALUE;
        goto L12; // [338] 346
    }
    else{
        _6670 = NOVALUE;
    }
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    _6657 = NOVALUE;
    _6659 = NOVALUE;
    _6662 = NOVALUE;
    _6664 = NOVALUE;
    return 0;
L12: 

    /** 		if atom(m_[VALUE_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6671 = (int)*(((s1_ptr)_2)->base + 6);
    _6672 = IS_ATOM(_6671);
    _6671 = NOVALUE;
    if (_6672 == 0)
    {
        _6672 = NOVALUE;
        goto L13; // [357] 365
    }
    else{
        _6672 = NOVALUE;
    }
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    _6657 = NOVALUE;
    _6659 = NOVALUE;
    _6662 = NOVALUE;
    _6664 = NOVALUE;
    return 0;
L13: 

    /** 		if length(m_[KEY_BUCKETS]) != length(m_[VALUE_BUCKETS])	then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11734);
    _6673 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6673)){
            _6674 = SEQ_PTR(_6673)->length;
    }
    else {
        _6674 = 1;
    }
    _6673 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11734);
    _6675 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_6675)){
            _6676 = SEQ_PTR(_6675)->length;
    }
    else {
        _6676 = 1;
    }
    _6675 = NOVALUE;
    if (_6674 == _6676)
    goto L10; // [385] 404
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    _6657 = NOVALUE;
    _6659 = NOVALUE;
    _6662 = NOVALUE;
    _6664 = NOVALUE;
    _6673 = NOVALUE;
    _6675 = NOVALUE;
    return 0;
    goto L10; // [394] 404
L11: 

    /** 		return 0*/
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    _6657 = NOVALUE;
    _6659 = NOVALUE;
    _6662 = NOVALUE;
    _6664 = NOVALUE;
    _6673 = NOVALUE;
    _6675 = NOVALUE;
    return 0;
L10: 

    /** 	return 1*/
    DeRef(_obj_p_11730);
    DeRef(_m__11734);
    _6654 = NOVALUE;
    _6657 = NOVALUE;
    _6659 = NOVALUE;
    _6662 = NOVALUE;
    _6664 = NOVALUE;
    _6673 = NOVALUE;
    _6675 = NOVALUE;
    return 1;
    ;
}


int _26calc_hash(int _key_p_11811, int _max_hash_p_11812)
{
    int _ret__11813 = NOVALUE;
    int _6681 = NOVALUE;
    int _6680 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_hash_p_11812)) {
        _1 = (long)(DBL_PTR(_max_hash_p_11812)->dbl);
        if (UNIQUE(DBL_PTR(_max_hash_p_11812)) && (DBL_PTR(_max_hash_p_11812)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_hash_p_11812);
        _max_hash_p_11812 = _1;
    }

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    _ret__11813 = calc_hash(_key_p_11811, -6);
    if (!IS_ATOM_INT(_ret__11813)) {
        _1 = (long)(DBL_PTR(_ret__11813)->dbl);
        if (UNIQUE(DBL_PTR(_ret__11813)) && (DBL_PTR(_ret__11813)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__11813);
        _ret__11813 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _6680 = (_ret__11813 % _max_hash_p_11812);
    _6681 = _6680 + 1;
    if (_6681 > MAXINT){
        _6681 = NewDouble((double)_6681);
    }
    _6680 = NOVALUE;
    DeRef(_key_p_11811);
    return _6681;
    ;
}


int _26threshold(int _new_value_p_11819)
{
    int _old_value__11822 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_new_value_p_11819)) {
        _1 = (long)(DBL_PTR(_new_value_p_11819)->dbl);
        if (UNIQUE(DBL_PTR(_new_value_p_11819)) && (DBL_PTR(_new_value_p_11819)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_value_p_11819);
        _new_value_p_11819 = _1;
    }

    /** 	if new_value_p < 1 then*/
    if (_new_value_p_11819 >= 1)
    goto L1; // [7] 20

    /** 		return threshold_size*/
    return _26threshold_size_11724;
L1: 

    /** 	integer old_value_ = threshold_size*/
    _old_value__11822 = _26threshold_size_11724;

    /** 	threshold_size = new_value_p*/
    _26threshold_size_11724 = _new_value_p_11819;

    /** 	return old_value_*/
    return _old_value__11822;
    ;
}


int _26type_of(int _the_map_p_11825)
{
    int _6684 = NOVALUE;
    int _6683 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][MAP_TYPE]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_11825)){
        _6683 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_11825)->dbl));
    }
    else{
        _6683 = (int)*(((s1_ptr)_2)->base + _the_map_p_11825);
    }
    _2 = (int)SEQ_PTR(_6683);
    _6684 = (int)*(((s1_ptr)_2)->base + 4);
    _6683 = NOVALUE;
    Ref(_6684);
    DeRef(_the_map_p_11825);
    return _6684;
    ;
}


void _26rehash(int _the_map_p_11830, int _requested_bucket_size_p_11831)
{
    int _size__11832 = NOVALUE;
    int _index_2__11833 = NOVALUE;
    int _old_key_buckets__11834 = NOVALUE;
    int _old_val_buckets__11835 = NOVALUE;
    int _new_key_buckets__11836 = NOVALUE;
    int _new_val_buckets__11837 = NOVALUE;
    int _key__11838 = NOVALUE;
    int _value__11839 = NOVALUE;
    int _pos_11840 = NOVALUE;
    int _new_keys_11841 = NOVALUE;
    int _in_use_11842 = NOVALUE;
    int _elem_count_11843 = NOVALUE;
    int _6745 = NOVALUE;
    int _6744 = NOVALUE;
    int _6743 = NOVALUE;
    int _6742 = NOVALUE;
    int _6741 = NOVALUE;
    int _6740 = NOVALUE;
    int _6739 = NOVALUE;
    int _6738 = NOVALUE;
    int _6737 = NOVALUE;
    int _6735 = NOVALUE;
    int _6734 = NOVALUE;
    int _6733 = NOVALUE;
    int _6730 = NOVALUE;
    int _6729 = NOVALUE;
    int _6727 = NOVALUE;
    int _6726 = NOVALUE;
    int _6725 = NOVALUE;
    int _6724 = NOVALUE;
    int _6722 = NOVALUE;
    int _6720 = NOVALUE;
    int _6718 = NOVALUE;
    int _6714 = NOVALUE;
    int _6712 = NOVALUE;
    int _6711 = NOVALUE;
    int _6710 = NOVALUE;
    int _6709 = NOVALUE;
    int _6707 = NOVALUE;
    int _6705 = NOVALUE;
    int _6703 = NOVALUE;
    int _6702 = NOVALUE;
    int _6700 = NOVALUE;
    int _6698 = NOVALUE;
    int _6695 = NOVALUE;
    int _6693 = NOVALUE;
    int _6692 = NOVALUE;
    int _6691 = NOVALUE;
    int _6690 = NOVALUE;
    int _6689 = NOVALUE;
    int _6686 = NOVALUE;
    int _6685 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_11830)) {
        _1 = (long)(DBL_PTR(_the_map_p_11830)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_11830)) && (DBL_PTR(_the_map_p_11830)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_11830);
        _the_map_p_11830 = _1;
    }
    if (!IS_ATOM_INT(_requested_bucket_size_p_11831)) {
        _1 = (long)(DBL_PTR(_requested_bucket_size_p_11831)->dbl);
        if (UNIQUE(DBL_PTR(_requested_bucket_size_p_11831)) && (DBL_PTR(_requested_bucket_size_p_11831)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_requested_bucket_size_p_11831);
        _requested_bucket_size_p_11831 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = SMALLMAP then*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6685 = (int)*(((s1_ptr)_2)->base + _the_map_p_11830);
    _2 = (int)SEQ_PTR(_6685);
    _6686 = (int)*(((s1_ptr)_2)->base + 4);
    _6685 = NOVALUE;
    if (binary_op_a(NOTEQ, _6686, 115)){
        _6686 = NOVALUE;
        goto L1; // [23] 33
    }
    _6686 = NOVALUE;

    /** 		return -- small maps are not hashed.*/
    DeRef(_old_key_buckets__11834);
    DeRef(_old_val_buckets__11835);
    DeRef(_new_key_buckets__11836);
    DeRef(_new_val_buckets__11837);
    DeRef(_key__11838);
    DeRef(_value__11839);
    DeRef(_new_keys_11841);
    return;
L1: 

    /** 	if requested_bucket_size_p <= 0 then*/
    if (_requested_bucket_size_p_11831 > 0)
    goto L2; // [35] 72

    /** 		size_ = floor(length(eumem:ram_space[the_map_p][KEY_BUCKETS]) * 3.5) + 1*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6689 = (int)*(((s1_ptr)_2)->base + _the_map_p_11830);
    _2 = (int)SEQ_PTR(_6689);
    _6690 = (int)*(((s1_ptr)_2)->base + 5);
    _6689 = NOVALUE;
    if (IS_SEQUENCE(_6690)){
            _6691 = SEQ_PTR(_6690)->length;
    }
    else {
        _6691 = 1;
    }
    _6690 = NOVALUE;
    _6692 = NewDouble((double)_6691 * DBL_PTR(_6176)->dbl);
    _6691 = NOVALUE;
    _6693 = unary_op(FLOOR, _6692);
    DeRefDS(_6692);
    _6692 = NOVALUE;
    if (IS_ATOM_INT(_6693)) {
        _size__11832 = _6693 + 1;
    }
    else
    { // coercing _size__11832 to an integer 1
        _size__11832 = 1+(long)(DBL_PTR(_6693)->dbl);
        if( !IS_ATOM_INT(_size__11832) ){
            _size__11832 = (object)DBL_PTR(_size__11832)->dbl;
        }
    }
    DeRef(_6693);
    _6693 = NOVALUE;
    goto L3; // [69] 80
L2: 

    /** 		size_ = requested_bucket_size_p*/
    _size__11832 = _requested_bucket_size_p_11831;
L3: 

    /** 	size_ = primes:next_prime(size_, -size_, 2)	-- Allow up to 2 seconds to calc next prime.*/
    if ((unsigned long)_size__11832 == 0xC0000000)
    _6695 = (int)NewDouble((double)-0xC0000000);
    else
    _6695 = - _size__11832;
    _size__11832 = _28next_prime(_size__11832, _6695, 2);
    _6695 = NOVALUE;
    if (!IS_ATOM_INT(_size__11832)) {
        _1 = (long)(DBL_PTR(_size__11832)->dbl);
        if (UNIQUE(DBL_PTR(_size__11832)) && (DBL_PTR(_size__11832)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size__11832);
        _size__11832 = _1;
    }

    /** 	if size_ < 0 then*/
    if (_size__11832 >= 0)
    goto L4; // [99] 109

    /** 		return  -- don't do anything. New size would take too long.*/
    DeRef(_old_key_buckets__11834);
    DeRef(_old_val_buckets__11835);
    DeRef(_new_key_buckets__11836);
    DeRef(_new_val_buckets__11837);
    DeRef(_key__11838);
    DeRef(_value__11839);
    DeRef(_new_keys_11841);
    _6690 = NOVALUE;
    return;
L4: 

    /** 	old_key_buckets_ = eumem:ram_space[the_map_p][KEY_BUCKETS]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6698 = (int)*(((s1_ptr)_2)->base + _the_map_p_11830);
    DeRef(_old_key_buckets__11834);
    _2 = (int)SEQ_PTR(_6698);
    _old_key_buckets__11834 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_old_key_buckets__11834);
    _6698 = NOVALUE;

    /** 	old_val_buckets_ = eumem:ram_space[the_map_p][VALUE_BUCKETS]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6700 = (int)*(((s1_ptr)_2)->base + _the_map_p_11830);
    DeRef(_old_val_buckets__11835);
    _2 = (int)SEQ_PTR(_6700);
    _old_val_buckets__11835 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_old_val_buckets__11835);
    _6700 = NOVALUE;

    /** 	new_key_buckets_ = repeat(repeat(1, threshold_size + 1), size_)*/
    _6702 = _26threshold_size_11724 + 1;
    _6703 = Repeat(1, _6702);
    _6702 = NOVALUE;
    DeRef(_new_key_buckets__11836);
    _new_key_buckets__11836 = Repeat(_6703, _size__11832);
    DeRefDS(_6703);
    _6703 = NOVALUE;

    /** 	new_val_buckets_ = repeat(repeat(0, threshold_size), size_)*/
    _6705 = Repeat(0, _26threshold_size_11724);
    DeRef(_new_val_buckets__11837);
    _new_val_buckets__11837 = Repeat(_6705, _size__11832);
    DeRefDS(_6705);
    _6705 = NOVALUE;

    /** 	elem_count = eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6707 = (int)*(((s1_ptr)_2)->base + _the_map_p_11830);
    _2 = (int)SEQ_PTR(_6707);
    _elem_count_11843 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_elem_count_11843)){
        _elem_count_11843 = (long)DBL_PTR(_elem_count_11843)->dbl;
    }
    _6707 = NOVALUE;

    /** 	in_use = 0*/
    _in_use_11842 = 0;

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_11830);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	for index = 1 to length(old_key_buckets_) do*/
    if (IS_SEQUENCE(_old_key_buckets__11834)){
            _6709 = SEQ_PTR(_old_key_buckets__11834)->length;
    }
    else {
        _6709 = 1;
    }
    {
        int _index_11873;
        _index_11873 = 1;
L5: 
        if (_index_11873 > _6709){
            goto L6; // [207] 387
        }

        /** 		for entry_idx = 1 to length(old_key_buckets_[index]) do*/
        _2 = (int)SEQ_PTR(_old_key_buckets__11834);
        _6710 = (int)*(((s1_ptr)_2)->base + _index_11873);
        if (IS_SEQUENCE(_6710)){
                _6711 = SEQ_PTR(_6710)->length;
        }
        else {
            _6711 = 1;
        }
        _6710 = NOVALUE;
        {
            int _entry_idx_11876;
            _entry_idx_11876 = 1;
L7: 
            if (_entry_idx_11876 > _6711){
                goto L8; // [223] 380
            }

            /** 			key_ = old_key_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_key_buckets__11834);
            _6712 = (int)*(((s1_ptr)_2)->base + _index_11873);
            DeRef(_key__11838);
            _2 = (int)SEQ_PTR(_6712);
            _key__11838 = (int)*(((s1_ptr)_2)->base + _entry_idx_11876);
            Ref(_key__11838);
            _6712 = NOVALUE;

            /** 			value_ = old_val_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_val_buckets__11835);
            _6714 = (int)*(((s1_ptr)_2)->base + _index_11873);
            DeRef(_value__11839);
            _2 = (int)SEQ_PTR(_6714);
            _value__11839 = (int)*(((s1_ptr)_2)->base + _entry_idx_11876);
            Ref(_value__11839);
            _6714 = NOVALUE;

            /** 			index_2_ = calc_hash(key_, size_)*/
            Ref(_key__11838);
            _index_2__11833 = _26calc_hash(_key__11838, _size__11832);
            if (!IS_ATOM_INT(_index_2__11833)) {
                _1 = (long)(DBL_PTR(_index_2__11833)->dbl);
                if (UNIQUE(DBL_PTR(_index_2__11833)) && (DBL_PTR(_index_2__11833)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_index_2__11833);
                _index_2__11833 = _1;
            }

            /** 			new_keys = new_key_buckets_[index_2_]*/
            DeRef(_new_keys_11841);
            _2 = (int)SEQ_PTR(_new_key_buckets__11836);
            _new_keys_11841 = (int)*(((s1_ptr)_2)->base + _index_2__11833);
            RefDS(_new_keys_11841);

            /** 			pos = new_keys[$]*/
            if (IS_SEQUENCE(_new_keys_11841)){
                    _6718 = SEQ_PTR(_new_keys_11841)->length;
            }
            else {
                _6718 = 1;
            }
            _2 = (int)SEQ_PTR(_new_keys_11841);
            _pos_11840 = (int)*(((s1_ptr)_2)->base + _6718);
            if (!IS_ATOM_INT(_pos_11840))
            _pos_11840 = (long)DBL_PTR(_pos_11840)->dbl;

            /** 			if length(new_keys) = pos then*/
            if (IS_SEQUENCE(_new_keys_11841)){
                    _6720 = SEQ_PTR(_new_keys_11841)->length;
            }
            else {
                _6720 = 1;
            }
            if (_6720 != _pos_11840)
            goto L9; // [285] 322

            /** 				new_keys &= repeat(pos, threshold_size)*/
            _6722 = Repeat(_pos_11840, _26threshold_size_11724);
            Concat((object_ptr)&_new_keys_11841, _new_keys_11841, _6722);
            DeRefDS(_6722);
            _6722 = NOVALUE;

            /** 				new_val_buckets_[index_2_] &= repeat(0, threshold_size)*/
            _6724 = Repeat(0, _26threshold_size_11724);
            _2 = (int)SEQ_PTR(_new_val_buckets__11837);
            _6725 = (int)*(((s1_ptr)_2)->base + _index_2__11833);
            if (IS_SEQUENCE(_6725) && IS_ATOM(_6724)) {
            }
            else if (IS_ATOM(_6725) && IS_SEQUENCE(_6724)) {
                Ref(_6725);
                Prepend(&_6726, _6724, _6725);
            }
            else {
                Concat((object_ptr)&_6726, _6725, _6724);
                _6725 = NOVALUE;
            }
            _6725 = NOVALUE;
            DeRefDS(_6724);
            _6724 = NOVALUE;
            _2 = (int)SEQ_PTR(_new_val_buckets__11837);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__11837 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__11833);
            _1 = *(int *)_2;
            *(int *)_2 = _6726;
            if( _1 != _6726 ){
                DeRef(_1);
            }
            _6726 = NOVALUE;
L9: 

            /** 			new_keys[pos] = key_*/
            Ref(_key__11838);
            _2 = (int)SEQ_PTR(_new_keys_11841);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_11841 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_11840);
            _1 = *(int *)_2;
            *(int *)_2 = _key__11838;
            DeRef(_1);

            /** 			new_val_buckets_[index_2_][pos] = value_*/
            _2 = (int)SEQ_PTR(_new_val_buckets__11837);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__11837 = MAKE_SEQ(_2);
            }
            _3 = (int)(_index_2__11833 + ((s1_ptr)_2)->base);
            Ref(_value__11839);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_11840);
            _1 = *(int *)_2;
            *(int *)_2 = _value__11839;
            DeRef(_1);
            _6727 = NOVALUE;

            /** 			new_keys[$] = pos + 1*/
            if (IS_SEQUENCE(_new_keys_11841)){
                    _6729 = SEQ_PTR(_new_keys_11841)->length;
            }
            else {
                _6729 = 1;
            }
            _6730 = _pos_11840 + 1;
            if (_6730 > MAXINT){
                _6730 = NewDouble((double)_6730);
            }
            _2 = (int)SEQ_PTR(_new_keys_11841);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_11841 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _6729);
            _1 = *(int *)_2;
            *(int *)_2 = _6730;
            if( _1 != _6730 ){
                DeRef(_1);
            }
            _6730 = NOVALUE;

            /** 			new_key_buckets_[index_2_] = new_keys*/
            RefDS(_new_keys_11841);
            _2 = (int)SEQ_PTR(_new_key_buckets__11836);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_key_buckets__11836 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__11833);
            _1 = *(int *)_2;
            *(int *)_2 = _new_keys_11841;
            DeRefDS(_1);

            /** 			if pos = 1 then*/
            if (_pos_11840 != 1)
            goto LA; // [360] 373

            /** 				in_use += 1*/
            _in_use_11842 = _in_use_11842 + 1;
LA: 

            /** 		end for*/
            _entry_idx_11876 = _entry_idx_11876 + 1;
            goto L7; // [375] 230
L8: 
            ;
        }

        /** 	end for*/
        _index_11873 = _index_11873 + 1;
        goto L5; // [382] 214
L6: 
        ;
    }

    /** 	for index = 1 to length(new_key_buckets_) do*/
    if (IS_SEQUENCE(_new_key_buckets__11836)){
            _6733 = SEQ_PTR(_new_key_buckets__11836)->length;
    }
    else {
        _6733 = 1;
    }
    {
        int _index_11903;
        _index_11903 = 1;
LB: 
        if (_index_11903 > _6733){
            goto LC; // [392] 467
        }

        /** 		pos = new_key_buckets_[index][$]*/
        _2 = (int)SEQ_PTR(_new_key_buckets__11836);
        _6734 = (int)*(((s1_ptr)_2)->base + _index_11903);
        if (IS_SEQUENCE(_6734)){
                _6735 = SEQ_PTR(_6734)->length;
        }
        else {
            _6735 = 1;
        }
        _2 = (int)SEQ_PTR(_6734);
        _pos_11840 = (int)*(((s1_ptr)_2)->base + _6735);
        if (!IS_ATOM_INT(_pos_11840)){
            _pos_11840 = (long)DBL_PTR(_pos_11840)->dbl;
        }
        _6734 = NOVALUE;

        /** 		new_key_buckets_[index] = remove(new_key_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_key_buckets__11836);
        _6737 = (int)*(((s1_ptr)_2)->base + _index_11903);
        _2 = (int)SEQ_PTR(_new_key_buckets__11836);
        _6738 = (int)*(((s1_ptr)_2)->base + _index_11903);
        if (IS_SEQUENCE(_6738)){
                _6739 = SEQ_PTR(_6738)->length;
        }
        else {
            _6739 = 1;
        }
        _6738 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_6737);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_11840)) ? _pos_11840 : (long)(DBL_PTR(_pos_11840)->dbl);
            int stop = (IS_ATOM_INT(_6739)) ? _6739 : (long)(DBL_PTR(_6739)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_6737);
                DeRef(_6740);
                _6740 = _6737;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_6737), start, &_6740 );
                }
                else Tail(SEQ_PTR(_6737), stop+1, &_6740);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_6737), start, &_6740);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_6740);
                _6740 = _1;
            }
        }
        _6737 = NOVALUE;
        _6739 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_key_buckets__11836);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_key_buckets__11836 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_11903);
        _1 = *(int *)_2;
        *(int *)_2 = _6740;
        if( _1 != _6740 ){
            DeRefDS(_1);
        }
        _6740 = NOVALUE;

        /** 		new_val_buckets_[index] = remove(new_val_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_val_buckets__11837);
        _6741 = (int)*(((s1_ptr)_2)->base + _index_11903);
        _2 = (int)SEQ_PTR(_new_val_buckets__11837);
        _6742 = (int)*(((s1_ptr)_2)->base + _index_11903);
        if (IS_SEQUENCE(_6742)){
                _6743 = SEQ_PTR(_6742)->length;
        }
        else {
            _6743 = 1;
        }
        _6742 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_6741);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_11840)) ? _pos_11840 : (long)(DBL_PTR(_pos_11840)->dbl);
            int stop = (IS_ATOM_INT(_6743)) ? _6743 : (long)(DBL_PTR(_6743)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_6741);
                DeRef(_6744);
                _6744 = _6741;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_6741), start, &_6744 );
                }
                else Tail(SEQ_PTR(_6741), stop+1, &_6744);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_6741), start, &_6744);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_6744);
                _6744 = _1;
            }
        }
        _6741 = NOVALUE;
        _6743 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_val_buckets__11837);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_val_buckets__11837 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_11903);
        _1 = *(int *)_2;
        *(int *)_2 = _6744;
        if( _1 != _6744 ){
            DeRef(_1);
        }
        _6744 = NOVALUE;

        /** 	end for*/
        _index_11903 = _index_11903 + 1;
        goto LB; // [462] 399
LC: 
        ;
    }

    /** 	eumem:ram_space[the_map_p] = { */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_26type_is_map_11702);
    *((int *)(_2+4)) = _26type_is_map_11702;
    *((int *)(_2+8)) = _elem_count_11843;
    *((int *)(_2+12)) = _in_use_11842;
    *((int *)(_2+16)) = 76;
    RefDS(_new_key_buckets__11836);
    *((int *)(_2+20)) = _new_key_buckets__11836;
    RefDS(_new_val_buckets__11837);
    *((int *)(_2+24)) = _new_val_buckets__11837;
    _6745 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_11830);
    _1 = *(int *)_2;
    *(int *)_2 = _6745;
    if( _1 != _6745 ){
        DeRef(_1);
    }
    _6745 = NOVALUE;

    /** end procedure*/
    DeRef(_old_key_buckets__11834);
    DeRef(_old_val_buckets__11835);
    DeRefDS(_new_key_buckets__11836);
    DeRefDS(_new_val_buckets__11837);
    DeRef(_key__11838);
    DeRef(_value__11839);
    DeRef(_new_keys_11841);
    _6690 = NOVALUE;
    _6710 = NOVALUE;
    _6738 = NOVALUE;
    _6742 = NOVALUE;
    return;
    ;
}


int _26new(int _initial_size_p_11919)
{
    int _buckets__11921 = NOVALUE;
    int _new_map__11922 = NOVALUE;
    int _temp_map__11923 = NOVALUE;
    int _6758 = NOVALUE;
    int _6757 = NOVALUE;
    int _6756 = NOVALUE;
    int _6754 = NOVALUE;
    int _6753 = NOVALUE;
    int _6750 = NOVALUE;
    int _6749 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_11919)) {
        _1 = (long)(DBL_PTR(_initial_size_p_11919)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_11919)) && (DBL_PTR(_initial_size_p_11919)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_11919);
        _initial_size_p_11919 = _1;
    }

    /** 	if initial_size_p < 3 then*/
    if (_initial_size_p_11919 >= 3)
    goto L1; // [7] 19

    /** 		initial_size_p = 3*/
    _initial_size_p_11919 = 3;
L1: 

    /** 	if initial_size_p > threshold_size then*/
    if (_initial_size_p_11919 <= _26threshold_size_11724)
    goto L2; // [23] 83

    /** 		buckets_ = floor((initial_size_p + threshold_size - 1) / threshold_size)*/
    _6749 = _initial_size_p_11919 + _26threshold_size_11724;
    if ((long)((unsigned long)_6749 + (unsigned long)HIGH_BITS) >= 0) 
    _6749 = NewDouble((double)_6749);
    if (IS_ATOM_INT(_6749)) {
        _6750 = _6749 - 1;
        if ((long)((unsigned long)_6750 +(unsigned long) HIGH_BITS) >= 0){
            _6750 = NewDouble((double)_6750);
        }
    }
    else {
        _6750 = NewDouble(DBL_PTR(_6749)->dbl - (double)1);
    }
    DeRef(_6749);
    _6749 = NOVALUE;
    if (IS_ATOM_INT(_6750)) {
        if (_26threshold_size_11724 > 0 && _6750 >= 0) {
            _buckets__11921 = _6750 / _26threshold_size_11724;
        }
        else {
            temp_dbl = floor((double)_6750 / (double)_26threshold_size_11724);
            _buckets__11921 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _6750, _26threshold_size_11724);
        _buckets__11921 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_6750);
    _6750 = NOVALUE;
    if (!IS_ATOM_INT(_buckets__11921)) {
        _1 = (long)(DBL_PTR(_buckets__11921)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__11921)) && (DBL_PTR(_buckets__11921)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__11921);
        _buckets__11921 = _1;
    }

    /** 		buckets_ = primes:next_prime(buckets_)*/
    _buckets__11921 = _28next_prime(_buckets__11921, -1, 1);
    if (!IS_ATOM_INT(_buckets__11921)) {
        _1 = (long)(DBL_PTR(_buckets__11921)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__11921)) && (DBL_PTR(_buckets__11921)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__11921);
        _buckets__11921 = _1;
    }

    /** 		new_map_ = { type_is_map, 0, 0, LARGEMAP, repeat({}, buckets_), repeat({}, buckets_) }*/
    _6753 = Repeat(_5, _buckets__11921);
    _6754 = Repeat(_5, _buckets__11921);
    _0 = _new_map__11922;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_26type_is_map_11702);
    *((int *)(_2+4)) = _26type_is_map_11702;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 76;
    *((int *)(_2+20)) = _6753;
    *((int *)(_2+24)) = _6754;
    _new_map__11922 = MAKE_SEQ(_1);
    DeRef(_0);
    _6754 = NOVALUE;
    _6753 = NOVALUE;
    goto L3; // [80] 108
L2: 

    /** 		new_map_ = {*/
    _6756 = Repeat(_26init_small_map_key_11725, _initial_size_p_11919);
    _6757 = Repeat(0, _initial_size_p_11919);
    _6758 = Repeat(0, _initial_size_p_11919);
    _0 = _new_map__11922;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_26type_is_map_11702);
    *((int *)(_2+4)) = _26type_is_map_11702;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _6756;
    *((int *)(_2+24)) = _6757;
    *((int *)(_2+28)) = _6758;
    _new_map__11922 = MAKE_SEQ(_1);
    DeRef(_0);
    _6758 = NOVALUE;
    _6757 = NOVALUE;
    _6756 = NOVALUE;
L3: 

    /** 	temp_map_ = eumem:malloc()*/
    _0 = _temp_map__11923;
    _temp_map__11923 = _27malloc(1, 1);
    DeRef(_0);

    /** 	eumem:ram_space[temp_map_] = new_map_*/
    RefDS(_new_map__11922);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map__11923))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map__11923)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map__11923);
    _1 = *(int *)_2;
    *(int *)_2 = _new_map__11922;
    DeRef(_1);

    /** 	return temp_map_*/
    DeRefDS(_new_map__11922);
    return _temp_map__11923;
    ;
}


int _26new_extra(int _the_map_p_11943, int _initial_size_p_11944)
{
    int _6762 = NOVALUE;
    int _6761 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_initial_size_p_11944)) {
        _1 = (long)(DBL_PTR(_initial_size_p_11944)->dbl);
        if (UNIQUE(DBL_PTR(_initial_size_p_11944)) && (DBL_PTR(_initial_size_p_11944)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_initial_size_p_11944);
        _initial_size_p_11944 = _1;
    }

    /** 	if map(the_map_p) then*/
    Ref(_the_map_p_11943);
    _6761 = _26map(_the_map_p_11943);
    if (_6761 == 0) {
        DeRef(_6761);
        _6761 = NOVALUE;
        goto L1; // [11] 23
    }
    else {
        if (!IS_ATOM_INT(_6761) && DBL_PTR(_6761)->dbl == 0.0){
            DeRef(_6761);
            _6761 = NOVALUE;
            goto L1; // [11] 23
        }
        DeRef(_6761);
        _6761 = NOVALUE;
    }
    DeRef(_6761);
    _6761 = NOVALUE;

    /** 		return the_map_p*/
    return _the_map_p_11943;
    goto L2; // [20] 34
L1: 

    /** 		return new(initial_size_p)*/
    _6762 = _26new(_initial_size_p_11944);
    DeRef(_the_map_p_11943);
    return _6762;
L2: 
    ;
}


int _26compare(int _map_1_p_11951, int _map_2_p_11952, int _scope_p_11953)
{
    int _data_set_1__11954 = NOVALUE;
    int _data_set_2__11955 = NOVALUE;
    int _6776 = NOVALUE;
    int _6770 = NOVALUE;
    int _6768 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_scope_p_11953)) {
        _1 = (long)(DBL_PTR(_scope_p_11953)->dbl);
        if (UNIQUE(DBL_PTR(_scope_p_11953)) && (DBL_PTR(_scope_p_11953)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_scope_p_11953);
        _scope_p_11953 = _1;
    }

    /** 	if map_1_p = map_2_p then*/
    if (binary_op_a(NOTEQ, _map_1_p_11951, _map_2_p_11952)){
        goto L1; // [7] 18
    }

    /** 		return 0*/
    DeRef(_map_1_p_11951);
    DeRef(_map_2_p_11952);
    DeRef(_data_set_1__11954);
    DeRef(_data_set_2__11955);
    return 0;
L1: 

    /** 	switch scope_p do*/
    _0 = _scope_p_11953;
    switch ( _0 ){ 

        /** 		case 'v', 'V' then*/
        case 118:
        case 86:

        /** 			data_set_1_ = stdsort:sort(values(map_1_p))*/
        Ref(_map_1_p_11951);
        _6768 = _26values(_map_1_p_11951, 0, 0);
        _0 = _data_set_1__11954;
        _data_set_1__11954 = _22sort(_6768, 1);
        DeRef(_0);
        _6768 = NOVALUE;

        /** 			data_set_2_ = stdsort:sort(values(map_2_p))*/
        Ref(_map_2_p_11952);
        _6770 = _26values(_map_2_p_11952, 0, 0);
        _0 = _data_set_2__11955;
        _data_set_2__11955 = _22sort(_6770, 1);
        DeRef(_0);
        _6770 = NOVALUE;
        goto L2; // [61] 112

        /** 		case 'k', 'K' then*/
        case 107:
        case 75:

        /** 			data_set_1_ = keys(map_1_p, 1)*/
        Ref(_map_1_p_11951);
        _0 = _data_set_1__11954;
        _data_set_1__11954 = _26keys(_map_1_p_11951, 1);
        DeRef(_0);

        /** 			data_set_2_ = keys(map_2_p, 1)*/
        Ref(_map_2_p_11952);
        _0 = _data_set_2__11955;
        _data_set_2__11955 = _26keys(_map_2_p_11952, 1);
        DeRef(_0);
        goto L2; // [87] 112

        /** 		case else*/
        default:

        /** 			data_set_1_ = pairs(map_1_p, 1)*/
        Ref(_map_1_p_11951);
        _0 = _data_set_1__11954;
        _data_set_1__11954 = _26pairs(_map_1_p_11951, 1);
        DeRef(_0);

        /** 			data_set_2_ = pairs(map_2_p, 1)*/
        Ref(_map_2_p_11952);
        _0 = _data_set_2__11955;
        _data_set_2__11955 = _26pairs(_map_2_p_11952, 1);
        DeRef(_0);
    ;}L2: 

    /** 	if equal(data_set_1_, data_set_2_) then*/
    if (_data_set_1__11954 == _data_set_2__11955)
    _6776 = 1;
    else if (IS_ATOM_INT(_data_set_1__11954) && IS_ATOM_INT(_data_set_2__11955))
    _6776 = 0;
    else
    _6776 = (compare(_data_set_1__11954, _data_set_2__11955) == 0);
    if (_6776 == 0)
    {
        _6776 = NOVALUE;
        goto L3; // [122] 132
    }
    else{
        _6776 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_map_1_p_11951);
    DeRef(_map_2_p_11952);
    DeRefDS(_data_set_1__11954);
    DeRefDS(_data_set_2__11955);
    return 1;
L3: 

    /** 	return -1*/
    DeRef(_map_1_p_11951);
    DeRef(_map_2_p_11952);
    DeRef(_data_set_1__11954);
    DeRef(_data_set_2__11955);
    return -1;
    ;
}


int _26has(int _the_map_p_11983, int _the_key_p_11984)
{
    int _index__11985 = NOVALUE;
    int _pos__11986 = NOVALUE;
    int _from__11987 = NOVALUE;
    int _6801 = NOVALUE;
    int _6799 = NOVALUE;
    int _6798 = NOVALUE;
    int _6795 = NOVALUE;
    int _6794 = NOVALUE;
    int _6793 = NOVALUE;
    int _6791 = NOVALUE;
    int _6790 = NOVALUE;
    int _6788 = NOVALUE;
    int _6786 = NOVALUE;
    int _6785 = NOVALUE;
    int _6784 = NOVALUE;
    int _6782 = NOVALUE;
    int _6781 = NOVALUE;
    int _6780 = NOVALUE;
    int _6778 = NOVALUE;
    int _6777 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_11983)) {
        _1 = (long)(DBL_PTR(_the_map_p_11983)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_11983)) && (DBL_PTR(_the_map_p_11983)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_11983);
        _the_map_p_11983 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6777 = (int)*(((s1_ptr)_2)->base + _the_map_p_11983);
    _2 = (int)SEQ_PTR(_6777);
    _6778 = (int)*(((s1_ptr)_2)->base + 4);
    _6777 = NOVALUE;
    if (binary_op_a(NOTEQ, _6778, 76)){
        _6778 = NOVALUE;
        goto L1; // [19] 77
    }
    _6778 = NOVALUE;

    /** 		index_ = calc_hash(the_key_p, length(eumem:ram_space[the_map_p][KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6780 = (int)*(((s1_ptr)_2)->base + _the_map_p_11983);
    _2 = (int)SEQ_PTR(_6780);
    _6781 = (int)*(((s1_ptr)_2)->base + 5);
    _6780 = NOVALUE;
    if (IS_SEQUENCE(_6781)){
            _6782 = SEQ_PTR(_6781)->length;
    }
    else {
        _6782 = 1;
    }
    _6781 = NOVALUE;
    Ref(_the_key_p_11984);
    _index__11985 = _26calc_hash(_the_key_p_11984, _6782);
    _6782 = NOVALUE;
    if (!IS_ATOM_INT(_index__11985)) {
        _1 = (long)(DBL_PTR(_index__11985)->dbl);
        if (UNIQUE(DBL_PTR(_index__11985)) && (DBL_PTR(_index__11985)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index__11985);
        _index__11985 = _1;
    }

    /** 		pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_BUCKETS][index_])*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6784 = (int)*(((s1_ptr)_2)->base + _the_map_p_11983);
    _2 = (int)SEQ_PTR(_6784);
    _6785 = (int)*(((s1_ptr)_2)->base + 5);
    _6784 = NOVALUE;
    _2 = (int)SEQ_PTR(_6785);
    _6786 = (int)*(((s1_ptr)_2)->base + _index__11985);
    _6785 = NOVALUE;
    _pos__11986 = find_from(_the_key_p_11984, _6786, 1);
    _6786 = NOVALUE;
    goto L2; // [74] 206
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_11984 == _26init_small_map_key_11725)
    _6788 = 1;
    else if (IS_ATOM_INT(_the_key_p_11984) && IS_ATOM_INT(_26init_small_map_key_11725))
    _6788 = 0;
    else
    _6788 = (compare(_the_key_p_11984, _26init_small_map_key_11725) == 0);
    if (_6788 == 0)
    {
        _6788 = NOVALUE;
        goto L3; // [83] 183
    }
    else{
        _6788 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__11987 = 1;

    /** 			while from_ > 0 do*/
L4: 
    if (_from__11987 <= 0)
    goto L5; // [98] 205

    /** 				pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6790 = (int)*(((s1_ptr)_2)->base + _the_map_p_11983);
    _2 = (int)SEQ_PTR(_6790);
    _6791 = (int)*(((s1_ptr)_2)->base + 5);
    _6790 = NOVALUE;
    _pos__11986 = find_from(_the_key_p_11984, _6791, _from__11987);
    _6791 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__11986 == 0)
    {
        goto L6; // [125] 160
    }
    else{
    }

    /** 					if eumem:ram_space[the_map_p][FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6793 = (int)*(((s1_ptr)_2)->base + _the_map_p_11983);
    _2 = (int)SEQ_PTR(_6793);
    _6794 = (int)*(((s1_ptr)_2)->base + 7);
    _6793 = NOVALUE;
    _2 = (int)SEQ_PTR(_6794);
    _6795 = (int)*(((s1_ptr)_2)->base + _pos__11986);
    _6794 = NOVALUE;
    if (binary_op_a(NOTEQ, _6795, 1)){
        _6795 = NOVALUE;
        goto L7; // [146] 167
    }
    _6795 = NOVALUE;

    /** 						return 1*/
    DeRef(_the_key_p_11984);
    _6781 = NOVALUE;
    return 1;
    goto L7; // [157] 167
L6: 

    /** 					return 0*/
    DeRef(_the_key_p_11984);
    _6781 = NOVALUE;
    return 0;
L7: 

    /** 				from_ = pos_ + 1*/
    _from__11987 = _pos__11986 + 1;

    /** 			end while*/
    goto L4; // [177] 98
    goto L5; // [180] 205
L3: 

    /** 			pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST])*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _6798 = (int)*(((s1_ptr)_2)->base + _the_map_p_11983);
    _2 = (int)SEQ_PTR(_6798);
    _6799 = (int)*(((s1_ptr)_2)->base + 5);
    _6798 = NOVALUE;
    _pos__11986 = find_from(_the_key_p_11984, _6799, 1);
    _6799 = NOVALUE;
L5: 
L2: 

    /** 	return (pos_  != 0)	*/
    _6801 = (_pos__11986 != 0);
    DeRef(_the_key_p_11984);
    _6781 = NOVALUE;
    return _6801;
    ;
}


int _26get(int _the_map_p_12023, int _the_key_p_12024, int _default_value_p_12025)
{
    int _bucket__12026 = NOVALUE;
    int _pos__12027 = NOVALUE;
    int _from__12028 = NOVALUE;
    int _themap_12029 = NOVALUE;
    int _thekeys_12034 = NOVALUE;
    int _6827 = NOVALUE;
    int _6826 = NOVALUE;
    int _6824 = NOVALUE;
    int _6822 = NOVALUE;
    int _6821 = NOVALUE;
    int _6819 = NOVALUE;
    int _6818 = NOVALUE;
    int _6816 = NOVALUE;
    int _6814 = NOVALUE;
    int _6813 = NOVALUE;
    int _6812 = NOVALUE;
    int _6811 = NOVALUE;
    int _6808 = NOVALUE;
    int _6806 = NOVALUE;
    int _6803 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_12023)) {
        _1 = (long)(DBL_PTR(_the_map_p_12023)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12023)) && (DBL_PTR(_the_map_p_12023)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12023);
        _the_map_p_12023 = _1;
    }

    /** 	themap = eumem:ram_space[the_map_p]*/
    DeRef(_themap_12029);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _themap_12029 = (int)*(((s1_ptr)_2)->base + _the_map_p_12023);
    Ref(_themap_12029);

    /** 	if themap[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_themap_12029);
    _6803 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6803, 76)){
        _6803 = NOVALUE;
        goto L1; // [23] 104
    }
    _6803 = NOVALUE;

    /** 		sequence thekeys*/

    /** 		thekeys = themap[KEY_BUCKETS]*/
    DeRef(_thekeys_12034);
    _2 = (int)SEQ_PTR(_themap_12029);
    _thekeys_12034 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_thekeys_12034);

    /** 		bucket_ = calc_hash(the_key_p, length(thekeys))*/
    if (IS_SEQUENCE(_thekeys_12034)){
            _6806 = SEQ_PTR(_thekeys_12034)->length;
    }
    else {
        _6806 = 1;
    }
    Ref(_the_key_p_12024);
    _bucket__12026 = _26calc_hash(_the_key_p_12024, _6806);
    _6806 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12026)) {
        _1 = (long)(DBL_PTR(_bucket__12026)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12026)) && (DBL_PTR(_bucket__12026)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12026);
        _bucket__12026 = _1;
    }

    /** 		pos_ = find(the_key_p, thekeys[bucket_])*/
    _2 = (int)SEQ_PTR(_thekeys_12034);
    _6808 = (int)*(((s1_ptr)_2)->base + _bucket__12026);
    _pos__12027 = find_from(_the_key_p_12024, _6808, 1);
    _6808 = NOVALUE;

    /** 		if pos_ > 0 then*/
    if (_pos__12027 <= 0)
    goto L2; // [68] 93

    /** 			return themap[VALUE_BUCKETS][bucket_][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12029);
    _6811 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6811);
    _6812 = (int)*(((s1_ptr)_2)->base + _bucket__12026);
    _6811 = NOVALUE;
    _2 = (int)SEQ_PTR(_6812);
    _6813 = (int)*(((s1_ptr)_2)->base + _pos__12027);
    _6812 = NOVALUE;
    Ref(_6813);
    DeRefDS(_thekeys_12034);
    DeRef(_the_key_p_12024);
    DeRef(_default_value_p_12025);
    DeRefDS(_themap_12029);
    return _6813;
L2: 

    /** 		return default_value_p*/
    DeRef(_thekeys_12034);
    DeRef(_the_key_p_12024);
    DeRef(_themap_12029);
    _6813 = NOVALUE;
    return _default_value_p_12025;
    goto L3; // [101] 247
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12024 == _26init_small_map_key_11725)
    _6814 = 1;
    else if (IS_ATOM_INT(_the_key_p_12024) && IS_ATOM_INT(_26init_small_map_key_11725))
    _6814 = 0;
    else
    _6814 = (compare(_the_key_p_12024, _26init_small_map_key_11725) == 0);
    if (_6814 == 0)
    {
        _6814 = NOVALUE;
        goto L4; // [110] 208
    }
    else{
        _6814 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12028 = 1;

    /** 			while from_ > 0 do*/
L5: 
    if (_from__12028 <= 0)
    goto L6; // [125] 246

    /** 				pos_ = find(the_key_p, themap[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_themap_12029);
    _6816 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__12027 = find_from(_the_key_p_12024, _6816, _from__12028);
    _6816 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__12027 == 0)
    {
        goto L7; // [146] 185
    }
    else{
    }

    /** 					if themap[FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_themap_12029);
    _6818 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_6818);
    _6819 = (int)*(((s1_ptr)_2)->base + _pos__12027);
    _6818 = NOVALUE;
    if (binary_op_a(NOTEQ, _6819, 1)){
        _6819 = NOVALUE;
        goto L8; // [161] 192
    }
    _6819 = NOVALUE;

    /** 						return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12029);
    _6821 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6821);
    _6822 = (int)*(((s1_ptr)_2)->base + _pos__12027);
    _6821 = NOVALUE;
    Ref(_6822);
    DeRef(_the_key_p_12024);
    DeRef(_default_value_p_12025);
    DeRefDS(_themap_12029);
    _6813 = NOVALUE;
    return _6822;
    goto L8; // [182] 192
L7: 

    /** 					return default_value_p*/
    DeRef(_the_key_p_12024);
    DeRef(_themap_12029);
    _6813 = NOVALUE;
    _6822 = NOVALUE;
    return _default_value_p_12025;
L8: 

    /** 				from_ = pos_ + 1*/
    _from__12028 = _pos__12027 + 1;

    /** 			end while*/
    goto L5; // [202] 125
    goto L6; // [205] 246
L4: 

    /** 			pos_ = find(the_key_p, themap[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_themap_12029);
    _6824 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__12027 = find_from(_the_key_p_12024, _6824, 1);
    _6824 = NOVALUE;

    /** 			if pos_  then*/
    if (_pos__12027 == 0)
    {
        goto L9; // [225] 245
    }
    else{
    }

    /** 				return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12029);
    _6826 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6826);
    _6827 = (int)*(((s1_ptr)_2)->base + _pos__12027);
    _6826 = NOVALUE;
    Ref(_6827);
    DeRef(_the_key_p_12024);
    DeRef(_default_value_p_12025);
    DeRefDS(_themap_12029);
    _6813 = NOVALUE;
    _6822 = NOVALUE;
    return _6827;
L9: 
L6: 
L3: 

    /** 	return default_value_p*/
    DeRef(_the_key_p_12024);
    DeRef(_themap_12029);
    _6813 = NOVALUE;
    _6822 = NOVALUE;
    _6827 = NOVALUE;
    return _default_value_p_12025;
    ;
}


int _26nested_get(int _the_map_p_12069, int _the_keys_p_12070, int _default_value_p_12071)
{
    int _val__12076 = NOVALUE;
    int _6837 = NOVALUE;
    int _6836 = NOVALUE;
    int _6834 = NOVALUE;
    int _6832 = NOVALUE;
    int _6830 = NOVALUE;
    int _6829 = NOVALUE;
    int _6828 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( the_keys_p ) - 1 do*/
    if (IS_SEQUENCE(_the_keys_p_12070)){
            _6828 = SEQ_PTR(_the_keys_p_12070)->length;
    }
    else {
        _6828 = 1;
    }
    _6829 = _6828 - 1;
    _6828 = NOVALUE;
    {
        int _i_12073;
        _i_12073 = 1;
L1: 
        if (_i_12073 > _6829){
            goto L2; // [12] 74
        }

        /** 		object val_ = get( the_map_p, the_keys_p[1], 0 )*/
        _2 = (int)SEQ_PTR(_the_keys_p_12070);
        _6830 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_the_map_p_12069);
        Ref(_6830);
        _0 = _val__12076;
        _val__12076 = _26get(_the_map_p_12069, _6830, 0);
        DeRef(_0);
        _6830 = NOVALUE;

        /** 		if not map( val_ ) then*/
        Ref(_val__12076);
        _6832 = _26map(_val__12076);
        if (IS_ATOM_INT(_6832)) {
            if (_6832 != 0){
                DeRef(_6832);
                _6832 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        else {
            if (DBL_PTR(_6832)->dbl != 0.0){
                DeRef(_6832);
                _6832 = NOVALUE;
                goto L3; // [37] 49
            }
        }
        DeRef(_6832);
        _6832 = NOVALUE;

        /** 			return default_value_p*/
        DeRef(_val__12076);
        DeRef(_the_map_p_12069);
        DeRefDS(_the_keys_p_12070);
        DeRef(_6829);
        _6829 = NOVALUE;
        return _default_value_p_12071;
        goto L4; // [46] 65
L3: 

        /** 			the_map_p = val_*/
        Ref(_val__12076);
        DeRef(_the_map_p_12069);
        _the_map_p_12069 = _val__12076;

        /** 			the_keys_p = the_keys_p[2..$]*/
        if (IS_SEQUENCE(_the_keys_p_12070)){
                _6834 = SEQ_PTR(_the_keys_p_12070)->length;
        }
        else {
            _6834 = 1;
        }
        rhs_slice_target = (object_ptr)&_the_keys_p_12070;
        RHS_Slice(_the_keys_p_12070, 2, _6834);
L4: 
        DeRef(_val__12076);
        _val__12076 = NOVALUE;

        /** 	end for*/
        _i_12073 = _i_12073 + 1;
        goto L1; // [69] 19
L2: 
        ;
    }

    /** 	return get( the_map_p, the_keys_p[1], default_value_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12070);
    _6836 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12069);
    Ref(_6836);
    Ref(_default_value_p_12071);
    _6837 = _26get(_the_map_p_12069, _6836, _default_value_p_12071);
    _6836 = NOVALUE;
    DeRef(_the_map_p_12069);
    DeRefDS(_the_keys_p_12070);
    DeRef(_default_value_p_12071);
    DeRef(_6829);
    _6829 = NOVALUE;
    return _6837;
    ;
}


void _26put(int _the_map_p_12089, int _the_key_p_12090, int _the_value_p_12091, int _operation_p_12092, int _trigger_p_12093)
{
    int _index__12094 = NOVALUE;
    int _bucket__12095 = NOVALUE;
    int _average_length__12096 = NOVALUE;
    int _from__12097 = NOVALUE;
    int _map_data_12098 = NOVALUE;
    int _data_12137 = NOVALUE;
    int _msg_inlined_crash_at_347_12153 = NOVALUE;
    int _msg_inlined_crash_at_394_12159 = NOVALUE;
    int _tmp_seqk_12173 = NOVALUE;
    int _tmp_seqv_12181 = NOVALUE;
    int _msg_inlined_crash_at_778_12217 = NOVALUE;
    int _msg_inlined_crash_at_1176_12280 = NOVALUE;
    int _6972 = NOVALUE;
    int _6971 = NOVALUE;
    int _6969 = NOVALUE;
    int _6968 = NOVALUE;
    int _6967 = NOVALUE;
    int _6966 = NOVALUE;
    int _6964 = NOVALUE;
    int _6963 = NOVALUE;
    int _6962 = NOVALUE;
    int _6960 = NOVALUE;
    int _6959 = NOVALUE;
    int _6958 = NOVALUE;
    int _6956 = NOVALUE;
    int _6955 = NOVALUE;
    int _6954 = NOVALUE;
    int _6952 = NOVALUE;
    int _6951 = NOVALUE;
    int _6950 = NOVALUE;
    int _6948 = NOVALUE;
    int _6946 = NOVALUE;
    int _6940 = NOVALUE;
    int _6939 = NOVALUE;
    int _6938 = NOVALUE;
    int _6937 = NOVALUE;
    int _6935 = NOVALUE;
    int _6933 = NOVALUE;
    int _6932 = NOVALUE;
    int _6931 = NOVALUE;
    int _6930 = NOVALUE;
    int _6929 = NOVALUE;
    int _6928 = NOVALUE;
    int _6925 = NOVALUE;
    int _6923 = NOVALUE;
    int _6920 = NOVALUE;
    int _6918 = NOVALUE;
    int _6915 = NOVALUE;
    int _6914 = NOVALUE;
    int _6912 = NOVALUE;
    int _6909 = NOVALUE;
    int _6908 = NOVALUE;
    int _6905 = NOVALUE;
    int _6902 = NOVALUE;
    int _6900 = NOVALUE;
    int _6898 = NOVALUE;
    int _6895 = NOVALUE;
    int _6893 = NOVALUE;
    int _6892 = NOVALUE;
    int _6891 = NOVALUE;
    int _6890 = NOVALUE;
    int _6889 = NOVALUE;
    int _6888 = NOVALUE;
    int _6887 = NOVALUE;
    int _6886 = NOVALUE;
    int _6885 = NOVALUE;
    int _6879 = NOVALUE;
    int _6877 = NOVALUE;
    int _6876 = NOVALUE;
    int _6874 = NOVALUE;
    int _6872 = NOVALUE;
    int _6869 = NOVALUE;
    int _6868 = NOVALUE;
    int _6867 = NOVALUE;
    int _6866 = NOVALUE;
    int _6864 = NOVALUE;
    int _6863 = NOVALUE;
    int _6862 = NOVALUE;
    int _6860 = NOVALUE;
    int _6859 = NOVALUE;
    int _6858 = NOVALUE;
    int _6856 = NOVALUE;
    int _6855 = NOVALUE;
    int _6854 = NOVALUE;
    int _6852 = NOVALUE;
    int _6850 = NOVALUE;
    int _6845 = NOVALUE;
    int _6844 = NOVALUE;
    int _6842 = NOVALUE;
    int _6841 = NOVALUE;
    int _6839 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_12089)) {
        _1 = (long)(DBL_PTR(_the_map_p_12089)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12089)) && (DBL_PTR(_the_map_p_12089)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12089);
        _the_map_p_12089 = _1;
    }
    if (!IS_ATOM_INT(_operation_p_12092)) {
        _1 = (long)(DBL_PTR(_operation_p_12092)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_12092)) && (DBL_PTR(_operation_p_12092)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_12092);
        _operation_p_12092 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_12093)) {
        _1 = (long)(DBL_PTR(_trigger_p_12093)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_12093)) && (DBL_PTR(_trigger_p_12093)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_12093);
        _trigger_p_12093 = _1;
    }

    /** 	sequence map_data = eumem:ram_space[the_map_p]*/
    DeRef(_map_data_12098);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _map_data_12098 = (int)*(((s1_ptr)_2)->base + _the_map_p_12089);
    Ref(_map_data_12098);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12089);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if map_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6839 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6839, 76)){
        _6839 = NOVALUE;
        goto L1; // [39] 659
    }
    _6839 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p,  length(map_data[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6841 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6841)){
            _6842 = SEQ_PTR(_6841)->length;
    }
    else {
        _6842 = 1;
    }
    _6841 = NOVALUE;
    Ref(_the_key_p_12090);
    _bucket__12095 = _26calc_hash(_the_key_p_12090, _6842);
    _6842 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12095)) {
        _1 = (long)(DBL_PTR(_bucket__12095)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12095)) && (DBL_PTR(_bucket__12095)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12095);
        _bucket__12095 = _1;
    }

    /** 		index_ = find(the_key_p, map_data[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6844 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6844);
    _6845 = (int)*(((s1_ptr)_2)->base + _bucket__12095);
    _6844 = NOVALUE;
    _index__12094 = find_from(_the_key_p_12090, _6845, 1);
    _6845 = NOVALUE;

    /** 		if index_ > 0 then*/
    if (_index__12094 <= 0)
    goto L2; // [84] 381

    /** 			switch operation_p do*/
    _0 = _operation_p_12092;
    switch ( _0 ){ 

        /** 				case PUT then*/
        case 1:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12095 + ((s1_ptr)_2)->base);
        _6850 = NOVALUE;
        Ref(_the_value_p_12091);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_12091;
        DeRef(_1);
        _6850 = NOVALUE;
        goto L3; // [117] 367

        /** 				case ADD then*/
        case 2:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12095 + ((s1_ptr)_2)->base);
        _6852 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6854 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6852 = NOVALUE;
        if (IS_ATOM_INT(_6854) && IS_ATOM_INT(_the_value_p_12091)) {
            _6855 = _6854 + _the_value_p_12091;
            if ((long)((unsigned long)_6855 + (unsigned long)HIGH_BITS) >= 0) 
            _6855 = NewDouble((double)_6855);
        }
        else {
            _6855 = binary_op(PLUS, _6854, _the_value_p_12091);
        }
        _6854 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6855;
        if( _1 != _6855 ){
            DeRef(_1);
        }
        _6855 = NOVALUE;
        _6852 = NOVALUE;
        goto L3; // [149] 367

        /** 				case SUBTRACT then*/
        case 3:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12095 + ((s1_ptr)_2)->base);
        _6856 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6858 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6856 = NOVALUE;
        if (IS_ATOM_INT(_6858) && IS_ATOM_INT(_the_value_p_12091)) {
            _6859 = _6858 - _the_value_p_12091;
            if ((long)((unsigned long)_6859 +(unsigned long) HIGH_BITS) >= 0){
                _6859 = NewDouble((double)_6859);
            }
        }
        else {
            _6859 = binary_op(MINUS, _6858, _the_value_p_12091);
        }
        _6858 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6859;
        if( _1 != _6859 ){
            DeRef(_1);
        }
        _6859 = NOVALUE;
        _6856 = NOVALUE;
        goto L3; // [181] 367

        /** 				case MULTIPLY then*/
        case 4:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12095 + ((s1_ptr)_2)->base);
        _6860 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6862 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6860 = NOVALUE;
        if (IS_ATOM_INT(_6862) && IS_ATOM_INT(_the_value_p_12091)) {
            if (_6862 == (short)_6862 && _the_value_p_12091 <= INT15 && _the_value_p_12091 >= -INT15)
            _6863 = _6862 * _the_value_p_12091;
            else
            _6863 = NewDouble(_6862 * (double)_the_value_p_12091);
        }
        else {
            _6863 = binary_op(MULTIPLY, _6862, _the_value_p_12091);
        }
        _6862 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6863;
        if( _1 != _6863 ){
            DeRef(_1);
        }
        _6863 = NOVALUE;
        _6860 = NOVALUE;
        goto L3; // [213] 367

        /** 				case DIVIDE then*/
        case 5:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12095 + ((s1_ptr)_2)->base);
        _6864 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6866 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6864 = NOVALUE;
        if (IS_ATOM_INT(_6866) && IS_ATOM_INT(_the_value_p_12091)) {
            _6867 = (_6866 % _the_value_p_12091) ? NewDouble((double)_6866 / _the_value_p_12091) : (_6866 / _the_value_p_12091);
        }
        else {
            _6867 = binary_op(DIVIDE, _6866, _the_value_p_12091);
        }
        _6866 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6867;
        if( _1 != _6867 ){
            DeRef(_1);
        }
        _6867 = NOVALUE;
        _6864 = NOVALUE;
        goto L3; // [245] 367

        /** 				case APPEND then*/
        case 6:

        /** 					sequence data = map_data[VALUE_BUCKETS][bucket_][index_]*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        _6868 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_6868);
        _6869 = (int)*(((s1_ptr)_2)->base + _bucket__12095);
        _6868 = NOVALUE;
        DeRef(_data_12137);
        _2 = (int)SEQ_PTR(_6869);
        _data_12137 = (int)*(((s1_ptr)_2)->base + _index__12094);
        Ref(_data_12137);
        _6869 = NOVALUE;

        /** 					data = append( data, the_value_p )*/
        Ref(_the_value_p_12091);
        Append(&_data_12137, _data_12137, _the_value_p_12091);

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = data*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12095 + ((s1_ptr)_2)->base);
        _6872 = NOVALUE;
        RefDS(_data_12137);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _data_12137;
        DeRef(_1);
        _6872 = NOVALUE;
        DeRefDS(_data_12137);
        _data_12137 = NOVALUE;
        goto L3; // [295] 367

        /** 				case CONCAT then*/
        case 7:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12095 + ((s1_ptr)_2)->base);
        _6874 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6876 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6874 = NOVALUE;
        if (IS_SEQUENCE(_6876) && IS_ATOM(_the_value_p_12091)) {
            Ref(_the_value_p_12091);
            Append(&_6877, _6876, _the_value_p_12091);
        }
        else if (IS_ATOM(_6876) && IS_SEQUENCE(_the_value_p_12091)) {
            Ref(_6876);
            Prepend(&_6877, _the_value_p_12091, _6876);
        }
        else {
            Concat((object_ptr)&_6877, _6876, _the_value_p_12091);
            _6876 = NOVALUE;
        }
        _6876 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6877;
        if( _1 != _6877 ){
            DeRef(_1);
        }
        _6877 = NOVALUE;
        _6874 = NOVALUE;
        goto L3; // [327] 367

        /** 				case LEAVE then*/
        case 8:

        /** 					operation_p = operation_p*/
        _operation_p_12092 = _operation_p_12092;
        goto L3; // [340] 367

        /** 				case else*/
        default:

        /** 					error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_347_12153);
        _msg_inlined_crash_at_347_12153 = EPrintf(-9999999, _6878, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_347_12153);

        /** end procedure*/
        goto L4; // [361] 364
L4: 
        DeRefi(_msg_inlined_crash_at_347_12153);
        _msg_inlined_crash_at_347_12153 = NOVALUE;
    ;}L3: 

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12098);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12089);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12098;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_12173);
    DeRef(_tmp_seqv_12181);
    DeRef(_the_key_p_12090);
    DeRef(_the_value_p_12091);
    DeRef(_average_length__12096);
    DeRefDS(_map_data_12098);
    _6841 = NOVALUE;
    return;
L2: 

    /** 		if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _6879 = find_from(_operation_p_12092, _26INIT_OPERATIONS_11719, 1);
    if (_6879 != 0)
    goto L5; // [390] 414
    _6879 = NOVALUE;

    /** 				error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_394_12159);
    _msg_inlined_crash_at_394_12159 = EPrintf(-9999999, _6881, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_394_12159);

    /** end procedure*/
    goto L6; // [408] 411
L6: 
    DeRefi(_msg_inlined_crash_at_394_12159);
    _msg_inlined_crash_at_394_12159 = NOVALUE;
L5: 

    /** 		if operation_p = LEAVE then*/
    if (_operation_p_12092 != 8)
    goto L7; // [418] 436

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12098);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12089);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12098;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_12173);
    DeRef(_tmp_seqv_12181);
    DeRef(_the_key_p_12090);
    DeRef(_the_value_p_12091);
    DeRef(_average_length__12096);
    DeRefDS(_map_data_12098);
    _6841 = NOVALUE;
    return;
L7: 

    /** 		if operation_p = APPEND then*/
    if (_operation_p_12092 != 6)
    goto L8; // [440] 451

    /** 			the_value_p = { the_value_p }*/
    _0 = _the_value_p_12091;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_12091);
    *((int *)(_2+4)) = _the_value_p_12091;
    _the_value_p_12091 = MAKE_SEQ(_1);
    DeRef(_0);
L8: 

    /** 		map_data[IN_USE] += (length(map_data[KEY_BUCKETS][bucket_]) = 0)*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6885 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6885);
    _6886 = (int)*(((s1_ptr)_2)->base + _bucket__12095);
    _6885 = NOVALUE;
    if (IS_SEQUENCE(_6886)){
            _6887 = SEQ_PTR(_6886)->length;
    }
    else {
        _6887 = 1;
    }
    _6886 = NOVALUE;
    _6888 = (_6887 == 0);
    _6887 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6889 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_6889)) {
        _6890 = _6889 + _6888;
        if ((long)((unsigned long)_6890 + (unsigned long)HIGH_BITS) >= 0) 
        _6890 = NewDouble((double)_6890);
    }
    else {
        _6890 = binary_op(PLUS, _6889, _6888);
    }
    _6889 = NOVALUE;
    _6888 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _6890;
    if( _1 != _6890 ){
        DeRef(_1);
    }
    _6890 = NOVALUE;

    /** 		map_data[ELEMENT_COUNT] += 1 -- elementCount		*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6891 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6891)) {
        _6892 = _6891 + 1;
        if (_6892 > MAXINT){
            _6892 = NewDouble((double)_6892);
        }
    }
    else
    _6892 = binary_op(PLUS, 1, _6891);
    _6891 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _6892;
    if( _1 != _6892 ){
        DeRef(_1);
    }
    _6892 = NOVALUE;

    /** 		sequence tmp_seqk*/

    /** 		tmp_seqk = map_data[KEY_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6893 = (int)*(((s1_ptr)_2)->base + 5);
    DeRef(_tmp_seqk_12173);
    _2 = (int)SEQ_PTR(_6893);
    _tmp_seqk_12173 = (int)*(((s1_ptr)_2)->base + _bucket__12095);
    Ref(_tmp_seqk_12173);
    _6893 = NOVALUE;

    /** 		map_data[KEY_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12095);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _6895 = NOVALUE;

    /** 		tmp_seqk = append( tmp_seqk, the_key_p)*/
    Ref(_the_key_p_12090);
    Append(&_tmp_seqk_12173, _tmp_seqk_12173, _the_key_p_12090);

    /** 		map_data[KEY_BUCKETS][bucket_] = tmp_seqk*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqk_12173);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12095);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqk_12173;
    DeRef(_1);
    _6898 = NOVALUE;

    /** 		sequence tmp_seqv*/

    /** 		tmp_seqv = map_data[VALUE_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6900 = (int)*(((s1_ptr)_2)->base + 6);
    DeRef(_tmp_seqv_12181);
    _2 = (int)SEQ_PTR(_6900);
    _tmp_seqv_12181 = (int)*(((s1_ptr)_2)->base + _bucket__12095);
    Ref(_tmp_seqv_12181);
    _6900 = NOVALUE;

    /** 		map_data[VALUE_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12095);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _6902 = NOVALUE;

    /** 		tmp_seqv = append( tmp_seqv, the_value_p)*/
    Ref(_the_value_p_12091);
    Append(&_tmp_seqv_12181, _tmp_seqv_12181, _the_value_p_12091);

    /** 		map_data[VALUE_BUCKETS][bucket_] = tmp_seqv*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqv_12181);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12095);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqv_12181;
    DeRef(_1);
    _6905 = NOVALUE;

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12098);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12089);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12098;
    DeRef(_1);

    /** 		if trigger_p > 0 then*/
    if (_trigger_p_12093 <= 0)
    goto L9; // [606] 649

    /** 			average_length_ = map_data[ELEMENT_COUNT] / map_data[IN_USE]*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6908 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6909 = (int)*(((s1_ptr)_2)->base + 3);
    DeRef(_average_length__12096);
    if (IS_ATOM_INT(_6908) && IS_ATOM_INT(_6909)) {
        _average_length__12096 = (_6908 % _6909) ? NewDouble((double)_6908 / _6909) : (_6908 / _6909);
    }
    else {
        _average_length__12096 = binary_op(DIVIDE, _6908, _6909);
    }
    _6908 = NOVALUE;
    _6909 = NOVALUE;

    /** 			if (average_length_ >= trigger_p) then*/
    if (binary_op_a(LESS, _average_length__12096, _trigger_p_12093)){
        goto LA; // [630] 648
    }

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_12098);
    _map_data_12098 = _5;

    /** 				rehash(the_map_p)*/
    _26rehash(_the_map_p_12089, 0);
LA: 
L9: 

    /** 		return*/
    DeRef(_tmp_seqk_12173);
    DeRef(_tmp_seqv_12181);
    DeRef(_the_key_p_12090);
    DeRef(_the_value_p_12091);
    DeRef(_average_length__12096);
    DeRef(_map_data_12098);
    _6841 = NOVALUE;
    _6886 = NOVALUE;
    return;
    goto LB; // [656] 1209
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12090 == _26init_small_map_key_11725)
    _6912 = 1;
    else if (IS_ATOM_INT(_the_key_p_12090) && IS_ATOM_INT(_26init_small_map_key_11725))
    _6912 = 0;
    else
    _6912 = (compare(_the_key_p_12090, _26init_small_map_key_11725) == 0);
    if (_6912 == 0)
    {
        _6912 = NOVALUE;
        goto LC; // [665] 741
    }
    else{
        _6912 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12097 = 1;

    /** 			while index_ > 0 with entry do*/
    goto LD; // [677] 718
LE: 
    if (_index__12094 <= 0)
    goto LF; // [682] 757

    /** 				if map_data[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6914 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_6914);
    _6915 = (int)*(((s1_ptr)_2)->base + _index__12094);
    _6914 = NOVALUE;
    if (binary_op_a(NOTEQ, _6915, 1)){
        _6915 = NOVALUE;
        goto L10; // [698] 707
    }
    _6915 = NOVALUE;

    /** 					exit*/
    goto LF; // [704] 757
L10: 

    /** 				from_ = index_ + 1*/
    _from__12097 = _index__12094 + 1;

    /** 			  entry*/
LD: 

    /** 				index_ = find(the_key_p, map_data[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6918 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12094 = find_from(_the_key_p_12090, _6918, _from__12097);
    _6918 = NOVALUE;

    /** 			end while*/
    goto LE; // [735] 680
    goto LF; // [738] 757
LC: 

    /** 			index_ = find(the_key_p, map_data[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6920 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12094 = find_from(_the_key_p_12090, _6920, 1);
    _6920 = NOVALUE;
LF: 

    /** 		if index_ = 0 then*/
    if (_index__12094 != 0)
    goto L11; // [761] 963

    /** 			if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _6923 = find_from(_operation_p_12092, _26INIT_OPERATIONS_11719, 1);
    if (_6923 != 0)
    goto L12; // [774] 798
    _6923 = NOVALUE;

    /** 					error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_778_12217);
    _msg_inlined_crash_at_778_12217 = EPrintf(-9999999, _6881, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_778_12217);

    /** end procedure*/
    goto L13; // [792] 795
L13: 
    DeRefi(_msg_inlined_crash_at_778_12217);
    _msg_inlined_crash_at_778_12217 = NOVALUE;
L12: 

    /** 			index_ = find(0, map_data[FREE_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6925 = (int)*(((s1_ptr)_2)->base + 7);
    _index__12094 = find_from(0, _6925, 1);
    _6925 = NOVALUE;

    /** 			if index_ = 0 then*/
    if (_index__12094 != 0)
    goto L14; // [815] 869

    /** 				eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12098);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12089);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12098;
    DeRef(_1);

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_12098);
    _map_data_12098 = _5;

    /** 				convert_to_large_map(the_map_p)*/
    _26convert_to_large_map(_the_map_p_12089);

    /** 				put(the_map_p, the_key_p, the_value_p, operation_p, trigger_p)*/
    DeRef(_6928);
    _6928 = _the_map_p_12089;
    Ref(_the_key_p_12090);
    DeRef(_6929);
    _6929 = _the_key_p_12090;
    Ref(_the_value_p_12091);
    DeRef(_6930);
    _6930 = _the_value_p_12091;
    DeRef(_6931);
    _6931 = _operation_p_12092;
    DeRef(_6932);
    _6932 = _trigger_p_12093;
    _26put(_6928, _6929, _6930, _6931, _6932);
    _6928 = NOVALUE;
    _6929 = NOVALUE;
    _6930 = NOVALUE;
    _6931 = NOVALUE;
    _6932 = NOVALUE;

    /** 				return*/
    DeRef(_the_key_p_12090);
    DeRef(_the_value_p_12091);
    DeRef(_average_length__12096);
    DeRefDS(_map_data_12098);
    _6841 = NOVALUE;
    _6886 = NOVALUE;
    return;
L14: 

    /** 			map_data[KEY_LIST][index_] = the_key_p*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    Ref(_the_key_p_12090);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12094);
    _1 = *(int *)_2;
    *(int *)_2 = _the_key_p_12090;
    DeRef(_1);
    _6933 = NOVALUE;

    /** 			map_data[FREE_LIST][index_] = 1*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12094);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _6935 = NOVALUE;

    /** 			map_data[IN_USE] += 1*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6937 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_6937)) {
        _6938 = _6937 + 1;
        if (_6938 > MAXINT){
            _6938 = NewDouble((double)_6938);
        }
    }
    else
    _6938 = binary_op(PLUS, 1, _6937);
    _6937 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _6938;
    if( _1 != _6938 ){
        DeRef(_1);
    }
    _6938 = NOVALUE;

    /** 			map_data[ELEMENT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_map_data_12098);
    _6939 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6939)) {
        _6940 = _6939 + 1;
        if (_6940 > MAXINT){
            _6940 = NewDouble((double)_6940);
        }
    }
    else
    _6940 = binary_op(PLUS, 1, _6939);
    _6939 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12098);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12098 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _6940;
    if( _1 != _6940 ){
        DeRef(_1);
    }
    _6940 = NOVALUE;

    /** 			if operation_p = APPEND then*/
    if (_operation_p_12092 != 6)
    goto L15; // [931] 942

    /** 				the_value_p = { the_value_p }*/
    _0 = _the_value_p_12091;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_12091);
    *((int *)(_2+4)) = _the_value_p_12091;
    _the_value_p_12091 = MAKE_SEQ(_1);
    DeRef(_0);
L15: 

    /** 			if operation_p != LEAVE then*/
    if (_operation_p_12092 == 8)
    goto L16; // [946] 962

    /** 				operation_p = PUT	-- Initially, nearly everything is a PUT.*/
    _operation_p_12092 = 1;
L16: 
L11: 

    /** 		switch operation_p do*/
    _0 = _operation_p_12092;
    switch ( _0 ){ 

        /** 			case PUT then*/
        case 1:

        /** 				map_data[VALUE_LIST][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        Ref(_the_value_p_12091);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_12091;
        DeRef(_1);
        _6946 = NOVALUE;
        goto L17; // [987] 1195

        /** 			case ADD then*/
        case 2:

        /** 				map_data[VALUE_LIST][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6950 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6948 = NOVALUE;
        if (IS_ATOM_INT(_6950) && IS_ATOM_INT(_the_value_p_12091)) {
            _6951 = _6950 + _the_value_p_12091;
            if ((long)((unsigned long)_6951 + (unsigned long)HIGH_BITS) >= 0) 
            _6951 = NewDouble((double)_6951);
        }
        else {
            _6951 = binary_op(PLUS, _6950, _the_value_p_12091);
        }
        _6950 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6951;
        if( _1 != _6951 ){
            DeRef(_1);
        }
        _6951 = NOVALUE;
        _6948 = NOVALUE;
        goto L17; // [1014] 1195

        /** 			case SUBTRACT then*/
        case 3:

        /** 				map_data[VALUE_LIST][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6954 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6952 = NOVALUE;
        if (IS_ATOM_INT(_6954) && IS_ATOM_INT(_the_value_p_12091)) {
            _6955 = _6954 - _the_value_p_12091;
            if ((long)((unsigned long)_6955 +(unsigned long) HIGH_BITS) >= 0){
                _6955 = NewDouble((double)_6955);
            }
        }
        else {
            _6955 = binary_op(MINUS, _6954, _the_value_p_12091);
        }
        _6954 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6955;
        if( _1 != _6955 ){
            DeRef(_1);
        }
        _6955 = NOVALUE;
        _6952 = NOVALUE;
        goto L17; // [1041] 1195

        /** 			case MULTIPLY then*/
        case 4:

        /** 				map_data[VALUE_LIST][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6958 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6956 = NOVALUE;
        if (IS_ATOM_INT(_6958) && IS_ATOM_INT(_the_value_p_12091)) {
            if (_6958 == (short)_6958 && _the_value_p_12091 <= INT15 && _the_value_p_12091 >= -INT15)
            _6959 = _6958 * _the_value_p_12091;
            else
            _6959 = NewDouble(_6958 * (double)_the_value_p_12091);
        }
        else {
            _6959 = binary_op(MULTIPLY, _6958, _the_value_p_12091);
        }
        _6958 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6959;
        if( _1 != _6959 ){
            DeRef(_1);
        }
        _6959 = NOVALUE;
        _6956 = NOVALUE;
        goto L17; // [1068] 1195

        /** 			case DIVIDE then*/
        case 5:

        /** 				map_data[VALUE_LIST][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6962 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6960 = NOVALUE;
        if (IS_ATOM_INT(_6962) && IS_ATOM_INT(_the_value_p_12091)) {
            _6963 = (_6962 % _the_value_p_12091) ? NewDouble((double)_6962 / _the_value_p_12091) : (_6962 / _the_value_p_12091);
        }
        else {
            _6963 = binary_op(DIVIDE, _6962, _the_value_p_12091);
        }
        _6962 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6963;
        if( _1 != _6963 ){
            DeRef(_1);
        }
        _6963 = NOVALUE;
        _6960 = NOVALUE;
        goto L17; // [1095] 1195

        /** 			case APPEND then*/
        case 6:

        /** 				map_data[VALUE_LIST][index_] = append( map_data[VALUE_LIST][index_], the_value_p )*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_map_data_12098);
        _6966 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_6966);
        _6967 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6966 = NOVALUE;
        Ref(_the_value_p_12091);
        Append(&_6968, _6967, _the_value_p_12091);
        _6967 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6968;
        if( _1 != _6968 ){
            DeRef(_1);
        }
        _6968 = NOVALUE;
        _6964 = NOVALUE;
        goto L17; // [1128] 1195

        /** 			case CONCAT then*/
        case 7:

        /** 				map_data[VALUE_LIST][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12098);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12098 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6971 = (int)*(((s1_ptr)_2)->base + _index__12094);
        _6969 = NOVALUE;
        if (IS_SEQUENCE(_6971) && IS_ATOM(_the_value_p_12091)) {
            Ref(_the_value_p_12091);
            Append(&_6972, _6971, _the_value_p_12091);
        }
        else if (IS_ATOM(_6971) && IS_SEQUENCE(_the_value_p_12091)) {
            Ref(_6971);
            Prepend(&_6972, _the_value_p_12091, _6971);
        }
        else {
            Concat((object_ptr)&_6972, _6971, _the_value_p_12091);
            _6971 = NOVALUE;
        }
        _6971 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12094);
        _1 = *(int *)_2;
        *(int *)_2 = _6972;
        if( _1 != _6972 ){
            DeRef(_1);
        }
        _6972 = NOVALUE;
        _6969 = NOVALUE;
        goto L17; // [1155] 1195

        /** 			case LEAVE then*/
        case 8:

        /** 				operation_p = operation_p*/
        _operation_p_12092 = _operation_p_12092;
        goto L17; // [1168] 1195

        /** 			case else*/
        default:

        /** 				error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_1176_12280);
        _msg_inlined_crash_at_1176_12280 = EPrintf(-9999999, _6878, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_1176_12280);

        /** end procedure*/
        goto L18; // [1189] 1192
L18: 
        DeRefi(_msg_inlined_crash_at_1176_12280);
        _msg_inlined_crash_at_1176_12280 = NOVALUE;
    ;}L17: 

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12098);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12089);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12098;
    DeRef(_1);

    /** 		return*/
    DeRef(_the_key_p_12090);
    DeRef(_the_value_p_12091);
    DeRef(_average_length__12096);
    DeRefDS(_map_data_12098);
    _6841 = NOVALUE;
    _6886 = NOVALUE;
    return;
LB: 

    /** end procedure*/
    DeRef(_the_key_p_12090);
    DeRef(_the_value_p_12091);
    DeRef(_average_length__12096);
    DeRef(_map_data_12098);
    _6841 = NOVALUE;
    _6886 = NOVALUE;
    return;
    ;
}


void _26nested_put(int _the_map_p_12283, int _the_keys_p_12284, int _the_value_p_12285, int _operation_p_12286, int _trigger_p_12287)
{
    int _temp_map__12288 = NOVALUE;
    int _6985 = NOVALUE;
    int _6984 = NOVALUE;
    int _6983 = NOVALUE;
    int _6982 = NOVALUE;
    int _6981 = NOVALUE;
    int _6980 = NOVALUE;
    int _6979 = NOVALUE;
    int _6977 = NOVALUE;
    int _6976 = NOVALUE;
    int _6975 = NOVALUE;
    int _6973 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_operation_p_12286)) {
        _1 = (long)(DBL_PTR(_operation_p_12286)->dbl);
        if (UNIQUE(DBL_PTR(_operation_p_12286)) && (DBL_PTR(_operation_p_12286)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_operation_p_12286);
        _operation_p_12286 = _1;
    }
    if (!IS_ATOM_INT(_trigger_p_12287)) {
        _1 = (long)(DBL_PTR(_trigger_p_12287)->dbl);
        if (UNIQUE(DBL_PTR(_trigger_p_12287)) && (DBL_PTR(_trigger_p_12287)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_trigger_p_12287);
        _trigger_p_12287 = _1;
    }

    /** 	if length( the_keys_p ) = 1 then*/
    if (IS_SEQUENCE(_the_keys_p_12284)){
            _6973 = SEQ_PTR(_the_keys_p_12284)->length;
    }
    else {
        _6973 = 1;
    }
    if (_6973 != 1)
    goto L1; // [16] 36

    /** 		put( the_map_p, the_keys_p[1], the_value_p, operation_p, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12284);
    _6975 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12283);
    Ref(_6975);
    Ref(_the_value_p_12285);
    _26put(_the_map_p_12283, _6975, _the_value_p_12285, _operation_p_12286, _trigger_p_12287);
    _6975 = NOVALUE;
    goto L2; // [33] 98
L1: 

    /** 		temp_map_ = new_extra( get( the_map_p, the_keys_p[1] ) )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12284);
    _6976 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12283);
    Ref(_6976);
    _6977 = _26get(_the_map_p_12283, _6976, 0);
    _6976 = NOVALUE;
    _0 = _temp_map__12288;
    _temp_map__12288 = _26new_extra(_6977, 690);
    DeRef(_0);
    _6977 = NOVALUE;

    /** 		nested_put( temp_map_, the_keys_p[2..$], the_value_p, operation_p, trigger_p )*/
    if (IS_SEQUENCE(_the_keys_p_12284)){
            _6979 = SEQ_PTR(_the_keys_p_12284)->length;
    }
    else {
        _6979 = 1;
    }
    rhs_slice_target = (object_ptr)&_6980;
    RHS_Slice(_the_keys_p_12284, 2, _6979);
    Ref(_temp_map__12288);
    DeRef(_6981);
    _6981 = _temp_map__12288;
    Ref(_the_value_p_12285);
    DeRef(_6982);
    _6982 = _the_value_p_12285;
    DeRef(_6983);
    _6983 = _operation_p_12286;
    DeRef(_6984);
    _6984 = _trigger_p_12287;
    _26nested_put(_6981, _6980, _6982, _6983, _6984);
    _6981 = NOVALUE;
    _6980 = NOVALUE;
    _6982 = NOVALUE;
    _6983 = NOVALUE;
    _6984 = NOVALUE;

    /** 		put( the_map_p, the_keys_p[1], temp_map_, PUT, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12284);
    _6985 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12283);
    Ref(_6985);
    Ref(_temp_map__12288);
    _26put(_the_map_p_12283, _6985, _temp_map__12288, 1, _trigger_p_12287);
    _6985 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_the_map_p_12283);
    DeRefDS(_the_keys_p_12284);
    DeRef(_the_value_p_12285);
    DeRef(_temp_map__12288);
    return;
    ;
}


void _26remove(int _the_map_p_12306, int _the_key_p_12307)
{
    int _index__12308 = NOVALUE;
    int _bucket__12309 = NOVALUE;
    int _temp_map__12310 = NOVALUE;
    int _from__12311 = NOVALUE;
    int _7051 = NOVALUE;
    int _7050 = NOVALUE;
    int _7049 = NOVALUE;
    int _7048 = NOVALUE;
    int _7046 = NOVALUE;
    int _7044 = NOVALUE;
    int _7042 = NOVALUE;
    int _7040 = NOVALUE;
    int _7039 = NOVALUE;
    int _7037 = NOVALUE;
    int _7034 = NOVALUE;
    int _7033 = NOVALUE;
    int _7032 = NOVALUE;
    int _7031 = NOVALUE;
    int _7030 = NOVALUE;
    int _7029 = NOVALUE;
    int _7028 = NOVALUE;
    int _7027 = NOVALUE;
    int _7026 = NOVALUE;
    int _7025 = NOVALUE;
    int _7024 = NOVALUE;
    int _7023 = NOVALUE;
    int _7022 = NOVALUE;
    int _7020 = NOVALUE;
    int _7019 = NOVALUE;
    int _7018 = NOVALUE;
    int _7017 = NOVALUE;
    int _7016 = NOVALUE;
    int _7015 = NOVALUE;
    int _7014 = NOVALUE;
    int _7013 = NOVALUE;
    int _7012 = NOVALUE;
    int _7011 = NOVALUE;
    int _7010 = NOVALUE;
    int _7008 = NOVALUE;
    int _7006 = NOVALUE;
    int _7004 = NOVALUE;
    int _7003 = NOVALUE;
    int _7002 = NOVALUE;
    int _7000 = NOVALUE;
    int _6999 = NOVALUE;
    int _6998 = NOVALUE;
    int _6997 = NOVALUE;
    int _6996 = NOVALUE;
    int _6993 = NOVALUE;
    int _6992 = NOVALUE;
    int _6990 = NOVALUE;
    int _6989 = NOVALUE;
    int _6987 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12310);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12306)){
        _temp_map__12310 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12306)->dbl));
    }
    else{
        _temp_map__12310 = (int)*(((s1_ptr)_2)->base + _the_map_p_12306);
    }
    Ref(_temp_map__12310);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12306))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12306)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12306);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _6987 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6987, 76)){
        _6987 = NOVALUE;
        goto L1; // [27] 318
    }
    _6987 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _6989 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6989)){
            _6990 = SEQ_PTR(_6989)->length;
    }
    else {
        _6990 = 1;
    }
    _6989 = NOVALUE;
    Ref(_the_key_p_12307);
    _bucket__12309 = _26calc_hash(_the_key_p_12307, _6990);
    _6990 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12309)) {
        _1 = (long)(DBL_PTR(_bucket__12309)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12309)) && (DBL_PTR(_bucket__12309)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12309);
        _bucket__12309 = _1;
    }

    /** 		index_ = find(the_key_p, temp_map_[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _6992 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6992);
    _6993 = (int)*(((s1_ptr)_2)->base + _bucket__12309);
    _6992 = NOVALUE;
    _index__12308 = find_from(_the_key_p_12307, _6993, 1);
    _6993 = NOVALUE;

    /** 		if index_ != 0 then*/
    if (_index__12308 == 0)
    goto L2; // [72] 464

    /** 			temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _6996 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6996)) {
        _6997 = _6996 - 1;
        if ((long)((unsigned long)_6997 +(unsigned long) HIGH_BITS) >= 0){
            _6997 = NewDouble((double)_6997);
        }
    }
    else {
        _6997 = binary_op(MINUS, _6996, 1);
    }
    _6996 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _6997;
    if( _1 != _6997 ){
        DeRef(_1);
    }
    _6997 = NOVALUE;

    /** 			if length(temp_map_[KEY_BUCKETS][bucket_]) = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _6998 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6998);
    _6999 = (int)*(((s1_ptr)_2)->base + _bucket__12309);
    _6998 = NOVALUE;
    if (IS_SEQUENCE(_6999)){
            _7000 = SEQ_PTR(_6999)->length;
    }
    else {
        _7000 = 1;
    }
    _6999 = NOVALUE;
    if (_7000 != 1)
    goto L3; // [107] 156

    /** 				temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7002 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7002)) {
        _7003 = _7002 - 1;
        if ((long)((unsigned long)_7003 +(unsigned long) HIGH_BITS) >= 0){
            _7003 = NewDouble((double)_7003);
        }
    }
    else {
        _7003 = binary_op(MINUS, _7002, 1);
    }
    _7002 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7003;
    if( _1 != _7003 ){
        DeRef(_1);
    }
    _7003 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12309);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7004 = NOVALUE;

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12309);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7006 = NOVALUE;
    goto L4; // [153] 273
L3: 

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = temp_map_[VALUE_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7010 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7010);
    _7011 = (int)*(((s1_ptr)_2)->base + _bucket__12309);
    _7010 = NOVALUE;
    _7012 = _index__12308 - 1;
    rhs_slice_target = (object_ptr)&_7013;
    RHS_Slice(_7011, 1, _7012);
    _7011 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7014 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7014);
    _7015 = (int)*(((s1_ptr)_2)->base + _bucket__12309);
    _7014 = NOVALUE;
    _7016 = _index__12308 + 1;
    if (_7016 > MAXINT){
        _7016 = NewDouble((double)_7016);
    }
    if (IS_SEQUENCE(_7015)){
            _7017 = SEQ_PTR(_7015)->length;
    }
    else {
        _7017 = 1;
    }
    rhs_slice_target = (object_ptr)&_7018;
    RHS_Slice(_7015, _7016, _7017);
    _7015 = NOVALUE;
    Concat((object_ptr)&_7019, _7013, _7018);
    DeRefDS(_7013);
    _7013 = NOVALUE;
    DeRef(_7013);
    _7013 = NOVALUE;
    DeRefDS(_7018);
    _7018 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12309);
    _1 = *(int *)_2;
    *(int *)_2 = _7019;
    if( _1 != _7019 ){
        DeRef(_1);
    }
    _7019 = NOVALUE;
    _7008 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = temp_map_[KEY_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7022 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7022);
    _7023 = (int)*(((s1_ptr)_2)->base + _bucket__12309);
    _7022 = NOVALUE;
    _7024 = _index__12308 - 1;
    rhs_slice_target = (object_ptr)&_7025;
    RHS_Slice(_7023, 1, _7024);
    _7023 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7026 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7026);
    _7027 = (int)*(((s1_ptr)_2)->base + _bucket__12309);
    _7026 = NOVALUE;
    _7028 = _index__12308 + 1;
    if (_7028 > MAXINT){
        _7028 = NewDouble((double)_7028);
    }
    if (IS_SEQUENCE(_7027)){
            _7029 = SEQ_PTR(_7027)->length;
    }
    else {
        _7029 = 1;
    }
    rhs_slice_target = (object_ptr)&_7030;
    RHS_Slice(_7027, _7028, _7029);
    _7027 = NOVALUE;
    Concat((object_ptr)&_7031, _7025, _7030);
    DeRefDS(_7025);
    _7025 = NOVALUE;
    DeRef(_7025);
    _7025 = NOVALUE;
    DeRefDS(_7030);
    _7030 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12309);
    _1 = *(int *)_2;
    *(int *)_2 = _7031;
    if( _1 != _7031 ){
        DeRef(_1);
    }
    _7031 = NOVALUE;
    _7020 = NOVALUE;
L4: 

    /** 			if temp_map_[ELEMENT_COUNT] < floor(51 * threshold_size / 100) then*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7032 = (int)*(((s1_ptr)_2)->base + 2);
    if (_26threshold_size_11724 <= INT15 && _26threshold_size_11724 >= -INT15)
    _7033 = 51 * _26threshold_size_11724;
    else
    _7033 = NewDouble(51 * (double)_26threshold_size_11724);
    if (IS_ATOM_INT(_7033)) {
        if (100 > 0 && _7033 >= 0) {
            _7034 = _7033 / 100;
        }
        else {
            temp_dbl = floor((double)_7033 / (double)100);
            if (_7033 != MININT)
            _7034 = (long)temp_dbl;
            else
            _7034 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7033, 100);
        _7034 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_7033);
    _7033 = NOVALUE;
    if (binary_op_a(GREATEREQ, _7032, _7034)){
        _7032 = NOVALUE;
        DeRef(_7034);
        _7034 = NOVALUE;
        goto L2; // [291] 464
    }
    _7032 = NOVALUE;
    DeRef(_7034);
    _7034 = NOVALUE;

    /** 				eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12310);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12306))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12306)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12306);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12310;
    DeRef(_1);

    /** 				convert_to_small_map(the_map_p)*/
    Ref(_the_map_p_12306);
    _26convert_to_small_map(_the_map_p_12306);

    /** 				return*/
    DeRef(_the_map_p_12306);
    DeRef(_the_key_p_12307);
    DeRefDS(_temp_map__12310);
    _6989 = NOVALUE;
    _6999 = NOVALUE;
    DeRef(_7012);
    _7012 = NOVALUE;
    DeRef(_7024);
    _7024 = NOVALUE;
    DeRef(_7016);
    _7016 = NOVALUE;
    DeRef(_7028);
    _7028 = NOVALUE;
    return;
    goto L2; // [315] 464
L1: 

    /** 		from_ = 1*/
    _from__12311 = 1;

    /** 		while from_ > 0 do*/
L5: 
    if (_from__12311 <= 0)
    goto L6; // [330] 463

    /** 			index_ = find(the_key_p, temp_map_[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7037 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12308 = find_from(_the_key_p_12307, _7037, _from__12311);
    _7037 = NOVALUE;

    /** 			if index_ then*/
    if (_index__12308 == 0)
    {
        goto L6; // [351] 463
    }
    else{
    }

    /** 				if temp_map_[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7039 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7039);
    _7040 = (int)*(((s1_ptr)_2)->base + _index__12308);
    _7039 = NOVALUE;
    if (binary_op_a(NOTEQ, _7040, 1)){
        _7040 = NOVALUE;
        goto L7; // [366] 450
    }
    _7040 = NOVALUE;

    /** 					temp_map_[FREE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12308);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7042 = NOVALUE;

    /** 					temp_map_[KEY_LIST][index_] = init_small_map_key*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_26init_small_map_key_11725);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12308);
    _1 = *(int *)_2;
    *(int *)_2 = _26init_small_map_key_11725;
    DeRef(_1);
    _7044 = NOVALUE;

    /** 					temp_map_[VALUE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12308);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7046 = NOVALUE;

    /** 					temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7048 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7048)) {
        _7049 = _7048 - 1;
        if ((long)((unsigned long)_7049 +(unsigned long) HIGH_BITS) >= 0){
            _7049 = NewDouble((double)_7049);
        }
    }
    else {
        _7049 = binary_op(MINUS, _7048, 1);
    }
    _7048 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7049;
    if( _1 != _7049 ){
        DeRef(_1);
    }
    _7049 = NOVALUE;

    /** 					temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12310);
    _7050 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7050)) {
        _7051 = _7050 - 1;
        if ((long)((unsigned long)_7051 +(unsigned long) HIGH_BITS) >= 0){
            _7051 = NewDouble((double)_7051);
        }
    }
    else {
        _7051 = binary_op(MINUS, _7050, 1);
    }
    _7050 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12310);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12310 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7051;
    if( _1 != _7051 ){
        DeRef(_1);
    }
    _7051 = NOVALUE;
    goto L7; // [442] 450

    /** 				exit*/
    goto L6; // [447] 463
L7: 

    /** 			from_ = index_ + 1*/
    _from__12311 = _index__12308 + 1;

    /** 		end while*/
    goto L5; // [460] 330
L6: 
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12310);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12306))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12306)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12306);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12310;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_12306);
    DeRef(_the_key_p_12307);
    DeRefDS(_temp_map__12310);
    _6989 = NOVALUE;
    _6999 = NOVALUE;
    DeRef(_7012);
    _7012 = NOVALUE;
    DeRef(_7024);
    _7024 = NOVALUE;
    DeRef(_7016);
    _7016 = NOVALUE;
    DeRef(_7028);
    _7028 = NOVALUE;
    return;
    ;
}


void _26clear(int _the_map_p_12392)
{
    int _temp_map__12393 = NOVALUE;
    int _7070 = NOVALUE;
    int _7069 = NOVALUE;
    int _7068 = NOVALUE;
    int _7067 = NOVALUE;
    int _7066 = NOVALUE;
    int _7065 = NOVALUE;
    int _7064 = NOVALUE;
    int _7063 = NOVALUE;
    int _7062 = NOVALUE;
    int _7061 = NOVALUE;
    int _7060 = NOVALUE;
    int _7059 = NOVALUE;
    int _7058 = NOVALUE;
    int _7057 = NOVALUE;
    int _7056 = NOVALUE;
    int _7054 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12393);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12392)){
        _temp_map__12393 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12392)->dbl));
    }
    else{
        _temp_map__12393 = (int)*(((s1_ptr)_2)->base + _the_map_p_12392);
    }
    Ref(_temp_map__12393);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    _7054 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7054, 76)){
        _7054 = NOVALUE;
        goto L1; // [19] 84
    }
    _7054 = NOVALUE;

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_BUCKETS] = repeat({}, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    _7056 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7056)){
            _7057 = SEQ_PTR(_7056)->length;
    }
    else {
        _7057 = 1;
    }
    _7056 = NOVALUE;
    _7058 = Repeat(_5, _7057);
    _7057 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7058;
    if( _1 != _7058 ){
        DeRef(_1);
    }
    _7058 = NOVALUE;

    /** 		temp_map_[VALUE_BUCKETS] = repeat({}, length(temp_map_[VALUE_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    _7059 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7059)){
            _7060 = SEQ_PTR(_7059)->length;
    }
    else {
        _7060 = 1;
    }
    _7059 = NOVALUE;
    _7061 = Repeat(_5, _7060);
    _7060 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7061;
    if( _1 != _7061 ){
        DeRef(_1);
    }
    _7061 = NOVALUE;
    goto L2; // [81] 164
L1: 

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_LIST] = repeat(init_small_map_key, length(temp_map_[KEY_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    _7062 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7062)){
            _7063 = SEQ_PTR(_7062)->length;
    }
    else {
        _7063 = 1;
    }
    _7062 = NOVALUE;
    _7064 = Repeat(_26init_small_map_key_11725, _7063);
    _7063 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7064;
    if( _1 != _7064 ){
        DeRef(_1);
    }
    _7064 = NOVALUE;

    /** 		temp_map_[VALUE_LIST] = repeat(0, length(temp_map_[VALUE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    _7065 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7065)){
            _7066 = SEQ_PTR(_7065)->length;
    }
    else {
        _7066 = 1;
    }
    _7065 = NOVALUE;
    _7067 = Repeat(0, _7066);
    _7066 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7067;
    if( _1 != _7067 ){
        DeRef(_1);
    }
    _7067 = NOVALUE;

    /** 		temp_map_[FREE_LIST] = repeat(0, length(temp_map_[FREE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12393);
    _7068 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7068)){
            _7069 = SEQ_PTR(_7068)->length;
    }
    else {
        _7069 = 1;
    }
    _7068 = NOVALUE;
    _7070 = Repeat(0, _7069);
    _7069 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _7070;
    if( _1 != _7070 ){
        DeRef(_1);
    }
    _7070 = NOVALUE;
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12393);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12392))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12392)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12392);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12393;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_12392);
    DeRefDS(_temp_map__12393);
    _7056 = NOVALUE;
    _7059 = NOVALUE;
    _7062 = NOVALUE;
    _7065 = NOVALUE;
    _7068 = NOVALUE;
    return;
    ;
}


int _26size(int _the_map_p_12416)
{
    int _7072 = NOVALUE;
    int _7071 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12416)){
        _7071 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12416)->dbl));
    }
    else{
        _7071 = (int)*(((s1_ptr)_2)->base + _the_map_p_12416);
    }
    _2 = (int)SEQ_PTR(_7071);
    _7072 = (int)*(((s1_ptr)_2)->base + 2);
    _7071 = NOVALUE;
    Ref(_7072);
    DeRef(_the_map_p_12416);
    return _7072;
    ;
}


int _26statistics(int _the_map_p_12434)
{
    int _statistic_set__12435 = NOVALUE;
    int _lengths__12436 = NOVALUE;
    int _length__12437 = NOVALUE;
    int _temp_map__12438 = NOVALUE;
    int _7109 = NOVALUE;
    int _7108 = NOVALUE;
    int _7107 = NOVALUE;
    int _7106 = NOVALUE;
    int _7105 = NOVALUE;
    int _7104 = NOVALUE;
    int _7103 = NOVALUE;
    int _7102 = NOVALUE;
    int _7101 = NOVALUE;
    int _7100 = NOVALUE;
    int _7099 = NOVALUE;
    int _7098 = NOVALUE;
    int _7095 = NOVALUE;
    int _7093 = NOVALUE;
    int _7090 = NOVALUE;
    int _7089 = NOVALUE;
    int _7088 = NOVALUE;
    int _7087 = NOVALUE;
    int _7085 = NOVALUE;
    int _7084 = NOVALUE;
    int _7083 = NOVALUE;
    int _7082 = NOVALUE;
    int _7080 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12438);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12434)){
        _temp_map__12438 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12434)->dbl));
    }
    else{
        _temp_map__12438 = (int)*(((s1_ptr)_2)->base + _the_map_p_12434);
    }
    Ref(_temp_map__12438);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7080 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7080, 76)){
        _7080 = NOVALUE;
        goto L1; // [19] 196
    }
    _7080 = NOVALUE;

    /** 		statistic_set_ = { */
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7082 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7083 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7084 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7084)){
            _7085 = SEQ_PTR(_7084)->length;
    }
    else {
        _7085 = 1;
    }
    _7084 = NOVALUE;
    _0 = _statistic_set__12435;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7082);
    *((int *)(_2+4)) = _7082;
    Ref(_7083);
    *((int *)(_2+8)) = _7083;
    *((int *)(_2+12)) = _7085;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 1073741823;
    *((int *)(_2+24)) = 0;
    *((int *)(_2+28)) = 0;
    _statistic_set__12435 = MAKE_SEQ(_1);
    DeRef(_0);
    _7085 = NOVALUE;
    _7083 = NOVALUE;
    _7082 = NOVALUE;

    /** 		lengths_ = {}*/
    RefDS(_5);
    DeRefi(_lengths__12436);
    _lengths__12436 = _5;

    /** 		for i = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7087 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7087)){
            _7088 = SEQ_PTR(_7087)->length;
    }
    else {
        _7088 = 1;
    }
    _7087 = NOVALUE;
    {
        int _i_12449;
        _i_12449 = 1;
L2: 
        if (_i_12449 > _7088){
            goto L3; // [74] 160
        }

        /** 			length_ = length(temp_map_[KEY_BUCKETS][i])*/
        _2 = (int)SEQ_PTR(_temp_map__12438);
        _7089 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7089);
        _7090 = (int)*(((s1_ptr)_2)->base + _i_12449);
        _7089 = NOVALUE;
        if (IS_SEQUENCE(_7090)){
                _length__12437 = SEQ_PTR(_7090)->length;
        }
        else {
            _length__12437 = 1;
        }
        _7090 = NOVALUE;

        /** 			if length_ > 0 then*/
        if (_length__12437 <= 0)
        goto L4; // [100] 153

        /** 				if length_ > statistic_set_[LARGEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__12435);
        _7093 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(LESSEQ, _length__12437, _7093)){
            _7093 = NOVALUE;
            goto L5; // [112] 125
        }
        _7093 = NOVALUE;

        /** 					statistic_set_[LARGEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__12435);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__12435 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 4);
        _1 = *(int *)_2;
        *(int *)_2 = _length__12437;
        DeRef(_1);
L5: 

        /** 				if length_ < statistic_set_[SMALLEST_BUCKET] then*/
        _2 = (int)SEQ_PTR(_statistic_set__12435);
        _7095 = (int)*(((s1_ptr)_2)->base + 5);
        if (binary_op_a(GREATEREQ, _length__12437, _7095)){
            _7095 = NOVALUE;
            goto L6; // [133] 146
        }
        _7095 = NOVALUE;

        /** 					statistic_set_[SMALLEST_BUCKET] = length_*/
        _2 = (int)SEQ_PTR(_statistic_set__12435);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _statistic_set__12435 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 5);
        _1 = *(int *)_2;
        *(int *)_2 = _length__12437;
        DeRef(_1);
L6: 

        /** 				lengths_ &= length_*/
        Append(&_lengths__12436, _lengths__12436, _length__12437);
L4: 

        /** 		end for*/
        _i_12449 = _i_12449 + 1;
        goto L2; // [155] 81
L3: 
        ;
    }

    /** 		statistic_set_[AVERAGE_BUCKET] = stats:average(lengths_)*/
    RefDS(_lengths__12436);
    _7098 = _29average(_lengths__12436, 1);
    _2 = (int)SEQ_PTR(_statistic_set__12435);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__12435 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7098;
    if( _1 != _7098 ){
        DeRef(_1);
    }
    _7098 = NOVALUE;

    /** 		statistic_set_[STDEV_BUCKET] = stats:stdev(lengths_)*/
    RefDS(_lengths__12436);
    _7099 = _29stdev(_lengths__12436, 1, 2);
    _2 = (int)SEQ_PTR(_statistic_set__12435);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _statistic_set__12435 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _7099;
    if( _1 != _7099 ){
        DeRef(_1);
    }
    _7099 = NOVALUE;
    goto L7; // [193] 257
L1: 

    /** 		statistic_set_ = {*/
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7100 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7101 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7102 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7102)){
            _7103 = SEQ_PTR(_7102)->length;
    }
    else {
        _7103 = 1;
    }
    _7102 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7104 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7104)){
            _7105 = SEQ_PTR(_7104)->length;
    }
    else {
        _7105 = 1;
    }
    _7104 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7106 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7106)){
            _7107 = SEQ_PTR(_7106)->length;
    }
    else {
        _7107 = 1;
    }
    _7106 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12438);
    _7108 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7108)){
            _7109 = SEQ_PTR(_7108)->length;
    }
    else {
        _7109 = 1;
    }
    _7108 = NOVALUE;
    _0 = _statistic_set__12435;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_7100);
    *((int *)(_2+4)) = _7100;
    Ref(_7101);
    *((int *)(_2+8)) = _7101;
    *((int *)(_2+12)) = _7103;
    *((int *)(_2+16)) = _7105;
    *((int *)(_2+20)) = _7107;
    *((int *)(_2+24)) = _7109;
    *((int *)(_2+28)) = 0;
    _statistic_set__12435 = MAKE_SEQ(_1);
    DeRef(_0);
    _7109 = NOVALUE;
    _7107 = NOVALUE;
    _7105 = NOVALUE;
    _7103 = NOVALUE;
    _7101 = NOVALUE;
    _7100 = NOVALUE;
L7: 

    /** 	return statistic_set_*/
    DeRef(_the_map_p_12434);
    DeRefi(_lengths__12436);
    DeRef(_temp_map__12438);
    _7087 = NOVALUE;
    _7090 = NOVALUE;
    _7084 = NOVALUE;
    _7102 = NOVALUE;
    _7104 = NOVALUE;
    _7106 = NOVALUE;
    _7108 = NOVALUE;
    return _statistic_set__12435;
    ;
}


int _26keys(int _the_map_p_12480, int _sorted_result_12481)
{
    int _buckets__12482 = NOVALUE;
    int _current_bucket__12483 = NOVALUE;
    int _results__12484 = NOVALUE;
    int _pos__12485 = NOVALUE;
    int _temp_map__12486 = NOVALUE;
    int _7134 = NOVALUE;
    int _7132 = NOVALUE;
    int _7131 = NOVALUE;
    int _7129 = NOVALUE;
    int _7128 = NOVALUE;
    int _7127 = NOVALUE;
    int _7126 = NOVALUE;
    int _7124 = NOVALUE;
    int _7123 = NOVALUE;
    int _7122 = NOVALUE;
    int _7121 = NOVALUE;
    int _7119 = NOVALUE;
    int _7117 = NOVALUE;
    int _7114 = NOVALUE;
    int _7112 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sorted_result_12481)) {
        _1 = (long)(DBL_PTR(_sorted_result_12481)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_12481)) && (DBL_PTR(_sorted_result_12481)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_12481);
        _sorted_result_12481 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12486);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12480)){
        _temp_map__12486 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12480)->dbl));
    }
    else{
        _temp_map__12486 = (int)*(((s1_ptr)_2)->base + _the_map_p_12480);
    }
    Ref(_temp_map__12486);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__12486);
    _7112 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12484);
    _results__12484 = Repeat(0, _7112);
    _7112 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12485 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12486);
    _7114 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7114, 76)){
        _7114 = NOVALUE;
        goto L1; // [42] 125
    }
    _7114 = NOVALUE;

    /** 		buckets_ = temp_map_[KEY_BUCKETS]*/
    DeRef(_buckets__12482);
    _2 = (int)SEQ_PTR(_temp_map__12486);
    _buckets__12482 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_buckets__12482);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__12482)){
            _7117 = SEQ_PTR(_buckets__12482)->length;
    }
    else {
        _7117 = 1;
    }
    {
        int _index_12495;
        _index_12495 = 1;
L2: 
        if (_index_12495 > _7117){
            goto L3; // [61] 122
        }

        /** 			current_bucket_ = buckets_[index]*/
        DeRef(_current_bucket__12483);
        _2 = (int)SEQ_PTR(_buckets__12482);
        _current_bucket__12483 = (int)*(((s1_ptr)_2)->base + _index_12495);
        Ref(_current_bucket__12483);

        /** 			if length(current_bucket_) > 0 then*/
        if (IS_SEQUENCE(_current_bucket__12483)){
                _7119 = SEQ_PTR(_current_bucket__12483)->length;
        }
        else {
            _7119 = 1;
        }
        if (_7119 <= 0)
        goto L4; // [81] 115

        /** 				results_[pos_ .. pos_ + length(current_bucket_) - 1] = current_bucket_*/
        if (IS_SEQUENCE(_current_bucket__12483)){
                _7121 = SEQ_PTR(_current_bucket__12483)->length;
        }
        else {
            _7121 = 1;
        }
        _7122 = _pos__12485 + _7121;
        if ((long)((unsigned long)_7122 + (unsigned long)HIGH_BITS) >= 0) 
        _7122 = NewDouble((double)_7122);
        _7121 = NOVALUE;
        if (IS_ATOM_INT(_7122)) {
            _7123 = _7122 - 1;
        }
        else {
            _7123 = NewDouble(DBL_PTR(_7122)->dbl - (double)1);
        }
        DeRef(_7122);
        _7122 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__12484;
        AssignSlice(_pos__12485, _7123, _current_bucket__12483);
        DeRef(_7123);
        _7123 = NOVALUE;

        /** 				pos_ += length(current_bucket_)*/
        if (IS_SEQUENCE(_current_bucket__12483)){
                _7124 = SEQ_PTR(_current_bucket__12483)->length;
        }
        else {
            _7124 = 1;
        }
        _pos__12485 = _pos__12485 + _7124;
        _7124 = NOVALUE;
L4: 

        /** 		end for*/
        _index_12495 = _index_12495 + 1;
        goto L2; // [117] 68
L3: 
        ;
    }
    goto L5; // [122] 192
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12486);
    _7126 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7126)){
            _7127 = SEQ_PTR(_7126)->length;
    }
    else {
        _7127 = 1;
    }
    _7126 = NOVALUE;
    {
        int _index_12508;
        _index_12508 = 1;
L6: 
        if (_index_12508 > _7127){
            goto L7; // [136] 191
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12486);
        _7128 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7128);
        _7129 = (int)*(((s1_ptr)_2)->base + _index_12508);
        _7128 = NOVALUE;
        if (binary_op_a(EQUALS, _7129, 0)){
            _7129 = NOVALUE;
            goto L8; // [155] 184
        }
        _7129 = NOVALUE;

        /** 				results_[pos_] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12486);
        _7131 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7131);
        _7132 = (int)*(((s1_ptr)_2)->base + _index_12508);
        _7131 = NOVALUE;
        Ref(_7132);
        _2 = (int)SEQ_PTR(_results__12484);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12484 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__12485);
        _1 = *(int *)_2;
        *(int *)_2 = _7132;
        if( _1 != _7132 ){
            DeRef(_1);
        }
        _7132 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12485 = _pos__12485 + 1;
L8: 

        /** 		end for*/
        _index_12508 = _index_12508 + 1;
        goto L6; // [186] 143
L7: 
        ;
    }
L5: 

    /** 	if sorted_result then*/
    if (_sorted_result_12481 == 0)
    {
        goto L9; // [194] 211
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__12484);
    _7134 = _22sort(_results__12484, 1);
    DeRef(_the_map_p_12480);
    DeRef(_buckets__12482);
    DeRef(_current_bucket__12483);
    DeRefDS(_results__12484);
    DeRef(_temp_map__12486);
    _7126 = NOVALUE;
    return _7134;
    goto LA; // [208] 218
L9: 

    /** 		return results_*/
    DeRef(_the_map_p_12480);
    DeRef(_buckets__12482);
    DeRef(_current_bucket__12483);
    DeRef(_temp_map__12486);
    _7126 = NOVALUE;
    DeRef(_7134);
    _7134 = NOVALUE;
    return _results__12484;
LA: 
    ;
}


int _26values(int _the_map_12523, int _keys_12524, int _default_values_12525)
{
    int _buckets__12549 = NOVALUE;
    int _bucket__12550 = NOVALUE;
    int _results__12551 = NOVALUE;
    int _pos__12552 = NOVALUE;
    int _temp_map__12553 = NOVALUE;
    int _7174 = NOVALUE;
    int _7173 = NOVALUE;
    int _7171 = NOVALUE;
    int _7170 = NOVALUE;
    int _7169 = NOVALUE;
    int _7168 = NOVALUE;
    int _7166 = NOVALUE;
    int _7165 = NOVALUE;
    int _7164 = NOVALUE;
    int _7163 = NOVALUE;
    int _7161 = NOVALUE;
    int _7159 = NOVALUE;
    int _7156 = NOVALUE;
    int _7154 = NOVALUE;
    int _7152 = NOVALUE;
    int _7151 = NOVALUE;
    int _7150 = NOVALUE;
    int _7149 = NOVALUE;
    int _7147 = NOVALUE;
    int _7146 = NOVALUE;
    int _7145 = NOVALUE;
    int _7144 = NOVALUE;
    int _7143 = NOVALUE;
    int _7142 = NOVALUE;
    int _7140 = NOVALUE;
    int _7139 = NOVALUE;
    int _7137 = NOVALUE;
    int _7136 = NOVALUE;
    int _7135 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(keys) then*/
    _7135 = IS_SEQUENCE(_keys_12524);
    if (_7135 == 0)
    {
        _7135 = NOVALUE;
        goto L1; // [6] 116
    }
    else{
        _7135 = NOVALUE;
    }

    /** 		if atom(default_values) then*/
    _7136 = IS_ATOM(_default_values_12525);
    if (_7136 == 0)
    {
        _7136 = NOVALUE;
        goto L2; // [14] 29
    }
    else{
        _7136 = NOVALUE;
    }

    /** 			default_values = repeat(default_values, length(keys))*/
    if (IS_SEQUENCE(_keys_12524)){
            _7137 = SEQ_PTR(_keys_12524)->length;
    }
    else {
        _7137 = 1;
    }
    _0 = _default_values_12525;
    _default_values_12525 = Repeat(_default_values_12525, _7137);
    DeRef(_0);
    _7137 = NOVALUE;
    goto L3; // [26] 70
L2: 

    /** 		elsif length(default_values) < length(keys) then*/
    if (IS_SEQUENCE(_default_values_12525)){
            _7139 = SEQ_PTR(_default_values_12525)->length;
    }
    else {
        _7139 = 1;
    }
    if (IS_SEQUENCE(_keys_12524)){
            _7140 = SEQ_PTR(_keys_12524)->length;
    }
    else {
        _7140 = 1;
    }
    if (_7139 >= _7140)
    goto L4; // [37] 69

    /** 			default_values &= repeat(default_values[$], length(keys) - length(default_values))*/
    if (IS_SEQUENCE(_default_values_12525)){
            _7142 = SEQ_PTR(_default_values_12525)->length;
    }
    else {
        _7142 = 1;
    }
    _2 = (int)SEQ_PTR(_default_values_12525);
    _7143 = (int)*(((s1_ptr)_2)->base + _7142);
    if (IS_SEQUENCE(_keys_12524)){
            _7144 = SEQ_PTR(_keys_12524)->length;
    }
    else {
        _7144 = 1;
    }
    if (IS_SEQUENCE(_default_values_12525)){
            _7145 = SEQ_PTR(_default_values_12525)->length;
    }
    else {
        _7145 = 1;
    }
    _7146 = _7144 - _7145;
    _7144 = NOVALUE;
    _7145 = NOVALUE;
    _7147 = Repeat(_7143, _7146);
    _7143 = NOVALUE;
    _7146 = NOVALUE;
    if (IS_SEQUENCE(_default_values_12525) && IS_ATOM(_7147)) {
    }
    else if (IS_ATOM(_default_values_12525) && IS_SEQUENCE(_7147)) {
        Ref(_default_values_12525);
        Prepend(&_default_values_12525, _7147, _default_values_12525);
    }
    else {
        Concat((object_ptr)&_default_values_12525, _default_values_12525, _7147);
    }
    DeRefDS(_7147);
    _7147 = NOVALUE;
L4: 
L3: 

    /** 		for i = 1 to length(keys) do*/
    if (IS_SEQUENCE(_keys_12524)){
            _7149 = SEQ_PTR(_keys_12524)->length;
    }
    else {
        _7149 = 1;
    }
    {
        int _i_12544;
        _i_12544 = 1;
L5: 
        if (_i_12544 > _7149){
            goto L6; // [75] 109
        }

        /** 			keys[i] = get(the_map, keys[i], default_values[i])*/
        _2 = (int)SEQ_PTR(_keys_12524);
        _7150 = (int)*(((s1_ptr)_2)->base + _i_12544);
        _2 = (int)SEQ_PTR(_default_values_12525);
        _7151 = (int)*(((s1_ptr)_2)->base + _i_12544);
        Ref(_the_map_12523);
        Ref(_7150);
        Ref(_7151);
        _7152 = _26get(_the_map_12523, _7150, _7151);
        _7150 = NOVALUE;
        _7151 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys_12524);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys_12524 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12544);
        _1 = *(int *)_2;
        *(int *)_2 = _7152;
        if( _1 != _7152 ){
            DeRef(_1);
        }
        _7152 = NOVALUE;

        /** 		end for*/
        _i_12544 = _i_12544 + 1;
        goto L5; // [104] 82
L6: 
        ;
    }

    /** 		return keys*/
    DeRef(_the_map_12523);
    DeRef(_default_values_12525);
    DeRef(_buckets__12549);
    DeRef(_bucket__12550);
    DeRef(_results__12551);
    DeRef(_temp_map__12553);
    return _keys_12524;
L1: 

    /** 	sequence buckets_*/

    /** 	sequence bucket_*/

    /** 	sequence results_*/

    /** 	integer pos_*/

    /** 	sequence temp_map_*/

    /** 	temp_map_ = eumem:ram_space[the_map]*/
    DeRef(_temp_map__12553);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_12523)){
        _temp_map__12553 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_12523)->dbl));
    }
    else{
        _temp_map__12553 = (int)*(((s1_ptr)_2)->base + _the_map_12523);
    }
    Ref(_temp_map__12553);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__12553);
    _7154 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12551);
    _results__12551 = Repeat(0, _7154);
    _7154 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12552 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12553);
    _7156 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7156, 76)){
        _7156 = NOVALUE;
        goto L7; // [163] 246
    }
    _7156 = NOVALUE;

    /** 		buckets_ = temp_map_[VALUE_BUCKETS]*/
    DeRef(_buckets__12549);
    _2 = (int)SEQ_PTR(_temp_map__12553);
    _buckets__12549 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_buckets__12549);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__12549)){
            _7159 = SEQ_PTR(_buckets__12549)->length;
    }
    else {
        _7159 = 1;
    }
    {
        int _index_12562;
        _index_12562 = 1;
L8: 
        if (_index_12562 > _7159){
            goto L9; // [182] 243
        }

        /** 			bucket_ = buckets_[index]*/
        DeRef(_bucket__12550);
        _2 = (int)SEQ_PTR(_buckets__12549);
        _bucket__12550 = (int)*(((s1_ptr)_2)->base + _index_12562);
        Ref(_bucket__12550);

        /** 			if length(bucket_) > 0 then*/
        if (IS_SEQUENCE(_bucket__12550)){
                _7161 = SEQ_PTR(_bucket__12550)->length;
        }
        else {
            _7161 = 1;
        }
        if (_7161 <= 0)
        goto LA; // [202] 236

        /** 				results_[pos_ .. pos_ + length(bucket_) - 1] = bucket_*/
        if (IS_SEQUENCE(_bucket__12550)){
                _7163 = SEQ_PTR(_bucket__12550)->length;
        }
        else {
            _7163 = 1;
        }
        _7164 = _pos__12552 + _7163;
        if ((long)((unsigned long)_7164 + (unsigned long)HIGH_BITS) >= 0) 
        _7164 = NewDouble((double)_7164);
        _7163 = NOVALUE;
        if (IS_ATOM_INT(_7164)) {
            _7165 = _7164 - 1;
        }
        else {
            _7165 = NewDouble(DBL_PTR(_7164)->dbl - (double)1);
        }
        DeRef(_7164);
        _7164 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__12551;
        AssignSlice(_pos__12552, _7165, _bucket__12550);
        DeRef(_7165);
        _7165 = NOVALUE;

        /** 				pos_ += length(bucket_)*/
        if (IS_SEQUENCE(_bucket__12550)){
                _7166 = SEQ_PTR(_bucket__12550)->length;
        }
        else {
            _7166 = 1;
        }
        _pos__12552 = _pos__12552 + _7166;
        _7166 = NOVALUE;
LA: 

        /** 		end for*/
        _index_12562 = _index_12562 + 1;
        goto L8; // [238] 189
L9: 
        ;
    }
    goto LB; // [243] 313
L7: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12553);
    _7168 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7168)){
            _7169 = SEQ_PTR(_7168)->length;
    }
    else {
        _7169 = 1;
    }
    _7168 = NOVALUE;
    {
        int _index_12575;
        _index_12575 = 1;
LC: 
        if (_index_12575 > _7169){
            goto LD; // [257] 312
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12553);
        _7170 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7170);
        _7171 = (int)*(((s1_ptr)_2)->base + _index_12575);
        _7170 = NOVALUE;
        if (binary_op_a(EQUALS, _7171, 0)){
            _7171 = NOVALUE;
            goto LE; // [276] 305
        }
        _7171 = NOVALUE;

        /** 				results_[pos_] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12553);
        _7173 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7173);
        _7174 = (int)*(((s1_ptr)_2)->base + _index_12575);
        _7173 = NOVALUE;
        Ref(_7174);
        _2 = (int)SEQ_PTR(_results__12551);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12551 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__12552);
        _1 = *(int *)_2;
        *(int *)_2 = _7174;
        if( _1 != _7174 ){
            DeRef(_1);
        }
        _7174 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12552 = _pos__12552 + 1;
LE: 

        /** 		end for*/
        _index_12575 = _index_12575 + 1;
        goto LC; // [307] 264
LD: 
        ;
    }
LB: 

    /** 	return results_*/
    DeRef(_the_map_12523);
    DeRef(_keys_12524);
    DeRef(_default_values_12525);
    DeRef(_buckets__12549);
    DeRef(_bucket__12550);
    DeRef(_temp_map__12553);
    _7168 = NOVALUE;
    return _results__12551;
    ;
}


int _26pairs(int _the_map_p_12587, int _sorted_result_12588)
{
    int _key_bucket__12589 = NOVALUE;
    int _value_bucket__12590 = NOVALUE;
    int _results__12591 = NOVALUE;
    int _pos__12592 = NOVALUE;
    int _temp_map__12593 = NOVALUE;
    int _7210 = NOVALUE;
    int _7208 = NOVALUE;
    int _7207 = NOVALUE;
    int _7205 = NOVALUE;
    int _7204 = NOVALUE;
    int _7203 = NOVALUE;
    int _7201 = NOVALUE;
    int _7199 = NOVALUE;
    int _7198 = NOVALUE;
    int _7197 = NOVALUE;
    int _7196 = NOVALUE;
    int _7194 = NOVALUE;
    int _7192 = NOVALUE;
    int _7191 = NOVALUE;
    int _7189 = NOVALUE;
    int _7188 = NOVALUE;
    int _7186 = NOVALUE;
    int _7184 = NOVALUE;
    int _7183 = NOVALUE;
    int _7182 = NOVALUE;
    int _7180 = NOVALUE;
    int _7178 = NOVALUE;
    int _7177 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sorted_result_12588)) {
        _1 = (long)(DBL_PTR(_sorted_result_12588)->dbl);
        if (UNIQUE(DBL_PTR(_sorted_result_12588)) && (DBL_PTR(_sorted_result_12588)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sorted_result_12588);
        _sorted_result_12588 = _1;
    }

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12593);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12587)){
        _temp_map__12593 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12587)->dbl));
    }
    else{
        _temp_map__12593 = (int)*(((s1_ptr)_2)->base + _the_map_p_12587);
    }
    Ref(_temp_map__12593);

    /** 	results_ = repeat({ 0, 0 }, temp_map_[ELEMENT_COUNT])*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _7177 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _7178 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12591);
    _results__12591 = Repeat(_7177, _7178);
    DeRefDS(_7177);
    _7177 = NOVALUE;
    _7178 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12592 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _7180 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7180, 76)){
        _7180 = NOVALUE;
        goto L1; // [46] 163
    }
    _7180 = NOVALUE;

    /** 		for index = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _7182 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7182)){
            _7183 = SEQ_PTR(_7182)->length;
    }
    else {
        _7183 = 1;
    }
    _7182 = NOVALUE;
    {
        int _index_12602;
        _index_12602 = 1;
L2: 
        if (_index_12602 > _7183){
            goto L3; // [61] 160
        }

        /** 			key_bucket_ = temp_map_[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12593);
        _7184 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_key_bucket__12589);
        _2 = (int)SEQ_PTR(_7184);
        _key_bucket__12589 = (int)*(((s1_ptr)_2)->base + _index_12602);
        Ref(_key_bucket__12589);
        _7184 = NOVALUE;

        /** 			value_bucket_ = temp_map_[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12593);
        _7186 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_bucket__12590);
        _2 = (int)SEQ_PTR(_7186);
        _value_bucket__12590 = (int)*(((s1_ptr)_2)->base + _index_12602);
        Ref(_value_bucket__12590);
        _7186 = NOVALUE;

        /** 			for j = 1 to length(key_bucket_) do*/
        if (IS_SEQUENCE(_key_bucket__12589)){
                _7188 = SEQ_PTR(_key_bucket__12589)->length;
        }
        else {
            _7188 = 1;
        }
        {
            int _j_12610;
            _j_12610 = 1;
L4: 
            if (_j_12610 > _7188){
                goto L5; // [101] 153
            }

            /** 				results_[pos_][1] = key_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__12591);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__12591 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__12592 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_key_bucket__12589);
            _7191 = (int)*(((s1_ptr)_2)->base + _j_12610);
            Ref(_7191);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 1);
            _1 = *(int *)_2;
            *(int *)_2 = _7191;
            if( _1 != _7191 ){
                DeRef(_1);
            }
            _7191 = NOVALUE;
            _7189 = NOVALUE;

            /** 				results_[pos_][2] = value_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__12591);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__12591 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__12592 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_value_bucket__12590);
            _7194 = (int)*(((s1_ptr)_2)->base + _j_12610);
            Ref(_7194);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _7194;
            if( _1 != _7194 ){
                DeRef(_1);
            }
            _7194 = NOVALUE;
            _7192 = NOVALUE;

            /** 				pos_ += 1*/
            _pos__12592 = _pos__12592 + 1;

            /** 			end for*/
            _j_12610 = _j_12610 + 1;
            goto L4; // [148] 108
L5: 
            ;
        }

        /** 		end for*/
        _index_12602 = _index_12602 + 1;
        goto L2; // [155] 68
L3: 
        ;
    }
    goto L6; // [160] 256
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _7196 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7196)){
            _7197 = SEQ_PTR(_7196)->length;
    }
    else {
        _7197 = 1;
    }
    _7196 = NOVALUE;
    {
        int _index_12621;
        _index_12621 = 1;
L7: 
        if (_index_12621 > _7197){
            goto L8; // [174] 255
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12593);
        _7198 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7198);
        _7199 = (int)*(((s1_ptr)_2)->base + _index_12621);
        _7198 = NOVALUE;
        if (binary_op_a(EQUALS, _7199, 0)){
            _7199 = NOVALUE;
            goto L9; // [193] 248
        }
        _7199 = NOVALUE;

        /** 				results_[pos_][1] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__12591);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12591 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__12592 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__12593);
        _7203 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7203);
        _7204 = (int)*(((s1_ptr)_2)->base + _index_12621);
        _7203 = NOVALUE;
        Ref(_7204);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _7204;
        if( _1 != _7204 ){
            DeRef(_1);
        }
        _7204 = NOVALUE;
        _7201 = NOVALUE;

        /** 				results_[pos_][2] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__12591);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12591 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__12592 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__12593);
        _7207 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7207);
        _7208 = (int)*(((s1_ptr)_2)->base + _index_12621);
        _7207 = NOVALUE;
        Ref(_7208);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _7208;
        if( _1 != _7208 ){
            DeRef(_1);
        }
        _7208 = NOVALUE;
        _7205 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12592 = _pos__12592 + 1;
L9: 

        /** 		end for	*/
        _index_12621 = _index_12621 + 1;
        goto L7; // [250] 181
L8: 
        ;
    }
L6: 

    /** 	if sorted_result then*/
    if (_sorted_result_12588 == 0)
    {
        goto LA; // [258] 275
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__12591);
    _7210 = _22sort(_results__12591, 1);
    DeRef(_the_map_p_12587);
    DeRef(_key_bucket__12589);
    DeRef(_value_bucket__12590);
    DeRefDS(_results__12591);
    DeRef(_temp_map__12593);
    _7182 = NOVALUE;
    _7196 = NOVALUE;
    return _7210;
    goto LB; // [272] 282
LA: 

    /** 		return results_*/
    DeRef(_the_map_p_12587);
    DeRef(_key_bucket__12589);
    DeRef(_value_bucket__12590);
    DeRef(_temp_map__12593);
    _7182 = NOVALUE;
    _7196 = NOVALUE;
    DeRef(_7210);
    _7210 = NOVALUE;
    return _results__12591;
LB: 
    ;
}


void _26optimize(int _the_map_p_12642, int _max_p_12643, int _grow_p_12644)
{
    int _stats__12646 = NOVALUE;
    int _next_guess__12647 = NOVALUE;
    int _prev_guess_12648 = NOVALUE;
    int _7231 = NOVALUE;
    int _7230 = NOVALUE;
    int _7228 = NOVALUE;
    int _7227 = NOVALUE;
    int _7226 = NOVALUE;
    int _7225 = NOVALUE;
    int _7224 = NOVALUE;
    int _7222 = NOVALUE;
    int _7220 = NOVALUE;
    int _7219 = NOVALUE;
    int _7218 = NOVALUE;
    int _7217 = NOVALUE;
    int _7213 = NOVALUE;
    int _7212 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_max_p_12643)) {
        _1 = (long)(DBL_PTR(_max_p_12643)->dbl);
        if (UNIQUE(DBL_PTR(_max_p_12643)) && (DBL_PTR(_max_p_12643)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_max_p_12643);
        _max_p_12643 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12642)){
        _7212 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12642)->dbl));
    }
    else{
        _7212 = (int)*(((s1_ptr)_2)->base + _the_map_p_12642);
    }
    _2 = (int)SEQ_PTR(_7212);
    _7213 = (int)*(((s1_ptr)_2)->base + 4);
    _7212 = NOVALUE;
    if (binary_op_a(NOTEQ, _7213, 76)){
        _7213 = NOVALUE;
        goto L1; // [19] 204
    }
    _7213 = NOVALUE;

    /** 		if grow_p < 1 then*/
    if (binary_op_a(GREATEREQ, _grow_p_12644, 1)){
        goto L2; // [25] 35
    }

    /** 			grow_p = 1.333*/
    RefDS(_7211);
    DeRef(_grow_p_12644);
    _grow_p_12644 = _7211;
L2: 

    /** 		if max_p < 3 then*/
    if (_max_p_12643 >= 3)
    goto L3; // [37] 49

    /** 			max_p = 3*/
    _max_p_12643 = 3;
L3: 

    /** 		next_guess_ = math:max({1, floor(eumem:ram_space[the_map_p][ELEMENT_COUNT] / max_p)})*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_the_map_p_12642)){
        _7217 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12642)->dbl));
    }
    else{
        _7217 = (int)*(((s1_ptr)_2)->base + _the_map_p_12642);
    }
    _2 = (int)SEQ_PTR(_7217);
    _7218 = (int)*(((s1_ptr)_2)->base + 2);
    _7217 = NOVALUE;
    if (IS_ATOM_INT(_7218)) {
        if (_max_p_12643 > 0 && _7218 >= 0) {
            _7219 = _7218 / _max_p_12643;
        }
        else {
            temp_dbl = floor((double)_7218 / (double)_max_p_12643);
            if (_7218 != MININT)
            _7219 = (long)temp_dbl;
            else
            _7219 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _7218, _max_p_12643);
        _7219 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _7218 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = _7219;
    _7220 = MAKE_SEQ(_1);
    _7219 = NOVALUE;
    _next_guess__12647 = _18max(_7220);
    _7220 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__12647)) {
        _1 = (long)(DBL_PTR(_next_guess__12647)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__12647)) && (DBL_PTR(_next_guess__12647)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__12647);
        _next_guess__12647 = _1;
    }

    /** 		while 1 with entry do*/
    goto L4; // [81] 184
L5: 

    /** 			if stats_[LARGEST_BUCKET] <= max_p then*/
    _2 = (int)SEQ_PTR(_stats__12646);
    _7222 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(GREATER, _7222, _max_p_12643)){
        _7222 = NOVALUE;
        goto L6; // [94] 103
    }
    _7222 = NOVALUE;

    /** 				exit -- Largest is now smaller than the maximum I wanted.*/
    goto L7; // [100] 203
L6: 

    /** 			if stats_[LARGEST_BUCKET] <= (stats_[STDEV_BUCKET]*3 + stats_[AVERAGE_BUCKET]) then*/
    _2 = (int)SEQ_PTR(_stats__12646);
    _7224 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_stats__12646);
    _7225 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_ATOM_INT(_7225)) {
        if (_7225 == (short)_7225)
        _7226 = _7225 * 3;
        else
        _7226 = NewDouble(_7225 * (double)3);
    }
    else {
        _7226 = binary_op(MULTIPLY, _7225, 3);
    }
    _7225 = NOVALUE;
    _2 = (int)SEQ_PTR(_stats__12646);
    _7227 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_7226) && IS_ATOM_INT(_7227)) {
        _7228 = _7226 + _7227;
        if ((long)((unsigned long)_7228 + (unsigned long)HIGH_BITS) >= 0) 
        _7228 = NewDouble((double)_7228);
    }
    else {
        _7228 = binary_op(PLUS, _7226, _7227);
    }
    DeRef(_7226);
    _7226 = NOVALUE;
    _7227 = NOVALUE;
    if (binary_op_a(GREATER, _7224, _7228)){
        _7224 = NOVALUE;
        DeRef(_7228);
        _7228 = NOVALUE;
        goto L8; // [131] 140
    }
    _7224 = NOVALUE;
    DeRef(_7228);
    _7228 = NOVALUE;

    /** 				exit -- Largest is smaller than is statistically expected.*/
    goto L7; // [137] 203
L8: 

    /** 			prev_guess = next_guess_*/
    _prev_guess_12648 = _next_guess__12647;

    /** 			next_guess_ = floor(stats_[NUM_BUCKETS] * grow_p)*/
    _2 = (int)SEQ_PTR(_stats__12646);
    _7230 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7230) && IS_ATOM_INT(_grow_p_12644)) {
        if (_7230 == (short)_7230 && _grow_p_12644 <= INT15 && _grow_p_12644 >= -INT15)
        _7231 = _7230 * _grow_p_12644;
        else
        _7231 = NewDouble(_7230 * (double)_grow_p_12644);
    }
    else {
        _7231 = binary_op(MULTIPLY, _7230, _grow_p_12644);
    }
    _7230 = NOVALUE;
    if (IS_ATOM_INT(_7231))
    _next_guess__12647 = e_floor(_7231);
    else
    _next_guess__12647 = unary_op(FLOOR, _7231);
    DeRef(_7231);
    _7231 = NOVALUE;
    if (!IS_ATOM_INT(_next_guess__12647)) {
        _1 = (long)(DBL_PTR(_next_guess__12647)->dbl);
        if (UNIQUE(DBL_PTR(_next_guess__12647)) && (DBL_PTR(_next_guess__12647)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_next_guess__12647);
        _next_guess__12647 = _1;
    }

    /** 			if prev_guess = next_guess_ then*/
    if (_prev_guess_12648 != _next_guess__12647)
    goto L9; // [168] 181

    /** 				next_guess_ += 1*/
    _next_guess__12647 = _next_guess__12647 + 1;
L9: 

    /** 		entry*/
L4: 

    /** 			rehash(the_map_p, next_guess_)*/
    Ref(_the_map_p_12642);
    _26rehash(_the_map_p_12642, _next_guess__12647);

    /** 			stats_ = statistics(the_map_p)*/
    Ref(_the_map_p_12642);
    _0 = _stats__12646;
    _stats__12646 = _26statistics(_the_map_p_12642);
    DeRef(_0);

    /** 		end while*/
    goto L5; // [200] 84
L7: 
L1: 

    /** end procedure*/
    DeRef(_the_map_p_12642);
    DeRef(_grow_p_12644);
    DeRef(_stats__12646);
    return;
    ;
}


int _26load_map(int _input_file_name_12682)
{
    int _file_handle_12683 = NOVALUE;
    int _line_in_12684 = NOVALUE;
    int _logical_line_12685 = NOVALUE;
    int _has_comment_12686 = NOVALUE;
    int _delim_pos_12687 = NOVALUE;
    int _data_value_12688 = NOVALUE;
    int _data_key_12689 = NOVALUE;
    int _conv_res_12690 = NOVALUE;
    int _new_map_12691 = NOVALUE;
    int _line_conts_12692 = NOVALUE;
    int _value_inlined_value_at_339_12760 = NOVALUE;
    int _value_inlined_value_at_510_12790 = NOVALUE;
    int _in_quote_12825 = NOVALUE;
    int _last_in_12826 = NOVALUE;
    int _cur_in_12827 = NOVALUE;
    int _seek_1__tmp_at925_12855 = NOVALUE;
    int _seek_inlined_seek_at_925_12854 = NOVALUE;
    int _7363 = NOVALUE;
    int _7362 = NOVALUE;
    int _7361 = NOVALUE;
    int _7360 = NOVALUE;
    int _7355 = NOVALUE;
    int _7353 = NOVALUE;
    int _7352 = NOVALUE;
    int _7350 = NOVALUE;
    int _7349 = NOVALUE;
    int _7348 = NOVALUE;
    int _7347 = NOVALUE;
    int _7339 = NOVALUE;
    int _7337 = NOVALUE;
    int _7330 = NOVALUE;
    int _7329 = NOVALUE;
    int _7328 = NOVALUE;
    int _7327 = NOVALUE;
    int _7323 = NOVALUE;
    int _7322 = NOVALUE;
    int _7318 = NOVALUE;
    int _7317 = NOVALUE;
    int _7314 = NOVALUE;
    int _7313 = NOVALUE;
    int _7311 = NOVALUE;
    int _7303 = NOVALUE;
    int _7302 = NOVALUE;
    int _7301 = NOVALUE;
    int _7300 = NOVALUE;
    int _7299 = NOVALUE;
    int _7298 = NOVALUE;
    int _7297 = NOVALUE;
    int _7296 = NOVALUE;
    int _7294 = NOVALUE;
    int _7293 = NOVALUE;
    int _7292 = NOVALUE;
    int _7289 = NOVALUE;
    int _7288 = NOVALUE;
    int _7286 = NOVALUE;
    int _7284 = NOVALUE;
    int _7283 = NOVALUE;
    int _7271 = NOVALUE;
    int _7269 = NOVALUE;
    int _7268 = NOVALUE;
    int _7263 = NOVALUE;
    int _7260 = NOVALUE;
    int _7256 = NOVALUE;
    int _7253 = NOVALUE;
    int _7250 = NOVALUE;
    int _7249 = NOVALUE;
    int _7245 = NOVALUE;
    int _7243 = NOVALUE;
    int _7237 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence line_conts =   ",${"*/
    RefDS(_7236);
    DeRefi(_line_conts_12692);
    _line_conts_12692 = _7236;

    /** 	if sequence(input_file_name) then*/
    _7237 = IS_SEQUENCE(_input_file_name_12682);
    if (_7237 == 0)
    {
        _7237 = NOVALUE;
        goto L1; // [13] 28
    }
    else{
        _7237 = NOVALUE;
    }

    /** 		file_handle = open(input_file_name, "rb")*/
    _file_handle_12683 = EOpen(_input_file_name_12682, _3811, 0);
    goto L2; // [25] 38
L1: 

    /** 		file_handle = input_file_name*/
    Ref(_input_file_name_12682);
    _file_handle_12683 = _input_file_name_12682;
    if (!IS_ATOM_INT(_file_handle_12683)) {
        _1 = (long)(DBL_PTR(_file_handle_12683)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle_12683)) && (DBL_PTR(_file_handle_12683)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle_12683);
        _file_handle_12683 = _1;
    }
L2: 

    /** 	if file_handle = -1 then*/
    if (_file_handle_12683 != -1)
    goto L3; // [42] 53

    /** 		return -1*/
    DeRef(_input_file_name_12682);
    DeRef(_line_in_12684);
    DeRef(_logical_line_12685);
    DeRef(_data_value_12688);
    DeRef(_data_key_12689);
    DeRef(_conv_res_12690);
    DeRef(_new_map_12691);
    DeRefi(_line_conts_12692);
    return -1;
L3: 

    /** 	new_map = new(threshold_size) -- Assume a small map initially.*/
    _0 = _new_map_12691;
    _new_map_12691 = _26new(_26threshold_size_11724);
    DeRef(_0);

    /** 	for i = 1 to 10 do*/
    {
        int _i_12702;
        _i_12702 = 1;
L4: 
        if (_i_12702 > 10){
            goto L5; // [63] 126
        }

        /** 		delim_pos = getc(file_handle)*/
        if (_file_handle_12683 != last_r_file_no) {
            last_r_file_ptr = which_file(_file_handle_12683, EF_READ);
            last_r_file_no = _file_handle_12683;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _delim_pos_12687 = getKBchar();
            }
            else
            _delim_pos_12687 = getc(last_r_file_ptr);
        }
        else
        _delim_pos_12687 = getc(last_r_file_ptr);

        /** 		if delim_pos = -1 then */
        if (_delim_pos_12687 != -1)
        goto L6; // [79] 88

        /** 			exit*/
        goto L5; // [85] 126
L6: 

        /** 		if not t_print(delim_pos) then */
        _7243 = _11t_print(_delim_pos_12687);
        if (IS_ATOM_INT(_7243)) {
            if (_7243 != 0){
                DeRef(_7243);
                _7243 = NOVALUE;
                goto L7; // [94] 112
            }
        }
        else {
            if (DBL_PTR(_7243)->dbl != 0.0){
                DeRef(_7243);
                _7243 = NOVALUE;
                goto L7; // [94] 112
            }
        }
        DeRef(_7243);
        _7243 = NOVALUE;

        /** 	    	if not t_space(delim_pos) then*/
        _7245 = _11t_space(_delim_pos_12687);
        if (IS_ATOM_INT(_7245)) {
            if (_7245 != 0){
                DeRef(_7245);
                _7245 = NOVALUE;
                goto L8; // [103] 111
            }
        }
        else {
            if (DBL_PTR(_7245)->dbl != 0.0){
                DeRef(_7245);
                _7245 = NOVALUE;
                goto L8; // [103] 111
            }
        }
        DeRef(_7245);
        _7245 = NOVALUE;

        /** 	    		exit*/
        goto L5; // [108] 126
L8: 
L7: 

        /** 	    delim_pos = -1*/
        _delim_pos_12687 = -1;

        /** 	end for*/
        _i_12702 = _i_12702 + 1;
        goto L4; // [121] 70
L5: 
        ;
    }

    /** 	if delim_pos = -1 then*/
    if (_delim_pos_12687 != -1)
    goto L9; // [130] 921

    /** 		close(file_handle)*/
    EClose(_file_handle_12683);

    /** 		file_handle = open(input_file_name, "r")*/
    _file_handle_12683 = EOpen(_input_file_name_12682, _3966, 0);

    /** 		while sequence(logical_line) with entry do*/
    goto LA; // [149] 574
LB: 
    _7249 = IS_SEQUENCE(_logical_line_12685);
    if (_7249 == 0)
    {
        _7249 = NOVALUE;
        goto LC; // [157] 1062
    }
    else{
        _7249 = NOVALUE;
    }

    /** 			logical_line = search:match_replace({-2}, logical_line, "\\#3D")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    _7250 = MAKE_SEQ(_1);
    Ref(_logical_line_12685);
    RefDS(_7251);
    _0 = _logical_line_12685;
    _logical_line_12685 = _14match_replace(_7250, _logical_line_12685, _7251, 0);
    DeRef(_0);
    _7250 = NOVALUE;

    /** 			logical_line = search:match_replace({-3}, logical_line, "\\#23")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -3;
    _7253 = MAKE_SEQ(_1);
    Ref(_logical_line_12685);
    RefDS(_7254);
    _0 = _logical_line_12685;
    _logical_line_12685 = _14match_replace(_7253, _logical_line_12685, _7254, 0);
    DeRef(_0);
    _7253 = NOVALUE;

    /** 			logical_line = search:match_replace({-4}, logical_line, "\\#24")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -4;
    _7256 = MAKE_SEQ(_1);
    Ref(_logical_line_12685);
    RefDS(_7257);
    _0 = _logical_line_12685;
    _logical_line_12685 = _14match_replace(_7256, _logical_line_12685, _7257, 0);
    DeRef(_0);
    _7256 = NOVALUE;

    /** 			logical_line = search:match_replace({-5}, logical_line, "\\#2C")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -5;
    _7260 = MAKE_SEQ(_1);
    Ref(_logical_line_12685);
    RefDS(_7261);
    _0 = _logical_line_12685;
    _logical_line_12685 = _14match_replace(_7260, _logical_line_12685, _7261, 0);
    DeRef(_0);
    _7260 = NOVALUE;

    /** 			logical_line = search:match_replace({-6}, logical_line, "\\-")*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -6;
    _7263 = MAKE_SEQ(_1);
    Ref(_logical_line_12685);
    RefDS(_7264);
    _0 = _logical_line_12685;
    _logical_line_12685 = _14match_replace(_7263, _logical_line_12685, _7264, 0);
    DeRef(_0);
    _7263 = NOVALUE;

    /** 			delim_pos = find('=', logical_line)*/
    _delim_pos_12687 = find_from(61, _logical_line_12685, 1);

    /** 			if delim_pos > 0 then*/
    if (_delim_pos_12687 <= 0)
    goto LD; // [236] 571

    /** 				data_key = text:trim(logical_line[1..delim_pos-1])*/
    _7268 = _delim_pos_12687 - 1;
    rhs_slice_target = (object_ptr)&_7269;
    RHS_Slice(_logical_line_12685, 1, _7268);
    RefDS(_3967);
    _0 = _data_key_12689;
    _data_key_12689 = _12trim(_7269, _3967, 0);
    DeRef(_0);
    _7269 = NOVALUE;

    /** 				if length(data_key) > 0 then*/
    if (IS_SEQUENCE(_data_key_12689)){
            _7271 = SEQ_PTR(_data_key_12689)->length;
    }
    else {
        _7271 = 1;
    }
    if (_7271 <= 0)
    goto LE; // [262] 570

    /** 					data_key = search:match_replace("\\#2C", data_key, ",")*/
    RefDS(_7261);
    Ref(_data_key_12689);
    RefDS(_7273);
    _0 = _data_key_12689;
    _data_key_12689 = _14match_replace(_7261, _data_key_12689, _7273, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#24", data_key, "$")*/
    RefDS(_7257);
    Ref(_data_key_12689);
    RefDS(_7275);
    _0 = _data_key_12689;
    _data_key_12689 = _14match_replace(_7257, _data_key_12689, _7275, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#22", data_key, "\"")*/
    RefDS(_7277);
    Ref(_data_key_12689);
    RefDS(_4967);
    _0 = _data_key_12689;
    _data_key_12689 = _14match_replace(_7277, _data_key_12689, _4967, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#3D", data_key, "=")*/
    RefDS(_7251);
    Ref(_data_key_12689);
    RefDS(_2471);
    _0 = _data_key_12689;
    _data_key_12689 = _14match_replace(_7251, _data_key_12689, _2471, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\#23", data_key, "#")*/
    RefDS(_7254);
    Ref(_data_key_12689);
    RefDS(_319);
    _0 = _data_key_12689;
    _data_key_12689 = _14match_replace(_7254, _data_key_12689, _319, 0);
    DeRef(_0);

    /** 					data_key = search:match_replace("\\-", data_key, "-")*/
    RefDS(_7264);
    Ref(_data_key_12689);
    RefDS(_7281);
    _0 = _data_key_12689;
    _data_key_12689 = _14match_replace(_7264, _data_key_12689, _7281, 0);
    DeRef(_0);

    /** 					if not t_alpha(data_key[1]) then*/
    _2 = (int)SEQ_PTR(_data_key_12689);
    _7283 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_7283);
    _7284 = _11t_alpha(_7283);
    _7283 = NOVALUE;
    if (IS_ATOM_INT(_7284)) {
        if (_7284 != 0){
            DeRef(_7284);
            _7284 = NOVALUE;
            goto LF; // [330] 386
        }
    }
    else {
        if (DBL_PTR(_7284)->dbl != 0.0){
            DeRef(_7284);
            _7284 = NOVALUE;
            goto LF; // [330] 386
        }
    }
    DeRef(_7284);
    _7284 = NOVALUE;

    /** 						conv_res = stdget:value(data_key,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_4GET_LONG_ANSWER_10747)) {
        _1 = (long)(DBL_PTR(_4GET_LONG_ANSWER_10747)->dbl);
        if (UNIQUE(DBL_PTR(_4GET_LONG_ANSWER_10747)) && (DBL_PTR(_4GET_LONG_ANSWER_10747)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4GET_LONG_ANSWER_10747);
        _4GET_LONG_ANSWER_10747 = _1;
    }
    if (!IS_ATOM_INT(_4GET_LONG_ANSWER_10747)) {
        _1 = (long)(DBL_PTR(_4GET_LONG_ANSWER_10747)->dbl);
        if (UNIQUE(DBL_PTR(_4GET_LONG_ANSWER_10747)) && (DBL_PTR(_4GET_LONG_ANSWER_10747)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4GET_LONG_ANSWER_10747);
        _4GET_LONG_ANSWER_10747 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_key_12689);
    _0 = _conv_res_12690;
    _conv_res_12690 = _4get_value(_data_key_12689, 1, _4GET_LONG_ANSWER_10747);
    DeRef(_0);

    /** 						if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_12690);
    _7286 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _7286, 0)){
        _7286 = NOVALUE;
        goto L10; // [360] 385
    }
    _7286 = NOVALUE;

    /** 							if conv_res[3] = length(data_key) then*/
    _2 = (int)SEQ_PTR(_conv_res_12690);
    _7288 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_key_12689)){
            _7289 = SEQ_PTR(_data_key_12689)->length;
    }
    else {
        _7289 = 1;
    }
    if (binary_op_a(NOTEQ, _7288, _7289)){
        _7288 = NOVALUE;
        _7289 = NOVALUE;
        goto L11; // [373] 384
    }
    _7288 = NOVALUE;
    _7289 = NOVALUE;

    /** 								data_key = conv_res[2]*/
    DeRef(_data_key_12689);
    _2 = (int)SEQ_PTR(_conv_res_12690);
    _data_key_12689 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_key_12689);
L11: 
L10: 
LF: 

    /** 					data_value = text:trim(logical_line[delim_pos+1..$])*/
    _7292 = _delim_pos_12687 + 1;
    if (_7292 > MAXINT){
        _7292 = NewDouble((double)_7292);
    }
    if (IS_SEQUENCE(_logical_line_12685)){
            _7293 = SEQ_PTR(_logical_line_12685)->length;
    }
    else {
        _7293 = 1;
    }
    rhs_slice_target = (object_ptr)&_7294;
    RHS_Slice(_logical_line_12685, _7292, _7293);
    RefDS(_3967);
    _0 = _data_value_12688;
    _data_value_12688 = _12trim(_7294, _3967, 0);
    DeRef(_0);
    _7294 = NOVALUE;

    /** 					if data_value[1] = '\"' and data_value[$] = '\"' then*/
    _2 = (int)SEQ_PTR(_data_value_12688);
    _7296 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_7296)) {
        _7297 = (_7296 == 34);
    }
    else {
        _7297 = binary_op(EQUALS, _7296, 34);
    }
    _7296 = NOVALUE;
    if (IS_ATOM_INT(_7297)) {
        if (_7297 == 0) {
            goto L12; // [416] 450
        }
    }
    else {
        if (DBL_PTR(_7297)->dbl == 0.0) {
            goto L12; // [416] 450
        }
    }
    if (IS_SEQUENCE(_data_value_12688)){
            _7299 = SEQ_PTR(_data_value_12688)->length;
    }
    else {
        _7299 = 1;
    }
    _2 = (int)SEQ_PTR(_data_value_12688);
    _7300 = (int)*(((s1_ptr)_2)->base + _7299);
    if (IS_ATOM_INT(_7300)) {
        _7301 = (_7300 == 34);
    }
    else {
        _7301 = binary_op(EQUALS, _7300, 34);
    }
    _7300 = NOVALUE;
    if (_7301 == 0) {
        DeRef(_7301);
        _7301 = NOVALUE;
        goto L12; // [432] 450
    }
    else {
        if (!IS_ATOM_INT(_7301) && DBL_PTR(_7301)->dbl == 0.0){
            DeRef(_7301);
            _7301 = NOVALUE;
            goto L12; // [432] 450
        }
        DeRef(_7301);
        _7301 = NOVALUE;
    }
    DeRef(_7301);
    _7301 = NOVALUE;

    /** 					    data_value = data_value[2..$-1]*/
    if (IS_SEQUENCE(_data_value_12688)){
            _7302 = SEQ_PTR(_data_value_12688)->length;
    }
    else {
        _7302 = 1;
    }
    _7303 = _7302 - 1;
    _7302 = NOVALUE;
    rhs_slice_target = (object_ptr)&_data_value_12688;
    RHS_Slice(_data_value_12688, 2, _7303);
L12: 

    /** 					data_value = search:match_replace("\\#2C", data_value, ",")*/
    RefDS(_7261);
    Ref(_data_value_12688);
    RefDS(_7273);
    _0 = _data_value_12688;
    _data_value_12688 = _14match_replace(_7261, _data_value_12688, _7273, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#24", data_value, "$")*/
    RefDS(_7257);
    Ref(_data_value_12688);
    RefDS(_7275);
    _0 = _data_value_12688;
    _data_value_12688 = _14match_replace(_7257, _data_value_12688, _7275, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#22", data_value, "\"")*/
    RefDS(_7277);
    Ref(_data_value_12688);
    RefDS(_4967);
    _0 = _data_value_12688;
    _data_value_12688 = _14match_replace(_7277, _data_value_12688, _4967, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#3D", data_value, "=")*/
    RefDS(_7251);
    Ref(_data_value_12688);
    RefDS(_2471);
    _0 = _data_value_12688;
    _data_value_12688 = _14match_replace(_7251, _data_value_12688, _2471, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\#23", data_value, "#")*/
    RefDS(_7254);
    Ref(_data_value_12688);
    RefDS(_319);
    _0 = _data_value_12688;
    _data_value_12688 = _14match_replace(_7254, _data_value_12688, _319, 0);
    DeRef(_0);

    /** 					data_value = search:match_replace("\\-", data_value, "-")*/
    RefDS(_7264);
    Ref(_data_value_12688);
    RefDS(_7281);
    _0 = _data_value_12688;
    _data_value_12688 = _14match_replace(_7264, _data_value_12688, _7281, 0);
    DeRef(_0);

    /** 					conv_res = stdget:value(data_value,,stdget:GET_LONG_ANSWER)*/
    if (!IS_ATOM_INT(_4GET_LONG_ANSWER_10747)) {
        _1 = (long)(DBL_PTR(_4GET_LONG_ANSWER_10747)->dbl);
        if (UNIQUE(DBL_PTR(_4GET_LONG_ANSWER_10747)) && (DBL_PTR(_4GET_LONG_ANSWER_10747)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4GET_LONG_ANSWER_10747);
        _4GET_LONG_ANSWER_10747 = _1;
    }
    if (!IS_ATOM_INT(_4GET_LONG_ANSWER_10747)) {
        _1 = (long)(DBL_PTR(_4GET_LONG_ANSWER_10747)->dbl);
        if (UNIQUE(DBL_PTR(_4GET_LONG_ANSWER_10747)) && (DBL_PTR(_4GET_LONG_ANSWER_10747)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_4GET_LONG_ANSWER_10747);
        _4GET_LONG_ANSWER_10747 = _1;
    }

    /** 	return get_value(st, start_point, answer)*/
    Ref(_data_value_12688);
    _0 = _conv_res_12690;
    _conv_res_12690 = _4get_value(_data_value_12688, 1, _4GET_LONG_ANSWER_10747);
    DeRef(_0);

    /** 					if conv_res[1] = stdget:GET_SUCCESS then*/
    _2 = (int)SEQ_PTR(_conv_res_12690);
    _7311 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _7311, 0)){
        _7311 = NOVALUE;
        goto L13; // [531] 556
    }
    _7311 = NOVALUE;

    /** 						if conv_res[3] = length(data_value) then*/
    _2 = (int)SEQ_PTR(_conv_res_12690);
    _7313 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_data_value_12688)){
            _7314 = SEQ_PTR(_data_value_12688)->length;
    }
    else {
        _7314 = 1;
    }
    if (binary_op_a(NOTEQ, _7313, _7314)){
        _7313 = NOVALUE;
        _7314 = NOVALUE;
        goto L14; // [544] 555
    }
    _7313 = NOVALUE;
    _7314 = NOVALUE;

    /** 							data_value = conv_res[2]*/
    DeRef(_data_value_12688);
    _2 = (int)SEQ_PTR(_conv_res_12690);
    _data_value_12688 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_data_value_12688);
L14: 
L13: 

    /** 					put(new_map, data_key, data_value)*/
    Ref(_new_map_12691);
    Ref(_data_key_12689);
    Ref(_data_value_12688);
    _26put(_new_map_12691, _data_key_12689, _data_value_12688, 1, _26threshold_size_11724);
LE: 
LD: 

    /** 		entry*/
LA: 

    /** 			logical_line = -1*/
    DeRef(_logical_line_12685);
    _logical_line_12685 = -1;

    /** 			while sequence(line_in) with entry do*/
    goto L15; // [581] 708
L16: 
    _7317 = IS_SEQUENCE(_line_in_12684);
    if (_7317 == 0)
    {
        _7317 = NOVALUE;
        goto LB; // [589] 152
    }
    else{
        _7317 = NOVALUE;
    }

    /** 				if atom(logical_line) then*/
    _7318 = IS_ATOM(_logical_line_12685);
    if (_7318 == 0)
    {
        _7318 = NOVALUE;
        goto L17; // [597] 606
    }
    else{
        _7318 = NOVALUE;
    }

    /** 					logical_line = ""*/
    RefDS(_5);
    DeRef(_logical_line_12685);
    _logical_line_12685 = _5;
L17: 

    /** 				has_comment = match("--", line_in)*/
    _has_comment_12686 = e_match_from(_7319, _line_in_12684, 1);

    /** 				if has_comment != 0 then*/
    if (_has_comment_12686 == 0)
    goto L18; // [617] 641

    /** 					line_in = text:trim(line_in[1..has_comment-1])*/
    _7322 = _has_comment_12686 - 1;
    rhs_slice_target = (object_ptr)&_7323;
    RHS_Slice(_line_in_12684, 1, _7322);
    RefDS(_3967);
    _0 = _line_in_12684;
    _line_in_12684 = _12trim(_7323, _3967, 0);
    DeRef(_0);
    _7323 = NOVALUE;
    goto L19; // [638] 650
L18: 

    /** 					line_in = text:trim(line_in)*/
    Ref(_line_in_12684);
    RefDS(_3967);
    _0 = _line_in_12684;
    _line_in_12684 = _12trim(_line_in_12684, _3967, 0);
    DeRef(_0);
L19: 

    /** 				logical_line &= line_in*/
    if (IS_SEQUENCE(_logical_line_12685) && IS_ATOM(_line_in_12684)) {
        Ref(_line_in_12684);
        Append(&_logical_line_12685, _logical_line_12685, _line_in_12684);
    }
    else if (IS_ATOM(_logical_line_12685) && IS_SEQUENCE(_line_in_12684)) {
        Ref(_logical_line_12685);
        Prepend(&_logical_line_12685, _line_in_12684, _logical_line_12685);
    }
    else {
        Concat((object_ptr)&_logical_line_12685, _logical_line_12685, _line_in_12684);
    }

    /** 				if length(line_in) then*/
    if (IS_SEQUENCE(_line_in_12684)){
            _7327 = SEQ_PTR(_line_in_12684)->length;
    }
    else {
        _7327 = 1;
    }
    if (_7327 == 0)
    {
        _7327 = NOVALUE;
        goto L1A; // [661] 705
    }
    else{
        _7327 = NOVALUE;
    }

    /** 					if not find(line_in[$], line_conts) then*/
    if (IS_SEQUENCE(_line_in_12684)){
            _7328 = SEQ_PTR(_line_in_12684)->length;
    }
    else {
        _7328 = 1;
    }
    _2 = (int)SEQ_PTR(_line_in_12684);
    _7329 = (int)*(((s1_ptr)_2)->base + _7328);
    _7330 = find_from(_7329, _line_conts_12692, 1);
    _7329 = NOVALUE;
    if (_7330 != 0)
    goto L1B; // [678] 704
    _7330 = NOVALUE;

    /** 						logical_line = search:match_replace(`",$"`, logical_line, "")*/
    RefDS(_7332);
    RefDS(_logical_line_12685);
    RefDS(_5);
    _0 = _logical_line_12685;
    _logical_line_12685 = _14match_replace(_7332, _logical_line_12685, _5, 0);
    DeRefDS(_0);

    /** 						logical_line = search:match_replace(`,$`, logical_line, "")*/
    RefDS(_7334);
    Ref(_logical_line_12685);
    RefDS(_5);
    _0 = _logical_line_12685;
    _logical_line_12685 = _14match_replace(_7334, _logical_line_12685, _5, 0);
    DeRef(_0);

    /** 						exit*/
    goto LB; // [701] 152
L1B: 
L1A: 

    /** 			entry*/
L15: 

    /** 				line_in = gets(file_handle)*/
    DeRef(_line_in_12684);
    _line_in_12684 = EGets(_file_handle_12683);

    /**                 integer in_quote = 0, last_in = -1, cur_in = -1*/
    _in_quote_12825 = 0;
    _last_in_12826 = -1;
    _cur_in_12827 = -1;

    /**                 if not equal(line_in, -1) then*/
    if (_line_in_12684 == -1)
    _7337 = 1;
    else if (IS_ATOM_INT(_line_in_12684) && IS_ATOM_INT(-1))
    _7337 = 0;
    else
    _7337 = (compare(_line_in_12684, -1) == 0);
    if (_7337 != 0)
    goto L1C; // [736] 906
    _7337 = NOVALUE;

    /**                     for i = 1 to length(line_in) do*/
    if (IS_SEQUENCE(_line_in_12684)){
            _7339 = SEQ_PTR(_line_in_12684)->length;
    }
    else {
        _7339 = 1;
    }
    {
        int _i_12832;
        _i_12832 = 1;
L1D: 
        if (_i_12832 > _7339){
            goto L1E; // [744] 905
        }

        /**                         cur_in = line_in[i]*/
        _2 = (int)SEQ_PTR(_line_in_12684);
        _cur_in_12827 = (int)*(((s1_ptr)_2)->base + _i_12832);
        if (!IS_ATOM_INT(_cur_in_12827)){
            _cur_in_12827 = (long)DBL_PTR(_cur_in_12827)->dbl;
        }

        /**                         if cur_in = '"' then*/
        if (_cur_in_12827 != 34)
        goto L1F; // [763] 777

        /**                             in_quote = not in_quote*/
        _in_quote_12825 = (_in_quote_12825 == 0);
        goto L20; // [774] 885
L1F: 

        /**                         elsif in_quote then*/
        if (_in_quote_12825 == 0)
        {
            goto L21; // [779] 884
        }
        else{
        }

        /**                             if cur_in = '=' then*/
        if (_cur_in_12827 != 61)
        goto L22; // [784] 798

        /**                                 cur_in = -2 */
        _cur_in_12827 = -2;
        goto L23; // [795] 883
L22: 

        /**                             elsif cur_in = '#' then*/
        if (_cur_in_12827 != 35)
        goto L24; // [800] 814

        /**                                 cur_in = -3 */
        _cur_in_12827 = -3;
        goto L23; // [811] 883
L24: 

        /**                             elsif cur_in = '$' then*/
        if (_cur_in_12827 != 36)
        goto L25; // [816] 830

        /**                                 cur_in = -4*/
        _cur_in_12827 = -4;
        goto L23; // [827] 883
L25: 

        /**                             elsif cur_in = ',' then*/
        if (_cur_in_12827 != 44)
        goto L26; // [832] 846

        /**                                 cur_in = -5*/
        _cur_in_12827 = -5;
        goto L23; // [843] 883
L26: 

        /**                             elsif cur_in = '-' and last_in = '-' then*/
        _7347 = (_cur_in_12827 == 45);
        if (_7347 == 0) {
            goto L27; // [852] 882
        }
        _7349 = (_last_in_12826 == 45);
        if (_7349 == 0)
        {
            DeRef(_7349);
            _7349 = NOVALUE;
            goto L27; // [861] 882
        }
        else{
            DeRef(_7349);
            _7349 = NOVALUE;
        }

        /**                                 cur_in = -6*/
        _cur_in_12827 = -6;

        /**                                 line_in[i-1] = -6*/
        _7350 = _i_12832 - 1;
        _2 = (int)SEQ_PTR(_line_in_12684);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_12684 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _7350);
        _1 = *(int *)_2;
        *(int *)_2 = -6;
        DeRef(_1);
L27: 
L23: 
L21: 
L20: 

        /**                         line_in[i] = cur_in*/
        _2 = (int)SEQ_PTR(_line_in_12684);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _line_in_12684 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12832);
        _1 = *(int *)_2;
        *(int *)_2 = _cur_in_12827;
        DeRef(_1);

        /**                         last_in = cur_in*/
        _last_in_12826 = _cur_in_12827;

        /**                     end for*/
        _i_12832 = _i_12832 + 1;
        goto L1D; // [900] 751
L1E: 
        ;
    }
L1C: 

    /** 			end while*/
    goto L16; // [910] 584

    /** 		end while*/
    goto LB; // [915] 152
    goto LC; // [918] 1062
L9: 

    /** 		io:seek(file_handle, 0)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at925_12855);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _file_handle_12683;
    ((int *)_2)[2] = 0;
    _seek_1__tmp_at925_12855 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_925_12854 = machine(19, _seek_1__tmp_at925_12855);
    DeRefi(_seek_1__tmp_at925_12855);
    _seek_1__tmp_at925_12855 = NOVALUE;

    /** 		line_in  = serialize:deserialize(file_handle)*/
    _0 = _line_in_12684;
    _line_in_12684 = _25deserialize(_file_handle_12683, 1);
    DeRef(_0);

    /** 		if atom(line_in) then*/
    _7352 = IS_ATOM(_line_in_12684);
    if (_7352 == 0)
    {
        _7352 = NOVALUE;
        goto L28; // [948] 958
    }
    else{
        _7352 = NOVALUE;
    }

    /** 			return -2*/
    DeRef(_input_file_name_12682);
    DeRef(_line_in_12684);
    DeRef(_logical_line_12685);
    DeRef(_data_value_12688);
    DeRef(_data_key_12689);
    DeRef(_conv_res_12690);
    DeRef(_new_map_12691);
    DeRefi(_line_conts_12692);
    DeRef(_7268);
    _7268 = NOVALUE;
    DeRef(_7292);
    _7292 = NOVALUE;
    DeRef(_7303);
    _7303 = NOVALUE;
    DeRef(_7297);
    _7297 = NOVALUE;
    DeRef(_7322);
    _7322 = NOVALUE;
    DeRef(_7347);
    _7347 = NOVALUE;
    DeRef(_7350);
    _7350 = NOVALUE;
    return -2;
L28: 

    /** 		if length(line_in) > 1 then*/
    if (IS_SEQUENCE(_line_in_12684)){
            _7353 = SEQ_PTR(_line_in_12684)->length;
    }
    else {
        _7353 = 1;
    }
    if (_7353 <= 1)
    goto L29; // [963] 1054

    /** 			switch line_in[1] do*/
    _2 = (int)SEQ_PTR(_line_in_12684);
    _7355 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_SEQUENCE(_7355) ){
        goto L2A; // [973] 1042
    }
    if(!IS_ATOM_INT(_7355)){
        if( (DBL_PTR(_7355)->dbl != (double) ((int) DBL_PTR(_7355)->dbl) ) ){
            goto L2A; // [973] 1042
        }
        _0 = (int) DBL_PTR(_7355)->dbl;
    }
    else {
        _0 = _7355;
    };
    _7355 = NOVALUE;
    switch ( _0 ){ 

        /** 				case 1, 2 then*/
        case 1:
        case 2:

        /** 					data_key   = serialize:deserialize(file_handle)*/
        _0 = _data_key_12689;
        _data_key_12689 = _25deserialize(_file_handle_12683, 1);
        DeRef(_0);

        /** 					data_value =  serialize:deserialize(file_handle)*/
        _0 = _data_value_12688;
        _data_value_12688 = _25deserialize(_file_handle_12683, 1);
        DeRef(_0);

        /** 					for i = 1 to length(data_key) do*/
        if (IS_SEQUENCE(_data_key_12689)){
                _7360 = SEQ_PTR(_data_key_12689)->length;
        }
        else {
            _7360 = 1;
        }
        {
            int _i_12869;
            _i_12869 = 1;
L2B: 
            if (_i_12869 > _7360){
                goto L2C; // [1003] 1038
            }

            /** 						put(new_map, data_key[i], data_value[i])*/
            _2 = (int)SEQ_PTR(_data_key_12689);
            _7361 = (int)*(((s1_ptr)_2)->base + _i_12869);
            _2 = (int)SEQ_PTR(_data_value_12688);
            _7362 = (int)*(((s1_ptr)_2)->base + _i_12869);
            Ref(_new_map_12691);
            Ref(_7361);
            Ref(_7362);
            _26put(_new_map_12691, _7361, _7362, 1, _26threshold_size_11724);
            _7361 = NOVALUE;
            _7362 = NOVALUE;

            /** 					end for*/
            _i_12869 = _i_12869 + 1;
            goto L2B; // [1033] 1010
L2C: 
            ;
        }
        goto L2D; // [1038] 1061

        /** 				case else*/
        default:
L2A: 

        /** 					return -2*/
        DeRef(_input_file_name_12682);
        DeRef(_line_in_12684);
        DeRef(_logical_line_12685);
        DeRef(_data_value_12688);
        DeRef(_data_key_12689);
        DeRef(_conv_res_12690);
        DeRef(_new_map_12691);
        DeRefi(_line_conts_12692);
        DeRef(_7268);
        _7268 = NOVALUE;
        DeRef(_7292);
        _7292 = NOVALUE;
        DeRef(_7303);
        _7303 = NOVALUE;
        DeRef(_7297);
        _7297 = NOVALUE;
        DeRef(_7322);
        _7322 = NOVALUE;
        DeRef(_7347);
        _7347 = NOVALUE;
        DeRef(_7350);
        _7350 = NOVALUE;
        return -2;
    ;}    goto L2D; // [1051] 1061
L29: 

    /** 			return -2*/
    DeRef(_input_file_name_12682);
    DeRef(_line_in_12684);
    DeRef(_logical_line_12685);
    DeRef(_data_value_12688);
    DeRef(_data_key_12689);
    DeRef(_conv_res_12690);
    DeRef(_new_map_12691);
    DeRefi(_line_conts_12692);
    DeRef(_7268);
    _7268 = NOVALUE;
    DeRef(_7292);
    _7292 = NOVALUE;
    DeRef(_7303);
    _7303 = NOVALUE;
    DeRef(_7297);
    _7297 = NOVALUE;
    DeRef(_7322);
    _7322 = NOVALUE;
    DeRef(_7347);
    _7347 = NOVALUE;
    DeRef(_7350);
    _7350 = NOVALUE;
    return -2;
L2D: 
LC: 

    /** 	if sequence(input_file_name) then*/
    _7363 = IS_SEQUENCE(_input_file_name_12682);
    if (_7363 == 0)
    {
        _7363 = NOVALUE;
        goto L2E; // [1067] 1075
    }
    else{
        _7363 = NOVALUE;
    }

    /** 		close(file_handle)*/
    EClose(_file_handle_12683);
L2E: 

    /** 	optimize(new_map)*/
    Ref(_new_map_12691);
    RefDS(_7211);
    _26optimize(_new_map_12691, _26threshold_size_11724, _7211);

    /** 	return new_map*/
    DeRef(_input_file_name_12682);
    DeRef(_line_in_12684);
    DeRef(_logical_line_12685);
    DeRef(_data_value_12688);
    DeRef(_data_key_12689);
    DeRef(_conv_res_12690);
    DeRefi(_line_conts_12692);
    DeRef(_7268);
    _7268 = NOVALUE;
    DeRef(_7292);
    _7292 = NOVALUE;
    DeRef(_7303);
    _7303 = NOVALUE;
    DeRef(_7297);
    _7297 = NOVALUE;
    DeRef(_7322);
    _7322 = NOVALUE;
    DeRef(_7347);
    _7347 = NOVALUE;
    DeRef(_7350);
    _7350 = NOVALUE;
    return _new_map_12691;
    ;
}


int _26save_map(int _the_map__12882, int _file_name_p_12883, int _type__12884)
{
    int _file_handle__12885 = NOVALUE;
    int _keys__12886 = NOVALUE;
    int _values__12887 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_188_12920 = NOVALUE;
    int _options_inlined_pretty_sprint_at_185_12919 = NOVALUE;
    int _x_inlined_pretty_sprint_at_182_12918 = NOVALUE;
    int _pretty_sprint_inlined_pretty_sprint_at_344_12938 = NOVALUE;
    int _options_inlined_pretty_sprint_at_341_12937 = NOVALUE;
    int _x_inlined_pretty_sprint_at_338_12936 = NOVALUE;
    int _7415 = NOVALUE;
    int _7414 = NOVALUE;
    int _7413 = NOVALUE;
    int _7412 = NOVALUE;
    int _7411 = NOVALUE;
    int _7409 = NOVALUE;
    int _7408 = NOVALUE;
    int _7407 = NOVALUE;
    int _7406 = NOVALUE;
    int _7405 = NOVALUE;
    int _7404 = NOVALUE;
    int _7403 = NOVALUE;
    int _7402 = NOVALUE;
    int _7401 = NOVALUE;
    int _7400 = NOVALUE;
    int _7399 = NOVALUE;
    int _7398 = NOVALUE;
    int _7397 = NOVALUE;
    int _7396 = NOVALUE;
    int _7395 = NOVALUE;
    int _7394 = NOVALUE;
    int _7393 = NOVALUE;
    int _7392 = NOVALUE;
    int _7391 = NOVALUE;
    int _7390 = NOVALUE;
    int _7389 = NOVALUE;
    int _7388 = NOVALUE;
    int _7387 = NOVALUE;
    int _7386 = NOVALUE;
    int _7385 = NOVALUE;
    int _7384 = NOVALUE;
    int _7383 = NOVALUE;
    int _7382 = NOVALUE;
    int _7381 = NOVALUE;
    int _7380 = NOVALUE;
    int _7379 = NOVALUE;
    int _7378 = NOVALUE;
    int _7377 = NOVALUE;
    int _7376 = NOVALUE;
    int _7375 = NOVALUE;
    int _7373 = NOVALUE;
    int _7365 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_type__12884)) {
        _1 = (long)(DBL_PTR(_type__12884)->dbl);
        if (UNIQUE(DBL_PTR(_type__12884)) && (DBL_PTR(_type__12884)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_type__12884);
        _type__12884 = _1;
    }

    /** 	integer file_handle_ = -2*/
    _file_handle__12885 = -2;

    /** 	if sequence(file_name_p) then*/
    _7365 = IS_SEQUENCE(_file_name_p_12883);
    if (_7365 == 0)
    {
        _7365 = NOVALUE;
        goto L1; // [17] 53
    }
    else{
        _7365 = NOVALUE;
    }

    /** 		if type_ = SM_TEXT then*/
    if (_type__12884 != 1)
    goto L2; // [24] 40

    /** 			file_handle_ = open(file_name_p, "w")*/
    _file_handle__12885 = EOpen(_file_name_p_12883, _5740, 0);
    goto L3; // [37] 63
L2: 

    /** 			file_handle_ = open(file_name_p, "wb")*/
    _file_handle__12885 = EOpen(_file_name_p_12883, _3269, 0);
    goto L3; // [50] 63
L1: 

    /** 		file_handle_ = file_name_p*/
    Ref(_file_name_p_12883);
    _file_handle__12885 = _file_name_p_12883;
    if (!IS_ATOM_INT(_file_handle__12885)) {
        _1 = (long)(DBL_PTR(_file_handle__12885)->dbl);
        if (UNIQUE(DBL_PTR(_file_handle__12885)) && (DBL_PTR(_file_handle__12885)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_file_handle__12885);
        _file_handle__12885 = _1;
    }
L3: 

    /** 	if file_handle_ < 0 then*/
    if (_file_handle__12885 >= 0)
    goto L4; // [65] 76

    /** 		return -1*/
    DeRef(_the_map__12882);
    DeRef(_file_name_p_12883);
    DeRef(_keys__12886);
    DeRef(_values__12887);
    return -1;
L4: 

    /** 	keys_ = keys(the_map_)*/
    Ref(_the_map__12882);
    _0 = _keys__12886;
    _keys__12886 = _26keys(_the_map__12882, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    Ref(_the_map__12882);
    _0 = _values__12887;
    _values__12887 = _26values(_the_map__12882, 0, 0);
    DeRef(_0);

    /** 	if type_ = SM_RAW then*/
    if (_type__12884 != 2)
    goto L5; // [99] 151

    /** 		puts(file_handle_, serialize:serialize({*/
    _7373 = _16now_gmt();
    RefDS(_7374);
    _7375 = _16format(_7373, _7374);
    _7373 = NOVALUE;
    _7376 = _30version_string(0);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 2;
    *((int *)(_2+8)) = _7375;
    *((int *)(_2+12)) = _7376;
    _7377 = MAKE_SEQ(_1);
    _7376 = NOVALUE;
    _7375 = NOVALUE;
    _7378 = _25serialize(_7377);
    _7377 = NOVALUE;
    EPuts(_file_handle__12885, _7378); // DJP 
    DeRef(_7378);
    _7378 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(keys_))*/
    RefDS(_keys__12886);
    _7379 = _25serialize(_keys__12886);
    EPuts(_file_handle__12885, _7379); // DJP 
    DeRef(_7379);
    _7379 = NOVALUE;

    /** 		puts(file_handle_, serialize:serialize(values_))*/
    RefDS(_values__12887);
    _7380 = _25serialize(_values__12887);
    EPuts(_file_handle__12885, _7380); // DJP 
    DeRef(_7380);
    _7380 = NOVALUE;
    goto L6; // [148] 501
L5: 

    /** 		for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__12886)){
            _7381 = SEQ_PTR(_keys__12886)->length;
    }
    else {
        _7381 = 1;
    }
    {
        int _i_12913;
        _i_12913 = 1;
L7: 
        if (_i_12913 > _7381){
            goto L8; // [156] 500
        }

        /** 			keys_[i] = pretty:pretty_sprint(keys_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7382 = (int)*(((s1_ptr)_2)->base + _i_12913);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_952);
        *((int *)(_2+20)) = _952;
        RefDS(_5449);
        *((int *)(_2+24)) = _5449;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _7383 = MAKE_SEQ(_1);
        Ref(_7382);
        DeRef(_x_inlined_pretty_sprint_at_182_12918);
        _x_inlined_pretty_sprint_at_182_12918 = _7382;
        _7382 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_185_12919);
        _options_inlined_pretty_sprint_at_185_12919 = _7383;
        _7383 = NOVALUE;

        /** 	pretty_printing = 0*/
        _24pretty_printing_7550 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_182_12918);
        RefDS(_options_inlined_pretty_sprint_at_185_12919);
        _24pretty(_x_inlined_pretty_sprint_at_182_12918, _options_inlined_pretty_sprint_at_185_12919);

        /** 	return pretty_line*/
        RefDS(_24pretty_line_7553);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_188_12920);
        _pretty_sprint_inlined_pretty_sprint_at_188_12920 = _24pretty_line_7553;
        DeRef(_x_inlined_pretty_sprint_at_182_12918);
        _x_inlined_pretty_sprint_at_182_12918 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_185_12919);
        _options_inlined_pretty_sprint_at_185_12919 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_188_12920);
        _2 = (int)SEQ_PTR(_keys__12886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__12886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_188_12920;
        DeRef(_1);

        /** 			keys_[i] = search:match_replace("#", keys_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7384 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_319);
        Ref(_7384);
        RefDS(_7254);
        _7385 = _14match_replace(_319, _7384, _7254, 0);
        _7384 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__12886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__12886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7385;
        if( _1 != _7385 ){
            DeRef(_1);
        }
        _7385 = NOVALUE;

        /** 			keys_[i] = search:match_replace("\"", keys_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7386 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_4967);
        Ref(_7386);
        RefDS(_7277);
        _7387 = _14match_replace(_4967, _7386, _7277, 0);
        _7386 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__12886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__12886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7387;
        if( _1 != _7387 ){
            DeRef(_1);
        }
        _7387 = NOVALUE;

        /** 			keys_[i] = search:match_replace("$", keys_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7388 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_7275);
        Ref(_7388);
        RefDS(_7257);
        _7389 = _14match_replace(_7275, _7388, _7257, 0);
        _7388 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__12886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__12886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7389;
        if( _1 != _7389 ){
            DeRef(_1);
        }
        _7389 = NOVALUE;

        /** 			keys_[i] = search:match_replace(",", keys_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7390 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_7273);
        Ref(_7390);
        RefDS(_7261);
        _7391 = _14match_replace(_7273, _7390, _7261, 0);
        _7390 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__12886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__12886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7391;
        if( _1 != _7391 ){
            DeRef(_1);
        }
        _7391 = NOVALUE;

        /** 			keys_[i] = search:match_replace("=", keys_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7392 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_2471);
        Ref(_7392);
        RefDS(_7251);
        _7393 = _14match_replace(_2471, _7392, _7251, 0);
        _7392 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__12886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__12886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7393;
        if( _1 != _7393 ){
            DeRef(_1);
        }
        _7393 = NOVALUE;

        /** 			keys_[i] = search:match_replace("-", keys_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7394 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_7281);
        Ref(_7394);
        RefDS(_7264);
        _7395 = _14match_replace(_7281, _7394, _7264, 0);
        _7394 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys__12886);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys__12886 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7395;
        if( _1 != _7395 ){
            DeRef(_1);
        }
        _7395 = NOVALUE;

        /** 			values_[i] = pretty:pretty_sprint(values_[i], {2,0,1,0,"%d","%.15g",32,127,1,0})*/
        _2 = (int)SEQ_PTR(_values__12887);
        _7396 = (int)*(((s1_ptr)_2)->base + _i_12913);
        _1 = NewS1(10);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 2;
        *((int *)(_2+8)) = 0;
        *((int *)(_2+12)) = 1;
        *((int *)(_2+16)) = 0;
        RefDS(_952);
        *((int *)(_2+20)) = _952;
        RefDS(_5449);
        *((int *)(_2+24)) = _5449;
        *((int *)(_2+28)) = 32;
        *((int *)(_2+32)) = 127;
        *((int *)(_2+36)) = 1;
        *((int *)(_2+40)) = 0;
        _7397 = MAKE_SEQ(_1);
        Ref(_7396);
        DeRef(_x_inlined_pretty_sprint_at_338_12936);
        _x_inlined_pretty_sprint_at_338_12936 = _7396;
        _7396 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_341_12937);
        _options_inlined_pretty_sprint_at_341_12937 = _7397;
        _7397 = NOVALUE;

        /** 	pretty_printing = 0*/
        _24pretty_printing_7550 = 0;

        /** 	pretty( x, options )*/
        Ref(_x_inlined_pretty_sprint_at_338_12936);
        RefDS(_options_inlined_pretty_sprint_at_341_12937);
        _24pretty(_x_inlined_pretty_sprint_at_338_12936, _options_inlined_pretty_sprint_at_341_12937);

        /** 	return pretty_line*/
        RefDS(_24pretty_line_7553);
        DeRef(_pretty_sprint_inlined_pretty_sprint_at_344_12938);
        _pretty_sprint_inlined_pretty_sprint_at_344_12938 = _24pretty_line_7553;
        DeRef(_x_inlined_pretty_sprint_at_338_12936);
        _x_inlined_pretty_sprint_at_338_12936 = NOVALUE;
        DeRef(_options_inlined_pretty_sprint_at_341_12937);
        _options_inlined_pretty_sprint_at_341_12937 = NOVALUE;
        RefDS(_pretty_sprint_inlined_pretty_sprint_at_344_12938);
        _2 = (int)SEQ_PTR(_values__12887);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__12887 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _pretty_sprint_inlined_pretty_sprint_at_344_12938;
        DeRef(_1);

        /** 			values_[i] = search:match_replace("#", values_[i], "\\#23")*/
        _2 = (int)SEQ_PTR(_values__12887);
        _7398 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_319);
        Ref(_7398);
        RefDS(_7254);
        _7399 = _14match_replace(_319, _7398, _7254, 0);
        _7398 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__12887);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__12887 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7399;
        if( _1 != _7399 ){
            DeRef(_1);
        }
        _7399 = NOVALUE;

        /** 			values_[i] = search:match_replace("\"", values_[i], "\\#22")*/
        _2 = (int)SEQ_PTR(_values__12887);
        _7400 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_4967);
        Ref(_7400);
        RefDS(_7277);
        _7401 = _14match_replace(_4967, _7400, _7277, 0);
        _7400 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__12887);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__12887 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7401;
        if( _1 != _7401 ){
            DeRef(_1);
        }
        _7401 = NOVALUE;

        /** 			values_[i] = search:match_replace("$", values_[i], "\\#24")*/
        _2 = (int)SEQ_PTR(_values__12887);
        _7402 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_7275);
        Ref(_7402);
        RefDS(_7257);
        _7403 = _14match_replace(_7275, _7402, _7257, 0);
        _7402 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__12887);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__12887 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7403;
        if( _1 != _7403 ){
            DeRef(_1);
        }
        _7403 = NOVALUE;

        /** 			values_[i] = search:match_replace(",", values_[i], "\\#2C")*/
        _2 = (int)SEQ_PTR(_values__12887);
        _7404 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_7273);
        Ref(_7404);
        RefDS(_7261);
        _7405 = _14match_replace(_7273, _7404, _7261, 0);
        _7404 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__12887);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__12887 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7405;
        if( _1 != _7405 ){
            DeRef(_1);
        }
        _7405 = NOVALUE;

        /** 			values_[i] = search:match_replace("=", values_[i], "\\#3D")*/
        _2 = (int)SEQ_PTR(_values__12887);
        _7406 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_2471);
        Ref(_7406);
        RefDS(_7251);
        _7407 = _14match_replace(_2471, _7406, _7251, 0);
        _7406 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__12887);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__12887 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7407;
        if( _1 != _7407 ){
            DeRef(_1);
        }
        _7407 = NOVALUE;

        /** 			values_[i] = search:match_replace("-", values_[i], "\\-")*/
        _2 = (int)SEQ_PTR(_values__12887);
        _7408 = (int)*(((s1_ptr)_2)->base + _i_12913);
        RefDS(_7281);
        Ref(_7408);
        RefDS(_7264);
        _7409 = _14match_replace(_7281, _7408, _7264, 0);
        _7408 = NOVALUE;
        _2 = (int)SEQ_PTR(_values__12887);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _values__12887 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12913);
        _1 = *(int *)_2;
        *(int *)_2 = _7409;
        if( _1 != _7409 ){
            DeRef(_1);
        }
        _7409 = NOVALUE;

        /** 			printf(file_handle_, "%s = %s\n", {keys_[i], values_[i]})*/
        _2 = (int)SEQ_PTR(_keys__12886);
        _7411 = (int)*(((s1_ptr)_2)->base + _i_12913);
        _2 = (int)SEQ_PTR(_values__12887);
        _7412 = (int)*(((s1_ptr)_2)->base + _i_12913);
        Ref(_7412);
        Ref(_7411);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _7411;
        ((int *)_2)[2] = _7412;
        _7413 = MAKE_SEQ(_1);
        _7412 = NOVALUE;
        _7411 = NOVALUE;
        EPrintf(_file_handle__12885, _7410, _7413);
        DeRefDS(_7413);
        _7413 = NOVALUE;

        /** 		end for*/
        _i_12913 = _i_12913 + 1;
        goto L7; // [495] 163
L8: 
        ;
    }
L6: 

    /** 	if sequence(file_name_p) then*/
    _7414 = IS_SEQUENCE(_file_name_p_12883);
    if (_7414 == 0)
    {
        _7414 = NOVALUE;
        goto L9; // [506] 514
    }
    else{
        _7414 = NOVALUE;
    }

    /** 		close(file_handle_)*/
    EClose(_file_handle__12885);
L9: 

    /** 	return length(keys_)*/
    if (IS_SEQUENCE(_keys__12886)){
            _7415 = SEQ_PTR(_keys__12886)->length;
    }
    else {
        _7415 = 1;
    }
    DeRef(_the_map__12882);
    DeRef(_file_name_p_12883);
    DeRefDS(_keys__12886);
    DeRef(_values__12887);
    return _7415;
    ;
}


int _26copy(int _source_map_12960, int _dest_map_12961, int _put_operation_12962)
{
    int _keys_set_12965 = NOVALUE;
    int _value_set_12966 = NOVALUE;
    int _source_data_12967 = NOVALUE;
    int _temp_map_12999 = NOVALUE;
    int _7439 = NOVALUE;
    int _7437 = NOVALUE;
    int _7436 = NOVALUE;
    int _7435 = NOVALUE;
    int _7434 = NOVALUE;
    int _7432 = NOVALUE;
    int _7431 = NOVALUE;
    int _7430 = NOVALUE;
    int _7429 = NOVALUE;
    int _7428 = NOVALUE;
    int _7427 = NOVALUE;
    int _7426 = NOVALUE;
    int _7424 = NOVALUE;
    int _7422 = NOVALUE;
    int _7421 = NOVALUE;
    int _7420 = NOVALUE;
    int _7418 = NOVALUE;
    int _7416 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_put_operation_12962)) {
        _1 = (long)(DBL_PTR(_put_operation_12962)->dbl);
        if (UNIQUE(DBL_PTR(_put_operation_12962)) && (DBL_PTR(_put_operation_12962)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_put_operation_12962);
        _put_operation_12962 = _1;
    }

    /** 	if map(dest_map) then*/
    Ref(_dest_map_12961);
    _7416 = _26map(_dest_map_12961);
    if (_7416 == 0) {
        DeRef(_7416);
        _7416 = NOVALUE;
        goto L1; // [11] 221
    }
    else {
        if (!IS_ATOM_INT(_7416) && DBL_PTR(_7416)->dbl == 0.0){
            DeRef(_7416);
            _7416 = NOVALUE;
            goto L1; // [11] 221
        }
        DeRef(_7416);
        _7416 = NOVALUE;
    }
    DeRef(_7416);
    _7416 = NOVALUE;

    /** 		sequence keys_set*/

    /** 		sequence value_set		*/

    /** 		sequence source_data*/

    /** 		source_data = eumem:ram_space[source_map]	*/
    DeRef(_source_data_12967);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_source_map_12960)){
        _source_data_12967 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_12960)->dbl));
    }
    else{
        _source_data_12967 = (int)*(((s1_ptr)_2)->base + _source_map_12960);
    }
    Ref(_source_data_12967);

    /** 		if source_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_source_data_12967);
    _7418 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7418, 76)){
        _7418 = NOVALUE;
        goto L2; // [38] 136
    }
    _7418 = NOVALUE;

    /** 			for index = 1 to length(source_data[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_source_data_12967);
    _7420 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7420)){
            _7421 = SEQ_PTR(_7420)->length;
    }
    else {
        _7421 = 1;
    }
    _7420 = NOVALUE;
    {
        int _index_12973;
        _index_12973 = 1;
L3: 
        if (_index_12973 > _7421){
            goto L4; // [53] 133
        }

        /** 				keys_set = source_data[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_12967);
        _7422 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_keys_set_12965);
        _2 = (int)SEQ_PTR(_7422);
        _keys_set_12965 = (int)*(((s1_ptr)_2)->base + _index_12973);
        Ref(_keys_set_12965);
        _7422 = NOVALUE;

        /** 				value_set = source_data[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_source_data_12967);
        _7424 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_set_12966);
        _2 = (int)SEQ_PTR(_7424);
        _value_set_12966 = (int)*(((s1_ptr)_2)->base + _index_12973);
        Ref(_value_set_12966);
        _7424 = NOVALUE;

        /** 				for j = 1 to length(keys_set) do*/
        if (IS_SEQUENCE(_keys_set_12965)){
                _7426 = SEQ_PTR(_keys_set_12965)->length;
        }
        else {
            _7426 = 1;
        }
        {
            int _j_12981;
            _j_12981 = 1;
L5: 
            if (_j_12981 > _7426){
                goto L6; // [93] 126
            }

            /** 					put(dest_map, keys_set[j], value_set[j], put_operation)*/
            _2 = (int)SEQ_PTR(_keys_set_12965);
            _7427 = (int)*(((s1_ptr)_2)->base + _j_12981);
            _2 = (int)SEQ_PTR(_value_set_12966);
            _7428 = (int)*(((s1_ptr)_2)->base + _j_12981);
            Ref(_dest_map_12961);
            Ref(_7427);
            Ref(_7428);
            _26put(_dest_map_12961, _7427, _7428, _put_operation_12962, _26threshold_size_11724);
            _7427 = NOVALUE;
            _7428 = NOVALUE;

            /** 				end for*/
            _j_12981 = _j_12981 + 1;
            goto L5; // [121] 100
L6: 
            ;
        }

        /** 			end for*/
        _index_12973 = _index_12973 + 1;
        goto L3; // [128] 60
L4: 
        ;
    }
    goto L7; // [133] 210
L2: 

    /** 			for index = 1 to length(source_data[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_source_data_12967);
    _7429 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7429)){
            _7430 = SEQ_PTR(_7429)->length;
    }
    else {
        _7430 = 1;
    }
    _7429 = NOVALUE;
    {
        int _index_12987;
        _index_12987 = 1;
L8: 
        if (_index_12987 > _7430){
            goto L9; // [147] 209
        }

        /** 				if source_data[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_source_data_12967);
        _7431 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7431);
        _7432 = (int)*(((s1_ptr)_2)->base + _index_12987);
        _7431 = NOVALUE;
        if (binary_op_a(EQUALS, _7432, 0)){
            _7432 = NOVALUE;
            goto LA; // [166] 202
        }
        _7432 = NOVALUE;

        /** 					put(dest_map, source_data[KEY_LIST][index], */
        _2 = (int)SEQ_PTR(_source_data_12967);
        _7434 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7434);
        _7435 = (int)*(((s1_ptr)_2)->base + _index_12987);
        _7434 = NOVALUE;
        _2 = (int)SEQ_PTR(_source_data_12967);
        _7436 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7436);
        _7437 = (int)*(((s1_ptr)_2)->base + _index_12987);
        _7436 = NOVALUE;
        Ref(_dest_map_12961);
        Ref(_7435);
        Ref(_7437);
        _26put(_dest_map_12961, _7435, _7437, _put_operation_12962, _26threshold_size_11724);
        _7435 = NOVALUE;
        _7437 = NOVALUE;
LA: 

        /** 			end for*/
        _index_12987 = _index_12987 + 1;
        goto L8; // [204] 154
L9: 
        ;
    }
L7: 

    /** 		return dest_map*/
    DeRef(_keys_set_12965);
    DeRef(_value_set_12966);
    DeRef(_source_data_12967);
    DeRef(_source_map_12960);
    _7420 = NOVALUE;
    _7429 = NOVALUE;
    return _dest_map_12961;
    goto LB; // [218] 251
L1: 

    /** 		atom temp_map = eumem:malloc()*/
    _0 = _temp_map_12999;
    _temp_map_12999 = _27malloc(1, 1);
    DeRef(_0);

    /** 	 	eumem:ram_space[temp_map] = eumem:ram_space[source_map]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_source_map_12960)){
        _7439 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_source_map_12960)->dbl));
    }
    else{
        _7439 = (int)*(((s1_ptr)_2)->base + _source_map_12960);
    }
    Ref(_7439);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map_12999))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map_12999)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map_12999);
    _1 = *(int *)_2;
    *(int *)_2 = _7439;
    if( _1 != _7439 ){
        DeRef(_1);
    }
    _7439 = NOVALUE;

    /** 		return temp_map*/
    DeRef(_source_map_12960);
    DeRef(_dest_map_12961);
    _7420 = NOVALUE;
    _7429 = NOVALUE;
    return _temp_map_12999;
    DeRef(_temp_map_12999);
    _temp_map_12999 = NOVALUE;
LB: 
    ;
}


int _26new_from_kvpairs(int _kv_pairs_13004)
{
    int _new_map_13005 = NOVALUE;
    int _7451 = NOVALUE;
    int _7450 = NOVALUE;
    int _7449 = NOVALUE;
    int _7448 = NOVALUE;
    int _7446 = NOVALUE;
    int _7445 = NOVALUE;
    int _7444 = NOVALUE;
    int _7442 = NOVALUE;
    int _7441 = NOVALUE;
    int _7440 = NOVALUE;
    int _0, _1, _2;
    

    /** 	new_map = new( floor(7 * length(kv_pairs) / 2) )*/
    if (IS_SEQUENCE(_kv_pairs_13004)){
            _7440 = SEQ_PTR(_kv_pairs_13004)->length;
    }
    else {
        _7440 = 1;
    }
    if (_7440 <= INT15)
    _7441 = 7 * _7440;
    else
    _7441 = NewDouble(7 * (double)_7440);
    _7440 = NOVALUE;
    if (IS_ATOM_INT(_7441)) {
        _7442 = _7441 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _7441, 2);
        _7442 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_7441);
    _7441 = NOVALUE;
    _0 = _new_map_13005;
    _new_map_13005 = _26new(_7442);
    DeRef(_0);
    _7442 = NOVALUE;

    /** 	for i = 1 to length(kv_pairs) do*/
    if (IS_SEQUENCE(_kv_pairs_13004)){
            _7444 = SEQ_PTR(_kv_pairs_13004)->length;
    }
    else {
        _7444 = 1;
    }
    {
        int _i_13011;
        _i_13011 = 1;
L1: 
        if (_i_13011 > _7444){
            goto L2; // [25] 82
        }

        /** 		if length(kv_pairs[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_kv_pairs_13004);
        _7445 = (int)*(((s1_ptr)_2)->base + _i_13011);
        if (IS_SEQUENCE(_7445)){
                _7446 = SEQ_PTR(_7445)->length;
        }
        else {
            _7446 = 1;
        }
        _7445 = NOVALUE;
        if (_7446 != 2)
        goto L3; // [41] 75

        /** 			put(new_map, kv_pairs[i][1], kv_pairs[i][2])*/
        _2 = (int)SEQ_PTR(_kv_pairs_13004);
        _7448 = (int)*(((s1_ptr)_2)->base + _i_13011);
        _2 = (int)SEQ_PTR(_7448);
        _7449 = (int)*(((s1_ptr)_2)->base + 1);
        _7448 = NOVALUE;
        _2 = (int)SEQ_PTR(_kv_pairs_13004);
        _7450 = (int)*(((s1_ptr)_2)->base + _i_13011);
        _2 = (int)SEQ_PTR(_7450);
        _7451 = (int)*(((s1_ptr)_2)->base + 2);
        _7450 = NOVALUE;
        Ref(_new_map_13005);
        Ref(_7449);
        Ref(_7451);
        _26put(_new_map_13005, _7449, _7451, 1, _26threshold_size_11724);
        _7449 = NOVALUE;
        _7451 = NOVALUE;
L3: 

        /** 	end for*/
        _i_13011 = _i_13011 + 1;
        goto L1; // [77] 32
L2: 
        ;
    }

    /** 	return new_map	*/
    DeRefDS(_kv_pairs_13004);
    _7445 = NOVALUE;
    return _new_map_13005;
    ;
}


int _26new_from_string(int _kv_string_13023)
{
    int _7453 = NOVALUE;
    int _7452 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return new_from_kvpairs( text:keyvalues (kv_string) )*/
    RefDS(_kv_string_13023);
    RefDS(_4790);
    RefDS(_4791);
    RefDS(_4792);
    RefDS(_149);
    _7452 = _12keyvalues(_kv_string_13023, _4790, _4791, _4792, _149, 1);
    _7453 = _26new_from_kvpairs(_7452);
    _7452 = NOVALUE;
    DeRefDS(_kv_string_13023);
    return _7453;
    ;
}


int _26for_each(int _source_map_13028, int _user_rid_13029, int _user_data_13030, int _in_sorted_order_13031, int _signal_boundary_13032)
{
    int _lKV_13033 = NOVALUE;
    int _lRes_13034 = NOVALUE;
    int _progress_code_13035 = NOVALUE;
    int _7472 = NOVALUE;
    int _7470 = NOVALUE;
    int _7469 = NOVALUE;
    int _7468 = NOVALUE;
    int _7467 = NOVALUE;
    int _7466 = NOVALUE;
    int _7464 = NOVALUE;
    int _7463 = NOVALUE;
    int _7462 = NOVALUE;
    int _7461 = NOVALUE;
    int _7460 = NOVALUE;
    int _7459 = NOVALUE;
    int _7458 = NOVALUE;
    int _7457 = NOVALUE;
    int _7456 = NOVALUE;
    int _7455 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_user_rid_13029)) {
        _1 = (long)(DBL_PTR(_user_rid_13029)->dbl);
        if (UNIQUE(DBL_PTR(_user_rid_13029)) && (DBL_PTR(_user_rid_13029)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_user_rid_13029);
        _user_rid_13029 = _1;
    }
    if (!IS_ATOM_INT(_in_sorted_order_13031)) {
        _1 = (long)(DBL_PTR(_in_sorted_order_13031)->dbl);
        if (UNIQUE(DBL_PTR(_in_sorted_order_13031)) && (DBL_PTR(_in_sorted_order_13031)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_in_sorted_order_13031);
        _in_sorted_order_13031 = _1;
    }
    if (!IS_ATOM_INT(_signal_boundary_13032)) {
        _1 = (long)(DBL_PTR(_signal_boundary_13032)->dbl);
        if (UNIQUE(DBL_PTR(_signal_boundary_13032)) && (DBL_PTR(_signal_boundary_13032)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_signal_boundary_13032);
        _signal_boundary_13032 = _1;
    }

    /** 	lKV = pairs(source_map, in_sorted_order)	*/
    Ref(_source_map_13028);
    _0 = _lKV_13033;
    _lKV_13033 = _26pairs(_source_map_13028, _in_sorted_order_13031);
    DeRef(_0);

    /** 	if length(lKV) = 0 and signal_boundary != 0 then*/
    if (IS_SEQUENCE(_lKV_13033)){
            _7455 = SEQ_PTR(_lKV_13033)->length;
    }
    else {
        _7455 = 1;
    }
    _7456 = (_7455 == 0);
    _7455 = NOVALUE;
    if (_7456 == 0) {
        goto L1; // [31] 61
    }
    _7458 = (_signal_boundary_13032 != 0);
    if (_7458 == 0)
    {
        DeRef(_7458);
        _7458 = NOVALUE;
        goto L1; // [40] 61
    }
    else{
        DeRef(_7458);
        _7458 = NOVALUE;
    }

    /** 		return call_func(user_rid, {0,0,user_data,0} )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    Ref(_user_data_13030);
    *((int *)(_2+12)) = _user_data_13030;
    *((int *)(_2+16)) = 0;
    _7459 = MAKE_SEQ(_1);
    _1 = (int)SEQ_PTR(_7459);
    _2 = (int)((s1_ptr)_1)->base;
    _0 = (int)_00[_user_rid_13029].addr;
    Ref(*(int *)(_2+4));
    Ref(*(int *)(_2+8));
    Ref(*(int *)(_2+12));
    Ref(*(int *)(_2+16));
    _1 = (*(int (*)())_0)(
                        *(int *)(_2+4), 
                        *(int *)(_2+8), 
                        *(int *)(_2+12), 
                        *(int *)(_2+16)
                         );
    DeRef(_7460);
    _7460 = _1;
    DeRefDS(_7459);
    _7459 = NOVALUE;
    DeRef(_source_map_13028);
    DeRef(_user_data_13030);
    DeRef(_lKV_13033);
    DeRef(_lRes_13034);
    DeRef(_7456);
    _7456 = NOVALUE;
    return _7460;
L1: 

    /** 	for i = 1 to length(lKV) do*/
    if (IS_SEQUENCE(_lKV_13033)){
            _7461 = SEQ_PTR(_lKV_13033)->length;
    }
    else {
        _7461 = 1;
    }
    {
        int _i_13045;
        _i_13045 = 1;
L2: 
        if (_i_13045 > _7461){
            goto L3; // [66] 164
        }

        /** 		if i = length(lKV) and signal_boundary then*/
        if (IS_SEQUENCE(_lKV_13033)){
                _7462 = SEQ_PTR(_lKV_13033)->length;
        }
        else {
            _7462 = 1;
        }
        _7463 = (_i_13045 == _7462);
        _7462 = NOVALUE;
        if (_7463 == 0) {
            goto L4; // [82] 102
        }
        if (_signal_boundary_13032 == 0)
        {
            goto L4; // [87] 102
        }
        else{
        }

        /** 			progress_code = -i*/
        _progress_code_13035 = - _i_13045;
        goto L5; // [99] 110
L4: 

        /** 			progress_code = i*/
        _progress_code_13035 = _i_13045;
L5: 

        /** 		lRes = call_func(user_rid, {lKV[i][1], lKV[i][2], user_data, progress_code})*/
        _2 = (int)SEQ_PTR(_lKV_13033);
        _7466 = (int)*(((s1_ptr)_2)->base + _i_13045);
        _2 = (int)SEQ_PTR(_7466);
        _7467 = (int)*(((s1_ptr)_2)->base + 1);
        _7466 = NOVALUE;
        _2 = (int)SEQ_PTR(_lKV_13033);
        _7468 = (int)*(((s1_ptr)_2)->base + _i_13045);
        _2 = (int)SEQ_PTR(_7468);
        _7469 = (int)*(((s1_ptr)_2)->base + 2);
        _7468 = NOVALUE;
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_7467);
        *((int *)(_2+4)) = _7467;
        Ref(_7469);
        *((int *)(_2+8)) = _7469;
        Ref(_user_data_13030);
        *((int *)(_2+12)) = _user_data_13030;
        *((int *)(_2+16)) = _progress_code_13035;
        _7470 = MAKE_SEQ(_1);
        _7469 = NOVALUE;
        _7467 = NOVALUE;
        _1 = (int)SEQ_PTR(_7470);
        _2 = (int)((s1_ptr)_1)->base;
        _0 = (int)_00[_user_rid_13029].addr;
        Ref(*(int *)(_2+4));
        Ref(*(int *)(_2+8));
        Ref(*(int *)(_2+12));
        Ref(*(int *)(_2+16));
        _1 = (*(int (*)())_0)(
                            *(int *)(_2+4), 
                            *(int *)(_2+8), 
                            *(int *)(_2+12), 
                            *(int *)(_2+16)
                             );
        DeRef(_lRes_13034);
        _lRes_13034 = _1;
        DeRefDS(_7470);
        _7470 = NOVALUE;

        /** 		if not equal(lRes, 0) then*/
        if (_lRes_13034 == 0)
        _7472 = 1;
        else if (IS_ATOM_INT(_lRes_13034) && IS_ATOM_INT(0))
        _7472 = 0;
        else
        _7472 = (compare(_lRes_13034, 0) == 0);
        if (_7472 != 0)
        goto L6; // [147] 157
        _7472 = NOVALUE;

        /** 			return lRes*/
        DeRef(_source_map_13028);
        DeRef(_user_data_13030);
        DeRefDS(_lKV_13033);
        DeRef(_7456);
        _7456 = NOVALUE;
        DeRef(_7463);
        _7463 = NOVALUE;
        DeRef(_7460);
        _7460 = NOVALUE;
        return _lRes_13034;
L6: 

        /** 	end for*/
        _i_13045 = _i_13045 + 1;
        goto L2; // [159] 73
L3: 
        ;
    }

    /** 	return 0*/
    DeRef(_source_map_13028);
    DeRef(_user_data_13030);
    DeRef(_lKV_13033);
    DeRef(_lRes_13034);
    DeRef(_7456);
    _7456 = NOVALUE;
    DeRef(_7463);
    _7463 = NOVALUE;
    DeRef(_7460);
    _7460 = NOVALUE;
    return 0;
    ;
}


void _26convert_to_large_map(int _the_map__13064)
{
    int _temp_map__13065 = NOVALUE;
    int _map_handle__13066 = NOVALUE;
    int _7485 = NOVALUE;
    int _7484 = NOVALUE;
    int _7483 = NOVALUE;
    int _7482 = NOVALUE;
    int _7481 = NOVALUE;
    int _7479 = NOVALUE;
    int _7478 = NOVALUE;
    int _7477 = NOVALUE;
    int _7476 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_]*/
    DeRef(_temp_map__13065);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    _temp_map__13065 = (int)*(((s1_ptr)_2)->base + _the_map__13064);
    Ref(_temp_map__13065);

    /** 	map_handle_ = new()*/
    _0 = _map_handle__13066;
    _map_handle__13066 = _26new(690);
    DeRef(_0);

    /** 	for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__13065);
    _7476 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7476)){
            _7477 = SEQ_PTR(_7476)->length;
    }
    else {
        _7477 = 1;
    }
    _7476 = NOVALUE;
    {
        int _index_13070;
        _index_13070 = 1;
L1: 
        if (_index_13070 > _7477){
            goto L2; // [32] 96
        }

        /** 		if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__13065);
        _7478 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7478);
        _7479 = (int)*(((s1_ptr)_2)->base + _index_13070);
        _7478 = NOVALUE;
        if (binary_op_a(EQUALS, _7479, 0)){
            _7479 = NOVALUE;
            goto L3; // [51] 89
        }
        _7479 = NOVALUE;

        /** 			put(map_handle_, temp_map_[KEY_LIST][index], temp_map_[VALUE_LIST][index])*/
        _2 = (int)SEQ_PTR(_temp_map__13065);
        _7481 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7481);
        _7482 = (int)*(((s1_ptr)_2)->base + _index_13070);
        _7481 = NOVALUE;
        _2 = (int)SEQ_PTR(_temp_map__13065);
        _7483 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7483);
        _7484 = (int)*(((s1_ptr)_2)->base + _index_13070);
        _7483 = NOVALUE;
        Ref(_map_handle__13066);
        Ref(_7482);
        Ref(_7484);
        _26put(_map_handle__13066, _7482, _7484, 1, _26threshold_size_11724);
        _7482 = NOVALUE;
        _7484 = NOVALUE;
L3: 

        /** 	end for*/
        _index_13070 = _index_13070 + 1;
        goto L1; // [91] 39
L2: 
        ;
    }

    /** 	eumem:ram_space[the_map_] = eumem:ram_space[map_handle_]*/
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!IS_ATOM_INT(_map_handle__13066)){
        _7485 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_map_handle__13066)->dbl));
    }
    else{
        _7485 = (int)*(((s1_ptr)_2)->base + _map_handle__13066);
    }
    Ref(_7485);
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__13064);
    _1 = *(int *)_2;
    *(int *)_2 = _7485;
    if( _1 != _7485 ){
        DeRef(_1);
    }
    _7485 = NOVALUE;

    /** end procedure*/
    DeRef(_temp_map__13065);
    DeRef(_map_handle__13066);
    _7476 = NOVALUE;
    return;
    ;
}


void _26convert_to_small_map(int _the_map__13084)
{
    int _keys__13085 = NOVALUE;
    int _values__13086 = NOVALUE;
    int _7494 = NOVALUE;
    int _7493 = NOVALUE;
    int _7492 = NOVALUE;
    int _7491 = NOVALUE;
    int _7490 = NOVALUE;
    int _7489 = NOVALUE;
    int _7488 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map__13084)) {
        _1 = (long)(DBL_PTR(_the_map__13084)->dbl);
        if (UNIQUE(DBL_PTR(_the_map__13084)) && (DBL_PTR(_the_map__13084)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map__13084);
        _the_map__13084 = _1;
    }

    /** 	keys_ = keys(the_map_)*/
    _0 = _keys__13085;
    _keys__13085 = _26keys(_the_map__13084, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    _0 = _values__13086;
    _values__13086 = _26values(_the_map__13084, 0, 0);
    DeRef(_0);

    /** 	eumem:ram_space[the_map_] = {*/
    _7488 = Repeat(_26init_small_map_key_11725, _26threshold_size_11724);
    _7489 = Repeat(0, _26threshold_size_11724);
    _7490 = Repeat(0, _26threshold_size_11724);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_26type_is_map_11702);
    *((int *)(_2+4)) = _26type_is_map_11702;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _7488;
    *((int *)(_2+24)) = _7489;
    *((int *)(_2+28)) = _7490;
    _7491 = MAKE_SEQ(_1);
    _7490 = NOVALUE;
    _7489 = NOVALUE;
    _7488 = NOVALUE;
    _2 = (int)SEQ_PTR(_27ram_space_10813);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _27ram_space_10813 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__13084);
    _1 = *(int *)_2;
    *(int *)_2 = _7491;
    if( _1 != _7491 ){
        DeRef(_1);
    }
    _7491 = NOVALUE;

    /** 	for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__13085)){
            _7492 = SEQ_PTR(_keys__13085)->length;
    }
    else {
        _7492 = 1;
    }
    {
        int _i_13094;
        _i_13094 = 1;
L1: 
        if (_i_13094 > _7492){
            goto L2; // [65] 98
        }

        /** 		put(the_map_, keys_[i], values_[i], PUT, 0)*/
        _2 = (int)SEQ_PTR(_keys__13085);
        _7493 = (int)*(((s1_ptr)_2)->base + _i_13094);
        _2 = (int)SEQ_PTR(_values__13086);
        _7494 = (int)*(((s1_ptr)_2)->base + _i_13094);
        Ref(_7493);
        Ref(_7494);
        _26put(_the_map__13084, _7493, _7494, 1, 0);
        _7493 = NOVALUE;
        _7494 = NOVALUE;

        /** 	end for*/
        _i_13094 = _i_13094 + 1;
        goto L1; // [93] 72
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_keys__13085);
    DeRef(_values__13086);
    return;
    ;
}



// 0x00F3B054
