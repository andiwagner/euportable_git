// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _13int_to_bytes(int _x_1764)
{
    int _a_1765 = NOVALUE;
    int _b_1766 = NOVALUE;
    int _c_1767 = NOVALUE;
    int _d_1768 = NOVALUE;
    int _768 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1764)) {
        _a_1765 = (_x_1764 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _a_1765 = Dremainder(DBL_PTR(_x_1764), &temp_d);
    }
    if (!IS_ATOM_INT(_a_1765)) {
        _1 = (long)(DBL_PTR(_a_1765)->dbl);
        if (UNIQUE(DBL_PTR(_a_1765)) && (DBL_PTR(_a_1765)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_1765);
        _a_1765 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1764;
    if (IS_ATOM_INT(_x_1764)) {
        if (256 > 0 && _x_1764 >= 0) {
            _x_1764 = _x_1764 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1764 / (double)256);
            if (_x_1764 != MININT)
            _x_1764 = (long)temp_dbl;
            else
            _x_1764 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1764, 256);
        _x_1764 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	b = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1764)) {
        _b_1766 = (_x_1764 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _b_1766 = Dremainder(DBL_PTR(_x_1764), &temp_d);
    }
    if (!IS_ATOM_INT(_b_1766)) {
        _1 = (long)(DBL_PTR(_b_1766)->dbl);
        if (UNIQUE(DBL_PTR(_b_1766)) && (DBL_PTR(_b_1766)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_b_1766);
        _b_1766 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1764;
    if (IS_ATOM_INT(_x_1764)) {
        if (256 > 0 && _x_1764 >= 0) {
            _x_1764 = _x_1764 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1764 / (double)256);
            if (_x_1764 != MININT)
            _x_1764 = (long)temp_dbl;
            else
            _x_1764 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1764, 256);
        _x_1764 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	c = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1764)) {
        _c_1767 = (_x_1764 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _c_1767 = Dremainder(DBL_PTR(_x_1764), &temp_d);
    }
    if (!IS_ATOM_INT(_c_1767)) {
        _1 = (long)(DBL_PTR(_c_1767)->dbl);
        if (UNIQUE(DBL_PTR(_c_1767)) && (DBL_PTR(_c_1767)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_1767);
        _c_1767 = _1;
    }

    /** 	x = floor(x / #100)*/
    _0 = _x_1764;
    if (IS_ATOM_INT(_x_1764)) {
        if (256 > 0 && _x_1764 >= 0) {
            _x_1764 = _x_1764 / 256;
        }
        else {
            temp_dbl = floor((double)_x_1764 / (double)256);
            if (_x_1764 != MININT)
            _x_1764 = (long)temp_dbl;
            else
            _x_1764 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_1764, 256);
        _x_1764 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_0);

    /** 	d = remainder(x, #100)*/
    if (IS_ATOM_INT(_x_1764)) {
        _d_1768 = (_x_1764 % 256);
    }
    else {
        temp_d.dbl = (double)256;
        _d_1768 = Dremainder(DBL_PTR(_x_1764), &temp_d);
    }
    if (!IS_ATOM_INT(_d_1768)) {
        _1 = (long)(DBL_PTR(_d_1768)->dbl);
        if (UNIQUE(DBL_PTR(_d_1768)) && (DBL_PTR(_d_1768)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_d_1768);
        _d_1768 = _1;
    }

    /** 	return {a,b,c,d}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _a_1765;
    *((int *)(_2+8)) = _b_1766;
    *((int *)(_2+12)) = _c_1767;
    *((int *)(_2+16)) = _d_1768;
    _768 = MAKE_SEQ(_1);
    DeRef(_x_1764);
    return _768;
    ;
}


int _13int_to_bits(int _x_1806, int _nbits_1807)
{
    int _bits_1808 = NOVALUE;
    int _mask_1809 = NOVALUE;
    int _794 = NOVALUE;
    int _793 = NOVALUE;
    int _791 = NOVALUE;
    int _788 = NOVALUE;
    int _787 = NOVALUE;
    int _786 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if nbits < 1 then*/

    /** 	bits = repeat(0, nbits)*/
    DeRef(_bits_1808);
    _bits_1808 = Repeat(0, 32);

    /** 	if nbits <= 32 then*/

    /** 		mask = 1*/
    DeRef(_mask_1809);
    _mask_1809 = 1;

    /** 		for i = 1 to nbits do*/
    _786 = 32;
    {
        int _i_1816;
        _i_1816 = 1;
L1: 
        if (_i_1816 > 32){
            goto L2; // [40] 74
        }

        /** 			bits[i] = and_bits(x, mask) and 1*/
        if (IS_ATOM_INT(_x_1806) && IS_ATOM_INT(_mask_1809)) {
            {unsigned long tu;
                 tu = (unsigned long)_x_1806 & (unsigned long)_mask_1809;
                 _787 = MAKE_UINT(tu);
            }
        }
        else {
            if (IS_ATOM_INT(_x_1806)) {
                temp_d.dbl = (double)_x_1806;
                _787 = Dand_bits(&temp_d, DBL_PTR(_mask_1809));
            }
            else {
                if (IS_ATOM_INT(_mask_1809)) {
                    temp_d.dbl = (double)_mask_1809;
                    _787 = Dand_bits(DBL_PTR(_x_1806), &temp_d);
                }
                else
                _787 = Dand_bits(DBL_PTR(_x_1806), DBL_PTR(_mask_1809));
            }
        }
        if (IS_ATOM_INT(_787)) {
            _788 = (_787 != 0 && 1 != 0);
        }
        else {
            temp_d.dbl = (double)1;
            _788 = Dand(DBL_PTR(_787), &temp_d);
        }
        DeRef(_787);
        _787 = NOVALUE;
        _2 = (int)SEQ_PTR(_bits_1808);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_1808 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1816);
        _1 = *(int *)_2;
        *(int *)_2 = _788;
        if( _1 != _788 ){
            DeRef(_1);
        }
        _788 = NOVALUE;

        /** 			mask *= 2*/
        _0 = _mask_1809;
        if (IS_ATOM_INT(_mask_1809) && IS_ATOM_INT(_mask_1809)) {
            _mask_1809 = _mask_1809 + _mask_1809;
            if ((long)((unsigned long)_mask_1809 + (unsigned long)HIGH_BITS) >= 0) 
            _mask_1809 = NewDouble((double)_mask_1809);
        }
        else {
            if (IS_ATOM_INT(_mask_1809)) {
                _mask_1809 = NewDouble((double)_mask_1809 + DBL_PTR(_mask_1809)->dbl);
            }
            else {
                if (IS_ATOM_INT(_mask_1809)) {
                    _mask_1809 = NewDouble(DBL_PTR(_mask_1809)->dbl + (double)_mask_1809);
                }
                else
                _mask_1809 = NewDouble(DBL_PTR(_mask_1809)->dbl + DBL_PTR(_mask_1809)->dbl);
            }
        }
        DeRef(_0);

        /** 		end for*/
        _i_1816 = _i_1816 + 1;
        goto L1; // [69] 47
L2: 
        ;
    }
    goto L3; // [74] 130

    /** 		if x < 0 then*/
    if (binary_op_a(GREATEREQ, _x_1806, 0)){
        goto L4; // [79] 94
    }

    /** 			x += power(2, nbits) -- for 2's complement bit pattern*/
    _791 = power(2, _nbits_1807);
    _0 = _x_1806;
    if (IS_ATOM_INT(_x_1806) && IS_ATOM_INT(_791)) {
        _x_1806 = _x_1806 + _791;
        if ((long)((unsigned long)_x_1806 + (unsigned long)HIGH_BITS) >= 0) 
        _x_1806 = NewDouble((double)_x_1806);
    }
    else {
        if (IS_ATOM_INT(_x_1806)) {
            _x_1806 = NewDouble((double)_x_1806 + DBL_PTR(_791)->dbl);
        }
        else {
            if (IS_ATOM_INT(_791)) {
                _x_1806 = NewDouble(DBL_PTR(_x_1806)->dbl + (double)_791);
            }
            else
            _x_1806 = NewDouble(DBL_PTR(_x_1806)->dbl + DBL_PTR(_791)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_791);
    _791 = NOVALUE;
L4: 

    /** 		for i = 1 to nbits do*/
    _793 = _nbits_1807;
    {
        int _i_1827;
        _i_1827 = 1;
L5: 
        if (_i_1827 > _793){
            goto L6; // [99] 129
        }

        /** 			bits[i] = remainder(x, 2)*/
        if (IS_ATOM_INT(_x_1806)) {
            _794 = (_x_1806 % 2);
        }
        else {
            temp_d.dbl = (double)2;
            _794 = Dremainder(DBL_PTR(_x_1806), &temp_d);
        }
        _2 = (int)SEQ_PTR(_bits_1808);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _bits_1808 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_1827);
        _1 = *(int *)_2;
        *(int *)_2 = _794;
        if( _1 != _794 ){
            DeRef(_1);
        }
        _794 = NOVALUE;

        /** 			x = floor(x / 2)*/
        _0 = _x_1806;
        if (IS_ATOM_INT(_x_1806)) {
            _x_1806 = _x_1806 >> 1;
        }
        else {
            _1 = binary_op(DIVIDE, _x_1806, 2);
            _x_1806 = unary_op(FLOOR, _1);
            DeRef(_1);
        }
        DeRef(_0);

        /** 		end for*/
        _i_1827 = _i_1827 + 1;
        goto L5; // [124] 106
L6: 
        ;
    }
L3: 

    /** 	return bits*/
    DeRef(_x_1806);
    DeRef(_mask_1809);
    return _bits_1808;
    ;
}


int _13float64_to_atom(int _ieee64_1853)
{
    int _802 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_F64_TO_A, ieee64)*/
    _802 = machine(47, _ieee64_1853);
    DeRefDSi(_ieee64_1853);
    return _802;
    ;
}


int _13hex_text(int _text_1861)
{
    int _res_1862 = NOVALUE;
    int _fp_1863 = NOVALUE;
    int _div_1864 = NOVALUE;
    int _pos_1865 = NOVALUE;
    int _sign_1866 = NOVALUE;
    int _n_1867 = NOVALUE;
    int _831 = NOVALUE;
    int _829 = NOVALUE;
    int _820 = NOVALUE;
    int _819 = NOVALUE;
    int _818 = NOVALUE;
    int _817 = NOVALUE;
    int _814 = NOVALUE;
    int _811 = NOVALUE;
    int _807 = NOVALUE;
    int _805 = NOVALUE;
    int _804 = NOVALUE;
    int _0, _1, _2;
    

    /** 	res = 0*/
    DeRef(_res_1862);
    _res_1862 = 0;

    /** 	fp = 0*/
    DeRef(_fp_1863);
    _fp_1863 = 0;

    /** 	div = 0*/
    _div_1864 = 0;

    /** 	sign = 0*/
    _sign_1866 = 0;

    /** 	n = 0*/
    _n_1867 = 0;

    /** 	for i = 1 to length(text) do*/
    if (IS_SEQUENCE(_text_1861)){
            _804 = SEQ_PTR(_text_1861)->length;
    }
    else {
        _804 = 1;
    }
    {
        int _i_1869;
        _i_1869 = 1;
L1: 
        if (_i_1869 > _804){
            goto L2; // [39] 274
        }

        /** 		if text[i] = '_' then*/
        _2 = (int)SEQ_PTR(_text_1861);
        _805 = (int)*(((s1_ptr)_2)->base + _i_1869);
        if (binary_op_a(NOTEQ, _805, 95)){
            _805 = NOVALUE;
            goto L3; // [52] 61
        }
        _805 = NOVALUE;

        /** 			continue*/
        goto L4; // [58] 269
L3: 

        /** 		if text[i] = '#' then*/
        _2 = (int)SEQ_PTR(_text_1861);
        _807 = (int)*(((s1_ptr)_2)->base + _i_1869);
        if (binary_op_a(NOTEQ, _807, 35)){
            _807 = NOVALUE;
            goto L5; // [67] 90
        }
        _807 = NOVALUE;

        /** 			if n = 0 then*/
        if (_n_1867 != 0)
        goto L2; // [73] 274

        /** 				continue*/
        goto L4; // [79] 269
        goto L6; // [81] 89

        /** 				exit*/
        goto L2; // [86] 274
L6: 
L5: 

        /** 		if text[i] = '.' then*/
        _2 = (int)SEQ_PTR(_text_1861);
        _811 = (int)*(((s1_ptr)_2)->base + _i_1869);
        if (binary_op_a(NOTEQ, _811, 46)){
            _811 = NOVALUE;
            goto L7; // [96] 126
        }
        _811 = NOVALUE;

        /** 			if div = 0 then*/
        if (_div_1864 != 0)
        goto L2; // [102] 274

        /** 				div = 1*/
        _div_1864 = 1;

        /** 				continue*/
        goto L4; // [115] 269
        goto L8; // [117] 125

        /** 				exit*/
        goto L2; // [122] 274
L8: 
L7: 

        /** 		if text[i] = '-' then*/
        _2 = (int)SEQ_PTR(_text_1861);
        _814 = (int)*(((s1_ptr)_2)->base + _i_1869);
        if (binary_op_a(NOTEQ, _814, 45)){
            _814 = NOVALUE;
            goto L9; // [132] 174
        }
        _814 = NOVALUE;

        /** 			if sign = 0 and n = 0 then*/
        _817 = (_sign_1866 == 0);
        if (_817 == 0) {
            goto L2; // [142] 274
        }
        _819 = (_n_1867 == 0);
        if (_819 == 0)
        {
            DeRef(_819);
            _819 = NOVALUE;
            goto L2; // [151] 274
        }
        else{
            DeRef(_819);
            _819 = NOVALUE;
        }

        /** 				sign = -1*/
        _sign_1866 = -1;

        /** 				continue*/
        goto L4; // [163] 269
        goto LA; // [165] 173

        /** 				exit*/
        goto L2; // [170] 274
LA: 
L9: 

        /** 		pos = eu:find(text[i], "0123456789abcdefABCDEF")*/
        _2 = (int)SEQ_PTR(_text_1861);
        _820 = (int)*(((s1_ptr)_2)->base + _i_1869);
        _pos_1865 = find_from(_820, _821, 1);
        _820 = NOVALUE;

        /** 		if pos = 0 then*/
        if (_pos_1865 != 0)
        goto LB; // [189] 198

        /** 			exit*/
        goto L2; // [195] 274
LB: 

        /** 		if pos > 16 then*/
        if (_pos_1865 <= 16)
        goto LC; // [200] 213

        /** 			pos -= 6*/
        _pos_1865 = _pos_1865 - 6;
LC: 

        /** 		pos -= 1*/
        _pos_1865 = _pos_1865 - 1;

        /** 		if div = 0 then*/
        if (_div_1864 != 0)
        goto LD; // [223] 240

        /** 			res = res * 16 + pos*/
        if (IS_ATOM_INT(_res_1862)) {
            if (_res_1862 == (short)_res_1862)
            _829 = _res_1862 * 16;
            else
            _829 = NewDouble(_res_1862 * (double)16);
        }
        else {
            _829 = NewDouble(DBL_PTR(_res_1862)->dbl * (double)16);
        }
        DeRef(_res_1862);
        if (IS_ATOM_INT(_829)) {
            _res_1862 = _829 + _pos_1865;
            if ((long)((unsigned long)_res_1862 + (unsigned long)HIGH_BITS) >= 0) 
            _res_1862 = NewDouble((double)_res_1862);
        }
        else {
            _res_1862 = NewDouble(DBL_PTR(_829)->dbl + (double)_pos_1865);
        }
        DeRef(_829);
        _829 = NOVALUE;
        goto LE; // [237] 259
LD: 

        /** 		    fp = fp * 16 + pos*/
        if (IS_ATOM_INT(_fp_1863)) {
            if (_fp_1863 == (short)_fp_1863)
            _831 = _fp_1863 * 16;
            else
            _831 = NewDouble(_fp_1863 * (double)16);
        }
        else {
            _831 = NewDouble(DBL_PTR(_fp_1863)->dbl * (double)16);
        }
        DeRef(_fp_1863);
        if (IS_ATOM_INT(_831)) {
            _fp_1863 = _831 + _pos_1865;
            if ((long)((unsigned long)_fp_1863 + (unsigned long)HIGH_BITS) >= 0) 
            _fp_1863 = NewDouble((double)_fp_1863);
        }
        else {
            _fp_1863 = NewDouble(DBL_PTR(_831)->dbl + (double)_pos_1865);
        }
        DeRef(_831);
        _831 = NOVALUE;

        /** 		    div += 1*/
        _div_1864 = _div_1864 + 1;
LE: 

        /** 		n += 1*/
        _n_1867 = _n_1867 + 1;

        /** 	end for*/
L4: 
        _i_1869 = _i_1869 + 1;
        goto L1; // [269] 46
L2: 
        ;
    }

    /** 	while div > 1 do*/
LF: 
    if (_div_1864 <= 1)
    goto L10; // [279] 302

    /** 		fp /= 16*/
    _0 = _fp_1863;
    if (IS_ATOM_INT(_fp_1863)) {
        _fp_1863 = (_fp_1863 % 16) ? NewDouble((double)_fp_1863 / 16) : (_fp_1863 / 16);
    }
    else {
        _fp_1863 = NewDouble(DBL_PTR(_fp_1863)->dbl / (double)16);
    }
    DeRef(_0);

    /** 		div -= 1*/
    _div_1864 = _div_1864 - 1;

    /** 	end while*/
    goto LF; // [299] 279
L10: 

    /** 	res += fp*/
    _0 = _res_1862;
    if (IS_ATOM_INT(_res_1862) && IS_ATOM_INT(_fp_1863)) {
        _res_1862 = _res_1862 + _fp_1863;
        if ((long)((unsigned long)_res_1862 + (unsigned long)HIGH_BITS) >= 0) 
        _res_1862 = NewDouble((double)_res_1862);
    }
    else {
        if (IS_ATOM_INT(_res_1862)) {
            _res_1862 = NewDouble((double)_res_1862 + DBL_PTR(_fp_1863)->dbl);
        }
        else {
            if (IS_ATOM_INT(_fp_1863)) {
                _res_1862 = NewDouble(DBL_PTR(_res_1862)->dbl + (double)_fp_1863);
            }
            else
            _res_1862 = NewDouble(DBL_PTR(_res_1862)->dbl + DBL_PTR(_fp_1863)->dbl);
        }
    }
    DeRef(_0);

    /** 	if sign != 0 then*/
    if (_sign_1866 == 0)
    goto L11; // [310] 320

    /** 		res = -res*/
    _0 = _res_1862;
    if (IS_ATOM_INT(_res_1862)) {
        if ((unsigned long)_res_1862 == 0xC0000000)
        _res_1862 = (int)NewDouble((double)-0xC0000000);
        else
        _res_1862 = - _res_1862;
    }
    else {
        _res_1862 = unary_op(UMINUS, _res_1862);
    }
    DeRef(_0);
L11: 

    /** 	return res*/
    DeRefDS(_text_1861);
    DeRef(_fp_1863);
    DeRef(_817);
    _817 = NOVALUE;
    return _res_1862;
    ;
}



// 0xC2860B3A
