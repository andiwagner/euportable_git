// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _16time()
{
    int _ptra_2198 = NOVALUE;
    int _valhi_2199 = NOVALUE;
    int _vallow_2200 = NOVALUE;
    int _deltahi_2201 = NOVALUE;
    int _deltalow_2202 = NOVALUE;
    int _1008 = NOVALUE;
    int _1006 = NOVALUE;
    int _1005 = NOVALUE;
    int _1004 = NOVALUE;
    int _1001 = NOVALUE;
    int _996 = NOVALUE;
    int _994 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		atom ptra, valhi, vallow, deltahi, deltalow*/

    /** 		deltahi = 27111902*/
    _deltahi_2201 = 27111902;

    /** 		deltalow = 3577643008*/
    RefDS(_992);
    DeRef(_deltalow_2202);
    _deltalow_2202 = _992;

    /** 		ptra = machine:allocate(8)*/
    _0 = _ptra_2198;
    _ptra_2198 = _7allocate(8, 0);
    DeRef(_0);

    /** 		c_proc(time_, {ptra})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_ptra_2198);
    *((int *)(_2+4)) = _ptra_2198;
    _994 = MAKE_SEQ(_1);
    call_c(0, _16time__2180, _994);
    DeRefDS(_994);
    _994 = NOVALUE;

    /** 		vallow = peek4u(ptra)*/
    DeRef(_vallow_2200);
    if (IS_ATOM_INT(_ptra_2198)) {
        _vallow_2200 = *(unsigned long *)_ptra_2198;
        if ((unsigned)_vallow_2200 > (unsigned)MAXINT)
        _vallow_2200 = NewDouble((double)(unsigned long)_vallow_2200);
    }
    else {
        _vallow_2200 = *(unsigned long *)(unsigned long)(DBL_PTR(_ptra_2198)->dbl);
        if ((unsigned)_vallow_2200 > (unsigned)MAXINT)
        _vallow_2200 = NewDouble((double)(unsigned long)_vallow_2200);
    }

    /** 		valhi = peek4u(ptra+4)*/
    if (IS_ATOM_INT(_ptra_2198)) {
        _996 = _ptra_2198 + 4;
        if ((long)((unsigned long)_996 + (unsigned long)HIGH_BITS) >= 0) 
        _996 = NewDouble((double)_996);
    }
    else {
        _996 = NewDouble(DBL_PTR(_ptra_2198)->dbl + (double)4);
    }
    DeRef(_valhi_2199);
    if (IS_ATOM_INT(_996)) {
        _valhi_2199 = *(unsigned long *)_996;
        if ((unsigned)_valhi_2199 > (unsigned)MAXINT)
        _valhi_2199 = NewDouble((double)(unsigned long)_valhi_2199);
    }
    else {
        _valhi_2199 = *(unsigned long *)(unsigned long)(DBL_PTR(_996)->dbl);
        if ((unsigned)_valhi_2199 > (unsigned)MAXINT)
        _valhi_2199 = NewDouble((double)(unsigned long)_valhi_2199);
    }
    DeRef(_996);
    _996 = NOVALUE;

    /** 		machine:free(ptra)*/
    Ref(_ptra_2198);
    _7free(_ptra_2198);

    /** 		vallow -= deltalow*/
    _0 = _vallow_2200;
    if (IS_ATOM_INT(_vallow_2200)) {
        _vallow_2200 = NewDouble((double)_vallow_2200 - DBL_PTR(_deltalow_2202)->dbl);
    }
    else {
        _vallow_2200 = NewDouble(DBL_PTR(_vallow_2200)->dbl - DBL_PTR(_deltalow_2202)->dbl);
    }
    DeRef(_0);

    /** 		valhi -= deltahi*/
    _0 = _valhi_2199;
    if (IS_ATOM_INT(_valhi_2199)) {
        _valhi_2199 = _valhi_2199 - 27111902;
        if ((long)((unsigned long)_valhi_2199 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_2199 = NewDouble((double)_valhi_2199);
        }
    }
    else {
        _valhi_2199 = NewDouble(DBL_PTR(_valhi_2199)->dbl - (double)27111902);
    }
    DeRef(_0);

    /** 		if vallow < 0 then*/
    if (binary_op_a(GREATEREQ, _vallow_2200, 0)){
        goto L1; // [67] 88
    }

    /** 			vallow += power(2, 32)*/
    _1001 = power(2, 32);
    _0 = _vallow_2200;
    if (IS_ATOM_INT(_1001)) {
        _vallow_2200 = NewDouble(DBL_PTR(_vallow_2200)->dbl + (double)_1001);
    }
    else
    _vallow_2200 = NewDouble(DBL_PTR(_vallow_2200)->dbl + DBL_PTR(_1001)->dbl);
    DeRefDS(_0);
    DeRef(_1001);
    _1001 = NOVALUE;

    /** 			valhi -= 1*/
    _0 = _valhi_2199;
    if (IS_ATOM_INT(_valhi_2199)) {
        _valhi_2199 = _valhi_2199 - 1;
        if ((long)((unsigned long)_valhi_2199 +(unsigned long) HIGH_BITS) >= 0){
            _valhi_2199 = NewDouble((double)_valhi_2199);
        }
    }
    else {
        _valhi_2199 = NewDouble(DBL_PTR(_valhi_2199)->dbl - (double)1);
    }
    DeRef(_0);
L1: 

    /** 		return floor(((valhi * power(2,32)) + vallow) / 10000000)*/
    _1004 = power(2, 32);
    if (IS_ATOM_INT(_valhi_2199) && IS_ATOM_INT(_1004)) {
        if (_valhi_2199 == (short)_valhi_2199 && _1004 <= INT15 && _1004 >= -INT15)
        _1005 = _valhi_2199 * _1004;
        else
        _1005 = NewDouble(_valhi_2199 * (double)_1004);
    }
    else {
        if (IS_ATOM_INT(_valhi_2199)) {
            _1005 = NewDouble((double)_valhi_2199 * DBL_PTR(_1004)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1004)) {
                _1005 = NewDouble(DBL_PTR(_valhi_2199)->dbl * (double)_1004);
            }
            else
            _1005 = NewDouble(DBL_PTR(_valhi_2199)->dbl * DBL_PTR(_1004)->dbl);
        }
    }
    DeRef(_1004);
    _1004 = NOVALUE;
    if (IS_ATOM_INT(_1005) && IS_ATOM_INT(_vallow_2200)) {
        _1006 = _1005 + _vallow_2200;
        if ((long)((unsigned long)_1006 + (unsigned long)HIGH_BITS) >= 0) 
        _1006 = NewDouble((double)_1006);
    }
    else {
        if (IS_ATOM_INT(_1005)) {
            _1006 = NewDouble((double)_1005 + DBL_PTR(_vallow_2200)->dbl);
        }
        else {
            if (IS_ATOM_INT(_vallow_2200)) {
                _1006 = NewDouble(DBL_PTR(_1005)->dbl + (double)_vallow_2200);
            }
            else
            _1006 = NewDouble(DBL_PTR(_1005)->dbl + DBL_PTR(_vallow_2200)->dbl);
        }
    }
    DeRef(_1005);
    _1005 = NOVALUE;
    if (IS_ATOM_INT(_1006)) {
        if (10000000 > 0 && _1006 >= 0) {
            _1008 = _1006 / 10000000;
        }
        else {
            temp_dbl = floor((double)_1006 / (double)10000000);
            if (_1006 != MININT)
            _1008 = (long)temp_dbl;
            else
            _1008 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _1006, 10000000);
        _1008 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_1006);
    _1006 = NOVALUE;
    DeRef(_ptra_2198);
    DeRef(_valhi_2199);
    DeRef(_vallow_2200);
    DeRef(_deltalow_2202);
    return _1008;
    ;
}


int _16gmtime(int _time_2224)
{
    int _ret_2225 = NOVALUE;
    int _timep_2226 = NOVALUE;
    int _tm_p_2227 = NOVALUE;
    int _n_2228 = NOVALUE;
    int _1014 = NOVALUE;
    int _1013 = NOVALUE;
    int _1010 = NOVALUE;
    int _0, _1, _2;
    

    /** 	timep = machine:allocate(4)*/
    _0 = _timep_2226;
    _timep_2226 = _7allocate(4, 0);
    DeRef(_0);

    /** 	poke4(timep, time)*/
    if (IS_ATOM_INT(_timep_2226)){
        poke4_addr = (unsigned long *)_timep_2226;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_timep_2226)->dbl);
    }
    if (IS_ATOM_INT(_time_2224)) {
        *poke4_addr = (unsigned long)_time_2224;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_time_2224)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	tm_p = c_func(gmtime_, {timep})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_timep_2226);
    *((int *)(_2+4)) = _timep_2226;
    _1010 = MAKE_SEQ(_1);
    DeRef(_tm_p_2227);
    _tm_p_2227 = call_c(1, _16gmtime__2174, _1010);
    DeRefDS(_1010);
    _1010 = NOVALUE;

    /** 	machine:free(timep)*/
    Ref(_timep_2226);
    _7free(_timep_2226);

    /** 	ret = repeat(0, 9)*/
    DeRef(_ret_2225);
    _ret_2225 = Repeat(0, 9);

    /** 	n = 0*/
    _n_2228 = 0;

    /** 	for i = 1 to 9 do*/
    {
        int _i_2234;
        _i_2234 = 1;
L1: 
        if (_i_2234 > 9){
            goto L2; // [46] 81
        }

        /** 		ret[i] = peek4s(tm_p+n)*/
        if (IS_ATOM_INT(_tm_p_2227)) {
            _1013 = _tm_p_2227 + _n_2228;
            if ((long)((unsigned long)_1013 + (unsigned long)HIGH_BITS) >= 0) 
            _1013 = NewDouble((double)_1013);
        }
        else {
            _1013 = NewDouble(DBL_PTR(_tm_p_2227)->dbl + (double)_n_2228);
        }
        if (IS_ATOM_INT(_1013)) {
            _1014 = *(unsigned long *)_1013;
            if (_1014 < MININT || _1014 > MAXINT)
            _1014 = NewDouble((double)(long)_1014);
        }
        else {
            _1014 = *(unsigned long *)(unsigned long)(DBL_PTR(_1013)->dbl);
            if (_1014 < MININT || _1014 > MAXINT)
            _1014 = NewDouble((double)(long)_1014);
        }
        DeRef(_1013);
        _1013 = NOVALUE;
        _2 = (int)SEQ_PTR(_ret_2225);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _ret_2225 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_2234);
        _1 = *(int *)_2;
        *(int *)_2 = _1014;
        if( _1 != _1014 ){
            DeRef(_1);
        }
        _1014 = NOVALUE;

        /** 		n = n + 4*/
        _n_2228 = _n_2228 + 4;

        /** 	end for*/
        _i_2234 = _i_2234 + 1;
        goto L1; // [76] 53
L2: 
        ;
    }

    /** 	return ret*/
    DeRef(_time_2224);
    DeRef(_timep_2226);
    DeRef(_tm_p_2227);
    return _ret_2225;
    ;
}


int _16tolower(int _x_2251)
{
    int _1027 = NOVALUE;
    int _1026 = NOVALUE;
    int _1025 = NOVALUE;
    int _1024 = NOVALUE;
    int _1023 = NOVALUE;
    int _1022 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return x + (x >= 'A' and x <= 'Z') * ('a' - 'A')*/
    _1022 = binary_op(GREATEREQ, _x_2251, 65);
    _1023 = binary_op(LESSEQ, _x_2251, 90);
    _1024 = binary_op(AND, _1022, _1023);
    DeRefDS(_1022);
    _1022 = NOVALUE;
    DeRefDS(_1023);
    _1023 = NOVALUE;
    _1025 = 32;
    _1026 = binary_op(MULTIPLY, _1024, 32);
    DeRefDS(_1024);
    _1024 = NOVALUE;
    _1025 = NOVALUE;
    _1027 = binary_op(PLUS, _x_2251, _1026);
    DeRefDS(_1026);
    _1026 = NOVALUE;
    DeRefDS(_x_2251);
    return _1027;
    ;
}


int _16isLeap(int _year_2260)
{
    int _ly_2261 = NOVALUE;
    int _1046 = NOVALUE;
    int _1045 = NOVALUE;
    int _1043 = NOVALUE;
    int _1042 = NOVALUE;
    int _1041 = NOVALUE;
    int _1040 = NOVALUE;
    int _1039 = NOVALUE;
    int _1038 = NOVALUE;
    int _1037 = NOVALUE;
    int _1034 = NOVALUE;
    int _1032 = NOVALUE;
    int _1031 = NOVALUE;
    int _0, _1, _2;
    

    /** 		ly = (remainder(year, {4, 100, 400, 3200, 80000})=0)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 4;
    *((int *)(_2+8)) = 100;
    *((int *)(_2+12)) = 400;
    *((int *)(_2+16)) = 3200;
    *((int *)(_2+20)) = 80000;
    _1031 = MAKE_SEQ(_1);
    _1032 = binary_op(REMAINDER, _year_2260, _1031);
    DeRefDS(_1031);
    _1031 = NOVALUE;
    DeRefi(_ly_2261);
    _ly_2261 = binary_op(EQUALS, _1032, 0);
    DeRefDS(_1032);
    _1032 = NOVALUE;

    /** 		if not ly[1] then return 0 end if*/
    _2 = (int)SEQ_PTR(_ly_2261);
    _1034 = (int)*(((s1_ptr)_2)->base + 1);
    if (_1034 != 0)
    goto L1; // [31] 39
    _1034 = NOVALUE;
    DeRefDSi(_ly_2261);
    return 0;
L1: 

    /** 		if year <= Gregorian_Reformation then*/
    if (_year_2260 > 1752)
    goto L2; // [41] 54

    /** 				return 1 -- ly[1] can't possibly be 0 here so set shortcut as '1'.*/
    DeRefi(_ly_2261);
    return 1;
    goto L3; // [51] 97
L2: 

    /** 				return ly[1] - ly[2] + ly[3] - ly[4] + ly[5]*/
    _2 = (int)SEQ_PTR(_ly_2261);
    _1037 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_ly_2261);
    _1038 = (int)*(((s1_ptr)_2)->base + 2);
    _1039 = _1037 - _1038;
    if ((long)((unsigned long)_1039 +(unsigned long) HIGH_BITS) >= 0){
        _1039 = NewDouble((double)_1039);
    }
    _1037 = NOVALUE;
    _1038 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_2261);
    _1040 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_1039)) {
        _1041 = _1039 + _1040;
        if ((long)((unsigned long)_1041 + (unsigned long)HIGH_BITS) >= 0) 
        _1041 = NewDouble((double)_1041);
    }
    else {
        _1041 = NewDouble(DBL_PTR(_1039)->dbl + (double)_1040);
    }
    DeRef(_1039);
    _1039 = NOVALUE;
    _1040 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_2261);
    _1042 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1041)) {
        _1043 = _1041 - _1042;
        if ((long)((unsigned long)_1043 +(unsigned long) HIGH_BITS) >= 0){
            _1043 = NewDouble((double)_1043);
        }
    }
    else {
        _1043 = NewDouble(DBL_PTR(_1041)->dbl - (double)_1042);
    }
    DeRef(_1041);
    _1041 = NOVALUE;
    _1042 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_2261);
    _1045 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1043)) {
        _1046 = _1043 + _1045;
        if ((long)((unsigned long)_1046 + (unsigned long)HIGH_BITS) >= 0) 
        _1046 = NewDouble((double)_1046);
    }
    else {
        _1046 = NewDouble(DBL_PTR(_1043)->dbl + (double)_1045);
    }
    DeRef(_1043);
    _1043 = NOVALUE;
    _1045 = NOVALUE;
    DeRefDSi(_ly_2261);
    return _1046;
L3: 
    ;
}


int _16daysInMonth(int _year_2286, int _month_2287)
{
    int _1055 = NOVALUE;
    int _1054 = NOVALUE;
    int _1053 = NOVALUE;
    int _1052 = NOVALUE;
    int _1049 = NOVALUE;
    int _1048 = NOVALUE;
    int _1047 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_month_2287)) {
        _1 = (long)(DBL_PTR(_month_2287)->dbl);
        if (UNIQUE(DBL_PTR(_month_2287)) && (DBL_PTR(_month_2287)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_2287);
        _month_2287 = _1;
    }

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1047 = (_year_2286 == 1752);
    if (_1047 == 0) {
        goto L1; // [15] 36
    }
    _1049 = (_month_2287 == 9);
    if (_1049 == 0)
    {
        DeRef(_1049);
        _1049 = NOVALUE;
        goto L1; // [24] 36
    }
    else{
        DeRef(_1049);
        _1049 = NOVALUE;
    }

    /** 		return 19*/
    DeRef(_1047);
    _1047 = NOVALUE;
    return 19;
    goto L2; // [33] 74
L1: 

    /** 	elsif month != 2 then*/
    if (_month_2287 == 2)
    goto L3; // [38] 55

    /** 		return DaysPerMonth[month]*/
    _2 = (int)SEQ_PTR(_16DaysPerMonth_2242);
    _1052 = (int)*(((s1_ptr)_2)->base + _month_2287);
    Ref(_1052);
    DeRef(_1047);
    _1047 = NOVALUE;
    return _1052;
    goto L2; // [52] 74
L3: 

    /** 		return DaysPerMonth[month] + isLeap(year)*/
    _2 = (int)SEQ_PTR(_16DaysPerMonth_2242);
    _1053 = (int)*(((s1_ptr)_2)->base + _month_2287);
    _1054 = _16isLeap(_year_2286);
    if (IS_ATOM_INT(_1053) && IS_ATOM_INT(_1054)) {
        _1055 = _1053 + _1054;
        if ((long)((unsigned long)_1055 + (unsigned long)HIGH_BITS) >= 0) 
        _1055 = NewDouble((double)_1055);
    }
    else {
        _1055 = binary_op(PLUS, _1053, _1054);
    }
    _1053 = NOVALUE;
    DeRef(_1054);
    _1054 = NOVALUE;
    DeRef(_1047);
    _1047 = NOVALUE;
    _1052 = NOVALUE;
    return _1055;
L2: 
    ;
}


int _16julianDayOfYear(int _ymd_2311)
{
    int _year_2312 = NOVALUE;
    int _month_2313 = NOVALUE;
    int _day_2314 = NOVALUE;
    int _d_2315 = NOVALUE;
    int _1071 = NOVALUE;
    int _1070 = NOVALUE;
    int _1069 = NOVALUE;
    int _1066 = NOVALUE;
    int _1065 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_2311);
    _year_2312 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_2312)){
        _year_2312 = (long)DBL_PTR(_year_2312)->dbl;
    }

    /** 	month = ymd[2]*/
    _2 = (int)SEQ_PTR(_ymd_2311);
    _month_2313 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_month_2313)){
        _month_2313 = (long)DBL_PTR(_month_2313)->dbl;
    }

    /** 	day = ymd[3]*/
    _2 = (int)SEQ_PTR(_ymd_2311);
    _day_2314 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_day_2314)){
        _day_2314 = (long)DBL_PTR(_day_2314)->dbl;
    }

    /** 	if month = 1 then return day end if*/
    if (_month_2313 != 1)
    goto L1; // [33] 42
    DeRef(_ymd_2311);
    return _day_2314;
L1: 

    /** 	d = 0*/
    _d_2315 = 0;

    /** 	for i = 1 to month - 1 do*/
    _1065 = _month_2313 - 1;
    if ((long)((unsigned long)_1065 +(unsigned long) HIGH_BITS) >= 0){
        _1065 = NewDouble((double)_1065);
    }
    {
        int _i_2322;
        _i_2322 = 1;
L2: 
        if (binary_op_a(GREATER, _i_2322, _1065)){
            goto L3; // [55] 84
        }

        /** 		d += daysInMonth(year, i)*/
        Ref(_i_2322);
        _1066 = _16daysInMonth(_year_2312, _i_2322);
        if (IS_ATOM_INT(_1066)) {
            _d_2315 = _d_2315 + _1066;
        }
        else {
            _d_2315 = binary_op(PLUS, _d_2315, _1066);
        }
        DeRef(_1066);
        _1066 = NOVALUE;
        if (!IS_ATOM_INT(_d_2315)) {
            _1 = (long)(DBL_PTR(_d_2315)->dbl);
            if (UNIQUE(DBL_PTR(_d_2315)) && (DBL_PTR(_d_2315)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_2315);
            _d_2315 = _1;
        }

        /** 	end for*/
        _0 = _i_2322;
        if (IS_ATOM_INT(_i_2322)) {
            _i_2322 = _i_2322 + 1;
            if ((long)((unsigned long)_i_2322 +(unsigned long) HIGH_BITS) >= 0){
                _i_2322 = NewDouble((double)_i_2322);
            }
        }
        else {
            _i_2322 = binary_op_a(PLUS, _i_2322, 1);
        }
        DeRef(_0);
        goto L2; // [79] 62
L3: 
        ;
        DeRef(_i_2322);
    }

    /** 	d += day*/
    _d_2315 = _d_2315 + _day_2314;

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1069 = (_year_2312 == 1752);
    if (_1069 == 0) {
        goto L4; // [98] 142
    }
    _1071 = (_month_2313 == 9);
    if (_1071 == 0)
    {
        DeRef(_1071);
        _1071 = NOVALUE;
        goto L4; // [107] 142
    }
    else{
        DeRef(_1071);
        _1071 = NOVALUE;
    }

    /** 		if day > 13 then*/
    if (_day_2314 <= 13)
    goto L5; // [112] 127

    /** 			d -= 11*/
    _d_2315 = _d_2315 - 11;
    goto L6; // [124] 141
L5: 

    /** 		elsif day > 2 then*/
    if (_day_2314 <= 2)
    goto L7; // [129] 140

    /** 			return 0*/
    DeRef(_ymd_2311);
    DeRef(_1065);
    _1065 = NOVALUE;
    DeRef(_1069);
    _1069 = NOVALUE;
    return 0;
L7: 
L6: 
L4: 

    /** 	return d*/
    DeRef(_ymd_2311);
    DeRef(_1065);
    _1065 = NOVALUE;
    DeRef(_1069);
    _1069 = NOVALUE;
    return _d_2315;
    ;
}


int _16julianDay(int _ymd_2339)
{
    int _year_2340 = NOVALUE;
    int _j_2341 = NOVALUE;
    int _greg00_2342 = NOVALUE;
    int _1101 = NOVALUE;
    int _1098 = NOVALUE;
    int _1095 = NOVALUE;
    int _1094 = NOVALUE;
    int _1093 = NOVALUE;
    int _1092 = NOVALUE;
    int _1091 = NOVALUE;
    int _1090 = NOVALUE;
    int _1089 = NOVALUE;
    int _1088 = NOVALUE;
    int _1086 = NOVALUE;
    int _1085 = NOVALUE;
    int _1084 = NOVALUE;
    int _1083 = NOVALUE;
    int _1082 = NOVALUE;
    int _1081 = NOVALUE;
    int _1080 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_2339);
    _year_2340 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_2340)){
        _year_2340 = (long)DBL_PTR(_year_2340)->dbl;
    }

    /** 	j = julianDayOfYear(ymd)*/
    Ref(_ymd_2339);
    _j_2341 = _16julianDayOfYear(_ymd_2339);
    if (!IS_ATOM_INT(_j_2341)) {
        _1 = (long)(DBL_PTR(_j_2341)->dbl);
        if (UNIQUE(DBL_PTR(_j_2341)) && (DBL_PTR(_j_2341)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_2341);
        _j_2341 = _1;
    }

    /** 	year  -= 1*/
    _year_2340 = _year_2340 - 1;

    /** 	greg00 = year - Gregorian_Reformation00*/
    _greg00_2342 = _year_2340 - 1700;

    /** 	j += (*/
    if (_year_2340 <= INT15 && _year_2340 >= -INT15)
    _1080 = 365 * _year_2340;
    else
    _1080 = NewDouble(365 * (double)_year_2340);
    if (4 > 0 && _year_2340 >= 0) {
        _1081 = _year_2340 / 4;
    }
    else {
        temp_dbl = floor((double)_year_2340 / (double)4);
        _1081 = (long)temp_dbl;
    }
    if (IS_ATOM_INT(_1080)) {
        _1082 = _1080 + _1081;
        if ((long)((unsigned long)_1082 + (unsigned long)HIGH_BITS) >= 0) 
        _1082 = NewDouble((double)_1082);
    }
    else {
        _1082 = NewDouble(DBL_PTR(_1080)->dbl + (double)_1081);
    }
    DeRef(_1080);
    _1080 = NOVALUE;
    _1081 = NOVALUE;
    _1083 = (_greg00_2342 > 0);
    if (100 > 0 && _greg00_2342 >= 0) {
        _1084 = _greg00_2342 / 100;
    }
    else {
        temp_dbl = floor((double)_greg00_2342 / (double)100);
        _1084 = (long)temp_dbl;
    }
    _1085 = - _1084;
    _1086 = (_greg00_2342 % 400) ? NewDouble((double)_greg00_2342 / 400) : (_greg00_2342 / 400);
    if (IS_ATOM_INT(_1086)) {
        _1088 = NewDouble((double)_1086 + DBL_PTR(_1087)->dbl);
    }
    else {
        _1088 = NewDouble(DBL_PTR(_1086)->dbl + DBL_PTR(_1087)->dbl);
    }
    DeRef(_1086);
    _1086 = NOVALUE;
    _1089 = unary_op(FLOOR, _1088);
    DeRefDS(_1088);
    _1088 = NOVALUE;
    if (IS_ATOM_INT(_1089)) {
        _1090 = _1085 + _1089;
        if ((long)((unsigned long)_1090 + (unsigned long)HIGH_BITS) >= 0) 
        _1090 = NewDouble((double)_1090);
    }
    else {
        _1090 = binary_op(PLUS, _1085, _1089);
    }
    _1085 = NOVALUE;
    DeRef(_1089);
    _1089 = NOVALUE;
    if (IS_ATOM_INT(_1090)) {
        if (_1090 <= INT15 && _1090 >= -INT15)
        _1091 = _1083 * _1090;
        else
        _1091 = NewDouble(_1083 * (double)_1090);
    }
    else {
        _1091 = binary_op(MULTIPLY, _1083, _1090);
    }
    _1083 = NOVALUE;
    DeRef(_1090);
    _1090 = NOVALUE;
    if (IS_ATOM_INT(_1082) && IS_ATOM_INT(_1091)) {
        _1092 = _1082 + _1091;
        if ((long)((unsigned long)_1092 + (unsigned long)HIGH_BITS) >= 0) 
        _1092 = NewDouble((double)_1092);
    }
    else {
        _1092 = binary_op(PLUS, _1082, _1091);
    }
    DeRef(_1082);
    _1082 = NOVALUE;
    DeRef(_1091);
    _1091 = NOVALUE;
    _1093 = (_year_2340 >= 1752);
    _1094 = 11 * _1093;
    _1093 = NOVALUE;
    if (IS_ATOM_INT(_1092)) {
        _1095 = _1092 - _1094;
        if ((long)((unsigned long)_1095 +(unsigned long) HIGH_BITS) >= 0){
            _1095 = NewDouble((double)_1095);
        }
    }
    else {
        _1095 = binary_op(MINUS, _1092, _1094);
    }
    DeRef(_1092);
    _1092 = NOVALUE;
    _1094 = NOVALUE;
    if (IS_ATOM_INT(_1095)) {
        _j_2341 = _j_2341 + _1095;
    }
    else {
        _j_2341 = binary_op(PLUS, _j_2341, _1095);
    }
    DeRef(_1095);
    _1095 = NOVALUE;
    if (!IS_ATOM_INT(_j_2341)) {
        _1 = (long)(DBL_PTR(_j_2341)->dbl);
        if (UNIQUE(DBL_PTR(_j_2341)) && (DBL_PTR(_j_2341)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_2341);
        _j_2341 = _1;
    }

    /** 	if year >= 3200 then*/
    if (_year_2340 < 3200)
    goto L1; // [107] 147

    /** 		j -= floor(year/ 3200)*/
    if (3200 > 0 && _year_2340 >= 0) {
        _1098 = _year_2340 / 3200;
    }
    else {
        temp_dbl = floor((double)_year_2340 / (double)3200);
        _1098 = (long)temp_dbl;
    }
    _j_2341 = _j_2341 - _1098;
    _1098 = NOVALUE;

    /** 		if year >= 80000 then*/
    if (_year_2340 < 80000)
    goto L2; // [127] 146

    /** 			j += floor(year/80000)*/
    if (80000 > 0 && _year_2340 >= 0) {
        _1101 = _year_2340 / 80000;
    }
    else {
        temp_dbl = floor((double)_year_2340 / (double)80000);
        _1101 = (long)temp_dbl;
    }
    _j_2341 = _j_2341 + _1101;
    _1101 = NOVALUE;
L2: 
L1: 

    /** 	return j*/
    DeRef(_ymd_2339);
    DeRef(_1084);
    _1084 = NOVALUE;
    return _j_2341;
    ;
}


int _16datetimeToSeconds(int _dt_2423)
{
    int _1149 = NOVALUE;
    int _1148 = NOVALUE;
    int _1147 = NOVALUE;
    int _1146 = NOVALUE;
    int _1145 = NOVALUE;
    int _1144 = NOVALUE;
    int _1143 = NOVALUE;
    int _1141 = NOVALUE;
    int _1140 = NOVALUE;
    int _1139 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return julianDay(dt) * DayLengthInSeconds + (dt[4] * 60 + dt[5]) * 60 + dt[6]*/
    Ref(_dt_2423);
    _1139 = _16julianDay(_dt_2423);
    if (IS_ATOM_INT(_1139)) {
        _1140 = NewDouble(_1139 * (double)86400);
    }
    else {
        _1140 = binary_op(MULTIPLY, _1139, 86400);
    }
    DeRef(_1139);
    _1139 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_2423);
    _1141 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1141)) {
        if (_1141 == (short)_1141)
        _1143 = _1141 * 60;
        else
        _1143 = NewDouble(_1141 * (double)60);
    }
    else {
        _1143 = binary_op(MULTIPLY, _1141, 60);
    }
    _1141 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_2423);
    _1144 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1143) && IS_ATOM_INT(_1144)) {
        _1145 = _1143 + _1144;
        if ((long)((unsigned long)_1145 + (unsigned long)HIGH_BITS) >= 0) 
        _1145 = NewDouble((double)_1145);
    }
    else {
        _1145 = binary_op(PLUS, _1143, _1144);
    }
    DeRef(_1143);
    _1143 = NOVALUE;
    _1144 = NOVALUE;
    if (IS_ATOM_INT(_1145)) {
        if (_1145 == (short)_1145)
        _1146 = _1145 * 60;
        else
        _1146 = NewDouble(_1145 * (double)60);
    }
    else {
        _1146 = binary_op(MULTIPLY, _1145, 60);
    }
    DeRef(_1145);
    _1145 = NOVALUE;
    if (IS_ATOM_INT(_1140) && IS_ATOM_INT(_1146)) {
        _1147 = _1140 + _1146;
        if ((long)((unsigned long)_1147 + (unsigned long)HIGH_BITS) >= 0) 
        _1147 = NewDouble((double)_1147);
    }
    else {
        _1147 = binary_op(PLUS, _1140, _1146);
    }
    DeRef(_1140);
    _1140 = NOVALUE;
    DeRef(_1146);
    _1146 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_2423);
    _1148 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_1147) && IS_ATOM_INT(_1148)) {
        _1149 = _1147 + _1148;
        if ((long)((unsigned long)_1149 + (unsigned long)HIGH_BITS) >= 0) 
        _1149 = NewDouble((double)_1149);
    }
    else {
        _1149 = binary_op(PLUS, _1147, _1148);
    }
    DeRef(_1147);
    _1147 = NOVALUE;
    _1148 = NOVALUE;
    DeRef(_dt_2423);
    return _1149;
    ;
}


int _16from_date(int _src_2602)
{
    int _1276 = NOVALUE;
    int _1275 = NOVALUE;
    int _1274 = NOVALUE;
    int _1273 = NOVALUE;
    int _1272 = NOVALUE;
    int _1271 = NOVALUE;
    int _1270 = NOVALUE;
    int _1268 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {src[YEAR]+1900, src[MONTH], src[DAY], src[HOUR], src[MINUTE], src[SECOND]}*/
    _2 = (int)SEQ_PTR(_src_2602);
    _1268 = (int)*(((s1_ptr)_2)->base + 1);
    _1270 = _1268 + 1900;
    if ((long)((unsigned long)_1270 + (unsigned long)HIGH_BITS) >= 0) 
    _1270 = NewDouble((double)_1270);
    _1268 = NOVALUE;
    _2 = (int)SEQ_PTR(_src_2602);
    _1271 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_src_2602);
    _1272 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_src_2602);
    _1273 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_src_2602);
    _1274 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_src_2602);
    _1275 = (int)*(((s1_ptr)_2)->base + 6);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1270;
    *((int *)(_2+8)) = _1271;
    *((int *)(_2+12)) = _1272;
    *((int *)(_2+16)) = _1273;
    *((int *)(_2+20)) = _1274;
    *((int *)(_2+24)) = _1275;
    _1276 = MAKE_SEQ(_1);
    _1275 = NOVALUE;
    _1274 = NOVALUE;
    _1273 = NOVALUE;
    _1272 = NOVALUE;
    _1271 = NOVALUE;
    _1270 = NOVALUE;
    DeRefDSi(_src_2602);
    return _1276;
    ;
}


int _16now_gmt()
{
    int _t1_2618 = NOVALUE;
    int _1289 = NOVALUE;
    int _1288 = NOVALUE;
    int _1287 = NOVALUE;
    int _1286 = NOVALUE;
    int _1285 = NOVALUE;
    int _1284 = NOVALUE;
    int _1283 = NOVALUE;
    int _1282 = NOVALUE;
    int _1281 = NOVALUE;
    int _1279 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence t1 = gmtime(time())*/
    _1279 = _16time();
    _0 = _t1_2618;
    _t1_2618 = _16gmtime(_1279);
    DeRef(_0);
    _1279 = NOVALUE;

    /** 	return { */
    _2 = (int)SEQ_PTR(_t1_2618);
    _1281 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_1281)) {
        _1282 = _1281 + 1900;
        if ((long)((unsigned long)_1282 + (unsigned long)HIGH_BITS) >= 0) 
        _1282 = NewDouble((double)_1282);
    }
    else {
        _1282 = binary_op(PLUS, _1281, 1900);
    }
    _1281 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_2618);
    _1283 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1283)) {
        _1284 = _1283 + 1;
        if (_1284 > MAXINT){
            _1284 = NewDouble((double)_1284);
        }
    }
    else
    _1284 = binary_op(PLUS, 1, _1283);
    _1283 = NOVALUE;
    _2 = (int)SEQ_PTR(_t1_2618);
    _1285 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_t1_2618);
    _1286 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_t1_2618);
    _1287 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_t1_2618);
    _1288 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1282;
    *((int *)(_2+8)) = _1284;
    Ref(_1285);
    *((int *)(_2+12)) = _1285;
    Ref(_1286);
    *((int *)(_2+16)) = _1286;
    Ref(_1287);
    *((int *)(_2+20)) = _1287;
    Ref(_1288);
    *((int *)(_2+24)) = _1288;
    _1289 = MAKE_SEQ(_1);
    _1288 = NOVALUE;
    _1287 = NOVALUE;
    _1286 = NOVALUE;
    _1285 = NOVALUE;
    _1284 = NOVALUE;
    _1282 = NOVALUE;
    DeRefDS(_t1_2618);
    return _1289;
    ;
}


int _16new(int _year_2632, int _month_2633, int _day_2634, int _hour_2635, int _minute_2636, int _second_2637)
{
    int _d_2638 = NOVALUE;
    int _now_1__tmp_at51_2645 = NOVALUE;
    int _now_inlined_now_at_51_2644 = NOVALUE;
    int _1292 = NOVALUE;
    int _1291 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_year_2632)) {
        _1 = (long)(DBL_PTR(_year_2632)->dbl);
        if (UNIQUE(DBL_PTR(_year_2632)) && (DBL_PTR(_year_2632)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_year_2632);
        _year_2632 = _1;
    }
    if (!IS_ATOM_INT(_month_2633)) {
        _1 = (long)(DBL_PTR(_month_2633)->dbl);
        if (UNIQUE(DBL_PTR(_month_2633)) && (DBL_PTR(_month_2633)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_2633);
        _month_2633 = _1;
    }
    if (!IS_ATOM_INT(_day_2634)) {
        _1 = (long)(DBL_PTR(_day_2634)->dbl);
        if (UNIQUE(DBL_PTR(_day_2634)) && (DBL_PTR(_day_2634)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_day_2634);
        _day_2634 = _1;
    }
    if (!IS_ATOM_INT(_hour_2635)) {
        _1 = (long)(DBL_PTR(_hour_2635)->dbl);
        if (UNIQUE(DBL_PTR(_hour_2635)) && (DBL_PTR(_hour_2635)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hour_2635);
        _hour_2635 = _1;
    }
    if (!IS_ATOM_INT(_minute_2636)) {
        _1 = (long)(DBL_PTR(_minute_2636)->dbl);
        if (UNIQUE(DBL_PTR(_minute_2636)) && (DBL_PTR(_minute_2636)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minute_2636);
        _minute_2636 = _1;
    }

    /** 	d = {year, month, day, hour, minute, second}*/
    _0 = _d_2638;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _year_2632;
    *((int *)(_2+8)) = _month_2633;
    *((int *)(_2+12)) = _day_2634;
    *((int *)(_2+16)) = _hour_2635;
    *((int *)(_2+20)) = _minute_2636;
    Ref(_second_2637);
    *((int *)(_2+24)) = _second_2637;
    _d_2638 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	if equal(d, {0,0,0,0,0,0}) then*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    *((int *)(_2+24)) = 0;
    _1291 = MAKE_SEQ(_1);
    if (_d_2638 == _1291)
    _1292 = 1;
    else if (IS_ATOM_INT(_d_2638) && IS_ATOM_INT(_1291))
    _1292 = 0;
    else
    _1292 = (compare(_d_2638, _1291) == 0);
    DeRefDS(_1291);
    _1291 = NOVALUE;
    if (_1292 == 0)
    {
        _1292 = NOVALUE;
        goto L1; // [47] 70
    }
    else{
        _1292 = NOVALUE;
    }

    /** 		return now()*/

    /** 	return from_date(date())*/
    DeRefi(_now_1__tmp_at51_2645);
    _now_1__tmp_at51_2645 = Date();
    RefDS(_now_1__tmp_at51_2645);
    _0 = _now_inlined_now_at_51_2644;
    _now_inlined_now_at_51_2644 = _16from_date(_now_1__tmp_at51_2645);
    DeRef(_0);
    DeRefi(_now_1__tmp_at51_2645);
    _now_1__tmp_at51_2645 = NOVALUE;
    DeRef(_second_2637);
    DeRef(_d_2638);
    return _now_inlined_now_at_51_2644;
    goto L2; // [67] 77
L1: 

    /** 		return d*/
    DeRef(_second_2637);
    return _d_2638;
L2: 
    ;
}


int _16format(int _d_2698, int _pattern_2699)
{
    int _in_fmt_2701 = NOVALUE;
    int _ch_2702 = NOVALUE;
    int _tmp_2703 = NOVALUE;
    int _res_2704 = NOVALUE;
    int _weeks_day_4__tmp_at73_2720 = NOVALUE;
    int _weeks_day_3__tmp_at73_2719 = NOVALUE;
    int _weeks_day_2__tmp_at73_2718 = NOVALUE;
    int _weeks_day_1__tmp_at73_2717 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_73_2716 = NOVALUE;
    int _weeks_day_4__tmp_at119_2730 = NOVALUE;
    int _weeks_day_3__tmp_at119_2729 = NOVALUE;
    int _weeks_day_2__tmp_at119_2728 = NOVALUE;
    int _weeks_day_1__tmp_at119_2727 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_119_2726 = NOVALUE;
    int _to_unix_1__tmp_at626_2835 = NOVALUE;
    int _to_unix_inlined_to_unix_at_626_2834 = NOVALUE;
    int _weeks_day_4__tmp_at683_2852 = NOVALUE;
    int _weeks_day_3__tmp_at683_2851 = NOVALUE;
    int _weeks_day_2__tmp_at683_2850 = NOVALUE;
    int _weeks_day_1__tmp_at683_2849 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_683_2848 = NOVALUE;
    int _weeks_day_4__tmp_at729_2863 = NOVALUE;
    int _weeks_day_3__tmp_at729_2862 = NOVALUE;
    int _weeks_day_2__tmp_at729_2861 = NOVALUE;
    int _weeks_day_1__tmp_at729_2860 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_729_2859 = NOVALUE;
    int _weeks_day_4__tmp_at778_2875 = NOVALUE;
    int _weeks_day_3__tmp_at778_2874 = NOVALUE;
    int _weeks_day_2__tmp_at778_2873 = NOVALUE;
    int _weeks_day_1__tmp_at778_2872 = NOVALUE;
    int _weeks_day_inlined_weeks_day_at_778_2871 = NOVALUE;
    int _1438 = NOVALUE;
    int _1437 = NOVALUE;
    int _1432 = NOVALUE;
    int _1431 = NOVALUE;
    int _1430 = NOVALUE;
    int _1429 = NOVALUE;
    int _1427 = NOVALUE;
    int _1423 = NOVALUE;
    int _1422 = NOVALUE;
    int _1418 = NOVALUE;
    int _1417 = NOVALUE;
    int _1410 = NOVALUE;
    int _1409 = NOVALUE;
    int _1405 = NOVALUE;
    int _1401 = NOVALUE;
    int _1400 = NOVALUE;
    int _1398 = NOVALUE;
    int _1397 = NOVALUE;
    int _1395 = NOVALUE;
    int _1391 = NOVALUE;
    int _1389 = NOVALUE;
    int _1387 = NOVALUE;
    int _1383 = NOVALUE;
    int _1382 = NOVALUE;
    int _1378 = NOVALUE;
    int _1377 = NOVALUE;
    int _1373 = NOVALUE;
    int _1365 = NOVALUE;
    int _1364 = NOVALUE;
    int _1360 = NOVALUE;
    int _1359 = NOVALUE;
    int _1355 = NOVALUE;
    int _1347 = NOVALUE;
    int _1346 = NOVALUE;
    int _1343 = NOVALUE;
    int _1342 = NOVALUE;
    int _1339 = NOVALUE;
    int _1338 = NOVALUE;
    int _1337 = NOVALUE;
    int _1333 = NOVALUE;
    int _1332 = NOVALUE;
    int _1329 = NOVALUE;
    int _1328 = NOVALUE;
    int _1325 = NOVALUE;
    int _1322 = NOVALUE;
    int _1317 = NOVALUE;
    int _0, _1, _2;
    

    /** 	in_fmt = 0*/
    _in_fmt_2701 = 0;

    /** 	res = ""*/
    RefDS(_5);
    DeRef(_res_2704);
    _res_2704 = _5;

    /** 	for i = 1 to length(pattern) do*/
    _1317 = 12;
    {
        int _i_2706;
        _i_2706 = 1;
L1: 
        if (_i_2706 > 12){
            goto L2; // [22] 927
        }

        /** 		ch = pattern[i]*/
        _2 = (int)SEQ_PTR(_pattern_2699);
        _ch_2702 = (int)*(((s1_ptr)_2)->base + _i_2706);

        /** 		if in_fmt then*/
        if (_in_fmt_2701 == 0)
        {
            goto L3; // [39] 897
        }
        else{
        }

        /** 			in_fmt = 0*/
        _in_fmt_2701 = 0;

        /** 			if ch = '%' then*/
        if (_ch_2702 != 37)
        goto L4; // [51] 64

        /** 				res &= '%'*/
        Append(&_res_2704, _res_2704, 37);
        goto L5; // [61] 920
L4: 

        /** 			elsif ch = 'a' then*/
        if (_ch_2702 != 97)
        goto L6; // [66] 110

        /** 				res &= day_abbrs[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_2698);
        _0 = _weeks_day_1__tmp_at73_2717;
        _weeks_day_1__tmp_at73_2717 = _16julianDay(_d_2698);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at73_2718);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at73_2717)) {
            _weeks_day_2__tmp_at73_2718 = _weeks_day_1__tmp_at73_2717 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at73_2718 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at73_2718 = NewDouble((double)_weeks_day_2__tmp_at73_2718);
            }
        }
        else {
            _weeks_day_2__tmp_at73_2718 = binary_op(MINUS, _weeks_day_1__tmp_at73_2717, 1);
        }
        DeRef(_weeks_day_3__tmp_at73_2719);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at73_2718)) {
            _weeks_day_3__tmp_at73_2719 = _weeks_day_2__tmp_at73_2718 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at73_2719 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at73_2719 = NewDouble((double)_weeks_day_3__tmp_at73_2719);
        }
        else {
            _weeks_day_3__tmp_at73_2719 = binary_op(PLUS, _weeks_day_2__tmp_at73_2718, 4094);
        }
        DeRef(_weeks_day_4__tmp_at73_2720);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at73_2719)) {
            _weeks_day_4__tmp_at73_2720 = (_weeks_day_3__tmp_at73_2719 % 7);
        }
        else {
            _weeks_day_4__tmp_at73_2720 = binary_op(REMAINDER, _weeks_day_3__tmp_at73_2719, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_73_2716);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at73_2720)) {
            _weeks_day_inlined_weeks_day_at_73_2716 = _weeks_day_4__tmp_at73_2720 + 1;
            if (_weeks_day_inlined_weeks_day_at_73_2716 > MAXINT){
                _weeks_day_inlined_weeks_day_at_73_2716 = NewDouble((double)_weeks_day_inlined_weeks_day_at_73_2716);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_73_2716 = binary_op(PLUS, 1, _weeks_day_4__tmp_at73_2720);
        DeRef(_weeks_day_1__tmp_at73_2717);
        _weeks_day_1__tmp_at73_2717 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at73_2718);
        _weeks_day_2__tmp_at73_2718 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at73_2719);
        _weeks_day_3__tmp_at73_2719 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at73_2720);
        _weeks_day_4__tmp_at73_2720 = NOVALUE;
        _2 = (int)SEQ_PTR(_16day_abbrs_2489);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_73_2716)){
            _1322 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_73_2716)->dbl));
        }
        else{
            _1322 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_73_2716);
        }
        Concat((object_ptr)&_res_2704, _res_2704, _1322);
        _1322 = NOVALUE;
        goto L5; // [107] 920
L6: 

        /** 			elsif ch = 'A' then*/
        if (_ch_2702 != 65)
        goto L7; // [112] 156

        /** 				res &= day_names[weeks_day(d)]*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_2698);
        _0 = _weeks_day_1__tmp_at119_2727;
        _weeks_day_1__tmp_at119_2727 = _16julianDay(_d_2698);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at119_2728);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at119_2727)) {
            _weeks_day_2__tmp_at119_2728 = _weeks_day_1__tmp_at119_2727 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at119_2728 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at119_2728 = NewDouble((double)_weeks_day_2__tmp_at119_2728);
            }
        }
        else {
            _weeks_day_2__tmp_at119_2728 = binary_op(MINUS, _weeks_day_1__tmp_at119_2727, 1);
        }
        DeRef(_weeks_day_3__tmp_at119_2729);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at119_2728)) {
            _weeks_day_3__tmp_at119_2729 = _weeks_day_2__tmp_at119_2728 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at119_2729 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at119_2729 = NewDouble((double)_weeks_day_3__tmp_at119_2729);
        }
        else {
            _weeks_day_3__tmp_at119_2729 = binary_op(PLUS, _weeks_day_2__tmp_at119_2728, 4094);
        }
        DeRef(_weeks_day_4__tmp_at119_2730);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at119_2729)) {
            _weeks_day_4__tmp_at119_2730 = (_weeks_day_3__tmp_at119_2729 % 7);
        }
        else {
            _weeks_day_4__tmp_at119_2730 = binary_op(REMAINDER, _weeks_day_3__tmp_at119_2729, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_119_2726);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at119_2730)) {
            _weeks_day_inlined_weeks_day_at_119_2726 = _weeks_day_4__tmp_at119_2730 + 1;
            if (_weeks_day_inlined_weeks_day_at_119_2726 > MAXINT){
                _weeks_day_inlined_weeks_day_at_119_2726 = NewDouble((double)_weeks_day_inlined_weeks_day_at_119_2726);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_119_2726 = binary_op(PLUS, 1, _weeks_day_4__tmp_at119_2730);
        DeRef(_weeks_day_1__tmp_at119_2727);
        _weeks_day_1__tmp_at119_2727 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at119_2728);
        _weeks_day_2__tmp_at119_2728 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at119_2729);
        _weeks_day_3__tmp_at119_2729 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at119_2730);
        _weeks_day_4__tmp_at119_2730 = NOVALUE;
        _2 = (int)SEQ_PTR(_16day_names_2480);
        if (!IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_119_2726)){
            _1325 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_weeks_day_inlined_weeks_day_at_119_2726)->dbl));
        }
        else{
            _1325 = (int)*(((s1_ptr)_2)->base + _weeks_day_inlined_weeks_day_at_119_2726);
        }
        Concat((object_ptr)&_res_2704, _res_2704, _1325);
        _1325 = NOVALUE;
        goto L5; // [153] 920
L7: 

        /** 			elsif ch = 'b' then*/
        if (_ch_2702 != 98)
        goto L8; // [158] 183

        /** 				res &= month_abbrs[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1328 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_16month_abbrs_2467);
        if (!IS_ATOM_INT(_1328)){
            _1329 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_1328)->dbl));
        }
        else{
            _1329 = (int)*(((s1_ptr)_2)->base + _1328);
        }
        Concat((object_ptr)&_res_2704, _res_2704, _1329);
        _1329 = NOVALUE;
        goto L5; // [180] 920
L8: 

        /** 			elsif ch = 'B' then*/
        if (_ch_2702 != 66)
        goto L9; // [185] 210

        /** 				res &= month_names[d[MONTH]]*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1332 = (int)*(((s1_ptr)_2)->base + 2);
        _2 = (int)SEQ_PTR(_16month_names_2453);
        if (!IS_ATOM_INT(_1332)){
            _1333 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_1332)->dbl));
        }
        else{
            _1333 = (int)*(((s1_ptr)_2)->base + _1332);
        }
        Concat((object_ptr)&_res_2704, _res_2704, _1333);
        _1333 = NOVALUE;
        goto L5; // [207] 920
L9: 

        /** 			elsif ch = 'C' then*/
        if (_ch_2702 != 67)
        goto LA; // [212] 239

        /** 				res &= sprintf("%02d", d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1337 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_1337)) {
            _1338 = (_1337 % 100) ? NewDouble((double)_1337 / 100) : (_1337 / 100);
        }
        else {
            _1338 = binary_op(DIVIDE, _1337, 100);
        }
        _1337 = NOVALUE;
        _1339 = EPrintf(-9999999, _1336, _1338);
        DeRef(_1338);
        _1338 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1339);
        DeRefDS(_1339);
        _1339 = NOVALUE;
        goto L5; // [236] 920
LA: 

        /** 			elsif ch = 'd' then*/
        if (_ch_2702 != 100)
        goto LB; // [241] 264

        /** 				res &= sprintf("%02d", d[DAY])*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1342 = (int)*(((s1_ptr)_2)->base + 3);
        _1343 = EPrintf(-9999999, _1336, _1342);
        _1342 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1343);
        DeRefDS(_1343);
        _1343 = NOVALUE;
        goto L5; // [261] 920
LB: 

        /** 			elsif ch = 'H' then*/
        if (_ch_2702 != 72)
        goto LC; // [266] 289

        /** 				res &= sprintf("%02d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1346 = (int)*(((s1_ptr)_2)->base + 4);
        _1347 = EPrintf(-9999999, _1336, _1346);
        _1346 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1347);
        DeRefDS(_1347);
        _1347 = NOVALUE;
        goto L5; // [286] 920
LC: 

        /** 			elsif ch = 'I' then*/
        if (_ch_2702 != 73)
        goto LD; // [291] 352

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_2698);
        _tmp_2703 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_2703)){
            _tmp_2703 = (long)DBL_PTR(_tmp_2703)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_2703 <= 12)
        goto LE; // [309] 324

        /** 					tmp -= 12*/
        _tmp_2703 = _tmp_2703 - 12;
        goto LF; // [321] 339
LE: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_2703 != 0)
        goto L10; // [326] 338

        /** 					tmp = 12*/
        _tmp_2703 = 12;
L10: 
LF: 

        /** 				res &= sprintf("%02d", tmp)*/
        _1355 = EPrintf(-9999999, _1336, _tmp_2703);
        Concat((object_ptr)&_res_2704, _res_2704, _1355);
        DeRefDS(_1355);
        _1355 = NOVALUE;
        goto L5; // [349] 920
LD: 

        /** 			elsif ch = 'j' then*/
        if (_ch_2702 != 106)
        goto L11; // [354] 375

        /** 				res &= sprintf("%d", julianDayOfYear(d))*/
        Ref(_d_2698);
        _1359 = _16julianDayOfYear(_d_2698);
        _1360 = EPrintf(-9999999, _952, _1359);
        DeRef(_1359);
        _1359 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1360);
        DeRefDS(_1360);
        _1360 = NOVALUE;
        goto L5; // [372] 920
L11: 

        /** 			elsif ch = 'k' then*/
        if (_ch_2702 != 107)
        goto L12; // [377] 400

        /** 				res &= sprintf("%d", d[HOUR])*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1364 = (int)*(((s1_ptr)_2)->base + 4);
        _1365 = EPrintf(-9999999, _952, _1364);
        _1364 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1365);
        DeRefDS(_1365);
        _1365 = NOVALUE;
        goto L5; // [397] 920
L12: 

        /** 			elsif ch = 'l' then*/
        if (_ch_2702 != 108)
        goto L13; // [402] 463

        /** 				tmp = d[HOUR]*/
        _2 = (int)SEQ_PTR(_d_2698);
        _tmp_2703 = (int)*(((s1_ptr)_2)->base + 4);
        if (!IS_ATOM_INT(_tmp_2703)){
            _tmp_2703 = (long)DBL_PTR(_tmp_2703)->dbl;
        }

        /** 				if tmp > 12 then*/
        if (_tmp_2703 <= 12)
        goto L14; // [420] 435

        /** 					tmp -= 12*/
        _tmp_2703 = _tmp_2703 - 12;
        goto L15; // [432] 450
L14: 

        /** 				elsif tmp = 0 then*/
        if (_tmp_2703 != 0)
        goto L16; // [437] 449

        /** 					tmp = 12*/
        _tmp_2703 = 12;
L16: 
L15: 

        /** 				res &= sprintf("%d", tmp)*/
        _1373 = EPrintf(-9999999, _952, _tmp_2703);
        Concat((object_ptr)&_res_2704, _res_2704, _1373);
        DeRefDS(_1373);
        _1373 = NOVALUE;
        goto L5; // [460] 920
L13: 

        /** 			elsif ch = 'm' then*/
        if (_ch_2702 != 109)
        goto L17; // [465] 488

        /** 				res &= sprintf("%02d", d[MONTH])*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1377 = (int)*(((s1_ptr)_2)->base + 2);
        _1378 = EPrintf(-9999999, _1336, _1377);
        _1377 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1378);
        DeRefDS(_1378);
        _1378 = NOVALUE;
        goto L5; // [485] 920
L17: 

        /** 			elsif ch = 'M' then*/
        if (_ch_2702 != 77)
        goto L18; // [490] 513

        /** 				res &= sprintf("%02d", d[MINUTE])*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1382 = (int)*(((s1_ptr)_2)->base + 5);
        _1383 = EPrintf(-9999999, _1336, _1382);
        _1382 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1383);
        DeRefDS(_1383);
        _1383 = NOVALUE;
        goto L5; // [510] 920
L18: 

        /** 			elsif ch = 'p' then*/
        if (_ch_2702 != 112)
        goto L19; // [515] 562

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1387 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _1387, 12)){
            _1387 = NOVALUE;
            goto L1A; // [527] 546
        }
        _1387 = NOVALUE;

        /** 					res &= ampm[1]*/
        _2 = (int)SEQ_PTR(_16ampm_2498);
        _1389 = (int)*(((s1_ptr)_2)->base + 1);
        Concat((object_ptr)&_res_2704, _res_2704, _1389);
        _1389 = NOVALUE;
        goto L5; // [543] 920
L1A: 

        /** 					res &= ampm[2]*/
        _2 = (int)SEQ_PTR(_16ampm_2498);
        _1391 = (int)*(((s1_ptr)_2)->base + 2);
        Concat((object_ptr)&_res_2704, _res_2704, _1391);
        _1391 = NOVALUE;
        goto L5; // [559] 920
L19: 

        /** 			elsif ch = 'P' then*/
        if (_ch_2702 != 80)
        goto L1B; // [564] 619

        /** 				if d[HOUR] <= 12 then*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1395 = (int)*(((s1_ptr)_2)->base + 4);
        if (binary_op_a(GREATER, _1395, 12)){
            _1395 = NOVALUE;
            goto L1C; // [576] 599
        }
        _1395 = NOVALUE;

        /** 					res &= tolower(ampm[1])*/
        _2 = (int)SEQ_PTR(_16ampm_2498);
        _1397 = (int)*(((s1_ptr)_2)->base + 1);
        RefDS(_1397);
        _1398 = _16tolower(_1397);
        _1397 = NOVALUE;
        if (IS_SEQUENCE(_res_2704) && IS_ATOM(_1398)) {
            Ref(_1398);
            Append(&_res_2704, _res_2704, _1398);
        }
        else if (IS_ATOM(_res_2704) && IS_SEQUENCE(_1398)) {
        }
        else {
            Concat((object_ptr)&_res_2704, _res_2704, _1398);
        }
        DeRef(_1398);
        _1398 = NOVALUE;
        goto L5; // [596] 920
L1C: 

        /** 					res &= tolower(ampm[2])*/
        _2 = (int)SEQ_PTR(_16ampm_2498);
        _1400 = (int)*(((s1_ptr)_2)->base + 2);
        RefDS(_1400);
        _1401 = _16tolower(_1400);
        _1400 = NOVALUE;
        if (IS_SEQUENCE(_res_2704) && IS_ATOM(_1401)) {
            Ref(_1401);
            Append(&_res_2704, _res_2704, _1401);
        }
        else if (IS_ATOM(_res_2704) && IS_SEQUENCE(_1401)) {
        }
        else {
            Concat((object_ptr)&_res_2704, _res_2704, _1401);
        }
        DeRef(_1401);
        _1401 = NOVALUE;
        goto L5; // [616] 920
L1B: 

        /** 			elsif ch = 's' then*/
        if (_ch_2702 != 115)
        goto L1D; // [621] 651

        /** 				res &= sprintf("%d", to_unix(d))*/

        /** 	return datetimeToSeconds(dt) - EPOCH_1970*/
        Ref(_d_2698);
        _0 = _to_unix_1__tmp_at626_2835;
        _to_unix_1__tmp_at626_2835 = _16datetimeToSeconds(_d_2698);
        DeRef(_0);
        DeRef(_to_unix_inlined_to_unix_at_626_2834);
        _to_unix_inlined_to_unix_at_626_2834 = binary_op(MINUS, _to_unix_1__tmp_at626_2835, _16EPOCH_1970_2245);
        DeRef(_to_unix_1__tmp_at626_2835);
        _to_unix_1__tmp_at626_2835 = NOVALUE;
        _1405 = EPrintf(-9999999, _952, _to_unix_inlined_to_unix_at_626_2834);
        Concat((object_ptr)&_res_2704, _res_2704, _1405);
        DeRefDS(_1405);
        _1405 = NOVALUE;
        goto L5; // [648] 920
L1D: 

        /** 			elsif ch = 'S' then*/
        if (_ch_2702 != 83)
        goto L1E; // [653] 676

        /** 				res &= sprintf("%02d", d[SECOND])*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1409 = (int)*(((s1_ptr)_2)->base + 6);
        _1410 = EPrintf(-9999999, _1336, _1409);
        _1409 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1410);
        DeRefDS(_1410);
        _1410 = NOVALUE;
        goto L5; // [673] 920
L1E: 

        /** 			elsif ch = 'u' then*/
        if (_ch_2702 != 117)
        goto L1F; // [678] 771

        /** 				tmp = weeks_day(d)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_2698);
        _0 = _weeks_day_1__tmp_at683_2849;
        _weeks_day_1__tmp_at683_2849 = _16julianDay(_d_2698);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at683_2850);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at683_2849)) {
            _weeks_day_2__tmp_at683_2850 = _weeks_day_1__tmp_at683_2849 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at683_2850 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at683_2850 = NewDouble((double)_weeks_day_2__tmp_at683_2850);
            }
        }
        else {
            _weeks_day_2__tmp_at683_2850 = binary_op(MINUS, _weeks_day_1__tmp_at683_2849, 1);
        }
        DeRef(_weeks_day_3__tmp_at683_2851);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at683_2850)) {
            _weeks_day_3__tmp_at683_2851 = _weeks_day_2__tmp_at683_2850 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at683_2851 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at683_2851 = NewDouble((double)_weeks_day_3__tmp_at683_2851);
        }
        else {
            _weeks_day_3__tmp_at683_2851 = binary_op(PLUS, _weeks_day_2__tmp_at683_2850, 4094);
        }
        DeRef(_weeks_day_4__tmp_at683_2852);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at683_2851)) {
            _weeks_day_4__tmp_at683_2852 = (_weeks_day_3__tmp_at683_2851 % 7);
        }
        else {
            _weeks_day_4__tmp_at683_2852 = binary_op(REMAINDER, _weeks_day_3__tmp_at683_2851, 7);
        }
        if (IS_ATOM_INT(_weeks_day_4__tmp_at683_2852)) {
            _tmp_2703 = _weeks_day_4__tmp_at683_2852 + 1;
        }
        else
        { // coercing _tmp_2703 to an integer 1
            _tmp_2703 = binary_op(PLUS, 1, _weeks_day_4__tmp_at683_2852);
            if( !IS_ATOM_INT(_tmp_2703) ){
                _tmp_2703 = (object)DBL_PTR(_tmp_2703)->dbl;
            }
        }
        DeRef(_weeks_day_1__tmp_at683_2849);
        _weeks_day_1__tmp_at683_2849 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at683_2850);
        _weeks_day_2__tmp_at683_2850 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at683_2851);
        _weeks_day_3__tmp_at683_2851 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at683_2852);
        _weeks_day_4__tmp_at683_2852 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_2703)) {
            _1 = (long)(DBL_PTR(_tmp_2703)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_2703)) && (DBL_PTR(_tmp_2703)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_2703);
            _tmp_2703 = _1;
        }

        /** 				if tmp = 1 then*/
        if (_tmp_2703 != 1)
        goto L20; // [715] 728

        /** 					res &= "7" -- Sunday*/
        Concat((object_ptr)&_res_2704, _res_2704, _1415);
        goto L5; // [725] 920
L20: 

        /** 					res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_2698);
        _0 = _weeks_day_1__tmp_at729_2860;
        _weeks_day_1__tmp_at729_2860 = _16julianDay(_d_2698);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at729_2861);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at729_2860)) {
            _weeks_day_2__tmp_at729_2861 = _weeks_day_1__tmp_at729_2860 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at729_2861 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at729_2861 = NewDouble((double)_weeks_day_2__tmp_at729_2861);
            }
        }
        else {
            _weeks_day_2__tmp_at729_2861 = binary_op(MINUS, _weeks_day_1__tmp_at729_2860, 1);
        }
        DeRef(_weeks_day_3__tmp_at729_2862);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at729_2861)) {
            _weeks_day_3__tmp_at729_2862 = _weeks_day_2__tmp_at729_2861 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at729_2862 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at729_2862 = NewDouble((double)_weeks_day_3__tmp_at729_2862);
        }
        else {
            _weeks_day_3__tmp_at729_2862 = binary_op(PLUS, _weeks_day_2__tmp_at729_2861, 4094);
        }
        DeRef(_weeks_day_4__tmp_at729_2863);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at729_2862)) {
            _weeks_day_4__tmp_at729_2863 = (_weeks_day_3__tmp_at729_2862 % 7);
        }
        else {
            _weeks_day_4__tmp_at729_2863 = binary_op(REMAINDER, _weeks_day_3__tmp_at729_2862, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_729_2859);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at729_2863)) {
            _weeks_day_inlined_weeks_day_at_729_2859 = _weeks_day_4__tmp_at729_2863 + 1;
            if (_weeks_day_inlined_weeks_day_at_729_2859 > MAXINT){
                _weeks_day_inlined_weeks_day_at_729_2859 = NewDouble((double)_weeks_day_inlined_weeks_day_at_729_2859);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_729_2859 = binary_op(PLUS, 1, _weeks_day_4__tmp_at729_2863);
        DeRef(_weeks_day_1__tmp_at729_2860);
        _weeks_day_1__tmp_at729_2860 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at729_2861);
        _weeks_day_2__tmp_at729_2861 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at729_2862);
        _weeks_day_3__tmp_at729_2862 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at729_2863);
        _weeks_day_4__tmp_at729_2863 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_729_2859)) {
            _1417 = _weeks_day_inlined_weeks_day_at_729_2859 - 1;
            if ((long)((unsigned long)_1417 +(unsigned long) HIGH_BITS) >= 0){
                _1417 = NewDouble((double)_1417);
            }
        }
        else {
            _1417 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_729_2859, 1);
        }
        _1418 = EPrintf(-9999999, _952, _1417);
        DeRef(_1417);
        _1417 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1418);
        DeRefDS(_1418);
        _1418 = NOVALUE;
        goto L5; // [768] 920
L1F: 

        /** 			elsif ch = 'w' then*/
        if (_ch_2702 != 119)
        goto L21; // [773] 819

        /** 				res &= sprintf("%d", weeks_day(d) - 1)*/

        /** 	return remainder(julianDay(dt)-1+4094, 7) + 1*/
        Ref(_d_2698);
        _0 = _weeks_day_1__tmp_at778_2872;
        _weeks_day_1__tmp_at778_2872 = _16julianDay(_d_2698);
        DeRef(_0);
        DeRef(_weeks_day_2__tmp_at778_2873);
        if (IS_ATOM_INT(_weeks_day_1__tmp_at778_2872)) {
            _weeks_day_2__tmp_at778_2873 = _weeks_day_1__tmp_at778_2872 - 1;
            if ((long)((unsigned long)_weeks_day_2__tmp_at778_2873 +(unsigned long) HIGH_BITS) >= 0){
                _weeks_day_2__tmp_at778_2873 = NewDouble((double)_weeks_day_2__tmp_at778_2873);
            }
        }
        else {
            _weeks_day_2__tmp_at778_2873 = binary_op(MINUS, _weeks_day_1__tmp_at778_2872, 1);
        }
        DeRef(_weeks_day_3__tmp_at778_2874);
        if (IS_ATOM_INT(_weeks_day_2__tmp_at778_2873)) {
            _weeks_day_3__tmp_at778_2874 = _weeks_day_2__tmp_at778_2873 + 4094;
            if ((long)((unsigned long)_weeks_day_3__tmp_at778_2874 + (unsigned long)HIGH_BITS) >= 0) 
            _weeks_day_3__tmp_at778_2874 = NewDouble((double)_weeks_day_3__tmp_at778_2874);
        }
        else {
            _weeks_day_3__tmp_at778_2874 = binary_op(PLUS, _weeks_day_2__tmp_at778_2873, 4094);
        }
        DeRef(_weeks_day_4__tmp_at778_2875);
        if (IS_ATOM_INT(_weeks_day_3__tmp_at778_2874)) {
            _weeks_day_4__tmp_at778_2875 = (_weeks_day_3__tmp_at778_2874 % 7);
        }
        else {
            _weeks_day_4__tmp_at778_2875 = binary_op(REMAINDER, _weeks_day_3__tmp_at778_2874, 7);
        }
        DeRef(_weeks_day_inlined_weeks_day_at_778_2871);
        if (IS_ATOM_INT(_weeks_day_4__tmp_at778_2875)) {
            _weeks_day_inlined_weeks_day_at_778_2871 = _weeks_day_4__tmp_at778_2875 + 1;
            if (_weeks_day_inlined_weeks_day_at_778_2871 > MAXINT){
                _weeks_day_inlined_weeks_day_at_778_2871 = NewDouble((double)_weeks_day_inlined_weeks_day_at_778_2871);
            }
        }
        else
        _weeks_day_inlined_weeks_day_at_778_2871 = binary_op(PLUS, 1, _weeks_day_4__tmp_at778_2875);
        DeRef(_weeks_day_1__tmp_at778_2872);
        _weeks_day_1__tmp_at778_2872 = NOVALUE;
        DeRef(_weeks_day_2__tmp_at778_2873);
        _weeks_day_2__tmp_at778_2873 = NOVALUE;
        DeRef(_weeks_day_3__tmp_at778_2874);
        _weeks_day_3__tmp_at778_2874 = NOVALUE;
        DeRef(_weeks_day_4__tmp_at778_2875);
        _weeks_day_4__tmp_at778_2875 = NOVALUE;
        if (IS_ATOM_INT(_weeks_day_inlined_weeks_day_at_778_2871)) {
            _1422 = _weeks_day_inlined_weeks_day_at_778_2871 - 1;
            if ((long)((unsigned long)_1422 +(unsigned long) HIGH_BITS) >= 0){
                _1422 = NewDouble((double)_1422);
            }
        }
        else {
            _1422 = binary_op(MINUS, _weeks_day_inlined_weeks_day_at_778_2871, 1);
        }
        _1423 = EPrintf(-9999999, _952, _1422);
        DeRef(_1422);
        _1422 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1423);
        DeRefDS(_1423);
        _1423 = NOVALUE;
        goto L5; // [816] 920
L21: 

        /** 			elsif ch = 'y' then*/
        if (_ch_2702 != 121)
        goto L22; // [821] 868

        /** 			   tmp = floor(d[YEAR] / 100)*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1427 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_1427)) {
            if (100 > 0 && _1427 >= 0) {
                _tmp_2703 = _1427 / 100;
            }
            else {
                temp_dbl = floor((double)_1427 / (double)100);
                _tmp_2703 = (long)temp_dbl;
            }
        }
        else {
            _2 = binary_op(DIVIDE, _1427, 100);
            _tmp_2703 = unary_op(FLOOR, _2);
            DeRef(_2);
        }
        _1427 = NOVALUE;
        if (!IS_ATOM_INT(_tmp_2703)) {
            _1 = (long)(DBL_PTR(_tmp_2703)->dbl);
            if (UNIQUE(DBL_PTR(_tmp_2703)) && (DBL_PTR(_tmp_2703)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_tmp_2703);
            _tmp_2703 = _1;
        }

        /** 			   res &= sprintf("%02d", d[YEAR] - (tmp * 100))*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1429 = (int)*(((s1_ptr)_2)->base + 1);
        if (_tmp_2703 == (short)_tmp_2703)
        _1430 = _tmp_2703 * 100;
        else
        _1430 = NewDouble(_tmp_2703 * (double)100);
        if (IS_ATOM_INT(_1429) && IS_ATOM_INT(_1430)) {
            _1431 = _1429 - _1430;
            if ((long)((unsigned long)_1431 +(unsigned long) HIGH_BITS) >= 0){
                _1431 = NewDouble((double)_1431);
            }
        }
        else {
            _1431 = binary_op(MINUS, _1429, _1430);
        }
        _1429 = NOVALUE;
        DeRef(_1430);
        _1430 = NOVALUE;
        _1432 = EPrintf(-9999999, _1336, _1431);
        DeRef(_1431);
        _1431 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1432);
        DeRefDS(_1432);
        _1432 = NOVALUE;
        goto L5; // [865] 920
L22: 

        /** 			elsif ch = 'Y' then*/
        if (_ch_2702 != 89)
        goto L5; // [870] 920

        /** 				res &= sprintf("%04d", d[YEAR])*/
        _2 = (int)SEQ_PTR(_d_2698);
        _1437 = (int)*(((s1_ptr)_2)->base + 1);
        _1438 = EPrintf(-9999999, _1436, _1437);
        _1437 = NOVALUE;
        Concat((object_ptr)&_res_2704, _res_2704, _1438);
        DeRefDS(_1438);
        _1438 = NOVALUE;
        goto L5; // [890] 920
        goto L5; // [894] 920
L3: 

        /** 		elsif ch = '%' then*/
        if (_ch_2702 != 37)
        goto L23; // [899] 913

        /** 			in_fmt = 1*/
        _in_fmt_2701 = 1;
        goto L5; // [910] 920
L23: 

        /** 			res &= ch*/
        Append(&_res_2704, _res_2704, _ch_2702);
L5: 

        /** 	end for*/
        _i_2706 = _i_2706 + 1;
        goto L1; // [922] 29
L2: 
        ;
    }

    /** 	return res*/
    DeRef(_d_2698);
    DeRefDSi(_pattern_2699);
    _1328 = NOVALUE;
    _1332 = NOVALUE;
    return _res_2704;
    ;
}



// 0x62608848
