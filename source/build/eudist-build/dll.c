// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _10open_dll(int _file_name_848)
{
    int _fh_858 = NOVALUE;
    int _346 = NOVALUE;
    int _344 = NOVALUE;
    int _343 = NOVALUE;
    int _342 = NOVALUE;
    int _341 = NOVALUE;
    int _340 = NOVALUE;
    int _339 = NOVALUE;
    int _338 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(file_name) > 0 and types:string(file_name) then*/
    if (IS_SEQUENCE(_file_name_848)){
            _338 = SEQ_PTR(_file_name_848)->length;
    }
    else {
        _338 = 1;
    }
    _339 = (_338 > 0);
    _338 = NOVALUE;
    if (_339 == 0) {
        goto L1; // [12] 35
    }
    RefDS(_file_name_848);
    _341 = _11string(_file_name_848);
    if (_341 == 0) {
        DeRef(_341);
        _341 = NOVALUE;
        goto L1; // [21] 35
    }
    else {
        if (!IS_ATOM_INT(_341) && DBL_PTR(_341)->dbl == 0.0){
            DeRef(_341);
            _341 = NOVALUE;
            goto L1; // [21] 35
        }
        DeRef(_341);
        _341 = NOVALUE;
    }
    DeRef(_341);
    _341 = NOVALUE;

    /** 		return machine_func(M_OPEN_DLL, file_name)*/
    _342 = machine(50, _file_name_848);
    DeRefDSi(_file_name_848);
    DeRef(_339);
    _339 = NOVALUE;
    return _342;
L1: 

    /** 	for idx = 1 to length(file_name) do*/
    if (IS_SEQUENCE(_file_name_848)){
            _343 = SEQ_PTR(_file_name_848)->length;
    }
    else {
        _343 = 1;
    }
    {
        int _idx_856;
        _idx_856 = 1;
L2: 
        if (_idx_856 > _343){
            goto L3; // [40] 82
        }

        /** 		atom fh = machine_func(M_OPEN_DLL, file_name[idx])*/
        _2 = (int)SEQ_PTR(_file_name_848);
        _344 = (int)*(((s1_ptr)_2)->base + _idx_856);
        DeRef(_fh_858);
        _fh_858 = machine(50, _344);
        _344 = NOVALUE;

        /** 		if not fh = 0 then*/
        if (IS_ATOM_INT(_fh_858)) {
            _346 = (_fh_858 == 0);
        }
        else {
            _346 = unary_op(NOT, _fh_858);
        }
        if (_346 != 0)
        goto L4; // [62] 73

        /** 			return fh*/
        DeRefDSi(_file_name_848);
        DeRef(_339);
        _339 = NOVALUE;
        DeRef(_342);
        _342 = NOVALUE;
        _346 = NOVALUE;
        return _fh_858;
L4: 
        DeRef(_fh_858);
        _fh_858 = NOVALUE;

        /** 	end for*/
        _idx_856 = _idx_856 + 1;
        goto L2; // [77] 47
L3: 
        ;
    }

    /** 	return 0*/
    DeRefDSi(_file_name_848);
    DeRef(_339);
    _339 = NOVALUE;
    DeRef(_342);
    _342 = NOVALUE;
    DeRef(_346);
    _346 = NOVALUE;
    return 0;
    ;
}


int _10define_c_proc(int _lib_872, int _routine_name_873, int _arg_types_874)
{
    int _safe_address_inlined_safe_address_at_13_879 = NOVALUE;
    int _msg_inlined_crash_at_28_883 = NOVALUE;
    int _355 = NOVALUE;
    int _354 = NOVALUE;
    int _352 = NOVALUE;
    int _351 = NOVALUE;
    int _350 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _350 = 0;
    if (_350 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_879 = 1;
    _352 = (1 == 0);
    if (_352 == 0)
    {
        DeRef(_352);
        _352 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_352);
        _352 = NOVALUE;
    }

    /**         error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_883);
    _msg_inlined_crash_at_28_883 = EPrintf(-9999999, _353, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_883);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_883);
    _msg_inlined_crash_at_28_883 = NOVALUE;
L1: 

    /** 	return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, 0})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_872);
    *((int *)(_2+4)) = _lib_872;
    Ref(_routine_name_873);
    *((int *)(_2+8)) = _routine_name_873;
    RefDS(_arg_types_874);
    *((int *)(_2+12)) = _arg_types_874;
    *((int *)(_2+16)) = 0;
    _354 = MAKE_SEQ(_1);
    _355 = machine(51, _354);
    DeRefDS(_354);
    _354 = NOVALUE;
    DeRef(_lib_872);
    DeRef(_routine_name_873);
    DeRefDSi(_arg_types_874);
    return _355;
    ;
}


int _10define_c_func(int _lib_888, int _routine_name_889, int _arg_types_890, int _return_type_891)
{
    int _safe_address_inlined_safe_address_at_13_896 = NOVALUE;
    int _msg_inlined_crash_at_28_899 = NOVALUE;
    int _360 = NOVALUE;
    int _359 = NOVALUE;
    int _358 = NOVALUE;
    int _357 = NOVALUE;
    int _356 = NOVALUE;
    int _0, _1, _2;
    

    /** 	  if atom(routine_name) and not machine:safe_address(routine_name, 1, machine:A_EXECUTE) then*/
    _356 = 0;
    if (_356 == 0) {
        goto L1; // [8] 48
    }

    /** 	return 1*/
    _safe_address_inlined_safe_address_at_13_896 = 1;
    _358 = (1 == 0);
    if (_358 == 0)
    {
        DeRef(_358);
        _358 = NOVALUE;
        goto L1; // [24] 48
    }
    else{
        DeRef(_358);
        _358 = NOVALUE;
    }

    /** 	      error:crash("A C function is being defined from Non-executable memory.")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_28_899);
    _msg_inlined_crash_at_28_899 = EPrintf(-9999999, _353, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_28_899);

    /** end procedure*/
    goto L2; // [42] 45
L2: 
    DeRefi(_msg_inlined_crash_at_28_899);
    _msg_inlined_crash_at_28_899 = NOVALUE;
L1: 

    /** 	  return machine_func(M_DEFINE_C, {lib, routine_name, arg_types, return_type})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_lib_888);
    *((int *)(_2+4)) = _lib_888;
    Ref(_routine_name_889);
    *((int *)(_2+8)) = _routine_name_889;
    RefDS(_arg_types_890);
    *((int *)(_2+12)) = _arg_types_890;
    *((int *)(_2+16)) = _return_type_891;
    _359 = MAKE_SEQ(_1);
    _360 = machine(51, _359);
    DeRefDS(_359);
    _359 = NOVALUE;
    DeRef(_lib_888);
    DeRefi(_routine_name_889);
    DeRefDSi(_arg_types_890);
    return _360;
    ;
}



// 0x44F3C3C4
