// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _7allocate(int _n_912, int _cleanup_913)
{
    int _iaddr_914 = NOVALUE;
    int _eaddr_915 = NOVALUE;
    int _364 = NOVALUE;
    int _363 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE then*/

    /** 		iaddr = eu:machine_func( memconst:M_ALLOC, n + memory:BORDER_SPACE * 2)*/
    _363 = 0;
    _364 = _n_912 + 0;
    _363 = NOVALUE;
    DeRef(_iaddr_914);
    _iaddr_914 = machine(16, _364);
    _364 = NOVALUE;

    /** 		eaddr = memory:prepare_block( iaddr, n, PAGE_READ_WRITE )*/
    Ref(_iaddr_914);
    _0 = _eaddr_915;
    _eaddr_915 = _9prepare_block(_iaddr_914, _n_912, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 	return eaddr*/
    DeRef(_iaddr_914);
    return _eaddr_915;
    ;
}


int _7allocate_data(int _n_925, int _cleanup_926)
{
    int _a_927 = NOVALUE;
    int _sla_929 = NOVALUE;
    int _369 = NOVALUE;
    int _368 = NOVALUE;
    int _0, _1, _2;
    

    /** 	a = eu:machine_func( memconst:M_ALLOC, n+BORDER_SPACE*2)*/
    _368 = 0;
    _369 = 4;
    _368 = NOVALUE;
    DeRef(_a_927);
    _a_927 = machine(16, 4);
    _369 = NOVALUE;

    /** 	sla = memory:prepare_block(a, n, PAGE_READ_WRITE )*/
    Ref(_a_927);
    _0 = _sla_929;
    _sla_929 = _9prepare_block(_a_927, 4, 4);
    DeRef(_0);

    /** 	if cleanup then*/

    /** 		return sla*/
    DeRef(_a_927);
    return _sla_929;
    ;
}


int _7VirtualAlloc(int _addr_1088, int _size_1089, int _allocation_type_1090, int _protect__1091)
{
    int _r1_1092 = NOVALUE;
    int _454 = NOVALUE;
    int _0, _1, _2;
    

    /** 		r1 = c_func( VirtualAlloc_rid, {addr, size, allocation_type, protect_ } )*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 1;
    Ref(_allocation_type_1090);
    *((int *)(_2+12)) = _allocation_type_1090;
    *((int *)(_2+16)) = 64;
    _454 = MAKE_SEQ(_1);
    DeRef(_r1_1092);
    _r1_1092 = call_c(1, _7VirtualAlloc_rid_1025, _454);
    DeRefDS(_454);
    _454 = NOVALUE;

    /** 		return r1*/
    DeRef(_allocation_type_1090);
    return _r1_1092;
    ;
}


int _7allocate_code(int _data_1104, int _wordsize_1105)
{
    int _456 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return allocate_protect( data, wordsize, PAGE_EXECUTE )*/
    RefDS(_data_1104);
    _456 = _7allocate_protect(_data_1104, 1, 16);
    DeRefDSi(_data_1104);
    return _456;
    ;
}


int _7allocate_string(int _s_1111, int _cleanup_1112)
{
    int _mem_1113 = NOVALUE;
    int _461 = NOVALUE;
    int _460 = NOVALUE;
    int _458 = NOVALUE;
    int _457 = NOVALUE;
    int _0, _1, _2;
    

    /** 	mem = allocate( length(s) + 1) -- Thanks to Igor*/
    if (IS_SEQUENCE(_s_1111)){
            _457 = SEQ_PTR(_s_1111)->length;
    }
    else {
        _457 = 1;
    }
    _458 = _457 + 1;
    _457 = NOVALUE;
    _0 = _mem_1113;
    _mem_1113 = _7allocate(_458, 0);
    DeRef(_0);
    _458 = NOVALUE;

    /** 	if mem then*/
    if (_mem_1113 == 0) {
        goto L1; // [19] 54
    }
    else {
        if (!IS_ATOM_INT(_mem_1113) && DBL_PTR(_mem_1113)->dbl == 0.0){
            goto L1; // [19] 54
        }
    }

    /** 		poke(mem, s)*/
    if (IS_ATOM_INT(_mem_1113)){
        poke_addr = (unsigned char *)_mem_1113;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_mem_1113)->dbl);
    }
    _1 = (int)SEQ_PTR(_s_1111);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 		poke(mem+length(s), 0)  -- Thanks to Aku*/
    if (IS_SEQUENCE(_s_1111)){
            _460 = SEQ_PTR(_s_1111)->length;
    }
    else {
        _460 = 1;
    }
    if (IS_ATOM_INT(_mem_1113)) {
        _461 = _mem_1113 + _460;
        if ((long)((unsigned long)_461 + (unsigned long)HIGH_BITS) >= 0) 
        _461 = NewDouble((double)_461);
    }
    else {
        _461 = NewDouble(DBL_PTR(_mem_1113)->dbl + (double)_460);
    }
    _460 = NOVALUE;
    if (IS_ATOM_INT(_461)){
        poke_addr = (unsigned char *)_461;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_461)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_461);
    _461 = NOVALUE;

    /** 		if cleanup then*/
L1: 

    /** 	return mem*/
    DeRefDS(_s_1111);
    return _mem_1113;
    ;
}


int _7allocate_protect(int _data_1124, int _wordsize_1125, int _protection_1127)
{
    int _iaddr_1128 = NOVALUE;
    int _eaddr_1130 = NOVALUE;
    int _size_1131 = NOVALUE;
    int _first_protection_1133 = NOVALUE;
    int _true_protection_1135 = NOVALUE;
    int _prepare_block_inlined_prepare_block_at_108_1150 = NOVALUE;
    int _msg_inlined_crash_at_192_1163 = NOVALUE;
    int _482 = NOVALUE;
    int _481 = NOVALUE;
    int _479 = NOVALUE;
    int _478 = NOVALUE;
    int _477 = NOVALUE;
    int _473 = NOVALUE;
    int _471 = NOVALUE;
    int _468 = NOVALUE;
    int _467 = NOVALUE;
    int _465 = NOVALUE;
    int _463 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom iaddr = 0*/
    DeRef(_iaddr_1128);
    _iaddr_1128 = 0;

    /** 	integer size*/

    /** 	valid_memory_protection_constant true_protection = protection*/
    _true_protection_1135 = 16;

    /** 	ifdef SAFE then	*/

    /** 	if atom(data) then*/
    _463 = 0;
    if (_463 == 0)
    {
        _463 = NOVALUE;
        goto L1; // [20] 41
    }
    else{
        _463 = NOVALUE;
    }

    /** 		size = data * wordsize*/
    _size_1131 = binary_op(MULTIPLY, _data_1124, 1);
    if (!IS_ATOM_INT(_size_1131)) {
        _1 = (long)(DBL_PTR(_size_1131)->dbl);
        if (UNIQUE(DBL_PTR(_size_1131)) && (DBL_PTR(_size_1131)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_1131);
        _size_1131 = _1;
    }

    /** 		first_protection = true_protection*/
    _first_protection_1133 = 16;
    goto L2; // [38] 62
L1: 

    /** 		size = length(data) * wordsize*/
    if (IS_SEQUENCE(_data_1124)){
            _465 = SEQ_PTR(_data_1124)->length;
    }
    else {
        _465 = 1;
    }
    _size_1131 = _465 * _wordsize_1125;
    _465 = NOVALUE;

    /** 		first_protection = PAGE_READ_WRITE*/
    _first_protection_1133 = 4;
L2: 

    /** 	iaddr = local_allocate_protected_memory( size + memory:BORDER_SPACE * 2, first_protection )*/
    _467 = 0;
    _468 = _size_1131 + 0;
    _467 = NOVALUE;
    _0 = _iaddr_1128;
    _iaddr_1128 = _7local_allocate_protected_memory(_468, _first_protection_1133);
    DeRef(_0);
    _468 = NOVALUE;

    /** 	if iaddr = 0 then*/
    if (binary_op_a(NOTEQ, _iaddr_1128, 0)){
        goto L3; // [83] 94
    }

    /** 		return 0*/
    DeRefi(_data_1124);
    DeRef(_iaddr_1128);
    DeRef(_eaddr_1130);
    return 0;
L3: 

    /** 	eaddr = memory:prepare_block( iaddr, size, protection )*/
    if (!IS_ATOM_INT(_protection_1127)) {
        _1 = (long)(DBL_PTR(_protection_1127)->dbl);
        if (UNIQUE(DBL_PTR(_protection_1127)) && (DBL_PTR(_protection_1127)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_1127);
        _protection_1127 = _1;
    }

    /** 	return addr*/
    Ref(_iaddr_1128);
    DeRef(_eaddr_1130);
    _eaddr_1130 = _iaddr_1128;

    /** 	if eaddr = 0 or atom( data ) then*/
    if (IS_ATOM_INT(_eaddr_1130)) {
        _471 = (_eaddr_1130 == 0);
    }
    else {
        _471 = (DBL_PTR(_eaddr_1130)->dbl == (double)0);
    }
    if (_471 != 0) {
        goto L4; // [112] 124
    }
    _473 = IS_ATOM(_data_1124);
    if (_473 == 0)
    {
        _473 = NOVALUE;
        goto L5; // [120] 131
    }
    else{
        _473 = NOVALUE;
    }
L4: 

    /** 		return eaddr*/
    DeRefi(_data_1124);
    DeRef(_iaddr_1128);
    DeRef(_471);
    _471 = NOVALUE;
    return _eaddr_1130;
L5: 

    /** 	switch wordsize do*/
    _0 = _wordsize_1125;
    switch ( _0 ){ 

        /** 		case 1 then*/
        case 1:

        /** 			eu:poke( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_1130)){
            poke_addr = (unsigned char *)_eaddr_1130;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_eaddr_1130)->dbl);
        }
        if (IS_ATOM_INT(_data_1124)) {
            *poke_addr = (unsigned char)_data_1124;
        }
        else if (IS_ATOM(_data_1124)) {
            _1 = (signed char)DBL_PTR(_data_1124)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_1124);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        goto L6; // [147] 196

        /** 		case 2 then*/
        case 2:

        /** 			eu:poke2( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_1130)){
            poke2_addr = (unsigned short *)_eaddr_1130;
        }
        else {
            poke2_addr = (unsigned short *)(unsigned long)(DBL_PTR(_eaddr_1130)->dbl);
        }
        if (IS_ATOM_INT(_data_1124)) {
            *poke2_addr = (unsigned short)_data_1124;
        }
        else if (IS_ATOM(_data_1124)) {
            _1 = (signed char)DBL_PTR(_data_1124)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_1124);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke2_addr++ = (unsigned short)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned short)DBL_PTR(_2)->dbl;
                    *poke2_addr++ = (unsigned short)_0;
                }
            }
        }
        goto L6; // [158] 196

        /** 		case 4 then*/
        case 4:

        /** 			eu:poke4( eaddr, data )*/
        if (IS_ATOM_INT(_eaddr_1130)){
            poke4_addr = (unsigned long *)_eaddr_1130;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_eaddr_1130)->dbl);
        }
        if (IS_ATOM_INT(_data_1124)) {
            *poke4_addr = (unsigned long)_data_1124;
        }
        else if (IS_ATOM(_data_1124)) {
            _1 = (unsigned long)DBL_PTR(_data_1124)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_data_1124);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        goto L6; // [169] 196

        /** 		case else*/
        default:

        /** 			error:crash("Parameter error: Wrong word size %d in allocate_protect().", wordsize)*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_192_1163);
        _msg_inlined_crash_at_192_1163 = EPrintf(-9999999, _476, _wordsize_1125);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_192_1163);

        /** end procedure*/
        goto L7; // [190] 193
L7: 
        DeRefi(_msg_inlined_crash_at_192_1163);
        _msg_inlined_crash_at_192_1163 = NOVALUE;
    ;}L6: 

    /** 	ifdef SAFE then*/

    /** 	if local_change_protection_on_protected_memory( iaddr, size + memory:BORDER_SPACE * 2, true_protection ) = -1 then*/
    _477 = 0;
    _478 = _size_1131 + 0;
    _477 = NOVALUE;
    Ref(_iaddr_1128);
    _479 = _7local_change_protection_on_protected_memory(_iaddr_1128, _478, _true_protection_1135);
    _478 = NOVALUE;
    if (binary_op_a(NOTEQ, _479, -1)){
        DeRef(_479);
        _479 = NOVALUE;
        goto L8; // [214] 238
    }
    DeRef(_479);
    _479 = NOVALUE;

    /** 		local_free_protected_memory( iaddr, size + memory:BORDER_SPACE * 2 )*/
    _481 = 0;
    _482 = _size_1131 + 0;
    _481 = NOVALUE;
    Ref(_iaddr_1128);
    _7local_free_protected_memory(_iaddr_1128, _482);
    _482 = NOVALUE;

    /** 		eaddr = 0*/
    DeRef(_eaddr_1130);
    _eaddr_1130 = 0;
L8: 

    /** 	return eaddr*/
    DeRefi(_data_1124);
    DeRef(_iaddr_1128);
    DeRef(_471);
    _471 = NOVALUE;
    return _eaddr_1130;
    ;
}


int _7local_allocate_protected_memory(int _s_1178, int _first_protection_1179)
{
    int _488 = NOVALUE;
    int _487 = NOVALUE;
    int _486 = NOVALUE;
    int _485 = NOVALUE;
    int _484 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _484 = _9dep_works();
    if (_484 == 0) {
        DeRef(_484);
        _484 = NOVALUE;
        goto L1; // [16] 50
    }
    else {
        if (!IS_ATOM_INT(_484) && DBL_PTR(_484)->dbl == 0.0){
            DeRef(_484);
            _484 = NOVALUE;
            goto L1; // [16] 50
        }
        DeRef(_484);
        _484 = NOVALUE;
    }
    DeRef(_484);
    _484 = NOVALUE;

    /** 			return eu:c_func(VirtualAlloc_rid, */
    {unsigned long tu;
         tu = (unsigned long)8192 | (unsigned long)4096;
         _485 = MAKE_UINT(tu);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = _s_1178;
    *((int *)(_2+12)) = _485;
    *((int *)(_2+16)) = _first_protection_1179;
    _486 = MAKE_SEQ(_1);
    _485 = NOVALUE;
    _487 = call_c(1, _7VirtualAlloc_rid_1025, _486);
    DeRefDS(_486);
    _486 = NOVALUE;
    return _487;
    goto L2; // [47] 65
L1: 

    /** 			return machine_func(M_ALLOC, PAGE_SIZE)*/
    _488 = machine(16, _7PAGE_SIZE_1085);
    DeRef(_487);
    _487 = NOVALUE;
    return _488;
L2: 
    ;
}


int _7local_change_protection_on_protected_memory(int _p_1193, int _s_1194, int _new_protection_1195)
{
    int _491 = NOVALUE;
    int _490 = NOVALUE;
    int _489 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _489 = _9dep_works();
    if (_489 == 0) {
        DeRef(_489);
        _489 = NOVALUE;
        goto L1; // [16] 49
    }
    else {
        if (!IS_ATOM_INT(_489) && DBL_PTR(_489)->dbl == 0.0){
            DeRef(_489);
            _489 = NOVALUE;
            goto L1; // [16] 49
        }
        DeRef(_489);
        _489 = NOVALUE;
    }
    DeRef(_489);
    _489 = NOVALUE;

    /** 			if eu:c_func( VirtualProtect_rid, { p, s, new_protection , oldprotptr } ) = 0 then*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_1193);
    *((int *)(_2+4)) = _p_1193;
    *((int *)(_2+8)) = _s_1194;
    *((int *)(_2+12)) = _new_protection_1195;
    Ref(_7oldprotptr_1174);
    *((int *)(_2+16)) = _7oldprotptr_1174;
    _490 = MAKE_SEQ(_1);
    _491 = call_c(1, _7VirtualProtect_rid_1026, _490);
    DeRefDS(_490);
    _490 = NOVALUE;
    if (binary_op_a(NOTEQ, _491, 0)){
        DeRef(_491);
        _491 = NOVALUE;
        goto L2; // [37] 48
    }
    DeRef(_491);
    _491 = NOVALUE;

    /** 				return -1*/
    DeRef(_p_1193);
    return -1;
L2: 
L1: 

    /** 		return 0*/
    DeRef(_p_1193);
    return 0;
    ;
}


void _7local_free_protected_memory(int _p_1205, int _s_1206)
{
    int _497 = NOVALUE;
    int _496 = NOVALUE;
    int _495 = NOVALUE;
    int _494 = NOVALUE;
    int _493 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		if dep_works() then*/
    _493 = _9dep_works();
    if (_493 == 0) {
        DeRef(_493);
        _493 = NOVALUE;
        goto L1; // [12] 35
    }
    else {
        if (!IS_ATOM_INT(_493) && DBL_PTR(_493)->dbl == 0.0){
            DeRef(_493);
            _493 = NOVALUE;
            goto L1; // [12] 35
        }
        DeRef(_493);
        _493 = NOVALUE;
    }
    DeRef(_493);
    _493 = NOVALUE;

    /** 			c_func(VirtualFree_rid, { p, s, MEM_RELEASE })*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_1205);
    *((int *)(_2+4)) = _p_1205;
    *((int *)(_2+8)) = _s_1206;
    *((int *)(_2+12)) = 32768;
    _494 = MAKE_SEQ(_1);
    _495 = call_c(1, _9VirtualFree_rid_333, _494);
    DeRefDS(_494);
    _494 = NOVALUE;
    goto L2; // [32] 48
L1: 

    /** 			machine_func(M_FREE, {p})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_p_1205);
    *((int *)(_2+4)) = _p_1205;
    _496 = MAKE_SEQ(_1);
    _497 = machine(17, _496);
    DeRefDS(_496);
    _496 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_p_1205);
    DeRef(_495);
    _495 = NOVALUE;
    DeRef(_497);
    _497 = NOVALUE;
    return;
    ;
}


void _7free(int _addr_1220)
{
    int _msg_inlined_crash_at_27_1229 = NOVALUE;
    int _data_inlined_crash_at_24_1228 = NOVALUE;
    int _addr_inlined_deallocate_at_64_1235 = NOVALUE;
    int _msg_inlined_crash_at_106_1240 = NOVALUE;
    int _504 = NOVALUE;
    int _503 = NOVALUE;
    int _502 = NOVALUE;
    int _501 = NOVALUE;
    int _499 = NOVALUE;
    int _498 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if types:number_array (addr) then*/
    Ref(_addr_1220);
    _498 = _11number_array(_addr_1220);
    if (_498 == 0) {
        DeRef(_498);
        _498 = NOVALUE;
        goto L1; // [7] 97
    }
    else {
        if (!IS_ATOM_INT(_498) && DBL_PTR(_498)->dbl == 0.0){
            DeRef(_498);
            _498 = NOVALUE;
            goto L1; // [7] 97
        }
        DeRef(_498);
        _498 = NOVALUE;
    }
    DeRef(_498);
    _498 = NOVALUE;

    /** 		if types:ascii_string(addr) then*/
    Ref(_addr_1220);
    _499 = _11ascii_string(_addr_1220);
    if (_499 == 0) {
        DeRef(_499);
        _499 = NOVALUE;
        goto L2; // [16] 47
    }
    else {
        if (!IS_ATOM_INT(_499) && DBL_PTR(_499)->dbl == 0.0){
            DeRef(_499);
            _499 = NOVALUE;
            goto L2; // [16] 47
        }
        DeRef(_499);
        _499 = NOVALUE;
    }
    DeRef(_499);
    _499 = NOVALUE;

    /** 			error:crash("free(\"%s\") is not a valid address", {addr})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_1220);
    *((int *)(_2+4)) = _addr_1220;
    _501 = MAKE_SEQ(_1);
    DeRef(_data_inlined_crash_at_24_1228);
    _data_inlined_crash_at_24_1228 = _501;
    _501 = NOVALUE;

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_27_1229);
    _msg_inlined_crash_at_27_1229 = EPrintf(-9999999, _500, _data_inlined_crash_at_24_1228);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_27_1229);

    /** end procedure*/
    goto L3; // [41] 44
L3: 
    DeRef(_data_inlined_crash_at_24_1228);
    _data_inlined_crash_at_24_1228 = NOVALUE;
    DeRefi(_msg_inlined_crash_at_27_1229);
    _msg_inlined_crash_at_27_1229 = NOVALUE;
L2: 

    /** 		for i = 1 to length(addr) do*/
    if (IS_SEQUENCE(_addr_1220)){
            _502 = SEQ_PTR(_addr_1220)->length;
    }
    else {
        _502 = 1;
    }
    {
        int _i_1231;
        _i_1231 = 1;
L4: 
        if (_i_1231 > _502){
            goto L5; // [52] 89
        }

        /** 			memory:deallocate( addr[i] )*/
        _2 = (int)SEQ_PTR(_addr_1220);
        _503 = (int)*(((s1_ptr)_2)->base + _i_1231);
        Ref(_503);
        DeRef(_addr_inlined_deallocate_at_64_1235);
        _addr_inlined_deallocate_at_64_1235 = _503;
        _503 = NOVALUE;

        /** 	ifdef DATA_EXECUTE and WINDOWS then*/

        /**    	machine_proc( memconst:M_FREE, addr)*/
        machine(17, _addr_inlined_deallocate_at_64_1235);

        /** end procedure*/
        goto L6; // [77] 80
L6: 
        DeRef(_addr_inlined_deallocate_at_64_1235);
        _addr_inlined_deallocate_at_64_1235 = NOVALUE;

        /** 		end for*/
        _i_1231 = _i_1231 + 1;
        goto L4; // [84] 59
L5: 
        ;
    }

    /** 		return*/
    DeRef(_addr_1220);
    return;
    goto L7; // [94] 127
L1: 

    /** 	elsif sequence(addr) then*/
    _504 = IS_SEQUENCE(_addr_1220);
    if (_504 == 0)
    {
        _504 = NOVALUE;
        goto L8; // [102] 126
    }
    else{
        _504 = NOVALUE;
    }

    /** 		error:crash("free() called with nested sequence")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_106_1240);
    _msg_inlined_crash_at_106_1240 = EPrintf(-9999999, _505, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_106_1240);

    /** end procedure*/
    goto L9; // [120] 123
L9: 
    DeRefi(_msg_inlined_crash_at_106_1240);
    _msg_inlined_crash_at_106_1240 = NOVALUE;
L8: 
L7: 

    /** 	if addr = 0 then*/
    if (binary_op_a(NOTEQ, _addr_1220, 0)){
        goto LA; // [129] 139
    }

    /** 		return*/
    DeRef(_addr_1220);
    return;
LA: 

    /** 	memory:deallocate( addr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_1220);

    /** end procedure*/
    goto LB; // [150] 153
LB: 

    /** end procedure*/
    DeRef(_addr_1220);
    return;
    ;
}


void _7free_pointer_array(int _pointers_array_1248)
{
    int _saved_1249 = NOVALUE;
    int _ptr_1250 = NOVALUE;
    int _0, _1, _2;
    

    /** 	atom saved = pointers_array*/
    Ref(_pointers_array_1248);
    DeRef(_saved_1249);
    _saved_1249 = _pointers_array_1248;

    /** 	while ptr with entry do*/
    goto L1; // [8] 39
L2: 
    if (_ptr_1250 <= 0) {
        if (_ptr_1250 == 0) {
            goto L3; // [13] 49
        }
        else {
            if (!IS_ATOM_INT(_ptr_1250) && DBL_PTR(_ptr_1250)->dbl == 0.0){
                goto L3; // [13] 49
            }
        }
    }

    /** 		memory:deallocate( ptr )*/

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _ptr_1250);

    /** end procedure*/
    goto L4; // [27] 30
L4: 

    /** 		pointers_array += ADDRESS_LENGTH*/
    _0 = _pointers_array_1248;
    if (IS_ATOM_INT(_pointers_array_1248)) {
        _pointers_array_1248 = _pointers_array_1248 + 4;
        if ((long)((unsigned long)_pointers_array_1248 + (unsigned long)HIGH_BITS) >= 0) 
        _pointers_array_1248 = NewDouble((double)_pointers_array_1248);
    }
    else {
        _pointers_array_1248 = NewDouble(DBL_PTR(_pointers_array_1248)->dbl + (double)4);
    }
    DeRef(_0);

    /** 	entry*/
L1: 

    /** 		ptr = peek4u(pointers_array)*/
    DeRef(_ptr_1250);
    if (IS_ATOM_INT(_pointers_array_1248)) {
        _ptr_1250 = *(unsigned long *)_pointers_array_1248;
        if ((unsigned)_ptr_1250 > (unsigned)MAXINT)
        _ptr_1250 = NewDouble((double)(unsigned long)_ptr_1250);
    }
    else {
        _ptr_1250 = *(unsigned long *)(unsigned long)(DBL_PTR(_pointers_array_1248)->dbl);
        if ((unsigned)_ptr_1250 > (unsigned)MAXINT)
        _ptr_1250 = NewDouble((double)(unsigned long)_ptr_1250);
    }

    /** 	end while*/
    goto L2; // [46] 11
L3: 

    /** 	free(saved)*/
    Ref(_saved_1249);
    _7free(_saved_1249);

    /** end procedure*/
    DeRef(_pointers_array_1248);
    DeRef(_saved_1249);
    DeRef(_ptr_1250);
    return;
    ;
}



// 0x7CC469A9
