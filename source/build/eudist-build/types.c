// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _11char_test(int _test_data_402, int _char_set_403)
{
    int _lChr_404 = NOVALUE;
    int _118 = NOVALUE;
    int _117 = NOVALUE;
    int _116 = NOVALUE;
    int _115 = NOVALUE;
    int _114 = NOVALUE;
    int _113 = NOVALUE;
    int _112 = NOVALUE;
    int _111 = NOVALUE;
    int _110 = NOVALUE;
    int _109 = NOVALUE;
    int _108 = NOVALUE;
    int _105 = NOVALUE;
    int _104 = NOVALUE;
    int _103 = NOVALUE;
    int _102 = NOVALUE;
    int _100 = NOVALUE;
    int _98 = NOVALUE;
    int _97 = NOVALUE;
    int _96 = NOVALUE;
    int _95 = NOVALUE;
    int _94 = NOVALUE;
    int _93 = NOVALUE;
    int _92 = NOVALUE;
    int _91 = NOVALUE;
    int _90 = NOVALUE;
    int _89 = NOVALUE;
    int _88 = NOVALUE;
    int _87 = NOVALUE;
    int _86 = NOVALUE;
    int _85 = NOVALUE;
    int _84 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(test_data) then*/
    if (IS_ATOM_INT(_test_data_402))
    _84 = 1;
    else if (IS_ATOM_DBL(_test_data_402))
    _84 = IS_ATOM_INT(DoubleToInt(_test_data_402));
    else
    _84 = 0;
    if (_84 == 0)
    {
        _84 = NOVALUE;
        goto L1; // [8] 115
    }
    else{
        _84 = NOVALUE;
    }

    /** 		if sequence(char_set[1]) then*/
    _2 = (int)SEQ_PTR(_char_set_403);
    _85 = (int)*(((s1_ptr)_2)->base + 1);
    _86 = IS_SEQUENCE(_85);
    _85 = NOVALUE;
    if (_86 == 0)
    {
        _86 = NOVALUE;
        goto L2; // [20] 96
    }
    else{
        _86 = NOVALUE;
    }

    /** 			for j = 1 to length(char_set) do*/
    if (IS_SEQUENCE(_char_set_403)){
            _87 = SEQ_PTR(_char_set_403)->length;
    }
    else {
        _87 = 1;
    }
    {
        int _j_411;
        _j_411 = 1;
L3: 
        if (_j_411 > _87){
            goto L4; // [28] 85
        }

        /** 				if test_data >= char_set[j][1] and test_data <= char_set[j][2] then */
        _2 = (int)SEQ_PTR(_char_set_403);
        _88 = (int)*(((s1_ptr)_2)->base + _j_411);
        _2 = (int)SEQ_PTR(_88);
        _89 = (int)*(((s1_ptr)_2)->base + 1);
        _88 = NOVALUE;
        if (IS_ATOM_INT(_test_data_402) && IS_ATOM_INT(_89)) {
            _90 = (_test_data_402 >= _89);
        }
        else {
            _90 = binary_op(GREATEREQ, _test_data_402, _89);
        }
        _89 = NOVALUE;
        if (IS_ATOM_INT(_90)) {
            if (_90 == 0) {
                goto L5; // [49] 78
            }
        }
        else {
            if (DBL_PTR(_90)->dbl == 0.0) {
                goto L5; // [49] 78
            }
        }
        _2 = (int)SEQ_PTR(_char_set_403);
        _92 = (int)*(((s1_ptr)_2)->base + _j_411);
        _2 = (int)SEQ_PTR(_92);
        _93 = (int)*(((s1_ptr)_2)->base + 2);
        _92 = NOVALUE;
        if (IS_ATOM_INT(_test_data_402) && IS_ATOM_INT(_93)) {
            _94 = (_test_data_402 <= _93);
        }
        else {
            _94 = binary_op(LESSEQ, _test_data_402, _93);
        }
        _93 = NOVALUE;
        if (_94 == 0) {
            DeRef(_94);
            _94 = NOVALUE;
            goto L5; // [66] 78
        }
        else {
            if (!IS_ATOM_INT(_94) && DBL_PTR(_94)->dbl == 0.0){
                DeRef(_94);
                _94 = NOVALUE;
                goto L5; // [66] 78
            }
            DeRef(_94);
            _94 = NOVALUE;
        }
        DeRef(_94);
        _94 = NOVALUE;

        /** 					return TRUE */
        DeRef(_test_data_402);
        DeRefDS(_char_set_403);
        DeRef(_90);
        _90 = NOVALUE;
        return _11TRUE_357;
L5: 

        /** 			end for*/
        _j_411 = _j_411 + 1;
        goto L3; // [80] 35
L4: 
        ;
    }

    /** 			return FALSE*/
    DeRef(_test_data_402);
    DeRefDS(_char_set_403);
    DeRef(_90);
    _90 = NOVALUE;
    return _11FALSE_355;
    goto L6; // [93] 330
L2: 

    /** 			return find(test_data, char_set) > 0*/
    _95 = find_from(_test_data_402, _char_set_403, 1);
    _96 = (_95 > 0);
    _95 = NOVALUE;
    DeRef(_test_data_402);
    DeRefDS(_char_set_403);
    DeRef(_90);
    _90 = NOVALUE;
    return _96;
    goto L6; // [112] 330
L1: 

    /** 	elsif sequence(test_data) then*/
    _97 = IS_SEQUENCE(_test_data_402);
    if (_97 == 0)
    {
        _97 = NOVALUE;
        goto L7; // [120] 321
    }
    else{
        _97 = NOVALUE;
    }

    /** 		if length(test_data) = 0 then */
    if (IS_SEQUENCE(_test_data_402)){
            _98 = SEQ_PTR(_test_data_402)->length;
    }
    else {
        _98 = 1;
    }
    if (_98 != 0)
    goto L8; // [128] 141

    /** 			return FALSE */
    DeRef(_test_data_402);
    DeRefDS(_char_set_403);
    DeRef(_96);
    _96 = NOVALUE;
    DeRef(_90);
    _90 = NOVALUE;
    return _11FALSE_355;
L8: 

    /** 		for i = 1 to length(test_data) label "NXTCHR" do*/
    if (IS_SEQUENCE(_test_data_402)){
            _100 = SEQ_PTR(_test_data_402)->length;
    }
    else {
        _100 = 1;
    }
    {
        int _i_430;
        _i_430 = 1;
L9: 
        if (_i_430 > _100){
            goto LA; // [146] 310
        }

        /** 			if sequence(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_402);
        _102 = (int)*(((s1_ptr)_2)->base + _i_430);
        _103 = IS_SEQUENCE(_102);
        _102 = NOVALUE;
        if (_103 == 0)
        {
            _103 = NOVALUE;
            goto LB; // [162] 174
        }
        else{
            _103 = NOVALUE;
        }

        /** 				return FALSE*/
        DeRef(_test_data_402);
        DeRefDS(_char_set_403);
        DeRef(_96);
        _96 = NOVALUE;
        DeRef(_90);
        _90 = NOVALUE;
        return _11FALSE_355;
LB: 

        /** 			if not integer(test_data[i]) then */
        _2 = (int)SEQ_PTR(_test_data_402);
        _104 = (int)*(((s1_ptr)_2)->base + _i_430);
        if (IS_ATOM_INT(_104))
        _105 = 1;
        else if (IS_ATOM_DBL(_104))
        _105 = IS_ATOM_INT(DoubleToInt(_104));
        else
        _105 = 0;
        _104 = NOVALUE;
        if (_105 != 0)
        goto LC; // [183] 195
        _105 = NOVALUE;

        /** 				return FALSE*/
        DeRef(_test_data_402);
        DeRefDS(_char_set_403);
        DeRef(_96);
        _96 = NOVALUE;
        DeRef(_90);
        _90 = NOVALUE;
        return _11FALSE_355;
LC: 

        /** 			lChr = test_data[i]*/
        _2 = (int)SEQ_PTR(_test_data_402);
        _lChr_404 = (int)*(((s1_ptr)_2)->base + _i_430);
        if (!IS_ATOM_INT(_lChr_404)){
            _lChr_404 = (long)DBL_PTR(_lChr_404)->dbl;
        }

        /** 			if sequence(char_set[1]) then*/
        _2 = (int)SEQ_PTR(_char_set_403);
        _108 = (int)*(((s1_ptr)_2)->base + 1);
        _109 = IS_SEQUENCE(_108);
        _108 = NOVALUE;
        if (_109 == 0)
        {
            _109 = NOVALUE;
            goto LD; // [214] 278
        }
        else{
            _109 = NOVALUE;
        }

        /** 				for j = 1 to length(char_set) do*/
        if (IS_SEQUENCE(_char_set_403)){
                _110 = SEQ_PTR(_char_set_403)->length;
        }
        else {
            _110 = 1;
        }
        {
            int _j_445;
            _j_445 = 1;
LE: 
            if (_j_445 > _110){
                goto LF; // [222] 275
            }

            /** 					if lChr >= char_set[j][1] and lChr <= char_set[j][2] then*/
            _2 = (int)SEQ_PTR(_char_set_403);
            _111 = (int)*(((s1_ptr)_2)->base + _j_445);
            _2 = (int)SEQ_PTR(_111);
            _112 = (int)*(((s1_ptr)_2)->base + 1);
            _111 = NOVALUE;
            if (IS_ATOM_INT(_112)) {
                _113 = (_lChr_404 >= _112);
            }
            else {
                _113 = binary_op(GREATEREQ, _lChr_404, _112);
            }
            _112 = NOVALUE;
            if (IS_ATOM_INT(_113)) {
                if (_113 == 0) {
                    goto L10; // [243] 268
                }
            }
            else {
                if (DBL_PTR(_113)->dbl == 0.0) {
                    goto L10; // [243] 268
                }
            }
            _2 = (int)SEQ_PTR(_char_set_403);
            _115 = (int)*(((s1_ptr)_2)->base + _j_445);
            _2 = (int)SEQ_PTR(_115);
            _116 = (int)*(((s1_ptr)_2)->base + 2);
            _115 = NOVALUE;
            if (IS_ATOM_INT(_116)) {
                _117 = (_lChr_404 <= _116);
            }
            else {
                _117 = binary_op(LESSEQ, _lChr_404, _116);
            }
            _116 = NOVALUE;
            if (_117 == 0) {
                DeRef(_117);
                _117 = NOVALUE;
                goto L10; // [260] 268
            }
            else {
                if (!IS_ATOM_INT(_117) && DBL_PTR(_117)->dbl == 0.0){
                    DeRef(_117);
                    _117 = NOVALUE;
                    goto L10; // [260] 268
                }
                DeRef(_117);
                _117 = NOVALUE;
            }
            DeRef(_117);
            _117 = NOVALUE;

            /** 						continue "NXTCHR" */
            goto L11; // [265] 305
L10: 

            /** 				end for*/
            _j_445 = _j_445 + 1;
            goto LE; // [270] 229
LF: 
            ;
        }
        goto L12; // [275] 295
LD: 

        /** 				if find(lChr, char_set) > 0 then*/
        _118 = find_from(_lChr_404, _char_set_403, 1);
        if (_118 <= 0)
        goto L13; // [285] 294

        /** 					continue "NXTCHR"*/
        goto L11; // [291] 305
L13: 
L12: 

        /** 			return FALSE*/
        DeRef(_test_data_402);
        DeRefDS(_char_set_403);
        DeRef(_96);
        _96 = NOVALUE;
        DeRef(_90);
        _90 = NOVALUE;
        DeRef(_113);
        _113 = NOVALUE;
        return _11FALSE_355;

        /** 		end for*/
L11: 
        _i_430 = _i_430 + 1;
        goto L9; // [305] 153
LA: 
        ;
    }

    /** 		return TRUE*/
    DeRef(_test_data_402);
    DeRefDS(_char_set_403);
    DeRef(_96);
    _96 = NOVALUE;
    DeRef(_90);
    _90 = NOVALUE;
    DeRef(_113);
    _113 = NOVALUE;
    return _11TRUE_357;
    goto L6; // [318] 330
L7: 

    /** 		return FALSE*/
    DeRef(_test_data_402);
    DeRefDS(_char_set_403);
    DeRef(_96);
    _96 = NOVALUE;
    DeRef(_90);
    _90 = NOVALUE;
    DeRef(_113);
    _113 = NOVALUE;
    return _11FALSE_355;
L6: 
    ;
}


void _11set_default_charsets()
{
    int _186 = NOVALUE;
    int _184 = NOVALUE;
    int _183 = NOVALUE;
    int _181 = NOVALUE;
    int _178 = NOVALUE;
    int _177 = NOVALUE;
    int _176 = NOVALUE;
    int _175 = NOVALUE;
    int _172 = NOVALUE;
    int _170 = NOVALUE;
    int _159 = NOVALUE;
    int _152 = NOVALUE;
    int _148 = NOVALUE;
    int _140 = NOVALUE;
    int _137 = NOVALUE;
    int _136 = NOVALUE;
    int _135 = NOVALUE;
    int _132 = NOVALUE;
    int _129 = NOVALUE;
    int _121 = NOVALUE;
    int _120 = NOVALUE;
    int _0, _1, _2;
    

    /** 	Defined_Sets = repeat(0, CS_LAST - CS_FIRST - 1)*/
    _120 = 20;
    _121 = 19;
    _120 = NOVALUE;
    DeRef(_11Defined_Sets_460);
    _11Defined_Sets_460 = Repeat(0, 19);
    _121 = NOVALUE;

    /** 	Defined_Sets[CS_Alphabetic	] = {{'a', 'z'}, {'A', 'Z'}}*/
    RefDS(_128);
    RefDS(_125);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _125;
    ((int *)_2)[2] = _128;
    _129 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 12);
    *(int *)_2 = _129;
    if( _1 != _129 ){
    }
    _129 = NOVALUE;

    /** 	Defined_Sets[CS_Alphanumeric] = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_131);
    *((int *)(_2+4)) = _131;
    RefDS(_125);
    *((int *)(_2+8)) = _125;
    RefDS(_128);
    *((int *)(_2+12)) = _128;
    _132 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 10);
    _1 = *(int *)_2;
    *(int *)_2 = _132;
    if( _1 != _132 ){
        DeRef(_1);
    }
    _132 = NOVALUE;

    /** 	Defined_Sets[CS_Identifier]   = {{'0', '9'}, {'a', 'z'}, {'A', 'Z'}, {'_', '_'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_131);
    *((int *)(_2+4)) = _131;
    RefDS(_125);
    *((int *)(_2+8)) = _125;
    RefDS(_128);
    *((int *)(_2+12)) = _128;
    RefDS(_134);
    *((int *)(_2+16)) = _134;
    _135 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 11);
    _1 = *(int *)_2;
    *(int *)_2 = _135;
    if( _1 != _135 ){
        DeRef(_1);
    }
    _135 = NOVALUE;

    /** 	Defined_Sets[CS_Uppercase 	] = {{'A', 'Z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_128);
    *((int *)(_2+4)) = _128;
    _136 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 9);
    _1 = *(int *)_2;
    *(int *)_2 = _136;
    if( _1 != _136 ){
        DeRef(_1);
    }
    _136 = NOVALUE;

    /** 	Defined_Sets[CS_Lowercase 	] = {{'a', 'z'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_125);
    *((int *)(_2+4)) = _125;
    _137 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 8);
    _1 = *(int *)_2;
    *(int *)_2 = _137;
    if( _1 != _137 ){
        DeRef(_1);
    }
    _137 = NOVALUE;

    /** 	Defined_Sets[CS_Printable 	] = {{' ', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_139);
    *((int *)(_2+4)) = _139;
    _140 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _140;
    if( _1 != _140 ){
        DeRef(_1);
    }
    _140 = NOVALUE;

    /** 	Defined_Sets[CS_Displayable ] = {{' ', '~'}, "  ", "\t\t", "\n\n", "\r\r", {8,8}, {7,7} }*/
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_139);
    *((int *)(_2+4)) = _139;
    RefDS(_141);
    *((int *)(_2+8)) = _141;
    RefDS(_142);
    *((int *)(_2+12)) = _142;
    RefDS(_143);
    *((int *)(_2+16)) = _143;
    RefDS(_144);
    *((int *)(_2+20)) = _144;
    RefDS(_145);
    *((int *)(_2+24)) = _145;
    RefDS(_147);
    *((int *)(_2+28)) = _147;
    _148 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _148;
    if( _1 != _148 ){
        DeRef(_1);
    }
    _148 = NOVALUE;

    /** 	Defined_Sets[CS_Whitespace 	] = " \t\n\r" & 11 & 160*/
    {
        int concat_list[3];

        concat_list[0] = 160;
        concat_list[1] = 11;
        concat_list[2] = _149;
        Concat_N((object_ptr)&_152, concat_list, 3);
    }
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _152;
    if( _1 != _152 ){
        DeRef(_1);
    }
    _152 = NOVALUE;

    /** 	Defined_Sets[CS_Consonant 	] = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ"*/
    RefDS(_153);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _153;
    DeRef(_1);

    /** 	Defined_Sets[CS_Vowel 		] = "aeiouAEIOU"*/
    RefDS(_154);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _154;
    DeRef(_1);

    /** 	Defined_Sets[CS_Hexadecimal ] = {{'0', '9'}, {'A', 'F'},{'a', 'f'}}*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_131);
    *((int *)(_2+4)) = _131;
    RefDS(_156);
    *((int *)(_2+8)) = _156;
    RefDS(_158);
    *((int *)(_2+12)) = _158;
    _159 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _159;
    if( _1 != _159 ){
        DeRef(_1);
    }
    _159 = NOVALUE;

    /** 	Defined_Sets[CS_Punctuation ] = {{' ', '/'}, {':', '?'}, {'[', '`'}, {'{', '~'}}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_161);
    *((int *)(_2+4)) = _161;
    RefDS(_164);
    *((int *)(_2+8)) = _164;
    RefDS(_167);
    *((int *)(_2+12)) = _167;
    RefDS(_169);
    *((int *)(_2+16)) = _169;
    _170 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _170;
    if( _1 != _170 ){
        DeRef(_1);
    }
    _170 = NOVALUE;

    /** 	Defined_Sets[CS_Control 	] = {{0, 31}, {127, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 31;
    _172 = MAKE_SEQ(_1);
    RefDS(_174);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _172;
    ((int *)_2)[2] = _174;
    _175 = MAKE_SEQ(_1);
    _172 = NOVALUE;
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 14);
    _1 = *(int *)_2;
    *(int *)_2 = _175;
    if( _1 != _175 ){
        DeRef(_1);
    }
    _175 = NOVALUE;

    /** 	Defined_Sets[CS_ASCII 		] = {{0, 127}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 127;
    _176 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _176;
    _177 = MAKE_SEQ(_1);
    _176 = NOVALUE;
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 13);
    _1 = *(int *)_2;
    *(int *)_2 = _177;
    if( _1 != _177 ){
        DeRef(_1);
    }
    _177 = NOVALUE;

    /** 	Defined_Sets[CS_Digit 		] = {{'0', '9'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_131);
    *((int *)(_2+4)) = _131;
    _178 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _178;
    if( _1 != _178 ){
        DeRef(_1);
    }
    _178 = NOVALUE;

    /** 	Defined_Sets[CS_Graphic 	] = {{'!', '~'}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_180);
    *((int *)(_2+4)) = _180;
    _181 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 16);
    _1 = *(int *)_2;
    *(int *)_2 = _181;
    if( _1 != _181 ){
        DeRef(_1);
    }
    _181 = NOVALUE;

    /** 	Defined_Sets[CS_Bytes	 	] = {{0, 255}}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 255;
    _183 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _183;
    _184 = MAKE_SEQ(_1);
    _183 = NOVALUE;
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 17);
    _1 = *(int *)_2;
    *(int *)_2 = _184;
    if( _1 != _184 ){
        DeRef(_1);
    }
    _184 = NOVALUE;

    /** 	Defined_Sets[CS_SpecWord 	] = "_"*/
    RefDS(_185);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 18);
    _1 = *(int *)_2;
    *(int *)_2 = _185;
    DeRef(_1);

    /** 	Defined_Sets[CS_Boolean     ] = {TRUE,FALSE}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _11TRUE_357;
    ((int *)_2)[2] = _11FALSE_355;
    _186 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _2 = (int)(((s1_ptr)_2)->base + 19);
    _1 = *(int *)_2;
    *(int *)_2 = _186;
    if( _1 != _186 ){
        DeRef(_1);
    }
    _186 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _11get_charsets()
{
    int _result__532 = NOVALUE;
    int _190 = NOVALUE;
    int _189 = NOVALUE;
    int _188 = NOVALUE;
    int _187 = NOVALUE;
    int _0, _1, _2;
    

    /** 	result_ = {}*/
    RefDS(_5);
    DeRef(_result__532);
    _result__532 = _5;

    /** 	for i = CS_FIRST + 1 to CS_LAST - 1 do*/
    _187 = 1;
    _188 = 19;
    {
        int _i_534;
        _i_534 = 1;
L1: 
        if (_i_534 > 19){
            goto L2; // [22] 52
        }

        /** 		result_ = append(result_, {i, Defined_Sets[i]} )*/
        _2 = (int)SEQ_PTR(_11Defined_Sets_460);
        _189 = (int)*(((s1_ptr)_2)->base + _i_534);
        Ref(_189);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _i_534;
        ((int *)_2)[2] = _189;
        _190 = MAKE_SEQ(_1);
        _189 = NOVALUE;
        RefDS(_190);
        Append(&_result__532, _result__532, _190);
        DeRefDS(_190);
        _190 = NOVALUE;

        /** 	end for*/
        _i_534 = _i_534 + 1;
        goto L1; // [47] 29
L2: 
        ;
    }

    /** 	return result_*/
    DeRef(_187);
    _187 = NOVALUE;
    DeRef(_188);
    _188 = NOVALUE;
    return _result__532;
    ;
}


void _11set_charsets(int _charset_list_542)
{
    int _213 = NOVALUE;
    int _212 = NOVALUE;
    int _211 = NOVALUE;
    int _210 = NOVALUE;
    int _209 = NOVALUE;
    int _208 = NOVALUE;
    int _207 = NOVALUE;
    int _206 = NOVALUE;
    int _205 = NOVALUE;
    int _204 = NOVALUE;
    int _203 = NOVALUE;
    int _202 = NOVALUE;
    int _201 = NOVALUE;
    int _200 = NOVALUE;
    int _199 = NOVALUE;
    int _198 = NOVALUE;
    int _197 = NOVALUE;
    int _196 = NOVALUE;
    int _195 = NOVALUE;
    int _194 = NOVALUE;
    int _193 = NOVALUE;
    int _192 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(charset_list) do*/
    if (IS_SEQUENCE(_charset_list_542)){
            _192 = SEQ_PTR(_charset_list_542)->length;
    }
    else {
        _192 = 1;
    }
    {
        int _i_544;
        _i_544 = 1;
L1: 
        if (_i_544 > _192){
            goto L2; // [8] 133
        }

        /** 		if sequence(charset_list[i]) and length(charset_list[i]) = 2 then*/
        _2 = (int)SEQ_PTR(_charset_list_542);
        _193 = (int)*(((s1_ptr)_2)->base + _i_544);
        _194 = IS_SEQUENCE(_193);
        _193 = NOVALUE;
        if (_194 == 0) {
            goto L3; // [24] 126
        }
        _2 = (int)SEQ_PTR(_charset_list_542);
        _196 = (int)*(((s1_ptr)_2)->base + _i_544);
        if (IS_SEQUENCE(_196)){
                _197 = SEQ_PTR(_196)->length;
        }
        else {
            _197 = 1;
        }
        _196 = NOVALUE;
        _198 = (_197 == 2);
        _197 = NOVALUE;
        if (_198 == 0)
        {
            DeRef(_198);
            _198 = NOVALUE;
            goto L3; // [40] 126
        }
        else{
            DeRef(_198);
            _198 = NOVALUE;
        }

        /** 			if integer(charset_list[i][1]) and charset_list[i][1] > CS_FIRST and charset_list[i][1] < CS_LAST then*/
        _2 = (int)SEQ_PTR(_charset_list_542);
        _199 = (int)*(((s1_ptr)_2)->base + _i_544);
        _2 = (int)SEQ_PTR(_199);
        _200 = (int)*(((s1_ptr)_2)->base + 1);
        _199 = NOVALUE;
        if (IS_ATOM_INT(_200))
        _201 = 1;
        else if (IS_ATOM_DBL(_200))
        _201 = IS_ATOM_INT(DoubleToInt(_200));
        else
        _201 = 0;
        _200 = NOVALUE;
        if (_201 == 0) {
            _202 = 0;
            goto L4; // [56] 78
        }
        _2 = (int)SEQ_PTR(_charset_list_542);
        _203 = (int)*(((s1_ptr)_2)->base + _i_544);
        _2 = (int)SEQ_PTR(_203);
        _204 = (int)*(((s1_ptr)_2)->base + 1);
        _203 = NOVALUE;
        if (IS_ATOM_INT(_204)) {
            _205 = (_204 > 0);
        }
        else {
            _205 = binary_op(GREATER, _204, 0);
        }
        _204 = NOVALUE;
        if (IS_ATOM_INT(_205))
        _202 = (_205 != 0);
        else
        _202 = DBL_PTR(_205)->dbl != 0.0;
L4: 
        if (_202 == 0) {
            goto L5; // [78] 125
        }
        _2 = (int)SEQ_PTR(_charset_list_542);
        _207 = (int)*(((s1_ptr)_2)->base + _i_544);
        _2 = (int)SEQ_PTR(_207);
        _208 = (int)*(((s1_ptr)_2)->base + 1);
        _207 = NOVALUE;
        if (IS_ATOM_INT(_208)) {
            _209 = (_208 < 20);
        }
        else {
            _209 = binary_op(LESS, _208, 20);
        }
        _208 = NOVALUE;
        if (_209 == 0) {
            DeRef(_209);
            _209 = NOVALUE;
            goto L5; // [97] 125
        }
        else {
            if (!IS_ATOM_INT(_209) && DBL_PTR(_209)->dbl == 0.0){
                DeRef(_209);
                _209 = NOVALUE;
                goto L5; // [97] 125
            }
            DeRef(_209);
            _209 = NOVALUE;
        }
        DeRef(_209);
        _209 = NOVALUE;

        /** 				Defined_Sets[charset_list[i][1]] = charset_list[i][2]*/
        _2 = (int)SEQ_PTR(_charset_list_542);
        _210 = (int)*(((s1_ptr)_2)->base + _i_544);
        _2 = (int)SEQ_PTR(_210);
        _211 = (int)*(((s1_ptr)_2)->base + 1);
        _210 = NOVALUE;
        _2 = (int)SEQ_PTR(_charset_list_542);
        _212 = (int)*(((s1_ptr)_2)->base + _i_544);
        _2 = (int)SEQ_PTR(_212);
        _213 = (int)*(((s1_ptr)_2)->base + 2);
        _212 = NOVALUE;
        Ref(_213);
        _2 = (int)SEQ_PTR(_11Defined_Sets_460);
        if (!IS_ATOM_INT(_211))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_211)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _211);
        _1 = *(int *)_2;
        *(int *)_2 = _213;
        if( _1 != _213 ){
            DeRef(_1);
        }
        _213 = NOVALUE;
L5: 
L3: 

        /** 	end for*/
        _i_544 = _i_544 + 1;
        goto L1; // [128] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_charset_list_542);
    _196 = NOVALUE;
    _211 = NOVALUE;
    DeRef(_205);
    _205 = NOVALUE;
    return;
    ;
}


int _11boolean(int _test_data_571)
{
    int _216 = NOVALUE;
    int _215 = NOVALUE;
    int _214 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return find(test_data,{1,0}) != 0*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _214 = MAKE_SEQ(_1);
    _215 = find_from(_test_data_571, _214, 1);
    DeRefDS(_214);
    _214 = NOVALUE;
    _216 = (_215 != 0);
    _215 = NOVALUE;
    DeRef(_test_data_571);
    return _216;
    ;
}


int _11t_boolean(int _test_data_577)
{
    int _218 = NOVALUE;
    int _217 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Boolean])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _217 = (int)*(((s1_ptr)_2)->base + 19);
    Ref(_test_data_577);
    Ref(_217);
    _218 = _11char_test(_test_data_577, _217);
    _217 = NOVALUE;
    DeRef(_test_data_577);
    return _218;
    ;
}


int _11t_alnum(int _test_data_582)
{
    int _220 = NOVALUE;
    int _219 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphanumeric])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _219 = (int)*(((s1_ptr)_2)->base + 10);
    Ref(_test_data_582);
    Ref(_219);
    _220 = _11char_test(_test_data_582, _219);
    _219 = NOVALUE;
    DeRef(_test_data_582);
    return _220;
    ;
}


int _11t_identifier(int _test_data_587)
{
    int _230 = NOVALUE;
    int _229 = NOVALUE;
    int _228 = NOVALUE;
    int _227 = NOVALUE;
    int _226 = NOVALUE;
    int _225 = NOVALUE;
    int _224 = NOVALUE;
    int _223 = NOVALUE;
    int _222 = NOVALUE;
    int _221 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if t_digit(test_data) then*/
    Ref(_test_data_587);
    _221 = _11t_digit(_test_data_587);
    if (_221 == 0) {
        DeRef(_221);
        _221 = NOVALUE;
        goto L1; // [7] 19
    }
    else {
        if (!IS_ATOM_INT(_221) && DBL_PTR(_221)->dbl == 0.0){
            DeRef(_221);
            _221 = NOVALUE;
            goto L1; // [7] 19
        }
        DeRef(_221);
        _221 = NOVALUE;
    }
    DeRef(_221);
    _221 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_587);
    return 0;
    goto L2; // [16] 63
L1: 

    /** 	elsif sequence(test_data) and length(test_data) > 0 and t_digit(test_data[1]) then*/
    _222 = IS_SEQUENCE(_test_data_587);
    if (_222 == 0) {
        _223 = 0;
        goto L3; // [24] 39
    }
    if (IS_SEQUENCE(_test_data_587)){
            _224 = SEQ_PTR(_test_data_587)->length;
    }
    else {
        _224 = 1;
    }
    _225 = (_224 > 0);
    _224 = NOVALUE;
    _223 = (_225 != 0);
L3: 
    if (_223 == 0) {
        goto L4; // [39] 62
    }
    _2 = (int)SEQ_PTR(_test_data_587);
    _227 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_227);
    _228 = _11t_digit(_227);
    _227 = NOVALUE;
    if (_228 == 0) {
        DeRef(_228);
        _228 = NOVALUE;
        goto L4; // [52] 62
    }
    else {
        if (!IS_ATOM_INT(_228) && DBL_PTR(_228)->dbl == 0.0){
            DeRef(_228);
            _228 = NOVALUE;
            goto L4; // [52] 62
        }
        DeRef(_228);
        _228 = NOVALUE;
    }
    DeRef(_228);
    _228 = NOVALUE;

    /** 		return 0*/
    DeRef(_test_data_587);
    DeRef(_225);
    _225 = NOVALUE;
    return 0;
L4: 
L2: 

    /** 	return char_test(test_data, Defined_Sets[CS_Identifier])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _229 = (int)*(((s1_ptr)_2)->base + 11);
    Ref(_test_data_587);
    Ref(_229);
    _230 = _11char_test(_test_data_587, _229);
    _229 = NOVALUE;
    DeRef(_test_data_587);
    DeRef(_225);
    _225 = NOVALUE;
    return _230;
    ;
}


int _11t_alpha(int _test_data_604)
{
    int _232 = NOVALUE;
    int _231 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Alphabetic])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _231 = (int)*(((s1_ptr)_2)->base + 12);
    Ref(_test_data_604);
    Ref(_231);
    _232 = _11char_test(_test_data_604, _231);
    _231 = NOVALUE;
    DeRef(_test_data_604);
    return _232;
    ;
}


int _11t_ascii(int _test_data_609)
{
    int _234 = NOVALUE;
    int _233 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_ASCII])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _233 = (int)*(((s1_ptr)_2)->base + 13);
    Ref(_test_data_609);
    Ref(_233);
    _234 = _11char_test(_test_data_609, _233);
    _233 = NOVALUE;
    DeRef(_test_data_609);
    return _234;
    ;
}


int _11t_cntrl(int _test_data_614)
{
    int _236 = NOVALUE;
    int _235 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Control])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _235 = (int)*(((s1_ptr)_2)->base + 14);
    Ref(_test_data_614);
    Ref(_235);
    _236 = _11char_test(_test_data_614, _235);
    _235 = NOVALUE;
    DeRef(_test_data_614);
    return _236;
    ;
}


int _11t_digit(int _test_data_619)
{
    int _238 = NOVALUE;
    int _237 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Digit])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _237 = (int)*(((s1_ptr)_2)->base + 15);
    Ref(_test_data_619);
    Ref(_237);
    _238 = _11char_test(_test_data_619, _237);
    _237 = NOVALUE;
    DeRef(_test_data_619);
    return _238;
    ;
}


int _11t_graph(int _test_data_624)
{
    int _240 = NOVALUE;
    int _239 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Graphic])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _239 = (int)*(((s1_ptr)_2)->base + 16);
    Ref(_test_data_624);
    Ref(_239);
    _240 = _11char_test(_test_data_624, _239);
    _239 = NOVALUE;
    DeRef(_test_data_624);
    return _240;
    ;
}


int _11t_specword(int _test_data_629)
{
    int _242 = NOVALUE;
    int _241 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_SpecWord])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _241 = (int)*(((s1_ptr)_2)->base + 18);
    Ref(_test_data_629);
    Ref(_241);
    _242 = _11char_test(_test_data_629, _241);
    _241 = NOVALUE;
    DeRef(_test_data_629);
    return _242;
    ;
}


int _11t_bytearray(int _test_data_634)
{
    int _244 = NOVALUE;
    int _243 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Bytes])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _243 = (int)*(((s1_ptr)_2)->base + 17);
    Ref(_test_data_634);
    Ref(_243);
    _244 = _11char_test(_test_data_634, _243);
    _243 = NOVALUE;
    DeRef(_test_data_634);
    return _244;
    ;
}


int _11t_lower(int _test_data_639)
{
    int _246 = NOVALUE;
    int _245 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Lowercase])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _245 = (int)*(((s1_ptr)_2)->base + 8);
    Ref(_test_data_639);
    Ref(_245);
    _246 = _11char_test(_test_data_639, _245);
    _245 = NOVALUE;
    DeRef(_test_data_639);
    return _246;
    ;
}


int _11t_print(int _test_data_644)
{
    int _248 = NOVALUE;
    int _247 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Printable])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _247 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_test_data_644);
    Ref(_247);
    _248 = _11char_test(_test_data_644, _247);
    _247 = NOVALUE;
    DeRef(_test_data_644);
    return _248;
    ;
}


int _11t_display(int _test_data_649)
{
    int _250 = NOVALUE;
    int _249 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Displayable])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _249 = (int)*(((s1_ptr)_2)->base + 7);
    Ref(_test_data_649);
    Ref(_249);
    _250 = _11char_test(_test_data_649, _249);
    _249 = NOVALUE;
    DeRef(_test_data_649);
    return _250;
    ;
}


int _11t_punct(int _test_data_654)
{
    int _252 = NOVALUE;
    int _251 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Punctuation])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _251 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_test_data_654);
    Ref(_251);
    _252 = _11char_test(_test_data_654, _251);
    _251 = NOVALUE;
    DeRef(_test_data_654);
    return _252;
    ;
}


int _11t_space(int _test_data_659)
{
    int _254 = NOVALUE;
    int _253 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Whitespace])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _253 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_test_data_659);
    Ref(_253);
    _254 = _11char_test(_test_data_659, _253);
    _253 = NOVALUE;
    DeRef(_test_data_659);
    return _254;
    ;
}


int _11t_upper(int _test_data_664)
{
    int _256 = NOVALUE;
    int _255 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Uppercase])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _255 = (int)*(((s1_ptr)_2)->base + 9);
    Ref(_test_data_664);
    Ref(_255);
    _256 = _11char_test(_test_data_664, _255);
    _255 = NOVALUE;
    DeRef(_test_data_664);
    return _256;
    ;
}


int _11t_xdigit(int _test_data_669)
{
    int _258 = NOVALUE;
    int _257 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Hexadecimal])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _257 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_test_data_669);
    Ref(_257);
    _258 = _11char_test(_test_data_669, _257);
    _257 = NOVALUE;
    DeRef(_test_data_669);
    return _258;
    ;
}


int _11t_vowel(int _test_data_674)
{
    int _260 = NOVALUE;
    int _259 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Vowel])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _259 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_test_data_674);
    Ref(_259);
    _260 = _11char_test(_test_data_674, _259);
    _259 = NOVALUE;
    DeRef(_test_data_674);
    return _260;
    ;
}


int _11t_consonant(int _test_data_679)
{
    int _262 = NOVALUE;
    int _261 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return char_test(test_data, Defined_Sets[CS_Consonant])*/
    _2 = (int)SEQ_PTR(_11Defined_Sets_460);
    _261 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_test_data_679);
    Ref(_261);
    _262 = _11char_test(_test_data_679, _261);
    _261 = NOVALUE;
    DeRef(_test_data_679);
    return _262;
    ;
}


int _11integer_array(int _x_684)
{
    int _267 = NOVALUE;
    int _266 = NOVALUE;
    int _265 = NOVALUE;
    int _263 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _263 = IS_SEQUENCE(_x_684);
    if (_263 != 0)
    goto L1; // [6] 16
    _263 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_684);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_684)){
            _265 = SEQ_PTR(_x_684)->length;
    }
    else {
        _265 = 1;
    }
    {
        int _i_689;
        _i_689 = 1;
L2: 
        if (_i_689 > _265){
            goto L3; // [21] 54
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_684);
        _266 = (int)*(((s1_ptr)_2)->base + _i_689);
        if (IS_ATOM_INT(_266))
        _267 = 1;
        else if (IS_ATOM_DBL(_266))
        _267 = IS_ATOM_INT(DoubleToInt(_266));
        else
        _267 = 0;
        _266 = NOVALUE;
        if (_267 != 0)
        goto L4; // [37] 47
        _267 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_684);
        return 0;
L4: 

        /** 	end for*/
        _i_689 = _i_689 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_684);
    return 1;
    ;
}


int _11t_text(int _x_697)
{
    int _275 = NOVALUE;
    int _273 = NOVALUE;
    int _272 = NOVALUE;
    int _271 = NOVALUE;
    int _269 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _269 = IS_SEQUENCE(_x_697);
    if (_269 != 0)
    goto L1; // [6] 16
    _269 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_697);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_697)){
            _271 = SEQ_PTR(_x_697)->length;
    }
    else {
        _271 = 1;
    }
    {
        int _i_702;
        _i_702 = 1;
L2: 
        if (_i_702 > _271){
            goto L3; // [21] 71
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_697);
        _272 = (int)*(((s1_ptr)_2)->base + _i_702);
        if (IS_ATOM_INT(_272))
        _273 = 1;
        else if (IS_ATOM_DBL(_272))
        _273 = IS_ATOM_INT(DoubleToInt(_272));
        else
        _273 = 0;
        _272 = NOVALUE;
        if (_273 != 0)
        goto L4; // [37] 47
        _273 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_697);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_697);
        _275 = (int)*(((s1_ptr)_2)->base + _i_702);
        if (binary_op_a(GREATEREQ, _275, 0)){
            _275 = NOVALUE;
            goto L5; // [53] 64
        }
        _275 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_697);
        return 0;
L5: 

        /** 	end for*/
        _i_702 = _i_702 + 1;
        goto L2; // [66] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_697);
    return 1;
    ;
}


int _11number_array(int _x_713)
{
    int _281 = NOVALUE;
    int _280 = NOVALUE;
    int _279 = NOVALUE;
    int _277 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _277 = IS_SEQUENCE(_x_713);
    if (_277 != 0)
    goto L1; // [6] 16
    _277 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_713);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_713)){
            _279 = SEQ_PTR(_x_713)->length;
    }
    else {
        _279 = 1;
    }
    {
        int _i_718;
        _i_718 = 1;
L2: 
        if (_i_718 > _279){
            goto L3; // [21] 54
        }

        /** 		if not atom(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_713);
        _280 = (int)*(((s1_ptr)_2)->base + _i_718);
        _281 = IS_ATOM(_280);
        _280 = NOVALUE;
        if (_281 != 0)
        goto L4; // [37] 47
        _281 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_713);
        return 0;
L4: 

        /** 	end for*/
        _i_718 = _i_718 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_713);
    return 1;
    ;
}


int _11sequence_array(int _x_726)
{
    int _287 = NOVALUE;
    int _286 = NOVALUE;
    int _285 = NOVALUE;
    int _283 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _283 = IS_SEQUENCE(_x_726);
    if (_283 != 0)
    goto L1; // [6] 16
    _283 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_726);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_726)){
            _285 = SEQ_PTR(_x_726)->length;
    }
    else {
        _285 = 1;
    }
    {
        int _i_731;
        _i_731 = 1;
L2: 
        if (_i_731 > _285){
            goto L3; // [21] 54
        }

        /** 		if not sequence(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_726);
        _286 = (int)*(((s1_ptr)_2)->base + _i_731);
        _287 = IS_SEQUENCE(_286);
        _286 = NOVALUE;
        if (_287 != 0)
        goto L4; // [37] 47
        _287 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_726);
        return 0;
L4: 

        /** 	end for*/
        _i_731 = _i_731 + 1;
        goto L2; // [49] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_726);
    return 1;
    ;
}


int _11ascii_string(int _x_739)
{
    int _297 = NOVALUE;
    int _295 = NOVALUE;
    int _293 = NOVALUE;
    int _292 = NOVALUE;
    int _291 = NOVALUE;
    int _289 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _289 = IS_SEQUENCE(_x_739);
    if (_289 != 0)
    goto L1; // [6] 16
    _289 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_739);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_739)){
            _291 = SEQ_PTR(_x_739)->length;
    }
    else {
        _291 = 1;
    }
    {
        int _i_744;
        _i_744 = 1;
L2: 
        if (_i_744 > _291){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_739);
        _292 = (int)*(((s1_ptr)_2)->base + _i_744);
        if (IS_ATOM_INT(_292))
        _293 = 1;
        else if (IS_ATOM_DBL(_292))
        _293 = IS_ATOM_INT(DoubleToInt(_292));
        else
        _293 = 0;
        _292 = NOVALUE;
        if (_293 != 0)
        goto L4; // [37] 47
        _293 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_739);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_739);
        _295 = (int)*(((s1_ptr)_2)->base + _i_744);
        if (binary_op_a(GREATEREQ, _295, 0)){
            _295 = NOVALUE;
            goto L5; // [53] 64
        }
        _295 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_739);
        return 0;
L5: 

        /** 		if x[i] > 127 then*/
        _2 = (int)SEQ_PTR(_x_739);
        _297 = (int)*(((s1_ptr)_2)->base + _i_744);
        if (binary_op_a(LESSEQ, _297, 127)){
            _297 = NOVALUE;
            goto L6; // [70] 81
        }
        _297 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_739);
        return 0;
L6: 

        /** 	end for*/
        _i_744 = _i_744 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_739);
    return 1;
    ;
}


int _11string(int _x_758)
{
    int _307 = NOVALUE;
    int _305 = NOVALUE;
    int _303 = NOVALUE;
    int _302 = NOVALUE;
    int _301 = NOVALUE;
    int _299 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _299 = IS_SEQUENCE(_x_758);
    if (_299 != 0)
    goto L1; // [6] 16
    _299 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_758);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_758)){
            _301 = SEQ_PTR(_x_758)->length;
    }
    else {
        _301 = 1;
    }
    {
        int _i_763;
        _i_763 = 1;
L2: 
        if (_i_763 > _301){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_758);
        _302 = (int)*(((s1_ptr)_2)->base + _i_763);
        if (IS_ATOM_INT(_302))
        _303 = 1;
        else if (IS_ATOM_DBL(_302))
        _303 = IS_ATOM_INT(DoubleToInt(_302));
        else
        _303 = 0;
        _302 = NOVALUE;
        if (_303 != 0)
        goto L4; // [37] 47
        _303 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_758);
        return 0;
L4: 

        /** 		if x[i] < 0 then*/
        _2 = (int)SEQ_PTR(_x_758);
        _305 = (int)*(((s1_ptr)_2)->base + _i_763);
        if (binary_op_a(GREATEREQ, _305, 0)){
            _305 = NOVALUE;
            goto L5; // [53] 64
        }
        _305 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_758);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_758);
        _307 = (int)*(((s1_ptr)_2)->base + _i_763);
        if (binary_op_a(LESSEQ, _307, 255)){
            _307 = NOVALUE;
            goto L6; // [70] 81
        }
        _307 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_758);
        return 0;
L6: 

        /** 	end for*/
        _i_763 = _i_763 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_758);
    return 1;
    ;
}


int _11cstring(int _x_777)
{
    int _317 = NOVALUE;
    int _315 = NOVALUE;
    int _313 = NOVALUE;
    int _312 = NOVALUE;
    int _311 = NOVALUE;
    int _309 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not sequence(x) then*/
    _309 = IS_SEQUENCE(_x_777);
    if (_309 != 0)
    goto L1; // [6] 16
    _309 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_777);
    return 0;
L1: 

    /** 	for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_777)){
            _311 = SEQ_PTR(_x_777)->length;
    }
    else {
        _311 = 1;
    }
    {
        int _i_782;
        _i_782 = 1;
L2: 
        if (_i_782 > _311){
            goto L3; // [21] 88
        }

        /** 		if not integer(x[i]) then*/
        _2 = (int)SEQ_PTR(_x_777);
        _312 = (int)*(((s1_ptr)_2)->base + _i_782);
        if (IS_ATOM_INT(_312))
        _313 = 1;
        else if (IS_ATOM_DBL(_312))
        _313 = IS_ATOM_INT(DoubleToInt(_312));
        else
        _313 = 0;
        _312 = NOVALUE;
        if (_313 != 0)
        goto L4; // [37] 47
        _313 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_777);
        return 0;
L4: 

        /** 		if x[i] <= 0 then*/
        _2 = (int)SEQ_PTR(_x_777);
        _315 = (int)*(((s1_ptr)_2)->base + _i_782);
        if (binary_op_a(GREATER, _315, 0)){
            _315 = NOVALUE;
            goto L5; // [53] 64
        }
        _315 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_777);
        return 0;
L5: 

        /** 		if x[i] > 255 then*/
        _2 = (int)SEQ_PTR(_x_777);
        _317 = (int)*(((s1_ptr)_2)->base + _i_782);
        if (binary_op_a(LESSEQ, _317, 255)){
            _317 = NOVALUE;
            goto L6; // [70] 81
        }
        _317 = NOVALUE;

        /** 			return 0*/
        DeRef(_x_777);
        return 0;
L6: 

        /** 	end for*/
        _i_782 = _i_782 + 1;
        goto L2; // [83] 28
L3: 
        ;
    }

    /** 	return 1*/
    DeRef(_x_777);
    return 1;
    ;
}



// 0xFF52F31C
