// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _28calc_primes(int _approx_limit_10875, int _time_limit_p_10876)
{
    int _result__10877 = NOVALUE;
    int _candidate__10878 = NOVALUE;
    int _pos__10879 = NOVALUE;
    int _time_out__10880 = NOVALUE;
    int _maxp__10881 = NOVALUE;
    int _maxf__10882 = NOVALUE;
    int _maxf_idx_10883 = NOVALUE;
    int _next_trigger_10884 = NOVALUE;
    int _growth_10885 = NOVALUE;
    int _6211 = NOVALUE;
    int _6208 = NOVALUE;
    int _6206 = NOVALUE;
    int _6203 = NOVALUE;
    int _6200 = NOVALUE;
    int _6197 = NOVALUE;
    int _6190 = NOVALUE;
    int _6188 = NOVALUE;
    int _6185 = NOVALUE;
    int _6182 = NOVALUE;
    int _6178 = NOVALUE;
    int _6177 = NOVALUE;
    int _6173 = NOVALUE;
    int _6167 = NOVALUE;
    int _6165 = NOVALUE;
    int _6163 = NOVALUE;
    int _6158 = NOVALUE;
    int _6157 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if approx_limit <= list_of_primes[$] then*/
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _6157 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _6157 = 1;
    }
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _6158 = (int)*(((s1_ptr)_2)->base + _6157);
    if (binary_op_a(GREATER, _approx_limit_10875, _6158)){
        _6158 = NOVALUE;
        goto L1; // [16] 65
    }
    _6158 = NOVALUE;

    /** 		pos_ = search:binary_search(approx_limit, list_of_primes)*/
    RefDS(_28list_of_primes_10871);
    _pos__10879 = _14binary_search(_approx_limit_10875, _28list_of_primes_10871, 1, 0);
    if (!IS_ATOM_INT(_pos__10879)) {
        _1 = (long)(DBL_PTR(_pos__10879)->dbl);
        if (UNIQUE(DBL_PTR(_pos__10879)) && (DBL_PTR(_pos__10879)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos__10879);
        _pos__10879 = _1;
    }

    /** 		if pos_ < 0 then*/
    if (_pos__10879 >= 0)
    goto L2; // [37] 51

    /** 			pos_ = (-pos_)*/
    _pos__10879 = - _pos__10879;
L2: 

    /** 		return list_of_primes[1..pos_]*/
    rhs_slice_target = (object_ptr)&_6163;
    RHS_Slice(_28list_of_primes_10871, 1, _pos__10879);
    DeRef(_result__10877);
    DeRef(_time_out__10880);
    return _6163;
L1: 

    /** 	pos_ = length(list_of_primes)*/
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _pos__10879 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _pos__10879 = 1;
    }

    /** 	candidate_ = list_of_primes[$]*/
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _6165 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _6165 = 1;
    }
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _candidate__10878 = (int)*(((s1_ptr)_2)->base + _6165);
    if (!IS_ATOM_INT(_candidate__10878))
    _candidate__10878 = (long)DBL_PTR(_candidate__10878)->dbl;

    /** 	maxf_ = floor(power(candidate_, 0.5))*/
    temp_d.dbl = (double)_candidate__10878;
    _6167 = Dpower(&temp_d, DBL_PTR(_1715));
    _maxf__10882 = unary_op(FLOOR, _6167);
    DeRefDS(_6167);
    _6167 = NOVALUE;
    if (!IS_ATOM_INT(_maxf__10882)) {
        _1 = (long)(DBL_PTR(_maxf__10882)->dbl);
        if (UNIQUE(DBL_PTR(_maxf__10882)) && (DBL_PTR(_maxf__10882)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf__10882);
        _maxf__10882 = _1;
    }

    /** 	maxf_idx = search:binary_search(maxf_, list_of_primes)*/
    RefDS(_28list_of_primes_10871);
    _maxf_idx_10883 = _14binary_search(_maxf__10882, _28list_of_primes_10871, 1, 0);
    if (!IS_ATOM_INT(_maxf_idx_10883)) {
        _1 = (long)(DBL_PTR(_maxf_idx_10883)->dbl);
        if (UNIQUE(DBL_PTR(_maxf_idx_10883)) && (DBL_PTR(_maxf_idx_10883)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf_idx_10883);
        _maxf_idx_10883 = _1;
    }

    /** 	if maxf_idx < 0 then*/
    if (_maxf_idx_10883 >= 0)
    goto L3; // [117] 141

    /** 		maxf_idx = (-maxf_idx)*/
    _maxf_idx_10883 = - _maxf_idx_10883;

    /** 		maxf_ = list_of_primes[maxf_idx]*/
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _maxf__10882 = (int)*(((s1_ptr)_2)->base + _maxf_idx_10883);
    if (!IS_ATOM_INT(_maxf__10882))
    _maxf__10882 = (long)DBL_PTR(_maxf__10882)->dbl;
L3: 

    /** 	next_trigger = list_of_primes[maxf_idx+1]*/
    _6173 = _maxf_idx_10883 + 1;
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _next_trigger_10884 = (int)*(((s1_ptr)_2)->base + _6173);
    if (!IS_ATOM_INT(_next_trigger_10884))
    _next_trigger_10884 = (long)DBL_PTR(_next_trigger_10884)->dbl;

    /** 	next_trigger *= next_trigger*/
    _next_trigger_10884 = _next_trigger_10884 * _next_trigger_10884;

    /** 	growth = floor(approx_limit  / 3.5) - length(list_of_primes)*/
    _2 = binary_op(DIVIDE, _approx_limit_10875, _6176);
    _6177 = unary_op(FLOOR, _2);
    DeRef(_2);
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _6178 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _6178 = 1;
    }
    if (IS_ATOM_INT(_6177)) {
        _growth_10885 = _6177 - _6178;
    }
    else {
        _growth_10885 = NewDouble(DBL_PTR(_6177)->dbl - (double)_6178);
    }
    DeRef(_6177);
    _6177 = NOVALUE;
    _6178 = NOVALUE;
    if (!IS_ATOM_INT(_growth_10885)) {
        _1 = (long)(DBL_PTR(_growth_10885)->dbl);
        if (UNIQUE(DBL_PTR(_growth_10885)) && (DBL_PTR(_growth_10885)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_growth_10885);
        _growth_10885 = _1;
    }

    /** 	if growth <= 0 then*/
    if (_growth_10885 > 0)
    goto L4; // [186] 200

    /** 		growth = length(list_of_primes)*/
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _growth_10885 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _growth_10885 = 1;
    }
L4: 

    /** 	result_ = list_of_primes & repeat(0, growth)*/
    _6182 = Repeat(0, _growth_10885);
    Concat((object_ptr)&_result__10877, _28list_of_primes_10871, _6182);
    DeRefDS(_6182);
    _6182 = NOVALUE;

    /** 	if time_limit_p < 0 then*/
    if (_time_limit_p_10876 >= 0)
    goto L5; // [214] 229

    /** 		time_out_ = time() + 100_000_000*/
    DeRef(_6185);
    _6185 = NewDouble(current_time());
    DeRef(_time_out__10880);
    _time_out__10880 = NewDouble(DBL_PTR(_6185)->dbl + (double)100000000);
    DeRefDS(_6185);
    _6185 = NOVALUE;
    goto L6; // [226] 238
L5: 

    /** 		time_out_ = time() + time_limit_p*/
    DeRef(_6188);
    _6188 = NewDouble(current_time());
    DeRef(_time_out__10880);
    _time_out__10880 = NewDouble(DBL_PTR(_6188)->dbl + (double)_time_limit_p_10876);
    DeRefDS(_6188);
    _6188 = NOVALUE;
L6: 

    /** 	while time_out_ >= time()  label "MW" do*/
L7: 
    DeRef(_6190);
    _6190 = NewDouble(current_time());
    if (binary_op_a(LESS, _time_out__10880, _6190)){
        DeRefDS(_6190);
        _6190 = NOVALUE;
        goto L8; // [247] 410
    }
    DeRef(_6190);
    _6190 = NOVALUE;

    /** 		task_yield()*/
    task_yield();

    /** 		candidate_ += 2*/
    _candidate__10878 = _candidate__10878 + 2;

    /** 		if candidate_ >= next_trigger then*/
    if (_candidate__10878 < _next_trigger_10884)
    goto L9; // [264] 307

    /** 			maxf_idx += 1*/
    _maxf_idx_10883 = _maxf_idx_10883 + 1;

    /** 			maxf_ = result_[maxf_idx]*/
    _2 = (int)SEQ_PTR(_result__10877);
    _maxf__10882 = (int)*(((s1_ptr)_2)->base + _maxf_idx_10883);
    if (!IS_ATOM_INT(_maxf__10882))
    _maxf__10882 = (long)DBL_PTR(_maxf__10882)->dbl;

    /** 			next_trigger = result_[maxf_idx+1]*/
    _6197 = _maxf_idx_10883 + 1;
    _2 = (int)SEQ_PTR(_result__10877);
    _next_trigger_10884 = (int)*(((s1_ptr)_2)->base + _6197);
    if (!IS_ATOM_INT(_next_trigger_10884))
    _next_trigger_10884 = (long)DBL_PTR(_next_trigger_10884)->dbl;

    /** 			next_trigger *= next_trigger*/
    _next_trigger_10884 = _next_trigger_10884 * _next_trigger_10884;
L9: 

    /** 		for i = 2 to pos_ do*/
    _6200 = _pos__10879;
    {
        int _i_10938;
        _i_10938 = 2;
LA: 
        if (_i_10938 > _6200){
            goto LB; // [312] 360
        }

        /** 			maxp_ = result_[i]*/
        _2 = (int)SEQ_PTR(_result__10877);
        _maxp__10881 = (int)*(((s1_ptr)_2)->base + _i_10938);
        if (!IS_ATOM_INT(_maxp__10881))
        _maxp__10881 = (long)DBL_PTR(_maxp__10881)->dbl;

        /** 			if maxp_ > maxf_ then*/
        if (_maxp__10881 <= _maxf__10882)
        goto LC; // [329] 338

        /** 				exit*/
        goto LB; // [335] 360
LC: 

        /** 			if remainder(candidate_, maxp_) = 0 then*/
        _6203 = (_candidate__10878 % _maxp__10881);
        if (_6203 != 0)
        goto LD; // [344] 353

        /** 				continue "MW"*/
        goto L7; // [350] 243
LD: 

        /** 		end for*/
        _i_10938 = _i_10938 + 1;
        goto LA; // [355] 319
LB: 
        ;
    }

    /** 		pos_ += 1*/
    _pos__10879 = _pos__10879 + 1;

    /** 		if pos_ >= length(result_) then*/
    if (IS_SEQUENCE(_result__10877)){
            _6206 = SEQ_PTR(_result__10877)->length;
    }
    else {
        _6206 = 1;
    }
    if (_pos__10879 < _6206)
    goto LE; // [373] 388

    /** 			result_ &= repeat(0, 1000)*/
    _6208 = Repeat(0, 1000);
    Concat((object_ptr)&_result__10877, _result__10877, _6208);
    DeRefDS(_6208);
    _6208 = NOVALUE;
LE: 

    /** 		result_[pos_] = candidate_*/
    _2 = (int)SEQ_PTR(_result__10877);
    _2 = (int)(((s1_ptr)_2)->base + _pos__10879);
    _1 = *(int *)_2;
    *(int *)_2 = _candidate__10878;
    DeRef(_1);

    /** 		if candidate_ >= approx_limit then*/
    if (_candidate__10878 < _approx_limit_10875)
    goto L7; // [396] 243

    /** 			exit*/
    goto L8; // [402] 410

    /** 	end while*/
    goto L7; // [407] 243
L8: 

    /** 	return result_[1..pos_]*/
    rhs_slice_target = (object_ptr)&_6211;
    RHS_Slice(_result__10877, 1, _pos__10879);
    DeRefDS(_result__10877);
    DeRef(_time_out__10880);
    DeRef(_6163);
    _6163 = NOVALUE;
    DeRef(_6173);
    _6173 = NOVALUE;
    DeRef(_6197);
    _6197 = NOVALUE;
    DeRef(_6203);
    _6203 = NOVALUE;
    return _6211;
    ;
}


int _28next_prime(int _n_10957, int _fail_signal_p_10958, int _time_out_p_10959)
{
    int _i_10960 = NOVALUE;
    int _6231 = NOVALUE;
    int _6225 = NOVALUE;
    int _6224 = NOVALUE;
    int _6223 = NOVALUE;
    int _6222 = NOVALUE;
    int _6221 = NOVALUE;
    int _6218 = NOVALUE;
    int _6217 = NOVALUE;
    int _6214 = NOVALUE;
    int _6213 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if n < 0 then*/
    if (_n_10957 >= 0)
    goto L1; // [7] 18

    /** 		return fail_signal_p*/
    return _fail_signal_p_10958;
L1: 

    /** 	if list_of_primes[$] < n then*/
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _6213 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _6213 = 1;
    }
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _6214 = (int)*(((s1_ptr)_2)->base + _6213);
    if (binary_op_a(GREATEREQ, _6214, _n_10957)){
        _6214 = NOVALUE;
        goto L2; // [29] 43
    }
    _6214 = NOVALUE;

    /** 		list_of_primes = calc_primes(n,time_out_p)*/
    _0 = _28calc_primes(_n_10957, _time_out_p_10959);
    DeRefDS(_28list_of_primes_10871);
    _28list_of_primes_10871 = _0;
L2: 

    /** 	if n > list_of_primes[$] then*/
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _6217 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _6217 = 1;
    }
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _6218 = (int)*(((s1_ptr)_2)->base + _6217);
    if (binary_op_a(LESSEQ, _n_10957, _6218)){
        _6218 = NOVALUE;
        goto L3; // [54] 65
    }
    _6218 = NOVALUE;

    /** 		return fail_signal_p*/
    return _fail_signal_p_10958;
L3: 

    /** 	if n < 1009 and 1009 <= list_of_primes[$] then*/
    _6221 = (_n_10957 < 1009);
    if (_6221 == 0) {
        goto L4; // [71] 110
    }
    if (IS_SEQUENCE(_28list_of_primes_10871)){
            _6223 = SEQ_PTR(_28list_of_primes_10871)->length;
    }
    else {
        _6223 = 1;
    }
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _6224 = (int)*(((s1_ptr)_2)->base + _6223);
    if (IS_ATOM_INT(_6224)) {
        _6225 = (1009 <= _6224);
    }
    else {
        _6225 = binary_op(LESSEQ, 1009, _6224);
    }
    _6224 = NOVALUE;
    if (_6225 == 0) {
        DeRef(_6225);
        _6225 = NOVALUE;
        goto L4; // [89] 110
    }
    else {
        if (!IS_ATOM_INT(_6225) && DBL_PTR(_6225)->dbl == 0.0){
            DeRef(_6225);
            _6225 = NOVALUE;
            goto L4; // [89] 110
        }
        DeRef(_6225);
        _6225 = NOVALUE;
    }
    DeRef(_6225);
    _6225 = NOVALUE;

    /** 		i = search:binary_search(n, list_of_primes, ,169)*/
    RefDS(_28list_of_primes_10871);
    _i_10960 = _14binary_search(_n_10957, _28list_of_primes_10871, 1, 169);
    if (!IS_ATOM_INT(_i_10960)) {
        _1 = (long)(DBL_PTR(_i_10960)->dbl);
        if (UNIQUE(DBL_PTR(_i_10960)) && (DBL_PTR(_i_10960)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_10960);
        _i_10960 = _1;
    }
    goto L5; // [107] 126
L4: 

    /** 		i = search:binary_search(n, list_of_primes)*/
    RefDS(_28list_of_primes_10871);
    _i_10960 = _14binary_search(_n_10957, _28list_of_primes_10871, 1, 0);
    if (!IS_ATOM_INT(_i_10960)) {
        _1 = (long)(DBL_PTR(_i_10960)->dbl);
        if (UNIQUE(DBL_PTR(_i_10960)) && (DBL_PTR(_i_10960)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_10960);
        _i_10960 = _1;
    }
L5: 

    /** 	if i < 0 then*/
    if (_i_10960 >= 0)
    goto L6; // [130] 144

    /** 		i = (-i)*/
    _i_10960 = - _i_10960;
L6: 

    /** 	return list_of_primes[i]*/
    _2 = (int)SEQ_PTR(_28list_of_primes_10871);
    _6231 = (int)*(((s1_ptr)_2)->base + _i_10960);
    Ref(_6231);
    DeRef(_fail_signal_p_10958);
    DeRef(_6221);
    _6221 = NOVALUE;
    return _6231;
    ;
}



// 0x44411DE3
