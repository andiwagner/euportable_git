// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _9positive_int(int _x_266)
{
    int _41 = NOVALUE;
    int _39 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(x) then*/
    if (IS_ATOM_INT(_x_266))
    _39 = 1;
    else if (IS_ATOM_DBL(_x_266))
    _39 = IS_ATOM_INT(DoubleToInt(_x_266));
    else
    _39 = 0;
    if (_39 != 0)
    goto L1; // [6] 16
    _39 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_266);
    return 0;
L1: 

    /**     return x >= 1*/
    if (IS_ATOM_INT(_x_266)) {
        _41 = (_x_266 >= 1);
    }
    else {
        _41 = binary_op(GREATEREQ, _x_266, 1);
    }
    DeRef(_x_266);
    return _41;
    ;
}


int _9machine_addr(int _a_273)
{
    int _50 = NOVALUE;
    int _49 = NOVALUE;
    int _48 = NOVALUE;
    int _46 = NOVALUE;
    int _44 = NOVALUE;
    int _42 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(a) then*/
    _42 = IS_ATOM(_a_273);
    if (_42 != 0)
    goto L1; // [6] 16
    _42 = NOVALUE;

    /** 		return 0*/
    DeRef(_a_273);
    return 0;
L1: 

    /** 	if not integer(a)then*/
    if (IS_ATOM_INT(_a_273))
    _44 = 1;
    else if (IS_ATOM_DBL(_a_273))
    _44 = IS_ATOM_INT(DoubleToInt(_a_273));
    else
    _44 = 0;
    if (_44 != 0)
    goto L2; // [21] 41
    _44 = NOVALUE;

    /** 		if floor(a) != a then*/
    if (IS_ATOM_INT(_a_273))
    _46 = e_floor(_a_273);
    else
    _46 = unary_op(FLOOR, _a_273);
    if (binary_op_a(EQUALS, _46, _a_273)){
        DeRef(_46);
        _46 = NOVALUE;
        goto L3; // [29] 40
    }
    DeRef(_46);
    _46 = NOVALUE;

    /** 			return 0*/
    DeRef(_a_273);
    return 0;
L3: 
L2: 

    /** 	return a > 0 and a <= MAX_ADDR*/
    if (IS_ATOM_INT(_a_273)) {
        _48 = (_a_273 > 0);
    }
    else {
        _48 = binary_op(GREATER, _a_273, 0);
    }
    if (IS_ATOM_INT(_a_273) && IS_ATOM_INT(_9MAX_ADDR_259)) {
        _49 = (_a_273 <= _9MAX_ADDR_259);
    }
    else {
        _49 = binary_op(LESSEQ, _a_273, _9MAX_ADDR_259);
    }
    if (IS_ATOM_INT(_48) && IS_ATOM_INT(_49)) {
        _50 = (_48 != 0 && _49 != 0);
    }
    else {
        _50 = binary_op(AND, _48, _49);
    }
    DeRef(_48);
    _48 = NOVALUE;
    DeRef(_49);
    _49 = NOVALUE;
    DeRef(_a_273);
    return _50;
    ;
}


void _9deallocate(int _addr_288)
{
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_288);

    /** end procedure*/
    DeRef(_addr_288);
    return;
    ;
}


void _9register_block(int _block_addr_295, int _block_len_296, int _protection_297)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_protection_297)) {
        _1 = (long)(DBL_PTR(_protection_297)->dbl);
        if (UNIQUE(DBL_PTR(_protection_297)) && (DBL_PTR(_protection_297)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_297);
        _protection_297 = _1;
    }

    /** end procedure*/
    return;
    ;
}


void _9unregister_block(int _block_addr_301)
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _9safe_address(int _start_305, int _len_306, int _action_307)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_len_306)) {
        _1 = (long)(DBL_PTR(_len_306)->dbl);
        if (UNIQUE(DBL_PTR(_len_306)) && (DBL_PTR(_len_306)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_len_306);
        _len_306 = _1;
    }

    /** 	return 1*/
    return 1;
    ;
}


void _9check_all_blocks()
{
    int _0, _1, _2;
    

    /** end procedure*/
    return;
    ;
}


int _9prepare_block(int _addr_314, int _a_315, int _protection_316)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_315)) {
        _1 = (long)(DBL_PTR(_a_315)->dbl);
        if (UNIQUE(DBL_PTR(_a_315)) && (DBL_PTR(_a_315)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_315);
        _a_315 = _1;
    }
    if (!IS_ATOM_INT(_protection_316)) {
        _1 = (long)(DBL_PTR(_protection_316)->dbl);
        if (UNIQUE(DBL_PTR(_protection_316)) && (DBL_PTR(_protection_316)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_316);
        _protection_316 = _1;
    }

    /** 	return addr*/
    return _addr_314;
    ;
}


int _9bordered_address(int _addr_324)
{
    int _55 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(addr) then*/
    _55 = IS_ATOM(_addr_324);
    if (_55 != 0)
    goto L1; // [6] 16
    _55 = NOVALUE;

    /** 		return 0*/
    DeRef(_addr_324);
    return 0;
L1: 

    /** 	return 1*/
    DeRef(_addr_324);
    return 1;
    ;
}


int _9dep_works()
{
    int _57 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return (DEP_really_works and use_DEP)*/
    _57 = (_8DEP_really_works_230 != 0 && 1 != 0);
    return _57;

    /** 	return 1*/
    _57 = NOVALUE;
    return 1;
    ;
}


void _9free_code(int _addr_336, int _size_337, int _wordsize_339)
{
    int _61 = NOVALUE;
    int _60 = NOVALUE;
    int _59 = NOVALUE;
    int _58 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_size_337)) {
        _1 = (long)(DBL_PTR(_size_337)->dbl);
        if (UNIQUE(DBL_PTR(_size_337)) && (DBL_PTR(_size_337)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size_337);
        _size_337 = _1;
    }

    /** 		if dep_works() then*/
    _58 = _9dep_works();
    if (_58 == 0) {
        DeRef(_58);
        _58 = NOVALUE;
        goto L1; // [10] 37
    }
    else {
        if (!IS_ATOM_INT(_58) && DBL_PTR(_58)->dbl == 0.0){
            DeRef(_58);
            _58 = NOVALUE;
            goto L1; // [10] 37
        }
        DeRef(_58);
        _58 = NOVALUE;
    }
    DeRef(_58);
    _58 = NOVALUE;

    /** 			c_func(VirtualFree_rid, { addr, size*wordsize, MEM_RELEASE })*/
    if (IS_ATOM_INT(_wordsize_339)) {
        if (_size_337 == (short)_size_337 && _wordsize_339 <= INT15 && _wordsize_339 >= -INT15)
        _59 = _size_337 * _wordsize_339;
        else
        _59 = NewDouble(_size_337 * (double)_wordsize_339);
    }
    else {
        _59 = binary_op(MULTIPLY, _size_337, _wordsize_339);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_addr_336);
    *((int *)(_2+4)) = _addr_336;
    *((int *)(_2+8)) = _59;
    *((int *)(_2+12)) = 32768;
    _60 = MAKE_SEQ(_1);
    _59 = NOVALUE;
    _61 = call_c(1, _9VirtualFree_rid_333, _60);
    DeRefDS(_60);
    _60 = NOVALUE;
    goto L2; // [34] 43
L1: 

    /** 			machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_336);
L2: 

    /** end procedure*/
    DeRef(_addr_336);
    DeRef(_wordsize_339);
    DeRef(_61);
    _61 = NOVALUE;
    return;
    ;
}



// 0x1ED40A20
