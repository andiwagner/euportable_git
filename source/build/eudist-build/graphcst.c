// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _31color(int _x_13157)
{
    int _7519 = NOVALUE;
    int _7518 = NOVALUE;
    int _7517 = NOVALUE;
    int _7516 = NOVALUE;
    int _7515 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) and x >= 0 and x <= 255 then*/
    if (IS_ATOM_INT(_x_13157))
    _7515 = 1;
    else if (IS_ATOM_DBL(_x_13157))
    _7515 = IS_ATOM_INT(DoubleToInt(_x_13157));
    else
    _7515 = 0;
    if (_7515 == 0) {
        _7516 = 0;
        goto L1; // [6] 18
    }
    if (IS_ATOM_INT(_x_13157)) {
        _7517 = (_x_13157 >= 0);
    }
    else {
        _7517 = binary_op(GREATEREQ, _x_13157, 0);
    }
    if (IS_ATOM_INT(_7517))
    _7516 = (_7517 != 0);
    else
    _7516 = DBL_PTR(_7517)->dbl != 0.0;
L1: 
    if (_7516 == 0) {
        goto L2; // [18] 39
    }
    if (IS_ATOM_INT(_x_13157)) {
        _7519 = (_x_13157 <= 255);
    }
    else {
        _7519 = binary_op(LESSEQ, _x_13157, 255);
    }
    if (_7519 == 0) {
        DeRef(_7519);
        _7519 = NOVALUE;
        goto L2; // [27] 39
    }
    else {
        if (!IS_ATOM_INT(_7519) && DBL_PTR(_7519)->dbl == 0.0){
            DeRef(_7519);
            _7519 = NOVALUE;
            goto L2; // [27] 39
        }
        DeRef(_7519);
        _7519 = NOVALUE;
    }
    DeRef(_7519);
    _7519 = NOVALUE;

    /** 		return 1*/
    DeRef(_x_13157);
    DeRef(_7517);
    _7517 = NOVALUE;
    return 1;
    goto L3; // [36] 46
L2: 

    /** 		return 0*/
    DeRef(_x_13157);
    DeRef(_7517);
    _7517 = NOVALUE;
    return 0;
L3: 
    ;
}


int _31mixture(int _s_13167)
{
    int _7528 = NOVALUE;
    int _7526 = NOVALUE;
    int _7524 = NOVALUE;
    int _7523 = NOVALUE;
    int _7521 = NOVALUE;
    int _7520 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(s) then*/
    _7520 = IS_ATOM(_s_13167);
    if (_7520 == 0)
    {
        _7520 = NOVALUE;
        goto L1; // [6] 16
    }
    else{
        _7520 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_s_13167);
    return 0;
L1: 

    /** 	if length(s) != 3 then*/
    if (IS_SEQUENCE(_s_13167)){
            _7521 = SEQ_PTR(_s_13167)->length;
    }
    else {
        _7521 = 1;
    }
    if (_7521 == 3)
    goto L2; // [21] 32

    /** 		return 0*/
    DeRef(_s_13167);
    return 0;
L2: 

    /** 	for i=1 to 3 do*/
    {
        int _i_13174;
        _i_13174 = 1;
L3: 
        if (_i_13174 > 3){
            goto L4; // [34] 87
        }

        /** 		if not integer(s[i]) then*/
        _2 = (int)SEQ_PTR(_s_13167);
        _7523 = (int)*(((s1_ptr)_2)->base + _i_13174);
        if (IS_ATOM_INT(_7523))
        _7524 = 1;
        else if (IS_ATOM_DBL(_7523))
        _7524 = IS_ATOM_INT(DoubleToInt(_7523));
        else
        _7524 = 0;
        _7523 = NOVALUE;
        if (_7524 != 0)
        goto L5; // [50] 60
        _7524 = NOVALUE;

        /** 			return 0*/
        DeRef(_s_13167);
        return 0;
L5: 

        /**   		if and_bits(s[i],#FFFFFFC0) then*/
        _2 = (int)SEQ_PTR(_s_13167);
        _7526 = (int)*(((s1_ptr)_2)->base + _i_13174);
        _7528 = binary_op(AND_BITS, _7526, _7527);
        _7526 = NOVALUE;
        if (_7528 == 0) {
            DeRef(_7528);
            _7528 = NOVALUE;
            goto L6; // [70] 80
        }
        else {
            if (!IS_ATOM_INT(_7528) && DBL_PTR(_7528)->dbl == 0.0){
                DeRef(_7528);
                _7528 = NOVALUE;
                goto L6; // [70] 80
            }
            DeRef(_7528);
            _7528 = NOVALUE;
        }
        DeRef(_7528);
        _7528 = NOVALUE;

        /**   			return 0*/
        DeRef(_s_13167);
        return 0;
L6: 

        /** 	end for*/
        _i_13174 = _i_13174 + 1;
        goto L3; // [82] 41
L4: 
        ;
    }

    /** 	return 1*/
    DeRef(_s_13167);
    return 1;
    ;
}


int _31video_config()
{
    int _7529 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return machine_func(M_VIDEO_CONFIG, 0)*/
    _7529 = machine(13, 0);
    return _7529;
    ;
}



// 0xF71BE1C0
