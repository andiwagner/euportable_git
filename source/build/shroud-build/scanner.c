// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _62set_qualified_fwd(int _fwd_24658)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_fwd_24658)) {
        _1 = (long)(DBL_PTR(_fwd_24658)->dbl);
        if (UNIQUE(DBL_PTR(_fwd_24658)) && (DBL_PTR(_fwd_24658)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwd_24658);
        _fwd_24658 = _1;
    }

    /** 	qualified_fwd = fwd*/
    _62qualified_fwd_24655 = _fwd_24658;

    /** end procedure*/
    return;
    ;
}


int _62get_qualified_fwd()
{
    int _fwd_24661 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer fwd = qualified_fwd*/
    _fwd_24661 = _62qualified_fwd_24655;

    /** 	set_qualified_fwd( -1 )*/

    /** 	qualified_fwd = fwd*/
    _62qualified_fwd_24655 = -1;

    /** end procedure*/
    goto L1; // [17] 20
L1: 

    /** 	return fwd*/
    return _fwd_24661;
    ;
}


void _62InitLex()
{
    int _14304 = NOVALUE;
    int _14303 = NOVALUE;
    int _14302 = NOVALUE;
    int _14300 = NOVALUE;
    int _14299 = NOVALUE;
    int _14298 = NOVALUE;
    int _14297 = NOVALUE;
    int _0, _1, _2;
    

    /** 	gline_number = 0*/
    _38gline_number_16951 = 0;

    /** 	line_number = 0*/
    _38line_number_16947 = 0;

    /** 	IncludeStk = {}*/
    RefDS(_5);
    DeRef(_62IncludeStk_24632);
    _62IncludeStk_24632 = _5;

    /** 	char_class = repeat(ILLEGAL_CHAR, 255)  -- we screen out the 0 character*/
    DeRefi(_62char_class_24630);
    _62char_class_24630 = Repeat(-20, 255);

    /** 	char_class['0'..'9'] = DIGIT*/
    assign_slice_seq = (s1_ptr *)&_62char_class_24630;
    AssignSlice(48, 57, -7);

    /** 	char_class['_']      = DIGIT*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 95);
    *(int *)_2 = -7;

    /** 	char_class['a'..'z'] = LETTER*/
    assign_slice_seq = (s1_ptr *)&_62char_class_24630;
    AssignSlice(97, 122, -2);

    /** 	char_class['A'..'Z'] = LETTER*/
    assign_slice_seq = (s1_ptr *)&_62char_class_24630;
    AssignSlice(65, 90, -2);

    /** 	char_class[KEYWORD_BASE+1..KEYWORD_BASE+NUM_KEYWORDS] = KEYWORD*/
    _14297 = 129;
    _14298 = 152;
    assign_slice_seq = (s1_ptr *)&_62char_class_24630;
    AssignSlice(129, 152, -10);
    _14297 = NOVALUE;
    _14298 = NOVALUE;

    /** 	char_class[BUILTIN_BASE+1..BUILTIN_BASE+NUM_BUILTINS] = BUILTIN*/
    _14299 = 171;
    _14300 = 234;
    assign_slice_seq = (s1_ptr *)&_62char_class_24630;
    AssignSlice(171, 234, -9);
    _14299 = NOVALUE;
    _14300 = NOVALUE;

    /** 	char_class[' '] = BLANK*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 32);
    *(int *)_2 = -8;

    /** 	char_class['\t'] = BLANK*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 9);
    *(int *)_2 = -8;

    /** 	char_class['+'] = PLUS*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 43);
    *(int *)_2 = 11;

    /** 	char_class['-'] = MINUS*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 45);
    *(int *)_2 = 10;

    /** 	char_class['*'] = res:MULTIPLY*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 42);
    *(int *)_2 = 13;

    /** 	char_class['/'] = res:DIVIDE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 47);
    *(int *)_2 = 14;

    /** 	char_class['='] = EQUALS*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 61);
    *(int *)_2 = 3;

    /** 	char_class['<'] = LESS*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 60);
    *(int *)_2 = 1;

    /** 	char_class['>'] = GREATER*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 62);
    *(int *)_2 = 6;

    /** 	char_class['\''] = SINGLE_QUOTE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 39);
    *(int *)_2 = -5;

    /** 	char_class['"'] = DOUBLE_QUOTE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 34);
    *(int *)_2 = -4;

    /** 	char_class['`'] = BACK_QUOTE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 96);
    *(int *)_2 = -12;

    /** 	char_class['.'] = DOT*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 46);
    *(int *)_2 = -3;

    /** 	char_class[':'] = COLON*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 58);
    *(int *)_2 = -23;

    /** 	char_class['\r'] = NEWLINE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 13);
    *(int *)_2 = -6;

    /** 	char_class['\n'] = NEWLINE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 10);
    *(int *)_2 = -6;

    /** 	char_class['!'] = BANG*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 33);
    *(int *)_2 = -1;

    /** 	char_class['{'] = LEFT_BRACE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 123);
    *(int *)_2 = -24;

    /** 	char_class['}'] = RIGHT_BRACE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 125);
    *(int *)_2 = -25;

    /** 	char_class['('] = LEFT_ROUND*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 40);
    *(int *)_2 = -26;

    /** 	char_class[')'] = RIGHT_ROUND*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 41);
    *(int *)_2 = -27;

    /** 	char_class['['] = LEFT_SQUARE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 91);
    *(int *)_2 = -28;

    /** 	char_class[']'] = RIGHT_SQUARE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 93);
    *(int *)_2 = -29;

    /** 	char_class['$'] = DOLLAR*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 36);
    *(int *)_2 = -22;

    /** 	char_class[','] = COMMA*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 44);
    *(int *)_2 = -30;

    /** 	char_class['&'] = res:CONCAT*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 38);
    *(int *)_2 = 15;

    /** 	char_class['?'] = QUESTION_MARK*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 63);
    *(int *)_2 = -31;

    /** 	char_class['#'] = NUMBER_SIGN*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 35);
    *(int *)_2 = -11;

    /** 	char_class[END_OF_FILE_CHAR] = END_OF_FILE*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _2 = (int)(((s1_ptr)_2)->base + 26);
    *(int *)_2 = -21;

    /** 	id_char = repeat(FALSE, 255)*/
    DeRefi(_62id_char_24631);
    _62id_char_24631 = Repeat(_9FALSE_426, 255);

    /** 	for i = 1 to 255 do*/
    {
        int _i_24709;
        _i_24709 = 1;
L1: 
        if (_i_24709 > 255){
            goto L2; // [407] 456
        }

        /** 		if find(char_class[i], {LETTER, DIGIT}) then*/
        _2 = (int)SEQ_PTR(_62char_class_24630);
        _14302 = (int)*(((s1_ptr)_2)->base + _i_24709);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = -7;
        _14303 = MAKE_SEQ(_1);
        _14304 = find_from(_14302, _14303, 1);
        _14302 = NOVALUE;
        DeRefDS(_14303);
        _14303 = NOVALUE;
        if (_14304 == 0)
        {
            _14304 = NOVALUE;
            goto L3; // [435] 449
        }
        else{
            _14304 = NOVALUE;
        }

        /** 			id_char[i] = TRUE*/
        _2 = (int)SEQ_PTR(_62id_char_24631);
        _2 = (int)(((s1_ptr)_2)->base + _i_24709);
        *(int *)_2 = _9TRUE_428;
L3: 

        /** 	end for*/
        _i_24709 = _i_24709 + 1;
        goto L1; // [451] 414
L2: 
        ;
    }

    /** 	default_namespaces = {0}*/
    _0 = _62default_namespaces_24629;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    _62default_namespaces_24629 = MAKE_SEQ(_1);
    DeRef(_0);

    /** end procedure*/
    return;
    ;
}


void _62ResetTP()
{
    int _0, _1, _2;
    

    /** 	OpTrace = FALSE*/
    _38OpTrace_17015 = _9FALSE_426;

    /** 	OpProfileStatement = FALSE*/
    _38OpProfileStatement_17017 = _9FALSE_426;

    /** 	OpProfileTime = FALSE*/
    _38OpProfileTime_17018 = _9FALSE_426;

    /** 	AnyStatementProfile = FALSE*/
    _35AnyStatementProfile_15618 = _9FALSE_426;

    /** 	AnyTimeProfile = FALSE*/
    _35AnyTimeProfile_15617 = _9FALSE_426;

    /** end procedure*/
    return;
    ;
}


int _62pack_source(int _src_24739)
{
    int _start_24740 = NOVALUE;
    int _14328 = NOVALUE;
    int _14327 = NOVALUE;
    int _14326 = NOVALUE;
    int _14325 = NOVALUE;
    int _14323 = NOVALUE;
    int _14321 = NOVALUE;
    int _14320 = NOVALUE;
    int _14319 = NOVALUE;
    int _14315 = NOVALUE;
    int _14313 = NOVALUE;
    int _14312 = NOVALUE;
    int _14309 = NOVALUE;
    int _14308 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(src, 0) then*/
    if (_src_24739 == 0)
    _14308 = 1;
    else if (IS_ATOM_INT(_src_24739) && IS_ATOM_INT(0))
    _14308 = 0;
    else
    _14308 = (compare(_src_24739, 0) == 0);
    if (_14308 == 0)
    {
        _14308 = NOVALUE;
        goto L1; // [7] 17
    }
    else{
        _14308 = NOVALUE;
    }

    /** 		return 0*/
    DeRef(_src_24739);
    return 0;
L1: 

    /** 	if length(src) >= SOURCE_CHUNK then*/
    if (IS_SEQUENCE(_src_24739)){
            _14309 = SEQ_PTR(_src_24739)->length;
    }
    else {
        _14309 = 1;
    }
    if (_14309 < 10000)
    goto L2; // [22] 34

    /** 		src = src[1..100] -- enough for trace or profile display*/
    rhs_slice_target = (object_ptr)&_src_24739;
    RHS_Slice(_src_24739, 1, 100);
L2: 

    /** 	if current_source_next + length(src) >= SOURCE_CHUNK then*/
    if (IS_SEQUENCE(_src_24739)){
            _14312 = SEQ_PTR(_src_24739)->length;
    }
    else {
        _14312 = 1;
    }
    _14313 = _62current_source_next_24735 + _14312;
    if ((long)((unsigned long)_14313 + (unsigned long)HIGH_BITS) >= 0) 
    _14313 = NewDouble((double)_14313);
    _14312 = NOVALUE;
    if (binary_op_a(LESS, _14313, 10000)){
        DeRef(_14313);
        _14313 = NOVALUE;
        goto L3; // [45] 94
    }
    DeRef(_14313);
    _14313 = NOVALUE;

    /** 		current_source = allocate(SOURCE_CHUNK + LINE_BUFLEN)*/
    _14315 = 10400;
    _0 = _4allocate(10400, 0);
    DeRef(_62current_source_24734);
    _62current_source_24734 = _0;
    _14315 = NOVALUE;

    /** 		if current_source = 0 then*/
    if (binary_op_a(NOTEQ, _62current_source_24734, 0)){
        goto L4; // [64] 76
    }

    /** 			CompileErr(123)*/
    RefDS(_22663);
    _46CompileErr(123, _22663, 0);
L4: 

    /** 		all_source = append(all_source, current_source)*/
    Ref(_62current_source_24734);
    Append(&_35all_source_15619, _35all_source_15619, _62current_source_24734);

    /** 		current_source_next = 1*/
    _62current_source_next_24735 = 1;
L3: 

    /** 	start = current_source_next*/
    _start_24740 = _62current_source_next_24735;

    /** 	poke(current_source+current_source_next, src)*/
    if (IS_ATOM_INT(_62current_source_24734)) {
        _14319 = _62current_source_24734 + _62current_source_next_24735;
        if ((long)((unsigned long)_14319 + (unsigned long)HIGH_BITS) >= 0) 
        _14319 = NewDouble((double)_14319);
    }
    else {
        _14319 = NewDouble(DBL_PTR(_62current_source_24734)->dbl + (double)_62current_source_next_24735);
    }
    if (IS_ATOM_INT(_14319)){
        poke_addr = (unsigned char *)_14319;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_14319)->dbl);
    }
    if (IS_ATOM_INT(_src_24739)) {
        *poke_addr = (unsigned char)_src_24739;
    }
    else if (IS_ATOM(_src_24739)) {
        _1 = (signed char)DBL_PTR(_src_24739)->dbl;
        *poke_addr = _1;
    }
    else {
        _1 = (int)SEQ_PTR(_src_24739);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
    }
    DeRef(_14319);
    _14319 = NOVALUE;

    /** 	current_source_next += length(src)-1*/
    if (IS_SEQUENCE(_src_24739)){
            _14320 = SEQ_PTR(_src_24739)->length;
    }
    else {
        _14320 = 1;
    }
    _14321 = _14320 - 1;
    _14320 = NOVALUE;
    _62current_source_next_24735 = _62current_source_next_24735 + _14321;
    _14321 = NOVALUE;

    /** 	poke(current_source+current_source_next, 0) -- overwrite \n*/
    if (IS_ATOM_INT(_62current_source_24734)) {
        _14323 = _62current_source_24734 + _62current_source_next_24735;
        if ((long)((unsigned long)_14323 + (unsigned long)HIGH_BITS) >= 0) 
        _14323 = NewDouble((double)_14323);
    }
    else {
        _14323 = NewDouble(DBL_PTR(_62current_source_24734)->dbl + (double)_62current_source_next_24735);
    }
    if (IS_ATOM_INT(_14323)){
        poke_addr = (unsigned char *)_14323;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_14323)->dbl);
    }
    *poke_addr = (unsigned char)0;
    DeRef(_14323);
    _14323 = NOVALUE;

    /** 	current_source_next += 1*/
    _62current_source_next_24735 = _62current_source_next_24735 + 1;

    /** 	return start + SOURCE_CHUNK * (length(all_source)-1)*/
    if (IS_SEQUENCE(_35all_source_15619)){
            _14325 = SEQ_PTR(_35all_source_15619)->length;
    }
    else {
        _14325 = 1;
    }
    _14326 = _14325 - 1;
    _14325 = NOVALUE;
    if (_14326 <= INT15)
    _14327 = 10000 * _14326;
    else
    _14327 = NewDouble(10000 * (double)_14326);
    _14326 = NOVALUE;
    if (IS_ATOM_INT(_14327)) {
        _14328 = _start_24740 + _14327;
        if ((long)((unsigned long)_14328 + (unsigned long)HIGH_BITS) >= 0) 
        _14328 = NewDouble((double)_14328);
    }
    else {
        _14328 = NewDouble((double)_start_24740 + DBL_PTR(_14327)->dbl);
    }
    DeRef(_14327);
    _14327 = NOVALUE;
    DeRef(_src_24739);
    return _14328;
    ;
}


int _62fetch_line(int _start_24773)
{
    int _line_24774 = NOVALUE;
    int _memdata_24775 = NOVALUE;
    int _c_24776 = NOVALUE;
    int _chunk_24777 = NOVALUE;
    int _p_24778 = NOVALUE;
    int _n_24779 = NOVALUE;
    int _m_24780 = NOVALUE;
    int _14353 = NOVALUE;
    int _14352 = NOVALUE;
    int _14350 = NOVALUE;
    int _14348 = NOVALUE;
    int _14342 = NOVALUE;
    int _14340 = NOVALUE;
    int _14336 = NOVALUE;
    int _14334 = NOVALUE;
    int _14331 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_24773)) {
        _1 = (long)(DBL_PTR(_start_24773)->dbl);
        if (UNIQUE(DBL_PTR(_start_24773)) && (DBL_PTR(_start_24773)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_24773);
        _start_24773 = _1;
    }

    /** 	if start = 0 then*/
    if (_start_24773 != 0)
    goto L1; // [5] 16

    /** 		return ""*/
    RefDS(_5);
    DeRef(_line_24774);
    DeRefi(_memdata_24775);
    DeRef(_p_24778);
    return _5;
L1: 

    /** 	line = repeat(0, LINE_BUFLEN)*/
    DeRef(_line_24774);
    _line_24774 = Repeat(0, 400);

    /** 	n = 0*/
    _n_24779 = 0;

    /** 	chunk = 1+floor(start / SOURCE_CHUNK)*/
    if (10000 > 0 && _start_24773 >= 0) {
        _14331 = _start_24773 / 10000;
    }
    else {
        temp_dbl = floor((double)_start_24773 / (double)10000);
        _14331 = (long)temp_dbl;
    }
    _chunk_24777 = _14331 + 1;
    _14331 = NOVALUE;

    /** 	start = remainder(start, SOURCE_CHUNK)*/
    _start_24773 = (_start_24773 % 10000);

    /** 	p = all_source[chunk] + start*/
    _2 = (int)SEQ_PTR(_35all_source_15619);
    _14334 = (int)*(((s1_ptr)_2)->base + _chunk_24777);
    DeRef(_p_24778);
    if (IS_ATOM_INT(_14334)) {
        _p_24778 = _14334 + _start_24773;
        if ((long)((unsigned long)_p_24778 + (unsigned long)HIGH_BITS) >= 0) 
        _p_24778 = NewDouble((double)_p_24778);
    }
    else {
        _p_24778 = NewDouble(DBL_PTR(_14334)->dbl + (double)_start_24773);
    }
    _14334 = NOVALUE;

    /** 	memdata = peek({p, LINE_BUFLEN})*/
    Ref(_p_24778);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _p_24778;
    ((int *)_2)[2] = 400;
    _14336 = MAKE_SEQ(_1);
    DeRefi(_memdata_24775);
    _1 = (int)SEQ_PTR(_14336);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _memdata_24775 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_14336);
    _14336 = NOVALUE;

    /** 	p += LINE_BUFLEN*/
    _0 = _p_24778;
    if (IS_ATOM_INT(_p_24778)) {
        _p_24778 = _p_24778 + 400;
        if ((long)((unsigned long)_p_24778 + (unsigned long)HIGH_BITS) >= 0) 
        _p_24778 = NewDouble((double)_p_24778);
    }
    else {
        _p_24778 = NewDouble(DBL_PTR(_p_24778)->dbl + (double)400);
    }
    DeRef(_0);

    /** 	m = 0*/
    _m_24780 = 0;

    /** 	while TRUE do*/
L2: 
    if (_9TRUE_428 == 0)
    {
        goto L3; // [84] 179
    }
    else{
    }

    /** 		m += 1*/
    _m_24780 = _m_24780 + 1;

    /** 		if m > length(memdata) then*/
    if (IS_SEQUENCE(_memdata_24775)){
            _14340 = SEQ_PTR(_memdata_24775)->length;
    }
    else {
        _14340 = 1;
    }
    if (_m_24780 <= _14340)
    goto L4; // [98] 125

    /** 			memdata = peek({p, LINE_BUFLEN})*/
    Ref(_p_24778);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _p_24778;
    ((int *)_2)[2] = 400;
    _14342 = MAKE_SEQ(_1);
    DeRefDSi(_memdata_24775);
    _1 = (int)SEQ_PTR(_14342);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _memdata_24775 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    DeRefDS(_14342);
    _14342 = NOVALUE;

    /** 			p += LINE_BUFLEN*/
    _0 = _p_24778;
    if (IS_ATOM_INT(_p_24778)) {
        _p_24778 = _p_24778 + 400;
        if ((long)((unsigned long)_p_24778 + (unsigned long)HIGH_BITS) >= 0) 
        _p_24778 = NewDouble((double)_p_24778);
    }
    else {
        _p_24778 = NewDouble(DBL_PTR(_p_24778)->dbl + (double)400);
    }
    DeRef(_0);

    /** 			m = 1*/
    _m_24780 = 1;
L4: 

    /** 		c = memdata[m]*/
    _2 = (int)SEQ_PTR(_memdata_24775);
    _c_24776 = (int)*(((s1_ptr)_2)->base + _m_24780);

    /** 		if c = 0 then*/
    if (_c_24776 != 0)
    goto L5; // [133] 142

    /** 			exit*/
    goto L3; // [139] 179
L5: 

    /** 		n += 1*/
    _n_24779 = _n_24779 + 1;

    /** 		if n > length(line) then*/
    if (IS_SEQUENCE(_line_24774)){
            _14348 = SEQ_PTR(_line_24774)->length;
    }
    else {
        _14348 = 1;
    }
    if (_n_24779 <= _14348)
    goto L6; // [153] 168

    /** 			line &= repeat(0, LINE_BUFLEN)*/
    _14350 = Repeat(0, 400);
    Concat((object_ptr)&_line_24774, _line_24774, _14350);
    DeRefDS(_14350);
    _14350 = NOVALUE;
L6: 

    /** 		line[n] = c*/
    _2 = (int)SEQ_PTR(_line_24774);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _line_24774 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _n_24779);
    _1 = *(int *)_2;
    *(int *)_2 = _c_24776;
    DeRef(_1);

    /** 	end while*/
    goto L2; // [176] 82
L3: 

    /** 	line = remove( line, n+1, length( line ) )*/
    _14352 = _n_24779 + 1;
    if (_14352 > MAXINT){
        _14352 = NewDouble((double)_14352);
    }
    if (IS_SEQUENCE(_line_24774)){
            _14353 = SEQ_PTR(_line_24774)->length;
    }
    else {
        _14353 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_line_24774);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_14352)) ? _14352 : (long)(DBL_PTR(_14352)->dbl);
        int stop = (IS_ATOM_INT(_14353)) ? _14353 : (long)(DBL_PTR(_14353)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_line_24774), start, &_line_24774 );
            }
            else Tail(SEQ_PTR(_line_24774), stop+1, &_line_24774);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_line_24774), start, &_line_24774);
        }
        else {
            assign_slice_seq = &assign_space;
            _line_24774 = Remove_elements(start, stop, (SEQ_PTR(_line_24774)->ref == 1));
        }
    }
    DeRef(_14352);
    _14352 = NOVALUE;
    _14353 = NOVALUE;

    /** 	return line*/
    DeRefi(_memdata_24775);
    DeRef(_p_24778);
    return _line_24774;
    ;
}


void _62AppendSourceLine()
{
    int _new_24816 = NOVALUE;
    int _old_24817 = NOVALUE;
    int _options_24818 = NOVALUE;
    int _src_24819 = NOVALUE;
    int _14394 = NOVALUE;
    int _14390 = NOVALUE;
    int _14388 = NOVALUE;
    int _14387 = NOVALUE;
    int _14384 = NOVALUE;
    int _14383 = NOVALUE;
    int _14382 = NOVALUE;
    int _14381 = NOVALUE;
    int _14380 = NOVALUE;
    int _14379 = NOVALUE;
    int _14378 = NOVALUE;
    int _14377 = NOVALUE;
    int _14376 = NOVALUE;
    int _14375 = NOVALUE;
    int _14374 = NOVALUE;
    int _14373 = NOVALUE;
    int _14372 = NOVALUE;
    int _14371 = NOVALUE;
    int _14370 = NOVALUE;
    int _14369 = NOVALUE;
    int _14368 = NOVALUE;
    int _14367 = NOVALUE;
    int _14365 = NOVALUE;
    int _14364 = NOVALUE;
    int _14363 = NOVALUE;
    int _14361 = NOVALUE;
    int _14356 = NOVALUE;
    int _14355 = NOVALUE;
    int _0, _1, _2;
    

    /** 	src = 0*/
    DeRef(_src_24819);
    _src_24819 = 0;

    /** 	options = 0*/
    _options_24818 = 0;

    /** 	if TRANSLATE or OpTrace or OpProfileStatement or OpProfileTime then*/
    if (_38TRANSLATE_16564 != 0) {
        _14355 = 1;
        goto L1; // [15] 25
    }
    _14355 = (_38OpTrace_17015 != 0);
L1: 
    if (_14355 != 0) {
        _14356 = 1;
        goto L2; // [25] 35
    }
    _14356 = (_38OpProfileStatement_17017 != 0);
L2: 
    if (_14356 != 0) {
        goto L3; // [35] 46
    }
    if (_38OpProfileTime_17018 == 0)
    {
        goto L4; // [42] 136
    }
    else{
    }
L3: 

    /** 		src = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_src_24819);
    _src_24819 = _46ThisLine_49482;

    /** 		if OpTrace then*/
    if (_38OpTrace_17015 == 0)
    {
        goto L5; // [57] 70
    }
    else{
    }

    /** 			options = SOP_TRACE*/
    _options_24818 = 1;
L5: 

    /** 		if OpProfileTime then*/
    if (_38OpProfileTime_17018 == 0)
    {
        goto L6; // [74] 88
    }
    else{
    }

    /** 			options = or_bits(options, SOP_PROFILE_TIME)*/
    {unsigned long tu;
         tu = (unsigned long)_options_24818 | (unsigned long)2;
         _options_24818 = MAKE_UINT(tu);
    }
    if (!IS_ATOM_INT(_options_24818)) {
        _1 = (long)(DBL_PTR(_options_24818)->dbl);
        if (UNIQUE(DBL_PTR(_options_24818)) && (DBL_PTR(_options_24818)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_options_24818);
        _options_24818 = _1;
    }
L6: 

    /** 		if OpProfileStatement then*/
    if (_38OpProfileStatement_17017 == 0)
    {
        goto L7; // [92] 106
    }
    else{
    }

    /** 			options = or_bits(options, SOP_PROFILE_STATEMENT)*/
    {unsigned long tu;
         tu = (unsigned long)_options_24818 | (unsigned long)4;
         _options_24818 = MAKE_UINT(tu);
    }
    if (!IS_ATOM_INT(_options_24818)) {
        _1 = (long)(DBL_PTR(_options_24818)->dbl);
        if (UNIQUE(DBL_PTR(_options_24818)) && (DBL_PTR(_options_24818)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_options_24818);
        _options_24818 = _1;
    }
L7: 

    /** 		if OpProfileStatement or OpProfileTime then*/
    if (_38OpProfileStatement_17017 != 0) {
        goto L8; // [110] 121
    }
    if (_38OpProfileTime_17018 == 0)
    {
        goto L9; // [117] 135
    }
    else{
    }
L8: 

    /** 			src = {0,0,0,0} & src*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    _14361 = MAKE_SEQ(_1);
    if (IS_SEQUENCE(_14361) && IS_ATOM(_src_24819)) {
        Ref(_src_24819);
        Append(&_src_24819, _14361, _src_24819);
    }
    else if (IS_ATOM(_14361) && IS_SEQUENCE(_src_24819)) {
    }
    else {
        Concat((object_ptr)&_src_24819, _14361, _src_24819);
        DeRefDS(_14361);
        _14361 = NOVALUE;
    }
    DeRef(_14361);
    _14361 = NOVALUE;
L9: 
L4: 

    /** 	if length(slist) then*/
    if (IS_SEQUENCE(_38slist_17040)){
            _14363 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _14363 = 1;
    }
    if (_14363 == 0)
    {
        _14363 = NOVALUE;
        goto LA; // [143] 345
    }
    else{
        _14363 = NOVALUE;
    }

    /** 		old = slist[$-1]*/
    if (IS_SEQUENCE(_38slist_17040)){
            _14364 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _14364 = 1;
    }
    _14365 = _14364 - 1;
    _14364 = NOVALUE;
    DeRef(_old_24817);
    _2 = (int)SEQ_PTR(_38slist_17040);
    _old_24817 = (int)*(((s1_ptr)_2)->base + _14365);
    Ref(_old_24817);

    /** 		if equal(src, old[SRC]) and*/
    _2 = (int)SEQ_PTR(_old_24817);
    _14367 = (int)*(((s1_ptr)_2)->base + 1);
    if (_src_24819 == _14367)
    _14368 = 1;
    else if (IS_ATOM_INT(_src_24819) && IS_ATOM_INT(_14367))
    _14368 = 0;
    else
    _14368 = (compare(_src_24819, _14367) == 0);
    _14367 = NOVALUE;
    if (_14368 == 0) {
        _14369 = 0;
        goto LB; // [175] 195
    }
    _2 = (int)SEQ_PTR(_old_24817);
    _14370 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_14370)) {
        _14371 = (_38current_file_no_16946 == _14370);
    }
    else {
        _14371 = binary_op(EQUALS, _38current_file_no_16946, _14370);
    }
    _14370 = NOVALUE;
    if (IS_ATOM_INT(_14371))
    _14369 = (_14371 != 0);
    else
    _14369 = DBL_PTR(_14371)->dbl != 0.0;
LB: 
    if (_14369 == 0) {
        _14372 = 0;
        goto LC; // [195] 232
    }
    _2 = (int)SEQ_PTR(_old_24817);
    _14373 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_14373)) {
        _14374 = _14373 + 1;
        if (_14374 > MAXINT){
            _14374 = NewDouble((double)_14374);
        }
    }
    else
    _14374 = binary_op(PLUS, 1, _14373);
    _14373 = NOVALUE;
    if (IS_SEQUENCE(_38slist_17040)){
            _14375 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _14375 = 1;
    }
    _2 = (int)SEQ_PTR(_38slist_17040);
    _14376 = (int)*(((s1_ptr)_2)->base + _14375);
    if (IS_ATOM_INT(_14374) && IS_ATOM_INT(_14376)) {
        _14377 = _14374 + _14376;
        if ((long)((unsigned long)_14377 + (unsigned long)HIGH_BITS) >= 0) 
        _14377 = NewDouble((double)_14377);
    }
    else {
        _14377 = binary_op(PLUS, _14374, _14376);
    }
    DeRef(_14374);
    _14374 = NOVALUE;
    _14376 = NOVALUE;
    if (IS_ATOM_INT(_14377)) {
        _14378 = (_38line_number_16947 == _14377);
    }
    else {
        _14378 = binary_op(EQUALS, _38line_number_16947, _14377);
    }
    DeRef(_14377);
    _14377 = NOVALUE;
    if (IS_ATOM_INT(_14378))
    _14372 = (_14378 != 0);
    else
    _14372 = DBL_PTR(_14378)->dbl != 0.0;
LC: 
    if (_14372 == 0) {
        goto LD; // [232] 272
    }
    _2 = (int)SEQ_PTR(_old_24817);
    _14380 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_14380)) {
        _14381 = (_options_24818 == _14380);
    }
    else {
        _14381 = binary_op(EQUALS, _options_24818, _14380);
    }
    _14380 = NOVALUE;
    if (_14381 == 0) {
        DeRef(_14381);
        _14381 = NOVALUE;
        goto LD; // [247] 272
    }
    else {
        if (!IS_ATOM_INT(_14381) && DBL_PTR(_14381)->dbl == 0.0){
            DeRef(_14381);
            _14381 = NOVALUE;
            goto LD; // [247] 272
        }
        DeRef(_14381);
        _14381 = NOVALUE;
    }
    DeRef(_14381);
    _14381 = NOVALUE;

    /** 			slist[$] += 1*/
    if (IS_SEQUENCE(_38slist_17040)){
            _14382 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _14382 = 1;
    }
    _2 = (int)SEQ_PTR(_38slist_17040);
    _14383 = (int)*(((s1_ptr)_2)->base + _14382);
    if (IS_ATOM_INT(_14383)) {
        _14384 = _14383 + 1;
        if (_14384 > MAXINT){
            _14384 = NewDouble((double)_14384);
        }
    }
    else
    _14384 = binary_op(PLUS, 1, _14383);
    _14383 = NOVALUE;
    _2 = (int)SEQ_PTR(_38slist_17040);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38slist_17040 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14382);
    _1 = *(int *)_2;
    *(int *)_2 = _14384;
    if( _1 != _14384 ){
        DeRef(_1);
    }
    _14384 = NOVALUE;
    goto LE; // [269] 371
LD: 

    /** 			src = pack_source(src)*/
    Ref(_src_24819);
    _0 = _src_24819;
    _src_24819 = _62pack_source(_src_24819);
    DeRef(_0);

    /** 			new = {src, line_number, current_file_no, options}*/
    _0 = _new_24816;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_src_24819);
    *((int *)(_2+4)) = _src_24819;
    *((int *)(_2+8)) = _38line_number_16947;
    *((int *)(_2+12)) = _38current_file_no_16946;
    *((int *)(_2+16)) = _options_24818;
    _new_24816 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 			if slist[$] = 0 then*/
    if (IS_SEQUENCE(_38slist_17040)){
            _14387 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _14387 = 1;
    }
    _2 = (int)SEQ_PTR(_38slist_17040);
    _14388 = (int)*(((s1_ptr)_2)->base + _14387);
    if (binary_op_a(NOTEQ, _14388, 0)){
        _14388 = NOVALUE;
        goto LF; // [302] 320
    }
    _14388 = NOVALUE;

    /** 				slist[$] = new*/
    if (IS_SEQUENCE(_38slist_17040)){
            _14390 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _14390 = 1;
    }
    RefDS(_new_24816);
    _2 = (int)SEQ_PTR(_38slist_17040);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38slist_17040 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14390);
    _1 = *(int *)_2;
    *(int *)_2 = _new_24816;
    DeRef(_1);
    goto L10; // [317] 331
LF: 

    /** 				slist = append(slist, new)*/
    RefDS(_new_24816);
    Append(&_38slist_17040, _38slist_17040, _new_24816);
L10: 

    /** 			slist = append(slist, 0)*/
    Append(&_38slist_17040, _38slist_17040, 0);
    goto LE; // [342] 371
LA: 

    /** 		src = pack_source(src)*/
    Ref(_src_24819);
    _0 = _src_24819;
    _src_24819 = _62pack_source(_src_24819);
    DeRef(_0);

    /** 		slist = {{src, line_number, current_file_no, options}, 0}*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_src_24819);
    *((int *)(_2+4)) = _src_24819;
    *((int *)(_2+8)) = _38line_number_16947;
    *((int *)(_2+12)) = _38current_file_no_16946;
    *((int *)(_2+16)) = _options_24818;
    _14394 = MAKE_SEQ(_1);
    DeRef(_38slist_17040);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _14394;
    ((int *)_2)[2] = 0;
    _38slist_17040 = MAKE_SEQ(_1);
    _14394 = NOVALUE;
LE: 

    /** end procedure*/
    DeRef(_new_24816);
    DeRef(_old_24817);
    DeRef(_src_24819);
    DeRef(_14365);
    _14365 = NOVALUE;
    DeRef(_14378);
    _14378 = NOVALUE;
    DeRef(_14371);
    _14371 = NOVALUE;
    return;
    ;
}


int _62s_expand(int _slist_24908)
{
    int _new_slist_24909 = NOVALUE;
    int _14408 = NOVALUE;
    int _14407 = NOVALUE;
    int _14406 = NOVALUE;
    int _14405 = NOVALUE;
    int _14403 = NOVALUE;
    int _14402 = NOVALUE;
    int _14401 = NOVALUE;
    int _14399 = NOVALUE;
    int _14398 = NOVALUE;
    int _14397 = NOVALUE;
    int _14396 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	new_slist = {}*/
    RefDS(_5);
    DeRef(_new_slist_24909);
    _new_slist_24909 = _5;

    /** 	for i = 1 to length(slist) do*/
    if (IS_SEQUENCE(_slist_24908)){
            _14396 = SEQ_PTR(_slist_24908)->length;
    }
    else {
        _14396 = 1;
    }
    {
        int _i_24911;
        _i_24911 = 1;
L1: 
        if (_i_24911 > _14396){
            goto L2; // [15] 114
        }

        /** 		if sequence(slist[i]) then*/
        _2 = (int)SEQ_PTR(_slist_24908);
        _14397 = (int)*(((s1_ptr)_2)->base + _i_24911);
        _14398 = IS_SEQUENCE(_14397);
        _14397 = NOVALUE;
        if (_14398 == 0)
        {
            _14398 = NOVALUE;
            goto L3; // [31] 47
        }
        else{
            _14398 = NOVALUE;
        }

        /** 			new_slist = append(new_slist, slist[i])*/
        _2 = (int)SEQ_PTR(_slist_24908);
        _14399 = (int)*(((s1_ptr)_2)->base + _i_24911);
        Ref(_14399);
        Append(&_new_slist_24909, _new_slist_24909, _14399);
        _14399 = NOVALUE;
        goto L4; // [44] 107
L3: 

        /** 			for j = 1 to slist[i] do*/
        _2 = (int)SEQ_PTR(_slist_24908);
        _14401 = (int)*(((s1_ptr)_2)->base + _i_24911);
        {
            int _j_24920;
            _j_24920 = 1;
L5: 
            if (binary_op_a(GREATER, _j_24920, _14401)){
                goto L6; // [53] 106
            }

            /** 				slist[i-1][LINE] += 1*/
            _14402 = _i_24911 - 1;
            _2 = (int)SEQ_PTR(_slist_24908);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _slist_24908 = MAKE_SEQ(_2);
            }
            _3 = (int)(_14402 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(*(int *)_3);
            _14405 = (int)*(((s1_ptr)_2)->base + 2);
            _14403 = NOVALUE;
            if (IS_ATOM_INT(_14405)) {
                _14406 = _14405 + 1;
                if (_14406 > MAXINT){
                    _14406 = NewDouble((double)_14406);
                }
            }
            else
            _14406 = binary_op(PLUS, 1, _14405);
            _14405 = NOVALUE;
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _14406;
            if( _1 != _14406 ){
                DeRef(_1);
            }
            _14406 = NOVALUE;
            _14403 = NOVALUE;

            /** 				new_slist = append(new_slist, slist[i-1])*/
            _14407 = _i_24911 - 1;
            _2 = (int)SEQ_PTR(_slist_24908);
            _14408 = (int)*(((s1_ptr)_2)->base + _14407);
            Ref(_14408);
            Append(&_new_slist_24909, _new_slist_24909, _14408);
            _14408 = NOVALUE;

            /** 			end for*/
            _0 = _j_24920;
            if (IS_ATOM_INT(_j_24920)) {
                _j_24920 = _j_24920 + 1;
                if ((long)((unsigned long)_j_24920 +(unsigned long) HIGH_BITS) >= 0){
                    _j_24920 = NewDouble((double)_j_24920);
                }
            }
            else {
                _j_24920 = binary_op_a(PLUS, _j_24920, 1);
            }
            DeRef(_0);
            goto L5; // [101] 60
L6: 
            ;
            DeRef(_j_24920);
        }
L4: 

        /** 	end for*/
        _i_24911 = _i_24911 + 1;
        goto L1; // [109] 22
L2: 
        ;
    }

    /** 	return new_slist*/
    DeRefDS(_slist_24908);
    _14401 = NOVALUE;
    DeRef(_14402);
    _14402 = NOVALUE;
    DeRef(_14407);
    _14407 = NOVALUE;
    return _new_slist_24909;
    ;
}


void _62set_dont_read(int _read_24935)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_read_24935)) {
        _1 = (long)(DBL_PTR(_read_24935)->dbl);
        if (UNIQUE(DBL_PTR(_read_24935)) && (DBL_PTR(_read_24935)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_read_24935);
        _read_24935 = _1;
    }

    /** 	dont_read = read*/
    _62dont_read_24932 = _read_24935;

    /** end procedure*/
    return;
    ;
}


void _62read_line()
{
    int _n_24938 = NOVALUE;
    int _14422 = NOVALUE;
    int _14421 = NOVALUE;
    int _14419 = NOVALUE;
    int _14418 = NOVALUE;
    int _14416 = NOVALUE;
    int _14415 = NOVALUE;
    int _0, _1, _2;
    

    /** 	line_number += 1*/
    _38line_number_16947 = _38line_number_16947 + 1;

    /** 	gline_number += 1*/
    _38gline_number_16951 = _38gline_number_16951 + 1;

    /** 	if dont_read then*/
    if (_62dont_read_24932 == 0)
    {
        goto L1; // [25] 36
    }
    else{
    }

    /** 		ThisLine = -1*/
    DeRef(_46ThisLine_49482);
    _46ThisLine_49482 = -1;
    goto L2; // [33] 108
L1: 

    /** 	elsif src_file < 0 then*/
    if (_38src_file_17087 >= 0)
    goto L3; // [40] 52

    /** 		ThisLine = -1*/
    DeRef(_46ThisLine_49482);
    _46ThisLine_49482 = -1;
    goto L2; // [49] 108
L3: 

    /** 		ThisLine = gets(src_file)*/
    DeRef(_46ThisLine_49482);
    _46ThisLine_49482 = EGets(_38src_file_17087);

    /** 		if sequence(ThisLine) and ends( {13,10}, ThisLine ) then*/
    _14415 = IS_SEQUENCE(_46ThisLine_49482);
    if (_14415 == 0) {
        goto L4; // [66] 107
    }
    RefDS(_14417);
    Ref(_46ThisLine_49482);
    _14418 = _12ends(_14417, _46ThisLine_49482);
    if (_14418 == 0) {
        DeRef(_14418);
        _14418 = NOVALUE;
        goto L4; // [78] 107
    }
    else {
        if (!IS_ATOM_INT(_14418) && DBL_PTR(_14418)->dbl == 0.0){
            DeRef(_14418);
            _14418 = NOVALUE;
            goto L4; // [78] 107
        }
        DeRef(_14418);
        _14418 = NOVALUE;
    }
    DeRef(_14418);
    _14418 = NOVALUE;

    /** 			ThisLine = remove(ThisLine, length(ThisLine))*/
    if (IS_SEQUENCE(_46ThisLine_49482)){
            _14419 = SEQ_PTR(_46ThisLine_49482)->length;
    }
    else {
        _14419 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_46ThisLine_49482);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_14419)) ? _14419 : (long)(DBL_PTR(_14419)->dbl);
        int stop = (IS_ATOM_INT(_14419)) ? _14419 : (long)(DBL_PTR(_14419)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_46ThisLine_49482), start, &_46ThisLine_49482 );
            }
            else Tail(SEQ_PTR(_46ThisLine_49482), stop+1, &_46ThisLine_49482);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_46ThisLine_49482), start, &_46ThisLine_49482);
        }
        else {
            assign_slice_seq = &assign_space;
            _46ThisLine_49482 = Remove_elements(start, stop, (SEQ_PTR(_46ThisLine_49482)->ref == 1));
        }
    }
    _14419 = NOVALUE;
    _14419 = NOVALUE;

    /** 			ThisLine[$] = 10*/
    if (IS_SEQUENCE(_46ThisLine_49482)){
            _14421 = SEQ_PTR(_46ThisLine_49482)->length;
    }
    else {
        _14421 = 1;
    }
    _2 = (int)SEQ_PTR(_46ThisLine_49482);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _46ThisLine_49482 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14421);
    _1 = *(int *)_2;
    *(int *)_2 = 10;
    DeRef(_1);
L4: 
L2: 

    /** 	if atom(ThisLine) then*/
    _14422 = IS_ATOM(_46ThisLine_49482);
    if (_14422 == 0)
    {
        _14422 = NOVALUE;
        goto L5; // [115] 149
    }
    else{
        _14422 = NOVALUE;
    }

    /** 		ThisLine = {END_OF_FILE_CHAR}*/
    _0 = _46ThisLine_49482;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 26;
    _46ThisLine_49482 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 		if src_file >= 0 then*/
    if (_38src_file_17087 < 0)
    goto L6; // [130] 141

    /** 			close(src_file)*/
    EClose(_38src_file_17087);
L6: 

    /** 		src_file = -1*/
    _38src_file_17087 = -1;
L5: 

    /** 	bp = 1*/
    _46bp_49486 = 1;

    /** 	AppendSourceLine()*/
    _62AppendSourceLine();

    /** end procedure*/
    return;
    ;
}


int _62getch()
{
    int _c_24982 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c = ThisLine[bp]*/
    _2 = (int)SEQ_PTR(_46ThisLine_49482);
    _c_24982 = (int)*(((s1_ptr)_2)->base + _46bp_49486);
    if (!IS_ATOM_INT(_c_24982)){
        _c_24982 = (long)DBL_PTR(_c_24982)->dbl;
    }

    /** 	bp += 1*/
    _46bp_49486 = _46bp_49486 + 1;

    /** 	return c*/
    return _c_24982;
    ;
}


void _62ungetch()
{
    int _0, _1, _2;
    

    /** 	bp -= 1*/
    _46bp_49486 = _46bp_49486 - 1;

    /** end procedure*/
    return;
    ;
}


int _62get_file_path(int _s_24994)
{
    int _14433 = NOVALUE;
    int _14431 = NOVALUE;
    int _14430 = NOVALUE;
    int _14429 = NOVALUE;
    int _14428 = NOVALUE;
    int _0, _1, _2;
    

    /** 		for t=length(s) to 1 by -1 do*/
    if (IS_SEQUENCE(_s_24994)){
            _14428 = SEQ_PTR(_s_24994)->length;
    }
    else {
        _14428 = 1;
    }
    {
        int _t_24996;
        _t_24996 = _14428;
L1: 
        if (_t_24996 < 1){
            goto L2; // [8] 50
        }

        /** 				if find(s[t],SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_s_24994);
        _14429 = (int)*(((s1_ptr)_2)->base + _t_24996);
        _14430 = find_from(_14429, _42SLASH_CHARS_17124, 1);
        _14429 = NOVALUE;
        if (_14430 == 0)
        {
            _14430 = NOVALUE;
            goto L3; // [28] 43
        }
        else{
            _14430 = NOVALUE;
        }

        /** 						return s[1..t]*/
        rhs_slice_target = (object_ptr)&_14431;
        RHS_Slice(_s_24994, 1, _t_24996);
        DeRefDS(_s_24994);
        return _14431;
L3: 

        /** 		end for*/
        _t_24996 = _t_24996 + -1;
        goto L1; // [45] 15
L2: 
        ;
    }

    /** 		return "." & SLASH*/
    Append(&_14433, _14432, 92);
    DeRefDS(_s_24994);
    DeRef(_14431);
    _14431 = NOVALUE;
    return _14433;
    ;
}


int _62find_file(int _fname_25008)
{
    int _try_25009 = NOVALUE;
    int _full_path_25010 = NOVALUE;
    int _errbuff_25011 = NOVALUE;
    int _currdir_25012 = NOVALUE;
    int _conf_path_25013 = NOVALUE;
    int _scan_result_25014 = NOVALUE;
    int _inc_path_25015 = NOVALUE;
    int _mainpath_25034 = NOVALUE;
    int _32386 = NOVALUE;
    int _32385 = NOVALUE;
    int _14530 = NOVALUE;
    int _14528 = NOVALUE;
    int _14527 = NOVALUE;
    int _14526 = NOVALUE;
    int _14524 = NOVALUE;
    int _14522 = NOVALUE;
    int _14520 = NOVALUE;
    int _14519 = NOVALUE;
    int _14517 = NOVALUE;
    int _14516 = NOVALUE;
    int _14513 = NOVALUE;
    int _14510 = NOVALUE;
    int _14509 = NOVALUE;
    int _14508 = NOVALUE;
    int _14507 = NOVALUE;
    int _14506 = NOVALUE;
    int _14505 = NOVALUE;
    int _14504 = NOVALUE;
    int _14503 = NOVALUE;
    int _14500 = NOVALUE;
    int _14499 = NOVALUE;
    int _14495 = NOVALUE;
    int _14492 = NOVALUE;
    int _14491 = NOVALUE;
    int _14490 = NOVALUE;
    int _14489 = NOVALUE;
    int _14488 = NOVALUE;
    int _14487 = NOVALUE;
    int _14486 = NOVALUE;
    int _14485 = NOVALUE;
    int _14482 = NOVALUE;
    int _14478 = NOVALUE;
    int _14476 = NOVALUE;
    int _14475 = NOVALUE;
    int _14474 = NOVALUE;
    int _14473 = NOVALUE;
    int _14472 = NOVALUE;
    int _14469 = NOVALUE;
    int _14468 = NOVALUE;
    int _14467 = NOVALUE;
    int _14466 = NOVALUE;
    int _14465 = NOVALUE;
    int _14464 = NOVALUE;
    int _14462 = NOVALUE;
    int _14461 = NOVALUE;
    int _14460 = NOVALUE;
    int _14459 = NOVALUE;
    int _14458 = NOVALUE;
    int _14456 = NOVALUE;
    int _14455 = NOVALUE;
    int _14452 = NOVALUE;
    int _14449 = NOVALUE;
    int _14447 = NOVALUE;
    int _14444 = NOVALUE;
    int _14442 = NOVALUE;
    int _14441 = NOVALUE;
    int _14438 = NOVALUE;
    int _14437 = NOVALUE;
    int _14435 = NOVALUE;
    int _14434 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if absolute_path(fname) then*/
    RefDS(_fname_25008);
    _14434 = _13absolute_path(_fname_25008);
    if (_14434 == 0) {
        DeRef(_14434);
        _14434 = NOVALUE;
        goto L1; // [9] 42
    }
    else {
        if (!IS_ATOM_INT(_14434) && DBL_PTR(_14434)->dbl == 0.0){
            DeRef(_14434);
            _14434 = NOVALUE;
            goto L1; // [9] 42
        }
        DeRef(_14434);
        _14434 = NOVALUE;
    }
    DeRef(_14434);
    _14434 = NOVALUE;

    /** 		if not file_exists(fname) then*/
    RefDS(_fname_25008);
    _14435 = _13file_exists(_fname_25008);
    if (IS_ATOM_INT(_14435)) {
        if (_14435 != 0){
            DeRef(_14435);
            _14435 = NOVALUE;
            goto L2; // [18] 35
        }
    }
    else {
        if (DBL_PTR(_14435)->dbl != 0.0){
            DeRef(_14435);
            _14435 = NOVALUE;
            goto L2; // [18] 35
        }
    }
    DeRef(_14435);
    _14435 = NOVALUE;

    /** 			CompileErr(51, {new_include_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_38new_include_name_17088);
    *((int *)(_2+4)) = _38new_include_name_17088;
    _14437 = MAKE_SEQ(_1);
    _46CompileErr(51, _14437, 0);
    _14437 = NOVALUE;
L2: 

    /** 		return fname*/
    DeRef(_full_path_25010);
    DeRef(_errbuff_25011);
    DeRef(_currdir_25012);
    DeRef(_conf_path_25013);
    DeRef(_scan_result_25014);
    DeRef(_inc_path_25015);
    DeRef(_mainpath_25034);
    return _fname_25008;
L1: 

    /** 	currdir = get_file_path( known_files[current_file_no] )*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _14438 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    Ref(_14438);
    _0 = _currdir_25012;
    _currdir_25012 = _62get_file_path(_14438);
    DeRef(_0);
    _14438 = NOVALUE;

    /** 	full_path = currdir & fname*/
    Concat((object_ptr)&_full_path_25010, _currdir_25012, _fname_25008);

    /** 	if file_exists(full_path) then*/
    RefDS(_full_path_25010);
    _14441 = _13file_exists(_full_path_25010);
    if (_14441 == 0) {
        DeRef(_14441);
        _14441 = NOVALUE;
        goto L3; // [70] 80
    }
    else {
        if (!IS_ATOM_INT(_14441) && DBL_PTR(_14441)->dbl == 0.0){
            DeRef(_14441);
            _14441 = NOVALUE;
            goto L3; // [70] 80
        }
        DeRef(_14441);
        _14441 = NOVALUE;
    }
    DeRef(_14441);
    _14441 = NOVALUE;

    /** 		return full_path*/
    DeRefDS(_fname_25008);
    DeRef(_errbuff_25011);
    DeRefDS(_currdir_25012);
    DeRef(_conf_path_25013);
    DeRef(_scan_result_25014);
    DeRef(_inc_path_25015);
    DeRef(_mainpath_25034);
    return _full_path_25010;
L3: 

    /** 	sequence mainpath = main_path[1..rfind(SLASH, main_path)]*/
    RefDS(_38main_path_17086);
    DeRef(_32385);
    _32385 = _38main_path_17086;
    if (IS_SEQUENCE(_32385)){
            _32386 = SEQ_PTR(_32385)->length;
    }
    else {
        _32386 = 1;
    }
    _32385 = NOVALUE;
    RefDS(_38main_path_17086);
    _14442 = _12rfind(92, _38main_path_17086, _32386);
    _32386 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mainpath_25034;
    RHS_Slice(_38main_path_17086, 1, _14442);

    /** 	if not equal(mainpath, currdir) then*/
    if (_mainpath_25034 == _currdir_25012)
    _14444 = 1;
    else if (IS_ATOM_INT(_mainpath_25034) && IS_ATOM_INT(_currdir_25012))
    _14444 = 0;
    else
    _14444 = (compare(_mainpath_25034, _currdir_25012) == 0);
    if (_14444 != 0)
    goto L4; // [111] 139
    _14444 = NOVALUE;

    /** 		full_path = mainpath & new_include_name*/
    Concat((object_ptr)&_full_path_25010, _mainpath_25034, _38new_include_name_17088);

    /** 		if file_exists(full_path) then*/
    RefDS(_full_path_25010);
    _14447 = _13file_exists(_full_path_25010);
    if (_14447 == 0) {
        DeRef(_14447);
        _14447 = NOVALUE;
        goto L5; // [128] 138
    }
    else {
        if (!IS_ATOM_INT(_14447) && DBL_PTR(_14447)->dbl == 0.0){
            DeRef(_14447);
            _14447 = NOVALUE;
            goto L5; // [128] 138
        }
        DeRef(_14447);
        _14447 = NOVALUE;
    }
    DeRef(_14447);
    _14447 = NOVALUE;

    /** 			return full_path*/
    DeRefDS(_fname_25008);
    DeRef(_errbuff_25011);
    DeRefDS(_currdir_25012);
    DeRef(_conf_path_25013);
    DeRef(_scan_result_25014);
    DeRef(_inc_path_25015);
    DeRefDS(_mainpath_25034);
    _32385 = NOVALUE;
    DeRef(_14442);
    _14442 = NOVALUE;
    return _full_path_25010;
L5: 
L4: 

    /** 	scan_result = ConfPath(new_include_name)*/
    RefDS(_38new_include_name_17088);
    _0 = _scan_result_25014;
    _scan_result_25014 = _44ConfPath(_38new_include_name_17088);
    DeRef(_0);

    /** 	if atom(scan_result) then*/
    _14449 = IS_ATOM(_scan_result_25014);
    if (_14449 == 0)
    {
        _14449 = NOVALUE;
        goto L6; // [152] 164
    }
    else{
        _14449 = NOVALUE;
    }

    /** 		scan_result = ScanPath(fname,"EUINC",0)*/
    RefDS(_fname_25008);
    RefDS(_14450);
    _0 = _scan_result_25014;
    _scan_result_25014 = _44ScanPath(_fname_25008, _14450, 0);
    DeRef(_0);
L6: 

    /** 	if atom(scan_result) then*/
    _14452 = IS_ATOM(_scan_result_25014);
    if (_14452 == 0)
    {
        _14452 = NOVALUE;
        goto L7; // [169] 181
    }
    else{
        _14452 = NOVALUE;
    }

    /** 		scan_result = ScanPath(fname, "EUDIR",1)*/
    RefDS(_fname_25008);
    RefDS(_14453);
    _0 = _scan_result_25014;
    _scan_result_25014 = _44ScanPath(_fname_25008, _14453, 1);
    DeRef(_0);
L7: 

    /** 	if atom(scan_result) then*/
    _14455 = IS_ATOM(_scan_result_25014);
    if (_14455 == 0)
    {
        _14455 = NOVALUE;
        goto L8; // [186] 223
    }
    else{
        _14455 = NOVALUE;
    }

    /** 		full_path = get_eudir() & SLASH & "include" & SLASH & fname*/
    _14456 = _35get_eudir();
    {
        int concat_list[5];

        concat_list[0] = _fname_25008;
        concat_list[1] = 92;
        concat_list[2] = _13873;
        concat_list[3] = 92;
        concat_list[4] = _14456;
        Concat_N((object_ptr)&_full_path_25010, concat_list, 5);
    }
    DeRef(_14456);
    _14456 = NOVALUE;

    /** 		if file_exists(full_path) then*/
    RefDS(_full_path_25010);
    _14458 = _13file_exists(_full_path_25010);
    if (_14458 == 0) {
        DeRef(_14458);
        _14458 = NOVALUE;
        goto L9; // [212] 222
    }
    else {
        if (!IS_ATOM_INT(_14458) && DBL_PTR(_14458)->dbl == 0.0){
            DeRef(_14458);
            _14458 = NOVALUE;
            goto L9; // [212] 222
        }
        DeRef(_14458);
        _14458 = NOVALUE;
    }
    DeRef(_14458);
    _14458 = NOVALUE;

    /** 			return full_path*/
    DeRefDS(_fname_25008);
    DeRef(_errbuff_25011);
    DeRef(_currdir_25012);
    DeRef(_conf_path_25013);
    DeRef(_scan_result_25014);
    DeRef(_inc_path_25015);
    DeRef(_mainpath_25034);
    _32385 = NOVALUE;
    DeRef(_14442);
    _14442 = NOVALUE;
    return _full_path_25010;
L9: 
L8: 

    /** 	if sequence(scan_result) then*/
    _14459 = IS_SEQUENCE(_scan_result_25014);
    if (_14459 == 0)
    {
        _14459 = NOVALUE;
        goto LA; // [228] 250
    }
    else{
        _14459 = NOVALUE;
    }

    /** 		close(scan_result[2])*/
    _2 = (int)SEQ_PTR(_scan_result_25014);
    _14460 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_14460))
    EClose(_14460);
    else
    EClose((int)DBL_PTR(_14460)->dbl);
    _14460 = NOVALUE;

    /** 		return scan_result[1]*/
    _2 = (int)SEQ_PTR(_scan_result_25014);
    _14461 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_14461);
    DeRefDS(_fname_25008);
    DeRef(_full_path_25010);
    DeRef(_errbuff_25011);
    DeRef(_currdir_25012);
    DeRef(_conf_path_25013);
    DeRef(_scan_result_25014);
    DeRef(_inc_path_25015);
    DeRef(_mainpath_25034);
    _32385 = NOVALUE;
    DeRef(_14442);
    _14442 = NOVALUE;
    return _14461;
LA: 

    /** 	errbuff = ""*/
    RefDS(_5);
    DeRef(_errbuff_25011);
    _errbuff_25011 = _5;

    /** 	full_path = {}*/
    RefDS(_5);
    DeRef(_full_path_25010);
    _full_path_25010 = _5;

    /** 	if length(currdir) > 0 then*/
    if (IS_SEQUENCE(_currdir_25012)){
            _14462 = SEQ_PTR(_currdir_25012)->length;
    }
    else {
        _14462 = 1;
    }
    if (_14462 <= 0)
    goto LB; // [269] 321

    /** 		if find(currdir[$], SLASH_CHARS) then*/
    if (IS_SEQUENCE(_currdir_25012)){
            _14464 = SEQ_PTR(_currdir_25012)->length;
    }
    else {
        _14464 = 1;
    }
    _2 = (int)SEQ_PTR(_currdir_25012);
    _14465 = (int)*(((s1_ptr)_2)->base + _14464);
    _14466 = find_from(_14465, _42SLASH_CHARS_17124, 1);
    _14465 = NOVALUE;
    if (_14466 == 0)
    {
        _14466 = NOVALUE;
        goto LC; // [289] 313
    }
    else{
        _14466 = NOVALUE;
    }

    /** 			full_path = append(full_path, currdir[1..$-1])*/
    if (IS_SEQUENCE(_currdir_25012)){
            _14467 = SEQ_PTR(_currdir_25012)->length;
    }
    else {
        _14467 = 1;
    }
    _14468 = _14467 - 1;
    _14467 = NOVALUE;
    rhs_slice_target = (object_ptr)&_14469;
    RHS_Slice(_currdir_25012, 1, _14468);
    RefDS(_14469);
    Append(&_full_path_25010, _full_path_25010, _14469);
    DeRefDS(_14469);
    _14469 = NOVALUE;
    goto LD; // [310] 320
LC: 

    /** 			full_path = append(full_path, currdir)*/
    RefDS(_currdir_25012);
    Append(&_full_path_25010, _full_path_25010, _currdir_25012);
LD: 
LB: 

    /** 	if find(main_path[$], SLASH_CHARS) then*/
    if (IS_SEQUENCE(_38main_path_17086)){
            _14472 = SEQ_PTR(_38main_path_17086)->length;
    }
    else {
        _14472 = 1;
    }
    _2 = (int)SEQ_PTR(_38main_path_17086);
    _14473 = (int)*(((s1_ptr)_2)->base + _14472);
    _14474 = find_from(_14473, _42SLASH_CHARS_17124, 1);
    _14473 = NOVALUE;
    if (_14474 == 0)
    {
        _14474 = NOVALUE;
        goto LE; // [339] 361
    }
    else{
        _14474 = NOVALUE;
    }

    /** 		errbuff = main_path[1..$-1]  -- looks better*/
    if (IS_SEQUENCE(_38main_path_17086)){
            _14475 = SEQ_PTR(_38main_path_17086)->length;
    }
    else {
        _14475 = 1;
    }
    _14476 = _14475 - 1;
    _14475 = NOVALUE;
    rhs_slice_target = (object_ptr)&_errbuff_25011;
    RHS_Slice(_38main_path_17086, 1, _14476);
    goto LF; // [358] 371
LE: 

    /** 		errbuff = main_path*/
    RefDS(_38main_path_17086);
    DeRef(_errbuff_25011);
    _errbuff_25011 = _38main_path_17086;
LF: 

    /** 	if not find(errbuff, full_path) then*/
    _14478 = find_from(_errbuff_25011, _full_path_25010, 1);
    if (_14478 != 0)
    goto L10; // [378] 388
    _14478 = NOVALUE;

    /** 		full_path = append(full_path, errbuff)*/
    RefDS(_errbuff_25011);
    Append(&_full_path_25010, _full_path_25010, _errbuff_25011);
L10: 

    /** 	conf_path = get_conf_dirs()*/
    _0 = _conf_path_25013;
    _conf_path_25013 = _44get_conf_dirs();
    DeRef(_0);

    /** 	if length(conf_path) > 0 then*/
    if (IS_SEQUENCE(_conf_path_25013)){
            _14482 = SEQ_PTR(_conf_path_25013)->length;
    }
    else {
        _14482 = 1;
    }
    if (_14482 <= 0)
    goto L11; // [400] 507

    /** 		conf_path = split(conf_path, PATHSEP)*/
    RefDS(_conf_path_25013);
    _0 = _conf_path_25013;
    _conf_path_25013 = _21split(_conf_path_25013, 59, 0, 0);
    DeRefDS(_0);

    /** 		for i = 1 to length(conf_path) do*/
    if (IS_SEQUENCE(_conf_path_25013)){
            _14485 = SEQ_PTR(_conf_path_25013)->length;
    }
    else {
        _14485 = 1;
    }
    {
        int _i_25115;
        _i_25115 = 1;
L12: 
        if (_i_25115 > _14485){
            goto L13; // [422] 506
        }

        /** 			if find(conf_path[i][$], SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_conf_path_25013);
        _14486 = (int)*(((s1_ptr)_2)->base + _i_25115);
        if (IS_SEQUENCE(_14486)){
                _14487 = SEQ_PTR(_14486)->length;
        }
        else {
            _14487 = 1;
        }
        _2 = (int)SEQ_PTR(_14486);
        _14488 = (int)*(((s1_ptr)_2)->base + _14487);
        _14486 = NOVALUE;
        _14489 = find_from(_14488, _42SLASH_CHARS_17124, 1);
        _14488 = NOVALUE;
        if (_14489 == 0)
        {
            _14489 = NOVALUE;
            goto L14; // [449] 473
        }
        else{
            _14489 = NOVALUE;
        }

        /** 				errbuff = conf_path[i][1..$-1]  -- looks better*/
        _2 = (int)SEQ_PTR(_conf_path_25013);
        _14490 = (int)*(((s1_ptr)_2)->base + _i_25115);
        if (IS_SEQUENCE(_14490)){
                _14491 = SEQ_PTR(_14490)->length;
        }
        else {
            _14491 = 1;
        }
        _14492 = _14491 - 1;
        _14491 = NOVALUE;
        rhs_slice_target = (object_ptr)&_errbuff_25011;
        RHS_Slice(_14490, 1, _14492);
        _14490 = NOVALUE;
        goto L15; // [470] 482
L14: 

        /** 				errbuff = conf_path[i]*/
        DeRef(_errbuff_25011);
        _2 = (int)SEQ_PTR(_conf_path_25013);
        _errbuff_25011 = (int)*(((s1_ptr)_2)->base + _i_25115);
        Ref(_errbuff_25011);
L15: 

        /** 			if not find(errbuff, full_path) then*/
        _14495 = find_from(_errbuff_25011, _full_path_25010, 1);
        if (_14495 != 0)
        goto L16; // [489] 499
        _14495 = NOVALUE;

        /** 				full_path = append(full_path, errbuff)*/
        RefDS(_errbuff_25011);
        Append(&_full_path_25010, _full_path_25010, _errbuff_25011);
L16: 

        /** 		end for*/
        _i_25115 = _i_25115 + 1;
        goto L12; // [501] 429
L13: 
        ;
    }
L11: 

    /** 	inc_path = getenv("EUINC")*/
    DeRef(_inc_path_25015);
    _inc_path_25015 = EGetEnv(_14450);

    /** 	if sequence(inc_path) then*/
    _14499 = IS_SEQUENCE(_inc_path_25015);
    if (_14499 == 0)
    {
        _14499 = NOVALUE;
        goto L17; // [517] 631
    }
    else{
        _14499 = NOVALUE;
    }

    /** 		if length(inc_path) > 0 then*/
    if (IS_SEQUENCE(_inc_path_25015)){
            _14500 = SEQ_PTR(_inc_path_25015)->length;
    }
    else {
        _14500 = 1;
    }
    if (_14500 <= 0)
    goto L18; // [525] 630

    /** 			inc_path = split(inc_path, PATHSEP)*/
    Ref(_inc_path_25015);
    _0 = _inc_path_25015;
    _inc_path_25015 = _21split(_inc_path_25015, 59, 0, 0);
    DeRefi(_0);

    /** 			for i = 1 to length(inc_path) do*/
    if (IS_SEQUENCE(_inc_path_25015)){
            _14503 = SEQ_PTR(_inc_path_25015)->length;
    }
    else {
        _14503 = 1;
    }
    {
        int _i_25143;
        _i_25143 = 1;
L19: 
        if (_i_25143 > _14503){
            goto L1A; // [545] 629
        }

        /** 				if find(inc_path[i][$], SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_inc_path_25015);
        _14504 = (int)*(((s1_ptr)_2)->base + _i_25143);
        if (IS_SEQUENCE(_14504)){
                _14505 = SEQ_PTR(_14504)->length;
        }
        else {
            _14505 = 1;
        }
        _2 = (int)SEQ_PTR(_14504);
        _14506 = (int)*(((s1_ptr)_2)->base + _14505);
        _14504 = NOVALUE;
        _14507 = find_from(_14506, _42SLASH_CHARS_17124, 1);
        _14506 = NOVALUE;
        if (_14507 == 0)
        {
            _14507 = NOVALUE;
            goto L1B; // [572] 596
        }
        else{
            _14507 = NOVALUE;
        }

        /** 					errbuff = inc_path[i][1..$-1]  -- looks better*/
        _2 = (int)SEQ_PTR(_inc_path_25015);
        _14508 = (int)*(((s1_ptr)_2)->base + _i_25143);
        if (IS_SEQUENCE(_14508)){
                _14509 = SEQ_PTR(_14508)->length;
        }
        else {
            _14509 = 1;
        }
        _14510 = _14509 - 1;
        _14509 = NOVALUE;
        rhs_slice_target = (object_ptr)&_errbuff_25011;
        RHS_Slice(_14508, 1, _14510);
        _14508 = NOVALUE;
        goto L1C; // [593] 605
L1B: 

        /** 					errbuff = inc_path[i]*/
        DeRef(_errbuff_25011);
        _2 = (int)SEQ_PTR(_inc_path_25015);
        _errbuff_25011 = (int)*(((s1_ptr)_2)->base + _i_25143);
        Ref(_errbuff_25011);
L1C: 

        /** 				if not find(errbuff, full_path) then*/
        _14513 = find_from(_errbuff_25011, _full_path_25010, 1);
        if (_14513 != 0)
        goto L1D; // [612] 622
        _14513 = NOVALUE;

        /** 					full_path = append(full_path, errbuff)*/
        RefDS(_errbuff_25011);
        Append(&_full_path_25010, _full_path_25010, _errbuff_25011);
L1D: 

        /** 			end for*/
        _i_25143 = _i_25143 + 1;
        goto L19; // [624] 552
L1A: 
        ;
    }
L18: 
L17: 

    /** 	if length(get_eudir()) > 0 then*/
    _14516 = _35get_eudir();
    if (IS_SEQUENCE(_14516)){
            _14517 = SEQ_PTR(_14516)->length;
    }
    else {
        _14517 = 1;
    }
    DeRef(_14516);
    _14516 = NOVALUE;
    if (_14517 <= 0)
    goto L1E; // [639] 667

    /** 		if not find(get_eudir(), full_path) then*/
    _14519 = _35get_eudir();
    _14520 = find_from(_14519, _full_path_25010, 1);
    DeRef(_14519);
    _14519 = NOVALUE;
    if (_14520 != 0)
    goto L1F; // [653] 666
    _14520 = NOVALUE;

    /** 			full_path = append(full_path, get_eudir())*/
    _14522 = _35get_eudir();
    Ref(_14522);
    Append(&_full_path_25010, _full_path_25010, _14522);
    DeRef(_14522);
    _14522 = NOVALUE;
L1F: 
L1E: 

    /** 	errbuff = ""*/
    RefDS(_5);
    DeRef(_errbuff_25011);
    _errbuff_25011 = _5;

    /** 	for i = 1 to length(full_path) do*/
    if (IS_SEQUENCE(_full_path_25010)){
            _14524 = SEQ_PTR(_full_path_25010)->length;
    }
    else {
        _14524 = 1;
    }
    {
        int _i_25175;
        _i_25175 = 1;
L20: 
        if (_i_25175 > _14524){
            goto L21; // [679] 711
        }

        /** 		errbuff &= sprintf("\t%s\n", {full_path[i]})*/
        _2 = (int)SEQ_PTR(_full_path_25010);
        _14526 = (int)*(((s1_ptr)_2)->base + _i_25175);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_14526);
        *((int *)(_2+4)) = _14526;
        _14527 = MAKE_SEQ(_1);
        _14526 = NOVALUE;
        _14528 = EPrintf(-9999999, _14525, _14527);
        DeRefDS(_14527);
        _14527 = NOVALUE;
        Concat((object_ptr)&_errbuff_25011, _errbuff_25011, _14528);
        DeRefDS(_14528);
        _14528 = NOVALUE;

        /** 	end for*/
        _i_25175 = _i_25175 + 1;
        goto L20; // [706] 686
L21: 
        ;
    }

    /** 	CompileErr(52, {new_include_name, errbuff})*/
    RefDS(_errbuff_25011);
    RefDS(_38new_include_name_17088);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _38new_include_name_17088;
    ((int *)_2)[2] = _errbuff_25011;
    _14530 = MAKE_SEQ(_1);
    _46CompileErr(52, _14530, 0);
    _14530 = NOVALUE;
    ;
}


int _62path_open()
{
    int _fh_25187 = NOVALUE;
    int _0, _1, _2;
    

    /** 	new_include_name = find_file(new_include_name)*/
    RefDS(_38new_include_name_17088);
    _0 = _62find_file(_38new_include_name_17088);
    DeRefDS(_38new_include_name_17088);
    _38new_include_name_17088 = _0;

    /** 	new_include_name = maybe_preprocess(new_include_name)*/
    RefDS(_38new_include_name_17088);
    _0 = _66maybe_preprocess(_38new_include_name_17088);
    DeRefDS(_38new_include_name_17088);
    _38new_include_name_17088 = _0;

    /** 	fh = open_locked(new_include_name)*/
    RefDS(_38new_include_name_17088);
    _fh_25187 = _35open_locked(_38new_include_name_17088);
    if (!IS_ATOM_INT(_fh_25187)) {
        _1 = (long)(DBL_PTR(_fh_25187)->dbl);
        if (UNIQUE(DBL_PTR(_fh_25187)) && (DBL_PTR(_fh_25187)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_25187);
        _fh_25187 = _1;
    }

    /** 	return fh*/
    return _fh_25187;
    ;
}


int _62NameSpace_declaration(int _sym_25215)
{
    int _h_25216 = NOVALUE;
    int _14554 = NOVALUE;
    int _14552 = NOVALUE;
    int _14550 = NOVALUE;
    int _14548 = NOVALUE;
    int _14547 = NOVALUE;
    int _14546 = NOVALUE;
    int _14544 = NOVALUE;
    int _14543 = NOVALUE;
    int _14542 = NOVALUE;
    int _14541 = NOVALUE;
    int _14540 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sym_25215)) {
        _1 = (long)(DBL_PTR(_sym_25215)->dbl);
        if (UNIQUE(DBL_PTR(_sym_25215)) && (DBL_PTR(_sym_25215)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_25215);
        _sym_25215 = _1;
    }

    /** 	DefinedYet(sym)*/
    _55DefinedYet(_sym_25215);

    /** 	if find(SymTab[sym][S_SCOPE], {SC_GLOBAL, SC_PUBLIC, SC_EXPORT, SC_PREDEF}) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _14540 = (int)*(((s1_ptr)_2)->base + _sym_25215);
    _2 = (int)SEQ_PTR(_14540);
    _14541 = (int)*(((s1_ptr)_2)->base + 4);
    _14540 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 6;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 11;
    *((int *)(_2+16)) = 7;
    _14542 = MAKE_SEQ(_1);
    _14543 = find_from(_14541, _14542, 1);
    _14541 = NOVALUE;
    DeRefDS(_14542);
    _14542 = NOVALUE;
    if (_14543 == 0)
    {
        _14543 = NOVALUE;
        goto L1; // [42] 104
    }
    else{
        _14543 = NOVALUE;
    }

    /** 		h = SymTab[sym][S_HASHVAL]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _14544 = (int)*(((s1_ptr)_2)->base + _sym_25215);
    _2 = (int)SEQ_PTR(_14544);
    _h_25216 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_h_25216)){
        _h_25216 = (long)DBL_PTR(_h_25216)->dbl;
    }
    _14544 = NOVALUE;

    /** 		sym = NewEntry(SymTab[sym][S_NAME], 0, 0, VARIABLE, h, buckets[h], 0)*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _14546 = (int)*(((s1_ptr)_2)->base + _sym_25215);
    _2 = (int)SEQ_PTR(_14546);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _14547 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _14547 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _14546 = NOVALUE;
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _14548 = (int)*(((s1_ptr)_2)->base + _h_25216);
    Ref(_14547);
    Ref(_14548);
    _sym_25215 = _55NewEntry(_14547, 0, 0, -100, _h_25216, _14548, 0);
    _14547 = NOVALUE;
    _14548 = NOVALUE;
    if (!IS_ATOM_INT(_sym_25215)) {
        _1 = (long)(DBL_PTR(_sym_25215)->dbl);
        if (UNIQUE(DBL_PTR(_sym_25215)) && (DBL_PTR(_sym_25215)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_25215);
        _sym_25215 = _1;
    }

    /** 		buckets[h] = sym*/
    _2 = (int)SEQ_PTR(_55buckets_47051);
    _2 = (int)(((s1_ptr)_2)->base + _h_25216);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_25215;
    DeRef(_1);
L1: 

    /** 	SymTab[sym][S_SCOPE] = SC_LOCAL*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25215 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 5;
    DeRef(_1);
    _14550 = NOVALUE;

    /** 	SymTab[sym][S_MODE] = M_CONSTANT*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25215 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _14552 = NOVALUE;

    /** 	SymTab[sym][S_TOKEN] = NAMESPACE -- [S_OBJ] will get the file number referred-to*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25215 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_TOKEN_16603))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    _1 = *(int *)_2;
    *(int *)_2 = 523;
    DeRef(_1);
    _14554 = NOVALUE;

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2; // [159] 173
    }
    else{
    }

    /** 		num_routines += 1 -- order of ns declaration relative to routines*/
    _38num_routines_16955 = _38num_routines_16955 + 1;
L2: 

    /** 	return sym*/
    return _sym_25215;
    ;
}


void _62default_namespace()
{
    int _tok_25266 = NOVALUE;
    int _sym_25268 = NOVALUE;
    int _14578 = NOVALUE;
    int _14577 = NOVALUE;
    int _14575 = NOVALUE;
    int _14573 = NOVALUE;
    int _14570 = NOVALUE;
    int _14567 = NOVALUE;
    int _14565 = NOVALUE;
    int _14563 = NOVALUE;
    int _14562 = NOVALUE;
    int _14561 = NOVALUE;
    int _14560 = NOVALUE;
    int _14559 = NOVALUE;
    int _14558 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	tok = call_func( scanner_rid, {} )*/
    _0 = (int)_00[_62scanner_rid_25262].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_tok_25266);
    _tok_25266 = _1;

    /** 	if tok[T_ID] = VARIABLE and equal( SymTab[tok[T_SYM]][S_NAME], "namespace" ) then*/
    _2 = (int)SEQ_PTR(_tok_25266);
    _14558 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_14558)) {
        _14559 = (_14558 == -100);
    }
    else {
        _14559 = binary_op(EQUALS, _14558, -100);
    }
    _14558 = NOVALUE;
    if (IS_ATOM_INT(_14559)) {
        if (_14559 == 0) {
            goto L1; // [23] 177
        }
    }
    else {
        if (DBL_PTR(_14559)->dbl == 0.0) {
            goto L1; // [23] 177
        }
    }
    _2 = (int)SEQ_PTR(_tok_25266);
    _14561 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_14561)){
        _14562 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14561)->dbl));
    }
    else{
        _14562 = (int)*(((s1_ptr)_2)->base + _14561);
    }
    _2 = (int)SEQ_PTR(_14562);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _14563 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _14563 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _14562 = NOVALUE;
    if (_14563 == _14564)
    _14565 = 1;
    else if (IS_ATOM_INT(_14563) && IS_ATOM_INT(_14564))
    _14565 = 0;
    else
    _14565 = (compare(_14563, _14564) == 0);
    _14563 = NOVALUE;
    if (_14565 == 0)
    {
        _14565 = NOVALUE;
        goto L1; // [50] 177
    }
    else{
        _14565 = NOVALUE;
    }

    /** 		tok = call_func( scanner_rid, {} )*/
    _0 = (int)_00[_62scanner_rid_25262].addr;
    _1 = (*(int (*)())_0)(
                         );
    DeRef(_tok_25266);
    _tok_25266 = _1;

    /** 		if tok[T_ID] != VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_25266);
    _14567 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _14567, -100)){
        _14567 = NOVALUE;
        goto L2; // [71] 83
    }
    _14567 = NOVALUE;

    /** 			CompileErr(114)*/
    RefDS(_22663);
    _46CompileErr(114, _22663, 0);
L2: 

    /** 		sym = tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_25266);
    _sym_25268 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_25268)){
        _sym_25268 = (long)DBL_PTR(_sym_25268)->dbl;
    }

    /** 		SymTab[sym][S_FILE_NO] = current_file_no*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25268 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_FILE_NO_16594))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    _1 = *(int *)_2;
    *(int *)_2 = _38current_file_no_16946;
    DeRef(_1);
    _14570 = NOVALUE;

    /** 		sym  = NameSpace_declaration( sym )*/
    _sym_25268 = _62NameSpace_declaration(_sym_25268);
    if (!IS_ATOM_INT(_sym_25268)) {
        _1 = (long)(DBL_PTR(_sym_25268)->dbl);
        if (UNIQUE(DBL_PTR(_sym_25268)) && (DBL_PTR(_sym_25268)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_25268);
        _sym_25268 = _1;
    }

    /** 		SymTab[sym][S_OBJ] = current_file_no*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25268 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38current_file_no_16946;
    DeRef(_1);
    _14573 = NOVALUE;

    /** 		SymTab[sym][S_SCOPE] = SC_PUBLIC*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sym_25268 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 13;
    DeRef(_1);
    _14575 = NOVALUE;

    /** 		default_namespaces[current_file_no] = SymTab[sym][S_NAME]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _14577 = (int)*(((s1_ptr)_2)->base + _sym_25268);
    _2 = (int)SEQ_PTR(_14577);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _14578 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _14578 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _14577 = NOVALUE;
    Ref(_14578);
    _2 = (int)SEQ_PTR(_62default_namespaces_24629);
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _14578;
    if( _1 != _14578 ){
        DeRef(_1);
    }
    _14578 = NOVALUE;
    goto L3; // [174] 185
L1: 

    /** 		bp = 1*/
    _46bp_49486 = 1;
L3: 

    /** end procedure*/
    DeRef(_tok_25266);
    _14561 = NOVALUE;
    DeRef(_14559);
    _14559 = NOVALUE;
    return;
    ;
}


void _62add_exports(int _from_file_25318, int _to_file_25319)
{
    int _exports_25320 = NOVALUE;
    int _direct_25321 = NOVALUE;
    int _14598 = NOVALUE;
    int _14597 = NOVALUE;
    int _14596 = NOVALUE;
    int _14595 = NOVALUE;
    int _14594 = NOVALUE;
    int _14592 = NOVALUE;
    int _14590 = NOVALUE;
    int _14589 = NOVALUE;
    int _14587 = NOVALUE;
    int _14586 = NOVALUE;
    int _14585 = NOVALUE;
    int _14583 = NOVALUE;
    int _14582 = NOVALUE;
    int _14581 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	direct = file_include[to_file]*/
    DeRef(_direct_25321);
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _direct_25321 = (int)*(((s1_ptr)_2)->base + _to_file_25319);
    Ref(_direct_25321);

    /** 	exports = file_public[from_file]*/
    DeRef(_exports_25320);
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _exports_25320 = (int)*(((s1_ptr)_2)->base + _from_file_25318);
    Ref(_exports_25320);

    /** 	for i = 1 to length(exports) do*/
    if (IS_SEQUENCE(_exports_25320)){
            _14581 = SEQ_PTR(_exports_25320)->length;
    }
    else {
        _14581 = 1;
    }
    {
        int _i_25327;
        _i_25327 = 1;
L1: 
        if (_i_25327 > _14581){
            goto L2; // [30] 127
        }

        /** 		if not find( exports[i], direct ) then*/
        _2 = (int)SEQ_PTR(_exports_25320);
        _14582 = (int)*(((s1_ptr)_2)->base + _i_25327);
        _14583 = find_from(_14582, _direct_25321, 1);
        _14582 = NOVALUE;
        if (_14583 != 0)
        goto L3; // [48] 120
        _14583 = NOVALUE;

        /** 			if not find( -exports[i], direct ) then*/
        _2 = (int)SEQ_PTR(_exports_25320);
        _14585 = (int)*(((s1_ptr)_2)->base + _i_25327);
        if (IS_ATOM_INT(_14585)) {
            if ((unsigned long)_14585 == 0xC0000000)
            _14586 = (int)NewDouble((double)-0xC0000000);
            else
            _14586 = - _14585;
        }
        else {
            _14586 = unary_op(UMINUS, _14585);
        }
        _14585 = NOVALUE;
        _14587 = find_from(_14586, _direct_25321, 1);
        DeRef(_14586);
        _14586 = NOVALUE;
        if (_14587 != 0)
        goto L4; // [65] 82
        _14587 = NOVALUE;

        /** 				direct &= -exports[i]*/
        _2 = (int)SEQ_PTR(_exports_25320);
        _14589 = (int)*(((s1_ptr)_2)->base + _i_25327);
        if (IS_ATOM_INT(_14589)) {
            if ((unsigned long)_14589 == 0xC0000000)
            _14590 = (int)NewDouble((double)-0xC0000000);
            else
            _14590 = - _14589;
        }
        else {
            _14590 = unary_op(UMINUS, _14589);
        }
        _14589 = NOVALUE;
        if (IS_SEQUENCE(_direct_25321) && IS_ATOM(_14590)) {
            Ref(_14590);
            Append(&_direct_25321, _direct_25321, _14590);
        }
        else if (IS_ATOM(_direct_25321) && IS_SEQUENCE(_14590)) {
        }
        else {
            Concat((object_ptr)&_direct_25321, _direct_25321, _14590);
        }
        DeRef(_14590);
        _14590 = NOVALUE;
L4: 

        /** 			include_matrix[to_file][exports[i]] = or_bits( PUBLIC_INCLUDE, include_matrix[to_file][exports[i]] )*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35include_matrix_15602 = MAKE_SEQ(_2);
        }
        _3 = (int)(_to_file_25319 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_exports_25320);
        _14594 = (int)*(((s1_ptr)_2)->base + _i_25327);
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _14595 = (int)*(((s1_ptr)_2)->base + _to_file_25319);
        _2 = (int)SEQ_PTR(_exports_25320);
        _14596 = (int)*(((s1_ptr)_2)->base + _i_25327);
        _2 = (int)SEQ_PTR(_14595);
        if (!IS_ATOM_INT(_14596)){
            _14597 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14596)->dbl));
        }
        else{
            _14597 = (int)*(((s1_ptr)_2)->base + _14596);
        }
        _14595 = NOVALUE;
        if (IS_ATOM_INT(_14597)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14597;
                 _14598 = MAKE_UINT(tu);
            }
        }
        else {
            _14598 = binary_op(OR_BITS, 4, _14597);
        }
        _14597 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_14594))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14594)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _14594);
        _1 = *(int *)_2;
        *(int *)_2 = _14598;
        if( _1 != _14598 ){
            DeRef(_1);
        }
        _14598 = NOVALUE;
        _14592 = NOVALUE;
L3: 

        /** 	end for*/
        _i_25327 = _i_25327 + 1;
        goto L1; // [122] 37
L2: 
        ;
    }

    /** 	file_include[to_file] = direct*/
    RefDS(_direct_25321);
    _2 = (int)SEQ_PTR(_35file_include_15600);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35file_include_15600 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _to_file_25319);
    _1 = *(int *)_2;
    *(int *)_2 = _direct_25321;
    DeRef(_1);

    /** end procedure*/
    DeRef(_exports_25320);
    DeRefDS(_direct_25321);
    _14594 = NOVALUE;
    _14596 = NOVALUE;
    return;
    ;
}


void _62patch_exports(int _for_file_25354)
{
    int _export_len_25355 = NOVALUE;
    int _14609 = NOVALUE;
    int _14608 = NOVALUE;
    int _14606 = NOVALUE;
    int _14605 = NOVALUE;
    int _14604 = NOVALUE;
    int _14603 = NOVALUE;
    int _14601 = NOVALUE;
    int _14600 = NOVALUE;
    int _14599 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length(file_include) do*/
    if (IS_SEQUENCE(_35file_include_15600)){
            _14599 = SEQ_PTR(_35file_include_15600)->length;
    }
    else {
        _14599 = 1;
    }
    {
        int _i_25357;
        _i_25357 = 1;
L1: 
        if (_i_25357 > _14599){
            goto L2; // [10] 99
        }

        /** 		if find( for_file, file_include[i] ) or find( -for_file, file_include[i] ) then*/
        _2 = (int)SEQ_PTR(_35file_include_15600);
        _14600 = (int)*(((s1_ptr)_2)->base + _i_25357);
        _14601 = find_from(_for_file_25354, _14600, 1);
        _14600 = NOVALUE;
        if (_14601 != 0) {
            goto L3; // [30] 53
        }
        if ((unsigned long)_for_file_25354 == 0xC0000000)
        _14603 = (int)NewDouble((double)-0xC0000000);
        else
        _14603 = - _for_file_25354;
        _2 = (int)SEQ_PTR(_35file_include_15600);
        _14604 = (int)*(((s1_ptr)_2)->base + _i_25357);
        _14605 = find_from(_14603, _14604, 1);
        DeRef(_14603);
        _14603 = NOVALUE;
        _14604 = NOVALUE;
        if (_14605 == 0)
        {
            _14605 = NOVALUE;
            goto L4; // [49] 92
        }
        else{
            _14605 = NOVALUE;
        }
L3: 

        /** 			export_len = length( file_include[i] )*/
        _2 = (int)SEQ_PTR(_35file_include_15600);
        _14606 = (int)*(((s1_ptr)_2)->base + _i_25357);
        if (IS_SEQUENCE(_14606)){
                _export_len_25355 = SEQ_PTR(_14606)->length;
        }
        else {
            _export_len_25355 = 1;
        }
        _14606 = NOVALUE;

        /** 			add_exports( for_file, i )*/
        _62add_exports(_for_file_25354, _i_25357);

        /** 			if length( file_include[i] ) != export_len then*/
        _2 = (int)SEQ_PTR(_35file_include_15600);
        _14608 = (int)*(((s1_ptr)_2)->base + _i_25357);
        if (IS_SEQUENCE(_14608)){
                _14609 = SEQ_PTR(_14608)->length;
        }
        else {
            _14609 = 1;
        }
        _14608 = NOVALUE;
        if (_14609 == _export_len_25355)
        goto L5; // [81] 91

        /** 				patch_exports( i )*/
        _62patch_exports(_i_25357);
L5: 
L4: 

        /** 	end for*/
        _i_25357 = _i_25357 + 1;
        goto L1; // [94] 17
L2: 
        ;
    }

    /** end procedure*/
    _14606 = NOVALUE;
    _14608 = NOVALUE;
    return;
    ;
}


void _62update_include_matrix(int _included_file_25379, int _from_file_25380)
{
    int _add_public_25390 = NOVALUE;
    int _px_25408 = NOVALUE;
    int _indirect_25467 = NOVALUE;
    int _mask_25470 = NOVALUE;
    int _ix_25481 = NOVALUE;
    int _indirect_file_25485 = NOVALUE;
    int _14685 = NOVALUE;
    int _14684 = NOVALUE;
    int _14682 = NOVALUE;
    int _14681 = NOVALUE;
    int _14680 = NOVALUE;
    int _14679 = NOVALUE;
    int _14678 = NOVALUE;
    int _14677 = NOVALUE;
    int _14676 = NOVALUE;
    int _14675 = NOVALUE;
    int _14674 = NOVALUE;
    int _14671 = NOVALUE;
    int _14669 = NOVALUE;
    int _14668 = NOVALUE;
    int _14667 = NOVALUE;
    int _14665 = NOVALUE;
    int _14663 = NOVALUE;
    int _14662 = NOVALUE;
    int _14660 = NOVALUE;
    int _14659 = NOVALUE;
    int _14658 = NOVALUE;
    int _14657 = NOVALUE;
    int _14656 = NOVALUE;
    int _14654 = NOVALUE;
    int _14653 = NOVALUE;
    int _14652 = NOVALUE;
    int _14651 = NOVALUE;
    int _14650 = NOVALUE;
    int _14649 = NOVALUE;
    int _14647 = NOVALUE;
    int _14646 = NOVALUE;
    int _14645 = NOVALUE;
    int _14643 = NOVALUE;
    int _14642 = NOVALUE;
    int _14641 = NOVALUE;
    int _14640 = NOVALUE;
    int _14639 = NOVALUE;
    int _14638 = NOVALUE;
    int _14637 = NOVALUE;
    int _14636 = NOVALUE;
    int _14635 = NOVALUE;
    int _14634 = NOVALUE;
    int _14633 = NOVALUE;
    int _14631 = NOVALUE;
    int _14630 = NOVALUE;
    int _14628 = NOVALUE;
    int _14626 = NOVALUE;
    int _14624 = NOVALUE;
    int _14623 = NOVALUE;
    int _14622 = NOVALUE;
    int _14621 = NOVALUE;
    int _14619 = NOVALUE;
    int _14618 = NOVALUE;
    int _14617 = NOVALUE;
    int _14615 = NOVALUE;
    int _14614 = NOVALUE;
    int _14613 = NOVALUE;
    int _14611 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	include_matrix[from_file][included_file] = or_bits( DIRECT_INCLUDE, include_matrix[from_file][included_file] )*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    _3 = (int)(_from_file_25380 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14613 = (int)*(((s1_ptr)_2)->base + _from_file_25380);
    _2 = (int)SEQ_PTR(_14613);
    _14614 = (int)*(((s1_ptr)_2)->base + _included_file_25379);
    _14613 = NOVALUE;
    if (IS_ATOM_INT(_14614)) {
        {unsigned long tu;
             tu = (unsigned long)2 | (unsigned long)_14614;
             _14615 = MAKE_UINT(tu);
        }
    }
    else {
        _14615 = binary_op(OR_BITS, 2, _14614);
    }
    _14614 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25379);
    _1 = *(int *)_2;
    *(int *)_2 = _14615;
    if( _1 != _14615 ){
        DeRef(_1);
    }
    _14615 = NOVALUE;
    _14611 = NOVALUE;

    /** 	if public_include then*/
    if (_62public_include_24626 == 0)
    {
        goto L1; // [38] 339
    }
    else{
    }

    /** 		sequence add_public = file_include_by[from_file]*/
    DeRef(_add_public_25390);
    _2 = (int)SEQ_PTR(_35file_include_by_15608);
    _add_public_25390 = (int)*(((s1_ptr)_2)->base + _from_file_25380);
    Ref(_add_public_25390);

    /** 		for i = 1 to length( add_public ) do*/
    if (IS_SEQUENCE(_add_public_25390)){
            _14617 = SEQ_PTR(_add_public_25390)->length;
    }
    else {
        _14617 = 1;
    }
    {
        int _i_25394;
        _i_25394 = 1;
L2: 
        if (_i_25394 > _14617){
            goto L3; // [56] 107
        }

        /** 			include_matrix[add_public[i]][included_file] =*/
        _2 = (int)SEQ_PTR(_add_public_25390);
        _14618 = (int)*(((s1_ptr)_2)->base + _i_25394);
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35include_matrix_15602 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_14618))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14618)->dbl));
        else
        _3 = (int)(_14618 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_add_public_25390);
        _14621 = (int)*(((s1_ptr)_2)->base + _i_25394);
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!IS_ATOM_INT(_14621)){
            _14622 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14621)->dbl));
        }
        else{
            _14622 = (int)*(((s1_ptr)_2)->base + _14621);
        }
        _2 = (int)SEQ_PTR(_14622);
        _14623 = (int)*(((s1_ptr)_2)->base + _included_file_25379);
        _14622 = NOVALUE;
        if (IS_ATOM_INT(_14623)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14623;
                 _14624 = MAKE_UINT(tu);
            }
        }
        else {
            _14624 = binary_op(OR_BITS, 4, _14623);
        }
        _14623 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _included_file_25379);
        _1 = *(int *)_2;
        *(int *)_2 = _14624;
        if( _1 != _14624 ){
            DeRef(_1);
        }
        _14624 = NOVALUE;
        _14619 = NOVALUE;

        /** 		end for*/
        _i_25394 = _i_25394 + 1;
        goto L2; // [102] 63
L3: 
        ;
    }

    /** 		add_public = file_public_by[from_file]*/
    DeRef(_add_public_25390);
    _2 = (int)SEQ_PTR(_35file_public_by_15610);
    _add_public_25390 = (int)*(((s1_ptr)_2)->base + _from_file_25380);
    Ref(_add_public_25390);

    /** 		integer px = length( add_public ) + 1*/
    if (IS_SEQUENCE(_add_public_25390)){
            _14626 = SEQ_PTR(_add_public_25390)->length;
    }
    else {
        _14626 = 1;
    }
    _px_25408 = _14626 + 1;
    _14626 = NOVALUE;

    /** 		while px <= length( add_public ) do*/
L4: 
    if (IS_SEQUENCE(_add_public_25390)){
            _14628 = SEQ_PTR(_add_public_25390)->length;
    }
    else {
        _14628 = 1;
    }
    if (_px_25408 > _14628)
    goto L5; // [134] 338

    /** 			include_matrix[add_public[px]][included_file] =*/
    _2 = (int)SEQ_PTR(_add_public_25390);
    _14630 = (int)*(((s1_ptr)_2)->base + _px_25408);
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_14630))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14630)->dbl));
    else
    _3 = (int)(_14630 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_add_public_25390);
    _14633 = (int)*(((s1_ptr)_2)->base + _px_25408);
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!IS_ATOM_INT(_14633)){
        _14634 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14633)->dbl));
    }
    else{
        _14634 = (int)*(((s1_ptr)_2)->base + _14633);
    }
    _2 = (int)SEQ_PTR(_14634);
    _14635 = (int)*(((s1_ptr)_2)->base + _included_file_25379);
    _14634 = NOVALUE;
    if (IS_ATOM_INT(_14635)) {
        {unsigned long tu;
             tu = (unsigned long)4 | (unsigned long)_14635;
             _14636 = MAKE_UINT(tu);
        }
    }
    else {
        _14636 = binary_op(OR_BITS, 4, _14635);
    }
    _14635 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25379);
    _1 = *(int *)_2;
    *(int *)_2 = _14636;
    if( _1 != _14636 ){
        DeRef(_1);
    }
    _14636 = NOVALUE;
    _14631 = NOVALUE;

    /** 			for i = 1 to length( file_public_by[add_public[px]] ) do*/
    _2 = (int)SEQ_PTR(_add_public_25390);
    _14637 = (int)*(((s1_ptr)_2)->base + _px_25408);
    _2 = (int)SEQ_PTR(_35file_public_by_15610);
    if (!IS_ATOM_INT(_14637)){
        _14638 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14637)->dbl));
    }
    else{
        _14638 = (int)*(((s1_ptr)_2)->base + _14637);
    }
    if (IS_SEQUENCE(_14638)){
            _14639 = SEQ_PTR(_14638)->length;
    }
    else {
        _14639 = 1;
    }
    _14638 = NOVALUE;
    {
        int _i_25425;
        _i_25425 = 1;
L6: 
        if (_i_25425 > _14639){
            goto L7; // [190] 249
        }

        /** 				if not find( file_public[add_public[px]][i], add_public ) then*/
        _2 = (int)SEQ_PTR(_add_public_25390);
        _14640 = (int)*(((s1_ptr)_2)->base + _px_25408);
        _2 = (int)SEQ_PTR(_35file_public_15606);
        if (!IS_ATOM_INT(_14640)){
            _14641 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14640)->dbl));
        }
        else{
            _14641 = (int)*(((s1_ptr)_2)->base + _14640);
        }
        _2 = (int)SEQ_PTR(_14641);
        _14642 = (int)*(((s1_ptr)_2)->base + _i_25425);
        _14641 = NOVALUE;
        _14643 = find_from(_14642, _add_public_25390, 1);
        _14642 = NOVALUE;
        if (_14643 != 0)
        goto L8; // [218] 242
        _14643 = NOVALUE;

        /** 					add_public &= file_public[add_public[px]][i]*/
        _2 = (int)SEQ_PTR(_add_public_25390);
        _14645 = (int)*(((s1_ptr)_2)->base + _px_25408);
        _2 = (int)SEQ_PTR(_35file_public_15606);
        if (!IS_ATOM_INT(_14645)){
            _14646 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14645)->dbl));
        }
        else{
            _14646 = (int)*(((s1_ptr)_2)->base + _14645);
        }
        _2 = (int)SEQ_PTR(_14646);
        _14647 = (int)*(((s1_ptr)_2)->base + _i_25425);
        _14646 = NOVALUE;
        if (IS_SEQUENCE(_add_public_25390) && IS_ATOM(_14647)) {
            Ref(_14647);
            Append(&_add_public_25390, _add_public_25390, _14647);
        }
        else if (IS_ATOM(_add_public_25390) && IS_SEQUENCE(_14647)) {
        }
        else {
            Concat((object_ptr)&_add_public_25390, _add_public_25390, _14647);
        }
        _14647 = NOVALUE;
L8: 

        /** 			end for*/
        _i_25425 = _i_25425 + 1;
        goto L6; // [244] 197
L7: 
        ;
    }

    /** 			for i = 1 to length( file_include_by[add_public[px]] ) do*/
    _2 = (int)SEQ_PTR(_add_public_25390);
    _14649 = (int)*(((s1_ptr)_2)->base + _px_25408);
    _2 = (int)SEQ_PTR(_35file_include_by_15608);
    if (!IS_ATOM_INT(_14649)){
        _14650 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14649)->dbl));
    }
    else{
        _14650 = (int)*(((s1_ptr)_2)->base + _14649);
    }
    if (IS_SEQUENCE(_14650)){
            _14651 = SEQ_PTR(_14650)->length;
    }
    else {
        _14651 = 1;
    }
    _14650 = NOVALUE;
    {
        int _i_25443;
        _i_25443 = 1;
L9: 
        if (_i_25443 > _14651){
            goto LA; // [264] 327
        }

        /** 				include_matrix[file_include_by[add_public[px]]][included_file] =*/
        _2 = (int)SEQ_PTR(_add_public_25390);
        _14652 = (int)*(((s1_ptr)_2)->base + _px_25408);
        _2 = (int)SEQ_PTR(_35file_include_by_15608);
        if (!IS_ATOM_INT(_14652)){
            _14653 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14652)->dbl));
        }
        else{
            _14653 = (int)*(((s1_ptr)_2)->base + _14652);
        }
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35include_matrix_15602 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_14653))
        _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_14653)->dbl));
        else
        _3 = (int)(_14653 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_add_public_25390);
        _14656 = (int)*(((s1_ptr)_2)->base + _px_25408);
        _2 = (int)SEQ_PTR(_35file_include_by_15608);
        if (!IS_ATOM_INT(_14656)){
            _14657 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14656)->dbl));
        }
        else{
            _14657 = (int)*(((s1_ptr)_2)->base + _14656);
        }
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!IS_ATOM_INT(_14657)){
            _14658 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_14657)->dbl));
        }
        else{
            _14658 = (int)*(((s1_ptr)_2)->base + _14657);
        }
        _2 = (int)SEQ_PTR(_14658);
        _14659 = (int)*(((s1_ptr)_2)->base + _included_file_25379);
        _14658 = NOVALUE;
        if (IS_ATOM_INT(_14659)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14659;
                 _14660 = MAKE_UINT(tu);
            }
        }
        else {
            _14660 = binary_op(OR_BITS, 4, _14659);
        }
        _14659 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _included_file_25379);
        _1 = *(int *)_2;
        *(int *)_2 = _14660;
        if( _1 != _14660 ){
            DeRef(_1);
        }
        _14660 = NOVALUE;
        _14654 = NOVALUE;

        /** 			end for*/
        _i_25443 = _i_25443 + 1;
        goto L9; // [322] 271
LA: 
        ;
    }

    /** 			px += 1*/
    _px_25408 = _px_25408 + 1;

    /** 		end while*/
    goto L4; // [335] 131
L5: 
L1: 
    DeRef(_add_public_25390);
    _add_public_25390 = NOVALUE;

    /** 	if indirect_include[from_file][included_file] then*/
    _2 = (int)SEQ_PTR(_35indirect_include_15604);
    _14662 = (int)*(((s1_ptr)_2)->base + _from_file_25380);
    _2 = (int)SEQ_PTR(_14662);
    _14663 = (int)*(((s1_ptr)_2)->base + _included_file_25379);
    _14662 = NOVALUE;
    if (_14663 == 0) {
        _14663 = NOVALUE;
        goto LB; // [353] 545
    }
    else {
        if (!IS_ATOM_INT(_14663) && DBL_PTR(_14663)->dbl == 0.0){
            _14663 = NOVALUE;
            goto LB; // [353] 545
        }
        _14663 = NOVALUE;
    }
    _14663 = NOVALUE;

    /** 		sequence indirect = file_include_by[from_file]*/
    DeRef(_indirect_25467);
    _2 = (int)SEQ_PTR(_35file_include_by_15608);
    _indirect_25467 = (int)*(((s1_ptr)_2)->base + _from_file_25380);
    Ref(_indirect_25467);

    /** 		sequence mask = include_matrix[included_file] != 0*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14665 = (int)*(((s1_ptr)_2)->base + _included_file_25379);
    DeRef(_mask_25470);
    if (IS_ATOM_INT(_14665)) {
        _mask_25470 = (_14665 != 0);
    }
    else {
        _mask_25470 = binary_op(NOTEQ, _14665, 0);
    }
    _14665 = NOVALUE;

    /** 		include_matrix[from_file] = or_bits( include_matrix[from_file], mask )*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14667 = (int)*(((s1_ptr)_2)->base + _from_file_25380);
    _14668 = binary_op(OR_BITS, _14667, _mask_25470);
    _14667 = NOVALUE;
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _from_file_25380);
    _1 = *(int *)_2;
    *(int *)_2 = _14668;
    if( _1 != _14668 ){
        DeRef(_1);
    }
    _14668 = NOVALUE;

    /** 		mask = include_matrix[from_file] != 0*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14669 = (int)*(((s1_ptr)_2)->base + _from_file_25380);
    DeRefDS(_mask_25470);
    if (IS_ATOM_INT(_14669)) {
        _mask_25470 = (_14669 != 0);
    }
    else {
        _mask_25470 = binary_op(NOTEQ, _14669, 0);
    }
    _14669 = NOVALUE;

    /** 		integer ix = 1*/
    _ix_25481 = 1;

    /** 		while ix <= length(indirect) do*/
LC: 
    if (IS_SEQUENCE(_indirect_25467)){
            _14671 = SEQ_PTR(_indirect_25467)->length;
    }
    else {
        _14671 = 1;
    }
    if (_ix_25481 > _14671)
    goto LD; // [425] 544

    /** 			integer indirect_file = indirect[ix]*/
    _2 = (int)SEQ_PTR(_indirect_25467);
    _indirect_file_25485 = (int)*(((s1_ptr)_2)->base + _ix_25481);
    if (!IS_ATOM_INT(_indirect_file_25485))
    _indirect_file_25485 = (long)DBL_PTR(_indirect_file_25485)->dbl;

    /** 			if indirect_include[indirect_file][included_file] then*/
    _2 = (int)SEQ_PTR(_35indirect_include_15604);
    _14674 = (int)*(((s1_ptr)_2)->base + _indirect_file_25485);
    _2 = (int)SEQ_PTR(_14674);
    _14675 = (int)*(((s1_ptr)_2)->base + _included_file_25379);
    _14674 = NOVALUE;
    if (_14675 == 0) {
        _14675 = NOVALUE;
        goto LE; // [447] 531
    }
    else {
        if (!IS_ATOM_INT(_14675) && DBL_PTR(_14675)->dbl == 0.0){
            _14675 = NOVALUE;
            goto LE; // [447] 531
        }
        _14675 = NOVALUE;
    }
    _14675 = NOVALUE;

    /** 				include_matrix[indirect_file] =*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14676 = (int)*(((s1_ptr)_2)->base + _indirect_file_25485);
    _14677 = binary_op(OR_BITS, _mask_25470, _14676);
    _14676 = NOVALUE;
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _indirect_file_25485);
    _1 = *(int *)_2;
    *(int *)_2 = _14677;
    if( _1 != _14677 ){
        DeRef(_1);
    }
    _14677 = NOVALUE;

    /** 				for i = 1 to length( file_include_by[indirect_file] ) do*/
    _2 = (int)SEQ_PTR(_35file_include_by_15608);
    _14678 = (int)*(((s1_ptr)_2)->base + _indirect_file_25485);
    if (IS_SEQUENCE(_14678)){
            _14679 = SEQ_PTR(_14678)->length;
    }
    else {
        _14679 = 1;
    }
    _14678 = NOVALUE;
    {
        int _i_25496;
        _i_25496 = 1;
LF: 
        if (_i_25496 > _14679){
            goto L10; // [479] 530
        }

        /** 					if not find( file_include_by[indirect_file][i], indirect ) then*/
        _2 = (int)SEQ_PTR(_35file_include_by_15608);
        _14680 = (int)*(((s1_ptr)_2)->base + _indirect_file_25485);
        _2 = (int)SEQ_PTR(_14680);
        _14681 = (int)*(((s1_ptr)_2)->base + _i_25496);
        _14680 = NOVALUE;
        _14682 = find_from(_14681, _indirect_25467, 1);
        _14681 = NOVALUE;
        if (_14682 != 0)
        goto L11; // [503] 523
        _14682 = NOVALUE;

        /** 						indirect &= file_include_by[indirect_file][i]*/
        _2 = (int)SEQ_PTR(_35file_include_by_15608);
        _14684 = (int)*(((s1_ptr)_2)->base + _indirect_file_25485);
        _2 = (int)SEQ_PTR(_14684);
        _14685 = (int)*(((s1_ptr)_2)->base + _i_25496);
        _14684 = NOVALUE;
        if (IS_SEQUENCE(_indirect_25467) && IS_ATOM(_14685)) {
            Ref(_14685);
            Append(&_indirect_25467, _indirect_25467, _14685);
        }
        else if (IS_ATOM(_indirect_25467) && IS_SEQUENCE(_14685)) {
        }
        else {
            Concat((object_ptr)&_indirect_25467, _indirect_25467, _14685);
        }
        _14685 = NOVALUE;
L11: 

        /** 				end for*/
        _i_25496 = _i_25496 + 1;
        goto LF; // [525] 486
L10: 
        ;
    }
LE: 

    /** 			ix += 1*/
    _ix_25481 = _ix_25481 + 1;

    /** 		end while*/
    goto LC; // [541] 422
LD: 
LB: 
    DeRef(_indirect_25467);
    _indirect_25467 = NOVALUE;
    DeRef(_mask_25470);
    _mask_25470 = NOVALUE;

    /** 	public_include = FALSE*/
    _62public_include_24626 = _9FALSE_426;

    /** end procedure*/
    _14618 = NOVALUE;
    _14630 = NOVALUE;
    _14621 = NOVALUE;
    _14637 = NOVALUE;
    _14633 = NOVALUE;
    _14638 = NOVALUE;
    _14640 = NOVALUE;
    _14645 = NOVALUE;
    _14649 = NOVALUE;
    _14650 = NOVALUE;
    _14652 = NOVALUE;
    _14653 = NOVALUE;
    _14678 = NOVALUE;
    _14656 = NOVALUE;
    _14657 = NOVALUE;
    return;
    ;
}


void _62add_include_by(int _by_file_25514, int _included_file_25515, int _is_public_25516)
{
    int _14732 = NOVALUE;
    int _14731 = NOVALUE;
    int _14730 = NOVALUE;
    int _14728 = NOVALUE;
    int _14727 = NOVALUE;
    int _14726 = NOVALUE;
    int _14725 = NOVALUE;
    int _14723 = NOVALUE;
    int _14722 = NOVALUE;
    int _14721 = NOVALUE;
    int _14720 = NOVALUE;
    int _14719 = NOVALUE;
    int _14718 = NOVALUE;
    int _14717 = NOVALUE;
    int _14716 = NOVALUE;
    int _14714 = NOVALUE;
    int _14713 = NOVALUE;
    int _14712 = NOVALUE;
    int _14711 = NOVALUE;
    int _14709 = NOVALUE;
    int _14708 = NOVALUE;
    int _14707 = NOVALUE;
    int _14706 = NOVALUE;
    int _14704 = NOVALUE;
    int _14703 = NOVALUE;
    int _14702 = NOVALUE;
    int _14701 = NOVALUE;
    int _14699 = NOVALUE;
    int _14698 = NOVALUE;
    int _14697 = NOVALUE;
    int _14696 = NOVALUE;
    int _14695 = NOVALUE;
    int _14693 = NOVALUE;
    int _14692 = NOVALUE;
    int _14691 = NOVALUE;
    int _14690 = NOVALUE;
    int _14688 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	include_matrix[by_file][included_file] = or_bits( DIRECT_INCLUDE, include_matrix[by_file][included_file] )*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    _3 = (int)(_by_file_25514 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14690 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
    _2 = (int)SEQ_PTR(_14690);
    _14691 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
    _14690 = NOVALUE;
    if (IS_ATOM_INT(_14691)) {
        {unsigned long tu;
             tu = (unsigned long)2 | (unsigned long)_14691;
             _14692 = MAKE_UINT(tu);
        }
    }
    else {
        _14692 = binary_op(OR_BITS, 2, _14691);
    }
    _14691 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25515);
    _1 = *(int *)_2;
    *(int *)_2 = _14692;
    if( _1 != _14692 ){
        DeRef(_1);
    }
    _14692 = NOVALUE;
    _14688 = NOVALUE;

    /** 	if is_public then*/
    if (_is_public_25516 == 0)
    {
        goto L1; // [38] 71
    }
    else{
    }

    /** 		include_matrix[by_file][included_file] = or_bits( PUBLIC_INCLUDE, include_matrix[by_file][included_file] )*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    _3 = (int)(_by_file_25514 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14695 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
    _2 = (int)SEQ_PTR(_14695);
    _14696 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
    _14695 = NOVALUE;
    if (IS_ATOM_INT(_14696)) {
        {unsigned long tu;
             tu = (unsigned long)4 | (unsigned long)_14696;
             _14697 = MAKE_UINT(tu);
        }
    }
    else {
        _14697 = binary_op(OR_BITS, 4, _14696);
    }
    _14696 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25515);
    _1 = *(int *)_2;
    *(int *)_2 = _14697;
    if( _1 != _14697 ){
        DeRef(_1);
    }
    _14697 = NOVALUE;
    _14693 = NOVALUE;
L1: 

    /** 	if not find( by_file, file_include_by[included_file] ) then*/
    _2 = (int)SEQ_PTR(_35file_include_by_15608);
    _14698 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
    _14699 = find_from(_by_file_25514, _14698, 1);
    _14698 = NOVALUE;
    if (_14699 != 0)
    goto L2; // [84] 104
    _14699 = NOVALUE;

    /** 		file_include_by[included_file] &= by_file*/
    _2 = (int)SEQ_PTR(_35file_include_by_15608);
    _14701 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
    if (IS_SEQUENCE(_14701) && IS_ATOM(_by_file_25514)) {
        Append(&_14702, _14701, _by_file_25514);
    }
    else if (IS_ATOM(_14701) && IS_SEQUENCE(_by_file_25514)) {
    }
    else {
        Concat((object_ptr)&_14702, _14701, _by_file_25514);
        _14701 = NOVALUE;
    }
    _14701 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_include_by_15608);
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25515);
    _1 = *(int *)_2;
    *(int *)_2 = _14702;
    if( _1 != _14702 ){
        DeRef(_1);
    }
    _14702 = NOVALUE;
L2: 

    /** 	if not find( included_file, file_include[by_file] ) then*/
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _14703 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
    _14704 = find_from(_included_file_25515, _14703, 1);
    _14703 = NOVALUE;
    if (_14704 != 0)
    goto L3; // [117] 137
    _14704 = NOVALUE;

    /** 		file_include[by_file] &= included_file*/
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _14706 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
    if (IS_SEQUENCE(_14706) && IS_ATOM(_included_file_25515)) {
        Append(&_14707, _14706, _included_file_25515);
    }
    else if (IS_ATOM(_14706) && IS_SEQUENCE(_included_file_25515)) {
    }
    else {
        Concat((object_ptr)&_14707, _14706, _included_file_25515);
        _14706 = NOVALUE;
    }
    _14706 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_include_15600);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35file_include_15600 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _by_file_25514);
    _1 = *(int *)_2;
    *(int *)_2 = _14707;
    if( _1 != _14707 ){
        DeRef(_1);
    }
    _14707 = NOVALUE;
L3: 

    /** 	if is_public then*/
    if (_is_public_25516 == 0)
    {
        goto L4; // [139] 209
    }
    else{
    }

    /** 		if not find( by_file, file_public_by[included_file] ) then*/
    _2 = (int)SEQ_PTR(_35file_public_by_15610);
    _14708 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
    _14709 = find_from(_by_file_25514, _14708, 1);
    _14708 = NOVALUE;
    if (_14709 != 0)
    goto L5; // [155] 175
    _14709 = NOVALUE;

    /** 			file_public_by[included_file] &= by_file*/
    _2 = (int)SEQ_PTR(_35file_public_by_15610);
    _14711 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
    if (IS_SEQUENCE(_14711) && IS_ATOM(_by_file_25514)) {
        Append(&_14712, _14711, _by_file_25514);
    }
    else if (IS_ATOM(_14711) && IS_SEQUENCE(_by_file_25514)) {
    }
    else {
        Concat((object_ptr)&_14712, _14711, _by_file_25514);
        _14711 = NOVALUE;
    }
    _14711 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_public_by_15610);
    _2 = (int)(((s1_ptr)_2)->base + _included_file_25515);
    _1 = *(int *)_2;
    *(int *)_2 = _14712;
    if( _1 != _14712 ){
        DeRef(_1);
    }
    _14712 = NOVALUE;
L5: 

    /** 		if not find( included_file, file_public[by_file] ) then*/
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _14713 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
    _14714 = find_from(_included_file_25515, _14713, 1);
    _14713 = NOVALUE;
    if (_14714 != 0)
    goto L6; // [188] 208
    _14714 = NOVALUE;

    /** 			file_public[by_file] &= included_file*/
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _14716 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
    if (IS_SEQUENCE(_14716) && IS_ATOM(_included_file_25515)) {
        Append(&_14717, _14716, _included_file_25515);
    }
    else if (IS_ATOM(_14716) && IS_SEQUENCE(_included_file_25515)) {
    }
    else {
        Concat((object_ptr)&_14717, _14716, _included_file_25515);
        _14716 = NOVALUE;
    }
    _14716 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _2 = (int)(((s1_ptr)_2)->base + _by_file_25514);
    _1 = *(int *)_2;
    *(int *)_2 = _14717;
    if( _1 != _14717 ){
        DeRef(_1);
    }
    _14717 = NOVALUE;
L6: 
L4: 

    /** 	for propagate = 1 to length( include_matrix[included_file] ) do*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _14718 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
    if (IS_SEQUENCE(_14718)){
            _14719 = SEQ_PTR(_14718)->length;
    }
    else {
        _14719 = 1;
    }
    _14718 = NOVALUE;
    {
        int _propagate_25568;
        _propagate_25568 = 1;
L7: 
        if (_propagate_25568 > _14719){
            goto L8; // [220] 320
        }

        /** 		if and_bits( PUBLIC_INCLUDE, include_matrix[included_file][propagate] ) then*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _14720 = (int)*(((s1_ptr)_2)->base + _included_file_25515);
        _2 = (int)SEQ_PTR(_14720);
        _14721 = (int)*(((s1_ptr)_2)->base + _propagate_25568);
        _14720 = NOVALUE;
        if (IS_ATOM_INT(_14721)) {
            {unsigned long tu;
                 tu = (unsigned long)4 & (unsigned long)_14721;
                 _14722 = MAKE_UINT(tu);
            }
        }
        else {
            _14722 = binary_op(AND_BITS, 4, _14721);
        }
        _14721 = NOVALUE;
        if (_14722 == 0) {
            DeRef(_14722);
            _14722 = NOVALUE;
            goto L9; // [245] 313
        }
        else {
            if (!IS_ATOM_INT(_14722) && DBL_PTR(_14722)->dbl == 0.0){
                DeRef(_14722);
                _14722 = NOVALUE;
                goto L9; // [245] 313
            }
            DeRef(_14722);
            _14722 = NOVALUE;
        }
        DeRef(_14722);
        _14722 = NOVALUE;

        /** 			include_matrix[by_file][propagate] = or_bits( DIRECT_INCLUDE, include_matrix[by_file][propagate] )*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35include_matrix_15602 = MAKE_SEQ(_2);
        }
        _3 = (int)(_by_file_25514 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _14725 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
        _2 = (int)SEQ_PTR(_14725);
        _14726 = (int)*(((s1_ptr)_2)->base + _propagate_25568);
        _14725 = NOVALUE;
        if (IS_ATOM_INT(_14726)) {
            {unsigned long tu;
                 tu = (unsigned long)2 | (unsigned long)_14726;
                 _14727 = MAKE_UINT(tu);
            }
        }
        else {
            _14727 = binary_op(OR_BITS, 2, _14726);
        }
        _14726 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _propagate_25568);
        _1 = *(int *)_2;
        *(int *)_2 = _14727;
        if( _1 != _14727 ){
            DeRef(_1);
        }
        _14727 = NOVALUE;
        _14723 = NOVALUE;

        /** 			if is_public then*/
        if (_is_public_25516 == 0)
        {
            goto LA; // [279] 312
        }
        else{
        }

        /** 				include_matrix[by_file][propagate] = or_bits( PUBLIC_INCLUDE, include_matrix[by_file][propagate] )*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35include_matrix_15602 = MAKE_SEQ(_2);
        }
        _3 = (int)(_by_file_25514 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _14730 = (int)*(((s1_ptr)_2)->base + _by_file_25514);
        _2 = (int)SEQ_PTR(_14730);
        _14731 = (int)*(((s1_ptr)_2)->base + _propagate_25568);
        _14730 = NOVALUE;
        if (IS_ATOM_INT(_14731)) {
            {unsigned long tu;
                 tu = (unsigned long)4 | (unsigned long)_14731;
                 _14732 = MAKE_UINT(tu);
            }
        }
        else {
            _14732 = binary_op(OR_BITS, 4, _14731);
        }
        _14731 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _propagate_25568);
        _1 = *(int *)_2;
        *(int *)_2 = _14732;
        if( _1 != _14732 ){
            DeRef(_1);
        }
        _14732 = NOVALUE;
        _14728 = NOVALUE;
LA: 
L9: 

        /** 	end for*/
        _propagate_25568 = _propagate_25568 + 1;
        goto L7; // [315] 227
L8: 
        ;
    }

    /** end procedure*/
    _14718 = NOVALUE;
    return;
    ;
}


void _62IncludePush()
{
    int _new_file_handle_25597 = NOVALUE;
    int _old_file_no_25598 = NOVALUE;
    int _new_hash_25599 = NOVALUE;
    int _idx_25600 = NOVALUE;
    int _14819 = NOVALUE;
    int _14816 = NOVALUE;
    int _14814 = NOVALUE;
    int _14813 = NOVALUE;
    int _14812 = NOVALUE;
    int _14810 = NOVALUE;
    int _14809 = NOVALUE;
    int _14803 = NOVALUE;
    int _14802 = NOVALUE;
    int _14801 = NOVALUE;
    int _14800 = NOVALUE;
    int _14799 = NOVALUE;
    int _14798 = NOVALUE;
    int _14797 = NOVALUE;
    int _14794 = NOVALUE;
    int _14792 = NOVALUE;
    int _14790 = NOVALUE;
    int _14789 = NOVALUE;
    int _14788 = NOVALUE;
    int _14786 = NOVALUE;
    int _14785 = NOVALUE;
    int _14783 = NOVALUE;
    int _14782 = NOVALUE;
    int _14780 = NOVALUE;
    int _14779 = NOVALUE;
    int _14778 = NOVALUE;
    int _14777 = NOVALUE;
    int _14776 = NOVALUE;
    int _14775 = NOVALUE;
    int _14774 = NOVALUE;
    int _14770 = NOVALUE;
    int _14768 = NOVALUE;
    int _14767 = NOVALUE;
    int _14766 = NOVALUE;
    int _14765 = NOVALUE;
    int _14764 = NOVALUE;
    int _14763 = NOVALUE;
    int _14762 = NOVALUE;
    int _14761 = NOVALUE;
    int _14760 = NOVALUE;
    int _14758 = NOVALUE;
    int _14757 = NOVALUE;
    int _14756 = NOVALUE;
    int _14754 = NOVALUE;
    int _14753 = NOVALUE;
    int _14752 = NOVALUE;
    int _14751 = NOVALUE;
    int _14749 = NOVALUE;
    int _14748 = NOVALUE;
    int _14747 = NOVALUE;
    int _14746 = NOVALUE;
    int _14745 = NOVALUE;
    int _14743 = NOVALUE;
    int _14742 = NOVALUE;
    int _14741 = NOVALUE;
    int _14740 = NOVALUE;
    int _14738 = NOVALUE;
    int _14734 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	start_include = FALSE*/
    _62start_include_24623 = _9FALSE_426;

    /** 	new_file_handle = path_open() -- sets new_include_name to full path*/
    _new_file_handle_25597 = _62path_open();
    if (!IS_ATOM_INT(_new_file_handle_25597)) {
        _1 = (long)(DBL_PTR(_new_file_handle_25597)->dbl);
        if (UNIQUE(DBL_PTR(_new_file_handle_25597)) && (DBL_PTR(_new_file_handle_25597)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_file_handle_25597);
        _new_file_handle_25597 = _1;
    }

    /** 	new_hash = hash(canonical_path(new_include_name,,CORRECT), stdhash:HSIEH32)*/
    RefDS(_38new_include_name_17088);
    _14734 = _13canonical_path(_38new_include_name_17088, 0, 2);
    DeRef(_new_hash_25599);
    _new_hash_25599 = calc_hash(_14734, -5);
    DeRef(_14734);
    _14734 = NOVALUE;

    /** 	idx = find(new_hash, known_files_hash)*/
    _idx_25600 = find_from(_new_hash_25599, _35known_files_hash_15597, 1);

    /** 	if idx then*/
    if (_idx_25600 == 0)
    {
        goto L1; // [44] 337
    }
    else{
    }

    /** 		if new_include_space != 0 then*/
    if (_62new_include_space_24621 == 0)
    goto L2; // [51] 73

    /** 			SymTab[new_include_space][S_OBJ] = idx -- but note any namespace*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_62new_include_space_24621 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _idx_25600;
    DeRef(_1);
    _14738 = NOVALUE;
L2: 

    /** 		close(new_file_handle)*/
    EClose(_new_file_handle_25597);

    /** 		if find( -idx, file_include[current_file_no] ) then*/
    if ((unsigned long)_idx_25600 == 0xC0000000)
    _14740 = (int)NewDouble((double)-0xC0000000);
    else
    _14740 = - _idx_25600;
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _14741 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _14742 = find_from(_14740, _14741, 1);
    DeRef(_14740);
    _14740 = NOVALUE;
    _14741 = NOVALUE;
    if (_14742 == 0)
    {
        _14742 = NOVALUE;
        goto L3; // [95] 132
    }
    else{
        _14742 = NOVALUE;
    }

    /** 			file_include[current_file_no][ find( -idx, file_include[current_file_no] ) ] = idx*/
    _2 = (int)SEQ_PTR(_35file_include_15600);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35file_include_15600 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38current_file_no_16946 + ((s1_ptr)_2)->base);
    if ((unsigned long)_idx_25600 == 0xC0000000)
    _14745 = (int)NewDouble((double)-0xC0000000);
    else
    _14745 = - _idx_25600;
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _14746 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _14747 = find_from(_14745, _14746, 1);
    DeRef(_14745);
    _14745 = NOVALUE;
    _14746 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14747);
    _1 = *(int *)_2;
    *(int *)_2 = _idx_25600;
    DeRef(_1);
    _14743 = NOVALUE;
    goto L4; // [129] 230
L3: 

    /** 		elsif not find( idx, file_include[current_file_no] ) then*/
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _14748 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _14749 = find_from(_idx_25600, _14748, 1);
    _14748 = NOVALUE;
    if (_14749 != 0)
    goto L5; // [147] 229
    _14749 = NOVALUE;

    /** 			file_include[current_file_no] &= idx*/
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _14751 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_14751) && IS_ATOM(_idx_25600)) {
        Append(&_14752, _14751, _idx_25600);
    }
    else if (IS_ATOM(_14751) && IS_SEQUENCE(_idx_25600)) {
    }
    else {
        Concat((object_ptr)&_14752, _14751, _idx_25600);
        _14751 = NOVALUE;
    }
    _14751 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_include_15600);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35file_include_15600 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _14752;
    if( _1 != _14752 ){
        DeRef(_1);
    }
    _14752 = NOVALUE;

    /** 			add_exports( idx, current_file_no )*/
    _62add_exports(_idx_25600, _38current_file_no_16946);

    /** 			if public_include then*/
    if (_62public_include_24626 == 0)
    {
        goto L6; // [180] 228
    }
    else{
    }

    /** 				if not find( idx, file_public[current_file_no] ) then*/
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _14753 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _14754 = find_from(_idx_25600, _14753, 1);
    _14753 = NOVALUE;
    if (_14754 != 0)
    goto L7; // [198] 227
    _14754 = NOVALUE;

    /** 					file_public[current_file_no] &= idx*/
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _14756 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_14756) && IS_ATOM(_idx_25600)) {
        Append(&_14757, _14756, _idx_25600);
    }
    else if (IS_ATOM(_14756) && IS_SEQUENCE(_idx_25600)) {
    }
    else {
        Concat((object_ptr)&_14757, _14756, _idx_25600);
        _14756 = NOVALUE;
    }
    _14756 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _14757;
    if( _1 != _14757 ){
        DeRef(_1);
    }
    _14757 = NOVALUE;

    /** 					patch_exports( current_file_no )*/
    _62patch_exports(_38current_file_no_16946);
L7: 
L6: 
L5: 
L4: 

    /** 		indirect_include[current_file_no][idx] = OpIndirectInclude*/
    _2 = (int)SEQ_PTR(_35indirect_include_15604);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35indirect_include_15604 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38current_file_no_16946 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _idx_25600);
    _1 = *(int *)_2;
    *(int *)_2 = _38OpIndirectInclude_17024;
    DeRef(_1);
    _14758 = NOVALUE;

    /** 		add_include_by( current_file_no, idx, public_include )*/
    _62add_include_by(_38current_file_no_16946, _idx_25600, _62public_include_24626);

    /** 		update_include_matrix( idx, current_file_no )*/
    _62update_include_matrix(_idx_25600, _38current_file_no_16946);

    /** 		public_include = FALSE*/
    _62public_include_24626 = _9FALSE_426;

    /** 		read_line() -- we can't return without reading a line first*/
    _62read_line();

    /** 		if not find( idx, file_include_depend[current_file_no] ) and not finished_files[idx] then*/
    _2 = (int)SEQ_PTR(_35file_include_depend_15599);
    _14760 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _14761 = find_from(_idx_25600, _14760, 1);
    _14760 = NOVALUE;
    _14762 = (_14761 == 0);
    _14761 = NOVALUE;
    if (_14762 == 0) {
        goto L8; // [295] 331
    }
    _2 = (int)SEQ_PTR(_35finished_files_15598);
    _14764 = (int)*(((s1_ptr)_2)->base + _idx_25600);
    _14765 = (_14764 == 0);
    _14764 = NOVALUE;
    if (_14765 == 0)
    {
        DeRef(_14765);
        _14765 = NOVALUE;
        goto L8; // [309] 331
    }
    else{
        DeRef(_14765);
        _14765 = NOVALUE;
    }

    /** 			file_include_depend[current_file_no] &= idx*/
    _2 = (int)SEQ_PTR(_35file_include_depend_15599);
    _14766 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_14766) && IS_ATOM(_idx_25600)) {
        Append(&_14767, _14766, _idx_25600);
    }
    else if (IS_ATOM(_14766) && IS_SEQUENCE(_idx_25600)) {
    }
    else {
        Concat((object_ptr)&_14767, _14766, _idx_25600);
        _14766 = NOVALUE;
    }
    _14766 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_include_depend_15599);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35file_include_depend_15599 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _14767;
    if( _1 != _14767 ){
        DeRef(_1);
    }
    _14767 = NOVALUE;
L8: 

    /** 		return -- ignore it*/
    DeRef(_new_hash_25599);
    DeRef(_14762);
    _14762 = NOVALUE;
    return;
L1: 

    /** 	if length(IncludeStk) >= INCLUDE_LIMIT then*/
    if (IS_SEQUENCE(_62IncludeStk_24632)){
            _14768 = SEQ_PTR(_62IncludeStk_24632)->length;
    }
    else {
        _14768 = 1;
    }
    if (_14768 < 30)
    goto L9; // [344] 356

    /** 		CompileErr(104)*/
    RefDS(_22663);
    _46CompileErr(104, _22663, 0);
L9: 

    /** 	IncludeStk = append(IncludeStk,*/
    _1 = NewS1(22);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _38current_file_no_16946;
    *((int *)(_2+8)) = _38line_number_16947;
    *((int *)(_2+12)) = _38src_file_17087;
    *((int *)(_2+16)) = _38file_start_sym_16952;
    *((int *)(_2+20)) = _38OpWarning_17013;
    *((int *)(_2+24)) = _38OpTrace_17015;
    *((int *)(_2+28)) = _38OpTypeCheck_17016;
    *((int *)(_2+32)) = _38OpProfileTime_17018;
    *((int *)(_2+36)) = _38OpProfileStatement_17017;
    RefDS(_38OpDefines_17019);
    *((int *)(_2+40)) = _38OpDefines_17019;
    *((int *)(_2+44)) = _38prev_OpWarning_17014;
    *((int *)(_2+48)) = _38OpInline_17023;
    *((int *)(_2+52)) = _38OpIndirectInclude_17024;
    *((int *)(_2+56)) = _38putback_fwd_line_number_16949;
    Ref(_46putback_ForwardLine_49484);
    *((int *)(_2+60)) = _46putback_ForwardLine_49484;
    *((int *)(_2+64)) = _46putback_forward_bp_49488;
    *((int *)(_2+68)) = _38last_fwd_line_number_16950;
    Ref(_46last_ForwardLine_49485);
    *((int *)(_2+72)) = _46last_ForwardLine_49485;
    *((int *)(_2+76)) = _46last_forward_bp_49489;
    Ref(_46ThisLine_49482);
    *((int *)(_2+80)) = _46ThisLine_49482;
    *((int *)(_2+84)) = _38fwd_line_number_16948;
    *((int *)(_2+88)) = _46forward_bp_49487;
    _14770 = MAKE_SEQ(_1);
    RefDS(_14770);
    Append(&_62IncludeStk_24632, _62IncludeStk_24632, _14770);
    DeRefDS(_14770);
    _14770 = NOVALUE;

    /** 	file_include = append( file_include, {} )*/
    RefDS(_5);
    Append(&_35file_include_15600, _35file_include_15600, _5);

    /** 	file_include_by = append( file_include_by, {} )*/
    RefDS(_5);
    Append(&_35file_include_by_15608, _35file_include_by_15608, _5);

    /** 	for i = 1 to length( include_matrix) do*/
    if (IS_SEQUENCE(_35include_matrix_15602)){
            _14774 = SEQ_PTR(_35include_matrix_15602)->length;
    }
    else {
        _14774 = 1;
    }
    {
        int _i_25712;
        _i_25712 = 1;
LA: 
        if (_i_25712 > _14774){
            goto LB; // [460] 506
        }

        /** 		include_matrix[i]   &= 0*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _14775 = (int)*(((s1_ptr)_2)->base + _i_25712);
        if (IS_SEQUENCE(_14775) && IS_ATOM(0)) {
            Append(&_14776, _14775, 0);
        }
        else if (IS_ATOM(_14775) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_14776, _14775, 0);
            _14775 = NOVALUE;
        }
        _14775 = NOVALUE;
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35include_matrix_15602 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_25712);
        _1 = *(int *)_2;
        *(int *)_2 = _14776;
        if( _1 != _14776 ){
            DeRef(_1);
        }
        _14776 = NOVALUE;

        /** 		indirect_include[i] &= 0*/
        _2 = (int)SEQ_PTR(_35indirect_include_15604);
        _14777 = (int)*(((s1_ptr)_2)->base + _i_25712);
        if (IS_SEQUENCE(_14777) && IS_ATOM(0)) {
            Append(&_14778, _14777, 0);
        }
        else if (IS_ATOM(_14777) && IS_SEQUENCE(0)) {
        }
        else {
            Concat((object_ptr)&_14778, _14777, 0);
            _14777 = NOVALUE;
        }
        _14777 = NOVALUE;
        _2 = (int)SEQ_PTR(_35indirect_include_15604);
        _2 = (int)(((s1_ptr)_2)->base + _i_25712);
        _1 = *(int *)_2;
        *(int *)_2 = _14778;
        if( _1 != _14778 ){
            DeRef(_1);
        }
        _14778 = NOVALUE;

        /** 	end for*/
        _i_25712 = _i_25712 + 1;
        goto LA; // [501] 467
LB: 
        ;
    }

    /** 	include_matrix = append( include_matrix, repeat( 0, length( file_include ) ) )*/
    if (IS_SEQUENCE(_35file_include_15600)){
            _14779 = SEQ_PTR(_35file_include_15600)->length;
    }
    else {
        _14779 = 1;
    }
    _14780 = Repeat(0, _14779);
    _14779 = NOVALUE;
    RefDS(_14780);
    Append(&_35include_matrix_15602, _35include_matrix_15602, _14780);
    DeRefDS(_14780);
    _14780 = NOVALUE;

    /** 	include_matrix[$][$] = DIRECT_INCLUDE*/
    if (IS_SEQUENCE(_35include_matrix_15602)){
            _14782 = SEQ_PTR(_35include_matrix_15602)->length;
    }
    else {
        _14782 = 1;
    }
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    _3 = (int)(_14782 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(*(object_ptr)_3)){
            _14785 = SEQ_PTR(*(object_ptr)_3)->length;
    }
    else {
        _14785 = 1;
    }
    _14783 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14785);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _14783 = NOVALUE;

    /** 	include_matrix[current_file_no][$] = DIRECT_INCLUDE*/
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35include_matrix_15602 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38current_file_no_16946 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(*(object_ptr)_3)){
            _14788 = SEQ_PTR(*(object_ptr)_3)->length;
    }
    else {
        _14788 = 1;
    }
    _14786 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14788);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _14786 = NOVALUE;

    /** 	indirect_include = append( indirect_include, repeat( 0, length( file_include ) ) )*/
    if (IS_SEQUENCE(_35file_include_15600)){
            _14789 = SEQ_PTR(_35file_include_15600)->length;
    }
    else {
        _14789 = 1;
    }
    _14790 = Repeat(0, _14789);
    _14789 = NOVALUE;
    RefDS(_14790);
    Append(&_35indirect_include_15604, _35indirect_include_15604, _14790);
    DeRefDS(_14790);
    _14790 = NOVALUE;

    /** 	indirect_include[current_file_no][$] = OpIndirectInclude*/
    _2 = (int)SEQ_PTR(_35indirect_include_15604);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35indirect_include_15604 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38current_file_no_16946 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(*(object_ptr)_3)){
            _14794 = SEQ_PTR(*(object_ptr)_3)->length;
    }
    else {
        _14794 = 1;
    }
    _14792 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _14794);
    _1 = *(int *)_2;
    *(int *)_2 = _38OpIndirectInclude_17024;
    DeRef(_1);
    _14792 = NOVALUE;

    /** 	OpIndirectInclude = 1*/
    _38OpIndirectInclude_17024 = 1;

    /** 	file_public  = append( file_public, {} )*/
    RefDS(_5);
    Append(&_35file_public_15606, _35file_public_15606, _5);

    /** 	file_public_by = append( file_public_by, {} )*/
    RefDS(_5);
    Append(&_35file_public_by_15610, _35file_public_by_15610, _5);

    /** 	file_include[current_file_no] &= length( file_include )*/
    if (IS_SEQUENCE(_35file_include_15600)){
            _14797 = SEQ_PTR(_35file_include_15600)->length;
    }
    else {
        _14797 = 1;
    }
    _2 = (int)SEQ_PTR(_35file_include_15600);
    _14798 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_14798) && IS_ATOM(_14797)) {
        Append(&_14799, _14798, _14797);
    }
    else if (IS_ATOM(_14798) && IS_SEQUENCE(_14797)) {
    }
    else {
        Concat((object_ptr)&_14799, _14798, _14797);
        _14798 = NOVALUE;
    }
    _14798 = NOVALUE;
    _14797 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_include_15600);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35file_include_15600 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _14799;
    if( _1 != _14799 ){
        DeRef(_1);
    }
    _14799 = NOVALUE;

    /** 	add_include_by( current_file_no, length(file_include), public_include )*/
    if (IS_SEQUENCE(_35file_include_15600)){
            _14800 = SEQ_PTR(_35file_include_15600)->length;
    }
    else {
        _14800 = 1;
    }
    _62add_include_by(_38current_file_no_16946, _14800, _62public_include_24626);
    _14800 = NOVALUE;

    /** 	if public_include then*/
    if (_62public_include_24626 == 0)
    {
        goto LC; // [675] 709
    }
    else{
    }

    /** 		file_public[current_file_no] &= length( file_public )*/
    if (IS_SEQUENCE(_35file_public_15606)){
            _14801 = SEQ_PTR(_35file_public_15606)->length;
    }
    else {
        _14801 = 1;
    }
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _14802 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_14802) && IS_ATOM(_14801)) {
        Append(&_14803, _14802, _14801);
    }
    else if (IS_ATOM(_14802) && IS_SEQUENCE(_14801)) {
    }
    else {
        Concat((object_ptr)&_14803, _14802, _14801);
        _14802 = NOVALUE;
    }
    _14802 = NOVALUE;
    _14801 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_public_15606);
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _14803;
    if( _1 != _14803 ){
        DeRef(_1);
    }
    _14803 = NOVALUE;

    /** 		patch_exports( current_file_no )*/
    _62patch_exports(_38current_file_no_16946);
LC: 

    /** ifdef STDDEBUG then*/

    /** 	src_file = new_file_handle*/
    _38src_file_17087 = _new_file_handle_25597;

    /** 	file_start_sym = last_sym*/
    _38file_start_sym_16952 = _55last_sym_47064;

    /** 	if current_file_no >= MAX_FILE then*/
    if (_38current_file_no_16946 < 256)
    goto LD; // [731] 743

    /** 		CompileErr(126)*/
    RefDS(_22663);
    _46CompileErr(126, _22663, 0);
LD: 

    /** 	known_files = append(known_files, new_include_name)*/
    RefDS(_38new_include_name_17088);
    Append(&_35known_files_15596, _35known_files_15596, _38new_include_name_17088);

    /** 	known_files_hash &= new_hash*/
    Ref(_new_hash_25599);
    Append(&_35known_files_hash_15597, _35known_files_hash_15597, _new_hash_25599);

    /** 	finished_files &= 0*/
    Append(&_35finished_files_15598, _35finished_files_15598, 0);

    /** 	file_include_depend = append( file_include_depend, { length( known_files ) } )*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _14809 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _14809 = 1;
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _14809;
    _14810 = MAKE_SEQ(_1);
    _14809 = NOVALUE;
    RefDS(_14810);
    Append(&_35file_include_depend_15599, _35file_include_depend_15599, _14810);
    DeRefDS(_14810);
    _14810 = NOVALUE;

    /** 	file_include_depend[current_file_no] &= length( known_files )*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _14812 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _14812 = 1;
    }
    _2 = (int)SEQ_PTR(_35file_include_depend_15599);
    _14813 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    if (IS_SEQUENCE(_14813) && IS_ATOM(_14812)) {
        Append(&_14814, _14813, _14812);
    }
    else if (IS_ATOM(_14813) && IS_SEQUENCE(_14812)) {
    }
    else {
        Concat((object_ptr)&_14814, _14813, _14812);
        _14813 = NOVALUE;
    }
    _14813 = NOVALUE;
    _14812 = NOVALUE;
    _2 = (int)SEQ_PTR(_35file_include_depend_15599);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35file_include_depend_15599 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = *(int *)_2;
    *(int *)_2 = _14814;
    if( _1 != _14814 ){
        DeRef(_1);
    }
    _14814 = NOVALUE;

    /** 	check_coverage()*/
    _52check_coverage();

    /** 	default_namespaces &= 0*/
    Append(&_62default_namespaces_24629, _62default_namespaces_24629, 0);

    /** 	update_include_matrix( length( file_include ), current_file_no )*/
    if (IS_SEQUENCE(_35file_include_15600)){
            _14816 = SEQ_PTR(_35file_include_15600)->length;
    }
    else {
        _14816 = 1;
    }
    _62update_include_matrix(_14816, _38current_file_no_16946);
    _14816 = NOVALUE;

    /** 	old_file_no = current_file_no*/
    _old_file_no_25598 = _38current_file_no_16946;

    /** 	current_file_no = length(known_files)*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _38current_file_no_16946 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _38current_file_no_16946 = 1;
    }

    /** 	line_number = 0*/
    _38line_number_16947 = 0;

    /** 	read_line()*/
    _62read_line();

    /** 	if new_include_space != 0 then*/
    if (_62new_include_space_24621 == 0)
    goto LE; // [875] 899

    /** 		SymTab[new_include_space][S_OBJ] = current_file_no*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_62new_include_space_24621 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38current_file_no_16946;
    DeRef(_1);
    _14819 = NOVALUE;
LE: 

    /** 	default_namespace( )*/
    _62default_namespace();

    /** end procedure*/
    DeRef(_new_hash_25599);
    DeRef(_14762);
    _14762 = NOVALUE;
    return;
    ;
}


void _62update_include_completion(int _file_no_25822)
{
    int _fx_25831 = NOVALUE;
    int _14829 = NOVALUE;
    int _14828 = NOVALUE;
    int _14827 = NOVALUE;
    int _14826 = NOVALUE;
    int _14824 = NOVALUE;
    int _14823 = NOVALUE;
    int _14822 = NOVALUE;
    int _14821 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( file_include_depend ) do*/
    if (IS_SEQUENCE(_35file_include_depend_15599)){
            _14821 = SEQ_PTR(_35file_include_depend_15599)->length;
    }
    else {
        _14821 = 1;
    }
    {
        int _i_25824;
        _i_25824 = 1;
L1: 
        if (_i_25824 > _14821){
            goto L2; // [10] 114
        }

        /** 		if length( file_include_depend[i] ) then*/
        _2 = (int)SEQ_PTR(_35file_include_depend_15599);
        _14822 = (int)*(((s1_ptr)_2)->base + _i_25824);
        if (IS_SEQUENCE(_14822)){
                _14823 = SEQ_PTR(_14822)->length;
        }
        else {
            _14823 = 1;
        }
        _14822 = NOVALUE;
        if (_14823 == 0)
        {
            _14823 = NOVALUE;
            goto L3; // [28] 105
        }
        else{
            _14823 = NOVALUE;
        }

        /** 			integer fx = find( file_no, file_include_depend[i] )*/
        _2 = (int)SEQ_PTR(_35file_include_depend_15599);
        _14824 = (int)*(((s1_ptr)_2)->base + _i_25824);
        _fx_25831 = find_from(_file_no_25822, _14824, 1);
        _14824 = NOVALUE;

        /** 			if fx then*/
        if (_fx_25831 == 0)
        {
            goto L4; // [46] 104
        }
        else{
        }

        /** 				file_include_depend[i] = remove( file_include_depend[i], fx )*/
        _2 = (int)SEQ_PTR(_35file_include_depend_15599);
        _14826 = (int)*(((s1_ptr)_2)->base + _i_25824);
        {
            s1_ptr assign_space = SEQ_PTR(_14826);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_25831)) ? _fx_25831 : (long)(DBL_PTR(_fx_25831)->dbl);
            int stop = (IS_ATOM_INT(_fx_25831)) ? _fx_25831 : (long)(DBL_PTR(_fx_25831)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_14826);
                DeRef(_14827);
                _14827 = _14826;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_14826), start, &_14827 );
                }
                else Tail(SEQ_PTR(_14826), stop+1, &_14827);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_14826), start, &_14827);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_14827);
                _14827 = _1;
            }
        }
        _14826 = NOVALUE;
        _2 = (int)SEQ_PTR(_35file_include_depend_15599);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35file_include_depend_15599 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_25824);
        _1 = *(int *)_2;
        *(int *)_2 = _14827;
        if( _1 != _14827 ){
            DeRef(_1);
        }
        _14827 = NOVALUE;

        /** 				if not length( file_include_depend[i] ) then*/
        _2 = (int)SEQ_PTR(_35file_include_depend_15599);
        _14828 = (int)*(((s1_ptr)_2)->base + _i_25824);
        if (IS_SEQUENCE(_14828)){
                _14829 = SEQ_PTR(_14828)->length;
        }
        else {
            _14829 = 1;
        }
        _14828 = NOVALUE;
        if (_14829 != 0)
        goto L5; // [79] 103
        _14829 = NOVALUE;

        /** 					finished_files[i] = 1*/
        _2 = (int)SEQ_PTR(_35finished_files_15598);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35finished_files_15598 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_25824);
        *(int *)_2 = 1;

        /** 					if i != file_no then*/
        if (_i_25824 == _file_no_25822)
        goto L6; // [92] 102

        /** 						update_include_completion( i )*/
        _62update_include_completion(_i_25824);
L6: 
L5: 
L4: 
L3: 

        /** 	end for*/
        _i_25824 = _i_25824 + 1;
        goto L1; // [109] 17
L2: 
        ;
    }

    /** end procedure*/
    _14822 = NOVALUE;
    _14828 = NOVALUE;
    return;
    ;
}


int _62IncludePop()
{
    int _top_25862 = NOVALUE;
    int _14860 = NOVALUE;
    int _14858 = NOVALUE;
    int _14857 = NOVALUE;
    int _14835 = NOVALUE;
    int _14833 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	update_include_completion( current_file_no )*/
    _62update_include_completion(_38current_file_no_16946);

    /** 	Resolve_forward_references()*/
    _40Resolve_forward_references(0);

    /** 	HideLocals()*/
    _55HideLocals();

    /** 	if src_file >= 0 then*/
    if (_38src_file_17087 < 0)
    goto L1; // [21] 39

    /** 		close(src_file)*/
    EClose(_38src_file_17087);

    /** 		src_file = -1*/
    _38src_file_17087 = -1;
L1: 

    /** 	if length(IncludeStk) = 0 then*/
    if (IS_SEQUENCE(_62IncludeStk_24632)){
            _14833 = SEQ_PTR(_62IncludeStk_24632)->length;
    }
    else {
        _14833 = 1;
    }
    if (_14833 != 0)
    goto L2; // [46] 59

    /** 		return FALSE  -- the end*/
    DeRef(_top_25862);
    return _9FALSE_426;
L2: 

    /** 	sequence top = IncludeStk[$]*/
    if (IS_SEQUENCE(_62IncludeStk_24632)){
            _14835 = SEQ_PTR(_62IncludeStk_24632)->length;
    }
    else {
        _14835 = 1;
    }
    DeRef(_top_25862);
    _2 = (int)SEQ_PTR(_62IncludeStk_24632);
    _top_25862 = (int)*(((s1_ptr)_2)->base + _14835);
    RefDS(_top_25862);

    /** 	current_file_no    = top[FILE_NO]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38current_file_no_16946 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_38current_file_no_16946)){
        _38current_file_no_16946 = (long)DBL_PTR(_38current_file_no_16946)->dbl;
    }

    /** 	line_number        = top[LINE_NO]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38line_number_16947 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_38line_number_16947)){
        _38line_number_16947 = (long)DBL_PTR(_38line_number_16947)->dbl;
    }

    /** 	src_file           = top[FILE_PTR]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38src_file_17087 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_38src_file_17087)){
        _38src_file_17087 = (long)DBL_PTR(_38src_file_17087)->dbl;
    }

    /** 	file_start_sym     = top[FILE_START_SYM]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38file_start_sym_16952 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_38file_start_sym_16952)){
        _38file_start_sym_16952 = (long)DBL_PTR(_38file_start_sym_16952)->dbl;
    }

    /** 	OpWarning          = top[OP_WARNING]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpWarning_17013 = (int)*(((s1_ptr)_2)->base + 5);
    if (!IS_ATOM_INT(_38OpWarning_17013)){
        _38OpWarning_17013 = (long)DBL_PTR(_38OpWarning_17013)->dbl;
    }

    /** 	OpTrace            = top[OP_TRACE]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpTrace_17015 = (int)*(((s1_ptr)_2)->base + 6);
    if (!IS_ATOM_INT(_38OpTrace_17015)){
        _38OpTrace_17015 = (long)DBL_PTR(_38OpTrace_17015)->dbl;
    }

    /** 	OpTypeCheck        = top[OP_TYPE_CHECK]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpTypeCheck_17016 = (int)*(((s1_ptr)_2)->base + 7);
    if (!IS_ATOM_INT(_38OpTypeCheck_17016)){
        _38OpTypeCheck_17016 = (long)DBL_PTR(_38OpTypeCheck_17016)->dbl;
    }

    /** 	OpProfileTime      = top[OP_PROFILE_TIME]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpProfileTime_17018 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_38OpProfileTime_17018)){
        _38OpProfileTime_17018 = (long)DBL_PTR(_38OpProfileTime_17018)->dbl;
    }

    /** 	OpProfileStatement = top[OP_PROFILE_STATEMENT]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpProfileStatement_17017 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_38OpProfileStatement_17017)){
        _38OpProfileStatement_17017 = (long)DBL_PTR(_38OpProfileStatement_17017)->dbl;
    }

    /** 	OpDefines          = top[OP_DEFINES]*/
    DeRef(_38OpDefines_17019);
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpDefines_17019 = (int)*(((s1_ptr)_2)->base + 10);
    Ref(_38OpDefines_17019);

    /** 	prev_OpWarning     = top[PREV_OP_WARNING]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38prev_OpWarning_17014 = (int)*(((s1_ptr)_2)->base + 11);
    if (!IS_ATOM_INT(_38prev_OpWarning_17014)){
        _38prev_OpWarning_17014 = (long)DBL_PTR(_38prev_OpWarning_17014)->dbl;
    }

    /** 	OpInline           = top[OP_INLINE]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpInline_17023 = (int)*(((s1_ptr)_2)->base + 12);
    if (!IS_ATOM_INT(_38OpInline_17023)){
        _38OpInline_17023 = (long)DBL_PTR(_38OpInline_17023)->dbl;
    }

    /** 	OpIndirectInclude  = top[OP_INDIRECT_INCLUDE]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38OpIndirectInclude_17024 = (int)*(((s1_ptr)_2)->base + 13);
    if (!IS_ATOM_INT(_38OpIndirectInclude_17024)){
        _38OpIndirectInclude_17024 = (long)DBL_PTR(_38OpIndirectInclude_17024)->dbl;
    }

    /** 	putback_fwd_line_number = line_number -- top[PUTBACK_FWD_LINE_NUMBER]*/
    _38putback_fwd_line_number_16949 = _38line_number_16947;

    /** 	putback_ForwardLine = top[PUTBACK_FORWARDLINE]*/
    DeRef(_46putback_ForwardLine_49484);
    _2 = (int)SEQ_PTR(_top_25862);
    _46putback_ForwardLine_49484 = (int)*(((s1_ptr)_2)->base + 15);
    Ref(_46putback_ForwardLine_49484);

    /** 	putback_forward_bp = top[PUTBACK_FORWARD_BP]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _46putback_forward_bp_49488 = (int)*(((s1_ptr)_2)->base + 16);
    if (!IS_ATOM_INT(_46putback_forward_bp_49488)){
        _46putback_forward_bp_49488 = (long)DBL_PTR(_46putback_forward_bp_49488)->dbl;
    }

    /** 	last_fwd_line_number = top[LAST_FWD_LINE_NUMBER]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _38last_fwd_line_number_16950 = (int)*(((s1_ptr)_2)->base + 17);
    if (!IS_ATOM_INT(_38last_fwd_line_number_16950)){
        _38last_fwd_line_number_16950 = (long)DBL_PTR(_38last_fwd_line_number_16950)->dbl;
    }

    /** 	last_ForwardLine = top[LAST_FORWARDLINE]*/
    DeRef(_46last_ForwardLine_49485);
    _2 = (int)SEQ_PTR(_top_25862);
    _46last_ForwardLine_49485 = (int)*(((s1_ptr)_2)->base + 18);
    Ref(_46last_ForwardLine_49485);

    /** 	last_forward_bp = top[LAST_FORWARD_BP]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _46last_forward_bp_49489 = (int)*(((s1_ptr)_2)->base + 19);
    if (!IS_ATOM_INT(_46last_forward_bp_49489)){
        _46last_forward_bp_49489 = (long)DBL_PTR(_46last_forward_bp_49489)->dbl;
    }

    /** 	ThisLine = top[THISLINE]*/
    DeRef(_46ThisLine_49482);
    _2 = (int)SEQ_PTR(_top_25862);
    _46ThisLine_49482 = (int)*(((s1_ptr)_2)->base + 20);
    Ref(_46ThisLine_49482);

    /** 	fwd_line_number = line_number --top[FWD_LINE_NUMBER]*/
    _38fwd_line_number_16948 = _38line_number_16947;

    /** 	forward_bp = top[FORWARD_BP]*/
    _2 = (int)SEQ_PTR(_top_25862);
    _46forward_bp_49487 = (int)*(((s1_ptr)_2)->base + 22);
    if (!IS_ATOM_INT(_46forward_bp_49487)){
        _46forward_bp_49487 = (long)DBL_PTR(_46forward_bp_49487)->dbl;
    }

    /** 	ForwardLine = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_46ForwardLine_49483);
    _46ForwardLine_49483 = _46ThisLine_49482;

    /** 	putback_ForwardLine = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_46putback_ForwardLine_49484);
    _46putback_ForwardLine_49484 = _46ThisLine_49482;

    /** 	last_ForwardLine = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_46last_ForwardLine_49485);
    _46last_ForwardLine_49485 = _46ThisLine_49482;

    /** 	IncludeStk = IncludeStk[1..$-1]*/
    if (IS_SEQUENCE(_62IncludeStk_24632)){
            _14857 = SEQ_PTR(_62IncludeStk_24632)->length;
    }
    else {
        _14857 = 1;
    }
    _14858 = _14857 - 1;
    _14857 = NOVALUE;
    rhs_slice_target = (object_ptr)&_62IncludeStk_24632;
    RHS_Slice(_62IncludeStk_24632, 1, _14858);

    /** 	SymTab[TopLevelSub][S_CODE] = Code*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38TopLevelSub_16953 + ((s1_ptr)_2)->base);
    RefDS(_38Code_17038);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_CODE_16610))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
    _1 = *(int *)_2;
    *(int *)_2 = _38Code_17038;
    DeRef(_1);
    _14860 = NOVALUE;

    /** 	return TRUE*/
    DeRefDS(_top_25862);
    _14858 = NOVALUE;
    return _9TRUE_428;
    ;
}


int _62MakeInt(int _text_25961, int _nBase_25962)
{
    int _num_25963 = NOVALUE;
    int _fnum_25964 = NOVALUE;
    int _digit_25965 = NOVALUE;
    int _maxchk_25966 = NOVALUE;
    int _14927 = NOVALUE;
    int _14926 = NOVALUE;
    int _14924 = NOVALUE;
    int _14923 = NOVALUE;
    int _14920 = NOVALUE;
    int _14918 = NOVALUE;
    int _14915 = NOVALUE;
    int _14914 = NOVALUE;
    int _14913 = NOVALUE;
    int _14911 = NOVALUE;
    int _14909 = NOVALUE;
    int _14907 = NOVALUE;
    int _14906 = NOVALUE;
    int _14904 = NOVALUE;
    int _14902 = NOVALUE;
    int _14901 = NOVALUE;
    int _14899 = NOVALUE;
    int _14898 = NOVALUE;
    int _14895 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_nBase_25962)) {
        _1 = (long)(DBL_PTR(_nBase_25962)->dbl);
        if (UNIQUE(DBL_PTR(_nBase_25962)) && (DBL_PTR(_nBase_25962)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_nBase_25962);
        _nBase_25962 = _1;
    }

    /** 	switch nBase do*/
    _0 = _nBase_25962;
    switch ( _0 ){ 

        /** 		case 2 then*/
        case 2:

        /** 			maxchk = 536870911*/
        _maxchk_25966 = 536870911;
        goto L1; // [21] 82

        /** 		case 8 then*/
        case 8:

        /** 			maxchk = 134217727*/
        _maxchk_25966 = 134217727;
        goto L1; // [32] 82

        /** 		case 10 then*/
        case 10:

        /** 			num = find(text, common_int_text)*/
        _num_25963 = find_from(_text_25961, _62common_int_text_25936, 1);

        /** 			if num then*/
        if (_num_25963 == 0)
        {
            goto L2; // [49] 65
        }
        else{
        }

        /** 				return common_ints[num]*/
        _2 = (int)SEQ_PTR(_62common_ints_25956);
        _14895 = (int)*(((s1_ptr)_2)->base + _num_25963);
        DeRefDS(_text_25961);
        DeRef(_fnum_25964);
        return _14895;
L2: 

        /** 			maxchk = 107374181*/
        _maxchk_25966 = 107374181;
        goto L1; // [70] 82

        /** 		case 16 then*/
        case 16:

        /** 			maxchk = 67108863*/
        _maxchk_25966 = 67108863;
    ;}L1: 

    /** 	num = 0*/
    _num_25963 = 0;

    /** 	fnum = 0*/
    DeRef(_fnum_25964);
    _fnum_25964 = 0;

    /** 	for i = 1 to length(text) do*/
    if (IS_SEQUENCE(_text_25961)){
            _14898 = SEQ_PTR(_text_25961)->length;
    }
    else {
        _14898 = 1;
    }
    {
        int _i_25981;
        _i_25981 = 1;
L3: 
        if (_i_25981 > _14898){
            goto L4; // [97] 307
        }

        /** 		if text[i] > 'a' then*/
        _2 = (int)SEQ_PTR(_text_25961);
        _14899 = (int)*(((s1_ptr)_2)->base + _i_25981);
        if (binary_op_a(LESSEQ, _14899, 97)){
            _14899 = NOVALUE;
            goto L5; // [110] 133
        }
        _14899 = NOVALUE;

        /** 			digit = text[i] - 'a' + 10*/
        _2 = (int)SEQ_PTR(_text_25961);
        _14901 = (int)*(((s1_ptr)_2)->base + _i_25981);
        if (IS_ATOM_INT(_14901)) {
            _14902 = _14901 - 97;
            if ((long)((unsigned long)_14902 +(unsigned long) HIGH_BITS) >= 0){
                _14902 = NewDouble((double)_14902);
            }
        }
        else {
            _14902 = binary_op(MINUS, _14901, 97);
        }
        _14901 = NOVALUE;
        if (IS_ATOM_INT(_14902)) {
            _digit_25965 = _14902 + 10;
        }
        else {
            _digit_25965 = binary_op(PLUS, _14902, 10);
        }
        DeRef(_14902);
        _14902 = NOVALUE;
        if (!IS_ATOM_INT(_digit_25965)) {
            _1 = (long)(DBL_PTR(_digit_25965)->dbl);
            if (UNIQUE(DBL_PTR(_digit_25965)) && (DBL_PTR(_digit_25965)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_digit_25965);
            _digit_25965 = _1;
        }
        goto L6; // [130] 175
L5: 

        /** 		elsif text[i] > 'A' then*/
        _2 = (int)SEQ_PTR(_text_25961);
        _14904 = (int)*(((s1_ptr)_2)->base + _i_25981);
        if (binary_op_a(LESSEQ, _14904, 65)){
            _14904 = NOVALUE;
            goto L7; // [139] 162
        }
        _14904 = NOVALUE;

        /** 			digit = text[i] - 'A' + 10*/
        _2 = (int)SEQ_PTR(_text_25961);
        _14906 = (int)*(((s1_ptr)_2)->base + _i_25981);
        if (IS_ATOM_INT(_14906)) {
            _14907 = _14906 - 65;
            if ((long)((unsigned long)_14907 +(unsigned long) HIGH_BITS) >= 0){
                _14907 = NewDouble((double)_14907);
            }
        }
        else {
            _14907 = binary_op(MINUS, _14906, 65);
        }
        _14906 = NOVALUE;
        if (IS_ATOM_INT(_14907)) {
            _digit_25965 = _14907 + 10;
        }
        else {
            _digit_25965 = binary_op(PLUS, _14907, 10);
        }
        DeRef(_14907);
        _14907 = NOVALUE;
        if (!IS_ATOM_INT(_digit_25965)) {
            _1 = (long)(DBL_PTR(_digit_25965)->dbl);
            if (UNIQUE(DBL_PTR(_digit_25965)) && (DBL_PTR(_digit_25965)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_digit_25965);
            _digit_25965 = _1;
        }
        goto L6; // [159] 175
L7: 

        /** 			digit = text[i] - '0'*/
        _2 = (int)SEQ_PTR(_text_25961);
        _14909 = (int)*(((s1_ptr)_2)->base + _i_25981);
        if (IS_ATOM_INT(_14909)) {
            _digit_25965 = _14909 - 48;
        }
        else {
            _digit_25965 = binary_op(MINUS, _14909, 48);
        }
        _14909 = NOVALUE;
        if (!IS_ATOM_INT(_digit_25965)) {
            _1 = (long)(DBL_PTR(_digit_25965)->dbl);
            if (UNIQUE(DBL_PTR(_digit_25965)) && (DBL_PTR(_digit_25965)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_digit_25965);
            _digit_25965 = _1;
        }
L6: 

        /** 		if digit >= nBase or digit < 0 then*/
        _14911 = (_digit_25965 >= _nBase_25962);
        if (_14911 != 0) {
            goto L8; // [183] 196
        }
        _14913 = (_digit_25965 < 0);
        if (_14913 == 0)
        {
            DeRef(_14913);
            _14913 = NOVALUE;
            goto L9; // [192] 212
        }
        else{
            DeRef(_14913);
            _14913 = NOVALUE;
        }
L8: 

        /** 			CompileErr(62, {text[i],i})*/
        _2 = (int)SEQ_PTR(_text_25961);
        _14914 = (int)*(((s1_ptr)_2)->base + _i_25981);
        Ref(_14914);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _14914;
        ((int *)_2)[2] = _i_25981;
        _14915 = MAKE_SEQ(_1);
        _14914 = NOVALUE;
        _46CompileErr(62, _14915, 0);
        _14915 = NOVALUE;
L9: 

        /** 		if fnum = 0 then*/
        if (binary_op_a(NOTEQ, _fnum_25964, 0)){
            goto LA; // [214] 255
        }

        /** 			if num <= maxchk then*/
        if (_num_25963 > _maxchk_25966)
        goto LB; // [222] 241

        /** 				num = num * nBase + digit*/
        if (_num_25963 == (short)_num_25963 && _nBase_25962 <= INT15 && _nBase_25962 >= -INT15)
        _14918 = _num_25963 * _nBase_25962;
        else
        _14918 = NewDouble(_num_25963 * (double)_nBase_25962);
        if (IS_ATOM_INT(_14918)) {
            _num_25963 = _14918 + _digit_25965;
        }
        else {
            _num_25963 = NewDouble(DBL_PTR(_14918)->dbl + (double)_digit_25965);
        }
        DeRef(_14918);
        _14918 = NOVALUE;
        if (!IS_ATOM_INT(_num_25963)) {
            _1 = (long)(DBL_PTR(_num_25963)->dbl);
            if (UNIQUE(DBL_PTR(_num_25963)) && (DBL_PTR(_num_25963)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_num_25963);
            _num_25963 = _1;
        }
        goto LC; // [238] 300
LB: 

        /** 				fnum = num * nBase + digit*/
        if (_num_25963 == (short)_num_25963 && _nBase_25962 <= INT15 && _nBase_25962 >= -INT15)
        _14920 = _num_25963 * _nBase_25962;
        else
        _14920 = NewDouble(_num_25963 * (double)_nBase_25962);
        DeRef(_fnum_25964);
        if (IS_ATOM_INT(_14920)) {
            _fnum_25964 = _14920 + _digit_25965;
            if ((long)((unsigned long)_fnum_25964 + (unsigned long)HIGH_BITS) >= 0) 
            _fnum_25964 = NewDouble((double)_fnum_25964);
        }
        else {
            _fnum_25964 = NewDouble(DBL_PTR(_14920)->dbl + (double)_digit_25965);
        }
        DeRef(_14920);
        _14920 = NOVALUE;
        goto LC; // [252] 300
LA: 

        /** 			if fnum >= almost_max_16 then*/
        if (binary_op_a(LESS, _fnum_25964, _62almost_max_16_25933)){
            goto LD; // [259] 289
        }

        /** 				if fnum > (MAX_ATOM - digit)/nBase then*/
        if (IS_ATOM_INT(_62MAX_ATOM_25925)) {
            _14923 = _62MAX_ATOM_25925 - _digit_25965;
            if ((long)((unsigned long)_14923 +(unsigned long) HIGH_BITS) >= 0){
                _14923 = NewDouble((double)_14923);
            }
        }
        else {
            _14923 = binary_op(MINUS, _62MAX_ATOM_25925, _digit_25965);
        }
        if (IS_ATOM_INT(_14923)) {
            _14924 = (_14923 % _nBase_25962) ? NewDouble((double)_14923 / _nBase_25962) : (_14923 / _nBase_25962);
        }
        else {
            _14924 = binary_op(DIVIDE, _14923, _nBase_25962);
        }
        DeRef(_14923);
        _14923 = NOVALUE;
        if (binary_op_a(LESSEQ, _fnum_25964, _14924)){
            DeRef(_14924);
            _14924 = NOVALUE;
            goto LE; // [275] 288
        }
        DeRef(_14924);
        _14924 = NOVALUE;

        /** 					fenv:raise(FE_OVERFLOW)					*/
        _14926 = _64raise(111);
LE: 
LD: 

        /** 			fnum = fnum * nBase + digit*/
        if (IS_ATOM_INT(_fnum_25964)) {
            if (_fnum_25964 == (short)_fnum_25964 && _nBase_25962 <= INT15 && _nBase_25962 >= -INT15)
            _14927 = _fnum_25964 * _nBase_25962;
            else
            _14927 = NewDouble(_fnum_25964 * (double)_nBase_25962);
        }
        else {
            _14927 = NewDouble(DBL_PTR(_fnum_25964)->dbl * (double)_nBase_25962);
        }
        DeRef(_fnum_25964);
        if (IS_ATOM_INT(_14927)) {
            _fnum_25964 = _14927 + _digit_25965;
            if ((long)((unsigned long)_fnum_25964 + (unsigned long)HIGH_BITS) >= 0) 
            _fnum_25964 = NewDouble((double)_fnum_25964);
        }
        else {
            _fnum_25964 = NewDouble(DBL_PTR(_14927)->dbl + (double)_digit_25965);
        }
        DeRef(_14927);
        _14927 = NOVALUE;
LC: 

        /** 	end for*/
        _i_25981 = _i_25981 + 1;
        goto L3; // [302] 104
L4: 
        ;
    }

    /** 	if fnum = 0 then*/
    if (binary_op_a(NOTEQ, _fnum_25964, 0)){
        goto LF; // [309] 322
    }

    /** 		return num*/
    DeRefDS(_text_25961);
    DeRef(_fnum_25964);
    _14895 = NOVALUE;
    DeRef(_14911);
    _14911 = NOVALUE;
    DeRef(_14926);
    _14926 = NOVALUE;
    return _num_25963;
    goto L10; // [319] 329
LF: 

    /** 		return fnum*/
    DeRefDS(_text_25961);
    _14895 = NOVALUE;
    DeRef(_14911);
    _14911 = NOVALUE;
    DeRef(_14926);
    _14926 = NOVALUE;
    return _fnum_25964;
L10: 
    ;
}


int _62GetHexChar(int _cnt_26030, int _errno_26031)
{
    int _val_26032 = NOVALUE;
    int _d_26033 = NOVALUE;
    int _14937 = NOVALUE;
    int _14936 = NOVALUE;
    int _14931 = NOVALUE;
    int _0, _1, _2;
    

    /** 	val = 0*/
    DeRef(_val_26032);
    _val_26032 = 0;

    /** 	while cnt > 0 do*/
L1: 
    if (_cnt_26030 <= 0)
    goto L2; // [15] 88

    /** 		d = find(getch(), "0123456789ABCDEFabcdef_")*/
    _14931 = _62getch();
    _d_26033 = find_from(_14931, _14932, 1);
    DeRef(_14931);
    _14931 = NOVALUE;

    /** 		if d = 0 then*/
    if (_d_26033 != 0)
    goto L3; // [31] 43

    /** 			CompileErr( errno )*/
    RefDS(_22663);
    _46CompileErr(_errno_26031, _22663, 0);
L3: 

    /** 		if d != 23 then*/
    if (_d_26033 == 23)
    goto L1; // [45] 15

    /** 			val = val * 16 + d - 1*/
    if (IS_ATOM_INT(_val_26032)) {
        if (_val_26032 == (short)_val_26032)
        _14936 = _val_26032 * 16;
        else
        _14936 = NewDouble(_val_26032 * (double)16);
    }
    else {
        _14936 = NewDouble(DBL_PTR(_val_26032)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_14936)) {
        _14937 = _14936 + _d_26033;
        if ((long)((unsigned long)_14937 + (unsigned long)HIGH_BITS) >= 0) 
        _14937 = NewDouble((double)_14937);
    }
    else {
        _14937 = NewDouble(DBL_PTR(_14936)->dbl + (double)_d_26033);
    }
    DeRef(_14936);
    _14936 = NOVALUE;
    DeRef(_val_26032);
    if (IS_ATOM_INT(_14937)) {
        _val_26032 = _14937 - 1;
        if ((long)((unsigned long)_val_26032 +(unsigned long) HIGH_BITS) >= 0){
            _val_26032 = NewDouble((double)_val_26032);
        }
    }
    else {
        _val_26032 = NewDouble(DBL_PTR(_14937)->dbl - (double)1);
    }
    DeRef(_14937);
    _14937 = NOVALUE;

    /** 			if d > 16 then*/
    if (_d_26033 <= 16)
    goto L4; // [65] 76

    /** 				val -= 6*/
    _0 = _val_26032;
    if (IS_ATOM_INT(_val_26032)) {
        _val_26032 = _val_26032 - 6;
        if ((long)((unsigned long)_val_26032 +(unsigned long) HIGH_BITS) >= 0){
            _val_26032 = NewDouble((double)_val_26032);
        }
    }
    else {
        _val_26032 = NewDouble(DBL_PTR(_val_26032)->dbl - (double)6);
    }
    DeRef(_0);
L4: 

    /** 			cnt -= 1*/
    _cnt_26030 = _cnt_26030 - 1;

    /** 	end while*/
    goto L1; // [85] 15
L2: 

    /** 	return val*/
    return _val_26032;
    ;
}


int _62GetBinaryChar(int _delim_26053)
{
    int _val_26054 = NOVALUE;
    int _d_26055 = NOVALUE;
    int _vchars_26056 = NOVALUE;
    int _cnt_26059 = NOVALUE;
    int _14951 = NOVALUE;
    int _14950 = NOVALUE;
    int _14944 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence vchars = "01_ " & delim*/
    Append(&_vchars_26056, _14942, _delim_26053);

    /** 	integer cnt = 0*/
    _cnt_26059 = 0;

    /** 	val = 0*/
    DeRef(_val_26054);
    _val_26054 = 0;

    /** 	while 1 do*/
L1: 

    /** 		d = find(getch(), vchars)*/
    _14944 = _62getch();
    _d_26055 = find_from(_14944, _vchars_26056, 1);
    DeRef(_14944);
    _14944 = NOVALUE;

    /** 		if d = 0 then*/
    if (_d_26055 != 0)
    goto L2; // [36] 48

    /** 			CompileErr( 343 )*/
    RefDS(_22663);
    _46CompileErr(343, _22663, 0);
L2: 

    /** 		if d = 5 then*/
    if (_d_26055 != 5)
    goto L3; // [50] 63

    /** 			ungetch()*/
    _62ungetch();

    /** 			exit*/
    goto L4; // [60] 106
L3: 

    /** 		if d = 4 then*/
    if (_d_26055 != 4)
    goto L5; // [65] 74

    /** 			exit*/
    goto L4; // [71] 106
L5: 

    /** 		if d != 3 then*/
    if (_d_26055 == 3)
    goto L1; // [76] 24

    /** 			val = val * 2 + d - 1*/
    if (IS_ATOM_INT(_val_26054) && IS_ATOM_INT(_val_26054)) {
        _14950 = _val_26054 + _val_26054;
        if ((long)((unsigned long)_14950 + (unsigned long)HIGH_BITS) >= 0) 
        _14950 = NewDouble((double)_14950);
    }
    else {
        if (IS_ATOM_INT(_val_26054)) {
            _14950 = NewDouble((double)_val_26054 + DBL_PTR(_val_26054)->dbl);
        }
        else {
            if (IS_ATOM_INT(_val_26054)) {
                _14950 = NewDouble(DBL_PTR(_val_26054)->dbl + (double)_val_26054);
            }
            else
            _14950 = NewDouble(DBL_PTR(_val_26054)->dbl + DBL_PTR(_val_26054)->dbl);
        }
    }
    if (IS_ATOM_INT(_14950)) {
        _14951 = _14950 + _d_26055;
        if ((long)((unsigned long)_14951 + (unsigned long)HIGH_BITS) >= 0) 
        _14951 = NewDouble((double)_14951);
    }
    else {
        _14951 = NewDouble(DBL_PTR(_14950)->dbl + (double)_d_26055);
    }
    DeRef(_14950);
    _14950 = NOVALUE;
    DeRef(_val_26054);
    if (IS_ATOM_INT(_14951)) {
        _val_26054 = _14951 - 1;
        if ((long)((unsigned long)_val_26054 +(unsigned long) HIGH_BITS) >= 0){
            _val_26054 = NewDouble((double)_val_26054);
        }
    }
    else {
        _val_26054 = NewDouble(DBL_PTR(_14951)->dbl - (double)1);
    }
    DeRef(_14951);
    _14951 = NOVALUE;

    /** 			cnt += 1*/
    _cnt_26059 = _cnt_26059 + 1;

    /** 	end while*/
    goto L1; // [103] 24
L4: 

    /** 	if cnt = 0 then*/
    if (_cnt_26059 != 0)
    goto L6; // [108] 120

    /** 		CompileErr(343)*/
    RefDS(_22663);
    _46CompileErr(343, _22663, 0);
L6: 

    /** 	return val*/
    DeRefi(_vchars_26056);
    return _val_26054;
    ;
}


int _62EscapeChar(int _delim_26081)
{
    int _c_26082 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c = getch()*/
    _0 = _c_26082;
    _c_26082 = _62getch();
    DeRef(_0);

    /** 	switch c do*/
    if (IS_SEQUENCE(_c_26082) ){
        goto L1; // [10] 135
    }
    if(!IS_ATOM_INT(_c_26082)){
        if( (DBL_PTR(_c_26082)->dbl != (double) ((int) DBL_PTR(_c_26082)->dbl) ) ){
            goto L1; // [10] 135
        }
        _0 = (int) DBL_PTR(_c_26082)->dbl;
    }
    else {
        _0 = _c_26082;
    };
    switch ( _0 ){ 

        /** 		case 'n' then*/
        case 110:

        /** 			c = 10 -- Newline*/
        DeRef(_c_26082);
        _c_26082 = 10;
        goto L2; // [24] 145

        /** 		case 't' then*/
        case 116:

        /** 			c = 9 -- Tabulator*/
        DeRef(_c_26082);
        _c_26082 = 9;
        goto L2; // [35] 145

        /** 		case '"', '\\', '\'' then*/
        case 34:
        case 92:
        case 39:

        /** 		case 'r' then*/
        goto L2; // [47] 145
        case 114:

        /** 			c = 13 -- Carriage Return*/
        DeRef(_c_26082);
        _c_26082 = 13;
        goto L2; // [56] 145

        /** 		case '0' then*/
        case 48:

        /** 			c = 0 -- Null*/
        DeRef(_c_26082);
        _c_26082 = 0;
        goto L2; // [67] 145

        /** 		case 'e', 'E' then*/
        case 101:
        case 69:

        /** 			c = 27 -- escape char.*/
        DeRef(_c_26082);
        _c_26082 = 27;
        goto L2; // [80] 145

        /** 		case 'x' then*/
        case 120:

        /** 			c = GetHexChar(2, 340)*/
        _0 = _c_26082;
        _c_26082 = _62GetHexChar(2, 340);
        DeRef(_0);
        goto L2; // [93] 145

        /** 		case 'u' then*/
        case 117:

        /** 			c = GetHexChar(4, 341)*/
        _0 = _c_26082;
        _c_26082 = _62GetHexChar(4, 341);
        DeRef(_0);
        goto L2; // [106] 145

        /** 		case 'U' then*/
        case 85:

        /** 			c = GetHexChar(8, 342)*/
        _0 = _c_26082;
        _c_26082 = _62GetHexChar(8, 342);
        DeRef(_0);
        goto L2; // [119] 145

        /** 		case 'b' then*/
        case 98:

        /** 			c = GetBinaryChar(delim)*/
        _0 = _c_26082;
        _c_26082 = _62GetBinaryChar(_delim_26081);
        DeRef(_0);
        goto L2; // [131] 145

        /** 		case else*/
        default:
L1: 

        /** 			CompileErr(155)*/
        RefDS(_22663);
        _46CompileErr(155, _22663, 0);
    ;}L2: 

    /** 	return c*/
    return _c_26082;
    ;
}


int _62my_sscanf(int _yytext_26105)
{
    int _e_sign_26106 = NOVALUE;
    int _ndigits_26107 = NOVALUE;
    int _e_mag_26108 = NOVALUE;
    int _mantissa_26109 = NOVALUE;
    int _c_26110 = NOVALUE;
    int _i_26111 = NOVALUE;
    int _dec_26112 = NOVALUE;
    int _ex_26113 = NOVALUE;
    int _real_overflow_26114 = NOVALUE;
    int _not_zero_26142 = NOVALUE;
    int _back_dec_26151 = NOVALUE;
    int _num_back_26153 = NOVALUE;
    int _num_26154 = NOVALUE;
    int _frac_26180 = NOVALUE;
    int _15017 = NOVALUE;
    int _15016 = NOVALUE;
    int _15014 = NOVALUE;
    int _15013 = NOVALUE;
    int _15012 = NOVALUE;
    int _15006 = NOVALUE;
    int _15005 = NOVALUE;
    int _15004 = NOVALUE;
    int _15003 = NOVALUE;
    int _14997 = NOVALUE;
    int _14996 = NOVALUE;
    int _14993 = NOVALUE;
    int _14992 = NOVALUE;
    int _14991 = NOVALUE;
    int _14987 = NOVALUE;
    int _14986 = NOVALUE;
    int _14985 = NOVALUE;
    int _14984 = NOVALUE;
    int _14980 = NOVALUE;
    int _14979 = NOVALUE;
    int _14976 = NOVALUE;
    int _14975 = NOVALUE;
    int _14974 = NOVALUE;
    int _14968 = NOVALUE;
    int _14966 = NOVALUE;
    int _14965 = NOVALUE;
    int _14963 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer real_overflow = 0*/
    _real_overflow_26114 = 0;

    /** 	if length(yytext) < 2 then*/
    if (IS_SEQUENCE(_yytext_26105)){
            _14963 = SEQ_PTR(_yytext_26105)->length;
    }
    else {
        _14963 = 1;
    }
    if (_14963 >= 2)
    goto L1; // [13] 25

    /** 		CompileErr(121)*/
    RefDS(_22663);
    _46CompileErr(121, _22663, 0);
L1: 

    /** 	fenv:clear(FE_ALL_EXCEPT)*/
    RefDS(_64FE_ALL_EXCEPT_22956);
    _14965 = _64clear(_64FE_ALL_EXCEPT_22956);

    /** 	if find( 'e', yytext ) or find( 'E', yytext ) then*/
    _14966 = find_from(101, _yytext_26105, 1);
    if (_14966 != 0) {
        goto L2; // [40] 54
    }
    _14968 = find_from(69, _yytext_26105, 1);
    if (_14968 == 0)
    {
        _14968 = NOVALUE;
        goto L3; // [50] 67
    }
    else{
        _14968 = NOVALUE;
    }
L2: 

    /** 		mantissa = scientific_to_atom( yytext )*/
    RefDS(_yytext_26105);
    _0 = _mantissa_26109;
    _mantissa_26109 = _63scientific_to_atom(_yytext_26105);
    DeRef(_0);

    /** 		goto "floating_point_check"*/
    goto G4;
L3: 

    /** 	mantissa = 0.0*/
    RefDS(_14971);
    DeRef(_mantissa_26109);
    _mantissa_26109 = _14971;

    /** 	ndigits = 0*/
    _ndigits_26107 = 0;

    /** 	yytext &= 0 -- end marker*/
    Append(&_yytext_26105, _yytext_26105, 0);

    /** 	c = yytext[1]*/
    _2 = (int)SEQ_PTR(_yytext_26105);
    _c_26110 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_c_26110))
    _c_26110 = (long)DBL_PTR(_c_26110)->dbl;

    /** 	i = 2*/
    _i_26111 = 2;

    /** 	while c >= '0' and c <= '9' do*/
L5: 
    _14974 = (_c_26110 >= 48);
    if (_14974 == 0) {
        goto L6; // [103] 152
    }
    _14976 = (_c_26110 <= 57);
    if (_14976 == 0)
    {
        DeRef(_14976);
        _14976 = NOVALUE;
        goto L6; // [112] 152
    }
    else{
        DeRef(_14976);
        _14976 = NOVALUE;
    }

    /** 		ndigits += 1*/
    _ndigits_26107 = _ndigits_26107 + 1;

    /** 		mantissa = mantissa * 10.0 + (c - '0')*/
    if (IS_ATOM_INT(_mantissa_26109)) {
        _14979 = NewDouble((double)_mantissa_26109 * DBL_PTR(_14978)->dbl);
    }
    else {
        _14979 = NewDouble(DBL_PTR(_mantissa_26109)->dbl * DBL_PTR(_14978)->dbl);
    }
    _14980 = _c_26110 - 48;
    if ((long)((unsigned long)_14980 +(unsigned long) HIGH_BITS) >= 0){
        _14980 = NewDouble((double)_14980);
    }
    DeRef(_mantissa_26109);
    if (IS_ATOM_INT(_14980)) {
        _mantissa_26109 = NewDouble(DBL_PTR(_14979)->dbl + (double)_14980);
    }
    else
    _mantissa_26109 = NewDouble(DBL_PTR(_14979)->dbl + DBL_PTR(_14980)->dbl);
    DeRefDS(_14979);
    _14979 = NOVALUE;
    DeRef(_14980);
    _14980 = NOVALUE;

    /** 		c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26105);
    _c_26110 = (int)*(((s1_ptr)_2)->base + _i_26111);
    if (!IS_ATOM_INT(_c_26110))
    _c_26110 = (long)DBL_PTR(_c_26110)->dbl;

    /** 		i += 1*/
    _i_26111 = _i_26111 + 1;

    /** 	end while*/
    goto L5; // [149] 99
L6: 

    /** 	integer not_zero = 0*/
    _not_zero_26142 = 0;

    /** 	if c = '.' and not fenv:test(FE_OVERFLOW) then*/
    _14984 = (_c_26110 == 46);
    if (_14984 == 0) {
        goto L7; // [163] 409
    }
    _14986 = _64test(111);
    if (IS_ATOM_INT(_14986)) {
        _14987 = (_14986 == 0);
    }
    else {
        _14987 = unary_op(NOT, _14986);
    }
    DeRef(_14986);
    _14986 = NOVALUE;
    if (_14987 == 0) {
        DeRef(_14987);
        _14987 = NOVALUE;
        goto L7; // [177] 409
    }
    else {
        if (!IS_ATOM_INT(_14987) && DBL_PTR(_14987)->dbl == 0.0){
            DeRef(_14987);
            _14987 = NOVALUE;
            goto L7; // [177] 409
        }
        DeRef(_14987);
        _14987 = NOVALUE;
    }
    DeRef(_14987);
    _14987 = NOVALUE;

    /** 		c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26105);
    _c_26110 = (int)*(((s1_ptr)_2)->base + _i_26111);
    if (!IS_ATOM_INT(_c_26110))
    _c_26110 = (long)DBL_PTR(_c_26110)->dbl;

    /** 		i += 1*/
    _i_26111 = _i_26111 + 1;

    /** 		atom back_dec*/

    /** 		dec = 1.0*/
    RefDS(_14990);
    DeRef(_dec_26112);
    _dec_26112 = _14990;

    /** 		atom num_back, num = 0*/
    DeRef(_num_26154);
    _num_26154 = 0;

    /** 		while c >= '0' and c <= '9' do*/
L8: 
    _14991 = (_c_26110 >= 48);
    if (_14991 == 0) {
        goto L9; // [213] 319
    }
    _14993 = (_c_26110 <= 57);
    if (_14993 == 0)
    {
        DeRef(_14993);
        _14993 = NOVALUE;
        goto L9; // [222] 319
    }
    else{
        DeRef(_14993);
        _14993 = NOVALUE;
    }

    /** 			ndigits += 1*/
    _ndigits_26107 = _ndigits_26107 + 1;

    /** 			if c != '0' then*/
    if (_c_26110 == 48)
    goto LA; // [233] 243

    /** 				not_zero = 1*/
    _not_zero_26142 = 1;
LA: 

    /** 			num_back = num*/
    Ref(_num_26154);
    DeRef(_num_back_26153);
    _num_back_26153 = _num_26154;

    /** 			num = num_back * 10 + (c - '0')*/
    if (IS_ATOM_INT(_num_back_26153)) {
        if (_num_back_26153 == (short)_num_back_26153)
        _14996 = _num_back_26153 * 10;
        else
        _14996 = NewDouble(_num_back_26153 * (double)10);
    }
    else {
        _14996 = NewDouble(DBL_PTR(_num_back_26153)->dbl * (double)10);
    }
    _14997 = _c_26110 - 48;
    if ((long)((unsigned long)_14997 +(unsigned long) HIGH_BITS) >= 0){
        _14997 = NewDouble((double)_14997);
    }
    DeRef(_num_26154);
    if (IS_ATOM_INT(_14996) && IS_ATOM_INT(_14997)) {
        _num_26154 = _14996 + _14997;
        if ((long)((unsigned long)_num_26154 + (unsigned long)HIGH_BITS) >= 0) 
        _num_26154 = NewDouble((double)_num_26154);
    }
    else {
        if (IS_ATOM_INT(_14996)) {
            _num_26154 = NewDouble((double)_14996 + DBL_PTR(_14997)->dbl);
        }
        else {
            if (IS_ATOM_INT(_14997)) {
                _num_26154 = NewDouble(DBL_PTR(_14996)->dbl + (double)_14997);
            }
            else
            _num_26154 = NewDouble(DBL_PTR(_14996)->dbl + DBL_PTR(_14997)->dbl);
        }
    }
    DeRef(_14996);
    _14996 = NOVALUE;
    DeRef(_14997);
    _14997 = NOVALUE;

    /** 			back_dec = dec*/
    Ref(_dec_26112);
    DeRef(_back_dec_26151);
    _back_dec_26151 = _dec_26112;

    /** 			dec *= 10.0*/
    _0 = _dec_26112;
    if (IS_ATOM_INT(_dec_26112)) {
        _dec_26112 = NewDouble((double)_dec_26112 * DBL_PTR(_14978)->dbl);
    }
    else {
        _dec_26112 = NewDouble(DBL_PTR(_dec_26112)->dbl * DBL_PTR(_14978)->dbl);
    }
    DeRef(_0);

    /** 			c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26105);
    _c_26110 = (int)*(((s1_ptr)_2)->base + _i_26111);
    if (!IS_ATOM_INT(_c_26110))
    _c_26110 = (long)DBL_PTR(_c_26110)->dbl;

    /** 			i += 1*/
    _i_26111 = _i_26111 + 1;

    /** 			if dec = PINF then*/
    if (binary_op_a(NOTEQ, _dec_26112, _20PINF_4320)){
        goto L8; // [289] 209
    }

    /** 				fenv:clear(fenv:FE_OVERFLOW)*/
    _15003 = _64clear(111);

    /** 				num = num_back*/
    Ref(_num_back_26153);
    DeRef(_num_26154);
    _num_26154 = _num_back_26153;

    /** 				dec = back_dec*/
    Ref(_back_dec_26151);
    DeRefDS(_dec_26112);
    _dec_26112 = _back_dec_26151;

    /** 				exit*/
    goto L9; // [311] 319

    /** 		end while*/
    goto L8; // [316] 209
L9: 

    /** 		while c >= '0' and c <= '9' do*/
LB: 
    _15004 = (_c_26110 >= 48);
    if (_15004 == 0) {
        goto LC; // [328] 373
    }
    _15006 = (_c_26110 <= 57);
    if (_15006 == 0)
    {
        DeRef(_15006);
        _15006 = NOVALUE;
        goto LC; // [337] 373
    }
    else{
        DeRef(_15006);
        _15006 = NOVALUE;
    }

    /** 			if c != '0' then*/
    if (_c_26110 == 48)
    goto LD; // [342] 356

    /** 				not_zero = 1*/
    _not_zero_26142 = 1;

    /** 				exit*/
    goto LC; // [353] 373
LD: 

    /** 			c = yytext[i]*/
    _2 = (int)SEQ_PTR(_yytext_26105);
    _c_26110 = (int)*(((s1_ptr)_2)->base + _i_26111);
    if (!IS_ATOM_INT(_c_26110))
    _c_26110 = (long)DBL_PTR(_c_26110)->dbl;

    /** 			i += 1*/
    _i_26111 = _i_26111 + 1;

    /** 		end while*/
    goto LB; // [370] 324
LC: 

    /** 		atom frac*/

    /** 		frac = num / dec*/
    DeRef(_frac_26180);
    if (IS_ATOM_INT(_num_26154) && IS_ATOM_INT(_dec_26112)) {
        _frac_26180 = (_num_26154 % _dec_26112) ? NewDouble((double)_num_26154 / _dec_26112) : (_num_26154 / _dec_26112);
    }
    else {
        if (IS_ATOM_INT(_num_26154)) {
            _frac_26180 = NewDouble((double)_num_26154 / DBL_PTR(_dec_26112)->dbl);
        }
        else {
            if (IS_ATOM_INT(_dec_26112)) {
                _frac_26180 = NewDouble(DBL_PTR(_num_26154)->dbl / (double)_dec_26112);
            }
            else
            _frac_26180 = NewDouble(DBL_PTR(_num_26154)->dbl / DBL_PTR(_dec_26112)->dbl);
        }
    }

    /** 		mantissa += frac  */
    _0 = _mantissa_26109;
    if (IS_ATOM_INT(_mantissa_26109) && IS_ATOM_INT(_frac_26180)) {
        _mantissa_26109 = _mantissa_26109 + _frac_26180;
        if ((long)((unsigned long)_mantissa_26109 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_26109 = NewDouble((double)_mantissa_26109);
    }
    else {
        if (IS_ATOM_INT(_mantissa_26109)) {
            _mantissa_26109 = NewDouble((double)_mantissa_26109 + DBL_PTR(_frac_26180)->dbl);
        }
        else {
            if (IS_ATOM_INT(_frac_26180)) {
                _mantissa_26109 = NewDouble(DBL_PTR(_mantissa_26109)->dbl + (double)_frac_26180);
            }
            else
            _mantissa_26109 = NewDouble(DBL_PTR(_mantissa_26109)->dbl + DBL_PTR(_frac_26180)->dbl);
        }
    }
    DeRef(_0);

    /** 		if frac = 0 and not_zero then*/
    if (IS_ATOM_INT(_frac_26180)) {
        _15012 = (_frac_26180 == 0);
    }
    else {
        _15012 = (DBL_PTR(_frac_26180)->dbl == (double)0);
    }
    if (_15012 == 0) {
        goto LE; // [393] 408
    }
    if (_not_zero_26142 == 0)
    {
        goto LE; // [398] 408
    }
    else{
    }

    /** 			fenv:raise(fenv:FE_UNDERFLOW)*/
    _15014 = _64raise(117);
LE: 
L7: 
    DeRef(_back_dec_26151);
    _back_dec_26151 = NOVALUE;
    DeRef(_num_back_26153);
    _num_back_26153 = NOVALUE;
    DeRef(_num_26154);
    _num_26154 = NOVALUE;
    DeRef(_frac_26180);
    _frac_26180 = NOVALUE;

    /** 	if ndigits = 0 then*/
    if (_ndigits_26107 != 0)
    goto LF; // [413] 425

    /** 		CompileErr(121)  -- no digits*/
    RefDS(_22663);
    _46CompileErr(121, _22663, 0);
LF: 

    /** 	label "floating_point_check"*/
G4:

    /** 	if fenv:test(fenv:FE_UNDERFLOW) then*/
    _15016 = _64test(117);
    if (_15016 == 0) {
        DeRef(_15016);
        _15016 = NOVALUE;
        goto L10; // [435] 450
    }
    else {
        if (!IS_ATOM_INT(_15016) && DBL_PTR(_15016)->dbl == 0.0){
            DeRef(_15016);
            _15016 = NOVALUE;
            goto L10; // [435] 450
        }
        DeRef(_15016);
        _15016 = NOVALUE;
    }
    DeRef(_15016);
    _15016 = NOVALUE;

    /** 		CompileErr(NUMBER_IS_TOO_SMALL)*/
    RefDS(_22663);
    _46CompileErr(356, _22663, 0);
    goto L11; // [447] 476
L10: 

    /** 	elsif fenv:test(fenv:FE_OVERFLOW) or real_overflow then -- ex = {FE_OVERFLOW}*/
    _15017 = _64test(111);
    if (IS_ATOM_INT(_15017)) {
        if (_15017 != 0) {
            goto L12; // [456] 465
        }
    }
    else {
        if (DBL_PTR(_15017)->dbl != 0.0) {
            goto L12; // [456] 465
        }
    }
    if (_real_overflow_26114 == 0)
    {
        goto L13; // [461] 475
    }
    else{
    }
L12: 

    /** 		CompileErr(NUMBER_IS_TOO_BIG)*/
    RefDS(_22663);
    _46CompileErr(357, _22663, 0);
L13: 
L11: 

    /** 	return mantissa*/
    DeRefDS(_yytext_26105);
    DeRef(_dec_26112);
    DeRef(_14965);
    _14965 = NOVALUE;
    DeRef(_14974);
    _14974 = NOVALUE;
    DeRef(_14984);
    _14984 = NOVALUE;
    DeRef(_14991);
    _14991 = NOVALUE;
    DeRef(_15003);
    _15003 = NOVALUE;
    DeRef(_15004);
    _15004 = NOVALUE;
    DeRef(_15012);
    _15012 = NOVALUE;
    DeRef(_15014);
    _15014 = NOVALUE;
    DeRef(_15017);
    _15017 = NOVALUE;
    return _mantissa_26109;
    ;
}


void _62maybe_namespace()
{
    int _0, _1, _2;
    

    /** 	might_be_namespace = 1*/
    _62might_be_namespace_26199 = 1;

    /** end procedure*/
    return;
    ;
}


int _62ExtendedString(int _ech_26209)
{
    int _ch_26210 = NOVALUE;
    int _fch_26211 = NOVALUE;
    int _cline_26212 = NOVALUE;
    int _string_text_26213 = NOVALUE;
    int _trimming_26214 = NOVALUE;
    int _15069 = NOVALUE;
    int _15068 = NOVALUE;
    int _15066 = NOVALUE;
    int _15065 = NOVALUE;
    int _15064 = NOVALUE;
    int _15063 = NOVALUE;
    int _15062 = NOVALUE;
    int _15061 = NOVALUE;
    int _15060 = NOVALUE;
    int _15059 = NOVALUE;
    int _15057 = NOVALUE;
    int _15056 = NOVALUE;
    int _15055 = NOVALUE;
    int _15054 = NOVALUE;
    int _15053 = NOVALUE;
    int _15052 = NOVALUE;
    int _15049 = NOVALUE;
    int _15048 = NOVALUE;
    int _15047 = NOVALUE;
    int _15045 = NOVALUE;
    int _15044 = NOVALUE;
    int _15043 = NOVALUE;
    int _15042 = NOVALUE;
    int _15039 = NOVALUE;
    int _15024 = NOVALUE;
    int _15022 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cline = line_number*/
    _cline_26212 = _38line_number_16947;

    /** 	string_text = ""*/
    RefDS(_5);
    DeRefi(_string_text_26213);
    _string_text_26213 = _5;

    /** 	trimming = 0*/
    _trimming_26214 = 0;

    /** 	ch = getch()*/
    _ch_26210 = _62getch();
    if (!IS_ATOM_INT(_ch_26210)) {
        _1 = (long)(DBL_PTR(_ch_26210)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26210)) && (DBL_PTR(_ch_26210)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26210);
        _ch_26210 = _1;
    }

    /** 	if bp > length(ThisLine) then*/
    if (IS_SEQUENCE(_46ThisLine_49482)){
            _15022 = SEQ_PTR(_46ThisLine_49482)->length;
    }
    else {
        _15022 = 1;
    }
    if (_46bp_49486 <= _15022)
    goto L1; // [40] 101

    /** 		read_line()*/
    _62read_line();

    /** 		while ThisLine[bp] = '_' do*/
L2: 
    _2 = (int)SEQ_PTR(_46ThisLine_49482);
    _15024 = (int)*(((s1_ptr)_2)->base + _46bp_49486);
    if (binary_op_a(NOTEQ, _15024, 95)){
        _15024 = NOVALUE;
        goto L3; // [61] 86
    }
    _15024 = NOVALUE;

    /** 			trimming += 1*/
    _trimming_26214 = _trimming_26214 + 1;

    /** 			bp += 1*/
    _46bp_49486 = _46bp_49486 + 1;

    /** 		end while*/
    goto L2; // [83] 53
L3: 

    /** 		if trimming > 0 then*/
    if (_trimming_26214 <= 0)
    goto L4; // [88] 100

    /** 			ch = getch()*/
    _ch_26210 = _62getch();
    if (!IS_ATOM_INT(_ch_26210)) {
        _1 = (long)(DBL_PTR(_ch_26210)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26210)) && (DBL_PTR(_ch_26210)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26210);
        _ch_26210 = _1;
    }
L4: 
L1: 

    /** 	while 1 do*/
L5: 

    /** 		if ch = END_OF_FILE_CHAR then*/
    if (_ch_26210 != 26)
    goto L6; // [110] 122

    /** 			CompileErr(129, cline)*/
    _46CompileErr(129, _cline_26212, 0);
L6: 

    /** 		if ch = ech then*/
    if (_ch_26210 != _ech_26209)
    goto L7; // [124] 180

    /** 			if ech != '"' then*/
    if (_ech_26209 == 34)
    goto L8; // [130] 139

    /** 				exit*/
    goto L9; // [136] 310
L8: 

    /** 			fch = getch()*/
    _fch_26211 = _62getch();
    if (!IS_ATOM_INT(_fch_26211)) {
        _1 = (long)(DBL_PTR(_fch_26211)->dbl);
        if (UNIQUE(DBL_PTR(_fch_26211)) && (DBL_PTR(_fch_26211)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fch_26211);
        _fch_26211 = _1;
    }

    /** 			if fch = '"' then*/
    if (_fch_26211 != 34)
    goto LA; // [148] 175

    /** 				fch = getch()*/
    _fch_26211 = _62getch();
    if (!IS_ATOM_INT(_fch_26211)) {
        _1 = (long)(DBL_PTR(_fch_26211)->dbl);
        if (UNIQUE(DBL_PTR(_fch_26211)) && (DBL_PTR(_fch_26211)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fch_26211);
        _fch_26211 = _1;
    }

    /** 				if fch = '"' then*/
    if (_fch_26211 != 34)
    goto LB; // [161] 170

    /** 					exit*/
    goto L9; // [167] 310
LB: 

    /** 				ungetch()*/
    _62ungetch();
LA: 

    /** 			ungetch()*/
    _62ungetch();
L7: 

    /** 		if ch != '\r' then*/
    if (_ch_26210 == 13)
    goto LC; // [182] 193

    /** 			string_text &= ch*/
    Append(&_string_text_26213, _string_text_26213, _ch_26210);
LC: 

    /** 		if bp > length(ThisLine) then*/
    if (IS_SEQUENCE(_46ThisLine_49482)){
            _15039 = SEQ_PTR(_46ThisLine_49482)->length;
    }
    else {
        _15039 = 1;
    }
    if (_46bp_49486 <= _15039)
    goto LD; // [202] 298

    /** 			read_line() -- sets bp to 1, btw.*/
    _62read_line();

    /** 			if trimming > 0 then*/
    if (_trimming_26214 <= 0)
    goto LE; // [212] 297

    /** 				while bp <= trimming and bp <= length(ThisLine) do*/
LF: 
    _15042 = (_46bp_49486 <= _trimming_26214);
    if (_15042 == 0) {
        goto L10; // [227] 296
    }
    if (IS_SEQUENCE(_46ThisLine_49482)){
            _15044 = SEQ_PTR(_46ThisLine_49482)->length;
    }
    else {
        _15044 = 1;
    }
    _15045 = (_46bp_49486 <= _15044);
    _15044 = NOVALUE;
    if (_15045 == 0)
    {
        DeRef(_15045);
        _15045 = NOVALUE;
        goto L10; // [243] 296
    }
    else{
        DeRef(_15045);
        _15045 = NOVALUE;
    }

    /** 					ch = ThisLine[bp]*/
    _2 = (int)SEQ_PTR(_46ThisLine_49482);
    _ch_26210 = (int)*(((s1_ptr)_2)->base + _46bp_49486);
    if (!IS_ATOM_INT(_ch_26210)){
        _ch_26210 = (long)DBL_PTR(_ch_26210)->dbl;
    }

    /** 					if ch != ' ' and ch != '\t' then*/
    _15047 = (_ch_26210 != 32);
    if (_15047 == 0) {
        goto L11; // [264] 281
    }
    _15049 = (_ch_26210 != 9);
    if (_15049 == 0)
    {
        DeRef(_15049);
        _15049 = NOVALUE;
        goto L11; // [273] 281
    }
    else{
        DeRef(_15049);
        _15049 = NOVALUE;
    }

    /** 						exit*/
    goto L10; // [278] 296
L11: 

    /** 					bp += 1*/
    _46bp_49486 = _46bp_49486 + 1;

    /** 				end while*/
    goto LF; // [293] 221
L10: 
LE: 
LD: 

    /** 		ch = getch()*/
    _ch_26210 = _62getch();
    if (!IS_ATOM_INT(_ch_26210)) {
        _1 = (long)(DBL_PTR(_ch_26210)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26210)) && (DBL_PTR(_ch_26210)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26210);
        _ch_26210 = _1;
    }

    /** 	end while*/
    goto L5; // [307] 106
L9: 

    /** 	if length(string_text) > 0 and string_text[1] = '\n' then*/
    if (IS_SEQUENCE(_string_text_26213)){
            _15052 = SEQ_PTR(_string_text_26213)->length;
    }
    else {
        _15052 = 1;
    }
    _15053 = (_15052 > 0);
    _15052 = NOVALUE;
    if (_15053 == 0) {
        goto L12; // [319] 389
    }
    _2 = (int)SEQ_PTR(_string_text_26213);
    _15055 = (int)*(((s1_ptr)_2)->base + 1);
    _15056 = (_15055 == 10);
    _15055 = NOVALUE;
    if (_15056 == 0)
    {
        DeRef(_15056);
        _15056 = NOVALUE;
        goto L12; // [332] 389
    }
    else{
        DeRef(_15056);
        _15056 = NOVALUE;
    }

    /** 		string_text = string_text[2 .. $]*/
    if (IS_SEQUENCE(_string_text_26213)){
            _15057 = SEQ_PTR(_string_text_26213)->length;
    }
    else {
        _15057 = 1;
    }
    rhs_slice_target = (object_ptr)&_string_text_26213;
    RHS_Slice(_string_text_26213, 2, _15057);

    /** 		if length(string_text) > 0 and string_text[$] = '\n' then*/
    if (IS_SEQUENCE(_string_text_26213)){
            _15059 = SEQ_PTR(_string_text_26213)->length;
    }
    else {
        _15059 = 1;
    }
    _15060 = (_15059 > 0);
    _15059 = NOVALUE;
    if (_15060 == 0) {
        goto L13; // [354] 388
    }
    if (IS_SEQUENCE(_string_text_26213)){
            _15062 = SEQ_PTR(_string_text_26213)->length;
    }
    else {
        _15062 = 1;
    }
    _2 = (int)SEQ_PTR(_string_text_26213);
    _15063 = (int)*(((s1_ptr)_2)->base + _15062);
    _15064 = (_15063 == 10);
    _15063 = NOVALUE;
    if (_15064 == 0)
    {
        DeRef(_15064);
        _15064 = NOVALUE;
        goto L13; // [370] 388
    }
    else{
        DeRef(_15064);
        _15064 = NOVALUE;
    }

    /** 			string_text = string_text[1 .. $-1]*/
    if (IS_SEQUENCE(_string_text_26213)){
            _15065 = SEQ_PTR(_string_text_26213)->length;
    }
    else {
        _15065 = 1;
    }
    _15066 = _15065 - 1;
    _15065 = NOVALUE;
    rhs_slice_target = (object_ptr)&_string_text_26213;
    RHS_Slice(_string_text_26213, 1, _15066);
L13: 
L12: 

    /** 	return {STRING, NewStringSym(string_text)}*/
    RefDS(_string_text_26213);
    _15068 = _55NewStringSym(_string_text_26213);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 503;
    ((int *)_2)[2] = _15068;
    _15069 = MAKE_SEQ(_1);
    _15068 = NOVALUE;
    DeRefDSi(_string_text_26213);
    DeRef(_15042);
    _15042 = NOVALUE;
    DeRef(_15047);
    _15047 = NOVALUE;
    DeRef(_15053);
    _15053 = NOVALUE;
    DeRef(_15060);
    _15060 = NOVALUE;
    DeRef(_15066);
    _15066 = NOVALUE;
    return _15069;
    ;
}


int _62GetHexString(int _maxnibbles_26300)
{
    int _ch_26301 = NOVALUE;
    int _digit_26302 = NOVALUE;
    int _val_26303 = NOVALUE;
    int _cline_26304 = NOVALUE;
    int _nibble_26305 = NOVALUE;
    int _string_text_26306 = NOVALUE;
    int _15083 = NOVALUE;
    int _15082 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cline = line_number*/
    _cline_26304 = _38line_number_16947;

    /** 	string_text = ""*/
    RefDS(_5);
    DeRef(_string_text_26306);
    _string_text_26306 = _5;

    /** 	nibble = 1*/
    _nibble_26305 = 1;

    /** 	val = -1*/
    DeRef(_val_26303);
    _val_26303 = -1;

    /** 	ch = getch()*/
    _ch_26301 = _62getch();
    if (!IS_ATOM_INT(_ch_26301)) {
        _1 = (long)(DBL_PTR(_ch_26301)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26301)) && (DBL_PTR(_ch_26301)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26301);
        _ch_26301 = _1;
    }

    /** 	while 1 do*/
L1: 

    /** 		if ch = END_OF_FILE_CHAR then*/
    if (_ch_26301 != 26)
    goto L2; // [45] 57

    /** 			CompileErr(129, cline)*/
    _46CompileErr(129, _cline_26304, 0);
L2: 

    /** 		if ch = '"' then*/
    if (_ch_26301 != 34)
    goto L3; // [59] 68

    /** 			exit*/
    goto L4; // [65] 224
L3: 

    /** 		digit = find(ch, "0123456789ABCDEFabcdef_ \t\n\r")*/
    _digit_26302 = find_from(_ch_26301, _15073, 1);

    /** 		if digit = 0 then*/
    if (_digit_26302 != 0)
    goto L5; // [77] 89

    /** 			CompileErr(329)*/
    RefDS(_22663);
    _46CompileErr(329, _22663, 0);
L5: 

    /** 		if digit <= 23 then*/
    if (_digit_26302 > 23)
    goto L6; // [91] 177

    /** 			if digit != 23 then*/
    if (_digit_26302 == 23)
    goto L7; // [97] 212

    /** 				if digit > 16 then*/
    if (_digit_26302 <= 16)
    goto L8; // [103] 114

    /** 					digit -= 6*/
    _digit_26302 = _digit_26302 - 6;
L8: 

    /** 				if nibble = 1 then*/
    if (_nibble_26305 != 1)
    goto L9; // [116] 129

    /** 					val = digit - 1*/
    DeRef(_val_26303);
    _val_26303 = _digit_26302 - 1;
    if ((long)((unsigned long)_val_26303 +(unsigned long) HIGH_BITS) >= 0){
        _val_26303 = NewDouble((double)_val_26303);
    }
    goto LA; // [126] 167
L9: 

    /** 					val = val * 16 + digit - 1*/
    if (IS_ATOM_INT(_val_26303)) {
        if (_val_26303 == (short)_val_26303)
        _15082 = _val_26303 * 16;
        else
        _15082 = NewDouble(_val_26303 * (double)16);
    }
    else {
        _15082 = NewDouble(DBL_PTR(_val_26303)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_15082)) {
        _15083 = _15082 + _digit_26302;
        if ((long)((unsigned long)_15083 + (unsigned long)HIGH_BITS) >= 0) 
        _15083 = NewDouble((double)_15083);
    }
    else {
        _15083 = NewDouble(DBL_PTR(_15082)->dbl + (double)_digit_26302);
    }
    DeRef(_15082);
    _15082 = NOVALUE;
    DeRef(_val_26303);
    if (IS_ATOM_INT(_15083)) {
        _val_26303 = _15083 - 1;
        if ((long)((unsigned long)_val_26303 +(unsigned long) HIGH_BITS) >= 0){
            _val_26303 = NewDouble((double)_val_26303);
        }
    }
    else {
        _val_26303 = NewDouble(DBL_PTR(_15083)->dbl - (double)1);
    }
    DeRef(_15083);
    _15083 = NOVALUE;

    /** 					if nibble = maxnibbles then*/
    if (_nibble_26305 != _maxnibbles_26300)
    goto LB; // [145] 166

    /** 						string_text &= val*/
    Ref(_val_26303);
    Append(&_string_text_26306, _string_text_26306, _val_26303);

    /** 						val = -1*/
    DeRef(_val_26303);
    _val_26303 = -1;

    /** 						nibble = 0*/
    _nibble_26305 = 0;
LB: 
LA: 

    /** 				nibble += 1*/
    _nibble_26305 = _nibble_26305 + 1;
    goto L7; // [174] 212
L6: 

    /** 			if val >= 0 then*/
    if (binary_op_a(LESS, _val_26303, 0)){
        goto LC; // [179] 195
    }

    /** 				string_text &= val*/
    Ref(_val_26303);
    Append(&_string_text_26306, _string_text_26306, _val_26303);

    /** 				val = -1*/
    DeRef(_val_26303);
    _val_26303 = -1;
LC: 

    /** 			nibble = 1*/
    _nibble_26305 = 1;

    /** 			if ch = '\n' then*/
    if (_ch_26301 != 10)
    goto LD; // [202] 211

    /** 				read_line()*/
    _62read_line();
LD: 
L7: 

    /** 		ch = getch()*/
    _ch_26301 = _62getch();
    if (!IS_ATOM_INT(_ch_26301)) {
        _1 = (long)(DBL_PTR(_ch_26301)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26301)) && (DBL_PTR(_ch_26301)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26301);
        _ch_26301 = _1;
    }

    /** 	end while*/
    goto L1; // [221] 41
L4: 

    /** 	if val >= 0 then	*/
    if (binary_op_a(LESS, _val_26303, 0)){
        goto LE; // [226] 237
    }

    /** 		string_text &= val*/
    Ref(_val_26303);
    Append(&_string_text_26306, _string_text_26306, _val_26303);
LE: 

    /** 	return string_text*/
    DeRef(_val_26303);
    return _string_text_26306;
    ;
}


int _62GetBitString()
{
    int _ch_26351 = NOVALUE;
    int _digit_26352 = NOVALUE;
    int _val_26353 = NOVALUE;
    int _cline_26354 = NOVALUE;
    int _bitcnt_26355 = NOVALUE;
    int _string_text_26356 = NOVALUE;
    int _15105 = NOVALUE;
    int _15104 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cline = line_number*/
    _cline_26354 = _38line_number_16947;

    /** 	string_text = ""*/
    RefDS(_5);
    DeRef(_string_text_26356);
    _string_text_26356 = _5;

    /** 	bitcnt = 1*/
    _bitcnt_26355 = 1;

    /** 	val = -1*/
    DeRef(_val_26353);
    _val_26353 = -1;

    /** 	ch = getch()*/
    _ch_26351 = _62getch();
    if (!IS_ATOM_INT(_ch_26351)) {
        _1 = (long)(DBL_PTR(_ch_26351)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26351)) && (DBL_PTR(_ch_26351)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26351);
        _ch_26351 = _1;
    }

    /** 	while 1 do*/
L1: 

    /** 		if ch = END_OF_FILE_CHAR then*/
    if (_ch_26351 != 26)
    goto L2; // [43] 55

    /** 			CompileErr(129, cline)*/
    _46CompileErr(129, _cline_26354, 0);
L2: 

    /** 		if ch = '"' then*/
    if (_ch_26351 != 34)
    goto L3; // [57] 66

    /** 			exit*/
    goto L4; // [63] 186
L3: 

    /** 		digit = find(ch, "01_ \t\n\r")*/
    _digit_26352 = find_from(_ch_26351, _15097, 1);

    /** 		if digit = 0 then*/
    if (_digit_26352 != 0)
    goto L5; // [75] 87

    /** 			CompileErr(329)*/
    RefDS(_22663);
    _46CompileErr(329, _22663, 0);
L5: 

    /** 		if digit <= 3 then*/
    if (_digit_26352 > 3)
    goto L6; // [89] 139

    /** 			if digit != 3 then*/
    if (_digit_26352 == 3)
    goto L7; // [95] 174

    /** 				if bitcnt = 1 then*/
    if (_bitcnt_26355 != 1)
    goto L8; // [101] 114

    /** 					val = digit - 1*/
    DeRef(_val_26353);
    _val_26353 = _digit_26352 - 1;
    if ((long)((unsigned long)_val_26353 +(unsigned long) HIGH_BITS) >= 0){
        _val_26353 = NewDouble((double)_val_26353);
    }
    goto L9; // [111] 129
L8: 

    /** 					val = val * 2 + digit - 1*/
    if (IS_ATOM_INT(_val_26353) && IS_ATOM_INT(_val_26353)) {
        _15104 = _val_26353 + _val_26353;
        if ((long)((unsigned long)_15104 + (unsigned long)HIGH_BITS) >= 0) 
        _15104 = NewDouble((double)_15104);
    }
    else {
        if (IS_ATOM_INT(_val_26353)) {
            _15104 = NewDouble((double)_val_26353 + DBL_PTR(_val_26353)->dbl);
        }
        else {
            if (IS_ATOM_INT(_val_26353)) {
                _15104 = NewDouble(DBL_PTR(_val_26353)->dbl + (double)_val_26353);
            }
            else
            _15104 = NewDouble(DBL_PTR(_val_26353)->dbl + DBL_PTR(_val_26353)->dbl);
        }
    }
    if (IS_ATOM_INT(_15104)) {
        _15105 = _15104 + _digit_26352;
        if ((long)((unsigned long)_15105 + (unsigned long)HIGH_BITS) >= 0) 
        _15105 = NewDouble((double)_15105);
    }
    else {
        _15105 = NewDouble(DBL_PTR(_15104)->dbl + (double)_digit_26352);
    }
    DeRef(_15104);
    _15104 = NOVALUE;
    DeRef(_val_26353);
    if (IS_ATOM_INT(_15105)) {
        _val_26353 = _15105 - 1;
        if ((long)((unsigned long)_val_26353 +(unsigned long) HIGH_BITS) >= 0){
            _val_26353 = NewDouble((double)_val_26353);
        }
    }
    else {
        _val_26353 = NewDouble(DBL_PTR(_15105)->dbl - (double)1);
    }
    DeRef(_15105);
    _15105 = NOVALUE;
L9: 

    /** 				bitcnt += 1*/
    _bitcnt_26355 = _bitcnt_26355 + 1;
    goto L7; // [136] 174
L6: 

    /** 			if val >= 0 then*/
    if (binary_op_a(LESS, _val_26353, 0)){
        goto LA; // [141] 157
    }

    /** 				string_text &= val*/
    Ref(_val_26353);
    Append(&_string_text_26356, _string_text_26356, _val_26353);

    /** 				val = -1*/
    DeRef(_val_26353);
    _val_26353 = -1;
LA: 

    /** 			bitcnt = 1*/
    _bitcnt_26355 = 1;

    /** 			if ch = '\n' then*/
    if (_ch_26351 != 10)
    goto LB; // [164] 173

    /** 				read_line()*/
    _62read_line();
LB: 
L7: 

    /** 		ch = getch()*/
    _ch_26351 = _62getch();
    if (!IS_ATOM_INT(_ch_26351)) {
        _1 = (long)(DBL_PTR(_ch_26351)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26351)) && (DBL_PTR(_ch_26351)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26351);
        _ch_26351 = _1;
    }

    /** 	end while*/
    goto L1; // [183] 39
L4: 

    /** 	if val >= 0 then	*/
    if (binary_op_a(LESS, _val_26353, 0)){
        goto LC; // [188] 199
    }

    /** 		string_text &= val*/
    Ref(_val_26353);
    Append(&_string_text_26356, _string_text_26356, _val_26353);
LC: 

    /** 	return string_text*/
    DeRef(_val_26353);
    return _string_text_26356;
    ;
}


int _62Scanner()
{
    int _fwd_inlined_set_qualified_fwd_at_441_26494 = NOVALUE;
    int _ch_26395 = NOVALUE;
    int _i_26396 = NOVALUE;
    int _sp_26397 = NOVALUE;
    int _prev_Nne_26398 = NOVALUE;
    int _pch_26399 = NOVALUE;
    int _cline_26400 = NOVALUE;
    int _yytext_26401 = NOVALUE;
    int _namespaces_26402 = NOVALUE;
    int _d_26403 = NOVALUE;
    int _tok_26405 = NOVALUE;
    int _is_int_26406 = NOVALUE;
    int _class_26407 = NOVALUE;
    int _name_26408 = NOVALUE;
    int _is_namespace_26467 = NOVALUE;
    int _basetype_26701 = NOVALUE;
    int _hdigit_26741 = NOVALUE;
    int _fch_26886 = NOVALUE;
    int _cnest_27072 = NOVALUE;
    int _ach_27101 = NOVALUE;
    int _32384 = NOVALUE;
    int _32383 = NOVALUE;
    int _32382 = NOVALUE;
    int _32381 = NOVALUE;
    int _32380 = NOVALUE;
    int _32379 = NOVALUE;
    int _32378 = NOVALUE;
    int _15506 = NOVALUE;
    int _15505 = NOVALUE;
    int _15503 = NOVALUE;
    int _15501 = NOVALUE;
    int _15500 = NOVALUE;
    int _15499 = NOVALUE;
    int _15497 = NOVALUE;
    int _15496 = NOVALUE;
    int _15495 = NOVALUE;
    int _15494 = NOVALUE;
    int _15492 = NOVALUE;
    int _15491 = NOVALUE;
    int _15489 = NOVALUE;
    int _15487 = NOVALUE;
    int _15486 = NOVALUE;
    int _15484 = NOVALUE;
    int _15482 = NOVALUE;
    int _15481 = NOVALUE;
    int _15479 = NOVALUE;
    int _15477 = NOVALUE;
    int _15476 = NOVALUE;
    int _15475 = NOVALUE;
    int _15474 = NOVALUE;
    int _15473 = NOVALUE;
    int _15471 = NOVALUE;
    int _15470 = NOVALUE;
    int _15460 = NOVALUE;
    int _15447 = NOVALUE;
    int _15443 = NOVALUE;
    int _15442 = NOVALUE;
    int _15438 = NOVALUE;
    int _15437 = NOVALUE;
    int _15436 = NOVALUE;
    int _15435 = NOVALUE;
    int _15433 = NOVALUE;
    int _15432 = NOVALUE;
    int _15431 = NOVALUE;
    int _15430 = NOVALUE;
    int _15427 = NOVALUE;
    int _15426 = NOVALUE;
    int _15425 = NOVALUE;
    int _15424 = NOVALUE;
    int _15423 = NOVALUE;
    int _15422 = NOVALUE;
    int _15420 = NOVALUE;
    int _15419 = NOVALUE;
    int _15418 = NOVALUE;
    int _15417 = NOVALUE;
    int _15416 = NOVALUE;
    int _15415 = NOVALUE;
    int _15413 = NOVALUE;
    int _15412 = NOVALUE;
    int _15409 = NOVALUE;
    int _15406 = NOVALUE;
    int _15401 = NOVALUE;
    int _15400 = NOVALUE;
    int _15399 = NOVALUE;
    int _15398 = NOVALUE;
    int _15397 = NOVALUE;
    int _15396 = NOVALUE;
    int _15394 = NOVALUE;
    int _15393 = NOVALUE;
    int _15392 = NOVALUE;
    int _15391 = NOVALUE;
    int _15390 = NOVALUE;
    int _15389 = NOVALUE;
    int _15387 = NOVALUE;
    int _15386 = NOVALUE;
    int _15383 = NOVALUE;
    int _15380 = NOVALUE;
    int _15379 = NOVALUE;
    int _15377 = NOVALUE;
    int _15376 = NOVALUE;
    int _15372 = NOVALUE;
    int _15371 = NOVALUE;
    int _15367 = NOVALUE;
    int _15366 = NOVALUE;
    int _15365 = NOVALUE;
    int _15363 = NOVALUE;
    int _15358 = NOVALUE;
    int _15355 = NOVALUE;
    int _15354 = NOVALUE;
    int _15353 = NOVALUE;
    int _15352 = NOVALUE;
    int _15346 = NOVALUE;
    int _15344 = NOVALUE;
    int _15339 = NOVALUE;
    int _15338 = NOVALUE;
    int _15337 = NOVALUE;
    int _15336 = NOVALUE;
    int _15335 = NOVALUE;
    int _15334 = NOVALUE;
    int _15333 = NOVALUE;
    int _15331 = NOVALUE;
    int _15329 = NOVALUE;
    int _15328 = NOVALUE;
    int _15327 = NOVALUE;
    int _15326 = NOVALUE;
    int _15325 = NOVALUE;
    int _15324 = NOVALUE;
    int _15322 = NOVALUE;
    int _15321 = NOVALUE;
    int _15317 = NOVALUE;
    int _15316 = NOVALUE;
    int _15315 = NOVALUE;
    int _15314 = NOVALUE;
    int _15313 = NOVALUE;
    int _15311 = NOVALUE;
    int _15310 = NOVALUE;
    int _15308 = NOVALUE;
    int _15304 = NOVALUE;
    int _15301 = NOVALUE;
    int _15300 = NOVALUE;
    int _15298 = NOVALUE;
    int _15297 = NOVALUE;
    int _15296 = NOVALUE;
    int _15293 = NOVALUE;
    int _15291 = NOVALUE;
    int _15290 = NOVALUE;
    int _15286 = NOVALUE;
    int _15282 = NOVALUE;
    int _15279 = NOVALUE;
    int _15273 = NOVALUE;
    int _15265 = NOVALUE;
    int _15264 = NOVALUE;
    int _15263 = NOVALUE;
    int _15261 = NOVALUE;
    int _15258 = NOVALUE;
    int _15256 = NOVALUE;
    int _15254 = NOVALUE;
    int _15251 = NOVALUE;
    int _15248 = NOVALUE;
    int _15246 = NOVALUE;
    int _15244 = NOVALUE;
    int _15242 = NOVALUE;
    int _15241 = NOVALUE;
    int _15237 = NOVALUE;
    int _15234 = NOVALUE;
    int _15232 = NOVALUE;
    int _15229 = NOVALUE;
    int _15222 = NOVALUE;
    int _15220 = NOVALUE;
    int _15217 = NOVALUE;
    int _15211 = NOVALUE;
    int _15210 = NOVALUE;
    int _15209 = NOVALUE;
    int _15208 = NOVALUE;
    int _15207 = NOVALUE;
    int _15206 = NOVALUE;
    int _15205 = NOVALUE;
    int _15203 = NOVALUE;
    int _15201 = NOVALUE;
    int _15199 = NOVALUE;
    int _15197 = NOVALUE;
    int _15195 = NOVALUE;
    int _15194 = NOVALUE;
    int _15193 = NOVALUE;
    int _15192 = NOVALUE;
    int _15191 = NOVALUE;
    int _15189 = NOVALUE;
    int _15186 = NOVALUE;
    int _15184 = NOVALUE;
    int _15183 = NOVALUE;
    int _15182 = NOVALUE;
    int _15180 = NOVALUE;
    int _15175 = NOVALUE;
    int _15171 = NOVALUE;
    int _15168 = NOVALUE;
    int _15166 = NOVALUE;
    int _15165 = NOVALUE;
    int _15163 = NOVALUE;
    int _15161 = NOVALUE;
    int _15159 = NOVALUE;
    int _15158 = NOVALUE;
    int _15157 = NOVALUE;
    int _15155 = NOVALUE;
    int _15150 = NOVALUE;
    int _15147 = NOVALUE;
    int _15145 = NOVALUE;
    int _15142 = NOVALUE;
    int _15141 = NOVALUE;
    int _15139 = NOVALUE;
    int _15138 = NOVALUE;
    int _15137 = NOVALUE;
    int _15136 = NOVALUE;
    int _15135 = NOVALUE;
    int _15134 = NOVALUE;
    int _15133 = NOVALUE;
    int _15132 = NOVALUE;
    int _15131 = NOVALUE;
    int _15130 = NOVALUE;
    int _15129 = NOVALUE;
    int _15128 = NOVALUE;
    int _15127 = NOVALUE;
    int _15122 = NOVALUE;
    int _15120 = NOVALUE;
    int _15117 = NOVALUE;
    int _15115 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer is_int, class*/

    /** 	sequence name*/

    /** 	while TRUE do*/
L1: 
    if (_9TRUE_428 == 0)
    {
        goto L2; // [12] 3868
    }
    else{
    }

    /** 		ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 		while ch = ' ' or ch = '\t' do*/
L3: 
    _15115 = (_ch_26395 == 32);
    if (_15115 != 0) {
        goto L4; // [31] 44
    }
    _15117 = (_ch_26395 == 9);
    if (_15117 == 0)
    {
        DeRef(_15117);
        _15117 = NOVALUE;
        goto L5; // [40] 56
    }
    else{
        DeRef(_15117);
        _15117 = NOVALUE;
    }
L4: 

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 		end while*/
    goto L3; // [53] 27
L5: 

    /** 		class = char_class[ch]*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _class_26407 = (int)*(((s1_ptr)_2)->base + _ch_26395);

    /** 		if class = LETTER or ch = '_' then*/
    _15120 = (_class_26407 == -2);
    if (_15120 != 0) {
        goto L6; // [72] 85
    }
    _15122 = (_ch_26395 == 95);
    if (_15122 == 0)
    {
        DeRef(_15122);
        _15122 = NOVALUE;
        goto L7; // [81] 1282
    }
    else{
        DeRef(_15122);
        _15122 = NOVALUE;
    }
L6: 

    /** 			sp = bp*/
    _sp_26397 = _46bp_49486;

    /** 			pch = ch*/
    _pch_26399 = _ch_26395;

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			if ch = '"' then*/
    if (_ch_26395 != 34)
    goto L8; // [108] 222

    /** 				switch pch do*/
    _0 = _pch_26399;
    switch ( _0 ){ 

        /** 					case 'x' then*/
        case 120:

        /** 						return {STRING, NewStringSym(GetHexString(2))}*/
        _15127 = _62GetHexString(2);
        _15128 = _55NewStringSym(_15127);
        _15127 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15128;
        _15129 = MAKE_SEQ(_1);
        _15128 = NOVALUE;
        DeRef(_yytext_26401);
        DeRef(_namespaces_26402);
        DeRef(_d_26403);
        DeRef(_tok_26405);
        DeRef(_name_26408);
        DeRef(_15115);
        _15115 = NOVALUE;
        DeRef(_15120);
        _15120 = NOVALUE;
        return _15129;
        goto L9; // [143] 221

        /** 					case 'u' then*/
        case 117:

        /** 						return {STRING, NewStringSym(GetHexString(4))}*/
        _15130 = _62GetHexString(4);
        _15131 = _55NewStringSym(_15130);
        _15130 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15131;
        _15132 = MAKE_SEQ(_1);
        _15131 = NOVALUE;
        DeRef(_yytext_26401);
        DeRef(_namespaces_26402);
        DeRef(_d_26403);
        DeRef(_tok_26405);
        DeRef(_name_26408);
        DeRef(_15115);
        _15115 = NOVALUE;
        DeRef(_15120);
        _15120 = NOVALUE;
        DeRef(_15129);
        _15129 = NOVALUE;
        return _15132;
        goto L9; // [169] 221

        /** 					case 'U' then*/
        case 85:

        /** 						return {STRING, NewStringSym(GetHexString(8))}*/
        _15133 = _62GetHexString(8);
        _15134 = _55NewStringSym(_15133);
        _15133 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15134;
        _15135 = MAKE_SEQ(_1);
        _15134 = NOVALUE;
        DeRef(_yytext_26401);
        DeRef(_namespaces_26402);
        DeRef(_d_26403);
        DeRef(_tok_26405);
        DeRef(_name_26408);
        DeRef(_15115);
        _15115 = NOVALUE;
        DeRef(_15120);
        _15120 = NOVALUE;
        DeRef(_15129);
        _15129 = NOVALUE;
        DeRef(_15132);
        _15132 = NOVALUE;
        return _15135;
        goto L9; // [195] 221

        /** 					case 'b' then*/
        case 98:

        /** 						return {STRING, NewStringSym(GetBitString())}*/
        _15136 = _62GetBitString();
        _15137 = _55NewStringSym(_15136);
        _15136 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 503;
        ((int *)_2)[2] = _15137;
        _15138 = MAKE_SEQ(_1);
        _15137 = NOVALUE;
        DeRef(_yytext_26401);
        DeRef(_namespaces_26402);
        DeRef(_d_26403);
        DeRef(_tok_26405);
        DeRef(_name_26408);
        DeRef(_15115);
        _15115 = NOVALUE;
        DeRef(_15120);
        _15120 = NOVALUE;
        DeRef(_15129);
        _15129 = NOVALUE;
        DeRef(_15132);
        _15132 = NOVALUE;
        DeRef(_15135);
        _15135 = NOVALUE;
        return _15138;
    ;}L9: 
L8: 

    /** 			while id_char[ch] do*/
LA: 
    _2 = (int)SEQ_PTR(_62id_char_24631);
    _15139 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15139 == 0)
    {
        _15139 = NOVALUE;
        goto LB; // [233] 248
    }
    else{
        _15139 = NOVALUE;
    }

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			end while*/
    goto LA; // [245] 227
LB: 

    /** 			yytext = ThisLine[sp-1..bp-2]*/
    _15141 = _sp_26397 - 1;
    if ((long)((unsigned long)_15141 +(unsigned long) HIGH_BITS) >= 0){
        _15141 = NewDouble((double)_15141);
    }
    _15142 = _46bp_49486 - 2;
    rhs_slice_target = (object_ptr)&_yytext_26401;
    RHS_Slice(_46ThisLine_49482, _15141, _15142);

    /** 			ungetch()*/
    _62ungetch();

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			while ch = ' ' or ch = '\t' do*/
LC: 
    _15145 = (_ch_26395 == 32);
    if (_15145 != 0) {
        goto LD; // [287] 300
    }
    _15147 = (_ch_26395 == 9);
    if (_15147 == 0)
    {
        DeRef(_15147);
        _15147 = NOVALUE;
        goto LE; // [296] 312
    }
    else{
        DeRef(_15147);
        _15147 = NOVALUE;
    }
LD: 

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			end while*/
    goto LC; // [309] 283
LE: 

    /** 			integer is_namespace*/

    /** 			if might_be_namespace then*/
    if (_62might_be_namespace_26199 == 0)
    {
        goto LF; // [318] 361
    }
    else{
    }

    /** 				tok = keyfind(yytext, -1, , -1 )*/
    RefDS(_yytext_26401);
    _32384 = _55hashfn(_yytext_26401);
    RefDS(_yytext_26401);
    _0 = _tok_26405;
    _tok_26405 = _55keyfind(_yytext_26401, -1, _38current_file_no_16946, -1, _32384);
    DeRef(_0);
    _32384 = NOVALUE;

    /** 				is_namespace = tok[T_ID] = NAMESPACE*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15150 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_15150)) {
        _is_namespace_26467 = (_15150 == 523);
    }
    else {
        _is_namespace_26467 = binary_op(EQUALS, _15150, 523);
    }
    _15150 = NOVALUE;
    if (!IS_ATOM_INT(_is_namespace_26467)) {
        _1 = (long)(DBL_PTR(_is_namespace_26467)->dbl);
        if (UNIQUE(DBL_PTR(_is_namespace_26467)) && (DBL_PTR(_is_namespace_26467)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_namespace_26467);
        _is_namespace_26467 = _1;
    }

    /** 				might_be_namespace = 0*/
    _62might_be_namespace_26199 = 0;
    goto L10; // [358] 384
LF: 

    /** 				is_namespace = ch = ':'*/
    _is_namespace_26467 = (_ch_26395 == 58);

    /** 				tok = keyfind(yytext, -1, , is_namespace )*/
    RefDS(_yytext_26401);
    _32383 = _55hashfn(_yytext_26401);
    RefDS(_yytext_26401);
    _0 = _tok_26405;
    _tok_26405 = _55keyfind(_yytext_26401, -1, _38current_file_no_16946, _is_namespace_26467, _32383);
    DeRef(_0);
    _32383 = NOVALUE;
L10: 

    /** 			if not is_namespace then*/
    if (_is_namespace_26467 != 0)
    goto L11; // [388] 396

    /** 				ungetch()*/
    _62ungetch();
L11: 

    /** 			if is_namespace then*/
    if (_is_namespace_26467 == 0)
    {
        goto L12; // [398] 1119
    }
    else{
    }

    /** 				namespaces = yytext*/
    RefDS(_yytext_26401);
    DeRef(_namespaces_26402);
    _namespaces_26402 = _yytext_26401;

    /** 				if tok[T_ID] = NAMESPACE then -- known namespace*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15155 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15155, 523)){
        _15155 = NOVALUE;
        goto L13; // [420] 974
    }
    _15155 = NOVALUE;

    /** 					set_qualified_fwd( SymTab[tok[T_SYM]][S_OBJ] )*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15157 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_15157)){
        _15158 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15157)->dbl));
    }
    else{
        _15158 = (int)*(((s1_ptr)_2)->base + _15157);
    }
    _2 = (int)SEQ_PTR(_15158);
    _15159 = (int)*(((s1_ptr)_2)->base + 1);
    _15158 = NOVALUE;
    Ref(_15159);
    DeRef(_fwd_inlined_set_qualified_fwd_at_441_26494);
    _fwd_inlined_set_qualified_fwd_at_441_26494 = _15159;
    _15159 = NOVALUE;
    if (!IS_ATOM_INT(_fwd_inlined_set_qualified_fwd_at_441_26494)) {
        _1 = (long)(DBL_PTR(_fwd_inlined_set_qualified_fwd_at_441_26494)->dbl);
        if (UNIQUE(DBL_PTR(_fwd_inlined_set_qualified_fwd_at_441_26494)) && (DBL_PTR(_fwd_inlined_set_qualified_fwd_at_441_26494)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fwd_inlined_set_qualified_fwd_at_441_26494);
        _fwd_inlined_set_qualified_fwd_at_441_26494 = _1;
    }

    /** 	qualified_fwd = fwd*/
    _62qualified_fwd_24655 = _fwd_inlined_set_qualified_fwd_at_441_26494;

    /** end procedure*/
    goto L14; // [456] 459
L14: 
    DeRef(_fwd_inlined_set_qualified_fwd_at_441_26494);
    _fwd_inlined_set_qualified_fwd_at_441_26494 = NOVALUE;

    /** 					ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 					while ch = ' ' or ch = '\t' do*/
L15: 
    _15161 = (_ch_26395 == 32);
    if (_15161 != 0) {
        goto L16; // [477] 490
    }
    _15163 = (_ch_26395 == 9);
    if (_15163 == 0)
    {
        DeRef(_15163);
        _15163 = NOVALUE;
        goto L17; // [486] 502
    }
    else{
        DeRef(_15163);
        _15163 = NOVALUE;
    }
L16: 

    /** 						ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 					end while*/
    goto L15; // [499] 473
L17: 

    /** 					yytext = ""*/
    RefDS(_5);
    DeRef(_yytext_26401);
    _yytext_26401 = _5;

    /** 					if char_class[ch] = LETTER or ch = '_' then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15165 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    _15166 = (_15165 == -2);
    _15165 = NOVALUE;
    if (_15166 != 0) {
        goto L18; // [523] 536
    }
    _15168 = (_ch_26395 == 95);
    if (_15168 == 0)
    {
        DeRef(_15168);
        _15168 = NOVALUE;
        goto L19; // [532] 589
    }
    else{
        DeRef(_15168);
        _15168 = NOVALUE;
    }
L18: 

    /** 						yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);

    /** 						ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 						while id_char[ch] = TRUE do*/
L1A: 
    _2 = (int)SEQ_PTR(_62id_char_24631);
    _15171 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15171 != _9TRUE_428)
    goto L1B; // [562] 584

    /** 							yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);

    /** 							ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 						end while*/
    goto L1A; // [581] 554
L1B: 

    /** 						ungetch()*/
    _62ungetch();
L19: 

    /** 					if length(yytext) = 0 then*/
    if (IS_SEQUENCE(_yytext_26401)){
            _15175 = SEQ_PTR(_yytext_26401)->length;
    }
    else {
        _15175 = 1;
    }
    if (_15175 != 0)
    goto L1C; // [594] 606

    /** 						CompileErr(32)*/
    RefDS(_22663);
    _46CompileErr(32, _22663, 0);
L1C: 

    /** 				    if Parser_mode = PAM_RECORD then*/
    if (_38Parser_mode_17071 != 1)
    goto L1D; // [612] 773

    /** 		                Recorded = append(Recorded,yytext)*/
    RefDS(_yytext_26401);
    Append(&_38Recorded_17072, _38Recorded_17072, _yytext_26401);

    /** 		                Ns_recorded = append(Ns_recorded,namespaces)*/
    RefDS(_namespaces_26402);
    Append(&_38Ns_recorded_17073, _38Ns_recorded_17073, _namespaces_26402);

    /** 		                Ns_recorded_sym &= tok[T_SYM]*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15180 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_38Ns_recorded_sym_17075) && IS_ATOM(_15180)) {
        Ref(_15180);
        Append(&_38Ns_recorded_sym_17075, _38Ns_recorded_sym_17075, _15180);
    }
    else if (IS_ATOM(_38Ns_recorded_sym_17075) && IS_SEQUENCE(_15180)) {
    }
    else {
        Concat((object_ptr)&_38Ns_recorded_sym_17075, _38Ns_recorded_sym_17075, _15180);
    }
    _15180 = NOVALUE;

    /** 		                prev_Nne = No_new_entry*/
    _prev_Nne_26398 = _55No_new_entry_48227;

    /** 						No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 						tok = keyfind(yytext, SymTab[tok[T_SYM]][S_OBJ])*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15182 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_15182)){
        _15183 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15182)->dbl));
    }
    else{
        _15183 = (int)*(((s1_ptr)_2)->base + _15182);
    }
    _2 = (int)SEQ_PTR(_15183);
    _15184 = (int)*(((s1_ptr)_2)->base + 1);
    _15183 = NOVALUE;
    RefDS(_yytext_26401);
    _32382 = _55hashfn(_yytext_26401);
    RefDS(_yytext_26401);
    Ref(_15184);
    _0 = _tok_26405;
    _tok_26405 = _55keyfind(_yytext_26401, _15184, _38current_file_no_16946, 0, _32382);
    DeRef(_0);
    _15184 = NOVALUE;
    _32382 = NOVALUE;

    /** 						if tok[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15186 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15186, 509)){
        _15186 = NOVALUE;
        goto L1E; // [712] 729
    }
    _15186 = NOVALUE;

    /** 							Recorded_sym &= 0 -- must resolve on call site*/
    Append(&_38Recorded_sym_17074, _38Recorded_sym_17074, 0);
    goto L1F; // [726] 746
L1E: 

    /** 							Recorded_sym &= tok[T_SYM] -- fallback when symbol is undefined on call site*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15189 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_38Recorded_sym_17074) && IS_ATOM(_15189)) {
        Ref(_15189);
        Append(&_38Recorded_sym_17074, _38Recorded_sym_17074, _15189);
    }
    else if (IS_ATOM(_38Recorded_sym_17074) && IS_SEQUENCE(_15189)) {
    }
    else {
        Concat((object_ptr)&_38Recorded_sym_17074, _38Recorded_sym_17074, _15189);
    }
    _15189 = NOVALUE;
L1F: 

    /** 		                No_new_entry = prev_Nne*/
    _55No_new_entry_48227 = _prev_Nne_26398;

    /** 		                return {RECORDED,length(Recorded)}*/
    if (IS_SEQUENCE(_38Recorded_17072)){
            _15191 = SEQ_PTR(_38Recorded_17072)->length;
    }
    else {
        _15191 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _15191;
    _15192 = MAKE_SEQ(_1);
    _15191 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15115);
    _15115 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15171 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    _15182 = NOVALUE;
    return _15192;
    goto L20; // [770] 915
L1D: 

    /** 						tok = keyfind(yytext, SymTab[tok[T_SYM]][S_OBJ])*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15193 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_15193)){
        _15194 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15193)->dbl));
    }
    else{
        _15194 = (int)*(((s1_ptr)_2)->base + _15193);
    }
    _2 = (int)SEQ_PTR(_15194);
    _15195 = (int)*(((s1_ptr)_2)->base + 1);
    _15194 = NOVALUE;
    RefDS(_yytext_26401);
    _32381 = _55hashfn(_yytext_26401);
    RefDS(_yytext_26401);
    Ref(_15195);
    _0 = _tok_26405;
    _tok_26405 = _55keyfind(_yytext_26401, _15195, _38current_file_no_16946, 0, _32381);
    DeRef(_0);
    _15195 = NOVALUE;
    _32381 = NOVALUE;

    /** 						if tok[T_ID] = VARIABLE then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15197 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15197, -100)){
        _15197 = NOVALUE;
        goto L21; // [817] 834
    }
    _15197 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_VARIABLE*/
    _2 = (int)SEQ_PTR(_tok_26405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26405 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 512;
    DeRef(_1);
    goto L22; // [831] 914
L21: 

    /** 						elsif tok[T_ID] = FUNC then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15199 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15199, 501)){
        _15199 = NOVALUE;
        goto L23; // [844] 861
    }
    _15199 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_FUNC*/
    _2 = (int)SEQ_PTR(_tok_26405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26405 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 520;
    DeRef(_1);
    goto L22; // [858] 914
L23: 

    /** 						elsif tok[T_ID] = PROC then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15201 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15201, 27)){
        _15201 = NOVALUE;
        goto L24; // [871] 888
    }
    _15201 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_PROC*/
    _2 = (int)SEQ_PTR(_tok_26405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26405 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 521;
    DeRef(_1);
    goto L22; // [885] 914
L24: 

    /** 						elsif tok[T_ID] = TYPE then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15203 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15203, 504)){
        _15203 = NOVALUE;
        goto L25; // [898] 913
    }
    _15203 = NOVALUE;

    /** 							tok[T_ID] = QUALIFIED_TYPE*/
    _2 = (int)SEQ_PTR(_tok_26405);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _tok_26405 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 522;
    DeRef(_1);
L25: 
L22: 
L20: 

    /** 					if atom( tok[T_SYM] ) and  SymTab[tok[T_SYM]][S_SCOPE] != SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15205 = (int)*(((s1_ptr)_2)->base + 2);
    _15206 = IS_ATOM(_15205);
    _15205 = NOVALUE;
    if (_15206 == 0) {
        goto L26; // [926] 1269
    }
    _2 = (int)SEQ_PTR(_tok_26405);
    _15208 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_15208)){
        _15209 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15208)->dbl));
    }
    else{
        _15209 = (int)*(((s1_ptr)_2)->base + _15208);
    }
    _2 = (int)SEQ_PTR(_15209);
    _15210 = (int)*(((s1_ptr)_2)->base + 4);
    _15209 = NOVALUE;
    if (IS_ATOM_INT(_15210)) {
        _15211 = (_15210 != 9);
    }
    else {
        _15211 = binary_op(NOTEQ, _15210, 9);
    }
    _15210 = NOVALUE;
    if (_15211 == 0) {
        DeRef(_15211);
        _15211 = NOVALUE;
        goto L26; // [955] 1269
    }
    else {
        if (!IS_ATOM_INT(_15211) && DBL_PTR(_15211)->dbl == 0.0){
            DeRef(_15211);
            _15211 = NOVALUE;
            goto L26; // [955] 1269
        }
        DeRef(_15211);
        _15211 = NOVALUE;
    }
    DeRef(_15211);
    _15211 = NOVALUE;

    /** 						set_qualified_fwd( -1 )*/

    /** 	qualified_fwd = fwd*/
    _62qualified_fwd_24655 = -1;

    /** end procedure*/
    goto L26; // [967] 1269
    goto L26; // [971] 1269
L13: 

    /** 					ungetch()*/
    _62ungetch();

    /** 				    if Parser_mode = PAM_RECORD then*/
    if (_38Parser_mode_17071 != 1)
    goto L26; // [984] 1269

    /** 		                Ns_recorded &= 0*/
    Append(&_38Ns_recorded_17073, _38Ns_recorded_17073, 0);

    /** 		                Ns_recorded_sym &= 0*/
    Append(&_38Ns_recorded_sym_17075, _38Ns_recorded_sym_17075, 0);

    /** 		                Recorded = append(Recorded,yytext)*/
    RefDS(_yytext_26401);
    Append(&_38Recorded_17072, _38Recorded_17072, _yytext_26401);

    /** 		                prev_Nne = No_new_entry*/
    _prev_Nne_26398 = _55No_new_entry_48227;

    /** 						No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 						tok = keyfind(yytext, -1)*/
    RefDS(_yytext_26401);
    _32380 = _55hashfn(_yytext_26401);
    RefDS(_yytext_26401);
    _0 = _tok_26405;
    _tok_26405 = _55keyfind(_yytext_26401, -1, _38current_file_no_16946, 0, _32380);
    DeRef(_0);
    _32380 = NOVALUE;

    /** 						if tok[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15217 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15217, 509)){
        _15217 = NOVALUE;
        goto L27; // [1060] 1077
    }
    _15217 = NOVALUE;

    /** 							Recorded_sym &= 0 -- must resolve on call site*/
    Append(&_38Recorded_sym_17074, _38Recorded_sym_17074, 0);
    goto L28; // [1074] 1094
L27: 

    /** 							Recorded_sym &= tok[T_SYM] -- fallback when symbol is undefined on call site*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15220 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_38Recorded_sym_17074) && IS_ATOM(_15220)) {
        Ref(_15220);
        Append(&_38Recorded_sym_17074, _38Recorded_sym_17074, _15220);
    }
    else if (IS_ATOM(_38Recorded_sym_17074) && IS_SEQUENCE(_15220)) {
    }
    else {
        Concat((object_ptr)&_38Recorded_sym_17074, _38Recorded_sym_17074, _15220);
    }
    _15220 = NOVALUE;
L28: 

    /** 		                No_new_entry = prev_Nne*/
    _55No_new_entry_48227 = _prev_Nne_26398;

    /** 		                tok = {RECORDED,length(Recorded)}*/
    if (IS_SEQUENCE(_38Recorded_17072)){
            _15222 = SEQ_PTR(_38Recorded_17072)->length;
    }
    else {
        _15222 = 1;
    }
    DeRef(_tok_26405);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _15222;
    _tok_26405 = MAKE_SEQ(_1);
    _15222 = NOVALUE;
    goto L26; // [1116] 1269
L12: 

    /** 				set_qualified_fwd( -1 )*/

    /** 	qualified_fwd = fwd*/
    _62qualified_fwd_24655 = -1;

    /** end procedure*/
    goto L29; // [1128] 1131
L29: 

    /** 			    if Parser_mode = PAM_RECORD then*/
    if (_38Parser_mode_17071 != 1)
    goto L2A; // [1137] 1268

    /** 	                Ns_recorded_sym &= 0*/
    Append(&_38Ns_recorded_sym_17075, _38Ns_recorded_sym_17075, 0);

    /** 						Recorded = append(Recorded, yytext)*/
    RefDS(_yytext_26401);
    Append(&_38Recorded_17072, _38Recorded_17072, _yytext_26401);

    /** 		                Ns_recorded &= 0*/
    Append(&_38Ns_recorded_17073, _38Ns_recorded_17073, 0);

    /** 		                prev_Nne = No_new_entry*/
    _prev_Nne_26398 = _55No_new_entry_48227;

    /** 						No_new_entry = 1*/
    _55No_new_entry_48227 = 1;

    /** 						tok = keyfind(yytext, -1)*/
    RefDS(_yytext_26401);
    _32379 = _55hashfn(_yytext_26401);
    RefDS(_yytext_26401);
    _0 = _tok_26405;
    _tok_26405 = _55keyfind(_yytext_26401, -1, _38current_file_no_16946, 0, _32379);
    DeRef(_0);
    _32379 = NOVALUE;

    /** 						if tok[T_ID] = IGNORED then*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15229 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15229, 509)){
        _15229 = NOVALUE;
        goto L2B; // [1213] 1230
    }
    _15229 = NOVALUE;

    /** 							Recorded_sym &= 0 -- must resolve on call site*/
    Append(&_38Recorded_sym_17074, _38Recorded_sym_17074, 0);
    goto L2C; // [1227] 1247
L2B: 

    /** 							Recorded_sym &= tok[T_SYM] -- fallback when symbol is undefined on call site*/
    _2 = (int)SEQ_PTR(_tok_26405);
    _15232 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_SEQUENCE(_38Recorded_sym_17074) && IS_ATOM(_15232)) {
        Ref(_15232);
        Append(&_38Recorded_sym_17074, _38Recorded_sym_17074, _15232);
    }
    else if (IS_ATOM(_38Recorded_sym_17074) && IS_SEQUENCE(_15232)) {
    }
    else {
        Concat((object_ptr)&_38Recorded_sym_17074, _38Recorded_sym_17074, _15232);
    }
    _15232 = NOVALUE;
L2C: 

    /** 		                No_new_entry = prev_Nne*/
    _55No_new_entry_48227 = _prev_Nne_26398;

    /** 	                tok = {RECORDED, length(Recorded)}*/
    if (IS_SEQUENCE(_38Recorded_17072)){
            _15234 = SEQ_PTR(_38Recorded_17072)->length;
    }
    else {
        _15234 = 1;
    }
    DeRef(_tok_26405);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 508;
    ((int *)_2)[2] = _15234;
    _tok_26405 = MAKE_SEQ(_1);
    _15234 = NOVALUE;
L2A: 
L26: 

    /** 			return tok*/
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_name_26408);
    DeRef(_15115);
    _15115 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15171 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15193 = NOVALUE;
    _15208 = NOVALUE;
    return _tok_26405;
    goto L1; // [1279] 10
L7: 

    /** 		elsif class < ILLEGAL_CHAR then*/
    if (_class_26407 >= -20)
    goto L2D; // [1286] 1303

    /** 			return {class, 0}  -- brackets, punctuation, eof, illegal char etc.*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _class_26407;
    ((int *)_2)[2] = 0;
    _15237 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15115);
    _15115 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15171 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15193 = NOVALUE;
    _15208 = NOVALUE;
    return _15237;
    goto L1; // [1300] 10
L2D: 

    /** 		elsif class = ILLEGAL_CHAR then*/
    if (_class_26407 != -20)
    goto L2E; // [1307] 1321

    /** 			CompileErr(101)*/
    RefDS(_22663);
    _46CompileErr(101, _22663, 0);
    goto L1; // [1318] 10
L2E: 

    /** 		elsif class = NEWLINE then*/
    if (_class_26407 != -6)
    goto L2F; // [1325] 1351

    /** 			if start_include then*/
    if (_62start_include_24623 == 0)
    {
        goto L30; // [1333] 1343
    }
    else{
    }

    /** 				IncludePush()*/
    _62IncludePush();
    goto L1; // [1340] 10
L30: 

    /** 				read_line()*/
    _62read_line();
    goto L1; // [1348] 10
L2F: 

    /** 		elsif class = EQUALS then*/
    if (_class_26407 != 3)
    goto L31; // [1355] 1372

    /** 			return {class, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _class_26407;
    ((int *)_2)[2] = 0;
    _15241 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15115);
    _15115 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15171 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15193 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    return _15241;
    goto L1; // [1369] 10
L31: 

    /** 		elsif class = DOT or class = DIGIT then*/
    _15242 = (_class_26407 == -3);
    if (_15242 != 0) {
        goto L32; // [1380] 1395
    }
    _15244 = (_class_26407 == -7);
    if (_15244 == 0)
    {
        DeRef(_15244);
        _15244 = NOVALUE;
        goto L33; // [1391] 2225
    }
    else{
        DeRef(_15244);
        _15244 = NOVALUE;
    }
L32: 

    /** 			integer basetype*/

    /** 			if class = DOT then*/
    if (_class_26407 != -3)
    goto L34; // [1401] 1435

    /** 				if getch() = '.' then*/
    _15246 = _62getch();
    if (binary_op_a(NOTEQ, _15246, 46)){
        DeRef(_15246);
        _15246 = NOVALUE;
        goto L35; // [1410] 1429
    }
    DeRef(_15246);
    _15246 = NOVALUE;

    /** 					return {SLICE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 513;
    ((int *)_2)[2] = 0;
    _15248 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15115);
    _15115 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15171 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15193 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    return _15248;
    goto L36; // [1426] 1434
L35: 

    /** 					ungetch()*/
    _62ungetch();
L36: 
L34: 

    /** 			yytext = {ch}*/
    _0 = _yytext_26401;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ch_26395;
    _yytext_26401 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 			is_int = (ch != '.')*/
    _is_int_26406 = (_ch_26395 != 46);

    /** 			basetype = -1 -- default is decimal*/
    _basetype_26701 = -1;

    /** 			while 1 with entry do*/
    goto L37; // [1454] 1645
L38: 

    /** 				if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15251 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15251 != -7)
    goto L39; // [1467] 1480

    /** 					yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);
    goto L3A; // [1477] 1642
L39: 

    /** 				elsif equal(yytext, "0") then*/
    if (_yytext_26401 == _14869)
    _15254 = 1;
    else if (IS_ATOM_INT(_yytext_26401) && IS_ATOM_INT(_14869))
    _15254 = 0;
    else
    _15254 = (compare(_yytext_26401, _14869) == 0);
    if (_15254 == 0)
    {
        _15254 = NOVALUE;
        goto L3B; // [1486] 1581
    }
    else{
        _15254 = NOVALUE;
    }

    /** 					basetype = find(ch, nbasecode)*/
    _basetype_26701 = find_from(_ch_26395, _62nbasecode_26203, 1);

    /** 					if basetype > length(nbase) then*/
    if (IS_SEQUENCE(_62nbase_26202)){
            _15256 = SEQ_PTR(_62nbase_26202)->length;
    }
    else {
        _15256 = 1;
    }
    if (_basetype_26701 <= _15256)
    goto L3C; // [1501] 1515

    /** 						basetype -= length(nbase)*/
    if (IS_SEQUENCE(_62nbase_26202)){
            _15258 = SEQ_PTR(_62nbase_26202)->length;
    }
    else {
        _15258 = 1;
    }
    _basetype_26701 = _basetype_26701 - _15258;
    _15258 = NOVALUE;
L3C: 

    /** 					if basetype = 0 then*/
    if (_basetype_26701 != 0)
    goto L3D; // [1517] 1572

    /** 						if char_class[ch] = LETTER then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15261 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15261 != -2)
    goto L3E; // [1531] 1562

    /** 							if ch != 'e' and ch != 'E' then*/
    _15263 = (_ch_26395 != 101);
    if (_15263 == 0) {
        goto L3F; // [1541] 1561
    }
    _15265 = (_ch_26395 != 69);
    if (_15265 == 0)
    {
        DeRef(_15265);
        _15265 = NOVALUE;
        goto L3F; // [1550] 1561
    }
    else{
        DeRef(_15265);
        _15265 = NOVALUE;
    }

    /** 								CompileErr(105, ch)*/
    _46CompileErr(105, _ch_26395, 0);
L3F: 
L3E: 

    /** 						basetype = -1 -- decimal*/
    _basetype_26701 = -1;

    /** 						exit*/
    goto L40; // [1569] 1657
L3D: 

    /** 					yytext &= '0'*/
    Append(&_yytext_26401, _yytext_26401, 48);
    goto L3A; // [1578] 1642
L3B: 

    /** 				elsif basetype = 4 then -- hexadecimal*/
    if (_basetype_26701 != 4)
    goto L40; // [1583] 1657

    /** 					integer hdigit*/

    /** 					hdigit = find(ch, "ABCDEFabcdef")*/
    _hdigit_26741 = find_from(_ch_26395, _15268, 1);

    /** 					if hdigit = 0 then*/
    if (_hdigit_26741 != 0)
    goto L41; // [1598] 1609

    /** 						exit*/
    goto L40; // [1606] 1657
L41: 

    /** 					if hdigit > 6 then*/
    if (_hdigit_26741 <= 6)
    goto L42; // [1611] 1622

    /** 						hdigit -= 6*/
    _hdigit_26741 = _hdigit_26741 - 6;
L42: 

    /** 					yytext &= hexasc[hdigit]*/
    _2 = (int)SEQ_PTR(_62hexasc_26205);
    _15273 = (int)*(((s1_ptr)_2)->base + _hdigit_26741);
    if (IS_SEQUENCE(_yytext_26401) && IS_ATOM(_15273)) {
        Ref(_15273);
        Append(&_yytext_26401, _yytext_26401, _15273);
    }
    else if (IS_ATOM(_yytext_26401) && IS_SEQUENCE(_15273)) {
    }
    else {
        Concat((object_ptr)&_yytext_26401, _yytext_26401, _15273);
    }
    _15273 = NOVALUE;
    goto L3A; // [1634] 1642

    /** 					exit*/
    goto L40; // [1639] 1657
L3A: 

    /** 			entry*/
L37: 

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			end while*/
    goto L38; // [1654] 1457
L40: 

    /** 			if ch = '.' then*/
    if (_ch_26395 != 46)
    goto L43; // [1659] 1794

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 				if ch = '.' then*/
    if (_ch_26395 != 46)
    goto L44; // [1672] 1683

    /** 					ungetch()*/
    _62ungetch();
    goto L45; // [1680] 1793
L44: 

    /** 					is_int = FALSE*/
    _is_int_26406 = _9FALSE_426;

    /** 					if yytext[1] = '.' then*/
    _2 = (int)SEQ_PTR(_yytext_26401);
    _15279 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _15279, 46)){
        _15279 = NOVALUE;
        goto L46; // [1698] 1712
    }
    _15279 = NOVALUE;

    /** 						CompileErr(124)*/
    RefDS(_22663);
    _46CompileErr(124, _22663, 0);
    goto L47; // [1709] 1719
L46: 

    /** 						yytext &= '.'*/
    Append(&_yytext_26401, _yytext_26401, 46);
L47: 

    /** 					if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15282 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15282 != -7)
    goto L48; // [1729] 1784

    /** 						yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);

    /** 						ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 						while char_class[ch] = DIGIT do*/
L49: 
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15286 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15286 != -7)
    goto L4A; // [1759] 1792

    /** 							yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);

    /** 							ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 						end while*/
    goto L49; // [1778] 1751
    goto L4A; // [1781] 1792
L48: 

    /** 						CompileErr(94)*/
    RefDS(_22663);
    _46CompileErr(94, _22663, 0);
L4A: 
L45: 
L43: 

    /** 			if basetype = -1 and find(ch, "eE") then*/
    _15290 = (_basetype_26701 == -1);
    if (_15290 == 0) {
        goto L4B; // [1800] 1936
    }
    _15293 = find_from(_ch_26395, _15292, 1);
    if (_15293 == 0)
    {
        _15293 = NOVALUE;
        goto L4B; // [1810] 1936
    }
    else{
        _15293 = NOVALUE;
    }

    /** 				is_int = FALSE*/
    _is_int_26406 = _9FALSE_426;

    /** 				yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 				if ch = '-' or ch = '+' or char_class[ch] = DIGIT then*/
    _15296 = (_ch_26395 == 45);
    if (_15296 != 0) {
        _15297 = 1;
        goto L4C; // [1841] 1853
    }
    _15298 = (_ch_26395 == 43);
    _15297 = (_15298 != 0);
L4C: 
    if (_15297 != 0) {
        goto L4D; // [1853] 1874
    }
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15300 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    _15301 = (_15300 == -7);
    _15300 = NOVALUE;
    if (_15301 == 0)
    {
        DeRef(_15301);
        _15301 = NOVALUE;
        goto L4E; // [1870] 1883
    }
    else{
        DeRef(_15301);
        _15301 = NOVALUE;
    }
L4D: 

    /** 					yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);
    goto L4F; // [1880] 1891
L4E: 

    /** 					CompileErr(86)*/
    RefDS(_22663);
    _46CompileErr(86, _22663, 0);
L4F: 

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 				while char_class[ch] = DIGIT do*/
L50: 
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15304 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15304 != -7)
    goto L51; // [1911] 1967

    /** 					yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);

    /** 					ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 				end while*/
    goto L50; // [1930] 1903
    goto L51; // [1933] 1967
L4B: 

    /** 			elsif char_class[ch] = LETTER then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15308 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15308 != -2)
    goto L52; // [1946] 1966

    /** 				CompileErr(127, {{ch}})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ch_26395;
    _15310 = MAKE_SEQ(_1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _15310;
    _15311 = MAKE_SEQ(_1);
    _15310 = NOVALUE;
    _46CompileErr(127, _15311, 0);
    _15311 = NOVALUE;
L52: 
L51: 

    /** 			ungetch()*/
    _62ungetch();

    /** 			while i != 0 with entry do*/
    goto L53; // [1973] 2012
L54: 
    if (_i_26396 == 0)
    goto L55; // [1978] 2024

    /** 			    yytext = yytext[1 .. i-1] & yytext[i+1 .. $]*/
    _15313 = _i_26396 - 1;
    rhs_slice_target = (object_ptr)&_15314;
    RHS_Slice(_yytext_26401, 1, _15313);
    _15315 = _i_26396 + 1;
    if (_15315 > MAXINT){
        _15315 = NewDouble((double)_15315);
    }
    if (IS_SEQUENCE(_yytext_26401)){
            _15316 = SEQ_PTR(_yytext_26401)->length;
    }
    else {
        _15316 = 1;
    }
    rhs_slice_target = (object_ptr)&_15317;
    RHS_Slice(_yytext_26401, _15315, _15316);
    Concat((object_ptr)&_yytext_26401, _15314, _15317);
    DeRefDS(_15314);
    _15314 = NOVALUE;
    DeRef(_15314);
    _15314 = NOVALUE;
    DeRefDS(_15317);
    _15317 = NOVALUE;

    /** 			  entry*/
L53: 

    /** 			    i = find('_', yytext)*/
    _i_26396 = find_from(95, _yytext_26401, 1);

    /** 			end while*/
    goto L54; // [2021] 1976
L55: 

    /** 			if is_int then*/
    if (_is_int_26406 == 0)
    {
        goto L56; // [2026] 2126
    }
    else{
    }

    /** 				if basetype = -1 then*/
    if (_basetype_26701 != -1)
    goto L57; // [2031] 2041

    /** 					basetype = 3 -- decimal*/
    _basetype_26701 = 3;
L57: 

    /** 				fenv:clear(FE_OVERFLOW)*/
    _15321 = _64clear(111);

    /** 				d = MakeInt(yytext, nbase[basetype])*/
    _2 = (int)SEQ_PTR(_62nbase_26202);
    _15322 = (int)*(((s1_ptr)_2)->base + _basetype_26701);
    RefDS(_yytext_26401);
    Ref(_15322);
    _0 = _d_26403;
    _d_26403 = _62MakeInt(_yytext_26401, _15322);
    DeRef(_0);
    _15322 = NOVALUE;

    /** 				if fenv:test(FE_OVERFLOW) then*/
    _15324 = _64test(111);
    if (_15324 == 0) {
        DeRef(_15324);
        _15324 = NOVALUE;
        goto L58; // [2068] 2081
    }
    else {
        if (!IS_ATOM_INT(_15324) && DBL_PTR(_15324)->dbl == 0.0){
            DeRef(_15324);
            _15324 = NOVALUE;
            goto L58; // [2068] 2081
        }
        DeRef(_15324);
        _15324 = NOVALUE;
    }
    DeRef(_15324);
    _15324 = NOVALUE;

    /** 					CompileErr(NUMBER_IS_TOO_BIG)*/
    RefDS(_22663);
    _46CompileErr(357, _22663, 0);
L58: 

    /** 				if integer(d) then*/
    if (IS_ATOM_INT(_d_26403))
    _15325 = 1;
    else if (IS_ATOM_DBL(_d_26403))
    _15325 = IS_ATOM_INT(DoubleToInt(_d_26403));
    else
    _15325 = 0;
    if (_15325 == 0)
    {
        _15325 = NOVALUE;
        goto L59; // [2086] 2108
    }
    else{
        _15325 = NOVALUE;
    }

    /** 					return {ATOM, NewIntSym(d)}*/
    Ref(_d_26403);
    _15326 = _55NewIntSym(_d_26403);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15326;
    _15327 = MAKE_SEQ(_1);
    _15326 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15327;
    goto L5A; // [2105] 2125
L59: 

    /** 					return {ATOM, NewDoubleSym(d)}*/
    Ref(_d_26403);
    _15328 = _55NewDoubleSym(_d_26403);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15328;
    _15329 = MAKE_SEQ(_1);
    _15328 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15329;
L5A: 
L56: 

    /** 			if basetype != -1 then*/
    if (_basetype_26701 == -1)
    goto L5B; // [2128] 2144

    /** 				CompileErr(125, nbasecode[basetype])*/
    _2 = (int)SEQ_PTR(_62nbasecode_26203);
    _15331 = (int)*(((s1_ptr)_2)->base + _basetype_26701);
    Ref(_15331);
    _46CompileErr(125, _15331, 0);
    _15331 = NOVALUE;
L5B: 

    /** 			d = my_sscanf(yytext)*/
    RefDS(_yytext_26401);
    _0 = _d_26403;
    _d_26403 = _62my_sscanf(_yytext_26401);
    DeRef(_0);

    /** 			if sequence(d) then*/
    _15333 = IS_SEQUENCE(_d_26403);
    if (_15333 == 0)
    {
        _15333 = NOVALUE;
        goto L5C; // [2155] 2168
    }
    else{
        _15333 = NOVALUE;
    }

    /** 				CompileErr(121)*/
    RefDS(_22663);
    _46CompileErr(121, _22663, 0);
    goto L5D; // [2165] 2220
L5C: 

    /** 			elsif is_int and d <= MAXINT_DBL then*/
    if (_is_int_26406 == 0) {
        goto L5E; // [2170] 2203
    }
    if (IS_ATOM_INT(_d_26403)) {
        _15335 = (_d_26403 <= 1073741823);
    }
    else {
        _15335 = binary_op(LESSEQ, _d_26403, 1073741823);
    }
    if (_15335 == 0) {
        DeRef(_15335);
        _15335 = NOVALUE;
        goto L5E; // [2181] 2203
    }
    else {
        if (!IS_ATOM_INT(_15335) && DBL_PTR(_15335)->dbl == 0.0){
            DeRef(_15335);
            _15335 = NOVALUE;
            goto L5E; // [2181] 2203
        }
        DeRef(_15335);
        _15335 = NOVALUE;
    }
    DeRef(_15335);
    _15335 = NOVALUE;

    /** 				return {ATOM, NewIntSym(d)}  -- 1 to 1.07 billion*/
    Ref(_d_26403);
    _15336 = _55NewIntSym(_d_26403);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15336;
    _15337 = MAKE_SEQ(_1);
    _15336 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15337;
    goto L5D; // [2200] 2220
L5E: 

    /** 				return {ATOM, NewDoubleSym(d)}*/
    Ref(_d_26403);
    _15338 = _55NewDoubleSym(_d_26403);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15338;
    _15339 = MAKE_SEQ(_1);
    _15338 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15339;
L5D: 
    goto L1; // [2222] 10
L33: 

    /** 		elsif class = MINUS then*/
    if (_class_26407 != 10)
    goto L5F; // [2229] 2315

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			if ch = '-' then*/
    if (_ch_26395 != 45)
    goto L60; // [2242] 2268

    /** 				if start_include then*/
    if (_62start_include_24623 == 0)
    {
        goto L61; // [2250] 2260
    }
    else{
    }

    /** 					IncludePush()*/
    _62IncludePush();
    goto L1; // [2257] 10
L61: 

    /** 					read_line()*/
    _62read_line();
    goto L1; // [2265] 10
L60: 

    /** 			elsif ch = '=' then*/
    if (_ch_26395 != 61)
    goto L62; // [2270] 2289

    /** 				return {MINUS_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 516;
    ((int *)_2)[2] = 0;
    _15344 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15344;
    goto L1; // [2286] 10
L62: 

    /** 				bp -= 1*/
    _46bp_49486 = _46bp_49486 - 1;

    /** 				return {MINUS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 10;
    ((int *)_2)[2] = 0;
    _15346 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15346;
    goto L1; // [2312] 10
L5F: 

    /** 		elsif class = DOUBLE_QUOTE then*/
    if (_class_26407 != -4)
    goto L63; // [2319] 2513

    /** 			integer fch*/

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			if ch = '"' then*/
    if (_ch_26395 != 34)
    goto L64; // [2334] 2370

    /** 				fch = getch()*/
    _fch_26886 = _62getch();
    if (!IS_ATOM_INT(_fch_26886)) {
        _1 = (long)(DBL_PTR(_fch_26886)->dbl);
        if (UNIQUE(DBL_PTR(_fch_26886)) && (DBL_PTR(_fch_26886)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fch_26886);
        _fch_26886 = _1;
    }

    /** 				if fch = '"' then*/
    if (_fch_26886 != 34)
    goto L65; // [2347] 2364

    /** 					return ExtendedString( fch )*/
    _15352 = _62ExtendedString(_fch_26886);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15352;
    goto L66; // [2361] 2369
L65: 

    /** 					ungetch()*/
    _62ungetch();
L66: 
L64: 

    /** 			yytext = ""*/
    RefDS(_5);
    DeRef(_yytext_26401);
    _yytext_26401 = _5;

    /** 			while ch != '\n' and ch != '\r' do -- can't be EOF*/
L67: 
    _15353 = (_ch_26395 != 10);
    if (_15353 == 0) {
        goto L68; // [2386] 2465
    }
    _15355 = (_ch_26395 != 13);
    if (_15355 == 0)
    {
        DeRef(_15355);
        _15355 = NOVALUE;
        goto L68; // [2395] 2465
    }
    else{
        DeRef(_15355);
        _15355 = NOVALUE;
    }

    /** 				if ch = '"' then*/
    if (_ch_26395 != 34)
    goto L69; // [2400] 2411

    /** 					exit*/
    goto L68; // [2406] 2465
    goto L6A; // [2408] 2453
L69: 

    /** 				elsif ch = '\\' then*/
    if (_ch_26395 != 92)
    goto L6B; // [2413] 2430

    /** 					yytext &= EscapeChar('"')*/
    _15358 = _62EscapeChar(34);
    if (IS_SEQUENCE(_yytext_26401) && IS_ATOM(_15358)) {
        Ref(_15358);
        Append(&_yytext_26401, _yytext_26401, _15358);
    }
    else if (IS_ATOM(_yytext_26401) && IS_SEQUENCE(_15358)) {
    }
    else {
        Concat((object_ptr)&_yytext_26401, _yytext_26401, _15358);
    }
    DeRef(_15358);
    _15358 = NOVALUE;
    goto L6A; // [2427] 2453
L6B: 

    /** 				elsif ch = '\t' then*/
    if (_ch_26395 != 9)
    goto L6C; // [2432] 2446

    /** 					CompileErr(145)*/
    RefDS(_22663);
    _46CompileErr(145, _22663, 0);
    goto L6A; // [2443] 2453
L6C: 

    /** 					yytext &= ch*/
    Append(&_yytext_26401, _yytext_26401, _ch_26395);
L6A: 

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			end while*/
    goto L67; // [2462] 2382
L68: 

    /** 			if ch = '\n' or ch = '\r' then*/
    _15363 = (_ch_26395 == 10);
    if (_15363 != 0) {
        goto L6D; // [2471] 2484
    }
    _15365 = (_ch_26395 == 13);
    if (_15365 == 0)
    {
        DeRef(_15365);
        _15365 = NOVALUE;
        goto L6E; // [2480] 2492
    }
    else{
        DeRef(_15365);
        _15365 = NOVALUE;
    }
L6D: 

    /** 				CompileErr(67)*/
    RefDS(_22663);
    _46CompileErr(67, _22663, 0);
L6E: 

    /** 			return {STRING, NewStringSym(yytext)}*/
    RefDS(_yytext_26401);
    _15366 = _55NewStringSym(_yytext_26401);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 503;
    ((int *)_2)[2] = _15366;
    _15367 = MAKE_SEQ(_1);
    _15366 = NOVALUE;
    DeRefDS(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15367;
    goto L1; // [2510] 10
L63: 

    /** 		elsif class = PLUS then*/
    if (_class_26407 != 11)
    goto L6F; // [2517] 2569

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26395 != 61)
    goto L70; // [2530] 2549

    /** 				return {PLUS_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 515;
    ((int *)_2)[2] = 0;
    _15371 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    return _15371;
    goto L1; // [2546] 10
L70: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				return {PLUS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 11;
    ((int *)_2)[2] = 0;
    _15372 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15372;
    goto L1; // [2566] 10
L6F: 

    /** 		elsif class = res:CONCAT then*/
    if (_class_26407 != 15)
    goto L71; // [2571] 2621

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26395 != 61)
    goto L72; // [2584] 2603

    /** 				return {CONCAT_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 519;
    ((int *)_2)[2] = 0;
    _15376 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15376;
    goto L1; // [2600] 10
L72: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				return {res:CONCAT, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 15;
    ((int *)_2)[2] = 0;
    _15377 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15377;
    goto L1; // [2618] 10
L71: 

    /** 		elsif class = NUMBER_SIGN then*/
    if (_class_26407 != -11)
    goto L73; // [2625] 3180

    /** 			fenv:clear(FE_OVERFLOW)		*/
    _15379 = _64clear(111);

    /** 			i = 0*/
    _i_26396 = 0;

    /** 			is_int = -1*/
    _is_int_26406 = -1;

    /** 			while i < MAXINT/32 do*/
L74: 
    _15380 = (1073741823 % 32) ? NewDouble((double)1073741823 / 32) : (1073741823 / 32);
    if (binary_op_a(GREATEREQ, _i_26396, _15380)){
        DeRef(_15380);
        _15380 = NOVALUE;
        goto L75; // [2658] 2828
    }
    DeRef(_15380);
    _15380 = NOVALUE;

    /** 				ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 				if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15383 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15383 != -7)
    goto L76; // [2679] 2718

    /** 					if ch != '_' then*/
    if (_ch_26395 == 95)
    goto L74; // [2685] 2652

    /** 						i = i * 16 + ch - '0'*/
    if (_i_26396 == (short)_i_26396)
    _15386 = _i_26396 * 16;
    else
    _15386 = NewDouble(_i_26396 * (double)16);
    if (IS_ATOM_INT(_15386)) {
        _15387 = _15386 + _ch_26395;
        if ((long)((unsigned long)_15387 + (unsigned long)HIGH_BITS) >= 0) 
        _15387 = NewDouble((double)_15387);
    }
    else {
        _15387 = NewDouble(DBL_PTR(_15386)->dbl + (double)_ch_26395);
    }
    DeRef(_15386);
    _15386 = NOVALUE;
    if (IS_ATOM_INT(_15387)) {
        _i_26396 = _15387 - 48;
    }
    else {
        _i_26396 = NewDouble(DBL_PTR(_15387)->dbl - (double)48);
    }
    DeRef(_15387);
    _15387 = NOVALUE;
    if (!IS_ATOM_INT(_i_26396)) {
        _1 = (long)(DBL_PTR(_i_26396)->dbl);
        if (UNIQUE(DBL_PTR(_i_26396)) && (DBL_PTR(_i_26396)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_26396);
        _i_26396 = _1;
    }

    /** 						is_int = TRUE*/
    _is_int_26406 = _9TRUE_428;
    goto L74; // [2715] 2652
L76: 

    /** 				elsif ch >= 'A' and ch <= 'F' then*/
    _15389 = (_ch_26395 >= 65);
    if (_15389 == 0) {
        goto L77; // [2724] 2768
    }
    _15391 = (_ch_26395 <= 70);
    if (_15391 == 0)
    {
        DeRef(_15391);
        _15391 = NOVALUE;
        goto L77; // [2733] 2768
    }
    else{
        DeRef(_15391);
        _15391 = NOVALUE;
    }

    /** 					i = (i * 16) + ch - ('A'-10)*/
    if (_i_26396 == (short)_i_26396)
    _15392 = _i_26396 * 16;
    else
    _15392 = NewDouble(_i_26396 * (double)16);
    if (IS_ATOM_INT(_15392)) {
        _15393 = _15392 + _ch_26395;
        if ((long)((unsigned long)_15393 + (unsigned long)HIGH_BITS) >= 0) 
        _15393 = NewDouble((double)_15393);
    }
    else {
        _15393 = NewDouble(DBL_PTR(_15392)->dbl + (double)_ch_26395);
    }
    DeRef(_15392);
    _15392 = NOVALUE;
    _15394 = 55;
    if (IS_ATOM_INT(_15393)) {
        _i_26396 = _15393 - 55;
    }
    else {
        _i_26396 = NewDouble(DBL_PTR(_15393)->dbl - (double)55);
    }
    DeRef(_15393);
    _15393 = NOVALUE;
    _15394 = NOVALUE;
    if (!IS_ATOM_INT(_i_26396)) {
        _1 = (long)(DBL_PTR(_i_26396)->dbl);
        if (UNIQUE(DBL_PTR(_i_26396)) && (DBL_PTR(_i_26396)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_26396);
        _i_26396 = _1;
    }

    /** 					is_int = TRUE*/
    _is_int_26406 = _9TRUE_428;
    goto L74; // [2765] 2652
L77: 

    /** 				elsif ch >= 'a' and ch <= 'f' then*/
    _15396 = (_ch_26395 >= 97);
    if (_15396 == 0) {
        goto L75; // [2774] 2828
    }
    _15398 = (_ch_26395 <= 102);
    if (_15398 == 0)
    {
        DeRef(_15398);
        _15398 = NOVALUE;
        goto L75; // [2783] 2828
    }
    else{
        DeRef(_15398);
        _15398 = NOVALUE;
    }

    /** 					i = (i * 16) + ch - ('a'-10)*/
    if (_i_26396 == (short)_i_26396)
    _15399 = _i_26396 * 16;
    else
    _15399 = NewDouble(_i_26396 * (double)16);
    if (IS_ATOM_INT(_15399)) {
        _15400 = _15399 + _ch_26395;
        if ((long)((unsigned long)_15400 + (unsigned long)HIGH_BITS) >= 0) 
        _15400 = NewDouble((double)_15400);
    }
    else {
        _15400 = NewDouble(DBL_PTR(_15399)->dbl + (double)_ch_26395);
    }
    DeRef(_15399);
    _15399 = NOVALUE;
    _15401 = 87;
    if (IS_ATOM_INT(_15400)) {
        _i_26396 = _15400 - 87;
    }
    else {
        _i_26396 = NewDouble(DBL_PTR(_15400)->dbl - (double)87);
    }
    DeRef(_15400);
    _15400 = NOVALUE;
    _15401 = NOVALUE;
    if (!IS_ATOM_INT(_i_26396)) {
        _1 = (long)(DBL_PTR(_i_26396)->dbl);
        if (UNIQUE(DBL_PTR(_i_26396)) && (DBL_PTR(_i_26396)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_26396);
        _i_26396 = _1;
    }

    /** 					is_int = TRUE*/
    _is_int_26406 = _9TRUE_428;
    goto L74; // [2815] 2652

    /** 					exit*/
    goto L75; // [2820] 2828

    /** 			end while*/
    goto L74; // [2825] 2652
L75: 

    /** 			if is_int = -1 then*/
    if (_is_int_26406 != -1)
    goto L78; // [2830] 2893

    /** 				if ch = '!' then*/
    if (_ch_26395 != 33)
    goto L79; // [2836] 2882

    /** 					if line_number > 1 then*/
    if (_38line_number_16947 <= 1)
    goto L7A; // [2844] 2856

    /** 						CompileErr(161)*/
    RefDS(_22663);
    _46CompileErr(161, _22663, 0);
L7A: 

    /** 					shebang = ThisLine*/
    Ref(_46ThisLine_49482);
    DeRef(_62shebang_24628);
    _62shebang_24628 = _46ThisLine_49482;

    /** 					if start_include then*/
    if (_62start_include_24623 == 0)
    {
        goto L7B; // [2867] 2875
    }
    else{
    }

    /** 						IncludePush()*/
    _62IncludePush();
L7B: 

    /** 					read_line()*/
    _62read_line();
    goto L1; // [2879] 10
L79: 

    /** 					CompileErr(97)*/
    RefDS(_22663);
    _46CompileErr(97, _22663, 0);
    goto L1; // [2890] 10
L78: 

    /** 				if i >= MAXINT/32 then*/
    _15406 = (1073741823 % 32) ? NewDouble((double)1073741823 / 32) : (1073741823 / 32);
    if (binary_op_a(LESS, _i_26396, _15406)){
        DeRef(_15406);
        _15406 = NOVALUE;
        goto L7C; // [2901] 3072
    }
    DeRef(_15406);
    _15406 = NOVALUE;

    /** 					d = i*/
    DeRef(_d_26403);
    _d_26403 = _i_26396;

    /** 					is_int = FALSE*/
    _is_int_26406 = _9FALSE_426;

    /** 					while TRUE do*/
L7D: 
    if (_9TRUE_428 == 0)
    {
        goto L7E; // [2926] 3071
    }
    else{
    }

    /** 						ch = getch()  -- eventually END_OF_FILE_CHAR or new-line*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 						if char_class[ch] = DIGIT then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15409 = (int)*(((s1_ptr)_2)->base + _ch_26395);
    if (_15409 != -7)
    goto L7F; // [2946] 2974

    /** 							if ch != '_' then*/
    if (_ch_26395 == 95)
    goto L7D; // [2952] 2924

    /** 								d = (d * 16) + ch - '0'*/
    if (IS_ATOM_INT(_d_26403)) {
        if (_d_26403 == (short)_d_26403)
        _15412 = _d_26403 * 16;
        else
        _15412 = NewDouble(_d_26403 * (double)16);
    }
    else {
        _15412 = binary_op(MULTIPLY, _d_26403, 16);
    }
    if (IS_ATOM_INT(_15412)) {
        _15413 = _15412 + _ch_26395;
        if ((long)((unsigned long)_15413 + (unsigned long)HIGH_BITS) >= 0) 
        _15413 = NewDouble((double)_15413);
    }
    else {
        _15413 = binary_op(PLUS, _15412, _ch_26395);
    }
    DeRef(_15412);
    _15412 = NOVALUE;
    DeRef(_d_26403);
    if (IS_ATOM_INT(_15413)) {
        _d_26403 = _15413 - 48;
        if ((long)((unsigned long)_d_26403 +(unsigned long) HIGH_BITS) >= 0){
            _d_26403 = NewDouble((double)_d_26403);
        }
    }
    else {
        _d_26403 = binary_op(MINUS, _15413, 48);
    }
    DeRef(_15413);
    _15413 = NOVALUE;
    goto L7D; // [2971] 2924
L7F: 

    /** 						elsif ch >= 'A' and ch <= 'F' then*/
    _15415 = (_ch_26395 >= 65);
    if (_15415 == 0) {
        goto L80; // [2980] 3013
    }
    _15417 = (_ch_26395 <= 70);
    if (_15417 == 0)
    {
        DeRef(_15417);
        _15417 = NOVALUE;
        goto L80; // [2989] 3013
    }
    else{
        DeRef(_15417);
        _15417 = NOVALUE;
    }

    /** 							d = (d * 16) + ch - ('A'- 10)*/
    if (IS_ATOM_INT(_d_26403)) {
        if (_d_26403 == (short)_d_26403)
        _15418 = _d_26403 * 16;
        else
        _15418 = NewDouble(_d_26403 * (double)16);
    }
    else {
        _15418 = binary_op(MULTIPLY, _d_26403, 16);
    }
    if (IS_ATOM_INT(_15418)) {
        _15419 = _15418 + _ch_26395;
        if ((long)((unsigned long)_15419 + (unsigned long)HIGH_BITS) >= 0) 
        _15419 = NewDouble((double)_15419);
    }
    else {
        _15419 = binary_op(PLUS, _15418, _ch_26395);
    }
    DeRef(_15418);
    _15418 = NOVALUE;
    _15420 = 55;
    DeRef(_d_26403);
    if (IS_ATOM_INT(_15419)) {
        _d_26403 = _15419 - 55;
        if ((long)((unsigned long)_d_26403 +(unsigned long) HIGH_BITS) >= 0){
            _d_26403 = NewDouble((double)_d_26403);
        }
    }
    else {
        _d_26403 = binary_op(MINUS, _15419, 55);
    }
    DeRef(_15419);
    _15419 = NOVALUE;
    _15420 = NOVALUE;
    goto L7D; // [3010] 2924
L80: 

    /** 						elsif ch >= 'a' and ch <= 'f' then*/
    _15422 = (_ch_26395 >= 97);
    if (_15422 == 0) {
        goto L81; // [3019] 3052
    }
    _15424 = (_ch_26395 <= 102);
    if (_15424 == 0)
    {
        DeRef(_15424);
        _15424 = NOVALUE;
        goto L81; // [3028] 3052
    }
    else{
        DeRef(_15424);
        _15424 = NOVALUE;
    }

    /** 							d = (d * 16) + ch - ('a'-10)*/
    if (IS_ATOM_INT(_d_26403)) {
        if (_d_26403 == (short)_d_26403)
        _15425 = _d_26403 * 16;
        else
        _15425 = NewDouble(_d_26403 * (double)16);
    }
    else {
        _15425 = binary_op(MULTIPLY, _d_26403, 16);
    }
    if (IS_ATOM_INT(_15425)) {
        _15426 = _15425 + _ch_26395;
        if ((long)((unsigned long)_15426 + (unsigned long)HIGH_BITS) >= 0) 
        _15426 = NewDouble((double)_15426);
    }
    else {
        _15426 = binary_op(PLUS, _15425, _ch_26395);
    }
    DeRef(_15425);
    _15425 = NOVALUE;
    _15427 = 87;
    DeRef(_d_26403);
    if (IS_ATOM_INT(_15426)) {
        _d_26403 = _15426 - 87;
        if ((long)((unsigned long)_d_26403 +(unsigned long) HIGH_BITS) >= 0){
            _d_26403 = NewDouble((double)_d_26403);
        }
    }
    else {
        _d_26403 = binary_op(MINUS, _15426, 87);
    }
    DeRef(_15426);
    _15426 = NOVALUE;
    _15427 = NOVALUE;
    goto L7D; // [3049] 2924
L81: 

    /** 						elsif ch = '_' then*/
    if (_ch_26395 != 95)
    goto L7E; // [3054] 3071
    goto L7D; // [3058] 2924

    /** 							exit*/
    goto L7E; // [3063] 3071

    /** 					end while*/
    goto L7D; // [3068] 2924
L7E: 
L7C: 

    /** 				if fenv:test(FE_OVERFLOW) then*/
    _15430 = _64test(111);
    if (_15430 == 0) {
        DeRef(_15430);
        _15430 = NOVALUE;
        goto L82; // [3080] 3093
    }
    else {
        if (!IS_ATOM_INT(_15430) && DBL_PTR(_15430)->dbl == 0.0){
            DeRef(_15430);
            _15430 = NOVALUE;
            goto L82; // [3080] 3093
        }
        DeRef(_15430);
        _15430 = NOVALUE;
    }
    DeRef(_15430);
    _15430 = NOVALUE;

    /** 					CompileErr(NUMBER_IS_TOO_BIG)*/
    RefDS(_22663);
    _46CompileErr(357, _22663, 0);
L82: 

    /** 				fenv:clear(FE_OVERFLOW)*/
    _15431 = _64clear(111);

    /** 				ungetch()*/
    _62ungetch();

    /** 				if is_int then*/
    if (_is_int_26406 == 0)
    {
        goto L83; // [3107] 3129
    }
    else{
    }

    /** 					return {ATOM, NewIntSym(i)}*/
    _15432 = _55NewIntSym(_i_26396);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15432;
    _15433 = MAKE_SEQ(_1);
    _15432 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15433;
    goto L1; // [3126] 10
L83: 

    /** 					if d <= MAXINT_DBL then            -- d is always >= 0*/
    if (binary_op_a(GREATER, _d_26403, 1073741823)){
        goto L84; // [3135] 3158
    }

    /** 						return {ATOM, NewIntSym(d)}*/
    Ref(_d_26403);
    _15435 = _55NewIntSym(_d_26403);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15435;
    _15436 = MAKE_SEQ(_1);
    _15435 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15436;
    goto L1; // [3155] 10
L84: 

    /** 						return {ATOM, NewDoubleSym(d)}*/
    Ref(_d_26403);
    _15437 = _55NewDoubleSym(_d_26403);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15437;
    _15438 = MAKE_SEQ(_1);
    _15437 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15438;
    goto L1; // [3177] 10
L73: 

    /** 		elsif class = res:MULTIPLY then*/
    if (_class_26407 != 13)
    goto L85; // [3182] 3232

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26395 != 61)
    goto L86; // [3195] 3214

    /** 				return {MULTIPLY_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 517;
    ((int *)_2)[2] = 0;
    _15442 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15442;
    goto L1; // [3211] 10
L86: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				return {res:MULTIPLY, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 13;
    ((int *)_2)[2] = 0;
    _15443 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15443;
    goto L1; // [3229] 10
L85: 

    /** 		elsif class = res:DIVIDE then*/
    if (_class_26407 != 14)
    goto L87; // [3234] 3436

    /** 			ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 			if ch = '=' then*/
    if (_ch_26395 != 61)
    goto L88; // [3247] 3266

    /** 				return {DIVIDE_EQUALS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 518;
    ((int *)_2)[2] = 0;
    _15447 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15447;
    goto L1; // [3263] 10
L88: 

    /** 			elsif ch = '*' then*/
    if (_ch_26395 != 42)
    goto L89; // [3268] 3418

    /** 				cline = line_number*/
    _cline_26400 = _38line_number_16947;

    /** 				integer cnest = 1*/
    _cnest_27072 = 1;

    /** 				while cnest > 0 do*/
L8A: 
    if (_cnest_27072 <= 0)
    goto L8B; // [3291] 3399

    /** 					ch = getch()*/
    _ch_26395 = _62getch();
    if (!IS_ATOM_INT(_ch_26395)) {
        _1 = (long)(DBL_PTR(_ch_26395)->dbl);
        if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_26395);
        _ch_26395 = _1;
    }

    /** 					switch ch do*/
    _0 = _ch_26395;
    switch ( _0 ){ 

        /** 						case  END_OF_FILE_CHAR then*/
        case 26:

        /** 							exit*/
        goto L8B; // [3315] 3399
        goto L8A; // [3317] 3291

        /** 						case '\n' then*/
        case 10:

        /** 							read_line()*/
        _62read_line();
        goto L8A; // [3327] 3291

        /** 						case '*' then*/
        case 42:

        /** 							ch = getch()*/
        _ch_26395 = _62getch();
        if (!IS_ATOM_INT(_ch_26395)) {
            _1 = (long)(DBL_PTR(_ch_26395)->dbl);
            if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ch_26395);
            _ch_26395 = _1;
        }

        /** 							if ch = '/' then*/
        if (_ch_26395 != 47)
        goto L8C; // [3342] 3355

        /** 								cnest -= 1*/
        _cnest_27072 = _cnest_27072 - 1;
        goto L8A; // [3352] 3291
L8C: 

        /** 								ungetch()*/
        _62ungetch();
        goto L8A; // [3360] 3291

        /** 						case '/' then*/
        case 47:

        /** 							ch = getch()*/
        _ch_26395 = _62getch();
        if (!IS_ATOM_INT(_ch_26395)) {
            _1 = (long)(DBL_PTR(_ch_26395)->dbl);
            if (UNIQUE(DBL_PTR(_ch_26395)) && (DBL_PTR(_ch_26395)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_ch_26395);
            _ch_26395 = _1;
        }

        /** 							if ch = '*' then*/
        if (_ch_26395 != 42)
        goto L8D; // [3375] 3388

        /** 								cnest += 1*/
        _cnest_27072 = _cnest_27072 + 1;
        goto L8E; // [3385] 3393
L8D: 

        /** 								ungetch()*/
        _62ungetch();
L8E: 
    ;}
    /** 				end while*/
    goto L8A; // [3396] 3291
L8B: 

    /** 				if cnest > 0 then*/
    if (_cnest_27072 <= 0)
    goto L8F; // [3401] 3413

    /** 					CompileErr(42, cline)*/
    _46CompileErr(42, _cline_26400, 0);
L8F: 
    goto L1; // [3415] 10
L89: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				return {res:DIVIDE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 14;
    ((int *)_2)[2] = 0;
    _15460 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15460;
    goto L1; // [3433] 10
L87: 

    /** 		elsif class = SINGLE_QUOTE then*/
    if (_class_26407 != -5)
    goto L90; // [3440] 3581

    /** 			atom ach = getch()*/
    _0 = _ach_27101;
    _ach_27101 = _62getch();
    DeRef(_0);

    /** 			if ach = '\\' then*/
    if (binary_op_a(NOTEQ, _ach_27101, 92)){
        goto L91; // [3451] 3464
    }

    /** 				ach = EscapeChar('\'')*/
    _0 = _ach_27101;
    _ach_27101 = _62EscapeChar(39);
    DeRef(_0);
    goto L92; // [3461] 3515
L91: 

    /** 			elsif ach = '\t' then*/
    if (binary_op_a(NOTEQ, _ach_27101, 9)){
        goto L93; // [3466] 3480
    }

    /** 				CompileErr(145)*/
    RefDS(_22663);
    _46CompileErr(145, _22663, 0);
    goto L92; // [3477] 3515
L93: 

    /** 			elsif ach = '\'' then*/
    if (binary_op_a(NOTEQ, _ach_27101, 39)){
        goto L94; // [3482] 3496
    }

    /** 				CompileErr(137)*/
    RefDS(_22663);
    _46CompileErr(137, _22663, 0);
    goto L92; // [3493] 3515
L94: 

    /** 			elsif ach = '\n' then*/
    if (binary_op_a(NOTEQ, _ach_27101, 10)){
        goto L95; // [3498] 3514
    }

    /** 				CompileErr(68, {"character", "end of line"})*/
    RefDS(_15469);
    RefDS(_15468);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _15468;
    ((int *)_2)[2] = _15469;
    _15470 = MAKE_SEQ(_1);
    _46CompileErr(68, _15470, 0);
    _15470 = NOVALUE;
L95: 
L92: 

    /** 			if getch() != '\'' then*/
    _15471 = _62getch();
    if (binary_op_a(EQUALS, _15471, 39)){
        DeRef(_15471);
        _15471 = NOVALUE;
        goto L96; // [3520] 3532
    }
    DeRef(_15471);
    _15471 = NOVALUE;

    /** 				CompileErr(56)*/
    RefDS(_22663);
    _46CompileErr(56, _22663, 0);
L96: 

    /** 			if integer(ach) then*/
    if (IS_ATOM_INT(_ach_27101))
    _15473 = 1;
    else if (IS_ATOM_DBL(_ach_27101))
    _15473 = IS_ATOM_INT(DoubleToInt(_ach_27101));
    else
    _15473 = 0;
    if (_15473 == 0)
    {
        _15473 = NOVALUE;
        goto L97; // [3537] 3559
    }
    else{
        _15473 = NOVALUE;
    }

    /** 				return {ATOM, NewIntSym(ach)}*/
    Ref(_ach_27101);
    _15474 = _55NewIntSym(_ach_27101);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15474;
    _15475 = MAKE_SEQ(_1);
    _15474 = NOVALUE;
    DeRef(_ach_27101);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15475;
    goto L98; // [3556] 3576
L97: 

    /** 				return {ATOM, NewDoubleSym(ach)}*/
    Ref(_ach_27101);
    _15476 = _55NewDoubleSym(_ach_27101);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 502;
    ((int *)_2)[2] = _15476;
    _15477 = MAKE_SEQ(_1);
    _15476 = NOVALUE;
    DeRef(_ach_27101);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15477;
L98: 
    DeRef(_ach_27101);
    _ach_27101 = NOVALUE;
    goto L1; // [3578] 10
L90: 

    /** 		elsif class = LESS then*/
    if (_class_26407 != 1)
    goto L99; // [3585] 3633

    /** 			if getch() = '=' then*/
    _15479 = _62getch();
    if (binary_op_a(NOTEQ, _15479, 61)){
        DeRef(_15479);
        _15479 = NOVALUE;
        goto L9A; // [3594] 3613
    }
    DeRef(_15479);
    _15479 = NOVALUE;

    /** 				return {LESSEQ, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 5;
    ((int *)_2)[2] = 0;
    _15481 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15481;
    goto L1; // [3610] 10
L9A: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				return {LESS, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _15482 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15482;
    goto L1; // [3630] 10
L99: 

    /** 		elsif class = GREATER then*/
    if (_class_26407 != 6)
    goto L9B; // [3637] 3685

    /** 			if getch() = '=' then*/
    _15484 = _62getch();
    if (binary_op_a(NOTEQ, _15484, 61)){
        DeRef(_15484);
        _15484 = NOVALUE;
        goto L9C; // [3646] 3665
    }
    DeRef(_15484);
    _15484 = NOVALUE;

    /** 				return {GREATEREQ, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 2;
    ((int *)_2)[2] = 0;
    _15486 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15482);
    _15482 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15486;
    goto L1; // [3662] 10
L9C: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				return {GREATER, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 6;
    ((int *)_2)[2] = 0;
    _15487 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15486);
    _15486 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15482);
    _15482 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15487;
    goto L1; // [3682] 10
L9B: 

    /** 		elsif class = BANG then*/
    if (_class_26407 != -1)
    goto L9D; // [3689] 3737

    /** 			if getch() = '=' then*/
    _15489 = _62getch();
    if (binary_op_a(NOTEQ, _15489, 61)){
        DeRef(_15489);
        _15489 = NOVALUE;
        goto L9E; // [3698] 3717
    }
    DeRef(_15489);
    _15489 = NOVALUE;

    /** 				return {NOTEQ, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = 0;
    _15491 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15486);
    _15486 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15487);
    _15487 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15482);
    _15482 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15491;
    goto L1; // [3714] 10
L9E: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				return {BANG, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _15492 = MAKE_SEQ(_1);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15486);
    _15486 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    DeRef(_15491);
    _15491 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15487);
    _15487 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15482);
    _15482 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15492;
    goto L1; // [3734] 10
L9D: 

    /** 		elsif class = KEYWORD then*/
    if (_class_26407 != -10)
    goto L9F; // [3741] 3774

    /** 			return {keylist[ch - KEYWORD_BASE][K_TOKEN], 0}*/
    _15494 = _ch_26395 - 128;
    _2 = (int)SEQ_PTR(_65keylist_23552);
    _15495 = (int)*(((s1_ptr)_2)->base + _15494);
    _2 = (int)SEQ_PTR(_15495);
    _15496 = (int)*(((s1_ptr)_2)->base + 3);
    _15495 = NOVALUE;
    Ref(_15496);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _15496;
    ((int *)_2)[2] = 0;
    _15497 = MAKE_SEQ(_1);
    _15496 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15486);
    _15486 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    DeRef(_15491);
    _15491 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15492);
    _15492 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    _15494 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15487);
    _15487 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15482);
    _15482 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15497;
    goto L1; // [3771] 10
L9F: 

    /** 		elsif class = BUILTIN then*/
    if (_class_26407 != -9)
    goto LA0; // [3778] 3831

    /** 			name = keylist[ch - BUILTIN_BASE + NUM_KEYWORDS][K_NAME]*/
    _15499 = _ch_26395 - 170;
    if ((long)((unsigned long)_15499 +(unsigned long) HIGH_BITS) >= 0){
        _15499 = NewDouble((double)_15499);
    }
    if (IS_ATOM_INT(_15499)) {
        _15500 = _15499 + 24;
    }
    else {
        _15500 = NewDouble(DBL_PTR(_15499)->dbl + (double)24);
    }
    DeRef(_15499);
    _15499 = NOVALUE;
    _2 = (int)SEQ_PTR(_65keylist_23552);
    if (!IS_ATOM_INT(_15500)){
        _15501 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_15500)->dbl));
    }
    else{
        _15501 = (int)*(((s1_ptr)_2)->base + _15500);
    }
    DeRef(_name_26408);
    _2 = (int)SEQ_PTR(_15501);
    _name_26408 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_name_26408);
    _15501 = NOVALUE;

    /** 			return keyfind(name, -1)*/
    RefDS(_name_26408);
    _32378 = _55hashfn(_name_26408);
    RefDS(_name_26408);
    _15503 = _55keyfind(_name_26408, -1, _38current_file_no_16946, 0, _32378);
    _32378 = NOVALUE;
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRefDS(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15486);
    _15486 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    DeRef(_15491);
    _15491 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15492);
    _15492 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15487);
    _15487 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15482);
    _15482 = NOVALUE;
    DeRef(_15497);
    _15497 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15503;
    goto L1; // [3828] 10
LA0: 

    /** 		elsif class = BACK_QUOTE then*/
    if (_class_26407 != -12)
    goto LA1; // [3835] 3852

    /** 			return ExtendedString( '`' )*/
    _15505 = _62ExtendedString(96);
    DeRef(_yytext_26401);
    DeRef(_namespaces_26402);
    DeRef(_d_26403);
    DeRef(_tok_26405);
    DeRef(_name_26408);
    DeRef(_15263);
    _15263 = NOVALUE;
    _15304 = NOVALUE;
    DeRef(_15296);
    _15296 = NOVALUE;
    DeRef(_15337);
    _15337 = NOVALUE;
    DeRef(_15352);
    _15352 = NOVALUE;
    DeRef(_15321);
    _15321 = NOVALUE;
    DeRef(_15460);
    _15460 = NOVALUE;
    DeRef(_15503);
    _15503 = NOVALUE;
    DeRef(_15115);
    _15115 = NOVALUE;
    _15282 = NOVALUE;
    DeRef(_15346);
    _15346 = NOVALUE;
    DeRef(_15379);
    _15379 = NOVALUE;
    DeRef(_15443);
    _15443 = NOVALUE;
    DeRef(_15145);
    _15145 = NOVALUE;
    DeRef(_15315);
    _15315 = NOVALUE;
    DeRef(_15376);
    _15376 = NOVALUE;
    DeRef(_15242);
    _15242 = NOVALUE;
    DeRef(_15367);
    _15367 = NOVALUE;
    _15182 = NOVALUE;
    DeRef(_15436);
    _15436 = NOVALUE;
    DeRef(_15161);
    _15161 = NOVALUE;
    _15409 = NOVALUE;
    DeRef(_15135);
    _15135 = NOVALUE;
    DeRef(_15141);
    _15141 = NOVALUE;
    DeRef(_15166);
    _15166 = NOVALUE;
    DeRef(_15363);
    _15363 = NOVALUE;
    DeRef(_15431);
    _15431 = NOVALUE;
    DeRef(_15486);
    _15486 = NOVALUE;
    DeRef(_15120);
    _15120 = NOVALUE;
    DeRef(_15344);
    _15344 = NOVALUE;
    DeRef(_15438);
    _15438 = NOVALUE;
    DeRef(_15237);
    _15237 = NOVALUE;
    DeRef(_15290);
    _15290 = NOVALUE;
    DeRef(_15329);
    _15329 = NOVALUE;
    DeRef(_15377);
    _15377 = NOVALUE;
    DeRef(_15129);
    _15129 = NOVALUE;
    DeRef(_15339);
    _15339 = NOVALUE;
    DeRef(_15491);
    _15491 = NOVALUE;
    _15157 = NOVALUE;
    DeRef(_15492);
    _15492 = NOVALUE;
    DeRef(_15132);
    _15132 = NOVALUE;
    _15261 = NOVALUE;
    DeRef(_15494);
    _15494 = NOVALUE;
    DeRef(_15298);
    _15298 = NOVALUE;
    DeRef(_15415);
    _15415 = NOVALUE;
    _15171 = NOVALUE;
    _15193 = NOVALUE;
    DeRef(_15248);
    _15248 = NOVALUE;
    _15251 = NOVALUE;
    _15383 = NOVALUE;
    DeRef(_15433);
    _15433 = NOVALUE;
    DeRef(_15241);
    _15241 = NOVALUE;
    DeRef(_15142);
    _15142 = NOVALUE;
    _15286 = NOVALUE;
    _15308 = NOVALUE;
    DeRef(_15327);
    _15327 = NOVALUE;
    DeRef(_15487);
    _15487 = NOVALUE;
    DeRef(_15353);
    _15353 = NOVALUE;
    DeRef(_15422);
    _15422 = NOVALUE;
    DeRef(_15481);
    _15481 = NOVALUE;
    DeRef(_15192);
    _15192 = NOVALUE;
    DeRef(_15442);
    _15442 = NOVALUE;
    DeRef(_15482);
    _15482 = NOVALUE;
    DeRef(_15497);
    _15497 = NOVALUE;
    DeRef(_15475);
    _15475 = NOVALUE;
    DeRef(_15500);
    _15500 = NOVALUE;
    DeRef(_15372);
    _15372 = NOVALUE;
    DeRef(_15447);
    _15447 = NOVALUE;
    _15208 = NOVALUE;
    DeRef(_15389);
    _15389 = NOVALUE;
    DeRef(_15477);
    _15477 = NOVALUE;
    DeRef(_15313);
    _15313 = NOVALUE;
    DeRef(_15396);
    _15396 = NOVALUE;
    DeRef(_15138);
    _15138 = NOVALUE;
    DeRef(_15371);
    _15371 = NOVALUE;
    return _15505;
    goto L1; // [3849] 10
LA1: 

    /** 			InternalErr(268, {class})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _class_26407;
    _15506 = MAKE_SEQ(_1);
    _46InternalErr(268, _15506);
    _15506 = NOVALUE;

    /**    end while*/
    goto L1; // [3865] 10
L2: 
    ;
}


void _62eu_namespace()
{
    int _eu_tok_27198 = NOVALUE;
    int _eu_ns_27200 = NOVALUE;
    int _32377 = NOVALUE;
    int _32376 = NOVALUE;
    int _15515 = NOVALUE;
    int _15513 = NOVALUE;
    int _15511 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	eu_tok = keyfind("eu", -1, , 1)*/
    RefDS(_15509);
    _32376 = _15509;
    _32377 = _55hashfn(_32376);
    _32376 = NOVALUE;
    RefDS(_15509);
    _0 = _eu_tok_27198;
    _eu_tok_27198 = _55keyfind(_15509, -1, _38current_file_no_16946, 1, _32377);
    DeRef(_0);
    _32377 = NOVALUE;

    /** 	eu_ns  = NameSpace_declaration(eu_tok[T_SYM])*/
    _2 = (int)SEQ_PTR(_eu_tok_27198);
    _15511 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_15511);
    _eu_ns_27200 = _62NameSpace_declaration(_15511);
    _15511 = NOVALUE;
    if (!IS_ATOM_INT(_eu_ns_27200)) {
        _1 = (long)(DBL_PTR(_eu_ns_27200)->dbl);
        if (UNIQUE(DBL_PTR(_eu_ns_27200)) && (DBL_PTR(_eu_ns_27200)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_eu_ns_27200);
        _eu_ns_27200 = _1;
    }

    /** 	SymTab[eu_ns][S_OBJ] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_eu_ns_27200 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _15513 = NOVALUE;

    /** 	SymTab[eu_ns][S_SCOPE] = SC_GLOBAL*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_eu_ns_27200 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = 6;
    DeRef(_1);
    _15515 = NOVALUE;

    /** end procedure*/
    DeRef(_eu_tok_27198);
    return;
    ;
}


int _62StringToken(int _pDelims_27218)
{
    int _ch_27219 = NOVALUE;
    int _m_27220 = NOVALUE;
    int _gtext_27221 = NOVALUE;
    int _level_27252 = NOVALUE;
    int _15554 = NOVALUE;
    int _15552 = NOVALUE;
    int _15550 = NOVALUE;
    int _15531 = NOVALUE;
    int _15530 = NOVALUE;
    int _15524 = NOVALUE;
    int _15522 = NOVALUE;
    int _15520 = NOVALUE;
    int _15518 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 	while ch = ' ' or ch = '\t' do*/
L1: 
    _15518 = (_ch_27219 == 32);
    if (_15518 != 0) {
        goto L2; // [19] 32
    }
    _15520 = (_ch_27219 == 9);
    if (_15520 == 0)
    {
        DeRef(_15520);
        _15520 = NOVALUE;
        goto L3; // [28] 44
    }
    else{
        DeRef(_15520);
        _15520 = NOVALUE;
    }
L2: 

    /** 		ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 	end while*/
    goto L1; // [41] 15
L3: 

    /** 	pDelims &= {' ', '\t', '\n', '\r', END_OF_FILE_CHAR}*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 32;
    *((int *)(_2+8)) = 9;
    *((int *)(_2+12)) = 10;
    *((int *)(_2+16)) = 13;
    *((int *)(_2+20)) = 26;
    _15522 = MAKE_SEQ(_1);
    Concat((object_ptr)&_pDelims_27218, _pDelims_27218, _15522);
    DeRefDS(_15522);
    _15522 = NOVALUE;

    /** 	gtext = ""*/
    RefDS(_5);
    DeRefi(_gtext_27221);
    _gtext_27221 = _5;

    /** 	while not find(ch,  pDelims) label "top" do*/
L4: 
    _15524 = find_from(_ch_27219, _pDelims_27218, 1);
    if (_15524 != 0)
    goto L5; // [77] 391
    _15524 = NOVALUE;

    /** 		if ch = '-' then*/
    if (_ch_27219 != 45)
    goto L6; // [82] 145

    /** 			ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 			if ch = '-' then*/
    if (_ch_27219 != 45)
    goto L7; // [95] 137

    /** 				while not find(ch, {'\n', END_OF_FILE_CHAR}) do*/
L8: 
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 10;
    ((int *)_2)[2] = 26;
    _15530 = MAKE_SEQ(_1);
    _15531 = find_from(_ch_27219, _15530, 1);
    DeRefDS(_15530);
    _15530 = NOVALUE;
    if (_15531 != 0)
    goto L5; // [115] 391
    _15531 = NOVALUE;

    /** 					ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 				end while*/
    goto L8; // [127] 104

    /** 				exit*/
    goto L5; // [132] 391
    goto L9; // [134] 373
L7: 

    /** 				ungetch()*/
    _62ungetch();
    goto L9; // [142] 373
L6: 

    /** 		elsif ch = '/' then*/
    if (_ch_27219 != 47)
    goto LA; // [147] 372

    /** 			ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 			if ch = '*' then*/
    if (_ch_27219 != 42)
    goto LB; // [160] 361

    /** 				integer level = 1*/
    _level_27252 = 1;

    /** 				while level > 0 do*/
LC: 
    if (_level_27252 <= 0)
    goto LD; // [174] 293

    /** 					ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 					if ch = '/' then*/
    if (_ch_27219 != 47)
    goto LE; // [187] 221

    /** 						ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 						if ch = '*' then*/
    if (_ch_27219 != 42)
    goto LF; // [200] 213

    /** 							level += 1*/
    _level_27252 = _level_27252 + 1;
    goto LC; // [210] 174
LF: 

    /** 							ungetch()*/
    _62ungetch();
    goto LC; // [218] 174
LE: 

    /** 					elsif ch = '*' then*/
    if (_ch_27219 != 42)
    goto L10; // [223] 257

    /** 						ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 						if ch = '/' then*/
    if (_ch_27219 != 47)
    goto L11; // [236] 249

    /** 							level -= 1*/
    _level_27252 = _level_27252 - 1;
    goto LC; // [246] 174
L11: 

    /** 							ungetch()*/
    _62ungetch();
    goto LC; // [254] 174
L10: 

    /** 					elsif ch = '\n' then*/
    if (_ch_27219 != 10)
    goto L12; // [259] 270

    /** 						read_line()*/
    _62read_line();
    goto LC; // [267] 174
L12: 

    /** 					elsif ch = END_OF_FILE_CHAR then*/
    if (_ch_27219 != 26)
    goto LC; // [274] 174

    /** 						ungetch()*/
    _62ungetch();

    /** 						exit*/
    goto LD; // [284] 293

    /** 				end while*/
    goto LC; // [290] 174
LD: 

    /** 				ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 				if length(gtext) = 0 then*/
    if (IS_SEQUENCE(_gtext_27221)){
            _15550 = SEQ_PTR(_gtext_27221)->length;
    }
    else {
        _15550 = 1;
    }
    if (_15550 != 0)
    goto L13; // [305] 350

    /** 					while ch = ' ' or ch = '\t' do*/
L14: 
    _15552 = (_ch_27219 == 32);
    if (_15552 != 0) {
        goto L15; // [318] 331
    }
    _15554 = (_ch_27219 == 9);
    if (_15554 == 0)
    {
        DeRef(_15554);
        _15554 = NOVALUE;
        goto L16; // [327] 343
    }
    else{
        DeRef(_15554);
        _15554 = NOVALUE;
    }
L15: 

    /** 						ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 					end while*/
    goto L14; // [340] 314
L16: 

    /** 					continue "top"*/
    goto L4; // [347] 72
L13: 

    /** 				exit*/
    goto L5; // [354] 391
    goto L17; // [358] 371
LB: 

    /** 				ungetch()*/
    _62ungetch();

    /** 				ch = '/'*/
    _ch_27219 = 47;
L17: 
LA: 
L9: 

    /** 		gtext &= ch*/
    Append(&_gtext_27221, _gtext_27221, _ch_27219);

    /** 		ch = getch()*/
    _ch_27219 = _62getch();
    if (!IS_ATOM_INT(_ch_27219)) {
        _1 = (long)(DBL_PTR(_ch_27219)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27219)) && (DBL_PTR(_ch_27219)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27219);
        _ch_27219 = _1;
    }

    /** 	end while*/
    goto L4; // [388] 72
L5: 

    /** 	ungetch() -- put back end-word token.*/
    _62ungetch();

    /** 	return gtext*/
    DeRefDS(_pDelims_27218);
    DeRef(_15518);
    _15518 = NOVALUE;
    DeRef(_15552);
    _15552 = NOVALUE;
    return _gtext_27221;
    ;
}


void _62IncludeScan(int _is_public_27289)
{
    int _ch_27290 = NOVALUE;
    int _gtext_27291 = NOVALUE;
    int _s_27293 = NOVALUE;
    int _32375 = NOVALUE;
    int _15618 = NOVALUE;
    int _15617 = NOVALUE;
    int _15615 = NOVALUE;
    int _15613 = NOVALUE;
    int _15612 = NOVALUE;
    int _15607 = NOVALUE;
    int _15604 = NOVALUE;
    int _15602 = NOVALUE;
    int _15601 = NOVALUE;
    int _15599 = NOVALUE;
    int _15597 = NOVALUE;
    int _15595 = NOVALUE;
    int _15593 = NOVALUE;
    int _15587 = NOVALUE;
    int _15585 = NOVALUE;
    int _15579 = NOVALUE;
    int _15575 = NOVALUE;
    int _15574 = NOVALUE;
    int _15569 = NOVALUE;
    int _15566 = NOVALUE;
    int _15565 = NOVALUE;
    int _15561 = NOVALUE;
    int _15559 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_is_public_27289)) {
        _1 = (long)(DBL_PTR(_is_public_27289)->dbl);
        if (UNIQUE(DBL_PTR(_is_public_27289)) && (DBL_PTR(_is_public_27289)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_public_27289);
        _is_public_27289 = _1;
    }

    /** 	ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 	while ch = ' ' or ch = '\t' do*/
L1: 
    _15559 = (_ch_27290 == 32);
    if (_15559 != 0) {
        goto L2; // [19] 32
    }
    _15561 = (_ch_27290 == 9);
    if (_15561 == 0)
    {
        DeRef(_15561);
        _15561 = NOVALUE;
        goto L3; // [28] 44
    }
    else{
        DeRef(_15561);
        _15561 = NOVALUE;
    }
L2: 

    /** 		ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 	end while*/
    goto L1; // [41] 15
L3: 

    /** 	gtext = ""*/
    RefDS(_5);
    DeRef(_gtext_27291);
    _gtext_27291 = _5;

    /** 	if ch = '"' then*/
    if (_ch_27290 != 34)
    goto L4; // [53] 141

    /** 		ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 		while not find(ch, {'\n', '\r', '"', END_OF_FILE_CHAR}) do*/
L5: 
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 10;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 34;
    *((int *)(_2+16)) = 26;
    _15565 = MAKE_SEQ(_1);
    _15566 = find_from(_ch_27290, _15565, 1);
    DeRefDS(_15565);
    _15565 = NOVALUE;
    if (_15566 != 0)
    goto L6; // [83] 124
    _15566 = NOVALUE;

    /** 			if ch = '\\' then*/
    if (_ch_27290 != 92)
    goto L7; // [88] 105

    /** 				gtext &= EscapeChar('"')*/
    _15569 = _62EscapeChar(34);
    if (IS_SEQUENCE(_gtext_27291) && IS_ATOM(_15569)) {
        Ref(_15569);
        Append(&_gtext_27291, _gtext_27291, _15569);
    }
    else if (IS_ATOM(_gtext_27291) && IS_SEQUENCE(_15569)) {
    }
    else {
        Concat((object_ptr)&_gtext_27291, _gtext_27291, _15569);
    }
    DeRef(_15569);
    _15569 = NOVALUE;
    goto L8; // [102] 112
L7: 

    /** 				gtext &= ch*/
    Append(&_gtext_27291, _gtext_27291, _ch_27290);
L8: 

    /** 			ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 		end while*/
    goto L5; // [121] 69
L6: 

    /** 		if ch != '"' then*/
    if (_ch_27290 == 34)
    goto L9; // [126] 187

    /** 			CompileErr(115)*/
    RefDS(_22663);
    _46CompileErr(115, _22663, 0);
    goto L9; // [138] 187
L4: 

    /** 		while not find(ch, {' ', '\t', '\n', '\r', END_OF_FILE_CHAR}) do*/
LA: 
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 32;
    *((int *)(_2+8)) = 9;
    *((int *)(_2+12)) = 10;
    *((int *)(_2+16)) = 13;
    *((int *)(_2+20)) = 26;
    _15574 = MAKE_SEQ(_1);
    _15575 = find_from(_ch_27290, _15574, 1);
    DeRefDS(_15574);
    _15574 = NOVALUE;
    if (_15575 != 0)
    goto LB; // [161] 182
    _15575 = NOVALUE;

    /** 			gtext &= ch*/
    Append(&_gtext_27291, _gtext_27291, _ch_27290);

    /** 			ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 		end while*/
    goto LA; // [179] 146
LB: 

    /** 		ungetch()*/
    _62ungetch();
L9: 

    /** 	if length(gtext) = 0 then*/
    if (IS_SEQUENCE(_gtext_27291)){
            _15579 = SEQ_PTR(_gtext_27291)->length;
    }
    else {
        _15579 = 1;
    }
    if (_15579 != 0)
    goto LC; // [192] 204

    /** 		CompileErr(95)*/
    RefDS(_22663);
    _46CompileErr(95, _22663, 0);
LC: 

    /** 	ifdef WINDOWS then*/

    /** 		new_include_name = match_replace(`/`, gtext, `\`)*/
    RefDS(_15581);
    RefDS(_gtext_27291);
    RefDS(_15582);
    _0 = _12match_replace(_15581, _gtext_27291, _15582, 0);
    DeRef(_38new_include_name_17088);
    _38new_include_name_17088 = _0;

    /** 	ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 	while ch = ' ' or ch = '\t' do*/
LD: 
    _15585 = (_ch_27290 == 32);
    if (_15585 != 0) {
        goto LE; // [233] 246
    }
    _15587 = (_ch_27290 == 9);
    if (_15587 == 0)
    {
        DeRef(_15587);
        _15587 = NOVALUE;
        goto LF; // [242] 258
    }
    else{
        DeRef(_15587);
        _15587 = NOVALUE;
    }
LE: 

    /** 		ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 	end while*/
    goto LD; // [255] 229
LF: 

    /** 	new_include_space = 0*/
    _62new_include_space_24621 = 0;

    /** 	if ch = 'a' then*/
    if (_ch_27290 != 97)
    goto L10; // [267] 524

    /** 		ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 		if ch = 's' then*/
    if (_ch_27290 != 115)
    goto L11; // [280] 513

    /** 			ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 			if ch = ' ' or ch = '\t' then*/
    _15593 = (_ch_27290 == 32);
    if (_15593 != 0) {
        goto L12; // [297] 310
    }
    _15595 = (_ch_27290 == 9);
    if (_15595 == 0)
    {
        DeRef(_15595);
        _15595 = NOVALUE;
        goto L13; // [306] 502
    }
    else{
        DeRef(_15595);
        _15595 = NOVALUE;
    }
L12: 

    /** 				ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 				while ch = ' ' or ch = '\t' do*/
L14: 
    _15597 = (_ch_27290 == 32);
    if (_15597 != 0) {
        goto L15; // [326] 339
    }
    _15599 = (_ch_27290 == 9);
    if (_15599 == 0)
    {
        DeRef(_15599);
        _15599 = NOVALUE;
        goto L16; // [335] 351
    }
    else{
        DeRef(_15599);
        _15599 = NOVALUE;
    }
L15: 

    /** 					ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 				end while*/
    goto L14; // [348] 322
L16: 

    /** 				if char_class[ch] = LETTER or ch = '_' then*/
    _2 = (int)SEQ_PTR(_62char_class_24630);
    _15601 = (int)*(((s1_ptr)_2)->base + _ch_27290);
    _15602 = (_15601 == -2);
    _15601 = NOVALUE;
    if (_15602 != 0) {
        goto L17; // [365] 378
    }
    _15604 = (_ch_27290 == 95);
    if (_15604 == 0)
    {
        DeRef(_15604);
        _15604 = NOVALUE;
        goto L18; // [374] 491
    }
    else{
        DeRef(_15604);
        _15604 = NOVALUE;
    }
L17: 

    /** 					gtext = {ch}*/
    _0 = _gtext_27291;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _ch_27290;
    _gtext_27291 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 					ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 					while id_char[ch] = TRUE do*/
L19: 
    _2 = (int)SEQ_PTR(_62id_char_24631);
    _15607 = (int)*(((s1_ptr)_2)->base + _ch_27290);
    if (_15607 != _9TRUE_428)
    goto L1A; // [404] 426

    /** 						gtext &= ch*/
    Append(&_gtext_27291, _gtext_27291, _ch_27290);

    /** 						ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 					end while*/
    goto L19; // [423] 396
L1A: 

    /** 					ungetch()*/
    _62ungetch();

    /** 					s = keyfind(gtext, -1, , 1)*/
    RefDS(_gtext_27291);
    _32375 = _55hashfn(_gtext_27291);
    RefDS(_gtext_27291);
    _0 = _s_27293;
    _s_27293 = _55keyfind(_gtext_27291, -1, _38current_file_no_16946, 1, _32375);
    DeRef(_0);
    _32375 = NOVALUE;

    /** 					if not find(s[T_ID], ID_TOKS) then*/
    _2 = (int)SEQ_PTR(_s_27293);
    _15612 = (int)*(((s1_ptr)_2)->base + 1);
    _15613 = find_from(_15612, _39ID_TOKS_16553, 1);
    _15612 = NOVALUE;
    if (_15613 != 0)
    goto L1B; // [463] 474
    _15613 = NOVALUE;

    /** 						CompileErr(36)*/
    RefDS(_22663);
    _46CompileErr(36, _22663, 0);
L1B: 

    /** 					new_include_space = NameSpace_declaration(s[T_SYM])*/
    _2 = (int)SEQ_PTR(_s_27293);
    _15615 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_15615);
    _0 = _62NameSpace_declaration(_15615);
    _62new_include_space_24621 = _0;
    _15615 = NOVALUE;
    if (!IS_ATOM_INT(_62new_include_space_24621)) {
        _1 = (long)(DBL_PTR(_62new_include_space_24621)->dbl);
        if (UNIQUE(DBL_PTR(_62new_include_space_24621)) && (DBL_PTR(_62new_include_space_24621)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_62new_include_space_24621);
        _62new_include_space_24621 = _1;
    }
    goto L1C; // [488] 633
L18: 

    /** 					CompileErr(113)*/
    RefDS(_22663);
    _46CompileErr(113, _22663, 0);
    goto L1C; // [499] 633
L13: 

    /** 				CompileErr(100)*/
    RefDS(_22663);
    _46CompileErr(100, _22663, 0);
    goto L1C; // [510] 633
L11: 

    /** 			CompileErr(100)*/
    RefDS(_22663);
    _46CompileErr(100, _22663, 0);
    goto L1C; // [521] 633
L10: 

    /** 	elsif find(ch, {'\n', '\r', END_OF_FILE_CHAR}) then*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 10;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 26;
    _15617 = MAKE_SEQ(_1);
    _15618 = find_from(_ch_27290, _15617, 1);
    DeRefDS(_15617);
    _15617 = NOVALUE;
    if (_15618 == 0)
    {
        _15618 = NOVALUE;
        goto L1D; // [539] 549
    }
    else{
        _15618 = NOVALUE;
    }

    /** 		ungetch()*/
    _62ungetch();
    goto L1C; // [546] 633
L1D: 

    /** 	elsif ch = '-' then*/
    if (_ch_27290 != 45)
    goto L1E; // [551] 587

    /** 		ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 		if ch != '-' then*/
    if (_ch_27290 == 45)
    goto L1F; // [564] 576

    /** 			CompileErr(100)*/
    RefDS(_22663);
    _46CompileErr(100, _22663, 0);
L1F: 

    /** 		ungetch()*/
    _62ungetch();

    /** 		ungetch()*/
    _62ungetch();
    goto L1C; // [584] 633
L1E: 

    /** 	elsif ch = '/' then*/
    if (_ch_27290 != 47)
    goto L20; // [589] 625

    /** 		ch = getch()*/
    _ch_27290 = _62getch();
    if (!IS_ATOM_INT(_ch_27290)) {
        _1 = (long)(DBL_PTR(_ch_27290)->dbl);
        if (UNIQUE(DBL_PTR(_ch_27290)) && (DBL_PTR(_ch_27290)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ch_27290);
        _ch_27290 = _1;
    }

    /** 		if ch != '*' then*/
    if (_ch_27290 == 42)
    goto L21; // [602] 614

    /** 			CompileErr(100)*/
    RefDS(_22663);
    _46CompileErr(100, _22663, 0);
L21: 

    /** 		ungetch()*/
    _62ungetch();

    /** 		ungetch()*/
    _62ungetch();
    goto L1C; // [622] 633
L20: 

    /** 		CompileErr(100)*/
    RefDS(_22663);
    _46CompileErr(100, _22663, 0);
L1C: 

    /** 	start_include = TRUE -- let scanner know*/
    _62start_include_24623 = _9TRUE_428;

    /** 	public_include = is_public*/
    _62public_include_24626 = _is_public_27289;

    /** end procedure*/
    DeRef(_gtext_27291);
    DeRef(_s_27293);
    DeRef(_15559);
    _15559 = NOVALUE;
    DeRef(_15585);
    _15585 = NOVALUE;
    DeRef(_15593);
    _15593 = NOVALUE;
    DeRef(_15597);
    _15597 = NOVALUE;
    _15607 = NOVALUE;
    DeRef(_15602);
    _15602 = NOVALUE;
    return;
    ;
}


void _62main_file()
{
    int _0, _1, _2;
    

    /** 	ifdef STDDEBUG then*/

    /** 		read_line()*/
    _62read_line();

    /** 		default_namespace( )*/
    _62default_namespace();

    /** end procedure*/
    return;
    ;
}


void _62cleanup_open_includes()
{
    int _15628 = NOVALUE;
    int _15627 = NOVALUE;
    int _15626 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( IncludeStk ) do*/
    if (IS_SEQUENCE(_62IncludeStk_24632)){
            _15626 = SEQ_PTR(_62IncludeStk_24632)->length;
    }
    else {
        _15626 = 1;
    }
    {
        int _i_27414;
        _i_27414 = 1;
L1: 
        if (_i_27414 > _15626){
            goto L2; // [8] 36
        }

        /** 		close( IncludeStk[i][FILE_PTR] )*/
        _2 = (int)SEQ_PTR(_62IncludeStk_24632);
        _15627 = (int)*(((s1_ptr)_2)->base + _i_27414);
        _2 = (int)SEQ_PTR(_15627);
        _15628 = (int)*(((s1_ptr)_2)->base + 3);
        _15627 = NOVALUE;
        if (IS_ATOM_INT(_15628))
        EClose(_15628);
        else
        EClose((int)DBL_PTR(_15628)->dbl);
        _15628 = NOVALUE;

        /** 	end for*/
        _i_27414 = _i_27414 + 1;
        goto L1; // [31] 15
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}



// 0x3C49FE7A
