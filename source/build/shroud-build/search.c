// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _12find_all(int _needle_1392, int _haystack_1393, int _start_1394)
{
    int _kx_1395 = NOVALUE;
    int _553 = NOVALUE;
    int _552 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer kx = 0*/
    _kx_1395 = 0;

    /** 	while start with entry do*/
    goto L1; // [12] 39
L2: 
    if (_start_1394 == 0)
    {
        goto L3; // [15] 51
    }
    else{
    }

    /** 		kx += 1*/
    _kx_1395 = _kx_1395 + 1;

    /** 		haystack[kx] = start*/
    _2 = (int)SEQ_PTR(_haystack_1393);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_1393 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _kx_1395);
    _1 = *(int *)_2;
    *(int *)_2 = _start_1394;
    DeRef(_1);

    /** 		start += 1*/
    _start_1394 = _start_1394 + 1;

    /** 	entry*/
L1: 

    /** 		start = find(needle, haystack, start)*/
    _start_1394 = find_from(_needle_1392, _haystack_1393, _start_1394);

    /** 	end while*/
    goto L2; // [48] 15
L3: 

    /** 	haystack = remove( haystack, kx+1, length( haystack ) )*/
    _552 = _kx_1395 + 1;
    if (_552 > MAXINT){
        _552 = NewDouble((double)_552);
    }
    if (IS_SEQUENCE(_haystack_1393)){
            _553 = SEQ_PTR(_haystack_1393)->length;
    }
    else {
        _553 = 1;
    }
    {
        s1_ptr assign_space = SEQ_PTR(_haystack_1393);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_552)) ? _552 : (long)(DBL_PTR(_552)->dbl);
        int stop = (IS_ATOM_INT(_553)) ? _553 : (long)(DBL_PTR(_553)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_haystack_1393), start, &_haystack_1393 );
            }
            else Tail(SEQ_PTR(_haystack_1393), stop+1, &_haystack_1393);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_haystack_1393), start, &_haystack_1393);
        }
        else {
            assign_slice_seq = &assign_space;
            _haystack_1393 = Remove_elements(start, stop, (SEQ_PTR(_haystack_1393)->ref == 1));
        }
    }
    DeRef(_552);
    _552 = NOVALUE;
    _553 = NOVALUE;

    /** 	return haystack*/
    return _haystack_1393;
    ;
}


int _12rfind(int _needle_1510, int _haystack_1511, int _start_1512)
{
    int _len_1514 = NOVALUE;
    int _614 = NOVALUE;
    int _613 = NOVALUE;
    int _610 = NOVALUE;
    int _609 = NOVALUE;
    int _607 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer len = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1511)){
            _len_1514 = SEQ_PTR(_haystack_1511)->length;
    }
    else {
        _len_1514 = 1;
    }

    /** 	if start = 0 then start = len end if*/
    if (_start_1512 != 0)
    goto L1; // [12] 20
    _start_1512 = _len_1514;
L1: 

    /** 	if (start > len) or (len + start < 1) then*/
    _607 = (_start_1512 > _len_1514);
    if (_607 != 0) {
        goto L2; // [26] 43
    }
    _609 = _len_1514 + _start_1512;
    if ((long)((unsigned long)_609 + (unsigned long)HIGH_BITS) >= 0) 
    _609 = NewDouble((double)_609);
    if (IS_ATOM_INT(_609)) {
        _610 = (_609 < 1);
    }
    else {
        _610 = (DBL_PTR(_609)->dbl < (double)1);
    }
    DeRef(_609);
    _609 = NOVALUE;
    if (_610 == 0)
    {
        DeRef(_610);
        _610 = NOVALUE;
        goto L3; // [39] 50
    }
    else{
        DeRef(_610);
        _610 = NOVALUE;
    }
L2: 

    /** 		return 0*/
    DeRef(_needle_1510);
    DeRefDS(_haystack_1511);
    DeRef(_607);
    _607 = NOVALUE;
    return 0;
L3: 

    /** 	if start < 1 then*/
    if (_start_1512 >= 1)
    goto L4; // [52] 63

    /** 		start = len + start*/
    _start_1512 = _len_1514 + _start_1512;
L4: 

    /** 	for i = start to 1 by -1 do*/
    {
        int _i_1527;
        _i_1527 = _start_1512;
L5: 
        if (_i_1527 < 1){
            goto L6; // [65] 99
        }

        /** 		if equal(haystack[i], needle) then*/
        _2 = (int)SEQ_PTR(_haystack_1511);
        _613 = (int)*(((s1_ptr)_2)->base + _i_1527);
        if (_613 == _needle_1510)
        _614 = 1;
        else if (IS_ATOM_INT(_613) && IS_ATOM_INT(_needle_1510))
        _614 = 0;
        else
        _614 = (compare(_613, _needle_1510) == 0);
        _613 = NOVALUE;
        if (_614 == 0)
        {
            _614 = NOVALUE;
            goto L7; // [82] 92
        }
        else{
            _614 = NOVALUE;
        }

        /** 			return i*/
        DeRef(_needle_1510);
        DeRefDS(_haystack_1511);
        DeRef(_607);
        _607 = NOVALUE;
        return _i_1527;
L7: 

        /** 	end for*/
        _i_1527 = _i_1527 + -1;
        goto L5; // [94] 72
L6: 
        ;
    }

    /** 	return 0*/
    DeRef(_needle_1510);
    DeRefDS(_haystack_1511);
    DeRef(_607);
    _607 = NOVALUE;
    return 0;
    ;
}


int _12find_replace(int _needle_1533, int _haystack_1534, int _replacement_1535, int _max_1536)
{
    int _posn_1537 = NOVALUE;
    int _618 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer posn = 0*/
    _posn_1537 = 0;

    /** 	while posn != 0 entry do */
    goto L1; // [12] 45
L2: 
    if (_posn_1537 == 0)
    goto L3; // [15] 61

    /** 		haystack[posn] = replacement*/
    Ref(_replacement_1535);
    _2 = (int)SEQ_PTR(_haystack_1534);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _haystack_1534 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _posn_1537);
    _1 = *(int *)_2;
    *(int *)_2 = _replacement_1535;
    DeRef(_1);

    /** 		max -= 1*/
    _max_1536 = _max_1536 - 1;

    /** 		if max = 0 then*/
    if (_max_1536 != 0)
    goto L4; // [33] 42

    /** 			exit*/
    goto L3; // [39] 61
L4: 

    /** 	entry*/
L1: 

    /** 		posn = find(needle, haystack, posn + 1)*/
    _618 = _posn_1537 + 1;
    if (_618 > MAXINT){
        _618 = NewDouble((double)_618);
    }
    _posn_1537 = find_from(_needle_1533, _haystack_1534, _618);
    DeRef(_618);
    _618 = NOVALUE;

    /** 	end while*/
    goto L2; // [58] 15
L3: 

    /** 	return haystack*/
    DeRef(_needle_1533);
    DeRefi(_replacement_1535);
    return _haystack_1534;
    ;
}


int _12match_replace(int _needle_1547, int _haystack_1548, int _replacement_1549, int _max_1550)
{
    int _posn_1551 = NOVALUE;
    int _needle_len_1552 = NOVALUE;
    int _replacement_len_1553 = NOVALUE;
    int _scan_from_1554 = NOVALUE;
    int _cnt_1555 = NOVALUE;
    int _630 = NOVALUE;
    int _627 = NOVALUE;
    int _625 = NOVALUE;
    int _623 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if max < 0 then*/

    /** 	cnt = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1548)){
            _cnt_1555 = SEQ_PTR(_haystack_1548)->length;
    }
    else {
        _cnt_1555 = 1;
    }

    /** 	if max != 0 then*/

    /** 	if atom(needle) then*/
    _623 = IS_ATOM(_needle_1547);
    if (_623 == 0)
    {
        _623 = NOVALUE;
        goto L1; // [40] 50
    }
    else{
        _623 = NOVALUE;
    }

    /** 		needle = {needle}*/
    _0 = _needle_1547;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_needle_1547);
    *((int *)(_2+4)) = _needle_1547;
    _needle_1547 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	if atom(replacement) then*/
    _625 = IS_ATOM(_replacement_1549);
    if (_625 == 0)
    {
        _625 = NOVALUE;
        goto L2; // [55] 65
    }
    else{
        _625 = NOVALUE;
    }

    /** 		replacement = {replacement}*/
    _0 = _replacement_1549;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_replacement_1549);
    *((int *)(_2+4)) = _replacement_1549;
    _replacement_1549 = MAKE_SEQ(_1);
    DeRef(_0);
L2: 

    /** 	needle_len = length(needle) - 1*/
    if (IS_SEQUENCE(_needle_1547)){
            _627 = SEQ_PTR(_needle_1547)->length;
    }
    else {
        _627 = 1;
    }
    _needle_len_1552 = _627 - 1;
    _627 = NOVALUE;

    /** 	replacement_len = length(replacement)*/
    if (IS_SEQUENCE(_replacement_1549)){
            _replacement_len_1553 = SEQ_PTR(_replacement_1549)->length;
    }
    else {
        _replacement_len_1553 = 1;
    }

    /** 	scan_from = 1*/
    _scan_from_1554 = 1;

    /** 	while posn with entry do*/
    goto L3; // [86] 132
L4: 
    if (_posn_1551 == 0)
    {
        goto L5; // [91] 144
    }
    else{
    }

    /** 		haystack = replace(haystack, replacement, posn, posn + needle_len)*/
    _630 = _posn_1551 + _needle_len_1552;
    if ((long)((unsigned long)_630 + (unsigned long)HIGH_BITS) >= 0) 
    _630 = NewDouble((double)_630);
    {
        int p1 = _haystack_1548;
        int p2 = _replacement_1549;
        int p3 = _posn_1551;
        int p4 = _630;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_haystack_1548;
        Replace( &replace_params );
    }
    DeRef(_630);
    _630 = NOVALUE;

    /** 		cnt -= 1*/
    _cnt_1555 = _cnt_1555 - 1;

    /** 		if cnt = 0 then*/
    if (_cnt_1555 != 0)
    goto L6; // [114] 123

    /** 			exit*/
    goto L5; // [120] 144
L6: 

    /** 		scan_from = posn + replacement_len*/
    _scan_from_1554 = _posn_1551 + _replacement_len_1553;

    /** 	entry*/
L3: 

    /** 		posn = match(needle, haystack, scan_from)*/
    _posn_1551 = e_match_from(_needle_1547, _haystack_1548, _scan_from_1554);

    /** 	end while*/
    goto L4; // [141] 89
L5: 

    /** 	return haystack*/
    DeRef(_needle_1547);
    DeRef(_replacement_1549);
    return _haystack_1548;
    ;
}


int _12binary_search(int _needle_1580, int _haystack_1581, int _start_point_1582, int _end_point_1583)
{
    int _lo_1584 = NOVALUE;
    int _hi_1585 = NOVALUE;
    int _mid_1586 = NOVALUE;
    int _c_1587 = NOVALUE;
    int _656 = NOVALUE;
    int _648 = NOVALUE;
    int _646 = NOVALUE;
    int _643 = NOVALUE;
    int _642 = NOVALUE;
    int _641 = NOVALUE;
    int _640 = NOVALUE;
    int _637 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lo = start_point*/
    _lo_1584 = 1;

    /** 	if end_point <= 0 then*/
    if (_end_point_1583 > 0)
    goto L1; // [14] 30

    /** 		hi = length(haystack) + end_point*/
    if (IS_SEQUENCE(_haystack_1581)){
            _637 = SEQ_PTR(_haystack_1581)->length;
    }
    else {
        _637 = 1;
    }
    _hi_1585 = _637 + _end_point_1583;
    _637 = NOVALUE;
    goto L2; // [27] 36
L1: 

    /** 		hi = end_point*/
    _hi_1585 = _end_point_1583;
L2: 

    /** 	if lo<1 then*/
    if (_lo_1584 >= 1)
    goto L3; // [38] 48

    /** 		lo=1*/
    _lo_1584 = 1;
L3: 

    /** 	if lo > hi and length(haystack) > 0 then*/
    _640 = (_lo_1584 > _hi_1585);
    if (_640 == 0) {
        goto L4; // [56] 77
    }
    if (IS_SEQUENCE(_haystack_1581)){
            _642 = SEQ_PTR(_haystack_1581)->length;
    }
    else {
        _642 = 1;
    }
    _643 = (_642 > 0);
    _642 = NOVALUE;
    if (_643 == 0)
    {
        DeRef(_643);
        _643 = NOVALUE;
        goto L4; // [68] 77
    }
    else{
        DeRef(_643);
        _643 = NOVALUE;
    }

    /** 		hi = length(haystack)*/
    if (IS_SEQUENCE(_haystack_1581)){
            _hi_1585 = SEQ_PTR(_haystack_1581)->length;
    }
    else {
        _hi_1585 = 1;
    }
L4: 

    /** 	mid = start_point*/
    _mid_1586 = _start_point_1582;

    /** 	c = 0*/
    _c_1587 = 0;

    /** 	while lo <= hi do*/
L5: 
    if (_lo_1584 > _hi_1585)
    goto L6; // [92] 160

    /** 		mid = floor((lo + hi) / 2)*/
    _646 = _lo_1584 + _hi_1585;
    if ((long)((unsigned long)_646 + (unsigned long)HIGH_BITS) >= 0) 
    _646 = NewDouble((double)_646);
    if (IS_ATOM_INT(_646)) {
        _mid_1586 = _646 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _646, 2);
        _mid_1586 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_646);
    _646 = NOVALUE;
    if (!IS_ATOM_INT(_mid_1586)) {
        _1 = (long)(DBL_PTR(_mid_1586)->dbl);
        if (UNIQUE(DBL_PTR(_mid_1586)) && (DBL_PTR(_mid_1586)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_1586);
        _mid_1586 = _1;
    }

    /** 		c = eu:compare(needle, haystack[mid])*/
    _2 = (int)SEQ_PTR(_haystack_1581);
    _648 = (int)*(((s1_ptr)_2)->base + _mid_1586);
    if (IS_ATOM_INT(_needle_1580) && IS_ATOM_INT(_648)){
        _c_1587 = (_needle_1580 < _648) ? -1 : (_needle_1580 > _648);
    }
    else{
        _c_1587 = compare(_needle_1580, _648);
    }
    _648 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_1587 >= 0)
    goto L7; // [120] 133

    /** 			hi = mid - 1*/
    _hi_1585 = _mid_1586 - 1;
    goto L5; // [130] 92
L7: 

    /** 		elsif c > 0 then*/
    if (_c_1587 <= 0)
    goto L8; // [135] 148

    /** 			lo = mid + 1*/
    _lo_1584 = _mid_1586 + 1;
    goto L5; // [145] 92
L8: 

    /** 			return mid*/
    DeRefDS(_haystack_1581);
    DeRef(_640);
    _640 = NOVALUE;
    return _mid_1586;

    /** 	end while*/
    goto L5; // [157] 92
L6: 

    /** 	if c > 0 then*/
    if (_c_1587 <= 0)
    goto L9; // [162] 173

    /** 		mid += 1*/
    _mid_1586 = _mid_1586 + 1;
L9: 

    /** 	return -mid*/
    if ((unsigned long)_mid_1586 == 0xC0000000)
    _656 = (int)NewDouble((double)-0xC0000000);
    else
    _656 = - _mid_1586;
    DeRefDS(_haystack_1581);
    DeRef(_640);
    _640 = NOVALUE;
    return _656;
    ;
}


int _12begins(int _sub_text_1668, int _full_text_1669)
{
    int _694 = NOVALUE;
    int _693 = NOVALUE;
    int _692 = NOVALUE;
    int _690 = NOVALUE;
    int _689 = NOVALUE;
    int _688 = NOVALUE;
    int _687 = NOVALUE;
    int _686 = NOVALUE;
    int _684 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1669)){
            _684 = SEQ_PTR(_full_text_1669)->length;
    }
    else {
        _684 = 1;
    }
    if (_684 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRef(_sub_text_1668);
    DeRefDS(_full_text_1669);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _686 = IS_ATOM(_sub_text_1668);
    if (_686 == 0)
    {
        _686 = NOVALUE;
        goto L2; // [24] 57
    }
    else{
        _686 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[1]) then*/
    _2 = (int)SEQ_PTR(_full_text_1669);
    _687 = (int)*(((s1_ptr)_2)->base + 1);
    if (_sub_text_1668 == _687)
    _688 = 1;
    else if (IS_ATOM_INT(_sub_text_1668) && IS_ATOM_INT(_687))
    _688 = 0;
    else
    _688 = (compare(_sub_text_1668, _687) == 0);
    _687 = NOVALUE;
    if (_688 == 0)
    {
        _688 = NOVALUE;
        goto L3; // [37] 49
    }
    else{
        _688 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_sub_text_1668);
    DeRefDS(_full_text_1669);
    return 1;
    goto L4; // [46] 56
L3: 

    /** 			return 0*/
    DeRef(_sub_text_1668);
    DeRefDS(_full_text_1669);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1668)){
            _689 = SEQ_PTR(_sub_text_1668)->length;
    }
    else {
        _689 = 1;
    }
    if (IS_SEQUENCE(_full_text_1669)){
            _690 = SEQ_PTR(_full_text_1669)->length;
    }
    else {
        _690 = 1;
    }
    if (_689 <= _690)
    goto L5; // [65] 76

    /** 		return 0*/
    DeRef(_sub_text_1668);
    DeRefDS(_full_text_1669);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[1.. length(sub_text)]) then*/
    if (IS_SEQUENCE(_sub_text_1668)){
            _692 = SEQ_PTR(_sub_text_1668)->length;
    }
    else {
        _692 = 1;
    }
    rhs_slice_target = (object_ptr)&_693;
    RHS_Slice(_full_text_1669, 1, _692);
    if (_sub_text_1668 == _693)
    _694 = 1;
    else if (IS_ATOM_INT(_sub_text_1668) && IS_ATOM_INT(_693))
    _694 = 0;
    else
    _694 = (compare(_sub_text_1668, _693) == 0);
    DeRefDS(_693);
    _693 = NOVALUE;
    if (_694 == 0)
    {
        _694 = NOVALUE;
        goto L6; // [90] 102
    }
    else{
        _694 = NOVALUE;
    }

    /** 		return 1*/
    DeRef(_sub_text_1668);
    DeRefDS(_full_text_1669);
    return 1;
    goto L7; // [99] 109
L6: 

    /** 		return 0*/
    DeRef(_sub_text_1668);
    DeRefDS(_full_text_1669);
    return 0;
L7: 
    ;
}


int _12ends(int _sub_text_1690, int _full_text_1691)
{
    int _710 = NOVALUE;
    int _709 = NOVALUE;
    int _708 = NOVALUE;
    int _707 = NOVALUE;
    int _706 = NOVALUE;
    int _705 = NOVALUE;
    int _704 = NOVALUE;
    int _702 = NOVALUE;
    int _701 = NOVALUE;
    int _700 = NOVALUE;
    int _699 = NOVALUE;
    int _698 = NOVALUE;
    int _697 = NOVALUE;
    int _695 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(full_text) = 0 then*/
    if (IS_SEQUENCE(_full_text_1691)){
            _695 = SEQ_PTR(_full_text_1691)->length;
    }
    else {
        _695 = 1;
    }
    if (_695 != 0)
    goto L1; // [8] 19

    /** 		return 0*/
    DeRefDSi(_sub_text_1690);
    DeRefDS(_full_text_1691);
    return 0;
L1: 

    /** 	if atom(sub_text) then*/
    _697 = IS_ATOM(_sub_text_1690);
    if (_697 == 0)
    {
        _697 = NOVALUE;
        goto L2; // [24] 60
    }
    else{
        _697 = NOVALUE;
    }

    /** 		if equal(sub_text, full_text[$]) then*/
    if (IS_SEQUENCE(_full_text_1691)){
            _698 = SEQ_PTR(_full_text_1691)->length;
    }
    else {
        _698 = 1;
    }
    _2 = (int)SEQ_PTR(_full_text_1691);
    _699 = (int)*(((s1_ptr)_2)->base + _698);
    if (_sub_text_1690 == _699)
    _700 = 1;
    else if (IS_ATOM_INT(_sub_text_1690) && IS_ATOM_INT(_699))
    _700 = 0;
    else
    _700 = (compare(_sub_text_1690, _699) == 0);
    _699 = NOVALUE;
    if (_700 == 0)
    {
        _700 = NOVALUE;
        goto L3; // [40] 52
    }
    else{
        _700 = NOVALUE;
    }

    /** 			return 1*/
    DeRefi(_sub_text_1690);
    DeRefDS(_full_text_1691);
    return 1;
    goto L4; // [49] 59
L3: 

    /** 			return 0*/
    DeRefi(_sub_text_1690);
    DeRefDS(_full_text_1691);
    return 0;
L4: 
L2: 

    /** 	if length(sub_text) > length(full_text) then*/
    if (IS_SEQUENCE(_sub_text_1690)){
            _701 = SEQ_PTR(_sub_text_1690)->length;
    }
    else {
        _701 = 1;
    }
    if (IS_SEQUENCE(_full_text_1691)){
            _702 = SEQ_PTR(_full_text_1691)->length;
    }
    else {
        _702 = 1;
    }
    if (_701 <= _702)
    goto L5; // [68] 79

    /** 		return 0*/
    DeRefi(_sub_text_1690);
    DeRefDS(_full_text_1691);
    return 0;
L5: 

    /** 	if equal(sub_text, full_text[$ - length(sub_text) + 1 .. $]) then*/
    if (IS_SEQUENCE(_full_text_1691)){
            _704 = SEQ_PTR(_full_text_1691)->length;
    }
    else {
        _704 = 1;
    }
    if (IS_SEQUENCE(_sub_text_1690)){
            _705 = SEQ_PTR(_sub_text_1690)->length;
    }
    else {
        _705 = 1;
    }
    _706 = _704 - _705;
    _704 = NOVALUE;
    _705 = NOVALUE;
    _707 = _706 + 1;
    _706 = NOVALUE;
    if (IS_SEQUENCE(_full_text_1691)){
            _708 = SEQ_PTR(_full_text_1691)->length;
    }
    else {
        _708 = 1;
    }
    rhs_slice_target = (object_ptr)&_709;
    RHS_Slice(_full_text_1691, _707, _708);
    if (_sub_text_1690 == _709)
    _710 = 1;
    else if (IS_ATOM_INT(_sub_text_1690) && IS_ATOM_INT(_709))
    _710 = 0;
    else
    _710 = (compare(_sub_text_1690, _709) == 0);
    DeRefDS(_709);
    _709 = NOVALUE;
    if (_710 == 0)
    {
        _710 = NOVALUE;
        goto L6; // [107] 119
    }
    else{
        _710 = NOVALUE;
    }

    /** 		return 1*/
    DeRefi(_sub_text_1690);
    DeRefDS(_full_text_1691);
    _707 = NOVALUE;
    return 1;
    goto L7; // [116] 126
L6: 

    /** 		return 0*/
    DeRefi(_sub_text_1690);
    DeRefDS(_full_text_1691);
    DeRef(_707);
    _707 = NOVALUE;
    return 0;
L7: 
    ;
}



// 0x33150702
