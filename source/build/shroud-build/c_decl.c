// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _59get_eucompiledir()
{
    int _x_42634 = NOVALUE;
    int _22999 = NOVALUE;
    int _22995 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object x = getenv("EUCOMPILEDIR")*/
    DeRef(_x_42634);
    _x_42634 = EGetEnv(_22993);

    /** 	if is_eudir_from_cmdline() then*/
    _22995 = _35is_eudir_from_cmdline();
    if (_22995 == 0) {
        DeRef(_22995);
        _22995 = NOVALUE;
        goto L1; // [11] 20
    }
    else {
        if (!IS_ATOM_INT(_22995) && DBL_PTR(_22995)->dbl == 0.0){
            DeRef(_22995);
            _22995 = NOVALUE;
            goto L1; // [11] 20
        }
        DeRef(_22995);
        _22995 = NOVALUE;
    }
    DeRef(_22995);
    _22995 = NOVALUE;

    /** 		x = get_eudir()*/
    _0 = _x_42634;
    _x_42634 = _35get_eudir();
    DeRefi(_0);
L1: 

    /** 	ifdef UNIX then*/

    /** 	if equal(x, -1) then*/
    if (_x_42634 == -1)
    _22999 = 1;
    else if (IS_ATOM_INT(_x_42634) && IS_ATOM_INT(-1))
    _22999 = 0;
    else
    _22999 = (compare(_x_42634, -1) == 0);
    if (_22999 == 0)
    {
        _22999 = NOVALUE;
        goto L2; // [28] 37
    }
    else{
        _22999 = NOVALUE;
    }

    /** 		x = get_eudir()*/
    _0 = _x_42634;
    _x_42634 = _35get_eudir();
    DeRef(_0);
L2: 

    /** 	return x*/
    return _x_42634;
    ;
}


void _59NewBB(int _a_call_42650, int _mask_42651, int _sub_42653)
{
    int _s_42655 = NOVALUE;
    int _23032 = NOVALUE;
    int _23031 = NOVALUE;
    int _23029 = NOVALUE;
    int _23028 = NOVALUE;
    int _23026 = NOVALUE;
    int _23025 = NOVALUE;
    int _23024 = NOVALUE;
    int _23023 = NOVALUE;
    int _23022 = NOVALUE;
    int _23021 = NOVALUE;
    int _23020 = NOVALUE;
    int _23019 = NOVALUE;
    int _23018 = NOVALUE;
    int _23017 = NOVALUE;
    int _23016 = NOVALUE;
    int _23015 = NOVALUE;
    int _23014 = NOVALUE;
    int _23013 = NOVALUE;
    int _23012 = NOVALUE;
    int _23011 = NOVALUE;
    int _23010 = NOVALUE;
    int _23009 = NOVALUE;
    int _23008 = NOVALUE;
    int _23007 = NOVALUE;
    int _23006 = NOVALUE;
    int _23005 = NOVALUE;
    int _23004 = NOVALUE;
    int _23002 = NOVALUE;
    int _23001 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_a_call_42650)) {
        _1 = (long)(DBL_PTR(_a_call_42650)->dbl);
        if (UNIQUE(DBL_PTR(_a_call_42650)) && (DBL_PTR(_a_call_42650)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_call_42650);
        _a_call_42650 = _1;
    }
    if (!IS_ATOM_INT(_mask_42651)) {
        _1 = (long)(DBL_PTR(_mask_42651)->dbl);
        if (UNIQUE(DBL_PTR(_mask_42651)) && (DBL_PTR(_mask_42651)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mask_42651);
        _mask_42651 = _1;
    }
    if (!IS_ATOM_INT(_sub_42653)) {
        _1 = (long)(DBL_PTR(_sub_42653)->dbl);
        if (UNIQUE(DBL_PTR(_sub_42653)) && (DBL_PTR(_sub_42653)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_42653);
        _sub_42653 = _1;
    }

    /** 	if a_call then*/
    if (_a_call_42650 == 0)
    {
        goto L1; // [9] 258
    }
    else{
    }

    /** 		for i = 1 to length(BB_info) do*/
    if (IS_SEQUENCE(_59BB_info_42604)){
            _23001 = SEQ_PTR(_59BB_info_42604)->length;
    }
    else {
        _23001 = 1;
    }
    {
        int _i_42658;
        _i_42658 = 1;
L2: 
        if (_i_42658 > _23001){
            goto L3; // [19] 255
        }

        /** 			s = BB_info[i][BB_VAR]*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23002 = (int)*(((s1_ptr)_2)->base + _i_42658);
        _2 = (int)SEQ_PTR(_23002);
        _s_42655 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_s_42655)){
            _s_42655 = (long)DBL_PTR(_s_42655)->dbl;
        }
        _23002 = NOVALUE;

        /** 			if SymTab[s][S_MODE] = M_NORMAL and*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23004 = (int)*(((s1_ptr)_2)->base + _s_42655);
        _2 = (int)SEQ_PTR(_23004);
        _23005 = (int)*(((s1_ptr)_2)->base + 3);
        _23004 = NOVALUE;
        if (IS_ATOM_INT(_23005)) {
            _23006 = (_23005 == 1);
        }
        else {
            _23006 = binary_op(EQUALS, _23005, 1);
        }
        _23005 = NOVALUE;
        if (IS_ATOM_INT(_23006)) {
            if (_23006 == 0) {
                goto L4; // [62] 248
            }
        }
        else {
            if (DBL_PTR(_23006)->dbl == 0.0) {
                goto L4; // [62] 248
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23008 = (int)*(((s1_ptr)_2)->base + _s_42655);
        _2 = (int)SEQ_PTR(_23008);
        _23009 = (int)*(((s1_ptr)_2)->base + 4);
        _23008 = NOVALUE;
        if (IS_ATOM_INT(_23009)) {
            _23010 = (_23009 == 6);
        }
        else {
            _23010 = binary_op(EQUALS, _23009, 6);
        }
        _23009 = NOVALUE;
        if (IS_ATOM_INT(_23010)) {
            if (_23010 != 0) {
                DeRef(_23011);
                _23011 = 1;
                goto L5; // [84] 110
            }
        }
        else {
            if (DBL_PTR(_23010)->dbl != 0.0) {
                DeRef(_23011);
                _23011 = 1;
                goto L5; // [84] 110
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23012 = (int)*(((s1_ptr)_2)->base + _s_42655);
        _2 = (int)SEQ_PTR(_23012);
        _23013 = (int)*(((s1_ptr)_2)->base + 4);
        _23012 = NOVALUE;
        if (IS_ATOM_INT(_23013)) {
            _23014 = (_23013 == 5);
        }
        else {
            _23014 = binary_op(EQUALS, _23013, 5);
        }
        _23013 = NOVALUE;
        DeRef(_23011);
        if (IS_ATOM_INT(_23014))
        _23011 = (_23014 != 0);
        else
        _23011 = DBL_PTR(_23014)->dbl != 0.0;
L5: 
        if (_23011 != 0) {
            _23015 = 1;
            goto L6; // [110] 136
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23016 = (int)*(((s1_ptr)_2)->base + _s_42655);
        _2 = (int)SEQ_PTR(_23016);
        _23017 = (int)*(((s1_ptr)_2)->base + 4);
        _23016 = NOVALUE;
        if (IS_ATOM_INT(_23017)) {
            _23018 = (_23017 == 11);
        }
        else {
            _23018 = binary_op(EQUALS, _23017, 11);
        }
        _23017 = NOVALUE;
        if (IS_ATOM_INT(_23018))
        _23015 = (_23018 != 0);
        else
        _23015 = DBL_PTR(_23018)->dbl != 0.0;
L6: 
        if (_23015 != 0) {
            DeRef(_23019);
            _23019 = 1;
            goto L7; // [136] 162
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23020 = (int)*(((s1_ptr)_2)->base + _s_42655);
        _2 = (int)SEQ_PTR(_23020);
        _23021 = (int)*(((s1_ptr)_2)->base + 4);
        _23020 = NOVALUE;
        if (IS_ATOM_INT(_23021)) {
            _23022 = (_23021 == 13);
        }
        else {
            _23022 = binary_op(EQUALS, _23021, 13);
        }
        _23021 = NOVALUE;
        if (IS_ATOM_INT(_23022))
        _23019 = (_23022 != 0);
        else
        _23019 = DBL_PTR(_23022)->dbl != 0.0;
L7: 
        if (_23019 == 0)
        {
            _23019 = NOVALUE;
            goto L4; // [163] 248
        }
        else{
            _23019 = NOVALUE;
        }

        /** 				  if and_bits(mask, power(2, remainder(s, E_SIZE))) then*/
        _23023 = (_s_42655 % 29);
        _23024 = power(2, _23023);
        _23023 = NOVALUE;
        if (IS_ATOM_INT(_23024)) {
            {unsigned long tu;
                 tu = (unsigned long)_mask_42651 & (unsigned long)_23024;
                 _23025 = MAKE_UINT(tu);
            }
        }
        else {
            temp_d.dbl = (double)_mask_42651;
            _23025 = Dand_bits(&temp_d, DBL_PTR(_23024));
        }
        DeRef(_23024);
        _23024 = NOVALUE;
        if (_23025 == 0) {
            DeRef(_23025);
            _23025 = NOVALUE;
            goto L8; // [182] 247
        }
        else {
            if (!IS_ATOM_INT(_23025) && DBL_PTR(_23025)->dbl == 0.0){
                DeRef(_23025);
                _23025 = NOVALUE;
                goto L8; // [182] 247
            }
            DeRef(_23025);
            _23025 = NOVALUE;
        }
        DeRef(_23025);
        _23025 = NOVALUE;

        /** 					  if mask = E_ALL_EFFECT or s < sub then*/
        _23026 = (_mask_42651 == 1073741823);
        if (_23026 != 0) {
            goto L9; // [193] 206
        }
        _23028 = (_s_42655 < _sub_42653);
        if (_23028 == 0)
        {
            DeRef(_23028);
            _23028 = NOVALUE;
            goto LA; // [202] 246
        }
        else{
            DeRef(_23028);
            _23028 = NOVALUE;
        }
L9: 

        /** 						  BB_info[i][BB_TYPE..BB_OBJ] =*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _59BB_info_42604 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_42658 + ((s1_ptr)_2)->base);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -1073741824;
        ((int *)_2)[2] = 1073741823;
        _23031 = MAKE_SEQ(_1);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 0;
        *((int *)(_2+8)) = 0;
        Ref(_38NOVALUE_16800);
        *((int *)(_2+12)) = _38NOVALUE_16800;
        *((int *)(_2+16)) = _23031;
        _23032 = MAKE_SEQ(_1);
        _23031 = NOVALUE;
        assign_slice_seq = (s1_ptr *)_3;
        AssignSlice(2, 5, _23032);
        DeRefDS(_23032);
        _23032 = NOVALUE;
LA: 
L8: 
L4: 

        /** 		end for*/
        _i_42658 = _i_42658 + 1;
        goto L2; // [250] 26
L3: 
        ;
    }
    goto LB; // [255] 266
L1: 

    /** 		BB_info = {}*/
    RefDS(_22663);
    DeRef(_59BB_info_42604);
    _59BB_info_42604 = _22663;
LB: 

    /** end procedure*/
    DeRef(_23026);
    _23026 = NOVALUE;
    DeRef(_23006);
    _23006 = NOVALUE;
    DeRef(_23010);
    _23010 = NOVALUE;
    DeRef(_23014);
    _23014 = NOVALUE;
    DeRef(_23018);
    _23018 = NOVALUE;
    DeRef(_23022);
    _23022 = NOVALUE;
    DeRef(_23029);
    _23029 = NOVALUE;
    return;
    ;
}


int _59BB_var_obj(int _var_42723)
{
    int _bbi_42724 = NOVALUE;
    int _23043 = NOVALUE;
    int _23041 = NOVALUE;
    int _23039 = NOVALUE;
    int _23038 = NOVALUE;
    int _23036 = NOVALUE;
    int _23034 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_var_42723)) {
        _1 = (long)(DBL_PTR(_var_42723)->dbl);
        if (UNIQUE(DBL_PTR(_var_42723)) && (DBL_PTR(_var_42723)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_42723);
        _var_42723 = _1;
    }

    /** 	for i = length(BB_info) to 1 by -1 do*/
    if (IS_SEQUENCE(_59BB_info_42604)){
            _23034 = SEQ_PTR(_59BB_info_42604)->length;
    }
    else {
        _23034 = 1;
    }
    {
        int _i_42726;
        _i_42726 = _23034;
L1: 
        if (_i_42726 < 1){
            goto L2; // [10] 105
        }

        /** 		bbi = BB_info[i]*/
        DeRef(_bbi_42724);
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _bbi_42724 = (int)*(((s1_ptr)_2)->base + _i_42726);
        Ref(_bbi_42724);

        /** 		if bbi[BB_VAR] != var then*/
        _2 = (int)SEQ_PTR(_bbi_42724);
        _23036 = (int)*(((s1_ptr)_2)->base + 1);
        if (binary_op_a(EQUALS, _23036, _var_42723)){
            _23036 = NOVALUE;
            goto L3; // [33] 42
        }
        _23036 = NOVALUE;

        /** 			continue*/
        goto L4; // [39] 100
L3: 

        /** 		if SymTab[var][S_MODE] != M_NORMAL then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23038 = (int)*(((s1_ptr)_2)->base + _var_42723);
        _2 = (int)SEQ_PTR(_23038);
        _23039 = (int)*(((s1_ptr)_2)->base + 3);
        _23038 = NOVALUE;
        if (binary_op_a(EQUALS, _23039, 1)){
            _23039 = NOVALUE;
            goto L5; // [58] 67
        }
        _23039 = NOVALUE;

        /** 			continue*/
        goto L4; // [64] 100
L5: 

        /** 		if bbi[BB_TYPE] != TYPE_INTEGER then*/
        _2 = (int)SEQ_PTR(_bbi_42724);
        _23041 = (int)*(((s1_ptr)_2)->base + 2);
        if (binary_op_a(EQUALS, _23041, 1)){
            _23041 = NOVALUE;
            goto L6; // [77] 86
        }
        _23041 = NOVALUE;

        /** 			exit*/
        goto L2; // [83] 105
L6: 

        /** 		return bbi[BB_OBJ]*/
        _2 = (int)SEQ_PTR(_bbi_42724);
        _23043 = (int)*(((s1_ptr)_2)->base + 5);
        Ref(_23043);
        DeRef(_bbi_42724);
        return _23043;

        /** 	end for*/
L4: 
        _i_42726 = _i_42726 + -1;
        goto L1; // [100] 17
L2: 
        ;
    }

    /** 	return BB_def_values*/
    RefDS(_59BB_def_values_42717);
    DeRef(_bbi_42724);
    _23043 = NOVALUE;
    return _59BB_def_values_42717;
    ;
}


int _59BB_var_type(int _var_42746)
{
    int _23058 = NOVALUE;
    int _23057 = NOVALUE;
    int _23055 = NOVALUE;
    int _23054 = NOVALUE;
    int _23053 = NOVALUE;
    int _23052 = NOVALUE;
    int _23051 = NOVALUE;
    int _23050 = NOVALUE;
    int _23049 = NOVALUE;
    int _23048 = NOVALUE;
    int _23047 = NOVALUE;
    int _23046 = NOVALUE;
    int _23045 = NOVALUE;
    int _23044 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_var_42746)) {
        _1 = (long)(DBL_PTR(_var_42746)->dbl);
        if (UNIQUE(DBL_PTR(_var_42746)) && (DBL_PTR(_var_42746)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_42746);
        _var_42746 = _1;
    }

    /** 	for i = length(BB_info) to 1 by -1 do*/
    if (IS_SEQUENCE(_59BB_info_42604)){
            _23044 = SEQ_PTR(_59BB_info_42604)->length;
    }
    else {
        _23044 = 1;
    }
    {
        int _i_42748;
        _i_42748 = _23044;
L1: 
        if (_i_42748 < 1){
            goto L2; // [10] 133
        }

        /** 		if BB_info[i][BB_VAR] = var and*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23045 = (int)*(((s1_ptr)_2)->base + _i_42748);
        _2 = (int)SEQ_PTR(_23045);
        _23046 = (int)*(((s1_ptr)_2)->base + 1);
        _23045 = NOVALUE;
        if (IS_ATOM_INT(_23046)) {
            _23047 = (_23046 == _var_42746);
        }
        else {
            _23047 = binary_op(EQUALS, _23046, _var_42746);
        }
        _23046 = NOVALUE;
        if (IS_ATOM_INT(_23047)) {
            if (_23047 == 0) {
                goto L3; // [35] 126
            }
        }
        else {
            if (DBL_PTR(_23047)->dbl == 0.0) {
                goto L3; // [35] 126
            }
        }
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23049 = (int)*(((s1_ptr)_2)->base + _i_42748);
        _2 = (int)SEQ_PTR(_23049);
        _23050 = (int)*(((s1_ptr)_2)->base + 1);
        _23049 = NOVALUE;
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_23050)){
            _23051 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23050)->dbl));
        }
        else{
            _23051 = (int)*(((s1_ptr)_2)->base + _23050);
        }
        _2 = (int)SEQ_PTR(_23051);
        _23052 = (int)*(((s1_ptr)_2)->base + 3);
        _23051 = NOVALUE;
        if (IS_ATOM_INT(_23052)) {
            _23053 = (_23052 == 1);
        }
        else {
            _23053 = binary_op(EQUALS, _23052, 1);
        }
        _23052 = NOVALUE;
        if (_23053 == 0) {
            DeRef(_23053);
            _23053 = NOVALUE;
            goto L3; // [70] 126
        }
        else {
            if (!IS_ATOM_INT(_23053) && DBL_PTR(_23053)->dbl == 0.0){
                DeRef(_23053);
                _23053 = NOVALUE;
                goto L3; // [70] 126
            }
            DeRef(_23053);
            _23053 = NOVALUE;
        }
        DeRef(_23053);
        _23053 = NOVALUE;

        /** 			ifdef DEBUG then*/

        /** 			if BB_info[i][BB_TYPE] = TYPE_NULL then  -- var has only been read*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23054 = (int)*(((s1_ptr)_2)->base + _i_42748);
        _2 = (int)SEQ_PTR(_23054);
        _23055 = (int)*(((s1_ptr)_2)->base + 2);
        _23054 = NOVALUE;
        if (binary_op_a(NOTEQ, _23055, 0)){
            _23055 = NOVALUE;
            goto L4; // [91] 106
        }
        _23055 = NOVALUE;

        /** 				return TYPE_OBJECT*/
        _23050 = NOVALUE;
        DeRef(_23047);
        _23047 = NOVALUE;
        return 16;
        goto L5; // [103] 125
L4: 

        /** 				return BB_info[i][BB_TYPE]*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23057 = (int)*(((s1_ptr)_2)->base + _i_42748);
        _2 = (int)SEQ_PTR(_23057);
        _23058 = (int)*(((s1_ptr)_2)->base + 2);
        _23057 = NOVALUE;
        Ref(_23058);
        _23050 = NOVALUE;
        DeRef(_23047);
        _23047 = NOVALUE;
        return _23058;
L5: 
L3: 

        /** 	end for*/
        _i_42748 = _i_42748 + -1;
        goto L1; // [128] 17
L2: 
        ;
    }

    /** 	return TYPE_OBJECT*/
    _23050 = NOVALUE;
    DeRef(_23047);
    _23047 = NOVALUE;
    _23058 = NOVALUE;
    return 16;
    ;
}


int _59GType(int _s_42776)
{
    int _t_42777 = NOVALUE;
    int _local_t_42778 = NOVALUE;
    int _23062 = NOVALUE;
    int _23061 = NOVALUE;
    int _23059 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_42776)) {
        _1 = (long)(DBL_PTR(_s_42776)->dbl);
        if (UNIQUE(DBL_PTR(_s_42776)) && (DBL_PTR(_s_42776)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42776);
        _s_42776 = _1;
    }

    /** 	t = SymTab[s][S_GTYPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23059 = (int)*(((s1_ptr)_2)->base + _s_42776);
    _2 = (int)SEQ_PTR(_23059);
    _t_42777 = (int)*(((s1_ptr)_2)->base + 36);
    if (!IS_ATOM_INT(_t_42777)){
        _t_42777 = (long)DBL_PTR(_t_42777)->dbl;
    }
    _23059 = NOVALUE;

    /** 	ifdef DEBUG then*/

    /** 	if SymTab[s][S_MODE] != M_NORMAL then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23061 = (int)*(((s1_ptr)_2)->base + _s_42776);
    _2 = (int)SEQ_PTR(_23061);
    _23062 = (int)*(((s1_ptr)_2)->base + 3);
    _23061 = NOVALUE;
    if (binary_op_a(EQUALS, _23062, 1)){
        _23062 = NOVALUE;
        goto L1; // [37] 48
    }
    _23062 = NOVALUE;

    /** 		return t*/
    return _t_42777;
L1: 

    /** 	local_t = BB_var_type(s)*/
    _local_t_42778 = _59BB_var_type(_s_42776);
    if (!IS_ATOM_INT(_local_t_42778)) {
        _1 = (long)(DBL_PTR(_local_t_42778)->dbl);
        if (UNIQUE(DBL_PTR(_local_t_42778)) && (DBL_PTR(_local_t_42778)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_local_t_42778);
        _local_t_42778 = _1;
    }

    /** 	if local_t = TYPE_OBJECT then*/
    if (_local_t_42778 != 16)
    goto L2; // [60] 71

    /** 		return t*/
    return _t_42777;
L2: 

    /** 	if t = TYPE_INTEGER then*/
    if (_t_42777 != 1)
    goto L3; // [75] 88

    /** 		return TYPE_INTEGER*/
    return 1;
L3: 

    /** 	return local_t*/
    return _local_t_42778;
    ;
}


int _59GDelete()
{
    int _0, _1, _2;
    

    /** 	return g_has_delete*/
    return _59g_has_delete_42798;
    ;
}


int _59HasDelete(int _s_42805)
{
    int _23077 = NOVALUE;
    int _23076 = NOVALUE;
    int _23074 = NOVALUE;
    int _23073 = NOVALUE;
    int _23072 = NOVALUE;
    int _23071 = NOVALUE;
    int _23069 = NOVALUE;
    int _23068 = NOVALUE;
    int _23067 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_42805)) {
        _1 = (long)(DBL_PTR(_s_42805)->dbl);
        if (UNIQUE(DBL_PTR(_s_42805)) && (DBL_PTR(_s_42805)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42805);
        _s_42805 = _1;
    }

    /** 	for i = length(BB_info) to 1 by -1 do*/
    if (IS_SEQUENCE(_59BB_info_42604)){
            _23067 = SEQ_PTR(_59BB_info_42604)->length;
    }
    else {
        _23067 = 1;
    }
    {
        int _i_42807;
        _i_42807 = _23067;
L1: 
        if (_i_42807 < 1){
            goto L2; // [10] 61
        }

        /** 		if BB_info[i][BB_VAR] = s then*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23068 = (int)*(((s1_ptr)_2)->base + _i_42807);
        _2 = (int)SEQ_PTR(_23068);
        _23069 = (int)*(((s1_ptr)_2)->base + 1);
        _23068 = NOVALUE;
        if (binary_op_a(NOTEQ, _23069, _s_42805)){
            _23069 = NOVALUE;
            goto L3; // [31] 54
        }
        _23069 = NOVALUE;

        /** 			return BB_info[i][BB_DELETE]*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23071 = (int)*(((s1_ptr)_2)->base + _i_42807);
        _2 = (int)SEQ_PTR(_23071);
        _23072 = (int)*(((s1_ptr)_2)->base + 6);
        _23071 = NOVALUE;
        Ref(_23072);
        return _23072;
L3: 

        /** 	end for*/
        _i_42807 = _i_42807 + -1;
        goto L1; // [56] 17
L2: 
        ;
    }

    /** 	if length(SymTab[s]) < S_HAS_DELETE then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23073 = (int)*(((s1_ptr)_2)->base + _s_42805);
    if (IS_SEQUENCE(_23073)){
            _23074 = SEQ_PTR(_23073)->length;
    }
    else {
        _23074 = 1;
    }
    _23073 = NOVALUE;
    if (_23074 >= 54)
    goto L4; // [74] 85

    /** 		return 0*/
    _23072 = NOVALUE;
    _23073 = NOVALUE;
    return 0;
L4: 

    /** 	return SymTab[s][S_HAS_DELETE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23076 = (int)*(((s1_ptr)_2)->base + _s_42805);
    _2 = (int)SEQ_PTR(_23076);
    _23077 = (int)*(((s1_ptr)_2)->base + 54);
    _23076 = NOVALUE;
    Ref(_23077);
    _23072 = NOVALUE;
    _23073 = NOVALUE;
    return _23077;
    ;
}


int _59ObjValue(int _s_42828)
{
    int _local_t_42829 = NOVALUE;
    int _st_42830 = NOVALUE;
    int _tmin_42831 = NOVALUE;
    int _tmax_42832 = NOVALUE;
    int _23090 = NOVALUE;
    int _23088 = NOVALUE;
    int _23087 = NOVALUE;
    int _23085 = NOVALUE;
    int _23082 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_42828)) {
        _1 = (long)(DBL_PTR(_s_42828)->dbl);
        if (UNIQUE(DBL_PTR(_s_42828)) && (DBL_PTR(_s_42828)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42828);
        _s_42828 = _1;
    }

    /** 	st = SymTab[s]*/
    DeRef(_st_42830);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _st_42830 = (int)*(((s1_ptr)_2)->base + _s_42828);
    Ref(_st_42830);

    /** 	tmin = st[S_OBJ_MIN]*/
    DeRef(_tmin_42831);
    _2 = (int)SEQ_PTR(_st_42830);
    _tmin_42831 = (int)*(((s1_ptr)_2)->base + 30);
    Ref(_tmin_42831);

    /** 	tmax = st[S_OBJ_MAX]*/
    DeRef(_tmax_42832);
    _2 = (int)SEQ_PTR(_st_42830);
    _tmax_42832 = (int)*(((s1_ptr)_2)->base + 31);
    Ref(_tmax_42832);

    /** 	if tmin != tmax then*/
    if (binary_op_a(EQUALS, _tmin_42831, _tmax_42832)){
        goto L1; // [29] 41
    }

    /** 		tmin = NOVALUE*/
    Ref(_38NOVALUE_16800);
    DeRef(_tmin_42831);
    _tmin_42831 = _38NOVALUE_16800;
L1: 

    /** 	if st[S_MODE] != M_NORMAL then*/
    _2 = (int)SEQ_PTR(_st_42830);
    _23082 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(EQUALS, _23082, 1)){
        _23082 = NOVALUE;
        goto L2; // [51] 62
    }
    _23082 = NOVALUE;

    /** 		return tmin*/
    DeRef(_local_t_42829);
    DeRef(_st_42830);
    DeRef(_tmax_42832);
    return _tmin_42831;
L2: 

    /** 	local_t = BB_var_obj(s)*/
    _0 = _local_t_42829;
    _local_t_42829 = _59BB_var_obj(_s_42828);
    DeRef(_0);

    /** 	if local_t[MIN] = NOVALUE then*/
    _2 = (int)SEQ_PTR(_local_t_42829);
    _23085 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _23085, _38NOVALUE_16800)){
        _23085 = NOVALUE;
        goto L3; // [80] 91
    }
    _23085 = NOVALUE;

    /** 		return tmin*/
    DeRefDS(_local_t_42829);
    DeRef(_st_42830);
    DeRef(_tmax_42832);
    return _tmin_42831;
L3: 

    /** 	if local_t[MIN] != local_t[MAX] then*/
    _2 = (int)SEQ_PTR(_local_t_42829);
    _23087 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_local_t_42829);
    _23088 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(EQUALS, _23087, _23088)){
        _23087 = NOVALUE;
        _23088 = NOVALUE;
        goto L4; // [105] 116
    }
    _23087 = NOVALUE;
    _23088 = NOVALUE;

    /** 		return tmin*/
    DeRefDS(_local_t_42829);
    DeRef(_st_42830);
    DeRef(_tmax_42832);
    return _tmin_42831;
L4: 

    /** 	return local_t[MIN]*/
    _2 = (int)SEQ_PTR(_local_t_42829);
    _23090 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23090);
    DeRefDS(_local_t_42829);
    DeRef(_st_42830);
    DeRef(_tmin_42831);
    DeRef(_tmax_42832);
    return _23090;
    ;
}


int _59TypeIs(int _x_42863, int _typei_42864)
{
    int _23092 = NOVALUE;
    int _23091 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42863)) {
        _1 = (long)(DBL_PTR(_x_42863)->dbl);
        if (UNIQUE(DBL_PTR(_x_42863)) && (DBL_PTR(_x_42863)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42863);
        _x_42863 = _1;
    }
    if (!IS_ATOM_INT(_typei_42864)) {
        _1 = (long)(DBL_PTR(_typei_42864)->dbl);
        if (UNIQUE(DBL_PTR(_typei_42864)) && (DBL_PTR(_typei_42864)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_typei_42864);
        _typei_42864 = _1;
    }

    /** 	return GType(x) = typei*/
    _23091 = _59GType(_x_42863);
    if (IS_ATOM_INT(_23091)) {
        _23092 = (_23091 == _typei_42864);
    }
    else {
        _23092 = binary_op(EQUALS, _23091, _typei_42864);
    }
    DeRef(_23091);
    _23091 = NOVALUE;
    return _23092;
    ;
}


int _59TypeIsIn(int _x_42869, int _types_42870)
{
    int _23094 = NOVALUE;
    int _23093 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42869)) {
        _1 = (long)(DBL_PTR(_x_42869)->dbl);
        if (UNIQUE(DBL_PTR(_x_42869)) && (DBL_PTR(_x_42869)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42869);
        _x_42869 = _1;
    }

    /** 	return find(GType(x), types)*/
    _23093 = _59GType(_x_42869);
    _23094 = find_from(_23093, _types_42870, 1);
    DeRef(_23093);
    _23093 = NOVALUE;
    DeRefDS(_types_42870);
    return _23094;
    ;
}


int _59TypeIsNot(int _x_42875, int _typei_42876)
{
    int _23096 = NOVALUE;
    int _23095 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42875)) {
        _1 = (long)(DBL_PTR(_x_42875)->dbl);
        if (UNIQUE(DBL_PTR(_x_42875)) && (DBL_PTR(_x_42875)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42875);
        _x_42875 = _1;
    }
    if (!IS_ATOM_INT(_typei_42876)) {
        _1 = (long)(DBL_PTR(_typei_42876)->dbl);
        if (UNIQUE(DBL_PTR(_typei_42876)) && (DBL_PTR(_typei_42876)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_typei_42876);
        _typei_42876 = _1;
    }

    /** 	return GType(x) != typei*/
    _23095 = _59GType(_x_42875);
    if (IS_ATOM_INT(_23095)) {
        _23096 = (_23095 != _typei_42876);
    }
    else {
        _23096 = binary_op(NOTEQ, _23095, _typei_42876);
    }
    DeRef(_23095);
    _23095 = NOVALUE;
    return _23096;
    ;
}


int _59TypeIsNotIn(int _x_42881, int _types_42882)
{
    int _23099 = NOVALUE;
    int _23098 = NOVALUE;
    int _23097 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_42881)) {
        _1 = (long)(DBL_PTR(_x_42881)->dbl);
        if (UNIQUE(DBL_PTR(_x_42881)) && (DBL_PTR(_x_42881)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_42881);
        _x_42881 = _1;
    }

    /** 	return not find(GType(x), types)*/
    _23097 = _59GType(_x_42881);
    _23098 = find_from(_23097, _types_42882, 1);
    DeRef(_23097);
    _23097 = NOVALUE;
    _23099 = (_23098 == 0);
    _23098 = NOVALUE;
    DeRefDS(_types_42882);
    return _23099;
    ;
}


int _59or_type(int _t1_42888, int _t2_42889)
{
    int _23119 = NOVALUE;
    int _23118 = NOVALUE;
    int _23117 = NOVALUE;
    int _23116 = NOVALUE;
    int _23111 = NOVALUE;
    int _23109 = NOVALUE;
    int _23104 = NOVALUE;
    int _23102 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_t1_42888)) {
        _1 = (long)(DBL_PTR(_t1_42888)->dbl);
        if (UNIQUE(DBL_PTR(_t1_42888)) && (DBL_PTR(_t1_42888)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t1_42888);
        _t1_42888 = _1;
    }
    if (!IS_ATOM_INT(_t2_42889)) {
        _1 = (long)(DBL_PTR(_t2_42889)->dbl);
        if (UNIQUE(DBL_PTR(_t2_42889)) && (DBL_PTR(_t2_42889)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t2_42889);
        _t2_42889 = _1;
    }

    /** 	if t1 = TYPE_NULL then*/
    if (_t1_42888 != 0)
    goto L1; // [9] 22

    /** 		return t2*/
    return _t2_42889;
    goto L2; // [19] 307
L1: 

    /** 	elsif t2 = TYPE_NULL then*/
    if (_t2_42889 != 0)
    goto L3; // [26] 39

    /** 		return t1*/
    return _t1_42888;
    goto L2; // [36] 307
L3: 

    /** 	elsif t1 = TYPE_OBJECT or t2 = TYPE_OBJECT then*/
    _23102 = (_t1_42888 == 16);
    if (_23102 != 0) {
        goto L4; // [47] 62
    }
    _23104 = (_t2_42889 == 16);
    if (_23104 == 0)
    {
        DeRef(_23104);
        _23104 = NOVALUE;
        goto L5; // [58] 73
    }
    else{
        DeRef(_23104);
        _23104 = NOVALUE;
    }
L4: 

    /** 		return TYPE_OBJECT*/
    DeRef(_23102);
    _23102 = NOVALUE;
    return 16;
    goto L2; // [70] 307
L5: 

    /** 	elsif t1 = TYPE_SEQUENCE then*/
    if (_t1_42888 != 8)
    goto L6; // [77] 112

    /** 		if t2 = TYPE_SEQUENCE then*/
    if (_t2_42889 != 8)
    goto L7; // [85] 100

    /** 			return TYPE_SEQUENCE*/
    DeRef(_23102);
    _23102 = NOVALUE;
    return 8;
    goto L2; // [97] 307
L7: 

    /** 			return TYPE_OBJECT*/
    DeRef(_23102);
    _23102 = NOVALUE;
    return 16;
    goto L2; // [109] 307
L6: 

    /** 	elsif t2 = TYPE_SEQUENCE then*/
    if (_t2_42889 != 8)
    goto L8; // [116] 151

    /** 		if t1 = TYPE_SEQUENCE then*/
    if (_t1_42888 != 8)
    goto L9; // [124] 139

    /** 			return TYPE_SEQUENCE*/
    DeRef(_23102);
    _23102 = NOVALUE;
    return 8;
    goto L2; // [136] 307
L9: 

    /** 			return TYPE_OBJECT*/
    DeRef(_23102);
    _23102 = NOVALUE;
    return 16;
    goto L2; // [148] 307
L8: 

    /** 	elsif t1 = TYPE_ATOM or t2 = TYPE_ATOM then*/
    _23109 = (_t1_42888 == 4);
    if (_23109 != 0) {
        goto LA; // [159] 174
    }
    _23111 = (_t2_42889 == 4);
    if (_23111 == 0)
    {
        DeRef(_23111);
        _23111 = NOVALUE;
        goto LB; // [170] 185
    }
    else{
        DeRef(_23111);
        _23111 = NOVALUE;
    }
LA: 

    /** 		return TYPE_ATOM*/
    DeRef(_23102);
    _23102 = NOVALUE;
    DeRef(_23109);
    _23109 = NOVALUE;
    return 4;
    goto L2; // [182] 307
LB: 

    /** 	elsif t1 = TYPE_DOUBLE then*/
    if (_t1_42888 != 2)
    goto LC; // [189] 224

    /** 		if t2 = TYPE_INTEGER then*/
    if (_t2_42889 != 1)
    goto LD; // [197] 212

    /** 			return TYPE_ATOM*/
    DeRef(_23102);
    _23102 = NOVALUE;
    DeRef(_23109);
    _23109 = NOVALUE;
    return 4;
    goto L2; // [209] 307
LD: 

    /** 			return TYPE_DOUBLE*/
    DeRef(_23102);
    _23102 = NOVALUE;
    DeRef(_23109);
    _23109 = NOVALUE;
    return 2;
    goto L2; // [221] 307
LC: 

    /** 	elsif t2 = TYPE_DOUBLE then*/
    if (_t2_42889 != 2)
    goto LE; // [228] 263

    /** 		if t1 = TYPE_INTEGER then*/
    if (_t1_42888 != 1)
    goto LF; // [236] 251

    /** 			return TYPE_ATOM*/
    DeRef(_23102);
    _23102 = NOVALUE;
    DeRef(_23109);
    _23109 = NOVALUE;
    return 4;
    goto L2; // [248] 307
LF: 

    /** 			return TYPE_DOUBLE*/
    DeRef(_23102);
    _23102 = NOVALUE;
    DeRef(_23109);
    _23109 = NOVALUE;
    return 2;
    goto L2; // [260] 307
LE: 

    /** 	elsif t1 = TYPE_INTEGER and t2 = TYPE_INTEGER then*/
    _23116 = (_t1_42888 == 1);
    if (_23116 == 0) {
        goto L10; // [271] 296
    }
    _23118 = (_t2_42889 == 1);
    if (_23118 == 0)
    {
        DeRef(_23118);
        _23118 = NOVALUE;
        goto L10; // [282] 296
    }
    else{
        DeRef(_23118);
        _23118 = NOVALUE;
    }

    /** 		return TYPE_INTEGER*/
    DeRef(_23102);
    _23102 = NOVALUE;
    DeRef(_23109);
    _23109 = NOVALUE;
    DeRef(_23116);
    _23116 = NOVALUE;
    return 1;
    goto L2; // [293] 307
L10: 

    /** 		InternalErr(258, {t1, t2})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _t1_42888;
    ((int *)_2)[2] = _t2_42889;
    _23119 = MAKE_SEQ(_1);
    _46InternalErr(258, _23119);
    _23119 = NOVALUE;
L2: 
    ;
}


void _59RemoveFromBB(int _s_42959)
{
    int _int_42960 = NOVALUE;
    int _23121 = NOVALUE;
    int _23120 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_42959)) {
        _1 = (long)(DBL_PTR(_s_42959)->dbl);
        if (UNIQUE(DBL_PTR(_s_42959)) && (DBL_PTR(_s_42959)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42959);
        _s_42959 = _1;
    }

    /** 	for i = 1 to length(BB_info) do*/
    if (IS_SEQUENCE(_59BB_info_42604)){
            _23120 = SEQ_PTR(_59BB_info_42604)->length;
    }
    else {
        _23120 = 1;
    }
    {
        int _i_42962;
        _i_42962 = 1;
L1: 
        if (_i_42962 > _23120){
            goto L2; // [10] 61
        }

        /** 		int = BB_info[i][BB_VAR]*/
        _2 = (int)SEQ_PTR(_59BB_info_42604);
        _23121 = (int)*(((s1_ptr)_2)->base + _i_42962);
        _2 = (int)SEQ_PTR(_23121);
        _int_42960 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_int_42960)){
            _int_42960 = (long)DBL_PTR(_int_42960)->dbl;
        }
        _23121 = NOVALUE;

        /** 		if int = s then*/
        if (_int_42960 != _s_42959)
        goto L3; // [35] 54

        /** 			BB_info = remove( BB_info, int )*/
        {
            s1_ptr assign_space = SEQ_PTR(_59BB_info_42604);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_int_42960)) ? _int_42960 : (long)(DBL_PTR(_int_42960)->dbl);
            int stop = (IS_ATOM_INT(_int_42960)) ? _int_42960 : (long)(DBL_PTR(_int_42960)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_59BB_info_42604), start, &_59BB_info_42604 );
                }
                else Tail(SEQ_PTR(_59BB_info_42604), stop+1, &_59BB_info_42604);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_59BB_info_42604), start, &_59BB_info_42604);
            }
            else {
                assign_slice_seq = &assign_space;
                _59BB_info_42604 = Remove_elements(start, stop, (SEQ_PTR(_59BB_info_42604)->ref == 1));
            }
        }

        /** 			return*/
        return;
L3: 

        /** 	end for*/
        _i_42962 = _i_42962 + 1;
        goto L1; // [56] 17
L2: 
        ;
    }

    /** end procedure*/
    return;
    ;
}


void _59SetBBType(int _s_42980, int _t_42981, int _val_42982, int _etype_42983, int _has_delete_42984)
{
    int _found_42985 = NOVALUE;
    int _i_42986 = NOVALUE;
    int _tn_42987 = NOVALUE;
    int _int_42988 = NOVALUE;
    int _sym_42989 = NOVALUE;
    int _mode_42994 = NOVALUE;
    int _gtype_43009 = NOVALUE;
    int _new_type_43046 = NOVALUE;
    int _bbsym_43069 = NOVALUE;
    int _bbi_43205 = NOVALUE;
    int _23240 = NOVALUE;
    int _23239 = NOVALUE;
    int _23238 = NOVALUE;
    int _23236 = NOVALUE;
    int _23235 = NOVALUE;
    int _23234 = NOVALUE;
    int _23232 = NOVALUE;
    int _23231 = NOVALUE;
    int _23229 = NOVALUE;
    int _23227 = NOVALUE;
    int _23226 = NOVALUE;
    int _23224 = NOVALUE;
    int _23222 = NOVALUE;
    int _23221 = NOVALUE;
    int _23220 = NOVALUE;
    int _23219 = NOVALUE;
    int _23218 = NOVALUE;
    int _23217 = NOVALUE;
    int _23216 = NOVALUE;
    int _23215 = NOVALUE;
    int _23214 = NOVALUE;
    int _23211 = NOVALUE;
    int _23207 = NOVALUE;
    int _23202 = NOVALUE;
    int _23200 = NOVALUE;
    int _23199 = NOVALUE;
    int _23198 = NOVALUE;
    int _23196 = NOVALUE;
    int _23194 = NOVALUE;
    int _23193 = NOVALUE;
    int _23192 = NOVALUE;
    int _23190 = NOVALUE;
    int _23189 = NOVALUE;
    int _23187 = NOVALUE;
    int _23186 = NOVALUE;
    int _23185 = NOVALUE;
    int _23183 = NOVALUE;
    int _23182 = NOVALUE;
    int _23179 = NOVALUE;
    int _23178 = NOVALUE;
    int _23177 = NOVALUE;
    int _23175 = NOVALUE;
    int _23173 = NOVALUE;
    int _23172 = NOVALUE;
    int _23170 = NOVALUE;
    int _23169 = NOVALUE;
    int _23168 = NOVALUE;
    int _23166 = NOVALUE;
    int _23165 = NOVALUE;
    int _23154 = NOVALUE;
    int _23152 = NOVALUE;
    int _23149 = NOVALUE;
    int _23148 = NOVALUE;
    int _23145 = NOVALUE;
    int _23144 = NOVALUE;
    int _23143 = NOVALUE;
    int _23141 = NOVALUE;
    int _23140 = NOVALUE;
    int _23139 = NOVALUE;
    int _23137 = NOVALUE;
    int _23136 = NOVALUE;
    int _23134 = NOVALUE;
    int _23131 = NOVALUE;
    int _23129 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_42980)) {
        _1 = (long)(DBL_PTR(_s_42980)->dbl);
        if (UNIQUE(DBL_PTR(_s_42980)) && (DBL_PTR(_s_42980)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_42980);
        _s_42980 = _1;
    }
    if (!IS_ATOM_INT(_t_42981)) {
        _1 = (long)(DBL_PTR(_t_42981)->dbl);
        if (UNIQUE(DBL_PTR(_t_42981)) && (DBL_PTR(_t_42981)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_t_42981);
        _t_42981 = _1;
    }
    if (!IS_ATOM_INT(_etype_42983)) {
        _1 = (long)(DBL_PTR(_etype_42983)->dbl);
        if (UNIQUE(DBL_PTR(_etype_42983)) && (DBL_PTR(_etype_42983)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_etype_42983);
        _etype_42983 = _1;
    }
    if (!IS_ATOM_INT(_has_delete_42984)) {
        _1 = (long)(DBL_PTR(_has_delete_42984)->dbl);
        if (UNIQUE(DBL_PTR(_has_delete_42984)) && (DBL_PTR(_has_delete_42984)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_has_delete_42984);
        _has_delete_42984 = _1;
    }

    /** 	if has_delete then*/
    if (_has_delete_42984 == 0)
    {
        goto L1; // [13] 27
    }
    else{
    }

    /** 		p_has_delete = 1*/
    _59p_has_delete_42799 = 1;

    /** 		g_has_delete = 1*/
    _59g_has_delete_42798 = 1;
L1: 

    /** 	sym = SymTab[s]*/
    DeRef(_sym_42989);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _sym_42989 = (int)*(((s1_ptr)_2)->base + _s_42980);
    Ref(_sym_42989);

    /** 	SymTab[s] = 0*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _s_42980);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	integer mode = sym[S_MODE]*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _mode_42994 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_42994))
    _mode_42994 = (long)DBL_PTR(_mode_42994)->dbl;

    /** 	if mode = M_NORMAL or mode = M_TEMP  then*/
    _23129 = (_mode_42994 == 1);
    if (_23129 != 0) {
        goto L2; // [61] 76
    }
    _23131 = (_mode_42994 == 3);
    if (_23131 == 0)
    {
        DeRef(_23131);
        _23131 = NOVALUE;
        goto L3; // [72] 1197
    }
    else{
        DeRef(_23131);
        _23131 = NOVALUE;
    }
L2: 

    /** 		found = FALSE*/
    _found_42985 = _9FALSE_426;

    /** 		if mode = M_TEMP then*/
    if (_mode_42994 != 3)
    goto L4; // [89] 467

    /** 			sym[S_GTYPE] = t*/
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _t_42981;
    DeRef(_1);

    /** 			sym[S_SEQ_ELEM] = etype*/
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _etype_42983;
    DeRef(_1);

    /** 			integer gtype = sym[S_GTYPE]*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _gtype_43009 = (int)*(((s1_ptr)_2)->base + 36);
    if (!IS_ATOM_INT(_gtype_43009))
    _gtype_43009 = (long)DBL_PTR(_gtype_43009)->dbl;

    /** 			if gtype = TYPE_OBJECT*/
    _23134 = (_gtype_43009 == 16);
    if (_23134 != 0) {
        goto L5; // [125] 140
    }
    _23136 = (_gtype_43009 == 8);
    if (_23136 == 0)
    {
        DeRef(_23136);
        _23136 = NOVALUE;
        goto L6; // [136] 213
    }
    else{
        DeRef(_23136);
        _23136 = NOVALUE;
    }
L5: 

    /** 				if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23137 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23137, 0)){
        _23137 = NOVALUE;
        goto L7; // [148] 165
    }
    _23137 = NOVALUE;

    /** 					sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L8; // [162] 180
L7: 

    /** 					sym[S_SEQ_LEN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23139 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23139);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _23139;
    if( _1 != _23139 ){
        DeRef(_1);
    }
    _23139 = NOVALUE;
L8: 

    /** 				sym[S_OBJ] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);

    /** 				sym[S_OBJ_MIN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);

    /** 				sym[S_OBJ_MAX] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L9; // [210] 252
L6: 

    /** 				sym[S_OBJ_MIN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23140 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23140);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _23140;
    if( _1 != _23140 ){
        DeRef(_1);
    }
    _23140 = NOVALUE;

    /** 				sym[S_OBJ_MAX] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23141 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23141);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _23141;
    if( _1 != _23141 ){
        DeRef(_1);
    }
    _23141 = NOVALUE;

    /** 				sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
L9: 

    /** 			if not Initializing then*/
    if (_38Initializing_17029 != 0)
    goto LA; // [256] 326

    /** 				integer new_type = or_type(temp_name_type[sym[S_TEMP_NAME]][T_GTYPE_NEW], t)*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23143 = (int)*(((s1_ptr)_2)->base + 34);
    _2 = (int)SEQ_PTR(_38temp_name_type_17031);
    if (!IS_ATOM_INT(_23143)){
        _23144 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23143)->dbl));
    }
    else{
        _23144 = (int)*(((s1_ptr)_2)->base + _23143);
    }
    _2 = (int)SEQ_PTR(_23144);
    _23145 = (int)*(((s1_ptr)_2)->base + 2);
    _23144 = NOVALUE;
    Ref(_23145);
    _new_type_43046 = _59or_type(_23145, _t_42981);
    _23145 = NOVALUE;
    if (!IS_ATOM_INT(_new_type_43046)) {
        _1 = (long)(DBL_PTR(_new_type_43046)->dbl);
        if (UNIQUE(DBL_PTR(_new_type_43046)) && (DBL_PTR(_new_type_43046)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_type_43046);
        _new_type_43046 = _1;
    }

    /** 				if new_type = TYPE_NULL then*/
    if (_new_type_43046 != 0)
    goto LB; // [290] 304

    /** 					new_type = TYPE_OBJECT*/
    _new_type_43046 = 16;
LB: 

    /** 				temp_name_type[sym[S_TEMP_NAME]][T_GTYPE_NEW] = new_type*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23148 = (int)*(((s1_ptr)_2)->base + 34);
    _2 = (int)SEQ_PTR(_38temp_name_type_17031);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _38temp_name_type_17031 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_23148))
    _3 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_23148)->dbl));
    else
    _3 = (int)(_23148 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _new_type_43046;
    DeRef(_1);
    _23149 = NOVALUE;
LA: 

    /** 			tn = sym[S_TEMP_NAME]*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _tn_42987 = (int)*(((s1_ptr)_2)->base + 34);
    if (!IS_ATOM_INT(_tn_42987))
    _tn_42987 = (long)DBL_PTR(_tn_42987)->dbl;

    /** 			i = 1*/
    _i_42986 = 1;

    /** 			while i <= length(BB_info) do*/
LC: 
    if (IS_SEQUENCE(_59BB_info_42604)){
            _23152 = SEQ_PTR(_59BB_info_42604)->length;
    }
    else {
        _23152 = 1;
    }
    if (_i_42986 > _23152)
    goto LD; // [351] 462

    /** 				sequence bbsym*/

    /** 				int = BB_info[i][BB_VAR]*/
    _2 = (int)SEQ_PTR(_59BB_info_42604);
    _23154 = (int)*(((s1_ptr)_2)->base + _i_42986);
    _2 = (int)SEQ_PTR(_23154);
    _int_42988 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_int_42988)){
        _int_42988 = (long)DBL_PTR(_int_42988)->dbl;
    }
    _23154 = NOVALUE;

    /** 				if int = s then*/
    if (_int_42988 != _s_42980)
    goto LE; // [375] 389

    /** 					bbsym = sym*/
    RefDS(_sym_42989);
    DeRef(_bbsym_43069);
    _bbsym_43069 = _sym_42989;
    goto LF; // [386] 400
LE: 

    /** 					bbsym = SymTab[int]*/
    DeRef(_bbsym_43069);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _bbsym_43069 = (int)*(((s1_ptr)_2)->base + _int_42988);
    Ref(_bbsym_43069);
LF: 

    /** 				int = bbsym[S_MODE]*/
    _2 = (int)SEQ_PTR(_bbsym_43069);
    _int_42988 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_int_42988))
    _int_42988 = (long)DBL_PTR(_int_42988)->dbl;

    /** 				if int = M_TEMP then*/
    if (_int_42988 != 3)
    goto L10; // [414] 449

    /** 					int = bbsym[S_TEMP_NAME]*/
    _2 = (int)SEQ_PTR(_bbsym_43069);
    _int_42988 = (int)*(((s1_ptr)_2)->base + 34);
    if (!IS_ATOM_INT(_int_42988))
    _int_42988 = (long)DBL_PTR(_int_42988)->dbl;

    /** 					if int = tn then*/
    if (_int_42988 != _tn_42987)
    goto L11; // [428] 448

    /** 						found = TRUE*/
    _found_42985 = _9TRUE_428;

    /** 						exit*/
    DeRefDS(_bbsym_43069);
    _bbsym_43069 = NOVALUE;
    goto LD; // [445] 462
L11: 
L10: 

    /** 				i += 1*/
    _i_42986 = _i_42986 + 1;
    DeRef(_bbsym_43069);
    _bbsym_43069 = NOVALUE;

    /** 			end while*/
    goto LC; // [459] 346
LD: 
    goto L12; // [464] 893
L4: 

    /** 			if t != TYPE_NULL then*/
    if (_t_42981 == 0)
    goto L13; // [471] 826

    /** 				if not Initializing then*/
    if (_38Initializing_17029 != 0)
    goto L14; // [479] 502

    /** 					sym[S_GTYPE_NEW] = or_type(sym[S_GTYPE_NEW], t)*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23165 = (int)*(((s1_ptr)_2)->base + 38);
    Ref(_23165);
    _23166 = _59or_type(_23165, _t_42981);
    _23165 = NOVALUE;
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 38);
    _1 = *(int *)_2;
    *(int *)_2 = _23166;
    if( _1 != _23166 ){
        DeRef(_1);
    }
    _23166 = NOVALUE;
L14: 

    /** 				if t = TYPE_SEQUENCE then*/
    if (_t_42981 != 8)
    goto L15; // [506] 635

    /** 					sym[S_SEQ_ELEM_NEW] =*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23168 = (int)*(((s1_ptr)_2)->base + 40);
    Ref(_23168);
    _23169 = _59or_type(_23168, _etype_42983);
    _23168 = NOVALUE;
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = _23169;
    if( _1 != _23169 ){
        DeRef(_1);
    }
    _23169 = NOVALUE;

    /** 					if val[MIN] != -1 then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23170 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _23170, -1)){
        _23170 = NOVALUE;
        goto L16; // [537] 825
    }
    _23170 = NOVALUE;

    /** 						if sym[S_SEQ_LEN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23172 = (int)*(((s1_ptr)_2)->base + 39);
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23173 = (int)NewDouble((double)-0xC0000000);
        else
        _23173 = - _38NOVALUE_16800;
    }
    else {
        _23173 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    if (binary_op_a(NOTEQ, _23172, _23173)){
        _23172 = NOVALUE;
        DeRef(_23173);
        _23173 = NOVALUE;
        goto L17; // [554] 601
    }
    _23172 = NOVALUE;
    DeRef(_23173);
    _23173 = NOVALUE;

    /** 							if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23175 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23175, 0)){
        _23175 = NOVALUE;
        goto L18; // [566] 583
    }
    _23175 = NOVALUE;

    /** 								sym[S_SEQ_LEN_NEW] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L16; // [580] 825
L18: 

    /** 								sym[S_SEQ_LEN_NEW] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23177 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23177);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _23177;
    if( _1 != _23177 ){
        DeRef(_1);
    }
    _23177 = NOVALUE;
    goto L16; // [598] 825
L17: 

    /** 						elsif val[MIN] != sym[S_SEQ_LEN_NEW] then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23178 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_sym_42989);
    _23179 = (int)*(((s1_ptr)_2)->base + 39);
    if (binary_op_a(EQUALS, _23178, _23179)){
        _23178 = NOVALUE;
        _23179 = NOVALUE;
        goto L16; // [615] 825
    }
    _23178 = NOVALUE;
    _23179 = NOVALUE;

    /** 							sym[S_SEQ_LEN_NEW] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L16; // [632] 825
L15: 

    /** 				elsif t = TYPE_INTEGER then*/
    if (_t_42981 != 1)
    goto L19; // [639] 776

    /** 					if sym[S_OBJ_MIN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23182 = (int)*(((s1_ptr)_2)->base + 41);
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23183 = (int)NewDouble((double)-0xC0000000);
        else
        _23183 = - _38NOVALUE_16800;
    }
    else {
        _23183 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    if (binary_op_a(NOTEQ, _23182, _23183)){
        _23182 = NOVALUE;
        DeRef(_23183);
        _23183 = NOVALUE;
        goto L1A; // [656] 691
    }
    _23182 = NOVALUE;
    DeRef(_23183);
    _23183 = NOVALUE;

    /** 						sym[S_OBJ_MIN_NEW] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23185 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23185);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _23185;
    if( _1 != _23185 ){
        DeRef(_1);
    }
    _23185 = NOVALUE;

    /** 						sym[S_OBJ_MAX_NEW] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23186 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23186);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 42);
    _1 = *(int *)_2;
    *(int *)_2 = _23186;
    if( _1 != _23186 ){
        DeRef(_1);
    }
    _23186 = NOVALUE;
    goto L16; // [688] 825
L1A: 

    /** 					elsif sym[S_OBJ_MIN_NEW] != NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23187 = (int)*(((s1_ptr)_2)->base + 41);
    if (binary_op_a(EQUALS, _23187, _38NOVALUE_16800)){
        _23187 = NOVALUE;
        goto L16; // [701] 825
    }
    _23187 = NOVALUE;

    /** 						if val[MIN] < sym[S_OBJ_MIN_NEW] then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23189 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_sym_42989);
    _23190 = (int)*(((s1_ptr)_2)->base + 41);
    if (binary_op_a(GREATEREQ, _23189, _23190)){
        _23189 = NOVALUE;
        _23190 = NOVALUE;
        goto L1B; // [719] 738
    }
    _23189 = NOVALUE;
    _23190 = NOVALUE;

    /** 							sym[S_OBJ_MIN_NEW] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23192 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23192);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _23192;
    if( _1 != _23192 ){
        DeRef(_1);
    }
    _23192 = NOVALUE;
L1B: 

    /** 						if val[MAX] > sym[S_OBJ_MAX_NEW] then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23193 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_sym_42989);
    _23194 = (int)*(((s1_ptr)_2)->base + 42);
    if (binary_op_a(LESSEQ, _23193, _23194)){
        _23193 = NOVALUE;
        _23194 = NOVALUE;
        goto L16; // [752] 825
    }
    _23193 = NOVALUE;
    _23194 = NOVALUE;

    /** 							sym[S_OBJ_MAX_NEW] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23196 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23196);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 42);
    _1 = *(int *)_2;
    *(int *)_2 = _23196;
    if( _1 != _23196 ){
        DeRef(_1);
    }
    _23196 = NOVALUE;
    goto L16; // [773] 825
L19: 

    /** 					sym[S_OBJ_MIN_NEW] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);

    /** 					if t = TYPE_OBJECT then*/
    if (_t_42981 != 16)
    goto L1C; // [790] 824

    /** 						sym[S_SEQ_ELEM_NEW] =*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23198 = (int)*(((s1_ptr)_2)->base + 40);
    Ref(_23198);
    _23199 = _59or_type(_23198, _etype_42983);
    _23198 = NOVALUE;
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = _23199;
    if( _1 != _23199 ){
        DeRef(_1);
    }
    _23199 = NOVALUE;

    /** 						sym[S_SEQ_LEN_NEW] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
L1C: 
L16: 
L13: 

    /** 			i = 1*/
    _i_42986 = 1;

    /** 			while i <= length(BB_info) do*/
L1D: 
    if (IS_SEQUENCE(_59BB_info_42604)){
            _23200 = SEQ_PTR(_59BB_info_42604)->length;
    }
    else {
        _23200 = 1;
    }
    if (_i_42986 > _23200)
    goto L1E; // [841] 892

    /** 				int = BB_info[i][BB_VAR]*/
    _2 = (int)SEQ_PTR(_59BB_info_42604);
    _23202 = (int)*(((s1_ptr)_2)->base + _i_42986);
    _2 = (int)SEQ_PTR(_23202);
    _int_42988 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_int_42988)){
        _int_42988 = (long)DBL_PTR(_int_42988)->dbl;
    }
    _23202 = NOVALUE;

    /** 				if int = s then*/
    if (_int_42988 != _s_42980)
    goto L1F; // [863] 881

    /** 					found = TRUE*/
    _found_42985 = _9TRUE_428;

    /** 					exit*/
    goto L1E; // [878] 892
L1F: 

    /** 				i += 1*/
    _i_42986 = _i_42986 + 1;

    /** 			end while*/
    goto L1D; // [889] 836
L1E: 
L12: 

    /** 		if not found then*/
    if (_found_42985 != 0)
    goto L20; // [895] 911

    /** 			BB_info = append(BB_info, repeat(0, 6))*/
    _23207 = Repeat(0, 6);
    RefDS(_23207);
    Append(&_59BB_info_42604, _59BB_info_42604, _23207);
    DeRefDS(_23207);
    _23207 = NOVALUE;
L20: 

    /** 		if t = TYPE_NULL then*/
    if (_t_42981 != 0)
    goto L21; // [915] 955

    /** 			if not found then*/
    if (_found_42985 != 0)
    goto L22; // [921] 1338

    /** 				BB_info[i] = dummy_bb*/
    RefDS(_59dummy_bb_42969);
    _2 = (int)SEQ_PTR(_59BB_info_42604);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _59BB_info_42604 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_42986);
    _1 = *(int *)_2;
    *(int *)_2 = _59dummy_bb_42969;
    DeRef(_1);

    /** 				BB_info[i][BB_VAR] = s*/
    _2 = (int)SEQ_PTR(_59BB_info_42604);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _59BB_info_42604 = MAKE_SEQ(_2);
    }
    _3 = (int)(_i_42986 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _s_42980;
    DeRef(_1);
    _23211 = NOVALUE;
    goto L22; // [952] 1338
L21: 

    /** 			sequence bbi = BB_info[i]*/
    DeRef(_bbi_43205);
    _2 = (int)SEQ_PTR(_59BB_info_42604);
    _bbi_43205 = (int)*(((s1_ptr)_2)->base + _i_42986);
    Ref(_bbi_43205);

    /** 			BB_info[i] = 0*/
    _2 = (int)SEQ_PTR(_59BB_info_42604);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _59BB_info_42604 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_42986);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			bbi[BB_VAR] = s*/
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _s_42980;
    DeRef(_1);

    /** 			bbi[BB_TYPE] = t*/
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _t_42981;
    DeRef(_1);

    /** 			bbi[BB_DELETE] = has_delete*/
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _has_delete_42984;
    DeRef(_1);

    /** 			if t = TYPE_SEQUENCE and val[MIN] = -1 then*/
    _23214 = (_t_42981 == 8);
    if (_23214 == 0) {
        goto L23; // [1007] 1099
    }
    _2 = (int)SEQ_PTR(_val_42982);
    _23216 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_23216)) {
        _23217 = (_23216 == -1);
    }
    else {
        _23217 = binary_op(EQUALS, _23216, -1);
    }
    _23216 = NOVALUE;
    if (_23217 == 0) {
        DeRef(_23217);
        _23217 = NOVALUE;
        goto L23; // [1022] 1099
    }
    else {
        if (!IS_ATOM_INT(_23217) && DBL_PTR(_23217)->dbl == 0.0){
            DeRef(_23217);
            _23217 = NOVALUE;
            goto L23; // [1022] 1099
        }
        DeRef(_23217);
        _23217 = NOVALUE;
    }
    DeRef(_23217);
    _23217 = NOVALUE;

    /** 				if found and bbi[BB_ELEM] != TYPE_NULL then*/
    if (_found_42985 == 0) {
        goto L24; // [1027] 1069
    }
    _2 = (int)SEQ_PTR(_bbi_43205);
    _23219 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_23219)) {
        _23220 = (_23219 != 0);
    }
    else {
        _23220 = binary_op(NOTEQ, _23219, 0);
    }
    _23219 = NOVALUE;
    if (_23220 == 0) {
        DeRef(_23220);
        _23220 = NOVALUE;
        goto L24; // [1044] 1069
    }
    else {
        if (!IS_ATOM_INT(_23220) && DBL_PTR(_23220)->dbl == 0.0){
            DeRef(_23220);
            _23220 = NOVALUE;
            goto L24; // [1044] 1069
        }
        DeRef(_23220);
        _23220 = NOVALUE;
    }
    DeRef(_23220);
    _23220 = NOVALUE;

    /** 					bbi[BB_ELEM] = or_type(bbi[BB_ELEM], etype)*/
    _2 = (int)SEQ_PTR(_bbi_43205);
    _23221 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_23221);
    _23222 = _59or_type(_23221, _etype_42983);
    _23221 = NOVALUE;
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _23222;
    if( _1 != _23222 ){
        DeRef(_1);
    }
    _23222 = NOVALUE;
    goto L25; // [1066] 1080
L24: 

    /** 					bbi[BB_ELEM] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
L25: 

    /** 				if not found then*/
    if (_found_42985 != 0)
    goto L26; // [1082] 1183

    /** 					bbi[BB_SEQLEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L26; // [1096] 1183
L23: 

    /** 				bbi[BB_ELEM] = etype*/
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _etype_42983;
    DeRef(_1);

    /** 				if t = TYPE_SEQUENCE or t = TYPE_OBJECT then*/
    _23224 = (_t_42981 == 8);
    if (_23224 != 0) {
        goto L27; // [1115] 1130
    }
    _23226 = (_t_42981 == 16);
    if (_23226 == 0)
    {
        DeRef(_23226);
        _23226 = NOVALUE;
        goto L28; // [1126] 1173
    }
    else{
        DeRef(_23226);
        _23226 = NOVALUE;
    }
L27: 

    /** 					if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23227 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23227, 0)){
        _23227 = NOVALUE;
        goto L29; // [1138] 1155
    }
    _23227 = NOVALUE;

    /** 						bbi[BB_SEQLEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L2A; // [1152] 1182
L29: 

    /** 						bbi[BB_SEQLEN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23229 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23229);
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _23229;
    if( _1 != _23229 ){
        DeRef(_1);
    }
    _23229 = NOVALUE;
    goto L2A; // [1170] 1182
L28: 

    /** 					bbi[BB_OBJ] = val*/
    RefDS(_val_42982);
    _2 = (int)SEQ_PTR(_bbi_43205);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bbi_43205 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _val_42982;
    DeRef(_1);
L2A: 
L26: 

    /** 			BB_info[i] = bbi*/
    RefDS(_bbi_43205);
    _2 = (int)SEQ_PTR(_59BB_info_42604);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _59BB_info_42604 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _i_42986);
    _1 = *(int *)_2;
    *(int *)_2 = _bbi_43205;
    DeRef(_1);
    DeRefDS(_bbi_43205);
    _bbi_43205 = NOVALUE;
    goto L22; // [1194] 1338
L3: 

    /** 	elsif mode = M_CONSTANT then*/
    if (_mode_42994 != 2)
    goto L2B; // [1201] 1337

    /** 		sym[S_GTYPE] = t*/
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _t_42981;
    DeRef(_1);

    /** 		sym[S_SEQ_ELEM] = etype*/
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _etype_42983;
    DeRef(_1);

    /** 		if sym[S_GTYPE] = TYPE_SEQUENCE or*/
    _2 = (int)SEQ_PTR(_sym_42989);
    _23231 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23231)) {
        _23232 = (_23231 == 8);
    }
    else {
        _23232 = binary_op(EQUALS, _23231, 8);
    }
    _23231 = NOVALUE;
    if (IS_ATOM_INT(_23232)) {
        if (_23232 != 0) {
            goto L2C; // [1235] 1256
        }
    }
    else {
        if (DBL_PTR(_23232)->dbl != 0.0) {
            goto L2C; // [1235] 1256
        }
    }
    _2 = (int)SEQ_PTR(_sym_42989);
    _23234 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23234)) {
        _23235 = (_23234 == 16);
    }
    else {
        _23235 = binary_op(EQUALS, _23234, 16);
    }
    _23234 = NOVALUE;
    if (_23235 == 0) {
        DeRef(_23235);
        _23235 = NOVALUE;
        goto L2D; // [1252] 1299
    }
    else {
        if (!IS_ATOM_INT(_23235) && DBL_PTR(_23235)->dbl == 0.0){
            DeRef(_23235);
            _23235 = NOVALUE;
            goto L2D; // [1252] 1299
        }
        DeRef(_23235);
        _23235 = NOVALUE;
    }
    DeRef(_23235);
    _23235 = NOVALUE;
L2C: 

    /** 			if val[MIN] < 0 then*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23236 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(GREATEREQ, _23236, 0)){
        _23236 = NOVALUE;
        goto L2E; // [1264] 1281
    }
    _23236 = NOVALUE;

    /** 				sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L2F; // [1278] 1328
L2E: 

    /** 				sym[S_SEQ_LEN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23238 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23238);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _23238;
    if( _1 != _23238 ){
        DeRef(_1);
    }
    _23238 = NOVALUE;
    goto L2F; // [1296] 1328
L2D: 

    /** 			sym[S_OBJ_MIN] = val[MIN]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23239 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_23239);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _23239;
    if( _1 != _23239 ){
        DeRef(_1);
    }
    _23239 = NOVALUE;

    /** 			sym[S_OBJ_MAX] = val[MAX]*/
    _2 = (int)SEQ_PTR(_val_42982);
    _23240 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_23240);
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _23240;
    if( _1 != _23240 ){
        DeRef(_1);
    }
    _23240 = NOVALUE;
L2F: 

    /** 		sym[S_HAS_DELETE] = has_delete*/
    _2 = (int)SEQ_PTR(_sym_42989);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_42989 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 54);
    _1 = *(int *)_2;
    *(int *)_2 = _has_delete_42984;
    DeRef(_1);
L2B: 
L22: 

    /** 	SymTab[s] = sym*/
    RefDS(_sym_42989);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _s_42980);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_42989;
    DeRef(_1);

    /** end procedure*/
    DeRefDS(_val_42982);
    DeRefDS(_sym_42989);
    DeRef(_23129);
    _23129 = NOVALUE;
    DeRef(_23134);
    _23134 = NOVALUE;
    _23143 = NOVALUE;
    _23148 = NOVALUE;
    DeRef(_23214);
    _23214 = NOVALUE;
    DeRef(_23224);
    _23224 = NOVALUE;
    DeRef(_23232);
    _23232 = NOVALUE;
    return;
    ;
}


void _59CName(int _s_43279)
{
    int _v_43280 = NOVALUE;
    int _mode_43282 = NOVALUE;
    int _23301 = NOVALUE;
    int _23300 = NOVALUE;
    int _23299 = NOVALUE;
    int _23298 = NOVALUE;
    int _23297 = NOVALUE;
    int _23296 = NOVALUE;
    int _23295 = NOVALUE;
    int _23294 = NOVALUE;
    int _23293 = NOVALUE;
    int _23292 = NOVALUE;
    int _23290 = NOVALUE;
    int _23288 = NOVALUE;
    int _23287 = NOVALUE;
    int _23286 = NOVALUE;
    int _23285 = NOVALUE;
    int _23284 = NOVALUE;
    int _23283 = NOVALUE;
    int _23282 = NOVALUE;
    int _23281 = NOVALUE;
    int _23280 = NOVALUE;
    int _23279 = NOVALUE;
    int _23278 = NOVALUE;
    int _23276 = NOVALUE;
    int _23275 = NOVALUE;
    int _23274 = NOVALUE;
    int _23273 = NOVALUE;
    int _23272 = NOVALUE;
    int _23271 = NOVALUE;
    int _23269 = NOVALUE;
    int _23268 = NOVALUE;
    int _23266 = NOVALUE;
    int _23265 = NOVALUE;
    int _23264 = NOVALUE;
    int _23263 = NOVALUE;
    int _23262 = NOVALUE;
    int _23261 = NOVALUE;
    int _23260 = NOVALUE;
    int _23259 = NOVALUE;
    int _23258 = NOVALUE;
    int _23257 = NOVALUE;
    int _23256 = NOVALUE;
    int _23255 = NOVALUE;
    int _23253 = NOVALUE;
    int _23252 = NOVALUE;
    int _23250 = NOVALUE;
    int _23249 = NOVALUE;
    int _23248 = NOVALUE;
    int _23247 = NOVALUE;
    int _23246 = NOVALUE;
    int _23245 = NOVALUE;
    int _23242 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_s_43279)) {
        _1 = (long)(DBL_PTR(_s_43279)->dbl);
        if (UNIQUE(DBL_PTR(_s_43279)) && (DBL_PTR(_s_43279)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_43279);
        _s_43279 = _1;
    }

    /** 	v = ObjValue(s)*/
    _0 = _v_43280;
    _v_43280 = _59ObjValue(_s_43279);
    DeRef(_0);

    /** 	integer mode = SymTab[s][S_MODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23242 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23242);
    _mode_43282 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_43282)){
        _mode_43282 = (long)DBL_PTR(_mode_43282)->dbl;
    }
    _23242 = NOVALUE;

    /**  	if mode = M_NORMAL then*/
    if (_mode_43282 != 1)
    goto L1; // [29] 240

    /** 		if LeftSym = FALSE and GType(s) = TYPE_INTEGER and v != NOVALUE then*/
    _23245 = (_59LeftSym_42605 == _9FALSE_426);
    if (_23245 == 0) {
        _23246 = 0;
        goto L2; // [43] 61
    }
    _23247 = _59GType(_s_43279);
    if (IS_ATOM_INT(_23247)) {
        _23248 = (_23247 == 1);
    }
    else {
        _23248 = binary_op(EQUALS, _23247, 1);
    }
    DeRef(_23247);
    _23247 = NOVALUE;
    if (IS_ATOM_INT(_23248))
    _23246 = (_23248 != 0);
    else
    _23246 = DBL_PTR(_23248)->dbl != 0.0;
L2: 
    if (_23246 == 0) {
        goto L3; // [61] 84
    }
    if (IS_ATOM_INT(_v_43280) && IS_ATOM_INT(_38NOVALUE_16800)) {
        _23250 = (_v_43280 != _38NOVALUE_16800);
    }
    else {
        _23250 = binary_op(NOTEQ, _v_43280, _38NOVALUE_16800);
    }
    if (_23250 == 0) {
        DeRef(_23250);
        _23250 = NOVALUE;
        goto L3; // [72] 84
    }
    else {
        if (!IS_ATOM_INT(_23250) && DBL_PTR(_23250)->dbl == 0.0){
            DeRef(_23250);
            _23250 = NOVALUE;
            goto L3; // [72] 84
        }
        DeRef(_23250);
        _23250 = NOVALUE;
    }
    DeRef(_23250);
    _23250 = NOVALUE;

    /** 			c_printf("%d", v)*/
    RefDS(_23251);
    Ref(_v_43280);
    _56c_printf(_23251, _v_43280);
    goto L4; // [81] 166
L3: 

    /** 			if SymTab[s][S_SCOPE] > SC_PRIVATE then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23252 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23252);
    _23253 = (int)*(((s1_ptr)_2)->base + 4);
    _23252 = NOVALUE;
    if (binary_op_a(LESSEQ, _23253, 3)){
        _23253 = NOVALUE;
        goto L5; // [100] 142
    }
    _23253 = NOVALUE;

    /** 				c_printf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23255 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23255);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23256 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23256 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _23255 = NOVALUE;
    RefDS(_22756);
    Ref(_23256);
    _56c_printf(_22756, _23256);
    _23256 = NOVALUE;

    /** 				c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23257 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23257);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23258 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23258 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _23257 = NOVALUE;
    Ref(_23258);
    _56c_puts(_23258);
    _23258 = NOVALUE;
    goto L6; // [139] 165
L5: 

    /** 				c_puts("_")*/
    RefDS(_22735);
    _56c_puts(_22735);

    /** 				c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23259 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23259);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23260 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23260 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _23259 = NOVALUE;
    Ref(_23260);
    _56c_puts(_23260);
    _23260 = NOVALUE;
L6: 
L4: 

    /** 		if s != CurrentSub and SymTab[s][S_NREFS] < 2 then*/
    _23261 = (_s_43279 != _38CurrentSub_16954);
    if (_23261 == 0) {
        goto L7; // [174] 222
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23263 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23263);
    _23264 = (int)*(((s1_ptr)_2)->base + 12);
    _23263 = NOVALUE;
    if (IS_ATOM_INT(_23264)) {
        _23265 = (_23264 < 2);
    }
    else {
        _23265 = binary_op(LESS, _23264, 2);
    }
    _23264 = NOVALUE;
    if (_23265 == 0) {
        DeRef(_23265);
        _23265 = NOVALUE;
        goto L7; // [195] 222
    }
    else {
        if (!IS_ATOM_INT(_23265) && DBL_PTR(_23265)->dbl == 0.0){
            DeRef(_23265);
            _23265 = NOVALUE;
            goto L7; // [195] 222
        }
        DeRef(_23265);
        _23265 = NOVALUE;
    }
    DeRef(_23265);
    _23265 = NOVALUE;

    /** 			SymTab[s][S_NREFS] += 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_43279 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _23268 = (int)*(((s1_ptr)_2)->base + 12);
    _23266 = NOVALUE;
    if (IS_ATOM_INT(_23268)) {
        _23269 = _23268 + 1;
        if (_23269 > MAXINT){
            _23269 = NewDouble((double)_23269);
        }
    }
    else
    _23269 = binary_op(PLUS, 1, _23268);
    _23268 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _23269;
    if( _1 != _23269 ){
        DeRef(_1);
    }
    _23269 = NOVALUE;
    _23266 = NOVALUE;
L7: 

    /** 		SetBBType(s, TYPE_NULL, novalue, TYPE_OBJECT, 0) -- record that this var was referenced in this BB*/
    RefDS(_56novalue_46451);
    _59SetBBType(_s_43279, 0, _56novalue_46451, 16, 0);
    goto L8; // [237] 490
L1: 

    /**  	elsif mode = M_CONSTANT then*/
    if (_mode_43282 != 2)
    goto L9; // [244] 419

    /** 		if (integer( sym_obj( s ) ) and SymTab[s][S_GTYPE] != TYPE_DOUBLE ) or (LeftSym = FALSE and TypeIs(s, TYPE_INTEGER) and v != NOVALUE) then*/
    _23271 = _55sym_obj(_s_43279);
    if (IS_ATOM_INT(_23271))
    _23272 = 1;
    else if (IS_ATOM_DBL(_23271))
    _23272 = IS_ATOM_INT(DoubleToInt(_23271));
    else
    _23272 = 0;
    DeRef(_23271);
    _23271 = NOVALUE;
    if (_23272 == 0) {
        _23273 = 0;
        goto LA; // [257] 283
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23274 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23274);
    _23275 = (int)*(((s1_ptr)_2)->base + 36);
    _23274 = NOVALUE;
    if (IS_ATOM_INT(_23275)) {
        _23276 = (_23275 != 2);
    }
    else {
        _23276 = binary_op(NOTEQ, _23275, 2);
    }
    _23275 = NOVALUE;
    if (IS_ATOM_INT(_23276))
    _23273 = (_23276 != 0);
    else
    _23273 = DBL_PTR(_23276)->dbl != 0.0;
LA: 
    if (_23273 != 0) {
        goto LB; // [283] 329
    }
    _23278 = (_59LeftSym_42605 == _9FALSE_426);
    if (_23278 == 0) {
        _23279 = 0;
        goto LC; // [295] 310
    }
    _23280 = _59TypeIs(_s_43279, 1);
    if (IS_ATOM_INT(_23280))
    _23279 = (_23280 != 0);
    else
    _23279 = DBL_PTR(_23280)->dbl != 0.0;
LC: 
    if (_23279 == 0) {
        DeRef(_23281);
        _23281 = 0;
        goto LD; // [310] 324
    }
    if (IS_ATOM_INT(_v_43280) && IS_ATOM_INT(_38NOVALUE_16800)) {
        _23282 = (_v_43280 != _38NOVALUE_16800);
    }
    else {
        _23282 = binary_op(NOTEQ, _v_43280, _38NOVALUE_16800);
    }
    if (IS_ATOM_INT(_23282))
    _23281 = (_23282 != 0);
    else
    _23281 = DBL_PTR(_23282)->dbl != 0.0;
LD: 
    if (_23281 == 0)
    {
        _23281 = NOVALUE;
        goto LE; // [325] 338
    }
    else{
        _23281 = NOVALUE;
    }
LB: 

    /** 			c_printf("%d", v)*/
    RefDS(_23251);
    Ref(_v_43280);
    _56c_printf(_23251, _v_43280);
    goto L8; // [335] 490
LE: 

    /** 			c_printf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23283 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23283);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23284 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23284 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _23283 = NOVALUE;
    RefDS(_22756);
    Ref(_23284);
    _56c_printf(_22756, _23284);
    _23284 = NOVALUE;

    /** 			c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23285 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23285);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23286 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23286 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _23285 = NOVALUE;
    Ref(_23286);
    _56c_puts(_23286);
    _23286 = NOVALUE;

    /** 			if SymTab[s][S_NREFS] < 2 then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23287 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23287);
    _23288 = (int)*(((s1_ptr)_2)->base + 12);
    _23287 = NOVALUE;
    if (binary_op_a(GREATEREQ, _23288, 2)){
        _23288 = NOVALUE;
        goto L8; // [387] 490
    }
    _23288 = NOVALUE;

    /** 				SymTab[s][S_NREFS] += 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_43279 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    _23292 = (int)*(((s1_ptr)_2)->base + 12);
    _23290 = NOVALUE;
    if (IS_ATOM_INT(_23292)) {
        _23293 = _23292 + 1;
        if (_23293 > MAXINT){
            _23293 = NewDouble((double)_23293);
        }
    }
    else
    _23293 = binary_op(PLUS, 1, _23292);
    _23292 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = _23293;
    if( _1 != _23293 ){
        DeRef(_1);
    }
    _23293 = NOVALUE;
    _23290 = NOVALUE;
    goto L8; // [416] 490
L9: 

    /** 		if LeftSym = FALSE and GType(s) = TYPE_INTEGER and v != NOVALUE then*/
    _23294 = (_59LeftSym_42605 == _9FALSE_426);
    if (_23294 == 0) {
        _23295 = 0;
        goto LF; // [429] 447
    }
    _23296 = _59GType(_s_43279);
    if (IS_ATOM_INT(_23296)) {
        _23297 = (_23296 == 1);
    }
    else {
        _23297 = binary_op(EQUALS, _23296, 1);
    }
    DeRef(_23296);
    _23296 = NOVALUE;
    if (IS_ATOM_INT(_23297))
    _23295 = (_23297 != 0);
    else
    _23295 = DBL_PTR(_23297)->dbl != 0.0;
LF: 
    if (_23295 == 0) {
        goto L10; // [447] 470
    }
    if (IS_ATOM_INT(_v_43280) && IS_ATOM_INT(_38NOVALUE_16800)) {
        _23299 = (_v_43280 != _38NOVALUE_16800);
    }
    else {
        _23299 = binary_op(NOTEQ, _v_43280, _38NOVALUE_16800);
    }
    if (_23299 == 0) {
        DeRef(_23299);
        _23299 = NOVALUE;
        goto L10; // [458] 470
    }
    else {
        if (!IS_ATOM_INT(_23299) && DBL_PTR(_23299)->dbl == 0.0){
            DeRef(_23299);
            _23299 = NOVALUE;
            goto L10; // [458] 470
        }
        DeRef(_23299);
        _23299 = NOVALUE;
    }
    DeRef(_23299);
    _23299 = NOVALUE;

    /** 			c_printf("%d", v)*/
    RefDS(_23251);
    Ref(_v_43280);
    _56c_printf(_23251, _v_43280);
    goto L11; // [467] 489
L10: 

    /** 			c_printf("_%d", SymTab[s][S_TEMP_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23300 = (int)*(((s1_ptr)_2)->base + _s_43279);
    _2 = (int)SEQ_PTR(_23300);
    _23301 = (int)*(((s1_ptr)_2)->base + 34);
    _23300 = NOVALUE;
    RefDS(_22756);
    Ref(_23301);
    _56c_printf(_22756, _23301);
    _23301 = NOVALUE;
L11: 
L8: 

    /** 	LeftSym = FALSE*/
    _59LeftSym_42605 = _9FALSE_426;

    /** end procedure*/
    DeRef(_v_43280);
    DeRef(_23245);
    _23245 = NOVALUE;
    DeRef(_23261);
    _23261 = NOVALUE;
    DeRef(_23248);
    _23248 = NOVALUE;
    DeRef(_23278);
    _23278 = NOVALUE;
    DeRef(_23276);
    _23276 = NOVALUE;
    DeRef(_23280);
    _23280 = NOVALUE;
    DeRef(_23282);
    _23282 = NOVALUE;
    DeRef(_23294);
    _23294 = NOVALUE;
    DeRef(_23297);
    _23297 = NOVALUE;
    return;
    ;
}


void _59c_stmt(int _stmt_43413, int _arg_43414, int _lhs_arg_43416)
{
    int _argcount_43417 = NOVALUE;
    int _i_43418 = NOVALUE;
    int _23356 = NOVALUE;
    int _23355 = NOVALUE;
    int _23354 = NOVALUE;
    int _23353 = NOVALUE;
    int _23352 = NOVALUE;
    int _23351 = NOVALUE;
    int _23350 = NOVALUE;
    int _23349 = NOVALUE;
    int _23348 = NOVALUE;
    int _23347 = NOVALUE;
    int _23346 = NOVALUE;
    int _23345 = NOVALUE;
    int _23344 = NOVALUE;
    int _23343 = NOVALUE;
    int _23342 = NOVALUE;
    int _23341 = NOVALUE;
    int _23340 = NOVALUE;
    int _23338 = NOVALUE;
    int _23336 = NOVALUE;
    int _23334 = NOVALUE;
    int _23333 = NOVALUE;
    int _23332 = NOVALUE;
    int _23331 = NOVALUE;
    int _23329 = NOVALUE;
    int _23328 = NOVALUE;
    int _23327 = NOVALUE;
    int _23326 = NOVALUE;
    int _23325 = NOVALUE;
    int _23324 = NOVALUE;
    int _23323 = NOVALUE;
    int _23322 = NOVALUE;
    int _23321 = NOVALUE;
    int _23320 = NOVALUE;
    int _23319 = NOVALUE;
    int _23318 = NOVALUE;
    int _23317 = NOVALUE;
    int _23316 = NOVALUE;
    int _23313 = NOVALUE;
    int _23312 = NOVALUE;
    int _23311 = NOVALUE;
    int _23310 = NOVALUE;
    int _23309 = NOVALUE;
    int _23308 = NOVALUE;
    int _23306 = NOVALUE;
    int _23304 = NOVALUE;
    int _23303 = NOVALUE;
    int _23302 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_lhs_arg_43416)) {
        _1 = (long)(DBL_PTR(_lhs_arg_43416)->dbl);
        if (UNIQUE(DBL_PTR(_lhs_arg_43416)) && (DBL_PTR(_lhs_arg_43416)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_lhs_arg_43416);
        _lhs_arg_43416 = _1;
    }

    /** 	if LAST_PASS = TRUE and Initializing = FALSE then*/
    _23302 = (_59LAST_PASS_42590 == _9TRUE_428);
    if (_23302 == 0) {
        goto L1; // [15] 47
    }
    _23304 = (_38Initializing_17029 == _9FALSE_426);
    if (_23304 == 0)
    {
        DeRef(_23304);
        _23304 = NOVALUE;
        goto L1; // [28] 47
    }
    else{
        DeRef(_23304);
        _23304 = NOVALUE;
    }

    /** 		cfile_size += 1*/
    _38cfile_size_17028 = _38cfile_size_17028 + 1;

    /** 		update_checksum( stmt )*/
    RefDS(_stmt_43413);
    _57update_checksum(_stmt_43413);
L1: 

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L2; // [51] 60
    }
    else{
    }

    /** 		adjust_indent_before(stmt)*/
    RefDS(_stmt_43413);
    _56adjust_indent_before(_stmt_43413);
L2: 

    /** 	if atom(arg) then*/
    _23306 = IS_ATOM(_arg_43414);
    if (_23306 == 0)
    {
        _23306 = NOVALUE;
        goto L3; // [65] 75
    }
    else{
        _23306 = NOVALUE;
    }

    /** 		arg = {arg}*/
    _0 = _arg_43414;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_arg_43414);
    *((int *)(_2+4)) = _arg_43414;
    _arg_43414 = MAKE_SEQ(_1);
    DeRef(_0);
L3: 

    /** 	argcount = 1*/
    _argcount_43417 = 1;

    /** 	i = 1*/
    _i_43418 = 1;

    /** 	while i <= length(stmt) and length(stmt) > 0 do*/
L4: 
    if (IS_SEQUENCE(_stmt_43413)){
            _23308 = SEQ_PTR(_stmt_43413)->length;
    }
    else {
        _23308 = 1;
    }
    _23309 = (_i_43418 <= _23308);
    _23308 = NOVALUE;
    if (_23309 == 0) {
        goto L5; // [97] 435
    }
    if (IS_SEQUENCE(_stmt_43413)){
            _23311 = SEQ_PTR(_stmt_43413)->length;
    }
    else {
        _23311 = 1;
    }
    _23312 = (_23311 > 0);
    _23311 = NOVALUE;
    if (_23312 == 0)
    {
        DeRef(_23312);
        _23312 = NOVALUE;
        goto L5; // [109] 435
    }
    else{
        DeRef(_23312);
        _23312 = NOVALUE;
    }

    /** 		if stmt[i] = '@' then*/
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23313 = (int)*(((s1_ptr)_2)->base + _i_43418);
    if (binary_op_a(NOTEQ, _23313, 64)){
        _23313 = NOVALUE;
        goto L6; // [118] 288
    }
    _23313 = NOVALUE;

    /** 			if i = 1 then*/
    if (_i_43418 != 1)
    goto L7; // [124] 138

    /** 				LeftSym = TRUE*/
    _59LeftSym_42605 = _9TRUE_428;
L7: 

    /** 			if i < length(stmt) and stmt[i+1] > '0' and stmt[i+1] <= '9' then*/
    if (IS_SEQUENCE(_stmt_43413)){
            _23316 = SEQ_PTR(_stmt_43413)->length;
    }
    else {
        _23316 = 1;
    }
    _23317 = (_i_43418 < _23316);
    _23316 = NOVALUE;
    if (_23317 == 0) {
        _23318 = 0;
        goto L8; // [147] 167
    }
    _23319 = _i_43418 + 1;
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23320 = (int)*(((s1_ptr)_2)->base + _23319);
    if (IS_ATOM_INT(_23320)) {
        _23321 = (_23320 > 48);
    }
    else {
        _23321 = binary_op(GREATER, _23320, 48);
    }
    _23320 = NOVALUE;
    if (IS_ATOM_INT(_23321))
    _23318 = (_23321 != 0);
    else
    _23318 = DBL_PTR(_23321)->dbl != 0.0;
L8: 
    if (_23318 == 0) {
        goto L9; // [167] 249
    }
    _23323 = _i_43418 + 1;
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23324 = (int)*(((s1_ptr)_2)->base + _23323);
    if (IS_ATOM_INT(_23324)) {
        _23325 = (_23324 <= 57);
    }
    else {
        _23325 = binary_op(LESSEQ, _23324, 57);
    }
    _23324 = NOVALUE;
    if (_23325 == 0) {
        DeRef(_23325);
        _23325 = NOVALUE;
        goto L9; // [184] 249
    }
    else {
        if (!IS_ATOM_INT(_23325) && DBL_PTR(_23325)->dbl == 0.0){
            DeRef(_23325);
            _23325 = NOVALUE;
            goto L9; // [184] 249
        }
        DeRef(_23325);
        _23325 = NOVALUE;
    }
    DeRef(_23325);
    _23325 = NOVALUE;

    /** 				if arg[stmt[i+1]-'0'] = lhs_arg then*/
    _23326 = _i_43418 + 1;
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23327 = (int)*(((s1_ptr)_2)->base + _23326);
    if (IS_ATOM_INT(_23327)) {
        _23328 = _23327 - 48;
    }
    else {
        _23328 = binary_op(MINUS, _23327, 48);
    }
    _23327 = NOVALUE;
    _2 = (int)SEQ_PTR(_arg_43414);
    if (!IS_ATOM_INT(_23328)){
        _23329 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23328)->dbl));
    }
    else{
        _23329 = (int)*(((s1_ptr)_2)->base + _23328);
    }
    if (binary_op_a(NOTEQ, _23329, _lhs_arg_43416)){
        _23329 = NOVALUE;
        goto LA; // [205] 219
    }
    _23329 = NOVALUE;

    /** 					LeftSym = TRUE*/
    _59LeftSym_42605 = _9TRUE_428;
LA: 

    /** 				CName(arg[stmt[i+1]-'0'])*/
    _23331 = _i_43418 + 1;
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23332 = (int)*(((s1_ptr)_2)->base + _23331);
    if (IS_ATOM_INT(_23332)) {
        _23333 = _23332 - 48;
    }
    else {
        _23333 = binary_op(MINUS, _23332, 48);
    }
    _23332 = NOVALUE;
    _2 = (int)SEQ_PTR(_arg_43414);
    if (!IS_ATOM_INT(_23333)){
        _23334 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23333)->dbl));
    }
    else{
        _23334 = (int)*(((s1_ptr)_2)->base + _23333);
    }
    Ref(_23334);
    _59CName(_23334);
    _23334 = NOVALUE;

    /** 				i += 1*/
    _i_43418 = _i_43418 + 1;
    goto LB; // [246] 279
L9: 

    /** 				if arg[argcount] = lhs_arg then*/
    _2 = (int)SEQ_PTR(_arg_43414);
    _23336 = (int)*(((s1_ptr)_2)->base + _argcount_43417);
    if (binary_op_a(NOTEQ, _23336, _lhs_arg_43416)){
        _23336 = NOVALUE;
        goto LC; // [255] 269
    }
    _23336 = NOVALUE;

    /** 					LeftSym = TRUE*/
    _59LeftSym_42605 = _9TRUE_428;
LC: 

    /** 				CName(arg[argcount])*/
    _2 = (int)SEQ_PTR(_arg_43414);
    _23338 = (int)*(((s1_ptr)_2)->base + _argcount_43417);
    Ref(_23338);
    _59CName(_23338);
    _23338 = NOVALUE;
LB: 

    /** 			argcount += 1*/
    _argcount_43417 = _argcount_43417 + 1;
    goto LD; // [285] 353
L6: 

    /** 			c_putc(stmt[i])*/
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23340 = (int)*(((s1_ptr)_2)->base + _i_43418);
    Ref(_23340);
    _56c_putc(_23340);
    _23340 = NOVALUE;

    /** 			if stmt[i] = '&' and i < length(stmt) and stmt[i+1] = '@' then*/
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23341 = (int)*(((s1_ptr)_2)->base + _i_43418);
    if (IS_ATOM_INT(_23341)) {
        _23342 = (_23341 == 38);
    }
    else {
        _23342 = binary_op(EQUALS, _23341, 38);
    }
    _23341 = NOVALUE;
    if (IS_ATOM_INT(_23342)) {
        if (_23342 == 0) {
            DeRef(_23343);
            _23343 = 0;
            goto LE; // [307] 322
        }
    }
    else {
        if (DBL_PTR(_23342)->dbl == 0.0) {
            DeRef(_23343);
            _23343 = 0;
            goto LE; // [307] 322
        }
    }
    if (IS_SEQUENCE(_stmt_43413)){
            _23344 = SEQ_PTR(_stmt_43413)->length;
    }
    else {
        _23344 = 1;
    }
    _23345 = (_i_43418 < _23344);
    _23344 = NOVALUE;
    DeRef(_23343);
    _23343 = (_23345 != 0);
LE: 
    if (_23343 == 0) {
        goto LF; // [322] 352
    }
    _23347 = _i_43418 + 1;
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23348 = (int)*(((s1_ptr)_2)->base + _23347);
    if (IS_ATOM_INT(_23348)) {
        _23349 = (_23348 == 64);
    }
    else {
        _23349 = binary_op(EQUALS, _23348, 64);
    }
    _23348 = NOVALUE;
    if (_23349 == 0) {
        DeRef(_23349);
        _23349 = NOVALUE;
        goto LF; // [339] 352
    }
    else {
        if (!IS_ATOM_INT(_23349) && DBL_PTR(_23349)->dbl == 0.0){
            DeRef(_23349);
            _23349 = NOVALUE;
            goto LF; // [339] 352
        }
        DeRef(_23349);
        _23349 = NOVALUE;
    }
    DeRef(_23349);
    _23349 = NOVALUE;

    /** 				LeftSym = TRUE -- never say: x = x &y or andy - always leave space*/
    _59LeftSym_42605 = _9TRUE_428;
LF: 
LD: 

    /** 		if stmt[i] = '\n' and i < length(stmt) then*/
    _2 = (int)SEQ_PTR(_stmt_43413);
    _23350 = (int)*(((s1_ptr)_2)->base + _i_43418);
    if (IS_ATOM_INT(_23350)) {
        _23351 = (_23350 == 10);
    }
    else {
        _23351 = binary_op(EQUALS, _23350, 10);
    }
    _23350 = NOVALUE;
    if (IS_ATOM_INT(_23351)) {
        if (_23351 == 0) {
            goto L10; // [363] 424
        }
    }
    else {
        if (DBL_PTR(_23351)->dbl == 0.0) {
            goto L10; // [363] 424
        }
    }
    if (IS_SEQUENCE(_stmt_43413)){
            _23353 = SEQ_PTR(_stmt_43413)->length;
    }
    else {
        _23353 = 1;
    }
    _23354 = (_i_43418 < _23353);
    _23353 = NOVALUE;
    if (_23354 == 0)
    {
        DeRef(_23354);
        _23354 = NOVALUE;
        goto L10; // [375] 424
    }
    else{
        DeRef(_23354);
        _23354 = NOVALUE;
    }

    /** 			if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L11; // [382] 391
    }
    else{
    }

    /** 				adjust_indent_after(stmt)*/
    RefDS(_stmt_43413);
    _56adjust_indent_after(_stmt_43413);
L11: 

    /** 			stmt = stmt[i+1..$]*/
    _23355 = _i_43418 + 1;
    if (_23355 > MAXINT){
        _23355 = NewDouble((double)_23355);
    }
    if (IS_SEQUENCE(_stmt_43413)){
            _23356 = SEQ_PTR(_stmt_43413)->length;
    }
    else {
        _23356 = 1;
    }
    rhs_slice_target = (object_ptr)&_stmt_43413;
    RHS_Slice(_stmt_43413, _23355, _23356);

    /** 			i = 0*/
    _i_43418 = 0;

    /** 			if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L12; // [414] 423
    }
    else{
    }

    /** 				adjust_indent_before(stmt)*/
    RefDS(_stmt_43413);
    _56adjust_indent_before(_stmt_43413);
L12: 
L10: 

    /** 		i += 1*/
    _i_43418 = _i_43418 + 1;

    /** 	end while*/
    goto L4; // [432] 90
L5: 

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L13; // [439] 448
    }
    else{
    }

    /** 		adjust_indent_after(stmt)*/
    RefDS(_stmt_43413);
    _56adjust_indent_after(_stmt_43413);
L13: 

    /** end procedure*/
    DeRefDS(_stmt_43413);
    DeRef(_arg_43414);
    DeRef(_23302);
    _23302 = NOVALUE;
    DeRef(_23309);
    _23309 = NOVALUE;
    DeRef(_23317);
    _23317 = NOVALUE;
    DeRef(_23319);
    _23319 = NOVALUE;
    DeRef(_23323);
    _23323 = NOVALUE;
    DeRef(_23321);
    _23321 = NOVALUE;
    DeRef(_23326);
    _23326 = NOVALUE;
    DeRef(_23331);
    _23331 = NOVALUE;
    DeRef(_23328);
    _23328 = NOVALUE;
    DeRef(_23345);
    _23345 = NOVALUE;
    DeRef(_23333);
    _23333 = NOVALUE;
    DeRef(_23342);
    _23342 = NOVALUE;
    DeRef(_23347);
    _23347 = NOVALUE;
    DeRef(_23355);
    _23355 = NOVALUE;
    DeRef(_23351);
    _23351 = NOVALUE;
    return;
    ;
}


void _59c_stmt0(int _stmt_43512)
{
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L1; // [7] 18
    }
    else{
    }

    /** 		c_stmt(stmt, {})*/
    RefDS(_stmt_43512);
    RefDS(_22663);
    _59c_stmt(_stmt_43512, _22663, 0);
L1: 

    /** end procedure*/
    DeRefDS(_stmt_43512);
    return;
    ;
}


void _59DeclareFileVars()
{
    int _s_43518 = NOVALUE;
    int _eentry_43520 = NOVALUE;
    int _23398 = NOVALUE;
    int _23397 = NOVALUE;
    int _23396 = NOVALUE;
    int _23393 = NOVALUE;
    int _23391 = NOVALUE;
    int _23390 = NOVALUE;
    int _23389 = NOVALUE;
    int _23388 = NOVALUE;
    int _23384 = NOVALUE;
    int _23383 = NOVALUE;
    int _23382 = NOVALUE;
    int _23381 = NOVALUE;
    int _23380 = NOVALUE;
    int _23379 = NOVALUE;
    int _23378 = NOVALUE;
    int _23377 = NOVALUE;
    int _23376 = NOVALUE;
    int _23375 = NOVALUE;
    int _23374 = NOVALUE;
    int _23373 = NOVALUE;
    int _23372 = NOVALUE;
    int _23371 = NOVALUE;
    int _23370 = NOVALUE;
    int _23369 = NOVALUE;
    int _23368 = NOVALUE;
    int _23367 = NOVALUE;
    int _23366 = NOVALUE;
    int _23365 = NOVALUE;
    int _23364 = NOVALUE;
    int _23363 = NOVALUE;
    int _23360 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c_puts("// Declaring file vars\n")*/
    RefDS(_23359);
    _56c_puts(_23359);

    /** 	s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23360 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
    _2 = (int)SEQ_PTR(_23360);
    _s_43518 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43518)){
        _s_43518 = (long)DBL_PTR(_s_43518)->dbl;
    }
    _23360 = NOVALUE;

    /** 	while s do*/
L1: 
    if (_s_43518 == 0)
    {
        goto L2; // [29] 321
    }
    else{
    }

    /** 		eentry = SymTab[s]*/
    DeRef(_eentry_43520);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _eentry_43520 = (int)*(((s1_ptr)_2)->base + _s_43518);
    Ref(_eentry_43520);

    /** 		if eentry[S_SCOPE] >= SC_LOCAL*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23363 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23363)) {
        _23364 = (_23363 >= 5);
    }
    else {
        _23364 = binary_op(GREATEREQ, _23363, 5);
    }
    _23363 = NOVALUE;
    if (IS_ATOM_INT(_23364)) {
        if (_23364 == 0) {
            DeRef(_23365);
            _23365 = 0;
            goto L3; // [56] 116
        }
    }
    else {
        if (DBL_PTR(_23364)->dbl == 0.0) {
            DeRef(_23365);
            _23365 = 0;
            goto L3; // [56] 116
        }
    }
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23366 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23366)) {
        _23367 = (_23366 <= 6);
    }
    else {
        _23367 = binary_op(LESSEQ, _23366, 6);
    }
    _23366 = NOVALUE;
    if (IS_ATOM_INT(_23367)) {
        if (_23367 != 0) {
            DeRef(_23368);
            _23368 = 1;
            goto L4; // [72] 92
        }
    }
    else {
        if (DBL_PTR(_23367)->dbl != 0.0) {
            DeRef(_23368);
            _23368 = 1;
            goto L4; // [72] 92
        }
    }
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23369 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23369)) {
        _23370 = (_23369 == 11);
    }
    else {
        _23370 = binary_op(EQUALS, _23369, 11);
    }
    _23369 = NOVALUE;
    DeRef(_23368);
    if (IS_ATOM_INT(_23370))
    _23368 = (_23370 != 0);
    else
    _23368 = DBL_PTR(_23370)->dbl != 0.0;
L4: 
    if (_23368 != 0) {
        _23371 = 1;
        goto L5; // [92] 112
    }
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23372 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_23372)) {
        _23373 = (_23372 == 13);
    }
    else {
        _23373 = binary_op(EQUALS, _23372, 13);
    }
    _23372 = NOVALUE;
    if (IS_ATOM_INT(_23373))
    _23371 = (_23373 != 0);
    else
    _23371 = DBL_PTR(_23373)->dbl != 0.0;
L5: 
    DeRef(_23365);
    _23365 = (_23371 != 0);
L3: 
    if (_23365 == 0) {
        _23374 = 0;
        goto L6; // [116] 136
    }
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23375 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_23375)) {
        _23376 = (_23375 != 0);
    }
    else {
        _23376 = binary_op(NOTEQ, _23375, 0);
    }
    _23375 = NOVALUE;
    if (IS_ATOM_INT(_23376))
    _23374 = (_23376 != 0);
    else
    _23374 = DBL_PTR(_23376)->dbl != 0.0;
L6: 
    if (_23374 == 0) {
        _23377 = 0;
        goto L7; // [136] 156
    }
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23378 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_23378)) {
        _23379 = (_23378 != 99);
    }
    else {
        _23379 = binary_op(NOTEQ, _23378, 99);
    }
    _23378 = NOVALUE;
    if (IS_ATOM_INT(_23379))
    _23377 = (_23379 != 0);
    else
    _23377 = DBL_PTR(_23379)->dbl != 0.0;
L7: 
    if (_23377 == 0) {
        goto L8; // [156] 300
    }
    _2 = (int)SEQ_PTR(_eentry_43520);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23381 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23381 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _23382 = find_from(_23381, _39RTN_TOKS_16547, 1);
    _23381 = NOVALUE;
    _23383 = (_23382 == 0);
    _23382 = NOVALUE;
    if (_23383 == 0)
    {
        DeRef(_23383);
        _23383 = NOVALUE;
        goto L8; // [177] 300
    }
    else{
        DeRef(_23383);
        _23383 = NOVALUE;
    }

    /** 			if eentry[S_TOKEN] = PROC then*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23384 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23384 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    if (binary_op_a(NOTEQ, _23384, 27)){
        _23384 = NOVALUE;
        goto L9; // [190] 202
    }
    _23384 = NOVALUE;

    /** 				c_puts( "void ")*/
    RefDS(_23386);
    _56c_puts(_23386);
    goto LA; // [199] 208
L9: 

    /** 				c_puts("int ")*/
    RefDS(_23387);
    _56c_puts(_23387);
LA: 

    /** 			c_printf("_%d", eentry[S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23388 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23388 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    RefDS(_22756);
    Ref(_23388);
    _56c_printf(_22756, _23388);
    _23388 = NOVALUE;

    /** 			c_puts(eentry[S_NAME])*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23389 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23389 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_23389);
    _56c_puts(_23389);
    _23389 = NOVALUE;

    /** 			if integer( eentry[S_OBJ] ) then*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23390 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_23390))
    _23391 = 1;
    else if (IS_ATOM_DBL(_23390))
    _23391 = IS_ATOM_INT(DoubleToInt(_23390));
    else
    _23391 = 0;
    _23390 = NOVALUE;
    if (_23391 == 0)
    {
        _23391 = NOVALUE;
        goto LB; // [242] 260
    }
    else{
        _23391 = NOVALUE;
    }

    /** 					c_printf(" = %d;\n", eentry[S_OBJ] )*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    _23393 = (int)*(((s1_ptr)_2)->base + 1);
    RefDS(_23392);
    Ref(_23393);
    _56c_printf(_23392, _23393);
    _23393 = NOVALUE;
    goto LC; // [257] 266
LB: 

    /** 				c_puts(" = NOVALUE;\n")*/
    RefDS(_23394);
    _56c_puts(_23394);
LC: 

    /** 			c_hputs("extern int ")*/
    RefDS(_23395);
    _56c_hputs(_23395);

    /** 			c_hprintf("_%d", eentry[S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23396 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23396 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    RefDS(_22756);
    Ref(_23396);
    _56c_hprintf(_22756, _23396);
    _23396 = NOVALUE;

    /** 			c_hputs(eentry[S_NAME])*/
    _2 = (int)SEQ_PTR(_eentry_43520);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23397 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23397 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    Ref(_23397);
    _56c_hputs(_23397);
    _23397 = NOVALUE;

    /** 			c_hputs(";\n")*/
    RefDS(_22893);
    _56c_hputs(_22893);
L8: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23398 = (int)*(((s1_ptr)_2)->base + _s_43518);
    _2 = (int)SEQ_PTR(_23398);
    _s_43518 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43518)){
        _s_43518 = (long)DBL_PTR(_s_43518)->dbl;
    }
    _23398 = NOVALUE;

    /** 	end while*/
    goto L1; // [318] 29
L2: 

    /** 	c_puts("\n")*/
    RefDS(_22815);
    _56c_puts(_22815);

    /** 	c_hputs("\n")*/
    RefDS(_22815);
    _56c_hputs(_22815);

    /** end procedure*/
    DeRef(_eentry_43520);
    DeRef(_23364);
    _23364 = NOVALUE;
    DeRef(_23367);
    _23367 = NOVALUE;
    DeRef(_23370);
    _23370 = NOVALUE;
    DeRef(_23373);
    _23373 = NOVALUE;
    DeRef(_23376);
    _23376 = NOVALUE;
    DeRef(_23379);
    _23379 = NOVALUE;
    return;
    ;
}


int _59PromoteTypeInfo()
{
    int _updsym_43612 = NOVALUE;
    int _s_43614 = NOVALUE;
    int _sym_43615 = NOVALUE;
    int _symo_43616 = NOVALUE;
    int _upd_43845 = NOVALUE;
    int _23498 = NOVALUE;
    int _23496 = NOVALUE;
    int _23495 = NOVALUE;
    int _23494 = NOVALUE;
    int _23493 = NOVALUE;
    int _23491 = NOVALUE;
    int _23489 = NOVALUE;
    int _23488 = NOVALUE;
    int _23487 = NOVALUE;
    int _23486 = NOVALUE;
    int _23485 = NOVALUE;
    int _23481 = NOVALUE;
    int _23478 = NOVALUE;
    int _23477 = NOVALUE;
    int _23476 = NOVALUE;
    int _23475 = NOVALUE;
    int _23474 = NOVALUE;
    int _23473 = NOVALUE;
    int _23472 = NOVALUE;
    int _23471 = NOVALUE;
    int _23470 = NOVALUE;
    int _23469 = NOVALUE;
    int _23468 = NOVALUE;
    int _23466 = NOVALUE;
    int _23465 = NOVALUE;
    int _23464 = NOVALUE;
    int _23463 = NOVALUE;
    int _23462 = NOVALUE;
    int _23461 = NOVALUE;
    int _23460 = NOVALUE;
    int _23459 = NOVALUE;
    int _23458 = NOVALUE;
    int _23457 = NOVALUE;
    int _23455 = NOVALUE;
    int _23454 = NOVALUE;
    int _23453 = NOVALUE;
    int _23451 = NOVALUE;
    int _23450 = NOVALUE;
    int _23449 = NOVALUE;
    int _23447 = NOVALUE;
    int _23446 = NOVALUE;
    int _23445 = NOVALUE;
    int _23444 = NOVALUE;
    int _23443 = NOVALUE;
    int _23442 = NOVALUE;
    int _23441 = NOVALUE;
    int _23439 = NOVALUE;
    int _23438 = NOVALUE;
    int _23437 = NOVALUE;
    int _23436 = NOVALUE;
    int _23434 = NOVALUE;
    int _23433 = NOVALUE;
    int _23431 = NOVALUE;
    int _23430 = NOVALUE;
    int _23429 = NOVALUE;
    int _23428 = NOVALUE;
    int _23427 = NOVALUE;
    int _23426 = NOVALUE;
    int _23425 = NOVALUE;
    int _23423 = NOVALUE;
    int _23422 = NOVALUE;
    int _23421 = NOVALUE;
    int _23420 = NOVALUE;
    int _23419 = NOVALUE;
    int _23418 = NOVALUE;
    int _23417 = NOVALUE;
    int _23416 = NOVALUE;
    int _23415 = NOVALUE;
    int _23414 = NOVALUE;
    int _23413 = NOVALUE;
    int _23412 = NOVALUE;
    int _23411 = NOVALUE;
    int _23410 = NOVALUE;
    int _23408 = NOVALUE;
    int _23407 = NOVALUE;
    int _23406 = NOVALUE;
    int _23404 = NOVALUE;
    int _23403 = NOVALUE;
    int _23400 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence sym, symo*/

    /** 	updsym = 0*/
    _updsym_43612 = 0;

    /** 	g_has_delete = p_has_delete*/
    _59g_has_delete_42798 = _59p_has_delete_42799;

    /** 	s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23400 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
    _2 = (int)SEQ_PTR(_23400);
    _s_43614 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43614)){
        _s_43614 = (long)DBL_PTR(_s_43614)->dbl;
    }
    _23400 = NOVALUE;

    /** 	while s do*/
L1: 
    if (_s_43614 == 0)
    {
        goto L2; // [38] 921
    }
    else{
    }

    /** 		sym = SymTab[s]*/
    DeRef(_sym_43615);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _sym_43615 = (int)*(((s1_ptr)_2)->base + _s_43614);
    Ref(_sym_43615);

    /** 		symo = sym*/
    RefDS(_sym_43615);
    DeRef(_symo_43616);
    _symo_43616 = _sym_43615;

    /** 		if sym[S_TOKEN] = FUNC or sym[S_TOKEN] = TYPE then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23403 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23403 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    if (IS_ATOM_INT(_23403)) {
        _23404 = (_23403 == 501);
    }
    else {
        _23404 = binary_op(EQUALS, _23403, 501);
    }
    _23403 = NOVALUE;
    if (IS_ATOM_INT(_23404)) {
        if (_23404 != 0) {
            goto L3; // [72] 93
        }
    }
    else {
        if (DBL_PTR(_23404)->dbl != 0.0) {
            goto L3; // [72] 93
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23406 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23406 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    if (IS_ATOM_INT(_23406)) {
        _23407 = (_23406 == 504);
    }
    else {
        _23407 = binary_op(EQUALS, _23406, 504);
    }
    _23406 = NOVALUE;
    if (_23407 == 0) {
        DeRef(_23407);
        _23407 = NOVALUE;
        goto L4; // [89] 138
    }
    else {
        if (!IS_ATOM_INT(_23407) && DBL_PTR(_23407)->dbl == 0.0){
            DeRef(_23407);
            _23407 = NOVALUE;
            goto L4; // [89] 138
        }
        DeRef(_23407);
        _23407 = NOVALUE;
    }
    DeRef(_23407);
    _23407 = NOVALUE;
L3: 

    /** 			if sym[S_GTYPE_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23408 = (int)*(((s1_ptr)_2)->base + 38);
    if (binary_op_a(NOTEQ, _23408, 0)){
        _23408 = NOVALUE;
        goto L5; // [103] 120
    }
    _23408 = NOVALUE;

    /** 				sym[S_GTYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto L6; // [117] 549
L5: 

    /** 				sym[S_GTYPE] = sym[S_GTYPE_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23410 = (int)*(((s1_ptr)_2)->base + 38);
    Ref(_23410);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _23410;
    if( _1 != _23410 ){
        DeRef(_1);
    }
    _23410 = NOVALUE;
    goto L6; // [135] 549
L4: 

    /** 			if sym[S_GTYPE] != TYPE_INTEGER and*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23411 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23411)) {
        _23412 = (_23411 != 1);
    }
    else {
        _23412 = binary_op(NOTEQ, _23411, 1);
    }
    _23411 = NOVALUE;
    if (IS_ATOM_INT(_23412)) {
        if (_23412 == 0) {
            DeRef(_23413);
            _23413 = 0;
            goto L7; // [152] 172
        }
    }
    else {
        if (DBL_PTR(_23412)->dbl == 0.0) {
            DeRef(_23413);
            _23413 = 0;
            goto L7; // [152] 172
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23414 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23414)) {
        _23415 = (_23414 != 16);
    }
    else {
        _23415 = binary_op(NOTEQ, _23414, 16);
    }
    _23414 = NOVALUE;
    DeRef(_23413);
    if (IS_ATOM_INT(_23415))
    _23413 = (_23415 != 0);
    else
    _23413 = DBL_PTR(_23415)->dbl != 0.0;
L7: 
    if (_23413 == 0) {
        goto L8; // [172] 283
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23417 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23417)) {
        _23418 = (_23417 != 0);
    }
    else {
        _23418 = binary_op(NOTEQ, _23417, 0);
    }
    _23417 = NOVALUE;
    if (_23418 == 0) {
        DeRef(_23418);
        _23418 = NOVALUE;
        goto L8; // [189] 283
    }
    else {
        if (!IS_ATOM_INT(_23418) && DBL_PTR(_23418)->dbl == 0.0){
            DeRef(_23418);
            _23418 = NOVALUE;
            goto L8; // [189] 283
        }
        DeRef(_23418);
        _23418 = NOVALUE;
    }
    DeRef(_23418);
    _23418 = NOVALUE;

    /** 				if sym[S_GTYPE_NEW] = TYPE_INTEGER or*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23419 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23419)) {
        _23420 = (_23419 == 1);
    }
    else {
        _23420 = binary_op(EQUALS, _23419, 1);
    }
    _23419 = NOVALUE;
    if (IS_ATOM_INT(_23420)) {
        if (_23420 != 0) {
            DeRef(_23421);
            _23421 = 1;
            goto L9; // [206] 226
        }
    }
    else {
        if (DBL_PTR(_23420)->dbl != 0.0) {
            DeRef(_23421);
            _23421 = 1;
            goto L9; // [206] 226
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23422 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23422)) {
        _23423 = (_23422 == 16);
    }
    else {
        _23423 = binary_op(EQUALS, _23422, 16);
    }
    _23422 = NOVALUE;
    DeRef(_23421);
    if (IS_ATOM_INT(_23423))
    _23421 = (_23423 != 0);
    else
    _23421 = DBL_PTR(_23423)->dbl != 0.0;
L9: 
    if (_23421 != 0) {
        goto LA; // [226] 267
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23425 = (int)*(((s1_ptr)_2)->base + 36);
    if (IS_ATOM_INT(_23425)) {
        _23426 = (_23425 == 4);
    }
    else {
        _23426 = binary_op(EQUALS, _23425, 4);
    }
    _23425 = NOVALUE;
    if (IS_ATOM_INT(_23426)) {
        if (_23426 == 0) {
            DeRef(_23427);
            _23427 = 0;
            goto LB; // [242] 262
        }
    }
    else {
        if (DBL_PTR(_23426)->dbl == 0.0) {
            DeRef(_23427);
            _23427 = 0;
            goto LB; // [242] 262
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23428 = (int)*(((s1_ptr)_2)->base + 38);
    if (IS_ATOM_INT(_23428)) {
        _23429 = (_23428 == 2);
    }
    else {
        _23429 = binary_op(EQUALS, _23428, 2);
    }
    _23428 = NOVALUE;
    DeRef(_23427);
    if (IS_ATOM_INT(_23429))
    _23427 = (_23429 != 0);
    else
    _23427 = DBL_PTR(_23429)->dbl != 0.0;
LB: 
    if (_23427 == 0)
    {
        _23427 = NOVALUE;
        goto LC; // [263] 282
    }
    else{
        _23427 = NOVALUE;
    }
LA: 

    /** 						sym[S_GTYPE] = sym[S_GTYPE_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23430 = (int)*(((s1_ptr)_2)->base + 38);
    Ref(_23430);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 36);
    _1 = *(int *)_2;
    *(int *)_2 = _23430;
    if( _1 != _23430 ){
        DeRef(_1);
    }
    _23430 = NOVALUE;
LC: 
L8: 

    /** 			if sym[S_ARG_TYPE_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23431 = (int)*(((s1_ptr)_2)->base + 44);
    if (binary_op_a(NOTEQ, _23431, 0)){
        _23431 = NOVALUE;
        goto LD; // [293] 310
    }
    _23431 = NOVALUE;

    /** 				sym[S_ARG_TYPE] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 43);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto LE; // [307] 325
LD: 

    /** 				sym[S_ARG_TYPE] = sym[S_ARG_TYPE_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23433 = (int)*(((s1_ptr)_2)->base + 44);
    Ref(_23433);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 43);
    _1 = *(int *)_2;
    *(int *)_2 = _23433;
    if( _1 != _23433 ){
        DeRef(_1);
    }
    _23433 = NOVALUE;
LE: 

    /** 			sym[S_ARG_TYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 44);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			if sym[S_ARG_SEQ_ELEM_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23434 = (int)*(((s1_ptr)_2)->base + 46);
    if (binary_op_a(NOTEQ, _23434, 0)){
        _23434 = NOVALUE;
        goto LF; // [345] 362
    }
    _23434 = NOVALUE;

    /** 				sym[S_ARG_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 45);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto L10; // [359] 377
LF: 

    /** 				sym[S_ARG_SEQ_ELEM] = sym[S_ARG_SEQ_ELEM_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23436 = (int)*(((s1_ptr)_2)->base + 46);
    Ref(_23436);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 45);
    _1 = *(int *)_2;
    *(int *)_2 = _23436;
    if( _1 != _23436 ){
        DeRef(_1);
    }
    _23436 = NOVALUE;
L10: 

    /** 			sym[S_ARG_SEQ_ELEM_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 46);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 			if sym[S_ARG_MIN_NEW] = -NOVALUE or*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23437 = (int)*(((s1_ptr)_2)->base + 49);
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23438 = (int)NewDouble((double)-0xC0000000);
        else
        _23438 = - _38NOVALUE_16800;
    }
    else {
        _23438 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    if (IS_ATOM_INT(_23437) && IS_ATOM_INT(_23438)) {
        _23439 = (_23437 == _23438);
    }
    else {
        _23439 = binary_op(EQUALS, _23437, _23438);
    }
    _23437 = NOVALUE;
    DeRef(_23438);
    _23438 = NOVALUE;
    if (IS_ATOM_INT(_23439)) {
        if (_23439 != 0) {
            goto L11; // [404] 425
        }
    }
    else {
        if (DBL_PTR(_23439)->dbl != 0.0) {
            goto L11; // [404] 425
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23441 = (int)*(((s1_ptr)_2)->base + 49);
    if (IS_ATOM_INT(_23441) && IS_ATOM_INT(_38NOVALUE_16800)) {
        _23442 = (_23441 == _38NOVALUE_16800);
    }
    else {
        _23442 = binary_op(EQUALS, _23441, _38NOVALUE_16800);
    }
    _23441 = NOVALUE;
    if (_23442 == 0) {
        DeRef(_23442);
        _23442 = NOVALUE;
        goto L12; // [421] 448
    }
    else {
        if (!IS_ATOM_INT(_23442) && DBL_PTR(_23442)->dbl == 0.0){
            DeRef(_23442);
            _23442 = NOVALUE;
            goto L12; // [421] 448
        }
        DeRef(_23442);
        _23442 = NOVALUE;
    }
    DeRef(_23442);
    _23442 = NOVALUE;
L11: 

    /** 				sym[S_ARG_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 47);
    _1 = *(int *)_2;
    *(int *)_2 = -1073741824;
    DeRef(_1);

    /** 				sym[S_ARG_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 48);
    _1 = *(int *)_2;
    *(int *)_2 = 1073741823;
    DeRef(_1);
    goto L13; // [445] 477
L12: 

    /** 				sym[S_ARG_MIN] = sym[S_ARG_MIN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23443 = (int)*(((s1_ptr)_2)->base + 49);
    Ref(_23443);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 47);
    _1 = *(int *)_2;
    *(int *)_2 = _23443;
    if( _1 != _23443 ){
        DeRef(_1);
    }
    _23443 = NOVALUE;

    /** 				sym[S_ARG_MAX] = sym[S_ARG_MAX_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23444 = (int)*(((s1_ptr)_2)->base + 50);
    Ref(_23444);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 48);
    _1 = *(int *)_2;
    *(int *)_2 = _23444;
    if( _1 != _23444 ){
        DeRef(_1);
    }
    _23444 = NOVALUE;
L13: 

    /** 			sym[S_ARG_MIN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23445 = (int)NewDouble((double)-0xC0000000);
        else
        _23445 = - _38NOVALUE_16800;
    }
    else {
        _23445 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 49);
    _1 = *(int *)_2;
    *(int *)_2 = _23445;
    if( _1 != _23445 ){
        DeRef(_1);
    }
    _23445 = NOVALUE;

    /** 			if sym[S_ARG_SEQ_LEN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23446 = (int)*(((s1_ptr)_2)->base + 52);
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23447 = (int)NewDouble((double)-0xC0000000);
        else
        _23447 = - _38NOVALUE_16800;
    }
    else {
        _23447 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    if (binary_op_a(NOTEQ, _23446, _23447)){
        _23446 = NOVALUE;
        DeRef(_23447);
        _23447 = NOVALUE;
        goto L14; // [503] 520
    }
    _23446 = NOVALUE;
    DeRef(_23447);
    _23447 = NOVALUE;

    /** 				sym[S_ARG_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 51);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L15; // [517] 535
L14: 

    /** 				sym[S_ARG_SEQ_LEN] = sym[S_ARG_SEQ_LEN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23449 = (int)*(((s1_ptr)_2)->base + 52);
    Ref(_23449);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 51);
    _1 = *(int *)_2;
    *(int *)_2 = _23449;
    if( _1 != _23449 ){
        DeRef(_1);
    }
    _23449 = NOVALUE;
L15: 

    /** 			sym[S_ARG_SEQ_LEN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23450 = (int)NewDouble((double)-0xC0000000);
        else
        _23450 = - _38NOVALUE_16800;
    }
    else {
        _23450 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 52);
    _1 = *(int *)_2;
    *(int *)_2 = _23450;
    if( _1 != _23450 ){
        DeRef(_1);
    }
    _23450 = NOVALUE;
L6: 

    /** 		sym[S_GTYPE_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 38);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		if sym[S_SEQ_ELEM_NEW] = TYPE_NULL then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23451 = (int)*(((s1_ptr)_2)->base + 40);
    if (binary_op_a(NOTEQ, _23451, 0)){
        _23451 = NOVALUE;
        goto L16; // [569] 586
    }
    _23451 = NOVALUE;

    /** 		   sym[S_SEQ_ELEM] = TYPE_OBJECT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = 16;
    DeRef(_1);
    goto L17; // [583] 601
L16: 

    /** 			sym[S_SEQ_ELEM] = sym[S_SEQ_ELEM_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23453 = (int)*(((s1_ptr)_2)->base + 40);
    Ref(_23453);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 33);
    _1 = *(int *)_2;
    *(int *)_2 = _23453;
    if( _1 != _23453 ){
        DeRef(_1);
    }
    _23453 = NOVALUE;
L17: 

    /** 		sym[S_SEQ_ELEM_NEW] = TYPE_NULL*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 40);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		if sym[S_SEQ_LEN_NEW] = -NOVALUE then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23454 = (int)*(((s1_ptr)_2)->base + 39);
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23455 = (int)NewDouble((double)-0xC0000000);
        else
        _23455 = - _38NOVALUE_16800;
    }
    else {
        _23455 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    if (binary_op_a(NOTEQ, _23454, _23455)){
        _23454 = NOVALUE;
        DeRef(_23455);
        _23455 = NOVALUE;
        goto L18; // [624] 641
    }
    _23454 = NOVALUE;
    DeRef(_23455);
    _23455 = NOVALUE;

    /** 			sym[S_SEQ_LEN] = NOVALUE*/
    Ref(_38NOVALUE_16800);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _38NOVALUE_16800;
    DeRef(_1);
    goto L19; // [638] 656
L18: 

    /** 			sym[S_SEQ_LEN] = sym[S_SEQ_LEN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23457 = (int)*(((s1_ptr)_2)->base + 39);
    Ref(_23457);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 32);
    _1 = *(int *)_2;
    *(int *)_2 = _23457;
    if( _1 != _23457 ){
        DeRef(_1);
    }
    _23457 = NOVALUE;
L19: 

    /** 		sym[S_SEQ_LEN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23458 = (int)NewDouble((double)-0xC0000000);
        else
        _23458 = - _38NOVALUE_16800;
    }
    else {
        _23458 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 39);
    _1 = *(int *)_2;
    *(int *)_2 = _23458;
    if( _1 != _23458 ){
        DeRef(_1);
    }
    _23458 = NOVALUE;

    /** 		if sym[S_TOKEN] != NAMESPACE*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23459 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23459 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    if (IS_ATOM_INT(_23459)) {
        _23460 = (_23459 != 523);
    }
    else {
        _23460 = binary_op(NOTEQ, _23459, 523);
    }
    _23459 = NOVALUE;
    if (IS_ATOM_INT(_23460)) {
        if (_23460 == 0) {
            goto L1A; // [683] 794
        }
    }
    else {
        if (DBL_PTR(_23460)->dbl == 0.0) {
            goto L1A; // [683] 794
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23462 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_23462)) {
        _23463 = (_23462 != 2);
    }
    else {
        _23463 = binary_op(NOTEQ, _23462, 2);
    }
    _23462 = NOVALUE;
    if (_23463 == 0) {
        DeRef(_23463);
        _23463 = NOVALUE;
        goto L1A; // [700] 794
    }
    else {
        if (!IS_ATOM_INT(_23463) && DBL_PTR(_23463)->dbl == 0.0){
            DeRef(_23463);
            _23463 = NOVALUE;
            goto L1A; // [700] 794
        }
        DeRef(_23463);
        _23463 = NOVALUE;
    }
    DeRef(_23463);
    _23463 = NOVALUE;

    /** 			if sym[S_OBJ_MIN_NEW] = -NOVALUE or*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23464 = (int)*(((s1_ptr)_2)->base + 41);
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23465 = (int)NewDouble((double)-0xC0000000);
        else
        _23465 = - _38NOVALUE_16800;
    }
    else {
        _23465 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    if (IS_ATOM_INT(_23464) && IS_ATOM_INT(_23465)) {
        _23466 = (_23464 == _23465);
    }
    else {
        _23466 = binary_op(EQUALS, _23464, _23465);
    }
    _23464 = NOVALUE;
    DeRef(_23465);
    _23465 = NOVALUE;
    if (IS_ATOM_INT(_23466)) {
        if (_23466 != 0) {
            goto L1B; // [720] 741
        }
    }
    else {
        if (DBL_PTR(_23466)->dbl != 0.0) {
            goto L1B; // [720] 741
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    _23468 = (int)*(((s1_ptr)_2)->base + 41);
    if (IS_ATOM_INT(_23468) && IS_ATOM_INT(_38NOVALUE_16800)) {
        _23469 = (_23468 == _38NOVALUE_16800);
    }
    else {
        _23469 = binary_op(EQUALS, _23468, _38NOVALUE_16800);
    }
    _23468 = NOVALUE;
    if (_23469 == 0) {
        DeRef(_23469);
        _23469 = NOVALUE;
        goto L1C; // [737] 764
    }
    else {
        if (!IS_ATOM_INT(_23469) && DBL_PTR(_23469)->dbl == 0.0){
            DeRef(_23469);
            _23469 = NOVALUE;
            goto L1C; // [737] 764
        }
        DeRef(_23469);
        _23469 = NOVALUE;
    }
    DeRef(_23469);
    _23469 = NOVALUE;
L1B: 

    /** 				sym[S_OBJ_MIN] = MININT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = -1073741824;
    DeRef(_1);

    /** 				sym[S_OBJ_MAX] = MAXINT*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = 1073741823;
    DeRef(_1);
    goto L1D; // [761] 793
L1C: 

    /** 				sym[S_OBJ_MIN] = sym[S_OBJ_MIN_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23470 = (int)*(((s1_ptr)_2)->base + 41);
    Ref(_23470);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 30);
    _1 = *(int *)_2;
    *(int *)_2 = _23470;
    if( _1 != _23470 ){
        DeRef(_1);
    }
    _23470 = NOVALUE;

    /** 				sym[S_OBJ_MAX] = sym[S_OBJ_MAX_NEW]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23471 = (int)*(((s1_ptr)_2)->base + 42);
    Ref(_23471);
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 31);
    _1 = *(int *)_2;
    *(int *)_2 = _23471;
    if( _1 != _23471 ){
        DeRef(_1);
    }
    _23471 = NOVALUE;
L1D: 
L1A: 

    /** 		sym[S_OBJ_MIN_NEW] = -NOVALUE*/
    if (IS_ATOM_INT(_38NOVALUE_16800)) {
        if ((unsigned long)_38NOVALUE_16800 == 0xC0000000)
        _23472 = (int)NewDouble((double)-0xC0000000);
        else
        _23472 = - _38NOVALUE_16800;
    }
    else {
        _23472 = unary_op(UMINUS, _38NOVALUE_16800);
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 41);
    _1 = *(int *)_2;
    *(int *)_2 = _23472;
    if( _1 != _23472 ){
        DeRef(_1);
    }
    _23472 = NOVALUE;

    /** 		if sym[S_NREFS] = 1 and*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23473 = (int)*(((s1_ptr)_2)->base + 12);
    if (IS_ATOM_INT(_23473)) {
        _23474 = (_23473 == 1);
    }
    else {
        _23474 = binary_op(EQUALS, _23473, 1);
    }
    _23473 = NOVALUE;
    if (IS_ATOM_INT(_23474)) {
        if (_23474 == 0) {
            goto L1E; // [819] 874
        }
    }
    else {
        if (DBL_PTR(_23474)->dbl == 0.0) {
            goto L1E; // [819] 874
        }
    }
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23476 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23476 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _23477 = find_from(_23476, _39RTN_TOKS_16547, 1);
    _23476 = NOVALUE;
    if (_23477 == 0)
    {
        _23477 = NOVALUE;
        goto L1E; // [837] 874
    }
    else{
        _23477 = NOVALUE;
    }

    /** 			if sym[S_USAGE] != U_DELETED then*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _23478 = (int)*(((s1_ptr)_2)->base + 5);
    if (binary_op_a(EQUALS, _23478, 99)){
        _23478 = NOVALUE;
        goto L1F; // [850] 873
    }
    _23478 = NOVALUE;

    /** 				sym[S_USAGE] = U_DELETED*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 99;
    DeRef(_1);

    /** 				deleted_routines += 1*/
    _59deleted_routines_43609 = _59deleted_routines_43609 + 1;
L1F: 
L1E: 

    /** 		sym[S_NREFS] = 0*/
    _2 = (int)SEQ_PTR(_sym_43615);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _sym_43615 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		if not equal(symo, sym) then*/
    if (_symo_43616 == _sym_43615)
    _23481 = 1;
    else if (IS_ATOM_INT(_symo_43616) && IS_ATOM_INT(_sym_43615))
    _23481 = 0;
    else
    _23481 = (compare(_symo_43616, _sym_43615) == 0);
    if (_23481 != 0)
    goto L20; // [888] 906
    _23481 = NOVALUE;

    /** 			SymTab[s] = sym*/
    RefDS(_sym_43615);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _s_43614);
    _1 = *(int *)_2;
    *(int *)_2 = _sym_43615;
    DeRef(_1);

    /** 			updsym += 1*/
    _updsym_43612 = _updsym_43612 + 1;
L20: 

    /** 		s = sym[S_NEXT]*/
    _2 = (int)SEQ_PTR(_sym_43615);
    _s_43614 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_43614)){
        _s_43614 = (long)DBL_PTR(_s_43614)->dbl;
    }

    /** 	end while*/
    goto L1; // [918] 38
L2: 

    /** 	for i = 1 to length(temp_name_type) do*/
    if (IS_SEQUENCE(_38temp_name_type_17031)){
            _23485 = SEQ_PTR(_38temp_name_type_17031)->length;
    }
    else {
        _23485 = 1;
    }
    {
        int _i_43842;
        _i_43842 = 1;
L21: 
        if (_i_43842 > _23485){
            goto L22; // [928] 1061
        }

        /** 		integer upd = 0*/
        _upd_43845 = 0;

        /** 		if temp_name_type[i][T_GTYPE] != temp_name_type[i][T_GTYPE_NEW] then*/
        _2 = (int)SEQ_PTR(_38temp_name_type_17031);
        _23486 = (int)*(((s1_ptr)_2)->base + _i_43842);
        _2 = (int)SEQ_PTR(_23486);
        _23487 = (int)*(((s1_ptr)_2)->base + 1);
        _23486 = NOVALUE;
        _2 = (int)SEQ_PTR(_38temp_name_type_17031);
        _23488 = (int)*(((s1_ptr)_2)->base + _i_43842);
        _2 = (int)SEQ_PTR(_23488);
        _23489 = (int)*(((s1_ptr)_2)->base + 2);
        _23488 = NOVALUE;
        if (binary_op_a(EQUALS, _23487, _23489)){
            _23487 = NOVALUE;
            _23489 = NOVALUE;
            goto L23; // [966] 1003
        }
        _23487 = NOVALUE;
        _23489 = NOVALUE;

        /** 			temp_name_type[i][T_GTYPE] = temp_name_type[i][T_GTYPE_NEW]*/
        _2 = (int)SEQ_PTR(_38temp_name_type_17031);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38temp_name_type_17031 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_43842 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_38temp_name_type_17031);
        _23493 = (int)*(((s1_ptr)_2)->base + _i_43842);
        _2 = (int)SEQ_PTR(_23493);
        _23494 = (int)*(((s1_ptr)_2)->base + 2);
        _23493 = NOVALUE;
        Ref(_23494);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _23494;
        if( _1 != _23494 ){
            DeRef(_1);
        }
        _23494 = NOVALUE;
        _23491 = NOVALUE;

        /** 			upd = 1*/
        _upd_43845 = 1;
L23: 

        /** 		if temp_name_type[i][T_GTYPE_NEW] != TYPE_NULL then*/
        _2 = (int)SEQ_PTR(_38temp_name_type_17031);
        _23495 = (int)*(((s1_ptr)_2)->base + _i_43842);
        _2 = (int)SEQ_PTR(_23495);
        _23496 = (int)*(((s1_ptr)_2)->base + 2);
        _23495 = NOVALUE;
        if (binary_op_a(EQUALS, _23496, 0)){
            _23496 = NOVALUE;
            goto L24; // [1019] 1046
        }
        _23496 = NOVALUE;

        /** 			temp_name_type[i][T_GTYPE_NEW] = TYPE_NULL*/
        _2 = (int)SEQ_PTR(_38temp_name_type_17031);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _38temp_name_type_17031 = MAKE_SEQ(_2);
        }
        _3 = (int)(_i_43842 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = 0;
        DeRef(_1);
        _23498 = NOVALUE;

        /** 			upd = 1*/
        _upd_43845 = 1;
L24: 

        /** 		updsym += upd*/
        _updsym_43612 = _updsym_43612 + _upd_43845;

        /** 	end for*/
        _i_43842 = _i_43842 + 1;
        goto L21; // [1056] 935
L22: 
        ;
    }

    /** 	return updsym*/
    DeRef(_sym_43615);
    DeRef(_symo_43616);
    DeRef(_23404);
    _23404 = NOVALUE;
    DeRef(_23412);
    _23412 = NOVALUE;
    DeRef(_23415);
    _23415 = NOVALUE;
    DeRef(_23420);
    _23420 = NOVALUE;
    DeRef(_23423);
    _23423 = NOVALUE;
    DeRef(_23426);
    _23426 = NOVALUE;
    DeRef(_23429);
    _23429 = NOVALUE;
    DeRef(_23460);
    _23460 = NOVALUE;
    DeRef(_23439);
    _23439 = NOVALUE;
    DeRef(_23474);
    _23474 = NOVALUE;
    DeRef(_23466);
    _23466 = NOVALUE;
    return _updsym_43612;
    ;
}


void _59declare_prototype(int _s_43880)
{
    int _ret_type_43881 = NOVALUE;
    int _scope_43892 = NOVALUE;
    int _23518 = NOVALUE;
    int _23517 = NOVALUE;
    int _23515 = NOVALUE;
    int _23514 = NOVALUE;
    int _23513 = NOVALUE;
    int _23512 = NOVALUE;
    int _23510 = NOVALUE;
    int _23509 = NOVALUE;
    int _23508 = NOVALUE;
    int _23507 = NOVALUE;
    int _23506 = NOVALUE;
    int _23504 = NOVALUE;
    int _23503 = NOVALUE;
    int _23501 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sym_token( s ) = PROC then*/
    _23501 = _55sym_token(_s_43880);
    if (binary_op_a(NOTEQ, _23501, 27)){
        DeRef(_23501);
        _23501 = NOVALUE;
        goto L1; // [11] 25
    }
    DeRef(_23501);
    _23501 = NOVALUE;

    /** 		ret_type = "void "*/
    RefDS(_23386);
    DeRefi(_ret_type_43881);
    _ret_type_43881 = _23386;
    goto L2; // [22] 33
L1: 

    /** 		ret_type ="int "*/
    RefDS(_23387);
    DeRefi(_ret_type_43881);
    _ret_type_43881 = _23387;
L2: 

    /** 	c_hputs(ret_type)*/
    RefDS(_ret_type_43881);
    _56c_hputs(_ret_type_43881);

    /** 	if dll_option and TWINDOWS  then*/
    if (_59dll_option_42608 == 0) {
        goto L3; // [44] 116
    }
    if (_42TWINDOWS_17110 == 0)
    {
        goto L3; // [51] 116
    }
    else{
    }

    /** 		integer scope = SymTab[s][S_SCOPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23504 = (int)*(((s1_ptr)_2)->base + _s_43880);
    _2 = (int)SEQ_PTR(_23504);
    _scope_43892 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_43892)){
        _scope_43892 = (long)DBL_PTR(_scope_43892)->dbl;
    }
    _23504 = NOVALUE;

    /** 		if (scope = SC_PUBLIC*/
    _23506 = (_scope_43892 == 13);
    if (_23506 != 0) {
        _23507 = 1;
        goto L4; // [78] 92
    }
    _23508 = (_scope_43892 == 11);
    _23507 = (_23508 != 0);
L4: 
    if (_23507 != 0) {
        DeRef(_23509);
        _23509 = 1;
        goto L5; // [92] 106
    }
    _23510 = (_scope_43892 == 6);
    _23509 = (_23510 != 0);
L5: 
    if (_23509 == 0)
    {
        _23509 = NOVALUE;
        goto L6; // [106] 115
    }
    else{
        _23509 = NOVALUE;
    }

    /** 			c_hputs("__stdcall ")*/
    RefDS(_23511);
    _56c_hputs(_23511);
L6: 
L3: 

    /** 	c_hprintf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23512 = (int)*(((s1_ptr)_2)->base + _s_43880);
    _2 = (int)SEQ_PTR(_23512);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23513 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23513 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _23512 = NOVALUE;
    RefDS(_22756);
    Ref(_23513);
    _56c_hprintf(_22756, _23513);
    _23513 = NOVALUE;

    /** 	c_hputs(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23514 = (int)*(((s1_ptr)_2)->base + _s_43880);
    _2 = (int)SEQ_PTR(_23514);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23515 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23515 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _23514 = NOVALUE;
    Ref(_23515);
    _56c_hputs(_23515);
    _23515 = NOVALUE;

    /** 	c_hputs("(")*/
    RefDS(_23516);
    _56c_hputs(_23516);

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23517 = (int)*(((s1_ptr)_2)->base + _s_43880);
    _2 = (int)SEQ_PTR(_23517);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _23518 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _23518 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _23517 = NOVALUE;
    {
        int _i_43921;
        _i_43921 = 1;
L7: 
        if (binary_op_a(GREATER, _i_43921, _23518)){
            goto L8; // [172] 206
        }

        /** 		if i = 1 then*/
        if (binary_op_a(NOTEQ, _i_43921, 1)){
            goto L9; // [181] 193
        }

        /** 			c_hputs("int")*/
        RefDS(_23520);
        _56c_hputs(_23520);
        goto LA; // [190] 199
L9: 

        /** 			c_hputs(", int")*/
        RefDS(_23521);
        _56c_hputs(_23521);
LA: 

        /** 	end for*/
        _0 = _i_43921;
        if (IS_ATOM_INT(_i_43921)) {
            _i_43921 = _i_43921 + 1;
            if ((long)((unsigned long)_i_43921 +(unsigned long) HIGH_BITS) >= 0){
                _i_43921 = NewDouble((double)_i_43921);
            }
        }
        else {
            _i_43921 = binary_op_a(PLUS, _i_43921, 1);
        }
        DeRef(_0);
        goto L7; // [201] 179
L8: 
        ;
        DeRef(_i_43921);
    }

    /** 	c_hputs(");\n")*/
    RefDS(_22925);
    _56c_hputs(_22925);

    /** end procedure*/
    DeRefi(_ret_type_43881);
    DeRef(_23506);
    _23506 = NOVALUE;
    DeRef(_23508);
    _23508 = NOVALUE;
    DeRef(_23510);
    _23510 = NOVALUE;
    _23518 = NOVALUE;
    return;
    ;
}


void _59add_to_routine_list(int _s_43937, int _seq_num_43938, int _first_43939)
{
    int _p_44014 = NOVALUE;
    int _23567 = NOVALUE;
    int _23565 = NOVALUE;
    int _23563 = NOVALUE;
    int _23561 = NOVALUE;
    int _23559 = NOVALUE;
    int _23558 = NOVALUE;
    int _23557 = NOVALUE;
    int _23555 = NOVALUE;
    int _23553 = NOVALUE;
    int _23551 = NOVALUE;
    int _23550 = NOVALUE;
    int _23548 = NOVALUE;
    int _23547 = NOVALUE;
    int _23543 = NOVALUE;
    int _23542 = NOVALUE;
    int _23541 = NOVALUE;
    int _23540 = NOVALUE;
    int _23539 = NOVALUE;
    int _23538 = NOVALUE;
    int _23537 = NOVALUE;
    int _23536 = NOVALUE;
    int _23535 = NOVALUE;
    int _23534 = NOVALUE;
    int _23532 = NOVALUE;
    int _23531 = NOVALUE;
    int _23530 = NOVALUE;
    int _23529 = NOVALUE;
    int _23526 = NOVALUE;
    int _23525 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if not first then*/
    if (_first_43939 != 0)
    goto L1; // [9] 18

    /** 		c_puts(",\n")*/
    RefDS(_23523);
    _56c_puts(_23523);
L1: 

    /** 	c_puts("  {\"")*/
    RefDS(_23524);
    _56c_puts(_23524);

    /** 	c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23525 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23525);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23526 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23526 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _23525 = NOVALUE;
    Ref(_23526);
    _56c_puts(_23526);
    _23526 = NOVALUE;

    /** 	c_puts("\", ")*/
    RefDS(_23527);
    _56c_puts(_23527);

    /** 	c_puts("(int (*)())")*/
    RefDS(_23528);
    _56c_puts(_23528);

    /** 	c_printf("_%d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23529 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23529);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23530 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23530 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _23529 = NOVALUE;
    RefDS(_22756);
    Ref(_23530);
    _56c_printf(_22756, _23530);
    _23530 = NOVALUE;

    /** 	c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23531 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23531);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23532 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23532 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _23531 = NOVALUE;
    Ref(_23532);
    _56c_puts(_23532);
    _23532 = NOVALUE;

    /** 	c_printf(", %d", seq_num)*/
    RefDS(_23533);
    _56c_printf(_23533, _seq_num_43938);

    /** 	c_printf(", %d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23534 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23534);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23535 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23535 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _23534 = NOVALUE;
    RefDS(_23533);
    Ref(_23535);
    _56c_printf(_23533, _23535);
    _23535 = NOVALUE;

    /** 	c_printf(", %d", SymTab[s][S_NUM_ARGS])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23536 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23536);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _23537 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _23537 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _23536 = NOVALUE;
    RefDS(_23533);
    Ref(_23537);
    _56c_printf(_23533, _23537);
    _23537 = NOVALUE;

    /** 	if TWINDOWS and dll_option and find( SymTab[s][S_SCOPE], { SC_GLOBAL, SC_EXPORT, SC_PUBLIC} ) then*/
    if (_42TWINDOWS_17110 == 0) {
        _23538 = 0;
        goto L2; // [131] 141
    }
    _23538 = (_59dll_option_42608 != 0);
L2: 
    if (_23538 == 0) {
        goto L3; // [141] 186
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23540 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23540);
    _23541 = (int)*(((s1_ptr)_2)->base + 4);
    _23540 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 6;
    *((int *)(_2+8)) = 11;
    *((int *)(_2+12)) = 13;
    _23542 = MAKE_SEQ(_1);
    _23543 = find_from(_23541, _23542, 1);
    _23541 = NOVALUE;
    DeRefDS(_23542);
    _23542 = NOVALUE;
    if (_23543 == 0)
    {
        _23543 = NOVALUE;
        goto L3; // [175] 186
    }
    else{
        _23543 = NOVALUE;
    }

    /** 		c_puts(", 1")  -- must call with __stdcall convention*/
    RefDS(_23544);
    _56c_puts(_23544);
    goto L4; // [183] 192
L3: 

    /** 		c_puts(", 0")  -- default: call with normal or __cdecl convention*/
    RefDS(_23545);
    _56c_puts(_23545);
L4: 

    /** 	c_printf(", %d, 0", SymTab[s][S_SCOPE] )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23547 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23547);
    _23548 = (int)*(((s1_ptr)_2)->base + 4);
    _23547 = NOVALUE;
    RefDS(_23546);
    Ref(_23548);
    _56c_printf(_23546, _23548);
    _23548 = NOVALUE;

    /** 	c_puts("}")*/
    RefDS(_23549);
    _56c_puts(_23549);

    /** 	if SymTab[s][S_NREFS] < 2 then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23550 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23550);
    _23551 = (int)*(((s1_ptr)_2)->base + 12);
    _23550 = NOVALUE;
    if (binary_op_a(GREATEREQ, _23551, 2)){
        _23551 = NOVALUE;
        goto L5; // [229] 249
    }
    _23551 = NOVALUE;

    /** 		SymTab[s][S_NREFS] = 2 --s->nrefs++*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_s_43937 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 12);
    _1 = *(int *)_2;
    *(int *)_2 = 2;
    DeRef(_1);
    _23553 = NOVALUE;
L5: 

    /** 	symtab_index p = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23555 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23555);
    _p_44014 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_p_44014)){
        _p_44014 = (long)DBL_PTR(_p_44014)->dbl;
    }
    _23555 = NOVALUE;

    /** 	for i = 1 to SymTab[s][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23557 = (int)*(((s1_ptr)_2)->base + _s_43937);
    _2 = (int)SEQ_PTR(_23557);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _23558 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _23558 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _23557 = NOVALUE;
    {
        int _i_44020;
        _i_44020 = 1;
L6: 
        if (binary_op_a(GREATER, _i_44020, _23558)){
            goto L7; // [279] 377
        }

        /** 		SymTab[p][S_ARG_SEQ_ELEM_NEW] = TYPE_OBJECT*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44014 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 46);
        _1 = *(int *)_2;
        *(int *)_2 = 16;
        DeRef(_1);
        _23559 = NOVALUE;

        /** 		SymTab[p][S_ARG_TYPE_NEW] = TYPE_OBJECT*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44014 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 44);
        _1 = *(int *)_2;
        *(int *)_2 = 16;
        DeRef(_1);
        _23561 = NOVALUE;

        /** 		SymTab[p][S_ARG_MIN_NEW] = NOVALUE*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44014 + ((s1_ptr)_2)->base);
        Ref(_38NOVALUE_16800);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 49);
        _1 = *(int *)_2;
        *(int *)_2 = _38NOVALUE_16800;
        DeRef(_1);
        _23563 = NOVALUE;

        /** 		SymTab[p][S_ARG_SEQ_LEN_NEW] = NOVALUE*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_p_44014 + ((s1_ptr)_2)->base);
        Ref(_38NOVALUE_16800);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 52);
        _1 = *(int *)_2;
        *(int *)_2 = _38NOVALUE_16800;
        DeRef(_1);
        _23565 = NOVALUE;

        /** 		p = SymTab[p][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23567 = (int)*(((s1_ptr)_2)->base + _p_44014);
        _2 = (int)SEQ_PTR(_23567);
        _p_44014 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_p_44014)){
            _p_44014 = (long)DBL_PTR(_p_44014)->dbl;
        }
        _23567 = NOVALUE;

        /** 	end for*/
        _0 = _i_44020;
        if (IS_ATOM_INT(_i_44020)) {
            _i_44020 = _i_44020 + 1;
            if ((long)((unsigned long)_i_44020 +(unsigned long) HIGH_BITS) >= 0){
                _i_44020 = NewDouble((double)_i_44020);
            }
        }
        else {
            _i_44020 = binary_op_a(PLUS, _i_44020, 1);
        }
        DeRef(_0);
        goto L6; // [372] 286
L7: 
        ;
        DeRef(_i_44020);
    }

    /** end procedure*/
    _23558 = NOVALUE;
    return;
    ;
}


void _59DeclareRoutineList()
{
    int _s_44052 = NOVALUE;
    int _first_44053 = NOVALUE;
    int _seq_num_44054 = NOVALUE;
    int _these_routines_44062 = NOVALUE;
    int _these_routines_44084 = NOVALUE;
    int _23583 = NOVALUE;
    int _23582 = NOVALUE;
    int _23580 = NOVALUE;
    int _23578 = NOVALUE;
    int _23575 = NOVALUE;
    int _23574 = NOVALUE;
    int _23572 = NOVALUE;
    int _23570 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer first, seq_num*/

    /** 	c_hputs("extern struct routine_list _00[];\n")*/
    RefDS(_23569);
    _56c_hputs(_23569);

    /** 	check_file_routines()*/
    _59check_file_routines();

    /** 	for f = 1 to length( file_routines ) do*/
    if (IS_SEQUENCE(_59file_routines_44638)){
            _23570 = SEQ_PTR(_59file_routines_44638)->length;
    }
    else {
        _23570 = 1;
    }
    {
        int _f_44059;
        _f_44059 = 1;
L1: 
        if (_f_44059 > _23570){
            goto L2; // [19] 98
        }

        /** 		sequence these_routines = file_routines[f]*/
        DeRef(_these_routines_44062);
        _2 = (int)SEQ_PTR(_59file_routines_44638);
        _these_routines_44062 = (int)*(((s1_ptr)_2)->base + _f_44059);
        Ref(_these_routines_44062);

        /** 		for r = 1 to length( these_routines ) do*/
        if (IS_SEQUENCE(_these_routines_44062)){
                _23572 = SEQ_PTR(_these_routines_44062)->length;
        }
        else {
            _23572 = 1;
        }
        {
            int _r_44066;
            _r_44066 = 1;
L3: 
            if (_r_44066 > _23572){
                goto L4; // [41] 89
            }

            /** 			s = these_routines[r]*/
            _2 = (int)SEQ_PTR(_these_routines_44062);
            _s_44052 = (int)*(((s1_ptr)_2)->base + _r_44066);
            if (!IS_ATOM_INT(_s_44052)){
                _s_44052 = (long)DBL_PTR(_s_44052)->dbl;
            }

            /** 			if SymTab[s][S_USAGE] != U_DELETED then*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23574 = (int)*(((s1_ptr)_2)->base + _s_44052);
            _2 = (int)SEQ_PTR(_23574);
            _23575 = (int)*(((s1_ptr)_2)->base + 5);
            _23574 = NOVALUE;
            if (binary_op_a(EQUALS, _23575, 99)){
                _23575 = NOVALUE;
                goto L5; // [72] 82
            }
            _23575 = NOVALUE;

            /** 				declare_prototype( s )*/
            _59declare_prototype(_s_44052);
L5: 

            /** 		end for*/
            _r_44066 = _r_44066 + 1;
            goto L3; // [84] 48
L4: 
            ;
        }
        DeRef(_these_routines_44062);
        _these_routines_44062 = NOVALUE;

        /** 	end for*/
        _f_44059 = _f_44059 + 1;
        goto L1; // [93] 26
L2: 
        ;
    }

    /** 	c_puts("\n")*/
    RefDS(_22815);
    _56c_puts(_22815);

    /** 	seq_num = 0*/
    _seq_num_44054 = 0;

    /** 	first = TRUE*/
    _first_44053 = _9TRUE_428;

    /** 	c_puts("struct routine_list _00[] = {\n")*/
    RefDS(_23577);
    _56c_puts(_23577);

    /** 	for f = 1 to length( file_routines ) do*/
    if (IS_SEQUENCE(_59file_routines_44638)){
            _23578 = SEQ_PTR(_59file_routines_44638)->length;
    }
    else {
        _23578 = 1;
    }
    {
        int _f_44081;
        _f_44081 = 1;
L6: 
        if (_f_44081 > _23578){
            goto L7; // [129] 222
        }

        /** 		sequence these_routines = file_routines[f]*/
        DeRef(_these_routines_44084);
        _2 = (int)SEQ_PTR(_59file_routines_44638);
        _these_routines_44084 = (int)*(((s1_ptr)_2)->base + _f_44081);
        Ref(_these_routines_44084);

        /** 		for r = 1 to length( these_routines ) do*/
        if (IS_SEQUENCE(_these_routines_44084)){
                _23580 = SEQ_PTR(_these_routines_44084)->length;
        }
        else {
            _23580 = 1;
        }
        {
            int _r_44088;
            _r_44088 = 1;
L8: 
            if (_r_44088 > _23580){
                goto L9; // [151] 213
            }

            /** 			s = these_routines[r]*/
            _2 = (int)SEQ_PTR(_these_routines_44084);
            _s_44052 = (int)*(((s1_ptr)_2)->base + _r_44088);
            if (!IS_ATOM_INT(_s_44052)){
                _s_44052 = (long)DBL_PTR(_s_44052)->dbl;
            }

            /** 			if SymTab[s][S_RI_TARGET] then*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23582 = (int)*(((s1_ptr)_2)->base + _s_44052);
            _2 = (int)SEQ_PTR(_23582);
            _23583 = (int)*(((s1_ptr)_2)->base + 53);
            _23582 = NOVALUE;
            if (_23583 == 0) {
                _23583 = NOVALUE;
                goto LA; // [180] 200
            }
            else {
                if (!IS_ATOM_INT(_23583) && DBL_PTR(_23583)->dbl == 0.0){
                    _23583 = NOVALUE;
                    goto LA; // [180] 200
                }
                _23583 = NOVALUE;
            }
            _23583 = NOVALUE;

            /** 				add_to_routine_list( s, seq_num, first )*/
            _59add_to_routine_list(_s_44052, _seq_num_44054, _first_44053);

            /** 				first = FALSE*/
            _first_44053 = _9FALSE_426;
LA: 

            /** 			seq_num += 1*/
            _seq_num_44054 = _seq_num_44054 + 1;

            /** 		end for*/
            _r_44088 = _r_44088 + 1;
            goto L8; // [208] 158
L9: 
            ;
        }
        DeRef(_these_routines_44084);
        _these_routines_44084 = NOVALUE;

        /** 	end for*/
        _f_44081 = _f_44081 + 1;
        goto L6; // [217] 136
L7: 
        ;
    }

    /** 	if not first then*/
    if (_first_44053 != 0)
    goto LB; // [224] 233

    /** 		c_puts(",\n")*/
    RefDS(_23523);
    _56c_puts(_23523);
LB: 

    /** 	c_puts("  {\"\", 0, 999999999, 0, 0, 0, 0}\n};\n\n")  -- end marker*/
    RefDS(_23586);
    _56c_puts(_23586);

    /** 	c_hputs("extern unsigned char ** _02;\n")*/
    RefDS(_23587);
    _56c_hputs(_23587);

    /** 	c_puts("unsigned char ** _02;\n")*/
    RefDS(_23588);
    _56c_puts(_23588);

    /** 	c_hputs("extern object _0switches;\n")*/
    RefDS(_23589);
    _56c_hputs(_23589);

    /** 	c_puts("object _0switches;\n")*/
    RefDS(_23590);
    _56c_puts(_23590);

    /** end procedure*/
    return;
    ;
}


void _59DeclareNameSpaceList()
{
    int _s_44114 = NOVALUE;
    int _first_44115 = NOVALUE;
    int _seq_num_44116 = NOVALUE;
    int _23610 = NOVALUE;
    int _23608 = NOVALUE;
    int _23607 = NOVALUE;
    int _23606 = NOVALUE;
    int _23605 = NOVALUE;
    int _23603 = NOVALUE;
    int _23602 = NOVALUE;
    int _23599 = NOVALUE;
    int _23598 = NOVALUE;
    int _23597 = NOVALUE;
    int _23596 = NOVALUE;
    int _23595 = NOVALUE;
    int _23593 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer first, seq_num*/

    /** 	c_hputs("extern struct ns_list _01[];\n")*/
    RefDS(_23591);
    _56c_hputs(_23591);

    /** 	c_puts("struct ns_list _01[] = {\n")*/
    RefDS(_23592);
    _56c_puts(_23592);

    /** 	seq_num = 0*/
    _seq_num_44116 = 0;

    /** 	first = TRUE*/
    _first_44115 = _9TRUE_428;

    /** 	s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23593 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
    _2 = (int)SEQ_PTR(_23593);
    _s_44114 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44114)){
        _s_44114 = (long)DBL_PTR(_s_44114)->dbl;
    }
    _23593 = NOVALUE;

    /** 	while s do*/
L1: 
    if (_s_44114 == 0)
    {
        goto L2; // [50] 215
    }
    else{
    }

    /** 		if find(SymTab[s][S_TOKEN], NAMED_TOKS) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23595 = (int)*(((s1_ptr)_2)->base + _s_44114);
    _2 = (int)SEQ_PTR(_23595);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23596 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23596 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _23595 = NOVALUE;
    _23597 = find_from(_23596, _39NAMED_TOKS_16549, 1);
    _23596 = NOVALUE;
    if (_23597 == 0)
    {
        _23597 = NOVALUE;
        goto L3; // [74] 194
    }
    else{
        _23597 = NOVALUE;
    }

    /** 			if SymTab[s][S_TOKEN] = NAMESPACE then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23598 = (int)*(((s1_ptr)_2)->base + _s_44114);
    _2 = (int)SEQ_PTR(_23598);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23599 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23599 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _23598 = NOVALUE;
    if (binary_op_a(NOTEQ, _23599, 523)){
        _23599 = NOVALUE;
        goto L4; // [93] 187
    }
    _23599 = NOVALUE;

    /** 				if not first then*/
    if (_first_44115 != 0)
    goto L5; // [99] 108

    /** 					c_puts(",\n")*/
    RefDS(_23523);
    _56c_puts(_23523);
L5: 

    /** 				first = FALSE*/
    _first_44115 = _9FALSE_426;

    /** 				c_puts("  {\"")*/
    RefDS(_23524);
    _56c_puts(_23524);

    /** 				c_puts(SymTab[s][S_NAME])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23602 = (int)*(((s1_ptr)_2)->base + _s_44114);
    _2 = (int)SEQ_PTR(_23602);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _23603 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _23603 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _23602 = NOVALUE;
    Ref(_23603);
    _56c_puts(_23603);
    _23603 = NOVALUE;

    /** 				c_printf("\", %d", SymTab[s][S_OBJ])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23605 = (int)*(((s1_ptr)_2)->base + _s_44114);
    _2 = (int)SEQ_PTR(_23605);
    _23606 = (int)*(((s1_ptr)_2)->base + 1);
    _23605 = NOVALUE;
    RefDS(_23604);
    Ref(_23606);
    _56c_printf(_23604, _23606);
    _23606 = NOVALUE;

    /** 				c_printf(", %d", seq_num)*/
    RefDS(_23533);
    _56c_printf(_23533, _seq_num_44116);

    /** 				c_printf(", %d", SymTab[s][S_FILE_NO])*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23607 = (int)*(((s1_ptr)_2)->base + _s_44114);
    _2 = (int)SEQ_PTR(_23607);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23608 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23608 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _23607 = NOVALUE;
    RefDS(_23533);
    Ref(_23608);
    _56c_printf(_23533, _23608);
    _23608 = NOVALUE;

    /** 				c_puts("}")*/
    RefDS(_23549);
    _56c_puts(_23549);
L4: 

    /** 			seq_num += 1*/
    _seq_num_44116 = _seq_num_44116 + 1;
L3: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23610 = (int)*(((s1_ptr)_2)->base + _s_44114);
    _2 = (int)SEQ_PTR(_23610);
    _s_44114 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44114)){
        _s_44114 = (long)DBL_PTR(_s_44114)->dbl;
    }
    _23610 = NOVALUE;

    /** 	end while*/
    goto L1; // [212] 50
L2: 

    /** 	if not first then*/
    if (_first_44115 != 0)
    goto L6; // [217] 226

    /** 		c_puts(",\n")*/
    RefDS(_23523);
    _56c_puts(_23523);
L6: 

    /** 	c_puts("  {\"\", 0, 999999999, 0}\n};\n\n")  -- end marker*/
    RefDS(_23613);
    _56c_puts(_23613);

    /** end procedure*/
    return;
    ;
}


int _59is_exported(int _s_44178)
{
    int _eentry_44179 = NOVALUE;
    int _scope_44182 = NOVALUE;
    int _23628 = NOVALUE;
    int _23627 = NOVALUE;
    int _23626 = NOVALUE;
    int _23625 = NOVALUE;
    int _23624 = NOVALUE;
    int _23623 = NOVALUE;
    int _23622 = NOVALUE;
    int _23621 = NOVALUE;
    int _23620 = NOVALUE;
    int _23619 = NOVALUE;
    int _23618 = NOVALUE;
    int _23616 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_s_44178)) {
        _1 = (long)(DBL_PTR(_s_44178)->dbl);
        if (UNIQUE(DBL_PTR(_s_44178)) && (DBL_PTR(_s_44178)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_s_44178);
        _s_44178 = _1;
    }

    /** 	sequence eentry = SymTab[s]*/
    DeRef(_eentry_44179);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _eentry_44179 = (int)*(((s1_ptr)_2)->base + _s_44178);
    Ref(_eentry_44179);

    /** 	integer scope = eentry[S_SCOPE]*/
    _2 = (int)SEQ_PTR(_eentry_44179);
    _scope_44182 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_scope_44182))
    _scope_44182 = (long)DBL_PTR(_scope_44182)->dbl;

    /** 	if eentry[S_MODE] = M_NORMAL then*/
    _2 = (int)SEQ_PTR(_eentry_44179);
    _23616 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(NOTEQ, _23616, 1)){
        _23616 = NOVALUE;
        goto L1; // [31] 125
    }
    _23616 = NOVALUE;

    /** 		if eentry[S_FILE_NO] = 1 and find(scope, { SC_EXPORT, SC_PUBLIC, SC_GLOBAL }) then*/
    _2 = (int)SEQ_PTR(_eentry_44179);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23618 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23618 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (IS_ATOM_INT(_23618)) {
        _23619 = (_23618 == 1);
    }
    else {
        _23619 = binary_op(EQUALS, _23618, 1);
    }
    _23618 = NOVALUE;
    if (IS_ATOM_INT(_23619)) {
        if (_23619 == 0) {
            goto L2; // [47] 79
        }
    }
    else {
        if (DBL_PTR(_23619)->dbl == 0.0) {
            goto L2; // [47] 79
        }
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 11;
    *((int *)(_2+8)) = 13;
    *((int *)(_2+12)) = 6;
    _23621 = MAKE_SEQ(_1);
    _23622 = find_from(_scope_44182, _23621, 1);
    DeRefDS(_23621);
    _23621 = NOVALUE;
    if (_23622 == 0)
    {
        _23622 = NOVALUE;
        goto L2; // [69] 79
    }
    else{
        _23622 = NOVALUE;
    }

    /** 			return 1*/
    DeRef(_eentry_44179);
    DeRef(_23619);
    _23619 = NOVALUE;
    return 1;
L2: 

    /** 		if scope = SC_PUBLIC and*/
    _23623 = (_scope_44182 == 13);
    if (_23623 == 0) {
        goto L3; // [87] 124
    }
    _2 = (int)SEQ_PTR(_35include_matrix_15602);
    _23625 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_eentry_44179);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23626 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23626 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _2 = (int)SEQ_PTR(_23625);
    if (!IS_ATOM_INT(_23626)){
        _23627 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23626)->dbl));
    }
    else{
        _23627 = (int)*(((s1_ptr)_2)->base + _23626);
    }
    _23625 = NOVALUE;
    if (IS_ATOM_INT(_23627)) {
        {unsigned long tu;
             tu = (unsigned long)_23627 & (unsigned long)4;
             _23628 = MAKE_UINT(tu);
        }
    }
    else {
        _23628 = binary_op(AND_BITS, _23627, 4);
    }
    _23627 = NOVALUE;
    if (_23628 == 0) {
        DeRef(_23628);
        _23628 = NOVALUE;
        goto L3; // [114] 124
    }
    else {
        if (!IS_ATOM_INT(_23628) && DBL_PTR(_23628)->dbl == 0.0){
            DeRef(_23628);
            _23628 = NOVALUE;
            goto L3; // [114] 124
        }
        DeRef(_23628);
        _23628 = NOVALUE;
    }
    DeRef(_23628);
    _23628 = NOVALUE;

    /** 			return 1*/
    DeRef(_eentry_44179);
    DeRef(_23623);
    _23623 = NOVALUE;
    DeRef(_23619);
    _23619 = NOVALUE;
    _23626 = NOVALUE;
    return 1;
L3: 
L1: 

    /** 	return 0*/
    DeRef(_eentry_44179);
    DeRef(_23623);
    _23623 = NOVALUE;
    DeRef(_23619);
    _23619 = NOVALUE;
    _23626 = NOVALUE;
    return 0;
    ;
}


void _59version()
{
    int _23662 = NOVALUE;
    int _23661 = NOVALUE;
    int _0, _1, _2;
    

    /** 	c_puts("// Euphoria To C version " & version_string() & "\n")*/
    _23661 = _26version_string(0);
    {
        int concat_list[3];

        concat_list[0] = _22815;
        concat_list[1] = _23661;
        concat_list[2] = _23660;
        Concat_N((object_ptr)&_23662, concat_list, 3);
    }
    DeRef(_23661);
    _23661 = NOVALUE;
    _56c_puts(_23662);
    _23662 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


void _59new_c_file(int _name_44286)
{
    int _23665 = NOVALUE;
    int _0, _1, _2;
    

    /** 	cfile_size = 0*/
    _38cfile_size_17028 = 0;

    /** 	if LAST_PASS = FALSE then*/
    if (_59LAST_PASS_42590 != _9FALSE_426)
    goto L1; // [16] 26

    /** 		return*/
    DeRefDS(_name_44286);
    return;
L1: 

    /** 	write_checksum( c_code )*/
    _57write_checksum(_56c_code_46447);

    /** 	close(c_code)*/
    EClose(_56c_code_46447);

    /** 	c_code = open(output_dir & name & ".c", "w")*/
    {
        int concat_list[3];

        concat_list[0] = _23664;
        concat_list[1] = _name_44286;
        concat_list[2] = _59output_dir_42630;
        Concat_N((object_ptr)&_23665, concat_list, 3);
    }
    _56c_code_46447 = EOpen(_23665, _22769, 0);
    DeRefDS(_23665);
    _23665 = NOVALUE;

    /** 	if c_code = -1 then*/
    if (_56c_code_46447 != -1)
    goto L2; // [60] 72

    /** 		CompileErr(57)*/
    RefDS(_22663);
    _46CompileErr(57, _22663, 0);
L2: 

    /** 	cfile_count += 1*/
    _38cfile_count_17027 = _38cfile_count_17027 + 1;

    /** 	version()*/
    _59version();

    /** 	c_puts("#include \"include/euphoria.h\"\n")*/
    RefDS(_22773);
    _56c_puts(_22773);

    /** 	c_puts("#include \"main-.h\"\n\n")*/
    RefDS(_22774);
    _56c_puts(_22774);

    /** 	if not TUNIX then*/

    /** 		name = lower(name)  -- for faster compare later*/
    RefDS(_name_44286);
    _0 = _name_44286;
    _name_44286 = _10lower(_name_44286);
    DeRefDS(_0);

    /** end procedure*/
    DeRefDS(_name_44286);
    return;
    ;
}


int _59unique_c_name(int _name_44315)
{
    int _i_44316 = NOVALUE;
    int _compare_name_44317 = NOVALUE;
    int _next_fc_44318 = NOVALUE;
    int _23681 = NOVALUE;
    int _23679 = NOVALUE;
    int _23678 = NOVALUE;
    int _23677 = NOVALUE;
    int _23675 = NOVALUE;
    int _0, _1, _2;
    

    /** 	compare_name = name & ".c"*/
    Concat((object_ptr)&_compare_name_44317, _name_44315, _23664);

    /** 	if not TUNIX then*/

    /** 		compare_name = lower(compare_name)*/
    RefDS(_compare_name_44317);
    _0 = _compare_name_44317;
    _compare_name_44317 = _10lower(_compare_name_44317);
    DeRefDS(_0);

    /** 	next_fc = 1*/
    _next_fc_44318 = 1;

    /** 	i = 1*/
    _i_44316 = 1;

    /** 	while i <= length(generated_files) do*/
L1: 
    if (IS_SEQUENCE(_59generated_files_42612)){
            _23675 = SEQ_PTR(_59generated_files_42612)->length;
    }
    else {
        _23675 = 1;
    }
    if (_i_44316 > _23675)
    goto L2; // [45] 139

    /** 		if equal(generated_files[i], compare_name) then*/
    _2 = (int)SEQ_PTR(_59generated_files_42612);
    _23677 = (int)*(((s1_ptr)_2)->base + _i_44316);
    if (_23677 == _compare_name_44317)
    _23678 = 1;
    else if (IS_ATOM_INT(_23677) && IS_ATOM_INT(_compare_name_44317))
    _23678 = 0;
    else
    _23678 = (compare(_23677, _compare_name_44317) == 0);
    _23677 = NOVALUE;
    if (_23678 == 0)
    {
        _23678 = NOVALUE;
        goto L3; // [61] 127
    }
    else{
        _23678 = NOVALUE;
    }

    /** 			if next_fc > length(file_chars) then*/
    if (IS_SEQUENCE(_59file_chars_44311)){
            _23679 = SEQ_PTR(_59file_chars_44311)->length;
    }
    else {
        _23679 = 1;
    }
    if (_next_fc_44318 <= _23679)
    goto L4; // [69] 81

    /** 				CompileErr(140)*/
    RefDS(_22663);
    _46CompileErr(140, _22663, 0);
L4: 

    /** 			name[1] = file_chars[next_fc]*/
    _2 = (int)SEQ_PTR(_59file_chars_44311);
    _23681 = (int)*(((s1_ptr)_2)->base + _next_fc_44318);
    Ref(_23681);
    _2 = (int)SEQ_PTR(_name_44315);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _name_44315 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _23681;
    if( _1 != _23681 ){
        DeRef(_1);
    }
    _23681 = NOVALUE;

    /** 			compare_name = name & ".c"*/
    Concat((object_ptr)&_compare_name_44317, _name_44315, _23664);

    /** 			if not TUNIX then*/

    /** 				compare_name = lower(compare_name)*/
    RefDS(_compare_name_44317);
    _0 = _compare_name_44317;
    _compare_name_44317 = _10lower(_compare_name_44317);
    DeRefDS(_0);

    /** 			next_fc += 1*/
    _next_fc_44318 = _next_fc_44318 + 1;

    /** 			i = 1 -- start over and compare again*/
    _i_44316 = 1;
    goto L1; // [124] 40
L3: 

    /** 			i += 1*/
    _i_44316 = _i_44316 + 1;

    /** 	end while*/
    goto L1; // [136] 40
L2: 

    /** 	return name*/
    DeRef(_compare_name_44317);
    return _name_44315;
    ;
}


int _59is_file_newer(int _f1_44347, int _f2_44348)
{
    int _d1_44349 = NOVALUE;
    int _d2_44352 = NOVALUE;
    int _diff_2__tmp_at42_44363 = NOVALUE;
    int _diff_1__tmp_at42_44362 = NOVALUE;
    int _diff_inlined_diff_at_42_44361 = NOVALUE;
    int _23691 = NOVALUE;
    int _23689 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d1 = file_timestamp(f1)*/
    RefDS(_f1_44347);
    _0 = _d1_44349;
    _d1_44349 = _13file_timestamp(_f1_44347);
    DeRef(_0);

    /** 	object d2 = file_timestamp(f2)*/
    RefDS(_f2_44348);
    _0 = _d2_44352;
    _d2_44352 = _13file_timestamp(_f2_44348);
    DeRef(_0);

    /** 	if atom(d1) or atom(d2) then return 1 end if*/
    _23689 = IS_ATOM(_d1_44349);
    if (_23689 != 0) {
        goto L1; // [22] 34
    }
    _23691 = IS_ATOM(_d2_44352);
    if (_23691 == 0)
    {
        _23691 = NOVALUE;
        goto L2; // [30] 39
    }
    else{
        _23691 = NOVALUE;
    }
L1: 
    DeRefDS(_f1_44347);
    DeRefDS(_f2_44348);
    DeRef(_d1_44349);
    DeRef(_d2_44352);
    return 1;
L2: 

    /** 	if datetime:diff(d1, d2) < 0 then*/

    /** 	return datetimeToSeconds(dt2) - datetimeToSeconds(dt1)*/
    Ref(_d2_44352);
    _0 = _diff_1__tmp_at42_44362;
    _diff_1__tmp_at42_44362 = _14datetimeToSeconds(_d2_44352);
    DeRef(_0);
    Ref(_d1_44349);
    _0 = _diff_2__tmp_at42_44363;
    _diff_2__tmp_at42_44363 = _14datetimeToSeconds(_d1_44349);
    DeRef(_0);
    DeRef(_diff_inlined_diff_at_42_44361);
    if (IS_ATOM_INT(_diff_1__tmp_at42_44362) && IS_ATOM_INT(_diff_2__tmp_at42_44363)) {
        _diff_inlined_diff_at_42_44361 = _diff_1__tmp_at42_44362 - _diff_2__tmp_at42_44363;
        if ((long)((unsigned long)_diff_inlined_diff_at_42_44361 +(unsigned long) HIGH_BITS) >= 0){
            _diff_inlined_diff_at_42_44361 = NewDouble((double)_diff_inlined_diff_at_42_44361);
        }
    }
    else {
        _diff_inlined_diff_at_42_44361 = binary_op(MINUS, _diff_1__tmp_at42_44362, _diff_2__tmp_at42_44363);
    }
    DeRef(_diff_1__tmp_at42_44362);
    _diff_1__tmp_at42_44362 = NOVALUE;
    DeRef(_diff_2__tmp_at42_44363);
    _diff_2__tmp_at42_44363 = NOVALUE;
    if (binary_op_a(GREATEREQ, _diff_inlined_diff_at_42_44361, 0)){
        goto L3; // [58] 69
    }

    /** 		return 1*/
    DeRefDS(_f1_44347);
    DeRefDS(_f2_44348);
    DeRef(_d1_44349);
    DeRef(_d2_44352);
    return 1;
L3: 

    /** 	return 0*/
    DeRefDS(_f1_44347);
    DeRefDS(_f2_44348);
    DeRef(_d1_44349);
    DeRef(_d2_44352);
    return 0;
    ;
}


void _59add_file(int _filename_44367, int _eu_filename_44368)
{
    int _obj_fname_44388 = NOVALUE;
    int _src_fname_44389 = NOVALUE;
    int _23715 = NOVALUE;
    int _23714 = NOVALUE;
    int _23701 = NOVALUE;
    int _23700 = NOVALUE;
    int _23697 = NOVALUE;
    int _23696 = NOVALUE;
    int _23695 = NOVALUE;
    int _23694 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal("c", fileext(filename)) then*/
    RefDS(_filename_44367);
    _23694 = _13fileext(_filename_44367);
    if (_23693 == _23694)
    _23695 = 1;
    else if (IS_ATOM_INT(_23693) && IS_ATOM_INT(_23694))
    _23695 = 0;
    else
    _23695 = (compare(_23693, _23694) == 0);
    DeRef(_23694);
    _23694 = NOVALUE;
    if (_23695 == 0)
    {
        _23695 = NOVALUE;
        goto L1; // [15] 35
    }
    else{
        _23695 = NOVALUE;
    }

    /** 		filename = filename[1..$-2]*/
    if (IS_SEQUENCE(_filename_44367)){
            _23696 = SEQ_PTR(_filename_44367)->length;
    }
    else {
        _23696 = 1;
    }
    _23697 = _23696 - 2;
    _23696 = NOVALUE;
    rhs_slice_target = (object_ptr)&_filename_44367;
    RHS_Slice(_filename_44367, 1, _23697);
    goto L2; // [32] 82
L1: 

    /** 	elsif equal("h", fileext(filename)) then*/
    RefDS(_filename_44367);
    _23700 = _13fileext(_filename_44367);
    if (_23699 == _23700)
    _23701 = 1;
    else if (IS_ATOM_INT(_23699) && IS_ATOM_INT(_23700))
    _23701 = 0;
    else
    _23701 = (compare(_23699, _23700) == 0);
    DeRef(_23700);
    _23700 = NOVALUE;
    if (_23701 == 0)
    {
        _23701 = NOVALUE;
        goto L3; // [45] 81
    }
    else{
        _23701 = NOVALUE;
    }

    /** 		generated_files = append(generated_files, filename)*/
    RefDS(_filename_44367);
    Append(&_59generated_files_42612, _59generated_files_42612, _filename_44367);

    /** 		if build_system_type = BUILD_DIRECT then*/

    /** 			outdated_files  = append(outdated_files, 0)*/
    Append(&_59outdated_files_42613, _59outdated_files_42613, 0);

    /** 		return*/
    DeRefDS(_filename_44367);
    DeRefDS(_eu_filename_44368);
    DeRef(_obj_fname_44388);
    DeRef(_src_fname_44389);
    DeRef(_23697);
    _23697 = NOVALUE;
    return;
L3: 
L2: 

    /** 	sequence obj_fname = filename, src_fname = filename & ".c"*/
    RefDS(_filename_44367);
    DeRef(_obj_fname_44388);
    _obj_fname_44388 = _filename_44367;
    Concat((object_ptr)&_src_fname_44389, _filename_44367, _23664);

    /** 	if compiler_type = COMPILER_WATCOM then*/

    /** 		obj_fname &= ".o"*/
    Concat((object_ptr)&_obj_fname_44388, _obj_fname_44388, _23709);

    /** 	generated_files = append(generated_files, src_fname)*/
    RefDS(_src_fname_44389);
    Append(&_59generated_files_42612, _59generated_files_42612, _src_fname_44389);

    /** 	generated_files = append(generated_files, obj_fname)*/
    RefDS(_obj_fname_44388);
    Append(&_59generated_files_42612, _59generated_files_42612, _obj_fname_44388);

    /** 	if build_system_type = BUILD_DIRECT then*/

    /** 		outdated_files  = append(outdated_files, is_file_newer(eu_filename, output_dir & src_fname))*/
    Concat((object_ptr)&_23714, _59output_dir_42630, _src_fname_44389);
    RefDS(_eu_filename_44368);
    _23715 = _59is_file_newer(_eu_filename_44368, _23714);
    _23714 = NOVALUE;
    Ref(_23715);
    Append(&_59outdated_files_42613, _59outdated_files_42613, _23715);
    DeRef(_23715);
    _23715 = NOVALUE;

    /** 		outdated_files  = append(outdated_files, 0)*/
    Append(&_59outdated_files_42613, _59outdated_files_42613, 0);

    /** end procedure*/
    DeRefDS(_filename_44367);
    DeRefDS(_eu_filename_44368);
    DeRef(_obj_fname_44388);
    DeRef(_src_fname_44389);
    DeRef(_23697);
    _23697 = NOVALUE;
    return;
    ;
}


int _59any_code(int _file_no_44412)
{
    int _these_routines_44414 = NOVALUE;
    int _s_44421 = NOVALUE;
    int _23731 = NOVALUE;
    int _23730 = NOVALUE;
    int _23729 = NOVALUE;
    int _23728 = NOVALUE;
    int _23727 = NOVALUE;
    int _23726 = NOVALUE;
    int _23725 = NOVALUE;
    int _23724 = NOVALUE;
    int _23723 = NOVALUE;
    int _23722 = NOVALUE;
    int _23721 = NOVALUE;
    int _23719 = NOVALUE;
    int _0, _1, _2;
    

    /** 	check_file_routines()*/
    _59check_file_routines();

    /** 	sequence these_routines = file_routines[file_no]*/
    DeRef(_these_routines_44414);
    _2 = (int)SEQ_PTR(_59file_routines_44638);
    _these_routines_44414 = (int)*(((s1_ptr)_2)->base + _file_no_44412);
    Ref(_these_routines_44414);

    /** 	for i = 1 to length( these_routines ) do*/
    if (IS_SEQUENCE(_these_routines_44414)){
            _23719 = SEQ_PTR(_these_routines_44414)->length;
    }
    else {
        _23719 = 1;
    }
    {
        int _i_44418;
        _i_44418 = 1;
L1: 
        if (_i_44418 > _23719){
            goto L2; // [22] 126
        }

        /** 		symtab_index s = these_routines[i]*/
        _2 = (int)SEQ_PTR(_these_routines_44414);
        _s_44421 = (int)*(((s1_ptr)_2)->base + _i_44418);
        if (!IS_ATOM_INT(_s_44421)){
            _s_44421 = (long)DBL_PTR(_s_44421)->dbl;
        }

        /** 		if SymTab[s][S_FILE_NO] = file_no and*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23721 = (int)*(((s1_ptr)_2)->base + _s_44421);
        _2 = (int)SEQ_PTR(_23721);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _23722 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _23722 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        _23721 = NOVALUE;
        if (IS_ATOM_INT(_23722)) {
            _23723 = (_23722 == _file_no_44412);
        }
        else {
            _23723 = binary_op(EQUALS, _23722, _file_no_44412);
        }
        _23722 = NOVALUE;
        if (IS_ATOM_INT(_23723)) {
            if (_23723 == 0) {
                DeRef(_23724);
                _23724 = 0;
                goto L3; // [55] 81
            }
        }
        else {
            if (DBL_PTR(_23723)->dbl == 0.0) {
                DeRef(_23724);
                _23724 = 0;
                goto L3; // [55] 81
            }
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23725 = (int)*(((s1_ptr)_2)->base + _s_44421);
        _2 = (int)SEQ_PTR(_23725);
        _23726 = (int)*(((s1_ptr)_2)->base + 5);
        _23725 = NOVALUE;
        if (IS_ATOM_INT(_23726)) {
            _23727 = (_23726 != 99);
        }
        else {
            _23727 = binary_op(NOTEQ, _23726, 99);
        }
        _23726 = NOVALUE;
        DeRef(_23724);
        if (IS_ATOM_INT(_23727))
        _23724 = (_23727 != 0);
        else
        _23724 = DBL_PTR(_23727)->dbl != 0.0;
L3: 
        if (_23724 == 0) {
            goto L4; // [81] 117
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23729 = (int)*(((s1_ptr)_2)->base + _s_44421);
        _2 = (int)SEQ_PTR(_23729);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _23730 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _23730 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _23729 = NOVALUE;
        _23731 = find_from(_23730, _39RTN_TOKS_16547, 1);
        _23730 = NOVALUE;
        if (_23731 == 0)
        {
            _23731 = NOVALUE;
            goto L4; // [105] 117
        }
        else{
            _23731 = NOVALUE;
        }

        /** 			return TRUE -- found a non-deleted routine in this file*/
        DeRef(_these_routines_44414);
        DeRef(_23723);
        _23723 = NOVALUE;
        DeRef(_23727);
        _23727 = NOVALUE;
        return _9TRUE_428;
L4: 

        /** 	end for*/
        _i_44418 = _i_44418 + 1;
        goto L1; // [121] 29
L2: 
        ;
    }

    /** 	return FALSE*/
    DeRef(_these_routines_44414);
    DeRef(_23723);
    _23723 = NOVALUE;
    DeRef(_23727);
    _23727 = NOVALUE;
    return _9FALSE_426;
    ;
}


int _59legaldos_filename_char(int _i_44448)
{
    int _23746 = NOVALUE;
    int _23745 = NOVALUE;
    int _23742 = NOVALUE;
    int _23741 = NOVALUE;
    int _23740 = NOVALUE;
    int _23739 = NOVALUE;
    int _23738 = NOVALUE;
    int _23737 = NOVALUE;
    int _23736 = NOVALUE;
    int _23735 = NOVALUE;
    int _23734 = NOVALUE;
    int _23733 = NOVALUE;
    int _23732 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_i_44448)) {
        _1 = (long)(DBL_PTR(_i_44448)->dbl);
        if (UNIQUE(DBL_PTR(_i_44448)) && (DBL_PTR(_i_44448)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_44448);
        _i_44448 = _1;
    }

    /** 	if ('A' <= i and i <= 'Z') or*/
    _23732 = (65 <= _i_44448);
    if (_23732 == 0) {
        _23733 = 0;
        goto L1; // [9] 21
    }
    _23734 = (_i_44448 <= 90);
    _23733 = (_23734 != 0);
L1: 
    if (_23733 != 0) {
        _23735 = 1;
        goto L2; // [21] 45
    }
    _23736 = (48 <= _i_44448);
    if (_23736 == 0) {
        _23737 = 0;
        goto L3; // [29] 41
    }
    _23738 = (_i_44448 <= 57);
    _23737 = (_23738 != 0);
L3: 
    _23735 = (_23737 != 0);
L2: 
    if (_23735 != 0) {
        _23739 = 1;
        goto L4; // [45] 69
    }
    _23740 = (128 <= _i_44448);
    if (_23740 == 0) {
        _23741 = 0;
        goto L5; // [53] 65
    }
    _23742 = (_i_44448 <= 255);
    _23741 = (_23742 != 0);
L5: 
    _23739 = (_23741 != 0);
L4: 
    if (_23739 != 0) {
        goto L6; // [69] 87
    }
    _23745 = find_from(_i_44448, _23744, 1);
    _23746 = (_23745 != 0);
    _23745 = NOVALUE;
    if (_23746 == 0)
    {
        DeRef(_23746);
        _23746 = NOVALUE;
        goto L7; // [83] 96
    }
    else{
        DeRef(_23746);
        _23746 = NOVALUE;
    }
L6: 

    /** 		return 1*/
    DeRef(_23732);
    _23732 = NOVALUE;
    DeRef(_23734);
    _23734 = NOVALUE;
    DeRef(_23736);
    _23736 = NOVALUE;
    DeRef(_23738);
    _23738 = NOVALUE;
    DeRef(_23740);
    _23740 = NOVALUE;
    DeRef(_23742);
    _23742 = NOVALUE;
    return 1;
    goto L8; // [93] 103
L7: 

    /** 		return 0*/
    DeRef(_23732);
    _23732 = NOVALUE;
    DeRef(_23734);
    _23734 = NOVALUE;
    DeRef(_23736);
    _23736 = NOVALUE;
    DeRef(_23738);
    _23738 = NOVALUE;
    DeRef(_23740);
    _23740 = NOVALUE;
    DeRef(_23742);
    _23742 = NOVALUE;
    return 0;
L8: 
    ;
}


int _59legaldos_filename(int _s_44468)
{
    int _dloc_44469 = NOVALUE;
    int _23773 = NOVALUE;
    int _23772 = NOVALUE;
    int _23770 = NOVALUE;
    int _23769 = NOVALUE;
    int _23768 = NOVALUE;
    int _23767 = NOVALUE;
    int _23765 = NOVALUE;
    int _23764 = NOVALUE;
    int _23763 = NOVALUE;
    int _23762 = NOVALUE;
    int _23761 = NOVALUE;
    int _23760 = NOVALUE;
    int _23758 = NOVALUE;
    int _23757 = NOVALUE;
    int _23756 = NOVALUE;
    int _23755 = NOVALUE;
    int _23754 = NOVALUE;
    int _23753 = NOVALUE;
    int _23752 = NOVALUE;
    int _23750 = NOVALUE;
    int _23749 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if find( s, { "..", "." } ) then*/
    RefDS(_23748);
    RefDS(_23747);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23747;
    ((int *)_2)[2] = _23748;
    _23749 = MAKE_SEQ(_1);
    _23750 = find_from(_s_44468, _23749, 1);
    DeRefDS(_23749);
    _23749 = NOVALUE;
    if (_23750 == 0)
    {
        _23750 = NOVALUE;
        goto L1; // [14] 24
    }
    else{
        _23750 = NOVALUE;
    }

    /** 		return 1*/
    DeRefDS(_s_44468);
    return 1;
L1: 

    /** 	dloc = find('.',s)*/
    _dloc_44469 = find_from(46, _s_44468, 1);

    /** 	if dloc > 8 or ( dloc > 0 and dloc + 3 < length(s) ) or ( dloc = 0 and length(s) > 8 ) then*/
    _23752 = (_dloc_44469 > 8);
    if (_23752 != 0) {
        _23753 = 1;
        goto L2; // [37] 68
    }
    _23754 = (_dloc_44469 > 0);
    if (_23754 == 0) {
        _23755 = 0;
        goto L3; // [45] 64
    }
    _23756 = _dloc_44469 + 3;
    if (IS_SEQUENCE(_s_44468)){
            _23757 = SEQ_PTR(_s_44468)->length;
    }
    else {
        _23757 = 1;
    }
    _23758 = (_23756 < _23757);
    _23756 = NOVALUE;
    _23757 = NOVALUE;
    _23755 = (_23758 != 0);
L3: 
    _23753 = (_23755 != 0);
L2: 
    if (_23753 != 0) {
        goto L4; // [68] 96
    }
    _23760 = (_dloc_44469 == 0);
    if (_23760 == 0) {
        DeRef(_23761);
        _23761 = 0;
        goto L5; // [76] 91
    }
    if (IS_SEQUENCE(_s_44468)){
            _23762 = SEQ_PTR(_s_44468)->length;
    }
    else {
        _23762 = 1;
    }
    _23763 = (_23762 > 8);
    _23762 = NOVALUE;
    _23761 = (_23763 != 0);
L5: 
    if (_23761 == 0)
    {
        _23761 = NOVALUE;
        goto L6; // [92] 103
    }
    else{
        _23761 = NOVALUE;
    }
L4: 

    /** 		return 0*/
    DeRefDS(_s_44468);
    DeRef(_23752);
    _23752 = NOVALUE;
    DeRef(_23754);
    _23754 = NOVALUE;
    DeRef(_23760);
    _23760 = NOVALUE;
    DeRef(_23758);
    _23758 = NOVALUE;
    DeRef(_23763);
    _23763 = NOVALUE;
    return 0;
L6: 

    /** 	for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_44468)){
            _23764 = SEQ_PTR(_s_44468)->length;
    }
    else {
        _23764 = 1;
    }
    {
        int _i_44490;
        _i_44490 = 1;
L7: 
        if (_i_44490 > _23764){
            goto L8; // [108] 200
        }

        /** 		if s[i] = '.' then*/
        _2 = (int)SEQ_PTR(_s_44468);
        _23765 = (int)*(((s1_ptr)_2)->base + _i_44490);
        if (binary_op_a(NOTEQ, _23765, 46)){
            _23765 = NOVALUE;
            goto L9; // [121] 173
        }
        _23765 = NOVALUE;

        /** 			for j = i+1 to length(s) do*/
        _23767 = _i_44490 + 1;
        if (IS_SEQUENCE(_s_44468)){
                _23768 = SEQ_PTR(_s_44468)->length;
        }
        else {
            _23768 = 1;
        }
        {
            int _j_44496;
            _j_44496 = _23767;
LA: 
            if (_j_44496 > _23768){
                goto LB; // [134] 168
            }

            /** 				if not legaldos_filename_char(s[j]) then*/
            _2 = (int)SEQ_PTR(_s_44468);
            _23769 = (int)*(((s1_ptr)_2)->base + _j_44496);
            Ref(_23769);
            _23770 = _59legaldos_filename_char(_23769);
            _23769 = NOVALUE;
            if (IS_ATOM_INT(_23770)) {
                if (_23770 != 0){
                    DeRef(_23770);
                    _23770 = NOVALUE;
                    goto LC; // [151] 161
                }
            }
            else {
                if (DBL_PTR(_23770)->dbl != 0.0){
                    DeRef(_23770);
                    _23770 = NOVALUE;
                    goto LC; // [151] 161
                }
            }
            DeRef(_23770);
            _23770 = NOVALUE;

            /** 					return 0*/
            DeRefDS(_s_44468);
            DeRef(_23752);
            _23752 = NOVALUE;
            DeRef(_23754);
            _23754 = NOVALUE;
            DeRef(_23760);
            _23760 = NOVALUE;
            DeRef(_23758);
            _23758 = NOVALUE;
            DeRef(_23763);
            _23763 = NOVALUE;
            DeRef(_23767);
            _23767 = NOVALUE;
            return 0;
LC: 

            /** 			end for*/
            _j_44496 = _j_44496 + 1;
            goto LA; // [163] 141
LB: 
            ;
        }

        /** 			exit*/
        goto L8; // [170] 200
L9: 

        /** 		if not legaldos_filename_char(s[i]) then*/
        _2 = (int)SEQ_PTR(_s_44468);
        _23772 = (int)*(((s1_ptr)_2)->base + _i_44490);
        Ref(_23772);
        _23773 = _59legaldos_filename_char(_23772);
        _23772 = NOVALUE;
        if (IS_ATOM_INT(_23773)) {
            if (_23773 != 0){
                DeRef(_23773);
                _23773 = NOVALUE;
                goto LD; // [183] 193
            }
        }
        else {
            if (DBL_PTR(_23773)->dbl != 0.0){
                DeRef(_23773);
                _23773 = NOVALUE;
                goto LD; // [183] 193
            }
        }
        DeRef(_23773);
        _23773 = NOVALUE;

        /** 			return 0*/
        DeRefDS(_s_44468);
        DeRef(_23752);
        _23752 = NOVALUE;
        DeRef(_23754);
        _23754 = NOVALUE;
        DeRef(_23760);
        _23760 = NOVALUE;
        DeRef(_23758);
        _23758 = NOVALUE;
        DeRef(_23763);
        _23763 = NOVALUE;
        DeRef(_23767);
        _23767 = NOVALUE;
        return 0;
LD: 

        /** 	end for*/
        _i_44490 = _i_44490 + 1;
        goto L7; // [195] 115
L8: 
        ;
    }

    /** 	return 1*/
    DeRefDS(_s_44468);
    DeRef(_23752);
    _23752 = NOVALUE;
    DeRef(_23754);
    _23754 = NOVALUE;
    DeRef(_23760);
    _23760 = NOVALUE;
    DeRef(_23758);
    _23758 = NOVALUE;
    DeRef(_23763);
    _23763 = NOVALUE;
    DeRef(_23767);
    _23767 = NOVALUE;
    return 1;
    ;
}


int _59shrink_to_83(int _s_44509)
{
    int _dl_44510 = NOVALUE;
    int _sl_44511 = NOVALUE;
    int _osl_44512 = NOVALUE;
    int _se_44513 = NOVALUE;
    int _23854 = NOVALUE;
    int _23853 = NOVALUE;
    int _23852 = NOVALUE;
    int _23851 = NOVALUE;
    int _23850 = NOVALUE;
    int _23849 = NOVALUE;
    int _23848 = NOVALUE;
    int _23847 = NOVALUE;
    int _23846 = NOVALUE;
    int _23845 = NOVALUE;
    int _23843 = NOVALUE;
    int _23842 = NOVALUE;
    int _23841 = NOVALUE;
    int _23840 = NOVALUE;
    int _23838 = NOVALUE;
    int _23837 = NOVALUE;
    int _23836 = NOVALUE;
    int _23835 = NOVALUE;
    int _23832 = NOVALUE;
    int _23831 = NOVALUE;
    int _23830 = NOVALUE;
    int _23827 = NOVALUE;
    int _23826 = NOVALUE;
    int _23825 = NOVALUE;
    int _23823 = NOVALUE;
    int _23822 = NOVALUE;
    int _23821 = NOVALUE;
    int _23820 = NOVALUE;
    int _23818 = NOVALUE;
    int _23817 = NOVALUE;
    int _23815 = NOVALUE;
    int _23814 = NOVALUE;
    int _23812 = NOVALUE;
    int _23811 = NOVALUE;
    int _23810 = NOVALUE;
    int _23809 = NOVALUE;
    int _23808 = NOVALUE;
    int _23807 = NOVALUE;
    int _23806 = NOVALUE;
    int _23805 = NOVALUE;
    int _23804 = NOVALUE;
    int _23803 = NOVALUE;
    int _23801 = NOVALUE;
    int _23800 = NOVALUE;
    int _23799 = NOVALUE;
    int _23796 = NOVALUE;
    int _23795 = NOVALUE;
    int _23794 = NOVALUE;
    int _23793 = NOVALUE;
    int _23790 = NOVALUE;
    int _23789 = NOVALUE;
    int _23788 = NOVALUE;
    int _23786 = NOVALUE;
    int _23785 = NOVALUE;
    int _23784 = NOVALUE;
    int _23783 = NOVALUE;
    int _23781 = NOVALUE;
    int _23779 = NOVALUE;
    int _23778 = NOVALUE;
    int _23777 = NOVALUE;
    int _23776 = NOVALUE;
    int _0, _1, _2;
    

    /** 	osl = find( ':', s )*/
    _osl_44512 = find_from(58, _s_44509, 1);

    /** 	sl = osl + find( '\\', s[osl+1..$] ) -- find_from osl*/
    _23776 = _osl_44512 + 1;
    if (IS_SEQUENCE(_s_44509)){
            _23777 = SEQ_PTR(_s_44509)->length;
    }
    else {
        _23777 = 1;
    }
    rhs_slice_target = (object_ptr)&_23778;
    RHS_Slice(_s_44509, _23776, _23777);
    _23779 = find_from(92, _23778, 1);
    DeRefDS(_23778);
    _23778 = NOVALUE;
    _sl_44511 = _osl_44512 + _23779;
    _23779 = NOVALUE;

    /** 	if sl=osl+1 then*/
    _23781 = _osl_44512 + 1;
    if (_sl_44511 != _23781)
    goto L1; // [39] 49

    /** 		osl = sl*/
    _osl_44512 = _sl_44511;
L1: 

    /** 	sl = osl + find( '\\', s[osl+1..$] )*/
    _23783 = _osl_44512 + 1;
    if (_23783 > MAXINT){
        _23783 = NewDouble((double)_23783);
    }
    if (IS_SEQUENCE(_s_44509)){
            _23784 = SEQ_PTR(_s_44509)->length;
    }
    else {
        _23784 = 1;
    }
    rhs_slice_target = (object_ptr)&_23785;
    RHS_Slice(_s_44509, _23783, _23784);
    _23786 = find_from(92, _23785, 1);
    DeRefDS(_23785);
    _23785 = NOVALUE;
    _sl_44511 = _osl_44512 + _23786;
    _23786 = NOVALUE;

    /** 	dl = osl + find( '.', s[osl+1..sl] )*/
    _23788 = _osl_44512 + 1;
    rhs_slice_target = (object_ptr)&_23789;
    RHS_Slice(_s_44509, _23788, _sl_44511);
    _23790 = find_from(46, _23789, 1);
    DeRefDS(_23789);
    _23789 = NOVALUE;
    _dl_44510 = _osl_44512 + _23790;
    _23790 = NOVALUE;

    /** 	if dl > osl then*/
    if (_dl_44510 <= _osl_44512)
    goto L2; // [94] 124

    /** 		se = s[dl..min({dl+3,sl-1})]*/
    _23793 = _dl_44510 + 3;
    if ((long)((unsigned long)_23793 + (unsigned long)HIGH_BITS) >= 0) 
    _23793 = NewDouble((double)_23793);
    _23794 = _sl_44511 - 1;
    if ((long)((unsigned long)_23794 +(unsigned long) HIGH_BITS) >= 0){
        _23794 = NewDouble((double)_23794);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23793;
    ((int *)_2)[2] = _23794;
    _23795 = MAKE_SEQ(_1);
    _23794 = NOVALUE;
    _23793 = NOVALUE;
    _23796 = _18min(_23795);
    _23795 = NOVALUE;
    rhs_slice_target = (object_ptr)&_se_44513;
    RHS_Slice(_s_44509, _dl_44510, _23796);
    goto L3; // [121] 132
L2: 

    /** 		se = ""*/
    RefDS(_22663);
    DeRef(_se_44513);
    _se_44513 = _22663;
L3: 

    /** 	while sl != osl do*/
L4: 
    if (_sl_44511 == _osl_44512)
    goto L5; // [137] 333

    /** 		if find( ' ', s[osl+1..sl] ) or not legaldos_filename(upper(s[osl+1..sl-1])) then*/
    _23799 = _osl_44512 + 1;
    rhs_slice_target = (object_ptr)&_23800;
    RHS_Slice(_s_44509, _23799, _sl_44511);
    _23801 = find_from(32, _23800, 1);
    DeRefDS(_23800);
    _23800 = NOVALUE;
    if (_23801 != 0) {
        goto L6; // [157] 190
    }
    _23803 = _osl_44512 + 1;
    if (_23803 > MAXINT){
        _23803 = NewDouble((double)_23803);
    }
    _23804 = _sl_44511 - 1;
    rhs_slice_target = (object_ptr)&_23805;
    RHS_Slice(_s_44509, _23803, _23804);
    _23806 = _10upper(_23805);
    _23805 = NOVALUE;
    _23807 = _59legaldos_filename(_23806);
    _23806 = NOVALUE;
    if (IS_ATOM_INT(_23807)) {
        _23808 = (_23807 == 0);
    }
    else {
        _23808 = unary_op(NOT, _23807);
    }
    DeRef(_23807);
    _23807 = NOVALUE;
    if (_23808 == 0) {
        DeRef(_23808);
        _23808 = NOVALUE;
        goto L7; // [186] 244
    }
    else {
        if (!IS_ATOM_INT(_23808) && DBL_PTR(_23808)->dbl == 0.0){
            DeRef(_23808);
            _23808 = NOVALUE;
            goto L7; // [186] 244
        }
        DeRef(_23808);
        _23808 = NOVALUE;
    }
    DeRef(_23808);
    _23808 = NOVALUE;
L6: 

    /** 			s = s[1..osl] & s[osl+1..osl+6] & "~1" & se & s[sl..$]*/
    rhs_slice_target = (object_ptr)&_23809;
    RHS_Slice(_s_44509, 1, _osl_44512);
    _23810 = _osl_44512 + 1;
    if (_23810 > MAXINT){
        _23810 = NewDouble((double)_23810);
    }
    _23811 = _osl_44512 + 6;
    rhs_slice_target = (object_ptr)&_23812;
    RHS_Slice(_s_44509, _23810, _23811);
    if (IS_SEQUENCE(_s_44509)){
            _23814 = SEQ_PTR(_s_44509)->length;
    }
    else {
        _23814 = 1;
    }
    rhs_slice_target = (object_ptr)&_23815;
    RHS_Slice(_s_44509, _sl_44511, _23814);
    {
        int concat_list[5];

        concat_list[0] = _23815;
        concat_list[1] = _se_44513;
        concat_list[2] = _23813;
        concat_list[3] = _23812;
        concat_list[4] = _23809;
        Concat_N((object_ptr)&_s_44509, concat_list, 5);
    }
    DeRefDS(_23815);
    _23815 = NOVALUE;
    DeRefDS(_23812);
    _23812 = NOVALUE;
    DeRefDS(_23809);
    _23809 = NOVALUE;

    /** 			sl = osl+8+length(se)*/
    _23817 = _osl_44512 + 8;
    if ((long)((unsigned long)_23817 + (unsigned long)HIGH_BITS) >= 0) 
    _23817 = NewDouble((double)_23817);
    if (IS_SEQUENCE(_se_44513)){
            _23818 = SEQ_PTR(_se_44513)->length;
    }
    else {
        _23818 = 1;
    }
    if (IS_ATOM_INT(_23817)) {
        _sl_44511 = _23817 + _23818;
    }
    else {
        _sl_44511 = NewDouble(DBL_PTR(_23817)->dbl + (double)_23818);
    }
    DeRef(_23817);
    _23817 = NOVALUE;
    _23818 = NOVALUE;
    if (!IS_ATOM_INT(_sl_44511)) {
        _1 = (long)(DBL_PTR(_sl_44511)->dbl);
        if (UNIQUE(DBL_PTR(_sl_44511)) && (DBL_PTR(_sl_44511)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sl_44511);
        _sl_44511 = _1;
    }
L7: 

    /** 		osl = sl*/
    _osl_44512 = _sl_44511;

    /** 		sl += find( '\\', s[sl+1..$] )*/
    _23820 = _sl_44511 + 1;
    if (_23820 > MAXINT){
        _23820 = NewDouble((double)_23820);
    }
    if (IS_SEQUENCE(_s_44509)){
            _23821 = SEQ_PTR(_s_44509)->length;
    }
    else {
        _23821 = 1;
    }
    rhs_slice_target = (object_ptr)&_23822;
    RHS_Slice(_s_44509, _23820, _23821);
    _23823 = find_from(92, _23822, 1);
    DeRefDS(_23822);
    _23822 = NOVALUE;
    _sl_44511 = _sl_44511 + _23823;
    _23823 = NOVALUE;

    /** 		dl = osl + find( '.', s[osl+1..sl] )*/
    _23825 = _osl_44512 + 1;
    rhs_slice_target = (object_ptr)&_23826;
    RHS_Slice(_s_44509, _23825, _sl_44511);
    _23827 = find_from(46, _23826, 1);
    DeRefDS(_23826);
    _23826 = NOVALUE;
    _dl_44510 = _osl_44512 + _23827;
    _23827 = NOVALUE;

    /** 		if dl > osl then*/
    if (_dl_44510 <= _osl_44512)
    goto L8; // [294] 320

    /** 			se = s[dl..min({dl+3,sl})]*/
    _23830 = _dl_44510 + 3;
    if ((long)((unsigned long)_23830 + (unsigned long)HIGH_BITS) >= 0) 
    _23830 = NewDouble((double)_23830);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23830;
    ((int *)_2)[2] = _sl_44511;
    _23831 = MAKE_SEQ(_1);
    _23830 = NOVALUE;
    _23832 = _18min(_23831);
    _23831 = NOVALUE;
    rhs_slice_target = (object_ptr)&_se_44513;
    RHS_Slice(_s_44509, _dl_44510, _23832);
    goto L4; // [317] 137
L8: 

    /** 			se = ""*/
    RefDS(_22663);
    DeRef(_se_44513);
    _se_44513 = _22663;

    /** 	end while*/
    goto L4; // [330] 137
L5: 

    /** 	if dl > osl then*/
    if (_dl_44510 <= _osl_44512)
    goto L9; // [335] 362

    /** 		se = s[dl..min({dl+3,length(s)})]*/
    _23835 = _dl_44510 + 3;
    if ((long)((unsigned long)_23835 + (unsigned long)HIGH_BITS) >= 0) 
    _23835 = NewDouble((double)_23835);
    if (IS_SEQUENCE(_s_44509)){
            _23836 = SEQ_PTR(_s_44509)->length;
    }
    else {
        _23836 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _23835;
    ((int *)_2)[2] = _23836;
    _23837 = MAKE_SEQ(_1);
    _23836 = NOVALUE;
    _23835 = NOVALUE;
    _23838 = _18min(_23837);
    _23837 = NOVALUE;
    rhs_slice_target = (object_ptr)&_se_44513;
    RHS_Slice(_s_44509, _dl_44510, _23838);
L9: 

    /** 	if find( ' ', s[osl+1..$] ) or not legaldos_filename(upper(s[osl+1..$])) then*/
    _23840 = _osl_44512 + 1;
    if (_23840 > MAXINT){
        _23840 = NewDouble((double)_23840);
    }
    if (IS_SEQUENCE(_s_44509)){
            _23841 = SEQ_PTR(_s_44509)->length;
    }
    else {
        _23841 = 1;
    }
    rhs_slice_target = (object_ptr)&_23842;
    RHS_Slice(_s_44509, _23840, _23841);
    _23843 = find_from(32, _23842, 1);
    DeRefDS(_23842);
    _23842 = NOVALUE;
    if (_23843 != 0) {
        goto LA; // [381] 413
    }
    _23845 = _osl_44512 + 1;
    if (_23845 > MAXINT){
        _23845 = NewDouble((double)_23845);
    }
    if (IS_SEQUENCE(_s_44509)){
            _23846 = SEQ_PTR(_s_44509)->length;
    }
    else {
        _23846 = 1;
    }
    rhs_slice_target = (object_ptr)&_23847;
    RHS_Slice(_s_44509, _23845, _23846);
    _23848 = _10upper(_23847);
    _23847 = NOVALUE;
    _23849 = _59legaldos_filename(_23848);
    _23848 = NOVALUE;
    if (IS_ATOM_INT(_23849)) {
        _23850 = (_23849 == 0);
    }
    else {
        _23850 = unary_op(NOT, _23849);
    }
    DeRef(_23849);
    _23849 = NOVALUE;
    if (_23850 == 0) {
        DeRef(_23850);
        _23850 = NOVALUE;
        goto LB; // [409] 443
    }
    else {
        if (!IS_ATOM_INT(_23850) && DBL_PTR(_23850)->dbl == 0.0){
            DeRef(_23850);
            _23850 = NOVALUE;
            goto LB; // [409] 443
        }
        DeRef(_23850);
        _23850 = NOVALUE;
    }
    DeRef(_23850);
    _23850 = NOVALUE;
LA: 

    /** 		s = s[1..osl] & s[osl+1..osl+6] & "~1" & se*/
    rhs_slice_target = (object_ptr)&_23851;
    RHS_Slice(_s_44509, 1, _osl_44512);
    _23852 = _osl_44512 + 1;
    if (_23852 > MAXINT){
        _23852 = NewDouble((double)_23852);
    }
    _23853 = _osl_44512 + 6;
    rhs_slice_target = (object_ptr)&_23854;
    RHS_Slice(_s_44509, _23852, _23853);
    {
        int concat_list[4];

        concat_list[0] = _se_44513;
        concat_list[1] = _23813;
        concat_list[2] = _23854;
        concat_list[3] = _23851;
        Concat_N((object_ptr)&_s_44509, concat_list, 4);
    }
    DeRefDS(_23854);
    _23854 = NOVALUE;
    DeRefDS(_23851);
    _23851 = NOVALUE;
LB: 

    /** 	return s*/
    DeRef(_se_44513);
    DeRef(_23776);
    _23776 = NOVALUE;
    DeRef(_23781);
    _23781 = NOVALUE;
    DeRef(_23783);
    _23783 = NOVALUE;
    DeRef(_23788);
    _23788 = NOVALUE;
    DeRef(_23799);
    _23799 = NOVALUE;
    DeRef(_23796);
    _23796 = NOVALUE;
    DeRef(_23803);
    _23803 = NOVALUE;
    DeRef(_23804);
    _23804 = NOVALUE;
    DeRef(_23820);
    _23820 = NOVALUE;
    DeRef(_23810);
    _23810 = NOVALUE;
    DeRef(_23811);
    _23811 = NOVALUE;
    DeRef(_23825);
    _23825 = NOVALUE;
    DeRef(_23840);
    _23840 = NOVALUE;
    DeRef(_23832);
    _23832 = NOVALUE;
    DeRef(_23838);
    _23838 = NOVALUE;
    DeRef(_23845);
    _23845 = NOVALUE;
    DeRef(_23852);
    _23852 = NOVALUE;
    DeRef(_23853);
    _23853 = NOVALUE;
    return _s_44509;
    ;
}


int _59truncate_to_83(int _lfn_44611)
{
    int _dl_44612 = NOVALUE;
    int _23875 = NOVALUE;
    int _23874 = NOVALUE;
    int _23873 = NOVALUE;
    int _23872 = NOVALUE;
    int _23871 = NOVALUE;
    int _23870 = NOVALUE;
    int _23869 = NOVALUE;
    int _23868 = NOVALUE;
    int _23867 = NOVALUE;
    int _23866 = NOVALUE;
    int _23865 = NOVALUE;
    int _23864 = NOVALUE;
    int _23863 = NOVALUE;
    int _23862 = NOVALUE;
    int _23861 = NOVALUE;
    int _23860 = NOVALUE;
    int _23859 = NOVALUE;
    int _23858 = NOVALUE;
    int _23857 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer dl = find( '.', lfn )*/
    _dl_44612 = find_from(46, _lfn_44611, 1);

    /** 	if dl = 0 and length(lfn) > 8 then*/
    _23857 = (_dl_44612 == 0);
    if (_23857 == 0) {
        goto L1; // [16] 45
    }
    if (IS_SEQUENCE(_lfn_44611)){
            _23859 = SEQ_PTR(_lfn_44611)->length;
    }
    else {
        _23859 = 1;
    }
    _23860 = (_23859 > 8);
    _23859 = NOVALUE;
    if (_23860 == 0)
    {
        DeRef(_23860);
        _23860 = NOVALUE;
        goto L1; // [28] 45
    }
    else{
        DeRef(_23860);
        _23860 = NOVALUE;
    }

    /** 		return lfn[1..8]*/
    rhs_slice_target = (object_ptr)&_23861;
    RHS_Slice(_lfn_44611, 1, 8);
    DeRefDS(_lfn_44611);
    DeRef(_23857);
    _23857 = NOVALUE;
    return _23861;
    goto L2; // [42] 138
L1: 

    /** 	elsif dl = 0 and length(lfn) <= 8 then*/
    _23862 = (_dl_44612 == 0);
    if (_23862 == 0) {
        goto L3; // [51] 75
    }
    if (IS_SEQUENCE(_lfn_44611)){
            _23864 = SEQ_PTR(_lfn_44611)->length;
    }
    else {
        _23864 = 1;
    }
    _23865 = (_23864 <= 8);
    _23864 = NOVALUE;
    if (_23865 == 0)
    {
        DeRef(_23865);
        _23865 = NOVALUE;
        goto L3; // [63] 75
    }
    else{
        DeRef(_23865);
        _23865 = NOVALUE;
    }

    /** 		return lfn*/
    DeRef(_23857);
    _23857 = NOVALUE;
    DeRef(_23861);
    _23861 = NOVALUE;
    DeRef(_23862);
    _23862 = NOVALUE;
    return _lfn_44611;
    goto L2; // [72] 138
L3: 

    /** 	elsif dl > 9 and dl + 3 <= length(lfn) then*/
    _23866 = (_dl_44612 > 9);
    if (_23866 == 0) {
        goto L4; // [81] 126
    }
    _23868 = _dl_44612 + 3;
    if ((long)((unsigned long)_23868 + (unsigned long)HIGH_BITS) >= 0) 
    _23868 = NewDouble((double)_23868);
    if (IS_SEQUENCE(_lfn_44611)){
            _23869 = SEQ_PTR(_lfn_44611)->length;
    }
    else {
        _23869 = 1;
    }
    if (IS_ATOM_INT(_23868)) {
        _23870 = (_23868 <= _23869);
    }
    else {
        _23870 = (DBL_PTR(_23868)->dbl <= (double)_23869);
    }
    DeRef(_23868);
    _23868 = NOVALUE;
    _23869 = NOVALUE;
    if (_23870 == 0)
    {
        DeRef(_23870);
        _23870 = NOVALUE;
        goto L4; // [97] 126
    }
    else{
        DeRef(_23870);
        _23870 = NOVALUE;
    }

    /** 		return lfn[1..8] & lfn[dl..$]*/
    rhs_slice_target = (object_ptr)&_23871;
    RHS_Slice(_lfn_44611, 1, 8);
    if (IS_SEQUENCE(_lfn_44611)){
            _23872 = SEQ_PTR(_lfn_44611)->length;
    }
    else {
        _23872 = 1;
    }
    rhs_slice_target = (object_ptr)&_23873;
    RHS_Slice(_lfn_44611, _dl_44612, _23872);
    Concat((object_ptr)&_23874, _23871, _23873);
    DeRefDS(_23871);
    _23871 = NOVALUE;
    DeRef(_23871);
    _23871 = NOVALUE;
    DeRefDS(_23873);
    _23873 = NOVALUE;
    DeRefDS(_lfn_44611);
    DeRef(_23857);
    _23857 = NOVALUE;
    DeRef(_23861);
    _23861 = NOVALUE;
    DeRef(_23862);
    _23862 = NOVALUE;
    DeRef(_23866);
    _23866 = NOVALUE;
    return _23874;
    goto L2; // [123] 138
L4: 

    /** 		CompileErr( 48, {lfn})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_lfn_44611);
    *((int *)(_2+4)) = _lfn_44611;
    _23875 = MAKE_SEQ(_1);
    _46CompileErr(48, _23875, 0);
    _23875 = NOVALUE;
L2: 
    ;
}


void _59check_file_routines()
{
    int _s_44647 = NOVALUE;
    int _23893 = NOVALUE;
    int _23892 = NOVALUE;
    int _23891 = NOVALUE;
    int _23890 = NOVALUE;
    int _23889 = NOVALUE;
    int _23888 = NOVALUE;
    int _23887 = NOVALUE;
    int _23886 = NOVALUE;
    int _23885 = NOVALUE;
    int _23884 = NOVALUE;
    int _23883 = NOVALUE;
    int _23882 = NOVALUE;
    int _23880 = NOVALUE;
    int _23878 = NOVALUE;
    int _23876 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length( file_routines ) then*/
    if (IS_SEQUENCE(_59file_routines_44638)){
            _23876 = SEQ_PTR(_59file_routines_44638)->length;
    }
    else {
        _23876 = 1;
    }
    if (_23876 != 0)
    goto L1; // [8] 146
    _23876 = NOVALUE;

    /** 		file_routines = repeat( {}, length( known_files ) )*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _23878 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _23878 = 1;
    }
    DeRefDS(_59file_routines_44638);
    _59file_routines_44638 = Repeat(_22663, _23878);
    _23878 = NOVALUE;

    /** 		integer s = SymTab[TopLevelSub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23880 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
    _2 = (int)SEQ_PTR(_23880);
    _s_44647 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44647)){
        _s_44647 = (long)DBL_PTR(_s_44647)->dbl;
    }
    _23880 = NOVALUE;

    /** 		while s do*/
L2: 
    if (_s_44647 == 0)
    {
        goto L3; // [45] 145
    }
    else{
    }

    /** 			if SymTab[s][S_USAGE] != U_DELETED and*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23882 = (int)*(((s1_ptr)_2)->base + _s_44647);
    _2 = (int)SEQ_PTR(_23882);
    _23883 = (int)*(((s1_ptr)_2)->base + 5);
    _23882 = NOVALUE;
    if (IS_ATOM_INT(_23883)) {
        _23884 = (_23883 != 99);
    }
    else {
        _23884 = binary_op(NOTEQ, _23883, 99);
    }
    _23883 = NOVALUE;
    if (IS_ATOM_INT(_23884)) {
        if (_23884 == 0) {
            goto L4; // [68] 124
        }
    }
    else {
        if (DBL_PTR(_23884)->dbl == 0.0) {
            goto L4; // [68] 124
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23886 = (int)*(((s1_ptr)_2)->base + _s_44647);
    _2 = (int)SEQ_PTR(_23886);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _23887 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _23887 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _23886 = NOVALUE;
    _23888 = find_from(_23887, _39RTN_TOKS_16547, 1);
    _23887 = NOVALUE;
    if (_23888 == 0)
    {
        _23888 = NOVALUE;
        goto L4; // [92] 124
    }
    else{
        _23888 = NOVALUE;
    }

    /** 				file_routines[SymTab[s][S_FILE_NO]] &= s*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23889 = (int)*(((s1_ptr)_2)->base + _s_44647);
    _2 = (int)SEQ_PTR(_23889);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _23890 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _23890 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    _23889 = NOVALUE;
    _2 = (int)SEQ_PTR(_59file_routines_44638);
    if (!IS_ATOM_INT(_23890)){
        _23891 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_23890)->dbl));
    }
    else{
        _23891 = (int)*(((s1_ptr)_2)->base + _23890);
    }
    if (IS_SEQUENCE(_23891) && IS_ATOM(_s_44647)) {
        Append(&_23892, _23891, _s_44647);
    }
    else if (IS_ATOM(_23891) && IS_SEQUENCE(_s_44647)) {
    }
    else {
        Concat((object_ptr)&_23892, _23891, _s_44647);
        _23891 = NOVALUE;
    }
    _23891 = NOVALUE;
    _2 = (int)SEQ_PTR(_59file_routines_44638);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _59file_routines_44638 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_23890))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_23890)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _23890);
    _1 = *(int *)_2;
    *(int *)_2 = _23892;
    if( _1 != _23892 ){
        DeRef(_1);
    }
    _23892 = NOVALUE;
L4: 

    /** 			s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _23893 = (int)*(((s1_ptr)_2)->base + _s_44647);
    _2 = (int)SEQ_PTR(_23893);
    _s_44647 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_44647)){
        _s_44647 = (long)DBL_PTR(_s_44647)->dbl;
    }
    _23893 = NOVALUE;

    /** 		end while*/
    goto L2; // [142] 45
L3: 
L1: 

    /** end procedure*/
    _23890 = NOVALUE;
    DeRef(_23884);
    _23884 = NOVALUE;
    return;
    ;
}


void _59GenerateUserRoutines()
{
    int _s_44681 = NOVALUE;
    int _sp_44682 = NOVALUE;
    int _next_c_char_44683 = NOVALUE;
    int _q_44684 = NOVALUE;
    int _temps_44685 = NOVALUE;
    int _buff_44686 = NOVALUE;
    int _base_name_44687 = NOVALUE;
    int _long_c_file_44688 = NOVALUE;
    int _c_file_44689 = NOVALUE;
    int _these_routines_44756 = NOVALUE;
    int _ret_type_44814 = NOVALUE;
    int _scope_44885 = NOVALUE;
    int _names_44919 = NOVALUE;
    int _name_44929 = NOVALUE;
    int _24106 = NOVALUE;
    int _24104 = NOVALUE;
    int _24103 = NOVALUE;
    int _24073 = NOVALUE;
    int _24072 = NOVALUE;
    int _24071 = NOVALUE;
    int _24069 = NOVALUE;
    int _24067 = NOVALUE;
    int _24066 = NOVALUE;
    int _24065 = NOVALUE;
    int _24064 = NOVALUE;
    int _24063 = NOVALUE;
    int _24062 = NOVALUE;
    int _24061 = NOVALUE;
    int _24059 = NOVALUE;
    int _24058 = NOVALUE;
    int _24057 = NOVALUE;
    int _24056 = NOVALUE;
    int _24055 = NOVALUE;
    int _24054 = NOVALUE;
    int _24053 = NOVALUE;
    int _24052 = NOVALUE;
    int _24050 = NOVALUE;
    int _24049 = NOVALUE;
    int _24047 = NOVALUE;
    int _24046 = NOVALUE;
    int _24045 = NOVALUE;
    int _24044 = NOVALUE;
    int _24043 = NOVALUE;
    int _24042 = NOVALUE;
    int _24041 = NOVALUE;
    int _24040 = NOVALUE;
    int _24038 = NOVALUE;
    int _24037 = NOVALUE;
    int _24035 = NOVALUE;
    int _24034 = NOVALUE;
    int _24033 = NOVALUE;
    int _24031 = NOVALUE;
    int _24028 = NOVALUE;
    int _24027 = NOVALUE;
    int _24025 = NOVALUE;
    int _24023 = NOVALUE;
    int _24017 = NOVALUE;
    int _24016 = NOVALUE;
    int _24015 = NOVALUE;
    int _24014 = NOVALUE;
    int _24013 = NOVALUE;
    int _24012 = NOVALUE;
    int _24011 = NOVALUE;
    int _24010 = NOVALUE;
    int _24008 = NOVALUE;
    int _24007 = NOVALUE;
    int _24005 = NOVALUE;
    int _24004 = NOVALUE;
    int _24001 = NOVALUE;
    int _23999 = NOVALUE;
    int _23998 = NOVALUE;
    int _23997 = NOVALUE;
    int _23993 = NOVALUE;
    int _23990 = NOVALUE;
    int _23987 = NOVALUE;
    int _23986 = NOVALUE;
    int _23985 = NOVALUE;
    int _23984 = NOVALUE;
    int _23982 = NOVALUE;
    int _23981 = NOVALUE;
    int _23979 = NOVALUE;
    int _23978 = NOVALUE;
    int _23977 = NOVALUE;
    int _23975 = NOVALUE;
    int _23972 = NOVALUE;
    int _23971 = NOVALUE;
    int _23970 = NOVALUE;
    int _23969 = NOVALUE;
    int _23968 = NOVALUE;
    int _23967 = NOVALUE;
    int _23965 = NOVALUE;
    int _23964 = NOVALUE;
    int _23962 = NOVALUE;
    int _23959 = NOVALUE;
    int _23958 = NOVALUE;
    int _23954 = NOVALUE;
    int _23953 = NOVALUE;
    int _23951 = NOVALUE;
    int _23947 = NOVALUE;
    int _23946 = NOVALUE;
    int _23945 = NOVALUE;
    int _23944 = NOVALUE;
    int _23943 = NOVALUE;
    int _23942 = NOVALUE;
    int _23941 = NOVALUE;
    int _23940 = NOVALUE;
    int _23939 = NOVALUE;
    int _23938 = NOVALUE;
    int _23937 = NOVALUE;
    int _23936 = NOVALUE;
    int _23935 = NOVALUE;
    int _23934 = NOVALUE;
    int _23932 = NOVALUE;
    int _23931 = NOVALUE;
    int _23929 = NOVALUE;
    int _23926 = NOVALUE;
    int _23923 = NOVALUE;
    int _23920 = NOVALUE;
    int _23917 = NOVALUE;
    int _23916 = NOVALUE;
    int _23915 = NOVALUE;
    int _23912 = NOVALUE;
    int _23909 = NOVALUE;
    int _23907 = NOVALUE;
    int _23903 = NOVALUE;
    int _23902 = NOVALUE;
    int _23900 = NOVALUE;
    int _23899 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer next_c_char, q, temps*/

    /** 	sequence buff, base_name, long_c_file, c_file*/

    /** 	if not silent then*/
    if (_38silent_17081 != 0)
    goto L1; // [9] 62

    /** 		if Pass = 1 then*/
    if (_59Pass_42592 != 1)
    goto L2; // [16] 29

    /** 			ShowMsg(1, 239,,0)*/
    RefDS(_22663);
    _47ShowMsg(1, 239, _22663, 0);
L2: 

    /** 		if LAST_PASS = TRUE then*/
    if (_59LAST_PASS_42590 != _9TRUE_428)
    goto L3; // [35] 50

    /** 			ShowMsg(1, 240)*/
    RefDS(_22663);
    _47ShowMsg(1, 240, _22663, 1);
    goto L4; // [47] 61
L3: 

    /** 			ShowMsg(1, 241, Pass, 0)*/
    _47ShowMsg(1, 241, _59Pass_42592, 0);
L4: 
L1: 

    /** 	check_file_routines()*/
    _59check_file_routines();

    /** 	c_puts("// GenerateUserRoutines\n")*/
    RefDS(_23898);
    _56c_puts(_23898);

    /** 	for file_no = 1 to length(known_files) do*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _23899 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _23899 = 1;
    }
    {
        int _file_no_44705;
        _file_no_44705 = 1;
L5: 
        if (_file_no_44705 > _23899){
            goto L6; // [78] 2060
        }

        /** 		if file_no = 1 or any_code(file_no) then*/
        _23900 = (_file_no_44705 == 1);
        if (_23900 != 0) {
            goto L7; // [91] 104
        }
        _23902 = _59any_code(_file_no_44705);
        if (_23902 == 0) {
            DeRef(_23902);
            _23902 = NOVALUE;
            goto L8; // [100] 2051
        }
        else {
            if (!IS_ATOM_INT(_23902) && DBL_PTR(_23902)->dbl == 0.0){
                DeRef(_23902);
                _23902 = NOVALUE;
                goto L8; // [100] 2051
            }
            DeRef(_23902);
            _23902 = NOVALUE;
        }
        DeRef(_23902);
        _23902 = NOVALUE;
L7: 

        /** 			next_c_char = 1*/
        _next_c_char_44683 = 1;

        /** 			base_name = name_ext(known_files[file_no])*/
        _2 = (int)SEQ_PTR(_35known_files_15596);
        _23903 = (int)*(((s1_ptr)_2)->base + _file_no_44705);
        Ref(_23903);
        _0 = _base_name_44687;
        _base_name_44687 = _55name_ext(_23903);
        DeRef(_0);
        _23903 = NOVALUE;

        /** 			c_file = base_name*/
        RefDS(_base_name_44687);
        DeRef(_c_file_44689);
        _c_file_44689 = _base_name_44687;

        /** 			q = length(c_file)*/
        if (IS_SEQUENCE(_c_file_44689)){
                _q_44684 = SEQ_PTR(_c_file_44689)->length;
        }
        else {
            _q_44684 = 1;
        }

        /** 			while q >= 1 do*/
L9: 
        if (_q_44684 < 1)
        goto LA; // [140] 181

        /** 				if c_file[q] = '.' then*/
        _2 = (int)SEQ_PTR(_c_file_44689);
        _23907 = (int)*(((s1_ptr)_2)->base + _q_44684);
        if (binary_op_a(NOTEQ, _23907, 46)){
            _23907 = NOVALUE;
            goto LB; // [150] 170
        }
        _23907 = NOVALUE;

        /** 					c_file = c_file[1..q-1]*/
        _23909 = _q_44684 - 1;
        rhs_slice_target = (object_ptr)&_c_file_44689;
        RHS_Slice(_c_file_44689, 1, _23909);

        /** 					exit*/
        goto LA; // [167] 181
LB: 

        /** 				q -= 1*/
        _q_44684 = _q_44684 - 1;

        /** 			end while*/
        goto L9; // [178] 140
LA: 

        /** 			if find(lower(c_file), {"main-", "init-"})  then*/
        RefDS(_c_file_44689);
        _23912 = _10lower(_c_file_44689);
        RefDS(_23914);
        RefDS(_23913);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _23913;
        ((int *)_2)[2] = _23914;
        _23915 = MAKE_SEQ(_1);
        _23916 = find_from(_23912, _23915, 1);
        DeRef(_23912);
        _23912 = NOVALUE;
        DeRefDS(_23915);
        _23915 = NOVALUE;
        if (_23916 == 0)
        {
            _23916 = NOVALUE;
            goto LC; // [196] 211
        }
        else{
            _23916 = NOVALUE;
        }

        /** 				CompileErr(12, {base_name})*/
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        RefDS(_base_name_44687);
        *((int *)(_2+4)) = _base_name_44687;
        _23917 = MAKE_SEQ(_1);
        _46CompileErr(12, _23917, 0);
        _23917 = NOVALUE;
LC: 

        /** 			long_c_file = c_file*/
        RefDS(_c_file_44689);
        DeRef(_long_c_file_44688);
        _long_c_file_44688 = _c_file_44689;

        /** 			if LAST_PASS = TRUE then*/
        if (_59LAST_PASS_42590 != _9TRUE_428)
        goto LD; // [224] 249

        /** 				c_file = unique_c_name(c_file)*/
        RefDS(_c_file_44689);
        _0 = _c_file_44689;
        _c_file_44689 = _59unique_c_name(_c_file_44689);
        DeRefDS(_0);

        /** 				add_file(c_file, known_files[file_no])*/
        _2 = (int)SEQ_PTR(_35known_files_15596);
        _23920 = (int)*(((s1_ptr)_2)->base + _file_no_44705);
        RefDS(_c_file_44689);
        Ref(_23920);
        _59add_file(_c_file_44689, _23920);
        _23920 = NOVALUE;
LD: 

        /** 			if file_no = 1 then*/
        if (_file_no_44705 != 1)
        goto LE; // [251] 314

        /** 				if LAST_PASS = TRUE then*/
        if (_59LAST_PASS_42590 != _9TRUE_428)
        goto LF; // [261] 306

        /** 					add_file("main-")*/
        RefDS(_23913);
        RefDS(_22663);
        _59add_file(_23913, _22663);

        /** 					for i = 0 to main_name_num-1 do*/
        _23923 = _56main_name_num_46449 - 1;
        if ((long)((unsigned long)_23923 +(unsigned long) HIGH_BITS) >= 0){
            _23923 = NewDouble((double)_23923);
        }
        {
            int _i_44746;
            _i_44746 = 0;
L10: 
            if (binary_op_a(GREATER, _i_44746, _23923)){
                goto L11; // [279] 305
            }

            /** 						buff = sprintf("main-%d", i)*/
            DeRefi(_buff_44686);
            _buff_44686 = EPrintf(-9999999, _23924, _i_44746);

            /** 						add_file(buff)*/
            RefDS(_buff_44686);
            RefDS(_22663);
            _59add_file(_buff_44686, _22663);

            /** 					end for*/
            _0 = _i_44746;
            if (IS_ATOM_INT(_i_44746)) {
                _i_44746 = _i_44746 + 1;
                if ((long)((unsigned long)_i_44746 +(unsigned long) HIGH_BITS) >= 0){
                    _i_44746 = NewDouble((double)_i_44746);
                }
            }
            else {
                _i_44746 = binary_op_a(PLUS, _i_44746, 1);
            }
            DeRef(_0);
            goto L10; // [300] 286
L11: 
            ;
            DeRef(_i_44746);
        }
LF: 

        /** 				file0 = long_c_file*/
        RefDS(_long_c_file_44688);
        DeRef(_59file0_44445);
        _59file0_44445 = _long_c_file_44688;
LE: 

        /** 			new_c_file(c_file)*/
        RefDS(_c_file_44689);
        _59new_c_file(_c_file_44689);

        /** 			s = SymTab[TopLevelSub][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _23926 = (int)*(((s1_ptr)_2)->base + _38TopLevelSub_16953);
        _2 = (int)SEQ_PTR(_23926);
        _s_44681 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_44681)){
            _s_44681 = (long)DBL_PTR(_s_44681)->dbl;
        }
        _23926 = NOVALUE;

        /** 			sequence these_routines = file_routines[file_no]*/
        DeRef(_these_routines_44756);
        _2 = (int)SEQ_PTR(_59file_routines_44638);
        _these_routines_44756 = (int)*(((s1_ptr)_2)->base + _file_no_44705);
        Ref(_these_routines_44756);

        /** 			for routine_no = 1 to length( these_routines ) do*/
        if (IS_SEQUENCE(_these_routines_44756)){
                _23929 = SEQ_PTR(_these_routines_44756)->length;
        }
        else {
            _23929 = 1;
        }
        {
            int _routine_no_44759;
            _routine_no_44759 = 1;
L12: 
            if (_routine_no_44759 > _23929){
                goto L13; // [352] 2050
            }

            /** 				s = these_routines[routine_no]*/
            _2 = (int)SEQ_PTR(_these_routines_44756);
            _s_44681 = (int)*(((s1_ptr)_2)->base + _routine_no_44759);
            if (!IS_ATOM_INT(_s_44681)){
                _s_44681 = (long)DBL_PTR(_s_44681)->dbl;
            }

            /** 				if SymTab[s][S_USAGE] != U_DELETED then*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23931 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_23931);
            _23932 = (int)*(((s1_ptr)_2)->base + 5);
            _23931 = NOVALUE;
            if (binary_op_a(EQUALS, _23932, 99)){
                _23932 = NOVALUE;
                goto L14; // [383] 2041
            }
            _23932 = NOVALUE;

            /** 					if LAST_PASS = TRUE and*/
            _23934 = (_59LAST_PASS_42590 == _9TRUE_428);
            if (_23934 == 0) {
                goto L15; // [397] 593
            }
            _23936 = (_38cfile_size_17028 > 100000);
            if (_23936 != 0) {
                DeRef(_23937);
                _23937 = 1;
                goto L16; // [409] 472
            }
            _23938 = (_s_44681 != _38TopLevelSub_16953);
            if (_23938 == 0) {
                _23939 = 0;
                goto L17; // [419] 439
            }
            _23940 = (100000 % 4) ? NewDouble((double)100000 / 4) : (100000 / 4);
            if (IS_ATOM_INT(_23940)) {
                _23941 = (_38cfile_size_17028 > _23940);
            }
            else {
                _23941 = ((double)_38cfile_size_17028 > DBL_PTR(_23940)->dbl);
            }
            DeRef(_23940);
            _23940 = NOVALUE;
            _23939 = (_23941 != 0);
L17: 
            if (_23939 == 0) {
                _23942 = 0;
                goto L18; // [439] 468
            }
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23943 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_23943);
            if (!IS_ATOM_INT(_38S_CODE_16610)){
                _23944 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
            }
            else{
                _23944 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
            }
            _23943 = NOVALUE;
            if (IS_SEQUENCE(_23944)){
                    _23945 = SEQ_PTR(_23944)->length;
            }
            else {
                _23945 = 1;
            }
            _23944 = NOVALUE;
            _23946 = (_23945 > 100000);
            _23945 = NOVALUE;
            _23942 = (_23946 != 0);
L18: 
            DeRef(_23937);
            _23937 = (_23942 != 0);
L16: 
            if (_23937 == 0)
            {
                _23937 = NOVALUE;
                goto L15; // [473] 593
            }
            else{
                _23937 = NOVALUE;
            }

            /** 						if length(c_file) = 7 then*/
            if (IS_SEQUENCE(_c_file_44689)){
                    _23947 = SEQ_PTR(_c_file_44689)->length;
            }
            else {
                _23947 = 1;
            }
            if (_23947 != 7)
            goto L19; // [481] 492

            /** 							c_file &= " "*/
            Concat((object_ptr)&_c_file_44689, _c_file_44689, _23949);
L19: 

            /** 						if length(c_file) >= 8 then*/
            if (IS_SEQUENCE(_c_file_44689)){
                    _23951 = SEQ_PTR(_c_file_44689)->length;
            }
            else {
                _23951 = 1;
            }
            if (_23951 < 8)
            goto L1A; // [497] 520

            /** 							c_file[7] = '_'*/
            _2 = (int)SEQ_PTR(_c_file_44689);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _c_file_44689 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 7);
            _1 = *(int *)_2;
            *(int *)_2 = 95;
            DeRef(_1);

            /** 							c_file[8] = file_chars[next_c_char]*/
            _2 = (int)SEQ_PTR(_59file_chars_44311);
            _23953 = (int)*(((s1_ptr)_2)->base + _next_c_char_44683);
            Ref(_23953);
            _2 = (int)SEQ_PTR(_c_file_44689);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _c_file_44689 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 8);
            _1 = *(int *)_2;
            *(int *)_2 = _23953;
            if( _1 != _23953 ){
                DeRef(_1);
            }
            _23953 = NOVALUE;
            goto L1B; // [517] 552
L1A: 

            /** 							if find('_', c_file) = 0 then*/
            _23954 = find_from(95, _c_file_44689, 1);
            if (_23954 != 0)
            goto L1C; // [527] 538

            /** 								c_file &= "_ "*/
            Concat((object_ptr)&_c_file_44689, _c_file_44689, _23956);
L1C: 

            /** 							c_file[$] = file_chars[next_c_char]*/
            if (IS_SEQUENCE(_c_file_44689)){
                    _23958 = SEQ_PTR(_c_file_44689)->length;
            }
            else {
                _23958 = 1;
            }
            _2 = (int)SEQ_PTR(_59file_chars_44311);
            _23959 = (int)*(((s1_ptr)_2)->base + _next_c_char_44683);
            Ref(_23959);
            _2 = (int)SEQ_PTR(_c_file_44689);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _c_file_44689 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _23958);
            _1 = *(int *)_2;
            *(int *)_2 = _23959;
            if( _1 != _23959 ){
                DeRef(_1);
            }
            _23959 = NOVALUE;
L1B: 

            /** 						c_file = unique_c_name(c_file)*/
            RefDS(_c_file_44689);
            _0 = _c_file_44689;
            _c_file_44689 = _59unique_c_name(_c_file_44689);
            DeRefDS(_0);

            /** 						new_c_file(c_file)*/
            RefDS(_c_file_44689);
            _59new_c_file(_c_file_44689);

            /** 						next_c_char += 1*/
            _next_c_char_44683 = _next_c_char_44683 + 1;

            /** 						if next_c_char > length(file_chars) then*/
            if (IS_SEQUENCE(_59file_chars_44311)){
                    _23962 = SEQ_PTR(_59file_chars_44311)->length;
            }
            else {
                _23962 = 1;
            }
            if (_next_c_char_44683 <= _23962)
            goto L1D; // [576] 586

            /** 							next_c_char = 1  -- (unique_c_name will resolve)*/
            _next_c_char_44683 = 1;
L1D: 

            /** 						add_file(c_file)*/
            RefDS(_c_file_44689);
            RefDS(_22663);
            _59add_file(_c_file_44689, _22663);
L15: 

            /** 					sequence ret_type*/

            /** 					if SymTab[s][S_TOKEN] = PROC then*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23964 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_23964);
            if (!IS_ATOM_INT(_38S_TOKEN_16603)){
                _23965 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
            }
            else{
                _23965 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
            }
            _23964 = NOVALUE;
            if (binary_op_a(NOTEQ, _23965, 27)){
                _23965 = NOVALUE;
                goto L1E; // [611] 625
            }
            _23965 = NOVALUE;

            /** 						ret_type = "void "*/
            RefDS(_23386);
            DeRefi(_ret_type_44814);
            _ret_type_44814 = _23386;
            goto L1F; // [622] 633
L1E: 

            /** 						ret_type = "int "*/
            RefDS(_23387);
            DeRefi(_ret_type_44814);
            _ret_type_44814 = _23387;
L1F: 

            /** 					if find( SymTab[s][S_SCOPE], {SC_GLOBAL, SC_EXPORT, SC_PUBLIC} ) and dll_option then*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23967 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_23967);
            _23968 = (int)*(((s1_ptr)_2)->base + 4);
            _23967 = NOVALUE;
            _1 = NewS1(3);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = 6;
            *((int *)(_2+8)) = 11;
            *((int *)(_2+12)) = 13;
            _23969 = MAKE_SEQ(_1);
            _23970 = find_from(_23968, _23969, 1);
            _23968 = NOVALUE;
            DeRefDS(_23969);
            _23969 = NOVALUE;
            if (_23970 == 0) {
                goto L20; // [664] 740
            }
            if (_59dll_option_42608 == 0)
            {
                goto L20; // [671] 740
            }
            else{
            }

            /** 						SymTab[s][S_RI_TARGET] = TRUE*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _35SymTab_15595 = MAKE_SEQ(_2);
            }
            _3 = (int)(_s_44681 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 53);
            _1 = *(int *)_2;
            *(int *)_2 = _9TRUE_428;
            DeRef(_1);
            _23972 = NOVALUE;

            /** 						LeftSym = TRUE*/
            _59LeftSym_42605 = _9TRUE_428;

            /** 						if TWINDOWS then*/
            if (_42TWINDOWS_17110 == 0)
            {
                goto L21; // [704] 723
            }
            else{
            }

            /** 							c_stmt(ret_type & " __stdcall @(", s)*/
            Concat((object_ptr)&_23975, _ret_type_44814, _23974);
            _59c_stmt(_23975, _s_44681, 0);
            _23975 = NOVALUE;
            goto L22; // [720] 763
L21: 

            /** 							c_stmt(ret_type & "@(", s)*/
            Concat((object_ptr)&_23977, _ret_type_44814, _23976);
            _59c_stmt(_23977, _s_44681, 0);
            _23977 = NOVALUE;
            goto L22; // [737] 763
L20: 

            /** 						LeftSym = TRUE*/
            _59LeftSym_42605 = _9TRUE_428;

            /** 						c_stmt( ret_type & "@(", s)*/
            Concat((object_ptr)&_23978, _ret_type_44814, _23976);
            _59c_stmt(_23978, _s_44681, 0);
            _23978 = NOVALUE;
L22: 

            /** 					sp = SymTab[s][S_NEXT]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23979 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_23979);
            _sp_44682 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44682)){
                _sp_44682 = (long)DBL_PTR(_sp_44682)->dbl;
            }
            _23979 = NOVALUE;

            /** 					for p = 1 to SymTab[s][S_NUM_ARGS] do*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23981 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_23981);
            if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
                _23982 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
            }
            else{
                _23982 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
            }
            _23981 = NOVALUE;
            {
                int _p_44855;
                _p_44855 = 1;
L23: 
                if (binary_op_a(GREATER, _p_44855, _23982)){
                    goto L24; // [793] 869
                }

                /** 						c_puts("int _")*/
                RefDS(_23983);
                _56c_puts(_23983);

                /** 						c_puts(SymTab[sp][S_NAME])*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _23984 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_23984);
                if (!IS_ATOM_INT(_38S_NAME_16598)){
                    _23985 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
                }
                else{
                    _23985 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
                }
                _23984 = NOVALUE;
                Ref(_23985);
                _56c_puts(_23985);
                _23985 = NOVALUE;

                /** 						if p != SymTab[s][S_NUM_ARGS] then*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _23986 = (int)*(((s1_ptr)_2)->base + _s_44681);
                _2 = (int)SEQ_PTR(_23986);
                if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
                    _23987 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
                }
                else{
                    _23987 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
                }
                _23986 = NOVALUE;
                if (binary_op_a(EQUALS, _p_44855, _23987)){
                    _23987 = NOVALUE;
                    goto L25; // [836] 846
                }
                _23987 = NOVALUE;

                /** 							c_puts(", ")*/
                RefDS(_23989);
                _56c_puts(_23989);
L25: 

                /** 						sp = SymTab[sp][S_NEXT]*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _23990 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_23990);
                _sp_44682 = (int)*(((s1_ptr)_2)->base + 2);
                if (!IS_ATOM_INT(_sp_44682)){
                    _sp_44682 = (long)DBL_PTR(_sp_44682)->dbl;
                }
                _23990 = NOVALUE;

                /** 					end for*/
                _0 = _p_44855;
                if (IS_ATOM_INT(_p_44855)) {
                    _p_44855 = _p_44855 + 1;
                    if ((long)((unsigned long)_p_44855 +(unsigned long) HIGH_BITS) >= 0){
                        _p_44855 = NewDouble((double)_p_44855);
                    }
                }
                else {
                    _p_44855 = binary_op_a(PLUS, _p_44855, 1);
                }
                DeRef(_0);
                goto L23; // [864] 800
L24: 
                ;
                DeRef(_p_44855);
            }

            /** 					c_puts(")\n")*/
            RefDS(_23992);
            _56c_puts(_23992);

            /** 					c_stmt0("{\n")*/
            RefDS(_22820);
            _59c_stmt0(_22820);

            /** 					NewBB(0, E_ALL_EFFECT, 0)*/
            _59NewBB(0, 1073741823, 0);

            /** 					Initializing = TRUE*/
            _38Initializing_17029 = _9TRUE_428;

            /** 					while sp do*/
L26: 
            if (_sp_44682 == 0)
            {
                goto L27; // [902] 1041
            }
            else{
            }

            /** 						integer scope = SymTab[sp][S_SCOPE]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23993 = (int)*(((s1_ptr)_2)->base + _sp_44682);
            _2 = (int)SEQ_PTR(_23993);
            _scope_44885 = (int)*(((s1_ptr)_2)->base + 4);
            if (!IS_ATOM_INT(_scope_44885)){
                _scope_44885 = (long)DBL_PTR(_scope_44885)->dbl;
            }
            _23993 = NOVALUE;

            /** 						switch scope with fallthru do*/
            _0 = _scope_44885;
            switch ( _0 ){ 

                /** 							case SC_LOOP_VAR, SC_UNDEFINED then*/
                case 2:
                case 9:

                /** 								break*/
                goto L28; // [936] 1018

                /** 							case SC_PRIVATE then*/
                case 3:

                /** 								c_stmt0("int ")*/
                RefDS(_23387);
                _59c_stmt0(_23387);

                /** 								c_puts("_")*/
                RefDS(_22735);
                _56c_puts(_22735);

                /** 								c_puts(SymTab[sp][S_NAME])*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _23997 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_23997);
                if (!IS_ATOM_INT(_38S_NAME_16598)){
                    _23998 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
                }
                else{
                    _23998 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
                }
                _23997 = NOVALUE;
                Ref(_23998);
                _56c_puts(_23998);
                _23998 = NOVALUE;

                /** 								c_puts(" = NOVALUE;\n")*/
                RefDS(_23394);
                _56c_puts(_23394);

                /** 								target[MIN] = NOVALUE*/
                Ref(_38NOVALUE_16800);
                _2 = (int)SEQ_PTR(_60target_28275);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _60target_28275 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = _38NOVALUE_16800;
                DeRef(_1);

                /** 								target[MAX] = NOVALUE*/
                Ref(_38NOVALUE_16800);
                _2 = (int)SEQ_PTR(_60target_28275);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _60target_28275 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 2);
                _1 = *(int *)_2;
                *(int *)_2 = _38NOVALUE_16800;
                DeRef(_1);

                /** 								RemoveFromBB( sp )*/
                _59RemoveFromBB(_sp_44682);

                /** 								break*/
                goto L28; // [1005] 1018

                /** 							case else*/
                default:

                /** 								exit*/
                goto L27; // [1015] 1041
            ;}L28: 

            /** 						sp = SymTab[sp][S_NEXT]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _23999 = (int)*(((s1_ptr)_2)->base + _sp_44682);
            _2 = (int)SEQ_PTR(_23999);
            _sp_44682 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44682)){
                _sp_44682 = (long)DBL_PTR(_sp_44682)->dbl;
            }
            _23999 = NOVALUE;

            /** 					end while*/
            goto L26; // [1038] 902
L27: 

            /** 					temps = SymTab[s][S_TEMPS]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24001 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_24001);
            if (!IS_ATOM_INT(_38S_TEMPS_16643)){
                _temps_44685 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
            }
            else{
                _temps_44685 = (int)*(((s1_ptr)_2)->base + _38S_TEMPS_16643);
            }
            if (!IS_ATOM_INT(_temps_44685)){
                _temps_44685 = (long)DBL_PTR(_temps_44685)->dbl;
            }
            _24001 = NOVALUE;

            /** 					sequence names = {}*/
            RefDS(_22663);
            DeRef(_names_44919);
            _names_44919 = _22663;

            /** 					while temps != 0 do*/
L29: 
            if (_temps_44685 == 0)
            goto L2A; // [1069] 1261

            /** 						if SymTab[temps][S_SCOPE] != DELETED then*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24004 = (int)*(((s1_ptr)_2)->base + _temps_44685);
            _2 = (int)SEQ_PTR(_24004);
            _24005 = (int)*(((s1_ptr)_2)->base + 4);
            _24004 = NOVALUE;
            if (binary_op_a(EQUALS, _24005, 2)){
                _24005 = NOVALUE;
                goto L2B; // [1089] 1221
            }
            _24005 = NOVALUE;

            /** 							sequence name = sprintf("_%d", SymTab[temps][S_TEMP_NAME] )*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24007 = (int)*(((s1_ptr)_2)->base + _temps_44685);
            _2 = (int)SEQ_PTR(_24007);
            _24008 = (int)*(((s1_ptr)_2)->base + 34);
            _24007 = NOVALUE;
            DeRefi(_name_44929);
            _name_44929 = EPrintf(-9999999, _22756, _24008);
            _24008 = NOVALUE;

            /** 							if temp_name_type[SymTab[temps][S_TEMP_NAME]][T_GTYPE]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24010 = (int)*(((s1_ptr)_2)->base + _temps_44685);
            _2 = (int)SEQ_PTR(_24010);
            _24011 = (int)*(((s1_ptr)_2)->base + 34);
            _24010 = NOVALUE;
            _2 = (int)SEQ_PTR(_38temp_name_type_17031);
            if (!IS_ATOM_INT(_24011)){
                _24012 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_24011)->dbl));
            }
            else{
                _24012 = (int)*(((s1_ptr)_2)->base + _24011);
            }
            _2 = (int)SEQ_PTR(_24012);
            _24013 = (int)*(((s1_ptr)_2)->base + 1);
            _24012 = NOVALUE;
            if (IS_ATOM_INT(_24013)) {
                _24014 = (_24013 != 0);
            }
            else {
                _24014 = binary_op(NOTEQ, _24013, 0);
            }
            _24013 = NOVALUE;
            if (IS_ATOM_INT(_24014)) {
                if (_24014 == 0) {
                    goto L2C; // [1143] 1217
                }
            }
            else {
                if (DBL_PTR(_24014)->dbl == 0.0) {
                    goto L2C; // [1143] 1217
                }
            }
            _24016 = find_from(_name_44929, _names_44919, 1);
            _24017 = (_24016 == 0);
            _24016 = NOVALUE;
            if (_24017 == 0)
            {
                DeRef(_24017);
                _24017 = NOVALUE;
                goto L2C; // [1156] 1217
            }
            else{
                DeRef(_24017);
                _24017 = NOVALUE;
            }

            /** 								c_stmt0("int ")*/
            RefDS(_23387);
            _59c_stmt0(_23387);

            /** 								c_puts( name )*/
            RefDS(_name_44929);
            _56c_puts(_name_44929);

            /** 								c_puts(" = NOVALUE")*/
            RefDS(_24018);
            _56c_puts(_24018);

            /** 								target = {NOVALUE, NOVALUE}*/
            Ref(_38NOVALUE_16800);
            Ref(_38NOVALUE_16800);
            DeRef(_60target_28275);
            _1 = NewS1(2);
            _2 = (int)((s1_ptr)_1)->base;
            ((int *)_2)[1] = _38NOVALUE_16800;
            ((int *)_2)[2] = _38NOVALUE_16800;
            _60target_28275 = MAKE_SEQ(_1);

            /** 								SetBBType(temps, TYPE_INTEGER, target, TYPE_OBJECT, 0)*/
            RefDS(_60target_28275);
            _59SetBBType(_temps_44685, 1, _60target_28275, 16, 0);

            /** 								ifdef DEBUG then*/

            /** 									c_puts(";\n")*/
            RefDS(_22893);
            _56c_puts(_22893);

            /** 								names = prepend( names, name )*/
            RefDS(_name_44929);
            Prepend(&_names_44919, _names_44919, _name_44929);
            goto L2D; // [1214] 1220
L2C: 

            /** 								ifdef DEBUG then*/
L2D: 
L2B: 
            DeRefi(_name_44929);
            _name_44929 = NOVALUE;

            /** 						SymTab[temps][S_GTYPE] = TYPE_OBJECT*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _35SymTab_15595 = MAKE_SEQ(_2);
            }
            _3 = (int)(_temps_44685 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 36);
            _1 = *(int *)_2;
            *(int *)_2 = 16;
            DeRef(_1);
            _24023 = NOVALUE;

            /** 						temps = SymTab[temps][S_NEXT]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24025 = (int)*(((s1_ptr)_2)->base + _temps_44685);
            _2 = (int)SEQ_PTR(_24025);
            _temps_44685 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_temps_44685)){
                _temps_44685 = (long)DBL_PTR(_temps_44685)->dbl;
            }
            _24025 = NOVALUE;

            /** 					end while*/
            goto L29; // [1258] 1069
L2A: 

            /** 					Initializing = FALSE*/
            _38Initializing_17029 = _9FALSE_426;

            /** 					if SymTab[s][S_LHS_SUBS2] then*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24027 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_24027);
            _24028 = (int)*(((s1_ptr)_2)->base + 37);
            _24027 = NOVALUE;
            if (_24028 == 0) {
                _24028 = NOVALUE;
                goto L2E; // [1284] 1295
            }
            else {
                if (!IS_ATOM_INT(_24028) && DBL_PTR(_24028)->dbl == 0.0){
                    _24028 = NOVALUE;
                    goto L2E; // [1284] 1295
                }
                _24028 = NOVALUE;
            }
            _24028 = NOVALUE;

            /** 						c_stmt0("int _0, _1, _2, _3;\n\n")*/
            RefDS(_24029);
            _59c_stmt0(_24029);
            goto L2F; // [1292] 1301
L2E: 

            /** 						c_stmt0("int _0, _1, _2;\n\n")*/
            RefDS(_24030);
            _59c_stmt0(_24030);
L2F: 

            /** 					sp = SymTab[s][S_NEXT]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24031 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_24031);
            _sp_44682 = (int)*(((s1_ptr)_2)->base + 2);
            if (!IS_ATOM_INT(_sp_44682)){
                _sp_44682 = (long)DBL_PTR(_sp_44682)->dbl;
            }
            _24031 = NOVALUE;

            /** 					for p = 1 to SymTab[s][S_NUM_ARGS] do*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24033 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_24033);
            if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
                _24034 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
            }
            else{
                _24034 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
            }
            _24033 = NOVALUE;
            {
                int _p_44988;
                _p_44988 = 1;
L30: 
                if (binary_op_a(GREATER, _p_44988, _24034)){
                    goto L31; // [1331] 1682
                }

                /** 						SymTab[sp][S_ONE_REF] = FALSE*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _35SymTab_15595 = MAKE_SEQ(_2);
                }
                _3 = (int)(_sp_44682 + ((s1_ptr)_2)->base);
                _2 = (int)SEQ_PTR(*(int *)_3);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    *(int *)_3 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 35);
                _1 = *(int *)_2;
                *(int *)_2 = _9FALSE_426;
                DeRef(_1);
                _24035 = NOVALUE;

                /** 						if SymTab[sp][S_ARG_TYPE] = TYPE_SEQUENCE then*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24037 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24037);
                _24038 = (int)*(((s1_ptr)_2)->base + 43);
                _24037 = NOVALUE;
                if (binary_op_a(NOTEQ, _24038, 8)){
                    _24038 = NOVALUE;
                    goto L32; // [1371] 1435
                }
                _24038 = NOVALUE;

                /** 							target[MIN] = SymTab[sp][S_ARG_SEQ_LEN]*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24040 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24040);
                _24041 = (int)*(((s1_ptr)_2)->base + 51);
                _24040 = NOVALUE;
                Ref(_24041);
                _2 = (int)SEQ_PTR(_60target_28275);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _60target_28275 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = _24041;
                if( _1 != _24041 ){
                    DeRef(_1);
                }
                _24041 = NOVALUE;

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], target,*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24042 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24042);
                _24043 = (int)*(((s1_ptr)_2)->base + 43);
                _24042 = NOVALUE;
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24044 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24044);
                _24045 = (int)*(((s1_ptr)_2)->base + 45);
                _24044 = NOVALUE;
                Ref(_24043);
                RefDS(_60target_28275);
                Ref(_24045);
                _59SetBBType(_sp_44682, _24043, _60target_28275, _24045, 0);
                _24043 = NOVALUE;
                _24045 = NOVALUE;
                goto L33; // [1432] 1659
L32: 

                /** 						elsif SymTab[sp][S_ARG_TYPE] = TYPE_INTEGER then*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24046 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24046);
                _24047 = (int)*(((s1_ptr)_2)->base + 43);
                _24046 = NOVALUE;
                if (binary_op_a(NOTEQ, _24047, 1)){
                    _24047 = NOVALUE;
                    goto L34; // [1451] 1575
                }
                _24047 = NOVALUE;

                /** 							if SymTab[sp][S_ARG_MIN] = NOVALUE then*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24049 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24049);
                _24050 = (int)*(((s1_ptr)_2)->base + 47);
                _24049 = NOVALUE;
                if (binary_op_a(NOTEQ, _24050, _38NOVALUE_16800)){
                    _24050 = NOVALUE;
                    goto L35; // [1471] 1502
                }
                _24050 = NOVALUE;

                /** 								target[MIN] = MININT*/
                _2 = (int)SEQ_PTR(_60target_28275);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _60target_28275 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = -1073741824;
                DeRef(_1);

                /** 								target[MAX] = MAXINT*/
                _2 = (int)SEQ_PTR(_60target_28275);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _60target_28275 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 2);
                _1 = *(int *)_2;
                *(int *)_2 = 1073741823;
                DeRef(_1);
                goto L36; // [1499] 1547
L35: 

                /** 								target[MIN] = SymTab[sp][S_ARG_MIN]*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24052 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24052);
                _24053 = (int)*(((s1_ptr)_2)->base + 47);
                _24052 = NOVALUE;
                Ref(_24053);
                _2 = (int)SEQ_PTR(_60target_28275);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _60target_28275 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 1);
                _1 = *(int *)_2;
                *(int *)_2 = _24053;
                if( _1 != _24053 ){
                    DeRef(_1);
                }
                _24053 = NOVALUE;

                /** 								target[MAX] = SymTab[sp][S_ARG_MAX]*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24054 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24054);
                _24055 = (int)*(((s1_ptr)_2)->base + 48);
                _24054 = NOVALUE;
                Ref(_24055);
                _2 = (int)SEQ_PTR(_60target_28275);
                if (!UNIQUE(_2)) {
                    _2 = (int)SequenceCopy((s1_ptr)_2);
                    _60target_28275 = MAKE_SEQ(_2);
                }
                _2 = (int)(((s1_ptr)_2)->base + 2);
                _1 = *(int *)_2;
                *(int *)_2 = _24055;
                if( _1 != _24055 ){
                    DeRef(_1);
                }
                _24055 = NOVALUE;
L36: 

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], target, TYPE_OBJECT, 0)*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24056 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24056);
                _24057 = (int)*(((s1_ptr)_2)->base + 43);
                _24056 = NOVALUE;
                Ref(_24057);
                RefDS(_60target_28275);
                _59SetBBType(_sp_44682, _24057, _60target_28275, 16, 0);
                _24057 = NOVALUE;
                goto L33; // [1572] 1659
L34: 

                /** 						elsif SymTab[sp][S_ARG_TYPE] = TYPE_OBJECT then*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24058 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24058);
                _24059 = (int)*(((s1_ptr)_2)->base + 43);
                _24058 = NOVALUE;
                if (binary_op_a(NOTEQ, _24059, 16)){
                    _24059 = NOVALUE;
                    goto L37; // [1591] 1633
                }
                _24059 = NOVALUE;

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], novalue,*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24061 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24061);
                _24062 = (int)*(((s1_ptr)_2)->base + 43);
                _24061 = NOVALUE;
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24063 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24063);
                _24064 = (int)*(((s1_ptr)_2)->base + 45);
                _24063 = NOVALUE;
                Ref(_24062);
                RefDS(_56novalue_46451);
                Ref(_24064);
                _59SetBBType(_sp_44682, _24062, _56novalue_46451, _24064, 0);
                _24062 = NOVALUE;
                _24064 = NOVALUE;
                goto L33; // [1630] 1659
L37: 

                /** 							SetBBType(sp, SymTab[sp][S_ARG_TYPE], novalue, TYPE_OBJECT, 0)*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24065 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24065);
                _24066 = (int)*(((s1_ptr)_2)->base + 43);
                _24065 = NOVALUE;
                Ref(_24066);
                RefDS(_56novalue_46451);
                _59SetBBType(_sp_44682, _24066, _56novalue_46451, 16, 0);
                _24066 = NOVALUE;
L33: 

                /** 						sp = SymTab[sp][S_NEXT]*/
                _2 = (int)SEQ_PTR(_35SymTab_15595);
                _24067 = (int)*(((s1_ptr)_2)->base + _sp_44682);
                _2 = (int)SEQ_PTR(_24067);
                _sp_44682 = (int)*(((s1_ptr)_2)->base + 2);
                if (!IS_ATOM_INT(_sp_44682)){
                    _sp_44682 = (long)DBL_PTR(_sp_44682)->dbl;
                }
                _24067 = NOVALUE;

                /** 					end for*/
                _0 = _p_44988;
                if (IS_ATOM_INT(_p_44988)) {
                    _p_44988 = _p_44988 + 1;
                    if ((long)((unsigned long)_p_44988 +(unsigned long) HIGH_BITS) >= 0){
                        _p_44988 = NewDouble((double)_p_44988);
                    }
                }
                else {
                    _p_44988 = binary_op_a(PLUS, _p_44988, 1);
                }
                DeRef(_0);
                goto L30; // [1677] 1338
L31: 
                ;
                DeRef(_p_44988);
            }

            /** 					call_proc(Execute_id, {s})*/
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = _s_44681;
            _24069 = MAKE_SEQ(_1);
            _1 = (int)SEQ_PTR(_24069);
            _2 = (int)((s1_ptr)_1)->base;
            _0 = (int)_00[_38Execute_id_17037].addr;
            Ref(*(int *)(_2+4));
            (*(int (*)())_0)(
                                *(int *)(_2+4)
                                 );
            DeRefDS(_24069);
            _24069 = NOVALUE;

            /** 					c_puts("    ;\n}\n")*/
            RefDS(_24070);
            _56c_puts(_24070);

            /** 					if TUNIX and dll_option and is_exported( s ) then*/
            if (0 == 0) {
                _24071 = 0;
                goto L38; // [1702] 1712
            }
            _24071 = (_59dll_option_42608 != 0);
L38: 
            if (_24071 == 0) {
                goto L39; // [1712] 2035
            }
            _24073 = _59is_exported(_s_44681);
            if (_24073 == 0) {
                DeRef(_24073);
                _24073 = NOVALUE;
                goto L39; // [1721] 2035
            }
            else {
                if (!IS_ATOM_INT(_24073) && DBL_PTR(_24073)->dbl == 0.0){
                    DeRef(_24073);
                    _24073 = NOVALUE;
                    goto L39; // [1721] 2035
                }
                DeRef(_24073);
                _24073 = NOVALUE;
            }
            DeRef(_24073);
            _24073 = NOVALUE;

            /** 						LeftSym = TRUE*/
            _59LeftSym_42605 = _9TRUE_428;

            /** 						if TOSX then*/

            /** 							c_stmt( ret_type & SymTab[s][S_NAME] & "() __attribute__ ((alias (\"@\")));\n", s )*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _24103 = (int)*(((s1_ptr)_2)->base + _s_44681);
            _2 = (int)SEQ_PTR(_24103);
            if (!IS_ATOM_INT(_38S_NAME_16598)){
                _24104 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
            }
            else{
                _24104 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
            }
            _24103 = NOVALUE;
            {
                int concat_list[3];

                concat_list[0] = _24105;
                concat_list[1] = _24104;
                concat_list[2] = _ret_type_44814;
                Concat_N((object_ptr)&_24106, concat_list, 3);
            }
            _24104 = NOVALUE;
            _59c_stmt(_24106, _s_44681, 0);
            _24106 = NOVALUE;

            /** 						LeftSym = FALSE*/
            _59LeftSym_42605 = _9FALSE_426;
L39: 

            /** 					c_puts("\n\n" )*/
            RefDS(_22777);
            _56c_puts(_22777);
L14: 
            DeRefi(_ret_type_44814);
            _ret_type_44814 = NOVALUE;
            DeRef(_names_44919);
            _names_44919 = NOVALUE;

            /** 			end for*/
            _routine_no_44759 = _routine_no_44759 + 1;
            goto L12; // [2045] 359
L13: 
            ;
        }
L8: 
        DeRef(_these_routines_44756);
        _these_routines_44756 = NOVALUE;

        /** 	end for*/
        _file_no_44705 = _file_no_44705 + 1;
        goto L5; // [2055] 85
L6: 
        ;
    }

    /** end procedure*/
    DeRefi(_buff_44686);
    DeRef(_base_name_44687);
    DeRef(_long_c_file_44688);
    DeRef(_c_file_44689);
    DeRef(_23900);
    _23900 = NOVALUE;
    DeRef(_23909);
    _23909 = NOVALUE;
    DeRef(_23923);
    _23923 = NOVALUE;
    DeRef(_23934);
    _23934 = NOVALUE;
    DeRef(_23936);
    _23936 = NOVALUE;
    DeRef(_23938);
    _23938 = NOVALUE;
    _23944 = NOVALUE;
    DeRef(_23941);
    _23941 = NOVALUE;
    DeRef(_23946);
    _23946 = NOVALUE;
    _23982 = NOVALUE;
    _24011 = NOVALUE;
    _24034 = NOVALUE;
    DeRef(_24014);
    _24014 = NOVALUE;
    return;
    ;
}



// 0xAAF192A2
