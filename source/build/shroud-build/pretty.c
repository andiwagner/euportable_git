// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _24pretty_out(int _text_8614)
{
    int _4682 = NOVALUE;
    int _4680 = NOVALUE;
    int _4678 = NOVALUE;
    int _4677 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pretty_line &= text*/
    if (IS_SEQUENCE(_24pretty_line_8611) && IS_ATOM(_text_8614)) {
        Ref(_text_8614);
        Append(&_24pretty_line_8611, _24pretty_line_8611, _text_8614);
    }
    else if (IS_ATOM(_24pretty_line_8611) && IS_SEQUENCE(_text_8614)) {
    }
    else {
        Concat((object_ptr)&_24pretty_line_8611, _24pretty_line_8611, _text_8614);
    }

    /** 	if equal(text, '\n') and pretty_printing then*/
    if (_text_8614 == 10)
    _4677 = 1;
    else if (IS_ATOM_INT(_text_8614) && IS_ATOM_INT(10))
    _4677 = 0;
    else
    _4677 = (compare(_text_8614, 10) == 0);
    if (_4677 == 0) {
        goto L1; // [15] 50
    }
    goto L1; // [22] 50

    /** 		puts(pretty_file, pretty_line)*/
    EPuts(_24pretty_file_8599, _24pretty_line_8611); // DJP 

    /** 		pretty_line = ""*/
    RefDS(_5);
    DeRefDS(_24pretty_line_8611);
    _24pretty_line_8611 = _5;

    /** 		pretty_line_count += 1*/
    _24pretty_line_count_8604 = _24pretty_line_count_8604 + 1;
L1: 

    /** 	if atom(text) then*/
    _4680 = IS_ATOM(_text_8614);
    if (_4680 == 0)
    {
        _4680 = NOVALUE;
        goto L2; // [55] 69
    }
    else{
        _4680 = NOVALUE;
    }

    /** 		pretty_chars += 1*/
    _24pretty_chars_8596 = _24pretty_chars_8596 + 1;
    goto L3; // [66] 81
L2: 

    /** 		pretty_chars += length(text)*/
    if (IS_SEQUENCE(_text_8614)){
            _4682 = SEQ_PTR(_text_8614)->length;
    }
    else {
        _4682 = 1;
    }
    _24pretty_chars_8596 = _24pretty_chars_8596 + _4682;
    _4682 = NOVALUE;
L3: 

    /** end procedure*/
    DeRef(_text_8614);
    return;
    ;
}


void _24cut_line(int _n_8628)
{
    int _4685 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not pretty_line_breaks then	*/
    if (_24pretty_line_breaks_8607 != 0)
    goto L1; // [7] 21

    /** 		pretty_chars = 0*/
    _24pretty_chars_8596 = 0;

    /** 		return*/
    return;
L1: 

    /** 	if pretty_chars + n > pretty_end_col then*/
    _4685 = _24pretty_chars_8596 + _n_8628;
    if ((long)((unsigned long)_4685 + (unsigned long)HIGH_BITS) >= 0) 
    _4685 = NewDouble((double)_4685);
    if (binary_op_a(LESSEQ, _4685, _24pretty_end_col_8595)){
        DeRef(_4685);
        _4685 = NOVALUE;
        goto L2; // [31] 46
    }
    DeRef(_4685);
    _4685 = NOVALUE;

    /** 		pretty_out('\n')*/
    _24pretty_out(10);

    /** 		pretty_chars = 0*/
    _24pretty_chars_8596 = 0;
L2: 

    /** end procedure*/
    return;
    ;
}


void _24indent()
{
    int _4693 = NOVALUE;
    int _4692 = NOVALUE;
    int _4691 = NOVALUE;
    int _4690 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if pretty_line_breaks = 0 then	*/
    if (_24pretty_line_breaks_8607 != 0)
    goto L1; // [5] 22

    /** 		pretty_chars = 0*/
    _24pretty_chars_8596 = 0;

    /** 		return*/
    return;
    goto L2; // [19] 85
L1: 

    /** 	elsif pretty_line_breaks = -1 then*/
    if (_24pretty_line_breaks_8607 != -1)
    goto L3; // [26] 38

    /** 		cut_line( 0 )*/
    _24cut_line(0);
    goto L2; // [35] 85
L3: 

    /** 		if pretty_chars > 0 then*/
    if (_24pretty_chars_8596 <= 0)
    goto L4; // [42] 57

    /** 			pretty_out('\n')*/
    _24pretty_out(10);

    /** 			pretty_chars = 0*/
    _24pretty_chars_8596 = 0;
L4: 

    /** 		pretty_out(repeat(' ', (pretty_start_col-1) + */
    _4690 = _24pretty_start_col_8597 - 1;
    if ((long)((unsigned long)_4690 +(unsigned long) HIGH_BITS) >= 0){
        _4690 = NewDouble((double)_4690);
    }
    if (_24pretty_level_8598 == (short)_24pretty_level_8598 && _24pretty_indent_8601 <= INT15 && _24pretty_indent_8601 >= -INT15)
    _4691 = _24pretty_level_8598 * _24pretty_indent_8601;
    else
    _4691 = NewDouble(_24pretty_level_8598 * (double)_24pretty_indent_8601);
    if (IS_ATOM_INT(_4690) && IS_ATOM_INT(_4691)) {
        _4692 = _4690 + _4691;
    }
    else {
        if (IS_ATOM_INT(_4690)) {
            _4692 = NewDouble((double)_4690 + DBL_PTR(_4691)->dbl);
        }
        else {
            if (IS_ATOM_INT(_4691)) {
                _4692 = NewDouble(DBL_PTR(_4690)->dbl + (double)_4691);
            }
            else
            _4692 = NewDouble(DBL_PTR(_4690)->dbl + DBL_PTR(_4691)->dbl);
        }
    }
    DeRef(_4690);
    _4690 = NOVALUE;
    DeRef(_4691);
    _4691 = NOVALUE;
    _4693 = Repeat(32, _4692);
    DeRef(_4692);
    _4692 = NOVALUE;
    _24pretty_out(_4693);
    _4693 = NOVALUE;
L2: 

    /** end procedure*/
    return;
    ;
}


int _24esc_char(int _a_8649)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_8649)) {
        _1 = (long)(DBL_PTR(_a_8649)->dbl);
        if (UNIQUE(DBL_PTR(_a_8649)) && (DBL_PTR(_a_8649)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_8649);
        _a_8649 = _1;
    }

    /** 	switch a do*/
    _0 = _a_8649;
    switch ( _0 ){ 

        /** 		case'\t' then*/
        case 9:

        /** 			return `\t`*/
        RefDS(_4696);
        return _4696;
        goto L1; // [20] 81

        /** 		case'\n' then*/
        case 10:

        /** 			return `\n`*/
        RefDS(_4697);
        return _4697;
        goto L1; // [32] 81

        /** 		case'\r' then*/
        case 13:

        /** 			return `\r`*/
        RefDS(_4698);
        return _4698;
        goto L1; // [44] 81

        /** 		case'\\' then*/
        case 92:

        /** 			return `\\`*/
        RefDS(_959);
        return _959;
        goto L1; // [56] 81

        /** 		case'"' then*/
        case 34:

        /** 			return `\"`*/
        RefDS(_4699);
        return _4699;
        goto L1; // [68] 81

        /** 		case else*/
        default:

        /** 			return a*/
        return _a_8649;
    ;}L1: 
    ;
}


void _24rPrint(int _a_8664)
{
    int _sbuff_8665 = NOVALUE;
    int _multi_line_8666 = NOVALUE;
    int _all_ascii_8667 = NOVALUE;
    int _4755 = NOVALUE;
    int _4754 = NOVALUE;
    int _4753 = NOVALUE;
    int _4752 = NOVALUE;
    int _4748 = NOVALUE;
    int _4747 = NOVALUE;
    int _4746 = NOVALUE;
    int _4745 = NOVALUE;
    int _4743 = NOVALUE;
    int _4742 = NOVALUE;
    int _4740 = NOVALUE;
    int _4739 = NOVALUE;
    int _4737 = NOVALUE;
    int _4736 = NOVALUE;
    int _4735 = NOVALUE;
    int _4734 = NOVALUE;
    int _4733 = NOVALUE;
    int _4732 = NOVALUE;
    int _4731 = NOVALUE;
    int _4730 = NOVALUE;
    int _4729 = NOVALUE;
    int _4728 = NOVALUE;
    int _4727 = NOVALUE;
    int _4726 = NOVALUE;
    int _4725 = NOVALUE;
    int _4724 = NOVALUE;
    int _4723 = NOVALUE;
    int _4722 = NOVALUE;
    int _4721 = NOVALUE;
    int _4717 = NOVALUE;
    int _4716 = NOVALUE;
    int _4715 = NOVALUE;
    int _4714 = NOVALUE;
    int _4713 = NOVALUE;
    int _4712 = NOVALUE;
    int _4710 = NOVALUE;
    int _4709 = NOVALUE;
    int _4706 = NOVALUE;
    int _4705 = NOVALUE;
    int _4704 = NOVALUE;
    int _4701 = NOVALUE;
    int _4700 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(a) then*/
    _4700 = IS_ATOM(_a_8664);
    if (_4700 == 0)
    {
        _4700 = NOVALUE;
        goto L1; // [6] 176
    }
    else{
        _4700 = NOVALUE;
    }

    /** 		if integer(a) then*/
    if (IS_ATOM_INT(_a_8664))
    _4701 = 1;
    else if (IS_ATOM_DBL(_a_8664))
    _4701 = IS_ATOM_INT(DoubleToInt(_a_8664));
    else
    _4701 = 0;
    if (_4701 == 0)
    {
        _4701 = NOVALUE;
        goto L2; // [14] 157
    }
    else{
        _4701 = NOVALUE;
    }

    /** 			sbuff = sprintf(pretty_int_format, a)*/
    DeRef(_sbuff_8665);
    _sbuff_8665 = EPrintf(-9999999, _24pretty_int_format_8610, _a_8664);

    /** 			if pretty_ascii then */
    if (_24pretty_ascii_8600 == 0)
    {
        goto L3; // [29] 166
    }
    else{
    }

    /** 				if pretty_ascii >= 3 then */
    if (_24pretty_ascii_8600 < 3)
    goto L4; // [36] 103

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) then*/
    if (IS_ATOM_INT(_a_8664)) {
        _4704 = (_a_8664 >= _24pretty_ascii_min_8602);
    }
    else {
        _4704 = binary_op(GREATEREQ, _a_8664, _24pretty_ascii_min_8602);
    }
    if (IS_ATOM_INT(_4704)) {
        if (_4704 == 0) {
            _4705 = 0;
            goto L5; // [48] 62
        }
    }
    else {
        if (DBL_PTR(_4704)->dbl == 0.0) {
            _4705 = 0;
            goto L5; // [48] 62
        }
    }
    if (IS_ATOM_INT(_a_8664)) {
        _4706 = (_a_8664 <= _24pretty_ascii_max_8603);
    }
    else {
        _4706 = binary_op(LESSEQ, _a_8664, _24pretty_ascii_max_8603);
    }
    DeRef(_4705);
    if (IS_ATOM_INT(_4706))
    _4705 = (_4706 != 0);
    else
    _4705 = DBL_PTR(_4706)->dbl != 0.0;
L5: 
    if (_4705 == 0)
    {
        _4705 = NOVALUE;
        goto L6; // [62] 76
    }
    else{
        _4705 = NOVALUE;
    }

    /** 						sbuff = '\'' & a & '\''  -- display char only*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_8664;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_8665, concat_list, 3);
    }
    goto L3; // [73] 166
L6: 

    /** 					elsif find(a, "\t\n\r\\") then*/
    _4709 = find_from(_a_8664, _4708, 1);
    if (_4709 == 0)
    {
        _4709 = NOVALUE;
        goto L3; // [83] 166
    }
    else{
        _4709 = NOVALUE;
    }

    /** 						sbuff = '\'' & esc_char(a) & '\''  -- display char only*/
    Ref(_a_8664);
    _4710 = _24esc_char(_a_8664);
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _4710;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_sbuff_8665, concat_list, 3);
    }
    DeRef(_4710);
    _4710 = NOVALUE;
    goto L3; // [100] 166
L4: 

    /** 					if (a >= pretty_ascii_min and a <= pretty_ascii_max) and pretty_ascii < 2 then*/
    if (IS_ATOM_INT(_a_8664)) {
        _4712 = (_a_8664 >= _24pretty_ascii_min_8602);
    }
    else {
        _4712 = binary_op(GREATEREQ, _a_8664, _24pretty_ascii_min_8602);
    }
    if (IS_ATOM_INT(_4712)) {
        if (_4712 == 0) {
            DeRef(_4713);
            _4713 = 0;
            goto L7; // [111] 125
        }
    }
    else {
        if (DBL_PTR(_4712)->dbl == 0.0) {
            DeRef(_4713);
            _4713 = 0;
            goto L7; // [111] 125
        }
    }
    if (IS_ATOM_INT(_a_8664)) {
        _4714 = (_a_8664 <= _24pretty_ascii_max_8603);
    }
    else {
        _4714 = binary_op(LESSEQ, _a_8664, _24pretty_ascii_max_8603);
    }
    DeRef(_4713);
    if (IS_ATOM_INT(_4714))
    _4713 = (_4714 != 0);
    else
    _4713 = DBL_PTR(_4714)->dbl != 0.0;
L7: 
    if (_4713 == 0) {
        goto L3; // [125] 166
    }
    _4716 = (_24pretty_ascii_8600 < 2);
    if (_4716 == 0)
    {
        DeRef(_4716);
        _4716 = NOVALUE;
        goto L3; // [136] 166
    }
    else{
        DeRef(_4716);
        _4716 = NOVALUE;
    }

    /** 						sbuff &= '\'' & a & '\'' -- add to numeric display*/
    {
        int concat_list[3];

        concat_list[0] = 39;
        concat_list[1] = _a_8664;
        concat_list[2] = 39;
        Concat_N((object_ptr)&_4717, concat_list, 3);
    }
    Concat((object_ptr)&_sbuff_8665, _sbuff_8665, _4717);
    DeRefDS(_4717);
    _4717 = NOVALUE;
    goto L3; // [154] 166
L2: 

    /** 			sbuff = sprintf(pretty_fp_format, a)*/
    DeRef(_sbuff_8665);
    _sbuff_8665 = EPrintf(-9999999, _24pretty_fp_format_8609, _a_8664);
L3: 

    /** 		pretty_out(sbuff)*/
    RefDS(_sbuff_8665);
    _24pretty_out(_sbuff_8665);
    goto L8; // [173] 535
L1: 

    /** 		cut_line(1)*/
    _24cut_line(1);

    /** 		multi_line = 0*/
    _multi_line_8666 = 0;

    /** 		all_ascii = pretty_ascii > 1*/
    _all_ascii_8667 = (_24pretty_ascii_8600 > 1);

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_8664)){
            _4721 = SEQ_PTR(_a_8664)->length;
    }
    else {
        _4721 = 1;
    }
    {
        int _i_8700;
        _i_8700 = 1;
L9: 
        if (_i_8700 > _4721){
            goto LA; // [199] 345
        }

        /** 			if sequence(a[i]) and length(a[i]) > 0 then*/
        _2 = (int)SEQ_PTR(_a_8664);
        _4722 = (int)*(((s1_ptr)_2)->base + _i_8700);
        _4723 = IS_SEQUENCE(_4722);
        _4722 = NOVALUE;
        if (_4723 == 0) {
            goto LB; // [215] 249
        }
        _2 = (int)SEQ_PTR(_a_8664);
        _4725 = (int)*(((s1_ptr)_2)->base + _i_8700);
        if (IS_SEQUENCE(_4725)){
                _4726 = SEQ_PTR(_4725)->length;
        }
        else {
            _4726 = 1;
        }
        _4725 = NOVALUE;
        _4727 = (_4726 > 0);
        _4726 = NOVALUE;
        if (_4727 == 0)
        {
            DeRef(_4727);
            _4727 = NOVALUE;
            goto LB; // [231] 249
        }
        else{
            DeRef(_4727);
            _4727 = NOVALUE;
        }

        /** 				multi_line = 1*/
        _multi_line_8666 = 1;

        /** 				all_ascii = 0*/
        _all_ascii_8667 = 0;

        /** 				exit*/
        goto LA; // [246] 345
LB: 

        /** 			if not integer(a[i]) or*/
        _2 = (int)SEQ_PTR(_a_8664);
        _4728 = (int)*(((s1_ptr)_2)->base + _i_8700);
        if (IS_ATOM_INT(_4728))
        _4729 = 1;
        else if (IS_ATOM_DBL(_4728))
        _4729 = IS_ATOM_INT(DoubleToInt(_4728));
        else
        _4729 = 0;
        _4728 = NOVALUE;
        _4730 = (_4729 == 0);
        _4729 = NOVALUE;
        if (_4730 != 0) {
            _4731 = 1;
            goto LC; // [261] 313
        }
        _2 = (int)SEQ_PTR(_a_8664);
        _4732 = (int)*(((s1_ptr)_2)->base + _i_8700);
        if (IS_ATOM_INT(_4732)) {
            _4733 = (_4732 < _24pretty_ascii_min_8602);
        }
        else {
            _4733 = binary_op(LESS, _4732, _24pretty_ascii_min_8602);
        }
        _4732 = NOVALUE;
        if (IS_ATOM_INT(_4733)) {
            if (_4733 == 0) {
                DeRef(_4734);
                _4734 = 0;
                goto LD; // [275] 309
            }
        }
        else {
            if (DBL_PTR(_4733)->dbl == 0.0) {
                DeRef(_4734);
                _4734 = 0;
                goto LD; // [275] 309
            }
        }
        _4735 = (_24pretty_ascii_8600 < 2);
        if (_4735 != 0) {
            _4736 = 1;
            goto LE; // [285] 305
        }
        _2 = (int)SEQ_PTR(_a_8664);
        _4737 = (int)*(((s1_ptr)_2)->base + _i_8700);
        _4739 = find_from(_4737, _4738, 1);
        _4737 = NOVALUE;
        _4740 = (_4739 == 0);
        _4739 = NOVALUE;
        _4736 = (_4740 != 0);
LE: 
        DeRef(_4734);
        _4734 = (_4736 != 0);
LD: 
        _4731 = (_4734 != 0);
LC: 
        if (_4731 != 0) {
            goto LF; // [313] 332
        }
        _2 = (int)SEQ_PTR(_a_8664);
        _4742 = (int)*(((s1_ptr)_2)->base + _i_8700);
        if (IS_ATOM_INT(_4742)) {
            _4743 = (_4742 > _24pretty_ascii_max_8603);
        }
        else {
            _4743 = binary_op(GREATER, _4742, _24pretty_ascii_max_8603);
        }
        _4742 = NOVALUE;
        if (_4743 == 0) {
            DeRef(_4743);
            _4743 = NOVALUE;
            goto L10; // [328] 338
        }
        else {
            if (!IS_ATOM_INT(_4743) && DBL_PTR(_4743)->dbl == 0.0){
                DeRef(_4743);
                _4743 = NOVALUE;
                goto L10; // [328] 338
            }
            DeRef(_4743);
            _4743 = NOVALUE;
        }
        DeRef(_4743);
        _4743 = NOVALUE;
LF: 

        /** 				all_ascii = 0*/
        _all_ascii_8667 = 0;
L10: 

        /** 		end for*/
        _i_8700 = _i_8700 + 1;
        goto L9; // [340] 206
LA: 
        ;
    }

    /** 		if all_ascii then*/
    if (_all_ascii_8667 == 0)
    {
        goto L11; // [347] 358
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _24pretty_out(34);
    goto L12; // [355] 364
L11: 

    /** 			pretty_out('{')*/
    _24pretty_out(123);
L12: 

    /** 		pretty_level += 1*/
    _24pretty_level_8598 = _24pretty_level_8598 + 1;

    /** 		for i = 1 to length(a) do*/
    if (IS_SEQUENCE(_a_8664)){
            _4745 = SEQ_PTR(_a_8664)->length;
    }
    else {
        _4745 = 1;
    }
    {
        int _i_8730;
        _i_8730 = 1;
L13: 
        if (_i_8730 > _4745){
            goto L14; // [377] 497
        }

        /** 			if multi_line then*/
        if (_multi_line_8666 == 0)
        {
            goto L15; // [386] 394
        }
        else{
        }

        /** 				indent()*/
        _24indent();
L15: 

        /** 			if all_ascii then*/
        if (_all_ascii_8667 == 0)
        {
            goto L16; // [396] 415
        }
        else{
        }

        /** 				pretty_out(esc_char(a[i]))*/
        _2 = (int)SEQ_PTR(_a_8664);
        _4746 = (int)*(((s1_ptr)_2)->base + _i_8730);
        Ref(_4746);
        _4747 = _24esc_char(_4746);
        _4746 = NOVALUE;
        _24pretty_out(_4747);
        _4747 = NOVALUE;
        goto L17; // [412] 425
L16: 

        /** 				rPrint(a[i])*/
        _2 = (int)SEQ_PTR(_a_8664);
        _4748 = (int)*(((s1_ptr)_2)->base + _i_8730);
        Ref(_4748);
        _24rPrint(_4748);
        _4748 = NOVALUE;
L17: 

        /** 			if pretty_line_count >= pretty_line_max then*/
        if (_24pretty_line_count_8604 < _24pretty_line_max_8605)
        goto L18; // [431] 459

        /** 				if not pretty_dots then*/
        if (_24pretty_dots_8606 != 0)
        goto L19; // [439] 448

        /** 					pretty_out(" ...")*/
        RefDS(_4751);
        _24pretty_out(_4751);
L19: 

        /** 				pretty_dots = 1*/
        _24pretty_dots_8606 = 1;

        /** 				return*/
        DeRef(_a_8664);
        DeRef(_sbuff_8665);
        DeRef(_4704);
        _4704 = NOVALUE;
        DeRef(_4706);
        _4706 = NOVALUE;
        DeRef(_4712);
        _4712 = NOVALUE;
        DeRef(_4714);
        _4714 = NOVALUE;
        _4725 = NOVALUE;
        DeRef(_4730);
        _4730 = NOVALUE;
        DeRef(_4735);
        _4735 = NOVALUE;
        DeRef(_4733);
        _4733 = NOVALUE;
        DeRef(_4740);
        _4740 = NOVALUE;
        return;
L18: 

        /** 			if i != length(a) and not all_ascii then*/
        if (IS_SEQUENCE(_a_8664)){
                _4752 = SEQ_PTR(_a_8664)->length;
        }
        else {
            _4752 = 1;
        }
        _4753 = (_i_8730 != _4752);
        _4752 = NOVALUE;
        if (_4753 == 0) {
            goto L1A; // [468] 490
        }
        _4755 = (_all_ascii_8667 == 0);
        if (_4755 == 0)
        {
            DeRef(_4755);
            _4755 = NOVALUE;
            goto L1A; // [476] 490
        }
        else{
            DeRef(_4755);
            _4755 = NOVALUE;
        }

        /** 				pretty_out(',')*/
        _24pretty_out(44);

        /** 				cut_line(6)*/
        _24cut_line(6);
L1A: 

        /** 		end for*/
        _i_8730 = _i_8730 + 1;
        goto L13; // [492] 384
L14: 
        ;
    }

    /** 		pretty_level -= 1*/
    _24pretty_level_8598 = _24pretty_level_8598 - 1;

    /** 		if multi_line then*/
    if (_multi_line_8666 == 0)
    {
        goto L1B; // [507] 515
    }
    else{
    }

    /** 			indent()*/
    _24indent();
L1B: 

    /** 		if all_ascii then*/
    if (_all_ascii_8667 == 0)
    {
        goto L1C; // [517] 528
    }
    else{
    }

    /** 			pretty_out('\"')*/
    _24pretty_out(34);
    goto L1D; // [525] 534
L1C: 

    /** 			pretty_out('}')*/
    _24pretty_out(125);
L1D: 
L8: 

    /** end procedure*/
    DeRef(_a_8664);
    DeRef(_sbuff_8665);
    DeRef(_4704);
    _4704 = NOVALUE;
    DeRef(_4706);
    _4706 = NOVALUE;
    DeRef(_4712);
    _4712 = NOVALUE;
    DeRef(_4714);
    _4714 = NOVALUE;
    _4725 = NOVALUE;
    DeRef(_4730);
    _4730 = NOVALUE;
    DeRef(_4735);
    _4735 = NOVALUE;
    DeRef(_4733);
    _4733 = NOVALUE;
    DeRef(_4740);
    _4740 = NOVALUE;
    DeRef(_4753);
    _4753 = NOVALUE;
    return;
    ;
}


void _24pretty(int _x_8778, int _options_8779)
{
    int _4771 = NOVALUE;
    int _4770 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(options) < length( PRETTY_DEFAULT ) then*/
    _4770 = 10;
    _4771 = 10;

    /** 	pretty_ascii = options[DISPLAY_ASCII] */
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_ascii_8600 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_24pretty_ascii_8600))
    _24pretty_ascii_8600 = (long)DBL_PTR(_24pretty_ascii_8600)->dbl;

    /** 	pretty_indent = options[INDENT]*/
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_indent_8601 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_24pretty_indent_8601))
    _24pretty_indent_8601 = (long)DBL_PTR(_24pretty_indent_8601)->dbl;

    /** 	pretty_start_col = options[START_COLUMN]*/
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_start_col_8597 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_24pretty_start_col_8597))
    _24pretty_start_col_8597 = (long)DBL_PTR(_24pretty_start_col_8597)->dbl;

    /** 	pretty_end_col = options[WRAP]*/
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_end_col_8595 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_24pretty_end_col_8595))
    _24pretty_end_col_8595 = (long)DBL_PTR(_24pretty_end_col_8595)->dbl;

    /** 	pretty_int_format = options[INT_FORMAT]*/
    DeRef(_24pretty_int_format_8610);
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_int_format_8610 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_24pretty_int_format_8610);

    /** 	pretty_fp_format = options[FP_FORMAT]*/
    DeRef(_24pretty_fp_format_8609);
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_fp_format_8609 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_24pretty_fp_format_8609);

    /** 	pretty_ascii_min = options[MIN_ASCII]*/
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_ascii_min_8602 = (int)*(((s1_ptr)_2)->base + 7);
    if (!IS_ATOM_INT(_24pretty_ascii_min_8602))
    _24pretty_ascii_min_8602 = (long)DBL_PTR(_24pretty_ascii_min_8602)->dbl;

    /** 	pretty_ascii_max = options[MAX_ASCII]*/
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_ascii_max_8603 = (int)*(((s1_ptr)_2)->base + 8);
    if (!IS_ATOM_INT(_24pretty_ascii_max_8603))
    _24pretty_ascii_max_8603 = (long)DBL_PTR(_24pretty_ascii_max_8603)->dbl;

    /** 	pretty_line_max = options[MAX_LINES]*/
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_line_max_8605 = (int)*(((s1_ptr)_2)->base + 9);
    if (!IS_ATOM_INT(_24pretty_line_max_8605))
    _24pretty_line_max_8605 = (long)DBL_PTR(_24pretty_line_max_8605)->dbl;

    /** 	pretty_line_breaks = options[LINE_BREAKS]*/
    _2 = (int)SEQ_PTR(_options_8779);
    _24pretty_line_breaks_8607 = (int)*(((s1_ptr)_2)->base + 10);
    if (!IS_ATOM_INT(_24pretty_line_breaks_8607))
    _24pretty_line_breaks_8607 = (long)DBL_PTR(_24pretty_line_breaks_8607)->dbl;

    /** 	pretty_chars = pretty_start_col*/
    _24pretty_chars_8596 = _24pretty_start_col_8597;

    /** 	pretty_level = 0 */
    _24pretty_level_8598 = 0;

    /** 	pretty_line = ""*/
    RefDS(_5);
    DeRef(_24pretty_line_8611);
    _24pretty_line_8611 = _5;

    /** 	pretty_line_count = 0*/
    _24pretty_line_count_8604 = 0;

    /** 	pretty_dots = 0*/
    _24pretty_dots_8606 = 0;

    /** 	rPrint(x)*/
    Ref(_x_8778);
    _24rPrint(_x_8778);

    /** end procedure*/
    DeRef(_x_8778);
    DeRefDS(_options_8779);
    return;
    ;
}



// 0xCE7B37CF
