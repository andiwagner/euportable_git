// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _51fatal(int _errcode_18127, int _msg_18128, int _routine_name_18129, int _parms_18130)
{
    int _10448 = NOVALUE;
    int _0, _1, _2;
    

    /** 	vLastErrors = append(vLastErrors, {errcode, msg, routine_name, parms})*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _errcode_18127;
    RefDS(_msg_18128);
    *((int *)(_2+8)) = _msg_18128;
    RefDS(_routine_name_18129);
    *((int *)(_2+12)) = _routine_name_18129;
    RefDS(_parms_18130);
    *((int *)(_2+16)) = _parms_18130;
    _10448 = MAKE_SEQ(_1);
    RefDS(_10448);
    Append(&_51vLastErrors_18124, _51vLastErrors_18124, _10448);
    DeRefDS(_10448);
    _10448 = NOVALUE;

    /** 	if db_fatal_id >= 0 then*/

    /** end procedure*/
    DeRefDSi(_msg_18128);
    DeRefDSi(_routine_name_18129);
    DeRefDS(_parms_18130);
    return;
    ;
}


int _51get4()
{
    int _10464 = NOVALUE;
    int _10463 = NOVALUE;
    int _10462 = NOVALUE;
    int _10461 = NOVALUE;
    int _10460 = NOVALUE;
    int _0, _1, _2;
    

    /** 	poke(mem0, getc(current_db))*/
    if (_51current_db_18100 != last_r_file_no) {
        last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
        last_r_file_no = _51current_db_18100;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10460 = getKBchar();
        }
        else
        _10460 = getc(last_r_file_ptr);
    }
    else
    _10460 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_51mem0_18142)){
        poke_addr = (unsigned char *)_51mem0_18142;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke_addr = (unsigned char)_10460;
    _10460 = NOVALUE;

    /** 	poke(mem1, getc(current_db))*/
    if (_51current_db_18100 != last_r_file_no) {
        last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
        last_r_file_no = _51current_db_18100;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10461 = getKBchar();
        }
        else
        _10461 = getc(last_r_file_ptr);
    }
    else
    _10461 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_51mem1_18143)){
        poke_addr = (unsigned char *)_51mem1_18143;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_51mem1_18143)->dbl);
    }
    *poke_addr = (unsigned char)_10461;
    _10461 = NOVALUE;

    /** 	poke(mem2, getc(current_db))*/
    if (_51current_db_18100 != last_r_file_no) {
        last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
        last_r_file_no = _51current_db_18100;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10462 = getKBchar();
        }
        else
        _10462 = getc(last_r_file_ptr);
    }
    else
    _10462 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_51mem2_18144)){
        poke_addr = (unsigned char *)_51mem2_18144;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_51mem2_18144)->dbl);
    }
    *poke_addr = (unsigned char)_10462;
    _10462 = NOVALUE;

    /** 	poke(mem3, getc(current_db))*/
    if (_51current_db_18100 != last_r_file_no) {
        last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
        last_r_file_no = _51current_db_18100;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _10463 = getKBchar();
        }
        else
        _10463 = getc(last_r_file_ptr);
    }
    else
    _10463 = getc(last_r_file_ptr);
    if (IS_ATOM_INT(_51mem3_18145)){
        poke_addr = (unsigned char *)_51mem3_18145;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_51mem3_18145)->dbl);
    }
    *poke_addr = (unsigned char)_10463;
    _10463 = NOVALUE;

    /** 	return peek4u(mem0)*/
    if (IS_ATOM_INT(_51mem0_18142)) {
        _10464 = *(unsigned long *)_51mem0_18142;
        if ((unsigned)_10464 > (unsigned)MAXINT)
        _10464 = NewDouble((double)(unsigned long)_10464);
    }
    else {
        _10464 = *(unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
        if ((unsigned)_10464 > (unsigned)MAXINT)
        _10464 = NewDouble((double)(unsigned long)_10464);
    }
    return _10464;
    ;
}


int _51get_string()
{
    int _where_inlined_where_at_33_18170 = NOVALUE;
    int _s_18159 = NOVALUE;
    int _c_18160 = NOVALUE;
    int _i_18161 = NOVALUE;
    int _10477 = NOVALUE;
    int _10474 = NOVALUE;
    int _10472 = NOVALUE;
    int _10470 = NOVALUE;
    int _0, _1, _2;
    

    /** 	s = repeat(0, 256)*/
    DeRefi(_s_18159);
    _s_18159 = Repeat(0, 256);

    /** 	i = 0*/
    _i_18161 = 0;

    /** 	while c with entry do*/
    goto L1; // [14] 91
L2: 
    if (_c_18160 == 0)
    {
        goto L3; // [19] 103
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_18160 != -1)
    goto L4; // [24] 56

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "get_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_33_18170);
    _where_inlined_where_at_33_18170 = machine(20, _51current_db_18100);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_33_18170);
    *((int *)(_2+4)) = _where_inlined_where_at_33_18170;
    _10470 = MAKE_SEQ(_1);
    RefDS(_10468);
    RefDS(_10469);
    _51fatal(900, _10468, _10469, _10470);
    _10470 = NOVALUE;

    /** 			exit*/
    goto L3; // [53] 103
L4: 

    /** 		i += 1*/
    _i_18161 = _i_18161 + 1;

    /** 		if i > length(s) then*/
    if (IS_SEQUENCE(_s_18159)){
            _10472 = SEQ_PTR(_s_18159)->length;
    }
    else {
        _10472 = 1;
    }
    if (_i_18161 <= _10472)
    goto L5; // [67] 82

    /** 			s &= repeat(0, 256)*/
    _10474 = Repeat(0, 256);
    Concat((object_ptr)&_s_18159, _s_18159, _10474);
    DeRefDS(_10474);
    _10474 = NOVALUE;
L5: 

    /** 		s[i] = c*/
    _2 = (int)SEQ_PTR(_s_18159);
    _2 = (int)(((s1_ptr)_2)->base + _i_18161);
    *(int *)_2 = _c_18160;

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_51current_db_18100 != last_r_file_no) {
        last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
        last_r_file_no = _51current_db_18100;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_18160 = getKBchar();
        }
        else
        _c_18160 = getc(last_r_file_ptr);
    }
    else
    _c_18160 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [100] 17
L3: 

    /** 	return s[1..i]*/
    rhs_slice_target = (object_ptr)&_10477;
    RHS_Slice(_s_18159, 1, _i_18161);
    DeRefDSi(_s_18159);
    return _10477;
    ;
}


int _51equal_string(int _target_18182)
{
    int _c_18183 = NOVALUE;
    int _i_18184 = NOVALUE;
    int _where_inlined_where_at_29_18190 = NOVALUE;
    int _10488 = NOVALUE;
    int _10487 = NOVALUE;
    int _10484 = NOVALUE;
    int _10482 = NOVALUE;
    int _10480 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = 0*/
    _i_18184 = 0;

    /** 	while c with entry do*/
    goto L1; // [10] 98
L2: 
    if (_c_18183 == 0)
    {
        goto L3; // [15] 110
    }
    else{
    }

    /** 		if c = -1 then*/
    if (_c_18183 != -1)
    goto L4; // [20] 56

    /** 			fatal(MISSING_END, "string is missing 0 terminator", "equal_string", {io:where(current_db)})*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_29_18190);
    _where_inlined_where_at_29_18190 = machine(20, _51current_db_18100);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_where_inlined_where_at_29_18190);
    *((int *)(_2+4)) = _where_inlined_where_at_29_18190;
    _10480 = MAKE_SEQ(_1);
    RefDS(_10468);
    RefDS(_10479);
    _51fatal(900, _10468, _10479, _10480);
    _10480 = NOVALUE;

    /** 			return DB_FATAL_FAIL*/
    DeRefDS(_target_18182);
    return -404;
L4: 

    /** 		i += 1*/
    _i_18184 = _i_18184 + 1;

    /** 		if i > length(target) then*/
    if (IS_SEQUENCE(_target_18182)){
            _10482 = SEQ_PTR(_target_18182)->length;
    }
    else {
        _10482 = 1;
    }
    if (_i_18184 <= _10482)
    goto L5; // [67] 78

    /** 			return 0*/
    DeRefDS(_target_18182);
    return 0;
L5: 

    /** 		if target[i] != c then*/
    _2 = (int)SEQ_PTR(_target_18182);
    _10484 = (int)*(((s1_ptr)_2)->base + _i_18184);
    if (binary_op_a(EQUALS, _10484, _c_18183)){
        _10484 = NOVALUE;
        goto L6; // [84] 95
    }
    _10484 = NOVALUE;

    /** 			return 0*/
    DeRefDS(_target_18182);
    return 0;
L6: 

    /** 	  entry*/
L1: 

    /** 		c = getc(current_db)*/
    if (_51current_db_18100 != last_r_file_no) {
        last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
        last_r_file_no = _51current_db_18100;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_18183 = getKBchar();
        }
        else
        _c_18183 = getc(last_r_file_ptr);
    }
    else
    _c_18183 = getc(last_r_file_ptr);

    /** 	end while*/
    goto L2; // [107] 13
L3: 

    /** 	return (i = length(target))*/
    if (IS_SEQUENCE(_target_18182)){
            _10487 = SEQ_PTR(_target_18182)->length;
    }
    else {
        _10487 = 1;
    }
    _10488 = (_i_18184 == _10487);
    _10487 = NOVALUE;
    DeRefDS(_target_18182);
    return _10488;
    ;
}


int _51decompress(int _c_18241)
{
    int _s_18242 = NOVALUE;
    int _len_18243 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_176_18279 = NOVALUE;
    int _ieee32_inlined_float32_to_atom_at_173_18278 = NOVALUE;
    int _float64_to_atom_inlined_float64_to_atom_at_251_18292 = NOVALUE;
    int _ieee64_inlined_float64_to_atom_at_248_18291 = NOVALUE;
    int _10557 = NOVALUE;
    int _10556 = NOVALUE;
    int _10555 = NOVALUE;
    int _10552 = NOVALUE;
    int _10547 = NOVALUE;
    int _10546 = NOVALUE;
    int _10545 = NOVALUE;
    int _10544 = NOVALUE;
    int _10543 = NOVALUE;
    int _10542 = NOVALUE;
    int _10541 = NOVALUE;
    int _10540 = NOVALUE;
    int _10539 = NOVALUE;
    int _10538 = NOVALUE;
    int _10537 = NOVALUE;
    int _10536 = NOVALUE;
    int _10535 = NOVALUE;
    int _10534 = NOVALUE;
    int _10533 = NOVALUE;
    int _10532 = NOVALUE;
    int _10531 = NOVALUE;
    int _10530 = NOVALUE;
    int _10529 = NOVALUE;
    int _10528 = NOVALUE;
    int _10526 = NOVALUE;
    int _10525 = NOVALUE;
    int _10524 = NOVALUE;
    int _10523 = NOVALUE;
    int _10522 = NOVALUE;
    int _10521 = NOVALUE;
    int _10520 = NOVALUE;
    int _10519 = NOVALUE;
    int _10518 = NOVALUE;
    int _10515 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if c = 0 then*/
    if (_c_18241 != 0)
    goto L1; // [5] 34

    /** 		c = getc(current_db)*/
    if (_51current_db_18100 != last_r_file_no) {
        last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
        last_r_file_no = _51current_db_18100;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _c_18241 = getKBchar();
        }
        else
        _c_18241 = getc(last_r_file_ptr);
    }
    else
    _c_18241 = getc(last_r_file_ptr);

    /** 		if c < I2B then*/
    if (_c_18241 >= 249)
    goto L2; // [18] 33

    /** 			return c + MIN1B*/
    _10515 = _c_18241 + -9;
    DeRef(_s_18242);
    return _10515;
L2: 
L1: 

    /** 	switch c with fallthru do*/
    _0 = _c_18241;
    switch ( _0 ){ 

        /** 		case I2B then*/
        case 249:

        /** 			return getc(current_db) +*/
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10518 = getKBchar();
            }
            else
            _10518 = getc(last_r_file_ptr);
        }
        else
        _10518 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10519 = getKBchar();
            }
            else
            _10519 = getc(last_r_file_ptr);
        }
        else
        _10519 = getc(last_r_file_ptr);
        _10520 = 256 * _10519;
        _10519 = NOVALUE;
        _10521 = _10518 + _10520;
        _10518 = NOVALUE;
        _10520 = NOVALUE;
        _10522 = _10521 + _51MIN2B_18221;
        if ((long)((unsigned long)_10522 + (unsigned long)HIGH_BITS) >= 0) 
        _10522 = NewDouble((double)_10522);
        _10521 = NOVALUE;
        DeRef(_s_18242);
        DeRef(_10515);
        _10515 = NOVALUE;
        return _10522;

        /** 		case I3B then*/
        case 250:

        /** 			return getc(current_db) +*/
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10523 = getKBchar();
            }
            else
            _10523 = getc(last_r_file_ptr);
        }
        else
        _10523 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10524 = getKBchar();
            }
            else
            _10524 = getc(last_r_file_ptr);
        }
        else
        _10524 = getc(last_r_file_ptr);
        _10525 = 256 * _10524;
        _10524 = NOVALUE;
        _10526 = _10523 + _10525;
        _10523 = NOVALUE;
        _10525 = NOVALUE;
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10528 = getKBchar();
            }
            else
            _10528 = getc(last_r_file_ptr);
        }
        else
        _10528 = getc(last_r_file_ptr);
        _10529 = 65536 * _10528;
        _10528 = NOVALUE;
        _10530 = _10526 + _10529;
        _10526 = NOVALUE;
        _10529 = NOVALUE;
        _10531 = _10530 + _51MIN3B_18228;
        if ((long)((unsigned long)_10531 + (unsigned long)HIGH_BITS) >= 0) 
        _10531 = NewDouble((double)_10531);
        _10530 = NOVALUE;
        DeRef(_s_18242);
        DeRef(_10515);
        _10515 = NOVALUE;
        DeRef(_10522);
        _10522 = NOVALUE;
        return _10531;

        /** 		case I4B then*/
        case 251:

        /** 			return get4() + MIN4B*/
        _10532 = _51get4();
        if (IS_ATOM_INT(_10532) && IS_ATOM_INT(_51MIN4B_18235)) {
            _10533 = _10532 + _51MIN4B_18235;
            if ((long)((unsigned long)_10533 + (unsigned long)HIGH_BITS) >= 0) 
            _10533 = NewDouble((double)_10533);
        }
        else {
            _10533 = binary_op(PLUS, _10532, _51MIN4B_18235);
        }
        DeRef(_10532);
        _10532 = NOVALUE;
        DeRef(_s_18242);
        DeRef(_10515);
        _10515 = NOVALUE;
        DeRef(_10522);
        _10522 = NOVALUE;
        DeRef(_10531);
        _10531 = NOVALUE;
        return _10533;

        /** 		case F4B then*/
        case 252:

        /** 			return convert:float32_to_atom({getc(current_db), getc(current_db),*/
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10534 = getKBchar();
            }
            else
            _10534 = getc(last_r_file_ptr);
        }
        else
        _10534 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10535 = getKBchar();
            }
            else
            _10535 = getc(last_r_file_ptr);
        }
        else
        _10535 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10536 = getKBchar();
            }
            else
            _10536 = getc(last_r_file_ptr);
        }
        else
        _10536 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10537 = getKBchar();
            }
            else
            _10537 = getc(last_r_file_ptr);
        }
        else
        _10537 = getc(last_r_file_ptr);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _10534;
        *((int *)(_2+8)) = _10535;
        *((int *)(_2+12)) = _10536;
        *((int *)(_2+16)) = _10537;
        _10538 = MAKE_SEQ(_1);
        _10537 = NOVALUE;
        _10536 = NOVALUE;
        _10535 = NOVALUE;
        _10534 = NOVALUE;
        DeRefi(_ieee32_inlined_float32_to_atom_at_173_18278);
        _ieee32_inlined_float32_to_atom_at_173_18278 = _10538;
        _10538 = NOVALUE;

        /** 	return machine_func(M_F32_TO_A, ieee32)*/
        DeRef(_float32_to_atom_inlined_float32_to_atom_at_176_18279);
        _float32_to_atom_inlined_float32_to_atom_at_176_18279 = machine(49, _ieee32_inlined_float32_to_atom_at_173_18278);
        DeRefi(_ieee32_inlined_float32_to_atom_at_173_18278);
        _ieee32_inlined_float32_to_atom_at_173_18278 = NOVALUE;
        DeRef(_s_18242);
        DeRef(_10515);
        _10515 = NOVALUE;
        DeRef(_10522);
        _10522 = NOVALUE;
        DeRef(_10531);
        _10531 = NOVALUE;
        DeRef(_10533);
        _10533 = NOVALUE;
        return _float32_to_atom_inlined_float32_to_atom_at_176_18279;

        /** 		case F8B then*/
        case 253:

        /** 			return convert:float64_to_atom({getc(current_db), getc(current_db),*/
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10539 = getKBchar();
            }
            else
            _10539 = getc(last_r_file_ptr);
        }
        else
        _10539 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10540 = getKBchar();
            }
            else
            _10540 = getc(last_r_file_ptr);
        }
        else
        _10540 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10541 = getKBchar();
            }
            else
            _10541 = getc(last_r_file_ptr);
        }
        else
        _10541 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10542 = getKBchar();
            }
            else
            _10542 = getc(last_r_file_ptr);
        }
        else
        _10542 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10543 = getKBchar();
            }
            else
            _10543 = getc(last_r_file_ptr);
        }
        else
        _10543 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10544 = getKBchar();
            }
            else
            _10544 = getc(last_r_file_ptr);
        }
        else
        _10544 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10545 = getKBchar();
            }
            else
            _10545 = getc(last_r_file_ptr);
        }
        else
        _10545 = getc(last_r_file_ptr);
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _10546 = getKBchar();
            }
            else
            _10546 = getc(last_r_file_ptr);
        }
        else
        _10546 = getc(last_r_file_ptr);
        _1 = NewS1(8);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _10539;
        *((int *)(_2+8)) = _10540;
        *((int *)(_2+12)) = _10541;
        *((int *)(_2+16)) = _10542;
        *((int *)(_2+20)) = _10543;
        *((int *)(_2+24)) = _10544;
        *((int *)(_2+28)) = _10545;
        *((int *)(_2+32)) = _10546;
        _10547 = MAKE_SEQ(_1);
        _10546 = NOVALUE;
        _10545 = NOVALUE;
        _10544 = NOVALUE;
        _10543 = NOVALUE;
        _10542 = NOVALUE;
        _10541 = NOVALUE;
        _10540 = NOVALUE;
        _10539 = NOVALUE;
        DeRefi(_ieee64_inlined_float64_to_atom_at_248_18291);
        _ieee64_inlined_float64_to_atom_at_248_18291 = _10547;
        _10547 = NOVALUE;

        /** 	return machine_func(M_F64_TO_A, ieee64)*/
        DeRef(_float64_to_atom_inlined_float64_to_atom_at_251_18292);
        _float64_to_atom_inlined_float64_to_atom_at_251_18292 = machine(47, _ieee64_inlined_float64_to_atom_at_248_18291);
        DeRefi(_ieee64_inlined_float64_to_atom_at_248_18291);
        _ieee64_inlined_float64_to_atom_at_248_18291 = NOVALUE;
        DeRef(_s_18242);
        DeRef(_10515);
        _10515 = NOVALUE;
        DeRef(_10522);
        _10522 = NOVALUE;
        DeRef(_10531);
        _10531 = NOVALUE;
        DeRef(_10533);
        _10533 = NOVALUE;
        return _float64_to_atom_inlined_float64_to_atom_at_251_18292;

        /** 		case else*/
        default:

        /** 			if c = S1B then*/
        if (_c_18241 != 254)
        goto L3; // [273] 287

        /** 				len = getc(current_db)*/
        if (_51current_db_18100 != last_r_file_no) {
            last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
            last_r_file_no = _51current_db_18100;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _len_18243 = getKBchar();
            }
            else
            _len_18243 = getc(last_r_file_ptr);
        }
        else
        _len_18243 = getc(last_r_file_ptr);
        goto L4; // [284] 295
L3: 

        /** 				len = get4()*/
        _len_18243 = _51get4();
        if (!IS_ATOM_INT(_len_18243)) {
            _1 = (long)(DBL_PTR(_len_18243)->dbl);
            if (UNIQUE(DBL_PTR(_len_18243)) && (DBL_PTR(_len_18243)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_len_18243);
            _len_18243 = _1;
        }
L4: 

        /** 			s = repeat(0, len)*/
        DeRef(_s_18242);
        _s_18242 = Repeat(0, _len_18243);

        /** 			for i = 1 to len do*/
        _10552 = _len_18243;
        {
            int _i_18301;
            _i_18301 = 1;
L5: 
            if (_i_18301 > _10552){
                goto L6; // [308] 362
            }

            /** 				c = getc(current_db)*/
            if (_51current_db_18100 != last_r_file_no) {
                last_r_file_ptr = which_file(_51current_db_18100, EF_READ);
                last_r_file_no = _51current_db_18100;
            }
            if (last_r_file_ptr == xstdin) {
                show_console();
                if (in_from_keyb) {
                    _c_18241 = getKBchar();
                }
                else
                _c_18241 = getc(last_r_file_ptr);
            }
            else
            _c_18241 = getc(last_r_file_ptr);

            /** 				if c < I2B then*/
            if (_c_18241 >= 249)
            goto L7; // [324] 341

            /** 					s[i] = c + MIN1B*/
            _10555 = _c_18241 + -9;
            _2 = (int)SEQ_PTR(_s_18242);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_18242 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_18301);
            _1 = *(int *)_2;
            *(int *)_2 = _10555;
            if( _1 != _10555 ){
                DeRef(_1);
            }
            _10555 = NOVALUE;
            goto L8; // [338] 355
L7: 

            /** 					s[i] = decompress(c)*/
            DeRef(_10556);
            _10556 = _c_18241;
            _10557 = _51decompress(_10556);
            _10556 = NOVALUE;
            _2 = (int)SEQ_PTR(_s_18242);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _s_18242 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _i_18301);
            _1 = *(int *)_2;
            *(int *)_2 = _10557;
            if( _1 != _10557 ){
                DeRef(_1);
            }
            _10557 = NOVALUE;
L8: 

            /** 			end for*/
            _i_18301 = _i_18301 + 1;
            goto L5; // [357] 315
L6: 
            ;
        }

        /** 			return s*/
        DeRef(_10515);
        _10515 = NOVALUE;
        DeRef(_10522);
        _10522 = NOVALUE;
        DeRef(_10531);
        _10531 = NOVALUE;
        DeRef(_10533);
        _10533 = NOVALUE;
        return _s_18242;
    ;}    ;
}


int _51compress(int _x_18312)
{
    int _x4_18313 = NOVALUE;
    int _s_18314 = NOVALUE;
    int _atom_to_float32_inlined_atom_to_float32_at_192_18348 = NOVALUE;
    int _float32_to_atom_inlined_float32_to_atom_at_203_18351 = NOVALUE;
    int _atom_to_float64_inlined_atom_to_float64_at_229_18356 = NOVALUE;
    int _10596 = NOVALUE;
    int _10595 = NOVALUE;
    int _10594 = NOVALUE;
    int _10592 = NOVALUE;
    int _10591 = NOVALUE;
    int _10589 = NOVALUE;
    int _10587 = NOVALUE;
    int _10586 = NOVALUE;
    int _10585 = NOVALUE;
    int _10583 = NOVALUE;
    int _10582 = NOVALUE;
    int _10581 = NOVALUE;
    int _10580 = NOVALUE;
    int _10579 = NOVALUE;
    int _10578 = NOVALUE;
    int _10577 = NOVALUE;
    int _10576 = NOVALUE;
    int _10575 = NOVALUE;
    int _10573 = NOVALUE;
    int _10572 = NOVALUE;
    int _10571 = NOVALUE;
    int _10570 = NOVALUE;
    int _10569 = NOVALUE;
    int _10568 = NOVALUE;
    int _10566 = NOVALUE;
    int _10565 = NOVALUE;
    int _10564 = NOVALUE;
    int _10563 = NOVALUE;
    int _10562 = NOVALUE;
    int _10561 = NOVALUE;
    int _10560 = NOVALUE;
    int _10559 = NOVALUE;
    int _10558 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if integer(x) then*/
    if (IS_ATOM_INT(_x_18312))
    _10558 = 1;
    else if (IS_ATOM_DBL(_x_18312))
    _10558 = IS_ATOM_INT(DoubleToInt(_x_18312));
    else
    _10558 = 0;
    if (_10558 == 0)
    {
        _10558 = NOVALUE;
        goto L1; // [6] 183
    }
    else{
        _10558 = NOVALUE;
    }

    /** 		if x >= MIN1B and x <= MAX1B then*/
    if (IS_ATOM_INT(_x_18312)) {
        _10559 = (_x_18312 >= -9);
    }
    else {
        _10559 = binary_op(GREATEREQ, _x_18312, -9);
    }
    if (IS_ATOM_INT(_10559)) {
        if (_10559 == 0) {
            goto L2; // [15] 44
        }
    }
    else {
        if (DBL_PTR(_10559)->dbl == 0.0) {
            goto L2; // [15] 44
        }
    }
    if (IS_ATOM_INT(_x_18312)) {
        _10561 = (_x_18312 <= 239);
    }
    else {
        _10561 = binary_op(LESSEQ, _x_18312, 239);
    }
    if (_10561 == 0) {
        DeRef(_10561);
        _10561 = NOVALUE;
        goto L2; // [24] 44
    }
    else {
        if (!IS_ATOM_INT(_10561) && DBL_PTR(_10561)->dbl == 0.0){
            DeRef(_10561);
            _10561 = NOVALUE;
            goto L2; // [24] 44
        }
        DeRef(_10561);
        _10561 = NOVALUE;
    }
    DeRef(_10561);
    _10561 = NOVALUE;

    /** 			return {x - MIN1B}*/
    if (IS_ATOM_INT(_x_18312)) {
        _10562 = _x_18312 - -9;
        if ((long)((unsigned long)_10562 +(unsigned long) HIGH_BITS) >= 0){
            _10562 = NewDouble((double)_10562);
        }
    }
    else {
        _10562 = binary_op(MINUS, _x_18312, -9);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _10562;
    _10563 = MAKE_SEQ(_1);
    _10562 = NOVALUE;
    DeRef(_x_18312);
    DeRefi(_x4_18313);
    DeRef(_s_18314);
    DeRef(_10559);
    _10559 = NOVALUE;
    return _10563;
    goto L3; // [41] 328
L2: 

    /** 		elsif x >= MIN2B and x <= MAX2B then*/
    if (IS_ATOM_INT(_x_18312)) {
        _10564 = (_x_18312 >= _51MIN2B_18221);
    }
    else {
        _10564 = binary_op(GREATEREQ, _x_18312, _51MIN2B_18221);
    }
    if (IS_ATOM_INT(_10564)) {
        if (_10564 == 0) {
            goto L4; // [52] 97
        }
    }
    else {
        if (DBL_PTR(_10564)->dbl == 0.0) {
            goto L4; // [52] 97
        }
    }
    if (IS_ATOM_INT(_x_18312)) {
        _10566 = (_x_18312 <= 32767);
    }
    else {
        _10566 = binary_op(LESSEQ, _x_18312, 32767);
    }
    if (_10566 == 0) {
        DeRef(_10566);
        _10566 = NOVALUE;
        goto L4; // [63] 97
    }
    else {
        if (!IS_ATOM_INT(_10566) && DBL_PTR(_10566)->dbl == 0.0){
            DeRef(_10566);
            _10566 = NOVALUE;
            goto L4; // [63] 97
        }
        DeRef(_10566);
        _10566 = NOVALUE;
    }
    DeRef(_10566);
    _10566 = NOVALUE;

    /** 			x -= MIN2B*/
    _0 = _x_18312;
    if (IS_ATOM_INT(_x_18312)) {
        _x_18312 = _x_18312 - _51MIN2B_18221;
        if ((long)((unsigned long)_x_18312 +(unsigned long) HIGH_BITS) >= 0){
            _x_18312 = NewDouble((double)_x_18312);
        }
    }
    else {
        _x_18312 = binary_op(MINUS, _x_18312, _51MIN2B_18221);
    }
    DeRef(_0);

    /** 			return {I2B, and_bits(x, #FF), floor(x / #100)}*/
    if (IS_ATOM_INT(_x_18312)) {
        {unsigned long tu;
             tu = (unsigned long)_x_18312 & (unsigned long)255;
             _10568 = MAKE_UINT(tu);
        }
    }
    else {
        _10568 = binary_op(AND_BITS, _x_18312, 255);
    }
    if (IS_ATOM_INT(_x_18312)) {
        if (256 > 0 && _x_18312 >= 0) {
            _10569 = _x_18312 / 256;
        }
        else {
            temp_dbl = floor((double)_x_18312 / (double)256);
            if (_x_18312 != MININT)
            _10569 = (long)temp_dbl;
            else
            _10569 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_18312, 256);
        _10569 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 249;
    *((int *)(_2+8)) = _10568;
    *((int *)(_2+12)) = _10569;
    _10570 = MAKE_SEQ(_1);
    _10569 = NOVALUE;
    _10568 = NOVALUE;
    DeRef(_x_18312);
    DeRefi(_x4_18313);
    DeRef(_s_18314);
    DeRef(_10559);
    _10559 = NOVALUE;
    DeRef(_10563);
    _10563 = NOVALUE;
    DeRef(_10564);
    _10564 = NOVALUE;
    return _10570;
    goto L3; // [94] 328
L4: 

    /** 		elsif x >= MIN3B and x <= MAX3B then*/
    if (IS_ATOM_INT(_x_18312)) {
        _10571 = (_x_18312 >= _51MIN3B_18228);
    }
    else {
        _10571 = binary_op(GREATEREQ, _x_18312, _51MIN3B_18228);
    }
    if (IS_ATOM_INT(_10571)) {
        if (_10571 == 0) {
            goto L5; // [105] 159
        }
    }
    else {
        if (DBL_PTR(_10571)->dbl == 0.0) {
            goto L5; // [105] 159
        }
    }
    if (IS_ATOM_INT(_x_18312)) {
        _10573 = (_x_18312 <= 8388607);
    }
    else {
        _10573 = binary_op(LESSEQ, _x_18312, 8388607);
    }
    if (_10573 == 0) {
        DeRef(_10573);
        _10573 = NOVALUE;
        goto L5; // [116] 159
    }
    else {
        if (!IS_ATOM_INT(_10573) && DBL_PTR(_10573)->dbl == 0.0){
            DeRef(_10573);
            _10573 = NOVALUE;
            goto L5; // [116] 159
        }
        DeRef(_10573);
        _10573 = NOVALUE;
    }
    DeRef(_10573);
    _10573 = NOVALUE;

    /** 			x -= MIN3B*/
    _0 = _x_18312;
    if (IS_ATOM_INT(_x_18312)) {
        _x_18312 = _x_18312 - _51MIN3B_18228;
        if ((long)((unsigned long)_x_18312 +(unsigned long) HIGH_BITS) >= 0){
            _x_18312 = NewDouble((double)_x_18312);
        }
    }
    else {
        _x_18312 = binary_op(MINUS, _x_18312, _51MIN3B_18228);
    }
    DeRef(_0);

    /** 			return {I3B, and_bits(x, #FF), and_bits(floor(x / #100), #FF), floor(x / #10000)}*/
    if (IS_ATOM_INT(_x_18312)) {
        {unsigned long tu;
             tu = (unsigned long)_x_18312 & (unsigned long)255;
             _10575 = MAKE_UINT(tu);
        }
    }
    else {
        _10575 = binary_op(AND_BITS, _x_18312, 255);
    }
    if (IS_ATOM_INT(_x_18312)) {
        if (256 > 0 && _x_18312 >= 0) {
            _10576 = _x_18312 / 256;
        }
        else {
            temp_dbl = floor((double)_x_18312 / (double)256);
            if (_x_18312 != MININT)
            _10576 = (long)temp_dbl;
            else
            _10576 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_18312, 256);
        _10576 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (IS_ATOM_INT(_10576)) {
        {unsigned long tu;
             tu = (unsigned long)_10576 & (unsigned long)255;
             _10577 = MAKE_UINT(tu);
        }
    }
    else {
        _10577 = binary_op(AND_BITS, _10576, 255);
    }
    DeRef(_10576);
    _10576 = NOVALUE;
    if (IS_ATOM_INT(_x_18312)) {
        if (65536 > 0 && _x_18312 >= 0) {
            _10578 = _x_18312 / 65536;
        }
        else {
            temp_dbl = floor((double)_x_18312 / (double)65536);
            if (_x_18312 != MININT)
            _10578 = (long)temp_dbl;
            else
            _10578 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _x_18312, 65536);
        _10578 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 250;
    *((int *)(_2+8)) = _10575;
    *((int *)(_2+12)) = _10577;
    *((int *)(_2+16)) = _10578;
    _10579 = MAKE_SEQ(_1);
    _10578 = NOVALUE;
    _10577 = NOVALUE;
    _10575 = NOVALUE;
    DeRef(_x_18312);
    DeRefi(_x4_18313);
    DeRef(_s_18314);
    DeRef(_10559);
    _10559 = NOVALUE;
    DeRef(_10563);
    _10563 = NOVALUE;
    DeRef(_10564);
    _10564 = NOVALUE;
    DeRef(_10570);
    _10570 = NOVALUE;
    DeRef(_10571);
    _10571 = NOVALUE;
    return _10579;
    goto L3; // [156] 328
L5: 

    /** 			return I4B & convert:int_to_bytes(x-MIN4B)*/
    if (IS_ATOM_INT(_x_18312) && IS_ATOM_INT(_51MIN4B_18235)) {
        _10580 = _x_18312 - _51MIN4B_18235;
        if ((long)((unsigned long)_10580 +(unsigned long) HIGH_BITS) >= 0){
            _10580 = NewDouble((double)_10580);
        }
    }
    else {
        _10580 = binary_op(MINUS, _x_18312, _51MIN4B_18235);
    }
    _10581 = _11int_to_bytes(_10580);
    _10580 = NOVALUE;
    if (IS_SEQUENCE(251) && IS_ATOM(_10581)) {
    }
    else if (IS_ATOM(251) && IS_SEQUENCE(_10581)) {
        Prepend(&_10582, _10581, 251);
    }
    else {
        Concat((object_ptr)&_10582, 251, _10581);
    }
    DeRef(_10581);
    _10581 = NOVALUE;
    DeRef(_x_18312);
    DeRefi(_x4_18313);
    DeRef(_s_18314);
    DeRef(_10559);
    _10559 = NOVALUE;
    DeRef(_10563);
    _10563 = NOVALUE;
    DeRef(_10564);
    _10564 = NOVALUE;
    DeRef(_10570);
    _10570 = NOVALUE;
    DeRef(_10571);
    _10571 = NOVALUE;
    DeRef(_10579);
    _10579 = NOVALUE;
    return _10582;
    goto L3; // [180] 328
L1: 

    /** 	elsif atom(x) then*/
    _10583 = IS_ATOM(_x_18312);
    if (_10583 == 0)
    {
        _10583 = NOVALUE;
        goto L6; // [188] 249
    }
    else{
        _10583 = NOVALUE;
    }

    /** 		x4 = convert:atom_to_float32(x)*/

    /** 	return machine_func(M_A_TO_F32, a)*/
    DeRefi(_x4_18313);
    _x4_18313 = machine(48, _x_18312);

    /** 		if x = convert:float32_to_atom(x4) then*/

    /** 	return machine_func(M_F32_TO_A, ieee32)*/
    DeRef(_float32_to_atom_inlined_float32_to_atom_at_203_18351);
    _float32_to_atom_inlined_float32_to_atom_at_203_18351 = machine(49, _x4_18313);
    if (binary_op_a(NOTEQ, _x_18312, _float32_to_atom_inlined_float32_to_atom_at_203_18351)){
        goto L7; // [211] 228
    }

    /** 			return F4B & x4*/
    Prepend(&_10585, _x4_18313, 252);
    DeRef(_x_18312);
    DeRefDSi(_x4_18313);
    DeRef(_s_18314);
    DeRef(_10559);
    _10559 = NOVALUE;
    DeRef(_10563);
    _10563 = NOVALUE;
    DeRef(_10564);
    _10564 = NOVALUE;
    DeRef(_10570);
    _10570 = NOVALUE;
    DeRef(_10571);
    _10571 = NOVALUE;
    DeRef(_10579);
    _10579 = NOVALUE;
    DeRef(_10582);
    _10582 = NOVALUE;
    return _10585;
    goto L3; // [225] 328
L7: 

    /** 			return F8B & convert:atom_to_float64(x)*/

    /** 	return machine_func(M_A_TO_F64, a)*/
    DeRefi(_atom_to_float64_inlined_atom_to_float64_at_229_18356);
    _atom_to_float64_inlined_atom_to_float64_at_229_18356 = machine(46, _x_18312);
    Prepend(&_10586, _atom_to_float64_inlined_atom_to_float64_at_229_18356, 253);
    DeRef(_x_18312);
    DeRefi(_x4_18313);
    DeRef(_s_18314);
    DeRef(_10559);
    _10559 = NOVALUE;
    DeRef(_10563);
    _10563 = NOVALUE;
    DeRef(_10564);
    _10564 = NOVALUE;
    DeRef(_10570);
    _10570 = NOVALUE;
    DeRef(_10571);
    _10571 = NOVALUE;
    DeRef(_10579);
    _10579 = NOVALUE;
    DeRef(_10582);
    _10582 = NOVALUE;
    DeRef(_10585);
    _10585 = NOVALUE;
    return _10586;
    goto L3; // [246] 328
L6: 

    /** 		if length(x) <= 255 then*/
    if (IS_SEQUENCE(_x_18312)){
            _10587 = SEQ_PTR(_x_18312)->length;
    }
    else {
        _10587 = 1;
    }
    if (_10587 > 255)
    goto L8; // [254] 270

    /** 			s = {S1B, length(x)}*/
    if (IS_SEQUENCE(_x_18312)){
            _10589 = SEQ_PTR(_x_18312)->length;
    }
    else {
        _10589 = 1;
    }
    DeRef(_s_18314);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 254;
    ((int *)_2)[2] = _10589;
    _s_18314 = MAKE_SEQ(_1);
    _10589 = NOVALUE;
    goto L9; // [267] 284
L8: 

    /** 			s = S4B & convert:int_to_bytes(length(x))*/
    if (IS_SEQUENCE(_x_18312)){
            _10591 = SEQ_PTR(_x_18312)->length;
    }
    else {
        _10591 = 1;
    }
    _10592 = _11int_to_bytes(_10591);
    _10591 = NOVALUE;
    if (IS_SEQUENCE(255) && IS_ATOM(_10592)) {
    }
    else if (IS_ATOM(255) && IS_SEQUENCE(_10592)) {
        Prepend(&_s_18314, _10592, 255);
    }
    else {
        Concat((object_ptr)&_s_18314, 255, _10592);
    }
    DeRef(_10592);
    _10592 = NOVALUE;
L9: 

    /** 		for i = 1 to length(x) do*/
    if (IS_SEQUENCE(_x_18312)){
            _10594 = SEQ_PTR(_x_18312)->length;
    }
    else {
        _10594 = 1;
    }
    {
        int _i_18369;
        _i_18369 = 1;
LA: 
        if (_i_18369 > _10594){
            goto LB; // [289] 319
        }

        /** 			s &= compress(x[i])*/
        _2 = (int)SEQ_PTR(_x_18312);
        _10595 = (int)*(((s1_ptr)_2)->base + _i_18369);
        Ref(_10595);
        _10596 = _51compress(_10595);
        _10595 = NOVALUE;
        if (IS_SEQUENCE(_s_18314) && IS_ATOM(_10596)) {
            Ref(_10596);
            Append(&_s_18314, _s_18314, _10596);
        }
        else if (IS_ATOM(_s_18314) && IS_SEQUENCE(_10596)) {
        }
        else {
            Concat((object_ptr)&_s_18314, _s_18314, _10596);
        }
        DeRef(_10596);
        _10596 = NOVALUE;

        /** 		end for*/
        _i_18369 = _i_18369 + 1;
        goto LA; // [314] 296
LB: 
        ;
    }

    /** 		return s*/
    DeRef(_x_18312);
    DeRefi(_x4_18313);
    DeRef(_10559);
    _10559 = NOVALUE;
    DeRef(_10563);
    _10563 = NOVALUE;
    DeRef(_10564);
    _10564 = NOVALUE;
    DeRef(_10570);
    _10570 = NOVALUE;
    DeRef(_10571);
    _10571 = NOVALUE;
    DeRef(_10579);
    _10579 = NOVALUE;
    DeRef(_10582);
    _10582 = NOVALUE;
    DeRef(_10585);
    _10585 = NOVALUE;
    DeRef(_10586);
    _10586 = NOVALUE;
    return _s_18314;
L3: 
    ;
}


int _51db_allocate(int _n_18753)
{
    int _free_list_18754 = NOVALUE;
    int _size_18755 = NOVALUE;
    int _size_ptr_18756 = NOVALUE;
    int _addr_18757 = NOVALUE;
    int _free_count_18758 = NOVALUE;
    int _remaining_18759 = NOVALUE;
    int _seek_1__tmp_at4_18762 = NOVALUE;
    int _seek_inlined_seek_at_4_18761 = NOVALUE;
    int _seek_1__tmp_at39_18769 = NOVALUE;
    int _seek_inlined_seek_at_39_18768 = NOVALUE;
    int _seek_1__tmp_at111_18786 = NOVALUE;
    int _seek_inlined_seek_at_111_18785 = NOVALUE;
    int _pos_inlined_seek_at_108_18784 = NOVALUE;
    int _put4_1__tmp_at137_18791 = NOVALUE;
    int _x_inlined_put4_at_134_18790 = NOVALUE;
    int _seek_1__tmp_at167_18794 = NOVALUE;
    int _seek_inlined_seek_at_167_18793 = NOVALUE;
    int _put4_1__tmp_at193_18799 = NOVALUE;
    int _x_inlined_put4_at_190_18798 = NOVALUE;
    int _seek_1__tmp_at244_18807 = NOVALUE;
    int _seek_inlined_seek_at_244_18806 = NOVALUE;
    int _pos_inlined_seek_at_241_18805 = NOVALUE;
    int _put4_1__tmp_at266_18811 = NOVALUE;
    int _x_inlined_put4_at_263_18810 = NOVALUE;
    int _seek_1__tmp_at333_18822 = NOVALUE;
    int _seek_inlined_seek_at_333_18821 = NOVALUE;
    int _pos_inlined_seek_at_330_18820 = NOVALUE;
    int _seek_1__tmp_at364_18826 = NOVALUE;
    int _seek_inlined_seek_at_364_18825 = NOVALUE;
    int _put4_1__tmp_at386_18830 = NOVALUE;
    int _x_inlined_put4_at_383_18829 = NOVALUE;
    int _seek_1__tmp_at423_18835 = NOVALUE;
    int _seek_inlined_seek_at_423_18834 = NOVALUE;
    int _pos_inlined_seek_at_420_18833 = NOVALUE;
    int _put4_1__tmp_at438_18837 = NOVALUE;
    int _seek_1__tmp_at490_18841 = NOVALUE;
    int _seek_inlined_seek_at_490_18840 = NOVALUE;
    int _put4_1__tmp_at512_18845 = NOVALUE;
    int _x_inlined_put4_at_509_18844 = NOVALUE;
    int _where_inlined_where_at_542_18847 = NOVALUE;
    int _10808 = NOVALUE;
    int _10806 = NOVALUE;
    int _10805 = NOVALUE;
    int _10804 = NOVALUE;
    int _10803 = NOVALUE;
    int _10802 = NOVALUE;
    int _10800 = NOVALUE;
    int _10799 = NOVALUE;
    int _10798 = NOVALUE;
    int _10797 = NOVALUE;
    int _10795 = NOVALUE;
    int _10794 = NOVALUE;
    int _10793 = NOVALUE;
    int _10792 = NOVALUE;
    int _10791 = NOVALUE;
    int _10790 = NOVALUE;
    int _10789 = NOVALUE;
    int _10787 = NOVALUE;
    int _10785 = NOVALUE;
    int _10782 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_18762);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at4_18762 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_18761 = machine(19, _seek_1__tmp_at4_18762);
    DeRefi(_seek_1__tmp_at4_18762);
    _seek_1__tmp_at4_18762 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_18758 = _51get4();
    if (!IS_ATOM_INT(_free_count_18758)) {
        _1 = (long)(DBL_PTR(_free_count_18758)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_18758)) && (DBL_PTR(_free_count_18758)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_18758);
        _free_count_18758 = _1;
    }

    /** 	if free_count > 0 then*/
    if (_free_count_18758 <= 0)
    goto L1; // [27] 487

    /** 		free_list = get4()*/
    _0 = _free_list_18754;
    _free_list_18754 = _51get4();
    DeRef(_0);

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_18754);
    DeRef(_seek_1__tmp_at39_18769);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _free_list_18754;
    _seek_1__tmp_at39_18769 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_39_18768 = machine(19, _seek_1__tmp_at39_18769);
    DeRef(_seek_1__tmp_at39_18769);
    _seek_1__tmp_at39_18769 = NOVALUE;

    /** 		size_ptr = free_list + 4*/
    DeRef(_size_ptr_18756);
    if (IS_ATOM_INT(_free_list_18754)) {
        _size_ptr_18756 = _free_list_18754 + 4;
        if ((long)((unsigned long)_size_ptr_18756 + (unsigned long)HIGH_BITS) >= 0) 
        _size_ptr_18756 = NewDouble((double)_size_ptr_18756);
    }
    else {
        _size_ptr_18756 = NewDouble(DBL_PTR(_free_list_18754)->dbl + (double)4);
    }

    /** 		for i = 1 to free_count do*/
    _10782 = _free_count_18758;
    {
        int _i_18772;
        _i_18772 = 1;
L2: 
        if (_i_18772 > _10782){
            goto L3; // [64] 486
        }

        /** 			addr = get4()*/
        _0 = _addr_18757;
        _addr_18757 = _51get4();
        DeRef(_0);

        /** 			size = get4()*/
        _0 = _size_18755;
        _size_18755 = _51get4();
        DeRef(_0);

        /** 			if size >= n+4 then*/
        if (IS_ATOM_INT(_n_18753)) {
            _10785 = _n_18753 + 4;
            if ((long)((unsigned long)_10785 + (unsigned long)HIGH_BITS) >= 0) 
            _10785 = NewDouble((double)_10785);
        }
        else {
            _10785 = NewDouble(DBL_PTR(_n_18753)->dbl + (double)4);
        }
        if (binary_op_a(LESS, _size_18755, _10785)){
            DeRef(_10785);
            _10785 = NOVALUE;
            goto L4; // [87] 473
        }
        DeRef(_10785);
        _10785 = NOVALUE;

        /** 				if size >= n+16 then*/
        if (IS_ATOM_INT(_n_18753)) {
            _10787 = _n_18753 + 16;
            if ((long)((unsigned long)_10787 + (unsigned long)HIGH_BITS) >= 0) 
            _10787 = NewDouble((double)_10787);
        }
        else {
            _10787 = NewDouble(DBL_PTR(_n_18753)->dbl + (double)16);
        }
        if (binary_op_a(LESS, _size_18755, _10787)){
            DeRef(_10787);
            _10787 = NOVALUE;
            goto L5; // [97] 296
        }
        DeRef(_10787);
        _10787 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_18757)) {
            _10789 = _addr_18757 - 4;
            if ((long)((unsigned long)_10789 +(unsigned long) HIGH_BITS) >= 0){
                _10789 = NewDouble((double)_10789);
            }
        }
        else {
            _10789 = NewDouble(DBL_PTR(_addr_18757)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_108_18784);
        _pos_inlined_seek_at_108_18784 = _10789;
        _10789 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_108_18784);
        DeRef(_seek_1__tmp_at111_18786);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _pos_inlined_seek_at_108_18784;
        _seek_1__tmp_at111_18786 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_111_18785 = machine(19, _seek_1__tmp_at111_18786);
        DeRef(_pos_inlined_seek_at_108_18784);
        _pos_inlined_seek_at_108_18784 = NOVALUE;
        DeRef(_seek_1__tmp_at111_18786);
        _seek_1__tmp_at111_18786 = NOVALUE;

        /** 					put4(size-n-4) -- shrink the block*/
        if (IS_ATOM_INT(_size_18755) && IS_ATOM_INT(_n_18753)) {
            _10790 = _size_18755 - _n_18753;
            if ((long)((unsigned long)_10790 +(unsigned long) HIGH_BITS) >= 0){
                _10790 = NewDouble((double)_10790);
            }
        }
        else {
            if (IS_ATOM_INT(_size_18755)) {
                _10790 = NewDouble((double)_size_18755 - DBL_PTR(_n_18753)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_18753)) {
                    _10790 = NewDouble(DBL_PTR(_size_18755)->dbl - (double)_n_18753);
                }
                else
                _10790 = NewDouble(DBL_PTR(_size_18755)->dbl - DBL_PTR(_n_18753)->dbl);
            }
        }
        if (IS_ATOM_INT(_10790)) {
            _10791 = _10790 - 4;
            if ((long)((unsigned long)_10791 +(unsigned long) HIGH_BITS) >= 0){
                _10791 = NewDouble((double)_10791);
            }
        }
        else {
            _10791 = NewDouble(DBL_PTR(_10790)->dbl - (double)4);
        }
        DeRef(_10790);
        _10790 = NOVALUE;
        DeRef(_x_inlined_put4_at_134_18790);
        _x_inlined_put4_at_134_18790 = _10791;
        _10791 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_51mem0_18142)){
            poke4_addr = (unsigned long *)_51mem0_18142;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_134_18790)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_134_18790;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_134_18790)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at137_18791);
        _1 = (int)SEQ_PTR(_51memseq_18377);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at137_18791 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_51current_db_18100, _put4_1__tmp_at137_18791); // DJP 

        /** end procedure*/
        goto L6; // [159] 162
L6: 
        DeRef(_x_inlined_put4_at_134_18790);
        _x_inlined_put4_at_134_18790 = NOVALUE;
        DeRefi(_put4_1__tmp_at137_18791);
        _put4_1__tmp_at137_18791 = NOVALUE;

        /** 					io:seek(current_db, size_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_size_ptr_18756);
        DeRef(_seek_1__tmp_at167_18794);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _size_ptr_18756;
        _seek_1__tmp_at167_18794 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_167_18793 = machine(19, _seek_1__tmp_at167_18794);
        DeRef(_seek_1__tmp_at167_18794);
        _seek_1__tmp_at167_18794 = NOVALUE;

        /** 					put4(size-n-4) -- update size on free list too*/
        if (IS_ATOM_INT(_size_18755) && IS_ATOM_INT(_n_18753)) {
            _10792 = _size_18755 - _n_18753;
            if ((long)((unsigned long)_10792 +(unsigned long) HIGH_BITS) >= 0){
                _10792 = NewDouble((double)_10792);
            }
        }
        else {
            if (IS_ATOM_INT(_size_18755)) {
                _10792 = NewDouble((double)_size_18755 - DBL_PTR(_n_18753)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_18753)) {
                    _10792 = NewDouble(DBL_PTR(_size_18755)->dbl - (double)_n_18753);
                }
                else
                _10792 = NewDouble(DBL_PTR(_size_18755)->dbl - DBL_PTR(_n_18753)->dbl);
            }
        }
        if (IS_ATOM_INT(_10792)) {
            _10793 = _10792 - 4;
            if ((long)((unsigned long)_10793 +(unsigned long) HIGH_BITS) >= 0){
                _10793 = NewDouble((double)_10793);
            }
        }
        else {
            _10793 = NewDouble(DBL_PTR(_10792)->dbl - (double)4);
        }
        DeRef(_10792);
        _10792 = NOVALUE;
        DeRef(_x_inlined_put4_at_190_18798);
        _x_inlined_put4_at_190_18798 = _10793;
        _10793 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_51mem0_18142)){
            poke4_addr = (unsigned long *)_51mem0_18142;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_190_18798)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_190_18798;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_190_18798)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at193_18799);
        _1 = (int)SEQ_PTR(_51memseq_18377);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at193_18799 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_51current_db_18100, _put4_1__tmp_at193_18799); // DJP 

        /** end procedure*/
        goto L7; // [215] 218
L7: 
        DeRef(_x_inlined_put4_at_190_18798);
        _x_inlined_put4_at_190_18798 = NOVALUE;
        DeRefi(_put4_1__tmp_at193_18799);
        _put4_1__tmp_at193_18799 = NOVALUE;

        /** 					addr += size-n-4*/
        if (IS_ATOM_INT(_size_18755) && IS_ATOM_INT(_n_18753)) {
            _10794 = _size_18755 - _n_18753;
            if ((long)((unsigned long)_10794 +(unsigned long) HIGH_BITS) >= 0){
                _10794 = NewDouble((double)_10794);
            }
        }
        else {
            if (IS_ATOM_INT(_size_18755)) {
                _10794 = NewDouble((double)_size_18755 - DBL_PTR(_n_18753)->dbl);
            }
            else {
                if (IS_ATOM_INT(_n_18753)) {
                    _10794 = NewDouble(DBL_PTR(_size_18755)->dbl - (double)_n_18753);
                }
                else
                _10794 = NewDouble(DBL_PTR(_size_18755)->dbl - DBL_PTR(_n_18753)->dbl);
            }
        }
        if (IS_ATOM_INT(_10794)) {
            _10795 = _10794 - 4;
            if ((long)((unsigned long)_10795 +(unsigned long) HIGH_BITS) >= 0){
                _10795 = NewDouble((double)_10795);
            }
        }
        else {
            _10795 = NewDouble(DBL_PTR(_10794)->dbl - (double)4);
        }
        DeRef(_10794);
        _10794 = NOVALUE;
        _0 = _addr_18757;
        if (IS_ATOM_INT(_addr_18757) && IS_ATOM_INT(_10795)) {
            _addr_18757 = _addr_18757 + _10795;
            if ((long)((unsigned long)_addr_18757 + (unsigned long)HIGH_BITS) >= 0) 
            _addr_18757 = NewDouble((double)_addr_18757);
        }
        else {
            if (IS_ATOM_INT(_addr_18757)) {
                _addr_18757 = NewDouble((double)_addr_18757 + DBL_PTR(_10795)->dbl);
            }
            else {
                if (IS_ATOM_INT(_10795)) {
                    _addr_18757 = NewDouble(DBL_PTR(_addr_18757)->dbl + (double)_10795);
                }
                else
                _addr_18757 = NewDouble(DBL_PTR(_addr_18757)->dbl + DBL_PTR(_10795)->dbl);
            }
        }
        DeRef(_0);
        DeRef(_10795);
        _10795 = NOVALUE;

        /** 					io:seek(current_db, addr - 4) */
        if (IS_ATOM_INT(_addr_18757)) {
            _10797 = _addr_18757 - 4;
            if ((long)((unsigned long)_10797 +(unsigned long) HIGH_BITS) >= 0){
                _10797 = NewDouble((double)_10797);
            }
        }
        else {
            _10797 = NewDouble(DBL_PTR(_addr_18757)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_241_18805);
        _pos_inlined_seek_at_241_18805 = _10797;
        _10797 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_241_18805);
        DeRef(_seek_1__tmp_at244_18807);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _pos_inlined_seek_at_241_18805;
        _seek_1__tmp_at244_18807 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_244_18806 = machine(19, _seek_1__tmp_at244_18807);
        DeRef(_pos_inlined_seek_at_241_18805);
        _pos_inlined_seek_at_241_18805 = NOVALUE;
        DeRef(_seek_1__tmp_at244_18807);
        _seek_1__tmp_at244_18807 = NOVALUE;

        /** 					put4(n+4)*/
        if (IS_ATOM_INT(_n_18753)) {
            _10798 = _n_18753 + 4;
            if ((long)((unsigned long)_10798 + (unsigned long)HIGH_BITS) >= 0) 
            _10798 = NewDouble((double)_10798);
        }
        else {
            _10798 = NewDouble(DBL_PTR(_n_18753)->dbl + (double)4);
        }
        DeRef(_x_inlined_put4_at_263_18810);
        _x_inlined_put4_at_263_18810 = _10798;
        _10798 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_51mem0_18142)){
            poke4_addr = (unsigned long *)_51mem0_18142;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_263_18810)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_263_18810;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_263_18810)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at266_18811);
        _1 = (int)SEQ_PTR(_51memseq_18377);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at266_18811 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_51current_db_18100, _put4_1__tmp_at266_18811); // DJP 

        /** end procedure*/
        goto L8; // [288] 291
L8: 
        DeRef(_x_inlined_put4_at_263_18810);
        _x_inlined_put4_at_263_18810 = NOVALUE;
        DeRefi(_put4_1__tmp_at266_18811);
        _put4_1__tmp_at266_18811 = NOVALUE;
        goto L9; // [293] 466
L5: 

        /** 					remaining = io:get_bytes(current_db, (free_count-i) * 8)*/
        _10799 = _free_count_18758 - _i_18772;
        if ((long)((unsigned long)_10799 +(unsigned long) HIGH_BITS) >= 0){
            _10799 = NewDouble((double)_10799);
        }
        if (IS_ATOM_INT(_10799)) {
            if (_10799 == (short)_10799)
            _10800 = _10799 * 8;
            else
            _10800 = NewDouble(_10799 * (double)8);
        }
        else {
            _10800 = NewDouble(DBL_PTR(_10799)->dbl * (double)8);
        }
        DeRef(_10799);
        _10799 = NOVALUE;
        _0 = _remaining_18759;
        _remaining_18759 = _16get_bytes(_51current_db_18100, _10800);
        DeRef(_0);
        _10800 = NOVALUE;

        /** 					io:seek(current_db, free_list+8*(i-1))*/
        _10802 = _i_18772 - 1;
        if (_10802 <= INT15)
        _10803 = 8 * _10802;
        else
        _10803 = NewDouble(8 * (double)_10802);
        _10802 = NOVALUE;
        if (IS_ATOM_INT(_free_list_18754) && IS_ATOM_INT(_10803)) {
            _10804 = _free_list_18754 + _10803;
            if ((long)((unsigned long)_10804 + (unsigned long)HIGH_BITS) >= 0) 
            _10804 = NewDouble((double)_10804);
        }
        else {
            if (IS_ATOM_INT(_free_list_18754)) {
                _10804 = NewDouble((double)_free_list_18754 + DBL_PTR(_10803)->dbl);
            }
            else {
                if (IS_ATOM_INT(_10803)) {
                    _10804 = NewDouble(DBL_PTR(_free_list_18754)->dbl + (double)_10803);
                }
                else
                _10804 = NewDouble(DBL_PTR(_free_list_18754)->dbl + DBL_PTR(_10803)->dbl);
            }
        }
        DeRef(_10803);
        _10803 = NOVALUE;
        DeRef(_pos_inlined_seek_at_330_18820);
        _pos_inlined_seek_at_330_18820 = _10804;
        _10804 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_330_18820);
        DeRef(_seek_1__tmp_at333_18822);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _pos_inlined_seek_at_330_18820;
        _seek_1__tmp_at333_18822 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_333_18821 = machine(19, _seek_1__tmp_at333_18822);
        DeRef(_pos_inlined_seek_at_330_18820);
        _pos_inlined_seek_at_330_18820 = NOVALUE;
        DeRef(_seek_1__tmp_at333_18822);
        _seek_1__tmp_at333_18822 = NOVALUE;

        /** 					putn(remaining)*/

        /** 	puts(current_db, s)*/
        EPuts(_51current_db_18100, _remaining_18759); // DJP 

        /** end procedure*/
        goto LA; // [358] 361
LA: 

        /** 					io:seek(current_db, FREE_COUNT)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        DeRefi(_seek_1__tmp_at364_18826);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = 7;
        _seek_1__tmp_at364_18826 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_364_18825 = machine(19, _seek_1__tmp_at364_18826);
        DeRefi(_seek_1__tmp_at364_18826);
        _seek_1__tmp_at364_18826 = NOVALUE;

        /** 					put4(free_count-1)*/
        _10805 = _free_count_18758 - 1;
        if ((long)((unsigned long)_10805 +(unsigned long) HIGH_BITS) >= 0){
            _10805 = NewDouble((double)_10805);
        }
        DeRef(_x_inlined_put4_at_383_18829);
        _x_inlined_put4_at_383_18829 = _10805;
        _10805 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_51mem0_18142)){
            poke4_addr = (unsigned long *)_51mem0_18142;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_383_18829)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_383_18829;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_383_18829)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at386_18830);
        _1 = (int)SEQ_PTR(_51memseq_18377);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at386_18830 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_51current_db_18100, _put4_1__tmp_at386_18830); // DJP 

        /** end procedure*/
        goto LB; // [408] 411
LB: 
        DeRef(_x_inlined_put4_at_383_18829);
        _x_inlined_put4_at_383_18829 = NOVALUE;
        DeRefi(_put4_1__tmp_at386_18830);
        _put4_1__tmp_at386_18830 = NOVALUE;

        /** 					io:seek(current_db, addr - 4)*/
        if (IS_ATOM_INT(_addr_18757)) {
            _10806 = _addr_18757 - 4;
            if ((long)((unsigned long)_10806 +(unsigned long) HIGH_BITS) >= 0){
                _10806 = NewDouble((double)_10806);
            }
        }
        else {
            _10806 = NewDouble(DBL_PTR(_addr_18757)->dbl - (double)4);
        }
        DeRef(_pos_inlined_seek_at_420_18833);
        _pos_inlined_seek_at_420_18833 = _10806;
        _10806 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_420_18833);
        DeRef(_seek_1__tmp_at423_18835);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _pos_inlined_seek_at_420_18833;
        _seek_1__tmp_at423_18835 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_423_18834 = machine(19, _seek_1__tmp_at423_18835);
        DeRef(_pos_inlined_seek_at_420_18833);
        _pos_inlined_seek_at_420_18833 = NOVALUE;
        DeRef(_seek_1__tmp_at423_18835);
        _seek_1__tmp_at423_18835 = NOVALUE;

        /** 					put4(size) -- in case size was not updated by db_free()*/

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_51mem0_18142)){
            poke4_addr = (unsigned long *)_51mem0_18142;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
        }
        if (IS_ATOM_INT(_size_18755)) {
            *poke4_addr = (unsigned long)_size_18755;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_size_18755)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at438_18837);
        _1 = (int)SEQ_PTR(_51memseq_18377);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at438_18837 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_51current_db_18100, _put4_1__tmp_at438_18837); // DJP 

        /** end procedure*/
        goto LC; // [460] 463
LC: 
        DeRefi(_put4_1__tmp_at438_18837);
        _put4_1__tmp_at438_18837 = NOVALUE;
L9: 

        /** 				return addr*/
        DeRef(_n_18753);
        DeRef(_free_list_18754);
        DeRef(_size_18755);
        DeRef(_size_ptr_18756);
        DeRef(_remaining_18759);
        return _addr_18757;
L4: 

        /** 			size_ptr += 8*/
        _0 = _size_ptr_18756;
        if (IS_ATOM_INT(_size_ptr_18756)) {
            _size_ptr_18756 = _size_ptr_18756 + 8;
            if ((long)((unsigned long)_size_ptr_18756 + (unsigned long)HIGH_BITS) >= 0) 
            _size_ptr_18756 = NewDouble((double)_size_ptr_18756);
        }
        else {
            _size_ptr_18756 = NewDouble(DBL_PTR(_size_ptr_18756)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _i_18772 = _i_18772 + 1;
        goto L2; // [481] 71
L3: 
        ;
    }
L1: 

    /** 	io:seek(current_db, -1)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at490_18841);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = -1;
    _seek_1__tmp_at490_18841 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_490_18840 = machine(19, _seek_1__tmp_at490_18841);
    DeRefi(_seek_1__tmp_at490_18841);
    _seek_1__tmp_at490_18841 = NOVALUE;

    /** 	put4(n+4)*/
    if (IS_ATOM_INT(_n_18753)) {
        _10808 = _n_18753 + 4;
        if ((long)((unsigned long)_10808 + (unsigned long)HIGH_BITS) >= 0) 
        _10808 = NewDouble((double)_10808);
    }
    else {
        _10808 = NewDouble(DBL_PTR(_n_18753)->dbl + (double)4);
    }
    DeRef(_x_inlined_put4_at_509_18844);
    _x_inlined_put4_at_509_18844 = _10808;
    _10808 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_509_18844)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_509_18844;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_509_18844)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at512_18845);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at512_18845 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at512_18845); // DJP 

    /** end procedure*/
    goto LD; // [534] 537
LD: 
    DeRef(_x_inlined_put4_at_509_18844);
    _x_inlined_put4_at_509_18844 = NOVALUE;
    DeRefi(_put4_1__tmp_at512_18845);
    _put4_1__tmp_at512_18845 = NOVALUE;

    /** 	return io:where(current_db)*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_542_18847);
    _where_inlined_where_at_542_18847 = machine(20, _51current_db_18100);
    DeRef(_n_18753);
    DeRef(_free_list_18754);
    DeRef(_size_18755);
    DeRef(_size_ptr_18756);
    DeRef(_addr_18757);
    DeRef(_remaining_18759);
    return _where_inlined_where_at_542_18847;
    ;
}


void _51db_free(int _p_18850)
{
    int _psize_18851 = NOVALUE;
    int _i_18852 = NOVALUE;
    int _size_18853 = NOVALUE;
    int _addr_18854 = NOVALUE;
    int _free_list_18855 = NOVALUE;
    int _free_list_space_18856 = NOVALUE;
    int _new_space_18857 = NOVALUE;
    int _to_be_freed_18858 = NOVALUE;
    int _prev_addr_18859 = NOVALUE;
    int _prev_size_18860 = NOVALUE;
    int _free_count_18861 = NOVALUE;
    int _remaining_18862 = NOVALUE;
    int _seek_1__tmp_at11_18867 = NOVALUE;
    int _seek_inlined_seek_at_11_18866 = NOVALUE;
    int _pos_inlined_seek_at_8_18865 = NOVALUE;
    int _seek_1__tmp_at33_18871 = NOVALUE;
    int _seek_inlined_seek_at_33_18870 = NOVALUE;
    int _seek_1__tmp_at69_18878 = NOVALUE;
    int _seek_inlined_seek_at_69_18877 = NOVALUE;
    int _pos_inlined_seek_at_66_18876 = NOVALUE;
    int _seek_1__tmp_at133_18891 = NOVALUE;
    int _seek_inlined_seek_at_133_18890 = NOVALUE;
    int _seek_1__tmp_at157_18895 = NOVALUE;
    int _seek_inlined_seek_at_157_18894 = NOVALUE;
    int _put4_1__tmp_at172_18897 = NOVALUE;
    int _seek_1__tmp_at202_18900 = NOVALUE;
    int _seek_inlined_seek_at_202_18899 = NOVALUE;
    int _seek_1__tmp_at234_18905 = NOVALUE;
    int _seek_inlined_seek_at_234_18904 = NOVALUE;
    int _s_inlined_putn_at_274_18911 = NOVALUE;
    int _seek_1__tmp_at297_18914 = NOVALUE;
    int _seek_inlined_seek_at_297_18913 = NOVALUE;
    int _seek_1__tmp_at430_18935 = NOVALUE;
    int _seek_inlined_seek_at_430_18934 = NOVALUE;
    int _pos_inlined_seek_at_427_18933 = NOVALUE;
    int _put4_1__tmp_at482_18945 = NOVALUE;
    int _x_inlined_put4_at_479_18944 = NOVALUE;
    int _seek_1__tmp_at523_18951 = NOVALUE;
    int _seek_inlined_seek_at_523_18950 = NOVALUE;
    int _pos_inlined_seek_at_520_18949 = NOVALUE;
    int _seek_1__tmp_at574_18961 = NOVALUE;
    int _seek_inlined_seek_at_574_18960 = NOVALUE;
    int _pos_inlined_seek_at_571_18959 = NOVALUE;
    int _seek_1__tmp_at611_18966 = NOVALUE;
    int _seek_inlined_seek_at_611_18965 = NOVALUE;
    int _put4_1__tmp_at626_18968 = NOVALUE;
    int _put4_1__tmp_at664_18973 = NOVALUE;
    int _x_inlined_put4_at_661_18972 = NOVALUE;
    int _seek_1__tmp_at737_18985 = NOVALUE;
    int _seek_inlined_seek_at_737_18984 = NOVALUE;
    int _pos_inlined_seek_at_734_18983 = NOVALUE;
    int _put4_1__tmp_at752_18987 = NOVALUE;
    int _put4_1__tmp_at789_18991 = NOVALUE;
    int _x_inlined_put4_at_786_18990 = NOVALUE;
    int _seek_1__tmp_at837_18999 = NOVALUE;
    int _seek_inlined_seek_at_837_18998 = NOVALUE;
    int _pos_inlined_seek_at_834_18997 = NOVALUE;
    int _seek_1__tmp_at883_19007 = NOVALUE;
    int _seek_inlined_seek_at_883_19006 = NOVALUE;
    int _put4_1__tmp_at898_19009 = NOVALUE;
    int _seek_1__tmp_at943_19016 = NOVALUE;
    int _seek_inlined_seek_at_943_19015 = NOVALUE;
    int _pos_inlined_seek_at_940_19014 = NOVALUE;
    int _put4_1__tmp_at958_19018 = NOVALUE;
    int _put4_1__tmp_at986_19020 = NOVALUE;
    int _10877 = NOVALUE;
    int _10876 = NOVALUE;
    int _10875 = NOVALUE;
    int _10874 = NOVALUE;
    int _10871 = NOVALUE;
    int _10870 = NOVALUE;
    int _10869 = NOVALUE;
    int _10868 = NOVALUE;
    int _10867 = NOVALUE;
    int _10866 = NOVALUE;
    int _10865 = NOVALUE;
    int _10864 = NOVALUE;
    int _10863 = NOVALUE;
    int _10862 = NOVALUE;
    int _10861 = NOVALUE;
    int _10860 = NOVALUE;
    int _10859 = NOVALUE;
    int _10858 = NOVALUE;
    int _10857 = NOVALUE;
    int _10855 = NOVALUE;
    int _10854 = NOVALUE;
    int _10853 = NOVALUE;
    int _10851 = NOVALUE;
    int _10850 = NOVALUE;
    int _10849 = NOVALUE;
    int _10848 = NOVALUE;
    int _10847 = NOVALUE;
    int _10846 = NOVALUE;
    int _10845 = NOVALUE;
    int _10844 = NOVALUE;
    int _10843 = NOVALUE;
    int _10842 = NOVALUE;
    int _10841 = NOVALUE;
    int _10840 = NOVALUE;
    int _10839 = NOVALUE;
    int _10838 = NOVALUE;
    int _10837 = NOVALUE;
    int _10836 = NOVALUE;
    int _10835 = NOVALUE;
    int _10834 = NOVALUE;
    int _10828 = NOVALUE;
    int _10827 = NOVALUE;
    int _10826 = NOVALUE;
    int _10824 = NOVALUE;
    int _10820 = NOVALUE;
    int _10819 = NOVALUE;
    int _10817 = NOVALUE;
    int _10816 = NOVALUE;
    int _10814 = NOVALUE;
    int _10813 = NOVALUE;
    int _10809 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, p-4)*/
    if (IS_ATOM_INT(_p_18850)) {
        _10809 = _p_18850 - 4;
        if ((long)((unsigned long)_10809 +(unsigned long) HIGH_BITS) >= 0){
            _10809 = NewDouble((double)_10809);
        }
    }
    else {
        _10809 = NewDouble(DBL_PTR(_p_18850)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_18865);
    _pos_inlined_seek_at_8_18865 = _10809;
    _10809 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_18865);
    DeRef(_seek_1__tmp_at11_18867);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_18865;
    _seek_1__tmp_at11_18867 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_18866 = machine(19, _seek_1__tmp_at11_18867);
    DeRef(_pos_inlined_seek_at_8_18865);
    _pos_inlined_seek_at_8_18865 = NOVALUE;
    DeRef(_seek_1__tmp_at11_18867);
    _seek_1__tmp_at11_18867 = NOVALUE;

    /** 	psize = get4()*/
    _0 = _psize_18851;
    _psize_18851 = _51get4();
    DeRef(_0);

    /** 	io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at33_18871);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at33_18871 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_33_18870 = machine(19, _seek_1__tmp_at33_18871);
    DeRefi(_seek_1__tmp_at33_18871);
    _seek_1__tmp_at33_18871 = NOVALUE;

    /** 	free_count = get4()*/
    _free_count_18861 = _51get4();
    if (!IS_ATOM_INT(_free_count_18861)) {
        _1 = (long)(DBL_PTR(_free_count_18861)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_18861)) && (DBL_PTR(_free_count_18861)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_18861);
        _free_count_18861 = _1;
    }

    /** 	free_list = get4()*/
    _0 = _free_list_18855;
    _free_list_18855 = _51get4();
    DeRef(_0);

    /** 	io:seek(current_db, free_list - 4)*/
    if (IS_ATOM_INT(_free_list_18855)) {
        _10813 = _free_list_18855 - 4;
        if ((long)((unsigned long)_10813 +(unsigned long) HIGH_BITS) >= 0){
            _10813 = NewDouble((double)_10813);
        }
    }
    else {
        _10813 = NewDouble(DBL_PTR(_free_list_18855)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_66_18876);
    _pos_inlined_seek_at_66_18876 = _10813;
    _10813 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_66_18876);
    DeRef(_seek_1__tmp_at69_18878);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_66_18876;
    _seek_1__tmp_at69_18878 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_69_18877 = machine(19, _seek_1__tmp_at69_18878);
    DeRef(_pos_inlined_seek_at_66_18876);
    _pos_inlined_seek_at_66_18876 = NOVALUE;
    DeRef(_seek_1__tmp_at69_18878);
    _seek_1__tmp_at69_18878 = NOVALUE;

    /** 	free_list_space = get4()-4*/
    _10814 = _51get4();
    DeRef(_free_list_space_18856);
    if (IS_ATOM_INT(_10814)) {
        _free_list_space_18856 = _10814 - 4;
        if ((long)((unsigned long)_free_list_space_18856 +(unsigned long) HIGH_BITS) >= 0){
            _free_list_space_18856 = NewDouble((double)_free_list_space_18856);
        }
    }
    else {
        _free_list_space_18856 = binary_op(MINUS, _10814, 4);
    }
    DeRef(_10814);
    _10814 = NOVALUE;

    /** 	if free_list_space < 8 * (free_count+1) then*/
    _10816 = _free_count_18861 + 1;
    if (_10816 > MAXINT){
        _10816 = NewDouble((double)_10816);
    }
    if (IS_ATOM_INT(_10816)) {
        if (_10816 <= INT15 && _10816 >= -INT15)
        _10817 = 8 * _10816;
        else
        _10817 = NewDouble(8 * (double)_10816);
    }
    else {
        _10817 = NewDouble((double)8 * DBL_PTR(_10816)->dbl);
    }
    DeRef(_10816);
    _10816 = NOVALUE;
    if (binary_op_a(GREATEREQ, _free_list_space_18856, _10817)){
        DeRef(_10817);
        _10817 = NOVALUE;
        goto L1; // [102] 314
    }
    DeRef(_10817);
    _10817 = NOVALUE;

    /** 		new_space = floor(free_list_space + free_list_space / 2)*/
    if (IS_ATOM_INT(_free_list_space_18856)) {
        if (_free_list_space_18856 & 1) {
            _10819 = NewDouble((_free_list_space_18856 >> 1) + 0.5);
        }
        else
        _10819 = _free_list_space_18856 >> 1;
    }
    else {
        _10819 = binary_op(DIVIDE, _free_list_space_18856, 2);
    }
    if (IS_ATOM_INT(_free_list_space_18856) && IS_ATOM_INT(_10819)) {
        _10820 = _free_list_space_18856 + _10819;
        if ((long)((unsigned long)_10820 + (unsigned long)HIGH_BITS) >= 0) 
        _10820 = NewDouble((double)_10820);
    }
    else {
        if (IS_ATOM_INT(_free_list_space_18856)) {
            _10820 = NewDouble((double)_free_list_space_18856 + DBL_PTR(_10819)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10819)) {
                _10820 = NewDouble(DBL_PTR(_free_list_space_18856)->dbl + (double)_10819);
            }
            else
            _10820 = NewDouble(DBL_PTR(_free_list_space_18856)->dbl + DBL_PTR(_10819)->dbl);
        }
    }
    DeRef(_10819);
    _10819 = NOVALUE;
    DeRef(_new_space_18857);
    if (IS_ATOM_INT(_10820))
    _new_space_18857 = e_floor(_10820);
    else
    _new_space_18857 = unary_op(FLOOR, _10820);
    DeRef(_10820);
    _10820 = NOVALUE;

    /** 		to_be_freed = free_list*/
    Ref(_free_list_18855);
    DeRef(_to_be_freed_18858);
    _to_be_freed_18858 = _free_list_18855;

    /** 		free_list = db_allocate(new_space)*/
    Ref(_new_space_18857);
    _0 = _free_list_18855;
    _free_list_18855 = _51db_allocate(_new_space_18857);
    DeRef(_0);

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at133_18891);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at133_18891 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_133_18890 = machine(19, _seek_1__tmp_at133_18891);
    DeRefi(_seek_1__tmp_at133_18891);
    _seek_1__tmp_at133_18891 = NOVALUE;

    /** 		free_count = get4() -- db_allocate may have changed it*/
    _free_count_18861 = _51get4();
    if (!IS_ATOM_INT(_free_count_18861)) {
        _1 = (long)(DBL_PTR(_free_count_18861)->dbl);
        if (UNIQUE(DBL_PTR(_free_count_18861)) && (DBL_PTR(_free_count_18861)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_free_count_18861);
        _free_count_18861 = _1;
    }

    /** 		io:seek(current_db, FREE_LIST)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at157_18895);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 11;
    _seek_1__tmp_at157_18895 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_157_18894 = machine(19, _seek_1__tmp_at157_18895);
    DeRefi(_seek_1__tmp_at157_18895);
    _seek_1__tmp_at157_18895 = NOVALUE;

    /** 		put4(free_list)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_free_list_18855)) {
        *poke4_addr = (unsigned long)_free_list_18855;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_free_list_18855)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at172_18897);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at172_18897 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at172_18897); // DJP 

    /** end procedure*/
    goto L2; // [194] 197
L2: 
    DeRefi(_put4_1__tmp_at172_18897);
    _put4_1__tmp_at172_18897 = NOVALUE;

    /** 		io:seek(current_db, to_be_freed)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_to_be_freed_18858);
    DeRef(_seek_1__tmp_at202_18900);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _to_be_freed_18858;
    _seek_1__tmp_at202_18900 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_202_18899 = machine(19, _seek_1__tmp_at202_18900);
    DeRef(_seek_1__tmp_at202_18900);
    _seek_1__tmp_at202_18900 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, 8*free_count)*/
    if (_free_count_18861 <= INT15 && _free_count_18861 >= -INT15)
    _10824 = 8 * _free_count_18861;
    else
    _10824 = NewDouble(8 * (double)_free_count_18861);
    _0 = _remaining_18862;
    _remaining_18862 = _16get_bytes(_51current_db_18100, _10824);
    DeRef(_0);
    _10824 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_18855);
    DeRef(_seek_1__tmp_at234_18905);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _free_list_18855;
    _seek_1__tmp_at234_18905 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_234_18904 = machine(19, _seek_1__tmp_at234_18905);
    DeRef(_seek_1__tmp_at234_18905);
    _seek_1__tmp_at234_18905 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _remaining_18862); // DJP 

    /** end procedure*/
    goto L3; // [259] 262
L3: 

    /** 		putn(repeat(0, new_space-length(remaining)))*/
    if (IS_SEQUENCE(_remaining_18862)){
            _10826 = SEQ_PTR(_remaining_18862)->length;
    }
    else {
        _10826 = 1;
    }
    if (IS_ATOM_INT(_new_space_18857)) {
        _10827 = _new_space_18857 - _10826;
    }
    else {
        _10827 = NewDouble(DBL_PTR(_new_space_18857)->dbl - (double)_10826);
    }
    _10826 = NOVALUE;
    _10828 = Repeat(0, _10827);
    DeRef(_10827);
    _10827 = NOVALUE;
    DeRefi(_s_inlined_putn_at_274_18911);
    _s_inlined_putn_at_274_18911 = _10828;
    _10828 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_274_18911); // DJP 

    /** end procedure*/
    goto L4; // [289] 292
L4: 
    DeRefi(_s_inlined_putn_at_274_18911);
    _s_inlined_putn_at_274_18911 = NOVALUE;

    /** 		io:seek(current_db, free_list)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_free_list_18855);
    DeRef(_seek_1__tmp_at297_18914);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _free_list_18855;
    _seek_1__tmp_at297_18914 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_297_18913 = machine(19, _seek_1__tmp_at297_18914);
    DeRef(_seek_1__tmp_at297_18914);
    _seek_1__tmp_at297_18914 = NOVALUE;
    goto L5; // [311] 320
L1: 

    /** 		new_space = 0*/
    DeRef(_new_space_18857);
    _new_space_18857 = 0;
L5: 

    /** 	i = 1*/
    DeRef(_i_18852);
    _i_18852 = 1;

    /** 	prev_addr = 0*/
    DeRef(_prev_addr_18859);
    _prev_addr_18859 = 0;

    /** 	prev_size = 0*/
    DeRef(_prev_size_18860);
    _prev_size_18860 = 0;

    /** 	while i <= free_count do*/
L6: 
    if (binary_op_a(GREATER, _i_18852, _free_count_18861)){
        goto L7; // [340] 386
    }

    /** 		addr = get4()*/
    _0 = _addr_18854;
    _addr_18854 = _51get4();
    DeRef(_0);

    /** 		size = get4()*/
    _0 = _size_18853;
    _size_18853 = _51get4();
    DeRef(_0);

    /** 		if p < addr then*/
    if (binary_op_a(GREATEREQ, _p_18850, _addr_18854)){
        goto L8; // [356] 365
    }

    /** 			exit*/
    goto L7; // [362] 386
L8: 

    /** 		prev_addr = addr*/
    Ref(_addr_18854);
    DeRef(_prev_addr_18859);
    _prev_addr_18859 = _addr_18854;

    /** 		prev_size = size*/
    Ref(_size_18853);
    DeRef(_prev_size_18860);
    _prev_size_18860 = _size_18853;

    /** 		i += 1*/
    _0 = _i_18852;
    if (IS_ATOM_INT(_i_18852)) {
        _i_18852 = _i_18852 + 1;
        if (_i_18852 > MAXINT){
            _i_18852 = NewDouble((double)_i_18852);
        }
    }
    else
    _i_18852 = binary_op(PLUS, 1, _i_18852);
    DeRef(_0);

    /** 	end while*/
    goto L6; // [383] 340
L7: 

    /** 	if i > 1 and prev_addr + prev_size = p then*/
    if (IS_ATOM_INT(_i_18852)) {
        _10834 = (_i_18852 > 1);
    }
    else {
        _10834 = (DBL_PTR(_i_18852)->dbl > (double)1);
    }
    if (_10834 == 0) {
        goto L9; // [392] 695
    }
    if (IS_ATOM_INT(_prev_addr_18859) && IS_ATOM_INT(_prev_size_18860)) {
        _10836 = _prev_addr_18859 + _prev_size_18860;
        if ((long)((unsigned long)_10836 + (unsigned long)HIGH_BITS) >= 0) 
        _10836 = NewDouble((double)_10836);
    }
    else {
        if (IS_ATOM_INT(_prev_addr_18859)) {
            _10836 = NewDouble((double)_prev_addr_18859 + DBL_PTR(_prev_size_18860)->dbl);
        }
        else {
            if (IS_ATOM_INT(_prev_size_18860)) {
                _10836 = NewDouble(DBL_PTR(_prev_addr_18859)->dbl + (double)_prev_size_18860);
            }
            else
            _10836 = NewDouble(DBL_PTR(_prev_addr_18859)->dbl + DBL_PTR(_prev_size_18860)->dbl);
        }
    }
    if (IS_ATOM_INT(_10836) && IS_ATOM_INT(_p_18850)) {
        _10837 = (_10836 == _p_18850);
    }
    else {
        if (IS_ATOM_INT(_10836)) {
            _10837 = ((double)_10836 == DBL_PTR(_p_18850)->dbl);
        }
        else {
            if (IS_ATOM_INT(_p_18850)) {
                _10837 = (DBL_PTR(_10836)->dbl == (double)_p_18850);
            }
            else
            _10837 = (DBL_PTR(_10836)->dbl == DBL_PTR(_p_18850)->dbl);
        }
    }
    DeRef(_10836);
    _10836 = NOVALUE;
    if (_10837 == 0)
    {
        DeRef(_10837);
        _10837 = NOVALUE;
        goto L9; // [405] 695
    }
    else{
        DeRef(_10837);
        _10837 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-2)*8+4)*/
    if (IS_ATOM_INT(_i_18852)) {
        _10838 = _i_18852 - 2;
        if ((long)((unsigned long)_10838 +(unsigned long) HIGH_BITS) >= 0){
            _10838 = NewDouble((double)_10838);
        }
    }
    else {
        _10838 = NewDouble(DBL_PTR(_i_18852)->dbl - (double)2);
    }
    if (IS_ATOM_INT(_10838)) {
        if (_10838 == (short)_10838)
        _10839 = _10838 * 8;
        else
        _10839 = NewDouble(_10838 * (double)8);
    }
    else {
        _10839 = NewDouble(DBL_PTR(_10838)->dbl * (double)8);
    }
    DeRef(_10838);
    _10838 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18855) && IS_ATOM_INT(_10839)) {
        _10840 = _free_list_18855 + _10839;
        if ((long)((unsigned long)_10840 + (unsigned long)HIGH_BITS) >= 0) 
        _10840 = NewDouble((double)_10840);
    }
    else {
        if (IS_ATOM_INT(_free_list_18855)) {
            _10840 = NewDouble((double)_free_list_18855 + DBL_PTR(_10839)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10839)) {
                _10840 = NewDouble(DBL_PTR(_free_list_18855)->dbl + (double)_10839);
            }
            else
            _10840 = NewDouble(DBL_PTR(_free_list_18855)->dbl + DBL_PTR(_10839)->dbl);
        }
    }
    DeRef(_10839);
    _10839 = NOVALUE;
    if (IS_ATOM_INT(_10840)) {
        _10841 = _10840 + 4;
        if ((long)((unsigned long)_10841 + (unsigned long)HIGH_BITS) >= 0) 
        _10841 = NewDouble((double)_10841);
    }
    else {
        _10841 = NewDouble(DBL_PTR(_10840)->dbl + (double)4);
    }
    DeRef(_10840);
    _10840 = NOVALUE;
    DeRef(_pos_inlined_seek_at_427_18933);
    _pos_inlined_seek_at_427_18933 = _10841;
    _10841 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_427_18933);
    DeRef(_seek_1__tmp_at430_18935);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_427_18933;
    _seek_1__tmp_at430_18935 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_430_18934 = machine(19, _seek_1__tmp_at430_18935);
    DeRef(_pos_inlined_seek_at_427_18933);
    _pos_inlined_seek_at_427_18933 = NOVALUE;
    DeRef(_seek_1__tmp_at430_18935);
    _seek_1__tmp_at430_18935 = NOVALUE;

    /** 		if i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_18852)) {
        _10842 = (_i_18852 < _free_count_18861);
    }
    else {
        _10842 = (DBL_PTR(_i_18852)->dbl < (double)_free_count_18861);
    }
    if (_10842 == 0) {
        goto LA; // [450] 656
    }
    if (IS_ATOM_INT(_p_18850) && IS_ATOM_INT(_psize_18851)) {
        _10844 = _p_18850 + _psize_18851;
        if ((long)((unsigned long)_10844 + (unsigned long)HIGH_BITS) >= 0) 
        _10844 = NewDouble((double)_10844);
    }
    else {
        if (IS_ATOM_INT(_p_18850)) {
            _10844 = NewDouble((double)_p_18850 + DBL_PTR(_psize_18851)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18851)) {
                _10844 = NewDouble(DBL_PTR(_p_18850)->dbl + (double)_psize_18851);
            }
            else
            _10844 = NewDouble(DBL_PTR(_p_18850)->dbl + DBL_PTR(_psize_18851)->dbl);
        }
    }
    if (IS_ATOM_INT(_10844) && IS_ATOM_INT(_addr_18854)) {
        _10845 = (_10844 == _addr_18854);
    }
    else {
        if (IS_ATOM_INT(_10844)) {
            _10845 = ((double)_10844 == DBL_PTR(_addr_18854)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_18854)) {
                _10845 = (DBL_PTR(_10844)->dbl == (double)_addr_18854);
            }
            else
            _10845 = (DBL_PTR(_10844)->dbl == DBL_PTR(_addr_18854)->dbl);
        }
    }
    DeRef(_10844);
    _10844 = NOVALUE;
    if (_10845 == 0)
    {
        DeRef(_10845);
        _10845 = NOVALUE;
        goto LA; // [465] 656
    }
    else{
        DeRef(_10845);
        _10845 = NOVALUE;
    }

    /** 			put4(prev_size+psize+size) -- update size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_18860) && IS_ATOM_INT(_psize_18851)) {
        _10846 = _prev_size_18860 + _psize_18851;
        if ((long)((unsigned long)_10846 + (unsigned long)HIGH_BITS) >= 0) 
        _10846 = NewDouble((double)_10846);
    }
    else {
        if (IS_ATOM_INT(_prev_size_18860)) {
            _10846 = NewDouble((double)_prev_size_18860 + DBL_PTR(_psize_18851)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18851)) {
                _10846 = NewDouble(DBL_PTR(_prev_size_18860)->dbl + (double)_psize_18851);
            }
            else
            _10846 = NewDouble(DBL_PTR(_prev_size_18860)->dbl + DBL_PTR(_psize_18851)->dbl);
        }
    }
    if (IS_ATOM_INT(_10846) && IS_ATOM_INT(_size_18853)) {
        _10847 = _10846 + _size_18853;
        if ((long)((unsigned long)_10847 + (unsigned long)HIGH_BITS) >= 0) 
        _10847 = NewDouble((double)_10847);
    }
    else {
        if (IS_ATOM_INT(_10846)) {
            _10847 = NewDouble((double)_10846 + DBL_PTR(_size_18853)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_18853)) {
                _10847 = NewDouble(DBL_PTR(_10846)->dbl + (double)_size_18853);
            }
            else
            _10847 = NewDouble(DBL_PTR(_10846)->dbl + DBL_PTR(_size_18853)->dbl);
        }
    }
    DeRef(_10846);
    _10846 = NOVALUE;
    DeRef(_x_inlined_put4_at_479_18944);
    _x_inlined_put4_at_479_18944 = _10847;
    _10847 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_479_18944)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_479_18944;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_479_18944)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at482_18945);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at482_18945 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at482_18945); // DJP 

    /** end procedure*/
    goto LB; // [504] 507
LB: 
    DeRef(_x_inlined_put4_at_479_18944);
    _x_inlined_put4_at_479_18944 = NOVALUE;
    DeRefi(_put4_1__tmp_at482_18945);
    _put4_1__tmp_at482_18945 = NOVALUE;

    /** 			io:seek(current_db, free_list+i*8)*/
    if (IS_ATOM_INT(_i_18852)) {
        if (_i_18852 == (short)_i_18852)
        _10848 = _i_18852 * 8;
        else
        _10848 = NewDouble(_i_18852 * (double)8);
    }
    else {
        _10848 = NewDouble(DBL_PTR(_i_18852)->dbl * (double)8);
    }
    if (IS_ATOM_INT(_free_list_18855) && IS_ATOM_INT(_10848)) {
        _10849 = _free_list_18855 + _10848;
        if ((long)((unsigned long)_10849 + (unsigned long)HIGH_BITS) >= 0) 
        _10849 = NewDouble((double)_10849);
    }
    else {
        if (IS_ATOM_INT(_free_list_18855)) {
            _10849 = NewDouble((double)_free_list_18855 + DBL_PTR(_10848)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10848)) {
                _10849 = NewDouble(DBL_PTR(_free_list_18855)->dbl + (double)_10848);
            }
            else
            _10849 = NewDouble(DBL_PTR(_free_list_18855)->dbl + DBL_PTR(_10848)->dbl);
        }
    }
    DeRef(_10848);
    _10848 = NOVALUE;
    DeRef(_pos_inlined_seek_at_520_18949);
    _pos_inlined_seek_at_520_18949 = _10849;
    _10849 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_520_18949);
    DeRef(_seek_1__tmp_at523_18951);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_520_18949;
    _seek_1__tmp_at523_18951 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_523_18950 = machine(19, _seek_1__tmp_at523_18951);
    DeRef(_pos_inlined_seek_at_520_18949);
    _pos_inlined_seek_at_520_18949 = NOVALUE;
    DeRef(_seek_1__tmp_at523_18951);
    _seek_1__tmp_at523_18951 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, (free_count-i)*8)*/
    if (IS_ATOM_INT(_i_18852)) {
        _10850 = _free_count_18861 - _i_18852;
        if ((long)((unsigned long)_10850 +(unsigned long) HIGH_BITS) >= 0){
            _10850 = NewDouble((double)_10850);
        }
    }
    else {
        _10850 = NewDouble((double)_free_count_18861 - DBL_PTR(_i_18852)->dbl);
    }
    if (IS_ATOM_INT(_10850)) {
        if (_10850 == (short)_10850)
        _10851 = _10850 * 8;
        else
        _10851 = NewDouble(_10850 * (double)8);
    }
    else {
        _10851 = NewDouble(DBL_PTR(_10850)->dbl * (double)8);
    }
    DeRef(_10850);
    _10850 = NOVALUE;
    _0 = _remaining_18862;
    _remaining_18862 = _16get_bytes(_51current_db_18100, _10851);
    DeRef(_0);
    _10851 = NOVALUE;

    /** 			io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18852)) {
        _10853 = _i_18852 - 1;
        if ((long)((unsigned long)_10853 +(unsigned long) HIGH_BITS) >= 0){
            _10853 = NewDouble((double)_10853);
        }
    }
    else {
        _10853 = NewDouble(DBL_PTR(_i_18852)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10853)) {
        if (_10853 == (short)_10853)
        _10854 = _10853 * 8;
        else
        _10854 = NewDouble(_10853 * (double)8);
    }
    else {
        _10854 = NewDouble(DBL_PTR(_10853)->dbl * (double)8);
    }
    DeRef(_10853);
    _10853 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18855) && IS_ATOM_INT(_10854)) {
        _10855 = _free_list_18855 + _10854;
        if ((long)((unsigned long)_10855 + (unsigned long)HIGH_BITS) >= 0) 
        _10855 = NewDouble((double)_10855);
    }
    else {
        if (IS_ATOM_INT(_free_list_18855)) {
            _10855 = NewDouble((double)_free_list_18855 + DBL_PTR(_10854)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10854)) {
                _10855 = NewDouble(DBL_PTR(_free_list_18855)->dbl + (double)_10854);
            }
            else
            _10855 = NewDouble(DBL_PTR(_free_list_18855)->dbl + DBL_PTR(_10854)->dbl);
        }
    }
    DeRef(_10854);
    _10854 = NOVALUE;
    DeRef(_pos_inlined_seek_at_571_18959);
    _pos_inlined_seek_at_571_18959 = _10855;
    _10855 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_571_18959);
    DeRef(_seek_1__tmp_at574_18961);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_571_18959;
    _seek_1__tmp_at574_18961 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_574_18960 = machine(19, _seek_1__tmp_at574_18961);
    DeRef(_pos_inlined_seek_at_571_18959);
    _pos_inlined_seek_at_571_18959 = NOVALUE;
    DeRef(_seek_1__tmp_at574_18961);
    _seek_1__tmp_at574_18961 = NOVALUE;

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _remaining_18862); // DJP 

    /** end procedure*/
    goto LC; // [599] 602
LC: 

    /** 			free_count -= 1*/
    _free_count_18861 = _free_count_18861 - 1;

    /** 			io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at611_18966);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at611_18966 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_611_18965 = machine(19, _seek_1__tmp_at611_18966);
    DeRefi(_seek_1__tmp_at611_18966);
    _seek_1__tmp_at611_18966 = NOVALUE;

    /** 			put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_18861;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at626_18968);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at626_18968 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at626_18968); // DJP 

    /** end procedure*/
    goto LD; // [648] 651
LD: 
    DeRefi(_put4_1__tmp_at626_18968);
    _put4_1__tmp_at626_18968 = NOVALUE;
    goto LE; // [653] 1028
LA: 

    /** 			put4(prev_size+psize) -- increase previous size on free list (only)*/
    if (IS_ATOM_INT(_prev_size_18860) && IS_ATOM_INT(_psize_18851)) {
        _10857 = _prev_size_18860 + _psize_18851;
        if ((long)((unsigned long)_10857 + (unsigned long)HIGH_BITS) >= 0) 
        _10857 = NewDouble((double)_10857);
    }
    else {
        if (IS_ATOM_INT(_prev_size_18860)) {
            _10857 = NewDouble((double)_prev_size_18860 + DBL_PTR(_psize_18851)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18851)) {
                _10857 = NewDouble(DBL_PTR(_prev_size_18860)->dbl + (double)_psize_18851);
            }
            else
            _10857 = NewDouble(DBL_PTR(_prev_size_18860)->dbl + DBL_PTR(_psize_18851)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_661_18972);
    _x_inlined_put4_at_661_18972 = _10857;
    _10857 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_661_18972)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_661_18972;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_661_18972)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at664_18973);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at664_18973 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at664_18973); // DJP 

    /** end procedure*/
    goto LF; // [686] 689
LF: 
    DeRef(_x_inlined_put4_at_661_18972);
    _x_inlined_put4_at_661_18972 = NOVALUE;
    DeRefi(_put4_1__tmp_at664_18973);
    _put4_1__tmp_at664_18973 = NOVALUE;
    goto LE; // [692] 1028
L9: 

    /** 	elsif i < free_count and p + psize = addr then*/
    if (IS_ATOM_INT(_i_18852)) {
        _10858 = (_i_18852 < _free_count_18861);
    }
    else {
        _10858 = (DBL_PTR(_i_18852)->dbl < (double)_free_count_18861);
    }
    if (_10858 == 0) {
        goto L10; // [701] 819
    }
    if (IS_ATOM_INT(_p_18850) && IS_ATOM_INT(_psize_18851)) {
        _10860 = _p_18850 + _psize_18851;
        if ((long)((unsigned long)_10860 + (unsigned long)HIGH_BITS) >= 0) 
        _10860 = NewDouble((double)_10860);
    }
    else {
        if (IS_ATOM_INT(_p_18850)) {
            _10860 = NewDouble((double)_p_18850 + DBL_PTR(_psize_18851)->dbl);
        }
        else {
            if (IS_ATOM_INT(_psize_18851)) {
                _10860 = NewDouble(DBL_PTR(_p_18850)->dbl + (double)_psize_18851);
            }
            else
            _10860 = NewDouble(DBL_PTR(_p_18850)->dbl + DBL_PTR(_psize_18851)->dbl);
        }
    }
    if (IS_ATOM_INT(_10860) && IS_ATOM_INT(_addr_18854)) {
        _10861 = (_10860 == _addr_18854);
    }
    else {
        if (IS_ATOM_INT(_10860)) {
            _10861 = ((double)_10860 == DBL_PTR(_addr_18854)->dbl);
        }
        else {
            if (IS_ATOM_INT(_addr_18854)) {
                _10861 = (DBL_PTR(_10860)->dbl == (double)_addr_18854);
            }
            else
            _10861 = (DBL_PTR(_10860)->dbl == DBL_PTR(_addr_18854)->dbl);
        }
    }
    DeRef(_10860);
    _10860 = NOVALUE;
    if (_10861 == 0)
    {
        DeRef(_10861);
        _10861 = NOVALUE;
        goto L10; // [716] 819
    }
    else{
        DeRef(_10861);
        _10861 = NOVALUE;
    }

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18852)) {
        _10862 = _i_18852 - 1;
        if ((long)((unsigned long)_10862 +(unsigned long) HIGH_BITS) >= 0){
            _10862 = NewDouble((double)_10862);
        }
    }
    else {
        _10862 = NewDouble(DBL_PTR(_i_18852)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10862)) {
        if (_10862 == (short)_10862)
        _10863 = _10862 * 8;
        else
        _10863 = NewDouble(_10862 * (double)8);
    }
    else {
        _10863 = NewDouble(DBL_PTR(_10862)->dbl * (double)8);
    }
    DeRef(_10862);
    _10862 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18855) && IS_ATOM_INT(_10863)) {
        _10864 = _free_list_18855 + _10863;
        if ((long)((unsigned long)_10864 + (unsigned long)HIGH_BITS) >= 0) 
        _10864 = NewDouble((double)_10864);
    }
    else {
        if (IS_ATOM_INT(_free_list_18855)) {
            _10864 = NewDouble((double)_free_list_18855 + DBL_PTR(_10863)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10863)) {
                _10864 = NewDouble(DBL_PTR(_free_list_18855)->dbl + (double)_10863);
            }
            else
            _10864 = NewDouble(DBL_PTR(_free_list_18855)->dbl + DBL_PTR(_10863)->dbl);
        }
    }
    DeRef(_10863);
    _10863 = NOVALUE;
    DeRef(_pos_inlined_seek_at_734_18983);
    _pos_inlined_seek_at_734_18983 = _10864;
    _10864 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_734_18983);
    DeRef(_seek_1__tmp_at737_18985);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_734_18983;
    _seek_1__tmp_at737_18985 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_737_18984 = machine(19, _seek_1__tmp_at737_18985);
    DeRef(_pos_inlined_seek_at_734_18983);
    _pos_inlined_seek_at_734_18983 = NOVALUE;
    DeRef(_seek_1__tmp_at737_18985);
    _seek_1__tmp_at737_18985 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_p_18850)) {
        *poke4_addr = (unsigned long)_p_18850;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_18850)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at752_18987);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at752_18987 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at752_18987); // DJP 

    /** end procedure*/
    goto L11; // [774] 777
L11: 
    DeRefi(_put4_1__tmp_at752_18987);
    _put4_1__tmp_at752_18987 = NOVALUE;

    /** 		put4(psize+size)*/
    if (IS_ATOM_INT(_psize_18851) && IS_ATOM_INT(_size_18853)) {
        _10865 = _psize_18851 + _size_18853;
        if ((long)((unsigned long)_10865 + (unsigned long)HIGH_BITS) >= 0) 
        _10865 = NewDouble((double)_10865);
    }
    else {
        if (IS_ATOM_INT(_psize_18851)) {
            _10865 = NewDouble((double)_psize_18851 + DBL_PTR(_size_18853)->dbl);
        }
        else {
            if (IS_ATOM_INT(_size_18853)) {
                _10865 = NewDouble(DBL_PTR(_psize_18851)->dbl + (double)_size_18853);
            }
            else
            _10865 = NewDouble(DBL_PTR(_psize_18851)->dbl + DBL_PTR(_size_18853)->dbl);
        }
    }
    DeRef(_x_inlined_put4_at_786_18990);
    _x_inlined_put4_at_786_18990 = _10865;
    _10865 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_786_18990)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_786_18990;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_786_18990)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at789_18991);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at789_18991 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at789_18991); // DJP 

    /** end procedure*/
    goto L12; // [811] 814
L12: 
    DeRef(_x_inlined_put4_at_786_18990);
    _x_inlined_put4_at_786_18990 = NOVALUE;
    DeRefi(_put4_1__tmp_at789_18991);
    _put4_1__tmp_at789_18991 = NOVALUE;
    goto LE; // [816] 1028
L10: 

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18852)) {
        _10866 = _i_18852 - 1;
        if ((long)((unsigned long)_10866 +(unsigned long) HIGH_BITS) >= 0){
            _10866 = NewDouble((double)_10866);
        }
    }
    else {
        _10866 = NewDouble(DBL_PTR(_i_18852)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10866)) {
        if (_10866 == (short)_10866)
        _10867 = _10866 * 8;
        else
        _10867 = NewDouble(_10866 * (double)8);
    }
    else {
        _10867 = NewDouble(DBL_PTR(_10866)->dbl * (double)8);
    }
    DeRef(_10866);
    _10866 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18855) && IS_ATOM_INT(_10867)) {
        _10868 = _free_list_18855 + _10867;
        if ((long)((unsigned long)_10868 + (unsigned long)HIGH_BITS) >= 0) 
        _10868 = NewDouble((double)_10868);
    }
    else {
        if (IS_ATOM_INT(_free_list_18855)) {
            _10868 = NewDouble((double)_free_list_18855 + DBL_PTR(_10867)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10867)) {
                _10868 = NewDouble(DBL_PTR(_free_list_18855)->dbl + (double)_10867);
            }
            else
            _10868 = NewDouble(DBL_PTR(_free_list_18855)->dbl + DBL_PTR(_10867)->dbl);
        }
    }
    DeRef(_10867);
    _10867 = NOVALUE;
    DeRef(_pos_inlined_seek_at_834_18997);
    _pos_inlined_seek_at_834_18997 = _10868;
    _10868 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_834_18997);
    DeRef(_seek_1__tmp_at837_18999);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_834_18997;
    _seek_1__tmp_at837_18999 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_837_18998 = machine(19, _seek_1__tmp_at837_18999);
    DeRef(_pos_inlined_seek_at_834_18997);
    _pos_inlined_seek_at_834_18997 = NOVALUE;
    DeRef(_seek_1__tmp_at837_18999);
    _seek_1__tmp_at837_18999 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (free_count-i+1)*8)*/
    if (IS_ATOM_INT(_i_18852)) {
        _10869 = _free_count_18861 - _i_18852;
        if ((long)((unsigned long)_10869 +(unsigned long) HIGH_BITS) >= 0){
            _10869 = NewDouble((double)_10869);
        }
    }
    else {
        _10869 = NewDouble((double)_free_count_18861 - DBL_PTR(_i_18852)->dbl);
    }
    if (IS_ATOM_INT(_10869)) {
        _10870 = _10869 + 1;
        if (_10870 > MAXINT){
            _10870 = NewDouble((double)_10870);
        }
    }
    else
    _10870 = binary_op(PLUS, 1, _10869);
    DeRef(_10869);
    _10869 = NOVALUE;
    if (IS_ATOM_INT(_10870)) {
        if (_10870 == (short)_10870)
        _10871 = _10870 * 8;
        else
        _10871 = NewDouble(_10870 * (double)8);
    }
    else {
        _10871 = NewDouble(DBL_PTR(_10870)->dbl * (double)8);
    }
    DeRef(_10870);
    _10870 = NOVALUE;
    _0 = _remaining_18862;
    _remaining_18862 = _16get_bytes(_51current_db_18100, _10871);
    DeRef(_0);
    _10871 = NOVALUE;

    /** 		free_count += 1*/
    _free_count_18861 = _free_count_18861 + 1;

    /** 		io:seek(current_db, FREE_COUNT)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at883_19007);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 7;
    _seek_1__tmp_at883_19007 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_883_19006 = machine(19, _seek_1__tmp_at883_19007);
    DeRefi(_seek_1__tmp_at883_19007);
    _seek_1__tmp_at883_19007 = NOVALUE;

    /** 		put4(free_count)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)_free_count_18861;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at898_19009);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at898_19009 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at898_19009); // DJP 

    /** end procedure*/
    goto L13; // [920] 923
L13: 
    DeRefi(_put4_1__tmp_at898_19009);
    _put4_1__tmp_at898_19009 = NOVALUE;

    /** 		io:seek(current_db, free_list+(i-1)*8)*/
    if (IS_ATOM_INT(_i_18852)) {
        _10874 = _i_18852 - 1;
        if ((long)((unsigned long)_10874 +(unsigned long) HIGH_BITS) >= 0){
            _10874 = NewDouble((double)_10874);
        }
    }
    else {
        _10874 = NewDouble(DBL_PTR(_i_18852)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_10874)) {
        if (_10874 == (short)_10874)
        _10875 = _10874 * 8;
        else
        _10875 = NewDouble(_10874 * (double)8);
    }
    else {
        _10875 = NewDouble(DBL_PTR(_10874)->dbl * (double)8);
    }
    DeRef(_10874);
    _10874 = NOVALUE;
    if (IS_ATOM_INT(_free_list_18855) && IS_ATOM_INT(_10875)) {
        _10876 = _free_list_18855 + _10875;
        if ((long)((unsigned long)_10876 + (unsigned long)HIGH_BITS) >= 0) 
        _10876 = NewDouble((double)_10876);
    }
    else {
        if (IS_ATOM_INT(_free_list_18855)) {
            _10876 = NewDouble((double)_free_list_18855 + DBL_PTR(_10875)->dbl);
        }
        else {
            if (IS_ATOM_INT(_10875)) {
                _10876 = NewDouble(DBL_PTR(_free_list_18855)->dbl + (double)_10875);
            }
            else
            _10876 = NewDouble(DBL_PTR(_free_list_18855)->dbl + DBL_PTR(_10875)->dbl);
        }
    }
    DeRef(_10875);
    _10875 = NOVALUE;
    DeRef(_pos_inlined_seek_at_940_19014);
    _pos_inlined_seek_at_940_19014 = _10876;
    _10876 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_940_19014);
    DeRef(_seek_1__tmp_at943_19016);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_940_19014;
    _seek_1__tmp_at943_19016 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_943_19015 = machine(19, _seek_1__tmp_at943_19016);
    DeRef(_pos_inlined_seek_at_940_19014);
    _pos_inlined_seek_at_940_19014 = NOVALUE;
    DeRef(_seek_1__tmp_at943_19016);
    _seek_1__tmp_at943_19016 = NOVALUE;

    /** 		put4(p)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_p_18850)) {
        *poke4_addr = (unsigned long)_p_18850;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_p_18850)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at958_19018);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at958_19018 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at958_19018); // DJP 

    /** end procedure*/
    goto L14; // [980] 983
L14: 
    DeRefi(_put4_1__tmp_at958_19018);
    _put4_1__tmp_at958_19018 = NOVALUE;

    /** 		put4(psize)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_psize_18851)) {
        *poke4_addr = (unsigned long)_psize_18851;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_psize_18851)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at986_19020);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at986_19020 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at986_19020); // DJP 

    /** end procedure*/
    goto L15; // [1008] 1011
L15: 
    DeRefi(_put4_1__tmp_at986_19020);
    _put4_1__tmp_at986_19020 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _remaining_18862); // DJP 

    /** end procedure*/
    goto L16; // [1024] 1027
L16: 
LE: 

    /** 	if new_space then*/
    if (_new_space_18857 == 0) {
        goto L17; // [1032] 1046
    }
    else {
        if (!IS_ATOM_INT(_new_space_18857) && DBL_PTR(_new_space_18857)->dbl == 0.0){
            goto L17; // [1032] 1046
        }
    }

    /** 		db_free(to_be_freed) -- free the old space*/
    Ref(_to_be_freed_18858);
    DeRef(_10877);
    _10877 = _to_be_freed_18858;
    _51db_free(_10877);
    _10877 = NOVALUE;
L17: 

    /** end procedure*/
    DeRef(_p_18850);
    DeRef(_psize_18851);
    DeRef(_i_18852);
    DeRef(_size_18853);
    DeRef(_addr_18854);
    DeRef(_free_list_18855);
    DeRef(_free_list_space_18856);
    DeRef(_new_space_18857);
    DeRef(_to_be_freed_18858);
    DeRef(_prev_addr_18859);
    DeRef(_prev_size_18860);
    DeRef(_remaining_18862);
    DeRef(_10834);
    _10834 = NOVALUE;
    DeRef(_10842);
    _10842 = NOVALUE;
    DeRef(_10858);
    _10858 = NOVALUE;
    return;
    ;
}


void _51save_keys()
{
    int _k_19026 = NOVALUE;
    int _10884 = NOVALUE;
    int _10880 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if caching_option = 1 then*/

    /** 		if current_table_pos > 0 then*/
    if (binary_op_a(LESSEQ, _51current_table_pos_18101, 0)){
        goto L1; // [13] 81
    }

    /** 			k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_51current_table_pos_18101);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _51current_table_pos_18101;
    _10880 = MAKE_SEQ(_1);
    _k_19026 = find_from(_10880, _51cache_index_18109, 1);
    DeRefDS(_10880);
    _10880 = NOVALUE;

    /** 			if k != 0 then*/
    if (_k_19026 == 0)
    goto L2; // [36] 53

    /** 				key_cache[k] = key_pointers*/
    RefDS(_51key_pointers_18107);
    _2 = (int)SEQ_PTR(_51key_cache_18108);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _51key_cache_18108 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _k_19026);
    _1 = *(int *)_2;
    *(int *)_2 = _51key_pointers_18107;
    DeRef(_1);
    goto L3; // [50] 80
L2: 

    /** 				key_cache = append(key_cache, key_pointers)*/
    RefDS(_51key_pointers_18107);
    Append(&_51key_cache_18108, _51key_cache_18108, _51key_pointers_18107);

    /** 				cache_index = append(cache_index, {current_db, current_table_pos})*/
    Ref(_51current_table_pos_18101);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _51current_table_pos_18101;
    _10884 = MAKE_SEQ(_1);
    RefDS(_10884);
    Append(&_51cache_index_18109, _51cache_index_18109, _10884);
    DeRefDS(_10884);
    _10884 = NOVALUE;
L3: 
L1: 

    /** end procedure*/
    return;
    ;
}


int _51db_create(int _path_19123, int _lock_method_19124, int _init_tables_19125, int _init_free_19126)
{
    int _db_19127 = NOVALUE;
    int _lock_file_1__tmp_at246_19167 = NOVALUE;
    int _lock_file_inlined_lock_file_at_246_19166 = NOVALUE;
    int _put4_1__tmp_at368_19176 = NOVALUE;
    int _put4_1__tmp_at396_19178 = NOVALUE;
    int _put4_1__tmp_at439_19184 = NOVALUE;
    int _x_inlined_put4_at_436_19183 = NOVALUE;
    int _put4_1__tmp_at478_19189 = NOVALUE;
    int _x_inlined_put4_at_475_19188 = NOVALUE;
    int _put4_1__tmp_at506_19191 = NOVALUE;
    int _s_inlined_putn_at_542_19195 = NOVALUE;
    int _put4_1__tmp_at574_19200 = NOVALUE;
    int _x_inlined_put4_at_571_19199 = NOVALUE;
    int _s_inlined_putn_at_610_19204 = NOVALUE;
    int _10983 = NOVALUE;
    int _10982 = NOVALUE;
    int _10981 = NOVALUE;
    int _10980 = NOVALUE;
    int _10979 = NOVALUE;
    int _10978 = NOVALUE;
    int _10977 = NOVALUE;
    int _10976 = NOVALUE;
    int _10975 = NOVALUE;
    int _10974 = NOVALUE;
    int _10973 = NOVALUE;
    int _10955 = NOVALUE;
    int _10953 = NOVALUE;
    int _10952 = NOVALUE;
    int _10950 = NOVALUE;
    int _10949 = NOVALUE;
    int _10947 = NOVALUE;
    int _10946 = NOVALUE;
    int _10944 = NOVALUE;
    int _0, _1, _2;
    

    /** 	db = find(path, Known_Aliases)*/
    _db_19127 = find_from(_path_19123, _51Known_Aliases_18121, 1);

    /** 	if db then*/
    if (_db_19127 == 0)
    {
        goto L1; // [20] 100
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _10944 = (int)*(((s1_ptr)_2)->base + _db_19127);
    DeRefDS(_path_19123);
    _2 = (int)SEQ_PTR(_10944);
    _path_19123 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_19123);
    _10944 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _10946 = (int)*(((s1_ptr)_2)->base + _db_19127);
    _2 = (int)SEQ_PTR(_10946);
    _10947 = (int)*(((s1_ptr)_2)->base + 2);
    _10946 = NOVALUE;
    _2 = (int)SEQ_PTR(_10947);
    _lock_method_19124 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_19124)){
        _lock_method_19124 = (long)DBL_PTR(_lock_method_19124)->dbl;
    }
    _10947 = NOVALUE;

    /** 		init_tables = Alias_Details[db][2][CONNECT_TABLES]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _10949 = (int)*(((s1_ptr)_2)->base + _db_19127);
    _2 = (int)SEQ_PTR(_10949);
    _10950 = (int)*(((s1_ptr)_2)->base + 2);
    _10949 = NOVALUE;
    _2 = (int)SEQ_PTR(_10950);
    _init_tables_19125 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_init_tables_19125)){
        _init_tables_19125 = (long)DBL_PTR(_init_tables_19125)->dbl;
    }
    _10950 = NOVALUE;

    /** 		init_free = Alias_Details[db][2][CONNECT_FREE]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _10952 = (int)*(((s1_ptr)_2)->base + _db_19127);
    _2 = (int)SEQ_PTR(_10952);
    _10953 = (int)*(((s1_ptr)_2)->base + 2);
    _10952 = NOVALUE;
    _2 = (int)SEQ_PTR(_10953);
    _init_free_19126 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_init_free_19126)){
        _init_free_19126 = (long)DBL_PTR(_init_free_19126)->dbl;
    }
    _10953 = NOVALUE;
    goto L2; // [97] 142
L1: 

    /** 		path = filesys:canonical_path( defaultext(path, "edb") )*/
    RefDS(_path_19123);
    RefDS(_10938);
    _10955 = _13defaultext(_path_19123, _10938);
    _0 = _path_19123;
    _path_19123 = _13canonical_path(_10955, 0, 0);
    DeRefDS(_0);
    _10955 = NOVALUE;

    /** 		if init_tables < 1 then*/
    if (_init_tables_19125 >= 1)
    goto L3; // [119] 129

    /** 			init_tables = 1*/
    _init_tables_19125 = 1;
L3: 

    /** 		if init_free < 0 then*/
    if (_init_free_19126 >= 0)
    goto L4; // [131] 141

    /** 			init_free = 0*/
    _init_free_19126 = 0;
L4: 
L2: 

    /** 	db = open(path, "rb")*/
    _db_19127 = EOpen(_path_19123, _1133, 0);

    /** 	if db != -1 then*/
    if (_db_19127 == -1)
    goto L5; // [151] 168

    /** 		close(db)*/
    EClose(_db_19127);

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_path_19123);
    return -2;
L5: 

    /** 	db = open(path, "wb")*/
    _db_19127 = EOpen(_path_19123, _10961, 0);

    /** 	if db = -1 then*/
    if (_db_19127 != -1)
    goto L6; // [177] 190

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_19123);
    return -1;
L6: 

    /** 	close(db)*/
    EClose(_db_19127);

    /** 	db = open(path, "ub")*/
    _db_19127 = EOpen(_path_19123, _10964, 0);

    /** 	if db = -1 then*/
    if (_db_19127 != -1)
    goto L7; // [203] 216

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_19123);
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_19124 != 1)
    goto L8; // [220] 234

    /** 		lock_method = DB_LOCK_NO*/
    _lock_method_19124 = 0;
L8: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_19124 != 2)
    goto L9; // [238] 274

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at246_19167;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_19127;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at246_19167 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_246_19166 = machine(61, _lock_file_1__tmp_at246_19167);
    DeRef(_lock_file_1__tmp_at246_19167);
    _lock_file_1__tmp_at246_19167 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_246_19166 != 0)
    goto LA; // [261] 273

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_19123);
    return -3;
LA: 
L9: 

    /** 	save_keys()*/
    _51save_keys();

    /** 	current_db = db*/
    _51current_db_18100 = _db_19127;

    /** 	current_lock = lock_method*/
    _51current_lock_18106 = _lock_method_19124;

    /** 	current_table_pos = -1*/
    DeRef(_51current_table_pos_18101);
    _51current_table_pos_18101 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_51current_table_name_18102);
    _51current_table_name_18102 = _5;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_19123);
    Append(&_51db_names_18103, _51db_names_18103, _path_19123);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_51db_lock_methods_18105, _51db_lock_methods_18105, _lock_method_19124);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_51db_file_nums_18104, _51db_file_nums_18104, _db_19127);

    /** 	put1(DB_MAGIC) -- so we know what type of file it is*/

    /** 	puts(current_db, x)*/
    EPuts(_51current_db_18100, 77); // DJP 

    /** end procedure*/
    goto LB; // [335] 338
LB: 

    /** 	put1(DB_MAJOR) -- major version*/

    /** 	puts(current_db, x)*/
    EPuts(_51current_db_18100, 4); // DJP 

    /** end procedure*/
    goto LC; // [349] 352
LC: 

    /** 	put1(DB_MINOR) -- minor version*/

    /** 	puts(current_db, x)*/
    EPuts(_51current_db_18100, 0); // DJP 

    /** end procedure*/
    goto LD; // [363] 366
LD: 

    /** 	put4(19)  -- pointer to tables*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)19;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at368_19176);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at368_19176 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at368_19176); // DJP 

    /** end procedure*/
    goto LE; // [389] 392
LE: 
    DeRefi(_put4_1__tmp_at368_19176);
    _put4_1__tmp_at368_19176 = NOVALUE;

    /** 	put4(0)   -- number of free blocks*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at396_19178);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at396_19178 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at396_19178); // DJP 

    /** end procedure*/
    goto LF; // [417] 420
LF: 
    DeRefi(_put4_1__tmp_at396_19178);
    _put4_1__tmp_at396_19178 = NOVALUE;

    /** 	put4(23 + init_tables * SIZEOF_TABLE_HEADER + 4)   -- pointer to free list*/
    if (_init_tables_19125 == (short)_init_tables_19125)
    _10973 = _init_tables_19125 * 16;
    else
    _10973 = NewDouble(_init_tables_19125 * (double)16);
    if (IS_ATOM_INT(_10973)) {
        _10974 = 23 + _10973;
        if ((long)((unsigned long)_10974 + (unsigned long)HIGH_BITS) >= 0) 
        _10974 = NewDouble((double)_10974);
    }
    else {
        _10974 = NewDouble((double)23 + DBL_PTR(_10973)->dbl);
    }
    DeRef(_10973);
    _10973 = NOVALUE;
    if (IS_ATOM_INT(_10974)) {
        _10975 = _10974 + 4;
        if ((long)((unsigned long)_10975 + (unsigned long)HIGH_BITS) >= 0) 
        _10975 = NewDouble((double)_10975);
    }
    else {
        _10975 = NewDouble(DBL_PTR(_10974)->dbl + (double)4);
    }
    DeRef(_10974);
    _10974 = NOVALUE;
    DeRef(_x_inlined_put4_at_436_19183);
    _x_inlined_put4_at_436_19183 = _10975;
    _10975 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_436_19183)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_436_19183;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_436_19183)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at439_19184);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at439_19184 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at439_19184); // DJP 

    /** end procedure*/
    goto L10; // [460] 463
L10: 
    DeRef(_x_inlined_put4_at_436_19183);
    _x_inlined_put4_at_436_19183 = NOVALUE;
    DeRefi(_put4_1__tmp_at439_19184);
    _put4_1__tmp_at439_19184 = NOVALUE;

    /** 	put4( 8 + init_tables * SIZEOF_TABLE_HEADER)  -- allocated size*/
    if (_init_tables_19125 == (short)_init_tables_19125)
    _10976 = _init_tables_19125 * 16;
    else
    _10976 = NewDouble(_init_tables_19125 * (double)16);
    if (IS_ATOM_INT(_10976)) {
        _10977 = 8 + _10976;
        if ((long)((unsigned long)_10977 + (unsigned long)HIGH_BITS) >= 0) 
        _10977 = NewDouble((double)_10977);
    }
    else {
        _10977 = NewDouble((double)8 + DBL_PTR(_10976)->dbl);
    }
    DeRef(_10976);
    _10976 = NOVALUE;
    DeRef(_x_inlined_put4_at_475_19188);
    _x_inlined_put4_at_475_19188 = _10977;
    _10977 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_475_19188)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_475_19188;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_475_19188)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at478_19189);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at478_19189 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at478_19189); // DJP 

    /** end procedure*/
    goto L11; // [499] 502
L11: 
    DeRef(_x_inlined_put4_at_475_19188);
    _x_inlined_put4_at_475_19188 = NOVALUE;
    DeRefi(_put4_1__tmp_at478_19189);
    _put4_1__tmp_at478_19189 = NOVALUE;

    /** 	put4(0)   -- number of tables that currently exist*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at506_19191);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at506_19191 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at506_19191); // DJP 

    /** end procedure*/
    goto L12; // [527] 530
L12: 
    DeRefi(_put4_1__tmp_at506_19191);
    _put4_1__tmp_at506_19191 = NOVALUE;

    /** 	putn(repeat(0, init_tables * SIZEOF_TABLE_HEADER))*/
    _10978 = _init_tables_19125 * 16;
    _10979 = Repeat(0, _10978);
    _10978 = NOVALUE;
    DeRefi(_s_inlined_putn_at_542_19195);
    _s_inlined_putn_at_542_19195 = _10979;
    _10979 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_542_19195); // DJP 

    /** end procedure*/
    goto L13; // [556] 559
L13: 
    DeRefi(_s_inlined_putn_at_542_19195);
    _s_inlined_putn_at_542_19195 = NOVALUE;

    /** 	put4(4+init_free*8)   -- allocated size*/
    if (_init_free_19126 == (short)_init_free_19126)
    _10980 = _init_free_19126 * 8;
    else
    _10980 = NewDouble(_init_free_19126 * (double)8);
    if (IS_ATOM_INT(_10980)) {
        _10981 = 4 + _10980;
        if ((long)((unsigned long)_10981 + (unsigned long)HIGH_BITS) >= 0) 
        _10981 = NewDouble((double)_10981);
    }
    else {
        _10981 = NewDouble((double)4 + DBL_PTR(_10980)->dbl);
    }
    DeRef(_10980);
    _10980 = NOVALUE;
    DeRef(_x_inlined_put4_at_571_19199);
    _x_inlined_put4_at_571_19199 = _10981;
    _10981 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_571_19199)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_571_19199;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_571_19199)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at574_19200);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at574_19200 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at574_19200); // DJP 

    /** end procedure*/
    goto L14; // [595] 598
L14: 
    DeRef(_x_inlined_put4_at_571_19199);
    _x_inlined_put4_at_571_19199 = NOVALUE;
    DeRefi(_put4_1__tmp_at574_19200);
    _put4_1__tmp_at574_19200 = NOVALUE;

    /** 	putn(repeat(0, init_free * 8))*/
    _10982 = _init_free_19126 * 8;
    _10983 = Repeat(0, _10982);
    _10982 = NOVALUE;
    DeRefi(_s_inlined_putn_at_610_19204);
    _s_inlined_putn_at_610_19204 = _10983;
    _10983 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_610_19204); // DJP 

    /** end procedure*/
    goto L15; // [624] 627
L15: 
    DeRefi(_s_inlined_putn_at_610_19204);
    _s_inlined_putn_at_610_19204 = NOVALUE;

    /** 	return DB_OK*/
    DeRefDS(_path_19123);
    return 0;
    ;
}


int _51db_open(int _path_19207, int _lock_method_19208)
{
    int _db_19209 = NOVALUE;
    int _magic_19210 = NOVALUE;
    int _lock_file_1__tmp_at161_19237 = NOVALUE;
    int _lock_file_inlined_lock_file_at_161_19236 = NOVALUE;
    int _lock_file_1__tmp_at207_19244 = NOVALUE;
    int _lock_file_inlined_lock_file_at_207_19243 = NOVALUE;
    int _10994 = NOVALUE;
    int _10992 = NOVALUE;
    int _10990 = NOVALUE;
    int _10988 = NOVALUE;
    int _10987 = NOVALUE;
    int _10985 = NOVALUE;
    int _0, _1, _2;
    

    /** 	db = find(path, Known_Aliases)*/
    _db_19209 = find_from(_path_19207, _51Known_Aliases_18121, 1);

    /** 	if db then*/
    if (_db_19209 == 0)
    {
        goto L1; // [16] 56
    }
    else{
    }

    /** 		path = Alias_Details[db][1]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _10985 = (int)*(((s1_ptr)_2)->base + _db_19209);
    DeRefDS(_path_19207);
    _2 = (int)SEQ_PTR(_10985);
    _path_19207 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_19207);
    _10985 = NOVALUE;

    /** 		lock_method = Alias_Details[db][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _10987 = (int)*(((s1_ptr)_2)->base + _db_19209);
    _2 = (int)SEQ_PTR(_10987);
    _10988 = (int)*(((s1_ptr)_2)->base + 2);
    _10987 = NOVALUE;
    _2 = (int)SEQ_PTR(_10988);
    _lock_method_19208 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_19208)){
        _lock_method_19208 = (long)DBL_PTR(_lock_method_19208)->dbl;
    }
    _10988 = NOVALUE;
    goto L2; // [53] 74
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_19207);
    RefDS(_10938);
    _10990 = _13defaultext(_path_19207, _10938);
    _0 = _path_19207;
    _path_19207 = _13canonical_path(_10990, 0, 0);
    DeRefDS(_0);
    _10990 = NOVALUE;
L2: 

    /** 	if lock_method = DB_LOCK_NO or*/
    _10992 = (_lock_method_19208 == 0);
    if (_10992 != 0) {
        goto L3; // [82] 97
    }
    _10994 = (_lock_method_19208 == 2);
    if (_10994 == 0)
    {
        DeRef(_10994);
        _10994 = NOVALUE;
        goto L4; // [93] 107
    }
    else{
        DeRef(_10994);
        _10994 = NOVALUE;
    }
L3: 

    /** 		db = open(path, "ub")*/
    _db_19209 = EOpen(_path_19207, _10964, 0);
    goto L5; // [104] 115
L4: 

    /** 		db = open(path, "rb")*/
    _db_19209 = EOpen(_path_19207, _1133, 0);
L5: 

    /** ifdef WINDOWS then*/

    /** 	if lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_19208 != 1)
    goto L6; // [121] 135

    /** 		lock_method = DB_LOCK_EXCLUSIVE*/
    _lock_method_19208 = 2;
L6: 

    /** 	if db = -1 then*/
    if (_db_19209 != -1)
    goto L7; // [137] 150

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_19207);
    DeRef(_10992);
    _10992 = NOVALUE;
    return -1;
L7: 

    /** 	if lock_method = DB_LOCK_EXCLUSIVE then*/
    if (_lock_method_19208 != 2)
    goto L8; // [154] 196

    /** 		if not io:lock_file(db, io:LOCK_EXCLUSIVE, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at161_19237;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_19209;
    *((int *)(_2+8)) = 2;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at161_19237 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_161_19236 = machine(61, _lock_file_1__tmp_at161_19237);
    DeRef(_lock_file_1__tmp_at161_19237);
    _lock_file_1__tmp_at161_19237 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_161_19236 != 0)
    goto L9; // [177] 241

    /** 			close(db)*/
    EClose(_db_19209);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_19207);
    DeRef(_10992);
    _10992 = NOVALUE;
    return -3;
    goto L9; // [193] 241
L8: 

    /** 	elsif lock_method = DB_LOCK_SHARED then*/
    if (_lock_method_19208 != 1)
    goto LA; // [200] 240

    /** 		if not io:lock_file(db, io:LOCK_SHARED, {}) then*/

    /** 	return machine_func(M_LOCK_FILE, {fn, t, r})*/
    _0 = _lock_file_1__tmp_at207_19244;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _db_19209;
    *((int *)(_2+8)) = 1;
    RefDS(_5);
    *((int *)(_2+12)) = _5;
    _lock_file_1__tmp_at207_19244 = MAKE_SEQ(_1);
    DeRef(_0);
    _lock_file_inlined_lock_file_at_207_19243 = machine(61, _lock_file_1__tmp_at207_19244);
    DeRef(_lock_file_1__tmp_at207_19244);
    _lock_file_1__tmp_at207_19244 = NOVALUE;
    if (_lock_file_inlined_lock_file_at_207_19243 != 0)
    goto LB; // [223] 239

    /** 			close(db)*/
    EClose(_db_19209);

    /** 			return DB_LOCK_FAIL*/
    DeRefDS(_path_19207);
    DeRef(_10992);
    _10992 = NOVALUE;
    return -3;
LB: 
LA: 
L9: 

    /** 	magic = getc(db)*/
    if (_db_19209 != last_r_file_no) {
        last_r_file_ptr = which_file(_db_19209, EF_READ);
        last_r_file_no = _db_19209;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _magic_19210 = getKBchar();
        }
        else
        _magic_19210 = getc(last_r_file_ptr);
    }
    else
    _magic_19210 = getc(last_r_file_ptr);

    /** 	if magic != DB_MAGIC then*/
    if (_magic_19210 == 77)
    goto LC; // [248] 265

    /** 		close(db)*/
    EClose(_db_19209);

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_path_19207);
    DeRef(_10992);
    _10992 = NOVALUE;
    return -1;
LC: 

    /** 	save_keys()*/
    _51save_keys();

    /** 	current_db = db */
    _51current_db_18100 = _db_19209;

    /** 	current_table_pos = -1*/
    DeRef(_51current_table_pos_18101);
    _51current_table_pos_18101 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_51current_table_name_18102);
    _51current_table_name_18102 = _5;

    /** 	current_lock = lock_method*/
    _51current_lock_18106 = _lock_method_19208;

    /** 	db_names = append(db_names, path)*/
    RefDS(_path_19207);
    Append(&_51db_names_18103, _51db_names_18103, _path_19207);

    /** 	db_lock_methods = append(db_lock_methods, lock_method)*/
    Append(&_51db_lock_methods_18105, _51db_lock_methods_18105, _lock_method_19208);

    /** 	db_file_nums = append(db_file_nums, db)*/
    Append(&_51db_file_nums_18104, _51db_file_nums_18104, _db_19209);

    /** 	return DB_OK*/
    DeRefDS(_path_19207);
    DeRef(_10992);
    _10992 = NOVALUE;
    return 0;
    ;
}


int _51db_select(int _path_19254, int _lock_method_19255)
{
    int _index_19256 = NOVALUE;
    int _11014 = NOVALUE;
    int _11012 = NOVALUE;
    int _11011 = NOVALUE;
    int _11009 = NOVALUE;
    int _0, _1, _2;
    

    /** 	index = find(path, Known_Aliases)*/
    _index_19256 = find_from(_path_19254, _51Known_Aliases_18121, 1);

    /** 	if index then*/
    if (_index_19256 == 0)
    {
        goto L1; // [16] 56
    }
    else{
    }

    /** 		path = Alias_Details[index][1]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _11009 = (int)*(((s1_ptr)_2)->base + _index_19256);
    DeRefDS(_path_19254);
    _2 = (int)SEQ_PTR(_11009);
    _path_19254 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_path_19254);
    _11009 = NOVALUE;

    /** 		lock_method = Alias_Details[index][2][CONNECT_LOCK]*/
    _2 = (int)SEQ_PTR(_51Alias_Details_18122);
    _11011 = (int)*(((s1_ptr)_2)->base + _index_19256);
    _2 = (int)SEQ_PTR(_11011);
    _11012 = (int)*(((s1_ptr)_2)->base + 2);
    _11011 = NOVALUE;
    _2 = (int)SEQ_PTR(_11012);
    _lock_method_19255 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_lock_method_19255)){
        _lock_method_19255 = (long)DBL_PTR(_lock_method_19255)->dbl;
    }
    _11012 = NOVALUE;
    goto L2; // [53] 74
L1: 

    /** 		path = filesys:canonical_path( filesys:defaultext(path, "edb") )*/
    RefDS(_path_19254);
    RefDS(_10938);
    _11014 = _13defaultext(_path_19254, _10938);
    _0 = _path_19254;
    _path_19254 = _13canonical_path(_11014, 0, 0);
    DeRefDS(_0);
    _11014 = NOVALUE;
L2: 

    /** 	index = eu:find(path, db_names)*/
    _index_19256 = find_from(_path_19254, _51db_names_18103, 1);

    /** 	if index = 0 then*/
    if (_index_19256 != 0)
    goto L3; // [85] 138

    /** 		if lock_method = -1 then*/
    if (_lock_method_19255 != -1)
    goto L4; // [91] 104

    /** 			return DB_OPEN_FAIL*/
    DeRefDS(_path_19254);
    return -1;
L4: 

    /** 		index = db_open(path, lock_method)*/
    RefDS(_path_19254);
    _index_19256 = _51db_open(_path_19254, _lock_method_19255);
    if (!IS_ATOM_INT(_index_19256)) {
        _1 = (long)(DBL_PTR(_index_19256)->dbl);
        if (UNIQUE(DBL_PTR(_index_19256)) && (DBL_PTR(_index_19256)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_19256);
        _index_19256 = _1;
    }

    /** 		if index != DB_OK then*/
    if (_index_19256 == 0)
    goto L5; // [117] 128

    /** 			return index*/
    DeRefDS(_path_19254);
    return _index_19256;
L5: 

    /** 		index = eu:find(path, db_names)*/
    _index_19256 = find_from(_path_19254, _51db_names_18103, 1);
L3: 

    /** 	save_keys()*/
    _51save_keys();

    /** 	current_db = db_file_nums[index]*/
    _2 = (int)SEQ_PTR(_51db_file_nums_18104);
    _51current_db_18100 = (int)*(((s1_ptr)_2)->base + _index_19256);
    if (!IS_ATOM_INT(_51current_db_18100))
    _51current_db_18100 = (long)DBL_PTR(_51current_db_18100)->dbl;

    /** 	current_lock = db_lock_methods[index]*/
    _2 = (int)SEQ_PTR(_51db_lock_methods_18105);
    _51current_lock_18106 = (int)*(((s1_ptr)_2)->base + _index_19256);
    if (!IS_ATOM_INT(_51current_lock_18106))
    _51current_lock_18106 = (long)DBL_PTR(_51current_lock_18106)->dbl;

    /** 	current_table_pos = -1*/
    DeRef(_51current_table_pos_18101);
    _51current_table_pos_18101 = -1;

    /** 	current_table_name = ""*/
    RefDS(_5);
    DeRef(_51current_table_name_18102);
    _51current_table_name_18102 = _5;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_51key_pointers_18107);
    _51key_pointers_18107 = _5;

    /** 	return DB_OK*/
    DeRefDS(_path_19254);
    return 0;
    ;
}


void _51db_close()
{
    int _unlock_file_1__tmp_at25_19285 = NOVALUE;
    int _index_19280 = NOVALUE;
    int _11031 = NOVALUE;
    int _11030 = NOVALUE;
    int _11029 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if current_db = -1 then*/
    if (_51current_db_18100 != -1)
    goto L1; // [5] 15

    /** 		return*/
    return;
L1: 

    /** 	if current_lock then*/
    if (_51current_lock_18106 == 0)
    {
        goto L2; // [19] 43
    }
    else{
    }

    /** 		io:unlock_file(current_db, {})*/

    /** 	machine_proc(M_UNLOCK_FILE, {fn, r})*/
    RefDS(_5);
    DeRef(_unlock_file_1__tmp_at25_19285);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _5;
    _unlock_file_1__tmp_at25_19285 = MAKE_SEQ(_1);
    machine(62, _unlock_file_1__tmp_at25_19285);

    /** end procedure*/
    goto L3; // [37] 40
L3: 
    DeRef(_unlock_file_1__tmp_at25_19285);
    _unlock_file_1__tmp_at25_19285 = NOVALUE;
L2: 

    /** 	close(current_db)*/
    EClose(_51current_db_18100);

    /** 	index = eu:find(current_db, db_file_nums)*/
    _index_19280 = find_from(_51current_db_18100, _51db_file_nums_18104, 1);

    /** 	db_names = remove(db_names, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_51db_names_18103);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_19280)) ? _index_19280 : (long)(DBL_PTR(_index_19280)->dbl);
        int stop = (IS_ATOM_INT(_index_19280)) ? _index_19280 : (long)(DBL_PTR(_index_19280)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_51db_names_18103), start, &_51db_names_18103 );
            }
            else Tail(SEQ_PTR(_51db_names_18103), stop+1, &_51db_names_18103);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_51db_names_18103), start, &_51db_names_18103);
        }
        else {
            assign_slice_seq = &assign_space;
            _51db_names_18103 = Remove_elements(start, stop, (SEQ_PTR(_51db_names_18103)->ref == 1));
        }
    }

    /** 	db_file_nums = remove(db_file_nums, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_51db_file_nums_18104);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_19280)) ? _index_19280 : (long)(DBL_PTR(_index_19280)->dbl);
        int stop = (IS_ATOM_INT(_index_19280)) ? _index_19280 : (long)(DBL_PTR(_index_19280)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_51db_file_nums_18104), start, &_51db_file_nums_18104 );
            }
            else Tail(SEQ_PTR(_51db_file_nums_18104), stop+1, &_51db_file_nums_18104);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_51db_file_nums_18104), start, &_51db_file_nums_18104);
        }
        else {
            assign_slice_seq = &assign_space;
            _51db_file_nums_18104 = Remove_elements(start, stop, (SEQ_PTR(_51db_file_nums_18104)->ref == 1));
        }
    }

    /** 	db_lock_methods = remove(db_lock_methods, index)*/
    {
        s1_ptr assign_space = SEQ_PTR(_51db_lock_methods_18105);
        int len = assign_space->length;
        int start = (IS_ATOM_INT(_index_19280)) ? _index_19280 : (long)(DBL_PTR(_index_19280)->dbl);
        int stop = (IS_ATOM_INT(_index_19280)) ? _index_19280 : (long)(DBL_PTR(_index_19280)->dbl);
        if (stop > len){
            stop = len;
        }
        if (start > len || start > stop || stop<1) {
        }
        else if (start < 2) {
            if (stop >= len) {
                Head( SEQ_PTR(_51db_lock_methods_18105), start, &_51db_lock_methods_18105 );
            }
            else Tail(SEQ_PTR(_51db_lock_methods_18105), stop+1, &_51db_lock_methods_18105);
        }
        else if (stop >= len){
            Head(SEQ_PTR(_51db_lock_methods_18105), start, &_51db_lock_methods_18105);
        }
        else {
            assign_slice_seq = &assign_space;
            _51db_lock_methods_18105 = Remove_elements(start, stop, (SEQ_PTR(_51db_lock_methods_18105)->ref == 1));
        }
    }

    /** 	for i = length(cache_index) to 1 by -1 do*/
    if (IS_SEQUENCE(_51cache_index_18109)){
            _11029 = SEQ_PTR(_51cache_index_18109)->length;
    }
    else {
        _11029 = 1;
    }
    {
        int _i_19291;
        _i_19291 = _11029;
L4: 
        if (_i_19291 < 1){
            goto L5; // [94] 145
        }

        /** 		if cache_index[i][1] = current_db then*/
        _2 = (int)SEQ_PTR(_51cache_index_18109);
        _11030 = (int)*(((s1_ptr)_2)->base + _i_19291);
        _2 = (int)SEQ_PTR(_11030);
        _11031 = (int)*(((s1_ptr)_2)->base + 1);
        _11030 = NOVALUE;
        if (binary_op_a(NOTEQ, _11031, _51current_db_18100)){
            _11031 = NOVALUE;
            goto L6; // [115] 138
        }
        _11031 = NOVALUE;

        /** 			cache_index = remove(cache_index, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_51cache_index_18109);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_19291)) ? _i_19291 : (long)(DBL_PTR(_i_19291)->dbl);
            int stop = (IS_ATOM_INT(_i_19291)) ? _i_19291 : (long)(DBL_PTR(_i_19291)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_51cache_index_18109), start, &_51cache_index_18109 );
                }
                else Tail(SEQ_PTR(_51cache_index_18109), stop+1, &_51cache_index_18109);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_51cache_index_18109), start, &_51cache_index_18109);
            }
            else {
                assign_slice_seq = &assign_space;
                _51cache_index_18109 = Remove_elements(start, stop, (SEQ_PTR(_51cache_index_18109)->ref == 1));
            }
        }

        /** 			key_cache = remove(key_cache, i)*/
        {
            s1_ptr assign_space = SEQ_PTR(_51key_cache_18108);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_i_19291)) ? _i_19291 : (long)(DBL_PTR(_i_19291)->dbl);
            int stop = (IS_ATOM_INT(_i_19291)) ? _i_19291 : (long)(DBL_PTR(_i_19291)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_51key_cache_18108), start, &_51key_cache_18108 );
                }
                else Tail(SEQ_PTR(_51key_cache_18108), stop+1, &_51key_cache_18108);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_51key_cache_18108), start, &_51key_cache_18108);
            }
            else {
                assign_slice_seq = &assign_space;
                _51key_cache_18108 = Remove_elements(start, stop, (SEQ_PTR(_51key_cache_18108)->ref == 1));
            }
        }
L6: 

        /** 	end for*/
        _i_19291 = _i_19291 + -1;
        goto L4; // [140] 101
L5: 
        ;
    }

    /** 	current_table_pos = -1*/
    DeRef(_51current_table_pos_18101);
    _51current_table_pos_18101 = -1;

    /** 	current_table_name = ""	*/
    RefDS(_5);
    DeRef(_51current_table_name_18102);
    _51current_table_name_18102 = _5;

    /** 	current_db = -1*/
    _51current_db_18100 = -1;

    /** 	key_pointers = {}*/
    RefDS(_5);
    DeRef(_51key_pointers_18107);
    _51key_pointers_18107 = _5;

    /** end procedure*/
    return;
    ;
}


int _51table_find(int _name_19301)
{
    int _tables_19302 = NOVALUE;
    int _nt_19303 = NOVALUE;
    int _t_header_19304 = NOVALUE;
    int _name_ptr_19305 = NOVALUE;
    int _seek_1__tmp_at6_19308 = NOVALUE;
    int _seek_inlined_seek_at_6_19307 = NOVALUE;
    int _seek_1__tmp_at44_19315 = NOVALUE;
    int _seek_inlined_seek_at_44_19314 = NOVALUE;
    int _seek_1__tmp_at84_19323 = NOVALUE;
    int _seek_inlined_seek_at_84_19322 = NOVALUE;
    int _seek_1__tmp_at106_19327 = NOVALUE;
    int _seek_inlined_seek_at_106_19326 = NOVALUE;
    int _11042 = NOVALUE;
    int _11040 = NOVALUE;
    int _11035 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at6_19308);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at6_19308 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_6_19307 = machine(19, _seek_1__tmp_at6_19308);
    DeRefi(_seek_1__tmp_at6_19308);
    _seek_1__tmp_at6_19308 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_51vLastErrors_18124)){
            _11035 = SEQ_PTR(_51vLastErrors_18124)->length;
    }
    else {
        _11035 = 1;
    }
    if (_11035 <= 0)
    goto L1; // [27] 36
    DeRefDS(_name_19301);
    DeRef(_tables_19302);
    DeRef(_nt_19303);
    DeRef(_t_header_19304);
    DeRef(_name_ptr_19305);
    return -1;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_19302;
    _tables_19302 = _51get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_19302);
    DeRef(_seek_1__tmp_at44_19315);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _tables_19302;
    _seek_1__tmp_at44_19315 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_44_19314 = machine(19, _seek_1__tmp_at44_19315);
    DeRef(_seek_1__tmp_at44_19315);
    _seek_1__tmp_at44_19315 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_19303;
    _nt_19303 = _51get4();
    DeRef(_0);

    /** 	t_header = tables+4*/
    DeRef(_t_header_19304);
    if (IS_ATOM_INT(_tables_19302)) {
        _t_header_19304 = _tables_19302 + 4;
        if ((long)((unsigned long)_t_header_19304 + (unsigned long)HIGH_BITS) >= 0) 
        _t_header_19304 = NewDouble((double)_t_header_19304);
    }
    else {
        _t_header_19304 = NewDouble(DBL_PTR(_tables_19302)->dbl + (double)4);
    }

    /** 	for i = 1 to nt do*/
    Ref(_nt_19303);
    DeRef(_11040);
    _11040 = _nt_19303;
    {
        int _i_19319;
        _i_19319 = 1;
L2: 
        if (binary_op_a(GREATER, _i_19319, _11040)){
            goto L3; // [74] 150
        }

        /** 		io:seek(current_db, t_header)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_t_header_19304);
        DeRef(_seek_1__tmp_at84_19323);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _t_header_19304;
        _seek_1__tmp_at84_19323 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_84_19322 = machine(19, _seek_1__tmp_at84_19323);
        DeRef(_seek_1__tmp_at84_19323);
        _seek_1__tmp_at84_19323 = NOVALUE;

        /** 		name_ptr = get4()*/
        _0 = _name_ptr_19305;
        _name_ptr_19305 = _51get4();
        DeRef(_0);

        /** 		io:seek(current_db, name_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_ptr_19305);
        DeRef(_seek_1__tmp_at106_19327);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _name_ptr_19305;
        _seek_1__tmp_at106_19327 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_106_19326 = machine(19, _seek_1__tmp_at106_19327);
        DeRef(_seek_1__tmp_at106_19327);
        _seek_1__tmp_at106_19327 = NOVALUE;

        /** 		if equal_string(name) > 0 then*/
        RefDS(_name_19301);
        _11042 = _51equal_string(_name_19301);
        if (binary_op_a(LESSEQ, _11042, 0)){
            DeRef(_11042);
            _11042 = NOVALUE;
            goto L4; // [126] 137
        }
        DeRef(_11042);
        _11042 = NOVALUE;

        /** 			return t_header*/
        DeRef(_i_19319);
        DeRefDS(_name_19301);
        DeRef(_tables_19302);
        DeRef(_nt_19303);
        DeRef(_name_ptr_19305);
        return _t_header_19304;
L4: 

        /** 		t_header += SIZEOF_TABLE_HEADER*/
        _0 = _t_header_19304;
        if (IS_ATOM_INT(_t_header_19304)) {
            _t_header_19304 = _t_header_19304 + 16;
            if ((long)((unsigned long)_t_header_19304 + (unsigned long)HIGH_BITS) >= 0) 
            _t_header_19304 = NewDouble((double)_t_header_19304);
        }
        else {
            _t_header_19304 = NewDouble(DBL_PTR(_t_header_19304)->dbl + (double)16);
        }
        DeRef(_0);

        /** 	end for*/
        _0 = _i_19319;
        if (IS_ATOM_INT(_i_19319)) {
            _i_19319 = _i_19319 + 1;
            if ((long)((unsigned long)_i_19319 +(unsigned long) HIGH_BITS) >= 0){
                _i_19319 = NewDouble((double)_i_19319);
            }
        }
        else {
            _i_19319 = binary_op_a(PLUS, _i_19319, 1);
        }
        DeRef(_0);
        goto L2; // [145] 81
L3: 
        ;
        DeRef(_i_19319);
    }

    /** 	return -1*/
    DeRefDS(_name_19301);
    DeRef(_tables_19302);
    DeRef(_nt_19303);
    DeRef(_t_header_19304);
    DeRef(_name_ptr_19305);
    return -1;
    ;
}


int _51db_select_table(int _name_19334)
{
    int _table_19335 = NOVALUE;
    int _nkeys_19336 = NOVALUE;
    int _index_19337 = NOVALUE;
    int _block_ptr_19338 = NOVALUE;
    int _block_size_19339 = NOVALUE;
    int _blocks_19340 = NOVALUE;
    int _k_19341 = NOVALUE;
    int _seek_1__tmp_at124_19360 = NOVALUE;
    int _seek_inlined_seek_at_124_19359 = NOVALUE;
    int _pos_inlined_seek_at_121_19358 = NOVALUE;
    int _seek_1__tmp_at182_19370 = NOVALUE;
    int _seek_inlined_seek_at_182_19369 = NOVALUE;
    int _seek_1__tmp_at209_19375 = NOVALUE;
    int _seek_inlined_seek_at_209_19374 = NOVALUE;
    int _11063 = NOVALUE;
    int _11062 = NOVALUE;
    int _11059 = NOVALUE;
    int _11054 = NOVALUE;
    int _11049 = NOVALUE;
    int _11045 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if equal(current_table_name, name) then*/
    if (_51current_table_name_18102 == _name_19334)
    _11045 = 1;
    else if (IS_ATOM_INT(_51current_table_name_18102) && IS_ATOM_INT(_name_19334))
    _11045 = 0;
    else
    _11045 = (compare(_51current_table_name_18102, _name_19334) == 0);
    if (_11045 == 0)
    {
        _11045 = NOVALUE;
        goto L1; // [11] 23
    }
    else{
        _11045 = NOVALUE;
    }

    /** 		return DB_OK*/
    DeRefDS(_name_19334);
    DeRef(_table_19335);
    DeRef(_nkeys_19336);
    DeRef(_index_19337);
    DeRef(_block_ptr_19338);
    DeRef(_block_size_19339);
    return 0;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_19334);
    _0 = _table_19335;
    _table_19335 = _51table_find(_name_19334);
    DeRef(_0);

    /** 	if table = -1 then*/
    if (binary_op_a(NOTEQ, _table_19335, -1)){
        goto L2; // [31] 44
    }

    /** 		return DB_OPEN_FAIL*/
    DeRefDS(_name_19334);
    DeRef(_table_19335);
    DeRef(_nkeys_19336);
    DeRef(_index_19337);
    DeRef(_block_ptr_19338);
    DeRef(_block_size_19339);
    return -1;
L2: 

    /** 	save_keys()*/
    _51save_keys();

    /** 	current_table_pos = table*/
    Ref(_table_19335);
    DeRef(_51current_table_pos_18101);
    _51current_table_pos_18101 = _table_19335;

    /** 	current_table_name = name*/
    RefDS(_name_19334);
    DeRef(_51current_table_name_18102);
    _51current_table_name_18102 = _name_19334;

    /** 	k = 0*/
    _k_19341 = 0;

    /** 	if caching_option = 1 then*/

    /** 		k = eu:find({current_db, current_table_pos}, cache_index)*/
    Ref(_51current_table_pos_18101);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _51current_table_pos_18101;
    _11049 = MAKE_SEQ(_1);
    _k_19341 = find_from(_11049, _51cache_index_18109, 1);
    DeRefDS(_11049);
    _11049 = NOVALUE;

    /** 		if k != 0 then*/
    if (_k_19341 == 0)
    goto L3; // [92] 107

    /** 			key_pointers = key_cache[k]*/
    DeRef(_51key_pointers_18107);
    _2 = (int)SEQ_PTR(_51key_cache_18108);
    _51key_pointers_18107 = (int)*(((s1_ptr)_2)->base + _k_19341);
    Ref(_51key_pointers_18107);
L3: 

    /** 	if k = 0 then*/
    if (_k_19341 != 0)
    goto L4; // [110] 273

    /** 		io:seek(current_db, table+4)*/
    if (IS_ATOM_INT(_table_19335)) {
        _11054 = _table_19335 + 4;
        if ((long)((unsigned long)_11054 + (unsigned long)HIGH_BITS) >= 0) 
        _11054 = NewDouble((double)_11054);
    }
    else {
        _11054 = NewDouble(DBL_PTR(_table_19335)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_121_19358);
    _pos_inlined_seek_at_121_19358 = _11054;
    _11054 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_121_19358);
    DeRef(_seek_1__tmp_at124_19360);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_121_19358;
    _seek_1__tmp_at124_19360 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_124_19359 = machine(19, _seek_1__tmp_at124_19360);
    DeRef(_pos_inlined_seek_at_121_19358);
    _pos_inlined_seek_at_121_19358 = NOVALUE;
    DeRef(_seek_1__tmp_at124_19360);
    _seek_1__tmp_at124_19360 = NOVALUE;

    /** 		nkeys = get4()*/
    _0 = _nkeys_19336;
    _nkeys_19336 = _51get4();
    DeRef(_0);

    /** 		blocks = get4()*/
    _blocks_19340 = _51get4();
    if (!IS_ATOM_INT(_blocks_19340)) {
        _1 = (long)(DBL_PTR(_blocks_19340)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_19340)) && (DBL_PTR(_blocks_19340)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_19340);
        _blocks_19340 = _1;
    }

    /** 		index = get4()*/
    _0 = _index_19337;
    _index_19337 = _51get4();
    DeRef(_0);

    /** 		key_pointers = repeat(0, nkeys)*/
    DeRef(_51key_pointers_18107);
    _51key_pointers_18107 = Repeat(0, _nkeys_19336);

    /** 		k = 1*/
    _k_19341 = 1;

    /** 		for b = 0 to blocks-1 do*/
    _11059 = _blocks_19340 - 1;
    if ((long)((unsigned long)_11059 +(unsigned long) HIGH_BITS) >= 0){
        _11059 = NewDouble((double)_11059);
    }
    {
        int _b_19366;
        _b_19366 = 0;
L5: 
        if (binary_op_a(GREATER, _b_19366, _11059)){
            goto L6; // [172] 272
        }

        /** 			io:seek(current_db, index)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_index_19337);
        DeRef(_seek_1__tmp_at182_19370);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _index_19337;
        _seek_1__tmp_at182_19370 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_182_19369 = machine(19, _seek_1__tmp_at182_19370);
        DeRef(_seek_1__tmp_at182_19370);
        _seek_1__tmp_at182_19370 = NOVALUE;

        /** 			block_size = get4()*/
        _0 = _block_size_19339;
        _block_size_19339 = _51get4();
        DeRef(_0);

        /** 			block_ptr = get4()*/
        _0 = _block_ptr_19338;
        _block_ptr_19338 = _51get4();
        DeRef(_0);

        /** 			io:seek(current_db, block_ptr)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_block_ptr_19338);
        DeRef(_seek_1__tmp_at209_19375);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _block_ptr_19338;
        _seek_1__tmp_at209_19375 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_209_19374 = machine(19, _seek_1__tmp_at209_19375);
        DeRef(_seek_1__tmp_at209_19375);
        _seek_1__tmp_at209_19375 = NOVALUE;

        /** 			for j = 1 to block_size do*/
        Ref(_block_size_19339);
        DeRef(_11062);
        _11062 = _block_size_19339;
        {
            int _j_19377;
            _j_19377 = 1;
L7: 
            if (binary_op_a(GREATER, _j_19377, _11062)){
                goto L8; // [228] 259
            }

            /** 				key_pointers[k] = get4()*/
            _11063 = _51get4();
            _2 = (int)SEQ_PTR(_51key_pointers_18107);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _51key_pointers_18107 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _k_19341);
            _1 = *(int *)_2;
            *(int *)_2 = _11063;
            if( _1 != _11063 ){
                DeRef(_1);
            }
            _11063 = NOVALUE;

            /** 				k += 1*/
            _k_19341 = _k_19341 + 1;

            /** 			end for*/
            _0 = _j_19377;
            if (IS_ATOM_INT(_j_19377)) {
                _j_19377 = _j_19377 + 1;
                if ((long)((unsigned long)_j_19377 +(unsigned long) HIGH_BITS) >= 0){
                    _j_19377 = NewDouble((double)_j_19377);
                }
            }
            else {
                _j_19377 = binary_op_a(PLUS, _j_19377, 1);
            }
            DeRef(_0);
            goto L7; // [254] 235
L8: 
            ;
            DeRef(_j_19377);
        }

        /** 			index += 8*/
        _0 = _index_19337;
        if (IS_ATOM_INT(_index_19337)) {
            _index_19337 = _index_19337 + 8;
            if ((long)((unsigned long)_index_19337 + (unsigned long)HIGH_BITS) >= 0) 
            _index_19337 = NewDouble((double)_index_19337);
        }
        else {
            _index_19337 = NewDouble(DBL_PTR(_index_19337)->dbl + (double)8);
        }
        DeRef(_0);

        /** 		end for*/
        _0 = _b_19366;
        if (IS_ATOM_INT(_b_19366)) {
            _b_19366 = _b_19366 + 1;
            if ((long)((unsigned long)_b_19366 +(unsigned long) HIGH_BITS) >= 0){
                _b_19366 = NewDouble((double)_b_19366);
            }
        }
        else {
            _b_19366 = binary_op_a(PLUS, _b_19366, 1);
        }
        DeRef(_0);
        goto L5; // [267] 179
L6: 
        ;
        DeRef(_b_19366);
    }
L4: 

    /** 	return DB_OK*/
    DeRefDS(_name_19334);
    DeRef(_table_19335);
    DeRef(_nkeys_19336);
    DeRef(_index_19337);
    DeRef(_block_ptr_19338);
    DeRef(_block_size_19339);
    DeRef(_11059);
    _11059 = NOVALUE;
    return 0;
    ;
}


int _51db_create_table(int _name_19386, int _init_records_19387)
{
    int _name_ptr_19388 = NOVALUE;
    int _nt_19389 = NOVALUE;
    int _tables_19390 = NOVALUE;
    int _newtables_19391 = NOVALUE;
    int _table_19392 = NOVALUE;
    int _records_ptr_19393 = NOVALUE;
    int _size_19394 = NOVALUE;
    int _newsize_19395 = NOVALUE;
    int _index_ptr_19396 = NOVALUE;
    int _remaining_19397 = NOVALUE;
    int _init_index_19398 = NOVALUE;
    int _seek_1__tmp_at72_19412 = NOVALUE;
    int _seek_inlined_seek_at_72_19411 = NOVALUE;
    int _seek_1__tmp_at101_19418 = NOVALUE;
    int _seek_inlined_seek_at_101_19417 = NOVALUE;
    int _pos_inlined_seek_at_98_19416 = NOVALUE;
    int _put4_1__tmp_at163_19431 = NOVALUE;
    int _seek_1__tmp_at200_19436 = NOVALUE;
    int _seek_inlined_seek_at_200_19435 = NOVALUE;
    int _pos_inlined_seek_at_197_19434 = NOVALUE;
    int _seek_1__tmp_at243_19444 = NOVALUE;
    int _seek_inlined_seek_at_243_19443 = NOVALUE;
    int _pos_inlined_seek_at_240_19442 = NOVALUE;
    int _s_inlined_putn_at_292_19452 = NOVALUE;
    int _seek_1__tmp_at320_19455 = NOVALUE;
    int _seek_inlined_seek_at_320_19454 = NOVALUE;
    int _put4_1__tmp_at335_19457 = NOVALUE;
    int _seek_1__tmp_at373_19461 = NOVALUE;
    int _seek_inlined_seek_at_373_19460 = NOVALUE;
    int _put4_1__tmp_at388_19463 = NOVALUE;
    int _s_inlined_putn_at_435_19469 = NOVALUE;
    int _put4_1__tmp_at466_19473 = NOVALUE;
    int _put4_1__tmp_at494_19475 = NOVALUE;
    int _s_inlined_putn_at_534_19480 = NOVALUE;
    int _s_inlined_putn_at_572_19486 = NOVALUE;
    int _seek_1__tmp_at614_19494 = NOVALUE;
    int _seek_inlined_seek_at_614_19493 = NOVALUE;
    int _pos_inlined_seek_at_611_19492 = NOVALUE;
    int _put4_1__tmp_at629_19496 = NOVALUE;
    int _put4_1__tmp_at657_19498 = NOVALUE;
    int _put4_1__tmp_at685_19500 = NOVALUE;
    int _put4_1__tmp_at713_19502 = NOVALUE;
    int _11112 = NOVALUE;
    int _11111 = NOVALUE;
    int _11110 = NOVALUE;
    int _11109 = NOVALUE;
    int _11108 = NOVALUE;
    int _11107 = NOVALUE;
    int _11105 = NOVALUE;
    int _11104 = NOVALUE;
    int _11103 = NOVALUE;
    int _11102 = NOVALUE;
    int _11101 = NOVALUE;
    int _11099 = NOVALUE;
    int _11098 = NOVALUE;
    int _11097 = NOVALUE;
    int _11095 = NOVALUE;
    int _11094 = NOVALUE;
    int _11093 = NOVALUE;
    int _11092 = NOVALUE;
    int _11091 = NOVALUE;
    int _11090 = NOVALUE;
    int _11089 = NOVALUE;
    int _11087 = NOVALUE;
    int _11086 = NOVALUE;
    int _11085 = NOVALUE;
    int _11082 = NOVALUE;
    int _11081 = NOVALUE;
    int _11079 = NOVALUE;
    int _11078 = NOVALUE;
    int _11076 = NOVALUE;
    int _11074 = NOVALUE;
    int _11071 = NOVALUE;
    int _11066 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not cstring(name) then*/
    RefDS(_name_19386);
    _11066 = _9cstring(_name_19386);
    if (IS_ATOM_INT(_11066)) {
        if (_11066 != 0){
            DeRef(_11066);
            _11066 = NOVALUE;
            goto L1; // [11] 23
        }
    }
    else {
        if (DBL_PTR(_11066)->dbl != 0.0){
            DeRef(_11066);
            _11066 = NOVALUE;
            goto L1; // [11] 23
        }
    }
    DeRef(_11066);
    _11066 = NOVALUE;

    /** 		return DB_BAD_NAME*/
    DeRefDS(_name_19386);
    DeRef(_name_ptr_19388);
    DeRef(_nt_19389);
    DeRef(_tables_19390);
    DeRef(_newtables_19391);
    DeRef(_table_19392);
    DeRef(_records_ptr_19393);
    DeRef(_size_19394);
    DeRef(_newsize_19395);
    DeRef(_index_ptr_19396);
    DeRef(_remaining_19397);
    return -4;
L1: 

    /** 	table = table_find(name)*/
    RefDS(_name_19386);
    _0 = _table_19392;
    _table_19392 = _51table_find(_name_19386);
    DeRef(_0);

    /** 	if table != -1 then*/
    if (binary_op_a(EQUALS, _table_19392, -1)){
        goto L2; // [31] 44
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRefDS(_name_19386);
    DeRef(_name_ptr_19388);
    DeRef(_nt_19389);
    DeRef(_tables_19390);
    DeRef(_newtables_19391);
    DeRef(_table_19392);
    DeRef(_records_ptr_19393);
    DeRef(_size_19394);
    DeRef(_newsize_19395);
    DeRef(_index_ptr_19396);
    DeRef(_remaining_19397);
    return -2;
L2: 

    /** 	if init_records < 1 then*/
    if (_init_records_19387 >= 1)
    goto L3; // [46] 56

    /** 		init_records = 1*/
    _init_records_19387 = 1;
L3: 

    /** 	init_index = math:min({init_records, MAX_INDEX})*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _init_records_19387;
    ((int *)_2)[2] = 10;
    _11071 = MAKE_SEQ(_1);
    _init_index_19398 = _18min(_11071);
    _11071 = NOVALUE;
    if (!IS_ATOM_INT(_init_index_19398)) {
        _1 = (long)(DBL_PTR(_init_index_19398)->dbl);
        if (UNIQUE(DBL_PTR(_init_index_19398)) && (DBL_PTR(_init_index_19398)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_init_index_19398);
        _init_index_19398 = _1;
    }

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at72_19412);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at72_19412 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_72_19411 = machine(19, _seek_1__tmp_at72_19412);
    DeRefi(_seek_1__tmp_at72_19412);
    _seek_1__tmp_at72_19412 = NOVALUE;

    /** 	tables = get4()*/
    _0 = _tables_19390;
    _tables_19390 = _51get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables-4)*/
    if (IS_ATOM_INT(_tables_19390)) {
        _11074 = _tables_19390 - 4;
        if ((long)((unsigned long)_11074 +(unsigned long) HIGH_BITS) >= 0){
            _11074 = NewDouble((double)_11074);
        }
    }
    else {
        _11074 = NewDouble(DBL_PTR(_tables_19390)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_98_19416);
    _pos_inlined_seek_at_98_19416 = _11074;
    _11074 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_98_19416);
    DeRef(_seek_1__tmp_at101_19418);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_98_19416;
    _seek_1__tmp_at101_19418 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_101_19417 = machine(19, _seek_1__tmp_at101_19418);
    DeRef(_pos_inlined_seek_at_98_19416);
    _pos_inlined_seek_at_98_19416 = NOVALUE;
    DeRef(_seek_1__tmp_at101_19418);
    _seek_1__tmp_at101_19418 = NOVALUE;

    /** 	size = get4()*/
    _0 = _size_19394;
    _size_19394 = _51get4();
    DeRef(_0);

    /** 	nt = get4()+1*/
    _11076 = _51get4();
    DeRef(_nt_19389);
    if (IS_ATOM_INT(_11076)) {
        _nt_19389 = _11076 + 1;
        if (_nt_19389 > MAXINT){
            _nt_19389 = NewDouble((double)_nt_19389);
        }
    }
    else
    _nt_19389 = binary_op(PLUS, 1, _11076);
    DeRef(_11076);
    _11076 = NOVALUE;

    /** 	if nt*SIZEOF_TABLE_HEADER + 8 > size then*/
    if (IS_ATOM_INT(_nt_19389)) {
        if (_nt_19389 == (short)_nt_19389)
        _11078 = _nt_19389 * 16;
        else
        _11078 = NewDouble(_nt_19389 * (double)16);
    }
    else {
        _11078 = NewDouble(DBL_PTR(_nt_19389)->dbl * (double)16);
    }
    if (IS_ATOM_INT(_11078)) {
        _11079 = _11078 + 8;
        if ((long)((unsigned long)_11079 + (unsigned long)HIGH_BITS) >= 0) 
        _11079 = NewDouble((double)_11079);
    }
    else {
        _11079 = NewDouble(DBL_PTR(_11078)->dbl + (double)8);
    }
    DeRef(_11078);
    _11078 = NOVALUE;
    if (binary_op_a(LESSEQ, _11079, _size_19394)){
        DeRef(_11079);
        _11079 = NOVALUE;
        goto L4; // [138] 369
    }
    DeRef(_11079);
    _11079 = NOVALUE;

    /** 		newsize = floor(size + size / 2)*/
    if (IS_ATOM_INT(_size_19394)) {
        if (_size_19394 & 1) {
            _11081 = NewDouble((_size_19394 >> 1) + 0.5);
        }
        else
        _11081 = _size_19394 >> 1;
    }
    else {
        _11081 = binary_op(DIVIDE, _size_19394, 2);
    }
    if (IS_ATOM_INT(_size_19394) && IS_ATOM_INT(_11081)) {
        _11082 = _size_19394 + _11081;
        if ((long)((unsigned long)_11082 + (unsigned long)HIGH_BITS) >= 0) 
        _11082 = NewDouble((double)_11082);
    }
    else {
        if (IS_ATOM_INT(_size_19394)) {
            _11082 = NewDouble((double)_size_19394 + DBL_PTR(_11081)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11081)) {
                _11082 = NewDouble(DBL_PTR(_size_19394)->dbl + (double)_11081);
            }
            else
            _11082 = NewDouble(DBL_PTR(_size_19394)->dbl + DBL_PTR(_11081)->dbl);
        }
    }
    DeRef(_11081);
    _11081 = NOVALUE;
    DeRef(_newsize_19395);
    if (IS_ATOM_INT(_11082))
    _newsize_19395 = e_floor(_11082);
    else
    _newsize_19395 = unary_op(FLOOR, _11082);
    DeRef(_11082);
    _11082 = NOVALUE;

    /** 		newtables = db_allocate(newsize)*/
    Ref(_newsize_19395);
    _0 = _newtables_19391;
    _newtables_19391 = _51db_allocate(_newsize_19395);
    DeRef(_0);

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_nt_19389)) {
        *poke4_addr = (unsigned long)_nt_19389;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_19389)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at163_19431);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at163_19431 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at163_19431); // DJP 

    /** end procedure*/
    goto L5; // [184] 187
L5: 
    DeRefi(_put4_1__tmp_at163_19431);
    _put4_1__tmp_at163_19431 = NOVALUE;

    /** 		io:seek(current_db, tables+4)*/
    if (IS_ATOM_INT(_tables_19390)) {
        _11085 = _tables_19390 + 4;
        if ((long)((unsigned long)_11085 + (unsigned long)HIGH_BITS) >= 0) 
        _11085 = NewDouble((double)_11085);
    }
    else {
        _11085 = NewDouble(DBL_PTR(_tables_19390)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_197_19434);
    _pos_inlined_seek_at_197_19434 = _11085;
    _11085 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_197_19434);
    DeRef(_seek_1__tmp_at200_19436);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_197_19434;
    _seek_1__tmp_at200_19436 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_200_19435 = machine(19, _seek_1__tmp_at200_19436);
    DeRef(_pos_inlined_seek_at_197_19434);
    _pos_inlined_seek_at_197_19434 = NOVALUE;
    DeRef(_seek_1__tmp_at200_19436);
    _seek_1__tmp_at200_19436 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, (nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_nt_19389)) {
        _11086 = _nt_19389 - 1;
        if ((long)((unsigned long)_11086 +(unsigned long) HIGH_BITS) >= 0){
            _11086 = NewDouble((double)_11086);
        }
    }
    else {
        _11086 = NewDouble(DBL_PTR(_nt_19389)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_11086)) {
        if (_11086 == (short)_11086)
        _11087 = _11086 * 16;
        else
        _11087 = NewDouble(_11086 * (double)16);
    }
    else {
        _11087 = NewDouble(DBL_PTR(_11086)->dbl * (double)16);
    }
    DeRef(_11086);
    _11086 = NOVALUE;
    _0 = _remaining_19397;
    _remaining_19397 = _16get_bytes(_51current_db_18100, _11087);
    DeRef(_0);
    _11087 = NOVALUE;

    /** 		io:seek(current_db, newtables+4)*/
    if (IS_ATOM_INT(_newtables_19391)) {
        _11089 = _newtables_19391 + 4;
        if ((long)((unsigned long)_11089 + (unsigned long)HIGH_BITS) >= 0) 
        _11089 = NewDouble((double)_11089);
    }
    else {
        _11089 = NewDouble(DBL_PTR(_newtables_19391)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_240_19442);
    _pos_inlined_seek_at_240_19442 = _11089;
    _11089 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_240_19442);
    DeRef(_seek_1__tmp_at243_19444);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_240_19442;
    _seek_1__tmp_at243_19444 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_243_19443 = machine(19, _seek_1__tmp_at243_19444);
    DeRef(_pos_inlined_seek_at_240_19442);
    _pos_inlined_seek_at_240_19442 = NOVALUE;
    DeRef(_seek_1__tmp_at243_19444);
    _seek_1__tmp_at243_19444 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _remaining_19397); // DJP 

    /** end procedure*/
    goto L6; // [267] 270
L6: 

    /** 		putn(repeat(0, newsize - 4 - (nt-1)*SIZEOF_TABLE_HEADER))*/
    if (IS_ATOM_INT(_newsize_19395)) {
        _11090 = _newsize_19395 - 4;
        if ((long)((unsigned long)_11090 +(unsigned long) HIGH_BITS) >= 0){
            _11090 = NewDouble((double)_11090);
        }
    }
    else {
        _11090 = NewDouble(DBL_PTR(_newsize_19395)->dbl - (double)4);
    }
    if (IS_ATOM_INT(_nt_19389)) {
        _11091 = _nt_19389 - 1;
        if ((long)((unsigned long)_11091 +(unsigned long) HIGH_BITS) >= 0){
            _11091 = NewDouble((double)_11091);
        }
    }
    else {
        _11091 = NewDouble(DBL_PTR(_nt_19389)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_11091)) {
        if (_11091 == (short)_11091)
        _11092 = _11091 * 16;
        else
        _11092 = NewDouble(_11091 * (double)16);
    }
    else {
        _11092 = NewDouble(DBL_PTR(_11091)->dbl * (double)16);
    }
    DeRef(_11091);
    _11091 = NOVALUE;
    if (IS_ATOM_INT(_11090) && IS_ATOM_INT(_11092)) {
        _11093 = _11090 - _11092;
    }
    else {
        if (IS_ATOM_INT(_11090)) {
            _11093 = NewDouble((double)_11090 - DBL_PTR(_11092)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11092)) {
                _11093 = NewDouble(DBL_PTR(_11090)->dbl - (double)_11092);
            }
            else
            _11093 = NewDouble(DBL_PTR(_11090)->dbl - DBL_PTR(_11092)->dbl);
        }
    }
    DeRef(_11090);
    _11090 = NOVALUE;
    DeRef(_11092);
    _11092 = NOVALUE;
    _11094 = Repeat(0, _11093);
    DeRef(_11093);
    _11093 = NOVALUE;
    DeRefi(_s_inlined_putn_at_292_19452);
    _s_inlined_putn_at_292_19452 = _11094;
    _11094 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_292_19452); // DJP 

    /** end procedure*/
    goto L7; // [306] 309
L7: 
    DeRefi(_s_inlined_putn_at_292_19452);
    _s_inlined_putn_at_292_19452 = NOVALUE;

    /** 		db_free(tables)*/
    Ref(_tables_19390);
    _51db_free(_tables_19390);

    /** 		io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at320_19455);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at320_19455 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_320_19454 = machine(19, _seek_1__tmp_at320_19455);
    DeRefi(_seek_1__tmp_at320_19455);
    _seek_1__tmp_at320_19455 = NOVALUE;

    /** 		put4(newtables)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_newtables_19391)) {
        *poke4_addr = (unsigned long)_newtables_19391;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_newtables_19391)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at335_19457);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at335_19457 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at335_19457); // DJP 

    /** end procedure*/
    goto L8; // [356] 359
L8: 
    DeRefi(_put4_1__tmp_at335_19457);
    _put4_1__tmp_at335_19457 = NOVALUE;

    /** 		tables = newtables*/
    Ref(_newtables_19391);
    DeRef(_tables_19390);
    _tables_19390 = _newtables_19391;
    goto L9; // [366] 415
L4: 

    /** 		io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_19390);
    DeRef(_seek_1__tmp_at373_19461);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _tables_19390;
    _seek_1__tmp_at373_19461 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_373_19460 = machine(19, _seek_1__tmp_at373_19461);
    DeRef(_seek_1__tmp_at373_19461);
    _seek_1__tmp_at373_19461 = NOVALUE;

    /** 		put4(nt)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_nt_19389)) {
        *poke4_addr = (unsigned long)_nt_19389;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nt_19389)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at388_19463);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at388_19463 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at388_19463); // DJP 

    /** end procedure*/
    goto LA; // [409] 412
LA: 
    DeRefi(_put4_1__tmp_at388_19463);
    _put4_1__tmp_at388_19463 = NOVALUE;
L9: 

    /** 	records_ptr = db_allocate(init_records * 4)*/
    if (_init_records_19387 == (short)_init_records_19387)
    _11095 = _init_records_19387 * 4;
    else
    _11095 = NewDouble(_init_records_19387 * (double)4);
    _0 = _records_ptr_19393;
    _records_ptr_19393 = _51db_allocate(_11095);
    DeRef(_0);
    _11095 = NOVALUE;

    /** 	putn(repeat(0, init_records * 4))*/
    _11097 = _init_records_19387 * 4;
    _11098 = Repeat(0, _11097);
    _11097 = NOVALUE;
    DeRefi(_s_inlined_putn_at_435_19469);
    _s_inlined_putn_at_435_19469 = _11098;
    _11098 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_435_19469); // DJP 

    /** end procedure*/
    goto LB; // [449] 452
LB: 
    DeRefi(_s_inlined_putn_at_435_19469);
    _s_inlined_putn_at_435_19469 = NOVALUE;

    /** 	index_ptr = db_allocate(init_index * 8)*/
    if (_init_index_19398 == (short)_init_index_19398)
    _11099 = _init_index_19398 * 8;
    else
    _11099 = NewDouble(_init_index_19398 * (double)8);
    _0 = _index_ptr_19396;
    _index_ptr_19396 = _51db_allocate(_11099);
    DeRef(_0);
    _11099 = NOVALUE;

    /** 	put4(0)  -- 0 records*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at466_19473);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at466_19473 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at466_19473); // DJP 

    /** end procedure*/
    goto LC; // [487] 490
LC: 
    DeRefi(_put4_1__tmp_at466_19473);
    _put4_1__tmp_at466_19473 = NOVALUE;

    /** 	put4(records_ptr) -- point to 1st block*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_records_ptr_19393)) {
        *poke4_addr = (unsigned long)_records_ptr_19393;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_records_ptr_19393)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at494_19475);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at494_19475 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at494_19475); // DJP 

    /** end procedure*/
    goto LD; // [515] 518
LD: 
    DeRefi(_put4_1__tmp_at494_19475);
    _put4_1__tmp_at494_19475 = NOVALUE;

    /** 	putn(repeat(0, (init_index-1) * 8))*/
    _11101 = _init_index_19398 - 1;
    if ((long)((unsigned long)_11101 +(unsigned long) HIGH_BITS) >= 0){
        _11101 = NewDouble((double)_11101);
    }
    if (IS_ATOM_INT(_11101)) {
        _11102 = _11101 * 8;
    }
    else {
        _11102 = NewDouble(DBL_PTR(_11101)->dbl * (double)8);
    }
    DeRef(_11101);
    _11101 = NOVALUE;
    _11103 = Repeat(0, _11102);
    DeRef(_11102);
    _11102 = NOVALUE;
    DeRefi(_s_inlined_putn_at_534_19480);
    _s_inlined_putn_at_534_19480 = _11103;
    _11103 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_534_19480); // DJP 

    /** end procedure*/
    goto LE; // [548] 551
LE: 
    DeRefi(_s_inlined_putn_at_534_19480);
    _s_inlined_putn_at_534_19480 = NOVALUE;

    /** 	name_ptr = db_allocate(length(name)+1)*/
    if (IS_SEQUENCE(_name_19386)){
            _11104 = SEQ_PTR(_name_19386)->length;
    }
    else {
        _11104 = 1;
    }
    _11105 = _11104 + 1;
    _11104 = NOVALUE;
    _0 = _name_ptr_19388;
    _name_ptr_19388 = _51db_allocate(_11105);
    DeRef(_0);
    _11105 = NOVALUE;

    /** 	putn(name & 0)*/
    Append(&_11107, _name_19386, 0);
    DeRef(_s_inlined_putn_at_572_19486);
    _s_inlined_putn_at_572_19486 = _11107;
    _11107 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_572_19486); // DJP 

    /** end procedure*/
    goto LF; // [586] 589
LF: 
    DeRef(_s_inlined_putn_at_572_19486);
    _s_inlined_putn_at_572_19486 = NOVALUE;

    /** 	io:seek(current_db, tables+4+(nt-1)*SIZEOF_TABLE_HEADER)*/
    if (IS_ATOM_INT(_tables_19390)) {
        _11108 = _tables_19390 + 4;
        if ((long)((unsigned long)_11108 + (unsigned long)HIGH_BITS) >= 0) 
        _11108 = NewDouble((double)_11108);
    }
    else {
        _11108 = NewDouble(DBL_PTR(_tables_19390)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_nt_19389)) {
        _11109 = _nt_19389 - 1;
        if ((long)((unsigned long)_11109 +(unsigned long) HIGH_BITS) >= 0){
            _11109 = NewDouble((double)_11109);
        }
    }
    else {
        _11109 = NewDouble(DBL_PTR(_nt_19389)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_11109)) {
        if (_11109 == (short)_11109)
        _11110 = _11109 * 16;
        else
        _11110 = NewDouble(_11109 * (double)16);
    }
    else {
        _11110 = NewDouble(DBL_PTR(_11109)->dbl * (double)16);
    }
    DeRef(_11109);
    _11109 = NOVALUE;
    if (IS_ATOM_INT(_11108) && IS_ATOM_INT(_11110)) {
        _11111 = _11108 + _11110;
        if ((long)((unsigned long)_11111 + (unsigned long)HIGH_BITS) >= 0) 
        _11111 = NewDouble((double)_11111);
    }
    else {
        if (IS_ATOM_INT(_11108)) {
            _11111 = NewDouble((double)_11108 + DBL_PTR(_11110)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11110)) {
                _11111 = NewDouble(DBL_PTR(_11108)->dbl + (double)_11110);
            }
            else
            _11111 = NewDouble(DBL_PTR(_11108)->dbl + DBL_PTR(_11110)->dbl);
        }
    }
    DeRef(_11108);
    _11108 = NOVALUE;
    DeRef(_11110);
    _11110 = NOVALUE;
    DeRef(_pos_inlined_seek_at_611_19492);
    _pos_inlined_seek_at_611_19492 = _11111;
    _11111 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_611_19492);
    DeRef(_seek_1__tmp_at614_19494);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_611_19492;
    _seek_1__tmp_at614_19494 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_614_19493 = machine(19, _seek_1__tmp_at614_19494);
    DeRef(_pos_inlined_seek_at_611_19492);
    _pos_inlined_seek_at_611_19492 = NOVALUE;
    DeRef(_seek_1__tmp_at614_19494);
    _seek_1__tmp_at614_19494 = NOVALUE;

    /** 	put4(name_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_name_ptr_19388)) {
        *poke4_addr = (unsigned long)_name_ptr_19388;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_name_ptr_19388)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at629_19496);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at629_19496 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at629_19496); // DJP 

    /** end procedure*/
    goto L10; // [650] 653
L10: 
    DeRefi(_put4_1__tmp_at629_19496);
    _put4_1__tmp_at629_19496 = NOVALUE;

    /** 	put4(0)  -- start with 0 records total*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at657_19498);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at657_19498 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at657_19498); // DJP 

    /** end procedure*/
    goto L11; // [678] 681
L11: 
    DeRefi(_put4_1__tmp_at657_19498);
    _put4_1__tmp_at657_19498 = NOVALUE;

    /** 	put4(1)  -- start with 1 block of records in index*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)1;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at685_19500);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at685_19500 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at685_19500); // DJP 

    /** end procedure*/
    goto L12; // [706] 709
L12: 
    DeRefi(_put4_1__tmp_at685_19500);
    _put4_1__tmp_at685_19500 = NOVALUE;

    /** 	put4(index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_index_ptr_19396)) {
        *poke4_addr = (unsigned long)_index_ptr_19396;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_index_ptr_19396)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at713_19502);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at713_19502 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at713_19502); // DJP 

    /** end procedure*/
    goto L13; // [734] 737
L13: 
    DeRefi(_put4_1__tmp_at713_19502);
    _put4_1__tmp_at713_19502 = NOVALUE;

    /** 	if db_select_table(name) then*/
    RefDS(_name_19386);
    _11112 = _51db_select_table(_name_19386);
    if (_11112 == 0) {
        DeRef(_11112);
        _11112 = NOVALUE;
        goto L14; // [745] 749
    }
    else {
        if (!IS_ATOM_INT(_11112) && DBL_PTR(_11112)->dbl == 0.0){
            DeRef(_11112);
            _11112 = NOVALUE;
            goto L14; // [745] 749
        }
        DeRef(_11112);
        _11112 = NOVALUE;
    }
    DeRef(_11112);
    _11112 = NOVALUE;
L14: 

    /** 	return DB_OK*/
    DeRefDS(_name_19386);
    DeRef(_name_ptr_19388);
    DeRef(_nt_19389);
    DeRef(_tables_19390);
    DeRef(_newtables_19391);
    DeRef(_table_19392);
    DeRef(_records_ptr_19393);
    DeRef(_size_19394);
    DeRef(_newsize_19395);
    DeRef(_index_ptr_19396);
    DeRef(_remaining_19397);
    return 0;
    ;
}


int _51db_table_list()
{
    int _seek_1__tmp_at120_19761 = NOVALUE;
    int _seek_inlined_seek_at_120_19760 = NOVALUE;
    int _seek_1__tmp_at98_19757 = NOVALUE;
    int _seek_inlined_seek_at_98_19756 = NOVALUE;
    int _pos_inlined_seek_at_95_19755 = NOVALUE;
    int _seek_1__tmp_at42_19745 = NOVALUE;
    int _seek_inlined_seek_at_42_19744 = NOVALUE;
    int _seek_1__tmp_at4_19738 = NOVALUE;
    int _seek_inlined_seek_at_4_19737 = NOVALUE;
    int _table_names_19732 = NOVALUE;
    int _tables_19733 = NOVALUE;
    int _nt_19734 = NOVALUE;
    int _name_19735 = NOVALUE;
    int _11211 = NOVALUE;
    int _11210 = NOVALUE;
    int _11208 = NOVALUE;
    int _11207 = NOVALUE;
    int _11206 = NOVALUE;
    int _11205 = NOVALUE;
    int _11200 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, TABLE_HEADERS)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    DeRefi(_seek_1__tmp_at4_19738);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = 3;
    _seek_1__tmp_at4_19738 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_4_19737 = machine(19, _seek_1__tmp_at4_19738);
    DeRefi(_seek_1__tmp_at4_19738);
    _seek_1__tmp_at4_19738 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return {} end if*/
    if (IS_SEQUENCE(_51vLastErrors_18124)){
            _11200 = SEQ_PTR(_51vLastErrors_18124)->length;
    }
    else {
        _11200 = 1;
    }
    if (_11200 <= 0)
    goto L1; // [25] 34
    RefDS(_5);
    DeRef(_table_names_19732);
    DeRef(_tables_19733);
    DeRef(_nt_19734);
    DeRef(_name_19735);
    return _5;
L1: 

    /** 	tables = get4()*/
    _0 = _tables_19733;
    _tables_19733 = _51get4();
    DeRef(_0);

    /** 	io:seek(current_db, tables)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_tables_19733);
    DeRef(_seek_1__tmp_at42_19745);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _tables_19733;
    _seek_1__tmp_at42_19745 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_42_19744 = machine(19, _seek_1__tmp_at42_19745);
    DeRef(_seek_1__tmp_at42_19745);
    _seek_1__tmp_at42_19745 = NOVALUE;

    /** 	nt = get4()*/
    _0 = _nt_19734;
    _nt_19734 = _51get4();
    DeRef(_0);

    /** 	table_names = repeat(0, nt)*/
    DeRef(_table_names_19732);
    _table_names_19732 = Repeat(0, _nt_19734);

    /** 	for i = 0 to nt-1 do*/
    if (IS_ATOM_INT(_nt_19734)) {
        _11205 = _nt_19734 - 1;
        if ((long)((unsigned long)_11205 +(unsigned long) HIGH_BITS) >= 0){
            _11205 = NewDouble((double)_11205);
        }
    }
    else {
        _11205 = NewDouble(DBL_PTR(_nt_19734)->dbl - (double)1);
    }
    {
        int _i_19749;
        _i_19749 = 0;
L2: 
        if (binary_op_a(GREATER, _i_19749, _11205)){
            goto L3; // [73] 154
        }

        /** 		io:seek(current_db, tables + 4 + i*SIZEOF_TABLE_HEADER)*/
        if (IS_ATOM_INT(_tables_19733)) {
            _11206 = _tables_19733 + 4;
            if ((long)((unsigned long)_11206 + (unsigned long)HIGH_BITS) >= 0) 
            _11206 = NewDouble((double)_11206);
        }
        else {
            _11206 = NewDouble(DBL_PTR(_tables_19733)->dbl + (double)4);
        }
        if (IS_ATOM_INT(_i_19749)) {
            if (_i_19749 == (short)_i_19749)
            _11207 = _i_19749 * 16;
            else
            _11207 = NewDouble(_i_19749 * (double)16);
        }
        else {
            _11207 = NewDouble(DBL_PTR(_i_19749)->dbl * (double)16);
        }
        if (IS_ATOM_INT(_11206) && IS_ATOM_INT(_11207)) {
            _11208 = _11206 + _11207;
            if ((long)((unsigned long)_11208 + (unsigned long)HIGH_BITS) >= 0) 
            _11208 = NewDouble((double)_11208);
        }
        else {
            if (IS_ATOM_INT(_11206)) {
                _11208 = NewDouble((double)_11206 + DBL_PTR(_11207)->dbl);
            }
            else {
                if (IS_ATOM_INT(_11207)) {
                    _11208 = NewDouble(DBL_PTR(_11206)->dbl + (double)_11207);
                }
                else
                _11208 = NewDouble(DBL_PTR(_11206)->dbl + DBL_PTR(_11207)->dbl);
            }
        }
        DeRef(_11206);
        _11206 = NOVALUE;
        DeRef(_11207);
        _11207 = NOVALUE;
        DeRef(_pos_inlined_seek_at_95_19755);
        _pos_inlined_seek_at_95_19755 = _11208;
        _11208 = NOVALUE;

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_pos_inlined_seek_at_95_19755);
        DeRef(_seek_1__tmp_at98_19757);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _pos_inlined_seek_at_95_19755;
        _seek_1__tmp_at98_19757 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_98_19756 = machine(19, _seek_1__tmp_at98_19757);
        DeRef(_pos_inlined_seek_at_95_19755);
        _pos_inlined_seek_at_95_19755 = NOVALUE;
        DeRef(_seek_1__tmp_at98_19757);
        _seek_1__tmp_at98_19757 = NOVALUE;

        /** 		name = get4()*/
        _0 = _name_19735;
        _name_19735 = _51get4();
        DeRef(_0);

        /** 		io:seek(current_db, name)*/

        /** 	return machine_func(M_SEEK, {fn, pos})*/
        Ref(_name_19735);
        DeRef(_seek_1__tmp_at120_19761);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _51current_db_18100;
        ((int *)_2)[2] = _name_19735;
        _seek_1__tmp_at120_19761 = MAKE_SEQ(_1);
        _seek_inlined_seek_at_120_19760 = machine(19, _seek_1__tmp_at120_19761);
        DeRef(_seek_1__tmp_at120_19761);
        _seek_1__tmp_at120_19761 = NOVALUE;

        /** 		table_names[i+1] = get_string()*/
        if (IS_ATOM_INT(_i_19749)) {
            _11210 = _i_19749 + 1;
            if (_11210 > MAXINT){
                _11210 = NewDouble((double)_11210);
            }
        }
        else
        _11210 = binary_op(PLUS, 1, _i_19749);
        _11211 = _51get_string();
        _2 = (int)SEQ_PTR(_table_names_19732);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _table_names_19732 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_11210))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_11210)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _11210);
        _1 = *(int *)_2;
        *(int *)_2 = _11211;
        if( _1 != _11211 ){
            DeRef(_1);
        }
        _11211 = NOVALUE;

        /** 	end for*/
        _0 = _i_19749;
        if (IS_ATOM_INT(_i_19749)) {
            _i_19749 = _i_19749 + 1;
            if ((long)((unsigned long)_i_19749 +(unsigned long) HIGH_BITS) >= 0){
                _i_19749 = NewDouble((double)_i_19749);
            }
        }
        else {
            _i_19749 = binary_op_a(PLUS, _i_19749, 1);
        }
        DeRef(_0);
        goto L2; // [149] 80
L3: 
        ;
        DeRef(_i_19749);
    }

    /** 	return table_names*/
    DeRef(_tables_19733);
    DeRef(_nt_19734);
    DeRef(_name_19735);
    DeRef(_11205);
    _11205 = NOVALUE;
    DeRef(_11210);
    _11210 = NOVALUE;
    return _table_names_19732;
    ;
}


int _51key_value(int _ptr_19766)
{
    int _seek_1__tmp_at11_19771 = NOVALUE;
    int _seek_inlined_seek_at_11_19770 = NOVALUE;
    int _pos_inlined_seek_at_8_19769 = NOVALUE;
    int _11213 = NOVALUE;
    int _11212 = NOVALUE;
    int _0, _1, _2;
    

    /** 	io:seek(current_db, ptr+4) -- skip ptr to data*/
    if (IS_ATOM_INT(_ptr_19766)) {
        _11212 = _ptr_19766 + 4;
        if ((long)((unsigned long)_11212 + (unsigned long)HIGH_BITS) >= 0) 
        _11212 = NewDouble((double)_11212);
    }
    else {
        _11212 = NewDouble(DBL_PTR(_ptr_19766)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_8_19769);
    _pos_inlined_seek_at_8_19769 = _11212;
    _11212 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_8_19769);
    DeRef(_seek_1__tmp_at11_19771);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_8_19769;
    _seek_1__tmp_at11_19771 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_11_19770 = machine(19, _seek_1__tmp_at11_19771);
    DeRef(_pos_inlined_seek_at_8_19769);
    _pos_inlined_seek_at_8_19769 = NOVALUE;
    DeRef(_seek_1__tmp_at11_19771);
    _seek_1__tmp_at11_19771 = NOVALUE;

    /** 	return decompress(0)*/
    _11213 = _51decompress(0);
    DeRef(_ptr_19766);
    return _11213;
    ;
}


int _51db_find_key(int _key_19775, int _table_name_19776)
{
    int _lo_19777 = NOVALUE;
    int _hi_19778 = NOVALUE;
    int _mid_19779 = NOVALUE;
    int _c_19780 = NOVALUE;
    int _11237 = NOVALUE;
    int _11229 = NOVALUE;
    int _11228 = NOVALUE;
    int _11226 = NOVALUE;
    int _11223 = NOVALUE;
    int _11220 = NOVALUE;
    int _11216 = NOVALUE;
    int _11214 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_19776 == _51current_table_name_18102)
    _11214 = 1;
    else if (IS_ATOM_INT(_table_name_19776) && IS_ATOM_INT(_51current_table_name_18102))
    _11214 = 0;
    else
    _11214 = (compare(_table_name_19776, _51current_table_name_18102) == 0);
    if (_11214 != 0)
    goto L1; // [9] 46
    _11214 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_19776);
    _11216 = _51db_select_table(_table_name_19776);
    if (binary_op_a(EQUALS, _11216, 0)){
        DeRef(_11216);
        _11216 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_11216);
    _11216 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_find_key", {key, table_name})*/
    RefDS(_table_name_19776);
    Ref(_key_19775);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_19775;
    ((int *)_2)[2] = _table_name_19776;
    _11220 = MAKE_SEQ(_1);
    RefDS(_11218);
    RefDS(_11219);
    _51fatal(903, _11218, _11219, _11220);
    _11220 = NOVALUE;

    /** 			return 0*/
    DeRef(_key_19775);
    DeRefDS(_table_name_19776);
    return 0;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _51current_table_pos_18101, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_find_key", {key, table_name})*/
    Ref(_table_name_19776);
    Ref(_key_19775);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_19775;
    ((int *)_2)[2] = _table_name_19776;
    _11223 = MAKE_SEQ(_1);
    RefDS(_11222);
    RefDS(_11219);
    _51fatal(903, _11222, _11219, _11223);
    _11223 = NOVALUE;

    /** 		return 0*/
    DeRef(_key_19775);
    DeRef(_table_name_19776);
    return 0;
L3: 

    /** 	lo = 1*/
    _lo_19777 = 1;

    /** 	hi = length(key_pointers)*/
    if (IS_SEQUENCE(_51key_pointers_18107)){
            _hi_19778 = SEQ_PTR(_51key_pointers_18107)->length;
    }
    else {
        _hi_19778 = 1;
    }

    /** 	mid = 1*/
    _mid_19779 = 1;

    /** 	c = 0*/
    _c_19780 = 0;

    /** 	while lo <= hi do*/
L4: 
    if (_lo_19777 > _hi_19778)
    goto L5; // [102] 176

    /** 		mid = floor((lo + hi) / 2)*/
    _11226 = _lo_19777 + _hi_19778;
    if ((long)((unsigned long)_11226 + (unsigned long)HIGH_BITS) >= 0) 
    _11226 = NewDouble((double)_11226);
    if (IS_ATOM_INT(_11226)) {
        _mid_19779 = _11226 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _11226, 2);
        _mid_19779 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    DeRef(_11226);
    _11226 = NOVALUE;
    if (!IS_ATOM_INT(_mid_19779)) {
        _1 = (long)(DBL_PTR(_mid_19779)->dbl);
        if (UNIQUE(DBL_PTR(_mid_19779)) && (DBL_PTR(_mid_19779)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mid_19779);
        _mid_19779 = _1;
    }

    /** 		c = eu:compare(key, key_value(key_pointers[mid]))*/
    _2 = (int)SEQ_PTR(_51key_pointers_18107);
    _11228 = (int)*(((s1_ptr)_2)->base + _mid_19779);
    Ref(_11228);
    _11229 = _51key_value(_11228);
    _11228 = NOVALUE;
    if (IS_ATOM_INT(_key_19775) && IS_ATOM_INT(_11229)){
        _c_19780 = (_key_19775 < _11229) ? -1 : (_key_19775 > _11229);
    }
    else{
        _c_19780 = compare(_key_19775, _11229);
    }
    DeRef(_11229);
    _11229 = NOVALUE;

    /** 		if c < 0 then*/
    if (_c_19780 >= 0)
    goto L6; // [136] 149

    /** 			hi = mid - 1*/
    _hi_19778 = _mid_19779 - 1;
    goto L4; // [146] 102
L6: 

    /** 		elsif c > 0 then*/
    if (_c_19780 <= 0)
    goto L7; // [151] 164

    /** 			lo = mid + 1*/
    _lo_19777 = _mid_19779 + 1;
    goto L4; // [161] 102
L7: 

    /** 			return mid*/
    DeRef(_key_19775);
    DeRef(_table_name_19776);
    return _mid_19779;

    /** 	end while*/
    goto L4; // [173] 102
L5: 

    /** 	if c > 0 then*/
    if (_c_19780 <= 0)
    goto L8; // [178] 189

    /** 		mid += 1*/
    _mid_19779 = _mid_19779 + 1;
L8: 

    /** 	return -mid*/
    if ((unsigned long)_mid_19779 == 0xC0000000)
    _11237 = (int)NewDouble((double)-0xC0000000);
    else
    _11237 = - _mid_19779;
    DeRef(_key_19775);
    DeRef(_table_name_19776);
    return _11237;
    ;
}


int _51db_insert(int _key_19815, int _data_19816, int _table_name_19817)
{
    int _key_string_19818 = NOVALUE;
    int _data_string_19819 = NOVALUE;
    int _last_part_19820 = NOVALUE;
    int _remaining_19821 = NOVALUE;
    int _key_ptr_19822 = NOVALUE;
    int _data_ptr_19823 = NOVALUE;
    int _records_ptr_19824 = NOVALUE;
    int _nrecs_19825 = NOVALUE;
    int _current_block_19826 = NOVALUE;
    int _size_19827 = NOVALUE;
    int _new_size_19828 = NOVALUE;
    int _key_location_19829 = NOVALUE;
    int _new_block_19830 = NOVALUE;
    int _index_ptr_19831 = NOVALUE;
    int _new_index_ptr_19832 = NOVALUE;
    int _total_recs_19833 = NOVALUE;
    int _r_19834 = NOVALUE;
    int _blocks_19835 = NOVALUE;
    int _new_recs_19836 = NOVALUE;
    int _n_19837 = NOVALUE;
    int _put4_1__tmp_at81_19851 = NOVALUE;
    int _seek_1__tmp_at134_19857 = NOVALUE;
    int _seek_inlined_seek_at_134_19856 = NOVALUE;
    int _pos_inlined_seek_at_131_19855 = NOVALUE;
    int _seek_1__tmp_at176_19865 = NOVALUE;
    int _seek_inlined_seek_at_176_19864 = NOVALUE;
    int _pos_inlined_seek_at_173_19863 = NOVALUE;
    int _put4_1__tmp_at191_19867 = NOVALUE;
    int _seek_1__tmp_at319_19885 = NOVALUE;
    int _seek_inlined_seek_at_319_19884 = NOVALUE;
    int _pos_inlined_seek_at_316_19883 = NOVALUE;
    int _seek_1__tmp_at341_19889 = NOVALUE;
    int _seek_inlined_seek_at_341_19888 = NOVALUE;
    int _where_inlined_where_at_406_19898 = NOVALUE;
    int _seek_1__tmp_at450_19908 = NOVALUE;
    int _seek_inlined_seek_at_450_19907 = NOVALUE;
    int _pos_inlined_seek_at_447_19906 = NOVALUE;
    int _put4_1__tmp_at495_19917 = NOVALUE;
    int _x_inlined_put4_at_492_19916 = NOVALUE;
    int _seek_1__tmp_at532_19920 = NOVALUE;
    int _seek_inlined_seek_at_532_19919 = NOVALUE;
    int _put4_1__tmp_at553_19923 = NOVALUE;
    int _seek_1__tmp_at590_19928 = NOVALUE;
    int _seek_inlined_seek_at_590_19927 = NOVALUE;
    int _pos_inlined_seek_at_587_19926 = NOVALUE;
    int _seek_1__tmp_at692_19953 = NOVALUE;
    int _seek_inlined_seek_at_692_19952 = NOVALUE;
    int _pos_inlined_seek_at_689_19951 = NOVALUE;
    int _s_inlined_putn_at_753_19962 = NOVALUE;
    int _seek_1__tmp_at776_19965 = NOVALUE;
    int _seek_inlined_seek_at_776_19964 = NOVALUE;
    int _put4_1__tmp_at798_19969 = NOVALUE;
    int _x_inlined_put4_at_795_19968 = NOVALUE;
    int _seek_1__tmp_at835_19974 = NOVALUE;
    int _seek_inlined_seek_at_835_19973 = NOVALUE;
    int _pos_inlined_seek_at_832_19972 = NOVALUE;
    int _seek_1__tmp_at886_19984 = NOVALUE;
    int _seek_inlined_seek_at_886_19983 = NOVALUE;
    int _pos_inlined_seek_at_883_19982 = NOVALUE;
    int _put4_1__tmp_at901_19986 = NOVALUE;
    int _put4_1__tmp_at929_19988 = NOVALUE;
    int _seek_1__tmp_at982_19994 = NOVALUE;
    int _seek_inlined_seek_at_982_19993 = NOVALUE;
    int _pos_inlined_seek_at_979_19992 = NOVALUE;
    int _put4_1__tmp_at1003_19997 = NOVALUE;
    int _seek_1__tmp_at1040_20002 = NOVALUE;
    int _seek_inlined_seek_at_1040_20001 = NOVALUE;
    int _pos_inlined_seek_at_1037_20000 = NOVALUE;
    int _s_inlined_putn_at_1138_20020 = NOVALUE;
    int _seek_1__tmp_at1175_20025 = NOVALUE;
    int _seek_inlined_seek_at_1175_20024 = NOVALUE;
    int _pos_inlined_seek_at_1172_20023 = NOVALUE;
    int _put4_1__tmp_at1190_20027 = NOVALUE;
    int _11333 = NOVALUE;
    int _11332 = NOVALUE;
    int _11331 = NOVALUE;
    int _11330 = NOVALUE;
    int _11327 = NOVALUE;
    int _11326 = NOVALUE;
    int _11324 = NOVALUE;
    int _11322 = NOVALUE;
    int _11321 = NOVALUE;
    int _11319 = NOVALUE;
    int _11318 = NOVALUE;
    int _11316 = NOVALUE;
    int _11315 = NOVALUE;
    int _11313 = NOVALUE;
    int _11312 = NOVALUE;
    int _11311 = NOVALUE;
    int _11310 = NOVALUE;
    int _11309 = NOVALUE;
    int _11308 = NOVALUE;
    int _11307 = NOVALUE;
    int _11306 = NOVALUE;
    int _11305 = NOVALUE;
    int _11302 = NOVALUE;
    int _11301 = NOVALUE;
    int _11300 = NOVALUE;
    int _11299 = NOVALUE;
    int _11296 = NOVALUE;
    int _11293 = NOVALUE;
    int _11292 = NOVALUE;
    int _11291 = NOVALUE;
    int _11290 = NOVALUE;
    int _11286 = NOVALUE;
    int _11285 = NOVALUE;
    int _11283 = NOVALUE;
    int _11282 = NOVALUE;
    int _11280 = NOVALUE;
    int _11279 = NOVALUE;
    int _11278 = NOVALUE;
    int _11277 = NOVALUE;
    int _11276 = NOVALUE;
    int _11275 = NOVALUE;
    int _11274 = NOVALUE;
    int _11272 = NOVALUE;
    int _11269 = NOVALUE;
    int _11264 = NOVALUE;
    int _11262 = NOVALUE;
    int _11261 = NOVALUE;
    int _11259 = NOVALUE;
    int _11258 = NOVALUE;
    int _11257 = NOVALUE;
    int _11254 = NOVALUE;
    int _11252 = NOVALUE;
    int _11249 = NOVALUE;
    int _11248 = NOVALUE;
    int _11246 = NOVALUE;
    int _11245 = NOVALUE;
    int _11243 = NOVALUE;
    int _0, _1, _2;
    

    /** 	key_location = db_find_key(key, table_name) -- Let it set the current table if necessary*/
    Ref(_key_19815);
    RefDS(_table_name_19817);
    _0 = _key_location_19829;
    _key_location_19829 = _51db_find_key(_key_19815, _table_name_19817);
    DeRef(_0);

    /** 	if key_location > 0 then*/
    if (binary_op_a(LESSEQ, _key_location_19829, 0)){
        goto L1; // [10] 23
    }

    /** 		return DB_EXISTS_ALREADY*/
    DeRef(_key_19815);
    DeRefDS(_table_name_19817);
    DeRef(_key_string_19818);
    DeRef(_data_string_19819);
    DeRef(_last_part_19820);
    DeRef(_remaining_19821);
    DeRef(_key_ptr_19822);
    DeRef(_data_ptr_19823);
    DeRef(_records_ptr_19824);
    DeRef(_nrecs_19825);
    DeRef(_current_block_19826);
    DeRef(_size_19827);
    DeRef(_new_size_19828);
    DeRef(_key_location_19829);
    DeRef(_new_block_19830);
    DeRef(_index_ptr_19831);
    DeRef(_new_index_ptr_19832);
    DeRef(_total_recs_19833);
    return -2;
L1: 

    /** 	key_location = -key_location*/
    _0 = _key_location_19829;
    if (IS_ATOM_INT(_key_location_19829)) {
        if ((unsigned long)_key_location_19829 == 0xC0000000)
        _key_location_19829 = (int)NewDouble((double)-0xC0000000);
        else
        _key_location_19829 = - _key_location_19829;
    }
    else {
        _key_location_19829 = unary_op(UMINUS, _key_location_19829);
    }
    DeRef(_0);

    /** 	data_string = compress(data)*/
    _0 = _data_string_19819;
    _data_string_19819 = _51compress(_data_19816);
    DeRef(_0);

    /** 	key_string  = compress(key)*/
    Ref(_key_19815);
    _0 = _key_string_19818;
    _key_string_19818 = _51compress(_key_19815);
    DeRef(_0);

    /** 	data_ptr = db_allocate(length(data_string))*/
    if (IS_SEQUENCE(_data_string_19819)){
            _11243 = SEQ_PTR(_data_string_19819)->length;
    }
    else {
        _11243 = 1;
    }
    _0 = _data_ptr_19823;
    _data_ptr_19823 = _51db_allocate(_11243);
    DeRef(_0);
    _11243 = NOVALUE;

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _data_string_19819); // DJP 

    /** end procedure*/
    goto L2; // [64] 67
L2: 

    /** 	key_ptr = db_allocate(4+length(key_string))*/
    if (IS_SEQUENCE(_key_string_19818)){
            _11245 = SEQ_PTR(_key_string_19818)->length;
    }
    else {
        _11245 = 1;
    }
    _11246 = 4 + _11245;
    _11245 = NOVALUE;
    _0 = _key_ptr_19822;
    _key_ptr_19822 = _51db_allocate(_11246);
    DeRef(_0);
    _11246 = NOVALUE;

    /** 	put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_19823)) {
        *poke4_addr = (unsigned long)_data_ptr_19823;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_19823)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at81_19851);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at81_19851 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at81_19851); // DJP 

    /** end procedure*/
    goto L3; // [103] 106
L3: 
    DeRefi(_put4_1__tmp_at81_19851);
    _put4_1__tmp_at81_19851 = NOVALUE;

    /** 	putn(key_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _key_string_19818); // DJP 

    /** end procedure*/
    goto L4; // [119] 122
L4: 

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_51current_table_pos_18101)) {
        _11248 = _51current_table_pos_18101 + 4;
        if ((long)((unsigned long)_11248 + (unsigned long)HIGH_BITS) >= 0) 
        _11248 = NewDouble((double)_11248);
    }
    else {
        _11248 = NewDouble(DBL_PTR(_51current_table_pos_18101)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_131_19855);
    _pos_inlined_seek_at_131_19855 = _11248;
    _11248 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_131_19855);
    DeRef(_seek_1__tmp_at134_19857);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_131_19855;
    _seek_1__tmp_at134_19857 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_134_19856 = machine(19, _seek_1__tmp_at134_19857);
    DeRef(_pos_inlined_seek_at_131_19855);
    _pos_inlined_seek_at_131_19855 = NOVALUE;
    DeRef(_seek_1__tmp_at134_19857);
    _seek_1__tmp_at134_19857 = NOVALUE;

    /** 	total_recs = get4()+1*/
    _11249 = _51get4();
    DeRef(_total_recs_19833);
    if (IS_ATOM_INT(_11249)) {
        _total_recs_19833 = _11249 + 1;
        if (_total_recs_19833 > MAXINT){
            _total_recs_19833 = NewDouble((double)_total_recs_19833);
        }
    }
    else
    _total_recs_19833 = binary_op(PLUS, 1, _11249);
    DeRef(_11249);
    _11249 = NOVALUE;

    /** 	blocks = get4()*/
    _blocks_19835 = _51get4();
    if (!IS_ATOM_INT(_blocks_19835)) {
        _1 = (long)(DBL_PTR(_blocks_19835)->dbl);
        if (UNIQUE(DBL_PTR(_blocks_19835)) && (DBL_PTR(_blocks_19835)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_blocks_19835);
        _blocks_19835 = _1;
    }

    /** 	io:seek(current_db, current_table_pos+4)*/
    if (IS_ATOM_INT(_51current_table_pos_18101)) {
        _11252 = _51current_table_pos_18101 + 4;
        if ((long)((unsigned long)_11252 + (unsigned long)HIGH_BITS) >= 0) 
        _11252 = NewDouble((double)_11252);
    }
    else {
        _11252 = NewDouble(DBL_PTR(_51current_table_pos_18101)->dbl + (double)4);
    }
    DeRef(_pos_inlined_seek_at_173_19863);
    _pos_inlined_seek_at_173_19863 = _11252;
    _11252 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_173_19863);
    DeRef(_seek_1__tmp_at176_19865);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_173_19863;
    _seek_1__tmp_at176_19865 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_176_19864 = machine(19, _seek_1__tmp_at176_19865);
    DeRef(_pos_inlined_seek_at_173_19863);
    _pos_inlined_seek_at_173_19863 = NOVALUE;
    DeRef(_seek_1__tmp_at176_19865);
    _seek_1__tmp_at176_19865 = NOVALUE;

    /** 	put4(total_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_total_recs_19833)) {
        *poke4_addr = (unsigned long)_total_recs_19833;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_total_recs_19833)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at191_19867);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at191_19867 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at191_19867); // DJP 

    /** end procedure*/
    goto L5; // [213] 216
L5: 
    DeRefi(_put4_1__tmp_at191_19867);
    _put4_1__tmp_at191_19867 = NOVALUE;

    /** 	n = length(key_pointers)*/
    if (IS_SEQUENCE(_51key_pointers_18107)){
            _n_19837 = SEQ_PTR(_51key_pointers_18107)->length;
    }
    else {
        _n_19837 = 1;
    }

    /** 	if key_location >= floor(n/2) then*/
    _11254 = _n_19837 >> 1;
    if (binary_op_a(LESS, _key_location_19829, _11254)){
        _11254 = NOVALUE;
        goto L6; // [231] 270
    }
    DeRef(_11254);
    _11254 = NOVALUE;

    /** 		key_pointers = append(key_pointers, 0)*/
    Append(&_51key_pointers_18107, _51key_pointers_18107, 0);

    /** 		key_pointers[key_location+1..n+1] = key_pointers[key_location..n]*/
    if (IS_ATOM_INT(_key_location_19829)) {
        _11257 = _key_location_19829 + 1;
        if (_11257 > MAXINT){
            _11257 = NewDouble((double)_11257);
        }
    }
    else
    _11257 = binary_op(PLUS, 1, _key_location_19829);
    _11258 = _n_19837 + 1;
    rhs_slice_target = (object_ptr)&_11259;
    RHS_Slice(_51key_pointers_18107, _key_location_19829, _n_19837);
    assign_slice_seq = (s1_ptr *)&_51key_pointers_18107;
    AssignSlice(_11257, _11258, _11259);
    DeRef(_11257);
    _11257 = NOVALUE;
    _11258 = NOVALUE;
    DeRefDS(_11259);
    _11259 = NOVALUE;
    goto L7; // [267] 299
L6: 

    /** 		key_pointers = prepend(key_pointers, 0)*/
    Prepend(&_51key_pointers_18107, _51key_pointers_18107, 0);

    /** 		key_pointers[1..key_location-1] = key_pointers[2..key_location]*/
    if (IS_ATOM_INT(_key_location_19829)) {
        _11261 = _key_location_19829 - 1;
        if ((long)((unsigned long)_11261 +(unsigned long) HIGH_BITS) >= 0){
            _11261 = NewDouble((double)_11261);
        }
    }
    else {
        _11261 = NewDouble(DBL_PTR(_key_location_19829)->dbl - (double)1);
    }
    rhs_slice_target = (object_ptr)&_11262;
    RHS_Slice(_51key_pointers_18107, 2, _key_location_19829);
    assign_slice_seq = (s1_ptr *)&_51key_pointers_18107;
    AssignSlice(1, _11261, _11262);
    DeRef(_11261);
    _11261 = NOVALUE;
    DeRefDS(_11262);
    _11262 = NOVALUE;
L7: 

    /** 	key_pointers[key_location] = key_ptr*/
    Ref(_key_ptr_19822);
    _2 = (int)SEQ_PTR(_51key_pointers_18107);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _51key_pointers_18107 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_key_location_19829))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_key_location_19829)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _key_location_19829);
    _1 = *(int *)_2;
    *(int *)_2 = _key_ptr_19822;
    DeRef(_1);

    /** 	io:seek(current_db, current_table_pos+12) -- get after put - seek is necessary*/
    if (IS_ATOM_INT(_51current_table_pos_18101)) {
        _11264 = _51current_table_pos_18101 + 12;
        if ((long)((unsigned long)_11264 + (unsigned long)HIGH_BITS) >= 0) 
        _11264 = NewDouble((double)_11264);
    }
    else {
        _11264 = NewDouble(DBL_PTR(_51current_table_pos_18101)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_316_19883);
    _pos_inlined_seek_at_316_19883 = _11264;
    _11264 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_316_19883);
    DeRef(_seek_1__tmp_at319_19885);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_316_19883;
    _seek_1__tmp_at319_19885 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_319_19884 = machine(19, _seek_1__tmp_at319_19885);
    DeRef(_pos_inlined_seek_at_316_19883);
    _pos_inlined_seek_at_316_19883 = NOVALUE;
    DeRef(_seek_1__tmp_at319_19885);
    _seek_1__tmp_at319_19885 = NOVALUE;

    /** 	index_ptr = get4()*/
    _0 = _index_ptr_19831;
    _index_ptr_19831 = _51get4();
    DeRef(_0);

    /** 	io:seek(current_db, index_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_index_ptr_19831);
    DeRef(_seek_1__tmp_at341_19889);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _index_ptr_19831;
    _seek_1__tmp_at341_19889 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_341_19888 = machine(19, _seek_1__tmp_at341_19889);
    DeRef(_seek_1__tmp_at341_19889);
    _seek_1__tmp_at341_19889 = NOVALUE;

    /** 	r = 0*/
    _r_19834 = 0;

    /** 	while TRUE do*/
L8: 

    /** 		nrecs = get4()*/
    _0 = _nrecs_19825;
    _nrecs_19825 = _51get4();
    DeRef(_0);

    /** 		records_ptr = get4()*/
    _0 = _records_ptr_19824;
    _records_ptr_19824 = _51get4();
    DeRef(_0);

    /** 		r += nrecs*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        _r_19834 = _r_19834 + _nrecs_19825;
    }
    else {
        _r_19834 = NewDouble((double)_r_19834 + DBL_PTR(_nrecs_19825)->dbl);
    }
    if (!IS_ATOM_INT(_r_19834)) {
        _1 = (long)(DBL_PTR(_r_19834)->dbl);
        if (UNIQUE(DBL_PTR(_r_19834)) && (DBL_PTR(_r_19834)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_r_19834);
        _r_19834 = _1;
    }

    /** 		if r + 1 >= key_location then*/
    _11269 = _r_19834 + 1;
    if (_11269 > MAXINT){
        _11269 = NewDouble((double)_11269);
    }
    if (binary_op_a(LESS, _11269, _key_location_19829)){
        DeRef(_11269);
        _11269 = NOVALUE;
        goto L8; // [389] 365
    }
    DeRef(_11269);
    _11269 = NOVALUE;

    /** 			exit*/
    goto L9; // [395] 403

    /** 	end while*/
    goto L8; // [400] 365
L9: 

    /** 	current_block = io:where(current_db)-8*/

    /** 	return machine_func(M_WHERE, fn)*/
    DeRef(_where_inlined_where_at_406_19898);
    _where_inlined_where_at_406_19898 = machine(20, _51current_db_18100);
    DeRef(_current_block_19826);
    if (IS_ATOM_INT(_where_inlined_where_at_406_19898)) {
        _current_block_19826 = _where_inlined_where_at_406_19898 - 8;
        if ((long)((unsigned long)_current_block_19826 +(unsigned long) HIGH_BITS) >= 0){
            _current_block_19826 = NewDouble((double)_current_block_19826);
        }
    }
    else {
        _current_block_19826 = NewDouble(DBL_PTR(_where_inlined_where_at_406_19898)->dbl - (double)8);
    }

    /** 	key_location -= (r-nrecs)*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        _11272 = _r_19834 - _nrecs_19825;
        if ((long)((unsigned long)_11272 +(unsigned long) HIGH_BITS) >= 0){
            _11272 = NewDouble((double)_11272);
        }
    }
    else {
        _11272 = NewDouble((double)_r_19834 - DBL_PTR(_nrecs_19825)->dbl);
    }
    _0 = _key_location_19829;
    if (IS_ATOM_INT(_key_location_19829) && IS_ATOM_INT(_11272)) {
        _key_location_19829 = _key_location_19829 - _11272;
        if ((long)((unsigned long)_key_location_19829 +(unsigned long) HIGH_BITS) >= 0){
            _key_location_19829 = NewDouble((double)_key_location_19829);
        }
    }
    else {
        if (IS_ATOM_INT(_key_location_19829)) {
            _key_location_19829 = NewDouble((double)_key_location_19829 - DBL_PTR(_11272)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11272)) {
                _key_location_19829 = NewDouble(DBL_PTR(_key_location_19829)->dbl - (double)_11272);
            }
            else
            _key_location_19829 = NewDouble(DBL_PTR(_key_location_19829)->dbl - DBL_PTR(_11272)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_11272);
    _11272 = NOVALUE;

    /** 	io:seek(current_db, records_ptr+4*(key_location-1))*/
    if (IS_ATOM_INT(_key_location_19829)) {
        _11274 = _key_location_19829 - 1;
        if ((long)((unsigned long)_11274 +(unsigned long) HIGH_BITS) >= 0){
            _11274 = NewDouble((double)_11274);
        }
    }
    else {
        _11274 = NewDouble(DBL_PTR(_key_location_19829)->dbl - (double)1);
    }
    if (IS_ATOM_INT(_11274)) {
        if (_11274 <= INT15 && _11274 >= -INT15)
        _11275 = 4 * _11274;
        else
        _11275 = NewDouble(4 * (double)_11274);
    }
    else {
        _11275 = NewDouble((double)4 * DBL_PTR(_11274)->dbl);
    }
    DeRef(_11274);
    _11274 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_19824) && IS_ATOM_INT(_11275)) {
        _11276 = _records_ptr_19824 + _11275;
        if ((long)((unsigned long)_11276 + (unsigned long)HIGH_BITS) >= 0) 
        _11276 = NewDouble((double)_11276);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_19824)) {
            _11276 = NewDouble((double)_records_ptr_19824 + DBL_PTR(_11275)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11275)) {
                _11276 = NewDouble(DBL_PTR(_records_ptr_19824)->dbl + (double)_11275);
            }
            else
            _11276 = NewDouble(DBL_PTR(_records_ptr_19824)->dbl + DBL_PTR(_11275)->dbl);
        }
    }
    DeRef(_11275);
    _11275 = NOVALUE;
    DeRef(_pos_inlined_seek_at_447_19906);
    _pos_inlined_seek_at_447_19906 = _11276;
    _11276 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_447_19906);
    DeRef(_seek_1__tmp_at450_19908);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_447_19906;
    _seek_1__tmp_at450_19908 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_450_19907 = machine(19, _seek_1__tmp_at450_19908);
    DeRef(_pos_inlined_seek_at_447_19906);
    _pos_inlined_seek_at_447_19906 = NOVALUE;
    DeRef(_seek_1__tmp_at450_19908);
    _seek_1__tmp_at450_19908 = NOVALUE;

    /** 	for i = key_location to nrecs+1 do*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        _11277 = _nrecs_19825 + 1;
        if (_11277 > MAXINT){
            _11277 = NewDouble((double)_11277);
        }
    }
    else
    _11277 = binary_op(PLUS, 1, _nrecs_19825);
    {
        int _i_19910;
        Ref(_key_location_19829);
        _i_19910 = _key_location_19829;
LA: 
        if (binary_op_a(GREATER, _i_19910, _11277)){
            goto LB; // [470] 529
        }

        /** 		put4(key_pointers[i+r-nrecs])*/
        if (IS_ATOM_INT(_i_19910)) {
            _11278 = _i_19910 + _r_19834;
            if ((long)((unsigned long)_11278 + (unsigned long)HIGH_BITS) >= 0) 
            _11278 = NewDouble((double)_11278);
        }
        else {
            _11278 = NewDouble(DBL_PTR(_i_19910)->dbl + (double)_r_19834);
        }
        if (IS_ATOM_INT(_11278) && IS_ATOM_INT(_nrecs_19825)) {
            _11279 = _11278 - _nrecs_19825;
        }
        else {
            if (IS_ATOM_INT(_11278)) {
                _11279 = NewDouble((double)_11278 - DBL_PTR(_nrecs_19825)->dbl);
            }
            else {
                if (IS_ATOM_INT(_nrecs_19825)) {
                    _11279 = NewDouble(DBL_PTR(_11278)->dbl - (double)_nrecs_19825);
                }
                else
                _11279 = NewDouble(DBL_PTR(_11278)->dbl - DBL_PTR(_nrecs_19825)->dbl);
            }
        }
        DeRef(_11278);
        _11278 = NOVALUE;
        _2 = (int)SEQ_PTR(_51key_pointers_18107);
        if (!IS_ATOM_INT(_11279)){
            _11280 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_11279)->dbl));
        }
        else{
            _11280 = (int)*(((s1_ptr)_2)->base + _11279);
        }
        Ref(_11280);
        DeRef(_x_inlined_put4_at_492_19916);
        _x_inlined_put4_at_492_19916 = _11280;
        _11280 = NOVALUE;

        /** 	poke4(mem0, x) -- faster than doing divides etc.*/
        if (IS_ATOM_INT(_51mem0_18142)){
            poke4_addr = (unsigned long *)_51mem0_18142;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
        }
        if (IS_ATOM_INT(_x_inlined_put4_at_492_19916)) {
            *poke4_addr = (unsigned long)_x_inlined_put4_at_492_19916;
        }
        else if (IS_ATOM(_x_inlined_put4_at_492_19916)) {
            _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_492_19916)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_x_inlined_put4_at_492_19916);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }

        /** 	puts(current_db, peek(memseq))*/
        DeRefi(_put4_1__tmp_at495_19917);
        _1 = (int)SEQ_PTR(_51memseq_18377);
        poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
        _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
        poke4_addr = (unsigned long *)NewS1(_2);
        _put4_1__tmp_at495_19917 = MAKE_SEQ(poke4_addr);
        poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
        while (--_2 >= 0) {
            poke4_addr++;
            _1 = (int)(unsigned char)*poke_addr++;
            *(int *)poke4_addr = _1;
        }
        EPuts(_51current_db_18100, _put4_1__tmp_at495_19917); // DJP 

        /** end procedure*/
        goto LC; // [517] 520
LC: 
        DeRef(_x_inlined_put4_at_492_19916);
        _x_inlined_put4_at_492_19916 = NOVALUE;
        DeRefi(_put4_1__tmp_at495_19917);
        _put4_1__tmp_at495_19917 = NOVALUE;

        /** 	end for*/
        _0 = _i_19910;
        if (IS_ATOM_INT(_i_19910)) {
            _i_19910 = _i_19910 + 1;
            if ((long)((unsigned long)_i_19910 +(unsigned long) HIGH_BITS) >= 0){
                _i_19910 = NewDouble((double)_i_19910);
            }
        }
        else {
            _i_19910 = binary_op_a(PLUS, _i_19910, 1);
        }
        DeRef(_0);
        goto LA; // [524] 477
LB: 
        ;
        DeRef(_i_19910);
    }

    /** 	io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_19826);
    DeRef(_seek_1__tmp_at532_19920);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _current_block_19826;
    _seek_1__tmp_at532_19920 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_532_19919 = machine(19, _seek_1__tmp_at532_19920);
    DeRef(_seek_1__tmp_at532_19920);
    _seek_1__tmp_at532_19920 = NOVALUE;

    /** 	nrecs += 1*/
    _0 = _nrecs_19825;
    if (IS_ATOM_INT(_nrecs_19825)) {
        _nrecs_19825 = _nrecs_19825 + 1;
        if (_nrecs_19825 > MAXINT){
            _nrecs_19825 = NewDouble((double)_nrecs_19825);
        }
    }
    else
    _nrecs_19825 = binary_op(PLUS, 1, _nrecs_19825);
    DeRef(_0);

    /** 	put4(nrecs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_nrecs_19825)) {
        *poke4_addr = (unsigned long)_nrecs_19825;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_nrecs_19825)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at553_19923);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at553_19923 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at553_19923); // DJP 

    /** end procedure*/
    goto LD; // [575] 578
LD: 
    DeRefi(_put4_1__tmp_at553_19923);
    _put4_1__tmp_at553_19923 = NOVALUE;

    /** 	io:seek(current_db, records_ptr - 4)*/
    if (IS_ATOM_INT(_records_ptr_19824)) {
        _11282 = _records_ptr_19824 - 4;
        if ((long)((unsigned long)_11282 +(unsigned long) HIGH_BITS) >= 0){
            _11282 = NewDouble((double)_11282);
        }
    }
    else {
        _11282 = NewDouble(DBL_PTR(_records_ptr_19824)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_587_19926);
    _pos_inlined_seek_at_587_19926 = _11282;
    _11282 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_587_19926);
    DeRef(_seek_1__tmp_at590_19928);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_587_19926;
    _seek_1__tmp_at590_19928 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_590_19927 = machine(19, _seek_1__tmp_at590_19928);
    DeRef(_pos_inlined_seek_at_587_19926);
    _pos_inlined_seek_at_587_19926 = NOVALUE;
    DeRef(_seek_1__tmp_at590_19928);
    _seek_1__tmp_at590_19928 = NOVALUE;

    /** 	size = get4() - 4*/
    _11283 = _51get4();
    DeRef(_size_19827);
    if (IS_ATOM_INT(_11283)) {
        _size_19827 = _11283 - 4;
        if ((long)((unsigned long)_size_19827 +(unsigned long) HIGH_BITS) >= 0){
            _size_19827 = NewDouble((double)_size_19827);
        }
    }
    else {
        _size_19827 = binary_op(MINUS, _11283, 4);
    }
    DeRef(_11283);
    _11283 = NOVALUE;

    /** 	if nrecs*4 > size-4 then*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        if (_nrecs_19825 == (short)_nrecs_19825)
        _11285 = _nrecs_19825 * 4;
        else
        _11285 = NewDouble(_nrecs_19825 * (double)4);
    }
    else {
        _11285 = NewDouble(DBL_PTR(_nrecs_19825)->dbl * (double)4);
    }
    if (IS_ATOM_INT(_size_19827)) {
        _11286 = _size_19827 - 4;
        if ((long)((unsigned long)_11286 +(unsigned long) HIGH_BITS) >= 0){
            _11286 = NewDouble((double)_11286);
        }
    }
    else {
        _11286 = NewDouble(DBL_PTR(_size_19827)->dbl - (double)4);
    }
    if (binary_op_a(LESSEQ, _11285, _11286)){
        DeRef(_11285);
        _11285 = NOVALUE;
        DeRef(_11286);
        _11286 = NOVALUE;
        goto LE; // [623] 1219
    }
    DeRef(_11285);
    _11285 = NOVALUE;
    DeRef(_11286);
    _11286 = NOVALUE;

    /** 		new_size = 8 * (20 + floor(sqrt(1.5 * total_recs)))*/
    if (IS_ATOM_INT(_total_recs_19833)) {
        _11290 = NewDouble(DBL_PTR(_11289)->dbl * (double)_total_recs_19833);
    }
    else
    _11290 = NewDouble(DBL_PTR(_11289)->dbl * DBL_PTR(_total_recs_19833)->dbl);
    _11291 = unary_op(SQRT, _11290);
    DeRefDS(_11290);
    _11290 = NOVALUE;
    _11292 = unary_op(FLOOR, _11291);
    DeRefDS(_11291);
    _11291 = NOVALUE;
    if (IS_ATOM_INT(_11292)) {
        _11293 = 20 + _11292;
        if ((long)((unsigned long)_11293 + (unsigned long)HIGH_BITS) >= 0) 
        _11293 = NewDouble((double)_11293);
    }
    else {
        _11293 = binary_op(PLUS, 20, _11292);
    }
    DeRef(_11292);
    _11292 = NOVALUE;
    DeRef(_new_size_19828);
    if (IS_ATOM_INT(_11293)) {
        if (_11293 <= INT15 && _11293 >= -INT15)
        _new_size_19828 = 8 * _11293;
        else
        _new_size_19828 = NewDouble(8 * (double)_11293);
    }
    else {
        _new_size_19828 = binary_op(MULTIPLY, 8, _11293);
    }
    DeRef(_11293);
    _11293 = NOVALUE;

    /** 		new_recs = floor(new_size/8)*/
    if (IS_ATOM_INT(_new_size_19828)) {
        if (8 > 0 && _new_size_19828 >= 0) {
            _new_recs_19836 = _new_size_19828 / 8;
        }
        else {
            temp_dbl = floor((double)_new_size_19828 / (double)8);
            _new_recs_19836 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _new_size_19828, 8);
        _new_recs_19836 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    if (!IS_ATOM_INT(_new_recs_19836)) {
        _1 = (long)(DBL_PTR(_new_recs_19836)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_19836)) && (DBL_PTR(_new_recs_19836)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_19836);
        _new_recs_19836 = _1;
    }

    /** 		if new_recs > floor(nrecs/2) then*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        _11296 = _nrecs_19825 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_19825, 2);
        _11296 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (binary_op_a(LESSEQ, _new_recs_19836, _11296)){
        DeRef(_11296);
        _11296 = NOVALUE;
        goto LF; // [661] 674
    }
    DeRef(_11296);
    _11296 = NOVALUE;

    /** 			new_recs = floor(nrecs/2)*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        _new_recs_19836 = _nrecs_19825 >> 1;
    }
    else {
        _1 = binary_op(DIVIDE, _nrecs_19825, 2);
        _new_recs_19836 = unary_op(FLOOR, _1);
        DeRef(_1);
    }
    if (!IS_ATOM_INT(_new_recs_19836)) {
        _1 = (long)(DBL_PTR(_new_recs_19836)->dbl);
        if (UNIQUE(DBL_PTR(_new_recs_19836)) && (DBL_PTR(_new_recs_19836)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_new_recs_19836);
        _new_recs_19836 = _1;
    }
LF: 

    /** 		io:seek(current_db, records_ptr + (nrecs-new_recs)*4)*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        _11299 = _nrecs_19825 - _new_recs_19836;
        if ((long)((unsigned long)_11299 +(unsigned long) HIGH_BITS) >= 0){
            _11299 = NewDouble((double)_11299);
        }
    }
    else {
        _11299 = NewDouble(DBL_PTR(_nrecs_19825)->dbl - (double)_new_recs_19836);
    }
    if (IS_ATOM_INT(_11299)) {
        if (_11299 == (short)_11299)
        _11300 = _11299 * 4;
        else
        _11300 = NewDouble(_11299 * (double)4);
    }
    else {
        _11300 = NewDouble(DBL_PTR(_11299)->dbl * (double)4);
    }
    DeRef(_11299);
    _11299 = NOVALUE;
    if (IS_ATOM_INT(_records_ptr_19824) && IS_ATOM_INT(_11300)) {
        _11301 = _records_ptr_19824 + _11300;
        if ((long)((unsigned long)_11301 + (unsigned long)HIGH_BITS) >= 0) 
        _11301 = NewDouble((double)_11301);
    }
    else {
        if (IS_ATOM_INT(_records_ptr_19824)) {
            _11301 = NewDouble((double)_records_ptr_19824 + DBL_PTR(_11300)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11300)) {
                _11301 = NewDouble(DBL_PTR(_records_ptr_19824)->dbl + (double)_11300);
            }
            else
            _11301 = NewDouble(DBL_PTR(_records_ptr_19824)->dbl + DBL_PTR(_11300)->dbl);
        }
    }
    DeRef(_11300);
    _11300 = NOVALUE;
    DeRef(_pos_inlined_seek_at_689_19951);
    _pos_inlined_seek_at_689_19951 = _11301;
    _11301 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_689_19951);
    DeRef(_seek_1__tmp_at692_19953);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_689_19951;
    _seek_1__tmp_at692_19953 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_692_19952 = machine(19, _seek_1__tmp_at692_19953);
    DeRef(_pos_inlined_seek_at_689_19951);
    _pos_inlined_seek_at_689_19951 = NOVALUE;
    DeRef(_seek_1__tmp_at692_19953);
    _seek_1__tmp_at692_19953 = NOVALUE;

    /** 		last_part = io:get_bytes(current_db, new_recs*4)*/
    if (_new_recs_19836 == (short)_new_recs_19836)
    _11302 = _new_recs_19836 * 4;
    else
    _11302 = NewDouble(_new_recs_19836 * (double)4);
    _0 = _last_part_19820;
    _last_part_19820 = _16get_bytes(_51current_db_18100, _11302);
    DeRef(_0);
    _11302 = NOVALUE;

    /** 		new_block = db_allocate(new_size)*/
    Ref(_new_size_19828);
    _0 = _new_block_19830;
    _new_block_19830 = _51db_allocate(_new_size_19828);
    DeRef(_0);

    /** 		putn(last_part)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _last_part_19820); // DJP 

    /** end procedure*/
    goto L10; // [738] 741
L10: 

    /** 		putn(repeat(0, new_size-length(last_part)))*/
    if (IS_SEQUENCE(_last_part_19820)){
            _11305 = SEQ_PTR(_last_part_19820)->length;
    }
    else {
        _11305 = 1;
    }
    if (IS_ATOM_INT(_new_size_19828)) {
        _11306 = _new_size_19828 - _11305;
    }
    else {
        _11306 = NewDouble(DBL_PTR(_new_size_19828)->dbl - (double)_11305);
    }
    _11305 = NOVALUE;
    _11307 = Repeat(0, _11306);
    DeRef(_11306);
    _11306 = NOVALUE;
    DeRefi(_s_inlined_putn_at_753_19962);
    _s_inlined_putn_at_753_19962 = _11307;
    _11307 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_753_19962); // DJP 

    /** end procedure*/
    goto L11; // [768] 771
L11: 
    DeRefi(_s_inlined_putn_at_753_19962);
    _s_inlined_putn_at_753_19962 = NOVALUE;

    /** 		io:seek(current_db, current_block)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_current_block_19826);
    DeRef(_seek_1__tmp_at776_19965);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _current_block_19826;
    _seek_1__tmp_at776_19965 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_776_19964 = machine(19, _seek_1__tmp_at776_19965);
    DeRef(_seek_1__tmp_at776_19965);
    _seek_1__tmp_at776_19965 = NOVALUE;

    /** 		put4(nrecs-new_recs)*/
    if (IS_ATOM_INT(_nrecs_19825)) {
        _11308 = _nrecs_19825 - _new_recs_19836;
        if ((long)((unsigned long)_11308 +(unsigned long) HIGH_BITS) >= 0){
            _11308 = NewDouble((double)_11308);
        }
    }
    else {
        _11308 = NewDouble(DBL_PTR(_nrecs_19825)->dbl - (double)_new_recs_19836);
    }
    DeRef(_x_inlined_put4_at_795_19968);
    _x_inlined_put4_at_795_19968 = _11308;
    _11308 = NOVALUE;

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_x_inlined_put4_at_795_19968)) {
        *poke4_addr = (unsigned long)_x_inlined_put4_at_795_19968;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_x_inlined_put4_at_795_19968)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at798_19969);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at798_19969 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at798_19969); // DJP 

    /** end procedure*/
    goto L12; // [820] 823
L12: 
    DeRef(_x_inlined_put4_at_795_19968);
    _x_inlined_put4_at_795_19968 = NOVALUE;
    DeRefi(_put4_1__tmp_at798_19969);
    _put4_1__tmp_at798_19969 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_19826)) {
        _11309 = _current_block_19826 + 8;
        if ((long)((unsigned long)_11309 + (unsigned long)HIGH_BITS) >= 0) 
        _11309 = NewDouble((double)_11309);
    }
    else {
        _11309 = NewDouble(DBL_PTR(_current_block_19826)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_832_19972);
    _pos_inlined_seek_at_832_19972 = _11309;
    _11309 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_832_19972);
    DeRef(_seek_1__tmp_at835_19974);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_832_19972;
    _seek_1__tmp_at835_19974 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_835_19973 = machine(19, _seek_1__tmp_at835_19974);
    DeRef(_pos_inlined_seek_at_832_19972);
    _pos_inlined_seek_at_832_19972 = NOVALUE;
    DeRef(_seek_1__tmp_at835_19974);
    _seek_1__tmp_at835_19974 = NOVALUE;

    /** 		remaining = io:get_bytes(current_db, index_ptr+blocks*8-(current_block+8))*/
    if (_blocks_19835 == (short)_blocks_19835)
    _11310 = _blocks_19835 * 8;
    else
    _11310 = NewDouble(_blocks_19835 * (double)8);
    if (IS_ATOM_INT(_index_ptr_19831) && IS_ATOM_INT(_11310)) {
        _11311 = _index_ptr_19831 + _11310;
        if ((long)((unsigned long)_11311 + (unsigned long)HIGH_BITS) >= 0) 
        _11311 = NewDouble((double)_11311);
    }
    else {
        if (IS_ATOM_INT(_index_ptr_19831)) {
            _11311 = NewDouble((double)_index_ptr_19831 + DBL_PTR(_11310)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11310)) {
                _11311 = NewDouble(DBL_PTR(_index_ptr_19831)->dbl + (double)_11310);
            }
            else
            _11311 = NewDouble(DBL_PTR(_index_ptr_19831)->dbl + DBL_PTR(_11310)->dbl);
        }
    }
    DeRef(_11310);
    _11310 = NOVALUE;
    if (IS_ATOM_INT(_current_block_19826)) {
        _11312 = _current_block_19826 + 8;
        if ((long)((unsigned long)_11312 + (unsigned long)HIGH_BITS) >= 0) 
        _11312 = NewDouble((double)_11312);
    }
    else {
        _11312 = NewDouble(DBL_PTR(_current_block_19826)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_11311) && IS_ATOM_INT(_11312)) {
        _11313 = _11311 - _11312;
        if ((long)((unsigned long)_11313 +(unsigned long) HIGH_BITS) >= 0){
            _11313 = NewDouble((double)_11313);
        }
    }
    else {
        if (IS_ATOM_INT(_11311)) {
            _11313 = NewDouble((double)_11311 - DBL_PTR(_11312)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11312)) {
                _11313 = NewDouble(DBL_PTR(_11311)->dbl - (double)_11312);
            }
            else
            _11313 = NewDouble(DBL_PTR(_11311)->dbl - DBL_PTR(_11312)->dbl);
        }
    }
    DeRef(_11311);
    _11311 = NOVALUE;
    DeRef(_11312);
    _11312 = NOVALUE;
    _0 = _remaining_19821;
    _remaining_19821 = _16get_bytes(_51current_db_18100, _11313);
    DeRef(_0);
    _11313 = NOVALUE;

    /** 		io:seek(current_db, current_block+8)*/
    if (IS_ATOM_INT(_current_block_19826)) {
        _11315 = _current_block_19826 + 8;
        if ((long)((unsigned long)_11315 + (unsigned long)HIGH_BITS) >= 0) 
        _11315 = NewDouble((double)_11315);
    }
    else {
        _11315 = NewDouble(DBL_PTR(_current_block_19826)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_883_19982);
    _pos_inlined_seek_at_883_19982 = _11315;
    _11315 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_883_19982);
    DeRef(_seek_1__tmp_at886_19984);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_883_19982;
    _seek_1__tmp_at886_19984 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_886_19983 = machine(19, _seek_1__tmp_at886_19984);
    DeRef(_pos_inlined_seek_at_883_19982);
    _pos_inlined_seek_at_883_19982 = NOVALUE;
    DeRef(_seek_1__tmp_at886_19984);
    _seek_1__tmp_at886_19984 = NOVALUE;

    /** 		put4(new_recs)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)_new_recs_19836;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at901_19986);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at901_19986 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at901_19986); // DJP 

    /** end procedure*/
    goto L13; // [923] 926
L13: 
    DeRefi(_put4_1__tmp_at901_19986);
    _put4_1__tmp_at901_19986 = NOVALUE;

    /** 		put4(new_block)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_new_block_19830)) {
        *poke4_addr = (unsigned long)_new_block_19830;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_block_19830)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at929_19988);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at929_19988 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at929_19988); // DJP 

    /** end procedure*/
    goto L14; // [951] 954
L14: 
    DeRefi(_put4_1__tmp_at929_19988);
    _put4_1__tmp_at929_19988 = NOVALUE;

    /** 		putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _remaining_19821); // DJP 

    /** end procedure*/
    goto L15; // [967] 970
L15: 

    /** 		io:seek(current_db, current_table_pos+8)*/
    if (IS_ATOM_INT(_51current_table_pos_18101)) {
        _11316 = _51current_table_pos_18101 + 8;
        if ((long)((unsigned long)_11316 + (unsigned long)HIGH_BITS) >= 0) 
        _11316 = NewDouble((double)_11316);
    }
    else {
        _11316 = NewDouble(DBL_PTR(_51current_table_pos_18101)->dbl + (double)8);
    }
    DeRef(_pos_inlined_seek_at_979_19992);
    _pos_inlined_seek_at_979_19992 = _11316;
    _11316 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_979_19992);
    DeRef(_seek_1__tmp_at982_19994);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_979_19992;
    _seek_1__tmp_at982_19994 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_982_19993 = machine(19, _seek_1__tmp_at982_19994);
    DeRef(_pos_inlined_seek_at_979_19992);
    _pos_inlined_seek_at_979_19992 = NOVALUE;
    DeRef(_seek_1__tmp_at982_19994);
    _seek_1__tmp_at982_19994 = NOVALUE;

    /** 		blocks += 1*/
    _blocks_19835 = _blocks_19835 + 1;

    /** 		put4(blocks)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    *poke4_addr = (unsigned long)_blocks_19835;

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1003_19997);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1003_19997 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at1003_19997); // DJP 

    /** end procedure*/
    goto L16; // [1025] 1028
L16: 
    DeRefi(_put4_1__tmp_at1003_19997);
    _put4_1__tmp_at1003_19997 = NOVALUE;

    /** 		io:seek(current_db, index_ptr-4)*/
    if (IS_ATOM_INT(_index_ptr_19831)) {
        _11318 = _index_ptr_19831 - 4;
        if ((long)((unsigned long)_11318 +(unsigned long) HIGH_BITS) >= 0){
            _11318 = NewDouble((double)_11318);
        }
    }
    else {
        _11318 = NewDouble(DBL_PTR(_index_ptr_19831)->dbl - (double)4);
    }
    DeRef(_pos_inlined_seek_at_1037_20000);
    _pos_inlined_seek_at_1037_20000 = _11318;
    _11318 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1037_20000);
    DeRef(_seek_1__tmp_at1040_20002);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_1037_20000;
    _seek_1__tmp_at1040_20002 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1040_20001 = machine(19, _seek_1__tmp_at1040_20002);
    DeRef(_pos_inlined_seek_at_1037_20000);
    _pos_inlined_seek_at_1037_20000 = NOVALUE;
    DeRef(_seek_1__tmp_at1040_20002);
    _seek_1__tmp_at1040_20002 = NOVALUE;

    /** 		size = get4() - 4*/
    _11319 = _51get4();
    DeRef(_size_19827);
    if (IS_ATOM_INT(_11319)) {
        _size_19827 = _11319 - 4;
        if ((long)((unsigned long)_size_19827 +(unsigned long) HIGH_BITS) >= 0){
            _size_19827 = NewDouble((double)_size_19827);
        }
    }
    else {
        _size_19827 = binary_op(MINUS, _11319, 4);
    }
    DeRef(_11319);
    _11319 = NOVALUE;

    /** 		if blocks*8 > size-8 then*/
    if (_blocks_19835 == (short)_blocks_19835)
    _11321 = _blocks_19835 * 8;
    else
    _11321 = NewDouble(_blocks_19835 * (double)8);
    if (IS_ATOM_INT(_size_19827)) {
        _11322 = _size_19827 - 8;
        if ((long)((unsigned long)_11322 +(unsigned long) HIGH_BITS) >= 0){
            _11322 = NewDouble((double)_11322);
        }
    }
    else {
        _11322 = NewDouble(DBL_PTR(_size_19827)->dbl - (double)8);
    }
    if (binary_op_a(LESSEQ, _11321, _11322)){
        DeRef(_11321);
        _11321 = NOVALUE;
        DeRef(_11322);
        _11322 = NOVALUE;
        goto L17; // [1073] 1218
    }
    DeRef(_11321);
    _11321 = NOVALUE;
    DeRef(_11322);
    _11322 = NOVALUE;

    /** 			remaining = io:get_bytes(current_db, blocks*8)*/
    if (_blocks_19835 == (short)_blocks_19835)
    _11324 = _blocks_19835 * 8;
    else
    _11324 = NewDouble(_blocks_19835 * (double)8);
    _0 = _remaining_19821;
    _remaining_19821 = _16get_bytes(_51current_db_18100, _11324);
    DeRef(_0);
    _11324 = NOVALUE;

    /** 			new_size = floor(size + size/2)*/
    if (IS_ATOM_INT(_size_19827)) {
        if (_size_19827 & 1) {
            _11326 = NewDouble((_size_19827 >> 1) + 0.5);
        }
        else
        _11326 = _size_19827 >> 1;
    }
    else {
        _11326 = binary_op(DIVIDE, _size_19827, 2);
    }
    if (IS_ATOM_INT(_size_19827) && IS_ATOM_INT(_11326)) {
        _11327 = _size_19827 + _11326;
        if ((long)((unsigned long)_11327 + (unsigned long)HIGH_BITS) >= 0) 
        _11327 = NewDouble((double)_11327);
    }
    else {
        if (IS_ATOM_INT(_size_19827)) {
            _11327 = NewDouble((double)_size_19827 + DBL_PTR(_11326)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11326)) {
                _11327 = NewDouble(DBL_PTR(_size_19827)->dbl + (double)_11326);
            }
            else
            _11327 = NewDouble(DBL_PTR(_size_19827)->dbl + DBL_PTR(_11326)->dbl);
        }
    }
    DeRef(_11326);
    _11326 = NOVALUE;
    DeRef(_new_size_19828);
    if (IS_ATOM_INT(_11327))
    _new_size_19828 = e_floor(_11327);
    else
    _new_size_19828 = unary_op(FLOOR, _11327);
    DeRef(_11327);
    _11327 = NOVALUE;

    /** 			new_index_ptr = db_allocate(new_size)*/
    Ref(_new_size_19828);
    _0 = _new_index_ptr_19832;
    _new_index_ptr_19832 = _51db_allocate(_new_size_19828);
    DeRef(_0);

    /** 			putn(remaining)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _remaining_19821); // DJP 

    /** end procedure*/
    goto L18; // [1122] 1125
L18: 

    /** 			putn(repeat(0, new_size-blocks*8))*/
    if (_blocks_19835 == (short)_blocks_19835)
    _11330 = _blocks_19835 * 8;
    else
    _11330 = NewDouble(_blocks_19835 * (double)8);
    if (IS_ATOM_INT(_new_size_19828) && IS_ATOM_INT(_11330)) {
        _11331 = _new_size_19828 - _11330;
    }
    else {
        if (IS_ATOM_INT(_new_size_19828)) {
            _11331 = NewDouble((double)_new_size_19828 - DBL_PTR(_11330)->dbl);
        }
        else {
            if (IS_ATOM_INT(_11330)) {
                _11331 = NewDouble(DBL_PTR(_new_size_19828)->dbl - (double)_11330);
            }
            else
            _11331 = NewDouble(DBL_PTR(_new_size_19828)->dbl - DBL_PTR(_11330)->dbl);
        }
    }
    DeRef(_11330);
    _11330 = NOVALUE;
    _11332 = Repeat(0, _11331);
    DeRef(_11331);
    _11331 = NOVALUE;
    DeRefi(_s_inlined_putn_at_1138_20020);
    _s_inlined_putn_at_1138_20020 = _11332;
    _11332 = NOVALUE;

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _s_inlined_putn_at_1138_20020); // DJP 

    /** end procedure*/
    goto L19; // [1153] 1156
L19: 
    DeRefi(_s_inlined_putn_at_1138_20020);
    _s_inlined_putn_at_1138_20020 = NOVALUE;

    /** 			db_free(index_ptr)*/
    Ref(_index_ptr_19831);
    _51db_free(_index_ptr_19831);

    /** 			io:seek(current_db, current_table_pos+12)*/
    if (IS_ATOM_INT(_51current_table_pos_18101)) {
        _11333 = _51current_table_pos_18101 + 12;
        if ((long)((unsigned long)_11333 + (unsigned long)HIGH_BITS) >= 0) 
        _11333 = NewDouble((double)_11333);
    }
    else {
        _11333 = NewDouble(DBL_PTR(_51current_table_pos_18101)->dbl + (double)12);
    }
    DeRef(_pos_inlined_seek_at_1172_20023);
    _pos_inlined_seek_at_1172_20023 = _11333;
    _11333 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_1172_20023);
    DeRef(_seek_1__tmp_at1175_20025);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_1172_20023;
    _seek_1__tmp_at1175_20025 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_1175_20024 = machine(19, _seek_1__tmp_at1175_20025);
    DeRef(_pos_inlined_seek_at_1172_20023);
    _pos_inlined_seek_at_1172_20023 = NOVALUE;
    DeRef(_seek_1__tmp_at1175_20025);
    _seek_1__tmp_at1175_20025 = NOVALUE;

    /** 			put4(new_index_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_new_index_ptr_19832)) {
        *poke4_addr = (unsigned long)_new_index_ptr_19832;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_new_index_ptr_19832)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at1190_20027);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at1190_20027 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at1190_20027); // DJP 

    /** end procedure*/
    goto L1A; // [1212] 1215
L1A: 
    DeRefi(_put4_1__tmp_at1190_20027);
    _put4_1__tmp_at1190_20027 = NOVALUE;
L17: 
LE: 

    /** 	return DB_OK*/
    DeRef(_key_19815);
    DeRef(_table_name_19817);
    DeRef(_key_string_19818);
    DeRef(_data_string_19819);
    DeRef(_last_part_19820);
    DeRef(_remaining_19821);
    DeRef(_key_ptr_19822);
    DeRef(_data_ptr_19823);
    DeRef(_records_ptr_19824);
    DeRef(_nrecs_19825);
    DeRef(_current_block_19826);
    DeRef(_size_19827);
    DeRef(_new_size_19828);
    DeRef(_key_location_19829);
    DeRef(_new_block_19830);
    DeRef(_index_ptr_19831);
    DeRef(_new_index_ptr_19832);
    DeRef(_total_recs_19833);
    DeRef(_11277);
    _11277 = NOVALUE;
    DeRef(_11279);
    _11279 = NOVALUE;
    return 0;
    ;
}


void _51db_replace_data(int _key_location_20162, int _data_20163, int _table_name_20164)
{
    int _11407 = NOVALUE;
    int _11406 = NOVALUE;
    int _11405 = NOVALUE;
    int _11404 = NOVALUE;
    int _11402 = NOVALUE;
    int _11401 = NOVALUE;
    int _11399 = NOVALUE;
    int _11396 = NOVALUE;
    int _11394 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_20164 == _51current_table_name_18102)
    _11394 = 1;
    else if (IS_ATOM_INT(_table_name_20164) && IS_ATOM_INT(_51current_table_name_18102))
    _11394 = 0;
    else
    _11394 = (compare(_table_name_20164, _51current_table_name_18102) == 0);
    if (_11394 != 0)
    goto L1; // [11] 49
    _11394 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_20164);
    _11396 = _51db_select_table(_table_name_20164);
    if (binary_op_a(EQUALS, _11396, 0)){
        DeRef(_11396);
        _11396 = NOVALUE;
        goto L2; // [22] 48
    }
    DeRef(_11396);
    _11396 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_20162;
    *((int *)(_2+8)) = _data_20163;
    RefDS(_table_name_20164);
    *((int *)(_2+12)) = _table_name_20164;
    _11399 = MAKE_SEQ(_1);
    RefDS(_11218);
    RefDS(_11398);
    _51fatal(903, _11218, _11398, _11399);
    _11399 = NOVALUE;

    /** 			return*/
    DeRefDS(_table_name_20164);
    return;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _51current_table_pos_18101, -1)){
        goto L3; // [53] 79
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_20162;
    *((int *)(_2+8)) = _data_20163;
    Ref(_table_name_20164);
    *((int *)(_2+12)) = _table_name_20164;
    _11401 = MAKE_SEQ(_1);
    RefDS(_11222);
    RefDS(_11398);
    _51fatal(903, _11222, _11398, _11401);
    _11401 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_20164);
    return;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _11402 = (_key_location_20162 < 1);
    if (_11402 != 0) {
        goto L4; // [85] 103
    }
    if (IS_SEQUENCE(_51key_pointers_18107)){
            _11404 = SEQ_PTR(_51key_pointers_18107)->length;
    }
    else {
        _11404 = 1;
    }
    _11405 = (_key_location_20162 > _11404);
    _11404 = NOVALUE;
    if (_11405 == 0)
    {
        DeRef(_11405);
        _11405 = NOVALUE;
        goto L5; // [99] 125
    }
    else{
        DeRef(_11405);
        _11405 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_replace_data", {key_location, data, table_name})*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _key_location_20162;
    *((int *)(_2+8)) = _data_20163;
    Ref(_table_name_20164);
    *((int *)(_2+12)) = _table_name_20164;
    _11406 = MAKE_SEQ(_1);
    RefDS(_11346);
    RefDS(_11398);
    _51fatal(905, _11346, _11398, _11406);
    _11406 = NOVALUE;

    /** 		return*/
    DeRef(_table_name_20164);
    DeRef(_11402);
    _11402 = NOVALUE;
    return;
L5: 

    /** 	db_replace_recid(key_pointers[key_location], data)*/
    _2 = (int)SEQ_PTR(_51key_pointers_18107);
    _11407 = (int)*(((s1_ptr)_2)->base + _key_location_20162);
    Ref(_11407);
    _51db_replace_recid(_11407, _data_20163);
    _11407 = NOVALUE;

    /** end procedure*/
    DeRef(_table_name_20164);
    DeRef(_11402);
    _11402 = NOVALUE;
    return;
    ;
}


int _51db_table_size(int _table_name_20186)
{
    int _11416 = NOVALUE;
    int _11415 = NOVALUE;
    int _11413 = NOVALUE;
    int _11410 = NOVALUE;
    int _11408 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_20186 == _51current_table_name_18102)
    _11408 = 1;
    else if (IS_ATOM_INT(_table_name_20186) && IS_ATOM_INT(_51current_table_name_18102))
    _11408 = 0;
    else
    _11408 = (compare(_table_name_20186, _51current_table_name_18102) == 0);
    if (_11408 != 0)
    goto L1; // [9] 46
    _11408 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_20186);
    _11410 = _51db_select_table(_table_name_20186);
    if (binary_op_a(EQUALS, _11410, 0)){
        DeRef(_11410);
        _11410 = NOVALUE;
        goto L2; // [20] 45
    }
    DeRef(_11410);
    _11410 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_table_name_20186);
    *((int *)(_2+4)) = _table_name_20186;
    _11413 = MAKE_SEQ(_1);
    RefDS(_11218);
    RefDS(_11412);
    _51fatal(903, _11218, _11412, _11413);
    _11413 = NOVALUE;

    /** 			return -1*/
    DeRefDS(_table_name_20186);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _51current_table_pos_18101, -1)){
        goto L3; // [50] 75
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_table_size", {table_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_table_name_20186);
    *((int *)(_2+4)) = _table_name_20186;
    _11415 = MAKE_SEQ(_1);
    RefDS(_11222);
    RefDS(_11412);
    _51fatal(903, _11222, _11412, _11415);
    _11415 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_20186);
    return -1;
L3: 

    /** 	return length(key_pointers)*/
    if (IS_SEQUENCE(_51key_pointers_18107)){
            _11416 = SEQ_PTR(_51key_pointers_18107)->length;
    }
    else {
        _11416 = 1;
    }
    DeRef(_table_name_20186);
    return _11416;
    ;
}


int _51db_record_data(int _key_location_20201, int _table_name_20202)
{
    int _data_ptr_20203 = NOVALUE;
    int _data_value_20204 = NOVALUE;
    int _seek_1__tmp_at134_20226 = NOVALUE;
    int _seek_inlined_seek_at_134_20225 = NOVALUE;
    int _pos_inlined_seek_at_131_20224 = NOVALUE;
    int _seek_1__tmp_at172_20233 = NOVALUE;
    int _seek_inlined_seek_at_172_20232 = NOVALUE;
    int _11431 = NOVALUE;
    int _11430 = NOVALUE;
    int _11429 = NOVALUE;
    int _11428 = NOVALUE;
    int _11427 = NOVALUE;
    int _11425 = NOVALUE;
    int _11424 = NOVALUE;
    int _11422 = NOVALUE;
    int _11419 = NOVALUE;
    int _11417 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_20201)) {
        _1 = (long)(DBL_PTR(_key_location_20201)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_20201)) && (DBL_PTR(_key_location_20201)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_20201);
        _key_location_20201 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_20202 == _51current_table_name_18102)
    _11417 = 1;
    else if (IS_ATOM_INT(_table_name_20202) && IS_ATOM_INT(_51current_table_name_18102))
    _11417 = 0;
    else
    _11417 = (compare(_table_name_20202, _51current_table_name_18102) == 0);
    if (_11417 != 0)
    goto L1; // [11] 48
    _11417 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_20202);
    _11419 = _51db_select_table(_table_name_20202);
    if (binary_op_a(EQUALS, _11419, 0)){
        DeRef(_11419);
        _11419 = NOVALUE;
        goto L2; // [22] 47
    }
    DeRef(_11419);
    _11419 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_data", {key_location, table_name})*/
    RefDS(_table_name_20202);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_20201;
    ((int *)_2)[2] = _table_name_20202;
    _11422 = MAKE_SEQ(_1);
    RefDS(_11218);
    RefDS(_11421);
    _51fatal(903, _11218, _11421, _11422);
    _11422 = NOVALUE;

    /** 			return -1*/
    DeRefDS(_table_name_20202);
    DeRef(_data_ptr_20203);
    DeRef(_data_value_20204);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _51current_table_pos_18101, -1)){
        goto L3; // [52] 77
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_20202);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_20201;
    ((int *)_2)[2] = _table_name_20202;
    _11424 = MAKE_SEQ(_1);
    RefDS(_11222);
    RefDS(_11421);
    _51fatal(903, _11222, _11421, _11424);
    _11424 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_20202);
    DeRef(_data_ptr_20203);
    DeRef(_data_value_20204);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _11425 = (_key_location_20201 < 1);
    if (_11425 != 0) {
        goto L4; // [83] 101
    }
    if (IS_SEQUENCE(_51key_pointers_18107)){
            _11427 = SEQ_PTR(_51key_pointers_18107)->length;
    }
    else {
        _11427 = 1;
    }
    _11428 = (_key_location_20201 > _11427);
    _11427 = NOVALUE;
    if (_11428 == 0)
    {
        DeRef(_11428);
        _11428 = NOVALUE;
        goto L5; // [97] 122
    }
    else{
        DeRef(_11428);
        _11428 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_data", {key_location, table_name})*/
    Ref(_table_name_20202);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_20201;
    ((int *)_2)[2] = _table_name_20202;
    _11429 = MAKE_SEQ(_1);
    RefDS(_11346);
    RefDS(_11421);
    _51fatal(905, _11346, _11421, _11429);
    _11429 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_20202);
    DeRef(_data_ptr_20203);
    DeRef(_data_value_20204);
    DeRef(_11425);
    _11425 = NOVALUE;
    return -1;
L5: 

    /** 	io:seek(current_db, key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_51key_pointers_18107);
    _11430 = (int)*(((s1_ptr)_2)->base + _key_location_20201);
    Ref(_11430);
    DeRef(_pos_inlined_seek_at_131_20224);
    _pos_inlined_seek_at_131_20224 = _11430;
    _11430 = NOVALUE;

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_pos_inlined_seek_at_131_20224);
    DeRef(_seek_1__tmp_at134_20226);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _pos_inlined_seek_at_131_20224;
    _seek_1__tmp_at134_20226 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_134_20225 = machine(19, _seek_1__tmp_at134_20226);
    DeRef(_pos_inlined_seek_at_131_20224);
    _pos_inlined_seek_at_131_20224 = NOVALUE;
    DeRef(_seek_1__tmp_at134_20226);
    _seek_1__tmp_at134_20226 = NOVALUE;

    /** 	if length(vLastErrors) > 0 then return -1 end if*/
    if (IS_SEQUENCE(_51vLastErrors_18124)){
            _11431 = SEQ_PTR(_51vLastErrors_18124)->length;
    }
    else {
        _11431 = 1;
    }
    if (_11431 <= 0)
    goto L6; // [155] 164
    DeRef(_table_name_20202);
    DeRef(_data_ptr_20203);
    DeRef(_data_value_20204);
    DeRef(_11425);
    _11425 = NOVALUE;
    return -1;
L6: 

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_20203;
    _data_ptr_20203 = _51get4();
    DeRef(_0);

    /** 	io:seek(current_db, data_ptr)*/

    /** 	return machine_func(M_SEEK, {fn, pos})*/
    Ref(_data_ptr_20203);
    DeRef(_seek_1__tmp_at172_20233);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _51current_db_18100;
    ((int *)_2)[2] = _data_ptr_20203;
    _seek_1__tmp_at172_20233 = MAKE_SEQ(_1);
    _seek_inlined_seek_at_172_20232 = machine(19, _seek_1__tmp_at172_20233);
    DeRef(_seek_1__tmp_at172_20233);
    _seek_1__tmp_at172_20233 = NOVALUE;

    /** 	data_value = decompress(0)*/
    _0 = _data_value_20204;
    _data_value_20204 = _51decompress(0);
    DeRef(_0);

    /** 	return data_value*/
    DeRef(_table_name_20202);
    DeRef(_data_ptr_20203);
    DeRef(_11425);
    _11425 = NOVALUE;
    return _data_value_20204;
    ;
}


int _51db_fetch_record(int _key_20237, int _table_name_20238)
{
    int _pos_20239 = NOVALUE;
    int _11437 = NOVALUE;
    int _0, _1, _2;
    

    /** 	pos = db_find_key(key, table_name)*/
    Ref(_key_20237);
    RefDS(_table_name_20238);
    _pos_20239 = _51db_find_key(_key_20237, _table_name_20238);
    if (!IS_ATOM_INT(_pos_20239)) {
        _1 = (long)(DBL_PTR(_pos_20239)->dbl);
        if (UNIQUE(DBL_PTR(_pos_20239)) && (DBL_PTR(_pos_20239)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos_20239);
        _pos_20239 = _1;
    }

    /** 	if pos > 0 then*/
    if (_pos_20239 <= 0)
    goto L1; // [12] 30

    /** 		return db_record_data(pos, table_name)*/
    RefDS(_table_name_20238);
    _11437 = _51db_record_data(_pos_20239, _table_name_20238);
    DeRef(_key_20237);
    DeRefDS(_table_name_20238);
    return _11437;
    goto L2; // [27] 37
L1: 

    /** 		return pos*/
    DeRef(_key_20237);
    DeRef(_table_name_20238);
    DeRef(_11437);
    _11437 = NOVALUE;
    return _pos_20239;
L2: 
    ;
}


int _51db_record_key(int _key_location_20247, int _table_name_20248)
{
    int _11452 = NOVALUE;
    int _11451 = NOVALUE;
    int _11450 = NOVALUE;
    int _11449 = NOVALUE;
    int _11448 = NOVALUE;
    int _11446 = NOVALUE;
    int _11445 = NOVALUE;
    int _11443 = NOVALUE;
    int _11440 = NOVALUE;
    int _11438 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_key_location_20247)) {
        _1 = (long)(DBL_PTR(_key_location_20247)->dbl);
        if (UNIQUE(DBL_PTR(_key_location_20247)) && (DBL_PTR(_key_location_20247)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_key_location_20247);
        _key_location_20247 = _1;
    }

    /** 	if not equal(table_name, current_table_name) then*/
    if (_table_name_20248 == _51current_table_name_18102)
    _11438 = 1;
    else if (IS_ATOM_INT(_table_name_20248) && IS_ATOM_INT(_51current_table_name_18102))
    _11438 = 0;
    else
    _11438 = (compare(_table_name_20248, _51current_table_name_18102) == 0);
    if (_11438 != 0)
    goto L1; // [11] 48
    _11438 = NOVALUE;

    /** 		if db_select_table(table_name) != DB_OK then*/
    RefDS(_table_name_20248);
    _11440 = _51db_select_table(_table_name_20248);
    if (binary_op_a(EQUALS, _11440, 0)){
        DeRef(_11440);
        _11440 = NOVALUE;
        goto L2; // [22] 47
    }
    DeRef(_11440);
    _11440 = NOVALUE;

    /** 			fatal(NO_TABLE, "invalid table name given", "db_record_key", {key_location, table_name})*/
    RefDS(_table_name_20248);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_20247;
    ((int *)_2)[2] = _table_name_20248;
    _11443 = MAKE_SEQ(_1);
    RefDS(_11218);
    RefDS(_11442);
    _51fatal(903, _11218, _11442, _11443);
    _11443 = NOVALUE;

    /** 			return -1*/
    DeRefDS(_table_name_20248);
    return -1;
L2: 
L1: 

    /** 	if current_table_pos = -1 then*/
    if (binary_op_a(NOTEQ, _51current_table_pos_18101, -1)){
        goto L3; // [52] 77
    }

    /** 		fatal(NO_TABLE, "no table selected", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_20248);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_20247;
    ((int *)_2)[2] = _table_name_20248;
    _11445 = MAKE_SEQ(_1);
    RefDS(_11222);
    RefDS(_11442);
    _51fatal(903, _11222, _11442, _11445);
    _11445 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_20248);
    return -1;
L3: 

    /** 	if key_location < 1 or key_location > length(key_pointers) then*/
    _11446 = (_key_location_20247 < 1);
    if (_11446 != 0) {
        goto L4; // [83] 101
    }
    if (IS_SEQUENCE(_51key_pointers_18107)){
            _11448 = SEQ_PTR(_51key_pointers_18107)->length;
    }
    else {
        _11448 = 1;
    }
    _11449 = (_key_location_20247 > _11448);
    _11448 = NOVALUE;
    if (_11449 == 0)
    {
        DeRef(_11449);
        _11449 = NOVALUE;
        goto L5; // [97] 122
    }
    else{
        DeRef(_11449);
        _11449 = NOVALUE;
    }
L4: 

    /** 		fatal(BAD_RECNO, "bad record number", "db_record_key", {key_location, table_name})*/
    Ref(_table_name_20248);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _key_location_20247;
    ((int *)_2)[2] = _table_name_20248;
    _11450 = MAKE_SEQ(_1);
    RefDS(_11346);
    RefDS(_11442);
    _51fatal(905, _11346, _11442, _11450);
    _11450 = NOVALUE;

    /** 		return -1*/
    DeRef(_table_name_20248);
    DeRef(_11446);
    _11446 = NOVALUE;
    return -1;
L5: 

    /** 	return key_value(key_pointers[key_location])*/
    _2 = (int)SEQ_PTR(_51key_pointers_18107);
    _11451 = (int)*(((s1_ptr)_2)->base + _key_location_20247);
    Ref(_11451);
    _11452 = _51key_value(_11451);
    _11451 = NOVALUE;
    DeRef(_table_name_20248);
    DeRef(_11446);
    _11446 = NOVALUE;
    return _11452;
    ;
}


void _51db_replace_recid(int _recid_20361, int _data_20362)
{
    int _old_size_20363 = NOVALUE;
    int _new_size_20364 = NOVALUE;
    int _data_ptr_20365 = NOVALUE;
    int _data_string_20366 = NOVALUE;
    int _put4_1__tmp_at111_20386 = NOVALUE;
    int _11551 = NOVALUE;
    int _11550 = NOVALUE;
    int _11549 = NOVALUE;
    int _11548 = NOVALUE;
    int _11547 = NOVALUE;
    int _11519 = NOVALUE;
    int _11517 = NOVALUE;
    int _11516 = NOVALUE;
    int _11515 = NOVALUE;
    int _11514 = NOVALUE;
    int _11513 = NOVALUE;
    int _11509 = NOVALUE;
    int _11508 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_recid_20361)) {
        _1 = (long)(DBL_PTR(_recid_20361)->dbl);
        if (UNIQUE(DBL_PTR(_recid_20361)) && (DBL_PTR(_recid_20361)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_recid_20361);
        _recid_20361 = _1;
    }

    /** 	seek(current_db, recid)*/
    _11551 = _16seek(_51current_db_18100, _recid_20361);
    DeRef(_11551);
    _11551 = NOVALUE;

    /** 	data_ptr = get4()*/
    _0 = _data_ptr_20365;
    _data_ptr_20365 = _51get4();
    DeRef(_0);

    /** 	seek(current_db, data_ptr-4)*/
    if (IS_ATOM_INT(_data_ptr_20365)) {
        _11508 = _data_ptr_20365 - 4;
        if ((long)((unsigned long)_11508 +(unsigned long) HIGH_BITS) >= 0){
            _11508 = NewDouble((double)_11508);
        }
    }
    else {
        _11508 = NewDouble(DBL_PTR(_data_ptr_20365)->dbl - (double)4);
    }
    _11550 = _16seek(_51current_db_18100, _11508);
    _11508 = NOVALUE;
    DeRef(_11550);
    _11550 = NOVALUE;

    /** 	old_size = get4()-4*/
    _11509 = _51get4();
    DeRef(_old_size_20363);
    if (IS_ATOM_INT(_11509)) {
        _old_size_20363 = _11509 - 4;
        if ((long)((unsigned long)_old_size_20363 +(unsigned long) HIGH_BITS) >= 0){
            _old_size_20363 = NewDouble((double)_old_size_20363);
        }
    }
    else {
        _old_size_20363 = binary_op(MINUS, _11509, 4);
    }
    DeRef(_11509);
    _11509 = NOVALUE;

    /** 	data_string = compress(data)*/
    _0 = _data_string_20366;
    _data_string_20366 = _51compress(_data_20362);
    DeRef(_0);

    /** 	new_size = length(data_string)*/
    if (IS_SEQUENCE(_data_string_20366)){
            _new_size_20364 = SEQ_PTR(_data_string_20366)->length;
    }
    else {
        _new_size_20364 = 1;
    }

    /** 	if new_size <= old_size and*/
    if (IS_ATOM_INT(_old_size_20363)) {
        _11513 = (_new_size_20364 <= _old_size_20363);
    }
    else {
        _11513 = ((double)_new_size_20364 <= DBL_PTR(_old_size_20363)->dbl);
    }
    if (_11513 == 0) {
        goto L1; // [62] 92
    }
    if (IS_ATOM_INT(_old_size_20363)) {
        _11515 = _old_size_20363 - 16;
        if ((long)((unsigned long)_11515 +(unsigned long) HIGH_BITS) >= 0){
            _11515 = NewDouble((double)_11515);
        }
    }
    else {
        _11515 = NewDouble(DBL_PTR(_old_size_20363)->dbl - (double)16);
    }
    if (IS_ATOM_INT(_11515)) {
        _11516 = (_new_size_20364 >= _11515);
    }
    else {
        _11516 = ((double)_new_size_20364 >= DBL_PTR(_11515)->dbl);
    }
    DeRef(_11515);
    _11515 = NOVALUE;
    if (_11516 == 0)
    {
        DeRef(_11516);
        _11516 = NOVALUE;
        goto L1; // [75] 92
    }
    else{
        DeRef(_11516);
        _11516 = NOVALUE;
    }

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_20365);
    _11549 = _16seek(_51current_db_18100, _data_ptr_20365);
    DeRef(_11549);
    _11549 = NOVALUE;
    goto L2; // [89] 168
L1: 

    /** 		db_free(data_ptr)*/
    Ref(_data_ptr_20365);
    _51db_free(_data_ptr_20365);

    /** 		data_ptr = db_allocate(new_size + 8)*/
    _11517 = _new_size_20364 + 8;
    if ((long)((unsigned long)_11517 + (unsigned long)HIGH_BITS) >= 0) 
    _11517 = NewDouble((double)_11517);
    _0 = _data_ptr_20365;
    _data_ptr_20365 = _51db_allocate(_11517);
    DeRef(_0);
    _11517 = NOVALUE;

    /** 		seek(current_db, recid)*/
    _11548 = _16seek(_51current_db_18100, _recid_20361);
    DeRef(_11548);
    _11548 = NOVALUE;

    /** 		put4(data_ptr)*/

    /** 	poke4(mem0, x) -- faster than doing divides etc.*/
    if (IS_ATOM_INT(_51mem0_18142)){
        poke4_addr = (unsigned long *)_51mem0_18142;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_51mem0_18142)->dbl);
    }
    if (IS_ATOM_INT(_data_ptr_20365)) {
        *poke4_addr = (unsigned long)_data_ptr_20365;
    }
    else {
        _1 = (unsigned long)DBL_PTR(_data_ptr_20365)->dbl;
        *poke4_addr = (unsigned long)_1;
    }

    /** 	puts(current_db, peek(memseq))*/
    DeRefi(_put4_1__tmp_at111_20386);
    _1 = (int)SEQ_PTR(_51memseq_18377);
    poke_addr = (unsigned char *)get_pos_int("peek", *(((s1_ptr)_1)->base+1));
    _2 = get_pos_int("peek", *(((s1_ptr)_1)->base+2));
    poke4_addr = (unsigned long *)NewS1(_2);
    _put4_1__tmp_at111_20386 = MAKE_SEQ(poke4_addr);
    poke4_addr = (unsigned long *)((s1_ptr)poke4_addr)->base;
    while (--_2 >= 0) {
        poke4_addr++;
        _1 = (int)(unsigned char)*poke_addr++;
        *(int *)poke4_addr = _1;
    }
    EPuts(_51current_db_18100, _put4_1__tmp_at111_20386); // DJP 

    /** end procedure*/
    goto L3; // [141] 144
L3: 
    DeRefi(_put4_1__tmp_at111_20386);
    _put4_1__tmp_at111_20386 = NOVALUE;

    /** 		seek(current_db, data_ptr)*/
    Ref(_data_ptr_20365);
    _11547 = _16seek(_51current_db_18100, _data_ptr_20365);
    DeRef(_11547);
    _11547 = NOVALUE;

    /** 		data_string &= repeat( 0, 8 )*/
    _11519 = Repeat(0, 8);
    Concat((object_ptr)&_data_string_20366, _data_string_20366, _11519);
    DeRefDS(_11519);
    _11519 = NOVALUE;
L2: 

    /** 	putn(data_string)*/

    /** 	puts(current_db, s)*/
    EPuts(_51current_db_18100, _data_string_20366); // DJP 

    /** end procedure*/
    goto L4; // [179] 182
L4: 

    /** end procedure*/
    DeRef(_old_size_20363);
    DeRef(_data_ptr_20365);
    DeRef(_data_string_20366);
    DeRef(_11513);
    _11513 = NOVALUE;
    return;
    ;
}



// 0xB8B008B1
