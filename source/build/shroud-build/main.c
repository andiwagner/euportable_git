// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _71GetSourceName()
{
    int _real_name_65669 = NOVALUE;
    int _fh_65670 = NOVALUE;
    int _has_extension_65672 = NOVALUE;
    int _33088 = NOVALUE;
    int _33086 = NOVALUE;
    int _33085 = NOVALUE;
    int _33082 = NOVALUE;
    int _33081 = NOVALUE;
    int _33080 = NOVALUE;
    int _33079 = NOVALUE;
    int _33078 = NOVALUE;
    int _33077 = NOVALUE;
    int _33074 = NOVALUE;
    int _33073 = NOVALUE;
    int _33071 = NOVALUE;
    int _33070 = NOVALUE;
    int _33069 = NOVALUE;
    int _33068 = NOVALUE;
    int _33067 = NOVALUE;
    int _33066 = NOVALUE;
    int _33063 = NOVALUE;
    int _33062 = NOVALUE;
    int _33060 = NOVALUE;
    int _33059 = NOVALUE;
    int _33056 = NOVALUE;
    int _0, _1, _2;
    

    /** 	boolean has_extension = FALSE*/
    _has_extension_65672 = _9FALSE_426;

    /** 	if length(src_name) = 0 then*/
    if (IS_SEQUENCE(_45src_name_49828)){
            _33056 = SEQ_PTR(_45src_name_49828)->length;
    }
    else {
        _33056 = 1;
    }
    if (_33056 != 0)
    goto L1; // [15] 30

    /** 		show_banner()*/
    _45show_banner();

    /** 		return -2 -- No source file*/
    DeRef(_real_name_65669);
    return -2;
L1: 

    /** 	ifdef WINDOWS then*/

    /** 		src_name = match_replace("/", src_name, "\\")*/
    RefDS(_24119);
    RefDS(_45src_name_49828);
    RefDS(_22990);
    _0 = _12match_replace(_24119, _45src_name_49828, _22990, 0);
    DeRefDS(_45src_name_49828);
    _45src_name_49828 = _0;

    /** 	for p = length(src_name) to 1 by -1 do*/
    if (IS_SEQUENCE(_45src_name_49828)){
            _33059 = SEQ_PTR(_45src_name_49828)->length;
    }
    else {
        _33059 = 1;
    }
    {
        int _p_65684;
        _p_65684 = _33059;
L2: 
        if (_p_65684 < 1){
            goto L3; // [52] 116
        }

        /** 		if src_name[p] = '.' then*/
        _2 = (int)SEQ_PTR(_45src_name_49828);
        _33060 = (int)*(((s1_ptr)_2)->base + _p_65684);
        if (binary_op_a(NOTEQ, _33060, 46)){
            _33060 = NOVALUE;
            goto L4; // [67] 85
        }
        _33060 = NOVALUE;

        /** 		   has_extension = TRUE*/
        _has_extension_65672 = _9TRUE_428;

        /** 		   exit*/
        goto L3; // [80] 116
        goto L5; // [82] 109
L4: 

        /** 		elsif find(src_name[p], SLASH_CHARS) then*/
        _2 = (int)SEQ_PTR(_45src_name_49828);
        _33062 = (int)*(((s1_ptr)_2)->base + _p_65684);
        _33063 = find_from(_33062, _42SLASH_CHARS_17124, 1);
        _33062 = NOVALUE;
        if (_33063 == 0)
        {
            _33063 = NOVALUE;
            goto L6; // [100] 108
        }
        else{
            _33063 = NOVALUE;
        }

        /** 		   exit*/
        goto L3; // [105] 116
L6: 
L5: 

        /** 	end for*/
        _p_65684 = _p_65684 + -1;
        goto L2; // [111] 59
L3: 
        ;
    }

    /** 	if not has_extension then*/
    if (_has_extension_65672 != 0)
    goto L7; // [118] 223

    /** 		known_files = append(known_files, "")*/
    RefDS(_22663);
    Append(&_35known_files_15596, _35known_files_15596, _22663);

    /** 		for i = 1 to length( DEFAULT_EXTS ) do*/
    _33066 = 4;
    {
        int _i_65703;
        _i_65703 = 1;
L8: 
        if (_i_65703 > 4){
            goto L9; // [138] 203
        }

        /** 			known_files[$] = src_name & DEFAULT_EXTS[i]*/
        if (IS_SEQUENCE(_35known_files_15596)){
                _33067 = SEQ_PTR(_35known_files_15596)->length;
        }
        else {
            _33067 = 1;
        }
        _2 = (int)SEQ_PTR(_42DEFAULT_EXTS_17105);
        _33068 = (int)*(((s1_ptr)_2)->base + _i_65703);
        Concat((object_ptr)&_33069, _45src_name_49828, _33068);
        _33068 = NOVALUE;
        _2 = (int)SEQ_PTR(_35known_files_15596);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35known_files_15596 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _33067);
        _1 = *(int *)_2;
        *(int *)_2 = _33069;
        if( _1 != _33069 ){
            DeRef(_1);
        }
        _33069 = NOVALUE;

        /** 			real_name = e_path_find(known_files[$])*/
        if (IS_SEQUENCE(_35known_files_15596)){
                _33070 = SEQ_PTR(_35known_files_15596)->length;
        }
        else {
            _33070 = 1;
        }
        _2 = (int)SEQ_PTR(_35known_files_15596);
        _33071 = (int)*(((s1_ptr)_2)->base + _33070);
        Ref(_33071);
        _0 = _real_name_65669;
        _real_name_65669 = _44e_path_find(_33071);
        DeRef(_0);
        _33071 = NOVALUE;

        /** 			if sequence(real_name) then*/
        _33073 = IS_SEQUENCE(_real_name_65669);
        if (_33073 == 0)
        {
            _33073 = NOVALUE;
            goto LA; // [188] 196
        }
        else{
            _33073 = NOVALUE;
        }

        /** 				exit*/
        goto L9; // [193] 203
LA: 

        /** 		end for*/
        _i_65703 = _i_65703 + 1;
        goto L8; // [198] 145
L9: 
        ;
    }

    /** 		if atom(real_name) then*/
    _33074 = IS_ATOM(_real_name_65669);
    if (_33074 == 0)
    {
        _33074 = NOVALUE;
        goto LB; // [210] 259
    }
    else{
        _33074 = NOVALUE;
    }

    /** 			return -1*/
    DeRef(_real_name_65669);
    return -1;
    goto LB; // [220] 259
L7: 

    /** 		known_files = append(known_files, src_name)*/
    RefDS(_45src_name_49828);
    Append(&_35known_files_15596, _35known_files_15596, _45src_name_49828);

    /** 		real_name = e_path_find(src_name)*/
    RefDS(_45src_name_49828);
    _0 = _real_name_65669;
    _real_name_65669 = _44e_path_find(_45src_name_49828);
    DeRef(_0);

    /** 		if atom(real_name) then*/
    _33077 = IS_ATOM(_real_name_65669);
    if (_33077 == 0)
    {
        _33077 = NOVALUE;
        goto LC; // [248] 258
    }
    else{
        _33077 = NOVALUE;
    }

    /** 			return -1*/
    DeRef(_real_name_65669);
    return -1;
LC: 
LB: 

    /** 	known_files[$] = canonical_path(real_name,,CORRECT)*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _33078 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _33078 = 1;
    }
    Ref(_real_name_65669);
    _33079 = _13canonical_path(_real_name_65669, 0, 2);
    _2 = (int)SEQ_PTR(_35known_files_15596);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35known_files_15596 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _33078);
    _1 = *(int *)_2;
    *(int *)_2 = _33079;
    if( _1 != _33079 ){
        DeRef(_1);
    }
    _33079 = NOVALUE;

    /** 	known_files_hash &= hash(known_files[$], stdhash:HSIEH32)*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _33080 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _33080 = 1;
    }
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _33081 = (int)*(((s1_ptr)_2)->base + _33080);
    _33082 = calc_hash(_33081, -5);
    _33081 = NOVALUE;
    Ref(_33082);
    Append(&_35known_files_hash_15597, _35known_files_hash_15597, _33082);
    DeRef(_33082);
    _33082 = NOVALUE;

    /** 	finished_files &= 0*/
    Append(&_35finished_files_15598, _35finished_files_15598, 0);

    /** 	file_include_depend = append( file_include_depend, { length( known_files ) } )*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _33085 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _33085 = 1;
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _33085;
    _33086 = MAKE_SEQ(_1);
    _33085 = NOVALUE;
    RefDS(_33086);
    Append(&_35file_include_depend_15599, _35file_include_depend_15599, _33086);
    DeRefDS(_33086);
    _33086 = NOVALUE;

    /** 	if file_exists(real_name) then*/
    Ref(_real_name_65669);
    _33088 = _13file_exists(_real_name_65669);
    if (_33088 == 0) {
        DeRef(_33088);
        _33088 = NOVALUE;
        goto LD; // [340] 364
    }
    else {
        if (!IS_ATOM_INT(_33088) && DBL_PTR(_33088)->dbl == 0.0){
            DeRef(_33088);
            _33088 = NOVALUE;
            goto LD; // [340] 364
        }
        DeRef(_33088);
        _33088 = NOVALUE;
    }
    DeRef(_33088);
    _33088 = NOVALUE;

    /** 		real_name = maybe_preprocess(real_name)*/
    Ref(_real_name_65669);
    _0 = _real_name_65669;
    _real_name_65669 = _66maybe_preprocess(_real_name_65669);
    DeRef(_0);

    /** 		fh = open_locked(real_name)*/
    Ref(_real_name_65669);
    _fh_65670 = _35open_locked(_real_name_65669);
    if (!IS_ATOM_INT(_fh_65670)) {
        _1 = (long)(DBL_PTR(_fh_65670)->dbl);
        if (UNIQUE(DBL_PTR(_fh_65670)) && (DBL_PTR(_fh_65670)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_fh_65670);
        _fh_65670 = _1;
    }

    /** 		return fh*/
    DeRef(_real_name_65669);
    return _fh_65670;
LD: 

    /** 	return -1*/
    DeRef(_real_name_65669);
    return -1;
    ;
}


void _71main()
{
    int _argc_65772 = NOVALUE;
    int _argv_65773 = NOVALUE;
    int _33119 = NOVALUE;
    int _33118 = NOVALUE;
    int _33115 = NOVALUE;
    int _33113 = NOVALUE;
    int _33111 = NOVALUE;
    int _33110 = NOVALUE;
    int _33109 = NOVALUE;
    int _33108 = NOVALUE;
    int _33107 = NOVALUE;
    int _33106 = NOVALUE;
    int _33105 = NOVALUE;
    int _33104 = NOVALUE;
    int _33100 = NOVALUE;
    int _0, _1, _2;
    

    /** 	argv = command_line()*/
    DeRef(_argv_65773);
    _argv_65773 = Command_Line();

    /** 	if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L1; // [9] 21
    }
    else{
    }

    /** 		argv = extract_options(argv)*/
    RefDS(_argv_65773);
    _0 = _argv_65773;
    _argv_65773 = _2extract_options(_argv_65773);
    DeRefDS(_0);
L1: 

    /** 	argc = length(argv)*/
    if (IS_SEQUENCE(_argv_65773)){
            _argc_65772 = SEQ_PTR(_argv_65773)->length;
    }
    else {
        _argc_65772 = 1;
    }

    /** 	Argv = argv*/
    RefDS(_argv_65773);
    DeRef(_38Argv_16957);
    _38Argv_16957 = _argv_65773;

    /** 	Argc = argc*/
    _38Argc_16956 = _argc_65772;

    /** 	TempErrName = "ex.err"*/
    RefDS(_33099);
    DeRefi(_46TempErrName_49480);
    _46TempErrName_49480 = _33099;

    /** 	TempWarningName = STDERR*/
    DeRef(_38TempWarningName_16960);
    _38TempWarningName_16960 = 2;

    /** 	display_warnings = 1*/
    _46display_warnings_49481 = 1;

    /** 	InitGlobals()*/
    _41InitGlobals();

    /** 	if TRANSLATE or BIND or INTERPRET then*/
    if (_38TRANSLATE_16564 != 0) {
        _33100 = 1;
        goto L2; // [69] 79
    }
    _33100 = (_38BIND_16567 != 0);
L2: 
    if (_33100 != 0) {
        goto L3; // [79] 90
    }
    if (_38INTERPRET_16561 == 0)
    {
        goto L4; // [86] 96
    }
    else{
    }
L3: 

    /** 		InitBackEnd(0)*/
    _2InitBackEnd(0);
L4: 

    /** 	src_file = GetSourceName()*/
    _0 = _71GetSourceName();
    _38src_file_17087 = _0;
    if (!IS_ATOM_INT(_38src_file_17087)) {
        _1 = (long)(DBL_PTR(_38src_file_17087)->dbl);
        if (UNIQUE(DBL_PTR(_38src_file_17087)) && (DBL_PTR(_38src_file_17087)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_38src_file_17087);
        _38src_file_17087 = _1;
    }

    /** 	if src_file = -1 then*/
    if (_38src_file_17087 != -1)
    goto L5; // [107] 181

    /** 		screen_output(STDERR, GetMsgText(51, 0, {known_files[$]}))*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _33104 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _33104 = 1;
    }
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _33105 = (int)*(((s1_ptr)_2)->base + _33104);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_33105);
    *((int *)(_2+4)) = _33105;
    _33106 = MAKE_SEQ(_1);
    _33105 = NOVALUE;
    _33107 = _47GetMsgText(51, 0, _33106);
    _33106 = NOVALUE;
    _46screen_output(2, _33107);
    _33107 = NOVALUE;

    /** 		if not batch_job and not test_only then*/
    _33108 = (_38batch_job_16959 == 0);
    if (_33108 == 0) {
        goto L6; // [145] 173
    }
    _33110 = (_38test_only_16958 == 0);
    if (_33110 == 0)
    {
        DeRef(_33110);
        _33110 = NOVALUE;
        goto L6; // [155] 173
    }
    else{
        DeRef(_33110);
        _33110 = NOVALUE;
    }

    /** 			maybe_any_key(GetMsgText(277,0), STDERR)*/
    RefDS(_22663);
    _33111 = _47GetMsgText(277, 0, _22663);
    _28maybe_any_key(_33111, 2);
    _33111 = NOVALUE;
L6: 

    /** 		Cleanup(1)*/
    _46Cleanup(1);
    goto L7; // [178] 226
L5: 

    /** 	elsif src_file >= 0 then*/
    if (_38src_file_17087 < 0)
    goto L8; // [185] 225

    /** 		main_path = known_files[$]*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _33113 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _33113 = 1;
    }
    DeRef(_38main_path_17086);
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _38main_path_17086 = (int)*(((s1_ptr)_2)->base + _33113);
    Ref(_38main_path_17086);

    /** 		if length(main_path) = 0 then*/
    if (IS_SEQUENCE(_38main_path_17086)){
            _33115 = SEQ_PTR(_38main_path_17086)->length;
    }
    else {
        _33115 = 1;
    }
    if (_33115 != 0)
    goto L9; // [209] 224

    /** 			main_path = '.' & SLASH*/
    Concat((object_ptr)&_38main_path_17086, 46, 92);
L9: 
L8: 
L7: 

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto LA; // [230] 239
    }
    else{
    }

    /** 		InitBackEnd(1)*/
    _2InitBackEnd(1);
LA: 

    /** 	CheckPlatform()*/
    _2CheckPlatform();

    /** 	InitSymTab()*/
    _55InitSymTab();

    /** 	InitEmit()*/
    _43InitEmit();

    /** 	InitLex()*/
    _62InitLex();

    /** 	InitParser()*/
    _41InitParser();

    /** 	eu_namespace()*/
    _62eu_namespace();

    /** 	ifdef TRANSLATOR then*/

    /** 	main_file()*/
    _62main_file();

    /** 	check_coverage()*/
    _52check_coverage();

    /** 	parser()*/
    _41parser();

    /** 	init_coverage()*/
    _52init_coverage();

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto LB; // [285] 296
    }
    else{
    }

    /** 		BackEnd(0) -- translate IL to C*/
    _2BackEnd(0);
    goto LC; // [293] 336
LB: 

    /** 	elsif BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto LD; // [300] 310
    }
    else{
    }

    /** 		OutputIL()*/
    _2OutputIL();
    goto LC; // [307] 336
LD: 

    /** 	elsif INTERPRET and not test_only then*/
    if (_38INTERPRET_16561 == 0) {
        goto LE; // [314] 335
    }
    _33119 = (_38test_only_16958 == 0);
    if (_33119 == 0)
    {
        DeRef(_33119);
        _33119 = NOVALUE;
        goto LE; // [324] 335
    }
    else{
        DeRef(_33119);
        _33119 = NOVALUE;
    }

    /** 		ifdef not STDDEBUG then*/

    /** 			BackEnd(0) -- execute IL using Euphoria-coded back-end*/
    _2BackEnd(0);
LE: 
LC: 

    /** 	Cleanup(0) -- does warnings*/
    _46Cleanup(0);

    /** end procedure*/
    DeRef(_argv_65773);
    DeRef(_33108);
    _33108 = NOVALUE;
    return;
    ;
}



// 0x6B7FBF66
