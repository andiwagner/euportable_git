// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _31calc_primes(int _approx_limit_11110, int _time_limit_p_11111)
{
    int _result__11112 = NOVALUE;
    int _candidate__11113 = NOVALUE;
    int _pos__11114 = NOVALUE;
    int _time_out__11115 = NOVALUE;
    int _maxp__11116 = NOVALUE;
    int _maxf__11117 = NOVALUE;
    int _maxf_idx_11118 = NOVALUE;
    int _next_trigger_11119 = NOVALUE;
    int _growth_11120 = NOVALUE;
    int _6302 = NOVALUE;
    int _6299 = NOVALUE;
    int _6297 = NOVALUE;
    int _6294 = NOVALUE;
    int _6291 = NOVALUE;
    int _6288 = NOVALUE;
    int _6281 = NOVALUE;
    int _6279 = NOVALUE;
    int _6276 = NOVALUE;
    int _6273 = NOVALUE;
    int _6269 = NOVALUE;
    int _6268 = NOVALUE;
    int _6264 = NOVALUE;
    int _6258 = NOVALUE;
    int _6256 = NOVALUE;
    int _6254 = NOVALUE;
    int _6249 = NOVALUE;
    int _6248 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if approx_limit <= list_of_primes[$] then*/
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _6248 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _6248 = 1;
    }
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _6249 = (int)*(((s1_ptr)_2)->base + _6248);
    if (binary_op_a(GREATER, _approx_limit_11110, _6249)){
        _6249 = NOVALUE;
        goto L1; // [14] 59
    }
    _6249 = NOVALUE;

    /** 		pos_ = search:binary_search(approx_limit, list_of_primes)*/
    RefDS(_31list_of_primes_11106);
    _pos__11114 = _12binary_search(_approx_limit_11110, _31list_of_primes_11106, 1, 0);
    if (!IS_ATOM_INT(_pos__11114)) {
        _1 = (long)(DBL_PTR(_pos__11114)->dbl);
        if (UNIQUE(DBL_PTR(_pos__11114)) && (DBL_PTR(_pos__11114)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pos__11114);
        _pos__11114 = _1;
    }

    /** 		if pos_ < 0 then*/
    if (_pos__11114 >= 0)
    goto L2; // [33] 45

    /** 			pos_ = (-pos_)*/
    _pos__11114 = - _pos__11114;
L2: 

    /** 		return list_of_primes[1..pos_]*/
    rhs_slice_target = (object_ptr)&_6254;
    RHS_Slice(_31list_of_primes_11106, 1, _pos__11114);
    DeRef(_result__11112);
    DeRef(_time_out__11115);
    return _6254;
L1: 

    /** 	pos_ = length(list_of_primes)*/
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _pos__11114 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _pos__11114 = 1;
    }

    /** 	candidate_ = list_of_primes[$]*/
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _6256 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _6256 = 1;
    }
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _candidate__11113 = (int)*(((s1_ptr)_2)->base + _6256);
    if (!IS_ATOM_INT(_candidate__11113))
    _candidate__11113 = (long)DBL_PTR(_candidate__11113)->dbl;

    /** 	maxf_ = floor(power(candidate_, 0.5))*/
    temp_d.dbl = (double)_candidate__11113;
    _6258 = Dpower(&temp_d, DBL_PTR(_2249));
    _maxf__11117 = unary_op(FLOOR, _6258);
    DeRefDS(_6258);
    _6258 = NOVALUE;
    if (!IS_ATOM_INT(_maxf__11117)) {
        _1 = (long)(DBL_PTR(_maxf__11117)->dbl);
        if (UNIQUE(DBL_PTR(_maxf__11117)) && (DBL_PTR(_maxf__11117)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf__11117);
        _maxf__11117 = _1;
    }

    /** 	maxf_idx = search:binary_search(maxf_, list_of_primes)*/
    RefDS(_31list_of_primes_11106);
    _maxf_idx_11118 = _12binary_search(_maxf__11117, _31list_of_primes_11106, 1, 0);
    if (!IS_ATOM_INT(_maxf_idx_11118)) {
        _1 = (long)(DBL_PTR(_maxf_idx_11118)->dbl);
        if (UNIQUE(DBL_PTR(_maxf_idx_11118)) && (DBL_PTR(_maxf_idx_11118)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_maxf_idx_11118);
        _maxf_idx_11118 = _1;
    }

    /** 	if maxf_idx < 0 then*/
    if (_maxf_idx_11118 >= 0)
    goto L3; // [103] 123

    /** 		maxf_idx = (-maxf_idx)*/
    _maxf_idx_11118 = - _maxf_idx_11118;

    /** 		maxf_ = list_of_primes[maxf_idx]*/
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _maxf__11117 = (int)*(((s1_ptr)_2)->base + _maxf_idx_11118);
    if (!IS_ATOM_INT(_maxf__11117))
    _maxf__11117 = (long)DBL_PTR(_maxf__11117)->dbl;
L3: 

    /** 	next_trigger = list_of_primes[maxf_idx+1]*/
    _6264 = _maxf_idx_11118 + 1;
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _next_trigger_11119 = (int)*(((s1_ptr)_2)->base + _6264);
    if (!IS_ATOM_INT(_next_trigger_11119))
    _next_trigger_11119 = (long)DBL_PTR(_next_trigger_11119)->dbl;

    /** 	next_trigger *= next_trigger*/
    _next_trigger_11119 = _next_trigger_11119 * _next_trigger_11119;

    /** 	growth = floor(approx_limit  / 3.5) - length(list_of_primes)*/
    _2 = binary_op(DIVIDE, _approx_limit_11110, _6267);
    _6268 = unary_op(FLOOR, _2);
    DeRef(_2);
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _6269 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _6269 = 1;
    }
    if (IS_ATOM_INT(_6268)) {
        _growth_11120 = _6268 - _6269;
    }
    else {
        _growth_11120 = NewDouble(DBL_PTR(_6268)->dbl - (double)_6269);
    }
    DeRef(_6268);
    _6268 = NOVALUE;
    _6269 = NOVALUE;
    if (!IS_ATOM_INT(_growth_11120)) {
        _1 = (long)(DBL_PTR(_growth_11120)->dbl);
        if (UNIQUE(DBL_PTR(_growth_11120)) && (DBL_PTR(_growth_11120)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_growth_11120);
        _growth_11120 = _1;
    }

    /** 	if growth <= 0 then*/
    if (_growth_11120 > 0)
    goto L4; // [162] 174

    /** 		growth = length(list_of_primes)*/
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _growth_11120 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _growth_11120 = 1;
    }
L4: 

    /** 	result_ = list_of_primes & repeat(0, growth)*/
    _6273 = Repeat(0, _growth_11120);
    Concat((object_ptr)&_result__11112, _31list_of_primes_11106, _6273);
    DeRefDS(_6273);
    _6273 = NOVALUE;

    /** 	if time_limit_p < 0 then*/
    if (_time_limit_p_11111 >= 0)
    goto L5; // [188] 203

    /** 		time_out_ = time() + 100_000_000*/
    DeRef(_6276);
    _6276 = NewDouble(current_time());
    DeRef(_time_out__11115);
    _time_out__11115 = NewDouble(DBL_PTR(_6276)->dbl + (double)100000000);
    DeRefDS(_6276);
    _6276 = NOVALUE;
    goto L6; // [200] 212
L5: 

    /** 		time_out_ = time() + time_limit_p*/
    DeRef(_6279);
    _6279 = NewDouble(current_time());
    DeRef(_time_out__11115);
    _time_out__11115 = NewDouble(DBL_PTR(_6279)->dbl + (double)_time_limit_p_11111);
    DeRefDS(_6279);
    _6279 = NOVALUE;
L6: 

    /** 	while time_out_ >= time()  label "MW" do*/
L7: 
    DeRef(_6281);
    _6281 = NewDouble(current_time());
    if (binary_op_a(LESS, _time_out__11115, _6281)){
        DeRefDS(_6281);
        _6281 = NOVALUE;
        goto L8; // [221] 370
    }
    DeRef(_6281);
    _6281 = NOVALUE;

    /** 		task_yield()*/
    task_yield();

    /** 		candidate_ += 2*/
    _candidate__11113 = _candidate__11113 + 2;

    /** 		if candidate_ >= next_trigger then*/
    if (_candidate__11113 < _next_trigger_11119)
    goto L9; // [236] 271

    /** 			maxf_idx += 1*/
    _maxf_idx_11118 = _maxf_idx_11118 + 1;

    /** 			maxf_ = result_[maxf_idx]*/
    _2 = (int)SEQ_PTR(_result__11112);
    _maxf__11117 = (int)*(((s1_ptr)_2)->base + _maxf_idx_11118);
    if (!IS_ATOM_INT(_maxf__11117))
    _maxf__11117 = (long)DBL_PTR(_maxf__11117)->dbl;

    /** 			next_trigger = result_[maxf_idx+1]*/
    _6288 = _maxf_idx_11118 + 1;
    _2 = (int)SEQ_PTR(_result__11112);
    _next_trigger_11119 = (int)*(((s1_ptr)_2)->base + _6288);
    if (!IS_ATOM_INT(_next_trigger_11119))
    _next_trigger_11119 = (long)DBL_PTR(_next_trigger_11119)->dbl;

    /** 			next_trigger *= next_trigger*/
    _next_trigger_11119 = _next_trigger_11119 * _next_trigger_11119;
L9: 

    /** 		for i = 2 to pos_ do*/
    _6291 = _pos__11114;
    {
        int _i_11173;
        _i_11173 = 2;
LA: 
        if (_i_11173 > _6291){
            goto LB; // [276] 322
        }

        /** 			maxp_ = result_[i]*/
        _2 = (int)SEQ_PTR(_result__11112);
        _maxp__11116 = (int)*(((s1_ptr)_2)->base + _i_11173);
        if (!IS_ATOM_INT(_maxp__11116))
        _maxp__11116 = (long)DBL_PTR(_maxp__11116)->dbl;

        /** 			if maxp_ > maxf_ then*/
        if (_maxp__11116 <= _maxf__11117)
        goto LC; // [291] 300

        /** 				exit*/
        goto LB; // [297] 322
LC: 

        /** 			if remainder(candidate_, maxp_) = 0 then*/
        _6294 = (_candidate__11113 % _maxp__11116);
        if (_6294 != 0)
        goto LD; // [306] 315

        /** 				continue "MW"*/
        goto L7; // [312] 217
LD: 

        /** 		end for*/
        _i_11173 = _i_11173 + 1;
        goto LA; // [317] 283
LB: 
        ;
    }

    /** 		pos_ += 1*/
    _pos__11114 = _pos__11114 + 1;

    /** 		if pos_ >= length(result_) then*/
    if (IS_SEQUENCE(_result__11112)){
            _6297 = SEQ_PTR(_result__11112)->length;
    }
    else {
        _6297 = 1;
    }
    if (_pos__11114 < _6297)
    goto LE; // [333] 348

    /** 			result_ &= repeat(0, 1000)*/
    _6299 = Repeat(0, 1000);
    Concat((object_ptr)&_result__11112, _result__11112, _6299);
    DeRefDS(_6299);
    _6299 = NOVALUE;
LE: 

    /** 		result_[pos_] = candidate_*/
    _2 = (int)SEQ_PTR(_result__11112);
    _2 = (int)(((s1_ptr)_2)->base + _pos__11114);
    _1 = *(int *)_2;
    *(int *)_2 = _candidate__11113;
    DeRef(_1);

    /** 		if candidate_ >= approx_limit then*/
    if (_candidate__11113 < _approx_limit_11110)
    goto L7; // [356] 217

    /** 			exit*/
    goto L8; // [362] 370

    /** 	end while*/
    goto L7; // [367] 217
L8: 

    /** 	return result_[1..pos_]*/
    rhs_slice_target = (object_ptr)&_6302;
    RHS_Slice(_result__11112, 1, _pos__11114);
    DeRefDS(_result__11112);
    DeRef(_time_out__11115);
    DeRef(_6254);
    _6254 = NOVALUE;
    DeRef(_6264);
    _6264 = NOVALUE;
    DeRef(_6288);
    _6288 = NOVALUE;
    DeRef(_6294);
    _6294 = NOVALUE;
    return _6302;
    ;
}


int _31next_prime(int _n_11192, int _fail_signal_p_11193, int _time_out_p_11194)
{
    int _i_11195 = NOVALUE;
    int _6322 = NOVALUE;
    int _6316 = NOVALUE;
    int _6315 = NOVALUE;
    int _6314 = NOVALUE;
    int _6313 = NOVALUE;
    int _6312 = NOVALUE;
    int _6309 = NOVALUE;
    int _6308 = NOVALUE;
    int _6305 = NOVALUE;
    int _6304 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if n < 0 then*/
    if (_n_11192 >= 0)
    goto L1; // [5] 16

    /** 		return fail_signal_p*/
    return _fail_signal_p_11193;
L1: 

    /** 	if list_of_primes[$] < n then*/
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _6304 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _6304 = 1;
    }
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _6305 = (int)*(((s1_ptr)_2)->base + _6304);
    if (binary_op_a(GREATEREQ, _6305, _n_11192)){
        _6305 = NOVALUE;
        goto L2; // [27] 41
    }
    _6305 = NOVALUE;

    /** 		list_of_primes = calc_primes(n,time_out_p)*/
    _0 = _31calc_primes(_n_11192, _time_out_p_11194);
    DeRefDS(_31list_of_primes_11106);
    _31list_of_primes_11106 = _0;
L2: 

    /** 	if n > list_of_primes[$] then*/
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _6308 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _6308 = 1;
    }
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _6309 = (int)*(((s1_ptr)_2)->base + _6308);
    if (binary_op_a(LESSEQ, _n_11192, _6309)){
        _6309 = NOVALUE;
        goto L3; // [52] 63
    }
    _6309 = NOVALUE;

    /** 		return fail_signal_p*/
    return _fail_signal_p_11193;
L3: 

    /** 	if n < 1009 and 1009 <= list_of_primes[$] then*/
    _6312 = (_n_11192 < 1009);
    if (_6312 == 0) {
        goto L4; // [69] 106
    }
    if (IS_SEQUENCE(_31list_of_primes_11106)){
            _6314 = SEQ_PTR(_31list_of_primes_11106)->length;
    }
    else {
        _6314 = 1;
    }
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _6315 = (int)*(((s1_ptr)_2)->base + _6314);
    if (IS_ATOM_INT(_6315)) {
        _6316 = (1009 <= _6315);
    }
    else {
        _6316 = binary_op(LESSEQ, 1009, _6315);
    }
    _6315 = NOVALUE;
    if (_6316 == 0) {
        DeRef(_6316);
        _6316 = NOVALUE;
        goto L4; // [87] 106
    }
    else {
        if (!IS_ATOM_INT(_6316) && DBL_PTR(_6316)->dbl == 0.0){
            DeRef(_6316);
            _6316 = NOVALUE;
            goto L4; // [87] 106
        }
        DeRef(_6316);
        _6316 = NOVALUE;
    }
    DeRef(_6316);
    _6316 = NOVALUE;

    /** 		i = search:binary_search(n, list_of_primes, ,169)*/
    RefDS(_31list_of_primes_11106);
    _i_11195 = _12binary_search(_n_11192, _31list_of_primes_11106, 1, 169);
    if (!IS_ATOM_INT(_i_11195)) {
        _1 = (long)(DBL_PTR(_i_11195)->dbl);
        if (UNIQUE(DBL_PTR(_i_11195)) && (DBL_PTR(_i_11195)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_11195);
        _i_11195 = _1;
    }
    goto L5; // [103] 120
L4: 

    /** 		i = search:binary_search(n, list_of_primes)*/
    RefDS(_31list_of_primes_11106);
    _i_11195 = _12binary_search(_n_11192, _31list_of_primes_11106, 1, 0);
    if (!IS_ATOM_INT(_i_11195)) {
        _1 = (long)(DBL_PTR(_i_11195)->dbl);
        if (UNIQUE(DBL_PTR(_i_11195)) && (DBL_PTR(_i_11195)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_i_11195);
        _i_11195 = _1;
    }
L5: 

    /** 	if i < 0 then*/
    if (_i_11195 >= 0)
    goto L6; // [124] 136

    /** 		i = (-i)*/
    _i_11195 = - _i_11195;
L6: 

    /** 	return list_of_primes[i]*/
    _2 = (int)SEQ_PTR(_31list_of_primes_11106);
    _6322 = (int)*(((s1_ptr)_2)->base + _i_11195);
    Ref(_6322);
    DeRef(_fail_signal_p_11193);
    DeRef(_6312);
    _6312 = NOVALUE;
    return _6322;
    ;
}



// 0xA1A9A359
