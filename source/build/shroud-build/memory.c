// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _6positive_int(int _x_307)
{
    int _51 = NOVALUE;
    int _49 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not integer(x) then*/
    if (IS_ATOM_INT(_x_307))
    _49 = 1;
    else if (IS_ATOM_DBL(_x_307))
    _49 = IS_ATOM_INT(DoubleToInt(_x_307));
    else
    _49 = 0;
    if (_49 != 0)
    goto L1; // [6] 16
    _49 = NOVALUE;

    /** 		return 0*/
    DeRef(_x_307);
    return 0;
L1: 

    /**     return x >= 1*/
    if (IS_ATOM_INT(_x_307)) {
        _51 = (_x_307 >= 1);
    }
    else {
        _51 = binary_op(GREATEREQ, _x_307, 1);
    }
    DeRef(_x_307);
    return _51;
    ;
}


void _6deallocate(int _addr_329)
{
    int _0, _1, _2;
    

    /** 	ifdef DATA_EXECUTE and WINDOWS then*/

    /**    	machine_proc( memconst:M_FREE, addr)*/
    machine(17, _addr_329);

    /** end procedure*/
    DeRef(_addr_329);
    return;
    ;
}


int _6prepare_block(int _addr_355, int _a_356, int _protection_357)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_a_356)) {
        _1 = (long)(DBL_PTR(_a_356)->dbl);
        if (UNIQUE(DBL_PTR(_a_356)) && (DBL_PTR(_a_356)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_a_356);
        _a_356 = _1;
    }
    if (!IS_ATOM_INT(_protection_357)) {
        _1 = (long)(DBL_PTR(_protection_357)->dbl);
        if (UNIQUE(DBL_PTR(_protection_357)) && (DBL_PTR(_protection_357)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_protection_357);
        _protection_357 = _1;
    }

    /** 	return addr*/
    return _addr_355;
    ;
}


int _6bordered_address(int _addr_366)
{
    int _66 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not atom(addr) then*/
    _66 = IS_ATOM(_addr_366);
    if (_66 != 0)
    goto L1; // [6] 16
    _66 = NOVALUE;

    /** 		return 0*/
    DeRef(_addr_366);
    return 0;
L1: 

    /** 	return 1*/
    DeRef(_addr_366);
    return 1;
    ;
}


int _6dep_works()
{
    int _68 = NOVALUE;
    int _0, _1, _2;
    

    /** 	ifdef WINDOWS then*/

    /** 		return (DEP_really_works and use_DEP)*/
    _68 = (_5DEP_really_works_271 != 0 && 1 != 0);
    return _68;

    /** 	return 1*/
    _68 = NOVALUE;
    return 1;
    ;
}



// 0xDCCE45A1
