// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _66is_file_newer(int _f1_24419, int _f2_24420)
{
    int _d1_24421 = NOVALUE;
    int _d2_24424 = NOVALUE;
    int _diff_2__tmp_at33_24433 = NOVALUE;
    int _diff_1__tmp_at33_24432 = NOVALUE;
    int _diff_inlined_diff_at_33_24431 = NOVALUE;
    int _14199 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object d1 = file_timestamp(f1)*/
    RefDS(_f1_24419);
    _0 = _d1_24421;
    _d1_24421 = _13file_timestamp(_f1_24419);
    DeRef(_0);

    /** 	object d2 = file_timestamp(f2)*/
    RefDS(_f2_24420);
    _0 = _d2_24424;
    _d2_24424 = _13file_timestamp(_f2_24420);
    DeRef(_0);

    /** 	if atom(d2) then return 1 end if*/
    _14199 = IS_ATOM(_d2_24424);
    if (_14199 == 0)
    {
        _14199 = NOVALUE;
        goto L1; // [22] 30
    }
    else{
        _14199 = NOVALUE;
    }
    DeRefDS(_f1_24419);
    DeRefDS(_f2_24420);
    DeRef(_d1_24421);
    DeRef(_d2_24424);
    return 1;
L1: 

    /** 	if dt:diff(d1, d2) < 0 then*/

    /** 	return datetimeToSeconds(dt2) - datetimeToSeconds(dt1)*/
    Ref(_d2_24424);
    _0 = _diff_1__tmp_at33_24432;
    _diff_1__tmp_at33_24432 = _14datetimeToSeconds(_d2_24424);
    DeRef(_0);
    Ref(_d1_24421);
    _0 = _diff_2__tmp_at33_24433;
    _diff_2__tmp_at33_24433 = _14datetimeToSeconds(_d1_24421);
    DeRef(_0);
    DeRef(_diff_inlined_diff_at_33_24431);
    if (IS_ATOM_INT(_diff_1__tmp_at33_24432) && IS_ATOM_INT(_diff_2__tmp_at33_24433)) {
        _diff_inlined_diff_at_33_24431 = _diff_1__tmp_at33_24432 - _diff_2__tmp_at33_24433;
        if ((long)((unsigned long)_diff_inlined_diff_at_33_24431 +(unsigned long) HIGH_BITS) >= 0){
            _diff_inlined_diff_at_33_24431 = NewDouble((double)_diff_inlined_diff_at_33_24431);
        }
    }
    else {
        _diff_inlined_diff_at_33_24431 = binary_op(MINUS, _diff_1__tmp_at33_24432, _diff_2__tmp_at33_24433);
    }
    DeRef(_diff_1__tmp_at33_24432);
    _diff_1__tmp_at33_24432 = NOVALUE;
    DeRef(_diff_2__tmp_at33_24433);
    _diff_2__tmp_at33_24433 = NOVALUE;
    if (binary_op_a(GREATEREQ, _diff_inlined_diff_at_33_24431, 0)){
        goto L2; // [49] 60
    }

    /** 		return 1*/
    DeRefDS(_f1_24419);
    DeRefDS(_f2_24420);
    DeRef(_d1_24421);
    DeRef(_d2_24424);
    return 1;
L2: 

    /** 	return 0*/
    DeRefDS(_f1_24419);
    DeRefDS(_f2_24420);
    DeRef(_d1_24421);
    DeRef(_d2_24424);
    return 0;
    ;
}


void _66add_preprocessor(int _file_ext_24437, int _command_24438, int _params_24439)
{
    int _tmp_24442 = NOVALUE;
    int _file_exts_24452 = NOVALUE;
    int _exts_24458 = NOVALUE;
    int _14216 = NOVALUE;
    int _14215 = NOVALUE;
    int _14214 = NOVALUE;
    int _14213 = NOVALUE;
    int _14211 = NOVALUE;
    int _14206 = NOVALUE;
    int _14201 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(command) then*/
    _14201 = 1;
    if (_14201 == 0)
    {
        _14201 = NOVALUE;
        goto L1; // [8] 53
    }
    else{
        _14201 = NOVALUE;
    }

    /** 		sequence tmp = split( file_ext, ":")*/
    RefDS(_file_ext_24437);
    RefDS(_14202);
    _0 = _tmp_24442;
    _tmp_24442 = _21split(_file_ext_24437, _14202, 0, 0);
    DeRef(_0);

    /** 		file_ext = tmp[1]*/
    DeRefDS(_file_ext_24437);
    _2 = (int)SEQ_PTR(_tmp_24442);
    _file_ext_24437 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_file_ext_24437);

    /** 		command = tmp[2]*/
    _2 = (int)SEQ_PTR(_tmp_24442);
    _command_24438 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_command_24438);

    /** 		if length(tmp) >= 3 then*/
    if (IS_SEQUENCE(_tmp_24442)){
            _14206 = SEQ_PTR(_tmp_24442)->length;
    }
    else {
        _14206 = 1;
    }
    if (_14206 < 3)
    goto L2; // [41] 52

    /** 			params = tmp[3]*/
    _2 = (int)SEQ_PTR(_tmp_24442);
    _params_24439 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_params_24439);
L2: 
L1: 
    DeRef(_tmp_24442);
    _tmp_24442 = NOVALUE;

    /** 	sequence file_exts = split( file_ext, "," )*/
    RefDS(_file_ext_24437);
    RefDS(_14209);
    _0 = _file_exts_24452;
    _file_exts_24452 = _21split(_file_ext_24437, _14209, 0, 0);
    DeRef(_0);

    /** 	if atom(params) then*/
    _14211 = IS_ATOM(_params_24439);
    if (_14211 == 0)
    {
        _14211 = NOVALUE;
        goto L3; // [71] 80
    }
    else{
        _14211 = NOVALUE;
    }

    /** 		params = ""*/
    RefDS(_5);
    DeRef(_params_24439);
    _params_24439 = _5;
L3: 

    /** 	sequence exts = split(file_ext, ",")*/
    RefDS(_file_ext_24437);
    RefDS(_14209);
    _0 = _exts_24458;
    _exts_24458 = _21split(_file_ext_24437, _14209, 0, 0);
    DeRef(_0);

    /** 	for i = 1 to length(exts) do*/
    if (IS_SEQUENCE(_exts_24458)){
            _14213 = SEQ_PTR(_exts_24458)->length;
    }
    else {
        _14213 = 1;
    }
    {
        int _i_24462;
        _i_24462 = 1;
L4: 
        if (_i_24462 > _14213){
            goto L5; // [96] 135
        }

        /** 		preprocessors &= { { exts[i], command, params, -1 } }*/
        _2 = (int)SEQ_PTR(_exts_24458);
        _14214 = (int)*(((s1_ptr)_2)->base + _i_24462);
        _1 = NewS1(4);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_14214);
        *((int *)(_2+4)) = _14214;
        Ref(_command_24438);
        *((int *)(_2+8)) = _command_24438;
        Ref(_params_24439);
        *((int *)(_2+12)) = _params_24439;
        *((int *)(_2+16)) = -1;
        _14215 = MAKE_SEQ(_1);
        _14214 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _14215;
        _14216 = MAKE_SEQ(_1);
        _14215 = NOVALUE;
        Concat((object_ptr)&_35preprocessors_15612, _35preprocessors_15612, _14216);
        DeRefDS(_14216);
        _14216 = NOVALUE;

        /** 	end for*/
        _i_24462 = _i_24462 + 1;
        goto L4; // [130] 103
L5: 
        ;
    }

    /** end procedure */
    DeRefDS(_file_ext_24437);
    DeRef(_command_24438);
    DeRef(_params_24439);
    DeRef(_file_exts_24452);
    DeRef(_exts_24458);
    return;
    ;
}


int _66maybe_preprocess(int _fname_24471)
{
    int _pp_24472 = NOVALUE;
    int _pp_id_24473 = NOVALUE;
    int _fext_24477 = NOVALUE;
    int _post_fname_24494 = NOVALUE;
    int _rid_24522 = NOVALUE;
    int _dll_id_24526 = NOVALUE;
    int _public_cmd_args_24562 = NOVALUE;
    int _cmd_args_24565 = NOVALUE;
    int _cmd_24594 = NOVALUE;
    int _pcmd_24599 = NOVALUE;
    int _result_24604 = NOVALUE;
    int _14295 = NOVALUE;
    int _14294 = NOVALUE;
    int _14289 = NOVALUE;
    int _14288 = NOVALUE;
    int _14286 = NOVALUE;
    int _14285 = NOVALUE;
    int _14283 = NOVALUE;
    int _14281 = NOVALUE;
    int _14280 = NOVALUE;
    int _14278 = NOVALUE;
    int _14275 = NOVALUE;
    int _14273 = NOVALUE;
    int _14271 = NOVALUE;
    int _14269 = NOVALUE;
    int _14268 = NOVALUE;
    int _14266 = NOVALUE;
    int _14265 = NOVALUE;
    int _14263 = NOVALUE;
    int _14260 = NOVALUE;
    int _14259 = NOVALUE;
    int _14258 = NOVALUE;
    int _14256 = NOVALUE;
    int _14252 = NOVALUE;
    int _14250 = NOVALUE;
    int _14249 = NOVALUE;
    int _14248 = NOVALUE;
    int _14244 = NOVALUE;
    int _14241 = NOVALUE;
    int _14240 = NOVALUE;
    int _14239 = NOVALUE;
    int _14237 = NOVALUE;
    int _14234 = NOVALUE;
    int _14232 = NOVALUE;
    int _14231 = NOVALUE;
    int _14229 = NOVALUE;
    int _14227 = NOVALUE;
    int _14225 = NOVALUE;
    int _14223 = NOVALUE;
    int _14222 = NOVALUE;
    int _14221 = NOVALUE;
    int _14220 = NOVALUE;
    int _14218 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	sequence pp = {}*/
    RefDS(_5);
    DeRef(_pp_24472);
    _pp_24472 = _5;

    /** 	if length(preprocessors) then*/
    if (IS_SEQUENCE(_35preprocessors_15612)){
            _14218 = SEQ_PTR(_35preprocessors_15612)->length;
    }
    else {
        _14218 = 1;
    }
    if (_14218 == 0)
    {
        _14218 = NOVALUE;
        goto L1; // [17] 89
    }
    else{
        _14218 = NOVALUE;
    }

    /** 		sequence fext = fileext(fname)*/
    RefDS(_fname_24471);
    _0 = _fext_24477;
    _fext_24477 = _13fileext(_fname_24471);
    DeRef(_0);

    /** 		for i = 1 to length(preprocessors) do*/
    if (IS_SEQUENCE(_35preprocessors_15612)){
            _14220 = SEQ_PTR(_35preprocessors_15612)->length;
    }
    else {
        _14220 = 1;
    }
    {
        int _i_24481;
        _i_24481 = 1;
L2: 
        if (_i_24481 > _14220){
            goto L3; // [35] 88
        }

        /** 			if equal(fext, preprocessors[i][1]) then*/
        _2 = (int)SEQ_PTR(_35preprocessors_15612);
        _14221 = (int)*(((s1_ptr)_2)->base + _i_24481);
        _2 = (int)SEQ_PTR(_14221);
        _14222 = (int)*(((s1_ptr)_2)->base + 1);
        _14221 = NOVALUE;
        if (_fext_24477 == _14222)
        _14223 = 1;
        else if (IS_ATOM_INT(_fext_24477) && IS_ATOM_INT(_14222))
        _14223 = 0;
        else
        _14223 = (compare(_fext_24477, _14222) == 0);
        _14222 = NOVALUE;
        if (_14223 == 0)
        {
            _14223 = NOVALUE;
            goto L4; // [58] 81
        }
        else{
            _14223 = NOVALUE;
        }

        /** 				pp_id = i*/
        _pp_id_24473 = _i_24481;

        /** 				pp = preprocessors[pp_id]*/
        DeRef(_pp_24472);
        _2 = (int)SEQ_PTR(_35preprocessors_15612);
        _pp_24472 = (int)*(((s1_ptr)_2)->base + _pp_id_24473);
        RefDS(_pp_24472);

        /** 				exit*/
        goto L3; // [78] 88
L4: 

        /** 		end for*/
        _i_24481 = _i_24481 + 1;
        goto L2; // [83] 42
L3: 
        ;
    }
L1: 
    DeRef(_fext_24477);
    _fext_24477 = NOVALUE;

    /** 	if length(pp) = 0 then */
    if (IS_SEQUENCE(_pp_24472)){
            _14225 = SEQ_PTR(_pp_24472)->length;
    }
    else {
        _14225 = 1;
    }
    if (_14225 != 0)
    goto L5; // [96] 107

    /** 		return fname*/
    DeRefDS(_pp_24472);
    DeRef(_post_fname_24494);
    return _fname_24471;
L5: 

    /** 	sequence post_fname = filebase(fname) & ".pp." & fileext(fname)*/
    RefDS(_fname_24471);
    _14227 = _13filebase(_fname_24471);
    RefDS(_fname_24471);
    _14229 = _13fileext(_fname_24471);
    {
        int concat_list[3];

        concat_list[0] = _14229;
        concat_list[1] = _14228;
        concat_list[2] = _14227;
        Concat_N((object_ptr)&_post_fname_24494, concat_list, 3);
    }
    DeRef(_14229);
    _14229 = NOVALUE;
    DeRef(_14227);
    _14227 = NOVALUE;

    /** 	if length(dirname(fname)) > 0 then*/
    RefDS(_fname_24471);
    _14231 = _13dirname(_fname_24471, 0);
    if (IS_SEQUENCE(_14231)){
            _14232 = SEQ_PTR(_14231)->length;
    }
    else {
        _14232 = 1;
    }
    DeRef(_14231);
    _14231 = NOVALUE;
    if (_14232 <= 0)
    goto L6; // [133] 153

    /** 		post_fname = dirname(fname) & SLASH & post_fname*/
    RefDS(_fname_24471);
    _14234 = _13dirname(_fname_24471, 0);
    {
        int concat_list[3];

        concat_list[0] = _post_fname_24494;
        concat_list[1] = 92;
        concat_list[2] = _14234;
        Concat_N((object_ptr)&_post_fname_24494, concat_list, 3);
    }
    DeRef(_14234);
    _14234 = NOVALUE;
L6: 

    /** 	if not force_preprocessor then*/
    if (_35force_preprocessor_15613 != 0)
    goto L7; // [157] 178

    /** 		if not is_file_newer(fname, post_fname) then*/
    RefDS(_fname_24471);
    RefDS(_post_fname_24494);
    _14237 = _66is_file_newer(_fname_24471, _post_fname_24494);
    if (IS_ATOM_INT(_14237)) {
        if (_14237 != 0){
            DeRef(_14237);
            _14237 = NOVALUE;
            goto L8; // [167] 177
        }
    }
    else {
        if (DBL_PTR(_14237)->dbl != 0.0){
            DeRef(_14237);
            _14237 = NOVALUE;
            goto L8; // [167] 177
        }
    }
    DeRef(_14237);
    _14237 = NOVALUE;

    /** 			return post_fname*/
    DeRefDS(_fname_24471);
    DeRef(_pp_24472);
    _14231 = NOVALUE;
    return _post_fname_24494;
L8: 
L7: 

    /** 	if equal(fileext(pp[PP_COMMAND]), SHARED_LIB_EXT) then*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _14239 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14239);
    _14240 = _13fileext(_14239);
    _14239 = NOVALUE;
    if (_14240 == _13SHARED_LIB_EXT_7011)
    _14241 = 1;
    else if (IS_ATOM_INT(_14240) && IS_ATOM_INT(_13SHARED_LIB_EXT_7011))
    _14241 = 0;
    else
    _14241 = (compare(_14240, _13SHARED_LIB_EXT_7011) == 0);
    DeRef(_14240);
    _14240 = NOVALUE;
    if (_14241 == 0)
    {
        _14241 = NOVALUE;
        goto L9; // [196] 360
    }
    else{
        _14241 = NOVALUE;
    }

    /** 		integer rid = pp[PP_RID]*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _rid_24522 = (int)*(((s1_ptr)_2)->base + 4);
    if (!IS_ATOM_INT(_rid_24522))
    _rid_24522 = (long)DBL_PTR(_rid_24522)->dbl;

    /** 		if rid = -1 then*/
    if (_rid_24522 != -1)
    goto LA; // [209] 317

    /** 			integer dll_id = open_dll(pp[PP_COMMAND])*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _14244 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14244);
    _dll_id_24526 = _7open_dll(_14244);
    _14244 = NOVALUE;
    if (!IS_ATOM_INT(_dll_id_24526)) {
        _1 = (long)(DBL_PTR(_dll_id_24526)->dbl);
        if (UNIQUE(DBL_PTR(_dll_id_24526)) && (DBL_PTR(_dll_id_24526)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_dll_id_24526);
        _dll_id_24526 = _1;
    }

    /** 			if dll_id = -1 then*/
    if (_dll_id_24526 != -1)
    goto LB; // [229] 255

    /** 				CompileErr(sprintf("Preprocessor shared library '%s' could not be loaded\n",*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _14248 = (int)*(((s1_ptr)_2)->base + 2);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_14248);
    *((int *)(_2+4)) = _14248;
    _14249 = MAKE_SEQ(_1);
    _14248 = NOVALUE;
    _14250 = EPrintf(-9999999, _14247, _14249);
    DeRefDS(_14249);
    _14249 = NOVALUE;
    RefDS(_22663);
    _46CompileErr(_14250, _22663, 1);
    _14250 = NOVALUE;
LB: 

    /** 			rid = define_c_func(dll_id, "preprocess", { E_SEQUENCE, E_SEQUENCE, E_SEQUENCE }, */
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 134217732;
    *((int *)(_2+8)) = 134217732;
    *((int *)(_2+12)) = 134217732;
    _14252 = MAKE_SEQ(_1);
    RefDS(_14251);
    _rid_24522 = _7define_c_func(_dll_id_24526, _14251, _14252, 100663300);
    _14252 = NOVALUE;
    if (!IS_ATOM_INT(_rid_24522)) {
        _1 = (long)(DBL_PTR(_rid_24522)->dbl);
        if (UNIQUE(DBL_PTR(_rid_24522)) && (DBL_PTR(_rid_24522)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_rid_24522);
        _rid_24522 = _1;
    }

    /** 			if rid = -1 then*/
    if (_rid_24522 != -1)
    goto LC; // [282] 299

    /** 				CompileErr("Preprocessor entry point cound not be found\n",,1)*/
    RefDS(_14255);
    RefDS(_22663);
    _46CompileErr(_14255, _22663, 1);

    /** 				Cleanup(1)*/
    _46Cleanup(1);
LC: 

    /** 			preprocessors[pp_id][PP_RID] = rid*/
    _2 = (int)SEQ_PTR(_35preprocessors_15612);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35preprocessors_15612 = MAKE_SEQ(_2);
    }
    _3 = (int)(_pp_id_24473 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 4);
    _1 = *(int *)_2;
    *(int *)_2 = _rid_24522;
    DeRef(_1);
    _14256 = NOVALUE;
LA: 

    /** 		if c_func(rid, { fname, post_fname, pp[PP_PARAMS] }) != 0 then*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _14258 = (int)*(((s1_ptr)_2)->base + 3);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_fname_24471);
    *((int *)(_2+4)) = _fname_24471;
    RefDS(_post_fname_24494);
    *((int *)(_2+8)) = _post_fname_24494;
    Ref(_14258);
    *((int *)(_2+12)) = _14258;
    _14259 = MAKE_SEQ(_1);
    _14258 = NOVALUE;
    _14260 = call_c(1, _rid_24522, _14259);
    DeRefDS(_14259);
    _14259 = NOVALUE;
    if (binary_op_a(EQUALS, _14260, 0)){
        DeRef(_14260);
        _14260 = NOVALUE;
        goto LD; // [338] 355
    }
    DeRef(_14260);
    _14260 = NOVALUE;

    /** 			CompileErr("Preprocessor call failed\n",,1)*/
    RefDS(_14262);
    RefDS(_22663);
    _46CompileErr(_14262, _22663, 1);

    /** 			Cleanup(1)*/
    _46Cleanup(1);
LD: 
    goto LE; // [357] 542
L9: 

    /** 		sequence public_cmd_args = {pp[PP_COMMAND]}*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _14263 = (int)*(((s1_ptr)_2)->base + 2);
    _0 = _public_cmd_args_24562;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_14263);
    *((int *)(_2+4)) = _14263;
    _public_cmd_args_24562 = MAKE_SEQ(_1);
    DeRef(_0);
    _14263 = NOVALUE;

    /** 		sequence cmd_args = {canonical_path(pp[PP_COMMAND],,TO_SHORT)}*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _14265 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14265);
    _14266 = _13canonical_path(_14265, 0, 4);
    _14265 = NOVALUE;
    _0 = _cmd_args_24565;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _14266;
    _cmd_args_24565 = MAKE_SEQ(_1);
    DeRef(_0);
    _14266 = NOVALUE;

    /** 		if equal(fileext(pp[PP_COMMAND]), "ex") then*/
    _2 = (int)SEQ_PTR(_pp_24472);
    _14268 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_14268);
    _14269 = _13fileext(_14268);
    _14268 = NOVALUE;
    if (_14269 == _14270)
    _14271 = 1;
    else if (IS_ATOM_INT(_14269) && IS_ATOM_INT(_14270))
    _14271 = 0;
    else
    _14271 = (compare(_14269, _14270) == 0);
    DeRef(_14269);
    _14269 = NOVALUE;
    if (_14271 == 0)
    {
        _14271 = NOVALUE;
        goto LF; // [408] 432
    }
    else{
        _14271 = NOVALUE;
    }

    /** 			public_cmd_args = { "eui" } & public_cmd_args*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14272);
    *((int *)(_2+4)) = _14272;
    _14273 = MAKE_SEQ(_1);
    Concat((object_ptr)&_public_cmd_args_24562, _14273, _public_cmd_args_24562);
    DeRefDS(_14273);
    _14273 = NOVALUE;
    DeRef(_14273);
    _14273 = NOVALUE;

    /** 			cmd_args = { "eui" } & cmd_args*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14272);
    *((int *)(_2+4)) = _14272;
    _14275 = MAKE_SEQ(_1);
    Concat((object_ptr)&_cmd_args_24565, _14275, _cmd_args_24565);
    DeRefDS(_14275);
    _14275 = NOVALUE;
    DeRef(_14275);
    _14275 = NOVALUE;
LF: 

    /** 		cmd_args &= { "-i", canonical_path(fname,,TO_SHORT), "-o", canonical_path(post_fname,,TO_SHORT) }*/
    RefDS(_fname_24471);
    _14278 = _13canonical_path(_fname_24471, 0, 4);
    RefDS(_post_fname_24494);
    _14280 = _13canonical_path(_post_fname_24494, 0, 4);
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14277);
    *((int *)(_2+4)) = _14277;
    *((int *)(_2+8)) = _14278;
    RefDS(_14279);
    *((int *)(_2+12)) = _14279;
    *((int *)(_2+16)) = _14280;
    _14281 = MAKE_SEQ(_1);
    _14280 = NOVALUE;
    _14278 = NOVALUE;
    Concat((object_ptr)&_cmd_args_24565, _cmd_args_24565, _14281);
    DeRefDS(_14281);
    _14281 = NOVALUE;

    /** 		public_cmd_args &= { "-i", fname, "-o", post_fname }*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_14277);
    *((int *)(_2+4)) = _14277;
    RefDS(_fname_24471);
    *((int *)(_2+8)) = _fname_24471;
    RefDS(_14279);
    *((int *)(_2+12)) = _14279;
    RefDS(_post_fname_24494);
    *((int *)(_2+16)) = _post_fname_24494;
    _14283 = MAKE_SEQ(_1);
    Concat((object_ptr)&_public_cmd_args_24562, _public_cmd_args_24562, _14283);
    DeRefDS(_14283);
    _14283 = NOVALUE;

    /** 		sequence cmd = build_commandline( cmd_args ) & pp[PP_PARAMS]*/
    RefDS(_cmd_args_24565);
    _14285 = _27build_commandline(_cmd_args_24565);
    _2 = (int)SEQ_PTR(_pp_24472);
    _14286 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_14285) && IS_ATOM(_14286)) {
        Ref(_14286);
        Append(&_cmd_24594, _14285, _14286);
    }
    else if (IS_ATOM(_14285) && IS_SEQUENCE(_14286)) {
        Ref(_14285);
        Prepend(&_cmd_24594, _14286, _14285);
    }
    else {
        Concat((object_ptr)&_cmd_24594, _14285, _14286);
        DeRef(_14285);
        _14285 = NOVALUE;
    }
    DeRef(_14285);
    _14285 = NOVALUE;
    _14286 = NOVALUE;

    /** 		sequence pcmd = build_commandline(public_cmd_args) & pp[PP_PARAMS]*/
    RefDS(_public_cmd_args_24562);
    _14288 = _27build_commandline(_public_cmd_args_24562);
    _2 = (int)SEQ_PTR(_pp_24472);
    _14289 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_SEQUENCE(_14288) && IS_ATOM(_14289)) {
        Ref(_14289);
        Append(&_pcmd_24599, _14288, _14289);
    }
    else if (IS_ATOM(_14288) && IS_SEQUENCE(_14289)) {
        Ref(_14288);
        Prepend(&_pcmd_24599, _14289, _14288);
    }
    else {
        Concat((object_ptr)&_pcmd_24599, _14288, _14289);
        DeRef(_14288);
        _14288 = NOVALUE;
    }
    DeRef(_14288);
    _14288 = NOVALUE;
    _14289 = NOVALUE;

    /** 		integer result = system_exec(cmd, 2)*/
    _result_24604 = system_exec_call(_cmd_24594, 2);

    /** 		if result != 0 then*/
    if (_result_24604 == 0)
    goto L10; // [514] 539

    /** 			CompileErr(sprintf("Preprocessor command failed (%d): %s\n", { result, pcmd } ),,1)*/
    RefDS(_pcmd_24599);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _result_24604;
    ((int *)_2)[2] = _pcmd_24599;
    _14294 = MAKE_SEQ(_1);
    _14295 = EPrintf(-9999999, _14293, _14294);
    DeRefDS(_14294);
    _14294 = NOVALUE;
    RefDS(_22663);
    _46CompileErr(_14295, _22663, 1);
    _14295 = NOVALUE;

    /** 			Cleanup(1)*/
    _46Cleanup(1);
L10: 
    DeRef(_public_cmd_args_24562);
    _public_cmd_args_24562 = NOVALUE;
    DeRef(_cmd_args_24565);
    _cmd_args_24565 = NOVALUE;
    DeRef(_cmd_24594);
    _cmd_24594 = NOVALUE;
    DeRef(_pcmd_24599);
    _pcmd_24599 = NOVALUE;
LE: 

    /** 	return post_fname*/
    DeRefDS(_fname_24471);
    DeRef(_pp_24472);
    _14231 = NOVALUE;
    return _post_fname_24494;
    ;
}



// 0x085B1BA9
