// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _37InitBackEnd(int _x_64378)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_x_64378)) {
        _1 = (long)(DBL_PTR(_x_64378)->dbl);
        if (UNIQUE(DBL_PTR(_x_64378)) && (DBL_PTR(_x_64378)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_x_64378);
        _x_64378 = _1;
    }

    /** 	if not BIND then*/
    if (_38BIND_16567 != 0)
    goto L1; // [7] 15

    /** 		intoptions()	*/
    _70intoptions();
L1: 

    /** end procedure*/
    return;
    ;
}


int _37get_next(int _sym_64413)
{
    int _32452 = NOVALUE;
    int _32451 = NOVALUE;
    int _32450 = NOVALUE;
    int _32449 = NOVALUE;
    int _32446 = NOVALUE;
    int _32445 = NOVALUE;
    int _32444 = NOVALUE;
    int _32443 = NOVALUE;
    int _32442 = NOVALUE;
    int _32441 = NOVALUE;
    int _32440 = NOVALUE;
    int _32439 = NOVALUE;
    int _32438 = NOVALUE;
    int _32437 = NOVALUE;
    int _32436 = NOVALUE;
    int _32435 = NOVALUE;
    int _32434 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sym_64413)) {
        _1 = (long)(DBL_PTR(_sym_64413)->dbl);
        if (UNIQUE(DBL_PTR(_sym_64413)) && (DBL_PTR(_sym_64413)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sym_64413);
        _sym_64413 = _1;
    }

    /** 	if get_backend() then*/
    _32434 = _2get_backend();
    if (_32434 == 0) {
        DeRef(_32434);
        _32434 = NOVALUE;
        goto L1; // [8] 120
    }
    else {
        if (!IS_ATOM_INT(_32434) && DBL_PTR(_32434)->dbl == 0.0){
            DeRef(_32434);
            _32434 = NOVALUE;
            goto L1; // [8] 120
        }
        DeRef(_32434);
        _32434 = NOVALUE;
    }
    DeRef(_32434);
    _32434 = NOVALUE;

    /** 		while sym and */
L2: 
    if (_sym_64413 == 0) {
        goto L3; // [16] 165
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32436 = (int)*(((s1_ptr)_2)->base + _sym_64413);
    _32437 = IS_SEQUENCE(_32436);
    _32436 = NOVALUE;
    if (_32437 == 0) {
        _32438 = 0;
        goto L4; // [29] 47
    }
    _32439 = _55sym_scope(_sym_64413);
    if (IS_ATOM_INT(_32439)) {
        _32440 = (_32439 == 9);
    }
    else {
        _32440 = binary_op(EQUALS, _32439, 9);
    }
    DeRef(_32439);
    _32439 = NOVALUE;
    if (IS_ATOM_INT(_32440))
    _32438 = (_32440 != 0);
    else
    _32438 = DBL_PTR(_32440)->dbl != 0.0;
L4: 
    if (_32438 != 0) {
        DeRef(_32441);
        _32441 = 1;
        goto L5; // [47] 64
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32442 = (int)*(((s1_ptr)_2)->base + _sym_64413);
    _32443 = IS_ATOM(_32442);
    _32442 = NOVALUE;
    _32441 = (_32443 != 0);
L5: 
    if (_32441 == 0)
    {
        _32441 = NOVALUE;
        goto L3; // [65] 165
    }
    else{
        _32441 = NOVALUE;
    }

    /** 			if sequence(SymTab[sym]) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32444 = (int)*(((s1_ptr)_2)->base + _sym_64413);
    _32445 = IS_SEQUENCE(_32444);
    _32444 = NOVALUE;
    if (_32445 == 0)
    {
        _32445 = NOVALUE;
        goto L6; // [79] 101
    }
    else{
        _32445 = NOVALUE;
    }

    /** 				sym = SymTab[sym][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32446 = (int)*(((s1_ptr)_2)->base + _sym_64413);
    _2 = (int)SEQ_PTR(_32446);
    _sym_64413 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_64413)){
        _sym_64413 = (long)DBL_PTR(_sym_64413)->dbl;
    }
    _32446 = NOVALUE;
    goto L2; // [98] 16
L6: 

    /** 				sym = SymTab[sym]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _sym_64413 = (int)*(((s1_ptr)_2)->base + _sym_64413);
    if (!IS_ATOM_INT(_sym_64413)){
        _sym_64413 = (long)DBL_PTR(_sym_64413)->dbl;
    }

    /** 		end while*/
    goto L2; // [114] 16
    goto L3; // [117] 165
L1: 

    /** 		while sym and sym_scope( sym ) = SC_UNDEFINED do*/
L7: 
    if (_sym_64413 == 0) {
        goto L8; // [125] 164
    }
    _32450 = _55sym_scope(_sym_64413);
    if (IS_ATOM_INT(_32450)) {
        _32451 = (_32450 == 9);
    }
    else {
        _32451 = binary_op(EQUALS, _32450, 9);
    }
    DeRef(_32450);
    _32450 = NOVALUE;
    if (_32451 <= 0) {
        if (_32451 == 0) {
            DeRef(_32451);
            _32451 = NOVALUE;
            goto L8; // [140] 164
        }
        else {
            if (!IS_ATOM_INT(_32451) && DBL_PTR(_32451)->dbl == 0.0){
                DeRef(_32451);
                _32451 = NOVALUE;
                goto L8; // [140] 164
            }
            DeRef(_32451);
            _32451 = NOVALUE;
        }
    }
    DeRef(_32451);
    _32451 = NOVALUE;

    /** 			sym = SymTab[sym][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _32452 = (int)*(((s1_ptr)_2)->base + _sym_64413);
    _2 = (int)SEQ_PTR(_32452);
    _sym_64413 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_sym_64413)){
        _sym_64413 = (long)DBL_PTR(_sym_64413)->dbl;
    }
    _32452 = NOVALUE;

    /** 		end while*/
    goto L7; // [161] 125
L8: 
L3: 

    /** 	return sym*/
    DeRef(_32440);
    _32440 = NOVALUE;
    return _sym_64413;
    ;
}


void _37BackEnd(int _il_file_64455)
{
    int _addr_64456 = NOVALUE;
    int _st_64457 = NOVALUE;
    int _nm_64458 = NOVALUE;
    int _ms_64459 = NOVALUE;
    int _sl_64460 = NOVALUE;
    int _lit_64461 = NOVALUE;
    int _fn_64462 = NOVALUE;
    int _entry_addr_64463 = NOVALUE;
    int _e_addr_64464 = NOVALUE;
    int _l_addr_64465 = NOVALUE;
    int _no_name_64466 = NOVALUE;
    int _include_info_64467 = NOVALUE;
    int _include_node_64468 = NOVALUE;
    int _include_array_64469 = NOVALUE;
    int _string_size_64470 = NOVALUE;
    int _short_64471 = NOVALUE;
    int _size_64472 = NOVALUE;
    int _repcount_64473 = NOVALUE;
    int _lit_string_64474 = NOVALUE;
    int _other_strings_64475 = NOVALUE;
    int _eentry_64476 = NOVALUE;
    int _32692 = NOVALUE;
    int _32691 = NOVALUE;
    int _32689 = NOVALUE;
    int _32688 = NOVALUE;
    int _32687 = NOVALUE;
    int _32684 = NOVALUE;
    int _32683 = NOVALUE;
    int _32681 = NOVALUE;
    int _32680 = NOVALUE;
    int _32679 = NOVALUE;
    int _32676 = NOVALUE;
    int _32675 = NOVALUE;
    int _32674 = NOVALUE;
    int _32671 = NOVALUE;
    int _32670 = NOVALUE;
    int _32669 = NOVALUE;
    int _32668 = NOVALUE;
    int _32667 = NOVALUE;
    int _32666 = NOVALUE;
    int _32665 = NOVALUE;
    int _32664 = NOVALUE;
    int _32662 = NOVALUE;
    int _32661 = NOVALUE;
    int _32660 = NOVALUE;
    int _32659 = NOVALUE;
    int _32658 = NOVALUE;
    int _32657 = NOVALUE;
    int _32656 = NOVALUE;
    int _32655 = NOVALUE;
    int _32654 = NOVALUE;
    int _32652 = NOVALUE;
    int _32651 = NOVALUE;
    int _32650 = NOVALUE;
    int _32648 = NOVALUE;
    int _32647 = NOVALUE;
    int _32646 = NOVALUE;
    int _32645 = NOVALUE;
    int _32643 = NOVALUE;
    int _32641 = NOVALUE;
    int _32640 = NOVALUE;
    int _32639 = NOVALUE;
    int _32638 = NOVALUE;
    int _32636 = NOVALUE;
    int _32635 = NOVALUE;
    int _32634 = NOVALUE;
    int _32633 = NOVALUE;
    int _32632 = NOVALUE;
    int _32631 = NOVALUE;
    int _32630 = NOVALUE;
    int _32629 = NOVALUE;
    int _32628 = NOVALUE;
    int _32627 = NOVALUE;
    int _32625 = NOVALUE;
    int _32624 = NOVALUE;
    int _32623 = NOVALUE;
    int _32622 = NOVALUE;
    int _32621 = NOVALUE;
    int _32620 = NOVALUE;
    int _32619 = NOVALUE;
    int _32617 = NOVALUE;
    int _32615 = NOVALUE;
    int _32614 = NOVALUE;
    int _32613 = NOVALUE;
    int _32612 = NOVALUE;
    int _32610 = NOVALUE;
    int _32608 = NOVALUE;
    int _32606 = NOVALUE;
    int _32605 = NOVALUE;
    int _32604 = NOVALUE;
    int _32602 = NOVALUE;
    int _32601 = NOVALUE;
    int _32599 = NOVALUE;
    int _32598 = NOVALUE;
    int _32596 = NOVALUE;
    int _32594 = NOVALUE;
    int _32593 = NOVALUE;
    int _32592 = NOVALUE;
    int _32590 = NOVALUE;
    int _32589 = NOVALUE;
    int _32588 = NOVALUE;
    int _32586 = NOVALUE;
    int _32585 = NOVALUE;
    int _32584 = NOVALUE;
    int _32583 = NOVALUE;
    int _32582 = NOVALUE;
    int _32580 = NOVALUE;
    int _32579 = NOVALUE;
    int _32578 = NOVALUE;
    int _32575 = NOVALUE;
    int _32574 = NOVALUE;
    int _32573 = NOVALUE;
    int _32572 = NOVALUE;
    int _32571 = NOVALUE;
    int _32570 = NOVALUE;
    int _32568 = NOVALUE;
    int _32567 = NOVALUE;
    int _32564 = NOVALUE;
    int _32560 = NOVALUE;
    int _32558 = NOVALUE;
    int _32555 = NOVALUE;
    int _32554 = NOVALUE;
    int _32553 = NOVALUE;
    int _32552 = NOVALUE;
    int _32551 = NOVALUE;
    int _32550 = NOVALUE;
    int _32549 = NOVALUE;
    int _32548 = NOVALUE;
    int _32546 = NOVALUE;
    int _32545 = NOVALUE;
    int _32544 = NOVALUE;
    int _32543 = NOVALUE;
    int _32542 = NOVALUE;
    int _32541 = NOVALUE;
    int _32540 = NOVALUE;
    int _32539 = NOVALUE;
    int _32538 = NOVALUE;
    int _32536 = NOVALUE;
    int _32535 = NOVALUE;
    int _32534 = NOVALUE;
    int _32533 = NOVALUE;
    int _32532 = NOVALUE;
    int _32530 = NOVALUE;
    int _32529 = NOVALUE;
    int _32528 = NOVALUE;
    int _32527 = NOVALUE;
    int _32526 = NOVALUE;
    int _32525 = NOVALUE;
    int _32524 = NOVALUE;
    int _32523 = NOVALUE;
    int _32522 = NOVALUE;
    int _32521 = NOVALUE;
    int _32520 = NOVALUE;
    int _32519 = NOVALUE;
    int _32518 = NOVALUE;
    int _32517 = NOVALUE;
    int _32516 = NOVALUE;
    int _32514 = NOVALUE;
    int _32513 = NOVALUE;
    int _32512 = NOVALUE;
    int _32511 = NOVALUE;
    int _32510 = NOVALUE;
    int _32509 = NOVALUE;
    int _32508 = NOVALUE;
    int _32507 = NOVALUE;
    int _32506 = NOVALUE;
    int _32505 = NOVALUE;
    int _32503 = NOVALUE;
    int _32502 = NOVALUE;
    int _32501 = NOVALUE;
    int _32500 = NOVALUE;
    int _32499 = NOVALUE;
    int _32498 = NOVALUE;
    int _32497 = NOVALUE;
    int _32496 = NOVALUE;
    int _32495 = NOVALUE;
    int _32494 = NOVALUE;
    int _32493 = NOVALUE;
    int _32492 = NOVALUE;
    int _32491 = NOVALUE;
    int _32489 = NOVALUE;
    int _32487 = NOVALUE;
    int _32486 = NOVALUE;
    int _32485 = NOVALUE;
    int _32484 = NOVALUE;
    int _32483 = NOVALUE;
    int _32482 = NOVALUE;
    int _32481 = NOVALUE;
    int _32480 = NOVALUE;
    int _32479 = NOVALUE;
    int _32478 = NOVALUE;
    int _32477 = NOVALUE;
    int _32476 = NOVALUE;
    int _32475 = NOVALUE;
    int _32474 = NOVALUE;
    int _32473 = NOVALUE;
    int _32472 = NOVALUE;
    int _32471 = NOVALUE;
    int _32470 = NOVALUE;
    int _32469 = NOVALUE;
    int _32468 = NOVALUE;
    int _32467 = NOVALUE;
    int _32466 = NOVALUE;
    int _32465 = NOVALUE;
    int _32464 = NOVALUE;
    int _32463 = NOVALUE;
    int _32462 = NOVALUE;
    int _32460 = NOVALUE;
    int _32458 = NOVALUE;
    int _32455 = NOVALUE;
    int _32454 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_il_file_64455)) {
        _1 = (long)(DBL_PTR(_il_file_64455)->dbl);
        if (UNIQUE(DBL_PTR(_il_file_64455)) && (DBL_PTR(_il_file_64455)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_il_file_64455);
        _il_file_64455 = _1;
    }

    /** 	size = (1+length(SymTab)) * ST_ENTRY_SIZE */
    if (IS_SEQUENCE(_35SymTab_15595)){
            _32454 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _32454 = 1;
    }
    _32455 = _32454 + 1;
    _32454 = NOVALUE;
    _size_64472 = _32455 * 60;
    _32455 = NOVALUE;

    /** 	st = allocate(size)  -- symbol table*/
    _0 = _st_64457;
    _st_64457 = _4allocate(_size_64472, 0);
    DeRef(_0);

    /** 	mem_set(st, 0, size) -- all fields are 0 (NULL) by default*/
    memory_set(_st_64457, 0, _size_64472);

    /** 	poke4(st, length(SymTab))*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _32458 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _32458 = 1;
    }
    if (IS_ATOM_INT(_st_64457)){
        poke4_addr = (unsigned long *)_st_64457;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_st_64457)->dbl);
    }
    *poke4_addr = (unsigned long)_32458;
    _32458 = NOVALUE;

    /** 	lit_string = "" -- literal values are stored in a string like EDS*/
    RefDS(_22663);
    DeRef(_lit_string_64474);
    _lit_string_64474 = _22663;

    /** 	string_size = 0 -- precompute total space needed for symbol names*/
    _string_size_64470 = 0;

    /** 	addr = st + ST_ENTRY_SIZE*/
    DeRef(_addr_64456);
    if (IS_ATOM_INT(_st_64457)) {
        _addr_64456 = _st_64457 + 60;
        if ((long)((unsigned long)_addr_64456 + (unsigned long)HIGH_BITS) >= 0) 
        _addr_64456 = NewDouble((double)_addr_64456);
    }
    else {
        _addr_64456 = NewDouble(DBL_PTR(_st_64457)->dbl + (double)60);
    }

    /** 	for i = 1 to length(SymTab) do*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _32460 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _32460 = 1;
    }
    {
        int _i_64487;
        _i_64487 = 1;
L1: 
        if (_i_64487 > _32460){
            goto L2; // [68] 761
        }

        /** 		eentry = SymTab[i]*/
        DeRef(_eentry_64476);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _eentry_64476 = (int)*(((s1_ptr)_2)->base + _i_64487);
        Ref(_eentry_64476);

        /** 		if atom(eentry) then*/
        _32462 = IS_ATOM(_eentry_64476);
        if (_32462 == 0)
        {
            _32462 = NOVALUE;
            goto L3; // [88] 129
        }
        else{
            _32462 = NOVALUE;
        }

        /** 			poke4(addr + ST_NEXT, get_next( eentry) ) -- NEXT*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32463 = _addr_64456 + 4;
            if ((long)((unsigned long)_32463 + (unsigned long)HIGH_BITS) >= 0) 
            _32463 = NewDouble((double)_32463);
        }
        else {
            _32463 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)4);
        }
        Ref(_eentry_64476);
        _32464 = _37get_next(_eentry_64476);
        if (IS_ATOM_INT(_32463)){
            poke4_addr = (unsigned long *)_32463;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32463)->dbl);
        }
        if (IS_ATOM_INT(_32464)) {
            *poke4_addr = (unsigned long)_32464;
        }
        else if (IS_ATOM(_32464)) {
            _1 = (unsigned long)DBL_PTR(_32464)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32464);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32463);
        _32463 = NOVALUE;
        DeRef(_32464);
        _32464 = NOVALUE;

        /** 			poke(addr + ST_MODE, M_TEMP) -- MODE*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32465 = _addr_64456 + 12;
            if ((long)((unsigned long)_32465 + (unsigned long)HIGH_BITS) >= 0) 
            _32465 = NewDouble((double)_32465);
        }
        else {
            _32465 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)12);
        }
        if (IS_ATOM_INT(_32465)){
            poke_addr = (unsigned char *)_32465;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_32465)->dbl);
        }
        *poke_addr = (unsigned char)3;
        DeRef(_32465);
        _32465 = NOVALUE;

        /** 			poke(addr + ST_SCOPE, SC_UNDEFINED)  -- SCOPE, must be > S_PRIVATE */
        if (IS_ATOM_INT(_addr_64456)) {
            _32466 = _addr_64456 + 13;
            if ((long)((unsigned long)_32466 + (unsigned long)HIGH_BITS) >= 0) 
            _32466 = NewDouble((double)_32466);
        }
        else {
            _32466 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)13);
        }
        if (IS_ATOM_INT(_32466)){
            poke_addr = (unsigned char *)_32466;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_32466)->dbl);
        }
        *poke_addr = (unsigned char)9;
        DeRef(_32466);
        _32466 = NOVALUE;
        goto L4; // [126] 748
L3: 

        /** 			poke4(addr + ST_NEXT, get_next( eentry[S_NEXT]) )*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32467 = _addr_64456 + 4;
            if ((long)((unsigned long)_32467 + (unsigned long)HIGH_BITS) >= 0) 
            _32467 = NewDouble((double)_32467);
        }
        else {
            _32467 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)4);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32468 = (int)*(((s1_ptr)_2)->base + 2);
        Ref(_32468);
        _32469 = _37get_next(_32468);
        _32468 = NOVALUE;
        if (IS_ATOM_INT(_32467)){
            poke4_addr = (unsigned long *)_32467;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32467)->dbl);
        }
        if (IS_ATOM_INT(_32469)) {
            *poke4_addr = (unsigned long)_32469;
        }
        else if (IS_ATOM(_32469)) {
            _1 = (unsigned long)DBL_PTR(_32469)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32469);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32467);
        _32467 = NOVALUE;
        DeRef(_32469);
        _32469 = NOVALUE;

        /** 			poke4(addr + ST_NEXT_IN_BLOCK, eentry[S_NEXT_IN_BLOCK])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32470 = _addr_64456 + 8;
            if ((long)((unsigned long)_32470 + (unsigned long)HIGH_BITS) >= 0) 
            _32470 = NewDouble((double)_32470);
        }
        else {
            _32470 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)8);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NEXT_IN_BLOCK_16590)){
            _32471 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NEXT_IN_BLOCK_16590)->dbl));
        }
        else{
            _32471 = (int)*(((s1_ptr)_2)->base + _38S_NEXT_IN_BLOCK_16590);
        }
        if (IS_ATOM_INT(_32470)){
            poke4_addr = (unsigned long *)_32470;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32470)->dbl);
        }
        if (IS_ATOM_INT(_32471)) {
            *poke4_addr = (unsigned long)_32471;
        }
        else if (IS_ATOM(_32471)) {
            _1 = (unsigned long)DBL_PTR(_32471)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32471);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32470);
        _32470 = NOVALUE;
        _32471 = NOVALUE;

        /** 			poke(addr + ST_MODE, eentry[S_MODE])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32472 = _addr_64456 + 12;
            if ((long)((unsigned long)_32472 + (unsigned long)HIGH_BITS) >= 0) 
            _32472 = NewDouble((double)_32472);
        }
        else {
            _32472 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)12);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32473 = (int)*(((s1_ptr)_2)->base + 3);
        if (IS_ATOM_INT(_32472)){
            poke_addr = (unsigned char *)_32472;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_32472)->dbl);
        }
        if (IS_ATOM_INT(_32473)) {
            *poke_addr = (unsigned char)_32473;
        }
        else if (IS_ATOM(_32473)) {
            _1 = (signed char)DBL_PTR(_32473)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_32473);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        DeRef(_32472);
        _32472 = NOVALUE;
        _32473 = NOVALUE;

        /** 			poke(addr + ST_SCOPE, eentry[S_SCOPE])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32474 = _addr_64456 + 13;
            if ((long)((unsigned long)_32474 + (unsigned long)HIGH_BITS) >= 0) 
            _32474 = NewDouble((double)_32474);
        }
        else {
            _32474 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)13);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32475 = (int)*(((s1_ptr)_2)->base + 4);
        if (IS_ATOM_INT(_32474)){
            poke_addr = (unsigned char *)_32474;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_32474)->dbl);
        }
        if (IS_ATOM_INT(_32475)) {
            *poke_addr = (unsigned char)_32475;
        }
        else if (IS_ATOM(_32475)) {
            _1 = (signed char)DBL_PTR(_32475)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_32475);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        DeRef(_32474);
        _32474 = NOVALUE;
        _32475 = NOVALUE;

        /** 			if length(eentry) >= S_NAME and sequence(eentry[S_NAME]) then*/
        if (IS_SEQUENCE(_eentry_64476)){
                _32476 = SEQ_PTR(_eentry_64476)->length;
        }
        else {
            _32476 = 1;
        }
        if (IS_ATOM_INT(_38S_NAME_16598)) {
            _32477 = (_32476 >= _38S_NAME_16598);
        }
        else {
            _32477 = binary_op(GREATEREQ, _32476, _38S_NAME_16598);
        }
        _32476 = NOVALUE;
        if (IS_ATOM_INT(_32477)) {
            if (_32477 == 0) {
                goto L5; // [204] 273
            }
        }
        else {
            if (DBL_PTR(_32477)->dbl == 0.0) {
                goto L5; // [204] 273
            }
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _32479 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _32479 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        _32480 = IS_SEQUENCE(_32479);
        _32479 = NOVALUE;
        if (_32480 == 0)
        {
            _32480 = NOVALUE;
            goto L5; // [218] 273
        }
        else{
            _32480 = NOVALUE;
        }

        /** 				poke(addr + ST_FILE_NO, eentry[S_FILE_NO])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32481 = _addr_64456 + 14;
            if ((long)((unsigned long)_32481 + (unsigned long)HIGH_BITS) >= 0) 
            _32481 = NewDouble((double)_32481);
        }
        else {
            _32481 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)14);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
            _32482 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
        }
        else{
            _32482 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
        }
        if (IS_ATOM_INT(_32481)){
            poke_addr = (unsigned char *)_32481;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_32481)->dbl);
        }
        if (IS_ATOM_INT(_32482)) {
            *poke_addr = (unsigned char)_32482;
        }
        else if (IS_ATOM(_32482)) {
            _1 = (signed char)DBL_PTR(_32482)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_32482);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        DeRef(_32481);
        _32481 = NOVALUE;
        _32482 = NOVALUE;

        /** 				poke4(addr + ST_TOKEN, eentry[S_TOKEN])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32483 = _addr_64456 + 20;
            if ((long)((unsigned long)_32483 + (unsigned long)HIGH_BITS) >= 0) 
            _32483 = NewDouble((double)_32483);
        }
        else {
            _32483 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)20);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _32484 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _32484 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        if (IS_ATOM_INT(_32483)){
            poke4_addr = (unsigned long *)_32483;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32483)->dbl);
        }
        if (IS_ATOM_INT(_32484)) {
            *poke4_addr = (unsigned long)_32484;
        }
        else if (IS_ATOM(_32484)) {
            _1 = (unsigned long)DBL_PTR(_32484)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32484);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32483);
        _32483 = NOVALUE;
        _32484 = NOVALUE;

        /** 				string_size += length(eentry[S_NAME])+1*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _32485 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _32485 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        if (IS_SEQUENCE(_32485)){
                _32486 = SEQ_PTR(_32485)->length;
        }
        else {
            _32486 = 1;
        }
        _32485 = NOVALUE;
        _32487 = _32486 + 1;
        _32486 = NOVALUE;
        _string_size_64470 = _string_size_64470 + _32487;
        _32487 = NOVALUE;
L5: 

        /** 			if eentry[S_MODE] = M_NORMAL then*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32489 = (int)*(((s1_ptr)_2)->base + 3);
        if (binary_op_a(NOTEQ, _32489, 1)){
            _32489 = NOVALUE;
            goto L6; // [283] 563
        }
        _32489 = NOVALUE;

        /** 				if find(eentry[S_TOKEN], RTN_TOKS) then*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _32491 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _32491 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _32492 = find_from(_32491, _39RTN_TOKS_16547, 1);
        _32491 = NOVALUE;
        if (_32492 == 0)
        {
            _32492 = NOVALUE;
            goto L7; // [302] 544
        }
        else{
            _32492 = NOVALUE;
        }

        /** 					if sequence(eentry[S_CODE]) and (get_backend() or eentry[S_OPCODE]=0) then  */
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _32493 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _32493 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        _32494 = IS_SEQUENCE(_32493);
        _32493 = NOVALUE;
        if (_32494 == 0) {
            goto L8; // [316] 466
        }
        _32496 = _2get_backend();
        if (IS_ATOM_INT(_32496)) {
            if (_32496 != 0) {
                DeRef(_32497);
                _32497 = 1;
                goto L9; // [323] 341
            }
        }
        else {
            if (DBL_PTR(_32496)->dbl != 0.0) {
                DeRef(_32497);
                _32497 = 1;
                goto L9; // [323] 341
            }
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32498 = (int)*(((s1_ptr)_2)->base + 21);
        if (IS_ATOM_INT(_32498)) {
            _32499 = (_32498 == 0);
        }
        else {
            _32499 = binary_op(EQUALS, _32498, 0);
        }
        _32498 = NOVALUE;
        DeRef(_32497);
        if (IS_ATOM_INT(_32499))
        _32497 = (_32499 != 0);
        else
        _32497 = DBL_PTR(_32499)->dbl != 0.0;
L9: 
        if (_32497 == 0)
        {
            _32497 = NOVALUE;
            goto L8; // [342] 466
        }
        else{
            _32497 = NOVALUE;
        }

        /** 						e_addr = allocate(4+4*length(eentry[S_CODE])) -- IL code*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _32500 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _32500 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        if (IS_SEQUENCE(_32500)){
                _32501 = SEQ_PTR(_32500)->length;
        }
        else {
            _32501 = 1;
        }
        _32500 = NOVALUE;
        if (_32501 <= INT15)
        _32502 = 4 * _32501;
        else
        _32502 = NewDouble(4 * (double)_32501);
        _32501 = NOVALUE;
        if (IS_ATOM_INT(_32502)) {
            _32503 = 4 + _32502;
            if ((long)((unsigned long)_32503 + (unsigned long)HIGH_BITS) >= 0) 
            _32503 = NewDouble((double)_32503);
        }
        else {
            _32503 = NewDouble((double)4 + DBL_PTR(_32502)->dbl);
        }
        DeRef(_32502);
        _32502 = NOVALUE;
        _0 = _e_addr_64464;
        _e_addr_64464 = _4allocate(_32503, 0);
        DeRef(_0);
        _32503 = NOVALUE;

        /** 						poke4(e_addr, length(eentry[S_CODE]))*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _32505 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _32505 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        if (IS_SEQUENCE(_32505)){
                _32506 = SEQ_PTR(_32505)->length;
        }
        else {
            _32506 = 1;
        }
        _32505 = NOVALUE;
        if (IS_ATOM_INT(_e_addr_64464)){
            poke4_addr = (unsigned long *)_e_addr_64464;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_e_addr_64464)->dbl);
        }
        *poke4_addr = (unsigned long)_32506;
        _32506 = NOVALUE;

        /** 						poke4(e_addr+4, eentry[S_CODE])*/
        if (IS_ATOM_INT(_e_addr_64464)) {
            _32507 = _e_addr_64464 + 4;
            if ((long)((unsigned long)_32507 + (unsigned long)HIGH_BITS) >= 0) 
            _32507 = NewDouble((double)_32507);
        }
        else {
            _32507 = NewDouble(DBL_PTR(_e_addr_64464)->dbl + (double)4);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_CODE_16610)){
            _32508 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
        }
        else{
            _32508 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
        }
        if (IS_ATOM_INT(_32507)){
            poke4_addr = (unsigned long *)_32507;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32507)->dbl);
        }
        if (IS_ATOM_INT(_32508)) {
            *poke4_addr = (unsigned long)_32508;
        }
        else if (IS_ATOM(_32508)) {
            _1 = (unsigned long)DBL_PTR(_32508)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32508);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32507);
        _32507 = NOVALUE;
        _32508 = NOVALUE;

        /** 						poke4(addr + ST_CODE, e_addr)*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32509 = _addr_64456 + 24;
            if ((long)((unsigned long)_32509 + (unsigned long)HIGH_BITS) >= 0) 
            _32509 = NewDouble((double)_32509);
        }
        else {
            _32509 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)24);
        }
        if (IS_ATOM_INT(_32509)){
            poke4_addr = (unsigned long *)_32509;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32509)->dbl);
        }
        if (IS_ATOM_INT(_e_addr_64464)) {
            *poke4_addr = (unsigned long)_e_addr_64464;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_e_addr_64464)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        DeRef(_32509);
        _32509 = NOVALUE;

        /** 						if sequence(eentry[S_LINETAB]) then*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_LINETAB_16633)){
            _32510 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
        }
        else{
            _32510 = (int)*(((s1_ptr)_2)->base + _38S_LINETAB_16633);
        }
        _32511 = IS_SEQUENCE(_32510);
        _32510 = NOVALUE;
        if (_32511 == 0)
        {
            _32511 = NOVALUE;
            goto LA; // [418] 464
        }
        else{
            _32511 = NOVALUE;
        }

        /** 							l_addr = allocate(4*length(eentry[S_LINETAB])) */
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_LINETAB_16633)){
            _32512 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
        }
        else{
            _32512 = (int)*(((s1_ptr)_2)->base + _38S_LINETAB_16633);
        }
        if (IS_SEQUENCE(_32512)){
                _32513 = SEQ_PTR(_32512)->length;
        }
        else {
            _32513 = 1;
        }
        _32512 = NOVALUE;
        if (_32513 <= INT15)
        _32514 = 4 * _32513;
        else
        _32514 = NewDouble(4 * (double)_32513);
        _32513 = NOVALUE;
        _0 = _l_addr_64465;
        _l_addr_64465 = _4allocate(_32514, 0);
        DeRef(_0);
        _32514 = NOVALUE;

        /** 							poke4(l_addr, eentry[S_LINETAB])*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_LINETAB_16633)){
            _32516 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
        }
        else{
            _32516 = (int)*(((s1_ptr)_2)->base + _38S_LINETAB_16633);
        }
        if (IS_ATOM_INT(_l_addr_64465)){
            poke4_addr = (unsigned long *)_l_addr_64465;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_l_addr_64465)->dbl);
        }
        if (IS_ATOM_INT(_32516)) {
            *poke4_addr = (unsigned long)_32516;
        }
        else if (IS_ATOM(_32516)) {
            _1 = (unsigned long)DBL_PTR(_32516)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32516);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        _32516 = NOVALUE;

        /** 							poke4(addr + ST_LINETAB, l_addr)*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32517 = _addr_64456 + 28;
            if ((long)((unsigned long)_32517 + (unsigned long)HIGH_BITS) >= 0) 
            _32517 = NewDouble((double)_32517);
        }
        else {
            _32517 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)28);
        }
        if (IS_ATOM_INT(_32517)){
            poke4_addr = (unsigned long *)_32517;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32517)->dbl);
        }
        if (IS_ATOM_INT(_l_addr_64465)) {
            *poke4_addr = (unsigned long)_l_addr_64465;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_l_addr_64465)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        DeRef(_32517);
        _32517 = NOVALUE;
        goto LB; // [461] 465
LA: 
LB: 
L8: 

        /** 					poke4(addr + ST_FIRSTLINE, eentry[S_FIRSTLINE])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32518 = _addr_64456 + 32;
            if ((long)((unsigned long)_32518 + (unsigned long)HIGH_BITS) >= 0) 
            _32518 = NewDouble((double)_32518);
        }
        else {
            _32518 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)32);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_FIRSTLINE_16638)){
            _32519 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FIRSTLINE_16638)->dbl));
        }
        else{
            _32519 = (int)*(((s1_ptr)_2)->base + _38S_FIRSTLINE_16638);
        }
        if (IS_ATOM_INT(_32518)){
            poke4_addr = (unsigned long *)_32518;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32518)->dbl);
        }
        if (IS_ATOM_INT(_32519)) {
            *poke4_addr = (unsigned long)_32519;
        }
        else if (IS_ATOM(_32519)) {
            _1 = (unsigned long)DBL_PTR(_32519)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32519);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32518);
        _32518 = NOVALUE;
        _32519 = NOVALUE;

        /** 					poke4(addr + ST_TEMPS, eentry[S_TEMPS])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32520 = _addr_64456 + 36;
            if ((long)((unsigned long)_32520 + (unsigned long)HIGH_BITS) >= 0) 
            _32520 = NewDouble((double)_32520);
        }
        else {
            _32520 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)36);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_TEMPS_16643)){
            _32521 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TEMPS_16643)->dbl));
        }
        else{
            _32521 = (int)*(((s1_ptr)_2)->base + _38S_TEMPS_16643);
        }
        if (IS_ATOM_INT(_32520)){
            poke4_addr = (unsigned long *)_32520;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32520)->dbl);
        }
        if (IS_ATOM_INT(_32521)) {
            *poke4_addr = (unsigned long)_32521;
        }
        else if (IS_ATOM(_32521)) {
            _1 = (unsigned long)DBL_PTR(_32521)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32521);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32520);
        _32520 = NOVALUE;
        _32521 = NOVALUE;

        /** 					poke4(addr + ST_NUM_ARGS, eentry[S_NUM_ARGS])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32522 = _addr_64456 + 40;
            if ((long)((unsigned long)_32522 + (unsigned long)HIGH_BITS) >= 0) 
            _32522 = NewDouble((double)_32522);
        }
        else {
            _32522 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)40);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
            _32523 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
        }
        else{
            _32523 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
        }
        if (IS_ATOM_INT(_32522)){
            poke4_addr = (unsigned long *)_32522;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32522)->dbl);
        }
        if (IS_ATOM_INT(_32523)) {
            *poke4_addr = (unsigned long)_32523;
        }
        else if (IS_ATOM(_32523)) {
            _1 = (unsigned long)DBL_PTR(_32523)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32523);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32522);
        _32522 = NOVALUE;
        _32523 = NOVALUE;

        /** 					poke4(addr + ST_STACK_SPACE, eentry[S_STACK_SPACE])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32524 = _addr_64456 + 52;
            if ((long)((unsigned long)_32524 + (unsigned long)HIGH_BITS) >= 0) 
            _32524 = NewDouble((double)_32524);
        }
        else {
            _32524 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)52);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_STACK_SPACE_16658)){
            _32525 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
        }
        else{
            _32525 = (int)*(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
        }
        if (IS_ATOM_INT(_32524)){
            poke4_addr = (unsigned long *)_32524;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32524)->dbl);
        }
        if (IS_ATOM_INT(_32525)) {
            *poke4_addr = (unsigned long)_32525;
        }
        else if (IS_ATOM(_32525)) {
            _1 = (unsigned long)DBL_PTR(_32525)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32525);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32524);
        _32524 = NOVALUE;
        _32525 = NOVALUE;

        /** 					poke4(addr + ST_BLOCK, eentry[S_BLOCK])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32526 = _addr_64456 + 56;
            if ((long)((unsigned long)_32526 + (unsigned long)HIGH_BITS) >= 0) 
            _32526 = NewDouble((double)_32526);
        }
        else {
            _32526 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)56);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_BLOCK_16618)){
            _32527 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_BLOCK_16618)->dbl));
        }
        else{
            _32527 = (int)*(((s1_ptr)_2)->base + _38S_BLOCK_16618);
        }
        if (IS_ATOM_INT(_32526)){
            poke4_addr = (unsigned long *)_32526;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32526)->dbl);
        }
        if (IS_ATOM_INT(_32527)) {
            *poke4_addr = (unsigned long)_32527;
        }
        else if (IS_ATOM(_32527)) {
            _1 = (unsigned long)DBL_PTR(_32527)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32527);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32526);
        _32526 = NOVALUE;
        _32527 = NOVALUE;
        goto LC; // [541] 747
L7: 

        /** 					poke4(addr + ST_DECLARED_IN, eentry[S_BLOCK] )*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32528 = _addr_64456 + 24;
            if ((long)((unsigned long)_32528 + (unsigned long)HIGH_BITS) >= 0) 
            _32528 = NewDouble((double)_32528);
        }
        else {
            _32528 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)24);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_BLOCK_16618)){
            _32529 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_BLOCK_16618)->dbl));
        }
        else{
            _32529 = (int)*(((s1_ptr)_2)->base + _38S_BLOCK_16618);
        }
        if (IS_ATOM_INT(_32528)){
            poke4_addr = (unsigned long *)_32528;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32528)->dbl);
        }
        if (IS_ATOM_INT(_32529)) {
            *poke4_addr = (unsigned long)_32529;
        }
        else if (IS_ATOM(_32529)) {
            _1 = (unsigned long)DBL_PTR(_32529)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32529);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32528);
        _32528 = NOVALUE;
        _32529 = NOVALUE;
        goto LC; // [560] 747
L6: 

        /** 			elsif eentry[S_MODE] = M_BLOCK then*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32530 = (int)*(((s1_ptr)_2)->base + 3);
        if (binary_op_a(NOTEQ, _32530, 4)){
            _32530 = NOVALUE;
            goto LD; // [573] 652
        }
        _32530 = NOVALUE;

        /** 				poke4(addr + ST_NEXT_IN_BLOCK, eentry[S_NEXT_IN_BLOCK] )*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32532 = _addr_64456 + 8;
            if ((long)((unsigned long)_32532 + (unsigned long)HIGH_BITS) >= 0) 
            _32532 = NewDouble((double)_32532);
        }
        else {
            _32532 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)8);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NEXT_IN_BLOCK_16590)){
            _32533 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NEXT_IN_BLOCK_16590)->dbl));
        }
        else{
            _32533 = (int)*(((s1_ptr)_2)->base + _38S_NEXT_IN_BLOCK_16590);
        }
        if (IS_ATOM_INT(_32532)){
            poke4_addr = (unsigned long *)_32532;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32532)->dbl);
        }
        if (IS_ATOM_INT(_32533)) {
            *poke4_addr = (unsigned long)_32533;
        }
        else if (IS_ATOM(_32533)) {
            _1 = (unsigned long)DBL_PTR(_32533)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32533);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32532);
        _32532 = NOVALUE;
        _32533 = NOVALUE;

        /** 				poke4(addr + ST_BLOCK, eentry[S_BLOCK])*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32534 = _addr_64456 + 56;
            if ((long)((unsigned long)_32534 + (unsigned long)HIGH_BITS) >= 0) 
            _32534 = NewDouble((double)_32534);
        }
        else {
            _32534 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)56);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_BLOCK_16618)){
            _32535 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_BLOCK_16618)->dbl));
        }
        else{
            _32535 = (int)*(((s1_ptr)_2)->base + _38S_BLOCK_16618);
        }
        if (IS_ATOM_INT(_32534)){
            poke4_addr = (unsigned long *)_32534;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32534)->dbl);
        }
        if (IS_ATOM_INT(_32535)) {
            *poke4_addr = (unsigned long)_32535;
        }
        else if (IS_ATOM(_32535)) {
            _1 = (unsigned long)DBL_PTR(_32535)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32535);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32534);
        _32534 = NOVALUE;
        _32535 = NOVALUE;

        /** 				if length(eentry) >= S_FIRST_LINE then*/
        if (IS_SEQUENCE(_eentry_64476)){
                _32536 = SEQ_PTR(_eentry_64476)->length;
        }
        else {
            _32536 = 1;
        }
        if (binary_op_a(LESS, _32536, _38S_FIRST_LINE_16623)){
            _32536 = NOVALUE;
            goto LC; // [614] 747
        }
        _32536 = NOVALUE;

        /** 					poke4(addr + ST_FIRST_LINE, eentry[S_FIRST_LINE] )*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32538 = _addr_64456 + 24;
            if ((long)((unsigned long)_32538 + (unsigned long)HIGH_BITS) >= 0) 
            _32538 = NewDouble((double)_32538);
        }
        else {
            _32538 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)24);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_FIRST_LINE_16623)){
            _32539 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FIRST_LINE_16623)->dbl));
        }
        else{
            _32539 = (int)*(((s1_ptr)_2)->base + _38S_FIRST_LINE_16623);
        }
        if (IS_ATOM_INT(_32538)){
            poke4_addr = (unsigned long *)_32538;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32538)->dbl);
        }
        if (IS_ATOM_INT(_32539)) {
            *poke4_addr = (unsigned long)_32539;
        }
        else if (IS_ATOM(_32539)) {
            _1 = (unsigned long)DBL_PTR(_32539)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32539);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32538);
        _32538 = NOVALUE;
        _32539 = NOVALUE;

        /** 					poke4(addr + ST_LAST_LINE, eentry[S_LAST_LINE] )*/
        if (IS_ATOM_INT(_addr_64456)) {
            _32540 = _addr_64456 + 28;
            if ((long)((unsigned long)_32540 + (unsigned long)HIGH_BITS) >= 0) 
            _32540 = NewDouble((double)_32540);
        }
        else {
            _32540 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)28);
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_LAST_LINE_16628)){
            _32541 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LAST_LINE_16628)->dbl));
        }
        else{
            _32541 = (int)*(((s1_ptr)_2)->base + _38S_LAST_LINE_16628);
        }
        if (IS_ATOM_INT(_32540)){
            poke4_addr = (unsigned long *)_32540;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32540)->dbl);
        }
        if (IS_ATOM_INT(_32541)) {
            *poke4_addr = (unsigned long)_32541;
        }
        else if (IS_ATOM(_32541)) {
            _1 = (unsigned long)DBL_PTR(_32541)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        else {
            _1 = (int)SEQ_PTR(_32541);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *(int *)poke4_addr++ = (unsigned long)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (unsigned long)DBL_PTR(_2)->dbl;
                    *(int *)poke4_addr++ = (unsigned long)_0;
                }
            }
        }
        DeRef(_32540);
        _32540 = NOVALUE;
        _32541 = NOVALUE;
        goto LC; // [649] 747
LD: 

        /** 			elsif (length(eentry) < S_NAME and eentry[S_MODE] = M_CONSTANT) or*/
        if (IS_SEQUENCE(_eentry_64476)){
                _32542 = SEQ_PTR(_eentry_64476)->length;
        }
        else {
            _32542 = 1;
        }
        if (IS_ATOM_INT(_38S_NAME_16598)) {
            _32543 = (_32542 < _38S_NAME_16598);
        }
        else {
            _32543 = binary_op(LESS, _32542, _38S_NAME_16598);
        }
        _32542 = NOVALUE;
        if (IS_ATOM_INT(_32543)) {
            if (_32543 == 0) {
                DeRef(_32544);
                _32544 = 0;
                goto LE; // [663] 683
            }
        }
        else {
            if (DBL_PTR(_32543)->dbl == 0.0) {
                DeRef(_32544);
                _32544 = 0;
                goto LE; // [663] 683
            }
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32545 = (int)*(((s1_ptr)_2)->base + 3);
        if (IS_ATOM_INT(_32545)) {
            _32546 = (_32545 == 2);
        }
        else {
            _32546 = binary_op(EQUALS, _32545, 2);
        }
        _32545 = NOVALUE;
        DeRef(_32544);
        if (IS_ATOM_INT(_32546))
        _32544 = (_32546 != 0);
        else
        _32544 = DBL_PTR(_32546)->dbl != 0.0;
LE: 
        if (_32544 != 0) {
            goto LF; // [683] 721
        }
        if (IS_SEQUENCE(_eentry_64476)){
                _32548 = SEQ_PTR(_eentry_64476)->length;
        }
        else {
            _32548 = 1;
        }
        if (IS_ATOM_INT(_38S_TOKEN_16603)) {
            _32549 = (_32548 >= _38S_TOKEN_16603);
        }
        else {
            _32549 = binary_op(GREATEREQ, _32548, _38S_TOKEN_16603);
        }
        _32548 = NOVALUE;
        if (IS_ATOM_INT(_32549)) {
            if (_32549 == 0) {
                DeRef(_32550);
                _32550 = 0;
                goto L10; // [696] 716
            }
        }
        else {
            if (DBL_PTR(_32549)->dbl == 0.0) {
                DeRef(_32550);
                _32550 = 0;
                goto L10; // [696] 716
            }
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32551 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32551) && IS_ATOM_INT(_38NOVALUE_16800)){
            _32552 = (_32551 < _38NOVALUE_16800) ? -1 : (_32551 > _38NOVALUE_16800);
        }
        else{
            _32552 = compare(_32551, _38NOVALUE_16800);
        }
        _32551 = NOVALUE;
        DeRef(_32550);
        _32550 = (_32552 != 0);
L10: 
        if (_32550 == 0)
        {
            _32550 = NOVALUE;
            goto L11; // [717] 746
        }
        else{
            _32550 = NOVALUE;
        }
LF: 

        /** 				poke4(addr, length(lit_string))  -- record the current offset*/
        if (IS_SEQUENCE(_lit_string_64474)){
                _32553 = SEQ_PTR(_lit_string_64474)->length;
        }
        else {
            _32553 = 1;
        }
        if (IS_ATOM_INT(_addr_64456)){
            poke4_addr = (unsigned long *)_addr_64456;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_addr_64456)->dbl);
        }
        *poke4_addr = (unsigned long)_32553;
        _32553 = NOVALUE;

        /** 				lit_string &= compress(eentry[S_OBJ])*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32554 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_32554);
        _32555 = _36compress(_32554);
        _32554 = NOVALUE;
        if (IS_SEQUENCE(_lit_string_64474) && IS_ATOM(_32555)) {
            Ref(_32555);
            Append(&_lit_string_64474, _lit_string_64474, _32555);
        }
        else if (IS_ATOM(_lit_string_64474) && IS_SEQUENCE(_32555)) {
        }
        else {
            Concat((object_ptr)&_lit_string_64474, _lit_string_64474, _32555);
        }
        DeRef(_32555);
        _32555 = NOVALUE;
L11: 
LC: 
L4: 

        /** 		addr += ST_ENTRY_SIZE  -- could save some bytes by changing st structure*/
        _0 = _addr_64456;
        if (IS_ATOM_INT(_addr_64456)) {
            _addr_64456 = _addr_64456 + 60;
            if ((long)((unsigned long)_addr_64456 + (unsigned long)HIGH_BITS) >= 0) 
            _addr_64456 = NewDouble((double)_addr_64456);
        }
        else {
            _addr_64456 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)60);
        }
        DeRef(_0);

        /** 	end for*/
        _i_64487 = _i_64487 + 1;
        goto L1; // [756] 75
L2: 
        ;
    }

    /** 	lit = allocate(length(lit_string))*/
    if (IS_SEQUENCE(_lit_string_64474)){
            _32558 = SEQ_PTR(_lit_string_64474)->length;
    }
    else {
        _32558 = 1;
    }
    _0 = _lit_64461;
    _lit_64461 = _4allocate(_32558, 0);
    DeRef(_0);
    _32558 = NOVALUE;

    /** 	poke(lit, lit_string) -- shouldn't need 0*/
    if (IS_ATOM_INT(_lit_64461)){
        poke_addr = (unsigned char *)_lit_64461;
    }
    else {
        poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_lit_64461)->dbl);
    }
    _1 = (int)SEQ_PTR(_lit_string_64474);
    _1 = (int)((s1_ptr)_1)->base;
    while (1) {
        _1 += 4;
        _2 = *((int *)_1);
        if (IS_ATOM_INT(_2))
        *poke_addr++ = (unsigned char)_2;
        else if (_2 == NOVALUE)
        break;
        else {
            _0 = (signed char)DBL_PTR(_2)->dbl;
            *poke_addr++ = (signed char)_0;
        }
    }

    /** 	lit_string = {}*/
    RefDS(_22663);
    DeRefDS(_lit_string_64474);
    _lit_string_64474 = _22663;

    /** 	nm = allocate(1+string_size)  */
    _32560 = _string_size_64470 + 1;
    if (_32560 > MAXINT){
        _32560 = NewDouble((double)_32560);
    }
    _0 = _nm_64458;
    _nm_64458 = _4allocate(_32560, 0);
    DeRef(_0);
    _32560 = NOVALUE;

    /** 	addr = nm*/
    Ref(_nm_64458);
    DeRef(_addr_64456);
    _addr_64456 = _nm_64458;

    /** 	entry_addr = st*/
    Ref(_st_64457);
    DeRef(_entry_addr_64463);
    _entry_addr_64463 = _st_64457;

    /** 	no_name = allocate_string("<no-name>")*/
    RefDS(_32562);
    _0 = _no_name_64466;
    _no_name_64466 = _4allocate_string(_32562, 0);
    DeRef(_0);

    /** 	for i = 1 to length(SymTab) do*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _32564 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _32564 = 1;
    }
    {
        int _i_64657;
        _i_64657 = 1;
L12: 
        if (_i_64657 > _32564){
            goto L13; // [818] 1018
        }

        /** 		eentry = SymTab[i]*/
        DeRef(_eentry_64476);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _eentry_64476 = (int)*(((s1_ptr)_2)->base + _i_64657);
        Ref(_eentry_64476);

        /** 		entry_addr += ST_ENTRY_SIZE*/
        _0 = _entry_addr_64463;
        if (IS_ATOM_INT(_entry_addr_64463)) {
            _entry_addr_64463 = _entry_addr_64463 + 60;
            if ((long)((unsigned long)_entry_addr_64463 + (unsigned long)HIGH_BITS) >= 0) 
            _entry_addr_64463 = NewDouble((double)_entry_addr_64463);
        }
        else {
            _entry_addr_64463 = NewDouble(DBL_PTR(_entry_addr_64463)->dbl + (double)60);
        }
        DeRef(_0);

        /** 		if sequence(eentry) then */
        _32567 = IS_SEQUENCE(_eentry_64476);
        if (_32567 == 0)
        {
            _32567 = NOVALUE;
            goto L14; // [844] 1011
        }
        else{
            _32567 = NOVALUE;
        }

        /** 			if length(eentry) >= S_NAME then*/
        if (IS_SEQUENCE(_eentry_64476)){
                _32568 = SEQ_PTR(_eentry_64476)->length;
        }
        else {
            _32568 = 1;
        }
        if (binary_op_a(LESS, _32568, _38S_NAME_16598)){
            _32568 = NOVALUE;
            goto L15; // [854] 982
        }
        _32568 = NOVALUE;

        /** 				if sequence(eentry[S_NAME]) then*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _32570 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _32570 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        _32571 = IS_SEQUENCE(_32570);
        _32570 = NOVALUE;
        if (_32571 == 0)
        {
            _32571 = NOVALUE;
            goto L16; // [869] 921
        }
        else{
            _32571 = NOVALUE;
        }

        /** 					poke4(entry_addr + ST_NAME, addr) */
        if (IS_ATOM_INT(_entry_addr_64463)) {
            _32572 = _entry_addr_64463 + 16;
            if ((long)((unsigned long)_32572 + (unsigned long)HIGH_BITS) >= 0) 
            _32572 = NewDouble((double)_32572);
        }
        else {
            _32572 = NewDouble(DBL_PTR(_entry_addr_64463)->dbl + (double)16);
        }
        if (IS_ATOM_INT(_32572)){
            poke4_addr = (unsigned long *)_32572;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32572)->dbl);
        }
        if (IS_ATOM_INT(_addr_64456)) {
            *poke4_addr = (unsigned long)_addr_64456;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_addr_64456)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        DeRef(_32572);
        _32572 = NOVALUE;

        /** 					poke(addr, eentry[S_NAME])*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _32573 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _32573 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        if (IS_ATOM_INT(_addr_64456)){
            poke_addr = (unsigned char *)_addr_64456;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_addr_64456)->dbl);
        }
        if (IS_ATOM_INT(_32573)) {
            *poke_addr = (unsigned char)_32573;
        }
        else if (IS_ATOM(_32573)) {
            _1 = (signed char)DBL_PTR(_32573)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_32573);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        _32573 = NOVALUE;

        /** 					addr += length(eentry[S_NAME])*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_NAME_16598)){
            _32574 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
        }
        else{
            _32574 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
        }
        if (IS_SEQUENCE(_32574)){
                _32575 = SEQ_PTR(_32574)->length;
        }
        else {
            _32575 = 1;
        }
        _32574 = NOVALUE;
        _0 = _addr_64456;
        if (IS_ATOM_INT(_addr_64456)) {
            _addr_64456 = _addr_64456 + _32575;
            if ((long)((unsigned long)_addr_64456 + (unsigned long)HIGH_BITS) >= 0) 
            _addr_64456 = NewDouble((double)_addr_64456);
        }
        else {
            _addr_64456 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)_32575);
        }
        DeRef(_0);
        _32575 = NOVALUE;

        /** 					poke(addr, 0)  -- 0-delimited string*/
        if (IS_ATOM_INT(_addr_64456)){
            poke_addr = (unsigned char *)_addr_64456;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_addr_64456)->dbl);
        }
        *poke_addr = (unsigned char)0;

        /** 					addr += 1*/
        _0 = _addr_64456;
        if (IS_ATOM_INT(_addr_64456)) {
            _addr_64456 = _addr_64456 + 1;
            if (_addr_64456 > MAXINT){
                _addr_64456 = NewDouble((double)_addr_64456);
            }
        }
        else
        _addr_64456 = binary_op(PLUS, 1, _addr_64456);
        DeRef(_0);
        goto L17; // [918] 931
L16: 

        /** 					poke4(entry_addr + ST_NAME, no_name)*/
        if (IS_ATOM_INT(_entry_addr_64463)) {
            _32578 = _entry_addr_64463 + 16;
            if ((long)((unsigned long)_32578 + (unsigned long)HIGH_BITS) >= 0) 
            _32578 = NewDouble((double)_32578);
        }
        else {
            _32578 = NewDouble(DBL_PTR(_entry_addr_64463)->dbl + (double)16);
        }
        if (IS_ATOM_INT(_32578)){
            poke4_addr = (unsigned long *)_32578;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32578)->dbl);
        }
        if (IS_ATOM_INT(_no_name_64466)) {
            *poke4_addr = (unsigned long)_no_name_64466;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_no_name_64466)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        DeRef(_32578);
        _32578 = NOVALUE;
L17: 

        /** 				if eentry[S_TOKEN] = NAMESPACE or compare( eentry[S_OBJ], NOVALUE ) then*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _32579 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _32579 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        if (IS_ATOM_INT(_32579)) {
            _32580 = (_32579 == 523);
        }
        else {
            _32580 = binary_op(EQUALS, _32579, 523);
        }
        _32579 = NOVALUE;
        if (IS_ATOM_INT(_32580)) {
            if (_32580 != 0) {
                goto L18; // [945] 966
            }
        }
        else {
            if (DBL_PTR(_32580)->dbl != 0.0) {
                goto L18; // [945] 966
            }
        }
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32582 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32582) && IS_ATOM_INT(_38NOVALUE_16800)){
            _32583 = (_32582 < _38NOVALUE_16800) ? -1 : (_32582 > _38NOVALUE_16800);
        }
        else{
            _32583 = compare(_32582, _38NOVALUE_16800);
        }
        _32582 = NOVALUE;
        if (_32583 == 0)
        {
            _32583 = NOVALUE;
            goto L19; // [962] 1010
        }
        else{
            _32583 = NOVALUE;
        }
L18: 

        /** 					poke4(entry_addr, peek4u(entry_addr)+lit)*/
        if (IS_ATOM_INT(_entry_addr_64463)) {
            _32584 = *(unsigned long *)_entry_addr_64463;
            if ((unsigned)_32584 > (unsigned)MAXINT)
            _32584 = NewDouble((double)(unsigned long)_32584);
        }
        else {
            _32584 = *(unsigned long *)(unsigned long)(DBL_PTR(_entry_addr_64463)->dbl);
            if ((unsigned)_32584 > (unsigned)MAXINT)
            _32584 = NewDouble((double)(unsigned long)_32584);
        }
        if (IS_ATOM_INT(_32584) && IS_ATOM_INT(_lit_64461)) {
            _32585 = _32584 + _lit_64461;
            if ((long)((unsigned long)_32585 + (unsigned long)HIGH_BITS) >= 0) 
            _32585 = NewDouble((double)_32585);
        }
        else {
            if (IS_ATOM_INT(_32584)) {
                _32585 = NewDouble((double)_32584 + DBL_PTR(_lit_64461)->dbl);
            }
            else {
                if (IS_ATOM_INT(_lit_64461)) {
                    _32585 = NewDouble(DBL_PTR(_32584)->dbl + (double)_lit_64461);
                }
                else
                _32585 = NewDouble(DBL_PTR(_32584)->dbl + DBL_PTR(_lit_64461)->dbl);
            }
        }
        DeRef(_32584);
        _32584 = NOVALUE;
        if (IS_ATOM_INT(_entry_addr_64463)){
            poke4_addr = (unsigned long *)_entry_addr_64463;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_entry_addr_64463)->dbl);
        }
        if (IS_ATOM_INT(_32585)) {
            *poke4_addr = (unsigned long)_32585;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_32585)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        DeRef(_32585);
        _32585 = NOVALUE;
        goto L19; // [979] 1010
L15: 

        /** 			elsif eentry[S_MODE] = M_CONSTANT then*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32586 = (int)*(((s1_ptr)_2)->base + 3);
        if (binary_op_a(NOTEQ, _32586, 2)){
            _32586 = NOVALUE;
            goto L1A; // [992] 1009
        }
        _32586 = NOVALUE;

        /** 				poke4(entry_addr, peek4u(entry_addr)+lit) */
        if (IS_ATOM_INT(_entry_addr_64463)) {
            _32588 = *(unsigned long *)_entry_addr_64463;
            if ((unsigned)_32588 > (unsigned)MAXINT)
            _32588 = NewDouble((double)(unsigned long)_32588);
        }
        else {
            _32588 = *(unsigned long *)(unsigned long)(DBL_PTR(_entry_addr_64463)->dbl);
            if ((unsigned)_32588 > (unsigned)MAXINT)
            _32588 = NewDouble((double)(unsigned long)_32588);
        }
        if (IS_ATOM_INT(_32588) && IS_ATOM_INT(_lit_64461)) {
            _32589 = _32588 + _lit_64461;
            if ((long)((unsigned long)_32589 + (unsigned long)HIGH_BITS) >= 0) 
            _32589 = NewDouble((double)_32589);
        }
        else {
            if (IS_ATOM_INT(_32588)) {
                _32589 = NewDouble((double)_32588 + DBL_PTR(_lit_64461)->dbl);
            }
            else {
                if (IS_ATOM_INT(_lit_64461)) {
                    _32589 = NewDouble(DBL_PTR(_32588)->dbl + (double)_lit_64461);
                }
                else
                _32589 = NewDouble(DBL_PTR(_32588)->dbl + DBL_PTR(_lit_64461)->dbl);
            }
        }
        DeRef(_32588);
        _32588 = NOVALUE;
        if (IS_ATOM_INT(_entry_addr_64463)){
            poke4_addr = (unsigned long *)_entry_addr_64463;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_entry_addr_64463)->dbl);
        }
        if (IS_ATOM_INT(_32589)) {
            *poke4_addr = (unsigned long)_32589;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_32589)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        DeRef(_32589);
        _32589 = NOVALUE;
L1A: 
L19: 
L14: 

        /** 	end for*/
        _i_64657 = _i_64657 + 1;
        goto L12; // [1013] 825
L13: 
        ;
    }

    /** 	if not has_coverage() then*/
    _32590 = _52has_coverage();
    if (IS_ATOM_INT(_32590)) {
        if (_32590 != 0){
            DeRef(_32590);
            _32590 = NOVALUE;
            goto L1B; // [1023] 1034
        }
    }
    else {
        if (DBL_PTR(_32590)->dbl != 0.0){
            DeRef(_32590);
            _32590 = NOVALUE;
            goto L1B; // [1023] 1034
        }
    }
    DeRef(_32590);
    _32590 = NOVALUE;

    /** 		SymTab = {}  -- free up some space*/
    RefDS(_22663);
    DeRef(_35SymTab_15595);
    _35SymTab_15595 = _22663;
L1B: 

    /** 	size = 0*/
    _size_64472 = 0;

    /** 	for i = 1 to length(slist) do*/
    if (IS_SEQUENCE(_38slist_17040)){
            _32592 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _32592 = 1;
    }
    {
        int _i_64708;
        _i_64708 = 1;
L1C: 
        if (_i_64708 > _32592){
            goto L1D; // [1046] 1098
        }

        /** 		if sequence(slist[i]) then*/
        _2 = (int)SEQ_PTR(_38slist_17040);
        _32593 = (int)*(((s1_ptr)_2)->base + _i_64708);
        _32594 = IS_SEQUENCE(_32593);
        _32593 = NOVALUE;
        if (_32594 == 0)
        {
            _32594 = NOVALUE;
            goto L1E; // [1064] 1076
        }
        else{
            _32594 = NOVALUE;
        }

        /** 			size += 1*/
        _size_64472 = _size_64472 + 1;
        goto L1F; // [1073] 1091
L1E: 

        /** 			size += slist[i]*/
        _2 = (int)SEQ_PTR(_38slist_17040);
        _32596 = (int)*(((s1_ptr)_2)->base + _i_64708);
        if (IS_ATOM_INT(_32596)) {
            _size_64472 = _size_64472 + _32596;
        }
        else {
            _size_64472 = binary_op(PLUS, _size_64472, _32596);
        }
        _32596 = NOVALUE;
        if (!IS_ATOM_INT(_size_64472)) {
            _1 = (long)(DBL_PTR(_size_64472)->dbl);
            if (UNIQUE(DBL_PTR(_size_64472)) && (DBL_PTR(_size_64472)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_size_64472);
            _size_64472 = _1;
        }
L1F: 

        /** 	end for*/
        _i_64708 = _i_64708 + 1;
        goto L1C; // [1093] 1053
L1D: 
        ;
    }

    /** 	sl = allocate((size+1)*8)*/
    _32598 = _size_64472 + 1;
    if (_32598 > MAXINT){
        _32598 = NewDouble((double)_32598);
    }
    if (IS_ATOM_INT(_32598)) {
        if (_32598 == (short)_32598)
        _32599 = _32598 * 8;
        else
        _32599 = NewDouble(_32598 * (double)8);
    }
    else {
        _32599 = NewDouble(DBL_PTR(_32598)->dbl * (double)8);
    }
    DeRef(_32598);
    _32598 = NOVALUE;
    _0 = _sl_64460;
    _sl_64460 = _4allocate(_32599, 0);
    DeRef(_0);
    _32599 = NOVALUE;

    /** 	mem_set(sl, 0, (size+1)*8)*/
    _32601 = _size_64472 + 1;
    if (_32601 > MAXINT){
        _32601 = NewDouble((double)_32601);
    }
    if (IS_ATOM_INT(_32601)) {
        _32602 = _32601 * 8;
    }
    else {
        _32602 = NewDouble(DBL_PTR(_32601)->dbl * (double)8);
    }
    DeRef(_32601);
    _32601 = NOVALUE;
    memory_set(_sl_64460, 0, _32602);
    DeRef(_32602);
    _32602 = NOVALUE;

    /** 	poke4(sl, size)*/
    if (IS_ATOM_INT(_sl_64460)){
        poke4_addr = (unsigned long *)_sl_64460;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_sl_64460)->dbl);
    }
    *poke4_addr = (unsigned long)_size_64472;

    /** 	addr = sl+8 -- 0th element is ignored - origin 1*/
    DeRef(_addr_64456);
    if (IS_ATOM_INT(_sl_64460)) {
        _addr_64456 = _sl_64460 + 8;
        if ((long)((unsigned long)_addr_64456 + (unsigned long)HIGH_BITS) >= 0) 
        _addr_64456 = NewDouble((double)_addr_64456);
    }
    else {
        _addr_64456 = NewDouble(DBL_PTR(_sl_64460)->dbl + (double)8);
    }

    /** 	string_size = 0*/
    _string_size_64470 = 0;

    /** 	for i = 1 to length(slist) do*/
    if (IS_SEQUENCE(_38slist_17040)){
            _32604 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _32604 = 1;
    }
    {
        int _i_64728;
        _i_64728 = 1;
L20: 
        if (_i_64728 > _32604){
            goto L21; // [1150] 1428
        }

        /** 		if sequence(slist[i]) then*/
        _2 = (int)SEQ_PTR(_38slist_17040);
        _32605 = (int)*(((s1_ptr)_2)->base + _i_64728);
        _32606 = IS_SEQUENCE(_32605);
        _32605 = NOVALUE;
        if (_32606 == 0)
        {
            _32606 = NOVALUE;
            goto L22; // [1168] 1187
        }
        else{
            _32606 = NOVALUE;
        }

        /** 			eentry = slist[i]*/
        DeRef(_eentry_64476);
        _2 = (int)SEQ_PTR(_38slist_17040);
        _eentry_64476 = (int)*(((s1_ptr)_2)->base + _i_64728);
        Ref(_eentry_64476);

        /** 			repcount = 1*/
        _repcount_64473 = 1;
        goto L23; // [1184] 1253
L22: 

        /** 			eentry = slist[i-1]*/
        _32608 = _i_64728 - 1;
        DeRef(_eentry_64476);
        _2 = (int)SEQ_PTR(_38slist_17040);
        _eentry_64476 = (int)*(((s1_ptr)_2)->base + _32608);
        Ref(_eentry_64476);

        /** 			if length(eentry) < 4 then*/
        if (IS_SEQUENCE(_eentry_64476)){
                _32610 = SEQ_PTR(_eentry_64476)->length;
        }
        else {
            _32610 = 1;
        }
        if (_32610 >= 4)
        goto L24; // [1204] 1225

        /** 				eentry[1] += 1*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32612 = (int)*(((s1_ptr)_2)->base + 1);
        if (IS_ATOM_INT(_32612)) {
            _32613 = _32612 + 1;
            if (_32613 > MAXINT){
                _32613 = NewDouble((double)_32613);
            }
        }
        else
        _32613 = binary_op(PLUS, 1, _32612);
        _32612 = NOVALUE;
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _eentry_64476 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _32613;
        if( _1 != _32613 ){
            DeRef(_1);
        }
        _32613 = NOVALUE;
        goto L25; // [1222] 1242
L24: 

        /** 				eentry[LINE] += 1*/
        _2 = (int)SEQ_PTR(_eentry_64476);
        _32614 = (int)*(((s1_ptr)_2)->base + 2);
        if (IS_ATOM_INT(_32614)) {
            _32615 = _32614 + 1;
            if (_32615 > MAXINT){
                _32615 = NewDouble((double)_32615);
            }
        }
        else
        _32615 = binary_op(PLUS, 1, _32614);
        _32614 = NOVALUE;
        _2 = (int)SEQ_PTR(_eentry_64476);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _eentry_64476 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _32615;
        if( _1 != _32615 ){
            DeRef(_1);
        }
        _32615 = NOVALUE;
L25: 

        /** 			repcount = slist[i]*/
        _2 = (int)SEQ_PTR(_38slist_17040);
        _repcount_64473 = (int)*(((s1_ptr)_2)->base + _i_64728);
        if (!IS_ATOM_INT(_repcount_64473)){
            _repcount_64473 = (long)DBL_PTR(_repcount_64473)->dbl;
        }
L23: 

        /** 		short = length(eentry) < 4*/
        if (IS_SEQUENCE(_eentry_64476)){
                _32617 = SEQ_PTR(_eentry_64476)->length;
        }
        else {
            _32617 = 1;
        }
        _short_64471 = (_32617 < 4);
        _32617 = NOVALUE;

        /** 		for j = 1 to repcount do*/
        _32619 = _repcount_64473;
        {
            int _j_64755;
            _j_64755 = 1;
L26: 
            if (_j_64755 > _32619){
                goto L27; // [1271] 1421
            }

            /** 			poke4(addr+4, eentry[LINE-short])  -- hits 4,5,6,7 */
            if (IS_ATOM_INT(_addr_64456)) {
                _32620 = _addr_64456 + 4;
                if ((long)((unsigned long)_32620 + (unsigned long)HIGH_BITS) >= 0) 
                _32620 = NewDouble((double)_32620);
            }
            else {
                _32620 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)4);
            }
            _32621 = 2 - _short_64471;
            _2 = (int)SEQ_PTR(_eentry_64476);
            _32622 = (int)*(((s1_ptr)_2)->base + _32621);
            if (IS_ATOM_INT(_32620)){
                poke4_addr = (unsigned long *)_32620;
            }
            else {
                poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32620)->dbl);
            }
            if (IS_ATOM_INT(_32622)) {
                *poke4_addr = (unsigned long)_32622;
            }
            else if (IS_ATOM(_32622)) {
                _1 = (unsigned long)DBL_PTR(_32622)->dbl;
                *poke4_addr = (unsigned long)_1;
            }
            else {
                _1 = (int)SEQ_PTR(_32622);
                _1 = (int)((s1_ptr)_1)->base;
                while (1) {
                    _1 += 4;
                    _2 = *((int *)_1);
                    if (IS_ATOM_INT(_2))
                    *(int *)poke4_addr++ = (unsigned long)_2;
                    else if (_2 == NOVALUE)
                    break;
                    else {
                        _0 = (unsigned long)DBL_PTR(_2)->dbl;
                        *(int *)poke4_addr++ = (unsigned long)_0;
                    }
                }
            }
            DeRef(_32620);
            _32620 = NOVALUE;
            _32622 = NOVALUE;

            /** 			poke(addr+6, eentry[LOCAL_FILE_NO-short])*/
            if (IS_ATOM_INT(_addr_64456)) {
                _32623 = _addr_64456 + 6;
                if ((long)((unsigned long)_32623 + (unsigned long)HIGH_BITS) >= 0) 
                _32623 = NewDouble((double)_32623);
            }
            else {
                _32623 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)6);
            }
            _32624 = 3 - _short_64471;
            _2 = (int)SEQ_PTR(_eentry_64476);
            _32625 = (int)*(((s1_ptr)_2)->base + _32624);
            if (IS_ATOM_INT(_32623)){
                poke_addr = (unsigned char *)_32623;
            }
            else {
                poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_32623)->dbl);
            }
            if (IS_ATOM_INT(_32625)) {
                *poke_addr = (unsigned char)_32625;
            }
            else if (IS_ATOM(_32625)) {
                _1 = (signed char)DBL_PTR(_32625)->dbl;
                *poke_addr = _1;
            }
            else {
                _1 = (int)SEQ_PTR(_32625);
                _1 = (int)((s1_ptr)_1)->base;
                while (1) {
                    _1 += 4;
                    _2 = *((int *)_1);
                    if (IS_ATOM_INT(_2))
                    *poke_addr++ = (unsigned char)_2;
                    else if (_2 == NOVALUE)
                    break;
                    else {
                        _0 = (signed char)DBL_PTR(_2)->dbl;
                        *poke_addr++ = (signed char)_0;
                    }
                }
            }
            DeRef(_32623);
            _32623 = NOVALUE;
            _32625 = NOVALUE;

            /** 			if not short then*/
            if (_short_64471 != 0)
            goto L28; // [1318] 1388

            /** 				if eentry[SRC] then*/
            _2 = (int)SEQ_PTR(_eentry_64476);
            _32627 = (int)*(((s1_ptr)_2)->base + 1);
            if (_32627 == 0) {
                _32627 = NOVALUE;
                goto L29; // [1329] 1372
            }
            else {
                if (!IS_ATOM_INT(_32627) && DBL_PTR(_32627)->dbl == 0.0){
                    _32627 = NOVALUE;
                    goto L29; // [1329] 1372
                }
                _32627 = NOVALUE;
            }
            _32627 = NOVALUE;

            /** 					poke4(addr, all_source[1+floor(eentry[SRC]/SOURCE_CHUNK)]*/
            _2 = (int)SEQ_PTR(_eentry_64476);
            _32628 = (int)*(((s1_ptr)_2)->base + 1);
            if (IS_ATOM_INT(_32628)) {
                if (10000 > 0 && _32628 >= 0) {
                    _32629 = _32628 / 10000;
                }
                else {
                    temp_dbl = floor((double)_32628 / (double)10000);
                    if (_32628 != MININT)
                    _32629 = (long)temp_dbl;
                    else
                    _32629 = NewDouble(temp_dbl);
                }
            }
            else {
                _2 = binary_op(DIVIDE, _32628, 10000);
                _32629 = unary_op(FLOOR, _2);
                DeRef(_2);
            }
            _32628 = NOVALUE;
            if (IS_ATOM_INT(_32629)) {
                _32630 = _32629 + 1;
            }
            else
            _32630 = binary_op(PLUS, 1, _32629);
            DeRef(_32629);
            _32629 = NOVALUE;
            _2 = (int)SEQ_PTR(_35all_source_15619);
            if (!IS_ATOM_INT(_32630)){
                _32631 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_32630)->dbl));
            }
            else{
                _32631 = (int)*(((s1_ptr)_2)->base + _32630);
            }
            _2 = (int)SEQ_PTR(_eentry_64476);
            _32632 = (int)*(((s1_ptr)_2)->base + 1);
            if (IS_ATOM_INT(_32632)) {
                _32633 = (_32632 % 10000);
            }
            else {
                _32633 = binary_op(REMAINDER, _32632, 10000);
            }
            _32632 = NOVALUE;
            if (IS_ATOM_INT(_32631) && IS_ATOM_INT(_32633)) {
                _32634 = _32631 + _32633;
                if ((long)((unsigned long)_32634 + (unsigned long)HIGH_BITS) >= 0) 
                _32634 = NewDouble((double)_32634);
            }
            else {
                _32634 = binary_op(PLUS, _32631, _32633);
            }
            _32631 = NOVALUE;
            DeRef(_32633);
            _32633 = NOVALUE;
            if (IS_ATOM_INT(_addr_64456)){
                poke4_addr = (unsigned long *)_addr_64456;
            }
            else {
                poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_addr_64456)->dbl);
            }
            if (IS_ATOM_INT(_32634)) {
                *poke4_addr = (unsigned long)_32634;
            }
            else if (IS_ATOM(_32634)) {
                _1 = (unsigned long)DBL_PTR(_32634)->dbl;
                *poke4_addr = (unsigned long)_1;
            }
            else {
                _1 = (int)SEQ_PTR(_32634);
                _1 = (int)((s1_ptr)_1)->base;
                while (1) {
                    _1 += 4;
                    _2 = *((int *)_1);
                    if (IS_ATOM_INT(_2))
                    *(int *)poke4_addr++ = (unsigned long)_2;
                    else if (_2 == NOVALUE)
                    break;
                    else {
                        _0 = (unsigned long)DBL_PTR(_2)->dbl;
                        *(int *)poke4_addr++ = (unsigned long)_0;
                    }
                }
            }
            DeRef(_32634);
            _32634 = NOVALUE;
L29: 

            /** 				poke(addr+7, eentry[OPTIONS]) -- else leave it 0*/
            if (IS_ATOM_INT(_addr_64456)) {
                _32635 = _addr_64456 + 7;
                if ((long)((unsigned long)_32635 + (unsigned long)HIGH_BITS) >= 0) 
                _32635 = NewDouble((double)_32635);
            }
            else {
                _32635 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)7);
            }
            _2 = (int)SEQ_PTR(_eentry_64476);
            _32636 = (int)*(((s1_ptr)_2)->base + 4);
            if (IS_ATOM_INT(_32635)){
                poke_addr = (unsigned char *)_32635;
            }
            else {
                poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_32635)->dbl);
            }
            if (IS_ATOM_INT(_32636)) {
                *poke_addr = (unsigned char)_32636;
            }
            else if (IS_ATOM(_32636)) {
                _1 = (signed char)DBL_PTR(_32636)->dbl;
                *poke_addr = _1;
            }
            else {
                _1 = (int)SEQ_PTR(_32636);
                _1 = (int)((s1_ptr)_1)->base;
                while (1) {
                    _1 += 4;
                    _2 = *((int *)_1);
                    if (IS_ATOM_INT(_2))
                    *poke_addr++ = (unsigned char)_2;
                    else if (_2 == NOVALUE)
                    break;
                    else {
                        _0 = (signed char)DBL_PTR(_2)->dbl;
                        *poke_addr++ = (signed char)_0;
                    }
                }
            }
            DeRef(_32635);
            _32635 = NOVALUE;
            _32636 = NOVALUE;
L28: 

            /** 			addr += 8*/
            _0 = _addr_64456;
            if (IS_ATOM_INT(_addr_64456)) {
                _addr_64456 = _addr_64456 + 8;
                if ((long)((unsigned long)_addr_64456 + (unsigned long)HIGH_BITS) >= 0) 
                _addr_64456 = NewDouble((double)_addr_64456);
            }
            else {
                _addr_64456 = NewDouble(DBL_PTR(_addr_64456)->dbl + (double)8);
            }
            DeRef(_0);

            /** 			eentry[LINE-short] += 1*/
            _32638 = 2 - _short_64471;
            _2 = (int)SEQ_PTR(_eentry_64476);
            _32639 = (int)*(((s1_ptr)_2)->base + _32638);
            if (IS_ATOM_INT(_32639)) {
                _32640 = _32639 + 1;
                if (_32640 > MAXINT){
                    _32640 = NewDouble((double)_32640);
                }
            }
            else
            _32640 = binary_op(PLUS, 1, _32639);
            _32639 = NOVALUE;
            _2 = (int)SEQ_PTR(_eentry_64476);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _eentry_64476 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _32638);
            _1 = *(int *)_2;
            *(int *)_2 = _32640;
            if( _1 != _32640 ){
                DeRef(_1);
            }
            _32640 = NOVALUE;

            /** 		end for*/
            _j_64755 = _j_64755 + 1;
            goto L26; // [1416] 1278
L27: 
            ;
        }

        /** 	end for*/
        _i_64728 = _i_64728 + 1;
        goto L20; // [1423] 1157
L21: 
        ;
    }

    /** 	if not has_coverage() then*/
    _32641 = _52has_coverage();
    if (IS_ATOM_INT(_32641)) {
        if (_32641 != 0){
            DeRef(_32641);
            _32641 = NOVALUE;
            goto L2A; // [1433] 1444
        }
    }
    else {
        if (DBL_PTR(_32641)->dbl != 0.0){
            DeRef(_32641);
            _32641 = NOVALUE;
            goto L2A; // [1433] 1444
        }
    }
    DeRef(_32641);
    _32641 = NOVALUE;

    /** 		slist = {}  -- free up some space*/
    RefDS(_22663);
    DeRef(_38slist_17040);
    _38slist_17040 = _22663;
L2A: 

    /** 	other_strings = append(known_files, file_name_entered) & warning_list*/
    RefDS(_38file_name_entered_16943);
    Append(&_32643, _35known_files_15596, _38file_name_entered_16943);
    Concat((object_ptr)&_other_strings_64475, _32643, _46warning_list_49490);
    DeRefDS(_32643);
    _32643 = NOVALUE;
    DeRef(_32643);
    _32643 = NOVALUE;

    /** 	string_size = 0*/
    _string_size_64470 = 0;

    /** 	for i = 1 to length(other_strings) do*/
    if (IS_SEQUENCE(_other_strings_64475)){
            _32645 = SEQ_PTR(_other_strings_64475)->length;
    }
    else {
        _32645 = 1;
    }
    {
        int _i_64799;
        _i_64799 = 1;
L2B: 
        if (_i_64799 > _32645){
            goto L2C; // [1470] 1503
        }

        /** 		string_size += length(other_strings[i])+1*/
        _2 = (int)SEQ_PTR(_other_strings_64475);
        _32646 = (int)*(((s1_ptr)_2)->base + _i_64799);
        if (IS_SEQUENCE(_32646)){
                _32647 = SEQ_PTR(_32646)->length;
        }
        else {
            _32647 = 1;
        }
        _32646 = NOVALUE;
        _32648 = _32647 + 1;
        _32647 = NOVALUE;
        _string_size_64470 = _string_size_64470 + _32648;
        _32648 = NOVALUE;

        /** 	end for*/
        _i_64799 = _i_64799 + 1;
        goto L2B; // [1498] 1477
L2C: 
        ;
    }

    /** 	ms = allocate(4*(10+length(other_strings))) -- miscellaneous*/
    if (IS_SEQUENCE(_other_strings_64475)){
            _32650 = SEQ_PTR(_other_strings_64475)->length;
    }
    else {
        _32650 = 1;
    }
    _32651 = 10 + _32650;
    _32650 = NOVALUE;
    if (_32651 <= INT15)
    _32652 = 4 * _32651;
    else
    _32652 = NewDouble(4 * (double)_32651);
    _32651 = NOVALUE;
    _0 = _ms_64459;
    _ms_64459 = _4allocate(_32652, 0);
    DeRef(_0);
    _32652 = NOVALUE;

    /** 	poke4(ms, max_stack_per_call)*/
    if (IS_ATOM_INT(_ms_64459)){
        poke4_addr = (unsigned long *)_ms_64459;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_ms_64459)->dbl);
    }
    *poke4_addr = (unsigned long)_38max_stack_per_call_17063;

    /** 	poke4(ms+4, AnyTimeProfile)*/
    if (IS_ATOM_INT(_ms_64459)) {
        _32654 = _ms_64459 + 4;
        if ((long)((unsigned long)_32654 + (unsigned long)HIGH_BITS) >= 0) 
        _32654 = NewDouble((double)_32654);
    }
    else {
        _32654 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)4);
    }
    if (IS_ATOM_INT(_32654)){
        poke4_addr = (unsigned long *)_32654;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32654)->dbl);
    }
    *poke4_addr = (unsigned long)_35AnyTimeProfile_15617;
    DeRef(_32654);
    _32654 = NOVALUE;

    /** 	poke4(ms+8, AnyStatementProfile)*/
    if (IS_ATOM_INT(_ms_64459)) {
        _32655 = _ms_64459 + 8;
        if ((long)((unsigned long)_32655 + (unsigned long)HIGH_BITS) >= 0) 
        _32655 = NewDouble((double)_32655);
    }
    else {
        _32655 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)8);
    }
    if (IS_ATOM_INT(_32655)){
        poke4_addr = (unsigned long *)_32655;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32655)->dbl);
    }
    *poke4_addr = (unsigned long)_35AnyStatementProfile_15618;
    DeRef(_32655);
    _32655 = NOVALUE;

    /** 	poke4(ms+12, sample_size)*/
    if (IS_ATOM_INT(_ms_64459)) {
        _32656 = _ms_64459 + 12;
        if ((long)((unsigned long)_32656 + (unsigned long)HIGH_BITS) >= 0) 
        _32656 = NewDouble((double)_32656);
    }
    else {
        _32656 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)12);
    }
    if (IS_ATOM_INT(_32656)){
        poke4_addr = (unsigned long *)_32656;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32656)->dbl);
    }
    *poke4_addr = (unsigned long)_38sample_size_17064;
    DeRef(_32656);
    _32656 = NOVALUE;

    /** 	poke4(ms+16, gline_number)*/
    if (IS_ATOM_INT(_ms_64459)) {
        _32657 = _ms_64459 + 16;
        if ((long)((unsigned long)_32657 + (unsigned long)HIGH_BITS) >= 0) 
        _32657 = NewDouble((double)_32657);
    }
    else {
        _32657 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)16);
    }
    if (IS_ATOM_INT(_32657)){
        poke4_addr = (unsigned long *)_32657;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32657)->dbl);
    }
    *poke4_addr = (unsigned long)_38gline_number_16951;
    DeRef(_32657);
    _32657 = NOVALUE;

    /** 	poke4(ms+20, il_file)*/
    if (IS_ATOM_INT(_ms_64459)) {
        _32658 = _ms_64459 + 20;
        if ((long)((unsigned long)_32658 + (unsigned long)HIGH_BITS) >= 0) 
        _32658 = NewDouble((double)_32658);
    }
    else {
        _32658 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)20);
    }
    if (IS_ATOM_INT(_32658)){
        poke4_addr = (unsigned long *)_32658;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32658)->dbl);
    }
    *poke4_addr = (unsigned long)_il_file_64455;
    DeRef(_32658);
    _32658 = NOVALUE;

    /** 	poke4(ms+24, length(warning_list))*/
    if (IS_ATOM_INT(_ms_64459)) {
        _32659 = _ms_64459 + 24;
        if ((long)((unsigned long)_32659 + (unsigned long)HIGH_BITS) >= 0) 
        _32659 = NewDouble((double)_32659);
    }
    else {
        _32659 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)24);
    }
    if (IS_SEQUENCE(_46warning_list_49490)){
            _32660 = SEQ_PTR(_46warning_list_49490)->length;
    }
    else {
        _32660 = 1;
    }
    if (IS_ATOM_INT(_32659)){
        poke4_addr = (unsigned long *)_32659;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32659)->dbl);
    }
    *poke4_addr = (unsigned long)_32660;
    DeRef(_32659);
    _32659 = NOVALUE;
    _32660 = NOVALUE;

    /** 	poke4(ms+28, length(known_files)) -- stored in 0th position*/
    if (IS_ATOM_INT(_ms_64459)) {
        _32661 = _ms_64459 + 28;
        if ((long)((unsigned long)_32661 + (unsigned long)HIGH_BITS) >= 0) 
        _32661 = NewDouble((double)_32661);
    }
    else {
        _32661 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)28);
    }
    if (IS_SEQUENCE(_35known_files_15596)){
            _32662 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _32662 = 1;
    }
    if (IS_ATOM_INT(_32661)){
        poke4_addr = (unsigned long *)_32661;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32661)->dbl);
    }
    *poke4_addr = (unsigned long)_32662;
    DeRef(_32661);
    _32661 = NOVALUE;
    _32662 = NOVALUE;

    /** 	fn = allocate(string_size)*/
    _0 = _fn_64462;
    _fn_64462 = _4allocate(_string_size_64470, 0);
    DeRef(_0);

    /** 	for i = 1 to length(other_strings) do*/
    if (IS_SEQUENCE(_other_strings_64475)){
            _32664 = SEQ_PTR(_other_strings_64475)->length;
    }
    else {
        _32664 = 1;
    }
    {
        int _i_64829;
        _i_64829 = 1;
L2D: 
        if (_i_64829 > _32664){
            goto L2E; // [1621] 1689
        }

        /** 		poke4(ms+32+(i-1)*4, fn)*/
        if (IS_ATOM_INT(_ms_64459)) {
            _32665 = _ms_64459 + 32;
            if ((long)((unsigned long)_32665 + (unsigned long)HIGH_BITS) >= 0) 
            _32665 = NewDouble((double)_32665);
        }
        else {
            _32665 = NewDouble(DBL_PTR(_ms_64459)->dbl + (double)32);
        }
        _32666 = _i_64829 - 1;
        if (_32666 == (short)_32666)
        _32667 = _32666 * 4;
        else
        _32667 = NewDouble(_32666 * (double)4);
        _32666 = NOVALUE;
        if (IS_ATOM_INT(_32665) && IS_ATOM_INT(_32667)) {
            _32668 = _32665 + _32667;
            if ((long)((unsigned long)_32668 + (unsigned long)HIGH_BITS) >= 0) 
            _32668 = NewDouble((double)_32668);
        }
        else {
            if (IS_ATOM_INT(_32665)) {
                _32668 = NewDouble((double)_32665 + DBL_PTR(_32667)->dbl);
            }
            else {
                if (IS_ATOM_INT(_32667)) {
                    _32668 = NewDouble(DBL_PTR(_32665)->dbl + (double)_32667);
                }
                else
                _32668 = NewDouble(DBL_PTR(_32665)->dbl + DBL_PTR(_32667)->dbl);
            }
        }
        DeRef(_32665);
        _32665 = NOVALUE;
        DeRef(_32667);
        _32667 = NOVALUE;
        if (IS_ATOM_INT(_32668)){
            poke4_addr = (unsigned long *)_32668;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_32668)->dbl);
        }
        if (IS_ATOM_INT(_fn_64462)) {
            *poke4_addr = (unsigned long)_fn_64462;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_fn_64462)->dbl;
            *poke4_addr = (unsigned long)_1;
        }
        DeRef(_32668);
        _32668 = NOVALUE;

        /** 		poke(fn, other_strings[i])*/
        _2 = (int)SEQ_PTR(_other_strings_64475);
        _32669 = (int)*(((s1_ptr)_2)->base + _i_64829);
        if (IS_ATOM_INT(_fn_64462)){
            poke_addr = (unsigned char *)_fn_64462;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_fn_64462)->dbl);
        }
        if (IS_ATOM_INT(_32669)) {
            *poke_addr = (unsigned char)_32669;
        }
        else if (IS_ATOM(_32669)) {
            _1 = (signed char)DBL_PTR(_32669)->dbl;
            *poke_addr = _1;
        }
        else {
            _1 = (int)SEQ_PTR(_32669);
            _1 = (int)((s1_ptr)_1)->base;
            while (1) {
                _1 += 4;
                _2 = *((int *)_1);
                if (IS_ATOM_INT(_2))
                *poke_addr++ = (unsigned char)_2;
                else if (_2 == NOVALUE)
                break;
                else {
                    _0 = (signed char)DBL_PTR(_2)->dbl;
                    *poke_addr++ = (signed char)_0;
                }
            }
        }
        _32669 = NOVALUE;

        /** 		fn += length(other_strings[i])*/
        _2 = (int)SEQ_PTR(_other_strings_64475);
        _32670 = (int)*(((s1_ptr)_2)->base + _i_64829);
        if (IS_SEQUENCE(_32670)){
                _32671 = SEQ_PTR(_32670)->length;
        }
        else {
            _32671 = 1;
        }
        _32670 = NOVALUE;
        _0 = _fn_64462;
        if (IS_ATOM_INT(_fn_64462)) {
            _fn_64462 = _fn_64462 + _32671;
            if ((long)((unsigned long)_fn_64462 + (unsigned long)HIGH_BITS) >= 0) 
            _fn_64462 = NewDouble((double)_fn_64462);
        }
        else {
            _fn_64462 = NewDouble(DBL_PTR(_fn_64462)->dbl + (double)_32671);
        }
        DeRef(_0);
        _32671 = NOVALUE;

        /** 		poke(fn, 0)*/
        if (IS_ATOM_INT(_fn_64462)){
            poke_addr = (unsigned char *)_fn_64462;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_fn_64462)->dbl);
        }
        *poke_addr = (unsigned char)0;

        /** 		fn += 1*/
        _0 = _fn_64462;
        if (IS_ATOM_INT(_fn_64462)) {
            _fn_64462 = _fn_64462 + 1;
            if (_fn_64462 > MAXINT){
                _fn_64462 = NewDouble((double)_fn_64462);
            }
        }
        else
        _fn_64462 = binary_op(PLUS, 1, _fn_64462);
        DeRef(_0);

        /** 	end for*/
        _i_64829 = _i_64829 + 1;
        goto L2D; // [1684] 1628
L2E: 
        ;
    }

    /** 	include_info = allocate( 4 * (1 + length( include_matrix )) ) */
    if (IS_SEQUENCE(_35include_matrix_15602)){
            _32674 = SEQ_PTR(_35include_matrix_15602)->length;
    }
    else {
        _32674 = 1;
    }
    _32675 = _32674 + 1;
    _32674 = NOVALUE;
    if (_32675 <= INT15)
    _32676 = 4 * _32675;
    else
    _32676 = NewDouble(4 * (double)_32675);
    _32675 = NOVALUE;
    _0 = _include_info_64467;
    _include_info_64467 = _4allocate(_32676, 0);
    DeRef(_0);
    _32676 = NOVALUE;

    /** 	include_node = include_info*/
    Ref(_include_info_64467);
    DeRef(_include_node_64468);
    _include_node_64468 = _include_info_64467;

    /** 	poke4( include_info, 0 )*/
    if (IS_ATOM_INT(_include_info_64467)){
        poke4_addr = (unsigned long *)_include_info_64467;
    }
    else {
        poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_include_info_64467)->dbl);
    }
    *poke4_addr = (unsigned long)0;

    /** 	include_node += 4*/
    _0 = _include_node_64468;
    if (IS_ATOM_INT(_include_node_64468)) {
        _include_node_64468 = _include_node_64468 + 4;
        if ((long)((unsigned long)_include_node_64468 + (unsigned long)HIGH_BITS) >= 0) 
        _include_node_64468 = NewDouble((double)_include_node_64468);
    }
    else {
        _include_node_64468 = NewDouble(DBL_PTR(_include_node_64468)->dbl + (double)4);
    }
    DeRef(_0);

    /** 	for i = 1 to length( include_matrix ) do*/
    if (IS_SEQUENCE(_35include_matrix_15602)){
            _32679 = SEQ_PTR(_35include_matrix_15602)->length;
    }
    else {
        _32679 = 1;
    }
    {
        int _i_64848;
        _i_64848 = 1;
L2F: 
        if (_i_64848 > _32679){
            goto L30; // [1732] 1788
        }

        /** 		include_array = allocate( 1 + length( include_matrix ) )*/
        if (IS_SEQUENCE(_35include_matrix_15602)){
                _32680 = SEQ_PTR(_35include_matrix_15602)->length;
        }
        else {
            _32680 = 1;
        }
        _32681 = _32680 + 1;
        _32680 = NOVALUE;
        _0 = _include_array_64469;
        _include_array_64469 = _4allocate(_32681, 0);
        DeRef(_0);
        _32681 = NOVALUE;

        /** 		poke( include_array, i & include_matrix[i] )*/
        _2 = (int)SEQ_PTR(_35include_matrix_15602);
        _32683 = (int)*(((s1_ptr)_2)->base + _i_64848);
        if (IS_SEQUENCE(_i_64848) && IS_ATOM(_32683)) {
        }
        else if (IS_ATOM(_i_64848) && IS_SEQUENCE(_32683)) {
            Prepend(&_32684, _32683, _i_64848);
        }
        else {
            Concat((object_ptr)&_32684, _i_64848, _32683);
        }
        _32683 = NOVALUE;
        if (IS_ATOM_INT(_include_array_64469)){
            poke_addr = (unsigned char *)_include_array_64469;
        }
        else {
            poke_addr = (unsigned char *)(unsigned long)(DBL_PTR(_include_array_64469)->dbl);
        }
        _1 = (int)SEQ_PTR(_32684);
        _1 = (int)((s1_ptr)_1)->base;
        while (1) {
            _1 += 4;
            _2 = *((int *)_1);
            if (IS_ATOM_INT(_2))
            *poke_addr++ = (unsigned char)_2;
            else if (_2 == NOVALUE)
            break;
            else {
                _0 = (signed char)DBL_PTR(_2)->dbl;
                *poke_addr++ = (signed char)_0;
            }
        }
        DeRefDS(_32684);
        _32684 = NOVALUE;

        /** 		poke4( include_node, include_array )*/
        if (IS_ATOM_INT(_include_node_64468)){
            poke4_addr = (unsigned long *)_include_node_64468;
        }
        else {
            poke4_addr = (unsigned long *)(unsigned long)(DBL_PTR(_include_node_64468)->dbl);
        }
        if (IS_ATOM_INT(_include_array_64469)) {
            *poke4_addr = (unsigned long)_include_array_64469;
        }
        else {
            _1 = (unsigned long)DBL_PTR(_include_array_64469)->dbl;
            *poke4_addr = (unsigned long)_1;
        }

        /** 		include_node += 4*/
        _0 = _include_node_64468;
        if (IS_ATOM_INT(_include_node_64468)) {
            _include_node_64468 = _include_node_64468 + 4;
            if ((long)((unsigned long)_include_node_64468 + (unsigned long)HIGH_BITS) >= 0) 
            _include_node_64468 = NewDouble((double)_include_node_64468);
        }
        else {
            _include_node_64468 = NewDouble(DBL_PTR(_include_node_64468)->dbl + (double)4);
        }
        DeRef(_0);

        /** 	end for*/
        _i_64848 = _i_64848 + 1;
        goto L2F; // [1783] 1739
L30: 
        ;
    }

    /** 	if Argc > 2 then*/
    if (_38Argc_16956 <= 2)
    goto L31; // [1792] 1824

    /** 		Argv = {Argv[1]} & Argv[3 .. Argc]*/
    _2 = (int)SEQ_PTR(_38Argv_16957);
    _32687 = (int)*(((s1_ptr)_2)->base + 1);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_32687);
    *((int *)(_2+4)) = _32687;
    _32688 = MAKE_SEQ(_1);
    _32687 = NOVALUE;
    rhs_slice_target = (object_ptr)&_32689;
    RHS_Slice(_38Argv_16957, 3, _38Argc_16956);
    Concat((object_ptr)&_38Argv_16957, _32688, _32689);
    DeRefDS(_32688);
    _32688 = NOVALUE;
    DeRef(_32688);
    _32688 = NOVALUE;
    DeRefDS(_32689);
    _32689 = NOVALUE;
L31: 

    /** 	machine_proc(65, {st, sl, ms, lit, include_info, get_switches(), Argv })*/
    _32691 = _45get_switches();
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_st_64457);
    *((int *)(_2+4)) = _st_64457;
    Ref(_sl_64460);
    *((int *)(_2+8)) = _sl_64460;
    Ref(_ms_64459);
    *((int *)(_2+12)) = _ms_64459;
    Ref(_lit_64461);
    *((int *)(_2+16)) = _lit_64461;
    Ref(_include_info_64467);
    *((int *)(_2+20)) = _include_info_64467;
    *((int *)(_2+24)) = _32691;
    RefDS(_38Argv_16957);
    *((int *)(_2+28)) = _38Argv_16957;
    _32692 = MAKE_SEQ(_1);
    _32691 = NOVALUE;
    machine(65, _32692);
    DeRefDS(_32692);
    _32692 = NOVALUE;

    /** end procedure*/
    DeRef(_addr_64456);
    DeRef(_st_64457);
    DeRef(_nm_64458);
    DeRef(_ms_64459);
    DeRef(_sl_64460);
    DeRef(_lit_64461);
    DeRef(_fn_64462);
    DeRef(_entry_addr_64463);
    DeRef(_e_addr_64464);
    DeRef(_l_addr_64465);
    DeRef(_no_name_64466);
    DeRef(_include_info_64467);
    DeRef(_include_node_64468);
    DeRef(_include_array_64469);
    DeRef(_lit_string_64474);
    DeRef(_other_strings_64475);
    DeRef(_eentry_64476);
    DeRef(_32477);
    _32477 = NOVALUE;
    _32485 = NOVALUE;
    DeRef(_32496);
    _32496 = NOVALUE;
    _32500 = NOVALUE;
    DeRef(_32499);
    _32499 = NOVALUE;
    _32505 = NOVALUE;
    _32512 = NOVALUE;
    DeRef(_32543);
    _32543 = NOVALUE;
    DeRef(_32549);
    _32549 = NOVALUE;
    DeRef(_32546);
    _32546 = NOVALUE;
    _32574 = NOVALUE;
    DeRef(_32608);
    _32608 = NOVALUE;
    DeRef(_32580);
    _32580 = NOVALUE;
    DeRef(_32630);
    _32630 = NOVALUE;
    DeRef(_32621);
    _32621 = NOVALUE;
    DeRef(_32624);
    _32624 = NOVALUE;
    DeRef(_32638);
    _32638 = NOVALUE;
    _32646 = NOVALUE;
    _32670 = NOVALUE;
    return;
    ;
}



// 0xEAB547D1
