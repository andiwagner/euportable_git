// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _35open_locked(int _file_path_15637)
{
    int _fh_15638 = NOVALUE;
    int _0, _1, _2;
    

    /** 	fh = open(file_path, "u")*/
    _fh_15638 = EOpen(_file_path_15637, _8943, 0);

    /** 	if fh = -1 then*/
    if (_fh_15638 != -1)
    goto L1; // [14] 28

    /** 		fh = open(file_path, "r")*/
    _fh_15638 = EOpen(_file_path_15637, _1076, 0);
L1: 

    /** 	return fh*/
    DeRefDS(_file_path_15637);
    return _fh_15638;
    ;
}


int _35get_eudir()
{
    int _possible_paths_15655 = NOVALUE;
    int _homepath_15660 = NOVALUE;
    int _homedrive_15662 = NOVALUE;
    int _possible_path_15685 = NOVALUE;
    int _possible_path_15699 = NOVALUE;
    int _file_check_15713 = NOVALUE;
    int _8994 = NOVALUE;
    int _8993 = NOVALUE;
    int _8992 = NOVALUE;
    int _8990 = NOVALUE;
    int _8988 = NOVALUE;
    int _8986 = NOVALUE;
    int _8985 = NOVALUE;
    int _8984 = NOVALUE;
    int _8983 = NOVALUE;
    int _8982 = NOVALUE;
    int _8980 = NOVALUE;
    int _8978 = NOVALUE;
    int _8977 = NOVALUE;
    int _8973 = NOVALUE;
    int _8971 = NOVALUE;
    int _8968 = NOVALUE;
    int _8967 = NOVALUE;
    int _8966 = NOVALUE;
    int _8965 = NOVALUE;
    int _8964 = NOVALUE;
    int _8963 = NOVALUE;
    int _8962 = NOVALUE;
    int _8961 = NOVALUE;
    int _8960 = NOVALUE;
    int _8949 = NOVALUE;
    int _8947 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(eudir) then*/
    _8947 = IS_SEQUENCE(_35eudir_15621);
    if (_8947 == 0)
    {
        _8947 = NOVALUE;
        goto L1; // [8] 20
    }
    else{
        _8947 = NOVALUE;
    }

    /** 		return eudir*/
    Ref(_35eudir_15621);
    DeRef(_possible_paths_15655);
    DeRefi(_homepath_15660);
    DeRefi(_homedrive_15662);
    return _35eudir_15621;
L1: 

    /** 	eudir = getenv("EUDIR")*/
    DeRef(_35eudir_15621);
    _35eudir_15621 = EGetEnv(_4398);

    /** 	if sequence(eudir) then*/
    _8949 = IS_SEQUENCE(_35eudir_15621);
    if (_8949 == 0)
    {
        _8949 = NOVALUE;
        goto L2; // [32] 44
    }
    else{
        _8949 = NOVALUE;
    }

    /** 		return eudir*/
    Ref(_35eudir_15621);
    DeRef(_possible_paths_15655);
    DeRefi(_homepath_15660);
    DeRefi(_homedrive_15662);
    return _35eudir_15621;
L2: 

    /** 	ifdef UNIX then*/

    /** 		sequence possible_paths = {*/
    _0 = _possible_paths_15655;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_8954);
    *((int *)(_2+4)) = _8954;
    RefDS(_8955);
    *((int *)(_2+8)) = _8955;
    RefDS(_8956);
    *((int *)(_2+12)) = _8956;
    _possible_paths_15655 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 		object homepath = getenv("HOMEPATH")*/
    DeRefi(_homepath_15660);
    _homepath_15660 = EGetEnv(_4015);

    /** 		object homedrive = getenv("HOMEDRIVE")*/
    DeRefi(_homedrive_15662);
    _homedrive_15662 = EGetEnv(_4013);

    /** 		if sequence(homepath) and sequence(homedrive) then*/
    _8960 = IS_SEQUENCE(_homepath_15660);
    if (_8960 == 0) {
        goto L3; // [69] 134
    }
    _8962 = IS_SEQUENCE(_homedrive_15662);
    if (_8962 == 0)
    {
        _8962 = NOVALUE;
        goto L3; // [77] 134
    }
    else{
        _8962 = NOVALUE;
    }

    /** 			if length(homepath) and not equal(homepath[$], SLASH) then*/
    if (IS_SEQUENCE(_homepath_15660)){
            _8963 = SEQ_PTR(_homepath_15660)->length;
    }
    else {
        _8963 = 1;
    }
    if (_8963 == 0) {
        goto L4; // [85] 118
    }
    if (IS_SEQUENCE(_homepath_15660)){
            _8965 = SEQ_PTR(_homepath_15660)->length;
    }
    else {
        _8965 = 1;
    }
    _2 = (int)SEQ_PTR(_homepath_15660);
    _8966 = (int)*(((s1_ptr)_2)->base + _8965);
    if (_8966 == 92)
    _8967 = 1;
    else if (IS_ATOM_INT(_8966) && IS_ATOM_INT(92))
    _8967 = 0;
    else
    _8967 = (compare(_8966, 92) == 0);
    _8966 = NOVALUE;
    _8968 = (_8967 == 0);
    _8967 = NOVALUE;
    if (_8968 == 0)
    {
        DeRef(_8968);
        _8968 = NOVALUE;
        goto L4; // [106] 118
    }
    else{
        DeRef(_8968);
        _8968 = NOVALUE;
    }

    /** 				homepath &= SLASH*/
    if (IS_SEQUENCE(_homepath_15660) && IS_ATOM(92)) {
        Append(&_homepath_15660, _homepath_15660, 92);
    }
    else if (IS_ATOM(_homepath_15660) && IS_SEQUENCE(92)) {
    }
    else {
        Concat((object_ptr)&_homepath_15660, _homepath_15660, 92);
    }
L4: 

    /** 			possible_paths = append(possible_paths, homedrive & SLASH & homepath & "euphoria")*/
    {
        int concat_list[4];

        concat_list[0] = _8970;
        concat_list[1] = _homepath_15660;
        concat_list[2] = 92;
        concat_list[3] = _homedrive_15662;
        Concat_N((object_ptr)&_8971, concat_list, 4);
    }
    RefDS(_8971);
    Append(&_possible_paths_15655, _possible_paths_15655, _8971);
    DeRefDS(_8971);
    _8971 = NOVALUE;
L3: 

    /** 	for i = 1 to length(possible_paths) do*/
    if (IS_SEQUENCE(_possible_paths_15655)){
            _8973 = SEQ_PTR(_possible_paths_15655)->length;
    }
    else {
        _8973 = 1;
    }
    {
        int _i_15683;
        _i_15683 = 1;
L5: 
        if (_i_15683 > _8973){
            goto L6; // [141] 200
        }

        /** 		sequence possible_path = possible_paths[i]*/
        DeRef(_possible_path_15685);
        _2 = (int)SEQ_PTR(_possible_paths_15655);
        _possible_path_15685 = (int)*(((s1_ptr)_2)->base + _i_15683);
        RefDS(_possible_path_15685);

        /** 		if file_exists(possible_path & SLASH & "include" & SLASH & "euphoria.h") then*/
        {
            int concat_list[5];

            concat_list[0] = _8976;
            concat_list[1] = 92;
            concat_list[2] = _8975;
            concat_list[3] = 92;
            concat_list[4] = _possible_path_15685;
            Concat_N((object_ptr)&_8977, concat_list, 5);
        }
        _8978 = _13file_exists(_8977);
        _8977 = NOVALUE;
        if (_8978 == 0) {
            DeRef(_8978);
            _8978 = NOVALUE;
            goto L7; // [174] 191
        }
        else {
            if (!IS_ATOM_INT(_8978) && DBL_PTR(_8978)->dbl == 0.0){
                DeRef(_8978);
                _8978 = NOVALUE;
                goto L7; // [174] 191
            }
            DeRef(_8978);
            _8978 = NOVALUE;
        }
        DeRef(_8978);
        _8978 = NOVALUE;

        /** 			eudir = possible_path*/
        RefDS(_possible_path_15685);
        DeRef(_35eudir_15621);
        _35eudir_15621 = _possible_path_15685;

        /** 			return eudir*/
        RefDS(_35eudir_15621);
        DeRefDS(_possible_path_15685);
        DeRefDS(_possible_paths_15655);
        DeRefi(_homepath_15660);
        DeRefi(_homedrive_15662);
        return _35eudir_15621;
L7: 
        DeRef(_possible_path_15685);
        _possible_path_15685 = NOVALUE;

        /** 	end for*/
        _i_15683 = _i_15683 + 1;
        goto L5; // [195] 148
L6: 
        ;
    }

    /** 	possible_paths = include_paths(0)*/
    _0 = _possible_paths_15655;
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_4424);
    *((int *)(_2+4)) = _4424;
    RefDS(_4423);
    *((int *)(_2+8)) = _4423;
    RefDS(_4422);
    *((int *)(_2+12)) = _4422;
    RefDS(_4421);
    *((int *)(_2+16)) = _4421;
    RefDS(_4420);
    *((int *)(_2+20)) = _4420;
    _possible_paths_15655 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	for i = 1 to length(possible_paths) do*/
    _8980 = 5;
    {
        int _i_15697;
        _i_15697 = 1;
L8: 
        if (_i_15697 > 5){
            goto L9; // [215] 340
        }

        /** 		sequence possible_path = possible_paths[i]*/
        DeRef(_possible_path_15699);
        _2 = (int)SEQ_PTR(_possible_paths_15655);
        _possible_path_15699 = (int)*(((s1_ptr)_2)->base + _i_15697);
        RefDS(_possible_path_15699);

        /** 		if equal(possible_path[$], SLASH) then*/
        if (IS_SEQUENCE(_possible_path_15699)){
                _8982 = SEQ_PTR(_possible_path_15699)->length;
        }
        else {
            _8982 = 1;
        }
        _2 = (int)SEQ_PTR(_possible_path_15699);
        _8983 = (int)*(((s1_ptr)_2)->base + _8982);
        if (_8983 == 92)
        _8984 = 1;
        else if (IS_ATOM_INT(_8983) && IS_ATOM_INT(92))
        _8984 = 0;
        else
        _8984 = (compare(_8983, 92) == 0);
        _8983 = NOVALUE;
        if (_8984 == 0)
        {
            _8984 = NOVALUE;
            goto LA; // [245] 263
        }
        else{
            _8984 = NOVALUE;
        }

        /** 			possible_path = possible_path[1..$-1]*/
        if (IS_SEQUENCE(_possible_path_15699)){
                _8985 = SEQ_PTR(_possible_path_15699)->length;
        }
        else {
            _8985 = 1;
        }
        _8986 = _8985 - 1;
        _8985 = NOVALUE;
        rhs_slice_target = (object_ptr)&_possible_path_15699;
        RHS_Slice(_possible_path_15699, 1, _8986);
LA: 

        /** 		if not ends("include", possible_path) then*/
        RefDS(_8975);
        RefDS(_possible_path_15699);
        _8988 = _12ends(_8975, _possible_path_15699);
        if (IS_ATOM_INT(_8988)) {
            if (_8988 != 0){
                DeRef(_8988);
                _8988 = NOVALUE;
                goto LB; // [270] 280
            }
        }
        else {
            if (DBL_PTR(_8988)->dbl != 0.0){
                DeRef(_8988);
                _8988 = NOVALUE;
                goto LB; // [270] 280
            }
        }
        DeRef(_8988);
        _8988 = NOVALUE;

        /** 			continue*/
        DeRefDS(_possible_path_15699);
        _possible_path_15699 = NOVALUE;
        DeRef(_file_check_15713);
        _file_check_15713 = NOVALUE;
        goto LC; // [277] 335
LB: 

        /** 		sequence file_check = possible_path*/
        RefDS(_possible_path_15699);
        DeRef(_file_check_15713);
        _file_check_15713 = _possible_path_15699;

        /** 		file_check &= SLASH & "euphoria.h"*/
        Prepend(&_8990, _8976, 92);
        Concat((object_ptr)&_file_check_15713, _file_check_15713, _8990);
        DeRefDS(_8990);
        _8990 = NOVALUE;

        /** 		if file_exists(file_check) then*/
        RefDS(_file_check_15713);
        _8992 = _13file_exists(_file_check_15713);
        if (_8992 == 0) {
            DeRef(_8992);
            _8992 = NOVALUE;
            goto LD; // [305] 331
        }
        else {
            if (!IS_ATOM_INT(_8992) && DBL_PTR(_8992)->dbl == 0.0){
                DeRef(_8992);
                _8992 = NOVALUE;
                goto LD; // [305] 331
            }
            DeRef(_8992);
            _8992 = NOVALUE;
        }
        DeRef(_8992);
        _8992 = NOVALUE;

        /** 			eudir = possible_path[1..$-8] -- strip SLASH & "include"*/
        if (IS_SEQUENCE(_possible_path_15699)){
                _8993 = SEQ_PTR(_possible_path_15699)->length;
        }
        else {
            _8993 = 1;
        }
        _8994 = _8993 - 8;
        _8993 = NOVALUE;
        rhs_slice_target = (object_ptr)&_35eudir_15621;
        RHS_Slice(_possible_path_15699, 1, _8994);

        /** 			return eudir*/
        RefDS(_35eudir_15621);
        DeRefDS(_possible_path_15699);
        DeRefDS(_file_check_15713);
        DeRef(_possible_paths_15655);
        DeRefi(_homepath_15660);
        DeRefi(_homedrive_15662);
        DeRef(_8986);
        _8986 = NOVALUE;
        _8994 = NOVALUE;
        return _35eudir_15621;
LD: 
        DeRef(_possible_path_15699);
        _possible_path_15699 = NOVALUE;
        DeRef(_file_check_15713);
        _file_check_15713 = NOVALUE;

        /** 	end for*/
LC: 
        _i_15697 = _i_15697 + 1;
        goto L8; // [335] 222
L9: 
        ;
    }

    /** 	return ""*/
    RefDS(_5);
    DeRef(_possible_paths_15655);
    DeRefi(_homepath_15660);
    DeRefi(_homedrive_15662);
    DeRef(_8986);
    _8986 = NOVALUE;
    DeRef(_8994);
    _8994 = NOVALUE;
    return _5;
    ;
}


void _35set_eudir(int _new_eudir_15725)
{
    int _0, _1, _2;
    

    /** 	eudir = new_eudir*/
    RefDS(_new_eudir_15725);
    DeRef(_35eudir_15621);
    _35eudir_15621 = _new_eudir_15725;

    /** 	cmdline_eudir = 1*/
    _35cmdline_eudir_15634 = 1;

    /** end procedure*/
    DeRefDS(_new_eudir_15725);
    return;
    ;
}


int _35is_eudir_from_cmdline()
{
    int _0, _1, _2;
    

    /** 	return cmdline_eudir*/
    return _35cmdline_eudir_15634;
    ;
}



// 0xCAD4ED61
