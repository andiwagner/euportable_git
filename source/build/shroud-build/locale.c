// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _48get_text(int _MsgNum_20837, int _LocalQuals_20838, int _DBBase_20839)
{
    int _db_res_20841 = NOVALUE;
    int _lMsgText_20842 = NOVALUE;
    int _dbname_20843 = NOVALUE;
    int _11816 = NOVALUE;
    int _11814 = NOVALUE;
    int _11812 = NOVALUE;
    int _11811 = NOVALUE;
    int _11810 = NOVALUE;
    int _11808 = NOVALUE;
    int _11807 = NOVALUE;
    int _11806 = NOVALUE;
    int _11800 = NOVALUE;
    int _11799 = NOVALUE;
    int _11798 = NOVALUE;
    int _11791 = NOVALUE;
    int _11788 = NOVALUE;
    int _11786 = NOVALUE;
    int _11784 = NOVALUE;
    int _11783 = NOVALUE;
    int _11782 = NOVALUE;
    int _11781 = NOVALUE;
    int _0, _1, _2;
    

    /** 	db_res = -1*/
    _db_res_20841 = -1;

    /** 	lMsgText = 0*/
    DeRef(_lMsgText_20842);
    _lMsgText_20842 = 0;

    /** 	if string(LocalQuals) and length(LocalQuals) > 0 then*/
    RefDS(_LocalQuals_20838);
    _11781 = _9string(_LocalQuals_20838);
    if (IS_ATOM_INT(_11781)) {
        if (_11781 == 0) {
            goto L1; // [23] 45
        }
    }
    else {
        if (DBL_PTR(_11781)->dbl == 0.0) {
            goto L1; // [23] 45
        }
    }
    if (IS_SEQUENCE(_LocalQuals_20838)){
            _11783 = SEQ_PTR(_LocalQuals_20838)->length;
    }
    else {
        _11783 = 1;
    }
    _11784 = (_11783 > 0);
    _11783 = NOVALUE;
    if (_11784 == 0)
    {
        DeRef(_11784);
        _11784 = NOVALUE;
        goto L1; // [35] 45
    }
    else{
        DeRef(_11784);
        _11784 = NOVALUE;
    }

    /** 		LocalQuals = {LocalQuals}*/
    _0 = _LocalQuals_20838;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_LocalQuals_20838);
    *((int *)(_2+4)) = _LocalQuals_20838;
    _LocalQuals_20838 = MAKE_SEQ(_1);
    DeRefDS(_0);
L1: 

    /** 	for i = 1 to length(LocalQuals) do*/
    if (IS_SEQUENCE(_LocalQuals_20838)){
            _11786 = SEQ_PTR(_LocalQuals_20838)->length;
    }
    else {
        _11786 = 1;
    }
    {
        int _i_20852;
        _i_20852 = 1;
L2: 
        if (_i_20852 > _11786){
            goto L3; // [50] 142
        }

        /** 		dbname = DBBase & "_" & LocalQuals[i] & ".edb"*/
        _2 = (int)SEQ_PTR(_LocalQuals_20838);
        _11788 = (int)*(((s1_ptr)_2)->base + _i_20852);
        {
            int concat_list[4];

            concat_list[0] = _11789;
            concat_list[1] = _11788;
            concat_list[2] = _11787;
            concat_list[3] = _DBBase_20839;
            Concat_N((object_ptr)&_dbname_20843, concat_list, 4);
        }
        _11788 = NOVALUE;

        /** 		db_res = eds:db_select( filesys:locate_file( dbname ), eds:DB_LOCK_READ_ONLY)*/
        RefDS(_dbname_20843);
        RefDS(_5);
        RefDS(_5);
        _11791 = _13locate_file(_dbname_20843, _5, _5);
        _db_res_20841 = _51db_select(_11791, 3);
        _11791 = NOVALUE;
        if (!IS_ATOM_INT(_db_res_20841)) {
            _1 = (long)(DBL_PTR(_db_res_20841)->dbl);
            if (UNIQUE(DBL_PTR(_db_res_20841)) && (DBL_PTR(_db_res_20841)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_db_res_20841);
            _db_res_20841 = _1;
        }

        /** 		if db_res = eds:DB_OK then*/
        if (_db_res_20841 != 0)
        goto L4; // [91] 135

        /** 			db_res = eds:db_select_table("1")*/
        RefDS(_11794);
        _db_res_20841 = _51db_select_table(_11794);
        if (!IS_ATOM_INT(_db_res_20841)) {
            _1 = (long)(DBL_PTR(_db_res_20841)->dbl);
            if (UNIQUE(DBL_PTR(_db_res_20841)) && (DBL_PTR(_db_res_20841)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_db_res_20841);
            _db_res_20841 = _1;
        }

        /** 			if db_res = eds:DB_OK then*/
        if (_db_res_20841 != 0)
        goto L5; // [107] 134

        /** 				lMsgText = eds:db_fetch_record(MsgNum)*/
        RefDS(_51current_table_name_18102);
        _0 = _lMsgText_20842;
        _lMsgText_20842 = _51db_fetch_record(_MsgNum_20837, _51current_table_name_18102);
        DeRef(_0);

        /** 				if sequence(lMsgText) then*/
        _11798 = IS_SEQUENCE(_lMsgText_20842);
        if (_11798 == 0)
        {
            _11798 = NOVALUE;
            goto L6; // [125] 133
        }
        else{
            _11798 = NOVALUE;
        }

        /** 					exit*/
        goto L3; // [130] 142
L6: 
L5: 
L4: 

        /** 	end for*/
        _i_20852 = _i_20852 + 1;
        goto L2; // [137] 57
L3: 
        ;
    }

    /** 	if atom(lMsgText) then*/
    _11799 = IS_ATOM(_lMsgText_20842);
    if (_11799 == 0)
    {
        _11799 = NOVALUE;
        goto L7; // [147] 291
    }
    else{
        _11799 = NOVALUE;
    }

    /** 		dbname = filesys:locate_file( DBBase & ".edb" )*/
    Concat((object_ptr)&_11800, _DBBase_20839, _11789);
    RefDS(_5);
    RefDS(_5);
    _0 = _dbname_20843;
    _dbname_20843 = _13locate_file(_11800, _5, _5);
    DeRef(_0);
    _11800 = NOVALUE;

    /** 		db_res = eds:db_select(	dbname, DB_LOCK_READ_ONLY)*/
    RefDS(_dbname_20843);
    _db_res_20841 = _51db_select(_dbname_20843, 3);
    if (!IS_ATOM_INT(_db_res_20841)) {
        _1 = (long)(DBL_PTR(_db_res_20841)->dbl);
        if (UNIQUE(DBL_PTR(_db_res_20841)) && (DBL_PTR(_db_res_20841)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_db_res_20841);
        _db_res_20841 = _1;
    }

    /** 		if db_res = eds:DB_OK then*/
    if (_db_res_20841 != 0)
    goto L8; // [179] 290

    /** 			db_res = eds:db_select_table("1")*/
    RefDS(_11794);
    _db_res_20841 = _51db_select_table(_11794);
    if (!IS_ATOM_INT(_db_res_20841)) {
        _1 = (long)(DBL_PTR(_db_res_20841)->dbl);
        if (UNIQUE(DBL_PTR(_db_res_20841)) && (DBL_PTR(_db_res_20841)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_db_res_20841);
        _db_res_20841 = _1;
    }

    /** 			if db_res = eds:DB_OK then*/
    if (_db_res_20841 != 0)
    goto L9; // [195] 289

    /** 				for i = 1 to length(LocalQuals) do*/
    if (IS_SEQUENCE(_LocalQuals_20838)){
            _11806 = SEQ_PTR(_LocalQuals_20838)->length;
    }
    else {
        _11806 = 1;
    }
    {
        int _i_20881;
        _i_20881 = 1;
LA: 
        if (_i_20881 > _11806){
            goto LB; // [204] 248
        }

        /** 					lMsgText = eds:db_fetch_record({LocalQuals[i],MsgNum})*/
        _2 = (int)SEQ_PTR(_LocalQuals_20838);
        _11807 = (int)*(((s1_ptr)_2)->base + _i_20881);
        Ref(_11807);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _11807;
        ((int *)_2)[2] = _MsgNum_20837;
        _11808 = MAKE_SEQ(_1);
        _11807 = NOVALUE;
        RefDS(_51current_table_name_18102);
        _0 = _lMsgText_20842;
        _lMsgText_20842 = _51db_fetch_record(_11808, _51current_table_name_18102);
        DeRef(_0);
        _11808 = NOVALUE;

        /** 					if sequence(lMsgText) then*/
        _11810 = IS_SEQUENCE(_lMsgText_20842);
        if (_11810 == 0)
        {
            _11810 = NOVALUE;
            goto LC; // [233] 241
        }
        else{
            _11810 = NOVALUE;
        }

        /** 						exit*/
        goto LB; // [238] 248
LC: 

        /** 				end for*/
        _i_20881 = _i_20881 + 1;
        goto LA; // [243] 211
LB: 
        ;
    }

    /** 				if atom(lMsgText) then*/
    _11811 = IS_ATOM(_lMsgText_20842);
    if (_11811 == 0)
    {
        _11811 = NOVALUE;
        goto LD; // [253] 270
    }
    else{
        _11811 = NOVALUE;
    }

    /** 					lMsgText = eds:db_fetch_record({"",MsgNum})*/
    RefDS(_5);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _5;
    ((int *)_2)[2] = _MsgNum_20837;
    _11812 = MAKE_SEQ(_1);
    RefDS(_51current_table_name_18102);
    _0 = _lMsgText_20842;
    _lMsgText_20842 = _51db_fetch_record(_11812, _51current_table_name_18102);
    DeRef(_0);
    _11812 = NOVALUE;
LD: 

    /** 				if atom(lMsgText) then*/
    _11814 = IS_ATOM(_lMsgText_20842);
    if (_11814 == 0)
    {
        _11814 = NOVALUE;
        goto LE; // [275] 288
    }
    else{
        _11814 = NOVALUE;
    }

    /** 					lMsgText = eds:db_fetch_record(MsgNum)*/
    RefDS(_51current_table_name_18102);
    _0 = _lMsgText_20842;
    _lMsgText_20842 = _51db_fetch_record(_MsgNum_20837, _51current_table_name_18102);
    DeRef(_0);
LE: 
L9: 
L8: 
L7: 

    /** 	if atom(lMsgText) then*/
    _11816 = IS_ATOM(_lMsgText_20842);
    if (_11816 == 0)
    {
        _11816 = NOVALUE;
        goto LF; // [296] 308
    }
    else{
        _11816 = NOVALUE;
    }

    /** 		return 0*/
    DeRefDS(_LocalQuals_20838);
    DeRefDS(_DBBase_20839);
    DeRef(_lMsgText_20842);
    DeRef(_dbname_20843);
    DeRef(_11781);
    _11781 = NOVALUE;
    return 0;
    goto L10; // [305] 315
LF: 

    /** 		return lMsgText*/
    DeRefDS(_LocalQuals_20838);
    DeRefDS(_DBBase_20839);
    DeRef(_dbname_20843);
    DeRef(_11781);
    _11781 = NOVALUE;
    return _lMsgText_20842;
L10: 
    ;
}



// 0x6D88B022
