// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _63reverse(int _s_23052)
{
    int _lower_23053 = NOVALUE;
    int _n_23054 = NOVALUE;
    int _n2_23055 = NOVALUE;
    int _t_23056 = NOVALUE;
    int _13504 = NOVALUE;
    int _13503 = NOVALUE;
    int _13502 = NOVALUE;
    int _13499 = NOVALUE;
    int _0, _1, _2;
    

    /** 	n = length(s)*/
    if (IS_SEQUENCE(_s_23052)){
            _n_23054 = SEQ_PTR(_s_23052)->length;
    }
    else {
        _n_23054 = 1;
    }

    /** 	n2 = floor(n/2)+1*/
    _13499 = _n_23054 >> 1;
    _n2_23055 = _13499 + 1;
    _13499 = NOVALUE;

    /** 	t = repeat(0, n)*/
    DeRef(_t_23056);
    _t_23056 = Repeat(0, _n_23054);

    /** 	lower = 1*/
    _lower_23053 = 1;

    /** 	for upper = n to n2 by -1 do*/
    _13502 = _n2_23055;
    {
        int _upper_23062;
        _upper_23062 = _n_23054;
L1: 
        if (_upper_23062 < _13502){
            goto L2; // [34] 74
        }

        /** 		t[upper] = s[lower]*/
        _2 = (int)SEQ_PTR(_s_23052);
        _13503 = (int)*(((s1_ptr)_2)->base + _lower_23053);
        Ref(_13503);
        _2 = (int)SEQ_PTR(_t_23056);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_23056 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _upper_23062);
        _1 = *(int *)_2;
        *(int *)_2 = _13503;
        if( _1 != _13503 ){
            DeRef(_1);
        }
        _13503 = NOVALUE;

        /** 		t[lower] = s[upper]*/
        _2 = (int)SEQ_PTR(_s_23052);
        _13504 = (int)*(((s1_ptr)_2)->base + _upper_23062);
        Ref(_13504);
        _2 = (int)SEQ_PTR(_t_23056);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _t_23056 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _lower_23053);
        _1 = *(int *)_2;
        *(int *)_2 = _13504;
        if( _1 != _13504 ){
            DeRef(_1);
        }
        _13504 = NOVALUE;

        /** 		lower += 1*/
        _lower_23053 = _lower_23053 + 1;

        /** 	end for*/
        _upper_23062 = _upper_23062 + -1;
        goto L1; // [69] 41
L2: 
        ;
    }

    /** 	return t*/
    DeRefDS(_s_23052);
    return _t_23056;
    ;
}


int _63carry(int _a_23069, int _radix_23070)
{
    int _q_23071 = NOVALUE;
    int _r_23072 = NOVALUE;
    int _b_23073 = NOVALUE;
    int _rmax_23074 = NOVALUE;
    int _i_23075 = NOVALUE;
    int _13518 = NOVALUE;
    int _13517 = NOVALUE;
    int _13516 = NOVALUE;
    int _13513 = NOVALUE;
    int _13507 = NOVALUE;
    int _0, _1, _2;
    

    /** 		rmax = radix - 1*/
    DeRef(_rmax_23074);
    _rmax_23074 = _radix_23070 - 1;
    if ((long)((unsigned long)_rmax_23074 +(unsigned long) HIGH_BITS) >= 0){
        _rmax_23074 = NewDouble((double)_rmax_23074);
    }

    /** 		i = 1*/
    DeRef(_i_23075);
    _i_23075 = 1;

    /** 		while i <= length(a) do*/
L1: 
    if (IS_SEQUENCE(_a_23069)){
            _13507 = SEQ_PTR(_a_23069)->length;
    }
    else {
        _13507 = 1;
    }
    if (binary_op_a(GREATER, _i_23075, _13507)){
        _13507 = NOVALUE;
        goto L2; // [24] 104
    }
    _13507 = NOVALUE;

    /** 				b = a[i]*/
    DeRef(_b_23073);
    _2 = (int)SEQ_PTR(_a_23069);
    if (!IS_ATOM_INT(_i_23075)){
        _b_23073 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_23075)->dbl));
    }
    else{
        _b_23073 = (int)*(((s1_ptr)_2)->base + _i_23075);
    }
    Ref(_b_23073);

    /** 				if b > rmax then*/
    if (binary_op_a(LESSEQ, _b_23073, _rmax_23074)){
        goto L3; // [36] 93
    }

    /** 						q = floor( b / radix )*/
    DeRef(_q_23071);
    if (IS_ATOM_INT(_b_23073)) {
        if (_radix_23070 > 0 && _b_23073 >= 0) {
            _q_23071 = _b_23073 / _radix_23070;
        }
        else {
            temp_dbl = floor((double)_b_23073 / (double)_radix_23070);
            if (_b_23073 != MININT)
            _q_23071 = (long)temp_dbl;
            else
            _q_23071 = NewDouble(temp_dbl);
        }
    }
    else {
        _2 = binary_op(DIVIDE, _b_23073, _radix_23070);
        _q_23071 = unary_op(FLOOR, _2);
        DeRef(_2);
    }

    /** 						r = remainder( b, radix )*/
    DeRef(_r_23072);
    if (IS_ATOM_INT(_b_23073)) {
        _r_23072 = (_b_23073 % _radix_23070);
    }
    else {
        temp_d.dbl = (double)_radix_23070;
        _r_23072 = Dremainder(DBL_PTR(_b_23073), &temp_d);
    }

    /** 						a[i] = r*/
    Ref(_r_23072);
    _2 = (int)SEQ_PTR(_a_23069);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _a_23069 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_i_23075))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_23075)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _i_23075);
    _1 = *(int *)_2;
    *(int *)_2 = _r_23072;
    DeRef(_1);

    /** 						if i = length(a) then*/
    if (IS_SEQUENCE(_a_23069)){
            _13513 = SEQ_PTR(_a_23069)->length;
    }
    else {
        _13513 = 1;
    }
    if (binary_op_a(NOTEQ, _i_23075, _13513)){
        _13513 = NOVALUE;
        goto L4; // [63] 74
    }
    _13513 = NOVALUE;

    /** 								a &= 0*/
    Append(&_a_23069, _a_23069, 0);
L4: 

    /** 						a[i+1] += q*/
    if (IS_ATOM_INT(_i_23075)) {
        _13516 = _i_23075 + 1;
    }
    else
    _13516 = binary_op(PLUS, 1, _i_23075);
    _2 = (int)SEQ_PTR(_a_23069);
    if (!IS_ATOM_INT(_13516)){
        _13517 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_13516)->dbl));
    }
    else{
        _13517 = (int)*(((s1_ptr)_2)->base + _13516);
    }
    if (IS_ATOM_INT(_13517) && IS_ATOM_INT(_q_23071)) {
        _13518 = _13517 + _q_23071;
        if ((long)((unsigned long)_13518 + (unsigned long)HIGH_BITS) >= 0) 
        _13518 = NewDouble((double)_13518);
    }
    else {
        _13518 = binary_op(PLUS, _13517, _q_23071);
    }
    _13517 = NOVALUE;
    _2 = (int)SEQ_PTR(_a_23069);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _a_23069 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_13516))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_13516)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _13516);
    _1 = *(int *)_2;
    *(int *)_2 = _13518;
    if( _1 != _13518 ){
        DeRef(_1);
    }
    _13518 = NOVALUE;
L3: 

    /** 				i += 1*/
    _0 = _i_23075;
    if (IS_ATOM_INT(_i_23075)) {
        _i_23075 = _i_23075 + 1;
        if (_i_23075 > MAXINT){
            _i_23075 = NewDouble((double)_i_23075);
        }
    }
    else
    _i_23075 = binary_op(PLUS, 1, _i_23075);
    DeRef(_0);

    /** 		end while*/
    goto L1; // [101] 21
L2: 

    /** 		return a*/
    DeRef(_q_23071);
    DeRef(_r_23072);
    DeRef(_b_23073);
    DeRef(_rmax_23074);
    DeRef(_i_23075);
    DeRef(_13516);
    _13516 = NOVALUE;
    return _a_23069;
    ;
}


int _63add(int _a_23095, int _b_23096)
{
    int _13536 = NOVALUE;
    int _13534 = NOVALUE;
    int _13533 = NOVALUE;
    int _13532 = NOVALUE;
    int _13531 = NOVALUE;
    int _13529 = NOVALUE;
    int _13528 = NOVALUE;
    int _13526 = NOVALUE;
    int _13525 = NOVALUE;
    int _13524 = NOVALUE;
    int _13523 = NOVALUE;
    int _13521 = NOVALUE;
    int _13520 = NOVALUE;
    int _0, _1, _2;
    

    /** 		if length(a) < length(b) then*/
    if (IS_SEQUENCE(_a_23095)){
            _13520 = SEQ_PTR(_a_23095)->length;
    }
    else {
        _13520 = 1;
    }
    if (IS_SEQUENCE(_b_23096)){
            _13521 = SEQ_PTR(_b_23096)->length;
    }
    else {
        _13521 = 1;
    }
    if (_13520 >= _13521)
    goto L1; // [13] 40

    /** 				a &= repeat( 0, length(b) - length(a) )*/
    if (IS_SEQUENCE(_b_23096)){
            _13523 = SEQ_PTR(_b_23096)->length;
    }
    else {
        _13523 = 1;
    }
    if (IS_SEQUENCE(_a_23095)){
            _13524 = SEQ_PTR(_a_23095)->length;
    }
    else {
        _13524 = 1;
    }
    _13525 = _13523 - _13524;
    _13523 = NOVALUE;
    _13524 = NOVALUE;
    _13526 = Repeat(0, _13525);
    _13525 = NOVALUE;
    Concat((object_ptr)&_a_23095, _a_23095, _13526);
    DeRefDS(_13526);
    _13526 = NOVALUE;
    goto L2; // [37] 74
L1: 

    /** 		elsif length(b) < length(a) then*/
    if (IS_SEQUENCE(_b_23096)){
            _13528 = SEQ_PTR(_b_23096)->length;
    }
    else {
        _13528 = 1;
    }
    if (IS_SEQUENCE(_a_23095)){
            _13529 = SEQ_PTR(_a_23095)->length;
    }
    else {
        _13529 = 1;
    }
    if (_13528 >= _13529)
    goto L3; // [48] 73

    /** 				b &= repeat( 0, length(a) - length(b) )*/
    if (IS_SEQUENCE(_a_23095)){
            _13531 = SEQ_PTR(_a_23095)->length;
    }
    else {
        _13531 = 1;
    }
    if (IS_SEQUENCE(_b_23096)){
            _13532 = SEQ_PTR(_b_23096)->length;
    }
    else {
        _13532 = 1;
    }
    _13533 = _13531 - _13532;
    _13531 = NOVALUE;
    _13532 = NOVALUE;
    _13534 = Repeat(0, _13533);
    _13533 = NOVALUE;
    Concat((object_ptr)&_b_23096, _b_23096, _13534);
    DeRefDS(_13534);
    _13534 = NOVALUE;
L3: 
L2: 

    /** 		return a + b*/
    _13536 = binary_op(PLUS, _a_23095, _b_23096);
    DeRefDS(_a_23095);
    DeRefDS(_b_23096);
    return _13536;
    ;
}


int _63borrow(int _a_23118, int _radix_23119)
{
    int _13544 = NOVALUE;
    int _13543 = NOVALUE;
    int _13542 = NOVALUE;
    int _13541 = NOVALUE;
    int _13540 = NOVALUE;
    int _13538 = NOVALUE;
    int _13537 = NOVALUE;
    int _0, _1, _2;
    

    /** 		for i = length(a) to 2 by -1 do*/
    if (IS_SEQUENCE(_a_23118)){
            _13537 = SEQ_PTR(_a_23118)->length;
    }
    else {
        _13537 = 1;
    }
    {
        int _i_23121;
        _i_23121 = _13537;
L1: 
        if (_i_23121 < 2){
            goto L2; // [10] 67
        }

        /** 				if a[i] < 0 then*/
        _2 = (int)SEQ_PTR(_a_23118);
        _13538 = (int)*(((s1_ptr)_2)->base + _i_23121);
        if (binary_op_a(GREATEREQ, _13538, 0)){
            _13538 = NOVALUE;
            goto L3; // [23] 60
        }
        _13538 = NOVALUE;

        /** 						a[i] += radix*/
        _2 = (int)SEQ_PTR(_a_23118);
        _13540 = (int)*(((s1_ptr)_2)->base + _i_23121);
        if (IS_ATOM_INT(_13540)) {
            _13541 = _13540 + _radix_23119;
            if ((long)((unsigned long)_13541 + (unsigned long)HIGH_BITS) >= 0) 
            _13541 = NewDouble((double)_13541);
        }
        else {
            _13541 = binary_op(PLUS, _13540, _radix_23119);
        }
        _13540 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_23118);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_23118 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_23121);
        _1 = *(int *)_2;
        *(int *)_2 = _13541;
        if( _1 != _13541 ){
            DeRef(_1);
        }
        _13541 = NOVALUE;

        /** 						a[i-1] -= 1*/
        _13542 = _i_23121 - 1;
        _2 = (int)SEQ_PTR(_a_23118);
        _13543 = (int)*(((s1_ptr)_2)->base + _13542);
        if (IS_ATOM_INT(_13543)) {
            _13544 = _13543 - 1;
            if ((long)((unsigned long)_13544 +(unsigned long) HIGH_BITS) >= 0){
                _13544 = NewDouble((double)_13544);
            }
        }
        else {
            _13544 = binary_op(MINUS, _13543, 1);
        }
        _13543 = NOVALUE;
        _2 = (int)SEQ_PTR(_a_23118);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _a_23118 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _13542);
        _1 = *(int *)_2;
        *(int *)_2 = _13544;
        if( _1 != _13544 ){
            DeRef(_1);
        }
        _13544 = NOVALUE;
L3: 

        /** 		end for*/
        _i_23121 = _i_23121 + -1;
        goto L1; // [62] 17
L2: 
        ;
    }

    /** 		return a*/
    DeRef(_13542);
    _13542 = NOVALUE;
    return _a_23118;
    ;
}


int _63bits_to_bytes(int _bits_23133)
{
    int _bytes_23134 = NOVALUE;
    int _r_23135 = NOVALUE;
    int _13553 = NOVALUE;
    int _13552 = NOVALUE;
    int _13551 = NOVALUE;
    int _13550 = NOVALUE;
    int _13548 = NOVALUE;
    int _13547 = NOVALUE;
    int _13545 = NOVALUE;
    int _0, _1, _2;
    

    /** 		r = remainder( length(bits), 8 )*/
    if (IS_SEQUENCE(_bits_23133)){
            _13545 = SEQ_PTR(_bits_23133)->length;
    }
    else {
        _13545 = 1;
    }
    _r_23135 = (_13545 % 8);
    _13545 = NOVALUE;

    /** 		if r  then*/
    if (_r_23135 == 0)
    {
        goto L1; // [14] 32
    }
    else{
    }

    /** 				bits &= repeat( 0, 8 - r )*/
    _13547 = 8 - _r_23135;
    _13548 = Repeat(0, _13547);
    _13547 = NOVALUE;
    Concat((object_ptr)&_bits_23133, _bits_23133, _13548);
    DeRefDS(_13548);
    _13548 = NOVALUE;
L1: 

    /** 		bytes = {}*/
    RefDS(_5);
    DeRef(_bytes_23134);
    _bytes_23134 = _5;

    /** 		for i = 1 to length(bits) by 8 do*/
    if (IS_SEQUENCE(_bits_23133)){
            _13550 = SEQ_PTR(_bits_23133)->length;
    }
    else {
        _13550 = 1;
    }
    {
        int _i_23143;
        _i_23143 = 1;
L2: 
        if (_i_23143 > _13550){
            goto L3; // [44] 77
        }

        /** 				bytes &= bits_to_int( bits[i..i+7] )*/
        _13551 = _i_23143 + 7;
        rhs_slice_target = (object_ptr)&_13552;
        RHS_Slice(_bits_23133, _i_23143, _13551);
        _13553 = _11bits_to_int(_13552);
        _13552 = NOVALUE;
        if (IS_SEQUENCE(_bytes_23134) && IS_ATOM(_13553)) {
            Ref(_13553);
            Append(&_bytes_23134, _bytes_23134, _13553);
        }
        else if (IS_ATOM(_bytes_23134) && IS_SEQUENCE(_13553)) {
        }
        else {
            Concat((object_ptr)&_bytes_23134, _bytes_23134, _13553);
        }
        DeRef(_13553);
        _13553 = NOVALUE;

        /** 		end for*/
        _i_23143 = _i_23143 + 8;
        goto L2; // [72] 51
L3: 
        ;
    }

    /** 		return bytes*/
    DeRefDS(_bits_23133);
    DeRef(_13551);
    _13551 = NOVALUE;
    return _bytes_23134;
    ;
}


int _63bytes_to_bits(int _bytes_23152)
{
    int _bits_23153 = NOVALUE;
    int _13557 = NOVALUE;
    int _13556 = NOVALUE;
    int _13555 = NOVALUE;
    int _0, _1, _2;
    

    /** 		bits = {}*/
    RefDS(_5);
    DeRef(_bits_23153);
    _bits_23153 = _5;

    /** 		for i = 1 to length(bytes) do*/
    if (IS_SEQUENCE(_bytes_23152)){
            _13555 = SEQ_PTR(_bytes_23152)->length;
    }
    else {
        _13555 = 1;
    }
    {
        int _i_23155;
        _i_23155 = 1;
L1: 
        if (_i_23155 > _13555){
            goto L2; // [15] 44
        }

        /** 				bits &= int_to_bits( bytes[i], 8 )*/
        _2 = (int)SEQ_PTR(_bytes_23152);
        _13556 = (int)*(((s1_ptr)_2)->base + _i_23155);
        Ref(_13556);
        _13557 = _11int_to_bits(_13556, 8);
        _13556 = NOVALUE;
        if (IS_SEQUENCE(_bits_23153) && IS_ATOM(_13557)) {
            Ref(_13557);
            Append(&_bits_23153, _bits_23153, _13557);
        }
        else if (IS_ATOM(_bits_23153) && IS_SEQUENCE(_13557)) {
        }
        else {
            Concat((object_ptr)&_bits_23153, _bits_23153, _13557);
        }
        DeRef(_13557);
        _13557 = NOVALUE;

        /** 		end for*/
        _i_23155 = _i_23155 + 1;
        goto L1; // [39] 22
L2: 
        ;
    }

    /** 		return bits*/
    DeRefDS(_bytes_23152);
    return _bits_23153;
    ;
}


int _63convert_radix(int _number_23163, int _from_radix_23164, int _to_radix_23165)
{
    int _target_23166 = NOVALUE;
    int _base_23167 = NOVALUE;
    int _13564 = NOVALUE;
    int _13563 = NOVALUE;
    int _13562 = NOVALUE;
    int _13561 = NOVALUE;
    int _0, _1, _2;
    

    /** 		base = {1}*/
    RefDS(_13559);
    DeRef(_base_23167);
    _base_23167 = _13559;

    /** 		target = {0}*/
    _0 = _target_23166;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    _target_23166 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 		for i = 1 to length(number) do*/
    if (IS_SEQUENCE(_number_23163)){
            _13561 = SEQ_PTR(_number_23163)->length;
    }
    else {
        _13561 = 1;
    }
    {
        int _i_23171;
        _i_23171 = 1;
L1: 
        if (_i_23171 > _13561){
            goto L2; // [25] 78
        }

        /** 				target = carry( add( base * number[i], target ), to_radix )*/
        _2 = (int)SEQ_PTR(_number_23163);
        _13562 = (int)*(((s1_ptr)_2)->base + _i_23171);
        _13563 = binary_op(MULTIPLY, _base_23167, _13562);
        _13562 = NOVALUE;
        RefDS(_target_23166);
        _13564 = _63add(_13563, _target_23166);
        _13563 = NOVALUE;
        _0 = _target_23166;
        _target_23166 = _63carry(_13564, _to_radix_23165);
        DeRefDS(_0);
        _13564 = NOVALUE;

        /** 				base *= from_radix*/
        _0 = _base_23167;
        _base_23167 = binary_op(MULTIPLY, _base_23167, _from_radix_23164);
        DeRefDS(_0);

        /** 				base = carry( base, to_radix )*/
        RefDS(_base_23167);
        _0 = _base_23167;
        _base_23167 = _63carry(_base_23167, _to_radix_23165);
        DeRefDS(_0);

        /** 		end for*/
        _i_23171 = _i_23171 + 1;
        goto L1; // [73] 32
L2: 
        ;
    }

    /** 		return target*/
    DeRefDS(_number_23163);
    DeRef(_base_23167);
    return _target_23166;
    ;
}


int _63half(int _decimal_23181)
{
    int _quotient_23182 = NOVALUE;
    int _q_23183 = NOVALUE;
    int _Q_23184 = NOVALUE;
    int _13585 = NOVALUE;
    int _13584 = NOVALUE;
    int _13583 = NOVALUE;
    int _13582 = NOVALUE;
    int _13581 = NOVALUE;
    int _13580 = NOVALUE;
    int _13577 = NOVALUE;
    int _13575 = NOVALUE;
    int _13574 = NOVALUE;
    int _13571 = NOVALUE;
    int _13570 = NOVALUE;
    int _13568 = NOVALUE;
    int _0, _1, _2;
    

    /** 		quotient = repeat( 0, length(decimal) )*/
    if (IS_SEQUENCE(_decimal_23181)){
            _13568 = SEQ_PTR(_decimal_23181)->length;
    }
    else {
        _13568 = 1;
    }
    DeRef(_quotient_23182);
    _quotient_23182 = Repeat(0, _13568);
    _13568 = NOVALUE;

    /** 		for i = 1 to length( decimal ) do*/
    if (IS_SEQUENCE(_decimal_23181)){
            _13570 = SEQ_PTR(_decimal_23181)->length;
    }
    else {
        _13570 = 1;
    }
    {
        int _i_23188;
        _i_23188 = 1;
L1: 
        if (_i_23188 > _13570){
            goto L2; // [17] 101
        }

        /** 				q = decimal[i] / 2*/
        _2 = (int)SEQ_PTR(_decimal_23181);
        _13571 = (int)*(((s1_ptr)_2)->base + _i_23188);
        DeRef(_q_23183);
        if (IS_ATOM_INT(_13571)) {
            if (_13571 & 1) {
                _q_23183 = NewDouble((_13571 >> 1) + 0.5);
            }
            else
            _q_23183 = _13571 >> 1;
        }
        else {
            _q_23183 = binary_op(DIVIDE, _13571, 2);
        }
        _13571 = NOVALUE;

        /** 				Q = floor( q )*/
        DeRef(_Q_23184);
        if (IS_ATOM_INT(_q_23183))
        _Q_23184 = e_floor(_q_23183);
        else
        _Q_23184 = unary_op(FLOOR, _q_23183);

        /** 				quotient[i] +=  Q*/
        _2 = (int)SEQ_PTR(_quotient_23182);
        _13574 = (int)*(((s1_ptr)_2)->base + _i_23188);
        if (IS_ATOM_INT(_13574) && IS_ATOM_INT(_Q_23184)) {
            _13575 = _13574 + _Q_23184;
            if ((long)((unsigned long)_13575 + (unsigned long)HIGH_BITS) >= 0) 
            _13575 = NewDouble((double)_13575);
        }
        else {
            _13575 = binary_op(PLUS, _13574, _Q_23184);
        }
        _13574 = NOVALUE;
        _2 = (int)SEQ_PTR(_quotient_23182);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _quotient_23182 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_23188);
        _1 = *(int *)_2;
        *(int *)_2 = _13575;
        if( _1 != _13575 ){
            DeRef(_1);
        }
        _13575 = NOVALUE;

        /** 				if q != Q then*/
        if (binary_op_a(EQUALS, _q_23183, _Q_23184)){
            goto L3; // [55] 94
        }

        /** 						if length(quotient) = i then*/
        if (IS_SEQUENCE(_quotient_23182)){
                _13577 = SEQ_PTR(_quotient_23182)->length;
        }
        else {
            _13577 = 1;
        }
        if (_13577 != _i_23188)
        goto L4; // [64] 75

        /** 								quotient &= 0*/
        Append(&_quotient_23182, _quotient_23182, 0);
L4: 

        /** 						quotient[i+1] += 5*/
        _13580 = _i_23188 + 1;
        _2 = (int)SEQ_PTR(_quotient_23182);
        _13581 = (int)*(((s1_ptr)_2)->base + _13580);
        if (IS_ATOM_INT(_13581)) {
            _13582 = _13581 + 5;
            if ((long)((unsigned long)_13582 + (unsigned long)HIGH_BITS) >= 0) 
            _13582 = NewDouble((double)_13582);
        }
        else {
            _13582 = binary_op(PLUS, _13581, 5);
        }
        _13581 = NOVALUE;
        _2 = (int)SEQ_PTR(_quotient_23182);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _quotient_23182 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _13580);
        _1 = *(int *)_2;
        *(int *)_2 = _13582;
        if( _1 != _13582 ){
            DeRef(_1);
        }
        _13582 = NOVALUE;
L3: 

        /** 		end for*/
        _i_23188 = _i_23188 + 1;
        goto L1; // [96] 24
L2: 
        ;
    }

    /** 		return reverse( carry( reverse( quotient ), 10 ) )*/
    RefDS(_quotient_23182);
    _13583 = _63reverse(_quotient_23182);
    _13584 = _63carry(_13583, 10);
    _13583 = NOVALUE;
    _13585 = _63reverse(_13584);
    _13584 = NOVALUE;
    DeRefDS(_decimal_23181);
    DeRefDS(_quotient_23182);
    DeRef(_q_23183);
    DeRef(_Q_23184);
    DeRef(_13580);
    _13580 = NOVALUE;
    return _13585;
    ;
}


int _63decimals_to_bits(int _decimals_23217)
{
    int _sub_23218 = NOVALUE;
    int _bits_23219 = NOVALUE;
    int _bit_23220 = NOVALUE;
    int _assigned_23221 = NOVALUE;
    int _13612 = NOVALUE;
    int _13608 = NOVALUE;
    int _13607 = NOVALUE;
    int _13606 = NOVALUE;
    int _13605 = NOVALUE;
    int _13603 = NOVALUE;
    int _13602 = NOVALUE;
    int _13601 = NOVALUE;
    int _13599 = NOVALUE;
    int _13597 = NOVALUE;
    int _13596 = NOVALUE;
    int _13595 = NOVALUE;
    int _13594 = NOVALUE;
    int _13592 = NOVALUE;
    int _13590 = NOVALUE;
    int _0, _1, _2;
    

    /** 		sub = {5}*/
    RefDS(_13588);
    DeRef(_sub_23218);
    _sub_23218 = _13588;

    /** 		bits = repeat( 0, 53 )*/
    DeRef(_bits_23219);
    _bits_23219 = Repeat(0, 53);

    /** 		bit = 1*/
    _bit_23220 = 1;

    /** 		assigned = 0*/
    _assigned_23221 = 0;

    /** 		if compare(decimals, bits) > 0 then */
    if (IS_ATOM_INT(_decimals_23217) && IS_ATOM_INT(_bits_23219)){
        _13590 = (_decimals_23217 < _bits_23219) ? -1 : (_decimals_23217 > _bits_23219);
    }
    else{
        _13590 = compare(_decimals_23217, _bits_23219);
    }
    if (_13590 <= 0)
    goto L1; // [32] 160

    /** 			while (not assigned) or (bit < find( 1, bits ) + 54)  do*/
L2: 
    _13592 = (_assigned_23221 == 0);
    if (_13592 != 0) {
        goto L3; // [44] 66
    }
    _13594 = find_from(1, _bits_23219, 1);
    _13595 = _13594 + 54;
    _13594 = NOVALUE;
    _13596 = (_bit_23220 < _13595);
    _13595 = NOVALUE;
    if (_13596 == 0)
    {
        DeRef(_13596);
        _13596 = NOVALUE;
        goto L4; // [62] 159
    }
    else{
        DeRef(_13596);
        _13596 = NOVALUE;
    }
L3: 

    /** 				if compare( sub, decimals ) <= 0 then*/
    if (IS_ATOM_INT(_sub_23218) && IS_ATOM_INT(_decimals_23217)){
        _13597 = (_sub_23218 < _decimals_23217) ? -1 : (_sub_23218 > _decimals_23217);
    }
    else{
        _13597 = compare(_sub_23218, _decimals_23217);
    }
    if (_13597 > 0)
    goto L5; // [72] 140

    /** 						assigned = 1*/
    _assigned_23221 = 1;

    /** 						if length( bits ) < bit then*/
    if (IS_SEQUENCE(_bits_23219)){
            _13599 = SEQ_PTR(_bits_23219)->length;
    }
    else {
        _13599 = 1;
    }
    if (_13599 >= _bit_23220)
    goto L6; // [86] 108

    /** 								bits &= repeat( 0, bit - length(bits)) */
    if (IS_SEQUENCE(_bits_23219)){
            _13601 = SEQ_PTR(_bits_23219)->length;
    }
    else {
        _13601 = 1;
    }
    _13602 = _bit_23220 - _13601;
    _13601 = NOVALUE;
    _13603 = Repeat(0, _13602);
    _13602 = NOVALUE;
    Concat((object_ptr)&_bits_23219, _bits_23219, _13603);
    DeRefDS(_13603);
    _13603 = NOVALUE;
L6: 

    /** 						bits[bit] += 1*/
    _2 = (int)SEQ_PTR(_bits_23219);
    _13605 = (int)*(((s1_ptr)_2)->base + _bit_23220);
    if (IS_ATOM_INT(_13605)) {
        _13606 = _13605 + 1;
        if (_13606 > MAXINT){
            _13606 = NewDouble((double)_13606);
        }
    }
    else
    _13606 = binary_op(PLUS, 1, _13605);
    _13605 = NOVALUE;
    _2 = (int)SEQ_PTR(_bits_23219);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _bits_23219 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bit_23220);
    _1 = *(int *)_2;
    *(int *)_2 = _13606;
    if( _1 != _13606 ){
        DeRef(_1);
    }
    _13606 = NOVALUE;

    /** 						decimals = borrow( add( decimals, -sub ), 10 )*/
    _13607 = unary_op(UMINUS, _sub_23218);
    RefDS(_decimals_23217);
    _13608 = _63add(_decimals_23217, _13607);
    _13607 = NOVALUE;
    _0 = _decimals_23217;
    _decimals_23217 = _63borrow(_13608, 10);
    DeRefDS(_0);
    _13608 = NOVALUE;
L5: 

    /** 				sub = half( sub )*/
    RefDS(_sub_23218);
    _0 = _sub_23218;
    _sub_23218 = _63half(_sub_23218);
    DeRefDS(_0);

    /** 				bit += 1*/
    _bit_23220 = _bit_23220 + 1;

    /** 			end while*/
    goto L2; // [156] 41
L4: 
L1: 

    /** 		return reverse(bits)*/
    RefDS(_bits_23219);
    _13612 = _63reverse(_bits_23219);
    DeRefDS(_decimals_23217);
    DeRef(_sub_23218);
    DeRefDS(_bits_23219);
    DeRef(_13592);
    _13592 = NOVALUE;
    return _13612;
    ;
}


int _63string_to_int(int _s_23253)
{
    int _int_23254 = NOVALUE;
    int _13616 = NOVALUE;
    int _13615 = NOVALUE;
    int _13613 = NOVALUE;
    int _0, _1, _2;
    

    /** 		int = 0*/
    _int_23254 = 0;

    /** 		for i = 1 to length(s) do*/
    if (IS_SEQUENCE(_s_23253)){
            _13613 = SEQ_PTR(_s_23253)->length;
    }
    else {
        _13613 = 1;
    }
    {
        int _i_23256;
        _i_23256 = 1;
L1: 
        if (_i_23256 > _13613){
            goto L2; // [13] 51
        }

        /** 				int *= 10*/
        _int_23254 = _int_23254 * 10;

        /** 				int += s[i] - '0'*/
        _2 = (int)SEQ_PTR(_s_23253);
        _13615 = (int)*(((s1_ptr)_2)->base + _i_23256);
        if (IS_ATOM_INT(_13615)) {
            _13616 = _13615 - 48;
            if ((long)((unsigned long)_13616 +(unsigned long) HIGH_BITS) >= 0){
                _13616 = NewDouble((double)_13616);
            }
        }
        else {
            _13616 = binary_op(MINUS, _13615, 48);
        }
        _13615 = NOVALUE;
        if (IS_ATOM_INT(_13616)) {
            _int_23254 = _int_23254 + _13616;
        }
        else {
            _int_23254 = binary_op(PLUS, _int_23254, _13616);
        }
        DeRef(_13616);
        _13616 = NOVALUE;
        if (!IS_ATOM_INT(_int_23254)) {
            _1 = (long)(DBL_PTR(_int_23254)->dbl);
            if (UNIQUE(DBL_PTR(_int_23254)) && (DBL_PTR(_int_23254)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_int_23254);
            _int_23254 = _1;
        }

        /** 		end for*/
        _i_23256 = _i_23256 + 1;
        goto L1; // [46] 20
L2: 
        ;
    }

    /** 		return int*/
    DeRefDS(_s_23253);
    return _int_23254;
    ;
}


int _63trim_bits(int _bits_23264)
{
    int _13624 = NOVALUE;
    int _13623 = NOVALUE;
    int _13622 = NOVALUE;
    int _13621 = NOVALUE;
    int _13620 = NOVALUE;
    int _13619 = NOVALUE;
    int _13618 = NOVALUE;
    int _0, _1, _2;
    

    /** 		while length(bits) and not bits[$] do*/
L1: 
    if (IS_SEQUENCE(_bits_23264)){
            _13618 = SEQ_PTR(_bits_23264)->length;
    }
    else {
        _13618 = 1;
    }
    if (_13618 == 0) {
        goto L2; // [11] 48
    }
    if (IS_SEQUENCE(_bits_23264)){
            _13620 = SEQ_PTR(_bits_23264)->length;
    }
    else {
        _13620 = 1;
    }
    _2 = (int)SEQ_PTR(_bits_23264);
    _13621 = (int)*(((s1_ptr)_2)->base + _13620);
    if (IS_ATOM_INT(_13621)) {
        _13622 = (_13621 == 0);
    }
    else {
        _13622 = unary_op(NOT, _13621);
    }
    _13621 = NOVALUE;
    if (_13622 <= 0) {
        if (_13622 == 0) {
            DeRef(_13622);
            _13622 = NOVALUE;
            goto L2; // [26] 48
        }
        else {
            if (!IS_ATOM_INT(_13622) && DBL_PTR(_13622)->dbl == 0.0){
                DeRef(_13622);
                _13622 = NOVALUE;
                goto L2; // [26] 48
            }
            DeRef(_13622);
            _13622 = NOVALUE;
        }
    }
    DeRef(_13622);
    _13622 = NOVALUE;

    /** 				bits = bits[1..$-1]*/
    if (IS_SEQUENCE(_bits_23264)){
            _13623 = SEQ_PTR(_bits_23264)->length;
    }
    else {
        _13623 = 1;
    }
    _13624 = _13623 - 1;
    _13623 = NOVALUE;
    rhs_slice_target = (object_ptr)&_bits_23264;
    RHS_Slice(_bits_23264, 1, _13624);

    /** 		end while*/
    goto L1; // [45] 8
L2: 

    /** 		return bits*/
    DeRef(_13624);
    _13624 = NOVALUE;
    return _bits_23264;
    ;
}


int _63scientific_to_float64(int _s_23286)
{
    int _dp_23287 = NOVALUE;
    int _e_23288 = NOVALUE;
    int _exp_23289 = NOVALUE;
    int _almost_nothing_23290 = NOVALUE;
    int _carried_23291 = NOVALUE;
    int _int_bits_23292 = NOVALUE;
    int _frac_bits_23293 = NOVALUE;
    int _mbits_23294 = NOVALUE;
    int _ebits_23295 = NOVALUE;
    int _sbits_23296 = NOVALUE;
    int _mbits_len_23483 = NOVALUE;
    int _mbits_len_23504 = NOVALUE;
    int _13823 = NOVALUE;
    int _13822 = NOVALUE;
    int _13821 = NOVALUE;
    int _13820 = NOVALUE;
    int _13819 = NOVALUE;
    int _13818 = NOVALUE;
    int _13817 = NOVALUE;
    int _13815 = NOVALUE;
    int _13814 = NOVALUE;
    int _13809 = NOVALUE;
    int _13808 = NOVALUE;
    int _13807 = NOVALUE;
    int _13805 = NOVALUE;
    int _13804 = NOVALUE;
    int _13801 = NOVALUE;
    int _13800 = NOVALUE;
    int _13799 = NOVALUE;
    int _13798 = NOVALUE;
    int _13797 = NOVALUE;
    int _13796 = NOVALUE;
    int _13795 = NOVALUE;
    int _13793 = NOVALUE;
    int _13792 = NOVALUE;
    int _13791 = NOVALUE;
    int _13790 = NOVALUE;
    int _13788 = NOVALUE;
    int _13787 = NOVALUE;
    int _13784 = NOVALUE;
    int _13783 = NOVALUE;
    int _13782 = NOVALUE;
    int _13781 = NOVALUE;
    int _13780 = NOVALUE;
    int _13779 = NOVALUE;
    int _13778 = NOVALUE;
    int _13775 = NOVALUE;
    int _13772 = NOVALUE;
    int _13771 = NOVALUE;
    int _13770 = NOVALUE;
    int _13767 = NOVALUE;
    int _13766 = NOVALUE;
    int _13764 = NOVALUE;
    int _13763 = NOVALUE;
    int _13761 = NOVALUE;
    int _13759 = NOVALUE;
    int _13758 = NOVALUE;
    int _13756 = NOVALUE;
    int _13754 = NOVALUE;
    int _13753 = NOVALUE;
    int _13752 = NOVALUE;
    int _13751 = NOVALUE;
    int _13750 = NOVALUE;
    int _13749 = NOVALUE;
    int _13748 = NOVALUE;
    int _13747 = NOVALUE;
    int _13745 = NOVALUE;
    int _13744 = NOVALUE;
    int _13743 = NOVALUE;
    int _13742 = NOVALUE;
    int _13740 = NOVALUE;
    int _13738 = NOVALUE;
    int _13737 = NOVALUE;
    int _13736 = NOVALUE;
    int _13735 = NOVALUE;
    int _13734 = NOVALUE;
    int _13732 = NOVALUE;
    int _13731 = NOVALUE;
    int _13730 = NOVALUE;
    int _13729 = NOVALUE;
    int _13728 = NOVALUE;
    int _13727 = NOVALUE;
    int _13725 = NOVALUE;
    int _13724 = NOVALUE;
    int _13723 = NOVALUE;
    int _13722 = NOVALUE;
    int _13721 = NOVALUE;
    int _13719 = NOVALUE;
    int _13718 = NOVALUE;
    int _13716 = NOVALUE;
    int _13715 = NOVALUE;
    int _13713 = NOVALUE;
    int _13712 = NOVALUE;
    int _13711 = NOVALUE;
    int _13710 = NOVALUE;
    int _13709 = NOVALUE;
    int _13704 = NOVALUE;
    int _13701 = NOVALUE;
    int _13700 = NOVALUE;
    int _13699 = NOVALUE;
    int _13696 = NOVALUE;
    int _13695 = NOVALUE;
    int _13694 = NOVALUE;
    int _13693 = NOVALUE;
    int _13691 = NOVALUE;
    int _13690 = NOVALUE;
    int _13686 = NOVALUE;
    int _13685 = NOVALUE;
    int _13684 = NOVALUE;
    int _13683 = NOVALUE;
    int _13682 = NOVALUE;
    int _13680 = NOVALUE;
    int _13679 = NOVALUE;
    int _13677 = NOVALUE;
    int _13674 = NOVALUE;
    int _13673 = NOVALUE;
    int _13672 = NOVALUE;
    int _13671 = NOVALUE;
    int _13670 = NOVALUE;
    int _13668 = NOVALUE;
    int _13667 = NOVALUE;
    int _13666 = NOVALUE;
    int _13665 = NOVALUE;
    int _13663 = NOVALUE;
    int _13662 = NOVALUE;
    int _13661 = NOVALUE;
    int _13660 = NOVALUE;
    int _13658 = NOVALUE;
    int _13657 = NOVALUE;
    int _13655 = NOVALUE;
    int _13654 = NOVALUE;
    int _13653 = NOVALUE;
    int _13652 = NOVALUE;
    int _13650 = NOVALUE;
    int _13649 = NOVALUE;
    int _13643 = NOVALUE;
    int _13642 = NOVALUE;
    int _13641 = NOVALUE;
    int _13640 = NOVALUE;
    int _13639 = NOVALUE;
    int _13637 = NOVALUE;
    int _13635 = NOVALUE;
    int _13632 = NOVALUE;
    int _13630 = NOVALUE;
    int _0, _1, _2;
    

    /** 		almost_nothing = 0*/
    _almost_nothing_23290 = 0;

    /** 		carried = 0*/
    _carried_23291 = 0;

    /** 		if s[1] = '-' then*/
    _2 = (int)SEQ_PTR(_s_23286);
    _13630 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _13630, 45)){
        _13630 = NOVALUE;
        goto L1; // [19] 43
    }
    _13630 = NOVALUE;

    /** 				sbits = {1}*/
    RefDS(_13559);
    DeRefi(_sbits_23296);
    _sbits_23296 = _13559;

    /** 				s = s[2..$]*/
    if (IS_SEQUENCE(_s_23286)){
            _13632 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13632 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23286;
    RHS_Slice(_s_23286, 2, _13632);
    goto L2; // [40] 71
L1: 

    /** 				sbits = {0}*/
    _0 = _sbits_23296;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    _sbits_23296 = MAKE_SEQ(_1);
    DeRefi(_0);

    /** 				if s[1] = '+' then*/
    _2 = (int)SEQ_PTR(_s_23286);
    _13635 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _13635, 43)){
        _13635 = NOVALUE;
        goto L3; // [55] 70
    }
    _13635 = NOVALUE;

    /** 						s = s[2..$]*/
    if (IS_SEQUENCE(_s_23286)){
            _13637 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13637 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23286;
    RHS_Slice(_s_23286, 2, _13637);
L3: 
L2: 

    /** 		while length(s) and s[1] = '0' do*/
L4: 
    if (IS_SEQUENCE(_s_23286)){
            _13639 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13639 = 1;
    }
    if (_13639 == 0) {
        goto L5; // [79] 110
    }
    _2 = (int)SEQ_PTR(_s_23286);
    _13641 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_13641)) {
        _13642 = (_13641 == 48);
    }
    else {
        _13642 = binary_op(EQUALS, _13641, 48);
    }
    _13641 = NOVALUE;
    if (_13642 <= 0) {
        if (_13642 == 0) {
            DeRef(_13642);
            _13642 = NOVALUE;
            goto L5; // [92] 110
        }
        else {
            if (!IS_ATOM_INT(_13642) && DBL_PTR(_13642)->dbl == 0.0){
                DeRef(_13642);
                _13642 = NOVALUE;
                goto L5; // [92] 110
            }
            DeRef(_13642);
            _13642 = NOVALUE;
        }
    }
    DeRef(_13642);
    _13642 = NOVALUE;

    /** 			s = s[2..$]*/
    if (IS_SEQUENCE(_s_23286)){
            _13643 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13643 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23286;
    RHS_Slice(_s_23286, 2, _13643);

    /** 		end while*/
    goto L4; // [107] 76
L5: 

    /** 		dp = find('.', s)		*/
    _dp_23287 = find_from(46, _s_23286, 1);

    /** 		e = find( 'e', s )*/
    _e_23288 = find_from(101, _s_23286, 1);

    /** 		if not e then*/
    if (_e_23288 != 0)
    goto L6; // [126] 137

    /** 				e = find('E', s )*/
    _e_23288 = find_from(69, _s_23286, 1);
L6: 

    /** 		exp = 0*/
    _exp_23289 = 0;

    /** 		if s[e+1] = '-' then*/
    _13649 = _e_23288 + 1;
    _2 = (int)SEQ_PTR(_s_23286);
    _13650 = (int)*(((s1_ptr)_2)->base + _13649);
    if (binary_op_a(NOTEQ, _13650, 45)){
        _13650 = NOVALUE;
        goto L7; // [152] 183
    }
    _13650 = NOVALUE;

    /** 				exp -= string_to_int( s[e+2..$] )*/
    _13652 = _e_23288 + 2;
    if ((long)((unsigned long)_13652 + (unsigned long)HIGH_BITS) >= 0) 
    _13652 = NewDouble((double)_13652);
    if (IS_SEQUENCE(_s_23286)){
            _13653 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13653 = 1;
    }
    rhs_slice_target = (object_ptr)&_13654;
    RHS_Slice(_s_23286, _13652, _13653);
    _13655 = _63string_to_int(_13654);
    _13654 = NOVALUE;
    if (IS_ATOM_INT(_13655)) {
        _exp_23289 = _exp_23289 - _13655;
    }
    else {
        _exp_23289 = binary_op(MINUS, _exp_23289, _13655);
    }
    DeRef(_13655);
    _13655 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23289)) {
        _1 = (long)(DBL_PTR(_exp_23289)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23289)) && (DBL_PTR(_exp_23289)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23289);
        _exp_23289 = _1;
    }
    goto L8; // [180] 250
L7: 

    /** 				if s[e+1] = '+' then*/
    _13657 = _e_23288 + 1;
    _2 = (int)SEQ_PTR(_s_23286);
    _13658 = (int)*(((s1_ptr)_2)->base + _13657);
    if (binary_op_a(NOTEQ, _13658, 43)){
        _13658 = NOVALUE;
        goto L9; // [193] 224
    }
    _13658 = NOVALUE;

    /** 						exp += string_to_int( s[e+2..$] )*/
    _13660 = _e_23288 + 2;
    if ((long)((unsigned long)_13660 + (unsigned long)HIGH_BITS) >= 0) 
    _13660 = NewDouble((double)_13660);
    if (IS_SEQUENCE(_s_23286)){
            _13661 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13661 = 1;
    }
    rhs_slice_target = (object_ptr)&_13662;
    RHS_Slice(_s_23286, _13660, _13661);
    _13663 = _63string_to_int(_13662);
    _13662 = NOVALUE;
    if (IS_ATOM_INT(_13663)) {
        _exp_23289 = _exp_23289 + _13663;
    }
    else {
        _exp_23289 = binary_op(PLUS, _exp_23289, _13663);
    }
    DeRef(_13663);
    _13663 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23289)) {
        _1 = (long)(DBL_PTR(_exp_23289)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23289)) && (DBL_PTR(_exp_23289)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23289);
        _exp_23289 = _1;
    }
    goto LA; // [221] 249
L9: 

    /** 						exp += string_to_int( s[e+1..$] )*/
    _13665 = _e_23288 + 1;
    if (_13665 > MAXINT){
        _13665 = NewDouble((double)_13665);
    }
    if (IS_SEQUENCE(_s_23286)){
            _13666 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13666 = 1;
    }
    rhs_slice_target = (object_ptr)&_13667;
    RHS_Slice(_s_23286, _13665, _13666);
    _13668 = _63string_to_int(_13667);
    _13667 = NOVALUE;
    if (IS_ATOM_INT(_13668)) {
        _exp_23289 = _exp_23289 + _13668;
    }
    else {
        _exp_23289 = binary_op(PLUS, _exp_23289, _13668);
    }
    DeRef(_13668);
    _13668 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23289)) {
        _1 = (long)(DBL_PTR(_exp_23289)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23289)) && (DBL_PTR(_exp_23289)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23289);
        _exp_23289 = _1;
    }
LA: 
L8: 

    /** 		if dp then*/
    if (_dp_23287 == 0)
    {
        goto LB; // [252] 301
    }
    else{
    }

    /** 				s = s[1..dp-1] & s[dp+1..$]*/
    _13670 = _dp_23287 - 1;
    rhs_slice_target = (object_ptr)&_13671;
    RHS_Slice(_s_23286, 1, _13670);
    _13672 = _dp_23287 + 1;
    if (_13672 > MAXINT){
        _13672 = NewDouble((double)_13672);
    }
    if (IS_SEQUENCE(_s_23286)){
            _13673 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13673 = 1;
    }
    rhs_slice_target = (object_ptr)&_13674;
    RHS_Slice(_s_23286, _13672, _13673);
    Concat((object_ptr)&_s_23286, _13671, _13674);
    DeRefDS(_13671);
    _13671 = NOVALUE;
    DeRef(_13671);
    _13671 = NOVALUE;
    DeRefDS(_13674);
    _13674 = NOVALUE;

    /** 				e -= 1*/
    _e_23288 = _e_23288 - 1;

    /** 				exp -= e - dp*/
    _13677 = _e_23288 - _dp_23287;
    if ((long)((unsigned long)_13677 +(unsigned long) HIGH_BITS) >= 0){
        _13677 = NewDouble((double)_13677);
    }
    if (IS_ATOM_INT(_13677)) {
        _exp_23289 = _exp_23289 - _13677;
    }
    else {
        _exp_23289 = NewDouble((double)_exp_23289 - DBL_PTR(_13677)->dbl);
    }
    DeRef(_13677);
    _13677 = NOVALUE;
    if (!IS_ATOM_INT(_exp_23289)) {
        _1 = (long)(DBL_PTR(_exp_23289)->dbl);
        if (UNIQUE(DBL_PTR(_exp_23289)) && (DBL_PTR(_exp_23289)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_exp_23289);
        _exp_23289 = _1;
    }
LB: 

    /** 		s = s[1..e-1] - '0'*/
    _13679 = _e_23288 - 1;
    rhs_slice_target = (object_ptr)&_13680;
    RHS_Slice(_s_23286, 1, _13679);
    DeRefDS(_s_23286);
    _s_23286 = binary_op(MINUS, _13680, 48);
    DeRefDS(_13680);
    _13680 = NOVALUE;

    /** 		while length(s) and s[1] = 0 do*/
LC: 
    if (IS_SEQUENCE(_s_23286)){
            _13682 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13682 = 1;
    }
    if (_13682 == 0) {
        goto LD; // [326] 369
    }
    _2 = (int)SEQ_PTR(_s_23286);
    _13684 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_13684)) {
        _13685 = (_13684 == 0);
    }
    else {
        _13685 = binary_op(EQUALS, _13684, 0);
    }
    _13684 = NOVALUE;
    if (_13685 <= 0) {
        if (_13685 == 0) {
            DeRef(_13685);
            _13685 = NOVALUE;
            goto LD; // [339] 369
        }
        else {
            if (!IS_ATOM_INT(_13685) && DBL_PTR(_13685)->dbl == 0.0){
                DeRef(_13685);
                _13685 = NOVALUE;
                goto LD; // [339] 369
            }
            DeRef(_13685);
            _13685 = NOVALUE;
        }
    }
    DeRef(_13685);
    _13685 = NOVALUE;

    /** 			s = s[2..$]*/
    if (IS_SEQUENCE(_s_23286)){
            _13686 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13686 = 1;
    }
    rhs_slice_target = (object_ptr)&_s_23286;
    RHS_Slice(_s_23286, 2, _13686);

    /** 			e -= 1*/
    _e_23288 = _e_23288 - 1;

    /** 			dp -= 1*/
    _dp_23287 = _dp_23287 - 1;

    /** 		end while*/
    goto LC; // [366] 323
LD: 

    /** 		if not find(0, s = 0) then*/
    _13690 = binary_op(EQUALS, _s_23286, 0);
    _13691 = find_from(0, _13690, 1);
    DeRefDS(_13690);
    _13690 = NOVALUE;
    if (_13691 != 0)
    goto LE; // [380] 394
    _13691 = NOVALUE;

    /** 			return atom_to_float64(0)*/
    _13693 = _11atom_to_float64(0);
    DeRefDS(_s_23286);
    DeRef(_int_bits_23292);
    DeRef(_frac_bits_23293);
    DeRef(_mbits_23294);
    DeRef(_ebits_23295);
    DeRefi(_sbits_23296);
    DeRef(_13649);
    _13649 = NOVALUE;
    DeRef(_13652);
    _13652 = NOVALUE;
    DeRef(_13657);
    _13657 = NOVALUE;
    DeRef(_13660);
    _13660 = NOVALUE;
    DeRef(_13665);
    _13665 = NOVALUE;
    DeRef(_13670);
    _13670 = NOVALUE;
    DeRef(_13679);
    _13679 = NOVALUE;
    DeRef(_13672);
    _13672 = NOVALUE;
    return _13693;
LE: 

    /** 		if exp + length(s) - 1 > 308 then*/
    if (IS_SEQUENCE(_s_23286)){
            _13694 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13694 = 1;
    }
    _13695 = _exp_23289 + _13694;
    if ((long)((unsigned long)_13695 + (unsigned long)HIGH_BITS) >= 0) 
    _13695 = NewDouble((double)_13695);
    _13694 = NOVALUE;
    if (IS_ATOM_INT(_13695)) {
        _13696 = _13695 - 1;
        if ((long)((unsigned long)_13696 +(unsigned long) HIGH_BITS) >= 0){
            _13696 = NewDouble((double)_13696);
        }
    }
    else {
        _13696 = NewDouble(DBL_PTR(_13695)->dbl - (double)1);
    }
    DeRef(_13695);
    _13695 = NOVALUE;
    if (binary_op_a(LESSEQ, _13696, 308)){
        DeRef(_13696);
        _13696 = NOVALUE;
        goto LF; // [407] 425
    }
    DeRef(_13696);
    _13696 = NOVALUE;

    /** 			exp = 1024*/
    _exp_23289 = 1024;

    /** 			mbits = repeat(0, 52)*/
    DeRef(_mbits_23294);
    _mbits_23294 = Repeat(0, 52);
    goto L10; // [422] 1057
LF: 

    /** 		elsif exp + length(s) - 1 < -324 then -- -324 = floor((-1022-52)*log(2)/log(10))*/
    if (IS_SEQUENCE(_s_23286)){
            _13699 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13699 = 1;
    }
    _13700 = _exp_23289 + _13699;
    if ((long)((unsigned long)_13700 + (unsigned long)HIGH_BITS) >= 0) 
    _13700 = NewDouble((double)_13700);
    _13699 = NOVALUE;
    if (IS_ATOM_INT(_13700)) {
        _13701 = _13700 - 1;
        if ((long)((unsigned long)_13701 +(unsigned long) HIGH_BITS) >= 0){
            _13701 = NewDouble((double)_13701);
        }
    }
    else {
        _13701 = NewDouble(DBL_PTR(_13700)->dbl - (double)1);
    }
    DeRef(_13700);
    _13700 = NOVALUE;
    if (binary_op_a(GREATEREQ, _13701, -324)){
        DeRef(_13701);
        _13701 = NOVALUE;
        goto L11; // [438] 464
    }
    DeRef(_13701);
    _13701 = NOVALUE;

    /** 			fenv:raise(FE_UNDERFLOW)*/
    _13704 = _64raise(117);

    /** 			exp = -1023*/
    _exp_23289 = -1023;

    /** 			mbits = repeat(0, 52)*/
    DeRef(_mbits_23294);
    _mbits_23294 = Repeat(0, 52);
    goto L10; // [461] 1057
L11: 

    /** 			if exp >= 0 then*/
    if (_exp_23289 < 0)
    goto L12; // [466] 510

    /** 					int_bits = trim_bits( bytes_to_bits( convert_radix( repeat( 0, exp ) & reverse( s ), 10, #100 ) ) )*/
    _13709 = Repeat(0, _exp_23289);
    RefDS(_s_23286);
    _13710 = _63reverse(_s_23286);
    if (IS_SEQUENCE(_13709) && IS_ATOM(_13710)) {
        Ref(_13710);
        Append(&_13711, _13709, _13710);
    }
    else if (IS_ATOM(_13709) && IS_SEQUENCE(_13710)) {
    }
    else {
        Concat((object_ptr)&_13711, _13709, _13710);
        DeRefDS(_13709);
        _13709 = NOVALUE;
    }
    DeRef(_13709);
    _13709 = NOVALUE;
    DeRef(_13710);
    _13710 = NOVALUE;
    _13712 = _63convert_radix(_13711, 10, 256);
    _13711 = NOVALUE;
    _13713 = _63bytes_to_bits(_13712);
    _13712 = NOVALUE;
    _0 = _int_bits_23292;
    _int_bits_23292 = _63trim_bits(_13713);
    DeRef(_0);
    _13713 = NOVALUE;

    /** 					frac_bits = {}*/
    RefDS(_5);
    DeRef(_frac_bits_23293);
    _frac_bits_23293 = _5;
    goto L13; // [507] 637
L12: 

    /** 					almost_nothing = exp + e - dp = -324*/
    _13715 = _exp_23289 + _e_23288;
    if ((long)((unsigned long)_13715 + (unsigned long)HIGH_BITS) >= 0) 
    _13715 = NewDouble((double)_13715);
    if (IS_ATOM_INT(_13715)) {
        _13716 = _13715 - _dp_23287;
        if ((long)((unsigned long)_13716 +(unsigned long) HIGH_BITS) >= 0){
            _13716 = NewDouble((double)_13716);
        }
    }
    else {
        _13716 = NewDouble(DBL_PTR(_13715)->dbl - (double)_dp_23287);
    }
    DeRef(_13715);
    _13715 = NOVALUE;
    if (IS_ATOM_INT(_13716)) {
        _almost_nothing_23290 = (_13716 == -324);
    }
    else {
        _almost_nothing_23290 = (DBL_PTR(_13716)->dbl == (double)-324);
    }
    DeRef(_13716);
    _13716 = NOVALUE;

    /** 					if -exp > length(s) then*/
    if ((unsigned long)_exp_23289 == 0xC0000000)
    _13718 = (int)NewDouble((double)-0xC0000000);
    else
    _13718 = - _exp_23289;
    if (IS_SEQUENCE(_s_23286)){
            _13719 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13719 = 1;
    }
    if (binary_op_a(LESSEQ, _13718, _13719)){
        DeRef(_13718);
        _13718 = NOVALUE;
        _13719 = NOVALUE;
        goto L14; // [534] 574
    }
    DeRef(_13718);
    _13718 = NOVALUE;
    _13719 = NOVALUE;

    /** 							int_bits = {}*/
    RefDS(_5);
    DeRef(_int_bits_23292);
    _int_bits_23292 = _5;

    /** 							frac_bits = decimals_to_bits( repeat( 0, -exp-length(s) ) & s )*/
    if ((unsigned long)_exp_23289 == 0xC0000000)
    _13721 = (int)NewDouble((double)-0xC0000000);
    else
    _13721 = - _exp_23289;
    if (IS_SEQUENCE(_s_23286)){
            _13722 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13722 = 1;
    }
    if (IS_ATOM_INT(_13721)) {
        _13723 = _13721 - _13722;
    }
    else {
        _13723 = NewDouble(DBL_PTR(_13721)->dbl - (double)_13722);
    }
    DeRef(_13721);
    _13721 = NOVALUE;
    _13722 = NOVALUE;
    _13724 = Repeat(0, _13723);
    DeRef(_13723);
    _13723 = NOVALUE;
    Concat((object_ptr)&_13725, _13724, _s_23286);
    DeRefDS(_13724);
    _13724 = NOVALUE;
    DeRef(_13724);
    _13724 = NOVALUE;
    _0 = _frac_bits_23293;
    _frac_bits_23293 = _63decimals_to_bits(_13725);
    DeRef(_0);
    _13725 = NOVALUE;
    goto L15; // [571] 636
L14: 

    /** 							int_bits = trim_bits( bytes_to_bits( convert_radix( reverse( s[1..$+exp] ), 10, #100 ) ) )*/
    if (IS_SEQUENCE(_s_23286)){
            _13727 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13727 = 1;
    }
    _13728 = _13727 + _exp_23289;
    _13727 = NOVALUE;
    rhs_slice_target = (object_ptr)&_13729;
    RHS_Slice(_s_23286, 1, _13728);
    _13730 = _63reverse(_13729);
    _13729 = NOVALUE;
    _13731 = _63convert_radix(_13730, 10, 256);
    _13730 = NOVALUE;
    _13732 = _63bytes_to_bits(_13731);
    _13731 = NOVALUE;
    _0 = _int_bits_23292;
    _int_bits_23292 = _63trim_bits(_13732);
    DeRef(_0);
    _13732 = NOVALUE;

    /** 							frac_bits =  decimals_to_bits( s[$+exp+1..$] )*/
    if (IS_SEQUENCE(_s_23286)){
            _13734 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13734 = 1;
    }
    _13735 = _13734 + _exp_23289;
    if ((long)((unsigned long)_13735 + (unsigned long)HIGH_BITS) >= 0) 
    _13735 = NewDouble((double)_13735);
    _13734 = NOVALUE;
    if (IS_ATOM_INT(_13735)) {
        _13736 = _13735 + 1;
        if (_13736 > MAXINT){
            _13736 = NewDouble((double)_13736);
        }
    }
    else
    _13736 = binary_op(PLUS, 1, _13735);
    DeRef(_13735);
    _13735 = NOVALUE;
    if (IS_SEQUENCE(_s_23286)){
            _13737 = SEQ_PTR(_s_23286)->length;
    }
    else {
        _13737 = 1;
    }
    rhs_slice_target = (object_ptr)&_13738;
    RHS_Slice(_s_23286, _13736, _13737);
    _0 = _frac_bits_23293;
    _frac_bits_23293 = _63decimals_to_bits(_13738);
    DeRef(_0);
    _13738 = NOVALUE;
L15: 
L13: 

    /** 			if length(int_bits) >= 53 then*/
    if (IS_SEQUENCE(_int_bits_23292)){
            _13740 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13740 = 1;
    }
    if (_13740 < 53)
    goto L16; // [644] 762

    /** 				mbits = int_bits[$-52..$-1]*/
    if (IS_SEQUENCE(_int_bits_23292)){
            _13742 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13742 = 1;
    }
    _13743 = _13742 - 52;
    _13742 = NOVALUE;
    if (IS_SEQUENCE(_int_bits_23292)){
            _13744 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13744 = 1;
    }
    _13745 = _13744 - 1;
    _13744 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mbits_23294;
    RHS_Slice(_int_bits_23292, _13743, _13745);

    /** 				if length(int_bits) > 53 and int_bits[$-53] then*/
    if (IS_SEQUENCE(_int_bits_23292)){
            _13747 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13747 = 1;
    }
    _13748 = (_13747 > 53);
    _13747 = NOVALUE;
    if (_13748 == 0) {
        goto L17; // [678] 750
    }
    if (IS_SEQUENCE(_int_bits_23292)){
            _13750 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13750 = 1;
    }
    _13751 = _13750 - 53;
    _13750 = NOVALUE;
    _2 = (int)SEQ_PTR(_int_bits_23292);
    _13752 = (int)*(((s1_ptr)_2)->base + _13751);
    if (_13752 == 0) {
        _13752 = NOVALUE;
        goto L17; // [694] 750
    }
    else {
        if (!IS_ATOM_INT(_13752) && DBL_PTR(_13752)->dbl == 0.0){
            _13752 = NOVALUE;
            goto L17; // [694] 750
        }
        _13752 = NOVALUE;
    }
    _13752 = NOVALUE;

    /** 						mbits[1] += 1*/
    _2 = (int)SEQ_PTR(_mbits_23294);
    _13753 = (int)*(((s1_ptr)_2)->base + 1);
    if (IS_ATOM_INT(_13753)) {
        _13754 = _13753 + 1;
        if (_13754 > MAXINT){
            _13754 = NewDouble((double)_13754);
        }
    }
    else
    _13754 = binary_op(PLUS, 1, _13753);
    _13753 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23294);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _mbits_23294 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _13754;
    if( _1 != _13754 ){
        DeRef(_1);
    }
    _13754 = NOVALUE;

    /** 						mbits = carry( mbits, 2 )*/
    RefDS(_mbits_23294);
    _0 = _mbits_23294;
    _mbits_23294 = _63carry(_mbits_23294, 2);
    DeRefDS(_0);

    /** 						if length(mbits) = 53 then*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13756 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13756 = 1;
    }
    if (_13756 != 53)
    goto L18; // [725] 749

    /** 							mbits = mbits[1..$-1]*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13758 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13758 = 1;
    }
    _13759 = _13758 - 1;
    _13758 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mbits_23294;
    RHS_Slice(_mbits_23294, 1, _13759);

    /** 							carried = 1*/
    _carried_23291 = 1;
L18: 
L17: 

    /** 				exp = length(int_bits)-1*/
    if (IS_SEQUENCE(_int_bits_23292)){
            _13761 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13761 = 1;
    }
    _exp_23289 = _13761 - 1;
    _13761 = NOVALUE;
    goto L19; // [759] 1050
L16: 

    /** 					if length(int_bits) then*/
    if (IS_SEQUENCE(_int_bits_23292)){
            _13763 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13763 = 1;
    }
    if (_13763 == 0)
    {
        _13763 = NOVALUE;
        goto L1A; // [767] 782
    }
    else{
        _13763 = NOVALUE;
    }

    /** 							exp = length(int_bits)-1*/
    if (IS_SEQUENCE(_int_bits_23292)){
            _13764 = SEQ_PTR(_int_bits_23292)->length;
    }
    else {
        _13764 = 1;
    }
    _exp_23289 = _13764 - 1;
    _13764 = NOVALUE;
    goto L1B; // [779] 837
L1A: 

    /** 							exp = - find( 1, reverse( frac_bits ) )*/
    RefDS(_frac_bits_23293);
    _13766 = _63reverse(_frac_bits_23293);
    _13767 = find_from(1, _13766, 1);
    DeRef(_13766);
    _13766 = NOVALUE;
    _exp_23289 = - _13767;

    /** 							if exp < -1023 then*/
    if (_exp_23289 >= -1023)
    goto L1C; // [802] 812

    /** 									exp = -1023*/
    _exp_23289 = -1023;
L1C: 

    /** 							if exp then*/
    if (_exp_23289 == 0)
    {
        goto L1D; // [814] 836
    }
    else{
    }

    /** 									frac_bits = frac_bits[1..$+exp+1]*/
    if (IS_SEQUENCE(_frac_bits_23293)){
            _13770 = SEQ_PTR(_frac_bits_23293)->length;
    }
    else {
        _13770 = 1;
    }
    _13771 = _13770 + _exp_23289;
    if ((long)((unsigned long)_13771 + (unsigned long)HIGH_BITS) >= 0) 
    _13771 = NewDouble((double)_13771);
    _13770 = NOVALUE;
    if (IS_ATOM_INT(_13771)) {
        _13772 = _13771 + 1;
    }
    else
    _13772 = binary_op(PLUS, 1, _13771);
    DeRef(_13771);
    _13771 = NOVALUE;
    rhs_slice_target = (object_ptr)&_frac_bits_23293;
    RHS_Slice(_frac_bits_23293, 1, _13772);
L1D: 
L1B: 

    /** 					mbits = frac_bits & int_bits*/
    Concat((object_ptr)&_mbits_23294, _frac_bits_23293, _int_bits_23292);

    /** 					mbits = repeat( 0, 53 ) & mbits*/
    _13775 = Repeat(0, 53);
    Concat((object_ptr)&_mbits_23294, _13775, _mbits_23294);
    DeRefDS(_13775);
    _13775 = NOVALUE;
    DeRef(_13775);
    _13775 = NOVALUE;

    /** 					if exp > -1023 then*/
    if (_exp_23289 <= -1023)
    goto L1E; // [857] 958

    /** 							if mbits[$-53] then*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13778 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13778 = 1;
    }
    _13779 = _13778 - 53;
    _13778 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23294);
    _13780 = (int)*(((s1_ptr)_2)->base + _13779);
    if (_13780 == 0) {
        _13780 = NOVALUE;
        goto L1F; // [874] 932
    }
    else {
        if (!IS_ATOM_INT(_13780) && DBL_PTR(_13780)->dbl == 0.0){
            _13780 = NOVALUE;
            goto L1F; // [874] 932
        }
        _13780 = NOVALUE;
    }
    _13780 = NOVALUE;

    /** 									mbits[$-52] += 1*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13781 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13781 = 1;
    }
    _13782 = _13781 - 52;
    _13781 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23294);
    _13783 = (int)*(((s1_ptr)_2)->base + _13782);
    if (IS_ATOM_INT(_13783)) {
        _13784 = _13783 + 1;
        if (_13784 > MAXINT){
            _13784 = NewDouble((double)_13784);
        }
    }
    else
    _13784 = binary_op(PLUS, 1, _13783);
    _13783 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23294);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _mbits_23294 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _13782);
    _1 = *(int *)_2;
    *(int *)_2 = _13784;
    if( _1 != _13784 ){
        DeRef(_1);
    }
    _13784 = NOVALUE;

    /** 									integer mbits_len = length(mbits)*/
    if (IS_SEQUENCE(_mbits_23294)){
            _mbits_len_23483 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _mbits_len_23483 = 1;
    }

    /** 									mbits = carry( mbits, 2 )*/
    RefDS(_mbits_23294);
    _0 = _mbits_23294;
    _mbits_23294 = _63carry(_mbits_23294, 2);
    DeRefDS(_0);

    /** 									if length(mbits) = mbits_len + 1 then*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13787 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13787 = 1;
    }
    _13788 = _mbits_len_23483 + 1;
    if (_13787 != _13788)
    goto L20; // [921] 931

    /** 										carried = 1*/
    _carried_23291 = 1;
L20: 
L1F: 

    /** 							mbits = mbits[$-52..$-1]*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13790 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13790 = 1;
    }
    _13791 = _13790 - 52;
    _13790 = NOVALUE;
    if (IS_SEQUENCE(_mbits_23294)){
            _13792 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13792 = 1;
    }
    _13793 = _13792 - 1;
    _13792 = NOVALUE;
    rhs_slice_target = (object_ptr)&_mbits_23294;
    RHS_Slice(_mbits_23294, _13791, _13793);
    goto L21; // [955] 1049
L1E: 

    /** 							if mbits[$-52] then*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13795 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13795 = 1;
    }
    _13796 = _13795 - 52;
    _13795 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23294);
    _13797 = (int)*(((s1_ptr)_2)->base + _13796);
    if (_13797 == 0) {
        _13797 = NOVALUE;
        goto L22; // [971] 1029
    }
    else {
        if (!IS_ATOM_INT(_13797) && DBL_PTR(_13797)->dbl == 0.0){
            _13797 = NOVALUE;
            goto L22; // [971] 1029
        }
        _13797 = NOVALUE;
    }
    _13797 = NOVALUE;

    /** 									mbits[$-52] += 1*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13798 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13798 = 1;
    }
    _13799 = _13798 - 52;
    _13798 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23294);
    _13800 = (int)*(((s1_ptr)_2)->base + _13799);
    if (IS_ATOM_INT(_13800)) {
        _13801 = _13800 + 1;
        if (_13801 > MAXINT){
            _13801 = NewDouble((double)_13801);
        }
    }
    else
    _13801 = binary_op(PLUS, 1, _13800);
    _13800 = NOVALUE;
    _2 = (int)SEQ_PTR(_mbits_23294);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _mbits_23294 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _13799);
    _1 = *(int *)_2;
    *(int *)_2 = _13801;
    if( _1 != _13801 ){
        DeRef(_1);
    }
    _13801 = NOVALUE;

    /** 									integer mbits_len = length(mbits)*/
    if (IS_SEQUENCE(_mbits_23294)){
            _mbits_len_23504 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _mbits_len_23504 = 1;
    }

    /** 									mbits = carry( mbits, 2 )*/
    RefDS(_mbits_23294);
    _0 = _mbits_23294;
    _mbits_23294 = _63carry(_mbits_23294, 2);
    DeRefDS(_0);

    /** 									if length(mbits) = mbits_len + 1 then*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13804 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13804 = 1;
    }
    _13805 = _mbits_len_23504 + 1;
    if (_13804 != _13805)
    goto L23; // [1018] 1028

    /** 										carried = 1*/
    _carried_23291 = 1;
L23: 
L22: 

    /** 							mbits = mbits[$-51..$]*/
    if (IS_SEQUENCE(_mbits_23294)){
            _13807 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13807 = 1;
    }
    _13808 = _13807 - 51;
    _13807 = NOVALUE;
    if (IS_SEQUENCE(_mbits_23294)){
            _13809 = SEQ_PTR(_mbits_23294)->length;
    }
    else {
        _13809 = 1;
    }
    rhs_slice_target = (object_ptr)&_mbits_23294;
    RHS_Slice(_mbits_23294, _13808, _13809);
L21: 
L19: 

    /** 			exp += carried*/
    _exp_23289 = _exp_23289 + _carried_23291;
L10: 

    /** 		if exp >= 1024 then*/
    if (_exp_23289 < 1024)
    goto L24; // [1059] 1081

    /** 			exp   = 1024*/
    _exp_23289 = 1024;

    /** 			mbits = repeat(0, 52)*/
    DeRef(_mbits_23294);
    _mbits_23294 = Repeat(0, 52);

    /** 			fenv:raise(fenv:FE_OVERFLOW)*/
    _13814 = _64raise(111);
L24: 

    /** 		ebits = int_to_bits( exp + 1023, 11 )*/
    _13815 = _exp_23289 + 1023;
    if ((long)((unsigned long)_13815 + (unsigned long)HIGH_BITS) >= 0) 
    _13815 = NewDouble((double)_13815);
    _0 = _ebits_23295;
    _ebits_23295 = _11int_to_bits(_13815, 11);
    DeRef(_0);
    _13815 = NOVALUE;

    /** 		if almost_nothing and not find(1, mbits & ebits) then*/
    if (_almost_nothing_23290 == 0) {
        goto L25; // [1096] 1125
    }
    Concat((object_ptr)&_13818, _mbits_23294, _ebits_23295);
    _13819 = find_from(1, _13818, 1);
    DeRefDS(_13818);
    _13818 = NOVALUE;
    _13820 = (_13819 == 0);
    _13819 = NOVALUE;
    if (_13820 == 0)
    {
        DeRef(_13820);
        _13820 = NOVALUE;
        goto L25; // [1115] 1125
    }
    else{
        DeRef(_13820);
        _13820 = NOVALUE;
    }

    /** 			fenv:raise(fenv:FE_UNDERFLOW)*/
    _13821 = _64raise(117);
L25: 

    /** 		return bits_to_bytes( mbits & ebits & sbits )*/
    {
        int concat_list[3];

        concat_list[0] = _sbits_23296;
        concat_list[1] = _ebits_23295;
        concat_list[2] = _mbits_23294;
        Concat_N((object_ptr)&_13822, concat_list, 3);
    }
    _13823 = _63bits_to_bytes(_13822);
    _13822 = NOVALUE;
    DeRefDS(_s_23286);
    DeRef(_int_bits_23292);
    DeRef(_frac_bits_23293);
    DeRefDS(_mbits_23294);
    DeRefDS(_ebits_23295);
    DeRefDSi(_sbits_23296);
    DeRef(_13693);
    _13693 = NOVALUE;
    DeRef(_13660);
    _13660 = NOVALUE;
    DeRef(_13649);
    _13649 = NOVALUE;
    DeRef(_13728);
    _13728 = NOVALUE;
    DeRef(_13779);
    _13779 = NOVALUE;
    DeRef(_13788);
    _13788 = NOVALUE;
    DeRef(_13679);
    _13679 = NOVALUE;
    DeRef(_13759);
    _13759 = NOVALUE;
    DeRef(_13772);
    _13772 = NOVALUE;
    DeRef(_13793);
    _13793 = NOVALUE;
    DeRef(_13791);
    _13791 = NOVALUE;
    DeRef(_13665);
    _13665 = NOVALUE;
    DeRef(_13821);
    _13821 = NOVALUE;
    DeRef(_13796);
    _13796 = NOVALUE;
    DeRef(_13736);
    _13736 = NOVALUE;
    DeRef(_13814);
    _13814 = NOVALUE;
    DeRef(_13743);
    _13743 = NOVALUE;
    DeRef(_13672);
    _13672 = NOVALUE;
    DeRef(_13751);
    _13751 = NOVALUE;
    DeRef(_13799);
    _13799 = NOVALUE;
    DeRef(_13805);
    _13805 = NOVALUE;
    DeRef(_13808);
    _13808 = NOVALUE;
    DeRef(_13704);
    _13704 = NOVALUE;
    DeRef(_13745);
    _13745 = NOVALUE;
    DeRef(_13782);
    _13782 = NOVALUE;
    DeRef(_13657);
    _13657 = NOVALUE;
    DeRef(_13652);
    _13652 = NOVALUE;
    DeRef(_13748);
    _13748 = NOVALUE;
    DeRef(_13670);
    _13670 = NOVALUE;
    return _13823;
    ;
}


int _63scientific_to_atom(int _s_23533)
{
    int _13825 = NOVALUE;
    int _13824 = NOVALUE;
    int _0, _1, _2;
    

    /** 		return float64_to_atom( scientific_to_float64( s ) )*/
    RefDS(_s_23533);
    _13824 = _63scientific_to_float64(_s_23533);
    _13825 = _11float64_to_atom(_13824);
    _13824 = NOVALUE;
    DeRefDS(_s_23533);
    return _13825;
    ;
}



// 0x036E9EA3
