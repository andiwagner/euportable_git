// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _29map(int _obj_p_11818)
{
    int _m__11822 = NOVALUE;
    int _6684 = NOVALUE;
    int _6683 = NOVALUE;
    int _6682 = NOVALUE;
    int _6681 = NOVALUE;
    int _6680 = NOVALUE;
    int _6679 = NOVALUE;
    int _6678 = NOVALUE;
    int _6677 = NOVALUE;
    int _6676 = NOVALUE;
    int _6675 = NOVALUE;
    int _6673 = NOVALUE;
    int _6672 = NOVALUE;
    int _6671 = NOVALUE;
    int _6670 = NOVALUE;
    int _6668 = NOVALUE;
    int _6667 = NOVALUE;
    int _6666 = NOVALUE;
    int _6665 = NOVALUE;
    int _6663 = NOVALUE;
    int _6662 = NOVALUE;
    int _6661 = NOVALUE;
    int _6660 = NOVALUE;
    int _6659 = NOVALUE;
    int _6658 = NOVALUE;
    int _6657 = NOVALUE;
    int _6656 = NOVALUE;
    int _6655 = NOVALUE;
    int _6654 = NOVALUE;
    int _6652 = NOVALUE;
    int _6650 = NOVALUE;
    int _6649 = NOVALUE;
    int _6647 = NOVALUE;
    int _6645 = NOVALUE;
    int _6644 = NOVALUE;
    int _6642 = NOVALUE;
    int _6641 = NOVALUE;
    int _6639 = NOVALUE;
    int _6637 = NOVALUE;
    int _6635 = NOVALUE;
    int _6632 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not eumem:valid(obj_p, "") then return 0 end if*/
    Ref(_obj_p_11818);
    RefDS(_5);
    _6632 = _30valid(_obj_p_11818, _5);
    if (IS_ATOM_INT(_6632)) {
        if (_6632 != 0){
            DeRef(_6632);
            _6632 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    else {
        if (DBL_PTR(_6632)->dbl != 0.0){
            DeRef(_6632);
            _6632 = NOVALUE;
            goto L1; // [8] 16
        }
    }
    DeRef(_6632);
    _6632 = NOVALUE;
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L1: 

    /** 	object m_*/

    /** 	m_ = eumem:ram_space[obj_p]*/
    DeRef(_m__11822);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_obj_p_11818)){
        _m__11822 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_obj_p_11818)->dbl));
    }
    else{
        _m__11822 = (int)*(((s1_ptr)_2)->base + _obj_p_11818);
    }
    Ref(_m__11822);

    /** 	if not sequence(m_) then return 0 end if*/
    _6635 = IS_SEQUENCE(_m__11822);
    if (_6635 != 0)
    goto L2; // [31] 39
    _6635 = NOVALUE;
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L2: 

    /** 	if length(m_) < 6 then return 0 end if*/
    if (IS_SEQUENCE(_m__11822)){
            _6637 = SEQ_PTR(_m__11822)->length;
    }
    else {
        _6637 = 1;
    }
    if (_6637 >= 6)
    goto L3; // [44] 53
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L3: 

    /** 	if length(m_) > 7 then return 0 end if*/
    if (IS_SEQUENCE(_m__11822)){
            _6639 = SEQ_PTR(_m__11822)->length;
    }
    else {
        _6639 = 1;
    }
    if (_6639 <= 7)
    goto L4; // [58] 67
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L4: 

    /** 	if not equal(m_[TYPE_TAG], type_is_map) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6641 = (int)*(((s1_ptr)_2)->base + 1);
    if (_6641 == _29type_is_map_11790)
    _6642 = 1;
    else if (IS_ATOM_INT(_6641) && IS_ATOM_INT(_29type_is_map_11790))
    _6642 = 0;
    else
    _6642 = (compare(_6641, _29type_is_map_11790) == 0);
    _6641 = NOVALUE;
    if (_6642 != 0)
    goto L5; // [79] 87
    _6642 = NOVALUE;
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L5: 

    /** 	if not integer(m_[ELEMENT_COUNT]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6644 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6644))
    _6645 = 1;
    else if (IS_ATOM_DBL(_6644))
    _6645 = IS_ATOM_INT(DoubleToInt(_6644));
    else
    _6645 = 0;
    _6644 = NOVALUE;
    if (_6645 != 0)
    goto L6; // [98] 106
    _6645 = NOVALUE;
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L6: 

    /** 	if m_[ELEMENT_COUNT] < 0 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6647 = (int)*(((s1_ptr)_2)->base + 2);
    if (binary_op_a(GREATEREQ, _6647, 0)){
        _6647 = NOVALUE;
        goto L7; // [114] 123
    }
    _6647 = NOVALUE;
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L7: 

    /** 	if not integer(m_[IN_USE]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6649 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_6649))
    _6650 = 1;
    else if (IS_ATOM_DBL(_6649))
    _6650 = IS_ATOM_INT(DoubleToInt(_6649));
    else
    _6650 = 0;
    _6649 = NOVALUE;
    if (_6650 != 0)
    goto L8; // [134] 142
    _6650 = NOVALUE;
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L8: 

    /** 	if m_[IN_USE] < 0		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6652 = (int)*(((s1_ptr)_2)->base + 3);
    if (binary_op_a(GREATEREQ, _6652, 0)){
        _6652 = NOVALUE;
        goto L9; // [150] 159
    }
    _6652 = NOVALUE;
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
L9: 

    /** 	if equal(m_[MAP_TYPE],SMALLMAP) then*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6654 = (int)*(((s1_ptr)_2)->base + 4);
    if (_6654 == 115)
    _6655 = 1;
    else if (IS_ATOM_INT(_6654) && IS_ATOM_INT(115))
    _6655 = 0;
    else
    _6655 = (compare(_6654, 115) == 0);
    _6654 = NOVALUE;
    if (_6655 == 0)
    {
        _6655 = NOVALUE;
        goto LA; // [171] 312
    }
    else{
        _6655 = NOVALUE;
    }

    /** 		if atom(m_[KEY_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6656 = (int)*(((s1_ptr)_2)->base + 5);
    _6657 = IS_ATOM(_6656);
    _6656 = NOVALUE;
    if (_6657 == 0)
    {
        _6657 = NOVALUE;
        goto LB; // [185] 193
    }
    else{
        _6657 = NOVALUE;
    }
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
LB: 

    /** 		if atom(m_[VALUE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6658 = (int)*(((s1_ptr)_2)->base + 6);
    _6659 = IS_ATOM(_6658);
    _6658 = NOVALUE;
    if (_6659 == 0)
    {
        _6659 = NOVALUE;
        goto LC; // [204] 212
    }
    else{
        _6659 = NOVALUE;
    }
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
LC: 

    /** 		if atom(m_[FREE_LIST]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6660 = (int)*(((s1_ptr)_2)->base + 7);
    _6661 = IS_ATOM(_6660);
    _6660 = NOVALUE;
    if (_6661 == 0)
    {
        _6661 = NOVALUE;
        goto LD; // [223] 231
    }
    else{
        _6661 = NOVALUE;
    }
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    return 0;
LD: 

    /** 		if length(m_[KEY_LIST]) = 0  then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6662 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6662)){
            _6663 = SEQ_PTR(_6662)->length;
    }
    else {
        _6663 = 1;
    }
    _6662 = NOVALUE;
    if (_6663 != 0)
    goto LE; // [242] 251
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    return 0;
LE: 

    /** 		if length(m_[KEY_LIST]) != length(m_[VALUE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6665 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6665)){
            _6666 = SEQ_PTR(_6665)->length;
    }
    else {
        _6666 = 1;
    }
    _6665 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11822);
    _6667 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_6667)){
            _6668 = SEQ_PTR(_6667)->length;
    }
    else {
        _6668 = 1;
    }
    _6667 = NOVALUE;
    if (_6666 == _6668)
    goto LF; // [271] 280
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    _6665 = NOVALUE;
    _6667 = NOVALUE;
    return 0;
LF: 

    /** 		if length(m_[KEY_LIST]) != length(m_[FREE_LIST]) then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6670 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6670)){
            _6671 = SEQ_PTR(_6670)->length;
    }
    else {
        _6671 = 1;
    }
    _6670 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11822);
    _6672 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_6672)){
            _6673 = SEQ_PTR(_6672)->length;
    }
    else {
        _6673 = 1;
    }
    _6672 = NOVALUE;
    if (_6671 == _6673)
    goto L10; // [300] 404
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    _6665 = NOVALUE;
    _6667 = NOVALUE;
    _6670 = NOVALUE;
    _6672 = NOVALUE;
    return 0;
    goto L10; // [309] 404
LA: 

    /** 	elsif  equal(m_[MAP_TYPE],LARGEMAP) then*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6675 = (int)*(((s1_ptr)_2)->base + 4);
    if (_6675 == 76)
    _6676 = 1;
    else if (IS_ATOM_INT(_6675) && IS_ATOM_INT(76))
    _6676 = 0;
    else
    _6676 = (compare(_6675, 76) == 0);
    _6675 = NOVALUE;
    if (_6676 == 0)
    {
        _6676 = NOVALUE;
        goto L11; // [324] 397
    }
    else{
        _6676 = NOVALUE;
    }

    /** 		if atom(m_[KEY_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6677 = (int)*(((s1_ptr)_2)->base + 5);
    _6678 = IS_ATOM(_6677);
    _6677 = NOVALUE;
    if (_6678 == 0)
    {
        _6678 = NOVALUE;
        goto L12; // [338] 346
    }
    else{
        _6678 = NOVALUE;
    }
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    _6665 = NOVALUE;
    _6667 = NOVALUE;
    _6670 = NOVALUE;
    _6672 = NOVALUE;
    return 0;
L12: 

    /** 		if atom(m_[VALUE_BUCKETS]) 		then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6679 = (int)*(((s1_ptr)_2)->base + 6);
    _6680 = IS_ATOM(_6679);
    _6679 = NOVALUE;
    if (_6680 == 0)
    {
        _6680 = NOVALUE;
        goto L13; // [357] 365
    }
    else{
        _6680 = NOVALUE;
    }
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    _6665 = NOVALUE;
    _6667 = NOVALUE;
    _6670 = NOVALUE;
    _6672 = NOVALUE;
    return 0;
L13: 

    /** 		if length(m_[KEY_BUCKETS]) != length(m_[VALUE_BUCKETS])	then return 0 end if*/
    _2 = (int)SEQ_PTR(_m__11822);
    _6681 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6681)){
            _6682 = SEQ_PTR(_6681)->length;
    }
    else {
        _6682 = 1;
    }
    _6681 = NOVALUE;
    _2 = (int)SEQ_PTR(_m__11822);
    _6683 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_6683)){
            _6684 = SEQ_PTR(_6683)->length;
    }
    else {
        _6684 = 1;
    }
    _6683 = NOVALUE;
    if (_6682 == _6684)
    goto L10; // [385] 404
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    _6665 = NOVALUE;
    _6667 = NOVALUE;
    _6670 = NOVALUE;
    _6672 = NOVALUE;
    _6681 = NOVALUE;
    _6683 = NOVALUE;
    return 0;
    goto L10; // [394] 404
L11: 

    /** 		return 0*/
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    _6665 = NOVALUE;
    _6667 = NOVALUE;
    _6670 = NOVALUE;
    _6672 = NOVALUE;
    _6681 = NOVALUE;
    _6683 = NOVALUE;
    return 0;
L10: 

    /** 	return 1*/
    DeRef(_obj_p_11818);
    DeRef(_m__11822);
    _6662 = NOVALUE;
    _6665 = NOVALUE;
    _6667 = NOVALUE;
    _6670 = NOVALUE;
    _6672 = NOVALUE;
    _6681 = NOVALUE;
    _6683 = NOVALUE;
    return 1;
    ;
}


void _29rehash(int _the_map_p_11918, int _requested_bucket_size_p_11919)
{
    int _size__11920 = NOVALUE;
    int _index_2__11921 = NOVALUE;
    int _old_key_buckets__11922 = NOVALUE;
    int _old_val_buckets__11923 = NOVALUE;
    int _new_key_buckets__11924 = NOVALUE;
    int _new_val_buckets__11925 = NOVALUE;
    int _key__11926 = NOVALUE;
    int _value__11927 = NOVALUE;
    int _pos_11928 = NOVALUE;
    int _new_keys_11929 = NOVALUE;
    int _in_use_11930 = NOVALUE;
    int _elem_count_11931 = NOVALUE;
    int _calc_hash_1__tmp_at237_11974 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_237_11973 = NOVALUE;
    int _ret__inlined_calc_hash_at_237_11972 = NOVALUE;
    int _6752 = NOVALUE;
    int _6751 = NOVALUE;
    int _6750 = NOVALUE;
    int _6749 = NOVALUE;
    int _6748 = NOVALUE;
    int _6747 = NOVALUE;
    int _6746 = NOVALUE;
    int _6745 = NOVALUE;
    int _6744 = NOVALUE;
    int _6742 = NOVALUE;
    int _6741 = NOVALUE;
    int _6740 = NOVALUE;
    int _6737 = NOVALUE;
    int _6736 = NOVALUE;
    int _6734 = NOVALUE;
    int _6733 = NOVALUE;
    int _6732 = NOVALUE;
    int _6731 = NOVALUE;
    int _6729 = NOVALUE;
    int _6727 = NOVALUE;
    int _6725 = NOVALUE;
    int _6722 = NOVALUE;
    int _6720 = NOVALUE;
    int _6719 = NOVALUE;
    int _6718 = NOVALUE;
    int _6717 = NOVALUE;
    int _6715 = NOVALUE;
    int _6713 = NOVALUE;
    int _6711 = NOVALUE;
    int _6710 = NOVALUE;
    int _6708 = NOVALUE;
    int _6706 = NOVALUE;
    int _6703 = NOVALUE;
    int _6701 = NOVALUE;
    int _6700 = NOVALUE;
    int _6699 = NOVALUE;
    int _6698 = NOVALUE;
    int _6697 = NOVALUE;
    int _6694 = NOVALUE;
    int _6693 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = SMALLMAP then*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6693 = (int)*(((s1_ptr)_2)->base + _the_map_p_11918);
    _2 = (int)SEQ_PTR(_6693);
    _6694 = (int)*(((s1_ptr)_2)->base + 4);
    _6693 = NOVALUE;
    if (binary_op_a(NOTEQ, _6694, 115)){
        _6694 = NOVALUE;
        goto L1; // [19] 29
    }
    _6694 = NOVALUE;

    /** 		return -- small maps are not hashed.*/
    DeRef(_old_key_buckets__11922);
    DeRef(_old_val_buckets__11923);
    DeRef(_new_key_buckets__11924);
    DeRef(_new_val_buckets__11925);
    DeRef(_key__11926);
    DeRef(_value__11927);
    DeRef(_new_keys_11929);
    return;
L1: 

    /** 	if requested_bucket_size_p <= 0 then*/
    if (_requested_bucket_size_p_11919 > 0)
    goto L2; // [31] 66

    /** 		size_ = floor(length(eumem:ram_space[the_map_p][KEY_BUCKETS]) * 3.5) + 1*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6697 = (int)*(((s1_ptr)_2)->base + _the_map_p_11918);
    _2 = (int)SEQ_PTR(_6697);
    _6698 = (int)*(((s1_ptr)_2)->base + 5);
    _6697 = NOVALUE;
    if (IS_SEQUENCE(_6698)){
            _6699 = SEQ_PTR(_6698)->length;
    }
    else {
        _6699 = 1;
    }
    _6698 = NOVALUE;
    _6700 = NewDouble((double)_6699 * DBL_PTR(_6267)->dbl);
    _6699 = NOVALUE;
    _6701 = unary_op(FLOOR, _6700);
    DeRefDS(_6700);
    _6700 = NOVALUE;
    if (IS_ATOM_INT(_6701)) {
        _size__11920 = _6701 + 1;
    }
    else
    { // coercing _size__11920 to an integer 1
        _size__11920 = 1+(long)(DBL_PTR(_6701)->dbl);
        if( !IS_ATOM_INT(_size__11920) ){
            _size__11920 = (object)DBL_PTR(_size__11920)->dbl;
        }
    }
    DeRef(_6701);
    _6701 = NOVALUE;
    goto L3; // [63] 72
L2: 

    /** 		size_ = requested_bucket_size_p*/
    _size__11920 = _requested_bucket_size_p_11919;
L3: 

    /** 	size_ = primes:next_prime(size_, -size_, 2)	-- Allow up to 2 seconds to calc next prime.*/
    if ((unsigned long)_size__11920 == 0xC0000000)
    _6703 = (int)NewDouble((double)-0xC0000000);
    else
    _6703 = - _size__11920;
    _size__11920 = _31next_prime(_size__11920, _6703, 2);
    _6703 = NOVALUE;
    if (!IS_ATOM_INT(_size__11920)) {
        _1 = (long)(DBL_PTR(_size__11920)->dbl);
        if (UNIQUE(DBL_PTR(_size__11920)) && (DBL_PTR(_size__11920)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_size__11920);
        _size__11920 = _1;
    }

    /** 	if size_ < 0 then*/
    if (_size__11920 >= 0)
    goto L4; // [89] 99

    /** 		return  -- don't do anything. New size would take too long.*/
    DeRef(_old_key_buckets__11922);
    DeRef(_old_val_buckets__11923);
    DeRef(_new_key_buckets__11924);
    DeRef(_new_val_buckets__11925);
    DeRef(_key__11926);
    DeRef(_value__11927);
    DeRef(_new_keys_11929);
    _6698 = NOVALUE;
    return;
L4: 

    /** 	old_key_buckets_ = eumem:ram_space[the_map_p][KEY_BUCKETS]*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6706 = (int)*(((s1_ptr)_2)->base + _the_map_p_11918);
    DeRef(_old_key_buckets__11922);
    _2 = (int)SEQ_PTR(_6706);
    _old_key_buckets__11922 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_old_key_buckets__11922);
    _6706 = NOVALUE;

    /** 	old_val_buckets_ = eumem:ram_space[the_map_p][VALUE_BUCKETS]*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6708 = (int)*(((s1_ptr)_2)->base + _the_map_p_11918);
    DeRef(_old_val_buckets__11923);
    _2 = (int)SEQ_PTR(_6708);
    _old_val_buckets__11923 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_old_val_buckets__11923);
    _6708 = NOVALUE;

    /** 	new_key_buckets_ = repeat(repeat(1, threshold_size + 1), size_)*/
    _6710 = 24;
    _6711 = Repeat(1, 24);
    _6710 = NOVALUE;
    DeRef(_new_key_buckets__11924);
    _new_key_buckets__11924 = Repeat(_6711, _size__11920);
    DeRefDS(_6711);
    _6711 = NOVALUE;

    /** 	new_val_buckets_ = repeat(repeat(0, threshold_size), size_)*/
    _6713 = Repeat(0, 23);
    DeRef(_new_val_buckets__11925);
    _new_val_buckets__11925 = Repeat(_6713, _size__11920);
    DeRefDS(_6713);
    _6713 = NOVALUE;

    /** 	elem_count = eumem:ram_space[the_map_p][ELEMENT_COUNT]*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6715 = (int)*(((s1_ptr)_2)->base + _the_map_p_11918);
    _2 = (int)SEQ_PTR(_6715);
    _elem_count_11931 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_elem_count_11931)){
        _elem_count_11931 = (long)DBL_PTR(_elem_count_11931)->dbl;
    }
    _6715 = NOVALUE;

    /** 	in_use = 0*/
    _in_use_11930 = 0;

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_11918);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	for index = 1 to length(old_key_buckets_) do*/
    if (IS_SEQUENCE(_old_key_buckets__11922)){
            _6717 = SEQ_PTR(_old_key_buckets__11922)->length;
    }
    else {
        _6717 = 1;
    }
    {
        int _index_11961;
        _index_11961 = 1;
L5: 
        if (_index_11961 > _6717){
            goto L6; // [193] 385
        }

        /** 		for entry_idx = 1 to length(old_key_buckets_[index]) do*/
        _2 = (int)SEQ_PTR(_old_key_buckets__11922);
        _6718 = (int)*(((s1_ptr)_2)->base + _index_11961);
        if (IS_SEQUENCE(_6718)){
                _6719 = SEQ_PTR(_6718)->length;
        }
        else {
            _6719 = 1;
        }
        _6718 = NOVALUE;
        {
            int _entry_idx_11964;
            _entry_idx_11964 = 1;
L7: 
            if (_entry_idx_11964 > _6719){
                goto L8; // [209] 378
            }

            /** 			key_ = old_key_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_key_buckets__11922);
            _6720 = (int)*(((s1_ptr)_2)->base + _index_11961);
            DeRef(_key__11926);
            _2 = (int)SEQ_PTR(_6720);
            _key__11926 = (int)*(((s1_ptr)_2)->base + _entry_idx_11964);
            Ref(_key__11926);
            _6720 = NOVALUE;

            /** 			value_ = old_val_buckets_[index][entry_idx]*/
            _2 = (int)SEQ_PTR(_old_val_buckets__11923);
            _6722 = (int)*(((s1_ptr)_2)->base + _index_11961);
            DeRef(_value__11927);
            _2 = (int)SEQ_PTR(_6722);
            _value__11927 = (int)*(((s1_ptr)_2)->base + _entry_idx_11964);
            Ref(_value__11927);
            _6722 = NOVALUE;

            /** 			index_2_ = calc_hash(key_, size_)*/

            /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
            DeRef(_ret__inlined_calc_hash_at_237_11972);
            _ret__inlined_calc_hash_at_237_11972 = calc_hash(_key__11926, -6);
            if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_237_11972)) {
                _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_237_11972)->dbl);
                if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_237_11972)) && (DBL_PTR(_ret__inlined_calc_hash_at_237_11972)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_ret__inlined_calc_hash_at_237_11972);
                _ret__inlined_calc_hash_at_237_11972 = _1;
            }

            /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
            _calc_hash_1__tmp_at237_11974 = (_ret__inlined_calc_hash_at_237_11972 % _size__11920);
            _index_2__11921 = _calc_hash_1__tmp_at237_11974 + 1;
            DeRef(_ret__inlined_calc_hash_at_237_11972);
            _ret__inlined_calc_hash_at_237_11972 = NOVALUE;
            if (!IS_ATOM_INT(_index_2__11921)) {
                _1 = (long)(DBL_PTR(_index_2__11921)->dbl);
                if (UNIQUE(DBL_PTR(_index_2__11921)) && (DBL_PTR(_index_2__11921)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_index_2__11921);
                _index_2__11921 = _1;
            }

            /** 			new_keys = new_key_buckets_[index_2_]*/
            DeRef(_new_keys_11929);
            _2 = (int)SEQ_PTR(_new_key_buckets__11924);
            _new_keys_11929 = (int)*(((s1_ptr)_2)->base + _index_2__11921);
            RefDS(_new_keys_11929);

            /** 			pos = new_keys[$]*/
            if (IS_SEQUENCE(_new_keys_11929)){
                    _6725 = SEQ_PTR(_new_keys_11929)->length;
            }
            else {
                _6725 = 1;
            }
            _2 = (int)SEQ_PTR(_new_keys_11929);
            _pos_11928 = (int)*(((s1_ptr)_2)->base + _6725);
            if (!IS_ATOM_INT(_pos_11928))
            _pos_11928 = (long)DBL_PTR(_pos_11928)->dbl;

            /** 			if length(new_keys) = pos then*/
            if (IS_SEQUENCE(_new_keys_11929)){
                    _6727 = SEQ_PTR(_new_keys_11929)->length;
            }
            else {
                _6727 = 1;
            }
            if (_6727 != _pos_11928)
            goto L9; // [285] 322

            /** 				new_keys &= repeat(pos, threshold_size)*/
            _6729 = Repeat(_pos_11928, 23);
            Concat((object_ptr)&_new_keys_11929, _new_keys_11929, _6729);
            DeRefDS(_6729);
            _6729 = NOVALUE;

            /** 				new_val_buckets_[index_2_] &= repeat(0, threshold_size)*/
            _6731 = Repeat(0, 23);
            _2 = (int)SEQ_PTR(_new_val_buckets__11925);
            _6732 = (int)*(((s1_ptr)_2)->base + _index_2__11921);
            if (IS_SEQUENCE(_6732) && IS_ATOM(_6731)) {
            }
            else if (IS_ATOM(_6732) && IS_SEQUENCE(_6731)) {
                Ref(_6732);
                Prepend(&_6733, _6731, _6732);
            }
            else {
                Concat((object_ptr)&_6733, _6732, _6731);
                _6732 = NOVALUE;
            }
            _6732 = NOVALUE;
            DeRefDS(_6731);
            _6731 = NOVALUE;
            _2 = (int)SEQ_PTR(_new_val_buckets__11925);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__11925 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__11921);
            _1 = *(int *)_2;
            *(int *)_2 = _6733;
            if( _1 != _6733 ){
                DeRef(_1);
            }
            _6733 = NOVALUE;
L9: 

            /** 			new_keys[pos] = key_*/
            Ref(_key__11926);
            _2 = (int)SEQ_PTR(_new_keys_11929);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_11929 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_11928);
            _1 = *(int *)_2;
            *(int *)_2 = _key__11926;
            DeRef(_1);

            /** 			new_val_buckets_[index_2_][pos] = value_*/
            _2 = (int)SEQ_PTR(_new_val_buckets__11925);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_val_buckets__11925 = MAKE_SEQ(_2);
            }
            _3 = (int)(_index_2__11921 + ((s1_ptr)_2)->base);
            Ref(_value__11927);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pos_11928);
            _1 = *(int *)_2;
            *(int *)_2 = _value__11927;
            DeRef(_1);
            _6734 = NOVALUE;

            /** 			new_keys[$] = pos + 1*/
            if (IS_SEQUENCE(_new_keys_11929)){
                    _6736 = SEQ_PTR(_new_keys_11929)->length;
            }
            else {
                _6736 = 1;
            }
            _6737 = _pos_11928 + 1;
            if (_6737 > MAXINT){
                _6737 = NewDouble((double)_6737);
            }
            _2 = (int)SEQ_PTR(_new_keys_11929);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_keys_11929 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _6736);
            _1 = *(int *)_2;
            *(int *)_2 = _6737;
            if( _1 != _6737 ){
                DeRef(_1);
            }
            _6737 = NOVALUE;

            /** 			new_key_buckets_[index_2_] = new_keys*/
            RefDS(_new_keys_11929);
            _2 = (int)SEQ_PTR(_new_key_buckets__11924);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _new_key_buckets__11924 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _index_2__11921);
            _1 = *(int *)_2;
            *(int *)_2 = _new_keys_11929;
            DeRefDS(_1);

            /** 			if pos = 1 then*/
            if (_pos_11928 != 1)
            goto LA; // [360] 371

            /** 				in_use += 1*/
            _in_use_11930 = _in_use_11930 + 1;
LA: 

            /** 		end for*/
            _entry_idx_11964 = _entry_idx_11964 + 1;
            goto L7; // [373] 216
L8: 
            ;
        }

        /** 	end for*/
        _index_11961 = _index_11961 + 1;
        goto L5; // [380] 200
L6: 
        ;
    }

    /** 	for index = 1 to length(new_key_buckets_) do*/
    if (IS_SEQUENCE(_new_key_buckets__11924)){
            _6740 = SEQ_PTR(_new_key_buckets__11924)->length;
    }
    else {
        _6740 = 1;
    }
    {
        int _index_11994;
        _index_11994 = 1;
LB: 
        if (_index_11994 > _6740){
            goto LC; // [390] 463
        }

        /** 		pos = new_key_buckets_[index][$]*/
        _2 = (int)SEQ_PTR(_new_key_buckets__11924);
        _6741 = (int)*(((s1_ptr)_2)->base + _index_11994);
        if (IS_SEQUENCE(_6741)){
                _6742 = SEQ_PTR(_6741)->length;
        }
        else {
            _6742 = 1;
        }
        _2 = (int)SEQ_PTR(_6741);
        _pos_11928 = (int)*(((s1_ptr)_2)->base + _6742);
        if (!IS_ATOM_INT(_pos_11928)){
            _pos_11928 = (long)DBL_PTR(_pos_11928)->dbl;
        }
        _6741 = NOVALUE;

        /** 		new_key_buckets_[index] = remove(new_key_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_key_buckets__11924);
        _6744 = (int)*(((s1_ptr)_2)->base + _index_11994);
        _2 = (int)SEQ_PTR(_new_key_buckets__11924);
        _6745 = (int)*(((s1_ptr)_2)->base + _index_11994);
        if (IS_SEQUENCE(_6745)){
                _6746 = SEQ_PTR(_6745)->length;
        }
        else {
            _6746 = 1;
        }
        _6745 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_6744);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_11928)) ? _pos_11928 : (long)(DBL_PTR(_pos_11928)->dbl);
            int stop = (IS_ATOM_INT(_6746)) ? _6746 : (long)(DBL_PTR(_6746)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_6744);
                DeRef(_6747);
                _6747 = _6744;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_6744), start, &_6747 );
                }
                else Tail(SEQ_PTR(_6744), stop+1, &_6747);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_6744), start, &_6747);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_6747);
                _6747 = _1;
            }
        }
        _6744 = NOVALUE;
        _6746 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_key_buckets__11924);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_key_buckets__11924 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_11994);
        _1 = *(int *)_2;
        *(int *)_2 = _6747;
        if( _1 != _6747 ){
            DeRefDS(_1);
        }
        _6747 = NOVALUE;

        /** 		new_val_buckets_[index] = remove(new_val_buckets_[index], pos, */
        _2 = (int)SEQ_PTR(_new_val_buckets__11925);
        _6748 = (int)*(((s1_ptr)_2)->base + _index_11994);
        _2 = (int)SEQ_PTR(_new_val_buckets__11925);
        _6749 = (int)*(((s1_ptr)_2)->base + _index_11994);
        if (IS_SEQUENCE(_6749)){
                _6750 = SEQ_PTR(_6749)->length;
        }
        else {
            _6750 = 1;
        }
        _6749 = NOVALUE;
        {
            s1_ptr assign_space = SEQ_PTR(_6748);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_pos_11928)) ? _pos_11928 : (long)(DBL_PTR(_pos_11928)->dbl);
            int stop = (IS_ATOM_INT(_6750)) ? _6750 : (long)(DBL_PTR(_6750)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
                RefDS(_6748);
                DeRef(_6751);
                _6751 = _6748;
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_6748), start, &_6751 );
                }
                else Tail(SEQ_PTR(_6748), stop+1, &_6751);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_6748), start, &_6751);
            }
            else {
                assign_slice_seq = &assign_space;
                _1 = Remove_elements(start, stop, 0);
                DeRef(_6751);
                _6751 = _1;
            }
        }
        _6748 = NOVALUE;
        _6750 = NOVALUE;
        _2 = (int)SEQ_PTR(_new_val_buckets__11925);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _new_val_buckets__11925 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index_11994);
        _1 = *(int *)_2;
        *(int *)_2 = _6751;
        if( _1 != _6751 ){
            DeRef(_1);
        }
        _6751 = NOVALUE;

        /** 	end for*/
        _index_11994 = _index_11994 + 1;
        goto LB; // [458] 397
LC: 
        ;
    }

    /** 	eumem:ram_space[the_map_p] = { */
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_29type_is_map_11790);
    *((int *)(_2+4)) = _29type_is_map_11790;
    *((int *)(_2+8)) = _elem_count_11931;
    *((int *)(_2+12)) = _in_use_11930;
    *((int *)(_2+16)) = 76;
    RefDS(_new_key_buckets__11924);
    *((int *)(_2+20)) = _new_key_buckets__11924;
    RefDS(_new_val_buckets__11925);
    *((int *)(_2+24)) = _new_val_buckets__11925;
    _6752 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_11918);
    _1 = *(int *)_2;
    *(int *)_2 = _6752;
    if( _1 != _6752 ){
        DeRef(_1);
    }
    _6752 = NOVALUE;

    /** end procedure*/
    DeRef(_old_key_buckets__11922);
    DeRef(_old_val_buckets__11923);
    DeRefDS(_new_key_buckets__11924);
    DeRefDS(_new_val_buckets__11925);
    DeRef(_key__11926);
    DeRef(_value__11927);
    DeRef(_new_keys_11929);
    _6698 = NOVALUE;
    _6718 = NOVALUE;
    _6745 = NOVALUE;
    _6749 = NOVALUE;
    return;
    ;
}


int _29new(int _initial_size_p_12010)
{
    int _buckets__12012 = NOVALUE;
    int _new_map__12013 = NOVALUE;
    int _temp_map__12014 = NOVALUE;
    int _6765 = NOVALUE;
    int _6764 = NOVALUE;
    int _6763 = NOVALUE;
    int _6761 = NOVALUE;
    int _6760 = NOVALUE;
    int _6757 = NOVALUE;
    int _6756 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if initial_size_p < 3 then*/
    if (_initial_size_p_12010 >= 3)
    goto L1; // [5] 15

    /** 		initial_size_p = 3*/
    _initial_size_p_12010 = 3;
L1: 

    /** 	if initial_size_p > threshold_size then*/
    if (_initial_size_p_12010 <= 23)
    goto L2; // [19] 75

    /** 		buckets_ = floor((initial_size_p + threshold_size - 1) / threshold_size)*/
    _6756 = _initial_size_p_12010 + 23;
    if ((long)((unsigned long)_6756 + (unsigned long)HIGH_BITS) >= 0) 
    _6756 = NewDouble((double)_6756);
    if (IS_ATOM_INT(_6756)) {
        _6757 = _6756 - 1;
        if ((long)((unsigned long)_6757 +(unsigned long) HIGH_BITS) >= 0){
            _6757 = NewDouble((double)_6757);
        }
    }
    else {
        _6757 = NewDouble(DBL_PTR(_6756)->dbl - (double)1);
    }
    DeRef(_6756);
    _6756 = NOVALUE;
    if (IS_ATOM_INT(_6757)) {
        if (23 > 0 && _6757 >= 0) {
            _buckets__12012 = _6757 / 23;
        }
        else {
            temp_dbl = floor((double)_6757 / (double)23);
            _buckets__12012 = (long)temp_dbl;
        }
    }
    else {
        _2 = binary_op(DIVIDE, _6757, 23);
        _buckets__12012 = unary_op(FLOOR, _2);
        DeRef(_2);
    }
    DeRef(_6757);
    _6757 = NOVALUE;
    if (!IS_ATOM_INT(_buckets__12012)) {
        _1 = (long)(DBL_PTR(_buckets__12012)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__12012)) && (DBL_PTR(_buckets__12012)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__12012);
        _buckets__12012 = _1;
    }

    /** 		buckets_ = primes:next_prime(buckets_)*/
    _buckets__12012 = _31next_prime(_buckets__12012, -1, 1);
    if (!IS_ATOM_INT(_buckets__12012)) {
        _1 = (long)(DBL_PTR(_buckets__12012)->dbl);
        if (UNIQUE(DBL_PTR(_buckets__12012)) && (DBL_PTR(_buckets__12012)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_buckets__12012);
        _buckets__12012 = _1;
    }

    /** 		new_map_ = { type_is_map, 0, 0, LARGEMAP, repeat({}, buckets_), repeat({}, buckets_) }*/
    _6760 = Repeat(_5, _buckets__12012);
    _6761 = Repeat(_5, _buckets__12012);
    _0 = _new_map__12013;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_29type_is_map_11790);
    *((int *)(_2+4)) = _29type_is_map_11790;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 76;
    *((int *)(_2+20)) = _6760;
    *((int *)(_2+24)) = _6761;
    _new_map__12013 = MAKE_SEQ(_1);
    DeRef(_0);
    _6761 = NOVALUE;
    _6760 = NOVALUE;
    goto L3; // [72] 100
L2: 

    /** 		new_map_ = {*/
    _6763 = Repeat(_29init_small_map_key_11813, _initial_size_p_12010);
    _6764 = Repeat(0, _initial_size_p_12010);
    _6765 = Repeat(0, _initial_size_p_12010);
    _0 = _new_map__12013;
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_29type_is_map_11790);
    *((int *)(_2+4)) = _29type_is_map_11790;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _6763;
    *((int *)(_2+24)) = _6764;
    *((int *)(_2+28)) = _6765;
    _new_map__12013 = MAKE_SEQ(_1);
    DeRef(_0);
    _6765 = NOVALUE;
    _6764 = NOVALUE;
    _6763 = NOVALUE;
L3: 

    /** 	temp_map_ = eumem:malloc()*/
    _0 = _temp_map__12014;
    _temp_map__12014 = _30malloc(1, 1);
    DeRef(_0);

    /** 	eumem:ram_space[temp_map_] = new_map_*/
    RefDS(_new_map__12013);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_temp_map__12014))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_temp_map__12014)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _temp_map__12014);
    _1 = *(int *)_2;
    *(int *)_2 = _new_map__12013;
    DeRef(_1);

    /** 	return temp_map_*/
    DeRefDS(_new_map__12013);
    return _temp_map__12014;
    ;
}


int _29new_extra(int _the_map_p_12034, int _initial_size_p_12035)
{
    int _6769 = NOVALUE;
    int _6768 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if map(the_map_p) then*/
    Ref(_the_map_p_12034);
    _6768 = _29map(_the_map_p_12034);
    if (_6768 == 0) {
        DeRef(_6768);
        _6768 = NOVALUE;
        goto L1; // [9] 21
    }
    else {
        if (!IS_ATOM_INT(_6768) && DBL_PTR(_6768)->dbl == 0.0){
            DeRef(_6768);
            _6768 = NOVALUE;
            goto L1; // [9] 21
        }
        DeRef(_6768);
        _6768 = NOVALUE;
    }
    DeRef(_6768);
    _6768 = NOVALUE;

    /** 		return the_map_p*/
    return _the_map_p_12034;
    goto L2; // [18] 32
L1: 

    /** 		return new(initial_size_p)*/
    _6769 = _29new(_initial_size_p_12035);
    DeRef(_the_map_p_12034);
    return _6769;
L2: 
    ;
}


int _29has(int _the_map_p_12074, int _the_key_p_12075)
{
    int _index__12076 = NOVALUE;
    int _pos__12077 = NOVALUE;
    int _from__12078 = NOVALUE;
    int _calc_hash_1__tmp_at40_12090 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_40_12089 = NOVALUE;
    int _ret__inlined_calc_hash_at_40_12088 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_37_12087 = NOVALUE;
    int _6807 = NOVALUE;
    int _6805 = NOVALUE;
    int _6804 = NOVALUE;
    int _6801 = NOVALUE;
    int _6800 = NOVALUE;
    int _6799 = NOVALUE;
    int _6797 = NOVALUE;
    int _6796 = NOVALUE;
    int _6794 = NOVALUE;
    int _6792 = NOVALUE;
    int _6791 = NOVALUE;
    int _6790 = NOVALUE;
    int _6789 = NOVALUE;
    int _6788 = NOVALUE;
    int _6787 = NOVALUE;
    int _6785 = NOVALUE;
    int _6784 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_12074)) {
        _1 = (long)(DBL_PTR(_the_map_p_12074)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12074)) && (DBL_PTR(_the_map_p_12074)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12074);
        _the_map_p_12074 = _1;
    }

    /** 	if eumem:ram_space[the_map_p][MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6784 = (int)*(((s1_ptr)_2)->base + _the_map_p_12074);
    _2 = (int)SEQ_PTR(_6784);
    _6785 = (int)*(((s1_ptr)_2)->base + 4);
    _6784 = NOVALUE;
    if (binary_op_a(NOTEQ, _6785, 76)){
        _6785 = NOVALUE;
        goto L1; // [17] 94
    }
    _6785 = NOVALUE;

    /** 		index_ = calc_hash(the_key_p, length(eumem:ram_space[the_map_p][KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6787 = (int)*(((s1_ptr)_2)->base + _the_map_p_12074);
    _2 = (int)SEQ_PTR(_6787);
    _6788 = (int)*(((s1_ptr)_2)->base + 5);
    _6787 = NOVALUE;
    if (IS_SEQUENCE(_6788)){
            _6789 = SEQ_PTR(_6788)->length;
    }
    else {
        _6789 = 1;
    }
    _6788 = NOVALUE;
    _max_hash_p_inlined_calc_hash_at_37_12087 = _6789;
    _6789 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_40_12088);
    _ret__inlined_calc_hash_at_40_12088 = calc_hash(_the_key_p_12075, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_40_12088)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_40_12088)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_40_12088)) && (DBL_PTR(_ret__inlined_calc_hash_at_40_12088)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_40_12088);
        _ret__inlined_calc_hash_at_40_12088 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at40_12090 = (_ret__inlined_calc_hash_at_40_12088 % _max_hash_p_inlined_calc_hash_at_37_12087);
    _index__12076 = _calc_hash_1__tmp_at40_12090 + 1;
    DeRef(_ret__inlined_calc_hash_at_40_12088);
    _ret__inlined_calc_hash_at_40_12088 = NOVALUE;
    if (!IS_ATOM_INT(_index__12076)) {
        _1 = (long)(DBL_PTR(_index__12076)->dbl);
        if (UNIQUE(DBL_PTR(_index__12076)) && (DBL_PTR(_index__12076)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index__12076);
        _index__12076 = _1;
    }

    /** 		pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_BUCKETS][index_])*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6790 = (int)*(((s1_ptr)_2)->base + _the_map_p_12074);
    _2 = (int)SEQ_PTR(_6790);
    _6791 = (int)*(((s1_ptr)_2)->base + 5);
    _6790 = NOVALUE;
    _2 = (int)SEQ_PTR(_6791);
    _6792 = (int)*(((s1_ptr)_2)->base + _index__12076);
    _6791 = NOVALUE;
    _pos__12077 = find_from(_the_key_p_12075, _6792, 1);
    _6792 = NOVALUE;
    goto L2; // [91] 215
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12075 == _29init_small_map_key_11813)
    _6794 = 1;
    else if (IS_ATOM_INT(_the_key_p_12075) && IS_ATOM_INT(_29init_small_map_key_11813))
    _6794 = 0;
    else
    _6794 = (compare(_the_key_p_12075, _29init_small_map_key_11813) == 0);
    if (_6794 == 0)
    {
        _6794 = NOVALUE;
        goto L3; // [100] 194
    }
    else{
        _6794 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12078 = 1;

    /** 			while from_ > 0 do*/
L4: 
    if (_from__12078 <= 0)
    goto L5; // [113] 214

    /** 				pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6796 = (int)*(((s1_ptr)_2)->base + _the_map_p_12074);
    _2 = (int)SEQ_PTR(_6796);
    _6797 = (int)*(((s1_ptr)_2)->base + 5);
    _6796 = NOVALUE;
    _pos__12077 = find_from(_the_key_p_12075, _6797, _from__12078);
    _6797 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__12077 == 0)
    {
        goto L6; // [138] 173
    }
    else{
    }

    /** 					if eumem:ram_space[the_map_p][FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6799 = (int)*(((s1_ptr)_2)->base + _the_map_p_12074);
    _2 = (int)SEQ_PTR(_6799);
    _6800 = (int)*(((s1_ptr)_2)->base + 7);
    _6799 = NOVALUE;
    _2 = (int)SEQ_PTR(_6800);
    _6801 = (int)*(((s1_ptr)_2)->base + _pos__12077);
    _6800 = NOVALUE;
    if (binary_op_a(NOTEQ, _6801, 1)){
        _6801 = NOVALUE;
        goto L7; // [159] 180
    }
    _6801 = NOVALUE;

    /** 						return 1*/
    DeRef(_the_key_p_12075);
    _6788 = NOVALUE;
    return 1;
    goto L7; // [170] 180
L6: 

    /** 					return 0*/
    DeRef(_the_key_p_12075);
    _6788 = NOVALUE;
    return 0;
L7: 

    /** 				from_ = pos_ + 1*/
    _from__12078 = _pos__12077 + 1;

    /** 			end while*/
    goto L4; // [188] 113
    goto L5; // [191] 214
L3: 

    /** 			pos_ = find(the_key_p, eumem:ram_space[the_map_p][KEY_LIST])*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _6804 = (int)*(((s1_ptr)_2)->base + _the_map_p_12074);
    _2 = (int)SEQ_PTR(_6804);
    _6805 = (int)*(((s1_ptr)_2)->base + 5);
    _6804 = NOVALUE;
    _pos__12077 = find_from(_the_key_p_12075, _6805, 1);
    _6805 = NOVALUE;
L5: 
L2: 

    /** 	return (pos_  != 0)	*/
    _6807 = (_pos__12077 != 0);
    DeRef(_the_key_p_12075);
    _6788 = NOVALUE;
    return _6807;
    ;
}


int _29get(int _the_map_p_12118, int _the_key_p_12119, int _default_value_p_12120)
{
    int _bucket__12121 = NOVALUE;
    int _pos__12122 = NOVALUE;
    int _from__12123 = NOVALUE;
    int _themap_12124 = NOVALUE;
    int _thekeys_12129 = NOVALUE;
    int _calc_hash_1__tmp_at44_12136 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_44_12135 = NOVALUE;
    int _ret__inlined_calc_hash_at_44_12134 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_41_12133 = NOVALUE;
    int _6832 = NOVALUE;
    int _6831 = NOVALUE;
    int _6829 = NOVALUE;
    int _6827 = NOVALUE;
    int _6826 = NOVALUE;
    int _6824 = NOVALUE;
    int _6823 = NOVALUE;
    int _6821 = NOVALUE;
    int _6819 = NOVALUE;
    int _6818 = NOVALUE;
    int _6817 = NOVALUE;
    int _6816 = NOVALUE;
    int _6813 = NOVALUE;
    int _6812 = NOVALUE;
    int _6809 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map_p_12118)) {
        _1 = (long)(DBL_PTR(_the_map_p_12118)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12118)) && (DBL_PTR(_the_map_p_12118)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12118);
        _the_map_p_12118 = _1;
    }

    /** 	themap = eumem:ram_space[the_map_p]*/
    DeRef(_themap_12124);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _themap_12124 = (int)*(((s1_ptr)_2)->base + _the_map_p_12118);
    Ref(_themap_12124);

    /** 	if themap[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_themap_12124);
    _6809 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6809, 76)){
        _6809 = NOVALUE;
        goto L1; // [21] 121
    }
    _6809 = NOVALUE;

    /** 		sequence thekeys*/

    /** 		thekeys = themap[KEY_BUCKETS]*/
    DeRef(_thekeys_12129);
    _2 = (int)SEQ_PTR(_themap_12124);
    _thekeys_12129 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_thekeys_12129);

    /** 		bucket_ = calc_hash(the_key_p, length(thekeys))*/
    if (IS_SEQUENCE(_thekeys_12129)){
            _6812 = SEQ_PTR(_thekeys_12129)->length;
    }
    else {
        _6812 = 1;
    }
    _max_hash_p_inlined_calc_hash_at_41_12133 = _6812;
    _6812 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_44_12134);
    _ret__inlined_calc_hash_at_44_12134 = calc_hash(_the_key_p_12119, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_44_12134)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_44_12134)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_44_12134)) && (DBL_PTR(_ret__inlined_calc_hash_at_44_12134)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_44_12134);
        _ret__inlined_calc_hash_at_44_12134 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at44_12136 = (_ret__inlined_calc_hash_at_44_12134 % _max_hash_p_inlined_calc_hash_at_41_12133);
    _bucket__12121 = _calc_hash_1__tmp_at44_12136 + 1;
    DeRef(_ret__inlined_calc_hash_at_44_12134);
    _ret__inlined_calc_hash_at_44_12134 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12121)) {
        _1 = (long)(DBL_PTR(_bucket__12121)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12121)) && (DBL_PTR(_bucket__12121)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12121);
        _bucket__12121 = _1;
    }

    /** 		pos_ = find(the_key_p, thekeys[bucket_])*/
    _2 = (int)SEQ_PTR(_thekeys_12129);
    _6813 = (int)*(((s1_ptr)_2)->base + _bucket__12121);
    _pos__12122 = find_from(_the_key_p_12119, _6813, 1);
    _6813 = NOVALUE;

    /** 		if pos_ > 0 then*/
    if (_pos__12122 <= 0)
    goto L2; // [85] 110

    /** 			return themap[VALUE_BUCKETS][bucket_][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12124);
    _6816 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6816);
    _6817 = (int)*(((s1_ptr)_2)->base + _bucket__12121);
    _6816 = NOVALUE;
    _2 = (int)SEQ_PTR(_6817);
    _6818 = (int)*(((s1_ptr)_2)->base + _pos__12122);
    _6817 = NOVALUE;
    Ref(_6818);
    DeRefDS(_thekeys_12129);
    DeRef(_the_key_p_12119);
    DeRef(_default_value_p_12120);
    DeRefDS(_themap_12124);
    return _6818;
L2: 

    /** 		return default_value_p*/
    DeRef(_thekeys_12129);
    DeRef(_the_key_p_12119);
    DeRef(_themap_12124);
    _6818 = NOVALUE;
    return _default_value_p_12120;
    goto L3; // [118] 256
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12119 == _29init_small_map_key_11813)
    _6819 = 1;
    else if (IS_ATOM_INT(_the_key_p_12119) && IS_ATOM_INT(_29init_small_map_key_11813))
    _6819 = 0;
    else
    _6819 = (compare(_the_key_p_12119, _29init_small_map_key_11813) == 0);
    if (_6819 == 0)
    {
        _6819 = NOVALUE;
        goto L4; // [127] 219
    }
    else{
        _6819 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12123 = 1;

    /** 			while from_ > 0 do*/
L5: 
    if (_from__12123 <= 0)
    goto L6; // [140] 255

    /** 				pos_ = find(the_key_p, themap[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_themap_12124);
    _6821 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__12122 = find_from(_the_key_p_12119, _6821, _from__12123);
    _6821 = NOVALUE;

    /** 				if pos_ then*/
    if (_pos__12122 == 0)
    {
        goto L7; // [159] 198
    }
    else{
    }

    /** 					if themap[FREE_LIST][pos_] = 1 then*/
    _2 = (int)SEQ_PTR(_themap_12124);
    _6823 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_6823);
    _6824 = (int)*(((s1_ptr)_2)->base + _pos__12122);
    _6823 = NOVALUE;
    if (binary_op_a(NOTEQ, _6824, 1)){
        _6824 = NOVALUE;
        goto L8; // [174] 205
    }
    _6824 = NOVALUE;

    /** 						return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12124);
    _6826 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6826);
    _6827 = (int)*(((s1_ptr)_2)->base + _pos__12122);
    _6826 = NOVALUE;
    Ref(_6827);
    DeRef(_the_key_p_12119);
    DeRef(_default_value_p_12120);
    DeRefDS(_themap_12124);
    _6818 = NOVALUE;
    return _6827;
    goto L8; // [195] 205
L7: 

    /** 					return default_value_p*/
    DeRef(_the_key_p_12119);
    DeRef(_themap_12124);
    _6818 = NOVALUE;
    _6827 = NOVALUE;
    return _default_value_p_12120;
L8: 

    /** 				from_ = pos_ + 1*/
    _from__12123 = _pos__12122 + 1;

    /** 			end while*/
    goto L5; // [213] 140
    goto L6; // [216] 255
L4: 

    /** 			pos_ = find(the_key_p, themap[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_themap_12124);
    _6829 = (int)*(((s1_ptr)_2)->base + 5);
    _pos__12122 = find_from(_the_key_p_12119, _6829, 1);
    _6829 = NOVALUE;

    /** 			if pos_  then*/
    if (_pos__12122 == 0)
    {
        goto L9; // [234] 254
    }
    else{
    }

    /** 				return themap[VALUE_LIST][pos_]*/
    _2 = (int)SEQ_PTR(_themap_12124);
    _6831 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_6831);
    _6832 = (int)*(((s1_ptr)_2)->base + _pos__12122);
    _6831 = NOVALUE;
    Ref(_6832);
    DeRef(_the_key_p_12119);
    DeRef(_default_value_p_12120);
    DeRefDS(_themap_12124);
    _6818 = NOVALUE;
    _6827 = NOVALUE;
    return _6832;
L9: 
L6: 
L3: 

    /** 	return default_value_p*/
    DeRef(_the_key_p_12119);
    DeRef(_themap_12124);
    _6818 = NOVALUE;
    _6827 = NOVALUE;
    _6832 = NOVALUE;
    return _default_value_p_12120;
    ;
}


void _29put(int _the_map_p_12188, int _the_key_p_12189, int _the_value_p_12190, int _operation_p_12191, int _trigger_p_12192)
{
    int _index__12193 = NOVALUE;
    int _bucket__12194 = NOVALUE;
    int _average_length__12195 = NOVALUE;
    int _from__12196 = NOVALUE;
    int _map_data_12197 = NOVALUE;
    int _calc_hash_1__tmp_at50_12208 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_50_12207 = NOVALUE;
    int _ret__inlined_calc_hash_at_50_12206 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_47_12205 = NOVALUE;
    int _data_12240 = NOVALUE;
    int _msg_inlined_crash_at_358_12256 = NOVALUE;
    int _msg_inlined_crash_at_405_12262 = NOVALUE;
    int _tmp_seqk_12276 = NOVALUE;
    int _tmp_seqv_12284 = NOVALUE;
    int _msg_inlined_crash_at_781_12320 = NOVALUE;
    int _msg_inlined_crash_at_1173_12383 = NOVALUE;
    int _6976 = NOVALUE;
    int _6975 = NOVALUE;
    int _6973 = NOVALUE;
    int _6972 = NOVALUE;
    int _6971 = NOVALUE;
    int _6970 = NOVALUE;
    int _6968 = NOVALUE;
    int _6967 = NOVALUE;
    int _6966 = NOVALUE;
    int _6964 = NOVALUE;
    int _6963 = NOVALUE;
    int _6962 = NOVALUE;
    int _6960 = NOVALUE;
    int _6959 = NOVALUE;
    int _6958 = NOVALUE;
    int _6956 = NOVALUE;
    int _6955 = NOVALUE;
    int _6954 = NOVALUE;
    int _6952 = NOVALUE;
    int _6950 = NOVALUE;
    int _6944 = NOVALUE;
    int _6943 = NOVALUE;
    int _6942 = NOVALUE;
    int _6941 = NOVALUE;
    int _6939 = NOVALUE;
    int _6937 = NOVALUE;
    int _6936 = NOVALUE;
    int _6935 = NOVALUE;
    int _6934 = NOVALUE;
    int _6933 = NOVALUE;
    int _6932 = NOVALUE;
    int _6929 = NOVALUE;
    int _6927 = NOVALUE;
    int _6924 = NOVALUE;
    int _6922 = NOVALUE;
    int _6919 = NOVALUE;
    int _6918 = NOVALUE;
    int _6916 = NOVALUE;
    int _6913 = NOVALUE;
    int _6912 = NOVALUE;
    int _6909 = NOVALUE;
    int _6906 = NOVALUE;
    int _6904 = NOVALUE;
    int _6902 = NOVALUE;
    int _6899 = NOVALUE;
    int _6897 = NOVALUE;
    int _6896 = NOVALUE;
    int _6895 = NOVALUE;
    int _6894 = NOVALUE;
    int _6893 = NOVALUE;
    int _6892 = NOVALUE;
    int _6891 = NOVALUE;
    int _6890 = NOVALUE;
    int _6889 = NOVALUE;
    int _6883 = NOVALUE;
    int _6881 = NOVALUE;
    int _6880 = NOVALUE;
    int _6878 = NOVALUE;
    int _6876 = NOVALUE;
    int _6873 = NOVALUE;
    int _6872 = NOVALUE;
    int _6871 = NOVALUE;
    int _6870 = NOVALUE;
    int _6868 = NOVALUE;
    int _6867 = NOVALUE;
    int _6866 = NOVALUE;
    int _6864 = NOVALUE;
    int _6863 = NOVALUE;
    int _6862 = NOVALUE;
    int _6860 = NOVALUE;
    int _6859 = NOVALUE;
    int _6858 = NOVALUE;
    int _6856 = NOVALUE;
    int _6854 = NOVALUE;
    int _6849 = NOVALUE;
    int _6848 = NOVALUE;
    int _6847 = NOVALUE;
    int _6846 = NOVALUE;
    int _6844 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_the_map_p_12188)) {
        _1 = (long)(DBL_PTR(_the_map_p_12188)->dbl);
        if (UNIQUE(DBL_PTR(_the_map_p_12188)) && (DBL_PTR(_the_map_p_12188)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map_p_12188);
        _the_map_p_12188 = _1;
    }

    /** 	sequence map_data = eumem:ram_space[the_map_p]*/
    DeRef(_map_data_12197);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _map_data_12197 = (int)*(((s1_ptr)_2)->base + _the_map_p_12188);
    Ref(_map_data_12197);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12188);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if map_data[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6844 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6844, 76)){
        _6844 = NOVALUE;
        goto L1; // [33] 670
    }
    _6844 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p,  length(map_data[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6846 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6846)){
            _6847 = SEQ_PTR(_6846)->length;
    }
    else {
        _6847 = 1;
    }
    _6846 = NOVALUE;
    _max_hash_p_inlined_calc_hash_at_47_12205 = _6847;
    _6847 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_50_12206);
    _ret__inlined_calc_hash_at_50_12206 = calc_hash(_the_key_p_12189, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_50_12206)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_50_12206)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_50_12206)) && (DBL_PTR(_ret__inlined_calc_hash_at_50_12206)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_50_12206);
        _ret__inlined_calc_hash_at_50_12206 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at50_12208 = (_ret__inlined_calc_hash_at_50_12206 % _max_hash_p_inlined_calc_hash_at_47_12205);
    _bucket__12194 = _calc_hash_1__tmp_at50_12208 + 1;
    DeRef(_ret__inlined_calc_hash_at_50_12206);
    _ret__inlined_calc_hash_at_50_12206 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12194)) {
        _1 = (long)(DBL_PTR(_bucket__12194)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12194)) && (DBL_PTR(_bucket__12194)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12194);
        _bucket__12194 = _1;
    }

    /** 		index_ = find(the_key_p, map_data[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6848 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6848);
    _6849 = (int)*(((s1_ptr)_2)->base + _bucket__12194);
    _6848 = NOVALUE;
    _index__12193 = find_from(_the_key_p_12189, _6849, 1);
    _6849 = NOVALUE;

    /** 		if index_ > 0 then*/
    if (_index__12193 <= 0)
    goto L2; // [97] 392

    /** 			switch operation_p do*/
    _0 = _operation_p_12191;
    switch ( _0 ){ 

        /** 				case PUT then*/
        case 1:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12194 + ((s1_ptr)_2)->base);
        _6854 = NOVALUE;
        Ref(_the_value_p_12190);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_12190;
        DeRef(_1);
        _6854 = NOVALUE;
        goto L3; // [130] 378

        /** 				case ADD then*/
        case 2:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12194 + ((s1_ptr)_2)->base);
        _6856 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6858 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6856 = NOVALUE;
        if (IS_ATOM_INT(_6858) && IS_ATOM_INT(_the_value_p_12190)) {
            _6859 = _6858 + _the_value_p_12190;
            if ((long)((unsigned long)_6859 + (unsigned long)HIGH_BITS) >= 0) 
            _6859 = NewDouble((double)_6859);
        }
        else {
            _6859 = binary_op(PLUS, _6858, _the_value_p_12190);
        }
        _6858 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6859;
        if( _1 != _6859 ){
            DeRef(_1);
        }
        _6859 = NOVALUE;
        _6856 = NOVALUE;
        goto L3; // [162] 378

        /** 				case SUBTRACT then*/
        case 3:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12194 + ((s1_ptr)_2)->base);
        _6860 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6862 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6860 = NOVALUE;
        if (IS_ATOM_INT(_6862) && IS_ATOM_INT(_the_value_p_12190)) {
            _6863 = _6862 - _the_value_p_12190;
            if ((long)((unsigned long)_6863 +(unsigned long) HIGH_BITS) >= 0){
                _6863 = NewDouble((double)_6863);
            }
        }
        else {
            _6863 = binary_op(MINUS, _6862, _the_value_p_12190);
        }
        _6862 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6863;
        if( _1 != _6863 ){
            DeRef(_1);
        }
        _6863 = NOVALUE;
        _6860 = NOVALUE;
        goto L3; // [194] 378

        /** 				case MULTIPLY then*/
        case 4:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12194 + ((s1_ptr)_2)->base);
        _6864 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6866 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6864 = NOVALUE;
        if (IS_ATOM_INT(_6866) && IS_ATOM_INT(_the_value_p_12190)) {
            if (_6866 == (short)_6866 && _the_value_p_12190 <= INT15 && _the_value_p_12190 >= -INT15)
            _6867 = _6866 * _the_value_p_12190;
            else
            _6867 = NewDouble(_6866 * (double)_the_value_p_12190);
        }
        else {
            _6867 = binary_op(MULTIPLY, _6866, _the_value_p_12190);
        }
        _6866 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6867;
        if( _1 != _6867 ){
            DeRef(_1);
        }
        _6867 = NOVALUE;
        _6864 = NOVALUE;
        goto L3; // [226] 378

        /** 				case DIVIDE then*/
        case 5:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12194 + ((s1_ptr)_2)->base);
        _6868 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6870 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6868 = NOVALUE;
        if (IS_ATOM_INT(_6870) && IS_ATOM_INT(_the_value_p_12190)) {
            _6871 = (_6870 % _the_value_p_12190) ? NewDouble((double)_6870 / _the_value_p_12190) : (_6870 / _the_value_p_12190);
        }
        else {
            _6871 = binary_op(DIVIDE, _6870, _the_value_p_12190);
        }
        _6870 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6871;
        if( _1 != _6871 ){
            DeRef(_1);
        }
        _6871 = NOVALUE;
        _6868 = NOVALUE;
        goto L3; // [258] 378

        /** 				case APPEND then*/
        case 6:

        /** 					sequence data = map_data[VALUE_BUCKETS][bucket_][index_]*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        _6872 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_6872);
        _6873 = (int)*(((s1_ptr)_2)->base + _bucket__12194);
        _6872 = NOVALUE;
        DeRef(_data_12240);
        _2 = (int)SEQ_PTR(_6873);
        _data_12240 = (int)*(((s1_ptr)_2)->base + _index__12193);
        Ref(_data_12240);
        _6873 = NOVALUE;

        /** 					data = append( data, the_value_p )*/
        Ref(_the_value_p_12190);
        Append(&_data_12240, _data_12240, _the_value_p_12190);

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] = data*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12194 + ((s1_ptr)_2)->base);
        _6876 = NOVALUE;
        RefDS(_data_12240);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _data_12240;
        DeRef(_1);
        _6876 = NOVALUE;
        DeRefDS(_data_12240);
        _data_12240 = NOVALUE;
        goto L3; // [308] 378

        /** 				case CONCAT then*/
        case 7:

        /** 					map_data[VALUE_BUCKETS][bucket_][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(object_ptr)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(object_ptr)_3 = MAKE_SEQ(_2);
        }
        _3 = (int)(_bucket__12194 + ((s1_ptr)_2)->base);
        _6878 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6880 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6878 = NOVALUE;
        if (IS_SEQUENCE(_6880) && IS_ATOM(_the_value_p_12190)) {
            Ref(_the_value_p_12190);
            Append(&_6881, _6880, _the_value_p_12190);
        }
        else if (IS_ATOM(_6880) && IS_SEQUENCE(_the_value_p_12190)) {
            Ref(_6880);
            Prepend(&_6881, _the_value_p_12190, _6880);
        }
        else {
            Concat((object_ptr)&_6881, _6880, _the_value_p_12190);
            _6880 = NOVALUE;
        }
        _6880 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6881;
        if( _1 != _6881 ){
            DeRef(_1);
        }
        _6881 = NOVALUE;
        _6878 = NOVALUE;
        goto L3; // [340] 378

        /** 				case LEAVE then*/
        case 8:

        /** 					operation_p = operation_p*/
        _operation_p_12191 = _operation_p_12191;
        goto L3; // [351] 378

        /** 				case else*/
        default:

        /** 					error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_358_12256);
        _msg_inlined_crash_at_358_12256 = EPrintf(-9999999, _6882, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_358_12256);

        /** end procedure*/
        goto L4; // [372] 375
L4: 
        DeRefi(_msg_inlined_crash_at_358_12256);
        _msg_inlined_crash_at_358_12256 = NOVALUE;
    ;}L3: 

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12197);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12188);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12197;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_12276);
    DeRef(_tmp_seqv_12284);
    DeRef(_the_key_p_12189);
    DeRef(_the_value_p_12190);
    DeRef(_average_length__12195);
    DeRefDS(_map_data_12197);
    _6846 = NOVALUE;
    return;
L2: 

    /** 		if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _6883 = find_from(_operation_p_12191, _29INIT_OPERATIONS_11807, 1);
    if (_6883 != 0)
    goto L5; // [401] 425
    _6883 = NOVALUE;

    /** 				error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_405_12262);
    _msg_inlined_crash_at_405_12262 = EPrintf(-9999999, _6885, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_405_12262);

    /** end procedure*/
    goto L6; // [419] 422
L6: 
    DeRefi(_msg_inlined_crash_at_405_12262);
    _msg_inlined_crash_at_405_12262 = NOVALUE;
L5: 

    /** 		if operation_p = LEAVE then*/
    if (_operation_p_12191 != 8)
    goto L7; // [429] 447

    /** 			eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12197);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12188);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12197;
    DeRef(_1);

    /** 			return*/
    DeRef(_tmp_seqk_12276);
    DeRef(_tmp_seqv_12284);
    DeRef(_the_key_p_12189);
    DeRef(_the_value_p_12190);
    DeRef(_average_length__12195);
    DeRefDS(_map_data_12197);
    _6846 = NOVALUE;
    return;
L7: 

    /** 		if operation_p = APPEND then*/
    if (_operation_p_12191 != 6)
    goto L8; // [451] 462

    /** 			the_value_p = { the_value_p }*/
    _0 = _the_value_p_12190;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_12190);
    *((int *)(_2+4)) = _the_value_p_12190;
    _the_value_p_12190 = MAKE_SEQ(_1);
    DeRef(_0);
L8: 

    /** 		map_data[IN_USE] += (length(map_data[KEY_BUCKETS][bucket_]) = 0)*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6889 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6889);
    _6890 = (int)*(((s1_ptr)_2)->base + _bucket__12194);
    _6889 = NOVALUE;
    if (IS_SEQUENCE(_6890)){
            _6891 = SEQ_PTR(_6890)->length;
    }
    else {
        _6891 = 1;
    }
    _6890 = NOVALUE;
    _6892 = (_6891 == 0);
    _6891 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6893 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_6893)) {
        _6894 = _6893 + _6892;
        if ((long)((unsigned long)_6894 + (unsigned long)HIGH_BITS) >= 0) 
        _6894 = NewDouble((double)_6894);
    }
    else {
        _6894 = binary_op(PLUS, _6893, _6892);
    }
    _6893 = NOVALUE;
    _6892 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _6894;
    if( _1 != _6894 ){
        DeRef(_1);
    }
    _6894 = NOVALUE;

    /** 		map_data[ELEMENT_COUNT] += 1 -- elementCount		*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6895 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6895)) {
        _6896 = _6895 + 1;
        if (_6896 > MAXINT){
            _6896 = NewDouble((double)_6896);
        }
    }
    else
    _6896 = binary_op(PLUS, 1, _6895);
    _6895 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _6896;
    if( _1 != _6896 ){
        DeRef(_1);
    }
    _6896 = NOVALUE;

    /** 		sequence tmp_seqk*/

    /** 		tmp_seqk = map_data[KEY_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6897 = (int)*(((s1_ptr)_2)->base + 5);
    DeRef(_tmp_seqk_12276);
    _2 = (int)SEQ_PTR(_6897);
    _tmp_seqk_12276 = (int)*(((s1_ptr)_2)->base + _bucket__12194);
    Ref(_tmp_seqk_12276);
    _6897 = NOVALUE;

    /** 		map_data[KEY_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12194);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _6899 = NOVALUE;

    /** 		tmp_seqk = append( tmp_seqk, the_key_p)*/
    Ref(_the_key_p_12189);
    Append(&_tmp_seqk_12276, _tmp_seqk_12276, _the_key_p_12189);

    /** 		map_data[KEY_BUCKETS][bucket_] = tmp_seqk*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqk_12276);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12194);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqk_12276;
    DeRef(_1);
    _6902 = NOVALUE;

    /** 		sequence tmp_seqv*/

    /** 		tmp_seqv = map_data[VALUE_BUCKETS][bucket_]*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6904 = (int)*(((s1_ptr)_2)->base + 6);
    DeRef(_tmp_seqv_12284);
    _2 = (int)SEQ_PTR(_6904);
    _tmp_seqv_12284 = (int)*(((s1_ptr)_2)->base + _bucket__12194);
    Ref(_tmp_seqv_12284);
    _6904 = NOVALUE;

    /** 		map_data[VALUE_BUCKETS][bucket_] = 0*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12194);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _6906 = NOVALUE;

    /** 		tmp_seqv = append( tmp_seqv, the_value_p)*/
    Ref(_the_value_p_12190);
    Append(&_tmp_seqv_12284, _tmp_seqv_12284, _the_value_p_12190);

    /** 		map_data[VALUE_BUCKETS][bucket_] = tmp_seqv*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_tmp_seqv_12284);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12194);
    _1 = *(int *)_2;
    *(int *)_2 = _tmp_seqv_12284;
    DeRef(_1);
    _6909 = NOVALUE;

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12197);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12188);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12197;
    DeRef(_1);

    /** 		if trigger_p > 0 then*/
    if (_trigger_p_12192 <= 0)
    goto L9; // [617] 660

    /** 			average_length_ = map_data[ELEMENT_COUNT] / map_data[IN_USE]*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6912 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6913 = (int)*(((s1_ptr)_2)->base + 3);
    DeRef(_average_length__12195);
    if (IS_ATOM_INT(_6912) && IS_ATOM_INT(_6913)) {
        _average_length__12195 = (_6912 % _6913) ? NewDouble((double)_6912 / _6913) : (_6912 / _6913);
    }
    else {
        _average_length__12195 = binary_op(DIVIDE, _6912, _6913);
    }
    _6912 = NOVALUE;
    _6913 = NOVALUE;

    /** 			if (average_length_ >= trigger_p) then*/
    if (binary_op_a(LESS, _average_length__12195, _trigger_p_12192)){
        goto LA; // [641] 659
    }

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_12197);
    _map_data_12197 = _5;

    /** 				rehash(the_map_p)*/
    _29rehash(_the_map_p_12188, 0);
LA: 
L9: 

    /** 		return*/
    DeRef(_tmp_seqk_12276);
    DeRef(_tmp_seqv_12284);
    DeRef(_the_key_p_12189);
    DeRef(_the_value_p_12190);
    DeRef(_average_length__12195);
    DeRef(_map_data_12197);
    _6846 = NOVALUE;
    _6890 = NOVALUE;
    return;
    goto LB; // [667] 1206
L1: 

    /** 		if equal(the_key_p, init_small_map_key) then*/
    if (_the_key_p_12189 == _29init_small_map_key_11813)
    _6916 = 1;
    else if (IS_ATOM_INT(_the_key_p_12189) && IS_ATOM_INT(_29init_small_map_key_11813))
    _6916 = 0;
    else
    _6916 = (compare(_the_key_p_12189, _29init_small_map_key_11813) == 0);
    if (_6916 == 0)
    {
        _6916 = NOVALUE;
        goto LC; // [676] 746
    }
    else{
        _6916 = NOVALUE;
    }

    /** 			from_ = 1*/
    _from__12196 = 1;

    /** 			while index_ > 0 with entry do*/
    goto LD; // [686] 725
LE: 
    if (_index__12193 <= 0)
    goto LF; // [691] 760

    /** 				if map_data[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6918 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_6918);
    _6919 = (int)*(((s1_ptr)_2)->base + _index__12193);
    _6918 = NOVALUE;
    if (binary_op_a(NOTEQ, _6919, 1)){
        _6919 = NOVALUE;
        goto L10; // [707] 716
    }
    _6919 = NOVALUE;

    /** 					exit*/
    goto LF; // [713] 760
L10: 

    /** 				from_ = index_ + 1*/
    _from__12196 = _index__12193 + 1;

    /** 			  entry*/
LD: 

    /** 				index_ = find(the_key_p, map_data[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6922 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12193 = find_from(_the_key_p_12189, _6922, _from__12196);
    _6922 = NOVALUE;

    /** 			end while*/
    goto LE; // [740] 689
    goto LF; // [743] 760
LC: 

    /** 			index_ = find(the_key_p, map_data[KEY_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6924 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12193 = find_from(_the_key_p_12189, _6924, 1);
    _6924 = NOVALUE;
LF: 

    /** 		if index_ = 0 then*/
    if (_index__12193 != 0)
    goto L11; // [764] 962

    /** 			if not eu:find(operation_p, INIT_OPERATIONS) then*/
    _6927 = find_from(_operation_p_12191, _29INIT_OPERATIONS_11807, 1);
    if (_6927 != 0)
    goto L12; // [777] 801
    _6927 = NOVALUE;

    /** 					error:crash("Inappropriate initial operation given to map.e:put()")*/

    /** 	msg = sprintf(fmt, data)*/
    DeRefi(_msg_inlined_crash_at_781_12320);
    _msg_inlined_crash_at_781_12320 = EPrintf(-9999999, _6885, _5);

    /** 	machine_proc(M_CRASH, msg)*/
    machine(67, _msg_inlined_crash_at_781_12320);

    /** end procedure*/
    goto L13; // [795] 798
L13: 
    DeRefi(_msg_inlined_crash_at_781_12320);
    _msg_inlined_crash_at_781_12320 = NOVALUE;
L12: 

    /** 			index_ = find(0, map_data[FREE_LIST])*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6929 = (int)*(((s1_ptr)_2)->base + 7);
    _index__12193 = find_from(0, _6929, 1);
    _6929 = NOVALUE;

    /** 			if index_ = 0 then*/
    if (_index__12193 != 0)
    goto L14; // [816] 870

    /** 				eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12197);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12188);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12197;
    DeRef(_1);

    /** 				map_data = {}*/
    RefDS(_5);
    DeRefDS(_map_data_12197);
    _map_data_12197 = _5;

    /** 				convert_to_large_map(the_map_p)*/
    _29convert_to_large_map(_the_map_p_12188);

    /** 				put(the_map_p, the_key_p, the_value_p, operation_p, trigger_p)*/
    DeRef(_6932);
    _6932 = _the_map_p_12188;
    Ref(_the_key_p_12189);
    DeRef(_6933);
    _6933 = _the_key_p_12189;
    Ref(_the_value_p_12190);
    DeRef(_6934);
    _6934 = _the_value_p_12190;
    DeRef(_6935);
    _6935 = _operation_p_12191;
    DeRef(_6936);
    _6936 = _trigger_p_12192;
    _29put(_6932, _6933, _6934, _6935, _6936);
    _6932 = NOVALUE;
    _6933 = NOVALUE;
    _6934 = NOVALUE;
    _6935 = NOVALUE;
    _6936 = NOVALUE;

    /** 				return*/
    DeRef(_the_key_p_12189);
    DeRef(_the_value_p_12190);
    DeRef(_average_length__12195);
    DeRefDS(_map_data_12197);
    _6846 = NOVALUE;
    _6890 = NOVALUE;
    return;
L14: 

    /** 			map_data[KEY_LIST][index_] = the_key_p*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    Ref(_the_key_p_12189);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12193);
    _1 = *(int *)_2;
    *(int *)_2 = _the_key_p_12189;
    DeRef(_1);
    _6937 = NOVALUE;

    /** 			map_data[FREE_LIST][index_] = 1*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12193);
    _1 = *(int *)_2;
    *(int *)_2 = 1;
    DeRef(_1);
    _6939 = NOVALUE;

    /** 			map_data[IN_USE] += 1*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6941 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_6941)) {
        _6942 = _6941 + 1;
        if (_6942 > MAXINT){
            _6942 = NewDouble((double)_6942);
        }
    }
    else
    _6942 = binary_op(PLUS, 1, _6941);
    _6941 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _6942;
    if( _1 != _6942 ){
        DeRef(_1);
    }
    _6942 = NOVALUE;

    /** 			map_data[ELEMENT_COUNT] += 1*/
    _2 = (int)SEQ_PTR(_map_data_12197);
    _6943 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6943)) {
        _6944 = _6943 + 1;
        if (_6944 > MAXINT){
            _6944 = NewDouble((double)_6944);
        }
    }
    else
    _6944 = binary_op(PLUS, 1, _6943);
    _6943 = NOVALUE;
    _2 = (int)SEQ_PTR(_map_data_12197);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _map_data_12197 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _6944;
    if( _1 != _6944 ){
        DeRef(_1);
    }
    _6944 = NOVALUE;

    /** 			if operation_p = APPEND then*/
    if (_operation_p_12191 != 6)
    goto L15; // [932] 943

    /** 				the_value_p = { the_value_p }*/
    _0 = _the_value_p_12190;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_the_value_p_12190);
    *((int *)(_2+4)) = _the_value_p_12190;
    _the_value_p_12190 = MAKE_SEQ(_1);
    DeRef(_0);
L15: 

    /** 			if operation_p != LEAVE then*/
    if (_operation_p_12191 == 8)
    goto L16; // [947] 961

    /** 				operation_p = PUT	-- Initially, nearly everything is a PUT.*/
    _operation_p_12191 = 1;
L16: 
L11: 

    /** 		switch operation_p do*/
    _0 = _operation_p_12191;
    switch ( _0 ){ 

        /** 			case PUT then*/
        case 1:

        /** 				map_data[VALUE_LIST][index_] = the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        Ref(_the_value_p_12190);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _the_value_p_12190;
        DeRef(_1);
        _6950 = NOVALUE;
        goto L17; // [986] 1192

        /** 			case ADD then*/
        case 2:

        /** 				map_data[VALUE_LIST][index_] += the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6954 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6952 = NOVALUE;
        if (IS_ATOM_INT(_6954) && IS_ATOM_INT(_the_value_p_12190)) {
            _6955 = _6954 + _the_value_p_12190;
            if ((long)((unsigned long)_6955 + (unsigned long)HIGH_BITS) >= 0) 
            _6955 = NewDouble((double)_6955);
        }
        else {
            _6955 = binary_op(PLUS, _6954, _the_value_p_12190);
        }
        _6954 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6955;
        if( _1 != _6955 ){
            DeRef(_1);
        }
        _6955 = NOVALUE;
        _6952 = NOVALUE;
        goto L17; // [1013] 1192

        /** 			case SUBTRACT then*/
        case 3:

        /** 				map_data[VALUE_LIST][index_] -= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6958 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6956 = NOVALUE;
        if (IS_ATOM_INT(_6958) && IS_ATOM_INT(_the_value_p_12190)) {
            _6959 = _6958 - _the_value_p_12190;
            if ((long)((unsigned long)_6959 +(unsigned long) HIGH_BITS) >= 0){
                _6959 = NewDouble((double)_6959);
            }
        }
        else {
            _6959 = binary_op(MINUS, _6958, _the_value_p_12190);
        }
        _6958 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6959;
        if( _1 != _6959 ){
            DeRef(_1);
        }
        _6959 = NOVALUE;
        _6956 = NOVALUE;
        goto L17; // [1040] 1192

        /** 			case MULTIPLY then*/
        case 4:

        /** 				map_data[VALUE_LIST][index_] *= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6962 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6960 = NOVALUE;
        if (IS_ATOM_INT(_6962) && IS_ATOM_INT(_the_value_p_12190)) {
            if (_6962 == (short)_6962 && _the_value_p_12190 <= INT15 && _the_value_p_12190 >= -INT15)
            _6963 = _6962 * _the_value_p_12190;
            else
            _6963 = NewDouble(_6962 * (double)_the_value_p_12190);
        }
        else {
            _6963 = binary_op(MULTIPLY, _6962, _the_value_p_12190);
        }
        _6962 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6963;
        if( _1 != _6963 ){
            DeRef(_1);
        }
        _6963 = NOVALUE;
        _6960 = NOVALUE;
        goto L17; // [1067] 1192

        /** 			case DIVIDE then*/
        case 5:

        /** 				map_data[VALUE_LIST][index_] /= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6966 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6964 = NOVALUE;
        if (IS_ATOM_INT(_6966) && IS_ATOM_INT(_the_value_p_12190)) {
            _6967 = (_6966 % _the_value_p_12190) ? NewDouble((double)_6966 / _the_value_p_12190) : (_6966 / _the_value_p_12190);
        }
        else {
            _6967 = binary_op(DIVIDE, _6966, _the_value_p_12190);
        }
        _6966 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6967;
        if( _1 != _6967 ){
            DeRef(_1);
        }
        _6967 = NOVALUE;
        _6964 = NOVALUE;
        goto L17; // [1094] 1192

        /** 			case APPEND then*/
        case 6:

        /** 				map_data[VALUE_LIST][index_] = append( map_data[VALUE_LIST][index_], the_value_p )*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_map_data_12197);
        _6970 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_6970);
        _6971 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6970 = NOVALUE;
        Ref(_the_value_p_12190);
        Append(&_6972, _6971, _the_value_p_12190);
        _6971 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6972;
        if( _1 != _6972 ){
            DeRef(_1);
        }
        _6972 = NOVALUE;
        _6968 = NOVALUE;
        goto L17; // [1127] 1192

        /** 			case CONCAT then*/
        case 7:

        /** 				map_data[VALUE_LIST][index_] &= the_value_p*/
        _2 = (int)SEQ_PTR(_map_data_12197);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _map_data_12197 = MAKE_SEQ(_2);
        }
        _3 = (int)(6 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(*(int *)_3);
        _6975 = (int)*(((s1_ptr)_2)->base + _index__12193);
        _6973 = NOVALUE;
        if (IS_SEQUENCE(_6975) && IS_ATOM(_the_value_p_12190)) {
            Ref(_the_value_p_12190);
            Append(&_6976, _6975, _the_value_p_12190);
        }
        else if (IS_ATOM(_6975) && IS_SEQUENCE(_the_value_p_12190)) {
            Ref(_6975);
            Prepend(&_6976, _the_value_p_12190, _6975);
        }
        else {
            Concat((object_ptr)&_6976, _6975, _the_value_p_12190);
            _6975 = NOVALUE;
        }
        _6975 = NOVALUE;
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _index__12193);
        _1 = *(int *)_2;
        *(int *)_2 = _6976;
        if( _1 != _6976 ){
            DeRef(_1);
        }
        _6976 = NOVALUE;
        _6973 = NOVALUE;
        goto L17; // [1154] 1192

        /** 			case LEAVE then*/
        case 8:

        /** 				operation_p = operation_p*/
        _operation_p_12191 = _operation_p_12191;
        goto L17; // [1165] 1192

        /** 			case else*/
        default:

        /** 				error:crash("Unknown operation given to map.e:put()")*/

        /** 	msg = sprintf(fmt, data)*/
        DeRefi(_msg_inlined_crash_at_1173_12383);
        _msg_inlined_crash_at_1173_12383 = EPrintf(-9999999, _6882, _5);

        /** 	machine_proc(M_CRASH, msg)*/
        machine(67, _msg_inlined_crash_at_1173_12383);

        /** end procedure*/
        goto L18; // [1186] 1189
L18: 
        DeRefi(_msg_inlined_crash_at_1173_12383);
        _msg_inlined_crash_at_1173_12383 = NOVALUE;
    ;}L17: 

    /** 		eumem:ram_space[the_map_p] = map_data*/
    RefDS(_map_data_12197);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12188);
    _1 = *(int *)_2;
    *(int *)_2 = _map_data_12197;
    DeRef(_1);

    /** 		return*/
    DeRef(_the_key_p_12189);
    DeRef(_the_value_p_12190);
    DeRef(_average_length__12195);
    DeRefDS(_map_data_12197);
    _6846 = NOVALUE;
    _6890 = NOVALUE;
    return;
LB: 

    /** end procedure*/
    DeRef(_the_key_p_12189);
    DeRef(_the_value_p_12190);
    DeRef(_average_length__12195);
    DeRef(_map_data_12197);
    _6846 = NOVALUE;
    _6890 = NOVALUE;
    return;
    ;
}


void _29nested_put(int _the_map_p_12386, int _the_keys_p_12387, int _the_value_p_12388, int _operation_p_12389, int _trigger_p_12390)
{
    int _temp_map__12391 = NOVALUE;
    int _6989 = NOVALUE;
    int _6988 = NOVALUE;
    int _6987 = NOVALUE;
    int _6986 = NOVALUE;
    int _6985 = NOVALUE;
    int _6984 = NOVALUE;
    int _6983 = NOVALUE;
    int _6981 = NOVALUE;
    int _6980 = NOVALUE;
    int _6979 = NOVALUE;
    int _6977 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( the_keys_p ) = 1 then*/
    if (IS_SEQUENCE(_the_keys_p_12387)){
            _6977 = SEQ_PTR(_the_keys_p_12387)->length;
    }
    else {
        _6977 = 1;
    }
    if (_6977 != 1)
    goto L1; // [12] 32

    /** 		put( the_map_p, the_keys_p[1], the_value_p, operation_p, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12387);
    _6979 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12386);
    Ref(_6979);
    Ref(_the_value_p_12388);
    _29put(_the_map_p_12386, _6979, _the_value_p_12388, _operation_p_12389, _trigger_p_12390);
    _6979 = NOVALUE;
    goto L2; // [29] 94
L1: 

    /** 		temp_map_ = new_extra( get( the_map_p, the_keys_p[1] ) )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12387);
    _6980 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12386);
    Ref(_6980);
    _6981 = _29get(_the_map_p_12386, _6980, 0);
    _6980 = NOVALUE;
    _0 = _temp_map__12391;
    _temp_map__12391 = _29new_extra(_6981, 690);
    DeRef(_0);
    _6981 = NOVALUE;

    /** 		nested_put( temp_map_, the_keys_p[2..$], the_value_p, operation_p, trigger_p )*/
    if (IS_SEQUENCE(_the_keys_p_12387)){
            _6983 = SEQ_PTR(_the_keys_p_12387)->length;
    }
    else {
        _6983 = 1;
    }
    rhs_slice_target = (object_ptr)&_6984;
    RHS_Slice(_the_keys_p_12387, 2, _6983);
    Ref(_temp_map__12391);
    DeRef(_6985);
    _6985 = _temp_map__12391;
    Ref(_the_value_p_12388);
    DeRef(_6986);
    _6986 = _the_value_p_12388;
    DeRef(_6987);
    _6987 = _operation_p_12389;
    DeRef(_6988);
    _6988 = _trigger_p_12390;
    _29nested_put(_6985, _6984, _6986, _6987, _6988);
    _6985 = NOVALUE;
    _6984 = NOVALUE;
    _6986 = NOVALUE;
    _6987 = NOVALUE;
    _6988 = NOVALUE;

    /** 		put( the_map_p, the_keys_p[1], temp_map_, PUT, trigger_p )*/
    _2 = (int)SEQ_PTR(_the_keys_p_12387);
    _6989 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_the_map_p_12386);
    Ref(_6989);
    Ref(_temp_map__12391);
    _29put(_the_map_p_12386, _6989, _temp_map__12391, 1, _trigger_p_12390);
    _6989 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_the_map_p_12386);
    DeRefDS(_the_keys_p_12387);
    DeRef(_the_value_p_12388);
    DeRef(_temp_map__12391);
    return;
    ;
}


void _29remove(int _the_map_p_12409, int _the_key_p_12410)
{
    int _index__12411 = NOVALUE;
    int _bucket__12412 = NOVALUE;
    int _temp_map__12413 = NOVALUE;
    int _from__12414 = NOVALUE;
    int _calc_hash_1__tmp_at44_12425 = NOVALUE;
    int _calc_hash_inlined_calc_hash_at_44_12424 = NOVALUE;
    int _ret__inlined_calc_hash_at_44_12423 = NOVALUE;
    int _max_hash_p_inlined_calc_hash_at_41_12422 = NOVALUE;
    int _7054 = NOVALUE;
    int _7053 = NOVALUE;
    int _7052 = NOVALUE;
    int _7051 = NOVALUE;
    int _7049 = NOVALUE;
    int _7047 = NOVALUE;
    int _7045 = NOVALUE;
    int _7043 = NOVALUE;
    int _7042 = NOVALUE;
    int _7040 = NOVALUE;
    int _7037 = NOVALUE;
    int _7036 = NOVALUE;
    int _7035 = NOVALUE;
    int _7034 = NOVALUE;
    int _7033 = NOVALUE;
    int _7032 = NOVALUE;
    int _7031 = NOVALUE;
    int _7030 = NOVALUE;
    int _7029 = NOVALUE;
    int _7028 = NOVALUE;
    int _7027 = NOVALUE;
    int _7026 = NOVALUE;
    int _7025 = NOVALUE;
    int _7023 = NOVALUE;
    int _7022 = NOVALUE;
    int _7021 = NOVALUE;
    int _7020 = NOVALUE;
    int _7019 = NOVALUE;
    int _7018 = NOVALUE;
    int _7017 = NOVALUE;
    int _7016 = NOVALUE;
    int _7015 = NOVALUE;
    int _7014 = NOVALUE;
    int _7013 = NOVALUE;
    int _7011 = NOVALUE;
    int _7009 = NOVALUE;
    int _7007 = NOVALUE;
    int _7006 = NOVALUE;
    int _7005 = NOVALUE;
    int _7003 = NOVALUE;
    int _7002 = NOVALUE;
    int _7001 = NOVALUE;
    int _7000 = NOVALUE;
    int _6999 = NOVALUE;
    int _6996 = NOVALUE;
    int _6995 = NOVALUE;
    int _6994 = NOVALUE;
    int _6993 = NOVALUE;
    int _6991 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12413);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_the_map_p_12409)){
        _temp_map__12413 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12409)->dbl));
    }
    else{
        _temp_map__12413 = (int)*(((s1_ptr)_2)->base + _the_map_p_12409);
    }
    Ref(_temp_map__12413);

    /** 	eumem:ram_space[the_map_p] = 0*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12409))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12409)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12409);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _6991 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _6991, 76)){
        _6991 = NOVALUE;
        goto L1; // [27] 337
    }
    _6991 = NOVALUE;

    /** 		bucket_ = calc_hash(the_key_p, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _6993 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_6993)){
            _6994 = SEQ_PTR(_6993)->length;
    }
    else {
        _6994 = 1;
    }
    _6993 = NOVALUE;
    _max_hash_p_inlined_calc_hash_at_41_12422 = _6994;
    _6994 = NOVALUE;

    /**     ret_ = hash(key_p, stdhash:HSIEH30)*/
    DeRef(_ret__inlined_calc_hash_at_44_12423);
    _ret__inlined_calc_hash_at_44_12423 = calc_hash(_the_key_p_12410, -6);
    if (!IS_ATOM_INT(_ret__inlined_calc_hash_at_44_12423)) {
        _1 = (long)(DBL_PTR(_ret__inlined_calc_hash_at_44_12423)->dbl);
        if (UNIQUE(DBL_PTR(_ret__inlined_calc_hash_at_44_12423)) && (DBL_PTR(_ret__inlined_calc_hash_at_44_12423)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret__inlined_calc_hash_at_44_12423);
        _ret__inlined_calc_hash_at_44_12423 = _1;
    }

    /** 	return remainder(ret_, max_hash_p) + 1 -- 1-based*/
    _calc_hash_1__tmp_at44_12425 = (_ret__inlined_calc_hash_at_44_12423 % _max_hash_p_inlined_calc_hash_at_41_12422);
    _bucket__12412 = _calc_hash_1__tmp_at44_12425 + 1;
    DeRef(_ret__inlined_calc_hash_at_44_12423);
    _ret__inlined_calc_hash_at_44_12423 = NOVALUE;
    if (!IS_ATOM_INT(_bucket__12412)) {
        _1 = (long)(DBL_PTR(_bucket__12412)->dbl);
        if (UNIQUE(DBL_PTR(_bucket__12412)) && (DBL_PTR(_bucket__12412)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_bucket__12412);
        _bucket__12412 = _1;
    }

    /** 		index_ = find(the_key_p, temp_map_[KEY_BUCKETS][bucket_])*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _6995 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_6995);
    _6996 = (int)*(((s1_ptr)_2)->base + _bucket__12412);
    _6995 = NOVALUE;
    _index__12411 = find_from(_the_key_p_12410, _6996, 1);
    _6996 = NOVALUE;

    /** 		if index_ != 0 then*/
    if (_index__12411 == 0)
    goto L2; // [91] 477

    /** 			temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _6999 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_6999)) {
        _7000 = _6999 - 1;
        if ((long)((unsigned long)_7000 +(unsigned long) HIGH_BITS) >= 0){
            _7000 = NewDouble((double)_7000);
        }
    }
    else {
        _7000 = binary_op(MINUS, _6999, 1);
    }
    _6999 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7000;
    if( _1 != _7000 ){
        DeRef(_1);
    }
    _7000 = NOVALUE;

    /** 			if length(temp_map_[KEY_BUCKETS][bucket_]) = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7001 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7001);
    _7002 = (int)*(((s1_ptr)_2)->base + _bucket__12412);
    _7001 = NOVALUE;
    if (IS_SEQUENCE(_7002)){
            _7003 = SEQ_PTR(_7002)->length;
    }
    else {
        _7003 = 1;
    }
    _7002 = NOVALUE;
    if (_7003 != 1)
    goto L3; // [126] 175

    /** 				temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7005 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7005)) {
        _7006 = _7005 - 1;
        if ((long)((unsigned long)_7006 +(unsigned long) HIGH_BITS) >= 0){
            _7006 = NewDouble((double)_7006);
        }
    }
    else {
        _7006 = binary_op(MINUS, _7005, 1);
    }
    _7005 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7006;
    if( _1 != _7006 ){
        DeRef(_1);
    }
    _7006 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12412);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7007 = NOVALUE;

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = {}*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    RefDS(_5);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12412);
    _1 = *(int *)_2;
    *(int *)_2 = _5;
    DeRef(_1);
    _7009 = NOVALUE;
    goto L4; // [172] 292
L3: 

    /** 				temp_map_[VALUE_BUCKETS][bucket_] = temp_map_[VALUE_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7013 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7013);
    _7014 = (int)*(((s1_ptr)_2)->base + _bucket__12412);
    _7013 = NOVALUE;
    _7015 = _index__12411 - 1;
    rhs_slice_target = (object_ptr)&_7016;
    RHS_Slice(_7014, 1, _7015);
    _7014 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7017 = (int)*(((s1_ptr)_2)->base + 6);
    _2 = (int)SEQ_PTR(_7017);
    _7018 = (int)*(((s1_ptr)_2)->base + _bucket__12412);
    _7017 = NOVALUE;
    _7019 = _index__12411 + 1;
    if (_7019 > MAXINT){
        _7019 = NewDouble((double)_7019);
    }
    if (IS_SEQUENCE(_7018)){
            _7020 = SEQ_PTR(_7018)->length;
    }
    else {
        _7020 = 1;
    }
    rhs_slice_target = (object_ptr)&_7021;
    RHS_Slice(_7018, _7019, _7020);
    _7018 = NOVALUE;
    Concat((object_ptr)&_7022, _7016, _7021);
    DeRefDS(_7016);
    _7016 = NOVALUE;
    DeRef(_7016);
    _7016 = NOVALUE;
    DeRefDS(_7021);
    _7021 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12412);
    _1 = *(int *)_2;
    *(int *)_2 = _7022;
    if( _1 != _7022 ){
        DeRef(_1);
    }
    _7022 = NOVALUE;
    _7011 = NOVALUE;

    /** 				temp_map_[KEY_BUCKETS][bucket_] = temp_map_[KEY_BUCKETS][bucket_][1 .. index_-1] & */
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7025 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7025);
    _7026 = (int)*(((s1_ptr)_2)->base + _bucket__12412);
    _7025 = NOVALUE;
    _7027 = _index__12411 - 1;
    rhs_slice_target = (object_ptr)&_7028;
    RHS_Slice(_7026, 1, _7027);
    _7026 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7029 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_7029);
    _7030 = (int)*(((s1_ptr)_2)->base + _bucket__12412);
    _7029 = NOVALUE;
    _7031 = _index__12411 + 1;
    if (_7031 > MAXINT){
        _7031 = NewDouble((double)_7031);
    }
    if (IS_SEQUENCE(_7030)){
            _7032 = SEQ_PTR(_7030)->length;
    }
    else {
        _7032 = 1;
    }
    rhs_slice_target = (object_ptr)&_7033;
    RHS_Slice(_7030, _7031, _7032);
    _7030 = NOVALUE;
    Concat((object_ptr)&_7034, _7028, _7033);
    DeRefDS(_7028);
    _7028 = NOVALUE;
    DeRef(_7028);
    _7028 = NOVALUE;
    DeRefDS(_7033);
    _7033 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _bucket__12412);
    _1 = *(int *)_2;
    *(int *)_2 = _7034;
    if( _1 != _7034 ){
        DeRef(_1);
    }
    _7034 = NOVALUE;
    _7023 = NOVALUE;
L4: 

    /** 			if temp_map_[ELEMENT_COUNT] < floor(51 * threshold_size / 100) then*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7035 = (int)*(((s1_ptr)_2)->base + 2);
    _7036 = 1173;
    _7037 = 11;
    _7036 = NOVALUE;
    if (binary_op_a(GREATEREQ, _7035, 11)){
        _7035 = NOVALUE;
        _7037 = NOVALUE;
        goto L2; // [310] 477
    }
    _7035 = NOVALUE;
    DeRef(_7037);
    _7037 = NOVALUE;

    /** 				eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12413);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12409))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12409)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12409);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12413;
    DeRef(_1);

    /** 				convert_to_small_map(the_map_p)*/
    Ref(_the_map_p_12409);
    _29convert_to_small_map(_the_map_p_12409);

    /** 				return*/
    DeRef(_the_map_p_12409);
    DeRef(_the_key_p_12410);
    DeRefDS(_temp_map__12413);
    _6993 = NOVALUE;
    _7002 = NOVALUE;
    DeRef(_7015);
    _7015 = NOVALUE;
    DeRef(_7027);
    _7027 = NOVALUE;
    DeRef(_7019);
    _7019 = NOVALUE;
    DeRef(_7031);
    _7031 = NOVALUE;
    return;
    goto L2; // [334] 477
L1: 

    /** 		from_ = 1*/
    _from__12414 = 1;

    /** 		while from_ > 0 do*/
L5: 
    if (_from__12414 <= 0)
    goto L6; // [347] 476

    /** 			index_ = find(the_key_p, temp_map_[KEY_LIST], from_)*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7040 = (int)*(((s1_ptr)_2)->base + 5);
    _index__12411 = find_from(_the_key_p_12410, _7040, _from__12414);
    _7040 = NOVALUE;

    /** 			if index_ then*/
    if (_index__12411 == 0)
    {
        goto L6; // [366] 476
    }
    else{
    }

    /** 				if temp_map_[FREE_LIST][index_] = 1 then*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7042 = (int)*(((s1_ptr)_2)->base + 7);
    _2 = (int)SEQ_PTR(_7042);
    _7043 = (int)*(((s1_ptr)_2)->base + _index__12411);
    _7042 = NOVALUE;
    if (binary_op_a(NOTEQ, _7043, 1)){
        _7043 = NOVALUE;
        goto L7; // [381] 465
    }
    _7043 = NOVALUE;

    /** 					temp_map_[FREE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _3 = (int)(7 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12411);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7045 = NOVALUE;

    /** 					temp_map_[KEY_LIST][index_] = init_small_map_key*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _3 = (int)(5 + ((s1_ptr)_2)->base);
    RefDS(_29init_small_map_key_11813);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12411);
    _1 = *(int *)_2;
    *(int *)_2 = _29init_small_map_key_11813;
    DeRef(_1);
    _7047 = NOVALUE;

    /** 					temp_map_[VALUE_LIST][index_] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _3 = (int)(6 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _index__12411);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);
    _7049 = NOVALUE;

    /** 					temp_map_[IN_USE] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7051 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_7051)) {
        _7052 = _7051 - 1;
        if ((long)((unsigned long)_7052 +(unsigned long) HIGH_BITS) >= 0){
            _7052 = NewDouble((double)_7052);
        }
    }
    else {
        _7052 = binary_op(MINUS, _7051, 1);
    }
    _7051 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = _7052;
    if( _1 != _7052 ){
        DeRef(_1);
    }
    _7052 = NOVALUE;

    /** 					temp_map_[ELEMENT_COUNT] -= 1*/
    _2 = (int)SEQ_PTR(_temp_map__12413);
    _7053 = (int)*(((s1_ptr)_2)->base + 2);
    if (IS_ATOM_INT(_7053)) {
        _7054 = _7053 - 1;
        if ((long)((unsigned long)_7054 +(unsigned long) HIGH_BITS) >= 0){
            _7054 = NewDouble((double)_7054);
        }
    }
    else {
        _7054 = binary_op(MINUS, _7053, 1);
    }
    _7053 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12413);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12413 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _7054;
    if( _1 != _7054 ){
        DeRef(_1);
    }
    _7054 = NOVALUE;
    goto L7; // [457] 465

    /** 				exit*/
    goto L6; // [462] 476
L7: 

    /** 			from_ = index_ + 1*/
    _from__12414 = _index__12411 + 1;

    /** 		end while*/
    goto L5; // [473] 347
L6: 
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12413);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12409))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12409)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12409);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12413;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_12409);
    DeRef(_the_key_p_12410);
    DeRefDS(_temp_map__12413);
    _6993 = NOVALUE;
    _7002 = NOVALUE;
    DeRef(_7015);
    _7015 = NOVALUE;
    DeRef(_7027);
    _7027 = NOVALUE;
    DeRef(_7019);
    _7019 = NOVALUE;
    DeRef(_7031);
    _7031 = NOVALUE;
    return;
    ;
}


void _29clear(int _the_map_p_12499)
{
    int _temp_map__12500 = NOVALUE;
    int _7073 = NOVALUE;
    int _7072 = NOVALUE;
    int _7071 = NOVALUE;
    int _7070 = NOVALUE;
    int _7069 = NOVALUE;
    int _7068 = NOVALUE;
    int _7067 = NOVALUE;
    int _7066 = NOVALUE;
    int _7065 = NOVALUE;
    int _7064 = NOVALUE;
    int _7063 = NOVALUE;
    int _7062 = NOVALUE;
    int _7061 = NOVALUE;
    int _7060 = NOVALUE;
    int _7059 = NOVALUE;
    int _7057 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12500);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_the_map_p_12499)){
        _temp_map__12500 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12499)->dbl));
    }
    else{
        _temp_map__12500 = (int)*(((s1_ptr)_2)->base + _the_map_p_12499);
    }
    Ref(_temp_map__12500);

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    _7057 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7057, 76)){
        _7057 = NOVALUE;
        goto L1; // [19] 84
    }
    _7057 = NOVALUE;

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_BUCKETS] = repeat({}, length(temp_map_[KEY_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    _7059 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7059)){
            _7060 = SEQ_PTR(_7059)->length;
    }
    else {
        _7060 = 1;
    }
    _7059 = NOVALUE;
    _7061 = Repeat(_5, _7060);
    _7060 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7061;
    if( _1 != _7061 ){
        DeRef(_1);
    }
    _7061 = NOVALUE;

    /** 		temp_map_[VALUE_BUCKETS] = repeat({}, length(temp_map_[VALUE_BUCKETS]))*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    _7062 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7062)){
            _7063 = SEQ_PTR(_7062)->length;
    }
    else {
        _7063 = 1;
    }
    _7062 = NOVALUE;
    _7064 = Repeat(_5, _7063);
    _7063 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7064;
    if( _1 != _7064 ){
        DeRef(_1);
    }
    _7064 = NOVALUE;
    goto L2; // [81] 164
L1: 

    /** 		temp_map_[ELEMENT_COUNT] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[IN_USE] = 0*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 3);
    _1 = *(int *)_2;
    *(int *)_2 = 0;
    DeRef(_1);

    /** 		temp_map_[KEY_LIST] = repeat(init_small_map_key, length(temp_map_[KEY_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    _7065 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7065)){
            _7066 = SEQ_PTR(_7065)->length;
    }
    else {
        _7066 = 1;
    }
    _7065 = NOVALUE;
    _7067 = Repeat(_29init_small_map_key_11813, _7066);
    _7066 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = _7067;
    if( _1 != _7067 ){
        DeRef(_1);
    }
    _7067 = NOVALUE;

    /** 		temp_map_[VALUE_LIST] = repeat(0, length(temp_map_[VALUE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    _7068 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_SEQUENCE(_7068)){
            _7069 = SEQ_PTR(_7068)->length;
    }
    else {
        _7069 = 1;
    }
    _7068 = NOVALUE;
    _7070 = Repeat(0, _7069);
    _7069 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 6);
    _1 = *(int *)_2;
    *(int *)_2 = _7070;
    if( _1 != _7070 ){
        DeRef(_1);
    }
    _7070 = NOVALUE;

    /** 		temp_map_[FREE_LIST] = repeat(0, length(temp_map_[FREE_LIST]))*/
    _2 = (int)SEQ_PTR(_temp_map__12500);
    _7071 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7071)){
            _7072 = SEQ_PTR(_7071)->length;
    }
    else {
        _7072 = 1;
    }
    _7071 = NOVALUE;
    _7073 = Repeat(0, _7072);
    _7072 = NOVALUE;
    _2 = (int)SEQ_PTR(_temp_map__12500);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _temp_map__12500 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 7);
    _1 = *(int *)_2;
    *(int *)_2 = _7073;
    if( _1 != _7073 ){
        DeRef(_1);
    }
    _7073 = NOVALUE;
L2: 

    /** 	eumem:ram_space[the_map_p] = temp_map_*/
    RefDS(_temp_map__12500);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_the_map_p_12499))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12499)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _the_map_p_12499);
    _1 = *(int *)_2;
    *(int *)_2 = _temp_map__12500;
    DeRef(_1);

    /** end procedure*/
    DeRef(_the_map_p_12499);
    DeRefDS(_temp_map__12500);
    _7059 = NOVALUE;
    _7062 = NOVALUE;
    _7065 = NOVALUE;
    _7068 = NOVALUE;
    _7071 = NOVALUE;
    return;
    ;
}


int _29keys(int _the_map_p_12587, int _sorted_result_12588)
{
    int _buckets__12589 = NOVALUE;
    int _current_bucket__12590 = NOVALUE;
    int _results__12591 = NOVALUE;
    int _pos__12592 = NOVALUE;
    int _temp_map__12593 = NOVALUE;
    int _7137 = NOVALUE;
    int _7135 = NOVALUE;
    int _7134 = NOVALUE;
    int _7132 = NOVALUE;
    int _7131 = NOVALUE;
    int _7130 = NOVALUE;
    int _7129 = NOVALUE;
    int _7127 = NOVALUE;
    int _7126 = NOVALUE;
    int _7125 = NOVALUE;
    int _7124 = NOVALUE;
    int _7122 = NOVALUE;
    int _7120 = NOVALUE;
    int _7117 = NOVALUE;
    int _7115 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12593);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_the_map_p_12587)){
        _temp_map__12593 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12587)->dbl));
    }
    else{
        _temp_map__12593 = (int)*(((s1_ptr)_2)->base + _the_map_p_12587);
    }
    Ref(_temp_map__12593);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _7115 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12591);
    _results__12591 = Repeat(0, _7115);
    _7115 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12592 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _7117 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7117, 76)){
        _7117 = NOVALUE;
        goto L1; // [38] 119
    }
    _7117 = NOVALUE;

    /** 		buckets_ = temp_map_[KEY_BUCKETS]*/
    DeRef(_buckets__12589);
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _buckets__12589 = (int)*(((s1_ptr)_2)->base + 5);
    Ref(_buckets__12589);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__12589)){
            _7120 = SEQ_PTR(_buckets__12589)->length;
    }
    else {
        _7120 = 1;
    }
    {
        int _index_12602;
        _index_12602 = 1;
L2: 
        if (_index_12602 > _7120){
            goto L3; // [57] 116
        }

        /** 			current_bucket_ = buckets_[index]*/
        DeRef(_current_bucket__12590);
        _2 = (int)SEQ_PTR(_buckets__12589);
        _current_bucket__12590 = (int)*(((s1_ptr)_2)->base + _index_12602);
        Ref(_current_bucket__12590);

        /** 			if length(current_bucket_) > 0 then*/
        if (IS_SEQUENCE(_current_bucket__12590)){
                _7122 = SEQ_PTR(_current_bucket__12590)->length;
        }
        else {
            _7122 = 1;
        }
        if (_7122 <= 0)
        goto L4; // [77] 109

        /** 				results_[pos_ .. pos_ + length(current_bucket_) - 1] = current_bucket_*/
        if (IS_SEQUENCE(_current_bucket__12590)){
                _7124 = SEQ_PTR(_current_bucket__12590)->length;
        }
        else {
            _7124 = 1;
        }
        _7125 = _pos__12592 + _7124;
        if ((long)((unsigned long)_7125 + (unsigned long)HIGH_BITS) >= 0) 
        _7125 = NewDouble((double)_7125);
        _7124 = NOVALUE;
        if (IS_ATOM_INT(_7125)) {
            _7126 = _7125 - 1;
        }
        else {
            _7126 = NewDouble(DBL_PTR(_7125)->dbl - (double)1);
        }
        DeRef(_7125);
        _7125 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__12591;
        AssignSlice(_pos__12592, _7126, _current_bucket__12590);
        DeRef(_7126);
        _7126 = NOVALUE;

        /** 				pos_ += length(current_bucket_)*/
        if (IS_SEQUENCE(_current_bucket__12590)){
                _7127 = SEQ_PTR(_current_bucket__12590)->length;
        }
        else {
            _7127 = 1;
        }
        _pos__12592 = _pos__12592 + _7127;
        _7127 = NOVALUE;
L4: 

        /** 		end for*/
        _index_12602 = _index_12602 + 1;
        goto L2; // [111] 64
L3: 
        ;
    }
    goto L5; // [116] 184
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12593);
    _7129 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7129)){
            _7130 = SEQ_PTR(_7129)->length;
    }
    else {
        _7130 = 1;
    }
    _7129 = NOVALUE;
    {
        int _index_12615;
        _index_12615 = 1;
L6: 
        if (_index_12615 > _7130){
            goto L7; // [130] 183
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12593);
        _7131 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7131);
        _7132 = (int)*(((s1_ptr)_2)->base + _index_12615);
        _7131 = NOVALUE;
        if (binary_op_a(EQUALS, _7132, 0)){
            _7132 = NOVALUE;
            goto L8; // [149] 176
        }
        _7132 = NOVALUE;

        /** 				results_[pos_] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12593);
        _7134 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7134);
        _7135 = (int)*(((s1_ptr)_2)->base + _index_12615);
        _7134 = NOVALUE;
        Ref(_7135);
        _2 = (int)SEQ_PTR(_results__12591);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12591 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__12592);
        _1 = *(int *)_2;
        *(int *)_2 = _7135;
        if( _1 != _7135 ){
            DeRef(_1);
        }
        _7135 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12592 = _pos__12592 + 1;
L8: 

        /** 		end for*/
        _index_12615 = _index_12615 + 1;
        goto L6; // [178] 137
L7: 
        ;
    }
L5: 

    /** 	if sorted_result then*/
    if (_sorted_result_12588 == 0)
    {
        goto L9; // [186] 203
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__12591);
    _7137 = _22sort(_results__12591, 1);
    DeRef(_the_map_p_12587);
    DeRef(_buckets__12589);
    DeRef(_current_bucket__12590);
    DeRefDS(_results__12591);
    DeRef(_temp_map__12593);
    _7129 = NOVALUE;
    return _7137;
    goto LA; // [200] 210
L9: 

    /** 		return results_*/
    DeRef(_the_map_p_12587);
    DeRef(_buckets__12589);
    DeRef(_current_bucket__12590);
    DeRef(_temp_map__12593);
    _7129 = NOVALUE;
    DeRef(_7137);
    _7137 = NOVALUE;
    return _results__12591;
LA: 
    ;
}


int _29values(int _the_map_12630, int _keys_12631, int _default_values_12632)
{
    int _buckets__12656 = NOVALUE;
    int _bucket__12657 = NOVALUE;
    int _results__12658 = NOVALUE;
    int _pos__12659 = NOVALUE;
    int _temp_map__12660 = NOVALUE;
    int _7177 = NOVALUE;
    int _7176 = NOVALUE;
    int _7174 = NOVALUE;
    int _7173 = NOVALUE;
    int _7172 = NOVALUE;
    int _7171 = NOVALUE;
    int _7169 = NOVALUE;
    int _7168 = NOVALUE;
    int _7167 = NOVALUE;
    int _7166 = NOVALUE;
    int _7164 = NOVALUE;
    int _7162 = NOVALUE;
    int _7159 = NOVALUE;
    int _7157 = NOVALUE;
    int _7155 = NOVALUE;
    int _7154 = NOVALUE;
    int _7153 = NOVALUE;
    int _7152 = NOVALUE;
    int _7150 = NOVALUE;
    int _7149 = NOVALUE;
    int _7148 = NOVALUE;
    int _7147 = NOVALUE;
    int _7146 = NOVALUE;
    int _7145 = NOVALUE;
    int _7143 = NOVALUE;
    int _7142 = NOVALUE;
    int _7140 = NOVALUE;
    int _7139 = NOVALUE;
    int _7138 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(keys) then*/
    _7138 = 0;
    if (_7138 == 0)
    {
        _7138 = NOVALUE;
        goto L1; // [6] 116
    }
    else{
        _7138 = NOVALUE;
    }

    /** 		if atom(default_values) then*/
    _7139 = 1;
    if (_7139 == 0)
    {
        _7139 = NOVALUE;
        goto L2; // [14] 29
    }
    else{
        _7139 = NOVALUE;
    }

    /** 			default_values = repeat(default_values, length(keys))*/
    _7140 = 1;
    _default_values_12632 = Repeat(0, 1);
    _7140 = NOVALUE;
    goto L3; // [26] 70
L2: 

    /** 		elsif length(default_values) < length(keys) then*/
    if (IS_SEQUENCE(_default_values_12632)){
            _7142 = SEQ_PTR(_default_values_12632)->length;
    }
    else {
        _7142 = 1;
    }
    if (IS_SEQUENCE(_keys_12631)){
            _7143 = SEQ_PTR(_keys_12631)->length;
    }
    else {
        _7143 = 1;
    }
    if (_7142 >= _7143)
    goto L4; // [37] 69

    /** 			default_values &= repeat(default_values[$], length(keys) - length(default_values))*/
    if (IS_SEQUENCE(_default_values_12632)){
            _7145 = SEQ_PTR(_default_values_12632)->length;
    }
    else {
        _7145 = 1;
    }
    _2 = (int)SEQ_PTR(_default_values_12632);
    _7146 = (int)*(((s1_ptr)_2)->base + _7145);
    if (IS_SEQUENCE(_keys_12631)){
            _7147 = SEQ_PTR(_keys_12631)->length;
    }
    else {
        _7147 = 1;
    }
    if (IS_SEQUENCE(_default_values_12632)){
            _7148 = SEQ_PTR(_default_values_12632)->length;
    }
    else {
        _7148 = 1;
    }
    _7149 = _7147 - _7148;
    _7147 = NOVALUE;
    _7148 = NOVALUE;
    _7150 = Repeat(_7146, _7149);
    _7146 = NOVALUE;
    _7149 = NOVALUE;
    if (IS_SEQUENCE(_default_values_12632) && IS_ATOM(_7150)) {
    }
    else if (IS_ATOM(_default_values_12632) && IS_SEQUENCE(_7150)) {
        Ref(_default_values_12632);
        Prepend(&_default_values_12632, _7150, _default_values_12632);
    }
    else {
        Concat((object_ptr)&_default_values_12632, _default_values_12632, _7150);
    }
    DeRefDS(_7150);
    _7150 = NOVALUE;
L4: 
L3: 

    /** 		for i = 1 to length(keys) do*/
    if (IS_SEQUENCE(_keys_12631)){
            _7152 = SEQ_PTR(_keys_12631)->length;
    }
    else {
        _7152 = 1;
    }
    {
        int _i_12651;
        _i_12651 = 1;
L5: 
        if (_i_12651 > _7152){
            goto L6; // [75] 109
        }

        /** 			keys[i] = get(the_map, keys[i], default_values[i])*/
        _2 = (int)SEQ_PTR(_keys_12631);
        _7153 = (int)*(((s1_ptr)_2)->base + _i_12651);
        _2 = (int)SEQ_PTR(_default_values_12632);
        _7154 = (int)*(((s1_ptr)_2)->base + _i_12651);
        Ref(_7153);
        Ref(_7154);
        _7155 = _29get(_the_map_12630, _7153, _7154);
        _7153 = NOVALUE;
        _7154 = NOVALUE;
        _2 = (int)SEQ_PTR(_keys_12631);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _keys_12631 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_12651);
        _1 = *(int *)_2;
        *(int *)_2 = _7155;
        if( _1 != _7155 ){
            DeRef(_1);
        }
        _7155 = NOVALUE;

        /** 		end for*/
        _i_12651 = _i_12651 + 1;
        goto L5; // [104] 82
L6: 
        ;
    }

    /** 		return keys*/
    DeRef(_default_values_12632);
    DeRef(_buckets__12656);
    DeRef(_bucket__12657);
    DeRef(_results__12658);
    DeRef(_temp_map__12660);
    return _keys_12631;
L1: 

    /** 	sequence buckets_*/

    /** 	sequence bucket_*/

    /** 	sequence results_*/

    /** 	integer pos_*/

    /** 	sequence temp_map_*/

    /** 	temp_map_ = eumem:ram_space[the_map]*/
    DeRef(_temp_map__12660);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _temp_map__12660 = (int)*(((s1_ptr)_2)->base + _the_map_12630);
    Ref(_temp_map__12660);

    /** 	results_ = repeat(0, temp_map_[ELEMENT_COUNT])*/
    _2 = (int)SEQ_PTR(_temp_map__12660);
    _7157 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12658);
    _results__12658 = Repeat(0, _7157);
    _7157 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12659 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12660);
    _7159 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7159, 76)){
        _7159 = NOVALUE;
        goto L7; // [161] 242
    }
    _7159 = NOVALUE;

    /** 		buckets_ = temp_map_[VALUE_BUCKETS]*/
    DeRef(_buckets__12656);
    _2 = (int)SEQ_PTR(_temp_map__12660);
    _buckets__12656 = (int)*(((s1_ptr)_2)->base + 6);
    Ref(_buckets__12656);

    /** 		for index = 1 to length(buckets_) do*/
    if (IS_SEQUENCE(_buckets__12656)){
            _7162 = SEQ_PTR(_buckets__12656)->length;
    }
    else {
        _7162 = 1;
    }
    {
        int _index_12669;
        _index_12669 = 1;
L8: 
        if (_index_12669 > _7162){
            goto L9; // [180] 239
        }

        /** 			bucket_ = buckets_[index]*/
        DeRef(_bucket__12657);
        _2 = (int)SEQ_PTR(_buckets__12656);
        _bucket__12657 = (int)*(((s1_ptr)_2)->base + _index_12669);
        Ref(_bucket__12657);

        /** 			if length(bucket_) > 0 then*/
        if (IS_SEQUENCE(_bucket__12657)){
                _7164 = SEQ_PTR(_bucket__12657)->length;
        }
        else {
            _7164 = 1;
        }
        if (_7164 <= 0)
        goto LA; // [200] 232

        /** 				results_[pos_ .. pos_ + length(bucket_) - 1] = bucket_*/
        if (IS_SEQUENCE(_bucket__12657)){
                _7166 = SEQ_PTR(_bucket__12657)->length;
        }
        else {
            _7166 = 1;
        }
        _7167 = _pos__12659 + _7166;
        if ((long)((unsigned long)_7167 + (unsigned long)HIGH_BITS) >= 0) 
        _7167 = NewDouble((double)_7167);
        _7166 = NOVALUE;
        if (IS_ATOM_INT(_7167)) {
            _7168 = _7167 - 1;
        }
        else {
            _7168 = NewDouble(DBL_PTR(_7167)->dbl - (double)1);
        }
        DeRef(_7167);
        _7167 = NOVALUE;
        assign_slice_seq = (s1_ptr *)&_results__12658;
        AssignSlice(_pos__12659, _7168, _bucket__12657);
        DeRef(_7168);
        _7168 = NOVALUE;

        /** 				pos_ += length(bucket_)*/
        if (IS_SEQUENCE(_bucket__12657)){
                _7169 = SEQ_PTR(_bucket__12657)->length;
        }
        else {
            _7169 = 1;
        }
        _pos__12659 = _pos__12659 + _7169;
        _7169 = NOVALUE;
LA: 

        /** 		end for*/
        _index_12669 = _index_12669 + 1;
        goto L8; // [234] 187
L9: 
        ;
    }
    goto LB; // [239] 307
L7: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12660);
    _7171 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7171)){
            _7172 = SEQ_PTR(_7171)->length;
    }
    else {
        _7172 = 1;
    }
    _7171 = NOVALUE;
    {
        int _index_12682;
        _index_12682 = 1;
LC: 
        if (_index_12682 > _7172){
            goto LD; // [253] 306
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12660);
        _7173 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7173);
        _7174 = (int)*(((s1_ptr)_2)->base + _index_12682);
        _7173 = NOVALUE;
        if (binary_op_a(EQUALS, _7174, 0)){
            _7174 = NOVALUE;
            goto LE; // [272] 299
        }
        _7174 = NOVALUE;

        /** 				results_[pos_] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12660);
        _7176 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7176);
        _7177 = (int)*(((s1_ptr)_2)->base + _index_12682);
        _7176 = NOVALUE;
        Ref(_7177);
        _2 = (int)SEQ_PTR(_results__12658);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12658 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _pos__12659);
        _1 = *(int *)_2;
        *(int *)_2 = _7177;
        if( _1 != _7177 ){
            DeRef(_1);
        }
        _7177 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12659 = _pos__12659 + 1;
LE: 

        /** 		end for*/
        _index_12682 = _index_12682 + 1;
        goto LC; // [301] 260
LD: 
        ;
    }
LB: 

    /** 	return results_*/
    DeRef(_keys_12631);
    DeRef(_default_values_12632);
    DeRef(_buckets__12656);
    DeRef(_bucket__12657);
    DeRef(_temp_map__12660);
    _7171 = NOVALUE;
    return _results__12658;
    ;
}


int _29pairs(int _the_map_p_12694, int _sorted_result_12695)
{
    int _key_bucket__12696 = NOVALUE;
    int _value_bucket__12697 = NOVALUE;
    int _results__12698 = NOVALUE;
    int _pos__12699 = NOVALUE;
    int _temp_map__12700 = NOVALUE;
    int _7213 = NOVALUE;
    int _7211 = NOVALUE;
    int _7210 = NOVALUE;
    int _7208 = NOVALUE;
    int _7207 = NOVALUE;
    int _7206 = NOVALUE;
    int _7204 = NOVALUE;
    int _7202 = NOVALUE;
    int _7201 = NOVALUE;
    int _7200 = NOVALUE;
    int _7199 = NOVALUE;
    int _7197 = NOVALUE;
    int _7195 = NOVALUE;
    int _7194 = NOVALUE;
    int _7192 = NOVALUE;
    int _7191 = NOVALUE;
    int _7189 = NOVALUE;
    int _7187 = NOVALUE;
    int _7186 = NOVALUE;
    int _7185 = NOVALUE;
    int _7183 = NOVALUE;
    int _7181 = NOVALUE;
    int _7180 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	temp_map_ = eumem:ram_space[the_map_p]*/
    DeRef(_temp_map__12700);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_the_map_p_12694)){
        _temp_map__12700 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_the_map_p_12694)->dbl));
    }
    else{
        _temp_map__12700 = (int)*(((s1_ptr)_2)->base + _the_map_p_12694);
    }
    Ref(_temp_map__12700);

    /** 	results_ = repeat({ 0, 0 }, temp_map_[ELEMENT_COUNT])*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = 0;
    _7180 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_temp_map__12700);
    _7181 = (int)*(((s1_ptr)_2)->base + 2);
    DeRef(_results__12698);
    _results__12698 = Repeat(_7180, _7181);
    DeRefDS(_7180);
    _7180 = NOVALUE;
    _7181 = NOVALUE;

    /** 	pos_ = 1*/
    _pos__12699 = 1;

    /** 	if temp_map_[MAP_TYPE] = LARGEMAP then*/
    _2 = (int)SEQ_PTR(_temp_map__12700);
    _7183 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _7183, 76)){
        _7183 = NOVALUE;
        goto L1; // [42] 157
    }
    _7183 = NOVALUE;

    /** 		for index = 1 to length(temp_map_[KEY_BUCKETS]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12700);
    _7185 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_SEQUENCE(_7185)){
            _7186 = SEQ_PTR(_7185)->length;
    }
    else {
        _7186 = 1;
    }
    _7185 = NOVALUE;
    {
        int _index_12709;
        _index_12709 = 1;
L2: 
        if (_index_12709 > _7186){
            goto L3; // [57] 154
        }

        /** 			key_bucket_ = temp_map_[KEY_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12700);
        _7187 = (int)*(((s1_ptr)_2)->base + 5);
        DeRef(_key_bucket__12696);
        _2 = (int)SEQ_PTR(_7187);
        _key_bucket__12696 = (int)*(((s1_ptr)_2)->base + _index_12709);
        Ref(_key_bucket__12696);
        _7187 = NOVALUE;

        /** 			value_bucket_ = temp_map_[VALUE_BUCKETS][index]*/
        _2 = (int)SEQ_PTR(_temp_map__12700);
        _7189 = (int)*(((s1_ptr)_2)->base + 6);
        DeRef(_value_bucket__12697);
        _2 = (int)SEQ_PTR(_7189);
        _value_bucket__12697 = (int)*(((s1_ptr)_2)->base + _index_12709);
        Ref(_value_bucket__12697);
        _7189 = NOVALUE;

        /** 			for j = 1 to length(key_bucket_) do*/
        if (IS_SEQUENCE(_key_bucket__12696)){
                _7191 = SEQ_PTR(_key_bucket__12696)->length;
        }
        else {
            _7191 = 1;
        }
        {
            int _j_12717;
            _j_12717 = 1;
L4: 
            if (_j_12717 > _7191){
                goto L5; // [97] 147
            }

            /** 				results_[pos_][1] = key_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__12698);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__12698 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__12699 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_key_bucket__12696);
            _7194 = (int)*(((s1_ptr)_2)->base + _j_12717);
            Ref(_7194);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 1);
            _1 = *(int *)_2;
            *(int *)_2 = _7194;
            if( _1 != _7194 ){
                DeRef(_1);
            }
            _7194 = NOVALUE;
            _7192 = NOVALUE;

            /** 				results_[pos_][2] = value_bucket_[j]*/
            _2 = (int)SEQ_PTR(_results__12698);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _results__12698 = MAKE_SEQ(_2);
            }
            _3 = (int)(_pos__12699 + ((s1_ptr)_2)->base);
            _2 = (int)SEQ_PTR(_value_bucket__12697);
            _7197 = (int)*(((s1_ptr)_2)->base + _j_12717);
            Ref(_7197);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + 2);
            _1 = *(int *)_2;
            *(int *)_2 = _7197;
            if( _1 != _7197 ){
                DeRef(_1);
            }
            _7197 = NOVALUE;
            _7195 = NOVALUE;

            /** 				pos_ += 1*/
            _pos__12699 = _pos__12699 + 1;

            /** 			end for*/
            _j_12717 = _j_12717 + 1;
            goto L4; // [142] 104
L5: 
            ;
        }

        /** 		end for*/
        _index_12709 = _index_12709 + 1;
        goto L2; // [149] 64
L3: 
        ;
    }
    goto L6; // [154] 248
L1: 

    /** 		for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__12700);
    _7199 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7199)){
            _7200 = SEQ_PTR(_7199)->length;
    }
    else {
        _7200 = 1;
    }
    _7199 = NOVALUE;
    {
        int _index_12728;
        _index_12728 = 1;
L7: 
        if (_index_12728 > _7200){
            goto L8; // [168] 247
        }

        /** 			if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__12700);
        _7201 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7201);
        _7202 = (int)*(((s1_ptr)_2)->base + _index_12728);
        _7201 = NOVALUE;
        if (binary_op_a(EQUALS, _7202, 0)){
            _7202 = NOVALUE;
            goto L9; // [187] 240
        }
        _7202 = NOVALUE;

        /** 				results_[pos_][1] = temp_map_[KEY_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__12698);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12698 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__12699 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__12700);
        _7206 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7206);
        _7207 = (int)*(((s1_ptr)_2)->base + _index_12728);
        _7206 = NOVALUE;
        Ref(_7207);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _7207;
        if( _1 != _7207 ){
            DeRef(_1);
        }
        _7207 = NOVALUE;
        _7204 = NOVALUE;

        /** 				results_[pos_][2] = temp_map_[VALUE_LIST][index]*/
        _2 = (int)SEQ_PTR(_results__12698);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _results__12698 = MAKE_SEQ(_2);
        }
        _3 = (int)(_pos__12699 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_temp_map__12700);
        _7210 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7210);
        _7211 = (int)*(((s1_ptr)_2)->base + _index_12728);
        _7210 = NOVALUE;
        Ref(_7211);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 2);
        _1 = *(int *)_2;
        *(int *)_2 = _7211;
        if( _1 != _7211 ){
            DeRef(_1);
        }
        _7211 = NOVALUE;
        _7208 = NOVALUE;

        /** 				pos_ += 1*/
        _pos__12699 = _pos__12699 + 1;
L9: 

        /** 		end for	*/
        _index_12728 = _index_12728 + 1;
        goto L7; // [242] 175
L8: 
        ;
    }
L6: 

    /** 	if sorted_result then*/
    if (_sorted_result_12695 == 0)
    {
        goto LA; // [250] 267
    }
    else{
    }

    /** 		return stdsort:sort(results_)*/
    RefDS(_results__12698);
    _7213 = _22sort(_results__12698, 1);
    DeRef(_the_map_p_12694);
    DeRef(_key_bucket__12696);
    DeRef(_value_bucket__12697);
    DeRefDS(_results__12698);
    DeRef(_temp_map__12700);
    _7185 = NOVALUE;
    _7199 = NOVALUE;
    return _7213;
    goto LB; // [264] 274
LA: 

    /** 		return results_*/
    DeRef(_the_map_p_12694);
    DeRef(_key_bucket__12696);
    DeRef(_value_bucket__12697);
    DeRef(_temp_map__12700);
    _7185 = NOVALUE;
    _7199 = NOVALUE;
    DeRef(_7213);
    _7213 = NOVALUE;
    return _results__12698;
LB: 
    ;
}


void _29convert_to_large_map(int _the_map__13171)
{
    int _temp_map__13172 = NOVALUE;
    int _map_handle__13173 = NOVALUE;
    int _7488 = NOVALUE;
    int _7487 = NOVALUE;
    int _7486 = NOVALUE;
    int _7485 = NOVALUE;
    int _7484 = NOVALUE;
    int _7482 = NOVALUE;
    int _7481 = NOVALUE;
    int _7480 = NOVALUE;
    int _7479 = NOVALUE;
    int _0, _1, _2;
    

    /** 	temp_map_ = eumem:ram_space[the_map_]*/
    DeRef(_temp_map__13172);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    _temp_map__13172 = (int)*(((s1_ptr)_2)->base + _the_map__13171);
    Ref(_temp_map__13172);

    /** 	map_handle_ = new()*/
    _0 = _map_handle__13173;
    _map_handle__13173 = _29new(690);
    DeRef(_0);

    /** 	for index = 1 to length(temp_map_[FREE_LIST]) do*/
    _2 = (int)SEQ_PTR(_temp_map__13172);
    _7479 = (int)*(((s1_ptr)_2)->base + 7);
    if (IS_SEQUENCE(_7479)){
            _7480 = SEQ_PTR(_7479)->length;
    }
    else {
        _7480 = 1;
    }
    _7479 = NOVALUE;
    {
        int _index_13177;
        _index_13177 = 1;
L1: 
        if (_index_13177 > _7480){
            goto L2; // [30] 94
        }

        /** 		if temp_map_[FREE_LIST][index] !=  0 then*/
        _2 = (int)SEQ_PTR(_temp_map__13172);
        _7481 = (int)*(((s1_ptr)_2)->base + 7);
        _2 = (int)SEQ_PTR(_7481);
        _7482 = (int)*(((s1_ptr)_2)->base + _index_13177);
        _7481 = NOVALUE;
        if (binary_op_a(EQUALS, _7482, 0)){
            _7482 = NOVALUE;
            goto L3; // [49] 87
        }
        _7482 = NOVALUE;

        /** 			put(map_handle_, temp_map_[KEY_LIST][index], temp_map_[VALUE_LIST][index])*/
        _2 = (int)SEQ_PTR(_temp_map__13172);
        _7484 = (int)*(((s1_ptr)_2)->base + 5);
        _2 = (int)SEQ_PTR(_7484);
        _7485 = (int)*(((s1_ptr)_2)->base + _index_13177);
        _7484 = NOVALUE;
        _2 = (int)SEQ_PTR(_temp_map__13172);
        _7486 = (int)*(((s1_ptr)_2)->base + 6);
        _2 = (int)SEQ_PTR(_7486);
        _7487 = (int)*(((s1_ptr)_2)->base + _index_13177);
        _7486 = NOVALUE;
        Ref(_map_handle__13173);
        Ref(_7485);
        Ref(_7487);
        _29put(_map_handle__13173, _7485, _7487, 1, 23);
        _7485 = NOVALUE;
        _7487 = NOVALUE;
L3: 

        /** 	end for*/
        _index_13177 = _index_13177 + 1;
        goto L1; // [89] 37
L2: 
        ;
    }

    /** 	eumem:ram_space[the_map_] = eumem:ram_space[map_handle_]*/
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!IS_ATOM_INT(_map_handle__13173)){
        _7488 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_map_handle__13173)->dbl));
    }
    else{
        _7488 = (int)*(((s1_ptr)_2)->base + _map_handle__13173);
    }
    Ref(_7488);
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__13171);
    _1 = *(int *)_2;
    *(int *)_2 = _7488;
    if( _1 != _7488 ){
        DeRef(_1);
    }
    _7488 = NOVALUE;

    /** end procedure*/
    DeRef(_temp_map__13172);
    DeRef(_map_handle__13173);
    _7479 = NOVALUE;
    return;
    ;
}


void _29convert_to_small_map(int _the_map__13191)
{
    int _keys__13192 = NOVALUE;
    int _values__13193 = NOVALUE;
    int _7497 = NOVALUE;
    int _7496 = NOVALUE;
    int _7495 = NOVALUE;
    int _7494 = NOVALUE;
    int _7493 = NOVALUE;
    int _7492 = NOVALUE;
    int _7491 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_the_map__13191)) {
        _1 = (long)(DBL_PTR(_the_map__13191)->dbl);
        if (UNIQUE(DBL_PTR(_the_map__13191)) && (DBL_PTR(_the_map__13191)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_the_map__13191);
        _the_map__13191 = _1;
    }

    /** 	keys_ = keys(the_map_)*/
    _0 = _keys__13192;
    _keys__13192 = _29keys(_the_map__13191, 0);
    DeRef(_0);

    /** 	values_ = values(the_map_)*/
    _0 = _values__13193;
    _values__13193 = _29values(_the_map__13191, 0, 0);
    DeRef(_0);

    /** 	eumem:ram_space[the_map_] = {*/
    _7491 = Repeat(_29init_small_map_key_11813, 23);
    _7492 = Repeat(0, 23);
    _7493 = Repeat(0, 23);
    _1 = NewS1(7);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_29type_is_map_11790);
    *((int *)(_2+4)) = _29type_is_map_11790;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 115;
    *((int *)(_2+20)) = _7491;
    *((int *)(_2+24)) = _7492;
    *((int *)(_2+28)) = _7493;
    _7494 = MAKE_SEQ(_1);
    _7493 = NOVALUE;
    _7492 = NOVALUE;
    _7491 = NOVALUE;
    _2 = (int)SEQ_PTR(_30ram_space_11048);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _30ram_space_11048 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _the_map__13191);
    _1 = *(int *)_2;
    *(int *)_2 = _7494;
    if( _1 != _7494 ){
        DeRef(_1);
    }
    _7494 = NOVALUE;

    /** 	for i = 1 to length(keys_) do*/
    if (IS_SEQUENCE(_keys__13192)){
            _7495 = SEQ_PTR(_keys__13192)->length;
    }
    else {
        _7495 = 1;
    }
    {
        int _i_13201;
        _i_13201 = 1;
L1: 
        if (_i_13201 > _7495){
            goto L2; // [63] 96
        }

        /** 		put(the_map_, keys_[i], values_[i], PUT, 0)*/
        _2 = (int)SEQ_PTR(_keys__13192);
        _7496 = (int)*(((s1_ptr)_2)->base + _i_13201);
        _2 = (int)SEQ_PTR(_values__13193);
        _7497 = (int)*(((s1_ptr)_2)->base + _i_13201);
        Ref(_7496);
        Ref(_7497);
        _29put(_the_map__13191, _7496, _7497, 1, 0);
        _7496 = NOVALUE;
        _7497 = NOVALUE;

        /** 	end for*/
        _i_13201 = _i_13201 + 1;
        goto L1; // [91] 70
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_keys__13192);
    DeRef(_values__13193);
    return;
    ;
}



// 0x20798497
