// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _14isLeap(int _year_3321)
{
    int _ly_3322 = NOVALUE;
    int _1589 = NOVALUE;
    int _1588 = NOVALUE;
    int _1586 = NOVALUE;
    int _1585 = NOVALUE;
    int _1584 = NOVALUE;
    int _1583 = NOVALUE;
    int _1582 = NOVALUE;
    int _1581 = NOVALUE;
    int _1580 = NOVALUE;
    int _1577 = NOVALUE;
    int _1575 = NOVALUE;
    int _1574 = NOVALUE;
    int _0, _1, _2;
    

    /** 		ly = (remainder(year, {4, 100, 400, 3200, 80000})=0)*/
    _1 = NewS1(5);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 4;
    *((int *)(_2+8)) = 100;
    *((int *)(_2+12)) = 400;
    *((int *)(_2+16)) = 3200;
    *((int *)(_2+20)) = 80000;
    _1574 = MAKE_SEQ(_1);
    _1575 = binary_op(REMAINDER, _year_3321, _1574);
    DeRefDS(_1574);
    _1574 = NOVALUE;
    DeRefi(_ly_3322);
    _ly_3322 = binary_op(EQUALS, _1575, 0);
    DeRefDS(_1575);
    _1575 = NOVALUE;

    /** 		if not ly[1] then return 0 end if*/
    _2 = (int)SEQ_PTR(_ly_3322);
    _1577 = (int)*(((s1_ptr)_2)->base + 1);
    if (_1577 != 0)
    goto L1; // [29] 37
    _1577 = NOVALUE;
    DeRefDSi(_ly_3322);
    return 0;
L1: 

    /** 		if year <= Gregorian_Reformation then*/
    if (_year_3321 > 1752)
    goto L2; // [39] 52

    /** 				return 1 -- ly[1] can't possibly be 0 here so set shortcut as '1'.*/
    DeRefi(_ly_3322);
    return 1;
    goto L3; // [49] 95
L2: 

    /** 				return ly[1] - ly[2] + ly[3] - ly[4] + ly[5]*/
    _2 = (int)SEQ_PTR(_ly_3322);
    _1580 = (int)*(((s1_ptr)_2)->base + 1);
    _2 = (int)SEQ_PTR(_ly_3322);
    _1581 = (int)*(((s1_ptr)_2)->base + 2);
    _1582 = _1580 - _1581;
    if ((long)((unsigned long)_1582 +(unsigned long) HIGH_BITS) >= 0){
        _1582 = NewDouble((double)_1582);
    }
    _1580 = NOVALUE;
    _1581 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_3322);
    _1583 = (int)*(((s1_ptr)_2)->base + 3);
    if (IS_ATOM_INT(_1582)) {
        _1584 = _1582 + _1583;
        if ((long)((unsigned long)_1584 + (unsigned long)HIGH_BITS) >= 0) 
        _1584 = NewDouble((double)_1584);
    }
    else {
        _1584 = NewDouble(DBL_PTR(_1582)->dbl + (double)_1583);
    }
    DeRef(_1582);
    _1582 = NOVALUE;
    _1583 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_3322);
    _1585 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1584)) {
        _1586 = _1584 - _1585;
        if ((long)((unsigned long)_1586 +(unsigned long) HIGH_BITS) >= 0){
            _1586 = NewDouble((double)_1586);
        }
    }
    else {
        _1586 = NewDouble(DBL_PTR(_1584)->dbl - (double)_1585);
    }
    DeRef(_1584);
    _1584 = NOVALUE;
    _1585 = NOVALUE;
    _2 = (int)SEQ_PTR(_ly_3322);
    _1588 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1586)) {
        _1589 = _1586 + _1588;
        if ((long)((unsigned long)_1589 + (unsigned long)HIGH_BITS) >= 0) 
        _1589 = NewDouble((double)_1589);
    }
    else {
        _1589 = NewDouble(DBL_PTR(_1586)->dbl + (double)_1588);
    }
    DeRef(_1586);
    _1586 = NOVALUE;
    _1588 = NOVALUE;
    DeRefDSi(_ly_3322);
    return _1589;
L3: 
    ;
}


int _14daysInMonth(int _year_3347, int _month_3348)
{
    int _1597 = NOVALUE;
    int _1596 = NOVALUE;
    int _1595 = NOVALUE;
    int _1594 = NOVALUE;
    int _1592 = NOVALUE;
    int _1591 = NOVALUE;
    int _1590 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_month_3348)) {
        _1 = (long)(DBL_PTR(_month_3348)->dbl);
        if (UNIQUE(DBL_PTR(_month_3348)) && (DBL_PTR(_month_3348)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_3348);
        _month_3348 = _1;
    }

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1590 = (_year_3347 == 1752);
    if (_1590 == 0) {
        goto L1; // [11] 32
    }
    _1592 = (_month_3348 == 9);
    if (_1592 == 0)
    {
        DeRef(_1592);
        _1592 = NOVALUE;
        goto L1; // [20] 32
    }
    else{
        DeRef(_1592);
        _1592 = NOVALUE;
    }

    /** 		return 19*/
    DeRef(_1590);
    _1590 = NOVALUE;
    return 19;
    goto L2; // [29] 70
L1: 

    /** 	elsif month != 2 then*/
    if (_month_3348 == 2)
    goto L3; // [34] 51

    /** 		return DaysPerMonth[month]*/
    _2 = (int)SEQ_PTR(_14DaysPerMonth_3303);
    _1594 = (int)*(((s1_ptr)_2)->base + _month_3348);
    Ref(_1594);
    DeRef(_1590);
    _1590 = NOVALUE;
    return _1594;
    goto L2; // [48] 70
L3: 

    /** 		return DaysPerMonth[month] + isLeap(year)*/
    _2 = (int)SEQ_PTR(_14DaysPerMonth_3303);
    _1595 = (int)*(((s1_ptr)_2)->base + _month_3348);
    _1596 = _14isLeap(_year_3347);
    if (IS_ATOM_INT(_1595) && IS_ATOM_INT(_1596)) {
        _1597 = _1595 + _1596;
        if ((long)((unsigned long)_1597 + (unsigned long)HIGH_BITS) >= 0) 
        _1597 = NewDouble((double)_1597);
    }
    else {
        _1597 = binary_op(PLUS, _1595, _1596);
    }
    _1595 = NOVALUE;
    DeRef(_1596);
    _1596 = NOVALUE;
    DeRef(_1590);
    _1590 = NOVALUE;
    _1594 = NOVALUE;
    return _1597;
L2: 
    ;
}


int _14julianDayOfYear(int _ymd_3371)
{
    int _year_3372 = NOVALUE;
    int _month_3373 = NOVALUE;
    int _day_3374 = NOVALUE;
    int _d_3375 = NOVALUE;
    int _1613 = NOVALUE;
    int _1612 = NOVALUE;
    int _1611 = NOVALUE;
    int _1608 = NOVALUE;
    int _1607 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_3371);
    _year_3372 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_3372)){
        _year_3372 = (long)DBL_PTR(_year_3372)->dbl;
    }

    /** 	month = ymd[2]*/
    _2 = (int)SEQ_PTR(_ymd_3371);
    _month_3373 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_month_3373)){
        _month_3373 = (long)DBL_PTR(_month_3373)->dbl;
    }

    /** 	day = ymd[3]*/
    _2 = (int)SEQ_PTR(_ymd_3371);
    _day_3374 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_day_3374)){
        _day_3374 = (long)DBL_PTR(_day_3374)->dbl;
    }

    /** 	if month = 1 then return day end if*/
    if (_month_3373 != 1)
    goto L1; // [27] 36
    DeRef(_ymd_3371);
    return _day_3374;
L1: 

    /** 	d = 0*/
    _d_3375 = 0;

    /** 	for i = 1 to month - 1 do*/
    _1607 = _month_3373 - 1;
    if ((long)((unsigned long)_1607 +(unsigned long) HIGH_BITS) >= 0){
        _1607 = NewDouble((double)_1607);
    }
    {
        int _i_3382;
        _i_3382 = 1;
L2: 
        if (binary_op_a(GREATER, _i_3382, _1607)){
            goto L3; // [47] 74
        }

        /** 		d += daysInMonth(year, i)*/
        Ref(_i_3382);
        _1608 = _14daysInMonth(_year_3372, _i_3382);
        if (IS_ATOM_INT(_1608)) {
            _d_3375 = _d_3375 + _1608;
        }
        else {
            _d_3375 = binary_op(PLUS, _d_3375, _1608);
        }
        DeRef(_1608);
        _1608 = NOVALUE;
        if (!IS_ATOM_INT(_d_3375)) {
            _1 = (long)(DBL_PTR(_d_3375)->dbl);
            if (UNIQUE(DBL_PTR(_d_3375)) && (DBL_PTR(_d_3375)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_d_3375);
            _d_3375 = _1;
        }

        /** 	end for*/
        _0 = _i_3382;
        if (IS_ATOM_INT(_i_3382)) {
            _i_3382 = _i_3382 + 1;
            if ((long)((unsigned long)_i_3382 +(unsigned long) HIGH_BITS) >= 0){
                _i_3382 = NewDouble((double)_i_3382);
            }
        }
        else {
            _i_3382 = binary_op_a(PLUS, _i_3382, 1);
        }
        DeRef(_0);
        goto L2; // [69] 54
L3: 
        ;
        DeRef(_i_3382);
    }

    /** 	d += day*/
    _d_3375 = _d_3375 + _day_3374;

    /** 	if year = Gregorian_Reformation and month = 9 then*/
    _1611 = (_year_3372 == 1752);
    if (_1611 == 0) {
        goto L4; // [86] 128
    }
    _1613 = (_month_3373 == 9);
    if (_1613 == 0)
    {
        DeRef(_1613);
        _1613 = NOVALUE;
        goto L4; // [95] 128
    }
    else{
        DeRef(_1613);
        _1613 = NOVALUE;
    }

    /** 		if day > 13 then*/
    if (_day_3374 <= 13)
    goto L5; // [100] 113

    /** 			d -= 11*/
    _d_3375 = _d_3375 - 11;
    goto L6; // [110] 127
L5: 

    /** 		elsif day > 2 then*/
    if (_day_3374 <= 2)
    goto L7; // [115] 126

    /** 			return 0*/
    DeRef(_ymd_3371);
    DeRef(_1607);
    _1607 = NOVALUE;
    DeRef(_1611);
    _1611 = NOVALUE;
    return 0;
L7: 
L6: 
L4: 

    /** 	return d*/
    DeRef(_ymd_3371);
    DeRef(_1607);
    _1607 = NOVALUE;
    DeRef(_1611);
    _1611 = NOVALUE;
    return _d_3375;
    ;
}


int _14julianDay(int _ymd_3398)
{
    int _year_3399 = NOVALUE;
    int _j_3400 = NOVALUE;
    int _greg00_3401 = NOVALUE;
    int _1642 = NOVALUE;
    int _1639 = NOVALUE;
    int _1636 = NOVALUE;
    int _1635 = NOVALUE;
    int _1634 = NOVALUE;
    int _1633 = NOVALUE;
    int _1632 = NOVALUE;
    int _1631 = NOVALUE;
    int _1630 = NOVALUE;
    int _1629 = NOVALUE;
    int _1627 = NOVALUE;
    int _1626 = NOVALUE;
    int _1625 = NOVALUE;
    int _1624 = NOVALUE;
    int _1623 = NOVALUE;
    int _1622 = NOVALUE;
    int _1621 = NOVALUE;
    int _0, _1, _2;
    

    /** 	year = ymd[1]*/
    _2 = (int)SEQ_PTR(_ymd_3398);
    _year_3399 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_year_3399)){
        _year_3399 = (long)DBL_PTR(_year_3399)->dbl;
    }

    /** 	j = julianDayOfYear(ymd)*/
    Ref(_ymd_3398);
    _j_3400 = _14julianDayOfYear(_ymd_3398);
    if (!IS_ATOM_INT(_j_3400)) {
        _1 = (long)(DBL_PTR(_j_3400)->dbl);
        if (UNIQUE(DBL_PTR(_j_3400)) && (DBL_PTR(_j_3400)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_3400);
        _j_3400 = _1;
    }

    /** 	year  -= 1*/
    _year_3399 = _year_3399 - 1;

    /** 	greg00 = year - Gregorian_Reformation00*/
    _greg00_3401 = _year_3399 - 1700;

    /** 	j += (*/
    if (_year_3399 <= INT15 && _year_3399 >= -INT15)
    _1621 = 365 * _year_3399;
    else
    _1621 = NewDouble(365 * (double)_year_3399);
    if (4 > 0 && _year_3399 >= 0) {
        _1622 = _year_3399 / 4;
    }
    else {
        temp_dbl = floor((double)_year_3399 / (double)4);
        _1622 = (long)temp_dbl;
    }
    if (IS_ATOM_INT(_1621)) {
        _1623 = _1621 + _1622;
        if ((long)((unsigned long)_1623 + (unsigned long)HIGH_BITS) >= 0) 
        _1623 = NewDouble((double)_1623);
    }
    else {
        _1623 = NewDouble(DBL_PTR(_1621)->dbl + (double)_1622);
    }
    DeRef(_1621);
    _1621 = NOVALUE;
    _1622 = NOVALUE;
    _1624 = (_greg00_3401 > 0);
    if (100 > 0 && _greg00_3401 >= 0) {
        _1625 = _greg00_3401 / 100;
    }
    else {
        temp_dbl = floor((double)_greg00_3401 / (double)100);
        _1625 = (long)temp_dbl;
    }
    _1626 = - _1625;
    _1627 = (_greg00_3401 % 400) ? NewDouble((double)_greg00_3401 / 400) : (_greg00_3401 / 400);
    if (IS_ATOM_INT(_1627)) {
        _1629 = NewDouble((double)_1627 + DBL_PTR(_1628)->dbl);
    }
    else {
        _1629 = NewDouble(DBL_PTR(_1627)->dbl + DBL_PTR(_1628)->dbl);
    }
    DeRef(_1627);
    _1627 = NOVALUE;
    _1630 = unary_op(FLOOR, _1629);
    DeRefDS(_1629);
    _1629 = NOVALUE;
    if (IS_ATOM_INT(_1630)) {
        _1631 = _1626 + _1630;
        if ((long)((unsigned long)_1631 + (unsigned long)HIGH_BITS) >= 0) 
        _1631 = NewDouble((double)_1631);
    }
    else {
        _1631 = binary_op(PLUS, _1626, _1630);
    }
    _1626 = NOVALUE;
    DeRef(_1630);
    _1630 = NOVALUE;
    if (IS_ATOM_INT(_1631)) {
        if (_1631 <= INT15 && _1631 >= -INT15)
        _1632 = _1624 * _1631;
        else
        _1632 = NewDouble(_1624 * (double)_1631);
    }
    else {
        _1632 = binary_op(MULTIPLY, _1624, _1631);
    }
    _1624 = NOVALUE;
    DeRef(_1631);
    _1631 = NOVALUE;
    if (IS_ATOM_INT(_1623) && IS_ATOM_INT(_1632)) {
        _1633 = _1623 + _1632;
        if ((long)((unsigned long)_1633 + (unsigned long)HIGH_BITS) >= 0) 
        _1633 = NewDouble((double)_1633);
    }
    else {
        _1633 = binary_op(PLUS, _1623, _1632);
    }
    DeRef(_1623);
    _1623 = NOVALUE;
    DeRef(_1632);
    _1632 = NOVALUE;
    _1634 = (_year_3399 >= 1752);
    _1635 = 11 * _1634;
    _1634 = NOVALUE;
    if (IS_ATOM_INT(_1633)) {
        _1636 = _1633 - _1635;
        if ((long)((unsigned long)_1636 +(unsigned long) HIGH_BITS) >= 0){
            _1636 = NewDouble((double)_1636);
        }
    }
    else {
        _1636 = binary_op(MINUS, _1633, _1635);
    }
    DeRef(_1633);
    _1633 = NOVALUE;
    _1635 = NOVALUE;
    if (IS_ATOM_INT(_1636)) {
        _j_3400 = _j_3400 + _1636;
    }
    else {
        _j_3400 = binary_op(PLUS, _j_3400, _1636);
    }
    DeRef(_1636);
    _1636 = NOVALUE;
    if (!IS_ATOM_INT(_j_3400)) {
        _1 = (long)(DBL_PTR(_j_3400)->dbl);
        if (UNIQUE(DBL_PTR(_j_3400)) && (DBL_PTR(_j_3400)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_j_3400);
        _j_3400 = _1;
    }

    /** 	if year >= 3200 then*/
    if (_year_3399 < 3200)
    goto L1; // [97] 133

    /** 		j -= floor(year/ 3200)*/
    if (3200 > 0 && _year_3399 >= 0) {
        _1639 = _year_3399 / 3200;
    }
    else {
        temp_dbl = floor((double)_year_3399 / (double)3200);
        _1639 = (long)temp_dbl;
    }
    _j_3400 = _j_3400 - _1639;
    _1639 = NOVALUE;

    /** 		if year >= 80000 then*/
    if (_year_3399 < 80000)
    goto L2; // [115] 132

    /** 			j += floor(year/80000)*/
    if (80000 > 0 && _year_3399 >= 0) {
        _1642 = _year_3399 / 80000;
    }
    else {
        temp_dbl = floor((double)_year_3399 / (double)80000);
        _1642 = (long)temp_dbl;
    }
    _j_3400 = _j_3400 + _1642;
    _1642 = NOVALUE;
L2: 
L1: 

    /** 	return j*/
    DeRef(_ymd_3398);
    DeRef(_1625);
    _1625 = NOVALUE;
    return _j_3400;
    ;
}


int _14datetimeToSeconds(int _dt_3488)
{
    int _1686 = NOVALUE;
    int _1685 = NOVALUE;
    int _1684 = NOVALUE;
    int _1683 = NOVALUE;
    int _1682 = NOVALUE;
    int _1681 = NOVALUE;
    int _1680 = NOVALUE;
    int _1679 = NOVALUE;
    int _1678 = NOVALUE;
    int _1677 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return julianDay(dt) * DayLengthInSeconds + (dt[4] * 60 + dt[5]) * 60 + dt[6]*/
    Ref(_dt_3488);
    _1677 = _14julianDay(_dt_3488);
    if (IS_ATOM_INT(_1677)) {
        _1678 = NewDouble(_1677 * (double)86400);
    }
    else {
        _1678 = binary_op(MULTIPLY, _1677, 86400);
    }
    DeRef(_1677);
    _1677 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3488);
    _1679 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_1679)) {
        if (_1679 == (short)_1679)
        _1680 = _1679 * 60;
        else
        _1680 = NewDouble(_1679 * (double)60);
    }
    else {
        _1680 = binary_op(MULTIPLY, _1679, 60);
    }
    _1679 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3488);
    _1681 = (int)*(((s1_ptr)_2)->base + 5);
    if (IS_ATOM_INT(_1680) && IS_ATOM_INT(_1681)) {
        _1682 = _1680 + _1681;
        if ((long)((unsigned long)_1682 + (unsigned long)HIGH_BITS) >= 0) 
        _1682 = NewDouble((double)_1682);
    }
    else {
        _1682 = binary_op(PLUS, _1680, _1681);
    }
    DeRef(_1680);
    _1680 = NOVALUE;
    _1681 = NOVALUE;
    if (IS_ATOM_INT(_1682)) {
        if (_1682 == (short)_1682)
        _1683 = _1682 * 60;
        else
        _1683 = NewDouble(_1682 * (double)60);
    }
    else {
        _1683 = binary_op(MULTIPLY, _1682, 60);
    }
    DeRef(_1682);
    _1682 = NOVALUE;
    if (IS_ATOM_INT(_1678) && IS_ATOM_INT(_1683)) {
        _1684 = _1678 + _1683;
        if ((long)((unsigned long)_1684 + (unsigned long)HIGH_BITS) >= 0) 
        _1684 = NewDouble((double)_1684);
    }
    else {
        _1684 = binary_op(PLUS, _1678, _1683);
    }
    DeRef(_1678);
    _1678 = NOVALUE;
    DeRef(_1683);
    _1683 = NOVALUE;
    _2 = (int)SEQ_PTR(_dt_3488);
    _1685 = (int)*(((s1_ptr)_2)->base + 6);
    if (IS_ATOM_INT(_1684) && IS_ATOM_INT(_1685)) {
        _1686 = _1684 + _1685;
        if ((long)((unsigned long)_1686 + (unsigned long)HIGH_BITS) >= 0) 
        _1686 = NewDouble((double)_1686);
    }
    else {
        _1686 = binary_op(PLUS, _1684, _1685);
    }
    DeRef(_1684);
    _1684 = NOVALUE;
    _1685 = NOVALUE;
    DeRef(_dt_3488);
    return _1686;
    ;
}


int _14from_date(int _src_3666)
{
    int _1813 = NOVALUE;
    int _1812 = NOVALUE;
    int _1811 = NOVALUE;
    int _1810 = NOVALUE;
    int _1809 = NOVALUE;
    int _1808 = NOVALUE;
    int _1807 = NOVALUE;
    int _1805 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return {src[YEAR]+1900, src[MONTH], src[DAY], src[HOUR], src[MINUTE], src[SECOND]}*/
    _2 = (int)SEQ_PTR(_src_3666);
    _1805 = (int)*(((s1_ptr)_2)->base + 1);
    _1807 = _1805 + 1900;
    if ((long)((unsigned long)_1807 + (unsigned long)HIGH_BITS) >= 0) 
    _1807 = NewDouble((double)_1807);
    _1805 = NOVALUE;
    _2 = (int)SEQ_PTR(_src_3666);
    _1808 = (int)*(((s1_ptr)_2)->base + 2);
    _2 = (int)SEQ_PTR(_src_3666);
    _1809 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_src_3666);
    _1810 = (int)*(((s1_ptr)_2)->base + 4);
    _2 = (int)SEQ_PTR(_src_3666);
    _1811 = (int)*(((s1_ptr)_2)->base + 5);
    _2 = (int)SEQ_PTR(_src_3666);
    _1812 = (int)*(((s1_ptr)_2)->base + 6);
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _1807;
    *((int *)(_2+8)) = _1808;
    *((int *)(_2+12)) = _1809;
    *((int *)(_2+16)) = _1810;
    *((int *)(_2+20)) = _1811;
    *((int *)(_2+24)) = _1812;
    _1813 = MAKE_SEQ(_1);
    _1812 = NOVALUE;
    _1811 = NOVALUE;
    _1810 = NOVALUE;
    _1809 = NOVALUE;
    _1808 = NOVALUE;
    _1807 = NOVALUE;
    DeRefDSi(_src_3666);
    return _1813;
    ;
}


int _14new(int _year_3696, int _month_3697, int _day_3698, int _hour_3699, int _minute_3700, int _second_3701)
{
    int _d_3702 = NOVALUE;
    int _now_1__tmp_at41_3709 = NOVALUE;
    int _now_inlined_now_at_41_3708 = NOVALUE;
    int _1829 = NOVALUE;
    int _1828 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_year_3696)) {
        _1 = (long)(DBL_PTR(_year_3696)->dbl);
        if (UNIQUE(DBL_PTR(_year_3696)) && (DBL_PTR(_year_3696)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_year_3696);
        _year_3696 = _1;
    }
    if (!IS_ATOM_INT(_month_3697)) {
        _1 = (long)(DBL_PTR(_month_3697)->dbl);
        if (UNIQUE(DBL_PTR(_month_3697)) && (DBL_PTR(_month_3697)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_month_3697);
        _month_3697 = _1;
    }
    if (!IS_ATOM_INT(_day_3698)) {
        _1 = (long)(DBL_PTR(_day_3698)->dbl);
        if (UNIQUE(DBL_PTR(_day_3698)) && (DBL_PTR(_day_3698)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_day_3698);
        _day_3698 = _1;
    }
    if (!IS_ATOM_INT(_hour_3699)) {
        _1 = (long)(DBL_PTR(_hour_3699)->dbl);
        if (UNIQUE(DBL_PTR(_hour_3699)) && (DBL_PTR(_hour_3699)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_hour_3699);
        _hour_3699 = _1;
    }
    if (!IS_ATOM_INT(_minute_3700)) {
        _1 = (long)(DBL_PTR(_minute_3700)->dbl);
        if (UNIQUE(DBL_PTR(_minute_3700)) && (DBL_PTR(_minute_3700)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_minute_3700);
        _minute_3700 = _1;
    }

    /** 	d = {year, month, day, hour, minute, second}*/
    _0 = _d_3702;
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _year_3696;
    *((int *)(_2+8)) = _month_3697;
    *((int *)(_2+12)) = _day_3698;
    *((int *)(_2+16)) = _hour_3699;
    *((int *)(_2+20)) = _minute_3700;
    Ref(_second_3701);
    *((int *)(_2+24)) = _second_3701;
    _d_3702 = MAKE_SEQ(_1);
    DeRef(_0);

    /** 	if equal(d, {0,0,0,0,0,0}) then*/
    _1 = NewS1(6);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 0;
    *((int *)(_2+16)) = 0;
    *((int *)(_2+20)) = 0;
    *((int *)(_2+24)) = 0;
    _1828 = MAKE_SEQ(_1);
    if (_d_3702 == _1828)
    _1829 = 1;
    else if (IS_ATOM_INT(_d_3702) && IS_ATOM_INT(_1828))
    _1829 = 0;
    else
    _1829 = (compare(_d_3702, _1828) == 0);
    DeRefDS(_1828);
    _1828 = NOVALUE;
    if (_1829 == 0)
    {
        _1829 = NOVALUE;
        goto L1; // [37] 60
    }
    else{
        _1829 = NOVALUE;
    }

    /** 		return now()*/

    /** 	return from_date(date())*/
    DeRefi(_now_1__tmp_at41_3709);
    _now_1__tmp_at41_3709 = Date();
    RefDS(_now_1__tmp_at41_3709);
    _0 = _now_inlined_now_at_41_3708;
    _now_inlined_now_at_41_3708 = _14from_date(_now_1__tmp_at41_3709);
    DeRef(_0);
    DeRefi(_now_1__tmp_at41_3709);
    _now_1__tmp_at41_3709 = NOVALUE;
    DeRef(_second_3701);
    DeRef(_d_3702);
    return _now_inlined_now_at_41_3708;
    goto L2; // [57] 67
L1: 

    /** 		return d*/
    DeRef(_second_3701);
    return _d_3702;
L2: 
    ;
}



// 0x4EE1B1B1
