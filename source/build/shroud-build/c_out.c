// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _56c_putc(int _c_46457)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_c_46457)) {
        _1 = (long)(DBL_PTR(_c_46457)->dbl);
        if (UNIQUE(DBL_PTR(_c_46457)) && (DBL_PTR(_c_46457)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_46457);
        _c_46457 = _1;
    }

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L1; // [7] 23
    }
    else{
    }

    /** 		puts(c_code, c)*/
    EPuts(_56c_code_46447, _c_46457); // DJP 

    /** 		update_checksum( c )*/
    _57update_checksum(_c_46457);
L1: 

    /** end procedure*/
    return;
    ;
}


void _56c_hputs(int _c_source_46462)
{
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L1; // [7] 18
    }
    else{
    }

    /** 		puts(c_h, c_source)    */
    EPuts(_56c_h_46448, _c_source_46462); // DJP 
L1: 

    /** end procedure*/
    DeRefDS(_c_source_46462);
    return;
    ;
}


void _56c_puts(int _c_source_46466)
{
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L1; // [7] 23
    }
    else{
    }

    /** 		puts(c_code, c_source)*/
    EPuts(_56c_code_46447, _c_source_46466); // DJP 

    /** 		update_checksum( c_source )*/
    RefDS(_c_source_46466);
    _57update_checksum(_c_source_46466);
L1: 

    /** end procedure*/
    DeRefDS(_c_source_46466);
    return;
    ;
}


void _56c_hprintf(int _format_46471, int _value_46472)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_value_46472)) {
        _1 = (long)(DBL_PTR(_value_46472)->dbl);
        if (UNIQUE(DBL_PTR(_value_46472)) && (DBL_PTR(_value_46472)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_value_46472);
        _value_46472 = _1;
    }

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L1; // [9] 21
    }
    else{
    }

    /** 		printf(c_h, format, value)*/
    EPrintf(_56c_h_46448, _format_46471, _value_46472);
L1: 

    /** end procedure*/
    DeRefDS(_format_46471);
    return;
    ;
}


void _56c_printf(int _format_46476, int _value_46477)
{
    int _text_46479 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L1; // [7] 29
    }
    else{
    }

    /** 		sequence text = sprintf( format, value )*/
    DeRefi(_text_46479);
    _text_46479 = EPrintf(-9999999, _format_46476, _value_46477);

    /** 		puts(c_code, text)*/
    EPuts(_56c_code_46447, _text_46479); // DJP 

    /** 		update_checksum( text )*/
    RefDS(_text_46479);
    _57update_checksum(_text_46479);
L1: 
    DeRefi(_text_46479);
    _text_46479 = NOVALUE;

    /** end procedure*/
    DeRefDS(_format_46476);
    DeRef(_value_46477);
    return;
    ;
}


void _56c_printf8(int _value_46490)
{
    int _buff_46491 = NOVALUE;
    int _neg_46492 = NOVALUE;
    int _p_46493 = NOVALUE;
    int _24775 = NOVALUE;
    int _24774 = NOVALUE;
    int _24773 = NOVALUE;
    int _24771 = NOVALUE;
    int _24770 = NOVALUE;
    int _24768 = NOVALUE;
    int _24767 = NOVALUE;
    int _24765 = NOVALUE;
    int _24764 = NOVALUE;
    int _24762 = NOVALUE;
    int _24760 = NOVALUE;
    int _24758 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if emit_c_output then*/
    if (_56emit_c_output_46444 == 0)
    {
        goto L1; // [5] 217
    }
    else{
    }

    /** 		neg = 0*/
    _neg_46492 = 0;

    /** 		buff = sprintf("%.16e", value)*/
    DeRef(_buff_46491);
    _buff_46491 = EPrintf(-9999999, _24756, _value_46490);

    /** 		if length(buff) < 10 then*/
    if (IS_SEQUENCE(_buff_46491)){
            _24758 = SEQ_PTR(_buff_46491)->length;
    }
    else {
        _24758 = 1;
    }
    if (_24758 >= 10)
    goto L2; // [24] 209

    /** 			p = 1*/
    _p_46493 = 1;

    /** 			while p <= length(buff) do*/
L3: 
    if (IS_SEQUENCE(_buff_46491)){
            _24760 = SEQ_PTR(_buff_46491)->length;
    }
    else {
        _24760 = 1;
    }
    if (_p_46493 > _24760)
    goto L4; // [41] 208

    /** 				if buff[p] = '-' then*/
    _2 = (int)SEQ_PTR(_buff_46491);
    _24762 = (int)*(((s1_ptr)_2)->base + _p_46493);
    if (binary_op_a(NOTEQ, _24762, 45)){
        _24762 = NOVALUE;
        goto L5; // [51] 63
    }
    _24762 = NOVALUE;

    /** 					neg = 1*/
    _neg_46492 = 1;
    goto L6; // [60] 197
L5: 

    /** 				elsif buff[p] = 'i' or buff[p] = 'I' then*/
    _2 = (int)SEQ_PTR(_buff_46491);
    _24764 = (int)*(((s1_ptr)_2)->base + _p_46493);
    if (IS_ATOM_INT(_24764)) {
        _24765 = (_24764 == 105);
    }
    else {
        _24765 = binary_op(EQUALS, _24764, 105);
    }
    _24764 = NOVALUE;
    if (IS_ATOM_INT(_24765)) {
        if (_24765 != 0) {
            goto L7; // [73] 90
        }
    }
    else {
        if (DBL_PTR(_24765)->dbl != 0.0) {
            goto L7; // [73] 90
        }
    }
    _2 = (int)SEQ_PTR(_buff_46491);
    _24767 = (int)*(((s1_ptr)_2)->base + _p_46493);
    if (IS_ATOM_INT(_24767)) {
        _24768 = (_24767 == 73);
    }
    else {
        _24768 = binary_op(EQUALS, _24767, 73);
    }
    _24767 = NOVALUE;
    if (_24768 == 0) {
        DeRef(_24768);
        _24768 = NOVALUE;
        goto L8; // [86] 114
    }
    else {
        if (!IS_ATOM_INT(_24768) && DBL_PTR(_24768)->dbl == 0.0){
            DeRef(_24768);
            _24768 = NOVALUE;
            goto L8; // [86] 114
        }
        DeRef(_24768);
        _24768 = NOVALUE;
    }
    DeRef(_24768);
    _24768 = NOVALUE;
L7: 

    /** 					buff = CREATE_INF*/
    RefDS(_56CREATE_INF_46482);
    DeRef(_buff_46491);
    _buff_46491 = _56CREATE_INF_46482;

    /** 					if neg then*/
    if (_neg_46492 == 0)
    {
        goto L4; // [97] 208
    }
    else{
    }

    /** 						buff = prepend(buff, '-')*/
    Prepend(&_buff_46491, _buff_46491, 45);

    /** 					exit*/
    goto L4; // [109] 208
    goto L6; // [111] 197
L8: 

    /** 				elsif buff[p] = 'n' or buff[p] = 'N' then*/
    _2 = (int)SEQ_PTR(_buff_46491);
    _24770 = (int)*(((s1_ptr)_2)->base + _p_46493);
    if (IS_ATOM_INT(_24770)) {
        _24771 = (_24770 == 110);
    }
    else {
        _24771 = binary_op(EQUALS, _24770, 110);
    }
    _24770 = NOVALUE;
    if (IS_ATOM_INT(_24771)) {
        if (_24771 != 0) {
            goto L9; // [124] 141
        }
    }
    else {
        if (DBL_PTR(_24771)->dbl != 0.0) {
            goto L9; // [124] 141
        }
    }
    _2 = (int)SEQ_PTR(_buff_46491);
    _24773 = (int)*(((s1_ptr)_2)->base + _p_46493);
    if (IS_ATOM_INT(_24773)) {
        _24774 = (_24773 == 78);
    }
    else {
        _24774 = binary_op(EQUALS, _24773, 78);
    }
    _24773 = NOVALUE;
    if (_24774 == 0) {
        DeRef(_24774);
        _24774 = NOVALUE;
        goto LA; // [137] 196
    }
    else {
        if (!IS_ATOM_INT(_24774) && DBL_PTR(_24774)->dbl == 0.0){
            DeRef(_24774);
            _24774 = NOVALUE;
            goto LA; // [137] 196
        }
        DeRef(_24774);
        _24774 = NOVALUE;
    }
    DeRef(_24774);
    _24774 = NOVALUE;
L9: 

    /** 					ifdef UNIX then*/

    /** 						if sequence(wat_path) then*/
    _24775 = 0;
    if (_24775 == 0)
    {
        _24775 = NOVALUE;
        goto LB; // [150] 173
    }
    else{
        _24775 = NOVALUE;
    }

    /** 							buff = CREATE_NAN2*/
    RefDS(_56CREATE_NAN2_46486);
    DeRef(_buff_46491);
    _buff_46491 = _56CREATE_NAN2_46486;

    /** 							if not neg then*/
    if (_neg_46492 != 0)
    goto L4; // [160] 208

    /** 								buff = prepend(buff, '-')*/
    Prepend(&_buff_46491, _buff_46491, 45);
    goto L4; // [170] 208
LB: 

    /** 							buff = CREATE_NAN1*/
    RefDS(_56CREATE_NAN1_46484);
    DeRef(_buff_46491);
    _buff_46491 = _56CREATE_NAN1_46484;

    /** 							if neg then*/
    if (_neg_46492 == 0)
    {
        goto L4; // [180] 208
    }
    else{
    }

    /** 								buff = prepend(buff, '-')*/
    Prepend(&_buff_46491, _buff_46491, 45);

    /** 						exit*/
    goto L4; // [193] 208
LA: 
L6: 

    /** 				p += 1*/
    _p_46493 = _p_46493 + 1;

    /** 			end while*/
    goto L3; // [205] 38
L4: 
L2: 

    /** 		puts(c_code, buff)*/
    EPuts(_56c_code_46447, _buff_46491); // DJP 
L1: 

    /** end procedure*/
    DeRef(_value_46490);
    DeRef(_buff_46491);
    DeRef(_24765);
    _24765 = NOVALUE;
    DeRef(_24771);
    _24771 = NOVALUE;
    return;
    ;
}


void _56adjust_indent_before(int _stmt_46536)
{
    int _i_46537 = NOVALUE;
    int _lb_46539 = NOVALUE;
    int _rb_46540 = NOVALUE;
    int _24792 = NOVALUE;
    int _24790 = NOVALUE;
    int _24788 = NOVALUE;
    int _24782 = NOVALUE;
    int _24781 = NOVALUE;
    int _0, _1, _2;
    

    /** 	lb = FALSE*/
    _lb_46539 = _9FALSE_426;

    /** 	rb = FALSE*/
    _rb_46540 = _9FALSE_426;

    /** 	for p = 1 to length(stmt) do*/
    if (IS_SEQUENCE(_stmt_46536)){
            _24781 = SEQ_PTR(_stmt_46536)->length;
    }
    else {
        _24781 = 1;
    }
    {
        int _p_46544;
        _p_46544 = 1;
L1: 
        if (_p_46544 > _24781){
            goto L2; // [22] 102
        }

        /** 		switch stmt[p] do*/
        _2 = (int)SEQ_PTR(_stmt_46536);
        _24782 = (int)*(((s1_ptr)_2)->base + _p_46544);
        if (IS_SEQUENCE(_24782) ){
            goto L3; // [35] 95
        }
        if(!IS_ATOM_INT(_24782)){
            if( (DBL_PTR(_24782)->dbl != (double) ((int) DBL_PTR(_24782)->dbl) ) ){
                goto L3; // [35] 95
            }
            _0 = (int) DBL_PTR(_24782)->dbl;
        }
        else {
            _0 = _24782;
        };
        _24782 = NOVALUE;
        switch ( _0 ){ 

            /** 			case '\n' then*/
            case 10:

            /** 				exit*/
            goto L2; // [46] 102
            goto L3; // [48] 95

            /** 			case  '}' then*/
            case 125:

            /** 				rb = TRUE*/
            _rb_46540 = _9TRUE_428;

            /** 				if lb then*/
            if (_lb_46539 == 0)
            {
                goto L3; // [63] 95
            }
            else{
            }

            /** 					exit*/
            goto L2; // [68] 102
            goto L3; // [71] 95

            /** 			case '{' then*/
            case 123:

            /** 				lb = TRUE*/
            _lb_46539 = _9TRUE_428;

            /** 				if rb then */
            if (_rb_46540 == 0)
            {
                goto L4; // [86] 94
            }
            else{
            }

            /** 					exit*/
            goto L2; // [91] 102
L4: 
        ;}L3: 

        /** 	end for*/
        _p_46544 = _p_46544 + 1;
        goto L1; // [97] 29
L2: 
        ;
    }

    /** 	if rb then*/
    if (_rb_46540 == 0)
    {
        goto L5; // [104] 122
    }
    else{
    }

    /** 		if not lb then*/
    if (_lb_46539 != 0)
    goto L6; // [109] 121

    /** 			indent -= 4*/
    _56indent_46530 = _56indent_46530 - 4;
L6: 
L5: 

    /** 	i = indent + temp_indent*/
    _i_46537 = _56indent_46530 + _56temp_indent_46531;

    /** 	while i >= length(big_blanks) do*/
L7: 
    if (IS_SEQUENCE(_56big_blanks_46532)){
            _24788 = SEQ_PTR(_56big_blanks_46532)->length;
    }
    else {
        _24788 = 1;
    }
    if (_i_46537 < _24788)
    goto L8; // [140] 163

    /** 		c_puts(big_blanks)*/
    RefDS(_56big_blanks_46532);
    _56c_puts(_56big_blanks_46532);

    /** 		i -= length(big_blanks)*/
    if (IS_SEQUENCE(_56big_blanks_46532)){
            _24790 = SEQ_PTR(_56big_blanks_46532)->length;
    }
    else {
        _24790 = 1;
    }
    _i_46537 = _i_46537 - _24790;
    _24790 = NOVALUE;

    /** 	end while*/
    goto L7; // [160] 137
L8: 

    /** 	c_puts(big_blanks[1..i])*/
    rhs_slice_target = (object_ptr)&_24792;
    RHS_Slice(_56big_blanks_46532, 1, _i_46537);
    _56c_puts(_24792);
    _24792 = NOVALUE;

    /** 	temp_indent = 0    */
    _56temp_indent_46531 = 0;

    /** end procedure*/
    DeRefDS(_stmt_46536);
    return;
    ;
}


void _56adjust_indent_after(int _stmt_46569)
{
    int _24813 = NOVALUE;
    int _24812 = NOVALUE;
    int _24810 = NOVALUE;
    int _24808 = NOVALUE;
    int _24807 = NOVALUE;
    int _24804 = NOVALUE;
    int _24802 = NOVALUE;
    int _24801 = NOVALUE;
    int _24798 = NOVALUE;
    int _24794 = NOVALUE;
    int _24793 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for p = 1 to length(stmt) do*/
    if (IS_SEQUENCE(_stmt_46569)){
            _24793 = SEQ_PTR(_stmt_46569)->length;
    }
    else {
        _24793 = 1;
    }
    {
        int _p_46571;
        _p_46571 = 1;
L1: 
        if (_p_46571 > _24793){
            goto L2; // [8] 61
        }

        /** 		switch stmt[p] do*/
        _2 = (int)SEQ_PTR(_stmt_46569);
        _24794 = (int)*(((s1_ptr)_2)->base + _p_46571);
        if (IS_SEQUENCE(_24794) ){
            goto L3; // [21] 54
        }
        if(!IS_ATOM_INT(_24794)){
            if( (DBL_PTR(_24794)->dbl != (double) ((int) DBL_PTR(_24794)->dbl) ) ){
                goto L3; // [21] 54
            }
            _0 = (int) DBL_PTR(_24794)->dbl;
        }
        else {
            _0 = _24794;
        };
        _24794 = NOVALUE;
        switch ( _0 ){ 

            /** 			case '\n' then*/
            case 10:

            /** 				exit*/
            goto L2; // [32] 61
            goto L3; // [34] 54

            /** 			case '{' then*/
            case 123:

            /** 				indent += 4*/
            _56indent_46530 = _56indent_46530 + 4;

            /** 				return*/
            DeRefDS(_stmt_46569);
            return;
        ;}L3: 

        /** 	end for*/
        _p_46571 = _p_46571 + 1;
        goto L1; // [56] 15
L2: 
        ;
    }

    /** 	if length(stmt) < 3 then*/
    if (IS_SEQUENCE(_stmt_46569)){
            _24798 = SEQ_PTR(_stmt_46569)->length;
    }
    else {
        _24798 = 1;
    }
    if (_24798 >= 3)
    goto L4; // [66] 76

    /** 		return*/
    DeRefDS(_stmt_46569);
    return;
L4: 

    /** 	if not equal("if ", stmt[1..3]) then*/
    rhs_slice_target = (object_ptr)&_24801;
    RHS_Slice(_stmt_46569, 1, 3);
    if (_24800 == _24801)
    _24802 = 1;
    else if (IS_ATOM_INT(_24800) && IS_ATOM_INT(_24801))
    _24802 = 0;
    else
    _24802 = (compare(_24800, _24801) == 0);
    DeRefDS(_24801);
    _24801 = NOVALUE;
    if (_24802 != 0)
    goto L5; // [87] 96
    _24802 = NOVALUE;

    /** 		return*/
    DeRefDS(_stmt_46569);
    return;
L5: 

    /** 	if length(stmt) < 5 then*/
    if (IS_SEQUENCE(_stmt_46569)){
            _24804 = SEQ_PTR(_stmt_46569)->length;
    }
    else {
        _24804 = 1;
    }
    if (_24804 >= 5)
    goto L6; // [101] 111

    /** 		return*/
    DeRefDS(_stmt_46569);
    return;
L6: 

    /** 	if not equal("else", stmt[1..4]) then*/
    rhs_slice_target = (object_ptr)&_24807;
    RHS_Slice(_stmt_46569, 1, 4);
    if (_24806 == _24807)
    _24808 = 1;
    else if (IS_ATOM_INT(_24806) && IS_ATOM_INT(_24807))
    _24808 = 0;
    else
    _24808 = (compare(_24806, _24807) == 0);
    DeRefDS(_24807);
    _24807 = NOVALUE;
    if (_24808 != 0)
    goto L7; // [122] 131
    _24808 = NOVALUE;

    /** 		return*/
    DeRefDS(_stmt_46569);
    return;
L7: 

    /** 	if not find(stmt[5], {" \n"}) then*/
    _2 = (int)SEQ_PTR(_stmt_46569);
    _24810 = (int)*(((s1_ptr)_2)->base + 5);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_24811);
    *((int *)(_2+4)) = _24811;
    _24812 = MAKE_SEQ(_1);
    _24813 = find_from(_24810, _24812, 1);
    _24810 = NOVALUE;
    DeRefDS(_24812);
    _24812 = NOVALUE;
    if (_24813 != 0)
    goto L8; // [146] 155
    _24813 = NOVALUE;

    /** 		return*/
    DeRefDS(_stmt_46569);
    return;
L8: 

    /** 	temp_indent = 4*/
    _56temp_indent_46531 = 4;

    /** end procedure*/
    DeRefDS(_stmt_46569);
    return;
    ;
}



// 0x717C2AEF
