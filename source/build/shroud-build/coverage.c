// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _52check_coverage()
{
    int _25860 = NOVALUE;
    int _25859 = NOVALUE;
    int _25858 = NOVALUE;
    int _25857 = NOVALUE;
    int _25856 = NOVALUE;
    int _25855 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = length( file_coverage ) + 1 to length( known_files ) do*/
    if (IS_SEQUENCE(_52file_coverage_49064)){
            _25855 = SEQ_PTR(_52file_coverage_49064)->length;
    }
    else {
        _25855 = 1;
    }
    _25856 = _25855 + 1;
    _25855 = NOVALUE;
    if (IS_SEQUENCE(_35known_files_15596)){
            _25857 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _25857 = 1;
    }
    {
        int _i_49075;
        _i_49075 = _25856;
L1: 
        if (_i_49075 > _25857){
            goto L2; // [17] 58
        }

        /** 		file_coverage &= find( canonical_path( known_files[i],,1 ), covered_files )*/
        _2 = (int)SEQ_PTR(_35known_files_15596);
        _25858 = (int)*(((s1_ptr)_2)->base + _i_49075);
        Ref(_25858);
        _25859 = _13canonical_path(_25858, 0, 1);
        _25858 = NOVALUE;
        _25860 = find_from(_25859, _52covered_files_49063, 1);
        DeRef(_25859);
        _25859 = NOVALUE;
        Append(&_52file_coverage_49064, _52file_coverage_49064, _25860);
        _25860 = NOVALUE;

        /** 	end for*/
        _i_49075 = _i_49075 + 1;
        goto L1; // [53] 24
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_25856);
    _25856 = NOVALUE;
    return;
    ;
}


void _52init_coverage()
{
    int _cmd_49099 = NOVALUE;
    int _25878 = NOVALUE;
    int _25877 = NOVALUE;
    int _25875 = NOVALUE;
    int _25874 = NOVALUE;
    int _25873 = NOVALUE;
    int _25871 = NOVALUE;
    int _25869 = NOVALUE;
    int _25868 = NOVALUE;
    int _25866 = NOVALUE;
    int _25865 = NOVALUE;
    int _25864 = NOVALUE;
    int _25863 = NOVALUE;
    int _25862 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if initialized_coverage then*/
    if (_52initialized_coverage_49071 == 0)
    {
        goto L1; // [5] 14
    }
    else{
    }

    /** 		return*/
    return;
L1: 

    /** 	initialized_coverage = 1*/
    _52initialized_coverage_49071 = 1;

    /** 	for i = 1 to length( file_coverage ) do*/
    if (IS_SEQUENCE(_52file_coverage_49064)){
            _25862 = SEQ_PTR(_52file_coverage_49064)->length;
    }
    else {
        _25862 = 1;
    }
    {
        int _i_49090;
        _i_49090 = 1;
L2: 
        if (_i_49090 > _25862){
            goto L3; // [26] 67
        }

        /** 		file_coverage[i] = find( canonical_path( known_files[i],,1 ), covered_files )*/
        _2 = (int)SEQ_PTR(_35known_files_15596);
        _25863 = (int)*(((s1_ptr)_2)->base + _i_49090);
        Ref(_25863);
        _25864 = _13canonical_path(_25863, 0, 1);
        _25863 = NOVALUE;
        _25865 = find_from(_25864, _52covered_files_49063, 1);
        DeRef(_25864);
        _25864 = NOVALUE;
        _2 = (int)SEQ_PTR(_52file_coverage_49064);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _52file_coverage_49064 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _i_49090);
        *(int *)_2 = _25865;
        if( _1 != _25865 ){
        }
        _25865 = NOVALUE;

        /** 	end for*/
        _i_49090 = _i_49090 + 1;
        goto L2; // [62] 33
L3: 
        ;
    }

    /** 	if equal( coverage_db_name, "" ) then*/
    if (_52coverage_db_name_49065 == _22663)
    _25866 = 1;
    else if (IS_ATOM_INT(_52coverage_db_name_49065) && IS_ATOM_INT(_22663))
    _25866 = 0;
    else
    _25866 = (compare(_52coverage_db_name_49065, _22663) == 0);
    if (_25866 == 0)
    {
        _25866 = NOVALUE;
        goto L4; // [75] 107
    }
    else{
        _25866 = NOVALUE;
    }

    /** 		sequence cmd = command_line()*/
    DeRef(_cmd_49099);
    _cmd_49099 = Command_Line();

    /** 		coverage_db_name = canonical_path( filebase( cmd[2] ) & "-cvg.edb" )*/
    _2 = (int)SEQ_PTR(_cmd_49099);
    _25868 = (int)*(((s1_ptr)_2)->base + 2);
    RefDS(_25868);
    _25869 = _13filebase(_25868);
    _25868 = NOVALUE;
    if (IS_SEQUENCE(_25869) && IS_ATOM(_25870)) {
    }
    else if (IS_ATOM(_25869) && IS_SEQUENCE(_25870)) {
        Ref(_25869);
        Prepend(&_25871, _25870, _25869);
    }
    else {
        Concat((object_ptr)&_25871, _25869, _25870);
        DeRef(_25869);
        _25869 = NOVALUE;
    }
    DeRef(_25869);
    _25869 = NOVALUE;
    _0 = _13canonical_path(_25871, 0, 0);
    DeRefDS(_52coverage_db_name_49065);
    _52coverage_db_name_49065 = _0;
    _25871 = NOVALUE;
L4: 
    DeRef(_cmd_49099);
    _cmd_49099 = NOVALUE;

    /** 	if coverage_erase and file_exists( coverage_db_name ) then*/
    if (_52coverage_erase_49066 == 0) {
        goto L5; // [113] 153
    }
    RefDS(_52coverage_db_name_49065);
    _25874 = _13file_exists(_52coverage_db_name_49065);
    if (_25874 == 0) {
        DeRef(_25874);
        _25874 = NOVALUE;
        goto L5; // [124] 153
    }
    else {
        if (!IS_ATOM_INT(_25874) && DBL_PTR(_25874)->dbl == 0.0){
            DeRef(_25874);
            _25874 = NOVALUE;
            goto L5; // [124] 153
        }
        DeRef(_25874);
        _25874 = NOVALUE;
    }
    DeRef(_25874);
    _25874 = NOVALUE;

    /** 		if not delete_file( coverage_db_name ) then*/
    RefDS(_52coverage_db_name_49065);
    _25875 = _13delete_file(_52coverage_db_name_49065);
    if (IS_ATOM_INT(_25875)) {
        if (_25875 != 0){
            DeRef(_25875);
            _25875 = NOVALUE;
            goto L6; // [135] 152
        }
    }
    else {
        if (DBL_PTR(_25875)->dbl != 0.0){
            DeRef(_25875);
            _25875 = NOVALUE;
            goto L6; // [135] 152
        }
    }
    DeRef(_25875);
    _25875 = NOVALUE;

    /** 			CompileErr( 335, { coverage_db_name } )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_52coverage_db_name_49065);
    *((int *)(_2+4)) = _52coverage_db_name_49065;
    _25877 = MAKE_SEQ(_1);
    _46CompileErr(335, _25877, 0);
    _25877 = NOVALUE;
L6: 
L5: 

    /** 	if db_open( coverage_db_name ) = DB_OK then*/
    RefDS(_52coverage_db_name_49065);
    _25878 = _51db_open(_52coverage_db_name_49065, 0);
    if (binary_op_a(NOTEQ, _25878, 0)){
        DeRef(_25878);
        _25878 = NOVALUE;
        goto L7; // [166] 179
    }
    DeRef(_25878);
    _25878 = NOVALUE;

    /** 		read_coverage_db()*/
    _52read_coverage_db();

    /** 		db_close()*/
    _51db_close();
L7: 

    /** end procedure*/
    return;
    ;
}


void _52write_map(int _coverage_49128, int _table_name_49129)
{
    int _keys_49151 = NOVALUE;
    int _rec_49156 = NOVALUE;
    int _val_49160 = NOVALUE;
    int _32367 = NOVALUE;
    int _25895 = NOVALUE;
    int _25892 = NOVALUE;
    int _25890 = NOVALUE;
    int _25889 = NOVALUE;
    int _25887 = NOVALUE;
    int _25886 = NOVALUE;
    int _25884 = NOVALUE;
    int _25882 = NOVALUE;
    int _25880 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if db_select( coverage_db_name, DB_LOCK_EXCLUSIVE) = DB_OK then*/
    RefDS(_52coverage_db_name_49065);
    _25880 = _51db_select(_52coverage_db_name_49065, 2);
    if (binary_op_a(NOTEQ, _25880, 0)){
        DeRef(_25880);
        _25880 = NOVALUE;
        goto L1; // [16] 61
    }
    DeRef(_25880);
    _25880 = NOVALUE;

    /** 		if db_select_table( table_name ) != DB_OK then*/
    RefDS(_table_name_49129);
    _25882 = _51db_select_table(_table_name_49129);
    if (binary_op_a(EQUALS, _25882, 0)){
        DeRef(_25882);
        _25882 = NOVALUE;
        goto L2; // [28] 73
    }
    DeRef(_25882);
    _25882 = NOVALUE;

    /** 			if db_create_table( table_name ) != DB_OK then*/
    RefDS(_table_name_49129);
    _25884 = _51db_create_table(_table_name_49129, 50);
    if (binary_op_a(EQUALS, _25884, 0)){
        DeRef(_25884);
        _25884 = NOVALUE;
        goto L2; // [41] 73
    }
    DeRef(_25884);
    _25884 = NOVALUE;

    /** 				CompileErr( 336, {table_name} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_table_name_49129);
    *((int *)(_2+4)) = _table_name_49129;
    _25886 = MAKE_SEQ(_1);
    _46CompileErr(336, _25886, 0);
    _25886 = NOVALUE;
    goto L2; // [58] 73
L1: 

    /** 		CompileErr( 336, {table_name} )*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_table_name_49129);
    *((int *)(_2+4)) = _table_name_49129;
    _25887 = MAKE_SEQ(_1);
    _46CompileErr(336, _25887, 0);
    _25887 = NOVALUE;
L2: 

    /** 	sequence keys = map:keys( coverage )*/
    Ref(_coverage_49128);
    _0 = _keys_49151;
    _keys_49151 = _29keys(_coverage_49128, 0);
    DeRef(_0);

    /** 	for i = 1 to length( keys ) do*/
    if (IS_SEQUENCE(_keys_49151)){
            _25889 = SEQ_PTR(_keys_49151)->length;
    }
    else {
        _25889 = 1;
    }
    {
        int _i_49154;
        _i_49154 = 1;
L3: 
        if (_i_49154 > _25889){
            goto L4; // [87] 167
        }

        /** 		integer rec = db_find_key( keys[i] )*/
        _2 = (int)SEQ_PTR(_keys_49151);
        _25890 = (int)*(((s1_ptr)_2)->base + _i_49154);
        Ref(_25890);
        RefDS(_51current_table_name_18102);
        _rec_49156 = _51db_find_key(_25890, _51current_table_name_18102);
        _25890 = NOVALUE;
        if (!IS_ATOM_INT(_rec_49156)) {
            _1 = (long)(DBL_PTR(_rec_49156)->dbl);
            if (UNIQUE(DBL_PTR(_rec_49156)) && (DBL_PTR(_rec_49156)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_rec_49156);
            _rec_49156 = _1;
        }

        /** 		integer val = map:get( coverage, keys[i] )*/
        _2 = (int)SEQ_PTR(_keys_49151);
        _25892 = (int)*(((s1_ptr)_2)->base + _i_49154);
        Ref(_coverage_49128);
        Ref(_25892);
        _val_49160 = _29get(_coverage_49128, _25892, 0);
        _25892 = NOVALUE;
        if (!IS_ATOM_INT(_val_49160)) {
            _1 = (long)(DBL_PTR(_val_49160)->dbl);
            if (UNIQUE(DBL_PTR(_val_49160)) && (DBL_PTR(_val_49160)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_val_49160);
            _val_49160 = _1;
        }

        /** 		if rec > 0 then*/
        if (_rec_49156 <= 0)
        goto L5; // [125] 141

        /** 			db_replace_data( rec, val )*/
        RefDS(_51current_table_name_18102);
        _51db_replace_data(_rec_49156, _val_49160, _51current_table_name_18102);
        goto L6; // [138] 158
L5: 

        /** 			db_insert( keys[i], val )*/
        _2 = (int)SEQ_PTR(_keys_49151);
        _25895 = (int)*(((s1_ptr)_2)->base + _i_49154);
        Ref(_25895);
        RefDS(_51current_table_name_18102);
        _32367 = _51db_insert(_25895, _val_49160, _51current_table_name_18102);
        _25895 = NOVALUE;
        DeRef(_32367);
        _32367 = NOVALUE;
L6: 

        /** 	end for*/
        _i_49154 = _i_49154 + 1;
        goto L3; // [162] 94
L4: 
        ;
    }

    /** end procedure*/
    DeRef(_coverage_49128);
    DeRefDS(_table_name_49129);
    DeRef(_keys_49151);
    return;
    ;
}


int _52write_coverage_db()
{
    int _25910 = NOVALUE;
    int _25909 = NOVALUE;
    int _25908 = NOVALUE;
    int _25907 = NOVALUE;
    int _25906 = NOVALUE;
    int _25905 = NOVALUE;
    int _25904 = NOVALUE;
    int _25903 = NOVALUE;
    int _25900 = NOVALUE;
    int _25898 = NOVALUE;
    int _25896 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if wrote_coverage then*/
    if (_52wrote_coverage_49169 == 0)
    {
        goto L1; // [5] 15
    }
    else{
    }

    /** 		return 1*/
    return 1;
L1: 

    /** 	wrote_coverage = 1*/
    _52wrote_coverage_49169 = 1;

    /** 	init_coverage()*/
    _52init_coverage();

    /** 	if not length( covered_files ) then*/
    if (IS_SEQUENCE(_52covered_files_49063)){
            _25896 = SEQ_PTR(_52covered_files_49063)->length;
    }
    else {
        _25896 = 1;
    }
    if (_25896 != 0)
    goto L2; // [31] 41
    _25896 = NOVALUE;

    /** 		return 1*/
    return 1;
L2: 

    /** 	if DB_OK != db_open( coverage_db_name, DB_LOCK_EXCLUSIVE) then*/
    RefDS(_52coverage_db_name_49065);
    _25898 = _51db_open(_52coverage_db_name_49065, 2);
    if (binary_op_a(EQUALS, 0, _25898)){
        DeRef(_25898);
        _25898 = NOVALUE;
        goto L3; // [54] 97
    }
    DeRef(_25898);
    _25898 = NOVALUE;

    /** 		if DB_OK != db_create( coverage_db_name ) then*/
    RefDS(_52coverage_db_name_49065);
    _25900 = _51db_create(_52coverage_db_name_49065, 0, 5, 5);
    if (binary_op_a(EQUALS, 0, _25900)){
        DeRef(_25900);
        _25900 = NOVALUE;
        goto L4; // [73] 96
    }
    DeRef(_25900);
    _25900 = NOVALUE;

    /** 			printf(2, "error opening %s\n", {coverage_db_name})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_52coverage_db_name_49065);
    *((int *)(_2+4)) = _52coverage_db_name_49065;
    _25903 = MAKE_SEQ(_1);
    EPrintf(2, _25902, _25903);
    DeRefDS(_25903);
    _25903 = NOVALUE;

    /** 			return 0*/
    return 0;
L4: 
L3: 

    /** 	process_lines()*/
    _52process_lines();

    /** 	for tx = 1 to length( routine_map ) do*/
    if (IS_SEQUENCE(_52routine_map_49069)){
            _25904 = SEQ_PTR(_52routine_map_49069)->length;
    }
    else {
        _25904 = 1;
    }
    {
        int _tx_49191;
        _tx_49191 = 1;
L5: 
        if (_tx_49191 > _25904){
            goto L6; // [108] 166
        }

        /** 		write_map( routine_map[tx], 'r' & covered_files[tx] )*/
        _2 = (int)SEQ_PTR(_52routine_map_49069);
        _25905 = (int)*(((s1_ptr)_2)->base + _tx_49191);
        _2 = (int)SEQ_PTR(_52covered_files_49063);
        _25906 = (int)*(((s1_ptr)_2)->base + _tx_49191);
        if (IS_SEQUENCE(114) && IS_ATOM(_25906)) {
        }
        else if (IS_ATOM(114) && IS_SEQUENCE(_25906)) {
            Prepend(&_25907, _25906, 114);
        }
        else {
            Concat((object_ptr)&_25907, 114, _25906);
        }
        _25906 = NOVALUE;
        Ref(_25905);
        _52write_map(_25905, _25907);
        _25905 = NOVALUE;
        _25907 = NOVALUE;

        /** 		write_map( line_map[tx],    'l' & covered_files[tx] )*/
        _2 = (int)SEQ_PTR(_52line_map_49068);
        _25908 = (int)*(((s1_ptr)_2)->base + _tx_49191);
        _2 = (int)SEQ_PTR(_52covered_files_49063);
        _25909 = (int)*(((s1_ptr)_2)->base + _tx_49191);
        if (IS_SEQUENCE(108) && IS_ATOM(_25909)) {
        }
        else if (IS_ATOM(108) && IS_SEQUENCE(_25909)) {
            Prepend(&_25910, _25909, 108);
        }
        else {
            Concat((object_ptr)&_25910, 108, _25909);
        }
        _25909 = NOVALUE;
        Ref(_25908);
        _52write_map(_25908, _25910);
        _25908 = NOVALUE;
        _25910 = NOVALUE;

        /** 	end for*/
        _tx_49191 = _tx_49191 + 1;
        goto L5; // [161] 115
L6: 
        ;
    }

    /** 	db_close()*/
    _51db_close();

    /** 	routine_map = {}*/
    RefDS(_22663);
    DeRef(_52routine_map_49069);
    _52routine_map_49069 = _22663;

    /** 	line_map    = {}*/
    RefDS(_22663);
    DeRef(_52line_map_49068);
    _52line_map_49068 = _22663;

    /** 	return 1*/
    return 1;
    ;
}


void _52read_coverage_db()
{
    int _tables_49202 = NOVALUE;
    int _name_49208 = NOVALUE;
    int _fx_49212 = NOVALUE;
    int _the_map_49219 = NOVALUE;
    int _32366 = NOVALUE;
    int _25926 = NOVALUE;
    int _25925 = NOVALUE;
    int _25924 = NOVALUE;
    int _25920 = NOVALUE;
    int _25919 = NOVALUE;
    int _25918 = NOVALUE;
    int _25914 = NOVALUE;
    int _25913 = NOVALUE;
    int _25912 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence tables = db_table_list()*/
    _0 = _tables_49202;
    _tables_49202 = _51db_table_list();
    DeRef(_0);

    /** 	for i = 1 to length( tables ) do*/
    if (IS_SEQUENCE(_tables_49202)){
            _25912 = SEQ_PTR(_tables_49202)->length;
    }
    else {
        _25912 = 1;
    }
    {
        int _i_49206;
        _i_49206 = 1;
L1: 
        if (_i_49206 > _25912){
            goto L2; // [13] 161
        }

        /** 		sequence name = tables[i][2..$]*/
        _2 = (int)SEQ_PTR(_tables_49202);
        _25913 = (int)*(((s1_ptr)_2)->base + _i_49206);
        if (IS_SEQUENCE(_25913)){
                _25914 = SEQ_PTR(_25913)->length;
        }
        else {
            _25914 = 1;
        }
        rhs_slice_target = (object_ptr)&_name_49208;
        RHS_Slice(_25913, 2, _25914);
        _25913 = NOVALUE;

        /** 		integer fx = find( name, covered_files )*/
        _fx_49212 = find_from(_name_49208, _52covered_files_49063, 1);

        /** 		if not fx then*/
        if (_fx_49212 != 0)
        goto L3; // [45] 55

        /** 			continue*/
        DeRefDS(_name_49208);
        _name_49208 = NOVALUE;
        DeRef(_the_map_49219);
        _the_map_49219 = NOVALUE;
        goto L4; // [52] 156
L3: 

        /** 		db_select_table( tables[i] )*/
        _2 = (int)SEQ_PTR(_tables_49202);
        _25918 = (int)*(((s1_ptr)_2)->base + _i_49206);
        Ref(_25918);
        _32366 = _51db_select_table(_25918);
        _25918 = NOVALUE;
        DeRef(_32366);
        _32366 = NOVALUE;

        /** 		if tables[i][1] = 'r' then*/
        _2 = (int)SEQ_PTR(_tables_49202);
        _25919 = (int)*(((s1_ptr)_2)->base + _i_49206);
        _2 = (int)SEQ_PTR(_25919);
        _25920 = (int)*(((s1_ptr)_2)->base + 1);
        _25919 = NOVALUE;
        if (binary_op_a(NOTEQ, _25920, 114)){
            _25920 = NOVALUE;
            goto L5; // [77] 92
        }
        _25920 = NOVALUE;

        /** 			the_map = routine_map[fx]*/
        DeRef(_the_map_49219);
        _2 = (int)SEQ_PTR(_52routine_map_49069);
        _the_map_49219 = (int)*(((s1_ptr)_2)->base + _fx_49212);
        Ref(_the_map_49219);
        goto L6; // [89] 101
L5: 

        /** 			the_map = line_map[fx]*/
        DeRef(_the_map_49219);
        _2 = (int)SEQ_PTR(_52line_map_49068);
        _the_map_49219 = (int)*(((s1_ptr)_2)->base + _fx_49212);
        Ref(_the_map_49219);
L6: 

        /** 		for j = 1 to db_table_size() do*/
        RefDS(_51current_table_name_18102);
        _25924 = _51db_table_size(_51current_table_name_18102);
        {
            int _j_49228;
            _j_49228 = 1;
L7: 
            if (binary_op_a(GREATER, _j_49228, _25924)){
                goto L8; // [109] 152
            }

            /** 			map:put( the_map, db_record_key( j ), db_record_data( j ), map:ADD )*/
            Ref(_j_49228);
            RefDS(_51current_table_name_18102);
            _25925 = _51db_record_key(_j_49228, _51current_table_name_18102);
            Ref(_j_49228);
            RefDS(_51current_table_name_18102);
            _25926 = _51db_record_data(_j_49228, _51current_table_name_18102);
            Ref(_the_map_49219);
            _29put(_the_map_49219, _25925, _25926, 2, 23);
            _25925 = NOVALUE;
            _25926 = NOVALUE;

            /** 		end for*/
            _0 = _j_49228;
            if (IS_ATOM_INT(_j_49228)) {
                _j_49228 = _j_49228 + 1;
                if ((long)((unsigned long)_j_49228 +(unsigned long) HIGH_BITS) >= 0){
                    _j_49228 = NewDouble((double)_j_49228);
                }
            }
            else {
                _j_49228 = binary_op_a(PLUS, _j_49228, 1);
            }
            DeRef(_0);
            goto L7; // [147] 116
L8: 
            ;
            DeRef(_j_49228);
        }
        DeRef(_name_49208);
        _name_49208 = NOVALUE;
        DeRef(_the_map_49219);
        _the_map_49219 = NOVALUE;

        /** 	end for*/
L4: 
        _i_49206 = _i_49206 + 1;
        goto L1; // [156] 20
L2: 
        ;
    }

    /** end procedure*/
    DeRef(_tables_49202);
    DeRef(_25924);
    _25924 = NOVALUE;
    return;
    ;
}


void _52coverage_db(int _name_49237)
{
    int _0, _1, _2;
    

    /** 	coverage_db_name = name*/
    RefDS(_name_49237);
    DeRef(_52coverage_db_name_49065);
    _52coverage_db_name_49065 = _name_49237;

    /** end procedure*/
    DeRefDS(_name_49237);
    return;
    ;
}


int _52coverage_on()
{
    int _25927 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return file_coverage[current_file_no]*/
    _2 = (int)SEQ_PTR(_52file_coverage_49064);
    _25927 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    return _25927;
    ;
}


void _52new_covered_path(int _name_49249)
{
    int _25933 = NOVALUE;
    int _25931 = NOVALUE;
    int _0, _1, _2;
    

    /** 	covered_files = append( covered_files, name )*/
    RefDS(_name_49249);
    Append(&_52covered_files_49063, _52covered_files_49063, _name_49249);

    /** 	routine_map &= map:new()*/
    _25931 = _29new(690);
    if (IS_SEQUENCE(_52routine_map_49069) && IS_ATOM(_25931)) {
        Ref(_25931);
        Append(&_52routine_map_49069, _52routine_map_49069, _25931);
    }
    else if (IS_ATOM(_52routine_map_49069) && IS_SEQUENCE(_25931)) {
    }
    else {
        Concat((object_ptr)&_52routine_map_49069, _52routine_map_49069, _25931);
    }
    DeRef(_25931);
    _25931 = NOVALUE;

    /** 	line_map    &= map:new()*/
    _25933 = _29new(690);
    if (IS_SEQUENCE(_52line_map_49068) && IS_ATOM(_25933)) {
        Ref(_25933);
        Append(&_52line_map_49068, _52line_map_49068, _25933);
    }
    else if (IS_ATOM(_52line_map_49068) && IS_SEQUENCE(_25933)) {
    }
    else {
        Concat((object_ptr)&_52line_map_49068, _52line_map_49068, _25933);
    }
    DeRef(_25933);
    _25933 = NOVALUE;

    /** end procedure*/
    DeRefDS(_name_49249);
    return;
    ;
}


void _52add_coverage(int _cover_this_49257)
{
    int _path_49258 = NOVALUE;
    int _files_49267 = NOVALUE;
    int _subpath_49295 = NOVALUE;
    int _25968 = NOVALUE;
    int _25967 = NOVALUE;
    int _25966 = NOVALUE;
    int _25965 = NOVALUE;
    int _25964 = NOVALUE;
    int _25963 = NOVALUE;
    int _25962 = NOVALUE;
    int _25961 = NOVALUE;
    int _25960 = NOVALUE;
    int _25959 = NOVALUE;
    int _25958 = NOVALUE;
    int _25957 = NOVALUE;
    int _25955 = NOVALUE;
    int _25954 = NOVALUE;
    int _25953 = NOVALUE;
    int _25952 = NOVALUE;
    int _25951 = NOVALUE;
    int _25950 = NOVALUE;
    int _25949 = NOVALUE;
    int _25948 = NOVALUE;
    int _25946 = NOVALUE;
    int _25945 = NOVALUE;
    int _25944 = NOVALUE;
    int _25943 = NOVALUE;
    int _25942 = NOVALUE;
    int _25941 = NOVALUE;
    int _25940 = NOVALUE;
    int _25939 = NOVALUE;
    int _25936 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence path = canonical_path( cover_this,, CORRECT )*/
    RefDS(_cover_this_49257);
    _0 = _path_49258;
    _path_49258 = _13canonical_path(_cover_this_49257, 0, 2);
    DeRef(_0);

    /** 	if file_type( path ) = FILETYPE_DIRECTORY then*/
    RefDS(_path_49258);
    _25936 = _13file_type(_path_49258);
    if (binary_op_a(NOTEQ, _25936, 2)){
        DeRef(_25936);
        _25936 = NOVALUE;
        goto L1; // [23] 211
    }
    DeRef(_25936);
    _25936 = NOVALUE;

    /** 		sequence files = dir( path  )*/
    RefDS(_path_49258);
    _0 = _files_49267;
    _files_49267 = _13dir(_path_49258);
    DeRef(_0);

    /** 		for i = 1 to length( files ) do*/
    if (IS_SEQUENCE(_files_49267)){
            _25939 = SEQ_PTR(_files_49267)->length;
    }
    else {
        _25939 = 1;
    }
    {
        int _i_49271;
        _i_49271 = 1;
L2: 
        if (_i_49271 > _25939){
            goto L3; // [40] 206
        }

        /** 			if find( 'd', files[i][D_ATTRIBUTES] ) then*/
        _2 = (int)SEQ_PTR(_files_49267);
        _25940 = (int)*(((s1_ptr)_2)->base + _i_49271);
        _2 = (int)SEQ_PTR(_25940);
        _25941 = (int)*(((s1_ptr)_2)->base + 2);
        _25940 = NOVALUE;
        _25942 = find_from(100, _25941, 1);
        _25941 = NOVALUE;
        if (_25942 == 0)
        {
            _25942 = NOVALUE;
            goto L4; // [64] 118
        }
        else{
            _25942 = NOVALUE;
        }

        /** 				if not eu:find(files[i][D_NAME], {".", ".."}) then*/
        _2 = (int)SEQ_PTR(_files_49267);
        _25943 = (int)*(((s1_ptr)_2)->base + _i_49271);
        _2 = (int)SEQ_PTR(_25943);
        _25944 = (int)*(((s1_ptr)_2)->base + 1);
        _25943 = NOVALUE;
        RefDS(_23747);
        RefDS(_23748);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _23748;
        ((int *)_2)[2] = _23747;
        _25945 = MAKE_SEQ(_1);
        _25946 = find_from(_25944, _25945, 1);
        _25944 = NOVALUE;
        DeRefDS(_25945);
        _25945 = NOVALUE;
        if (_25946 != 0)
        goto L5; // [88] 199
        _25946 = NOVALUE;

        /** 					add_coverage( cover_this & SLASH & files[i][D_NAME] )*/
        _2 = (int)SEQ_PTR(_files_49267);
        _25948 = (int)*(((s1_ptr)_2)->base + _i_49271);
        _2 = (int)SEQ_PTR(_25948);
        _25949 = (int)*(((s1_ptr)_2)->base + 1);
        _25948 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _25949;
            concat_list[1] = 92;
            concat_list[2] = _cover_this_49257;
            Concat_N((object_ptr)&_25950, concat_list, 3);
        }
        _25949 = NOVALUE;
        _52add_coverage(_25950);
        _25950 = NOVALUE;
        goto L5; // [115] 199
L4: 

        /** 			elsif regex:has_match( eu_file, files[i][D_NAME] ) then*/
        _2 = (int)SEQ_PTR(_files_49267);
        _25951 = (int)*(((s1_ptr)_2)->base + _i_49271);
        _2 = (int)SEQ_PTR(_25951);
        _25952 = (int)*(((s1_ptr)_2)->base + 1);
        _25951 = NOVALUE;
        Ref(_52eu_file_49243);
        Ref(_25952);
        _25953 = _53has_match(_52eu_file_49243, _25952, 1, 0);
        _25952 = NOVALUE;
        if (_25953 == 0) {
            DeRef(_25953);
            _25953 = NOVALUE;
            goto L6; // [139] 196
        }
        else {
            if (!IS_ATOM_INT(_25953) && DBL_PTR(_25953)->dbl == 0.0){
                DeRef(_25953);
                _25953 = NOVALUE;
                goto L6; // [139] 196
            }
            DeRef(_25953);
            _25953 = NOVALUE;
        }
        DeRef(_25953);
        _25953 = NOVALUE;

        /** 				sequence subpath = path & SLASH & files[i][D_NAME]*/
        _2 = (int)SEQ_PTR(_files_49267);
        _25954 = (int)*(((s1_ptr)_2)->base + _i_49271);
        _2 = (int)SEQ_PTR(_25954);
        _25955 = (int)*(((s1_ptr)_2)->base + 1);
        _25954 = NOVALUE;
        {
            int concat_list[3];

            concat_list[0] = _25955;
            concat_list[1] = 92;
            concat_list[2] = _path_49258;
            Concat_N((object_ptr)&_subpath_49295, concat_list, 3);
        }
        _25955 = NOVALUE;

        /** 				if not find( subpath, covered_files ) and not excluded( subpath ) then*/
        _25957 = find_from(_subpath_49295, _52covered_files_49063, 1);
        _25958 = (_25957 == 0);
        _25957 = NOVALUE;
        if (_25958 == 0) {
            goto L7; // [174] 195
        }
        RefDS(_subpath_49295);
        _25960 = _52excluded(_subpath_49295);
        if (IS_ATOM_INT(_25960)) {
            _25961 = (_25960 == 0);
        }
        else {
            _25961 = unary_op(NOT, _25960);
        }
        DeRef(_25960);
        _25960 = NOVALUE;
        if (_25961 == 0) {
            DeRef(_25961);
            _25961 = NOVALUE;
            goto L7; // [186] 195
        }
        else {
            if (!IS_ATOM_INT(_25961) && DBL_PTR(_25961)->dbl == 0.0){
                DeRef(_25961);
                _25961 = NOVALUE;
                goto L7; // [186] 195
            }
            DeRef(_25961);
            _25961 = NOVALUE;
        }
        DeRef(_25961);
        _25961 = NOVALUE;

        /** 					new_covered_path( subpath )*/
        RefDS(_subpath_49295);
        _52new_covered_path(_subpath_49295);
L7: 
L6: 
        DeRef(_subpath_49295);
        _subpath_49295 = NOVALUE;
L5: 

        /** 		end for*/
        _i_49271 = _i_49271 + 1;
        goto L2; // [201] 47
L3: 
        ;
    }
    DeRef(_files_49267);
    _files_49267 = NOVALUE;
    goto L8; // [208] 262
L1: 

    /** 	elsif regex:has_match( eu_file, path ) and*/
    Ref(_52eu_file_49243);
    RefDS(_path_49258);
    _25962 = _53has_match(_52eu_file_49243, _path_49258, 1, 0);
    if (IS_ATOM_INT(_25962)) {
        if (_25962 == 0) {
            DeRef(_25963);
            _25963 = 0;
            goto L9; // [222] 240
        }
    }
    else {
        if (DBL_PTR(_25962)->dbl == 0.0) {
            DeRef(_25963);
            _25963 = 0;
            goto L9; // [222] 240
        }
    }
    _25964 = find_from(_path_49258, _52covered_files_49063, 1);
    _25965 = (_25964 == 0);
    _25964 = NOVALUE;
    DeRef(_25963);
    _25963 = (_25965 != 0);
L9: 
    if (_25963 == 0) {
        goto LA; // [240] 261
    }
    RefDS(_path_49258);
    _25967 = _52excluded(_path_49258);
    if (IS_ATOM_INT(_25967)) {
        _25968 = (_25967 == 0);
    }
    else {
        _25968 = unary_op(NOT, _25967);
    }
    DeRef(_25967);
    _25967 = NOVALUE;
    if (_25968 == 0) {
        DeRef(_25968);
        _25968 = NOVALUE;
        goto LA; // [252] 261
    }
    else {
        if (!IS_ATOM_INT(_25968) && DBL_PTR(_25968)->dbl == 0.0){
            DeRef(_25968);
            _25968 = NOVALUE;
            goto LA; // [252] 261
        }
        DeRef(_25968);
        _25968 = NOVALUE;
    }
    DeRef(_25968);
    _25968 = NOVALUE;

    /** 		new_covered_path( path )*/
    RefDS(_path_49258);
    _52new_covered_path(_path_49258);
LA: 
L8: 

    /** end procedure*/
    DeRefDS(_cover_this_49257);
    DeRef(_path_49258);
    DeRef(_25958);
    _25958 = NOVALUE;
    DeRef(_25962);
    _25962 = NOVALUE;
    DeRef(_25965);
    _25965 = NOVALUE;
    return;
    ;
}


int _52excluded(int _file_49319)
{
    int _25971 = NOVALUE;
    int _25970 = NOVALUE;
    int _25969 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( exclusion_patterns ) do*/
    if (IS_SEQUENCE(_52exclusion_patterns_49067)){
            _25969 = SEQ_PTR(_52exclusion_patterns_49067)->length;
    }
    else {
        _25969 = 1;
    }
    {
        int _i_49321;
        _i_49321 = 1;
L1: 
        if (_i_49321 > _25969){
            goto L2; // [10] 49
        }

        /** 		if regex:has_match( exclusion_patterns[i], file ) then*/
        _2 = (int)SEQ_PTR(_52exclusion_patterns_49067);
        _25970 = (int)*(((s1_ptr)_2)->base + _i_49321);
        Ref(_25970);
        RefDS(_file_49319);
        _25971 = _53has_match(_25970, _file_49319, 1, 0);
        _25970 = NOVALUE;
        if (_25971 == 0) {
            DeRef(_25971);
            _25971 = NOVALUE;
            goto L3; // [32] 42
        }
        else {
            if (!IS_ATOM_INT(_25971) && DBL_PTR(_25971)->dbl == 0.0){
                DeRef(_25971);
                _25971 = NOVALUE;
                goto L3; // [32] 42
            }
            DeRef(_25971);
            _25971 = NOVALUE;
        }
        DeRef(_25971);
        _25971 = NOVALUE;

        /** 			return 1*/
        DeRefDS(_file_49319);
        return 1;
L3: 

        /** 	end for*/
        _i_49321 = _i_49321 + 1;
        goto L1; // [44] 17
L2: 
        ;
    }

    /** 	return 0*/
    DeRefDS(_file_49319);
    return 0;
    ;
}


void _52coverage_exclude(int _patterns_49328)
{
    int _ex_49333 = NOVALUE;
    int _fx_49340 = NOVALUE;
    int _25989 = NOVALUE;
    int _25988 = NOVALUE;
    int _25987 = NOVALUE;
    int _25986 = NOVALUE;
    int _25980 = NOVALUE;
    int _25979 = NOVALUE;
    int _25977 = NOVALUE;
    int _25975 = NOVALUE;
    int _25973 = NOVALUE;
    int _25972 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to length( patterns ) do*/
    if (IS_SEQUENCE(_patterns_49328)){
            _25972 = SEQ_PTR(_patterns_49328)->length;
    }
    else {
        _25972 = 1;
    }
    {
        int _i_49330;
        _i_49330 = 1;
L1: 
        if (_i_49330 > _25972){
            goto L2; // [8] 161
        }

        /** 		regex ex = regex:new( patterns[i] )*/
        _2 = (int)SEQ_PTR(_patterns_49328);
        _25973 = (int)*(((s1_ptr)_2)->base + _i_49330);
        Ref(_25973);
        _0 = _ex_49333;
        _ex_49333 = _53new(_25973, 0);
        DeRef(_0);
        _25973 = NOVALUE;

        /** 		if regex( ex ) then*/
        Ref(_ex_49333);
        _25975 = _53regex(_ex_49333);
        if (_25975 == 0) {
            DeRef(_25975);
            _25975 = NOVALUE;
            goto L3; // [32] 127
        }
        else {
            if (!IS_ATOM_INT(_25975) && DBL_PTR(_25975)->dbl == 0.0){
                DeRef(_25975);
                _25975 = NOVALUE;
                goto L3; // [32] 127
            }
            DeRef(_25975);
            _25975 = NOVALUE;
        }
        DeRef(_25975);
        _25975 = NOVALUE;

        /** 			exclusion_patterns = append( exclusion_patterns, ex )*/
        Ref(_ex_49333);
        Append(&_52exclusion_patterns_49067, _52exclusion_patterns_49067, _ex_49333);

        /** 			integer fx = 1*/
        _fx_49340 = 1;

        /** 			while fx <= length( covered_files ) do*/
L4: 
        if (IS_SEQUENCE(_52covered_files_49063)){
                _25977 = SEQ_PTR(_52covered_files_49063)->length;
        }
        else {
            _25977 = 1;
        }
        if (_fx_49340 > _25977)
        goto L5; // [58] 122

        /** 				if regex:has_match( ex, covered_files[fx] ) then*/
        _2 = (int)SEQ_PTR(_52covered_files_49063);
        _25979 = (int)*(((s1_ptr)_2)->base + _fx_49340);
        Ref(_ex_49333);
        Ref(_25979);
        _25980 = _53has_match(_ex_49333, _25979, 1, 0);
        _25979 = NOVALUE;
        if (_25980 == 0) {
            DeRef(_25980);
            _25980 = NOVALUE;
            goto L6; // [77] 110
        }
        else {
            if (!IS_ATOM_INT(_25980) && DBL_PTR(_25980)->dbl == 0.0){
                DeRef(_25980);
                _25980 = NOVALUE;
                goto L6; // [77] 110
            }
            DeRef(_25980);
            _25980 = NOVALUE;
        }
        DeRef(_25980);
        _25980 = NOVALUE;

        /** 					covered_files = remove( covered_files, fx )*/
        {
            s1_ptr assign_space = SEQ_PTR(_52covered_files_49063);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_49340)) ? _fx_49340 : (long)(DBL_PTR(_fx_49340)->dbl);
            int stop = (IS_ATOM_INT(_fx_49340)) ? _fx_49340 : (long)(DBL_PTR(_fx_49340)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_52covered_files_49063), start, &_52covered_files_49063 );
                }
                else Tail(SEQ_PTR(_52covered_files_49063), stop+1, &_52covered_files_49063);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_52covered_files_49063), start, &_52covered_files_49063);
            }
            else {
                assign_slice_seq = &assign_space;
                _52covered_files_49063 = Remove_elements(start, stop, (SEQ_PTR(_52covered_files_49063)->ref == 1));
            }
        }

        /** 					routine_map   = remove( routine_map, fx )*/
        {
            s1_ptr assign_space = SEQ_PTR(_52routine_map_49069);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_49340)) ? _fx_49340 : (long)(DBL_PTR(_fx_49340)->dbl);
            int stop = (IS_ATOM_INT(_fx_49340)) ? _fx_49340 : (long)(DBL_PTR(_fx_49340)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_52routine_map_49069), start, &_52routine_map_49069 );
                }
                else Tail(SEQ_PTR(_52routine_map_49069), stop+1, &_52routine_map_49069);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_52routine_map_49069), start, &_52routine_map_49069);
            }
            else {
                assign_slice_seq = &assign_space;
                _52routine_map_49069 = Remove_elements(start, stop, (SEQ_PTR(_52routine_map_49069)->ref == 1));
            }
        }

        /** 					line_map      = remove( line_map, fx )*/
        {
            s1_ptr assign_space = SEQ_PTR(_52line_map_49068);
            int len = assign_space->length;
            int start = (IS_ATOM_INT(_fx_49340)) ? _fx_49340 : (long)(DBL_PTR(_fx_49340)->dbl);
            int stop = (IS_ATOM_INT(_fx_49340)) ? _fx_49340 : (long)(DBL_PTR(_fx_49340)->dbl);
            if (stop > len){
                stop = len;
            }
            if (start > len || start > stop || stop<1) {
            }
            else if (start < 2) {
                if (stop >= len) {
                    Head( SEQ_PTR(_52line_map_49068), start, &_52line_map_49068 );
                }
                else Tail(SEQ_PTR(_52line_map_49068), stop+1, &_52line_map_49068);
            }
            else if (stop >= len){
                Head(SEQ_PTR(_52line_map_49068), start, &_52line_map_49068);
            }
            else {
                assign_slice_seq = &assign_space;
                _52line_map_49068 = Remove_elements(start, stop, (SEQ_PTR(_52line_map_49068)->ref == 1));
            }
        }
        goto L4; // [107] 53
L6: 

        /** 					fx += 1*/
        _fx_49340 = _fx_49340 + 1;

        /** 			end while*/
        goto L4; // [119] 53
L5: 
        goto L7; // [124] 152
L3: 

        /** 			printf( 2,"%s\n", { GetMsgText( 339, 1, {patterns[i]}) } )*/
        _2 = (int)SEQ_PTR(_patterns_49328);
        _25986 = (int)*(((s1_ptr)_2)->base + _i_49330);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_25986);
        *((int *)(_2+4)) = _25986;
        _25987 = MAKE_SEQ(_1);
        _25986 = NOVALUE;
        _25988 = _47GetMsgText(339, 1, _25987);
        _25987 = NOVALUE;
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = _25988;
        _25989 = MAKE_SEQ(_1);
        _25988 = NOVALUE;
        EPrintf(2, _25985, _25989);
        DeRefDS(_25989);
        _25989 = NOVALUE;
L7: 
        DeRef(_ex_49333);
        _ex_49333 = NOVALUE;

        /** 	end for*/
        _i_49330 = _i_49330 + 1;
        goto L1; // [156] 15
L2: 
        ;
    }

    /** end procedure*/
    DeRefDS(_patterns_49328);
    return;
    ;
}


void _52new_coverage_db()
{
    int _0, _1, _2;
    

    /** 	coverage_erase = 1*/
    _52coverage_erase_49066 = 1;

    /** end procedure*/
    return;
    ;
}


void _52include_line(int _line_number_49363)
{
    int _25990 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_line_number_49363)) {
        _1 = (long)(DBL_PTR(_line_number_49363)->dbl);
        if (UNIQUE(DBL_PTR(_line_number_49363)) && (DBL_PTR(_line_number_49363)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_line_number_49363);
        _line_number_49363 = _1;
    }

    /** 	if coverage_on() then*/
    _25990 = _52coverage_on();
    if (_25990 == 0) {
        DeRef(_25990);
        _25990 = NOVALUE;
        goto L1; // [8] 34
    }
    else {
        if (!IS_ATOM_INT(_25990) && DBL_PTR(_25990)->dbl == 0.0){
            DeRef(_25990);
            _25990 = NOVALUE;
            goto L1; // [8] 34
        }
        DeRef(_25990);
        _25990 = NOVALUE;
    }
    DeRef(_25990);
    _25990 = NOVALUE;

    /** 		emit_op( COVERAGE_LINE )*/
    _43emit_op(210);

    /** 		emit_addr( gline_number )*/
    _43emit_addr(_38gline_number_16951);

    /** 		included_lines &= line_number*/
    Append(&_52included_lines_49070, _52included_lines_49070, _line_number_49363);
L1: 

    /** end procedure*/
    return;
    ;
}


void _52include_routine()
{
    int _file_no_49379 = NOVALUE;
    int _25997 = NOVALUE;
    int _25996 = NOVALUE;
    int _25995 = NOVALUE;
    int _25993 = NOVALUE;
    int _25992 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if coverage_on() then*/
    _25992 = _52coverage_on();
    if (_25992 == 0) {
        DeRef(_25992);
        _25992 = NOVALUE;
        goto L1; // [6] 73
    }
    else {
        if (!IS_ATOM_INT(_25992) && DBL_PTR(_25992)->dbl == 0.0){
            DeRef(_25992);
            _25992 = NOVALUE;
            goto L1; // [6] 73
        }
        DeRef(_25992);
        _25992 = NOVALUE;
    }
    DeRef(_25992);
    _25992 = NOVALUE;

    /** 		emit_op( COVERAGE_ROUTINE )*/
    _43emit_op(211);

    /** 		emit_addr( CurrentSub )*/
    _43emit_addr(_38CurrentSub_16954);

    /** 		integer file_no = SymTab[CurrentSub][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _25993 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_25993);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _file_no_49379 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _file_no_49379 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_file_no_49379)){
        _file_no_49379 = (long)DBL_PTR(_file_no_49379)->dbl;
    }
    _25993 = NOVALUE;

    /** 		map:put( routine_map[file_coverage[file_no]], sym_name( CurrentSub ), 0, map:ADD )*/
    _2 = (int)SEQ_PTR(_52file_coverage_49064);
    _25995 = (int)*(((s1_ptr)_2)->base + _file_no_49379);
    _2 = (int)SEQ_PTR(_52routine_map_49069);
    _25996 = (int)*(((s1_ptr)_2)->base + _25995);
    _25997 = _55sym_name(_38CurrentSub_16954);
    Ref(_25996);
    _29put(_25996, _25997, 0, 2, 23);
    _25996 = NOVALUE;
    _25997 = NOVALUE;
L1: 

    /** end procedure*/
    _25995 = NOVALUE;
    return;
    ;
}


void _52process_lines()
{
    int _sline_49407 = NOVALUE;
    int _file_49411 = NOVALUE;
    int _line_49421 = NOVALUE;
    int _26015 = NOVALUE;
    int _26013 = NOVALUE;
    int _26012 = NOVALUE;
    int _26011 = NOVALUE;
    int _26010 = NOVALUE;
    int _26009 = NOVALUE;
    int _26007 = NOVALUE;
    int _26005 = NOVALUE;
    int _26004 = NOVALUE;
    int _26002 = NOVALUE;
    int _26001 = NOVALUE;
    int _26000 = NOVALUE;
    int _25998 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if not length( included_lines ) then*/
    if (IS_SEQUENCE(_52included_lines_49070)){
            _25998 = SEQ_PTR(_52included_lines_49070)->length;
    }
    else {
        _25998 = 1;
    }
    if (_25998 != 0)
    goto L1; // [8] 17
    _25998 = NOVALUE;

    /** 		return*/
    return;
L1: 

    /** 	if atom(slist[$]) then*/
    if (IS_SEQUENCE(_38slist_17040)){
            _26000 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _26000 = 1;
    }
    _2 = (int)SEQ_PTR(_38slist_17040);
    _26001 = (int)*(((s1_ptr)_2)->base + _26000);
    _26002 = IS_ATOM(_26001);
    _26001 = NOVALUE;
    if (_26002 == 0)
    {
        _26002 = NOVALUE;
        goto L2; // [31] 45
    }
    else{
        _26002 = NOVALUE;
    }

    /** 		slist = s_expand( slist )*/
    RefDS(_38slist_17040);
    _0 = _62s_expand(_38slist_17040);
    DeRefDS(_38slist_17040);
    _38slist_17040 = _0;
L2: 

    /** 	for i = 1 to length( included_lines ) do*/
    if (IS_SEQUENCE(_52included_lines_49070)){
            _26004 = SEQ_PTR(_52included_lines_49070)->length;
    }
    else {
        _26004 = 1;
    }
    {
        int _i_49405;
        _i_49405 = 1;
L3: 
        if (_i_49405 > _26004){
            goto L4; // [52] 161
        }

        /** 		sequence sline = slist[included_lines[i]]*/
        _2 = (int)SEQ_PTR(_52included_lines_49070);
        _26005 = (int)*(((s1_ptr)_2)->base + _i_49405);
        DeRef(_sline_49407);
        _2 = (int)SEQ_PTR(_38slist_17040);
        _sline_49407 = (int)*(((s1_ptr)_2)->base + _26005);
        Ref(_sline_49407);

        /** 		integer file = file_coverage[sline[LOCAL_FILE_NO]]*/
        _2 = (int)SEQ_PTR(_sline_49407);
        _26007 = (int)*(((s1_ptr)_2)->base + 3);
        _2 = (int)SEQ_PTR(_52file_coverage_49064);
        if (!IS_ATOM_INT(_26007)){
            _file_49411 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_26007)->dbl));
        }
        else{
            _file_49411 = (int)*(((s1_ptr)_2)->base + _26007);
        }

        /** 		if file and file <= length( line_map ) and line_map[file] then*/
        if (_file_49411 == 0) {
            _26009 = 0;
            goto L5; // [91] 108
        }
        if (IS_SEQUENCE(_52line_map_49068)){
                _26010 = SEQ_PTR(_52line_map_49068)->length;
        }
        else {
            _26010 = 1;
        }
        _26011 = (_file_49411 <= _26010);
        _26010 = NOVALUE;
        _26009 = (_26011 != 0);
L5: 
        if (_26009 == 0) {
            goto L6; // [108] 150
        }
        _2 = (int)SEQ_PTR(_52line_map_49068);
        _26013 = (int)*(((s1_ptr)_2)->base + _file_49411);
        if (_26013 == 0) {
            _26013 = NOVALUE;
            goto L6; // [119] 150
        }
        else {
            if (!IS_ATOM_INT(_26013) && DBL_PTR(_26013)->dbl == 0.0){
                _26013 = NOVALUE;
                goto L6; // [119] 150
            }
            _26013 = NOVALUE;
        }
        _26013 = NOVALUE;

        /** 			integer line = sline[LINE]*/
        _2 = (int)SEQ_PTR(_sline_49407);
        _line_49421 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_line_49421))
        _line_49421 = (long)DBL_PTR(_line_49421)->dbl;

        /** 			map:put( line_map[file], line, 0, map:ADD )*/
        _2 = (int)SEQ_PTR(_52line_map_49068);
        _26015 = (int)*(((s1_ptr)_2)->base + _file_49411);
        Ref(_26015);
        _29put(_26015, _line_49421, 0, 2, 23);
        _26015 = NOVALUE;
L6: 
        DeRef(_sline_49407);
        _sline_49407 = NOVALUE;

        /** 	end for*/
        _i_49405 = _i_49405 + 1;
        goto L3; // [156] 59
L4: 
        ;
    }

    /** end procedure*/
    _26005 = NOVALUE;
    _26007 = NOVALUE;
    DeRef(_26011);
    _26011 = NOVALUE;
    return;
    ;
}


void _52cover_line(int _gline_number_49427)
{
    int _sline_49437 = NOVALUE;
    int _file_49440 = NOVALUE;
    int _line_49445 = NOVALUE;
    int _26024 = NOVALUE;
    int _26021 = NOVALUE;
    int _26018 = NOVALUE;
    int _26017 = NOVALUE;
    int _26016 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_gline_number_49427)) {
        _1 = (long)(DBL_PTR(_gline_number_49427)->dbl);
        if (UNIQUE(DBL_PTR(_gline_number_49427)) && (DBL_PTR(_gline_number_49427)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_gline_number_49427);
        _gline_number_49427 = _1;
    }

    /** 	if atom(slist[$]) then*/
    if (IS_SEQUENCE(_38slist_17040)){
            _26016 = SEQ_PTR(_38slist_17040)->length;
    }
    else {
        _26016 = 1;
    }
    _2 = (int)SEQ_PTR(_38slist_17040);
    _26017 = (int)*(((s1_ptr)_2)->base + _26016);
    _26018 = IS_ATOM(_26017);
    _26017 = NOVALUE;
    if (_26018 == 0)
    {
        _26018 = NOVALUE;
        goto L1; // [17] 31
    }
    else{
        _26018 = NOVALUE;
    }

    /** 		slist = s_expand(slist)*/
    RefDS(_38slist_17040);
    _0 = _62s_expand(_38slist_17040);
    DeRefDS(_38slist_17040);
    _38slist_17040 = _0;
L1: 

    /** 	sequence sline = slist[gline_number]*/
    DeRef(_sline_49437);
    _2 = (int)SEQ_PTR(_38slist_17040);
    _sline_49437 = (int)*(((s1_ptr)_2)->base + _gline_number_49427);
    Ref(_sline_49437);

    /** 	integer file = file_coverage[sline[LOCAL_FILE_NO]]*/
    _2 = (int)SEQ_PTR(_sline_49437);
    _26021 = (int)*(((s1_ptr)_2)->base + 3);
    _2 = (int)SEQ_PTR(_52file_coverage_49064);
    if (!IS_ATOM_INT(_26021)){
        _file_49440 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_26021)->dbl));
    }
    else{
        _file_49440 = (int)*(((s1_ptr)_2)->base + _26021);
    }

    /** 	if file then*/
    if (_file_49440 == 0)
    {
        goto L2; // [57] 88
    }
    else{
    }

    /** 		integer line = sline[LINE]*/
    _2 = (int)SEQ_PTR(_sline_49437);
    _line_49445 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_line_49445))
    _line_49445 = (long)DBL_PTR(_line_49445)->dbl;

    /** 		map:put( line_map[file], line, 1, map:ADD )*/
    _2 = (int)SEQ_PTR(_52line_map_49068);
    _26024 = (int)*(((s1_ptr)_2)->base + _file_49440);
    Ref(_26024);
    _29put(_26024, _line_49445, 1, 2, 23);
    _26024 = NOVALUE;
L2: 

    /** end procedure*/
    DeRef(_sline_49437);
    _26021 = NOVALUE;
    return;
    ;
}


void _52cover_routine(int _sub_49452)
{
    int _file_no_49453 = NOVALUE;
    int _26029 = NOVALUE;
    int _26028 = NOVALUE;
    int _26027 = NOVALUE;
    int _26025 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_sub_49452)) {
        _1 = (long)(DBL_PTR(_sub_49452)->dbl);
        if (UNIQUE(DBL_PTR(_sub_49452)) && (DBL_PTR(_sub_49452)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_49452);
        _sub_49452 = _1;
    }

    /** 	integer file_no = SymTab[sub][S_FILE_NO]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _26025 = (int)*(((s1_ptr)_2)->base + _sub_49452);
    _2 = (int)SEQ_PTR(_26025);
    if (!IS_ATOM_INT(_38S_FILE_NO_16594)){
        _file_no_49453 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_FILE_NO_16594)->dbl));
    }
    else{
        _file_no_49453 = (int)*(((s1_ptr)_2)->base + _38S_FILE_NO_16594);
    }
    if (!IS_ATOM_INT(_file_no_49453)){
        _file_no_49453 = (long)DBL_PTR(_file_no_49453)->dbl;
    }
    _26025 = NOVALUE;

    /** 	map:put( routine_map[file_coverage[file_no]], sym_name( sub ), 1, map:ADD )*/
    _2 = (int)SEQ_PTR(_52file_coverage_49064);
    _26027 = (int)*(((s1_ptr)_2)->base + _file_no_49453);
    _2 = (int)SEQ_PTR(_52routine_map_49069);
    _26028 = (int)*(((s1_ptr)_2)->base + _26027);
    _26029 = _55sym_name(_sub_49452);
    Ref(_26028);
    _29put(_26028, _26029, 1, 2, 23);
    _26028 = NOVALUE;
    _26029 = NOVALUE;

    /** end procedure*/
    _26027 = NOVALUE;
    return;
    ;
}


int _52has_coverage()
{
    int _26030 = NOVALUE;
    int _0, _1, _2;
    

    /** 	return length( covered_files )*/
    if (IS_SEQUENCE(_52covered_files_49063)){
            _26030 = SEQ_PTR(_52covered_files_49063)->length;
    }
    else {
        _26030 = 1;
    }
    return _26030;
    ;
}



// 0x7F16B990
