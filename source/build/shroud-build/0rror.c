// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _46screen_output(int _f_49493, int _msg_49494)
{
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_f_49493)) {
        _1 = (long)(DBL_PTR(_f_49493)->dbl);
        if (UNIQUE(DBL_PTR(_f_49493)) && (DBL_PTR(_f_49493)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_f_49493);
        _f_49493 = _1;
    }

    /** 	puts(f, msg)*/
    EPuts(_f_49493, _msg_49494); // DJP 

    /** end procedure*/
    DeRefDS(_msg_49494);
    return;
    ;
}


void _46Warning(int _msg_49497, int _mask_49498, int _args_49499)
{
    int _orig_mask_49500 = NOVALUE;
    int _text_49501 = NOVALUE;
    int _w_name_49502 = NOVALUE;
    int _26064 = NOVALUE;
    int _26062 = NOVALUE;
    int _26059 = NOVALUE;
    int _26056 = NOVALUE;
    int _26051 = NOVALUE;
    int _26049 = NOVALUE;
    int _26048 = NOVALUE;
    int _26047 = NOVALUE;
    int _26046 = NOVALUE;
    int _26044 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_mask_49498)) {
        _1 = (long)(DBL_PTR(_mask_49498)->dbl);
        if (UNIQUE(DBL_PTR(_mask_49498)) && (DBL_PTR(_mask_49498)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_mask_49498);
        _mask_49498 = _1;
    }

    /** 	if display_warnings = 0 then*/
    if (_46display_warnings_49481 != 0)
    goto L1; // [9] 19

    /** 		return*/
    DeRef(_msg_49497);
    DeRefDS(_args_49499);
    DeRef(_text_49501);
    DeRef(_w_name_49502);
    return;
L1: 

    /** 	if not Strict_is_on or Strict_Override then*/
    _26044 = (_38Strict_is_on_17011 == 0);
    if (_26044 != 0) {
        goto L2; // [26] 37
    }
    if (_38Strict_Override_17012 == 0)
    {
        goto L3; // [33] 56
    }
    else{
    }
L2: 

    /** 		if find(mask, strict_only_warnings) then*/
    _26046 = find_from(_mask_49498, _38strict_only_warnings_17009, 1);
    if (_26046 == 0)
    {
        _26046 = NOVALUE;
        goto L4; // [46] 55
    }
    else{
        _26046 = NOVALUE;
    }

    /** 			return*/
    DeRef(_msg_49497);
    DeRefDS(_args_49499);
    DeRef(_text_49501);
    DeRef(_w_name_49502);
    DeRef(_26044);
    _26044 = NOVALUE;
    return;
L4: 
L3: 

    /** 	orig_mask = mask -- =0 for non maskable warnings - none implemented so far*/
    _orig_mask_49500 = _mask_49498;

    /** 	if Strict_is_on and Strict_Override = 0 then*/
    if (_38Strict_is_on_17011 == 0) {
        goto L5; // [65] 85
    }
    _26048 = (_38Strict_Override_17012 == 0);
    if (_26048 == 0)
    {
        DeRef(_26048);
        _26048 = NOVALUE;
        goto L5; // [76] 85
    }
    else{
        DeRef(_26048);
        _26048 = NOVALUE;
    }

    /** 		mask = 0*/
    _mask_49498 = 0;
L5: 

    /** 	if mask = 0 or and_bits(OpWarning, mask) then*/
    _26049 = (_mask_49498 == 0);
    if (_26049 != 0) {
        goto L6; // [91] 106
    }
    {unsigned long tu;
         tu = (unsigned long)_38OpWarning_17013 & (unsigned long)_mask_49498;
         _26051 = MAKE_UINT(tu);
    }
    if (_26051 == 0) {
        DeRef(_26051);
        _26051 = NOVALUE;
        goto L7; // [102] 213
    }
    else {
        if (!IS_ATOM_INT(_26051) && DBL_PTR(_26051)->dbl == 0.0){
            DeRef(_26051);
            _26051 = NOVALUE;
            goto L7; // [102] 213
        }
        DeRef(_26051);
        _26051 = NOVALUE;
    }
    DeRef(_26051);
    _26051 = NOVALUE;
L6: 

    /** 		if orig_mask != 0 then*/
    if (_orig_mask_49500 == 0)
    goto L8; // [108] 122

    /** 			orig_mask = find(orig_mask,warning_flags)*/
    _orig_mask_49500 = find_from(_orig_mask_49500, _38warning_flags_16988, 1);
L8: 

    /** 		if orig_mask != 0 then*/
    if (_orig_mask_49500 == 0)
    goto L9; // [124] 145

    /** 			w_name = "{ " & warning_names[orig_mask] & " }"*/
    _2 = (int)SEQ_PTR(_38warning_names_16990);
    _26056 = (int)*(((s1_ptr)_2)->base + _orig_mask_49500);
    {
        int concat_list[3];

        concat_list[0] = _26057;
        concat_list[1] = _26056;
        concat_list[2] = _26055;
        Concat_N((object_ptr)&_w_name_49502, concat_list, 3);
    }
    _26056 = NOVALUE;
    goto LA; // [142] 153
L9: 

    /** 			w_name = "" -- not maskable*/
    RefDS(_22663);
    DeRef(_w_name_49502);
    _w_name_49502 = _22663;
LA: 

    /** 		if atom(msg) then*/
    _26059 = IS_ATOM(_msg_49497);
    if (_26059 == 0)
    {
        _26059 = NOVALUE;
        goto LB; // [158] 170
    }
    else{
        _26059 = NOVALUE;
    }

    /** 			msg = GetMsgText(msg, 1, args)*/
    Ref(_msg_49497);
    RefDS(_args_49499);
    _0 = _msg_49497;
    _msg_49497 = _47GetMsgText(_msg_49497, 1, _args_49499);
    DeRef(_0);
LB: 

    /** 		text = GetMsgText(204, 0, {w_name, msg})*/
    Ref(_msg_49497);
    RefDS(_w_name_49502);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _w_name_49502;
    ((int *)_2)[2] = _msg_49497;
    _26062 = MAKE_SEQ(_1);
    _0 = _text_49501;
    _text_49501 = _47GetMsgText(204, 0, _26062);
    DeRef(_0);
    _26062 = NOVALUE;

    /** 		if find(text, warning_list) then*/
    _26064 = find_from(_text_49501, _46warning_list_49490, 1);
    if (_26064 == 0)
    {
        _26064 = NOVALUE;
        goto LC; // [195] 204
    }
    else{
        _26064 = NOVALUE;
    }

    /** 			return -- duplicate*/
    DeRef(_msg_49497);
    DeRefDS(_args_49499);
    DeRefDS(_text_49501);
    DeRefDS(_w_name_49502);
    DeRef(_26044);
    _26044 = NOVALUE;
    DeRef(_26049);
    _26049 = NOVALUE;
    return;
LC: 

    /** 		warning_list = append(warning_list, text)*/
    RefDS(_text_49501);
    Append(&_46warning_list_49490, _46warning_list_49490, _text_49501);
L7: 

    /** end procedure*/
    DeRef(_msg_49497);
    DeRefDS(_args_49499);
    DeRef(_text_49501);
    DeRef(_w_name_49502);
    DeRef(_26044);
    _26044 = NOVALUE;
    DeRef(_26049);
    _26049 = NOVALUE;
    return;
    ;
}


void _46Log_warnings(int _policy_49548)
{
    int _26072 = NOVALUE;
    int _26071 = NOVALUE;
    int _26070 = NOVALUE;
    int _26069 = NOVALUE;
    int _26067 = NOVALUE;
    int _26066 = NOVALUE;
    int _0, _1, _2;
    

    /** 	display_warnings = 1*/
    _46display_warnings_49481 = 1;

    /** 	if sequence(policy) then*/
    _26066 = IS_SEQUENCE(_policy_49548);
    if (_26066 == 0)
    {
        _26066 = NOVALUE;
        goto L1; // [11] 34
    }
    else{
        _26066 = NOVALUE;
    }

    /** 		if length(policy)=0 then*/
    if (IS_SEQUENCE(_policy_49548)){
            _26067 = SEQ_PTR(_policy_49548)->length;
    }
    else {
        _26067 = 1;
    }
    if (_26067 != 0)
    goto L2; // [19] 82

    /** 			policy = STDERR*/
    DeRef(_policy_49548);
    _policy_49548 = 2;
    goto L2; // [31] 82
L1: 

    /** 		if policy >= 0 and policy < STDERR+1 then*/
    if (IS_ATOM_INT(_policy_49548)) {
        _26069 = (_policy_49548 >= 0);
    }
    else {
        _26069 = binary_op(GREATEREQ, _policy_49548, 0);
    }
    if (IS_ATOM_INT(_26069)) {
        if (_26069 == 0) {
            goto L3; // [40] 68
        }
    }
    else {
        if (DBL_PTR(_26069)->dbl == 0.0) {
            goto L3; // [40] 68
        }
    }
    _26071 = 3;
    if (IS_ATOM_INT(_policy_49548)) {
        _26072 = (_policy_49548 < 3);
    }
    else {
        _26072 = binary_op(LESS, _policy_49548, 3);
    }
    _26071 = NOVALUE;
    if (_26072 == 0) {
        DeRef(_26072);
        _26072 = NOVALUE;
        goto L3; // [55] 68
    }
    else {
        if (!IS_ATOM_INT(_26072) && DBL_PTR(_26072)->dbl == 0.0){
            DeRef(_26072);
            _26072 = NOVALUE;
            goto L3; // [55] 68
        }
        DeRef(_26072);
        _26072 = NOVALUE;
    }
    DeRef(_26072);
    _26072 = NOVALUE;

    /** 			policy  = STDERR*/
    DeRef(_policy_49548);
    _policy_49548 = 2;
    goto L4; // [65] 81
L3: 

    /** 		elsif policy < 0 then*/
    if (binary_op_a(GREATEREQ, _policy_49548, 0)){
        goto L5; // [70] 80
    }

    /** 			display_warnings = 0*/
    _46display_warnings_49481 = 0;
L5: 
L4: 
L2: 

    /** end procedure*/
    DeRef(_policy_49548);
    DeRef(_26069);
    _26069 = NOVALUE;
    return;
    ;
}


int _46ShowWarnings()
{
    int _c_49567 = NOVALUE;
    int _errfile_49568 = NOVALUE;
    int _twf_49569 = NOVALUE;
    int _26105 = NOVALUE;
    int _26101 = NOVALUE;
    int _26100 = NOVALUE;
    int _26099 = NOVALUE;
    int _26098 = NOVALUE;
    int _26097 = NOVALUE;
    int _26096 = NOVALUE;
    int _26094 = NOVALUE;
    int _26093 = NOVALUE;
    int _26092 = NOVALUE;
    int _26090 = NOVALUE;
    int _26089 = NOVALUE;
    int _26088 = NOVALUE;
    int _26087 = NOVALUE;
    int _26085 = NOVALUE;
    int _26080 = NOVALUE;
    int _26078 = NOVALUE;
    int _26077 = NOVALUE;
    int _26076 = NOVALUE;
    int _26074 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if display_warnings = 0 or length(warning_list) = 0 then*/
    _26074 = (_46display_warnings_49481 == 0);
    if (_26074 != 0) {
        goto L1; // [9] 27
    }
    if (IS_SEQUENCE(_46warning_list_49490)){
            _26076 = SEQ_PTR(_46warning_list_49490)->length;
    }
    else {
        _26076 = 1;
    }
    _26077 = (_26076 == 0);
    _26076 = NOVALUE;
    if (_26077 == 0)
    {
        DeRef(_26077);
        _26077 = NOVALUE;
        goto L2; // [23] 39
    }
    else{
        DeRef(_26077);
        _26077 = NOVALUE;
    }
L1: 

    /** 		return length(warning_list)*/
    if (IS_SEQUENCE(_46warning_list_49490)){
            _26078 = SEQ_PTR(_46warning_list_49490)->length;
    }
    else {
        _26078 = 1;
    }
    DeRef(_26074);
    _26074 = NOVALUE;
    return _26078;
L2: 

    /** 	if TempErrFile > 0 then*/
    if (_46TempErrFile_49479 <= 0)
    goto L3; // [43] 57

    /** 		errfile = TempErrFile*/
    _errfile_49568 = _46TempErrFile_49479;
    goto L4; // [54] 67
L3: 

    /** 		errfile = STDERR*/
    _errfile_49568 = 2;
L4: 

    /** 	if not integer(TempWarningName) then*/
    if (IS_ATOM_INT(_38TempWarningName_16960))
    _26080 = 1;
    else if (IS_ATOM_DBL(_38TempWarningName_16960))
    _26080 = IS_ATOM_INT(DoubleToInt(_38TempWarningName_16960));
    else
    _26080 = 0;
    if (_26080 != 0)
    goto L5; // [74] 179
    _26080 = NOVALUE;

    /** 		twf = open(TempWarningName,"w")*/
    _twf_49569 = EOpen(_38TempWarningName_16960, _22769, 0);

    /** 		if twf = -1 then*/
    if (_twf_49569 != -1)
    goto L6; // [88] 136

    /** 			ShowMsg(errfile, 205, {TempWarningName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_38TempWarningName_16960);
    *((int *)(_2+4)) = _38TempWarningName_16960;
    _26085 = MAKE_SEQ(_1);
    _47ShowMsg(_errfile_49568, 205, _26085, 1);
    _26085 = NOVALUE;

    /** 			if errfile != STDERR then*/
    if (_errfile_49568 == 2)
    goto L7; // [112] 173

    /** 				ShowMsg(STDERR, 205, {TempWarningName})*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_38TempWarningName_16960);
    *((int *)(_2+4)) = _38TempWarningName_16960;
    _26087 = MAKE_SEQ(_1);
    _47ShowMsg(2, 205, _26087, 1);
    _26087 = NOVALUE;
    goto L7; // [133] 173
L6: 

    /** 			for i = 1 to length(warning_list) do*/
    if (IS_SEQUENCE(_46warning_list_49490)){
            _26088 = SEQ_PTR(_46warning_list_49490)->length;
    }
    else {
        _26088 = 1;
    }
    {
        int _i_49601;
        _i_49601 = 1;
L8: 
        if (_i_49601 > _26088){
            goto L9; // [143] 168
        }

        /** 				puts(twf, warning_list[i])*/
        _2 = (int)SEQ_PTR(_46warning_list_49490);
        _26089 = (int)*(((s1_ptr)_2)->base + _i_49601);
        EPuts(_twf_49569, _26089); // DJP 
        _26089 = NOVALUE;

        /** 			end for*/
        _i_49601 = _i_49601 + 1;
        goto L8; // [163] 150
L9: 
        ;
    }

    /** 		    close(twf)*/
    EClose(_twf_49569);
L7: 

    /** 		TempWarningName = 99 -- Flag that we have done this already.*/
    DeRef(_38TempWarningName_16960);
    _38TempWarningName_16960 = 99;
L5: 

    /** 	if batch_job = 0 or errfile != STDERR then*/
    _26090 = (_38batch_job_16959 == 0);
    if (_26090 != 0) {
        goto LA; // [187] 204
    }
    _26092 = (_errfile_49568 != 2);
    if (_26092 == 0)
    {
        DeRef(_26092);
        _26092 = NOVALUE;
        goto LB; // [200] 311
    }
    else{
        DeRef(_26092);
        _26092 = NOVALUE;
    }
LA: 

    /** 		for i = 1 to length(warning_list) do*/
    if (IS_SEQUENCE(_46warning_list_49490)){
            _26093 = SEQ_PTR(_46warning_list_49490)->length;
    }
    else {
        _26093 = 1;
    }
    {
        int _i_49612;
        _i_49612 = 1;
LC: 
        if (_i_49612 > _26093){
            goto LD; // [211] 310
        }

        /** 			puts(errfile, warning_list[i])*/
        _2 = (int)SEQ_PTR(_46warning_list_49490);
        _26094 = (int)*(((s1_ptr)_2)->base + _i_49612);
        EPuts(_errfile_49568, _26094); // DJP 
        _26094 = NOVALUE;

        /** 			if errfile = STDERR then*/
        if (_errfile_49568 != 2)
        goto LE; // [235] 303

        /** 				if remainder(i, 20) = 0 and batch_job = 0 and test_only = 0 then*/
        _26096 = (_i_49612 % 20);
        _26097 = (_26096 == 0);
        _26096 = NOVALUE;
        if (_26097 == 0) {
            _26098 = 0;
            goto LF; // [249] 263
        }
        _26099 = (_38batch_job_16959 == 0);
        _26098 = (_26099 != 0);
LF: 
        if (_26098 == 0) {
            goto L10; // [263] 302
        }
        _26101 = (_38test_only_16958 == 0);
        if (_26101 == 0)
        {
            DeRef(_26101);
            _26101 = NOVALUE;
            goto L10; // [274] 302
        }
        else{
            DeRef(_26101);
            _26101 = NOVALUE;
        }

        /** 					ShowMsg(errfile, 206)*/
        RefDS(_22663);
        _47ShowMsg(_errfile_49568, 206, _22663, 1);

        /** 					c = getc(0)*/
        if (0 != last_r_file_no) {
            last_r_file_ptr = which_file(0, EF_READ);
            last_r_file_no = 0;
        }
        if (last_r_file_ptr == xstdin) {
            show_console();
            if (in_from_keyb) {
                _c_49567 = getKBchar();
            }
            else
            _c_49567 = getc(last_r_file_ptr);
        }
        else
        _c_49567 = getc(last_r_file_ptr);

        /** 					if c = 'q' then*/
        if (_c_49567 != 113)
        goto L11; // [292] 301

        /** 						exit*/
        goto LD; // [298] 310
L11: 
L10: 
LE: 

        /** 		end for*/
        _i_49612 = _i_49612 + 1;
        goto LC; // [305] 218
LD: 
        ;
    }
LB: 

    /** 	return length(warning_list)*/
    if (IS_SEQUENCE(_46warning_list_49490)){
            _26105 = SEQ_PTR(_46warning_list_49490)->length;
    }
    else {
        _26105 = 1;
    }
    DeRef(_26074);
    _26074 = NOVALUE;
    DeRef(_26090);
    _26090 = NOVALUE;
    DeRef(_26099);
    _26099 = NOVALUE;
    DeRef(_26097);
    _26097 = NOVALUE;
    return _26105;
    ;
}


void _46ShowDefines(int _errfile_49635)
{
    int _c_49636 = NOVALUE;
    int _26120 = NOVALUE;
    int _26119 = NOVALUE;
    int _26117 = NOVALUE;
    int _26116 = NOVALUE;
    int _26113 = NOVALUE;
    int _26112 = NOVALUE;
    int _26111 = NOVALUE;
    int _26110 = NOVALUE;
    int _26109 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_errfile_49635)) {
        _1 = (long)(DBL_PTR(_errfile_49635)->dbl);
        if (UNIQUE(DBL_PTR(_errfile_49635)) && (DBL_PTR(_errfile_49635)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_errfile_49635);
        _errfile_49635 = _1;
    }

    /** 	if errfile=0 then*/
    if (_errfile_49635 != 0)
    goto L1; // [5] 19

    /** 		errfile = STDERR*/
    _errfile_49635 = 2;
L1: 

    /** 	puts(errfile, format("\n--- [1] ---\n", {GetMsgText(207,0)}))*/
    RefDS(_22663);
    _26109 = _47GetMsgText(207, 0, _22663);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _26109;
    _26110 = MAKE_SEQ(_1);
    _26109 = NOVALUE;
    RefDS(_26107);
    _26111 = _10format(_26107, _26110);
    _26110 = NOVALUE;
    EPuts(_errfile_49635, _26111); // DJP 
    DeRef(_26111);
    _26111 = NOVALUE;

    /** 	for i = 1 to length(OpDefines) do*/
    if (IS_SEQUENCE(_38OpDefines_17019)){
            _26112 = SEQ_PTR(_38OpDefines_17019)->length;
    }
    else {
        _26112 = 1;
    }
    {
        int _i_49648;
        _i_49648 = 1;
L2: 
        if (_i_49648 > _26112){
            goto L3; // [46] 98
        }

        /** 		if find(OpDefines[i], {"_PLAT_START", "_PLAT_STOP"}) = 0 then*/
        _2 = (int)SEQ_PTR(_38OpDefines_17019);
        _26113 = (int)*(((s1_ptr)_2)->base + _i_49648);
        RefDS(_26115);
        RefDS(_26114);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _26114;
        ((int *)_2)[2] = _26115;
        _26116 = MAKE_SEQ(_1);
        _26117 = find_from(_26113, _26116, 1);
        _26113 = NOVALUE;
        DeRefDS(_26116);
        _26116 = NOVALUE;
        if (_26117 != 0)
        goto L4; // [70] 91

        /** 			printf(errfile, "%s\n", {OpDefines[i]})*/
        _2 = (int)SEQ_PTR(_38OpDefines_17019);
        _26119 = (int)*(((s1_ptr)_2)->base + _i_49648);
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        Ref(_26119);
        *((int *)(_2+4)) = _26119;
        _26120 = MAKE_SEQ(_1);
        _26119 = NOVALUE;
        EPrintf(_errfile_49635, _25985, _26120);
        DeRefDS(_26120);
        _26120 = NOVALUE;
L4: 

        /** 	end for*/
        _i_49648 = _i_49648 + 1;
        goto L2; // [93] 53
L3: 
        ;
    }

    /** 	puts(errfile, "-------------------\n")*/
    EPuts(_errfile_49635, _26121); // DJP 

    /** end procedure*/
    return;
    ;
}


void _46Cleanup(int _status_49665)
{
    int _w_49666 = NOVALUE;
    int _show_error_49667 = NOVALUE;
    int _26136 = NOVALUE;
    int _26135 = NOVALUE;
    int _26133 = NOVALUE;
    int _26132 = NOVALUE;
    int _26131 = NOVALUE;
    int _26130 = NOVALUE;
    int _26129 = NOVALUE;
    int _26128 = NOVALUE;
    int _26127 = NOVALUE;
    int _26126 = NOVALUE;
    int _26122 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_status_49665)) {
        _1 = (long)(DBL_PTR(_status_49665)->dbl);
        if (UNIQUE(DBL_PTR(_status_49665)) && (DBL_PTR(_status_49665)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_status_49665);
        _status_49665 = _1;
    }

    /** 	integer w, show_error = 0*/
    _show_error_49667 = 0;

    /** 	ifdef EU_EX then*/

    /** 	show_error = 1*/
    _show_error_49667 = 1;

    /** 	if object(src_file) = 0 then*/
    if( NOVALUE == _38src_file_17087 ){
        _26122 = 0;
    }
    else{
        _26122 = 1;
    }
    if (_26122 != 0)
    goto L1; // [20] 34

    /** 		src_file = -1*/
    _38src_file_17087 = -1;
    goto L2; // [31] 57
L1: 

    /** 	elsif src_file >= 0 then*/
    if (_38src_file_17087 < 0)
    goto L3; // [38] 56

    /** 		close(src_file)*/
    EClose(_38src_file_17087);

    /** 		src_file = -1*/
    _38src_file_17087 = -1;
L3: 
L2: 

    /** 	w = ShowWarnings()*/
    _w_49666 = _46ShowWarnings();
    if (!IS_ATOM_INT(_w_49666)) {
        _1 = (long)(DBL_PTR(_w_49666)->dbl);
        if (UNIQUE(DBL_PTR(_w_49666)) && (DBL_PTR(_w_49666)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_w_49666);
        _w_49666 = _1;
    }

    /** 	if not TRANSLATE and (BIND or show_error) and (w or Errors) then*/
    _26126 = (_38TRANSLATE_16564 == 0);
    if (_26126 == 0) {
        _26127 = 0;
        goto L4; // [71] 89
    }
    if (_38BIND_16567 != 0) {
        _26128 = 1;
        goto L5; // [77] 85
    }
    _26128 = (_show_error_49667 != 0);
L5: 
    _26127 = (_26128 != 0);
L4: 
    if (_26127 == 0) {
        goto L6; // [89] 148
    }
    if (_w_49666 != 0) {
        DeRef(_26130);
        _26130 = 1;
        goto L7; // [93] 103
    }
    _26130 = (_46Errors_49478 != 0);
L7: 
    if (_26130 == 0)
    {
        _26130 = NOVALUE;
        goto L6; // [104] 148
    }
    else{
        _26130 = NOVALUE;
    }

    /** 		if not batch_job and not test_only then*/
    _26131 = (_38batch_job_16959 == 0);
    if (_26131 == 0) {
        goto L8; // [114] 147
    }
    _26133 = (_38test_only_16958 == 0);
    if (_26133 == 0)
    {
        DeRef(_26133);
        _26133 = NOVALUE;
        goto L8; // [124] 147
    }
    else{
        DeRef(_26133);
        _26133 = NOVALUE;
    }

    /** 			screen_output(STDERR, GetMsgText(208,0))*/
    RefDS(_22663);
    _26135 = _47GetMsgText(208, 0, _22663);
    _46screen_output(2, _26135);
    _26135 = NOVALUE;

    /** 			getc(0) -- wait*/
    if (0 != last_r_file_no) {
        last_r_file_ptr = which_file(0, EF_READ);
        last_r_file_no = 0;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _26136 = getKBchar();
        }
        else
        _26136 = getc(last_r_file_ptr);
    }
    else
    _26136 = getc(last_r_file_ptr);
L8: 
L6: 

    /** 	cleanup_open_includes()*/
    _62cleanup_open_includes();

    /** 	abort(status)*/
    UserCleanup(_status_49665);

    /** end procedure*/
    DeRef(_26126);
    _26126 = NOVALUE;
    DeRef(_26131);
    _26131 = NOVALUE;
    return;
    ;
}


void _46OpenErrFile()
{
    int _26144 = NOVALUE;
    int _26143 = NOVALUE;
    int _26140 = NOVALUE;
    int _0, _1, _2;
    

    /**     if TempErrFile != -1 then*/
    if (_46TempErrFile_49479 == -1)
    goto L1; // [5] 19

    /** 		TempErrFile = open(TempErrName, "w")*/
    _46TempErrFile_49479 = EOpen(_46TempErrName_49480, _22769, 0);
L1: 

    /** 	if TempErrFile = -1 then*/
    if (_46TempErrFile_49479 != -1)
    goto L2; // [23] 64

    /** 		if length(TempErrName) > 0 then*/
    _26140 = 6;

    /** 			screen_output(STDERR, GetMsgText(209, 0, {TempErrName}))*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_46TempErrName_49480);
    *((int *)(_2+4)) = _46TempErrName_49480;
    _26143 = MAKE_SEQ(_1);
    _26144 = _47GetMsgText(209, 0, _26143);
    _26143 = NOVALUE;
    _46screen_output(2, _26144);
    _26144 = NOVALUE;

    /** 		abort(1) -- with no clean up*/
    UserCleanup(1);
L2: 

    /** end procedure*/
    return;
    ;
}


void _46ShowErr(int _f_49716)
{
    int _msg_inlined_screen_output_at_41_49729 = NOVALUE;
    int _26152 = NOVALUE;
    int _26151 = NOVALUE;
    int _26150 = NOVALUE;
    int _26147 = NOVALUE;
    int _26145 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length(known_files) = 0 then*/
    if (IS_SEQUENCE(_35known_files_15596)){
            _26145 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _26145 = 1;
    }
    if (_26145 != 0)
    goto L1; // [10] 20

    /** 		return*/
    return;
L1: 

    /** 	if ThisLine[1] = END_OF_FILE_CHAR then*/
    _2 = (int)SEQ_PTR(_46ThisLine_49482);
    _26147 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(NOTEQ, _26147, 26)){
        _26147 = NOVALUE;
        goto L2; // [30] 62
    }
    _26147 = NOVALUE;

    /** 		screen_output(f, GetMsgText(210,0))*/
    RefDS(_22663);
    _26150 = _47GetMsgText(210, 0, _22663);
    DeRef(_msg_inlined_screen_output_at_41_49729);
    _msg_inlined_screen_output_at_41_49729 = _26150;
    _26150 = NOVALUE;

    /** 	puts(f, msg)*/
    EPuts(_f_49716, _msg_inlined_screen_output_at_41_49729); // DJP 

    /** end procedure*/
    goto L3; // [54] 57
L3: 
    DeRef(_msg_inlined_screen_output_at_41_49729);
    _msg_inlined_screen_output_at_41_49729 = NOVALUE;
    goto L4; // [59] 79
L2: 

    /** 		screen_output(f, ThisLine)*/

    /** 	puts(f, msg)*/
    EPuts(_f_49716, _46ThisLine_49482); // DJP 

    /** end procedure*/
    goto L5; // [75] 78
L5: 
L4: 

    /** 	for i = 1 to bp-2 do -- bp-1 points to last character read*/
    _26151 = _46bp_49486 - 2;
    if ((long)((unsigned long)_26151 +(unsigned long) HIGH_BITS) >= 0){
        _26151 = NewDouble((double)_26151);
    }
    {
        int _i_49733;
        _i_49733 = 1;
L6: 
        if (binary_op_a(GREATER, _i_49733, _26151)){
            goto L7; // [87] 141
        }

        /** 		if ThisLine[i] = '\t' then*/
        _2 = (int)SEQ_PTR(_46ThisLine_49482);
        if (!IS_ATOM_INT(_i_49733)){
            _26152 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_i_49733)->dbl));
        }
        else{
            _26152 = (int)*(((s1_ptr)_2)->base + _i_49733);
        }
        if (binary_op_a(NOTEQ, _26152, 9)){
            _26152 = NOVALUE;
            goto L8; // [102] 121
        }
        _26152 = NOVALUE;

        /** 			screen_output(f, "\t")*/

        /** 	puts(f, msg)*/
        EPuts(_f_49716, _24480); // DJP 

        /** end procedure*/
        goto L9; // [115] 134
        goto L9; // [118] 134
L8: 

        /** 			screen_output(f, " ")*/

        /** 	puts(f, msg)*/
        EPuts(_f_49716, _23949); // DJP 

        /** end procedure*/
        goto LA; // [130] 133
LA: 
L9: 

        /** 	end for*/
        _0 = _i_49733;
        if (IS_ATOM_INT(_i_49733)) {
            _i_49733 = _i_49733 + 1;
            if ((long)((unsigned long)_i_49733 +(unsigned long) HIGH_BITS) >= 0){
                _i_49733 = NewDouble((double)_i_49733);
            }
        }
        else {
            _i_49733 = binary_op_a(PLUS, _i_49733, 1);
        }
        DeRef(_0);
        goto L6; // [136] 94
L7: 
        ;
        DeRef(_i_49733);
    }

    /** 	screen_output(f, "^\n\n")*/

    /** 	puts(f, msg)*/
    EPuts(_f_49716, _26154); // DJP 

    /** end procedure*/
    goto LB; // [150] 153
LB: 

    /** end procedure*/
    DeRef(_26151);
    _26151 = NOVALUE;
    return;
    ;
}


void _46CompileErr(int _msg_49745, int _args_49746, int _preproc_49747)
{
    int _errmsg_49748 = NOVALUE;
    int _26175 = NOVALUE;
    int _26171 = NOVALUE;
    int _26170 = NOVALUE;
    int _26169 = NOVALUE;
    int _26168 = NOVALUE;
    int _26167 = NOVALUE;
    int _26166 = NOVALUE;
    int _26164 = NOVALUE;
    int _26163 = NOVALUE;
    int _26161 = NOVALUE;
    int _26160 = NOVALUE;
    int _26159 = NOVALUE;
    int _26155 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_preproc_49747)) {
        _1 = (long)(DBL_PTR(_preproc_49747)->dbl);
        if (UNIQUE(DBL_PTR(_preproc_49747)) && (DBL_PTR(_preproc_49747)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_preproc_49747);
        _preproc_49747 = _1;
    }

    /** 	if integer(msg) then*/
    if (IS_ATOM_INT(_msg_49745))
    _26155 = 1;
    else if (IS_ATOM_DBL(_msg_49745))
    _26155 = IS_ATOM_INT(DoubleToInt(_msg_49745));
    else
    _26155 = 0;
    if (_26155 == 0)
    {
        _26155 = NOVALUE;
        goto L1; // [8] 20
    }
    else{
        _26155 = NOVALUE;
    }

    /** 		msg = GetMsgText(msg)*/
    Ref(_msg_49745);
    RefDS(_22663);
    _0 = _msg_49745;
    _msg_49745 = _47GetMsgText(_msg_49745, 1, _22663);
    DeRef(_0);
L1: 

    /** 	msg = format(msg, args)*/
    Ref(_msg_49745);
    Ref(_args_49746);
    _0 = _msg_49745;
    _msg_49745 = _10format(_msg_49745, _args_49746);
    DeRef(_0);

    /** 	Errors += 1*/
    _46Errors_49478 = _46Errors_49478 + 1;

    /** 	if not preproc and length(known_files) then*/
    _26159 = (_preproc_49747 == 0);
    if (_26159 == 0) {
        goto L2; // [40] 78
    }
    if (IS_SEQUENCE(_35known_files_15596)){
            _26161 = SEQ_PTR(_35known_files_15596)->length;
    }
    else {
        _26161 = 1;
    }
    if (_26161 == 0)
    {
        _26161 = NOVALUE;
        goto L2; // [50] 78
    }
    else{
        _26161 = NOVALUE;
    }

    /** 		errmsg = sprintf("%s:%d\n%s\n", {known_files[current_file_no],*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _26163 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_26163);
    *((int *)(_2+4)) = _26163;
    *((int *)(_2+8)) = _38line_number_16947;
    Ref(_msg_49745);
    *((int *)(_2+12)) = _msg_49745;
    _26164 = MAKE_SEQ(_1);
    _26163 = NOVALUE;
    DeRef(_errmsg_49748);
    _errmsg_49748 = EPrintf(-9999999, _26162, _26164);
    DeRefDS(_26164);
    _26164 = NOVALUE;
    goto L3; // [75] 121
L2: 

    /** 		errmsg = msg*/
    Ref(_msg_49745);
    DeRef(_errmsg_49748);
    _errmsg_49748 = _msg_49745;

    /** 		if length(msg) > 0 and msg[$] != '\n' then*/
    if (IS_SEQUENCE(_msg_49745)){
            _26166 = SEQ_PTR(_msg_49745)->length;
    }
    else {
        _26166 = 1;
    }
    _26167 = (_26166 > 0);
    _26166 = NOVALUE;
    if (_26167 == 0) {
        goto L4; // [94] 120
    }
    if (IS_SEQUENCE(_msg_49745)){
            _26169 = SEQ_PTR(_msg_49745)->length;
    }
    else {
        _26169 = 1;
    }
    _2 = (int)SEQ_PTR(_msg_49745);
    _26170 = (int)*(((s1_ptr)_2)->base + _26169);
    if (IS_ATOM_INT(_26170)) {
        _26171 = (_26170 != 10);
    }
    else {
        _26171 = binary_op(NOTEQ, _26170, 10);
    }
    _26170 = NOVALUE;
    if (_26171 == 0) {
        DeRef(_26171);
        _26171 = NOVALUE;
        goto L4; // [110] 120
    }
    else {
        if (!IS_ATOM_INT(_26171) && DBL_PTR(_26171)->dbl == 0.0){
            DeRef(_26171);
            _26171 = NOVALUE;
            goto L4; // [110] 120
        }
        DeRef(_26171);
        _26171 = NOVALUE;
    }
    DeRef(_26171);
    _26171 = NOVALUE;

    /** 			errmsg &= '\n'*/
    Append(&_errmsg_49748, _errmsg_49748, 10);
L4: 
L3: 

    /** 	if not preproc then*/
    if (_preproc_49747 != 0)
    goto L5; // [123] 131

    /** 		OpenErrFile() -- exits if error filename is ""*/
    _46OpenErrFile();
L5: 

    /** 	screen_output(STDERR, errmsg)*/
    RefDS(_errmsg_49748);
    _46screen_output(2, _errmsg_49748);

    /** 	if not preproc then*/
    if (_preproc_49747 != 0)
    goto L6; // [143] 196

    /** 		ShowErr(STDERR)*/
    _46ShowErr(2);

    /** 		puts(TempErrFile, errmsg)*/
    EPuts(_46TempErrFile_49479, _errmsg_49748); // DJP 

    /** 		ShowErr(TempErrFile)*/
    _46ShowErr(_46TempErrFile_49479);

    /** 		ShowWarnings()*/
    _26175 = _46ShowWarnings();

    /** 		ShowDefines(TempErrFile)*/
    _46ShowDefines(_46TempErrFile_49479);

    /** 		close(TempErrFile)*/
    EClose(_46TempErrFile_49479);

    /** 		TempErrFile = -2*/
    _46TempErrFile_49479 = -2;

    /** 		Cleanup(1)*/
    _46Cleanup(1);
L6: 

    /** end procedure*/
    DeRef(_msg_49745);
    DeRef(_args_49746);
    DeRef(_errmsg_49748);
    DeRef(_26159);
    _26159 = NOVALUE;
    DeRef(_26167);
    _26167 = NOVALUE;
    DeRef(_26175);
    _26175 = NOVALUE;
    return;
    ;
}


void _46InternalErr(int _msgno_49791, int _args_49792)
{
    int _msg_49793 = NOVALUE;
    int _26192 = NOVALUE;
    int _26190 = NOVALUE;
    int _26189 = NOVALUE;
    int _26188 = NOVALUE;
    int _26187 = NOVALUE;
    int _26186 = NOVALUE;
    int _26185 = NOVALUE;
    int _26184 = NOVALUE;
    int _26183 = NOVALUE;
    int _26182 = NOVALUE;
    int _26181 = NOVALUE;
    int _26177 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_msgno_49791)) {
        _1 = (long)(DBL_PTR(_msgno_49791)->dbl);
        if (UNIQUE(DBL_PTR(_msgno_49791)) && (DBL_PTR(_msgno_49791)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_msgno_49791);
        _msgno_49791 = _1;
    }

    /** 	if atom(args) then*/
    _26177 = IS_ATOM(_args_49792);
    if (_26177 == 0)
    {
        _26177 = NOVALUE;
        goto L1; // [8] 18
    }
    else{
        _26177 = NOVALUE;
    }

    /** 		args = {args}*/
    _0 = _args_49792;
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_args_49792);
    *((int *)(_2+4)) = _args_49792;
    _args_49792 = MAKE_SEQ(_1);
    DeRef(_0);
L1: 

    /** 	msg = GetMsgText(msgno, 1, args)*/
    Ref(_args_49792);
    _0 = _msg_49793;
    _msg_49793 = _47GetMsgText(_msgno_49791, 1, _args_49792);
    DeRef(_0);

    /** 	if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2; // [32] 56
    }
    else{
    }

    /** 		screen_output(STDERR, GetMsgText(211, 1, {msg}))*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    RefDS(_msg_49793);
    *((int *)(_2+4)) = _msg_49793;
    _26181 = MAKE_SEQ(_1);
    _26182 = _47GetMsgText(211, 1, _26181);
    _26181 = NOVALUE;
    _46screen_output(2, _26182);
    _26182 = NOVALUE;
    goto L3; // [53] 87
L2: 

    /** 		screen_output(STDERR, GetMsgText(212, 1, {known_files[current_file_no], line_number, msg}))*/
    _2 = (int)SEQ_PTR(_35known_files_15596);
    _26183 = (int)*(((s1_ptr)_2)->base + _38current_file_no_16946);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_26183);
    *((int *)(_2+4)) = _26183;
    *((int *)(_2+8)) = _38line_number_16947;
    RefDS(_msg_49793);
    *((int *)(_2+12)) = _msg_49793;
    _26184 = MAKE_SEQ(_1);
    _26183 = NOVALUE;
    _26185 = _47GetMsgText(212, 1, _26184);
    _26184 = NOVALUE;
    _46screen_output(2, _26185);
    _26185 = NOVALUE;
L3: 

    /** 	if not batch_job and not test_only then*/
    _26186 = (_38batch_job_16959 == 0);
    if (_26186 == 0) {
        goto L4; // [94] 127
    }
    _26188 = (_38test_only_16958 == 0);
    if (_26188 == 0)
    {
        DeRef(_26188);
        _26188 = NOVALUE;
        goto L4; // [104] 127
    }
    else{
        DeRef(_26188);
        _26188 = NOVALUE;
    }

    /** 		screen_output(STDERR, GetMsgText(208, 0))*/
    RefDS(_22663);
    _26189 = _47GetMsgText(208, 0, _22663);
    _46screen_output(2, _26189);
    _26189 = NOVALUE;

    /** 		getc(0)*/
    if (0 != last_r_file_no) {
        last_r_file_ptr = which_file(0, EF_READ);
        last_r_file_no = 0;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _26190 = getKBchar();
        }
        else
        _26190 = getc(last_r_file_ptr);
    }
    else
    _26190 = getc(last_r_file_ptr);
L4: 

    /** 	machine_proc(67, GetMsgText(213))*/
    RefDS(_22663);
    _26192 = _47GetMsgText(213, 1, _22663);
    machine(67, _26192);
    DeRef(_26192);
    _26192 = NOVALUE;

    /** end procedure*/
    DeRef(_args_49792);
    DeRef(_msg_49793);
    DeRef(_26186);
    _26186 = NOVALUE;
    return;
    ;
}



// 0x9BD8226B
