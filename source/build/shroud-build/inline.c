// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

int _69advance(int _pc_53688, int _code_53689)
{
    int _27926 = NOVALUE;
    int _27924 = NOVALUE;
    int _0, _1, _2;
    

    /** 	prev_pc = pc*/
    _69prev_pc_53673 = _pc_53688;

    /** 	if pc > length( code ) then*/
    if (IS_SEQUENCE(_code_53689)){
            _27924 = SEQ_PTR(_code_53689)->length;
    }
    else {
        _27924 = 1;
    }
    if (_pc_53688 <= _27924)
    goto L1; // [15] 26

    /** 		return pc*/
    DeRefDS(_code_53689);
    return _pc_53688;
L1: 

    /** 	return shift:advance( pc, code )*/
    RefDS(_code_53689);
    _27926 = _67advance(_pc_53688, _code_53689);
    DeRefDS(_code_53689);
    return _27926;
    ;
}


void _69shift(int _start_53696, int _amount_53697, int _bound_53698)
{
    int _temp_LineTable_53699 = NOVALUE;
    int _temp_Code_53701 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_amount_53697)) {
        _1 = (long)(DBL_PTR(_amount_53697)->dbl);
        if (UNIQUE(DBL_PTR(_amount_53697)) && (DBL_PTR(_amount_53697)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_amount_53697);
        _amount_53697 = _1;
    }

    /** 		temp_LineTable = LineTable,*/
    RefDS(_38LineTable_17039);
    DeRef(_temp_LineTable_53699);
    _temp_LineTable_53699 = _38LineTable_17039;

    /** 		temp_Code = Code*/
    RefDS(_38Code_17038);
    DeRef(_temp_Code_53701);
    _temp_Code_53701 = _38Code_17038;

    /** 	LineTable = {}*/
    RefDS(_22663);
    DeRefDS(_38LineTable_17039);
    _38LineTable_17039 = _22663;

    /** 	Code = inline_code*/
    RefDS(_69inline_code_53665);
    DeRefDS(_38Code_17038);
    _38Code_17038 = _69inline_code_53665;

    /** 	inline_code = {}*/
    RefDS(_22663);
    DeRefDS(_69inline_code_53665);
    _69inline_code_53665 = _22663;

    /** 	shift:shift( start, amount, bound )*/
    _67shift(_start_53696, _amount_53697, _bound_53698);

    /** 	LineTable = temp_LineTable*/
    RefDS(_temp_LineTable_53699);
    DeRefDS(_38LineTable_17039);
    _38LineTable_17039 = _temp_LineTable_53699;

    /** 	inline_code = Code*/
    RefDS(_38Code_17038);
    DeRefDS(_69inline_code_53665);
    _69inline_code_53665 = _38Code_17038;

    /** 	Code = temp_Code*/
    RefDS(_temp_Code_53701);
    DeRefDS(_38Code_17038);
    _38Code_17038 = _temp_Code_53701;

    /** end procedure*/
    DeRefDS(_temp_LineTable_53699);
    DeRefDS(_temp_Code_53701);
    return;
    ;
}


void _69insert_code(int _code_53710, int _index_53711)
{
    int _27928 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_index_53711)) {
        _1 = (long)(DBL_PTR(_index_53711)->dbl);
        if (UNIQUE(DBL_PTR(_index_53711)) && (DBL_PTR(_index_53711)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_index_53711);
        _index_53711 = _1;
    }

    /** 	inline_code = splice( inline_code, code, index )*/
    {
        s1_ptr assign_space;
        insert_pos = _index_53711;
        if (insert_pos <= 0) {
            Concat(&_69inline_code_53665,_code_53710,_69inline_code_53665);
        }
        else if (insert_pos > SEQ_PTR(_69inline_code_53665)->length){
            Concat(&_69inline_code_53665,_69inline_code_53665,_code_53710);
        }
        else if (IS_SEQUENCE(_code_53710)) {
            if( _69inline_code_53665 != _69inline_code_53665 || SEQ_PTR( _69inline_code_53665 )->ref != 1 ){
                DeRef( _69inline_code_53665 );
                RefDS( _69inline_code_53665 );
            }
            assign_space = Add_internal_space( _69inline_code_53665, insert_pos,((s1_ptr)SEQ_PTR(_code_53710))->length);
            assign_slice_seq = &assign_space;
            assign_space = Copy_elements( insert_pos, SEQ_PTR(_code_53710), _69inline_code_53665 == _69inline_code_53665 );
            _69inline_code_53665 = MAKE_SEQ( assign_space );
        }
        else {
            if( _69inline_code_53665 == _69inline_code_53665 && SEQ_PTR( _69inline_code_53665 )->ref == 1 ){
                _69inline_code_53665 = Insert( _69inline_code_53665, _code_53710, insert_pos);
            }
            else {
                DeRef( _69inline_code_53665 );
                RefDS( _69inline_code_53665 );
                _69inline_code_53665 = Insert( _69inline_code_53665, _code_53710, insert_pos);
            }
        }
    }

    /** 	shift( index, length( code ) )*/
    if (IS_SEQUENCE(_code_53710)){
            _27928 = SEQ_PTR(_code_53710)->length;
    }
    else {
        _27928 = 1;
    }
    _69shift(_index_53711, _27928, _index_53711);
    _27928 = NOVALUE;

    /** end procedure*/
    DeRefDS(_code_53710);
    return;
    ;
}


void _69replace_code(int _code_53716, int _start_53717, int _finish_53718)
{
    int _27933 = NOVALUE;
    int _27932 = NOVALUE;
    int _27931 = NOVALUE;
    int _27930 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_start_53717)) {
        _1 = (long)(DBL_PTR(_start_53717)->dbl);
        if (UNIQUE(DBL_PTR(_start_53717)) && (DBL_PTR(_start_53717)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_53717);
        _start_53717 = _1;
    }
    if (!IS_ATOM_INT(_finish_53718)) {
        _1 = (long)(DBL_PTR(_finish_53718)->dbl);
        if (UNIQUE(DBL_PTR(_finish_53718)) && (DBL_PTR(_finish_53718)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_finish_53718);
        _finish_53718 = _1;
    }

    /** 	inline_code = replace( inline_code, code, start, finish )*/
    {
        int p1 = _69inline_code_53665;
        int p2 = _code_53716;
        int p3 = _start_53717;
        int p4 = _finish_53718;
        struct replace_block replace_params;
        replace_params.copy_to   = &p1;
        replace_params.copy_from = &p2;
        replace_params.start     = &p3;
        replace_params.stop      = &p4;
        replace_params.target    = &_69inline_code_53665;
        Replace( &replace_params );
    }

    /** 	shift( start , length( code ) - (finish - start + 1), finish )*/
    if (IS_SEQUENCE(_code_53716)){
            _27930 = SEQ_PTR(_code_53716)->length;
    }
    else {
        _27930 = 1;
    }
    _27931 = _finish_53718 - _start_53717;
    if ((long)((unsigned long)_27931 +(unsigned long) HIGH_BITS) >= 0){
        _27931 = NewDouble((double)_27931);
    }
    if (IS_ATOM_INT(_27931)) {
        _27932 = _27931 + 1;
        if (_27932 > MAXINT){
            _27932 = NewDouble((double)_27932);
        }
    }
    else
    _27932 = binary_op(PLUS, 1, _27931);
    DeRef(_27931);
    _27931 = NOVALUE;
    if (IS_ATOM_INT(_27932)) {
        _27933 = _27930 - _27932;
        if ((long)((unsigned long)_27933 +(unsigned long) HIGH_BITS) >= 0){
            _27933 = NewDouble((double)_27933);
        }
    }
    else {
        _27933 = NewDouble((double)_27930 - DBL_PTR(_27932)->dbl);
    }
    _27930 = NOVALUE;
    DeRef(_27932);
    _27932 = NOVALUE;
    _69shift(_start_53717, _27933, _finish_53718);
    _27933 = NOVALUE;

    /** end procedure*/
    DeRefDS(_code_53716);
    return;
    ;
}


void _69defer()
{
    int _dx_53726 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer dx = find( inline_sub, deferred_inline_decisions )*/
    _dx_53726 = find_from(_69inline_sub_53679, _69deferred_inline_decisions_53681, 1);

    /** 	if not dx then*/
    if (_dx_53726 != 0)
    goto L1; // [14] 36

    /** 		deferred_inline_decisions &= inline_sub*/
    Append(&_69deferred_inline_decisions_53681, _69deferred_inline_decisions_53681, _69inline_sub_53679);

    /** 		deferred_inline_calls = append( deferred_inline_calls, {} )*/
    RefDS(_22663);
    Append(&_69deferred_inline_calls_53682, _69deferred_inline_calls_53682, _22663);
L1: 

    /** end procedure*/
    return;
    ;
}


int _69new_inline_temp(int _sym_53735)
{
    int _27939 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inline_temps &= sym*/
    Append(&_69inline_temps_53667, _69inline_temps_53667, _sym_53735);

    /** 	return length( inline_temps )*/
    if (IS_SEQUENCE(_69inline_temps_53667)){
            _27939 = SEQ_PTR(_69inline_temps_53667)->length;
    }
    else {
        _27939 = 1;
    }
    return _27939;
    ;
}


int _69get_inline_temp(int _sym_53741)
{
    int _temp_num_53742 = NOVALUE;
    int _27943 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer temp_num = find( sym, inline_params )*/
    _temp_num_53742 = find_from(_sym_53741, _69inline_params_53670, 1);

    /** 	if temp_num then*/
    if (_temp_num_53742 == 0)
    {
        goto L1; // [14] 24
    }
    else{
    }

    /** 		return temp_num*/
    return _temp_num_53742;
L1: 

    /** 	temp_num = find( sym, proc_vars )*/
    _temp_num_53742 = find_from(_sym_53741, _69proc_vars_53666, 1);

    /** 	if temp_num then*/
    if (_temp_num_53742 == 0)
    {
        goto L2; // [35] 45
    }
    else{
    }

    /** 		return temp_num*/
    return _temp_num_53742;
L2: 

    /** 	temp_num = find( sym, inline_temps )*/
    _temp_num_53742 = find_from(_sym_53741, _69inline_temps_53667, 1);

    /** 	if temp_num then*/
    if (_temp_num_53742 == 0)
    {
        goto L3; // [56] 66
    }
    else{
    }

    /** 		return temp_num*/
    return _temp_num_53742;
L3: 

    /** 	return new_inline_temp( sym )*/
    _27943 = _69new_inline_temp(_sym_53741);
    return _27943;
    ;
}


int _69generic_symbol(int _sym_53753)
{
    int _inline_type_53754 = NOVALUE;
    int _px_53755 = NOVALUE;
    int _eentry_53762 = NOVALUE;
    int _27952 = NOVALUE;
    int _27951 = NOVALUE;
    int _27950 = NOVALUE;
    int _27949 = NOVALUE;
    int _27947 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer px = find( sym, inline_params )*/
    _px_53755 = find_from(_sym_53753, _69inline_params_53670, 1);

    /** 	if px then*/
    if (_px_53755 == 0)
    {
        goto L1; // [14] 29
    }
    else{
    }

    /** 		inline_type = INLINE_PARAM*/
    _inline_type_53754 = 1;
    goto L2; // [26] 112
L1: 

    /** 		px = find( sym, proc_vars )*/
    _px_53755 = find_from(_sym_53753, _69proc_vars_53666, 1);

    /** 		if px then*/
    if (_px_53755 == 0)
    {
        goto L3; // [40] 55
    }
    else{
    }

    /** 			inline_type = INLINE_VAR*/
    _inline_type_53754 = 6;
    goto L4; // [52] 111
L3: 

    /** 			sequence eentry = SymTab[sym]*/
    DeRef(_eentry_53762);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _eentry_53762 = (int)*(((s1_ptr)_2)->base + _sym_53753);
    Ref(_eentry_53762);

    /** 			if is_literal( sym ) or eentry[S_SCOPE] > SC_PRIVATE then*/
    _27947 = _69is_literal(_sym_53753);
    if (IS_ATOM_INT(_27947)) {
        if (_27947 != 0) {
            goto L5; // [71] 92
        }
    }
    else {
        if (DBL_PTR(_27947)->dbl != 0.0) {
            goto L5; // [71] 92
        }
    }
    _2 = (int)SEQ_PTR(_eentry_53762);
    _27949 = (int)*(((s1_ptr)_2)->base + 4);
    if (IS_ATOM_INT(_27949)) {
        _27950 = (_27949 > 3);
    }
    else {
        _27950 = binary_op(GREATER, _27949, 3);
    }
    _27949 = NOVALUE;
    if (_27950 == 0) {
        DeRef(_27950);
        _27950 = NOVALUE;
        goto L6; // [88] 99
    }
    else {
        if (!IS_ATOM_INT(_27950) && DBL_PTR(_27950)->dbl == 0.0){
            DeRef(_27950);
            _27950 = NOVALUE;
            goto L6; // [88] 99
        }
        DeRef(_27950);
        _27950 = NOVALUE;
    }
    DeRef(_27950);
    _27950 = NOVALUE;
L5: 

    /** 				return sym*/
    DeRef(_eentry_53762);
    DeRef(_27947);
    _27947 = NOVALUE;
    return _sym_53753;
L6: 

    /** 			inline_type = INLINE_TEMP*/
    _inline_type_53754 = 2;
    DeRef(_eentry_53762);
    _eentry_53762 = NOVALUE;
L4: 
L2: 

    /** 	return { inline_type, get_inline_temp( sym ) }*/
    _27951 = _69get_inline_temp(_sym_53753);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _inline_type_53754;
    ((int *)_2)[2] = _27951;
    _27952 = MAKE_SEQ(_1);
    _27951 = NOVALUE;
    DeRef(_27947);
    _27947 = NOVALUE;
    return _27952;
    ;
}


int _69adjust_symbol(int _pc_53777)
{
    int _sym_53779 = NOVALUE;
    int _eentry_53785 = NOVALUE;
    int _27960 = NOVALUE;
    int _27958 = NOVALUE;
    int _27957 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_53777)) {
        _1 = (long)(DBL_PTR(_pc_53777)->dbl);
        if (UNIQUE(DBL_PTR(_pc_53777)) && (DBL_PTR(_pc_53777)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_53777);
        _pc_53777 = _1;
    }

    /** 	symtab_index sym = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _sym_53779 = (int)*(((s1_ptr)_2)->base + _pc_53777);
    if (!IS_ATOM_INT(_sym_53779)){
        _sym_53779 = (long)DBL_PTR(_sym_53779)->dbl;
    }

    /** 	if sym < 0 then*/
    if (_sym_53779 >= 0)
    goto L1; // [15] 28

    /** 		return 0*/
    DeRef(_eentry_53785);
    return 0;
    goto L2; // [25] 41
L1: 

    /** 	elsif not sym then*/
    if (_sym_53779 != 0)
    goto L3; // [30] 40

    /** 		return 1*/
    DeRef(_eentry_53785);
    return 1;
L3: 
L2: 

    /** 	sequence eentry = SymTab[sym]*/
    DeRef(_eentry_53785);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _eentry_53785 = (int)*(((s1_ptr)_2)->base + _sym_53779);
    Ref(_eentry_53785);

    /** 	if is_literal( sym ) then*/
    _27957 = _69is_literal(_sym_53779);
    if (_27957 == 0) {
        DeRef(_27957);
        _27957 = NOVALUE;
        goto L4; // [57] 69
    }
    else {
        if (!IS_ATOM_INT(_27957) && DBL_PTR(_27957)->dbl == 0.0){
            DeRef(_27957);
            _27957 = NOVALUE;
            goto L4; // [57] 69
        }
        DeRef(_27957);
        _27957 = NOVALUE;
    }
    DeRef(_27957);
    _27957 = NOVALUE;

    /** 		return 1*/
    DeRefDS(_eentry_53785);
    return 1;
    goto L5; // [66] 95
L4: 

    /** 	elsif eentry[S_SCOPE] = SC_UNDEFINED then*/
    _2 = (int)SEQ_PTR(_eentry_53785);
    _27958 = (int)*(((s1_ptr)_2)->base + 4);
    if (binary_op_a(NOTEQ, _27958, 9)){
        _27958 = NOVALUE;
        goto L6; // [79] 94
    }
    _27958 = NOVALUE;

    /** 		defer()*/
    _69defer();

    /** 		return 0*/
    DeRefDS(_eentry_53785);
    return 0;
L6: 
L5: 

    /** 	inline_code[pc] = generic_symbol( sym )*/
    _27960 = _69generic_symbol(_sym_53779);
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pc_53777);
    _1 = *(int *)_2;
    *(int *)_2 = _27960;
    if( _1 != _27960 ){
        DeRef(_1);
    }
    _27960 = NOVALUE;

    /** 	return 1*/
    DeRef(_eentry_53785);
    return 1;
    ;
}


int _69check_for_param(int _pc_53799)
{
    int _px_53800 = NOVALUE;
    int _27963 = NOVALUE;
    int _27961 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_53799)) {
        _1 = (long)(DBL_PTR(_pc_53799)->dbl);
        if (UNIQUE(DBL_PTR(_pc_53799)) && (DBL_PTR(_pc_53799)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_53799);
        _pc_53799 = _1;
    }

    /** 	integer px = find( inline_code[pc], inline_params )*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _27961 = (int)*(((s1_ptr)_2)->base + _pc_53799);
    _px_53800 = find_from(_27961, _69inline_params_53670, 1);
    _27961 = NOVALUE;

    /** 	if px then*/
    if (_px_53800 == 0)
    {
        goto L1; // [20] 51
    }
    else{
    }

    /** 		if not find( px, assigned_params ) then*/
    _27963 = find_from(_px_53800, _69assigned_params_53671, 1);
    if (_27963 != 0)
    goto L2; // [32] 44
    _27963 = NOVALUE;

    /** 			assigned_params &= px*/
    Append(&_69assigned_params_53671, _69assigned_params_53671, _px_53800);
L2: 

    /** 		return 1*/
    return 1;
L1: 

    /** 	return 0*/
    return 0;
    ;
}


void _69check_target(int _pc_53810, int _op_53811)
{
    int _targets_53812 = NOVALUE;
    int _27972 = NOVALUE;
    int _27971 = NOVALUE;
    int _27970 = NOVALUE;
    int _27969 = NOVALUE;
    int _27968 = NOVALUE;
    int _27966 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence targets = op_info[op][OP_TARGET]*/
    _2 = (int)SEQ_PTR(_67op_info_27424);
    _27966 = (int)*(((s1_ptr)_2)->base + _op_53811);
    DeRef(_targets_53812);
    _2 = (int)SEQ_PTR(_27966);
    _targets_53812 = (int)*(((s1_ptr)_2)->base + 4);
    Ref(_targets_53812);
    _27966 = NOVALUE;

    /** 	if length( targets ) then*/
    if (IS_SEQUENCE(_targets_53812)){
            _27968 = SEQ_PTR(_targets_53812)->length;
    }
    else {
        _27968 = 1;
    }
    if (_27968 == 0)
    {
        _27968 = NOVALUE;
        goto L1; // [26] 72
    }
    else{
        _27968 = NOVALUE;
    }

    /** 	for i = 1 to length( targets ) do*/
    if (IS_SEQUENCE(_targets_53812)){
            _27969 = SEQ_PTR(_targets_53812)->length;
    }
    else {
        _27969 = 1;
    }
    {
        int _i_53820;
        _i_53820 = 1;
L2: 
        if (_i_53820 > _27969){
            goto L3; // [34] 71
        }

        /** 			if check_for_param( pc + targets[i] ) then*/
        _2 = (int)SEQ_PTR(_targets_53812);
        _27970 = (int)*(((s1_ptr)_2)->base + _i_53820);
        if (IS_ATOM_INT(_27970)) {
            _27971 = _pc_53810 + _27970;
            if ((long)((unsigned long)_27971 + (unsigned long)HIGH_BITS) >= 0) 
            _27971 = NewDouble((double)_27971);
        }
        else {
            _27971 = binary_op(PLUS, _pc_53810, _27970);
        }
        _27970 = NOVALUE;
        _27972 = _69check_for_param(_27971);
        _27971 = NOVALUE;
        if (_27972 == 0) {
            DeRef(_27972);
            _27972 = NOVALUE;
            goto L4; // [55] 64
        }
        else {
            if (!IS_ATOM_INT(_27972) && DBL_PTR(_27972)->dbl == 0.0){
                DeRef(_27972);
                _27972 = NOVALUE;
                goto L4; // [55] 64
            }
            DeRef(_27972);
            _27972 = NOVALUE;
        }
        DeRef(_27972);
        _27972 = NOVALUE;

        /** 				return*/
        DeRefDS(_targets_53812);
        return;
L4: 

        /** 		end for*/
        _i_53820 = _i_53820 + 1;
        goto L2; // [66] 41
L3: 
        ;
    }
L1: 

    /** end procedure*/
    DeRef(_targets_53812);
    return;
    ;
}


int _69adjust_il(int _pc_53828, int _op_53829)
{
    int _addr_53837 = NOVALUE;
    int _sub_53843 = NOVALUE;
    int _27997 = NOVALUE;
    int _27996 = NOVALUE;
    int _27995 = NOVALUE;
    int _27994 = NOVALUE;
    int _27993 = NOVALUE;
    int _27992 = NOVALUE;
    int _27991 = NOVALUE;
    int _27990 = NOVALUE;
    int _27989 = NOVALUE;
    int _27988 = NOVALUE;
    int _27987 = NOVALUE;
    int _27986 = NOVALUE;
    int _27985 = NOVALUE;
    int _27984 = NOVALUE;
    int _27983 = NOVALUE;
    int _27982 = NOVALUE;
    int _27980 = NOVALUE;
    int _27979 = NOVALUE;
    int _27977 = NOVALUE;
    int _27976 = NOVALUE;
    int _27975 = NOVALUE;
    int _27974 = NOVALUE;
    int _27973 = NOVALUE;
    int _0, _1, _2;
    

    /** 	for i = 1 to op_info[op][OP_SIZE] - 1 do*/
    _2 = (int)SEQ_PTR(_67op_info_27424);
    _27973 = (int)*(((s1_ptr)_2)->base + _op_53829);
    _2 = (int)SEQ_PTR(_27973);
    _27974 = (int)*(((s1_ptr)_2)->base + 2);
    _27973 = NOVALUE;
    if (IS_ATOM_INT(_27974)) {
        _27975 = _27974 - 1;
        if ((long)((unsigned long)_27975 +(unsigned long) HIGH_BITS) >= 0){
            _27975 = NewDouble((double)_27975);
        }
    }
    else {
        _27975 = binary_op(MINUS, _27974, 1);
    }
    _27974 = NOVALUE;
    {
        int _i_53831;
        _i_53831 = 1;
L1: 
        if (binary_op_a(GREATER, _i_53831, _27975)){
            goto L2; // [23] 222
        }

        /** 		integer addr = find( i, op_info[op][OP_ADDR] )*/
        _2 = (int)SEQ_PTR(_67op_info_27424);
        _27976 = (int)*(((s1_ptr)_2)->base + _op_53829);
        _2 = (int)SEQ_PTR(_27976);
        _27977 = (int)*(((s1_ptr)_2)->base + 3);
        _27976 = NOVALUE;
        _addr_53837 = find_from(_i_53831, _27977, 1);
        _27977 = NOVALUE;

        /** 		integer sub  = find( i, op_info[op][OP_SUB] )*/
        _2 = (int)SEQ_PTR(_67op_info_27424);
        _27979 = (int)*(((s1_ptr)_2)->base + _op_53829);
        _2 = (int)SEQ_PTR(_27979);
        _27980 = (int)*(((s1_ptr)_2)->base + 5);
        _27979 = NOVALUE;
        _sub_53843 = find_from(_i_53831, _27980, 1);
        _27980 = NOVALUE;

        /** 		if addr then*/
        if (_addr_53837 == 0)
        {
            goto L3; // [70] 123
        }
        else{
        }

        /** 			if integer( inline_code[pc+i] ) then*/
        if (IS_ATOM_INT(_i_53831)) {
            _27982 = _pc_53828 + _i_53831;
        }
        else {
            _27982 = NewDouble((double)_pc_53828 + DBL_PTR(_i_53831)->dbl);
        }
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        if (!IS_ATOM_INT(_27982)){
            _27983 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27982)->dbl));
        }
        else{
            _27983 = (int)*(((s1_ptr)_2)->base + _27982);
        }
        if (IS_ATOM_INT(_27983))
        _27984 = 1;
        else if (IS_ATOM_DBL(_27983))
        _27984 = IS_ATOM_INT(DoubleToInt(_27983));
        else
        _27984 = 0;
        _27983 = NOVALUE;
        if (_27984 == 0)
        {
            _27984 = NOVALUE;
            goto L4; // [88] 213
        }
        else{
            _27984 = NOVALUE;
        }

        /** 				inline_code[pc + i] = { INLINE_ADDR, inline_code[pc + i] }*/
        if (IS_ATOM_INT(_i_53831)) {
            _27985 = _pc_53828 + _i_53831;
            if ((long)((unsigned long)_27985 + (unsigned long)HIGH_BITS) >= 0) 
            _27985 = NewDouble((double)_27985);
        }
        else {
            _27985 = NewDouble((double)_pc_53828 + DBL_PTR(_i_53831)->dbl);
        }
        if (IS_ATOM_INT(_i_53831)) {
            _27986 = _pc_53828 + _i_53831;
        }
        else {
            _27986 = NewDouble((double)_pc_53828 + DBL_PTR(_i_53831)->dbl);
        }
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        if (!IS_ATOM_INT(_27986)){
            _27987 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_27986)->dbl));
        }
        else{
            _27987 = (int)*(((s1_ptr)_2)->base + _27986);
        }
        Ref(_27987);
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = 4;
        ((int *)_2)[2] = _27987;
        _27988 = MAKE_SEQ(_1);
        _27987 = NOVALUE;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _69inline_code_53665 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_27985))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_27985)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _27985);
        _1 = *(int *)_2;
        *(int *)_2 = _27988;
        if( _1 != _27988 ){
            DeRef(_1);
        }
        _27988 = NOVALUE;
        goto L4; // [120] 213
L3: 

        /** 		elsif sub then*/
        if (_sub_53843 == 0)
        {
            goto L5; // [125] 149
        }
        else{
        }

        /** 			inline_code[pc+i] = {INLINE_SUB}*/
        if (IS_ATOM_INT(_i_53831)) {
            _27989 = _pc_53828 + _i_53831;
            if ((long)((unsigned long)_27989 + (unsigned long)HIGH_BITS) >= 0) 
            _27989 = NewDouble((double)_27989);
        }
        else {
            _27989 = NewDouble((double)_pc_53828 + DBL_PTR(_i_53831)->dbl);
        }
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 5;
        _27990 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _69inline_code_53665 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_27989))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_27989)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _27989);
        _1 = *(int *)_2;
        *(int *)_2 = _27990;
        if( _1 != _27990 ){
            DeRef(_1);
        }
        _27990 = NOVALUE;
        goto L4; // [146] 213
L5: 

        /** 			if op != STARTLINE and op != COVERAGE_LINE and op != COVERAGE_ROUTINE then*/
        _27991 = (_op_53829 != 58);
        if (_27991 == 0) {
            _27992 = 0;
            goto L6; // [157] 171
        }
        _27993 = (_op_53829 != 210);
        _27992 = (_27993 != 0);
L6: 
        if (_27992 == 0) {
            goto L7; // [171] 212
        }
        _27995 = (_op_53829 != 211);
        if (_27995 == 0)
        {
            DeRef(_27995);
            _27995 = NOVALUE;
            goto L7; // [182] 212
        }
        else{
            DeRef(_27995);
            _27995 = NOVALUE;
        }

        /** 				check_target( pc, op )*/
        _69check_target(_pc_53828, _op_53829);

        /** 				if not adjust_symbol( pc + i ) then*/
        if (IS_ATOM_INT(_i_53831)) {
            _27996 = _pc_53828 + _i_53831;
            if ((long)((unsigned long)_27996 + (unsigned long)HIGH_BITS) >= 0) 
            _27996 = NewDouble((double)_27996);
        }
        else {
            _27996 = NewDouble((double)_pc_53828 + DBL_PTR(_i_53831)->dbl);
        }
        _27997 = _69adjust_symbol(_27996);
        _27996 = NOVALUE;
        if (IS_ATOM_INT(_27997)) {
            if (_27997 != 0){
                DeRef(_27997);
                _27997 = NOVALUE;
                goto L8; // [201] 211
            }
        }
        else {
            if (DBL_PTR(_27997)->dbl != 0.0){
                DeRef(_27997);
                _27997 = NOVALUE;
                goto L8; // [201] 211
            }
        }
        DeRef(_27997);
        _27997 = NOVALUE;

        /** 					return 0*/
        DeRef(_i_53831);
        DeRef(_27982);
        _27982 = NOVALUE;
        DeRef(_27975);
        _27975 = NOVALUE;
        DeRef(_27985);
        _27985 = NOVALUE;
        DeRef(_27986);
        _27986 = NOVALUE;
        DeRef(_27989);
        _27989 = NOVALUE;
        DeRef(_27991);
        _27991 = NOVALUE;
        DeRef(_27993);
        _27993 = NOVALUE;
        return 0;
L8: 
L7: 
L4: 

        /** 	end for*/
        _0 = _i_53831;
        if (IS_ATOM_INT(_i_53831)) {
            _i_53831 = _i_53831 + 1;
            if ((long)((unsigned long)_i_53831 +(unsigned long) HIGH_BITS) >= 0){
                _i_53831 = NewDouble((double)_i_53831);
            }
        }
        else {
            _i_53831 = binary_op_a(PLUS, _i_53831, 1);
        }
        DeRef(_0);
        goto L1; // [217] 30
L2: 
        ;
        DeRef(_i_53831);
    }

    /** 	return 1*/
    DeRef(_27982);
    _27982 = NOVALUE;
    DeRef(_27975);
    _27975 = NOVALUE;
    DeRef(_27985);
    _27985 = NOVALUE;
    DeRef(_27986);
    _27986 = NOVALUE;
    DeRef(_27989);
    _27989 = NOVALUE;
    DeRef(_27991);
    _27991 = NOVALUE;
    DeRef(_27993);
    _27993 = NOVALUE;
    return 1;
    ;
}


int _69is_temp(int _sym_53878)
{
    int _28008 = NOVALUE;
    int _28007 = NOVALUE;
    int _28006 = NOVALUE;
    int _28005 = NOVALUE;
    int _28004 = NOVALUE;
    int _28003 = NOVALUE;
    int _28002 = NOVALUE;
    int _28001 = NOVALUE;
    int _28000 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sym <= 0 then*/
    if (_sym_53878 > 0)
    goto L1; // [5] 16

    /** 		return 0*/
    return 0;
L1: 

    /** 	return (SymTab[sym][S_MODE] = M_TEMP) and (not TRANSLATE or equal( NOVALUE, SymTab[sym][S_OBJ]) )*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28000 = (int)*(((s1_ptr)_2)->base + _sym_53878);
    _2 = (int)SEQ_PTR(_28000);
    _28001 = (int)*(((s1_ptr)_2)->base + 3);
    _28000 = NOVALUE;
    if (IS_ATOM_INT(_28001)) {
        _28002 = (_28001 == 3);
    }
    else {
        _28002 = binary_op(EQUALS, _28001, 3);
    }
    _28001 = NOVALUE;
    _28003 = (_38TRANSLATE_16564 == 0);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28004 = (int)*(((s1_ptr)_2)->base + _sym_53878);
    _2 = (int)SEQ_PTR(_28004);
    _28005 = (int)*(((s1_ptr)_2)->base + 1);
    _28004 = NOVALUE;
    if (_38NOVALUE_16800 == _28005)
    _28006 = 1;
    else if (IS_ATOM_INT(_38NOVALUE_16800) && IS_ATOM_INT(_28005))
    _28006 = 0;
    else
    _28006 = (compare(_38NOVALUE_16800, _28005) == 0);
    _28005 = NOVALUE;
    _28007 = (_28003 != 0 || _28006 != 0);
    _28003 = NOVALUE;
    _28006 = NOVALUE;
    if (IS_ATOM_INT(_28002)) {
        _28008 = (_28002 != 0 && _28007 != 0);
    }
    else {
        _28008 = binary_op(AND, _28002, _28007);
    }
    DeRef(_28002);
    _28002 = NOVALUE;
    _28007 = NOVALUE;
    return _28008;
    ;
}


int _69is_literal(int _sym_53900)
{
    int _mode_53903 = NOVALUE;
    int _28023 = NOVALUE;
    int _28022 = NOVALUE;
    int _28021 = NOVALUE;
    int _28020 = NOVALUE;
    int _28019 = NOVALUE;
    int _28018 = NOVALUE;
    int _28016 = NOVALUE;
    int _28015 = NOVALUE;
    int _28014 = NOVALUE;
    int _28013 = NOVALUE;
    int _28012 = NOVALUE;
    int _28010 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sym <= 0 then*/
    if (_sym_53900 > 0)
    goto L1; // [5] 16

    /** 		return 0*/
    return 0;
L1: 

    /** 	integer mode = SymTab[sym][S_MODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28010 = (int)*(((s1_ptr)_2)->base + _sym_53900);
    _2 = (int)SEQ_PTR(_28010);
    _mode_53903 = (int)*(((s1_ptr)_2)->base + 3);
    if (!IS_ATOM_INT(_mode_53903)){
        _mode_53903 = (long)DBL_PTR(_mode_53903)->dbl;
    }
    _28010 = NOVALUE;

    /** 	if (mode = M_CONSTANT and eu:compare( NOVALUE, SymTab[sym][S_OBJ]) ) */
    _28012 = (_mode_53903 == 2);
    if (_28012 == 0) {
        _28013 = 0;
        goto L2; // [40] 66
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28014 = (int)*(((s1_ptr)_2)->base + _sym_53900);
    _2 = (int)SEQ_PTR(_28014);
    _28015 = (int)*(((s1_ptr)_2)->base + 1);
    _28014 = NOVALUE;
    if (IS_ATOM_INT(_38NOVALUE_16800) && IS_ATOM_INT(_28015)){
        _28016 = (_38NOVALUE_16800 < _28015) ? -1 : (_38NOVALUE_16800 > _28015);
    }
    else{
        _28016 = compare(_38NOVALUE_16800, _28015);
    }
    _28015 = NOVALUE;
    _28013 = (_28016 != 0);
L2: 
    if (_28013 != 0) {
        goto L3; // [66] 117
    }
    if (_38TRANSLATE_16564 == 0) {
        _28018 = 0;
        goto L4; // [72] 86
    }
    _28019 = (_mode_53903 == 3);
    _28018 = (_28019 != 0);
L4: 
    if (_28018 == 0) {
        DeRef(_28020);
        _28020 = 0;
        goto L5; // [86] 112
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28021 = (int)*(((s1_ptr)_2)->base + _sym_53900);
    _2 = (int)SEQ_PTR(_28021);
    _28022 = (int)*(((s1_ptr)_2)->base + 1);
    _28021 = NOVALUE;
    if (IS_ATOM_INT(_28022) && IS_ATOM_INT(_38NOVALUE_16800)){
        _28023 = (_28022 < _38NOVALUE_16800) ? -1 : (_28022 > _38NOVALUE_16800);
    }
    else{
        _28023 = compare(_28022, _38NOVALUE_16800);
    }
    _28022 = NOVALUE;
    _28020 = (_28023 != 0);
L5: 
    if (_28020 == 0)
    {
        _28020 = NOVALUE;
        goto L6; // [113] 126
    }
    else{
        _28020 = NOVALUE;
    }
L3: 

    /** 		return 1*/
    DeRef(_28012);
    _28012 = NOVALUE;
    DeRef(_28019);
    _28019 = NOVALUE;
    return 1;
    goto L7; // [123] 133
L6: 

    /** 		return 0*/
    DeRef(_28012);
    _28012 = NOVALUE;
    DeRef(_28019);
    _28019 = NOVALUE;
    return 0;
L7: 
    ;
}


int _69returnf(int _pc_53950)
{
    int _retsym_53952 = NOVALUE;
    int _code_53985 = NOVALUE;
    int _ret_pc_53986 = NOVALUE;
    int _code_54033 = NOVALUE;
    int _ret_pc_54048 = NOVALUE;
    int _28099 = NOVALUE;
    int _28098 = NOVALUE;
    int _28096 = NOVALUE;
    int _28094 = NOVALUE;
    int _28093 = NOVALUE;
    int _28091 = NOVALUE;
    int _28090 = NOVALUE;
    int _28088 = NOVALUE;
    int _28087 = NOVALUE;
    int _28086 = NOVALUE;
    int _28084 = NOVALUE;
    int _28083 = NOVALUE;
    int _28082 = NOVALUE;
    int _28080 = NOVALUE;
    int _28078 = NOVALUE;
    int _28077 = NOVALUE;
    int _28075 = NOVALUE;
    int _28074 = NOVALUE;
    int _28072 = NOVALUE;
    int _28071 = NOVALUE;
    int _28070 = NOVALUE;
    int _28068 = NOVALUE;
    int _28067 = NOVALUE;
    int _28066 = NOVALUE;
    int _28065 = NOVALUE;
    int _28064 = NOVALUE;
    int _28063 = NOVALUE;
    int _28062 = NOVALUE;
    int _28061 = NOVALUE;
    int _28060 = NOVALUE;
    int _28059 = NOVALUE;
    int _28058 = NOVALUE;
    int _28057 = NOVALUE;
    int _28055 = NOVALUE;
    int _28053 = NOVALUE;
    int _28052 = NOVALUE;
    int _28051 = NOVALUE;
    int _28050 = NOVALUE;
    int _28049 = NOVALUE;
    int _28048 = NOVALUE;
    int _28047 = NOVALUE;
    int _28046 = NOVALUE;
    int _28045 = NOVALUE;
    int _28043 = NOVALUE;
    int _28042 = NOVALUE;
    int _28041 = NOVALUE;
    int _28039 = NOVALUE;
    int _28038 = NOVALUE;
    int _28037 = NOVALUE;
    int _28036 = NOVALUE;
    int _28035 = NOVALUE;
    int _28034 = NOVALUE;
    int _28032 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index retsym = inline_code[pc+3]*/
    _28032 = _pc_53950 + 3;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _retsym_53952 = (int)*(((s1_ptr)_2)->base + _28032);
    if (!IS_ATOM_INT(_retsym_53952)){
        _retsym_53952 = (long)DBL_PTR(_retsym_53952)->dbl;
    }

    /** 	if equal( inline_code[$], BADRETURNF ) then*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28034 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28034 = 1;
    }
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28035 = (int)*(((s1_ptr)_2)->base + _28034);
    if (_28035 == 43)
    _28036 = 1;
    else if (IS_ATOM_INT(_28035) && IS_ATOM_INT(43))
    _28036 = 0;
    else
    _28036 = (compare(_28035, 43) == 0);
    _28035 = NOVALUE;
    if (_28036 == 0)
    {
        _28036 = NOVALUE;
        goto L1; // [34] 102
    }
    else{
        _28036 = NOVALUE;
    }

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2; // [41] 60
    }
    else{
    }

    /** 			inline_code[$] = NOP1*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28037 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28037 = 1;
    }
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28037);
    _1 = *(int *)_2;
    *(int *)_2 = 159;
    DeRef(_1);
    goto L3; // [57] 101
L2: 

    /** 		elsif SymTab[inline_sub][S_TOKEN] = PROC then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28038 = (int)*(((s1_ptr)_2)->base + _69inline_sub_53679);
    _2 = (int)SEQ_PTR(_28038);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _28039 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _28039 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _28038 = NOVALUE;
    if (binary_op_a(NOTEQ, _28039, 27)){
        _28039 = NOVALUE;
        goto L4; // [78] 100
    }
    _28039 = NOVALUE;

    /** 			replace_code( {}, length(inline_code), length(inline_code) )*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28041 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28041 = 1;
    }
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28042 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28042 = 1;
    }
    RefDS(_22663);
    _69replace_code(_22663, _28041, _28042);
    _28041 = NOVALUE;
    _28042 = NOVALUE;
L4: 
L3: 
L1: 

    /** 	if is_temp( retsym ) */
    _28043 = _69is_temp(_retsym_53952);
    if (IS_ATOM_INT(_28043)) {
        if (_28043 != 0) {
            goto L5; // [108] 150
        }
    }
    else {
        if (DBL_PTR(_28043)->dbl != 0.0) {
            goto L5; // [108] 150
        }
    }
    _28045 = _69is_literal(_retsym_53952);
    if (IS_ATOM_INT(_28045)) {
        _28046 = (_28045 == 0);
    }
    else {
        _28046 = unary_op(NOT, _28045);
    }
    DeRef(_28045);
    _28045 = NOVALUE;
    if (IS_ATOM_INT(_28046)) {
        if (_28046 == 0) {
            DeRef(_28047);
            _28047 = 0;
            goto L6; // [119] 145
        }
    }
    else {
        if (DBL_PTR(_28046)->dbl == 0.0) {
            DeRef(_28047);
            _28047 = 0;
            goto L6; // [119] 145
        }
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28048 = (int)*(((s1_ptr)_2)->base + _retsym_53952);
    _2 = (int)SEQ_PTR(_28048);
    _28049 = (int)*(((s1_ptr)_2)->base + 4);
    _28048 = NOVALUE;
    if (IS_ATOM_INT(_28049)) {
        _28050 = (_28049 <= 3);
    }
    else {
        _28050 = binary_op(LESSEQ, _28049, 3);
    }
    _28049 = NOVALUE;
    DeRef(_28047);
    if (IS_ATOM_INT(_28050))
    _28047 = (_28050 != 0);
    else
    _28047 = DBL_PTR(_28050)->dbl != 0.0;
L6: 
    if (_28047 == 0)
    {
        _28047 = NOVALUE;
        goto L7; // [146] 415
    }
    else{
        _28047 = NOVALUE;
    }
L5: 

    /** 		sequence code = {}*/
    RefDS(_22663);
    DeRef(_code_53985);
    _code_53985 = _22663;

    /** 		integer ret_pc = 0*/
    _ret_pc_53986 = 0;

    /** 		if not (find( retsym, inline_params ) or find( retsym, proc_vars )) then*/
    _28051 = find_from(_retsym_53952, _69inline_params_53670, 1);
    if (_28051 != 0) {
        DeRef(_28052);
        _28052 = 1;
        goto L8; // [171] 186
    }
    _28053 = find_from(_retsym_53952, _69proc_vars_53666, 1);
    _28052 = (_28053 != 0);
L8: 
    if (_28052 != 0)
    goto L9; // [186] 206
    _28052 = NOVALUE;

    /** 			ret_pc = rfind( generic_symbol( retsym ), inline_code, pc )*/
    _28055 = _69generic_symbol(_retsym_53952);
    RefDS(_69inline_code_53665);
    _ret_pc_53986 = _12rfind(_28055, _69inline_code_53665, _pc_53950);
    _28055 = NOVALUE;
    if (!IS_ATOM_INT(_ret_pc_53986)) {
        _1 = (long)(DBL_PTR(_ret_pc_53986)->dbl);
        if (UNIQUE(DBL_PTR(_ret_pc_53986)) && (DBL_PTR(_ret_pc_53986)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ret_pc_53986);
        _ret_pc_53986 = _1;
    }
L9: 

    /** 		if ret_pc and eu:compare( inline_code[ret_pc-1], PRIVATE_INIT_CHECK ) then*/
    if (_ret_pc_53986 == 0) {
        goto LA; // [208] 289
    }
    _28058 = _ret_pc_53986 - 1;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28059 = (int)*(((s1_ptr)_2)->base + _28058);
    if (IS_ATOM_INT(_28059) && IS_ATOM_INT(30)){
        _28060 = (_28059 < 30) ? -1 : (_28059 > 30);
    }
    else{
        _28060 = compare(_28059, 30);
    }
    _28059 = NOVALUE;
    if (_28060 == 0)
    {
        _28060 = NOVALUE;
        goto LA; // [229] 289
    }
    else{
        _28060 = NOVALUE;
    }

    /** 			inline_code[ret_pc] = {INLINE_TARGET}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28061 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _ret_pc_53986);
    _1 = *(int *)_2;
    *(int *)_2 = _28061;
    if( _1 != _28061 ){
        DeRef(_1);
    }
    _28061 = NOVALUE;

    /** 			if equal( inline_code[ret_pc-1], REF_TEMP ) then*/
    _28062 = _ret_pc_53986 - 1;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28063 = (int)*(((s1_ptr)_2)->base + _28062);
    if (_28063 == 207)
    _28064 = 1;
    else if (IS_ATOM_INT(_28063) && IS_ATOM_INT(207))
    _28064 = 0;
    else
    _28064 = (compare(_28063, 207) == 0);
    _28063 = NOVALUE;
    if (_28064 == 0)
    {
        _28064 = NOVALUE;
        goto LB; // [264] 310
    }
    else{
        _28064 = NOVALUE;
    }

    /** 				inline_code[ret_pc-2] = {INLINE_TARGET}*/
    _28065 = _ret_pc_53986 - 2;
    if ((long)((unsigned long)_28065 +(unsigned long) HIGH_BITS) >= 0){
        _28065 = NewDouble((double)_28065);
    }
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28066 = MAKE_SEQ(_1);
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_28065))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28065)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _28065);
    _1 = *(int *)_2;
    *(int *)_2 = _28066;
    if( _1 != _28066 ){
        DeRef(_1);
    }
    _28066 = NOVALUE;
    goto LB; // [286] 310
LA: 

    /** 			code = {ASSIGN, generic_symbol( retsym ), {INLINE_TARGET}}*/
    _28067 = _69generic_symbol(_retsym_53952);
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28068 = MAKE_SEQ(_1);
    _0 = _code_53985;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    *((int *)(_2+8)) = _28067;
    *((int *)(_2+12)) = _28068;
    _code_53985 = MAKE_SEQ(_1);
    DeRef(_0);
    _28068 = NOVALUE;
    _28067 = NOVALUE;
LB: 

    /** 		if pc != length( inline_code ) - ( 3 + TRANSLATE ) then*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28070 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28070 = 1;
    }
    _28071 = 3 + _38TRANSLATE_16564;
    _28072 = _28070 - _28071;
    _28070 = NOVALUE;
    _28071 = NOVALUE;
    if (_pc_53950 == _28072)
    goto LC; // [327] 350

    /** 			code &= { ELSE, {INLINE_ADDR, -1 }}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28074 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 23;
    ((int *)_2)[2] = _28074;
    _28075 = MAKE_SEQ(_1);
    _28074 = NOVALUE;
    Concat((object_ptr)&_code_53985, _code_53985, _28075);
    DeRefDS(_28075);
    _28075 = NOVALUE;
LC: 

    /** 		replace_code( code, pc, pc + 3 )*/
    _28077 = _pc_53950 + 3;
    if ((long)((unsigned long)_28077 + (unsigned long)HIGH_BITS) >= 0) 
    _28077 = NewDouble((double)_28077);
    RefDS(_code_53985);
    _69replace_code(_code_53985, _pc_53950, _28077);
    _28077 = NOVALUE;

    /** 		ret_pc = find( { INLINE_ADDR, -1 }, inline_code, pc )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28078 = MAKE_SEQ(_1);
    _ret_pc_53986 = find_from(_28078, _69inline_code_53665, _pc_53950);
    DeRefDS(_28078);
    _28078 = NOVALUE;

    /** 		if ret_pc then*/
    if (_ret_pc_53986 == 0)
    {
        goto LD; // [378] 404
    }
    else{
    }

    /** 			inline_code[ret_pc][2] = length(inline_code) + 1*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ret_pc_53986 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28082 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28082 = 1;
    }
    _28083 = _28082 + 1;
    _28082 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _28083;
    if( _1 != _28083 ){
        DeRef(_1);
    }
    _28083 = NOVALUE;
    _28080 = NOVALUE;
LD: 

    /** 		return 1*/
    DeRef(_code_53985);
    DeRef(_28032);
    _28032 = NOVALUE;
    DeRef(_28043);
    _28043 = NOVALUE;
    DeRef(_28046);
    _28046 = NOVALUE;
    DeRef(_28058);
    _28058 = NOVALUE;
    DeRef(_28050);
    _28050 = NOVALUE;
    DeRef(_28062);
    _28062 = NOVALUE;
    DeRef(_28065);
    _28065 = NOVALUE;
    DeRef(_28072);
    _28072 = NOVALUE;
    return 1;
    goto LE; // [412] 534
L7: 

    /** 		sequence code = {ASSIGN, retsym, {INLINE_TARGET}}*/
    _1 = NewS1(1);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 3;
    _28084 = MAKE_SEQ(_1);
    _0 = _code_54033;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    *((int *)(_2+8)) = _retsym_53952;
    *((int *)(_2+12)) = _28084;
    _code_54033 = MAKE_SEQ(_1);
    DeRef(_0);
    _28084 = NOVALUE;

    /** 		if pc != length( inline_code ) - ( 3 + TRANSLATE ) then*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28086 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28086 = 1;
    }
    _28087 = 3 + _38TRANSLATE_16564;
    _28088 = _28086 - _28087;
    _28086 = NOVALUE;
    _28087 = NOVALUE;
    if (_pc_53950 == _28088)
    goto LF; // [448] 471

    /** 			code &= { ELSE, {INLINE_ADDR, -1 }}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28090 = MAKE_SEQ(_1);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 23;
    ((int *)_2)[2] = _28090;
    _28091 = MAKE_SEQ(_1);
    _28090 = NOVALUE;
    Concat((object_ptr)&_code_54033, _code_54033, _28091);
    DeRefDS(_28091);
    _28091 = NOVALUE;
LF: 

    /** 		replace_code( code, pc, pc + 3 )*/
    _28093 = _pc_53950 + 3;
    if ((long)((unsigned long)_28093 + (unsigned long)HIGH_BITS) >= 0) 
    _28093 = NewDouble((double)_28093);
    RefDS(_code_54033);
    _69replace_code(_code_54033, _pc_53950, _28093);
    _28093 = NOVALUE;

    /** 		integer ret_pc = find( { INLINE_ADDR, -1 }, inline_code, pc )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = -1;
    _28094 = MAKE_SEQ(_1);
    _ret_pc_54048 = find_from(_28094, _69inline_code_53665, _pc_53950);
    DeRefDS(_28094);
    _28094 = NOVALUE;

    /** 		if ret_pc then*/
    if (_ret_pc_54048 == 0)
    {
        goto L10; // [499] 525
    }
    else{
    }

    /** 			inline_code[ret_pc][2] = length(inline_code) + 1*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _3 = (int)(_ret_pc_54048 + ((s1_ptr)_2)->base);
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28098 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28098 = 1;
    }
    _28099 = _28098 + 1;
    _28098 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _28099;
    if( _1 != _28099 ){
        DeRef(_1);
    }
    _28099 = NOVALUE;
    _28096 = NOVALUE;
L10: 

    /** 		return 1*/
    DeRef(_code_54033);
    DeRef(_28032);
    _28032 = NOVALUE;
    DeRef(_28043);
    _28043 = NOVALUE;
    DeRef(_28046);
    _28046 = NOVALUE;
    DeRef(_28058);
    _28058 = NOVALUE;
    DeRef(_28050);
    _28050 = NOVALUE;
    DeRef(_28062);
    _28062 = NOVALUE;
    DeRef(_28065);
    _28065 = NOVALUE;
    DeRef(_28072);
    _28072 = NOVALUE;
    DeRef(_28088);
    _28088 = NOVALUE;
    return 1;
LE: 

    /** 	return 0*/
    DeRef(_28032);
    _28032 = NOVALUE;
    DeRef(_28043);
    _28043 = NOVALUE;
    DeRef(_28046);
    _28046 = NOVALUE;
    DeRef(_28058);
    _28058 = NOVALUE;
    DeRef(_28050);
    _28050 = NOVALUE;
    DeRef(_28062);
    _28062 = NOVALUE;
    DeRef(_28065);
    _28065 = NOVALUE;
    DeRef(_28072);
    _28072 = NOVALUE;
    DeRef(_28088);
    _28088 = NOVALUE;
    return 0;
    ;
}


int _69inline_op(int _pc_54058)
{
    int _op_54059 = NOVALUE;
    int _code_54064 = NOVALUE;
    int _stlen_54097 = NOVALUE;
    int _file_54102 = NOVALUE;
    int _ok_54107 = NOVALUE;
    int _original_table_54130 = NOVALUE;
    int _jump_table_54134 = NOVALUE;
    int _28161 = NOVALUE;
    int _28160 = NOVALUE;
    int _28159 = NOVALUE;
    int _28158 = NOVALUE;
    int _28157 = NOVALUE;
    int _28156 = NOVALUE;
    int _28155 = NOVALUE;
    int _28154 = NOVALUE;
    int _28153 = NOVALUE;
    int _28152 = NOVALUE;
    int _28151 = NOVALUE;
    int _28150 = NOVALUE;
    int _28149 = NOVALUE;
    int _28146 = NOVALUE;
    int _28145 = NOVALUE;
    int _28144 = NOVALUE;
    int _28143 = NOVALUE;
    int _28141 = NOVALUE;
    int _28139 = NOVALUE;
    int _28138 = NOVALUE;
    int _28136 = NOVALUE;
    int _28132 = NOVALUE;
    int _28131 = NOVALUE;
    int _28130 = NOVALUE;
    int _28129 = NOVALUE;
    int _28128 = NOVALUE;
    int _28127 = NOVALUE;
    int _28124 = NOVALUE;
    int _28123 = NOVALUE;
    int _28121 = NOVALUE;
    int _28120 = NOVALUE;
    int _28118 = NOVALUE;
    int _28116 = NOVALUE;
    int _28115 = NOVALUE;
    int _28114 = NOVALUE;
    int _28113 = NOVALUE;
    int _28112 = NOVALUE;
    int _28111 = NOVALUE;
    int _28110 = NOVALUE;
    int _28108 = NOVALUE;
    int _28107 = NOVALUE;
    int _28106 = NOVALUE;
    int _28104 = NOVALUE;
    int _28103 = NOVALUE;
    int _28102 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	integer op = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _op_54059 = (int)*(((s1_ptr)_2)->base + _pc_54058);
    if (!IS_ATOM_INT(_op_54059))
    _op_54059 = (long)DBL_PTR(_op_54059)->dbl;

    /** 	if op = RETURNP then*/
    if (_op_54059 != 29)
    goto L1; // [15] 152

    /** 		sequence code = ""*/
    RefDS(_22663);
    DeRef(_code_54064);
    _code_54064 = _22663;

    /** 		if pc != length( inline_code ) - 1 - TRANSLATE then*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28102 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28102 = 1;
    }
    _28103 = _28102 - 1;
    _28102 = NOVALUE;
    _28104 = _28103 - _38TRANSLATE_16564;
    _28103 = NOVALUE;
    if (_pc_54058 == _28104)
    goto L2; // [43] 94

    /** 			code = { ELSE, {INLINE_ADDR, length( inline_code ) + 1 }}*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28106 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28106 = 1;
    }
    _28107 = _28106 + 1;
    _28106 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 4;
    ((int *)_2)[2] = _28107;
    _28108 = MAKE_SEQ(_1);
    _28107 = NOVALUE;
    DeRefDS(_code_54064);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 23;
    ((int *)_2)[2] = _28108;
    _code_54064 = MAKE_SEQ(_1);
    _28108 = NOVALUE;

    /** 			if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L3; // [74] 136
    }
    else{
    }

    /** 				inline_code[$] = NOP1*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28110 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28110 = 1;
    }
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28110);
    _1 = *(int *)_2;
    *(int *)_2 = 159;
    DeRef(_1);
    goto L3; // [91] 136
L2: 

    /** 		elsif TRANSLATE and inline_code[$] = BADRETURNF then*/
    if (_38TRANSLATE_16564 == 0) {
        goto L4; // [98] 135
    }
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28112 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28112 = 1;
    }
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28113 = (int)*(((s1_ptr)_2)->base + _28112);
    if (IS_ATOM_INT(_28113)) {
        _28114 = (_28113 == 43);
    }
    else {
        _28114 = binary_op(EQUALS, _28113, 43);
    }
    _28113 = NOVALUE;
    if (_28114 == 0) {
        DeRef(_28114);
        _28114 = NOVALUE;
        goto L4; // [118] 135
    }
    else {
        if (!IS_ATOM_INT(_28114) && DBL_PTR(_28114)->dbl == 0.0){
            DeRef(_28114);
            _28114 = NOVALUE;
            goto L4; // [118] 135
        }
        DeRef(_28114);
        _28114 = NOVALUE;
    }
    DeRef(_28114);
    _28114 = NOVALUE;

    /** 			inline_code[$] = NOP1*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28115 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28115 = 1;
    }
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28115);
    _1 = *(int *)_2;
    *(int *)_2 = 159;
    DeRef(_1);
L4: 
L3: 

    /** 		replace_code( code, pc, pc + 2 )*/
    _28116 = _pc_54058 + 2;
    if ((long)((unsigned long)_28116 + (unsigned long)HIGH_BITS) >= 0) 
    _28116 = NewDouble((double)_28116);
    RefDS(_code_54064);
    _69replace_code(_code_54064, _pc_54058, _28116);
    _28116 = NOVALUE;
    DeRefDS(_code_54064);
    _code_54064 = NOVALUE;
    goto L5; // [149] 534
L1: 

    /** 	elsif op = RETURNF then*/
    if (_op_54059 != 28)
    goto L6; // [156] 173

    /** 		return returnf( pc )*/
    _28118 = _69returnf(_pc_54058);
    DeRef(_28104);
    _28104 = NOVALUE;
    return _28118;
    goto L5; // [170] 534
L6: 

    /** 	elsif op = ROUTINE_ID then*/
    if (_op_54059 != 134)
    goto L7; // [177] 275

    /** 		integer*/

    /** 			stlen = inline_code[pc+2+TRANSLATE],*/
    _28120 = _pc_54058 + 2;
    if ((long)((unsigned long)_28120 + (unsigned long)HIGH_BITS) >= 0) 
    _28120 = NewDouble((double)_28120);
    if (IS_ATOM_INT(_28120)) {
        _28121 = _28120 + _38TRANSLATE_16564;
    }
    else {
        _28121 = NewDouble(DBL_PTR(_28120)->dbl + (double)_38TRANSLATE_16564);
    }
    DeRef(_28120);
    _28120 = NOVALUE;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!IS_ATOM_INT(_28121)){
        _stlen_54097 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28121)->dbl));
    }
    else{
        _stlen_54097 = (int)*(((s1_ptr)_2)->base + _28121);
    }
    if (!IS_ATOM_INT(_stlen_54097))
    _stlen_54097 = (long)DBL_PTR(_stlen_54097)->dbl;

    /** 			file  = inline_code[pc+4+TRANSLATE],*/
    _28123 = _pc_54058 + 4;
    if ((long)((unsigned long)_28123 + (unsigned long)HIGH_BITS) >= 0) 
    _28123 = NewDouble((double)_28123);
    if (IS_ATOM_INT(_28123)) {
        _28124 = _28123 + _38TRANSLATE_16564;
    }
    else {
        _28124 = NewDouble(DBL_PTR(_28123)->dbl + (double)_38TRANSLATE_16564);
    }
    DeRef(_28123);
    _28123 = NOVALUE;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!IS_ATOM_INT(_28124)){
        _file_54102 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28124)->dbl));
    }
    else{
        _file_54102 = (int)*(((s1_ptr)_2)->base + _28124);
    }
    if (!IS_ATOM_INT(_file_54102))
    _file_54102 = (long)DBL_PTR(_file_54102)->dbl;

    /** 			ok    = adjust_il( pc, op )*/
    _ok_54107 = _69adjust_il(_pc_54058, _op_54059);
    if (!IS_ATOM_INT(_ok_54107)) {
        _1 = (long)(DBL_PTR(_ok_54107)->dbl);
        if (UNIQUE(DBL_PTR(_ok_54107)) && (DBL_PTR(_ok_54107)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ok_54107);
        _ok_54107 = _1;
    }

    /** 		inline_code[pc+2+TRANSLATE] = stlen*/
    _28127 = _pc_54058 + 2;
    if ((long)((unsigned long)_28127 + (unsigned long)HIGH_BITS) >= 0) 
    _28127 = NewDouble((double)_28127);
    if (IS_ATOM_INT(_28127)) {
        _28128 = _28127 + _38TRANSLATE_16564;
    }
    else {
        _28128 = NewDouble(DBL_PTR(_28127)->dbl + (double)_38TRANSLATE_16564);
    }
    DeRef(_28127);
    _28127 = NOVALUE;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_28128))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28128)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _28128);
    _1 = *(int *)_2;
    *(int *)_2 = _stlen_54097;
    DeRef(_1);

    /** 		inline_code[pc+4+TRANSLATE] = file*/
    _28129 = _pc_54058 + 4;
    if ((long)((unsigned long)_28129 + (unsigned long)HIGH_BITS) >= 0) 
    _28129 = NewDouble((double)_28129);
    if (IS_ATOM_INT(_28129)) {
        _28130 = _28129 + _38TRANSLATE_16564;
    }
    else {
        _28130 = NewDouble(DBL_PTR(_28129)->dbl + (double)_38TRANSLATE_16564);
    }
    DeRef(_28129);
    _28129 = NOVALUE;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_28130))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28130)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _28130);
    _1 = *(int *)_2;
    *(int *)_2 = _file_54102;
    DeRef(_1);

    /** 		return ok*/
    DeRef(_28118);
    _28118 = NOVALUE;
    DeRef(_28104);
    _28104 = NOVALUE;
    DeRef(_28121);
    _28121 = NOVALUE;
    DeRef(_28124);
    _28124 = NOVALUE;
    DeRef(_28128);
    _28128 = NOVALUE;
    DeRef(_28130);
    _28130 = NOVALUE;
    return _ok_54107;
    goto L5; // [272] 534
L7: 

    /** 	elsif op_info[op][OP_SIZE_TYPE] = FIXED_SIZE then*/
    _2 = (int)SEQ_PTR(_67op_info_27424);
    _28131 = (int)*(((s1_ptr)_2)->base + _op_54059);
    _2 = (int)SEQ_PTR(_28131);
    _28132 = (int)*(((s1_ptr)_2)->base + 1);
    _28131 = NOVALUE;
    if (binary_op_a(NOTEQ, _28132, 1)){
        _28132 = NOVALUE;
        goto L8; // [291] 399
    }
    _28132 = NOVALUE;

    /** 		switch op do*/
    _0 = _op_54059;
    switch ( _0 ){ 

        /** 			case SWITCH, SWITCH_RT, SWITCH_I, SWITCH_SPI then*/
        case 185:
        case 202:
        case 193:
        case 192:

        /** 				symtab_index original_table = inline_code[pc + 3]*/
        _28136 = _pc_54058 + 3;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _original_table_54130 = (int)*(((s1_ptr)_2)->base + _28136);
        if (!IS_ATOM_INT(_original_table_54130)){
            _original_table_54130 = (long)DBL_PTR(_original_table_54130)->dbl;
        }

        /** 				symtab_index jump_table = NewStringSym( {-2, length(SymTab) } )*/
        if (IS_SEQUENCE(_35SymTab_15595)){
                _28138 = SEQ_PTR(_35SymTab_15595)->length;
        }
        else {
            _28138 = 1;
        }
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = _28138;
        _28139 = MAKE_SEQ(_1);
        _28138 = NOVALUE;
        _jump_table_54134 = _55NewStringSym(_28139);
        _28139 = NOVALUE;
        if (!IS_ATOM_INT(_jump_table_54134)) {
            _1 = (long)(DBL_PTR(_jump_table_54134)->dbl);
            if (UNIQUE(DBL_PTR(_jump_table_54134)) && (DBL_PTR(_jump_table_54134)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_jump_table_54134);
            _jump_table_54134 = _1;
        }

        /** 				SymTab[jump_table][S_OBJ] = SymTab[original_table][S_OBJ]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _35SymTab_15595 = MAKE_SEQ(_2);
        }
        _3 = (int)(_jump_table_54134 + ((s1_ptr)_2)->base);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28143 = (int)*(((s1_ptr)_2)->base + _original_table_54130);
        _2 = (int)SEQ_PTR(_28143);
        _28144 = (int)*(((s1_ptr)_2)->base + 1);
        _28143 = NOVALUE;
        Ref(_28144);
        _2 = (int)SEQ_PTR(*(int *)_3);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            *(int *)_3 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + 1);
        _1 = *(int *)_2;
        *(int *)_2 = _28144;
        if( _1 != _28144 ){
            DeRef(_1);
        }
        _28144 = NOVALUE;
        _28141 = NOVALUE;

        /** 				inline_code[pc+3] = jump_table*/
        _28145 = _pc_54058 + 3;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _69inline_code_53665 = MAKE_SEQ(_2);
        }
        _2 = (int)(((s1_ptr)_2)->base + _28145);
        _1 = *(int *)_2;
        *(int *)_2 = _jump_table_54134;
        DeRef(_1);
    ;}
    /** 		return adjust_il( pc, op )*/
    _28146 = _69adjust_il(_pc_54058, _op_54059);
    DeRef(_28118);
    _28118 = NOVALUE;
    DeRef(_28104);
    _28104 = NOVALUE;
    DeRef(_28136);
    _28136 = NOVALUE;
    DeRef(_28121);
    _28121 = NOVALUE;
    DeRef(_28124);
    _28124 = NOVALUE;
    DeRef(_28128);
    _28128 = NOVALUE;
    DeRef(_28130);
    _28130 = NOVALUE;
    DeRef(_28145);
    _28145 = NOVALUE;
    return _28146;
    goto L5; // [396] 534
L8: 

    /** 		switch op with fallthru do*/
    _0 = _op_54059;
    switch ( _0 ){ 

        /** 			case REF_TEMP then*/
        case 207:

        /** 				inline_code[pc+1] = {INLINE_TARGET}*/
        _28149 = _pc_54058 + 1;
        if (_28149 > MAXINT){
            _28149 = NewDouble((double)_28149);
        }
        _1 = NewS1(1);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 3;
        _28150 = MAKE_SEQ(_1);
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _69inline_code_53665 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_28149))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_28149)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _28149);
        _1 = *(int *)_2;
        *(int *)_2 = _28150;
        if( _1 != _28150 ){
            DeRef(_1);
        }
        _28150 = NOVALUE;

        /** 			case CONCAT_N then*/
        case 157:
        case 31:

        /** 				if check_for_param( pc + 2 + inline_code[pc+1] ) then*/
        _28151 = _pc_54058 + 2;
        if ((long)((unsigned long)_28151 + (unsigned long)HIGH_BITS) >= 0) 
        _28151 = NewDouble((double)_28151);
        _28152 = _pc_54058 + 1;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28153 = (int)*(((s1_ptr)_2)->base + _28152);
        if (IS_ATOM_INT(_28151) && IS_ATOM_INT(_28153)) {
            _28154 = _28151 + _28153;
            if ((long)((unsigned long)_28154 + (unsigned long)HIGH_BITS) >= 0) 
            _28154 = NewDouble((double)_28154);
        }
        else {
            _28154 = binary_op(PLUS, _28151, _28153);
        }
        DeRef(_28151);
        _28151 = NOVALUE;
        _28153 = NOVALUE;
        _28155 = _69check_for_param(_28154);
        _28154 = NOVALUE;
        if (_28155 == 0) {
            DeRef(_28155);
            _28155 = NOVALUE;
            goto L9; // [458] 462
        }
        else {
            if (!IS_ATOM_INT(_28155) && DBL_PTR(_28155)->dbl == 0.0){
                DeRef(_28155);
                _28155 = NOVALUE;
                goto L9; // [458] 462
            }
            DeRef(_28155);
            _28155 = NOVALUE;
        }
        DeRef(_28155);
        _28155 = NOVALUE;
L9: 

        /** 				for i = pc + 2 to pc + 2 + inline_code[pc+1] do*/
        _28156 = _pc_54058 + 2;
        if ((long)((unsigned long)_28156 + (unsigned long)HIGH_BITS) >= 0) 
        _28156 = NewDouble((double)_28156);
        _28157 = _pc_54058 + 2;
        if ((long)((unsigned long)_28157 + (unsigned long)HIGH_BITS) >= 0) 
        _28157 = NewDouble((double)_28157);
        _28158 = _pc_54058 + 1;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28159 = (int)*(((s1_ptr)_2)->base + _28158);
        if (IS_ATOM_INT(_28157) && IS_ATOM_INT(_28159)) {
            _28160 = _28157 + _28159;
            if ((long)((unsigned long)_28160 + (unsigned long)HIGH_BITS) >= 0) 
            _28160 = NewDouble((double)_28160);
        }
        else {
            _28160 = binary_op(PLUS, _28157, _28159);
        }
        DeRef(_28157);
        _28157 = NOVALUE;
        _28159 = NOVALUE;
        {
            int _i_54167;
            Ref(_28156);
            _i_54167 = _28156;
LA: 
            if (binary_op_a(GREATER, _i_54167, _28160)){
                goto LB; // [486] 516
            }

            /** 					if not adjust_symbol( i ) then*/
            Ref(_i_54167);
            _28161 = _69adjust_symbol(_i_54167);
            if (IS_ATOM_INT(_28161)) {
                if (_28161 != 0){
                    DeRef(_28161);
                    _28161 = NOVALUE;
                    goto LC; // [499] 509
                }
            }
            else {
                if (DBL_PTR(_28161)->dbl != 0.0){
                    DeRef(_28161);
                    _28161 = NOVALUE;
                    goto LC; // [499] 509
                }
            }
            DeRef(_28161);
            _28161 = NOVALUE;

            /** 						return 0*/
            DeRef(_i_54167);
            DeRef(_28118);
            _28118 = NOVALUE;
            DeRef(_28104);
            _28104 = NOVALUE;
            DeRef(_28136);
            _28136 = NOVALUE;
            DeRef(_28121);
            _28121 = NOVALUE;
            DeRef(_28124);
            _28124 = NOVALUE;
            DeRef(_28128);
            _28128 = NOVALUE;
            DeRef(_28130);
            _28130 = NOVALUE;
            DeRef(_28145);
            _28145 = NOVALUE;
            DeRef(_28146);
            _28146 = NOVALUE;
            DeRef(_28149);
            _28149 = NOVALUE;
            DeRef(_28156);
            _28156 = NOVALUE;
            DeRef(_28152);
            _28152 = NOVALUE;
            DeRef(_28158);
            _28158 = NOVALUE;
            DeRef(_28160);
            _28160 = NOVALUE;
            return 0;
LC: 

            /** 				end for*/
            _0 = _i_54167;
            if (IS_ATOM_INT(_i_54167)) {
                _i_54167 = _i_54167 + 1;
                if ((long)((unsigned long)_i_54167 +(unsigned long) HIGH_BITS) >= 0){
                    _i_54167 = NewDouble((double)_i_54167);
                }
            }
            else {
                _i_54167 = binary_op_a(PLUS, _i_54167, 1);
            }
            DeRef(_0);
            goto LA; // [511] 493
LB: 
            ;
            DeRef(_i_54167);
        }

        /** 				return 1*/
        DeRef(_28118);
        _28118 = NOVALUE;
        DeRef(_28104);
        _28104 = NOVALUE;
        DeRef(_28136);
        _28136 = NOVALUE;
        DeRef(_28121);
        _28121 = NOVALUE;
        DeRef(_28124);
        _28124 = NOVALUE;
        DeRef(_28128);
        _28128 = NOVALUE;
        DeRef(_28130);
        _28130 = NOVALUE;
        DeRef(_28145);
        _28145 = NOVALUE;
        DeRef(_28146);
        _28146 = NOVALUE;
        DeRef(_28149);
        _28149 = NOVALUE;
        DeRef(_28156);
        _28156 = NOVALUE;
        DeRef(_28152);
        _28152 = NOVALUE;
        DeRef(_28158);
        _28158 = NOVALUE;
        DeRef(_28160);
        _28160 = NOVALUE;
        return 1;

        /** 			case else*/
        default:

        /** 				return 0*/
        DeRef(_28118);
        _28118 = NOVALUE;
        DeRef(_28104);
        _28104 = NOVALUE;
        DeRef(_28136);
        _28136 = NOVALUE;
        DeRef(_28121);
        _28121 = NOVALUE;
        DeRef(_28124);
        _28124 = NOVALUE;
        DeRef(_28128);
        _28128 = NOVALUE;
        DeRef(_28130);
        _28130 = NOVALUE;
        DeRef(_28145);
        _28145 = NOVALUE;
        DeRef(_28146);
        _28146 = NOVALUE;
        DeRef(_28149);
        _28149 = NOVALUE;
        DeRef(_28156);
        _28156 = NOVALUE;
        DeRef(_28152);
        _28152 = NOVALUE;
        DeRef(_28158);
        _28158 = NOVALUE;
        DeRef(_28160);
        _28160 = NOVALUE;
        return 0;
    ;}L5: 

    /** 	return 1*/
    DeRef(_28118);
    _28118 = NOVALUE;
    DeRef(_28104);
    _28104 = NOVALUE;
    DeRef(_28136);
    _28136 = NOVALUE;
    DeRef(_28121);
    _28121 = NOVALUE;
    DeRef(_28124);
    _28124 = NOVALUE;
    DeRef(_28128);
    _28128 = NOVALUE;
    DeRef(_28130);
    _28130 = NOVALUE;
    DeRef(_28145);
    _28145 = NOVALUE;
    DeRef(_28146);
    _28146 = NOVALUE;
    DeRef(_28149);
    _28149 = NOVALUE;
    DeRef(_28156);
    _28156 = NOVALUE;
    DeRef(_28152);
    _28152 = NOVALUE;
    DeRef(_28158);
    _28158 = NOVALUE;
    DeRef(_28160);
    _28160 = NOVALUE;
    return 1;
    ;
}


void _69restore_code()
{
    int _28163 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if length( temp_code ) then*/
    if (IS_SEQUENCE(_69temp_code_54177)){
            _28163 = SEQ_PTR(_69temp_code_54177)->length;
    }
    else {
        _28163 = 1;
    }
    if (_28163 == 0)
    {
        _28163 = NOVALUE;
        goto L1; // [8] 21
    }
    else{
        _28163 = NOVALUE;
    }

    /** 		Code = temp_code*/
    RefDS(_69temp_code_54177);
    DeRef(_38Code_17038);
    _38Code_17038 = _69temp_code_54177;
L1: 

    /** end procedure*/
    return;
    ;
}


void _69check_inline(int _sub_54186)
{
    int _pc_54215 = NOVALUE;
    int _s_54217 = NOVALUE;
    int _backpatch_op_54255 = NOVALUE;
    int _op_54259 = NOVALUE;
    int _rtn_idx_54270 = NOVALUE;
    int _args_54275 = NOVALUE;
    int _args_54307 = NOVALUE;
    int _values_54336 = NOVALUE;
    int _28250 = NOVALUE;
    int _28249 = NOVALUE;
    int _28247 = NOVALUE;
    int _28244 = NOVALUE;
    int _28242 = NOVALUE;
    int _28241 = NOVALUE;
    int _28240 = NOVALUE;
    int _28238 = NOVALUE;
    int _28237 = NOVALUE;
    int _28236 = NOVALUE;
    int _28235 = NOVALUE;
    int _28234 = NOVALUE;
    int _28233 = NOVALUE;
    int _28232 = NOVALUE;
    int _28231 = NOVALUE;
    int _28230 = NOVALUE;
    int _28228 = NOVALUE;
    int _28227 = NOVALUE;
    int _28226 = NOVALUE;
    int _28225 = NOVALUE;
    int _28224 = NOVALUE;
    int _28222 = NOVALUE;
    int _28221 = NOVALUE;
    int _28220 = NOVALUE;
    int _28219 = NOVALUE;
    int _28218 = NOVALUE;
    int _28216 = NOVALUE;
    int _28215 = NOVALUE;
    int _28214 = NOVALUE;
    int _28213 = NOVALUE;
    int _28212 = NOVALUE;
    int _28211 = NOVALUE;
    int _28210 = NOVALUE;
    int _28209 = NOVALUE;
    int _28208 = NOVALUE;
    int _28207 = NOVALUE;
    int _28206 = NOVALUE;
    int _28205 = NOVALUE;
    int _28204 = NOVALUE;
    int _28203 = NOVALUE;
    int _28201 = NOVALUE;
    int _28198 = NOVALUE;
    int _28193 = NOVALUE;
    int _28191 = NOVALUE;
    int _28188 = NOVALUE;
    int _28187 = NOVALUE;
    int _28186 = NOVALUE;
    int _28185 = NOVALUE;
    int _28184 = NOVALUE;
    int _28183 = NOVALUE;
    int _28182 = NOVALUE;
    int _28181 = NOVALUE;
    int _28179 = NOVALUE;
    int _28177 = NOVALUE;
    int _28176 = NOVALUE;
    int _28174 = NOVALUE;
    int _28172 = NOVALUE;
    int _28170 = NOVALUE;
    int _28168 = NOVALUE;
    int _28167 = NOVALUE;
    int _28166 = NOVALUE;
    int _28165 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sub_54186)) {
        _1 = (long)(DBL_PTR(_sub_54186)->dbl);
        if (UNIQUE(DBL_PTR(_sub_54186)) && (DBL_PTR(_sub_54186)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_54186);
        _sub_54186 = _1;
    }

    /** 	if OpTrace or SymTab[sub][S_TOKEN] = TYPE then*/
    if (_38OpTrace_17015 != 0) {
        goto L1; // [7] 34
    }
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28165 = (int)*(((s1_ptr)_2)->base + _sub_54186);
    _2 = (int)SEQ_PTR(_28165);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _28166 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _28166 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _28165 = NOVALUE;
    if (IS_ATOM_INT(_28166)) {
        _28167 = (_28166 == 504);
    }
    else {
        _28167 = binary_op(EQUALS, _28166, 504);
    }
    _28166 = NOVALUE;
    if (_28167 == 0) {
        DeRef(_28167);
        _28167 = NOVALUE;
        goto L2; // [30] 40
    }
    else {
        if (!IS_ATOM_INT(_28167) && DBL_PTR(_28167)->dbl == 0.0){
            DeRef(_28167);
            _28167 = NOVALUE;
            goto L2; // [30] 40
        }
        DeRef(_28167);
        _28167 = NOVALUE;
    }
    DeRef(_28167);
    _28167 = NOVALUE;
L1: 

    /** 		return*/
    DeRefi(_backpatch_op_54255);
    return;
L2: 

    /** 	inline_sub      = sub*/
    _69inline_sub_53679 = _sub_54186;

    /** 	if get_fwdref_count() then*/
    _28168 = _40get_fwdref_count();
    if (_28168 == 0) {
        DeRef(_28168);
        _28168 = NOVALUE;
        goto L3; // [52] 65
    }
    else {
        if (!IS_ATOM_INT(_28168) && DBL_PTR(_28168)->dbl == 0.0){
            DeRef(_28168);
            _28168 = NOVALUE;
            goto L3; // [52] 65
        }
        DeRef(_28168);
        _28168 = NOVALUE;
    }
    DeRef(_28168);
    _28168 = NOVALUE;

    /** 		defer()*/
    _69defer();

    /** 		return*/
    DeRefi(_backpatch_op_54255);
    return;
L3: 

    /** 	temp_code = ""*/
    RefDS(_22663);
    DeRef(_69temp_code_54177);
    _69temp_code_54177 = _22663;

    /** 	if sub != CurrentSub then*/
    if (_sub_54186 == _38CurrentSub_16954)
    goto L4; // [76] 99

    /** 		Code = SymTab[sub][S_CODE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28170 = (int)*(((s1_ptr)_2)->base + _sub_54186);
    DeRef(_38Code_17038);
    _2 = (int)SEQ_PTR(_28170);
    if (!IS_ATOM_INT(_38S_CODE_16610)){
        _38Code_17038 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
    }
    else{
        _38Code_17038 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
    }
    Ref(_38Code_17038);
    _28170 = NOVALUE;
    goto L5; // [96] 109
L4: 

    /** 		temp_code = Code*/
    RefDS(_38Code_17038);
    DeRef(_69temp_code_54177);
    _69temp_code_54177 = _38Code_17038;
L5: 

    /** 	if length(Code) > OpInline then*/
    if (IS_SEQUENCE(_38Code_17038)){
            _28172 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _28172 = 1;
    }
    if (_28172 <= _38OpInline_17023)
    goto L6; // [118] 128

    /** 		return*/
    DeRefi(_backpatch_op_54255);
    return;
L6: 

    /** 	inline_code     = Code*/
    RefDS(_38Code_17038);
    DeRef(_69inline_code_53665);
    _69inline_code_53665 = _38Code_17038;

    /** 	return_gotos    = 0*/
    _69return_gotos_53674 = 0;

    /** 	prev_pc         = 1*/
    _69prev_pc_53673 = 1;

    /** 	proc_vars       = {}*/
    RefDS(_22663);
    DeRefi(_69proc_vars_53666);
    _69proc_vars_53666 = _22663;

    /** 	inline_temps    = {}*/
    RefDS(_22663);
    DeRef(_69inline_temps_53667);
    _69inline_temps_53667 = _22663;

    /** 	inline_params   = {}*/
    RefDS(_22663);
    DeRefi(_69inline_params_53670);
    _69inline_params_53670 = _22663;

    /** 	assigned_params = {}*/
    RefDS(_22663);
    DeRef(_69assigned_params_53671);
    _69assigned_params_53671 = _22663;

    /** 	integer pc = 1*/
    _pc_54215 = 1;

    /** 	symtab_index s = SymTab[sub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28174 = (int)*(((s1_ptr)_2)->base + _sub_54186);
    _2 = (int)SEQ_PTR(_28174);
    _s_54217 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54217)){
        _s_54217 = (long)DBL_PTR(_s_54217)->dbl;
    }
    _28174 = NOVALUE;

    /** 	for p = 1 to SymTab[sub][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28176 = (int)*(((s1_ptr)_2)->base + _sub_54186);
    _2 = (int)SEQ_PTR(_28176);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _28177 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _28177 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _28176 = NOVALUE;
    {
        int _p_54223;
        _p_54223 = 1;
L7: 
        if (binary_op_a(GREATER, _p_54223, _28177)){
            goto L8; // [210] 248
        }

        /** 		inline_params &= s*/
        Append(&_69inline_params_53670, _69inline_params_53670, _s_54217);

        /** 		s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28179 = (int)*(((s1_ptr)_2)->base + _s_54217);
        _2 = (int)SEQ_PTR(_28179);
        _s_54217 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_54217)){
            _s_54217 = (long)DBL_PTR(_s_54217)->dbl;
        }
        _28179 = NOVALUE;

        /** 	end for*/
        _0 = _p_54223;
        if (IS_ATOM_INT(_p_54223)) {
            _p_54223 = _p_54223 + 1;
            if ((long)((unsigned long)_p_54223 +(unsigned long) HIGH_BITS) >= 0){
                _p_54223 = NewDouble((double)_p_54223);
            }
        }
        else {
            _p_54223 = binary_op_a(PLUS, _p_54223, 1);
        }
        DeRef(_0);
        goto L7; // [243] 217
L8: 
        ;
        DeRef(_p_54223);
    }

    /** 	while s != 0 and */
L9: 
    _28181 = (_s_54217 != 0);
    if (_28181 == 0) {
        goto LA; // [257] 335
    }
    _28183 = _55sym_scope(_s_54217);
    if (IS_ATOM_INT(_28183)) {
        _28184 = (_28183 <= 3);
    }
    else {
        _28184 = binary_op(LESSEQ, _28183, 3);
    }
    DeRef(_28183);
    _28183 = NOVALUE;
    if (IS_ATOM_INT(_28184)) {
        if (_28184 != 0) {
            DeRef(_28185);
            _28185 = 1;
            goto LB; // [271] 289
        }
    }
    else {
        if (DBL_PTR(_28184)->dbl != 0.0) {
            DeRef(_28185);
            _28185 = 1;
            goto LB; // [271] 289
        }
    }
    _28186 = _55sym_scope(_s_54217);
    if (IS_ATOM_INT(_28186)) {
        _28187 = (_28186 == 9);
    }
    else {
        _28187 = binary_op(EQUALS, _28186, 9);
    }
    DeRef(_28186);
    _28186 = NOVALUE;
    DeRef(_28185);
    if (IS_ATOM_INT(_28187))
    _28185 = (_28187 != 0);
    else
    _28185 = DBL_PTR(_28187)->dbl != 0.0;
LB: 
    if (_28185 == 0)
    {
        _28185 = NOVALUE;
        goto LA; // [290] 335
    }
    else{
        _28185 = NOVALUE;
    }

    /** 		if sym_scope( s ) != SC_UNDEFINED then*/
    _28188 = _55sym_scope(_s_54217);
    if (binary_op_a(EQUALS, _28188, 9)){
        DeRef(_28188);
        _28188 = NOVALUE;
        goto LC; // [301] 314
    }
    DeRef(_28188);
    _28188 = NOVALUE;

    /** 			proc_vars &= s*/
    Append(&_69proc_vars_53666, _69proc_vars_53666, _s_54217);
LC: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28191 = (int)*(((s1_ptr)_2)->base + _s_54217);
    _2 = (int)SEQ_PTR(_28191);
    _s_54217 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54217)){
        _s_54217 = (long)DBL_PTR(_s_54217)->dbl;
    }
    _28191 = NOVALUE;

    /** 	end while*/
    goto L9; // [332] 253
LA: 

    /** 	sequence backpatch_op = {}*/
    RefDS(_22663);
    DeRefi(_backpatch_op_54255);
    _backpatch_op_54255 = _22663;

    /** 	while pc < length( inline_code ) do*/
LD: 
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28193 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28193 = 1;
    }
    if (_pc_54215 >= _28193)
    goto LE; // [352] 869

    /** 		integer op = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _op_54259 = (int)*(((s1_ptr)_2)->base + _pc_54215);
    if (!IS_ATOM_INT(_op_54259))
    _op_54259 = (long)DBL_PTR(_op_54259)->dbl;

    /** 		switch op do*/
    _0 = _op_54259;
    switch ( _0 ){ 

        /** 			case PROC_FORWARD, FUNC_FORWARD then*/
        case 195:
        case 196:

        /** 				defer()*/
        _69defer();

        /** 				restore_code()*/
        _69restore_code();

        /** 				return*/
        DeRefi(_backpatch_op_54255);
        _28177 = NOVALUE;
        DeRef(_28181);
        _28181 = NOVALUE;
        DeRef(_28184);
        _28184 = NOVALUE;
        DeRef(_28187);
        _28187 = NOVALUE;
        return;
        goto LF; // [390] 851

        /** 			case PROC, FUNC then*/
        case 27:
        case 501:

        /** 				symtab_index rtn_idx = inline_code[pc+1]*/
        _28198 = _pc_54215 + 1;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _rtn_idx_54270 = (int)*(((s1_ptr)_2)->base + _28198);
        if (!IS_ATOM_INT(_rtn_idx_54270)){
            _rtn_idx_54270 = (long)DBL_PTR(_rtn_idx_54270)->dbl;
        }

        /** 				if rtn_idx = sub then*/
        if (_rtn_idx_54270 != _sub_54186)
        goto L10; // [414] 428

        /** 					restore_code()*/
        _69restore_code();

        /** 					return*/
        DeRefi(_backpatch_op_54255);
        _28177 = NOVALUE;
        DeRef(_28181);
        _28181 = NOVALUE;
        _28198 = NOVALUE;
        DeRef(_28184);
        _28184 = NOVALUE;
        DeRef(_28187);
        _28187 = NOVALUE;
        return;
L10: 

        /** 				integer args = SymTab[rtn_idx][S_NUM_ARGS]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28201 = (int)*(((s1_ptr)_2)->base + _rtn_idx_54270);
        _2 = (int)SEQ_PTR(_28201);
        if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
            _args_54275 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
        }
        else{
            _args_54275 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
        }
        if (!IS_ATOM_INT(_args_54275)){
            _args_54275 = (long)DBL_PTR(_args_54275)->dbl;
        }
        _28201 = NOVALUE;

        /** 				if SymTab[rtn_idx][S_TOKEN] != PROC and check_for_param( pc + args + 2 ) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28203 = (int)*(((s1_ptr)_2)->base + _rtn_idx_54270);
        _2 = (int)SEQ_PTR(_28203);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _28204 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _28204 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _28203 = NOVALUE;
        if (IS_ATOM_INT(_28204)) {
            _28205 = (_28204 != 27);
        }
        else {
            _28205 = binary_op(NOTEQ, _28204, 27);
        }
        _28204 = NOVALUE;
        if (IS_ATOM_INT(_28205)) {
            if (_28205 == 0) {
                goto L11; // [464] 485
            }
        }
        else {
            if (DBL_PTR(_28205)->dbl == 0.0) {
                goto L11; // [464] 485
            }
        }
        _28207 = _pc_54215 + _args_54275;
        if ((long)((unsigned long)_28207 + (unsigned long)HIGH_BITS) >= 0) 
        _28207 = NewDouble((double)_28207);
        if (IS_ATOM_INT(_28207)) {
            _28208 = _28207 + 2;
            if ((long)((unsigned long)_28208 + (unsigned long)HIGH_BITS) >= 0) 
            _28208 = NewDouble((double)_28208);
        }
        else {
            _28208 = NewDouble(DBL_PTR(_28207)->dbl + (double)2);
        }
        DeRef(_28207);
        _28207 = NOVALUE;
        _28209 = _69check_for_param(_28208);
        _28208 = NOVALUE;
        if (_28209 == 0) {
            DeRef(_28209);
            _28209 = NOVALUE;
            goto L11; // [481] 485
        }
        else {
            if (!IS_ATOM_INT(_28209) && DBL_PTR(_28209)->dbl == 0.0){
                DeRef(_28209);
                _28209 = NOVALUE;
                goto L11; // [481] 485
            }
            DeRef(_28209);
            _28209 = NOVALUE;
        }
        DeRef(_28209);
        _28209 = NOVALUE;
L11: 

        /** 				for i = 2 to args + 1 + (SymTab[rtn_idx][S_TOKEN] != PROC) do*/
        _28210 = _args_54275 + 1;
        if (_28210 > MAXINT){
            _28210 = NewDouble((double)_28210);
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28211 = (int)*(((s1_ptr)_2)->base + _rtn_idx_54270);
        _2 = (int)SEQ_PTR(_28211);
        if (!IS_ATOM_INT(_38S_TOKEN_16603)){
            _28212 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
        }
        else{
            _28212 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
        }
        _28211 = NOVALUE;
        if (IS_ATOM_INT(_28212)) {
            _28213 = (_28212 != 27);
        }
        else {
            _28213 = binary_op(NOTEQ, _28212, 27);
        }
        _28212 = NOVALUE;
        if (IS_ATOM_INT(_28210) && IS_ATOM_INT(_28213)) {
            _28214 = _28210 + _28213;
            if ((long)((unsigned long)_28214 + (unsigned long)HIGH_BITS) >= 0) 
            _28214 = NewDouble((double)_28214);
        }
        else {
            _28214 = binary_op(PLUS, _28210, _28213);
        }
        DeRef(_28210);
        _28210 = NOVALUE;
        DeRef(_28213);
        _28213 = NOVALUE;
        {
            int _i_54292;
            _i_54292 = 2;
L12: 
            if (binary_op_a(GREATER, _i_54292, _28214)){
                goto L13; // [513] 550
            }

            /** 					if not adjust_symbol( pc + i ) then */
            if (IS_ATOM_INT(_i_54292)) {
                _28215 = _pc_54215 + _i_54292;
                if ((long)((unsigned long)_28215 + (unsigned long)HIGH_BITS) >= 0) 
                _28215 = NewDouble((double)_28215);
            }
            else {
                _28215 = NewDouble((double)_pc_54215 + DBL_PTR(_i_54292)->dbl);
            }
            _28216 = _69adjust_symbol(_28215);
            _28215 = NOVALUE;
            if (IS_ATOM_INT(_28216)) {
                if (_28216 != 0){
                    DeRef(_28216);
                    _28216 = NOVALUE;
                    goto L14; // [530] 543
                }
            }
            else {
                if (DBL_PTR(_28216)->dbl != 0.0){
                    DeRef(_28216);
                    _28216 = NOVALUE;
                    goto L14; // [530] 543
                }
            }
            DeRef(_28216);
            _28216 = NOVALUE;

            /** 						defer()*/
            _69defer();

            /** 						return*/
            DeRef(_i_54292);
            DeRefi(_backpatch_op_54255);
            _28177 = NOVALUE;
            DeRef(_28181);
            _28181 = NOVALUE;
            DeRef(_28198);
            _28198 = NOVALUE;
            DeRef(_28184);
            _28184 = NOVALUE;
            DeRef(_28187);
            _28187 = NOVALUE;
            DeRef(_28205);
            _28205 = NOVALUE;
            DeRef(_28214);
            _28214 = NOVALUE;
            return;
L14: 

            /** 				end for*/
            _0 = _i_54292;
            if (IS_ATOM_INT(_i_54292)) {
                _i_54292 = _i_54292 + 1;
                if ((long)((unsigned long)_i_54292 +(unsigned long) HIGH_BITS) >= 0){
                    _i_54292 = NewDouble((double)_i_54292);
                }
            }
            else {
                _i_54292 = binary_op_a(PLUS, _i_54292, 1);
            }
            DeRef(_0);
            goto L12; // [545] 520
L13: 
            ;
            DeRef(_i_54292);
        }
        goto LF; // [552] 851

        /** 			case RIGHT_BRACE_N then*/
        case 31:

        /** 				sequence args = inline_code[pc+2..inline_code[pc+1] + pc + 1]*/
        _28218 = _pc_54215 + 2;
        if ((long)((unsigned long)_28218 + (unsigned long)HIGH_BITS) >= 0) 
        _28218 = NewDouble((double)_28218);
        _28219 = _pc_54215 + 1;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28220 = (int)*(((s1_ptr)_2)->base + _28219);
        if (IS_ATOM_INT(_28220)) {
            _28221 = _28220 + _pc_54215;
            if ((long)((unsigned long)_28221 + (unsigned long)HIGH_BITS) >= 0) 
            _28221 = NewDouble((double)_28221);
        }
        else {
            _28221 = binary_op(PLUS, _28220, _pc_54215);
        }
        _28220 = NOVALUE;
        if (IS_ATOM_INT(_28221)) {
            _28222 = _28221 + 1;
        }
        else
        _28222 = binary_op(PLUS, 1, _28221);
        DeRef(_28221);
        _28221 = NOVALUE;
        rhs_slice_target = (object_ptr)&_args_54307;
        RHS_Slice(_69inline_code_53665, _28218, _28222);

        /** 				for i = 1 to length(args) - 1 do*/
        if (IS_SEQUENCE(_args_54307)){
                _28224 = SEQ_PTR(_args_54307)->length;
        }
        else {
            _28224 = 1;
        }
        _28225 = _28224 - 1;
        _28224 = NOVALUE;
        {
            int _i_54315;
            _i_54315 = 1;
L15: 
            if (_i_54315 > _28225){
                goto L16; // [598] 644
            }

            /** 					if find( args[i], args, i + 1 ) then*/
            _2 = (int)SEQ_PTR(_args_54307);
            _28226 = (int)*(((s1_ptr)_2)->base + _i_54315);
            _28227 = _i_54315 + 1;
            _28228 = find_from(_28226, _args_54307, _28227);
            _28226 = NOVALUE;
            _28227 = NOVALUE;
            if (_28228 == 0)
            {
                _28228 = NOVALUE;
                goto L17; // [620] 637
            }
            else{
                _28228 = NOVALUE;
            }

            /** 						defer()*/
            _69defer();

            /** 						restore_code()*/
            _69restore_code();

            /** 						return*/
            DeRefDS(_args_54307);
            DeRefi(_backpatch_op_54255);
            _28177 = NOVALUE;
            DeRef(_28181);
            _28181 = NOVALUE;
            DeRef(_28198);
            _28198 = NOVALUE;
            DeRef(_28184);
            _28184 = NOVALUE;
            DeRef(_28187);
            _28187 = NOVALUE;
            DeRef(_28218);
            _28218 = NOVALUE;
            DeRef(_28205);
            _28205 = NOVALUE;
            DeRef(_28214);
            _28214 = NOVALUE;
            DeRef(_28219);
            _28219 = NOVALUE;
            DeRef(_28222);
            _28222 = NOVALUE;
            DeRef(_28225);
            _28225 = NOVALUE;
            return;
L17: 

            /** 				end for*/
            _i_54315 = _i_54315 + 1;
            goto L15; // [639] 605
L16: 
            ;
        }

        /** 				goto "inline op"*/
        DeRef(_args_54307);
        _args_54307 = NOVALUE;
        goto G18;
        goto LF; // [654] 851

        /** 			case RIGHT_BRACE_2 then*/
        case 85:

        /** 				if equal( inline_code[pc+1], inline_code[pc+2] ) then*/
        _28230 = _pc_54215 + 1;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28231 = (int)*(((s1_ptr)_2)->base + _28230);
        _28232 = _pc_54215 + 2;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28233 = (int)*(((s1_ptr)_2)->base + _28232);
        if (_28231 == _28233)
        _28234 = 1;
        else if (IS_ATOM_INT(_28231) && IS_ATOM_INT(_28233))
        _28234 = 0;
        else
        _28234 = (compare(_28231, _28233) == 0);
        _28231 = NOVALUE;
        _28233 = NOVALUE;
        if (_28234 == 0)
        {
            _28234 = NOVALUE;
            goto L19; // [686] 703
        }
        else{
            _28234 = NOVALUE;
        }

        /** 					defer()*/
        _69defer();

        /** 					restore_code()*/
        _69restore_code();

        /** 					return*/
        DeRefi(_backpatch_op_54255);
        _28177 = NOVALUE;
        DeRef(_28181);
        _28181 = NOVALUE;
        DeRef(_28198);
        _28198 = NOVALUE;
        DeRef(_28184);
        _28184 = NOVALUE;
        DeRef(_28187);
        _28187 = NOVALUE;
        DeRef(_28218);
        _28218 = NOVALUE;
        DeRef(_28205);
        _28205 = NOVALUE;
        DeRef(_28214);
        _28214 = NOVALUE;
        DeRef(_28219);
        _28219 = NOVALUE;
        DeRef(_28222);
        _28222 = NOVALUE;
        DeRef(_28225);
        _28225 = NOVALUE;
        _28230 = NOVALUE;
        _28232 = NOVALUE;
        return;
L19: 

        /** 				goto "inline op"*/
        goto G18;
        goto LF; // [711] 851

        /** 			case EXIT_BLOCK then*/
        case 206:

        /** 				replace_code( "", pc, pc + 1 )*/
        _28235 = _pc_54215 + 1;
        if (_28235 > MAXINT){
            _28235 = NewDouble((double)_28235);
        }
        RefDS(_22663);
        _69replace_code(_22663, _pc_54215, _28235);
        _28235 = NOVALUE;

        /** 				continue*/
        goto LD; // [732] 347
        goto LF; // [734] 851

        /** 			case SWITCH_RT then*/
        case 202:

        /** 				sequence values = SymTab[inline_code[pc+2]][S_OBJ]*/
        _28236 = _pc_54215 + 2;
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28237 = (int)*(((s1_ptr)_2)->base + _28236);
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        if (!IS_ATOM_INT(_28237)){
            _28238 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28237)->dbl));
        }
        else{
            _28238 = (int)*(((s1_ptr)_2)->base + _28237);
        }
        DeRef(_values_54336);
        _2 = (int)SEQ_PTR(_28238);
        _values_54336 = (int)*(((s1_ptr)_2)->base + 1);
        Ref(_values_54336);
        _28238 = NOVALUE;

        /** 				for i = 1 to length( values ) do*/
        if (IS_SEQUENCE(_values_54336)){
                _28240 = SEQ_PTR(_values_54336)->length;
        }
        else {
            _28240 = 1;
        }
        {
            int _i_54344;
            _i_54344 = 1;
L1A: 
            if (_i_54344 > _28240){
                goto L1B; // [771] 811
            }

            /** 					if sequence( values[i] ) then*/
            _2 = (int)SEQ_PTR(_values_54336);
            _28241 = (int)*(((s1_ptr)_2)->base + _i_54344);
            _28242 = IS_SEQUENCE(_28241);
            _28241 = NOVALUE;
            if (_28242 == 0)
            {
                _28242 = NOVALUE;
                goto L1C; // [787] 804
            }
            else{
                _28242 = NOVALUE;
            }

            /** 						defer()*/
            _69defer();

            /** 						restore_code()*/
            _69restore_code();

            /** 						return*/
            DeRefDS(_values_54336);
            DeRefi(_backpatch_op_54255);
            _28177 = NOVALUE;
            DeRef(_28181);
            _28181 = NOVALUE;
            DeRef(_28198);
            _28198 = NOVALUE;
            DeRef(_28184);
            _28184 = NOVALUE;
            DeRef(_28187);
            _28187 = NOVALUE;
            DeRef(_28218);
            _28218 = NOVALUE;
            DeRef(_28205);
            _28205 = NOVALUE;
            DeRef(_28214);
            _28214 = NOVALUE;
            DeRef(_28219);
            _28219 = NOVALUE;
            DeRef(_28222);
            _28222 = NOVALUE;
            DeRef(_28225);
            _28225 = NOVALUE;
            DeRef(_28230);
            _28230 = NOVALUE;
            DeRef(_28236);
            _28236 = NOVALUE;
            DeRef(_28232);
            _28232 = NOVALUE;
            _28237 = NOVALUE;
            return;
L1C: 

            /** 				end for*/
            _i_54344 = _i_54344 + 1;
            goto L1A; // [806] 778
L1B: 
            ;
        }

        /** 				backpatch_op = append( backpatch_op, pc )*/
        Append(&_backpatch_op_54255, _backpatch_op_54255, _pc_54215);
        DeRef(_values_54336);
        _values_54336 = NOVALUE;

        /** 			case else*/
        default:

        /** 			label "inline op"*/
G18:

        /** 				if not inline_op( pc ) then*/
        _28244 = _69inline_op(_pc_54215);
        if (IS_ATOM_INT(_28244)) {
            if (_28244 != 0){
                DeRef(_28244);
                _28244 = NOVALUE;
                goto L1D; // [833] 850
            }
        }
        else {
            if (DBL_PTR(_28244)->dbl != 0.0){
                DeRef(_28244);
                _28244 = NOVALUE;
                goto L1D; // [833] 850
            }
        }
        DeRef(_28244);
        _28244 = NOVALUE;

        /** 					defer()*/
        _69defer();

        /** 					restore_code()*/
        _69restore_code();

        /** 					return*/
        DeRefi(_backpatch_op_54255);
        _28177 = NOVALUE;
        DeRef(_28181);
        _28181 = NOVALUE;
        DeRef(_28198);
        _28198 = NOVALUE;
        DeRef(_28184);
        _28184 = NOVALUE;
        DeRef(_28187);
        _28187 = NOVALUE;
        DeRef(_28218);
        _28218 = NOVALUE;
        DeRef(_28205);
        _28205 = NOVALUE;
        DeRef(_28214);
        _28214 = NOVALUE;
        DeRef(_28219);
        _28219 = NOVALUE;
        DeRef(_28222);
        _28222 = NOVALUE;
        DeRef(_28225);
        _28225 = NOVALUE;
        DeRef(_28230);
        _28230 = NOVALUE;
        DeRef(_28236);
        _28236 = NOVALUE;
        DeRef(_28232);
        _28232 = NOVALUE;
        _28237 = NOVALUE;
        return;
L1D: 
    ;}LF: 

    /** 		pc = advance( pc, inline_code )*/
    RefDS(_69inline_code_53665);
    _pc_54215 = _69advance(_pc_54215, _69inline_code_53665);
    if (!IS_ATOM_INT(_pc_54215)) {
        _1 = (long)(DBL_PTR(_pc_54215)->dbl);
        if (UNIQUE(DBL_PTR(_pc_54215)) && (DBL_PTR(_pc_54215)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_54215);
        _pc_54215 = _1;
    }

    /** 	end while*/
    goto LD; // [866] 347
LE: 

    /** 	SymTab[sub][S_INLINE] = { sort( assigned_params ), inline_code, backpatch_op }*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_sub_54186 + ((s1_ptr)_2)->base);
    RefDS(_69assigned_params_53671);
    _28249 = _22sort(_69assigned_params_53671, 1);
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = _28249;
    RefDS(_69inline_code_53665);
    *((int *)(_2+8)) = _69inline_code_53665;
    RefDS(_backpatch_op_54255);
    *((int *)(_2+12)) = _backpatch_op_54255;
    _28250 = MAKE_SEQ(_1);
    _28249 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 29);
    _1 = *(int *)_2;
    *(int *)_2 = _28250;
    if( _1 != _28250 ){
        DeRef(_1);
    }
    _28250 = NOVALUE;
    _28247 = NOVALUE;

    /** 	restore_code()*/
    _69restore_code();

    /** end procedure*/
    DeRefDSi(_backpatch_op_54255);
    _28177 = NOVALUE;
    DeRef(_28181);
    _28181 = NOVALUE;
    DeRef(_28198);
    _28198 = NOVALUE;
    DeRef(_28184);
    _28184 = NOVALUE;
    DeRef(_28187);
    _28187 = NOVALUE;
    DeRef(_28218);
    _28218 = NOVALUE;
    DeRef(_28205);
    _28205 = NOVALUE;
    DeRef(_28214);
    _28214 = NOVALUE;
    DeRef(_28219);
    _28219 = NOVALUE;
    DeRef(_28222);
    _28222 = NOVALUE;
    DeRef(_28225);
    _28225 = NOVALUE;
    DeRef(_28230);
    _28230 = NOVALUE;
    DeRef(_28236);
    _28236 = NOVALUE;
    DeRef(_28232);
    _28232 = NOVALUE;
    _28237 = NOVALUE;
    return;
    ;
}


void _69replace_temp(int _pc_54364)
{
    int _temp_num_54365 = NOVALUE;
    int _needed_54368 = NOVALUE;
    int _28263 = NOVALUE;
    int _28262 = NOVALUE;
    int _28261 = NOVALUE;
    int _28260 = NOVALUE;
    int _28258 = NOVALUE;
    int _28256 = NOVALUE;
    int _28253 = NOVALUE;
    int _28251 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer temp_num = inline_code[pc][2]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28251 = (int)*(((s1_ptr)_2)->base + _pc_54364);
    _2 = (int)SEQ_PTR(_28251);
    _temp_num_54365 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_temp_num_54365)){
        _temp_num_54365 = (long)DBL_PTR(_temp_num_54365)->dbl;
    }
    _28251 = NOVALUE;

    /** 	integer needed = temp_num - length( inline_temps )*/
    if (IS_SEQUENCE(_69inline_temps_53667)){
            _28253 = SEQ_PTR(_69inline_temps_53667)->length;
    }
    else {
        _28253 = 1;
    }
    _needed_54368 = _temp_num_54365 - _28253;
    _28253 = NOVALUE;

    /** 	if needed > 0 then*/
    if (_needed_54368 <= 0)
    goto L1; // [30] 47

    /** 		inline_temps &= repeat( 0, needed )*/
    _28256 = Repeat(0, _needed_54368);
    Concat((object_ptr)&_69inline_temps_53667, _69inline_temps_53667, _28256);
    DeRefDS(_28256);
    _28256 = NOVALUE;
L1: 

    /** 	if not inline_temps[temp_num] then*/
    _2 = (int)SEQ_PTR(_69inline_temps_53667);
    _28258 = (int)*(((s1_ptr)_2)->base + _temp_num_54365);
    if (IS_ATOM_INT(_28258)) {
        if (_28258 != 0){
            _28258 = NOVALUE;
            goto L2; // [55] 100
        }
    }
    else {
        if (DBL_PTR(_28258)->dbl != 0.0){
            _28258 = NOVALUE;
            goto L2; // [55] 100
        }
    }
    _28258 = NOVALUE;

    /** 		if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L3; // [62] 84
    }
    else{
    }

    /** 			inline_temps[temp_num] = new_inline_var( -temp_num, 0 )*/
    if ((unsigned long)_temp_num_54365 == 0xC0000000)
    _28260 = (int)NewDouble((double)-0xC0000000);
    else
    _28260 = - _temp_num_54365;
    _28261 = _69new_inline_var(_28260, 0);
    _28260 = NOVALUE;
    _2 = (int)SEQ_PTR(_69inline_temps_53667);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_temps_53667 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp_num_54365);
    _1 = *(int *)_2;
    *(int *)_2 = _28261;
    if( _1 != _28261 ){
        DeRef(_1);
    }
    _28261 = NOVALUE;
    goto L4; // [81] 99
L3: 

    /** 			inline_temps[temp_num] = NewTempSym( TRUE )*/
    _28262 = _55NewTempSym(_9TRUE_428);
    _2 = (int)SEQ_PTR(_69inline_temps_53667);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_temps_53667 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _temp_num_54365);
    _1 = *(int *)_2;
    *(int *)_2 = _28262;
    if( _1 != _28262 ){
        DeRef(_1);
    }
    _28262 = NOVALUE;
L4: 
L2: 

    /** 	inline_code[pc] = inline_temps[temp_num]*/
    _2 = (int)SEQ_PTR(_69inline_temps_53667);
    _28263 = (int)*(((s1_ptr)_2)->base + _temp_num_54365);
    Ref(_28263);
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pc_54364);
    _1 = *(int *)_2;
    *(int *)_2 = _28263;
    if( _1 != _28263 ){
        DeRef(_1);
    }
    _28263 = NOVALUE;

    /** end procedure*/
    return;
    ;
}


int _69get_param_sym(int _pc_54390)
{
    int _il_54391 = NOVALUE;
    int _px_54399 = NOVALUE;
    int _28270 = NOVALUE;
    int _28267 = NOVALUE;
    int _28266 = NOVALUE;
    int _28265 = NOVALUE;
    int _0, _1, _2;
    

    /** 	object il = inline_code[pc]*/
    DeRef(_il_54391);
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _il_54391 = (int)*(((s1_ptr)_2)->base + _pc_54390);
    Ref(_il_54391);

    /** 	if integer( il ) then*/
    if (IS_ATOM_INT(_il_54391))
    _28265 = 1;
    else if (IS_ATOM_DBL(_il_54391))
    _28265 = IS_ATOM_INT(DoubleToInt(_il_54391));
    else
    _28265 = 0;
    if (_28265 == 0)
    {
        _28265 = NOVALUE;
        goto L1; // [16] 34
    }
    else{
        _28265 = NOVALUE;
    }

    /** 		return inline_code[pc]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28266 = (int)*(((s1_ptr)_2)->base + _pc_54390);
    Ref(_28266);
    DeRef(_il_54391);
    return _28266;
    goto L2; // [31] 53
L1: 

    /** 	elsif length( il ) = 1 then*/
    if (IS_SEQUENCE(_il_54391)){
            _28267 = SEQ_PTR(_il_54391)->length;
    }
    else {
        _28267 = 1;
    }
    if (_28267 != 1)
    goto L3; // [39] 52

    /** 		return inline_target*/
    DeRef(_il_54391);
    _28266 = NOVALUE;
    return _69inline_target_53672;
L3: 
L2: 

    /** 	integer px = il[2]*/
    _2 = (int)SEQ_PTR(_il_54391);
    _px_54399 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_px_54399)){
        _px_54399 = (long)DBL_PTR(_px_54399)->dbl;
    }

    /** 	return passed_params[px]*/
    _2 = (int)SEQ_PTR(_69passed_params_53668);
    _28270 = (int)*(((s1_ptr)_2)->base + _px_54399);
    Ref(_28270);
    DeRef(_il_54391);
    _28266 = NOVALUE;
    return _28270;
    ;
}


int _69get_original_sym(int _pc_54404)
{
    int _il_54405 = NOVALUE;
    int _px_54413 = NOVALUE;
    int _28277 = NOVALUE;
    int _28274 = NOVALUE;
    int _28273 = NOVALUE;
    int _28272 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_54404)) {
        _1 = (long)(DBL_PTR(_pc_54404)->dbl);
        if (UNIQUE(DBL_PTR(_pc_54404)) && (DBL_PTR(_pc_54404)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_54404);
        _pc_54404 = _1;
    }

    /** 	object il = inline_code[pc]*/
    DeRef(_il_54405);
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _il_54405 = (int)*(((s1_ptr)_2)->base + _pc_54404);
    Ref(_il_54405);

    /** 	if integer( il ) then*/
    if (IS_ATOM_INT(_il_54405))
    _28272 = 1;
    else if (IS_ATOM_DBL(_il_54405))
    _28272 = IS_ATOM_INT(DoubleToInt(_il_54405));
    else
    _28272 = 0;
    if (_28272 == 0)
    {
        _28272 = NOVALUE;
        goto L1; // [16] 34
    }
    else{
        _28272 = NOVALUE;
    }

    /** 		return inline_code[pc]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28273 = (int)*(((s1_ptr)_2)->base + _pc_54404);
    Ref(_28273);
    DeRef(_il_54405);
    return _28273;
    goto L2; // [31] 53
L1: 

    /** 	elsif length( il ) = 1 then*/
    if (IS_SEQUENCE(_il_54405)){
            _28274 = SEQ_PTR(_il_54405)->length;
    }
    else {
        _28274 = 1;
    }
    if (_28274 != 1)
    goto L3; // [39] 52

    /** 		return inline_target*/
    DeRef(_il_54405);
    _28273 = NOVALUE;
    return _69inline_target_53672;
L3: 
L2: 

    /** 	integer px = il[2]*/
    _2 = (int)SEQ_PTR(_il_54405);
    _px_54413 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_px_54413)){
        _px_54413 = (long)DBL_PTR(_px_54413)->dbl;
    }

    /** 	return original_params[px]*/
    _2 = (int)SEQ_PTR(_69original_params_53669);
    _28277 = (int)*(((s1_ptr)_2)->base + _px_54413);
    Ref(_28277);
    DeRef(_il_54405);
    _28273 = NOVALUE;
    return _28277;
    ;
}


void _69replace_var(int _pc_54422)
{
    int _28281 = NOVALUE;
    int _28280 = NOVALUE;
    int _28279 = NOVALUE;
    int _0, _1, _2;
    

    /** 	inline_code[pc] = proc_vars[inline_code[pc][2]]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28279 = (int)*(((s1_ptr)_2)->base + _pc_54422);
    _2 = (int)SEQ_PTR(_28279);
    _28280 = (int)*(((s1_ptr)_2)->base + 2);
    _28279 = NOVALUE;
    _2 = (int)SEQ_PTR(_69proc_vars_53666);
    if (!IS_ATOM_INT(_28280)){
        _28281 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28280)->dbl));
    }
    else{
        _28281 = (int)*(((s1_ptr)_2)->base + _28280);
    }
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _pc_54422);
    _1 = *(int *)_2;
    *(int *)_2 = _28281;
    if( _1 != _28281 ){
        DeRef(_1);
    }
    _28281 = NOVALUE;

    /** end procedure*/
    _28280 = NOVALUE;
    return;
    ;
}


void _69fix_switch_rt(int _pc_54428)
{
    int _value_table_54430 = NOVALUE;
    int _jump_table_54437 = NOVALUE;
    int _28301 = NOVALUE;
    int _28300 = NOVALUE;
    int _28299 = NOVALUE;
    int _28298 = NOVALUE;
    int _28297 = NOVALUE;
    int _28296 = NOVALUE;
    int _28294 = NOVALUE;
    int _28293 = NOVALUE;
    int _28292 = NOVALUE;
    int _28291 = NOVALUE;
    int _28290 = NOVALUE;
    int _28288 = NOVALUE;
    int _28286 = NOVALUE;
    int _28285 = NOVALUE;
    int _28283 = NOVALUE;
    int _28282 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	symtab_index value_table = NewStringSym( {-1, length(SymTab)} )*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _28282 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _28282 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _28282;
    _28283 = MAKE_SEQ(_1);
    _28282 = NOVALUE;
    _value_table_54430 = _55NewStringSym(_28283);
    _28283 = NOVALUE;
    if (!IS_ATOM_INT(_value_table_54430)) {
        _1 = (long)(DBL_PTR(_value_table_54430)->dbl);
        if (UNIQUE(DBL_PTR(_value_table_54430)) && (DBL_PTR(_value_table_54430)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_value_table_54430);
        _value_table_54430 = _1;
    }

    /** 	symtab_index jump_table  = NewStringSym( {-1, length(SymTab)} )*/
    if (IS_SEQUENCE(_35SymTab_15595)){
            _28285 = SEQ_PTR(_35SymTab_15595)->length;
    }
    else {
        _28285 = 1;
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = _28285;
    _28286 = MAKE_SEQ(_1);
    _28285 = NOVALUE;
    _jump_table_54437 = _55NewStringSym(_28286);
    _28286 = NOVALUE;
    if (!IS_ATOM_INT(_jump_table_54437)) {
        _1 = (long)(DBL_PTR(_jump_table_54437)->dbl);
        if (UNIQUE(DBL_PTR(_jump_table_54437)) && (DBL_PTR(_jump_table_54437)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_jump_table_54437);
        _jump_table_54437 = _1;
    }

    /** 	SymTab[value_table][S_OBJ] = SymTab[inline_code[pc+2]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_value_table_54430 + ((s1_ptr)_2)->base);
    _28290 = _pc_54428 + 2;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28291 = (int)*(((s1_ptr)_2)->base + _28290);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_28291)){
        _28292 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28291)->dbl));
    }
    else{
        _28292 = (int)*(((s1_ptr)_2)->base + _28291);
    }
    _2 = (int)SEQ_PTR(_28292);
    _28293 = (int)*(((s1_ptr)_2)->base + 1);
    _28292 = NOVALUE;
    Ref(_28293);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _28293;
    if( _1 != _28293 ){
        DeRef(_1);
    }
    _28293 = NOVALUE;
    _28288 = NOVALUE;

    /** 	SymTab[jump_table][S_OBJ]  = SymTab[inline_code[pc+3]][S_OBJ]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_jump_table_54437 + ((s1_ptr)_2)->base);
    _28296 = _pc_54428 + 3;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _28297 = (int)*(((s1_ptr)_2)->base + _28296);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!IS_ATOM_INT(_28297)){
        _28298 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_28297)->dbl));
    }
    else{
        _28298 = (int)*(((s1_ptr)_2)->base + _28297);
    }
    _2 = (int)SEQ_PTR(_28298);
    _28299 = (int)*(((s1_ptr)_2)->base + 1);
    _28298 = NOVALUE;
    Ref(_28299);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 1);
    _1 = *(int *)_2;
    *(int *)_2 = _28299;
    if( _1 != _28299 ){
        DeRef(_1);
    }
    _28299 = NOVALUE;
    _28294 = NOVALUE;

    /** 	inline_code[pc+2] = value_table*/
    _28300 = _pc_54428 + 2;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28300);
    _1 = *(int *)_2;
    *(int *)_2 = _value_table_54430;
    DeRef(_1);

    /** 	inline_code[pc+3] = jump_table*/
    _28301 = _pc_54428 + 3;
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69inline_code_53665 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28301);
    _1 = *(int *)_2;
    *(int *)_2 = _jump_table_54437;
    DeRef(_1);

    /** end procedure*/
    _28300 = NOVALUE;
    _28290 = NOVALUE;
    _28291 = NOVALUE;
    _28296 = NOVALUE;
    _28297 = NOVALUE;
    _28301 = NOVALUE;
    return;
    ;
}


void _69fixup_special_op(int _pc_54467)
{
    int _op_54468 = NOVALUE;
    int _0, _1, _2;
    
    if (!IS_ATOM_INT(_pc_54467)) {
        _1 = (long)(DBL_PTR(_pc_54467)->dbl);
        if (UNIQUE(DBL_PTR(_pc_54467)) && (DBL_PTR(_pc_54467)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_pc_54467);
        _pc_54467 = _1;
    }

    /** 	integer op = inline_code[pc]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _op_54468 = (int)*(((s1_ptr)_2)->base + _pc_54467);
    if (!IS_ATOM_INT(_op_54468))
    _op_54468 = (long)DBL_PTR(_op_54468)->dbl;

    /** 	switch op with fallthru do*/
    _0 = _op_54468;
    switch ( _0 ){ 

        /** 		case SWITCH_RT then*/
        case 202:

        /** 			fix_switch_rt( pc )*/
        _69fix_switch_rt(_pc_54467);

        /** 			break*/
        goto L1; // [29] 32
    ;}L1: 

    /** end procedure*/
    return;
    ;
}


int _69new_inline_var(int _ps_54479, int _reuse_54480)
{
    int _var_54482 = NOVALUE;
    int _vtype_54483 = NOVALUE;
    int _name_54484 = NOVALUE;
    int _s_54486 = NOVALUE;
    int _28364 = NOVALUE;
    int _28363 = NOVALUE;
    int _28361 = NOVALUE;
    int _28358 = NOVALUE;
    int _28357 = NOVALUE;
    int _28355 = NOVALUE;
    int _28352 = NOVALUE;
    int _28351 = NOVALUE;
    int _28350 = NOVALUE;
    int _28348 = NOVALUE;
    int _28343 = NOVALUE;
    int _28338 = NOVALUE;
    int _28337 = NOVALUE;
    int _28336 = NOVALUE;
    int _28335 = NOVALUE;
    int _28332 = NOVALUE;
    int _28330 = NOVALUE;
    int _28327 = NOVALUE;
    int _28322 = NOVALUE;
    int _28321 = NOVALUE;
    int _28320 = NOVALUE;
    int _28319 = NOVALUE;
    int _28318 = NOVALUE;
    int _28315 = NOVALUE;
    int _28314 = NOVALUE;
    int _28313 = NOVALUE;
    int _28312 = NOVALUE;
    int _28311 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_ps_54479)) {
        _1 = (long)(DBL_PTR(_ps_54479)->dbl);
        if (UNIQUE(DBL_PTR(_ps_54479)) && (DBL_PTR(_ps_54479)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_ps_54479);
        _ps_54479 = _1;
    }

    /** 		var = 0, */
    _var_54482 = 0;

    /** 	sequence name*/

    /** 	if reuse then*/

    /** 	if not var then*/

    /** 		if ps > 0 then*/
    if (_ps_54479 <= 0)
    goto L1; // [45] 222

    /** 			s = ps*/
    _s_54486 = _ps_54479;

    /** 			if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L2; // [60] 102
    }
    else{
    }

    /** 				name = sprintf( "%s_inlined_%s", {SymTab[s][S_NAME], SymTab[inline_sub][S_NAME] })*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28311 = (int)*(((s1_ptr)_2)->base + _s_54486);
    _2 = (int)SEQ_PTR(_28311);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _28312 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _28312 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _28311 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28313 = (int)*(((s1_ptr)_2)->base + _69inline_sub_53679);
    _2 = (int)SEQ_PTR(_28313);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _28314 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _28314 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _28313 = NOVALUE;
    Ref(_28314);
    Ref(_28312);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28312;
    ((int *)_2)[2] = _28314;
    _28315 = MAKE_SEQ(_1);
    _28314 = NOVALUE;
    _28312 = NOVALUE;
    DeRefi(_name_54484);
    _name_54484 = EPrintf(-9999999, _28310, _28315);
    DeRefDS(_28315);
    _28315 = NOVALUE;
    goto L3; // [99] 139
L2: 

    /** 				name = sprintf( "%s (from inlined routine '%s'", {SymTab[s][S_NAME], SymTab[inline_sub][S_NAME] })*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28318 = (int)*(((s1_ptr)_2)->base + _s_54486);
    _2 = (int)SEQ_PTR(_28318);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _28319 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _28319 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _28318 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28320 = (int)*(((s1_ptr)_2)->base + _69inline_sub_53679);
    _2 = (int)SEQ_PTR(_28320);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _28321 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _28321 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _28320 = NOVALUE;
    Ref(_28321);
    Ref(_28319);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28319;
    ((int *)_2)[2] = _28321;
    _28322 = MAKE_SEQ(_1);
    _28321 = NOVALUE;
    _28319 = NOVALUE;
    DeRefi(_name_54484);
    _name_54484 = EPrintf(-9999999, _28317, _28322);
    DeRefDS(_28322);
    _28322 = NOVALUE;
L3: 

    /** 			if reuse then*/
    if (_reuse_54480 == 0)
    {
        goto L4; // [141] 163
    }
    else{
    }

    /** 				if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L5; // [148] 203

    /** 					name &= ")"*/
    Concat((object_ptr)&_name_54484, _name_54484, _27096);
    goto L5; // [160] 203
L4: 

    /** 				if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L6; // [167] 187
    }
    else{
    }

    /** 					name &= sprintf( "_at_%d", inline_start)*/
    _28327 = EPrintf(-9999999, _28326, _69inline_start_53677);
    Concat((object_ptr)&_name_54484, _name_54484, _28327);
    DeRefDS(_28327);
    _28327 = NOVALUE;
    goto L7; // [184] 202
L6: 

    /** 					name &= sprintf( " at %d)", inline_start)*/
    _28330 = EPrintf(-9999999, _28329, _69inline_start_53677);
    Concat((object_ptr)&_name_54484, _name_54484, _28330);
    DeRefDS(_28330);
    _28330 = NOVALUE;
L7: 
L5: 

    /** 			vtype = SymTab[s][S_VTYPE]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28332 = (int)*(((s1_ptr)_2)->base + _s_54486);
    _2 = (int)SEQ_PTR(_28332);
    _vtype_54483 = (int)*(((s1_ptr)_2)->base + 15);
    if (!IS_ATOM_INT(_vtype_54483)){
        _vtype_54483 = (long)DBL_PTR(_vtype_54483)->dbl;
    }
    _28332 = NOVALUE;
    goto L8; // [219] 286
L1: 

    /** 			name = sprintf( "%s_%d", {SymTab[inline_sub][S_NAME], -ps})*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28335 = (int)*(((s1_ptr)_2)->base + _69inline_sub_53679);
    _2 = (int)SEQ_PTR(_28335);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _28336 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _28336 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _28335 = NOVALUE;
    if ((unsigned long)_ps_54479 == 0xC0000000)
    _28337 = (int)NewDouble((double)-0xC0000000);
    else
    _28337 = - _ps_54479;
    Ref(_28336);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _28336;
    ((int *)_2)[2] = _28337;
    _28338 = MAKE_SEQ(_1);
    _28337 = NOVALUE;
    _28336 = NOVALUE;
    DeRefi(_name_54484);
    _name_54484 = EPrintf(-9999999, _28334, _28338);
    DeRefDS(_28338);
    _28338 = NOVALUE;

    /** 			if reuse then*/
    if (_reuse_54480 == 0)
    {
        goto L9; // [251] 263
    }
    else{
    }

    /** 				name &= "__tmp"*/
    Concat((object_ptr)&_name_54484, _name_54484, _28340);
    goto LA; // [260] 276
L9: 

    /** 				name &= sprintf( "__tmp_at%d", inline_start)*/
    _28343 = EPrintf(-9999999, _28342, _69inline_start_53677);
    Concat((object_ptr)&_name_54484, _name_54484, _28343);
    DeRefDS(_28343);
    _28343 = NOVALUE;
LA: 

    /** 			vtype = object_type*/
    _vtype_54483 = _55object_type_47055;
L8: 

    /** 		if CurrentSub = TopLevelSub then*/
    if (_38CurrentSub_16954 != _38TopLevelSub_16953)
    goto LB; // [292] 325

    /** 			var = NewEntry( name, varnum, SC_LOCAL, VARIABLE, INLINE_HASHVAL, 0, vtype )*/
    RefDS(_name_54484);
    _var_54482 = _55NewEntry(_name_54484, _69varnum_53676, 5, -100, 2004, 0, _vtype_54483);
    if (!IS_ATOM_INT(_var_54482)) {
        _1 = (long)(DBL_PTR(_var_54482)->dbl);
        if (UNIQUE(DBL_PTR(_var_54482)) && (DBL_PTR(_var_54482)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_54482);
        _var_54482 = _1;
    }
    goto LC; // [322] 416
LB: 

    /** 			var = NewBasicEntry( name, varnum, SC_PRIVATE, VARIABLE, INLINE_HASHVAL, 0, vtype )*/
    RefDS(_name_54484);
    _var_54482 = _55NewBasicEntry(_name_54484, _69varnum_53676, 3, -100, 2004, 0, _vtype_54483);
    if (!IS_ATOM_INT(_var_54482)) {
        _1 = (long)(DBL_PTR(_var_54482)->dbl);
        if (UNIQUE(DBL_PTR(_var_54482)) && (DBL_PTR(_var_54482)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_54482);
        _var_54482 = _1;
    }

    /** 			SymTab[var][S_NEXT] = SymTab[last_param][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_var_54482 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28350 = (int)*(((s1_ptr)_2)->base + _69last_param_53680);
    _2 = (int)SEQ_PTR(_28350);
    _28351 = (int)*(((s1_ptr)_2)->base + 2);
    _28350 = NOVALUE;
    Ref(_28351);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _28351;
    if( _1 != _28351 ){
        DeRef(_1);
    }
    _28351 = NOVALUE;
    _28348 = NOVALUE;

    /** 			SymTab[last_param][S_NEXT] = var*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_69last_param_53680 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 2);
    _1 = *(int *)_2;
    *(int *)_2 = _var_54482;
    DeRef(_1);
    _28352 = NOVALUE;

    /** 			if last_param = last_sym then*/
    if (_69last_param_53680 != _55last_sym_47064)
    goto LD; // [403] 415

    /** 				last_sym = var*/
    _55last_sym_47064 = _var_54482;
LD: 
LC: 

    /** 		if deferred_inlining then*/
    if (_69deferred_inlining_53675 == 0)
    {
        goto LE; // [420] 451
    }
    else{
    }

    /** 			SymTab[CurrentSub][S_STACK_SPACE] += 1*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_38CurrentSub_16954 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658)){
        _28357 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    }
    else{
        _28357 = (int)*(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    }
    _28355 = NOVALUE;
    if (IS_ATOM_INT(_28357)) {
        _28358 = _28357 + 1;
        if (_28358 > MAXINT){
            _28358 = NewDouble((double)_28358);
        }
    }
    else
    _28358 = binary_op(PLUS, 1, _28357);
    _28357 = NOVALUE;
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    if (!IS_ATOM_INT(_38S_STACK_SPACE_16658))
    _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_STACK_SPACE_16658)->dbl));
    else
    _2 = (int)(((s1_ptr)_2)->base + _38S_STACK_SPACE_16658);
    _1 = *(int *)_2;
    *(int *)_2 = _28358;
    if( _1 != _28358 ){
        DeRef(_1);
    }
    _28358 = NOVALUE;
    _28355 = NOVALUE;
    goto LF; // [448] 471
LE: 

    /** 			if param_num != -1 then*/
    if (_41param_num_55131 == -1)
    goto L10; // [455] 470

    /** 				param_num += 1*/
    _41param_num_55131 = _41param_num_55131 + 1;
L10: 
LF: 

    /** 		SymTab[var][S_USAGE] = U_USED*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_var_54482 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 5);
    _1 = *(int *)_2;
    *(int *)_2 = 3;
    DeRef(_1);
    _28361 = NOVALUE;

    /** 		if reuse then*/
    if (_reuse_54480 == 0)
    {
        goto L11; // [490] 515
    }
    else{
    }

    /** 			map:nested_put( inline_var_map, {CurrentSub, ps }, var )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _38CurrentSub_16954;
    ((int *)_2)[2] = _ps_54479;
    _28363 = MAKE_SEQ(_1);
    Ref(_69inline_var_map_53684);
    _29nested_put(_69inline_var_map_53684, _28363, _var_54482, 1, 23);
    _28363 = NOVALUE;
L11: 

    /** 	Block_var( var )*/
    _68Block_var(_var_54482);

    /** 	if BIND then*/
    if (_38BIND_16567 == 0)
    {
        goto L12; // [525] 540
    }
    else{
    }

    /** 		add_ref( {VARIABLE, var} )*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -100;
    ((int *)_2)[2] = _var_54482;
    _28364 = MAKE_SEQ(_1);
    _55add_ref(_28364);
    _28364 = NOVALUE;
L12: 

    /** 	return var*/
    DeRefi(_name_54484);
    return _var_54482;
    ;
}


int _69get_inlined_code(int _sub_54616, int _start_54617, int _deferred_54618)
{
    int _is_proc_54619 = NOVALUE;
    int _backpatches_54637 = NOVALUE;
    int _prolog_54643 = NOVALUE;
    int _epilog_54644 = NOVALUE;
    int _s_54660 = NOVALUE;
    int _last_sym_54683 = NOVALUE;
    int _int_sym_54710 = NOVALUE;
    int _param_54718 = NOVALUE;
    int _ax_54721 = NOVALUE;
    int _var_54728 = NOVALUE;
    int _final_target_54743 = NOVALUE;
    int _var_54762 = NOVALUE;
    int _create_target_var_54775 = NOVALUE;
    int _check_pc_54798 = NOVALUE;
    int _op_54802 = NOVALUE;
    int _sym_54811 = NOVALUE;
    int _check_result_54816 = NOVALUE;
    int _inline_type_54894 = NOVALUE;
    int _replace_param_1__tmp_at1341_54905 = NOVALUE;
    int _28519 = NOVALUE;
    int _28518 = NOVALUE;
    int _28517 = NOVALUE;
    int _28516 = NOVALUE;
    int _28515 = NOVALUE;
    int _28514 = NOVALUE;
    int _28513 = NOVALUE;
    int _28512 = NOVALUE;
    int _28510 = NOVALUE;
    int _28507 = NOVALUE;
    int _28504 = NOVALUE;
    int _28503 = NOVALUE;
    int _28502 = NOVALUE;
    int _28501 = NOVALUE;
    int _28500 = NOVALUE;
    int _28499 = NOVALUE;
    int _28498 = NOVALUE;
    int _28497 = NOVALUE;
    int _28493 = NOVALUE;
    int _28492 = NOVALUE;
    int _28491 = NOVALUE;
    int _28490 = NOVALUE;
    int _28486 = NOVALUE;
    int _28485 = NOVALUE;
    int _28484 = NOVALUE;
    int _28483 = NOVALUE;
    int _28482 = NOVALUE;
    int _28481 = NOVALUE;
    int _28480 = NOVALUE;
    int _28478 = NOVALUE;
    int _28477 = NOVALUE;
    int _28476 = NOVALUE;
    int _28475 = NOVALUE;
    int _28474 = NOVALUE;
    int _28473 = NOVALUE;
    int _28472 = NOVALUE;
    int _28471 = NOVALUE;
    int _28470 = NOVALUE;
    int _28469 = NOVALUE;
    int _28468 = NOVALUE;
    int _28466 = NOVALUE;
    int _28464 = NOVALUE;
    int _28462 = NOVALUE;
    int _28461 = NOVALUE;
    int _28459 = NOVALUE;
    int _28458 = NOVALUE;
    int _28455 = NOVALUE;
    int _28454 = NOVALUE;
    int _28452 = NOVALUE;
    int _28450 = NOVALUE;
    int _28445 = NOVALUE;
    int _28441 = NOVALUE;
    int _28438 = NOVALUE;
    int _28434 = NOVALUE;
    int _28427 = NOVALUE;
    int _28426 = NOVALUE;
    int _28425 = NOVALUE;
    int _28424 = NOVALUE;
    int _28423 = NOVALUE;
    int _28422 = NOVALUE;
    int _28421 = NOVALUE;
    int _28419 = NOVALUE;
    int _28414 = NOVALUE;
    int _28411 = NOVALUE;
    int _28406 = NOVALUE;
    int _28405 = NOVALUE;
    int _28403 = NOVALUE;
    int _28402 = NOVALUE;
    int _28401 = NOVALUE;
    int _28398 = NOVALUE;
    int _28397 = NOVALUE;
    int _28396 = NOVALUE;
    int _28395 = NOVALUE;
    int _28394 = NOVALUE;
    int _28393 = NOVALUE;
    int _28392 = NOVALUE;
    int _28390 = NOVALUE;
    int _28389 = NOVALUE;
    int _28388 = NOVALUE;
    int _28386 = NOVALUE;
    int _28384 = NOVALUE;
    int _28383 = NOVALUE;
    int _28382 = NOVALUE;
    int _28381 = NOVALUE;
    int _28380 = NOVALUE;
    int _28379 = NOVALUE;
    int _28378 = NOVALUE;
    int _28375 = NOVALUE;
    int _28374 = NOVALUE;
    int _28372 = NOVALUE;
    int _28371 = NOVALUE;
    int _28369 = NOVALUE;
    int _28368 = NOVALUE;
    int _28366 = NOVALUE;
    int _28365 = NOVALUE;
    int _0, _1, _2, _3;
    
    if (!IS_ATOM_INT(_sub_54616)) {
        _1 = (long)(DBL_PTR(_sub_54616)->dbl);
        if (UNIQUE(DBL_PTR(_sub_54616)) && (DBL_PTR(_sub_54616)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_sub_54616);
        _sub_54616 = _1;
    }
    if (!IS_ATOM_INT(_start_54617)) {
        _1 = (long)(DBL_PTR(_start_54617)->dbl);
        if (UNIQUE(DBL_PTR(_start_54617)) && (DBL_PTR(_start_54617)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_start_54617);
        _start_54617 = _1;
    }
    if (!IS_ATOM_INT(_deferred_54618)) {
        _1 = (long)(DBL_PTR(_deferred_54618)->dbl);
        if (UNIQUE(DBL_PTR(_deferred_54618)) && (DBL_PTR(_deferred_54618)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_deferred_54618);
        _deferred_54618 = _1;
    }

    /** 	integer is_proc = SymTab[sub][S_TOKEN] = PROC*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28365 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28365);
    if (!IS_ATOM_INT(_38S_TOKEN_16603)){
        _28366 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
    }
    else{
        _28366 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
    }
    _28365 = NOVALUE;
    if (IS_ATOM_INT(_28366)) {
        _is_proc_54619 = (_28366 == 27);
    }
    else {
        _is_proc_54619 = binary_op(EQUALS, _28366, 27);
    }
    _28366 = NOVALUE;
    if (!IS_ATOM_INT(_is_proc_54619)) {
        _1 = (long)(DBL_PTR(_is_proc_54619)->dbl);
        if (UNIQUE(DBL_PTR(_is_proc_54619)) && (DBL_PTR(_is_proc_54619)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_is_proc_54619);
        _is_proc_54619 = _1;
    }

    /** 	clear_inline_targets()*/
    _43clear_inline_targets();

    /** 	inline_temps = {}*/
    RefDS(_22663);
    DeRef(_69inline_temps_53667);
    _69inline_temps_53667 = _22663;

    /** 	inline_params = {}*/
    RefDS(_22663);
    DeRefi(_69inline_params_53670);
    _69inline_params_53670 = _22663;

    /** 	assigned_params      = SymTab[sub][S_INLINE][1]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28368 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28368);
    _28369 = (int)*(((s1_ptr)_2)->base + 29);
    _28368 = NOVALUE;
    DeRef(_69assigned_params_53671);
    _2 = (int)SEQ_PTR(_28369);
    _69assigned_params_53671 = (int)*(((s1_ptr)_2)->base + 1);
    Ref(_69assigned_params_53671);
    _28369 = NOVALUE;

    /** 	inline_code          = SymTab[sub][S_INLINE][2]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28371 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28371);
    _28372 = (int)*(((s1_ptr)_2)->base + 29);
    _28371 = NOVALUE;
    DeRef(_69inline_code_53665);
    _2 = (int)SEQ_PTR(_28372);
    _69inline_code_53665 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_69inline_code_53665);
    _28372 = NOVALUE;

    /** 	sequence backpatches = SymTab[sub][S_INLINE][3]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28374 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28374);
    _28375 = (int)*(((s1_ptr)_2)->base + 29);
    _28374 = NOVALUE;
    DeRef(_backpatches_54637);
    _2 = (int)SEQ_PTR(_28375);
    _backpatches_54637 = (int)*(((s1_ptr)_2)->base + 3);
    Ref(_backpatches_54637);
    _28375 = NOVALUE;

    /** 	passed_params = {}*/
    RefDS(_22663);
    DeRef(_69passed_params_53668);
    _69passed_params_53668 = _22663;

    /** 	original_params = {}*/
    RefDS(_22663);
    DeRef(_69original_params_53669);
    _69original_params_53669 = _22663;

    /** 	proc_vars = {}*/
    RefDS(_22663);
    DeRefi(_69proc_vars_53666);
    _69proc_vars_53666 = _22663;

    /** 	sequence prolog = {}*/
    RefDS(_22663);
    DeRefi(_prolog_54643);
    _prolog_54643 = _22663;

    /** 	sequence epilog = {}*/
    RefDS(_22663);
    DeRef(_epilog_54644);
    _epilog_54644 = _22663;

    /** 	Start_block( EXIT_BLOCK, sprintf("Inline-%s from %s @ %d", */
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28378 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28378);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _28379 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _28379 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _28378 = NOVALUE;
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28380 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_28380);
    if (!IS_ATOM_INT(_38S_NAME_16598)){
        _28381 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NAME_16598)->dbl));
    }
    else{
        _28381 = (int)*(((s1_ptr)_2)->base + _38S_NAME_16598);
    }
    _28380 = NOVALUE;
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    Ref(_28379);
    *((int *)(_2+4)) = _28379;
    Ref(_28381);
    *((int *)(_2+8)) = _28381;
    *((int *)(_2+12)) = _start_54617;
    _28382 = MAKE_SEQ(_1);
    _28381 = NOVALUE;
    _28379 = NOVALUE;
    _28383 = EPrintf(-9999999, _28377, _28382);
    DeRefDS(_28382);
    _28382 = NOVALUE;
    _68Start_block(206, _28383);
    _28383 = NOVALUE;

    /** 	symtab_index s = SymTab[sub][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28384 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28384);
    _s_54660 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54660)){
        _s_54660 = (long)DBL_PTR(_s_54660)->dbl;
    }
    _28384 = NOVALUE;

    /** 	varnum = SymTab[CurrentSub][S_NUM_ARGS]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28386 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_28386);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _69varnum_53676 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _69varnum_53676 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    if (!IS_ATOM_INT(_69varnum_53676)){
        _69varnum_53676 = (long)DBL_PTR(_69varnum_53676)->dbl;
    }
    _28386 = NOVALUE;

    /** 	inline_start = start*/
    _69inline_start_53677 = _start_54617;

    /** 	last_param = CurrentSub*/
    _69last_param_53680 = _38CurrentSub_16954;

    /** 	for p = 1 to SymTab[CurrentSub][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28388 = (int)*(((s1_ptr)_2)->base + _38CurrentSub_16954);
    _2 = (int)SEQ_PTR(_28388);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _28389 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _28389 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _28388 = NOVALUE;
    {
        int _p_54672;
        _p_54672 = 1;
L1: 
        if (binary_op_a(GREATER, _p_54672, _28389)){
            goto L2; // [250] 282
        }

        /** 		last_param = SymTab[last_param][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28390 = (int)*(((s1_ptr)_2)->base + _69last_param_53680);
        _2 = (int)SEQ_PTR(_28390);
        _69last_param_53680 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_69last_param_53680)){
            _69last_param_53680 = (long)DBL_PTR(_69last_param_53680)->dbl;
        }
        _28390 = NOVALUE;

        /** 	end for*/
        _0 = _p_54672;
        if (IS_ATOM_INT(_p_54672)) {
            _p_54672 = _p_54672 + 1;
            if ((long)((unsigned long)_p_54672 +(unsigned long) HIGH_BITS) >= 0){
                _p_54672 = NewDouble((double)_p_54672);
            }
        }
        else {
            _p_54672 = binary_op_a(PLUS, _p_54672, 1);
        }
        DeRef(_0);
        goto L1; // [277] 257
L2: 
        ;
        DeRef(_p_54672);
    }

    /** 	symtab_index last_sym = last_param*/
    _last_sym_54683 = _69last_param_53680;

    /** 	while last_sym and */
L3: 
    if (_last_sym_54683 == 0) {
        goto L4; // [296] 368
    }
    _28393 = _55sym_scope(_last_sym_54683);
    if (IS_ATOM_INT(_28393)) {
        _28394 = (_28393 <= 3);
    }
    else {
        _28394 = binary_op(LESSEQ, _28393, 3);
    }
    DeRef(_28393);
    _28393 = NOVALUE;
    if (IS_ATOM_INT(_28394)) {
        if (_28394 != 0) {
            DeRef(_28395);
            _28395 = 1;
            goto L5; // [310] 328
        }
    }
    else {
        if (DBL_PTR(_28394)->dbl != 0.0) {
            DeRef(_28395);
            _28395 = 1;
            goto L5; // [310] 328
        }
    }
    _28396 = _55sym_scope(_last_sym_54683);
    if (IS_ATOM_INT(_28396)) {
        _28397 = (_28396 == 9);
    }
    else {
        _28397 = binary_op(EQUALS, _28396, 9);
    }
    DeRef(_28396);
    _28396 = NOVALUE;
    DeRef(_28395);
    if (IS_ATOM_INT(_28397))
    _28395 = (_28397 != 0);
    else
    _28395 = DBL_PTR(_28397)->dbl != 0.0;
L5: 
    if (_28395 == 0)
    {
        _28395 = NOVALUE;
        goto L4; // [329] 368
    }
    else{
        _28395 = NOVALUE;
    }

    /** 		last_param = last_sym*/
    _69last_param_53680 = _last_sym_54683;

    /** 		last_sym = SymTab[last_sym][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28398 = (int)*(((s1_ptr)_2)->base + _last_sym_54683);
    _2 = (int)SEQ_PTR(_28398);
    _last_sym_54683 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_last_sym_54683)){
        _last_sym_54683 = (long)DBL_PTR(_last_sym_54683)->dbl;
    }
    _28398 = NOVALUE;

    /** 		varnum += 1*/
    _69varnum_53676 = _69varnum_53676 + 1;

    /** 	end while*/
    goto L3; // [365] 296
L4: 

    /** 	for p = SymTab[sub][S_NUM_ARGS] to 1 by -1 do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28401 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28401);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _28402 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _28402 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _28401 = NOVALUE;
    {
        int _p_54701;
        Ref(_28402);
        _p_54701 = _28402;
L6: 
        if (binary_op_a(LESS, _p_54701, 1)){
            goto L7; // [382] 407
        }

        /** 		passed_params = prepend( passed_params, Pop() )*/
        _28403 = _43Pop();
        Ref(_28403);
        Prepend(&_69passed_params_53668, _69passed_params_53668, _28403);
        DeRef(_28403);
        _28403 = NOVALUE;

        /** 	end for*/
        _0 = _p_54701;
        if (IS_ATOM_INT(_p_54701)) {
            _p_54701 = _p_54701 + -1;
            if ((long)((unsigned long)_p_54701 +(unsigned long) HIGH_BITS) >= 0){
                _p_54701 = NewDouble((double)_p_54701);
            }
        }
        else {
            _p_54701 = binary_op_a(PLUS, _p_54701, -1);
        }
        DeRef(_0);
        goto L6; // [402] 389
L7: 
        ;
        DeRef(_p_54701);
    }

    /** 	original_params = passed_params*/
    RefDS(_69passed_params_53668);
    DeRef(_69original_params_53669);
    _69original_params_53669 = _69passed_params_53668;

    /** 	symtab_index int_sym = 0*/
    _int_sym_54710 = 0;

    /** 	for p = 1 to SymTab[sub][S_NUM_ARGS] do*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28405 = (int)*(((s1_ptr)_2)->base + _sub_54616);
    _2 = (int)SEQ_PTR(_28405);
    if (!IS_ATOM_INT(_38S_NUM_ARGS_16649)){
        _28406 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_NUM_ARGS_16649)->dbl));
    }
    else{
        _28406 = (int)*(((s1_ptr)_2)->base + _38S_NUM_ARGS_16649);
    }
    _28405 = NOVALUE;
    {
        int _p_54712;
        _p_54712 = 1;
L8: 
        if (binary_op_a(GREATER, _p_54712, _28406)){
            goto L9; // [437] 575
        }

        /** 		symtab_index param = passed_params[p]*/
        _2 = (int)SEQ_PTR(_69passed_params_53668);
        if (!IS_ATOM_INT(_p_54712)){
            _param_54718 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_p_54712)->dbl));
        }
        else{
            _param_54718 = (int)*(((s1_ptr)_2)->base + _p_54712);
        }
        if (!IS_ATOM_INT(_param_54718)){
            _param_54718 = (long)DBL_PTR(_param_54718)->dbl;
        }

        /** 		inline_params &= s*/
        Append(&_69inline_params_53670, _69inline_params_53670, _s_54660);

        /** 		integer ax = find( p, assigned_params )*/
        _ax_54721 = find_from(_p_54712, _69assigned_params_53671, 1);

        /** 		if ax or is_temp( param ) then*/
        if (_ax_54721 != 0) {
            goto LA; // [473] 486
        }
        _28411 = _69is_temp(_param_54718);
        if (_28411 == 0) {
            DeRef(_28411);
            _28411 = NOVALUE;
            goto LB; // [482] 548
        }
        else {
            if (!IS_ATOM_INT(_28411) && DBL_PTR(_28411)->dbl == 0.0){
                DeRef(_28411);
                _28411 = NOVALUE;
                goto LB; // [482] 548
            }
            DeRef(_28411);
            _28411 = NOVALUE;
        }
        DeRef(_28411);
        _28411 = NOVALUE;
LA: 

        /** 			varnum += 1*/
        _69varnum_53676 = _69varnum_53676 + 1;

        /** 			symtab_index var = new_inline_var( s, 0 )*/
        _var_54728 = _69new_inline_var(_s_54660, 0);
        if (!IS_ATOM_INT(_var_54728)) {
            _1 = (long)(DBL_PTR(_var_54728)->dbl);
            if (UNIQUE(DBL_PTR(_var_54728)) && (DBL_PTR(_var_54728)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_var_54728);
            _var_54728 = _1;
        }

        /** 			prolog &= {ASSIGN, param, var}*/
        _1 = NewS1(3);
        _2 = (int)((s1_ptr)_1)->base;
        *((int *)(_2+4)) = 18;
        *((int *)(_2+8)) = _param_54718;
        *((int *)(_2+12)) = _var_54728;
        _28414 = MAKE_SEQ(_1);
        Concat((object_ptr)&_prolog_54643, _prolog_54643, _28414);
        DeRefDS(_28414);
        _28414 = NOVALUE;

        /** 			if not int_sym then*/
        if (_int_sym_54710 != 0)
        goto LC; // [519] 531

        /** 				int_sym = NewIntSym( 0 )*/
        _int_sym_54710 = _55NewIntSym(0);
        if (!IS_ATOM_INT(_int_sym_54710)) {
            _1 = (long)(DBL_PTR(_int_sym_54710)->dbl);
            if (UNIQUE(DBL_PTR(_int_sym_54710)) && (DBL_PTR(_int_sym_54710)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_int_sym_54710);
            _int_sym_54710 = _1;
        }
LC: 

        /** 			inline_start += 3*/
        _69inline_start_53677 = _69inline_start_53677 + 3;

        /** 			passed_params[p] = var*/
        _2 = (int)SEQ_PTR(_69passed_params_53668);
        if (!UNIQUE(_2)) {
            _2 = (int)SequenceCopy((s1_ptr)_2);
            _69passed_params_53668 = MAKE_SEQ(_2);
        }
        if (!IS_ATOM_INT(_p_54712))
        _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_p_54712)->dbl));
        else
        _2 = (int)(((s1_ptr)_2)->base + _p_54712);
        _1 = *(int *)_2;
        *(int *)_2 = _var_54728;
        DeRef(_1);
LB: 

        /** 		s = SymTab[s][S_NEXT]*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28419 = (int)*(((s1_ptr)_2)->base + _s_54660);
        _2 = (int)SEQ_PTR(_28419);
        _s_54660 = (int)*(((s1_ptr)_2)->base + 2);
        if (!IS_ATOM_INT(_s_54660)){
            _s_54660 = (long)DBL_PTR(_s_54660)->dbl;
        }
        _28419 = NOVALUE;

        /** 	end for*/
        _0 = _p_54712;
        if (IS_ATOM_INT(_p_54712)) {
            _p_54712 = _p_54712 + 1;
            if ((long)((unsigned long)_p_54712 +(unsigned long) HIGH_BITS) >= 0){
                _p_54712 = NewDouble((double)_p_54712);
            }
        }
        else {
            _p_54712 = binary_op_a(PLUS, _p_54712, 1);
        }
        DeRef(_0);
        goto L8; // [570] 444
L9: 
        ;
        DeRef(_p_54712);
    }

    /** 	symtab_index final_target = 0*/
    _final_target_54743 = 0;

    /** 	while s and */
LD: 
    if (_s_54660 == 0) {
        goto LE; // [587] 699
    }
    _28422 = _55sym_scope(_s_54660);
    if (IS_ATOM_INT(_28422)) {
        _28423 = (_28422 <= 3);
    }
    else {
        _28423 = binary_op(LESSEQ, _28422, 3);
    }
    DeRef(_28422);
    _28422 = NOVALUE;
    if (IS_ATOM_INT(_28423)) {
        if (_28423 != 0) {
            DeRef(_28424);
            _28424 = 1;
            goto LF; // [601] 619
        }
    }
    else {
        if (DBL_PTR(_28423)->dbl != 0.0) {
            DeRef(_28424);
            _28424 = 1;
            goto LF; // [601] 619
        }
    }
    _28425 = _55sym_scope(_s_54660);
    if (IS_ATOM_INT(_28425)) {
        _28426 = (_28425 == 9);
    }
    else {
        _28426 = binary_op(EQUALS, _28425, 9);
    }
    DeRef(_28425);
    _28425 = NOVALUE;
    DeRef(_28424);
    if (IS_ATOM_INT(_28426))
    _28424 = (_28426 != 0);
    else
    _28424 = DBL_PTR(_28426)->dbl != 0.0;
LF: 
    if (_28424 == 0)
    {
        _28424 = NOVALUE;
        goto LE; // [620] 699
    }
    else{
        _28424 = NOVALUE;
    }

    /** 		if sym_scope( s ) != SC_UNDEFINED then*/
    _28427 = _55sym_scope(_s_54660);
    if (binary_op_a(EQUALS, _28427, 9)){
        DeRef(_28427);
        _28427 = NOVALUE;
        goto L10; // [631] 676
    }
    DeRef(_28427);
    _28427 = NOVALUE;

    /** 			varnum += 1*/
    _69varnum_53676 = _69varnum_53676 + 1;

    /** 			symtab_index var = new_inline_var( s, 0 )*/
    _var_54762 = _69new_inline_var(_s_54660, 0);
    if (!IS_ATOM_INT(_var_54762)) {
        _1 = (long)(DBL_PTR(_var_54762)->dbl);
        if (UNIQUE(DBL_PTR(_var_54762)) && (DBL_PTR(_var_54762)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_var_54762);
        _var_54762 = _1;
    }

    /** 			proc_vars &= var*/
    Append(&_69proc_vars_53666, _69proc_vars_53666, _var_54762);

    /** 			if int_sym = 0 then*/
    if (_int_sym_54710 != 0)
    goto L11; // [662] 675

    /** 				int_sym = NewIntSym( 0 )*/
    _int_sym_54710 = _55NewIntSym(0);
    if (!IS_ATOM_INT(_int_sym_54710)) {
        _1 = (long)(DBL_PTR(_int_sym_54710)->dbl);
        if (UNIQUE(DBL_PTR(_int_sym_54710)) && (DBL_PTR(_int_sym_54710)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_int_sym_54710);
        _int_sym_54710 = _1;
    }
L11: 
L10: 

    /** 		s = SymTab[s][S_NEXT]*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28434 = (int)*(((s1_ptr)_2)->base + _s_54660);
    _2 = (int)SEQ_PTR(_28434);
    _s_54660 = (int)*(((s1_ptr)_2)->base + 2);
    if (!IS_ATOM_INT(_s_54660)){
        _s_54660 = (long)DBL_PTR(_s_54660)->dbl;
    }
    _28434 = NOVALUE;

    /** 	end while*/
    goto LD; // [696] 587
LE: 

    /** 	if not is_proc then*/
    if (_is_proc_54619 != 0)
    goto L12; // [701] 831

    /** 		integer create_target_var = 1*/
    _create_target_var_54775 = 1;

    /** 		if deferred then*/
    if (_deferred_54618 == 0)
    {
        goto L13; // [711] 751
    }
    else{
    }

    /** 			inline_target = Pop()*/
    _0 = _43Pop();
    _69inline_target_53672 = _0;
    if (!IS_ATOM_INT(_69inline_target_53672)) {
        _1 = (long)(DBL_PTR(_69inline_target_53672)->dbl);
        if (UNIQUE(DBL_PTR(_69inline_target_53672)) && (DBL_PTR(_69inline_target_53672)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_69inline_target_53672);
        _69inline_target_53672 = _1;
    }

    /** 			if is_temp( inline_target ) then*/
    _28438 = _69is_temp(_69inline_target_53672);
    if (_28438 == 0) {
        DeRef(_28438);
        _28438 = NOVALUE;
        goto L14; // [729] 744
    }
    else {
        if (!IS_ATOM_INT(_28438) && DBL_PTR(_28438)->dbl == 0.0){
            DeRef(_28438);
            _28438 = NOVALUE;
            goto L14; // [729] 744
        }
        DeRef(_28438);
        _28438 = NOVALUE;
    }
    DeRef(_28438);
    _28438 = NOVALUE;

    /** 				final_target = inline_target*/
    _final_target_54743 = _69inline_target_53672;
    goto L15; // [741] 750
L14: 

    /** 				create_target_var = 0*/
    _create_target_var_54775 = 0;
L15: 
L13: 

    /** 		if create_target_var then*/
    if (_create_target_var_54775 == 0)
    {
        goto L16; // [753] 816
    }
    else{
    }

    /** 			varnum += 1*/
    _69varnum_53676 = _69varnum_53676 + 1;

    /** 			if TRANSLATE then*/
    if (_38TRANSLATE_16564 == 0)
    {
        goto L17; // [768] 806
    }
    else{
    }

    /** 				inline_target = new_inline_var( sub, 0 )*/
    _0 = _69new_inline_var(_sub_54616, 0);
    _69inline_target_53672 = _0;
    if (!IS_ATOM_INT(_69inline_target_53672)) {
        _1 = (long)(DBL_PTR(_69inline_target_53672)->dbl);
        if (UNIQUE(DBL_PTR(_69inline_target_53672)) && (DBL_PTR(_69inline_target_53672)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_69inline_target_53672);
        _69inline_target_53672 = _1;
    }

    /** 				SymTab[inline_target][S_VTYPE] = object_type*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _35SymTab_15595 = MAKE_SEQ(_2);
    }
    _3 = (int)(_69inline_target_53672 + ((s1_ptr)_2)->base);
    _2 = (int)SEQ_PTR(*(int *)_3);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        *(int *)_3 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + 15);
    _1 = *(int *)_2;
    *(int *)_2 = _55object_type_47055;
    DeRef(_1);
    _28441 = NOVALUE;

    /** 				Pop_block_var()*/
    _68Pop_block_var();
    goto L18; // [803] 815
L17: 

    /** 				inline_target = NewTempSym()*/
    _0 = _55NewTempSym(0);
    _69inline_target_53672 = _0;
    if (!IS_ATOM_INT(_69inline_target_53672)) {
        _1 = (long)(DBL_PTR(_69inline_target_53672)->dbl);
        if (UNIQUE(DBL_PTR(_69inline_target_53672)) && (DBL_PTR(_69inline_target_53672)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_69inline_target_53672);
        _69inline_target_53672 = _1;
    }
L18: 
L16: 

    /** 		proc_vars &= inline_target*/
    Append(&_69proc_vars_53666, _69proc_vars_53666, _69inline_target_53672);
    goto L19; // [828] 837
L12: 

    /** 		inline_target = 0*/
    _69inline_target_53672 = 0;
L19: 

    /** 	integer check_pc = 1*/
    _check_pc_54798 = 1;

    /** 	while length(inline_code) > check_pc do*/
L1A: 
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28445 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28445 = 1;
    }
    if (_28445 <= _check_pc_54798)
    goto L1B; // [852] 1216

    /** 		integer op = inline_code[check_pc]*/
    _2 = (int)SEQ_PTR(_69inline_code_53665);
    _op_54802 = (int)*(((s1_ptr)_2)->base + _check_pc_54798);
    if (!IS_ATOM_INT(_op_54802))
    _op_54802 = (long)DBL_PTR(_op_54802)->dbl;

    /** 		switch op with fallthru do*/
    _0 = _op_54802;
    switch ( _0 ){ 

        /** 			case ATOM_CHECK then*/
        case 101:
        case 97:
        case 96:

        /** 				symtab_index sym = get_original_sym( check_pc + 1 )*/
        _28450 = _check_pc_54798 + 1;
        if (_28450 > MAXINT){
            _28450 = NewDouble((double)_28450);
        }
        _sym_54811 = _69get_original_sym(_28450);
        _28450 = NOVALUE;
        if (!IS_ATOM_INT(_sym_54811)) {
            _1 = (long)(DBL_PTR(_sym_54811)->dbl);
            if (UNIQUE(DBL_PTR(_sym_54811)) && (DBL_PTR(_sym_54811)->cleanup != 0))
            RTFatal("Cannot assign value with a destructor to an integer");            DeRefDS(_sym_54811);
            _sym_54811 = _1;
        }

        /** 				if is_literal( sym ) then*/
        _28452 = _69is_literal(_sym_54811);
        if (_28452 == 0) {
            DeRef(_28452);
            _28452 = NOVALUE;
            goto L1C; // [897] 1010
        }
        else {
            if (!IS_ATOM_INT(_28452) && DBL_PTR(_28452)->dbl == 0.0){
                DeRef(_28452);
                _28452 = NOVALUE;
                goto L1C; // [897] 1010
            }
            DeRef(_28452);
            _28452 = NOVALUE;
        }
        DeRef(_28452);
        _28452 = NOVALUE;

        /** 					integer check_result*/

        /** 					if op = INTEGER_CHECK then*/
        if (_op_54802 != 96)
        goto L1D; // [906] 930

        /** 						check_result = integer( SymTab[sym][S_OBJ] )*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28454 = (int)*(((s1_ptr)_2)->base + _sym_54811);
        _2 = (int)SEQ_PTR(_28454);
        _28455 = (int)*(((s1_ptr)_2)->base + 1);
        _28454 = NOVALUE;
        if (IS_ATOM_INT(_28455))
        _check_result_54816 = 1;
        else if (IS_ATOM_DBL(_28455))
        _check_result_54816 = IS_ATOM_INT(DoubleToInt(_28455));
        else
        _check_result_54816 = 0;
        _28455 = NOVALUE;
        goto L1E; // [927] 976
L1D: 

        /** 					elsif op = SEQUENCE_CHECK then*/
        if (_op_54802 != 97)
        goto L1F; // [934] 958

        /** 						check_result = sequence( SymTab[sym][S_OBJ] )*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28458 = (int)*(((s1_ptr)_2)->base + _sym_54811);
        _2 = (int)SEQ_PTR(_28458);
        _28459 = (int)*(((s1_ptr)_2)->base + 1);
        _28458 = NOVALUE;
        _check_result_54816 = IS_SEQUENCE(_28459);
        _28459 = NOVALUE;
        goto L1E; // [955] 976
L1F: 

        /** 						check_result = atom( SymTab[sym][S_OBJ] )*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28461 = (int)*(((s1_ptr)_2)->base + _sym_54811);
        _2 = (int)SEQ_PTR(_28461);
        _28462 = (int)*(((s1_ptr)_2)->base + 1);
        _28461 = NOVALUE;
        _check_result_54816 = IS_ATOM(_28462);
        _28462 = NOVALUE;
L1E: 

        /** 					if check_result then*/
        if (_check_result_54816 == 0)
        {
            goto L20; // [980] 997
        }
        else{
        }

        /** 						replace_code( {}, check_pc, check_pc+1 )*/
        _28464 = _check_pc_54798 + 1;
        if (_28464 > MAXINT){
            _28464 = NewDouble((double)_28464);
        }
        RefDS(_22663);
        _69replace_code(_22663, _check_pc_54798, _28464);
        _28464 = NOVALUE;
        goto L21; // [994] 1005
L20: 

        /** 						CompileErr(146)*/
        RefDS(_22663);
        _46CompileErr(146, _22663, 0);
L21: 
        goto L22; // [1007] 1172
L1C: 

        /** 				elsif not is_temp( sym ) then*/
        _28466 = _69is_temp(_sym_54811);
        if (IS_ATOM_INT(_28466)) {
            if (_28466 != 0){
                DeRef(_28466);
                _28466 = NOVALUE;
                goto L23; // [1016] 1165
            }
        }
        else {
            if (DBL_PTR(_28466)->dbl != 0.0){
                DeRef(_28466);
                _28466 = NOVALUE;
                goto L23; // [1016] 1165
            }
        }
        DeRef(_28466);
        _28466 = NOVALUE;

        /** 					if (op = INTEGER_CHECK and SymTab[sym][S_VTYPE] = integer_type )*/
        _28468 = (_op_54802 == 96);
        if (_28468 == 0) {
            _28469 = 0;
            goto L24; // [1027] 1053
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28470 = (int)*(((s1_ptr)_2)->base + _sym_54811);
        _2 = (int)SEQ_PTR(_28470);
        _28471 = (int)*(((s1_ptr)_2)->base + 15);
        _28470 = NOVALUE;
        if (IS_ATOM_INT(_28471)) {
            _28472 = (_28471 == _55integer_type_47061);
        }
        else {
            _28472 = binary_op(EQUALS, _28471, _55integer_type_47061);
        }
        _28471 = NOVALUE;
        if (IS_ATOM_INT(_28472))
        _28469 = (_28472 != 0);
        else
        _28469 = DBL_PTR(_28472)->dbl != 0.0;
L24: 
        if (_28469 != 0) {
            _28473 = 1;
            goto L25; // [1053] 1093
        }
        _28474 = (_op_54802 == 97);
        if (_28474 == 0) {
            _28475 = 0;
            goto L26; // [1063] 1089
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28476 = (int)*(((s1_ptr)_2)->base + _sym_54811);
        _2 = (int)SEQ_PTR(_28476);
        _28477 = (int)*(((s1_ptr)_2)->base + 15);
        _28476 = NOVALUE;
        if (IS_ATOM_INT(_28477)) {
            _28478 = (_28477 == _55sequence_type_47059);
        }
        else {
            _28478 = binary_op(EQUALS, _28477, _55sequence_type_47059);
        }
        _28477 = NOVALUE;
        if (IS_ATOM_INT(_28478))
        _28475 = (_28478 != 0);
        else
        _28475 = DBL_PTR(_28478)->dbl != 0.0;
L26: 
        _28473 = (_28475 != 0);
L25: 
        if (_28473 != 0) {
            goto L27; // [1093] 1141
        }
        _28480 = (_op_54802 == 101);
        if (_28480 == 0) {
            DeRef(_28481);
            _28481 = 0;
            goto L28; // [1103] 1136
        }
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28482 = (int)*(((s1_ptr)_2)->base + _sym_54811);
        _2 = (int)SEQ_PTR(_28482);
        _28483 = (int)*(((s1_ptr)_2)->base + 15);
        _28482 = NOVALUE;
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = _55integer_type_47061;
        ((int *)_2)[2] = _55atom_type_47057;
        _28484 = MAKE_SEQ(_1);
        _28485 = find_from(_28483, _28484, 1);
        _28483 = NOVALUE;
        DeRefDS(_28484);
        _28484 = NOVALUE;
        _28481 = (_28485 != 0);
L28: 
        if (_28481 == 0)
        {
            _28481 = NOVALUE;
            goto L29; // [1137] 1155
        }
        else{
            _28481 = NOVALUE;
        }
L27: 

        /** 						replace_code( {}, check_pc, check_pc+1 )*/
        _28486 = _check_pc_54798 + 1;
        if (_28486 > MAXINT){
            _28486 = NewDouble((double)_28486);
        }
        RefDS(_22663);
        _69replace_code(_22663, _check_pc_54798, _28486);
        _28486 = NOVALUE;
        goto L22; // [1152] 1172
L29: 

        /** 						check_pc += 2*/
        _check_pc_54798 = _check_pc_54798 + 2;
        goto L22; // [1162] 1172
L23: 

        /** 					check_pc += 2*/
        _check_pc_54798 = _check_pc_54798 + 2;
L22: 

        /** 				continue*/
        goto L1A; // [1178] 847

        /** 			case STARTLINE then*/
        case 58:

        /** 				check_pc += 2*/
        _check_pc_54798 = _check_pc_54798 + 2;

        /** 				continue*/
        goto L1A; // [1196] 847

        /** 			case else*/
        default:

        /** 				exit*/
        goto L1B; // [1206] 1216
    ;}
    /** 	end while*/
    goto L1A; // [1213] 847
L1B: 

    /** 	for pc = 1 to length( inline_code ) do*/
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28490 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28490 = 1;
    }
    {
        int _pc_54889;
        _pc_54889 = 1;
L2A: 
        if (_pc_54889 > _28490){
            goto L2B; // [1223] 1420
        }

        /** 		if sequence( inline_code[pc] ) then*/
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28491 = (int)*(((s1_ptr)_2)->base + _pc_54889);
        _28492 = IS_SEQUENCE(_28491);
        _28491 = NOVALUE;
        if (_28492 == 0)
        {
            _28492 = NOVALUE;
            goto L2C; // [1241] 1411
        }
        else{
            _28492 = NOVALUE;
        }

        /** 			integer inline_type = inline_code[pc][1]*/
        _2 = (int)SEQ_PTR(_69inline_code_53665);
        _28493 = (int)*(((s1_ptr)_2)->base + _pc_54889);
        _2 = (int)SEQ_PTR(_28493);
        _inline_type_54894 = (int)*(((s1_ptr)_2)->base + 1);
        if (!IS_ATOM_INT(_inline_type_54894)){
            _inline_type_54894 = (long)DBL_PTR(_inline_type_54894)->dbl;
        }
        _28493 = NOVALUE;

        /** 			switch inline_type do*/
        _0 = _inline_type_54894;
        switch ( _0 ){ 

            /** 				case INLINE_SUB then*/
            case 5:

            /** 					inline_code[pc] = CurrentSub*/
            _2 = (int)SEQ_PTR(_69inline_code_53665);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _69inline_code_53665 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54889);
            _1 = *(int *)_2;
            *(int *)_2 = _38CurrentSub_16954;
            DeRef(_1);
            goto L2D; // [1279] 1410

            /** 				case INLINE_VAR then*/
            case 6:

            /** 					replace_var( pc )*/
            _69replace_var(_pc_54889);

            /** 					break*/
            goto L2D; // [1292] 1410
            goto L2D; // [1294] 1410

            /** 				case INLINE_TEMP then*/
            case 2:

            /** 					replace_temp( pc )*/
            _69replace_temp(_pc_54889);
            goto L2D; // [1305] 1410

            /** 				case INLINE_PARAM then*/
            case 1:

            /** 					replace_param( pc )*/

            /** 	inline_code[pc] = get_param_sym( pc )*/
            _0 = _replace_param_1__tmp_at1341_54905;
            _replace_param_1__tmp_at1341_54905 = _69get_param_sym(_pc_54889);
            DeRef(_0);
            Ref(_replace_param_1__tmp_at1341_54905);
            _2 = (int)SEQ_PTR(_69inline_code_53665);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _69inline_code_53665 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54889);
            _1 = *(int *)_2;
            *(int *)_2 = _replace_param_1__tmp_at1341_54905;
            DeRef(_1);

            /** end procedure*/
            goto L2E; // [1327] 1330
L2E: 
            DeRef(_replace_param_1__tmp_at1341_54905);
            _replace_param_1__tmp_at1341_54905 = NOVALUE;
            goto L2D; // [1332] 1410

            /** 				case INLINE_ADDR then*/
            case 4:

            /** 					inline_code[pc] = inline_start + inline_code[pc][2]*/
            _2 = (int)SEQ_PTR(_69inline_code_53665);
            _28497 = (int)*(((s1_ptr)_2)->base + _pc_54889);
            _2 = (int)SEQ_PTR(_28497);
            _28498 = (int)*(((s1_ptr)_2)->base + 2);
            _28497 = NOVALUE;
            if (IS_ATOM_INT(_28498)) {
                _28499 = _69inline_start_53677 + _28498;
                if ((long)((unsigned long)_28499 + (unsigned long)HIGH_BITS) >= 0) 
                _28499 = NewDouble((double)_28499);
            }
            else {
                _28499 = binary_op(PLUS, _69inline_start_53677, _28498);
            }
            _28498 = NOVALUE;
            _2 = (int)SEQ_PTR(_69inline_code_53665);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _69inline_code_53665 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54889);
            _1 = *(int *)_2;
            *(int *)_2 = _28499;
            if( _1 != _28499 ){
                DeRef(_1);
            }
            _28499 = NOVALUE;
            goto L2D; // [1362] 1410

            /** 				case INLINE_TARGET then*/
            case 3:

            /** 					inline_code[pc] = inline_target*/
            _2 = (int)SEQ_PTR(_69inline_code_53665);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _69inline_code_53665 = MAKE_SEQ(_2);
            }
            _2 = (int)(((s1_ptr)_2)->base + _pc_54889);
            _1 = *(int *)_2;
            *(int *)_2 = _69inline_target_53672;
            DeRef(_1);

            /** 					add_inline_target( pc + inline_start )*/
            _28500 = _pc_54889 + _69inline_start_53677;
            if ((long)((unsigned long)_28500 + (unsigned long)HIGH_BITS) >= 0) 
            _28500 = NewDouble((double)_28500);
            _43add_inline_target(_28500);
            _28500 = NOVALUE;

            /** 					break*/
            goto L2D; // [1391] 1410
            goto L2D; // [1393] 1410

            /** 				case else*/
            default:

            /** 					InternalErr( 265, {inline_type} )*/
            _1 = NewS1(1);
            _2 = (int)((s1_ptr)_1)->base;
            *((int *)(_2+4)) = _inline_type_54894;
            _28501 = MAKE_SEQ(_1);
            _46InternalErr(265, _28501);
            _28501 = NOVALUE;
        ;}L2D: 
L2C: 

        /** 	end for*/
        _pc_54889 = _pc_54889 + 1;
        goto L2A; // [1415] 1230
L2B: 
        ;
    }

    /** 	for i = 1 to length(backpatches) do*/
    if (IS_SEQUENCE(_backpatches_54637)){
            _28502 = SEQ_PTR(_backpatches_54637)->length;
    }
    else {
        _28502 = 1;
    }
    {
        int _i_54917;
        _i_54917 = 1;
L2F: 
        if (_i_54917 > _28502){
            goto L30; // [1425] 1448
        }

        /** 		fixup_special_op( backpatches[i] )*/
        _2 = (int)SEQ_PTR(_backpatches_54637);
        _28503 = (int)*(((s1_ptr)_2)->base + _i_54917);
        Ref(_28503);
        _69fixup_special_op(_28503);
        _28503 = NOVALUE;

        /** 	end for*/
        _i_54917 = _i_54917 + 1;
        goto L2F; // [1443] 1432
L30: 
        ;
    }

    /** 	epilog &= End_inline_block( EXIT_BLOCK )*/
    _28504 = _68End_inline_block(206);
    if (IS_SEQUENCE(_epilog_54644) && IS_ATOM(_28504)) {
        Ref(_28504);
        Append(&_epilog_54644, _epilog_54644, _28504);
    }
    else if (IS_ATOM(_epilog_54644) && IS_SEQUENCE(_28504)) {
    }
    else {
        Concat((object_ptr)&_epilog_54644, _epilog_54644, _28504);
    }
    DeRef(_28504);
    _28504 = NOVALUE;

    /** 	if is_proc then*/
    if (_is_proc_54619 == 0)
    {
        goto L31; // [1462] 1472
    }
    else{
    }

    /** 		clear_op()*/
    _43clear_op();
    goto L32; // [1469] 1595
L31: 

    /** 		if not deferred then*/
    if (_deferred_54618 != 0)
    goto L33; // [1474] 1489

    /** 			Push( inline_target )*/
    _43Push(_69inline_target_53672);

    /** 			inlined_function()*/
    _43inlined_function();
L33: 

    /** 		if final_target then*/
    if (_final_target_54743 == 0)
    {
        goto L34; // [1491] 1521
    }
    else{
    }

    /** 			epilog &= { ASSIGN, inline_target, final_target }*/
    _1 = NewS1(3);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 18;
    *((int *)(_2+8)) = _69inline_target_53672;
    *((int *)(_2+12)) = _final_target_54743;
    _28507 = MAKE_SEQ(_1);
    Concat((object_ptr)&_epilog_54644, _epilog_54644, _28507);
    DeRefDS(_28507);
    _28507 = NOVALUE;

    /** 			emit_temp( final_target, NEW_REFERENCE )*/
    _43emit_temp(_final_target_54743, 1);
    goto L35; // [1518] 1594
L34: 

    /** 			emit_temp( inline_target, NEW_REFERENCE )*/
    _43emit_temp(_69inline_target_53672, 1);

    /** 			if not TRANSLATE then*/
    if (_38TRANSLATE_16564 != 0)
    goto L36; // [1535] 1593

    /** 				epilog &= { ELSE, 0, PRIVATE_INIT_CHECK, inline_target }*/
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 23;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = 30;
    *((int *)(_2+16)) = _69inline_target_53672;
    _28510 = MAKE_SEQ(_1);
    Concat((object_ptr)&_epilog_54644, _epilog_54644, _28510);
    DeRefDS(_28510);
    _28510 = NOVALUE;

    /** 				epilog[$-2] = length(inline_code) + length(epilog) + inline_start + 1*/
    if (IS_SEQUENCE(_epilog_54644)){
            _28512 = SEQ_PTR(_epilog_54644)->length;
    }
    else {
        _28512 = 1;
    }
    _28513 = _28512 - 2;
    _28512 = NOVALUE;
    if (IS_SEQUENCE(_69inline_code_53665)){
            _28514 = SEQ_PTR(_69inline_code_53665)->length;
    }
    else {
        _28514 = 1;
    }
    if (IS_SEQUENCE(_epilog_54644)){
            _28515 = SEQ_PTR(_epilog_54644)->length;
    }
    else {
        _28515 = 1;
    }
    _28516 = _28514 + _28515;
    if ((long)((unsigned long)_28516 + (unsigned long)HIGH_BITS) >= 0) 
    _28516 = NewDouble((double)_28516);
    _28514 = NOVALUE;
    _28515 = NOVALUE;
    if (IS_ATOM_INT(_28516)) {
        _28517 = _28516 + _69inline_start_53677;
        if ((long)((unsigned long)_28517 + (unsigned long)HIGH_BITS) >= 0) 
        _28517 = NewDouble((double)_28517);
    }
    else {
        _28517 = NewDouble(DBL_PTR(_28516)->dbl + (double)_69inline_start_53677);
    }
    DeRef(_28516);
    _28516 = NOVALUE;
    if (IS_ATOM_INT(_28517)) {
        _28518 = _28517 + 1;
        if (_28518 > MAXINT){
            _28518 = NewDouble((double)_28518);
        }
    }
    else
    _28518 = binary_op(PLUS, 1, _28517);
    DeRef(_28517);
    _28517 = NOVALUE;
    _2 = (int)SEQ_PTR(_epilog_54644);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _epilog_54644 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _28513);
    _1 = *(int *)_2;
    *(int *)_2 = _28518;
    if( _1 != _28518 ){
        DeRef(_1);
    }
    _28518 = NOVALUE;
L36: 
L35: 
L32: 

    /** 	return prolog & inline_code & epilog*/
    {
        int concat_list[3];

        concat_list[0] = _epilog_54644;
        concat_list[1] = _69inline_code_53665;
        concat_list[2] = _prolog_54643;
        Concat_N((object_ptr)&_28519, concat_list, 3);
    }
    DeRef(_backpatches_54637);
    DeRefDSi(_prolog_54643);
    DeRefDS(_epilog_54644);
    _28389 = NOVALUE;
    _28402 = NOVALUE;
    DeRef(_28394);
    _28394 = NOVALUE;
    DeRef(_28397);
    _28397 = NOVALUE;
    _28406 = NOVALUE;
    DeRef(_28468);
    _28468 = NOVALUE;
    DeRef(_28423);
    _28423 = NOVALUE;
    DeRef(_28426);
    _28426 = NOVALUE;
    DeRef(_28474);
    _28474 = NOVALUE;
    DeRef(_28472);
    _28472 = NOVALUE;
    DeRef(_28480);
    _28480 = NOVALUE;
    DeRef(_28478);
    _28478 = NOVALUE;
    DeRef(_28513);
    _28513 = NOVALUE;
    return _28519;
    ;
}


void _69defer_call()
{
    int _defer_54957 = NOVALUE;
    int _28522 = NOVALUE;
    int _28521 = NOVALUE;
    int _0, _1, _2;
    

    /** 	integer defer = find( inline_sub, deferred_inline_decisions )*/
    _defer_54957 = find_from(_69inline_sub_53679, _69deferred_inline_decisions_53681, 1);

    /** 	if defer then*/
    if (_defer_54957 == 0)
    {
        goto L1; // [14] 36
    }
    else{
    }

    /** 		deferred_inline_calls[defer] &= CurrentSub*/
    _2 = (int)SEQ_PTR(_69deferred_inline_calls_53682);
    _28521 = (int)*(((s1_ptr)_2)->base + _defer_54957);
    if (IS_SEQUENCE(_28521) && IS_ATOM(_38CurrentSub_16954)) {
        Append(&_28522, _28521, _38CurrentSub_16954);
    }
    else if (IS_ATOM(_28521) && IS_SEQUENCE(_38CurrentSub_16954)) {
    }
    else {
        Concat((object_ptr)&_28522, _28521, _38CurrentSub_16954);
        _28521 = NOVALUE;
    }
    _28521 = NOVALUE;
    _2 = (int)SEQ_PTR(_69deferred_inline_calls_53682);
    if (!UNIQUE(_2)) {
        _2 = (int)SequenceCopy((s1_ptr)_2);
        _69deferred_inline_calls_53682 = MAKE_SEQ(_2);
    }
    _2 = (int)(((s1_ptr)_2)->base + _defer_54957);
    _1 = *(int *)_2;
    *(int *)_2 = _28522;
    if( _1 != _28522 ){
        DeRef(_1);
    }
    _28522 = NOVALUE;
L1: 

    /** end procedure*/
    return;
    ;
}


void _69emit_or_inline()
{
    int _sub_54966 = NOVALUE;
    int _code_54985 = NOVALUE;
    int _28529 = NOVALUE;
    int _28528 = NOVALUE;
    int _28526 = NOVALUE;
    int _28525 = NOVALUE;
    int _28524 = NOVALUE;
    int _0, _1, _2;
    

    /** 	symtab_index sub = op_info1*/
    _sub_54966 = _43op_info1_51218;

    /** 	inline_sub = sub*/
    _69inline_sub_53679 = _sub_54966;

    /** 	if Parser_mode != PAM_NORMAL then*/
    if (_38Parser_mode_17071 == 0)
    goto L1; // [23] 42

    /** 		emit_op( PROC )*/
    _43emit_op(27);

    /** 		return*/
    DeRef(_code_54985);
    return;
    goto L2; // [39] 90
L1: 

    /** 	elsif atom( SymTab[sub][S_INLINE] ) or has_forward_params(sub) then*/
    _2 = (int)SEQ_PTR(_35SymTab_15595);
    _28524 = (int)*(((s1_ptr)_2)->base + _sub_54966);
    _2 = (int)SEQ_PTR(_28524);
    _28525 = (int)*(((s1_ptr)_2)->base + 29);
    _28524 = NOVALUE;
    _28526 = IS_ATOM(_28525);
    _28525 = NOVALUE;
    if (_28526 != 0) {
        goto L3; // [59] 72
    }
    _28528 = _43has_forward_params(_sub_54966);
    if (_28528 == 0) {
        DeRef(_28528);
        _28528 = NOVALUE;
        goto L4; // [68] 89
    }
    else {
        if (!IS_ATOM_INT(_28528) && DBL_PTR(_28528)->dbl == 0.0){
            DeRef(_28528);
            _28528 = NOVALUE;
            goto L4; // [68] 89
        }
        DeRef(_28528);
        _28528 = NOVALUE;
    }
    DeRef(_28528);
    _28528 = NOVALUE;
L3: 

    /** 		defer_call()*/
    _69defer_call();

    /** 		emit_op( PROC )*/
    _43emit_op(27);

    /** 		return*/
    DeRef(_code_54985);
    return;
L4: 
L2: 

    /** 	sequence code = get_inlined_code( sub, length(Code) )*/
    if (IS_SEQUENCE(_38Code_17038)){
            _28529 = SEQ_PTR(_38Code_17038)->length;
    }
    else {
        _28529 = 1;
    }
    _0 = _code_54985;
    _code_54985 = _69get_inlined_code(_sub_54966, _28529, 0);
    DeRef(_0);
    _28529 = NOVALUE;

    /** 	emit_inline( code )*/
    RefDS(_code_54985);
    _43emit_inline(_code_54985);

    /** 	clear_last()*/
    _43clear_last();

    /** end procedure*/
    DeRefDS(_code_54985);
    return;
    ;
}


void _69inline_deferred_calls()
{
    int _sub_54999 = NOVALUE;
    int _ix_55011 = NOVALUE;
    int _calling_sub_55013 = NOVALUE;
    int _code_55027 = NOVALUE;
    int _calls_55028 = NOVALUE;
    int _is_func_55032 = NOVALUE;
    int _offset_55039 = NOVALUE;
    int _op_55050 = NOVALUE;
    int _size_55053 = NOVALUE;
    int _28587 = NOVALUE;
    int _28585 = NOVALUE;
    int _28583 = NOVALUE;
    int _28582 = NOVALUE;
    int _28581 = NOVALUE;
    int _28579 = NOVALUE;
    int _28578 = NOVALUE;
    int _28577 = NOVALUE;
    int _28576 = NOVALUE;
    int _28575 = NOVALUE;
    int _28574 = NOVALUE;
    int _28573 = NOVALUE;
    int _28572 = NOVALUE;
    int _28571 = NOVALUE;
    int _28570 = NOVALUE;
    int _28568 = NOVALUE;
    int _28567 = NOVALUE;
    int _28566 = NOVALUE;
    int _28565 = NOVALUE;
    int _28563 = NOVALUE;
    int _28562 = NOVALUE;
    int _28561 = NOVALUE;
    int _28559 = NOVALUE;
    int _28557 = NOVALUE;
    int _28555 = NOVALUE;
    int _28553 = NOVALUE;
    int _28552 = NOVALUE;
    int _28551 = NOVALUE;
    int _28550 = NOVALUE;
    int _28548 = NOVALUE;
    int _28547 = NOVALUE;
    int _28544 = NOVALUE;
    int _28542 = NOVALUE;
    int _28540 = NOVALUE;
    int _28539 = NOVALUE;
    int _28538 = NOVALUE;
    int _28537 = NOVALUE;
    int _28536 = NOVALUE;
    int _28535 = NOVALUE;
    int _28533 = NOVALUE;
    int _28532 = NOVALUE;
    int _28531 = NOVALUE;
    int _0, _1, _2, _3;
    

    /** 	deferred_inlining = 1*/
    _69deferred_inlining_53675 = 1;

    /** 	for i = 1 to length( deferred_inline_decisions ) do*/
    if (IS_SEQUENCE(_69deferred_inline_decisions_53681)){
            _28531 = SEQ_PTR(_69deferred_inline_decisions_53681)->length;
    }
    else {
        _28531 = 1;
    }
    {
        int _i_54994;
        _i_54994 = 1;
L1: 
        if (_i_54994 > _28531){
            goto L2; // [13] 476
        }

        /** 		if length( deferred_inline_calls[i] ) then*/
        _2 = (int)SEQ_PTR(_69deferred_inline_calls_53682);
        _28532 = (int)*(((s1_ptr)_2)->base + _i_54994);
        if (IS_SEQUENCE(_28532)){
                _28533 = SEQ_PTR(_28532)->length;
        }
        else {
            _28533 = 1;
        }
        _28532 = NOVALUE;
        if (_28533 == 0)
        {
            _28533 = NOVALUE;
            goto L3; // [31] 467
        }
        else{
            _28533 = NOVALUE;
        }

        /** 			integer sub = deferred_inline_decisions[i]*/
        _2 = (int)SEQ_PTR(_69deferred_inline_decisions_53681);
        _sub_54999 = (int)*(((s1_ptr)_2)->base + _i_54994);

        /** 			check_inline( sub )*/
        _69check_inline(_sub_54999);

        /** 			if atom( SymTab[sub][S_INLINE] ) then*/
        _2 = (int)SEQ_PTR(_35SymTab_15595);
        _28535 = (int)*(((s1_ptr)_2)->base + _sub_54999);
        _2 = (int)SEQ_PTR(_28535);
        _28536 = (int)*(((s1_ptr)_2)->base + 29);
        _28535 = NOVALUE;
        _28537 = IS_ATOM(_28536);
        _28536 = NOVALUE;
        if (_28537 == 0)
        {
            _28537 = NOVALUE;
            goto L4; // [64] 74
        }
        else{
            _28537 = NOVALUE;
        }

        /** 				continue*/
        goto L5; // [71] 471
L4: 

        /** 			for cx = 1 to length( deferred_inline_calls[i] ) do*/
        _2 = (int)SEQ_PTR(_69deferred_inline_calls_53682);
        _28538 = (int)*(((s1_ptr)_2)->base + _i_54994);
        if (IS_SEQUENCE(_28538)){
                _28539 = SEQ_PTR(_28538)->length;
        }
        else {
            _28539 = 1;
        }
        _28538 = NOVALUE;
        {
            int _cx_55008;
            _cx_55008 = 1;
L6: 
            if (_cx_55008 > _28539){
                goto L7; // [85] 466
            }

            /** 				integer ix = 1*/
            _ix_55011 = 1;

            /** 				symtab_index calling_sub = deferred_inline_calls[i][cx]*/
            _2 = (int)SEQ_PTR(_69deferred_inline_calls_53682);
            _28540 = (int)*(((s1_ptr)_2)->base + _i_54994);
            _2 = (int)SEQ_PTR(_28540);
            _calling_sub_55013 = (int)*(((s1_ptr)_2)->base + _cx_55008);
            if (!IS_ATOM_INT(_calling_sub_55013)){
                _calling_sub_55013 = (long)DBL_PTR(_calling_sub_55013)->dbl;
            }
            _28540 = NOVALUE;

            /** 				CurrentSub = calling_sub*/
            _38CurrentSub_16954 = _calling_sub_55013;

            /** 				Code = SymTab[calling_sub][S_CODE]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _28542 = (int)*(((s1_ptr)_2)->base + _calling_sub_55013);
            DeRef(_38Code_17038);
            _2 = (int)SEQ_PTR(_28542);
            if (!IS_ATOM_INT(_38S_CODE_16610)){
                _38Code_17038 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
            }
            else{
                _38Code_17038 = (int)*(((s1_ptr)_2)->base + _38S_CODE_16610);
            }
            Ref(_38Code_17038);
            _28542 = NOVALUE;

            /** 				LineTable = SymTab[calling_sub][S_LINETAB]*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _28544 = (int)*(((s1_ptr)_2)->base + _calling_sub_55013);
            DeRef(_38LineTable_17039);
            _2 = (int)SEQ_PTR(_28544);
            if (!IS_ATOM_INT(_38S_LINETAB_16633)){
                _38LineTable_17039 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
            }
            else{
                _38LineTable_17039 = (int)*(((s1_ptr)_2)->base + _38S_LINETAB_16633);
            }
            Ref(_38LineTable_17039);
            _28544 = NOVALUE;

            /** 				sequence code = {}*/
            RefDS(_22663);
            DeRef(_code_55027);
            _code_55027 = _22663;

            /** 				sequence calls = find_ops( 1, PROC )*/
            RefDS(_38Code_17038);
            _0 = _calls_55028;
            _calls_55028 = _67find_ops(1, 27, _38Code_17038);
            DeRef(_0);

            /** 				integer is_func = SymTab[sub][S_TOKEN] != PROC */
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            _28547 = (int)*(((s1_ptr)_2)->base + _sub_54999);
            _2 = (int)SEQ_PTR(_28547);
            if (!IS_ATOM_INT(_38S_TOKEN_16603)){
                _28548 = (int)*(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_TOKEN_16603)->dbl));
            }
            else{
                _28548 = (int)*(((s1_ptr)_2)->base + _38S_TOKEN_16603);
            }
            _28547 = NOVALUE;
            if (IS_ATOM_INT(_28548)) {
                _is_func_55032 = (_28548 != 27);
            }
            else {
                _is_func_55032 = binary_op(NOTEQ, _28548, 27);
            }
            _28548 = NOVALUE;
            if (!IS_ATOM_INT(_is_func_55032)) {
                _1 = (long)(DBL_PTR(_is_func_55032)->dbl);
                if (UNIQUE(DBL_PTR(_is_func_55032)) && (DBL_PTR(_is_func_55032)->cleanup != 0))
                RTFatal("Cannot assign value with a destructor to an integer");                DeRefDS(_is_func_55032);
                _is_func_55032 = _1;
            }

            /** 				integer offset = 0*/
            _offset_55039 = 0;

            /** 				for o = 1 to length( calls ) do*/
            if (IS_SEQUENCE(_calls_55028)){
                    _28550 = SEQ_PTR(_calls_55028)->length;
            }
            else {
                _28550 = 1;
            }
            {
                int _o_55041;
                _o_55041 = 1;
L8: 
                if (_o_55041 > _28550){
                    goto L9; // [203] 423
                }

                /** 					if calls[o][2][2] = sub then*/
                _2 = (int)SEQ_PTR(_calls_55028);
                _28551 = (int)*(((s1_ptr)_2)->base + _o_55041);
                _2 = (int)SEQ_PTR(_28551);
                _28552 = (int)*(((s1_ptr)_2)->base + 2);
                _28551 = NOVALUE;
                _2 = (int)SEQ_PTR(_28552);
                _28553 = (int)*(((s1_ptr)_2)->base + 2);
                _28552 = NOVALUE;
                if (binary_op_a(NOTEQ, _28553, _sub_54999)){
                    _28553 = NOVALUE;
                    goto LA; // [224] 414
                }
                _28553 = NOVALUE;

                /** 						ix = calls[o][1]*/
                _2 = (int)SEQ_PTR(_calls_55028);
                _28555 = (int)*(((s1_ptr)_2)->base + _o_55041);
                _2 = (int)SEQ_PTR(_28555);
                _ix_55011 = (int)*(((s1_ptr)_2)->base + 1);
                if (!IS_ATOM_INT(_ix_55011)){
                    _ix_55011 = (long)DBL_PTR(_ix_55011)->dbl;
                }
                _28555 = NOVALUE;

                /** 						sequence op = calls[o][2]*/
                _2 = (int)SEQ_PTR(_calls_55028);
                _28557 = (int)*(((s1_ptr)_2)->base + _o_55041);
                DeRef(_op_55050);
                _2 = (int)SEQ_PTR(_28557);
                _op_55050 = (int)*(((s1_ptr)_2)->base + 2);
                Ref(_op_55050);
                _28557 = NOVALUE;

                /** 						integer size = length( op ) - 1*/
                if (IS_SEQUENCE(_op_55050)){
                        _28559 = SEQ_PTR(_op_55050)->length;
                }
                else {
                    _28559 = 1;
                }
                _size_55053 = _28559 - 1;
                _28559 = NOVALUE;

                /** 						if is_func then*/
                if (_is_func_55032 == 0)
                {
                    goto LB; // [263] 289
                }
                else{
                }

                /** 							Push( op[$] )*/
                if (IS_SEQUENCE(_op_55050)){
                        _28561 = SEQ_PTR(_op_55050)->length;
                }
                else {
                    _28561 = 1;
                }
                _2 = (int)SEQ_PTR(_op_55050);
                _28562 = (int)*(((s1_ptr)_2)->base + _28561);
                Ref(_28562);
                _43Push(_28562);
                _28562 = NOVALUE;

                /** 							op = remove( op, length(op) )*/
                if (IS_SEQUENCE(_op_55050)){
                        _28563 = SEQ_PTR(_op_55050)->length;
                }
                else {
                    _28563 = 1;
                }
                {
                    s1_ptr assign_space = SEQ_PTR(_op_55050);
                    int len = assign_space->length;
                    int start = (IS_ATOM_INT(_28563)) ? _28563 : (long)(DBL_PTR(_28563)->dbl);
                    int stop = (IS_ATOM_INT(_28563)) ? _28563 : (long)(DBL_PTR(_28563)->dbl);
                    if (stop > len){
                        stop = len;
                    }
                    if (start > len || start > stop || stop<1) {
                    }
                    else if (start < 2) {
                        if (stop >= len) {
                            Head( SEQ_PTR(_op_55050), start, &_op_55050 );
                        }
                        else Tail(SEQ_PTR(_op_55050), stop+1, &_op_55050);
                    }
                    else if (stop >= len){
                        Head(SEQ_PTR(_op_55050), start, &_op_55050);
                    }
                    else {
                        assign_slice_seq = &assign_space;
                        _op_55050 = Remove_elements(start, stop, (SEQ_PTR(_op_55050)->ref == 1));
                    }
                }
                _28563 = NOVALUE;
                _28563 = NOVALUE;
LB: 

                /** 						for p = 3 to length( op ) do*/
                if (IS_SEQUENCE(_op_55050)){
                        _28565 = SEQ_PTR(_op_55050)->length;
                }
                else {
                    _28565 = 1;
                }
                {
                    int _p_55063;
                    _p_55063 = 3;
LC: 
                    if (_p_55063 > _28565){
                        goto LD; // [294] 317
                    }

                    /** 							Push( op[p] )*/
                    _2 = (int)SEQ_PTR(_op_55050);
                    _28566 = (int)*(((s1_ptr)_2)->base + _p_55063);
                    Ref(_28566);
                    _43Push(_28566);
                    _28566 = NOVALUE;

                    /** 						end for*/
                    _p_55063 = _p_55063 + 1;
                    goto LC; // [312] 301
LD: 
                    ;
                }

                /** 						code = get_inlined_code( sub, ix + offset - 1, 1 )*/
                _28567 = _ix_55011 + _offset_55039;
                if ((long)((unsigned long)_28567 + (unsigned long)HIGH_BITS) >= 0) 
                _28567 = NewDouble((double)_28567);
                if (IS_ATOM_INT(_28567)) {
                    _28568 = _28567 - 1;
                    if ((long)((unsigned long)_28568 +(unsigned long) HIGH_BITS) >= 0){
                        _28568 = NewDouble((double)_28568);
                    }
                }
                else {
                    _28568 = NewDouble(DBL_PTR(_28567)->dbl - (double)1);
                }
                DeRef(_28567);
                _28567 = NOVALUE;
                _0 = _code_55027;
                _code_55027 = _69get_inlined_code(_sub_54999, _28568, 1);
                DeRef(_0);
                _28568 = NOVALUE;

                /** 						shift:replace_code( repeat( NOP1, length(code) ), ix + offset, ix + offset + size )*/
                if (IS_SEQUENCE(_code_55027)){
                        _28570 = SEQ_PTR(_code_55027)->length;
                }
                else {
                    _28570 = 1;
                }
                _28571 = Repeat(159, _28570);
                _28570 = NOVALUE;
                _28572 = _ix_55011 + _offset_55039;
                if ((long)((unsigned long)_28572 + (unsigned long)HIGH_BITS) >= 0) 
                _28572 = NewDouble((double)_28572);
                _28573 = _ix_55011 + _offset_55039;
                if ((long)((unsigned long)_28573 + (unsigned long)HIGH_BITS) >= 0) 
                _28573 = NewDouble((double)_28573);
                if (IS_ATOM_INT(_28573)) {
                    _28574 = _28573 + _size_55053;
                    if ((long)((unsigned long)_28574 + (unsigned long)HIGH_BITS) >= 0) 
                    _28574 = NewDouble((double)_28574);
                }
                else {
                    _28574 = NewDouble(DBL_PTR(_28573)->dbl + (double)_size_55053);
                }
                DeRef(_28573);
                _28573 = NOVALUE;
                _67replace_code(_28571, _28572, _28574);
                _28571 = NOVALUE;
                _28572 = NOVALUE;
                _28574 = NOVALUE;

                /** 						Code = eu:replace( Code, code, ix + offset, ix + offset + length( code ) -1 )*/
                _28575 = _ix_55011 + _offset_55039;
                if ((long)((unsigned long)_28575 + (unsigned long)HIGH_BITS) >= 0) 
                _28575 = NewDouble((double)_28575);
                _28576 = _ix_55011 + _offset_55039;
                if ((long)((unsigned long)_28576 + (unsigned long)HIGH_BITS) >= 0) 
                _28576 = NewDouble((double)_28576);
                if (IS_SEQUENCE(_code_55027)){
                        _28577 = SEQ_PTR(_code_55027)->length;
                }
                else {
                    _28577 = 1;
                }
                if (IS_ATOM_INT(_28576)) {
                    _28578 = _28576 + _28577;
                    if ((long)((unsigned long)_28578 + (unsigned long)HIGH_BITS) >= 0) 
                    _28578 = NewDouble((double)_28578);
                }
                else {
                    _28578 = NewDouble(DBL_PTR(_28576)->dbl + (double)_28577);
                }
                DeRef(_28576);
                _28576 = NOVALUE;
                _28577 = NOVALUE;
                if (IS_ATOM_INT(_28578)) {
                    _28579 = _28578 - 1;
                    if ((long)((unsigned long)_28579 +(unsigned long) HIGH_BITS) >= 0){
                        _28579 = NewDouble((double)_28579);
                    }
                }
                else {
                    _28579 = NewDouble(DBL_PTR(_28578)->dbl - (double)1);
                }
                DeRef(_28578);
                _28578 = NOVALUE;
                {
                    int p1 = _38Code_17038;
                    int p2 = _code_55027;
                    int p3 = _28575;
                    int p4 = _28579;
                    struct replace_block replace_params;
                    replace_params.copy_to   = &p1;
                    replace_params.copy_from = &p2;
                    replace_params.start     = &p3;
                    replace_params.stop      = &p4;
                    replace_params.target    = &_38Code_17038;
                    Replace( &replace_params );
                }
                DeRef(_28575);
                _28575 = NOVALUE;
                DeRef(_28579);
                _28579 = NOVALUE;

                /** 						offset += length(code) - size - 1*/
                if (IS_SEQUENCE(_code_55027)){
                        _28581 = SEQ_PTR(_code_55027)->length;
                }
                else {
                    _28581 = 1;
                }
                _28582 = _28581 - _size_55053;
                if ((long)((unsigned long)_28582 +(unsigned long) HIGH_BITS) >= 0){
                    _28582 = NewDouble((double)_28582);
                }
                _28581 = NOVALUE;
                if (IS_ATOM_INT(_28582)) {
                    _28583 = _28582 - 1;
                    if ((long)((unsigned long)_28583 +(unsigned long) HIGH_BITS) >= 0){
                        _28583 = NewDouble((double)_28583);
                    }
                }
                else {
                    _28583 = NewDouble(DBL_PTR(_28582)->dbl - (double)1);
                }
                DeRef(_28582);
                _28582 = NOVALUE;
                if (IS_ATOM_INT(_28583)) {
                    _offset_55039 = _offset_55039 + _28583;
                }
                else {
                    _offset_55039 = NewDouble((double)_offset_55039 + DBL_PTR(_28583)->dbl);
                }
                DeRef(_28583);
                _28583 = NOVALUE;
                if (!IS_ATOM_INT(_offset_55039)) {
                    _1 = (long)(DBL_PTR(_offset_55039)->dbl);
                    if (UNIQUE(DBL_PTR(_offset_55039)) && (DBL_PTR(_offset_55039)->cleanup != 0))
                    RTFatal("Cannot assign value with a destructor to an integer");                    DeRefDS(_offset_55039);
                    _offset_55039 = _1;
                }
LA: 
                DeRef(_op_55050);
                _op_55050 = NOVALUE;

                /** 				end for*/
                _o_55041 = _o_55041 + 1;
                goto L8; // [418] 210
L9: 
                ;
            }

            /** 				SymTab[calling_sub][S_CODE] = Code*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _35SymTab_15595 = MAKE_SEQ(_2);
            }
            _3 = (int)(_calling_sub_55013 + ((s1_ptr)_2)->base);
            RefDS(_38Code_17038);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_38S_CODE_16610))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_CODE_16610)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _38S_CODE_16610);
            _1 = *(int *)_2;
            *(int *)_2 = _38Code_17038;
            DeRef(_1);
            _28585 = NOVALUE;

            /** 				SymTab[calling_sub][S_LINETAB] = LineTable*/
            _2 = (int)SEQ_PTR(_35SymTab_15595);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                _35SymTab_15595 = MAKE_SEQ(_2);
            }
            _3 = (int)(_calling_sub_55013 + ((s1_ptr)_2)->base);
            RefDS(_38LineTable_17039);
            _2 = (int)SEQ_PTR(*(int *)_3);
            if (!UNIQUE(_2)) {
                _2 = (int)SequenceCopy((s1_ptr)_2);
                *(int *)_3 = MAKE_SEQ(_2);
            }
            if (!IS_ATOM_INT(_38S_LINETAB_16633))
            _2 = (int)(((s1_ptr)_2)->base + (int)(DBL_PTR(_38S_LINETAB_16633)->dbl));
            else
            _2 = (int)(((s1_ptr)_2)->base + _38S_LINETAB_16633);
            _1 = *(int *)_2;
            *(int *)_2 = _38LineTable_17039;
            DeRef(_1);
            _28587 = NOVALUE;
            DeRef(_code_55027);
            _code_55027 = NOVALUE;
            DeRef(_calls_55028);
            _calls_55028 = NOVALUE;

            /** 			end for*/
            _cx_55008 = _cx_55008 + 1;
            goto L6; // [461] 92
L7: 
            ;
        }
L3: 

        /** 	end for*/
L5: 
        _i_54994 = _i_54994 + 1;
        goto L1; // [471] 20
L2: 
        ;
    }

    /** end procedure*/
    _28532 = NOVALUE;
    _28538 = NOVALUE;
    return;
    ;
}



// 0x291E3A53
