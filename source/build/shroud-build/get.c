// Euphoria To C version 4.0.6 EuPortable (cbee492464f0, 2015-12-13)
#include "include/euphoria.h"
#include "main-.h"

void _15get_ch()
{
    int _1216 = NOVALUE;
    int _1215 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if sequence(input_string) then*/
    _1215 = IS_SEQUENCE(_15input_string_2726);
    if (_1215 == 0)
    {
        _1215 = NOVALUE;
        goto L1; // [8] 56
    }
    else{
        _1215 = NOVALUE;
    }

    /** 		if string_next <= length(input_string) then*/
    if (IS_SEQUENCE(_15input_string_2726)){
            _1216 = SEQ_PTR(_15input_string_2726)->length;
    }
    else {
        _1216 = 1;
    }
    if (_15string_next_2727 > _1216)
    goto L2; // [20] 47

    /** 			ch = input_string[string_next]*/
    _2 = (int)SEQ_PTR(_15input_string_2726);
    _15ch_2728 = (int)*(((s1_ptr)_2)->base + _15string_next_2727);
    if (!IS_ATOM_INT(_15ch_2728)){
        _15ch_2728 = (long)DBL_PTR(_15ch_2728)->dbl;
    }

    /** 			string_next += 1*/
    _15string_next_2727 = _15string_next_2727 + 1;
    goto L3; // [44] 81
L2: 

    /** 			ch = GET_EOF*/
    _15ch_2728 = -1;
    goto L3; // [53] 81
L1: 

    /** 		ch = getc(input_file)*/
    if (_15input_file_2725 != last_r_file_no) {
        last_r_file_ptr = which_file(_15input_file_2725, EF_READ);
        last_r_file_no = _15input_file_2725;
    }
    if (last_r_file_ptr == xstdin) {
        show_console();
        if (in_from_keyb) {
            _15ch_2728 = getKBchar();
        }
        else
        _15ch_2728 = getc(last_r_file_ptr);
    }
    else
    _15ch_2728 = getc(last_r_file_ptr);

    /** 		if ch = GET_EOF then*/
    if (_15ch_2728 != -1)
    goto L4; // [67] 80

    /** 			string_next += 1*/
    _15string_next_2727 = _15string_next_2727 + 1;
L4: 
L3: 

    /** end procedure*/
    return;
    ;
}


int _15escape_char(int _c_2755)
{
    int _i_2756 = NOVALUE;
    int _1228 = NOVALUE;
    int _0, _1, _2;
    

    /** 	i = find(c, ESCAPE_CHARS)*/
    _i_2756 = find_from(_c_2755, _15ESCAPE_CHARS_2749, 1);

    /** 	if i = 0 then*/
    if (_i_2756 != 0)
    goto L1; // [12] 25

    /** 		return GET_FAIL*/
    return 1;
    goto L2; // [22] 36
L1: 

    /** 		return ESCAPED_CHARS[i]*/
    _2 = (int)SEQ_PTR(_15ESCAPED_CHARS_2751);
    _1228 = (int)*(((s1_ptr)_2)->base + _i_2756);
    Ref(_1228);
    return _1228;
L2: 
    ;
}


int _15get_qchar()
{
    int _c_2764 = NOVALUE;
    int _1239 = NOVALUE;
    int _1238 = NOVALUE;
    int _1236 = NOVALUE;
    int _1233 = NOVALUE;
    int _0, _1, _2;
    

    /** 	get_ch()*/
    _15get_ch();

    /** 	c = ch*/
    _c_2764 = _15ch_2728;

    /** 	if ch = '\\' then*/
    if (_15ch_2728 != 92)
    goto L1; // [16] 54

    /** 		get_ch()*/
    _15get_ch();

    /** 		c = escape_char(ch)*/
    _c_2764 = _15escape_char(_15ch_2728);
    if (!IS_ATOM_INT(_c_2764)) {
        _1 = (long)(DBL_PTR(_c_2764)->dbl);
        if (UNIQUE(DBL_PTR(_c_2764)) && (DBL_PTR(_c_2764)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_c_2764);
        _c_2764 = _1;
    }

    /** 		if c = GET_FAIL then*/
    if (_c_2764 != 1)
    goto L2; // [36] 74

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1233 = MAKE_SEQ(_1);
    return _1233;
    goto L2; // [51] 74
L1: 

    /** 	elsif ch = '\'' then*/
    if (_15ch_2728 != 39)
    goto L3; // [58] 73

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1236 = MAKE_SEQ(_1);
    DeRef(_1233);
    _1233 = NOVALUE;
    return _1236;
L3: 
L2: 

    /** 	get_ch()*/
    _15get_ch();

    /** 	if ch != '\'' then*/
    if (_15ch_2728 == 39)
    goto L4; // [82] 99

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1238 = MAKE_SEQ(_1);
    DeRef(_1233);
    _1233 = NOVALUE;
    DeRef(_1236);
    _1236 = NOVALUE;
    return _1238;
    goto L5; // [96] 114
L4: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		return {GET_SUCCESS, c}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _c_2764;
    _1239 = MAKE_SEQ(_1);
    DeRef(_1233);
    _1233 = NOVALUE;
    DeRef(_1236);
    _1236 = NOVALUE;
    DeRef(_1238);
    _1238 = NOVALUE;
    return _1239;
L5: 
    ;
}


int _15get_heredoc(int _terminator_2783)
{
    int _text_2784 = NOVALUE;
    int _ends_at_2785 = NOVALUE;
    int _1254 = NOVALUE;
    int _1253 = NOVALUE;
    int _1252 = NOVALUE;
    int _1251 = NOVALUE;
    int _1250 = NOVALUE;
    int _1247 = NOVALUE;
    int _1245 = NOVALUE;
    int _1244 = NOVALUE;
    int _1243 = NOVALUE;
    int _1242 = NOVALUE;
    int _1240 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sequence text = ""*/
    RefDS(_5);
    DeRefi(_text_2784);
    _text_2784 = _5;

    /** 	integer ends_at = 1 - length( terminator )*/
    if (IS_SEQUENCE(_terminator_2783)){
            _1240 = SEQ_PTR(_terminator_2783)->length;
    }
    else {
        _1240 = 1;
    }
    _ends_at_2785 = 1 - _1240;
    _1240 = NOVALUE;

    /** 	while ends_at < 1 or not match( terminator, text, ends_at ) with entry do*/
    goto L1; // [21] 69
L2: 
    _1242 = (_ends_at_2785 < 1);
    if (_1242 != 0) {
        DeRef(_1243);
        _1243 = 1;
        goto L3; // [28] 44
    }
    _1244 = e_match_from(_terminator_2783, _text_2784, _ends_at_2785);
    _1245 = (_1244 == 0);
    _1244 = NOVALUE;
    _1243 = (_1245 != 0);
L3: 
    if (_1243 == 0)
    {
        _1243 = NOVALUE;
        goto L4; // [44] 92
    }
    else{
        _1243 = NOVALUE;
    }

    /** 		if ch = GET_EOF then*/
    if (_15ch_2728 != -1)
    goto L5; // [51] 66

    /** 			return { GET_FAIL, 0 }*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1247 = MAKE_SEQ(_1);
    DeRefDSi(_terminator_2783);
    DeRefi(_text_2784);
    DeRef(_1242);
    _1242 = NOVALUE;
    DeRef(_1245);
    _1245 = NOVALUE;
    return _1247;
L5: 

    /** 	entry*/
L1: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		text &= ch*/
    Append(&_text_2784, _text_2784, _15ch_2728);

    /** 		ends_at += 1*/
    _ends_at_2785 = _ends_at_2785 + 1;

    /** 	end while*/
    goto L2; // [89] 24
L4: 

    /** 	return { GET_SUCCESS, head( text, length( text ) - length( terminator ) ) }*/
    if (IS_SEQUENCE(_text_2784)){
            _1250 = SEQ_PTR(_text_2784)->length;
    }
    else {
        _1250 = 1;
    }
    if (IS_SEQUENCE(_terminator_2783)){
            _1251 = SEQ_PTR(_terminator_2783)->length;
    }
    else {
        _1251 = 1;
    }
    _1252 = _1250 - _1251;
    _1250 = NOVALUE;
    _1251 = NOVALUE;
    {
        int len = SEQ_PTR(_text_2784)->length;
        int size = (IS_ATOM_INT(_1252)) ? _1252 : (object)(DBL_PTR(_1252)->dbl);
        if (size <= 0){
            DeRef( _1253 );
            _1253 = MAKE_SEQ(NewS1(0));
        }
        else if (len <= size) {
            RefDS(_text_2784);
            DeRef(_1253);
            _1253 = _text_2784;
        }
        else{
            Head(SEQ_PTR(_text_2784),size+1,&_1253);
        }
    }
    _1252 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _1253;
    _1254 = MAKE_SEQ(_1);
    _1253 = NOVALUE;
    DeRefDSi(_terminator_2783);
    DeRefDSi(_text_2784);
    DeRef(_1242);
    _1242 = NOVALUE;
    DeRef(_1245);
    _1245 = NOVALUE;
    DeRef(_1247);
    _1247 = NOVALUE;
    return _1254;
    ;
}


int _15get_string()
{
    int _text_2805 = NOVALUE;
    int _1271 = NOVALUE;
    int _1267 = NOVALUE;
    int _1266 = NOVALUE;
    int _1263 = NOVALUE;
    int _1262 = NOVALUE;
    int _1261 = NOVALUE;
    int _1260 = NOVALUE;
    int _1258 = NOVALUE;
    int _1257 = NOVALUE;
    int _1255 = NOVALUE;
    int _0, _1, _2;
    

    /** 	text = ""*/
    RefDS(_5);
    DeRefi(_text_2805);
    _text_2805 = _5;

    /** 	while TRUE do*/
L1: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch = GET_EOF or ch = '\n' then*/
    _1255 = (_15ch_2728 == -1);
    if (_1255 != 0) {
        goto L2; // [25] 40
    }
    _1257 = (_15ch_2728 == 10);
    if (_1257 == 0)
    {
        DeRef(_1257);
        _1257 = NOVALUE;
        goto L3; // [36] 53
    }
    else{
        DeRef(_1257);
        _1257 = NOVALUE;
    }
L2: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1258 = MAKE_SEQ(_1);
    DeRefi(_text_2805);
    DeRef(_1255);
    _1255 = NOVALUE;
    return _1258;
    goto L4; // [50] 164
L3: 

    /** 		elsif ch = '"' then*/
    if (_15ch_2728 != 34)
    goto L5; // [57] 121

    /** 			get_ch()*/
    _15get_ch();

    /** 			if length( text ) = 0 and ch = '"' then*/
    if (IS_SEQUENCE(_text_2805)){
            _1260 = SEQ_PTR(_text_2805)->length;
    }
    else {
        _1260 = 1;
    }
    _1261 = (_1260 == 0);
    _1260 = NOVALUE;
    if (_1261 == 0) {
        goto L6; // [74] 108
    }
    _1263 = (_15ch_2728 == 34);
    if (_1263 == 0)
    {
        DeRef(_1263);
        _1263 = NOVALUE;
        goto L6; // [85] 108
    }
    else{
        DeRef(_1263);
        _1263 = NOVALUE;
    }

    /** 				if ch = '"' then*/
    if (_15ch_2728 != 34)
    goto L7; // [92] 107

    /** 					return get_heredoc( `"""` )*/
    RefDS(_1265);
    _1266 = _15get_heredoc(_1265);
    DeRefi(_text_2805);
    DeRef(_1255);
    _1255 = NOVALUE;
    DeRef(_1258);
    _1258 = NOVALUE;
    DeRef(_1261);
    _1261 = NOVALUE;
    return _1266;
L7: 
L6: 

    /** 			return {GET_SUCCESS, text}*/
    RefDS(_text_2805);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _text_2805;
    _1267 = MAKE_SEQ(_1);
    DeRefDSi(_text_2805);
    DeRef(_1255);
    _1255 = NOVALUE;
    DeRef(_1258);
    _1258 = NOVALUE;
    DeRef(_1261);
    _1261 = NOVALUE;
    DeRef(_1266);
    _1266 = NOVALUE;
    return _1267;
    goto L4; // [118] 164
L5: 

    /** 		elsif ch = '\\' then*/
    if (_15ch_2728 != 92)
    goto L8; // [125] 163

    /** 			get_ch()*/
    _15get_ch();

    /** 			ch = escape_char(ch)*/
    _0 = _15escape_char(_15ch_2728);
    _15ch_2728 = _0;
    if (!IS_ATOM_INT(_15ch_2728)) {
        _1 = (long)(DBL_PTR(_15ch_2728)->dbl);
        if (UNIQUE(DBL_PTR(_15ch_2728)) && (DBL_PTR(_15ch_2728)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_15ch_2728);
        _15ch_2728 = _1;
    }

    /** 			if ch = GET_FAIL then*/
    if (_15ch_2728 != 1)
    goto L9; // [147] 162

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1271 = MAKE_SEQ(_1);
    DeRefi(_text_2805);
    DeRef(_1255);
    _1255 = NOVALUE;
    DeRef(_1258);
    _1258 = NOVALUE;
    DeRef(_1261);
    _1261 = NOVALUE;
    DeRef(_1266);
    _1266 = NOVALUE;
    DeRef(_1267);
    _1267 = NOVALUE;
    return _1271;
L9: 
L8: 
L4: 

    /** 		text = text & ch*/
    Append(&_text_2805, _text_2805, _15ch_2728);

    /** 	end while*/
    goto L1; // [174] 13
    ;
}


int _15read_comment()
{
    int _1292 = NOVALUE;
    int _1291 = NOVALUE;
    int _1289 = NOVALUE;
    int _1287 = NOVALUE;
    int _1285 = NOVALUE;
    int _1284 = NOVALUE;
    int _1283 = NOVALUE;
    int _1281 = NOVALUE;
    int _1280 = NOVALUE;
    int _1279 = NOVALUE;
    int _1278 = NOVALUE;
    int _1277 = NOVALUE;
    int _1276 = NOVALUE;
    int _0, _1, _2;
    

    /** 	if atom(input_string) then*/
    _1276 = IS_ATOM(_15input_string_2726);
    if (_1276 == 0)
    {
        _1276 = NOVALUE;
        goto L1; // [8] 98
    }
    else{
        _1276 = NOVALUE;
    }

    /** 		while ch!='\n' and ch!='\r' and ch!=-1 do*/
L2: 
    _1277 = (_15ch_2728 != 10);
    if (_1277 == 0) {
        _1278 = 0;
        goto L3; // [22] 36
    }
    _1279 = (_15ch_2728 != 13);
    _1278 = (_1279 != 0);
L3: 
    if (_1278 == 0) {
        goto L4; // [36] 59
    }
    _1281 = (_15ch_2728 != -1);
    if (_1281 == 0)
    {
        DeRef(_1281);
        _1281 = NOVALUE;
        goto L4; // [47] 59
    }
    else{
        DeRef(_1281);
        _1281 = NOVALUE;
    }

    /** 			get_ch()*/
    _15get_ch();

    /** 		end while*/
    goto L2; // [56] 16
L4: 

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch=-1 then*/
    if (_15ch_2728 != -1)
    goto L5; // [67] 84

    /** 			return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1283 = MAKE_SEQ(_1);
    DeRef(_1277);
    _1277 = NOVALUE;
    DeRef(_1279);
    _1279 = NOVALUE;
    return _1283;
    goto L6; // [81] 182
L5: 

    /** 			return {GET_IGNORE, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _1284 = MAKE_SEQ(_1);
    DeRef(_1277);
    _1277 = NOVALUE;
    DeRef(_1279);
    _1279 = NOVALUE;
    DeRef(_1283);
    _1283 = NOVALUE;
    return _1284;
    goto L6; // [95] 182
L1: 

    /** 		for i=string_next to length(input_string) do*/
    if (IS_SEQUENCE(_15input_string_2726)){
            _1285 = SEQ_PTR(_15input_string_2726)->length;
    }
    else {
        _1285 = 1;
    }
    {
        int _i_2855;
        _i_2855 = _15string_next_2727;
L7: 
        if (_i_2855 > _1285){
            goto L8; // [107] 171
        }

        /** 			ch=input_string[i]*/
        _2 = (int)SEQ_PTR(_15input_string_2726);
        _15ch_2728 = (int)*(((s1_ptr)_2)->base + _i_2855);
        if (!IS_ATOM_INT(_15ch_2728)){
            _15ch_2728 = (long)DBL_PTR(_15ch_2728)->dbl;
        }

        /** 			if ch='\n' or ch='\r' then*/
        _1287 = (_15ch_2728 == 10);
        if (_1287 != 0) {
            goto L9; // [132] 147
        }
        _1289 = (_15ch_2728 == 13);
        if (_1289 == 0)
        {
            DeRef(_1289);
            _1289 = NOVALUE;
            goto LA; // [143] 164
        }
        else{
            DeRef(_1289);
            _1289 = NOVALUE;
        }
L9: 

        /** 				string_next=i+1*/
        _15string_next_2727 = _i_2855 + 1;

        /** 				return {GET_IGNORE, 0}*/
        _1 = NewS1(2);
        _2 = (int)((s1_ptr)_1)->base;
        ((int *)_2)[1] = -2;
        ((int *)_2)[2] = 0;
        _1291 = MAKE_SEQ(_1);
        DeRef(_1277);
        _1277 = NOVALUE;
        DeRef(_1279);
        _1279 = NOVALUE;
        DeRef(_1283);
        _1283 = NOVALUE;
        DeRef(_1284);
        _1284 = NOVALUE;
        DeRef(_1287);
        _1287 = NOVALUE;
        return _1291;
LA: 

        /** 		end for*/
        _i_2855 = _i_2855 + 1;
        goto L7; // [166] 114
L8: 
        ;
    }

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1292 = MAKE_SEQ(_1);
    DeRef(_1277);
    _1277 = NOVALUE;
    DeRef(_1279);
    _1279 = NOVALUE;
    DeRef(_1283);
    _1283 = NOVALUE;
    DeRef(_1284);
    _1284 = NOVALUE;
    DeRef(_1287);
    _1287 = NOVALUE;
    DeRef(_1291);
    _1291 = NOVALUE;
    return _1292;
L6: 
    ;
}


int _15get_number()
{
    int _sign_2867 = NOVALUE;
    int _e_sign_2868 = NOVALUE;
    int _ndigits_2869 = NOVALUE;
    int _hex_digit_2870 = NOVALUE;
    int _mantissa_2871 = NOVALUE;
    int _dec_2872 = NOVALUE;
    int _e_mag_2873 = NOVALUE;
    int _1354 = NOVALUE;
    int _1352 = NOVALUE;
    int _1350 = NOVALUE;
    int _1346 = NOVALUE;
    int _1342 = NOVALUE;
    int _1340 = NOVALUE;
    int _1339 = NOVALUE;
    int _1338 = NOVALUE;
    int _1337 = NOVALUE;
    int _1336 = NOVALUE;
    int _1334 = NOVALUE;
    int _1333 = NOVALUE;
    int _1332 = NOVALUE;
    int _1329 = NOVALUE;
    int _1327 = NOVALUE;
    int _1325 = NOVALUE;
    int _1321 = NOVALUE;
    int _1320 = NOVALUE;
    int _1318 = NOVALUE;
    int _1317 = NOVALUE;
    int _1316 = NOVALUE;
    int _1313 = NOVALUE;
    int _1312 = NOVALUE;
    int _1310 = NOVALUE;
    int _1309 = NOVALUE;
    int _1308 = NOVALUE;
    int _1307 = NOVALUE;
    int _1306 = NOVALUE;
    int _1305 = NOVALUE;
    int _1302 = NOVALUE;
    int _1298 = NOVALUE;
    int _1295 = NOVALUE;
    int _0, _1, _2;
    

    /** 	sign = +1*/
    _sign_2867 = 1;

    /** 	mantissa = 0*/
    DeRef(_mantissa_2871);
    _mantissa_2871 = 0;

    /** 	ndigits = 0*/
    _ndigits_2869 = 0;

    /** 	if ch = '-' then*/
    if (_15ch_2728 != 45)
    goto L1; // [20] 54

    /** 		sign = -1*/
    _sign_2867 = -1;

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch='-' then*/
    if (_15ch_2728 != 45)
    goto L2; // [37] 68

    /** 			return read_comment()*/
    _1295 = _15read_comment();
    DeRef(_dec_2872);
    DeRef(_e_mag_2873);
    return _1295;
    goto L2; // [51] 68
L1: 

    /** 	elsif ch = '+' then*/
    if (_15ch_2728 != 43)
    goto L3; // [58] 67

    /** 		get_ch()*/
    _15get_ch();
L3: 
L2: 

    /** 	if ch = '#' then*/
    if (_15ch_2728 != 35)
    goto L4; // [72] 170

    /** 		get_ch()*/
    _15get_ch();

    /** 		while TRUE do*/
L5: 

    /** 			hex_digit = find(ch, HEX_DIGITS)-1*/
    _1298 = find_from(_15ch_2728, _15HEX_DIGITS_2708, 1);
    _hex_digit_2870 = _1298 - 1;
    _1298 = NOVALUE;

    /** 			if hex_digit >= 0 then*/
    if (_hex_digit_2870 < 0)
    goto L6; // [102] 129

    /** 				ndigits += 1*/
    _ndigits_2869 = _ndigits_2869 + 1;

    /** 				mantissa = mantissa * 16 + hex_digit*/
    if (IS_ATOM_INT(_mantissa_2871)) {
        if (_mantissa_2871 == (short)_mantissa_2871)
        _1302 = _mantissa_2871 * 16;
        else
        _1302 = NewDouble(_mantissa_2871 * (double)16);
    }
    else {
        _1302 = NewDouble(DBL_PTR(_mantissa_2871)->dbl * (double)16);
    }
    DeRef(_mantissa_2871);
    if (IS_ATOM_INT(_1302)) {
        _mantissa_2871 = _1302 + _hex_digit_2870;
        if ((long)((unsigned long)_mantissa_2871 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2871 = NewDouble((double)_mantissa_2871);
    }
    else {
        _mantissa_2871 = NewDouble(DBL_PTR(_1302)->dbl + (double)_hex_digit_2870);
    }
    DeRef(_1302);
    _1302 = NOVALUE;

    /** 				get_ch()*/
    _15get_ch();
    goto L5; // [126] 85
L6: 

    /** 				if ndigits > 0 then*/
    if (_ndigits_2869 <= 0)
    goto L7; // [131] 152

    /** 					return {GET_SUCCESS, sign * mantissa}*/
    if (IS_ATOM_INT(_mantissa_2871)) {
        if (_sign_2867 == (short)_sign_2867 && _mantissa_2871 <= INT15 && _mantissa_2871 >= -INT15)
        _1305 = _sign_2867 * _mantissa_2871;
        else
        _1305 = NewDouble(_sign_2867 * (double)_mantissa_2871);
    }
    else {
        _1305 = NewDouble((double)_sign_2867 * DBL_PTR(_mantissa_2871)->dbl);
    }
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _1305;
    _1306 = MAKE_SEQ(_1);
    _1305 = NOVALUE;
    DeRef(_mantissa_2871);
    DeRef(_dec_2872);
    DeRef(_e_mag_2873);
    DeRef(_1295);
    _1295 = NOVALUE;
    return _1306;
    goto L5; // [149] 85
L7: 

    /** 					return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1307 = MAKE_SEQ(_1);
    DeRef(_mantissa_2871);
    DeRef(_dec_2872);
    DeRef(_e_mag_2873);
    DeRef(_1295);
    _1295 = NOVALUE;
    DeRef(_1306);
    _1306 = NOVALUE;
    return _1307;

    /** 		end while*/
    goto L5; // [166] 85
L4: 

    /** 	while ch >= '0' and ch <= '9' do*/
L8: 
    _1308 = (_15ch_2728 >= 48);
    if (_1308 == 0) {
        goto L9; // [181] 226
    }
    _1310 = (_15ch_2728 <= 57);
    if (_1310 == 0)
    {
        DeRef(_1310);
        _1310 = NOVALUE;
        goto L9; // [192] 226
    }
    else{
        DeRef(_1310);
        _1310 = NOVALUE;
    }

    /** 		ndigits += 1*/
    _ndigits_2869 = _ndigits_2869 + 1;

    /** 		mantissa = mantissa * 10 + (ch - '0')*/
    if (IS_ATOM_INT(_mantissa_2871)) {
        if (_mantissa_2871 == (short)_mantissa_2871)
        _1312 = _mantissa_2871 * 10;
        else
        _1312 = NewDouble(_mantissa_2871 * (double)10);
    }
    else {
        _1312 = NewDouble(DBL_PTR(_mantissa_2871)->dbl * (double)10);
    }
    _1313 = _15ch_2728 - 48;
    if ((long)((unsigned long)_1313 +(unsigned long) HIGH_BITS) >= 0){
        _1313 = NewDouble((double)_1313);
    }
    DeRef(_mantissa_2871);
    if (IS_ATOM_INT(_1312) && IS_ATOM_INT(_1313)) {
        _mantissa_2871 = _1312 + _1313;
        if ((long)((unsigned long)_mantissa_2871 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2871 = NewDouble((double)_mantissa_2871);
    }
    else {
        if (IS_ATOM_INT(_1312)) {
            _mantissa_2871 = NewDouble((double)_1312 + DBL_PTR(_1313)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1313)) {
                _mantissa_2871 = NewDouble(DBL_PTR(_1312)->dbl + (double)_1313);
            }
            else
            _mantissa_2871 = NewDouble(DBL_PTR(_1312)->dbl + DBL_PTR(_1313)->dbl);
        }
    }
    DeRef(_1312);
    _1312 = NOVALUE;
    DeRef(_1313);
    _1313 = NOVALUE;

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L8; // [223] 175
L9: 

    /** 	if ch = '.' then*/
    if (_15ch_2728 != 46)
    goto LA; // [230] 306

    /** 		get_ch()*/
    _15get_ch();

    /** 		dec = 10*/
    DeRef(_dec_2872);
    _dec_2872 = 10;

    /** 		while ch >= '0' and ch <= '9' do*/
LB: 
    _1316 = (_15ch_2728 >= 48);
    if (_1316 == 0) {
        goto LC; // [254] 305
    }
    _1318 = (_15ch_2728 <= 57);
    if (_1318 == 0)
    {
        DeRef(_1318);
        _1318 = NOVALUE;
        goto LC; // [265] 305
    }
    else{
        DeRef(_1318);
        _1318 = NOVALUE;
    }

    /** 			ndigits += 1*/
    _ndigits_2869 = _ndigits_2869 + 1;

    /** 			mantissa += (ch - '0') / dec*/
    _1320 = _15ch_2728 - 48;
    if ((long)((unsigned long)_1320 +(unsigned long) HIGH_BITS) >= 0){
        _1320 = NewDouble((double)_1320);
    }
    if (IS_ATOM_INT(_1320) && IS_ATOM_INT(_dec_2872)) {
        _1321 = (_1320 % _dec_2872) ? NewDouble((double)_1320 / _dec_2872) : (_1320 / _dec_2872);
    }
    else {
        if (IS_ATOM_INT(_1320)) {
            _1321 = NewDouble((double)_1320 / DBL_PTR(_dec_2872)->dbl);
        }
        else {
            if (IS_ATOM_INT(_dec_2872)) {
                _1321 = NewDouble(DBL_PTR(_1320)->dbl / (double)_dec_2872);
            }
            else
            _1321 = NewDouble(DBL_PTR(_1320)->dbl / DBL_PTR(_dec_2872)->dbl);
        }
    }
    DeRef(_1320);
    _1320 = NOVALUE;
    _0 = _mantissa_2871;
    if (IS_ATOM_INT(_mantissa_2871) && IS_ATOM_INT(_1321)) {
        _mantissa_2871 = _mantissa_2871 + _1321;
        if ((long)((unsigned long)_mantissa_2871 + (unsigned long)HIGH_BITS) >= 0) 
        _mantissa_2871 = NewDouble((double)_mantissa_2871);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2871)) {
            _mantissa_2871 = NewDouble((double)_mantissa_2871 + DBL_PTR(_1321)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1321)) {
                _mantissa_2871 = NewDouble(DBL_PTR(_mantissa_2871)->dbl + (double)_1321);
            }
            else
            _mantissa_2871 = NewDouble(DBL_PTR(_mantissa_2871)->dbl + DBL_PTR(_1321)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1321);
    _1321 = NOVALUE;

    /** 			dec *= 10*/
    _0 = _dec_2872;
    if (IS_ATOM_INT(_dec_2872)) {
        if (_dec_2872 == (short)_dec_2872)
        _dec_2872 = _dec_2872 * 10;
        else
        _dec_2872 = NewDouble(_dec_2872 * (double)10);
    }
    else {
        _dec_2872 = NewDouble(DBL_PTR(_dec_2872)->dbl * (double)10);
    }
    DeRef(_0);

    /** 			get_ch()*/
    _15get_ch();

    /** 		end while*/
    goto LB; // [302] 248
LC: 
LA: 

    /** 	if ndigits = 0 then*/
    if (_ndigits_2869 != 0)
    goto LD; // [308] 323

    /** 		return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1325 = MAKE_SEQ(_1);
    DeRef(_mantissa_2871);
    DeRef(_dec_2872);
    DeRef(_e_mag_2873);
    DeRef(_1295);
    _1295 = NOVALUE;
    DeRef(_1306);
    _1306 = NOVALUE;
    DeRef(_1307);
    _1307 = NOVALUE;
    DeRef(_1308);
    _1308 = NOVALUE;
    DeRef(_1316);
    _1316 = NOVALUE;
    return _1325;
LD: 

    /** 	mantissa = sign * mantissa*/
    _0 = _mantissa_2871;
    if (IS_ATOM_INT(_mantissa_2871)) {
        if (_sign_2867 == (short)_sign_2867 && _mantissa_2871 <= INT15 && _mantissa_2871 >= -INT15)
        _mantissa_2871 = _sign_2867 * _mantissa_2871;
        else
        _mantissa_2871 = NewDouble(_sign_2867 * (double)_mantissa_2871);
    }
    else {
        _mantissa_2871 = NewDouble((double)_sign_2867 * DBL_PTR(_mantissa_2871)->dbl);
    }
    DeRef(_0);

    /** 	if ch = 'e' or ch = 'E' then*/
    _1327 = (_15ch_2728 == 101);
    if (_1327 != 0) {
        goto LE; // [337] 352
    }
    _1329 = (_15ch_2728 == 69);
    if (_1329 == 0)
    {
        DeRef(_1329);
        _1329 = NOVALUE;
        goto LF; // [348] 573
    }
    else{
        DeRef(_1329);
        _1329 = NOVALUE;
    }
LE: 

    /** 		e_sign = +1*/
    _e_sign_2868 = 1;

    /** 		e_mag = 0*/
    DeRef(_e_mag_2873);
    _e_mag_2873 = 0;

    /** 		get_ch()*/
    _15get_ch();

    /** 		if ch = '-' then*/
    if (_15ch_2728 != 45)
    goto L10; // [370] 386

    /** 			e_sign = -1*/
    _e_sign_2868 = -1;

    /** 			get_ch()*/
    _15get_ch();
    goto L11; // [383] 400
L10: 

    /** 		elsif ch = '+' then*/
    if (_15ch_2728 != 43)
    goto L12; // [390] 399

    /** 			get_ch()*/
    _15get_ch();
L12: 
L11: 

    /** 		if ch >= '0' and ch <= '9' then*/
    _1332 = (_15ch_2728 >= 48);
    if (_1332 == 0) {
        goto L13; // [408] 487
    }
    _1334 = (_15ch_2728 <= 57);
    if (_1334 == 0)
    {
        DeRef(_1334);
        _1334 = NOVALUE;
        goto L13; // [419] 487
    }
    else{
        DeRef(_1334);
        _1334 = NOVALUE;
    }

    /** 			e_mag = ch - '0'*/
    DeRef(_e_mag_2873);
    _e_mag_2873 = _15ch_2728 - 48;
    if ((long)((unsigned long)_e_mag_2873 +(unsigned long) HIGH_BITS) >= 0){
        _e_mag_2873 = NewDouble((double)_e_mag_2873);
    }

    /** 			get_ch()*/
    _15get_ch();

    /** 			while ch >= '0' and ch <= '9' do*/
L14: 
    _1336 = (_15ch_2728 >= 48);
    if (_1336 == 0) {
        goto L15; // [445] 498
    }
    _1338 = (_15ch_2728 <= 57);
    if (_1338 == 0)
    {
        DeRef(_1338);
        _1338 = NOVALUE;
        goto L15; // [456] 498
    }
    else{
        DeRef(_1338);
        _1338 = NOVALUE;
    }

    /** 				e_mag = e_mag * 10 + ch - '0'*/
    if (IS_ATOM_INT(_e_mag_2873)) {
        if (_e_mag_2873 == (short)_e_mag_2873)
        _1339 = _e_mag_2873 * 10;
        else
        _1339 = NewDouble(_e_mag_2873 * (double)10);
    }
    else {
        _1339 = NewDouble(DBL_PTR(_e_mag_2873)->dbl * (double)10);
    }
    if (IS_ATOM_INT(_1339)) {
        _1340 = _1339 + _15ch_2728;
        if ((long)((unsigned long)_1340 + (unsigned long)HIGH_BITS) >= 0) 
        _1340 = NewDouble((double)_1340);
    }
    else {
        _1340 = NewDouble(DBL_PTR(_1339)->dbl + (double)_15ch_2728);
    }
    DeRef(_1339);
    _1339 = NOVALUE;
    DeRef(_e_mag_2873);
    if (IS_ATOM_INT(_1340)) {
        _e_mag_2873 = _1340 - 48;
        if ((long)((unsigned long)_e_mag_2873 +(unsigned long) HIGH_BITS) >= 0){
            _e_mag_2873 = NewDouble((double)_e_mag_2873);
        }
    }
    else {
        _e_mag_2873 = NewDouble(DBL_PTR(_1340)->dbl - (double)48);
    }
    DeRef(_1340);
    _1340 = NOVALUE;

    /** 				get_ch()*/
    _15get_ch();

    /** 			end while*/
    goto L14; // [481] 439
    goto L15; // [484] 498
L13: 

    /** 			return {GET_FAIL, 0} -- no exponent*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1342 = MAKE_SEQ(_1);
    DeRef(_mantissa_2871);
    DeRef(_dec_2872);
    DeRef(_e_mag_2873);
    DeRef(_1295);
    _1295 = NOVALUE;
    DeRef(_1306);
    _1306 = NOVALUE;
    DeRef(_1307);
    _1307 = NOVALUE;
    DeRef(_1308);
    _1308 = NOVALUE;
    DeRef(_1316);
    _1316 = NOVALUE;
    DeRef(_1325);
    _1325 = NOVALUE;
    DeRef(_1327);
    _1327 = NOVALUE;
    DeRef(_1332);
    _1332 = NOVALUE;
    DeRef(_1336);
    _1336 = NOVALUE;
    return _1342;
L15: 

    /** 		e_mag *= e_sign*/
    _0 = _e_mag_2873;
    if (IS_ATOM_INT(_e_mag_2873)) {
        if (_e_mag_2873 == (short)_e_mag_2873 && _e_sign_2868 <= INT15 && _e_sign_2868 >= -INT15)
        _e_mag_2873 = _e_mag_2873 * _e_sign_2868;
        else
        _e_mag_2873 = NewDouble(_e_mag_2873 * (double)_e_sign_2868);
    }
    else {
        _e_mag_2873 = NewDouble(DBL_PTR(_e_mag_2873)->dbl * (double)_e_sign_2868);
    }
    DeRef(_0);

    /** 		if e_mag > 308 then*/
    if (binary_op_a(LESSEQ, _e_mag_2873, 308)){
        goto L16; // [506] 561
    }

    /** 			mantissa *= power(10, 308)*/
    _1346 = power(10, 308);
    _0 = _mantissa_2871;
    if (IS_ATOM_INT(_mantissa_2871) && IS_ATOM_INT(_1346)) {
        if (_mantissa_2871 == (short)_mantissa_2871 && _1346 <= INT15 && _1346 >= -INT15)
        _mantissa_2871 = _mantissa_2871 * _1346;
        else
        _mantissa_2871 = NewDouble(_mantissa_2871 * (double)_1346);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2871)) {
            _mantissa_2871 = NewDouble((double)_mantissa_2871 * DBL_PTR(_1346)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1346)) {
                _mantissa_2871 = NewDouble(DBL_PTR(_mantissa_2871)->dbl * (double)_1346);
            }
            else
            _mantissa_2871 = NewDouble(DBL_PTR(_mantissa_2871)->dbl * DBL_PTR(_1346)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1346);
    _1346 = NOVALUE;

    /** 			if e_mag > 1000 then*/
    if (binary_op_a(LESSEQ, _e_mag_2873, 1000)){
        goto L17; // [522] 532
    }

    /** 				e_mag = 1000*/
    DeRef(_e_mag_2873);
    _e_mag_2873 = 1000;
L17: 

    /** 			for i = 1 to e_mag - 308 do*/
    if (IS_ATOM_INT(_e_mag_2873)) {
        _1350 = _e_mag_2873 - 308;
        if ((long)((unsigned long)_1350 +(unsigned long) HIGH_BITS) >= 0){
            _1350 = NewDouble((double)_1350);
        }
    }
    else {
        _1350 = NewDouble(DBL_PTR(_e_mag_2873)->dbl - (double)308);
    }
    {
        int _i_2953;
        _i_2953 = 1;
L18: 
        if (binary_op_a(GREATER, _i_2953, _1350)){
            goto L19; // [538] 558
        }

        /** 				mantissa *= 10*/
        _0 = _mantissa_2871;
        if (IS_ATOM_INT(_mantissa_2871)) {
            if (_mantissa_2871 == (short)_mantissa_2871)
            _mantissa_2871 = _mantissa_2871 * 10;
            else
            _mantissa_2871 = NewDouble(_mantissa_2871 * (double)10);
        }
        else {
            _mantissa_2871 = NewDouble(DBL_PTR(_mantissa_2871)->dbl * (double)10);
        }
        DeRef(_0);

        /** 			end for*/
        _0 = _i_2953;
        if (IS_ATOM_INT(_i_2953)) {
            _i_2953 = _i_2953 + 1;
            if ((long)((unsigned long)_i_2953 +(unsigned long) HIGH_BITS) >= 0){
                _i_2953 = NewDouble((double)_i_2953);
            }
        }
        else {
            _i_2953 = binary_op_a(PLUS, _i_2953, 1);
        }
        DeRef(_0);
        goto L18; // [553] 545
L19: 
        ;
        DeRef(_i_2953);
    }
    goto L1A; // [558] 572
L16: 

    /** 			mantissa *= power(10, e_mag)*/
    if (IS_ATOM_INT(_e_mag_2873)) {
        _1352 = power(10, _e_mag_2873);
    }
    else {
        temp_d.dbl = (double)10;
        _1352 = Dpower(&temp_d, DBL_PTR(_e_mag_2873));
    }
    _0 = _mantissa_2871;
    if (IS_ATOM_INT(_mantissa_2871) && IS_ATOM_INT(_1352)) {
        if (_mantissa_2871 == (short)_mantissa_2871 && _1352 <= INT15 && _1352 >= -INT15)
        _mantissa_2871 = _mantissa_2871 * _1352;
        else
        _mantissa_2871 = NewDouble(_mantissa_2871 * (double)_1352);
    }
    else {
        if (IS_ATOM_INT(_mantissa_2871)) {
            _mantissa_2871 = NewDouble((double)_mantissa_2871 * DBL_PTR(_1352)->dbl);
        }
        else {
            if (IS_ATOM_INT(_1352)) {
                _mantissa_2871 = NewDouble(DBL_PTR(_mantissa_2871)->dbl * (double)_1352);
            }
            else
            _mantissa_2871 = NewDouble(DBL_PTR(_mantissa_2871)->dbl * DBL_PTR(_1352)->dbl);
        }
    }
    DeRef(_0);
    DeRef(_1352);
    _1352 = NOVALUE;
L1A: 
LF: 

    /** 	return {GET_SUCCESS, mantissa}*/
    Ref(_mantissa_2871);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _mantissa_2871;
    _1354 = MAKE_SEQ(_1);
    DeRef(_mantissa_2871);
    DeRef(_dec_2872);
    DeRef(_e_mag_2873);
    DeRef(_1295);
    _1295 = NOVALUE;
    DeRef(_1306);
    _1306 = NOVALUE;
    DeRef(_1307);
    _1307 = NOVALUE;
    DeRef(_1308);
    _1308 = NOVALUE;
    DeRef(_1316);
    _1316 = NOVALUE;
    DeRef(_1325);
    _1325 = NOVALUE;
    DeRef(_1327);
    _1327 = NOVALUE;
    DeRef(_1332);
    _1332 = NOVALUE;
    DeRef(_1336);
    _1336 = NOVALUE;
    DeRef(_1342);
    _1342 = NOVALUE;
    DeRef(_1350);
    _1350 = NOVALUE;
    return _1354;
    ;
}


int _15Get()
{
    int _skip_blanks_1__tmp_at328_3006 = NOVALUE;
    int _skip_blanks_1__tmp_at177_2987 = NOVALUE;
    int _skip_blanks_1__tmp_at88_2978 = NOVALUE;
    int _s_2962 = NOVALUE;
    int _e_2963 = NOVALUE;
    int _e1_2964 = NOVALUE;
    int _1393 = NOVALUE;
    int _1392 = NOVALUE;
    int _1390 = NOVALUE;
    int _1387 = NOVALUE;
    int _1385 = NOVALUE;
    int _1383 = NOVALUE;
    int _1381 = NOVALUE;
    int _1378 = NOVALUE;
    int _1376 = NOVALUE;
    int _1372 = NOVALUE;
    int _1368 = NOVALUE;
    int _1365 = NOVALUE;
    int _1364 = NOVALUE;
    int _1362 = NOVALUE;
    int _1360 = NOVALUE;
    int _1358 = NOVALUE;
    int _1357 = NOVALUE;
    int _1355 = NOVALUE;
    int _0, _1, _2;
    

    /** 	while find(ch, white_space) do*/
L1: 
    _1355 = find_from(_15ch_2728, _15white_space_2744, 1);
    if (_1355 == 0)
    {
        _1355 = NOVALUE;
        goto L2; // [13] 25
    }
    else{
        _1355 = NOVALUE;
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L1; // [22] 6
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_15ch_2728 != -1)
    goto L3; // [29] 44

    /** 		return {GET_EOF, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -1;
    ((int *)_2)[2] = 0;
    _1357 = MAKE_SEQ(_1);
    DeRef(_s_2962);
    DeRef(_e_2963);
    return _1357;
L3: 

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _1358 = find_from(_15ch_2728, _15START_NUMERIC_2711, 1);
    if (_1358 == 0)
    {
        _1358 = NOVALUE;
        goto L5; // [60] 157
    }
    else{
        _1358 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_2963;
    _e_2963 = _15get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_2963);
    _1360 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1360, -2)){
        _1360 = NOVALUE;
        goto L6; // [76] 87
    }
    _1360 = NOVALUE;

    /** 				return e*/
    DeRef(_s_2962);
    DeRef(_1357);
    _1357 = NOVALUE;
    return _e_2963;
L6: 

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L7: 
    _skip_blanks_1__tmp_at88_2978 = find_from(_15ch_2728, _15white_space_2744, 1);
    if (_skip_blanks_1__tmp_at88_2978 == 0)
    {
        goto L8; // [101] 118
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L7; // [110] 94

    /** end procedure*/
    goto L8; // [115] 118
L8: 

    /** 			if ch=-1 or ch='}' then -- '}' is expected only in the "{--\n}" case*/
    _1362 = (_15ch_2728 == -1);
    if (_1362 != 0) {
        goto L9; // [128] 143
    }
    _1364 = (_15ch_2728 == 125);
    if (_1364 == 0)
    {
        DeRef(_1364);
        _1364 = NOVALUE;
        goto L4; // [139] 49
    }
    else{
        DeRef(_1364);
        _1364 = NOVALUE;
    }
L9: 

    /** 				return {GET_NOTHING, 0} -- just a comment*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = -2;
    ((int *)_2)[2] = 0;
    _1365 = MAKE_SEQ(_1);
    DeRef(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    return _1365;
    goto L4; // [154] 49
L5: 

    /** 		elsif ch = '{' then*/
    if (_15ch_2728 != 123)
    goto LA; // [161] 465

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_2962);
    _s_2962 = _5;

    /** 			get_ch()*/
    _15get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
LB: 
    _skip_blanks_1__tmp_at177_2987 = find_from(_15ch_2728, _15white_space_2744, 1);
    if (_skip_blanks_1__tmp_at177_2987 == 0)
    {
        goto LC; // [190] 207
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto LB; // [199] 183

    /** end procedure*/
    goto LC; // [204] 207
LC: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_15ch_2728 != 125)
    goto LD; // [213] 232

    /** 				get_ch()*/
    _15get_ch();

    /** 				return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_2962);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2962;
    _1368 = MAKE_SEQ(_1);
    DeRefDS(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    return _1368;
LD: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LE: 

    /** 				while 1 do -- read zero or more comments and an element*/
LF: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_2963;
    _e_2963 = _15Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_2963);
    _e1_2964 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_2964))
    _e1_2964 = (long)DBL_PTR(_e1_2964)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_2964 != 0)
    goto L10; // [257] 278

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_2963);
    _1372 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_1372);
    Append(&_s_2962, _s_2962, _1372);
    _1372 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto L11; // [273] 322
    goto LF; // [275] 242
L10: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_2964 == -2)
    goto L12; // [280] 293

    /** 						return e*/
    DeRef(_s_2962);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    return _e_2963;
    goto LF; // [290] 242
L12: 

    /** 					elsif ch='}' then*/
    if (_15ch_2728 != 125)
    goto LF; // [297] 242

    /** 						get_ch()*/
    _15get_ch();

    /** 						return {GET_SUCCESS, s} -- empty sequence*/
    RefDS(_s_2962);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2962;
    _1376 = MAKE_SEQ(_1);
    DeRefDS(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    return _1376;

    /** 				end while*/
    goto LF; // [319] 242
L11: 

    /** 				while 1 do -- now read zero or more post element comments*/
L13: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L14: 
    _skip_blanks_1__tmp_at328_3006 = find_from(_15ch_2728, _15white_space_2744, 1);
    if (_skip_blanks_1__tmp_at328_3006 == 0)
    {
        goto L15; // [341] 358
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L14; // [350] 334

    /** end procedure*/
    goto L15; // [355] 358
L15: 

    /** 					if ch = '}' then*/
    if (_15ch_2728 != 125)
    goto L16; // [364] 385

    /** 						get_ch()*/
    _15get_ch();

    /** 					return {GET_SUCCESS, s}*/
    RefDS(_s_2962);
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 0;
    ((int *)_2)[2] = _s_2962;
    _1378 = MAKE_SEQ(_1);
    DeRefDS(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    return _1378;
    goto L13; // [382] 327
L16: 

    /** 					elsif ch!='-' then*/
    if (_15ch_2728 == 45)
    goto L17; // [389] 400

    /** 						exit*/
    goto L18; // [395] 434
    goto L13; // [397] 327
L17: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_2963;
    _e_2963 = _15get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it wasn't a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_2963);
    _1381 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1381, -2)){
        _1381 = NOVALUE;
        goto L13; // [413] 327
    }
    _1381 = NOVALUE;

    /** 							return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1383 = MAKE_SEQ(_1);
    DeRef(_s_2962);
    DeRefDS(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    DeRef(_1378);
    _1378 = NOVALUE;
    return _1383;

    /** 			end while*/
    goto L13; // [431] 327
L18: 

    /** 				if ch != ',' then*/
    if (_15ch_2728 == 44)
    goto L19; // [438] 453

    /** 				return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1385 = MAKE_SEQ(_1);
    DeRef(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    DeRef(_1378);
    _1378 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    return _1385;
L19: 

    /** 			get_ch() -- skip comma*/
    _15get_ch();

    /** 			end while*/
    goto LE; // [459] 237
    goto L4; // [462] 49
LA: 

    /** 		elsif ch = '\"' then*/
    if (_15ch_2728 != 34)
    goto L1A; // [469] 485

    /** 			return get_string()*/
    _1387 = _15get_string();
    DeRef(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    DeRef(_1378);
    _1378 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1385);
    _1385 = NOVALUE;
    return _1387;
    goto L4; // [482] 49
L1A: 

    /** 		elsif ch = '`' then*/
    if (_15ch_2728 != 96)
    goto L1B; // [489] 506

    /** 			return get_heredoc("`")*/
    RefDS(_1389);
    _1390 = _15get_heredoc(_1389);
    DeRef(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    DeRef(_1378);
    _1378 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1385);
    _1385 = NOVALUE;
    DeRef(_1387);
    _1387 = NOVALUE;
    return _1390;
    goto L4; // [503] 49
L1B: 

    /** 		elsif ch = '\'' then*/
    if (_15ch_2728 != 39)
    goto L1C; // [510] 526

    /** 			return get_qchar()*/
    _1392 = _15get_qchar();
    DeRef(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    DeRef(_1378);
    _1378 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1385);
    _1385 = NOVALUE;
    DeRef(_1387);
    _1387 = NOVALUE;
    DeRef(_1390);
    _1390 = NOVALUE;
    return _1392;
    goto L4; // [523] 49
L1C: 

    /** 			return {GET_FAIL, 0}*/
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = 1;
    ((int *)_2)[2] = 0;
    _1393 = MAKE_SEQ(_1);
    DeRef(_s_2962);
    DeRef(_e_2963);
    DeRef(_1357);
    _1357 = NOVALUE;
    DeRef(_1362);
    _1362 = NOVALUE;
    DeRef(_1365);
    _1365 = NOVALUE;
    DeRef(_1368);
    _1368 = NOVALUE;
    DeRef(_1376);
    _1376 = NOVALUE;
    DeRef(_1378);
    _1378 = NOVALUE;
    DeRef(_1383);
    _1383 = NOVALUE;
    DeRef(_1385);
    _1385 = NOVALUE;
    DeRef(_1387);
    _1387 = NOVALUE;
    DeRef(_1390);
    _1390 = NOVALUE;
    DeRef(_1392);
    _1392 = NOVALUE;
    return _1393;

    /** 	end while*/
    goto L4; // [539] 49
    ;
}


int _15Get2()
{
    int _skip_blanks_1__tmp_at464_3107 = NOVALUE;
    int _skip_blanks_1__tmp_at233_3074 = NOVALUE;
    int _s_3036 = NOVALUE;
    int _e_3037 = NOVALUE;
    int _e1_3038 = NOVALUE;
    int _offset_3039 = NOVALUE;
    int _1493 = NOVALUE;
    int _1492 = NOVALUE;
    int _1491 = NOVALUE;
    int _1490 = NOVALUE;
    int _1489 = NOVALUE;
    int _1488 = NOVALUE;
    int _1487 = NOVALUE;
    int _1486 = NOVALUE;
    int _1485 = NOVALUE;
    int _1484 = NOVALUE;
    int _1483 = NOVALUE;
    int _1480 = NOVALUE;
    int _1479 = NOVALUE;
    int _1478 = NOVALUE;
    int _1477 = NOVALUE;
    int _1476 = NOVALUE;
    int _1475 = NOVALUE;
    int _1472 = NOVALUE;
    int _1471 = NOVALUE;
    int _1470 = NOVALUE;
    int _1469 = NOVALUE;
    int _1468 = NOVALUE;
    int _1467 = NOVALUE;
    int _1464 = NOVALUE;
    int _1463 = NOVALUE;
    int _1462 = NOVALUE;
    int _1461 = NOVALUE;
    int _1460 = NOVALUE;
    int _1458 = NOVALUE;
    int _1457 = NOVALUE;
    int _1456 = NOVALUE;
    int _1455 = NOVALUE;
    int _1454 = NOVALUE;
    int _1452 = NOVALUE;
    int _1449 = NOVALUE;
    int _1448 = NOVALUE;
    int _1447 = NOVALUE;
    int _1446 = NOVALUE;
    int _1445 = NOVALUE;
    int _1443 = NOVALUE;
    int _1442 = NOVALUE;
    int _1441 = NOVALUE;
    int _1440 = NOVALUE;
    int _1439 = NOVALUE;
    int _1437 = NOVALUE;
    int _1436 = NOVALUE;
    int _1435 = NOVALUE;
    int _1434 = NOVALUE;
    int _1433 = NOVALUE;
    int _1432 = NOVALUE;
    int _1429 = NOVALUE;
    int _1425 = NOVALUE;
    int _1424 = NOVALUE;
    int _1423 = NOVALUE;
    int _1422 = NOVALUE;
    int _1421 = NOVALUE;
    int _1418 = NOVALUE;
    int _1417 = NOVALUE;
    int _1416 = NOVALUE;
    int _1415 = NOVALUE;
    int _1414 = NOVALUE;
    int _1412 = NOVALUE;
    int _1411 = NOVALUE;
    int _1410 = NOVALUE;
    int _1409 = NOVALUE;
    int _1408 = NOVALUE;
    int _1407 = NOVALUE;
    int _1405 = NOVALUE;
    int _1403 = NOVALUE;
    int _1401 = NOVALUE;
    int _1400 = NOVALUE;
    int _1399 = NOVALUE;
    int _1398 = NOVALUE;
    int _1397 = NOVALUE;
    int _1395 = NOVALUE;
    int _0, _1, _2;
    

    /** 	offset = string_next-1*/
    _offset_3039 = _15string_next_2727 - 1;

    /** 	get_ch()*/
    _15get_ch();

    /** 	while find(ch, white_space) do*/
L1: 
    _1395 = find_from(_15ch_2728, _15white_space_2744, 1);
    if (_1395 == 0)
    {
        _1395 = NOVALUE;
        goto L2; // [25] 37
    }
    else{
        _1395 = NOVALUE;
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L1; // [34] 18
L2: 

    /** 	if ch = -1 then -- string is made of whitespace only*/
    if (_15ch_2728 != -1)
    goto L3; // [41] 75

    /** 		return {GET_EOF, 0, string_next-1-offset ,string_next-1}*/
    _1397 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1397 +(unsigned long) HIGH_BITS) >= 0){
        _1397 = NewDouble((double)_1397);
    }
    if (IS_ATOM_INT(_1397)) {
        _1398 = _1397 - _offset_3039;
        if ((long)((unsigned long)_1398 +(unsigned long) HIGH_BITS) >= 0){
            _1398 = NewDouble((double)_1398);
        }
    }
    else {
        _1398 = NewDouble(DBL_PTR(_1397)->dbl - (double)_offset_3039);
    }
    DeRef(_1397);
    _1397 = NOVALUE;
    _1399 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1399 +(unsigned long) HIGH_BITS) >= 0){
        _1399 = NewDouble((double)_1399);
    }
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1398;
    *((int *)(_2+16)) = _1399;
    _1400 = MAKE_SEQ(_1);
    _1399 = NOVALUE;
    _1398 = NOVALUE;
    DeRef(_s_3036);
    DeRef(_e_3037);
    return _1400;
L3: 

    /** 	leading_whitespace = string_next-2-offset -- index of the last whitespace: string_next points past the first non whitespace*/
    _1401 = _15string_next_2727 - 2;
    if ((long)((unsigned long)_1401 +(unsigned long) HIGH_BITS) >= 0){
        _1401 = NewDouble((double)_1401);
    }
    if (IS_ATOM_INT(_1401)) {
        _15leading_whitespace_3033 = _1401 - _offset_3039;
    }
    else {
        _15leading_whitespace_3033 = NewDouble(DBL_PTR(_1401)->dbl - (double)_offset_3039);
    }
    DeRef(_1401);
    _1401 = NOVALUE;
    if (!IS_ATOM_INT(_15leading_whitespace_3033)) {
        _1 = (long)(DBL_PTR(_15leading_whitespace_3033)->dbl);
        if (UNIQUE(DBL_PTR(_15leading_whitespace_3033)) && (DBL_PTR(_15leading_whitespace_3033)->cleanup != 0))
        RTFatal("Cannot assign value with a destructor to an integer");        DeRefDS(_15leading_whitespace_3033);
        _15leading_whitespace_3033 = _1;
    }

    /** 	while 1 do*/
L4: 

    /** 		if find(ch, START_NUMERIC) then*/
    _1403 = find_from(_15ch_2728, _15START_NUMERIC_2711, 1);
    if (_1403 == 0)
    {
        _1403 = NOVALUE;
        goto L5; // [105] 213
    }
    else{
        _1403 = NOVALUE;
    }

    /** 			e = get_number()*/
    _0 = _e_3037;
    _e_3037 = _15get_number();
    DeRef(_0);

    /** 			if e[1] != GET_IGNORE then -- either a number or something illegal was read, so exit: the other goto*/
    _2 = (int)SEQ_PTR(_e_3037);
    _1405 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1405, -2)){
        _1405 = NOVALUE;
        goto L6; // [121] 162
    }
    _1405 = NOVALUE;

    /** 				return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1407 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1407 +(unsigned long) HIGH_BITS) >= 0){
        _1407 = NewDouble((double)_1407);
    }
    if (IS_ATOM_INT(_1407)) {
        _1408 = _1407 - _offset_3039;
        if ((long)((unsigned long)_1408 +(unsigned long) HIGH_BITS) >= 0){
            _1408 = NewDouble((double)_1408);
        }
    }
    else {
        _1408 = NewDouble(DBL_PTR(_1407)->dbl - (double)_offset_3039);
    }
    DeRef(_1407);
    _1407 = NOVALUE;
    _1409 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1408)) {
        _1410 = _1408 - _1409;
        if ((long)((unsigned long)_1410 +(unsigned long) HIGH_BITS) >= 0){
            _1410 = NewDouble((double)_1410);
        }
    }
    else {
        _1410 = NewDouble(DBL_PTR(_1408)->dbl - (double)_1409);
    }
    DeRef(_1408);
    _1408 = NOVALUE;
    _1409 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1410;
    ((int *)_2)[2] = _15leading_whitespace_3033;
    _1411 = MAKE_SEQ(_1);
    _1410 = NOVALUE;
    Concat((object_ptr)&_1412, _e_3037, _1411);
    DeRefDS(_1411);
    _1411 = NOVALUE;
    DeRef(_s_3036);
    DeRefDS(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    return _1412;
L6: 

    /** 			get_ch()*/
    _15get_ch();

    /** 			if ch=-1 then*/
    if (_15ch_2728 != -1)
    goto L4; // [170] 94

    /** 				return {GET_NOTHING, 0, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _1414 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1414 +(unsigned long) HIGH_BITS) >= 0){
        _1414 = NewDouble((double)_1414);
    }
    if (IS_ATOM_INT(_1414)) {
        _1415 = _1414 - _offset_3039;
        if ((long)((unsigned long)_1415 +(unsigned long) HIGH_BITS) >= 0){
            _1415 = NewDouble((double)_1415);
        }
    }
    else {
        _1415 = NewDouble(DBL_PTR(_1414)->dbl - (double)_offset_3039);
    }
    DeRef(_1414);
    _1414 = NOVALUE;
    _1416 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1415)) {
        _1417 = _1415 - _1416;
        if ((long)((unsigned long)_1417 +(unsigned long) HIGH_BITS) >= 0){
            _1417 = NewDouble((double)_1417);
        }
    }
    else {
        _1417 = NewDouble(DBL_PTR(_1415)->dbl - (double)_1416);
    }
    DeRef(_1415);
    _1415 = NOVALUE;
    _1416 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = -2;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1417;
    *((int *)(_2+16)) = _15leading_whitespace_3033;
    _1418 = MAKE_SEQ(_1);
    _1417 = NOVALUE;
    DeRef(_s_3036);
    DeRef(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    return _1418;
    goto L4; // [210] 94
L5: 

    /** 		elsif ch = '{' then*/
    if (_15ch_2728 != 123)
    goto L7; // [217] 676

    /** 			s = {}*/
    RefDS(_5);
    DeRef(_s_3036);
    _s_3036 = _5;

    /** 			get_ch()*/
    _15get_ch();

    /** 			skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L8: 
    _skip_blanks_1__tmp_at233_3074 = find_from(_15ch_2728, _15white_space_2744, 1);
    if (_skip_blanks_1__tmp_at233_3074 == 0)
    {
        goto L9; // [246] 263
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L8; // [255] 239

    /** end procedure*/
    goto L9; // [260] 263
L9: 

    /** 			if ch = '}' then -- empty sequence*/
    if (_15ch_2728 != 125)
    goto LA; // [269] 313

    /** 				get_ch()*/
    _15get_ch();

    /** 				return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace} -- empty sequence*/
    _1421 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1421 +(unsigned long) HIGH_BITS) >= 0){
        _1421 = NewDouble((double)_1421);
    }
    if (IS_ATOM_INT(_1421)) {
        _1422 = _1421 - _offset_3039;
        if ((long)((unsigned long)_1422 +(unsigned long) HIGH_BITS) >= 0){
            _1422 = NewDouble((double)_1422);
        }
    }
    else {
        _1422 = NewDouble(DBL_PTR(_1421)->dbl - (double)_offset_3039);
    }
    DeRef(_1421);
    _1421 = NOVALUE;
    _1423 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1422)) {
        _1424 = _1422 - _1423;
        if ((long)((unsigned long)_1424 +(unsigned long) HIGH_BITS) >= 0){
            _1424 = NewDouble((double)_1424);
        }
    }
    else {
        _1424 = NewDouble(DBL_PTR(_1422)->dbl - (double)_1423);
    }
    DeRef(_1422);
    _1422 = NOVALUE;
    _1423 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_3036);
    *((int *)(_2+8)) = _s_3036;
    *((int *)(_2+12)) = _1424;
    *((int *)(_2+16)) = _15leading_whitespace_3033;
    _1425 = MAKE_SEQ(_1);
    _1424 = NOVALUE;
    DeRefDS(_s_3036);
    DeRef(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    return _1425;
LA: 

    /** 			while TRUE do -- read: comment(s), element, comment(s), comma and so on till it terminates or errors out*/
LB: 

    /** 				while 1 do -- read zero or more comments and an element*/
LC: 

    /** 					e = Get() -- read next element, using standard function*/
    _0 = _e_3037;
    _e_3037 = _15Get();
    DeRef(_0);

    /** 					e1 = e[1]*/
    _2 = (int)SEQ_PTR(_e_3037);
    _e1_3038 = (int)*(((s1_ptr)_2)->base + 1);
    if (!IS_ATOM_INT(_e1_3038))
    _e1_3038 = (long)DBL_PTR(_e1_3038)->dbl;

    /** 					if e1 = GET_SUCCESS then*/
    if (_e1_3038 != 0)
    goto LD; // [338] 359

    /** 						s = append(s, e[2])*/
    _2 = (int)SEQ_PTR(_e_3037);
    _1429 = (int)*(((s1_ptr)_2)->base + 2);
    Ref(_1429);
    Append(&_s_3036, _s_3036, _1429);
    _1429 = NOVALUE;

    /** 						exit  -- element read and added to result*/
    goto LE; // [354] 458
    goto LC; // [356] 323
LD: 

    /** 					elsif e1 != GET_IGNORE then*/
    if (_e1_3038 == -2)
    goto LF; // [361] 404

    /** 						return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1432 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1432 +(unsigned long) HIGH_BITS) >= 0){
        _1432 = NewDouble((double)_1432);
    }
    if (IS_ATOM_INT(_1432)) {
        _1433 = _1432 - _offset_3039;
        if ((long)((unsigned long)_1433 +(unsigned long) HIGH_BITS) >= 0){
            _1433 = NewDouble((double)_1433);
        }
    }
    else {
        _1433 = NewDouble(DBL_PTR(_1432)->dbl - (double)_offset_3039);
    }
    DeRef(_1432);
    _1432 = NOVALUE;
    _1434 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1433)) {
        _1435 = _1433 - _1434;
        if ((long)((unsigned long)_1435 +(unsigned long) HIGH_BITS) >= 0){
            _1435 = NewDouble((double)_1435);
        }
    }
    else {
        _1435 = NewDouble(DBL_PTR(_1433)->dbl - (double)_1434);
    }
    DeRef(_1433);
    _1433 = NOVALUE;
    _1434 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1435;
    ((int *)_2)[2] = _15leading_whitespace_3033;
    _1436 = MAKE_SEQ(_1);
    _1435 = NOVALUE;
    Concat((object_ptr)&_1437, _e_3037, _1436);
    DeRefDS(_1436);
    _1436 = NOVALUE;
    DeRef(_s_3036);
    DeRefDS(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    return _1437;
    goto LC; // [401] 323
LF: 

    /** 					elsif ch='}' then*/
    if (_15ch_2728 != 125)
    goto LC; // [408] 323

    /** 						get_ch()*/
    _15get_ch();

    /** 						return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1),leading_whitespace} -- empty sequence*/
    _1439 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1439 +(unsigned long) HIGH_BITS) >= 0){
        _1439 = NewDouble((double)_1439);
    }
    if (IS_ATOM_INT(_1439)) {
        _1440 = _1439 - _offset_3039;
        if ((long)((unsigned long)_1440 +(unsigned long) HIGH_BITS) >= 0){
            _1440 = NewDouble((double)_1440);
        }
    }
    else {
        _1440 = NewDouble(DBL_PTR(_1439)->dbl - (double)_offset_3039);
    }
    DeRef(_1439);
    _1439 = NOVALUE;
    _1441 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1440)) {
        _1442 = _1440 - _1441;
        if ((long)((unsigned long)_1442 +(unsigned long) HIGH_BITS) >= 0){
            _1442 = NewDouble((double)_1442);
        }
    }
    else {
        _1442 = NewDouble(DBL_PTR(_1440)->dbl - (double)_1441);
    }
    DeRef(_1440);
    _1440 = NOVALUE;
    _1441 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_3036);
    *((int *)(_2+8)) = _s_3036;
    *((int *)(_2+12)) = _1442;
    *((int *)(_2+16)) = _15leading_whitespace_3033;
    _1443 = MAKE_SEQ(_1);
    _1442 = NOVALUE;
    DeRefDS(_s_3036);
    DeRef(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    return _1443;

    /** 				end while*/
    goto LC; // [455] 323
LE: 

    /** 				while 1 do -- now read zero or more post element comments*/
L10: 

    /** 					skip_blanks()*/

    /** 	while find(ch, white_space) do*/
L11: 
    _skip_blanks_1__tmp_at464_3107 = find_from(_15ch_2728, _15white_space_2744, 1);
    if (_skip_blanks_1__tmp_at464_3107 == 0)
    {
        goto L12; // [477] 494
    }
    else{
    }

    /** 		get_ch()*/
    _15get_ch();

    /** 	end while*/
    goto L11; // [486] 470

    /** end procedure*/
    goto L12; // [491] 494
L12: 

    /** 					if ch = '}' then*/
    if (_15ch_2728 != 125)
    goto L13; // [500] 546

    /** 						get_ch()*/
    _15get_ch();

    /** 					return {GET_SUCCESS, s, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1445 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1445 +(unsigned long) HIGH_BITS) >= 0){
        _1445 = NewDouble((double)_1445);
    }
    if (IS_ATOM_INT(_1445)) {
        _1446 = _1445 - _offset_3039;
        if ((long)((unsigned long)_1446 +(unsigned long) HIGH_BITS) >= 0){
            _1446 = NewDouble((double)_1446);
        }
    }
    else {
        _1446 = NewDouble(DBL_PTR(_1445)->dbl - (double)_offset_3039);
    }
    DeRef(_1445);
    _1445 = NOVALUE;
    _1447 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1446)) {
        _1448 = _1446 - _1447;
        if ((long)((unsigned long)_1448 +(unsigned long) HIGH_BITS) >= 0){
            _1448 = NewDouble((double)_1448);
        }
    }
    else {
        _1448 = NewDouble(DBL_PTR(_1446)->dbl - (double)_1447);
    }
    DeRef(_1446);
    _1446 = NOVALUE;
    _1447 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 0;
    RefDS(_s_3036);
    *((int *)(_2+8)) = _s_3036;
    *((int *)(_2+12)) = _1448;
    *((int *)(_2+16)) = _15leading_whitespace_3033;
    _1449 = MAKE_SEQ(_1);
    _1448 = NOVALUE;
    DeRefDS(_s_3036);
    DeRef(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    return _1449;
    goto L10; // [543] 463
L13: 

    /** 					elsif ch!='-' then*/
    if (_15ch_2728 == 45)
    goto L14; // [550] 561

    /** 						exit*/
    goto L15; // [556] 620
    goto L10; // [558] 463
L14: 

    /** 						e = get_number() -- reads anything starting with '-'*/
    _0 = _e_3037;
    _e_3037 = _15get_number();
    DeRef(_0);

    /** 						if e[1] != GET_IGNORE then  -- it was not a comment, this is illegal*/
    _2 = (int)SEQ_PTR(_e_3037);
    _1452 = (int)*(((s1_ptr)_2)->base + 1);
    if (binary_op_a(EQUALS, _1452, -2)){
        _1452 = NOVALUE;
        goto L10; // [574] 463
    }
    _1452 = NOVALUE;

    /** 							return {GET_FAIL, 0, string_next-1-offset-(ch!=-1),leading_whitespace}*/
    _1454 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1454 +(unsigned long) HIGH_BITS) >= 0){
        _1454 = NewDouble((double)_1454);
    }
    if (IS_ATOM_INT(_1454)) {
        _1455 = _1454 - _offset_3039;
        if ((long)((unsigned long)_1455 +(unsigned long) HIGH_BITS) >= 0){
            _1455 = NewDouble((double)_1455);
        }
    }
    else {
        _1455 = NewDouble(DBL_PTR(_1454)->dbl - (double)_offset_3039);
    }
    DeRef(_1454);
    _1454 = NOVALUE;
    _1456 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1455)) {
        _1457 = _1455 - _1456;
        if ((long)((unsigned long)_1457 +(unsigned long) HIGH_BITS) >= 0){
            _1457 = NewDouble((double)_1457);
        }
    }
    else {
        _1457 = NewDouble(DBL_PTR(_1455)->dbl - (double)_1456);
    }
    DeRef(_1455);
    _1455 = NOVALUE;
    _1456 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1457;
    *((int *)(_2+16)) = _15leading_whitespace_3033;
    _1458 = MAKE_SEQ(_1);
    _1457 = NOVALUE;
    DeRef(_s_3036);
    DeRefDS(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    DeRef(_1449);
    _1449 = NOVALUE;
    return _1458;

    /** 			end while*/
    goto L10; // [617] 463
L15: 

    /** 				if ch != ',' then*/
    if (_15ch_2728 == 44)
    goto L16; // [624] 664

    /** 				return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1460 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1460 +(unsigned long) HIGH_BITS) >= 0){
        _1460 = NewDouble((double)_1460);
    }
    if (IS_ATOM_INT(_1460)) {
        _1461 = _1460 - _offset_3039;
        if ((long)((unsigned long)_1461 +(unsigned long) HIGH_BITS) >= 0){
            _1461 = NewDouble((double)_1461);
        }
    }
    else {
        _1461 = NewDouble(DBL_PTR(_1460)->dbl - (double)_offset_3039);
    }
    DeRef(_1460);
    _1460 = NOVALUE;
    _1462 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1461)) {
        _1463 = _1461 - _1462;
        if ((long)((unsigned long)_1463 +(unsigned long) HIGH_BITS) >= 0){
            _1463 = NewDouble((double)_1463);
        }
    }
    else {
        _1463 = NewDouble(DBL_PTR(_1461)->dbl - (double)_1462);
    }
    DeRef(_1461);
    _1461 = NOVALUE;
    _1462 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1463;
    *((int *)(_2+16)) = _15leading_whitespace_3033;
    _1464 = MAKE_SEQ(_1);
    _1463 = NOVALUE;
    DeRef(_s_3036);
    DeRef(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    DeRef(_1449);
    _1449 = NOVALUE;
    DeRef(_1458);
    _1458 = NOVALUE;
    return _1464;
L16: 

    /** 			get_ch() -- skip comma*/
    _15get_ch();

    /** 			end while*/
    goto LB; // [670] 318
    goto L4; // [673] 94
L7: 

    /** 		elsif ch = '\"' then*/
    if (_15ch_2728 != 34)
    goto L17; // [680] 730

    /** 			e = get_string()*/
    _0 = _e_3037;
    _e_3037 = _15get_string();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1467 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1467 +(unsigned long) HIGH_BITS) >= 0){
        _1467 = NewDouble((double)_1467);
    }
    if (IS_ATOM_INT(_1467)) {
        _1468 = _1467 - _offset_3039;
        if ((long)((unsigned long)_1468 +(unsigned long) HIGH_BITS) >= 0){
            _1468 = NewDouble((double)_1468);
        }
    }
    else {
        _1468 = NewDouble(DBL_PTR(_1467)->dbl - (double)_offset_3039);
    }
    DeRef(_1467);
    _1467 = NOVALUE;
    _1469 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1468)) {
        _1470 = _1468 - _1469;
        if ((long)((unsigned long)_1470 +(unsigned long) HIGH_BITS) >= 0){
            _1470 = NewDouble((double)_1470);
        }
    }
    else {
        _1470 = NewDouble(DBL_PTR(_1468)->dbl - (double)_1469);
    }
    DeRef(_1468);
    _1468 = NOVALUE;
    _1469 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1470;
    ((int *)_2)[2] = _15leading_whitespace_3033;
    _1471 = MAKE_SEQ(_1);
    _1470 = NOVALUE;
    Concat((object_ptr)&_1472, _e_3037, _1471);
    DeRefDS(_1471);
    _1471 = NOVALUE;
    DeRef(_s_3036);
    DeRefDS(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    DeRef(_1449);
    _1449 = NOVALUE;
    DeRef(_1458);
    _1458 = NOVALUE;
    DeRef(_1464);
    _1464 = NOVALUE;
    return _1472;
    goto L4; // [727] 94
L17: 

    /** 		elsif ch = '`' then*/
    if (_15ch_2728 != 96)
    goto L18; // [734] 785

    /** 			e = get_heredoc("`")*/
    RefDS(_1389);
    _0 = _e_3037;
    _e_3037 = _15get_heredoc(_1389);
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1475 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1475 +(unsigned long) HIGH_BITS) >= 0){
        _1475 = NewDouble((double)_1475);
    }
    if (IS_ATOM_INT(_1475)) {
        _1476 = _1475 - _offset_3039;
        if ((long)((unsigned long)_1476 +(unsigned long) HIGH_BITS) >= 0){
            _1476 = NewDouble((double)_1476);
        }
    }
    else {
        _1476 = NewDouble(DBL_PTR(_1475)->dbl - (double)_offset_3039);
    }
    DeRef(_1475);
    _1475 = NOVALUE;
    _1477 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1476)) {
        _1478 = _1476 - _1477;
        if ((long)((unsigned long)_1478 +(unsigned long) HIGH_BITS) >= 0){
            _1478 = NewDouble((double)_1478);
        }
    }
    else {
        _1478 = NewDouble(DBL_PTR(_1476)->dbl - (double)_1477);
    }
    DeRef(_1476);
    _1476 = NOVALUE;
    _1477 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1478;
    ((int *)_2)[2] = _15leading_whitespace_3033;
    _1479 = MAKE_SEQ(_1);
    _1478 = NOVALUE;
    Concat((object_ptr)&_1480, _e_3037, _1479);
    DeRefDS(_1479);
    _1479 = NOVALUE;
    DeRef(_s_3036);
    DeRefDS(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    DeRef(_1449);
    _1449 = NOVALUE;
    DeRef(_1458);
    _1458 = NOVALUE;
    DeRef(_1464);
    _1464 = NOVALUE;
    DeRef(_1472);
    _1472 = NOVALUE;
    return _1480;
    goto L4; // [782] 94
L18: 

    /** 		elsif ch = '\'' then*/
    if (_15ch_2728 != 39)
    goto L19; // [789] 839

    /** 			e = get_qchar()*/
    _0 = _e_3037;
    _e_3037 = _15get_qchar();
    DeRef(_0);

    /** 			return e & {string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1483 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1483 +(unsigned long) HIGH_BITS) >= 0){
        _1483 = NewDouble((double)_1483);
    }
    if (IS_ATOM_INT(_1483)) {
        _1484 = _1483 - _offset_3039;
        if ((long)((unsigned long)_1484 +(unsigned long) HIGH_BITS) >= 0){
            _1484 = NewDouble((double)_1484);
        }
    }
    else {
        _1484 = NewDouble(DBL_PTR(_1483)->dbl - (double)_offset_3039);
    }
    DeRef(_1483);
    _1483 = NOVALUE;
    _1485 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1484)) {
        _1486 = _1484 - _1485;
        if ((long)((unsigned long)_1486 +(unsigned long) HIGH_BITS) >= 0){
            _1486 = NewDouble((double)_1486);
        }
    }
    else {
        _1486 = NewDouble(DBL_PTR(_1484)->dbl - (double)_1485);
    }
    DeRef(_1484);
    _1484 = NOVALUE;
    _1485 = NOVALUE;
    _1 = NewS1(2);
    _2 = (int)((s1_ptr)_1)->base;
    ((int *)_2)[1] = _1486;
    ((int *)_2)[2] = _15leading_whitespace_3033;
    _1487 = MAKE_SEQ(_1);
    _1486 = NOVALUE;
    Concat((object_ptr)&_1488, _e_3037, _1487);
    DeRefDS(_1487);
    _1487 = NOVALUE;
    DeRef(_s_3036);
    DeRefDS(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    DeRef(_1449);
    _1449 = NOVALUE;
    DeRef(_1458);
    _1458 = NOVALUE;
    DeRef(_1464);
    _1464 = NOVALUE;
    DeRef(_1472);
    _1472 = NOVALUE;
    DeRef(_1480);
    _1480 = NOVALUE;
    return _1488;
    goto L4; // [836] 94
L19: 

    /** 			return {GET_FAIL, 0, string_next-1-offset-(ch!=-1), leading_whitespace}*/
    _1489 = _15string_next_2727 - 1;
    if ((long)((unsigned long)_1489 +(unsigned long) HIGH_BITS) >= 0){
        _1489 = NewDouble((double)_1489);
    }
    if (IS_ATOM_INT(_1489)) {
        _1490 = _1489 - _offset_3039;
        if ((long)((unsigned long)_1490 +(unsigned long) HIGH_BITS) >= 0){
            _1490 = NewDouble((double)_1490);
        }
    }
    else {
        _1490 = NewDouble(DBL_PTR(_1489)->dbl - (double)_offset_3039);
    }
    DeRef(_1489);
    _1489 = NOVALUE;
    _1491 = (_15ch_2728 != -1);
    if (IS_ATOM_INT(_1490)) {
        _1492 = _1490 - _1491;
        if ((long)((unsigned long)_1492 +(unsigned long) HIGH_BITS) >= 0){
            _1492 = NewDouble((double)_1492);
        }
    }
    else {
        _1492 = NewDouble(DBL_PTR(_1490)->dbl - (double)_1491);
    }
    DeRef(_1490);
    _1490 = NOVALUE;
    _1491 = NOVALUE;
    _1 = NewS1(4);
    _2 = (int)((s1_ptr)_1)->base;
    *((int *)(_2+4)) = 1;
    *((int *)(_2+8)) = 0;
    *((int *)(_2+12)) = _1492;
    *((int *)(_2+16)) = _15leading_whitespace_3033;
    _1493 = MAKE_SEQ(_1);
    _1492 = NOVALUE;
    DeRef(_s_3036);
    DeRef(_e_3037);
    DeRef(_1400);
    _1400 = NOVALUE;
    DeRef(_1412);
    _1412 = NOVALUE;
    DeRef(_1418);
    _1418 = NOVALUE;
    DeRef(_1425);
    _1425 = NOVALUE;
    DeRef(_1437);
    _1437 = NOVALUE;
    DeRef(_1443);
    _1443 = NOVALUE;
    DeRef(_1449);
    _1449 = NOVALUE;
    DeRef(_1458);
    _1458 = NOVALUE;
    DeRef(_1464);
    _1464 = NOVALUE;
    DeRef(_1472);
    _1472 = NOVALUE;
    DeRef(_1480);
    _1480 = NOVALUE;
    DeRef(_1488);
    _1488 = NOVALUE;
    return _1493;

    /** 	end while*/
    goto L4; // [877] 94
    ;
}



// 0x3F3945CC
